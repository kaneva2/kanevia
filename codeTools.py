#!/usr/bin/env python

import os
import argparse
import fileinput
import re
import sys
import time
from datetime import date
from subprocess import check_output

_files = []

_excludes = [
    "tools/",
    "Tools/",
    "ToolsBuilt/",
    "MagicAjax",
    "Cybersource",
    "Selenium",
    "website/WOK Website/kgp/buyGameItem.aspx.cs",
    "website/Website/ParaturePassThrough.aspx.cs",
    "afx",
    "resource.h",
    "Resource.h"
]

def file_not_excluded(file):
    return not any(x in file for x in _excludes)

def get_files_git(path, ext):
    files = check_output('git ls-files {!s}'.format(path), shell=True).decode('ASCII').splitlines()
    for f in files:
        for e in ext:
            if f.endswith(e) and file_not_excluded(f):
                yield f

def get_files_ext(ext):
    global _files
    path=''
    _files = list(get_files_git(path, ext))
    return _files

def get_files_args(args):
    ext = ['.h', '.hpp', '.c', '.cpp']
    if args.headers:
        ext = ['.h', '.hpp']
    elif args.lua:
        ext = ['.lua']
    elif args.python:
        ext = ['.py']
    elif args.csharp:
        ext = ['.cs']
    elif args.sql:
        ext = ['.sql']
    elif args.css:
        ext = ['.css']
    elif args.html:
        ext = ['.html']
    elif args.php:
        ext = ['.php']
    return get_files_ext(ext)

def format_file_args(fname, args):
    cmd = 'clang-format -i ' + fname
    cmd2 = 'unix2dos ' + fname
    check_output(cmd, shell=True)
    check_output(cmd2, shell=True)
    return True

def once_file_args(fname, args):
    wordsIf = []
    lineIf = 0
    lineDefine = 0
    lineEnd = 0
    found = False
    with open(fname) as f:
        lines = f.readlines()
        f.close()
    for i, line in enumerate(lines):
        if not found:
            if "#pragma once" in line:
                return False            
            if "#endif" in line:
                return False
            if "#ifndef" in line:
                found = True
                wordsIf = line.split()
                lineIf = i                
        else:
            if "#define" in line:
                wordsDefine = line.split()
                if (wordsDefine[1] == wordsIf[1]):
                    lineDefine = i
                    for j in range(i, len(lines)):
                        if "#endif" in lines[j]:
                            lineEnd = j
                    with open(fname+".new", "w+") as fNew:
                        for k in range(0, len(lines)):
                            if (k == lineIf):
                                fNew.write("#pragma once\n");
                            elif (k == lineDefine) or (k == lineEnd):
                                continue
                            else:
                                fNew.write(lines[k])
                        fNew.close()
                        os.remove(fname)
                        os.rename(fname+".new", fname)
                    return True
            return False
    with open(fname+".new", "w+") as fNew:
        fNew.write("#pragma once\n");
        for k in range(0, len(lines)):
            fNew.write(lines[k])
        fNew.close()
        os.remove(fname)
        os.rename(fname+".new", fname)
    return True
        
def process_files_args(args):
    print('Processing...')
    start = time.time()
    files = get_files_args(args)
    for f in files:
        print(f)    
        if args.format:
            format_file_args(f, args)
        if args.once:
            once_file_args(f, args)
    end = time.time()
    print('{!s} files processed in {!s} sec.\n'.format(len(files), end-start))

def parse_args():
    description = "codeTools"
    epilog = """
Usage:
  File selection -> 
    --sources - .h, .hpp, .c, .cpp
    --headers - .h, .hpp
    --lua - .lua
    --python - .py
    --csharp - .cs
    --sql - .sql
    --css - .css
    --html - .html
    --php - .php
  Process selection ->
    --format - formats selected files
    --once - pragma onces selected files
""".format(arg0=sys.argv[0])
    parser = argparse.ArgumentParser(description=description, epilog=epilog, formatter_class=argparse.RawDescriptionHelpFormatter)
    current_year = date.today().year
    parser.add_argument("--sources", "-cpp", help="all non git-ignored .h,.hpp,.c,.cpp files", action="count", default=0)
    parser.add_argument("--headers", "-hpp", help="all non git-ignored .h,.hpp files", action="count", default=0)
    parser.add_argument("--lua", "-lua", help="all non git-ignored .lua files", action="count", default=0)
    parser.add_argument("--python", "-py", help="all non git-ignored .py files", action="count", default=0)
    parser.add_argument("--csharp", "-cs", help="all non git-ignored .cs files", action="count", default=0)
    parser.add_argument("--sql", "-sql", help="all non git-ignored .sql files", action="count", default=0)
    parser.add_argument("--css", "-css", help="all non git-ignored .css files", action="count", default=0)
    parser.add_argument("--php", "-php", help="all non git-ignored .php files", action="count", default=0)
    parser.add_argument("--html", "-html", help="all non git-ignored .html files", action="count", default=0)
    parser.add_argument("--format", "-f", help="formats selected files", action="count", default=0)
    parser.add_argument("--once", "-o", help="pragma onces selected files", action="count", default=0)
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    args = parse_args()
    process_files_args(args)
