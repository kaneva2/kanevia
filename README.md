***Copyright Kaneva 2001-2020***  

***This file is licensed under the PolyForm Noncommercial License 1.0.0***  

***https://polyformproject.org/licenses/noncommercial/1.0.0/***  


# Kanevia Game Engine

The Kanevia game engine is a fork of the original proprietary Kaneva game engine that has now been open-sourced under the PolyForm Noncommercial License above and may be used or modified by anyone for any non-commercial purpose.  Negotiations for commercial licensing are possible however only through direct agreement with Chris Klaus, owner of the original Kaneva game engine.  

## Requires
   - git - https://git-scm.com/downloads
   - Visual Studio 2015 - https://visualstudio.microsoft.com/vs/older-downloads/
   - Python 2.4 - https://www.python.org/download/releases/2.4/
   - ActivePerl-5.26.3.2603-MSWin32-x64-a95bce075.exe - https://www.npackd.org/p/com.activestate.ActivePerl64/5.26.3.2603
   - MySql 5.0.96 (32-bit) -> '\Program Files (x86)\MySQL\MySQL Server 5.0' - https://downloads.mysql.com/archives/get/p/23/file/mysql-5.0.96-win32.zip
   - OpenAL 1.1 -> '\Program Files (x86)\OpenAL 1.1 SDK' - https://openal.org/downloads/
   - DirectX 9.0 SDK (October 2004) -> '\Program Files (x86)\Microsoft DirectX 9.0 SDK (October 2004)' - https://archive.org/download/dxsdk_oct2004/dxsdk_oct2004.exe
   
## Setup Development Environment   

Clone Kanevia GIT repository...
```
 > mkdir \git\kanevia
 > cd \git\kanevia
 > git clone https://kaneva2@bitbucket.org/kaneva2/kanevia.git .
```

Add system environment variable KANEVIA_INCLUDES ...
```
  \Program Files (x86)\MySQL\MySQL Server 5.0\include
  \Program Files (x86)\Microsoft DirectX 9.0 SDK (October 2004)\Include
  \Program Files (x86)\OpenAL 1.1 SDK\include
```

Add system environment variable KANEVIA_LIBS ...
```
  \Program Files (x86)\MySQL\MySQL Server 5.0\lib\debug
  \Program Files (x86)\Microsoft DirectX 9.0 SDK (October 2004)\Lib
  \Program Files (x86)\OpenAL 1.1 SDK\libs\Win32
```

Add system environment variable PATH ...
```
  \python24
  \perl\bin
  \Perl64\bin
  \Perl64\site\bin
  \Program Files (x86)\MySQL\MySQL Server 5.0\bin
  \Program Files (x86)\NSIS
```

Launch core\source\platform\platform.sln

View -> Other Windows -> Property Manager

Expand AppPluginLibrary -> Debug -> Microsoft.Cpp.Win32.user -> properties -> VC++ Directories -> Include Directories -add-> $(KANEVIA_INCLUDES)
```
The full list of includes should look like this...
 \Program Files (x86)\MySQL\MySQL Server 5.0\include
 \Program Files (x86)\Microsoft DirectX 9.0 SDK (October 2004)\Include
 \Program Files (x86)\OpenAL 1.1 SDK\include
 \Program Files (x86)\Microsoft Visual Studio 14.0\VC\include
 \Program Files (x86)\Microsoft Visual Studio 14.0\VC\atlmfc\include
 \Program Files (x86)\Windows Kits\10\Include\10.0.10240.0\ucrt
 \Program Files (x86)\Microsoft SDKs\Windows\v7.1A\include
```

Expand AppPluginLibrary -> Debug -> Microsoft.Cpp.Win32.user -> properties -> VC++ Directories -> Library Directories -add-> $(KANEVIA_LIBS) 
```
The full list of libraries should look like this...
 \Program Files (x86)\MySQL\MySQL Server 5.0\lib\debug
 \Program Files (x86)\Microsoft DirectX 9.0 SDK (October 2004)\Lib
 \Program Files (x86)\OpenAL 1.1 SDK\libs\Win32
 \Program Files (x86)\Microsoft Visual Studio 14.0\VC\lib
 \Program Files (x86)\Microsoft Visual Studio 14.0\VC\atlmfc\lib
 \Program Files (x86)\Windows Kits\10\lib\10.0.10240.0\ucrt\x86
 \Program Files (x86)\Microsoft SDKs\Windows\v7.1A\lib
```

Build Solutions 
```
Open C:\Git\kanevia\core\Source\Platform\Tools\libogg-1.1.4\win32\VS2015\libogg_static
Build solution
Open C:\Git\kanevia\core\Source\Platform\Tools\libvorbis-1.2.3\win32\VS2015\vorbis_static
Build solution
```

Use dev-scripts
```
Go to C:\Git\Kanevia\dev-scripts
Run 'CopyLauncherBins' as administrator
Run 'CopyClientBins' as administrator
```

## Star Environments

'Star' environments are the server environments the client runs against.  
There are 3 environments as enumerated in Platform/Common/include/KEPStar.h:
```
 3296 - NET_STAR_PRO - Production environment where real users are playing. This is the default environment.
 3298 - NET_STAR_PRE - Preview environment where QA can preview changes before releasing into production environment.
 5316 - NET_STAR_DEV - Development environment where developers are busy breaking everything all the time and is entirely unstable.
```
 
## GameLauncher Solution

'GameLauncher' solution is located in ...\core\Source\Platform\Patcher\GameLauncher.sln
It has various projects for creating the client launcher (KanevaLauncher.exe) that downloads/patches the game client.
The launcher is what runs first on the user's computer and is responsible for downloading/patching the game client and assets appropriate for the specified Star environment.
After all required files have been downloaded/patched into the Star folder it starts the client in that environment.
  
## Platform Solution

'Platform' solution is located in ...\core\Source\Platform\Platform.sln
It has various projects for creating the game servers (KGPServer.exe) that run the worlds and the end user client (KepClient.exe) that plays the game.