***NO COPYRIGHT***  

***All files in this folder are not copyright anything, use it as you like***  

# Kanevia Web

The Kanevia game engine is a fork of the original proprietary Kaneva game engine that has now been open-sourced under the PolyForm Noncommercial License above and may be used or modified by anyone for any non-commercial purpose.  Negotiations for commercial licensing are possible however only through direct agreement with Chris Klaus, owner of the original Kaneva game engine.
The original Kaneva however used an insanely obtuse and ugly web framework that we no longer need.
This folder contains the new Kanevia web framework.

FastAPI was chosen as the python framework for the new webcalls.  

## Requires
   - Python 3.7+ - https://www.python.org/download/releases/3.7/
   - FastAPI - https://fastapi.tiangolo.com

   
## Setup Development Environment   
1. Install latest Python 3.7+ with latest pip
2. Install latest FastAPI ...
```
  > pip install fastapi[all]
```
