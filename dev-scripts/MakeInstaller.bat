@echo off
call SetVars

set K_VERSION=0.1.0

set K_INSTALLER=%K_GIT%\installer
set K_INSTALLER_INPUT=%K_INSTALLER%\input
set K_INSTALLER_OUTPUT=%K_INSTALLER%\output
set K_INSTALLER_XML=%K_INSTALLER%\Installer.xml

echo Creating Installer %K_VERSION% %K_CFG% ...

call RmMkGo %K_INSTALLER_OUTPUT%
call RmMkGo %K_INSTALLER_INPUT%

call CopyLauncherBins %K_INSTALLER_INPUT%

cd %K_INSTALLER%
call builder-cli build %K_INSTALLER_XML% windows --setvars project.version=%K_VERSION% --setvars runSignTool=0
cd %K_INSTALLER_OUTPUT%
dir

PAUSE
EXIT /B 0