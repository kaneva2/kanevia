@echo off
ECHO Checking In Menu %1 %2...

REM Set Variables
call SetVars xml %1 %2

REM Git Checkin (prompts for comment)
call git_checkin "%GFM%\%FN%"

EXIT /B 0
