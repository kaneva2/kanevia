@echo off
ECHO Changing Script %1 %2...

REM Set Variables
call SetVars lua %1 %2

del "%K_STAR_GFS%\%FN%"
call MkLink /H "%K_STAR_GFS%\%FN%" "%GFS%\%FN%"

EXIT /B 0
