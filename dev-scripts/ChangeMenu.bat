@echo off
ECHO Changing Menu %1 %2...

REM Set Variables
call SetVars xml %1 %2

REM Remove All Variant Star Menus
if exist "%K_STAR_GFM%\%1_ab*" del /F "%K_STAR_GFM%\%1_ab*"

del "%K_STAR_GFM%\%FN%"
call mklink /H "%K_STAR_GFM%\%FN%" "%GFM%\%FN%"

EXIT /B 0

