@echo on
ECHO Copying Server Binaries...

REM Set Variables
call SetVars

@echo on

REM Set Copy File Destination Paths To Server
set BIN=%K_K_SERVER_BIN%
set BLADES=%K_K_SERVER_BLADES%
set BLADESD=%K_K_SERVER_BLADESD%
set BLADESCRIPTS=%K_K_SERVER_BLADESCRIPTS%
set BLADESCRIPTSD=%K_K_SERVER_BLADESCRIPTSD%

REM Copy Server Debug Binaries
xcopy "%K_DEPLOY_BIND%\*.exe" "%BIN%" /y
xcopy "%K_DEPLOY_BIND%\*.dll" "%BIN%" /y
xcopy /s "%K_DEPLOY_BLADESD%" "%BLADESD%\" /y
xcopy /s "%K_DEPLOY_BLADESCRIPTSD%" "%BLADESCRIPTSD%\" /y

REM Delete Blades Server Does Not Use
del /F /Q "%BLADESD%\*.pdb"
del /F /Q "%BLADESD%\*.lib"
del /F /Q "%BLADESD%\*.exp"
del /F /Q "%BLADESD%\*.ilk"
del /F /Q "%BLADESCRIPTSD%\*.pdb"
del /F /Q "%BLADESCRIPTSD%\*.lib"
del /F /Q "%BLADESCRIPTSD%\*.exp"
del /F /Q "%BLADESCRIPTSD%\*.ilk"

PAUSE
EXIT /B 0
