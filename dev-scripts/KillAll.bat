@echo off

REM Kill Kaneva Processes (forceably, all threads, by name)
call taskkill /f /im tail*
call taskkill /f /im kaneva*
call taskkill /f /im kep*

REM Sleep A Second
call ping -n 2 127.0.0.1 > nul