@echo off
call SetVars

REM Set Destination Directory
if (%1)==() (
	set K_DST=%K_LAUNCHER%
) else (
	set K_DST=%1
)

echo Copying Launcher %K_CFG% Binaries to %K_DST% ...

REM Copy Launcher Binaries
set K_BINS_LAUNCHER=%K_SRC_LAUNCHER%\%K_CFG%
xcopy "%K_BINS_LAUNCHER%\*.exe" "%K_DST%\" /y
REM xcopy "%K_BINS_LAUNCHER%\*.dll" "%K_DST%\" /y

REM Copy GameContent Binaries
xcopy "%K_BINS_GC%\Crash*.*" "%K_DST%\" /y
xcopy "%K_BINS_GC%\log4cplus*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\Lzma*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\zlib*.dll" "%K_DST%\" /y

REM Copy Tools Binaries
xcopy "%K_BINS_TOOLS%\KLauncherLogCfg.cfg" "%K_DST%\" /y

echo === VERIFY ALL FILES COPIED ===
PAUSE
EXIT /B 0
