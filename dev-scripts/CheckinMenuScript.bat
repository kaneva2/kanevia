@echo off
ECHO Checking In Menu Script %1 %2...

REM Set Variables
call SetVars lua %1 %2

REM Git Checkin (prompts for comment)
call git_checkin "%GFMS%\%FN%"

EXIT /B 0