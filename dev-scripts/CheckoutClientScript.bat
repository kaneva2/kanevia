@echo off
REM echo Checking Out Client Script %1 %2...

REM Set Variables
call SetVars lua %1 %2

REM Git Checkout For Edit
call git_checkout "%GFCS%\%FN%"

EXIT /B 0
