@echo off
REM echo Checking Out Menu Script %1 %2...

REM Set Variables
call SetVars lua %1 %2

REM Git Checkout For Edit
call git_checkout "%GFMS%\%FN%"

EXIT /B 0
