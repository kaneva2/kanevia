@echo off
ECHO Removing Star DEV...

REM Set Variables
call SetVars

REM Remove Star Folder Except network.cfg
rmdir /s /q "%K_STAR_DEV%"
mkdir "%K_STAR_DEV%"
copy "\Tools\network.cfg.dev" "%K_STAR_DEV%\network.cfg"

EXIT /B 0

