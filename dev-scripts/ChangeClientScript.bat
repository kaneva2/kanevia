@echo off
ECHO Changing Client Script %1 %2...

REM Set Variables
call SetVars lua %1 %2

del "%K_STAR_GFCS%\%FN%"
call mklink /H "%K_STAR_GFCS%\%FN%" "%GFCS%\%FN%"

EXIT /B 0
