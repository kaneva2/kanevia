@echo off
ECHO Changing Menu Script %1 %2...

REM Set Variables
call SetVars lua %1 %2

del "%K_STAR_GFMS%\%FN%"
call mklink /H "%K_STAR_GFMS%\%FN%" "%GFMS%\%FN%"

EXIT /B 0
