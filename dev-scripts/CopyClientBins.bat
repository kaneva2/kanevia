@echo off
call SetVars

REM Set Destination Directory
if (%1)==() (
	set K_DST=%K_STAR%
) else (
	set K_DST=%1
)

echo Copying Client %K_CFG% Binaries to %K_DST% ...

echo on

REM Copy Client Binaries
set K_BINS_CLIENT=%K_DEPLOY_GC_SHARED%
xcopy "%K_BINS_GC%\CorePluginLibrary%D%.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\CoreUtil%D%.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\kepmath%D%.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\keputil%D%.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\kepphysics%D%.dll" "%K_DST%\" /y
xcopy "%K_BINS_CLIENT%\KClient%D%.exe" "%K_DST%\" /y
xcopy "%K_BINS_CLIENT%\KClientMedia%D%.exe" "%K_DST%\" /y

REM Copy Client Blades Binaries
xcopy "%K_BINS_CLIENT%\ClientBlades%D%\*.dll" "%K_DST%\ClientBlades%D%\" /y

REM Copy Client BladeScripts Binaries & Delete Unused
xcopy /s "%K_BINS_CLIENT%\GameFiles\bladescripts%D%" "%K_DST%\GameFiles\bladescripts%D%\" /y
del /F /Q "%K_DST%\GameFiles\bladescripts%D%\Python*.*"
del /F /Q "%K_DST%\GameFiles\bladescripts%D%\ScriptServer*.*"
del /F /Q "%K_DST%\GameFiles\bladescripts%D%\KDP*.*"
del /F /Q "%K_DST%\GameFiles\bladescripts%D%\PlayList*.*"

REM Copy GameContent Binaries
xcopy "%K_BINS_GC%\dbg*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\cjson*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\Crash*.*" "%K_DST%\" /y
xcopy "%K_BINS_GC%\f_in_box*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\libcollada*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\log4cplus*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\Lzma*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\lua*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\mysql*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\nvtt*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\Simple*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\OpenAL32*.dll" "%K_DST%\" /y
xcopy "%K_BINS_GC%\zlib*.dll" "%K_DST%\" /y

REM Copy Tools Binaries
xcopy "%K_BINS_TOOLS%\libpng*.dll" "%K_DST%\" /y
xcopy "%K_BINS_TOOLS%\jpeg62*.dll" "%K_DST%\" /y
xcopy "%K_BINS_TOOLS%\KClientLogCfg.cfg" "%K_DST%\" /y
xcopy "%K_BINS_TOOLS%\KClientMediaLogCfg.cfg" "%K_DST%\" /y

REM Copy Lua GameFiles Menus & Scripts
xcopy /s "%K_GFS%\*.lua" "%K_DST%\GameFiles\Scripts\" /y
xcopy /s "%K_GFCS%\*.lua" "%K_DST%\GameFiles\ClientScripts\" /y
xcopy /s "%K_GFMS%\*.lua" "%K_DST%\GameFiles\MenuScripts\" /y
xcopy /s "%K_GFM%\*.*" "%K_DST%\GameFiles\Menus\" /y

REM Copy Lua GameFiles ScriptServerScripts Framework Includes
set K_SSS=%K_GFSSS%\Framework\Includes
xcopy /s "%K_SSS%\*.lua" "%K_DST%\GameFiles\MenuScripts\" /y


REM Delete Deprecated ClientScripts
del /F /Q "%K_DST%\GameFiles\ClientScripts\MetricsController.*"
del /F /Q "%K_DST%\GameFiles\ClientScripts\WFTracking.*"
del /F /Q "%K_DST%\GameFiles\ClientScripts\GoogleAnalytics.*"
del /F /Q "%K_DST%\GameFiles\ClientScripts\Tut*.*"
del /F /Q "%K_DST%\GameFiles\ClientScripts\HudNotifications*.*"

REM Delete Deprecated Menus
del /F /Q "%K_DST%\GameFiles\MenuScripts\Fame*.*"
del /F /Q "%K_DST%\GameFiles\Menus\Fame*.*"
del /F /Q "%K_DST%\GameFiles\MenuScripts\MyFame*.*"
del /F /Q "%K_DST%\GameFiles\Menus\MyFame*.*"
del /F /Q "%K_DST%\GameFiles\MenuScripts\Tut*.*"
del /F /Q "%K_DST%\GameFiles\Menus\Tut*.*"
del /F /Q "%K_DST%\GameFiles\MenuScripts\Badge*.*"
del /F /Q "%K_DST%\GameFiles\Menus\Badge*.*"
del /F /Q "%K_DST%\GameFiles\MenuScripts\DailyBonus*.*"
del /F /Q "%K_DST%\GameFiles\Menus\DailyBonus*.*"
del /F /Q "%K_DST%\GameFiles\MenuScripts\builderlevel*.lua"
del /F /Q "%K_DST%\GameFiles\Menus\builderlevel*.*"
del /F /Q "%K_DST%\GameFiles\MenuScripts\LevelUp*.lua"
del /F /Q "%K_DST%\GameFiles\Menus\LevelUp*.*"
del /F /Q "%K_DST%\GameFiles\MenuScripts\Nudge*.lua"
del /F /Q "%K_DST%\GameFiles\Menus\Nudge*.*"
del /F /Q "%K_DST%\GameFiles\MenuScripts\Notification*.lua"
del /F /Q "%K_DST%\GameFiles\Menus\Notification*.*"

REM Copy Lua '*Ids' Scripts (created during build)
xcopy /s "%K_DEPLOY_GF_SCRIPTS%\*.lua" "%K_DST%\GameFiles\Scripts\" /y

echo === VERIFY ALL FILES COPIED ===
PAUSE
EXIT /B 0
