@echo off

REM K Names
set K_NAME=Kanevia
set K_NAME_CLIENT=KClient
set K_NAME_SERVER=KServer
set K_NAME_LAUNCHER=KLauncher
set K_NAME_INSTALLER=KInstaller

REM K Repository Directory
set K_GIT=\Git\kanevia

REM K Target Configuration
set K_CFG=Release
REM set K_CFG=Debug

REM K Runtime Directory
REM set K_RUN=\Program Files (x86)\%K_NAME%
set K_RUN=\%K_NAME%

REM K Core Directories
set K_CORE=%K_GIT%\core
set K_SRC=%K_CORE%\Source
set K_SRC_PLATFORM=%K_SRC%\Platform
set K_SRC_LAUNCHER=%K_SRC_PLATFORM%\Patcher
set K_SRC_TOOLS=%K_SRC_PLATFORM%\ToolsBuilt
set K_SRC_TOOLS_CRASHRPT=%K_SRC_TOOLS%\CrashRpt_v.1.4.3_r1645
set K_SRC_TOOLS_LUA=%K_SRC_TOOLS%\lua-5.1.5-coco
set K_SRC_TOOLS_CJSON=%K_SRC_TOOLS%\lua-cjson-5.1.5-coco
set K_SRC_TOOLS_F_IN_BOX=%K_SRC_TOOLS%\f_in_box-4.4.0.0

REM K Core Scripts Directories
set K_SCRIPTS=%K_GIT%\core-scripts
set K_GF=%K_SCRIPTS%\GameFiles
set K_GFS=%K_SCRIPTS%\GameFiles\Scripts
set K_GFM=%K_SCRIPTS%\GameFiles\Menus
set K_GFMS=%K_SCRIPTS%\GameFiles\MenuScripts
set K_GFCS=%K_SCRIPTS%\GameFiles\ClientScripts
set K_GFSSS=%K_SCRIPTS%\GameFiles\ScriptServerScripts

REM K Deploy Directories
set K_DEPLOY=%K_CORE%\Deploy
set K_DEPLOY_GC_BIN=%K_DEPLOY%\GameContent\bin
set K_DEPLOY_GC_BIND=%K_DEPLOY%\GameContent\bind
set K_DEPLOY_GC_BLADES=%K_DEPLOY%\GameContent\Blades
set K_DEPLOY_GC_BLADESD=%K_DEPLOY%\GameContent\Bladesd
set K_DEPLOY_GC_SHARED=%K_DEPLOY%\GameContent\templates\Shared
set K_DEPLOY_GF_BLADESCRIPTS=%K_DEPLOY_GC_SHARED%\GameFiles\bladescripts
set K_DEPLOY_GF_BLADESCRIPTSD=%K_DEPLOY_GC_SHARED%\GameFiles\bladescriptsd
set K_DEPLOY_GF_SCRIPTS=%K_DEPLOY_GC_SHARED%\GameFiles\Scripts

REM K Target Configuration Bins
if (%K_CFG%)==(Debug) (
	set D=d
	set DD=-d
	set K_BINS_GC=%K_DEPLOY_GC_BIND%
) else (
	set D=
	set DD=
	set K_BINS_GC=%K_DEPLOY_GC_BIN%
)

REM Binaries Directories
set K_BINS_TOOLS=%K_DEPLOY_GC_SHARED%
set K_BINS_CRASHRPT=%K_SRC_TOOLS_CRASHRPT%\bin

REM K Runtime Star Environments
set K_STAR_PRO_ID=3296
set K_STAR_PRE_ID=3298
set K_STAR_DEV_ID=5316
set K_STAR_ID=%K_STAR_PRO_ID%

REM K Runtime Star Directories
set K_STAR_ROOT=%K_RUN%\Star
set K_STAR=%K_STAR_ROOT%\%K_STAR_ID%
set K_STAR_GF=%K_STAR%\GameFiles
set K_STAR_GFS=%K_STAR_GF%\Scripts
set K_STAR_GFM=%K_STAR_GF%\Menus
set K_STAR_GFMS=%K_STAR_GF%\MenuScripts
set K_STAR_GFCS=%K_STAR_GF%\ClientScripts

REM K Runtime Launcher Directories
set K_LAUNCHER=%K_RUN%

REM K Runtime Client Directories
set K_CLIENT=%K_STAR%

REM K Runtime Server Directories
set K_SERVER=%K_RUN%\%K_NAME_SERVER%
set K_SERVER_BIN=%K_SERVER%\bin
set K_SERVER_BLADES=%K_SERVER%\Blades
set K_SERVER_BLADESD=%K_SERVER%\Bladesd
set K_SERVER_BLADESCRIPTS=%K_SERVER%\BladeScripts
set K_SERVER_BLADESCRIPTSD=%K_SERVER%\BladeScriptsd

REM K Script Changes Directory (symlink into runtime star directory)
set K_CHANGES=\Changes

EXIT /B 0
