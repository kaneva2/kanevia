@echo off
ECHO Checking In Client Script %1 %2...

REM Set Variables
call SetVars lua %1 %2

REM Git Checkin (prompts for comment)
call git_checkin "%GFCS%\%FN%"

EXIT /B 0