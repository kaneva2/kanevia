@echo off
ECHO Checking In Script %1 %2...

REM Set Variables
call SetVars lua %1 %2

REM Git Checkin (prompts for comment)
call git_checkin "%GFS%\%FN%"

EXIT /B 0