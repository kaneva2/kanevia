@echo off

echo RemoveAppData - Kaneva LLC - Copyright 2013
echo ------------------WARNING------------------
echo This will remove all Kaneva application user data including history, favorites, emotes, 
echo and other user preferences. Kaneva will assume default behavior upon next launch.
echo Once removed this data cannot be recovered.

:GETINPUT
set /p confirmCaseSensitive=Are you sure [y/n]?:
if "%confirmCaseSensitive%"=="y" (goto YES) else (goto NO)

:YES
rmdir /s /q "%APPDATA%\Kaneva"
echo COMPLETE
goto END

:NO
echo CANCELLED

:END
PAUSE
