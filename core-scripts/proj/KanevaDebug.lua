

assert(debug, "ERROR: debug table must be available at this point")

------------------------------------------------------------------------------------------------------------
-- Helpers
------------------------------------------------------------------------------------------------------------
StringUtils = {}

function StringUtils.split(str, pat)
  local t = {}
  local fpat = "(.-)" .. pat
  local last_end = 1
  local s, e, cap = string.find(str, fpat, 1)
  while s do
    if s ~= 1 or cap ~= "" then
      table.insert(t,cap)
    end
    last_end = e+1
    s, e, cap = string.find(str, fpat, last_end)
  end
  if last_end <= string.len(str) then
    cap = string.sub(str, last_end)
    table.insert(t, cap)
  end
  return t
end



------------------------------------------------------------------------------------------------------------
-- Class KanevaDebugger
------------------------------------------------------------------------------------------------------------

local function indent(n) 
  return string.rep(" ", n)
end


local messages = {
  init  = "Error: logger not properly initalized you must call KanevaDebugger:setLoggerFunction",
  usage = "Error: invalid arguments passed to function"
}



--
-- public 
--

KanevaDebugger = {}

function KanevaDebugger:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self

  self.properties = {
    id       = "",
    parentId = ""
  }

  self.loggerFunc    = nil
  self.logprefix     = "file[%s] func[%s] line[%s] msg[%s]"
  self.isInitialized = false
  return o
end


function KanevaDebugger:initalize()
  if (not self.isInitalize) then
    self.properties.id       = self:getFileName(1)
    self.properties.parentId = self:getFileName(2)
    self.isInitialized = true
  end
  return self.isInitialized
end


function KanevaDebugger:getLocals()
  self:initalize()
  local retval = {}
  local i=1
  repeat
    local k, v = debug.getlocal(2, i)
    if (k) then
      retval[k] = v
      i = i + 1
    end
  until k == nil
  return retval
end

function KanevaDebugger:getUpValues()
  self:initalize()

  local retval = {}
  local func = debug.getinfo(2, "f").func
  local i=1
  repeat
    local k, v = debug.getupvalue(func, i)
    if (k) then
      retval[k] = v
      i = i + 1
    end
  until k == nil
  return retval
end

function KanevaDebugger:getGlobals()
  self:initalize()

  local retval = {}
  for k,v in pairs(_G) do
    retval[k] = v
  end
  return retval
end

function KanevaDebugger:findGlobal(f)
  self:initalize()
  for k,v in pairs(_G) do
    if v == f then 
      return k 
    end
  end
end



function KanevaDebugger:getCurrentFileName(level)
  self:initalize()
  local sourceParts = StringUtils.split(debug.getinfo(level).source, "\\")
  local curFileName = sourceParts[#sourceParts]
  return curFileName or "???"
end

function KanevaDebugger:getCurrentFuncName(level)
  self:initalize()
  assert(level, messages.usage)

  local info = debug.getinfo(level, "nSlf")
  local name = info.name or self.findglobal(info.func)
  return name
end

function KanevaDebugger:getCurrentLine(level)
  self:initalize()
  assert(level, messages.usage)
  return tostring(debug.getinfo(level).currentline) or "???"
end

function KanevaDebugger:getFileName(level)
  self:initalize()

  --assert(level, messages.usage)
  logThis("_SCR_ DEBUG getFileName["..tostring(level).."]")

  local sourceParts = nil
  local retval = "???"

  local source = debug.getinfo(level).source
  if (source) then
    sourceParts = StringUtils.split(source, "\\")
    if (#sourceParts > 0) then
      retval = sourceParts[#sourceParts]
    end
  end
  return retval
end

--
--  Logging
--

function KanevaDebugger:setLoggerFunction(logFunc)
  self:initalize()
  assert(logFunc, messages.usage)
  self.loggerFunc = logFunc
end

function KanevaDebugger:getStackDepth(caller)
  self:initalize()
  assert(caller, messages.usage)

  local stackLevel = 2
  local itr
  repeat
    itr = self:getFileName(stackLevel)
    stackLevel = stackLevel + 1
  until (itr == caller)

  return stackLevel
end


function KanevaDebugger:log(str)
  self:initalize()
  assert(self.loggerFunc, messages.init)

  local stackLevel  = self:getStackDepth(self.properties.parentId)
  local msg = string.format(self.logprefix, self:getFileName(stackLevel), self:getCurrentFuncName(stackLevel), self:getCurrentLine(stackLevel), tostring(str))
  self.loggerFunc(str)
end

function KanevaDebugger:logVarDump(aVar, indent)
  assert(self.loggerFunc)

  indent = indent or 0
  if (type(aVar) ~= "table") then
    self:log(string.rep(" ", indent).." Value["..tostring(aVar).."]")
  else
    for k, v in pairs(aVar) do
      if type(v) == "table" then
        self:log(string.rep(" ", indent).." Key["..tostring(k).."]")
        self:logVarDump(v, indent + 2)
      else 
        self:log(string.rep(" ", indent).." Key["..tostring(k).."], Value["..tostring(v).."]")
      end
    end
  end
end



traceData = {}
local Depth = nil

function KanevaDebugger:startTrace()

  self:log("starting trace")

  -- Before setting the hook, find the bottom of the stack
  if not Depth then
    Depth = 1
    for Info in
      function()
      return debug.getinfo(Depth, "n")
      end
    do
      Depth = Depth + 1
    end
    -- Don't count the iterator itself or the empty frame counted at the end of the loop:
    Depth = Depth - 2
    debug.sethook(Hook, "cr")
  else
    -- Do nothing if Trace() is called twice.
  end

end

function KanevaDebugger:stopTrace()
  debug.sethook()
  Depth = nil
  self:log("stoping trace")
end


function KanevaDebugger:GetInfo(StackLvl, WithLineNum)
-- StackLvl is reckoned from the caller's level:
  StackLvl = StackLvl + 1
  local Ret
  local Info = debug.getinfo(StackLvl, "nlS")
  if Info then
    local Name, What, LineNum, ShortSrc = Info.name, Info.what, Info.currentline, Info.short_src
    if What == "tail" then
      Ret = "overwritten stack frame"
    else
      if not Name then
        if What == "main" then
          Name = "chunk"
        else
          Name = What .. "function"
        end
      end

      if Name == "C function" then
        Ret = Name
      else
        -- Only use real line numbers:
        LineNum = LineNum >= 1 and LineNum
        if WithLineNum and LineNum then
          Ret = Name .. " (" .. ShortSrc .. ", line " .. LineNum .. ")"
        else
          Ret = Name .. " (" .. ShortSrc .. ")"
        end
      end
    end
  else
    -- Below the bottom of the stack:
    Ret = "nowhere"
  end
  return Ret
end

function KanevaDebugger:Hook(Event)

  -- Info for the running function being called or returned from:
  local Running = self:GetInfo(2)

  -- Info for the function that called that function:
  local Caller = self:GetInfo(3, true)
  if not string.find(Running..Caller, "modules") then
    if Event == "call" then
      Depth = Depth + 1
      table.insert(traceData, tostring(Indent(Depth) .. "calling " ..Running.." from "..Caller.. "\n"))
    else
      local RetType
      if Event == "return" then
        RetType = "returning from "
      elseif Event == "tail return" then
        RetType = "tail-returning from "
      end
      table.insert(traceData, Indent(Depth) .. RetType .. Running .. " to " .. Caller .. "\n")
      Depth = Depth - 1
    end
  end
end



function KanevaDebugger:startHook()
  debug.sethook(HooksDoFile, "c")
end

function KanevaDebugger:stopHook()
  debug.sethook()
end

function KanevaDebugger:getHookData()
  local data  = KDTraceData
  return data
end

function KanevaDebugger:logHookData()
  assert(self.loggerFunc)
end


-- GLOBAL Functions --
function getStackInfo(event)

  local retval = {srcFile = "", funcName = "", line = -1}

  local sInfo = debug.getinfo(event, "Snl")
  if (sInfo) then
    retval.srcFile  = sInfo.source
    retval.funcName = sInfo.name
    retval.line     = sInfo.currentline
  end

  return retval
end



-- GLOBAL HOOKS  --
KDTraceData = {
  callSequence   = 0,
  tracedKey      = "",
  traceData      = {}
}

local lua_DoFile = dofile

function HooksDoFile(filename)

  dofile = function(filename)
    -- start hook specific
    table.insert(KDTraceData, {
        callSequence = #KDTraceData+1,
        tracedKey    = fileName,
        traceData    = getStackInfo(3)
      }
    )
    -- end hook specific

    -- call original function
    lua_DoFile(filename)
  end
end

