


-- reads luac listings and reports global variable usage
-- lines where a global is written to are marked with "*"
-- typical usage: luac -p -l file.lua | lua globals.lua | sort | lua table.lua
function XXX:globals()
	retval = {}
	 local ok,_,l,op,g=string.find(s,"%[%-?(%d*)%]%s*([GS])ETGLOBAL.-;%s+(.*)$")
	 if ok then
		if op=="S" then op="*" else op="" end
		table.insert(retval, g.."\t"..l..op.."\n")
	end
end




-- recursivly dump table
function Proxy_TraceDump(aVar, indent)
	indent = indent or 0
	if (type(aVar) ~= "table") then
		KEP_Log(string.rep(" ", indent).." Value["..tostring(aVar).."]")
	else
		for k, v in pairs(aVar) do
			if type(v) == "table" then
				KEP_Log(string.rep(" ", indent).." Key["..tostring(k).."]")
				Proxy_TraceDump(v, indent + 2)
			else
				KEP_Log(string.rep(" ", indent).." Key["..tostring(k).."], Value["..tostring(v).."]")
			end
		end
	end
end
