--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--colors for chat
--format hex number for alpha, red, green, blue

chatcolor         = {}
chatcolor.talk    = "FFFFFFFF"
chatcolor.clan    = "FF00FF00"
chatcolor.group   = "FF00FF00"
chatcolor.whisper = "C0C0C0C0"
chatcolor.shout   = "FFFF0000"
chatcolor.system  = "FFFFFF00"
chatcolor.private = "FF00FFFF"
chatcolor.emote   = "FF3DF500"