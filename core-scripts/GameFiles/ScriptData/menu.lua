--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Menu.lua - Kaneva Menu Script Template
-- 
-- Template script for creating a new Kaneva client menu.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\MenuHelper.lua")

-- InitializeKEPEvents - Register all script events here. 
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	-- TODO - MyMenuEvent Registration Example
	KEP_EventRegister( "MyMenuEvent", KEP.MED_PRIO )
end

-- InitializeKEPEventHandlers - Register all script event handlers here. 
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	-- TODO - MyMenuEvent Handler Registration Example
	KEP_EventRegisterHandler( "MyMenuEventHandler", "MyMenuEvent", KEP.MED_PRIO ) 
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) -- REQUIRED - DO NOT REMOVE
	
	-- TODO - Encode MyMenuEvent Example
	event = KEP_EventCreate("MyMenuEvent")
	KEP_EventEncodeString(event, "MyEventData")
	KEP_EventQueue(event)
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle ) -- REQUIRED - DO NOT REMOVE
end

-- Dialog_OnRender - Called upon screen render. Animate your script here.
function Dialog_OnRender(dialogHandle, elapsedSec)
end

-- MyMenuEventHandler - Called upon receipt of MyMenuEvent. Handle your event here.
function MyMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- TODO - Decode MyMenuEvent Example
	local myEventData = KEP_EventDecodeString(event)
	KEP_Log("MyEventData = "..myEventData)
end