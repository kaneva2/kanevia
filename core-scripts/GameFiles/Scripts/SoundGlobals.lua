--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--Sound indices as of August 05, 2008--
SOUND_HORNET 		        = 0
SOUND_SPLAT 		        = 1
SOUND_WATER 		        = 2
SOUND_CHEERING 		        = 3
SOUND_MISS 			        = 4
SOUND_PERFECT 		        = 5
SOUND_LEVELUP 		        = 6
SOUND_FINALCHEER 	        = 7
SOUND_ARROWCLICK 	        = 8
SOUND_COINPICKUP 	        = 9
SOUND_KACHING_COIN          = 10
SOUND_KACHING_DEATHFEM1     = 11
SOUND_KACHING_DEATHFEM2     = 12
SOUND_KACHING_DEATHMALE1    = 13
SOUND_KACHING_DEATHMALE2    = 14
SOUND_KACHING_DROPOFF       = 15
SOUND_KACHING_FIRE1         = 16
SOUND_KACHING_FIRE2         = 17
SOUND_KACHING_FIRE3         = 18
SOUND_KACHING_FIRE4         = 19
SOUND_KACHING_IMPACT1       = 20
SOUND_KACHING_IMPACT2       = 21
SOUND_KACHING_NOAMMO        = 22
SOUND_KACHING_PICKUP1       = 23
SOUND_KACHING_PICKUP2       = 24
SOUND_KACHING_PICKUP3       = 25
SOUND_KACHING_RELOAD1       = 26
SOUND_KACHING_RELOAD2       = 27
SOUND_KACHING_RELOAD3       = 28
SOUND_KCVO_CHECK            = 29
SOUND_KCVO_DENIED           = 30
SOUND_KCVO_GAINEDLEAD       = 31
SOUND_KCVO_GAMEOVER         = 32
SOUND_KCVO_KACHING          = 33
SOUND_KCVO_LOCKLOAD         = 34
SOUND_KCVO_ONEMINUTE        = 35
SOUND_KCVO_REDNEARVIC       = 36
SOUND_KCVO_RELOAD           = 37
SOUND_KCVO_TENSECONDS       = 38
SOUND_KCVO_GOTBOUNCED       = 39
SOUND_KCVO_BLUENEARVIC      = 40
SOUND_KACHING_JUMPPAD       = 41
SOUND_KCVO_REDSAVE          = 42
SOUND_KCVO_BLUESAVE         = 43
SOUND_KCVO_LOSTLEAD         = 44

-----------------------------------------------

SG = {}

SG.CLIP_RADIUS = "clipRadius"
SG.SOUND_FACTOR = "factor"
-- looping, 0 false, 1 true
SG.LOOPING = "looping"
--index is its list position in the editor
SG.INDEX = "index"

SG.ItemSounds = {}
sound = {}

-- Item Sounds --
-- These are indexed by the NPC id from the NPC configuration in the editor
-- Coins are NPC id 188-198 --
-- sound attributes
sound[SG.INDEX] = 9
sound[SG.LOOPING] = 0
sound[SG.SOUND_FACTOR] = 0.03
sound[SG.CLIP_RADIUS] = 50

SG.ItemSounds[188] = sound
SG.ItemSounds[189] = sound
SG.ItemSounds[190] = sound
SG.ItemSounds[191] = sound
SG.ItemSounds[192] = sound
SG.ItemSounds[193] = sound
SG.ItemSounds[194] = sound
SG.ItemSounds[195] = sound
SG.ItemSounds[196] = sound
SG.ItemSounds[197] = sound
SG.ItemSounds[198] = sound

-- End Coins --
