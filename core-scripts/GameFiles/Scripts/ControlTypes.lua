--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--Control Types
ControlTypes						= {}
ControlTypes.DXUT_CONTROL_BUTTON 						= 0
ControlTypes.DXUT_CONTROL_STATIC 						= 1
ControlTypes.DXUT_CONTROL_CHECKBOX 						= 2
ControlTypes.DXUT_CONTROL_RADIOBUTTON 					= 3
ControlTypes.DXUT_CONTROL_COMBOBOX 						= 4
ControlTypes.DXUT_CONTROL_SLIDER 						= 5
ControlTypes.DXUT_CONTROL_EDITBOX 						= 6
ControlTypes.DXUT_CONTROL_IMEEDITBOX 					= 7
ControlTypes.DXUT_CONTROL_LISTBOX 						= 8
ControlTypes.DXUT_CONTROL_SCROLLBAR 					= 9
ControlTypes.DXUT_CONTROL_IMAGE 						= 10
ControlTypes.DXUT_CONTROL_FLASH 						= 11
ControlTypes.DXUT_CONTROL_BROWSER 						= 12
ControlTypes.DXUT_CONTROL_COMMENT						= 13
ControlTypes.DXUT_CONTROL_SIMPLEEDITBOX 				= 14

ControlStates 								= {}
ControlStates.DXUT_STATE_NORMAL				= 0
ControlStates.DXUT_STATE_DISABLED			= 1
ControlStates.DXUT_STATE_HIDDEN				= 2
ControlStates.DXUT_STATE_FOCUS				= 3
ControlStates.DXUT_STATE_MOUSEOVER			= 4
ControlStates.DXUT_STATE_PRESSED			= 5