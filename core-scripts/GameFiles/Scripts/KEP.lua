--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- global variable and "constants"

-- Make sure one-time initialized fields will *NOT* be overwritten if KEP.lua is included again after KEP_PreInitialize is called
if type(KEP)=="table" and KEP.__initialized then
	-- KEP.lua already processed, return now
	return
end

KEP = {}

-- DRF - ADDED FOR KANEVIA
KEP.KANEVIA = true

-- event priority
KEP.LOW_PRIO = 10
KEP.MED_PRIO = 100
KEP.HIGH_PRIO = 200

-- event security
KEP.ESEC_LOCAL_ONLY         = 0 		-- 0x0000   -- never matches anything, so can be from network
KEP.ESEC_ALL                = 65535		-- 0xffff
KEP.ESEC_CLIENT             = 1			-- 0x0001   -- game client
KEP.ESEC_SERVER             = 2			-- 0x0002   -- game server
KEP.ESEC_AI                 = 4			-- 0x0004   -- ai server
KEP.ESEC_REMOTE_SERVER      = 8			-- 0x0008   -- a remote server, this allows

-- from log4cplus headers
KEP.OFF_LOG_LEVEL     = 60000
KEP.FATAL_LOG_LEVEL   = 50000
KEP.ERROR_LOG_LEVEL   = 40000
KEP.WARN_LOG_LEVEL    = 30000
KEP.INFO_LOG_LEVEL    = 20000
KEP.DEBUG_LOG_LEVEL   = 10000
KEP.TRACE_LOG_LEVEL   = 0

-- return codes for event handling
KEP.EPR_OK				= 0	-- didn't change event
KEP.EPR_NOT_PROCESSED 	= 1 -- I don't want this event
KEP.EPR_CONSUMED		= 2 -- consumed event, don't let others process it

-- for getting C functions into the script
KEP.GET_SERVER_FN 		= "ServerGameFunctions"
KEP.GET_CLIENT_FN 		= "ClientGameFunctions"
KEP.GET_XWIN_FN 		= "XWinFunctions"
KEP.GET_MENU_FN 		= "MenuFunctions"
KEP.GET_ARENA_FN		= "ArenaFunctions"
KEP.GET_AI_FN           = "AIFunctions"
KEP.GET_PLAYER_FN		= "PlayerFunctions"
KEP.GET_SKILLDB_FN		= "SkillDBFunctions"
KEP.GET_KEPFILE_FN      = "KEPFileFunctions"
KEP.GET_KEPCONFIG_FN    = "KEPConfigFunctions"
KEP.GET_UGCIMPORT_FN	= "UGCImportFunctions"

-- instance types
KEP.CI_NOT_INSTANCED	= 0
KEP.CI_GROUP			= 1
KEP.CI_GUILD			= 2
KEP.CI_HOUSING   		= 3
KEP.CI_PERMANENT 		= 4
KEP.CI_ARENA			= 5
KEP.CI_CHANNEL			= 6

KEP.ArenaReady			= 0
KEP.ArenaWaiting 		= 1
KEP.ArenaTimedOut		= 2

KEP.INVALID_ZONE_INDEX  = 4293918720

-- from TextEvent.h
ChatType = {}
ChatType.Talk 			= 0	--
ChatType.Group 			= 1	--
ChatType.Clan 			= 2	--
ChatType.Private		= 3	-- just to you from one other
ChatType.Whisper 		= 4	-- talk w/ limited radius
ChatType.Shout 			= 5	-- talk w/ large radius
ChatType.GM 			= 6	-- from GameMaster
ChatType.System 		= 7	-- from System Admin
ChatType.External		= 8	-- from an external source
ChatType.ArenaMgr		= 9	-- related to an arena
ChatType.Team           = 10 -- restrict to same team
ChatType.Race           = 11 -- restrict to same race
ChatType.Battle         = 12 -- battle related, etc. damage
ChatType.S2S			= 15 -- server to server relay message
ChatType.Emote          = 16 -- /em command
ChatType.ScriptObject	= 17 -- from a script object

-- GetSet.AddNewMember types
KEP.amBool              = 1
KEP.amInt               = 8
KEP.amLong              = 10
KEP.amDouble            = 14
KEP.amString            = 15

-- add member flags
KEP.amfTransient            = 33554432  -- 0x02000000 -- this item is not serialized
KEP.amfReplicate            = 134217728 -- 0x08000000 -- transfer between server and client, for dynamic
KEP.amfSaveAcrossSpawn      = 268435456 -- 0x10000000 -- save across a spawn for dynamic player data
KEP.amfMask                 = 503316480 -- 0x1e000000

-- add member/find return values
KEP.amrDupeWrongType        = 4294967294 -- 0xfffffffe
KEP.amrBadType              = 4294967293 -- 0xfffffffd
KEP.amrNotFound             = 4294967295 -- 0xffffffff

-- type of damage
KEP.MISSILE				= 0
KEP.MELEE				= 1
KEP.EXPLOSION 			= 2

-- Network ID of the Server
KEP.SERVER_NETID        = 1

--triggers
KEP.TriggerEnter        = 1
KEP.TriggerExit         = 2

-- for registering filtered events
KEP.FILTER_IS_MASK      	= 1
KEP.FILTER_MATCH        	= 0
KEP.NO_FILTER           	= 0
KEP.NO_OBJID_FILTER     	= 0

-- for GotoPlaceEvent
KEP.GOTOPLACE_EVENT_NAME   = "GotoPlaceEvent"
KEP.GOTO_ZONE				= 1
KEP.GOTO_PLAYER				= 2
KEP.GOTO_PLAYERS_APT		= 3
KEP.GOTO_ZONE_EX			= 4
KEP.GOTO_TUTORIAL           = 5
KEP.GOTO_SERVER             = 6
KEP.GOTO_GAME               = 7
KEP.GOTO_URL                = 8


-- for pending spawn event
KEP.PENDINGSPAWN_EVENT_NAME	= "PendingSpawnEvent"

-- For keyboard input
Keyboard = {
	TAB = 9,
	ENTER = 13,
	SHIFT = 16,
	CTRL = 17,
	ALT = 18,
	ESC	= 27,
	SPACEBAR = 32,
	PAGE_UP	= 33,
	PAGE_DOWN = 34,
	END = 35,
	HOME = 36,
	LEFT = 37,
	UP = 38,
	RIGHT = 39,
	DOWN = 40,
	INSERT = 45,
	DEL = 46,
	NUM0 = 48, 
	NUM1 = 49, 
	NUM2 = 50, 
	NUM3 = 51, 
	NUM4 = 52, 
	NUM5 = 53, 
	NUM6 = 54, 
	NUM7 = 55, 
	NUM8 = 56, 
	NUM9 = 57,
	A = 65, 
	B = 66, 
	C = 67, 
	D = 68, 
	E = 69, 
	F = 70, 
	G = 71, 
	H = 72, 
	I = 73, 
	J = 74, 
	K = 75, 
	L = 76, 
	M = 77, 
	N = 78, 
	O = 79, 
	P = 80, 
	Q = 81, 
	R = 82, 
	S = 83, 
	T = 84, 
	U = 85, 
	V = 86, 
	W = 87, 
	X = 88, 
	Y = 89, 
	Z = 90,
	F1 = 112,
	F2 = 113,
	F3 = 114,
	F4 = 115,
	F11	= 122,
	F12 = 123,
	SEMICOLON = 186,
	LEFT_BRACKET = 219,
	RIGHT_BRACKET = 221,
}

-- for KDP
-- The world object id for the dance floor. This ID is the first kaneva dance floor
KEP.KDP1ID = 369
KEP.KDP2ID = 370
KEP.KDP3ID = 371
KEP.LARGE_DISCO_FLOOR = 883
KEP.MEDIUM_DISCO_FLOOR = 1101
KEP.SMALL_DISCO_FLOOR = 1102
KEP.SNOWGLOBE_DISCO_FLOOR = 1321
KEP.ANIMATING_FLOOR_32 = 2474
KEP.ANIMATING_FLOOR_48 = 2473
KEP.ANIMATING_FLOOR_64 = 2475
KEP.KDP1_EVENT = "DDREvent"
KEP.KDP1_MENU_EVENT = "DDRMenuEvent"
KEP.FILTER_START_GAME = 5
KEP.FILTER_POSTED_SCORE = 4
KEP.FILTER_END_GAME = 6
KEP.FILTER_CLOSE_DIALOG = 7
KEP.FILTER_GAME_OVER_DIALOG = 8
KEP.FILTER_READY_TO_PLAY = 9
KEP.FILTER_POST_PLAYERS_SCORES = 10
KEP.FILTER_ACCESS_DENIED = 13
KEP.FILTER_PAUSE_REQUEST = 16
KEP.FILTER_PAUSE_RESPONSE = 17

-- YES/NO box answer filters
KEP.FILTER_KDP_ANSWER_PLAY = 5

-- World Fame Event and Filters
KEP.WORLD_FAME_EVENT = "WorldFameEvent"
KEP.FILTER_WF_TUTORIAL = 1
KEP.FILTER_WF_PLACEOBJ = 2
KEP.FILTER_WF_CLOTHES = 3
KEP.FILTER_WF_PATTERN = 4
KEP.FILTER_WF_MEDIA = 5
KEP.FILTER_WF_HANGOUT = 6
KEP.FILTER_WF_ZONE = 7
KEP.FILTER_WF_DANCELEVEL = 8
KEP.FILTER_WF_LOGIN = 9

-- TryOn Inventory Expire Event
KEP.FILTER_TRYONINVENTORY = 1
KEP.FILTER_TRYONINVENTORY_EXPIRED = 2

KEP.TRYON_INVENTORY_EXPIRE_EVENT = "TryOnInventoryExpire"
KEP.FILTER_TRYON_EXPIRE = 1

-- Generic Message Box
KEP.GENERIC_INFO_BOX_EVENT = "GenericInfoBoxEvent"
KEP.FILTER_GENERIC_INFO_BOX_MSG1 = 1;

-- use types for player use item event
KEP.USE_TYPE_NONE			= 0
KEP.USE_TYPE_HEALING		= 1
KEP.USE_TYPE_AMMO			= 2
KEP.USE_TYPE_SKILL			= 3
KEP.USE_TYPE_MENU			= 4
KEP.USE_TYPE_COMBINE		= 5
KEP.USE_TYPE_PLACE_HOUSE	= 6
KEP.USE_TYPE_INSTALL_HOUSE	= 7
KEP.USE_TYPE_SUMMON_CREATURE= 8
KEP.USE_TYPE_OLD_SCRIPT		= 9
KEP.USE_TYPE_ADD_DYN_OBJ	= 10
KEP.USE_TYPE_ADD_ATTACH_OBJ	= 11
KEP.USE_TYPE_MOUNT			= 12
KEP.USE_TYPE_EQUIP			= 13
KEP.USE_TYPE_ANIMATION      = 200
KEP.USE_TYPE_P2P_ANIMATION  = 201
KEP.USE_TYPE_NPC            = 202
KEP.USE_TYPE_DEED           = 203
KEP.USE_TYPE_QUEST          = 204
KEP.USE_TYPE_PREMIUM		= 205
KEP.USE_TYPE_PARTICLE		= 206
KEP.USE_TYPE_SOUND			= 207
KEP.USE_TYPE_BUNDLE			= 208
KEP.USE_TYPE_CUSTOM_DEED	= 209
KEP.USE_TYPE_ACTION_ITEM	= 210
KEP.USE_TYPE_GAME_ITEM		= 211

KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME = "ServerDynObjRequestEvent"
KEP.SERVERDYNOBJ_REPLY_EVENT_NAME = "ServerDynObjReplyEvent"

-- UGC GLID Constants
KEP.GLID_UGC_BASE = 3000000

-- ControlDynamicObjectEvent Filters
-- Copied From KEPConstants.h
KEP.ANIM_START = 1
KEP.ANIM_STOP = 2
KEP.FRICTION_ENABLE = 3
KEP.FRICTION_DISABLE = 4
KEP.OBJECT_SHOW = 8
KEP.OBJECT_HIDE = 9
KEP.SOUND_CONFIG = 16
KEP.SOUND_START = 17
KEP.SOUND_STOP = 18
KEP.PARTICLE_START = 19
KEP.PARTICLE_STOP = 20

-- External notification types (see NotifyExternalEventListener)
KEP.EN_GENERIC = 0
KEP.EN_PING = 1
KEP.EN_ACTIVATED = 2
KEP.EN_DEACTIVATED = 3
KEP.EN_FRIENDS = 4

-- Game Id Enumeration (development, preview, production, RC)
KEP.GID_DEV = 5316
KEP.GID_DEV_STR = "5316"
KEP.GID_PRE = 3298
KEP.GID_PRE_STR = "3298"
KEP.GID_PRO = 3296
KEP.GID_PRO_STR = "3296"
KEP.GID_RC = 5310
KEP.GID_RC_STR = "5310"

GetAttributeType_GET_INVENTORY_PLAYER = 50 -- stephane added

--[[ DRF - These functions log boilerplate calls]]
function KepLog(txt)
--	KEP_Log(KEP.scriptName.."::KEP::"..txt)
end
function KepLogError(txt)
	KEP_LogError(KEP.scriptName.."::KEP::"..txt)
end

--[[ DRF - This function gets the KEP.xxx globals required by everything else]]
KEP.dispatcher = 0
KEP.parent = 0
KEP.debugLevel = 0 -- script type (0.0=menu, 1.0=client)
KEP.scriptType = "nil"
KEP.scriptName = "<scriptName.lua>"
KEP.isClientScript = false
KEP.isMenuScript = false
KEP.game = 0
KEP.gs = 0
function KEP_GetGlobals(dispatcher, handler, debugLevel, scriptName)
	KEP.dispatcher = dispatcher
	KEP.parent = handler
	KEP.debugLevel = debugLevel
	KEP.scriptName = scriptName
    if (KEP.debugLevel == 0) then 
        KEP.scriptType = "MenuScript"
        KEP.isMenuScript = true
        KEP.isClientScript = false
    else
        KEP.scriptType = "ClientScript"
        KEP.isMenuScript = false
        KEP.isClientScript = true
    end
	if (KEP.game == 0) then 
        KEP.game = Dispatcher_GetGame(dispatcher) or 0 
        if (KEP.game == 0) then 
            KepLogError("KEP_GetGlobals(): ERROR Getting Game (KEP.game)")
        end
    end
	if (KEP.gs == 0) then 
        KEP.gs = Dispatcher_GetGameAsGetSet(dispatcher) or 0
        if (KEP.gs == 0) then 
            KepLogError("KEP_GetGlobals(): ERROR Getting Game As GetSet (KEP.gs)")
        end
    end
end

--[[ DRF - This function gets all platform glue functions.]]
KEP.xwinFnOk = 0
KEP.menuFnOk = 0
KEP.ugcFnOk = 0
KEP.configFnOk = 0
KEP.fileFnOk = 0
function KEP_GetFunctions()
	if (KEP_SetInstance == nil) then
		KEP.xwinFnOk = Dispatcher_GetFunctions(KEP.dispatcher, KEP.GET_XWIN_FN )
        if (KEP_SetInstance ~= nil and KEP.game ~= 0) then KEP_SetInstance(KEP.game, KEP.gs, KEP.dispatcher) end
	end
	if (Dialog_CreateAtCoordinates == nil) then
		KEP.menuFnOk = Dispatcher_GetFunctions(KEP.dispatcher, KEP.GET_MENU_FN )
	end
	if (UGC_ClassifyImportType== nil) then
		KEP.ugcFnOk = Dispatcher_GetFunctions(KEP.dispatcher, KEP.GET_UGCIMPORT_FN )
	end
	if (KEPConfig_GetString == nil) then
		KEP.configFnOk = Dispatcher_GetFunctions(KEP.dispatcher, KEP.GET_KEPCONFIG_FN )
	end
	if (KEPFile_ReadLine == nil) then
		KEP.fileFnOk = Dispatcher_GetFunctions(KEP.dispatcher, KEP.GET_KEPFILE_FN )
	end
end

--[[ DRF - This function is called 1st during script loading. It sets all globals.]]
function KEP_PreInitialize(dispatcher, handler, debugLevel, scriptName)
    KEP_GetGlobals(dispatcher, handler, debugLevel, scriptName)
    KepLog("KEP_PreInitialize()")
    if (KEP.isMenuScript) then KEP_GetFunctions() end -- functions exist for menu scripts pre initialize
end

--[[ DRF - This function is called 2nd during script loading.]]
function KEP_InitializeEvents(dispatcher, handler, debugLevel, scriptName)
    KepLog("KEP_InitializeEvents()")
end

--[[ DRF - This function is called 3rd during script loading.]]
function KEP_InitializeEventHandlers(dispatcher, handler, debugLevel, scriptName)
    KepLog("KEP_InitializeEventHandlers()")
end

--[[ DRF - This function is called 4th during script loading. It gets all platform glue functions.]]
function KEP_PostInitialize(dispatcher, handler, debugLevel, scriptName)
    KepLog("KEP_PostInitialize()")
    if (KEP.isClientScript) then KEP_GetFunctions() end -- functions don't exist for client scripts until post initialize
end

--[[ DRF - Runtime Environment API ]]
function KEP_IsPro() return (KEP_GetStarId() == KEP.GID_PRO_STR) end
function KEP_IsPre() return (KEP_GetStarId() == KEP.GID_PRE_STR) end
function KEP_IsDev() return (KEP_GetStarId() == KEP.GID_DEV_STR) end
function KEP_IsRC() return (KEP_GetStarId() == KEP.GID_RC_STR) end

--[[ DRF - Old Disabled Functions -> KEP API]]
function KEP_GetUsername() return "" end

-- DRF - Asynchronous single thread lowest priority no response html (fire & forget) webcallers
function KEP_WebCall_Script(a) return KEP_WebCall(a, "Script") end

-- DRF - Synchronous single thread lowest priority no response html webcaller returns URL_OK
function KEP_WebCallAndWait_Script(a) return KEP_WebCallAndWait(a, "Script") end

--[[ DRF - UGC Glue -> KEP API]]
-- No Namespace Changes (internal functions)

--[[ DRF - KEP -> Misc / DispatcherGlue API]]
function KEP_CurrentTime() return Dispatcher_CurrentTime() end
function KEP_GetCurrentEpochTime() return Dispatcher_GetCurrentEpochTime() end -- unused
function KEP_GetCurrentTimeStamp() return Dispatcher_GetCurrentTimeStamp() end -- unused
function KEP_EpochTimeToTimeStamp(a) return Dispatcher_EpochTimeToTimeStamp(a) end -- unused
function KEP_DateToEpochTime(a, b, c, d, e, f, g) return Dispatcher_DateToEpochTime(a, b, c, d, e, f, g) end -- unused
function KEP_GetCurrentThreadId() return Dispatcher_GetCurrentThreadId() end -- unused
function KEP_GetGame() return KEP.game end
function KEP_GetGameAsGetSet() return KEP.gs end

--[[ DRF - KEP -> Log / DispatcherGlue API]]
function KEP_Log(a) return Dispatcher_LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, a) end
function KEP_LogInfo(a) return Dispatcher_LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, a) end
function KEP_LogTrace(a) return Dispatcher_LogMessage(KEP.dispatcher, KEP.TRACE_LOG_LEVEL, a) end
function KEP_LogDebug(a) return Dispatcher_LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, a) end
function KEP_LogWarn(a) return Dispatcher_LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, a) end
function KEP_LogError(a) return Dispatcher_LogMessage(KEP.dispatcher, KEP.ERROR_LOG_LEVEL, a) end
function KEP_LogMessage(a, b) return Dispatcher_LogMessage(KEP.dispatcher, a, b) end
function KEP_LogAlertMessage(a) return Dispatcher_LogAlertMessage(KEP.dispatcher, a) end

--[[ DRF - KEP -> Event / DispatcherGlue API]]
function KEP_EventCreate(a) return Dispatcher_MakeEvent(KEP.dispatcher, a) end
function KEP_EventCancel(a) return Dispatcher_CancelEvent(KEP.dispatcher, a) end
function KEP_EventGetId(a) return Dispatcher_GetEventId(KEP.dispatcher, a) end
function KEP_EventQueue(a) return Dispatcher_QueueEvent(KEP.dispatcher, a) end
function KEP_EventQueueInFuture(a, b) return Dispatcher_QueueEventInFuture(KEP.dispatcher, a, b) end
function KEP_EventQueueInFutureMS(a, b) return Dispatcher_QueueEventInFutureMS(KEP.dispatcher, a, b) end
function KEP_EventRegister(a, b) return Dispatcher_RegisterEvent(KEP.dispatcher, 1, 0, 0, 0, a, b) end
function KEP_EventRegisterV(a, b) return Dispatcher_RegisterEvent(KEP.dispatcher, 1, 1, 0, 0, a, b) end
function KEP_EventRegisterSecure(a, b, c, d) return Dispatcher_RegisterSecureEvent(KEP.dispatcher, 1, 0, 0, 0, a, b, c, d) end
function KEP_EventRegisterSecureV(a, b, c, d) return Dispatcher_RegisterSecureEvent(KEP.dispatcher, 1, 1, 0, 0, a, b, c, d) end
function KEP_EventReschedule(a, b) return Dispatcher_RescheduleEvent(KEP.dispatcher, a, b) end

--[[ DRF - KEP -> New Event Helpers API]]
function KEP_EventCreateAndQueue(a) return KEP_EventQueue(KEP_EventCreate(a)) end
function KEP_EventCreateAndQueueInFuture(a, b) return KEP_EventQueueInFuture(KEP_EventCreate(a), b) end
function KEP_EventCreateAndQueueInFutureMS(a, b) return KEP_EventQueueInFutureMS(KEP_EventCreate(a), b) end

--[[ DRF - KEP -> EventGlue API]]
function KEP_EventAddTo(a, b) return Event_AddTo(a, b) end
function KEP_EventAddToServer(a) return Event_AddTo(a, KEP.SERVER_NETID) end
function KEP_EventSetFilter(a, b) return Event_SetFilter(a, b) end
function KEP_EventSetFlags(a, b) return Event_SetFlags(a, b) end
function KEP_EventSetObjectId(a, b) return Event_SetObjectId(a, b) end
function KEP_EventSetRecurrence(a, b) return Event_SetRecurrence(a, b) end
function KEP_EventGetRecurrence(a) return Event_GetRecurrence(a) end
function KEP_EventResetForOutput(a) return Event_ResetForOutput(a) end
function KEP_EventEncodeNumber(a, b) return Event_EncodeNumber(a, b) end
function KEP_EventEncodeString(a, b) return Event_EncodeString(a, b) end
function KEP_EventDecodeNumber(a) return Event_DecodeNumber(a) end
function KEP_EventDecodeString(a) return Event_DecodeString(a) end
function KEP_EventDecodeObject(a) return Event_DecodeObject(a) end
function KEP_EventMoreToDecode(a) return Event_MoreToDecode(a) end

--[[ DRF - KEP -> Event / ParentHandlerGlue API]]
function KEP_EventRegisterHandler(a, b, c) return ParentHandler_RegisterEventHandler(KEP.dispatcher, KEP.parent, a, 1, 0, 0, 0, b, c) end
function KEP_EventRegisterHandlerV(a, b, c) return ParentHandler_RegisterEventHandler(KEP.dispatcher, KEP.parent, a, 1, 1, 0, 0, b, c) end
function KEP_EventRegisterHandlerFiltered(a, b, c, d, e, f) return ParentHandler_RegisterFilteredEventHandler(KEP.dispatcher, KEP.parent, a, 1, 0, 0, 0, b, c, d, e, f) end
function KEP_EventRegisterHandlerFilteredV(a, b, c, d, e, f) return ParentHandler_RegisterFilteredEventHandler(KEP.dispatcher, KEP.parent, a, 1, 1, 0, 0, b, c, d, e, f) end
function KEP_EventRemoveHandler(a, b) return ParentHandler_RemoveEventHandler(KEP.dispatcher, KEP.parent, a, b) end
function KEP_GetVMFileName() return ParentHandler_GetVMFileName(KEP.parent) end

--[[ DRF - KEP -> ClientEngine GetSetGlue API]]
dofile("..\\Scripts\\ClientEngineIds.lua")
function KEP_GetLoginName() return GetSet_GetString(KEP.gs, ClientEngineIds.LOGINNAME) end
function KEP_GetLoginGender() return GetSet_GetString(KEP.gs, ClientEngineIds.LOGINGENDER) end
function KEP_IsMale() return (KEP_GetLoginGender() == "M") end
function KEP_IsFemale() return (KEP_GetLoginGender() == "F") end
function KEP_GetParentGameId() return GetSet_GetNumber(KEP.gs, ClientEngineIds.PARENTGAMEID) end
function KEP_IsWorld() return (KEP_GetParentGameId() ~= 0) end
function KEP_IsCommunity() return (KEP_GetParentGameId() == 0) end
function KEP_GetGameId() return GetSet_GetNumber(KEP.gs, ClientEngineIds.GAMEID) end
function KEP_GetGameName() return GetSet_GetString(KEP.gs, ClientEngineIds.GAMENAME) end
function KEP_GetABGroupList() return GetSet_GetString(KEP.gs, ClientEngineIds.ABGROUPLIST) end
function KEP_SetABGroupList(a) return GetSet_SetString(KEP.gs, ClientEngineIds.ABGROUPLIST, a) end
function KEP_GetCurrentWorkingZoneIndex() return KEP_GetZoneIndex() end -- superceded
function KEP_GetCurrentZoneIndex() return KEP_GetZoneIndex() end -- superceded
function KEP_GetCurrentWorkingFile() return KEP_GetZoneFileName() end -- superceded
function KEP_GetCurrentWorkingZoneName() return KEP_GetZoneFileName() end -- superceded
function KEP_GetCurrentZoneName() return KEP_GetZoneFileName() end -- superceded
function KEP_GetZonePermissions() return GetSet_GetNumber(KEP.gs, ClientEngineIds.ZONEPERMISSIONS) end
function KEP_IsOwner() return (KEP_GetZonePermissions() <= 1) end
function KEP_IsModerator() return (KEP_GetZonePermissions() == 2) end
function KEP_IsOwnerOrModerator() return (KEP_GetZonePermissions() <= 2) end
function KEP_bulkZoneOpsEnabled() return ((KEP_GetZonePermissions() <= 1)) end
function KEP_GetZoneInstanceId() return GetSet_GetNumber(KEP.gs, ClientEngineIds.ZONEINSTANCEID) end
function KEP_GetUrlParameters() return GetSet_GetString(KEP.gs, ClientEngineIds.URLPARAMETERS) end
function KEP_SetUrlParameters(a) return GetSet_SetString(KEP.gs, ClientEngineIds.URLPARAMETERS, a) end
function KEP_SetTryOnState(a) return GetSet_SetString(KEP.gs, ClientEngineIds.INTRYONSTATE, a) end
function KEP_SetTryOnObjPlacementId(a) return GetSet_SetNumber(KEP.gs, ClientEngineIds.TRYONOBJPLACEMENTID, a) end
function KEP_SetTryOnOnStartup(a) return GetSet_SetNumber(KEP.gs, ClientEngineIds.TRYONONSTARTUP, a) end
function KEP_GetCurrentWorldURL() return GetSet_GetString(KEP.gs, GetSet_FindIdByName(KEP.gs, "CurrentWorldURL")) end

--[[ DRF - KEP -> File / KEPFileGlue / ClientEngineGlue API]]
function ClientEngine_OpenFile(x, a, b, c) return KEP_FileOpen(a, b, c) end
function KEP_FileOpenConfig(a, b) return KEP_FileOpen(a, b, "KanevaConfig") end
function KEP_FileReadLine(a) return KEPFile_ReadLine(a) end
function KEP_FileWriteLine(a, b) return KEPFile_WriteLine(a, b) end
function KEP_FileClose(a) return KEPFile_CloseAndDelete(a) end

--[[ DRF - KEP -> Config / KEPConfigGlue / ClientEngineGlue API]]
function ClientEngine_OpenConfig(x, a, b) return KEP_ConfigOpen(a, b) end
function KEP_ConfigOpenWOK() return KEP_ConfigOpen("WOKConfig.xml","WOKConfig") end
function KEP_ConfigOpenBuild() return KEP_ConfigOpen("BuildConfig.xml","BuildConfig") end
function KEP_ConfigGetString(a, b, c) return KEPConfig_GetString(a, b, c) end
function KEP_ConfigGetNumber(a, b, c) return KEPConfig_GetNumber(a, b, c) end
function KEP_ConfigSetString(a, b, c) return KEPConfig_SetString(a, b, c) end
function KEP_ConfigSetNumber(a, b, c) return KEPConfig_SetNumber(a, b, c) end
function KEP_ConfigValueExists(a, b) return KEPConfig_SetNumber(a, b) end
function KEP_ConfigSave(a) return KEPConfig_Save(a) end

--[[ DRF - Deprecated Common Functions -> KEP API]]
function showYesNoBox(x, ...) return KEP_YesNoBox(...) end
function logThis(a) return KEP_Log(a) end
function getGameName() return KEP_GetGameName() end
function getParentGame() return KEP_GetParentGameId() end
function isIn3DApp() return KEP_IsWorld() end
function LoadIconTextureByID(x, a, b, c) return KEP_LoadIconTextureByID(a, b, c) end
function showMessageBox(x, ...) return KEP_MessageBox(...) end
 
--[[ DRF - Deprecated GetSet_Safe Functions -> Now Safe GetSet Functions]]
function GetSet_Safe_FindIdByName(...) return GetSet_FindIdByName(...) end
function GetSet_Safe_GetArrayCount(...) return GetSet_GetArrayCount(...) end
function GetSet_AddNewNumericMember(...) return GetSet_AddNewNumber(...) end
function GetSet_Safe_AddNewNumericMember(...) return GetSet_AddNewNumber(...) end
function GetSet_Safe_GetNumber(...) return GetSet_GetNumber(...) end
function GetSet_Safe_SetNumber(...) return GetSet_SetNumber(...) end
function GetSet_Safe_GetNumberByName(...) return GetSet_GetNumberByName(...) end
function GetSet_Safe_SetNumberByName(...) return GetSet_SetNumberByName(...) end
function GetSet_Safe_GetNumberInArray(...) return GetSet_GetNumberInArray(...) end
function GetSet_AddNewStringMember(...) return GetSet_AddNewString(...) end
function GetSet_Safe_AddNewStringMember(...) return GetSet_AddNewString(...) end
function GetSet_Safe_GetString(...) return GetSet_GetString(...) end
function GetSet_Safe_SetString(...) return GetSet_SetString(...) end
function GetSet_Safe_GetVector(...) return GetSet_GetVector(...) end
function GetSet_Safe_SetVector(...) return GetSet_SetVector(...) end
function GetSet_Safe_GetColor(...) return GetSet_GetColor(...) end
function GetSet_Safe_SetColor(...) return GetSet_SetColor(...) end
function GetSet_Safe_GetObject(...) return GetSet_GetObject(...) end
function GetSet_Safe_GetObjectInArray(...) return GetSet_GetObjectInArray(...) end

-- Return a string indicate caller's source information: FileName.lua(LineNumber). LineNumber might not always be available.
-- Argument `where' indicates the number of step to travel up in the call stack. Default is 0.
local function LUA_SourceInfo(where)
	-- Catch-all: in case debug info is not available, return the file name of the main script.
	local caller = tostring(__SCRIPT__)

	-- Use debug API for more information
	if type(debug)=="table" and type(debug.getinfo)=="function" then
		local dbg = debug.getinfo(2 + (where or 0), "Sl")
		-- file name available through debug info?
		caller = dbg.source and string.match(dbg.source, "@.-([^\\]+)$") or caller
		if dbg.currentline~=nil and dbg.currentline~=-1 then
			-- line number is available
			caller = caller .. "(" .. tostring(dbg.currentline) .. ")"
		end
	end

	return caller
end

-- Define a wrapper for a deprecated Lua function
local function LUA_DEPRECATED(packageName, functionName, functionImpl, suggestion)
	local package = _G
	local fullName
	if packageName~=nil then
		package = _G[packageName]
		if type(package)~="table" then
			error("package " .. packageName .. " is not defined", 2)
		end
		fullName = packageName .. "." .. functionName
	else
		fullName = functionName
	end

	if package[functionName]~=nil then
		error(fullName .. " is already defined as a " .. type(package[functionName]), 2)
	end

	package[functionName] = function(...) 
		-- Log deprecation warnings
		KEP_LogWarn(LUA_SourceInfo(1) .. ": " .. fullName .. "() is deprecated. " .. (suggestion or ""))

		-- call implementation
		return functionImpl(...)
	end
end

-- Lua Compatibility
if not __LUA_COMPAT_DONE__ then

	-- Pre-5.1 Compatibility
	if __LUA_VERSION__>=501 then
		-- Remove the deprecated pre-5.1 functions if still provided by Lua (built with compat flag e.g.)
		table.getn = nil
		table.setn = nil
		math.mod = nil
		string.gfind = nil
		table.foreach = nil
		table.foreachi = nil
		loadlib = nil
		
		if __LUA_COMPAT_LEVEL__<501 then
			-- Replace them with wrappers that generate deprecation warnings
			LUA_DEPRECATED("table", "getn", function(tbl) return #tbl end, "Use # operator instead")
			LUA_DEPRECATED("table", "setn", 
				function(tbl, n) 
					-- Shrink table by remove records with index > n. No expansions. 
					for i=n+1,#tbl do
						tbl[i] = nil
					end
				end, "It should no longer be used")
			LUA_DEPRECATED("math", "mod", math.fmod, "Use math.fmod() instead")
			LUA_DEPRECATED("string", "gfind", string.gmatch, "Use string.gmatch() instead")
			LUA_DEPRECATED("table", "foreach", 
				function(table, f) 
					for k, v in pairs(table) do
						f(k,v)
					end
				end, "Use pairs() instead")
			LUA_DEPRECATED("table", "foreachi", 
				function(table, f) 
					for k, v in ipairs(table) do
						f(k,v)
					end
				end, "Use ipairs() instead")
		end
	end

	-- Pre-5.2 Compatibility
	if __LUA_VERSION__>=502 then
		-- Remove the deprecated pre-5.2 functions if still provided by Lua (built with compat flag e.g.)
		math.log10 = nil
		table.maxn = nil
		loadstring = nil
		unpack = nil

		if __LUA_COMPAT_LEVEL__<502 then
			-- Replace them with wrappers that generate deprecation warnings
			LUA_DEPRECATED("math", "log10", function(x) return math.log(x, 10) end, "Use math.log() instead")
			LUA_DEPRECATED("table", "maxn", function(tbl) return #tbl end, "It should no longer be used")
			LUA_DEPRECATED(nil, "loadstring", function(str, chunkname) return load(str, chunkname) end, "Use load() instead")
			LUA_DEPRECATED(nil, "unpack", function(tbl) return table.unpack(tbl) end, "Use table.unpack() instead")
		end
	end

	-- Only do this once
	__LUA_COMPAT_DONE__ = true
end

KEP.__initialized = true