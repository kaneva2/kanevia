#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#


# from ChatType.h
Talk            = 0 #
Group           = 1 #
Clan            = 2 #
Private         = 3 # just to you from one other
Whisper         = 4 # talk w/ limited radius
Shout           = 5 # talk w/ large radius
GM              = 6 # from GameMaster
System          = 7 # from System Admin
External        = 8 # from an external source
ArenaMgr        = 9
Team            = 10 # restrict to same team
Race            = 11 # restrict to same race
Battle          = 12 # battle related, etc. damage
S2S             = 15 # server to server relay message
Emote           = 16 # /em command for players


