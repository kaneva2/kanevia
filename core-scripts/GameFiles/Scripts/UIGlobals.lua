--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

ALIGN = {}

-- Using strings for readability in config files
ALIGN.TOP 		= "top"
ALIGN.BOTTOM 	= "bottom"
ALIGN.LEFT 		= "left"
ALIGN.RIGHT 	= "right"
ALIGN.CENTER 	= "center"