--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

PlaceCategoryIds                           = {}
PlaceCategoryIds.OTHER_HANGOUTS  		   = 0
PlaceCategoryIds.ARCADE_HANGOUTS 	       = 1
PlaceCategoryIds.ARTGALLERY_HANGOUTS  	   = 2
PlaceCategoryIds.CASINO_HANGOUTS  		   = 3
PlaceCategoryIds.DANCECLUB_HANGOUTS  	   = 4
PlaceCategoryIds.FESTIVAL_HANGOUTS  	   = 5
PlaceCategoryIds.GAMELOUNGE_HANGOUTS  	   = 6
PlaceCategoryIds.MUSEUM_HANGOUTS  		   = 7
PlaceCategoryIds.MUSICVENUE_HANGOUTS  	   = 8
PlaceCategoryIds.PLACEOFWORSHIP_HANGOUTS   = 9
PlaceCategoryIds.SPORTSBAR_HANGOUTS        = 10
PlaceCategoryIds.STORE_HANGOUTS  		   = 11
PlaceCategoryIds.THEATER_HANGOUTS  		   = 12
PlaceCategoryIds.THEMEPARK_HANGOUTS  	   = 13
PlaceCategoryIds.WEDDINGCHAPEL_HANGOUTS    = 14
PlaceCategoryIds.NONE					   = 15
PlaceCategoryIds.HOMES					   = 16
PlaceCategoryIds.HANGOUTS                  = 17
PlaceCategoryIds.MY_COMMUNITIES            = 18
PlaceCategoryIds.PUBLIC_PLACES			   = 19
PlaceCategoryIds.APPS			   		   = 20