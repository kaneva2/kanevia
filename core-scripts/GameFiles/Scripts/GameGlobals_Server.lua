--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

GameGlobals = {}

--actors
GameGlobals.EDB_MALE          = 3
GameGlobals.EDB_FEMALE        = 4

--zones
GameGlobals.ZONE_MALL       = 0
GameGlobals.ZONE_LOGINMENU  = 1
GameGlobals.ZONE_CASINO     = 2
GameGlobals.ZONE_THEATRE    = 3
GameGlobals.ZONE_APT_01     = 4  --1bdr_big
GameGlobals.ZONE_APT_02     = 5  --studio
GameGlobals.ZONE_APT_03     = 6  --1bdr_mid
GameGlobals.ZONE_APT_LOFT   = 7  --loft apt
GameGlobals.ZONE_APT_04     = 8  --L shaped
GameGlobals.ZONE_DANCEBAR   = 9  --dance pub
GameGlobals.ZONE_PENTHOUSE  = 10 --penthouse
GameGlobals.ZONE_HANGAR     = 11
GameGlobals.ZONE_NIGHTCLUB  = 12
GameGlobals.ZONE_CONFERENCE = 13
GameGlobals.ZONE_BBTHEATER  = 14
GameGlobals.ZONE_CAFE_SMALL = 15
GameGlobals.ZONE_CAFE_LARGE = 16 --kaneva cafe
GameGlobals.ZONE_APT_32x64x64   = 19
GameGlobals.ZONE_GREEN_ACRES    = 20
GameGlobals.ZONE_APT_MANOR_HOME = 21
GameGlobals.ZONE_APT_CITY_LOFT  = 32
GameGlobals.ZONE_PREM_ISLAND    = 36
GameGlobals.ZONE_ISLAND         = 37
GameGlobals.ZONE_ARENALOBBY   = 38
GameGlobals.ZONE_CITY_BLOCK     = 43
GameGlobals.ZONE_FASHIONISTA_FLAT       = 44
GameGlobals.ZONE_TRADITIONAL_LIVING     = 45
GameGlobals.ZONE_CONTEMPORARY_CONDO     = 46
GameGlobals.ZONE_GAMERS_DEN             = 47
GameGlobals.ZONE_SPORTS_ZONE            = 48
GameGlobals.ZONE_OUTDOOR_RETREAT        = 50
GameGlobals.ZONE_PREM_OUTDOOR_RETREAT   = 51
GameGlobals.ZONE_BEACH_HOUSE            = 52
GameGlobals.ZONE_INDEX_RESIDENCE    = 0x30000000
GameGlobals.ZONE_INDEX_MALL         = 0x40000000
GameGlobals.ZONE_INDEX_DANCE        = 0x40000023
GameGlobals.ZONE_INDEX_CITY         = 0x40000021
GameGlobals.ZONE_INDEX_PLAZA        = 0x4000001C
GameGlobals.ZONE_INDEX_ARENALOBBY   = 0x40000026
GameGlobals.ZONE_INDEX_EMPLOYMENT   = 0x40000029
GameGlobals.ZONE_INDEX_HANGOUT      = 0x60000000

--use values for apt upgrade
GameGlobals.UV_PORTAL_CASINO    = 100
GameGlobals.UV_PORTAL_THEATRE   = 101
GameGlobals.UV_PORTAL_PENTHOUSE = 102
GameGlobals.UV_APT_STUDIO       = 103
GameGlobals.UV_APT_1BDR         = 104
GameGlobals.UV_APT_FLAT         = 105
GameGlobals.UV_APT_LARGE1BDR    = 106
GameGlobals.UV_APT_LOFT         = 107
GameGlobals.UV_APT_PENTHOUSE    = 108
GameGlobals.UV_APT_32x64x64     = 109
GameGlobals.UV_APT_MANOR_HOME   = 110
GameGlobals.UV_APT_GREEN_ACRES  = 111
GameGlobals.UV_APT_CITY_LOFT    = 112
GameGlobals.UV_APT_ISLAND       = 113
GameGlobals.UV_APT_PREM_ISLAND  = 114
GameGlobals.UV_APT_FASHIONISTA_FLAT      = 117
GameGlobals.UV_APT_TRADITIONAL_LIVING    = 118
GameGlobals.UV_APT_CONTEMPORARY_CONDO    = 119
GameGlobals.UV_APT_GAMERS_DEN            = 120
GameGlobals.UV_APT_SPORTS_ZONE           = 121
GameGlobals.UV_APT_OUTDOOR_RETREAT       = 115
GameGlobals.UV_APT_PREM_OUTDOOR_RETREAT  = 116
GameGlobals.UV_APT_BEACH_HOUSE           = 122

--use values for broadband upgrade
GameGlobals.UV_BB_CASINO        = 200
GameGlobals.UV_BB_THEATRE       = 201
GameGlobals.UV_BB_DANCEBAR      = 202
GameGlobals.UV_BB_PENTHOUSE     = 203
GameGlobals.UV_BB_HANGAR        = 204
GameGlobals.UV_BB_NIGHTCLUB     = 205
GameGlobals.UV_BB_CONFERENCE    = 206
GameGlobals.UV_BB_BBTHEATER     = 207
GameGlobals.UV_BB_CAFE_SMALL    = 208
GameGlobals.UV_BB_CAFE_LARGE    = 209
GameGlobals.UV_BB_GREEN_ACRES   = 210
GameGlobals.UV_BB_ISLAND        = 211
GameGlobals.UV_BB_PREM_ISLAND   = 212
GameGlobals.UV_BB_DELUXE_COFFEE_HOUSE   = 213
GameGlobals.UV_BB_CITY_BLOCK            = 214
GameGlobals.UV_BB_OUTDOOR_RETREAT       = 215
GameGlobals.UV_BB_PREM_OUTDOOR_RETREAT  = 216
GameGlobals.UV_THANKSGIVING_PACK = 300
GameGlobals.UV_BB_BEACH_HOUSE    = 123

--items GLID
GameGlobals.PORTAL_CASINO     = 0
GameGlobals.PORTAL_THEATRE    = 1
GameGlobals.PORTAL_PENTHOUSE  = 2 

--apartments GLID
GameGlobals.ITEM_APT_STUDIO    = 533
GameGlobals.ITEM_APT_1BDR      = 534
GameGlobals.ITEM_APT_FLAT      = 535
GameGlobals.ITEM_APT_LARGE1BDR = 536
GameGlobals.ITEM_APT_LOFT      = 537
GameGlobals.ITEM_APT_PENTHOUSE = 538
GameGlobals.ITEM_APT_32x64x64  = 1055
GameGlobals.ITEM_APT_MANOR_HOME             = 1112 
GameGlobals.ITEM_APT_GREEN_ACRES            = 1636
GameGlobals.ITEM_APT_CITY_LOFT              = 2229
GameGlobals.ITEM_APT_ISLAND                 = 2876
GameGlobals.ITEM_APT_PREM_ISLAND            = 2878
GameGlobals.ITEM_APT_OUTDOOR_RETREAT        = 4554
GameGlobals.ITEM_APT_PREM_OUTDOOR_RETREAT   = 4555
GameGlobals.ITEM_APT_FASHIONISTA_FLAT       = 4647
GameGlobals.ITEM_APT_TRADITIONAL_LIVING     = 4648
GameGlobals.ITEM_APT_CONTEMPORARY_CONDO     = 4649
GameGlobals.ITEM_APT_GAMERS_DEN             = 4650
GameGlobals.ITEM_APT_SPORTS_ZONE            = 4651
GameGlobals.ITEM_APT_BEACH_HOUSE            = 4943

--bb deed item GLID
GameGlobals.ITEM_BB_THEATER    = 905
GameGlobals.ITEM_BB_DANCE_BAR  = 906
GameGlobals.ITEM_BB_NIGHT_CLUB = 907
GameGlobals.ITEM_BB_CONF_ROOM  = 908
GameGlobals.ITEM_BB_LG_THEATER = 909
GameGlobals.ITEM_BB_SM_CAFE    = 910
GameGlobals.ITEM_BB_LG_CAFE    = 911
GameGlobals.ITEM_BB_GREEN_ACRES             = 1638 
GameGlobals.ITEM_BB_ISLAND                  = 2875
GameGlobals.ITEM_BB_PREM_ISLAND             = 2877
GameGlobals.ITEM_BB_DELUXE_COFFEE_HOUSE     = 4372
GameGlobals.ITEM_BB_CITY_BLOCK              = 4373
GameGlobals.ITEM_BB_OUTDOOR_RETREAT         = 4556
GameGlobals.ITEM_BB_PREM_OUTDOOR_RETREAT    = 4557
GameGlobals.ITEM_BB_BEACH_HOUSE             = 4944