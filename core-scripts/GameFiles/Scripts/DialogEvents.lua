--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

DialogEvents = {}
-- from DialogEvents.h
DialogEvents.DIALOG_OPEN            = 1
DialogEvents.DIALOG_CLOSED          = 2
DialogEvents.DIALOG_BUTTON_CLICKED  = 3