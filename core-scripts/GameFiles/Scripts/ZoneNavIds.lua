--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

ZONE_NAV = {}

ZONE_NAV.HISTORY_REQUEST 		= 1
ZONE_NAV.HISTORY_INFO 			= 2
ZONE_NAV.FAVORITES_REQUEST 		= 3
ZONE_NAV.FAVORITES_INFO 		= 4
ZONE_NAV.ADD_FAVORITE 			= 5
ZONE_NAV.REMOVE_FAVORITE 		= 6
ZONE_NAV.CURRENT_URL_REQUEST 	= 7
ZONE_NAV.CURRENT_URL_INFO 		= 8
ZONE_NAV.GO_BACK 				= 9
ZONE_NAV.GO_FORWARD 			= 10
ZONE_NAV.SEARCH_REQUEST 		= 11
ZONE_NAV.SEARCH_RESPONSE 		= 12