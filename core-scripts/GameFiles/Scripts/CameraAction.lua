--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- from CCameraClass.h

CameraAction			= {}
CameraAction.Add		= 0
CameraAction.Remove          	= 1
CameraAction.Update          	= 2
CameraAction.Bind            	= 3
