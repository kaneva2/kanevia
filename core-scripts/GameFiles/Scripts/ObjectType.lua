--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--********************************************************* 
-- 	Global ObjectType definition used by
-- 	CPP modules and LUA script files
--
-- 	Objective is for Game script and Platform C++ code
--  to use the same base definition. 
--
--  Reference the file ObjectType.h in the
--  Platform project.
--********************************************************* 
ObjectType = {}
ObjectType.NONE 		= 0
ObjectType.UNKNOWN		= 0 
ObjectType.WORLD 		= 1
ObjectType.DYNAMIC 		= 4
ObjectType.PLAYER 		= 8
ObjectType.ENTITY 		= 8
ObjectType.EQUIPPABLE 	= 16
ObjectType.BONE 		= 32
ObjectType.CAMERA 		= 64
ObjectType.SOUND		= 128

