--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

------------BUILD SETTINGS-------------------------------------
BUILD_SETTINGS_EVENT = "BuildSettingsEvent"

BUILD_SETTINGS_CAMERA_COLLISION = 1
BUILD_SETTINGS_SELECTION_GLOW = 2
BUILD_SETTINGS_SELECTION_SHOW_XYZ = 3
BUILD_SETTINGS_SELECTION_SHOW_UNDO_REDO = 4
BUILD_SETTINGS_SELECTION_SHOW_SOUNDS = 5
BUILD_SETTINGS_SELECTION_SHOW_WIDGET = 6
BUILD_SETTINGS_SELECTION_ADVANCED_MOVEMENT = 7
BUILD_SETTINGS_DROP_DISTANCE = 8
BUILD_SETTINGS_ADVANCED_BUILD = 9
BUILD_SETTINGS_SELECTION_SHOW_MEDIA = 10

---------------MODES---------------------------------------------
--Modes
MODE_RETAIN		= -1
MODE_NONE		= 0
MODE_MOVE 		= 1
MODE_ROTATE 	= 2
MODE_TEXTURES 	= 3
MODE_SOUNDS     = 4
MODE_INVENTORY  = 5

CHANGE_BUILD_MODE_EVENT = "ChangeBuildModeEvent"
EQUIPPABLE_ITEM_TRANSLATE_EVENT = "EquippableItemTranslateEvent"