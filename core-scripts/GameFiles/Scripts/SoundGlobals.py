#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

##--Sound indices as of August 05, 2008--
SOUND_HORNET 		        = 0
SOUND_SPLAT 		        = 1 ## Doesn't work?
SOUND_WATER 		        = 2 ## Doesn't work?
SOUND_CHEERING 		        = 3 ## Doesn't work?
SOUND_MISS 			        = 4
SOUND_PERFECT 		        = 5
SOUND_LEVELUP 		        = 6
SOUND_FINALCHEER 	        = 7
SOUND_ARROWCLICK 	        = 8
SOUND_COINPICKUP 	        = 9
SOUND_KACHING_COIN          = 10
SOUND_KACHING_DEATHFEM1     = 11
SOUND_KACHING_DEATHFEM2     = 12
SOUND_KACHING_DEATHMALE1    = 13
SOUND_KACHING_DEATHMALE2    = 14
SOUND_KACHING_DROPOFF       = 15
SOUND_KACHING_FIRE1         = 16
SOUND_KACHING_FIRE2         = 17
SOUND_KACHING_FIRE3         = 18
SOUND_KACHING_FIRE4         = 19
SOUND_KACHING_IMPACT1       = 20
SOUND_KACHING_IMPACT2       = 21
SOUND_KACHING_NOAMMO        = 22
SOUND_KACHING_PICKUP1       = 23
SOUND_KACHING_PICKUP2       = 24
SOUND_KACHING_PICKUP3       = 25
SOUND_KACHING_RELOAD1       = 26
SOUND_KACHING_RELOAD2       = 27
SOUND_KACHING_RELOAD3       = 28
SOUND_KCVO_CHECK            = 29
SOUND_KCVO_DENIED           = 30
SOUND_KCVO_GAINEDLEAD       = 31
SOUND_KCVO_GAMEOVER         = 32
SOUND_KCVO_KACHING          = 33
SOUND_KCVO_LOCKLOAD         = 34
SOUND_KCVO_ONEMINUTE        = 35
SOUND_KCVO_REDNEARVIC       = 36
SOUND_KCVO_RELOAD           = 37
SOUND_KCVO_TENSECONDS       = 38
SOUND_KCVO_GOTBOUNCED       = 39
SOUND_KCVO_BLUENEARVIC      = 40
SOUND_KACHING_JUMPPAD       = 41
SOUND_KCVO_REDSAVE          = 42
SOUND_KCVO_BLUESAVE         = 43
SOUND_KCVO_LOSTLEAD         = 44


## The tuple is sound falloff and clip distance in feet.
## falloff of 0 means the sound can be heard at full volume through out the entire range
## falloff of 1 means the sound is reduced to min volume instantly
SoundAttribs = { SOUND_KACHING_COIN         : [ 0.1, 30 ],
                 SOUND_KACHING_DEATHFEM1    : [ 0.1, 150 ],
                 SOUND_KACHING_DEATHFEM2    : [ 0.1, 200 ],
                 SOUND_KACHING_DEATHMALE1   : [ 0.1, 150 ],
                 SOUND_KACHING_DEATHMALE2   : [ 0.1, 200 ],
                 SOUND_KACHING_DROPOFF      : [ 0.1, 200 ],
                 SOUND_KACHING_FIRE1        : [ 0.6, 200 ],
                 SOUND_KACHING_FIRE2        : [ 0.4, 200 ],
                 SOUND_KACHING_FIRE3        : [ 0.2, 200 ],
                 SOUND_KACHING_FIRE4        : [ 0.1, 200 ],
                 SOUND_KACHING_IMPACT1      : [ 0.3, 100 ],
                 SOUND_KACHING_IMPACT2      : [ 0.3, 100 ],
                 SOUND_KACHING_NOAMMO       : [ 0.5, 20 ],
                 SOUND_KACHING_PICKUP1      : [ 0.1, 30 ],
                 SOUND_KACHING_PICKUP2      : [ 0.1, 30 ],
                 SOUND_KACHING_PICKUP3      : [ 0.1, 30 ],
                 SOUND_KACHING_RELOAD1      : [ 0.2, 30 ],
                 SOUND_KACHING_RELOAD2      : [ 0.2, 30 ],
                 SOUND_KACHING_RELOAD3      : [ 0.2, 30 ],
                 SOUND_KCVO_CHECK           : [ 0.0, 10 ],
                 SOUND_KCVO_DENIED          : [ 0.0, 10 ],
                 SOUND_KCVO_GAINEDLEAD      : [ 0.0, 30 ],
                 SOUND_KCVO_GAMEOVER        : [ 0.0, 999 ],
                 SOUND_KCVO_KACHING         : [ 0.1, 200 ],
                 SOUND_KCVO_LOCKLOAD        : [ 0.0, 10 ],
                 SOUND_KCVO_ONEMINUTE       : [ 0.0, 999 ],
                 SOUND_KCVO_REDNEARVIC      : [ 0.0, 999 ],
                 SOUND_KCVO_RELOAD          : [ 0.5, 20 ],
                 SOUND_KCVO_TENSECONDS      : [ 0.0, 999 ],
                 SOUND_KCVO_GOTBOUNCED      : [ 0.0, 10 ],
                 SOUND_KCVO_BLUENEARVIC     : [ 0.0, 999 ],
                 SOUND_KACHING_JUMPPAD      : [ 0.1, 50 ],
                 SOUND_KCVO_REDSAVE         : [ 0.0, 10 ],
                 SOUND_KCVO_BLUESAVE        : [ 0.0, 10 ],
                 SOUND_KCVO_LOSTLEAD        : [ 0.0, 30 ]
               }