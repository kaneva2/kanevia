--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

UGC_TYPE = {}
UGC_TYPE.INVALID		= -1
UGC_TYPE.UNKNOWN		= 0
UGC_TYPE.COLLADA		= 1
UGC_TYPE.DYNOBJ			= 16 --tonumber("0010", 16)		-- Dynamic objects
UGC_TYPE.ZONEOBJ		= 32 --tonumber("0020", 16)		-- Zone objects (CWldObject/CReferenceObj) - used as parent type for custom textures
UGC_TYPE.TEXTURE		= 48 --tonumber("0030", 16)
UGC_TYPE.TEX_ZONE		= 49 --tonumber("0031", 16)		-- Custom textures for CWldObject and CReferenceObj
UGC_TYPE.TEX_DO			= 50 --tonumber("0032", 16)		-- Custom textures for dynamic objects from server
UGC_TYPE.TEX_UGCDO		= 51 --tonumber("0033", 16)		-- Custom textures for user created dynamic objects
UGC_TYPE.TEX_ICON		= 52 --tonumber("0034", 16)		-- Custom textures for user created dynamic objects
UGC_TYPE.ACTOR			= 64 --tonumber("0040", 16)
UGC_TYPE.CHARANIM		= 80 --tonumber("0050", 16)		-- Character animation
UGC_TYPE.SOUND			= 96 --tonumber("0060", 16)
UGC_TYPE.WAV_SOUND		= 97 --tonumber("0061", 16)
UGC_TYPE.OGG_SOUND		= 98 --tonumber("0062", 16)
UGC_TYPE.MP3_SOUND		= 99 --tonumber("0063", 16)
UGC_TYPE.EQUIPPABLE		= 112 --tonumber("0070", 16)		-- Equippable Objects
UGC_TYPE.EQUIPPABLE_RIGID = 113 --tonumber("0071", 16)	-- Rigid Equippable Objects
UGC_TYPE.EQUIPPABLE_SKELETAL = 114 --tonumber("0072", 16)	-- Skeletal Equippable Objects
UGC_TYPE.MEDIA_IMAGE	= 128 --tonumber("0080", 16)
UGC_TYPE.MEDIA_IMAGE_PATTERN	= 129 --tonumber("0081", 16)
UGC_TYPE.MEDIA_IMAGE_PICTURE	= 130 --tonumber("0082", 16)
UGC_TYPE.MEDIA_VIDEO	= 144 --tonumber("0090", 16)
UGC_TYPE.PARTICLE_EFFECT= 160 --tonumber("00A0", 16)
UGC_TYPE.SCRIPT			= 192 --tonumber("00C0", 16)
UGC_TYPE.SCRIPT_DO		= 193 --tonumber("00C1", 16)		-- Custom dynamic object scripts
UGC_TYPE.SCRIPT_BOT		= 194 --tonumber("00C2", 16)		-- Custom player control scripts
UGC_TYPE.SCRIPT_CHATTER = 195 --tonumber("00C3", 16)	-- Custom chatting scripts
UGC_TYPE.ACTION_ITEM	= 196 --tonumber("00C4", 16)
UGC_TYPE.APP_EDIT		= 197 --tonumber("00C5", 16)
UGC_TYPE.URL			= 240 --tonumber("00F0", 16)
UGC_TYPE.URL_DYNOBJ		= 241 --tonumber("00F1", 16)		-- set URL on a dynamic object
UGC_TYPE.URL_ACTOR		= 242 --tonumber("00F2", 16)		-- set URL on an actor
UGC_TYPE.CATEMASK		= 4080 --tonumber("0FF0", 16)
UGC_TYPE.TYPEMASK		= 16383 --tonumber("3FFF", 16)
UGC_TYPE.CUSTOMIZATION	= 16384 --tonumber("4000", 16)
UGC_TYPE.DERIVATIVE		= 32768 --tonumber("8000", 16)
UGC_TYPE.DYNOBJ_DERIVATIVE = UGC_TYPE.DYNOBJ + UGC_TYPE.DERIVATIVE
UGC_TYPE.DYNOBJ_CUSTOMIZATION = UGC_TYPE.DYNOBJ + UGC_TYPE.CUSTOMIZATION
UGC_TYPE.ZONEOBJ_CUSTOMIZATION = UGC_TYPE.ZONEOBJ + UGC_TYPE.CUSTOMIZATION

UGC_ERROR = {}
UGC_ERROR.SOURCE_READ_ERROR 		= -1
UGC_ERROR.UNRECOGNIZED_FILE_TYPE 	= -2
UGC_ERROR.MEMORY_ERROR		 	= -3
UGC_ERROR.UNSUPPORTED_CODEC	 	= -4
UGC_ERROR.VORBIS_ENCODE_ERROR		= -5
UGC_ERROR.CHANNEL_ERROR			= -6

eASSET_TYPE = {}
eASSET_TYPE.ALL = 0
eASSET_TYPE.GAME = 1
eASSET_TYPE.VIDEO = 2
eASSET_TYPE.ASSET = 3
eASSET_TYPE.MUSIC = 4
eASSET_TYPE.PICTURE = 5
eASSET_TYPE.PATTERN = 6
eASSET_TYPE.TV = 7
eASSET_TYPE.WIDGET = 8

eASSET_SUBTYPE = {}
eASSET_SUBTYPE.ALL = 0
eASSET_SUBTYPE.YOUTUBE = 1

URLTYPE = {}
URLTYPE.Unknown = 0
URLTYPE.SupportedVideoUrl = 1

KeyStates = {}
KeyStates.SHIFT = 4
KeyStates.CONTROL = 8
KeyStates.ALT = 32

PROP_MESH_CNT			= 0x1000
PROP_VERTEX_CNT			= 0x1001
PROP_TRIANGLE_CNT		= 0x1002
PROP_BOUNDBOX_SIZEX		= 0x1003
PROP_BOUNDBOX_SIZEY		= 0x1004
PROP_BOUNDBOX_SIZEZ		= 0x1005
PROP_MATERIAL_CNT		= 0x1006
PROP_TEXTURE_CNT		= 0x1007
PROP_DISTINCT_TEXTURE_CNT = 0x1008
PROP_BOUNDBOX_VOLUME	= 0x1009
PROP_BOUNDBOX_MINX		= 0x100A
PROP_BOUNDBOX_MINY		= 0x100B
PROP_BOUNDBOX_MINZ		= 0x100C
PROP_BOUNDBOX_MAXX		= 0x100D
PROP_BOUNDBOX_MAXY		= 0x100E
PROP_BOUNDBOX_MAXZ		= 0x100F

-- UGC import confirmation type
UIC = {}
UIC.GENERIC = 0
UIC.MISSING_TEXTURE = 1

-- Scaling Options
predefinedUnits = {
	{ name="Select unit of imported file", ratio=0 },
	{ name="Inch (in)", ratio=0.083 },
	{ name="Millimeter (mm)", ratio=0.00328 },
	{ name="Centimeter (cm)", ratio=0.0328 },
	{ name="Decimeter (dm)", ratio=0.328},
	{ name="Meter (m)", ratio=3.28 }
}

-- Actor types
actorTypes = {
	{ text = "Male", data = 1, gender = "M", stuntId = 6, actorGroup = 1 },
	{ text = "Female", data = 2, gender = "F", stuntId = 7, actorGroup = 2 }
}

actorTypesBoth = {
	{ text = "Both", data = 1, gender = "", stuntId = 0, actorGroup = 0 },
	{ text = "Male", data = 2, gender = "M", stuntId = 6, actorGroup = 1 },
	{ text = "Female", data = 3, gender = "F", stuntId = 7, actorGroup = 2 }
}

MT_NONE = 0
MT_RIDE = 1
MT_STUNT = 2

-- Filter for UGC events (e.g. UGCGlobalOptionResultEvent)
UGC_FILTER = {}
UGC_FILTER.DO_IMPORT_PROG = 1	-- "MeshImportProg.xml"
UGC_FILTER.DO_IMPORT_OPTS = 2	-- "MeshImportOptions.xml"
UGC_FILTER.CA_IMPORT_PROG = 3	-- "AnimationImportProg.xml"
UGC_FILTER.CA_IMPORT_OPTS = 4	-- "AnimationImportOptions.xml"
UGC_FILTER.EI_IMPORT_PROG = 5	-- "EquippableImportProg.xml"
UGC_FILTER.EI_IMPORT_OPTS = 6	-- "EquippableImportOptions.xml"

UGC_EQUIPPABLE_GROUP = 5