--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- This should match ScriptServerEvent::EventTypes
ScriptServerEventTypes = {}

ScriptServerEventTypes.Timer = 0
ScriptServerEventTypes.NewScript = nil		-- deprecated, was 1
ScriptServerEventTypes.Touch = 2
ScriptServerEventTypes.RemovePlacementObject = 3
ScriptServerEventTypes.PlayerArrived = 4
ScriptServerEventTypes.PlayerDeparted = 5
ScriptServerEventTypes.PlayerLocation = 6
ScriptServerEventTypes.ObjectMessage = 7
ScriptServerEventTypes.MenuEvent = 8
ScriptServerEventTypes.PlayerUseItem = 9
ScriptServerEventTypes.PlayerCheckInventory = 10
ScriptServerEventTypes.PlayerRemoveItem = 11
ScriptServerEventTypes.GameZonesInfo = 12
ScriptServerEventTypes.GameInstanceInfo = 13
ScriptServerEventTypes.CreateInstance = 14
ScriptServerEventTypes.PlayerJoinInstance = 15
ScriptServerEventTypes.LockInstance = 16
ScriptServerEventTypes.Trigger = 17
ScriptServerEventTypes.GetAllScripts = 18
ScriptServerEventTypes.UploadScript = 19
ScriptServerEventTypes.DeleteScript = 20
ScriptServerEventTypes.AttachScript = 21
ScriptServerEventTypes.DetachScript = 22
ScriptServerEventTypes.ScriptMakeWebcall = 23
ScriptServerEventTypes.PlayerEquipItem = 24
ScriptServerEventTypes.PlayerUnEquipItem = 25
ScriptServerEventTypes.ShutdownScript = 26
ScriptServerEventTypes.TransmitGameState = 27
ScriptServerEventTypes.PlayerRayIntersects = 28
ScriptServerEventTypes.MovePlacementObject = 29
ScriptServerEventTypes.ObjectGetNextPlayListItem = 30
ScriptServerEventTypes.ObjectSetNextPlayListItem = 31
ScriptServerEventTypes.ObjectEnumeratePlayList = 32
ScriptServerEventTypes.ObjectSetPlayList = 33
ScriptServerEventTypes.AddPlacementObject = 34
ScriptServerEventTypes.Collide = 35
ScriptServerEventTypes.DownloadAsset = 36
ScriptServerEventTypes.DeleteAsset = 37
ScriptServerEventTypes.PlayerAddItem = 38
ScriptServerEventTypes.ObjectClick = 39
ScriptServerEventTypes.PlayerClick = 40
ScriptServerEventTypes.PresetMenuAction = 41
ScriptServerEventTypes.GetServerLog = 42
ScriptServerEventTypes.ProcessUGCUpload = 43
-- ScriptServerEventTypes.ReloadScript = 44		-- Deprecated. Use AttachScript with same file path instead.
ScriptServerEventTypes.PlayerSetEquipped = 45
ScriptServerEventTypes.PlayerRemoveAllAccessories = 46
ScriptServerEventTypes.PlaceGameItem = 47
ScriptServerEventTypes.EndCollide = 48
