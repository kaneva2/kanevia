--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

TaskState = {}

-- Overall task state
TaskState.TS_START			= tonumber("0001", 16)		-- Start task
TaskState.TS_COMPLETE		= tonumber("0004", 16)		-- Task completed
TaskState.TS_FAILED			= tonumber("0008", 16)		-- Task failed
TaskState.TS_CANCELED		= tonumber("0002", 16)		-- Task canceled

-- Task pre-processing
TaskState.TS_PREP_START		= tonumber("0010", 16)		-- Start Preprocessing
TaskState.TS_PREP_PROGRESS	= tonumber("0020", 16)		-- Preprocessing in progress
TaskState.TS_PREP_COMPLETE	= tonumber("0040", 16)		-- Preprocessing completed
TaskState.TS_PREP_FAILED	= tonumber("0080", 16)		-- Preprocessing failed

-- Task processing
TaskState.TS_PROC_START		= tonumber("0100", 16)		-- Start processing task
TaskState.TS_PROC_PROGRESS	= tonumber("0200", 16)		-- Processing in progress
TaskState.TS_PROC_COMPLETE	= tonumber("0400", 16)		-- Processing completed
TaskState.TS_PROC_FAILED	= tonumber("0800", 16)		-- Processing failed


-- Task post-processing
TaskState.TS_PSTP_START		= tonumber("1000", 16)		-- Start post-processing task
TaskState.TS_PSTP_PROGRESS	= tonumber("2000", 16)		-- Postprocessing in progress
TaskState.TS_PSTP_COMPLETE	= tonumber("4000", 16)		-- Postprocessing completed
TaskState.TS_PSTP_FAILED	= tonumber("8000", 16)		-- Postprocessing failed
