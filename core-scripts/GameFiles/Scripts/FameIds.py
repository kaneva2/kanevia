#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

##INDICES OF ALL FAME PACKETS USED BY SERVER


##### FAME TYPE ######
WORLD_FAME                          = 1
DANCE_FAME                          = 2 ##NOT CURRENTLY IMPLEMENTED
KACHING_FAME                        = 3
FASHION_FAME                       = 4

#########Kaching Packets##########
KACHING_GAME_PLAYED                 = 61 ##NOT USED, ONLY FOR TESTING
KACHING_FIRST_GAME                  = 71
KACHING_FIRST_WIN                   = 81
KACHING_FIRST_SCORE                 = 91
KACHING_FIRST_CHECK                 = 101
KACHING_EVERY_WIN                   = 111
KACHING_EVERY_GAME_PLAYED           = 121
KACHING_CHECKS_ONE_GAME             = 131
KACHING_COINS_ONE_GAME              = 141
KACHING_DAILY_WINS                  = 151
KACHING_DAILY_PLAYS                 = 161 
FASHION_RAVE_RECEIVED              = 291
FASHION_DESIGN_PURCHASED           = 301
FASHION_DESIGNER_PURCHASED_ITEM    = 311
FASHION_DESIGN_TRIED_ON            = 321

######### Odd Jobs ##########
JOB_FAME_IDS                        = ( 46, 47, 48, 49, 50, 51, 52 )