#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

# from CommandActions.h

Shoot1              = 0
Shoot2              = 1
TurnRight           = 2
TurnLeft            = 3
YokeUp              = 4
YokeDown            = 5
SidestepRight       = 6
SidestepLeft        = 7
Forward             = 8
Backward            = 9
AutoRun             = 10
RollLeft            = 11
RollRight           = 12
Unused13            = 13
Unused14            = 14
Jump                = 15
LookRight           = 16	
LookLeft            = 17
LookUp              = 18
LookDown            = 19
Dismount            = 20
Respawn             = 21
Camera1             = 22  # old first person
Camera2             = 23  # old third person
NextWeapon          = 24
PrevWeapon          = 25
Chat                = 26
ZoomIn              = 27
ZoomOut             = 28
OpenMenu            = 29
ForceJet            = 30
StopMovement        = 31
Examine             = 32
UseItem             = 33
GroupChat           = 34
AnimTaunt1          = 35
AnimTaunt2          = 36
AnimTaunt3          = 37
AnimDance1          = 38
AnimDance2          = 39
AnimDance3          = 40
AnimWave            = 41
AnimSalute          = 42
AnimBow             = 43
AnimLaugh           = 44
EquipItem           = 45
Reload              = 46
Hide                = 47
Reveal              = 48
ClanChat            = 49
UseSkill            = 50
AnimSit             = 51
AnimCrouch          = 52
ToggleCursor        = 53
TargetNPC           = 54
TargetPC            = 55
TargetSelf          = 56
TargetGroupMember   = 57
TargetGroup2        = 58
TargetGroup3        = 59
TargetAutoAim       = 60
TargetAutoAttack    = 61
ToggleMouseLook     = 62
ToggleMouseSelect   = 63
Unused64            = 64
Unused65            = 65
Unused66            = 66
Unused67            = 67
Unused68            = 68
Unused69            = 69
Unused70            = 70
Unused71            = 71
ToggleShowFPS       = 72
OpenMainMenu        = 73
OpenInventoryMenu   = 74
