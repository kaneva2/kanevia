#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#


    #------------------------------------------------------------------------------------
KACHING = { 'PLAYER_KILLED1' : "@killer checked @killed.",
            'PLAYER_KILLED2' : "@killer sent @killed packing.",
            'PLAYER_KILLED3' : "@killer kicked @killed to the curb.",
            'PLAYER_KILLED4' : "@killed was taken out by @killer.",
            'PLAYER_KILLED5' : "@killed wasted by @killer.",
            'PLAYER_SCORED1' : "Ka-Ching! @player scored @points coins"
          }


#------------------------------------------------------------------------------------
TUTORIAL_TIPS = { 'ZONE_HANGOUT'     : "TUTORIAL: HANGOUT",
                  'ZONE_ARENA_LOBBY' : "TUTORIAL: ARENA LOBBY",
                  'ZONE_PLAZA'       : "TUTORIAL: PLAZA",
                  'ZONE_CITY'        : "TUTORIAL: CITY",
                  'ZONE_DANCE'       : "TUTORIAL: DANCE",
                  'ZONE_MALL'        : "TUTORIAL: MALL",
                  'ZONE_RESIDENCE'   : "TUTORIAL: RESIDENCE",
                  'ZONE_EMPLOYMENT'  : "Welcome to the One Click Employment Agency - where we reward you just for showing up to work!\n\nTo get your Rewards, walk up to an open computer and press TAB.\n\nYou can come back to work every 6 hours and earn additional Rewards. Plus, if you invite your friends, you'll earn even more!",
                  'MENU_KACHING1'    : "TUTORIAL: KACHING SIGNUP",
                  'MENU_WORLD_MAP'   : "TUTORIAL: WORLD MAP",
                  'MENU_DANCE'       : "Welcome to Dance Party 3D!\n\nTo start, click the 'Click to Play' button (in the bottom-right of your screen).\n\nFollow the arrows to dance!\n\nPlay Dance Party 3D to earn:\n   - Over $100 in Kaneva Rewards\n   - 20+ exclusive clothing items\n   - 16 unique Dance Party 3D trophies\n   - 10 special Dance Fame titles\n\nThe faster and more perfect you are, the quicker you win!"
                }

#------------------------------------------------------------------------------------
EMPLOYMENT = { 'TITLE_INVITE'       : "Welcome to the One Click Employment Agency",
               'TITLE_GOOD_JOB'     : "You Just Earned @rewards Rewards!",
               'TITLE_CLOSED'       : "You Can Only Work Every @hours Hours",
               'TITLE_TOP_RANK'     : "You're a @title!",
               'SUBTITLE_INVITE'    : "Your crew size: @crewsize. Your job title: @title.",
               'SUBTITLE_CLOSED'    : "Your crew size: @crewsize. Your job title: @title.",
               'BODY_INVITE'        : "Right now, you earn @rewards Rewards at the One Click Employment Agency.\n\nBy inviting friends to join Kaneva, you can get promoted and earn more Rewards!",
               'BODY_GOOD_JOB'      : "Well, that was easy! Come back to the One Click Employment Agency in @hours hours for more Rewards.\n\nWant to increase your earning potential? By inviting your friends to join Kaneva, you can earn more Rewards every time you show up for work!",
               'BODY_CLOSED'        : "Come back to the One Click Employment Agency in @timeleft for more Rewards.\n\nRight now, you earn @rewards Rewards at the One Click Employment Agency.\n\nBy inviting friends to join Kaneva, you'll get promoted and earn more Rewards!",
               'BODY_TOP_RANK'      : "Congrats! You've achieved the maximum salary for the One Click Employment Agency!\n\nYou will continue to earn @rewards Rewards every time you show up for work at the One Click Employment Agency.",
               'BODY_CLOSED_TOP'    : "Come back to the One Click Employment Agency in @timeleft to earn your well-deserved Rewards."
             }

