--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------


NudgeIds = {}

--Nudge Ids
NudgeIds.WELCOME_KANEVA = 64
NudgeIds.TAKE_FIRST_STEPS = 38
NudgeIds.TRAVEL_NEW_PLACE = 39
NudgeIds.CHAT_FIRST_TIME = 40
NudgeIds.PLAYED_3D_APP = 41
NudgeIds.SHOPPED_FIRST_TIME = 42
NudgeIds.ROTATED_CAMERA = 43
NudgeIds.QUICK_BUY_FIRST_TIME = 44
NudgeIds.DANCE_3D_APP_FIRST_TIME = 45
NudgeIds.ADDED_FRIEND_FIRST_TIME = 46
NudgeIds.POKER_3D_APP_FIRST_TIME = 47
NudgeIds.EMOTES_USE = 48
NudgeIds.CHAT_IN_PRIVATE_FIRST_TIME = 49
NudgeIds.KACHING_3D_APP_FIRST_TIME = 50
NudgeIds.VIEWED_SOMEONES_PROFILE = 51
NudgeIds.RAVED_SOMEONE = 34
NudgeIds.RAVED_SPACE = 53
NudgeIds.ADDED_PROFILE_PICTURE = 1
NudgeIds.OPENED_INVENTORY_MENU = 55
NudgeIds.PARTY_HOUR_BONUS = 56
NudgeIds.BUY_CREDITS_FIRST_TIME = 57
NudgeIds.BUILD_MODE_FIRST_TIME = 58
NudgeIds.P2P_ANIMATION_USE = 59
NudgeIds.COMPLETED_TOUR = 60
NudgeIds.TELEPORTED_TO_ANOTHER = 61
NudgeIds.MEGARAVED_FIRST_TIME = 62
NudgeIds.BLASTED_ON_THE_SITE = 63
NudgeIds.CHANGED_TV_MEDIA = 10
--NudgeIds.VISITED_COMMUNITY =
--NudgeIds.GET_SOME_GEAR =
--NudgeIds.GIVE_GIFT =
--NudgeIds.TRADE =
NudgeIds.VALIDATE_EMAIL_ADDRESS = 65
NudgeIds.BUY_AN_ANIMATION = 66
NudgeIds.FIRST_VISITOR_IN_YOUR_HOME = 67
NudgeIds.RAVE_SOME_CLOTHING = 68
NudgeIds.LOG_IN_3_DAYS_IN_A_ROW = 69
NudgeIds.BUY_A_VIP_PASS = 70
NudgeIds.CLICK_MY_FAME = 71
NudgeIds.CREATOR_WELCOME = 72
NudgeIds.DEVELOPER_EDIT_MENUS = 73
NudgeIds.DEVELOPER_ATTACH_SCRIPT = 74
NudgeIds.DEVELOPER_UPDATE_PROFILE_AND_PUBLISH = 75
NudgeIds.DEVELOPER_PLACE_OBJECT = 76
NudgeIds.DEVELOPER_CUSTOMIZE_ZONE = 77
NudgeIds.MAKE_A_GAME = 78
NudgeIds.BUILDER_WELCOME = 79
NudgeIds.BUILDER_PLACE_AN_OBJECT = 80
NudgeIds.BUILDER_MOVE_AN_OBJECT = 81
NudgeIds.BUILDER_ROTATE_AN_OBJECT = 82
NudgeIds.BUILDER_MULTISELECT = 83
NudgeIds.BUILDER_BUY_A_ZONE = 84
NudgeIds.BUILDER_SELL_A_ZONE = 85


--Nudge Data

NudgeData = {}
NudgeData[NudgeIds.WELCOME_KANEVA] = {}
NudgeData[NudgeIds.WELCOME_KANEVA].menuType = "WELCOME"
NudgeData[NudgeIds.WELCOME_KANEVA].textureName = "white.tga"
NudgeData[NudgeIds.WELCOME_KANEVA].textureCoords = {1,1,3,3}
NudgeData[NudgeIds.WELCOME_KANEVA].buttonText = "OK"
NudgeData[NudgeIds.WELCOME_KANEVA].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.WELCOME_KANEVA].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.TAKE_FIRST_STEPS] = {}
NudgeData[NudgeIds.TAKE_FIRST_STEPS].menuType = "LARGE"
NudgeData[NudgeIds.TAKE_FIRST_STEPS].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.TAKE_FIRST_STEPS].textureCoords = {0,0,288,104}
NudgeData[NudgeIds.TAKE_FIRST_STEPS].buttonText = "OK"
NudgeData[NudgeIds.TAKE_FIRST_STEPS].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.TAKE_FIRST_STEPS].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.TRAVEL_NEW_PLACE] = {}
NudgeData[NudgeIds.TRAVEL_NEW_PLACE].menuType = "LARGE"
NudgeData[NudgeIds.TRAVEL_NEW_PLACE].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.TRAVEL_NEW_PLACE].textureCoords = {0,831,288,935}
NudgeData[NudgeIds.TRAVEL_NEW_PLACE].buttonText = "Open Travel"
NudgeData[NudgeIds.TRAVEL_NEW_PLACE].tipTextureName = "icon_map_64x64.tga"
NudgeData[NudgeIds.TRAVEL_NEW_PLACE].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.CHAT_FIRST_TIME] = {}
NudgeData[NudgeIds.CHAT_FIRST_TIME].menuType = "LARGE"
NudgeData[NudgeIds.CHAT_FIRST_TIME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.CHAT_FIRST_TIME].textureCoords = {288,0,576,104}
NudgeData[NudgeIds.CHAT_FIRST_TIME].buttonText = "Show Chat"
NudgeData[NudgeIds.CHAT_FIRST_TIME].tipTextureName = "icon_people_40x40.tga"
NudgeData[NudgeIds.CHAT_FIRST_TIME].tipTextureCoords = {0,0,40,40}

NudgeData[NudgeIds.PLAYED_3D_APP] = {}
NudgeData[NudgeIds.PLAYED_3D_APP].menuType = "SMALL"
NudgeData[NudgeIds.PLAYED_3D_APP].textureName = "icon_Worlds_64x64.tga"
NudgeData[NudgeIds.PLAYED_3D_APP].textureCoords = {0,0,64,64}
NudgeData[NudgeIds.PLAYED_3D_APP].buttonText = "Browse Worlds"
NudgeData[NudgeIds.PLAYED_3D_APP].tipTextureName = "icon_Worlds_64x64.tga"
NudgeData[NudgeIds.PLAYED_3D_APP].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.SHOPPED_FIRST_TIME] = {}
NudgeData[NudgeIds.SHOPPED_FIRST_TIME].menuType = "SMALL"
NudgeData[NudgeIds.SHOPPED_FIRST_TIME].textureName = "icon_shop_64x64.tga"
NudgeData[NudgeIds.SHOPPED_FIRST_TIME].textureCoords = {0,0,64,64}
NudgeData[NudgeIds.SHOPPED_FIRST_TIME].buttonText = "Go To Shop Kaneva"
NudgeData[NudgeIds.SHOPPED_FIRST_TIME].tipTextureName = "icon_shop_64x64.tga"
NudgeData[NudgeIds.SHOPPED_FIRST_TIME].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.ROTATED_CAMERA] = {}
NudgeData[NudgeIds.ROTATED_CAMERA].menuType = "SMALL"
NudgeData[NudgeIds.ROTATED_CAMERA].textureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.ROTATED_CAMERA].textureCoords = {0,0,64,64}
NudgeData[NudgeIds.ROTATED_CAMERA].buttonText = "OK"
NudgeData[NudgeIds.ROTATED_CAMERA].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.ROTATED_CAMERA].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.QUICK_BUY_FIRST_TIME] = {}
NudgeData[NudgeIds.QUICK_BUY_FIRST_TIME].menuType = "LARGE"
NudgeData[NudgeIds.QUICK_BUY_FIRST_TIME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.QUICK_BUY_FIRST_TIME].textureCoords = {576,0,864,104}
NudgeData[NudgeIds.QUICK_BUY_FIRST_TIME].buttonText = "OK"
NudgeData[NudgeIds.QUICK_BUY_FIRST_TIME].tipTextureName = "icon_inventoryPlus_24x24.tga"
NudgeData[NudgeIds.QUICK_BUY_FIRST_TIME].tipTextureCoords = {0,0,24,24}

NudgeData[NudgeIds.DANCE_3D_APP_FIRST_TIME] = {}
NudgeData[NudgeIds.DANCE_3D_APP_FIRST_TIME].menuType = "LARGE"
NudgeData[NudgeIds.DANCE_3D_APP_FIRST_TIME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.DANCE_3D_APP_FIRST_TIME].textureCoords = {0,104,288,208}
NudgeData[NudgeIds.DANCE_3D_APP_FIRST_TIME].buttonText = "Dance Party 3D!"
NudgeData[NudgeIds.DANCE_3D_APP_FIRST_TIME].tipTextureName = "icon_3D_64x64.tga"
NudgeData[NudgeIds.DANCE_3D_APP_FIRST_TIME].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.ADDED_FRIEND_FIRST_TIME] = {}
NudgeData[NudgeIds.ADDED_FRIEND_FIRST_TIME].menuType = "LARGE"
NudgeData[NudgeIds.ADDED_FRIEND_FIRST_TIME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.ADDED_FRIEND_FIRST_TIME].textureCoords = {288,104,576,208}
NudgeData[NudgeIds.ADDED_FRIEND_FIRST_TIME].buttonText = "OK"
NudgeData[NudgeIds.ADDED_FRIEND_FIRST_TIME].tipTextureName = "icon_people_40x40.tga"
NudgeData[NudgeIds.ADDED_FRIEND_FIRST_TIME].tipTextureCoords = {0,0,40,40}

NudgeData[NudgeIds.POKER_3D_APP_FIRST_TIME] = {}
NudgeData[NudgeIds.POKER_3D_APP_FIRST_TIME].menuType = "LARGE"
NudgeData[NudgeIds.POKER_3D_APP_FIRST_TIME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.POKER_3D_APP_FIRST_TIME].textureCoords = {576,104,864,208}
NudgeData[NudgeIds.POKER_3D_APP_FIRST_TIME].buttonText = "Go to Texas Hold Em!"
NudgeData[NudgeIds.POKER_3D_APP_FIRST_TIME].tipTextureName = "icon_3D_64x64.tga"
NudgeData[NudgeIds.POKER_3D_APP_FIRST_TIME].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.EMOTES_USE] = {}
NudgeData[NudgeIds.EMOTES_USE].menuType = "SMALL"
NudgeData[NudgeIds.EMOTES_USE].textureName = "icon_emotes_64x64.tga"
NudgeData[NudgeIds.EMOTES_USE].textureCoords = {0,0,64,64}
NudgeData[NudgeIds.EMOTES_USE].buttonText = "Open My Emotes"
NudgeData[NudgeIds.EMOTES_USE].tipTextureName = "icon_emotes_64x64.tga"
NudgeData[NudgeIds.EMOTES_USE].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.CHAT_IN_PRIVATE_FIRST_TIME] = {}
NudgeData[NudgeIds.CHAT_IN_PRIVATE_FIRST_TIME].menuType = "LARGE"
NudgeData[NudgeIds.CHAT_IN_PRIVATE_FIRST_TIME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.CHAT_IN_PRIVATE_FIRST_TIME].textureCoords = {0,208,288,312}
NudgeData[NudgeIds.CHAT_IN_PRIVATE_FIRST_TIME].buttonText = "OK"
NudgeData[NudgeIds.CHAT_IN_PRIVATE_FIRST_TIME].tipTextureName = "icon_privateChat_24x24.tga"
NudgeData[NudgeIds.CHAT_IN_PRIVATE_FIRST_TIME].tipTextureCoords = {0,0,24,24}

NudgeData[NudgeIds.KACHING_3D_APP_FIRST_TIME] = {}
NudgeData[NudgeIds.KACHING_3D_APP_FIRST_TIME].menuType = "LARGE"
NudgeData[NudgeIds.KACHING_3D_APP_FIRST_TIME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.KACHING_3D_APP_FIRST_TIME].textureCoords = {288,208,576,312}
NudgeData[NudgeIds.KACHING_3D_APP_FIRST_TIME].buttonText = "Play Ka-Ching!"
NudgeData[NudgeIds.KACHING_3D_APP_FIRST_TIME].tipTextureName = "icon_3D_64x64.tga"
NudgeData[NudgeIds.KACHING_3D_APP_FIRST_TIME].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.VIEWED_SOMEONES_PROFILE] = {}
NudgeData[NudgeIds.VIEWED_SOMEONES_PROFILE].menuType = "LARGE"
NudgeData[NudgeIds.VIEWED_SOMEONES_PROFILE].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.VIEWED_SOMEONES_PROFILE].textureCoords = {576,208,864,312}
NudgeData[NudgeIds.VIEWED_SOMEONES_PROFILE].buttonText = "OK"
NudgeData[NudgeIds.VIEWED_SOMEONES_PROFILE].tipTextureName = "icon_people_40x40.tga"
NudgeData[NudgeIds.VIEWED_SOMEONES_PROFILE].tipTextureCoords = {0,0,40,40}

NudgeData[NudgeIds.RAVED_SOMEONE] = {}
NudgeData[NudgeIds.RAVED_SOMEONE].menuType = "SMALL"
NudgeData[NudgeIds.RAVED_SOMEONE].textureName = "icon_rave_64x64.tga"
NudgeData[NudgeIds.RAVED_SOMEONE].textureCoords = {0,0,64,64}
NudgeData[NudgeIds.RAVED_SOMEONE].buttonText = "OK"
NudgeData[NudgeIds.RAVED_SOMEONE].tipTextureName = "icon_rave_64x64.tga"
NudgeData[NudgeIds.RAVED_SOMEONE].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.RAVED_SPACE] = {}
NudgeData[NudgeIds.RAVED_SPACE].menuType = "LARGE"
NudgeData[NudgeIds.RAVED_SPACE].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.RAVED_SPACE].textureCoords = {0,312,288,416}
NudgeData[NudgeIds.RAVED_SPACE].buttonText = "OK"
NudgeData[NudgeIds.RAVED_SPACE].tipTextureName = "icon_rave_64x64.tga"
NudgeData[NudgeIds.RAVED_SPACE].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.ADDED_PROFILE_PICTURE] = {}
NudgeData[NudgeIds.ADDED_PROFILE_PICTURE].menuType = "LARGE"
NudgeData[NudgeIds.ADDED_PROFILE_PICTURE].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.ADDED_PROFILE_PICTURE].textureCoords = {288,312,576,416}
NudgeData[NudgeIds.ADDED_PROFILE_PICTURE].buttonText = "Go to My Profile"
NudgeData[NudgeIds.ADDED_PROFILE_PICTURE].tipTextureName = "icon_people_40x40.tga"
NudgeData[NudgeIds.ADDED_PROFILE_PICTURE].tipTextureCoords = {0,0,40,40}

NudgeData[NudgeIds.OPENED_INVENTORY_MENU] = {}
NudgeData[NudgeIds.OPENED_INVENTORY_MENU].menuType = "SMALL"
NudgeData[NudgeIds.OPENED_INVENTORY_MENU].textureName = "button_icon64x64_clothing.tga"
NudgeData[NudgeIds.OPENED_INVENTORY_MENU].textureCoords = {0,0,64,64}
NudgeData[NudgeIds.OPENED_INVENTORY_MENU].buttonText = "Open Clothing"
NudgeData[NudgeIds.OPENED_INVENTORY_MENU].tipTextureName = "button_icon64x64_clothing.tga"
NudgeData[NudgeIds.OPENED_INVENTORY_MENU].tipTextureCoords = {0,0,64,64}

--[[
NudgeData[NudgeIds.VISITED_COMMUNITY] = {}
NudgeData[NudgeIds.VISITED_COMMUNITY].menuType = "SMALL"
NudgeData[NudgeIds.VISITED_COMMUNITY].textureName = "icon_places_64x64.tga"
NudgeData[NudgeIds.VISITED_COMMUNITY].textureCoords = {0,0,64,64}
NudgeData[NudgeIds.VISITED_COMMUNITY].buttonText = "Browse Communities"
NudgeData[NudgeIds.VISITED_COMMUNITY].tipTextureName = "icon_places_64x64.tga"
NudgeData[NudgeIds.VISITED_COMMUNITY].tipTextureCoords = {0,0,64,64}
]]

NudgeData[NudgeIds.PARTY_HOUR_BONUS] = {}
NudgeData[NudgeIds.PARTY_HOUR_BONUS].menuType = "LARGE"
NudgeData[NudgeIds.PARTY_HOUR_BONUS].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.PARTY_HOUR_BONUS].textureCoords = {576,312,864,416}
NudgeData[NudgeIds.PARTY_HOUR_BONUS].buttonText = "Go to Kaneva City"
NudgeData[NudgeIds.PARTY_HOUR_BONUS].tipTextureName = "icon_people_40x40.tga"
NudgeData[NudgeIds.PARTY_HOUR_BONUS].tipTextureCoords = {0,0,40,40}

--[[
NudgeData[NudgeIds.GET_SOME_GEAR] = {}
NudgeData[NudgeIds.GET_SOME_GEAR].menuType = "LARGE"
NudgeData[NudgeIds.GET_SOME_GEAR].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.GET_SOME_GEAR].textureCoords = {0,416,288,520}
NudgeData[NudgeIds.GET_SOME_GEAR].buttonText = "Shop Kaneva?"
NudgeData[NudgeIds.GET_SOME_GEAR].tipTextureName = "icon_shop_64x64.tga"
NudgeData[NudgeIds.GET_SOME_GEAR].tipTextureCoords = {0,0,64,64}
]]

NudgeData[NudgeIds.BUY_CREDITS_FIRST_TIME] = {}
NudgeData[NudgeIds.BUY_CREDITS_FIRST_TIME].menuType = "SMALL"
NudgeData[NudgeIds.BUY_CREDITS_FIRST_TIME].textureName = "icon_credits_64x64.tga"
NudgeData[NudgeIds.BUY_CREDITS_FIRST_TIME].textureCoords = {0,0,64,64}
NudgeData[NudgeIds.BUY_CREDITS_FIRST_TIME].buttonText = "Get Credits!"
NudgeData[NudgeIds.BUY_CREDITS_FIRST_TIME].tipTextureName = "icon_credits_64x64.tga"
NudgeData[NudgeIds.BUY_CREDITS_FIRST_TIME].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.BUILD_MODE_FIRST_TIME] = {}
NudgeData[NudgeIds.BUILD_MODE_FIRST_TIME].menuType = "LARGE"
NudgeData[NudgeIds.BUILD_MODE_FIRST_TIME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.BUILD_MODE_FIRST_TIME].textureCoords = {288,416,576,520}
NudgeData[NudgeIds.BUILD_MODE_FIRST_TIME].buttonText = "Open Inventory"
NudgeData[NudgeIds.BUILD_MODE_FIRST_TIME].tipTextureName = "icon_buildMode_24x24.tga"
NudgeData[NudgeIds.BUILD_MODE_FIRST_TIME].tipTextureCoords = {0,0,24,24}

--[[
NudgeData[NudgeIds.GIVE_GIFT] = {}
NudgeData[NudgeIds.GIVE_GIFT].menuType = "LARGE"
NudgeData[NudgeIds.GIVE_GIFT].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.GIVE_GIFT].textureCoords = {0,0,1,1}
NudgeData[NudgeIds.GIVE_GIFT].buttonText = "OK"
NudgeData[NudgeIds.GIVE_GIFT].tipTextureName = "icon_gift_24x24.tga"
NudgeData[NudgeIds.GIVE_GIFT].tipTextureCoords = {0,0,24,24}
]]

--[[
NudgeData[NudgeIds.TRADE] = {}
NudgeData[NudgeIds.TRADE].menuType = "LARGE"
NudgeData[NudgeIds.TRADE].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.TRADE].textureCoords = {576,416,864,520}
NudgeData[NudgeIds.TRADE].buttonText = "OK"
NudgeData[NudgeIds.TRADE].tipTextureName = "icon_trade_24x24.tga"
NudgeData[NudgeIds.TRADE].tipTextureCoords = {0,0,24,24}
]]

NudgeData[NudgeIds.P2P_ANIMATION_USE] = {}
NudgeData[NudgeIds.P2P_ANIMATION_USE].menuType = "LARGE"
NudgeData[NudgeIds.P2P_ANIMATION_USE].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.P2P_ANIMATION_USE].textureCoords = {0,520,288,624}
NudgeData[NudgeIds.P2P_ANIMATION_USE].buttonText = "OK"
NudgeData[NudgeIds.P2P_ANIMATION_USE].tipTextureName = "icon_peer2peer_24x24.tga"
NudgeData[NudgeIds.P2P_ANIMATION_USE].tipTextureCoords = {0,0,24,24}

NudgeData[NudgeIds.COMPLETED_TOUR] = {}
NudgeData[NudgeIds.COMPLETED_TOUR].menuType = "SMALL"
NudgeData[NudgeIds.COMPLETED_TOUR].textureName = "icon_places_64x64.tga"
NudgeData[NudgeIds.COMPLETED_TOUR].textureCoords = {0,0,64,64}
NudgeData[NudgeIds.COMPLETED_TOUR].buttonText = "OK"
NudgeData[NudgeIds.COMPLETED_TOUR].tipTextureName = "icon_places_64x64.tga"
NudgeData[NudgeIds.COMPLETED_TOUR].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.TELEPORTED_TO_ANOTHER] = {}
NudgeData[NudgeIds.TELEPORTED_TO_ANOTHER].menuType = "LARGE"
NudgeData[NudgeIds.TELEPORTED_TO_ANOTHER].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.TELEPORTED_TO_ANOTHER].textureCoords = {288,520,576,624}
NudgeData[NudgeIds.TELEPORTED_TO_ANOTHER].buttonText = "Show Friends"
NudgeData[NudgeIds.TELEPORTED_TO_ANOTHER].tipTextureName = "icon_people_40x40.tga"
NudgeData[NudgeIds.TELEPORTED_TO_ANOTHER].tipTextureCoords = {0,0,40,40}

NudgeData[NudgeIds.MEGARAVED_FIRST_TIME] = {}
NudgeData[NudgeIds.MEGARAVED_FIRST_TIME].menuType = "LARGE"
NudgeData[NudgeIds.MEGARAVED_FIRST_TIME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.MEGARAVED_FIRST_TIME].textureCoords = {576,520,864,624}
NudgeData[NudgeIds.MEGARAVED_FIRST_TIME].buttonText = "OK"
NudgeData[NudgeIds.MEGARAVED_FIRST_TIME].tipTextureName = "icon_megaRave_24x24.tga"
NudgeData[NudgeIds.MEGARAVED_FIRST_TIME].tipTextureCoords = {0,0,24,24}

NudgeData[NudgeIds.BLASTED_ON_THE_SITE] = {}
NudgeData[NudgeIds.BLASTED_ON_THE_SITE].menuType = "LARGE"
NudgeData[NudgeIds.BLASTED_ON_THE_SITE].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.BLASTED_ON_THE_SITE].textureCoords = {0,624,288,728}
NudgeData[NudgeIds.BLASTED_ON_THE_SITE].buttonText = "Check out My Kaneva"
NudgeData[NudgeIds.BLASTED_ON_THE_SITE].tipTextureName = "icon_blast_24x24.tga"
NudgeData[NudgeIds.BLASTED_ON_THE_SITE].tipTextureCoords = {0,0,24,24}

NudgeData[NudgeIds.CHANGED_TV_MEDIA] = {}
NudgeData[NudgeIds.CHANGED_TV_MEDIA].menuType = "LARGE"
NudgeData[NudgeIds.CHANGED_TV_MEDIA].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.CHANGED_TV_MEDIA].textureCoords = {288,624,576,728}
NudgeData[NudgeIds.CHANGED_TV_MEDIA].buttonText = "Go to My Media Library"
NudgeData[NudgeIds.CHANGED_TV_MEDIA].tipTextureName = "icon_changeMedia_24x24.tga"
NudgeData[NudgeIds.CHANGED_TV_MEDIA].tipTextureCoords = {0,0,24,24}

NudgeData[NudgeIds.VALIDATE_EMAIL_ADDRESS] = {}
NudgeData[NudgeIds.VALIDATE_EMAIL_ADDRESS].menuType = "LARGE"
NudgeData[NudgeIds.VALIDATE_EMAIL_ADDRESS].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.VALIDATE_EMAIL_ADDRESS].textureCoords = {288,728,576,832}
NudgeData[NudgeIds.VALIDATE_EMAIL_ADDRESS].buttonText = "Go to My Kaneva"
NudgeData[NudgeIds.VALIDATE_EMAIL_ADDRESS].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.VALIDATE_EMAIL_ADDRESS].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.BUY_AN_ANIMATION] = {}
NudgeData[NudgeIds.BUY_AN_ANIMATION].menuType = "SMALL"
NudgeData[NudgeIds.BUY_AN_ANIMATION].textureName = "nudgeIcons1.tga"
NudgeData[NudgeIds.BUY_AN_ANIMATION].textureCoords = {64,0,128,64}
NudgeData[NudgeIds.BUY_AN_ANIMATION].buttonText = "Go to Shop Kaneva"
NudgeData[NudgeIds.BUY_AN_ANIMATION].tipTextureName = "nudgeIcons1.tga"
NudgeData[NudgeIds.BUY_AN_ANIMATION].tipTextureCoords = {64,0,128,64}

NudgeData[NudgeIds.FIRST_VISITOR_IN_YOUR_HOME] = {}
NudgeData[NudgeIds.FIRST_VISITOR_IN_YOUR_HOME].menuType = "LARGE"
NudgeData[NudgeIds.FIRST_VISITOR_IN_YOUR_HOME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.FIRST_VISITOR_IN_YOUR_HOME].textureCoords = {288,831,576,935}
NudgeData[NudgeIds.FIRST_VISITOR_IN_YOUR_HOME].buttonText = "OK"
NudgeData[NudgeIds.FIRST_VISITOR_IN_YOUR_HOME].tipTextureName = "nudgeIcons1.tga"
NudgeData[NudgeIds.FIRST_VISITOR_IN_YOUR_HOME].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.RAVE_SOME_CLOTHING] = {}
NudgeData[NudgeIds.RAVE_SOME_CLOTHING].menuType = "LARGE"
NudgeData[NudgeIds.RAVE_SOME_CLOTHING].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.RAVE_SOME_CLOTHING].textureCoords = {0,728,288,832}
NudgeData[NudgeIds.RAVE_SOME_CLOTHING].buttonText = "Go to Shop Kaneva"
NudgeData[NudgeIds.RAVE_SOME_CLOTHING].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.RAVE_SOME_CLOTHING].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.LOG_IN_3_DAYS_IN_A_ROW] = {}
NudgeData[NudgeIds.LOG_IN_3_DAYS_IN_A_ROW].menuType = "SMALL"
NudgeData[NudgeIds.LOG_IN_3_DAYS_IN_A_ROW].textureName = "nudgeIcons1.tga"
NudgeData[NudgeIds.LOG_IN_3_DAYS_IN_A_ROW].textureCoords = {128,0,192,64}
NudgeData[NudgeIds.LOG_IN_3_DAYS_IN_A_ROW].buttonText = "OK"
NudgeData[NudgeIds.LOG_IN_3_DAYS_IN_A_ROW].tipTextureName = "nudgeIcons1.tga"
NudgeData[NudgeIds.LOG_IN_3_DAYS_IN_A_ROW].tipTextureCoords = {128,0,192,64}

NudgeData[NudgeIds.BUY_A_VIP_PASS] = {}
NudgeData[NudgeIds.BUY_A_VIP_PASS].menuType = "LARGE"
NudgeData[NudgeIds.BUY_A_VIP_PASS].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.BUY_A_VIP_PASS].textureCoords = {576,624,864,728}
NudgeData[NudgeIds.BUY_A_VIP_PASS].buttonText = "Get a VIP Pass"
NudgeData[NudgeIds.BUY_A_VIP_PASS].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.BUY_A_VIP_PASS].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.CLICK_MY_FAME] = {}
NudgeData[NudgeIds.CLICK_MY_FAME].menuType = "LARGE"
NudgeData[NudgeIds.CLICK_MY_FAME].textureName = "nudgeScreen1.tga"
NudgeData[NudgeIds.CLICK_MY_FAME].textureCoords = {576,728,864,832}
NudgeData[NudgeIds.CLICK_MY_FAME].buttonText = "View Your Fame"
NudgeData[NudgeIds.CLICK_MY_FAME].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.CLICK_MY_FAME].tipTextureCoords = {0,0,64,64}

NudgeData[NudgeIds.CREATOR_WELCOME] = {}
NudgeData[NudgeIds.CREATOR_WELCOME].menuType = "CREATOR_WELCOME"
NudgeData[NudgeIds.CREATOR_WELCOME].textureName = "white.tga"
NudgeData[NudgeIds.CREATOR_WELCOME].textureCoords = {1,1,3,3}
NudgeData[NudgeIds.CREATOR_WELCOME].buttonText = "Close"
NudgeData[NudgeIds.CREATOR_WELCOME].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.CREATOR_WELCOME].tipTextureCoords = {0,0,64,64}

-- DEVELOPER FAME NUDGES

NudgeData[NudgeIds.DEVELOPER_EDIT_MENUS] = {}
NudgeData[NudgeIds.DEVELOPER_EDIT_MENUS].menuType = "VERY_LARGE"
NudgeData[NudgeIds.DEVELOPER_EDIT_MENUS].textureName = "nudgeScreenDeveloper1.tga"
NudgeData[NudgeIds.DEVELOPER_EDIT_MENUS].textureCoords = {0,312,400,416}
NudgeData[NudgeIds.DEVELOPER_EDIT_MENUS].buttonText = "Detailed Instructions"
NudgeData[NudgeIds.DEVELOPER_EDIT_MENUS].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.DEVELOPER_EDIT_MENUS].tipTextureCoords = {448,0,512,64}

NudgeData[NudgeIds.DEVELOPER_ATTACH_SCRIPT] = {}
NudgeData[NudgeIds.DEVELOPER_ATTACH_SCRIPT].menuType = "VERY_LARGE"
NudgeData[NudgeIds.DEVELOPER_ATTACH_SCRIPT].textureName = "nudgeScreenDeveloper1.tga"
NudgeData[NudgeIds.DEVELOPER_ATTACH_SCRIPT].textureCoords = {0,208,400,312}
NudgeData[NudgeIds.DEVELOPER_ATTACH_SCRIPT].buttonText = "Detailed Instructions"
NudgeData[NudgeIds.DEVELOPER_ATTACH_SCRIPT].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.DEVELOPER_ATTACH_SCRIPT].tipTextureCoords = {448,0,512,64}

NudgeData[NudgeIds.DEVELOPER_UPDATE_PROFILE_AND_PUBLISH] = {}
NudgeData[NudgeIds.DEVELOPER_UPDATE_PROFILE_AND_PUBLISH].menuType = "VERY_LARGE"
NudgeData[NudgeIds.DEVELOPER_UPDATE_PROFILE_AND_PUBLISH].textureName = "nudgeScreenDeveloper1.tga"
NudgeData[NudgeIds.DEVELOPER_UPDATE_PROFILE_AND_PUBLISH].textureCoords = {0,416,400,520}
NudgeData[NudgeIds.DEVELOPER_UPDATE_PROFILE_AND_PUBLISH].buttonText = "Detailed Instructions"
NudgeData[NudgeIds.DEVELOPER_UPDATE_PROFILE_AND_PUBLISH].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.DEVELOPER_UPDATE_PROFILE_AND_PUBLISH].tipTextureCoords = {448,0,512,64}

NudgeData[NudgeIds.DEVELOPER_PLACE_OBJECT] = {}
NudgeData[NudgeIds.DEVELOPER_PLACE_OBJECT].menuType = "VERY_LARGE"
NudgeData[NudgeIds.DEVELOPER_PLACE_OBJECT].textureName = "nudgeScreenDeveloper1.tga"
NudgeData[NudgeIds.DEVELOPER_PLACE_OBJECT].textureCoords = {0,104,400,208}
NudgeData[NudgeIds.DEVELOPER_PLACE_OBJECT].buttonText = "Detailed Instructions"
NudgeData[NudgeIds.DEVELOPER_PLACE_OBJECT].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.DEVELOPER_PLACE_OBJECT].tipTextureCoords = {448,0,512,64}

NudgeData[NudgeIds.DEVELOPER_CUSTOMIZE_ZONE] = {}
NudgeData[NudgeIds.DEVELOPER_CUSTOMIZE_ZONE].menuType = "VERY_LARGE"
NudgeData[NudgeIds.DEVELOPER_CUSTOMIZE_ZONE].textureName = "nudgeScreenDeveloper1.tga"
NudgeData[NudgeIds.DEVELOPER_CUSTOMIZE_ZONE].textureCoords = {0,0,400,104}
NudgeData[NudgeIds.DEVELOPER_CUSTOMIZE_ZONE].buttonText = "Detailed Instructions"
NudgeData[NudgeIds.DEVELOPER_CUSTOMIZE_ZONE].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.DEVELOPER_CUSTOMIZE_ZONE].tipTextureCoords = {448,0,512,64}

NudgeData[NudgeIds.MAKE_A_GAME] = {}
NudgeData[NudgeIds.MAKE_A_GAME].menuType = "LARGE"
NudgeData[NudgeIds.MAKE_A_GAME].textureName = "nudges.tga"
NudgeData[NudgeIds.MAKE_A_GAME].textureCoords = {4,4,406,108}
NudgeData[NudgeIds.MAKE_A_GAME].buttonText = "Detailed Instructions"
NudgeData[NudgeIds.MAKE_A_GAME].tipTextureName = "notificationIcons_1.tga"
NudgeData[NudgeIds.MAKE_A_GAME].tipTextureCoords = {448,0,512,64}
