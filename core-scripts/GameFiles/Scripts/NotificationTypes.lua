--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

NotificationTypes                     = {}
NotificationTypes.WebLink                            =  1
NotificationTypes.RavePlayer                         =  2
NotificationTypes.FriendRequest                      =  3
NotificationTypes.ViewInbox		                     =  4
NotificationTypes.GoToURL		                     =  5
NotificationTypes.FameXPEarned						 =  6
NotificationTypes.FameLevelUp				         =  7 
NotificationTypes.FameBadgeEarned					 =  8
NotificationTypes.Nudge								 =  9
NotificationTypes.RewardsEarned						 =  10
NotificationTypes.GeneralMessage					 =  11
NotificationTypes.FameXPAndRewardsEarned			 =  12
NotificationTypes.EventReminder						 =  13
NotificationTypes.GiftInvite		                 =  14
NotificationTypes.ReceivedCreditGift				 =  15
NotificationTypes.ViewWorldInvite					 =  16

NotificationTypesName = { "WebLink", "RavePlayer", "FriendRequest", "ViewInbox", "GoToURL", "FameXPEarned", "FameLevelUp", "FameBadgeEarned", "Nudge", "RewardsEarned", "GeneralMessage", "XPAndRewardsEarned", "EventReminder", "GiftInvite", "ReceivedCreditGift", "ViewWorldInvite" }

NotificationTextureTypes			  = {}
NotificationTextureTypes.None					     =  1
NotificationTextureTypes.Web						 =  2
NotificationTextureTypes.Local_XPEarned				 =  3
NotificationTextureTypes.Local_RewardsEarned		 =  4
NotificationTextureTypes.Rave						 =  5
NotificationTextureTypes.Tip						 =  6
NotificationTextureTypes.Message					 =  7
NotificationTextureTypes.Badge						 =  8
NotificationTextureTypes.Local_XPAndRewardsEarned	 =  9
NotificationTextureTypes.DeveloperTip				 =  10
NotificationTextureTypes.BuilderTip					 =  11
NotificationTextureTypes.Loot						 =	12
NotificationTextureTypes.Flag						 =	13

function getTextureTypeByName(name)
	if name then
		return NotificationTextureTypes[name]
	end
end

--LOCAL TEXTURES
LOCAL_TEXTURES = {}
--XP Earned
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPEarned] = {}
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPEarned].NORMAL    = {"notificationIcons_1.tga", 256, 0, 320, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPEarned].MOUSEOVER = {"notificationIcons_1.tga", 256, 64, 320, 128}
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPEarned].PRESSED   = {"notificationIcons_1.tga", 256, 0, 320, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPEarned].FOCUSED   = {"notificationIcons_1.tga", 256, 0, 320, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPEarned].DISABLED  = {"notificationIcons_1.tga", 256, 0, 320, 64}
--Rewards Earned
LOCAL_TEXTURES[NotificationTextureTypes.Local_RewardsEarned] = {}
LOCAL_TEXTURES[NotificationTextureTypes.Local_RewardsEarned].NORMAL    = {"notificationIcons_1.tga", 128, 0, 192, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Local_RewardsEarned].MOUSEOVER = {"notificationIcons_1.tga", 128, 64, 192, 128}
LOCAL_TEXTURES[NotificationTextureTypes.Local_RewardsEarned].PRESSED   = {"notificationIcons_1.tga", 128, 0, 192, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Local_RewardsEarned].FOCUSED   = {"notificationIcons_1.tga", 128, 0, 192, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Local_RewardsEarned].DISABLED  = {"notificationIcons_1.tga", 128, 0, 192, 64}
--Rave
LOCAL_TEXTURES[NotificationTextureTypes.Rave] = {}
LOCAL_TEXTURES[NotificationTextureTypes.Rave].NORMAL    = {"notificationIcons_1.tga", 64, 0, 128, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Rave].MOUSEOVER = {"notificationIcons_1.tga", 64, 64, 128, 128}
LOCAL_TEXTURES[NotificationTextureTypes.Rave].PRESSED   = {"notificationIcons_1.tga", 64, 0, 128, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Rave].FOCUSED   = {"notificationIcons_1.tga", 64, 0, 128, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Rave].DISABLED  = {"notificationIcons_1.tga", 64, 0, 128, 64}
--Tip
LOCAL_TEXTURES[NotificationTextureTypes.Tip] = {}
LOCAL_TEXTURES[NotificationTextureTypes.Tip].NORMAL    = {"notificationIcons_1.tga", 0, 0, 64, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Tip].MOUSEOVER = {"notificationIcons_1.tga", 0, 64, 64, 128}
LOCAL_TEXTURES[NotificationTextureTypes.Tip].PRESSED   = {"notificationIcons_1.tga", 0, 0, 64, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Tip].FOCUSED   = {"notificationIcons_1.tga", 0, 0, 64, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Tip].DISABLED  = {"notificationIcons_1.tga", 0, 0, 64, 64}
--Message
LOCAL_TEXTURES[NotificationTextureTypes.Message] = {}
LOCAL_TEXTURES[NotificationTextureTypes.Message].NORMAL    = {"notificationIcons_1.tga", 192, 0, 256, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Message].MOUSEOVER = {"notificationIcons_1.tga", 192, 64, 256, 128}
LOCAL_TEXTURES[NotificationTextureTypes.Message].PRESSED   = {"notificationIcons_1.tga", 192, 0, 256, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Message].FOCUSED   = {"notificationIcons_1.tga", 192, 0, 256, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Message].DISABLED  = {"notificationIcons_1.tga", 192, 0, 256, 64}
--Badge
LOCAL_TEXTURES[NotificationTextureTypes.Badge] = {}
LOCAL_TEXTURES[NotificationTextureTypes.Badge].NORMAL    = {"notificationIcons_1.tga", 320, 0, 384, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Badge].MOUSEOVER = {"notificationIcons_1.tga", 320, 64, 384, 128}
LOCAL_TEXTURES[NotificationTextureTypes.Badge].PRESSED   = {"notificationIcons_1.tga", 320, 0, 384, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Badge].FOCUSED   = {"notificationIcons_1.tga", 320, 0, 384, 64}
LOCAL_TEXTURES[NotificationTextureTypes.Badge].DISABLED  = {"notificationIcons_1.tga", 320, 0, 384, 64}
--Combo XP/Rewards Earned
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPAndRewardsEarned] = {}
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPAndRewardsEarned].NORMAL    = {"notificationIcons_1.tga", 384,   0, 448,  64}
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPAndRewardsEarned].MOUSEOVER = {"notificationIcons_1.tga", 384,  64, 448, 128}
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPAndRewardsEarned].PRESSED   = {"notificationIcons_1.tga", 384,   0, 448,  64}
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPAndRewardsEarned].FOCUSED   = {"notificationIcons_1.tga", 384,   0, 448,  64}
LOCAL_TEXTURES[NotificationTextureTypes.Local_XPAndRewardsEarned].DISABLED  = {"notificationIcons_1.tga", 384,   0, 448,  64}
--Developer Tips
LOCAL_TEXTURES[NotificationTextureTypes.DeveloperTip] = {}
LOCAL_TEXTURES[NotificationTextureTypes.DeveloperTip].NORMAL    = {"notificationIcons_1.tga", 448,   0, 512,  64}
LOCAL_TEXTURES[NotificationTextureTypes.DeveloperTip].MOUSEOVER = {"notificationIcons_1.tga", 448,  64, 512, 128}
LOCAL_TEXTURES[NotificationTextureTypes.DeveloperTip].PRESSED   = {"notificationIcons_1.tga", 448,   0, 512,  64}
LOCAL_TEXTURES[NotificationTextureTypes.DeveloperTip].FOCUSED   = {"notificationIcons_1.tga", 448,   0, 512,  64}
LOCAL_TEXTURES[NotificationTextureTypes.DeveloperTip].DISABLED  = {"notificationIcons_1.tga", 448,   0, 512,  64}
--Builder Tips
LOCAL_TEXTURES[NotificationTextureTypes.BuilderTip] = {}
LOCAL_TEXTURES[NotificationTextureTypes.BuilderTip].NORMAL    = {"notificationIcons_1.tga", 0, 128, 64, 192}
LOCAL_TEXTURES[NotificationTextureTypes.BuilderTip].MOUSEOVER = {"notificationIcons_1.tga", 0, 192, 64, 256}
LOCAL_TEXTURES[NotificationTextureTypes.BuilderTip].PRESSED   = {"notificationIcons_1.tga", 0, 128, 64, 192}
LOCAL_TEXTURES[NotificationTextureTypes.BuilderTip].FOCUSED   = {"notificationIcons_1.tga", 0, 128, 64, 192}
LOCAL_TEXTURES[NotificationTextureTypes.BuilderTip].DISABLED  = {"notificationIcons_1.tga", 0, 128, 64, 192}
--Loot
LOCAL_TEXTURES[NotificationTextureTypes.Loot] = {}
LOCAL_TEXTURES[NotificationTextureTypes.Loot].NORMAL    = {"notificationIcons_1.tga", 64, 128, 128, 192}
LOCAL_TEXTURES[NotificationTextureTypes.Loot].MOUSEOVER = {"notificationIcons_1.tga", 64, 192, 128, 256}
LOCAL_TEXTURES[NotificationTextureTypes.Loot].PRESSED   = {"notificationIcons_1.tga", 64, 128, 128, 192}
LOCAL_TEXTURES[NotificationTextureTypes.Loot].FOCUSED   = {"notificationIcons_1.tga", 64, 128, 128, 192}
LOCAL_TEXTURES[NotificationTextureTypes.Loot].DISABLED  = {"notificationIcons_1.tga", 64, 128, 128, 192}
--Flag
LOCAL_TEXTURES[NotificationTextureTypes.Flag] = {}
LOCAL_TEXTURES[NotificationTextureTypes.Flag].NORMAL    = {"notificationIcons_1.tga", 128, 128, 192, 192}
LOCAL_TEXTURES[NotificationTextureTypes.Flag].MOUSEOVER = {"notificationIcons_1.tga", 128, 192, 192, 256}
LOCAL_TEXTURES[NotificationTextureTypes.Flag].PRESSED   = {"notificationIcons_1.tga", 128, 128, 192, 192}
LOCAL_TEXTURES[NotificationTextureTypes.Flag].FOCUSED   = {"notificationIcons_1.tga", 128, 128, 192, 192}
LOCAL_TEXTURES[NotificationTextureTypes.Flag].DISABLED  = {"notificationIcons_1.tga", 128, 128, 192, 192}