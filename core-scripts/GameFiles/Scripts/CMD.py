#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

# CommandHeader.pl generated file 2007-04-11 11:59
BLOCKIP = 1
UNBLOCKIP = 2
BANPLAYER = 3
UNBANPLAYER = 4
GENERATE = 5
REMOVEHOUSE = 6
VUP = 7
VDOWN = 8
PLAYERCOUNT = 9
ZEROMURDERS = 10
HELPADMIN = 11
HELPGM = 1001
BROADCAST = 1002
SPAWNGEN = 1003
WHO = 1004
GOTO = 1005
GOTOPLAYER = 1006
SUMMON = 1007
ANSWERNEXTPAGE = 1008
CLANSHARINGON = 2001
CLANSHARINGOFF = 2002
HELP = 2003
HELPGEN = 2004
HELPCLAN = 2005
HELPCHAT = 2006
HELPHOUSE = 2007
LOCATION = 2008
LOOT = 2009
REBIRTH = 2010
TRADE = 2011
BANK = 2012
PAGEGM = 2013
BUY = 2014
ARENASTATS = 2015
SHOUT = 2016
WHISPER = 2017
WITHDRAW = 2018
DEPOSIT = 2019
CLANCREATE = 2020
CLANDISBAND = 2021
CLANJOIN = 2022
CLANAPPROVE = 2023
CLANREMOVE = 2024
CLANDECLAREWAR = 2025
CLANACCEPTWAR = 2026
CLANSURRENDER = 2027
CLANROSTER = 2028
CLANWARLIST = 2029
DESTROYSKILL = 2030
DESTROYJNLENTRY = 2031
MANAGE = 2032
LOCKHOUSE = 2033
UNLOCKHOUSE = 2034
TELL = 2035
TEAM = 2036
TRIBE = 2037
GOHOME = 2038
GOTOFRIEND = 2039
REPLY = 2040
PLACEOBJECT = 2041
MOVEOBJECT = 2042
REMOVEOBJECT = 2043
LISTOBJECTS = 2044
KICK = 2045
INVITE = 3000
GROUPMENU = 3001
SQUELCH = 3002
DESQUELCH = 3003
JOIN = 3004
LEAVE = 3005
REMOVE = 3006
LEADER = 3007
RELOADBLADE = 5000
