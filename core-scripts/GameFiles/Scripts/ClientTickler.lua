--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--Client Tickler 
ClientTickler = {}
ClientTickler.FriendRequest 		= 1
ClientTickler.PlayerRaved 			= 2
ClientTickler.NewMessage 			= 3
ClientTickler.InWorldInvite 		= 4
ClientTickler.BalanceChange 		= 5
ClientTickler.Fame_PacketProgress	= 6
ClientTickler.EventReminder			= 7
ClientTickler.InWorldGiftInvite		= 8

ClientTickler.ReceivedCreditGift    = 18
ClientTickler.EventCreated			= 19
ClientTickler.WebWorldInvite		= 20
