--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- from CommandActions.h

CommandActions          	   = {}
CommandActions.Shoot1              = 0
CommandActions.Shoot2              = 1
CommandActions.TurnRight           = 2
CommandActions.TurnLeft            = 3
CommandActions.YokeUp              = 4
CommandActions.YokeDown            = 5
CommandActions.SidestepRight       = 6
CommandActions.SidestepLeft        = 7
CommandActions.Forward             = 8
CommandActions.Backward            = 9
CommandActions.AutoRun             = 10
CommandActions.RollLeft            = 11
CommandActions.RollRight           = 12
CommandActions.Unused13            = 13
CommandActions.Unused14            = 14
CommandActions.Jump                = 15
CommandActions.LookRight           = 16
CommandActions.LookLeft            = 17
CommandActions.LookUp              = 18
CommandActions.LookDown            = 19
CommandActions.Dismount            = 20
CommandActions.Respawn             = 21
CommandActions.Camera1             = 22  -- first person
CommandActions.Camera2             = 23  -- third person
CommandActions.NextWeapon          = 24
CommandActions.PrevWeapon          = 25
CommandActions.Chat                = 26
CommandActions.ZoomIn              = 27
CommandActions.ZoomOut             = 28
CommandActions.OpenMenu            = 29
CommandActions.ForceJet            = 30
CommandActions.StopMovement        = 31
CommandActions.Examine             = 32
CommandActions.UseItem             = 33
CommandActions.GroupChat           = 34
CommandActions.AnimTaunt1          = 35
CommandActions.AnimTaunt2          = 36
CommandActions.AnimTaunt3          = 37
CommandActions.AnimDance1          = 38
CommandActions.AnimDance2          = 39
CommandActions.AnimDance3          = 40
CommandActions.AnimWave            = 41
CommandActions.AnimSalute          = 42
CommandActions.AnimBow             = 43
CommandActions.AnimLaugh           = 44
CommandActions.EquipItem           = 45
CommandActions.Reload              = 46
CommandActions.Hide                = 47
CommandActions.Reveal              = 48
CommandActions.ClanChat            = 49
CommandActions.UseSkill            = 50
CommandActions.AnimSit             = 51
CommandActions.AnimCrouch          = 52
CommandActions.ToggleCursor        = 53
CommandActions.TargetNPC           = 54
CommandActions.TargetPC            = 55
CommandActions.TargetSelf          = 56
CommandActions.TargetGroupMember   = 57
CommandActions.TargetGroup2        = 58
CommandActions.TargetGroup3        = 59
CommandActions.TargetAutoAim       = 60
CommandActions.TargetAutoAttack    = 61
CommandActions.ToggleMouseLook     = 62
CommandActions.ToggleMouseSelect   = 63
CommandActions.Unused64            = 64
CommandActions.Unused65            = 65
CommandActions.Unused66            = 66
CommandActions.Unused67            = 67
CommandActions.Unused68            = 68
CommandActions.Unused69            = 69
CommandActions.Unused70            = 70
CommandActions.Unused71            = 71
CommandActions.ToggleShowFPS       = 72
CommandActions.OpenMainMenu        = 73
CommandActions.OpenInventoryMenu   = 74
CommandActions.CameraCharRotate    = 75
CommandActions.CameraRotate        = 76
CommandActions.OpenBuildMode       = 77
