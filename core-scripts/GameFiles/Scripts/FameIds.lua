--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--INDICES OF ALL FAME PACKETS USED BY SERVER 

FameIds = {}
----- FAME TYPE ------
FameIds.WORLD_FAME                          = 1
FameIds.DANCE_FAME                          = 2 --NOT CURRENTLY IMPLEMENTED
FameIds.KACHING_FAME                        = 3
FameIds.FASHION_FAME						= 4
FameIds.DEVELOPER_FAME						= 5
FameIds.BUILDER_FAME						= 6
-- NOTE: When you add a new fame type, make sure to add it in FameTypeIdToCommunityId()

FameTypes = 
{
	FameIds.WORLD_FAME,
	FameIds.DANCE_FAME,
	FameIds.KACHING_FAME,
	FameIds.FASHION_FAME,
	FameIds.DEVELOPER_FAME,
	FameIds.BUILDER_FAME
}

FameNames = {}
FameNames[FameIds.WORLD_FAME]				= "World Fame"
FameNames[FameIds.DANCE_FAME]				= "Dance Fame"
FameNames[FameIds.KACHING_FAME]				= "Ka-Ching! Fame"
FameNames[FameIds.FASHION_FAME]				= "Designer Fame"
FameNames[FameIds.DEVELOPER_FAME]			= "Developer Fame"
FameNames[FameIds.BUILDER_FAME]				= "Builder Fame"

FameTextures = {}
FameTextures[FameIds.WORLD_FAME]			= "filestore10/3906162/6743089/worldfame_ad.jpg"
FameTextures[FameIds.DANCE_FAME]			= "filestore10/3906162/6743085/danceparty3d_ad.jpg"
FameTextures[FameIds.KACHING_FAME]			= "filestore10/3906162/6743087/kaching_ad.jpg"
FameTextures[FameIds.FASHION_FAME]			= "filestore10/3906162/6743086/designerfame_ad.jpg"
FameTextures[FameIds.DEVELOPER_FAME]		= "filestore10/5553451/developer_fame_130x98_sm.jpg"
FameTextures[FameIds.BUILDER_FAME]			= "filestore10/5714735/BuilderFameThumb_la.jpg"

FameHeaderTextures = {}
FameHeaderTextures[FameIds.WORLD_FAME]		= "WorldFameHeader.tga"
FameHeaderTextures[FameIds.BUILDER_FAME]	= "BuildMode_436x95_LevelUp.tga"

FameBadgeTextures = {}
FameBadgeTextures[FameIds.WORLD_FAME]		= "icon_badge_64x64.tga"
FameBadgeTextures[FameIds.BUILDER_FAME]		= "BuildMode_64x64_LevelUpHelmet.tga"

FameChecklistID= {}
FameChecklistID[FameIds.WORLD_FAME]			= 3
FameChecklistID[FameIds.DANCE_FAME]			= nil
FameChecklistID[FameIds.KACHING_FAME]		= nil
FameChecklistID[FameIds.FASHION_FAME]		= nil
FameChecklistID[FameIds.DEVELOPER_FAME]		= 4
FameChecklistID[FameIds.BUILDER_FAME]		= 5

g_FameTypeIdToCommunityId = nil

function FameTypeIdToCommunityId(fameTypeId)
	if g_FameTypeIdToCommunityId == nil then
		g_FameTypeIdToCommunityId = {}
		g_FameTypeIdToCommunityId[FameIds.WORLD_FAME]		= 1
		g_FameTypeIdToCommunityId[FameIds.DANCE_FAME]		= 2
		g_FameTypeIdToCommunityId[FameIds.KACHING_FAME]		= 3
		g_FameTypeIdToCommunityId[FameIds.FASHION_FAME]		= 4

		-- Now fix the game-specific ones to work for Tech Preview, Preview and Production
		if GameGlobals.WEB_SITE_PREFIX == "http://wok.kaneva.com/" then
			g_FameTypeIdToCommunityId[FameIds.DEVELOPER_FAME] = 1003776695
			g_FameTypeIdToCommunityId[FameIds.BUILDER_FAME]   = 1003939978
		elseif GameGlobals.WEB_SITE_PREFIX == "http://dev-wok.kaneva.com/" then
			g_FameTypeIdToCommunityId[FameIds.DEVELOPER_FAME] = 1003389592
			g_FameTypeIdToCommunityId[FameIds.BUILDER_FAME]   = 1003395871
		elseif GameGlobals.WEB_SITE_PREFIX == "http://pv-wok.kaneva.com/" then
			g_FameTypeIdToCommunityId[FameIds.DEVELOPER_FAME] = 1003776695
			g_FameTypeIdToCommunityId[FameIds.BUILDER_FAME]   = 1003939978
		elseif GameGlobals.WEB_SITE_PREFIX == "http://rc-wok.kaneva.com/" then
			g_FameTypeIdToCommunityId[FameIds.DEVELOPER_FAME] = 1003776695
			g_FameTypeIdToCommunityId[FameIds.BUILDER_FAME]   = 1003939978
		else
			KEP_Log("ERROR: FameTypeIdToCommunityId() doesn't handle this WEB_SITE_PREFIX="..tostring(GameGlobals.WEB_SITE_PREFIX))
		end
	end

	return g_FameTypeIdToCommunityId[fameTypeId] or fameTypeId
end

g_CommunityIdToFameTypeId = nil

function CommunityIdToFameTypeId(communityId)
	if g_CommunityIdToFameTypeId == nil then
		g_CommunityIdToFameTypeId = {}
		g_CommunityIdToFameTypeId[1] = FameIds.WORLD_FAME
		g_CommunityIdToFameTypeId[2] = FameIds.DANCE_FAME
		g_CommunityIdToFameTypeId[3] = FameIds.KACHING_FAME
		g_CommunityIdToFameTypeId[4] = FameIds.FASHION_FAME

		-- Now fix the game-specific ones to work for Tech Preview, Preview and Production
		if GameGlobals.WEB_SITE_PREFIX == "http://wok.kaneva.com/" then
			g_CommunityIdToFameTypeId[1003776695]     = FameIds.DEVELOPER_FAME
			g_CommunityIdToFameTypeId[1003939978]     = FameIds.BUILDER_FAME
		elseif GameGlobals.WEB_SITE_PREFIX == "http://dev-wok.kaneva.com/" then
			g_CommunityIdToFameTypeId[1003389592]     = FameIds.DEVELOPER_FAME
			g_CommunityIdToFameTypeId[1003395871]     = FameIds.BUILDER_FAME
		elseif GameGlobals.WEB_SITE_PREFIX == "http://pv-wok.kaneva.com/" then
			g_CommunityIdToFameTypeId[1003776695]     = FameIds.DEVELOPER_FAME
			g_CommunityIdToFameTypeId[1003939978]     = FameIds.BUILDER_FAME
		else
			KEP_Log("ERROR: CommunityIdToFameTypeId() doesn't handle this WEB_SITE_PREFIX="..tostring(GameGlobals.WEB_SITE_PREFIX))
		end
	end
	return g_CommunityIdToFameTypeId[communityId] or communityId
end


---------Builder Fame Packets----------
FameIds.BUILDER_WELCOME							= 6000
FameIds.BUILDER_PLACED_FIRST_OBJECT				= 6001
FameIds.BUILDER_MOVED_AN_OBJECT					= 6002
FameIds.BUILDER_ROTATED_AN_OBJECT				= 6003
FameIds.BUILDER_BOUGHT_A_ZONE					= 6005
FameIds.BUILDER_SOLD_1ST_ZONE					= 6006
FameIds.BUILDER_SOLD_10_ZONES_FOR_CREDITS		= 6007
FameIds.BUILDER_SOLD_100_ZONES_FOR_CREDITS		= 6008
FameIds.BUILDER_SOLD_500_ZONES_FOR_CREDITS		= 6009
FameIds.BUILDER_SOLD_1000_ZONES_FOR_CREDITS		= 6010
FameIds.BUILDER_SOLD_10000_ZONES_FOR_CREDITS	= 6011
FameIds.BUILDER_SOLD_3_ZONES_FOR_REWARDS		= 6012
FameIds.BUILDER_SOLD_10_ZONES_FOR_REWARDS		= 6013
FameIds.BUILDER_SOLD_25_ZONES_FOR_REWARDS		= 6014
FameIds.BUILDER_SOLD_100_ZONES_FOR_REWARDS		= 6015
FameIds.BUILDER_PLACED_10_ITEMS					= 6016
FameIds.BUILDER_PLACED_30_ITEMS					= 6017
FameIds.BUILDER_PLACED_100_ITEMS				= 6018
FameIds.BUILDER_PLACED_500_ITEMS				= 6019
FameIds.BUILDER_PLACED_1000_ITEMS				= 6020
FameIds.BUILDER_DAILY_BONUS						= 6021
FameIds.BUILDER_ACTIVE_TIME						= 6022
FameIds.BUILDER_ACTIVE_TIME						= 6022
FameIds.BUILDER_PLACED_AN_OBJECT				= 6023


---------Developer Fame Packets----------
FameIds.DEVELOPER_TUTORIAL_READ						= 5000 -- AWARDED
FameIds.DEVELOPER_FIRST_MENU_EDITED					= 5001 -- AWARDED
FameIds.DEVELOPER_FIRST_SCRIPT_EDITED				= 5002 -- AWARDED
FameIds.DEVELOPER_FIRST_BADGE_ADDED					= 5003
FameIds.DEVELOPER_FIRST_PREMIUM_ADDED				= 5004
FameIds.DEVELOPER_FIRST_LEADERBOARD_MODDED			= 5005
FameIds.DEVELOPER_FIRST_MODEL_IMPORTED				= 5006
FameIds.DEVELOPER_FIRST_PROFILE_SETUP				= 5007
FameIds.DEVELOPER_FIRST_PUBLISH						= 5008 -- AWARDED
FameIds.DEVELOPER_FIRST_DEV_CHALLENGE				= 5009
FameIds.DEVELOPER_UNIQUE_VISITORS_1					= 5010
FameIds.DEVELOPER_UNIQUE_VISITORS_100				= 5011
FameIds.DEVELOPER_UNIQUE_VISITORS_500				= 5012
FameIds.DEVELOPER_UNIQUE_VISITORS_1000				= 5013
FameIds.DEVELOPER_UNIQUE_VISITORS_5000				= 5014
FameIds.DEVELOPER_FIRST_CUSTOMIZED_ZONE				= 5015 -- AWARDED
FameIds.DEVELOPER_FIRST_PLACED_OBJECT				= 5016 -- AWARDED
FameIds.DEVELOPER_PREMIUM_ITEM_SALE					= 5017
FameIds.DEVELOPER_EVERY_10_VISITORS					= 5018
FameIds.DEVELOPER_UNIQUE_VISITORS_10000				= 5019


---------Kaching Packets----------
FameIds.KACHING_GAME_PLAYED                 = 61 --NOT USED, ONLY FOR TESTING
FameIds.KACHING_FIRST_GAME                  = 71
FameIds.KACHING_FIRST_WIN                   = 81
FameIds.KACHING_FIRST_SCORE                 = 91
FameIds.KACHING_FIRST_CHECK                 = 101
FameIds.KACHING_EVERY_WIN                   = 111
FameIds.KACHING_EVERY_GAME_PLAYED           = 121
FameIds.KACHING_CHECKS_ONE_GAME             = 131
FameIds.KACHING_COINS_ONE_GAME              = 141
FameIds.KACHING_DAILY_WINS                  = 151
FameIds.KACHING_DAILY_PLAYS                 = 161 
FameIds.FASHION_RAVE_RECEIVED				= 281
FameIds.FASHION_DESIGN_PURCHASED_REWARDS	= 291
FameIds.FASHION_DESIGN_PURCHASED_CREDITS	= 301
FameIds.FASHION_DESIGNER_PURCHASED_ITEM    = 311
FameIds.FASHION_DESIGN_TRIED_ON			= 321

--------- WOK Fame Packets ----------
FameIds.WOK_FAME_DAILY_WEBSITE_LOGIN		= 39
FameIds.WOK_FAME_DAILY_LOGIN_BONUS			= 35
FameIds.WOK_FAME_ADDED_PROFILE_PICTURE		= 1
FameIds.WOK_FAME_VALIDATE_EMAIL_ADDRESS		= 44
FameIds.WOK_FAME_RAVE_SOMEONE				= 271
FameIds.WOK_FAME_YOU_WERE_MEGARAVED			= 55
FameIds.WOK_FAME_COMPLETED_TOUR				= 67
FameIds.WOK_FAME_CHANGE_YOUR_CLOTHING		= 6
FameIds.WOK_FAME_PLACE_AN_OBJECT_IN_YOUR_HOME = 5
FameIds.WOK_FAME_CHANGE_WALL_PATTERN		= 7
FameIds.WOK_FAME_CHANGE_TV_MEDIA			= 8
FameIds.WOK_FAME_COMPLETE_AVATAR_SELECT		= 171
FameIds.WOK_FAME_DAILY_BONUS_2				= 405 -- changed from LOGIN_2_DAYS_IN_A_ROW
FameIds.WOK_FAME_ADD_FIRST_FRIEND			= 406
FameIds.WOK_FAME_ADD_3_FRIENDS				= 407
FameIds.WOK_FAME_CHAT_FOR_THE_FIRST_TIME	= 408
FameIds.WOK_FAME_PRIVATE_CHAT_FOR_FIRST_TIME= 409
FameIds.WOK_FAME_USE_P2P_ANIMATION			= 410
FameIds.WOK_FAME_USE_AN_EMOTE				= 411
FameIds.WOK_FAME_PARTY_WITH_10_PEOPLE		= 412
FameIds.WOK_FAME_TRAVEL_TO_A_PERSON			= 413
FameIds.WOK_FAME_TRAVEL_TO_A_NEW_PLACE		= 414
FameIds.WOK_FAME_YOUR_HOME_WAS_RAVED		= 415
FameIds.WOK_FAME_USE_INSTANT_BUY			= 416
FameIds.WOK_FAME_BUY_AN_ITEM				= 417
FameIds.WOK_FAME_BUY_3RD_ITEM				= 418
FameIds.WOK_FAME_BUY_5TH_ITEM				= 419
FameIds.WOK_FAME_BUY_10TH_ITEM				= 420
FameIds.WOK_FAME_BUY_AN_ANIMATION			= 421
FameIds.WOK_FAME_TOOK_YOUR_FIRST_STEPS		= 422
FameIds.WOK_FAME_OPEN_INVENTORY_MENU		= 423
FameIds.WOK_FAME_OPEN_BUILD_MODE			= 424
FameIds.WOK_FAME_VIEW_SOMEONES_PROFILE		= 425
FameIds.WOK_FAME_SPENT_10_MINUTES_AT_EVENT	= 426
FameIds.WOK_FAME_PARTY_HOUR_BONUS			= 427
FameIds.WOK_FAME_1ST_VISITOR_IN_HOME		= 428
FameIds.WOK_FAME_10TH_VISITOR_IN_HOME		= 429
FameIds.WOK_FAME_VISIT_A_3D_APP				= 430
FameIds.WOK_FAME_VISIT_5_NEW_PLACES			= 431
FameIds.WOK_FAME_VISIT_10_NEW_PLACES		= 432
FameIds.WOK_FAME_ROTATED_CAMERA				= 433
FameIds.WOK_FAME_VISIT_DANCE_3D_APP			= 434
FameIds.WOK_FAME_VISIT_POKER_3D_APP			= 435
FameIds.WOK_FAME_VISIT_KACHING_3D_APP		= 436
FameIds.WOK_FAME_BUY_CREDITS				= 437
FameIds.WOK_FAME_RAVE_SOME_CLOTHING			= 438
FameIds.WOK_FAME_RAVE_A_PLACE				= 439
FameIds.WOK_FAME_SEND_A_BLAST				= 440
FameIds.WOK_FAME_BUY_AN_ANIMATION			= 441
FameIds.WOK_FAME_WELCOME_KANEVA				= 442
FameIds.WOK_FAME_DAILY_BONUS_3				= 443 -- changed from LOGIN_2_DAYS_IN_A_ROW
FameIds.WOK_FAME_LOGIN_5_DAYS_IN_A_ROW		= 444 -- deprecated
FameIds.WOK_FAME_ADD_10_FRIENDS				= 445
FameIds.WOK_FAME_ADD_20_FRIENDS				= 446
FameIds.WOK_FAME_SEND_BLAST_FROM_A_3D_APP	= 447
FameIds.WOK_FAME_USE_P2P_ANIMATION_10_TIMES	= 448
FameIds.WOK_FAME_PARTY_WITH_20_PEOPLE		= 449
FameIds.WOK_FAME_PARTY_WITH_50_PEOPLE		= 450
FameIds.WOK_FAME_20TH_VISITOR_IN_HOME		= 451
FameIds.WOK_FAME_50TH_VISITOR_IN_HOME		= 452
FameIds.WOK_FAME_100TH_VISITOR_IN_HOME		= 453
FameIds.WOK_FAME_250TH_VISITOR_IN_HOME		= 454
FameIds.WOK_FAME_500TH_VISITOR_IN_HOME		= 455
FameIds.WOK_FAME_1000TH_VISITOR_IN_HOME		= 456
FameIds.WOK_FAME_GO_TO_VIP_PLACE			= 457
FameIds.WOK_FAME_GO_TO_VIP_PLACE_5_TIMES	= 458
FameIds.WOK_FAME_YOUR_HOME_WAS_RAVED_10_TIMES = 459
FameIds.WOK_FAME_YOUR_HOME_WAS_RAVED_20_TIMES = 460
FameIds.WOK_FAME_YOUR_HOME_WAS_RAVED_50_TIMES = 461
FameIds.WOK_FAME_YOUR_HOME_WAS_RAVED_100_TIMES = 462
FameIds.WOK_FAME_YOUR_HOME_WAS_RAVED_250_TIMES = 463
FameIds.WOK_FAME_YOUR_HOME_WAS_RAVED_500_TIMES = 464
FameIds.WOK_FAME_YOUR_HOME_WAS_RAVED_1000_TIMES = 465
FameIds.WOK_FAME_VISIT_20_NEW_PLACES		= 466
FameIds.WOK_FAME_VISIT_50_NEW_PLACES		= 467
FameIds.WOK_FAME_VISIT_100_NEW_PLACES		= 468
FameIds.WOK_FAME_VISIT_250_NEW_PLACES		= 469
FameIds.WOK_FAME_VISIT_500_NEW_PLACES		= 470
FameIds.WOK_FAME_VISIT_1000_NEW_PLACES		= 471
FameIds.WOK_FAME_BUY_20TH_ITEM				= 472
FameIds.WOK_FAME_BUY_50TH_ITEM				= 473
FameIds.WOK_FAME_BUY_100TH_ITEM				= 474
FameIds.WOK_FAME_BUY_250TH_ITEM				= 475
FameIds.WOK_FAME_BUY_500TH_ITEM				= 476
FameIds.WOK_FAME_BUY_1000TH_ITEM			= 477
FameIds.WOK_FAME_UPLOAD_MEDIA_10_TIMES		= 478
FameIds.WOK_FAME_10_VISITORS_TO_COMMUNITY	= 479
FameIds.WOK_FAME_35_VISITORS_TO_COMMUNITY	= 480
FameIds.WOK_FAME_COMPLETED_TOUR_FIRST_TIME	= 481
FameIds.WOK_FAME_BUY_A_VIP_PASS				= 482
FameIds.WOK_FAME_TRAVEL_TO_10_DIFFERENT_ZONES= 483
FameIds.WOK_FAME_BUY_AN_ITEM				= 484
FameIds.WOK_FAME_IN_WORLD_FOR_15_MINUTES	= 485
FameIds.WOK_FAME_CLICK_MY_FAME_BUTTON		= 486
FameIds.WOK_FAME_SEND_FIRST_BLAST			= 487
FameIds.WOK_FAME_CREATOR_WELCOME			= 488
FameIds.WOK_FAME_MAKE_A_GAME				= 489
FameIds.WOK_FAME_DAILY_BONUS_1				= 490

--------- Tutorial Steps ----------
local tutPacketBase							= 491
local tutPacketMax							= 100
for tutIdx=1, tutPacketMax do
	FameIds["COMPLETED_WOK_TUTORIAL_STEP_" .. tutIdx] = tutPacketBase + tutIdx
end

--------- Odd Jobs ----------
FameIds.ODD_JOB_1                           = 46


RedeemPacketFilters = {}
RedeemPacketFilters.Default					= 0
RedeemPacketFilters.QueueForNextSpawn		= 1
