--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

PaymentMethodIds                       = {}
PaymentMethodIds.NONE                                		 =  0
PaymentMethodIds.REWARDS                                     =  1
PaymentMethodIds.CREDITS                                     =  2
PaymentMethodIds.ALWAYSREWARDS                               =  3
PaymentMethodIds.ALWAYSCREDITS                               =  4