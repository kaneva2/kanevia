--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Core item GLIDs
CoreItems = {}
CoreItems["EIPoseMale"] = 1
CoreItems["EIPoseFemale"] = 2
CoreItems["RestingPoseMale"] = 3
CoreItems["RestingPoseFemale"] = 4
CoreItems["NullObject"] = 5
CoreItems["PaperDollNPC"] = 6
CoreItems["MaleAvatar"] = 7
CoreItems["FemaleAvatar"] = 8
CoreItems["GemIndicator"] = 9
