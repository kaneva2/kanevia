--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------


-- from ChatType.h
ChatType.Talk            = 0 -- normal chat
ChatType.Group           = 1 -- group chat
ChatType.Clan            = 2 -- guild chat
ChatType.Private         = 3 -- just to you from one other
ChatType.Whisper         = 4 -- talk w/ limited radius
ChatType.Shout           = 21 -- talk w/ large radius
ChatType.GM              = 6 -- from GameMaster
ChatType.System          = 7 -- from System Admin
ChatType.External        = 8 -- from an external source
ChatType.ArenaMgr        = 9 -- system arean manager chat
ChatType.Team            = 10 -- restrict to same team 
ChatType.Race            = 11 -- restrict to same race
ChatType.Battle          = 12 -- battle related, etc. damage
ChatType.Emote           = 16 -- em command