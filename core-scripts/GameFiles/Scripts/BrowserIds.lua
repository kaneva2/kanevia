--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

BrowserIds                 = {}

-- event ids
BrowserIds.BROWSER_EVENT_NAME = "BrowserCallbackEvent"
BrowserIds.BROWSER_REPLY_EVENT_NAME = "BrowserReplyEvent"
BrowserIds.BROWSER_REQUEST_EVENT_NAME = "BrowserRequestEvent"

-- call back types
BrowserIds.BROWSER_KGP_LINK				= 1 -- sp = sz string that has everything after scheme: (scheme passed into create), wp = 0
BrowserIds.BROWSER_NAVIGATE_BEGIN		= 2 -- sp = "", wp = 0, can use to start throbber
BrowserIds.BROWSER_NAVIGATE_COMPLETE	= 3 -- sp = "", wp = 0, can use to stop throbber
BrowserIds.BROWSER_UPDATE_PROGRESS 		= 4 -- sp = "", wp = % progress
BrowserIds.BROWSER_STATUSTEXT_CHANGE 	= 5 -- sp = sz string for text, wp = 0
BrowserIds.BROWSER_LOCATION_CHANGE 		= 6 -- sp = sz string or redirected URL, wp = 0

-- types for the BROWSER_NAVIGATE type, add 100, just to avoid any confusion
BrowserIds.BROWSER_FORWARD				= 100
BrowserIds.BROWSER_BACK					= 101
BrowserIds.BROWSER_STOP					= 102
BrowserIds.BROWSER_RELOAD				= 103

-- reply event actions from server
BrowserIds.REPLY_REDIRECT 				= 1
BrowserIds.REPLY_CLOSE_WINDOW 			= 2

BrowserIds.BROWSER_VALIDATION		    = 1

function BrowserReplyEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- action, message, optional url
	action = KEP_EventDecodeNumber( event )
	message = KEP_EventDecodeString( event )

	if string.len(message) > 0 then
		if action == BrowserIds.REPLY_REDIRECT then
			local br = Dialog_GetStatic( gDialogHandle, "browser" )
			Static_SetText( br, message )
		else
			MessageBox( message )
		end
	end

	if action == BrowserIds.REPLY_CLOSE_WINDOW then
		DestroyMenu( gDialogHandle )
	end
end

function BrowserCallbackEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local type = KEP_EventDecodeNumber( event )
	if gDialogHandle == nil then
		return KEP.EPR_OK
	end
	local st = Dialog_GetStatic( gDialogHandle, "statusMsg" )
	local text = KEP_EventDecodeString( event )
	local num = KEP_EventDecodeNumber( event )

	if type == BrowserIds.BROWSER_STATUSTEXT_CHANGE then -- status text change
		Static_SetText( st, text )
	elseif type == BrowserIds.BROWSER_UPDATE_PROGRESS then -- % complete
		Static_SetText( st, tostring(num).."% complete" )
	elseif type == BrowserIds.BROWSER_KGP_LINK then
		if text == "cancel" then
			DestroyMenu( gDialogHandle )
		else
			e = KEP_EventCreate( BrowserIds.BROWSER_REQUEST_EVENT_NAME )
			KEP_EventEncodeString( e, text )
			KEP_EventAddToServer( e )
			KEP_EventQueue( e )
		end
	elseif type == BrowserIds.BROWSER_NAVIGATE_COMPLETE or
	type == BrowserIds.BROWSER_NAVIGATE_BEGIN or
	type == BrowserIds.BROWSER_NAVIGATE_LOCATION_CHANGE then
	-- do nothing
	else
		Static_SetText( st, "Huh?.  Type: "..tostring(type).." string "..st.." num "..num )
	end
	return KEP.EPR_OK
end

function BrowserHelperInitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( BrowserIds.BROWSER_REQUEST_EVENT_NAME, KEP.MED_PRIO )
	KEP_EventRegister( BrowserIds.BROWSER_REPLY_EVENT_NAME, KEP.MED_PRIO )
end

function BrowserHelperInitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserCallbackEventHandler", BrowserIds.BROWSER_EVENT_NAME, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserReplyEventHandler", BrowserIds.BROWSER_REPLY_EVENT_NAME, KEP.HIGH_PRIO )
end

function BrowserEnsureValidated( )
	KEP_EnsureValidated( BrowserIds.BROWSER_VALIDATION, "" )
end

