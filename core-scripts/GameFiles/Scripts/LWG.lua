--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

LWG = {}

-- Standard primitives (see PrimitiveType.h)
LWG.GT_SPHERE = 0
LWG.GT_BOX = 1
LWG.GT_CYLINDER = 2
LWG.GT_RECT_FRUSTUM = 3
LWG.GT_CIRC_FRUSTUM = 4

-- Geometry built with vertices
LWG.GT_TRIANGLES  = 0x100
LWG.GT_LINES      = 0x101
LWG.GT_LINESTRIP = 0x102

LWG.F_DRAW_SOLID	= 0x0000    -- 3D solid
LWG.F_DRAW_WIREFRAME= 0x0001    -- wireframe only
LWG.F_ALPHA_BLENDING= 0x0010    -- Alpha blending (translucency)
LWG.F_ALPHA_TESTING	= 0x0020    -- Alpha testing (on/off)
LWG.F_CULL_FRONT	= 0x0040
LWG.F_CULL_BACK		= 0x0080
LWG.F_ALWAY_ON_TOP	= 0x1000    -- Render on top of everything else
