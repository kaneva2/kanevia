#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

OTHER_HANGOUTS             = 0
ARCADE_HANGOUTS            = 1
ARTGALLERY_HANGOUTS        = 2
CASINO_HANGOUTS            = 3
DANCECLUB_HANGOUTS         = 4
FESTIVAL_HANGOUTS          = 5
GAMELOUNGE_HANGOUTS        = 6
MUSEUM_HANGOUTS            = 7
MUSICVENUE_HANGOUTS        = 8
PLACEOFWORSHIP_HANGOUTS    = 9
SPORTSBAR_HANGOUTS         = 10
STORE_HANGOUTS             = 11
THEATER_HANGOUTS            = 12
THEMEPARK_HANGOUTS         = 13
WEDDINGCHAPEL_HANGOUTS     = 14
NONE                       = 15
HOMES                      = 16
HANGOUTS                   = 17
MY_COMMUNITIES             = 18
PUBLIC_PLACES              = 19
APPS                       = 20