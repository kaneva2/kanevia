--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- global variable and "constants" for Python
KEP = {}

-- event priority
KEP.LOW_PRIO = 10
KEP.MED_PRIO = 100
KEP.HIGH_PRIO = 200

-- event security
KEP.ESEC_LOCAL_ONLY         = 0 		-- 0x0000   -- never matches anything, so can be from network
KEP.ESEC_ALL                = 65535		-- 0xffff
KEP.ESEC_CLIENT             = 1			-- 0x0001   -- game client
KEP.ESEC_SERVER             = 2			-- 0x0002   -- game server
KEP.ESEC_AI                 = 4			-- 0x0004   -- ai server
KEP.ESEC_REMOTE_SERVER      = 8			-- 0x0008   -- a remote server, this allows


-- from log4cplus headers
KEP.OFF_LOG_LEVEL     = 60000
KEP.FATAL_LOG_LEVEL   = 50000
KEP.ERROR_LOG_LEVEL   = 40000
KEP.WARN_LOG_LEVEL    = 30000
KEP.INFO_LOG_LEVEL    = 20000
KEP.DEBUG_LOG_LEVEL   = 10000
KEP.TRACE_LOG_LEVEL   = 0

-- our parent object, used when logging
KEP.parent = 0

-- the dispatcher
KEP.dispatcher = 0

-- passed in from server
KEP.debugLevel = 0

-- the script name (xxx.lua)
KEP.scriptName = "<scriptName>.lua"

-- return codes for event handling
KEP.EPR_OK				= 0	-- didn't change event
KEP.EPR_NOT_PROCESSED 	= 1 -- I don't want this event
KEP.EPR_CONSUMED		= 2 -- consumed event, don't let others process it

-- for getting C functions into the script
KEP.GET_SERVER_FN 		= "ServerGameFunctions"
KEP.GET_CLIENT_FN 		= "ClientGameFunctions"
KEP.GET_XWIN_FN 		= "XWinFunctions"
KEP.GET_MENU_FN 		= "MenuFunctions"
KEP.GET_ARENA_FN		= "ArenaFunctions"
KEP.GET_AI_FN           = "AIFunctions"
KEP.GET_PLAYER_FN		= "PlayerFunctions"
KEP.GET_SKILLDB_FN		= "SkillDBFunctions"
KEP.GET_KEPFILE_FN      = "KEPFileFunctions"
KEP.GET_KEPCONFIG_FN    = "KEPConfigFunctions"
KEP.GET_UGCIMPORT_FN	= "UGCImportFunctions"

-- instance types
KEP.CI_NOT_INSTANCED	= 0
KEP.CI_GROUP			= 1
KEP.CI_GUILD			= 2
KEP.CI_HOUSING   		= 3
KEP.CI_PERMANENT 		= 4
KEP.CI_ARENA			= 5
KEP.CI_CHANNEL			= 6

KEP.ArenaReady			= 0
KEP.ArenaWaiting 		= 1
KEP.ArenaTimedOut		= 2

KEP.INVALID_ZONE_INDEX  = 4293918720

-- from TextEvent.h
ChatType = {}
ChatType.Talk 			= 0	--
ChatType.Group 			= 1	--
ChatType.Clan 			= 2	--
ChatType.Private		= 3	-- just to you from one other
ChatType.Whisper 		= 4	-- talk w/ limited radius
ChatType.Shout 			= 5	-- talk w/ large radius
ChatType.GM 			= 6	-- from GameMaster
ChatType.System 		= 7	-- from System Admin
ChatType.External		= 8	-- from an external source
ChatType.ArenaMgr		= 9	-- related to an arena
ChatType.Team           = 10 -- restrict to same team
ChatType.Race           = 11 -- restrict to same race
ChatType.Battle         = 12 -- battle related, etc. damage
ChatType.S2S			= 15 -- server to server relay message
ChatType.Emote          = 16 -- /em command
ChatType.ScriptObject	= 17 -- from a script object

-- GetSet.AddNewMember types
KEP.amBool              = 1
KEP.amInt               = 8
KEP.amLong              = 10
KEP.amDouble            = 14
KEP.amString            = 15

-- add member flags
KEP.amfTransient            = 33554432  -- 0x02000000 -- this item is not serialized
KEP.amfReplicate            = 134217728 -- 0x08000000 -- transfer between server and client, for dynamic
KEP.amfSaveAcrossSpawn      = 268435456 -- 0x10000000 -- save across a spawn for dynamic player data
KEP.amfMask                 = 503316480 -- 0x1e000000

-- add member/find return values
KEP.amrDupeWrongType        = 4294967294 -- 0xfffffffe
KEP.amrBadType              = 4294967293 -- 0xfffffffd
KEP.amrNotFound             = 4294967295 -- 0xffffffff

-- type of damage
KEP.MISSILE				= 0
KEP.MELEE				= 1
KEP.EXPLOSION 			= 2

-- Network ID of the Server
KEP.SERVER_NETID        = 1

--triggers
KEP.TriggerEnter        = 1
KEP.TriggerExit         = 2

-- for registering filtered events
KEP.FILTER_IS_MASK      	= 1
KEP.FILTER_MATCH        	= 0
KEP.NO_FILTER           	= 0
KEP.NO_OBJID_FILTER     	= 0

-- for GotoPlaceEvent
KEP.GOTOPLACE_EVENT_NAME   = "GotoPlaceEvent"
KEP.GOTO_ZONE				= 1
KEP.GOTO_PLAYER				= 2
KEP.GOTO_PLAYERS_APT		= 3
KEP.GOTO_ZONE_EX			= 4
KEP.GOTO_TUTORIAL           = 5
KEP.GOTO_SERVER             = 6
KEP.GOTO_GAME               = 7
KEP.GOTO_URL                = 8


-- for pending spawn event
KEP.PENDINGSPAWN_EVENT_NAME	= "PendingSpawnEvent"

-- For keyboard input
Keyboard = {A = 65, B = 66, C = 67, D = 68, E = 69, F = 70, G = 71, H = 72, I = 73, J = 74, K = 75, L = 76, M = 77, N = 78, O = 79, P = 80, Q = 81, R = 82, S = 83, T = 84, U = 85, V = 86, W = 87, X = 88, Y = 89, Z = 90}
Keyboard.ESC = 27
Keyboard.SPACEBAR = 32
Keyboard.PAGE_UP = 33
Keyboard.PAGE_DOWN = 34
Keyboard.DEL = 46

-- for KDP
-- The world object id for the dance floor. This ID is the first kaneva dance floor
KEP.KDP1ID = 369
KEP.KDP2ID = 370
KEP.KDP3ID = 371
KEP.LARGE_DISCO_FLOOR = 883
KEP.MEDIUM_DISCO_FLOOR = 1101
KEP.SMALL_DISCO_FLOOR = 1102
KEP.SNOWGLOBE_DISCO_FLOOR = 1321
KEP.ANIMATING_FLOOR_32 = 2474
KEP.ANIMATING_FLOOR_48 = 2473
KEP.ANIMATING_FLOOR_64 = 2475
KEP.KDP1_EVENT = "DDREvent"
KEP.KDP1_MENU_EVENT = "DDRMenuEvent"
KEP.FILTER_START_GAME = 5
KEP.FILTER_POSTED_SCORE = 4
KEP.FILTER_END_GAME = 6
KEP.FILTER_CLOSE_DIALOG = 7
KEP.FILTER_GAME_OVER_DIALOG = 8
KEP.FILTER_READY_TO_PLAY = 9
KEP.FILTER_POST_PLAYERS_SCORES = 10
KEP.FILTER_ACCESS_DENIED = 13
KEP.FILTER_PAUSE_REQUEST = 16
KEP.FILTER_PAUSE_RESPONSE = 17

-- YES/NO box answer filters
KEP.FILTER_KDP_ANSWER_PLAY = 5

-- World Fame Event and Filters
KEP.WORLD_FAME_EVENT = "WorldFameEvent"
KEP.FILTER_WF_TUTORIAL = 1
KEP.FILTER_WF_PLACEOBJ = 2
KEP.FILTER_WF_CLOTHES = 3
KEP.FILTER_WF_PATTERN = 4
KEP.FILTER_WF_MEDIA = 5
KEP.FILTER_WF_HANGOUT = 6
KEP.FILTER_WF_ZONE = 7
KEP.FILTER_WF_DANCELEVEL = 8
KEP.FILTER_WF_LOGIN = 9

-- TryOn Inventory Expire Event
KEP.FILTER_TRYONINVENTORY = 1
KEP.FILTER_TRYONINVENTORY_EXPIRED = 2

KEP.TRYON_INVENTORY_EXPIRE_EVENT = "TryOnInventoryExpire"
KEP.FILTER_TRYON_EXPIRE = 1

-- Generic Message Box
KEP.GENERIC_INFO_BOX_EVENT = "GenericInfoBoxEvent"
KEP.FILTER_GENERIC_INFO_BOX_MSG1 = 1;

-- use types for player use item event
KEP.USE_TYPE_NONE			= 0
KEP.USE_TYPE_HEALING		= 1
KEP.USE_TYPE_AMMO			= 2
KEP.USE_TYPE_SKILL			= 3
KEP.USE_TYPE_MENU			= 4
KEP.USE_TYPE_COMBINE		= 5
KEP.USE_TYPE_PLACE_HOUSE	= 6
KEP.USE_TYPE_INSTALL_HOUSE	= 7
KEP.USE_TYPE_SUMMON_CREATURE= 8
KEP.USE_TYPE_OLD_SCRIPT		= 9
KEP.USE_TYPE_ADD_DYN_OBJ	= 10
KEP.USE_TYPE_ADD_ATTACH_OBJ	= 11
KEP.USE_TYPE_MOUNT			= 12
KEP.USE_TYPE_EQUIP			= 13
KEP.USE_TYPE_ANIMATION      = 200
KEP.USE_TYPE_P2P_ANIMATION  = 201
KEP.USE_TYPE_NPC            = 202
KEP.USE_TYPE_DEED           = 203
KEP.USE_TYPE_QUEST          = 204
KEP.USE_TYPE_PREMIUM		= 205
KEP.USE_TYPE_PARTICLE		= 206
KEP.USE_TYPE_SOUND			= 207
KEP.USE_TYPE_BUNDLE			= 208
KEP.USE_TYPE_CUSTOM_DEED	= 209
KEP.USE_TYPE_ACTION_ITEM	= 210
KEP.USE_TYPE_GAME_ITEM		= 211

KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME = "ServerDynObjRequestEvent"
KEP.SERVERDYNOBJ_REPLY_EVENT_NAME = "ServerDynObjReplyEvent"

-- UGC GLID Constants
KEP.GLID_UGC_BASE = 3000000

-- constants for ControlDynamicObjectEvent 
KEP.SOUND_CONFIG	= tonumber("0010", 16)
KEP.SOUND_START		= tonumber("0011", 16)
KEP.SOUND_STOP		= tonumber("0012", 16)

-- External notification types (see NotifyExternalEventListener)
KEP.EN_GENERIC = 0
KEP.EN_PING = 1
KEP.EN_ACTIVATED = 2
KEP.EN_DEACTIVATED = 3
KEP.EN_FRIENDS = 4

-- Game Id Enumeration (development, preview, production)
KEP.GID_DEV = 5316
KEP.GID_DEV_STR = "5316"
KEP.GID_PRE = 3298
KEP.GID_PRE_STR = "3298"
KEP.GID_PRO = 3296
KEP.GID_PRO_STR = "3296"
KEP.GID_RC = 5310
KEP.GID_RC_STR = "5310"

function KEP_PreInitialize(dispatcher, handler, debugLevel, scriptName)
end

function KEP_PostInitialize(dispatcher, handler, debugLevel, scriptName)
end

