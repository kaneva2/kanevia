--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

UnlockableType = {}
UnlockableType.Emote = 1
UnlockableType.Feature = 2

FeatureIds = {}
FeatureIds.Emotes = 1
FeatureIds.BuildMode = 2
FeatureIds.UGC = 3
FeatureIds.HostEvents = 4
FeatureIds.Trade = 5
FeatureIds.KmailNonFriends = 6

UnlockableRequestFilter = {}
UnlockableRequestFilter.FeatureRequest  = 1
UnlockableRequestFilter.FeatureResponse = 2
UnlockableRequestFilter.EmotesRequest   = 3
UnlockableRequestFilter.EmotesResponse  = 4