--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

GameGlobals = {}

--actors
GameGlobals.EDB_MALE          = 3
GameGlobals.EDB_FEMALE        = 4

--zones
GameGlobals.ZONE_MALL         = 0
GameGlobals.ZONE_LOGINMENU    = 1
GameGlobals.ZONE_CASINO       = 2
GameGlobals.ZONE_THEATRE      = 3
GameGlobals.ZONE_APT_01       = 4  --1bdr_big
GameGlobals.ZONE_APT_02       = 5  --studio
GameGlobals.ZONE_APT_03       = 6  --1bdr_mid
GameGlobals.ZONE_APT_LOFT     = 7  --loft apt
GameGlobals.ZONE_APT_04       = 8  --L shaped
GameGlobals.ZONE_DANCEBAR     = 9  --dance pub
GameGlobals.ZONE_PENTHOUSE    = 10 --penthouse
GameGlobals.ZONE_HANGAR       = 11
GameGlobals.ZONE_NIGHTCLUB    = 12
GameGlobals.ZONE_CONFERENCE   = 13
GameGlobals.ZONE_BBTHEATER    = 14
GameGlobals.ZONE_CAFE_SMALL   = 15
GameGlobals.ZONE_CAFE_LARGE   = 16 --kaneva cafe
GameGlobals.ZONE_APT_32x64x64 = 19
GameGlobals.ZONE_ARENALOBBY   = 38

--use values
GameGlobals.UV_PORTAL_CASINO    = 100
GameGlobals.UV_PORTAL_THEATRE   = 101
GameGlobals.UV_PORTAL_PENTHOUSE = 102
GameGlobals.UV_APT_STUDIO       = 103
GameGlobals.UV_APT_1BDR         = 104
GameGlobals.UV_APT_FLAT         = 105
GameGlobals.UV_APT_LARGE1BDR    = 106
GameGlobals.UV_APT_LOFT         = 107
GameGlobals.UV_APT_PENTHOUSE    = 108
GameGlobals.UV_APT_32x64x64     = 109

--items GLID
GameGlobals.PORTAL_CASINO     = 0
GameGlobals.PORTAL_THEATRE    = 1
GameGlobals.PORTAL_PENTHOUSE  = 2 

--apartments GLID
GameGlobals.ITEM_APT_STUDIO    = 533
GameGlobals.ITEM_APT_1BDR      = 534
GameGlobals.ITEM_APT_FLAT      = 535
GameGlobals.ITEM_APT_LARGE1BDR = 536
GameGlobals.ITEM_APT_LOFT      = 537
GameGlobals.ITEM_APT_PENTHOUSE = 538
GameGlobals.ITEM_APT_32x64x64  = 1055

--weapons

--arena weapons
GameGlobals.FEMALE_HORNET      = 63
GameGlobals.MALE_HORNET        = 61

--ammo
GameGlobals.HORNET_AMMO        = 62

GameGlobals.CONFIG_FILE			= "WOKConfig.xml"
GameGlobals.CONFIG_ROOT			= "WOKConfig"


-- for asking for images
GameGlobals.THUMBNAIL			= 1
GameGlobals.FULLSIZE			= 0

-- types of thumbnails
GameGlobals.CUSTOM_TEXTURE = 1
GameGlobals.FRIEND_PICTURE = 2
GameGlobals.GIFT_PICTURE   = 3

-- inventory types
GameGlobals.IT_NORMAL        = 0x100											-- bought w/ money
GameGlobals.IT_GIFT          = 0x200											-- given as gift
GameGlobals.IT_UNLIMITED     = 0x400											-- inventory item with no quantity limit (smart objects)
GameGlobals.IT_INFINTE       = 0x800											-- inventory item with no quantity limit (new 2048)

GameGlobals.IT_BOTH          = GameGlobals.IT_NORMAL + GameGlobals.IT_GIFT		-- credits or rewards
GameGlobals.IT_ALL           = GameGlobals.IT_BOTH + GameGlobals.IT_UNLIMITED + GameGlobals.IT_INFINTE	-- all types

--cache some data so we dont have to query for it every time
PlayerInfo = {}
PlayerInfo.name = ""
PlayerInfo.userID = 0

-- web fiters for GetBrowserPage
WF = {}
WF.NOMANSLAND					= 0		-- handlers always discard BrowserPageReadyEvent if filter is 0 (same as WF.IGNORE???)
WF.ACCEPT_FRIEND				= 1
WF.ACCEPT_GIFT					= 2
WF.ADDFRIEND					= 3
WF.COOL_PLACES					= 4
WF.DELETE_MSG					= 5
WF.FRIENDS						= 6
WF.GIFT_CATALOG					= 7
WF.IGNORE						= 8
WF.MESSAGES						= 9
WF.PLAYER_PROFILE				= 10
WF.PLAYLIST						= 11
WF.RAVE							= 12
WF.REJECT_FRIEND				= 13
WF.REJECT_GIFT					= 14
WF.TARGETED						= 15
WF.TEXTURE_LIST					= 16
WF.MESSAGE_COUNTS				= 17
WF.REPORT						= 18
WF.TRADE						= 19
WF.OBJECTINFO					= 20
WF.BLOCK                        = 21
WF.VENDOR                       = 22
WF.ZONE_ACCESS                  = 23
WF.LOC_HUD                      = 24
WF.INVENTORY                    = 25
WF.COVER_CHARGE					= 26
WF.KDP                          = 27
WF.WORLD_FAME                   = 28
WF.ZONE_LOADER                  = 29
WF.GET_USER_FAME				= 30
WF.CONTEXT_FAME					= 31
WF.GET_LEVEL_AWARDS				= 32
WF.FLASH_GAME                   = 33
WF.EVENT_SCHEDULE_LOCATION      = 34
WF.EVENT_LIST			        = 35
WF.EVENT_LIST_LOCATION			= 36
WF.EVENT_CREATE					= 37
WF.VENDOR_INVENTORY				= 38
WF.P2P_ANIM                     = 39
WF.APT_SELECT                   = 40
WF.GET_DYNAMIC_OBJECT_INFO      = 41
WF.BUY_DYNAMIC_OBJECT           = 42
WF.GET_PLAYERS_ARMED_ITEMS      = 43
WF.PLAYER_INVENTORY             = 44
WF.BANK_INVENTORY               = 45
WF.TRADE_ITEMS                  = 46
WF.GET_USER_PROFILE             = 47
WF.PARSE_ASSET_URL				= 48
WF.SINCE_LAST_LOGIN             = 49
WF.IMPORT_MESH_RESIZING			= 50	-- added for YesNoBox for mesh resizing in UGCImport.lua
WF.INSTANT_PLAYBACK				= 51	-- instant playback at medium drag-n-drop
WF.CONFIRM_MESH_SCALING			= 52	-- confirm mesh scaling
WF.DEFAULT_HANDLER				= 53 
WF.GET_BALANCES					= 54	-- getBalances()
WF.MINI_PROFILE					= 55
WF.BUY_RAVE						= 56
WF.PLAYER_PREFERENCES			= 57
WF.PLAYLISTS					= 58
WF.PLAYLIST_PATTERNS			= 59
WF.USER_ID						= 60
WF.LOC_RAVE						= 61
WF.INSTANTBUY_COPYITEMS			= 62
WF.GAMES_LIST					= 63
WF.TRY_ON_ITEM					= 64
WF.GET_RAVES					= 65
WF.HUD_USER_ID					= 66
WF.HUD_BALANCES					= 67
WF.CONFIRM_GROUP_OVERWRITE		= 68
WF.PREMIUM_ITEMS				= 69
WF.PREMIUM_ITEM_INFO			= 70
WF.PREMIUM_ITEM_PURCHASE		= 71
WF.UGC_GLOBAL_OPTIONS			= 72
WF.GET_USER						= 73
WF.CONFIRM_FRIEND_REQUEST		= 74
WF.CONTEXT_FRIEND				= 75
WF.GET_PROFILE_INFO				= 76
WF.CONTEXT_USERID				= 77
WF.RAVE_INFO					= 78
WF.PLACE_INFO					= 79
WF.RCLICK_PLACE_RAVE			= 80
WF.ITEM_ANIMATIONS			    = 81
WF.IN_WORLD_INVITE				= 82
WF.DASH_MOST_VISITED			= 83
WF.DASH_RANDOM_FRIENDS			= 84
WF.DASH_BALANCES				= 85
WF.DASH_COUNTS					= 86
WF.BROWSE_PLACES_SEARCH			= 87
WF.BROWSE_PLACES_USER_PROFILE	= 88
WF.MY_PLACES_GET_PICS			= 89
WF.FAME_GETALLLEVELS			= 90
WF.FAME_GETALLPACKETS			= 91
WF.FAME_GETALLLEVELAWARDS		= 92
WF.FAME_GETNUDGECHECKLIST		= 93
WF.REDEEM_PACKET				= 94
WF.FAME_GETPACKETSFORUSER		= 95
WF.FAME_GETUSERID				= 96
WF.MYFAME_GETPROFILE			= 97
WF.MYFAME_GETCOUNTS				= 98
WF.MYFAME_USERID				= 99
WF.MYFAME_GETBALANCE			= 100
WF.MYFAME_GET3DAPPINFO			= 101
WF.MYFAME_GETVISITED3DAPPS		= 102
WF.FAME_REPORTFAMEEVENTNOTIFIED	= 103
WF.FAME_GETALLNEWFAMEEVENTS		= 104
WF.FAME_GETBADGES				= 105
WF.FRIEND_IMAGE_URL				= 106
WF.FRIEND_THUMBNAIL_URL			= 107
WF.MY3DAPP_RETURN				= 108
WF.GET_MY_3DAPPS				= 109
WF.MENUEDITOR_EXIT_FILTER		= 110
WF.MENUEDITOR_CLOSE_FILTER		= 111
WF.MENUEDITOR_RELOAD_FILTER		= 112
WF.MENUEDITOR_NEW_FILTER		= 113
WF.MENUEDITOR_OPEN_FILTER		= 114
WF.PARTICLE_INVENTORY			= 115
WF.MENUEDITOR_BROWSE_FILTER		= 116
WF.MENUBROWSER_DELETE_FILTER	= 117
WF.OBJECT_BROWSER_INFO			= 118
WF.PARTICLE_BROWSER_INFO		= 119
WF.APP_ASSET_NAMECONFLICT		= 120
WF.DEED_ZONE_ITEMS_INFO			= 121
WF.APP_YES_NO_ANSWER1			= 122
WF.APP_YES_NO_ANSWER2			= 123
WF.APP_YES_NO_ANSWER3			= 124
WF.APP_YES_NO_ANSWER4			= 125
WF.APP_YES_NO_ANSWER5			= 126
WF.APP_YES_NO_ANSWER6			= 127
WF.APP_YES_NO_ANSWER7			= 128
WF.APP_YES_NO_ANSWER8			= 129
WF.APP_YES_NO_ANSWER9			= 130
WF.APP_YES_NO_ANSWER10			= 131
WF.APP_INFO						= 134
WF.MENUEDITOR_MISSING_SCRIPT_FILTER	= 135
WF.MENUSCRIPTBROWSER_DELETE_FILTER = 136
WF.DEED_INFO					= 137
WF.DASH_WORLD_COUNTS            = 138
WF.DASH_FRIEND_COUNTS           = 139
WF.DASH_EVENT_COUNTS            = 140
WF.DASH_MESSAGE_COUNTS          = 141
WF.HUD_WORLD_COUNTS             = 142
WF.HUD_FRIEND_COUNTS            = 143
WF.HUD_EVENT_COUNTS             = 144
WF.HUD_MESSAGE_COUNTS           = 145
WF.DAILY_BONUS_DATA             = 146
WF.PLAYER_INVENTORY_EMOTES		= 147
WF.DAILY_BONUS_RESET            = 148
WF.FAME_GETPACKETFORUSER        = 149
WF.SET_ZONE_ACCESS				= 150
WF.ITEM_CATEGORIES				= 151
WF.ACTION_ITEMS_PARAM			= 152
WF.ACTION_ITEMS_LIST			= 153
WF.PLAYER_EMOTES				= 154
WF.SMART_OBJ_QUANTITY			= 155
WF.ANIMATIONS_LIST				= 156
WF.PLAYER_ANIMATIONS			= 157
WF.PLAYER_EQUIP_INVENTORY		= 158
WF.COPIED_QUANTITIES			= 159
WF.CREATE_WORLD					= 160
WF.INVENTORY_USERID				= 161
WF.PROGRESS_METRICS             = 162
WF.TRACKING_REQ_GAME            = 163
WF.TRACKING_REQUEST_GAME_ACCEPT = 164
WF.TEMPLATE_LIST				= 165
WF.DECLINE_EVENT				= 166
WF.ACCEPT_EVENT					= 167
WF.TRACKING_EVENT_ACCEPT		= 168
WF.LAUNCH_EVENT_COUNT			= 169
WF.PREMIUM_EVENT_LIST			= 170
WF.NOTIFY_SPAWNING_IN_WORLD     = 171
WF.RECENTLY_PLACED_QUANTITIES	= 172
WF.STORAGE_INVENTORY			= 173
WF.STORAGE_QUANTITY				= 174
WF.PLAYER_INVENTORY_TRADE       = 175
WF.BUILD_SETTINGS_GET_SIGNUP	= 176
WF.ZONE_LOADER_BUILD_SETTINGS_USER_ID = 177
WF.GET_MY_3DAPPS_PROMPT			= 178
WF.GET_SCRIPT_BUNDLE			= 179
WF.DASHBOARD_GET_HISTORY		= 180
WF.RIGHTCLICK_PROFILE			= 181
WF.PLAYLIST_GROUPS				= 182
WF.ADD_TO_PLAYLIST				= 183
WF.MY_PLAYLISTS					= 184
WF.VALIDATE_STPURL				= 185
WF.TRAVEL_NOTICE_REQUEST_ACCESS = 186
WF.HUDPROFILE_INFO				= 187
WF.UNIQUE_GAME_ITEM_CHECK	    = 188
WF.COPY_GAME_ITEM_TO_INVENTORY	= 189
WF.REQUEST_GAME_ITEM_GLOBAL_CONFIG = 190
WF.DONATION_INFO				= 191
WF.PURCHASE_SUCCESS				= 192
WF.GAME_ITEM_METRICS			= 193
WF.DEFAULT_HOME_TEMPLATE_INFO	= 194
WF.DASH_AVAIL_CREDIT_REDEMPTIONS = 195
WF.MEMBER_ALTS					= 196
WF.CONFIRM_SYSTEM_SERVICE		= 197
WF.SEND_BUG_REPORT				= 198
WF.EXCHANGE_INFO				= 199
WF.GEM_REWARD					= 200
WF.GEM_AMOUNTS					= 201
WF.GEM_CONVERT					= 202
WF.CURRENCY_TRADING				= 203
WF.TOP_WORLDS_SEARCH			= 204
WF.REWARD_PURCHASE				= 205
WF.CREDIT_PURCHASE				= 206
WF.CONTEXTUAL_HELP				= 207
WF.CONTEXTUAL_HELP_RESULT		= 208
WF.FLAG_RAVE					= 209
WF.FLAG_VISIT					= 210
WF.FLAG_TEAM_UPDATE				= 211
WF.FLAG_TEAM_GET				= 212
WF.LEADERBOARD_GET 				= 213
WF.LEADERBOARD_GET_PRE			= 214
WF.LB_REWARD_STATUS_GET			= 215
WF.LB_REWARD_STATUS_UPDATE		= 216
WF.FLAG_NAME_UPDATE				= 217
WF.IMAGE_LIST					= 218
WF.SAT_PACKETREDEEMEDCHECK		= 219
WF.SAT_TESTUSERCREATED			= 220
WF.GEM_VENDOR_SPEND				= 221
WF.SAT_WEBLOGINSETUP			= 222
WF.LINKED_ZONES					= 223
WF.LINKED_ZONES_ADD				= 224
WF.LINKED_ZONES_DELETE			= 225
WF.SAT_CHECKWORLDTEMPLATEID		= 226
WF.GET_ZONE_ACCESS				= 227
WF.GET_ZONE_TEMPLATE 			= 228
WF.CREATE_WORLD_STATUS			= 229
WF.SAT_TRYONURL					= 230
WF.GET_URL 						= 231
-- YesNo Filter for rezone from HUD
HUD = {}
HUD.REZONE_MSG = "The world you are in has finished importing.  Do you want to reload the world?"
HUD.REZONE_FILTER = 137

-- messages for actions that generate message boxes
RM = {}
RM.FRIEND_REQUEST               = "Friend request sent to "
RM.REMOVE_FRIEND                = " was removed from your friends list."
RM.ALREADY_FRIENDS              = "You are already friends with "
RM.ADD_IGNORE                   = "You will now ignore "
RM.REMOVE_IGNORE                = "You will no longer ignore "
RM.ALREADY_IGNORE               = "You are already ignoring "
RM.RAVE                         = "You raved "
RM.ALREADY_RAVED                = "You have already raved "
RM.FRIEND_REQUEST_SENT			= "You have already sent a request to "

-- Invite default messages
IVTM = {}
IVTM.DEFAULT_MESSAGE						= "Come check this out!"
IVTM.EVENT_REMINDER_DEFAULT_MESSAGE			= "Your event is starting now!"

-- Filters for menus that are lightboxed so the user is forced to interact with them
LBOXM = {}
LBOXM.DASHBOARD = 0
LBOXM.BROWSE_PLACES = 1

GameGlobals.ZONE_INDEX_MALL 	= 1073741824
GameGlobals.ZONE_INDEX_COMPOUND = 1073741859
GameGlobals.ZONE_INDEX_DANCE    = 0
GameGlobals.ZONE_INDEX_KANEVACITY    = 1073741857
GameGlobals.ZONE_INDEX_PLAZA         = 1073741852
GameGlobals.ZONE_INDEX_UNDERGROUND   = 1073741850
GameGlobals.ZONE_INDEX_HELPCENTER    = 1073741855
GameGlobals.ZONE_INDEX_TBSHQ	     = 1073741853
GameGlobals.ZONE_INDEX_TNTBACKLOT    = 1073741847
GameGlobals.ZONE_INDEX_FAMILYGUY     = 1073741848
GameGlobals.ZONE_INDEX_ARENALOBBY    = 1073741862
GameGlobals.ZONE_INDEX_EMPLOYMENT	 = 1073741865

GameGlobals.FEATURED_CHANNEL = "WOKMedia"

GameGlobals.INWORLD_PLAYLIST_NAME = "In-World Playlist"
GameGlobals.UNFILED_MEDIA_PLAYLIST_NAME= "Unfiled Dropbox Media"


-- Client-side Tutorial Filters (must mirror TutorialTipManager.py)
--------------------------------------
GameGlobals.TUTMENU_WORLDMAP    = 2 --0x0002
GameGlobals.TUTMENU_KACHING1    = 4 --0x0004
GameGlobals.TUTMENU_DANCE    	= 8 --0x0008

GameGlobals.STARTER_HOME_ID	= 109  --This is (unfortunately) the index of the spawn point config in the editor

GameGlobals.CHARACTER_CREATION_ZI = 49 -- zone index for the character creation zone

-- Contextual Help Constants 
ContextualHelp = {}
ContextualHelp.STATUS_ENERGY			= 723
ContextualHelp.STATUS_HEALTH 			= 722
ContextualHelp.STATUS_AREA 				= 721
ContextualHelp.SHOW_TOOLBELT			= 720
ContextualHelp.TOOLBELT_VEHICLE			= 719
ContextualHelp.TOOLBELT_PET				= 718
ContextualHelp.TOOLBELT_PLACEABLE		= 717
ContextualHelp.TOOLBELT_CONSUMABLE		= 716
ContextualHelp.TOOLBELT_TOOL			= 715
ContextualHelp.TOOLBELT_WEAPON			= 714
ContextualHelp.ADD_FRIEND				= 713
ContextualHelp.RAVE 					= 712
ContextualHelp.EMOTE 					= 711
ContextualHelp.CLOSE_BACKPACK_X			= 710
ContextualHelp.USE_TOOLBELT 			= 709
ContextualHelp.SELECT_TOOLBELT			= 708
ContextualHelp.CLOSE_BACKPACK			= 707
ContextualHelp.TOOLBELT_OVERVIEW		= 706
ContextualHelp.OBJECT_ROLLOVER			= 705
ContextualHelp.EMPTY_BACKPACK			= 704
--ContextualHelp.OPEN_BACKPACK			= 702
ContextualHelp.ADD_BACKPACK				= 701
ContextualHelp.LOOT_CLICKED				= 700

-- Contextual Help Formats
ContextualHelpFormat = {}
ContextualHelpFormat.NO_ARROW			= 0
ContextualHelpFormat.LEFT_ARROW			= 1
ContextualHelpFormat.RIGHT_ARROW		= 2
ContextualHelpFormat.TOP_ARROW			= 3
ContextualHelpFormat.BOTTOM_ARROW		= 4
ContextualHelpFormat.LEFT_NO_ARROW		= 5
ContextualHelpFormat.RIGHT_NO_ARROW		= 6
ContextualHelpFormat.TOP_NO_ARROW		= 7
ContextualHelpFormat.BOTTOM_NO_ARROW	= 8

dofile( "..\\Scripts\\urls.lua" )
