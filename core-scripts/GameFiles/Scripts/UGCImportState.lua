--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- from UGC_Global.h

UGCIS = {}
UGCIS.NONE = 0
UGCIS.INIT = 1
UGCIS.PREVIEWING = 2
UGCIS.PREVIEWED = 3
UGCIS.PREVIEWFAILED = 4
UGCIS.IMPORTING = 5
UGCIS.IMPORTED = 6
UGCIS.IMPORTFAILED = 7
UGCIS.PROCESSING = 8
UGCIS.PROCESSED = 9
UGCIS.PROCESSINGFAILED = 10
UGCIS.UPLOADING = 11
UGCIS.UPLOADED = 12
UGCIS.UPLOADFAILED = 13
UGCIS.DESTROYING = 14
UGCIS.DESTROYED = 15
UGCIS.CANCELED = 16
UGCIS.CONVERTING = 17
UGCIS.CONVERTED = 18
UGCIS.CONVERSIONFAILED = 19
UGCIS.STARTING = 20
UGCIS.COMMITTING = 21
UGCIS.ANY = -1
