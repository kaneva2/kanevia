#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

# from CCameraClass.h

Add		= 0
Remove          = 1
Update          = 2
Bind            = 3
