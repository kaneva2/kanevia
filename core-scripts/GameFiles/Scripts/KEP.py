#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

# global variable and "constants" for Python

# currency types
CURRENCY_USING_CREDITS		= 0
CURRENCY_USING_GIFT_CREDITS	= 1

CASH_TT_BOUGHT_CREDITS          = 1
CASH_TT_TRADE                   = 2
CASH_TT_BOUGHT_ITEM             = 3
CASH_TT_SOLD_ITEM               = 4
CASH_TT_LOOT                    = 5
CASH_TT_MISC                    = 6
CASH_TT_REFUND                  = 7
CASH_TT_OPENING_BALANCE         = 8
CASH_TT_BOUGHT_HOUSE_ITEM       = 9
CASH_TT_QUEST                   = 10
CASH_TT_BANKING                 = 11
CASH_TT_HOUSE_BANKING           = 12
CASH_TT_BOUGHT_GIFT             = 13
CASH_TT_ROYALITY                = 14
CASH_TT_SPECIAL                 = 15
CASH_TT_COVER_CHARGE            = 16
CASH_TT_COMMISSION              = 17
CASH_TT_ITEM_PICKUP             = 18

# event priority
LOW_PRIO = 10
MED_PRIO = 100
HIGH_PRIO = 200

# event security
ESEC_LOCAL_ONLY         = 0x0000   # never matches anything, so can be from network
ESEC_ALL                = 0xffff
ESEC_CLIENT             = 0x0001   # game client
ESEC_SERVER             = 0x0002   # game server
ESEC_AI                 = 0x0004   # ai server
ESEC_REMOTE_SERVER      = 0x0008   # a remote server, this allows 


# from log4cplus headers
OFF_LOG_LEVEL     = 60000
FATAL_LOG_LEVEL   = 50000
ERROR_LOG_LEVEL   = 40000
WARN_LOG_LEVEL    = 30000
INFO_LOG_LEVEL    = 20000
DEBUG_LOG_LEVEL   = 10000
TRACE_LOG_LEVEL   = 0

# our parent object, used when logging 
parent = 0

# the dispatcher 
dispatcher = 0

# passed in from server
debugLevel = 0

# return codes for event handling
EPR_OK              = 0 # didn't change event
EPR_NOT_PROCESSED   = 1 # I don't want this event
EPR_CONSUMED        = 2 # consumed event, don't let others process it

SERVER_NETID        = 1 # from dplay header, reserved DPNID

# for getting C functions into the script
GET_SERVER_FN       = "ServerGameFunctions"
GET_CLIENT_FN       = "ClientGameFunctions"
GET_XWIN_FN         = "XWinFunctions"
GET_MENU_FN         = "MenuFunctions"
GET_ARENA_FN        = "ArenaFunctions"
GET_AI_FN           = "AIFunctions"
GET_PLAYER_FN       = "PlayerFunctions"
GET_SKILLDB_FN      = "SkillDBFunctions"
GET_KEPFILE_FN      = "KEPFileFunctions"
GET_KEPCONFIG_FN    = "KEPConfigFunctions"
GET_UGCIMPORT_FN    = "UGCImportFunctions"

# instance types
CI_NOT_INSTANCED    = 0
CI_GROUP            = 1
CI_GUILD            = 2
CI_HOUSING          = 3
CI_PERMANENT        = 4
CI_ARENA            = 5
CI_CHANNEL          = 6

ArenaReady          = 0
ArenaWaiting        = 1
ArenaTimedOut       = 2

TriggerEnter        = 1
TriggerExit         = 2

# type of damage
MISSILE             = 0 
MELEE               = 1
EXPLOSION           = 2

# for registering filtered events
FILTER_IS_MASK      = 1
FILTER_MATCH        = 0
NO_FILTER           = 0
NO_OBJID_FILTER     = 0

INVALID_ZONE_INDEX  = 4095

# columns indexes for the playlist rows
OBJ_PLACEMENT_ID_COL = 0
ASSET_ID_COL  = 1
OFFSITE_ASSETID_COL  = 2
ASSET_TYPE_ID_COL  = 3
ASSET_SUB_TYPE_ID_COL  = 4
RUN_TIME_SECONDS_COL  = 5
THUMBNAIL_MEDIUM_PATH_COL  = 6
MATURE_COL  = 7
PUBLIC_COL  = 8
OVER_21_REQUIRED_COL  = 9
NUMBER_OF_DIGGS_COL  = 10
MEDIA_PATH = 11
GLOBAL_ID_COL = 12

# GetSet.AddNewMember types
amBool              = 1             
amInt               = 8
amLong              = 10
amDouble            = 14
amString            = 15

# add member flags
amfTransient            = 0x02000000 # this item is not serialized
amfReplicate            = 0x08000000 # transfer between server and client, for dynamic
amfSaveAcrossSpawn      = 0x10000000 # save across a spawn for dynamic player data
amfMask                 = 0x1e000000 

# add member/find return values
amrDupeWrongType        = 0xfffffffe
amrBadType              = 0xfffffffd
amrNotFound             = 0xffffffff

# for GotoPlaceEvent
GOTOPLACE_EVENT_NAME   = "GotoPlaceEvent" 
GOTO_ZONE               = 1
GOTO_PLAYER             = 2
GOTO_PLAYERS_APT        = 3

# for upgrading
ApartmentUpgrade        = 1
ChannelUpgrade          = 2

# use types for player use item event
USE_TYPE_NONE			= 0
USE_TYPE_HEALING		= 1
USE_TYPE_AMMO			= 2
USE_TYPE_SKILL			= 3
USE_TYPE_MENU			= 4
USE_TYPE_COMBINE		= 5
USE_TYPE_PLACE_HOUSE	= 6
USE_TYPE_INSTALL_HOUSE	= 7
USE_TYPE_SUMMON_CREATURE= 8
USE_TYPE_OLD_SCRIPT		= 9
USE_TYPE_ADD_DYN_OBJ	= 10
USE_TYPE_ADD_ATTACH_OBJ	= 11
USE_TYPE_MOUNT			= 12
USE_TYPE_EQUIP			= 13
USE_TYPE_ANIMATION      = 200
USE_TYPE_P2P_ANIMATION  = 201
USE_TYPE_NPC            = 202
USE_TYPE_DEED           = 203
USE_TYPE_QUEST          = 204
USE_TYPE_PREMIUM		= 205
USE_TYPE_PARTICLE		= 206
USE_TYPE_SOUND			= 207
USE_TYPE_BUNDLE			= 208
USE_TYPE_CUSTOM_DEED	= 209
USE_TYPE_ACTION_ITEM	= 210
USE_TYPE_GAME_ITEM		= 211

SERVERDYNOBJ_REQUEST_EVENT_NAME = "ServerDynObjRequestEvent"
SERVERDYNOBJ_REPLY_EVENT_NAME = "ServerDynObjReplyEvent"

# UGC GLID Constants
GLID_UGC_BASE = 3000000

#columns/indexes for the dynamic objects in ZoneCustomizationsDB
OBJ_PLACEMENT_ID_DO = 0
OBJ_ID_DO = 1
TEX_ASSET_ID_DO = 2
PLAYER_ID_DO = 3
FRIEND_ID_DO = 4
POS_X_DO = 5
POS_Y_DO = 6
POS_Z_DO = 7
ROT_X_DO = 8
ROT_Y_DO = 9
ROT_Z_DO = 10
TEX_URL_DO = 11
SWF_NAME_DO = 12
SWF_PARAM_DO = 13
GLID_DO = 14

#columns/indexes for the world objects ZoneCustomizationsDB
WORLD_OBJ_ID_WO = 0
ASSET_ID_WO = 1
TEX_URL_WO = 2

#-------------------------------------------------------------------
gLoggingDispatcher = 0
debugPrinting = True
def debugPrint( *args ):
    "print only if KEP.debugPrinting is set to true"
    if debugPrinting:
        s = ""
        for x in args:
            s = s + str(x) + " "
        try:
            if gLoggingDispatcher != 0:
                import Dispatcher
                Dispatcher.LogMessage( gLoggingDispatcher, DEBUG_LOG_LEVEL, s )
            else:
                print s
        except:
            pass # probably running as a service

#-------------------------------------------------------------------
# to cut down on noise don't count the Python, or our own C modules as dependents
ignoredDeps = ("stat","sys","os","inspect","exceptions","types","re","string","imp","tokenize","_re",\
               "_sre","cp1252","codecs","aliases","linecache","dis","copy_reg","_copy_reg","sre_parse",\
               "UserDict","path","sre_compile","time",\
               "InstanceId", "Dispatcher", "Event", "GetSet", "ParentHandler", "Game", "Arena", "EngineXWin" )
def __getDependentModules(mod, ret, indent = "" ):

    import inspect
    import sys
    try:
        if sys.modules.has_key(mod):
            for x in sys.modules[mod].__dict__:
                try:
                    if inspect.ismodule(sys.modules[mod].__dict__[x]):
                        if x[:2] != "__" and not x in ignoredDeps:
                            if ret.has_key(x):
                                ret[x] += 1
                            else:
                                ret[x] = 1
                        getDependentModules(x, indent + "  ", ret ) # recurse
                    else:
                        pass 
                except:
                    pass
                    #KEP.debugPrint( indent, ">>>Got exception for ", mod, sys.exc_type, sys.exc_value )
    except AttributeError: 
        pass
    except:
        pass
        #KEP.debugPrint( indent, ">>>Got exception for ", mod, sys.exc_type, sys.exc_value )

#-------------------------------------------------------------------
# for reloading we need to load in dependent order
def getDependentModules(mod, indent = "" ):
    s = str(mod)+"\t" # do ourselves
    ret = {}
    __getDependentModules(mod,ret)
    for r in ret:
        s = s + r + "\t"

    #print ">>>Module", mod
    #print ">>>Sending back", s
    return s[:-1] # strip trailing \t

