--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- From GetAttributeEvent.h
GetAttributeType = {}
GetAttributeType.GET_ATTACHMENT_STATES  = 1
GetAttributeType.REMOVE_PLAYER_BY_NETID = 2
GetAttributeType.REQUEST_ITEM_INFO      = 4
GetAttributeType.CREATE_PLAYER          = 8
GetAttributeType.SELECT_CHARACTER       = 9
GetAttributeType.DELETE_PLAYER          = 10
GetAttributeType.PLAYER_IS_LOOTING      = 11
GetAttributeType.GET_CHARACTER_LIST     = 14
GetAttributeType.SET_INVENTORY_ITEM     = 15
GetAttributeType.AI_REQUEST_UPDATE      = 16
GetAttributeType.ARM_NEXT_ITEM          = 17
GetAttributeType.SET_DEFORMABLE_ITEM    = 18
GetAttributeType.BUY_ITEM               = 21
GetAttributeType.SELL_ITEM              = 22
GetAttributeType.LOOT_ITEM              = 23
GetAttributeType.BANK_DEPOSIT_ITEM      = 24
GetAttributeType.BANK_WITHDRAW_ITEM     = 25
GetAttributeType.REQUEST_PORTAL         = 26
GetAttributeType.USE_ITEM               = 27
GetAttributeType.CANCEL_TRANSACTION     = 29
GetAttributeType.ACCEPT_TRADE_TERMS     = 30
GetAttributeType.DESTROY_ITEM_BY_GLID   = 31
GetAttributeType.QUEST_HANDLING         = 32
GetAttributeType.USE_SKIL               = 33
GetAttributeType.EXPAND_ITEM_DATA       = 37
GetAttributeType.REQUEST_HOUSE_CONFIG   = 38
GetAttributeType.BUY_HOUSE_ITEM         = 39
GetAttributeType.ADD_HOUSE_ITEM         = 40
GetAttributeType.REMOVE_HOUSE_ITEM      = 41
GetAttributeType.REMOVE_HOUSE_CASH      = 42
GetAttributeType.REORG_BANK_ITEMS       = 43
GetAttributeType.REORD_INVENTORY_ITEMS  = 44
GetAttributeType.REQUEST_QUEST_JOURNAL  = 45
GetAttributeType.REPORT_MURDER          = 46
GetAttributeType.GET_PLAYER_INVENTORY_ATTACHABLE = 47
GetAttributeType.TRY_ON_ITEM               = 48
GetAttributeType.DESTROY_BANK_ITEM_BY_GLID = 49

GetAttributeType.GET_INVENTORY_PLAYER = 50
GetAttributeType.GET_INVENTORY_BANK   = 51
GetAttributeType.GET_INVENTORY_HOUSE  = 52
GetAttributeType.GET_INVENTORY_ALL    = 59