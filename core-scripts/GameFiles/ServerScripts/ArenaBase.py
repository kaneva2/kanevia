#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

"""
ArenaBase module that contains ArenaBase class
"""
#import helper Python code
import KEP
import ChatType
import ArenaEquipment
import SoundGlobals

# C functions
import PlayerIds
import InventoryIds
import ItemIds
import InstanceId
import Dispatcher
import Event
import GetSet

# python imports
import time
import random
import threading

# used for sign up returns
MSG_ALREADY_RUNNING = "The arena is already running.  Signup rejected."
MSG_SIGNUP_OK = "You have been scheduled to do battle in the %s arena.\r\nPrepare for battle."
MSG_SUICIDE = "You tagged yourself" 
MSG_KILLED = "You were tagged by %s"
MSG_SUICIDE_BROADCAST = "%s just tagged himself!"
MSG_KILL_BROADCAST = "%s was tagged by %s"
MSG_YOU_HAVE_LOST = "You have lost"
MSG_WINNER = "%s wins!"

# arena types set in editor (eventually)
NOT_TEAMED_ARENA        = 0
TEAMED_ARENA            = 1 # such as R/B, not base on property of character
RACE_TEAMED_ARENA       = 2
CLAN_TEAMED_ARENA       = 3

# filters for arena mgr events
ARENA_SPAWN_INFO_FILTER = 1
ARENA_KILLED_FILTER = 2
ARENA_ENDED_FILTER = 3
ARENA_SCORE_FILTER = 4
ARENA_TIMER_FILTER = 5
ARENA_RESPAWN_FILTER = 6
ARENA_LOBBY_CHAT_FILTER = 7
ARENA_PLAYER_UPDATED_FILTER = 8
ARENA_PLAYER_HEALTH_UPDATED_FILTER = 9
ARENA_START_FILTER = 10
#-------------------------------------------------------------------
class ArenaSpawnPoint(object):
    """class to hold a spawn point

    This class is used by ArenaBase and derived classes to 
    track a spawn point in an arena.
    """
    
    def __init__(self,points):
        self.x = points[0]
        self.y = points[1]
        self.z = points[2]


#-------------------------------------------------------------------
class PlayerStats(object):
    """class for tracking statistics for a player in an arena
    """

    def __init__(self, name, edb):
        """consturctor"""
        
        self.name = name
        self.playerId = 0
        self.famelevel = 0
        self.quitEarly = False
        self.joinedMidMatch = False
        self.shots = 0
        self.hits = 0
        self.kills = 0
        self.killsList = {}
        self.killed = 0
        self.points = 0
        self.heldCoins = 0
        self.edbIndex = edb           # entity type since gems/flags are edb-specific
        self.equipInfo = 0            # if != 0, a tuple of (gemEquipId,flagEquipId)
        self.respawnTime = 0          # if die, how soon can they respawn
        self.sentStats = False        # for LMS, only send stats on first re-spawn
        self.isReady = False          # can also be used to determine if in lobby while game is going on
        self.raceId = -1
        self.teamId = -1              # id of this player's team, if teamed
        self.equipment = None
        
        self.ai = False               # is this an AI guy?

    def EncodeStats( self, event ):
        """get all the stats and put them in the event

        This is called when sending the end-arena details to the client
        for each player.  It encodes all the details about the player
        """
        
        Event.EncodeNumber( event, self.teamId )
        Event.EncodeString( event, self.name )
        Event.EncodeNumber( event, self.hits )
        Event.EncodeNumber( event, self.shots )
        Event.EncodeNumber( event, self.killed)
        Event.EncodeNumber( event, self.kills )
        Event.EncodeNumber( event, self.points )

		

#------------------------------------------------------------------------------------       
def MakePlayerKey( playerNetId, playerNetAssignedId ):
    """heper to create a key used for the player dictionary"""
    return (long(playerNetAssignedId) << 32) + long(playerNetId)

#------------------------------------------------------------------------------------       
def NetIdFromKey( key ):
    """heper to extract the netid from a key"""
    return long(key) & 0xffffffffL

#------------------------------------------------------------------------------------       
def NetAssignedIdFromKey( key ):
    """heper to extract the netid from a key"""
    return long(key) >> 32

#------------------------------------------------------------------------------------       
def KeyEqNetId( playerKey, playerNetId ):
    """helper to compare a key to a netid, can't just compare since signed netid messes things up"""

    return playerKey == MakePlayerKey( playerNetId, 0 )

#------------------------------------------------------------------------------------       
def KeyNetIdEqNetId( playerKey, playerNetId ):
    """helper to compare a key to a netid, can't just compare since signed netid messes things up"""

    return NetIdFromKey( playerKey ) == NetIdFromKey( MakePlayerKey( playerNetId, 0 ) )

#------------------------------------------------------------------------------------       
def AdjustPlayerHealth(game, fromNetID, playerObj, amount):
    import Game
    
    if(playerObj != None):
        curEnergy = GetSet.GetNumber(playerObj, PlayerIds.ENERGY)
        maxEnergy = GetSet.GetNumber(playerObj, PlayerIds.MAXENERGY)
        curEnergy += amount
        if(curEnergy > maxEnergy):
            curEnergy = maxEnergy
        if(curEnergy < 0):
            curEnergy = 0
        GetSet.SetNumber(playerObj, PlayerIds.ENERGY, curEnergy)
        Game.UpdatePlayerHealth( game, fromNetID )  ##-----------What does this do?
            

#------------------------------------------------------------------------------------       
def AdjustPlayerHealthToMax(playerObj):
    import Game

    if(playerObj != None):
        maxEnergy = GetSet.GetNumber(playerObj, PlayerIds.MAXENERGY)
        GetSet.SetNumber(playerObj, PlayerIds.ENERGY, maxEnergy)
        


#-------------------------------------------------------------------
#-------------------------------------------------------------------
class ArenaBase(object):
    """base class for Arenas 

        This provide much of the basic arena functionality such as 
        moving players into arenas, tracking players' stats, handling
        respawn behavior etc.  Much of the behavior can be altered by 
        overriding the claas methods
    """
    
    ARENA_TIMER_NAME = "ArenaTimerEvent"
    GAME_ADMIN_BOOT_PLAYER_EVENT = "GameAdminBootPlayerEvent"
    GAME_ADMIN_START_ARENA_EVENT = "GameAdminStartArenaEvent"
    GAME_ADMIN_ENABLED_EVENT = "GameAdminEnabledEvent"
    LOBBY_CHAT_EVENT = "ArenaLobbyChatEvent"
    EQUIP_SHIELD_EVENT = "EquipShieldEvent"
    
    def __init__(self,name, parentList,game,arenaPtr,channelInstance,zi,maxBattleTime,pointsToWin,hudMenu,statMenu,parent,dispatcher):
        " constructor "
        
        import Arena

        self._hudMenu = hudMenu
        self._statMenu = statMenu
        self._arenaName = name
        self._mapName = "--"
        self._parentList = parentList
        self._arenaPtr = arenaPtr
        self._parent = parent
        self._dispatcher = dispatcher
        self._game = game
        self._arenaPtr = arenaPtr
        self.channelId = channelInstance
        self._respawnDelay = 5
        self._startCountdown = 15
        self._maxBattleTime = (maxBattleTime / 1000) + self._startCountdown
        self.minReadyPlayers = 1
        self.minPlayers = 1 # set later
        self.maxPlayers = 8
        self._pointsToWin = pointsToWin
        self._winnerName = "No one" # has won the arena
        self._countSuicideInKilleds = True # count suicides in the Killed count?
        self.players = dict()       # playerKey to playerStat, players still in arena
        self.exitedPlayers = dict() # netId to playerStat, players exited, for LMS, etc.
        
        self.inLobby = True
        self.locked = False
        self.arenaAdmin = 0 # netId of player responsible for starting game, booting players, etc.
        self.arenaKey = 0
        self._status = "In Lobby"
        self.genre = "Casual"
        self._nextSpawnPointIndex = 0
        self._spawnPointCount = Arena.GetSpawnPointCount(self._arenaPtr)
        self._startTime = time.time()
        self._estimatedEndTime = self._startTime
        self._endTime = time.time()
        self._hasArenaEnded = False
        self.noWeapons = False

        # derived classes can set these to control how this class works (w/o overriding)
        self._respawnInArenaOnDeath = True
        self._killArenaPt           = 2 # pts to add to killer's arena pts
        self._killedArenaPt         = 1 # pts to subtract from killed's arena pts

        # figure out the zone index        
        self.zoneIndex = InstanceId.SetInstanceFrom( int(zi), int(channelInstance) );
        self._timerEvent = 0

    #------------------------------------------------------------------------------------       
    def __del__(self):
        """destructor """
        KEP.debugPrint( "~~~~~~Deleting arena", self._arenaName )
        

    #------------------------------------------------------------------------------------       
    def PlayerKilled( self, playerNetId, playerNetAssignedId, killerNetId, killerNetAssignedId, locX, locY, locZ ): 
        """Called when a player has been killed 

        This function is called when a player has been killed in an arena.
        It will update stats, send the client an ArenaManagerEvent with a filter
        of ARENA_KILLED_FILTER indicating
        the player has been killed, so it can popup the respawn menut, and it 
        will call self._doWeHaveAWinner
        """
        
        killerStats = 0
        killerIsAI = True
        playerStats = 0
        playerIsAI = True
        
        
        killerKey = MakePlayerKey(killerNetId, killerNetAssignedId )
        playerKey = MakePlayerKey(playerNetId, playerNetAssignedId)
        
        if  self.players.has_key(killerKey):
            killerStats = self.players[killerKey]
            if killerKey != playerKey: # suicide doesn't count
                killerStats.kills += 1
                if playerKey in self.players:
                    if self.players[playerKey].playerId in killerStats.killsList:
                        killerStats.killsList[self.players[playerKey].playerId] += 1
                    else:
                        killerStats.killsList[self.players[playerKey].playerId] = 1
    
            killerIsAI = killerStats.ai
        else:
            Dispatcher.LogMessage( self._dispatcher, KEP.WARN_LOG_LEVEL,  "ArenaBase.PlayerKilled Couldn't find killer NetId = %d NetAssigneId = %d" % (killerNetId, killerNetAssignedId ) )
            return 

        if  self.players.has_key(playerKey):

            playerStats = self.players[playerKey]
            if killerKey != playerKey or self._countSuicideInKilleds: # suicide doesn't count
                playerStats.killed += 1
            playerIsAI = playerStats.ai

            # call before changing player key
            # self._adjustArenaPoints(playerKey, playerIsAI, killerKey, killerIsAI)
            
            # update the network assigned id to mark as "killed"
            # respawn him if not AI
            newKey = long(playerKey) | 0x8000000000000000L # set high bit to mark as "killed"
            self.players[newKey] = self.players[playerKey]
            del self.players[playerKey]
            playerKey = newKey
            """
            if playerNetAssignedId == killerNetAssignedId:
                self._broadcastMessage( 0, MSG_SUICIDE_BROADCAST % (killerStats.name), ChatType.ArenaMgr )
            else:
                self._broadcastMessage( 0, MSG_KILL_BROADCAST % (playerStats.name, killerStats.name ), ChatType.ArenaMgr )
            """
            if not playerStats.ai:
            
                # respawn him if not AI
                if self._respawnInArenaOnDeath:
                    import Game

                    playerStats.respawnTime = time.time() + (self._respawnDelay - 3)  # shave a few seconds, just for fudge factor, 1 was not enough
                    
                    respawnEvent = Dispatcher.MakeEvent( self._dispatcher, "ArenaManagerEvent" )
                    Event.EncodeNumber( respawnEvent, playerNetId )
                    Event.SetFilter( respawnEvent, ARENA_RESPAWN_FILTER )
                    Dispatcher.QueueEventInFuture( self._dispatcher, respawnEvent, self._respawnDelay )

                    playerKilledEvent = Dispatcher.MakeEvent( self._dispatcher, "ArenaManagerEvent" )
                    Event.EncodeNumber( playerKilledEvent, self._respawnDelay ) # seconds to wait
                    """
                    if playerNetAssignedId == killerNetAssignedId:
                        Event.EncodeString( startEvent, MSG_SUICIDE ) # title
                    else:
                        Event.EncodeString( startEvent, MSG_KILLED % (killerStats.name) ) # title
                    """
                    if killerStats != 0:
                        Event.EncodeString( playerKilledEvent, killerStats.name ) #desc
                    else:
                        Event.EncodeString( playerKilledEvent, "the Kaching! Gods" ) #desc
                    Event.SetFilter( playerKilledEvent, ARENA_KILLED_FILTER )
                    Event.AddTo( playerKilledEvent, playerNetId )
                    Dispatcher.QueueEvent( self._dispatcher, playerKilledEvent )
                    
                else:
                    # since sending them out, copy them to exited players array
                    self._playerExiting(playerKey,playerStats)
                    self.exitedPlayers[NetIdFromKey(playerKey)] = self.players[playerKey]
                    del self.players[playerKey]

            

            # call override to see if there is a winner
            self._doWeHaveAWinner()
            
        else:
            Dispatcher.LogMessage( self._dispatcher, KEP.WARN_LOG_LEVEL,  "ArenaBase.PlayerKilled Couldn't find killed NetId = %d NetAssigneId = %d" % (playerNetId, playerNetAssignedId) )
            
    """
    #------------------------------------------------------------------------------------       
    def _adjustArenaPoints( self, playerKey, playerIsAI, killerKey, killerIsAI ):
        import Game 

        # don't track arena points for AI
        playerNetId = 0
        killerNetId = 0
        
        if not killerIsAI:
            killerNetId = NetIdFromKey(killerKey)

        if not playerIsAI:
            playerNetId = NetIdFromKey(playerKey)

        if not killerIsAI and not playerIsAI: 
            Game.AdjustArenaPoints( self._game, NetIdFromKey(killerKey), self._killArenaPt, NetIdFromKey(playerKey), self._killedArenaPt )
        
        if not killerIsAI and playerKey != killerKey: 
            self._sendMessage( NetIdFromKey(killerKey), "You scored an arena point!", ChatType.ArenaMgr )
    """
        
    #-----------------------------------------------------------------
    def _doWeHaveAWinner(self):
        """override this to check if we have a winner when player killed/disconnected

        The default implementation does nothing."""
            
        pass
        return False
    
    #-----------------------------------------------------------------
    def _getArenaAdminName(self):
        """gets the name of the arena Admin"""
        
        adminNetId = NetIdFromKey(self.arenaAdmin)
        
        for player in self.players:
            if(KeyNetIdEqNetId( player, adminNetId )):
                return self.players[player].name
        return ""
    
    #-----------------------------------------------------------------
    def _getTimeRemaining(self):
        """gets the time remaining in the arena"""
        if(self.inLobby):
            return int(self._maxBattleTime)
        return int(self._estimatedEndTime - time.time())
    
    #-----------------------------------------------------------------
    def _getTimeDuration(self):
        """gets the duration of the arena"""
        if(self.inLobby):
            return 0
        dur = 0
        if(self._hasArenaEnded):
            dur = int( self._endTime - self._startTime )
        else:
            dur = int( time.time() - self._startTime )   
        return dur
    
    #-----------------------------------------------------------------
    def BootPlayer( self, playerKey ):
        msg = "You've been booted from the game."
        filter = 0
        e = Dispatcher.MakeEvent(KEP.dispatcher, ArenaBase.GAME_ADMIN_BOOT_PLAYER_EVENT)
        self.PlayerNetIdDisconnected(playerKey)
        Event.EncodeString(e, msg)
        self._sendEvent(NetIdFromKey( playerKey ), e)
        
    #-----------------------------------------------------------------
    def PlayerNetIdDisconnected( self, playerNetId ):
        """Called when a player is disconnected by net id.

       This is called by arena mgr.  
       This loops over keys and calls disconnect on each on with that netid
       since AI can disconnect more than one player when it disconnects
       """

        pcCount = 0
        
        ourKeys = self.players.keys()[:] # must copy in case they delete during iteration
        for playerKey in ourKeys:
            if KeyNetIdEqNetId( playerKey, playerNetId ):
                self.PlayerDisconnected(playerKey)
            elif not self.players[playerKey].ai:
                pcCount += 1
        arenaEndPlayerCount = 1
        
        if self.inLobby == True:
            arenaEndPlayerCount = 0
        if(self._hasArenaEnded == False):    
            if pcCount <= arenaEndPlayerCount: # only one (or less) person left after a disconnect
                self.ArenaTimedOut()
            else:
                self._doWeHaveAWinner()
            
    #------------------------------------------------------------------------------------       
    def ChangeArenaAdmin( self, newAdminKey ):
        if(len(self.players) > 0):
                for playerKey in self.players:
                    #just pick a random admin because 0 means admin left without choosing a new admin
                    if(newAdminKey == 0):
                        self.arenaAdmin = playerKey
                        ev = Dispatcher.MakeEvent(KEP.dispatcher, ArenaBase.GAME_ADMIN_ENABLED_EVENT)
                        Event.EncodeString(ev, str(1))
                        self._sendArenaAdminEvent(ev)
                        break
                    elif(newAdminKey == playerKey):
                        self.arenaAdmin = playerKey
                        ev = Dispatcher.MakeEvent(KEP.dispatcher, ArenaBase.GAME_ADMIN_ENABLED_EVENT)
                        Event.EncodeString(ev, str(1))
                        self._sendArenaAdminEvent(ev)
                        break
            
    def _logPlayers(self):
        for player in self.players:
            Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, self.players[player].name + ", " + str(player))
        
    #------------------------------------------------------------------------------------       
    def PlayerDisconnected( self, playerKey ):
        """Called when a player is disconnected from the server
        
        This removes player from the list
        """
        import Game
        import GameGlobals
        
        try:
            playerStats = self.players[playerKey]

            self._playerExiting(playerKey,playerStats)
            self.exitedPlayers[NetIdFromKey(playerKey)] = self.players[playerKey]
            del self.players[playerKey]
            if(self._hasArenaEnded == False):
                self.exitedPlayers[NetIdFromKey(playerKey)].quitEarly = True
            if(playerKey == self.arenaAdmin):
                self.ChangeArenaAdmin(0)
            
        except KeyError:
            Dispatcher.LogMessage( self._dispatcher, KEP.WARN_LOG_LEVEL,  "ArenaBase.PlayerDisconnected Couldn't find disconnected player Key = %d" % (playerKey) )


    #------------------------------------------------------------------------------------       
    def ArenaTimedOut(self):
        """Called when the arena has timed out

        This ends the arena by calling self._endArena
        """

        if len(self.players.values()) == 0:
            self._endArena("No one") # everybody gone home :-(
        else:
            self._endArena(self._getLeaderName())

    #------------------------------------------------------------------------------------       
    def PlayerSpawned( self, playerNetId, playerNetAssignedId, event, filter ):
        """Called when a player spawns into an arena

        This updates the client with the time remaining by sending 
        them an ArenaManagerEvent with a filter of ARENA_SPAWN_INFO_FILTER
        """

        newKey = MakePlayerKey( playerNetId, playerNetAssignedId )
        import Game
        
        
        # find the player and reset the assigned ID to the new one
        for playerKey in self.players:

            # if first time spawn, we won't have assigned id so key == netId
            # otherwise, the assigned id part of key will have the high bit set, when they were killed
            # or, if PC teleporting w/i zone, just check netId
            if KeyEqNetId( playerKey, playerNetId ) or \
               (long(playerKey) & 0x80000000ffffffffL) == (NetIdFromKey(newKey) | 0x8000000000000000L) or \
               (NetIdFromKey(playerKey) == NetIdFromKey(newKey) and not self.players[playerKey].ai):
                
                self.unarmPlayerUGCAccessories(playerKey)
                self.setupPlayerArenaInventory(playerKey)
                
                # possible match, also check name/edb
                playerObj = Game.GetPlayerByNetTraceId( self._game, playerNetAssignedId, 0, 0 )
                if playerObj:
                    # add, them and put them on the appropriate team
                    name = GetSet.GetString( playerObj, PlayerIds.CHARNAME )
                    edb =  GetSet.GetNumber( playerObj, PlayerIds.CUREDBINDEX )
                    AdjustPlayerHealthToMax( playerObj )
                    self.EquipShield( playerKey, playerNetId, True, 5 )

                    if self.players[playerKey].name != name or \
                       self.players[playerKey].edbIndex != edb:
                       continue
                
                # found them, update the network assigned id
                self.players[newKey] = self.players[playerKey]
                del self.players[playerKey]

                if not self.players[newKey].ai:
                    timeDiff = self._estimatedEndTime - time.time()
                    if timeDiff < 0:
                        timeDiff = 0
                        
                    Game.TellClientToOpenMenu( self._game, self._hudMenu, playerNetId, 0, 0 )
                    
                    startEvent = Dispatcher.MakeEvent( self._dispatcher, "ArenaManagerEvent" )
                    Event.EncodeString( startEvent, self._arenaName )
                    Event.EncodeNumber( startEvent, self._pointsToWin )
                    Event.EncodeNumber( startEvent,  int(self._maxBattleTime) )
                    Event.EncodeNumber( startEvent, timeDiff )
                    self._addSpawnInfo( startEvent, newKey )
                    Event.AddTo( startEvent, playerNetId )
                    Event.SetFilter( startEvent, ARENA_SPAWN_INFO_FILTER )
                    
                    Dispatcher.QueueEvent( self._dispatcher, startEvent )
                    
                    tempCountdown = self._startTime + self._startCountdown - time.time()
                    if(tempCountdown <= self._startCountdown and tempCountdown > 3):
                        e = Dispatcher.MakeEvent(KEP.dispatcher, "ArenaManagerEvent")
                        Event.SetFilter(e, ARENA_START_FILTER)
                        Event.EncodeNumber(e, tempCountdown)
                        Event.AddTo( e, playerNetId )
                        Dispatcher.QueueEvent( self._dispatcher, e )    
                break;
            else:
                pass 
                #KEP.debugPrint( "    not matched yet " )

        else:
            KEP.debugPrint( "    Newly spawned player not found, len is", len(self.players) )
            
            if playerNetId in self.exitedPlayers and not self.exitedPlayers[playerNetId].ai and not self.exitedPlayers[playerNetId].sentStats:
                # only send once
                self.exitedPlayers[playerNetId].sentStats = True
                """
                self._sendKilledPlayerStatList(playerNetId, MSG_YOU_HAVE_LOST )
                """
            
        return KEP.EPR_OK
    
    #------------------------------------------------------------------------------------
    def broadcastMusic(self, soundID):
        event = Dispatcher.MakeEvent( KEP.dispatcher, "BroadcastMusicEvent" )
        Event.EncodeNumber( event, soundID )
        self._broadcastEvent( event )
        
    #------------------------------------------------------------------------------------
    def buildSoundEvent(self, soundID, playerKey, playerNetID = 0):
        import Game
        soundAttrib = SoundGlobals.SoundAttribs[ soundID ]
        fallOff = soundAttrib[ 0 ]
        clipDist = soundAttrib[ 1 ]
        
        playerObj = Game.GetPlayerByNetId( self._game, playerNetID )
        if playerObj:
            x, y, z = GetSet.GetVector( playerObj, PlayerIds.N3DPOSITION )
            event = Dispatcher.MakeEvent( KEP.dispatcher, "BroadcastSoundEvent" )
            Event.EncodeNumber( event, soundID )
            Event.EncodeNumber( event, x )
            Event.EncodeNumber( event, y )
            Event.EncodeNumber( event, z )
            Event.EncodeNumber( event, fallOff )
            Event.EncodeNumber( event, clipDist )
            return event
        else:
            Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "PlayerIds.N3DPOSITION error was caught.")
            return None
        
    #------------------------------------------------------------------------------------
    def broadcastPlayerSound(self, soundID, playerKey, playerNetID = 0):
        if playerNetID == 0:
            playerNetID = NetIdFromKey( playerKey )
        event = self.buildSoundEvent(soundID, playerKey, playerNetID)
        if event != None:
            self._broadcastEvent( event )
    
    #------------------------------------------------------------------------------------       
    def singlePlayerSound(self, soundID, playerKey, playerNetID = 0):
        if playerNetID == 0:
            playerNetID = NetIdFromKey( playerKey )
        event = self.buildSoundEvent(soundID, playerKey, playerNetID)
        if event != None:
            self._sendEvent( playerNetID, event )
    
    #------------------------------------------------------------------------------------       
    def teamPlayerSound( self, soundID, teamID, playerKey, playerNetID = 0 ):
        """ play a singlePlayerSound on each team member """

        if len(self.players) > 0:
            for playerKey in self.players:
                if self.players[playerKey].teamId == teamID:
                    self.singlePlayerSound(soundID, playerKey, playerNetID)
    
    #------------------------------------------------------------------------------------       
    def _broadcastEvent( self, event ):
        """ helper to broadcast this event to all players in the arena """

        if len(self.players) > 0:
            for playerKey in self.players:
                if not self.players[playerKey].ai:
                    netId = NetIdFromKey( playerKey )
                    Event.AddTo( event, netId )
                    
            Dispatcher.QueueEvent( self._dispatcher, event )
    
    #------------------------------------------------------------------------------------       
    def _broadcastEvent_usePreSpawnPlayerKey( self, event ):
        if len(self.players) > 0:
            for playerKey in self.players:
                Event.AddTo( event, playerKey )
            Dispatcher.QueueEvent( KEP.dispatcher, event )
            
    #------------------------------------------------------------------------------------       
    def _sendEquipShieldEvent( self, playerKey, toEquip, delay ):
        e = Dispatcher.MakeEvent(KEP.dispatcher, ArenaBase.EQUIP_SHIELD_EVENT)
        Event.EncodeString(e, str(playerKey))
        Event.EncodeNumber(e, int(toEquip))
        Event.EncodeNumber(e, int(delay))
        Event.AddTo(e, KEP.SERVER_NETID)
        Dispatcher.QueueEventInFuture(KEP.dispatcher, e, delay)
    
     #------------------------------------------------------------------------------------       
    def _sendArenaAdminEvent( self, event ):
        """ helper to send an event to a specific player in the arena """

        Event.AddTo( event, NetIdFromKey(self.arenaAdmin ))
        Dispatcher.QueueEvent( self._dispatcher, event )
        
    #------------------------------------------------------------------------------------       
    def _sendEvent( self, playerNetId, event ):
        """ helper to send an event to a specific player in the arena """

        Event.AddTo( event, playerNetId )
        Dispatcher.QueueEvent( self._dispatcher, event )
        
    #------------------------------------------------------------------------------------       
    def _makeArenaMgrEvent( self, filter, *args ):
        """ helper to make an ArenaManagerEvent, encoding the args """

        event = Dispatcher.MakeEvent( self._dispatcher, "ArenaManagerEvent" )
        if event != 0:
            Event.SetFilter( event, filter  )
            
            for x in args:
                if isinstance(x,long) or isinstance(x,int) or isinstance(x,float):
                    Event.EncodeNumber( event, x )
                elif isinstance(x,str):
                    Event.EncodeString( event, x )
                else:
                    raise TypeError

        return event
        
    #------------------------------------------------------------------------------------       
    def PlayerFired( self, playerNetId, playerNetAssignedId, event, filter ):
        """ Called when a  player shot at hit something

        This tracks the shot for the player
        """

        # KEP.debugPrint( "!!In Base.PlayerFired", hex(int(filter)), "==", hex(int(self.channelId)), fromNetId, len(self.players) )
        playerKey = MakePlayerKey(playerNetId, playerNetAssignedId)
        if filter == self.channelId and playerKey in self.players:
            # KEP.debugPrint( "!!player shot at something" )  
            self.players[playerKey].shots += 1
            return KEP.EPR_OK
            
        return KEP.EPR_NOT_PROCESSED
    
    #------------------------------------------------------------------------------------       
    def PlayerUpdateAmmo( self, playerNetId, playerKey, maxCharge, curCharge, glid):
        """Updates the ArenaEquipment obj with appropriate ammo for player"""
        curEquip = self.players[playerKey].equipment
        curEquip.setWeaponAmmoInfo(playerNetId, maxCharge, curCharge, glid)
        if curEquip.validateCurrentWeapon() == False:
            success = curEquip.equipGunByIndex(ArenaEquipment.MACHINEGUN, playerNetId)
        return KEP.EPR_OK
    
    #------------------------------------------------------------------------------------       
    def PlayerReady( self, playerNetId):
        """ Called when a player is ready or not ready """

        for playerKey in self.players:
            if(KeyNetIdEqNetId(playerKey, playerNetId)):
                self.players[playerKey].isReady = not (self.players[playerKey].isReady)
                if self.inLobby == False:
                    self.PlayerJoinedMidMatch(playerKey)
       
        return KEP.EPR_OK
    
    #------------------------------------------------------------------------------------       
    def PlayerDidDamage( self, playerNetId, event, filter ):
        """ Called when a player hit something

        This tracks the hit for the player
        """
        #if filter == self.channelId:
        type    = Event.DecodeNumber( event )
        miscId  = Event.DecodeNumber( event )
        netId   = Event.DecodeNumber( event )
        playerNetAssignedId = Event.DecodeNumber( event )
        victimNetId = Event.DecodeNumber( event )
        victimNetAssignedId = Event.DecodeNumber( event )
        
        event = Dispatcher.MakeEvent(KEP.dispatcher, "ArenaManagerEvent")
        Event.SetFilter(event, ARENA_PLAYER_HEALTH_UPDATED_FILTER)
        self._sendEvent(victimNetId, event)
            
        playerKey = MakePlayerKey(playerNetId, playerNetAssignedId)
        if playerKey in self.players:
            # get the type, we only count missle damage
            if type == KEP.MISSILE or type == KEP.EXPLOSION:
                self.players[playerKey].hits += 1
                
                vicKey = MakePlayerKey(victimNetId, victimNetAssignedId)
                soundID = SoundGlobals.SOUND_KACHING_IMPACT1 + random.randint(0, 1) 
                self.broadcastPlayerSound(soundID, vicKey)
            
            return KEP.EPR_OK
            
        return KEP.EPR_NOT_PROCESSED

    #------------------------------------------------------------------------------------       
    def ArenaManagerEvent( self, filter, event, fromNetId ):
        """ Handle an event from the arena manager.

        The only thing the base class does is handle events
        with the ARENA_RESPAWN_FILTER filter and respawns
        the player in the arena
        """
        Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "ArenaBase - ArenaManagerEvent Filter : " + str(filter) + " playerNetId : " + str(fromNetId) )
        if filter == ARENA_RESPAWN_FILTER:
            playerKey = self.PlayerInArena( fromNetId )
            if playerKey != 0:
                self._respawnPlayerInArena( playerKey )
                return True
            
        if filter == ARENA_LOBBY_CHAT_FILTER:
            ev = Dispatcher.MakeEvent( KEP.dispatcher, ArenaBase.LOBBY_CHAT_EVENT )
            Event.EncodeNumber(ev, Event.DecodeNumber(event))
            Event.EncodeString(ev, Event.DecodeString(event))
            Event.EncodeString(ev, Event.DecodeString(event))
            self._broadcastEvent_usePreSpawnPlayerKey(ev)
            return True

        return False         
    
    #------------------------------------------------------------------------------------       
    def LockArena( self, filter ):
        if filter == 0:
            self.locked = False
        if filter == 1:
            self.locked = True
        if filter == 2:
            self.locked = not self.locked
    
    #------------------------------------------------------------------------------------       
    def SetNoWeapons( self, filter ):
        if filter == 0:
            self.noWeapons = False
        if filter == 1:
            self.noWeapons = True
        if filter == 2:
            self.noWeapons = not self.noWeapons
          
    #------------------------------------------------------------------------------------       
    def PlayerIsAdmin( self, playerNetId ):
        """ Is the player the admin of this arena?

        """
        if KeyNetIdEqNetId(self.arenaAdmin, playerNetId ):
            return True

        return False  
        
    #------------------------------------------------------------------------------------       
    def PlayerInArena( self, playerNetId ):
        """ Is the player in this arena?

        Returns the player's key if in the arena, otherwise
        it returns zero
        """

        for playerKey in self.players:
            if KeyNetIdEqNetId(playerKey, playerNetId ):
                return playerKey

        return 0
        
#------------------------------------------------------------------------------------       
    def GetArenaEnded(self):
        """return whether the arena has ended or not"""
        return self._hasArenaEnded

    ArenaEnded = property(GetArenaEnded, doc="Can the arena exit?")

    #------------------------------------------------------------------------------------       
    def StartArena(self):
        """startup the arena by spawning all the players to it"""
        import Arena
        import Game
        
        ret = self.VerifyStartConditions()
        if ret == True:
            self.inLobby = False
            self._status = "Game in Progress"
            # get players to arena, note that at this time all players are PC (no AI)
            for playerKey in self.players:
                playerNetId = NetIdFromKey(playerKey)
                playerObj = Game.GetPlayerByNetId( self._game, playerNetId )           
                if playerObj:
                    (x,y,z) = self._getPlayerSpawnPoint( playerObj, playerKey )
                    justSpawned =  GetSet.GetNumber( playerObj, PlayerIds.JUSTSPAWNED )
                    Game.SpawnToArenaPoint( self._game, playerObj, x, y, z, self.zoneIndex )
            #---------
            # Start the arena Timer
            #---------
            self._startTime = time.time()
            self._timerEvent = Dispatcher.MakeEvent( self._dispatcher, ArenaBase.ARENA_TIMER_NAME )
            if self._timerEvent != 0:
                Event.EncodeNumber( self._timerEvent, id(self) )           
                Dispatcher.QueueEventInFuture( self._dispatcher, self._timerEvent, long(self._maxBattleTime) )
                self._estimatedEndTime = time.time() + self._maxBattleTime
            else:
                KEP.debugPrint( "Failed to create timer" )
        
              
    def VerifyStartConditions(self):
        numPlayers = len(self.players)
        msg = ""
        if(numPlayers < self.minPlayers):
            msg = msg + "There must be at least " + str(self.minPlayers) + " players to start a game."
            ev = Dispatcher.MakeEvent(KEP.dispatcher, ArenaBase.GAME_ADMIN_START_ARENA_EVENT)
            Event.EncodeString(ev, msg)
            self._sendArenaAdminEvent(ev)
            return False
        readyCount = 0
        for playerKey in self.players:
            if(self.players[playerKey].isReady == True):
                readyCount += 1
                        
        if(readyCount < len(self.players)):
            msg = msg + "To start the game, all " + str(len(self.players)) + " players must click Ready."
            ev = Dispatcher.MakeEvent(KEP.dispatcher, ArenaBase.GAME_ADMIN_START_ARENA_EVENT)
            Event.EncodeString(ev, msg)
            self._sendArenaAdminEvent(ev)
            return False
               
        return True
            
    #------------------------------------------------------------------------------------     
    def PlayerJoinedMidMatch(self, pKey):
        import Game
        
        playerKey = pKey
        
        if playerKey in self.players:
            self.players[playerKey].joinedMidMatch = True
        
        playerNetId = NetIdFromKey(playerKey)
        playerObj = Game.GetPlayerByNetId( self._game, playerNetId )
             
        if playerObj:
            (x,y,z) = self._getPlayerSpawnPoint( playerObj, playerKey )
            aiControlled = False # TODO GetSet.GetNumber( playerObj, PlayerIds.AICONTROLLED )
            justSpawned =  GetSet.GetNumber( playerObj, PlayerIds.JUSTSPAWNED )
            Game.SpawnToArenaPoint( self._game, playerObj, x, y, z, self.zoneIndex )  

    #------------------------------------------------------------------------------------     
    def EquipWeapon(self, filter, playerKey, playerNetId):
        import Game
    
        if self.players[ playerKey ] != None:
            if self.players[ playerKey ].equipment != None:
                ## FILTERS IN 1, 2, 3  -----  FILTERS OUT 0, 1, 2
                success = self.players[ playerKey ].equipment.equipGunByIndex(filter - 1, playerNetId)
                    
     #------------------------------------------------------------------------------------       
    def EquipShield( self, playerKey, playerNetId, toEquip, delay = 10 ):
        import Game
        
        if(playerKey in self.players):
            self.players[playerKey].equipment.equipShield(playerNetId, bool(toEquip), delay)
            if toEquip:
                Game.ActivateTempInvulnerability( self._game, playerNetId, delay )
                self._sendEquipShieldEvent( playerKey, False, delay )
            
    #------------------------------------------------------------------------------------       
    def _addSpawnInfo( self, startEvent, playerKey ):
        """Called when sending a spawn event to allow derived classes to add
        additional, arena-specific data to the event object.
        
        This class just encodes zero
        """
        Event.EncodeNumber( startEvent, 0)

    #------------------------------------------------------------------------------------       
    def _getPlayerSpawnPoint( self, playerObj, playerKey ):
        """Get the spawn point based on the player"""
        import Arena

        # default is to get the next in round-robin fashion, ignoring player
        self._nextSpawnPointIndex += 1
        if self._nextSpawnPointIndex >= self._spawnPointCount:
            self._nextSpawnPointIndex = 0
        return Arena.GetSpawnPoint( self._arenaPtr, self._nextSpawnPointIndex )

    #------------------------------------------------------------------------------------       
    def _respawnPlayerInArena( self, playerKey ):
        """spawn the player back into the arena"""
        import Game
        playerObj = Game.GetPlayerOffAccountByNetId( self._game, NetIdFromKey(playerKey), -1 )
        
        if playerObj != 0:
            
            (x,y,z) = self._getPlayerSpawnPoint( playerObj, playerKey )
            
            # not in getset -- yet aiControlled = GetSet.GetNumber( playerObj, PlayerIds.AICONTROLLED )
            justSpawned =  GetSet.GetNumber( playerObj, PlayerIds.JUSTSPAWNED )

            #if not aiControlled and not justSpawned: this is how C++ did it..., many times we are justSpawned, esp for respawn
            playerName = GetSet.GetString( playerObj, PlayerIds.CHARNAME )
            playerStats = self.players[playerKey]
            if time.time() < playerStats.respawnTime:
                Dispatcher.LogMessage( self._dispatcher, KEP.WARN_LOG_LEVEL,  "Player %s is respawning before respawn time!" % (playerName) )
            else:
                Game.SpawnToArenaPoint( self._game, playerObj, x, y, z, self.zoneIndex )
        else:
            Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "Player Object not found, cant spawn player!" )

    #------------------------------------------------------------------------------------       
    def _getLeaderName(self):
        """return the current leader of the arena's name
        
           if you keep track on the fly, set self._winnerName, otherwise
           override this method
        """
        return self._winnerName 

    #------------------------------------------------------------------------------------       
    def _extraEndInfo(self):
        " for derived classes to add extra data in ended msg"
        return ""
        
    #------------------------------------------------------------------------------------       
    def _buildEndEvent(self, winner ):
        """create the event of the end-arena info

        Makes the event, and calls overrideable _appendStats to 
        let derived classes alter the event
        """

        event = self._makeArenaMgrEvent( ARENA_ENDED_FILTER, winner )
        
        self._appendStats( event )

        return event
        
    #------------------------------------------------------------------------------------       
    def _endArena(self,winner):
        """called when the arena is ending

        This builds broadcasts end arena event, and cleans up this arena
        """
        import Game

        if self._timerEvent != 0:
            Dispatcher.CancelEvent( self._dispatcher, self._timerEvent )
            self._timerEvent = 0
        self._hasArenaEnded = True   
        
        # let derived classes know going out
        for playerKey in self.players.keys():
            self._playerExiting(playerKey,self.players[playerKey])
        
        self._endTime = time.time()
        event = self._buildEndEvent( MSG_WINNER % (winner) )
        self._broadcastEvent( event  )
        
        if(len(self.players) > 0):
            self.broadcastPlayerSound(SoundGlobals.SOUND_KCVO_GAMEOVER, self.players.keys()[0]) ##playerKey doesn't matter, just pick first in list
        
        

        if self._timerEvent != 0:
            Dispatcher.CancelEvent( self._dispatcher, self._timerEvent )
            self._timerEvent = 0

        # do this last since this is our only reference, and gc may kill us
        # remove ourselves from the parent list
        self._parentList.remove(self)
        

    #------------------------------------------------------------------------------------       
    def _playerExiting( self, playerKey, playerStats):
        self.removePlayerArenaInventory(playerKey)
        if(self.players[playerKey].isReady == False):
            del self.players[playerKey]
        

    #------------------------------------------------------------------------------------
    def unarmPlayerUGCAccessories(self, playerKey):
        import Game
        
        playerNetID = NetIdFromKey( playerKey )
        playerObj = Game.GetPlayerByNetId( self._game, playerNetID )
        playerStats = self.players[ playerKey ]
        
        if self.noWeapons==False:
            
            Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "   >> Weapon used in game: unarm UGC accessories for NETID " + str(playerNetID))
			
            invCount = int(GetSet.GetArrayCount( playerObj, PlayerIds.INVENTORY ))
            for invIdx in range(invCount):
                invObj = GetSet.GetObjectInArray( playerObj, PlayerIds.INVENTORY, invIdx )
                if invObj!=None:
                    itemObj = GetSet.GetObject( invObj, InventoryIds.ITEMDATA )
                    isArmed = GetSet.GetNumber( invObj, InventoryIds.ARMED )
                    if isArmed!=0 and itemObj!=None:
                        globalId = GetSet.GetNumber( itemObj, ItemIds.GLOBALID )
                        useType = GetSet.GetNumber( itemObj, ItemIds.USETYPE )
                        if useType==KEP.USE_TYPE_EQUIP and globalId>=KEP.GLID_UGC_BASE:	# UGC Accessories
                            # Temporarily confiscated: unarm from player now. Inventory would be automatically restored from DB when player exits Kaching.
                            Game.EquipInventoryItem( self._game, playerNetID, -globalId )
                            Dispatcher.LogMessage(KEP.dispatcher, KEP.TRACE_LOG_LEVEL, "   ** Unarmed item [" + str(globalId) + "] for NETID " + str(playerNetID))
                            
                else:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "   ** Unable to obtain inv obj [" + str(invIdx) + "] for NETID " + str(playerNetID))

    #------------------------------------------------------------------------------------
    def setupPlayerArenaInventory(self, playerKey):
        import Game
        playerNetID = NetIdFromKey( playerKey )
        playerObj = Game.GetPlayerByNetId( self._game, playerNetID )
        playerStats = self.players[ playerKey ]
        playerStats.equipment = ArenaEquipment.ArenaEquipment( playerObj, playerStats, self._game, self, self.noWeapons )
        if(self.noWeapons == False):
            playerStats.equipment.addAmmo(playerObj, playerNetID, ArenaEquipment.MG_AMMO, 9999)
            playerStats.equipment.equipGun(playerNetID, ArenaEquipment.MACHINEGUN)
        playerStats.equipment.equipHeadIcon( playerNetID, ArenaEquipment.HEAD_GEM ) 
        ##----- playerStats.equipment.equipShield( playerNetID, True )
                
    #------------------------------------------------------------------------------------
    def removePlayerArenaInventory(self, playerKey):
        import Game
        
        playerStats = self.players[ playerKey ]
        playerNetID = NetIdFromKey( playerKey )
        playerObj = Game.GetPlayerByName( self._game, playerStats.name, 0, 0 )
        if ( playerStats.equipment != None and playerStats.equipment != 0 ):
            playerStats.equipment.removeWeapons(playerObj)
        playerStats.equipment = None

    #------------------------------------------------------------------------------------       
    def _appendStats(self, event ):

        Event.EncodeNumber( event, len(self.players) + len(self.exitedPlayers) )
        
        for playerKey in self.players:
            # send out stats
            self.players[playerKey].EncodeStats( event )
            
        for playerNetId in self.exitedPlayers:
            # send out stats
            self.exitedPlayers[playerNetId].EncodeStats( event )
        
    #------------------------------------------------------------------------------------       
    def _sendKilledPlayerStatList(self,playerNetId,winnerMsg):
        " when one player exits an arena, show them stats"
        
        event = self._buildEndEvent( winnerMsg )
        
        # send ended event
        self._sendEvent( playerNetId, event )
        
    #------------------------------------------------------------------------------------       
    def _broadcastMessage( self, playerNetId, message, type ):
        "broadcast a message to the players"

        import Game
        prefix = ""
        if playerNetId != 0:
            playerObj = Game.GetPlayerByNetId( self._game, playerNetId )
            if  playerObj == 0: 
                KEP.debugPrint( "Player not found for netid", playerNetId)
                # TODO: log message
                return ""
            charName = GetSet.GetString( playerObj, PlayerIds.CHARNAME )
            prefix = charName
        msg = prefix + message
        Game.BroadcastMessage( self._game, 
                    0,  # radius of zero, entire zone
                    msg,
                    0, 0, 0, # self.centerX,self.centerY,self.centerZ,
                    int(self.channelId), # int(self.zoneIndex),
                    0, # int(self.netid),
                    type,
                    0 )
        return prefix
            
    #------------------------------------------------------------------------------------       
    def _broadcastArenaMessage( self, message ):
        for playerKey in self.players:
            playerNetId = NetIdFromKey(playerKey)
            self._sendMessage(playerNetId, message)
        
    #------------------------------------------------------------------------------------       
    def _sendMessage( self, playerNetId, message, type = ChatType.Talk ):
        "send a message to just this player"
        import Game
        
        playerObj = Game.GetPlayerByNetId( self._game, playerNetId )
        if  playerObj == 0: 
            Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "   >> Player not found for netid" + str(playerNetId))
            # TODO: log message
            return KEP.EPR_OK
        Game.SendTextMessage( self._game, playerObj, message, type )

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function"""
    KEP.gLoggingDispatcher = dispatcher
    pass # do nothing, just do this so dispatcher knows about us

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaBase.GAME_ADMIN_ENABLED_EVENT, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "ArenaManagerEvent", KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaBase.EQUIP_SHIELD_EVENT, KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaBase.GAME_ADMIN_START_ARENA_EVENT, KEP.MED_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaBase.LOBBY_CHAT_EVENT, KEP.HIGH_PRIO )
    

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function
    
    called from C to give us a chance to cleanup before shutdown or re-init
    """
    return KEP.getDependentModules(__name__)


