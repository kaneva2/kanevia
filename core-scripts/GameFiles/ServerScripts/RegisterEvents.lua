--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile( "..\\Scripts\\PlayerIds.lua" )
dofile( "..\\Scripts\\KEP_Server.lua" )
dofile( "..\\Scripts\\CMD.lua" )
dofile( "..\\Scripts\\ServerItemIds.lua" )

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = debugLevel

	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "FameEarnedEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "PlayerRavedEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "ClientTickler", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "FameLevelUpEvent", KEP.MED_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, "FameBadgeAwardedEvent", KEP.MED_PRIO )
end

