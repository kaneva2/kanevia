#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import DatabaseInterface

gDBAccessor = None

#-------------------------------------------------------------------
def sqlGetCrewCount( userId ):
    """get the crew count based off of user_id"""
    global gDBAccessor
    
    result = []

    count = 0

    sql = "SELECT crew_count FROM kaneva.crews WHERE user_id = @userId"
    sql = sql.replace( "@userId", str(userId) )

    gDBAccessor.fetchRows( sql, result )

    if( len(result) > 0 ):
        count = result[0][0]
    
    return count

#-------------------------------------------------------------------
def sqlGetCrewRankRewards( ):
    """get the reward list"""
    global gDBAccessor
    
    result = []

    sql = "SELECT rewards FROM fame.packets WHERE name LIKE '%Job Level%'"

    gDBAccessor.fetchRows( sql, result )

    return result

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    pass

#-------------------------------------------------------------------
def InitializeKEPEventHandlers(dispatcher, handler, dbgLevel):
    """Called from C to initialize all the handlers in this script"""
    global gDBAccessor

    gDBAccessor = DatabaseInterface.DBAccess("QuestEmployDb")
    
