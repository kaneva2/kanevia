--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile( "..\\Scripts\\KEP_Server.lua" )
dofile( "..\\Scripts\\GameGlobals_Server.lua" )

gDeedData = {}
gDeedData[GameGlobals.UV_PORTAL_CASINO] = {0, GameGlobals.ZONE_CASINO}
gDeedData[GameGlobals.UV_PORTAL_THEATRE] = {0, GameGlobals.ZONE_THEATRE}
gDeedData[GameGlobals.UV_APT_STUDIO] = {GameGlobals.ITEM_APT_STUDIO, GameGlobals.ZONE_APT_02}
gDeedData[GameGlobals.UV_APT_1BDR] = {GameGlobals.ITEM_APT_1BDR, GameGlobals.ZONE_APT_03}
gDeedData[GameGlobals.UV_APT_FLAT] = {GameGlobals.ITEM_APT_FLAT, GameGlobals.ZONE_APT_04}
gDeedData[GameGlobals.UV_APT_LARGE1BDR] = {GameGlobals.ITEM_APT_LARGE1BDR, GameGlobals.ZONE_APT_01}
gDeedData[GameGlobals.UV_APT_LOFT] = {GameGlobals.ITEM_APT_LOFT, GameGlobals.ZONE_APT_LOFT}
gDeedData[GameGlobals.UV_APT_GREEN_ACRES] = {GameGlobals.ITEM_APT_GREEN_ACRES, GameGlobals.ZONE_GREEN_ACRES}
gDeedData[GameGlobals.UV_BB_BBTHEATER] = {GameGlobals.ITEM_BB_THEATER, GameGlobals.ZONE_BBTHEATER}
gDeedData[GameGlobals.UV_BB_CAFE_LARGE] = {GameGlobals.ITEM_BB_LG_CAFE, GameGlobals.ZONE_CAFE_LARGE}
gDeedData[GameGlobals.UV_BB_CAFE_SMALL] = {GameGlobals.ITEM_BB_SM_CAFE, GameGlobals.ZONE_CAFE_SMALL}
gDeedData[GameGlobals.UV_BB_CONFERENCE] = {GameGlobals.ITEM_BB_CONF_ROOM, GameGlobals.ZONE_CONFERENCE}
gDeedData[GameGlobals.UV_BB_DANCEBAR] = {GameGlobals.ITEM_BB_DANCE_BAR, GameGlobals.ZONE_DANCEBAR}
gDeedData[GameGlobals.UV_BB_GREEN_ACRES] = {GameGlobals.ITEM_BB_GREEN_ACRES, GameGlobals.ZONE_GREEN_ACRES}
gDeedData[GameGlobals.UV_BB_NIGHTCLUB] = {GameGlobals.ITEM_BB_NIGHT_CLUB, GameGlobals.ZONE_NIGHTCLUB}
gDeedData[GameGlobals.UV_BB_THEATRE] = {GameGlobals.ITEM_BB_THEATER, GameGlobals.ZONE_THEATRE}
gDeedData[GameGlobals.UV_APT_32x64x64] = {GameGlobals.ITEM_APT_32x64x64, GameGlobals.ZONE_APT_32x64x64}
gDeedData[GameGlobals.UV_APT_MANOR_HOME] = {GameGlobals.ITEM_APT_MANOR_HOME, GameGlobals.ZONE_APT_MANOR_HOME}
gDeedData[GameGlobals.UV_APT_CITY_LOFT] = {GameGlobals.ITEM_APT_CITY_LOFT, GameGlobals.ZONE_APT_CITY_LOFT}
gDeedData[GameGlobals.UV_APT_PREM_ISLAND] = {GameGlobals.ITEM_APT_PREM_ISLAND, GameGlobals.ZONE_PREM_ISLAND}
gDeedData[GameGlobals.UV_BB_PREM_ISLAND] = {GameGlobals.ITEM_BB_PREM_ISLAND, GameGlobals.ZONE_PREM_ISLAND}
gDeedData[GameGlobals.UV_APT_ISLAND] = {GameGlobals.ITEM_APT_ISLAND, GameGlobals.ZONE_ISLAND}
gDeedData[GameGlobals.UV_BB_ISLAND] = {GameGlobals.ITEM_BB_ISLAND, GameGlobals.ZONE_ISLAND}
gDeedData[GameGlobals.UV_BB_DELUXE_COFFEE_HOUSE] = {GameGlobals.ITEM_BB_DELUXE_COFFEE_HOUSE, GameGlobals.ZONE_CITY_BLOCK}
gDeedData[GameGlobals.UV_BB_CITY_BLOCK] = {GameGlobals.ITEM_BB_CITY_BLOCK, GameGlobals.ZONE_CITY_BLOCK}
gDeedData[GameGlobals.UV_APT_OUTDOOR_RETREAT] = {GameGlobals.ITEM_APT_OUTDOOR_RETREAT, GameGlobals.ZONE_OUTDOOR_RETREAT}
gDeedData[GameGlobals.UV_BB_OUTDOOR_RETREAT] = {GameGlobals.ITEM_BB_OUTDOOR_RETREAT, GameGlobals.ZONE_OUTDOOR_RETREAT}
gDeedData[GameGlobals.UV_APT_PREM_OUTDOOR_RETREAT] = {GameGlobals.ITEM_APT_PREM_OUTDOOR_RETREAT, GameGlobals.ZONE_PREM_OUTDOOR_RETREAT}
gDeedData[GameGlobals.UV_BB_PREM_OUTDOOR_RETREAT] = {GameGlobals.ITEM_BB_PREM_OUTDOOR_RETREAT, GameGlobals.ZONE_PREM_OUTDOOR_RETREAT}
gDeedData[GameGlobals.UV_APT_FASHIONISTA_FLAT] = {GameGlobals.ITEM_APT_FASHIONISTA_FLAT, GameGlobals.ZONE_FASHIONISTA_FLAT}
gDeedData[GameGlobals.UV_APT_TRADITIONAL_LIVING] = {GameGlobals.ITEM_APT_TRADITIONAL_LIVING, GameGlobals.ZONE_TRADITIONAL_LIVING}
gDeedData[GameGlobals.UV_APT_CONTEMPORARY_CONDO] = {GameGlobals.ITEM_APT_CONTEMPORARY_CONDO, GameGlobals.ZONE_CONTEMPORARY_CONDO}
gDeedData[GameGlobals.UV_APT_GAMERS_DEN] = {GameGlobals.ITEM_APT_GAMERS_DEN, GameGlobals.ZONE_GAMERS_DEN}
gDeedData[GameGlobals.UV_APT_SPORTS_ZONE] = {GameGlobals.ITEM_APT_SPORTS_ZONE, GameGlobals.ZONE_SPORTS_ZONE}
gDeedData[GameGlobals.UV_APT_BEACH_HOUSE] = {GameGlobals.ITEM_APT_BEACH_HOUSE, GameGlobals.ZONE_BEACH_HOUSE}
gDeedData[GameGlobals.UV_BB_BEACH_HOUSE] = {GameGlobals.ITEM_BB_BEACH_HOUSE, GameGlobals.ZONE_BEACH_HOUSE}

ITEM = 1
ZONE = 2

NEW_DEED_TYPE = 203
CUSTOM_DEED_TYPE = 209

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = debugLevel
	return ParentHandler_RegisterEventHandler( dispatcher, handler, "handlePlayerUseDeed", 1,0,0,0, "PlayerUseItem", KEP.MED_PRIO )
end
		
function changeZone( dispatcher, idReference, inventoryItemId, apartmentId, upgradeType, invType )
    e = Dispatcher_MakeEvent( dispatcher, "ZoneChangeEvent" )
    Event_EncodeNumber( e, idReference )
    Event_EncodeNumber( e, inventoryItemId )
    Event_EncodeNumber( e, apartmentId )
    Event_EncodeNumber( e, upgradeType )
    Event_EncodeNumber( e, invType )
    Event_EncodeNumber( e, 1 )
    
	Dispatcher_QueueEvent( dispatcher, e )
end

function handlePlayerUseDeed( dispatcher, fromNetid, event, eventid, filter, objectid)
    deedFilter = filter

    idReference = Event_DecodeNumber( event )
    idUseValue = Event_DecodeNumber( event )
    invType = Event_DecodeNumber( event )
    globalid = Event_DecodeNumber( event )
    useType = Event_DecodeNumber( event )

	ParentHandler_LogMessage( KEP.parent, KEP.INFO_LOG_LEVEL, "idReference: " .. tostring(idReference) .. " idUseValue: " .. tostring(idUseValue) .. " invType: " .. tostring(invType) .. " globalid: " .. tostring(globalid) .. " useType: " .. tostring(useType) )

	if Game_GetPlayerByNetId == nil then
		Dispatcher_GetFunctions( KEP.dispatcher, KEP.GET_SERVER_FN )
	end
	game = Dispatcher_GetGame(dispatcher)
	
    player1 = Game_GetPlayerByNetId( game, fromNetid )
    if player1 == 0 then
        return KEP.EPR_NOT_PROCESSED
	end
   
    if deedFilter == CUSTOM_DEED_TYPE then
        changeZone( dispatcher, idReference, globalid, idUseValue, KEP.ChannelUpgrade, invType )
		ParentHandler_LogMessage( KEP.parent, KEP.INFO_LOG_LEVEL, "Using custom deed! player: " .. tostring(idReference) .. " deed: " .. tostring(globalid) )
    elseif deedFilter == NEW_DEED_TYPE then
		deed = gDeedData[idUseValue]
        if deed then
			changeZone( dispatcher, idReference, deed[ITEM], deed[ZONE], KEP.ChannelUpgrade, invType )
        else
			ParentHandler_LogMessage( KEP.parent, KEP.WARN_LOG_LEVEL, "handlePlayerUseNew deed id not found in dict. Use value: " .. tostring(idUseValue) )
		end
    end
    return KEP.EPR_CONSUMED
end