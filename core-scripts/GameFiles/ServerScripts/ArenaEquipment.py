#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import time

import Event
import Dispatcher
import GameGlobals
import KEP
import SoundGlobals
import WoK

#weapon and ammo GLIDs
##----------------- MALE ####
GEM_MALE_BLUE               = 3035      ## Diamond 001
GEM_MALE_RED                = 3036      ## Diamond 002
COIN_MALE_BLUE              = 3070      
COIN_MALE_RED               = 3069      
BODY_SHIELD_MALE            = 3038
GUN_BFG_MALE                = 3039
GUN_MG_MALE                 = 3040      ## Machine Gun
GUN_SG_MALE                 = 3041      ## Shotgun
AMMO_BFG_MALE               = 3042
AMMO_MG_MALE                = 3043
AMMO_SG_MALE                = 3044
##--------------- FEMALE ####
GEM_FEMALE_BLUE             = 3046      ## Diamond 001
GEM_FEMALE_RED              = 3047      ## Diamond 002
COIN_FEMALE_BLUE            = 3074      
COIN_FEMALE_RED             = 3073      
BODY_SHIELD_FEMALE          = 3045
GUN_BFG_FEMALE              = 3049
GUN_MG_FEMALE               = 3050      ## Machine Gun
GUN_SG_FEMALE               = 3051      ## Shotgun
AMMO_BFG_FEMALE             = 3052
AMMO_MG_FEMALE              = 3053
AMMO_SG_FEMALE              = 3054

#----- Convenience Labels -----#
HEAD_GEM                    = 100
HEAD_COIN                   = 101
BODY_SHIELD                 = 200
MACHINEGUN                  = 0
SHOTGUN                     = 1
BFG                         = 2
MG_AMMO                     = 0
SG_AMMO                     = 1
BFG_AMMO                    = 2
AMMO_MAX                    = 0
AMMO_CUR                    = 1
AMMO_CLIPS                  = 2


#------------------------------------------------------------------------------------     
class ArenaEquipment(object):
    def __init__( self, playerObj, playerStats, gameInstance, arena, noWeapons ):
        self._game = gameInstance
        self._arena = arena
        self._ownerStats = playerStats
        self._gem = ArenaGem(self._ownerStats)
        self._coin = ArenaCoin(self._ownerStats)
        self._shield = ArenaShield(self._ownerStats)
        self._guns = [ ]
        if(noWeapons == False):
            self._guns = [ ArenaMachineGun(self._ownerStats), ArenaShotGun(self._ownerStats), ArenaBFG(self._ownerStats) ]
        self._ammos = [ ArenaMachineGunAmmo(self._ownerStats), ArenaShotGunAmmo(self._ownerStats), ArenaBFGAmmo(self._ownerStats) ]
        self.addWeapons(playerObj)
    
    def addWeapons(self, playerObj):
        import Game
        Game.AddInventoryItemToPlayer( self._game, playerObj, self._gem._GLID, 1, 0)
        Game.AddInventoryItemToPlayer( self._game, playerObj, self._coin._GLID, 1, 0)
        Game.AddInventoryItemToPlayer( self._game, playerObj, self._shield._GLID, 1, 0)
        for gun in self._guns:
            Game.AddInventoryItemToPlayer( self._game, playerObj, gun._GLID, 1, 0)
        #Game.AddInventoryItemToPlayer( self._game, playerObj, self._ammos[MACHINEGUN]._GLID, 9999, 0)   
          
    def removeWeapons(self, playerObj):
        import Game
        Game.RemoveInventoryItemFromPlayer( self._game, playerObj, self._gem._GLID, -1, 1, GameGlobals.IT_NORMAL )
        Game.RemoveInventoryItemFromPlayer( self._game, playerObj, self._coin._GLID, -1, 1, GameGlobals.IT_NORMAL )
        Game.RemoveInventoryItemFromPlayer( self._game, playerObj, self._shield._GLID, -1, 1, GameGlobals.IT_NORMAL )
        for gun in self._guns:
            Game.RemoveInventoryItemFromPlayer( self._game, playerObj, gun._GLID, -1, 1, GameGlobals.IT_NORMAL )
        for ammo in self._ammos:
            Game.RemoveInventoryItemFromPlayer( self._game, playerObj, ammo._GLID, -1, 1, GameGlobals.IT_NORMAL )
    
    def equipHeadIcon(self, playerNetId, iconType):
        import Game
        if iconType == HEAD_GEM:
            Game.EquipInventoryItem( self._game, playerNetId, self._gem._GLID )
        elif iconType == HEAD_COIN:
            Game.EquipInventoryItem( self._game, playerNetId, self._coin._GLID )
    
    def equipShield(self, playerNetId, equip, delay):
        import Game
        if equip == True:
            self._shield.setTimeStamp( delay )
            Game.EquipInventoryItem( self._game, playerNetId, self._shield._GLID )
        else:
            if self._shield.checkLastTimeStamp():
                Game.EquipInventoryItem( self._game, playerNetId, -self._shield._GLID )
    
    def equipGun(self, playerNetId, gunType):
        import Game
        index = gunType
        return self.equipGunByIndex(index, playerNetId)
    
    def equipGunByIndex(self, index, playerNetId):
        import Game
        if len(self._guns) - 1 < int(index):
            return False
        if self.validateWeaponByIndex(index) == False:
            return False
        success = self._guns[int(index)].activate(self._game, playerNetId, self.getCurrentWeapon() == None)
        if success:
            for count in range(3):
                if count != index:
                    self._guns[count].deactivate()
            self.playSoundEquip(index, playerNetId)
        else:
            Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "Cannot equip weapon at this time. Don't look at me!  Hard-coded, hard-coded...")
        return success
    
    def addAmmo(self, playerObj, playerNetId, ammoType, amount = 1):
        import Game
        index = ammoType
        if index >= 0 and index < len(self._guns):
            Game.AddInventoryItemToPlayer( self._game, playerObj, self._ammos[index]._GLID, amount, 0)
            reload = self._guns[index].addClip(amount)
            if(reload):
                Game.ReloadInventoryItem( self._game, playerNetId, self._guns[index]._GLID )

    def getCurrentWeapon(self):
        for gun in self._guns:
            if gun._active:
                return gun
        return None
            
    def setCurrentWeaponAmmoInfo(self, playerNetId, max, current):
        import Game
        curGun = self.getCurrentWeapon()
        if ( curGun != None ):
            gunIndex = self._guns.index(curGun)  ## relying on ammo indices to match respective gun indices
            numClips = Game.GetInventoryItemQty( self._game, playerNetId, self._ammos[gunIndex]._GLID )
            oldAmmoCount = self.getCurrentWeaponAmmoInfo()
            curGun.setAmmoCount(max, current, numClips)
            self.playSoundAmmo(gunIndex, playerNetId, oldAmmoCount, self.getCurrentWeaponAmmoInfo())
    
    
    def setWeaponAmmoInfo(self, playerNetId, max, current, glid):
        import Game
        
        for gun in self._guns:
            gunIndex = self._guns.index(gun)
            if self._guns[gunIndex]._GLID == glid:
                numClips = Game.GetInventoryItemQty( self._game, playerNetId, self._ammos[gunIndex]._GLID )
                oldAmmoCount = self.getWeaponAmmoInfo(glid)
                self._guns[gunIndex].setAmmoCount(max, current, numClips)
                if self._guns[gunIndex]._active:
                    self.playSoundAmmo(gunIndex, playerNetId, oldAmmoCount, self.getWeaponAmmoInfo(glid))
                return True
        return False
    
    def getWeaponAmmoInfo(self, glid):
        for gun in self._guns:
            gunIndex = self._guns.index(gun)
            if self._guns[gunIndex]._GLID == glid:
                return self._guns[gunIndex].getAmmoCount()
        return (0, 0, 0)
          
    def getCurrentWeaponAmmoInfo(self):
        curGun = self.getCurrentWeapon()
        if ( curGun != None ):
            return curGun.getAmmoCount()
        else:
            return (0, 0, 0)
        
    def validateCurrentWeapon(self):
        ammoTup = self.getCurrentWeaponAmmoInfo()
        if ammoTup[AMMO_CUR] == 0 and ammoTup[AMMO_CLIPS] == 0:
            return False
        return True
            
    def validateWeaponByIndex(self, index):
        ammoTup = self._guns[int(index)].getAmmoCount()
        if ammoTup[AMMO_CUR] == 0 and ammoTup[AMMO_CLIPS] == 0:
            return False
        return True
                
    def encodeWeaponInfo(self, event):
        gunIndex = 0
        curGun = self.getCurrentWeapon()
        if ( curGun != None ):
            gunIndex = self._guns.index(curGun) + 1  ##index from 1 (not 0) for sending to client
        Event.EncodeNumber( event, gunIndex ) ## machine gun current charge
        
        gunType = MACHINEGUN
        tup = [0, 0, 0]
        if(gunType < len(self._guns)):
            tup = self._guns[gunType].getAmmoCount()
        Event.EncodeNumber( event, tup[AMMO_CUR] ) ## machine gun current charge
        Event.EncodeNumber( event, tup[AMMO_MAX] ) ## machine gun max charge
        Event.EncodeNumber( event, tup[AMMO_CLIPS] ) ## machine gun total clips
        
        gunType = SHOTGUN
        tup = [0, 0, 0]
        if(gunType < len(self._guns)):
            tup = self._guns[gunType].getAmmoCount()
        Event.EncodeNumber( event, tup[AMMO_CUR] ) ## shotgun current charge
        Event.EncodeNumber( event, tup[AMMO_MAX] ) ## shotgun max charge
        Event.EncodeNumber( event, tup[AMMO_CLIPS] ) ## shotgun total clips
        
        gunType = BFG
        tup = [0, 0, 0]
        if(gunType < len(self._guns)):
            tup = self._guns[gunType].getAmmoCount()
        Event.EncodeNumber( event, tup[AMMO_CUR] ) ## BFG current charge
        Event.EncodeNumber( event, tup[AMMO_MAX] ) ## BFG max charge
        Event.EncodeNumber( event, tup[AMMO_CLIPS] ) ## BFG total clips
        return event
    
    def playSoundEquip(self, equipID, playerNetId):
        if self._arena != None:
            sound = -1
            if equipID == MACHINEGUN:
                sound = SoundGlobals.SOUND_KACHING_RELOAD1
            if equipID == SHOTGUN:
                sound = SoundGlobals.SOUND_KACHING_RELOAD2
            if equipID == BFG:
                sound = SoundGlobals.SOUND_KACHING_RELOAD3
            if sound != -1:
                self._arena.broadcastPlayerSound(sound, 0, playerNetId)
                
    def playSoundAmmo(self, equipID, playerNetId, oldAmmo, newAmmo):
        if self._arena != None:
            sound = -1
            if oldAmmo[AMMO_CLIPS] > newAmmo[AMMO_CLIPS]:
                if equipID == MACHINEGUN:
                    sound = SoundGlobals.SOUND_KACHING_RELOAD1
                if equipID == SHOTGUN:
                    sound = SoundGlobals.SOUND_KACHING_RELOAD2
                if equipID == BFG:
                    sound = SoundGlobals.SOUND_KACHING_RELOAD3
            elif oldAmmo[AMMO_CUR] > newAmmo[AMMO_CUR]:
                if equipID == MACHINEGUN:
                    sound = SoundGlobals.SOUND_KACHING_FIRE1
                if equipID == SHOTGUN:
                    sound = SoundGlobals.SOUND_KACHING_FIRE2
                if equipID == BFG:
                    sound = SoundGlobals.SOUND_KACHING_FIRE3
            if sound != -1:
                self._arena.broadcastPlayerSound(sound, 0, playerNetId)

#------------------------------------------------------------------------------------        
class ArenaEquipableItem(object):
    def __init__( self ): 
        self._active = False
        self._GLID = -1         

#------------------------------------------------------------------------------------         
class ArenaGem(ArenaEquipableItem):
    def __init__( self, playerStats ):
        super( ArenaGem, self ).__init__()
        if playerStats.teamId == WoK.BLUE_TEAMID:
            if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
                self._GLID = GEM_MALE_BLUE
            else:
                self._GLID = GEM_FEMALE_BLUE
        elif playerStats.teamId == WoK.RED_TEAMID:
            if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
                self._GLID = GEM_MALE_RED
            else:
                self._GLID = GEM_FEMALE_RED
            
class ArenaCoin(ArenaEquipableItem):
    def __init__( self, playerStats ):
        super( ArenaCoin, self ).__init__()
        if playerStats.teamId == WoK.BLUE_TEAMID:
            if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
                self._GLID = COIN_MALE_BLUE
            else:
                self._GLID = COIN_FEMALE_BLUE
        elif playerStats.teamId == WoK.RED_TEAMID:
            if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
                self._GLID = COIN_MALE_RED
            else:
                self._GLID = COIN_FEMALE_RED
        
class ArenaShield(ArenaEquipableItem):
    def __init__( self, playerStats ):
        super( ArenaShield, self ).__init__()
        if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
            self._GLID = BODY_SHIELD_MALE
        else:
            self._GLID = BODY_SHIELD_FEMALE
        self._timeStamp = -1
        
    def setTimeStamp( self, delay ):
        import Game
        curTime = time.time()
        self._timeStamp = curTime + ( delay )
        #Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "SHIELD END TIME STAMP: " + str( self._timeStamp ) + "  (curTime): " + str(curTime) ) 
        
    def checkLastTimeStamp( self ):
        import Game
        curTime = time.time()
        #Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "SHIELD CUR TIME: " + str( curTime ) ) 
        #Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "SHIELD DELTA: " + str( self._timeStamp - curTime ) ) 
        if self._timeStamp - curTime < 1:
            return True
        else:
            return False

#------------------------------------------------------------------------------------        
class ArenaGun(ArenaEquipableItem):
    def __init__( self ):
        self._maxAmmo = 0
        self._curAmmo = 0
        self._curClips = 0
        super( ArenaGun, self ).__init__()
        
    def activate( self, gameInst, playerNetId, firstGun ):
        import Game
        ret = Game.EquipInventoryItem( gameInst, playerNetId, self._GLID )  ## returns UNEQUIP GLID if successful, 0.0 if unsuccessful OR nothing to unequip
        if ( firstGun or ret != 0.0 ):   
            self._active = True
            return True
        return False  
        
    def deactivate( self ):
        ## doesn't actually unequip from inventory
        self._active = False
        
    def setAmmoCount(self, max, current, numClips):
        self._maxAmmo = max
        self._curAmmo = current
        self._curClips = numClips
        
    def getAmmoCount(self):
        ammoTup = (self._maxAmmo, self._curAmmo, self._curClips )
        return ammoTup
    
    def addClip(self, numClips):
        reload = False
        if self._curClips == 0 and numClips > 0 and self._curAmmo == 0:
            reload = True
        self._curClips += numClips        
        return reload       
        
class ArenaBFG(ArenaGun):
    def __init__( self, playerStats ):
        super( ArenaBFG, self ).__init__()
        if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
            self._GLID = GUN_BFG_MALE
        else:
            self._GLID = GUN_BFG_FEMALE
        
class ArenaMachineGun(ArenaGun):
    def __init__( self, playerStats ):
        super( ArenaMachineGun, self ).__init__()
        if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
            self._GLID = GUN_MG_MALE
        else:
            self._GLID = GUN_MG_FEMALE
        
class ArenaShotGun(ArenaGun):
    def __init__( self, playerStats ):
        super( ArenaShotGun, self ).__init__()
        if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
            self._GLID = GUN_SG_MALE
        else:
            self._GLID = GUN_SG_FEMALE

#------------------------------------------------------------------------------------         
class ArenaAmmo(ArenaEquipableItem):
    def __init__( self ):
        super( ArenaAmmo, self ).__init__()

class ArenaBFGAmmo(ArenaAmmo):
    def __init__( self, playerStats ):
        super( ArenaBFGAmmo, self ).__init__()
        if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
            self._GLID = AMMO_BFG_MALE
        else:
            self._GLID = AMMO_BFG_FEMALE
        
class ArenaMachineGunAmmo(ArenaAmmo):
    def __init__( self, playerStats ):
        super( ArenaMachineGunAmmo, self ).__init__()
        if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
            self._GLID = AMMO_MG_MALE
        else:
            self._GLID = AMMO_MG_FEMALE
        
class ArenaShotGunAmmo(ArenaAmmo):
    def __init__( self, playerStats ):
        super( ArenaShotGunAmmo, self ).__init__()
        if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
            self._GLID = AMMO_SG_MALE
        else:
            self._GLID = AMMO_SG_FEMALE
        
  
