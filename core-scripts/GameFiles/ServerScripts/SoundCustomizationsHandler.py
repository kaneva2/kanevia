#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP
import xml.dom.minidom

# C functions
import InstanceId
import Dispatcher
import Event
import GetSet
import ParentHandler

import SoundCustomizationsDb
import WorkerThread

import PlayerIds
import ChatType

SOUND_CUSTOMIZATION_EVENT = "SoundCustomizationRequestEvent"
SOUND_CUSTOMIZATION_REPLY_EVENT = "SoundCustomizationReplyEvent"
SOUND_CUSTOMIZATION_UPDATE_EVENT = "UpdateSoundCustomizationEvent"
SOUND_CUSTOMIZATION_REMOVE_EVENT = "RemoveSoundCustomizationEvent"


CHUNK_SIZE = 25 ## Number of objects to send at once

gDispatcher = 0
gParent = 0
gBackgroundThread = None
gRootTag = None

def logMsg( level, msg ):
    #needed for logging
    import ParentHandler
    ParentHandler.LogMessage( gParent, level, msg )
    #print msg


#-------------------------------------------------------------------
def SoundRequestHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
    "Get the sounds for the user -- event filter is the zoneIndex"
    import Game

    zoneIndex = filter
    instanceId = Event.DecodeNumber( event )

    zoneType = InstanceId.GetType(zoneIndex)

    if zoneType == KEP.CI_PERMANENT  or zoneType == KEP.CI_ARENA:
        instanceId = 1
        if zoneType == KEP.CI_ARENA:
            zoneIndex = InstanceId.MakeId(InstanceId.GetId(zoneIndex), 0, InstanceId.GetType(zoneIndex)) #TEMP

    logMsg(KEP.DEBUG_LOG_LEVEL, "Request for sounds received from: "+str(event)+"\n In zoneIndex: "+str(filter)+" instanceId: "+str(instanceId) )

    gBackgroundThread.performWork(threadSoundRequest, fromNetId, int(zoneIndex), int(instanceId))

    return KEP.EPR_CONSUMED 

#-------------------------------------------------------------------
def threadSoundRequest(fromNetId, zoneIndex, instanceId):

    soundList = list()

    SoundCustomizationsDb.getSounds(zoneIndex, instanceId, soundList)

    logMsg(KEP.TRACE_LOG_LEVEL, str(soundList) )

    splitAndSend(fromNetId, soundList)


#-------------------------------------------------------------------
def splitAndSend(fromNetId, soundList):
    """ Split up the xml into chunks to stay under the DPlay packet size limit """
    global gRootTag

    soundChunk = list()

    soundCount = len(soundList)

    doc = None
    
    doc = initXMLDoc( doc )

    #print "roottag: "+str(gRootTag)
    #print "dynObjCount: "+str(dynObjCount)+"\nworldObjCount: "+str(worldObjCount)+"\n"

    ##
    ## Sounds
    ##
    start = 0
    end = CHUNK_SIZE

    if soundCount <= CHUNK_SIZE:
        generateSoundXML( doc, gRootTag, soundList )
        sendXMLToClient( fromNetId, doc.toxml() )        
    else:
        while soundCount >= CHUNK_SIZE:
            soundChunk = soundList[start:end]
            #print "start count: "+str(start)+" end count: "+str(end)+"\n"
            generateSoundXML( doc, gRootTag, soundChunk )
            #print doc.toxml()
            sendXMLToClient( fromNetId, doc.toxml() )
            del soundChunk
            del doc
            doc = None
            doc = initXMLDoc( doc )
            soundCount -= CHUNK_SIZE
            start = end
            end = end + CHUNK_SIZE

        #Send remainder of list
        if soundCount > 0:
            #print "Final batch: "+str(len(soundList[start:]))
            generateSoundXML( doc, gRootTag, soundList[start:] )
            sendXMLToClient( fromNetId, doc.toxml() )       
            del doc
            doc = None
            


#-------------------------------------------------------------------
def initXMLDoc( doc ):
    global gRootTag

    returnXML = "<Result></Result>"

    doc = xml.dom.minidom.parseString(returnXML)

    resultTags = doc.getElementsByTagName("Result")

    #initizalized above, so safe to access 0
    gRootTag = resultTags[0]    

    return doc



#-------------------------------------------------------------------
def generateSoundXML( doc, rootTag, soundList ):

    #print "\nDyno list length: "+str(len(soundList))

    for sound in soundList:

        logMsg( KEP.TRACE_LOG_LEVEL, str(sound) )
        
        Stag = doc.createElement("S")
        
        o_tag = doc.createElement("o")
        o_text = doc.createTextNode(str(sound[SoundCustomizationsDb.OBJ_PLACEMENT_ID]))
        o_tag.appendChild(o_text)
        Stag.appendChild(o_tag)
        
        p_tag = doc.createElement("p")
        p_text = doc.createTextNode(str(sound[SoundCustomizationsDb.PITCH]))
        p_tag.appendChild(p_text)
        Stag.appendChild(p_tag)
        
        g_tag = doc.createElement("g")
        g_text = doc.createTextNode(str(sound[SoundCustomizationsDb.GAIN]))
        g_tag.appendChild(g_text)
        Stag.appendChild(g_tag)
        
        m_tag = doc.createElement("m")
        m_text = doc.createTextNode(str(sound[SoundCustomizationsDb.MAX_DISTANCE]))
        m_tag.appendChild(m_text)
        Stag.appendChild(m_tag)
        
        r_tag = doc.createElement("r")
        r_text = doc.createTextNode(str(sound[SoundCustomizationsDb.ROLL_OFF]))
        r_tag.appendChild(r_text)
        Stag.appendChild(r_tag)
        
        l_tag = doc.createElement("l")
        l_text = doc.createTextNode(str(sound[SoundCustomizationsDb.LOOP]))
        l_tag.appendChild(l_text)
        Stag.appendChild(l_tag)
        
        d_tag = doc.createElement("d")
        d_text = doc.createTextNode(str(sound[SoundCustomizationsDb.LOOP_DELAY]))
        d_tag.appendChild(d_text)
        Stag.appendChild(d_tag)
        
        x_tag = doc.createElement("x")
        x_text = doc.createTextNode(str(sound[SoundCustomizationsDb.DIR_X]))
        x_tag.appendChild(x_text)
        Stag.appendChild(x_tag)
        
        y_tag = doc.createElement("y")
        y_text = doc.createTextNode(str(sound[SoundCustomizationsDb.DIR_Y]))
        y_tag.appendChild(y_text)
        Stag.appendChild(y_tag)
        
        z_tag = doc.createElement("z")
        z_text = doc.createTextNode(str(sound[SoundCustomizationsDb.DIR_Z]))
        z_tag.appendChild(z_text)
        Stag.appendChild(z_tag)
        
        og_tag = doc.createElement("og")
        og_text = doc.createTextNode(str(sound[SoundCustomizationsDb.CONE_OUTER_GAIN]))
        og_tag.appendChild(og_text)
        Stag.appendChild(og_tag)
        
        ia_tag = doc.createElement("ia")
        ia_text = doc.createTextNode(str(sound[SoundCustomizationsDb.CONE_INNER_ANGLE]))
        ia_tag.appendChild(ia_text)
        Stag.appendChild(ia_tag)
        
        oa_tag = doc.createElement("oa")
        oa_text = doc.createTextNode(str(sound[SoundCustomizationsDb.CONE_OUTER_ANGLE]))
        oa_tag.appendChild(oa_text)
        Stag.appendChild(oa_tag)
                
        rootTag.appendChild(Stag)



#-------------------------------------------------------------------
def sendXMLToClient( destination, xml ):

    e = Dispatcher.MakeEvent( gDispatcher, SOUND_CUSTOMIZATION_REPLY_EVENT )
    if e != 0:
        Event.EncodeString(e, xml)
        Event.AddTo(e, destination)
        Dispatcher.QueueEvent( gDispatcher, e )


#-------------------------------------------------------------------
def SoundUpdateHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
    """Update or insert the sound record with custom settings"""
    import Game

    #objectid is the placementid

    player = Game.GetPlayerByNetId( Dispatcher.GetGame( dispatcher ), fromNetId )
    if player != 0:
        zi = GetSet.GetNumber( player, PlayerIds.CURRENTZONEINDEX )
        zoneIndex  = int(InstanceId.GetClearedInstanceId( zi ))
        instanceId = int(InstanceId.GetInstanceId( zi ))
    else:
        return KEP.EPR_CONSUMED

    ci = GetSet.GetNumber( player, PlayerIds.CURRENTCHANNEL )

    #Game.BroadcastEvent( Dispatcher.GetGame( dispatcher ), event, ci, 0, 0, 0, 0, 0, ChatType.System, 0 )

    glid = Event.DecodeNumber( event )
    pitch = Event.DecodeNumber( event )
    gain = Event.DecodeNumber( event )
    max_distance = Event.DecodeNumber( event )
    roll_off = Event.DecodeNumber( event )
    loop = Event.DecodeNumber( event )
    loop_delay = Event.DecodeNumber( event )
    cone_outer_gain = Event.DecodeNumber( event )
    cone_inner_angle = Event.DecodeNumber( event )
    cone_outer_angle = Event.DecodeNumber( event )
    dir_x = Event.DecodeNumber( event )
    dir_y = Event.DecodeNumber( event )
    dir_z = Event.DecodeNumber( event )

    #Send to background thread for db work
    gBackgroundThread.performWork(threadSoundUpdate, fromNetId, zoneIndex, instanceId, objectid, glid, pitch, gain, max_distance, roll_off, loop, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle)

    e = Dispatcher.MakeEvent( dispatcher, SOUND_CUSTOMIZATION_UPDATE_EVENT )
    if e != 0:
        Event.SetObjectId( e, objectid )
        
        Event.EncodeNumber( e, glid )
        Event.EncodeNumber( e, pitch )
        Event.EncodeNumber( e, gain )
        Event.EncodeNumber( e, max_distance )
        Event.EncodeNumber( e, roll_off )
        Event.EncodeNumber( e, loop )
        Event.EncodeNumber( e, loop_delay )
        Event.EncodeNumber( e, cone_outer_gain )
        Event.EncodeNumber( e, cone_inner_angle )
        Event.EncodeNumber( e, cone_outer_angle )
        Event.EncodeNumber( e, dir_x )
        Event.EncodeNumber( e, dir_y )
        Event.EncodeNumber( e, dir_z )        

        Game.BroadcastEvent( Dispatcher.GetGame( dispatcher ), e, ci, 0, 0, 0, 0, 0, ChatType.System, 0 )

    
    return KEP.EPR_CONSUMED

#-------------------------------------------------------------------
def threadSoundUpdate(fromNetId, zoneIndex, instanceId, objectid, glid, pitch, gain, max_distance, roll_off, loop, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle):
    """Tell database to update it"""

    SoundCustomizationsDb.updateSounds(zoneIndex, instanceId, objectid, glid, pitch, gain, max_distance, roll_off, loop, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle)


#-------------------------------------------------------------------
def SoundRemoveHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
    """Remove sound customization from database"""

    gBackgroundThread.performWork(threadSoundRemove, objectid)

#-------------------------------------------------------------------
def threadSoundRemove(objectid):
    """Tell database to remove it"""
    
    SoundCustomizationsDb.deleteSound(objectid)

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher
    global gBackgroundThread

    gParent = parent
    gDispatcher = dispatcher


    ParentHandler.RegisterEventHandler( dispatcher, parent, SoundRequestHandler, 1,0,0,0, SOUND_CUSTOMIZATION_EVENT, KEP.MED_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, parent, SoundUpdateHandler, 1,0,0,0, SOUND_CUSTOMIZATION_UPDATE_EVENT, KEP.MED_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, parent, SoundRemoveHandler, 1,0,0,0, SOUND_CUSTOMIZATION_REMOVE_EVENT, KEP.MED_PRIO )

    gBackgroundThread = WorkerThread.WorkerThread("SoundCustomizations")
    ParentHandler.LogMessage(parent, KEP.INFO_LOG_LEVEL, str(gBackgroundThread) + " tid: "+str(gBackgroundThread._get_my_tid()))

    return  True 


#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher

    # grab instances for debugging
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, SOUND_CUSTOMIZATION_EVENT, KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, SOUND_CUSTOMIZATION_REPLY_EVENT, KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, SOUND_CUSTOMIZATION_UPDATE_EVENT, KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, SOUND_CUSTOMIZATION_REMOVE_EVENT, KEP.MED_PRIO )
    
#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give us a chance to cleanup before shutdown or re-init"
    return KEP.getDependentModules(__name__)
        
