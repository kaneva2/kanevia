#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#thread modules
import threading
import Queue

#import the C methods from the exe
import ParentHandler
import Dispatcher

import KEP

#url module
import urllib2

#global vars
gParent = None 
gDispatcher = None

QUEUE_THRESHOLD = 1000




# Support classes
#

# ACT is an acronym for Asynchronous Completion Token and implements a known pattern for allowing
# threading clients to obtain the values returned by tasks executed asynchronously
#
class ACT:
    # Note that ACT functioning correctly is predicated on the ACT being instantiated on producer thread. 
    # there is no real good way to enforce this 
    def __init__(self, taskId):
        self._lock = threading.Lock()
        self._lock.acquire()
        self._ret = None
        self._taskId = taskId
        ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, str(self) + " - initialized")

        
    def toString(self):
        return "ACT:ret=;lockstate"
        
    # Called by the threading client to obtain the results of the asynchronous call.
    # This method will block until the async task has completed.  This effectively
    # makes the the call appear to be synchronous, defeating the value of the
    # async execution.  For some applications this is acceptable.  If the client
    # must run async in respect to the tasked placed on the worker thread, we can
    # then implement a subscription framework to alert clients when the task is 
    # complete.  In this scenario, a listener is registered with the ACT.  When
    # the task is complete, the listener is notified.
    # 
    def getCompletionResults(self):
        ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, str(self) + "getCompletion results: acquiring lock")
        self._lock.acquire()
        ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, str(self) + "getCompletion results: lock acquired")
        ret = self._ret;
        self._lock.release()
        ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, str(self) + "getCompletion results: lock released. returning [" + str(ret) + "]")
        return ret;

    # Called by the worker thread, this stores the results returned by the 
    # task execute asynchronously, and releases any threads blocked, waiting
    # for the results.
    #
    def complete(self, ret):
        self._ret = ret
        self._lock.release()
        ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, str(self) + "complete: releasing lock")


#-------------------------------------------------------------------
# Class WorkerThread
#-------------------------------------------------------------------
class WorkerThread(threading.Thread):
    """Worker thread to process data"""

    requestID = 0
    initTID = True
    active = True
    stopped = False

    #-------------------------------------------------------------------
    def __init__(self, threadName, queueThreshold = QUEUE_THRESHOLD, **kwds):
        """Worker Constructor"""

        threading.Thread.__init__(self, **kwds)
        self.setDaemon(1)
        self.workRequestQueue = Queue.Queue()
        self.resultQueue = Queue.Queue()
        self.setName(threadName)
        self.queueThreshold = queueThreshold
        self.start()
        ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, self.getName()+" worker thread - initialized -  work queue: "+str(self.workRequestQueue)+" result queue: "+str(self.resultQueue))

    #-------------------------------------------------------------------
    ### TODO: Need to find out if this is returning an OS thread handle or just a python handle        
    def _get_my_tid(self):
        """determines this (self's) thread id"""        

        if not self.isAlive():
            ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, self.getName()+" worker thread - NOT ACTIVE.  Could not retrieve thread id.")
            return 0

        # do we have it cached?
        if hasattr(self, "_thread_id"):
            return self._thread_id

        # no, look for it in the _active dict
        for tid, tobj in threading._active.items():
            if tobj is self:
                self._thread_id = tid
                return tid

        return 0

    #-------------------------------------------------------------------
    def performWork(self, entryPoint, *args, **kwds):
        """Called by main thread to queue work"""
        global QUEUE_THRESHOLD
        global gDispatcher

        self.requestID += 1

        ret = ACT(self.requestID)
        self.workRequestQueue.put((self.requestID, entryPoint, args, kwds, ret ))

        if self.workRequestQueue.qsize() > self.queueThreshold:
            Dispatcher.LogAlertMessage( gDispatcher, str(self.getName())+" queue has exceeded threshold of "+str(self.queueThreshold)+", current size: "+str(self.workRequestQueue.qsize()) )

        return ret

    #-------------------------------------------------------------------
    def stop(self):
        """Called by main thread to stop thread"""
        
        self.active = False

    def stopped(self):
        """Called by main thread to make sure thread isnt working"""

        return self.stopped


    #-------------------------------------------------------------------
    def run(self):
        """Process queued work"""

        while self.active:
            try:
                
                requestID, entryPoint, args, kwds, act = self.workRequestQueue.get()

                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - gRequestsQueue size: "+str(self.workRequestQueue.qsize()))

                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - requestID: "+str(requestID))
                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - entrypoint: "+str(entryPoint))
                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - args: "+str(args))
                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - kwds: "+str(kwds))

                # Execute the queued task and store the results in the ACT
                # This will cause any thread waiting for the results to unblock
                #
                act.complete(entryPoint(*args, **kwds))
                
                if self.initTID:
                    ###ParentHandler.LogMessage( gParent, KEP.INFO_LOG_LEVEL, self.getName()+" worker thread - tid from c++: "+str(Dispatcher.GetCurrentThreadId()))
                    ParentHandler.LogMessage( gParent, KEP.INFO_LOG_LEVEL, self.getName()+" worker thread - tid: "+str(self._get_my_tid()))
                    self.initTID = False
                
                ###Determine who needs results
                ###self.resultQueue.put((requestID, entryPoint(*args, **kwds)))
                ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "Worker thread - run - gRequestQueue size: "+str(self.workRequestQueue.qsize()))

            except urllib2.HTTPError, ehttp:
                ### This type of exception will kill the thread using the general execption handler
                ### It does not like ehttp.args
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **HTTP EXCEPTION** - class name: "+str(ehttp.__class__.__name__))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **HTTP EXCEPTION** - entry point: "+str(entryPoint))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **HTTP EXCEPTION** - entry point args: "+str(args))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **HTTP EXCEPTION** - gRquestsQueue size: "+str(self.workRequestQueue.qsize()))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **HTTP EXCEPTION** - exception object: "+str(ehttp))

                # Current implementation of ACT does not support communicating exceptions to the client thread
                # for now if should be assumed that if the client thread was expecting a response and received none
                # that an exception occurred.
                #
                act.complete(None)

            except Exception, e:

                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - class name: "+str(e.__class__.__name__))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - entry point: "+str(entryPoint))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - entry point args: "+str(args))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - exception args: "+str(e.args))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - gRquestsQueue size: "+str(self.workRequestQueue.qsize()))
                ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "Worker thread - **EXCEPTION** - exception object: "+str(e))
                
                # Current implementation of ACT does not support communicating exceptions to the client thread
                # for now if should be assumed that if the client thread was expecting a response and received none
                # that an exception occurred.
                #
                act.complete(None)
                
        self.stopped = True                
    
#-------------------------------------------------------------------
# INIT EVENTS AND HANDLERS
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    """Called from C to initialize all the handlers in this script"""

    global gParent
    global gDispatcher

    gParent = handler
    gDispatcher = dispatcher


#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    #DO NOT LEAVE THIS TURNED ON IN RELEASE!!!
    #IT WILL LEAK MEMORY BECAUSE GARBAGE COLLECTION IS TURNED OFF!!! 
    #import gc
    #gc.set_debug( gc.DEBUG_LEAK )
    
    pass

