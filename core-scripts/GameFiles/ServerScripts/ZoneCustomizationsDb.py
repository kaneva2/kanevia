#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import ServerGlobals
import KEP
import time
import DatabaseInterface

gDispatcher = 0
gParent = 0
gDBAccessor = None

def logMsg( level, msg ):
    #needed for logging
    import ParentHandler
    ParentHandler.LogMessage( gParent, level, msg )
    #print msg

zoneCustomizationsSQL = "CALL get_zone_customizations( @zoneIndex, @instanceId, @x, @y, @z )"
                    
#-------------------------------------------------------------------

def getZoneCustomizations( zoneIndex, instanceId, rowList, x, y, z ):
    "get the ALL playlist given the player_id (PlayerIds.HANDLE)"

    logMsg( KEP.DEBUG_LOG_LEVEL, "getZoneCustomizations")

    sql = zoneCustomizationsSQL.replace( "@zoneIndex", str(zoneIndex) )
    sql = sql.replace( "@instanceId", str(instanceId) )
    sql = sql.replace( "@x", str(x) )
    sql = sql.replace( "@y", str(y) )
    sql = sql.replace( "@z", str(z) )

    gDBAccessor.fetchRows( sql, rowList )    


#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    global gParent
    global gDispatcher


    # grab instances for debugging
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Manage resources for blade reload"""

    return KEP.getDependentModules(__name__)

#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    """Called from C to initialize all the handlers in this script"""
    global gDBAccessor

    gDBAccessor = DatabaseInterface.DBAccess("ZoneCustomizationsDb")    

"""
    
#-------------------------------------------------------------------
# Self test
if __name__ == "__main__":

    if True:
        dynobjlist = list()
        wolist = list()

        print "Creating db i/f"
        gDBAccessor = DatabaseInterface.DBAccess("ZoneCustomizationsDb")
        print "Loading config"
        ServerGlobals.loadDbConfig()
        print "Loaded config, getting customizations"
        getZoneCustomizations(805306376, 12271, dynobjlist)
        
        print "/////////////////////////////////////////////"
        print "Got DynObj total of "+str(len(dynobjlist))
        for r in dynobjlist:
            print str( r[0] )
            print str( r[0] )[0] 
            if str( r[0] )[0] == 'p':
                print "parameter!"
"""
"""            
            print "---------------------------------------------"
            print "    OBJ_PLACEMENT_ID_DO " + str(r[KEP.OBJ_PLACEMENT_ID_DO])
            print "    OBJ_ID_DO  " + str(r[KEP.OBJ_ID_DO ]) 
            print "    TEX_ASSET_ID_DO  " + str(r[KEP.TEX_ASSET_ID_DO ])
            print "    PLAYER_ID_DO  " + str(r[KEP.PLAYER_ID_DO ])
            print "    FRIEND_ID_DO  " + str(r[KEP.FRIEND_ID_DO ])
            print "    POS_X_DO  " + str(r[KEP.POS_X_DO] )
            print "    POS_Y_DO  " + str(r[KEP.POS_Y_DO ])
            print "    POS_Z_DO  " + str(r[KEP.POS_Z_DO ])
            print "    DIR_X_DO  " + str(r[KEP.DIR_X_DO ])
            print "    DIR_Y_DO  " + str(r[KEP.DIR_Y_DO ])
            print "    DIR_Z_DO  " + str(r[KEP.DIR_Z_DO ])
            print "    SLD_X_DO  " + str(r[KEP.SLD_X_DO ])
            print "    SLD_Y_DO  " + str(r[KEP.SLD_Y_DO ])
            print "    SLD_Z_DO  " + str(r[KEP.SLD_Z_DO ])
            print "    SWF_NAME_DO  " + str(r[KEP.SWF_NAME_DO ])
            print "    SWF_PARAM_DO  " + str(r[KEP.SWF_PARAM_DO ])
            print "---------------------------------------------"
        print "/////////////////////////////////////////////"
        print "Got WorldObj total of "+str(len(wolist))
        for r in wolist:
            print "---------------------------------------------"
            print "    WORLD_OBJ_ID_WO " + str(r[KEP.WORLD_OBJ_ID_WO])
            print "    ASSET_ID_WO  " + str(r[KEP.ASSET_ID_WO ]) 
            print "    TEX_URL_WO  " + str(r[KEP.TEX_URL_WO ])
            print "---------------------------------------------"
   
"""
        
