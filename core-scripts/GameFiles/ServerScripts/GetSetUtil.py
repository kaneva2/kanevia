#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP
import PlayerIds

# C functions
import GetSet

TABLE_DEFAULT_NUM = -9999

#------------------------------------------------------------------------------------       
def getPlayerIntValue( playerObj, getSetStringID, defaultValue = -1 ):
    return int( getPlayerNumValue( playerObj, getSetStringID, defaultValue ) )

#------------------------------------------------------------------------------------       
def getPlayerNumValue( playerObj, getSetStringID, defaultValue = -1.0 ):
    ret = defaultValue
    if playerObj:
        ( getSetID, getSetType ) = GetSet.FindIdByName( playerObj, getSetStringID )
        if getSetID == KEP.amrNotFound:
            getSetID = GetSet.AddNewNumericMember( playerObj, getSetStringID, TABLE_DEFAULT_NUM, KEP.amfSaveAcrossSpawn )
        value = GetSet.GetNumber( playerObj, getSetID )
        if value == TABLE_DEFAULT_NUM:
            GetSet.SetNumber( playerObj, getSetID, ret )
        else:
            ret = value
    return ret

#------------------------------------------------------------------------------------
def setPlayerNumValue( playerObj, getSetStringID, value ):
    if playerObj:
        ( getSetID, getSetType ) = GetSet.FindIdByName( playerObj, getSetStringID )
        if getSetID == KEP.amrNotFound:
            getSetID = GetSet.AddNewNumericMember( playerObj, getSetStringID, TABLE_DEFAULT_NUM, KEP.amfSaveAcrossSpawn )
        GetSet.SetNumber( playerObj, getSetID, value )
