#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

"""
TeamDeathMatchArena module that contains TeamDeathMatchArena class
"""
#import helper Python code
import KEP
import ChatType

# C functions
import PlayerIds
import InstanceId
import Dispatcher
import Event
import GetSet
import ParentHandler

# python imports
import time

import ArenaBase
import TeamedArena

#-------------------------------------------------------------------
#-------------------------------------------------------------------
class TeamDeathMatchArena(TeamedArena.TeamedArena):
    """Team death match-type arena

    Derive from this class to create an arena that has teams
    fighting each other.  Killed players will respawn into the arena.
    The match will end when one team gets the
    max kills, or time runs out.
    """

    @staticmethod
    def SignupSettings():
        """return type, and if AI balanced"""
        return (ArenaBase.TEAMED_ARENA,False)

    #-----------------------------------------------------------------
    def __init__(self,name,parentList,game,arenaPtr,channelInstance,zi,maxBattleTime,pointsToWin,parent,dispatcher):
        """constructor"""
        super(TeamDeathMatchArena,self).__init__(name,parentList,game,arenaPtr,channelInstance,zi,maxBattleTime,pointsToWin,parent,dispatcher)

        import Arena

        # set some controls on how the base class works
        self._respawnInArenaOnDeath = True

    #-----------------------------------------------------------------
    def PlayerKilled( self, playerNetId, playerNetAssignedId, killerNetId, killerNetAssignedId ):
        """override when player is killed

        This adds to the players score"""

        killerKey = ArenaBase.MakePlayerKey(killerNetId, killerNetAssignedId)
        playerKey = ArenaBase.MakePlayerKey(playerNetId, playerNetAssignedId)
        
        sendScore = False
        
        if self.players.has_key(killerKey):
        
            if playerNetAssignedId != killerNetAssignedId and not self._sameTeam(playerKey,killerKey): # suicide and frags don't count
                self._teams[self.players[killerKey].teamId].points += 1
                self.players[killerKey].points += 1
                sendScore = True
            else:
                pass

        else:
            KEP.debugPrint( "TDM.PlayerKilled Couldn't find key", hex(int(killerKey)), killerKey, "in", self.players.keys() )
        

        super(TeamDeathMatchArena,self).PlayerKilled(playerNetId, playerNetAssignedId, killerNetId, killerNetAssignedId )
        if sendScore:
            self._sendScore()        
    
    #-----------------------------------------------------------------
    def _getLeaderName(self):
        """returns who is currently leading, in the case the arena times out"""

        # TODO: now that dict, need better way        
        leaderPoints = self._teams[self._teams.keys()[0]].points
        leaderName = self._teams[self._teams.keys()[0]].name
        
        for i in range(len(self._teams)-1):
            if self._teams[self._teams.keys()[i+1]].points > leaderPoints:
                leaderPoints = self._teams[self._teams.keys()[i+1]].points 
                leaderName = self._teams[self._teams.keys()[i+1]].name
            elif self._teams[self._teams.keys()[i+1]].points == leaderPoints:
                leaderName = "No one (tie)" 

        return leaderName
        
    #-----------------------------------------------------------------
    def _doWeHaveAWinner(self):
        """override to check if we have a winner when player killed/disconnected

        This checkes to see if a player had hit the limit to win
        """

        KEP.debugPrint( "checking for winner in deathmatch" )
        # is the kill limit hit?
        for team in self._teams.values():
            KEP.debugPrint( "team", team.name, "has", team.points, "needed is", self._pointsToWin )
            if team.points >= self._pointsToWin:
                self._endArena( team.name + " Team")
                return True
                
        return False


#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function"""
    return  True 


#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    pass

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function"""
    " called from C to give us a chance to cleanup before shutdown or re-init"
    return KEP.getDependentModules(__name__)
        


