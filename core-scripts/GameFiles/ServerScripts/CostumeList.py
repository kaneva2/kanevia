#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

### !!! READ ME: Slowly removing the costumes since it is now
###              done within the engine.  Commenting out groups that are
###              verified updated.
### Costume Configs ###
### 
class CostumeObject(object):

    def __init__(self, glidList, defaultList):

        self.__numPieces = len(glidList)
        self.__glids = glidList
        self.__mainItem = glidList[0]
        self.__subItems = glidList[1:]
        self.__defaultItems = defaultList

    def getNumItems(self):

        return len(self.__glids)

    def getGlids(self):

        return self.__glids

    def getMainItem(self):

        return self.__mainItem

    def getSubItems(self):

        return self.__subItems

    def getDefaultItems(self):

        return self.__defaultItems

    def isSubItem(self, glid):

        if self.__subItems.count(glid) > 0:
            return True

        return False

    def hasGlid(self, glid):

        if self.__glids.count(glid) > 0:
            return True

        return False

#-------------------------------------------------------------------

Costumes = []

def isCostumePiece( glid ):
    """
    Search for costume and return reference if found, return None otherwise

    """

    for costume in Costumes:
        if costume.hasGlid(glid):
            return costume

    return None



### ADD COSTUMES ###
###
### NOTE:   MAIN ITEM SHOULD ALWAYS BE FIRST IN LIST
###         After main item list all other items associated with costume
###
### In Client C++ a glid of -1 will cause an instant return


#default clothing items
Male_Defaults = [1172, 1173, 1174]

Female_Defaults = [1247, 1248, 1249]

### Costume GLIDS
#Male 
Vegas = [1478, 1479]
Pimped_Out = [2123, 2124, 2125]
Toga = [2218, 2219]
Vegas_Devil = [2224, 2225]
Pirate_Male = [2247, 2248, 2249]
Gorilla = [2250, 2251, 2252]
Warrior = [2364, 2365]
Santa = [2366, 2367, 2368]
Elf_Male = [2403, 2404, 2405]
Elf_Pimp_Male = [2406, 2407, 2408]
Black_Tux = [2680, 2678, 2679]
Black_Tux_Bow_Tie = [2677, 2678, 2679]
Black_Tux_Tie = [2681, 2678, 2679]
Powder_Blue_Tux = [2685, 2683, 2684]
Powder_Blue_Tux_Bow_Tie = [2682, 2683, 2684]
Powder_Blue_Tux_Tie = [2686, 2683, 2684]
Cream_Tux = [2690, 2688, 2689]
Cream_Tux_Bow_Tie = [2687, 2688, 2689]
Cream_Tux_Tie = [2691, 2688, 2689]
IM_Rave_Me = [2802, 2757]
IM_Equalizer = [2803, 2758]
IM_Hot = [2804, 2759]
IM_Sexy = [2805, 2760]
IM_Wink = [2806, 2761]
IM_Rock_Star = [2807, 2795]
IM_Awesome = [2890, 2889]
IM_Speedboat = [2894, 2893]
IM_Blue_Rave_Me = [2899, 2898]
IM_Flame = [2892, 2891]
Male_Xtreme_Sport_Jumpsuit_Red = [3240, 3241, 3242]
Male_Xtreme_Sport_Jumpsuit_Blue = [3251, 3252, 3253]
Kaneva_Man = [3382, 3383, 3384]
Cowboy = [3385, 3386, 3387]
Male_Leather_Avenger = [3388, 3389, 3390]
Male_Gangster_Suit = [3305, 3306, 3307]
Male_Legendary_Leather = [3391, 3392, 3393]
Male_Blue_Hula = [3727, 3728, 3729]
Male_Green_Hula = [3730, 3731, 3732]
Male_Red_Hula = [3733, 3734, 3735]
Male_Target_Practice = [ 3736, 3737 ]
Male_Just_Shoot_Me = [ 3738, 3739 ]
Male_Kaching_This = [ 3740, 3741 ]
Male_HeadShot = [ 3742, 3743 ]



#Female
Pirate_Female = [1639, 1640, 1641]
Feline = [1683, 1684, 1685]
#Senorita = [1884, 1885, 1886]
#Light_Gray_Satin_Trim_Dress = [1607, 1608]
#Black_Turq_Satin_Trim_Dress = [1609, 1610]
#Black_Red_Satin_Trim_Dress = [1611, 1612]
#Black_Pink_Satin_Trim_Dress = [1613,1614]
Red_Halter_Satin_Trim_Dress = [1603, 1604]
Black_Halter_Satin_Trim_Dress = [1605, 1606]
Red_Oring_Club_Dress = [1887, 1888]
Black_Oring_Club_Dress = [1889, 1890]
White_Bust_Garter_Gstring = [1891, 1892, 1893]
Red_Bust_Garter_Gstring = [1894, 1895, 1896]
Pink_Bust_Garter_Gstring = [1897, 1898, 1899]
Black_Bust_Garter_Gstring = [1900, 1901, 1902]
Grey_Plaid_Mini_Dress = [1903, 1904]
Red_Plaid_Mini_Dress = [2014, 2015]
Green_Plaid_Mini_Dress = [2016, 2017]
Dark_Grey_Plaid_Mini_Dress = [2018, 2019]
Blue_Plaid_Mini_Dress = [2020, 2021]
Purple_Plaid_Mini_Dress = [2022, 2023]
Black_Leather_Strapped_Club_Dress = [2037, 2038]
Purple_Pixie = [2120, 2121, 2122]
Lady_Law = [2220, 2221, 2222]
Fairy_Princess = [2226, 2227, 2228]
Wicked_Witch = [2230, 2231, 2232]
Belly_Dancer = [2236, 2237, 2238]
French_Maid = [2233, 2234, 2235]
Red_Skull_Minidress = [2291, 2292]
Blk_Wht_Lace_Minidress = [2293, 2294]
Drk_Grn_Wht_Stripe_MiniDress = [2295, 2296]
Blk_Red_Stripe_Minidress = [2297, 2298]
Maroon_Plaid_Minidress = [2299, 2300]
Blk_Blu_Stripe_Minidress = [2301, 2302]
Red_Diagonal_Dress = [2308, 2309]
Mrs_Claus = [2409, 2410]
Grn_Sexy_Elf = [2411, 2412, 2413]
Grn_Sexy_Snowflake_Elf = [2414, 2415, 2416]
Red_Sexy_Elf = [2417, 2418, 2419]
Red_Sexy_Snowflake_Elf = [2420, 2421, 2422]
Wedding_Gown = [2535, 2536]
Wedding_Sashed_Gown = [2537, 2538]
Green_Bridesmaids_Dress = [2539, 2540]
Black_Bridesmaids_Dress = [2541, 2542]
Sky_Blue_Bridesmaids_Dress = [2543, 2544]
White_Bridesmaids_Dress = [2545, 2546]
Dark_Red_Bridesmaids_Dress = [2547, 2548]
Brown_Bridesmaids_Dress = [2549, 2550]
Red_Bridesmaids_Dress = [2551, 2552]
Strapless_Wedding_Gown = [2553, 2554]
Black_Strapless_Long_Gown = [2555, 2556]
Gray_Strapless_Long_Gown = [2557, 2558]
Brown_Strapless_Long_Gown = [2559, 2560]
Blue_Strapless_Long_Gown = [2561, 2562]
Pink_Bunny_Costume = [2736, 2737, 2738]
Fem_IM_Rave_Me = [2796, 2771]
Fem_IM_Equalizer = [2797, 2772]
Fem_IM_Hot = [2798, 2773]
Fem_IM_Sexy = [2799, 2774]
Fem_IM_Wink = [2800, 2775]
Fem_IM_Rock_Star = [2801, 2794]
Fem_IM_Pink_Rave_Me = [2888, 2887]
Fem_IM_Flame = [2882, 2881]
Fem_IM_Awesome = [2880, 2879]
Fem_Xtreme_Sports_Jumpsuit_Red = [3218, 3219, 3220]
Fem_Xtreme_Sports_Jumpsuit_Blue = [3229, 3230, 3231]
Fem_Leather_Avenger = [3376, 3377, 3378] 
Cowgirl = [3379, 3380, 3381]
Fem_Gangster_Suit = [3422, 3423, 3424]
White_Angel = [3284, 3285, 3286]
Red_Angel = [3287, 3288, 3289]
Black_Angel = [3290, 3291, 3292]
Black_White_Angel = [3293, 3294, 3295]
Black_Red_Angel = [3296, 3297, 3298]
Red_White_Angel = [3299, 3300, 3301]
Brown_Bunny_Costume = [3302, 3303, 3304]
Fem_Legendary_Leather = [3409, 3410, 3411]
Warrior_Princess = [3415, 3416, 3589]
Fem_Tuxedo = [3418, 3419, 3420]
Fem_Colorful_Hula = [3076, 3077, 3078]
Fem_Tropical_Hula = [3721, 3722, 3723]
Fem_Cool_Hula = [3724, 3725, 3726]
Fem_Target_Practice = [ 3751, 3752 ]
Fem_Just_Shoot_Me = [ 3753, 3754 ]
Fem_Kaching_This = [ 3755, 3756 ]
Fem_Headshot = [ 3757, 3758 ]



### Add to costume list
#Male
Costumes.append(CostumeObject(Vegas, Male_Defaults))
Costumes.append(CostumeObject(Pimped_Out, Male_Defaults))
Costumes.append(CostumeObject(Toga, Male_Defaults))
Costumes.append(CostumeObject(Vegas_Devil, Male_Defaults))
Costumes.append(CostumeObject(Pirate_Male, Male_Defaults))
Costumes.append(CostumeObject(Gorilla, Male_Defaults))
Costumes.append(CostumeObject(Warrior, Male_Defaults))
Costumes.append(CostumeObject(Santa, Male_Defaults))
Costumes.append(CostumeObject(Elf_Male, Male_Defaults))
Costumes.append(CostumeObject(Elf_Pimp_Male, Male_Defaults))
Costumes.append(CostumeObject(Black_Tux, Male_Defaults))
Costumes.append(CostumeObject(Black_Tux_Bow_Tie, Male_Defaults))
Costumes.append(CostumeObject(Black_Tux_Tie, Male_Defaults))
Costumes.append(CostumeObject(Powder_Blue_Tux, Male_Defaults))
Costumes.append(CostumeObject(Powder_Blue_Tux_Bow_Tie, Male_Defaults))
Costumes.append(CostumeObject(Powder_Blue_Tux_Tie, Male_Defaults))
Costumes.append(CostumeObject(Cream_Tux, Male_Defaults))
Costumes.append(CostumeObject(Cream_Tux_Bow_Tie, Male_Defaults))
Costumes.append(CostumeObject(Cream_Tux_Tie, Male_Defaults))
Costumes.append(CostumeObject(IM_Rave_Me, Male_Defaults))
Costumes.append(CostumeObject(IM_Equalizer, Male_Defaults))
Costumes.append(CostumeObject(IM_Hot, Male_Defaults))
Costumes.append(CostumeObject(IM_Sexy, Male_Defaults))
Costumes.append(CostumeObject(IM_Wink, Male_Defaults))
Costumes.append(CostumeObject(IM_Rock_Star, Male_Defaults))
Costumes.append(CostumeObject(IM_Awesome, Male_Defaults))
Costumes.append(CostumeObject(IM_Speedboat, Male_Defaults))
Costumes.append(CostumeObject(IM_Blue_Rave_Me, Male_Defaults))
Costumes.append(CostumeObject(IM_Flame, Male_Defaults))
Costumes.append(CostumeObject(Male_Xtreme_Sport_Jumpsuit_Red, Male_Defaults))
Costumes.append(CostumeObject(Male_Xtreme_Sport_Jumpsuit_Blue, Male_Defaults))
Costumes.append(CostumeObject(Kaneva_Man, Male_Defaults))
Costumes.append(CostumeObject(Cowboy, Male_Defaults))
Costumes.append(CostumeObject(Male_Leather_Avenger, Male_Defaults))
Costumes.append(CostumeObject(Male_Gangster_Suit, Male_Defaults))
Costumes.append(CostumeObject(Male_Legendary_Leather, Male_Defaults))
Costumes.append(CostumeObject(Male_Blue_Hula, Male_Defaults))
Costumes.append(CostumeObject(Male_Green_Hula, Male_Defaults))
Costumes.append(CostumeObject(Male_Red_Hula, Male_Defaults))
Costumes.append(CostumeObject(Male_Target_Practice, Male_Defaults))
Costumes.append(CostumeObject(Male_Just_Shoot_Me, Male_Defaults))
Costumes.append(CostumeObject(Male_Kaching_This, Male_Defaults))
Costumes.append(CostumeObject(Male_HeadShot, Male_Defaults))


#Female
Costumes.append(CostumeObject(Pirate_Female, Female_Defaults))
Costumes.append(CostumeObject(Feline, Female_Defaults))
#Costumes.append(CostumeObject(Senorita, Female_Defaults))
#Costumes.append(CostumeObject(Light_Gray_Satin_Trim_Dress, Female_Defaults))
#Costumes.append(CostumeObject(Black_Turq_Satin_Trim_Dress, Female_Defaults))
#Costumes.append(CostumeObject(Black_Red_Satin_Trim_Dress, Female_Defaults))
#Costumes.append(CostumeObject(Black_Pink_Satin_Trim_Dress, Female_Defaults))
Costumes.append(CostumeObject(Red_Halter_Satin_Trim_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Halter_Satin_Trim_Dress, Female_Defaults))
Costumes.append(CostumeObject(Red_Oring_Club_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Oring_Club_Dress, Female_Defaults))
Costumes.append(CostumeObject(White_Bust_Garter_Gstring, Female_Defaults))
Costumes.append(CostumeObject(Red_Bust_Garter_Gstring, Female_Defaults))
Costumes.append(CostumeObject(Pink_Bust_Garter_Gstring, Female_Defaults))
Costumes.append(CostumeObject(Black_Bust_Garter_Gstring, Female_Defaults))
Costumes.append(CostumeObject(Grey_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Red_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Green_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Dark_Grey_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Blue_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Purple_Plaid_Mini_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Leather_Strapped_Club_Dress, Female_Defaults))
Costumes.append(CostumeObject(Purple_Pixie, Female_Defaults))
Costumes.append(CostumeObject(Lady_Law, Female_Defaults))
Costumes.append(CostumeObject(Fairy_Princess, Female_Defaults))
Costumes.append(CostumeObject(Wicked_Witch, Female_Defaults))
Costumes.append(CostumeObject(Belly_Dancer, Female_Defaults))
Costumes.append(CostumeObject(French_Maid, Female_Defaults))
Costumes.append(CostumeObject(Red_Skull_Minidress, Female_Defaults))
Costumes.append(CostumeObject(Blk_Wht_Lace_Minidress, Female_Defaults))
Costumes.append(CostumeObject(Drk_Grn_Wht_Stripe_MiniDress, Female_Defaults))
Costumes.append(CostumeObject(Blk_Red_Stripe_Minidress, Female_Defaults))
Costumes.append(CostumeObject(Maroon_Plaid_Minidress, Female_Defaults))
Costumes.append(CostumeObject(Blk_Blu_Stripe_Minidress, Female_Defaults))
Costumes.append(CostumeObject(Red_Diagonal_Dress, Female_Defaults))
Costumes.append(CostumeObject(Mrs_Claus, Female_Defaults))
Costumes.append(CostumeObject(Grn_Sexy_Elf, Female_Defaults))
Costumes.append(CostumeObject(Grn_Sexy_Snowflake_Elf, Female_Defaults))
Costumes.append(CostumeObject(Red_Sexy_Elf, Female_Defaults))
Costumes.append(CostumeObject(Red_Sexy_Snowflake_Elf, Female_Defaults))
Costumes.append(CostumeObject(Wedding_Gown, Female_Defaults))
Costumes.append(CostumeObject(Wedding_Sashed_Gown, Female_Defaults))
Costumes.append(CostumeObject(Green_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Black_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Sky_Blue_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(White_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Dark_Red_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Brown_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Red_Bridesmaids_Dress, Female_Defaults))
Costumes.append(CostumeObject(Strapless_Wedding_Gown, Female_Defaults))
Costumes.append(CostumeObject(Black_Strapless_Long_Gown, Female_Defaults))
Costumes.append(CostumeObject(Gray_Strapless_Long_Gown, Female_Defaults))
Costumes.append(CostumeObject(Brown_Strapless_Long_Gown, Female_Defaults))
Costumes.append(CostumeObject(Blue_Strapless_Long_Gown, Female_Defaults))
Costumes.append(CostumeObject(Pink_Bunny_Costume, Female_Defaults))
Costumes.append(CostumeObject(Fem_IM_Rave_Me, Female_Defaults))
Costumes.append(CostumeObject(Fem_IM_Equalizer, Female_Defaults))
Costumes.append(CostumeObject(Fem_IM_Hot, Female_Defaults))
Costumes.append(CostumeObject(Fem_IM_Sexy, Female_Defaults))
Costumes.append(CostumeObject(Fem_IM_Wink, Female_Defaults))
Costumes.append(CostumeObject(Fem_IM_Rock_Star, Female_Defaults))
Costumes.append(CostumeObject(Fem_IM_Pink_Rave_Me, Female_Defaults))
Costumes.append(CostumeObject(Fem_IM_Flame, Female_Defaults))
Costumes.append(CostumeObject(Fem_IM_Awesome, Female_Defaults))
Costumes.append(CostumeObject(Fem_Xtreme_Sports_Jumpsuit_Red, Female_Defaults))
Costumes.append(CostumeObject(Fem_Xtreme_Sports_Jumpsuit_Blue, Female_Defaults))
Costumes.append(CostumeObject(Fem_Leather_Avenger, Female_Defaults))
Costumes.append(CostumeObject(Cowgirl, Female_Defaults))
Costumes.append(CostumeObject(Fem_Gangster_Suit, Female_Defaults))
Costumes.append(CostumeObject(White_Angel, Female_Defaults))
Costumes.append(CostumeObject(Red_Angel, Female_Defaults))
Costumes.append(CostumeObject(Black_Angel, Female_Defaults))
Costumes.append(CostumeObject(Black_White_Angel, Female_Defaults))
Costumes.append(CostumeObject(Black_Red_Angel, Female_Defaults))
Costumes.append(CostumeObject(Red_White_Angel, Female_Defaults))
Costumes.append(CostumeObject(Brown_Bunny_Costume, Female_Defaults))
Costumes.append(CostumeObject(Fem_Legendary_Leather, Female_Defaults))
Costumes.append(CostumeObject(Warrior_Princess, Female_Defaults))
Costumes.append(CostumeObject(Fem_Tuxedo, Female_Defaults))
Costumes.append(CostumeObject(Fem_Colorful_Hula, Female_Defaults))
Costumes.append(CostumeObject(Fem_Tropical_Hula, Female_Defaults))
Costumes.append(CostumeObject(Fem_Cool_Hula, Female_Defaults))
Costumes.append(CostumeObject(Fem_Target_Practice, Female_Defaults))
Costumes.append(CostumeObject(Fem_Just_Shoot_Me, Female_Defaults))
Costumes.append(CostumeObject(Fem_Kaching_This, Female_Defaults))
Costumes.append(CostumeObject(Fem_Headshot, Female_Defaults))



