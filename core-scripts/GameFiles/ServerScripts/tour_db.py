#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

from datetime import datetime, date, time, timedelta

import ServerGlobals
import KEP
import time
import DatabaseInterface
import cachedDBInterface
import GetSet
import PlayerIds

gDispatcher = 0
gParent     = 0
gDBAccessor = None

#global database name so we can move the tables to another database
dbName      = "tour"

# type values

def logMsg( level, msg ):
    #needed for logging                                               
    import ParentHandler
    ParentHandler.LogMessage( gParent, level, "TOUR:DB: " + msg )
    #print msg
    
#players can only be signed up to one tour so just use the player_id as the lookup
getPlayerStatusSQL        = "SELECT count(player_id) AS cnt FROM @dbName.tour_signups WHERE player_id = @playerId"
signupPlayerSQL           = "INSERT INTO @dbName.tour_signups (running_tour_id, player_id) VALUES ( @runningTour, @playerId)"
removePlayerSQL           = "DELETE from @dbName.tour_signups WHERE player_id = @playerId"
getActiveTourSQL          = "SELECT running_tour_id, tr.tour_id, date_format(start_time, '%Y-%m-%d %H:%i') stime, current_zone_id, if(date_add(start_time, interval (zone_duration*(select count(running_tour_id) from @dbName.tour_zones where running_tour_id = tr.running_tour_id))/60 MINUTE)< now(),1,0) ended, (select count(running_tour_id) from @dbName.tour_zones where running_tour_id = tr.running_tour_id) totalzones  FROM @dbName.tour_running tr, @dbName.tour_active ta, @dbName.tour_types tt where tr.tour_id = ta.tour_id and ta.type_id = tt.type_id ORDER BY start_time desc LIMIT 1"
setActiveTourZoneSQL      = "UPDATE @dbName.tour_running SET current_zone_id = @CurrentZone where running_tour_id = @RunningTourID"
getNextTourZoneSQL        = "SELECT zone_index, zone_instance_id FROM @dbName.tour_zones t WHERE sort_order = @CurrentZone and running_tour_id = @RunningTour"
getActiveSignupsSQL       = "SELECT running_tour_id, player_id, zone_count FROM @dbName.tour_signups"
getZoneDurationForTourSQL = "SELECT zone_duration FROM @dbName.tour_types tt, @dbName.tour_active ta WHERE tt.type_id = ta.type_id AND ta.tour_id = @TourId"
getTotalTourSignupsSQL    = "SELECT COUNT(player_id) pcnt, if(isnull(max(population_max)),100,population_max) pmax FROM @dbName.tour_signups ts, @dbName.tour_running tr, @dbName.tour_active ta, @dbName.tour_types tt WHERE ts.running_tour_id = @RunningTour and ts.running_tour_id = tr.running_tour_id and tr.tour_id = ta.tour_id and ta.type_id = tt.type_id"
getTourDetailsSQL         = "SELECT ta.name, ta.description, date_format(tr.start_time, '%H:%i') stime FROM @dbName.tour_running tr, @dbName.tour_active ta WHERE tr.tour_id = ta.tour_id AND tr.running_tour_id = @RunningTour"
getNextFamePacketSQL      = "SELECT @dbName.tourGetNextFamePacketForPlayerWithId( @playerId )"
incrementFamePacketSQL    = "call @dbName.tourIncrementFamePacketForPlayerWithId( @playerId )"
addTourPlayerHistorySQL   = "call @dbName.add_tour_player_history( @playerId, @zoneNumber, @tourId, @packetId )"

def getTourDetails( trows, RunningTourId ):
    sql = getTourDetailsSQL.replace("@dbName", dbName)
    sql = sql.replace("@RunningTour", str(RunningTourId))
    temprows=[]
    gDBAccessor.fetchRows(sql, temprows, 1800) #this info is static, lets cache!
    if len(temprows) > 0:
        
        msg = "getTourDetails(): name= " + temprows[0][0] + " desc=" + temprows[0][1] + " starttime" + temprows[0][2]
        logMsg( KEP.DEBUG_LOG_LEVEL, msg  )
        row = [temprows[0][0], temprows[0][1], temprows[0][2]] #name, description, start_time
        trows.append(row)
        return 1;
    else:
        row = ["", "", ""] #name, description, start_time
        trows.append(row)
        return 0;
        
def isTourFull( tourId ):
    sql = getTotalTourSignupsSQL.replace("@dbName", dbName)
    sql = sql.replace("@RunningTour", str(tourId))
    temprows=[]
    gDBAccessor.fetchRows(sql, temprows, -1) #dont cache this man!
    if len(temprows) > 0:
        spacesleft = int(temprows[0][1]) - int(temprows[0][0])
        msg = "isTourFull(): Active signups = " + str(temprows[0][0]) + " max population=" + str(temprows[0][1]) + " spaces left=" +str(spacesleft)
        logMsg( KEP.DEBUG_LOG_LEVEL, msg  )
        if spacesleft <= 0:
            return 1
        else:
            return 0   
    else:
        return 0

def getZoneDurationForTour( tourId ):
    global cache_zoneduration
    global gDBAccessor
    duration = 0
         
    sql = getZoneDurationForTourSQL.replace("@dbName", dbName)
    sql = sql.replace("@TourId", str(tourId))
    temprows=[]
    gDBAccessor.fetchRows(sql, temprows, 3600) #todo increase cache
    if len(temprows) > 0:
        duration = int(temprows[0][0])  
    else:
        duration = 111
         
    return duration
    
def getNextTourZone( trows ):
    "retrieves the next tour zone, instance and time"
    global gDBAccessor
    
    temprows = []
    getActiveTour(temprows)
    if len(temprows) > 0:
        runningtour = int(temprows[0][0])
        tourid      = int(temprows[0][1])
        starttime   = str(temprows[0][2])
        nxtzone     = int(temprows[0][3])
        ended       = int(temprows[0][4])
        totalzones  = int(temprows[0][5])
        secondstillnextevent = 0
        
        #calculate the number of seconds until this start time
        #have to do all this because we are still on python 2.4
        #replace with datetime.strptime when we get to 2.5 or above
        logMsg( KEP.DEBUG_LOG_LEVEL, "getNextTourZone(): Next tour start time is:" + starttime )
        splitstart = starttime.split(" ")
        dateparts = splitstart[0].split("-")
        timeparts = splitstart[1].split(":")
        schd = datetime(int(dateparts[0]),int(dateparts[1]),int(dateparts[2]),int(timeparts[0]),int(timeparts[1]))
        now  = datetime.today()
        diff = schd - now
        #logMsg( KEP.DEBUG_LOG_LEVEL, "getNextTourZone(): Diff days=" + str(diff.days) )
        if( diff.days >= 0 ): #hasnt started yet
            #logMsg( KEP.DEBUG_LOG_LEVEL, "getNextTourZone(): New Tour Starting sending nxtzone=1" )
            secondstillnextevent = diff.seconds
            nxtzone = 1
        else: #tour in progress       
            defaultduration = getZoneDurationForTour(tourid) #default time per zone
            diff = now - schd #time that has elapsed
            
            currzone = int(int(diff.seconds) / int(defaultduration)) + 1 #current running zone
            #if( currzone == 0 ): #fractional correction for the first zone
            #    currzone = 1         
            
            secondstillnextevent = defaultduration - (diff.seconds - (currzone * defaultduration))
            if( secondstillnextevent > defaultduration):
                secondstillnextevent = secondstillnextevent - defaultduration 
            
            nxtzone = currzone + 1
            #log out some helpful things
            #msg = "getNextTourZone(): duration=" + str(defaultduration) + " diff=" + str(diff.seconds) + " currzone=" + str(currzone) + " sectillnxt=" + str(secondstillnextevent) + " nxtzone=" + str(nxtzone)     
            #logMsg( KEP.DEBUG_LOG_LEVEL, msg  )
            #logMsg( KEP.DEBUG_LOG_LEVEL, "getNextTourZone(): Elapsed time in current tour=" + str(diff.seconds)  )
            #logMsg( KEP.DEBUG_LOG_LEVEL, "getNextTourZone(): Seconds left in this zone=" + str(secondstillnextevent)  )
        
        #logMsg( KEP.DEBUG_LOG_LEVEL, "getNextTourZone(): Calculated next zone is=" + str(nxtzone)  )  
        temprows2=[]
        sql = getNextTourZoneSQL.replace("@dbName", dbName)
        sql = sql.replace( "@CurrentZone", str(nxtzone) ) 
        sql = sql.replace( "@RunningTour", str(runningtour) )
    
        cachetime = secondstillnextevent - 30  #about 30 seconds before zone is to start so we dont run over
        if cachetime < 0:
            cachetime = -1
            
        gDBAccessor.fetchRows( sql, temprows2, cachetime ) 
        if (len(temprows2) > 0) and (nxtzone <= totalzones):
            #logMsg( KEP.DEBUG_LOG_LEVEL, "getNextTourZone(): retrieved next tour " + str(temprows2[0][0]) + ":" + str(temprows2[0][1]) + " starts in " + str(secondstillnextevent) )
            row = [temprows2[0][0], temprows2[0][1], secondstillnextevent, nxtzone, totalzones] #zoneid, instanance, starttime, nextzone, totalzones
            trows.append(row)
        else:
            #logMsg( KEP.DEBUG_LOG_LEVEL, "getNextTourZone(): no zone rows returned from getNextTourZoneSQL"  )
            row = [-1,-1,"ERROR",0,0]
            trows.append(row)
            #reset cache
            cache_zoneduration = 0
            
def getNextFamePacketForPlayer( player ):
    "determines the next usable fame packet ID for this user, prevents hacking"
    global gDBAccessor
    
    playerId = int(GetSet.GetNumber( player, PlayerIds.HANDLE))
    
    sql = getNextFamePacketSQL
    sql = sql.replace("@playerId", str(playerId))
    sql = sql.replace("@dbName", dbName)

    logMsg( KEP.DEBUG_LOG_LEVEL, "getNextFamePacketForPlayer [" + sql + "]"  )
    
    temprows2=[]
    gDBAccessor.fetchRows( sql, temprows2,  -1 )  
    return temprows2[0][0]
    
def incrementFamePacketForPlayerWithId(player):
    "increments the last fame packet awarded for this user for today."
    global gDBAccessor
    
    playerId = int(GetSet.GetNumber( player, PlayerIds.HANDLE))
    
    sql = incrementFamePacketSQL
    sql = sql.replace("@playerId", str(playerId))
    sql = sql.replace("@dbName", dbName)
    
    #logMsg( KEP.DEBUG_LOG_LEVEL, "incremetnFamePacketForPlayerWithId(): incrementFamePacket [" + sql + "]" )
    
    temprows2=[]
    gDBAccessor.fetchRows( sql, temprows2, -1 )  

def addTourPlayerHistory( playerId, zoneNumber, tourId, packetId ):
    "adds a record to the tour_player_history table"
    global gDBAccessor
    
    #   = "call @dbName.add_tour_player_history( @playerId, @zoneNumber, @tourId, @packetId )"
    sql = addTourPlayerHistorySQL
    sql = sql.replace( "@dbName", dbName ) 
    sql = sql.replace( "@playerId", str(playerId) )
    sql = sql.replace( "@zoneNumber", str(zoneNumber) )
    sql = sql.replace( "@tourId", str(tourId) )
    sql = sql.replace( "@packetId", str(packetId) )
    
    temprows2=[]
    gDBAccessor.fetchRows( sql, temprows2, -1 )  

def removePlayerFromTourByPlayerId(playerId, trows):
    "removes a player from the tour by player_id"
    global gDBAccessor
    
    #logMsg( KEP.DEBUG_LOG_LEVEL, "removePlayerFromTourByPlayerId(): Tour Signup Manager Worker thread - removePlayerFromTourByPlayerId")
    sql = removePlayerSQL.replace("@dbName", dbName)
    sql = sql.replace( "@playerId", str(playerId) )
    
    gDBAccessor.fetchRows( sql, trows, -1 )

def removePlayerFromTour( player, trows):
    "removes a player record from the tour"
                         
    playerId = int(GetSet.GetNumber( player, PlayerIds.HANDLE))
    removePlayerFromTourByPlayerId( playerId, trows )
    
def signupToTour( player, tour, trows) :
    "sign a player up to a tour"
    global gDBAccessor
                                
    playerId = int(GetSet.GetNumber( player, PlayerIds.HANDLE))
    
    #logMsg( KEP.DEBUG_LOG_LEVEL, "signupToTour(): Tour Signup Manager Worker thread - signupToTour")
    sql = signupPlayerSQL.replace("@dbName", dbName)
    sql = sql.replace( "@playerId", str(playerId) )
    sql = sql.replace( "@runningTour", str(tour) )
    
    gDBAccessor.fetchRows( sql, trows, -1 )     

def getActiveTour( trows ):
    "look up the active running tour"
    global gDBAccessor
    
    #logMsg( KEP.DEBUG_LOG_LEVEL, "getActiveTour(): Tour Signup Manager Worker thread - getActiveTour")
    sql = getActiveTourSQL.replace("@dbName", dbName)
    
    gDBAccessor.fetchRows( sql, trows, 10 )  #todo change this timeout after in production 
    #logMsg( KEP.DEBUG_LOG_LEVEL, "getActiveTour(): getActiveTour has recieved " + str(len(trows)) + " rows")
    
def getPlayerStatus( player, trows ):
    "check to see if a player is already in the signup table"
    global gDBAccessor
    
    playerId = int(GetSet.GetNumber( player, PlayerIds.HANDLE))
    
    #logMsg( KEP.DEBUG_LOG_LEVEL, "getPlayerStatus(): Tour Signup Manager Worker thread - getPlayerStatus")
    sql = getPlayerStatusSQL.replace( "@playerId", str(playerId) )
    sql = sql.replace("@dbName", dbName)    
    gDBAccessor.fetchRows( sql, trows, -1 ) 

def updateTourVisits(player, tour, currentzone):
    global gDBAccessor
    
    playerId = int(GetSet.GetNumber( player, PlayerIds.HANDLE))
    #rows=[]
    #getActiveTour( rows )
    #tourDate = rows[0][2]
    
    sql = "SELECT player_id FROM @dbName.tour_visitedzones WHERE player_id = @playerid and running_tour_id = @tourid and zone_number=@curzone"
    sql = sql.replace("@dbName", dbName)
    sql = sql.replace("@playerid", str(playerId))
    sql = sql.replace("@tourid", str(tour))
    sql = sql.replace("@curzone", str(currentzone)) 
    
    rows=[]
    gDBAccessor.fetchRows(sql, rows, 30)
    if( len(rows) == 0 ):
        #insert a new record                                                             
        sql = "INSERT INTO @dbName.tour_visitedzones (player_id, running_tour_id, zone_number) values (@playerid, @tourid, @curzone)"
        sql = sql.replace("@dbName", dbName)
        sql = sql.replace("@playerid", str(playerId))
        sql = sql.replace("@tourid", str(tour))
        sql = sql.replace("@curzone", str(currentzone))   
        rows=[]
        gDBAccessor.fetchRows(sql, rows, -1)
#-------------------------------------------------------------------
# INIT EVENTS AND HANDLERS
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    """Called from C to initialize all the handlers in this script"""

    global gDBAccessor
    
    #gDBAccessor = DatabaseInterface.DBAccess(dbName)
    gDBAccessor = cachedDBInterface.cachedDBAccess(dbName)

    
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    global gParent
    global gDispatcher

    # grab instances for debugging
    gParent = parent
    gDispatcher = dispatcher
     
    KEP.debugLevel = dbgLevel

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Manage resources for blade reload"""

    return KEP.getDependentModules(__name__)

#-------------------------------------------------------------------
def resetDBAccessor():
    """destroy and recreate the db access object"""
    global gDBAccessor

    del gDBAccessor
    #gDBAccessor = DatabaseInterface.DBAccess(dbName)
    gDBAccessor = cachedDBInterface.cachedDBAccess(dbName)    
