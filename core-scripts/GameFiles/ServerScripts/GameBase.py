#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP

# C functions
import PlayerIds
import InstanceId
import ParentHandler
import Dispatcher
import Event
import GetSet

# python imports
import time
import random
import threading

class Player(object):
    
    def __init__(self, netId):
        self.netId = netId

#-------------------------------------------------------------------
#-------------------------------------------------------------------
class GameBase(object):
    
    GAME_EVENT = "GameEvent"
    
    #FILTERS
    GAME_EVENT_START = 1
    GAME_EVENT_SPAWN_INFO = 2
    
    instances = dict()
    
    GAMESTATE_NOT_STARTED = 1
    GAMESTATE_RUNNING = 2
    GAMESTATE_ENDED = 3
    
    def __init__(self, parentList, instanceID, zoneIndex):
        " constructor "
        #add reference
        GameBase.instances[id(self)] = self
        
        self.parentList = parentList
        self.instanceId = instanceID
        self.zoneIndex = InstanceId.SetInstanceFrom(int(zoneIndex), int(instanceID))
        
        self.editMode = False
        self.players = dict()
        self.exitedPlayers = dict()
        self.gameState = GameBase.GAMESTATE_NOT_STARTED
        self.locked = False
        
        self.startingSpawnPoint = [0, 0, 0]
        self.startingRotation = 0
        
    #------------------------------------------------------------------------------------       
    def __del__(self):
        """destructor """
        Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "DELETING GAME INSTANCE " + str(self.instanceId))
        
    def StartGame(self):
        Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "Start game at " + str(self.startingSpawnPoint[0]) + ", " + str(self.startingSpawnPoint[1]) + ", " + str(self.startingSpawnPoint[2])+" : "+str(self.startingRotation))
        for playerNetId in self.players:
            self.SpawnPlayer(playerNetId, self.startingSpawnPoint[0], self.startingSpawnPoint[1], self.startingSpawnPoint[2], self.startingRotation)
        self.gameState = GameBase.GAMESTATE_RUNNING
        
        startEvent = Dispatcher.MakeEvent(KEP.dispatcher, GameBase.GAME_EVENT)
        Event.SetFilter(startEvent, GameBase.GAME_EVENT_START)
        #ENCODE LOBBY INFO
        self._broadcastEvent(startEvent)
    
    def EndGame(self):
        self.gameState = GameBase.GAMESTATE_ENDED
        self.parentList.remove(self)
        #del GameBase.instances[id(self)]
        
    def LockGame(self, locked):
        self.locked = locked
        return self.locked
    
    def AddPlayer(self, netId):
        if self.locked == True:
            return False
        
        newPlayer = Player(netId)
        self.players[netId] = newPlayer
        
        if self.gameState == GameBase.GAMESTATE_RUNNING:
            self.SpawnPlayer(netId, self.startingSpawnPoint[0], self.startingSpawnPoint[1], self.startingSpawnPoint[2], self.startingRotation)
        return True
    
    def RemovePlayer(self, netId):
        if netId in self.players:
            self.exitedPlayers[netId] = self.players[netId]
            del self.players[netId]
            if len(self.players) <= 0:
                self.EndGame()
            return True
        return False
    
    def SpawnPlayer(self, netId, x, y, z, rot=0):
        import Game
        if netId in self.players:
            game = Dispatcher.GetGame(KEP.dispatcher)
            playerObj = Game.GetPlayerByNetId(game, netId)
            if playerObj != 0 and playerObj != None:
                Game.SpawnToArenaPointWithRot(game, playerObj, int(x), int(y), int(z), int(rot), self.zoneIndex)
    
    def SpawnPlayerAtStartingSpawnPoint(self, netId):
        self.SpawnPlayer(netId, self.startingSpawnPoint[0], self.startingSpawnPoint[1], self.startingSpawnPoint[2], self.startingRotation)
           
    def PlayerInGame(self, netId):
        return netId in self.players      
    #-----------------------------------------------------------------
    def PlayerDisconnected(self, playerNetId):
        self.RemovePlayer(playerNetId)
        return True
    #------------------------------------------------------------------------------------       
    def PlayerSpawned(self, playerNetId, filter):
        import Game
        if filter != self.zoneIndex: #player has zoned out of valid games zone
            self.RemovePlayer(playerNetId)
            return False

        spawnEvent = Dispatcher.MakeEvent(KEP.dispatcher, GameBase.GAME_EVENT)
        Event.SetFilter(spawnEvent, GameBase.GAME_EVENT_SPAWN_INFO)
        self._sendEvent(playerNetId, spawnEvent)

        return True
    
    def PlayerJoin(self, playerNetId):
        if not(playerNetId in self.players) and self.locked == False:
            return self.AddPlayer(playerNetId)
        return False
    
    def encodePlayerInfo(self, event):
        for playerNetId in self.players:
            Event.EncodeNumber(event, playerNetId)
        return event
        
    def SetStartingSpawnPoint(self, x, y, z):
        self.startingSpawnPoint = [x, y, z]                  

    def SetStartingRotation(self, rot):
        self.startingRotation = rot
        
    #------------------------------------------------------------------------------------       
    def _broadcastEvent(self, event):
        for playerNetId in self.players:
            self._sendEvent(playerNetId, event)
        return True
  
    #------------------------------------------------------------------------------------       
    def _sendEvent(self, playerNetId, event):
        Event.AddTo(event, playerNetId)
        Dispatcher.QueueEvent(KEP.dispatcher, event)
        return True
    
    def _logPlayers(self):
        for player in self.players:
            Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, self.players[player].name + ", " + str(player))
    
    #-------------------------------------------------------------------
    @staticmethod
    def handleDisconnectedEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        playerNetId = Event.DecodeNumber(event)
        for gameInstance in GameBase.instances:
            if GameBase.instances[gameInstance].PlayerInGame(playerNetId):
                GameBase.instances[gameInstance].PlayerDisconnected(playerNetId)
        GameBase.removeFinishedGames()
        return KEP.EPR_OK
    
    #-------------------------------------------------------------------
    @staticmethod
    def handlePlayerSpawnedEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        for gameInstance in GameBase.instances:
            ret = GameBase.instances[gameInstance].PlayerInGame(fromNetId)
            if GameBase.instances[gameInstance].PlayerInGame(fromNetId):
                GameBase.instances[gameInstance].PlayerSpawned(fromNetId, filter)
        GameBase.removeFinishedGames()
        return KEP.EPR_OK

    #-------------------------------------------------------------------
    @staticmethod
    def removeFinishedGames():
        deleteList = list()
        for gameInstance in GameBase.instances:
            if GameBase.instances[gameInstance].gameState == GameBase.GAMESTATE_ENDED:
                deleteList.append(gameInstance)
        for gInstance in deleteList:
            del GameBase.instances[gInstance]
            
#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers(dispatcher, handler, dbgLevel):
    """Generic event handler interface function"""
    KEP.gLoggingDispatcher = dispatcher
   
    ParentHandler.RegisterEventHandler(dispatcher, handler, GameBase.handleDisconnectedEvent, 1, 0, 0, 0, "DisconnectedEvent", KEP.HIGH_PRIO) 
    ParentHandler.RegisterEventHandler(dispatcher, handler, GameBase.handlePlayerSpawnedEvent, 1, 0, 0, 0, "PlayerSpawnedEvent", KEP.HIGH_PRIO) 
    
    # filtered events
    ARENA_CHANNEL_MASK = 0x50000000;
    #ParentHandler.RegisterFilteredEventHandler( dispatcher, handler,  ArenaSignupManager.handleWeaponFiredEvent,   1,0,0,0, "WeaponFiredEvent", ARENA_CHANNEL_MASK, KEP.FILTER_IS_MASK, KEP.NO_OBJID_FILTER, KEP.MED_PRIO ) 
    

#-------------------------------------------------------------------
def InitializeKEPEvents(dispatcher, handler, dbgLevel):
    """Generic event handler interface function"""
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, GameBase.GAME_EVENT, KEP.MED_PRIO)

#-------------------------------------------------------------------
def ReinitalizeKEPEvents(dispatcher, parent, dbgLevel):
    """Generic event handler interface function
    
    called from C to give us a chance to cleanup before shutdown or re-init
    """
    return KEP.getDependentModules(__name__)


