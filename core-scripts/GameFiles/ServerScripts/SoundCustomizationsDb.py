#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import ServerGlobals
import KEP
import time
import DatabaseInterface

gDispatcher = 0
gParent = 0
gDBAccessor = None

def logMsg( level, msg ):
    #needed for logging
    import ParentHandler
    ParentHandler.LogMessage( gParent, level, msg )
    #print msg

OBJ_PLACEMENT_ID    = 0
PITCH               = 1
GAIN                = 2
MAX_DISTANCE        = 3
ROLL_OFF            = 4
LOOP                = 5
LOOP_DELAY          = 6
DIR_X               = 7
DIR_Y               = 8
DIR_Z               = 9
CONE_OUTER_GAIN     = 10
CONE_INNER_ANGLE    = 11
CONE_OUTER_ANGLE    = 12

getSoundsSQL = "SELECT `obj_placement_id` as o, `pitch` as p, `gain` as g, `max_distance` as m, `roll_off` as r, " + \
		    " `loop` as l, `loop_delay` as d, " + \
		    " `dir_x` as x, `dir_y` as y , `dir_z` as z, " + \
		    " `cone_outer_gain` as og, " + \
		    " `cone_inner_angle` as ia, " + \
		    " `cone_outer_angle` as oa " + \
		    " FROM `sound_customizations` " + \
		    " WHERE `zone_index` = @zoneIndex " + \
		    " AND `instance_id` = @instanceId "

updateSoundsSQL = "INSERT INTO `sound_customizations` " + \
                  " (`obj_placement_id`, `zone_index`, `instance_id`, `pitch`, `gain`, `max_distance`, `roll_off`, " + \
                  " `loop`, `loop_delay`, `dir_x`, `dir_y`, `dir_z`, `cone_outer_gain`, `cone_inner_angle`, `cone_outer_angle` ) " + \
                  " VALUES ( @obj_placement_id , @zone_index , @instance_id , @pitch , @gain , @max_distance , @roll_off , " + \
                  " @loop , @delay , @dir_x , @dir_y , @dir_z , @cone_outer_gain , @cone_inner_angle , @cone_outer_angle ) " + \
                  " ON DUPLICATE KEY UPDATE " + \
                  " `obj_placement_id` = @obj_placement_id , `zone_index` = @zone_index , `instance_id` = @instance_id , `pitch` = @pitch , " + \
                  " `gain` = @gain , `max_distance` = @max_distance , `roll_off` = @roll_off , `loop` = @loop , `loop_delay` = @delay , " + \
                  " `dir_x` = @dir_x , `dir_y` = @dir_y , `dir_z` = @dir_z , `cone_outer_gain` = @cone_outer_gain , " + \
                  " `cone_inner_angle` = @cone_inner_angle , `cone_outer_angle` = @cone_outer_angle "
                  

deleteSoundSQL =  "DELETE FROM `sound_customizations` " + \
                  "WHERE `obj_placement_id` = @obj_placement_id"

#-------------------------------------------------------------------
def getSounds( zoneIndex, instanceId, rowList ):
    """Get sounds for this zone instance"""
    global gDBAccessor
    global getSoundsSQL

    logMsg( KEP.TRACE_LOG_LEVEL, "getSounds")

    sql = getSoundsSQL.replace( "@zoneIndex", str(zoneIndex) )
    sql = sql.replace( "@instanceId", str(instanceId) )

    gDBAccessor.fetchRows( sql, rowList )    

#-------------------------------------------------------------------
def updateSounds(zoneIndex, instanceId, objectid, glid, pitch, gain, max_distance, roll_off, loop, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle):
    """Attempt to insert a new sound, update a sound if it currently exists"""
    global gDBAccessor
    global updateSoundsSQL
    
    logMsg( KEP.TRACE_LOG_LEVEL, "updateSounds" )

    sql = updateSoundsSQL.replace( "@obj_placement_id", str(objectid) )
    sql = sql.replace( "@zone_index", str(zoneIndex) )
    sql = sql.replace( "@instance_id", str(instanceId) )
    sql = sql.replace( "@pitch", str(pitch) )
    sql = sql.replace( "@gain", str(gain) )
    sql = sql.replace( "@max_distance", str(max_distance) )
    sql = sql.replace( "@roll_off", str(roll_off) )
    sql = sql.replace( "@loop", str(loop) )
    sql = sql.replace( "@delay", str(loop_delay) )
    sql = sql.replace( "@dir_x", str(dir_x) )
    sql = sql.replace( "@dir_y", str(dir_y) )
    sql = sql.replace( "@dir_z", str(dir_z) )
    sql = sql.replace( "@cone_outer_gain", str(cone_outer_gain) )
    sql = sql.replace( "@cone_inner_angle", str(cone_inner_angle) )
    sql = sql.replace( "@cone_outer_angle", str(cone_outer_angle) )

    gDBAccessor.fetchRows( sql, None )    

#-------------------------------------------------------------------
def deleteSound( objectid ):
    """Delete the sound customization from db"""
    global gDBAccessor
    global deleteSoundSQL
    
    logMsg( KEP.TRACE_LOG_LEVEL, "deleteSound" )

    sql = deleteSoundSQL.replace( "@obj_placement_id", str(objectid) )

    gDBAccessor.fetchRows( sql, None )
    
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    global gParent
    global gDispatcher

    # grab instances for debugging
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Manage resources for blade reload"""

    return KEP.getDependentModules(__name__)

#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the event handlers"""
    global gDBAccessor

    gDBAccessor = DatabaseInterface.DBAccess("SoundCustomizationsDb")

#-------------------------------------------------------------------
# Self test
"""
if __name__ == "__main__":

    if False:
        dynobjlist = list()
        wolist = list()

        getDynamicObjects(805306376, 12271, dynobjlist)
        getWorldObjects(805306376, 12271, dynobjlist)
        
        print "/////////////////////////////////////////////"
        print "Got DynObj total of "+str(len(dynobjlist))
        for r in dynobjlist:
            print "---------------------------------------------"
            print "    OBJ_PLACEMENT_ID_DO " + str(r[KEP.OBJ_PLACEMENT_ID_DO])
            print "    OBJ_ID_DO  " + str(r[KEP.OBJ_ID_DO ]) 
            print "    TEX_ASSET_ID_DO  " + str(r[KEP.TEX_ASSET_ID_DO ])
            print "    PLAYER_ID_DO  " + str(r[KEP.PLAYER_ID_DO ])
            print "    FRIEND_ID_DO  " + str(r[KEP.FRIEND_ID_DO ])
            print "    POS_X_DO  " + str(r[KEP.POS_X_DO] )
            print "    POS_Y_DO  " + str(r[KEP.POS_Y_DO ])
            print "    POS_Z_DO  " + str(r[KEP.POS_Z_DO ])
            print "    ROT_X_DO  " + str(r[KEP.ROT_X_DO ])
            print "    ROT_Y_DO  " + str(r[KEP.ROT_Y_DO ])
            print "    ROT_Z_DO  " + str(r[KEP.ROT_Z_DO ])
            print "    SWF_NAME_DO  " + str(r[KEP.SWF_NAME_DO ])
            print "    SWF_PARAM_DO  " + str(r[KEP.SWF_PARAM_DO ])
            print "---------------------------------------------"
        print "/////////////////////////////////////////////"
        print "Got WorldObj total of "+str(len(wolist))
        for r in wolist:
            print "---------------------------------------------"
            print "    WORLD_OBJ_ID_WO " + str(r[KEP.WORLD_OBJ_ID_WO])
            print "    ASSET_ID_WO  " + str(r[KEP.ASSET_ID_WO ]) 
            print "    TEX_URL_WO  " + str(r[KEP.TEX_URL_WO ])
            print "---------------------------------------------"
"""        

        
