#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import Dispatcher
import KEP
import DatabaseInterface
import FameIds
import WorldFameAwarder

gDBAccessor = None



#NEW_GAME = "insert into game set arena_id = 11, duration = 222, game_type_id = 11, datetime = now()"
ADD_GAME = "call create_game(@arenaid, @duration, @gametypeid);" #, @duration)"
ADD_PLAYER = "insert into player_game set player_id = @playerid, game_id = @gameid, team = @teamid, quit_early = @quitearly, win_lose = @winlose, num_kills = @kills, num_deaths = @deaths"
ADD_PLAYER_COINS = "insert into player_game_coin_stats set player_id = @playerid, game_id = @gameid, coin_pickups = NULL, coin_drops = NULL, coin_scores = @coinsScored"
ADD_PLAYER_KILLS = "insert into player_game_kill_stats set game_id = @gameid, killer_id = @killerid, killed_id = @killedid, total_kills = @totalkills"

KACHING_DB = "kaching"

#-------------------------------------------------------------------
def updateDatabaseForArena( arenaGameObj ):
    """kick off all the clean up and storage for a completed KaChing arena"""

    giveFamePoints(arenaGameObj)
    gameId = addGameToDB(arenaGameObj)
    addPlayersToDB(arenaGameObj, gameId)


#-------------------------------------------------------------------
def giveFamePoints(arenaGameObj):

    if arenaGameObj._getTimeDuration() > 60:
        for playerKey in arenaGameObj.players:
            player = arenaGameObj.players[playerKey]
            Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, player.name + " " + str(player.joinedMidMatch) + " " + str(player.quitEarly))
            if player.joinedMidMatch == False and player.quitEarly == False:
                Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, player.name + " sending fame packets: " + str(FameIds.KACHING_FIRST_GAME) + " " + str(FameIds.KACHING_EVERY_GAME_PLAYED) + " " + str(FameIds.KACHING_DAILY_PLAYS) )
                WorldFameAwarder.redeemWorldFamePacketFromName(player.name, FameIds.KACHING_FAME, FameIds.KACHING_FIRST_GAME)
                WorldFameAwarder.redeemWorldFamePacketFromName(player.name, FameIds.KACHING_FAME, FameIds.KACHING_EVERY_GAME_PLAYED)
                WorldFameAwarder.redeemWorldFamePacketFromName(player.name, FameIds.KACHING_FAME, FameIds.KACHING_DAILY_PLAYS)
                if arenaGameObj._getLeader() == player.teamId:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, player.name + " sending fame packets: " + str(FameIds.KACHING_FIRST_WIN) + " " + str(FameIds.KACHING_EVERY_WIN) + " " + str(FameIds.KACHING_DAILY_WINS) )
                    WorldFameAwarder.redeemWorldFamePacketFromName(player.name, FameIds.KACHING_FAME, FameIds.KACHING_FIRST_WIN)
                    WorldFameAwarder.redeemWorldFamePacketFromName(player.name, FameIds.KACHING_FAME, FameIds.KACHING_EVERY_WIN)
                    WorldFameAwarder.redeemWorldFamePacketFromName(player.name, FameIds.KACHING_FAME, FameIds.KACHING_DAILY_WINS)
                if player.kills >= 1:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, player.name + " sending fame packets: " + str(FameIds.KACHING_FIRST_CHECK) )
                    WorldFameAwarder.redeemWorldFamePacketFromName(player.name, FameIds.KACHING_FAME, FameIds.KACHING_FIRST_CHECK)
                if player.kills >= 5:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, player.name + " sending fame packets: " + str(FameIds.KACHING_CHECKS_ONE_GAME) )
                    WorldFameAwarder.redeemWorldFamePacketFromName(player.name, FameIds.KACHING_FAME, FameIds.KACHING_CHECKS_ONE_GAME)
                if player.points >= 1:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, player.name + " sending fame packets: " + str(FameIds.KACHING_FIRST_SCORE) )
                    WorldFameAwarder.redeemWorldFamePacketFromName(player.name, FameIds.KACHING_FAME, FameIds.KACHING_FIRST_SCORE)
                if player.points >= 10:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, player.name + " sending fame packets: " + str(FameIds.KACHING_COINS_ONE_GAME) )
                    WorldFameAwarder.redeemWorldFamePacketFromName(player.name, FameIds.KACHING_FAME, FameIds.KACHING_COINS_ONE_GAME)


#-------------------------------------------------------------------        
def addGameToDB(arenaGameObj):
    global ADD_GAME
    global gDBAccessor
    global KACHING_LEVELS

    level_name = arenaGameObj._mapName

    sql = ADD_GAME

    sql = sql.replace("@arenaid", str( int(arenaGameObj._originalZone)) )
    
    sql = sql.replace("@duration", str(arenaGameObj._getTimeDuration()))
    sql = sql.replace("@gametypeid", "11")
    
    game_id = []
    
    gDBAccessor.fetchRows(sql, game_id)
    
    gameId = int((game_id[0])[0])
    
    return gameId


#-------------------------------------------------------------------    
def addPlayersToDB(arenaGameObj, gameId):
    for player in arenaGameObj.players:
        addPlayerToDB(arenaGameObj.players[player], gameId, arenaGameObj._getLeader())
    for player in arenaGameObj.exitedPlayers:
        addPlayerToDB(arenaGameObj.exitedPlayers[player], gameId, arenaGameObj._getLeader())

        
#-------------------------------------------------------------------
def addPlayerToDB(player, gameId, leader):
    global ADD_PLAYER
    global gDBAccessor    
    
    sql = ADD_PLAYER
    sql = sql.replace("@playerid", str(player.playerId))
    sql = sql.replace("@gameid", str(gameId))
    sql = sql.replace("@teamid", str(player.teamId))
    sql = sql.replace("@quitearly", str(int(player.quitEarly)))
    win = 0
    winner = leader
    if(winner == 0):
        win = 2
    elif(winner == player.teamId and not(player.quitEarly)):
        win = 1
    sql = sql.replace("@winlose", str(win))
    sql = sql.replace("@kills", str(player.kills))
    sql = sql.replace("@deaths", str(player.killed))

    gDBAccessor.fetchRows(sql, None)

    addPlayerCoinStats(player, gameId)
    addPlayerKillStats(player, gameId)


#-------------------------------------------------------------------
def addPlayerCoinStats(player, gameId):
    global ADD_PLAYER_COINS
    global gDBAccessor
    
    sql = ADD_PLAYER_COINS
    sql = sql.replace("@playerid", str(player.playerId))
    sql = sql.replace("@gameid", str(gameId))
    sql = sql.replace("@coinsScored", str(player.points))
    gDBAccessor.fetchRows(sql, None)

    
#-------------------------------------------------------------------
def addPlayerKillStats(player, gameId):
    global ADD_PLAYER_KILLS
    global gDBAccessor
    
    for victimId in player.killsList:
        sql = ADD_PLAYER_KILLS
        sql = sql.replace("@gameid", str(gameId))
        sql = sql.replace("@killerid", str(player.playerId))
        sql = sql.replace("@killedid", str(victimId))
        sql = sql.replace("@totalkills", str(player.killsList[victimId]))
        gDBAccessor.fetchRows(sql, None)
        


#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    pass

#-------------------------------------------------------------------
def InitializeKEPEventHandlers(dispatcher, handler, dbgLevel):
    """Called from C to initialize all the handlers in this script"""
    global gDBAccessor

    gDBAccessor = DatabaseInterface.DBAccess("KachingDb", KACHING_DB)    
