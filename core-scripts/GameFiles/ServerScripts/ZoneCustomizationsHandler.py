#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP
import xml.dom.minidom

# C functions
import ServerGlobals
import InstanceId
import Dispatcher
import Event
import GetSet
import ParentHandler
import PlayerIds

import ZoneCustomizationsDb
import WorkerThread
import time

gDispatcher = 0
gParent = 0
gBackgroundThread = None
gRootTag = None

def logMsg( level, msg ):
    #needed for logging
    import ParentHandler
    ParentHandler.LogMessage( gParent, level, msg )
    #print msg


#-------------------------------------------------------------------
def DynObjRequestHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
    "Get the dynamic objects for the user -- event filter is the zoneIndex"
    import Game

    zoneIndex = filter
    instanceId = Event.DecodeNumber( event )

    zoneType = InstanceId.GetType(zoneIndex)

    if zoneType == KEP.CI_PERMANENT  or zoneType == KEP.CI_ARENA:
        instanceId = 1
        if zoneType == KEP.CI_ARENA:
            zoneIndex = InstanceId.MakeId(InstanceId.GetId(zoneIndex), 0, InstanceId.GetType(zoneIndex)) #TEMP

    logMsg(KEP.DEBUG_LOG_LEVEL, "Request for dynamic objects received from: "+str(event)+"\n In zoneIndex: "+str(filter)+" instanceId: "+str(instanceId) )

    gBackgroundThread.performWork(threadDynObjRequest, fromNetId, zoneIndex, instanceId)

    return KEP.EPR_CONSUMED 

#-------------------------------------------------------------------
def threadDynObjRequest(fromNetId, zoneIndex, instanceId):

    global gDispatcher

    import Game

    zoneCustomizations = list()

    try:
        x, y, z = 0, 0, 0 
        
        player = Game.GetPlayerByNetId(Dispatcher.GetGame(gDispatcher),fromNetId) 

        if player != 0:
            x, y, z = GetSet.GetVector(player, PlayerIds.N3DPOSITION)
            
        ZoneCustomizationsDb.getZoneCustomizations( zoneIndex, instanceId, zoneCustomizations, x, y, z )

        logMsg(KEP.TRACE_LOG_LEVEL, str(zoneCustomizations) )

        sendDataToClient( fromNetId, "r\t0\tOk\n" )
        i = 0

        exceededLimit = 0
        modulusDone = 0;

        maxDPlayDataToSend = ServerGlobals.NETWORK_DEFINITIONS_MaxBandwidthPerPerson
        data = ""

        # This zonecustomizations script is specific to 3DApps. Since we're not trying to limit a player's bandwidth in a given 'heavy' zone versus other
        # players in lighter zones on the same server, chunking the data packets isn't quite as important as in WOK. But, this alleviates possible error cases whereby
        # the MaxBandwidthPerPerson field needs to pushed higher and higher due to zones becoming more and more complex.

        for r in zoneCustomizations:
            preRec =  len(r[0])
            preData = len(data)
            preTotal = preRec + preData

            logMsg(KEP.TRACE_LOG_LEVEL, "ZoneCustomizationsHandler.py. PreRec is:" + str(preRec) )
            logMsg(KEP.TRACE_LOG_LEVEL, "ZoneCustomizationsHandler.py. PreData is:" + str(preData) )
            logMsg(KEP.TRACE_LOG_LEVEL, "ZoneCustomizationsHandler.py. PreTotal is:" + str(preTotal) )

            if preRec > int(maxDPlayDataToSend): # The length of an individual record exceeds our MaxBandwidthPerPerson setting. Should not occur but provide a safety.
                logMsg(KEP.ERROR_LOG_LEVEL, "Length of ZoneCustomization record exceeds NetworkDefinitions.xml MaxBandwidthPerPerson for DPlay. Record lengh is:" + str(preRec) + " MaxBandWidthPerPerson is:" + str(maxDPlayDataToSend) )
                data = ""
                data = data + r[0]
                sendDataToClient( fromNetId, data ) # This will error in DPlay due to exceeding the packet size, let this course of events fold out.
                return True

            if preTotal > int(maxDPlayDataToSend):
                exceededLimit = 1

            i = i + 1
            if i % 50 == 0:
                modulusDone = 1;

            if exceededLimit or modulusDone:
                #send what has been accumlated thus far
                logMsg(KEP.TRACE_LOG_LEVEL, "ZoneCustomizationsHandler.py. PreTotal before sending is:" + str(preTotal) + " Variable i is:" + str(i) + " Modulus:" + str(modulusDone) + " Exceeded:" + str(exceededLimit))
                logMsg(KEP.TRACE_LOG_LEVEL, "ZoneCustomizationsHandler.py. Length of ZoneCustomization data is:" + str(len(data)) + " MaxBandwidthPerPerson=" + str(maxDPlayDataToSend) )
                sendDataToClient( fromNetId, data )
                data = ""
                exceededLimit = 0
                modulusDone = 0
                time.sleep(.2)

            data = data + r[0]

            logMsg(KEP.TRACE_LOG_LEVEL, "ZoneCustomizationsHandler.py. Data addition is:" + str(len(data))  )
            
        if len(data) > 0:  # the remaining data
            logMsg(KEP.TRACE_LOG_LEVEL, "ZoneCustomizationsHandler.py. Remaining length of ZoneCustomization data is:" + str(len(data)) + " MaxBandwidthPerPerson=" + str(maxDPlayDataToSend) )
            sendDataToClient( fromNetId, data )

        sendDataToClient( fromNetId, "z\n" )
        
    except Exception, ee:       
        logMsg(KEP.WARN_LOG_LEVEL, "Exception getting zone customizations " + str(ee) )
        sendDataToClient( fromNetId, "r\t2\t" + str(ee) + "\n" )
    

#-------------------------------------------------------------------
def sendDataToClient( destination, xml ):

    ret = False

    e = Dispatcher.MakeEvent( gDispatcher, KEP.SERVERDYNOBJ_REPLY_EVENT_NAME )
    #print "destination: "+str(destination)+"\n"
    #print "e: "+str(e)+"\n"
    #print "gDispatcher: "+str(gDispatcher)+"\n"
    if e != 0:
        
        ret = True
        Event.EncodeString(e, xml)
        Event.AddTo(e, destination)
        Dispatcher.QueueEvent( gDispatcher, e )


    return True

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher
    global gBackgroundThread

    gParent = parent
    gDispatcher = dispatcher

    ParentHandler.RegisterEventHandler( dispatcher, parent,  DynObjRequestHandler, 1,0,0,0, KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME, KEP.HIGH_PRIO )

    gBackgroundThread = WorkerThread.WorkerThread("ZoneCustomizations")
    ParentHandler.LogMessage(parent, KEP.INFO_LOG_LEVEL, str(gBackgroundThread) + " tid: "+str(gBackgroundThread._get_my_tid()))

    return  True 


#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher

    # grab instances for debugging
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME, KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, KEP.SERVERDYNOBJ_REPLY_EVENT_NAME, KEP.MED_PRIO )

    


#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give us a chance to cleanup before shutdown or re-init"
    return KEP.getDependentModules(__name__)
        
