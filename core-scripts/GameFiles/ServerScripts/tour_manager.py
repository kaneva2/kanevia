#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#tour_manager.py - main server side handler for the tour system
#Copyright (c) 2009, Kaneva, Inc. All Rights Reserverd
#Author:  Greg Frame
#  Date:  08-20-2009
#
#Revisions
#08-20-2009 - Initial creation
#
#
#import base modules
import time, inspect
import pdb

#import helper Python code
import GameGlobals
import KEP
import CMD

#get set ids
import PlayerIds
import ChatType

#import the C methods from the exe
import ParentHandler
import Dispatcher
import Event
import GetSet
import InstanceId

#import other modules
import WorldFameAwarder
import tour_db
        
#menus
TOUR_SIGNUP_MENU_ID           = 675 #used to singup to a tour
TOUR_ALREADY_SIGNEDUP_MENU_ID = 676 #error message if they are already signed up
TOUR_STATUS_MENU_ID           = 677 #status menu containing time till next zone
TOUR_WORKER_MENU_ID           = 678 #used merely to interact with the NPC
TOUR_HELP_MENU_ID             = 679 #used to display help to the member
TOUR_WELCOME_MENU_ID          = 680 #used on the initial zone load
TOUR_CONFIRMATION_MENU_ID     = 681 #used after a successful signup
TOUR_COMPLETION_MENU_ID       = 682 #used when the tour is completed

#events
SIGNUP_EVENT_NAME       = "PlayerTourSignup"      #client -> server
VALIDATION_EVENT_NAME   = "TourSignupValidation"  #client -> server
QUIT_EVENT_NAME         = "PlayerTourQuit"        #client -> server
TOUR_INFO_EVENT_NAME    = "TourInfoEvent"         #server -> client
TOUR_QUIT_EVENT         = "TourQuitEvent"         #server -> client
TOUR_GETNEXT_EVENT      = "TourGetNextZoneEvent"  #client -> server
TOUR_COMPLETE_EVENT     = "TourCompleteEvent"     #server -> client
TOUR_OPEN_SIGNUP        = "TourOpenSignup"        #server -> client
#CLIENT_DISCONNECT_EVENT = "DisconnectedEvent"   #server -> client

#configuration
TOUR_FAME_PACKET_OFFSET = 55  #first zone tour packet - 2
TOUR_FAME_BONUS_PACKET  = 67  #bonus packet id
TOUR_FAME_BONUS_PACKET_FIRST_TIME  = 481  #bonus packet id
TOUR_BUFFER_TIME        = 5   #additional time to add to the tour start in seconds as a buffer

#common globals
gDispatcher             = 0          
gParent                 = 0
gActiveTourZoneCount    = 11

def HandleClientDisconnect(dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game
    game = Dispatcher.GetGame(gDispatcher)
    
    playernetid = Event.DecodeNumber(event)
    result=[]
    player = Game.GetPlayerByNetId(game, playernetid)
    tour_db.removePlayerFromTour( player, result )
    
    return KEP.EPR_OK 
    
def processRewardsForPlayer( player ):
    import Game
    game 	= Dispatcher.GetGame(gDispatcher)
    toNetId 	= GetSet.GetNumber(player, PlayerIds.CURRENTID)
    
    #ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:processRewardsForPlayer(): Redeeming fame packet for: " + str(toNetId) )
    WorldFameAwarder.redeemWorldFamePacket (toNetId, 1, TOUR_FAME_BONUS_PACKET)
    WorldFameAwarder.redeemWorldFamePacket (toNetId, 1, TOUR_FAME_BONUS_PACKET_FIRST_TIME)
    Game.SendTextMessage( game, player, "You have received the bonus for completing the tour!", ChatType.System)

def removePlayerFromTour( player ):
    import Game
    
    game = Dispatcher.GetGame(gDispatcher)
    
    result = []
    tour_db.removePlayerFromTour( player, result )
    
    #send event to the tour manager on the client that in effect removes the status menu
    #this should be sent no matter what to clear the client flag
    toNetId = GetSet.GetNumber(player, PlayerIds.CURRENTID)
    ev = Dispatcher.MakeEvent(gDispatcher, TOUR_QUIT_EVENT)
    Event.AddTo(ev, toNetId)
    Dispatcher.QueueEvent( gDispatcher, ev)

# For a given player at a netid, identify the next zone in the tour, and dispatch this to the client.
# This function also attempts to redeem a fame packet for the last zone completed.
# returns: 
#   1 if the next zone is identified and dispatched.
#   0 if no subsequent zone identified i.e. the end of the tour.
#
def sendTourInfoEvent(toNetId):
    import Game
    #ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:sendTourInfoEvent(): sending tour info event to: " + str(toNetId) )
    
    game = Dispatcher.GetGame(gDispatcher)
    player = Game.GetPlayerByNetId(game, toNetId)
    playerId = int(GetSet.GetNumber( player, PlayerIds.HANDLE))
    ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:sendTourInfoEvent(): netid=" + str(toNetId) + ";playerid=" + str(playerId) ) 

    rowList = []
    tour_db.getNextTourZone(rowList)
    if len(rowList) > 0 and int(rowList[0][0]) > -1:
        zoneInstance = rowList[0][1]
    
        ev = Dispatcher.MakeEvent(gDispatcher, TOUR_INFO_EVENT_NAME)
        Event.EncodeNumber(ev, rowList[0][0]) #index
        Event.EncodeNumber(ev, rowList[0][1]) #instance
        Event.EncodeNumber(ev, rowList[0][2] + TOUR_BUFFER_TIME ) #time (add a little for overlap protection)
        Event.EncodeNumber(ev, rowList[0][3]) #nextzone
        Event.EncodeNumber(ev, rowList[0][4]) #totalzones
        Event.AddTo(ev, toNetId)
        Dispatcher.QueueEvent( gDispatcher, ev)
        #ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:sendTourInfoEvent(): queued event for tour info zone=" + str(rowList[0][3]) )
        
        currentzone =  rowList[0][3]
        if( currentzone > 1 ): #next zone, meaning you are in a valid zone

			# Let's identify the current running tour.
			#
            result = []
            tour_db.getActiveTour( result )
            if len(result) > 0 :
                runningtour = result[0][0]
                activetour	= result[0][1]
            else:
				runningtour = -1
				activetour  = -1
        
            nextfamepacket = 0
            nextfamepacket = tour_db.getNextFamePacketForPlayer( player )
            ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:sendTourInfoEvent(): next frame packet [" + str(nextfamepacket) + "] for: " + str(playerId) )
            if(nextfamepacket != -1):

                # Return values
                #  0 - Success
                # -2 - Already redeemed
                # -3 - No packet found
                # -4 - No Action taken, did not meet packet requirements
                # -5 - Packet Id not redeemable
                # -6 - User not able to use fame
                # -7 - Error
                # -8 - User is at max redemption
                #
                redemptionResponse = WorldFameAwarder.redeemWorldFamePacket(toNetId, 1, nextfamepacket )
                ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:sendTourInfoEvent(): Redemption of packet [" + str(nextfamepacket) + "] for player [" + str(playerId) + "] result [" + str(redemptionResponse) + "]" )
                if ( redemptionResponse == 0 ):
                    tour_db.incrementFamePacketForPlayerWithId(player)
                    tour_db.addTourPlayerHistory( playerId, zoneInstance, activetour, nextfamepacket )
                else:
                    ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "TOUR:MGR:sendTourInfoEvent(): Exception raised redeeming fame packet [" + str(redemptionResponse)  + "]" )


            #update the database record for this tour
            if runningtour != -1 :
                tour_db.updateTourVisits(player, runningtour, currentzone-1)
        
        return 1
    else:
        #ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:sendTourInfoEvent(): I think the tour is done, removing this player." )
        removePlayerFromTour( player )
        return 0
        
#handles a player quitting a tour
def HandlePlayerTourQuit( dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game
    #ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:HandlePlayerTourQuit(): player netid : " + str(fromNetId) )
    
    game = Dispatcher.GetGame(gDispatcher)
    player = Game.GetPlayerByNetId(game, fromNetId)
    
    removePlayerFromTour( player )
    
    return KEP.EPR_CONSUMED    

def HandleGetNextTourZone( dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game
    #ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:HandleGetNextZone(): get next tour zone for player netid : " + str(fromNetId) )
       
    game = Dispatcher.GetGame(gDispatcher)
    player = Game.GetPlayerByNetId(game, fromNetId)
    
    result = []
    tour_db.getActiveTour( result )
    if len(result) > 0 :
        activetour = result[0][0]
        gActiveTourZoneCount = result[0][4]
        
        if( sendTourInfoEvent(fromNetId) == 0 ):
            #send tour complete event to the client
            ev = Dispatcher.MakeEvent(gDispatcher, TOUR_COMPLETE_EVENT)
            Event.AddTo(ev, fromNetId)
            Dispatcher.QueueEvent( gDispatcher, ev)
            
            processRewardsForPlayer(player)
            Game.SendTextMessage( game, player, "Tour Complete, check back with the tour guide for the next tour!", ChatType.System) 
    else:
        ParentHandler.LogMessage( gParent, KEP.WARN_LOG_LEVEL, "TOUR:MGR:HandleGetNextZone(): No Active tours found in the database!") 
        Game.SendTextMessage( game, player, "There are currently no acvtive tours, check back later.", ChatType.System)
    
    return KEP.EPR_CONSUMED

# handles a new player signing up to a tour
def HandlePlayerTourSignup( dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game
    #ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:HandlePlayerTourSignup(): player netid : " + str(fromNetId) )
    
    game = Dispatcher.GetGame(gDispatcher)
    player = Game.GetPlayerByNetId(game, fromNetId)
    
    #players currently can only be in one tour
    removePlayerFromTour( player )
    
    result = []
    tour_db.getActiveTour( result )
    if len(result) > 0 :
        activetour = result[0][0]
        tourfull = tour_db.isTourFull( activetour)
        if tourfull == 1:
            Game.SendTextMessage( game, player, "The current tour is full, please visit the next tour.", ChatType.System)
            return KEP.EPR_CONSUMED 
            
        tour_db.signupToTour( player, activetour, result )
        #Game.SendTextMessage( game, player, "Tour signup successful!", ChatType.System)
        if( sendTourInfoEvent(fromNetId) == 0 ):
            Game.SendTextMessage( game, player, "No tours are currently available, please check back later!", ChatType.System)
    else:
        ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "TOUR:MGR:HandlePlayerTourSignup(): Failed to sign up player to tour, this should not happen!") 
        Game.SendTextMessage( game, player, "The current tour is unavailable, check back later.", ChatType.System)
    
    return KEP.EPR_CONSUMED

# checks to see if the player is already signed up, this is called from the open event of the menu    
def HandleTourSignupValidation( dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game
    game = Dispatcher.GetGame(gDispatcher)
    
    #ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:HandleTourSignupValidation(): player netid : " + str(fromNetId) )
    player = Game.GetPlayerByNetId(game, fromNetId)
    #Game.SendTextMessage( game, player, "Got your Validation Message!", ChatType.System)
    
    #is there an active tour?
    result = []
    tour_db.getActiveTour( result )
    if len(result) > 0 :
        activetour = result[0][0]
        ended = result[0][4]
        if ( ended == 1):
            Game.SendTextMessage( game, player, "No tours are currently available, please check back later!", ChatType.System)
            return KEP.EPR_CONSUMED    
    else:
        Game.SendTextMessage( game, player, "No tours are currently available, please check back later!", ChatType.System)
        return KEP.EPR_CONSUMED
 
    #is the player already signed up?
    result = []
    tour_db.getPlayerStatus(player, result)
    if len(result) > 0:
        signedup = result[0][0]
        ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "TOUR:MGR:HandleTourSignupValidation(): signed up = : " + str(signedup) )
        if signedup != 0:
            #send already signed up event to client
            removePlayerFromTour( player )
     
        rows=[]
        ret = tour_db.getTourDetails(rows, activetour)
        if ret == 1:
            ev = Dispatcher.MakeEvent(gDispatcher, TOUR_OPEN_SIGNUP)
            Event.AddTo(ev, fromNetId)
            Event.EncodeString(ev, rows[0][0])
            Event.EncodeString(ev, rows[0][1])
            Event.EncodeString(ev, rows[0][2])
            Dispatcher.QueueEvent( gDispatcher, ev) #send event to client to open the signup menu
        else:
            Game.SendTextMessage( game, player, "No tours are currently available, please check back later!", ChatType.System)
            return KEP.EPR_CONSUMED
        
    else:    
        #error the count should return a value always
        ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "TOUR:MGR:HandleTourSignupValiation(): Get Player status failed, this should never happen!")
        Game.SendTextMessage( game, player, "Sorry the tour management system is experienceing problems try again later.", ChatType.System)
         
    return KEP.EPR_CONSUMED

    
#-------------------------------------------------------------------
#
# Called from C to initialize all the handlers in this script
#
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    
    global gParent
    global gDispatcher

    gParent = handler
    gDispatcher = dispatcher

    # get the game API 
    try:
        callable( Game.GetPlayerByNetId )
    except NameError:
        if not Dispatcher.GetFunctions( gDispatcher, KEP.GET_SERVER_FN ):
            Dispatcher.LogMessage( dispatcher, KEP.WARN_LOG_LEVEL,  "TOUR:MGR:InitializeKepEventHandlers(): Python tour_manager.py couldn't load server Game functions!!" )
            return False
        import Game

        # one more check        
        try:
            callable(Game.GetPlayerByNetId)
        except NameError:
            return False

    # register the event handler
    ParentHandler.RegisterEventHandler( dispatcher, handler, HandlePlayerTourSignup, 1,0,0,0, SIGNUP_EVENT_NAME, KEP.MED_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler, HandleTourSignupValidation, 1,0,0,0, VALIDATION_EVENT_NAME, KEP.MED_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler, HandlePlayerTourQuit, 1,0,0,0, QUIT_EVENT_NAME, KEP.MED_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler, HandleGetNextTourZone, 1,0,0,0, TOUR_GETNEXT_EVENT, KEP.MED_PRIO )
    #ParentHandler.RegisterEventHandler( dispatcher, handler, HandleClientDisconnect, 1,0,0,0, CLIENT_DISCONNECT_EVENT, KEP.MED_PRIO )
        
    return True


#-------------------------------------------------------------------
#
# Called from C to initialize all the events
#
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher

    #import gc
    #gc.set_debug( gc.DEBUG_LEAK )

    # call the c funtion to register our events
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    # register our incoming events
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, SIGNUP_EVENT_NAME, KEP.MED_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, VALIDATION_EVENT_NAME, KEP.MED_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, QUIT_EVENT_NAME, KEP.MED_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, TOUR_GETNEXT_EVENT, KEP.MED_PRIO )

    #outgoing events
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, TOUR_INFO_EVENT_NAME, KEP.MED_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, TOUR_QUIT_EVENT, KEP.MED_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, TOUR_COMPLETE_EVENT, KEP.MED_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, TOUR_OPEN_SIGNUP, KEP.MED_PRIO )

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give s a chance to cleanup before shutdown or re-init"
    
    ret = KEP.getDependentModules(__name__)

    return ret
