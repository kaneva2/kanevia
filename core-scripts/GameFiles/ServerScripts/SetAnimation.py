#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#python modules
import time, inspect
import pdb

#import helper Python code
import KEP
import CMD

#get set ids
import PlayerIds
import ChatType

#import the C methods from the exe
import ParentHandler
import Dispatcher
import Event
import GetSet
import InstanceId
import GameGlobals

gDispatcher = 0
gParent = 0

def HandleSetAnimation( dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game
    
    #decode the parameter
    playernetid = Event.DecodeNumber(event)
    animation   = Event.DecodeNumber(event)

    #get the game object
    game = Dispatcher.GetGame( dispatcher )

    #get the player object
    player = Game.GetPlayerByNetId( game, fromNetId )
    
    GetSet.SetNumber( player, PlayerIds.ANIMGLID, animation )
    
    Game.UpdatePlayerData( game, fromNetId )
    
    return KEP.EPR_CONSUMED
    
#-------------------------------------------------------------------
#
# Called from C to initialize all the handlers in this script
#
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    
    global gParent
    global gDispatcher

    gParent = handler
    gDispatcher = dispatcher

    # get the game API 
    try:
        callable( Game.GetPlayerByNetId )
    except NameError:
        if not Dispatcher.GetFunctions( gDispatcher, KEP.GET_SERVER_FN ):
            Dispatcher.LogMessage( dispatcher, KEP.WARN_LOG_LEVEL,  "Python SetAnimation couldn't load server Game functions!!" )
            return False
        import Game

        # one more check        
        try:
            callable(Game.GetPlayerByNetId)
        except NameError:
            return False

    ParentHandler.RegisterEventHandler( dispatcher, handler, HandleSetAnimation, 1,0,0,0, "SetAnimation", KEP.HIGH_PRIO )
    
    
#-------------------------------------------------------------------
#
# Called from C to initialize all the events
#
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher

    #import gc
    #gc.set_debug( gc.DEBUG_LEAK )

    # call the c funtion to register our events
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "SetAnimation", KEP.HIGH_PRIO )

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give s a chance to cleanup before shutdown or re-init"
    
    ret = KEP.getDependentModules(__name__)

    return ret
