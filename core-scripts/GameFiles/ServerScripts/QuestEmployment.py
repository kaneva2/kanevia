#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#


#python modules
import datetime
import time
import urllib2

#import helper Python code
import aaSqlUtil
import GameGlobals
import FameIds
import KEP
import MenuLauncher
import ServerGlobals
import StringTables
import WorldFameAwarder
import QuestEmployDb
import GetSetUtil
import PlayerIds
import GetSet
    
# C functions
import Dispatcher
import Event
import ParentHandler


DELAY_HOURS = 6
DELAY_SECONDS = DELAY_HOURS * 3600
CREW_RANKS = [ [ 1, 'Trainee', 0 ],
               [ 2, 'Worker', 0 ],
               [ 5, 'Lead', 0 ],
               [ 10, 'Supervisor', 0 ],
               [ 20, 'Manager', 0 ],
               [ 35, 'Director', 0 ],
               [ 50, 'Vice President', 0 ]
             ]

MENU_STATES = { 'PRE_WORK' : 0, 'INVITE' : 1, 'TOP_RANK' : 2, 'CLOSED' : 3, 'GOOD_JOB' : 4 }



#-------------------------------------------------------------------
def enterEmployment( fromNetId, selectionIndex, QID_ODDJOB, lastWorkTime, crewCount ):
    import Game

    level = getLevel( crewCount )
    levelTitle = CREW_RANKS[ level ][ 1 ]
    msgTitle, msgSubtitle, msgBody = "", "", ""
    curDateTime = datetime.datetime.now()
    curSeconds = time.mktime( curDateTime.timetuple() )
    setCrewRanksRewards()
    
    deltaSeconds = curSeconds - lastWorkTime
    remainderSeconds, remainderStr = 0, ""
    if deltaSeconds > DELAY_SECONDS:
        Dispatcher.LogMessage( KEP.dispatcher, KEP.TRACE_LOG_LEVEL, " TIME OKAY " + str( selectionIndex ) )
        
        if selectionIndex == MENU_STATES[ "PRE_WORK" ]:
            
            if levelTitle != 'Vice President':
                MenuLauncher.setJobMenuState( fromNetId, QID_ODDJOB, MENU_STATES[ "INVITE" ] )
                msgTitle = StringTables.EMPLOYMENT[ 'TITLE_INVITE' ]
                msgSubtitle = StringTables.EMPLOYMENT[ 'SUBTITLE_INVITE' ]
                msgBody = StringTables.EMPLOYMENT[ 'BODY_INVITE' ]
            else:
                performJob( fromNetId, curSeconds, level )
                MenuLauncher.setJobMenuState( fromNetId, QID_ODDJOB, MENU_STATES[ "TOP_RANK" ] )
                msgTitle = StringTables.EMPLOYMENT[ 'TITLE_TOP_RANK' ]
                msgSubtitle = StringTables.EMPLOYMENT[ 'SUBTITLE_INVITE' ]
                msgBody = StringTables.EMPLOYMENT[ 'BODY_TOP_RANK' ]
        elif selectionIndex == MENU_STATES[ "INVITE" ]:
            performJob( fromNetId, curSeconds, level )
            MenuLauncher.setJobMenuState( fromNetId, QID_ODDJOB, MENU_STATES[ "GOOD_JOB" ] )
            msgTitle = StringTables.EMPLOYMENT[ 'TITLE_GOOD_JOB' ]
            msgSubtitle = ""
            msgBody = StringTables.EMPLOYMENT[ 'BODY_GOOD_JOB' ]
            #msgTitle = msgTitle.replace( "@rewards", str( CREW_RANKS[ level ][ 2 ] ) )
            #msgBody = msgBody.replace( "@hours", str( DELAY_HOURS ) )
            #MenuLauncher.setTextInOpenMenu( fromNetId, QID_ODDJOB, msgTitle, msgSubtitle, msgBody )
    else:
        Dispatcher.LogMessage( KEP.dispatcher, KEP.TRACE_LOG_LEVEL, "enterEmployment TIME NOT OKAY " + str( selectionIndex ) )
        
        MenuLauncher.setJobMenuState( fromNetId, QID_ODDJOB, MENU_STATES[ "CLOSED" ] )
        msgTitle = StringTables.EMPLOYMENT[ 'TITLE_CLOSED' ]
        msgSubtitle = StringTables.EMPLOYMENT[ 'SUBTITLE_CLOSED' ]
        if levelTitle != 'Vice President':
            msgBody = StringTables.EMPLOYMENT[ 'BODY_CLOSED' ]
        else:
            msgBody = StringTables.EMPLOYMENT[ 'BODY_CLOSED_TOP' ]
        remainderSeconds = DELAY_SECONDS - deltaSeconds
        remainderStr = time.strftime("%H:%M:%S", time.gmtime( remainderSeconds ) )
    msgTitle = msgTitle.replace( "@rewards", str( CREW_RANKS[ level ][ 2 ] ) )
    msgTitle = msgTitle.replace( "@title", levelTitle )
    msgTitle = msgTitle.replace( "@hours", str( DELAY_HOURS ) )
    msgSubtitle = msgSubtitle.replace( "@crewsize", str( crewCount ) )
    msgSubtitle = msgSubtitle.replace( "@title", levelTitle )
    msgBody = msgBody.replace( "@rewards", str( CREW_RANKS[ level ][ 2 ] ) )
    msgBody = msgBody.replace( "@timeleft", remainderStr )
    msgBody = msgBody.replace( "@hours", str( DELAY_HOURS ) )
    #Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, msgTitle )
    
    MenuLauncher.setTextInOpenMenu( fromNetId, QID_ODDJOB, msgTitle, msgSubtitle, msgBody )
    MenuLauncher.setJobRankTable( fromNetId, QID_ODDJOB, CREW_RANKS )
        
            
        
#-------------------------------------------------------------------
def getCrewCount( NetID ):
    import Game
    
    playerID = 0

    playerObj = Game.GetPlayerByNetId( Dispatcher.GetGame( KEP.dispatcher ), NetID )
    if playerObj:
        playerID = GetSet.GetNumber( playerObj, PlayerIds.HANDLE )
    else:
        Dispatcher.LogMessage( KEP.dispatcher, KEP.ERROR_LOG_LEVEL, "QuestEmployment.getCrewCount failed.  Null player object." )
        return
        
    userID = aaSqlUtil.sqlGetKanevaUserIdFromPlayerId( playerID )
    crewSize = QuestEmployDb.sqlGetCrewCount( userID )
    
    Dispatcher.LogMessage( KEP.dispatcher, KEP.TRACE_LOG_LEVEL, "GET CREW COUNT = " + str( userID ) + " : " + str( crewSize ) )
    
    if crewSize < 0:
        crewSize = 0
        
    return crewSize + 1

#-------------------------------------------------------------------        
def getLevel( crewCount ):
    for level in range( 1, len( CREW_RANKS ) ):
        if crewCount < CREW_RANKS[ level ][ 0 ]:
            return level - 1
    return len( CREW_RANKS ) - 1
            
#-------------------------------------------------------------------        
def setCrewRanksRewards():
    rewardList = QuestEmployDb.sqlGetCrewRankRewards()
    for count in range( 0, len( CREW_RANKS ) ):
        CREW_RANKS[ count ][ 2 ] = rewardList[ count ][ 0 ]
         
#-------------------------------------------------------------------
def performJob( NetID, curSeconds, level ):
    import Game

    #Pass this back to foreground to apply GetSet changes
    e = Dispatcher.MakeEvent( KEP.dispatcher, "PerformEmploymentJobEvent" )
    if e != 0:
        Event.SetObjectId( e, NetID )
        Event.SetFilter( e, curSeconds )
        Event.EncodeNumber( e, level )
        Dispatcher.QueueEvent( KEP.dispatcher, e )

    ##Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, ">>> " + str( FameIds.JOB_FAME_IDS[ level ] ) )
    ##Dispatcher.LogMessage( KEP.dispatcher, KEP.TRACE_LOG_LEVEL, "performJob return: "+str(ret) )

    
 #-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "PerformEmploymentJobEvent", KEP.MED_PRIO )

    
    
    
    
    
    
    
