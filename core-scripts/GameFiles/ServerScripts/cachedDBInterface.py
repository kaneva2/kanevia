#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#cachedDBAccess
#author:  greg frame
#date  :  August 20, 2009
#this class implements a cached method to help reduce database activity
#this class is inetnded for queries with SMALL result sets
import ServerGlobals
import KEP
import DatabaseInterface
import md5
from datetime import date, time, timedelta, datetime

gDispatcher = 0
gParent = 0

def logMsg( level, msg ):
    #needed for logging                                               
    import ParentHandler
    ParentHandler.LogMessage( gParent, level, msg )
    #print msg

class cachedDBAccess( DatabaseInterface.DBAccess ):
    MAX_ROW_CACHE = 100
    hashtable = dict()
    rowcache  = dict()

    def fetchRows(self, sql, crows, timeout=-1):
        processed = 0
        
        #please dont try and cache write type commands coming through here
        tst = sql.upper()
        if(tst.find("INSERT")>0 or tst.find("UPDATE")>0 or tst.find("DELETE")>0 or tst.find("CALL")>0 ):
            logMsg( KEP.DEBUG_LOG_LEVEL, "cachedDBAccess: cant cache write sql="  + sql)
            timeout = -1    
        
        #timeout in seconds
        #pass a timeout of <0 and it will reset the cache
        #this allows a program to do a non cached query
        sqlhash = md5.new(sql).hexdigest()
        #logMsg( KEP.DEBUG_LOG_LEVEL, "cachedDBAccess: sql is:"  + sql)
        #logMsg( KEP.DEBUG_LOG_LEVEL, "cachedDBAccess: sqlhash is:"  + sqlhash)
        #logMsg( KEP.DEBUG_LOG_LEVEL, "cachedDBAccess: timeout is:"  + str(timeout))
               
        if( timeout > 0 and sqlhash in self.hashtable ):
            #check timeout value
            timeouttime = self.hashtable.get(sqlhash)
            if( timeouttime > datetime.now() ): #cache still good
                for row in self.rowcache[sqlhash]: #return the rows
                    crows.append(row)
                logMsg( KEP.DEBUG_LOG_LEVEL, "cachedDBAccess: returning " + str(len(crows)) + " cached rows." )
                processed = 1
                return
            else:
                #remove it from the cache
                logMsg( KEP.DEBUG_LOG_LEVEL, "cachedDBAccess: clearing cache.")
                del self.rowcache[sqlhash]
                del self.hashtable[sqlhash]
                processed = 0
        
        if( processed == 0 ):           
            #get new data if there was no cache or it expired
            logMsg( KEP.DEBUG_LOG_LEVEL, "cachedDBAccess: returning realtime results.")
            DatabaseInterface.DBAccess.fetchRows(self, sql, crows)
            if( len(crows) > 0 and len(crows) <= self.MAX_ROW_CACHE and timeout > 0):
                #calculate the datetime the query will expire
                timeouttime = datetime.now()
                timed = timedelta(seconds=timeout)
                timeouttime = timeouttime + timed
                self.hashtable[sqlhash] = timeouttime
                self.rowcache[sqlhash] = crows
                logMsg( KEP.DEBUG_LOG_LEVEL, "cachedDBAccess: cached sql for " + str(timed.seconds) + " seconds." ) 
        return
        
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    global gParent
    global gDispatcher
    global gDBAccessor
    global cache_zoneduration

    # grab instances for debugging
    gParent = parent
    gDispatcher = dispatcher
    
    KEP.debugLevel = dbgLevel