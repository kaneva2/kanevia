#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import KEP
import ParentHandler
import Dispatcher
import Event
import GetSet
import ServerGlobals
import PlayerIds
import DatabaseInterface
import ChatType

#Events used
PLAYER_TITLE_EVENT = "PlayerTitleEvent"
TITLE_LIST_EVENT = "TitleListEvent"
PLAYER_CHANGETITLE_EVENT = "PlayerChangeTitleEvent"
TITLE_EVENT = "TitleEvent"

#SQL Statements
getPlayerTitles = "select t.title_id, t.name from titles t, player_titles pt where pt.title_id = t.title_id and pt.player_id = @playerId"
getPlayerTitle = "select t.name from titles t, player_titles pt where pt.title_id = @titleId and t.title_id = @titleId and pt.player_id = @playerId"
updatePlayerTitle = "update players set title = '@titleText' where player_id = @playerId"

#Global Variables
gDispatcher = 0
gParent = 0
gDBAccessor = None

#-------------------------------------------------------------------
def logMsg(level, msg):
    "log Messages"
    
    import ParentHandler
    
    ParentHandler.LogMessage(gParent, level, msg)
  
#-------------------------------------------------------------------
def getTitles(playerId, rowList):
    "get the player's titles in an array of rows from the db"
    global gDBAccessor
    global getPlayerTitles
    
    sql = getPlayerTitles.replace("@playerId", str(playerId))
    #doSQL(sql, rowList)
    gDBAccessor.fetchRows(sql, rowList)

#-------------------------------------------------------------------    
def updateTitle(playerId, titleId):
    "update the player's title in the db"
    global gDBAccessor    
    global getPlayerTitle
    global updatePlayerTitle

    titleText = ""

    ## -1 means that the user selected <none> to be shown
    if ( titleId != -1 ):
        ##Verify that the player actually earned the title they requested
        titleNames = []
        sql = getPlayerTitle.replace("@playerId", str(playerId))
        sql = sql.replace("@titleId", str(titleId))
        gDBAccessor.fetchRows(sql, titleNames)
        if (len(titleNames) > 0):
            titleText = (titleNames[0])[0]
        
    ##Update the title
    sql = updatePlayerTitle.replace("@playerId", str(playerId))
    sql = sql.replace("@titleText", str(titleText))
    gDBAccessor.fetchRows(sql, None)
    return titleText

#-------------------------------------------------------------------
def handlePlayerTitleEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
    "handles PlayerTitleEvent, gets player's earned titles from db and returns them to the client in a TitleListEvent"
    
    playerId = Event.DecodeNumber(event)
    titlesList = []
    getTitles(playerId, titlesList)
    e = Dispatcher.MakeEvent(dispatcher, TITLE_LIST_EVENT)
    Event.AddTo(e, fromNetId)
    Event.EncodeNumber(e, len(titlesList))
    for title in titlesList:
        Event.EncodeNumber(e, int(title[0]))
        Event.EncodeString(e, str(title[1]))
    Dispatcher.QueueEvent(dispatcher, e)
    return KEP.EPR_CONSUMED

#-------------------------------------------------------------------
def handlePlayerChangeTitleEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
    "changes player's title in the db and the player record"
    
    import Game
    
    playerId = Event.DecodeNumber(event)
    titleId = Event.DecodeNumber(event)
    titleText = updateTitle(playerId, titleId)
    game = Dispatcher.GetGame(dispatcher)
    playerRecord = Game.GetPlayerByNetId(game, fromNetId)
    GetSet.SetString(playerRecord, PlayerIds.TITLE, titleText)

    channel = GetSet.GetNumber(playerRecord, PlayerIds.CURRENTCHANNEL)
    netAssignId = GetSet.GetNumber(playerRecord, PlayerIds.NETWORKASSIGNEDID)
    e = Dispatcher.MakeEvent(dispatcher, TITLE_EVENT)
    Event.EncodeString(e, titleText)
    Event.EncodeNumber(e, netAssignId)

    Game.BroadcastEvent( game, e, channel, 0, 0, 0, 0, 0, ChatType.System, 0 )
    
    return KEP.EPR_CONSUMED  

#-------------------------------------------------------------------
def InitializeKEPEvents(dispatcher, parent, dbgLevel):
    "Called from C to initialize all the events"  
    
    global gParent
    global gDispatcher

    gParent = parent
    gDispatcher = dispatcher
    
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, TITLE_LIST_EVENT, KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, PLAYER_TITLE_EVENT, KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, PLAYER_CHANGETITLE_EVENT, KEP.MED_PRIO)
   
#-------------------------------------------------------------------
def InitializeKEPEventHandlers(dispatcher, handler, dbgLevel):
    "Called from C to initialize all the handlers in this script"    

    global gDBAccessor

    gDBAccessor = DatabaseInterface.DBAccess("TitlesDB")

    # get the game API 
    try:
        callable(Game.GetPlayerByNetId)
    except NameError:
        if not Dispatcher.GetFunctions(dispatcher, KEP.GET_SERVER_FN):
            Dispatcher.LogMessage(dispatcher, KEP.WARN_LOG_LEVEL, "Python TitlesDB couldn't load server Game functions!!")
            return False
        
    ParentHandler.RegisterEventHandler(dispatcher, handler, handlePlayerChangeTitleEvent, 1, 0, 0, 0, PLAYER_CHANGETITLE_EVENT, KEP.MED_PRIO)     
    return ParentHandler.RegisterEventHandler(dispatcher, handler, handlePlayerTitleEvent, 1, 0, 0, 0, PLAYER_TITLE_EVENT, KEP.MED_PRIO)   
    

#-------------------------------------------------------------------
def ReinitalizeKEPEvents(dispatcher, parent, dbgLevel):
    "Manage resources for blade reload"

    return KEP.getDependentModules(__name__) 
