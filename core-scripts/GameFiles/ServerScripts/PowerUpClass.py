#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP

# constants for GPB
import WoK

# C functions
import InstanceId
import Dispatcher
import Event
import GetSet
import ParentHandler
import random

import ServerItemIds
import ArenaBase

COIN_NPCID          = 105
HEALTH_NPCID        = 108
SHIELD_NPCID        = 106
AMMO_BFG_NPCID      = 117
AMMO_MG_NPCID       = 119
AMMO_SG_NPCID       = 144

COMMON      = 3
UNCOMMON    = 2
RARE        = 1

class PowerUp( object ):
    def __init__( self, group, x, y, z, spawnID = 0 ):
        self._powGroup = group
        self._x, self._y, self._z = x, y, z
        self._netAssignedID = 0
        self._spawnID = spawnID
    
    def isActive( self ):
        return self._spawnID != 0

class PowerUpGroup( object ):
    def __init__( self, locationList, maxActive ):
        self._powSpawnIDWeights = dict()
        self._powerUps = list()
        self._maxActive = maxActive
        self._curActive = 0
        self.setupPowLocs( locationList )
        
    def setupPowLocs( self, locationList ):
        for element in locationList:
            x, y, z = element[ 0 ], element[ 1 ], element[ 2 ]    
            self._powerUps.append( PowerUp( self, x, y, z ) )

    def addWeightedSpawnID( self, spawnID, weight ):
        if weight >= RARE and weight <= COMMON:
            self._powSpawnIDWeights[ spawnID ] = weight
            
    def getWeightedSpawnIDList( self ):
        spawnIDList = list()
        for spawnIDKey in self._powSpawnIDWeights.keys():
            weight = self._powSpawnIDWeights[ spawnIDKey ]
            for n in range( 1, weight + 1 ):
                spawnIDList.append( spawnIDKey )
        return spawnIDList
    
    def getVacancyList( self ):
        self._curActive = 0
        vacancyList = list()
        for p in self._powerUps:
            if not p.isActive():
                vacancyList.append( p )
            else:
                self._curActive += 1
        return vacancyList
            
    def activateRandomPowerup( self ):
        vacancyList = self.getVacancyList()
        weightedSpawnIDList = self.getWeightedSpawnIDList()
        if len( weightedSpawnIDList ) > 0:
            spawnID = random.choice( weightedSpawnIDList )
            pow = random.choice( vacancyList )
            pow._spawnID = spawnID
            self._curActive += 1
            return pow
        return None

class PowerUpManager( object ):
    def __init__( self, dispatcher, game, channel ):
        self._game = game
        self._dispatcher = dispatcher
        self._channel = channel
        self._powGroups = list()
        self._spawnedItems = list() # NetworkAssignedIds
        random.seed()

    # despawn all existing ones otherwise they leak until server shutdown
    def __del__( self ):
        import Game
        for i in self._spawnedItems:
            Game.DeSpawnStaticAI( self._game, i ) 
            # print "<<<< removing power up in __del__ of PUMgr " + str(i) 
        
    def createPowerUpGroup( self, locationList, maxActive, spawnIDWeightDict ):
        group = PowerUpGroup( locationList, maxActive )
        for spawnIDKey in spawnIDWeightDict.keys():
            group.addWeightedSpawnID( spawnIDKey, spawnIDWeightDict[ spawnIDKey ] )
        while group._curActive < group._maxActive:
            self.spawnRandomPowerup( group )
        self._powGroups.append( group )
            
    def spawnRandomPowerup( self, powGroup ):
        if powGroup != None:
            p = powGroup.activateRandomPowerup()
            if p._spawnID != 0:
                self.spawnPowerup( p )

    def spawnPowerup( self, powerUp ):
        import Game
        itemPtr = Game.SpawnStaticAIByNumber( self._game, powerUp._spawnID, powerUp._x, powerUp._y, powerUp._z, self._channel )
        powerUp._netAssignedID = GetSet.GetNumber( itemPtr, ServerItemIds.NETWORKASSIGNEDID )
        self._spawnedItems.append( powerUp._netAssignedID )
        # print ">>>> adding power up " + str(powerUp._netAssignedID)
        
    def despawnPowerup( self, netID ):
        import Game
        powerUp = self.getPowerupByNetAssignedID( netID )
        if powerUp != None:
            powerUp._spawnID = 0
            powerUp._netAssignedID = 0
        for i in self._spawnedItems:
            if i == netID:
                Game.DeSpawnStaticAI( self._game, netID ) 
                self._spawnedItems.remove( i )
                # print "<<<< removing power up in despawnPowerup of PUMgr " + str(i)
                break
                     
    def getPowerupByNetAssignedID( self, netID ):
        for group in self._powGroups:
            for p in group._powerUps:
                if p._netAssignedID == netID:
                    return p
        return None

    def powerUpHit(self, player1, spawnID, fromNetId, powNetAssignedID ):
        import Game

        powerUp = self.getPowerupByNetAssignedID( powNetAssignedID )
        powGroup = None
        if powerUp != None:
            powGroup = powerUp._powGroup
        
        #health powerup param1 is max health
        if spawnID == HEALTH_NPCID: #health
            hpValue = 50
            ArenaBase.AdjustPlayerHealth(self._game, fromNetId, player1, hpValue)
            self.spawnRandomPowerup( powGroup )
            self.despawnPowerup( powNetAssignedID )
            return KEP.EPR_CONSUMED

        if spawnID == AMMO_MG_NPCID or spawnID == AMMO_SG_NPCID or spawnID == AMMO_BFG_NPCID or spawnID == SHIELD_NPCID:
            self.spawnRandomPowerup( powGroup )
            self.despawnPowerup( powNetAssignedID )
            #### TO DO: Move handling of effects, here.  (Currently they're in WOK_FoamGunArena) -Carson
            return KEP.EPR_CONSUMED

        if spawnID == COIN_NPCID:
            if powGroup != None:
                self.spawnRandomPowerup( powGroup )
            self.despawnPowerup( powNetAssignedID )
            return KEP.EPR_CONSUMED

        self.despawnPowerup( powNetAssignedID )
        return KEP.EPR_CONSUMED

    

