#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import DatabaseInterface

gDBAccessor = None

#-------------------------------------------------------------------
def getLevelRewardsByLevelId( level ):
    """get all rewards for the current fame level"""
    global gDBAccessor

    result = []

    sql = "SELECT * FROM wok.level_rewards WHERE level_id = @levelId"
    sql = sql.replace( "@levelId", str(level) )

    gDBAccessor.fetchRows( sql, result )

    return result

#-------------------------------------------------------------------
def getTitleIdFromLevel( level, titleType ):
    """get the title id based on the current level"""
    global gDBAccessor

    result = []
    titleId = -1

    sql = "SELECT @titleType FROM wok.skill_levels WHERE level_number = @level"
    sql = sql.replace( "@titleType", str(titleType) )
    sql = sql.replace( "@level", str(level) )

    gDBAccessor.fetchRows( sql, result )

    if( len(result) > 0 ):
        titleId = result[0][0]

    return int(titleId)

#-------------------------------------------------------------------
def getTitleNameFromId( titleId ):
    """get the title based off the id"""

    result = []
    titleName = ""

    sql = "SELECT name FROM wok.titles WHERE title_id = @titleId"
    sql = sql.replace( "@titleId", str(titleId) )

    gDBAccessor.fetchRows( sql, result )

    if( len(result) > 0 ):
        titleName = result[0][0]

    return titleName        

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    pass

#-------------------------------------------------------------------
def InitializeKEPEventHandlers(dispatcher, handler, dbgLevel):
    """Called from C to initialize all the handlers in this script"""
    global gDBAccessor

    gDBAccessor = DatabaseInterface.DBAccess("WorldFameAwardDb")    
