#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#python modules
import datetime
import pdb
import random

#import helper Python code
import GameGlobals
import KEP
import MenuLauncher
import QuestItemAwardDb

# C functions
import Dispatcher
import Event
import GetSet
import ParentHandler

#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------        
class QuestItemAwarder(object):
    sManagedList = list()
    
    def __init__( self, gameInstance ): 
        self._game = gameInstance
        self._itemList = list()  
        self._getSetStringID = ""
        self._getSetStringDefault = ""
        self._getSetString = ""  
    
    #------------------------------------------------------------------------------------    
    @staticmethod
    def handlePlayerSpawnedEvent( dispatcher, fromNetId, event, eventid, filter, objectid ): 
        QuestItemAwarder.initRequiredInstances( Dispatcher.GetGame( dispatcher ) )
        if len(QuestItemAwarder.sManagedList) > 0:
            netAssignedID = Event.DecodeNumber( event )
            playerObj = Event.DecodeObject( event )
            #isAICtrl = Event.DecodeNumber( event )
            #worldName = Event.DecodeString( event )
            #prevZoneIndex = Event.DecodeNumber( event )
            if playerObj:
                for awarder in QuestItemAwarder.sManagedList:
                    awarder.playerSpawned( dispatcher, fromNetId, netAssignedID, playerObj )
            else:
                return KEP.EPR_NOT_PROCESSED
        return KEP.EPR_OK
    
    #------------------------------------------------------------------------------------    
    @staticmethod
    def handlePlayerUseEvent( dispatcher, fromNetId, event, eventid, filter, objectid ):
        import Game

        ret = KEP.EPR_OK
        if len(QuestItemAwarder.sManagedList) > 0:
            idReference = Event.DecodeNumber( event )
            idUseValue = Event.DecodeNumber( event )
            invType = Event.DecodeNumber( event ) # gift or not
            playerObj = Game.GetPlayerByNetTraceId( Dispatcher.GetGame( dispatcher ), idReference, 0, 0 )
            if playerObj == 0:
                ret = KEP.EPR_NOT_PROCESSED
            for awarder in QuestItemAwarder.sManagedList:
               awarder.playerUseItem( dispatcher, fromNetId, playerObj, idUseValue )
            ret = KEP.EPR_CONSUMED
        return ret

    #------------------------------------------------------------------------------------
    @staticmethod
    def initRequiredInstances( gameInstance ):
        """
        if QuestHalloweenItemAwarder.activationRequired():
            QuestHalloweenItemAwarder.sInstance = QuestHalloweenItemAwarder( gameInstance )
            QuestItemAwarder.sManagedList.append( QuestHalloweenItemAwarder.sInstance )
        if QuestThanksgivingItemAwarder.activationRequired():
            QuestThanksgivingItemAwarder.sInstance = QuestThanksgivingItemAwarder( gameInstance )
            QuestItemAwarder.sManagedList.append( QuestThanksgivingItemAwarder.sInstance )
        """
            
    #------------------------------------------------------------------------------------       
    def awardItemByIndex( self, playerNetId, playerObj, index ):
        import Game
        if index < len( self._itemList ):
            glid = self._itemList[ index ]
            ##Game.AddInventoryItemToPlayer( self._game, playerObj, glid, 1, 0 )
            ##DatabaseAccess.addToInvPendingAdds( playerObj, glid )
        
#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------         
class QuestDailyItemAwarder( QuestItemAwarder ):
    def __init__( self, gameInstance ):
        super( QuestDailyItemAwarder, self ).__init__( gameInstance )
        self._startDate = 0
        self._endDate = 0
        
    #------------------------------------------------------------------------------------       
    @staticmethod
    def isTodayValid( startDate, endDate ):
        today = datetime.date.today()
        return ( today >= startDate and today <= endDate )
    
    #------------------------------------------------------------------------------------       
    def playerUseItem( self, dispatcher, playerNetId, playerObj, idUseValue ):
        pass
        
    #------------------------------------------------------------------------------------       
    def playerSpawned( self, dispatcher, playerNetId, netAssignedID, playerObj ):
        
        #Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL,  "Quest Item Awarder: Player Spawned in a residential zone" )
        if QuestDailyItemAwarder.isTodayValid( self._startDate, self._endDate ):
            ( stringID, stringType ) = GetSet.FindIdByName( playerObj, self._getSetStringID )
            self._getSetString = self._getSetStringDefault
            if stringID == KEP.amrNotFound:
                stringID = GetSet.AddNewStringMember( playerObj, self._getSetStringID, self._getSetStringDefault, KEP.amfSaveAcrossSpawn  )
                Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "new string member = " + str( stringID ) )
            else:
                self._getSetString = GetSet.GetString( playerObj, stringID )
                Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "old string member = " + str( self._getSetString ) )
                ### If start or end date has been altered since the creation
                ### of this entry, this string will be invalid. -Carson
        
            currentDay = self.getTodaysIndex()
            if self._getSetString[ currentDay ] != "T":
                prefix = self._getSetString[:currentDay]
                suffix = self._getSetString[currentDay+1:]
                self._getSetString = prefix + "T" + suffix
        
                Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "prefix = " + prefix )
                Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "suffix = " + suffix )
                Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "new string = " + self._getSetString )
                
                tutMsg = self.getPlayerSpawnedMessage()
                MenuLauncher.tutorialTipMenu( playerNetId, tutMsg )
        
                GetSet.SetString( playerObj, stringID, self._getSetString )
                self.awardItemByIndex( playerNetId, playerObj, currentDay )
        
    #------------------------------------------------------------------------------------       
    def getPlayerSpawnedMessage( self ):
        return ""
        
    #------------------------------------------------------------------------------------       
    def getTodaysIndex( self ):
        # Today's index in relation to the start date.  Maybe be negative.
        today = datetime.date.today()
        timeDelta = today - self._startDate 
        dayNum = timeDelta.days
        Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, str( timeDelta ) + " - today is day " + str( dayNum ) )
        return dayNum
    
    #------------------------------------------------------------------------------------       
    def setupRepeatingDefaults( self, getSetCharacter, itemGLID ):
        if isinstance( self._startDate, datetime.date ) and isinstance( self._endDate, datetime.date ):
            defaultStr = ""
            defaultList = list()
            timeDelta = self._endDate - self._startDate
            numDays = abs( timeDelta.days ) + 1
            for i in range( numDays ):
                defaultStr = defaultStr + getSetCharacter
                defaultList.append( itemGLID )
            self._getSetStringDefault = defaultStr
            self._itemList = defaultList

###UNUSED verify and delete            
###------------------------------------------------------------------------------------
###------------------------------------------------------------------------------------        
##class QuestHalloweenItemAwarder( QuestDailyItemAwarder ):
##    sInstance = None
##    PROGRESS_ID_HALLOWEEN_2008 = "10"
##    sStartDate = QuestItemAwardDb.getQuestDate( PROGRESS_ID_HALLOWEEN_2008, "datetime_begin" ).date()
##    sEndDate = QuestItemAwardDb.getQuestDate( PROGRESS_ID_HALLOWEEN_2008, "datetime_end" ).date()
##    
##    def __init__( self, gameInstance ): 
##        HALLOWEEN_CANDY_GLID = 3374
##        super( QuestHalloweenItemAwarder, self ).__init__( gameInstance )
##        self._startDate = QuestHalloweenItemAwarder.sStartDate
##        self._endDate = QuestHalloweenItemAwarder.sEndDate
##        self._getSetStringID = "halloweenCandy"
##        self.setupRepeatingDefaults( "F", HALLOWEEN_CANDY_GLID )
##        
##    #------------------------------------------------------------------------------------    
##    @staticmethod
##    def activationRequired():
##        if QuestDailyItemAwarder.isTodayValid( QuestHalloweenItemAwarder.sStartDate, QuestHalloweenItemAwarder.sEndDate ):
##            if QuestHalloweenItemAwarder.sInstance == None:
##                return True
##        else:
##            if QuestHalloweenItemAwarder.sInstance != None:
##                QuestItemAwarder.sManagedList.remove( QuestHalloweenItemAwarder.sInstance )
##                QuestHalloweenItemAwarder.sInstance = None
##        return False
##
##    #------------------------------------------------------------------------------------       
##    def getPlayerSpawnedMessage( self ):
##        msg = "Trick or Treat!?\n\nOnce each day that you log in, from now until Halloween, you will receive a piece of candy in your inventory.\n"
##        msg += "Speak to Celia, in Kaneva Plaza, to find out how collecting Halloween candy will grant you access to the Halloween Ball."
##        return msg
##
###------------------------------------------------------------------------------------
###------------------------------------------------------------------------------------        
##class QuestThanksgivingItemAwarder( QuestDailyItemAwarder ):
##    sInstance = None
##    PROGRESS_ID_THANKSGIVING_2008 = "20"
##    
##    sStartDate = QuestItemAwardDb.getQuestDate( PROGRESS_ID_THANKSGIVING_2008, "datetime_begin" ).date()
##    sEndDate = QuestItemAwardDb.getQuestDate( PROGRESS_ID_THANKSGIVING_2008, "datetime_end" ).date()
##    
##    def __init__( self, gameInstance ): 
##        super( QuestThanksgivingItemAwarder, self ).__init__( gameInstance )
##        self._startDate = QuestThanksgivingItemAwarder.sStartDate
##        self._endDate = QuestThanksgivingItemAwarder.sEndDate
##        self._getSetStringID = "thanksgivingHarvest"
##        self.setupRepeatingDefaults( "F", GameGlobals.HARVEST_PACK_GLID )
##        
##    #------------------------------------------------------------------------------------    
##    @staticmethod
##    def activationRequired():
##        if QuestDailyItemAwarder.isTodayValid( QuestThanksgivingItemAwarder.sStartDate, QuestThanksgivingItemAwarder.sEndDate ):
##            if QuestThanksgivingItemAwarder.sInstance == None:
##                return True
##        else:
##            if QuestThanksgivingItemAwarder.sInstance != None:
##                QuestItemAwarder.sManagedList.remove( QuestThanksgivingItemAwarder.sInstance )
##                QuestThanksgivingItemAwarder.sInstance = None
##        return False
##
##    #------------------------------------------------------------------------------------       
##    def getPlayerSpawnedMessage( self ):
##        msg = "Introducing the Kaneva Harvest Social\n\n"
##        msg += "Every day you sign in to The World of Kaneva from now until November 30th, you'll receive a Harvest Pack in your Inventory (one per day).\n\n"
##        msg += "Each Harvest Pack contains eight random food items, including five common, two uncommon, and one rare. Collect all 20 items and win the grand prize: a cornucopia of Thanksgiving-themed items!\n\n"
##        msg += "Visit Tom Turkey in Kaneva City for more information."
##        return msg
##    
##    #------------------------------------------------------------------------------------       
##    def playerUseItem( self, dispatcher, playerNetId, playerObj, idUseValue ):
##        import Game
##        
##        if idUseValue == GameGlobals.UV_THANKSGIVING_PACK:
##            # NO LONGER WORKS WITH PENDING ADDS!!!
##            Game.RemoveInventoryItemFromPlayer( self._game, playerObj, GameGlobals.HARVEST_PACK_GLID, 1, 1, GameGlobals.IT_NORMAL )
##            
##            r1 = random.choice( GameGlobals.HARVEST_RARES )
##            u1 = random.choice( GameGlobals.HARVEST_UNCOMMONS )
##            u2 = random.choice( GameGlobals.HARVEST_UNCOMMONS )
##            c1 = random.choice( GameGlobals.HARVEST_COMMONS )
##            c2 = random.choice( GameGlobals.HARVEST_COMMONS )
##            c3 = random.choice( GameGlobals.HARVEST_COMMONS )
##            c4 = random.choice( GameGlobals.HARVEST_COMMONS )
##            c5 = random.choice( GameGlobals.HARVEST_COMMONS )
##            #Game.AddInventoryItemToPlayer( self._game, playerObj, r1, 1, 0 )
##            #Game.AddInventoryItemToPlayer( self._game, playerObj, u1, 1, 0 )
##            #Game.AddInventoryItemToPlayer( self._game, playerObj, u2, 1, 0 )
##            #Game.AddInventoryItemToPlayer( self._game, playerObj, c1, 1, 0 )
##            #Game.AddInventoryItemToPlayer( self._game, playerObj, c2, 1, 0 )
##            #Game.AddInventoryItemToPlayer( self._game, playerObj, c3, 1, 0 )
##            #Game.AddInventoryItemToPlayer( self._game, playerObj, c4, 1, 0 )
##            #Game.AddInventoryItemToPlayer( self._game, playerObj, c5, 1, 0 )
##
##            ###This never worked properly.  Removed REK.  Also the DatabaseAccess
##            ###is not used anymore.  All databse access should be moved to the
##            ###companion QuestItemAwardDb.py script
##            #DatabaseAccess.addToInvPendingAdds( playerObj, r1 )
##            #DatabaseAccess.addToInvPendingAdds( playerObj, u1 )
##            #DatabaseAccess.addToInvPendingAdds( playerObj, u2 )
##            #DatabaseAccess.addToInvPendingAdds( playerObj, c1 )
##            #DatabaseAccess.addToInvPendingAdds( playerObj, c2 )
##            #DatabaseAccess.addToInvPendingAdds( playerObj, c3 )
##            #DatabaseAccess.addToInvPendingAdds( playerObj, c4 )
##            #DatabaseAccess.addToInvPendingAdds( playerObj, c5 )
##            
##            msg = "Opening this Harvest Pack has revealed the following trade items:\n\n"
##            msg += "Rare: GLID" + str( r1 ) + "\n"
##            msg += "Uncommon: GLID" + str( u1 ) + "\n"
##            msg += "Uncommon: GLID" + str( u2 ) + "\n"
##            msg += "Common: GLID" + str( c1 ) + "\n"
##            msg += "Common: GLID" + str( c2 ) + "\n"
##            msg += "Common: GLID" + str( c3 ) + "\n"
##            msg += "Common: GLID" + str( c4 ) + "\n"
##            msg += "Common: GLID" + str( c5 )
##            
##            QT_EMBED_GLIDS = 2  ## from QuestTrade.lua
##            MenuLauncher.questTradeMenu( playerNetId, QT_EMBED_GLIDS, msg, "" )

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""

    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler, QuestItemAwarder.handlePlayerUseEvent, 1,0,0,0, "PlayerUseItem", KEP.USE_TYPE_QUEST, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.HIGH_PRIO )
    RESIDENCE_MASK = 0x30000000;
    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler, QuestItemAwarder.handlePlayerSpawnedEvent, 1,0,0,0, "PlayerSpawnedEvent", RESIDENCE_MASK, KEP.FILTER_IS_MASK, KEP.NO_OBJID_FILTER, KEP.MED_PRIO )
    return True

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function
    called from C to give us a chance to cleanup before shutdown or re-init"""
    return KEP.getDependentModules(__name__)

        
