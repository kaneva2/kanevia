#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import GameGlobals
import KEP
import MenuLauncher

# C functions
import Dispatcher
import Event

#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------        
class QuestItemTrader( object ):
    
    def __init__( self, gameInstance, playerNetId ): 
        self._game = gameInstance
        self._playerNetId = playerNetId
    
    #------------------------------------------------------------------------------------        
    def checkGiveList( self, giveItemList ):
        import Game
        
        ### expected giveList and takeList format:
        ### ( ( qty, GLID ), ( qty, GLID ), ( qty, GLID ), ... )
        errorList = None
        for pair in giveItemList:
            qty = pair[ 0 ]
            glid = pair[ 1 ]
            playerQty = Game.GetInventoryItemQty( self._game, self._playerNetId, glid )
            dif = playerQty - qty
            if dif < 0:
                if errorList == None:
                    errorList = list()
                errorList.append( ( dif, glid ) )
        return errorList
    
    #------------------------------------------------------------------------------------
    def reportMissingGives( self, errorList ):
        msg = "You are missing the following items:\n"
        listMsg = ""
        for pair in errorList:
            qty = pair[ 0 ]
            glid = pair[ 1 ]
            if qty < 0:
                listMsg += str( glid ) + str( int( qty ) ) + ","
        QT_ITEM_LIST = 1  ## from QuestTrade.lua
        MenuLauncher.questTradeMenu( self._playerNetId, QT_ITEM_LIST, msg, listMsg )
        
    #------------------------------------------------------------------------------------    
    def reportQuestExchange( self, giveItemList, takeItemList ):
        msg = "You have made the following exchange:\n"
        listMsg = ""
        for pair in giveItemList:
            qty = pair[ 0 ]
            glid = pair[ 1 ]
            if qty > 0:
                listMsg += str( glid ) + "-" + str( qty ) + ","
        for pair in takeItemList:
            qty = pair[ 0 ]
            glid = pair[ 1 ]
            if qty > 0:
                listMsg += str( glid ) + "+" + str( qty ) + ","
        if listMsg == "":
            msg = "You have nothing to exchange, at this time.\n\n"
            msg += "  - If you have 10 of the same kind of common food item: I'll trade you one new Harvest Pack.\n"
            msg += "  - If you have 5 of the same kind of uncommon food item: I'll trade you one new Harvest Pack.\n\n"
            msg += "Note:\nI'll never take the last food item of any type from you. In order to trade, you'll need at least 11 common food items (10 to trade, 1 to keep) and 6 uncommon food items (5 to trade and 1 to keep).\n"
            msg += "The items in your inventory automatically determine this trade.\nFor instance, if you have 11 Harvest Carrot, and 11 Harvest Corn, I will take 10 of each and give your 2 Harvest Packs in return."
        QT_ITEM_LIST = 1  ## from QuestTrade.lua
        MenuLauncher.questTradeMenu( self._playerNetId, QT_ITEM_LIST, msg, listMsg )
    
    #------------------------------------------------------------------------------------    
    def doExchange( self, giveItemList, takeItemList ):
        import Game
        errorList = self.checkGiveList( giveItemList )
        if errorList != None:
            self.reportMissingGives( errorList )
        else:
            playerObj = Game.GetPlayerByNetId( self._game, self._playerNetId )
            if playerObj:
                for tuple in giveItemList:
                    qty = tuple[ 0 ]
                    glid = tuple[ 1 ]
                    if qty > 0:
                        Game.RemoveInventoryItemFromPlayer( self._game, playerObj, glid, qty, 1, GameGlobals.IT_NORMAL )
                for tuple in takeItemList:
                    qty = tuple[ 0 ]
                    glid = tuple[ 1 ]
                    type = GameGlobals.IT_NORMAL
                    if len( tuple ) == 3:
                        type = tuple[ 2 ]
                    if qty > 0:
                        #Game.AddInventoryItemToPlayer( self._game, playerObj, glid, qty, 0 )
                        #DatabaseAccess.addToInvPendingAdds( playerObj, glid, qty, type )
                        pass
                self.reportQuestExchange( giveItemList, takeItemList )
        
#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""

    return True

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function
    called from C to give us a chance to cleanup before shutdown or re-init"""
    return KEP.getDependentModules(__name__)
