#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import DatabaseInterface

gDBAccessor = None

#-------------------------------------------------------------------
def getQuestDate( prog_id, date ):
    """get quest date.  stage is either the start or the end or any other
        date column that might be used to track a quest """
    global gDBAccessor

    result = []

    timeStamp = ""

    sql = "SELECT @date FROM wok.quest_progress_condition WHERE prog_cond_id = @prog_id"
    sql = sql.replace( "@date", date )
    sql = sql.replace( "@prog_id", prog_id )
    
    gDBAccessor.fetchRows( sql, result )

    if( len(result) > 0 ):
        timeStamp = result[0][0]

    return timeStamp      

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    pass

#-------------------------------------------------------------------
def InitializeKEPEventHandlers(dispatcher, handler, dbgLevel):
    """Called from C to initialize all the handlers in this script"""
    global gDBAccessor

    gDBAccessor = DatabaseInterface.DBAccess("aaSqlUtil")
