#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import MySQLdb
from DBUtils.PersistentDB import PersistentDB
import time

import ServerGlobals
import KEP
import WorkerThread
import ParentHandler
import Dispatcher
import Event

CONNECTION_TIMEOUT = 2  #seconds
MAX_USAGE = 500 #number of times a connection is used before being recycled
SLOWCOUNT_LIMIT = 15

gDispatcher =  0
gParent = 0

#-------------------------------------------------------------------
# Timing function
#-------------------------------------------------------------------
## When timer code should output message (ms)
TIMER_THRESHOLD = 1000

def print_timing(func):
    def wrapper(*arg):
        t1 = time.clock()
        res = func(*arg)
        t2 = time.clock()
        delta = (t2-t1)*1000.0
        if delta > TIMER_THRESHOLD:
            ParentHandler.LogMessage( gParent, KEP.INFO_LOG_LEVEL, "Timing DB: "+str(func.func_name) + " " + str(int(delta)))
            ParentHandler.LogMessage( gParent, KEP.INFO_LOG_LEVEL, "Timing DB: SQL: "+str(arg[1]) )     
        return res
    return wrapper


#-------------------------------------------------------------------
# Class DBAccess
#-------------------------------------------------------------------
class DBAccess(object):
    """Interface to execute sql on database"""

    #-------------------------------------------------------------------
    def __init__(self, name, db_name = ServerGlobals.DB_Name, db_host = ServerGlobals.DB_Server, db_port = ServerGlobals.DB_Port, db_user = ServerGlobals.DB_User, db_passwd = ServerGlobals.DB_Password, **kwds):
        """Worker Constructor"""
        global MAX_USAGE

        self.name = name
        if db_name == ServerGlobals.UNINIT_DB_NAME: # default param is empty if ServerGlobals loade
            self.dbName = ServerGlobals.DB_Name
            self.dbHost = ServerGlobals.DB_Server
            self.dbPort = ServerGlobals.DB_Port
            self.dbUser = ServerGlobals.DB_User
            self.dbPasswd = ServerGlobals.DB_Password
        else:
            self.dbName = db_name
            self.dbHost = db_host
            self.dbPort = db_port
            self.dbUser = db_user
            self.dbPasswd = db_passwd
        #print ">>>>>>> " + name + " ServerGlobals.DB_Name is " + ServerGlobals.DB_Name
        #print ">>>>>>> " + name + " db_name is " + db_name
        #print ">>>>>>> " + name + " self.dbName is " + self.dbName
        self.persistConn = PersistentDB( self.connectToDb, MAX_USAGE ) #Thread safe at the connection level
        self.slowCount = 0                                                                                  #Reset connection after MAX_USAGE uses

    #-------------------------------------------------------------------
    def connectToDb(self):
        "connect to database"

        conn = None

        try:
            conn = MySQLdb.connect(host = self.dbHost, port = self.dbPort, user = self.dbUser, passwd = self.dbPasswd, db = self.dbName)
        except MySQLdb.Error, e:       
            ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "DatabaseInterface connection error %d: %s" % (e.args[0], e.args[1]) )
        except Exception, ee:       
            ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "connectToDb error " + str(ee) )

        return conn
        
    #-------------------------------------------------------------------
    @print_timing
    def fetchRows(self, sql, rowList):
        "common fn to get rows from db can add retry logic here"
        global gDispatcher
        
        ParentHandler.LogMessage( gParent, KEP.TRACE_LOG_LEVEL, "fetching rows" )

        timer1 = time.clock()
        
        #No sql just leave
        if( sql == None ):
            return

        ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, str(sql)+" - "+str(rowList)+" - "+str(self.dbName) )

        conn = self.persistConn.connection()
        
        cursor = conn.cursor()

        try:
            cursor.execute( sql )

            #No return values wanted.  Close cursor and leave.
            if( rowList == None ):
                cursor.close()
                conn.commit() #Must be done.  connection.py has autocommit turned off when creating a connection object
                conn.close()  #close silently ignored by PersistentDB class
                return   

            while ( 1 ):
                row = cursor.fetchone()

                if row == None:
                    if not cursor.nextset(): # add support for > 1 result set, just concat them all
                        break
                else:
                    rowList.append( row )

        except MySQLdb.Error, e:       
            ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "fetchRows error %d: %s" % (e.args[0], e.args[1]) )
        except Exception, ee:       
            ParentHandler.LogMessage( gParent, KEP.ERROR_LOG_LEVEL, "fetchRows error " + str(ee) )
            
        cursor.close()

        conn.commit() #Must be done.  connection.py has autocommit turned off when creating a connection object

        conn.close()  #close silently ignored by PersistentDB class

        ParentHandler.LogMessage( gParent, KEP.DEBUG_LOG_LEVEL, "Done with: "+str(sql)+"\n" )

        timer2 = time.clock()
        delta = (timer2-timer1)*1000.0
        if delta > TIMER_THRESHOLD:
            self.slowCount = self.slowCount + 1
            if self.slowCount >= SLOWCOUNT_LIMIT:
                self.slowCount = 0
                #Send event out with thread id.  The owning thread will decide what to do.
                ParentHandler.LogMessage( gParent, KEP.WARN_LOG_LEVEL, "slowCount exceeded "+str(SLOWCOUNT_LIMIT)+" DatabaseInterface sending DatabaseDistressEvent on thread: "+str(Dispatcher.GetCurrentThreadId()))
                e = Dispatcher.MakeEvent( gDispatcher, "DatabaseDistressEvent" )
                if e != 0:
                    Event.SetFilter(e, Dispatcher.GetCurrentThreadId())
                    Dispatcher.QueueEvent( gDispatcher, e )


            
#-------------------------------------------------------------------
##def randomWait():
##    """Stall out db call for testing"""
##    import random
##    import time
##
##    r = random.Random()
##    if r.randrange(1,10) == 1:
##        time.sleep(r.randrange(1,3))
    

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    global gParent
    global gDispatcher

    # grab instances for debugging
    gParent = parent
    gDispatcher = dispatcher

    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "DatabaseDistressEvent", KEP.MED_PRIO )       
 


