#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP

# C functions
import Dispatcher
import Event
import ParentHandler


def validateAvatarEventHandler( dispatcher, fromNetId, event, eventid, filter, objectid ):
    import Game
    
    Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "validateAvatarEventHandler" )
    
    return KEP.EPR_CONSUMED

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    ParentHandler.RegisterEventHandler( dispatcher, handler, validateAvatarEventHandler, 1,0,0,0, "ValidateAvatarEvent", KEP.MED_PRIO ) 
    return True

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "ValidateAvatarEvent", KEP.MED_PRIO )
    pass

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function
    called from C to give us a chance to cleanup before shutdown or re-init"""
    return KEP.getDependentModules(__name__)