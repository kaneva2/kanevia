#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

"""This file contains assorted general utility functions used by other
modules in the PyAIML package.

"""
import Dispatcher
import KEP

import math

def sentences(s):
    """Split the string s into a list of sentences."""
    try: s+""
    except: raise TypeError, "s must be a string"
    pos = 0
    sentenceList = []
    l = len(s)
    while pos < l:
        try: p = s.index('.', pos)
        except: p = l+1
        try: q = s.index('?', pos)
        except: q = l+1
        try: e = s.index('!', pos)
        except: e = l+1
        end = min(p,q,e)
        sentenceList.append( s[pos:end].strip() )
        pos = end+1
    # If no sentences were found, return a one-item list containing
    # the entire input string.
    if len(sentenceList) == 0: sentenceList.append(s)
    return sentenceList

# Self test
if __name__ == "__main__":
    # sentences
    sents = sentences("First.  Second, still?  Third and Final!  Well, not really")
    assert(len(sents) == 4)
    
    
def distance(p1, p2):
    """expects tuples as points"""
    xDif = p1[0] - p2[0]
    yDif = p1[1] - p2[1]
    dist = math.sqrt( xDif * xDif + yDif * yDif )
    Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "Utils.distance => " + str(dist))
    return dist