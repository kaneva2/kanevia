#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#python modules
import time, inspect
import pdb

#import helper Python code
import KEP
import CMD

#get set ids
import PlayerIds
import ChatType

#import the C methods from the exe
import ParentHandler
import Dispatcher
import Event
import GetSet
import InstanceId
import GameGlobals

gDispatcher = 0
gParent = 0

def HandlePlacePlayerMessages( dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game
    
    #decode the parameters
    place = Event.DecodeNumber(event)           # zoneIndex Sans instanceId
    instanceId = Event.DecodeNumber(event)

    Dispatcher.LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, ">>>> got "+hex(int(place))+","+str(int(instanceId)) )
    
    zoneIndex = InstanceId.SetInstanceFrom( place, instanceId )
    type = InstanceId.GetType( zoneIndex )
    zoneId = InstanceId.GetId( zoneIndex )
    if type == KEP.CI_CHANNEL and zoneId == 0xfff:
        Dispatcher.LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, ">>>> setting default zone" )
        zoneIndex = InstanceId.MakeId( GameGlobals.DEFAULT_CHANNEL_ZONE, instanceId, KEP.CI_CHANNEL )

    #get the game object
    game = Dispatcher.GetGame( dispatcher )

    #get the player object
    player = Game.GetPlayerByNetId( game, fromNetId )
    isgm    = GetSet.GetNumber( player, PlayerIds.GM )
    
    ok, x, y, z, rotation = Game.CanSpawnToZone( game, player, zoneIndex )

    if ok == 1.0 or isgm == 1:
    
        id = GetSet.AddNewStringMember( player, "LastVisited","", KEP.amfSaveAcrossSpawn );
        
        #only expose the zones you want people to be able to pop to defined in GameGlobals.py
        if place == GameGlobals.ZONE_THEATRE:
            GetSet.SetString( player, id, "Theater")
        elif place == GameGlobals.ZONE_PENTHOUSE:
            GetSet.SetString( player, id, "Penthouse")
        elif place == GameGlobals.ZONE_NIGHTCLUB:
            GetSet.SetString( player, id, "Nightclub")
        elif place == GameGlobals.ZONE_MALL:
            GetSet.SetString( player, id, "Mall")
        elif place == GameGlobals.ZONE_CAFE_LARGE:
            GetSet.SetString( player, id, "KanevaCafe")
            
        #isgm    = GetSet.GetNumber( player, PlayerIds.GM )
        Dispatcher.LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "After gm lookup" )
        #isadmin = GetSet.GetNumber( player, PlayerIds.ADMINISTRATORACCESS)
        #Dispatcher.LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "Have both" )
        #Dispatcher.LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, tostring(isgm))
        
        #isgm = 1.0
        #isadmin = 0.0

        #if isgm == 1.0 or isadmin == 1.0:
        if place == GameGlobals.ZONE_CASINO: 
            GetSet.SetString( player, id, "Casino")
        elif place == GameGlobals.ZONE_BBTHEATER:
            GetSet.SetString( player, id, "KanevaBBTheater")
        elif place == GameGlobals.ZONE_BBTHEATER:
            GetSet.SetString( player, id, "SmallTheater")
        elif place == GameGlobals.ZONE_DANCEBAR:
            GetSet.SetString( player, id, "Dancebar")
        elif place == GameGlobals.ZONE_CONFERENCE:
            GetSet.SetString( player, id, "Conference")
        
        Dispatcher.LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "CoolPlacesHandler spawning player to " + hex(int(zoneIndex)) + " " + str(x) + "," + str(y) + "," + str(z) )
        Game.SpawnToPoint( Dispatcher.GetGame( dispatcher ), player, x, y, z, zoneIndex, rotation )
    else:
        name = GetSet.GetString( player, PlayerIds.CHARNAME )
        Dispatcher.LogMessage( dispatcher, KEP.ERROR_LOG_LEVEL, "Permissions prevent spawn " + name + " to " + hex(int(zoneIndex)) )
        
    
    return KEP.EPR_CONSUMED

#-------------------------------------------------------------------
#
# Called from C to initialize all the handlers in this script
#
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    
    global gParent
    global gDispatcher

    gParent = handler
    gDispatcher = dispatcher

    # get the game API 
    try:
        callable( Game.GetPlayerByNetId )
    except NameError:
        if not Dispatcher.GetFunctions( gDispatcher, KEP.GET_SERVER_FN ):
            Dispatcher.LogMessage( dispatcher, KEP.WARN_LOG_LEVEL,  "Python CoolPlacesHandler couldn't load server Game functions!!" )
            return False
        import Game

        # one more check        
        try:
            callable(Game.GetPlayerByNetId)
        except NameError:
            return False

    # now this is handled in C++ code, do nothing now
    # ParentHandler.RegisterEventHandler( dispatcher, handler, HandlePlacePlayerMessages, 1,0,0,0, KEP.GOTOPLACE_EVENT_NAME, KEP.MED_PRIO )
    
    return True


#-------------------------------------------------------------------
#
# Called from C to initialize all the events
#
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher

    #import gc
    #gc.set_debug( gc.DEBUG_LEAK )

    # call the c funtion to register our events
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    # now this is handled in C++ code, do nothing now
    # ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, KEP.GOTOPLACE_EVENT_NAME, KEP.MED_PRIO )

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give s a chance to cleanup before shutdown or re-init"
    
    ret = KEP.getDependentModules(__name__)

    return ret
