#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import GameGlobals
import KEP
import urllib2
import ParentHandler
import WorkerThread
import PlayerIds
import WorldFameAwardDb
import FameSoap
import aaSqlUtil

import ServerGlobals

# C functions
import Dispatcher
import Event
import GetSet


from xml.dom.minidom import parseString


gBackgroundThread = None

#-------------------------------------------------------------------
class DanceLevelRewards(object):
    ###class for keeping track of dance fame rewards
    DanceFameLevels = dict()
    
    def __init__(self, lvl, mrewards, mitem, mtitle, frewards, fitem, ftitle):
        """consturctor"""
        
        self.level = lvl
        self.maleRewards = mrewards
        self.maleItem = mitem
        self.maleTitle = mtitle
        self.femaleRewards = frewards
        self.femaleItem = fitem
        self.femaleTitle = ftitle

DANCE_FAME_GET_LEVEL_INFO_EVENT = "DanceFameGetLevelInfoEvent"
FAME_CHECKLIST_INFO_EVENT = "GetFameChecklistInfoEvent"
REDEEM_FAME_PACKET_EVENT = "RedeemFamePacketEvent"

#------------------------------------------------------------------------------------
#Methods for redeeming world fame packets
#------------------------------------------------------------------------------------        
def threadRedeemWorldFamePacket(playerID, fame_type, packet_id):

    userId = aaSqlUtil.sqlGetKanevaUserIdFromPlayerId(playerID)
    
    url = ServerGlobals.BASE_URL + ":8086/redeemPacket.aspx?userId=@userid&packetId=@packetid&fameTypeId=@fametypeid"   
  
    if(userId != -1):
       url = url.replace("@userid", str(userId))
       url = url.replace("@packetid", str(packet_id))
       url = url.replace("@fametypeid", str(fame_type))
       Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, url)
    
       return (urllib2.urlopen(url)).read()    

#-------------------------------------------------------------------
def redeemWorldFamePacket(NetID, fame_type, packet_id):
    ##import ServerGlobals
    global gBackgroundThread

    import Game

    playerObj = Game.GetPlayerByNetId( Dispatcher.GetGame( KEP.dispatcher ), NetID )
    
    if playerObj:
        playerID = GetSet.GetNumber( playerObj, PlayerIds.HANDLE )
    else:
        Dispatcher.LogMessage( KEP.dispatcher, KEP.ERROR_LOG_LEVEL, "WorldFameAwarder. threadRedeemWorldFamePacket failed.  Null player object." )
        return    

    act = gBackgroundThread.performWork(threadRedeemWorldFamePacket, playerID, fame_type, packet_id)
    doc = parseString(act.getCompletionResults())

    return int(doc.getElementsByTagName( "ReturnCode" ).item(0).childNodes.item(0).nodeValue) 

#-------------------------------------------------------------------
def threadRedeemWorldFamePacketFromName(playerName, fame_type, packet_id):

    url = ServerGlobals.BASE_URL + ":8086/redeemPacket.aspx?userId=@userid&packetId=@packetid&fameTypeId=@fametypeid"
    userId = aaSqlUtil.sqlGetKanevaUserIdFromName(playerName)
    if(userId != -1):
       url = url.replace("@userid", str(userId))
       url = url.replace("@packetid", str(packet_id))
       url = url.replace("@fametypeid", str(fame_type))
       Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, url)
    
       return (urllib2.urlopen(url)).read()
   
#-------------------------------------------------------------------    
def redeemWorldFamePacketFromName(playerName, fame_type, packet_id):
    ##import ServerGlobals
    global gBackgroundThread
    
    act = gBackgroundThread.performWork(threadRedeemWorldFamePacketFromName, playerName, fame_type, packet_id)
    return act.getCompletionResults()

#-------------------------------------------------------------------
def threadFameCheckListFromNetId(fromNetId, playerID, checklistId):
    
     userId = aaSqlUtil.sqlGetKanevaUserIdFromPlayerId(playerID)
     FameSoap.sendChecklist(fromNetId, int(userId), int(checklistId))

#-------------------------------------------------------------------
def getFameChecklistInfoHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
    global gBackgroundThread

    import Game
    
    checklistId = Event.DecodeNumber(event)

    game = Dispatcher.GetGame(KEP.dispatcher)

    playerObj = Game.GetPlayerByNetId(game, fromNetId)

    if playerObj:
        playerID = GetSet.GetNumber( playerObj, PlayerIds.HANDLE )
    else:
        Dispatcher.LogMessage( KEP.dispatcher, KEP.ERROR_LOG_LEVEL, "WorldFameAwarder. threadRedeemWorldFamePacket failed.  Null player object." )
        return    

    gBackgroundThread.performWork(threadFameCheckListFromNetId, fromNetId, playerID, checklistId)
    
    return KEP.EPR_CONSUMED

#-------------------------------------------------------------------
def redeemPacketHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
    fametype = Event.DecodeNumber(event)
    packetId = Event.DecodeNumber(event)
    Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "Rewarding Fame Packet: " + str(packetId) + " of fame type " + str(fametype))
    redeemWorldFamePacket(int(fromNetId), int(fametype), int(packetId))
    
    return KEP.EPR_CONSUMED

#-------------------------------------------------------------------
def getDanceFameLevelInfoHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
    import Game
    import GetSet
    import PlayerIds
    import SkillIds
    level = int(Event.DecodeNumber(event))
    if(level in DanceLevelRewards.DanceFameLevels):
        ev = Dispatcher.MakeEvent(KEP.dispatcher, DANCE_FAME_GET_LEVEL_INFO_EVENT)
        Event.EncodeNumber(ev, DanceLevelRewards.DanceFameLevels[level].level)
        Event.EncodeNumber(ev, DanceLevelRewards.DanceFameLevels[level].maleRewards)
        Event.EncodeString(ev, DanceLevelRewards.DanceFameLevels[level].maleItem)
        Event.EncodeString(ev, DanceLevelRewards.DanceFameLevels[level].maleTitle)
        Event.EncodeNumber(ev, DanceLevelRewards.DanceFameLevels[level].femaleRewards)
        Event.EncodeString(ev, DanceLevelRewards.DanceFameLevels[level].femaleItem)
        Event.EncodeString(ev, DanceLevelRewards.DanceFameLevels[level].femaleTitle)
        Event.AddTo(ev, fromNetId)
        Dispatcher.QueueEvent(KEP.dispatcher, ev)
        #Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "Dance Fame Info-- Level " + str(level) + " earns " + str(DanceLevelRewards.DanceFameLevels[level].maleRewards) + " rewards")
    
    return KEP.EPR_CONSUMED

#-------------------------------------------------------------------
def getAllDanceFameInfoFromDB():
    #import DatabaseAccess
    MAX_DANCE_FAME_LEVEL = 50
    for i in range(1, MAX_DANCE_FAME_LEVEL + 1):
        DanceLevelRewards.DanceFameLevels[i] = getDanceFameLevelInfo(i)

#-------------------------------------------------------------------
def getDanceFameLevelInfo(level):
    mrewards = 0
    mitem = ""
    frewards = 0
    fitem = ""
    ret = WorldFameAwardDb.getLevelRewardsByLevelId( level + 1)
    if len( ret ) > 0 and len( ret[0] ) > 6:
        mrewards = int((ret[0])[6])
        mitem_glid = int((ret[0])[3])
        frewards = int((ret[0])[9])
        fitem_glid = int((ret[0])[7])
        Dispatcher.LogMessage(KEP.dispatcher, KEP.TRACE_LOG_LEVEL, "Ret: " + str(mrewards) + " - " + str(mitem_glid) + " - " + str(frewards) + " - " + str(fitem_glid))
   
        mitem = aaSqlUtil.sqlGetItemNameFromGlid( mitem_glid )
        fitem = aaSqlUtil.sqlGetItemNameFromGlid( fitem_glid ) 
        if(mitem == "-999"):
            mitem = ""
        if(fitem == "-999"):
            fitem = ""
        Dispatcher.LogMessage(KEP.dispatcher, KEP.TRACE_LOG_LEVEL, "Ret2: " + str(mitem) + " - " + str(fitem))
    
    mtitle_id = WorldFameAwardDb.getTitleIdFromLevel( level, "title_id" )
    ftitle_id = WorldFameAwardDb.getTitleIdFromLevel( level, "alt_title_id" )
    Dispatcher.LogMessage(KEP.dispatcher, KEP.TRACE_LOG_LEVEL, "Ret3: level: " + str(level) + " - " + str(mtitle_id) + " - " + str(ftitle_id))
   
    mtitle = WorldFameAwardDb.getTitleNameFromId( mtitle_id )
    ftitle = WorldFameAwardDb.getTitleNameFromId( ftitle_id )
    if(mtitle == "-999"):
        mtitle = ""
    if(ftitle == "-999"):
        ftitle = ""
    Dispatcher.LogMessage(KEP.dispatcher, KEP.TRACE_LOG_LEVEL, "Ret4: " + str(mtitle) + " - " + str(ftitle))
    return DanceLevelRewards(level, mrewards, mitem, mtitle, frewards, fitem, ftitle)
 
#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function"""
    global gBackgroundThread

    #ParentHandler.LogMessage( parent, KEP.INFO_LOG_LEVEL, "Main thread - tid from c++: "+str(Dispatcher.GetCurrentThreadId()))
    
    ParentHandler.RegisterEventHandler(dispatcher, parent, getDanceFameLevelInfoHandler, 1, 0, 0, 0, DANCE_FAME_GET_LEVEL_INFO_EVENT, KEP.MED_PRIO) 
    ParentHandler.RegisterEventHandler(dispatcher, parent, getFameChecklistInfoHandler, 1, 0, 0, 0, FAME_CHECKLIST_INFO_EVENT, KEP.MED_PRIO) 
    ParentHandler.RegisterEventHandler(dispatcher, parent, redeemPacketHandler, 1, 0, 0, 0, REDEEM_FAME_PACKET_EVENT, KEP.MED_PRIO) 
    
    gBackgroundThread = WorkerThread.WorkerThread("WorldFameAwarder")
    ParentHandler.LogMessage(parent, KEP.INFO_LOG_LEVEL, str(gBackgroundThread) + " tid: "+str(gBackgroundThread._get_my_tid()))

    getAllDanceFameInfoFromDB()


#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "FameEarnedEvent", KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "PlayerRavedEvent", KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, DANCE_FAME_GET_LEVEL_INFO_EVENT, KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, FAME_CHECKLIST_INFO_EVENT, KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, REDEEM_FAME_PACKET_EVENT, KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "ClientTickler", KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "FameLevelUpEvent", KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "FameBadgeAwardedEvent", KEP.MED_PRIO)
    
#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function
    called from C to give us a chance to cleanup before shutdown or re-init"""
    return KEP.getDependentModules(__name__)
