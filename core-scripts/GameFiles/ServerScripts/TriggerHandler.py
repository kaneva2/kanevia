#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#


#import helper Python code
import KEP
import CMD

import pdb

#import the C methods from the exe
import ParentHandler
import Dispatcher
import Event
import GetSet
import InstanceId

POKERTABLE1 = 1010
POKERTABLE2 = 1011

#-------------------------------------------------------------------
#
# Called from C to initialize all the handlers in this script
#
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):

    KEP.parent = handler
    KEP.dispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    try:
        callable( Game.GetPlayerByNetId )
    except NameError:
        Dispatcher.GetFunctions( KEP.dispatcher, KEP.GET_SERVER_FN )
        import Game

    #Removed poker table event resgistration
 
    return True

#-------------------------------------------------------------------
#
# handle a trigger event
#
#-------------------------------------------------------------------
def PokerTableFunction(dispatcher, fromNetid, event, eventid, filter, objectid):

    import Game
    game = Dispatcher.GetGame( dispatcher )
    
    # get the player and info since we usually need it
    player = Game.GetPlayerByNetId( game, fromNetid )
   
    if  player == 0: 
        # TODO: log message
        return KEP.EPR_OK

    # extract the parameters of a trigger event
    #   (*OutBuffer()) << triggerIndex << radius << triggerName << x << y << z;
    if filter == KEP.TriggerEnter: # enter the trigger
        radius = Event.DecodeNumber( event )
        triggerName = Event.DecodeString( event )
        x = Event.DecodeNumber( event )
        y = Event.DecodeNumber( event )
        z = Event.DecodeNumber( event )
        cx = Event.DecodeNumber( event )
        cy = Event.DecodeNumber( event )
        cz = Event.DecodeNumber( event )

        if triggerName == "Trigger_Poker_Table1":
            Game.TellClientToOpenMenu( game, POKERTABLE1, fromNetid, 0, 0)
        if triggerName == "Trigger_Poker_Table2":
            Game.TellClientToOpenMenu( game, POKERTABLE2, fromNetid, 0, 0)

        Game.SendTextMessage( game, player, "WOW! Look a poker table.", 0 )

    if filter == KEP.TriggerExit: # exit the trigger
        radius = Event.DecodeNumber( event )
        triggerName = Event.DecodeString( event )

        if triggerName == "Trigger_Poker_Table1":
            Game.TellClientToOpenMenu( game, (POKERTABLE1 * -1), fromNetid, 0, 0)
        if triggerName == "Trigger_Poker_Table2":
            Game.TellClientToOpenMenu( game, (POKERTABLE2 * -1), fromNetid, 0, 0)
        pass
    
    return KEP.EPR_CONSUMED
    
#-------------------------------------------------------------------
#
# Called from C to initialize all the events
#
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):

    KEP.dispatcher = dispatcher
    
