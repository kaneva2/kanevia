#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#global setting for WoK areanas
BLUE_TEAMID      = 1
RED_TEAMID       = 2
BLUE_NAME        = "Blue Team"
RED_NAME         = "Red Team"

#gender
MALE_EDB         = (13, 14, 15)  #Jimmy, Hondo, Meaty
FEMALE_EDB       = (10, 11, 12)  #Isabella, Sofia, Eva

#auto armed items for arenas
MALE_RED_GEM     = 102
MALE_BLUE_GEM    = 101
MALE_RED_FLAG    = 104
MALE_BLUE_FLAG   = 103
FEMALE_RED_GEM   = 106
FEMALE_BLUE_GEM  = 105
FEMALE_RED_FLAG  = 108
FEMALE_BLUE_FLAG = 107

# remember, you can only carry other teams flag, so that's why gem and flag are opposite
# either tribe can be red or blue
RED_TEAM_EQUIPMENT = { \
                        MALE_EDB : ( MALE_RED_GEM, MALE_BLUE_FLAG ), \
                        FEMALE_EDB : ( FEMALE_RED_GEM, FEMALE_BLUE_FLAG ) \
                     }

# either tribe can be red or blue
BLUE_TEAM_EQUIPMENT = { \
                        MALE_EDB : ( MALE_BLUE_GEM, MALE_RED_FLAG ), \
                        FEMALE_EDB : ( FEMALE_BLUE_GEM, FEMALE_RED_FLAG ) \
                      }

#------------------------------------------------------------------------------------       
def GetGenderFromEDBIndex( edbIndex ):
    if edbIndex in MALE_EDB:
        return MALE_EDB
    if edbIndex in FEMALE_EDB:
        return FEMALE_EDB
    return -1

#------------------------------------------------------------------------------------       
def getTeamNameFromIndex( index ):
    if index == BLUE_TEAMID:
        return BLUE_NAME
    elif index == RED_TEAMID:
        return RED_NAME
    else:
        return ""

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):
    return  True 


#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    pass

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give us a chance to cleanup before shutdown or re-init"
    pass
