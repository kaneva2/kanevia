#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import ArenaBase
import ChatType
import Dispatcher
import GameGlobals
import GetSet
import Event
import InstanceId
import KEP
import ParentHandler
import PlayerIds
import PowerUpClass
import ServerItemIds
import TeamedArena
import WoK
import ArenaEquipment
import SoundGlobals
import StringTables
import Utils
import WorldFameAwarder
import FameIds
import random
import WorkerThread
import KachingDb

gParent = 0
gBackgroundThread = None

BATTLE_ARENA1   = 39    #40 in editor
OFFICE_ARENA    = 40    #41 in editor
BATTLE_ARENA2   = 42    #43 in editor

NUM_START_COINS = 7
MIN_DROP_COINS = 3
NUM_START_POWS = 3

WRECKAGE_COINS = [[-13, 10, 75], [13, 10, 75],   [75, 10, -13],  [75, 10, 13],
                  [13, 10, -75], [-13, 10, -75], [-75, 10, -13], [-75, 10, 13],
                  [0, 1, -60],   [-60, 1, 0],    [60, 1, 0],     [-20, 2, -20],
                  [-20, 2, 20],  [20, 2, 20],    [20, 20, -20],  [30, 20, 0],
                  [0, 20, 30],   [-20, 20, 20],  [-20, 20, -20],
                  [12, 5, -15], [12, 10, -15],  [12, 15, -15],  [12, 20, -15]]

WRECKAGE_POWERUPS = [[-45, 21, -45], [-45, 21, 45], [45, 21, -45], [0, 21, -30], [-30, 21, 0], [20, 21, 20], [0, 1, 0], [0, 20, 60]]
#WRECKAGE_SHIELD = [[0, 1, 0], [0, 20, 60]]

OFFICE_COINS = [[ -12, 2, -10 ], [ -12, 2, 15 ], [  22, 2, 23 ], [  31, 2, -8 ],
                [ -45, 2, -6 ], [ -45, 2, 15 ], [ -57, 2, -6 ], [ -57, 2, 14 ],
                [ -68, 2, -5 ], [ -68, 2, 15 ], [ -87, 3, -5 ], [ -83, 7, -18 ],
                [ -85, 8, -33 ], [ -98, 8, -41 ], [ -63, 8, -41 ], [ -24, 2, -19 ],
                [ -32, 3, -25 ], [ -40, 5, -41 ], [  31, 3, -42 ], [  53, 6, -42 ],
                [  71, 8, -42 ], [ 104, 8, -41 ], [  91, 6, -14 ],
                [  89, 8, 34 ], [ 105, 8, 51 ], [  71, 8, 52 ],
                [  49, 6, 44 ], [  34, 2, 31 ], [  40, 4, 53 ],
                [  19, 2, 50 ], [ -15, 2, 50 ], [ -30, 4, 50 ],
                [ -58, 7, 50 ], [ -98, 8, 50 ], [ -86, 8, 35 ],
                [ -85, 5, 21 ], [  54, 2, 14 ], [  54, 2, -4 ], [  65, 2, 16 ],
                [  65, 2, -4 ], [  76, 6, 16 ], [  75, 2, -6 ]]

OFFICE_POWERUPS = [[-17, 2, 32], [29, 2, -22], [ -85, 8, -52], [ 91, 8, -55 ], [ 90, 8, 65 ], [ -85, 8, 65]]

BATTLE2_COINS = [[ -128, 5, 55 ], ## Table Tops
                 [ -106, 5, 56 ], 
                 [ -73, 5, 71 ],
                 [ -63, 5, 34 ], 
                 [ -23, 5, 33 ], 
                 [ -131, 5, -54 ], 
                 [ -107, 5, -55 ], 
                 [ -84, 5, -59 ], 
                 [ -61, 5, -31 ], 
                 [ -26, 5, -29 ], 
                 [ 22, 5, -37 ], 
                 [ 62, 5, -32 ], 
                 [ 76, 5, -72 ], 
                 [ 102, 5, -56 ], 
                 [ 129, 5, -58 ], 
                 [ 131, 5, 52 ], 
                 [ 107, 5, 54 ], 
                 [ 84, 5, 57 ], 
                 [ 53, 5, 36 ], 
                 [ 22, 5, 38 ],
                 ## Offices
                 [ -37, 2, 71 ],
                 [ 11, 2, 72 ],
                 [ 34, 2, 83 ],
                 [ 8, 19, 73 ],
                 [ -20, 19, 60 ],
                 [ -34, 2, -83 ],
                 [ 11, 2, -70 ],
                 [ 35, 2, -71 ],
                 [ -15, 19, -53 ],
                 [ 18, 19, -53 ]]

BATTLE2_POWERUPS = [[-51,3,94], [57,3,-97], [-14,3,-92], [-11, 3, 72], [-11, 3, -70], [0, 19, -82],
                    [-79, 14, 0], [27, 3, 0], [-27, 3, 0]]

#-------------------------------------------------------------------
#-------------------------------------------------------------------
class FoamGunArena(TeamedArena.TeamedArena):
    UPDATE_PLAYER_LISTS_EVENT = "UpdatePlayerListsEvent"
    GAME_STATS_EVENT = "GameStatsEvent"
    EVT_COINS_UPDATE = "CoinsUpdatedEvent"
    TEAM_COINS_UPDATED = "TeamCoinsUpdatedEvent"
    COIN_DROPOFF_EVENT = "CoinDropOffEvent"
    # class variables
    instances = dict()      # needed for dispatching events
    
    #-------------------------------------------------------------------
    #-------------------------------------------------------------------
    @staticmethod
    def SignupSettings():
        """ return type, and if AI balanced """
        return (ArenaBase.TEAMED_ARENA, True)
    
    
    #-------------------------------------------------------------------
    @staticmethod
    def collisionHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
        """ callback when collided with someone else """
        
        firstCharacterNetAssignedId = Event.DecodeNumber(event)
        secondCharacterNetAssignedId = Event.DecodeNumber(event)
    
        ##logMsg( KEP.DEBUG_LOG_LEVEL, "_____Arena: "+str(firstCharacterNetAssignedId)+" ____ "+str(secondCharacterNetAssignedId))
        
        for arenaInst in FoamGunArena.instances:
            if FoamGunArena.instances[arenaInst].processCollision(Dispatcher.GetGame(dispatcher), fromNetId, firstCharacterNetAssignedId, secondCharacterNetAssignedId):
                return KEP.EPR_OK
    
        return KEP.EPR_NOT_PROCESSED
    
    #-------------------------------------------------------------------
    @staticmethod
    def coinDropHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
        firstCharacterNetAssignedId = Event.DecodeNumber(event)
        for arenaInst in FoamGunArena.instances:
            if FoamGunArena.instances[arenaInst].processCoinDrop(Dispatcher.GetGame(dispatcher), fromNetId, firstCharacterNetAssignedId):
                return KEP.EPR_OK
        return KEP.EPR_NOT_PROCESSED
    
    #-------------------------------------------------------------------
    @staticmethod
    def jumpPadHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
        firstCharacterNetAssignedId = Event.DecodeNumber(event)
        for arenaInst in FoamGunArena.instances:
            if FoamGunArena.instances[arenaInst].processJumpPad(Dispatcher.GetGame(dispatcher), fromNetId, firstCharacterNetAssignedId):
                return KEP.EPR_OK
        return KEP.EPR_NOT_PROCESSED

    
    #-------------------------------------------------------------------
    @staticmethod
    def getPointsForCoins(numCoins):
        total = numCoins + FoamGunArena.getBonusPoints(numCoins)
        return total
    
    #-------------------------------------------------------------------
    @staticmethod
    def getBonusPoints(numCoins):
        bonus = (numCoins / MIN_DROP_COINS)
        if bonus < 1:
            bonus = 0
        return bonus
    
    #-------------------------------------------------------------------
    #-------------------------------------------------------------------
    def __init__(self, parentList, game, arenaPtr, channel, instanceID, zi, maxBattleTime, pointsToWin, parent, dispatcher):
        """ constructor """
        super(FoamGunArena, self).__init__("FoamGunArena", parentList, game, arenaPtr, instanceID, zi, maxBattleTime, pointsToWin, parent, dispatcher)
  
        self._originalZone = channel - 1
        self._addTeam(WoK.BLUE_NAME, WoK.BLUE_TEAMID, WoK.BLUE_TEAM_EQUIPMENT, 1)
        self._addTeam(WoK.RED_NAME, WoK.RED_TEAMID, WoK.RED_TEAM_EQUIPMENT, 0)
        self._teams[WoK.BLUE_TEAMID]._spawnGenName = "Blue Side"
        self._teams[WoK.RED_TEAMID]._spawnGenName = "Red Side"
        self._powManager = None

        FoamGunArena.instances[id(self)] = self

    #-------------------------------------------------------------------
    def StartArena(self):        
        super(FoamGunArena, self).StartArena()
            
    #------------------------------------------------------------------------------------       
    def _getLeaderName(self):
        """return the current leader of the arena's name
        
           if you keep track on the fly, set self._winnerName, otherwise
           override this method
        """
        leader = self._getLeader()
        if(leader == 0):
            return "(TIED)"
        return self._teams[leader].name
    
    def _getLeader(self):
        """return the current leader of the arena's name
        
           if you keep track on the fly, set self._winnerName, otherwise
           override this method
        """
        leader = 0
        leaderScore = -1
        tied = True
        for team in self._teams:
            if(self._teams[team].quitters == False):
                if self._teams[team].points > leaderScore:
                    leader = team
                    leaderScore = self._teams[team].points
                    tied = False
                elif self._teams[team].points == leaderScore:
                    tied = True
        if(tied):
            return 0
        
        return leader
    
    #------------------------------------------------------------------------------------       
    def _getLeaderScore(self):
        """return the current leader of the arena's name
        
           if you keep track on the fly, set self._winnerName, otherwise
           override this method
        """
        leaderScore = -1
        for team in self._teams:
            if self._teams[team].points >= leaderScore and self._teams[team].quitters == False:
                leaderScore = self._teams[team].points
        
        return leaderScore              
    #-----------------------------------------------------------------
    def _endArena(self, winner):
        import Game 
        # Called more than once.  I don't know why.  Only process the first time
        if(self._hasArenaEnded == False):
            self._hasArenaEnded = True
            if(self._powManager):
                del self._powManager
            super(FoamGunArena, self)._endArena(winner) 
            if(self.inLobby == False):
                gBackgroundThread.performWork(KachingDb.updateDatabaseForArena, self)
                #arenaThread = ArenaBase.ArenaDataBaseThread(self)
                #arenaThread.start()
            
        
    #-----------------------------------------------------------------
    def _doWeHaveAWinner(self):
        """ Check for higher coin points.  If tied, (which should be extremely rare) check for most kill points """
        ret = False
        if self._teams[WoK.BLUE_TEAMID].points >= self._pointsToWin:
            if self.teamCoinsTied():
                for team in self._teams.values():
                    if team.points >= self._pointsToWin:
                        self._endArena(team.name + " Team")
                        ret = True
            else:
                self._endArena(WoK.BLUE_NAME)
                ret = True
        elif self._teams[WoK.RED_TEAMID].points >= self._pointsToWin:
            self._endArena(WoK.RED_NAME)
            ret = True
        return ret
    
    #------------------------------------------------------------------------------------       
    def _buildEndEvent(self, winner):
        """create the event of the end-arena info

        Makes the event, and calls overrideable _appendStats to 
        let derived classes alter the event
        """
        ARENA_ENDED_FILTER = 3
        e = Dispatcher.MakeEvent(KEP.dispatcher, "ArenaManagerEvent")
        event = self.encodeGameStats(e, ARENA_ENDED_FILTER)
        return event
    
    #------------------------------------------------------------------------------------       
    def ChangeArenaAdmin(self, newAdminKey):
        super(FoamGunArena, self).ChangeArenaAdmin(newAdminKey)
        self.sendPlayerListsUpdate()

     #-----------------------------------------------------------------
    def BootPlayer(self, playerKey):
        super(FoamGunArena, self).BootPlayer(playerKey)
        self.sendPlayerListsUpdate()
        
    #------------------------------------------------------------------------------------    
    def sendPlayerListsUpdate(self):
        import WoK
        import Game
        
        ev = Dispatcher.MakeEvent(KEP.dispatcher, FoamGunArena.UPDATE_PLAYER_LISTS_EVENT)
        Event.EncodeString(ev, self._arenaName)
        Event.EncodeString(ev, self._mapName)
        Event.EncodeString(ev, str(self.arenaAdmin))
        Event.EncodeString(ev, str(self._getArenaAdminName()))
        Event.EncodeNumber(ev, int(self.inLobby))
        Event.EncodeNumber(ev, int(self.locked))
        Event.EncodeNumber(ev, int(self.noWeapons))
        Event.EncodeNumber(ev, self.arenaKey)
        Event.EncodeNumber(ev, self.channelId)
        Event.EncodeNumber(ev, len(self.players))
        Event.EncodeNumber(ev, WoK.BLUE_TEAMID)
        Event.EncodeNumber(ev, WoK.RED_TEAMID)
        
        for player in self.players:
            Event.EncodeNumber(ev, self.players[player].teamId)
            Event.EncodeString(ev, self.players[player].name)
            Event.EncodeString(ev, str(player))
            ready = 0
            if(self.players[player].isReady):
                ready = 1
            Event.EncodeNumber(ev, ready)
            Event.EncodeNumber(ev, self.players[player].famelevel)

        self._broadcastEvent(ev)
        
    def encodePlayerStats(self, ev, filter, playerNetId):
        Event.SetFilter(ev, filter)
        for player in self.players:
            if(ArenaBase.KeyNetIdEqNetId(player, playerNetId)):
                Event.EncodeNumber(ev, self.players[player].teamId)
                Event.EncodeString(ev, self.players[player].name)
                Event.EncodeNumber(ev, self.players[player].kills)
                Event.EncodeNumber(ev, self.players[player].killed)
                Event.EncodeNumber(ev, self.players[player].points)
        return ev
        
    #------------------------------------------------------------------------------------    
    def encodeGameStats(self, ev, filter):
        
        ## To Do: Replace these filter numbers with meaningful variable names
        if(filter == 1 or filter == 3 or filter == 5):
            Event.SetFilter(ev, filter)
            Event.EncodeString(ev, self._arenaName)
            Event.EncodeNumber(ev, WoK.BLUE_TEAMID)
            Event.EncodeNumber(ev, WoK.RED_TEAMID)
            Event.EncodeString(ev, self._getLeaderName())
            
            #Team Stats
            blueTeam = self._teams[WoK.BLUE_TEAMID]
            redTeam = self._teams[WoK.RED_TEAMID]
            
            Event.EncodeNumber(ev, blueTeam.points)
            Event.EncodeNumber(ev, blueTeam.kills)
            Event.EncodeNumber(ev, redTeam.points)
            Event.EncodeNumber(ev, redTeam.kills)
            
            #Player Stats
            Event.EncodeNumber(ev, len(self.players))
            tempSortPlayers = dict()
            for i in range (0, len(self.players)):
                curPlayerMax = 0
                curMax = 0
                for player in self.players:
                    if(self.players[player].points >= curMax and not(player in tempSortPlayers)):
                        curPlayerMax = player
                        curMax = self.players[player].points
                tempSortPlayers[curPlayerMax] = curMax 
                Event.EncodeNumber(ev, self.players[curPlayerMax].teamId)
                Event.EncodeString(ev, self.players[curPlayerMax].name)
                Event.EncodeNumber(ev, self.players[curPlayerMax].kills)
                Event.EncodeNumber(ev, self.players[curPlayerMax].killed)
                Event.EncodeNumber(ev, self.players[curPlayerMax].points)
        elif(filter == 2):
            Event.SetFilter(ev, filter)
            #Team Stats
            blueTeam = self._teams[WoK.BLUE_TEAMID]
            redTeam = self._teams[WoK.RED_TEAMID]
            Event.EncodeNumber(ev, blueTeam.points)
            Event.EncodeNumber(ev, blueTeam.kills)
            Event.EncodeNumber(ev, redTeam.points)
            Event.EncodeNumber(ev, redTeam.kills)
            
        return ev
    
    
    #-----------------------------------------------------------------
    def sendGameStatsToPlayerNetId(self, playerNetId, filter):
        ev = Dispatcher.MakeEvent(KEP.dispatcher, FoamGunArena.GAME_STATS_EVENT)
        if(filter == 5):
            event = self.encodePlayerStats(ev, filter, playerNetId)
            for player in self.players:
                if ArenaBase.KeyNetIdEqNetId( player, playerNetId ):
                    self.updatePlayerData(KEP.dispatcher, player, playerNetId, ArenaBase.ARENA_PLAYER_UPDATED_FILTER)
        else:
            event = self.encodeGameStats(ev, filter)
        self._sendEvent(playerNetId, event)
        
    #-----------------------------------------------------------------
    def teamCoinsTied(self):
        return self._teams[WoK.BLUE_TEAMID].points == self._teams[WoK.RED_TEAMID].points

    #-------------------------------------------------------------------
    def spawnStaticAI(self, channelInstance):
        import Game

        if self._originalZone == OFFICE_ARENA:
            spawnIDWeights = { PowerUpClass.COIN_NPCID : PowerUpClass.RARE }
            self._powManager.createPowerUpGroup( OFFICE_COINS, NUM_START_COINS, spawnIDWeights )
            
            spawnIDWeights = { PowerUpClass.HEALTH_NPCID : PowerUpClass.UNCOMMON,
                               PowerUpClass.AMMO_SG_NPCID : PowerUpClass.UNCOMMON,
                               PowerUpClass.AMMO_BFG_NPCID : PowerUpClass.RARE,
                               PowerUpClass.SHIELD_NPCID : PowerUpClass.RARE }
            self._powManager.createPowerUpGroup( OFFICE_POWERUPS, NUM_START_POWS, spawnIDWeights )
        elif self._originalZone == BATTLE_ARENA1:
            spawnIDWeights = { PowerUpClass.COIN_NPCID : PowerUpClass.RARE }
            self._powManager.createPowerUpGroup( WRECKAGE_COINS, NUM_START_COINS, spawnIDWeights )
            
            spawnIDWeights = { PowerUpClass.HEALTH_NPCID : PowerUpClass.UNCOMMON,
                               PowerUpClass.AMMO_SG_NPCID : PowerUpClass.UNCOMMON,
                               PowerUpClass.AMMO_BFG_NPCID : PowerUpClass.RARE,
                               PowerUpClass.SHIELD_NPCID : PowerUpClass.RARE }
            self._powManager.createPowerUpGroup( WRECKAGE_POWERUPS, NUM_START_POWS, spawnIDWeights )
            
            #spawnIDWeights = { PowerUpClass.AMMO_BFG_NPCID : PowerUpClass.RARE,
            #                   PowerUpClass.SHIELD_NPCID : PowerUpClass.RARE }
            #self._powManager.createPowerUpGroup( WRECKAGE_SHIELD, 1, spawnIDWeights )
        elif self._originalZone == BATTLE_ARENA2:
            spawnIDWeights = { PowerUpClass.COIN_NPCID : PowerUpClass.RARE }
            self._powManager.createPowerUpGroup( BATTLE2_COINS, NUM_START_COINS + 5, spawnIDWeights )
            
            
            spawnIDWeights = { PowerUpClass.HEALTH_NPCID : PowerUpClass.UNCOMMON,
                               PowerUpClass.AMMO_SG_NPCID : PowerUpClass.UNCOMMON,
                               PowerUpClass.AMMO_BFG_NPCID : PowerUpClass.RARE,
                               PowerUpClass.SHIELD_NPCID : PowerUpClass.RARE }
            self._powManager.createPowerUpGroup( BATTLE2_POWERUPS, NUM_START_POWS + 3, spawnIDWeights )
        
    
    #------------------------------------------------------------------------------------       
    def PlayerDisconnected(self, playerKey): 
        super(FoamGunArena, self).PlayerDisconnected(playerKey)
        self.sendPlayerListsUpdate()

    #-----------------------------------------------------------------
    def PlayerSwitchTeams(self, playerNetId):
        super(FoamGunArena, self).PlayerSwitchTeams(playerNetId)
        self.sendPlayerListsUpdate()
    
    #------------------------------------------------------------------------------------       
    def LockArena( self, filter ):
        super(FoamGunArena, self).LockArena(filter)
        self.sendPlayerListsUpdate()
    
    #------------------------------------------------------------------------------------       
    def SetNoWeapons( self, filter ):
        super(FoamGunArena, self).SetNoWeapons(filter)
        self.sendPlayerListsUpdate()
        
    #-----------------------------------------------------------------
    def PlayerReady(self, playerNetId):   
        super(FoamGunArena, self).PlayerReady(playerNetId)
        self.sendPlayerListsUpdate()
    
     #------------------------------------------------------------------------------------       
    def PlayerUpdateAmmo(self, playerNetId, playerKey, maxCharge, curCharge, glid):
        super(FoamGunArena, self).PlayerUpdateAmmo(playerNetId, playerKey, maxCharge, curCharge, glid)
        self.updatePlayerData(self._dispatcher, playerKey, playerNetId, ArenaBase.ARENA_PLAYER_UPDATED_FILTER)
        
    #------------------------------------------------------------------------------------       
    def PlayerSpawned(self, playerNetId, playerNetAssignedId, event, filter):
        import Game
        ret = super(FoamGunArena, self).PlayerSpawned(playerNetId, playerNetAssignedId, event, filter)
        playerObj = Game.GetPlayerByNetTraceId(self._game, playerNetAssignedId, 0, 0)
        # self.singlePlayerSound(SoundGlobals.SOUND_KCVO_LOCKLOAD, ArenaBase.MakePlayerKey(playerNetId, playerNetAssignedId))
        
        
        if self._powManager:
            #do nothing
            return ret;
        else:
            Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "Creating PowerUpManager for Game " + str(self.zoneIndex))
            if playerObj != 0:
                channel = GetSet.GetNumber(playerObj, PlayerIds.CURRENTCHANNEL)
                self._powManager = PowerUpClass.PowerUpManager(self._dispatcher, self._game, channel)
                self.spawnStaticAI(channel)
        return ret    
    #-----------------------------------------------------------------
    def PlayerKilled(self, playerNetId, playerNetAssignedId, killerNetId, killerNetAssignedId, locX, locY, locZ):
        """ Same as TeamDeathMatch, but no team score for player kills
        """
        killerStats = 0
        playerStats = 0
        killerKey = ArenaBase.MakePlayerKey(killerNetId, killerNetAssignedId)
        playerKey = ArenaBase.MakePlayerKey(playerNetId, playerNetAssignedId)
        if self.players.has_key(killerKey):
            killerStats = self.players[killerKey]
        else:
            Dispatcher.LogMessage( self._dispatcher, KEP.DEBUG_LOG_LEVEL,  "WOK_FGA.PlayerKilled Couldn't find killer." )
            return
        if self.players.has_key(playerKey):
            playerStats = self.players[playerKey]
        else:
            Dispatcher.LogMessage( self._dispatcher, KEP.DEBUG_LOG_LEVEL,  "WOK_FGA.PlayerKilled Couldn't find killed player." )
            return
        
        key = 'PLAYER_KILLED' + str( random.randint( 1, 5 ) )
        msg = StringTables.KACHING[ key ]
        msg = msg.replace("@killer", killerStats.name)
        msg = msg.replace("@killed", playerStats.name)
        #msg = StringTables.KACHING[ key ] % (killerStats.name, WoK.getTeamNameFromIndex(killerStats.teamId), playerStats.name)
        self._broadcastArenaMessage( msg )
        
        sendScore = False
        if playerNetAssignedId != killerNetAssignedId and not self._sameTeam(playerKey, killerKey): # suicide and frags don't count
            self._teams[killerStats.teamId].kills += 1
            #self.players[killerKey].points += 1
            sendScore = True

        self.playDeathSounds(killerKey, playerKey, locX, locY, locZ)
        self.killedPlayerLoseCoins(playerKey, locX, locY, locZ)
            
        super(FoamGunArena, self).PlayerKilled(playerNetId, playerNetAssignedId, killerNetId, killerNetAssignedId, locX, locY, locZ)
        if sendScore:
            self._sendScore()
    
    #-------------------------------------------------------------------
    def killedPlayerLoseCoins(self, playerKey, locX, locY, locZ):
        import Game
        if self.players[playerKey].heldCoins >= MIN_DROP_COINS:
            dropCoin = PowerUpClass.PowerUp( None, locX, locY + 2, locZ, PowerUpClass.COIN_NPCID )
            self._powManager.spawnPowerup( dropCoin )
        self.players[playerKey].heldCoins = 0
    
    #------------------------------------------------------------------------------------    
    def playDeathSounds(self, killerKey, playerKey, locX, locY, locZ):
        playerStats = self.players[playerKey]
        dropCoins = self.players[playerKey].heldCoins >= MIN_DROP_COINS
        if WoK.GetGenderFromEDBIndex( playerStats.edbIndex ) == WoK.MALE_EDB:
            if dropCoins:
                self.broadcastPlayerSound(SoundGlobals.SOUND_KACHING_DEATHMALE2, playerKey)
            else:
                self.broadcastPlayerSound(SoundGlobals.SOUND_KACHING_DEATHMALE1, playerKey)
        else:
            if dropCoins:
                self.broadcastPlayerSound(SoundGlobals.SOUND_KACHING_DEATHFEM2, playerKey)
            else:
                self.broadcastPlayerSound(SoundGlobals.SOUND_KACHING_DEATHFEM1, playerKey)
    
        DIST_DENIED = 25    ### to come from game parameters
        DROP_X = 0          ### dropzone loc to come from map database
        DROP_Z = 0          ### dropzone loc to come from map database
        dist = 9999
        if dropCoins:
            dist = Utils.distance( [locX, locZ], [DROP_X, DROP_Z] )
        if dist < DIST_DENIED:
            if playerStats.teamId == WoK.BLUE_TEAMID:
                self.singlePlayerSound(SoundGlobals.SOUND_KCVO_REDSAVE, killerKey)
            elif playerStats.teamId == WoK.RED_TEAMID:
                self.singlePlayerSound(SoundGlobals.SOUND_KCVO_BLUESAVE, killerKey)
            else:
                self.singlePlayerSound(SoundGlobals.SOUND_KCVO_CHECK, killerKey)
            self.singlePlayerSound(SoundGlobals.SOUND_KCVO_DENIED, playerKey)
        else:
            self.singlePlayerSound(SoundGlobals.SOUND_KCVO_CHECK, killerKey)
            self.singlePlayerSound(SoundGlobals.SOUND_KCVO_GOTBOUNCED, playerKey)
    
    #------------------------------------------------------------------------------------    
    def playScoreSounds(self, playerKey, teamID, oldScore):
        """relying on new score having updated by now"""
        
        blueTeam = self._teams[WoK.BLUE_TEAMID]
        redTeam = self._teams[WoK.RED_TEAMID]
        blueScore = blueTeam.points
        redScore = redTeam.points
        MAX_SCORE = 50  ## to come from game parameters
        NEARVIC_SCORE = 40  ## to come from game parameters
        
        if teamID == WoK.BLUE_TEAMID:
            if blueScore >= MAX_SCORE:
                return
            if blueScore >= NEARVIC_SCORE:
                self.broadcastPlayerSound(SoundGlobals.SOUND_KCVO_BLUENEARVIC, playerKey)
                return
            if oldScore <= redScore and blueScore > redScore:
                self.teamPlayerSound( SoundGlobals.SOUND_KCVO_GAINEDLEAD, WoK.BLUE_TEAMID, playerKey)
                self.teamPlayerSound( SoundGlobals.SOUND_KCVO_LOSTLEAD, WoK.RED_TEAMID, playerKey)
                return
        if teamID == WoK.RED_TEAMID:
            if redScore >= MAX_SCORE:
                return
            if redScore >= NEARVIC_SCORE:
                self.broadcastPlayerSound(SoundGlobals.SOUND_KCVO_REDNEARVIC, playerKey)
                return
            if oldScore <= blueScore and redScore > blueScore:
                self.teamPlayerSound( SoundGlobals.SOUND_KCVO_GAINEDLEAD, WoK.RED_TEAMID, playerKey)
                self.teamPlayerSound( SoundGlobals.SOUND_KCVO_LOSTLEAD, WoK.BLUE_TEAMID, playerKey)
                return
        self.broadcastPlayerSound(SoundGlobals.SOUND_KCVO_KACHING, playerKey)
    
    #------------------------------------------------------------------------------------       
    def processCoinDrop(self, game, fromNetId, firstCharacterNetAssignedId):
        import Game
        
        # get the player id for the first character, this should always be a player
        player1 = Game.GetPlayerByNetTraceId(game, firstCharacterNetAssignedId, 0, 0)
        if player1 == 0: # not found, or not in this arena, don't care
            #logMsg(KEP.DEBUG_LOG_LEVEL, "    player1 null")
            return False

        # get the channel and the net id from the initiating player
        netID = GetSet.GetNumber(player1, PlayerIds.CURRENTID)
        playerKey = ArenaBase.MakePlayerKey(netID, firstCharacterNetAssignedId)
        playerNetID = ArenaBase.NetIdFromKey(playerKey)

        if not self.players.has_key(playerKey): # not in this arena, don't care
            return False
        
        if self.players.has_key(playerKey):
            if self.players[playerKey].heldCoins >= MIN_DROP_COINS:
                teamID = self.players[playerKey].teamId
                if teamID == WoK.BLUE_TEAMID or teamID == WoK.RED_TEAMID:
                    playerStats = self.players[playerKey]
                    totalPoints = FoamGunArena.getPointsForCoins(self.players[playerKey].heldCoins)
                    if(self._teams[teamID].points + totalPoints > self._pointsToWin):
                        totalPoints = int(self._pointsToWin - self._teams[teamID].points)
                        if(totalPoints < 0):
                            totalPoints = 0 
                    oldScore = self._teams[teamID].points
                    self._teams[teamID].points += totalPoints
                    playerStats.points += totalPoints
                    playerStats.heldCoins = 0;
                    playerStats.equipment.equipHeadIcon(playerNetID, ArenaEquipment.HEAD_GEM)
                    self.playScoreSounds(playerKey, teamID, oldScore)
                    msg = StringTables.KACHING[ 'PLAYER_SCORED1' ]
                    msg = msg.replace("@player", playerStats.name)
                    msg = msg.replace("@points", str(totalPoints))
                    #msg = StringTables.KACHING[ 'PLAYER_SCORED1' ] % (playerStats.name, WoK.getTeamNameFromIndex(playerStats.teamId), totalPoints)
                    self._broadcastArenaMessage( msg )
                    
                    self.updateTeamCoinsEvent()
                    self.updatePlayerCoinsEvent(self._dispatcher, netID, FoamGunArena.EVT_COINS_UPDATE, self.players[playerKey].heldCoins)
                    self._doWeHaveAWinner()
                    return True
        return False
    #------------------------------------------------------------------------------------       
    def processJumpPad(self, game, fromNetId, firstCharacterNetAssignedId):
        for player in self.players:
            if(ArenaBase.KeyNetIdEqNetId(player, fromNetId)):
                self.broadcastPlayerSound(SoundGlobals.SOUND_KACHING_JUMPPAD, player, fromNetId)
                return True
        return False
        
    #------------------------------------------------------------------------------------       
    def processCollision(self, game, fromNetId, firstCharacterNetAssignedId, secondCharacterNetAssignedId):
        import Game
        
        # get the player id for the first character, this should always be a player
        player1 = Game.GetPlayerByNetTraceId(game, firstCharacterNetAssignedId, 0, 0)
        if player1 == 0: # not found, or not in this arena, don't care
            #logMsg(KEP.DEBUG_LOG_LEVEL, "    player1 null")
            return False

        # get the channel and the net id from the initiating player
        channelId = GetSet.GetNumber(player1, PlayerIds.CURRENTCHANNEL)
        netID = GetSet.GetNumber(player1, PlayerIds.CURRENTID)
        playerKey = ArenaBase.MakePlayerKey(netID, firstCharacterNetAssignedId)
        playerNetID = ArenaBase.NetIdFromKey(playerKey)

        if not self.players.has_key(playerKey): # not in this arena, don't care
            #logMsg( KEP.DEBUG_LOG_LEVEL, "    player not for us " + str(netID) + " key count is " + str(len(self.players)) + " firstcharacter is " + str(firstCharacterNetAssignedId))
            return False
        
        player2 = Game.GetItemObjectByNetTraceId(game, secondCharacterNetAssignedId)
        if player2 == 0:
            #logMsg( KEP.DEBUG_LOG_LEVEL, "_____Arena: player2 NULL " + str(secondCharacterNetAssignedId) + " on channel: " + str(channelId))
            return False
        
        ret = False
        Player2Cfg = GetSet.GetNumber(player2, ServerItemIds.SPAWNID)
        if self._powManager:
            if Player2Cfg == PowerUpClass.COIN_NPCID:
                if self.players.has_key(playerKey):
                    self.players[playerKey].heldCoins = self.players[playerKey].heldCoins + 1
                if self.players[playerKey].heldCoins >= MIN_DROP_COINS:
                    self.players[playerKey].equipment.equipHeadIcon(playerNetID, ArenaEquipment.HEAD_COIN)
                #logMsg( KEP.DEBUG_LOG_LEVEL, "_____Arena: player " + str(firstCharacterNetAssignedId) + " has " + str(self.players[playerKey].heldCoins) + " coins.")
                self._powManager.powerUpHit(player1, Player2Cfg, fromNetId, secondCharacterNetAssignedId)
                self.updatePlayerCoinsEvent(self._dispatcher, netID, FoamGunArena.EVT_COINS_UPDATE, self.players[playerKey].heldCoins)
                self.broadcastPlayerSound(SoundGlobals.SOUND_KACHING_COIN, playerKey)
                ret = True
            elif Player2Cfg == PowerUpClass.HEALTH_NPCID:
                self._powManager.powerUpHit(player1, Player2Cfg, fromNetId, secondCharacterNetAssignedId)
                self.updatePlayerData(self._dispatcher, playerKey, netID, ArenaBase.ARENA_PLAYER_UPDATED_FILTER)
                self.broadcastPlayerSound(SoundGlobals.SOUND_KACHING_PICKUP1, playerKey)
                ret = True
            elif Player2Cfg == PowerUpClass.SHIELD_NPCID:
                self._powManager.powerUpHit(player1, Player2Cfg, fromNetId, secondCharacterNetAssignedId)
                self.broadcastPlayerSound(SoundGlobals.SOUND_KACHING_PICKUP3, playerKey)
                self.EquipShield( playerKey, playerNetID, True, 10 )
                ret = True
            elif Player2Cfg == PowerUpClass.AMMO_MG_NPCID or Player2Cfg == PowerUpClass.AMMO_SG_NPCID or Player2Cfg == PowerUpClass.AMMO_BFG_NPCID:
                ammoID = -1
                self._powManager.powerUpHit(player1, Player2Cfg, fromNetId, secondCharacterNetAssignedId)
                playerStats = self.players[ playerKey ]
                if Player2Cfg == PowerUpClass.AMMO_MG_NPCID:
                    ammoID = ArenaEquipment.MG_AMMO
                    playerStats.equipment.addAmmo(player1, playerNetID, ammoID)
                if Player2Cfg == PowerUpClass.AMMO_SG_NPCID:
                    ammoID = ArenaEquipment.SG_AMMO
                    playerStats.equipment.addAmmo(player1, playerNetID, ammoID, 5)
                if Player2Cfg == PowerUpClass.AMMO_BFG_NPCID:
                    ammoID = ArenaEquipment.BFG_AMMO
                    # player gets 3 rockets
                    playerStats.equipment.addAmmo(player1, playerNetID, ammoID, 3)
                self.updatePlayerData(self._dispatcher, playerKey, netID, ArenaBase.ARENA_PLAYER_UPDATED_FILTER)
                self.broadcastPlayerSound(SoundGlobals.SOUND_KACHING_PICKUP2, playerKey)
                ret = True
                
            #self.players[playerKey].equipment.equipShield( playerNetID, not ret )
            
        return ret
    
    #------------------------------------------------------------------------------------    
    def updatePlayerData(self, dispatcher, playerKey, playerNetId, filterName):
        event = Dispatcher.MakeEvent(dispatcher, "ArenaManagerEvent")
        Event.SetFilter(event, filterName)
        Event.EncodeNumber(event, self.players[playerKey].teamId)
        ev = self.players[playerKey].equipment.encodeWeaponInfo(event)
        self._sendEvent(playerNetId, ev)
            
    #------------------------------------------------------------------------------------       
    def updatePlayerCoinsEvent(self, dispatcher, playerNetId, eventName, coinCount):
        event = Dispatcher.MakeEvent(dispatcher, eventName)
        Event.EncodeNumber(event, coinCount)
        Event.EncodeNumber(event, FoamGunArena.getBonusPoints(coinCount))
        self._sendEvent(playerNetId, event)
        
    #------------------------------------------------------------------------------------       
    def updateTeamCoinsEvent(self):
        event = Dispatcher.MakeEvent(KEP.dispatcher, FoamGunArena.GAME_STATS_EVENT)
        self.encodeGameStats(event, 2)
        self._broadcastEvent(event)
        

#-------------------------------------------------------------------
def logMsg(level, msg):
    #needed for logging
    import ParentHandler
    ParentHandler.LogMessage(gParent, level, msg)
    
#-------------------------------------------------------------------
def InitializeKEPEventHandlers(dispatcher, parent, dbgLevel):
    """Generic event handler interface function"""

    global gParent
    global gDispatcher
    global gBackgroundThread

    gParent = parent
    gDispatcher = dispatcher
    #KEP.debugLevel = dbgLevel

    ParentHandler.RegisterEventHandler(dispatcher, parent, FoamGunArena.collisionHandler, 1, 0, 0, 0, "EntityCollisionEvent", KEP.HIGH_PRIO) 
    ParentHandler.RegisterEventHandler(dispatcher, parent, FoamGunArena.coinDropHandler, 1, 0, 0, 0, "CoinDropOffEvent", KEP.HIGH_PRIO)
    ParentHandler.RegisterEventHandler(dispatcher, parent, FoamGunArena.jumpPadHandler, 1, 0, 0, 0, "JumpPadEvent", KEP.MED_PRIO) 

    gBackgroundThread = WorkerThread.WorkerThread("WOK_FoamGunArena")
    ParentHandler.LogMessage(gParent, KEP.INFO_LOG_LEVEL, str(gBackgroundThread) + " tid: "+str(gBackgroundThread._get_my_tid()))
    
    return  True 

#-------------------------------------------------------------------
def InitializeKEPEvents(dispatcher, handler, dbgLevel):
    
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, FoamGunArena.EVT_COINS_UPDATE, KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, FoamGunArena.TEAM_COINS_UPDATED, KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, FoamGunArena.GAME_STATS_EVENT, KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, FoamGunArena.UPDATE_PLAYER_LISTS_EVENT, KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "ArenaManagerEvent", KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "BroadcastSoundEvent", KEP.MED_PRIO)


#-------------------------------------------------------------------
def ReinitalizeKEPEvents(dispatcher, parent, dbgLevel):
    """ called from C to give us a chance to cleanup before shutdown or re-init """
    return KEP.getDependentModules(__name__)
	
