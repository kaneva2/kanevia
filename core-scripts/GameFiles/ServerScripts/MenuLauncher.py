#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#


#import helper Python code
import KEP

# C functions
import Dispatcher
import Event

#-------------------------------------------------------------------
#------------------------------------------------------------------------------------
def launchMenu( playerNetId, menuName ):
    event = Dispatcher.MakeEvent( KEP.dispatcher, "LaunchMenuEvent" )
    Event.EncodeString( event, menuName )
    Event.AddTo( event, playerNetId )
    Dispatcher.QueueEvent( KEP.dispatcher, event )

#------------------------------------------------------------------------------------
def tutorialTipMenu( playerNetId, message = "" ):
    launchMenu( playerNetId, "TutorialTip.xml" )
    
    event = Dispatcher.MakeEvent( KEP.dispatcher, "TutorialTipEvent" )
    Event.EncodeString( event, message )
    Event.AddTo( event, playerNetId )
    Dispatcher.QueueEvent( KEP.dispatcher, event )
    
#------------------------------------------------------------------------------------
def questTradeMenu( playerNetId, type = 1, msg = "", listMsg = "" ):
    launchMenu( playerNetId, "QuestTrade.xml" )
    
    event = Dispatcher.MakeEvent( KEP.dispatcher, "QuestTradeEvent" )
    Event.EncodeNumber( event, type )
    Event.EncodeString( event, msg )
    Event.EncodeString( event, listMsg )
    Event.AddTo( event, playerNetId )
    Dispatcher.QueueEvent( KEP.dispatcher, event )
    
#------------------------------------------------------------------------------------    
def setTextInOpenMenu( playerNetId, filter, titleText = "", subtitleText = "", bodyText = "" ):
    event = Dispatcher.MakeEvent( KEP.dispatcher, "SetTextEvent" )
    Event.EncodeString( event, titleText )
    Event.EncodeString( event, subtitleText )
    Event.EncodeString( event, bodyText )
    Event.SetFilter( event, filter )
    Event.AddTo( event, playerNetId )
    Dispatcher.QueueEvent( KEP.dispatcher, event )

#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------    
def setJobRankTable( playerNetId, filter, rankTable ):
    event = Dispatcher.MakeEvent( KEP.dispatcher, "SetJobRankTableEvent" )
    Event.EncodeNumber( event, len( rankTable ) )
    rankTable.reverse()
    for ( size, title, income ) in rankTable:
        msg = "%(@size)-2d Invites accepted   Rank: %(@title)-16s  Income: %(@income)-2d" % { '@size': ( size - 1 ), '@title': title, '@income':income }
        Event.EncodeString( event, msg )
    rankTable.reverse()
    Event.SetFilter( event, filter )
    Event.AddTo( event, playerNetId )
    Dispatcher.QueueEvent( KEP.dispatcher, event )

#------------------------------------------------------------------------------------    
def setJobMenuState( playerNetId, filter, jobMenuState ):
    event = Dispatcher.MakeEvent( KEP.dispatcher, "SetJobMenuStateEvent" )
    Event.EncodeNumber( event, jobMenuState )
    Event.SetFilter( event, filter )
    Event.AddTo( event, playerNetId )
    Dispatcher.QueueEvent( KEP.dispatcher, event )
    
#-------------------------------------------------------------------
#-------------------------------------------------------------------
def launchBrowser( playerNetId, url ):   
    event = Dispatcher.MakeEvent( KEP.dispatcher, "LaunchBrowserEvent" )
    Event.EncodeString( event, url )
    Event.AddTo( event, playerNetId )
    Dispatcher.QueueEvent( KEP.dispatcher, event )
    
#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    return True

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "LaunchMenuEvent", KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "TutorialTipEvent", KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "QuestTradeEvent", KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "LaunchBrowserEvent", KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "SetTextEvent", KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "SetJobRankTableEvent", KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "SetJobMenuStateEvent", KEP.MED_PRIO )

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function
    called from C to give us a chance to cleanup before shutdown or re-init"""
    return KEP.getDependentModules(__name__)