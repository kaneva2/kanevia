#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP

# C functions
import InstanceId
import Dispatcher
import Event
import GetSet
import ParentHandler

import PlayerIds
import GameGlobals

gParent = 0
gDispatcher = 0

CC_ZONE_INDEX_PLAIN = 49 ## zone_index for character creation zone

zoneIndexMap = dict()
zoneIndexMap[GameGlobals.ITEM_APT_FASHIONISTA_FLAT] = GameGlobals.ZONE_FASHIONISTA_FLAT
zoneIndexMap[GameGlobals.ITEM_APT_TRADITIONAL_LIVING] = GameGlobals.ZONE_TRADITIONAL_LIVING
zoneIndexMap[GameGlobals.ITEM_APT_CONTEMPORARY_CONDO] = GameGlobals.ZONE_CONTEMPORARY_CONDO
zoneIndexMap[GameGlobals.ITEM_APT_GAMERS_DEN] = GameGlobals.ZONE_GAMERS_DEN
zoneIndexMap[GameGlobals.ITEM_APT_SPORTS_ZONE] = GameGlobals.ZONE_SPORTS_ZONE

APT_SELECTION_EVENT = "ApartmentSelectionEvent"

def changeZone( dispatcher, idReference, inventoryItemId, apartmentId, upgradeType, invType ):
    e = Dispatcher.MakeEvent( dispatcher, "ZoneChangeEvent" )
    Event.EncodeNumber( e, idReference )
    Event.EncodeNumber( e, inventoryItemId )
    Event.EncodeNumber( e, apartmentId )
    Event.EncodeNumber( e, upgradeType )
    Event.EncodeNumber( e, invType )
    Event.EncodeNumber( e, 0 ) ## Dont readd to inventory on fail
    
    Dispatcher.QueueEvent( dispatcher, e ) 

#-------------------------------------------------------------------
#-------------------------------------------------------------------
class ApartmentSelector(object):
    """Simple wrapper object for ApartmentSelectionHandler"""

    def __init__(self):
        pass
    
    #-------------------------------------------------------------------    
    @staticmethod
    def ApartmentSelectionHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
        """Handles newbie apartment type selections

            The filter is the GLID of apt deed"""
        import Game

        kanevaUserId = 0

        glid = int(filter)
        gameRef = Dispatcher.GetGame(dispatcher)
        player = Game.GetPlayerByNetId(gameRef, fromNetId)


        if gameRef and player:

            ## Check to see if the glid is within the set of allowable apartments
            if not zoneIndexMap.has_key(glid):
                ParentHandler.LogMessage( gParent, KEP.WARN_LOG_LEVEL, "Unknown glid used for new apartment selection, glid: "+str(glid)  )
                return KEP.EPR_OK

            netID = GetSet.GetNumber( player, PlayerIds.NETWORKASSIGNEDID )
            playerID = GetSet.GetNumber( player, PlayerIds.HANDLE )
            name = GetSet.GetString( player, PlayerIds.CHARNAME )
            housingIndex = GetSet.GetNumber( player, PlayerIds.HOUSINGZONEINDEX )
            zid = int(InstanceId.GetId(housingIndex))
        
            if zid == CC_ZONE_INDEX_PLAIN:
                ## check to see if they already have one in their inventory somehow.  this will avoid the hard to catch inventory update exception
                qty = Game.GetInventoryItemQty(gameRef, fromNetId, glid)
                if qty == 0: 
                    Game.AddInventoryItemToPlayer(gameRef, player, glid, 1, 0)
                    Game.UpdatePlayer( gameRef, player )
                else:
                    ParentHandler.LogMessage( gParent, KEP.WARN_LOG_LEVEL, "Apartment deed already exists! skipping add qty: "+str(qty) )
                    
                changeZone( dispatcher, netID, glid, zoneIndexMap[glid], KEP.ApartmentUpgrade, GameGlobals.IT_NORMAL )
            else:
                ParentHandler.LogMessage( gParent, KEP.WARN_LOG_LEVEL, "Attempted use of new apartment template outside of character creation zone by: "+str(name) )

                

        return KEP.EPR_OK

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):
    global APT_SELECTION_EVENT

    ParentHandler.RegisterEventHandler( dispatcher, parent, ApartmentSelector.ApartmentSelectionHandler, 1,0,0,0, APT_SELECTION_EVENT, KEP.MED_PRIO )
    


#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):

    global gParent
    global gDispatcher

    # grab instances for debugging
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel

    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, APT_SELECTION_EVENT, KEP.MED_PRIO )

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give us a chance to cleanup before shutdown or re-init"
    return KEP.getDependentModules(__name__)
        


