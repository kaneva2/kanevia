#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#


###BASE_IMAGE_URL = "http://images.kaneva.com/"
###BASE_STREAMING_URL = "http://streaming.kaneva.com/stream.php?tId="
DEFAULT_THUMBNAIL_AUDIO =  "KanevaIconAudio_me.gif"

BASE_IMAGE_URL = "http://kanevadev/kimages/"
BASE_STREAMING_URL = "http://kanevadev/streaming/stream.php?tId="


DB_Server = "10.10.10.10"
DB_Port = 3306
DB_User = "testuser"
# todo encrypt this
DB_Password = "testpass" 
DB_Name = "wok"
DB_KanevaDBName = "kaneva"
