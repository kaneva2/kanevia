#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

"""
TeamedArena module that helper classes for teamed arenas
"""
#import helper Python code
import KEP
import ChatType

# C functions
import InstanceId
import Dispatcher
import Event
import GetSet
import ParentHandler

# python imports
import time

# GetSet Ids
import PlayerIds

import ArenaBase

#-------------------------------------------------------------------
#-------------------------------------------------------------------
class Team(object):
    """represents a team of players

    This tracks the data about a team, such as id, spawn points, etc.
    """
    
    def __init__(self,_name="",teamId=-1,_equipment=0):

        self.name = _name                         # name of the team
        self.equipOnSpawn = _equipment            # if != 0, a dict of edb to tuple (gemEquipId,flagEquipId) for when they spawn
        
        self.spawnPoints = list()                 # list of ArenaSpawnPoints for this team
        self.points = 0                           # their current score
        self.kills = 0                            # number of team kills, could be same as points if coins turned off
        self.teamId = teamId                      # the team to set everyone to
        self.playerCount = 0
        self.quitters = False
        
#-------------------------------------------------------------------
#-------------------------------------------------------------------
class TeamedArena(ArenaBase.ArenaBase):
    """base class for teamed arenas

    This allows an arena to have multiple teams.  This class
    tracks the temas, spawn points, score, etc. for the teams.

    This class also has logic for balancing teams with AI bots.
    """
    
    TEAMED_HUD_MENU = 6
    TEAMED_STAT_MENU = 7
    GAME_ADMIN_START_ARENA_EVENT = "GameAdminStartArenaEvent"
    
    #-----------------------------------------------------------------
    def __init__(self,name,parentList,game,arenaPtr,channelInstance,zi,maxBattleTime,pointsToWin,parent,dispatcher):
        """constructor"""
        super(TeamedArena,self).__init__(name,parentList,game,arenaPtr,channelInstance,zi,maxBattleTime,pointsToWin, \
                                            TeamedArena.TEAMED_HUD_MENU, TeamedArena.TEAMED_STAT_MENU, parent,dispatcher)

        # set some controls on how the base class works
        self._respawnInArenaOnDeath = True

        self._teams = dict()
        self.minPlayersPerTeam = 1
        self.maxPlayersPerTeam = 4
        self._nextTeamAssignment = 0
        self._balanceTeamsWithAI = False 
        self._startedSpawnGens = list()
        

    #-----------------------------------------------------------------
    def _makeTeam(self,_name,teamId,_equipment):
        """override this to make your version of the team

        This simple creates a Team object
        """

        return Team(_name,teamId,_equipment)

    #-----------------------------------------------------------------
    def _addTeam( self, teamName, teamId, equipItems, spawnPointIndex ):
        """helper for adding a team with their initial spawn point.
        
        Returns the index of the newly added team.  Call this when setting
        up your arena
        """
        import Arena
        
        self._teams[teamId] = self._makeTeam(teamName,teamId,equipItems)
        self._teams[teamId].spawnPoints.append( ArenaBase.ArenaSpawnPoint( Arena.GetSpawnPoint( self._arenaPtr, spawnPointIndex )) )

        return teamId


    #-----------------------------------------------------------------
    def _assignPlayerToTeam( self, playerKey ):
        """figure out what team to put player on

        this default impl just round-robins them to all the teams.  Override
        to assign by clan, race, etc.
        """

        ret = self._teams[self._teams.keys()[self._nextTeamAssignment]].teamId
        self._nextTeamAssignment += 1
    
        if self._nextTeamAssignment >= len( self._teams ):
            self._nextTeamAssignment = 0
        
        for team in self._teams:
            if(self._teams[team].playerCount <= self._teams[ret].playerCount):
                 ret = team
                 
        self.players[playerKey].teamId = ret
        self._teams[ret].playerCount += 1
        return ret
    
    #------------------------------------------------------------------------------------       
    def _buildEndEvent(self, winner ):
        """create the event of the end-arena info

        Calls the base class, and adds team info
        """

        event = super(TeamedArena,self)._buildEndEvent( winner )

        # add team school
        Event.EncodeNumber( event, len(self._teams) )
        for team in self._teams.values():
            Event.EncodeNumber( event, team.points )
            
        return event
        
    #-----------------------------------------------------------------
    def _getPlayerSpawnPoint( self, playerObj, playerKey ):
        """Get the spawn point based on the player

        This assigns a player to a team, if not already assigned, 
        and gets the spawn point for the team"""
        import Arena
        import Game

        team = 0
        
        playerStats = self.players[playerKey]
        
        if playerStats.teamId == -1:
            playerStats.teamId = self._assignPlayerToTeam( playerObj, playerKey, playerStats )

            playerNetId = ArenaBase.NetIdFromKey( playerKey )
            
            team = self._teams[playerStats.teamId]
            team.playerCount += 1

            KEP.debugPrint( "Assign player to team", playerStats.teamId, playerNetId, team.name, self._pointsToWin )
            
            # get the equip info for this player
            if team.equipOnSpawn.has_key(playerStats.edbIndex):
                playerStats.equipInfo = team.equipOnSpawn[playerStats.edbIndex]
            else:
                # TODO: shouldn't get here
                pass
            
        else:
            team = self._teams[playerStats.teamId]
            
        return (team.spawnPoints[0].x,team.spawnPoints[0].y,team.spawnPoints[0].z)

   
    #------------------------------------------------------------------------------------       
    def _addSpawnInfo( self, startEvent, playerKey ):
        """add extra info to the spawn event

        This adds the arena id, and the teams' names and points so the hud can update"""

        TEAMDED_ARENA_ID = 1
        
        playerStats = self.players[playerKey]
        
        Event.EncodeNumber( startEvent, TEAMDED_ARENA_ID )
        Event.EncodeNumber( startEvent, playerStats.teamId )
        Event.EncodeNumber( startEvent, len(self._teams ))
        for msgTeam in self._teams.values():
            Event.EncodeString( startEvent, msgTeam.name )
            Event.EncodeNumber( startEvent, msgTeam.teamId )   
            Event.EncodeNumber( startEvent, msgTeam.points )

    #------------------------------------------------------------------------------------       
    def _sameTeam( self, playerKey, killerKey):
        """returns True if the players are on same team"""

        try:
            KEP.debugPrint( "checking Same team ", playerKey, self.players[playerKey].teamId, "==",  killerKey, self.players[killerKey].teamId )
            return self.players[playerKey].teamId == self.players[killerKey].teamId
        except KeyError:
            KEP.debugPrint( "_sameTeam failed getting keys player haskey is", playerKey, self.players.has_key(playerKey) )
            KEP.debugPrint( "          failed getting keys killer haskey is", killerKey, self.players.has_key(killerKey) )
            return False
    
    """
    #------------------------------------------------------------------------------------       
    def _adjustArenaPoints( self, playerKey, playerIsAI, killerKey, killerIsAI ):
        #Override that is called when a player in the arena is killed. 
        #This checks teams when adjusting arena points
        
        import Game 

        KEP.debugPrint( "TeamedArena._adjustArenaPoints", playerKey, killerKey )
        # are they on the same team?
        if self._sameTeam( playerKey, killerKey ):

            # same team! baaaad boy! no biscuit!
            # subtract one from each, and let them know
            # don't track arena points for AI
            playerNetId = 0
            killerNetId = 0
            
            if not killerIsAI:
                killerNetId = ArenaBase.NetIdFromKey(killerKey)
                self._sendMessage( killerNetId, "You kill a teammate!  You lose an arena point", ChatType.ArenaMgr )

            if not playerIsAI:
                playerNetId = ArenaBase.NetIdFromKey(playerKey)
                self._sendMessage( playerNetId, "You were fragged!!", ChatType.ArenaMgr )

            if not killerIsAI and not playerIsAI: 
                Game.AdjustArenaPoints( self._game, killerNetId, -self._killedArenaPt, playerNetId, self._killedArenaPt )
                
        else:
            # use default 
            super( TeamedArena, self )._adjustArenaPoints( playerKey, playerIsAI, killerKey, killerIsAI )
    """
        
    #-----------------------------------------------------------------
    def _endArena(self,winner):
        """called when the arena is ending

        This cleans up this arena and calls the base class
        """

        # figure out the winner's name
        winner = "No one"
        highCount = 0

        for team in self._teams.values():
            if team.points > highCount:
                winner = team.name + " Team"
                highCount = team.points
            elif highCount > 0 and team.points == highCount: # tie
                winner = "No one"

        # kill off all the AI
        if len(self._startedSpawnGens) > 0:                
            import Game
            for sgen in self._startedSpawnGens:                
                Game.StopAISpawn( self._game, sgen, self.channelId )
        
        super(TeamedArena,self)._endArena(winner)

    #------------------------------------------------------------------------------------       
    def _doWeHaveAWinner(self):
        """override this to check if we have a winner when player killed/disconnected

        This checks to see if a team has won.
        """

        # if only one team left, end the arena
        teamId = -1
        for playerStats in self.players.values():
            if teamId >= 0 and playerStats.teamId != teamId: # another team!
                return False
            teamId = playerStats.teamId
            
        #only one team left            
        #self._endArena("")
        return True

    #-----------------------------------------------------------------
    def _sendScore( self, playerNetId = 0 ):
        """send the score to the arena

        derived classes can override to send extra data with the score
        """
        event = self._makeArenaMgrEvent( ArenaBase.ARENA_SCORE_FILTER )
        Event.EncodeNumber(event,len(self._teams))
        for team in self._teams.values():
            Event.EncodeNumber(event,team.points)
            Event.EncodeString(event,team.name)

        if playerNetId == 0:
            return self._broadcastEvent( event )
        else:
            self._sendEvent( playerNetId, event )
            return ""

        return ""
    #-------------------------------------------------------------------
    def PlayerNetIdDisconnected( self, playerNetId ):
        super(TeamedArena, self).PlayerNetIdDisconnected( playerNetId )
        
    def PlayerDisconnected(self, playerKey):
        endNow = False
        if playerKey in self.players:
            self._teams[self.players[playerKey].teamId].playerCount -= 1
            if(self._teams[self.players[playerKey].teamId].playerCount <= 0 and self.inLobby == False and self._hasArenaEnded == False): #entire team quit
                self._teams[self.players[playerKey].teamId].quitters = True
                endNow = True
        super(TeamedArena, self).PlayerDisconnected( playerKey )
        if(endNow == True):
            self.ArenaTimedOut()
        
    #-----------------------------------------------------------------
    def PlayerSwitchTeams( self, playerNetId ):
        """switch players teams"""
        
        import WoK
        
        for playerKey in self.players:
            if(ArenaBase.KeyNetIdEqNetId(playerKey, playerNetId) and self.inLobby == True):
                curTeam = self.players[playerKey].teamId;
                self._teams[curTeam].playerCount -= 1;
                if(curTeam == WoK.BLUE_TEAMID):
                    self._teams[WoK.RED_TEAMID].playerCount += 1
                    self.players[playerKey].teamId = WoK.RED_TEAMID
                else:
                    self._teams[WoK.BLUE_TEAMID].playerCount += 1
                    self.players[playerKey].teamId = WoK.BLUE_TEAMID
        
                
    #-----------------------------------------------------------------
    def PlayerSpawned( self, playerNetId, playerNetAssignedId, event, filter ):
        """Called when a player spawns into an arena

        This override arms the jewel on their head based on team, etc.
        """

        if filter == self.zoneIndex:
            # it's for us
            import Game
            
            super(TeamedArena,self).PlayerSpawned(playerNetId, playerNetAssignedId, event, filter )
            
            playerKey = ArenaBase.MakePlayerKey(playerNetId, playerNetAssignedId)
            if not self.players.has_key(playerKey):
            
                # we don't have him in our list, what's he doing in here?
                # if AI, then he's a fill in so add him
                KEP.debugPrint( "    Not found, must be new AI" )
                
                playerObj = Game.GetPlayerByNetTraceId( self._game, playerNetAssignedId, 0, 0 )
                if playerObj:
                    if GetSet.GetNumber( playerObj, PlayerIds.AICONTROLLED ) != 0:
                    
                        # add, them and put them on the appropriate team
                        name = GetSet.GetString( playerObj, PlayerIds.CHARNAME )
                        edb =  GetSet.GetNumber( playerObj, PlayerIds.CUREDBINDEX )
                        playerStats = ArenaBase.PlayerStats(name,edb)
                        playerStats.ai = True
                        playerStats.teamId = GetSet.GetNumber( playerObj, PlayerIds.TEAMID ) # for AI, each is already on a team   -- self._assignPlayerToTeam( playerObj, playerKey, playerStats )
                        team = self._teams[playerStats.teamId]
                        team.playerCount += 1
                        self.players[playerKey] = playerStats
                        GetSet.SetString( playerObj, PlayerIds.CHARNAME, "AI-" + str(team.playerCount)  )

                        
                        KEP.debugPrint( "   Added AI player", name, "to team", self._teams[playerStats.teamId].name, playerStats.teamId )
                    else:                  
                        Dispatcher.LogMessage( self._dispatcher, KEP.WARN_LOG_LEVEL,  "TeamedArena.PlayerSpawned Spawning player not AI NetAssigneId = %d" % (playerNetAssignedId) )
                        return KEP.EPR_CONSUMED
                else:
                    Dispatcher.LogMessage( self._dispatcher, KEP.WARN_LOG_LEVEL,  "TeamedArena.PlayerSpawned Couldn't get player obj for spawning player NetAssigneId = %d" % (playerNetAssignedId) )
                    return KEP.EPR_CONSUMED
           
                    

            # so now we should have them
            playerStats = self.players[playerKey]
            if not playerStats.ai:
            
                team = self._teams[playerStats.teamId]
                
                # do we need to set the team for this player
                if team.teamId >= 0:
                    # player team state restored by server
                    playerObj = Game.GetPlayerByNetTraceId( self._game, playerNetAssignedId, 0, 0 )
                    if playerObj:
                        oldTeam = GetSet.GetNumber( playerObj, PlayerIds.TEAMID ) # for AI, each is already on a team   -- self._assignPlayerToTeam( playerObj, playerKey, playerStats )
                        GetSet.SetNumber( playerObj, PlayerIds.TEAMID, team.teamId ) 
                   
                if team.equipOnSpawn != 0:
                    playerStats = self.players[playerKey]
                    if playerStats.equipInfo != 0:
                         #Game.AddInventoryItemToPlayer( self._game, playerNetId, playerStats.equipInfo[ArenaBase.GEM_INDEX] , 1, 1)
                        Game.EquipInventoryItem(self._game, playerNetId, playerStats.equipInfo[ArenaBase.GEM_INDEX]  )
            return KEP.EPR_CONSUMED
                
        else:
            return KEP.EPR_NOT_PROCESSED    

    #-------------------------------------------------------------------
    def StartArena(self):
        """startup the arena by spawning all the players to it

        This calls the base class, then calls _balanceTeams to 
        spawn any AI
        """
        super(TeamedArena,self).StartArena()

        # see if we need to fill in with AI
        if ( self._balanceTeamsWithAI ):
            self._balanceTeams()
    #-------------------------------------------------------------------
    def VerifyStartConditions(self):
        
        ret = super(TeamedArena,self).VerifyStartConditions()
        msg = ""
        if ret == True:
            for team in self._teams:
                if(self._teams[team].playerCount < self.minPlayersPerTeam):
                    msg = msg + self._teams[team].name + " must have at least " + str(self.minPlayersPerTeam) + " player(s) to start."
                    ev = Dispatcher.MakeEvent(KEP.dispatcher, TeamedArena.GAME_ADMIN_START_ARENA_EVENT)
                    Event.EncodeString(ev, msg)
                    self._sendArenaAdminEvent(ev)
                    return False
                elif(self._teams[team].playerCount > self.maxPlayersPerTeam):
                    msg = msg + self._teams[team].name + "can't have more than " + str(self.maxPlayersPerTeam) + " players."
                    ev = Dispatcher.MakeEvent(KEP.dispatcher, TeamedArena.GAME_ADMIN_START_ARENA_EVENT)
                    Event.EncodeString(ev, msg)
                    self._sendArenaAdminEvent(ev)
                    return False
        else:
            return False
        
        return True
    #-------------------------------------------------------------------
    def _balanceTeams(self):
        """balance the arena out by spawning AI

        This is called for arenas configured to balance out using AI.
        This checks the teams and configured counts for the teams to 
        determine how many AI should be spawned on each side
        """

        import Game

        try:
            teamCount  = len(self._teams)

            for team in self._teams.values():
            
                needed  = (self.minPlayers // teamCount) - team.playerCount
                
                if needed > 0:
                    # spawn the players
                    Game.StartAISpawn( self._game, team._spawnGenName, self.channelId , needed, 1000, 0 )
                    self._startedSpawnGens.append(team._spawnGenName)
                     
        except NameError:
            KEP.debugPrint( "Name error balancing teams", sys.exc_type, sys.exc_value )

                            
      
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function"""
    pass # do nothing, just do this so dispatcher knows about us

def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, TeamedArena.GAME_ADMIN_START_ARENA_EVENT, KEP.MED_PRIO )

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function"""
    " called from C to give us a chance to cleanup before shutdown or re-init"
    return KEP.getDependentModules(__name__)
        

    
