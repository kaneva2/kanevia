#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP
import ChatType
import WoK

# C functions
import PlayerIds
import InstanceId
import Dispatcher
import Event
import GetSet
import ParentHandler
import ArenaBase
import TeamDeathMatchArena
import PowerUpClass
import GameGlobals
import ServerItemIds

# added in this order
#BLUE_TEAM_INDEX = 1
#RED_TEAM_INDEX  = 2

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def HangarSpawnStaticAI( pclass, channelInstance ):
    #hornet red/blue
    pclass.AddPowerUp( channelInstance, 38, -293, 3976, 0   , PowerUpClass.WEAPONPOWERUP, GameGlobals.FEMALE_HORNET, GameGlobals.MALE_HORNET, GameGlobals.HORNET_AMMO, -1 ) #wep1,wep2, ammo
    pclass.SpawnIndexedPowerup( 38, 0 )
    pclass.AddPowerUp( channelInstance, 39, 293, 3976, 0    , PowerUpClass.WEAPONPOWERUP, GameGlobals.FEMALE_HORNET, GameGlobals.MALE_HORNET, GameGlobals.HORNET_AMMO, -1 ) #wep1,wep2, ammo
    pclass.SpawnIndexedPowerup( 39, 0 )
    
class TeamDeathMatchArena(TeamDeathMatchArena.TeamDeathMatchArena):
    "R/B Team death match-type arena"

    hangararenas = dict()
    
    @staticmethod
    def SignupSettings():
        " return type, and if AI balanced"
        return (ArenaBase.TEAMED_ARENA,True)

    #-------------------------------------------------------------------
    def __init__(self,parentList,game,arenaPtr,channelInstance,zi,maxBattleTime,pointsToWin,parent,dispatcher):
        """constructor"""
        super(TeamDeathMatchArena,self).__init__("TeamDeathMatchArena",parentList,game,arenaPtr,channelInstance,zi,maxBattleTime,pointsToWin,parent,dispatcher)

        # setup the specifics about this team DM
        # we have two teams, red and blue
        self._addTeam( WoK.BLUE_NAME, WoK.BLUE_TEAMID, WoK.BLUE_TEAM_EQUIPMENT, 1 )
        self._addTeam( WoK.RED_NAME, WoK.RED_TEAMID, WoK.RED_TEAM_EQUIPMENT, 0 )
        self._teams[WoK.BLUE_TEAMID]._spawnGenName = "Blue Side"
        self._teams[WoK.RED_TEAMID]._spawnGenName = "Red Side"

        #powerups
        self.hangararenas[channelInstance] = self
        self._powerups = PowerUpClass.PowerUpClass( dispatcher, game, channelInstance )

        HangarSpawnStaticAI( self._powerups, channelInstance )

    def powerUpHit(self, player1, Player2Cfg, fromNetId ):
        #------ self._powerups.powerUpHit( player1, Player2Cfg, fromNetId )
        return KEP.EPR_OK

    @staticmethod
    def collisionHandler(dispatcher, fromNetId, event, eventid, filter, objectid):
        "callback when collided with someone else"
        import Game
        
        if filter == KEP.TriggerEnter:
            firstCharacterNetAssignedId = Event.DecodeNumber( event )
            secondCharacterNetAssignedId = Event.DecodeNumber( event )

            # get the player id for the first character, this should always be a player
            player1 = Game.GetPlayerByNetTraceId( Dispatcher.GetGame( dispatcher ), firstCharacterNetAssignedId, 0, 0 )
            if player1 == 0: # not found, or not in this arena, don't care
                return KEP.EPR_NOT_PROCESSED

            # get the player id for the second character
            player2 = Game.GetItemObjectByNetTraceId( Dispatcher.GetGame( dispatcher ), secondCharacterNetAssignedId )
            if player2 == 0: # not found, or not in this arena, don't care
                return KEP.EPR_NOT_PROCESSED

            Player2Cfg     = GetSet.GetNumber( player2, ServerItemIds.SPAWNID)
            Player1Channel = GetSet.GetNumber( player1, PlayerIds.CURRENTCHANNEL )
            #------ if Player1Channel in TeamDeathMatchArena.hangararenas:
            #------     TeamDeathMatchArena.hangararenas[Player1Channel].powerUpHit( player1, Player2Cfg, fromNetId )
           
        return KEP.EPR_OK
    
    #-------------------------------------------------------------------
    def StartArena(self):
        """startup the arena by spawning all the players to it

        This calls the base class, then calls _balanceTeams to 
        spawn any AI
        """
        super(TeamDeathMatchArena,self).StartArena()

        # see if we need to fill in with AI
        self._balanceTeams()

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):
    ParentHandler.RegisterEventHandler( dispatcher, parent,  TeamDeathMatchArena.collisionHandler, 1,0,0,0, "EntityCollisionEvent", KEP.HIGH_PRIO )     
    return  True 

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    pass

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give us a chance to cleanup before shutdown or re-init"
    return KEP.getDependentModules(__name__)
        

