#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import httplib
import KEP
import Dispatcher
import Event
import GetSet

FAME_CHECKLIST_INFO_EVENT = "GetFameChecklistInfoEvent"

GET_CHECKLIST = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetChecklist xmlns="http://www.kaneva.com/webservices/Fame/">
      <userId>@userId</userId>
      <checklistId>@checkListId</checklistId>
      <filter></filter>
      <orderby></orderby>
      <pageNumber>1</pageNumber>
      <pageSize>7</pageSize>
    </GetChecklist>
  </soap:Body>
</soap:Envelope>
"""

def sendChecklist(fromNetId, userId, checkListId):
    import ServerGlobals
#construct and send the header
    requestString = GET_CHECKLIST
    requestString = requestString.replace("@userId", str(userId))
    requestString = requestString.replace("@checkListId", str(checkListId))
    #Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "Getting Checklist " + str(checkListId) + " for playerId " + str(userId) )
    hostname = ServerGlobals.BASE_URL + ":8086"
    hostname = hostname.replace("http://", "")
    #Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "from " + hostname)
    webservice = httplib.HTTP(hostname)
    webservice.putrequest("POST", "/Fame.asmx")
    webservice.putheader("Host", hostname)
    webservice.putheader("Content-Type", "text/xml; charset=utf-8")
    webservice.putheader("Content-Length", "%d" % len(requestString))
    webservice.endheaders()
    webservice.send(requestString)
    
    # get the response
    statuscode, statusmessage, header = webservice.getreply()
    #Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "Status Code: " + str(statuscode) + " " + str(statusmessage))
    res = webservice.getfile().read()
    #Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, res)
    ev = Dispatcher.MakeEvent(KEP.dispatcher, FAME_CHECKLIST_INFO_EVENT)
    Event.EncodeString(ev, res)
    Event.AddTo(ev, fromNetId)
    Dispatcher.QueueEvent(KEP.dispatcher, ev)