#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#python modules
import time, inspect
import pdb

#import helper Python code
import KEP
import CMD

#import the C methods from the exe
import ParentHandler
import Dispatcher
import Event
import GetSet
import InstanceId

#import Arenas
import GameBase

#import GetSet Ids
import ServerEngineIds
import MultiplayerIds
import WorldAvailableIds
import SpawnCfgIds

gDispatcher   = 0 
gParent       = 0
gInitialArena = 0
gAppGame      = 0
gMultiplayer  = 0
gBreakIntoDebugger = True

SCRIPT_CLIENT_EVENT = "ScriptClientEvent"
SCRIPT_SERVER_EVENT = "ScriptServerEvent"
UGC_ZONE_LITE_EVENT = "UgcZoneLiteEvent"
GM_SHUTDOWN_EVENT   = "ShutdownEvent"


#Script Client Filters
SC_ZONES_INFO                   = 13
SC_INSTANCE_INFO                = 14
SC_CREATE_INSTANCE              = 15
SC_JOIN_INSTANCE                = 16
SC_LOCK_INSTANCE                = 17

#Script Server Filters
SS_ZONES_INFO                   = 12
SS_INSTANCE_INFO                = 13
SS_CREATE_INSTANCE              = 14
SS_JOIN_INSTANCE                = 15
SS_LOCK_INSTANCE                = 16


#UGC ZoneLite Filters
REPLY_ZONES_LIST       = 101
##REPLY_SPAWNS_LIST        = 102 ignored
REPLY_CREATE_ZONE       = 103
REPLY_DELETE_ZONE       = 104
REPLY_RENAME_ZONE       = 105
##REPLY_SET_DEFAULT_ZONE  = 106 ignored
REPLY_ADD_SPAWN         = 107
REPLY_DELETE_SPAWN      = 108
##REPLY_SET_INITIAL_ARENA = 109 ignored
REPLY_UPDATE_SPAWN      = 110 
REPLY_RE_CREATE_ZONE    = 111

ARENA_TYPE_HEX         = 1342177280 #HEX 50000000

MAX_KANEVA_SUPPORTED_WORLD = 1000
#-----------------------------------------------------------------

gSpawnPoints = dict()

class SpawnPoint(object):

    def __init__(self, _position, _rotation, _name, _id):
        self.position = _position
        self.rotation = _rotation
        self.name     = _name
        self.id       = _id



#-------------------------------------------------------------------
# REGION GameSignupManager 
#-------------------------------------------------------------------
class GameSignupManager(object):
    # class-level variables
    # list of arenas currently active
    # map the instance id to the object
    gameSignupManagers = dict()
    nextInstanceId = 1

    #-------------------------------------------------------------------
    def __init__(self, gameId, name, scriptName):
        """constructor creates and initialized members"""

        self.__runningGameInstances = list()
        self.__runningClass = 0
        self.__zoneIndex = gameId
        self.__gameName = name
        self.__gameId = gameId #key in dictionary
        className = scriptName

        if len(className) > 0:
            try:
                i = className.find(".")
                if i >= 0:
                    modName = className[:i]
                    exec "import " + modName
                    self.__runningClass = eval( className )
                else:
                    ParentHandler.LogMessage(gParent, KEP.WARN_LOG_LEVEL, "GameManager::GameSignupManager Script name is invalid, needs module name: "+str(className))

            except NameError:
                ParentHandler.LogMessage(gParent, KEP.WARN_LOG_LEVEL, "GameManager::GameSignupManager Script name is invalid "+str(className))
        else:
            ParentHandler.LogMessage(gParent, KEP.WARN_LOG_LEVEL, "GameManager::GameSignupManager Empty script name")

        Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::GameSignupManager Game name is " + self.__gameName + " and class is " + str(self.__runningClass) + " id is " + str(id(self)) + " zoneIndex is " + str(self.__zoneIndex))

    #-------------------------------------------------------------------
    def CreateNewInstance(self, fromNetId):
        newGameInstance = self.__createNewGameInstance()
        if newGameInstance != None:
            self.SetStartingSpawnPoint(newGameInstance)
            newGameInstance.PlayerJoin(fromNetId)
            newGameInstance.StartGame()
            return newGameInstance.instanceId
        return -1

    def CreateNewInstanceWithoutPlayer(self):
      newGameInstance = self.__createNewGameInstance()

      if newGameInstance != None:
        self.SetStartingSpawnPoint(newGameInstance)
        newGameInstance.StartGame()
        return newGameInstance.instanceId
      return -1

    def SetStartingSpawnPoint(self, newGameInstance):
        global gSpawnPoints

        zoneIndex = InstanceId.GetClearedInstanceId(newGameInstance.zoneIndex) 
        if gSpawnPoints.has_key(zoneIndex):
            if len(gSpawnPoints[zoneIndex]) > 0:
                pos = gSpawnPoints[zoneIndex][0].position
                newGameInstance.SetStartingSpawnPoint(pos[0], pos[1], pos[2])
                newGameInstance.SetStartingRotation(gSpawnPoints[zoneIndex][0].rotation)
            else:
                Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::SetStartingSpawnPoint no spawn point found! "+str(zoneIndex))
        else:
            Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::SetStartingSpawnPoint zone not found! "+str(zoneIndex))

    #-------------------------------------------------------------------
    # private methods
    #-------------------------------------------------------------------
    def __encodeGameInfo(self, event):
        Event.EncodeString(event, self.__gameName)
        Event.EncodeNumber(event, self.__gameId)
        Event.EncodeNumber(event, len(self.__runningGameInstances))
        return event

    def __encodeGameInstanceInfo(self, event, instanceId):
        for gameInstance in self.__runningGameInstances:
            if gameInstance.instanceId == instanceId:
                Event.EncodeNumber(event, gameInstance.instanceId)
                return event
        Event.EncodeNumber(event, -1)
        return event

    def __encodeDetailedGameInstanceInfo(self, event, instanceId):
        found = False
        for gameInstance in self.__runningGameInstances:
            if gameInstance.instanceId == instanceId:
                found = True
                Event.EncodeNumber(event, gameInstance.instanceId)
                Event.EncodeNumber(event, int(gameInstance.locked))
                Event.EncodeNumber(event, len(gameInstance.players))
                gameInstance.encodePlayerInfo(event)
                return event
        Event.EncodeNumber(event, -1)
        return event

    def __encodeAllGameInstanceInfo(self, event):
        Event.EncodeString(event, self.__gameName)
        Event.EncodeNumber(event, self.__gameId)
        Event.EncodeNumber(event, len(self.__runningGameInstances))
        for gameInstance in self.__runningGameInstances:
            self.__encodeGameInstanceInfo(event, gameInstance.instanceId)
        return event 

    def __createNewGameInstance(self):

        # get the new instance id
        GameSignupManager.nextInstanceId = GameSignupManager.nextInstanceId + 1

        # clone
        newGameInstance = 0
        newGameInstance = self.__runningClass(self.__runningGameInstances, GameSignupManager.nextInstanceId, self.__zoneIndex )
        self.__runningGameInstances.append( newGameInstance )

        return newGameInstance

    def __createEditGameInstance(self):

        editInstanceId = 1

        # clone
        newGameInstance = 0
        newGameInstance = self.__runningClass(self.__runningGameInstances, editInstanceId, self.__zoneIndex )
        self.__runningGameInstances.append( newGameInstance )

        return newGameInstance


    #-------------------------------------------------------------------
    # static event handler methods
    #-------------------------------------------------------------------

    #-------------------------------------------------------------------
    @staticmethod
    def RemoveBrokenArenas():
        for gameSignUpManager in GameSignupManager.gameSignupManagers:
            for gameInstance in GameSignupManager.gameSignupManagers[gameSignUpManager].__runningGameInstances:
                if gameInstance.gameOver == True:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "GameManager:: forcefully removed game instance because it was broken.")
                    GameSignupManager.gameSignupManagers[gameSignUpManager].__runningGameInstances.remove(gameInstance)

    #-------------------------------------------------------------------
    @staticmethod
    def handleScriptClientEvent(dispatcher, fromNetId, event, eventid, filter, objectid):

        if filter != SC_ZONES_INFO and filter != SC_INSTANCE_INFO and filter != SC_CREATE_INSTANCE and filter != SC_JOIN_INSTANCE and filter != SC_LOCK_INSTANCE:
            return KEP.EPR_NOT_PROCESSED
		
        placementId = Event.DecodeNumber(event)
        zoneIndex   = Event.DecodeNumber(event)

        if filter == SC_ZONES_INFO:                        

            event = Dispatcher.MakeEvent(KEP.dispatcher, SCRIPT_SERVER_EVENT)
            Event.SetFilter(event, SS_ZONES_INFO)
            Event.EncodeNumber(event, placementId)
            Event.EncodeNumber(event, zoneIndex)
            Event.EncodeNumber(event, 0) #instance id            
            Event.EncodeNumber(event, len(GameSignupManager.gameSignupManagers))

            #send default arena first
            for gameSignUpManager in GameSignupManager.gameSignupManagers:
                if GameSignupManager.gameSignupManagers[gameSignUpManager].__gameId == gInitialArena:
                    GameSignupManager.gameSignupManagers[gameSignUpManager].__encodeAllGameInstanceInfo(event)
                    break

            #then send everything else
            for gameSignUpManager in GameSignupManager.gameSignupManagers:
                if GameSignupManager.gameSignupManagers[gameSignUpManager].__gameId != gInitialArena:
                    GameSignupManager.gameSignupManagers[gameSignUpManager].__encodeAllGameInstanceInfo(event)
            Dispatcher.QueueEvent(KEP.dispatcher, event)
            return KEP.EPR_OK

        if filter == SC_INSTANCE_INFO:
            instanceId = Event.DecodeNumber(event)			

            for gameSignUpManager in GameSignupManager.gameSignupManagers:
                for gameInstance in GameSignupManager.gameSignupManagers[gameSignUpManager].__runningGameInstances:
                    if gameInstance.instanceId == instanceId:
                        event = Dispatcher.MakeEvent(KEP.dispatcher, SCRIPT_SERVER_EVENT)
                        Event.SetFilter(event, SS_INSTANCE_INFO )
                        Event.EncodeNumber(event, placementId)
                        Event.EncodeNumber(event, zoneIndex)
                        Event.EncodeNumber(event, 0) #instance id
                        GameSignupManager.gameSignupManagers[gameSignUpManager].__encodeDetailedGameInstanceInfo(event, instanceId)
                        Dispatcher.QueueEvent(KEP.dispatcher, event)
                        return KEP.EPR_OK

            event = Dispatcher.MakeEvent(KEP.dispatcher, SCRIPT_SERVER_EVENT)
            Event.SetFilter(event, SS_INSTANCE_INFO )
            Event.EncodeNumber(event, placementId)
            Event.EncodeNumber(event, zoneIndex)
            Event.EncodeNumber(event, 0) #instance id
            Event.EncodeNumber(event, -1)
            Dispatcher.QueueEvent(KEP.dispatcher, event)
            return KEP.EPR_OK
        
        if filter == SC_CREATE_INSTANCE:
            gameKey = Event.DecodeNumber(event)
            clientNetId = Event.DecodeNumber(event)

            newInstanceID = -1
            for gameSignUpManager in GameSignupManager.gameSignupManagers:
                if gameKey == gameSignUpManager:
                    newInstanceID = GameSignupManager.gameSignupManagers[gameSignUpManager].CreateNewInstance(clientNetId)
                    break
            event = Dispatcher.MakeEvent(KEP.dispatcher, SCRIPT_SERVER_EVENT)
            Event.SetFilter(event, SS_CREATE_INSTANCE)
            Event.EncodeNumber(event, placementId)
            Event.EncodeNumber(event, zoneIndex)
            Event.EncodeNumber(event, 0)
            Event.EncodeNumber(event, newInstanceID) #instance id            
            Dispatcher.QueueEvent(KEP.dispatcher, event)
            return KEP.EPR_OK
        
        if filter == SC_JOIN_INSTANCE:
            instanceId	= Event.DecodeNumber(event)
            netId		= Event.DecodeNumber(event)

            success = False
            for gameSignUpManager in GameSignupManager.gameSignupManagers:
                for gameInstance in GameSignupManager.gameSignupManagers[gameSignUpManager].__runningGameInstances:
                    if gameInstance.instanceId == instanceId:
                        success = gameInstance.PlayerJoin(netId)
                        break
            event = Dispatcher.MakeEvent(KEP.dispatcher, SCRIPT_SERVER_EVENT)
            Event.SetFilter(event, SS_JOIN_INSTANCE)
            Event.EncodeNumber(event, placementId)
            Event.EncodeNumber(event, zoneIndex)
            Event.EncodeNumber(event, 0)
            Event.EncodeNumber(event, int(success))
            Dispatcher.QueueEvent(KEP.dispatcher, event)
            return KEP.EPR_OK
        
        if filter == SC_LOCK_INSTANCE:
            instanceId	= Event.DecodeNumber(event)
            locked		= Event.DecodeNumber(event)

            for gameSignUpManager in GameSignupManager.gameSignupManagers:
                for gameInstance in GameSignupManager.gameSignupManagers[gameSignUpManager].__runningGameInstances:
                    if gameInstance.instanceId == instanceId:
                        lockReturn = gameInstance.LockGame(bool(locked))
                        event = Dispatcher.MakeEvent(KEP.dispatcher, SCRIPT_SERVER_EVENT)
                        Event.SetFilter(event, SS_LOCK_INSTANCE)
                        Event.EncodeNumber(event, placementId)
                        Event.EncodeNumber(event, zoneIndex)
                        Event.EncodeNumber(event, 0)
                        Event.EncodeNumber(event, int(lockReturn))
                        Dispatcher.QueueEvent(KEP.dispatcher, event)
                        return KEP.EPR_OK

            # if we have not returned yet, there was a problem creating/locking the instance
            event = Dispatcher.MakeEvent(KEP.dispatcher, SCRIPT_SERVER_EVENT)
            Event.SetFilter(event, SS_LOCK_INSTANCE)
            Event.EncodeNumber(event, placementId)
            Event.EncodeNumber(event, zoneIndex)
            Event.EncodeNumber(event, 0)
            Event.EncodeNumber(event, 0)
            Dispatcher.QueueEvent(KEP.dispatcher, event)
            return KEP.EPR_OK

        return KEP.EPR_OK
    #-------------------------------------------------------------------

    #-------------------------------------------------------------------
    @staticmethod
    def handleShutdownEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        GameSignupManager.gameSignupManagers.clear()

        import gc
        gc.collect()

        return KEP.EPR_OK
    #-------------------------------------------------------------------

    #-------------------------------------------------------------------
    # The C++ handles this first so we look at replies
    @staticmethod
    def ugcZoneLiteHandler(dispatcher, fromNetid, event, eventid, filter, objectid):

        Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::ugcZoneLiteHandler eventid["+str(eventid)+"] filter["+str(filter)+"] objectid["+str(objectid)+"]")

        global gAppGame
        global gMultiplayer
        global gSpawnPoints

        if filter == REPLY_ZONES_LIST:

            zoneList = list()
            numZones = Event.DecodeNumber(event)
            for i in range(0,int(numZones)):
                zoneIndex     = Event.DecodeNumber(event)  # (plain)
                zoneName      = Event.DecodeString(event)  # displayName
                ignored       = Event.DecodeString(event)  # exg name
                isDefaultZone = Event.DecodeNumber(event)  # 0 NotDefault -- 1 Default

                arenaInstanceId = ARENA_TYPE_HEX + zoneIndex

                if (isDefaultZone != 0) or (zoneIndex >= MAX_KANEVA_SUPPORTED_WORLD or zoneIndex == 55):
                    if arenaInstanceId == gInitialArena:
                        # Initalize Default Initial Zone First
                        InitializeGame(arenaInstanceId, zoneName, "TemplateGame.TemplateGame")
                    else:
                        zoneList.append([arenaInstanceId, zoneName])

            # Initalize all remaining user zones
            for zoneData in zoneList:
                InitializeGame(zoneData[0], zoneData[1], "TemplateGame.TemplateGame")

            return KEP.EPR_OK


        if filter == REPLY_RE_CREATE_ZONE:
            oldZoneIndex = objectid                   # (plain)
            retval       = Event.DecodeNumber(event)  # 1 true -- 0 false
            newZoneIndex = Event.DecodeNumber(event)  # (plain)
            msg          = Event.DecodeString(event)  # 

            # 1) Rekey Zone from cache
            if retval == 1:
                oldArenaInstanceId = ARENA_TYPE_HEX + oldZoneIndex
                newArenaInstanceId = ARENA_TYPE_HEX + newZoneIndex

                # _SCR_
                ##DebugPrintGames()
                # _SCR_

                try:
                    zoneEntry = GameSignupManager.gameSignupManagers.pop(oldArenaInstanceId)
                    zoneEntry.__gameId    = newArenaInstanceId
                    zoneEntry.__zoneIndex = newArenaInstanceId
                    GameSignupManager.gameSignupManagers[newArenaInstanceId] = zoneEntry
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_RE_CREATE_ZONE] Success: zoneIndex["+str(oldZoneIndex)+"] rekeyed to ["+str(newZoneIndex)+"]")

                    # 2) rebuild SpawnPoints
                    gSpawnPoints.clear()
                    GetSpawnPoints()

                except:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_RE_CREATE_ZONE] warning(event ignored) -- Unknown zoneIndex["+str(oldZoneIndex)+"]")

            elif retval == 0:
                Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_RE_CREATE_ZONE] warning(event ignored) -- Failed request oldZoneIndex["+str(oldZoneIndex)+"] retval["+str(retval)+"] newZoneIndex["+str(newZoneIndex)+"] msg["+str(msg)+"]")



        if filter == REPLY_CREATE_ZONE:
            zoneIndex   = objectid                   # (plain)
            retval      = Event.DecodeNumber(event)  # 1 true -- 0 false
            newZoneName = Event.DecodeString(event)  # 
            msg         = Event.DecodeString(event)  # 

            if retval == 1:
                arenaInstanceId = ARENA_TYPE_HEX + zoneIndex

                # _SCR_
                ##DebugPrintGames()
                # _SCR_

                InitializeGame(arenaInstanceId, newZoneName, "TemplateGame.TemplateGame")

                # _SCR_
                ##DebugPrintGames()
                # _SCR_

                # rebuild SpawnPoints
                gSpawnPoints.clear()
                GetSpawnPoints()

            elif retval == 0:
                Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_CREATE_ZONE] warning(event ignored) -- Failed request newZoneName["+str(newZoneName)+"] retval["+str(retval)+"] msg["+str(msg)+"]")

            return KEP.EPR_OK



        if filter == REPLY_RENAME_ZONE:
            zoneIndex = objectid                   # (plain)
            retval    = Event.DecodeNumber(event)  # 1 true -- 0 false
            newName   = Event.DecodeString(event)  # 
            msg       = Event.DecodeString(event)  # 

            if retval == 1:

                arenaInstanceId = ARENA_TYPE_HEX + zoneIndex

                try:
                    GameSignupManager.gameSignupManagers[arenaInstanceId].__gameName = newName # Raises a KeyError if key is not in the map.
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_RENAME_ZONE] Success: zoneIndex["+str(zoneIndex)+"]  renamed to["+str(newName)+"]")
                except:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_RENAME_ZONE] warning(event ignored) -- Unknown zoneIndex["+str(zoneIndex)+"]")

            elif retval == 0:
                Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_RENAME_ZONE] warning(event ignored) -- Failed request zoneIndex["+str(zoneIndex)+"] retval["+str(retval)+"] msg["+str(msg)+"]")

            return KEP.EPR_OK



        if filter == REPLY_DELETE_ZONE:
            zoneIndex = objectid                   # (plain)
            retval    = Event.DecodeNumber(event)  # 1 true -- 0 false
            msg       = Event.DecodeString(event)  # 

            if retval == 1:
                arenaInstanceId = ARENA_TYPE_HEX + zoneIndex

                # _SCR_
                ##DebugPrintGames()
                # _SCR_

                # 1) DeleteZone from cache
                try:
                    del GameSignupManager.gameSignupManagers[arenaInstanceId] # Raises a KeyError if key is not in the map.
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_DELETE_ZONE] Success: zoneIndex["+str(zoneIndex)+"] removed from cache")

                    try:
                        # 2) Delete associated SpawnPoints

                        # _SCR_
                        ##DebugPrintSpawnPoints()
                        # _SCR_

                        del gSpawnPoints[arenaInstanceId]  # Raises a KeyError if key is not in the map.
                        Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_DELETE_ZONE] Success: zoneIndex["+str(zoneIndex)+"] spawnpoints removed from cache")
                    except:
                        Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_DELETE_ZONE] warning: No spawn points found for zoneIndex["+str(zoneIndex)+"]")

                except:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_DELETE_ZONE] warning(event ignored) -- Unknown zoneIndex["+str(zoneIndex)+"]")

            elif retval == 0:
                Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_DELETE_ZONE] warning(event ignored) -- Failed request zoneIndex["+str(zoneIndex)+"] retval["+str(retval)+"] msg["+str(msg)+"]")

            # _SCR_
            ##DebugPrintGames()
            # _SCR_

            return KEP.EPR_OK

        if (filter == REPLY_ADD_SPAWN or filter == REPLY_UPDATE_SPAWN or filter == REPLY_DELETE_SPAWN):

            zoneIndex = objectid                   # (plain)
            retval    = Event.DecodeNumber(event)  # 1 true -- 0 false
            spawnId   = Event.DecodeNumber(event)  # 
            msg       = Event.DecodeString(event)  # 

            if retval == 1:

                arenaInstanceId = ARENA_TYPE_HEX + zoneIndex

                Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_*_SPAWN] Success: zoneIndex["+str(zoneIndex)+"] refreshing spawnpoints")

                # if its an index we care about rebuild SpawnPoints
                if GameSignupManager.gameSignupManagers.has_key(arenaInstanceId):
                    gSpawnPoints.clear()
                    GetSpawnPoints()
                else:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_*_SPAWN] warning(event ignored) -- Unknown zoneIndex["+str(zoneIndex)+"]")

            elif retval == 0:
                Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::ugcZoneLiteHandler[REPLY_*_SPAWN] warning(event ignored) -- Failed request zoneIndex["+str(zoneIndex)+"] retval["+str(retval)+"] msg["+str(msg)+"]")

            return KEP.EPR_OK


        return KEP.EPR_OK
    #-------------------------------------------------------------------

#-------------------------------------------------------------------
# ENDREGION GameSignupManager 
#-------------------------------------------------------------------




#-------------------------------------------------------------------
def InitializeGame(gameKey, name, scriptName):
    if not GameSignupManager.gameSignupManagers.has_key(gameKey):
        GameSignupManager.gameSignupManagers[gameKey] = GameSignupManager(gameKey, name, scriptName) # create a new one



#-------------------------------------------------------------------
def DebugPrintGames():
    Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::DebugPrintGames begin")

    for (key, item) in GameSignupManager.gameSignupManagers.items():
        Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::DebugPrintGames Game(key["+str(key)+"])")

    Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::DebugPrintGames end")



#-------------------------------------------------------------------
def DebugPrintSpawnPoints():
    global gSpawnPoints

    Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::DebugPrintSpawnPoints begin") 

    for (key, item) in gSpawnPoints.items():
        for point in gSpawnPoints[key]:
            Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::DebugPrintSpawnPoints Key["+str(skey)+"] SpawnPoint "+str(point.position)+" "+str(point.rotation)+" "+str(point.name)+" "+str(point.id) )

    Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::DebugPrintSpawnPoints end") 

#-------------------------------------------------------------------    
def GetSpawnPoints():
    global gAppGame
    global gMultiplayer
    global gSpawnPoints

    spawnCount = int(GetSet.GetArrayCount(gMultiplayer, MultiplayerIds.SPAWNPOINTCFGS))

    #Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::GetSpawnPoints got spawnCount["+str(spawnCount)+"]")

    for spawnIndex in range(spawnCount):
        spawnCfg = GetSet.GetObjectInArray(gMultiplayer, MultiplayerIds.SPAWNPOINTCFGS, spawnIndex)
        if spawnCfg != 0:
            sZoneIndex = ARENA_TYPE_HEX + GetSet.GetNumber(spawnCfg, SpawnCfgIds.ZONEINDEX)
            sPosition  = GetSet.GetVector(spawnCfg, SpawnCfgIds.POSITION)
            sRotation  = GetSet.GetNumber(spawnCfg, SpawnCfgIds.ROTATION)
            sName      = GetSet.GetString(spawnCfg, SpawnCfgIds.SPAWNPOINTNAME)
            sId        = GetSet.GetNumber(spawnCfg, SpawnCfgIds.ID)

            #Dispatcher.LogMessage(KEP.dispatcher, KEP.INFO_LOG_LEVEL, "GameManager::spawn(sZoneIndex["+str(sZoneIndex)+"] sId["+str(sId)+"] sName["+str(sName)+"] )")

            if not gSpawnPoints.has_key(sZoneIndex):
                gSpawnPoints[sZoneIndex] = list()

            gSpawnPoints[sZoneIndex].append(SpawnPoint(sPosition, sRotation, sName, sId))


#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    """Called from C to initialize all the handlers in this script"""

    global gParent
    global gDispatcher
    global gInitialArena
    global gAppGame
    global gMultiplayer

    gParent     = handler
    gDispatcher = dispatcher

    initialArena = 1342177334

    # get the game API 
    try:
        callable( Game.GetPlayerByNetId )
    except NameError:
        if not Dispatcher.GetFunctions( gDispatcher, KEP.GET_SERVER_FN ):
            Dispatcher.LogMessage(dispatcher, KEP.WARN_LOG_LEVEL,  "GameManager::InitializeKEPEventHandlers Python ArenaManager couldn't load server Game functions!!")
            return False
        import Game

        # one more check
        try:
            callable(Game.GetPlayerByNetId)
        except NameError:
            return False
        
    # get the arena API 
    try:
        callable(Arena.GetSpawnPointCount)
    except NameError:
        Dispatcher.GetFunctions( gDispatcher, KEP.GET_ARENA_FN )
        import Arena

        try:
            callable(Arena.GetSpawnPointCount)
        except NameError:
            Dispatcher.LogMessage( dispatcher, KEP.WARN_LOG_LEVEL,  "GameManager::InitializeKEPEventHandlers Python ArenaManager couldn't load server Arena functions!!" )
            return

    ParentHandler.RegisterEventHandler(dispatcher, handler,  GameSignupManager.handleScriptClientEvent,   1,0,0,0, SCRIPT_CLIENT_EVENT, KEP.HIGH_PRIO)
    ParentHandler.RegisterEventHandler(dispatcher, handler,  GameSignupManager.handleShutdownEvent,       1,0,0,0, GM_SHUTDOWN_EVENT,   KEP.MED_PRIO)
    ParentHandler.RegisterEventHandler(dispatcher, handler,  GameSignupManager.ugcZoneLiteHandler,        1,0,0,0, UGC_ZONE_LITE_EVENT, KEP.LOW_PRIO)

    gAppGame = Dispatcher.GetGameAsGetSet(dispatcher)
    if gAppGame != 0:
        gMultiplayer = GetSet.GetObject(gAppGame, ServerEngineIds.MULTIPLAYEROBJ)
        if gMultiplayer != 0:
            zi = GetSet.GetNumber(gMultiplayer, MultiplayerIds.INITIALARENAZONEINDEX)
            if zi != 0:
                initialArena = long(zi)
        else:
            Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::InitializeKEPEventHandlers GetSet failed to retrieve gMultiplayer object")
    else:
        Dispatcher.LogMessage(KEP.dispatcher, KEP.WARN_LOG_LEVEL, "GameManager::InitializeKEPEventHandlers GetSet failed to retrieve game object")
                
    gInitialArena = initialArena
    Dispatcher.LogMessage( dispatcher, KEP.INFO_LOG_LEVEL,  "GameManager::InitializeKEPEventHandlers Initial arena id: "+str(initialArena) )

    GetSpawnPoints()

    return True


#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    global gParent
    global gDispatcher

    # call the c funtion to register our event, we have just one
    gParent        = parent
    gDispatcher    = dispatcher
    KEP.debugLevel = dbgLevel

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """ called from C to give s a chance to cleanup before shutdown or re-init"""

    ret = KEP.getDependentModules(__name__)
    GameSignupManager.gameSignupManagers.clear()
    return ret

