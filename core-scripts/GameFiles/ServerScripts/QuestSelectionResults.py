#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#python modules
import datetime
import time
import urllib2

#import helper Python code
###import DatabaseAccess
import FameIds
import GameGlobals
import GetSetUtil
import KEP
import MenuLauncher
import QuestItemTrader
import QuestEmployment
import WorkerThread
import WorldFameAwarder

# C functions
import Dispatcher
import Event
import ParentHandler

QUEST_OPTION_SELECTED_EVENT = "QuestOptionSelectedEvent"

#Where does this number come from?! -Carson
QID_THANKSGIVING   = 209
QID_WEBSHOP_1      = 231
QID_WEBSHOP_2      = 232
QID_ODDJOB         = 1001

gBackgroundThread = None

#-------------------------------------------------------------------
def handleQuestSelectionResults( dispatcher, fromNetId, event, eventid, filter, objectid ): 
    global gBackgroundThread
    import Game
    
    questID = Event.DecodeNumber( event )
    selectionIndex = Event.DecodeNumber( event )
    
    if questID == QID_ODDJOB:
        playerObj = Game.GetPlayerByNetId( Dispatcher.GetGame( KEP.dispatcher ), fromNetId )
        if playerObj != 0:
            lastWorkTime = GetSetUtil.getPlayerIntValue( playerObj, "oddJobTime", GetSetUtil.TABLE_DEFAULT_NUM )
            crewCount = QuestEmployment.getCrewCount( fromNetId )
            gBackgroundThread.performWork(QuestEmployment.enterEmployment, fromNetId, selectionIndex, QID_ODDJOB, lastWorkTime, crewCount)
    elif questID == QID_WEBSHOP_1 or questID == QID_WEBSHOP_2:
        shopOnlineLauncher( fromNetId, selectionIndex )
    else:
        Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL,  "\n\nUNHANDLED QUEST: " + str( questID ) )
    return KEP.EPR_OK


#-------------------------------------------------------------------
def handlePerformEmploymentJob( dispatcher, fromNetId, event, eventid, filter, objectid ):
    import Game

    NetID = objectid
    curSeconds = filter
    level = int(Event.DecodeNumber( event ))
    playerObj = Game.GetPlayerByNetId( Dispatcher.GetGame( KEP.dispatcher ), NetID )
    if playerObj != 0:
        GetSetUtil.setPlayerNumValue( playerObj, "oddJobTime", curSeconds )
        #Passed to a background thread in redeemWorldFamePacket so we dont block main
        WorldFameAwarder.redeemWorldFamePacket( NetID, FameIds.WORLD_FAME, FameIds.JOB_FAME_IDS[ level ] )

    return KEP.EPR_CONSUMED

#-------------------------------------------------------------------
def shopOnlineLauncher( fromNetId, selectionIndex ):
    SELECT_FEMALE_CLOTHES = 0
    SELECT_MALE_CLOTHES = 1
    MALE_CLOTHES = "21"
    FEMALE_CLOTHES = "22"
    SHOP_PREFIX = "http://shop.kaneva.com/Catalog.aspx?s=&cId="
    ###  pv-shopping.kaneva.com
    SHOP_SUFFIX = "&setcs=b&kbill=1"
    
    url = SHOP_PREFIX
    if selectionIndex == SELECT_FEMALE_CLOTHES:
        url += FEMALE_CLOTHES
    else:
        url += MALE_CLOTHES
    url += SHOP_SUFFIX
    MenuLauncher.launchBrowser( fromNetId, url )

#-------------------------------------------------------------------  
#-------------------------------------------------------------------
def thanksgivingQuestSelection( fromNetId, selectionIndex ):
    import Game
    #playerObj = Game.GetPlayerByNetId( Dispatcher.GetGame( KEP.dispatcher ), fromNetId )
##    Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL,  "QUEST SELECTION: " + str( selectionIndex ) )
##    if selectionIndex == 0:
##        msg = "Welcome to the Kaneva Harvest Social!\n\n"
##        msg += "Goal: Collect all 20 food items and win an exclusive Thanksgiving-themed item (not available in stores).\n\n"
##        msg += "How to play:\n\n1. Sign in to Kaneva every day through November 30th and you'll automatically receive one Harvest Pack per day in your Inventory.\nEach Harvest Pack contains eight random food items, including five common, two uncommon, and one rare.\n\n"
##        msg += "2. To unpack the food items, open your Inventory, highlight the Harvest Pack, and click Use.\n\n"
##        msg += "3. Trade food items with your friends to collect all 20 food items.\n\n"
##        msg += "4. Visit me (Tom Turkey) when you have a complete set (one of each item):\n"
##        msg += "  - If you have a complete set, you'll win the grand prize!\n"
##        msg += "  - If you don't have a complete set, I'll tell you what you're missing.\n\n"
##        msg += "Hint:\n\nHaving trouble finding some rare food items? No problem. We can trade!\n\n"
##        msg += "  - If you have 10 of the same common food items: I'll trade you one new Harvest Pack.\n"
##        msg += "  - If you have 5 of the same uncommon food items: I'll trade you one new Harvest Pack.\n\n"
##        msg += "Note: The items in your Inventory automatically determine this trade.\nI'll never take the last food item of any type from you. In order to trade, you'll need at least 11 common food items (10 to trade, 1 to keep) and 6 uncommon food items (5 to trade and 1 to keep)."
##        MenuLauncher.tutorialTipMenu( fromNetId, msg )
##    elif selectionIndex == 1: #20 unique items for the win
##        giveItemList = list()
##        takeItemList = list()
##        allGlids = GameGlobals.HARVEST_RARES + GameGlobals.HARVEST_UNCOMMONS + GameGlobals.HARVEST_COMMONS
##        for i in allGlids:
##            giveItemList.append( ( 1, i ) )
##        for i in GameGlobals.HARVEST_WIN_ITEMS:
##            takeItemList.append( ( 1, i, GameGlobals.IT_GIFT ) )
##        tradeObj = QuestItemTrader.QuestItemTrader( Dispatcher.GetGame( KEP.dispatcher ), fromNetId )
##        tradeObj.doExchange( giveItemList, takeItemList )
##    elif selectionIndex == 2: #10 like commons for harvest packs
##        thanksgivingTradeForPacks( fromNetId, 10, GameGlobals.HARVEST_COMMONS )
##    elif selectionIndex == 3: #5 like uncommons for harvest packs
##        thanksgivingTradeForPacks( fromNetId, 5, GameGlobals.HARVEST_UNCOMMONS )
        
#-------------------------------------------------------------------
def thanksgivingTradeForPacks( playerNetId, tradeQty, glidList ):
    import Game
##    game = Dispatcher.GetGame( KEP.dispatcher )
##    giveItemList = list()
##    takeItemList = list()
##    totalPacks = 0
##    for glid in glidList:
##        qty = Game.GetInventoryItemQty( game, playerNetId, glid ) - 1
##        Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "Player has : " + str( qty ) + " " + str( glid ) + "\n" )
##        if qty >= tradeQty:
##            numPacks = int( qty / tradeQty )
##            Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, "   trade for : " + str( numPacks ) + " packs.\n" )
##            giveItemList.append( ( numPacks * tradeQty, glid ) )
##            totalPacks += numPacks    
##    takeItemList.append( ( totalPacks, GameGlobals.HARVEST_PACK_GLID ) )
##    tradeObj = QuestItemTrader.QuestItemTrader( game, playerNetId )
##    tradeObj.doExchange( giveItemList, takeItemList )

#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    global gBackgroundThread

    ParentHandler.RegisterEventHandler( dispatcher, handler, handleQuestSelectionResults, 1,0,0,0, QUEST_OPTION_SELECTED_EVENT, KEP.HIGH_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler, handlePerformEmploymentJob, 1,0,0,0, "PerformEmploymentJobEvent", KEP.MED_PRIO )
    
    gBackgroundThread = WorkerThread.WorkerThread("QuestSelectionResults")
    Dispatcher.LogMessage( KEP.dispatcher, KEP.INFO_LOG_LEVEL, str(gBackgroundThread) + " tid: "+str(gBackgroundThread._get_my_tid()))    
    
    return True

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, QUEST_OPTION_SELECTED_EVENT, KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "GiveUserCashEvent", KEP.MED_PRIO )

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function
    called from C to give us a chance to cleanup before shutdown or re-init"""
    return KEP.getDependentModules(__name__)
