#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

import ServerEngineIds
import Dispatcher
import GetSet
import KEP

DEFAULT_THUMBNAIL_AUDIO =  "KanevaIconAudio_me.gif"

#####  PROD  #####
BASE_IMAGE_URL = "http://images.kaneva.com/" 
BASE_STREAMING_URL = "http://streaming.kaneva.com/" 
BASE_URL = "http://dev-wokweb1"
#####  PROD  #####

gDispatcher = 0
UNINIT_DB_NAME = "db.cfg"  # invalid db name to indicate need to load from db.cfg

# default values
DB_Server = ""
DB_Port = 3306
DB_User = ""
DB_Password = "" 
DB_Name = UNINIT_DB_NAME
DB_KanevaDBName = "kaneva"


 
def loadDbConfig(): 
    global gDispatcher
    global DB_Server
    global DB_Port 
    global DB_User
    global DB_Password
    global DB_Name
    global DB_KanevaDBName 
    
    if DB_Name == UNINIT_DB_NAME:
        if not Dispatcher.GetFunctions( gDispatcher, KEP.GET_SERVER_FN ):
            Dispatcher.LogMessage( gDispatcher, KEP.ERROR_LOG_LEVEL,  "Python ServerGlobals couldn't load server Game functions!!" )
            return False

        if not Dispatcher.GetFunctions( gDispatcher, KEP.GET_KEPCONFIG_FN ):
            Dispatcher.LogMessage( gDispatcher, KEP.ERROR_LOG_LEVEL,  "Python ServerGlobals couldn't load server KEPConfig functions!!" )
            return False

        import Game
        import KEPConfig

        if gDispatcher != 0:
            game = Dispatcher.GetGame( gDispatcher )
            if game != 0:
                dbConfig = Game.GetDBConfig( game )
                if dbConfig != 0:
                    res, DB_Server = KEPConfig.GetString(dbConfig, "DB_Server", "")
                    res, DB_Port = KEPConfig.GetNumber(dbConfig, "DB_Port", 3306)
                    res, DB_User = KEPConfig.GetString(dbConfig, "DB_User", "")
                    res, DB_Password = KEPConfig.GetString(dbConfig, "DB_PasswordClear", "")
                    res, DB_Name = KEPConfig.GetString(dbConfig, "DB_Name", UNINIT_DB_NAME)
                    res, DB_KanevaDBName = KEPConfig.GetString(dbConfig, "DB_KanevaDBName", "Kaneva")


def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    global gDispatcher
    gDispatcher = dispatcher
