#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP

# C functions
import PlayerIds
import InstanceId
import ParentHandler
import Dispatcher
import Event
import GetSet

# python imports
import time
import random
import threading
import GameBase

#-------------------------------------------------------------------
#-------------------------------------------------------------------
class TemplateGame(GameBase.GameBase):
    
    def __init__(self, parentList, instanceID, zoneIndex):
         return super(TemplateGame, self).__init__(parentList, instanceID, zoneIndex)
         
        
    #------------------------------------------------------------------------------------       
    def __del__(self):
        return super(TemplateGame, self).__del__()
        
    def StartGame(self):
        return super(TemplateGame, self).StartGame()
    
    def EndGame(self):
        return super(TemplateGame, self).EndGame()
    
    def LockGame(self, locked):
        return super(TemplateGame, self).LockGame(locked)
    
    def AddPlayer(self, netId):
        return super(TemplateGame, self).AddPlayer(netId)
    
    def RemovePlayer(self, netId):
        return super(TemplateGame, self).RemovePlayer(netId)
    
    def SpawnPlayer(self, netId, x, y, z, rot=0):
        return super(TemplateGame, self).SpawnPlayer(netId, x, y, z, rot)
                
    def PlayerInGame(self, netId):
        return super(TemplateGame, self).PlayerInGame(netId)
                
    #-----------------------------------------------------------------
    def PlayerDisconnected( self, playerNetId ):
        return super(TemplateGame, self).PlayerDisconnected(playerNetId)

    #------------------------------------------------------------------------------------       
    def PlayerSpawned( self, playerNetId, filter ):
        return super(TemplateGame, self).PlayerSpawned(playerNetId, filter)
    
    def PlayerJoin(self, playerNetId):
        return super(TemplateGame, self).PlayerJoin(playerNetId)
#-------------------------------------------------------------------
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    #ParentHandler.RegisterEventHandler( dispatcher, handler,  GameBase.handleDisconnectedEvent,    1,0,0,0, "DisconnectedEvent", KEP.HIGH_PRIO ) 
     
    # filtered events
    ARENA_CHANNEL_MASK = 0x50000000;
    #ParentHandler.RegisterFilteredEventHandler( dispatcher, handler,  ArenaSignupManager.handleWeaponFiredEvent,   1,0,0,0, "WeaponFiredEvent", ARENA_CHANNEL_MASK, KEP.FILTER_IS_MASK, KEP.NO_OBJID_FILTER, KEP.MED_PRIO ) 
    

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    
#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function
    
    called from C to give us a chance to cleanup before shutdown or re-init
    """
    return KEP.getDependentModules(__name__)


