#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

###File name contains initial aa because the scripts are loaded
###alphabetically and we need to make sure an intance of a DatabaseInterface
###is created before any other scripts call these routines.
import Dispatcher
import KEP
import DatabaseInterface
import ServerGlobals

gDBAccessor = None

#-------------------------------------------------------------------
def sqlGetUserIdFromPlayer( playerId ):
    """get the user_id based on the player_id"""
    global gDBAccessor
    
    result = []

    userId = -1

    sql = "SELECT user_id FROM wok.players WHERE player_id = @playerId"
    sql = sql.replace( "@playerId", str(playerId) )

    gDBAccessor.fetchRows( sql, result )

    if( len(result) > 0 ):
        userId = result[0][0]

    return userId

#-------------------------------------------------------------------
def sqlGetKanevaUserIdFromPlayerId( playerId ):
    """get the kaneva_user_id based on the player_id"""
    global gDBAccessor
    
    result = []

    kaneva_user_id = -1
    
    sql = "SELECT kaneva_user_id FROM wok.players WHERE player_id = @playerId"
    sql = sql.replace( "@playerId", str(playerId) )

    gDBAccessor.fetchRows( sql, result )

    if( len(result) > 0 ):
        kaneva_user_id = result[0][0]
    
    return kaneva_user_id

#-------------------------------------------------------------------
def sqlGetKanevaUserIdFromName( name ):
    """get the kaneva_user_id based off the name"""
    global gDBAccessor
    
    result = []

    kaneva_user_id = -1
    
    sql = "SELECT kaneva_user_id FROM wok.players WHERE name = '@name'"
    sql = sql.replace( "@name", str(name) )

    gDBAccessor.fetchRows( sql, result )

    if( len(result) > 0 ):
        kaneva_user_id = result[0][0]

    return kaneva_user_id

#-------------------------------------------------------------------
def sqlGetItemNameFromGlid( glid ):
    """get an item name based off of the global id"""
    global gDBAccessor

    result = []

    item_name = ""

    sql = "SELECT name FROM wok.items WHERE global_id = @glid"
    sql = sql.replace( "@glid", str(glid) )

    gDBAccessor.fetchRows( sql, result )

    if( len(result) > 0 ):
        item_name = result[0][0]

    return item_name        

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    pass

#-------------------------------------------------------------------
def InitializeKEPEventHandlers(dispatcher, handler, dbgLevel):
    """Called from C to initialize all the handlers in this script"""
    global gDBAccessor
    ServerGlobals.loadDbConfig()
    gDBAccessor = DatabaseInterface.DBAccess("aaSqlUtil")
    Dispatcher.LogMessage(dispatcher, KEP.WARN_LOG_LEVEL, "db object: "+str(gDBAccessor))
    
