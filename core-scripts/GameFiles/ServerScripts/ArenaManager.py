#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

"""
ArenaManager module contains the ArenaSignupManager class
"""
#python modules
import time, inspect

import pdb

#import helper Python code
import KEP
import CMD

#get set ids
import PlayerIds
import ArenaIds
import ChatType

#import the C methods from the exe
import ParentHandler
import Dispatcher
import Event
import GetSet
import InstanceId

# import arena types
import ArenaBase

gDispatcher = 0
gParent = 0
gBreakIntoDebugger = True

RETURN_TRUE = 1
RETURN_FALSE = 0

MSG_TOO_MANY_PLAYERS = "Max players signed up.  Try again later"
MSG_TOO_MANY_TEAMS = "Max teams signed up.  Try again later"
MSG_OUT_OF_LEVEL_RANGE = "You are not of the correct level to signup"



#-------------------------------------------------------------------
class ArenaSignupManager(object):
    """Arena manager class to manage all arenas of a given type

    One of these objects will be created for each arena configured in 
    the editor.  It manages all the signups, the signup restrictions,
    and starting of arenas.  It also subscribes to many of the event
    that arena need to process, and delegates them to the arena
    """

    # class-level variables
    # list of arenas currently active
    # map the instance id to the object
    arenas = dict()
    nextInstanceId = 1

    ARENA_TIMER_NAME = "ArenaTimerEvent"
    SEND_ARENAS_INFO = "SendArenasInfoEvent"
    CREATE_LOBBY = "CreateLobbyEvent"
    ARENA_JOIN = "ArenaJoinEvent"
    GAME_ADMIN_START_ARENA_EVENT = "GameAdminStartArenaEvent"
    PLAYER_LEFT_EVENT = "PlayerLeftEvent"
    GAME_ADMIN_ENABLED_EVENT = "GameAdminEnabledEvent"
    GAME_ADMIN_BOOT_PLAYER_EVENT = "GameAdminBootPlayerEvent"
    PLAYER_SWITCH_TEAM_EVENT = "PlayerSwitchTeamEvent"
    PLAYER_READY_EVENT = "PlayerReadyEvent"
    GAME_STATS_EVENT = "GameStatsEvent"
    LOBBY_CHAT_EVENT = "LobbyChatEvent"
    GAME_ADMIN_CHANGE_ADMIN = "ChangeArenaAdminEvent"
    EQUIP_WEAPON_EVENT = "EquipWeaponEvent"
    AMMO_EVENT = "AmmoEvent"
    EQUIP_SHIELD_EVENT = "EquipShieldEvent"
    LOCK_ARENA_EVENT = "LockArenaEvent"
    ARENA_NO_WEAPONS_EVENT = "ArenaNoWeaponsEvent"
    ARENA_JOIN_FAILED_EVENT = "ArenaJoinFaliedEvent"
    
    #-------------------------------------------------------------------
    def __init__(self,arenaPtr,game):
        """constructor creates and initialized members"""

        self.__runningArenas = list()
        self.__teams = list()
        self.__zoneIndex = 0
        self.__running = False
        self.__runningClass = 0

        self.__arenaPtr = arenaPtr # this in the server
        self.__game = game
        
        # get some stuff from the arena object
        self.__maxBattleTime = GetSet.GetNumber( arenaPtr, ArenaIds.BATTLEDURATION )
        self.__zoneIndex = GetSet.GetNumber( arenaPtr, ArenaIds.ZONEINDEX )
        self.__channel = GetSet.GetNumber( arenaPtr, ArenaIds.CHANNEL )
        self.__arenaName = GetSet.GetString( arenaPtr, ArenaIds.NAME )
        self.__pointsToWin = GetSet.GetNumber( arenaPtr, ArenaIds.MAXSCORE ) 
        self.__minPlayers = GetSet.GetNumber( arenaPtr, ArenaIds.BATTLEMINIMUM ) 
        self.__minLevel = GetSet.GetNumber( arenaPtr, ArenaIds.LEVELRANGEMIN )
        self.__maxLevel = GetSet.GetNumber( arenaPtr, ArenaIds.LEVELRANGEMAX )
        if self.__minPlayers < 1:
            self.__minPlayers = 1
        self.__maxPlayers = GetSet.GetNumber( arenaPtr, ArenaIds.BATTLEMAXIMUM ) 
        if self.__maxPlayers < self.__minPlayers:
            self.__maxPlayers = self.__minPlayers + 1
            
        self.__minTeams = GetSet.GetNumber( arenaPtr, ArenaIds.MINTEAMS ) 
        if self.__minTeams < 1:
            self.__minTeams = 1
        self.__maxTeams = GetSet.GetNumber( arenaPtr, ArenaIds.MAXTEAMS ) 
        if self.__maxTeams < self.__minTeams:
            self.__maxTeams = self.__minTeams + 1
        
        # until get in object, scripting sets these
        self.__arenaType = ArenaBase.NOT_TEAMED_ARENA
        self.__aiBalanced = False
        
        className = GetSet.GetString( arenaPtr, ArenaIds.SCRIPTNAME )
        if len(className) > 0:
            try:
                i = className.find(".")
                if i >= 0:
                    modName = className[:i]
                    exec "import " + modName
                    self.__runningClass = eval( className )
                    try:
                        signupSettings = eval( className+".SignupSettings" )
                        if inspect.isfunction( signupSettings ):
                            (self.__arenaType, self.__aiBalanced) = signupSettings()
                        
                    except AttributeError:
                        pass
                else:
                    ParentHandler.LogMessage( gParent, KEP.WARN_LOG_LEVEL, "Script name is invalid, needs module name: "+str(className) )

            except NameError:
                ParentHandler.LogMessage( gParent, KEP.WARN_LOG_LEVEL, "Script name is invalid "+str(className) )
        else:
            ParentHandler.LogMessage( gParent, KEP.WARN_LOG_LEVEL, "Empty script name" )

        KEP.debugPrint( "name is ", self.__arenaName, "and class is", self.__runningClass, "id is", id(self))

    #-------------------------------------------------------------------
    def PlayerKilled( self, playerNetId, playerNetAssignedId, killerNetId, killerNetAssignedId, locX, locY, locZ ): 
        """Called when a player is killed. 

        Calls all the instances of running arenas
        """

        for running in self.__runningArenas:
            running.PlayerKilled( playerNetId, playerNetAssignedId, killerNetId, killerNetAssignedId, locX, locY, locZ )
            
    #-------------------------------------------------------------------
    def PlayerDisconnected( self, playerNetId):
        """called when a player is disconnected from the server.  

        call all the instances of running arenas"""

        for running in self.__runningArenas:
            if running.PlayerInArena(playerNetId):
                running.PlayerNetIdDisconnected( playerNetId )

    #-------------------------------------------------------------------
    def PlayerSignedup( self, playerNetId, gameInstanceId, famelevel):
        """a player has signed up for this arena

        This checks the player against the arena's configured level, clan, count, etc.
        restrictions to see if they can signup """
        import Game
        import WoK
        import ArenaBase
        
        playerJoined = False
        for run in self.__runningArenas:
            if gameInstanceId == run.channelId and len(run.players) < run.maxPlayers and run.locked == False:
                playerObj = Game.GetPlayerByNetId( self.__game, playerNetId )
                if playerObj:
                    name = GetSet.GetString( playerObj, PlayerIds.CHARNAME )
                    playerid = GetSet.GetNumber(playerObj, PlayerIds.HANDLE )
                    edb =  GetSet.GetNumber( playerObj, PlayerIds.CUREDBINDEX )
                    playerNetAssignedId = GetSet.GetNumber( playerObj, PlayerIds.NETWORKASSIGNEDID)
                    playerStats = ArenaBase.PlayerStats( name, edb )
                    playerStats.playerId = playerid
                    playerStats.famelevel = famelevel
                    newKey = ArenaBase.MakePlayerKey( playerNetId, playerNetAssignedId )
                    run.players[newKey] = playerStats
                    run._assignPlayerToTeam(newKey)
                    run.sendPlayerListsUpdate()
                    playerJoined = True
                break
        if(playerJoined == False):
            ev = Dispatcher.MakeEvent(KEP.dispatcher, ArenaSignupManager.ARENA_JOIN_FAILED_EVENT)
            Event.AddTo(ev, playerNetId)
            Dispatcher.QueueEvent(KEP.dispatcher, ev)
    #-------------------------------------------------------------------
    def PlayerSpawned( self, fromNetId, networkAssignedId, event, filter ):
        """pass the spawned event to all running arenas"""

        for running in self.__runningArenas:
            if running.PlayerInArena(fromNetId):
                if running.PlayerSpawned( fromNetId, networkAssignedId, event, filter ) != KEP.EPR_NOT_PROCESSED:
                    return KEP.EPR_OK

        return KEP.EPR_NOT_PROCESSED

    #-------------------------------------------------------------------
    def PlayerFired( self, fromNetId, networkAssignedId, event, filter ):
        """pass the fired event to all running arenas"""

        for running in self.__runningArenas:
            if running.PlayerInArena(fromNetId):
                if running.PlayerFired( fromNetId, networkAssignedId, event, filter ) != KEP.EPR_NOT_PROCESSED:
                    return KEP.EPR_OK

        return KEP.EPR_NOT_PROCESSED
    
    #-------------------------------------------------------------------
    def CreateLobby( self, name,  fromNetId, arenaKey, genre ):
        
        lobbyArena = self.__lobbyArena(name, fromNetId, arenaKey, genre)
        e = Dispatcher.MakeEvent(KEP.dispatcher, ArenaSignupManager.GAME_ADMIN_ENABLED_EVENT)
        Event.EncodeString(e, str(1))
        Event.AddTo(e, fromNetId)
        Dispatcher.QueueEvent(KEP.dispatcher, e)
        return lobbyArena
    
    #-------------------------------------------------------------------
    def PlayerDidDamage( self, fromNetId, event, filter ):
        """pass the damage event to all running arenas"""

        for running in self.__runningArenas:
            if running.PlayerInArena(fromNetId):
                if running.PlayerDidDamage( fromNetId, event, filter ) != KEP.EPR_NOT_PROCESSED:
                    return KEP.EPR_OK

        return KEP.EPR_NOT_PROCESSED
    
    #-------------------------------------------------------------------
    def PlayerInArena( self, fromNetId ):
        """check all running arenas to see if this player is in one"""

        for running in self.__runningArenas:
            if running.PlayerInArena( fromNetId ) != 0:
                return True

        return False

    #-------------------------------------------------------------------
    def ArenaManagerEvent(self, filter, event, fromNetId ):
        """pass the arena manager event to all running arenas"""
    
        for running in self.__runningArenas:
            if running.PlayerInArena(fromNetId):
                if running.ArenaManagerEvent( filter, event, fromNetId ):
                    return True

        return False
    
    #-------------------------------------------------------------------
    def ArenaReady( self, adminNetId, gameInstanceId ):
        
        ret = self.__startArena(gameInstanceId)

        # encode the return code
        return ret

        
    #-------------------------------------------------------------------
    def ArenaTimedOut(self,arenaId):
        """called by timer when a running arena times out"""

        for running in self.__runningArenas:
            if id(running) == arenaId:
                running.ArenaTimedOut()
            else:
                pass
            
    #-------------------------------------------------------------------
    # private methods
    #-------------------------------------------------------------------
    def __cloneForBattleAndReset(self):
        """copy the list of players to a running arena

        This is called when the arena starts to move the players from the
        signup list to the active arean
        """
        
        # get the new instance id
        newInstanceId = InstanceId.MakeId( InstanceId.GetId(self.__channel), ArenaSignupManager.nextInstanceId, KEP.CI_ARENA )     
        ArenaSignupManager.nextInstanceId = ArenaSignupManager.nextInstanceId + 1
        
        # clone
        newArena = 0
        newArena = self.__runningClass( self.__runningArenas, self.__game, self.__arenaPtr, self.__channel, newInstanceId, self.__zoneIndex, self.__maxBattleTime, self.__pointsToWin, gParent, gDispatcher )
        #newArena.minPlayers = self.__minPlayers

        return newArena

    #-------------------------------------------------------------------
    def __lobbyArena(self, name, adminNetId, arenaKey, genre):
        import Game
     
        lobbiedArena = self.__cloneForBattleAndReset()
        lobbiedArena._arenaName = name
        lobbiedArena.genre = genre
        if(name == "_debug"):
            lobbiedArena.minPlayersPerTeam = 0
            lobbiedArena.maxPlayersPerTeam = 4
        lobbiedArena._mapName = self.__arenaName
        playerObj = Game.GetPlayerByNetId( self.__game, adminNetId )
        if playerObj:
            playerNetAssignedId = GetSet.GetNumber( playerObj, PlayerIds.NETWORKASSIGNEDID)
            lobbiedArena.arenaAdmin = ArenaBase.MakePlayerKey( adminNetId, playerNetAssignedId )
            lobbiedArena.arenaKey = arenaKey
            self.__runningArenas.append( lobbiedArena )
        
        return lobbiedArena
    
    #-------------------------------------------------------------------
    def __startArena(self, gameInstanceId):
        """start an arena 

        This does so by 
                copying the people in the list
                creating a new instance id
                allocating them to the spawn points
                spawining them
        """
        
        
        for lobby in self.__runningArenas:
            if lobby.channelId == gameInstanceId:
                lobby.StartArena()
                break
                   
        return KEP.ArenaReady


    #-------------------------------------------------------------------
    # static event handler methods
    #-------------------------------------------------------------------
    @staticmethod
    def handleArenaSignupEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for the arena signup event

        This will add the player to the signup list"""
        arenaKey = Event.DecodeNumber( event )
        instanceId = Event.DecodeNumber( event )
        playerNetId = Event.DecodeNumber( event )
        kachinglvl = Event.DecodeNumber( event )
        if(playerNetId == 0):
            playerNetId = fromNetId
        
        Event.ResetForOutput( event )
        if ArenaSignupManager.arenas.has_key( arenaKey ):
            ArenaSignupManager.arenas[arenaKey].PlayerSignedup( playerNetId, instanceId, kachinglvl )

        return KEP.EPR_CONSUMED

    #-------------------------------------------------------------------
    @staticmethod
    def handleArenaReadyCheckEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for periodic arena ready event 

        This checks to see if criteria for starting has been met yet
        """

        arena = Event.DecodeObject( event )
        arenaKey = Event.DecodeNumber( event )
        
        if not ArenaSignupManager.arenas.has_key( arenaKey ):
            ArenaSignupManager.arenas[arenaKey] = ArenaSignupManager(arena,Dispatcher.GetGame( dispatcher )) # create a new one
            # get the type of arena this is
            
        ret = False #ArenaSignupManager.arenas[arenaKey].ArenaReady(timeSinceStart,minSignupTimeMs)
        
        Event.ResetForOutput( event )
        Event.EncodeNumber( event, ret )

        return KEP.EPR_CONSUMED
        
    #-------------------------------------------------------------------
    @staticmethod
    def handleDeathEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for player death event

        Calls all the instances of ArenaSignupManagers
        """

        zoneIndex = Event.DecodeNumber( event )
        playerNetId = Event.DecodeNumber( event )
        playerNetAssignedId = Event.DecodeNumber( event )
        killerNetId = Event.DecodeNumber( event )
        killerNetAssignedId = Event.DecodeNumber( event )
        
        locX = Event.DecodeNumber( event )
        locY = Event.DecodeNumber( event )
        locZ = Event.DecodeNumber( event )
        
        for arena in ArenaSignupManager.arenas:
            ArenaSignupManager.arenas[arena].PlayerKilled( playerNetId, playerNetAssignedId, killerNetId, killerNetAssignedId, locX, locY, locZ )
        
        return KEP.EPR_OK

    #-------------------------------------------------------------------
    @staticmethod
    def handleDisconnectedEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for player disconnected event

        Calls all the instances of ArenaSignupManagers
        """

        playerNetId = Event.DecodeNumber( event )

        for arena in ArenaSignupManager.arenas:
            ArenaSignupManager.arenas[arena].PlayerDisconnected( playerNetId )

        return KEP.EPR_OK
    #-------------------------------------------------------------------
    @staticmethod
    def handlePlayerLeftEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for player disconnected event

        Calls all the instances of ArenaSignupManagers
        """
        for arena in ArenaSignupManager.arenas:
            ArenaSignupManager.arenas[arena].PlayerDisconnected( fromNetId )

        return KEP.EPR_OK
    
    #-------------------------------------------------------------------
    @staticmethod
    def handlePlayerReadyEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for player disconnected event

        Calls all the instances of ArenaSignupManagers
        """

        for arena in ArenaSignupManager.arenas:
            for run in ArenaSignupManager.arenas[arena].__runningArenas:
                if run.PlayerInArena( fromNetId ):
                    run.PlayerReady( fromNetId )
        return KEP.EPR_OK
    
    #-------------------------------------------------------------------
    @staticmethod
    def handlePlayerSwitchTeamEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for player disconnected event

        Calls all the instances of ArenaSignupManagers
        """

        for arena in ArenaSignupManager.arenas:
            for lobby in ArenaSignupManager.arenas[arena].__runningArenas:
                if lobby.PlayerInArena( fromNetId ):
                    lobby.PlayerSwitchTeams( fromNetId )

        return KEP.EPR_OK
    
    #-------------------------------------------------------------------
    @staticmethod
    def handleArenaTimerEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for arena timer event

        Calls all the instances of ArenaSignupManagers
        """
                
        runningArenaId = Event.DecodeNumber( event )
        
        for arena in ArenaSignupManager.arenas:
            ArenaSignupManager.arenas[arena].ArenaTimedOut( runningArenaId )

        return KEP.EPR_OK
        
    #-------------------------------------------------------------------
    @staticmethod
    def handlePlayerSpawnedEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for player spawn event

        Calls all the instances of ArenaSignupManagers
        """

        networkAssignedId = Event.DecodeNumber( event )
        for arena in ArenaSignupManager.arenas:
            if ArenaSignupManager.arenas[arena].PlayerSpawned( fromNetId, networkAssignedId, event, filter ) != KEP.EPR_NOT_PROCESSED :
                return KEP.EPR_OK 

        return KEP.EPR_OK
        
    #-------------------------------------------------------------------
    @staticmethod
    def handleWeaponFiredEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for player fires a missle weapon event

        Calls all the instances of ArenaSignupManagers
        """

        networkAssignedId = Event.DecodeNumber( event )
        for arena in ArenaSignupManager.arenas:
            if ArenaSignupManager.arenas[arena].PlayerFired( fromNetId, networkAssignedId, event, filter ) != KEP.EPR_NOT_PROCESSED :
                return KEP.EPR_OK 

        return KEP.EPR_OK
        
    
    #-------------------------------------------------------------------
    @staticmethod
    def handleWeaponDamagedEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for player hits another player with a missle weapon event
        
        Calls all the instances of ArenaSignupManagers
        """

        for arena in ArenaSignupManager.arenas:
            if ArenaSignupManager.arenas[arena].PlayerDidDamage( fromNetId, event, filter ) != KEP.EPR_NOT_PROCESSED :
                return KEP.EPR_OK 

        return KEP.EPR_OK
    
    #-------------------------------------------------------------------
    @staticmethod
    def handleChangeAdminEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for player hits another player with a missle weapon event
        
        Calls all the instances of ArenaSignupManagers
        """
        
        gameInstanceId = Event.DecodeNumber(event)
        playerKey = Event.DecodeNumber(event)
        
        for arena in ArenaSignupManager.arenas:
            for lobby in ArenaSignupManager.arenas[arena].__runningArenas:
                if lobby.channelId == gameInstanceId:
                    if ArenaBase.KeyNetIdEqNetId(lobby.arenaAdmin, fromNetId)and playerKey in lobby.players:
                        
                        lobby.ChangeArenaAdmin(playerKey)
                        e = Dispatcher.MakeEvent(KEP.dispatcher, ArenaSignupManager.GAME_ADMIN_ENABLED_EVENT)
                        Event.EncodeString(e, str(0))
                        Event.AddTo(e, fromNetId)
                        Dispatcher.QueueEvent(KEP.dispatcher, e)
                        
        return KEP.EPR_OK
          
    
    @staticmethod
    def handleAmmoEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for get attribute event
        """
        maxCharge = Event.DecodeNumber( event )
        curCharge = Event.DecodeNumber( event )
        glid = Event.DecodeNumber( event )
        
        for arena in ArenaSignupManager.arenas:
            for run in ArenaSignupManager.arenas[arena].__runningArenas:
                for player in run.players:
                     if ArenaBase.KeyNetIdEqNetId( player, fromNetId ):
                        run.PlayerUpdateAmmo(fromNetId, player, maxCharge, curCharge, glid)
                        break
        
        
        return KEP.EPR_OK

    @staticmethod
    def handleArenaManagerEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """handler for arena mgr event
        
        Calls all the instances of ArenaSignupManagers
        """
        ARENA_RESPAWN_FILTER = 6
        ARENA_LOBBY_CHAT_FILTER = 7

        playerNetId = 0
        if filter == ARENA_RESPAWN_FILTER:
            playerNetId = Event.DecodeNumber( event )
        else:
            playerNetId = fromNetId

        for arena in ArenaSignupManager.arenas:
            for run in ArenaSignupManager.arenas[arena].__runningArenas:
                for player in run.players:
                     if ArenaBase.KeyNetIdEqNetId( player, playerNetId ):
                        if run.ArenaManagerEvent(filter, event, playerNetId):
                            return KEP.EPR_CONSUMED
        
        return KEP.EPR_NOT_PROCESSED

    @staticmethod
    def handleArenaEjectEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """hanlder when player removed from arena w/o disconnect or death
        
        Calls all the instances of ArenaSignupManagers
        """ 
        
        
        gameInstanceId = Event.DecodeNumber( event )
        playerKey = Event.DecodeNumber( event )
        
        for arena in ArenaSignupManager.arenas:
            for lobby in ArenaSignupManager.arenas[arena].__runningArenas:
                if lobby.channelId == gameInstanceId and playerKey in lobby.players:
                    lobby.BootPlayer(playerKey)
                    

        return KEP.EPR_OK

    @staticmethod
    def handleGameStatsEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """sends GameStats to client
        """
        for arena in ArenaSignupManager.arenas:
            for run in ArenaSignupManager.arenas[arena].__runningArenas:
                for player in run.players:
                     if ArenaBase.KeyNetIdEqNetId( player, fromNetId ):
                        run.sendGameStatsToPlayerNetId(fromNetId, filter)

                        break
        return KEP.EPR_OK
     #-------------------------------------------------------------------
    @staticmethod
    def handleCreateLobbyEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
       
        name = Event.DecodeString( event )
        arenaKey = Event.DecodeNumber( event )
        genre = Event.DecodeString( event )
        kachinglvl = Event.DecodeNumber( event )
        
        for arena in ArenaSignupManager.arenas:
            if(arena == arenaKey):
                lobbiedArena = ArenaSignupManager.arenas[arena].CreateLobby(name, fromNetId, arenaKey, genre)
                ArenaSignupManager.arenas[arena].PlayerSignedup(fromNetId, lobbiedArena.channelId, kachinglvl)

        return KEP.EPR_CONSUMED
    
     #-------------------------------------------------------------------
    @staticmethod
    def handleArenaAdminStart(dispatcher, fromNetId, event, eventid, filter, objectid):
        
        arenaKey = Event.DecodeNumber( event )
        gameInstanceId = Event.DecodeNumber( event )
        
        for arena in ArenaSignupManager.arenas:
            if(arena == arenaKey):
                lobbiedArena = ArenaSignupManager.arenas[arena].ArenaReady(fromNetId, gameInstanceId)  
        
        return KEP.EPR_CONSUMED
    
    #-------------------------------------------------------------------
    @staticmethod
    def handleEquipWeaponEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        
        for arena in ArenaSignupManager.arenas:
            for running in ArenaSignupManager.arenas[arena].__runningArenas:
                for playerKey in running.players:
                    if(ArenaBase.KeyNetIdEqNetId( playerKey, fromNetId )):
                        running.EquipWeapon(filter, playerKey, fromNetId)
                        break  
        
        return KEP.EPR_CONSUMED
    
    #-------------------------------------------------------------------
    @staticmethod
    def handleEquipShieldEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        
        pKey = Event.DecodeString( event )
        toEquip = Event.DecodeNumber( event )
        delay = Event.DecodeNumber( event )
              
        for arena in ArenaSignupManager.arenas:
            for running in ArenaSignupManager.arenas[arena].__runningArenas:
                for playerKey in running.players:
                    if( ArenaBase.NetIdFromKey(playerKey) == ArenaBase.NetIdFromKey(int(pKey))):
                        running.EquipShield(playerKey, ArenaBase.NetIdFromKey(int(pKey)), toEquip, delay)
                        break  
        
        return KEP.EPR_CONSUMED
    
    #-------------------------------------------------------------------
    @staticmethod
    def encodeAllArenasInfo(filter, event):
        if(filter == 1):
            #ArenaSignupManager.arenas - arenas currently in menu
            
            arenaNum = len(ArenaSignupManager.arenas)
            Event.EncodeString(event, str(arenaNum))
            for arena in ArenaSignupManager.arenas:
                Event.EncodeString(event, ArenaSignupManager.arenas[arena].__arenaName)
                Event.EncodeNumber(event, arena)
                Event.EncodeNumber(event, len(ArenaSignupManager.arenas[arena].__runningArenas))
                for run in ArenaSignupManager.arenas[arena].__runningArenas:
                    Event.EncodeString(event, run._arenaName)
                    Event.EncodeString(event, run._getArenaAdminName())
                    Event.EncodeNumber(event, run.channelId)
                    Event.EncodeNumber(event, len(run.players))
                    Event.EncodeNumber(event, run.maxPlayers)
                    Event.EncodeString(event, run._status)
                    Event.EncodeNumber(event, int(run.locked))
                    Event.EncodeString(event, run.genre)
                    Event.EncodeNumber(event, run._getLeaderScore())
                    Event.EncodeNumber(event, run._getTimeRemaining())
        elif(filter == 2):
            #ArenaSignupManager.arenas - arenas currently in menu
            arenaNum = len(ArenaSignupManager.arenas)
            Event.EncodeString(event, str(arenaNum))
            for arena in ArenaSignupManager.arenas:
                Event.EncodeString(event, ArenaSignupManager.arenas[arena].__arenaName)
                Event.EncodeNumber(event, arena)
        
        return event
    
    #-------------------------------------------------------------------
    @staticmethod
    def handleArenaLockEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
         for arena in ArenaSignupManager.arenas:
           for run in ArenaSignupManager.arenas[arena].__runningArenas:
               if(run.PlayerIsAdmin(fromNetId)):
                   run.LockArena(filter)
    
         return KEP.EPR_OK  
     
    #-------------------------------------------------------------------
    @staticmethod
    def handleArenaNoWeaponsEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
         for arena in ArenaSignupManager.arenas:
           for run in ArenaSignupManager.arenas[arena].__runningArenas:
               if(run.PlayerIsAdmin(fromNetId)):
                   run.SetNoWeapons(filter)
    
         return KEP.EPR_OK           
             
    #-------------------------------------------------------------------
    @staticmethod
    def handleSendAllArenasInfo(dispatcher, fromNetId, event, eventid, filter, objectid):
        ArenaSignupManager.RemoveBrokenArenas()
        Event.ResetForOutput( event )
        ev = ArenaSignupManager.encodeAllArenasInfo(filter, event)
        Event.AddTo(ev, fromNetId)
        Dispatcher.QueueEvent(dispatcher, ev)
         
        return KEP.EPR_CONSUMED
    #-------------------------------------------------------------------
    @staticmethod
    def RemoveBrokenArenas():
        for arena in ArenaSignupManager.arenas:
            for run in ArenaSignupManager.arenas[arena].__runningArenas:
                if run._getTimeRemaining() < 0 or run._hasArenaEnded == True:
                    Dispatcher.LogMessage(KEP.dispatcher, KEP.DEBUG_LOG_LEVEL, "ArenaManager forcefully removed arena because it was broken.")
                    ArenaSignupManager.arenas[arena].__runningArenas.remove(run)
                        

    #-------------------------------------------------------------------
    @staticmethod
    def handleShutdownEvent(dispatcher, fromNetId, event, eventid, filter, objectid):
        """callback the server is shutting down

        cleans up all the ArenaSignupManagers
        """ 

        ArenaSignupManager.arenas.clear()

        import gc
        gc.collect()

        return KEP.EPR_OK
    
    
#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, handler, dbgLevel ):
    """Called from C to initialize all the handlers in this script"""    
    
    global gParent
    global gDispatcher

    gParent = handler
    gDispatcher = dispatcher

    # get the game API 
    try:
        callable( Game.GetPlayerByNetId )
    except NameError:
        if not Dispatcher.GetFunctions( gDispatcher, KEP.GET_SERVER_FN ):
            Dispatcher.LogMessage( dispatcher, KEP.WARN_LOG_LEVEL,  "Python ArenaManager couldn't load server Game functions!!" )
            return False
        import Game

        # one more check        
        try:
            callable(Game.GetPlayerByNetId)
        except NameError:
            return False
        
    # get the arena API 
    try:
        callable( Arena.GetSpawnPointCount)
    except NameError:
        Dispatcher.GetFunctions( gDispatcher, KEP.GET_ARENA_FN )
        import Arena
        
        try:
            callable(Arena.GetSpawnPointCount)
        except NameError:
            Dispatcher.LogMessage( dispatcher, KEP.WARN_LOG_LEVEL,  "Python ArenaManager couldn't load server Arena functions!!" )
            return
    
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleDeathEvent,           1,0,0,0, "DeathEvent", KEP.HIGH_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleDisconnectedEvent,    1,0,0,0, "DisconnectedEvent", KEP.HIGH_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleArenaTimerEvent,      1,0,0,0, ArenaSignupManager.ARENA_TIMER_NAME, KEP.HIGH_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleArenaReadyCheckEvent, 1,0,0,0, "ArenaReadyCheck", KEP.MED_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleArenaManagerEvent,   1,0,0,0, "ArenaManagerEvent", KEP.MED_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleArenaEjectEvent,   1,0,0,0, "ArenaEjectEvent", KEP.HIGH_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleShutdownEvent,   1,0,0,0, "ShutdownEvent", KEP.MED_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleSendAllArenasInfo,   1,0,0,0, ArenaSignupManager.SEND_ARENAS_INFO, KEP.MED_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleCreateLobbyEvent,     1,0,0,0, ArenaSignupManager.CREATE_LOBBY,  KEP.MED_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleArenaSignupEvent,     1,0,0,0, ArenaSignupManager.ARENA_JOIN,  KEP.MED_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleArenaAdminStart,     1,0,0,0, ArenaSignupManager.GAME_ADMIN_START_ARENA_EVENT,  KEP.MED_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handlePlayerLeftEvent,    1,0,0,0, ArenaSignupManager.PLAYER_LEFT_EVENT, KEP.HIGH_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleArenaEjectEvent,    1,0,0,0, ArenaSignupManager.GAME_ADMIN_BOOT_PLAYER_EVENT, KEP.HIGH_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handlePlayerSwitchTeamEvent,    1,0,0,0, ArenaSignupManager.PLAYER_SWITCH_TEAM_EVENT, KEP.HIGH_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handlePlayerReadyEvent,    1,0,0,0, ArenaSignupManager.PLAYER_READY_EVENT, KEP.HIGH_PRIO ) 
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleGameStatsEvent,    1,0,0,0, ArenaSignupManager.GAME_STATS_EVENT, KEP.HIGH_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleChangeAdminEvent,    1,0,0,0, ArenaSignupManager.GAME_ADMIN_CHANGE_ADMIN, KEP.HIGH_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleEquipWeaponEvent,    1,0,0,0, ArenaSignupManager.EQUIP_WEAPON_EVENT, KEP.HIGH_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleAmmoEvent,    1,0,0,0, ArenaSignupManager.AMMO_EVENT, KEP.HIGH_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleEquipShieldEvent,    1,0,0,0, ArenaSignupManager.EQUIP_SHIELD_EVENT, KEP.HIGH_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleArenaLockEvent,    1,0,0,0, ArenaSignupManager.LOCK_ARENA_EVENT, KEP.HIGH_PRIO )
    ParentHandler.RegisterEventHandler( dispatcher, handler,  ArenaSignupManager.handleArenaNoWeaponsEvent,    1,0,0,0, ArenaSignupManager.ARENA_NO_WEAPONS_EVENT, KEP.HIGH_PRIO )
    
    # filtered events
    ARENA_CHANNEL_MASK = 0x50000000;
    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler,  ArenaSignupManager.handleWeaponFiredEvent,   1,0,0,0, "WeaponFiredEvent", ARENA_CHANNEL_MASK, KEP.FILTER_IS_MASK, KEP.NO_OBJID_FILTER, KEP.MED_PRIO ) 
    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler,  ArenaSignupManager.handleWeaponDamagedEvent,   1,0,0,0, "WeaponDamagedEvent", ARENA_CHANNEL_MASK, KEP.FILTER_IS_MASK, KEP.NO_OBJID_FILTER, KEP.MED_PRIO ) 
    ParentHandler.RegisterFilteredEventHandler( dispatcher, handler,  ArenaSignupManager.handlePlayerSpawnedEvent,   1,0,0,0, "PlayerSpawnedEvent", ARENA_CHANNEL_MASK, KEP.FILTER_IS_MASK, KEP.NO_OBJID_FILTER, KEP.MED_PRIO )
    
    return True


#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, parent, dbgLevel ):
    """Called from C to initialize all the events"""

    global gParent
    global gDispatcher

    #import gc
    #gc.set_debug( gc.DEBUG_LEAK )
    
    # call the c funtion to register our event, we have just one
    gParent = parent
    gDispatcher = dispatcher
    KEP.debugLevel = dbgLevel
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.GAME_ADMIN_BOOT_PLAYER_EVENT, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.GAME_ADMIN_ENABLED_EVENT, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.PLAYER_LEFT_EVENT, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.GAME_ADMIN_START_ARENA_EVENT, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.SEND_ARENAS_INFO, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.CREATE_LOBBY, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.ARENA_JOIN, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.PLAYER_SWITCH_TEAM_EVENT, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.PLAYER_READY_EVENT, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.GAME_ADMIN_CHANGE_ADMIN, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.EQUIP_WEAPON_EVENT, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.AMMO_EVENT, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.LOCK_ARENA_EVENT, KEP.HIGH_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, "ArenaManagerEvent", KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.ARENA_JOIN_FAILED_EVENT, KEP.MED_PRIO )
    Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.ARENA_NO_WEAPONS_EVENT, KEP.MED_PRIO )
    ret = Dispatcher.RegisterEvent( dispatcher, 1,0,0,0, ArenaSignupManager.ARENA_TIMER_NAME, KEP.HIGH_PRIO )

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """ called from C to give s a chance to cleanup before shutdown or re-init"""
    
    ret = KEP.getDependentModules(__name__)

    # RuntimeError: dictionary changed size during iteration
    #for arena in ArenaSignupManager.arenas:
    #    del ArenaSignupManager.arenas[arena]

    ArenaSignupManager.arenas.clear()        
    # KEP.debugPrint( "arenas now has", len(ArenaSignupManager.arenas), len(ArenaSignupManager.arenas.keys()) )
    return ret

        


