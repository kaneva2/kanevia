#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP
import xml.dom.minidom

# C functions
import ServerGlobals
import InstanceId
import Dispatcher
import Event
import GetSet

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    """Generic event handler interface function"""
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "FameEarnedEvent", KEP.MED_PRIO)
 #   Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "PlayerRavedEvent", KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "ClientTickler", KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "FameLevelUpEvent", KEP.MED_PRIO)
    Dispatcher.RegisterEvent(dispatcher, 1, 0, 0, 0, "FameBadgeAwardedEvent", KEP.MED_PRIO)
    
#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    """Generic event handler interface function
    called from C to give us a chance to cleanup before shutdown or re-init"""
    return KEP.getDependentModules(__name__)
