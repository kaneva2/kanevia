#
# Copyright Kaneva 2001-2020
# This file is licensed under the PolyForm Noncommercial License 1.0.0
# https://polyformproject.org/licenses/noncommercial/1.0.0/ 
#

#import helper Python code
import KEP

# C functions
import PlayerIds
import Dispatcher
import Event
import GetSet
import ParentHandler
import GameGlobals
import InstanceId


#-------------------------------------------------------------------
def handleValidateUser( dispatcher, fromNetId, event, eventid, filter, objectid):
    """
        called when player is modified or created to allow scripting to 
        check to make sure the player is validated

        the player is a GetSet object, so you can have at it!

        fromNetId is player's from netid
        
    """
    import Game

    player = Event.DecodeObject( event )

    edb = GetSet.GetNumber( player, PlayerIds.ORIGINALEDBCFG)
    name = GetSet.GetString( player, PlayerIds.CHARNAME )
    
    # TEST CODE JUST LOGS. THIS DEMONSTRATES HOW TO DECODE THE EVENT
    # TEST CODE JUST LOGS. THIS DEMONSTRATES HOW TO DECODE THE EVENT
    # TEST CODE JUST LOGS. THIS DEMONSTRATES HOW TO DECODE THE EVENT
    # TEST CODE JUST LOGS. THIS DEMONSTRATES HOW TO DECODE THE EVENT
    # TEST CODE JUST LOGS. THIS DEMONSTRATES HOW TO DECODE THE EVENT

    # get the dim config info
    itemCount = Event.DecodeNumber( event )
    colorCount = Event.DecodeNumber( event )
    
    Dispatcher.LogMessage( dispatcher, KEP.DEBUG_LOG_LEVEL, "Player validated "+name+" has "+str(int(itemCount))+" items and "+str(int(colorCount))+" colors" )
    Dispatcher.LogMessage( dispatcher, KEP.DEBUG_LOG_LEVEL, "  EDB is "+str(int(edb)) )

    for i in range (0,int(itemCount)):
        equipId = Event.DecodeNumber( event )
        slotCount = Event.DecodeNumber( event )
        Dispatcher.LogMessage( dispatcher, KEP.DEBUG_LOG_LEVEL, "    Id "+str(int(equipId))+" has "+str(int(slotCount))+" slots" )
        for j in range (0,int(slotCount)):
            mesh = Event.DecodeNumber( event )
            mat = Event.DecodeNumber( event )
            Dispatcher.LogMessage( dispatcher, KEP.DEBUG_LOG_LEVEL, "      Slot "+str(int(j))+" mesh = "+str(int(mesh))+" mat = "+str(int(mat)) )

    # colors only come down if >= 0
    for i in range (0,int(colorCount)):
        equipId = Event.DecodeNumber( event )
        slotCount = Event.DecodeNumber( event )
        rColor = Event.DecodeNumber( event )
        gColor = Event.DecodeNumber( event )
        bColor = Event.DecodeNumber( event )

   
    # return non zero to ztop char creation
    # the server DOES NOTHING if you return non-zero
    # if you want to tell client, or log a message you must do it
    
    Event.SetFilter( event, 0 )  
    
    return KEP.EPR_OK


#-------------------------------------------------------------------
def InitializeKEPEventHandlers( dispatcher, parent, dbgLevel ):
    ParentHandler.RegisterEventHandler( dispatcher, parent, handleValidateUser, 1,0,0,0, "ValidateNewPlayer", KEP.MED_PRIO )
    return  True 

#-------------------------------------------------------------------
def InitializeKEPEvents( dispatcher, handler, dbgLevel ):
    pass    

#-------------------------------------------------------------------
def ReinitalizeKEPEvents( dispatcher, parent, dbgLevel ):
    " called from C to give us a chance to cleanup before shutdown or re-init"
    return KEP.getDependentModules(__name__)
