--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_RandomVendor.lua
-- 
-- Displays the random loot vendor menu (by the ashmeister)
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_DragDropHelper.lua")
dofile("Framework_ActorHelper.lua")
dofile("Framework_MenuShadowHelper.lua")
----------------------
-- Constants
----------------------
local TOOLTIP_TABLE = {header = "name"}

local COLORS_VALUES =	{	["Common"] = {a = 255, r = 255, g = 255, b = 255},
							["Rare"] = {a = 255, r = 0, g = 150, b = 255},
							["Very Rare"] = {a = 255, r = 174, g = 0, b = 255},
							["Extremely Rare"] = {a = 255, r = 255, g = 144, b = 0},
							["Never"] = {a = 255, r = 255, g = 144, b = 0}
						}

local ITEM_LEVEL_COORDS = {
							{left = 0, right = 15, top = 146, bottom = 161},
							{left = 16, right = 31, top = 146, bottom = 161},
							{left = 32, right = 47, top = 146, bottom = 161},
							{left = 0, right = 15, top = 162, bottom = 177},
							{left = 16, right = 31, top = 162, bottom = 177}
}

local ITEM_RARITY_BG = {	["Common"] = {left = 208, right = 292, top = 167, bottom = 251},
							["Rare"] = {left = 293, right = 377, top = 167, bottom = 251},
							["Very Rare"] = {left = 378, right = 462, top = 167, bottom = 251},
							["Extremely Rare"] = {left = 378, right = 462, top = 82, bottom = 166},
							["Never"] = {left = 378, right = 462, top = 82, bottom = 166}
						}

local ITEM_LEVEL_RARITY_OFFSET = {["Common"] = 0, ["Rare"] = 48, ["Very Rare"] = 96, ["Extremely Rare"] = 144}
local EMPTY_ITEM = {UNID = 0, properties = {name = "EMPTY", GLID = 0, itemType = "generic"}, count = 0}

----------------------------
-- Global Variables
----------------------------
local g_displayTable = {}
local g_lootTable = {}
local g_randLoot = {}

----------------------------
-- Local Variables
----------------------------
local m_inventory = {}
local m_inventoryCounts = {}
local m_tradePaid = false
local m_tradeRemoveTable = nil
local m_tradeAddTable = nil
local takeState = false
local resetState = false
local m_spendItem = nil
local m_recieveItem = nil
local m_itemReceived = false
local m_openSlot = false
m_vendorPID = nil
local m_gameItems = nil
local m_lootItem = nil

----------------------------
-- Local Functions
----------------------------
local setCounts -- ()
local updateItemContainer -- ()
local formatTooltips -- ()
local formatNumberSuffix -- ()
local checkIsBackpackFull -- ()

----------------------------
-- Create Method
----------------------------
function onCreate()
	CloseAllActorMenus()
	KEP_EventRegisterHandler("updateVendorClientFull", "UPDATE_VENDOR_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "clientResizeEventHandler", "ClientResizeEvent", KEP.LOW_PRIO )

	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)
	Events.registerHandler("UPDATE_VENDOR_HEADER", updateHeader)
	Events.registerHandler("UPDATE_RANDOM_VENDOR_LOOT", updateRandomVendorLoot)

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()
	InventoryHelper.initializeInventory("g_displayTable", true, 1)
	-- TODO: Fix tooltop issue ..
	-- InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")

	MenuSetLocationThis(84, 145)
	resizeDropShadow( )

	takeState = false  -- State to know if in first screen or second

	-- Request player's inventory
	requestInventory(GAME_PID)
	requestInventory(INVENTORY_PID)
	Control_SetEnabled(gHandles["btnPayORTake"], false)
	Control_SetVisible(gHandles["stcBackpackFull"], false)

	Control_SetVisible(gHandles["btnQuit"], false)
	Control_SetEnabled(gHandles["btnQuit"], false)
	Control_SetVisible(gHandles["btnTryAgain"], false)
	Control_SetEnabled(gHandles["btnTryAgain"], false)

	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")				
	end

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "player")
	KEP_EventQueue(ev)
end

function onDestroy()
	InventoryHelper.destroy()
	Events.sendEvent("FRAMEWORK_CLOSED_VENDOR", {PID = m_vendorPID})
end

-- Called upon screen render
function Dialog_OnRender(dialogHandle, elapsedSec)
end

----------------------------
-- Event Handlers
----------------------------
-- Called when a new container inventory is received.  Updates all items and re-displays.
function updateVendorClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local returnedPID = KEP_EventDecodeNumber(tEvent)
	if m_vendorPID == nil then
		m_vendorPID = returnedPID
	elseif m_vendorPID ~= returnedPID then
		return
	end

	m_itemReceived = true
	
	-- Turn on the item name
	Control_SetVisible(gHandles["stcItem1"], true)


	m_spendItem = decompileInventory(tEvent)


	m_tradeRemoveTable = {}
	local removeDetails = {UNID = m_spendItem.input, count = m_spendItem.cost or 0}
	table.insert(m_tradeRemoveTable, removeDetails)

	updateInventory()
	updateItemContainer()
	enablePayment()
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if m_inventory[updateIndex] then
		m_inventory[updateIndex] = updateItem
		updateInventory()
		updateItemContainer()
		setCounts()
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_inventory = decompileInventory(tEvent)
	updateInventory()
	updateItemContainer()
	setCounts()
	enablePayment()
end

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	log("--- Framework_GameItemEditor updateGameClientFull")
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
end


function closeVendor(event)
	if not MenuIsOpen("UnifiedInventory") then
		MenuCloseThis()
	end
end

function updateHeader(event)
	local header = event.header
	local name = event.name
	m_vendorPID = event.PID
	
	Static_SetText(gHandles["stcDescription"], header)
	if name and name ~= "" then
		Static_SetText(gHandles["Menu_Title"], name)
	end

	local glid = event.GLID or 4600898
	local imgHandle = Image_GetDisplayElement(gHandles["imgItem2"])
	if imgHandle and gDialogHandle then
		KEP_LoadIconTextureByID(tonumber(glid), imgHandle, gDialogHandle)
	end
end

function updateRandomVendorLoot(event)
		m_lootItem = deepCopy(event.lootTable)
		
		-- Create new item purchased tables


		
		m_tradeAddTable = {}
		local newItem = deepCopy(m_lootItem)

		--printTable(lootedItem)
		-- Assign a loot source for metric detection
		newItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "Vendor"}
		newItem.count = m_lootItem.count or 1
		newItem.randomizeDurability = true
		table.insert(m_tradeAddTable, newItem)
		

		enablePayment()
end

function btnPayORTake_OnButtonClicked(buttonHandle)
	spinTheWheel()
end

function btnQuit_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

function btnTryAgain_OnButtonClicked(buttonHandle)
	if resetState == false then
		resetState = true
		resetRandomVendor()
	end
end

function resetRandomVendor()
	g_lootTable = {}

	-- Update elements
	InventoryHelper.initializeInventory("g_displayTable", true, 1)

	takeState = false

	Control_SetVisible(gHandles["btnPayORTake"], true)
	Control_SetEnabled(gHandles["btnPayORTake"], false)

	-- Control_SetVisible(gHandles["stcBackpackFull"], false)

	-- Elements to hide
	Control_SetVisible(gHandles["btnQuit"], false)
	Control_SetEnabled(gHandles["btnQuit"], false)
	Control_SetVisible(gHandles["btnTryAgain"], false)
	Control_SetEnabled(gHandles["btnTryAgain"], false)
	Control_SetVisible(gHandles["imgIconBorder3"], false)
	Control_SetVisible(gHandles["imgIconBG3"], false)
	Control_SetVisible(gHandles["stcYouRecieved"], false)
	Control_SetEnabled(gHandles["imgIconBorder3"], false)
	Control_SetEnabled(gHandles["imgIconBG3"], false)
	Control_SetEnabled(gHandles["stcYouRecieved"], false)
	Control_SetVisible(gHandles["stcItemCount1"], false)
	Control_SetEnabled(gHandles["stcItemCount1"], false)
	Static_SetText(gHandles["stcItemCount1"], "")
	Control_SetVisible(gHandles["imgItemCountBG1"], false)
	Control_SetEnabled(gHandles["imgItemCountBG1"], false)
	Control_SetVisible(gHandles["stcSorry"], false)
	Control_SetVisible(gHandles["stcLose"], false)
	
	-- Elements to show
	Control_SetVisible(gHandles["imgIconBorder2"], true)
	Control_SetVisible(gHandles["imgIconBG2"], true)
	Control_SetVisible(gHandles["imgDivider"], true)
	Control_SetVisible(gHandles["imgItem2"], true)
	Control_SetVisible(gHandles["btnItem2"], true)
	Control_SetVisible(gHandles["stcDescription"], true)
	Control_SetVisible(gHandles["stcBackpackCount"], true)
	Control_SetVisible(gHandles["stcCost"], true)
	Control_SetVisible(gHandles["stcCostCount"], true)
	Control_SetEnabled(gHandles["imgIconBorder2"], true)
	Control_SetEnabled(gHandles["imgIconBG2"], true)
	Control_SetEnabled(gHandles["imgDivider"], true)
	Control_SetEnabled(gHandles["imgItem2"], true)
	Control_SetEnabled(gHandles["btnItem2"], true)
	Control_SetEnabled(gHandles["stcDescription"], true)
	Control_SetEnabled(gHandles["stcBackpackCount"], true)
	Control_SetEnabled(gHandles["stcCost"], true)
	Control_SetEnabled(gHandles["stcCostCount"], true)

	-- Elements to show: input item box
	Control_SetVisible(gHandles["imgItemBG1"], true)
	Control_SetVisible(gHandles["imgItem1"], true)
	Control_SetVisible(gHandles["imgItemCountBG1"], true)
	Control_SetVisible(gHandles["imgItemName1"], true)
	Control_SetVisible(gHandles["stcItem1"], false) -- Don't enable until name is updated
	Control_SetVisible(gHandles["imgItemOverlay1"], true)
	Control_SetVisible(gHandles["btnItem1"], true)

	-- Elements to move
	Control_SetLocation(gHandles["imgItemBG1"], 160,265)
	Control_SetLocation(gHandles["imgItem1"],161,266)
	Control_SetLocation(gHandles["imgItemCountBG1"], 223,265)
	Control_SetSize(gHandles["imgItemCountBG1"], 20, 15)
	Control_SetLocation(gHandles["stcItemCount1"],223,265)
	Control_SetSize(gHandles["stcItemCount1"], 20, 15)
	Control_SetLocation(gHandles["imgItemLevel1"],228,312)
	Control_SetLocation(gHandles["imgItemName1"],161,334)
	Control_SetLocation(gHandles["stcItem1"],163,334)
	Control_SetLocation(gHandles["imgItemOverlay1"],161,334)
	Control_SetLocation(gHandles["btnItem1"], 161, 266)

	-- Get new loot
	m_itemReceived = false
	Events.sendEvent("VENDOR_RESET", {PID = m_vendorPID})

	m_tradePaid = false
	m_openSlot = false

		-- Reset required input item
	updateItemContainer()

end

-- Receive random loot (generated by Class_Vendor on create)
function spinTheWheel()
	if not m_tradePaid then
		m_tradePaid = true
		
		updateInventory()
	
		resetState = false
		takeState = true

		-- switch menu to purchased state
		-- Static_SetText( gHandles["btnPayORTake"], "Okay" )
		Control_SetVisible(gHandles["btnPayORTake"], false)
		Control_SetEnabled(gHandles["btnPayORTake"], false)
		Control_SetVisible(gHandles["btnQuit"], true)
		Control_SetEnabled(gHandles["btnQuit"], true)
		Control_SetVisible(gHandles["btnTryAgain"], true)
		Control_SetEnabled(gHandles["btnTryAgain"], true)

		Control_SetVisible(gHandles["imgIconBorder3"], true)
		Control_SetVisible(gHandles["imgIconBG3"], true)
		Control_SetVisible(gHandles["stcYouRecieved"], true)

		Control_SetEnabled(gHandles["imgIconBorder3"], true)
		Control_SetEnabled(gHandles["imgIconBG3"], true)
		Control_SetEnabled(gHandles["stcYouRecieved"], true)

		-- hide other state's controls
		Control_SetVisible(gHandles["imgIconBorder2"], false)
		Control_SetVisible(gHandles["imgIconBG2"], false)
		Control_SetVisible(gHandles["imgDivider"], false)
		Control_SetVisible(gHandles["imgItem2"], false)
		Control_SetVisible(gHandles["btnItem2"], false)
		Control_SetVisible(gHandles["stcDescription"], false)
		Control_SetVisible(gHandles["stcBackpackCount"], false)
		Control_SetVisible(gHandles["stcCost"], false)
		Control_SetVisible(gHandles["stcCostCount"], false)

		Control_SetEnabled(gHandles["imgIconBorder2"], false)
		Control_SetEnabled(gHandles["imgIconBG2"], false)
		Control_SetEnabled(gHandles["imgDivider"], false)
		Control_SetEnabled(gHandles["imgItem2"], false)
		Control_SetEnabled(gHandles["btnItem2"], false)
		Control_SetEnabled(gHandles["stcDescription"], false)
		Control_SetEnabled(gHandles["stcBackpackCount"], false)
		Control_SetEnabled(gHandles["stcCost"], false)
		Control_SetEnabled(gHandles["stcCostCount"], false)

		-- TODO: move btnItem1 to (107,211) from (161,266) and other item controls
		Control_SetLocation(gHandles["imgItemBG1"], 109,210)
		Control_SetLocation(gHandles["imgItem1"],110,211)
		Control_SetLocation(gHandles["imgItemCountBG1"], 172,210)
		Control_SetLocation(gHandles["stcItemCount1"],172,210)
		Control_SetLocation(gHandles["imgItemLevel1"],177,257)
		Control_SetLocation(gHandles["imgItemName1"],110,279)
		Control_SetLocation(gHandles["stcItem1"],112,279)
		Control_SetLocation(gHandles["imgItemOverlay1"],110,279)
		Control_SetLocation(gHandles["btnItem1"], 110, 211)
		Control_SetSize(gHandles["imgItemCountBG1"], Control_GetWidth(gHandles["imgItemCountBG1"]) + 10, Control_GetHeight(gHandles["imgItemCountBG1"]))
		Control_SetLocationX(gHandles["imgItemCountBG1"], Control_GetLocationX(gHandles["imgItemCountBG1"]) - 10)
		Control_SetLocationX(gHandles["stcItemCount1"], Control_GetLocationX(gHandles["stcItemCount1"]) - 10 )
		Control_SetSize(gHandles["stcItemCount1"], Control_GetWidth(gHandles["stcItemCount1"]) + 10, Control_GetHeight(gHandles["imgItemCountBG1"]))

		--removing the empty box when you don't win anything
		local winNothing = (tonumber(m_lootItem.count) == 0)
		
		if winNothing then
			Control_SetVisible(gHandles["stcSorry"], true)
			Control_SetVisible(gHandles["stcLose"], true)
			Control_SetVisible(gHandles["stcYouRecieved"],false)
			processTransaction(m_tradeRemoveTable, {})
			m_tradePaid = false

		else 
			local itemProperties  = deepCopy(m_gameItems[tostring(m_lootItem.UNID)])
			m_tradeAddTable[1].properties = itemProperties
			processTransaction(m_tradeRemoveTable, m_tradeAddTable)
			m_lootItem.properties = itemProperties
			updateItemContainer()
			local recievedText = ""
			if tonumber(m_lootItem.count) == 1 then
				recievedText = "You received a "..m_lootItem.properties.name
			elseif tonumber(m_lootItem.count) > 1 then
				recievedText = "You received "..m_lootItem.properties.name.." x"..m_lootItem.count

				Static_SetText(gHandles["stcItemCount1"], formatNumberSuffix(m_lootItem.count))
				Control_SetVisible(gHandles["stcItemCount1"], true)
				Control_SetEnabled(gHandles["stcItemCount1"], true)

				Control_SetVisible(gHandles["imgItemCountBG1"], true)
				Control_SetEnabled(gHandles["imgItemCountBG1"], true)
			end

			Static_SetText(gHandles["stcYouRecieved"], recievedText)
		end

		Control_SetVisible(gHandles["imgItemBG1"], not winNothing)
		Control_SetVisible(gHandles["imgItem1"], not winNothing)
		Control_SetVisible(gHandles["imgItemCountBG1"], not winNothing)
		Control_SetVisible(gHandles["stcItemCount1"], not winNothing)
		Control_SetVisible(gHandles["imgItemName1"], not winNothing)
		Control_SetVisible(gHandles["stcItem1"], not winNothing)
		Control_SetVisible(gHandles["imgItemOverlay1"], not winNothing)
		Control_SetVisible(gHandles["btnItem1"], not winNothing)
	end
end

function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	if not success then
		closeSell()
	end

	resetState = false

end

----------------------------
-- Local Functions
----------------------------
-- Sets the counts for the input item
setCounts = function()
	checkIsBackpackFull()
	-- Set required item count statics
	local hasNuf = true
	local color

	local reqItem = m_spendItem.input
	local reqNeedCount = m_spendItem.cost
	
	-- Get how many of this item the player has
	local reqHasCount = m_inventoryCounts[reqItem] or 0
	
	-- Set the have color to red if you don't got nuf
	if reqNeedCount > reqHasCount then
		-- Set color to red
		hasNuf = false
		color = {a=255,r=255,g=0,b=0}
	else
		-- Set color to white
		color = {a=255,r=255,g=255,b=255}
	end	
	local stcElementHandle = Static_GetDisplayElement(gHandles["stcCostCount"])
	BlendColor_SetColor(Element_GetFontColor(stcElementHandle), 0, color)
	
	-- Set how many of this item the player has
	if takeState == false then 
		Control_SetEnabled(gHandles["imgItemOverlay1"], not hasNuf)
		Control_SetVisible(gHandles["imgItemOverlay1"], not hasNuf)
	end

	Static_SetText(gHandles["stcBackpackCount"], formatNumberSuffix(reqHasCount).." in backpack")
	Control_SetVisible(gHandles["stcBackpackCount"], true)
	Control_SetVisible(gHandles["imgDivider"], true)

	Static_SetText(gHandles["stcCostCount"], formatNumberSuffix(m_spendItem.cost))
	
	-- Enable the pay button
	if not m_tradePaid and m_tradeRemoveTable ~= nil and m_tradeAddTable ~= nil and m_openSlot then
		Control_SetEnabled(gHandles["btnPayORTake"], hasNuf)
	end

	if hasNuf == false then
		Control_SetEnabled(gHandles["btnPayORTake"], hasNuf)
	end
	
	return hasNuf
end

-- Displays all items inside the container
updateItemContainer = function()

	if m_tradePaid then
		g_displayTable = deepCopy(m_lootItem)
		g_displayTable.cost = g_displayTable.count
	else
		g_displayTable = deepCopy(m_spendItem)
	end


	if g_displayTable.cost <= 0 then
		g_displayTable.hide = true
	end
	
	local itemProperties =  deepCopy(g_displayTable.properties)

	formatTooltips(g_displayTable)
	
	applyImage(gHandles["imgItem1"], itemProperties.GLID)

	local element
	
	if 	itemProperties.itemType == ITEM_TYPES.WEAPON or 
		itemProperties.itemType == ITEM_TYPES.ARMOR or 
		itemProperties.itemType == ITEM_TYPES.TOOL then
		
		Control_SetVisible(gHandles["imgItemLevel1"], true)
		
		element = Image_GetDisplayElement(gHandles["imgItemLevel1"])
		local itemCoords = ITEM_LEVEL_COORDS[itemProperties.level]
		local rarityOffset = ITEM_LEVEL_RARITY_OFFSET[itemProperties.rarity] or 0
		Element_SetCoords(	element, 
							itemCoords.left + rarityOffset, 
							itemCoords.top, 
							itemCoords.right + rarityOffset, 
							itemCoords.bottom)
	else
		Control_SetVisible(gHandles["imgItemLevel1"], false)
	end

	local rarity = itemProperties.rarity or "Common"
	element = Image_GetDisplayElement(gHandles["imgItemBG1"])
	local coords = ITEM_RARITY_BG[rarity]
	Element_SetCoords(	element, 
						coords.left, 
						coords.top, 
						coords.right, 
						coords.bottom)

	Static_SetText(gHandles["stcItem1"], itemProperties.name)
end

-- Populates the menu
updateInventory = function()
	m_inventoryCounts = {}
	-- Compile UNID counts
	for i, item in ipairs(m_inventory) do
		local itemUNID = item.UNID
		if itemUNID ~= 0 then
			-- New UNID entry
			if m_inventoryCounts[itemUNID] == nil then
				m_inventoryCounts[itemUNID] = item.count
			-- Duplicate entry. Increment UNID count
			else
				m_inventoryCounts[itemUNID] = m_inventoryCounts[itemUNID] + item.count
			end
		end
	end

	-- Update overlays
	if not m_tradePaid then
		setPurchasableOverlays()
	end
end

enablePayment = function()
	if m_itemReceived then
		checkIsBackpackFull()
		local reqItem = m_spendItem.input
		local reqNeedCount = m_spendItem.cost
		local reqHasCount = m_inventoryCounts[reqItem] or 0 
		local hasNuf = reqNeedCount <= reqHasCount
		-- Enable the pay button if player can pay
		local enablePay = hasNuf and m_openSlot
		Control_SetEnabled(gHandles["btnPayORTake"], enablePay)
	end
end

-- Set if the designated item is craftable
setPurchasableOverlays = function()
	-- Iterate of the recipes we'll be displaying
	local hasNuf = true
	if m_spendItem  then
		if m_spendItem.cost > (m_inventoryCounts[m_spendItem.input] or 0) then
			hasNuf = false
		end
	
		if takeState == false then
			Control_SetEnabled(gHandles["imgItemOverlay1"], not hasNuf)
			Control_SetVisible(gHandles["imgItemOverlay1"], not hasNuf)
		end
	end
end

 formatTooltips = function(item)
	if item then
		item.tooltip = {}

		item.tooltip["header"] = {label = tostring(item.properties.name)}
		
		if item.properties.rarity then
			item.tooltip["header"].color = COLORS_VALUES[item.properties.rarity]
		end

		if item.properties.description then
			item.tooltip["body"] = {label = "\"" .. item.properties.description .. "\""}
		end

		if item.properties.itemType == ITEM_TYPES.WEAPON then
			item.tooltip["footer1"] = {label = "Weapon Level: "..tostring(item.properties.level)}
			item.tooltip["footer2"] = {label = "Range: "..tostring(item.properties.range)}
		elseif item.properties.itemType == ITEM_TYPES.ARMOR then
			item.properties.slotTypeDisplay = (item.properties.slotType:gsub("^%l", string.upper)) 
			item.tooltip["footer1"] = {label = "Armor Level: "..tostring(item.properties.level)}
			item.tooltip["footer2"] = {label = "Slot: "..tostring(item.properties.slotType:gsub("^%l", string.upper))}
		elseif item.properties.itemType == ITEM_TYPES.TOOL then
			item.tooltip["footer1"] = {label = "Tool Level: "..tostring(item.properties.level)}
		elseif item.properties.itemType == ITEM_TYPES.CONSUMABLE then
			item.properties.consumeValueDisplay = item.properties.consumeValue .. "%" 

			if item.properties.consumeType == "energy" then
				item.tooltip["footer1"] = {label = "Energy Bonus: "..tostring(item.properties.consumeValue) .. "%"}
			else
				item.tooltip["footer1"] = {label = "Health Bonus: "..tostring(item.properties.consumeValue) .. "%"}
			end
		elseif item.properties.itemType == ITEM_TYPES.PLACEABLE then
		elseif item.properties.itemType == ITEM_TYPES.GENERIC then
		elseif item.properties.itemType == ITEM_TYPES.AMMO then
		end
	end
end

checkIsBackpackFull = function()
	m_openSlot = false
	for i = 6, #m_inventory do
		if m_inventory[i].UNID == 0 then
			m_openSlot = true
		end
	end

	if not m_openSlot then
		Control_SetEnabled(gHandles["btnPayORTake"], false) -- Happens TOO EARLY, allows chance of receiving "EMPTY"
		Control_SetVisible(gHandles["stcBackpackFull"],  true)
	else
		Control_SetVisible(gHandles["stcBackpackFull"],  false)
		Control_SetEnabled(gHandles["btnPayORTake"], true)
	end
end

function formatNumberSuffix(count)
	-- If no need for formatting, then return
	if count < 10000 then return count end

	local strCount = tostring(count)
	local newCount = ""

	local format = { 	max = 0,
						min = 0,
					 	suffix = ""
					}

	-- Thousands
	if #strCount <= 6 then
		format.max = 6
		format.min = 3
		format.suffix = "K"
	-- Millions
	elseif #strCount <= 9 then
		format.max = 9
		format.min = 6
		format.suffix = "M"
	-- Billions
	elseif #strCount <= 12 then
		format.max = 12
		format.min = 9
		format.suffix = "B"
	-- Trillions
	elseif #strCount <= 15 then
		format.max = 15
		format.min = 12
		format.suffix = "T"
	end

	for i = 1, #strCount do
		local diff = #strCount - i
		if #strCount <= format.max then
			if diff >= format.min then
				newCount = newCount..string.sub(strCount,i,i)
			else
				return newCount..format.suffix
			end
		end
	end
end
