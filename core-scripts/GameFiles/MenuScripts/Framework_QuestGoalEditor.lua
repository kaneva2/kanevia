--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\MenuFunctions.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\Framework_InventoryHelper.lua")

----LOCATION LOCATION LOCATION---
local STARTING_Y = 65
local NEXT_Y = 30
local WAYPOINT_HEIGHT = 20
local SAVE_BUFFER =  15
local SIZE_BUFFER = 105
local BUTTON_OFFSET = 5
local EDITBOX_OFFSET = 3
local CHECKBOX_OFFSET = 7

local BACKGROUND_WIDTH = 365
local MENU_WIDTH = 400

local STARTING_ATTRIBUTES = 2

local EDIT_ITEM_MAX = 16

local QUANTITY_MAX = 4500000

local WAYPOINT_MAX = 100000 

--THE STATE OF QUEST---
--Each state is a table of controls with the Y releative to 
local QUEST_STATES = 
{ 
	["Collect"] = {
				target = false,
				item = true,
				quantity = true,
				destroy = true,
				crafting = false,
				location = false,
				place = false,
				kill = false,
				attribute = "collect"
			},
	["Craft Object"] = {
				target = false,
				item = false,
				crafting = true,
				quantity = true,
				destroy = false,
				location = false,
				place = false,
				kill = false,
				attribute = "craft"
			},
	["Go To"] = {
				target = false,
				item = false,
				crafting = false,
				quantity = false,
				destroy = false,
				location = true,
				place = false,
				kill = false,
				attribute = "goTo"
			},
	["Talk To"] = {
				target = true,
				item = false,
				crafting = false,
				quantity = false,
				destroy = false,
				location = false,
				place = false,
				kill = false,
				attribute = "talkTo"
			},
	["Take To"] = {
				target = true,
				item = true,
				crafting = false,
				quantity = true,
				destroy = true,
				location = false,
				place = false,
				kill = false,
				attribute = "takeTo"
			},
	["Place Object"] = {
				target = false,
				item = false,
				crafting = false,
				quantity = true,
				destroy = false,
				location = false,
				place = true,
				kill = false,
				attribute = "place"
			},
	["Kill"] = {
				target = false,
				item = false,
				crafting = false,
				quantity = true,
				destroy = false,
				location = false,
				place = false,
				kill = true,
				attribute = "kill"
			}

}



local CONTROL_TO_HIDE = {"stcAttribute_target", "btnEdit_target", "stcName_target", "edBox_target", "stcAttribute_required", "btnEdit_required", "stcName_required", "edBox_required", "stcAttribute_craft", "stcAttribute_place", "stcAttribute_kill", "stcQuantity",  "edQuantity", "stcDestroy", "boxDestroy", "stcLocation", "edLocation", "imgGreyBG2"}
local m_tQuestItem = {} --Presaved quest item
local m_tTempQuestItem = {} --New quest item

local m_sQuestState = nil

local m_iPendingQuestWaypoint = nil

local m_gameItems = {}

local m_bNewQuest = false


function onCreate()
	MenuCenterThis()
	--KEP_EventRegister("IMAGE_SELECT_EVENT", KEP.HIGH_PRIO)


	Events.registerHandler("FRAMEWORK_UPDATE_QUEST_WAYPOINT", updateQuestWaypoint)

	KEP_EventRegisterHandler("imageSelectEventHandler", "IMAGE_SELECT_EVENT", KEP.MED_PRIO )
	KEP_EventRegisterHandler("changeGameItemParam", "CHANGED_PARAM_EVENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("editQuestItemHandler", "FRAMEWORK_EDIT_POPOUT_OPEN", KEP.HIGH_PRIO)
	requestInventory(GAME_PID)
end

function onDestroy()
	if m_iPendingQuestWaypoint then
		Events.sendEvent("FRAMEWORK_REMOVE_QUEST_WAYPOINT", {waypointPID = m_iPendingQuestWaypoint})
	end
end
----------------
--EVENT HANDLERS
------------------
function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems

	ConfigureMenu()
end

function editQuestItemHandler( dispatcher, fromNetid, tEvent, eventid, filter, objectid )
	m_tQuestItem = Events.decode(KEP_EventDecodeString(tEvent))
	m_tTempQuestItem = deepCopy(m_tQuestItem)

	if not m_tQuestItem.questType then
		m_sQuestState = "Collect"
		m_tTempQuestItem.questType  = "collect"
	else
		for state, values in pairs(QUEST_STATES) do
			if values.attribute ==  m_tQuestItem.questType then
				m_sQuestState = state
				ComboBox_SetSelectedByText(gHandles["cmbGoalAction"], state)
			end
		end
	end

	local imageName = ""
	if m_tQuestItem.goalImage then
		imageName = m_tQuestItem.goalImage
		local imageString = tostring(imageName)
		local lastSlashIndex = string.find(imageString, "/[^/]*$")
		local periodIndex = string.find(imageString,  '.', 1, true)
		if lastSlashIndex and periodIndex then
			imageName =  string.sub(imageString, lastSlashIndex + 1, periodIndex - 1) or imageName
		end
	end
	Static_SetText(gHandles["stcImageName"], imageName)

	ConfigureWaypoint()
end

-- Attribute updated from item selector
function changeGameItemParam(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	
	local attribute = KEP_EventDecodeString(tEvent) -- Updated attribute
	local value = KEP_EventDecodeString(tEvent) -- Updated value
	local name = KEP_EventDecodeString(tEvent) -- Update name

	if attribute == "required" or attribute == "target" then
		if KEP_EventMoreToDecode(tEvent) == 1 then -- If there is a new GLID or UNID name to assign to this value
			itemValueType = KEP_EventDecodeString(tEvent)
			if itemValueType == "UNID" then
				
				m_tTempQuestItem[attribute.."UNID"] = value
				m_tQuestItem[attribute.."UNID"] = value
				Static_SetText(gHandles["stcName_"..attribute], name, true)
				
			end		
		end
	end
end

function imageSelectEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	local attribute = KEP_EventDecodeString(event)

	if attribute == "goalImage" then
		local imageName = KEP_EventDecodeString(event)
		local imageValue = KEP_EventDecodeString(event)
		m_tTempQuestItem.goalImage = imageValue
		
		Static_SetText(gHandles["stcImageName"], imageName)
	end
end

function updateQuestWaypoint(event)

	local waypoint = event.waypointPID

	if waypoint and tonumber(waypoint) > 0 then
		m_tTempQuestItem.waypointPID  = waypoint
		m_iPendingQuestWaypoint = waypoint --Needed to remove waypont if canceled
	else
		m_iPendingQuestWaypoint = nil
		m_tTempQuestItem.waypointPID = 0
	end

	ConfigureWaypoint()
end

------------------
--CONTORL HANDLERS
-------------------
function cmbGoalAction_OnComboBoxSelectionChanged(comboBoxHandle, selectedIndex, selectedText)		
	clearMenu()

	m_sQuestState = selectedText
	m_tTempQuestItem.questType  = QUEST_STATES[selectedText].attribute

	ConfigureMenu()
end

function btnClose_OnButtonClicked( buttonHandle )
	MenuCloseThis()
end


function btnSave_OnButtonClicked( buttonHandle )

	--Pending Waypont. If quest is not saved, have GameItemEditor remove it
	if m_iPendingQuestWaypoint then
		m_tTempQuestItem.pendingWaypoint = m_iPendingQuestWaypoint
		m_iPendingQuestWaypoint = nil
	end
	local editEvent = KEP_EventCreate("FRAMEWORK_EDIT_POPOUT_SAVED")
	KEP_EventEncodeString(editEvent, Events.encode(m_tTempQuestItem))
					
	KEP_EventQueue(editEvent)

	MenuCloseThis()
end

function edQuantity_OnEditBoxChange(editBoxHandle, editBoxText)
	local quantity = 0 

	if editBoxText == "" then
		quantity = 0
	elseif not tonumber(editBoxText)   then	
		quantity = m_tQuestItem.requiredCount or 0
	elseif tonumber(editBoxText) < 0  then
		quantity = 0
	elseif tonumber(editBoxText) > QUANTITY_MAX  then
		quantity = QUANTITY_MAX
	else
		quantity = tonumber(editBoxText)
	end

	m_tQuestItem.requiredCount = quantity
	m_tTempQuestItem.requiredCount = quantity
	EditBox_SetText(editBoxHandle, tostring(quantity), false)
end

function boxDestroy_OnCheckBoxChanged( checkboxHandle, checked )
	m_tQuestItem.destroyOnCompletion = checked == 1
	m_tTempQuestItem.destroyOnCompletion = checked == 1
end 

function btnImage_OnButtonClicked( buttonHandle )
	menuName = "Framework_ImageSelect.xml"
	local event = KEP_EventCreate( "EDIT_PARAM_EVENT" )
	KEP_EventEncodeString(event, "goalImage")
	KEP_EventQueue( event )
	MenuOpen(menuName)

	local pos = MenuGetLocationThis()


	MenuSetLocation(menuName, pos.x + 60 , pos.y - 60)										
end

function edLocation_OnEditBoxChange(editBoxHandle, editBoxText)

	local locationDescription =  editBoxText
	if string.len(locationDescription) > 50 then	
		locationDescription = m_tQuestItem.goToDescription
	end

	m_tQuestItem.goToDescription = locationDescription
	m_tTempQuestItem.goToDescription = locationDescription
	EditBox_SetText(editBoxHandle, tostring(locationDescription), false)
end
----------------
--LOCAL FUNCTIONS
----------------

function ConfigureMenu( )
	if m_sQuestState == nil then return end

	local questProperties = QUEST_STATES[m_sQuestState]

	local posY = STARTING_Y

	local totalAttributes = STARTING_ATTRIBUTES

	--Check each attribute and if the attribute is false, don't show controls and set value to nil for return to Game Item Editor
	if questProperties.target then
		posY = posY + NEXT_Y
		totalAttributes = totalAttributes + 1

		positionItemSelect("target", posY)
	else
		m_tTempQuestItem.targetUNID = nil
	end

	if questProperties.item then
		posY = posY + NEXT_Y
		totalAttributes = totalAttributes + 1

		positionItemSelect("required", posY)
	end

	if questProperties.crafting then
		posY = posY + NEXT_Y
		totalAttributes = totalAttributes + 1

		positionItemSelect("craft", posY)
	end

	if questProperties.place then
		posY = posY + NEXT_Y
		totalAttributes = totalAttributes + 1

		positionItemSelect("place", posY)
	end

	if questProperties.kill then
		posY = posY + NEXT_Y
		totalAttributes = totalAttributes + 1

		positionItemSelect("kill", posY)
	end

	--No Unid is required for these items
	if not questProperties.crafting and not questProperties.item and not questProperties.kill and not questProperties.place then
		m_tTempQuestItem.requiredUNID = 0
	end

	if questProperties.quantity then
		posY = posY + NEXT_Y
		totalAttributes = totalAttributes + 1

		Control_SetLocationY(gHandles["stcQuantity"], posY)
		setControlEnabledVisible("stcQuantity", true)

		Control_SetLocationY(gHandles["edQuantity"], posY + EDITBOX_OFFSET)
		setControlEnabledVisible("edQuantity", true)

		local quantity = m_tQuestItem.requiredCount or 1
		m_tTempQuestItem.requiredCount = quantity
		EditBox_SetText(gHandles["edQuantity"], tostring(quantity), false)
	else
		m_tTempQuestItem.requiredCount = nil
	end

	if questProperties.destroy then
		posY = posY + NEXT_Y
		totalAttributes = totalAttributes + 1

		Control_SetLocationY(gHandles["stcDestroy"], posY)
		setControlEnabledVisible("stcDestroy", true)

		Control_SetLocationY(gHandles["boxDestroy"], posY + CHECKBOX_OFFSET)
		setControlEnabledVisible("boxDestroy", true)

		local destroy = true
		if m_tQuestItem.destroyOnCompletion == nil  then
			destroy = true
		else
			destroy = (m_tQuestItem.destroyOnCompletion == "true")  or (m_tQuestItem.destroyOnCompletion == true )
		end

		m_tTempQuestItem.destroyOnCompletion = destroy
		CheckBox_SetChecked(gHandles["boxDestroy"], destroy)
	else
		m_tTempQuestItem.destroyOnCompletion = nil
	end

	if questProperties.location then
		posY = posY + NEXT_Y
		totalAttributes = totalAttributes + 1

		Control_SetLocationY(gHandles["stcLocation"], posY)
		setControlEnabledVisible("stcLocation", true)

		Control_SetLocationY(gHandles["edLocation"], posY + EDITBOX_OFFSET)
		setControlEnabledVisible("edLocation", true)

		local locationText = m_tQuestItem.goToDescription or ""
		m_tTempQuestItem.goToDescription = locationText
		EditBox_SetText(gHandles["edLocation"], locationText, false)
	else
		m_tTempQuestItem.moveToText = nil
	end
	setControlEnabledVisible("imgGreyBG2", totalAttributes > 4)

	--Configure waypoint and get the next posY
	posY = positionWaypoint(posY + NEXT_Y)

	--new Quest add one attribute, existing ones add 3
	if  m_bNewQuest then
		totalAttributes = totalAttributes + 1
	else
		totalAttributes = totalAttributes + 3
	end

	--Positon the bottom line
	Control_SetLocationY(gHandles["trimH2"], posY)

	--Set the background and trim heights
	local backgroundHeight = (NEXT_Y * totalAttributes) + WAYPOINT_HEIGHT
	Control_SetSize(gHandles["imgWhiteBG"], BACKGROUND_WIDTH, backgroundHeight)
	Control_SetSize(gHandles["trimV1"], 1, backgroundHeight + WAYPOINT_HEIGHT)
	Control_SetSize(gHandles["trimV2"], 1, backgroundHeight + WAYPOINT_HEIGHT)
	--position the save button
	Control_SetLocationY(gHandles["btnSave"], posY + SAVE_BUFFER)
	--Last but not least, resize menu
	MenuSetSizeThis(MENU_WIDTH, backgroundHeight + SIZE_BUFFER + SAVE_BUFFER)
end

function clearMenu()
	for i, control in pairs(CONTROL_TO_HIDE) do
		setControlEnabledVisible(control, false)
	end
end

function ConfigureWaypoint()
	
	local radius = m_tQuestItem.radius or 10
	local height = m_tQuestItem.height or 10

	local waypointPID = m_tTempQuestItem.waypointPID
	local isWaypointInWorld = KEP_DynamicObjectGetGLID(tonumber(waypointPID)) ~= 0


	EditBox_SetText(gHandles["edRadius"], tostring(radius), false)
	EditBox_SetText(gHandles["edHeight"], tostring(height), false)

	Control_SetEnabled(gHandles["btnSetWaypoint"], waypointPID == 0)

	--TODO: Goto waypoint functionality that has never been implemented
	setControlEnabledVisible("btnGoToWaypoint", false)
	Control_SetEnabled(gHandles["btnRemoveWaypoint"], isWaypointInWorld)

	_G["edRadius_OnEditBoxChange"] = function(editBoxHandle, editBoxText)
		updateWaypointValue("radius", radius, editBoxHandle, editBoxText)
	end

	_G["edHeight_OnEditBoxChange"] = function(editBoxHandle, editBoxText)
		updateWaypointValue("height", height, editBoxHandle, editBoxText)
	end

	_G["btnSetWaypoint_OnButtonClicked"] = function(buttonHandle)
		Events.sendEvent("FRAMEWORK_PLACE_QUEST_WAYPOINT", {questName = m_tQuestItem.name})
	end

	_G["btnRemoveWaypoint_OnButtonClicked"] = function(buttonHandle)
		Events.sendEvent("FRAMEWORK_REMOVE_QUEST_WAYPOINT", {waypointPID = waypointPID})
	end
	
end

function updateWaypointValue(attribute, value, editBoxHandle, editBoxText )
	
		if editBoxText == "" then
			value = 0
		elseif not tonumber(editBoxText)   then	
			value = m_tQuestItem[attribute] or 0
		elseif tonumber(editBoxText) < 0  then
			value = 0
		elseif tonumber(editBoxText) > WAYPOINT_MAX then
			value = WAYPOINT_MAX
		else
			value = tonumber(editBoxText)
		end

		m_tQuestItem[attribute] = value
		m_tTempQuestItem[attribute] = value
		EditBox_SetText(editBoxHandle, tostring(value), false)
end

function positionWaypoint(posY)
	Control_SetLocationY(gHandles["imgWaypointTab"], posY)
	Control_SetLocationY(gHandles["stcWaypointTab"], posY)

	posY = posY + WAYPOINT_HEIGHT

	if m_bNewQuest then
		Control_SetLocationY(gHandles["stcWaypointFail"], posY)
	else
		Control_SetLocationY(gHandles["stcWaypoint"], posY)
		Control_SetLocationY(gHandles["btnRemoveWaypoint"], posY + BUTTON_OFFSET)
		Control_SetLocationY(gHandles["btnSetWaypoint"], posY + BUTTON_OFFSET)
		Control_SetLocationY(gHandles["btnGoToWaypoint"], posY + BUTTON_OFFSET)

		posY = posY + NEXT_Y
		Control_SetLocationY(gHandles["stcRadius"], posY)
		Control_SetLocationY(gHandles["stcRadiusFeet"], posY)
		Control_SetLocationY(gHandles["edRadius"], posY + EDITBOX_OFFSET)
		Control_SetLocationY(gHandles["imgGreyWaypointBG"], posY)
		
		posY = posY + NEXT_Y
		Control_SetLocationY(gHandles["stcHeight"], posY)
		Control_SetLocationY(gHandles["stcHeightFeet"], posY)
		Control_SetLocationY(gHandles["edHeight"], posY + EDITBOX_OFFSET)
	end

	return posY + NEXT_Y
end

function positionItemSelect(attribute, posY)
	setControlEnabledVisible("stcAttribute_"..attribute, true)
	Control_SetLocationY(gHandles["stcAttribute_"..attribute], posY)

	local typeFilter = "all"
	local behaviorFilter = "all"
	local property = "Required Item"

	if attribute == "target" then
		typeFilter = "character"
		property = "Target Character"
		behaviorFilter = "vendor,shop_vendor,actor,quest_giver"
	elseif attribute == "place" then
		typeFilter = "placeable"

	elseif attribute == "kill" then
		typeFilter = "character"
		property = "Target Character"
		behaviorFilter = "monster,animal"
	end

	local originalAttribute = attribute

	--For control handles, needs to say required
	if attribute ~= "target" then
		attribute = "required"
	end

	setControlEnabledVisible("btnEdit_"..attribute, true)
	Control_SetLocationY(gHandles["btnEdit_"..attribute], posY + BUTTON_OFFSET)
	
	local itemName = ""


	if m_tQuestItem[attribute.."UNID"] then
		m_tTempQuestItem[attribute.."UNID"] = m_tQuestItem[attribute.."UNID"]
		local validItem = false
		local gameItem = m_gameItems[tostring(m_tQuestItem[attribute.."UNID"])]
		if gameItem then
			itemName = gameItem.name
			if originalAttribute == "kill" then
				if gameItem.behavior and (gameItem.behavior == "monster" or gameItem.behavior == "animal") then
					validItem = true
				end
			elseif originalAttribute == "place" then
				if gameItem.itemType and gameItem.itemType == "placeable" then
					validItem = true
				end
			elseif originalAttribute == "target" then
				if gameItem.itemType and gameItem.itemType == "character" and (gameItem.behavior ~= "monster" and gameItem.behavior ~= "animal") then
					validItem = true
				end
			elseif isLootItem(gameItem.itemType, gameItem.behavior) then
				validItem = true
			end
		end
		if validItem == false then
			m_tTempQuestItem[attribute.."UNID"] = 0
			itemName = ""
		end
	end

	setControlEnabledVisible("edBox_"..attribute, true)
	Control_SetLocationY(gHandles["edBox_"..attribute], posY + EDITBOX_OFFSET)

	Static_SetText(gHandles["stcName_"..attribute], itemName, false)
	setControlEnabledVisible("stcName_"..attribute, true)
	Control_SetLocationY(gHandles["stcName_"..attribute], posY + BUTTON_OFFSET)

	_G["btnEdit_" .. attribute .. "_OnButtonClicked"] = function(btnHandle)

		if attribute == "target" then
			
		end

		menuName = "Framework_ItemSelectEditor.xml"
		MenuOpen(menuName)										
		local event = KEP_EventCreate( "EDIT_PARAM_EVENT" )
		KEP_EventEncodeString(event, "UNID")
		KEP_EventEncodeString(event, typeFilter)
		KEP_EventEncodeString(event, behaviorFilter)
		KEP_EventEncodeString(event, attribute)
		KEP_EventEncodeString(event, property)
		KEP_EventEncodeString(event, itemName)
		KEP_EventEncodeString(event, "None")
		KEP_EventEncodeString(event, m_tQuestItem[attribute.."UNID"] or 0)
		KEP_EventQueue( event )
	end
end

function setControlEnabledVisible(controlName, bool)
	Control_SetVisible(gHandles[controlName], bool)
	Control_SetEnabled(gHandles[controlName], bool)
end

function isLootItem(itemType, behavior)
	if itemType == nil or behavior == nil then
		return false
	end
	if itemType == ITEM_TYPES.CHARACTER and behavior == "pet" then -- Pets are loot items
		return true
	elseif itemType == ITEM_TYPES.HARVESTABLE and behavior == "seed" then -- Seeds are loot items
		return true
	elseif	itemType == ITEM_TYPES.WEAPON or 
			itemType == ITEM_TYPES.AMMO or 
			itemType == ITEM_TYPES.ARMOR or 
			itemType == ITEM_TYPES.TOOL or 
			itemType == ITEM_TYPES.BLUEPRINT or 
			itemType == ITEM_TYPES.CONSUMABLE or 
			itemType == ITEM_TYPES.GENERIC or 
			itemType == ITEM_TYPES.PLACEABLE then
		return true
	end

	return false
end