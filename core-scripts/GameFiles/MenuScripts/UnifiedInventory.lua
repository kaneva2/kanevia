--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_DragDropHelper.lua")
dofile("MenuAnimation.lua")
dofile("UnifiedNavigationStates.lua")
dofile("Framework_ActorHelper.lua")
dofile("Framework_MenuShadowHelper.lua")

local NAVIGATION_MENU = "UnifiedNavigation.xml"
local ARMOR_MENU = "Framework_Armor.xml"

local SHOW_MENU_SYSTEM_SNAP_T0 = {x = 0, y = 30}
local NAV_WIDTH = 250

local ARMOR_OFFSET = 478
local ARMOR_WIDTH = 160

local m_currentMenu

local m_armorOpen = false

local m_tweening = false
local m_dragging = false

local m_snapToRight = false
local m_snapToLeft = false
local m_lastPosition

local NAV_OFFSET = 81 --Entire menu offset by navigation
local NAV_OFFSET_X = 1 --Offset for Nav menu X
local NAV_OFFSET_Y = 65 --Offset for Nev menu y


g_changeBuildModeEvent = true

-- When the menu is created
function onCreate()

	KEP_EventRegister("FRAMEWORK_OPEN_ARMOR_MENU", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_CLOSE_ARMOR_MENU", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_ALIGN_INVENTORY", KEP.HIGH_PRIO)

	KEP_EventRegister("UNIFIED_SET_TITLE", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("toggleArmorMenu", "FRAMEWORK_OPEN_ARMOR_MENU", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("closeArmorMenu", "FRAMEWORK_CLOSE_ARMOR_MENU", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("openMenuHandler", "UNIFIED_OPEN_MENU", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("setTitleHandler", "UNIFIED_SET_TITLE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("alignMenus", "FRAMEWORK_ALIGN_INVENTORY", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "buildModeChangedEventHandler", "BuildModeChangedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "clientResizeEventHandler", "ClientResizeEvent", KEP.LOW_PRIO )

	--close backpack when running away from a chest or a vendor 
	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", closeAllMenus)
	Events.registerHandler("FRAMEWORK_CLOSE_ITEM_CONTAINER", onChestClosed)
	--Events.registerHandler("PLAYER_OUT_CRAFTING_PROXIMITY", playerOutCraftingProximity)

	MenuSetSizeThis(Dialog_GetWidth(gDialogHandle) + NAV_OFFSET, Dialog_GetHeight(gDialogHandle))
	Control_SetSize(gHandles["imgFauxBG"], Control_GetWidth(gHandles["imgFauxBG"]) + NAV_OFFSET, Control_GetHeight(gHandles["imgFauxBG"]))
	Control_SetLocation(gHandles["btnClose"], Control_GetLocationX(gHandles["btnClose"]) + NAV_OFFSET, Control_GetLocationY(gHandles["btnClose"]))
	resizeDropShadow()

	m_lastPosition = MenuGetLocationThis()
	MenuSetLocationThis(m_lastPosition.x - NAV_OFFSET, m_lastPosition.y)
	local loc = getMenuSnapLocation()

	MenuSetLocation(NAVIGATION_MENU, loc.x + NAV_OFFSET_X, loc.y - NAV_OFFSET_Y)
	MenuBringToFront(NAVIGATION_MENU)
	ARMOR_OFFSET = ARMOR_OFFSET + NAV_OFFSET


end

function onDestroy()
	for _,menu in pairs(SYSTEM_MENUS) do
		if MenuIsOpen(menu) then
			MenuClose(menu)
		end
	end

	if MenuIsOpen(NAVIGATION_MENU) then
		MenuClose(NAVIGATION_MENU)
	end

	if MenuIsOpen(ARMOR_MENU) then
		MenuClose(ARMOR_MENU)
	end
	
	if (g_changeBuildModeEvent == true) then
		local ev = KEP_EventCreate("ChangeBuildModeEvent")
		KEP_EventSetFilter(ev, MODE_INVENTORY)
		KEP_EventQueue( ev)
	end
end


function onChestClosed(event)
    if MenuIsOpen("Framework_ItemContainer") then
        MenuClose("Framework_ItemContainer")
	    MenuCloseThis()
    end
end

-- function playerOutCraftingProximity(event)
-- 	MenuCloseThis()
-- end

function closeAllMenus(event)
	--ifActorMenuOpen(vendorsOnly) 
	--Determine if a menu is currently open to close it and if a vendor or repair, close backpack as well
	if isActorMenuOpen(false) then
	    if isActorMenuOpen(true) then
	   		MenuCloseThis()
	   	end
	   	CloseAllActorMenus()
	end
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	MenuAnimation_OnRender(fElapsedTime)

	curLoc = MenuGetLocationThis()
	if not m_dragging then
		if (curLoc.x ~= m_lastPosition.x or curLoc.y ~= m_lastPosition.y) then
			if Dialog_IsLMouseDown(dialogHandle) == 1 then
				m_dragging = true
			end
			MenuBringToFrontThis()

			local loc = getMenuSnapLocation()

			if m_armorOpen then
				MenuSetLocation(ARMOR_MENU, loc.x + ARMOR_OFFSET, loc.y)
				MenuSetPassthrough(ARMOR_MENU, true)
				MenuBringToFront(ARMOR_MENU)
			end

			if m_currentMenu then
				MenuSetLocation(m_currentMenu, loc.x + NAV_OFFSET, loc.y)
				MenuSetPassthrough(m_currentMenu, true)				
				MenuBringToFront(m_currentMenu)
			end

			loc = getNavSnapLocation()

			MenuSetLocation(NAVIGATION_MENU, loc.x + NAV_OFFSET_X, loc.y - NAV_OFFSET_Y)
			MenuSetPassthrough(NAVIGATION_MENU, true)
			MenuBringToFront(NAVIGATION_MENU)

			m_snapToRight = false
		end
	else
		local loc = getMenuSnapLocation()

		if m_currentMenu then
			MenuSetLocation(m_currentMenu , loc.x + NAV_OFFSET, loc.y)
		end

		if m_armorOpen then
			MenuSetLocation(ARMOR_MENU, loc.x + ARMOR_OFFSET, loc.y)
			MenuSetPassthrough(ARMOR_MENU, false)
		end
		
		loc = getNavSnapLocation()

		MenuSetLocation(NAVIGATION_MENU, loc.x + NAV_OFFSET_X, loc.y - NAV_OFFSET_Y)

		if Dialog_IsLMouseDown(dialogHandle) == 0 then
			m_dragging = false
			if m_currentMenu then
				MenuSetPassthrough(m_currentMenu, false)
			end
			
			MenuSetPassthrough(NAVIGATION_MENU, false)
		end
	end

	m_lastPosition = curLoc
end

function Dialog_OnMoved(dialogHandle, x, y)
	if m_currentMenu then
		local loc = getMenuSnapLocation()
		MenuSetLocation(m_currentMenu, loc.x + NAV_OFFSET, loc.y)
		if m_armorOpen then
			MenuSetLocation(ARMOR_MENU, loc.x + ARMOR_OFFSET, loc.y)		
		end
		loc = getNavSnapLocation()
		MenuSetLocation(NAVIGATION_MENU, loc.x + NAV_OFFSET_X, loc.y - NAV_OFFSET_Y)		
	end
end


function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	alignMenus()
end

function Dialog_OnRButtonDown(dialogHandle, x, y )
	alignMenus()
end
-------------------------------------------------
-- EVENT HANDLERS
-------------------------------------------------

function buildModeChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	g_build_enabled = filter
	if g_build_enabled ~= 1 then
		g_changeBuildModeEvent = false
		MenuCloseThis()
	end
end

function openMenuHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local openMenu = KEP_EventDecodeString(tEvent)
	
	toggleSystem(openMenu)
end

function setTitleHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local title = KEP_EventDecodeString(tEvent)
	Static_SetText(gHandles["Menu_Title"], title)
end

function setControlEnabledVisible(controlName, bool)
	Control_SetVisible(gHandles[controlName], bool)
	Control_SetEnabled(gHandles[controlName], bool)
end

function toggleArmorMenu()
	if m_armorOpen then
		closeArmorMenu()
	else
		openArmorMenu()
	end
end

function closeArmorMenu()	
	if m_armorOpen then
		closeArmorMenu()
	end
end

-------------------------------------------------
-- DISPLAY FUNCTIONS
-------------------------------------------------

function alignMenus()
	MenuBringToFrontThis()
	if m_armorOpen then
		MenuSetPassthrough(ARMOR_MENU, false)
		MenuBringToFront(ARMOR_MENU)
	end

	if m_currentMenu then
		MenuSetPassthrough(m_currentMenu, false)				
		MenuBringToFront(m_currentMenu)
	end

	MenuSetPassthrough(NAVIGATION_MENU, false)
	MenuBringToFront(NAVIGATION_MENU)

	if MenuIsOpen("Framework_ContextualHelp.xml") then
		MenuBringToFront("Framework_ContextualHelp.xml")
	end 
end

function toggleSystem(menu)
	if m_currentMenu then
		closeSystem()
	end	

	m_currentMenu = menu
	alignMenus()

	openSystem()
end

function openSystem()

	-- added this for the contextual help, problems with showing the help when not in the backpack
	if m_currentMenu ~= "Framework_Backpack.xml" then
		MenuClose("Framework_Backpack.xml")
	end 

	if MenuIsOpen(m_currentMenu) then
		MenuSetMinimized(m_currentMenu, false)
	else
		MenuOpen(m_currentMenu)
	end

	MenuBringToFrontThis()
	MenuBringToFront(m_currentMenu)
	local loc = getMenuSnapLocation()
	MenuSetLocation(m_currentMenu, loc.x + NAV_OFFSET, loc.y)

	if m_armorOpen then
		closeArmorMenu()
		m_armorOpen = false
	end

	MenuBringToFront(NAVIGATION_MENU)
end

function closeSystem()
	if m_currentMenu then
		MenuSetMinimized(m_currentMenu, true)
		m_currentMenu = nil
	end
end

function getMenuSnapLocation()
	local loc = MenuGetLocationThis()
	loc.x = loc.x + SHOW_MENU_SYSTEM_SNAP_T0.x 

	loc.y = loc.y + SHOW_MENU_SYSTEM_SNAP_T0.y
	return loc
end

function getNavSnapLocation()
	local loc = MenuGetLocationThis()
	loc.x = loc.x + SHOW_MENU_SYSTEM_SNAP_T0.x
	loc.y = loc.y + SHOW_MENU_SYSTEM_SNAP_T0.y
	return loc
end


function openArmorMenu()

	if not m_armorOpen and not m_tweening then
		m_tweening = true

		--Determine location of menu
		local menuShift = 0
		local menuLocataion = MenuGetLocationThis()
		local menuSize = MenuGetSizeThis() 
		local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
		local screenHeight = Dialog_GetScreenHeight(gDialogHandle)		

		if (menuLocataion.x + ARMOR_WIDTH + menuSize.width) >= screenWidth then
			menuShift = menuLocataion.x - (screenWidth - ARMOR_WIDTH - menuSize.width)
			m_snapToRight = true
		end

		local tweenMenuShift = Tween(gDialogHandle)
		if m_snapToRight then
			tweenMenuShift:move(-menuShift, 0, .10)
		end
		--TODO: Else statment that will delay shift, but nto move menu. tweenMenuShift:move(0, 0, .10) will still move
		tweenMenuShift:on(CALLBACK.STOP,	function() 
												--Open Armor Menu
												local loc = getMenuSnapLocation()
												MenuOpen(ARMOR_MENU)	
												MenuSetLocation(ARMOR_MENU, loc.x + ARMOR_OFFSET, loc.y)
												MenuBringToFront(ARMOR_MENU)
												m_armorOpen = true
												m_tweening = false
											end)
		tweenMenuShift:start()

		local tweenDialogResize = Tween(gDialogHandle)
		tweenDialogResize:resize(ARMOR_WIDTH, 0, .10)
		tweenDialogResize:start()

		local tweenBGResize = Tween(nil, {"imgFauxBG"})
		tweenBGResize:resize(ARMOR_WIDTH, 0, .10)
		tweenBGResize:start()

		local tweenShadowResize = Tween(nil, {"imgShadow"})
		tweenShadowResize:resize(ARMOR_WIDTH, 0, .10)
		tweenShadowResize:start()

		local tweenButtonMove = Tween(nil, {"btnClose"})
		tweenButtonMove:move(ARMOR_WIDTH, 0, .10)
		tweenButtonMove:start()

		if m_snapToRight then
			local tweenSystemMove = Tween({m_currentMenu}, nil)
			tweenSystemMove:move(-menuShift - 8, 0, .12)
			tweenSystemMove:start()

			local tweenNavMove = Tween({"UnifiedNavigation.xml"}, nil)
			tweenNavMove:move(-menuShift - 8, 0, .12)
			tweenNavMove:start()
		end
	end
end

function closeArmorMenu()
	if m_armorOpen and not m_tweening then
		MenuClose(ARMOR_MENU)
		m_tweening = true

		--Determine location of menu
		local menuShift = 0
		local menuLocataion = MenuGetLocationThis()
		local menuSize = MenuGetSizeThis()
		local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
		local screenHeight = Dialog_GetScreenHeight(gDialogHandle)	
		if m_snapToRight then
			if (menuLocataion.x - ARMOR_WIDTH + menuSize.width) < screenWidth then
				menuShift = menuLocataion.x - (screenWidth + ARMOR_WIDTH - menuSize.width)
			end
		end

		local tweenMenuShift = Tween(gDialogHandle)
		if m_snapToRight then
			tweenMenuShift:move(-menuShift, 0, .10)
		end
		tweenMenuShift:on(CALLBACK.STOP,	function()
												m_armorOpen = false
												m_tweening = false
											end)
		tweenMenuShift:start()

		local tweenDialogResize = Tween(gDialogHandle)
		tweenDialogResize:resize(-ARMOR_WIDTH, 0, .10)
		tweenDialogResize:start()

		local tweenBGResize = Tween(nil, {"imgFauxBG"})
		tweenBGResize:resize(-ARMOR_WIDTH, 0, .10)
		tweenBGResize:start()

		local tweenShadowResize = Tween(nil, {"imgShadow"})
		tweenShadowResize:resize(-ARMOR_WIDTH, 0, .10)
		tweenShadowResize:start()

		local tweenButtonMove = Tween(nil, {"btnClose"})
		tweenButtonMove:move(-ARMOR_WIDTH, 0, .10)
		tweenButtonMove:start()		

		if m_snapToRight then
			local tweenSystemMove = Tween({m_currentMenu}, nil)
			tweenSystemMove:move(-menuShift - 8, 0, .12)
			tweenSystemMove:start()

			local tweenNavMove = Tween({"UnifiedNavigation.xml"}, nil)
			tweenNavMove:move(-menuShift - 8, 0, .12)
			tweenNavMove:start()
		end
	end
end

