--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_QuestGiver.lua
--
-- Displays quests for a given Quest Giver
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("Framework_ActorHelper.lua")

-- Constants
QUEST_ICON = {AVAILABLE =	  {texture = "Framework_HudElements.tga",
							   left = 474, top = 108, right = 500, bottom = 134},
			  COMPLETE =	  {texture = "Framework_HudElements.tga",
							   left = 474, top = 134, right = 500, bottom = 160},
			  ACTIVE =		  {texture = "Framework_HudElements.tga",
							   left = 474, top = 82, right = 500, bottom = 108},
			  REP_AVAILABLE = {texture = "Framework_HudElements.tga",
							   left = 474, top = 160, right = 500, bottom = 186},
			  REP_ACTIVE	= {texture = "Framework_HudElements.tga",
							   left = 474, top = 186, right = 500, bottom = 212}
			 }

local QUEST_DESCRIPTION_Y_BUFFER = 8
local AVAILABLE_QUEST_Y_BUFFER = 20
local GOODBYE_Y_BUFFER = 20

local QUEST_ICON_SIZE = 26 -- Height and width for the Quest Icon image
local QUEST_Y_BUFFER = 12
local LINE_Y_BUFFER = 10
local BAD_API_WIDTH_BUFFER = 40

local QUESTS_PER_PAGE = 5 -- Number of quests displayed per page

local IMAGE_SIZE = 84 --Image size: Both width and height

--Icon Glow Constants
local ICON_GLOW_TEXTURE = "Framework_HudElements.tga"
local ICON_GLOW_LEFT = 467
local ICON_GLOW_TOP = 213
local ICON_GLOW_RIGHT = 506
local ICON_GLOW_BOTTOM = 252
local ICON_GLOW_SIZE = 39
-- Local
local m_quests = {} -- Quest giver quests
local m_rewardItem = {}
local m_displayQuests = {} -- Eligible quests to be displayed
local m_displayRewards = {}
local m_questEligible = {} -- Table matching m_displayQuests with the quest's state
local m_playerQuests = {} -- Quests a player has
local m_questPage = 1
local m_baseLineQuestY = 0
local m_topLineQuestY = 0
local m_highestMenuY = nil
local m_questInfoDialog = nil
local m_questsReceived = false
local m_playerQuestsRecieved = false
local m_actorPID = nil
local m_image = nil
local m_isRider = false
local m_riderPID = nil

-- local m_pendingBarks = {}
-- local m_questQueueTimer = 0

local m_gameItems = {}

-- Called when the menu is created
function onCreate()
	CloseAllActorMenus()
	-- Register server event handlers
	-- Handles quest details
	Events.registerHandler("FRAMEWORK_PASS_QUEST_GIVER_DETAILS", onQuestGiverDetails)
	
	-- Register client-client event handlers
	KEP_EventRegisterHandler("updateQuestsClient", "UPDATE_QUEST_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateQuestsClientFull", "UPDATE_QUEST_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("questInfoClosingHandler", "FRAMEWORK_QUEST_INFO_CLOSING", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("buildModeChangedEventHandler", "BuildModeChangedEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)
	requestInventory(QUEST_PID)
	requestInventory(GAME_PID)

	m_imgProfile = gHandles["imgProfile"]
	m_imgDispElement = Image_GetDisplayElement(m_imgProfile)
	m_imgProfileBG = gHandles["imgProfileBG"]
	m_imgBGHandle = Image_GetDisplayElement(gHandles["imgProfileBG"])
end

function onDestroy()
	-- KEP_EventCreateAndQueue("FRAMEWORK_QUEST_TURNIN_CLOSING")
	if m_isRider then
		Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = m_currActorPID, interacting = false})
	end
end

-- Called when the client is moved or resized
function Dialog_OnMoved(dialogHandle, x, y)
	m_highestMenuY = nil
	if not m_questInfoDialog then
		MenuCenterThis()
	end
end

-- Button Handlers

-- Previous page
function btnBack_OnButtonClicked(buttonHandle)
	-- Destroy all current quest buttons, images and statics
	clearPage()
	m_questPage = m_questPage - 1
	displayQuests()
end

-- Next page
function btnNext_OnButtonClicked(buttonHandle)
	-- Destroy all current quest buttons, images and statics
	clearPage()
	m_questPage = m_questPage + 1
	displayQuests()
end

-- Close menu
function btnGoodbye_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

-- Event handlers

-- Info for a quest giver
function onQuestGiverDetails(event)
	m_isRider = event.isRider or false
	if m_isRider then
		m_riderPID = event.riderPID
		Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = event.actorPID, interacting = true})
	end
	local name 	 = event.name
	local GLID 	 = event.GLID
	local dialog = event.dialog
	m_quests 	 = event.quests
	m_rewardItems = event.rewardItems
	m_actorPID	 = event.PID

	m_image = event.greetingImage

	-- Set image
	local imgHandle = Image_GetDisplayElement(gHandles["imgProfile"])
	if imgHandle and gDialogHandle then
		if m_image  and m_image ~= "" then
			local lastSlashIndex = string.find(m_image, "/[^/]*$")
			local periodIndex = string.find(m_image,  '.', 1, true)
			if lastSlashIndex and periodIndex then
				local imageName =  string.sub(m_image, lastSlashIndex + 1, periodIndex - 1) or "none"
				m_imageFile = downloadImage(m_image, imageName.."_QuestGiver")
			end
		else
			KEP_LoadIconTextureByID(tonumber(GLID), imgHandle, gDialogHandle)
		end
	end
	
	-- Detect how tall this static is and position the dialog text accordingly
	local nameHeight = Static_GetStringHeight(gHandles["stcName"], name or "Quest Giver")
	Static_SetText(gHandles["stcName"], name or "Quest Giver")
	Control_SetSize(gHandles["stcName"], Control_GetWidth(gHandles["stcName"]), nameHeight)
	Control_SetVisible(gHandles["stcName"], true)
	
	-- Set the dialog text and position based on the name of the quest giver
	Static_SetText(gHandles["stcDialog"], dialog)
	local descriptionY = Control_GetLocationY(gHandles["stcName"]) + nameHeight + QUEST_DESCRIPTION_Y_BUFFER
	Control_SetLocationY(gHandles["stcDialog"], descriptionY)
	local descriptionHeight = Static_GetStringHeight(gHandles["stcDialog"], dialog)
	Control_SetSize(gHandles["stcDialog"], Control_GetWidth(gHandles["stcName"]), descriptionHeight)
	Control_SetVisible(gHandles["stcDialog"], true)
	
	local questGiverImageY = Control_GetLocationY(gHandles["imgProfile"]) + Control_GetHeight(gHandles["imgProfile"])
	m_topLineQuestY = math.max(questGiverImageY, (descriptionY + descriptionHeight)) + AVAILABLE_QUEST_Y_BUFFER
	Control_SetVisible(gHandles["imgProfile"], true)
	
	if m_quests and type(m_quests) == "table" and getQuestsCount(m_quests) > 0 then
		m_questsReceived = true
		if m_playerQuestsRecieved then
			clearPage()
			--log("--- onQuestGiverDetails")
			compileEligibleQuests()
			setTop()
			displayQuests()
		end
	else
		setBottom(m_topLineQuestY)
		Control_SetVisible(gHandles["stcAvailableQuests"], false)
	end
	
	Control_SetVisible(gHandles["btnClose"], true)
end

-- Act as if a specific quest has been clicked
function openQuest(questIndex)
	local index = tonumber(questIndex)

	local questState = m_questEligible[index]

	local acceptable = false
	local isModal = "false"
	if questState == "COMPLETE" then
		m_questInfoDialog = MenuOpen("Framework_QuestTurnIn.xml")
	elseif questState == "ACTIVE" or questState == "REP_ACTIVE" then
		m_questInfoDialog = MenuOpen("Framework_QuestInfo.xml")
	elseif questState == "AVAILABLE" or questState == "REP_AVAILABLE" then
		if isQuestCompletable(m_displayQuests[index].properties) then
			m_questInfoDialog = MenuOpen("Framework_QuestTurnIn.xml")
		else
			acceptable = true
			m_questInfoDialog = MenuOpen("Framework_QuestInfo.xml")
		end

		if m_displayQuests[index].properties.autoAccept then 
			isModal = "true"
		end
	end
	m_displayingQuest = index

	local menuHeight = Dialog_GetHeight(m_questInfoDialog)
	local clientHeight = Dialog_GetScreenHeight(m_questInfoDialog)
	local menuY = math.max(0,(clientHeight/2) - (menuHeight/2))
	
	--log("--- menuY: ".. tostring(menuY))
	Dialog_SetLocationY(m_questInfoDialog, menuY)
	--log("--- Dialog_SetLocation: ".. tostring(Dialog_GetLocationY(m_questInfoDialog)))
	MenuSetLocationThis(-4000, -4000)
	
	-- Send quest info over
	local ev = KEP_EventCreate("FRAMEWORK_QUEST_INFO") -- Handled by Framework_QuestInfo or Framework_QuestTurnIn
	KEP_EventEncodeString(ev, Events.encode(m_displayQuests[index]))
	KEP_EventEncodeString(ev, tostring(acceptable))
	KEP_EventEncodeString(ev, isModal)
	KEP_EventEncodeString(ev, Events.encode(m_displayRewards[index]))
	KEP_EventEncodeNumber(ev, m_actorPID)
	KEP_EventQueue(ev)
end

--Check if the quest should be auto-complete, such as if no item to turn in or no target character
function isQuestCompletable(questProperties)
	local questType = questProperties.questType
	local requiredItem = tostring(questProperties.requiredItem.requiredUNID) ~= "0"
	local targetCharacter = tostring(questProperties.targetUNID or 0) ~= "0" 
	local waypointPID = tostring(questProperties.waypointPID or 0) ~= "0"

	if (questType == "collect" or questType == "takeTo" or usesFulfillCount(questType)) and requiredItem == false then
		return true
	elseif (questType == "takeTo" or questType == "talkTo") and targetCharacter == false then
		return true
	elseif (questType == "goTo") and waypointPID == false then
		return true
	end

	return false
end

-- Update a specific quest
function updateQuestsClient(dispatcher, fromNetid, event, eventid, filter, objectid)
	--log("--- updateQuestsClient")
	local updateUNID = KEP_EventDecodeNumber(event)
	local updateQuest = decompileInventoryItem(event)
	
	for i=1,#m_playerQuests do
		if m_playerQuests[i] and tonumber(m_playerQuests[i].UNID) == tonumber(updateUNID) then
			m_playerQuests[i] = updateQuest
			
			-- If a quest is newly available
			if m_questsReceived then
				clearPage()
				--log("--- updateQuestsClient")
				compileEligibleQuests()
				setTop()
				displayQuests() -- PROBLEM LINE
			end
		end
	end
end

-- Receives all of a player's quests from InventoryHandler
function updateQuestsClientFull(dispatcher, fromNetid, event, eventid, filter, objectid)
	--log("--- updateQuestsClientFull")
	m_displayingQuest = nil
	m_playerQuests = decompileInventory(event)
	
	m_playerQuestsRecieved = true
	if m_questsReceived then
		clearPage()
		--log("--- updateQuestsClientFull")
		compileEligibleQuests()
		setTop()
		displayQuests()
	end
end

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
	compileEligibleQuests()
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
	compileEligibleQuests()
end


-- Handled when the QuestInfo menu closes
function questInfoClosingHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_questInfoDialog = nil
	local delay = KEP_EventDecodeString(event)
	if delay == "true" then
		--log("--- delay == true")
		if m_questEligible[m_displayingQuest] then
			clearPage()
			compileEligibleQuests()
			setTop()
		end
	else
		m_displayingQuest = nil
	end

	-- Reposition this menu front and center
	MenuCenterThis()

	if #m_displayQuests == 0 then
		MenuCloseThis()
	end 
end

function buildModeChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	g_build_enabled = filter
	MenuCloseThis()
	return KEP.EPR_OK
end

-- Local functions

-- Deletes all quest controls to get ready for the next page
function clearPage()
	--if m_questInfoDialog then return end  --This was one of main causes for display issue. Commenting this as I seen no reason to not clear this menu even if info is up
	local firstDisplayQuest = ((m_questPage-1)*QUESTS_PER_PAGE+1)
	local totalPages = math.ceil(getQuestsCount(m_quests) / QUESTS_PER_PAGE)
	for i = firstDisplayQuest, (firstDisplayQuest+QUESTS_PER_PAGE-1) do
		if m_quests[i] then
			Dialog_RemoveControl(gDialogHandle, "imgQuest"..tostring(i))
			Dialog_RemoveControl(gDialogHandle, "btnQuest"..tostring(i))
			Dialog_RemoveControl(gDialogHandle, "stcQuest"..tostring(i))
			Dialog_RemoveControl(gDialogHandle, "imgGlow"..tostring(i))			
		end
	end
end

-- Adds a quest to the Quest Giver list and re-positions the bottom line, Goodbye button and resizes the menu accordingly
function displayQuests()
	local placementY = m_baseLineQuestY
	
	-- Only display eligible quests
	local firstDisplayQuest = ((m_questPage-1)*QUESTS_PER_PAGE+1)
	local totalPages = math.ceil(getQuestsCount(m_displayQuests) / QUESTS_PER_PAGE)

	for i = firstDisplayQuest, (firstDisplayQuest+QUESTS_PER_PAGE-1) do
		if m_displayQuests[i] ~= nil and m_displayQuests[i].UNID ~= nil and m_questEligible[i] ~= nil then
			--log("--- displaying ".. i.. " quest")
			local questState = m_questEligible[i]
			local questUNID = m_displayQuests[i].UNID
			
			local questIcon = QUEST_ICON[questState]
			
			-- Create image glow
			local iconGlow = Dialog_AddImage(gDialogHandle, "imgGlow"..tostring(i), 107.5, placementY -7,ICON_GLOW_TEXTURE, ICON_GLOW_LEFT, ICON_GLOW_TOP, 
			ICON_GLOW_RIGHT,ICON_GLOW_BOTTOM)
			Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgGlow"..tostring(i)), false)
			Control_SetSize(iconGlow, ICON_GLOW_SIZE, ICON_GLOW_SIZE)

			if questState == 'COMPLETE' then
				Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgGlow"..tostring(i)), true)
				Image_SetColor(Dialog_GetImage(gDialogHandle,"imgGlow"..tostring(i)), { a=255, r=255, g = 230, b=0})
			end

			-- Create image
			local image = Dialog_AddImage(gDialogHandle, "imgQuest"..tostring(i), 114, placementY, questIcon.texture, questIcon.left, questIcon.top,
			questIcon.right, questIcon.bottom)
			Control_SetSize(image, QUEST_ICON_SIZE, QUEST_ICON_SIZE) -- Resize image

			-- Create static
			local static = copyControl(gHandles["stcQuestExample"])
			Control_SetName(static, "stcQuest"..tostring(i))
			Static_SetText(static, tostring(m_displayQuests[i].properties.name))

			if questState == 'REP_AVAILABLE' or questState == 'REP_ACTIVE' then
				Static_SetText(static, "<font color=#ff959aff>"..tostring(m_displayQuests[i].properties.name).."</font>")
			end

			Control_SetLocation(static, 148, placementY)
			local questHeight = Static_GetStringHeight(static, tostring(m_displayQuests[i].properties.name))
			local questWidth = Static_GetStringWidth(static, tostring(m_displayQuests[i].properties.name))
			Control_SetSize(static, Control_GetWidth(static), questHeight)
			Control_SetVisible(static, true)
			
			-- Create button
			local button = copyControl(gHandles["btnQuestExample"])
			Control_SetSize(button, questWidth + BAD_API_WIDTH_BUFFER, math.max(Control_GetHeight(button), questHeight))
			Control_SetName(button, "btnQuest"..tostring(i))
			Control_SetVisible(button, true)
			Control_SetLocation(button, 114, placementY)
			
			-- Update placement location for next quest
			placementY = placementY + math.max(QUEST_ICON_SIZE, questHeight) + QUEST_Y_BUFFER
			
			-- Assign button handler functions for each quest
			_G["btnQuest" .. tostring(i) .. "_OnButtonClicked"] = function()
				openQuest(i)
			end

			_G["btnQuest" .. tostring(i) .. "_OnMouseEnter"] = function(buttonHandle)
				-- Add the glow and bold when hovering over Quest
				Element_AddFont(Static_GetDisplayElement(static), gDialogHandle, "Verdana", -14, 800, false)

				Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgGlow"..tostring(i)), true)
			end

			_G["btnQuest" .. tostring(i) .. "_OnMouseLeave"] = function(buttonHandle)
				-- Removes the glow and bold when the mouse leaves
				Element_AddFont(Static_GetDisplayElement(static), gDialogHandle, "Verdana", -14, 400, false)
				Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgGlow"..tostring(i)), false)
			end
		end
	end
	-- Set the bottom line
	Control_SetLocationY(gHandles["imgLine3"], placementY)
	Control_SetVisible(gHandles["imgLine3"], true)
	placementY = placementY + LINE_Y_BUFFER
	
	local paginationEnabled = false
	-- Does a quest exist 1 past the current page?
	-- Or are we beyond page 1?
	if m_displayQuests[(QUESTS_PER_PAGE*m_questPage)+1] or (m_questPage > 1) then
		paginationEnabled = true
	end
	-- Can't go back if you're on page 1
	Control_SetEnabled(gHandles["btnBack"], m_questPage > 1)
	-- We're on the last page
	Control_SetEnabled(gHandles["btnNext"], m_questPage ~= totalPages)
	
	if paginationEnabled then
		Control_SetLocationY(gHandles["stcPage"], placementY)
		Control_SetLocationY(gHandles["btnBack"], placementY)
		Control_SetLocationY(gHandles["btnNext"], placementY)
		Static_SetText(gHandles["stcPage"], "("..tostring(m_questPage).."/"..tostring(totalPages)..")")
		placementY = placementY + Control_GetHeight(gHandles["btnNext"]) + GOODBYE_Y_BUFFER
	end
	Control_SetVisible(gHandles["stcPage"], paginationEnabled)
	Control_SetVisible(gHandles["btnBack"], paginationEnabled)
	Control_SetVisible(gHandles["btnNext"], paginationEnabled)
	
	setBottom(placementY)
end

-- Sets the "Available Quests" text and line if applicable
function setTop()
	-- Baseline for Available Quests is a static buffer from the bottom of the quest giver image
	Control_SetLocationY(gHandles["stcAvailableQuests"], m_topLineQuestY)
	local eligibileQuestCount = getQuestsCount(m_questEligible)
	local displayQuestCount = getQuestsCount(m_displayQuests)
	--log("--- eligibileQuestCount: ".. eligibileQuestCount)
	--log("--- displayQuestCount: ".. displayQuestCount)
	if eligibileQuestCount > 0 or displayQuestCount > 0 then
	--if displayQuestCount > 0 then
		
		Control_SetVisible(gHandles["stcAvailableQuests"], true)
		Control_SetLocationY(gHandles["imgLine2"], m_topLineQuestY + 20)
		Control_SetVisible(gHandles["imgLine2"], true)
		
		m_baseLineQuestY = m_topLineQuestY + 25 + LINE_Y_BUFFER
	else
		Control_SetVisible(gHandles["stcAvailableQuests"], false)
		Control_SetVisible(gHandles["imgLine2"], false)
		
		m_baseLineQuestY = m_topLineQuestY
	end
end

function setBottom(placementY)
	--log("--- Framework_QuestGiver setBottom")
	-- Position the Goodbye button
	Control_SetLocationY(gHandles["btnGoodbye"], placementY)
	local menuHeight = Control_GetLocationY(gHandles["btnGoodbye"]) + Control_GetHeight(gHandles["btnGoodbye"]) + GOODBYE_Y_BUFFER
	
	Control_SetLocationX(gHandles["btnGoodbye"], (Dialog_GetWidth(gDialogHandle)/2)-(Control_GetWidth( gHandles["btnGoodbye"] )/2) )
	Control_SetVisible(gHandles["btnGoodbye"], true)
	--Control_SetVisible(gHandles["stcAvailableQuests"], false)
	-- Resize the menu accordingly
	Dialog_SetSize(gDialogHandle, Dialog_GetWidth(gDialogHandle), menuHeight)
	if not m_questInfoDialog then
		MenuCenterThis()
	end
end

-- Gets the state of a quest for a player
function getQuestState(quest)
	local questState = "NONE"
	local playerQuest
	for i, quest2 in pairs(m_playerQuests) do
		-- Got a match
		if quest2.UNID == quest.UNID then
			playerQuest = quest2
			break
		end
	end
	-- Player is actively on this quest
	if playerQuest then
		if playerQuest.active then
			if playerQuest.fulfilled then
				questState = "COMPLETE"
			elseif playerQuest.properties.questType == "collect" and playerQuest.properties.requiredItem and playerQuest.properties.requiredItem.requiredUNID ~= "0" then
				local questReqCount = playerQuest.properties.requiredItem.requiredCount or 0
				local playerItemCount = playerQuest.properties.requiredItem.progressCount or 0
				
				-- Player's ready to complete this quest
				if playerItemCount >= questReqCount then
					questState = "COMPLETE"
					
				-- Quest isn't ready to turn in yet
				else
					-- Quest is repeatable
					if quest.properties.repeatable == "true" then
						questState = "REP_ACTIVE"
					else
						questState = "ACTIVE"
					end
				end
			else
			-- Quest is repeatable
				if quest.properties.repeatable == "true" then
					questState = "REP_ACTIVE"
				else
					questState = "ACTIVE"
				end
			end
		
		-- Player has completed this quest before
		elseif playerQuest.complete then
			-- Repeatable quests have the opportunity to be re-acceptable if the player's off his/her cooldown
			if quest.properties.repeatable == "true" and playerQuest.repeatTimerUp then
				questState = "REP_AVAILABLE"
			end
		end
	else
		-- Player has never seen this quest before
		-- Quest has no prerequisite
		if tonumber(quest.properties.prerequisiteQuest) == 0 then
			-- Quest is repeatable
			if quest.properties.repeatable == "true" then
				questState = "REP_AVAILABLE"
			else
				questState = "AVAILABLE"
			end
		-- Quest has a prerequisite
		else
			for j, playerQuest2 in pairs(m_playerQuests) do
				-- Player has completed the prerequisite quest
				if (tonumber(quest.properties.prerequisiteQuest) == tonumber(playerQuest2.UNID)) and playerQuest2.complete then
					-- Quest is repeatable
					if quest.properties.repeatable == "true" then
						questState = "REP_AVAILABLE"
					else
						questState = "AVAILABLE"
					end
					break
				end
			end
		end
	end
	return questState
end

-- Compile the eligible quests table
function compileEligibleQuests()
	-- Construct the quest eligibility table every time we display the quests
	m_displayQuests = {}
	m_displayRewards = {}
	m_questEligible = {}
	for i=1, #m_quests do
		-- Don't add a quest currently being displayed
		if i ~= m_displayingQuest then
			local questEligible = getQuestState(m_quests[i])

			--log("--- QUEST eligibility ".. tostring(questEligible))

			-- Only populate with eligible quests
			if questEligible ~= "NONE" then
				local rewardUNID = tostring(m_rewardItems[i].UNID)
				if m_gameItems[rewardUNID] then
					m_rewardItems[i].properties = m_gameItems[rewardUNID]
				end
				table.insert(m_displayQuests, m_quests[i])
				table.insert(m_questEligible, questEligible)
				table.insert(m_displayRewards, m_rewardItems[i])
			end
		end
	end

	--log("--------------------------- compileEligibleQuests ".. tostring(#m_questEligible))
end

--Copy and paste controls
function copyControl(menuControl)
	if menuControl then
		Dialog_CopyControl( gDialogHandle, menuControl )
		Dialog_PasteControl( gDialogHandle )

		local numControls = Dialog_GetNumControls( gDialogHandle )

		local newControl = Dialog_GetControlByIndex( gDialogHandle, numControls - 1 )
		local controlName = Control_GetName( newControl )

		gHandles[controlName] = newControl	

		return newControl
	end
end

-- Gets the number of quests in an array (not counting empty indices)
function getQuestsCount(questList)
	local questsCount = 0
	for i,info in pairs(questList) do
		if info ~= nil and info.UNID ~= nil then
			questsCount = questsCount + 1
		end
	end
	return questsCount
end


--------------------------
-----Images
--------------------------

function downloadImage(imgURL, chapterName)
	--the name the image will saved as

	local filename = tostring(chapterName)..".jpg"
	local path = KEP_DownloadTextureAsyncToGameId(filename, imgURL)
	
    local gameId = KEP_GetGameId()
	
	path = string.gsub(path,".-\\", "" )
	path = "../../CustomTexture/".. gameId .. "/" .. path
	return path
end


function textureDownloadHandler()
	updateImage()
	--scaleImage(gHandles["imgProfile"], gHandles["imgProfileBG"], m_imageFile)
end

function updateImage()
	Element_AddTexture(m_imgDispElement, gDialogHandle,m_imageFile)
	local textureWidth = Element_GetTextureWidth(m_imgDispElement, gDialogHandle)
	local textureHeight = Element_GetTextureHeight(m_imgDispElement, gDialogHandle)

	m_imageWidth = textureWidth
	scaleQuestImage(textureWidth, textureHeight)
end

function scaleQuestImage(width, height)
	local newWidth = width
	local newHeight = height

	if width <= height then
		if width ~= IMAGE_SIZE then
			newHeight = height * (IMAGE_SIZE / width)
			newWidth = IMAGE_SIZE
		end
	else
		if height ~= IMAGE_SIZE then
			newWidth = width * (IMAGE_SIZE / height) 
			newHeight = IMAGE_SIZE
		end
	end
	setQuestImage(newWidth, newHeight)
	Element_SetCoords(m_imgDispElement, 0, 0, LeastPowerofTwo(width), LeastPowerofTwo(height))
end

function setQuestImage(width, height)
	Control_SetSize(m_imgProfile, width, height)
	if height > IMAGE_SIZE then
		local heightDifference  = height - IMAGE_SIZE
		local moveDistance = heightDifference / 2
		Control_SetLocation(m_imgProfile, Control_GetLocationX(m_imgProfile), Control_GetLocationY(m_imgProfile)-moveDistance)
	end

	if width > IMAGE_SIZE then
		local widthDifference  = width - IMAGE_SIZE
		local moveDistance = widthDifference / 2
		Control_SetLocation(m_imgProfile, Control_GetLocationX(m_imgProfile) - moveDistance, Control_GetLocationY(m_imgProfile))
	end

	local containerX, containerY = Control_GetLocationX(m_imgProfileBG), Control_GetLocationY(m_imgProfileBG)
	local containerW, containerH = Control_GetWidth(m_imgProfileBG), Control_GetHeight(m_imgProfileBG)
	Control_SetClipRect(m_imgProfile, containerX, containerY, containerX+containerW, containerY+containerH)
end