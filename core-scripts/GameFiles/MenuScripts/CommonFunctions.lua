--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua") 

dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\NudgeIds.lua")
dofile("..\\Scripts\\FameIds.lua")
dofile("..\\Scripts\\ZoneNavIds.lua")

dofile("..\\MenuScripts\\WebSuffix.lua")

dofile("..\\MenuScripts\\LogFunctions.lua") -- drf - added
dofile("..\\MenuScripts\\MenuFunctions.lua") -- drf - added
dofile("..\\MenuScripts\\ObjectFunctions.lua") -- drf - added
dofile("..\\MenuScripts\\ProgressFunctions.lua") -- drf - added

-- DRF - Global JSON Library (LuaHelper::cjson.dll loaded during vm creation)
JSON = cjson -- alias global cjson table

-- Extensions for loading friend thumb or avatar pic 
FRIEND_PIC_EXT			= ".fp"
FRIEND_THUMB_EXT		= ".fp_thumb" 
GIFT_PIC_EXT			= ".gt"
CUSTOM_TEXTURE_THUMB	= ".ct_thumb"

-- Path for the default avatar image
DEFAULT_AVATAR_PIC_PATH = "/images/KanevaIconUnknownGender.jpg"
DEFAULT_KANEVA_LOGO = "KanevaIconBroadband.gif"
DEFAULT_KANEVA_PLACE_ICON = "defaultworld.jpg"

-- Event names
YES_NO_BOX_EVENT = "YesNoBoxEvent"

-- DRF - Cursor Modes (from KEPConstants.h)
CURSOR_MODE_NONE = 0
CURSOR_MODE_MENU = 1
CURSOR_MODE_WIDGET = 2
CURSOR_MODE_OBJECT = 3
CURSOR_MODE_WORLD = 4
CURSOR_MODES = 5

-- DRF - Cursor Types (from KEPConstants.h)
CURSOR_TYPE_NONE = 0
CURSOR_TYPE_DEFAULT = 1
CURSOR_TYPE_CLICKABLE = 2
CURSOR_TYPE_DRAGGABLE = 3
CURSOR_TYPE_ROTATABLE = 4
CURSOR_TYPE_TRANSLATABLE = 5
CURSOR_TYPE_ATTACKABLE = 6
CURSOR_TYPE_ATTACKABLE2 = 7
CURSOR_TYPE_HEALABLE = 8
CURSOR_TYPES = 9

-- DRF - MouseOverModes (from IClientEngine.h)
MOUSEOVER_NONE = 0
MOUSEOVER_LIGHTEN = 1
MOUSEOVER_OUTLINE = 2
MOUSEOVER_CURSOR = 4

-- Element Text Style
TEXT_HALIGN_LEFT = 0
TEXT_HALIGN_CENTER = 1
TEXT_HALIGN_RIGHT = 2
TEXT_VALIGN_TOP	= 0
TEXT_VALIGN_CENTER = 4
TEXT_VALIGN_BOTTOM = 8
TEXT_WORDWRAP	   = 16

--Dialog Alignment
IMDL_NONE = 0
IMDL_TOP = 1
IMDL_LEFT = 2
IMDL_CENTER = 4
IMDL_RIGHT = 8
IMDL_VCENTER = 16
IMDL_BOTTOM = 32

-- Use types
USE_TYPE = {}
USE_TYPE.NONE = 0
USE_TYPE.HEALING = 1
USE_TYPE.AMMO = 2
USE_TYPE.SKILL = 3
USE_TYPE.MENU = 4
USE_TYPE.COMBINE = 5
USE_TYPE.PLACE_HOUSE = 6
USE_TYPE.INSTALL_HOUSE = 7
USE_TYPE.SUMMON_CREATURE = 8
USE_TYPE.OLD_SCRIPT = 9
USE_TYPE.ADD_DYN_OBJ = 10
USE_TYPE.ADD_ATTACH_OBJ = 11
USE_TYPE.MOUNT = 12
USE_TYPE.EQUIP = 13

USE_TYPE.FIRST_TYPE = 1
USE_TYPE.LAST_TYPE = 13

USE_TYPE.FIRST_EVENT = 200
USE_TYPE.ANIMATION = USE_TYPE.FIRST_EVENT + 0;
USE_TYPE.P2P_ANIMATION = USE_TYPE.FIRST_EVENT + 1;
USE_TYPE.NPC = USE_TYPE.FIRST_EVENT + 2;
USE_TYPE.DEED = USE_TYPE.FIRST_EVENT + 3;
USE_TYPE.QUEST = USE_TYPE.FIRST_EVENT + 4;
USE_TYPE.PREMIUM = USE_TYPE.FIRST_EVENT + 5;
USE_TYPE.PARTICLE = USE_TYPE.FIRST_EVENT + 6;
USE_TYPE.SOUND = USE_TYPE.FIRST_EVENT + 7;
USE_TYPE.BUNDLE = USE_TYPE.FIRST_EVENT + 8;
USE_TYPE.CUSTOM_DEED = USE_TYPE.FIRST_EVENT + 9;
USE_TYPE.ACTION_ITEM = USE_TYPE.FIRST_EVENT + 10;
USE_TYPE.GAME_ITEM = USE_TYPE.FIRST_EVENT + 11;

CORE_GLID_START = 1
CORE_GLID_END = 99

-- Webcall Playlist Types
PLAYLIST_TYPE_TV = "m" -- DYNAMIC_OBJTYPE_MEDIA_TV
PLAYLIST_TYPE_FRAME	= "s" -- DYNAMIC_OBJTYPE_MEDIA_FRAME (glids { 1770, 1771, 1772, 2999 })
PLAYLIST_TYPE_GAME = "g" -- DYNAMIC_OBJTYPE_MEDIA_GAME (isInteractive)

CAM_ENABLED_ADJUST_ANGLE = 20*(math.pi/180) -- 20 degrees
CAM_DISABLED_ADJUST_ANGLE = (math.pi/2) -- 90 degrees

-- Common Functions Loggers
local function cfLog(txt) Log("CommonFunctions::"..txt) end
local function cfLogWarn(txt) LogWarn("CommonFunctions::"..txt) end
local function cfLogError(txt) LogError("CommonFunctions::"..txt) end

---------------------------------------------------------------------
-- DRF - New Type Conversion Functions
---------------------------------------------------------------------

--[[ DRF - Converts anything to numeric BOOL (0/1) ]]
function ToBOOL(b)
	if ((b ~= nil) and (b ~= false) and (b ~= 0) and (b ~= "false")) then return 1 else return 0 end
end

--[[ DRF - Converts anything to natural bool (false/true) ]]
function ToBool(b)
	if ((b ~= nil) and (b ~= false) and (b ~= 0) and (b ~= "false")) then return true else return false end
end

---------------------------------------------------------------------
-- DRF - New Table Functions
---------------------------------------------------------------------

-- DRF - Returns the concatination of the given tables
function TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

---------------------------------------------------------------------
-- DRF - World Type Functions
---------------------------------------------------------------------

-- DRF - Performs Webcall To Reset World Requests Globally & Sends GlobalWorldReqsResetEvent
function GlobalWorldReqsReset()
	local gameId = KEP_GetStarId() or KEP.GID_PRO_STR
	cfLog("GlobalWorldReqsReset: gameId="..gameId)

	-- Reset Global Worlds Request Flag
	local url = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX .. "action=ClearAppNotification"
	url = url .. "&gameId=" .. gameId
	makeWebCall(url, 0)

	-- Register & Send Global Worlds Requests Reset Event (handler in Dashboard)
	KEP_EventRegister("GlobalWorldReqsResetEvent", KEP.MED_PRIO)
	KEP_EventCreateAndQueue("GlobalWorldReqsResetEvent")
end

-- DRF - Performs webcall to reset given world requests
function WorldReqsReset(gameId)
	gameId = gameId or KEP_GetStarId() or KEP.GID_PRO_STR
	cfLog("WorldReqsReset: gameId="..gameId)

	-- Perform 'ClearAppNotification' Web Call
	local url = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX .. "action=ClearAppNotification"
	url = url .. "&gameId=" .. gameId
	makeWebCall(url, 0)
end

---------------------------------------------------------------------
-- DRF - New Control Functions
---------------------------------------------------------------------

function setControlEnabled(ctrlName, enable)
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return end
	Control_SetEnabled( ctrl, ToBool(enable) )
	Control_SetVisible( ctrl, ToBool(enable) )
end
SetControlEnabled = setControlEnabled

function setControlVisible(ctrlName, enable)
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return end
	Control_SetVisible(ctrl, ToBool(enable))
end
SetControlVisible = setControlVisible

function getControlVisible(ctrlName)
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return false end
	return ToBool(Control_GetVisible(ctrl))
end
GetControlVisible = getControlVisible

function setControlRefresh(ctrlname)
	local visible = getControlVisible(ctrlname)
	setControlVisible(ctrlname, not visible)
	setControlVisible(ctrlname, visible)
end
SetControlRefresh = setControlRefresh

function getControlContainsPoint(ctrlName, x, y)
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return false end
	return ToBool(Control_ContainsPoint(ctrl, x, y))
end
GetControlContainsPoint = getControlContainsPoint

function setControlRelocate(ctrlName, deltaX, deltaY) 
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return end
	if (deltaX ~= 0) then Control_SetLocationX(ctrl, Control_GetLocationX(ctrl) + deltaX) end
	if (deltaY ~= 0) then Control_SetLocationY(ctrl, Control_GetLocationY(ctrl) + deltaY) end
end
SetControlRelocate = setControlRelocate

function setControlResize(ctrlName, deltaX, deltaY)
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return end
	local x = Control_GetWidth(ctrl)
	local y = Control_GetHeight(ctrl)
	Control_SetSize(ctrl, x + deltaX, y + deltaY)
end
SetControlResize = setControlResize

function getControlWidth(ctrlName)
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return 0 end
	return Control_GetWidth(ctrl)
end
GetControlWidth = getControlWidth

function setControlSize(ctrlName, sizeX, sizeY)
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return end
	Control_SetSize(ctrl, sizeX, sizeY)
end
SetControlSize = setControlSize

function setControlText(ctrlName, text)
	local ctrl = gHandles[ctrlName]
	local ctrlType = Control_GetType(ctrl)
	if (ctrlType == ControlTypes.DXUT_CONTROL_EDITBOX) then
		EditBox_SetText(ctrl, text, false)
	else
		Static_SetText(ctrl, text)
	end
end
SetControlText = setControlText

function setControlTexture(ctrlName, imgName)
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return end
	Element_AddTexture(Image_GetDisplayElement(ctrl), gDialogHandle, imgName)
end
SetControlTexture = setControlTexture

function setControlImage(ctrlName, imgFile)
	local ctrlPic = gHandles[ctrlName .. "_Pic"]
	local ctrlFrame = gHandles[ctrlName .. "_BG"]
	if (ctrlPic == nil or ctrlFrame == nil) then return end
	scaleImage(ctrlPic, ctrlFrame, imgFile)
end
SetControlImage = setControlImage

function setControlProgBarPercent(ctrlName, percent)
	local ctrlFg = gHandles[ctrlName]
	local ctrlBg = gHandles[ctrlName.."BG"]
	if (ctrlFg == nil or ctrlBg == nil) then return end
	local w = Control_GetWidth(ctrlBg)
	local h = Control_GetHeight(ctrlBg)
	Control_SetSize(ctrlFg, (w * percent) / 100.0, h)
end
SetControlProgBarPercent = setControlProgBarPercent

function getControlChecked(ctrlName)
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return false end
	local ctrlType = Control_GetType(ctrl)
	if (ctrlType == ControlTypes.DXUT_CONTROL_RADIOBUTTON) then
		return ToBool(CheckBox_GetChecked(RadioButton_GetCheckBox(ctrl)))
	elseif (ctrlType == ControlTypes.DXUT_CONTROL_CHECKBOX) then
		return ToBool(CheckBox_GetChecked(ctrl))
	end
	return false
end
GetControlChecked = getControlChecked

function setControlChecked(ctrlName, checked) 
	local ctrl = gHandles[ctrlName]
	if (ctrl == nil) then return end
	local ctrlType = Control_GetType(ctrl)
	if (ctrlType == ControlTypes.DXUT_CONTROL_RADIOBUTTON) then
		RadioButton_SetChecked(ctrl, checked)
	elseif (ctrlType == ControlTypes.DXUT_CONTROL_CHECKBOX) then
		CheckBox_SetChecked(ctrl, checked)
	end
end
SetControlChecked = setControlChecked


function getControl(ctl)
	if (type(ctl) == "string") then
		return Dialog_GetControl( gDialogHandle, ctl )
	else
		return ctl
	end
end

function enableControl( ctl, enable )
	ctl = getControl(ctl)
	if ctl~=nil then
		Control_SetEnabled( ctl, ToBOOL(enable) )
	end
end

function showControl( ctl )
	ctl = getControl(ctl)
	if ctl~=nil then
		Control_SetVisible( ctl, ToBOOL(true) )
	end
end

function hideControl( ctl )
	ctl = getControl(ctl)
	if ctl~=nil then
		Control_SetVisible( ctl, ToBOOL(false) )
	end
end

function setControl( ctl, enable )
	ctl = getControl(ctl)
	if ctl~=nil then
		Control_SetEnabled( ctl, ToBOOL(enable) )
		Control_SetVisible( ctl, ToBOOL(enable) )
	end
end

function moveControl( ctl, x, y )
	ctl = getControl(ctl)
	if ctl~=nil then
		Control_SetLocation( ctl, x, y)
	end
end

function focusControl( ctl )
	ctl = getControl(ctl)
	if ctl~=nil then
		Control_SetFocus( ctl )
	end
end

function setStaticText( staticName, text )
	local ctl = Dialog_GetStatic( gDialogHandle, staticName )
	if ctl~=nil then
		Static_SetText( ctl, text )
	end
end

function getStaticText( staticName )
	local ctl = Dialog_GetStatic( gDialogHandle, staticName )
	if ctl~=nil then
		return Static_GetText( ctl )
	end
	return nil
end

function setEditBoxText( editBoxName, text, selected )
	local ctl = getControl(editBoxName)
	if ctl~=nil then
		selected = selected or false
		EditBox_SetText( ctl, text, ToBOOL(selected) )
	end
end

function getEditBoxText( editBoxName )
	local ctl = getControl(editBoxName)
	if ctl~=nil then
		return EditBox_GetText( ctl )
	end
	return nil
end

function addListBoxItem( listBoxName, text, data )
	local ctl = Dialog_GetListBox( gDialogHandle, listBoxName )
	if ctl~=nil then
		ListBox_AddItem( ctl, text, data )
	end
end

function addListBoxItems( listBoxName, items )
	local ctl = Dialog_GetListBox( gDialogHandle, listBoxName )
	if ctl~=nil then
		for idx, item in pairs(items) do
			ListBox_AddItem( ctl, item.text, item.data )
		end
	end
end

function insertListBoxItem( listBoxName, itemIndex, text, data )
	local ctl = Dialog_GetListBox( gDialogHandle, listBoxName )
	if ctl~=nil then
		ListBox_InsertItem( ctl, itemIndex, text, data )
	end
end

function removeListBoxItem( listBoxName, itemIndex )
	local ctl = Dialog_GetListBox( gDialogHandle, listBoxName )
	if ctl~=nil then
		ListBox_RemoveItem( ctl, itemIndex )
	end
end

function removeAllListBoxItems( listBoxName )
	local ctl = Dialog_GetListBox( gDialogHandle, listBoxName )
	if ctl~=nil then
		ListBox_RemoveAllItems( ctl )
	end
end

function getListBoxSelectedItem( listBoxName )
	local ctl = Dialog_GetListBox( gDialogHandle, listBoxName )
	if ctl~=nil then
		local idx = ListBox_GetSelectedItemIndex( ctl )
		local text = ListBox_GetSelectedItemText( ctl )
		local data = ListBox_GetSelectedItemData( ctl )
		return idx, text, data
	end
end

function getListBoxItem( listBoxName, index )
	local ctl = Dialog_GetListBox( gDialogHandle, listBoxName )
	if ctl~=nil then
		local text = ListBox_GetItemText( ctl, index )
		local data = ListBox_GetItemData( ctl, index )
		return text, data
	end
end

function selectListBoxItemByIndex( listBoxName, itemIndex )
	local ctl = Dialog_GetListBox( gDialogHandle, listBoxName )
	if ctl~=nil then
		ListBox_SelectItem( ctl, itemIndex )
	end
end

function selectListBoxItemByData( listBoxName, itemData )
	local ctl = Dialog_GetListBox( gDialogHandle, listBoxName )
	if ctl~=nil then
		local itemIndex = ListBox_FindItem( ctl, itemData )
		if itemIndex~=nil and itemIndex>=0 then
			ListBox_SelectItem( ctl, itemIndex )
		end
	end
end

function clearListBoxSelection( listBoxName )
	local ctl = Dialog_GetListBox( gDialogHandle, listBoxName )
	if ctl~=nil then
		ListBox_ClearSelection( ctl )
	end
end

function addComboBoxItem( comboBoxName, text, data )
	local ctl = Dialog_GetComboBox( gDialogHandle, comboBoxName )
	if ctl~=nil then
		ComboBox_AddItem( ctl, text, data )
	end
end

function addComboBoxItems( comboBoxName, items )
	local ctl = Dialog_GetComboBox( gDialogHandle, comboBoxName )
	if ctl~=nil then
		for idx, item in pairs(items) do
			ComboBox_AddItem( ctl, item.text, item.data )
		end
	end
end

function removeComboBoxItem( comboBoxName, itemIndex )
	local ctl = Dialog_GetListBox( gDialogHandle, comboBoxName )
	if ctl~=nil then
		ComboBox_RemoveItem( ctl, itemIndex )
	end
end

function getComboBoxSelectedItem( comboBoxName )
	local ctl = Dialog_GetComboBox( gDialogHandle, comboBoxName )
	if ctl~=nil then
		local text = ComboBox_GetSelectedText( ctl )
		local data = ComboBox_GetSelectedData( ctl )
		return text, data
	end
end

function selectComboBoxItemByIndex( comboBoxName, itemIndex )
	local ctl = Dialog_GetComboBox( gDialogHandle, comboBoxName )
	if ctl~=nil then
		ComboBox_SetSelectedByIndex( ctl, itemIndex )
	end
end

function selectComboBoxItemByData( comboBoxName, itemData )
	local ctl = Dialog_GetComboBox( gDialogHandle, comboBoxName )
	if ctl~=nil then
		ComboBox_SetSelectedByData( ctl, itemData )
	end
end

function selectComboBoxItemByText( comboBoxName, itemText )
	local ctl = Dialog_GetComboBox( gDialogHandle, comboBoxName )
	if ctl~=nil then
		ComboBox_SetSelectedByText( ctl, itemText )
	end
end

function setupScrollBarDefaultTextures( hScrollBar )
	if hScrollBar ~= 0 then
		local eh = ScrollBar_GetTrackDisplayElement(hScrollBar)
		
		if eh ~= 0 then
			Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 37, 17, 69)
		end
		
		eh = ScrollBar_GetButtonDisplayElement(hScrollBar)
		if eh ~= 0 then
			Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 19, 17, 35)
		end
		
		eh = ScrollBar_GetUpArrowDisplayElement(hScrollBar)
		if eh ~= 0 then
			Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 1, 17, 17)
		end
		
		eh = ScrollBar_GetDownArrowDisplayElement(hScrollBar)
		if eh ~= 0 then
			Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 71, 17, 87)
		end
	end
end

function setupScrollBarLegacyTextures( hScrollBar )

	if hScrollBar ~= 0 then
		local eh = ScrollBar_GetTrackDisplayElement(hScrollBar)
		
		if eh ~= 0 then
			--Blue body
			Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
			Element_SetCoords(eh, 361, 579, 390, 586)
		end

		eh = ScrollBar_GetButtonDisplayElement(hScrollBar)
		if eh ~= 0 then
			--Grey lined
			Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
			Element_SetCoords(eh, 395, 573, 409, 588)
		end

		eh = ScrollBar_GetUpArrowDisplayElement(hScrollBar)
		if eh ~= 0 then
			--Up arrow button
			Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
			Element_SetCoords(eh, 395, 547, 409, 561)
		end

		eh = ScrollBar_GetDownArrowDisplayElement(hScrollBar)
		if eh ~= 0 then
			--Down Arrow Button
			Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
			Element_SetCoords(eh, 395, 642, 409, 656)
		end
	end
end

function setupListBoxScrollBarDefaultTextures( listBoxName )
	-- setup default look of listbox scrollbar
    local lbh = Dialog_GetListBox(gDialogHandle, listBoxName)
    if lbh ~= 0 then		
		setupScrollBarDefaultTextures( ListBox_GetScrollBar(lbh) )
	end
end

function setupListBoxScrollBarLegacyTextures(listBoxName)
	-- setup legacy look of listbox scrollbar
	local lbh = Dialog_GetListBox(gDialogHandle, listBoxName)
	if lbh ~= 0 then
		setupScrollBarLegacyTextures( ListBox_GetScrollBar(lbh) )
	end
end

function setupComboBoxScrollBarDefaultTextures( comboBoxName )
	-- setup default look of combobox scrollbar
    local cbh = Dialog_GetComboBox(gDialogHandle, comboBoxName)
    if cbh ~= 0 then
		setupScrollBarDefaultTextures( ComboBox_GetScrollBar(cbh) )
	end
end

function setupComboBoxScrollBarLegacyTextures(comboBoxName)
	-- setup legacy look of combobox scrollbar
    local cbh = Dialog_GetComboBox(gDialogHandle, comboBoxName)
    if cbh ~= 0 then		
		setupScrollBarLegacyTextures( ListBox_GetScrollBar(cbh) )
	end
end

function ShowMenuLightbox(dialogHandle, shadowTopHandle, shadowBottomHandle, shadowLeftHandle, shadowRightHandle, alpha)
	local screenWidth = Dialog_GetScreenWidth(dialogHandle)
	local screenHeight = Dialog_GetScreenHeight(dialogHandle)

	local dialogLoc = MenuGetLocationThis()
	local dialogSize = MenuGetSizeThis()
	local titlebarHeight = 30

	-- Set shadow location and size
	Control_SetLocation(shadowTopHandle, -dialogLoc.x , -dialogLoc.y - titlebarHeight)
	Control_SetSize(shadowTopHandle, screenWidth, dialogLoc.y)
	Control_SetLocation(shadowBottomHandle, -dialogLoc.x, dialogSize.height - titlebarHeight)
	Control_SetSize(shadowBottomHandle, screenWidth, screenHeight - (dialogLoc.y + dialogSize.height))
	Control_SetLocation(shadowRightHandle, dialogSize.width, -titlebarHeight)
	Control_SetSize(shadowRightHandle, screenWidth - (dialogLoc.x + dialogSize.width), dialogSize.height)
	Control_SetLocation(shadowLeftHandle, -dialogLoc.x , -titlebarHeight)
	Control_SetSize(shadowLeftHandle, dialogLoc.x, dialogSize.height)

	-- Enable the shadow panels
	Control_SetVisible(shadowTopHandle, true)
	Control_SetVisible(shadowBottomHandle, true)
	Control_SetVisible(shadowRightHandle, true)
	Control_SetVisible(shadowLeftHandle, true)

	Control_SetEnabled(shadowTopHandle, true)
	Control_SetEnabled(shadowBottomHandle, true)
	Control_SetEnabled(shadowRightHandle, true)
	Control_SetEnabled(shadowLeftHandle, true)

	-- Set shadow alpha, default to 100 if not provided
	alpha = alpha or 100
	local colorTopHandle = Element_GetTextureColor(Image_GetDisplayElement(shadowTopHandle))
	local colorBottomHandle = Element_GetTextureColor(Image_GetDisplayElement(shadowBottomHandle))
	local colorLeftHandle = Element_GetTextureColor(Image_GetDisplayElement(shadowLeftHandle))
	local colorRightHandle = Element_GetTextureColor(Image_GetDisplayElement(shadowRightHandle))

	BlendColor_SetColor(colorTopHandle, 0, {a=alpha, r=0, g=0, b=0})
	BlendColor_SetColor(colorBottomHandle, 0, {a=alpha, r=0, g=0, b=0})
	BlendColor_SetColor(colorLeftHandle, 0, {a=alpha, r=0, g=0, b=0})
	BlendColor_SetColor(colorRightHandle, 0, {a=alpha, r=0, g=0, b=0})


	-- Mute the world
	setGlobalSoundTempMute(true)
end

---------------------------------------------------------------------
-- DRF - New Parse Functions
---------------------------------------------------------------------

--[[ DRF - Scans the given xml string for the given tag and returns it's value as a string or valueDefault. ]]
function ParseTagStr(txtParse, tag, valueDefault, silent)
	valueDefault = valueDefault or ""
	silent = silent or false 
	_, _, valueParsed = string.find(txtParse, "<"..tag..">(.-)</"..tag..">")
	local valueStr = valueParsed or valueDefault
	if (valueParsed == nil and not silent) then
		cfLogError("ParseTagStr: <"..tag.."> NOT FOUND - value='"..valueStr.."'")
	else
--		cfLog("ParseTagStr: <"..tag.."> => '"..valueStr.."'")
	end
	return valueStr
end

--[[ DRF - Scans the given xml string for the given tag and returns it's value as a number or valueDefault. ]]
function ParseTagNum(txtParse, tag, valueDefault, silent)
	valueDefault = valueDefault or 0
	silent = silent or false
	_, _, valueParsed = string.find(txtParse, "<"..tag..">(.-)</"..tag..">")
	local valueNum = tonumber(valueParsed) or valueDefault
	if (valueParsed == nil and not silent) then
		cfLogError("ParseTagNum: <"..tag.."> NOT FOUND - value="..valueNum)
	else
--		cfLog("ParseTagNum: <"..tag.."> => "..valueNum)
	end
	return valueNum
end

--[[ DRF - Scans the given xml string for the given tag and returns it's value as a bool or valueDefault. ]]
function ParseTagBool(txtParse, tag, valueDefault, silent)
	valueDefault = valueDefault or false
	silent = silent or false
	_, _, valueParsed = string.find(txtParse, "<"..tag..">(.-)</"..tag..">")
	local valueBool = (valueParsed == "true") or valueDefault
	if (valueParsed == nil and not silent) then
		cfLogError("ParseTagBool: <"..tag.."> NOT FOUND - value="..tostring(valueBool))
	else
--		cfLog("ParseTagBool: <"..tag.."> => "..tostring(valueBool))
	end
	return valueBool
end

--[[ DRF - Scans the given xml string for the <ReturnCode> tag and returns true if it is "0" otherwise false. ]]
function ParseTagReturnCodeOk(txtParse, silent)
	local ok = (ParseTagStr(txtParse, "ReturnCode", silent) == "0")
	if (not ok and not silent) then
		cfLogError("ParseTagReturnCodeOk: NOT OK\n"..txtParse)
	else
--		cfLog("ParseTagRetrunCodeOk: OK")
	end
	return ok
end

---------------------------------------------------------------------
-- DRF - New General Functions
---------------------------------------------------------------------

--[[ DRF - Returns true if running within production environment, otherwise returns false. ]]
cf_isProduction = nil
function IsProduction()
    if (cf_isProduction == nil) then
		cf_isProduction = KEP_IsProduction()
    end
    return (cf_isProduction ~= 0)
end

--[[ DRF - Returns registry selected enumerated developer mode. ]]
cf_devMode = nil
function DevMode()
    if (cf_devMode == nil) then
		cf_devMode = KEP_DevMode()
    end
    return cf_devMode
end

--[[ DRF - Gets the unique user identifier eg. '12345'. ]]
cf_userId = nil
function GetUserId()
    if (cf_userId == nil) then
		cf_userId = KEP_GetKanevaUserId( )
    end
    return cf_userId
end

--[[ DRF - Gets the unique user name eg. 'groovyd' ]]
cf_userName = nil
function GetUserName()
    if (cf_userName == nil) then
		cf_userName = KEP_GetLoginName( )
    end
    return cf_userName
end

function GetTimeMs()
	return KEP_CurrentTime()
end

function GetTimeElapsedMs(timeMs)
    return (GetTimeMs() - timeMs)
end

---------------------------------------------------------------------
-- DRF - New Math Functions
---------------------------------------------------------------------

-- WARNING - Only use these contstants for object placement offsets that are due to 
-- the legacy platform angle bug which MatrixArb::DRF_NEW_ANGLES fixes !
--   If MatrixArb::DRF_NEW_ANGLES is 0 then set g_PI=math.pi
--   If MatrixArb::DRF_NEW_ANGLES is 1 then set g_PI=0
g_PI=math.pi
g_90=90
g_180=180

--[[ DRF - Degrees/Radians Conversion ]]
function Deg2Rad(d) 
	return (d or 0) * (math.pi / 180) 
end
function Rad2Deg(r) 
	return (r or 0) * (180 / math.pi)
end
function DegNorm(d) 
	d = math.fmod(d or 0, 360)
	if (d < 0) then d = (d + 360) end
	if (d >= 360) then d = (d - 360) end
	return math.abs(d)
end
function RadNorm(r) 
	r = math.fmod(r or 0, 2 * math.pi); 
	if (r < 0) then r = (r + 2 * math.pi) end
	if (r >= 2 * math.pi) then r = (r - 2 * math.pi) end
	return math.abs(r);
end

--[[ DRF - Return divisor and remainder ]]
function MathDivRem(a, b)
    local div = math.floor(a / b)
    local rem = a - div * b
    return div, rem
end

--[[ DRF - Return random number integer from 1 to a inclusive ]]
function MathRandomInt(a)
	local randomDouble = KEP_GetRandomNumber( )
    local randomInt = math.floor((randomDouble * (a - 1)) + 0.5) + 1
    return randomInt
end

--[[ DRF - Return a string representation of number rounded to dec decimals ]]
function MathRoundStr(a, dec)
	a = tonumber(a)
	if (a == nil) then return "<nil>" end
	return string.format("%." .. (dec or 0) .. "f", a)
end

--[[ DRF - Return a numeric representation of number rounded to dec decimals ]]
function MathRound(a, dec)
	return tonumber(MathRoundStr(a, dec))
end

--[[ DRF - Return a normalize direction vector and magnitude given a direction vector ]]
function MathVecNorm(vec)
	local magSq = (vec.x * vec.x + vec.y * vec.y + vec.z * vec.z)
	local mag = math.sqrt(magSq)
	if (mag == 0) then return { x=0, y=0, z=0, mag=0 } end
	return { x=vec.x/mag, y=vec.y/mag, z=vec.z/mag, mag=mag }  
end

--[[ DRF - Convert vector to a string for logging ]]
function VecToString(vec, dec)
	dec = dec or LOG_DEC_DEFAULT
	return "("..MathRoundStr(vec.x, dec).." "..MathRoundStr(vec.y, dec).." "..MathRoundStr(vec.z, dec)..")"
end

--[[ DRF - Convert rotation vector to a string for logging (y in degrees) ]]
function VecRotToString(vec, dec)
	dec = dec or LOG_DEC_DEFAULT
	return MathRoundStr(DegNorm(Rad2Deg(vec.y)), dec)
end

function snap1D(t, spacing)
	-- Convert into 'grid cells', add 0.5 for snap nearest behavior,
	-- then convert back into world coordinates
	return spacing * math.floor(t / spacing + 0.5)
end

function snap3D(x, y, z, spacing)
	-- For now 3D snap is just 3 1D snaps
	return snap1D(x, spacing), snap1D(y, spacing), snap1D(z, spacing)
end

---------------------------------------------------------------------
-- DRF - Legacy Miscellaneous Functions
---------------------------------------------------------------------

--[[ DRF - Moved From MenuHelper ]]
function DestroyMenu(dialogHandle)
	if (dialogHandle == nil or tostring(dialogHandle) == "[handle:null]") then return end
	Dialog_Destroy( dialogHandle )
end

--[[ DRF - Moved From MenuHelper ]]
-- simple string split function
-- returns a table of the parsed results, empty if nothing
-- e.g.
--			s = "this\tis\ta\ttest"
--			y = split(s, "\t" )
--			print ("Count: ",#y)
--			for x = 1, #y do
--				print ( "  ", y[x] )
--			end
function split( s, delim )
	local ret = {}
	local i = 1

	-- empty?
	if string.len(s) == 0 then
		return ret
	end

	-- no delimiters?
	local found = string.find( s, "["..delim.."]" )
	if found == nil then
		ret[i] = s
		return ret
	end

	for w in string.gmatch( s, "([^"..delim.."]*)["..delim.."]" ) do
		ret[i] = w
		i = i + 1
	end

	-- get the one after the last delim
	local found, endpos, last = string.find( s, ".*["..delim.."](.*)$" )
	if found ~= nil then
		ret[i] = last
	end

	return ret
end

function GetFilenameFromPath( path )
	if string.find(path, "\\") then
		path = string.gsub(path, "\\", "/")
	end
	local segments = split(path, "/")
	return segments[#segments]
end

--[[ DRF - Moved From MenuHelper ]]
function SendChatMessage( msg, msgType )
	cfLog("SendChatMessage: '"..msg.."'")
	textEvent = KEP_EventCreate( "RenderTextEvent" )
	KEP_EventEncodeString( textEvent, msg )
	KEP_EventEncodeNumber( textEvent, msgType )
	KEP_EventQueue( textEvent )
end

-- Check if an file exist
function FileExist(filename)
	local unaltered, fullPath, menuFileOnly, menuScriptOnly = 0, 0, 0, 0
	local i, j = 0, 0
	local relativePath = ""
	
	--Change Windows-style paths to Unix-style(C)
	if string.find(filename, "\\") then
		filename = string.gsub(filename, "\\", "/")
	end
	
	i, j, relativePath = string.find(filename, "(/GameFiles/Menu.*)")
	if relativePath then
		fullPath = KEP_TestPatchFileForUpdate(relativePath)
	end
	
	unaltered = KEP_TestPatchFileForUpdate(filename)
	menuFileOnly = KEP_TestPatchFileForUpdate("/GameFiles/Menus/"..filename)
	menuScriptOnly = KEP_TestPatchFileForUpdate("/GameFiles/MenuScripts/"..filename)
	return unaltered == 1 or fullPath == 1 or menuFileOnly == 1 or menuScriptOnly == 1
end

-- Parse XML Return Code As Number
function ParseXmlReturnCode(xml)
	xml = stripTagsXML( xml )
	local s, e = 0
	local returnCode = nil
	s, e, returnCode = string.find( xml, "<ReturnCode>(%d+)</ReturnCode>")
	return tonumber(returnCode)
end

--Unescape HTML Entities
--SEE: http://stackoverflow.com/questions/14899734/unescape-numeric-xml-entities-with-lua
---------------------------------------------------------------------
local gsub, char = string.gsub, string.char
local entityMap  = {["lt"]="<",["gt"]=">",["amp"]="&",["quot"]='"',["apos"]="'"}
local entitySwap = function(orig,n,s)
  return entityMap[s] or n=="#" and char(s) or orig
end

function UnescapeHTML(str)
	return gsub( str, '(&(#?)([%d%a]+);)', entitySwap )
end

---------------------------------------------------------------------
-- DRF - GLID Functions
---------------------------------------------------------------------
MIN_GLID = 1 -- minimum valid glid
STUDIO_BASE_GLID = 1 -- Studio created item glids (1 <= n < SGI_BASE_GLID)
SGI_BASE_GLID = 1000000 -- Script Game Item glids (1mil <= n < UGC_BASE_GLID)
UGC_BASE_GLID = 3000000 -- UGC Item glids (3mil <= n )
UGC_DO_BASE_GLID = 1000000000 -- Dynamic object glids (1bil <= n )
MAX_GLID = 2147483647 -- maximum valid glid (31-bits)

-- DRF - Validate Glid
function ValidGlid(glid)
	return (glid and (glid >= MIN_GLID) and (glid <= MAX_GLID))
end

function DecryptGlid(glid)
  local glidtable={}
  glidtable={"h","v","g","_","j","i","r","o","m","3"}
  local s = tostring(glid)
  local buff=""
  for i = 1, string.len(s) do
  num = tonumber(string.sub(s,i,i))+1 
  buff = buff..glidtable[num]
  end
  return buff
end

-- DRF - Use This Instead of LoadIconTextureByID()
function KEP_LoadIconTextureByID(glid, eh, DialogHandle)
	if (DialogHandle == nil) or (eh == nil) then
		cfLogError("KEP_LoadIconTextureByID: nil dialog or element")
		return false
	end
	if not ValidGlid(glid) then
		cfLogError("KEP_LoadIconTextureByID: INVALID GLID - glid="..tostring(glid))
		return false
	end
	local texturePath = KEP_AddItemThumbnailToDatabase( glid )
	if (texturePath == "") then
		cfLogError("KEP_LoadIconTextureByID: KEP_AddItemThumbnailToDatabase() FAILED - glid="..glid)
		return false
	end
	Element_AddExternalTexture(eh, DialogHandle, texturePath, "loading.dds")
	Element_SetCoords(eh, 0, 0, -1, -1)		-- use entire image for texturing
	return true
end

-- Creates a notification message with a hyperlink button
function createLinkMessage( text, buttontext, url )
	if text ~= nil then
		MenuOpen("HyperLinkBox.xml")
		local ev = KEP_EventCreate( "HyperLinkBoxEvent" )
		KEP_EventEncodeString( ev, text )
		KEP_EventEncodeString( ev, buttontext )
		KEP_EventEncodeString( ev, url )
		KEP_EventQueue( ev )
	end
end

---------------------------------------------------------------------
-- These 2 functions find the least power of two.
-- Powers of two required for texture map
---------------------------------------------------------------------
function HelperPowTwo(num, tar)
	if num >= tar then
		return num
	else
		return HelperPowTwo(num * 2, tar)
	end
end

function LeastPowerofTwo(tar)
	return HelperPowTwo(2, tar)
end

-- Map a thumbnail to an image element
-- imgHandle:  	handle id of image control
-- fileName: 	path of desired thumbnail
function displayBrowserThumbnail( imgHandle, fileName )
	if fileName ~= nil then
		fileName = "../../CustomTexture/".. fileName
		Control_SetVisible(Image_GetControl(imgHandle), true)

		eh = Image_GetDisplayElement(imgHandle)
		Element_AddTexture(eh, gDialogHandle, fileName)

		local r = Element_GetTextureWidth(eh, gDialogHandle)
		local b = Element_GetTextureHeight(eh, gDialogHandle)

		r = LeastPowerofTwo(r)
		b = LeastPowerofTwo(b)
		Element_SetCoords(eh, 0, 0, r, b)
	end
end

-- Insert Black Box Thingy Behind Thumbnail Function
-- Parameters: Two images, a picture to display and a black box to display it on top of
-- Determine the Scaling:
-- Objective: Scale the smaller side accourding to the larger side to maintain the same aspect ratio
-- Case 1: Width of foreground greater than the width of background I.E greater ratio 16:9 > 4:3
-- Determine the bounded wall and set lk;;
function scaleImage(foregroundImgBoxHandle, backgroundImgBoxHandle,foregroundImage, borderSize)
	borderSize = borderSize or 0
	
	foreDispElement = Image_GetDisplayElement(foregroundImgBoxHandle)
	
	Element_AddTexture(foreDispElement, gDialogHandle, foregroundImage)

	local textureWidth = Element_GetTextureWidth(foreDispElement, gDialogHandle)
	local textureHeight = Element_GetTextureHeight(foreDispElement, gDialogHandle)
	
	if textureWidth == nil or textureHeight == nil or textureWidth < 1 or textureHeight < 1 then
		return
	end
	
	local backgroundWidth = Control_GetWidth(backgroundImgBoxHandle)
	local backgroundHeight = Control_GetHeight(backgroundImgBoxHandle)
	
	if backgroundHeight < 1 then
		return
	end
	
	local backgroundX = Control_GetLocationX(backgroundImgBoxHandle)
	local backgroundY = Control_GetLocationY(backgroundImgBoxHandle)
	
	local finalWidth = backgroundWidth
	local finalHeight = backgroundHeight
	local finalX = backgroundX
	local finalY = backgroundY
	
	local aspectRatio = textureWidth / textureHeight
	local backgroundAspectRatio = (backgroundWidth/backgroundHeight)
	
	if aspectRatio > backgroundAspectRatio then
		finalHeight = textureHeight * backgroundWidth / textureWidth
		finalY = finalY + ((backgroundHeight - finalHeight) / 2)
	elseif aspectRatio < backgroundAspectRatio then
		finalWidth = textureWidth * backgroundHeight / textureHeight
		finalX = finalX + ((backgroundWidth - finalWidth) / 2)
	end
	
	Control_SetVisible(foregroundImgBoxHandle, true)
	Control_SetLocation(foregroundImgBoxHandle, finalX +borderSize, finalY +borderSize)
	Control_SetSize(foregroundImgBoxHandle, finalWidth - borderSize*2, finalHeight - borderSize*2)
	Element_SetCoords(foreDispElement, 0, 0, LeastPowerofTwo(textureWidth), LeastPowerofTwo(textureHeight))
end

function scaleImageInMenu(foregroundImgBoxHandle, backgroundImgBoxHandle,foregroundImage, dialogHandle, borderSize)
	borderSize = borderSize or 0
	
	foreDispElement = Image_GetDisplayElement(foregroundImgBoxHandle)
	
	Element_AddTexture(foreDispElement, dialogHandle, foregroundImage)

	local textureWidth = Element_GetTextureWidth(foreDispElement, dialogHandle)
	local textureHeight = Element_GetTextureHeight(foreDispElement, dialogHandle)
	
	if textureWidth == nil or textureHeight == nil or textureWidth < 1 or textureHeight < 1 then
		return
	end
	
	local backgroundWidth = Control_GetWidth(backgroundImgBoxHandle)
	local backgroundHeight = Control_GetHeight(backgroundImgBoxHandle)
	
	if backgroundHeight < 1 then
		return
	end
	
	local backgroundX = Control_GetLocationX(backgroundImgBoxHandle)
	local backgroundY = Control_GetLocationY(backgroundImgBoxHandle)
	
	local finalWidth = backgroundWidth
	local finalHeight = backgroundHeight
	local finalX = backgroundX
	local finalY = backgroundY
	
	local aspectRatio = textureWidth / textureHeight
	local backgroundAspectRatio = (backgroundWidth/backgroundHeight)
	
	if aspectRatio > backgroundAspectRatio then
		finalHeight = textureHeight * backgroundWidth / textureWidth
		finalY = finalY + ((backgroundHeight - finalHeight) / 2)
	elseif aspectRatio < backgroundAspectRatio then
		finalWidth = textureWidth * backgroundHeight / textureHeight
		finalX = finalX + ((backgroundWidth - finalWidth) / 2)
	end
	
	Control_SetVisible(foregroundImgBoxHandle, true)
	Control_SetLocation(foregroundImgBoxHandle, finalX +borderSize, finalY +borderSize)
	Control_SetSize(foregroundImgBoxHandle, finalWidth - borderSize*2, finalHeight - borderSize*2)
	Element_SetCoords(foreDispElement, 0, 0, LeastPowerofTwo(textureWidth), LeastPowerofTwo(textureHeight))
end

g_starUrlMap = {
	["3296"] = "kaneva://World of Kaneva",
	["3298"] = "kaneva://Kaneva Preview",
	["5316"] = "kaneva://Dev Star",
	["5310"] = "kaneva://RC Kaneva"
}

--[[ DRF - Return star base URL 'kaneva://World of Kaneva' ]]
function GetStarUrl()
	if (g_starUrl == nil) then
		local starId = KEP_GetStarId() or "3296"
		g_starUrl = g_starUrlMap[starId] or "kaneva://World of Kaneva"
	end
	return g_starUrl
end

--[[ DRF - Return user's home URL '<starUrl>/<loginName>.home' ]]
function GetHomeUrl()
	if (g_userHomeUrl == nil) then
		local loginName = KEP_GetLoginName() or ""
		local starId = KEP_GetStarId() or "3296"
		if (loginName == nil or loginName == "") then
			return nil
		end
		--g_userHomeUrl = ZoneURL.build( starId .. "/"..loginName..".home" )
		g_userHomeUrl = GetStarUrl().."/"..loginName..".home"
	end
	return g_userHomeUrl
end

--[[ DRF - Rezone to user's home ]]
function goHome()
	local homeUrl = GetHomeUrl()
	if (homeUrl == nil) then
		cfLogError("goHome: homeUrl is nil - QUITTING CLIENT")
		InfoBoxMsg("User Not Logged In - Restart Kaneva And Try Again")
		--KEP_Quit()
	else
		cfLog("goHome: '"..homeUrl.."'")
		gotoURL(homeUrl)
	end
end

--[[ DRF - DEPRECATED - Use MenuOpen() - Launches only one instance of the given menu ]]
function LaunchMenu(menuName)
	cfLog("LaunchMenu: DEPRECATED - Use MenuOpen() Instead")
    local menuHandle = MenuHandle(menuName)
    if not MenuHandleOk(menuHandle) then
		menuHandle = MenuOpen(menuName)
	end
    return menuHandle
end

-- DRF - This function will open the specified menu regardless of another instance of the same menu
-- already being open or not.  Be careful using this as you may end up with multiple of the same 
-- menu open.  Prefer using MenuOpen() & MenuClose() functions which first check if the menu is 
-- already open or not.
function gotoMenu(menuXml, bClose, bModal)
	cfLogWarn("gotoMenu() - DEPRECATED - Use MenuOpen() Instead")
	
	local dh = nil
	
	-- Check For Menu AB Variant
	local groupList = KEP_GetABGroupList()
	for groupId in string.gmatch(groupList, "([^,]+),") do
		-- Try to open the "_ab<groupId>" file, this will silently fail if it doesn't exist.
		-- Use Dialog_CreateTrusted to load AB menu from WOK only to prevent hacking (e.g. deploying an fake WOK AB menu in a 3dapp to intercept WOK menu open requests)
		dh = Dialog_CreateTrusted( string.gsub(menuXml, ".xml", "_ab"..groupId..".xml") )
		if dh ~= nil then
			break
		end
	end
	
	-- No Variant Then Open Regular Menu As Usual
	if dh == nil then
		dh = Dialog_Create( menuXml )
	end
	
	-- Menu Failure?
	if dh == nil or dh == 0 then
		cfLogError("gotoMenu: Dialog_Create("..menuXml..") FAILED")
        Dialog_MsgBox("Failed Opening " .. menuXml)
        return nil
	end

	-- Set Modal?
    if bModal == 1 then
		Dialog_SetModal(dh, 1)
		Dialog_BringToFront( dh )
	end

	-- Close Calling Menu?
	if bClose == 1 then
		MenuCloseThis()
	end
    
	return dh
end

-- makes a deep copy of a given table (the 2nd param is optional and for internal use)
-- circular dependencies are correctly copied.
function tcopy(t, lookup_table)

	local copy = {}
	for i,v in pairs(t) do
		if type(v) ~= "table" then
			copy[i] = v
  		else
   			lookup_table = lookup_table or {}
   			lookup_table[t] = copy
  			if lookup_table[v] then
    				copy[i] = lookup_table[v] -- we already copied this table. reuse the copy.
   			else
    				copy[i] = tcopy(v,lookup_table) -- not yet copied. copy it.
   			end
  		end
 	end
	return copy
end

-- Switch a button control between enabled and disabled
-- Enable or disable the selected button
function setButtonState( buttonHandle, state )
	if buttonHandle ~= nil then
		ch = Button_GetControl(buttonHandle)
		if ch ~= nil then
			Control_SetEnabled(ch, state)
		end
	end
end

function setState(controlHandle, state)
	Control_SetVisible(controlHandle, state)
	Control_SetEnabled(controlHandle, state)
end

-- Remove unsupported HTML tags from strings
-- A table to convert from HTML &blah; codes to ISO Latin-1.
-- From http://www.utexas.edu/learn/html/spchar.html
g_HTMLSpecialChars = {}
g_HTMLSpecialChars["ndash"]  = 8211  -- en dash
g_HTMLSpecialChars["mdash"]  = 8212  -- em dash
g_HTMLSpecialChars["iexcl"]  = 161   -- inverted exclamation
g_HTMLSpecialChars["iquest"] = 191   -- inverted question mark
g_HTMLSpecialChars["quot"]   = 34    -- quotation mark
g_HTMLSpecialChars["ldquo"]  = 8220  -- left double curly quote
g_HTMLSpecialChars["rdquo"]  = 8221  -- right double curly quote
g_HTMLSpecialChars["lsquo"]  = 8216  -- left single curly quote
g_HTMLSpecialChars["rsquo"]  = 8217  -- right single curly quote
g_HTMLSpecialChars["nbsp"]   = 160   -- non-breaking space 
g_HTMLSpecialChars["amp"]    = 38    -- ampersand
g_HTMLSpecialChars["gt"]     = 62    -- greater than
g_HTMLSpecialChars["lt"]     = 60    -- less than

function htmlSpecialCharacterToISOLatin1( str )
	return string.char(tonumber(g_HTMLSpecialChars[str])) or ""
end

function stripTags( text, keepHTML )

	if text ~= nil then

		local specialSubs = 0
		local digitSubs = 0
		repeat 
			-- Convert from HTML special character codes
			text, specialSubs = string.gsub(text, "&(%a+);", htmlSpecialCharacterToISOLatin1 )
		
			-- Convert from ISO-Latin 1 codes
			text, digitSubs = string.gsub(text, "&#(%d+);", function(d) return string.char(tonumber(d)) end )
		until specialSubs == 0 and digitSubs == 0
		
		-- Convert some HTML tags, or strip them out if there's no easy conversion.
		text = string.gsub(text, "</div>", "\n") -- line break after closing div
		if keepHTML ~= true then
			text = string.gsub(text, "%b<>", "") -- remove anything that looks like html
		end
	end
    return text
end

function stripTagsXML( text )
	if text ~= nil then
		text = string.gsub(text, "&lt;", "<") 	-- left bracket
		text = string.gsub(text, "&gt;", ">") 	-- right bracket
		text = string.gsub(text, "&amp;", "&") 	-- ampersand
		text = string.gsub(text, "&nbsp;", "") 	-- non-breaking space
		text = string.gsub(text, "&#8217;", "'")  	-- apostrophe
		text = string.gsub(text, "&amp;", "&")	-- literal ampersand (do me last)
	end
    return text
end

-- staticName:  name of a static label used to split up the message using
--				the controls word wrap option,
-- msg: string we are breaking up
function letsGetFunky( msg, staticName )
	local sh = Dialog_GetStatic( gDialogHandle, staticName )
	return getTableFromStaticText( msg, sh )
end

function getTableFromStaticText( msg, staticHandle )
	local t = {}
	local wid = Control_GetWidth( Static_GetControl( staticHandle ) )
	local line = ""
	local heightOneLine = Static_GetStringHeight( staticHandle, "Tp" )
	while msg ~= "" do
		-- make var adjustments
		local char = string.sub(msg, 1, 1)
		line = line .. char
		msg = string.sub(msg, 2, -1)
		if char == "\n" or Static_GetStringWidth( staticHandle, line ) > wid or Static_GetStringHeight( staticHandle, line ) > heightOneLine then
			-- This is a line break.
			if char ~= "\n"  and string.find(line, " ") ~= nil then
				-- There was a space in there that needs to be broken.
				local breakspcpos = string.find(line, " ")
				while string.find(line, " ", breakspcpos + 1) ~= nil do
					breakspcpos = string.find(line, " ", breakspcpos + 1)
				end
				msg = string.sub(line, breakspcpos + 1, -1) .. msg
				line = string.sub(line, 1, breakspcpos)
			else
				if char == "\n" then
					-- strip off the newline
					line = string.sub(line, 1, -2)
				end
			end
            table.insert(t, line)
			line = ""
		end
	end
	table.insert(t, line)
	return t
end

function setupTextScroller( message, staticHandle, listBoxHandle )
	-- Sets up a list box to mimic a scrolling textbox by breaking up text lines.
	-- Expects a static text box to have text already assigned to it.
	local subMsgList = getTableFromStaticText( message, staticHandle )
	if ListBox_GetSize( listBoxHandle ) > 0 then
		ListBox_RemoveAllItems( listBoxHandle )
	end
	for index,_ in pairs(subMsgList) do
		ListBox_AddItem( listBoxHandle, subMsgList[ index ], 1 )
	end
end

-- Convert the string from a GetBrowserPage call to a lua boolean value
function strToBool( strValue )
	if strValue == "1" then
		return true
	end
	    return false
end

-- Convert the string from a GetBrowserPage call to a lua boolean value
function numToBool( numValue )
	if numValue == 1 then
		return true
	end
	    return false
end

function __sortTableByField( tbl, field, reverse )
	local tblSort = tbl
	if (reverse == true) then
		table.sort(tblSort, function (a,b) return (tostring(a[field]) > tostring(b[field])) end)
	else
		table.sort(tblSort, function (a,b) return (tostring(a[field]) < tostring(b[field])) end)
	end
	return tblSort
end

function sortTableByField(tbl, field)
	return __sortTableByField(tbl, field, false)
end

function sortTableByFieldReverse(tbl, field)
	return __sortTableByField(tbl, field, true)
end

function decodeInventoryEvent( invEvent, decodeSellPrice )
	local mainTable = {}
	if invEvent ~= nil then
		local num_items = KEP_EventDecodeNumber( invEvent )
		for i = 0, num_items-1 do
			local t_item 			= {}
			local GLID 			= KEP_EventDecodeNumber( invEvent )
			local quantity 		= KEP_EventDecodeNumber( invEvent )
			local name 			= KEP_EventDecodeString( invEvent )
			local armed 		= false
			local gift			= false

			if quantity < 0 then
				quantity = -quantity -- if < 0 armed
				armed = true
			end

			if GLID < 0 then
				GLID = -GLID -- if < 0 gift
				gift = true
			end

			local use_type		= KEP_EventDecodeNumber( invEvent )
			local texture_name 	= KEP_EventDecodeString( invEvent )

			-- texture coords
			local left			= KEP_EventDecodeNumber( invEvent )
			local top			= KEP_EventDecodeNumber( invEvent )
			local right			= KEP_EventDecodeNumber( invEvent )
			local bottom		= KEP_EventDecodeNumber( invEvent )

			-- now always get it if decodeSellPrice then
			t_item["sell_price"] = KEP_EventDecodeNumber(invEvent)

			local passCount = KEP_EventDecodeNumber(invEvent)
			local itemPassList = {}
			for pC=1,passCount do
				table.insert(itemPassList, KEP_EventDecodeNumber(invEvent))
			end

			local isDefault = false
			local defaultItem = KEP_EventDecodeNumber(invEvent)
			if defaultItem ~= 0 then
				isDefault = true
			end
			local itemType = KEP_EventDecodeNumber( invEvent )
			itemType = tonumber(itemType)

			--item description
			local desc = KEP_EventDecodeString( invEvent )
			t_item["GLID"]				= math.abs(GLID)
			t_item["name"]				= name
			t_item["quantity"]			= quantity
			t_item["use_type"]			= use_type
			t_item["texture_name"]		= texture_name
			t_item["texture_coords"]	= {left, top, right, bottom}
			t_item["is_armed"]			= armed
			t_item["is_gift"]			= gift
			t_item["pass_list"] 		= itemPassList
			t_item["is_default"] 		= isDefault
			t_item["type"]				= itemType
			t_item["description"]		= desc
			t_item["can_trade"] = true
			if t_item["is_armed"] or t_item["is_gift"] or t_item["is_default"] then
				t_item["can_trade"] = false
			end
			table.insert(mainTable, t_item)
		end
	end
	return mainTable 
end

function getPlayerInventoryCanTrade(menu_filter, ...)
	local can_trade = true
	return __getPlayerInventory(menu_filter, can_trade, ...)
end

function getPlayerInventory(menu_filter, ...)
	local can_trade = false
	return __getPlayerInventory(menu_filter, can_trade, ...)
end

cf_inventoryPassthrough = 0
function __getPlayerInventory(menu_filter, can_trade, page, range, invType, search, category, use_type, gameItemTypes, includeStorage)
	cf_inventoryPassthrough = cf_inventoryPassthrough + 1

	-- "new" is the sort order
	local url = GameGlobals.WEB_SITE_PREFIX..PLAYER_INVENTORY_SUFFIX.."new"

	if invType then
		url = url .. "&type=" .. invType
	end

	if search then
		url = url .. "&search=" .. search
	end

	if category then
		if category == "Gifts" then
			url = url .. "&gifts=true"
		elseif category == "Access Pass" then
			url = url .. "&mature=true"
		else
			url = url .. "&category=" .. category
		end
	end

	if use_type then
		url = url .. "&use_type=" .. use_type
	end

	-- DRF - Added
	if (can_trade == true) then
		url = url .. "&can_trade=true"
	end

	-- Game item types separated by |
	if gameItemTypes then
		url = url .. "&gameItemTypes=" .. gameItemTypes
	end	

	url = url .. "&passthrough=" .. cf_inventoryPassthrough

	-- Include Storage?
	if includeStorage then
		url = url .. "&wokInventoryType=All"
	end
	
	cfLog("getPlayerInventory: '"..url.."'")

	-- the call to get the player's inventory has been constructed to perform a paged
	-- result whereby the menus can present this information in a more orderly fashion.
	-- Currently, we are getting all the items and placing them in a single list box. Thus,
	-- we'll arbitrarily ask for a million as the max.
	makeWebCall(url, menu_filter, page or 1, range or 1000000)
end

cf_bankPassthrough = 0
function getBankInventory(menu_filter, page, range, category, search)
	cf_bankPassthrough = cf_bankPassthrough + 1
	-- "new" is the sort order
		-- the call to get the bank's inventory has been constructed to perform a paged
		-- result whereby the menus can present this information in a more orderly fashion.
		-- Currently, we are getting all the items and placing them in a single list box. Thus,
		-- we'll arbitrarily ask for a million as the max.

	local url =  GameGlobals.WEB_SITE_PREFIX..BANK_INVENTORY_SUFFIX.."new"

	if category then
		if category == "Gifts" then
			url = url .. "&gifts=true"
		elseif category == "Access Pass" then
			url = url .. "&mature=true"
		else
			url = url .. "&category=" .. category
		end
	end

	if search then
		url = url .. "&search=" .. search
	end

	url = url .. "&passthrough=" .. cf_bankPassthrough

	makeWebCall(url, menu_filter, page or 1, range or 1000000)
end

--Get quantities for a number of glids either as a table or listed in individually
function getItemQuantities(menu_filter, glids)
	local web_address = GameGlobals.WEB_SITE_PREFIX..PLAYER_INVENTORY_SUFFIX.."new&quantities=true&glids="
	
	local totalGLIDs = 0
	for i,v in pairs(glids) do
		web_address = web_address .. tostring(v) .. "|"
		totalGLIDs = totalGLIDs + 1
	end

	web_address = string.sub(web_address, 1, -2)
	makeWebCall(web_address, menu_filter, 1, totalGLIDs)
end

function getItemAnimations(menu_filter, item_glid)
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..ITEM_ANIMATIONS_SUFFIX.. item_glid, menu_filter, 1, 1000000)
end

-- Parses the attributes from an xml element into a table
function xmlParseAttributes(s)
	local attributes = {}
	string.gsub(s, "([%-%w]+)=([\"'])(.-)%2", function (w, _, a)
		attributes[w] = a
	end)
	return attributes
end

-- Parses an xml document into a nested table    
function xmlStrToTable(s)
	local stack = {}
	local top = {}

	table.insert(stack, top)

	local ni,c,label,attributes, empty
	local i, j = 1, 1
	while true do
		ni,j,c,label,attributes, empty = string.find(s, "<(%/?)([%w:]+)(.-)(%/?)>", i)
		if not ni then 
			break 
		end

		local text = string.sub(s, i, ni-1)
		if not string.find(text, "^%s*$") then
			table.insert(top, text)
		end

		if empty == "/" then  -- empty element tag
			table.insert(top, {label=label, attributes=xmlParseAttributes(attributes), empty=1})
		elseif c == "" then   -- start tag
			top = {label=label, attributes=xmlParseAttributes(attributes)}
			table.insert(stack, top)   -- new level
		else  -- end tag
			local toclose = table.remove(stack)  -- remove top
			top = stack[#stack]
			if #stack < 1 then
				cfLogError("nothing to close with "..label)
				return
			end
			if toclose.label ~= label then
				cfLogError("trying to close "..toclose.label.." with "..label)
				return
			end
			table.insert(top, toclose)
		end

		i = j+1
	end

	local text = string.sub(s, i)
	if not string.find(text, "^%s*$") then
		table.insert(stack[#stack], text)
	end

	if #stack > 1 then
		cfLogError("unclosed "..stack[#stack].label)
		return
	end

	return stack[1]
end

-- Converts an xml table node into a formatted string for logging or display
function xmlTableNodeToStr(xmlNode, tabStr, tabCount, prependAttribName)
	if type(xmlNode) ~= "table" or not xmlNode.label then
		cfLogError("Invalid xmlNode.")
		return
	end

	tabCount = (tabCount or 0)
	tabStr = (tabStr or "   ")

	local nodeString = xmlNode.label
	for t = 1, tabCount do
		nodeString = tabStr .. nodeString
	end

	if xmlNode.attributes then
		nodeString = nodeString .. " [ "
		for attribName, attribVal in pairs(xmlNode.attributes) do
			if prependAttribName then
				nodeString = nodeString .. attribName .. "=" .. attribVal .. " "
			else
				nodeString = nodeString .. attribVal .. " "
			end
		end
		nodeString = nodeString .. "]"
	end

	return nodeString
end

-- Converts an xml string into a formatted list for logging or display
function xmlStrToList(xmlTree, tabStr, xmlList, tabCount, prependAttribName)
	if type(xmlTree) == "string" then
		xmlTree = xmlStrToTable(xmlTree)
	elseif type(xmlTree) ~= "table" or not xmlTree.label then
		cfLogError("Invalid xmlTree. Must be either an xml string or result of call to xmlStrToTable().")
		return
	end

	xmlList = (xmlList or {})
	tabCount = (tabCount or 0)

	if xmlTree.empty then
		-- Process single nodes
		table.insert(xmlList, xmlTableNodeToStr(xmlTree, tabStr, tabCount, prependAttribName))
	else
		-- Process subtrees
		for i, subTree in ipairs(xmlTree) do
			if type(subTree) == "table" then
				if subTree.empty then
					table.insert(xmlList, xmlTableNodeToStr(subTree, tabStr, tabCount, prependAttribName))
				else
					table.insert(xmlList, xmlTableNodeToStr(subTree, tabStr, tabCount, prependAttribName))
					
					-- Recursively Process Sub-Tree
					xmlStrToList(subTree, tabStr, xmlList, tabCount + 1, prependAttribName) 
				end
			end
		end
	end

	return xmlList
end

-- Adds an xml node to an associative table representing the xml document
function addXmlNodeToAssocTable(label, xmlNode, assocTable)
	if type(label) ~= "string" then cfLogError("Invalid label. Must be a string.") return end
	if type(xmlNode) ~= "string" and type(xmlNode) ~= "table" then cfLogError("Invalid xmlNode. Must be either a table or a string.") return end
	if type(assocTable) ~= "table" then cfLogError("Invalid assocTable. Must be a table.") return end

	if assocTable[label] == nil then
		assocTable[label] = xmlNode
	elseif type(assocTable[label]) ~= "table" or #assocTable[label] == 1 then
		local temp = assocTable[label]
		assocTable[label] = {}
		table.insert(assocTable[label], temp)
		table.insert(assocTable[label], xmlNode)
	else
		table.insert(assocTable[label], xmlNode)
	end
end

-- Converts an xml string to an associative table representing the document
-- Preserves proper node nesting. If a key has multiple values, they are listed
-- in an indexed table.
function nestedXmlStrToAssocTable(xmlTree)
	if type(xmlTree) == "string" then
		xmlTree = xmlStrToTable(xmlTree)
	elseif type(xmlTree) ~= "table" or not xmlTree.label then
		cfLogError("Invalid xmlTree. Must be either an xml string or result of call to xmlStrToTable().")
		return
	end

	local assocTable = {}
	local labelUsage = {}

	if xmlTree.empty then
		-- Process single nodes
		addXmlNodeToAssocTable(xmlTree.label, xmlTree, assocTable)
	else
		-- Process subtrees
		for i, subTree in ipairs(xmlTree) do
			if type(subTree) == "table" then
				if subTree.empty then
					addXmlNodeToAssocTable(subTree.label, subTree, assocTable)
				else
					-- Recursively Process Sub-Tree
					addXmlNodeToAssocTable(subTree.label, nestedXmlStrToAssocTable(subTree), assocTable)
				end
			elseif type(subTree) == "string" then
				-- Should only get here as stopping point of recursion
				return subTree
			else
				cfLogError("Invalid subTree.")
				return
			end
		end
	end

	return assocTable
end

function xmlParseAssociativeTable(xmlString)
	--This is just a single item, return it
	if not string.find(xmlString, "<(.-)>.-</%1>") then
		-- Convert XML tag to number if applicible
		if tonumber(xmlString) then
			xmlString = tonumber(xmlString)
		-- Convert XML tag to boolean?
		elseif xmlString == "true" then
			xmlString = true
		elseif xmlString == "false" then
			xmlString = false
		end
		return xmlString
	end

	-- There are multiple items, add each to table and recursively check for other subtables
	local subTable = {}
	for tag, data in string.gmatch(xmlString, "<(.-)>(.-)</%1>") do
		subTable[tag] = xmlParseAssociativeTable(data)
	end

	return subTable
end

function xmlGetInteger( xmlStanza, tag )
	return tonumber(string.match(xmlStanza, "<" .. tag .. ">(%d-)</" .. tag .. ">"))
end

-- TODO: Does not currently consider nested tags. A tag within a tag of the same name will fail.
function xmlGetString( xmlStanza, tag )
	return string.match(xmlStanza, "<" .. tag .. ">(.-)</" .. tag .. ">")
end

-- Read string tokens and convert to true/false
function xmlGetBoolean( xmlStanza, tag )
	local result = string.lower(xmlGetString(xmlStanza, tag) or "")
	return (result=="t" or result=="true")
end

function xmlRemoveNode ( xmlStanza, tag )
	return string.gsub(xmlStanza, "<" .. tag .. ">(.-)</" .. tag .. ">", "")
end

function decodeInventoryItemsFromWeb( item_table, xmlData, excludeArmedItems )
	
	-- Parse XML Return Code
	local returnCode = ParseXmlReturnCode(xmlData)
	if (returnCode == nil or returnCode ~= 0) then 
		cfLogError("decodeInventoryItemsFromWeb(): Unable to obtain inventory from web")
		-- InfoBoxMsg("Unable to obtain inventory from web", returnCode)
		return
	end

	-- gfind renamed gmatch in future versions of LUA
	local s, e = 0
	local result = nil
	for block_text in string.gmatch( xmlData, "<Inventory>(.-)</Inventory>") do

		local t_item = {}

		t_item.GLID               = xmlGetInteger(block_text, "global_id")
		t_item.name               = xmlGetString (block_text, "name")
		t_item.quantity           = xmlGetInteger(block_text, "quantity")
		t_item.use_type           = xmlGetInteger(block_text, "use_type")
		t_item.it_use_type        = xmlGetInteger(block_text, "it_use_type")
		t_item.is_armed           = xmlGetBoolean(block_text, "armed")
		t_item.inventory_sub_type = xmlGetInteger(block_text, "inventory_sub_type")
		if t_item.inventory_sub_type==nil then
			cfLogError("decodeInventoryItemsFromWeb missing inventory_sub_type setting default to IT_NORMAL")
			t_item["inventory_sub_type"] = GameGlobals.IT_NORMAL
		end
		t_item.sell_price         = xmlGetInteger(block_text, "selling_price")

		-- some number of item passes may be given for this item
		t_item.pass_list = {}
		for pass_block_text in string.gmatch(block_text, "<Pass>(.-)</Pass>") do
			local passID = xmlGetInteger(pass_block_text, "ID")
			if passID ~= nil then
				table.insert(t_item.pass_list, passID)
			end
		end
		t_item.is_default         = xmlGetInteger(block_text, "is_default")
		t_item.type               = xmlGetInteger(block_text, "inventory_type")		-- inventory_sub_type
		t_item.description        = xmlGetString (block_text, "description")
		t_item.creator            = xmlGetString (block_text, "creator_name")
		t_item.actor_group        = xmlGetInteger(block_text, "actor_group")
		t_item.can_trade          = not t_item.is_armed and not t_item.is_gift and t_item.is_default~=1
		t_item.use_value          = xmlGetInteger(block_text, "use_value")
		t_item.thumbnail_medium   = xmlGetString (block_text, "thumbnail_medium_path")
		t_item.thumbnail_large    = xmlGetString (block_text, "thumbnail_large_path")
		t_item.thumbnail_asset_details = xmlGetString (block_text, "thumbnail_assetdetails_path")
		t_item.base_global_id     = xmlGetInteger(block_text, "base_global_id")
		t_item.script_url         = xmlGetString (block_text, "script_path")
		t_item.parent_script_url  = xmlGetString (block_text, "parent_script_path")
		t_item.default_glid       = xmlGetInteger(block_text, "default_global_id")
		t_item.bundle_global_id   = xmlGetInteger(block_text, "bundle_global_id")

		if excludeArmedItems == true then
			-- Don't put the item in the player's inventory menu for the bank inventory main menu
			-- if it is armed.
			if not t_item.is_armed then
				table.insert(item_table, t_item)
			end
		else
			table.insert(item_table, t_item)
		end
	end

	-- Proxy_TraceDump("_SCR_ decodeInventoryItemsFromWeb added ["..tostring(#item_table).."] items")
end

function decodeGameItemsFromWeb(item_table, xmlData)
	-- Parse XML Return Code
	local returnCode = ParseXmlReturnCode(xmlData)
	if (returnCode == nil or returnCode ~= 0) then 
		cfLogError("decodeInventoryItemsFromWeb(): Unable to obtain game items from web")
		-- InfoBoxMsg("Unable to obtain game items from web", returnCode)
		return
	end

	for gameItem in string.gmatch(xmlData, "<ScriptGameItem>(.-)</ScriptGameItem>") do
		local t_item = {}

		-- Make sure these are always here
		t_item.GLID = xmlGetInteger(gameItem, "glid") or 0
		t_item.GIGLID = xmlGetInteger(gameItem, "game_item_glid") or 0
		t_item.name = xmlGetString(gameItem, "name") or ""
		t_item.description = xmlGetString(gameItem, "description") or ""
		t_item.itemType = xmlGetString(gameItem, "item_type") or ""
		t_item.use_type = USE_TYPE.GAME_ITEM -- always this use type since that's how it's requested, include in case inventory needs to know
		t_item.it_use_type = USE_TYPE.GAME_ITEM
		t_item.tooltipBehavior = t_item.behavior or t_item.itemType
		t_item.existsOnShop = xmlGetBoolean(gameItem, "exists_on_shop")

		-- Before building "metadata" property for game item, remove any inventory-only nodes
		gameItem = xmlRemoveNode(gameItem, "exists_on_shop")

		t_item.metadata = xmlParseAssociativeTable(gameItem)

		table.insert(item_table, t_item)
	end
end

function decodeItemAnimationsFromWeb( item_table, xmlData)
	
	-- Parse XML Return Code
	local returnCode = ParseXmlReturnCode(xmlData)
	if (returnCode == nil or returnCode ~= 0) then 
		cfLogError("decodeItemAnimationsFromWeb(): Unable to obtain item animations from web")
		-- InfoBoxMsg("Unable to obtain item animations from web", returnCode)
		return
	end

	-- gfind renamed gmatch in future versions of LUA
	local s, e = 0
	local result = nil
	for block_text in string.gmatch( xmlData, "<items>(.-)</items>") do
		local t_item = {}

		t_item.GLID               = xmlGetInteger(block_text, "global_id")
		t_item.name               = xmlGetString (block_text, "name")
		t_item.description        = xmlGetString (block_text, "description")
		t_item.use_type           = xmlGetInteger(block_text, "use_type")
		t_item.market_cost        = xmlGetInteger(block_text, "market_cost")
		t_item.sell_price         = xmlGetInteger(block_text, "selling_price")
		t_item.type               = xmlGetInteger(block_text, "inventory_type")		-- inventory_sub_type
		t_item.actor_group        = xmlGetInteger(block_text, "actor_group")
		t_item.item_active        = xmlGetInteger(block_text, "item_active")

		table.insert(item_table, t_item)
	end
end

function decodeTradeItemsFromWeb( item_table, xmlData )
	
	-- Parse XML Return Code
	local returnCode = ParseXmlReturnCode(xmlData)
	if (returnCode == nil or returnCode ~= 0) then
		cfLogError("decodeTradeItemsFromWeb: Unable to obtain trade items from web")
		-- InfoBoxMsg("Unable to obtain trade items from web", returnCode)
		return
	end

	-- gfind renamed gmatch in future versions of LUA
	local s, e = 0
	local result = nil
	for block_text in string.gmatch( xmlData, "<TradeItems>(.-)</TradeItems>") do

		local t_item = {}

		t_item.GLID               = xmlGetInteger(block_text, "global_id")
		t_item.name               = xmlGetString (block_text, "name")

		table.insert(item_table, t_item)
	end
end

function playerSpawnSubZone( data )
	local gameId = KEP_GetParentGameId()
	if gameId == 0 then --we are WoK so just use gamename
		gameId = KEP_GetGameName()
	end
	gotoURL(ZoneURL.build( gameId .. "/"..data ))
	return true
end

function getIconPreference()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		local okfilter
		local iconPref = KEP_ConfigGetString( config, "ShowIcons", tostring(g_useIcons) )
		if iconPref == "false" then
			return false
		end
	end
	return true
end

-- display a yes/no confirmation box
-- return true if dialog created and initialization message sent, false otherwise
function KEP_YesNoBox( message, title, filter, objectid, deprecated1, width, height, flushLeft, deprecated2 )

	-- Defaults
	title = title or "Confirm"
	width = width or -1
	height = height or -1

	MenuOpenModal("YesNoBox.xml")

	-- Send YesNoBox Event
	local e = KEP_EventCreate( YES_NO_BOX_EVENT )
	if (filter ~= nil) then KEP_EventSetFilter(e, filter) end
	if (objectid ~= nil) then KEP_EventSetObjectId(e, objectid) end
	KEP_EventEncodeString(e, message)
	KEP_EventEncodeString(e, title)
	KEP_EventEncodeNumber(e, width)
	KEP_EventEncodeNumber(e, height)
	if (flushLeft ~= nil) and flushLeft then
		KEP_EventEncodeNumber(e, 1)
	else
		KEP_EventEncodeNumber(e, 0)
	end
	KEP_EventQueue(e)

	return true
end

-- DRF - Use this instead of showMessageBox()
-- display a message box
-- return true if dialog created and initialization message sent, false otherwise
function KEP_MessageBox( message, title, filter, objectid, width, height, flushLeft, notModal )
	flushLeft = flushLeft or false -- default not flush left
	notModal = notModal or true -- default not modal
	cfLog("KEP_MessageBox: '"..message.."' notModal="..tostring(notModal))

	-- Open InfoBox Menu
	local bModal = not ToBool(notModal)
	MenuOpen("InfoBox.xml", bModal)

	-- create minimized, will restore to regular window in InfoBox.lua creation event handler
	MenuSetMinimized("InfoBox.xml", true)

	-- Send Message Box InfoBoxEvent { message, title, width, height, flushLeft }
	local infoBoxEvent = KEP_EventCreate( "InfoBoxEvent" )
	if filter~=nil then
		KEP_EventSetFilter( infoBoxEvent, filter  )
	end
	if objectid~=nil then
		KEP_EventSetObjectId( infoBoxEvent, objectid )
	end
	KEP_EventEncodeString( infoBoxEvent, message or "")
	KEP_EventEncodeString( infoBoxEvent, title or "Message")
	KEP_EventEncodeNumber( infoBoxEvent, width or -1 )
	KEP_EventEncodeNumber( infoBoxEvent, height or -1 )
	KEP_EventEncodeNumber( infoBoxEvent, ToBOOL(flushLeft))
	KEP_EventQueue( infoBoxEvent )

	return true, MenuHandle("InfoBox.xml")
end

WORLD_OBJ_MENU_EVENT = "WorldObjectMenuEvent"
DYNAMIC_OBJ_MENU_EVENT = "DynamicObjectMenuEvent"
FLASHGAME_MENU_OPEN_EVENT = "OpenFlashGameMenuEvent"
DYNAMIC_OBJECT_INFO_EVENT = "DynamicObjectInfoEvent"

function openDynamicObjectMenu(objId, owner)
	
	local name, description, canPlayMovie, isAttachableObject, isInteractive, isLocal, isDerivable, assetId, friendId, customTexture, globalId, invType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(objId)
	local objType = ObjectType.DYNAMIC

	-- Don't Display Object Menu For Core Objects
	if IsCoreGlid( globalId ) then return end

	if ( isLocal == 1 ) then
		cfLog("openDynamicObjectMenu: "..ObjectToString(objId).." - Opening DynamicObject Menu...")
		MenuOpen("DynamicObject.xml")
	else
		cfLog("openDynamicObjectMenu: "..ObjectToString(objId).." - Opening PlaceableObj Menu...")
		MenuOpen("PlaceableObj.xml")
	end

	local e = KEP_EventCreate( DYNAMIC_OBJECT_INFO_EVENT )
	KEP_EventEncodeNumber(e, objId)
	KEP_EventEncodeNumber(e, objType)
	KEP_EventQueue( e )
end

function openFlashGameMenu(placementId, owner)
	MenuOpen("FlashGameOverview.xml")

	local name, description, canPlayMovie, isAttachableObject, isInteractive, isLocal, isDerivable, assetId, friendId, customTexture, globalId, invType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(placementId)
	local x = 0
	local y = 0
	local rightClick = 1
	local leftClick = 0
	local e = KEP_EventCreate( FLASHGAME_MENU_OPEN_EVENT )
	KEP_EventEncodeNumber( e, placementId )
	if owner == true then
		KEP_EventEncodeNumber( e, 1 )
	else
		KEP_EventEncodeNumber( e, 0 )
	end
	KEP_EventEncodeNumber( e, friendId )
	KEP_EventEncodeNumber( e, x )
	KEP_EventEncodeNumber( e, y )
	KEP_EventEncodeNumber( e, isAttachableObject )
	KEP_EventEncodeString( e, name )
	KEP_EventEncodeNumber( e, canPlayMovie )
	KEP_EventEncodeNumber( e, isInteractive )
	KEP_EventEncodeString( e, description )
	KEP_EventEncodeNumber( e, rightClick )
	KEP_EventEncodeNumber( e, leftClick )
	KEP_EventQueue( e )
end

function openWorldObjectMenu(objID)
	local ev = KEP_EventCreate( ENTER_BUILD_MODE_EVENT)
	KEP_EventSetFilter(ev, 1)
	KEP_EventEncodeNumber(ev, 3) --Textures Mode
	KEP_EventQueue( ev)
end

-- Temporarily mutes client sound. Will be restored automatically upon next rezone.
function setGlobalSoundTempMute(applyMute)
	local ev = KEP_EventCreate("TemporarySoundMuteEvent")
	KEP_EventEncodeNumber(ev, (applyMute and 1 or 0))
	KEP_EventQueue(ev)
end

function makeWebCall(url, filter, start_index, max_items)
--	cfLog("makeWebCall: '"..url.."'")
	return Dialog_MakeWebCall(url, filter, start_index or 0, max_items or 0)
end

function makeWebCallCached(url, filter, start_index, max_items, seconds_to_cache, staleDoRefresh)
	
	-- Set Defaults if Not Specified
	if seconds_to_cache == nil then
		seconds_to_cache = 60
	end
	if staleDoRefresh == nil then
		staleDoRefresh = MenuNameThis()
	end

	local ev = KEP_EventCreate( "MakeWebCallCachedEvent")
	KEP_EventEncodeString(ev, url)
	KEP_EventEncodeNumber(ev, filter)
	KEP_EventEncodeNumber(ev, start_index)
	KEP_EventEncodeNumber(ev, max_items)
	KEP_EventEncodeNumber(ev, seconds_to_cache)
	KEP_EventEncodeString(ev, staleDoRefresh)
	KEP_EventQueue( ev)
end

function wcGetAllNewFameEvents()
-- DEPRECATED FAME
end

function wcReportFameEventNotified(notificationID)
-- DEPRECATED FAME
end

function sendNotificationEventNull(notificationID)
-- DEPRECATED FAME
end

-- DRF - Schedules player fame event notification to occur right now.
function sendNotificationEvent(text, rollOverText, rollOverSubtext, textureType, imageURL, imageName, linkURL, buttonText, duration, type, data1, data2, data3, data4, data5)
	sendNotificationEventInFuture(0, text, rollOverText, rollOverSubtext, textureType, imageURL, imageName, linkURL, buttonText, duration, type, data1, data2, data3, data4, data5)
end

-- DRF - Schedules player fame event notification to occur some time in the future.
function sendNotificationEventInFuture(delay, text, rollOverText, rollOverSubtext, textureType, imageURL, imageName, linkURL, buttonText, duration, type, data1, data2, data3, data4, data5)
	CREATE_NOTIFICATION_EVENT = "CreateNotificationEvent"
	local ev = KEP_EventCreate( CREATE_NOTIFICATION_EVENT)
	KEP_EventEncodeString(ev, text)
	KEP_EventEncodeString(ev, rollOverText)
	KEP_EventEncodeString(ev, rollOverSubtext)
	KEP_EventEncodeNumber(ev, textureType)
	KEP_EventEncodeString(ev, imageURL)
	KEP_EventEncodeString(ev, imageName)
	KEP_EventEncodeString(ev, linkURL)
	KEP_EventEncodeString(ev, buttonText)
	KEP_EventEncodeNumber(ev, duration)
	KEP_EventEncodeNumber(ev, type)
	KEP_EventEncodeString(ev, data1)
	KEP_EventEncodeString(ev, data2)
	KEP_EventEncodeString(ev, data3)
	KEP_EventEncodeString(ev, data4)
	KEP_EventEncodeString(ev, data5)
	if delay ~= 0 then
		KEP_EventQueueInFuture( ev, delay)
	else
		KEP_EventQueue( ev)
	end
end

-- [[ convenience method to show a timed information box ]]
function showTimedInfoBox(duration, message, texturepath, coordinates)
	-- Open up the timed info box
	MenuOpen("TimedInfoBox.xml")

	-- Configure timed info box
	local ev = KEP_EventCreate( "TimedInfoBoxEvent")
	KEP_EventSetFilter(ev, 0)
	KEP_EventEncodeNumber(ev, duration) -- seconds to display
	KEP_EventEncodeString(ev, message) -- message to display
	KEP_EventEncodeString(ev, texturepath) -- icon texture to display
	KEP_EventEncodeNumber(ev, coordinates.left) -- icon coordinate
	KEP_EventEncodeNumber(ev, coordinates.right) -- icon coordinate
	KEP_EventEncodeNumber(ev, coordinates.top) -- icon coordinate
	KEP_EventEncodeNumber(ev, coordinates.bottom) -- icon coordinate
	KEP_EventQueue( ev)
end

-- [[ called to open the client's web browser and display a timed notification ]]
function launchWebBrowser(link)
	-- show the timed info box
	showTimedInfoBox(3, "Opening Web Browser", "leave_notif_64x64.tga", {left = 66, right = 100, top = 13, bottom = 47})

	-- Launch the browser
	KEP_LaunchBrowser( link )
end

function isInAHome()
	return (KEP_GetZoneIndexType() == KEP.CI_HOUSING)
end

function sendFriendRequest(username, source)
	ADD_FRIEND_EVENT = "AddFriendEvent"
	local ev = KEP_EventCreate( ADD_FRIEND_EVENT)
	KEP_EventEncodeString(ev, username)
	KEP_EventEncodeString(ev, source)
	KEP_EventQueue( ev)
end

function acceptFriendRequest(friendId)
	ACCEPT_FRIEND_EVENT = "AcceptFriendEvent"
	local ev = KEP_EventCreate( ACCEPT_FRIEND_EVENT)
	KEP_EventEncodeNumber(ev, friendId)
	KEP_EventQueue( ev)
end

function sendRaveRequest(userId, username, source)
	SEND_RAVE_EVENT = "SendRaveEvent"
	local ev = KEP_EventCreate( SEND_RAVE_EVENT)
	KEP_EventEncodeNumber(ev, userId)
	KEP_EventEncodeString(ev, username)
	KEP_EventEncodeString(ev, source)
	KEP_EventQueue( ev)
end

function resize_9Slice( width, height )
	Dialog_SetSize( gDialogHandle, width, height )
	
	local defaultX = 0
	local defaultY = -30
	
	local defaultWidth = Control_GetWidth( gHandles["mUpperLeft_Slice"] )
	local defaultHeight = Control_GetHeight( gHandles["mUpperRight_Slice"] )
	
	local topHeight = 30
	local bottomHeight = 10
	
	--Set Top Row
	--TopLeft Remains the same
	--TopCenter changes Width
	local lWidth = width - ( Control_GetWidth( gHandles["mUpperLeft_Slice"] ) + Control_GetWidth( gHandles["mUpperRight_Slice"] ) )
	Control_SetSize( gHandles["mUpperCenter_Slice"], lWidth, topHeight )
	--TopRight changes X
	local lX = ( Control_GetWidth( gHandles["mUpperLeft_Slice"] ) + Control_GetWidth( gHandles["mUpperCenter_Slice"] ) )
	Control_SetLocation( gHandles["mUpperRight_Slice"], lX, defaultY )
	
	--Set Middle Row
	
	--MiddleLeft Changes Height
	local lHeight = height - Control_GetHeight( gHandles["mUpperLeft_Slice"] ) - Control_GetHeight( gHandles["mLowerLeft_Slice"] )
	Control_SetSize( gHandles["mMiddleLeft_Slice"], defaultWidth, lHeight )
	
	--MiddleCenter changes height and width
	Control_SetSize( gHandles["mMiddleCenter_Slice"], lWidth, lHeight )
	
	--MiddleRight changes height and x
	Control_SetSize( gHandles["mMiddleRight_Slice"], defaultWidth, lHeight )
	Control_SetLocation( gHandles["mMiddleRight_Slice"], lX, defaultY + Control_GetHeight( gHandles["mUpperRight_Slice"] ) )
	
	--BottomLeft Changes Y
	lY = lHeight
	Control_SetLocation( gHandles["mLowerLeft_Slice"], 0 , lY )
	
	--BottomCenter changes Y and width
	Control_SetLocation( gHandles["mLowerCenter_Slice"], Control_GetWidth( gHandles["mLowerLeft_Slice"] ), lY )	
	Control_SetSize( gHandles["mLowerCenter_Slice"], lWidth, bottomHeight )
	
	--BottomRight changes Y and X
	lX = ( Control_GetWidth( gHandles["mUpperLeft_Slice"] ) + Control_GetWidth( gHandles["mUpperCenter_Slice"] ) )
	Control_SetLocation( gHandles["mLowerRight_Slice"], lX, lY )	
end

g_urlMap = {
	["kaneva://kaneva city"] = "Kaneva City.zone",
	["kaneva://city"] = "Kaneva City.zone",
	["kaneva city"] = "Kaneva City.zone",
	["city"] = "Kaneva City.zone",
	["kaneva://one click employment agency"] = "Employment Agency.zone",
	["kaneva://employment agency"] = "Employment Agency.zone",
	["kaneva://employment"] = "Employment Agency.zone",
	["employment"] = "Employment Agency.zone",
	["kaneva://kaching"] = "Ka-Ching! Lobby.zone#kaching_default",
	["kaching"] = "Ka-Ching! Lobby.zone#kaching_default",
	["kaneva://dance party 3d game"] = "The Third Dimension.zone",
	["kaneva://dance party"] = "The Third Dimension.zone",
	["kaneva://dance"] = "The Third Dimension.zone",
	["dance"] = "The Third Dimension.zone",
	["kaneva://mall"] = "Mall.zone",
	["mall"] = "Mall.zone",
	["kaneva://the plaza"] = "The Plaza.zone",
	["kaneva://plaza"] = "The Plaza.zone",
	["the plaza"] = "The Plaza.zone",
	["plaza"] = "The Plaza.zone"
} 
function gotoURL( url, import )

    -- Valid URL?
    if (url == nil) then
        return
    end
    cfLog("gotoURL: '"..url.."'")
       
    -- DRF - Handle Mapped Shortcuts
    local urlNew = g_urlMap[string.lower(url)]
    if (urlNew ~= nil) then
        url =  GetStarUrl().."/"..urlNew
        cfLog("... -> urlNew='"..url.."'")
    end
    
	-- Handle Home Shortcut
	local urlHome = GetHomeUrl()
	if (string.lower(url) == "home") then
		url = urlHome
        cfLog("... -> urlNew='"..url.."'")
	end

	-- Handle Missing 'kaneva://' Shortcuts
	if (string.find(url, "kaneva://") == nil) then
		url = "kaneva://"..url
        cfLog("... -> urlNew='"..url.."'")
	end

	-- Queue 'ValidateURLEvent' With Url
	local ev = KEP_EventCreate( "ValidateURLEvent" )
	KEP_EventEncodeString ( ev, url)

	if import then
		KEP_EventEncodeString ( ev, "ImportZone")
	end

	KEP_EventQueue( ev )
end

function LongNumberToString(number)
	local result = tostring(number)
	local size = string.len(result)
	if size > 3 then
		local i = size - 2
		while i > 1 do
			result = string.sub(result, 1, i - 1)..","..string.sub(result, i, -1)
			i = i - 3
		end
	end
	return result
end

function IsCoreGlid( glid )
	return glid>=CORE_GLID_START and glid<=CORE_GLID_END
end

function requestUnlockableInformation(type, id)
	REQUEST_UNLOCKABLE_EVENT = "RequestUnlockableEvent"
	local ev = KEP_EventCreate( REQUEST_UNLOCKABLE_EVENT)
	KEP_EventSetFilter(ev, UnlockableRequestFilter.FeatureRequest)
	KEP_EventEncodeNumber(ev, type)
	KEP_EventEncodeNumber(ev, id)
	KEP_EventQueue( ev)
end

function Analytics_Track( page )
	local ev = KEP_EventCreate( "AnalyticsTrackEvent" )
	KEP_EventEncodeString( ev, page )
	KEP_EventQueue( ev )
end

function WOKAnalytics_Track( page )
	local ev = KEP_EventCreate( "WOKAnalyticsTrackEvent" )
	KEP_EventEncodeString( ev, page )
	KEP_EventQueue( ev )
end

function WorldsAnalytics_Track( page )
	local ev = KEP_EventCreate( "WorldsAnalyticsTrackEvent" )
	KEP_EventEncodeString( ev, page )
	KEP_EventQueue( ev )
end

--Sync the List Boxes
g_bSyncAllLists = 0
function syncAllLists( listhandles, listchanged )
	if g_bSyncAllLists == 1 then
		return
	end
	g_bSyncAllLists = 1
	lindex = ListBox_GetSelectedItemIndex(listchanged)
	for i,_ in pairs(listhandles) do
		if listhandles[i] ~= listchanged then
			if lindex ~= nil and lindex >= 0 then
				ListBox_SelectItem(listhandles[i], lindex )
			else
				ListBox_ClearSelection(listhandles[i])
			end
		end
	end
	g_bSyncAllLists = 0
end

function syncAllScrollBars(listhandles, listchanged)
	g_bSyncAllLists = 1
	sb = ListBox_GetScrollBar(listchanged)
	spos = ScrollBar_GetTrackPos(sb)
	for i,_ in pairs(listhandles) do
		if listhandles[i] ~= listchanged then
			sb = ListBox_GetScrollBar(listhandles[i])
			ScrollBar_SetTrackPos(sb, spos)
		end
	end
	g_bSyncAllLists = 0
	return spos
end

---------------------------------------------------------------------
-- DRF - Info Box Functions
---------------------------------------------------------------------

-- Creates an info box pop-up message
function InfoBoxMsg(msg, returnCode)
	if (returnCode ~= nil) then
		msg = msg.." ("..returnCode..")"
	end
    --cfLog("InfoBoxMsg: '"..msg.."'")
	local e = KEP_EventCreate( KEP.GENERIC_INFO_BOX_EVENT )
	KEP_EventEncodeString( e, msg )
	KEP_EventSetFilter( e, KEP.FILTER_GENERIC_INFO_BOX_MSG1 )
	KEP_EventQueue( e )
end

---------------------------------------------------------------------
-- Truncate Static:
-- Pass in text to truncate, handle it needs to fit in (must be static) 
-- and whether to add ellipsis
-- Returns static (potentially with ellipsis) of desired length
---------------------------------------------------------------------
function truncateStaticToControl(text, staticHandle, addEllipsis)
	local textWidth = Static_GetStringWidth(staticHandle, text)
	local controlWidth = Control_GetWidth(staticHandle)
	local returnString = text
	
	if textWidth > controlWidth then
		local low,high,mid = 1, string.len(text), 0
		
		while low <= high do
			mid = math.floor( (low+high)/2 )
			
			local tempString = string.sub(text, 1, mid)
			tempString = addEllipsis and (tempString .. "...") or tempString --if addEllipsis is true, add to end
			textWidth = Static_GetStringWidth(staticHandle, tempString)
			
			if textWidth >= controlWidth then
				high = mid - 1
			else
				low = mid + 1
				returnString = tempString -- Largest string that fits
			end
		end
	end
	
	return returnString
end

---------------------------------------------------------------------
-- Truncate String to Length:
-- Called to truncate a string variable to a given character count,
-- optionally ending in an ellipsis.
---------------------------------------------------------------------
function truncateStringToLength(text, length, addEllipsis)
	local returnString = ""

	if string.len(text) > length then
		if addEllipsis then
			returnString = (string.sub(text, 1, (length - 3)) .. "...")
		else
			returnString = string.sub(text, 1, length)
		end
	else
		returnString = text
	end
	
	return returnString
end

---------------------------------------------------------------------
-- Stretch Static To Text:
-- Pass in text to set, handle it needs to stretch (must be static) 
---------------------------------------------------------------------
function stretchStaticToText(staticHandle, text)
	local controlWidth = Static_GetStringWidth(staticHandle, text)
	local controlHeight = Control_GetHeight(staticHandle)
	Control_SetSize(staticHandle, controlWidth, controlHeight)
	Static_SetText(staticHandle, text)
end

---------------------------------------------------------------------
--Check to see if the player is new (based on MIN_SIGNUP M/D/Y)
--SignupDate = return value from browser
--thresholdDate = array with year, month, date
--Return true if new, false if old
---------------------------------------------------------------------
function playerNewerThanDate(signupDate, thresholdDate)
	if type(signupDate) ~= "string" or type(thresholdDate) ~= "table" then
		cfLogError("playerNewerThanDate: Incorrect parameter types")
		return true
	end

	local s, e, m, d, y = string.find(signupDate, "(%d+)/(%d+)/(%d+)")
	if m and d and y then 
		m = tonumber(m)
		d = tonumber(d)
		y = tonumber(y)
		
		if y <  thresholdDate.year or
		  (y <= thresholdDate.year and m <  thresholdDate.month) or
		  (y <= thresholdDate.year and m <= thresholdDate.month  and d < thresholdDate.day) then	
			--Player is an old player
			return false
		else
			--Player is a new player
			return true
		end
	else
		cfLogError("playerNewerThanDate: Signup Date not found or in incorrect format")
		return true
	end
end


-- recursivly dump table
function Proxy_TraceDump(aVar, indent)
	indent = indent or 0

	if (type(aVar) ~= "table") then
		cfLog(string.rep(" ", indent).." Value["..tostring(aVar).."]")
	else
		for k, v in pairs(aVar) do
			if type(v) == "table" then
				cfLog(string.rep(" ", indent).." Key["..tostring(k).."]")
				Proxy_TraceDump(v, indent + 2)
			else
				cfLog(string.rep(" ", indent).." Key["..tostring(k).."], Value["..tostring(v).."]")
			end
		end
	end
end

-- Change the font name/size/etc of a button. Must change all elements. All params after handle optional
function changeButtonFont(buttonHandle, fontSize, fontName, weight, italics)
	if (type(buttonHandle) ~= "userdata") then 
		cfLog("changeButtonFont::Param 1 must be a handle")
		return 
	end

	local elements = {
		Button_GetDisabledDisplayElement(buttonHandle),
		Button_GetFocusedDisplayElement(buttonHandle),
		Button_GetMouseOverDisplayElement(buttonHandle),
		Button_GetNormalDisplayElement(buttonHandle),
		Button_GetPressedDisplayElement(buttonHandle)
	}

	-- Font size breaks if not negative
	if fontSize and fontSize > 0 then
		fontSize = -fontSize
	end

	for i, element in pairs(elements) do
		Element_AddFont(element, gDialogHandle, fontName or "Verdana", fontSize or 11, weight or 400, italics or false)
	end
end

-- Change the texture of a button. Must change all elements.
function changeButtonTexture(buttonHandle, texture, coordinates)
	if (type(buttonHandle) ~= "userdata") then cfLog("changeButtonTexture::Param 1 must be a handle") return end
	if (type(texture) ~= "string") then cfLog("changeButtonFont::Param 2 must be a string") return end
	if (type(coordinates) ~= "table") then cfLog("changeButtonFont::Param 3 must be a table") return end

	local elements = {
		Button_GetDisabledDisplayElement(buttonHandle),
		Button_GetFocusedDisplayElement(buttonHandle),
		Button_GetMouseOverDisplayElement(buttonHandle),
		Button_GetNormalDisplayElement(buttonHandle),
		Button_GetPressedDisplayElement(buttonHandle)
	}

	for i, element in pairs(elements) do
		Element_AddTexture(element, gDialogHandle, texture)
		Element_SetCoords(element, coordinates.left, coordinates.top, coordinates.right, coordinates.bottom)
	end
end

function Button_SetFontAlpha(buttonHandle, alphaLevel)
	if (type(buttonHandle) ~= "userdata") then 
		cfLog("Button_SetFontAlpha::Param 1 must be a handle")
		return 
	end

	if Control_GetType(buttonHandle) ~= ControlTypes.DXUT_CONTROL_BUTTON then
		cfLog("Button_SetFontAlpha::Param 1 must be a button")
		return
	end

	-- In buttons, only want to update corresponding element + state combinations
	local elements = {
		{ element = Button_GetDisabledDisplayElement(buttonHandle),		state = ControlStates.DXUT_STATE_DISABLED},
		{ element = Button_GetFocusedDisplayElement(buttonHandle),		state = ControlStates.DXUT_STATE_FOCUS},
		{ element = Button_GetMouseOverDisplayElement(buttonHandle),	state = ControlStates.DXUT_STATE_MOUSEOVER},
		{ element = Button_GetNormalDisplayElement(buttonHandle),		state = ControlStates.DXUT_STATE_NORMAL},
		{ element = Button_GetPressedDisplayElement(buttonHandle),		state = ControlStates.DXUT_STATE_PRESSED}
	}

	for i, elementSet in pairs(elements) do
		local fontColorElement = Element_GetFontColor(elementSet.element)
		local fontColor = BlendColor_GetColor(fontColorElement, elementSet.state)
		fontColor.a = alphaLevel
		BlendColor_SetColor(fontColorElement, elementSet.state, fontColor)
	end
end

---------------------------------------------------------------------
-- DRF - Moved From ClientScripts\STPHelper.lua
---------------------------------------------------------------------

ZoneURL = {}
ZoneURL.__index = ZoneURL
PREFIX =  "kaneva"

function ZoneURL.build(suffix)
	return PREFIX .."://"..suffix
end

function ZoneURL.create(url, name, category)
	local zoneURL = {}             -- our new object
	setmetatable(zoneURL,ZoneURL)
	zoneURL.url = url
	zoneURL.name = name
	zoneURL.category = category
	zoneURL.timeStamp = 0
	return zoneURL
end

-- DRF - 'kaneva://whatever/path/Zone.Name' -> 'Zone.Name'
function ParseNameFromUrl(url)
	local a, b, c = string.match(url, "(.-)([^/]-([^%.]+))$")
	local name = b or ""
	cfLog("ParseNameFromUrl: url='"..url.."' name='"..name.."'")
	return name
end

function parseNameFromURL(url)
	s, e, name = string.find(url, PREFIX .."://.-/(.*)[.].*", 1)
	return name
end

function isURL3DApp(url)
	local gamename = parseGameNameFromURL(url)
	if ((string.lower(gamename) ~= "kaneva preview") and (string.lower(gamename) ~= "rc kaneva") and (string.lower(gamename) ~= "world of kaneva") and (string.lower(gamename) ~= "dev star")) then
		return true
	end
	return false
end

function parseFriendlyNameFromURL(url)
	local name     = parseNameFromURL(url)
	local zoneType = parseZoneTypeFromURL(url)
	local gamename = parseGameNameFromURL(url)

	if isURL3DApp(url) == true then
		name = gamename
	end

	local friendlyName = name
	if zoneType == "home" then
		friendlyName = friendlyName .. " - Home"
	end

	return friendlyName
end

function parseGameNameFromURL(url)
	s, e, name = string.find(url, PREFIX .."://(.-)/.*", 1)
	if name == nil then
		name = ""
	end
	return name
end

function parseZoneTypeFromURL(url)
	s, e, name = string.find(url, PREFIX .."://.-/.-[.](.*)", 1)
	if name == nil then
		name = ""
	end
	return name
end

function parseReferenceFromURL(url) --#default for example, the spawn reference
	s, e, name = string.find(url, ".-#(.+)", 1)
	if name == nil then
		name = ""
	end
	return name
end

function isUrlUserHome(url, username)
	local urlType = string.lower(parseZoneTypeFromURL(url) or "")
	local parsedName = string.lower(parseNameFromURL(url) or "")

	return urlType == "home" and parsedName == string.lower(username)
end

function readHistoryIntoTable(event)
	local zones = {}
	numZones = KEP_EventDecodeNumber(event)
	for i = 1, numZones do
		local name = KEP_EventDecodeString( event)
		local url  = KEP_EventDecodeString( event )
		local zoneURL = ZoneURL.create(url, name, "HISTORY")
		table.insert(zones, zoneURL)
	end

	return zones
end

function readFavoritesIntoTable(event)
	local zoneURLs = {}
	numZones = KEP_EventDecodeNumber( event )
	for i = 1, numZones do
		local name = KEP_EventDecodeString( event)
		local url = KEP_EventDecodeString( event )
		local category = KEP_EventDecodeString( event )
		local zoneURL = ZoneURL.create(url, name, category)
		table.insert(zoneURLs, zoneURL)
	end

	return zoneURLs
end

-- Generates a stroke for a static (4 statics offset the original)
function strokeTheString(control, RGB, textFormat)
	local controlX 		= Control_GetLocationX(control)
	local controlY 		= Control_GetLocationY(control)
	local controlWidth  = Control_GetWidth(control)
	local controlHeight = Control_GetHeight(control)
	local staticText 	= Static_GetText(control)
	local controlName 	= Control_GetName(control)
	
	local strokes = {topLeft 	= Dialog_AddStatic(gDialogHandle, controlName.."backgroundTL", staticText, controlX-1, controlY-1, controlWidth, controlHeight),
				topRight 	= Dialog_AddStatic(gDialogHandle, controlName.."backgroundTR", staticText, controlX+1, controlY-1, controlWidth, controlHeight),
				bottomLeft 	= Dialog_AddStatic(gDialogHandle, controlName.."backgroundBL", staticText, controlX-1, controlY+1, controlWidth, controlHeight),
				bottomRight = Dialog_AddStatic(gDialogHandle, controlName.."backgroundBR", staticText, controlX+1, controlY+1, controlWidth, controlHeight)
			   }
	for name, strokeControl in pairs(strokes) do
		-- Set color for background to input
		local backgroundElementHandle = Static_GetDisplayElement(strokeControl)
		BlendColor_Init(Element_GetFontColor(backgroundElementHandle), RGB, RGB, RGB)
		BlendColor_SetColor(Element_GetFontColor(backgroundElementHandle), 0, RGB)
		-- Set background test format to mirror original control's
		Element_SetTextFormat(backgroundElementHandle, textFormat)
		-- Push these statics to the back
		Dialog_MoveControlToBack(gDialogHandle, strokeControl)
	end
	return strokes
end

-- Adjusts stroke to keep up with their parent
function syncTheStrokes(strokes, control, font, fontSize, fontWeight, fontItalics, RGB)
	local controlX 		= Control_GetLocationX(control)
	local controlY 		= Control_GetLocationY(control)
	local controlWidth  = Control_GetWidth(control)
	local controlHeight = Control_GetHeight(control)
	local staticText 	= Static_GetText(control)
	
	-- Adjust all background statics
	for name, strokeControl in pairs(strokes) do
		Control_SetSize(strokeControl, controlWidth, controlHeight)
		Element_AddFont(Static_GetDisplayElement(strokeControl), gDialogHandle, font, fontSize, fontWeight, fontItalics)
		-- Match parent's font size and weight
		Static_SetText(strokeControl, staticText)
		
		BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(strokeControl)), 0, RGB)
	end
	
	Control_SetLocation(strokes.topLeft, 	 controlX-1, controlY-1)
	Control_SetLocation(strokes.topRight,    controlX+1, controlY-1)
	Control_SetLocation(strokes.bottomLeft,  controlX-1, controlY+1)
	Control_SetLocation(strokes.bottomRight, controlX+1, controlY+1)
end

-- DRF - Opens MemberAlts and sends a MemberAltsEvent to it with the given userName
function SendMemberAltsEvent(userName, userId)
	
	-- Only In DevMode As GM!
	local devMode = ToBool(KEP_DevMode())
	local isGM = ToBool(KEP_IsGM())
	if (not devMode or not isGM) then return end

	-- Register MemberAltsEvent (MemberAlts menu also needs it)
	KEP_EventRegister( "MemberAltsEvent", KEP.MED_PRIO )

	-- Open MemberAlts Menu
	MenuOpen("MemberAlts")

	-- Send MemberAltsEvent
	local ev = KEP_EventCreate( "MemberAltsEvent" )
	KEP_EventEncodeString(ev, userName or GetUserName())
	KEP_EventEncodeNumber(ev, userId or 0) -- optional
	KEP_EventQueue(ev)
end

function GetHideHUDSetting()
	local filename = validateConfigName(GetUserName().."HideHud")

	local setting = false
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local hideHudOK, hideHudON =  KEP_ConfigGetNumber( config, filename, setting and 1 or 0)
		setting = hideHudON == 1
	end 

	return setting
end

function SetHideHUDSetting(minimized)
	local filename = validateConfigName(GetUserName().."HideHud")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber(config, filename, minimized)
		KEP_ConfigSave(config)
	end

	local ev = KEP_EventCreate("HideHUDSettingEvent")
	KEP_EventEncodeNumber(ev, minimized)
	KEP_EventQueue(ev)
end

function GetMemberTipFirstTimeSetting()
	local filename = validateConfigName(GetUserName().."MemberTipFirstTime")
	local setting = true
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local memberTipFirstTimeOK, memberTipFirstTimeON =  KEP_ConfigGetNumber( config, filename, 1)
		setting = memberTipFirstTimeON == 1
	end 

	return setting
end

function SetMemberTipFirstTimeSetting(didOnce)
	local filename = validateConfigName(GetUserName().."MemberTipFirstTime")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber(config, filename, didOnce)
		KEP_ConfigSave(config)
	end
end

function GetHideToolbeltSetting()
	local filename = validateConfigName(GetUserName().."HideToolbelt")
	local setting = true
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local hideHudOK, hideHudON =  KEP_ConfigGetNumber(config, filename, setting and 1 or 0)
		setting = hideHudON == 1
	end 

	return setting
end

function SetHideToolbeltSetting(minimized)
	local filename = validateConfigName(GetUserName().."HideToolbelt")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber(config, filename, minimized)
		KEP_ConfigSave(config)
	end

	local ev = KEP_EventCreate("HideToolbeltSettingEvent")
	KEP_EventEncodeNumber(ev, minimized)
	KEP_EventQueue(ev)
end

function GetHideMapSetting()
	local filename = validateConfigName(GetUserName().."HideMapIcon")
	local setting = 1 -- default to hiding the map icon
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local hideMapOK, hideMapON = KEP_ConfigGetNumber(config, filename, setting)
		setting = hideMapON == 1
	end 

	return setting
end

function SetHideMapSetting(hideMap)
	local filename = validateConfigName(GetUserName().."HideMapIcon")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber(config, filename, hideMap)
		KEP_ConfigSave(config)
	end

	local ev = KEP_EventCreate("HideMapIconSettingEvent")
	KEP_EventEncodeNumber(ev, hideMap)
	KEP_EventQueue(ev)
end

function GetHideGlowSetting()
	local filename = validateConfigName(GetUserName().."HideObjectGlow")
	local setting = false 
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local hideGlowOK, hideGlowON = KEP_ConfigGetNumber(config, filename, setting and 1 or 0)
		setting = hideGlowON == 1
	end 

	return setting
end

function SetHideGlowSetting(hideGlow)
	local filename = validateConfigName(GetUserName().."HideObjectGlow")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber(config, filename, hideGlow)
		KEP_ConfigSave(config)
	end

	local ev = KEP_EventCreate("HideObjectGlowSettingEvent")
	KEP_EventEncodeNumber(ev, hideGlow)
	KEP_EventQueue(ev)
end

function GetHideAFKSetting()
	local filename = validateConfigName(GetUserName().."HideAFKStatus")
	local setting = false
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local hideAFKOK, hideAFKON = KEP_ConfigGetNumber(config, filename, setting and 1 or 0)
		setting = hideAFKON == 1
	end 

	return setting
end

function SetHideAFKSetting(hideAFK)
	local filename = validateConfigName(GetUserName().."HideAFKStatus")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber(config, filename, hideAFK)
		KEP_ConfigSave(config)
	end

	local ev = KEP_EventCreate("HideAFKStatusSettingEvent")
	KEP_EventEncodeNumber(ev, hideAFK)
	KEP_EventQueue(ev)
end

--Camera Elevation. If enabled, changing the value, else setting the camera to the config
--1 enables setting, 0 disables setting
function SetCameraLevel(enabled)
	local filename = validateConfigName(GetUserName().."AutoAdjustCamera")

	local setting = enabled or  1 --1 is default for on
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		if enabled then
			KEP_ConfigSetNumber(config, filename, enabled)
			KEP_ConfigSave(config)
		else
			local camOK, adjustCamera =  KEP_ConfigGetNumber( config, filename, setting)
			setting = adjustCamera
		end
	end 

	if setting == 1 then
		KEP_SetPlayerCameraAutoElevation(CAM_ENABLED_ADJUST_ANGLE)
	else
		KEP_SetPlayerCameraAutoElevation(CAM_DISABLED_ADJUST_ANGLE)
	end
end

--Save if the Home button should be hidden
function GetHideHomeSetting()
	local filename = validateConfigName(GetUserName().."HideHomeIcon")
	local setting = 0 -- default to show home icon
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local hideHomeOK, hideHomeON = KEP_ConfigGetNumber(config, filename, setting)
		setting = hideHomeON == 1
	end 

	return setting
end

function SetHideHomeSetting(hideHome)
	local filename = validateConfigName(GetUserName().."HideHomeIcon")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber(config, filename, hideHome)
		KEP_ConfigSave(config)
	end

	local ev = KEP_EventCreate("HideHomeIconSettingEvent")
	KEP_EventEncodeNumber(ev, hideHome)
	KEP_EventQueue(ev)
end

--Save where the home button will send the player
function GetHomeLink()
	local filename = validateConfigName(GetUserName().."HomeLink")
	local setting = GetHomeUrl() -- default to player's home
	--A/B Test for Home Icon setting: B group for setting home button to Kaneva City
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local homeLinkOK, homeLink = KEP_ConfigGetString(config, filename, setting)
		setting = homeLink
	end 

	return setting
end

function SetHomeLink(url)
	local filename = validateConfigName(GetUserName().."HomeLink")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetString(config, filename, url)
		KEP_ConfigSave(config)
	end
end

--Save where the player will start
function GetStartingLink()
	local filename = validateConfigName(GetUserName().."StartLink")
	local setting = nil
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local startLinkOK, startLink = KEP_ConfigGetString(config, filename, setting)
		setting = startLink
	end 

	return setting
end

function SetStartingLink(url)
	local filename = validateConfigName(GetUserName().."StartLink")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetString(config, filename, url)
		KEP_ConfigSave(config)
	end
end

--Save if starting settings is set to a world or continue where user left off
function GetStartingOption()
	local filename = validateConfigName(GetUserName().."StartOption")
	local setting = 1 -- default to start in home world
	--A/B Test for Starting World Setting: C group to start where I left off
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local startOptionOK, startOption = KEP_ConfigGetString(config, filename, setting)
		setting = startOption
	end 

	return setting
end

function SetStartingOption(index)
	local filename = validateConfigName(GetUserName().."StartOption")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetString(config, filename, index)
		KEP_ConfigSave(config)
	end
end

--Save the starting world string for Start in world string
function GetWorldStartString()
	local filename = validateConfigName(GetUserName().."StartString")
	local setting = GetHomeUrl() -- default to player's home
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		local startStringOK, startString = KEP_ConfigGetString(config, filename, setting)
		setting = startString
	end 

	return setting
end

function SetWorldStartString(url)
	local filename = validateConfigName(GetUserName().."StartString")
	local config = KEP_ConfigOpenWOK()

	if config ~= nil and config ~= 0 then
		KEP_ConfigSetString(config, filename, url)
		KEP_ConfigSave(config)
	end
end

-- Save emotes to '%APPDATA%\Kaneva\<gameId>\<userName>Emotes.cfg'
function SaveDefaultKeyBoardEmotes()

	-- Save Default Emotes
	local filename = GetUserName().."Emotes.cfg"
	local f, errmsg = KEP_FileOpenConfig(filename,"w")
	if (f == 0) then
		cfLogError("SaveDefaultKeyBoardEmotes: FileOpenConfig("..filename..") FAILED")
		return false
	end

	-- Page 1 - 10 Emotes
	KEP_FileWriteLine(f, "1\n") -- 1 Page
	KEP_FileWriteLine(f, "1\n") -- Page 1...
	--
	KEP_FileWriteLine(f, "-1\n") -- Emote Key
	KEP_FileWriteLine(f, "\n") -- Emote Name
	KEP_FileWriteLine(f, "-1\n") -- Emote GLID
	--
	KEP_FileWriteLine(f, "1\n") -- Emote Key
	KEP_FileWriteLine(f, "Sit\n") -- Emote Name
	KEP_FileWriteLine(f, "28\n") -- Emote GLID
	--
	KEP_FileWriteLine(f, "2\n") -- Emote Key
	KEP_FileWriteLine(f, "Clap\n") -- Emote Name
	KEP_FileWriteLine(f, "69\n") -- Emote GLID
	--
	KEP_FileWriteLine(f, "3\n") -- Emote Key
	KEP_FileWriteLine(f, "Wave\n") -- Emote Name
	KEP_FileWriteLine(f, "24\n") -- Emote GLID
	--
	KEP_FileWriteLine(f, "4\n") -- Emote Key
	KEP_FileWriteLine(f, "Laugh\n") -- Emote Name
	KEP_FileWriteLine(f, "27\n") -- Emote GLID
	--
	KEP_FileWriteLine(f, "5\n") -- Emote Key
	KEP_FileWriteLine(f, "Yes\n") -- Emote Name
	KEP_FileWriteLine(f, "64\n") -- Emote GLID
	--
	KEP_FileWriteLine(f, "6\n") -- Emote Key
	KEP_FileWriteLine(f, "No\n") -- Emote Name
	KEP_FileWriteLine(f, "65\n") -- Emote GLID
	--
	KEP_FileWriteLine(f, "7\n") -- Emote Key
	KEP_FileWriteLine(f, "Bustamove\n") -- Emote Name
	KEP_FileWriteLine(f, "196\n") -- Emote GLID
	--
	KEP_FileWriteLine(f, "8\n") -- Emote Key
	KEP_FileWriteLine(f, "Disco Fever\n") -- Emote Name
	KEP_FileWriteLine(f, "184\n") -- Emote GLID
	--
	KEP_FileWriteLine(f, "9\n") -- Emote Key
	KEP_FileWriteLine(f, "Bump N Grind\n") -- Emote Name
	if KEP_GetLoginGender() == "M" then
		KEP_FileWriteLine(f, "155\n") -- Emote GLID Male
	else
		KEP_FileWriteLine(f, "149\n") -- Emote GLID Female
	end

	KEP_FileClose(f)

	return true
end

function getDistanceToPlayer(targetName)
	if targetName == nil then cfLogError("getDistanceToPlayer: targetName is nil."); return end
	local distance = nil
	if targetName ~= KEP_GetLoginName() and ToBool(KEP_PlayerIsValid(targetName)) then
		local target = KEP_GetPlayerByName(targetName)
		if target ~= nil then
			local playerLoc = {KEP_GetPlayerPosition()}
			local targetLoc = {GetSet_Safe_GetVector(target, MovementIds.LASTPOSITION)}

			distance = math.sqrt(math.pow(targetLoc[1] - playerLoc[1], 2) + math.pow(targetLoc[2] - playerLoc[2], 2) + math.pow(targetLoc[3] - playerLoc[3], 2))
		end
	end
	return distance
end

--config names can NOT start with a number
function validateConfigName(filename)
	local firstCharacter = string.sub(filename, 1, 1)
	if tonumber(firstCharacter) then
		filename = "u"..filename
	end

	return filename
end
