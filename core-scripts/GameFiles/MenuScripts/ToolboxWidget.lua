--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\Scripts\\KEP.lua") 

screenHeight = 0
screenWidth = 0
widgetY = 100
widgetX = 0
menuWidth= 0
menuHeight= 0
newscreenHeight = 0

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) 
	
	screenWidth = Dialog_GetScreenWidth(dialogHandle)
	screenHeight = Dialog_GetScreenHeight(dialogHandle)
	local size = MenuGetSizeThis()
	widgetX = screenWidth - size.width
	
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber( config, "CreatorToolbox", 0 )
		KEP_ConfigSetNumber( config, "ToolboxWidget", 1 )
		KEP_ConfigSave( config )
		
		--read in previous position of widget		
		local ToolBoxXOK, ToolWidgetX =  KEP_ConfigGetNumber( config, "ToolboxX", widgetX)
		local ToolBoxYOK, ToolWidgetY =  KEP_ConfigGetNumber( config, "ToolboxY", 100)
		
		if ToolBoxWidgetOK == 1 then 
			widgetX = ToolWidgetX 
		end
		
		if ToolBoxYOK == 1 then 
			widgetY = ToolWidgetY
		end 
	end

	MenuSetLocationThis(widgetX, widgetY) 
end

function Dialog_OnMoved(dialogHandle)
	local newscreenWidth = Dialog_GetScreenWidth(dialogHandle)
	local newscreenHeight = Dialog_GetScreenHeight(dialogHandle)
	local ratio = widgetY/screenHeight --pos y of menu/screensize
	
	if newscreenWidth ~= screenWidth then 
		screenWidth = newscreenWidth
		widgetX = screenWidth-menuWidth
	end 
	if newscreenHeight ~= screenHeight then 
		widgetY = ratio * newscreenHeight 
		screenHeight = newscreenHeight
	end 
	
	MenuSetLocationThis(widgetX, widgetY) 
	writetoConfig(widgetX,widgetY)
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	local is_down = Dialog_IsLMouseDown( dialogHandle )
	
	if is_down then 
		if widgetY > (screenHeight-50-menuHeight) then 
			widgetY = screenHeight-50-menuHeight
		else 
			if widgetY < 0 then 
				widgetY = 0
			else 
				widgetY = Dialog_GetLocationY(dialogHandle)
			end
		end 
		
		if widgetX < screenWidth-menuWidth then 
			widgetX = screenWidth-menuWidth
		end
		MenuSetLocationThis(widgetX, widgetY) 
		writetoConfig(widgetX,widgetY)
	end 
end 

function writetoConfig(posX,posY)
	--write position to config file 
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber( config, "ToolboxX", posX )
		KEP_ConfigSetNumber( config, "ToolboxY", posY )
		KEP_ConfigSave( config )
	end
end 

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle ) 
end

function btnArrow_OnButtonClicked(buttonHandle) 
	MenuOpen("CreatorToolbox.xml")
	MenuCloseThis()
end
