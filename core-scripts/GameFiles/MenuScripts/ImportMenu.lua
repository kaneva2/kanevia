--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")
dofile("CreditBalances.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

gTarget = {}
gTarget.valid = 0
gTarget.pos = {}
gTarget.pos.x = 0
gTarget.pos.y = 0
gTarget.pos.z = 0
gTarget.normal = {}
gTarget.normal.x = 0
gTarget.normal.y = 0
gTarget.normal.z = 0
gTarget.type = ObjectType.UNKNOWN
gTarget.id = -1
gTarget.name = ""

gNextDropId = 1000000	-- a separate drop ID segment (1000000-...) DragDropEvents use (0~999999)

gImportType = UGC_TYPE.UNKNOWN
gImportTarget = nil

g_t_items = {}
g_t_acc = {}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "FileBrowserClosedHandler", "FileBrowserClosedEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "attribEventHandler", "AttribEvent", AttribEvent.PLAYER_INVEN, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.HIGH_PRIO+1 )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( "FileBrowserClosedEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- disable "Next" button before user makes a selection
	enableControl( "btnNext", false )

	-- load player inventory for accessory animation import
	KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function ImportTypeRadioButton_OnRadioButtonChanged(buttonHandle)
	local rb, chk

	enableControl( "lstAnimChoices", false )
	clearListBoxSelection( "lstAnimChoices" )
	gImportTarget = nil

	enableControl( "btnNext", true )

	rb = Dialog_GetRadioButton( gDialogHandle, "rbImportDynObj" )
	chk = RadioButton_GetCheckBox( rb )
	if CheckBox_GetChecked( chk )==1 then
		gImportType = UGC_TYPE.DYNOBJ
		return
	end

	rb = Dialog_GetRadioButton( gDialogHandle, "rbImportEmote" )
	chk = RadioButton_GetCheckBox( rb )
	if CheckBox_GetChecked( chk )==1 then
		gImportType = UGC_TYPE.CHARANIM
		gImportTarget = 0
		return
	end

	rb = Dialog_GetRadioButton( gDialogHandle, "rbImportAcc" )
	chk = RadioButton_GetCheckBox( rb )
	if CheckBox_GetChecked( chk )==1 then
		gImportType = UGC_TYPE.EQUIPPABLE
		return
	end

	rb = Dialog_GetRadioButton( gDialogHandle, "rbImportAccAnim" )
	chk = RadioButton_GetCheckBox( rb )
	if CheckBox_GetChecked( chk )==1 then
		gImportType = UGC_TYPE.CHARANIM
		enableControl( "lstAnimChoices", true )
		return
	end

	rb = Dialog_GetRadioButton( gDialogHandle, "rbImportSound" )
	chk = RadioButton_GetCheckBox( rb )
	if CheckBox_GetChecked( chk )==1 then
		gImportType = UGC_TYPE.SOUND
		return
	end

	-- if no match, disable Next button again
	enableControl( "btnNext", false )
end

rbImportDynObj_OnRadioButtonChanged = ImportTypeRadioButton_OnRadioButtonChanged
rbImportEmote_OnRadioButtonChanged = ImportTypeRadioButton_OnRadioButtonChanged
rbImportAcc_OnRadioButtonChanged = ImportTypeRadioButton_OnRadioButtonChanged
rbImportAccAnim_OnRadioButtonChanged = ImportTypeRadioButton_OnRadioButtonChanged
rbImportSound_OnRadioButtonChanged = ImportTypeRadioButton_OnRadioButtonChanged

function lstAnimChoices_OnListBoxSelection( listboxHandle, pt )
	local idx, text, data = getListBoxSelectedItem( "lstAnimChoices" )
	gImportTarget = data
end

function btnNext_OnButtonClicked(buttonHandle)
	if gImportType==UGC_TYPE.UNKNOWN then
		-- Import type not selected
		KEP_MessageBox( "Please select an import type before proceeding" )
		return
	elseif gImportType==UGC_TYPE.CHARANIM then
		-- Animation target desired
		if gImportTarget==nil then
			KEP_MessageBox( "Please select a target for animation import" )
			return
		end
	end

	BrowseAndImportAsync( gImportType, gImportTarget )
end

function btnHelp_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( DESIGNER_COLLADA_URL )
end

function btnImpPattern_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .. "mykaneva/upload.aspx" )
end

function FileBrowserClosedHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	local dialogResult = KEP_EventDecodeNumber( event )
	local filePath = ""
	if dialogResult~=0 then
		filePath = KEP_EventDecodeString( event )
	else
		MenuCloseThis()
		return
	end

	local dropId = gNextDropId
	gNextDropId = gNextDropId + 1

	local importInfo = {}
	importInfo.fullPath = filePath
	importInfo.sessionId = -1

	if filter==UGC_TYPE.DYNOBJ then
		importInfo.category = UGC_TYPE.DYNOBJ
		importInfo.type = UGC_TYPE.DYNOBJ
		UGCI.CallUGCHandler("DaeImport.xml", 1, dropId, importInfo, gTarget, 0, 0 )
	elseif filter==UGC_TYPE.CHARANIM then
		if objectid~=0 then
			-- Equippable animation import
			gTarget.type = ObjectType.EQUIPPABLE
			gTarget.id = objectid
		else
			gTarget.type = ObjectType.PLAYER
			gTarget.id = 0
		end

		importInfo.category = UGC_TYPE.CHARANIM
		importInfo.type = UGC_TYPE.CHARANIM

		UGCI.CallUGCHandler( "DaeImport.xml", 1, dropId, importInfo, gTarget, 0, 0 )

	elseif filter==UGC_TYPE.SOUND then

		if string.lower(string.sub(filePath, -4))==".wav" then
			importInfo.category = UGC_TYPE.SOUND
			importInfo.type = UGC_TYPE.WAV_SOUND
			UGCI.ImportAndSubmitWavFile( importInfo, gTarget, 0, 0 )
		elseif string.lower(string.sub(filePath, -4))==".ogg" then
			importInfo.category = UGC_TYPE.SOUND
			importInfo.type = UGC_TYPE.OGG_SOUND
			UGCI.ImportAndSubmitOggFile( importInfo, gTarget, 0, 0 )
		end
	elseif filter==UGC_TYPE.EQUIPPABLE then
		importInfo.category = UGC_TYPE.EQUIPPABLE
		importInfo.type = UGC_TYPE.EQUIPPABLE	-- actual type (rigid/skeletal) to be determined in import process
		UGCI.CallUGCHandler( "DaeImport.xml", 1, dropId, importInfo, gTarget, 0, 0 )
	end

	MenuCloseThis()
end

function BrowseAndImportAsync( ugcType, optionalGlid )
	local event = KEP_EventCreate( "FileBrowserClosedEvent" )
	if optionalGlid~=nil then
		KEP_EventSetObjectId( event, optionalGlid )
	end
	KEP_EventSetFilter( event, ugcType )
	UGC_BrowseFileByImportType( ugcType, event ) -- second param (initial folder) TBD
end

function ParsePlayerInventoryItems( xmlData )
	g_t_items = {}
	decodeInventoryItemsFromWeb( g_t_items, xmlData, false )
	g_t_acc = separateArmedAccessories(g_t_items)
	populateAccessories()
end

function separateArmedAccessories(items_table)
	local resultTable = {}
	local t_item = {}
	if items_table ~= nil then
		for i=1,#items_table do
			t_item = items_table[i]
			if t_item["is_armed"] then
				table.insert(resultTable, t_item)
			end
		end
	end
	return resultTable
end

function populateAccessories()
	local lbh = Dialog_GetListBox(gDialogHandle, "lstAnimChoices")
	ListBox_RemoveAllItems(lbh)
	for i=1, #g_t_acc do
		local item = g_t_acc[i]
		ListBox_AddItem(lbh, item["name"], item["GLID"])
	end
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == AttribEvent.PLAYER_INVEN then
		local lbh = Dialog_GetListBox(gDialogHandle, "lstAnimChoices")
		if lbh ~= 0 then
			ListBox_RemoveAllItems(lbhQ)
		end
		getPlayerInventory(WF.PLAYER_EQUIP_INVENTORY, nil, nil, nil, nil, nil, USE_TYPE.EQUIP)
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.PLAYER_EQUIP_INVENTORY then
		local xmlData = KEP_EventDecodeString( event )
		ParsePlayerInventoryItems( xmlData )
		return KEP.EPR_OK						-- don't consume this
	end
end
