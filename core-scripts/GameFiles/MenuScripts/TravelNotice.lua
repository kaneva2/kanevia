--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

TRAVEL_NOTICE_EVENT = "TravelNoticeEvent"

g_accessReason = 0
g_accessMessage = ""
g_communityId = 0
g_thumbnailPath = ""
g_thumbnailFile = ""
g_previousAccessMessage = ""

-- settings to render notice in generic message mode
g_genericSettings = {}
g_genericSettings.width = 334
g_genericSettings.height = 188
g_genericSettings.x = 40
g_genericSettings.btnOkay = {}
g_genericSettings.btnOkay.x = 128
g_genericSettings.btnOkay.y = 118
g_genericSettings.imgDivider = {}
g_genericSettings.imgDivider.width = 330
g_genericSettings.imgDivider.y = 106
g_genericSettings.btnClose = {}
g_genericSettings.btnClose.x = 311
g_genericSettings.stcAccessMessage = {}
g_genericSettings.stcAccessMessage.x = 81
g_genericSettings.stcAccessMessage.y = 78
g_genericSettings.imgWorldIcon = {}
g_genericSettings.imgWorldIcon.x = 134
g_genericSettings.imgWorldIcon.y = 10

g_messagePosition = {}
g_minCharsBeforeDisplacement = 40
g_maxCharsBeforeDisplacement = 70
g_minVerticalDisplacement = 5
g_maxVerticalDisplacement = 8

g_accessReasons = {}
g_accessReasons.Generic = -1
g_accessReasons.AccessAllowed = 0
g_accessReasons.Over21Required = 1
g_accessReasons.AccessPassRequired = 2
g_accessReasons.VIPPassRequired = 3
g_accessReasons.MembershipRequired = 4
g_accessReasons.FriendshipRequired = 5
g_accessReasons.OwnerAccessOnly = 6
g_accessReasons.UserBlocked = 7

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler("travelNoticeHandler", TRAVEL_NOTICE_EVENT, KEP.LOW_PRIO)
	KEP_EventRegisterHandler("textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.TRAVEL_NOTICE_REQUEST_ACCESS then
		KEP_MessageBox("The owner of this world has been notified of your request.")
		MenuCloseThis()
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnRequestAccess_OnButtonClicked( buttonHandle )
	local url = ""

	if g_accessReason == g_accessReasons.AccessPassRequired then
		url = GameGlobals.WEB_SITE_PREFIX_KANEVA .. "mykaneva/passDetails.aspx?pass=true&passId=82"
	elseif g_accessReason == g_accessReasons.VIPPassRequired then
		url = GameGlobals.WEB_SITE_PREFIX_KANEVA .. "mykaneva/passDetails.aspx?pass=true&passId=74"
	elseif g_accessReason == g_accessReasons.MembershipRequired then
		local webURL = GameGlobals.WEB_SITE_PREFIX..REQUEST_ACCESS_SUFFIX..g_communityId
		makeWebCall(webURL, WF.TRAVEL_NOTICE_REQUEST_ACCESS)
		Control_SetEnabled(buttonHandle, false)
	end

	if string.len(url) > 0 then
		KEP_LaunchBrowser( url )
	end
end

function btnOkay_OnButtonClicked( buttonHandle )
	MenuCloseThis()
end

function textureDownloadHandler()		
	if g_thumbnailFile then
		local ctrlPic = gHandles["imgWorldThumbnail"]
		local ctrlFrame = gHandles["imgWorldThumbnailFrame"]
		scaleImage(ctrlPic, ctrlFrame, g_thumbnailFile)
    end
end

function travelNoticeHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_accessReason = KEP_EventDecodeNumber(event) or -1
	g_accessMessage = KEP_EventDecodeString(event) or ""
	g_communityId = KEP_EventDecodeNumber(event) or 0
	g_thumbnailPath = KEP_EventDecodeString(event) or ""

	initialConfig()
	displayTitle()
	displayMessage()
	displayThumbnail()
	displayButtons()
end

function initialConfig()
	if g_accessReason == g_accessReasons.UserBlocked then

		-- Transform menu elements to match the generic configuration
		Dialog_SetSize(gDialogHandle, g_genericSettings.width, g_genericSettings.height)
		Dialog_SetLocationX(gDialogHandle, Dialog_GetLocationX(gDialogHandle) + g_genericSettings.x)
		Control_SetSize(gHandles["imgDivider"], g_genericSettings.imgDivider.width, Control_GetHeight(gHandles["imgDivider"]))
		Control_SetLocation(gHandles["btnOkay"], g_genericSettings.btnOkay.x, g_genericSettings.btnOkay.y)
		Control_SetLocation(gHandles["imgDivider"], Control_GetLocationX(gHandles["imgDivider"]), g_genericSettings.imgDivider.y)
		Control_SetLocation(gHandles["btnClose"], g_genericSettings.btnClose.x, Control_GetLocationY(gHandles["btnClose"]))
		Control_SetLocation(gHandles["stcAccessMessage"], g_genericSettings.stcAccessMessage.x, g_genericSettings.stcAccessMessage.y)
		Control_SetLocation(gHandles["imgWorldIcon"], g_genericSettings.imgWorldIcon.x, g_genericSettings.imgWorldIcon.y)
	end

	g_messagePosition.x = Control_GetLocationX(gHandles["stcAccessMessage"])
	g_messagePosition.y = Control_GetLocationY(gHandles["stcAccessMessage"])
end

function displayTitle()
	if g_accessReason == g_accessReasons.AccessPassRequired then
		Static_SetText(gHandles["Menu_Title"], "Access Pass Required")
	elseif g_accessReason == g_accessReasons.VIPPassRequired then
		Static_SetText(gHandles["Menu_Title"], "VIP Pass Required")
	else
		Static_SetText(gHandles["Menu_Title"], "World Access")
	end
end

function displayMessage()
	local newX = g_messagePosition.x
	local newY = g_messagePosition.y
				 
	if g_accessMessage ~= g_previousAccessMessage then
		if string.len(g_accessMessage) > g_maxCharsBeforeDisplacement then
			newY = newY - g_maxVerticalDisplacement
		elseif string.len(g_accessMessage) < g_minCharsBeforeDisplacement then
			newY = newY + g_minVerticalDisplacement
		end

		Control_SetLocation(gHandles["stcAccessMessage"], newX, newY)
		g_previousAccessMessage = g_accessMessage
	end
	Static_SetText(gHandles["stcAccessMessage"], g_accessMessage)
end

function displayThumbnail()
	if g_accessReason == g_accessReasons.AccessPassRequired then
		SetControlEnabled("imgPassIconAP", true)
	elseif g_accessReason == g_accessReasons.VIPPassRequired then
		SetControlEnabled("imgPassIconVIP", true)
	elseif g_accessReason == g_accessReasons.UserBlocked then
		SetControlEnabled("imgWorldIcon", true)
	else
		SetControlEnabled("imgWorldThumbnail", true)
		SetControlEnabled("imgWorldThumbnailFrame", true)

		local filename = ""

		if string.len(g_thumbnailPath) <= 0 then
			g_thumbnailPath = DEFAULT_KANEVA_PLACE_ICON
		end

		g_thumbnailFile = tostring(split(GetFilenameFromPath(g_thumbnailPath), ".")[1])
		g_thumbnailFile = g_communityId .. "_" .. g_thumbnailFile .. CUSTOM_TEXTURE_THUMB
					
		KEP_DownloadTextureAsyncStr( g_thumbnailFile, g_thumbnailPath )
		g_thumbnailFile = "../../CustomTexture/" .. g_thumbnailFile
	end
end

function displayButtons()
	if g_accessReason == g_accessReasons.AccessPassRequired or g_accessReason == g_accessReasons.VIPPassRequired then
		SetControlEnabled("btnRequestAccess", true)
		SetControlEnabled("btnCancel", true)
		Static_SetText(Button_GetStatic(gHandles["btnRequestAccess"]), "Learn More")
	elseif g_accessReason == g_accessReasons.MembershipRequired then
		SetControlEnabled("btnRequestAccess", true)
		SetControlEnabled("btnCancel", true)
	else
		SetControlEnabled("btnOkay", true)
	end
end
