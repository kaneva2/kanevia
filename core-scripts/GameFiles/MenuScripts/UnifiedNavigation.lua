--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- UnifiedNavigation.lua 
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("UnifiedNavigationStates.lua")
dofile("MenuAnimation.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")
dofile("ContextualHelper.lua")

local INVENTORY_MENU = "UnifiedInventory.xml"
local SELECTED_BOLD_CONTROL = "stcSelectedBold"
local TAB_COUNT = 10
local MORE_COUNT = 7
local ROLLOVER_DELAY = .150
local FADE_IN_TIME = .100


local m_fadeInTween
local m_fadeTweening = false
local m_rollingOverMore = false --used now for More options
local m_rollingTimer = 0
local m_navState
local m_saveState
local m_selectedFilterIndex = nil

local m_playerName
local m_userID
local m_playerRewards = 0
local m_playerCredits = 0

local m_mouseEntered = false
local m_HUDOpen = false
local m_MoreOpen = false
local m_selectedIndex = 0

local m_playerMode = "Player" -- Assume player  mode unless told otherwise

-- When the menu is created
function onCreate()
	--Menu opening events
	KEP_EventRegister("UNIFIED_OPEN_MENU", HIGH_PRIO)
	KEP_EventRegister("UNIFIED_GOTO_SYSTEM", HIGH_PRIO)
	KEP_EventRegister("UNIFIED_GOTO_GAME_ITEM", HIGH_PRIO)
	KEP_EventRegister("UNIFIED_ENABLE_TOOLTIPS", HIGH_PRIO)
	KEP_EventRegister("UNIFIED_DISABLE_TOOLTIPS", HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_NAV_GOTO_ITEM", HIGH_PRIO)

	--Menu filter events
	KEP_EventRegister("WOK_INVENTORY_FILTER", HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_GAMING_FILTER", HIGH_PRIO)	
	KEP_EventRegister("UNIFIED_SET_GAME_ITEM_MODE", HIGH_PRIO)

	KEP_EventRegisterHandler("gotoSystemHandler", "UNIFIED_GOTO_SYSTEM", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "buildModeChangedEventHandler", "BuildModeChangedEvent", KEP.MED_PRIO )

	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )

	Events.registerHandler("FRAMEWORK_GOTO_GAME_ITEM", onGotoItem)
	KEP_EventRegisterHandler("gotoGameItem", "FRAMEWORK_NAV_GOTO_ITEM", KEP.HIGH_PRIO)

	for i = 1, TAB_COUNT do
		_G["btnItem" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			closeMoreMenu()
			handleAction(i)
		end
	end

	for i = 1, TAB_COUNT do
		_G["btnItem" .. tostring(i) .. "_OnMouseEnter"] = function(btnHandle)
			if i ~= m_selectedIndex then
				Control_SetVisible(gHandles["imgItemRollover"..i], true)
			end
		end
	end

	for i = 1, TAB_COUNT do
		_G["btnItem" .. tostring(i) .. "_OnMouseLeave"] = function(btnHandle)
			Control_SetVisible(gHandles["imgItemRollover"..i], false)
		end
	end

	for i = 1, MORE_COUNT do
		_G["btnMore" .. tostring(i) .. "_OnMouseEnter"] = function(btnHandle)
			Control_SetVisible(gHandles["imgMoreRollover"..i], true)
			m_rollingOverMore = true
		end
	end

	for i = 1, MORE_COUNT do
		_G["btnMore" .. tostring(i) .. "_OnMouseLeave"] = function(btnHandle)
			Control_SetVisible(gHandles["imgMoreRollover"..i], false)
		end
	end

	for i = 1, TAB_COUNT do
		_G["btnMore" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			handleMoreAction(i)
		end
	end

	m_playerName = KEP_GetLoginName()
	requestUserId()
	
	MenuOpen(INVENTORY_MENU)	
	MenuBringToFrontThis()
end

function onDestroy()
end

function requestUserId()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..m_playerName, WF.MYFAME_USERID)
end

function ParseUserID(returnData)
	local result = nil  	-- result description
	local s, e = 0      	-- start and end of captured string
	s, e, result = string.find(returnData, "<ReturnCode>(%d+)</ReturnCode>")
	if result == "0" then
		-- Parse needed info from XML
		s, e, result = string.find(returnData, "<user_id>(%d+)</user_id>", e)
		m_userID = tonumber(result)

		requestCreditsRewards()
	end
end

-- Gets current player balance through CreditBalances.lua
function requestCreditsRewards()
	-- If the user requested exists, get their balance
	if m_userID > 0 then
		getBalances(m_userID, WF.MYFAME_GETBALANCE)
	end
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	MenuAnimation_OnRender(fElapsedTime)	
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	
	if m_rollingOverMore then
		local mouseLeft = true
		for i = 1, MORE_COUNT do
			if Control_ContainsPoint(gHandles["imgMoreBG"..i], x, y) ~= 0 then
				mouseLeft = false
			end
		end

		if mouseLeft then
			closeMoreMenu()
		end
	end

	if MenuIsOpen("Framework_NPCInventory") and MenuIsOpen("CharacterCreation3") then
		if Control_ContainsPoint(gHandles["imgDialogBG"], x, y) == 0 then
			Dialog_SetModal(gDialogHandle, false)
			MenuSetModal("Framework_NPCInventory", true)
		end
	end
end
-------------------------------------------------
-- EVENT HANDLERS
-------------------------------------------------

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	if filter == WF.MYFAME_GETBALANCE then
		local returnData = KEP_EventDecodeString( tEvent )
		m_creditBalance, m_rewards = ParseBalances(returnData)
		
	elseif filter == WF.MYFAME_USERID then
		local returnData = KEP_EventDecodeString( tEvent )
		ParseUserID(returnData)
	end
end


function gotoSystemHandler(dispatcher, fromNetid, event, eventid, filter, objectid)	
	local system = KEP_EventDecodeString(event)

	closeMoreMenu()
	--Close if menu is open

	if system == "gaming" and m_navState == NAV_STATES.GAMING then
		MenuClose("UnifiedInventory.xml")
		MenuCloseThis()
		return
	elseif system == "wokGaming" and m_navState == NAV_STATES.WOK_GAMING then
		MenuClose("UnifiedInventory.xml")
		MenuCloseThis()
		return
	elseif system == "building" and m_navState == NAV_STATES.BUILD then
		MenuClose("UnifiedInventory.xml")
		MenuCloseThis()
		return
	elseif system == "clothing" and m_navState == NAV_STATES.CLOTHING then
		if MenuIsOpen("Framework_ClothingVendor.xml") then return end
		MenuClose("UnifiedInventory.xml")
		MenuCloseThis()
		return
	end

	if system == "backpack" and m_navState == NAV_STATES.PLAYER_BACKPACK then
		MenuClose("UnifiedInventory.xml")
		MenuCloseThis()
		return
	end

	if system == "crafting" and m_navState == NAV_STATES.PLAYER_CRAFTING then
		MenuClose("UnifiedInventory.xml")
		MenuCloseThis()
		return
	end

	m_selectedIndex = 1
	if system == "clothing" then
		setNavState(NAV_STATES.CLOTHING, true)
		setBold(1, "All")
		setMenuTitle("Clothing")
	elseif system == "npc" then
		setNavState(NAV_STATES.NPC, true)
		setBold(1, "All")
		setMenuTitle("NPC Clothing")
	elseif system == "player" or system == "backpack" then
		--this one
		setNavState(NAV_STATES.PLAYER_BACKPACK, true)
		setBold(1, "Items")
		setMenuTitle("Backpack")

	elseif system == "crafting" then
		--this one
		m_selectedIndex = 3
		setNavState(NAV_STATES.PLAYER_CRAFTING, true)
		setBold(3, "Crafting")
		setMenuTitle("Backpack")
	elseif system == "building" then
		setNavState(NAV_STATES.BUILD, true)
		setBold(1, "All")
		setMenuTitle("Building")
	elseif system == "quests" then
		--this one
		m_selectedIndex = 2
		setNavState(NAV_STATES.PLAYER_QUESTS,true)
		setBold(2, "Quests")
		setMenuTitle("Backpack")
	elseif system == "gaming" then	
		setNavState(NAV_STATES.GAMING, true)
		sendUnifiedMenuEvent()
		local ev = KEP_EventCreate("UNIFIED_SET_GAME_ITEM_MODE")
		KEP_EventEncodeString(ev, "1")
		KEP_EventQueue(ev)
		setBold(1, "All")
		setMenuTitle("Game System")
	elseif system == "wokGaming" then
		setNavState(NAV_STATES.WOK_GAMING, true)
		sendUnifiedMenuEvent()
		local ev = KEP_EventCreate("UNIFIED_SET_GAME_ITEM_MODE")
		KEP_EventEncodeString(ev, "0")
		KEP_EventQueue(ev)
		setBold(1, "All")
		setMenuTitle("Gaming")
	end

	sendUnifiedMenuEvent()
	
    if system == "building" then
        sendUnifiedMenuFilterEvent(NAV_STATE_LAYOUTS[NAV_STATES.BUILD].actionList[1].filterEvent, NAV_STATE_LAYOUTS[NAV_STATES.BUILD].actionList[1].filterParams)
    end
end

function onGotoItem(event)
	setNavState(NAV_STATES.GAMING, true)
	sendUnifiedMenuEvent()
	handleAction(1)

	local itemGoto = event.itemUNID
--	local itemType = event.itemType
	--local behavior = event.behavior
	local ev = KEP_EventCreate("UNIFIED_GOTO_GAME_ITEM")
	KEP_EventEncodeString(ev, itemGoto)
	KEP_EventQueue(ev)
end

function gotoGameItem(dispatcher, fromNetid, event, eventid, filter, objectid)	

	local itemGoto = KEP_EventDecodeString(event)
	-- local itemType = event.itemType
	-- local behavior = event.behavior
	local ev = KEP_EventCreate("UNIFIED_GOTO_GAME_ITEM")
	KEP_EventEncodeString(ev, itemGoto)
	KEP_EventQueue(ev)
end

function showContextualHelp()
	--log("--- showContextualHelp")
	hideBackpackContextualHelp()

	-- show the contextual help for navigating to the backpack	
	local contextEvent = KEP_EventCreate("showContextualHelpEvent")
	local menuLocation = MenuGetLocationThis()
	local eventParams = {}
	eventParams.type = ContextualHelp.OPEN_BACKPACK
	eventParams.x = Control_GetLocationX(gHandles["imgItem1"]) + menuLocation.x
	eventParams.y = Control_GetLocationY(gHandles["imgItem1"]) + menuLocation.y
	eventParams.width = Control_GetWidth(gHandles["imgItem1"])
	eventParams.height = Control_GetHeight(gHandles["imgItem1"])
	eventParams.arrow = ContextualHelpFormat.RIGHT_ARROW
	KEP_EventEncodeString(contextEvent, Events.encode(eventParams))
	KEP_EventQueue(contextEvent)
end

function hideBackpackContextualHelp()
	-- hide all other backpack contextual help tips 
	-- CONTEXTUAL HELP OBJECT ROLLOVER HIDE
	ContextualHelper.hideHelp(ContextualHelp.OBJECT_ROLLOVER)

	-- CONTEXTUAL HELP TOOLBELT OVERVIEW HIDE
	ContextualHelper.hideHelp(ContextualHelp.TOOLBELT_OVERVIEW)

	-- CONTEXTUAL HELP EMPTY BACKPACK HIDE
	ContextualHelper.hideHelp(ContextualHelp.EMPTY_BACKPACK)

	-- CONTEXTUAL HELP HIDE CLOSE BACKPACK
	ContextualHelper.hideHelp(ContextualHelp.CLOSE_BACKPACK)
end

function buildModeChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	MenuCloseThis()
	MenuClose("UnifiedInventory.xml")
end
-------------------------------------------------
-- NAV DISPLAY FUNCTIONS
-------------------------------------------------

function handleAction(index)
	if NAV_STATE_LAYOUTS[m_navState] and NAV_STATE_LAYOUTS[m_navState].actionList[index] then
		local currentState = m_navState
		m_selectedIndex = index
		
		if NAV_STATE_LAYOUTS[currentState].actionList[index].text == "More" then

			openMoreMenu( Control_GetLocationY(gHandles["btnItem"..index]), index, currentState)
		
		elseif NAV_STATE_LAYOUTS[currentState].actionList[index].stateChange then
			m_selectedFilterIndex = nil
			setNavState(NAV_STATE_LAYOUTS[currentState].actionList[index].stateChange, true)
			sendUnifiedMenuEvent()
			KEP_EventCreateAndQueue("UNIFIED_DISABLE_TOOLTIPS")

			setBold(index, NAV_STATE_LAYOUTS[currentState].actionList[index].text)
			Static_SetText(gHandles["stcNavState"],   NAV_STATE_LAYOUTS[currentState].actionList[index].text)

		else
			m_selectedFilterIndex = index
			
			if currentState ~= m_saveState then
				setNavState(currentState, true)				
			end

			setBold(index, NAV_STATE_LAYOUTS[currentState].actionList[m_selectedFilterIndex].text)
			if  NAV_STATE_LAYOUTS[currentState].actionList[index].text == "All" then
				if currentState == NAV_STATES.GAMING then
					Static_SetText(gHandles["stcNavState"],  "Game System")
				else
					Static_SetText(gHandles["stcNavState"],  NAV_STATE_LAYOUTS[currentState].title)
				end
			else
				Static_SetText(gHandles["stcNavState"],  NAV_STATE_LAYOUTS[currentState].actionList[index].text)
			end
		end

		if NAV_STATE_LAYOUTS[currentState].actionList[index].gamingFilter then
			populateDropDown(index, currentState)
		else
			setControlEnabledVisible("cmbDropDown", false)
		end

		if NAV_STATE_LAYOUTS[currentState].actionList[index].filterEvent then
			sendUnifiedMenuFilterEvent(NAV_STATE_LAYOUTS[currentState].actionList[index].filterEvent, NAV_STATE_LAYOUTS[currentState].actionList[index].filterParams)
		end
	end
end

function handleMoreAction(index)
	if NAV_STATE_LAYOUTS[m_navState] then
		local currentState = m_navState
		
			if currentState ~= NAV_STATES.BUILD then
				Static_SetText(gHandles["stcNavState"],  NAV_STATE_LAYOUTS[currentState].actionList[m_selectedIndex].moreOptions[index].text)
			else
				Static_SetText(gHandles["stcNavState"],  NAV_STATE_LAYOUTS[currentState].title.." ".. NAV_STATE_LAYOUTS[currentState].actionList[m_selectedIndex].moreOptions[index].text)
			end

		setNavState(currentState, true)	

		if  NAV_STATE_LAYOUTS[currentState].actionList[m_selectedIndex].moreOptions[index].filterEvent then
			sendUnifiedMenuFilterEvent(NAV_STATE_LAYOUTS[currentState].actionList[m_selectedIndex].moreOptions[index].filterEvent, NAV_STATE_LAYOUTS[currentState].actionList[m_selectedIndex].moreOptions[index].filterParams)
		end
		setBold(m_selectedIndex, "More")
		closeMoreMenu()
	end
end

function openMoreMenu(y, index, currentState)
	Control_SetLocation(gHandles["imgMoreBG1"], Control_GetLocationX(gHandles["imgMoreBG1"]), y)
	Control_SetLocation(gHandles["imgMoreBG2"], Control_GetLocationX(gHandles["imgMoreBG2"]), y + 30)
	Control_SetLocation(gHandles["imgMoreRollover1"], Control_GetLocationX(gHandles["imgMoreRollover1"]), y + 5)
	Control_SetLocation(gHandles["imgMoreRollover2"], Control_GetLocationX(gHandles["imgMoreRollover2"]), y + 30)
	Control_SetLocation(gHandles["stcMore1"], Control_GetLocationX(gHandles["stcMore1"]), y + 5)
	Control_SetLocation(gHandles["stcMore2"], Control_GetLocationX(gHandles["stcMore2"]), y + 30)
	Control_SetLocation(gHandles["btnMore1"], Control_GetLocationX(gHandles["btnMore1"]), y + 5)
	Control_SetLocation(gHandles["btnMore2"], Control_GetLocationX(gHandles["btnMore2"]), y + 30)
	for i = 1, MORE_COUNT do
		if not  NAV_STATE_LAYOUTS[currentState].actionList[index].moreOptions[i].skip then
			if i > 2 then
			Control_SetLocation(gHandles["imgMoreBG"..i], Control_GetLocationX(gHandles["imgMoreBG"..i]), (Control_GetLocationY(gHandles["imgMoreBG"..(i - 1)]) + 25))
			Control_SetLocation(gHandles["imgMoreRollover"..i], Control_GetLocationX(gHandles["imgMoreRollover"..i]), (Control_GetLocationY(gHandles["imgMoreBG"..(i - 1)]) + 25))
			Control_SetLocation(gHandles["stcMore"..i], Control_GetLocationX(gHandles["stcMore"..i]), (Control_GetLocationY(gHandles["imgMoreBG"..(i - 1)]) + 25))
			Control_SetLocation(gHandles["btnMore"..i], Control_GetLocationX(gHandles["btnMore"..i]), (Control_GetLocationY(gHandles["imgMoreBG"..(i - 1)]) + 25))
			end
			setControlEnabledVisible("imgMoreBG"..i, true)
			setControlEnabledVisible("stcMore"..i, true)
			setControlEnabledVisible("btnMore"..i, true)

			Static_SetText(gHandles["stcMore"..i], NAV_STATE_LAYOUTS[currentState].actionList[index].moreOptions[i].text)
		end
	end
	m_selectedIndex = index
	m_MoreOpen = true
end

function closeMoreMenu()
	for i = 1, MORE_COUNT do
		--setControlEnabledVisible("imgMoreRollover"..i, false)
		setControlEnabledVisible("imgMoreBG"..i, false)
		setControlEnabledVisible("stcMore"..i, false)
		setControlEnabledVisible("btnMore"..i, false)
	end

	m_MoreOpen = false
	m_rollingOverMore = false
end

function populateDropDown(index, currentState)

	setControlEnabledVisible("cmbDropDown", true)
	local cb = Dialog_GetComboBox(gDialogHandle, "cmbDropDown")
	ComboBox_RemoveAllItems(cb)
	local count  = #NAV_STATE_LAYOUTS[currentState].actionList[index].gamingFilter 
	for i = 1, count do
		ComboBox_AddItem(cb, NAV_STATE_LAYOUTS[currentState].actionList[index].gamingFilter[i].text, i)
	end
	ComboBox_SetDropHeight(cb, count * 15)

	_G["cmbDropDown_OnComboBoxSelectionChanged"] = function(comboBoxHandle, selectedIndex, selectedText)		
		sendUnifiedMenuFilterEvent(NAV_STATE_LAYOUTS[currentState].actionList[index].gamingFilter[selectedIndex + 1].filterEvent, NAV_STATE_LAYOUTS[currentState].actionList[index].gamingFilter[selectedIndex + 1].filterParams)
	end

end

function setBold(index, text)
	local currentState = m_navState
	for i = 1, TAB_COUNT do
		if i == index then

			Static_SetText(gHandles[SELECTED_BOLD_CONTROL], text)
			Control_SetVisible(gHandles[SELECTED_BOLD_CONTROL], true)
			Control_SetLocation(gHandles[SELECTED_BOLD_CONTROL], Control_GetLocationX(gHandles["stcItem"..index]), Control_GetLocationY(gHandles["stcItem"..index]))
			Control_SetVisible(gHandles["stcItem"..index], false)
		else
			if Control_GetEnabled(gHandles["btnItem"..i]) == 1 then
				Control_SetVisible(gHandles["stcItem"..i], true)
			end
		end
	end
	Control_SetLocation(gHandles["imgItemSelected"], Control_GetLocationX(gHandles["imgItemRollover"..index]), Control_GetLocationY(gHandles["imgItemRollover"..index]))
	Control_SetVisible(gHandles["imgItemRollover"..index], false)
end
function sendUnifiedMenuEvent()
	local menu = getMenuForState(m_navState)

	local menuEvent = KEP_EventCreate( "UNIFIED_OPEN_MENU" )
	KEP_EventEncodeString(menuEvent, menu)
	KEP_EventQueue( menuEvent )
end

function sendUnifiedMenuFilterEvent(eventName, eventParam)
	local menuEvent = KEP_EventCreate(eventName)

	local eventInfo
	if type(eventParam) == "table" then
		eventInfo = Events.encode(eventParam)
	else
		eventInfo = eventParam
	end

	KEP_EventEncodeString(menuEvent, eventInfo)
	KEP_EventQueue( menuEvent )
end


function setNavState(state, save)
	
	if m_navState then
		--Clear old state controls
		if NAV_STATE_LAYOUTS[m_navState] and NAV_STATE_LAYOUTS[m_navState].actionList then
			for i=1,#NAV_STATE_LAYOUTS[m_navState].actionList do
				setControlEnabledVisible("imgItem" .. i, false)
				setControlEnabledVisible("stcItem" .. i, false)
				setControlEnabledVisible("btnItem" .. i, false)
			end
		end
	end

	if NAV_STATE_LAYOUTS[state] then	
		if NAV_STATE_LAYOUTS[state] and NAV_STATE_LAYOUTS[state].actionList then
			for i=1,#NAV_STATE_LAYOUTS[state].actionList do
				if NAV_STATE_LAYOUTS[state].actionList[i].skip then
					setControlEnabledVisible("imgItem" .. i, false)
					setControlEnabledVisible("stcItem" .. i, false)
					setControlEnabledVisible("btnItem" .. i, false)
				else						
					setControlEnabledVisible("imgItem" .. i, true)					
					setControlEnabledVisible("stcItem" .. i, true)
					setControlEnabledVisible("btnItem" .. i, true)


					local element = Image_GetDisplayElement(gHandles["imgItem" .. i])
					Element_SetCoords(element, NAV_STATE_LAYOUTS[state].actionList[i].coord.left, NAV_STATE_LAYOUTS[state].actionList[i].coord.top, NAV_STATE_LAYOUTS[state].actionList[i].coord.right, NAV_STATE_LAYOUTS[state].actionList[i].coord.bottom)
		

					Static_SetText(gHandles["stcItem" .. i], NAV_STATE_LAYOUTS[state].actionList[i].text)
				end
			end
		end
	end

	if state == NAV_STATES.GAMING then
		Static_SetText(gHandles["stcNavState"], "Game System")
	else
		Static_SetText(gHandles["stcNavState"], NAV_STATE_LAYOUTS[state].title)
	end
	--end

	m_navState = state

	if m_selectedIndex == 1 then
		setControlEnabledVisible("imgItemSelected", false)
		setControlEnabledVisible("imgItemSelectedTop", true)
		if NAV_STATE_LAYOUTS[m_navState].actionList[1].filterEvent then
			sendUnifiedMenuFilterEvent(NAV_STATE_LAYOUTS[m_navState].actionList[1].filterEvent, NAV_STATE_LAYOUTS[m_navState].actionList[1].filterParams)
		end
	else
		setControlEnabledVisible("imgItemSelected", true)
		setControlEnabledVisible("imgItemSelectedTop", false)
	end

	setControlEnabledVisible("cmbDropDown", false)
end

-- Add comma to separate thousands
function formatCommaValue(value)
  	local formatted = value
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end


function setControlEnabledVisible(controlName, bool)
	Control_SetVisible(gHandles[controlName], bool)
	Control_SetEnabled(gHandles[controlName], bool)
end

function isBackpackState(state)

	if state == NAV_STATES.GEMS or state == NAV_STATES.PLAYER_BACKPACK or state == NAV_STATES.PLAYER_QUESTS or state == NAV_STATES.PLAYER_CRAFTING then
		return true
	else
		return false
	end
end

function isGamingState(state)
	if state == NAV_STATES.GAMING then
			return true
	else
			return false
	end
end

function setMenuTitle(title)
	local ev = KEP_EventCreate("UNIFIED_SET_TITLE")
	KEP_EventEncodeString(ev, title)
	KEP_EventQueue(ev)
end