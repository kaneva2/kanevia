--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Common Script Includes
dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\CMD.lua")
dofile("..\\Scripts\\AttribEventIds.lua")
dofile("..\\Scripts\\BrowserIds.lua")
dofile("..\\Scripts\\BuildMode.lua")
dofile("..\\Scripts\\CameraIds.lua")
dofile("..\\Scripts\\ChatType.lua")
dofile("..\\Scripts\\ControlTypes.lua")
dofile("..\\Scripts\\FameIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\MovementCapsIds.lua")
dofile("..\\Scripts\\NotificationTypes.lua")
dofile("..\\Scripts\\NudgeIds.lua")
dofile("..\\Scripts\\PlaceCategoryIds.lua")
dofile("..\\Scripts\\PlayerIds.lua")
dofile("..\\Scripts\\ScriptClientEventTypes.lua")
dofile("..\\Scripts\\ScriptServerEventTypes.lua")
dofile("..\\Scripts\\SoundPlacementIds.lua")
dofile("..\\Scripts\\UGCGlobals.lua")
dofile("..\\Scripts\\UnlockableIds.lua")
dofile("..\\Scripts\\UIGlobals.lua")
dofile("..\\Scripts\\ZoneNavIds.lua")

-- Framework Library Includes
-- dofile("..\MenuScripts\\LibClient_Common.lua") -- drf - nested dofile issues

-- Common MenuScript Includes
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\WebSuffix.lua")

-- This Menu Script Properties
gDialogHandle = 0
gDialogMenuId = "MENU_ID_NOT_CREATED"
gDialogMenuName = "MENU_NAME_NOT_CREATED"
gDialogScriptName = "SCRIPT_NAME_NOT_CREATED"
gHandles = {} -- handles of all menu controls

function mhLog(txt) KEP_Log(LogName().."MenuHelper::"..txt) end
function mhLogError(txt) KEP_LogError(LogName().."MenuHelper::"..txt) end

-- DRF - Menu Script OnCreate() Must Call This First!
function Helper_Dialog_OnCreate(dialogHandle)

	-- Store This Dialog Handle, Menu & Script Names
	gDialogHandle = dialogHandle
	gDialogMenuId = Dialog_MenuId(dialogHandle)
	gDialogMenuName = Dialog_MenuName(dialogHandle)
	gDialogScriptName = Dialog_ScriptName(dialogHandle)
	
	-- Call Menu Script Initialization Functions (PreInitialize->InitializeKEPEvents/Handlers->PostInitialize)
	Dialog_RegisterGenericEvents(dialogHandle)

	-- Build Global Control Handle Table
	local numControls = Dialog_GetNumControls( dialogHandle )
	for i=0,numControls-1 do
		local control = Dialog_GetControlByIndex( dialogHandle, i )
		local name = Control_GetName( control )
		gHandles[name] = control
	end
end

-- DRF - Menu Script OnDestroy() Must Call This Last!
function Helper_Dialog_OnDestroy(dialogHandle)
	Dialog_UnRegisterGenericEvents(dialogHandle);
	gDialogHandle = 0
	gDialogMenuId = "MENU_ID_DESTROYED"
	gDialogMenuName = "MENU_NAME_DESTROYED"
	gDialogScriptName = "SCRIPT_NAME_DESTROYED"
end

-- DRF - DEPRECATED - Moved To KEP_InitializeEventHandlers()
function HelperInitializeKEPEventHandlers(dispatcher, handler, debugLevel)
    KEP_InitializeEventHandlers(dispatcher, handler, debugLevel)
end

-- DRF - DEPRECATED - Moved to KEP_InitializeEvents()
function HelperInitializeKEPEvents(dispatcher, handler, debugLevel)
    KEP_InitializeEvents(dispatcher, handler, debugLevel)
end

-- DRF - DEPRECATED - Moved To KEP_InitializeEvents()
function MenuHelperInitializeKEPEvents(dispatcher, handler, debugLevel)
end

-- DRF - Menu Script OnKeyDown() Should Call This First!
function Helper_Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	-- No Default Behavior
end

-- DRF - Default OnKeyDown Handler
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	Helper_Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
end

-- DRF - Menu Script OnKeyDown() Should Call This First!
function Helper_Dialog_OnMoved(dialogHandle, x, y)
	mhLog("Dialog_OnMoved("..x..", "..y..")")
end

-- DRF - Default OnMoved Handler
function Dialog_OnMoved(dialogHandle, x, y)
	Helper_Dialog_OnMoved(dialogHandle, x, y)
end

-- DRF - Default Ok Button Behavior
function btnOk_OnButtonClicked( buttonHandle)
	MenuCloseThis()
end

-- DRF - Default OK Button Behavior
function btnOK_OnButtonClicked( buttonHandle)
	MenuCloseThis()
end

-- DRF - Default Close Button Behavior
function btnClose_OnButtonClicked( buttonHandle)
	MenuCloseThis()
end

-- DRF - Default Cancel Button Behavior
function btnCancel_OnButtonClicked( buttonHandle)
	MenuCloseThis()
end

-- DRF - Default Error Handler - Error message logged before this call.
function Dialog_OnError(dialogHandle, errMsg)
	mhLogError("Dialog_OnError: ^^^^^ FIX THIS ERROR ^^^^^")
end
