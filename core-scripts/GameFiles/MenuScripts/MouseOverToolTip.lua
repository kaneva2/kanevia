--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\MenuHelper.lua")

m_text = ""
m_textHeight = 0
m_textWidth = 0

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "MouseOverToolTipEvent", KEP.MED_PRIO )
end

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "mouseToolTipHandler", "MouseOverToolTipEvent", KEP.HIGH_PRIO )
end

function SetText(text)
	
	-- Set New Text
	m_text = text or ""
	Static_SetText(gHandles["stcMouseToolTip"], m_text)
	
	-- Don't Show If No Text
	local hasText = (m_text ~= "")
	MenuSetMinimizedThis(not hasText)

	-- Resize Menu Based On Text
	m_textHeight = Control_GetHeight(gHandles["stcMouseToolTip"])
	Control_SetSize(gHandles["stcMouseToolTip"], 100000, m_textHeight) -- test height based on large width
	m_textWidth = Static_GetStringWidth( gHandles["stcMouseToolTip"], m_text )
	Control_SetSize(gHandles["stcMouseToolTip"], m_textWidth + 6, m_textHeight)
	MenuSetSizeThis(m_textWidth + 6, m_textHeight)

	-- Position Menu
	PositionMenu()
end

function PositionMenu()
	
	-- Don't Position If No Text
	local hasText = (m_text ~= "")
	if not hasText then return end

	-- To Left, Right, or Centered
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local mouseX = Cursor_GetLocationX()
	if mouseX <= (m_textWidth / 2) then
		xPos = mouseX + 5
	elseif mouseX > screenWidth - (m_textWidth / 2) then
		xPos = mouseX - m_textWidth - 5
	else
		xPos = mouseX - (m_textWidth / 2)
	end

	-- To Above or Below
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	local mouseY = Cursor_GetLocationY()
	if mouseY >= screenHeight - m_textHeight then
		yPos = mouseY - screenHeight - 5
	else
		yPos = mouseY + 20
	end

	-- Position Menu
	MenuSetLocationThis(xPos, yPos)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	MenuSetPassthroughThis(true) -- Just in case it intersects, allow the mouse to pass through
	SetText("")
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function mouseToolTipHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	SetText(KEP_EventDecodeString(event))
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	PositionMenu()
end
