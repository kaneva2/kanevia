--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_GameItemNameConflict.lua
-- 
-- Warns users of an impending name conflict on save from
-- Framework_GameItemEditor
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- When the menu is created
function onCreate()
	-- Register client - client event handlers
	KEP_EventRegisterHandler("nameConflictMessageHandler", "FRAMEWORK_NAME_CONFLICT_MESSAGE", KEP.HIGH_PRIO)
end

-- Handles name conflict message from Framework_GameItemEditor
function nameConflictMessageHandler(dispatcher, fromNetid, event, eventID, filter, objectID)
	local error = KEP_EventDecodeString(event)
	Static_SetText(gHandles["stcError"], tostring(error))
	Control_SetVisible(gHandles["stcError"], true)
end
