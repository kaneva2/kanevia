--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Default mounting side
gDefaultSide = "R"	-- default to "Right"
gOppositeSide = "L"

-- Mount slots exclusion groups

gMountSlotIDs =
{
	--	KANEVA_FOOTWEAR = { ID = 2 },
	--	KANEVA_BRACELET	= { ID = 4 },
	--	KANEVA_EARRINGS	= { ID = 5 },
	--	KANEVA_NECKLACE	= { ID = 6 },

	KANEVA_MISC		= { ID = 49 },		-- reserved 'misc' slot for internal use

	HEAD_ONLY		= { ID = 11 },
	HEADWEAR		= { ID = 19 },
	GLASSES			= { ID = 20 },
	HAND_HOLDING	= { R = 24, L = 25 },
	FULLFACE		= { ID = 26 },
	HAIR			= { ID = 27 },
	HEAD_W_HAIR		= { ID = 28 },

	PEVIS			= { ID = 50 },
	UPPERARM		= { R = 51, L = 52 },
	FOREARM			= { R = 53, L = 54 },
	HAND			= { R = 55, L = 56 },
	THUMB			= { R = 57, L = 58 },
	THUMB_TOP		= { R = 59, L = 60 },
	FINGER_IDX		= { R = 61, L = 62 },
	FINGER_IDX_TOP	= { R = 63, L = 64 },
	FINGER_MDL		= { R = 65, L = 66 },
	FINGER_MDL_TOP	= { R = 67, L = 68 },
	FINGER_RNG		= { R = 69, L = 70 },
	FINGER_RNG_TOP	= { R = 71, L = 72 },
	FINGER_LTL		= { R = 73, L = 74 },
	FINGER_LTL_TOP	= { R = 75, L = 76 },
	WRIST			= { R = 77, L = 78 },
	SHOULDER		= { R = 79, L = 80 },
	CHEST			= { ID = 81 },
	BACK_UPPER		= { ID = 82 },
	BACK_LOWER		= { ID = 83 },
	WAIST			= { ID = 84 },
	HIP				= { R = 85, L = 86 },
	THIGH			= { R = 87, L = 88 },
	LEG				= { R = 89, L = 90 },
	FOOTWEAR		= { R = 91, L = 92 },
	ORIGIN			= { ID = 93 },
}

function PrepareMountSlotIDs()
	for slotName, slid in pairs(gMountSlotIDs) do
		if slid.ID==nil then
			if slid.R~=nil and slid.L~=nil then
				if gDefaultSide=="R" then
					slid.ID = slid.R
					slid.ID2 = slid.L
				else
					slid.ID = slid.L
					slid.ID2 = slid.R
				end
			else
				slid.ID = 0
			end
		end
	end
end

PrepareMountSlotIDs()

-- Mount Slots
--------------------------------
--       Slot options         --
--------------------------------
-- 1. Name		- Slot name
-- 2. Bone		- Name of the attaching bone
-- 3. Offset	- Offset (x, y, z) between attached location and bone, default: (0, 0, 0)
-- 4. Scale		- Bone scaling (sx, sy, sz) (used to flip axis), default: (1, 1, 1)
-- 5. Camera	- special camera used for previewing, default: use the standard actor camera
--   5.1 Camera.LookAt			- Offset (x, y, z) between look-at-point and the look-at-bone, default: (0, 0, 0)
--   5.2 Camera.Distance		- Initial distance between camera and look-at-point, default: 0
-- 6. Mirror	- settings for mirror slot, default: no mirror slot available.
----------------------------------

gMountSlots = {
	{	Name = "Headwear", Bone = "Bip01 Head", Exclusion = gMountSlotIDs.HEADWEAR.ID,
		Offset = { 0.8, 0, 0 },
		Camera = {
			LookAt = { 0, 5.9, 0 },
			Distance = 4,
			MinDistance = 3,
		},
	},
	{	Name = "Glasses", Bone = "Bip01 Head", Exclusion = gMountSlotIDs.GLASSES.ID,
		Offset = { 0.4, 0, 0 },
		Camera = {
			LookAt = { 0, 5.5, 0 },
			Distance = 4,
			MinDistance = 3,
		},
	},
	{	Name = "Full Face", Bone = "Bip01 Head", Exclusion = gMountSlotIDs.FULLFACE.ID,
		Offset = { 0, 0, 0 },
		Camera = {
			LookAt = { 0, 5.4, 0 },
			Distance = 4,
			MinDistance = 3,
		},
	},
	{	Name = "Custom Hair", Bone = "Bip01 Head", Exclusion = gMountSlotIDs.HAIR.ID,
		Offset = { 0, 0, 0 },
		Camera = {
			LookAt = { 0, 5.4, 0 },
			Distance = 4,
			MinDistance = 3,
		},
	},
	{	Name = "Custom Head w/Hair", Bone = "Bip01 Head", Exclusion = gMountSlotIDs.HEAD_W_HAIR.ID,
		Offset = { 0, 0, 0 },
		Camera = {
			LookAt = { 0, 5.4, 0 },
			Distance = 4,
			MinDistance = 3,
		},
	},
	{	Name = "Custom Head Only", Bone = "Bip01 Head", Exclusion = gMountSlotIDs.HEAD_ONLY.ID,
		Offset = { 0, 0, 0 },
		Camera = {
			LookAt = { 0, 5.4, 0 },
			Distance = 4,
			MinDistance = 3,
		},
	},
	{	Name = "Shoulder",
		Bone = "Bip01 " .. gDefaultSide .. " Clavicle", Exclusion = gMountSlotIDs.SHOULDER.ID,
		Offset = { 0.45, -0.1, 0 },
		Camera = {
			LookAt = { -0.4, 5.0, 0 },
			Distance = 4,
			MinDistance = 3,
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Clavicle", Exclusion = gMountSlotIDs.SHOULDER.ID2,
			Offset = { 0.45, -0.1, 0 },
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 0.4, 5.0, 0 },
				Distance = 4,
				MinDistance = 3,
			},
		},
	},
	{ 	Name = "Upper Arm",
		Bone = "Bip01 " .. gDefaultSide .. " UpperArm", Exclusion = gMountSlotIDs.UPPERARM.ID,
		Camera = {
			LookAt = { -0.9, 4.9, 0 },
			Distance = 4,
			MinDistance = 3,
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " UpperArm", Exclusion = gMountSlotIDs.UPPERARM.ID2,
			Scale = { 0.9, -1, 1 },
			Camera = {
				LookAt = { 1, 4.9, 0 },
				Distance = 4,
				MinDistance = 3,
			},
		},
	},
	{	Name = "Forearm",
		Bone = "Bip01 " .. gDefaultSide .. " Forearm", Exclusion = gMountSlotIDs.FOREARM.ID,
		Camera = {
			LookAt = { -1.5, 4.9, 0 },
			Distance = 4,
			MinDistance = 3,
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Forearm", Exclusion = gMountSlotIDs.FOREARM.ID2,
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 1.5, 4.9, 0 },
				Distance = 4,
				MinDistance = 3,
			},
		},
	},
	{	Name = "Wrist",
		Bone = "Bip01 " .. gDefaultSide .. " Forearm", Exclusion = gMountSlotIDs.WRIST.ID,
		Offset = { 0.77, 0, 0 },
		Camera = {
			LookAt = { -2.0, 4.9, 0 },
			Distance = 3,
			MinDistance = 2,
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Forearm", Exclusion = gMountSlotIDs.WRIST.ID2,
			Offset = { 0.77, 0, 0 },
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.0, 4.9, 0 },
				Distance = 3,
				MinDistance = 2,
			},
		},
	},
	{	Name = "Hand",
		Bone = "Bip01 " .. gDefaultSide .. " Hand", Exclusion = gMountSlotIDs.HAND.ID,
		Camera = {
			LookAt = { -2.5, 4.9, 0 },
			Distance = 3,
			MinDistance = 2,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Hand", Exclusion = gMountSlotIDs.HAND.ID2,
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.9, 0 },
				Distance = 3,
				MinDistance = 2,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Hand (Holding)",
		Bone = "Bip01 " .. gDefaultSide .. " Hand", Exclusion = gMountSlotIDs.HAND_HOLDING.ID,
		Offset = { 0.2, 0, 0.1 },
		Camera = {
			LookAt = { -2.5, 4.65, 0 },
			Distance = 3,
			MinDistance = 2,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Hand", Exclusion = gMountSlotIDs.HAND_HOLDING.ID2,
			Offset = { 0.2, 0, 0 },
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.65, 0 },
				Distance = 3,
				MinDistance = 2,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Thumb (Base)",
		Bone = "Bip01 " .. gDefaultSide .. " Finger0", Exclusion = gMountSlotIDs.THUMB.ID,
		Offset = { 0.18, -0.02, 0 },
		Camera = {
			LookAt = { -2.5, 4.9, 0 },
			Distance = 3,
			MinDistance = 2,
			NearPlaneDist = 0.1,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Finger0", Exclusion = gMountSlotIDs.THUMB.ID2,
			Offset = { 0.18, -0.02, 0 },
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.9, 0 },
				Distance = 3,
				MinDistance = 2,
				NearPlaneDist = 0.1,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Thumb (Top)",
		Bone = "Bip01 " .. gDefaultSide .. " Finger01", Exclusion = gMountSlotIDs.THUMB_TOP.ID,
		Camera = {
			LookAt = { -2.5, 4.9, 0 },
			Distance = 3,
			MinDistance = 2,
			NearPlaneDist = 0.1,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Finger01", Exclusion = gMountSlotIDs.THUMB_TOP.ID2,
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.9, 0 },
				Distance = 3,
				MinDistance = 2,
				NearPlaneDist = 0.1,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Index Finger (Base)",
		Bone = "Bip01 " .. gDefaultSide .. " Finger1", Exclusion = gMountSlotIDs.FINGER_IDX.ID,
		Camera = {
			LookAt = { -2.5, 4.9, 0 },
			Distance = 3,
			MinDistance = 2,
			NearPlaneDist = 0.1,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Finger1", Exclusion = gMountSlotIDs.FINGER_IDX.ID2,
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.9, 0 },
				Distance = 3,
				MinDistance = 2,
				NearPlaneDist = 0.1,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Index Finger (Top)",
		Bone = "Bip01 " .. gDefaultSide .. " Finger11", Exclusion = gMountSlotIDs.FINGER_IDX_TOP.ID,
		Camera = {
			LookAt = { -2.5, 4.9, 0 },
			Distance = 3,
			MinDistance = 2,
			NearPlaneDist = 0.1,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Finger11", Exclusion = gMountSlotIDs.FINGER_IDX_TOP.ID2,
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.9, 0 },
				Distance = 3,
				MinDistance = 2,
				NearPlaneDist = 0.1,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Middle Finger (Base)",
		Bone = "Bip01 " .. gDefaultSide .. " Finger2", Exclusion = gMountSlotIDs.FINGER_MDL.ID,
		Camera = {
			LookAt = { -2.5, 4.9, 0 },
			Distance = 3,
			MinDistance = 2,
			NearPlaneDist = 0.1,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Finger2", Exclusion = gMountSlotIDs.FINGER_MDL.ID2,
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.9, 0 },
				Distance = 3,
				MinDistance = 2,
				NearPlaneDist = 0.1,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Middle Finger (Top)",
		Bone = "Bip01 " .. gDefaultSide .. " Finger21", Exclusion = gMountSlotIDs.FINGER_MDL_TOP.ID,
		Camera = {
			LookAt = { -2.5, 4.9, 0 },
			Distance = 3,
			MinDistance = 2,
			NearPlaneDist = 0.1,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Finger21", Exclusion = gMountSlotIDs.FINGER_MDL_TOP.ID2,
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.9, 0 },
				Distance = 3,
				MinDistance = 2,
				NearPlaneDist = 0.1,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Ring Finger (Base)",
		Bone = "Bip01 " .. gDefaultSide .. " Finger3", Exclusion = gMountSlotIDs.FINGER_RNG.ID,
		Offset = { 0, 0.04, 0 },		-- the offset caused by bone sharing between ring/small fingers
		Camera = {
			LookAt = { -2.5, 4.9, 0 },
			Distance = 3,
			MinDistance = 2,
			NearPlaneDist = 0.1,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Finger3", Exclusion = gMountSlotIDs.FINGER_RNG.ID2,
			Offset = { 0, 0.04, 0 },	-- the offset caused by bone sharing between ring/small fingers
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.9, 0 },
				Distance = 3,
				MinDistance = 2,
				NearPlaneDist = 0.1,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Ring Finger (Top)",
		Bone = "Bip01 " .. gDefaultSide .. " Finger31", Exclusion = gMountSlotIDs.FINGER_RNG_TOP.ID,
		Offset = { 0, 0.04, 0 },		-- the offset caused by bone sharing between ring/small fingers
		Camera = {
			LookAt = { -2.5, 4.9, 0 },
			Distance = 3,
			MinDistance = 2,
			NearPlaneDist = 0.1,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Finger31", Exclusion = gMountSlotIDs.FINGER_RNG_TOP.ID2,
			Offset = { 0, 0.04, 0 },	-- the offset caused by bone sharing between ring/small fingers
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.9, 0 },
				Distance = 3,
				MinDistance = 2,
				NearPlaneDist = 0.1,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Little Finger (Base)",
		Bone = "Bip01 " .. gDefaultSide .. " Finger3", Exclusion = gMountSlotIDs.FINGER_LTL.ID,
		Offset = { 0, -0.04, 0 },		-- the offset caused by bone sharing between ring/small fingers
		Camera = {
			LookAt = { -2.5, 4.85, 0 },
			Distance = 3,
			MinDistance = 2,
			NearPlaneDist = 0.1,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Finger3", Exclusion = gMountSlotIDs.FINGER_LTL.ID2,
			Offset = { 0, -0.04, 0 },	-- the offset caused by bone sharing between ring/small fingers
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.85, 0 },
				Distance = 3,
				MinDistance = 2,
				NearPlaneDist = 0.1,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Little Finger (Top)",
		Bone = "Bip01 " .. gDefaultSide .. " Finger31", Exclusion = gMountSlotIDs.FINGER_LTL_TOP.ID,
		Offset = { 0, -0.04, 0 },		-- the offset caused by bone sharing between ring/small fingers
		Camera = {
			LookAt = { -2.5, 4.85, 0 },
			Distance = 3,
			MinDistance = 2,
			NearPlaneDist = 0.1,
			AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Finger31", Exclusion = gMountSlotIDs.FINGER_LTL_TOP.ID2,
			Offset = { 0, -0.04, 0 },	-- the offset caused by bone sharing between ring/small fingers
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 2.5, 4.85, 0 },
				Distance = 3,
				MinDistance = 2,
				NearPlaneDist = 0.1,
				AdaptiveZoom = { ActivateDist = 1.5, ZoomScaling = 0.3 },
			},
		},
	},
	{	Name = "Chest", Bone = "Bip01 Spine2", Exclusion = gMountSlotIDs.CHEST.ID,
		Offset = { 0.5, 0, 0.4 },
		Camera = {
			LookAt = { 0, 4.75, 0 },
			Distance = 5,
			MinDistance = 4,
		},
	},
	{	Name = "Upper Back", Bone = "Bip01 Spine2", Exclusion = gMountSlotIDs.BACK_UPPER.ID,
		Offset = { 0.6, 0, -0.4 },
		Camera = {
			LookAt = { 0, 5.0, 0 },
			Distance = 5,
			MinDistance = 4,
		},
	},
	{	Name = "Lower Back", Bone = "Bip01 Spine1", Exclusion = gMountSlotIDs.BACK_LOWER.ID,
		Offset = { 0.2, 0, -0.4 },
		Camera = {
			LookAt = { 0, 4.25, 0 },
			Distance = 5,
			MinDistance = 4,
		},
	},
	{	Name = "Waist", Bone = "Bip01 Spine", Exclusion = gMountSlotIDs.WAIST.ID,
		Offset = { 0, 0, 0 },
		Camera = {
			LookAt = { 0, 3.8, 0 },
			Distance = 5,
			MinDistance = 4,
		},
	},
	{	Name = "Pelvis", Bone = "Bip01 Pelvis", Exclusion = gMountSlotIDs.PEVIS.ID,
		Camera = {
			LookAt = { 0, 3.2, 0 },
			Distance = 5,
			MinDistance = 4,
		},
	},
	{	Name = "Hip", Bone = "Bip01 Pelvis", Exclusion = gMountSlotIDs.HIP.ID,
		Offset = { 0, 0.25, -0.45 },
		Camera = {
			LookAt = { 0, 3.2, 0 },
			Distance = 5,
			MinDistance = 4,
		},
		Mirror = {
			Bone = "Bip01 Pelvis", Exclusion = gMountSlotIDs.HIP.ID2,
			Offset = { 0, 0.25, -0.45 },	-- the offset caused by bone sharing between ring/small fingers
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 0, 3.2, 0 },
				Distance = 5,
				MinDistance = 4,
			},
		},
	},
	{	Name = "Thigh",
		Bone = "Bip01 " .. gDefaultSide .. " Thigh", Exclusion = gMountSlotIDs.THIGH.ID,
		Offset = { 0.5, 0, 0 },		-- the offset caused by bone sharing between ring/small fingers
		Camera = {
			LookAt = { -0.5, 2.55, 0 },
			Distance = 5,
			MinDistance = 4,
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Thigh", Exclusion = gMountSlotIDs.THIGH.ID2,
			Offset = { 0.5, 0, 0 },	-- the offset caused by bone sharing between ring/small fingers
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 0.5, 2.55, 0 },
				Distance = 5,
				MinDistance = 4,
			},
		},
	},
	{	Name = "Leg",
		Bone = "Bip01 " .. gDefaultSide .. " Calf", Exclusion = gMountSlotIDs.LEG.ID,
		Offset = { 0.6, 0, 0 },		-- the offset caused by bone sharing between ring/small fingers
		Camera = {
			LookAt = { -0.5, 1.1, 0 },
			Distance = 5,
			MinDistance = 4,
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Calf", Exclusion = gMountSlotIDs.LEG.ID2,
			Offset = { 0.6, 0, 0 },	-- the offset caused by bone sharing between ring/small fingers
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 0.5, 1.1, 0 },
				Distance = 5,
				MinDistance = 4,
			},
		},
	},
	{	Name = "Footwear",
		Bone = "Bip01 " .. gDefaultSide .. " Foot", Exclusion = gMountSlotIDs.FOOTWEAR.ID,
		Offset = { 0, 0, 0 },		-- the offset caused by bone sharing between ring/small fingers
		Camera = {
			LookAt = { -0.5, 0.5, 0 },
			Distance = 5,
			MinDistance = 4,
		},
		Mirror = {
			Bone = "Bip01 " .. gOppositeSide .. " Foot", Exclusion = gMountSlotIDs.FOOTWEAR.ID2,
			Offset = { 0, 0, 0 },	-- the offset caused by bone sharing between ring/small fingers
			Scale = { 1, -1, 1 },
			Camera = {
				LookAt = { 0.5, 0.5, 0 },
				Distance = 5,
				MinDistance = 4,
			},
		},
	},
	{	Name = "Origin", Bone = "", Exclusion = gMountSlotIDs.ORIGIN.ID,
		Offset = { 0, 0, 0 },
		Camera = {
			LookAt = { 0, 3.2, 0 },
			Distance = 7,
			MinDistance = 4,
		},
	},
}
