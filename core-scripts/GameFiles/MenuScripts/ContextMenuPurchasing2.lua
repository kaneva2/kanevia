--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

PLAYER_USERID_EVENT = "PlayerUserIdEvent"

ArmedItem = {}
ArmedItem.__index = ArmedItem

function ArmedItem.create(glid, name, price, creator, invType, raveCount, playerRaveCount, thumbnail_medium_path)
	local armedItem = {}             -- our new object
	setmetatable(armedItem,ArmedItem)
	armedItem.glid = tonumber(glid or 0)
	armedItem.name = name
	armedItem.price = tonumber(price or 0)
	armedItem.creator = creator
	armedItem.inventoryType = invType
	armedItem.imagePath = thumbnail_medium_path
	armedItem.raves = tonumber(raveCount or 0)
	armedItem.playerRaves = tonumber(playerRaveCount or 0)
	return armedItem
end

g_currentPage = 0
g_totalPages = 0
items_per_page = 2

g_currentItem1Index = 0

g_currentItem2Index = 0

armedItems = {}

ITEM_INFO_EVENT = "ItemInfoEvent"

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "PlayerUserIdEventHandler", PLAYER_USERID_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( ITEM_INFO_EVENT, KEP.HIGH_PRIO )
end

function PlayerUserIdEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_userID = KEP_EventDecodeNumber( event )
	g_userName = KEP_EventDecodeString( event )
	requestPlayerArmedItems()
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.GET_PLAYERS_ARMED_ITEMS then
		armedData = KEP_EventDecodeString( event )
		parseArmedItems(armedData)
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function requestPlayerArmedItems()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..PLAYERS_ARMED_ITEM_LIST_SUFFIX.."&username="..g_userName, WF.GET_PLAYERS_ARMED_ITEMS)
end

function parseArmedItems(armedData)
	local s = 0
	local numRec = 0
	local strPos = 0
	local result = 0

	armedItems = {}
	s, e, result = string.find(armedData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		s, e, g_totalNumRec = string.find(armedData, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
		s, e, numRec = string.find(armedData, "<NumberRecords>([0-9]+)</NumberRecords>")
		numRec = tonumber(numRec)

		g_maxPages = (numRec / items_per_page)

		if numRec == 0 then
			KEP_MessageBox("This player has no available items for purchase.")
			MenuCloseThis()
		end

		for i=1,numRec do
			s, strPos, itemInfo = string.find(armedData, "<Armed_x0020_Items>(.-)</Armed_x0020_Items>", strPos)

			s, trash, glid = string.find(itemInfo, "<global_id>(.-)</global_id>", 1)
			s, trash, name = string.find(itemInfo, "<name>(.-)</name>", 1)
			s, trash, price = string.find(itemInfo, "<market_cost>(.-)</market_cost>", 1)
			s, trash, creator = string.find(itemInfo, "<creator_name>(.-)</creator_name>", 1)
			s, trash, invType = string.find(itemInfo, "<inventory_type>(.-)</inventory_type>", 1)
			s, trash, thumbnail_medium_path = string.find(itemInfo, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>", 1)
			s, trash, raveCount = string.find(itemInfo, "<number_of_raves>(.-)</number_of_raves>", 1)
			s, trash, playerRaveCount = string.find(itemInfo, "<player_rave_count>(.-)</player_rave_count>", 1)

			local armedItem = ArmedItem.create(glid, name, price, creator, invType, raveCount, playerRaveCount, thumbnail_medium_path)
			table.insert(armedItems, armedItem)
		end
		g_totalPages = #armedItems / items_per_page
		populateItems(1)
	else
		KEP_MessageBox("This player has no available items for purchase.")
		MenuCloseThis()
	end
end

function enableControl(ctrl, enable)
	Control_SetEnabled(ctrl, enable)
	Control_SetVisible(ctrl, enable)
end

function populateItems(page_number)
	local index = items_per_page * (page_number - 1) + 1
	local item1 = armedItems[index]
	local enableItem1 = true
	if item1 == nil then
		enableItem1 = false
	end
	enableControl(gHandles["lblHeader"], enableItem1)
	enableControl(gHandles["lblUserName"], enableItem1)
	enableControl(gHandles["lblRaveCount"], enableItem1)
	enableControl(gHandles["lblPrice"], enableItem1)
	enableControl(gHandles["imgRave"], enableItem1)
	enableControl(gHandles["imgPrice"], enableItem1)
	enableControl(gHandles["imgFrame"], enableItem1)
	enableControl(gHandles["imgObjectPic"], enableItem1)
	enableControl(gHandles["imgBackground"], enableItem1)
	enableControl(gHandles["btnWebView"], enableItem1)
	enableControl(gHandles["btnBuy"], enableItem1)
	enableControl(gHandles["icon_btnWebView"], enableItem1)
	enableControl(gHandles["icon_btnBuy"], enableItem1)
	enableControl(gHandles["icon_btnRave"], enableItem1)

	if enableItem1 == true then
		Static_SetText(gHandles["lblHeader"], item1.name)
		Static_SetText(gHandles["lblUserName"], item1.creator)
		Static_SetText(gHandles["lblRaveCount"], tostring(item1.raves))
		Static_SetText(gHandles["lblPrice"], tostring(item1.price))
		local element = Image_GetDisplayElement(gHandles["imgObjectPic"])
		KEP_LoadIconTextureByID(item1.glid, element, gDialogHandle)
		g_currentItem1Index = index

		if ( item1.playerRaves == 0 ) then
			enableControl(gHandles["btnRave"], true)
		else
			enableControl(gHandles["btnRave"], false)
		end
	end

	local item2 = armedItems[index + 1]
	local enableItem2 = true
	if item2 == nil then
		enableItem2 = false
	end

	enableControl(gHandles["lblHeader2"], enableItem2)
	enableControl(gHandles["lblUserName2"], enableItem2)
	enableControl(gHandles["lblRaveCount2"], enableItem2)
	enableControl(gHandles["lblPrice2"], enableItem2)
	enableControl(gHandles["imgRave2"], enableItem2)
	enableControl(gHandles["imgPrice2"], enableItem2)
	enableControl(gHandles["imgFrame2"], enableItem2)
	enableControl(gHandles["imgObjectPic2"], enableItem2)
	enableControl(gHandles["imgBackground2"], enableItem2)
	enableControl(gHandles["btnWebView2"], enableItem2)
	enableControl(gHandles["btnBuy2"], enableItem2)
	enableControl(gHandles["icon_btnWebView2"], enableItem2)
	enableControl(gHandles["icon_btnBuy2"], enableItem2)
	enableControl(gHandles["icon_btnRave2"], enableItem2)

	if enableItem2 == true then
		Static_SetText(gHandles["lblHeader2"], item2.name)
		Static_SetText(gHandles["lblUserName2"], item2.creator)
		Static_SetText(gHandles["lblRaveCount2"], tostring(item2.raves))
		Static_SetText(gHandles["lblPrice2"], tostring(item2.price))
		local element = Image_GetDisplayElement(gHandles["imgObjectPic2"])
		KEP_LoadIconTextureByID(item2.glid, element, gDialogHandle)
		g_currentItem2Index = index + 1

		if ( item2.playerRaves == 0 ) then
			enableControl(gHandles["btnRave2"], true)
		else
			enableControl(gHandles["btnRave2"], false)
		end
	else
		enableControl(gHandles["btnRave2"], false)
	end

	g_currentPage = page_number

	--manageButtons
	managePageButtons()
end

function managePageButtons()
	enableControl(gHandles["btnBackBG"], false)
	enableControl(gHandles["btnBackArrow"], false)
	enableControl(gHandles["btnBackLabel"], false)
	enableControl(gHandles["btnBack"], false)
	enableControl(gHandles["btnNextBG"], false)
	enableControl(gHandles["btnNextArrow"], false)
	enableControl(gHandles["btnNextLabel"], false)
	enableControl(gHandles["btnNext"], false)

	if g_currentPage > 1 then
		enableControl(gHandles["btnBackBG"], true)
		enableControl(gHandles["btnBackArrow"], true)
		enableControl(gHandles["btnBackLabel"], true)
		enableControl(gHandles["btnBack"], true)
	end

	if g_currentPage < g_totalPages then
		enableControl(gHandles["btnNextBG"], true)
		enableControl(gHandles["btnNextArrow"], true)
		enableControl(gHandles["btnNextLabel"], true)
		enableControl(gHandles["btnNext"], true)
	end
end

function btnNext_OnButtonClicked(buttonHandle)
	populateItems(g_currentPage + 1)
end

function btnBack_OnButtonClicked(buttonHandle)
	populateItems(g_currentPage - 1)
end

function btnWebView_OnButtonClicked(buttonHandle)
	local item = nil
	if (g_currentItem1Index ~= 0) then
		item = armedItems[g_currentItem1Index]
		openWebView( item )
	else
		return
	end
end

function btnWebView2_OnButtonClicked(buttonHandle)
	local item = nil
	if (g_currentItem2Index ~= 0) then
		item = armedItems[g_currentItem2Index]
		openWebView( item )
	else
		return
	end
end

function btnBuy_OnButtonClicked(buttonHandle)
	local item = nil
	if (g_currentItem1Index ~= 0) then
		item = armedItems[g_currentItem1Index]
		buyItem( item )
	else
		return
	end
end

function btnBuy2_OnButtonClicked(buttonHandle)
	local item = nil
	if (g_currentItem2Index ~= 0) then
		item = armedItems[g_currentItem2Index]
		buyItem( item )
	else
		return
	end
end

function btnRave_OnButtonClicked(buttonHandle)
	local item = nil
	if (g_currentItem1Index ~= 0) then
		item = armedItems[g_currentItem1Index]
		if ( item.playerRaves == 0 ) then
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..SET_ITEM_RAVES_SUFFIX.."&globalId="..item.glid, WF.RAVE, 1)
		end
	else
		return
	end
end

function btnRave2_OnButtonClicked(buttonHandle)
	local item = nil
	if (g_currentItem2Index ~= 0) then
		item = armedItems[g_currentItem2Index]
		if ( item.playerRaves == 0 ) then
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..SET_ITEM_RAVES_SUFFIX.."&globalId="..item.glid, WF.RAVE, 1)
		end
	else
		return
	end
end

function buyItem( item )
	MenuOpen("Checkout.xml")
	local ev = KEP_EventCreate( ITEM_INFO_EVENT )
	KEP_EventEncodeNumber( ev, item.inventoryType ) 
	KEP_EventEncodeNumber( ev, 1 ) -- num items
	KEP_EventEncodeNumber( ev, item.glid )
	KEP_EventEncodeNumber( ev, 1 ) --quantity of item
	KEP_EventEncodeString( ev, item.name ) -- name
	KEP_EventEncodeNumber( ev, item.price ) -- price
	KEP_EventQueue( ev )
end

function openWebView( item )
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_SHOP .. IMAGE_DETAILS_SUFFIX .. item.glid )
end
