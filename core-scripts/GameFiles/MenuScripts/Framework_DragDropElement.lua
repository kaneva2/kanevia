--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_DragDropElement.lua
--
-- Written by Paul, he likes when I write header comments on his behalf so 
-- here ya go Paul ;D - Signed: Rob Spessard
-- This script handles the image of the dragged item, hurr durr durr
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")

local ITEM_LEVEL_COORDS = {
							{left = 0, right = 15, top = 146, bottom = 161},
							{left = 16, right = 31, top = 146, bottom = 161},
							{left = 32, right = 47, top = 146, bottom = 161},
							{left = 0, right = 15, top = 162, bottom = 177},
							{left = 16, right = 31, top = 162, bottom = 177}
}

local DEFAULT_MAX_DURABILITY = 1000 -- 1,000 hits or attacks per weapon/armor
local DURABILITY_BY_LEVEL = { [1]=10, [2]=100, [3]=250, [4]=500, [5]=1000 }
local DURABILITY_BAR_WIDTH = 81
-- Left, top, right, bottom
local DURABILITY_COLORS = {GREEN  = {19, 179, 20, 185},
						   YELLOW = {31, 179, 32, 185},
						   RED 	  = {25, 179, 26, 185}
						  }

local ITEM_LEVEL_RARITY_OFFSET = {["Common"] = 0, ["Rare"] = 48, ["Very Rare"] = 96, ["Extremely Rare"] = 144, ["Never"] = 144}

local m_dragItem = nil

local getDurabilityColor -- (durabilityPercentage) Gets the durability color by percentage
local formatNumberSuffix -- (count)

function onCreate()
	KEP_EventRegisterHandler("createDragDropElement", "DRAG_DROP_CREATE_ELEMENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("setDragDropElementState", "DRAG_DROP_SET_ELEMENT_STATE", KEP.HIGH_PRIO)
end

function createDragDropElement(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local item = {}
	item = decompileInventoryItem(tEvent)

	if item.properties and item.properties.GLID and item.UNID and item.properties.name and item.count then
		applyImage(gHandles["imgItem"], item.properties.GLID)
		
		m_dragItem = item

		local singleItem = tablePopulated(m_dragItem.instanceData) or (item.properties.stackSize and item.properties.stackSize <= 1)
		if gHandles["imgItemBG"] then Control_SetEnabled(gHandles["imgItemBG"], true) end
		if gHandles["imgItemName"] then Control_SetEnabled(gHandles["imgItemName"], true) end
		if gHandles["stcItem"] then Static_SetText( gHandles["stcItem"], item.properties.name ) end
		if gHandles["stcItemCount"] then Static_SetText( gHandles["stcItemCount"], formatNumberSuffix(item.count))	end
		
		if gHandles["imgItem"] then Control_SetEnabled(gHandles["imgItem"], true) end
		if gHandles["imgItemCountBG"] then 
			Control_SetEnabled(gHandles["imgItemCountBG"], not singleItem) 
			Control_SetSize(gHandles["imgItemCountBG"], Control_GetWidth(gHandles["imgItemCountBG"]) + 5, Control_GetHeight(gHandles["imgItemCountBG"]))
			Control_SetLocationX(gHandles["imgItemCountBG"], Control_GetLocationX(gHandles["imgItemCountBG"]) - 6 )
		end
		if gHandles["stcItem"] then
			Control_SetEnabled(gHandles["stcItem"], true)
			Control_SetVisible(gHandles["stcItem"], true)
		end

		if gHandles["stcItemCount"] then
			Control_SetEnabled(gHandles["stcItemCount"], not singleItem)
			Control_SetVisible(gHandles["stcItemCount"], not singleItem)
			Control_SetLocationX(gHandles["stcItemCount"], Control_GetLocationX(gHandles["stcItemCount"]) - 3 )
		end
		
		if gHandles["imgValid"] then Control_SetEnabled(gHandles["imgValid"], true) end
		
		if item.properties.itemType == ITEM_TYPES.WEAPON or item.properties.itemType == ITEM_TYPES.ARMOR or item.properties.itemType == ITEM_TYPES.TOOL or item.properties.behavior == "pet" then
			Control_SetVisible(gHandles["imgItemLevel"], true)
			local element = Image_GetDisplayElement(gHandles["imgItemLevel"])
			local itemLevel = item.properties.level
			local rarityOffset = ITEM_LEVEL_RARITY_OFFSET[item.properties.rarity] or 0
			Element_SetCoords(element, ITEM_LEVEL_COORDS[itemLevel].left + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].top, ITEM_LEVEL_COORDS[itemLevel].right + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].bottom)
		else
			Control_SetVisible(gHandles["imgItemLevel"], false)
		end

		if tablePopulated(item.instanceData) and item.instanceData.durability then		
			-- Set durability bar
			local durability = item.instanceData.durability	
			local durabilityType = item.properties.durabilityType or "repairable"
			local durabilityPercentage = 0
			local itemLevel = item.properties.level

			if paintWeapon or durabilityType == "limited" then
				durabilityPercentage = math.min((durability/DURABILITY_BY_LEVEL[itemLevel]), 1)
			elseif durabilityType == "indestructible" then
				durabilityPercentage = 1
			else
				durabilityPercentage = math.min((durability/DEFAULT_MAX_DURABILITY), 1)
			end
			
			-- Set bar size
			Control_SetSize(gHandles["imgDurabilityBar"], durabilityPercentage * DURABILITY_BAR_WIDTH, 7)
			
			-- Set bar color
			local durColor = getDurabilityColor(durabilityPercentage)
			Element_SetCoords(Image_GetDisplayElement(gHandles["imgDurabilityBar"]), unpack(durColor))
			
			-- Set bar's visible
			Control_SetVisible(gHandles["imgDurabilityBar"], true)
			Control_SetVisible(gHandles["imgDurabilityBG"], true)
			Control_SetVisible(gHandles["imgDurabilityGradiant"], true)
			Control_SetVisible(gHandles["imgItemOverlay"], durability <= 0)
		else		
			Control_SetVisible(gHandles["imgDurabilityBar"], false)
			Control_SetVisible(gHandles["imgDurabilityBG"], false)
			Control_SetVisible(gHandles["imgDurabilityGradiant"], false)
			Control_SetVisible(gHandles["imgItemOverlay"], false)
		end
		
		MenuBringToFrontThis()
	end
	
	-- Pass Mouse Events Through Menu During Drag
	MenuSetPassthroughThis(true)
end

function setDragDropElementState(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local elementState = KEP_EventDecodeNumber(tEvent)
	
	if elementState then
		if gHandles["imgInvalid"] then Control_SetEnabled(gHandles["imgInvalid"], (elementState == 0)) end
		if gHandles["imgValid"] then Control_SetEnabled(gHandles["imgValid"], (elementState == 1)) end
		if gHandles["imgDrop"] then Control_SetEnabled(gHandles["imgDrop"], (elementState == 2)) end
	end
end

-- Returns the color for the durability bar based on percentage passed in
getDurabilityColor = function(durabilityPercentage)
	-- Green bar
	if durabilityPercentage >= .5 then
		return DURABILITY_COLORS.GREEN
	-- Return yellow
	elseif durabilityPercentage >= .15 then
		return DURABILITY_COLORS.YELLOW
	-- Return red
	else
		return DURABILITY_COLORS.RED
	end
end

function formatNumberSuffix(count)
	-- If no need for formatting, then return
	if count < 10000 then return count end

	local strCount = tostring(count)
	local newCount = ""

	local format = { 	max = 0,
						min = 0,
					 	suffix = ""
					}

	-- Thousands
	if #strCount <= 6 then
		format.max = 6
		format.min = 3
		format.suffix = "K"
	-- Millions
	elseif #strCount <= 9 then
		format.max = 9
		format.min = 6
		format.suffix = "M"
	-- Billions
	elseif #strCount <= 12 then
		format.max = 12
		format.min = 9
		format.suffix = "B"
	-- Trillions
	elseif #strCount <= 15 then
		format.max = 15
		format.min = 12
		format.suffix = "T"
	end

	for i = 1, #strCount do
		local diff = #strCount - i
		if #strCount <= format.max then
			if diff >= format.min then
				newCount = newCount..string.sub(strCount,i,i)
			else
				return newCount..format.suffix
			end
		end
	end
end