--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\MenuFunctions.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\Framework_InventoryHelper.lua")

local m_selectedPathWaypointPID = nil

local INTERACTION_RANGE_SQRD = 625

function onCreate()
	KEP_EventRegister( "EditPathWaypointEvent", KEP.HIGH_PRIO )
	
	KEP_EventRegisterHandler( "EditPathWaypointEventHandler", "EditPathWaypointEvent", KEP.HIGH_PRIO )

	MenuCenterThis()
end

function Dialog_OnRender(dialogHandle, elapsedSec)
	if m_selectedPathWaypointPID == nil then
		return
	end

	local posX, posY, posZ = KEP_GetPlayerPosition()
	local wpX, wpY, wpZ = KEP_DynamicObjectGetPositionAnim(m_selectedPathWaypointPID)
	local xDiff = posX - wpX
	local yDiff = posY - wpY
	local zDiff = posZ - wpZ
	local xyzDistanceSqrd = xDiff * xDiff + zDiff * zDiff + yDiff * yDiff
	if (INTERACTION_RANGE_SQRD <= xyzDistanceSqrd) then
		KEP_ClearSelection()
		MenuCloseThis()
	end
end

function EditPathWaypointEventHandler ( dispatcher, fromNetId, event, eventId, filter, objectId )
	m_selectedPathWaypointPID = KEP_EventDecodeNumber(event)
	Static_SetText( gHandles["stcWaypointName"], "Waypoint " .. tostring(KEP_EventDecodeNumber(event)) )
	Static_SetText( gHandles["stcParentPathName"], KEP_EventDecodeString(event) )
	pauseTime = KEP_EventDecodeNumber(event)
	EditBox_SetText(gHandles["edPauseTime"], tostring(pauseTime), true)
	ComboBox_SetSelectedByText(gHandles["cmbRotationType"], KEP_EventDecodeString(event))
	CheckBox_SetChecked(gHandles["boxEaseIn"], KEP_EventDecodeNumber(event))
	CheckBox_SetChecked(gHandles["boxEaseOut"], KEP_EventDecodeNumber(event))
	CheckBox_SetChecked(gHandles["boxUseTrigger"], KEP_EventDecodeNumber(event))
	triggerSize = KEP_EventDecodeNumber(event)
	EditBox_SetText(gHandles["edTriggerSize"], tostring(triggerSize), true)
end

function edPauseTime_OnEditBoxChange(editBoxHandle, editBoxText)
	if tonumber(editBoxText) then
		return
	elseif editBoxText == "" then
		return
	else
		EditBox_SetText(editBoxHandle, "", true)
		return ""
	end
end
function edTriggerSize_OnEditBoxChange(editBoxHandle, editBoxText)
	if tonumber(editBoxText) then
		return
	elseif editBoxText == "" then
		return
	else
		EditBox_SetText(editBoxHandle, "", true)
		return ""
	end
end

function saveCanceled()
	KEP_ClearSelection()
	MenuCloseThis()
end
function btnClose_OnButtonClicked( buttonHandle )
	saveCanceled()
end
function btnCancel_OnButtonClicked( buttonHandle )
	saveCanceled()
end

function btnSave_OnButtonClicked( buttonHandle )
	props = {pauseTime = tonumber(EditBox_GetText(gHandles["edPauseTime"])) or 0,
			 rotationType = getComboBoxSelectedItem("cmbRotationType"),
			 easeIn = CheckBox_GetChecked(gHandles["boxEaseIn"]),
			 easeOut = CheckBox_GetChecked(gHandles["boxEaseOut"]),
			 collisionTrigger = CheckBox_GetChecked(gHandles["boxUseTrigger"]),
			 triggerSize = tonumber(EditBox_GetText(gHandles["edTriggerSize"])) or 0}
	Events.sendEvent("UpdatePathWaypointPropertiesEvent", {pathWaypointPID = m_selectedPathWaypointPID, pathWaypointProperties = props})
	KEP_ClearSelection()
	MenuCloseThis()
end