--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- HUDHelper.lua 
-- Common functions utilized for HUD menus
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\PopupMenuHelper.lua")

g_MenuStruct = {} -- table for the PopupMenuHelper

g_initialWidth, g_initialHeight = 0, 0 -- Initial width/height of the menu for rollover resizing
g_lButtonDownInside = false -- hold whether the left button is down inside for preventing rollover while dragging

g_rolloverOpen = false -- Whether or not the rollover state is on

local m_overridingFade = false

function rolloverOpened()
	--Overwrite this in your own code if needed
end

function rolloverClosed()
	--Overwrite this in your own menu if needed
end

--Initlializes the rollover state, call from OnCreate
function initRollover(menuContent)
	-- Only need this event for menus with rollover state
	KEP_EventRegisterHandler( "clientResizeEventHandler",	"ClientResizeEvent",	KEP.HIGH_PRIO )

	-- connected to Dialog, not button so buttonHandler is nil, last variable is true for fade
	-- 2 numbers are speed of background fade, and speed of buttons to fade out (faster to avoid flashing button appearance)
	g_MenuStruct= PopupMenuHelper_InitPopupMenu(nil, menuContent, nil, nil, nil, true, .25, .2)
	g_initialWidth = Dialog_GetWidth(gDialogHandle)
	g_initialHeight = Dialog_GetHeight(gDialogHandle)

	-- Do this once for setup without actually calling the popup to avoid unncessary fade
	PopupMenuEvent_PopupUpdated(false)
end 

-- Close all rollovers when resizing/minimizing to avoid relocation issues while menu sizes are in large state
function clientResizeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	closeRollover(true)
end

-- Default, must override with HUD_OnRender in own function if overriding this
function Dialog_OnRender(dialogHandle, fTimeElapsed)
	HUD_OnRender(fTimeElapsed)
end

-- Handle anything that needs to happen in on render in here
function HUD_OnRender(fTimeElapsed)
	PopupMenuHelper_OnRender(fTimeElapsed)
end

-- Function to assist in opening the rollover, call from OnMouseMoved
function rolloverMenu(x, y, additionalRolloverControl)
	PopupMenuHelper_OnMouseMove(gDialogHandle, x, y, true)

	if (x < 0 or y < 0 or x > MenuGetSizeThis().width or y > MenuGetSizeThis().height) then
		g_lButtonDownInside = false
	end

	-- Don't open the rollover when the left mousebutton is down unless the mouse was put down inside the dialog
	if ( Dialog_IsRMouseDown(gDialogHandle) ~= 0 ) or
		( Dialog_IsLMouseDown(gDialogHandle) ~= 0 and not g_lButtonDownInside ) then 
		closeRollover()
		return
	end
	
	-- If within the dialog or either of the rollover controls while visible open it, otherwise close
	if ((x >= 0 and x <= MenuGetSizeThis().width) and (y >= 0 and y <= MenuGetSizeThis().height))
	or (rolloverControl2 ~= nil and Control_ContainsPoint(additionalRolloverControl, x, y) ~= 0 and Control_GetVisible(additionalRolloverControl) == 1) then 
		openRollover()
	else 
		closeRollover()
	end 
end

-- Open the rollover state of the dialog
function openRollover()
	-- Don't do this multiple times unnecessarily
	if g_rolloverOpen then
		return
	end

	MenuSetPassthroughThis(false)

	PopupMenuHelper_ShowPopupMenu(g_MenuStruct, true)

	MenuAlwaysBringToFrontThis()

	g_rolloverOpen = true

	rolloverOpened() -- Individual menus can override if they want to add more functionality when opening
end

-- Close the rollover state of the dialog
-- overrideFade: Don't allow user to reenable the menu by mousing back over the background
function closeRollover(overrideFade)
	-- Don't do this multiple times unnecessarily
	if g_rolloverOpen == false then
		return
	end

	-- Set to original size now if overriding so that it doesn't get retriggered
	if overrideFade then
		MenuSetSizeThis(g_initialWidth, g_initialHeight)
		m_overridingFade = true
	end

	PopupMenuHelper_ShowPopupMenu(g_MenuStruct, false)
	
	g_rolloverOpen = false
	
	-- To PopupMenuEvent_PopupUpdated when fade finished
end

-- If overriding be sure to call HUD_PopupUpdated
function PopupMenuEvent_PopupUpdated(state)
	HUD_PopupUpdated(state)
end

function HUD_PopupUpdated(state)
	if state == false then -- closing
		MenuSetPassthroughThis(true)
		MenuSetSizeThis(g_initialWidth, g_initialHeight)
		MenuSendToBackThis()
		MenuClearFocusThis()

		rolloverClosed() -- Individual menus can override if they want to add more functionality when opening
	end

	m_overridingFade = false
end

-- If you want to overwrite this, be sure to call the HUD_FadeUpdated at the top
function PopupMenuEvent_FadeUpdated(alphaLevel, alphaLevelFast)
	HUD_FadeUpdated(alphaLevel, alphaLevelFast)
end

function HUD_FadeUpdated(alphaLevel, alphaLevelFast)
	-- Don't update the size if we're overriding the fade to close it
	if m_overridingFade then
		return
	end

	-- Set the size at 50% to avoid accidental mouseovers
	if alphaLevel < MAX_ALPHA/2 and g_initialWidth > 0 and MenuGetSizeThis().width > g_initialWidth then
		MenuSetSizeThis(g_initialWidth, g_initialHeight)
	elseif alphaLevel >= MAX_ALPHA/2 and g_initialWidth > 0 and MenuGetSizeThis().width <= g_initialWidth then
		MenuSetSizeThis(getLargeSize(g_MenuStruct.Handles[1]))
	end
end

function getLargeSize(control)
	-- Offset by location of the control so that it doesn't go off the edge of the screen
	local width = Control_GetWidth(control) + Control_GetLocationX(control)
	local height = Control_GetHeight(control) + Control_GetLocationY(control)
	return width, height
end

-- Opens menu if closed, closes if opened. Returns true if opening
function toggleMenu(menuName)
	if MenuIsOpen(menuName) then
		MenuClose(menuName)
		return false
	else
		MenuOpen(menuName)
		return true
	end
end

-- Next 3 functions used for preventing opening while moving the camera
function Dialog_OnLButtonDownInside( dialogHandle )
	g_lButtonDownInside = true
end

function Dialog_OnRButtonDown( dialogHandle )
	closeRollover(true)
end

function Dialog_OnLButtonUp( dialogHandle )
	g_lButtonDownInside = false
end
