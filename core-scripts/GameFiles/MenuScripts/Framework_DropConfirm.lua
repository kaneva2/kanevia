--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_DragDropHelper.lua")
dofile("Easing.lua")

local m_droppedItem = {}
local ITEM_LEVEL_COORDS = {
							{left = 0, right = 15, top = 146, bottom = 161},
							{left = 16, right = 31, top = 146, bottom = 161},
							{left = 32, right = 47, top = 146, bottom = 161},
							{left = 0, right = 15, top = 162, bottom = 177},
							{left = 16, right = 31, top = 162, bottom = 177}
}

local m_vendorOpen = false
local m_canSell = false
local m_playerName = nil
local ITEM_LEVEL_RARITY_OFFSET = {["Common"] = 0, ["Rare"] = 48, ["Very Rare"] = 96, ["Extremely Rare"] = 144, ["Never"] = 144}

local BUTTON_DROP_VENDOR_X = 174
local BUTTON_DROP_X = 124
local BUTTON_CANCEL_VENDOR_X = 279
local BUTTON_CANCEL_X = 229

function onCreate()
	KEP_EventRegisterHandler("updateVendorItems", "FRAMEWORK_RETURN_VENDOR_ITEMS", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onConfirmDropHandler", "DRAG_DROP_CONFIRM_DROP", KEP.HIGH_PRIO)

	if MenuIsOpen("Framework_Vendor") then
		KEP_EventCreateAndQueue("FRAMEWORK_REQUEST_VENDOR_ITEMS")
		m_vendorOpen = true
	end

	m_playerName = KEP_GetLoginName()
end

function btnDrop_OnButtonClicked(buttonHandle)
	local position = {}
	local rotation = 0
	position.x, position.y, position.z, rotation = KEP_GetPlayerPosition()
	rotation = (rotation * math.pi) / 180
	position.rx = math.sin(rotation)
	position.ry = 0
	position.rz = math.cos(rotation)
	position.x = position.x - (3 * position.rx)
	position.z = position.z - (3 * position.rz)

	local items = {}
	table.insert(items, {UNID = m_droppedItem.UNID, count = m_droppedItem.count, instanceData = m_droppedItem.instanceData})
	local spawnInput = {items = items, despawnTime = 300}
	Events.sendEvent("OBJECT_SPAWN", {spawnerPID = 0, spawnType = "loot", GLID = 3234144, location = position, spawnInput = spawnInput})
	MenuCloseThis()
end

function btnCancelDrop_OnButtonClicked(buttonHandle)
	updateItem(m_index, m_pid, m_droppedItem, true)
	updateDirty()
	MenuCloseThis()
end

function btnSell_OnButtonClicked(buttonHandle)
	local sellItemEvent = KEP_EventCreate("DRAG_DROP_CONFIRM_SELL")
	sellItemEvent = compileInventoryItem(m_droppedItem, sellItemEvent)
	KEP_EventEncodeNumber(sellItemEvent, m_pid)
	KEP_EventEncodeNumber(sellItemEvent, m_index)
	KEP_EventSetFilter(sellItemEvent, 4)
	KEP_EventQueue(sellItemEvent)
	MenuCloseThis()
end

function chkHideMenu_OnCheckBoxChanged( checkboxHandle, checked )
	--Save the new config
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber( config, "HideDropMenu"..tostring(m_playerName), checked )
		KEP_ConfigSave( config )
	end
end

function onConfirmDropHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_droppedItem = {}
	m_droppedItem = decompileInventoryItem(tEvent)
	m_pid = KEP_EventDecodeNumber(tEvent)
	m_index = KEP_EventDecodeNumber(tEvent)

	if m_droppedItem.properties and m_droppedItem.properties.GLID and m_droppedItem.UNID and m_droppedItem.properties.name and m_droppedItem.count then
		applyImage(gHandles["imgItem"], m_droppedItem.properties.GLID)
		
		--m_droppedItem = item

		local singleItem = m_droppedItem.count == 1
		if gHandles["imgItemBG"] then Control_SetEnabled(gHandles["imgItemBG"], true) end
		if gHandles["imgItemName"] then Control_SetEnabled(gHandles["imgItemName"], true) end
		if gHandles["stcItem"] then Static_SetText( gHandles["stcItem"], m_droppedItem.properties.name ) end
		if gHandles["stcItemCount"] then Static_SetText( gHandles["stcItemCount"], formatNumberSuffix(m_droppedItem.count))	end
		
		if gHandles["imgItem"] then Control_SetEnabled(gHandles["imgItem"], true) end
		if gHandles["imgItemCountBG"] then 
			Control_SetEnabled(gHandles["imgItemCountBG"], not singleItem) 
			--Control_SetSize(gHandles["imgItemCountBG"], Control_GetWidth(gHandles["imgItemCountBG"]) + 5, Control_GetHeight(gHandles["imgItemCountBG"]))
			--Control_SetLocationX(gHandles["imgItemCountBG"], Control_GetLocationX(gHandles["imgItemCountBG"]) - 6 )
		end
		if gHandles["stcItem"] then
			Control_SetEnabled(gHandles["stcItem"], true)
			Control_SetVisible(gHandles["stcItem"], true)
		end

		if gHandles["stcItemCount"] then
			Control_SetEnabled(gHandles["stcItemCount"], not singleItem)
			Control_SetVisible(gHandles["stcItemCount"], not singleItem)
			--Control_SetLocationX(gHandles["stcItemCount"], Control_GetLocationX(gHandles["stcItemCount"]) - 3 )
		end
			
		if m_droppedItem.properties.itemType == ITEM_TYPES.WEAPON or m_droppedItem.properties.itemType == ITEM_TYPES.ARMOR or m_droppedItem.properties.itemType == ITEM_TYPES.TOOL or m_droppedItem.properties.behavior == "pet" then
			Control_SetVisible(gHandles["imgItemLevel"], true)
			local element = Image_GetDisplayElement(gHandles["imgItemLevel"])
			local itemLevel = m_droppedItem.properties.level
			local rarityOffset = ITEM_LEVEL_RARITY_OFFSET[m_droppedItem.properties.rarity] or 0
			Element_SetCoords(element, ITEM_LEVEL_COORDS[itemLevel].left + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].top, ITEM_LEVEL_COORDS[itemLevel].right + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].bottom)
		else
			Control_SetVisible(gHandles["imgItemLevel"], false)
		end

		if m_droppedItem.properties.itemType == ITEM_TYPES.PLACEABLE then
			Control_SetVisible(gHandles["stcTip"], true)
		end

		MenuBringToFrontThis()
		FormatMenu()
	end
end

-- Called when a new container inventory is received.  Updates all items and re-displays.
function updateVendorItems(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	--log("--- updateVendorItems")

	local g_trades = decompileInventory(tEvent)

	-- go through the items in the trades to see what can get sold 
	for i = 1,#g_trades do

		-- the item UNID is the key to the cache 
		local itemUNID = tostring(g_trades[i].output.UNID)

		if tostring(g_trades[i].output.UNID) == tostring(m_droppedItem.UNID) then 
			m_canSell = true
		end
	end
	FormatMenu()
end

function FormatMenu()
	Control_SetVisible(gHandles["btnSell"], m_canSell)
	Control_SetEnabled(gHandles["btnSell"], m_canSell)
	Control_SetLocationX(gHandles["btnDrop"], BUTTON_DROP_X)
	Control_SetLocationX(gHandles["btnCancelDrop"], BUTTON_CANCEL_X)
	local countText = ""
	local nameText  =  m_droppedItem.properties.name
	if m_droppedItem.count and m_droppedItem.count > 1 then
		countText =  "("..tostring(m_droppedItem.count)..")"
	end
	if m_canSell == true then
		Static_SetText(gHandles["stcMessage"], "Do you want to sell <b> "..countText..nameText.." </b> or drop it on the ground?")
		Control_SetLocationX(gHandles["btnDrop"], BUTTON_DROP_VENDOR_X)
	Control_SetLocationX(gHandles["btnCancelDrop"], BUTTON_CANCEL_VENDOR_X)
	elseif m_vendorOpen then
		Static_SetText(gHandles["stcMessage"], "Vendor will not buy <b> "..countText..nameText..". </b> Drop it on the ground?")
	else
		Static_SetText(gHandles["stcMessage"], "Do you want to drop <b> "..countText..nameText.." </b> on the ground?")
	end
end

function formatNumberSuffix(count)
	-- If no need for formatting, then return
	if count < 10000 then return count end

	local strCount = tostring(count)
	local newCount = ""

	local format = { 	max = 0,
						min = 0,
					 	suffix = ""
					}

	-- Thousands
	if #strCount <= 6 then
		format.max = 6
		format.min = 3
		format.suffix = "K"
	-- Millions
	elseif #strCount <= 9 then
		format.max = 9
		format.min = 6
		format.suffix = "M"
	-- Billions
	elseif #strCount <= 12 then
		format.max = 12
		format.min = 9
		format.suffix = "B"
	-- Trillions
	elseif #strCount <= 15 then
		format.max = 15
		format.min = 12
		format.suffix = "T"
	end

	for i = 1, #strCount do
		local diff = #strCount - i
		if #strCount <= format.max then
			if diff >= format.min then
				newCount = newCount..string.sub(strCount,i,i)
			else
				return newCount..format.suffix
			end
		end
	end
end