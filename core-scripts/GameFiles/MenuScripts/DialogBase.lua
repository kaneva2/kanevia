--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

DialogBase = {}
DialogBase.OuterFrame = {}
DialogBase.Content = {}

function setup( dialogHandle )
	DialogBase.hdl = dialogHandle

	DialogBase.OuterFrame.TopLeft = {}
	DialogBase.OuterFrame.TopLeft.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameTL" )
	DialogBase.OuterFrame.TopLeft.x = -2
	DialogBase.OuterFrame.TopLeft.y = -2
	DialogBase.OuterFrame.TopLeft.w = 5
	DialogBase.OuterFrame.TopLeft.h = 5

	DialogBase.OuterFrame.TopRight = {}
	DialogBase.OuterFrame.TopRight.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameTR" )
	DialogBase.OuterFrame.TopRight.x = nil
	DialogBase.OuterFrame.TopRight.y = -2
	DialogBase.OuterFrame.TopRight.w = 5
	DialogBase.OuterFrame.TopRight.h = 5

	DialogBase.OuterFrame.BottomLeft = {}
	DialogBase.OuterFrame.BottomLeft.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameBL" )
	DialogBase.OuterFrame.BottomLeft.x = -2
	DialogBase.OuterFrame.BottomLeft.y = nil
	DialogBase.OuterFrame.BottomLeft.w = 5
	DialogBase.OuterFrame.BottomLeft.h = 5

	DialogBase.OuterFrame.BottomRight = {}
	DialogBase.OuterFrame.BottomRight.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameBR" )
	DialogBase.OuterFrame.BottomRight.x = nil
	DialogBase.OuterFrame.BottomRight.y = nil
	DialogBase.OuterFrame.BottomRight.w = 5
	DialogBase.OuterFrame.BottomRight.h = 5

	DialogBase.OuterFrame.Top = {}
	DialogBase.OuterFrame.Top.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameTop" )
	DialogBase.OuterFrame.Top.x = 3
	DialogBase.OuterFrame.Top.y = -2
	DialogBase.OuterFrame.Top.w = nil
	DialogBase.OuterFrame.Top.h = 4

	DialogBase.OuterFrame.Left = {}
	DialogBase.OuterFrame.Left.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameLeft" )
	DialogBase.OuterFrame.Left.x = -2
	DialogBase.OuterFrame.Left.y = 3
	DialogBase.OuterFrame.Left.w = 4
	DialogBase.OuterFrame.Left.h = nil

	DialogBase.OuterFrame.Bottom = {}
	DialogBase.OuterFrame.Bottom.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameBottom" )
	DialogBase.OuterFrame.Bottom.x = 3
	DialogBase.OuterFrame.Bottom.y = nil
	DialogBase.OuterFrame.Bottom.w = nil
	DialogBase.OuterFrame.Bottom.h = 4

	DialogBase.OuterFrame.Right = {}
	DialogBase.OuterFrame.Right.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameRight" )
	DialogBase.OuterFrame.Right.x = nil
	DialogBase.OuterFrame.Right.y = 3
	DialogBase.OuterFrame.Right.w = 4
	DialogBase.OuterFrame.Right.h = nil

	DialogBase.OuterFrame.Header = {}
	DialogBase.OuterFrame.Header.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameHeader" )
	DialogBase.OuterFrame.Header.x = 2
	DialogBase.OuterFrame.Header.y = 2
	DialogBase.OuterFrame.Header.w = nil
	DialogBase.OuterFrame.Header.h = 40

	DialogBase.OuterFrame.Body = {}
	DialogBase.OuterFrame.Body.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameBody" )
	DialogBase.OuterFrame.Body.x = 2
	DialogBase.OuterFrame.Body.y = 2
	DialogBase.OuterFrame.Body.w = nil
	DialogBase.OuterFrame.Body.h = nil

	DialogBase.OuterFrame.Footer = {}
	DialogBase.OuterFrame.Footer.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameFooter" )
	DialogBase.OuterFrame.Footer.x = 2
	DialogBase.OuterFrame.Footer.y = nil
	DialogBase.OuterFrame.Footer.w = nil
	DialogBase.OuterFrame.Footer.h = 66

	DialogBase.OuterFrame.WhiteBar = {}
	DialogBase.OuterFrame.WhiteBar.hdl = Dialog_GetControl( DialogBase.hdl, "outerFrameWhiteBar" )
	DialogBase.OuterFrame.WhiteBar.x = 2
	DialogBase.OuterFrame.WhiteBar.y = nil
	DialogBase.OuterFrame.WhiteBar.w = nil
	DialogBase.OuterFrame.WhiteBar.h = 28

	DialogBase.Content.Left = {}
	DialogBase.Content.Left.hdl = Dialog_GetControl( DialogBase.hdl, "contentLeft" )
	DialogBase.Content.Left.x = 11
	DialogBase.Content.Left.y = 42
	DialogBase.Content.Left.w = 8
	DialogBase.Content.Left.h = nil

	DialogBase.Content.Mid = {}
	DialogBase.Content.Mid.hdl = Dialog_GetControl( DialogBase.hdl, "contentMid" )
	DialogBase.Content.Mid.x = 19
	DialogBase.Content.Mid.y = 42
	DialogBase.Content.Mid.w = nil
	DialogBase.Content.Mid.h = nil

	DialogBase.Content.Right = {}
	DialogBase.Content.Right.hdl = Dialog_GetControl( DialogBase.hdl, "contentRight" )
	DialogBase.Content.Right.x = nil
	DialogBase.Content.Right.y = 42
	DialogBase.Content.Right.w = 8
	DialogBase.Content.Right.h = nil
end

function DialogBase.resize( wid, hgt, fromCenter )
	if fromCenter == true then
		DialogBase.centerX = Dialog_GetLocationX( DialogBase.hdl ) + ( Dialog_GetWidth( DialogBase.hdl ) / 2 )
		DialogBase.centerY = Dialog_GetLocationY( DialogBase.hdl ) + ( Dialog_GetHeight( DialogBase.hdl ) / 2 )
	end
	DialogBase.w = wid
	DialogBase.h = hgt

	DialogBase.OuterFrame.resize( DialogBase.OuterFrame )
	DialogBase.Content.resize( DialogBase.Content )

	Dialog_SetSize( DialogBase.hdl, DialogBase.w, DialogBase.h )
	if fromCenter == true then
		Dialog_SetLocation( DialogBase.hdl, DialogBase.centerX - ( DialogBase.w / 2 ), DialogBase.centerY - ( DialogBase.h / 2 ) )
	end
end

function DialogBase.OuterFrame.resize( self )
	self.TopRight.x = DialogBase.w + 2
	self.BottomLeft.y = DialogBase.h + 2
	self.BottomRight.x = DialogBase.w + 2
	self.BottomRight.y = DialogBase.h + 2
	self.Top.w = DialogBase.w
	self.Left.h = DialogBase.h
	self.Bottom.y = DialogBase.h + 3
	self.Bottom.w = DialogBase.w
	self.Right.x = DialogBase.w + 3
	self.Right.h = DialogBase.h
	self.Header.w = DialogBase.w + 2
	self.Body.w = DialogBase.w + 2
	self.Body.h = DialogBase.h - 80
	self.Footer.y = DialogBase.h - 90
	self.Footer.w = DialogBase.w + 2
	self.WhiteBar.y = DialogBase.h - 25
	self.WhiteBar.w = DialogBase.w + 2
	for k, v in pairs( self ) do
		if v ~= DialogBase.OuterFrame.resize then	--This table (self) contains elements and functions. Only process the elements.
			Control_SetLocation( v.hdl, v.x, v.y )
			Control_SetSize( v.hdl, v.w, v.h )
		end
	end
end

function DialogBase.Content.resize( self )
	self.Left.h = DialogBase.h - 145
	self.Mid.w = DialogBase.w - 34
	self.Mid.h = DialogBase.h - 145
	self.Right.x = DialogBase.w - 15
	self.Right.h = DialogBase.h - 145
	for k, v in pairs( self ) do
		if v ~= DialogBase.Content.resize then	--This table (self) contains elements and functions. Only process the elements.
			Control_SetLocation( v.hdl, v.x, v.y )
			Control_SetSize( v.hdl, v.w, v.h )
		end
	end
end

function moveResizeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
    local ignoreX = KEP_EventDecodeNumber( event )
	local ignoreY = KEP_EventDecodeNumber( event )
	local wid = KEP_EventDecodeNumber( event )
	local hgt = KEP_EventDecodeNumber( event )
	local center = KEP_EventDecodeNumber( event )
	DialogBase.resize( wid, hgt, center ~= 0 )
	return KEP.EPR_OK
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "moveResizeEventHandler", "MoveResizeEvent", KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	setup( dialogHandle )
end

function Dialog_OnDestroy( dialogHandle )
	Helper_Dialog_OnDestroy( dialogHandle )
end
