--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

MY_PLACES_EVENT = "MyPlacesEvent"
QUERY_PLACES_EVENT = "QueryPlacesEvent"
ZONE_NAVIGATION_EVENT = "ZoneNavigationEvent"
ADD_FAVORITE_EVENT = "AddFavoriteEvent"

history = {}
favorites = {}
searched_favorites = {}

TAB_FAVORITES = 1
TAB_HISTORY = 2

g_currentTab = TAB_FAVORITES
g_currentPage = 1
g_totalPages = 1
g_selected = -1
g_username = ""

BLACK = { r = 0, g = 0, b = 0, a = 255 }
WHITE = { r = 255, g = 255, b = 255, a = 255 }
PLACES_PER_PAGE = 12
TOTAL_ROWS = 3
TOTAL_ITEMS_PER_ROW = 4

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "zoneNavigationEventHandler", ZONE_NAVIGATION_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "myPlacesEventHandler", MY_PLACES_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( ADD_FAVORITE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( MY_PLACES_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( QUERY_PLACES_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	enableFavorites(false)
	enableHistory(false)

	-- setup look of listbox scrollbars
	local lbh = Dialog_GetListBox(dialogHandle, "lbHistory")
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then
			local eh = ScrollBar_GetTrackDisplayElement(sbh)

			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end

	g_username = KEP_GetLoginName()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function myPlacesEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local tab = KEP_EventDecodeNumber(event)
	changeTab(tab)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.MY_PLACES_GET_PICS then
		local ret = KEP_EventDecodeString(event)
		parsePlaces(ret)
		displayFavorites(1)
	end
end

function textureDownloadHandler()
	if g_currentTab == TAB_FAVORITES then
		displayFavorites(g_currentPage)
	end
end

function zoneNavigationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ZONE_NAV.HISTORY_INFO then
		history = readHistoryIntoTable(event)
		displayHistory()
	elseif filter == ZONE_NAV.FAVORITES_INFO then
		favorites = readFavoritesIntoTable(event)
		getFavoritesData()
	end
end

function changeTab(tab)
	if tab == TAB_FAVORITES then
		g_currentTab = TAB_FAVORITES
		enableHistory(false)
		enableFavorites(true)
		requestFavorites()
	elseif tab == TAB_HISTORY then
		g_currentTab = TAB_HISTORY
		enableFavorites(false)
		enableHistory(true)
		requestHistory()
	end
end

function setControl(ctrl, enable)
	Control_SetEnabled(ctrl, enable)
	Control_SetVisible(ctrl, enable)
end

function displayHistory()
	ListBox_RemoveAllItems(gHandles["lbHistory"])
	for i = #history,1,-1 do
		local v = history[i]
		ListBox_AddItem(gHandles["lbHistory"], v.name, i)
	end
end

function displayFavorites(page_number)
	enableFavorites(true)
	local firstIndex = ((page_number - 1) * PLACES_PER_PAGE) + 1
	local lastIndex = firstIndex - 1
	for i=1,TOTAL_ROWS do
		for j=1,TOTAL_ITEMS_PER_ROW do
			local index = ((page_number - 1) * PLACES_PER_PAGE) + ((i-1)*TOTAL_ITEMS_PER_ROW + j)
			local data = searched_favorites[index]
			if data ~= nil then
				lastIndex = lastIndex + 1
				setControl(gHandles["imgRow" .. i .. "Item" .. j .."_Selection"], false)
				setControl(gHandles["imgRow" .. i .. "Item" .. j .."_Frame"], true)
				setControl(gHandles["imgRow" .. i .. "Item" .. j .."_Pic"], true)
				setControl(gHandles["btnRow" .. i .. "Item" .. j .."_Button"], true)
				setControl(gHandles["stcRow" .. i .. "Item" .. j .."_Name"], true)
				Static_SetText(gHandles["stcRow" .. i .. "Item" .. j .."_Name"], data.name)
				scaleImage(gHandles["imgRow" .. i .. "Item" .. j .."_Pic"], gHandles["imgRow" .. i .. "Item" .. j .."_Frame"], data.filename)
			end
		end
	end

	local lblpage = Dialog_GetControl(gDialogHandle, "lblCurrentPage")
	Static_SetText(lblpage, "Page " .. page_number)

	Static_SetText(gHandles["lblCurrentPageItems"], firstIndex .. "-" .. lastIndex .. " of " .. #searched_favorites)

	g_currentPage = page_number

	--manageButtons
	managePageButtons()
end

function enableHistory(enable)
	setControl(gHandles["lbHistory"], enable)
	setControl(gHandles["UpperLeft_Slice"], enable)
	setControl(gHandles["UpperCenter_Slice"], enable)
	setControl(gHandles["UpperRight_Slice"], enable)
	setControl(gHandles["MiddleLeft_Slice"], enable)
	setControl(gHandles["MiddleCenter_Slice"], enable)
	setControl(gHandles["MiddleRight_Slice"], enable)
	setControl(gHandles["LowerLeft_Slice"], enable)
	setControl(gHandles["LowerCenter_Slice"], enable)
	setControl(gHandles["LowerRight_Slice"], enable)
	setControl(gHandles["imgHistoryTab"], not enable)
	setControl(gHandles["btnHistoryTab"], not enable)
	local element = Static_GetDisplayElement(gHandles["stcHistoryTab"])
	local color = Element_GetFontColor(element)
	BlendColor_SetColor(color, 0, WHITE)
	if enable == true then
		BlendColor_SetColor(color, 0, BLACK)
	end
end

function enableFavorites(enable)
	setControl(gHandles["lblSendTo"], enable)
	setControl(gHandles["imgGradBG1"], enable)
	setControl(gHandles["editSendTo"], enable)
	setControl(gHandles["btnFind"], enable)
	setControl(gHandles["imgArrowsBG"], enable)
	setControl(gHandles["imgArrowsBGShadow"], enable)
	setControl(gHandles["lblCurrentPage"], enable)
	setControl(gHandles["lblCurrentPageItems"], enable)
	setControl(gHandles["imgHorizRuler_1"], enable)
	setControl(gHandles["imgHorizRuler_2"], enable)
	setControl(gHandles["imgMyPlacesTab"], not enable)
	setControl(gHandles["btnMyPlacesTab"], not enable)
	local element = Static_GetDisplayElement(gHandles["stcMyPlacesTab"])
	local color = Element_GetFontColor(element)
	BlendColor_SetColor(color, 0, WHITE)
	if enable == true then
		BlendColor_SetColor(color, 0, BLACK)
	end

	for i=1,3 do
		for j=1,4 do
			setControl(gHandles["imgRow" .. i .. "Item" .. j .."_Selection"], false)
			setControl(gHandles["imgRow" .. i .. "Item" .. j .."_Frame"], false)
			setControl(gHandles["imgRow" .. i .. "Item" .. j .."_Pic"], false)
			setControl(gHandles["btnRow" .. i .. "Item" .. j .."_Button"], false)
			setControl(gHandles["stcRow" .. i .. "Item" .. j .."_Name"], false)
			Element_AddTexture(Image_GetDisplayElement(gHandles["imgRow" .. i .. "Item" .. j .."_Pic"]), gDialogHandle, "loading.dds")
			Element_SetCoords(Image_GetDisplayElement(gHandles["imgRow" .. i .. "Item" .. j .."_Pic"]), 0, 0, -1, -1)		-- use entire image for texturing
		end
	end

	managePageButtons(enable)
end

function requestHistory()
	local ev = KEP_EventCreate( ZONE_NAVIGATION_EVENT)
	KEP_EventSetFilter(ev, ZONE_NAV.HISTORY_REQUEST)
	KEP_EventQueue( ev)
end

function requestFavorites()
	local ev = KEP_EventCreate( ZONE_NAVIGATION_EVENT)
	KEP_EventSetFilter(ev, ZONE_NAV.FAVORITES_REQUEST)
	KEP_EventQueue( ev)
end

function getFavoritesData()
	local names = ""
	for i,v in ipairs(favorites) do
		names = names .. getLocationName(v) .. ","
	end
	local url = GameGlobals.WEB_SITE_PREFIX .. GET_COMMUNITY_SUFFIX .. names
	makeWebCall( url, WF.MY_PLACES_GET_PICS)
end

function clearSelection()
	g_selected = -1
	for i=1,TOTAL_ROWS do
		for j=1,TOTAL_ITEMS_PER_ROW do
			setControl(gHandles["imgRow" .. i .. "Item" .. j .."_Selection"], false)
		end
	end
end

function setSelected(rowNumber, itemNumber)
	clearSelection()
	setControl(gHandles["imgRow" .. rowNumber .. "Item" .. itemNumber .."_Selection"], true)
	g_selected = (g_currentPage - 1) * PLACES_PER_PAGE + ((rowNumber - 1) * TOTAL_ITEMS_PER_ROW + itemNumber)
	local data = searched_favorites[g_selected]
	if data ~= nil then
		gotoURL(data.url)
	end
end

function managePageButtons(disable)
	local btnFirst = Dialog_GetControl(gDialogHandle, "btnFirstPage")
	local btnLast = Dialog_GetControl(gDialogHandle, "btnLastPage")
	local btnNext = Dialog_GetControl(gDialogHandle, "btnNextPage")
	local btnPrev = Dialog_GetControl(gDialogHandle, "btnPrevPage")

	setControl(btnFirst, false)
	setControl(btnLast, false)
	setControl(btnNext, false)
	setControl(btnPrev, false)

	if disable == true then
		return
	end

	if g_currentPage > 1 then
		setControl(btnFirst, true)
		setControl(btnPrev, true)
	end

	if g_currentPage < g_totalPages then
		setControl(btnNext, true)
		setControl(btnLast, true)
	end
end

function goToSelectedZone()
	local lstZones = Dialog_GetControl(gDialogHandle, "lbHistory")
	local data = ListBox_GetSelectedItemData(lstZones)
	if data == nil or data == 0 then return end

	local zoneURL = history[data]
	if zoneURL == nil then return end

	LogInfo("goToSelectedZone: url='"..zoneURL.url.."'")
	gotoURL(zoneURL.url)
end

function getLocationName(placeData)
	local name = parseNameFromURL(placeData.url)
	local gamename = parseGameNameFromURL(placeData.url)
	--this is hacky, basically test whether the gamename is wok
	if isURL3DApp(placeData.url) == true then
		name = gamename
	end
	return name
end

function getPlaceByName(list, name)
	for i,v in ipairs(list) do
		if v.name ~= nil then
			if v.name == name then
				return v
			end
		end
	end
	return nil
end

function parsePlaces(ret)
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	local placeData = {}

	s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		local s, e, numRecs = string.find(ret, "<TotalNumberRecords>(.-)</TotalNumberRecords>")
		g_totalRecs = tonumber(numRecs)

		s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
		if result == "0" then
			local s, e, numRecs = string.find(ret, "<NumberRecords>(.-)</NumberRecords>")
			numRecs = tonumber(numRecs)
			if numRecs > 0 then
				local start = 0
				local last = 0
				for i=1,numRecs do
					visit = nil
					temps, tempe, visit = string.find(ret, "<Communities>(.-)</Communities>", e)
					if visit ~= nil then
						s = temps
						e = tempe
						start, last, name = string.find(visit, "<name>(.-)</name>")
						start, last, commId = string.find(visit, "<community_id>(.-)</community_id>")
						start, last, dbPath = string.find(visit, "<thumbnail_small_path>(.-)</thumbnail_small_path>")
						if not dbPath then
							dbPath = DEFAULT_KANEVA_PLACE_ICON
						end
						local data = {}
						data.name = name
						data.id = tonumber(commId)
						data.thumbnail = dbPath
						table.insert(placeData, data)
					end
				end
			end
		end
	end

	for i,v in ipairs(favorites) do
		v.id = i
		v.thumbnail = DEFAULT_KANEVA_PLACE_ICON
		local placeName = getLocationName(v)
		for j,x in ipairs(placeData) do
			if placeName == x.name then
				v.thumbnail = x.thumbnail
				v.id = x.id
			end
		end
		if string.find(v.url, g_username ..".home") ~= nil then
			v.url = GetHomeLink()
			v.thumbnail = "filestore9/4674834/6550947/home_ad.jpg"
		elseif string.find(v.url, "Kaneva City.zone") ~= nil then
			v.thumbnail = "filestore2/2835121/images_la.jpg"
		elseif string.find(v.url, "Employment Agency.zone") ~= nil then
			v.thumbnail = "filestore9/4674834/6550948/empagency_ad.jpg"
		elseif string.find(v.url, "Ka-Ching! Lobby.zone") ~= nil then
			v.thumbnail = "filestore8/4429592/2541_kaching300x197_v1_la.jpg"
		elseif string.find(v.url, "Mall.zone") ~= nil then
			v.thumbnail = "filestore2/2835121/images_la.jpg"
		end

		local filename = tostring(split(GetFilenameFromPath(v.thumbnail), ".")[1])
		filename = v.id .. "_" .. filename .. CUSTOM_TEXTURE_THUMB
					
		KEP_DownloadTextureAsyncStr( filename, v.thumbnail )
		filename = "../../CustomTexture/".. filename

		v.filename = filename
	end
	searched_favorites = searchFavorites("")
end

function searchFavorites(searchString)

	searchString = string.lower(searchString)

	local validURLs = {}

	for i,v in ipairs(favorites) do
		if string.find(string.lower(v.url), searchString, 1, true) ~= nil or string.find(string.lower(v.name), searchString, 1, true) ~= nil then
			table.insert(validURLs, v)
		end
	end
	g_totalPages = math.ceil(#validURLs / PLACES_PER_PAGE)
	return validURLs
end

function sendQueryPlacesEvent(type, cat)
	ev = KEP_EventCreate( QUERY_PLACES_EVENT)
	KEP_EventEncodeNumber(ev, type)
	KEP_EventEncodeNumber(ev, cat)
	KEP_EventQueue( ev)
end

function btnFindPlaces_OnButtonClicked( buttonHandle )
	if Control_GetVisible(buttonHandle) then
		MenuOpen("BrowsePlaces.xml")
	end
	sendQueryPlacesEvent( PlaceCategoryIds.HANGOUTS, -1 )
end

function btnAddFavorite_OnButtonClicked( buttonHandle )
	if Control_GetVisible(buttonHandle) then
		local height = Dialog_GetScreenHeight(gDialogHandle)
		local addFav = Dialog_CreateAtCoordinates("AddFavorite.xml", 380, height - 180)
		Dialog_BringToFront(addFav)
		local ev= KEP_EventCreate( ADD_FAVORITE_EVENT)
		KEP_EventSetFilter(ev, 2)
		KEP_EventQueue( ev)
	end
end

function btnGo_OnButtonClicked( buttonHandle )
	if g_currentTab == TAB_FAVORITES then
		local data = searched_favorites[g_selected]
		if data ~= nil then
			gotoURL(data.url)
		end
	elseif g_currentTab == TAB_HISTORY then
		goToSelectedZone()
	end
end

function btnHistoryTab_OnButtonClicked( buttonHandle )
	changeTab(TAB_HISTORY)
end

function btnMyPlacesTab_OnButtonClicked( buttonHandle )
	changeTab(TAB_FAVORITES)
end

-- DRF - TODO - Why Does This Cause Cursor To Dissappear In New Zone?
--function lbHistory_OnListBoxItemDblClick(listboxHandle, selectedIndex, selectedText)
--	goToSelectedZone()
--end

function btnRow1Item1_Button_OnButtonClicked( buttonHandle )
	setSelected(1, 1)
end

function btnRow1Item2_Button_OnButtonClicked( buttonHandle )
	setSelected(1, 2)
end

function btnRow1Item3_Button_OnButtonClicked( buttonHandle )
	setSelected(1, 3)
end

function btnRow1Item4_Button_OnButtonClicked( buttonHandle )
	setSelected(1, 4)
end

function btnRow2Item1_Button_OnButtonClicked( buttonHandle )
	setSelected(2, 1)
end

function btnRow2Item2_Button_OnButtonClicked( buttonHandle )
	setSelected(2, 2)
end

function btnRow2Item3_Button_OnButtonClicked( buttonHandle )
	setSelected(2, 3)
end

function btnRow2Item4_Button_OnButtonClicked( buttonHandle )
	setSelected(2, 4)
end

function btnRow3Item1_Button_OnButtonClicked( buttonHandle )
	setSelected(3, 1)
end

function btnRow3Item2_Button_OnButtonClicked( buttonHandle )
	setSelected(3, 2)
end

function btnRow3Item3_Button_OnButtonClicked( buttonHandle )
	setSelected(3, 3)
end

function btnRow3Item4_Button_OnButtonClicked( buttonHandle )
	setSelected(3, 4)
end

function btnPrevPage_OnButtonClicked(buttonHandle)
	g_currentPage = g_currentPage - 1
	displayFavorites(g_currentPage)
end

function btnNextPage_OnButtonClicked(buttonHandle)
	g_currentPage = g_currentPage + 1
	displayFavorites(g_currentPage)
end

function btnFirstPage_OnButtonClicked(buttonHandle)
	g_currentPage = 1
	displayFavorites(g_currentPage)
end

function btnLastPage_OnButtonClicked(buttonHandle)
	g_currentPage = g_totalPages
	displayFavorites(g_currentPage)
end

function editSendTo_OnEditBoxChange(editBoxHandle)
	searched_favorites = searchFavorites(EditBox_GetText(editBoxHandle))
	displayFavorites(1)
end
