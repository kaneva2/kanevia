--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_LeaderboardCongrats.lua
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_GameItemEditor.lua")
dofile("Framework_InventoryHelper.lua")

---------------------
-- Local Variables --
---------------------

local m_username = ""
local m_zoneInstanceId
local m_zoneType

local OUTLINE_COLOR = "#FF646464"
local HIGHLIGHT_COLOR = "#FF77FFFF"
local TEXT_COLOR = "#FFFFFFFF"

local m_prizeData = {}
local WORLD_COIN_INDEX = 140
local m_rewardUNID = WORLD_COIN_INDEX -- Once generic, get this from web

local m_rewardItem = {}
local m_worldLeaderboardId = -1

local m_gameItems = {} -- added for the game item cache, keys are UNID as a string 

local REWARD_STATUS = {
	WAITING = 0,
	PRIZE_DATA_READY = 1,
	GAME_ITEMS_CACHED = 2,
	READY_TO_CLAIM = 3,
}
local m_rewardStatus = REWARD_STATUS.WAITING
local m_pendingRewards = 0

--------------------
-- Core Functions --
--------------------

-- When the menu is created
function onCreate()

	setVisibleAndEnabled(gHandles["btnGetPrize"], false) -- Wait until item data exists

	m_username = KEP_GetLoginName()
	m_zoneInstanceId = tostring(KEP_GetZoneInstanceId())
	m_zoneType       = tostring(KEP_GetZoneIndexType())

	_G["btnGetPrize_OnButtonClicked"] = function(btnHandle)
		attemptGetPrize()
	end

	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	
	requestInventory(GAME_PID)

	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=getAward&zoneInstanceId="..tostring(m_zoneInstanceId).."&zoneType="..tostring(m_zoneType)
	makeWebCall(address, WF.LB_REWARD_STATUS_GET)

	initializeVisibleElements()

end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- EMPTY
end

--------------------
-- Event Handlers --
--------------------

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)
	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
	
	-- Enable prize claim button if prize data is ready
	if m_rewardStatus == REWARD_STATUS.WAITING then
		m_rewardStatus = REWARD_STATUS.GAME_ITEMS_CACHED
	elseif m_rewardStatus == REWARD_STATUS.PRIZE_DATA_READY then
		m_rewardStatus = REWARD_STATUS.READY_TO_CLAIM
		initializeReward()
	end
end

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- If browser return for Location Data web call
	if filter == WF.FLAG_VISIT then
		local returnXML = KEP_EventDecodeString( tEvent )
		local returnCode = ParseXmlReturnCode(returnXML)
		local returnDescription = xmlGetString(returnXML, "ReturnDescription")

		-- Update local visitors list? Requires time tracking?

	elseif filter == WF.LB_REWARD_STATUS_GET then
		local returnXML = KEP_EventDecodeString( tEvent )
		local returnCode = ParseXmlReturnCode(returnXML)
		local returnDescription = xmlGetString(returnXML, "ReturnDescription")

		if returnCode == 0 then
			m_prizeData = {}
			m_pendingRewards = 0

			local filterString = "<user_awards>(.-)</user_awards>"
			for item in string.gmatch(returnXML, filterString) do
				local tempPrizeInfo = {}
				tempPrizeInfo.username = xmlGetString(item, "username")
				tempPrizeInfo.count = xmlGetInteger(item, "award_quantity")
				tempPrizeInfo.ranking = xmlGetInteger(item, "ranking")
				tempPrizeInfo.groupName = xmlGetString(item, "name")
				tempPrizeInfo.dateAwarded = xmlGetString(item, "date_awarded")
				tempPrizeInfo.dateAwarded = formatDateAwarded(tostring(tempPrizeInfo.dateAwarded))
				local tempLeaderboardId = xmlGetInteger(item, "world_leaderboard_id")

				if tempPrizeInfo.username == m_username then
					m_worldLeaderboardId = tempLeaderboardId
					m_prizeData = tempPrizeInfo
					m_pendingRewards = m_pendingRewards + 1
					updateVisibleElements()
				end
			end
		end
	elseif filter == WF.LB_REWARD_STATUS_UPDATE then
		local returnXML = KEP_EventDecodeString( tEvent )
		local returnCode = ParseXmlReturnCode(returnXML)
		local returnDescription = xmlGetString(returnXML, "ReturnDescription")
		if returnCode == 0 then
			m_pendingRewards = m_pendingRewards - 1
			if m_pendingRewards > 0 then
				-- Check for another pending reward
				local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=getAward&zoneInstanceId="..tostring(m_zoneInstanceId).."&zoneType="..tostring(m_zoneType)
				makeWebCall(address, WF.LB_REWARD_STATUS_GET)
			else
				MenuCloseThis()
			end
		end
	end
end

-- 2016-02-14T21:00:10-05:00
formatDateAwarded = function(inputString)
	local returnString = ""
	if inputString ~=nil then
		local year = string.sub(inputString, 1, 4)
		local month = tostring(tonumber(string.sub(inputString, 6, 7)))
		local day = tostring(tonumber(string.sub(inputString, 9, 10)))
		returnString = month.."/"..day.."/"..year
	end

	return returnString
end

---------------------
-- Local Functions --
---------------------

initializeVisibleElements = function()

	-- Enable elements
	setVisibleAndEnabled(gHandles["topBar"], true)
	setVisibleAndEnabled(gHandles["imgBG"], true)
	setVisibleAndEnabled(gHandles["stcCongratsDropShadow"], true)
	setVisibleAndEnabled(gHandles["stcCongrats"], true)
	generateOutlinedString("Congrats", 1, OUTLINE_COLOR)
	setVisibleAndEnabled(gHandles["stcLastWeek"], true)
	generateOutlinedString("LastWeek", 1, OUTLINE_COLOR)
	setVisibleAndEnabled(gHandles["stcYourShare"], true)
	generateOutlinedString("YourShare", 1, OUTLINE_COLOR)
	setVisibleAndEnabled(gHandles["stcNote"], true)

end

updateVisibleElements = function()

	local lastWeekText = "<font color="..tostring(TEXT_COLOR)..">Your team</font><font color="..tostring(HIGHLIGHT_COLOR).."> "..tostring(m_prizeData.groupName).."</font><font color="..tostring(TEXT_COLOR).."> took "..tostring(getRankingSuffix(m_prizeData.ranking)).." in the Top Places Weekly Leaderboard that ended on "..tostring(m_prizeData.dateAwarded).."!</font>"
	local lastWeekTextOutline = "Your team "..tostring(m_prizeData.groupName).." took "..tostring(getRankingSuffix(m_prizeData.ranking)).." in the Top Places Weekly Leaderboard that ended on "..tostring(m_prizeData.dateAwarded).."!"

	if m_prizeData.groupName == nil or m_prizeData.groupName == "" then
		lastWeekText = "<font color="..tostring(TEXT_COLOR)..">Your team took "..tostring(getRankingSuffix(m_prizeData.ranking)).." in the Top Places Weekly Leaderboard that ended on "..tostring(m_prizeData.dateAwarded).."!</font>"
		lastWeekTextOutline = "Your team took "..tostring(getRankingSuffix(m_prizeData.ranking)).." in the Top Places Weekly Leaderboard that ended on "..tostring(m_prizeData.dateAwarded).."!"
	end
	
	-- Static_SetText(gHandles["stcLastWeek"], lastWeekText)
	OutlinedString_SetText("LastWeek", lastWeekText, lastWeekTextOutline, OUTLINE_COLOR)
	local yourShareText = "Your share of the prize is "..formatNumberCommas(tostring(m_prizeData.count)).." World Coins."
	-- Static_SetText(gHandles["stcYourShare"], yourShareText)
	OutlinedString_SetText("YourShare", yourShareText, yourShareText, OUTLINE_COLOR)

	-- Enable prize claim button if item data is ready
	if m_rewardStatus == REWARD_STATUS.WAITING then
		m_rewardStatus = REWARD_STATUS.PRIZE_DATA_READY
	elseif m_rewardStatus == REWARD_STATUS.GAME_ITEMS_CACHED then
		m_rewardStatus = REWARD_STATUS.READY_TO_CLAIM
		initializeReward()
	end
end

initializeReward = function()
	m_rewardItem = {} -- NEW
	m_rewardItem.UNID = m_rewardUNID
	m_rewardItem.properties = deepCopy(m_gameItems[tostring(m_rewardUNID)])

	setVisibleAndEnabled(gHandles["btnGetPrize"], true)
end

attemptGetPrize = function()
	local removeTable = {}
	local addTable = {}
	if m_rewardItem.UNID and tonumber(m_rewardItem.UNID) > 0 then
		local addItem = {}
		addItem = deepCopy(m_rewardItem)
		addItem.lootInfo = {lootSource = "Leaderboard"} -- m_actorPID?
		addItem.count = m_prizeData.count or 1
		table.insert(addTable, addItem)
	end
	processTransaction(removeTable, addTable)
end

-- Response for loot acquisition event
function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	if success then
		local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=redeemAward&worldLeaderboardId="..tostring(m_worldLeaderboardId)
		makeWebCall(address, WF.LB_REWARD_STATUS_UPDATE)
	end
end

----------------------
-- Helper Functions --
----------------------

setVisibleAndEnabled = function(inputHandle, inputValue)
	Control_SetVisible(inputHandle, inputValue)
	Control_SetEnabled(inputHandle, inputValue)
end

function generateOutlinedString(baseName, outlineSize, outlineColor)
	local controlText = "<font color="..tostring(outlineColor)..">"..Static_GetText(gHandles["stc" .. baseName]).."</font>"
	local XLocation = Control_GetLocationX(gHandles["stc" .. baseName])
	local YLocation = Control_GetLocationY(gHandles["stc" .. baseName])
	local XOffset = {0, 0, outlineSize, -1*outlineSize}
	local YOffset = {outlineSize, -1*outlineSize, 0, 0}
	-- local controlWidth = Control_GetWidth(gHandles["stc" .. baseName])
	-- local controlHeight = Control_GetHeight(gHandles["stc" .. baseName])
	for i = 1, 4 do	
		local newControl = copyControl(gHandles["stc" .. baseName])
		local newControlName = "stc" .. baseName .. "BG" .. i
		Control_SetName(newControl, newControlName)
		Static_SetText(newControl, controlText)
		-- Control_SetSize(newControl, controlWidth, controlHeight)
		Control_SetLocationX(newControl, XLocation + XOffset[i])
		Control_SetLocationY(newControl, YLocation + YOffset[i])
		Static_SetUseHTML(newControl, true)
		gHandles[newControlName] = newControl
	end
	Dialog_MoveControlToFront(gDialogHandle, gHandles["stc" .. baseName])
end

function OutlinedString_SetText(baseName, baseText, outlineText, outlineColor)
		SetControlText("stc" .. baseName, baseText)

		for i = 1, 4 do
			SetControlText("stc" .. baseName .. "BG" .. i, "<font color="..tostring(outlineColor)..">"..outlineText.."</font>")
		end
end

getRankingSuffix = function(inputNumber)
	if inputNumber == 1 then
		return tostring(inputNumber).."st"
	elseif inputNumber == 2 then
		return tostring(inputNumber).."nd"
	elseif inputNumber == 3 then
		return tostring(inputNumber).."rd"
	else
		return tostring(inputNumber).."th"
	end
end

function formatNumberCommas(inputStr)
   if tonumber(inputStr) == nil then
       return inputStr
   end

   local formatted = inputStr
   while true do  
       formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
       if (k==0) then
           break
       end
   end
   return formatted
end