--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- DRF - Moved From CommonFunctions

--[[ DRF - Static menu functions logging. Only call from within this api. ]]
function mfLog(txt) KEP_Log(LogName().."MenuFunctions::"..txt) end
function mfLogWarn(txt) KEP_LogWarn(LogName().."MenuFunctions::"..txt) end
function mfLogError(txt) KEP_LogError(LogName().."MenuFunctions::"..txt) end

--[[ DRF - Return Menu Name Without '.xml' ]]
function MenuNameWithoutXml(menuName)
	return string.match(menuName, "([^.]+)") or ""
end

--[[ DRF - Return Menu Name With '.xml' ]]
function MenuNameWithXml(menuName)
	return MenuNameWithoutXml(menuName)..".xml"
end
function MenuNameNormalize(menuName) return MenuNameWithXml(menuName) end

--[[ DRF - Return handle of named menu or AB variant if open ]]
function MenuHandle(menuName)
	if not MenuNameOk(menuName) then return nil end
	return Menu_GetDialog(menuName)
end
function MenuHandleThis()
	return gDialogHandle
end

--[[ DRF - Return If Menu Handle Is Ok (non-zero user data)]]
function MenuHandleOk(menuHandle)
	return (menuHandle ~= nil and menuHandle ~= 0)
end

--[[ DRF - Returns menu identifier for given menu handle ]]
function MenuId(menuHandle)
	if not MenuHandleOk(menuHandle) then return nil end
	return Dialog_MenuId(menuHandle)
end
function MenuIdThis()
	return gDialogMenuId
end

--[[ DRF - Returns menu name for given menu handle ]]
--[[ DRF - MENU NAME IS NOW MENU ID !!! ]]
function MenuName(menuHandle)
	if not MenuHandleOk(menuHandle) then return nil end
	return Dialog_MenuId(menuHandle)
end
function MenuNameThis()
	return MenuIdThis()
end

--[[ DRF - Return If Menu Name Is Ok (non-empty string) ]]
function MenuNameOk(menuName)
	return (menuName ~= nil and menuName ~= "")
end

--[[ DRF - Return number of menus currently open ]]
function MenusOpen()
	return Dialog_NumOpen()
end

--[[ DRF - Returns true if named menu or AB variant is open or false if not.  Replaces deprecated IsDialogOrVariantOpenByName()==1. ]]
function MenuIsOpen(menuName)
	if not MenuNameOk(menuName) then return false end
	return Menu_IsOpen(menuName)
end
function MenuIsOpenThis() 
	return MenuIsOpen(MenuNameThis()) 
end

--[[ DRF - Returns true if named menu or AB variant is closed or false if not.  Replaces deprecated IsDialogOrVariantOpenByName()==0. ]]
function MenuIsClosed(menuName)
	if not MenuNameOk(menuName) then return true end
	return not Menu_IsOpen(menuName)
end
function MenuIsClosedThis() 
	return MenuIsClosed(MenuNameThis()) 
end

--[[ DRF - DEPRECATED - Use MenuHandle() Instead! ]]
function GetDialogOrVariantByName(menuName)
	mfLogWarn("GetDialogOrVariantByName() DEPRECATED - Use MenuHandle() instead")
	return MenuHandle(menuName)
end

--[[ DRF - DEPRECATED - Use MenuIsOpen() Instead! ]]
function IsDialogOrVariantOpenByName(menuName)
	mfLogWarn("IsDialogOrVariantByName() DEPRECATED - Use MenuIsOpen() instead")
	return ToBOOL(MenuIsOpen(menuName))
end
	
--[[ DRF - Open named menu or AB variant even if menu is already open. Returns menu handle. ]]
--[[ WARNING - It is very dangerous to use this directly! ]]
function DoMenuOpenAs(menuName, menuId, bModal)
	if not MenuNameOk(menuName) or not MenuNameOk(menuId) then return nil end
	
	-- Menu_Open()/Menu_OpenTrusted() Needs '.xml'
	-- TODO - Make Menu_Open Not Need '.xml'
	local menuNameWithoutXml = MenuNameWithoutXml(menuName)
	local menuNameWithXml = MenuNameWithXml(menuName)

	-- Attempt Open of Menu AB Variant
	local groupList = KEP_GetABGroupList()
	for groupId in string.gmatch(groupList, "([^,]+),") do
		-- Try to open the "_ab<groupId>" file, this will silently fail if it doesn't exist.
		-- Use Menu_OpenTrusted to load AB menu from WOK only to prevent hacking (e.g. deploying an fake WOK AB menu in a 3dapp to intercept WOK menu open requests)
		local menuNameAB = 	menuNameWithoutXml.."_ab"..groupId..".xml"
		if Menu_OpenTrustedAs(menuNameAB, menuId) then
			break
		end
	end
	
	-- No Variant Then Open Regular Menu As Usual
	if MenuIsClosed(menuId) then
		Menu_OpenAs(menuNameWithXml, menuId)
	end
	
	-- Menu Failure?
	if MenuIsClosed(menuId) then
		mfLogError("DoMenuOpen: Menu_OpenAs("..menuName.." as "..menuId..") FAILED")
--        if string.lower(menuName) ~= "infobox.xml" then     -- prevent recusion
--            KEP_MessageBox("Failed Opening " .. menuName)
--        end
        return nil
	end

	-- Set Modality (default non-modal)
	if (bModal) then 
		MenuSetModal(menuId, true)
	end

	return MenuHandle(menuId)
end

function DoMenuOpen(menuName, bModal) 
	return DoMenuOpenAs(menuName, menuName, bModal) 
end

--[[ DRF - Opens named menu if not already open and brings to front. Returns menu handle. Replaces deprecated Dialog_CreateAs(). ]]
function MenuOpenAs(menuName, menuId, bModal)
	
	-- Open Only If Not already Open
	local menuHandle = MenuHandle(menuId)
	if not MenuHandleOk(menuHandle) then 
		menuHandle = DoMenuOpenAs(menuName, menuId, bModal)
	end
	
	-- Bring Menu To Front
	MenuBringToFront(menuId)

	return menuHandle
end

--[[ DRF - Opens named menu if not already open and brings to front. Returns menu handle. Replaces deprecated Dialog_Create(). ]]
function MenuOpen(menuName, bModal)
	
	-- Open Only If Not already Open
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then 
		menuHandle = DoMenuOpen(menuName, bModal)
	end
	
	-- Bring Menu To Front
	MenuBringToFront(menuName)

	return menuHandle
end

--[[ DRF - Opens named menu modally if not already open and brings to front. Returns menu handle. Replaces deprecated Dialog_Create(). ]]
function MenuOpenModal(menuName)
	return MenuOpen(menuName, true)
end

--[[ DRF - Closes named menu if not already closed. ]]
function MenuClose(menuName)
	if not MenuNameOk(menuName) then return false end
	return Menu_Close(menuName)
end
function MenuCloseThis()
	return MenuClose(MenuNameThis())
end

--[[ DRF - Closes all menus. ]]
function MenuCloseAll()
	return Menu_CloseAll()
end

--[[ DRF - Closes all menus except named menu if not already closed. Replaces deprecated Dialog_CloseAll(). ]]
function MenuCloseAllExcept(menuName)
	if not MenuNameOk(menuName) then return false end
	return Menu_CloseAllExcept(menuName)
end
function MenuCloseAllExceptThis()
	return MenuCloseAllExcept(MenuNameThis())
end

--[[ DRF - Closes all non-essential menus except named menu if not already closed. ]]
function MenuCloseAllNonEssentialExcept(menuName, includeFramework)
	if not MenuNameOk(menuName) then return false end
	return Menu_CloseAllNonEssentialExcept(menuName, includeFramework or false)
end
function MenuCloseAllNonEssentialExceptThis(includeFramework)
	return MenuCloseAllNonEssentialExcept(MenuNameThis(), includeFramework)
end

--[[ DRF - Close All menus pre-rezone except LoginMenu, called by ProgressController. ]]
function MenuCloseAllPreRezone()
	-- Closing all menus caused LoginMenu crash for web 'Play Now' of 'World Unavailable'
	-- Closing all menus except LoginMenu beat not closing any menus in testGroup test.
	mfLog("MenuCloseAllPreRezone: Closing All Menus Except LoginMenu...")
	return MenuCloseAllExcept("LoginMenu.xml")
end

--[[ DRF - Toggle Open/Close of named menu. ]]
function MenuToggleOpenClose(menuName)
	if MenuIsOpen(menuName) then MenuClose(menuName) else MenuOpen(menuName) end
end

--[[ DRF - Sends named menu to back if open. Replaces deprecated Dialog_SendToBack(). ]]
function MenuSendToBack(menuName)
	if not MenuNameOk(menuName) then return false end
	return Menu_SendToBack(menuName)
end
function MenuSendToBackThis()
	return MenuSendToBack(MenuNameThis())
end

--[[ DRF - Brings named menu to front if open. Replaces deprecated Dialog_BringToFront(). ]]
function MenuBringToFront(menuName)
	if not MenuNameOk(menuName) then return false end
	return Menu_BringToFront(menuName)
end
function MenuBringToFrontThis()
	return MenuBringToFront(MenuNameThis())
end

--[[ DRF - Enables/disables allowing bringing named menu to front. Replaces deprecated Dialog_BringToFrontEnable(). ]]
function MenuBringToFrontEnable(menuName, enable)
	if not MenuNameOk(menuName) then return false end
	return Menu_BringToFrontEnable(menuName, ToBOOL(enable))
end
function MenuBringToFrontEnableThis(enable)
	return MenuBringToFrontEnable(MenuNameThis(), enable)
end

--[[ DRF - Always brings named menu to front if open. Replaces deprecated Dialog_AlwaysBringToFront(). ]]
function MenuAlwaysBringToFront(menuName)
	if not MenuNameOk(menuName) then return false end
	return Menu_AlwaysBringToFront(menuName)
end
function MenuAlwaysBringToFrontThis()
	return MenuAlwaysBringToFront(MenuNameThis())
end

--[[ DRF - Disable current always bring to front menu. Replaces deprecated Dialog_AlwaysBringToFrontDisable(). ]]
function MenuAlwaysBringToFrontDisable()
	return Menu_AlwaysBringToFrontDisable()
end

--[[ DRF - Get named menu location. Replaces deprecated Dialog_GetLocation(). ]]
function MenuGetLocation(menuName)
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then return nil end
	return { x=Dialog_GetLocationX(menuHandle), y=Dialog_GetLocationY(menuHandle) }
end
function MenuGetLocationThis() 
	return MenuGetLocation(MenuNameThis()) 
end

--[[ DRF - Set named menu location. Replaces deprecated Dialog_SetLocation(). ]]
function MenuSetLocation(menuName, x, y)
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then return false end
	if (x == nil or y == nil) then 
		local loc = MenuGetLocation(menuName)
		x = x or loc.x
		y = y or loc.y
	end
	return ToBool(Dialog_SetLocation(menuHandle, x, y))
end
function MenuSetLocationThis(x, y) 
	return MenuSetLocation(MenuNameThis(), x, y) 
end

--[[ DRF - Offset named menu location. ]]
function MenuOffsetLocation(menuName, x, y)
	local loc = MenuGetLocation(menuName)
	if (loc == nil) then return false end
	x = loc.x + (x or 0)
	y = loc.y + (y or 0)
	return MenuSetLocation(menuName, x, y)
end
function MenuOffsetLocationThis(x, y) 
	return MenuOffsetLocation(MenuNameThis(), x, y) 
end

--[[ DRF - Get named menu size. Replaces deprecated Dialog_GetWidth/Height(). ]]
function MenuGetSize(menuName)
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then return nil end
	return { width=Dialog_GetWidth(menuHandle), height=Dialog_GetHeight(menuHandle) }
end
function MenuGetSizeThis() 
	return MenuGetSize(MenuNameThis()) 
end

--[[ DRF - Set named menu size. Replaces deprecated Dialog_SetWidth/Height(). ]]
function MenuSetSize(menuName, width, height)
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then return false end
	if (width == nil or height == nil) then 
		local size = MenuGetSize(menuName)
		width = width or size.width
		height = height or size.height
	end
	return ToBool(Dialog_SetSize(menuHandle, width, height))
end
function MenuSetSizeThis(width, height) 
	return MenuSetSize(MenuNameThis(), width, height) 
end

--[[ DRF - Offset named menu size. ]]
function MenuOffsetSize(menuName, width, height)
	local size = MenuGetSize(menuName)
	if (size == nil) then return false end
	width = size.width + (width or 0)
	height = size.height + (height or 0)
	return MenuSetSize(menuName, width, height)
end
function MenuOffsetSizeThis(width, height) 
	return MenuOffsetSize(MenuNameThis(), width, height) 
end

--[[ DRF - Returns if this menu contains the given menu relative point within it's size box. ]]
function MenuContainsPointRel(menuName, pointX, pointY)
	if (pointX < 0) or (pointY < 0) then return false end
	local size = MenuGetSize(menuName)
	if (size == nil) then return false end
	return ((pointX < size.width) and (pointY < size.height))
end
function MenuContainsPointRelThis(pointX, pointY) 
	return MenuContainsPointRel(MenuNameThis(), pointX, pointY) 
end

--[[ DRF - Returns if this menu contains the given client absolute point within it's size box. ]]
function MenuContainsPointAbs(menuName, pointX, pointY)
	local loc = MenuGetLocation(menuName)
	if (loc == nil) then return false end
	local pointRelX = pointX - loc.x
	local pointRelY = pointY - loc.y
	return MenuContainsPointRel(menuName, pointRelX, pointRelY)
end
function MenuContainsPointAbsThis(pointX, pointY) 
	return MenuContainsPointAbs(MenuNameThis(), pointX, pointY) 
end

--[[ DRF - Set named menu capture keys. Replaces deprecated Dialog_SetCaptureKeys(). ]]
function MenuSetCaptureKeys(menuName, enable)
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then return false end
	return ToBool(Dialog_SetCaptureKeys(menuHandle, ToBOOL(enable)))
end
function MenuSetCaptureKeysThis(enable) 
	return MenuSetCaptureKeys(MenuNameThis(), enable) 
end

--[[ DRF - NEW - Replaces deprecated Dialog_CopyTemplate() ]]
function MenuCopyTemplate(menuName)
	if not MenuNameOk(menuName) then return false end
	return Menu_CopyTemplate(menuName)
end
function MenuCopyTemplateThis()
	return MenuCopyTemplate(MenuNameThis())
end

--[[ DRF - NEW - Replaces deprecated Dialog_ApplyInputSettings() (menuName NOT USED) ]]
function MenuApplyInputSettings(menuName)
	--if not MenuNameOk(menuName) then return false end
	return Menu_ApplyInputSettings("")
end
function MenuApplyInputSettingsThis()
	return MenuApplyInputSettings(MenuNameThis())
end

--[[ DRF - Enables/Disables named menu mouse passthrough. Replaces deprecated Dialog_SetPassthrough(). ]]
function MenuSetPassthrough(menuName, enable)
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then return false end
	return ToBool(Dialog_SetPassthrough(menuHandle, ToBOOL(enable)))
end
function MenuSetPassthroughThis(enable)
	return MenuSetPassthrough(MenuNameThis(), enable)
end

--[[ DRF - Modalizes/Unmodalizes named menu. Replaces deprecated Dialog_SetModal(). ]]
function MenuSetModal(menuName, enable)
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then return false end
	return ToBool(Dialog_SetModal(menuHandle, ToBOOL(enable)))
end
function MenuSetModalThis(enable)
	return MenuSetModal(MenuNameThis(), enable)
end

--[[ DRF - Returns true if named menu is minimized. Replaces deprecated Dialog_GetMinimized(). ]]
function MenuGetMinimized(menuName)
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then return false end
	return ToBool(Dialog_GetMinimized(menuHandle))
end
function MenuGetMinimizedThis()
	return MenuGetMinimized(MenuNameThis())
end

--[[ DRF - Minimizes/Restores named menu. Replaces deprecated Dialog_SetMinimized(). ]]
function MenuSetMinimized(menuName, enable)
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then return false end
	return ToBool(Dialog_SetMinimized(menuHandle, ToBOOL(enable)))
end
function MenuSetMinimizedThis(enable)
	return MenuSetMinimized(MenuNameThis(), enable)
end

--[[ DRF - Clears named menu focus. Replaces deprecated Dialog_ClearFocus(). ]]
function MenuClearFocus(menuName)
	local menuHandle = MenuHandle(menuName)
	if not MenuHandleOk(menuHandle) then return false end
	return ToBool(Dialog_ClearFocus(menuHandle))
end
function MenuClearFocusThis()
	return MenuClearFocus(MenuNameThis())
end

function MenuCenter(menuName, verticalOnly, horizontalOnly)
	local screenWidth = Dialog_GetScreenWidth(MenuHandle(menuName))
	local screenHeight = Dialog_GetScreenHeight(MenuHandle(menuName))
	local size = MenuGetSize(menuName)
	local menuWidth = size.width
	local menuHeight = size.height

	local currentPos = MenuGetLocationThis()
	local posX = screenWidth/2 - menuWidth/2
	local posY = screenHeight/2 - menuHeight/2
	
	if verticalOnly then
		posX = currentPos.x
	end

	if horizontalOnly then
		posY =  currentPos.y
	end

	MenuSetLocation(menuName, math.max(posX, 0), math.max(posY, 0))
end

function MenuCenterThis(verticalOnly, horizontalOnly)
	return MenuCenter(MenuNameThis(), verticalOnly, horizontalOnly)
end