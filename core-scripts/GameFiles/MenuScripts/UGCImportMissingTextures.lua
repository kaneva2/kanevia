--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

g_filter = 0
g_objectid = 0

g_sendAnswerEvent = 0

UGC_IMPORT_CONFIRMATION_MENU_EVENT = "UGCImportConfirmationMenuEvent"
UGC_IMPORT_CONFIRMATION_RESULT_EVENT = "UGCImportConfirmationResultEvent"

function menuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_filter = filter
	g_objectid = objectid
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "menuEventHandler", UGC_IMPORT_CONFIRMATION_MENU_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( UGC_IMPORT_CONFIRMATION_MENU_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)

	-- DRF - Added
	sendAnswerEvent( g_sendAnswerEvent )

	Helper_Dialog_OnDestroy( dialogHandle )
end

function sendAnswerEvent( answer )
	local e = KEP_EventCreate( UGC_IMPORT_CONFIRMATION_RESULT_EVENT )
	KEP_EventSetFilter( e, g_filter )
	KEP_EventSetObjectId( e, g_objectid )
	KEP_EventEncodeNumber( e, answer )
	KEP_EventQueue( e )
end

function btnConfirm_OnButtonClicked(buttonHandle)
	g_sendAnswerEvent = 1
	MenuCloseThis()
end
