--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\ChatType.lua")

-- chat type constants
ctTalk 	= ChatType.Talk
ctGroup	= ChatType.Group
ctClan	= ChatType.Clan
ctPrivate   = ChatType.Private
ctSystem = ChatType.System

g_chat_type = ChatType.Talk

function Helper_Chat_OnCreate(dialogHandle)
	-- set focus to the default control (should be txtChat)
	Dialog_FocusDefaultControl( dialogHandle)
end

function txtChat_OnEditBoxString(editBoxHandle, password)
	local msg = EditBox_GetText(editBoxHandle)
	if msg ~= "" then
		KEP_SendChatMessage( g_chat_type, msg )
	end
	MenuCloseThis()
end
