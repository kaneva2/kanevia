--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_LastGameItemWarning.lua
-- 
-- Warns the user of removing the last game item in the zone
-- Author: Wes Anderson
--This menu will also handle removing of an event flag that has an event scheduled with it
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_EventHelper.lua")

----------------------
-- Globals
----------------------
local m_lastGameItem
local m_eventFlag = false

-- When the menu is created
function onCreate()
	-- Register client - client event handlers
	KEP_EventRegisterHandler("frameworkLastGameItemPIDHandler", "FrameworkLastGameItemPID", KEP.HIGH_PRIO)
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Handles Game Item limit warning messages
function frameworkLastGameItemPIDHandler(dispatcher, fromNetid, event, eventID, filter, objectID)
	local lastPID
	--local eventFlag
	if KEP_EventMoreToDecode(event) ~= 0 then
		lastPID = KEP_EventDecodeNumber(event)
	end

	--Handles trying to remove event flag with a current event scheduled
	if KEP_EventMoreToDecode(event) ~= 0 then
		m_eventFlag = KEP_EventDecodeNumber(event) == 1

		--changes the menu to reflect deleting an event flag
		if m_eventFlag then
			Static_SetText(gHandles["stcWarning1"], "Removing the event flag will also delete the event associated with it.")
			Static_SetText(gHandles["stcWarning2"], "You will lose any credits or rewards you spent for a Premium Event")
			Static_SetText(gHandles["btnDisableGaming"], "Remove Event Flag")
			Dialog_SetCaptionText(gDialogHandle, " Remove Event Flag")
		end
	end

	m_lastGameItem = lastPID
end

-- Disable Gaming
function btnDisableGaming_OnButtonClicked(buttonHandle)
	if m_lastGameItem then
		-- Shut down the Framework if removing last game item. If removing flag, no need to de-instantiate
		if m_eventFlag == false then
			Events.sendEvent("FRAMEWORK_PICKUP_LAST_GAME_ITEM", {PID = m_lastGameItem})
		else
			Events.sendEvent("FRAMEWORK_PICKUP_GAME_ITEM", {PID = m_lastGameItem})
			Events.sendEvent("FRAMEWORK_DESTROY_EVENT_FLAG", {PID = m_lastGameItem})
		end
		
		MenuCloseThis()

		--Event so inventory knows to open to WOK Gaming
		local event = KEP_EventCreate("FRAMEWORK_WORLD_GAME_MODE")
		KEP_EventEncodeNumber(event, 0)
		KEP_EventQueue(event)
	else
		LogWarn("No valid Game Item PID passed for removal")
	end
end