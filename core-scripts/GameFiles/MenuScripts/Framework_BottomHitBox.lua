--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_BottomHitBox.lua
--
-- Shows/hides
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- Constants
local MINIMIZE_BOTTOM_ALL_FILTER = 0
local MINIMIZE_TOOLBELT_FILTER = 1
local VERTICAL_HIT_RANGE = 150
local MINIMIZE_DELAY = 1.25

-- Local vars
local m_cursorInBounds = false
local m_minimizedSetting = false
local m_minimizeTime = nil

-- Local functions
local checkCursorInBounds
local toggleHUDMinimized

-- Called when the menu is created
function onCreate()
	-- Event handlers
	KEP_EventRegisterHandler("frameworkMinimizeHUDHandler", "FRAMEWORK_MINIMIZE_HUD", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("hideToolbeltSettingEventHandler", "HideToolbeltSettingEvent", KEP.MED_PRIO)
	
	m_minimizedSetting = GetHideToolbeltSetting()
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_minimizedSetting then

		local isCursorInBounds = checkCursorInBounds()
		if isCursorInBounds ~= m_cursorInBounds then

			if isCursorInBounds then
				toggleHUDMinimized(false)
			else
				-- Minimize after a brief delay.
				m_minimizeTime = MINIMIZE_DELAY
			end

			m_cursorInBounds = isCursorInBounds
		end

		-- Minimize on timer.
		if m_minimizeTime and not isCursorInBounds then
			m_minimizeTime = m_minimizeTime - fElapsedTime

			if m_minimizeTime <= 0 then
				m_minimizeTime = nil
				toggleHUDMinimized(true)
			end
		end
	end
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Handles the "HideToolbeltSettingEvent" triggered when hud is hidden.
function hideToolbeltSettingEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local checked = Event_DecodeNumber(event)
	m_minimizedSetting = checked == 1
end

-- Handles the "frameworkMinimizeHUD" event triggered when hud is hidden.
function frameworkMinimizeHUDHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == MINIMIZE_BOTTOM_ALL_FILTER then
		local minimized = Event_DecodeNumber(event)
		local instant = Event_DecodeNumber(event)
		local dynamicEnable = Event_DecodeNumber(event)
		m_cursorInBounds = nil
		toggleHUDMinimized(minimized == 1, instant == 1, dynamicEnable == 1)
	end
end

-- -- -- -- -- -- --
-- Local Functions
-- -- -- -- -- -- --

-- Stretches the menu to fill the screen.
checkCursorInBounds = function()
	local cursorX = Cursor_GetLocationX()
	local cursorY = Cursor_GetLocationY()
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)

	local inBounds = cursorY >= screenHeight - VERTICAL_HIT_RANGE
	inBounds = inBounds or MenuContainsPointAbs("Framework_Toolbar", cursorX, cursorY)
	inBounds = inBounds or MenuContainsPointAbs("Framework_BottomRightHUD", cursorX, cursorY)

	return inBounds
end

-- Sends an event telling the game HUD to hide itself.
toggleHUDMinimized = function(minimized, instant, dynamicEnable)
	local ev = KEP_EventCreate("FRAMEWORK_MINIMIZE_HUD")
	KEP_EventSetFilter(ev, MINIMIZE_TOOLBELT_FILTER)
	KEP_EventEncodeNumber(ev, minimized and 1 or 0)
	KEP_EventEncodeNumber(ev, instant and 1 or 0)
	KEP_EventEncodeNumber(ev, dynamicEnable and 1 or 0)
	KEP_EventQueue(ev)
end