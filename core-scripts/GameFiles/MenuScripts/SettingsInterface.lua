--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

dofile("..\\MenuScripts\\LibClient_Common.lua")
local m_minimizedSetting = false

function onCreate()
	KEP_EventRegisterHandler( "hideHUDSettingEventHandler",	"HideHUDSettingEvent",KEP.MED_PRIO )
	KEP_EventRegisterHandler( "zoneNavigationEventHandler",	"ZoneNavigationEvent",KEP.MED_PRIO )

	m_minimizedSetting = GetHideHUDSetting()
	
	CheckBox_SetChecked(gHandles["chkHideHUD"], m_minimizedSetting)
	CheckBox_SetChecked(gHandles["chkHideToolbelt"], GetHideToolbeltSetting())
	CheckBox_SetChecked(gHandles["chkInteractive"], GetHideGlowSetting())
	CheckBox_SetChecked(gHandles["chkHideMap"], GetHideMapSetting())
end

function hideHUDSettingEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local checked = KEP_EventDecodeNumber(event)	
	m_minimizedSetting = checked == 1
	CheckBox_SetChecked(gHandles["chkHideHUD"], m_minimizedSetting)
end

---Button control functions
function chkHideHUD_OnCheckBoxChanged( checkboxHandle, checked )
	SetHideHUDSetting(checked)
end 

function chkHideToolbelt_OnCheckBoxChanged( checkboxHandle, checked )
	SetHideToolbeltSetting(checked)
end 

function chkInteractive_OnCheckBoxChanged( checkboxHandle, checked )
	SetHideGlowSetting(checked)
end 

function chkHideMap_OnCheckBoxChanged( checkboxHandle, checked )
	SetHideMapSetting(checked)
end 


function Dialog_OnDestroy(dialogHandle)

	Helper_Dialog_OnDestroy( dialogHandle )
end


function btnResetHidden_OnButtonClicked(buttonHandle)
	local playerName = KEP_GetLoginName()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber(config, "DoNotShowCopyToInventoryConfirmation", 0)
		KEP_ConfigSetNumber(config, "DoNotShowCopyToGameConfirmation", 0)
		KEP_ConfigSetNumber(config, "DoNotShowCreateSpawnerConfirmation", 0)
		KEP_ConfigSetNumber(config, "DoNotShowPlaceObjectConfirmation", 0)
		KEP_ConfigSetNumber(config, "HideDropMenu"..tostring(playerName), 0)
		KEP_ConfigSetNumber( config, "HideAddChildMenu"..tostring(playerName), 0 )
		KEP_ConfigSave(config)
	end 
end