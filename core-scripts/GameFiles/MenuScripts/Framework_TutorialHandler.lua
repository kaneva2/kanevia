--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------------------------------------
----Framework_MediaFlagHandler
--Handler for Event Flags, Media Players, Claim Flags and System Flags
----------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")

function onCreate( )
	Events.registerHandler("FRAMEWORK_GOTO_KTOWN", goToKTown)

	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
end

-----------------------------
---SERVER EVENT HANDLERS-----
-----------------------------
----Flags-----
function goToKTown(event)
	if event.instanceID then
		local url = GameGlobals.WEB_SITE_PREFIX..GET_LINKED_ZONES_SUFFIX.."&zoneInstanceID="..tostring(event.instanceID).."&zoneType=6"
		makeWebCall(url, WF.LINKED_ZONES)
	end
end




-----------------------------
----CLIENT EVENT HANDLERS----
-----------------------------

function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	if filter == WF.LINKED_ZONES then
		local estr = KEP_EventDecodeString( tEvent )
		ParseLocationData(estr)
	end
end


function ParseLocationData(locationData)
    local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	s, strPos, result = string.find(locationData, "<ReturnCode>(.-)</ReturnCode>")

	if result == "0" then
		for zoneData in string.gmatch(locationData, "<Table>(.-)</Table>") do
	
			s, strPos, url = string.find(zoneData, "<STPURL>(.-)</STPURL>")
			gotoURL(url)
			break
		end
	end
end