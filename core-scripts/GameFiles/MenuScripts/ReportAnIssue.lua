--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- ReportAnIssue.lua - Report an Issue menu
-- 
-- Menu used for reporting bugs
--
-- Copyright 2015 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\UGCFunctions.lua")
dofile("..\\MenuScripts\\MenuAnimation.lua")

SCREENSHOT_NONE = 0
SCREENSHOT_WAITING = 1
SCREENSHOT_TAKING = 2
SCREENSHOT_COMPLETE = 3

local m_tweenResize, m_tweenMove
local m_showScreenshot = false
local m_screenshotPath = nil
local m_menuLoc = {x = 0, y = 0}
local m_screenshotState = SCREENSHOT_NONE

REPORT_ISSUE_PREFIX = "kgp/reportIssue.aspx"

gNextDropId = 0

gTarget = {}
gTarget.valid = 0
gTarget.pos = {}
gTarget.pos.x = 0
gTarget.pos.y = 0
gTarget.pos.z = 0
gTarget.normal = {}
gTarget.normal.x = 0
gTarget.normal.y = 0
gTarget.normal.z = 0
gTarget.type = ObjectType.DYNAMIC
gTarget.id = -1
gTarget.name = "Media Library"

-- Ankit ----- bool to let the sciprt to know whether the image is available to be uploaded on the browser, for UGC content
g_prevImgForBrowserReady = false
g_screenShotTaken = false

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "UGCDeployCompletionEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PrevImgAvailableEvent", KEP.MED_PRIO )
end

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "UGCDeployCompletionHandler", "UGCDeployCompletionEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "PrevImgAvailableEventHandler", "PrevImgAvailableEvent", KEP.HIGH_PRIO )
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) -- REQUIRED - DO NOT REMOVE

	-- Setup tween for screenshot
	local resizeAmount = Control_GetHeight(gHandles["imgScreenshot"]) + 10
	m_tweenResize = Tween(gDialogHandle)
	m_tweenMove = Tween(nil, {"stcDisclaimer1", "stcDisclaimer2", "imgHorizRule2", "btnCancel", "btnSend"})
	m_tweenResize:resize(0, resizeAmount, .15)
	m_tweenMove:move(0, resizeAmount, .15)
	m_tweenResize:on(CALLBACK.STOP, resizeFinished)

	Static_SetText(gHandles["stcUsername"], KEP_GetLoginName())

	m_menuLoc = MenuGetLocationThis()
end

function UGCDeployCompletionHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local ugcType = Event_DecodeNumber( event )
	local ugcId = Event_DecodeNumber( event )
	local resultText = Event_DecodeString( event )

	-- If this is nil for some reason (eg upload failed),
	-- sendBugWebcall will treat it was bug report with no screenshot
	local filestore = string.match(resultText, "<TextureUrl>(.+)</TextureUrl>")
	sendBugWebcall(filestore)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.SEND_BUG_REPORT then
		local returnData = KEP_EventDecodeString(event)
		local returnCode = tonumber(string.match(returnData, "<ReturnCode>(%d-)</ReturnCode>") or -3)

		if returnCode == 0 then
			MenuCloseThis()
			return KEP.EPR_CONSUMED

		elseif returnCode == -1 then
			-- Should not reach this due to Send being greyed out if no text, but just in case
			KEP_MessageBox("You must give an issue description.")
		elseif returnCode < -1 then
			KEP_MessageBox("Unable to connect to bug reporter, please try again later.")
		end

		Static_SetText(gHandles["btnSend"], "Send Feedback")

		return KEP.EPR_CONSUMED
	end
end

function btnScreenshot_OnButtonClicked(btnHandle)
	if m_showScreenshot then
		Control_SetVisible(gHandles["imgScreenshot"], false)
		m_showScreenshot = false
		resizeMenu()
	else
		startScreenshot()
	end
end

function startScreenshot()
	m_menuLoc = MenuGetLocationThis()
	local size = MenuGetSizeThis()
	MenuSetLocationThis(-size.width, -size.height)
	m_screenshotState = SCREENSHOT_WAITING
end

function resizeMenu()
	m_tweenResize:reverse(not m_showScreenshot)
	m_tweenMove:reverse(not m_showScreenshot)

	m_tweenResize:start()
	m_tweenMove:start()
end

function btnSend_OnButtonClicked(btnHandle)
	Control_SetEnabled(btnHandle, false)
	Static_SetText(btnHandle, "Sending...")

	-- Upload screenshot if applicable, otherwise jump straight to webcall
	if m_showScreenshot and m_screenshotPath then
		uploadScreenshot(m_screenshotPath)
	else
		sendBugWebcall()
	end
end

function uploadScreenshot(screenshotFileLoc)
	if screenshotFileLoc == nil or screenshotFileLoc == "" then
		sendBugWebcall()
		return
	end

	local importInfo = {}
	importInfo.sessionId = -1
	importInfo.fullPath = screenshotFileLoc
	importInfo.category = UGC_TYPE.TEXTURE
	importInfo.type = UGC_TYPE.TEXTURE

	local dropId = gNextDropId
	gNextDropId = gNextDropId + 1

	UGCI.CallUGCHandler("TextureImport.xml", 0, dropId, importInfo, gTarget, 0, 0 )
end

function sendBugWebcall(screenshotURL)
	local url = GameGlobals.WEB_SITE_PREFIX .. REPORT_ISSUE_PREFIX

	local issueText = KEP_UrlEncode(EditBox_GetText(gHandles["edBug"]))
	url = url .. "?IssueText=" .. issueText

	if screenshotURL and screenshotURL ~= "" then
		url = url .. "&screenshots=" .. KEP_UrlEncode(screenshotURL)
	end

	local uuid = KEP_UuidCreate()
	url = url .. "&guid=" .. uuid

	makeWebCall(url, WF.SEND_BUG_REPORT)

	KEP_ReportIssue(uuid, "Script::ReportIssue")
end

function edBug_OnEditBoxChange(edHandle, text)
	local emptySpace = text == "" or string.match(text, "^ -$") ~= nil

	Control_SetEnabled(gHandles["btnSend"], not emptySpace)
end

function resizeFinished()
	Control_SetVisible(gHandles["imgScreenshot"], m_showScreenshot)

	if m_showScreenshot then
		Static_SetText(gHandles["btnScreenshot"], "Remove Screenshot")
	else
		Static_SetText(gHandles["btnScreenshot"], "Take a Screenshot")
	end
end

function setScreenshotImage(filename)
	if( g_prevImgForBrowserReady == true ) then
		local jpg = string.match(filename, "\\([^\\]-.jpg)") or filename
		scaleImage(gHandles["imgScreenshot"], gHandles["imgScreenshotBG"], "..\\..\\Screenshots\\"..jpg)
		g_screenShotTaken = false
		g_prevImgForBrowserReady = false
	end
end

function Dialog_OnRender(gDialogHandle, fElapsedTime)
	MenuAnimation_OnRender(fElapsedTime)

	if m_screenshotState == SCREENSHOT_WAITING then
		m_screenshotState = SCREENSHOT_TAKING

	elseif m_screenshotState == SCREENSHOT_TAKING then

		-- Ankit ----- replacing single call to SaveScreenshot with two calls, one for screen capture request and other to get the path name
		m_screenshotPath = KEP_ScreenshotCapture()

		g_screenShotTaken = true
	
		m_screenshotState = SCREENSHOT_COMPLETE
	elseif m_screenshotState == SCREENSHOT_COMPLETE then

		setScreenshotImage(m_screenshotPath)
		Control_SetVisible(gHandles["imgScreenshot"], false)
		MenuSetLocationThis(m_menuLoc.x, m_menuLoc.y)	

		m_showScreenshot = true
		resizeMenu()

		m_screenshotState = SCREENSHOT_NONE
	end
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle ) -- REQUIRED - DO NOT REMOVE
end

function PrevImgAvailableEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if( g_screenShotTaken == true ) then
		g_prevImgForBrowserReady = true
		setScreenshotImage(m_screenshotPath)
	end
end
