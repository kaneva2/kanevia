--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\MathHelper.lua")
dofile("..\\MenuScripts\\UGCZoneLiteProxy.lua")

-- Cached hash of zones (format described in UGCZoneLiteProxy.lua)
-- Key == zoneIndex / Value = zoneData
g_zoneDataList = {}

-- UI listItem selected zoneIndex
g_selectedZoneIndex = -1

g_currentZone = {}
g_currentZone.Index = 0
g_currentZone.Type  = 0

CZMenuMode = {}
CZMenuMode.NOMODE = 0
CZMenuMode.NEW    = 1
CZMenuMode.IMPORT = 2

g_ZEBusy = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	UGCZoneLiteProxy_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	UGCZoneLiteProxy_InitializeKEPEvents(dispatcher, handler, debugLevel)
end

function setupCreateZoneMenu(mode)
	local selZone = g_zoneDataList[g_selectedZoneIndex]
	if selZone == nil then
		LogError("setupCreateZoneMenu: unknown zoneIndex["..tostring(g_selectedZoneIndex).."]")
		return
	end

	local event = KEP_EventCreate( "CreateZoneMenuSetupEvent")
	if (mode == CZMenuMode.NEW) then
		KEP_EventSetFilter(event, 1)                               -- (1 new)
		KEP_EventEncodeNumber(event, 0)                            -- showNewSection  -- (1 false -- 0 true)
		KEP_EventEncodeNumber(event, 0)                            -- trackStatus     -- (0 false -- 1 true)
		KEP_EventEncodeNumber(event, 1)                            -- (1 All -- 2 ScriptedOnly --3 NonScriptedOnly)
		KEP_EventEncodeNumber(event, UGCZLPZoneTypes.CURRENT_TYPE) -- importzonetype  -- (3 home / 4 zone/ 5 arena / 6 channel)
		KEP_EventEncodeNumber(event, 0)                            -- zoneIndex       -- (N/A)
		KEP_EventEncodeNumber(event, 1)                            -- deleteDynamicObjects -- (0 delete all dynamic objects) (1 delete only dynamic objects with NO scripts)
		KEP_EventEncodeString(event, "")                           -- zoneName        -- (N/A)
	elseif (mode == CZMenuMode.IMPORT) then
		KEP_EventSetFilter(event, 2)                               -- (2 import)
		KEP_EventEncodeNumber(event, 1)                            -- showNewSection       -- (1 false -- 0 true)
		KEP_EventEncodeNumber(event, 0)                            -- trackStatus          -- (0 false -- 1 true)
		KEP_EventEncodeNumber(event, 1)                            -- (1 All -- 2 ScriptedOnly --3 NonScriptedOnly)
		KEP_EventEncodeNumber(event, UGCZLPZoneTypes.CURRENT_TYPE) -- importzonetype       -- (3 home / 4 zone/ 5 arena / 6 channel)
		KEP_EventEncodeNumber(event, selZone.zoneIndex)            -- zoneIndex            -- (imports will apply to this zone)
		KEP_EventEncodeNumber(event, 1)                            -- deleteDynamicObjects -- (0 delete all dynamic objects) (1 delete only dynamic objects with NO scripts)
		KEP_EventEncodeString(event, selZone.zoneName)             -- zoneName             -- (for display)
	end
	KEP_EventQueue( event)
end

-- enable/disable screen controls
function toggleScreenControls(blnState)
	-- from top/left to bottom/right
	-- clear all text values
	EditBox_ClearText(gHandles["edZoneName"])
	EditBox_ClearText(gHandles["edtX"])
	EditBox_ClearText(gHandles["edtY"])
	EditBox_ClearText(gHandles["edtZ"])
	EditBox_ClearText(gHandles["edtRot"])

	-- Disable all all controls
	Control_SetEnabled(gHandles["edZoneName"], blnState)
	Control_SetEnabled(gHandles["btnRename"], blnState)
	Control_SetEnabled(gHandles["btnSetDefault"], blnState)
	Control_SetEnabled(gHandles["btnGoZone"], blnState)
	Control_SetEnabled(gHandles["btnGoInstance"], blnState)
	Control_SetEnabled(gHandles["btnNewZone"], blnState)
	Control_SetEnabled(gHandles["btnImportZone"], blnState)
	Control_SetEnabled(gHandles["btnDeleteZone"], blnState)
	Control_SetEnabled(gHandles["edtX"], blnState)
	Control_SetEnabled(gHandles["edtY"], blnState)
	Control_SetEnabled(gHandles["edtZ"], blnState)
	Control_SetEnabled(gHandles["edtRot"], blnState)
	Control_SetEnabled(gHandles["btnSetSpawnPoint"], blnState)
	Control_SetEnabled(gHandles["btnGetCurrentLoc"], blnState)
end

function UIUpdateZonesList()
	g_ZEBusy = true
	local listZones = gHandles["listZones"]
	ListBox_RemoveAllItems(listZones)
	local displayName
	for k,v in pairs(g_zoneDataList) do
		displayName = v.zoneName

		-- do some extra formating and update ui
		if v.isDefault == true then
			displayName = displayName .. " (default)"
		end

		if v.zoneIndex == g_currentZone.Index then
			v.isCurrent = true
			displayName = displayName .. " (current)"

			-- Set selectedZoneIndex if it has not been set
			if (g_selectedZoneIndex < 0) then
				g_selectedZoneIndex = k
			end
		else
			v.isCurrent = false
		end

		if (v.lastOpResult ~= UGCZLPLastOperation.Result.NONE and v.lastOpResult ~= UGCZLPLastOperation.Result.SUCCESS) then
			if (v.lastOpResult == UGCZLPLastOperation.Result.IMPORTING) then
				displayName = displayName .."   ".. UGCZLPLastOpResultToString(v.lastOpResult) .." ".. v.lastOpMsg
			end
		end
		ListBox_AddItem(listZones, displayName, k)
	end
	g_ZEBusy = false

	-- select/highlight proper row in the list
	-- if we have dont have a zoneIndex highlight first element in the list
	if (g_selectedZoneIndex < 0) then
		ListBox_SelectItem(listZones, 0)
	else
		local itemIndex = ListBox_FindItem(listZones, g_selectedZoneIndex)
		if itemIndex~=nil and itemIndex>=0 then
			ListBox_SelectItem(listZones, itemIndex)
		end
	end
	UIUpdateControls()
end

function UIUpdateControls()

	-- turn everything off
	toggleScreenControls(false)

	local selZone = g_zoneDataList[g_selectedZoneIndex]
	if selZone == nil then
		LogError("UIUpdateControls: unknown zoneIndex["..tostring(g_selectedZoneIndex).."]")
		return
	end
	EditBox_SetText(gHandles["edZoneName"], selZone.zoneName, false)

	-- if the zone is being worked on by server
	-- then all user can do is (NewZone -- GetCurrentLocation)
	if ((selZone.lastOpResult == UGCZLPLastOperation.Result.PENDING) or (selZone.lastOpResult == UGCZLPLastOperation.Result.IMPORTING)) then
		Control_SetEnabled(gHandles["btnNewZone"], true)
		Control_SetEnabled(gHandles["btnGetCurrentLoc"], true)
		return
	end

	-- Check wether we are in an instance or zone
	local inZone = true
	if (g_currentZone.Type ~= 4) then
		inZone = false
	end

	-- can not rename lobby or kaneva zones
	if (string.lower(selZone.zoneName) ~= "lobby") then
		if (selZone.isKanevaSupplied == false) then
			Control_SetEnabled(gHandles["edZoneName"], true)
			Control_SetEnabled(gHandles["btnRename"], true)
		end
	end

	-- keep setdefault button off if the zone is already set to default
	if (selZone.isDefault == false) then
		Control_SetEnabled(gHandles["btnSetDefault"], true)
	end

	-- toggle gotoZone/gotoInstance/import based on location
	if (selZone.isCurrent == false) then
		Control_SetEnabled(gHandles["btnGoZone"], true)
		Control_SetEnabled(gHandles["btnGoInstance"], true)
	else
		if (inZone == true) then
			Control_SetEnabled(gHandles["btnGoInstance"], true)
		else
			Control_SetEnabled(gHandles["btnGoZone"], true)
		end
	end

	local bulkZoneOpsState = KEP_bulkZoneOpsEnabled()
	Control_SetEnabled(gHandles["btnNewZone"], bulkZoneOpsState)
	Control_SetEnabled(gHandles["btnImportZone"], bulkZoneOpsState)

	-- can not delete default or current zone
	if selZone.isDefault == false and selZone.isCurrent == false then
		Control_SetEnabled(gHandles["btnDeleteZone"], bulkZoneOpsState)
	end


	-- enable SpawnPoints
	if selZone.isCurrent then
		Control_SetEnabled(gHandles["btnGetCurrentLoc"], true)
		Control_SetEnabled(gHandles["btnSetSpawnPoint"], true)
		Control_SetEnabled(gHandles["edtX"], true)
		Control_SetEnabled(gHandles["edtY"], true)
		Control_SetEnabled(gHandles["edtZ"], true)
		Control_SetEnabled(gHandles["edtRot"], true)
	end

	-- display spawn point data
	if (selZone.spawnPoints ~= nil and #selZone.spawnPoints >= 1) then
		local spawnPoint = selZone.spawnPoints[1]
		EditBox_SetText(gHandles["edtX"],  round(spawnPoint.x, 2), false)
		EditBox_SetText(gHandles["edtY"],  round(spawnPoint.y, 2), false)
		EditBox_SetText(gHandles["edtZ"],  round(spawnPoint.z, 2), false)
		EditBox_SetText(gHandles["edtRot"], round(spawnPoint.r, 2), false)
	else
		Control_SetEnabled(gHandles["btnSetDefault"], false)
		Control_SetEnabled(gHandles["btnGoZone"],    false)
		Control_SetEnabled(gHandles["btnGoInstance"], false)
	end
end

function setSpawnPointsByZoneIndex(zoneIndex, spawnPoints)
	local retval = false
	if (zoneIndex ~= nil) then
		local zoneData = g_zoneDataList[zoneIndex]
		if (zoneData ~= nil) then
			zoneData.spawnPoints = spawnPoints
			retval = true
		else
			LogError("setSpawnPointsByZoneIndex: unknown zoneIndex["..zoneIndex.."]")
			retval = false
		end
	end
	return retval
end

function getSelectedSpawnPoint()
	local zoneData = g_zoneDataList[g_selectedZoneIndex]
	if (zoneData ~= nil) then
		if (zoneData.spawnPoints ~= nil and #zoneData.spawnPoints >= 1) then
			return zoneData.spawnPoints[1]
		end
	else
		LogError("getSelectedSpawnPoint: unknown zoneIndex["..zoneIndex.."]")
	end
	return nil
end

function btnRename_OnButtonClicked(buttonHandle)
	local newZoneName = EditBox_GetText(gHandles["edZoneName"])

	-- No blank zone names (no need for message)
	local fmtZoneName = string.gsub(newZoneName, "^%s*(.-)%s*$", "%1")
	if (fmtZoneName == "") then
		return
	end
	local selZone = g_zoneDataList[g_selectedZoneIndex]
	if (string.lower(selZone.zoneName) == string.lower(fmtZoneName)) then
		return
	end
	UGCZLP_RequestRenameZoneEvent(g_selectedZoneIndex, fmtZoneName)
end

function listZones_OnListBoxSelection(listBoxHandle, x, y, selIndex, selText)

	-- ignore the event if it was fired ListBox_AddItem see UIUpdateZonesList
	if (g_ZEBusy == true) then
		return
	end
	g_selectedZoneIndex = ListBox_GetSelectedItemData(gHandles["listZones"])

	-- get zoneData for zone selected in list
	local zoneData = g_zoneDataList[g_selectedZoneIndex]
	if (zoneData ~= nil and zoneData.spawnPoints ~= nil) then
		-- we alreday have spawnPoint data so just refresh the screen
		UIUpdateControls()
	else
		-- zone does not have any spawnPoints so request them (lazy load)
		UGCZLP_RequestSpawnList(g_selectedZoneIndex)
	end
end

function btnSetDefault_OnButtonClicked(buttonHandle)
	UGCZLP_RequestSetDefaultZoneEvent(g_selectedZoneIndex)
end

function btnGoZone_OnButtonClicked(buttonHandle)
	local zoneData = g_zoneDataList[g_selectedZoneIndex]
	if (zoneData ~= nil) then
		local gameName   = KEP_GetGameName()
		local url = "kaneva://" .. gameName .. "/" .. zoneData.zoneName .. ".zone"
		Log("btnGoZone: '"..tostring(url).."'")
		gotoURL(url)
	else
		LogError("btnGoZone: unknown zoneIndex["..g_selectedZoneIndex.."]")
	end
end

function btnGoInstance_OnButtonClicked(buttonHandle)
	local zoneData = g_zoneDataList[g_selectedZoneIndex]
	if (zoneData ~= nil) then
		local gameName  = KEP_GetGameName()
		local url = "kaneva://" .. gameName .. "/" .. zoneData.zoneName .. ".arena.1"
		Log("btnGoInstance: '"..tostring(url).."'")
		gotoURL(url)
	else
		LogError("btnGoInstance: unknown zoneIndex["..g_selectedZoneIndex.."]")
	end
end

function btnNewZone_OnButtonClicked(buttonHandle)
	if (KEP_bulkZoneOpsEnabled()) then
		if MenuIsClosed("CreateZone.xml") then
			MenuOpenModal("CreateZone.xml")
			setupCreateZoneMenu(CZMenuMode.NEW)
		end
	end
end

function btnImportZone_OnButtonClicked(buttonHandle )
	if (KEP_bulkZoneOpsEnabled()) then
		if MenuIsClosed("CreateZone.xml") then
			MenuOpenModal("CreateZone.xml")
			setupCreateZoneMenu(CZMenuMode.IMPORT)
		end
	end
end

function btnDeleteZone_OnButtonClicked(buttonHandle)
	if (KEP_bulkZoneOpsEnabled()) then
		UGCZLP_RequestDeleteZoneEvent(g_selectedZoneIndex, UGCZLPDynamicObjects.DELETE_ALL)
	end
end

function btnGetCurrentLoc_OnButtonClicked(buttonHandle)
	local x,y,z,rot = KEP_GetPlayerPosition()
	EditBox_SetText(gHandles["edtX"],  round(x, 2),  false)
	EditBox_SetText(gHandles["edtY"],  round(y, 2),  false)
	EditBox_SetText(gHandles["edtZ"],  round(z, 2),  false)
	EditBox_SetText(gHandles["edtRot"], round(rot, 2), false)
end

function btnSetSpawnPoint_OnButtonClicked(buttonHandle)
	-- fetch data from the screen
	local x = EditBox_GetText(gHandles["edtX"])
	local y = EditBox_GetText(gHandles["edtY"])
	local z = EditBox_GetText(gHandles["edtZ"])
	local r = EditBox_GetText(gHandles["edtRot"])
	if tonumber(x) == nil or tonumber(y) == nil or tonumber(z) == nil or tonumber(r) == nil then
		KEP_MessageBox("You must use numbers for spawn point position")
		Control_SetFocus(gHandles["edtX"])
		return
	end

	-- fetch zone to reuse zoneName as spawnPointName
	local selZone = g_zoneDataList[g_selectedZoneIndex]
	if selZone == nil then
		LogError("SetSpawnPoint: unknown zoneIndex["..tostring(g_selectedZoneIndex).."]")
		return
	end

	-- If we have spawnPoint data then its an edit
	-- otherwise its an add
	local spawnPoint = getSelectedSpawnPoint()
	if (spawnPoint ~= nil) then
		UGCZLP_RequestUpdateSpawnEvent(spawnPoint.id, g_selectedZoneIndex, selZone.zoneName, x, y, z, r)
	else
		UGCZLP_RequestAddSpawnEvent(g_selectedZoneIndex, selZone.zoneName, false, x, y, z, r)
	end
end

--------------------------------------------------
-- hooks for UGCZoneLiteProxy replies
--------------------------------------------------

function UGCZLP_ReplyZoneList(blnRetval, zoneList)
	if (blnRetval == false) then
		LogError("UGCZLP_ReplyZoneList: request returned error")
		return
	end
	g_zoneDataList = {}

	-- filter out all KanevaZones because we dont want to display them
	for i,v in ipairs(zoneList) do
		if (v.isKanevaSupplied == false) then
			table.insert(g_zoneDataList, v.zoneIndex, v)
		end
	end

	-- update our UI
	UIUpdateZonesList()
end

function UGCZLP_ReplySpawnList(blnRetval, zoneIndex, spawnPoints)
	if (blnRetval == false) then
		LogError("UGCZLP_ReplySpawnList: request returned error for zoneIndex["..tostring(zoneIndex).."]")
		return
	end

	-- associate these spawnPoints with the zone
	setSpawnPointsByZoneIndex(zoneIndex, spawnPoints)

	-- refresh screen
	UIUpdateControls()
end

function UGCZLP_ReplyRenameZoneEvent(blnRetval, zoneIndex, newZoneName, strMsg, lastOpResult, lastOpMsg)
	-- update our local cache with results
	local zoneData = g_zoneDataList[zoneIndex]
	if (zoneData ~= nil) then
		if (blnRetval == true) then
			zoneData.zoneName     = newZoneName
			zoneData.lastOpResult = lastOpResult
			zoneData.lastOpMsg    = lastOpMsg
		else
			LogError("UGCZLP_ReplyRenameZoneEvent: request returned error for zoneIndex["..tostring(zoneIndex).."] newZoneName["..tostring(newZoneName).."] msg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
			zoneData.lastOpResult = lastOpResult
			zoneData.lastOpMsg    = lastOpMsg
		end

		-- refresh screen
		UIUpdateZonesList()
	else
		LogError("UGCZLP_ReplyRenameZoneEvent: unknown zoneIndex["..tostring(zoneIndex).."]")
	end
end

function UGCZLP_ReplySetDefaultZoneEvent(blnRetval, zoneIndex, evtMsg, lastOpResult, lastOpMsg)
	-- update our local cache with results
	for k,v in pairs(g_zoneDataList) do
		if (k == zoneIndex) then
			if (blnRetval == true) then
				v.isDefault = true
				v.lastOpResult = lastOpResult
				v.lastOpMsg    = lastOpMsg
			else
				LogError("UGCZLP_ReplySetDefaultZoneEvent: request returned error for zoneIndex["..tostring(zoneIndex).."] msg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
				v.lastOpResult = lastOpResult
				v.lastOpMsg    = lastOpMsg
				break
			end
		else
			if (blnRetval == true) then
				v.isDefault = false
			end
		end
	end

	-- refresh screen
	UIUpdateZonesList()
end

function UGCZLP_ReplyDeleteZoneEvent(blnRetval, zoneIndex, strMsg, lastOpResult, lastOpMsg)
	-- update our local cache with results
	local zoneData = g_zoneDataList[zoneIndex]
	if (zoneData ~= nil) then

		if (blnRetval == true) then
			g_zoneDataList[zoneIndex] = nil
			g_selectedZoneIndex = -1
		else
			LogError("UGCZLP_ReplyDeleteZoneEvent: request returned error for zoneIndex["..tostring(zoneIndex).."] msg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
			zoneData.lastOpResult = lastOpResult
			zoneData.lastOpMsg    = lastOpMsg
		end
	else
		LogError("UGCZLP_ReplyDeleteZoneEvent: unknown zoneIndex["..tostring(zoneIndex).."]")
	end

	-- refresh screen
	UIUpdateZonesList()
end

function UGCZLP_ReplyUpdateSpawnEvent(blnRetval, zoneIndex, spawnId, strMsg, lastOpResult, lastOpMsg)
	if (blnRetval == false) then
		LogError("UGCZLP_ReplyUpdateSpawnEvent: request update spawn returned error for zoneIndex["..tostring(zoneIndex).."] spawnId["..tostring(spawnId).."] msg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
		return
	end

	-- delete all spawns and rerequest
	local zoneData = g_zoneDataList[zoneIndex]
	if (zoneData ~= nil) then
		zoneData.spawnPoints = nil
		UGCZLP_RequestSpawnList(zoneIndex)
	else
		LogError("UGCZLP_ReplyDeleteZoneEvent: unknown zoneIndex["..tostring(zoneIndex).."]")
	end
	-- refresh screen
	-- No need to refresh this will get done by the UGCZLP_ReplySpawnList
end

function UGCZLP_ReplyImportZoneEvent(blnRetval, zoneIndex, newZoneIndex, strMsg, lastOpResult, lastOpMsg)
	local zoneData = g_zoneDataList[zoneIndex]
	if (zoneData ~= nil) then
		if (blnRetval == true) then
			zoneData.zoneIndex    = newZoneIndex
			zoneData.lastOpResult = lastOpResult
			zoneData.lastOpMsg    = lastOpMsg
			g_zoneDataList[zoneIndex] = nil
			g_zoneDataList[newZoneIndex] = zoneData
			if (g_selectedZoneIndex == zoneIndex) then
				g_selectedZoneIndex = newZoneIndex
			end
		else
			LogError("UGCZLP_ReplyImportZoneEvent: request returned error for zoneIndex["..tostring(zoneIndex).."] msg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
			zoneData.lastOpResult = lastOpResult
			zoneData.lastOpMsg    = lastOpMsg
		end
	else
		LogError("UGCZLP_ReplyImportZoneEvent: unknown zoneIndex["..tostring(zoneIndex).."]")
	end

	-- refresh screen
	UIUpdateZonesList()
end

function UGCZLP_ReplyZoneStateChangeEvent(zoneIndex, lastOpResult, lastOpMsg)
	local zoneData = g_zoneDataList[zoneIndex]
	if (zoneData ~= nil) then
		zoneData.lastOpResult = lastOpResult
		zoneData.lastOpMsg    = lastOpMsg
		UIUpdateZonesList()
	end
end

function UGCZLP_ReplyCreateZoneEvent(blnRetval, zoneIndex, newZoneName, strMsg, lastOpResult, lastOpMsg)
	UGCZLP_RequestZoneList()
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate(dialogHandle)

	-- Disable entire screen
	toggleScreenControls(false)

	-- set our current index
	local zi = KEP_GetCurrentZoneIndex()
	g_currentZone = {}
	g_currentZone.Index = InstanceId_GetId(zi)
	g_currentZone.Type  = InstanceId_GetType(zi)

	UGCZLP_RequestZoneList()

	local listHandle = gHandles["listZones"]
	local scrollBarHandle = ListBox_GetScrollBar(listHandle)
	setupScrollBarDefaultTextures(scrollBarHandle)

	local gamename = KEP_GetGameName()
	Static_SetText(gHandles["menuTitle"], gamename .. " - Zone Editor")
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy(dialogHandle)
end
