--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_FlagHandler.lua
-- Handles the display of flag region components
-------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")

dofile("..\\MenuScripts\\Framework_CullingHelper.lua")
local CullingHelper = _G.CullingHelper

---------------------
-- LOCAL VARIABLES --
---------------------

-- Game item cache
local m_gameItems = {}
local m_initGameItemCache = false

local DEFAULT_FLAG_RADIUS = 250
local DEFAULT_FLAG_HEIGHT = 50
local DEFAULT_FLAG_WIDTH = 50
local DEFAULT_FLAG_DEPTH = 50

-- maintain a list of all flags to toggle on/off

-- flaghelper m_claimFlags

-- track the currently selected flag to keep ON when toggled off

--------------------
-- CORE FUNCTIONS --
--------------------

-- Called when the menu is created
function onCreate()
	KEP_EventRegister("FLAG_REGION_DISPLAY_UPDATE", KEP.MED_PRIO )

	KEP_EventRegisterHandler( "updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "flagRegionDisplayUpdateHandler", "FLAG_REGION_DISPLAY_UPDATE", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "updateFlagRegionColorHandler", "UPDATE_FLAG_REGION_COLOR", KEP.MED_PRIO )

	requestInventory(GAME_PID)

	CullingHelper.Init()
end

-- Called when the menu is destroyed
function onDestroy()
	CullingHelper.DeInit()
end

----------------
-- USER INPUT --
----------------

-- Called when a key is pressed
function Dialog_OnKeyDown(dialogHandle, key, shift, ctrl, alt)
	-- log("\n key: "..tostring(key))
	if (key == 220) then -- Backslash \ key
		-- Toggle display of all flags
	else
		--CullingHelper.HandleInput(key, shift, ctrl, alt)
	end
end

--------------------
-- EVENT HANDLERS --
--------------------

function flagRegionDisplayUpdateHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid) -- handler parameters must match these!!!
	local PID = KEP_EventDecodeNumber(tEvent)
	local selected = KEP_EventDecodeNumber(tEvent)
	local UNID = -1
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		UNID = KEP_EventDecodeNumber(tEvent)
	end
	updateFlagRegion(PID, selected, UNID)
end

-- Get the game item cache (update)
function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

-- Get the game item cache (full)
function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems

	m_initGameItemCache = true
end

function updateFlagRegionColorHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local red = KEP_EventDecodeNumber(tEvent) or 0
	local green = KEP_EventDecodeNumber(tEvent) or 255
	local blue = KEP_EventDecodeNumber(tEvent) or 0
	CullingHelper.SetBoxColor(red, green, blue)
end

---------------------
-- LOCAL FUNCTIONS --
---------------------

function updateFlagRegion(iPID, iSelected, iUNID)
	if iPID then
		local UNID = 0
		if iUNID and iUNID ~= -1 then
			UNID = iUNID
		else
			_, _, _, _, _, _, _, _, _, _, _, _, _, UNID, _ = KEP_DynamicObjectGetInfo(iPID)
		end
		if UNID and UNID > 0 then
			-- Get game item properties
			local flagItem = m_gameItems[tostring(UNID)]
			-- Check that the item is a flag
			if flagItem and (flagItem.behavior == "claim" or flagItem.behavior == "flag") then -- Check for other types of flags?
				-- Toggle when selected, also need a "global" toggle linked to the "\" key
				CullingHelper.EnableBoxDisplay(iSelected==1)
				if not iSelected then
					return
				else
					local flagPos = {x = 0, y = 0, z = 0}
					flagPos.x, flagPos.y, flagPos.z = KEP_DynamicObjectGetPositionAnim(iPID)
					CullingHelper.RecenterBox(flagPos.x, flagPos.y, flagPos.z)
					local height = DEFAULT_FLAG_HEIGHT
					local width = DEFAULT_FLAG_WIDTH
					local depth = DEFAULT_FLAG_DEPTH

					if flagItem.behaviorParams then
						height = flagItem.behaviorParams.height
						width = flagItem.behaviorParams.width
						depth = flagItem.behaviorParams.depth 
					end
					CullingHelper.SetSizeOfBox(depth, height, width)
					-- NOTE: Color no longer user-defined, handled based on build status
					-- local red = flagItem.regionColorRed or 0
					-- local green = flagItem.regionColorGreen or 0
					-- local blue = flagItem.regionColorBlue or 0
					-- local red = flagItem.regionColorRed or 0
					-- local green = flagItem.regionColorGreen or 0
					-- local blue = flagItem.regionColorBlue or 0
					-- CullingHelper.SetBoxColor(red, green, blue)
				end
			end
		else
			CullingHelper.EnableBoxDisplay(false)
		end
		
		-- CullingHelper.RecenterBox(flagPos.x, flagPos.y, flagPos.z)
	end
end
