--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Repair.lua
-- Author: PJD 2015
-- Client menu script for Repair Station feature
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_DragDropHelper.lua")
dofile("Framework_ActorHelper.lua")

local calculateCost --(curValue)
local updateInventoryCounts --()

local TOOLTIP_TABLE = {header = "name", body = "description"}
local DEFAULT_MAX_DURABILITY = 1000 -- 1,000 hits or attacks per weapon/armor
local DURABILITY_BY_LEVEL = { [1]=10, [2]=100, [3]=250, [4]=500, [5]=1000 }
local BUTTON_WIDTH = 82

local DURABILITY_COLORS = {GREEN  = {19, 179, 20, 185},
						   YELLOW = {31, 179, 32, 185},
						   RED 	  = {25, 179, 26, 185}
						  }

local COLORS_VALUES =	{	["Common"] = {a = 255, r = 255, g = 255, b = 255},
							["Rare"] = {a = 255, r = 0, g = 150, b = 255},
							["Very Rare"] = {a = 255, r = 174, g = 0, b = 255},
							["Extremely Rare"] = {a = 255, r = 255, g = 144, b = 0}
						}
						
local ITEM_RARITY_BG = {	["Common"] = {left = 208, right = 292, top = 167, bottom = 251},
							["Rare"] = {left = 293, right = 377, top = 167, bottom = 251},
							["Very Rare"] = {left = 378, right = 462, top = 167, bottom = 251},
							["Extremely Rare"] = {left = 378, right = 462, top = 82, bottom = 166},
							["Never"] = {left = 378, right = 462, top = 82, bottom = 166}
						}
						
local ITEM_LEVEL_COORDS = {
							{left = 0, right = 15, top = 146, bottom = 161},
							{left = 16, right = 31, top = 146, bottom = 161},
							{left = 32, right = 47, top = 146, bottom = 161},
							{left = 0, right = 15, top = 162, bottom = 177},
							{left = 16, right = 31, top = 162, bottom = 177}
}
						
local ITEM_LEVEL_RARITY_OFFSET = {["Common"] = 0, ["Rare"] = 48, ["Very Rare"] = 96, ["Extremely Rare"] = 144, ["Never"] = 144}

local OCCUPIED_ANIMATION_GLID = 4546763

local SLIDER_NAME = "sldrRepair"
local CUR_DURABILITY_NAME = "stcCurrentDurability"
local CUR_COST_NAME = "stcCurrentCost"
local ITEM_DURABILITY_NAME = "stcItemDurability"
local ITEM_IMAGE_NAME_1 = "imgItem1"
local ITEM_IMAGE_NAME_2 = "imgItem2"
local ITEM_BG_NAME_1 = "imgItemBG1"
local ITEM_BG_NAME_2 = "imgItemBG2"
local BG_IMAGE_NAME = "imgDialogBG"
local ITEM_BTN_NAME_1 = "btnItem1"
local ITEM_BTN_NAME_2 = "btnItem2"
local ITEM_NAME = "stcItemName"
local REPAIR_BUTTON = "btnRepair"
local BACKPACK_COUNT_NAME = "stcBackpackCount"
local REPAIR_AMOUNT_NAME = "stcRepairAmount"
local REPAIR_ITEM_NAME = "stcRepairName"
local MOVE_TO_REPAIR_NAME = "stcMoveToRepair"
local REPAIR_DETAILS_NAME = "stcRepairDetails"
local DROP_BUTTON_NAME = "btnItemDrop1"
local ITEM_LEVEL_NAME = "imgItemLevel1"

g_displayTable = {}

local STEP_VALUE = 10
local REPAIR_UNID = 124
local REPAIR_TIME = 5

local m_startValue = 0
local m_nextValue = 0
local m_rangeMax = nil
local m_sliderHandle = nil
local m_bPressed = false
local m_repairItem = nil
local m_repairTable = {}
local m_repairUNID = "0"
local m_requiredCount = 0
local m_inventory = {}
local m_requestHandle = nil
local m_overButton = false
local m_bRepairing = false
local m_repairTimer = 0
local m_repairPID = nil
local m_gameItems = nil

function onCreate()
	CloseAllActorMenus()
	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")				
	end

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "player")
	KEP_EventQueue(ev)
	
	applyImage(gHandles[ITEM_IMAGE_NAME_1], 0)
	applyImage(gHandles[ITEM_IMAGE_NAME_2], 0)
	
	m_sliderHandle = gHandles[SLIDER_NAME]
	m_rangeMax = Slider_GetRangeMax(m_sliderHandle)
	Slider_SetValue(m_sliderHandle, m_startValue)
	Static_SetText(gHandles[ITEM_DURABILITY_NAME], tostring(m_startValue).."% Durability")
	Static_SetText(gHandles[CUR_DURABILITY_NAME], tostring(m_startValue).."% Durability")
	Static_SetText(gHandles[REPAIR_AMOUNT_NAME], "repairs "..tostring(STEP_VALUE).."%")

	Events.registerHandler("UPDATE_REPAIR_UNID", updateRepairUNID)
	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)
	
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler("onReturnElement", "DRAG_DROP_RETURN_ELEMENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCustomDrop", "DRAG_DROP_CUSTOM_DROP", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("startDragDrop", "DRAG_DROP_START", KEP.HIGH_PRIO)
	
	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()	
	InventoryHelper.initializeInventory("g_displayTable", true, 2)
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")

	requestInventory(GAME_PID)
	requestInventory(INVENTORY_PID)
	
	g_displayTable[1] = deepCopy(EMPTY_ITEM)
	g_displayTable[2] = {}
	displayRepairItem()
	calculateCost(m_startValue)
	
	DragDrop.initializeDragDrop(0, 1, 0, INVENTORY_PID)
	
	Control_SetEnabled(gHandles[BG_IMAGE_NAME], false)
	Control_SetVisible(gHandles[BG_IMAGE_NAME], false)
	
	Control_SetVisible(gHandles[REPAIR_AMOUNT_NAME], false)
	Control_SetVisible(gHandles[REPAIR_ITEM_NAME], false)
	Control_SetVisible(gHandles[BACKPACK_COUNT_NAME], false)
end

function onDestroy()
	KEP_SetCurrentAnimationByGLID(0)
	InventoryHelper.destroy()
	returnItem()
end

function closeRepair(event)
	if not MenuIsOpen("UnifiedInventory") then
		MenuCloseThis()
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_inventory = decompileInventory(tEvent)
	updateInventoryCounts()
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if m_inventory[updateIndex] then
		m_inventory[updateIndex] = updateItem
		updateInventoryCounts()
	end
end

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
	displayRepairTool()
end

-- On mouse-over repair drop button
function DragDrop.buttonOnMouseEnterHandler(btnHandle)
	m_requestHandle = btnHandle
	local requestEvent = KEP_EventCreate("DRAG_DROP_REQUEST_ELEMENT")
	KEP_EventSetFilter(requestEvent, 4)
	KEP_EventQueue(requestEvent)
	
	m_overButton = true
end

function DragDrop.buttonOnMouseLeaveHandler(btnHandle)
	m_overButton = false
	DragDrop.buttonOnMouseLeave(btnHandle)
end

function updateRepairUNID(event)
	if m_repairPID and m_repairTable and m_repairTable.item and m_repairTable.item.properties then return end
	m_repairPID = event.PID
	m_repairUNID = tostring(event.repairUNID)
	displayBaseElements()

	displayRepairTool()
end

-- Returned from drag drop handler
function onReturnElement(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local elementExists = KEP_EventDecodeString(tEvent) == "true"
	local repairItem = {}
	if elementExists and m_requestHandle and m_overButton then
		local repairItemString = KEP_EventDecodeString(tEvent)
		repairItem = Events.decode(repairItemString)
		local durabilityType = repairItem.properties.durabilityType or "repairable"

		m_canDrop = false
		if (repairItem.properties.itemType == ITEM_TYPES.WEAPON or repairItem.properties.itemType == ITEM_TYPES.ARMOR or repairItem.properties.itemType == ITEM_TYPES.TOOL) and repairItem.instanceData and durabilityType == "repairable" and tonumber(repairItem.instanceData.durability) then
			m_canDrop = true
		elseif durabilityType == "indestructible" then
			local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeString(statusEvent, "You cannot repair an indestructible object.")
			KEP_EventEncodeNumber(statusEvent, 3)
			KEP_EventEncodeNumber(statusEvent, 255)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventSetFilter(statusEvent, 4)
			KEP_EventQueue(statusEvent)
		elseif durabilityType == "limited" then
			local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeString(statusEvent, "You cannot repair an object with limited uses.")
			KEP_EventEncodeNumber(statusEvent, 3)
			KEP_EventEncodeNumber(statusEvent, 255)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventSetFilter(statusEvent, 4)
			KEP_EventQueue(statusEvent)
			-- You cannot repair an item with limited uses.
		else
			local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeString(statusEvent, "You can only repair weapons, armor, or tools.")
			KEP_EventEncodeNumber(statusEvent, 3)
			KEP_EventEncodeNumber(statusEvent, 255)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventSetFilter(statusEvent, 4)
			KEP_EventQueue(statusEvent)
		end

		DragDrop.buttonOnMouseEnter(m_requestHandle, m_canDrop)
	end
	m_requestHandle = nil
end

-- Returned from custom drag drop handler
function onCustomDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local dropIndex = KEP_EventDecodeNumber(tEvent)

	if dropIndex ~= 0 and m_overButton then
		m_repairTable = {}
		m_repairTable.item = deepCopy(decompileInventoryItem(tEvent))
		m_repairTable.PID = KEP_EventDecodeNumber(tEvent)
		m_repairTable.origIndex = KEP_EventDecodeNumber(tEvent)
		if m_canDrop then
			g_displayTable[1] = deepCopy(m_repairTable.item)
			displayRepairItem()
			calculateCost(m_startValue)
		else
			returnItem()
		end
	end
	
	m_canDrop = false
	Control_SetEnabled(gHandles[BG_IMAGE_NAME], false)
	Control_SetVisible(gHandles[BG_IMAGE_NAME], false)
end

function startDragDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	Control_SetEnabled(gHandles[BG_IMAGE_NAME], true)
	Control_SetVisible(gHandles[BG_IMAGE_NAME], true)
	
	clearRepairItem()
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	m_bPressed = true
end

function Dialog_OnLButtonUp(dialogHandle, x, y)
	if m_bPressed then
		m_bPressed = false
	end
end

function Dialog_OnRender(dialogHandle, fTime)
	if m_bRepairing then
		m_repairTimer = m_repairTimer + fTime
		if m_repairTimer >= REPAIR_TIME then
			m_repairTimer = 0
			m_repairTable.item.instanceData.durability = m_repairTable.item.instanceData.durability + (DEFAULT_MAX_DURABILITY*(STEP_VALUE/100))
			if m_repairTable.item.instanceData.durability > DEFAULT_MAX_DURABILITY then
				m_repairTable.item.instanceData.durability = DEFAULT_MAX_DURABILITY
			end
			g_displayTable[1] = deepCopy(m_repairTable.item)
			displayRepairItem()
			consumeRepairRequiredItem()
			m_repairCost = m_repairCost - 1
			Static_SetText(gHandles[CUR_COST_NAME], "Cost "..tostring(m_repairCost))
			if m_repairCost == 0 then
				KEP_SetCurrentAnimationByGLID(0)
				m_bRepairing = false
				displayRepairElements()
				calculateCost(m_startValue)
			end
		else
			-- Console.Write(Console.INFO, "Setting animation to "..tostring(OCCUPIED_ANIMATION_GLID))
			KEP_SetCurrentAnimationByGLID(OCCUPIED_ANIMATION_GLID)
		end
		Control_SetSize(gHandles["imgItemProgress1"], BUTTON_WIDTH*(m_repairTimer/REPAIR_TIME), BUTTON_WIDTH)
	elseif m_bPressed and Dialog_IsLMouseDown(dialogHandle) == 0 then
		m_bPressed = false
		local sliderValue = Slider_GetValue(m_sliderHandle)
		local stepMod = math.fmod(sliderValue - m_startValue, STEP_VALUE)
		if sliderValue >= m_rangeMax then
			m_nextValue = m_rangeMax
		elseif stepMod == 0 then
			m_nextValue = sliderValue
		elseif stepMod >= STEP_VALUE/2 then
			m_nextValue = sliderValue + (STEP_VALUE - stepMod)
		else
			m_nextValue = sliderValue - stepMod
		end
		Slider_SetValue(m_sliderHandle, m_nextValue)
		calculateCost(m_nextValue)
	elseif not m_bPressed and Slider_GetValue(m_sliderHandle) ~= m_nextValue then
		Slider_SetValue(m_sliderHandle, m_nextValue)
		calculateCost(m_nextValue)
	end
end

function sldrRepair_OnSliderValueChanged(sliderHandle, sliderValue)
	calculateCost(sliderValue)
	if m_nextValue == sliderValue then
		return
	elseif sliderValue < m_startValue then
		m_nextValue = m_startValue
		Slider_SetValue(sliderHandle, m_startValue)
		calculateCost(m_startValue)
	else
		local stepMod = math.fmod(sliderValue - m_startValue, STEP_VALUE)
		if sliderValue >= m_rangeMax then
			m_nextValue = m_rangeMax
		elseif stepMod == 0 then
			m_nextValue = sliderValue
		elseif sliderValue > m_nextValue then
			m_nextValue = sliderValue + (STEP_VALUE - stepMod)
		else
			m_nextValue = sliderValue - stepMod
		end
	end
end

function btnCancel_OnButtonClicked(buttonHandle)
	if m_bRepairing then
		KEP_SetCurrentAnimationByGLID(0)
		m_bRepairing = false
		m_repairTimer = 0
		Control_SetSize(gHandles["imgItemProgress1"], BUTTON_WIDTH*(m_repairTimer/REPAIR_TIME), BUTTON_WIDTH)
		displayRepairItem()
		calculateCost(m_startValue)
		displayRepairElements()
	elseif g_displayTable[1].properties.GLID ~= 0 then
		clearRepairItem()
	else
		MenuCloseThis()
	end
end

function btnRepair_OnButtonClicked(buttonHandle)
	if not m_bRepairing and m_repairTable and m_repairTable.item and m_repairCost > 0 then
		m_bRepairing = true
		displayRepairingElements()
	end
end

function displayRepairTool()

	if m_repairUNID == nil or m_gameItems == nil then return end

	m_repairItem = {}
	m_repairItem.properties = m_gameItems[tostring(m_repairUNID)]
	m_repairItem.count = 0
	m_repairItem.UNID = tonumber(UNID)
	g_displayTable[2] = m_repairItem
	
	local rarity = m_repairItem.properties.rarity or "Common"
	element = Image_GetDisplayElement(gHandles[ITEM_BG_NAME_2])
	Element_SetCoords(element, ITEM_RARITY_BG[rarity].left, ITEM_RARITY_BG[rarity].top, ITEM_RARITY_BG[rarity].right, ITEM_RARITY_BG[rarity].bottom)
	formatTooltips(m_repairItem)
	applyImage(gHandles[ITEM_IMAGE_NAME_2], m_repairItem.properties.GLID)
	Static_SetText(gHandles[REPAIR_ITEM_NAME], "1 "..tostring(m_repairItem.properties.name))
					
	Control_SetVisible(gHandles[REPAIR_AMOUNT_NAME], true)
	Control_SetVisible(gHandles[REPAIR_ITEM_NAME], true)
	Control_SetVisible(gHandles[BACKPACK_COUNT_NAME], true)	
end

function consumeRepairRequiredItem()
	if m_repairUNID == "0" then return end
	for i = 1, #m_inventory do
		if tostring(m_inventory[i].UNID) == m_repairUNID then
			m_inventory[i].count = m_inventory[i].count - 1
			if m_inventory[i].count <= 0 then
				removeItem(i, INVENTORY_PID)
			else
				updateItem(i, INVENTORY_PID, m_inventory[i])
			end
			updateDirty()
			return
		end
	end
end

function returnItem()
	if m_repairTable and m_repairTable.item and m_repairTable.item.properties then
		updateItem(m_repairTable.origIndex, m_repairTable.PID, m_repairTable.item, true)
		updateDirty()

		m_repairTable.item = {}
		m_repairTable.PID = 0
		m_repairTable.origIndex = 0
	end
end

function displayRepairItem()
	local displayItem = g_displayTable[1]
	if displayItem.properties then
		if displayItem.count <= 0 then
			displayItem.hide = true
			
			displayBaseElements()
			
			Control_SetVisible(gHandles["imgDurabilityBar1"], false)
			Control_SetVisible(gHandles["imgDurabilityBG1"], false)
			Control_SetVisible(gHandles["imgDurabilityGradiant1"], false)
			Control_SetVisible(gHandles["imgItemBroken1"], false)
			Control_SetVisible(gHandles["imgItemOverlay1"], false)
		else
			local durability = displayItem.instanceData.durability
			local durabilityPercentage = math.min((durability/DEFAULT_MAX_DURABILITY)*100, 100)

			m_nextValue = durabilityPercentage
			m_startValue = durabilityPercentage
			
			-- Set bar size
			Control_SetSize(gHandles["imgDurabilityBar1"], durabilityPercentage/100 * BUTTON_WIDTH, 7)
			
			-- Set bar color
			local durColor = getDurabilityColor(durabilityPercentage/100)
			Element_SetCoords(Image_GetDisplayElement(gHandles["imgDurabilityBar1"]), unpack(durColor))
			
			-- Set bar's visible
			Control_SetVisible(gHandles["imgDurabilityBar1"], true)
			Control_SetVisible(gHandles["imgDurabilityBG1"], true)
			Control_SetVisible(gHandles["imgDurabilityGradiant1"], true)
			
			-- Is the item broken?
			Control_SetVisible(gHandles["imgItemBroken1"], durability <= 0)
			Control_SetVisible(gHandles["imgItemOverlay1"], durability <= 0)
			
			Slider_SetValue(m_sliderHandle, m_startValue)
			
			Static_SetText(gHandles[ITEM_NAME], tostring(displayItem.properties.name))
			Static_SetText(gHandles[ITEM_DURABILITY_NAME], tostring(m_startValue).."% Durability")
			
			if not m_bRepairing then
				Static_SetText(gHandles[CUR_DURABILITY_NAME], tostring(m_startValue).."% Durability")
				displayRepairElements()
			end
		end
		formatTooltips(displayItem)
		applyImage(gHandles[ITEM_IMAGE_NAME_1],displayItem.properties.GLID)
		
		local element = Image_GetDisplayElement(gHandles[ITEM_LEVEL_NAME])
		local itemLevel = displayItem.properties.level or 0
		
		if itemLevel ~= 0 then
			Control_SetVisible(gHandles[ITEM_LEVEL_NAME], true)
			local rarityOffset = ITEM_LEVEL_RARITY_OFFSET[displayItem.properties.rarity] or 0
			Element_SetCoords(element, ITEM_LEVEL_COORDS[itemLevel].left + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].top, ITEM_LEVEL_COORDS[itemLevel].right + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].bottom)
		else
			Control_SetVisible(gHandles[ITEM_LEVEL_NAME], false)
		end
		
		local rarity = displayItem.properties.rarity or "Common"
		element = Image_GetDisplayElement(gHandles[ITEM_BG_NAME_1])
		Element_SetCoords(element, ITEM_RARITY_BG[rarity].left, ITEM_RARITY_BG[rarity].top, ITEM_RARITY_BG[rarity].right, ITEM_RARITY_BG[rarity].bottom)
	end
	
	InventoryHelper.setNameplates("name", "hide")
	InventoryHelper.setQuantities("count")
end

function clearRepairItem()
	if g_displayTable[1].properties.GLID ~= 0 then
		returnItem()
		g_displayTable[1] = deepCopy(EMPTY_ITEM)
		
		m_startValue = 0
		m_nextValue = 0
		Slider_SetValue(m_sliderHandle, m_startValue)	
		
		displayRepairItem()
		calculateCost(m_startValue)
	end
end

function updateInventoryCounts()
	local requiredCount = 0
	
	for index, item in pairs(m_inventory) do
		if tostring(item.UNID) == m_repairUNID then
			requiredCount = requiredCount + item.count
		end
	end
	
	Static_SetText(gHandles[BACKPACK_COUNT_NAME], tostring(requiredCount).." in backpack")
	m_requiredCount = requiredCount
end

function calculateCost(curValue)
	local stepMod = math.fmod(curValue - m_startValue, STEP_VALUE)
	if stepMod == 0 then
		curValue = curValue
	elseif stepMod >= STEP_VALUE/2 or curValue == m_rangeMax then
		curValue = curValue + (STEP_VALUE - stepMod)
	else
		curValue = curValue - stepMod
	end
	if curValue >= m_rangeMax then
		curValue = m_rangeMax
	end
	Static_SetText(gHandles[CUR_DURABILITY_NAME], tostring(curValue).."% Durability")
	local cost = math.floor((curValue - m_startValue)/STEP_VALUE)
	if math.fmod(curValue - m_startValue, STEP_VALUE) ~= 0 then
		cost = cost + 1
	end
	Static_SetText(gHandles[CUR_COST_NAME], "Cost "..tostring(cost))
	m_repairCost = cost
	if (cost > m_requiredCount and m_repairUNID ~= "0") or cost == 0 then
		Control_SetEnabled(gHandles[REPAIR_BUTTON], false)
		if cost == 0 then
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles[CUR_COST_NAME])), ControlStates.DXUT_STATE_NORMAL, {a=255, r=200, g=200, b=200})
		else
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles[CUR_COST_NAME])), ControlStates.DXUT_STATE_NORMAL, {a=255, r=255, g=0, b=0})
		end
	else
		Control_SetEnabled(gHandles[REPAIR_BUTTON], true)
		BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles[CUR_COST_NAME])), ControlStates.DXUT_STATE_NORMAL, {a=255, r=200, g=200, b=200})
	end
end

function formatTooltips(item)
	if item and item.properties then
		item.tooltip = {}

		if item.properties.rarity then
			item.tooltip["header"] = {label = item.properties.name,
									  color = COLORS_VALUES[item.properties.rarity]}
		end

		if item.properties.description then
			item.tooltip["body"] = {label = "\"" .. item.properties.description .. "\""}

		end

		if item.properties.itemType == ITEM_TYPES.WEAPON then
			item.tooltip["footer1"] = {label = "Weapon Level: "..tostring(item.properties.level)}
			item.tooltip["footer2"] = {label = "Damage: "..tostring(item.properties.damage * item.properties.bonusDamage)}
			item.tooltip["footer3"] = {label = "Range: "..tostring(item.properties.range)}

			if item.instanceData then
				local itemLevel = item.properties.level
				local durability = item.instanceData.durability
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				item.tooltip["durability"] = {label = "Durability: "..tostring(durabilityPercentage).."%",
										  durability = durabilityPercentage}
			end
		elseif item.properties.itemType == ITEM_TYPES.TOOL then
			if item.instanceData then
				
				item.tooltip["footer1"] = {label = "Tool Level: "..tostring(item.properties.level)}
				
				local itemLevel = item.properties.level
				local durability = item.instanceData.durability
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				item.tooltip["durability"] = {label = "Durability: "..tostring(durabilityPercentage).."%",
										  durability = durabilityPercentage}
			end
		elseif item.properties.itemType == ITEM_TYPES.ARMOR then
			if item.instanceData then
				
				item.tooltip["footer1"] = {label = "Armor Level: "..tostring(item.properties.level)}
				item.tooltip["footer2"] = {label = "Slot: "..tostring(item.properties.slotType:gsub("^%l", string.upper))}

				local armorRating
				if item.properties.bonusHealth then
					armorRating = item.properties.armorRating * item.properties.bonusHealth
				else
					armorRating = item.properties.armorRating * 3
				end

				item.tooltip["footer3"] = {label = "Armor Rating: "..tostring(armorRating)}
			
				local itemLevel = item.properties.level
				local durability = item.instanceData.durability
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				item.tooltip["durability"] = {label = "Durability: "..tostring(durabilityPercentage).."%",
										  durability = durabilityPercentage}
			end
		elseif item.properties.itemType == ITEM_TYPES.CONSUMABLE then


			if item.properties.consumeType == "energy" then
				item.tooltip["footer1"] = {label = "Energy Bonus: "..tostring(item.properties.consumeValue) .. "%"}--, data = "consumeValueDisplay"}
			else
				item.tooltip["footer1"] = {label = "Health Bonus: "..tostring(item.properties.consumeValue) .. "%"}--, data = "consumeValueDisplay"}
			end
		elseif item.properties.itemType == ITEM_TYPES.PLACEABLE then

		elseif item.properties.itemType == ITEM_TYPES.GENERIC then

		elseif item.properties.itemType == ITEM_TYPES.AMMO then

		end
	end
end

function displayBaseElements()
	Control_SetVisible(gHandles[ITEM_BTN_NAME_1], false)
	Control_SetVisible(gHandles[ITEM_NAME], false)
	Control_SetVisible(gHandles[ITEM_DURABILITY_NAME], false)
	Control_SetVisible(gHandles[DROP_BUTTON_NAME], true)
	Control_SetVisible(gHandles[REPAIR_DETAILS_NAME], true)
	Control_SetVisible(gHandles[CUR_DURABILITY_NAME], false)
	Control_SetVisible(gHandles[MOVE_TO_REPAIR_NAME], false)
	Control_SetVisible(gHandles[SLIDER_NAME], false)
	Control_SetVisible(gHandles[CUR_COST_NAME], false)
	Control_SetVisible(gHandles[REPAIR_ITEM_NAME], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[REPAIR_AMOUNT_NAME], m_repairUNID ~= "0")
	
	Control_SetVisible(gHandles[ITEM_BG_NAME_2], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[ITEM_IMAGE_NAME_2], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[ITEM_BTN_NAME_2], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[BACKPACK_COUNT_NAME], m_repairUNID ~= "0")
	
	Control_SetLocationY(gHandles[ITEM_BG_NAME_2], 228)
	Control_SetLocationY(gHandles[ITEM_IMAGE_NAME_2], 229)
	Control_SetLocationY(gHandles[ITEM_BTN_NAME_2], 229)
	Control_SetLocationY(gHandles[BACKPACK_COUNT_NAME], 264)
end

function displayRepairElements()
	Control_SetVisible(gHandles[ITEM_BTN_NAME_1], true)
	Control_SetVisible(gHandles[ITEM_NAME], true)
	Control_SetVisible(gHandles[ITEM_DURABILITY_NAME], true)
	Control_SetVisible(gHandles[DROP_BUTTON_NAME], false)
	Control_SetVisible(gHandles[REPAIR_DETAILS_NAME], false)
	Control_SetVisible(gHandles[CUR_DURABILITY_NAME], true)
	Control_SetVisible(gHandles[MOVE_TO_REPAIR_NAME], true)
	Control_SetVisible(gHandles[SLIDER_NAME], true)
	Control_SetVisible(gHandles[CUR_COST_NAME], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[REPAIR_ITEM_NAME], false)
	Control_SetVisible(gHandles[REPAIR_AMOUNT_NAME], false)
	
	Control_SetVisible(gHandles[ITEM_BG_NAME_2], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[ITEM_IMAGE_NAME_2], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[ITEM_BTN_NAME_2], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[BACKPACK_COUNT_NAME], m_repairUNID ~= "0")
	
	Control_SetLocationY(gHandles[CUR_COST_NAME], 322)
	Control_SetLocationY(gHandles[CUR_DURABILITY_NAME], 283)
	Control_SetLocationY(gHandles[ITEM_BG_NAME_2], 320)
	Control_SetLocationY(gHandles[ITEM_IMAGE_NAME_2], 321)
	Control_SetLocationY(gHandles[ITEM_BTN_NAME_2], 321)
	Control_SetLocationY(gHandles[BACKPACK_COUNT_NAME], 350)
end

function displayRepairingElements()
	Control_SetVisible(gHandles[ITEM_BTN_NAME_1], true)
	Control_SetVisible(gHandles[ITEM_NAME], true)
	Control_SetVisible(gHandles[ITEM_DURABILITY_NAME], true)
	Control_SetVisible(gHandles[DROP_BUTTON_NAME], false)
	Control_SetVisible(gHandles[REPAIR_DETAILS_NAME], false)
	Control_SetVisible(gHandles[CUR_DURABILITY_NAME], true)
	Control_SetVisible(gHandles[MOVE_TO_REPAIR_NAME], false)
	Control_SetVisible(gHandles[SLIDER_NAME], false)
	Control_SetVisible(gHandles[CUR_COST_NAME], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[REPAIR_ITEM_NAME], false)
	Control_SetVisible(gHandles[REPAIR_AMOUNT_NAME], false)
	
	Control_SetVisible(gHandles[ITEM_BG_NAME_2], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[ITEM_IMAGE_NAME_2], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[ITEM_BTN_NAME_2], m_repairUNID ~= "0")
	Control_SetVisible(gHandles[BACKPACK_COUNT_NAME], m_repairUNID ~= "0")
	
	Control_SetLocationY(gHandles[CUR_COST_NAME], 270)
	Control_SetLocationY(gHandles[CUR_DURABILITY_NAME], 231)
	Control_SetLocationY(gHandles[ITEM_BG_NAME_2], 268)
	Control_SetLocationY(gHandles[ITEM_IMAGE_NAME_2], 269)
	Control_SetLocationY(gHandles[ITEM_BTN_NAME_2], 268)
	Control_SetLocationY(gHandles[BACKPACK_COUNT_NAME], 298)
	
	Control_SetEnabled(gHandles[REPAIR_BUTTON], false)
end

-- Returns the color for the durability bar based on percentage passed in
getDurabilityColor = function(durabilityPercentage)
	-- Green bar
	if durabilityPercentage >= .5 then
		return DURABILITY_COLORS.GREEN
	-- Return yellow
	elseif durabilityPercentage >= .15 then
		return DURABILITY_COLORS.YELLOW
	-- Return red
	else
		return DURABILITY_COLORS.RED
	end
end