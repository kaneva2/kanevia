--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- NewScriptServer.lua 
-- 
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\DevToolsConstants.lua")

ch = nil 

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "refreshScriptList", KEP.MED_PRIO )
end

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) 
	ch = Control_GetEditBox(gHandles["edNewScript"])
	Control_SetFocus(ch)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnSave_OnButtonClicked(buttonHandle)
	local name = EditBox_GetText(Control_GetEditBox( gHandles["edNewScript"] ))
	if name==nil then
		KEP_MessageBox( "Error reading input" )
		return
	end

	if name=="" then
		KEP_MessageBox( "Please enter a name for the new script" )
		return
	end

	if string.gsub(name, "[A-Za-z0-9_-]", "")~="" then
		KEP_MessageBox( "Invalid script name. Please use only letters, numbers, underscore or dash" )
		return
	end

	if string.upper(string.sub(name, -4))~=".LUA" then
		name = name .. ".lua"
	end

	local file = KEP_FileOpen( name, "w", "ScriptData" )
	if file==nil then
		KEP_MessageBox( "Error creating temporary file" )
		return
	end

	KEPFile_WriteLine( file, "-- " .. name )
	KEP_FileClose( file )
	
	local menuDir = KEP_GetMenuDir()
	local fullPath = string.gsub(menuDir, "[/\\]Menus$", "\\ScriptData\\" .. name)

	KEP_Import3DAppAsset(DevToolsUpload.SAVED_SCRIPT, fullPath)
	
	KEP_EventCreateAndQueue( "refreshScriptList")

	MenuCloseThis()
end

function edNewScript_OnEditBoxString(editBoxHandle, someString)
	btnSave_OnButtonClicked(gHandles["btnSave"])
end
