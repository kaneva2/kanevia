--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_PlayerHelper.lua
--
-- Client-side persistent player script
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\CAM.lua")
dofile("..\\Scripts\\BuildMode.lua")
dofile("..\\Scripts\\EnvironmentIds.lua")
dofile("..\\Scripts\\PlaceCategoryIds.lua")

local s_bIsInDebugMode = Environment.isRunningInDebugMode()
local CullingHelper
if s_bIsInDebugMode then
	dofile("..\\MenuScripts\\Framework_CullingHelper.lua")
	CullingHelper = _G.CullingHelper
end


-- dofile("..\\MenuScripts\\CharacterHelper.lua")
-- dofile("..\\MenuScripts\\CharacterCreation2.lua")

-- Constants
-- Events regularly used by Framework MenuScripts (Register here to help prevent race conditions for handler registration elsewhere)
local FRAMEWORK_CLIENT_EVENTS = {
	IDLE = {
		FRAMEWORK_BUILD_HAND_MODE				= KEP.HIGH_PRIO,
		FRAMEWORK_BUILD_ITEM_INFO				= KEP.HIGH_PRIO,
		FRAMEWORK_BUILD_PLACE_ITEM				= KEP.HIGH_PRIO,
		FRAMEWORK_BUILD_SETTING					= KEP.MED_PRIO, -- For changing move/rotate mode
		FRAMEWORK_PLAYER_BUILD_CHANGE_MODE		= KEP.MED_PRIO, -- For changing move/rotate mode
		FRAMEWORK_USE_SELECTED_ITEM			 	= KEP.MED_PRIO, -- For use button when an object is selected
		FRAMEWORK_SET_SELECTED_OBJECT			= KEP.HIGH_PRIO,
		FRAMEWORK_SET_PLAYER_MODE				= KEP.MED_PRIO,
		FRAMEWORK_SHOW_BUILD_TOOLS				= KEP.MED_PRIO,
		ITEM_MOUSE_OVER_BEHAVIOR				= KEP.MED_PRIO,
		QueryPlacesEvent						= KEP.MED_PRIO,
		DisplayTopWorlds						= KEP.MED_PRIO
	},
	ACTIVE = {
		CHANGED_PARAM_EVENT						= KEP.HIGH_PRIO,
		CLOSE_BACKPACK							= KEP.HIGH_PRIO, -- Event to close the Framework_PlayerSystems menu
		CLOSE_CHEST								= KEP.HIGH_PRIO, -- Event to close the Framework_ItemContainer menu
		CLOSE_VENDOR							= KEP.HIGH_PRIO,
		COLOR_START_EVENT						= KEP.MED_PRIO,
		COLOR_PICKER_EVENT						= KEP.MED_PRIO,
		EDIT_GAME_ITEM_INFO						= KEP.HIGH_PRIO,
		EDIT_PARAM_EVENT						= KEP.HIGH_PRIO,
		FRAMEWORK_ATTACK_FLASH					= KEP.MED_PRIO,
		FRAMEWORK_GAME_ITEM_LOAD_MESSAGE		= KEP.MED_PRIO, -- Informs Framework_GameItemLoadWarning of what to say to the user
		FRAMEWORK_GENERIC_PROMPT_INIT			= KEP.HIGH_PRIO,
		FRAMEWORK_GENERIC_PROMPT_RETURN			= KEP.HIGH_PRIO,
		FRAMEWORK_NAME_CONFLICT_MESSAGE			= KEP.HIGH_PRIO,
		FRAMEWORK_ATTRIBUTE_CONFLICT_MESSAGE	= KEP.HIGH_PRIO,
		FRAMEWORK_PUBLISH_GAME_ITEM				= KEP.HIGH_PRIO,
		FRAMEWORK_SAVE_VERIFY					= KEP.HIGH_PRIO,
		FRAMEWORK_SELECT_HAND					= KEP.MED_PRIO,
		FRAMEWORK_OVERWRITE_CONFIRMATION		= KEP.HIGH_PRIO,
		FRAMEWORK_WEAPON_EQUIPPED				= KEP.MED_PRIO,
		INVENTORY_HANDLER_ADD_ITEM				= KEP.HIGH_PRIO, -- Event to add or update an item in a given inventory slot
		INVENTORY_HANDLER_CHECK_LOOT			= KEP.HIGH_PRIO, -- Event to check to see if there is room in player inventory for loot
		INVENTORY_HANDLER_CHECK_LOOT_RESPONSE	= KEP.HIGH_PRIO, -- Response event for inventory loot space check
		INVENTORY_HANDLER_REMOVE_ITEM			= KEP.HIGH_PRIO, -- Event to remove an item from a given inventory slot
		INVENTORY_HANDLER_REQUEST_INVENTORY		= KEP.HIGH_PRIO, -- Event to request cached or server inventory (if no cache)
		INVENTORY_HANDLER_UPDATE_DIRTY			= KEP.HIGH_PRIO, -- Event to send all dirty inventories to the server
		SHOW_GAME_SYSTEM_MENU					= KEP.HIGH_PRIO,
		UPDATE_ARMORY_CLIENT					= KEP.HIGH_PRIO, -- Event to update an item slot in player armory inventory
		UPDATE_ARMORY_CLIENT_FULL				= KEP.HIGH_PRIO, -- Event to update the entire player armory inventory
		UPDATE_BLUEPRINTS_CLIENT_FULL			= KEP.HIGH_PRIO, -- Event to update the entire player blueprint inventory
		UPDATE_CONTAINER_CLIENT					= KEP.HIGH_PRIO, -- Event to update an item slot in a container (by PID)
		UPDATE_CONTAINER_CLIENT_FULL			= KEP.HIGH_PRIO, -- Event to update an entire container inventory (by PID)
		UPDATE_INVENTORY_CLIENT_FULL			= KEP.HIGH_PRIO, -- Event to update the entire player main inventory
		UPDATE_INVENTORY_CLIENT					= KEP.HIGH_PRIO, -- Event to update an item slot in player main inventory
		UPDATE_VENDOR_CLIENT_FULL				= KEP.HIGH_PRIO,
		FRAMEWORK_OBJECT_OFFENDING_FLAG			= KEP.HIGH_PRIO,
		FRAMEWORK_QUEST_INFO					= KEP.HIGH_PRIO, -- Sends quest info over to Framework_QuestInfo from Framework_QuestGiver
		FRAMEWORK_QUEST_INFO_CLOSING			= KEP.HIGH_PRIO, -- Informs QuestGiver that QuestInfo is closing
		FRAMEWORK_OPEN_CRAFTING_MENU			= KEP.HIGH_PRIO,
		FRAMEWORK_OPEN_QUEST_MENU				= KEP.HIGH_PRIO,
		FRAMEWORK_MINIMIZE_HUD					= KEP.HIGH_PRIO,
		FRAMEWORK_UNLOCK_TOOLBAR				= KEP.HIGH_PRIO,
		FRAMEWORK_UPDATE_TIMED_VENDOR			= KEP.HIGH_PRIO		  
	}
}

-- Menus that must be closed when entering Player Mode
-- Note, this is built in the order these menus will be closed in (in case destroy order matters)
local GOD_TO_PLAYER = {
	-- WOK Menus
	-- Creator Menus
	"CreatorToolbox.xml",
	"MyApps.xml",
	"Inventory.xml", --Remove?
	"ObjectBrowser.xml",
	"ZoneEditor.xml",
	"ServerScriptBrowser.xml",
	"MenuBrowser.xml",
	"MenuScriptBrowser.xml",
	"DevToolsPublish.xml",
	-- Import Menus
	"DaeImport.xml",
	"UGCImportProgCommon.xml",
	"MeshImportProg.xml",
	"AnimImportProg.xml",
	"EquippableImportProg.xml",
	"EquippableImportOptions.xml",
	"MeshImportOptions.xml",
	"ImportAccessoryAnimation.xml",
	"ImportMenu.xml",
	"UGCImportMissingTextures.xml",
	-- WOK Menus
	"Emotes.xml",
	"PlaceableObj.xml",
	"RClickObject.xml",
	"RClickShop.xml",
	-- Framework menus
	"Framework_Characters.xml",--Remove?
	"Framework_Quests.xml",--Remove?
	"Framework_CraftingRecipes.xml",--Remove?
	"Framework_GameSystems.xml",--Remove?
	"Framework_Placeables.xml",--Remove?
	"Framework_Spawners.xml",--Remove?
	"Framework_WorldInventory.xml",--Remove?
	"Framework_WorldSettings.xml",--Remove?
	"Framework_Harvestables.xml",--Remove?
	"Framework_JetPack.xml",
	"Framework_GameItemEditor.xml",
	"Framework_QuestHandler.xml",
	-- Unified Menu
	"UnifiedInventory.xml"
}

-- Menus that must be closed when entering God Mode
local PLAYER_TO_GOD = {
	-- Framework Menus
	"Framework_PlayerSystems.xml",--Remove?
	"Framework_Toolbar.xml",
	"Framework_Vendor.xml",
	"Framework_Repair.xml",
	"Framework_TimedResource.xml",
	"Framework_ClothingVendor.xml",
	"Framework_CreditsVendor.xml",
	"Framework_GemVendor.xml",
	"Framework_PlayerLeaderboard.xml",
	"Framework_LeaderboardCongrats.xml",
	"Framework_FlagInfo.xml",
	"Framework_GemExchange.xml",
	"Framework_GemBox.xml",
	"Framework_RandomVendor.xml",
	"Framework_BottomRightHUD.xml",
	"Framework_TopRightHUD.xml",
	"Framework_Cursor_HighlightHandler.xml",
	"Framework_QuestCompass.xml",
	"Framework_WorldCoinExchange.xml",
	"Framework_RandomVendor.xml",
	"Framework_CreditsVendor.xml",
	"Framework_BottomHitBox.xml",
	"Framework_StatusIcons.xml",
	-- "UnifiedInventory.xml"
	"Framework_ContextualHelp.xml",
	"Framework_TimedVendorHandler",
	"Framework_SingleItemVendor",
	"Framework_NPCActionMenu",
	"Framework_NPCGifting",
	"Framework_QuestHandler.xml",
	"Framework_GameBadges.xml"
}

local ALLOW_ALL			= 1
local ALLOW_PRIVATE		= 2
local ALLOW_OWNER_ONLY	= 3

local TEMP_PAPER_DOLL_DELAY = 3
local CHEST_PADDING_Y = 2

-- Local vars
local m_playerName, m_zoneInstanceId, m_zoneType, m_gameId
local m_perm
local m_playerMode
local m_buildMode = false
local m_initTimer = TEMP_PAPER_DOLL_DELAY
local m_worldGaming = true --This will be used to know if it needs to open Framework_AllGameItems to the wok or world version
local m_defaultEnvironment = {sun = {},
							  ambient = {}
							 }
local m_frameworkConfigs = {}

local m_frameworkEnabled, m_jumpsuitOn, m_anythingEquipped

local m_ignoreModeSet = false

local m_proxTriggers = {} -- list of proximity triggers this client is inside, maintained here so we know reglardless of mode
local s_lootables = {} -- list of lootable objects that need their highlights turned on

local m_currTime = 0
local m_elapsedTime = 0
local m_AFKStatus = false -- track whether the player is AFK
local m_hideAFKStatus = false --Determines weather to show AFK
local SECONDS_PER_MINUTE = 60
local AFK_LIMIT = 10 * SECONDS_PER_MINUTE
local m_AFKDeadline = AFK_LIMIT

local s_hasRequestedLootNotification = false
local s_bIsPlayerInVehicle = false



local MAX_ZOOM_DISTANCE = 150 --Original max zoom out is 30.
g_playersCamera	= nil

-- Local functions
local cleanup -- ()
local setPlayerMode -- ()
local leaveVehicle -- ()

-- Called when the menu is created
function onCreate()
	-- Register server event handlers
	Events.registerHandler("FRAMEWORK_ACTIVATE_PLAYER", activatePlayer)
	Events.registerHandler("FRAMEWORK_DEACTIVATE_PLAYER", deactivatePlayer)
	Events.registerHandler("FRAMEWORK_RETURN_IDLE_PLAYER_INFO", idlePlayerInfo)
	Events.registerHandler("FRAMEWORK_MODIFY_PRIVACY",  modifyPrivacy) -- Register for privacy modifier event
	Events.registerHandler("FRAMEWORK_ANYTHING_EQUIPPED",  anythingEquipped)
	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS",  returnSettings)
	Events.registerHandler("FRAMEWORK_OPEN_TRAVEL_MENU",  openTravelMenu)
	Events.registerHandler("FRAMEWORK_OPEN_PLAYER_MENU", openPlayerMenu)
	Events.registerHandler("FRAMEWORK_OPEN_CRAFTING_MENU", openCraftingMenu)
	
	Events.registerHandler("FRAMEWORK_REMOVE_EVENT", removeEventHandler)
	--Events.registerHandler("RETURN_CHAT_BAN_LIST", returnBanList)
	Events.registerHandler("RETURN_CURRENT_TIME", onReturnCurrentTime)
	Events.registerHandler("FRAMEWORK_UPDATE_TIMED_STATE", onTimedStateUpdated)
	Events.registerHandler("FRAMEWORK_PLAYER_HAS_LOOT", onPlayerLootDiscovered)
	Events.registerHandler("FRAMEWORK_REQ_FLAG_FEE_NOTIF", requestFlagFeeNotification)
	Events.registerHandler("FRAMEWORK_PLAYER_OCCUPIED", onPlayerEnteredOrLeftVehicle)
	Events.registerHandler("FRAMEWORK_REPOSITION_CHEST_CHECK", RepositionChestEventHandler)
	
	-- Register events we'll be sending from this menu
	KEP_EventRegister("UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO) -- Event to update an item slot in player main inventory
	KEP_EventRegister("UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO) -- Event to update the entire player main inventory
	KEP_EventRegister("UPDATE_BLUEPRINTS_CLIENT_FULL", KEP.HIGH_PRIO) -- Event to update the entire player blueprint inventory
	KEP_EventRegister("UPDATE_QUEST_CLIENT", KEP.HIGH_PRIO) -- Event to update one player quest
	KEP_EventRegister("UPDATE_QUEST_CLIENT_FULL", KEP.HIGH_PRIO) -- Event to update the entire player quest inventory
	KEP_EventRegister("UPDATE_GAME_CLIENT", KEP.HIGH_PRIO) -- Event to update a single game item
	KEP_EventRegister("UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO) -- Event to update the entire game item cache
	KEP_EventRegister("UPDATE_ARMORY_CLIENT", KEP.HIGH_PRIO) -- Event to update an item slot in player armory inventory
	KEP_EventRegister("UPDATE_ARMORY_CLIENT_FULL", KEP.HIGH_PRIO) -- Event to update the entire player armory inventory
	KEP_EventRegister("UPDATE_CONTAINER_CLIENT", KEP.HIGH_PRIO) -- Event to update an item slot in a container (by PID)
	KEP_EventRegister("UPDATE_CONTAINER_CLIENT_FULL", KEP.HIGH_PRIO) -- Event to update an entire container inventory (by PID)
	KEP_EventRegister("UPDATE_VENDOR_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_ALIGN_INVENTORY", KEP.HIGH_PRIO) --Event to make sure Unified Inventory  is in the right order
	KEP_EventRegister("FRAMEWORK_SHOW_EVENT_INVITE", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_ATTEMPT_TOOL", KEP.HIGH_PRIO)
	KEP_EventRegister("UPDATE_MEDIA_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegister("UPDATE_MEDIA_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegister("UPDATE_FLAG_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegister("UPDATE_FLAGS_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_REQUEST_FLAGS_LOCALLY", KEP.HIGH_PRIO)
	Events.registerHandler("FRAMEWORK_GOTO_GAME_ITEM", gotoGameItemHandler)
	
	KEP_EventRegister( "FRAMEWORK_RETURN_STATE", KEP.HIGH_PRIO )
	KEP_EventRegister( "FRAMEWORK_REQUEST_STATE", KEP.HIGH_PRIO )
	KEP_EventRegister( "SelectMediaEvent", KEP.MED_PRIO )
	KEP_EventRegister( "UpdateMediaEvent", KEP.MED_PRIO )
	KEP_EventRegister( "FRAMEWORK_REQUEST_VENDOR_ITEMS", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_RETURN_VENDOR_ITEMS", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_REMOVE_VENDOR_ITEMS", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_WORLD_GAME_MODE", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_REQUEST_WORLD_GAME_MODE", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_REQUEST_HIGHLIGHTABLES", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_RETURN_HIGHLIGHTABLES", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_OPEN_WELCOME", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_MAP_ICON_OPENED", KEP.HIGH_PRIO) -- Event to check when map icon is opened
	KEP_EventRegister("FRAMEWORK_INIT_CHAR_CREATOR", KEP.MED_PRIO)
	KEP_EventRegister("FRAMEWORK_SAVE_NPC_DATA", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_OPEN_NPC_INVENTORY", KEP.HIGH_PRIO)
	KEP_EventRegister( "SETTINGS_GOTO_SYSTEM", KEP.HIGH_PRIO) --Event for going to a specific settings tab
	KEP_EventRegister("UPDATE_CREDIT_TRANSACTION", KEP.HIGH_PRIO)

	
	
	-- Vehicle placement handling
	KEP_EventRegister("RepositionVehicleAfterExitEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("RepositionVehicleAfterExitEventHandler", "RepositionVehicleAfterExitEvent", KEP.HIGH_PRIO)

	KEP_EventRegister("RequestPlayerRespawnEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("RequestPlayerRespawnEventHandler", "RequestPlayerRespawnEvent", KEP.HIGH_PRIO)

	KEP_EventRegister("RESET_AFK", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("resetAFK", "RESET_AFK", KEP.HIGH_PRIO)
	-- KEP_EventRegisterHandler( "BuildExitEventHandler", "BuildExitEvent", KEP.MED_PRIO )
	
	-- Register all client events for the PlayerHandler's default state (Regardless of Framework state)
	for event, prio in pairs(FRAMEWORK_CLIENT_EVENTS.IDLE) do
		KEP_EventRegister(event, prio)
	end

	MenuOpen("Framework_PlayerBuild.xml") -- Framework_PlayerBuild must be open at all times (TODO: Can we move required logic here / SHOULD we)
	MenuOpen("Framework_MediaFlagHandler.xml") --Needs to be open at all times
	
	MenuOpen("Framework_FlagHandler.xml")

	KEP_EventRegisterHandler( "playerSetModeHandler", "FRAMEWORK_SET_PLAYER_MODE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "frameworkStateHandler", "GetFrameworkState", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "frameworkRequestStateHandler", "FRAMEWORK_REQUEST_STATE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "buildModeChangedEventHandler", "BuildModeChangedEvent", KEP.MED_PRIO ) -- needed to know if in build mode
	KEP_EventRegisterHandler( "worldGameModeHandler", "FRAMEWORK_WORLD_GAME_MODE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "requestWorldGameModeHandler", "FRAMEWORK_REQUEST_WORLD_GAME_MODE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "clientTriggerHandler", "TriggerEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "onHighlightablesRequested", "FRAMEWORK_REQUEST_HIGHLIGHTABLES", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "hideMapIconSettingEventHandler", "HideMapIconSettingEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "hideAFKStatusEventHandler", "HideAFKStatusSettingEvent", KEP.MED_PRIO )
	
	-- Request base player information
	Events.sendEvent("FRAMEWORK_REQUEST_IDLE_PLAYER_INFO")
	
	-- Get player's name
	m_playerName 	 = KEP_GetLoginName()
	-- Snag the default spawn point for this zone now
	m_zoneInstanceId = KEP_GetZoneInstanceId()
	m_zoneType 		 = KEP_GetZoneIndexType()
	m_gameId		 = KEP_GetGameId()
	
	-- Retrieve zone's default environment settings
	m_defaultEnvironment = KEP_GetEnvironment()
	local sun = {}
	local ambient = {}
	sun.r, sun.g, sun.b = GetSet_GetColor(m_defaultEnvironment, EnvironmentIds.SUNRED)
	ambient.r, ambient.g, ambient.b = GetSet_GetColor(m_defaultEnvironment, EnvironmentIds.AMBIENTLIGHTREDDAY)

	Events.sendEvent("FRAMEWORK_RETURN_ENVIRONMENT_DEFAULTS", {sun = sun,
															   ambient = ambient})
	
	-- Send the zoneInstanceId and zoneType to Controller_Game / Inventory for proper 
	Events.sendEvent("FRAMEWORK_RETURN_ZONE_DATA", {zoneInstanceId = KEP_GetZoneInstanceId(),
													zoneType = KEP_GetZoneIndexType()})

	Events.sendEvent("FRAMEWORK_REQUEST_CURRENT_TIME", {playerName = KEP_GetLoginName()})
	Events.sendEvent("FRAMEWORK_REQUEST_PARENT_DATA")

	g_playersCamera = KEP_GetPlayersActiveCamera()
	g_origMaxDistance = GetSet_GetNumber(g_playersCamera, CameraIds.ORBITMAXDIST)
	g_origDistance = GetSet_GetNumber(g_playersCamera, CameraIds.ORBITDIST)
	GetSet_SetNumber(g_playersCamera, CameraIds.ORBITMAXDIST, MAX_ZOOM_DISTANCE)

	m_hideAFKStatus = GetHideAFKSetting()

	if s_bIsInDebugMode then
		CullingHelper.Init()
	end

	SetCameraLevel()
end

function onDestroy()
	log("Framework_PlayerHandler - onDestroy")
	if s_bIsInDebugMode then
		CullingHelper.DeInit()
	end
end

-- Called each frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- Increment time elapsed since menu opened or updated
	if MenuIsOpen("ProgressMenu") then
		m_initTimer = TEMP_PAPER_DOLL_DELAY
	elseif m_initTimer > 0 then
		m_initTimer = m_initTimer - fElapsedTime
		if m_initTimer <= 0 then
			m_initTimer = 0
			Events.sendEvent("FRAMEWORK_SET_PLAYER_QUEST_ICONS")
		end
	end

	-- Check elapsed time and add to current time after a second has passed
	m_elapsedTime = m_elapsedTime + fElapsedTime
	if m_elapsedTime >= 1 then
		m_currTime = m_currTime + 1
		m_elapsedTime = m_elapsedTime - 1
		if m_currTime > m_AFKDeadline and m_AFKStatus == false and m_hideAFKStatus == false then
			m_AFKStatus = true
			-- Kneel emote
			-- ChangeAnimation(anim_kneel)
			Events.sendEvent("FRAMEWORK_REQUEST_CURRENT_TIME", {playerName = KEP_GetLoginName()})
			Events.sendEvent("FRAMEWORK_UPDATE_AFK_STATUS", {AFK = true})
		end
	end

	-- Reset AFK countdown if either mouse button is pressed
	if ToBool(Dialog_IsLMouseDown(gDialogHandle)) or ToBool(Dialog_IsRMouseDown(gDialogHandle)) then
		resetAFK()
	end
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

function Dialog_OnMouseMove(dialogHandle, key, shift, ctrl, alt)
	resetAFK()
end

---------------------------------------------------------------------------------------- Begin Paper Doll Sandbox

-- Borrowed Constants
local ACTOR_ID			= {  4, 3, 10, 11, 12, 13, 14, 15 }
local CONFIG_HEAD		= {  6, 6, 6, 6, 6, 6, 6, 6 }
local CONFIG_NECK		= { 99, 99, 1, 1, 1, 1, 1, 1 }
local CONFIG_TORSO		= {  7, 7, 2, 2, 2, 2, 2, 2 }
local CONFIG_CROTCH		= { 99, 99, 3, 3, 3, 3, 3, 3 }
local CONFIG_ARMS		= { 99, 99, 4, 4, 4, 4, 4, 4 }
local CONFIG_LEGS		= {  8, 8, 5, 5, 5, 5, 5, 5 }
local CONFIG_HANDS		= { 99, 99, 6, 6, 6, 6, 6, 6 }
local CONFIG_FEET		= { 14, 9, 7, 7, 7, 7, 7, 7 }
local FACES_MALE 		= { 2988, 2989, 2990, 3720, 3746 }
local FACES_FEMALE 		= { 2991, 2992, 2993, 3744, 3745 }
local FACE_EYE_RIGHT	= 0
local FACE_EYE_LEFT 	= 2
local FACE_EYEBROW		= 7
local FACE_FACECOVER	= 8
local HAIR_COLORS_MALE 	= { 8, 8, 13, 10, 8,
							8, 8, 8, 26, 26,
							26, 26, 26, 8, 12,
							8, 11, 8, 0, 8,
							8, 8, 8, 8, 8, 8}
local HAIR_COLORS_FEM 	= { 26, 26, 26, 26, 26,
							26, 26, 26, 26, 26,
							26, 26, 26, 26, 26,
							26, 26, 26, 26, 8,
							8,	8,	8,	8,	8 }
local MAX_BEARD 		= 15
local MAX_MAKEUP		= 15
local MAX_BROW_FEM		= 6
local MAX_BROW_MALE		= 5
local MAX_BROW_COLOR	= 3
local MAX_BEARD_COLOR 	= 4
local MAX_SKIN_COLOR	= 11
local MAX_EYE_COLOR 	= 14
local SCALE_TOLERANCE 	= 3

-- NPC Spawning Code

local genderIndex = 0
local g_npcCnt = 0



---------------------------------------------------------------------------------------- End Paper Doll Sandbox

-- Called when a key is pressed
function Dialog_OnKeyDown(dialogHandle, key, shift, ctrl, alt)
	resetAFK()

	-- 'M' pressed
	if key == Keyboard.M then
		-- Open the map in a non-game world or as a player in a game world
		if m_worldGaming == false or (m_playerMode and m_playerMode == "Player") then
			if MenuIsClosed("Framework_Map.xml") then
				MenuOpen("Framework_Map.xml")
			else
				MenuClose("Framework_Map.xml")
			end
		end
	end

	-- 'G' pressed
	if key == Keyboard.G then
		if m_playerMode and m_playerMode == "God" then
			if MenuIsClosed("UnifiedNavigation.xml") then
				MenuOpen("UnifiedNavigation.xml")				
			end

			local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
			if  m_worldGaming == false then
				KEP_EventEncodeString(ev, "wokGaming")
			else
				KEP_EventEncodeString(ev, "gaming")
			end
			KEP_EventQueue(ev)
		elseif m_buildMode  then
			if MenuIsClosed("UnifiedNavigation.xml") then
				MenuOpen("UnifiedNavigation.xml")				
			end
			local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
			KEP_EventEncodeString(ev, "wokGaming")
			KEP_EventQueue(ev)
		end	
	end

	-- 'C' pressed
	if key == Keyboard.C then
		--Players open the Crafting menu
		if m_playerMode and m_playerMode == "Player" then
			if MenuIsClosed("UnifiedNavigation.xml") then
				MenuOpen("UnifiedNavigation.xml")				
			end

			local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
			KEP_EventEncodeString(ev, "crafting")
			KEP_EventQueue(ev)
		end
	end

	-- 'I' pressed
	if key == Keyboard.I then
		if not m_frameworkEnabled then
			-- Open/Close the Inventory menu
			if MenuIsClosed("UnifiedNavigation.xml") then
				MenuOpen("UnifiedNavigation.xml")
			end
			local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
			if m_buildMode then
				KEP_EventEncodeString(ev, "building")
			else
				KEP_EventEncodeString(ev, "clothing")
			end
			KEP_EventQueue(ev)		
		else
			if m_playerMode then
				-- Gods open the Inventory menu
				if m_playerMode == "God" then
					if MenuIsClosed("UnifiedNavigation.xml") then
						MenuOpen("UnifiedNavigation.xml")
					end

					local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
					KEP_EventEncodeString(ev, "building")
					KEP_EventQueue(ev)
					
				-- Players
				elseif m_playerMode  == "Player" then
					-- Close Player System if it's open
					if MenuIsClosed("UnifiedNavigation.xml") then
						MenuOpen("UnifiedNavigation.xml")
					end

						local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
						KEP_EventEncodeString(ev, "backpack")
						KEP_EventQueue(ev)			
				end
			end
		end
	end
	
	if key == Keyboard.X then
		if s_bIsPlayerInVehicle then
			leaveVehicle()
		end
	end

	
	-- Handle key presses for occlusion culling tests
	if s_bIsInDebugMode then
		CullingHelper.HandleInput(key, shift, ctrl, alt)
	end
end



function onPlayerEnteredOrLeftVehicle(event)
	s_bIsPlayerInVehicle = event.occupied
end

-- -- -- -- -- -- --
-- Client Event Handlers
-- -- -- -- -- -- --

function clientTriggerHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter ~= 0) then return end
	local action  = KEP_EventDecodeNumber( event )
	if (action == 1) then --enter trigger
		if not KEP_DynamicObjectGetVisibility(objectid) then return end
		m_proxTriggers[tostring(objectid)] = true
	elseif (action == 2) then
		m_proxTriggers[tostring(objectid)] = nil
	end
end


function RepositionVehicleAfterExitEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local objPos = {}
	local PID = KEP_EventDecodeNumber(event)
	objPos.x = KEP_EventDecodeNumber(event)
	objPos.y = KEP_EventDecodeNumber(event)
	objPos.z = KEP_EventDecodeNumber(event)
	objPos.rx = KEP_EventDecodeNumber(event)
	objPos.ry = KEP_EventDecodeNumber(event)
	objPos.rz = KEP_EventDecodeNumber(event)
	-- local sidex = KEP_EventDecodeNumber(event)
	-- local sidey = KEP_EventDecodeNumber(event)
	-- local sidez = KEP_EventDecodeNumber(event)

	if PID and PID > 0 then
		-- Reposition player if necessary
		local minX, minY, minZ, maxX, maxY, maxZ = KEP_DynamicObjectGetBoundingBox(PID) -- Need to get vehicle object?
		local pX, pY, pZ, rot = KEP_GetPlayerPosition()
		local buffer = 0.1 -- Account for slight error
		-- local insideX = pX > minX - buffer and pX < maxX + buffer
		local insideY = pY > minY - buffer and pY < maxY + buffer
		-- local insideZ = pZ > minZ - buffer and pZ < maxZ + buffer
		-- If inside mesh, move outside of it
		if insideY then
			-- update player position
			-- local newX = maxX + 2*buffer -- pX
			local newY = maxY + 2*buffer
			-- local newZ = maxZ + 2*buffer -- pZ
			-- Convert rotation degrees to vector
			local rotation = {}
		    rot = (rot * math.pi) / 180
		    rotation.rx = math.sin(rot)
		    rotation.ry = 0
		    rotation.rz = math.cos(rot)
			Events.sendEvent("FRAMEWORK_MOVE_PLAYER", {user = KEP_GetLoginName(), x = pX, y = newY, z = pZ, rx = rotation.rx, ry = rotation.ry, rz = rotation.rz})
		end

		local UNID = 0
		_, _, _, _, _, _, _, _, _, _, _, _, _, UNID, _ = KEP_DynamicObjectGetInfo(PID)
		if UNID and UNID > 0 then
			Events.sendEvent("FRAMEWORK_DYNAMIC_OBJECT_SAVE_SERVER", {PID = PID, UNID = UNID, objPos = objPos})	
		end
	end
end

function RequestPlayerRespawnEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	Events.sendEvent("FRAMEWORK_RESPAWN_PLAYER", {forcedRespawn = true})
end

function onHighlightablesRequested(dispatcher, fromNetid, event, eventid, filter, objectid)
	if not tablePopulated(m_proxTriggers) and not next(s_lootables) then return end
	
	local response = KEP_EventCreate("FRAMEWORK_RETURN_HIGHLIGHTABLES")
	KEP_EventEncodeString(response, Events.encode(m_proxTriggers))
	KEP_EventEncodeString(response, Events.encode(s_lootables))
	
	KEP_EventQueue(response)
end

-- Handles requests for the state of the current zone
function frameworkStateHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	broadcastFrameworkState()
end

-- Sent from HUDBuild or Framework_Toolbar when a player changes modes
function playerSetModeHandler(dispatcher, fromNetId, event, filter, objectId)
	m_playerMode = KEP_EventDecodeString(event)
	
	-- Fire mode change event to the server
	Events.sendEvent("FRAMEWORK_PLAYER_CHANGE_MODE", {mode = m_playerMode})
	-- If we've entered God mode
	if m_playerMode == "God" then
		Events.sendEvent("PLAYER_TARGET", {type = "Clear"})
	end
	-- Destroy all menus that players can't see
	cleanup()
	setPlayerMode()
	
	Dialog_ClearFocus(gDialogHandle)
	
	broadcastFrameworkState()
end

function worldGameModeHandler(dispatcher, fromNetId, event, filter, objectId)
	m_worldGaming = KEP_EventDecodeNumber(event) == 1
end

function requestWorldGameModeHandler(dispatcher, fromNetId, event, filter, objectId)
	local event = KEP_EventCreate("FRAMEWORK_WORLD_GAME_MODE")
	if m_worldGaming then
		KEP_EventEncodeNumber(event, 1)
	else
		KEP_EventEncodeNumber(event, 0)
	end
	KEP_EventQueue(event)
end

function buildModeChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if m_buildMode == false then
		m_buildMode = true
	else
		m_buildMode = false
	end
end

-- -- -- -- -- -- --
-- Server Event Handlers
-- -- -- -- -- -- --
function onTimedStateUpdated(event)
	local PID = event.PID
	
	if( event.enable ) then
		s_lootables[tostring(PID)] = true
	else
		s_lootables[tostring(PID)] = nil
	end
end 



function onPlayerLootDiscovered(event)
	if( not s_hasRequestedLootNotification ) then
		requestLootNotification()
	
		s_hasRequestedLootNotification = true
	end
end 

function RepositionChestEventHandler(event)
	if event.PID and event.position then
		local PID  = event.PID
		local position = event.position
		position.y = position.y + CHEST_PADDING_Y
		collided, pos = KEP_PlacementCollisionTest(PID, {x=0,y=-1,z=0}, position)
		if collided == true then
			Events.sendEvent("FRAMEWORK_REPOSITION_CHEST_RETURN", {PID = PID, pos = pos})
		end
	end
end


-- Gets the current time for local tracking
function onReturnCurrentTime(event)
	m_currTime = event.timestamp or m_currTime -- modding the time down for floating point calculation issues
	m_AFKDeadline = m_currTime + AFK_LIMIT
end

-- Handles idle player base info
function idlePlayerInfo(event)
	m_perm = event.perm
	m_frameworkEnabled = event.frameworkEnabled == true
	m_frameworkConfigs = event.configs -- {WOK_FrameworkEnabled, GameItemLoad}
	m_jumpsuitOn = event.jumpsuitOn == true
	m_worldGaming = m_frameworkEnabled
	
	broadcastFrameworkState()
end

-- Called when a player has finished being instantiated on the server in a Framework-enabled world
function activatePlayer(event)
	-- Register all event handlers for the PlayerHandler's default state (Regardless of Framework state)
	for event, prio in pairs(FRAMEWORK_CLIENT_EVENTS.ACTIVE) do
		KEP_EventRegister(event, prio)
	end
	
	m_frameworkEnabled = true
	
	m_playerMode = event.mode
	
	setPlayerMode()
	broadcastFrameworkState()

	MenuOpen("Framework_TeleportHandler.xml") --Used when teleporting players between zones
	MenuOpen("Framework_MapDataHandler.xml")
	--Once the player is activated, requests all the Media objects and flags
	Events.sendEvent("FRAMEWORK_REQUEST_MEDIA_OBJECTS")
	Events.sendEvent("FRAMEWORK_REQUEST_FLAGS")
	

	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		local ok
		ok, m_hiddenArmor =  KEP_ConfigGetNumber(config, "HideArmor"..tostring(m_playerName), 0)
		Events.sendEvent("UPDATE_PLAYER_HIDE_ARMOR", {hideArmor = m_hiddenArmor})
	else
		Events.sendEvent("UPDATE_PLAYER_HIDE_ARMOR", {hideArmor = 0})
	end
end

function requestLootNotification()
	-- Display a notification message that indicates we have loot waiting.
	local notificationEvent = KEP_EventCreate( "NotificationRequestEvent" )
	KEP_EventEncodeString( notificationEvent, "You have earnings to collect. Look for the highlighted objects." )
	KEP_EventEncodeString( notificationEvent, "Loot" )
	KEP_EventQueue( notificationEvent )
end

function requestFlagFeeNotification(event)
	if event.message == nil then return end

	-- Display a notification message that indicates we have a flag fee that needs payment.
	local notificationEvent = KEP_EventCreate( "NotificationRequestEvent" )
	KEP_EventEncodeString( notificationEvent, event.message)
	KEP_EventEncodeString( notificationEvent, "Flag" )
	KEP_EventQueue( notificationEvent )
end


-- Called when a player is being deactivated (currently only happens when the Framework is being shut down)
function deactivatePlayer(event)
	m_frameworkEnabled = false
	m_playerMode = nil
	-- TODO: Close all Framework menus that are open?

	broadcastFrameworkState()
end

-- Modifies the zone's privacy settings
function modifyPrivacy(event)
	if event.privacy then
		local privacy
		if event.privacy == "Public" then
			privacy = ALLOW_ALL
		elseif event.privacy == "Private" then
			privacy = ALLOW_PRIVATE
		elseif event.privacy == "Owner Only" then
			privacy = ALLOW_OWNER_ONLY
		end
		if privacy == nil then return end
		
		-- Modify zone's privacy settings
		local web_address = GameGlobals.WEB_SITE_PREFIX..SET_ACCESS_SUFFIX..privacy.."&cover=0&gameId="..tostring(m_gameId).."&zoneInstanceId="..tostring(m_zoneInstanceId).."&zoneType="..tostring(m_zoneType)
		makeWebCall( web_address, WF.SET_ZONE_ACCESS)
	end
end

function anythingEquipped(event)
	m_anythingEquipped = event.anythingEquipped
end

-- Called when the server settings have been modified
function returnSettings(event)
	-- If the jumpsuit setting's been modified, broadcast the new setting
	if event.jumpsuit ~= m_jumpsuitOn then
		m_jumpsuitOn = event.jumpsuit == true
		broadcastFrameworkState()
	end
end

-- Opens the travel menu
function openTravelMenu(event)
	MenuOpen("BrowsePlaces.xml")
	if event.mode == "travel" then
		-- Request Query Places Event
		local ev = KEP_EventCreate("QueryPlacesEvent")
		KEP_EventEncodeNumber(ev, PlaceCategoryIds.APPS)
		KEP_EventEncodeNumber(ev, -1)
		KEP_EventQueue(ev)
	elseif event.mode == "topWorlds" then
		-- Tell BrowsePlaces to display the Top Worlds tab
		local ev = KEP_EventCreate("DisplayTopWorlds")
		KEP_EventQueue(ev)
	end
end

function openPlayerMenu(event)
	if m_playerMode == "Player" then
		MenuOpen(event.menu)
		
	end
end

function openCraftingMenu(event)
	if m_playerMode == "Player" then
		if MenuIsClosed("UnifiedNavigation.xml") then
			MenuOpen("UnifiedNavigation.xml")
		end
		local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
		KEP_EventEncodeString(ev, "crafting")
		KEP_EventQueue(ev)
	end
end


function gotoGameItemHandler(event)
	local itemUNID = event.itemUNID
	local itemType = event.itemType
	local behavior = event.behavior

	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")				
	end
end

function removeEventHandler(event)
	local eventID = event.eventID

	local url = "kgp/eventlist.aspx?eventId="..tostring(eventID).."&action=cancel"
	local web_address = GameGlobals.WEB_SITE_PREFIX..url
	makeWebCall(web_address, 0)

end


-- Handles the "HideMapIconSettingEvent" triggered when hud is hidden.
function hideMapIconSettingEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local checked = Event_DecodeNumber(event)
	m_hideSetting = checked == 1

	if m_hideSetting and MenuIsOpen("Framework_MapIcon.xml") then
		MenuClose("Framework_MapIcon.xml")
	elseif not m_hideSetting and MenuIsClosed("Framework_MapIcon.xml") then
		MenuOpen("Framework_MapIcon.xml")
	end
end

-- Handles the "HideAFKStatusSettingEvent" triggered when hud is hidden.
function hideAFKStatusEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local checked = Event_DecodeNumber(event)
	m_hideAFKStatus = checked == 1
end

---------------------
-- Local Functions --
---------------------

function tablePopulated(testTable)
	return testTable ~= nil and next(testTable) ~= nil
end

-- Delete all menus that players couldn't access
cleanup = function()
	local cleanupList = {}

	-- Select proper list for cleanup
	if m_playerMode == "God" then
		cleanupList = PLAYER_TO_GOD
	else
		cleanupList = GOD_TO_PLAYER
	end

	-- Close all cleanup menus
	for i=1, #cleanupList do
		MenuClose(cleanupList[i])
	end
end

-- Handle player based on the mode they're entering
setPlayerMode = function()
	if MenuIsClosed("Framework_InventoryHandler") then
		-- NOTE: InventoryHandler will open the DragDropHandler
		MenuOpen("Framework_InventoryHandler.xml")
	end
	
	if m_playerMode and m_playerMode == "Player" then
		MenuOpen("Framework_Cursor_HighlightHandler.xml")
		MenuOpen("Framework_BottomRightHUD.xml")
		MenuOpen("Framework_TopRightHUD.xml")
		MenuOpen("Framework_QuestCompass.xml")
		MenuOpen("Framework_BattleHandler.xml")
		MenuOpen("Framework_QuestHandler.xml")
		MenuOpen("Framework_BottomHitBox.xml")
		MenuOpen("Framework_TimedVendorHandler.xml")
		MenuOpen("Framework_GameBadges.xml")
	end
	if GetHideMapSetting() then -- Respect hide map icon setting
		enableMap()
	end
end

-- Broadcast info about the current state of the Framework
broadcastFrameworkState = function()

	local frameworkState = {enabled    = m_frameworkEnabled,
							configs	   = m_frameworkConfigs,
							playerMode = m_playerMode,
							jumpsuit   = m_jumpsuitOn,
							anythingEquipped = m_anythingEquipped
						   }
	local event = KEP_EventCreate("FrameworkState")
	KEP_EventEncodeString(event, cjson.encode(frameworkState))
	KEP_EventQueue(event)
end

frameworkRequestStateHandler = function()
	local frameworkState = {enabled    = m_frameworkEnabled,
							configs	   = m_frameworkConfigs,
							playerMode = m_playerMode,
							jumpsuit   = m_jumpsuitOn,
							anythingEquipped = m_anythingEquipped
						   }
	local event = KEP_EventCreate("FRAMEWORK_RETURN_STATE")
	KEP_EventEncodeString(event, cjson.encode(frameworkState))
	KEP_EventQueue(event)
end

resetAFK = function()
	m_AFKDeadline = m_currTime + AFK_LIMIT
	if m_AFKStatus == true then
		m_AFKStatus = false
		Events.sendEvent("FRAMEWORK_UPDATE_AFK_STATUS", {AFK = false})
	end
end

leaveVehicle = function()
	Events.sendEvent("FRAMEWORK_PLAYER_LEAVE_VEHICLE", {})
end

-- Clean up map-related menus when exiting build mode after deactivating gaming - use this if you need to disable map for JUST build mode
-- function BuildExitEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
-- 	if not m_frameworkEnabled and MenuIsOpen("Framework_MapDataHandler.xml") then
-- 		MenuClose("Framework_MapDataHandler.xml")
-- 	end
-- 	enableMap()
-- end

enableMap = function()
	-- Map feature runs on game worlds and non-game worlds
	if not GetHideMapSetting() and not MenuIsOpen("Framework_MapIcon.xml") then -- Respect hide map icon setting
		MenuOpen("Framework_MapIcon.xml")
	end
end