--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\ObjectType.lua")

-- Dynamic Object Types (from DynamicObj.h)
DYNAMIC_OBJTYPE_INVALID = 0
DYNAMIC_OBJTYPE_NORMAL = 1
DYNAMIC_OBJTYPE_MEDIA_TV = 2 -- normal tv (not frame or game)
DYNAMIC_OBJTYPE_MEDIA_FRAME = 3 -- glids { 1770, 1771, 1772, 2999 }
DYNAMIC_OBJTYPE_MEDIA_GAME = 4 -- isInteractive

--[[ DRF - Static object functions logging. Only call from within this api. ]]
function ofLog(txt) KEP_Log(LogName().."ObjectFunctions::"..txt) end
function ofLogWarn(txt) KEP_LogWarn(LogName().."ObjectFunctions::"..txt) end
function ofLogError(txt) KEP_LogError(LogName().."ObjectFunctions::"..txt) end

--[[ DRF - Convert object position and rotation into string for logging ]]
function ObjectPosRotToString(objPos, objRot)
	local str = ""
	if (objPos ~= nil) then 
		str = str.." "..VecToString(objPos)
	end
	if (objRot ~= nil) then 
		str = str.." "..VecRotToString(objRot)
	end
	return str
end

--[[ DRF - Convert object type into string for logging ]]
function ObjectTypeString(obj)
	if (obj.type == ObjectType.NONE) then
		return "NONE"
	elseif (obj.type == ObjectType.WORLD) then
		return "WORLD"
	elseif (obj.type == ObjectType.DYNAMIC) then
		return "DYNAMIC"
	elseif (obj.type == ObjectType.ENTITY) then
		return "ENTITY"
	elseif (obj.type == ObjectType.SOUND) then
		return "SOUND"
	elseif (obj.type == ObjectType.EQUIPPABLE) then
		return "EQUIPPABLE"
	else
		return "UNKNOWN"
	end
end

function ObjectTypeNumberValueString(type)
	if (type == ObjectType.NONE) then
		return "NONE"
	elseif (type == ObjectType.WORLD) then
		return "WORLD"
	elseif (type == ObjectType.DYNAMIC) then
		return "DYNAMIC"
	elseif (type == ObjectType.ENTITY) then
		return "ENTITY"
	elseif (type == ObjectType.SOUND) then
		return "SOUND"
	elseif (type == ObjectType.EQUIPPABLE) then
		return "EQUIPPABLE"
	else
		return "UNKNOWN"
	end
end

--[[ DRF - Convert object into string for logging ]]
function ObjectToString(objId, objType)
	local obj
	if (type(objId) == "table") then
		obj = objId
	elseif (tonumber(objId) > 0) then
		obj = ObjectGetInfo(tonumber(objId), objType or ObjectType.DYNAMIC)
	else
		return "Object <nil>"
	end
	
	local objTypeStr = ObjectTypeString(obj)
	
	local objAssetIdStr = ""
	if (obj.assetId > 0) then 
		objAssetIdStr = " assetId="..obj.assetId
	end
	
	local objTexUrlStr = ""
	if (obj.texUrl ~= "") then 
		objTexUrlStr = " texUrl='"..obj.texUrl.."'" 
	end
	
	if (obj.type == ObjectType.NONE) then
		return "Object <"..obj.id.." "..objTypeStr..">"
	elseif (obj.type == ObjectType.WORLD) then
		return "Object <"..obj.id.." "..objTypeStr..objAssetIdStr..objTexUrlStr..">"
	elseif (obj.type == ObjectType.DYNAMIC) then
		return "Object <"..obj.id..":"..obj.glid.." "..objTypeStr.." '"..obj.name.."'"..ObjectPosRotToString(obj.pos, obj.rot)..objAssetIdStr..objTexUrlStr..">"
	elseif (obj.type == ObjectType.ENTITY) then
		return "Object <"..obj.id.." "..objTypeStr..">"
	elseif (obj.type == ObjectType.SOUND) then
		return "Object <"..obj.id..":"..obj.glid.." "..objTypeStr.." '"..obj.name.."'"..ObjectPosRotToString(obj.pos, obj.rot)..">"
	elseif (obj.type == ObjectType.EQUIPPABLE) then
		return "Object <"..obj.id.." "..objTypeStr.." "..ObjectPosRotToString(obj.pos, obj.rot)..">"
	end
	return "Object <id="..obj.id.." type="..obj.type..">"
end
function ObjectToStr(objId, objType) return ObjectToString(objId, objType) end 

--[[ DRF - Return if given object exists in world or not. ]]
function ObjectExists(objId, objType)
	return ToBool(KEP_ObjectExists(objType or ObjectType.DYNAMIC, objId))
end

--[[ DRF - Return all information of given object. ]]
function ObjectGetInfo(objId, objType)
	objType = objType or ObjectType.DYNAMIC -- todo get type from id

	-- Object Defaults
	local obj = { 
		exists = false,
		id = objId, 
		type = objType, 
		glid = 0, 
		name = "",
		desc = "", 
		assetId = 0, 
		friendId = 0, 
		invType = GameGlobals.IT_NORMAL, 
		customTexture = 0, 
		texUrl = "", 
		posValid = false,
		pos = { x=0, y=0, z=0 },
		rotValid = false,
		rot = { x=0, y=0, z=0 },
		isMediaPlayer = 0, 
		isAttachable = 0, 
		isInteractive = 0, 
		isLocal = 0, 
		isDerivable = 0, 
		gameItemId = 0,
		animationGlid = 0,
		animationName = "",
		particleGlid = 0, 
		particleName = "",
		scriptName = "",
		playerId = 0,
		dynObjType = DYNAMIC_OBJTYPE_INVALID,
		getset = nil
	}
	
	-- Does Object Exist In World ?
	obj.exists = ObjectExists(obj.id, obj.type)
	if not obj.exists then
		ofLogWarn("ObjectGetInfo: Object Does Not Exist - objId="..obj.id.." objType="..obj.type)
		return obj
	end
		
	-- Get Object Information
	if (obj.type == ObjectType.DYNAMIC) then
		obj.name, 
		obj.desc, 
		obj.isMediaPlayer, 
		obj.isAttachable, 
		obj.isInteractive, 
		obj.isLocal, 
		obj.isDerivable, 
		obj.assetId, 
		obj.friendId, 
		obj.customTexture, 
		obj.glid, 
		obj.invType, 
		obj.texUrl, 
		obj.gameItemId, 
		obj.playerId,
		obj.dynObjType = KEP_DynamicObjectGetInfo(obj.id)
		obj.animationGlid, obj.animationName = KEP_GetDynamicObjectAnimation(obj.id)
		obj.particleGlid, obj.particleName = KEP_GetDynamicObjectParticleSystem(obj.id)
		obj.posValid = true
		obj.rotValid = true
	elseif (obj.type == ObjectType.SOUND) then
		obj.getset = KEP_SoundPlacementGet(obj.id)
		obj.name, obj.glid = KEP_SoundPlacementGetInfo(obj.id)
		obj.desc = "SOUND<"..tostring(obj.id)..">"
		obj.posValid = true
		obj.rotValid = true
	elseif (obj.type == ObjectType.WORLD) then
		obj.name = "WORLD<"..tostring(obj.id)..">"
		obj.desc = obj.name
		obj.assetId, 
		obj.customTexture, 
		obj.canHangPictures, 
		obj.canChangeTexture, 
		obj.texUrl = KEP_GetWorldObjectInfo(obj.id)
		obj.customizable = obj.canChangeTexture -- legacy compatibility
	elseif (obj.type == ObjectType.EQUIPPABLE) then
		obj.name = "EQUIPPABLE<"..tostring(obj.id)..">"
		obj.desc = obj.name
		obj.posValid = true
		obj.rotValid = true
	end

	-- Get Object Position & Rotation If Valid
	if obj.posValid then
		obj.pos = ObjectGetPosition(obj.id, obj.type)
	end
	if obj.rotValid then
		obj.rot = ObjectGetRotation(obj.id, obj.type)
	end

	return obj
end

--[[ DRF - Use given object by GLID from inventory. ]]
function ObjectUseInventory(objGlid, invType)
	KEP_UseInventoryItem(objGlid, invType or 0)
end

--[[ DRF - Add given object by GLID and inventory type. ]]
function ObjectAdd(objId, objType, invType)
	objType = objType or ObjectType.DYNAMIC -- todo get type from id
	if (objType == ObjectType.DYNAMIC) then
		KEP_AddDynamicObjectPlacement(objId, invType or 0)
	elseif (objType == ObjectType.SOUND) then
		KEP_SoundPlacementAdd(objId)
	end
end

--[[ DRF - Delete given object by placement id. ]]
function ObjectDel(objId, objType, isGameItem)
	objType = objType or ObjectType.DYNAMIC -- todo get type from id
	if (objType == ObjectType.DYNAMIC) then
		KEP_DeselectObject(objType, objId)
		if isGameItem ~= true then
			KEP_DeleteDynamicObjectPlacement(objId)
		end
	elseif (objType == ObjectType.SOUND) then
		KEP_DeselectObject(objType, objId)
		KEP_SoundPlacementDel(objId)
	    if objId > 0 then
			local e = KEP_EventCreate( "RemoveDynamicObjectEvent" )
			KEP_EventSetObjectId(e, objId)
			KEP_EventAddToServer(e)
			KEP_EventQueue(e)

			local e = KEP_EventCreate( "RemoveSoundCustomizationEvent" )
			KEP_EventSetObjectId(e, objId)
			KEP_EventAddToServer(e)
			KEP_EventQueue(e)
	    end
	end
end

--[[ DRF - Return position of given object. ]]
function ObjectGetPosition(objId, objType)
	local pos = {}
	pos.x, pos.y, pos.z = KEP_ObjectGetPosition(objType or ObjectType.DYNAMIC, objId)
	return pos
end

--[[ DRF - Set position of given object. ]]
function ObjectSetPosition(objId, objType, pos)
	objType = objType or ObjectType.DYNAMIC -- todo get type from id
	if (objType == ObjectType.EQUIPPABLE) then
		local e = KEP_EventCreate("EquippableItemTranslateEvent")
		KEP_EventEncodeNumber(e, pos.x)
		KEP_EventEncodeNumber(e, pos.y)
		KEP_EventEncodeNumber(e, pos.z)
		KEP_EventQueue(e)
	else
		KEP_ObjectSetPosition(objType, objId, pos.x, pos.y, pos.z)
	end
end

--[[ DRF - Return position (centroid) of all given objects. ]]
function ObjectsGetPosition(objs)
	local pos = { x=0, y=0, z=0 }
	if (#objs < 1) then return pos end
	
	-- Calculate Objects Position (centroid)
	local num = 0
	for i,v in ipairs(objs) do
		if v.posValid then
			pos.x = pos.x + v.pos.x
			pos.y = pos.y + v.pos.y
			pos.z = pos.z + v.pos.z
			num = num + 1
		end
	end
	if (num > 0) then
		pos.x = pos.x / num
		pos.y = pos.y / num
		pos.z = pos.z / num
	end

	return pos
end

--[[ DRF - Return rotation of given object. ]]
-- DRF_NEW_ANGLE BUG FIX
-- NOTE: We must add (g_PI / 2) to Y on Get to match what is expected from Set!
function ObjectGetRotation(objId, objType)
	local rot = {}
	rot.x, rot.y, rot.z = KEP_ObjectGetRotation(objType or ObjectType.DYNAMIC, objId)
	rot.x = RadNorm(rot.x)
	rot.y = RadNorm(rot.y + (g_PI / 2))
	rot.z = RadNorm(rot.z)
	return rot
end

--[[ DRF - Set rotation of given object. ]]
function ObjectSetRotation(objId, objType, rot)
	rot.x = RadNorm(rot.x)
	rot.y = RadNorm(rot.y)
	rot.z = RadNorm(rot.z)
	KEP_ObjectSetRotation(objType or ObjectType.DYNAMIC, objId, rot.x, rot.y, rot.z)
end

--[[ DRF - Return rotation (centroid) of all given objects. ]]
function ObjectsGetRotation(objs)
	local rot = { x=0, y=0, z=0 }
	if (#objs < 1) then return rot end
	
	-- Calculate Objects Rotation (centroid)
	local num = 0
	for i,v in ipairs(objs) do
		if v.rotValid then
			rot.x = rot.x + v.rot.x
			rot.y = rot.y + v.rot.y
			rot.z = rot.z + v.rot.z
			num = num + 1
		end
	end
	if (num > 0) then
		rot.x = rot.x / num
		rot.y = rot.y / num
		rot.z = rot.z / num
	end

	return rot
end

--[[ DRF - Return position and rotation of given object. ]]
function ObjectGetPositionAndRotation(objId, objType)
	local pos = ObjectGetPosition(objId, objType)
	local rot = ObjectGetRotation(objId, objType)
	return pos, rot
end

--[[ DRF - Sets position and rotation of given object. ]]
function ObjectSetPositionAndRotation(objId, objType, pos, rot)
	ObjectSetPosition(objId, objType, pos)
	ObjectSetRotation(objId, objType, rot)
end

--[[ DRF - Set texture of given object. (texGlid<=0 uses default texture) ]]
function ObjectSetTexture(objId, objType, texGlid, texUrl)
	objType = objType or ObjectType.DYNAMIC -- todo get type from id
	texGlid = texGlid or 0
	texUrl = texUrl or ""
	if (objType == ObjectType.DYNAMIC) then
		KEP_ApplyCustomTexture(objId, texGlid, GameGlobals.FULLSIZE, texUrl)
	elseif (objType == ObjectType.WORLD) then
		KEP_ApplyCustomTextureWldObject(objId, texGlid, GameGlobals.FULLSIZE)
	end
	ofLog("ObjectSetTexture: OK - "..ObjectToString(objId, objType).." texGlid="..texGlid)
end

--[[ DRF - Face player toward given object. ]]
function ObjectFace(objId, objType)
	if (objId == nil or objId < 0) then return end

	-- Get Object Position
	local objPos = ObjectGetPosition(objId, objType or ObjectType.DYNAMIC)
	if (objPos == nil) then return end

	-- Get Player Position
	local player = KEP_GetPlayer()
	if (player == nil) then return end
	local px, py, pz = KEP_GetPlayerPosition(player)
	if (px == nil or py == nil or pz == nil) then return end

	-- Face Player Toward Object (keep y same as player to not tilt player)
	KEP_SetPlayerLookAt( objPos.x, objPos.y, objPos.z)

	ofLog("ObjectFace: player=["..px..","..py..","..pz.."] -> object=["..objPos.x..","..objPos.y..","..objPos.z.."]")
end
function FaceObject(objId) ObjectFace(objId) end -- DEPRECATED - Use ObjectFace() Instead!

-------------------------------------------------------------------------------
-- Object Selected Functions
-------------------------------------------------------------------------------

--[[ DRF - Returns number of objects selected by user. ]]
function ObjectsSelected()
	return KEP_GetNumSelected()
end

--[[ DRF - Get object selected by user by index (1..ObjectsSelected()). ]]
function ObjectSelected(index)
	local objects = ObjectsSelected()
	if ((index == nil) or (index < 1) or (index > objects)) then
		ofLogError("ObjectSelected: Invalid index="..tostring(index).." objects="..objects)
		return nil
	end
	local obj = {}
	obj.type, obj.id = KEP_GetSelection(index - 1)
	obj = ObjectGetInfo(obj.id, obj.type)
	return obj
end
function SelectedObject(index) return ObjectSelected(index) end -- DEPRECATED - Use ObjectSelected() Instead!

--[[ DRF - Log all objects selected by user up to objectsMax (default=5). ]]
function ObjectsSelectedLog(objectsMax)
	local objects = ObjectsSelected()
	if (objects == 0) then
		ofLog("ObjectSelectedLog: objects=0")
	elseif (objects == 1) then
		local obj = ObjectSelected(1)
		ofLog("ObjectSelectedLog: objects=1 - "..ObjectToString(obj.id, obj.type))
	else
		ofLog("ObjectSelectedLog: objects="..objects)
		local maxed = false
		objectsMax = objectsMax or 5
		if (objects > objectsMax) then 
			objects = objectsMax 
			maxed = true
		end
		for i = 1, objects do
			local obj = ObjectSelected(i)
			ofLog(" ... "..ObjectToString(obj.id, obj.type))
		end
		if (maxed == true) then
			ofLog(" ... ")
		end
	end
end
function ObjectSelectedLog() ObjectsSelectedLog() end -- DEPRECATED - Use ObjectsSelectedLog() Instead!
function SelectedObjectsLog() ObjectsSelectedLog() end -- DEPRECATED - Use ObjectsSelectedLog() Instead!

--[[ DRF - Return information on all selected objects except given type. ]]
function ObjectsSelectedGetInfoExcept(objTypeExc)

	-- Get Number Of Selected Objects
	local objects = ObjectsSelected()
	if (objects < 1) then return {} end
	
	-- Get All Selected Objects Info Except Given Type
	local objs = {}
	for i = 1, objects do
		local obj = ObjectSelected(i)
		if (obj.type ~= objTypeExc) then
			table.insert(objs, ObjectSelected(i))
		end
	end
	return objs
end

--[[ DRF - Return information on all selected objects. ]]
function ObjectsSelectedGetInfo()
	return ObjectsSelectedGetInfoExcept(nil)
end

--[[ DRF - Return true if all objects selected by user includes the given object. ]]
function ObjectsSelectedIncludes(objId)
	
	-- Get Number Of Selected Objects
	local objects = ObjectsSelected()
	if (objects < 1) then 
		ofLog("ObjectsSelectedIncludes: "..ObjectToString(objId).." - NO OBJECTS SELECTED")
		return false 
	end
	
	-- Is Given Object Included ?
	for i = 1, objects do
		local obj = ObjectSelected(i)
		if (objId == obj.id) then 
			ofLog("ObjectsSelectedIncludes: "..ObjectToString(obj.id, obj.type).." - YES")
			return true 
		end
	end
	ofLog("ObjectsSelectedIncludes: "..ObjectToString(objId).." - NO")
	return false
end
function ObjectSelectedIncludes(objId) return ObjectsSelectedIncludes(objId) end -- DEPRECATED - Use ObjectsSelectedIncludes() Instead!
function SelectedObjectsIncludes(objId) return ObjectsSelectedIncludes(objId) end -- DEPRECATED - Use ObjectsSelectedIncludes() Instead!

--[[ DRF - Return position (centroid) of all selected objects. ]]
function ObjectsSelectedGetPosition()
	return ObjectsGetPosition(ObjectsSelectedGetInfo())
end

--[[ DRF - Return rotation (centroid) of all selected objects. ]]
function ObjectsSelectedGetRotation()
	return ObjectsGetRotation(ObjectsSelectedGetInfo())
end

--[[ DRF - Return position and rotation (centroids) of all selected objects. ]]
function ObjectsSelectedGetPositionAndRotation()
	local pos = ObjectsSelectedGetPosition()
	local rot = ObjectsSelectedGetRotation()
	return pos, rot
end
