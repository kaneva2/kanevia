--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_ActorHelper.lua
-------------------------------------------------------------------------------

-----------
-- MENUS --
-----------

ACTOR_MENUS = { "Framework_NPCAction.xml",
				"Framework_NPCGifting.xml",
				"Framework_NPCDialog.xml",
				"Framework_Vendor.xml",
				"Framework_SingleItemVendor.xml",
				"Framework_RandomVendor.xml",
				"Framework_CreditsVendor.xml",
				"Framework_GemVendor.xml",
				"Framework_ClothingVendor.xml",
				"Framework_TimedResource.xml",
				"Framework_QuestTurnIn.xml",
				"Framework_QuestInfo.xml",
				"Framework_QuestGiver.xml",
				"Framework_QuestDeliver.xml",
				"Framework_Repair.xml"
}

VENDOR_MENUS = {"Framework_Vendor.xml",
				"Framework_SingleItemVendor.xml",
				"Framework_RandomVendor.xml",
				"Framework_CreditsVendor.xml",
				"Framework_GemVendor.xml",
				"Framework_ClothingVendor.xml",
				"Framework_TimedResource.xml",
				"Framework_Repair.xml" -- Include repair in here as repair needs to close based on backpack
}

---------------
-- Functions --
---------------

function CloseAllActorMenus()
	local menuName = MenuNameThis()
	for index, menu in pairs(ACTOR_MENUS) do
		if menu ~= menuName then
			MenuClose(menu)
		end
	end
end

function isActorMenuOpen(onlyVendors)
	local menus = ACTOR_MENUS
	if onlyVendors then
		menus = VENDOR_MENUS
	end
	for index, menu in pairs(menus) do
		if MenuIsOpen(menu) then
			return true
		end
	end
	return false
end

function onCloseMenu(event)
	if not MenuIsOpen("UnifiedInventory") then
		MenuCloseThis()
	end
end