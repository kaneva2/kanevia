--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile( "..\\ClientScripts\\MenuGameIds.lua" )
dofile("MenuStyles.lua")
dofile("CreditBalances.lua")
dofile("MenuLocation.lua")
dofile("CAM.lua")
dofile("..\\ClientScripts\\ObjectGroups.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

TRYON_DURATION = 300	-- 5-min try-on

TAB_MY_ANIM = 1
TAB_AVAIL_ANIM = 2

g_placementId = nil
g_savedAnimGLID = nil
g_tryOnExpirationEvent = nil

g_devMode = false

g_t_anims = {}
g_t_anims_for_item = {}
g_t_anims_to_buy = {}
g_t_anims_owned = {}
g_t_anims_player = {}
g_userID = 0
g_playerName = 0
g_do_glid = 0
g_reRequestInv = false
g_current_tab = TAB_MY_ANIM

ITEM_INFO_EVENT = "ItemInfoEvent"
ITEM_SELECTED_EVENT = "SelectedItemInfoEvent"

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandlerFiltered( "attribEventHandler", "AttribEvent", AttribEvent.PLAYER_INVEN, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "itemInfoHandler", ITEM_SELECTED_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "expiredTryOnHandler", KEP.TRYON_INVENTORY_EXPIRE_EVENT, KEP.FILTER_TRYON_EXPIRE, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "TryOnExpirationHandler", "DOAnimationTryOnExpirationEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( ITEM_SELECTED_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "DOAnimationTryOnExpirationEvent", KEP.MED_PRIO )
	KEP_EventRegister( "UpdateAnimation", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	g_devMode = ToBool(DevMode())

	local lbh = Dialog_GetListBox(dialogHandle, "lstAnimations")
	if lbh ~= 0 then
		local sbh = ListBox_GetScrollBar(lbh)
		if sbh ~= 0 then
			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end
			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end

	-- DRF - Not Sure Why We Don't Call getPlayerInventory() Directly Here
	getUserID(KEP_GetLoginName(), WF.INVENTORY)

	-- DRF - Also Offer All Player Animations In Dev Mode
	if g_devMode then
		getPlayerInventory(WF.PLAYER_EMOTES, 1, 100000, "avatar", "", "emotes")
	end

	RequestInventory()
end

function Dialog_OnDestroy(dialogHandle)
	if g_tryOnExpirationEvent~=nil then
		KEP_EventCancel( g_tryOnExpirationEvent )
		g_tryOnExpirationEvent = nil
	end
	if g_placementId~=nil and g_savedAnimGLID~=nil then
		-- call SetAnimation to revert animation in case try-on is in progress
		SetAnimation( g_savedAnimGLID )
	end
	Helper_Dialog_OnDestroy( dialogHandle )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.ANIMATIONS_LIST then
		local xmlData = KEP_EventDecodeString( event )
		ParsePlayerInventoryItems( xmlData )
		return KEP.EPR_CONSUMED
	elseif filter == WF.INVENTORY then
		getPlayerInventory(WF.ANIMATIONS_LIST, nil, nil, nil, nil, "Object Animations", USE_TYPE.ANIMATION)
		return KEP.EPR_CONSUMED
	elseif filter == WF.ITEM_ANIMATIONS then
		local xmlData = KEP_EventDecodeString( event )
		ParseItemAnimations( xmlData )
		return KEP.EPR_CONSUMED
	elseif filter == WF.PLAYER_EMOTES then
		if not g_devMode then return end
		local xmlData = KEP_EventDecodeString( event )
		ParsePlayerAnimations(xmlData)
		return KEP.EPR_CONSUMED
	end
end

function RequestInventory()
	ResetInventory()
	g_t_anims = {}
	KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
end

function ParsePlayerInventoryItems( xmlData )
	g_t_anims = {}
	decodeInventoryItemsFromWeb( g_t_anims, xmlData, false )
	populateAnims()
end

function ParseItemAnimations( xmlData )
	g_t_anims_for_item = {}
	decodeItemAnimationsFromWeb(g_t_anims_for_item, xmlData)
	populateAnims()
end

function ParsePlayerAnimations( xmlData )
	if not g_devMode then return end
	g_t_anims_player = {}
	decodeInventoryItemsFromWeb( g_t_anims_player, xmlData, true )
	populateAnims()
end

function ParseUserID( browserString )
	local result = nil -- result description
	local s, e = 0     -- start and end of captured string
	s, e, result = string.find(browserString, "<ReturnCode>(%d+)</ReturnCode>")
	if result == "0" then
		s, e, result = string.find(browserString, "<user_id>(.-)</user_id>", e)
		g_userID = tonumber(result)
	end
	getBalances(g_userID, WF.INVENTORY)
end

function separateNotOwned()
	local t_result = {}
	if g_t_anims_for_item ~= nil then
		for i=1,#g_t_anims_for_item do
			local owned = 0
			t_avail = g_t_anims_for_item[i]
			for j=1,#g_t_anims do
				t_owned = g_t_anims[j]
				if t_owned["GLID"] == t_avail["GLID"] then
					owned = 1
				end
			end
			if owned == 0 and t_avail["item_active"] == 1 then
				table.insert(t_result, t_avail)
			end
		end
	end
	g_t_anims_to_buy = t_result
end

function separateOwned()
	local t_result = {}
	if g_t_anims_for_item ~= nil then
		for i=1,#g_t_anims_for_item do
			local owned = 0
			t_avail = g_t_anims_for_item[i]
			for j=1,#g_t_anims do
				t_owned = g_t_anims[j]
				if t_owned["GLID"] == t_avail["GLID"] then
					owned = 1
				end
			end
			if owned == 1 then
				table.insert(t_result, t_avail)
			end
		end
	end
	g_t_anims_owned = t_result
end

function get4SellByGlid(glid)
	if g_t_anims_for_item ~= nil then
		for i=1,#g_t_anims_for_item do
			t_avail = g_t_anims_for_item[i]
			if t_avail["GLID"] == glid then
				return t_avail
			end
		end
	end
	return nil
end

function ResetInventory()
	-- always remove all items (the list will repopulate when the request is answered)
	local lbh = Dialog_GetListBox(gDialogHandle, "lstAnimations")
	if lbh ~= 0 then
		ListBox_RemoveAllItems(lbh)
	end
end

function clearSelInfo()
	local lbh = Dialog_GetListBox(gDialogHandle, "lstAnimations")
	ListBox_ClearSelection( lbh )
	local edt = Dialog_GetEditBox(gDialogHandle, "ebxGLID")
	EditBox_SetText(edt, " ", false)
end

function btnAnimTab_OnButtonClicked(buttonHandle)
	g_current_tab = TAB_MY_ANIM
	clearSelInfo()
	populateAnims()
	enableControl("btnAnimTab", 0) --NEW LINE
	enableControl("btnBuyTab", 1) --NEW LINE
end

function btnBuyTab_OnButtonClicked(buttonHandle)
	g_current_tab = TAB_AVAIL_ANIM
	clearSelInfo()
	populateAnims()
	enableControl("btnAnimTab", 1) --NEW LINE
	enableControl("btnBuyTab", 0) --NEW LINE
end

function btnUseSelected_OnButtonClicked(buttonHandle)
	local lbh = Dialog_GetListBox(gDialogHandle, "lstAnimations")
	if lbh ~= 0 then
		local animGLID = ListBox_GetSelectedItemData(lbh)
		if animGLID >= 0 then
			if g_current_tab == TAB_MY_ANIM then
				SetAnimation( animGLID )
				g_savedAnimGLID = animGLID
				MenuCloseThis()
			elseif g_current_tab == TAB_AVAIL_ANIM then
				local item = get4SellByGlid(animGLID)
				if item ~= nil then
					MenuOpen("Checkout.xml")

					local ev = KEP_EventCreate( ITEM_INFO_EVENT )
					KEP_EventEncodeNumber( ev, item["type"] )
					KEP_EventEncodeNumber( ev, 1) -- num items
					KEP_EventEncodeNumber( ev, animGLID)
					KEP_EventEncodeNumber( ev, 1) --quantity of item
					KEP_EventEncodeString( ev, item["name"] ) -- name
					KEP_EventEncodeNumber( ev, item["market_cost"] ) -- price
					KEP_EventQueue( ev )
				end
			end
		end
	end
end

function SetAnimation( animGLID )
	if g_placementId ~= 0 then
		Log("SetAnimation: glid<"..animGLID.."> "..ObjectToStr(g_placementId, ObjectType.DYNAMIC))
		
		local e = KEP_EventCreate( "ControlDynamicObjectEvent")
		KEP_EventSetObjectId(e, g_placementId)
		KEP_EventSetFilter(e, KEP.ANIM_START)
		KEP_EventEncodeNumber(e, 1) -- argument count
		KEP_EventEncodeNumber(e, animGLID)
		KEP_EventAddToServer(e)
		KEP_EventQueue( e)

		local updateAnim = KEP_EventCreate( "UpdateAnimation")
		KEP_EventEncodeNumber(updateAnim, animGLID)
		KEP_EventQueue( updateAnim)
	else
		KEP_PlayerObjectSetEquippableAnimation(  KEP_GetPlayer( ), g_do_glid, animGLID )
	end
end

function populateAnims()
	local lbh = Dialog_GetListBox(gDialogHandle, "lstAnimations")
	local bh = Dialog_GetButton(gDialogHandle, "btnUseSelected")
	local sh = Button_GetStatic(bh)
	ListBox_RemoveAllItems(lbh)
	if g_current_tab == TAB_MY_ANIM then
		separateOwned()
		ListBox_AddItem( lbh, "[none]", 0 )
		for i=1, #g_t_anims_owned do
			local item = g_t_anims_owned[i]
			ListBox_AddItem(lbh, item["name"], item["GLID"])
		end
		if g_devMode then
			ListBox_AddItem( lbh, "--------------------DevMode--------------------", 0 )
			for i=1, #g_t_anims_player do
				local item = g_t_anims_player[i]
				ListBox_AddItem(lbh, item["name"], item["GLID"])
			end
		end
		Static_SetText(sh, "Use Selected")
	elseif g_current_tab == TAB_AVAIL_ANIM then
		separateNotOwned()
		if g_t_anims_to_buy ~= nil then
			for i=1,#g_t_anims_to_buy do
				local t_item = g_t_anims_to_buy[i]
				ListBox_AddItem(lbh, t_item["name"] .. "(" .. t_item["market_cost"] .. ")", t_item["GLID"])
			end
		end
		Static_SetText(sh, "buy selected")
	end
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == AttribEvent.PLAYER_INVEN then
		if g_reRequestInv then
			g_reRequestInv = false
			RequestInventory()
		else
			local lbh = Dialog_GetListBox(gDialogHandle, "lstAnimations")
			if lbh ~= nil and lbh ~= 0 then
				ListBox_RemoveAllItems(lbh)
			end
			getPlayerInventory(WF.ANIMATIONS_LIST, nil, nil, nil, nil, "Object Animations", USE_TYPE.ANIMATION)
		end
	end
end

function itemInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local itemQty = 0
	local itemPrice = 0
	local itemName = 0
	local inventoryType = KEP_EventDecodeNumber( event )
	local num_items = KEP_EventDecodeNumber( event )
	for i = 1, num_items do
		g_do_glid = KEP_EventDecodeNumber( event )
		g_placementId = KEP_EventDecodeNumber( event )
		itemQty = KEP_EventDecodeNumber( event )
		itemName = KEP_EventDecodeString( event )
		itemPrice = KEP_EventDecodeNumber( event )
	end

	-- Selection isn't important, what's important is the item that the RCM
	-- knows about when the 'choose animation' button is pressed and this menu
	-- is created.
	g_savedAnimGLID = KEP_GetDynamicObjectAnimation( g_placementId )
	getItemAnimations(WF.ITEM_ANIMATIONS, g_do_glid)
	
	MenuBringToFrontThis()
	
	return KEP.EPR_CONSUMED
end

function lstAnimations_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index, sel_text)
	local animGLID = ListBox_GetItemData( listBoxHandle, sel_index )
	if g_current_tab == TAB_MY_ANIM then
		if g_tryOnExpirationEvent~=nil then
			KEP_EventCancel( g_tryOnExpirationEvent )
			g_tryOnExpirationEvent = nil
		end
		SetAnimation( animGLID )
	elseif g_current_tab == TAB_AVAIL_ANIM then
		-- send animation try-on request to server
		tryAnim( animGLID )
	end
	if KEP_IsWorld() == true then
		local edt = Dialog_GetEditBox(gDialogHandle, "ebxGLID")
		EditBox_SetText(edt, "GLID:" .. tostring(animGLID), false)
		showControl("ebxGLID")
		enableControl( "ebxGLID", true )
	end
end

function tryAnim( animGLID )

	-- (re)schedule an expiration event
	if g_tryOnExpirationEvent~=nil then
		KEP_EventReschedule( g_tryOnExpirationEvent, TRYON_DURATION )
	else
		g_tryOnExpirationEvent = KEP_EventCreate( "DOAnimationTryOnExpirationEvent" )
		KEP_EventSetObjectId( g_tryOnExpirationEvent, g_placementId )
		KEP_EventQueueInFuture( g_tryOnExpirationEvent, TRYON_DURATION )
	end

	-- set DO animation locally
	if g_placementId ~= 0 then
		KEP_SetAnimationOnDynamicObject( g_placementId, animGLID )
	else
		KEP_PlayerObjectTryEquippableAnimation( KEP_GetPlayer( ), g_do_glid, animGLID )
	end
end

function TryOnExpirationHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if objectid==g_placementId then

		-- revert animation
		SetAnimation( g_savedAnimGLID )

		-- clear selection in try-on animation list
		if g_current_tab == TAB_AVAIL_ANIM then
			local lbh = Dialog_GetListBox(gDialogHandle, "lstAnimations")
			ListBox_ClearSelection( lbh )
		end

		-- clear saved event
		g_tryOnExpirationEvent = nil
	end

	return KEP.EPR_OK
end
