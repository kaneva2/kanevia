--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_NameResolver.lua
-- 
-- Resolves name conflicts with Game Items by type
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

----------------------
-- Globals
----------------------
local m_mode, m_itemName, m_suggestedName, m_itemType, m_GLID, m_conflictingUNID, m_itemData

-- When the menu is created
function onCreate()
	-- Server event handlers
	Events.registerHandler("FRAMEWORK_RESOLVE_THIS", resolveThisHandler)
	Events.registerHandler("FRAMEWORK_UNIQUE_NAME_RETURN", uniqueNameReturnHandler)
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Catches relevant data for this menu to display
function resolveThisHandler(event)
	m_mode 			  = event.mode
	m_itemName 		  = event.name
	m_suggestedName   = event.suggestedName
	m_itemType 		  = event.itemType
	m_GLID			  = event.GLID
	m_conflictingUNID = event.conflictingUNID
	m_itemData		  = event.itemData
	
	-- Adjust toolbar text accordingly
	if m_mode == "SaveAs" or m_mode == "Save" then
		Dialog_SetCaptionText(gDialogHandle, "     Can't Save")
		Static_SetText(Control_GetStatic(gHandles["btnConfirm"]), "Save")
	elseif m_mode == "AddToGame" then
		Dialog_SetCaptionText(gDialogHandle, "     Can't Copy to Game")
		Static_SetText(Control_GetStatic(gHandles["btnConfirm"]), "Copy to Game")
	elseif m_mode == "CreateSpawner" then
		Dialog_SetCaptionText(gDialogHandle, "     Can't Create Spawn")
		Static_SetText(Control_GetStatic(gHandles["btnConfirm"]), "Create Spawn")
	elseif m_mode == "Place" then
		Dialog_SetCaptionText(gDialogHandle, "     Can't Place Item")
		Static_SetText(Control_GetStatic(gHandles["btnConfirm"]), "Place Item")
	end
	
	-- Set GLID icon
	local eh = Image_GetDisplayElement(gHandles["imgItem"])
	KEP_LoadIconTextureByID(m_GLID, eh, gDialogHandle)
	
	-- Capitilize first letter of the system
	local itemSystem = m_itemType:gsub("^%l", string.upper)
	Static_SetText(gHandles["stcText"], "An item named "..tostring(event.name).." already exists in your "..tostring(itemSystem).." System.")
	EditBox_SetText(gHandles["edNewName"], m_suggestedName, false)
	Static_SetText(gHandles["stcOverwriteInstructions"], "If you use the name \""..tostring(m_itemName).."\", the item in your "..tostring(itemSystem).." Systems will be overwritten")
end

-- Called when a unique name check has been completed
function uniqueNameReturnHandler(event)
	local original 	 	  = event.original
	local isUnique 	 	  = event.isUnique
	local uniqueName 	  = event.uniqueName
	
	-- Name is unique. Save away, save away, save away
	if isUnique then
		local newItem = m_itemData
		newItem.name = uniqueName
		Events.sendEvent("WOK_ADD_TO_GAME", {item = newItem, mode = m_mode, overwrite = false})
		
		MenuCloseThis()
	end
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

-- Save
function btnConfirm_OnButtonClicked(buttonHandle)
	local name = EditBox_GetText(gHandles["edNewName"])
	
	-- If the name input is the one offered as an overwrite prompt, overwrite it
	if name == m_itemName then
		menuHandle = MenuOpen("Framework_OverwriteConfirmation.xml")
		local event = KEP_EventCreate("FRAMEWORK_OVERWRITE_CONFIRMATION")
		KEP_EventEncodeString(event, name)
		KEP_EventEncodeString(event, m_itemType)
		KEP_EventEncodeNumber(event, m_GLID)
		KEP_EventEncodeNumber(event, m_conflictingUNID)
		KEP_EventEncodeString(event, m_mode)
		KEP_EventEncodeString(event, JSON.encode(m_itemData))
		KEP_EventQueue(event)

	-- New name input. Check it
	else
		-- Check if this name is unique
		Events.sendEvent("UNIQUE_NAME_CHECK", {originalName = m_suggestedName, item = m_itemData, name = name, mode = m_mode})
	end
end