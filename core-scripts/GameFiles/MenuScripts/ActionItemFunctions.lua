--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\ScriptServerEventTypes.lua")

ActionItemFunctions = {}

ActionItemFunctions.requiredProperties = {name = 1, defaultGLID = 1}
ActionItemFunctions.optionalProperties = {recommendedGLIDs = 1, description = 1, category = 1}
ActionItemFunctions.optionalMethods = {process=1}
ActionItemFunctions.requiredParamProperties = {name = 1, description = 1, type = 1}
ActionItemFunctions.optionalParamProperties = {value = 1, enabled = 1, advanced = 1, x = 1, y = 1, z = 1, dx = 0, dy = 0, dz = 1, ux = 0, uy = 1, uz = 0, choices = 1} -- modify to include all possible properties
ActionItemFunctions.jsonProperties = {name = 1, defaultGLID = 1, description = 1}
-- TODO table of tables associating param type with required fields

SCRIPT_SERVER_EVENT              = "ScriptServerEvent"
ACTION_ITEM_SCRIPT_NAME_EVENT    = "ActionItemScriptNameEvent"
EDIT_PARAM_EVENT                 = "ActionItemEditParamEvent"
CHANGE_PARAM_EVENT               = "ActionItemChangeParamEvent"
DISPLAY_ACTION_ITEM_ERRORS_EVENT = "DisplayActionItemErrorsEvent"
SCRIPT_NAME_KEY                  = "___ScriptName___"

function ActionItemFunctions.getActionItemTable(path)
	-- clear existing table from previous script
	AID = nil

	-- load action item table from script
	local AIDScriptText = KEP_GetActionItemDefinition( path )
	if AIDScriptText~="" then
		local f = loadstring(AIDScriptText)
		if f==nil then
			LogError( "getActionItemTable: error loading action item table - [" .. path .. "]" )
		else
			-- load action item table into current VM
			f()
		end
	else
		LogError( "getActionItemTable: action item table not found - [" .. path .. "]" )
	end
end

function ActionItemFunctions.verifyProperties()
	-- first check that all required properties found
	local errorResult = ""
	if AID ~= nil and AID.params ~= nil then
		local key, param
		local missingProperties = ""
		for key, param in pairs(ActionItemFunctions.requiredProperties) do
			if AID[key] == nil then
				missingProperties = missingProperties .. " " .. key
			end
		end
		if missingProperties ~= "" then
			errorResult = "Required properties missing: " .. missingProperties .. "\n"
		end
		-- checks for unrecognized action item properties.  This is redundant with KEP_ActionItemSyntaxCheck()
		local unrecognizedProperties = ""
		for key, param in pairs(AID) do
			if key ~= "params" and 
				not ActionItemFunctions.requiredProperties[key] and 
				not ActionItemFunctions.optionalProperties[key] and
				not ActionItemFunctions.optionalMethods[key]
			then
				unrecognizedProperties = unrecognizedProperties .. " " .. key
			end
		end
		if unrecognizedProperties ~= "" then
			errorResult = errorResult .. "Unrecognized action item properties: " .. unrecognizedProperties .. "\n"
		end
	else
		-- They must have put their actionitem definition inside a function or something...
		errorResult = "Unable to find AID table.  Action item definitions must have global scope.\n"
	end
	if errorResult == "" then
		return "Success"
	else
		return errorResult
	end
end

function ActionItemFunctions.verifyParams()
	local errorString = ""
	local key, param
	local missingProperties = ""
	for paramkey, param in pairs(AID.params) do
		local missingCount = 0
		local missingString = "For param '" .. paramkey .. "' required parameter property(s) missing: "
		for requiredkey, requiredparam in pairs(ActionItemFunctions.requiredParamProperties) do
			if param[requiredkey] == nil then
				missingCount = missingCount + 1
				missingString = missingString .. requiredkey .. " "
			end
		end
		local unrecognizedCount = 0
		local unrecognizedString = "For param '" .. paramkey .. "' unrecognized parameter property(s): "
		for propertykey, propertyvalue in pairs(param) do
			if ActionItemFunctions.requiredParamProperties[propertykey] == nil and ActionItemFunctions.optionalParamProperties[propertykey] == nil then
				unrecognizedCount = unrecognizedCount + 1
				unrecognizedString = unrecognizedString .. propertykey .. " "
			end
		end
		-- TODO right here look at type and check that all required params for that type present
		if missingCount > 0 then
			errorString = errorString .. missingString .. "\n"
		end
		if unrecognizedCount > 0 then
			errorString = errorString .. unrecognizedString .. "\n"
		end
	end
	if errorString == "" then
		return "Success"
	end
	return errorString
end

function ActionItemFunctions.errorCheckLua(path)
	local errorString = KEP_GetLuaCompilerOutput(path) .. "\n"
	if errorString ~= "Success\n" then
		return errorString
	end
	errorString = KEP_ActionItemSyntaxCheck(path)
	if errorString ~= "Success" then
		return errorString
	end
	ActionItemFunctions.getActionItemTable(path)
	errorString = ActionItemFunctions.verifyProperties()
	if errorString ~= "Success" then
		return errorString
	end
	errorString = ActionItemFunctions.verifyParams()
	if errorString ~= "Success" then
		return errorString
	end
	return nil
end

-- helper function to add param to an event
-- format key(string), propCount(number)
-- for each prop propKey(string), propValue(string)
function ActionItemFunctions.encodeParam(event, param, paramKey)
	local propKey, paramProp
	local paramPropCount = 0
	for propKey, paramProp in pairs(param) do
		paramPropCount = paramPropCount + 1
	end
	KEP_EventEncodeString(event, paramKey)
	KEP_EventEncodeNumber(event, paramPropCount)
	for propKey, paramProp in pairs(param) do
		KEP_EventEncodeString(event, propKey)
		KEP_EventEncodeString(event, type(paramProp))
		if propKey == "choices" then
			local choiceCount = #paramProp
			KEP_EventEncodeNumber(event, choiceCount)
			for i, v in ipairs(paramProp) do
				KEP_EventEncodeString(event, tostring(v))
			end
		else
			KEP_EventEncodeString(event, tostring(paramProp))
		end
	end
end

function ActionItemFunctions.decodeParam(event)
	local param = {}
	local key = KEP_EventDecodeString(event)
	local propCount = KEP_EventDecodeNumber(event)
	local i
	for i = 1, propCount do
		local prop = KEP_EventDecodeString(event)
		local typeStr = KEP_EventDecodeString(event)
		local value
		if prop == "choices" then
			local choiceCount = KEP_EventDecodeNumber(event)
			value = {}
			local j
			for j = 1, choiceCount do
				local choiceStr = KEP_EventDecodeString(event)
				table.insert(value, choiceStr)
			end
		else
			value = KEP_EventDecodeString(event)
		end
		if typeStr == "number" then
			-- convert numbers back to numbers
			value = tonumber(value)
		elseif typeStr == "boolean" then
			-- convert "true" and "false" to true and false
			if value == "true" then
				value = true
			else
				value = false
			end
		end
		param[prop] = value
	end
	return key, param
end

-- sends SaveActionItemEvent.
-- Old script path (string)
-- New script path (string)
-- property count (number)
-- for each property:
--		property name (string)
--      property type (string)
--		property value (string)
-- param count (number)
-- for each param:
--		param name (string)
--		param property count (number)
--		for each param property:
--			property name (string)
--			property type (string)
--			property value (string)
function ActionItemFunctions.saveLua(oldPath, newPath)
	if AID==nil or AID.params==nil then
		LogError("AIF.saveLua: AID or AID.params not defined for [" .. tostring(newPath) .. "]")
		return
	end

	local key, param, prop
	local propCount = 0
	local paramCount = 0
	for key, prop in pairs(AID) do
		if key ~= "params" and not ActionItemFunctions.optionalMethods[key] then
			propCount = propCount + 1
		end
	end
	for key, param in pairs(AID.params) do
		paramCount = paramCount + 1
	end

	local event = KEP_EventCreate( "SaveActionItemEvent")
	KEP_EventEncodeString(event, oldPath)
	KEP_EventEncodeString(event, newPath)
	KEP_EventEncodeNumber(event, propCount)

	for key, prop in pairs(AID) do
		if key ~= "params" and not ActionItemFunctions.optionalMethods[key] then
			KEP_EventEncodeString(event, key)				-- property key
			KEP_EventEncodeString(event, type(prop))		-- property type
			if type(prop) == "table" then
				-- special case of table convert it to a string... assumes no tables containing tables
				local tableStr = "{"
				local i, v
				for i, v in ipairs(prop) do
					if i ~= 1 then
						tableStr = tableStr .. ", "
					end
					tableStr = tableStr .. tostring(v)
				end
				tableStr = tableStr .. "}"
				KEP_EventEncodeString(event, tableStr)
			else
				KEP_EventEncodeString(event, tostring(prop))	-- property value as string
			end
		end
	end
	KEP_EventEncodeNumber(event, paramCount)
	for key, param in pairs(AID.params) do
		ActionItemFunctions.encodeParam(event, param, key)
	end
	KEP_EventQueue( event)
end

function ActionItemFunctions.requestServerActionItemList()
	local ev = KEP_EventCreate( SCRIPT_SERVER_EVENT)
	KEP_EventSetFilter(ev, ScriptServerEventTypes.GetAllScripts)
	KEP_EventAddToServer(ev)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventQueue( ev)
end

function ActionItemFunctions.attachScript(objectId, filename)
	local ev = KEP_EventCreate( SCRIPT_SERVER_EVENT)
	KEP_EventSetFilter(ev, ScriptServerEventTypes.AttachScript)
	KEP_EventAddToServer(ev)
	KEP_EventEncodeNumber(ev, objectId)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeString(ev, filename)
	KEP_EventQueue( ev)
end

function ActionItemFunctions.OpenPreset(name, assetType, scriptPath, jsonPath, placementId, editScriptNow)
	MenuOpenModal("PresetEditor.xml")

	local event = KEP_EventCreate( ACTION_ITEM_SCRIPT_NAME_EVENT )
	KEP_EventEncodeString( event, name )
	KEP_EventEncodeNumber( event, assetType )
	KEP_EventEncodeString( event, scriptPath )
	KEP_EventEncodeString( event, jsonPath or "" )
	KEP_EventEncodeNumber( event, placementId or -1)
	if editScriptNow then
		KEP_EventEncodeNumber( event, 1 )
	else
		KEP_EventEncodeNumber( event, 0 )
	end
	KEP_EventQueue( event )
end

function ActionItemFunctions.isTypeItem(type)
	if type == "MENS_EMOTE" or type == "WOMENS_EMOTE" or type == "OBJECT" or type == "SOUND" or type == "OBJECT_ANIMATION" or type == "MENS_CLOTHING" or type == "WOMENS_CLOTHING" or type == "PARTICLE" or type == "ACCESSORY" then
		return true
	else
		return false
	end
end

function ActionItemFunctions.CalculateBundleGlids()
	local bundleGlids = tostring(AID.defaultGLID)
	for paramkey, param in pairs(AID.params) do
		if ActionItemFunctions.isTypeItem(param.type) and param.value ~= 0 then
			bundleGlids = bundleGlids .. "," .. tostring(param.value)
		end
	end
	return bundleGlids
end

function ActionItemFunctions.GetOrderedParams(scriptPath)
	local paramCount, orderedResults = KEP_GetActionItemParamOrder(scriptPath)
	local orderedTable = {}
	local findStart = 1
	local findFinish = 1
	local paramStart = 1
	for i = 1, paramCount do
		findStart, findFinish = string.find(orderedResults, " ", paramStart)
		local param = string.sub(orderedResults, paramStart, findStart - 1)
		paramStart = findFinish + 1
		orderedTable[i] = param
	end
	return orderedTable
end

-- Helper function copied from Lib_ActionItem.lua
local function UpdateParamValue(AIDParam, value, enabled)

	if type(AIDParam.value)==type(value) then
		AIDParam.value = value
	elseif AIDParam.type == "COORDINATE" and type(value.x)=="number" and type(value.y)=="number" and type(value.z)=="number" then
		-- special case for coordinates
		AIDParam.x = value.x
		AIDParam.y = value.y
		AIDParam.z = value.z
	else
		return "bad value type"
	end

	if AIDParam.enabled ~= nil or enabled == false then		-- 'enabled' is optional (nil==true)
		AIDParam.enabled = enabled
	end
end

-- Helper function to extract AID values
local function GetParamValue(AIDParam)
	if AIDParam.type == "COORDINATE" then
		return { x = AIDParam.x, y = AIDParam.y, z = AIDParam.z }
	else
		return AIDParam.value
	end
end

-- Returns full path of saved JSON file
function ActionItemFunctions.saveJson(baseName, scriptGlidOrName, isInstance)
	if AID==nil or AID.params==nil then
		LogError("AIF.saveJson: AID or AID.params not defined for [" .. tostring(baseName) .. "]")
		return ""
	end

	if type(scriptGlidOrName)~="number" and type(scriptGlidOrName)~="string" then
		LogError("AIF.saveJson: script GLID is a " .. type(scriptGlidOrName) .. " value for [" .. tostring(baseName) .. "]")
		return ""
	end

	-- Pack AID properties and parameters into a temporary table
	local tbl = { script = scriptGlidOrName }
	for key, prop in pairs(AID) do
		if ActionItemFunctions.jsonProperties[key]~=nil then
			tbl[key] = prop
		end
	end

	tbl.params = {}
	for key, param in pairs(AID.params) do
		if param.enabled~=false then		-- true or nil
			tbl.params[key]=GetParamValue(param)
		else								-- false
			tbl.disabled = tbl.disabled or {}
			tbl.disabled[key]=GetParamValue(param)
		end
	end

	-- Encode into JSON
	jsonString = cjson.encode(tbl)

	-- Save
	local uploadType = DevToolsUpload.ACTION_ITEM_PARAMS
	if isInstance then
		uploadType = DevToolsUpload.ACTION_INSTANCE_PARAMS
	end

	return KEP_Import3DAppAssetFromString(uploadType, baseName, jsonString)
end

-- Read and parse JSON file
function ActionItemFunctions.parseJson( assetType, jsonPath )
	local json= KEP_ReadActionItemParamsFile( assetType, jsonPath )
	if not json or json=="" then
		LogError("AIF.parseJson: error reading params file: " .. tostring(jsonPath) .. ", type: " .. tostring(assetType))
		return
	end

	local tbl = cjson.decode( json )
	if not tbl or type(tbl)~="table" then
		LogError("AIF.parseJson: error decoding params file: " .. tostring(jsonPath))
		return
	end

	return tbl
end

-- Check for errors
function ActionItemFunctions.errorCheckJson(assetType, path)
	local tbl = ActionItemFunctions.parseJson(assetType, path)
	if not tbl then
		return "Internal error"
	end

	for propName, _ in pairs(ActionItemFunctions.jsonProperties) do
		if tbl[propName]==nil then
			LogError("AIF.errorCheckJson: missing property [" .. tostring(propName) .. "]")
			return "Internal error"
		end
	end

	return nil
end

-- Check for errors
function ActionItemFunctions.errorCheck(assetType, path)
	if string.match(path, "%.json$")==nil then
		return ActionItemFunctions.errorCheckLua(path)
	else
		return ActionItemFunctions.errorCheckJson(assetType, path)
	end
end

-- Return asset type, base name
function ActionItemFunctions.getScriptInfoByRelPath( relPath )
	local folder, name, ext = string.match(relPath, "([^\\/]+)[\\/]([^\\/]+)%.([^.]+)$")
	if folder~=nil and name~=nil and ext~=nil then
		ext = string.lower(ext)
		if folder=="ActionItems" then
			if ext=="lua" then
				return DevToolsUpload.ACTION_ITEM, name
			elseif ext=="json" then
				return DevToolsUpload.ACTION_ITEM_PARAMS, name
			end
		elseif folder=="ActionInstances" then
			if ext=="lua" then
				return DevToolsUpload.ACTION_INSTANCE, name
			elseif ext=="json" then
				return DevToolsUpload.ACTION_INSTANCE_PARAMS, name
			end
		end
	end

	LogError("AIF.getScriptInfoByRelPath: Invalid smart object path: " .. tostring(relPath))
	return DevToolsUpload.UNKNOWN
end

-- Return relative path based on asset type, base name
function ActionItemFunctions.getRelPathByScriptInfo( assetType, baseName )
	local extension = "lua"
	if assetType==DevToolsUpload.ACTION_ITEM_PARAMS or assetType==DevToolsUpload.ACTION_INSTANCE_PARAMS then
		extension = "json"
	end

	local folder = "ActionItems"
	if assetType==DevToolsUpload.ACTION_INSTANCE or assetType==DevToolsUpload.ACTION_INSTANCE_PARAMS then
		folder = "ActionInstances"
	end

	return folder .. "\\" .. baseName .. "." .. extension
end

-- Display the error dialog
function ActionItemFunctions.openErrorDialog( errorData, path, assetType )
	MenuOpenModal("ActionItemErrors.xml")
	
	local event = KEP_EventCreate( DISPLAY_ACTION_ITEM_ERRORS_EVENT )
	KEP_EventEncodeString( event, tostring(errorData) )
	KEP_EventEncodeString( event, tostring(path) )
	KEP_EventEncodeNumber( event, assetType )
	KEP_EventQueue( event )
end

-- Duplicate an action item
function ActionItemFunctions.duplicate( newName, oldPath, newType, oldType, scriptGlidOrName )

	if oldType==DevToolsUpload.ACTION_INSTANCE or oldType==DevToolsUpload.ACTION_INSTANCE_PARAMS then
		LogError("AIF.duplicate: cannot duplicate an instance")
		return ""
	end

	local newPath = ""

	if oldType==DevToolsUpload.ACTION_ITEM and (newType==DevToolsUpload.ACTION_ITEM_PARAMS or newType==DevToolsUpload.ACTION_INSTANCE_PARAMS) then

		-- Export JSON
		local errors = ActionItemFunctions.errorCheck(oldType, oldPath)
		if not errors then
			if scriptGlidOrName~=nil then
				ActionItemFunctions.getActionItemTable(oldPath)
				newPath = ActionItemFunctions.saveJson(newName, scriptGlidOrName, newType==DevToolsUpload.ACTION_INSTANCE_PARAMS)
			else
				-- Internal error
				LogError("AIF.duplicate: script GLID not available")
			end
		else
			ActionItemFunctions.openErrorDialog("The smart object script contains one or more errors: " .. errors, oldPath, oldType)
		end

		-- Clear AID
		AID = nil

	elseif oldType==newType or
		   oldType==DevToolsUpload.ACTION_ITEM and newType==DevToolsUpload.ACTION_INSTANCE or
		   oldType==DevToolsUpload.ACTION_ITEM_PARAMS and newType==DevToolsUpload.ACTION_INSTANCE_PARAMS then

		-- Direct copy
		newPath = KEP_DuplicateActionItem(newName, oldPath, newType)
	else
		-- Bad parameter?
		LogError("AIF.duplicate: invalid parameter: newName=" .. tostring(newName) .. ", oldType=" .. tostring(oldType) .. ", newType=" .. tostring(newType))
	end

	return newPath
end

-- Read and parse JSON file. Then merge data with AID
function ActionItemFunctions.mergeJsonData( assetType, jsonPath, paramKeys )

	if AID==nil or AID.params==nil then
		LogError("AIF.mergeJsonData: AID or AID.params not defined")
		return "Internal error"
	end

	if jsonPath==nil then
		LogError("AIF.mergeJsonData: jsonPath is nil")
		return "Internal error"
	end

	local tbl = ActionItemFunctions.parseJson( assetType, jsonPath )
	if not tbl then
		return "Internal error"
	end

	-- Merge item properties
	for key, _ in pairs(ActionItemFunctions.jsonProperties) do
		if tbl[key]==nil then
			return "Missing required property value for [" .. tostring(key) .. "]"
		end

		if type(tbl[key])~=type(AID[key]) then
			LogError("AIF.mergeJsonData: bad value for property [" .. tostring(key) .. "], expected: " .. type(AID[key]) .. ", got " .. type(tbl[key]) .. ". File: " .. tostring(jsonPath))
			return "Internal error"
		end

		AID[key] = tbl[key]
	end

	-- Merge parameter values
	for _, key in ipairs(paramKeys) do
		local paramArray, paramEnabled
		if type(tbl.params)=="table" and tbl.params[key]~=nil then
			paramArray = "params"
			paramEnabled = true
		elseif type(tbl.disabled)=="table" and tbl.disabled[key]~=nil then
			paramArray = "disabled"
			paramEnabled = false
		else
			return "Missing parameter value for [" .. tostring(key) .. "]"
		end

		if type(AID.params[key])=="table" then
			local errors = UpdateParamValue(AID.params[key], tbl[paramArray][key], paramEnabled)
			if errors then
				LogError("AIF.mergeJsonData: " .. tostring(errors) .. " for parameter `" .. tostring(key) .. "', expected: " .. type(AID.params[key].value) .. ", got " .. type(tbl[paramArray][key]) .. ". File: " .. tostring(jsonPath))
				return "Internal error"
			end
		else
			LogError("AIF.mergeJsonData: parameter [" .. tostring(key) .. " not found in AID. File: " .. tostring(jsonPath))
		end
	end

	return nil
end

-- Name of a shop item will determined here instead of the one from asset server.
function ActionItemFunctions.formatShopItemName( glid )
	-- Use same GLID formatting as asset server for backward compatibility.
	if type(glid)=="number" then
		return string.format("%010d", glid)
	end
	return nil
end

-- Opens a parameter editor dialog
function ActionItemFunctions.openParamEditor(paramName, displayName, description, type, value)
	local param = {}
	param.enabled = nil					-- not used
	param.name = displayName
	param.description = description
	param.type = type
	param.value = value

	MenuOpenModal("ParamEditor.xml")

	local event = KEP_EventCreate( EDIT_PARAM_EVENT )
	ActionItemFunctions.encodeParam(event, param, paramName)
	KEP_EventQueue( event )
	-- Reply will come as CHANGE_PARAM_EVENT
end

----------------------------------------------------------------------
-- Begin action item pending task
----------------------------------------------------------------------
local TaskAction = {
	-- Inventory.lua
	place = 1,								-- place    : LUA/JSON -> LocalDev/Server
	duplicate = 2,							-- duplicate: LUA/JSON -> LocalDev/Server
	edit = 3,								-- edit     : LUA/JSON -> LocalDev/Server
	attach = 4,								-- attach   : LUA/JSON -> Server
	-- PresetEditor.lua
	bake = 5,
	upload = 6,
}

local function clearPendingTask(task)
	-- clear all values including additional parameters saved by script
	for k,v in pairs(task) do
		if type(v)~="function" then
			task[k] = nil
		end
	end
end

local function setPendingTask(task, action, item)
	if TaskAction[action]==nil then
		LogError("AIF.setPendingTask: invalid action \"" .. tostring(action) .. "\"")
		return
	end
	clearPendingTask(task)
	task.busy = true
	task.action = TaskAction[action]
	task.AAName = item.AAName
	task.AAType = item.AAType
end

local function taskPending(task, action)
	return task.busy and (action==nil or task.action==TaskAction[action])
end

function ActionItemFunctions.initPendingTask()
	local task = {
		busy = false,						-- bool that prevents undertaking a new action while waiting for the server
		action = TaskAction.none,			-- string that contains the action to perform when a download is completed.
		AAName = nil,						-- name of the action item being processed
		AAType = nil,						-- action item asset type
	}
	task.set = function(action, item) return setPendingTask(task, action, item) end
	task.clear = function() return clearPendingTask(task) end
	task.pending = function(action) return taskPending(task, action) end
	return task
end

----------------------------------------------------------------------
-- Action item download/upload state machines
----------------------------------------------------------------------

-- Return async, error message (if any)
function ActionItemFunctions.ensureItemOnServer(task, item, allItems)
	if item.server_path ~= nil then
		-- already on server
		return false	-- completed with no error
	end

	if item.localPath ~= nil then		-- in LocalDev?
		local errors = ActionItemFunctions.errorCheck(item.AAType, item.localPath)
		if not errors then
			KEP_UploadAssetTo3DApp(item.AAName, item.localPath, item.AAType)
			-- next stop: PublishCompleteHandler
		else
			return false, errors	-- completed with error
		end
	elseif item.script_url ~= nil then	-- in shop?
		if KEP_IsWorld() or ActionItemFunctions.isTrustedItem(item.parent_script_glid, allItems) then
			local baseName = ActionItemFunctions.formatShopItemName(item.GLID)
			KEP_DownloadActionItem(item.GLID, item.AAType, item.script_url, baseName)
			-- next stop: ActionItemDownloadFromWebHandler
		else
			LogError("AIF.ensureItemOnServer: script not found in WOK server (not a trusted item?), GLID: " .. tostring(item.GLID) .. ", name: " .. tostring(item.AAName))
			return false, ""	-- completed with internal error
		end
	else
		-- nowhere
		LogError("AIF.ensureItemOnServer: script not found [" .. tostring(item.AAName) .. "]. Action=" .. tostring(task.action))
		return false, ""	-- completed with internal error
	end

	return true	-- async processing
end

-- Return async, error message (if any)
function ActionItemFunctions.ensureItemInLocalDev(task, item, allItems)
	if item.localPath ~= nil then
		-- in LocalDev
		local errors = ActionItemFunctions.errorCheck(item.AAType, item.localPath)
		if not errors then
			if item.AAType == DevToolsUpload.ACTION_ITEM_PARAMS then
				-- Derivative: obtain parent item
				local jsonTable = ActionItemFunctions.parseJson( item.AAType, item.localPath )
				item.default_glid = jsonTable.defaultGLID				
				local parentName
				if type(jsonTable.script)=="number" then	-- GLID
					item.parent_script_glid = jsonTable.script
					parentName = ActionItemFunctions.formatShopItemName(jsonTable.script)
				else
					parentName = jsonTable.script
				end

				local parentItem = allItems[parentName]
				if not parentItem and item.parent_script_glid ~= nil then
					-- parent item is not in inventory
					-- create an "non-inventory" item for it
					allItems[parentName] = {}
					allItems[parentName].name             = parentName
					allItems[parentName].AAName           = parentName
					allItems[parentName].AAType           = DevToolsUpload.ACTION_ITEM
					allItems[parentName].script_url       = item.parent_script_url			-- Url to download the parent script
					allItems[parentName].hidden           = true							-- DO not include in inventory UI
					parentItem = allItems[parentName]
				end

				task.parentName = parentName

				if parentItem then
					if parentItem.AAType == DevToolsUpload.ACTION_ITEM then
						-- Make sure parent item is on server before we can manipulating a derivative
						return ActionItemFunctions.ensureItemOnServer(task, parentItem, allItems)
					else
						-- something is wrong
						LogError("AIF.ensureItemInLocalDev: Parent item [" .. tostring(parentName) .. "] has an invalid type: " .. tostring(parentItem.AAType))
						return false, ""	-- completed with internal error
					end

				else
					-- parent script is no where
					LogError("AIF.ensureItemInLocalDev: Cannot place smart object. Parent script not found or bad [" .. tostring(parentName) .. "]")
					return false, ""	-- completed with internal error
				end
			else
				-- Standalone or parent item
				ActionItemFunctions.getActionItemTable(item.localPath)
				item.default_glid = AID.defaultGLID
				return false	-- completed with no error
			end

		else
			return false, errors	-- completed with error
		end

	elseif item.server_path ~= nil then
		-- not in LocalDev but on server
		KEP_RequestSourceAssetFromAppServer( item.AAType, item.AAName )
		-- next stop ActionItemDownloadHandler

	elseif item.script_url ~= nil then
		-- not in LocalDev but in shop
		if KEP_IsWorld() or ActionItemFunctions.isTrustedItem(item.parent_script_glid, allItems) then
			local baseName = ActionItemFunctions.formatShopItemName(item.GLID)
			KEP_DownloadActionItem(item.GLID, item.AAType, item.script_url, baseName)
			-- next stop: ActionItemDownloadFromWebHandler
		else
			LogError("AIF.ensureItemInLocalDev: script not found in WOK server (not a trusted item?), GLID: " .. tostring(item.GLID) .. ", name: " .. tostring(item.AAName))
			return false, ""	-- completed with internal error
		end
	else
		-- no where
		LogError("AIF.ensureItemInLocalDev: script not found [" .. tostring(item.AAName) .. "]. Action=" .. tostring(task.action))
		return false, ""	-- completed with internal error
	end

	return true	-- async processing
end

-- Return true if item is trusted
function ActionItemFunctions.isTrustedItem(nameOrGlid, allItems)
	local itemName = nameOrGlid
	if type(nameOrGlid)=="number" then
		itemName = ActionItemFunctions.formatShopItemName(nameOrGlid)
	end
	return itemName~=nil and allItems~=nil and allItems[itemName]~=nil and allItems[itemName].server_path~=nil
end

----------------------------------------------------------------------
-- Lib_ActionItem adapter
----------------------------------------------------------------------
ActionItem = { newAID = function() return { process = function() end } end }
