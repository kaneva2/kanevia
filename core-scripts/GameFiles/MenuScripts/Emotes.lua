--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("MenuLocation.lua")
dofile("CAM.lua")
dofile("Framework_EventHelper.lua")

EMOTE_LOADING = " Loading... "

g_t_items = nil

g_emoteidlist={}
g_namelist={}
g_emotekeyindexlist={}

g_emoteglobalidlist={}
g_emotenamegloballist={}
g_emotekeyglobalindexlist={}

g_pageindex=1
g_maxpages=1
g_previouspage=nil
g_nextpage=nil

g_iZoneHasGameItems= false
--for AP and VIP passes
REQUEST_ACCESS_PASS_INFO_EVENT = "RequestAccessPassInfoEvent"
RESPONSE_ACCESS_PASS_INFO_EVENT = "ResponseAccessPassInfoEvent"
REQUEST_ACCESS_PASS_ZONE_INFO_EVENT = "RequestAccessPassZoneInfoEvent"
RESPONSE_ACCESS_PASS_ZONE_INFO_EVENT = "ResponseAccessPassZoneInfoEvent"

g_iHasAccessPass = 0
g_iHasVIPPass = 0
g_iZoneHasAccessPass = 0
g_iZoneHasVIPPass = 0

ACCESS_PASS_ID = 1
VIP_PASS_ID = 2

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "AddMyEmoteHandler", "MyCustomEvents", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassInfoHandler", RESPONSE_ACCESS_PASS_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassZoneInfoHandler", RESPONSE_ACCESS_PASS_ZONE_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "gameItemsExistHandler","GAME_ITEMS_EXIST",KEP.MED_PRIO )
	KEP_EventRegisterHandler( "returnFrameworkStateHandler", "FrameworkState", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "LoadCurrentPageEmotes",KEP.MED_PRIO )
	KEP_EventRegister( "GAME_ITEMS_EXIST",KEP.MED_PRIO )
	KEP_EventRegister( "REQUEST_GAME_ITEMS_EXIST",KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- DRF - Hot Fix
	gPlayerGender = KEP_GetLoginGender()

	--Allocate memory for an single animation name and id page
	CreateBlankPage(1)

	--Create 10 empty slots to save our custom emotes.
	for i = 1, 10 do
		g_emoteidlist[i]=-1
	end

	g_nextpage = Dialog_GetButton(gDialogHandle,"btnNextPage")
	g_previouspage = Dialog_GetButton(gDialogHandle,"btnPreviousPage")

	if(g_pageindex==1)then
		Control_SetVisible(g_previouspage,false)
	end

	g_iZoneHasGameItems	= (DevMode() > 0)

	if not g_iZoneHasGameItems then
		-- Is the Framework active in this world? Let's find out
		KEP_EventCreateAndQueue("GetFrameworkState")
	end

	-- ED-2630 - A/B test for hiding toolbelt numbers when the emote menu is enabled
	Static_SetText(gHandles["stcInstructions"], " ")

	--get the player's passes
	KEP_EventCreateAndQueue( REQUEST_ACCESS_PASS_INFO_EVENT )

	--get passes for current zone
	KEP_EventCreateAndQueue( REQUEST_ACCESS_PASS_ZONE_INFO_EVENT )

	LoadKeyBoardEmotes()

	getPlayerInventory(WF.PLAYER_EMOTES, 1, 100000, "avatar", "", "emotes")
end

function Dialog_OnMoved(dialogHandle, x, y)
	Helper_Dialog_OnMoved(dialogHandle, x, y)
end

function Dialog_OnDestroy(dialogHandle)
	SaveKeyBoardEmotes()
	Helper_Dialog_OnDestroy( dialogHandle )
end

function responseAccessPassInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iHasAccessPass = KEP_EventDecodeNumber(event)
	g_iHasVIPPass = KEP_EventDecodeNumber(event)
	return KEP.EPR_OK
end

function responseAccessPassZoneInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iZoneHasAccessPass = KEP_EventDecodeNumber(event)
	g_iZoneHasVIPPass = KEP_EventDecodeNumber(event)
	return KEP.EPR_OK
end

-- Called when a player changes Framework mode or when the Framework changes states (enabled/disabled)
function returnFrameworkStateHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local frameworkState = cjson.decode(Event_DecodeString(event))
	if frameworkState.enabled ~= nil and (frameworkState.enabled == true) then
		-- ED-2630 - A/B test for hiding toolbelt numbers when the emote menu is enabled
		Static_SetText(gHandles["stcInstructions"], " ")
	end
end

--Fill the current page with the correct animation names and indexes.
function FillCurrentAnimationPage(cpage)

	for i = 1, 10 do
		--Copy all the names to the buttons.
		bh = Dialog_GetButton(gDialogHandle,"btnEmote"..tostring(i-1))
		if bh~=nil then
			if g_emotenamegloballist[cpage][i]~="" then
				local animationName = g_emotenamegloballist[cpage][i]
				local requiredPass = getRequiredPass(g_emoteglobalidlist[cpage][i])
				setButtonDisplayElement(bh, requiredPass)
				Static_SetText(bh, animationName )
			else
				setButtonDisplayElement(bh, 0)
				Static_SetText(bh," ")
			end
		end

		--Place the correct indexes.
		if g_emoteglobalidlist[cpage][i]~=-1 then
			g_emoteidlist[i] = g_emoteglobalidlist[cpage][i]
		else
			g_emoteidlist[i] = -1
		end
	end
end

function setButtonDisplayElement(buttonHandle, passId)
	displayElement = Button_GetNormalDisplayElement(bh)
	local coordLeft = 299
	local coordTop = 120
	local coordRight = 300
	local coordBottom = 121
	if(passId == ACCESS_PASS_ID) then
		coordLeft = 309
		coordTop = 79
		coordRight = 310
		coordBottom = 80
	elseif(passId == VIP_PASS_ID) then
		coordLeft = 289
		coordTop = 79
		coordRight = 290
		coordBottom = 80
	end
end

--Click on this button to go the next emote page.
function btnNextPage_OnButtonClicked(buttonHandle)
	g_pageindex=g_pageindex+1

	if(g_pageindex>g_maxpages) then
		g_maxpages=g_pageindex
		CreateBlankPage(g_pageindex)
	end

	--Load the current custom emote page.
	FillCurrentAnimationPage(g_pageindex)

	Control_SetVisible(g_nextpage,true)
	Control_SetVisible(g_previouspage,true)

	SaveKeyBoardEmotes()
end

--Click on this button to go the previous emote page
function btnPreviousPage_OnButtonClicked(buttonHandle)

	g_pageindex=g_pageindex-1
	if g_pageindex<=1 then
		g_pageindex=1
		Control_SetVisible(g_nextpage,true)
		Control_SetVisible(g_previouspage,false)
	else
		Control_SetVisible(g_nextpage,true)
		Control_SetVisible(g_previouspage,true)
	end

	--Load the current custom emote page.
	FillCurrentAnimationPage(g_pageindex)

	SaveKeyBoardEmotes()
end

--Create an New Custom Emote Page
function CreateBlankPage(cpage)

	g_emotenamegloballist[cpage]={}
	g_emoteglobalidlist[cpage]={}
	g_emotekeyglobalindexlist[cpage]={}

	--Create 10 empty slots for each page
	for i = 1, 10 do
		g_emotekeyglobalindexlist[cpage][i]=-1
		g_emotenamegloballist[cpage][i]=""
		g_emoteglobalidlist[cpage][i]=-1
	end
end

--Event which get fired everytime we insert an emote into our custom list
function AddMyEmoteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local g_index = KEP_EventDecodeNumber(event)
	local g_animationindex = KEP_EventDecodeNumber(event)
	local g_animname = KEP_EventDecodeString(event)

	--Holds each index of the animation and the name
	g_emoteidlist[g_index+1]=g_animationindex
	g_namelist[g_index+1]=g_animname
	g_emotekeyindexlist[g_index+1]=g_index

	--Save each pages
	g_emotekeyglobalindexlist[g_pageindex][g_index+1]=g_index
	g_emotenamegloballist[g_pageindex][g_index+1]=g_namelist[g_index+1]
	g_emoteglobalidlist[g_pageindex][g_index+1]=g_emoteidlist[g_index+1]

	local requiredPass = getRequiredPass(g_animationindex)
	bh = Dialog_GetButton(gDialogHandle,"btnEmote"..tostring(g_index))
	Static_SetText(bh, g_animname)
	setButtonDisplayElement(bh, requiredPass)

	SaveKeyBoardEmotes()
end

-- Save emotes to '%APPDATA%\Kaneva\<gameId>\<userName>Emotes.cfg'
function SaveKeyBoardEmotes()

	-- Save Emotes
	local filename = GetUserName().."Emotes.cfg"
	local f, errmsg = KEP_FileOpenConfig(filename,"w")
	if (f == 0) then
		LogError("SaveKeyBoardEmotes: FileOpenConfig("..filename..") FAILED")
		return false
	end

	--Save all the pages
	line =tostring(g_maxpages.."\n")
	ok,errMsg = KEP_FileWriteLine( f,line)
	line =tostring(g_pageindex.."\n")
	ok,errMsg = KEP_FileWriteLine( f,line)
	for i = 1, g_maxpages do
		for j = 1,10 do
			--Save each animation index in that order Index:Name and Id
			line =tostring(g_emotekeyglobalindexlist[i][j].."\n")
			ok,errMsg = KEP_FileWriteLine( f,line)
			line =tostring(g_emotenamegloballist[i][j].."\n")
			ok,errMsg = KEP_FileWriteLine( f,line)
			line =tostring(g_emoteglobalidlist[i][j].."\n")
			ok,errMsg = KEP_FileWriteLine( f,line)
		end
	end

	KEP_FileClose(f)

	Log("SaveKeyBoardEmotes: '"..filename.."' pages="..g_maxpages.." emotes="..g_emotes)

	-- Tell HUd To Reload Saved Emotes
	local event = KEP_EventCreate("LoadCurrentPageEmotes")
	KEP_EventEncodeNumber(event,g_pageindex)
	KEP_EventQueue( event )

	return true
end

-- Load emotes from '%APPDATA%\Kaneva\<gameId>\<userName>Emotes.cfg'
function LoadKeyBoardEmotes()

	-- Load Emotes
	local filename = GetUserName().."Emotes.cfg"
	local f, errmsg = KEP_FileOpenConfig(filename,"r")
	if (f == 0) then
		LogWarn("LoadKeyBoardEmotes: FileOpenConfig("..filename..") FAILED - Calling SaveDefaultKeyBoardEmotes()...")
		if (not SaveDefaultKeyBoardEmotes()) then 
			LogError("LoadKeyBoardEmotes: SaveDefaultKeyBoardEmotes() FAILED")
			return false 
		end
		return LoadKeyBoardEmotes()
	end

	--Read the number of pages that we have stored.
	g_emotes = 0
	ok=1
	msg=nil
	errMsg=nil
	ok,msg,errMsg = KEP_FileReadLine( f )
	g_maxpages = tonumber(msg) or 1
	ok,msg,errMsg = KEP_FileReadLine( f )
	g_pageindex=tonumber(msg) or 1
	for i=1,g_maxpages do
		CreateBlankPage(i)
		for j =1,10 do
			ok,msg,errMsg = KEP_FileReadLine( f )
			ok,msg2,errMsg = KEP_FileReadLine( f )
			ok,msg3,errMsg = KEP_FileReadLine( f )
			local animId = tonumber(msg3)
			if (ok) then
				if animId<3000000 then
					g_emotekeyglobalindexlist[i][j]=tonumber(msg)
					g_emotenamegloballist[i][j]=msg2
					g_emoteglobalidlist[i][j]=tonumber(msg3)
				else
					-- UGC emote, list as pending until inventory is refreshed into g_t_items
					g_emotekeyglobalindexlist[i][j]=tonumber(msg)
					g_emotenamegloballist[i][j]=EMOTE_LOADING
					g_emoteglobalidlist[i][j]=tonumber(msg3)
				end
			end
			g_emotes = g_emotes + 1
		end
	end

	KEP_FileClose(f)

	Log("LoadKeyBoardEmotes: '"..filename.."' pages="..g_maxpages.." emotes="..g_emotes)

	--Load the last animation page which we closed on
	FillCurrentAnimationPage(g_pageindex)

	if g_pageindex>1 then
		Control_SetVisible(g_nextpage,true)
		Control_SetVisible(g_previouspage,true)
	end

	return true
end

function getRequiredPass(animation)
	if ( isVIPPassRequired(animation) ) then
		return VIP_PASS_ID
	end
	if ( isAccessPassRequired(animation) ) then
		return ACCESS_PASS_ID
	end
	return 0
end

function btnAddEmote_OnButtonClicked(buttonHandle)
	MenuOpen("FullEmotes2.xml")
end

function EmoteClicked(key)
	local index = key + 1
	if g_emoteidlist[index] ~= -1 then
		ChangeAnimation(g_emoteidlist[index]) -- takes a glid
	end
end
function btnEmote0_OnButtonClicked( buttonHandle ) EmoteClicked(0) end
function btnEmote1_OnButtonClicked( buttonHandle ) EmoteClicked(1) end
function btnEmote2_OnButtonClicked( buttonHandle ) EmoteClicked(2) end
function btnEmote3_OnButtonClicked( buttonHandle ) EmoteClicked(3) end
function btnEmote4_OnButtonClicked( buttonHandle ) EmoteClicked(4) end
function btnEmote5_OnButtonClicked( buttonHandle ) EmoteClicked(5) end
function btnEmote6_OnButtonClicked( buttonHandle ) EmoteClicked(6) end
function btnEmote7_OnButtonClicked( buttonHandle ) EmoteClicked(7) end
function btnEmote8_OnButtonClicked( buttonHandle ) EmoteClicked(8) end
function btnEmote9_OnButtonClicked( buttonHandle ) EmoteClicked(9) end

function btnGetMoreEmotes_OnButtonClicked( buttonHandle )
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_SHOP .. SHOPPING_EMOTES_SUFFIX )
end

function Dialog_OnMouseMove(dialogHandle, x, y)

	--Make sure we have the interface we need (stop errors in editor)
	local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")
	if sh ~= nil then
		if Control_ContainsPoint(Dialog_GetButton(gDialogHandle, "btnClose"), x, y) ~= 0 then
			Static_SetText(sh, "Close This Menu")
		else
			Static_SetText(sh, "Page: "..tostring(g_pageindex))
		end
	end
end

-- temp: get from inventory for now -- eventually we will have a separate wok service
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	if filter == WF.PLAYER_EMOTES then

		local xmlData = KEP_EventDecodeString( event )

		g_t_items 	= {}
		decodeInventoryItemsFromWeb( g_t_items, xmlData, true )

		--refresh pending UGC emotes
		RefreshPendingUGCEmotes()

		--refresh current animation list
		FillCurrentAnimationPage(g_pageindex)

		return KEP.EPR_OK	-- don't consume this, leave it for inventory menu
	end
end

function RefreshPendingUGCEmotes()

	for i=1,g_maxpages do
		for j =1,10 do

			-- look for pending UGC emotes
			if g_emotenamegloballist[i]~=nil and g_emotenamegloballist[i][j]==EMOTE_LOADING and g_emoteglobalidlist[i]~=nil and g_emoteglobalidlist[i][j]~=nil then

				-- validate UGC emote
				local isValid, errMsg, item = isValidUGCAnimation(g_emoteglobalidlist[i][j])
				if isValid then
					-- update name
					if item~=nil then
						g_emotenamegloballist[i][j]=item.name
					else
						LogWarn( "Emote [" .. g_emoteglobalidlist[i][j] .. "] validated but no item returned" )
					end
				else
					-- remove this entry
					Log( "Removed emote [" .. g_emoteglobalidlist[i][j] .. "] from list" )
					g_emotekeyglobalindexlist[i][j]=-1
					g_emotenamegloballist[i][j]=""
					g_emoteglobalidlist[i][j]=-1
				end
			end
		end
	end
end