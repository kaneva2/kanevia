--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")

g_username = ""
g_userID = 0
g_credits = 0
g_availCreditRedemptions = 0
g_rewards = 0
g_fileName = ""

g_worldfameLevel = 0
g_worldfameMaxLevel = 0
g_worldfameLevelPercentage = 0
g_worldLevelRewards = 0
g_worldLevelTitle = ""
g_worldLevelItem = ""

g_gotCounts = false
g_gotWorlds = false
g_gotFriends = false

g_shortcutsEnabled = true
ITEMS_SHORTCUTS = 5
ITEM_SHORTCUTS_WORLDS = 1
ITEM_SHORTCUTS_FRIENDS = 2
ITEM_SHORTCUTS_EVENTS = 3
ITEM_SHORTCUTS_MESSAGES = 4
ITEM_SHORTCUTS_COMMUNITIES = 5

g_worldsEnabled = true
g_worlds = {}
MAX_WORLDS = 10

g_friendsEnabled = true
g_friends = {}
MAX_FRIENDS = 3

BADGES = 5
BADGE_WORLDS = 1
BADGE_FRIENDS = 2
BADGE_EVENTS = 3
BADGE_MESSAGES = 4
BADGE_COMMUNITIES = 5
g_badgeEnables = {true, false, true, true, false}
g_badgeNames = {"Worlds", "Friends", "Events", "Messages", "Communities"}
g_badgeCountsTags = {"RequestCount", "FriendReqs", "TotalNumberRecords", "NewMessages", "RequestCountCommunities"}
g_badgeCounts = {0, 0, 0, 0, 0}

EVENT_NOTIFICATION_HOURS = 1 --Number of hours the notification display checks for

g_history = {}
ZONE_NAVIGATION_EVENT = "ZoneNavigationEvent"

g_layoutForce = true
g_layoutWorldsHdr = true
g_layoutWorldsRows = 2

g_showLightbox = false
g_lightboxAlpha = nil

---------------------------------------------------------------------
-- XML CONSTANTS
---------------------------------------------------------------------

LAYOUT_DELTA_Y_SHORTCUTS = 95 -- y offset w/o shortcuts
LAYOUT_DELTA_Y_SECTION_HDR = 30 -- y offset per section header
LAYOUT_DELTA_Y_SECTION_ROWS = 86 -- y offset per section row

ROW_SHORTCUTS_START = 1
ROW_SHORTCUTS_END = 1
ROW_WORLDS_START = 2
ROW_WORLDS_DEFAULT = 2
ROW_COMMUNITIES_DEFAULT = 3
ROW_WORLDS_END = 3
ROW_FRIENDS_START = 4
ROW_FRIENDS_DEFAULT = 4
ROW_FRIENDS_END = 4

ITEMS_WORLDS = 5
ITEMS_FRIENDS = 3

function setControlImage(ctrlName, imgFile)
	local ctrlPic = gHandles[ctrlName .. "_Pic"]
	local ctrlFrame = gHandles[ctrlName .. "_Frame"]
	scaleImage(ctrlPic, ctrlFrame, imgFile)
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "textureDownloadHandler", "LooseTextureDownloadEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "GlobalWorldReqsResetEventHandler", "GlobalWorldReqsResetEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "zoneNavigationEventHandler", ZONE_NAVIGATION_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "LightboxMenuEventHandler", "LightboxMenuEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "QueryPlacesEvent", KEP.MED_PRIO )
	KEP_EventRegister( "GlobalWorldReqsResetEvent", KEP.MED_PRIO )
	KEP_EventRegister( "CommunityTabEvent", KEP.MED_PRIO )
	KEP_EventRegister( "LightboxMenuEvent", KEP.MED_PRIO )
	KEP_EventRegister( "FriendsTabEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	-- Disable Menu Until Sections Populate
	allDisplay(false)
	
	-- Get User's Profile
	g_username = KEP_GetLoginName()
	setControlText("stcUserName", g_username)
	getUserID(g_username, WF.HUD_USER_ID)

	--Get Events happening in the inputted amount of time
	local webAddress = GameGlobals.WEB_SITE_PREFIX .. "kgp/eventList.aspx?type=100&onlyInvited=Y&nextHours=" .. EVENT_NOTIFICATION_HOURS
	makeWebCall(webAddress, WF.DASH_EVENT_COUNTS, 1, 100)
			
	-- Get available credit redemptions
	getAvailableCreditRedemptions(WF.DASH_AVAIL_CREDIT_REDEMPTIONS)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnMoved(dialogHandle, x, y)
	showLightbox()
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == WF.DASH_MOST_VISITED then
		local ret = KEP_EventDecodeString(event)
		parseMostVisitedWorlds(ret)
	elseif filter == WF.DASHBOARD_GET_HISTORY then
		local ret = KEP_EventDecodeString(event)
		parseBrowsingHistory(ret)
	elseif filter == WF.DASH_RANDOM_FRIENDS then
		local ret = KEP_EventDecodeString(event)
		parseRandomFriends(ret)
	elseif filter == WF.HUD_USER_ID then
		local estr = KEP_EventDecodeString( event )
		setControlEnabled("imgObject_Pic", true)
		setControlEnabled("imgObject_BG", true)
		ParseUserID(estr)
	elseif filter == WF.DASH_BALANCES then
		local estr = KEP_EventDecodeString( event )
		g_credits, g_rewards = ParseBalances(estr)
		setControlText("stcUserName", g_username)
		setControlText("stcCredits_Label", LongNumberToString(g_credits).." Credits")
		setControlText("stcRewards_Label", LongNumberToString(g_rewards).." Rewards")
	elseif filter == WF.DASH_AVAIL_CREDIT_REDEMPTIONS then
		local estr = KEP_EventDecodeString( event )
		g_availCreditRedemptions = ParseAvailableCreditRedemptions(estr)
		if g_availCreditRedemptions > 0 then
			stretchStaticToText(gHandles["stcCreditRedemptions_Label"], LongNumberToString(g_availCreditRedemptions) .. " World Credit Balance")
			
			local linkIconX = Control_GetLocationX(gHandles["stcCreditRedemptions_Label"]) + Control_GetWidth(gHandles["stcCreditRedemptions_Label"]) + 4
			Control_SetLocationX(gHandles["imgCreditRedemptions_LinkIcon"], linkIconX)

			local linkButtonWidth = linkIconX + 10 - Control_GetLocationX(gHandles["imgCreditRedemptions_Icon"])
			Control_SetSize(gHandles["btnCreditRedemptions"], linkButtonWidth, Control_GetHeight(gHandles["btnCreditRedemptions"]))

			setControlVisible("imgCreditRedemptions_Icon", true)
			setControlVisible("stcCreditRedemptions_Label", true)
			setControlVisible("imgCreditRedemptions_LinkIcon", true)
			setControlEnabled("btnCreditRedemptions", true)
		else
			setControlVisible("imgCreditRedemptions_Icon", false)
			setControlVisible("stcCreditRedemptions_Label", false)
			setControlVisible("imgCreditRedemptions_LinkIcon", false)
			setControlEnabled("btnCreditRedemptions", false)
		end
	elseif filter == WF.DASH_WORLD_COUNTS then
		local estr = KEP_EventDecodeString( event )
		parseCounts(estr)
	elseif filter == WF.DASH_FRIEND_COUNTS then
		local estr = KEP_EventDecodeString( event )
		parseCounts(estr)
	elseif filter == WF.DASH_EVENT_COUNTS then
		local estr = KEP_EventDecodeString( event )
		parseCounts(estr)
	elseif filter == WF.DASH_MESSAGE_COUNTS then
		local estr = KEP_EventDecodeString( event )
		parseCounts(estr)
	elseif filter == WF.FRIEND_THUMBNAIL_URL then
		local returnData = KEP_EventDecodeString( event )
		s, e, front, url = string.find(returnData, "(http://.-/)(.*)")
		s, e, front, file_name = string.find(returnData, "(.*/)(.*)")
		s, e, user_id = string.find(returnData, "http://.-/.*/(.*).*/")

		-- Filter Out URL Errors
		if (url == nil or filename == nil or user_id == nil or user_id == "images") then
--			LogError("BrowserPageReadyHandler: FRIEND_THUMBNAIL_URL ERROR")
			return
		end
			
		-- Get Game Id
		local gameId = KEP_GetGameId()

		-- Build Local Texture Filename
		new_filename = user_id .. "_" .. file_name .. FRIEND_THUMB_EXT
		if ( tonumber(user_id) == g_userID ) then
			g_fileName = "../../CustomTexture/" .. gameId .. "/" .. new_filename
		else
			for i = 1, #g_friends do
				local friend = g_friends[i]
				if friend ~= nil then
					if friend.id == user_id then
						friend.filename = "../../CustomTexture/" .. gameId .. "/" .. new_filename
						break
					end
				end
			end
		end
			
		-- Get Local Texture
		KEP_DownloadTextureAsyncToGameId( new_filename, url )
	end
end

function LightboxMenuEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == LBOXM.DASHBOARD then
		g_showLightbox = true

		if KEP_EventMoreToDecode(event) == 1 then
			g_lightboxAlpha = KEP_EventDecodeNumber(event)
		end

		showLightbox()
	end
end 

function showLightbox()
	if g_showLightbox then
		local blackOutN = Image_GetControl(gHandles["imgBlackoutN"])
		local blackOutS = Image_GetControl(gHandles["imgBlackoutS"])
		local blackOutE = Image_GetControl(gHandles["imgBlackoutE"])
		local blackOutW = Image_GetControl(gHandles["imgBlackoutW"])

		ShowMenuLightbox(gDialogHandle, blackOutN, blackOutS, blackOutW, blackOutE, g_lightboxAlpha)

		Dialog_SetMovable(gDialogHandle, false)
		Control_SetVisible(gHandles["btnClose"], false)
		Dialog_SetCloseOnESC(gDialogHandle, 0)
	end
end

function textureDownloadHandler()		
	local ihFore = Dialog_GetImage(gDialogHandle, "imgObject_Pic")
	local ihBack = Dialog_GetImage(gDialogHandle, "imgObject_BG")			
	if g_fileName then
		setControlImage("imgObject", g_filename)
    end
	allDisplay(true)
end

function zoneNavigationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ZONE_NAV.HISTORY_INFO then
		g_history = {}
		local usedNames = {}
		local recentHistory = readHistoryIntoTable(event)
		local nameString = ""

		for i = #recentHistory,1,-1 do
			local v = recentHistory[i]
			local historyItem = {}
			historyItem.url = v.url
			historyItem.type = parseZoneTypeFromURL(historyItem.url)
			
			if isURL3DApp(historyItem.url) then
				historyItem.name = parseGameNameFromURL(historyItem.url)
			else
				historyItem.name = parseNameFromURL(historyItem.url)
			end

			-- Select distinct items, order most recent, exclude user's home
			if #g_history >= MAX_WORLDS then
				break
			elseif g_username ~= historyItem.name and not usedNames[historyItem.name] then
				usedNames[historyItem.name] = historyItem.name
				local enc_historyItemName = KEP_UrlEncode(historyItem.name)
				nameString = nameString .. enc_historyItemName .. ","
				table.insert(g_history, historyItem)
			end
		end

		WebCall(GameGlobals.WEB_SITE_PREFIX .. GET_COMMUNITY_SUFFIX .. nameString, WF.DASHBOARD_GET_HISTORY, MAX_WORLDS)
	end
end

function requestHistory()
	local ev = KEP_EventCreate( ZONE_NAVIGATION_EVENT)
	KEP_EventSetFilter(ev, ZONE_NAV.HISTORY_REQUEST)
	KEP_EventQueue( ev)
end

function sendQueryPlacesEvent(placesType, cat)
	
	-- Request Query Places Event
	ev = KEP_EventCreate( "QueryPlacesEvent")
	KEP_EventEncodeNumber(ev, placesType)
	KEP_EventEncodeNumber(ev, cat)
	KEP_EventQueue( ev)
end

function sendCommunityTabEvent() 
	ev = KEP_EventCreate("CommunityTabEvent")
	KEP_EventQueue(ev)
end 

function requestFameUpdate(fameType) 

	-- Request Fame Event
	local ev = KEP_EventCreate( "GetUserFameEvent")
	KEP_EventSetFilter(ev, fameType)
	KEP_EventQueue( ev)
end

function fameUpdateEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	
	-- Decode Fame Event
	local numFameTypes = KEP_EventDecodeNumber(event)
	g_worldfameLevel = KEP_EventDecodeNumber(event)
	g_worldfameMaxLevel = KEP_EventDecodeNumber(event)
	g_worldfameLevelPercentage = KEP_EventDecodeNumber(event)
	g_worldLevelRewards = KEP_EventDecodeNumber(event)
	g_worldLevelTitle = KEP_EventDecodeString(event)
	g_worldLevelItem = KEP_EventDecodeString(event)
end

function GlobalWorldReqsResetEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	-- Disable Worlds Badge
	-- Expecting this to happen immediately by the server doesn't give the right answer
	-- so we are assuming it happens here instead of asking the server for it.
	g_badgeCounts[BADGE_WORLDS] = 0
	setControlEnabled("imgWorldsBadgeBG", false)
	setControlEnabled("stcWorldsBadgeCount", false)
end

function WebCall(url, filter, maxItems)
	makeWebCall(url, filter, 0, maxItems)
end

function WebCallCached(url, filter, maxItems)
	makeWebCallCached(url, filter, 0, maxItems)
end

function getAllUserData()
	if (g_userID == 0) then return end

	getCounts()
	getUserBalances()
	getMostVisitedWorlds()
	getRandomFriends()
end

function getUserBalances()
	WebCallCached(WEBCALL_USER_BALANCE..g_userID, WF.DASH_BALANCES, 0)
end

function getCounts()
	WebCallCached(WEBCALL_GAME_NOTIFICATION_COUNT, WF.DASH_WORLD_COUNTS, 0)
	WebCallCached(WEBCALL_USER_COUNTS..g_userID, WF.DASH_MESSAGE_COUNTS, 0)
end

function getMostVisitedWorlds()
	WebCallCached(WEBCALL_MOST_VISITED_100, WF.DASH_MOST_VISITED, MAX_WORLDS)
end

function getRandomFriends()
	WebCallCached(WEBCALL_RANDOM_ONLINE_FRIENDS, WF.DASH_RANDOM_FRIENDS, MAX_FRIENDS)
end

function ParseUserID(ret)
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(ret, "<ReturnCode>(%d+)</ReturnCode>")
	
	if result == "0" then
		-- Parse needed info from XML
		s, e, result = string.find(ret, "<user_id>(%d+)</user_id>", e)
		g_userID = tonumber(result)
		s, e, result = string.find(ret, "<player_id>(%d+)</player_id>", e)
		g_playerID = tonumber(result)
		
		s, e, dbPath = string.find(ret, "<thumbnail_square_path>(.-)</thumbnail_square_path>", e)
		if not dbPath then
			dbPath = DEFAULT_AVATAR_PIC_PATH
		end		
	
		-- Get User Profile Picture
		if g_userID > 0 and dbPath then
			local filename = KEP_DownloadTextureAsync( GameGlobals.FRIEND_PICTURE, g_userID, dbPath, GameGlobals.FULLSIZE )
			g_filename = string.gsub(filename,".-\\", "" )
			g_filename = "../../CustomTexture/".. g_filename
			WebCallCached(WEBCALL_USER_THUMBNAIL..g_userID, WF.FRIEND_THUMBNAIL_URL, 0)
		end
		
		-- Get All User Data
		getAllUserData()
	end
end

function parseMostVisitedWorlds(ret)
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		local s, e, numRecs = string.find(ret, "<TotalNumberRecords>(.-)</TotalNumberRecords>")
		numRecs = tonumber(numRecs)
		if numRecs > 0 then
			s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
			if result == "0" then
				local s, e, numRecs = string.find(ret, "<TotalNumberRecords>(.-)</TotalNumberRecords>")
				numRecs = tonumber(numRecs)
				if numRecs > 0 then
					g_worlds = {}
					local start = 0
					local last = 0
					for i=1,numRecs do
				
						-- Parse World Attributes
						s, e, visit = string.find(ret, "<Vists>(.-)</Vists>", e)
						start, last, location = string.find(visit, "<location>(.-)</location>")
						start, last, dbPath = string.find(visit, "<image_path>(.-)</image_path>")
						start, last, kurl = string.find(visit, "<STPURL>(.-)</STPURL>")
						start, last, gameId = string.find(visit, "<game_id>(.-)</game_id>")
						start, last, commId = string.find(visit, "<community_id>(.-)</community_id>")
						
						-- Get World Picture
						local filename = ""
						if not dbPath then
							dbPath = DEFAULT_KANEVA_PLACE_ICON
						end

						filename = tostring(split(GetFilenameFromPath(dbPath), ".")[1])
						filename = commId .. "_" .. filename .. CUSTOM_TEXTURE_THUMB
					
						KEP_DownloadTextureAsyncStr( filename, dbPath )
						filename = "../../CustomTexture/" .. filename
						
						-- Insert World Into Table
						local visitPlace = {}
						visitPlace.location = location
						visitPlace.url = kurl
						visitPlace.thumbnail = dbPath
						visitPlace.id = commId
						visitPlace.filename = filename
						visitPlace.gameId = tonumber(gameId) or 0
						if location ~= g_username then -- dont include user's home
							table.insert(g_worlds, visitPlace)
						end
					end
				end
			end
		end
 	end

	-- Refresh Display or Get Local History
	if #g_worlds > 0 then
		g_gotWorlds = true
		allDisplay(true)
	else
		requestHistory()
	end
end

function parseBrowsingHistory(ret)
	local result = string.match(ret, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		
		result = string.match(ret, "<ReturnCode>(.-)</ReturnCode>")
		if result == "0" then
			
			local numRecs = string.match(ret, "<NumberRecords>(.-)</NumberRecords>")
			numRecs = tonumber(numRecs)
			if numRecs > 0 then
				
				g_worlds = {}
				local tempWorlds = {}

				for visit in string.gmatch(ret, "<Communities>(.-)</Communities>") do
					local name = string.match(visit, "<name>(.-)</name>")
					local commId = string.match(visit, "<community_id>(.-)</community_id>")
					local dbPath = string.match(visit, "<thumbnail_small_path>(.-)</thumbnail_small_path>")

					local filename = ""
					if not dbPath then
						dbPath = DEFAULT_KANEVA_PLACE_ICON
					end

					filename = tostring(split(GetFilenameFromPath(dbPath), ".")[1])
					filename = commId .. "_" .. filename .. CUSTOM_TEXTURE_THUMB
					
					KEP_DownloadTextureAsyncStr( filename, dbPath )
					filename = "../../CustomTexture/" .. filename

					-- Insert World Into Temp Table
					local visitPlace = {}
					visitPlace.location = name
					visitPlace.thumbnail = dbPath
					visitPlace.id = commId
					visitPlace.filename = filename
					visitPlace.gameId = 0
						
					if name then
						tempWorlds[name] = visitPlace
					end
				end

				-- Insert into g_worlds, in correct historical order
				for i, v in ipairs(g_history) do
					if tempWorlds[v.name] then
						local tempPlace = tempWorlds[v.name]
						tempPlace.url = v.url
						table.insert(g_worlds, tempPlace)
					end
				end
			end
		end
	end

	-- Refresh Display
	g_gotWorlds = true
	allDisplay(true)
end

function parseRandomFriends(ret)
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	
	s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		local s, e, numRecs = string.find(ret, "<NumberRecords>(.-)</NumberRecords>")
		numRecs = tonumber(numRecs)
		if numRecs > 0 then
			g_friends = {}
			local start = 0
			local last = 0
			for i=1,numRecs do
			
				-- Parse Friend Attributes
				s, e, friend = string.find(ret, "<Friends>(.-)</Friends>", e)
				start, last, location = string.find(friend, "<location>(.-)</location>")
				start, last, username = string.find(friend, "<username>(.-)</username>")
				start, last, realname = string.find(friend, "<display_name>(.-)</display_name>")
				start, last, user_id = string.find(friend, "<user_id>(.-)</user_id>")
				start, last, kurl = string.find(friend, "<STPURL>(.-)</STPURL>")
				start, last, dbPath = string.find(friend, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>")
				if not dbPath then
					dbPath = DEFAULT_AVATAR_PIC_PATH
				end	
				
				-- Get Friend Profile Picture
				WebCallCached(WEBCALL_USER_THUMBNAIL..user_id, WF.FRIEND_THUMBNAIL_URL, 0)
				filename = "../../CustomTexture/"
				
				-- Insert Friend Into Table
				start, last, online = string.find(friend, "<online>(.-)</online>", last)
				local onlinefriend = {}
				onlinefriend.location = location
				onlinefriend.username = username
				onlinefriend.realname = realname
				onlinefriend.url = kurl
				onlinefriend.thumbnail = dbPath
				onlinefriend.online = online
				onlinefriend.filename = filename
				onlinefriend.id = user_id
				table.insert(g_friends, onlinefriend)
			end
		end
 	end

	-- Refresh Display
	g_gotFriends = true
	allDisplay(true)
end

function parseCounts(ret)
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(ret, "<ReturnCode>(%d+)</ReturnCode>")
	if (result == "0") then
		for i, tag in pairs(g_badgeCountsTags) do
			s, e = 0
			s, e, result = string.find(ret, "<"..tag..">(%d+)</"..tag..">", e)
			if (result) then
				g_badgeCounts[i] = tonumber(result) or 0
			end
			badgeEnable(true, i)
		end
	end
	
	-- Refresh Display
	g_gotCounts = true
	allDisplay(true)
end

---------------------------------------------------------------------------------
-- ALL SECTIONS FUNCTIONS
---------------------------------------------------------------------------------

--[[ DRF - Adjusts layout of and displays all sections. ]]
function allDisplay(enable)
	
	-- Layout All Sections
	allLayout()
	
	-- Display All Sections, only if we have the necessary data
	if g_gotCounts and g_gotWorlds and g_gotFriends then
		shortcutsDisplay(enable)
		worldsDisplay(enable)
		friendsDisplay(enable)
	end 

	-- Adjust lightbox if showing
	showLightbox()
end

--[[ DRF - Dynamically adjusts the layout of all menu sections:
-- Shortcuts - Disabled if user has no worlds
-- Worlds - 'Explore Worlds' & 'Explore Communities' buttons if user has no worlds, else 'Worlds' links
-- Friends - 'Invite Friends' button if user has no friends, else 'Friends' links
--]]
function allLayout()

	-- Determine Layout Parameters
	local numWorlds = math.min(#g_worlds, MAX_WORLDS)
	local shortcutsEnabled = (numWorlds ~= 0)
	local shortcutsChanged = (g_shortcutsEnabled ~= shortcutsEnabled)
	local worldsRows = math.max(1, math.floor((numWorlds - 1) / ITEMS_WORLDS) + 1)
	if (numWorlds == 0) then worldsRows = 1 end -- Explore Worlds & Communities Buttons
	local worldsRowsChanged = (g_layoutWorldsRows ~= worldsRows)
	local worldsHdr = (numWorlds ~= 0)
	local worldsHdrChanged = (g_layoutWorldsHdr ~= worldsHdr)
	local worldsChanged = (worldsHdrChanged or worldsRowsChanged)  
	local changed = (shortcutsChanged or worldsChanged) 

	--[[
	Log("allLayout: ...")
	Log("... shortcutsEnabled="..tostring(shortcutsEnabled).." shortcutsChanged="..tostring(shortcutsChanged))
	Log("... numWorlds="..numWorlds.." worldsRows="..worldsRows.." worldsHdr="..tostring(worldsHdr).." worldsChanged="..tostring(worldsChanged))
	Log("... changed="..tostring(changed).." layoutForce="..tostring(g_layoutForce))
	]]

	-- Layout Unchanged ?
	if ((changed == false) and (g_layoutForce == false)) then
		return
	end
	g_layoutForce = false
	
	-- Relocate All Sections Accumulating Delta
	local deltaY = 0

	-- 'Worlds' Follows 'Shortcuts'
	if (shortcutsChanged) then
		if (shortcutsEnabled) then
			deltaY = deltaY + LAYOUT_DELTA_Y_SHORTCUTS
		else
			deltaY = deltaY - LAYOUT_DELTA_Y_SHORTCUTS
		end
		g_shortcutsEnabled = shortcutsEnabled
	end
	worldsRelocate(deltaY)
		
	-- 'Friends' Follows 'Worlds'
	if (worldsHdrChanged) then
		if (worldsHdr) then
			deltaY = deltaY + LAYOUT_DELTA_Y_SECTION_HDR
		else
			deltaY = deltaY - LAYOUT_DELTA_Y_SECTION_HDR
		end
		g_layoutWorldsHdr = worldsHdr
	end
	if (worldsRowsChanged) then
		deltaY = deltaY + LAYOUT_DELTA_Y_SECTION_ROWS * (worldsRows - g_layoutWorldsRows)
		g_layoutWorldsRows = worldsRows
	end
	friendsRelocate(deltaY)
	
	-- 'Slices' Follows 'Friends'
	slicesRelocate(deltaY)
end

---------------------------------------------------------------------------------
-- SLICES SECTION FUNCTIONS
---------------------------------------------------------------------------------

--[[ DRF - Relocates slices to resize entire menu. ]]
function slicesRelocate(deltaY)
	setControlResize("mMiddleLeft_Slice", 0, deltaY)
	setControlResize("mMiddleCenter_Slice", 0, deltaY)
	setControlResize("mMiddleRight_Slice", 0, deltaY)
	setControlRelocate("mLowerLeft_Slice", 0, deltaY)
	setControlRelocate("mLowerCenter_Slice", 0, deltaY)
	setControlRelocate("mLowerRight_Slice", 0, deltaY)

	-- DRF - Added - Opaque Dialogs Require Correct Size
	Dialog_SetHeight(gDialogHandle, Dialog_GetHeight(gDialogHandle) + deltaY) 
end

---------------------------------------------------------------------------------
-- SHORTCUTS SECTION FUNCTIONS
---------------------------------------------------------------------------------

--[[ DRF - Displays Entire 'Shortcuts' Section ]]
function shortcutsDisplay(enable)
	for row=ROW_SHORTCUTS_START,ROW_SHORTCUTS_END do
		for item=1,ITEMS_SHORTCUTS do
			shortcutsEnableBtn(enable, row, item)
			badgeEnable(enable, item)
		end
	end
end

--[[ DRF - Enables/disables 'Shortcuts' button (row, item). ]]
function shortcutsEnableBtn(enable, row, item)
	enable = enable and g_shortcutsEnabled
	setControlEnabled("imgRow"..row.."Item"..item.."_BG", enable)
	setControlEnabled("imgRow"..row.."Item"..item.."_Icon", enable)
	setControlEnabled("btnRow"..row.."Item"..item.."_Button", enable)
	setControlEnabled("stcRow"..row.."Item"..item.."_Name", enable)
end

--[[ DRF - Enables/disables 'Shortcuts' badge (row, item). ]]
function badgeEnable(enable, badge)
	enable = enable and g_shortcutsEnabled and g_badgeEnables[badge] and (g_badgeCounts[badge] ~= 0)
	setControlEnabled("img"..g_badgeNames[badge].."BadgeBG", enable)
	setControlEnabled("stc"..g_badgeNames[badge].."BadgeCount", enable)
	if (badge == BADGE_WORLDS) then
		setControlText("stc"..g_badgeNames[badge].."BadgeCount", "NEW")
	else
		if (g_badgeCounts[badge] < 100) then
			setControlText("stc"..g_badgeNames[badge].."BadgeCount", g_badgeCounts[badge])
		else
			setControlText("stc"..g_badgeNames[badge].."BadgeCount", "99")
		end
	end
end

---------------------------------------------------------------------------------
-----WORLDS SECTION FUNCTIONS
---------------------------------------------------------------------------------

--[[ DRF - Displays entire 'Worlds' section. ]]
function worldsDisplay(enable)
	local numWorlds = math.min(#g_worlds, MAX_WORLDS)
	worldsEnableBtn(enable and (numWorlds == 0))
	worldsEnableLnks(enable and (numWorlds ~= 0))
end

--[[ DRF - Enables/disables 'Explore Worlds' button. ]]
function worldsEnableBtn(enable)
	enable = enable and g_worldsEnabled
	
	-- Enable 'Explore Worlds' Button
	local row = ROW_WORLDS_DEFAULT
	setControlEnabled("btnRow"..row.."Default_Button", enable)
	setControlEnabled("stcRow"..row.."Default_SubTitle", enable)
	setControlEnabled("stcRow"..row.."Default_Title", enable)
	setControlEnabled("stcRow"..row.."Default_TitleShadow", enable)
	setControlEnabled("imgRow"..row.."Default_Icon", enable)
	setControlEnabled("imgRow"..row.."Default_LeftBG", enable)
	setControlEnabled("imgRow"..row.."Default_MiddleBG", enable)
	setControlEnabled("imgRow"..row.."Default_RightBG", enable)
	setControlText("stcRow"..row.."Default_Title", "Explore Worlds")
	setControlText("stcRow"..row.."Default_TitleShadow", "Explore Worlds")
	setControlText("stcRow"..row.."Default_SubTitle", "Make Friends, Play Games, Have Fun!")

	-- Enable 'Explore Communities' Button
	--[[local row = ROW_COMMUNITIES_DEFAULT
	setControlEnabled("btnRow"..row.."Default_Button", enable)
	setControlEnabled("stcRow"..row.."Default_SubTitle", enable)
	setControlEnabled("stcRow"..row.."Default_Title", enable)
	setControlEnabled("stcRow"..row.."Default_TitleShadow", enable)
	setControlEnabled("imgRow"..row.."Default_Icon", enable)
	setControlEnabled("imgRow"..row.."Default_LeftBG", enable)
	setControlEnabled("imgRow"..row.."Default_MiddleBG", enable)
	setControlEnabled("imgRow"..row.."Default_RightBG", enable)
	setControlText("stcRow"..row.."Default_Title", "Explore Communities")
	setControlText("stcRow"..row.."Default_TitleShadow", "Explore Communities")
	setControlText("stcRow"..row.."Default_SubTitle", "Meet People, Make Friends, Share Interests!")--]]
end

--[[ DRF - Enables/disables 'Worlds' link (row, item). ]]
function worldsEnableLnk(enable, row, item)
	enable = enable and g_worldsEnabled
	setControlEnabled("btnRow"..row.."Item"..item.."_Button", enable)
	setControlEnabled("imgRow"..row.."Item"..item.."_Pic", enable)
	setControlEnabled("imgRow"..row.."Item"..item.."_Frame", enable)
	setControlEnabled("stcRow"..row.."Item"..item.."_Name", enable)
end

--[[ DRF - Enables/disables 'Worlds' links. ]]
function worldsEnableLnks(enable)
	enable = enable and g_worldsEnabled
	setControlEnabled("stcRow2and3Heading", enable)
	for row=ROW_WORLDS_START,ROW_WORLDS_END do
		for item=1,ITEMS_WORLDS do
			if (enable) then
				local world = g_worlds[((row - ROW_WORLDS_START) * ITEMS_WORLDS) + item]
				if world ~= nil then
					setControlText("stcRow"..row.."Item"..item.."_Name", world.location)
					setControlImage("imgRow"..row.."Item"..item, world.filename)
					worldsEnableLnk(true, row, item)
				else
					worldsEnableLnk(false, row, item)
				end
			else
				worldsEnableLnk(false, row, item)
			end
		end
	end
end

--[[ DRF - Relocates entire 'Worlds' section. ]]
function worldsRelocate(deltaY)

	-- Relocate 'Worlds' Section Divider
	setControlRelocate("imgHorizRuler_1", 0, deltaY)

	-- Relocate 'Worlds' Links
	setControlRelocate("stcRow2and3Heading", 0, deltaY)
	for row=ROW_WORLDS_START,ROW_WORLDS_END do
		for item=1,ITEMS_WORLDS do
			setControlRelocate("imgRow"..row.."Item"..item.."_Frame", 0, deltaY)
			setControlRelocate("imgRow"..row.."Item"..item.."_Pic", 0, deltaY)
			setControlRelocate("btnRow"..row.."Item"..item.."_Button", 0, deltaY)
			setControlRelocate("stcRow"..row.."Item"..item.."_Name", 0, deltaY)
		end
	end

	-- Relocate 'Explore Worlds' Button
	local row = ROW_WORLDS_DEFAULT
	setControlRelocate("imgRow"..row.."Default_LeftBG", 0, deltaY)
	setControlRelocate("imgRow"..row.."Default_RightBG", 0, deltaY)
	setControlRelocate("imgRow"..row.."Default_MiddleBG", 0, deltaY)
	setControlRelocate("imgRow"..row.."Default_Icon", 0, deltaY)
	setControlRelocate("stcRow"..row.."Default_TitleShadow", 0, deltaY)
	setControlRelocate("stcRow"..row.."Default_Title", 0, deltaY)
	setControlRelocate("stcRow"..row.."Default_SubTitle", 0, deltaY)
	setControlRelocate("btnRow"..row.."Default_Button", 0, deltaY)

	-- Relocate 'Explore Communities' Button
	local row = ROW_COMMUNITIES_DEFAULT
	setControlRelocate("imgRow"..row.."Default_LeftBG", 0, deltaY)
	setControlRelocate("imgRow"..row.."Default_RightBG", 0, deltaY)
	setControlRelocate("imgRow"..row.."Default_MiddleBG", 0, deltaY)
	setControlRelocate("imgRow"..row.."Default_Icon", 0, deltaY)
	setControlRelocate("stcRow"..row.."Default_TitleShadow", 0, deltaY)
	setControlRelocate("stcRow"..row.."Default_Title", 0, deltaY)
	setControlRelocate("stcRow"..row.."Default_SubTitle", 0, deltaY)
	setControlRelocate("btnRow"..row.."Default_Button", 0, deltaY)
end

---------------------------------------------------------------------------------
-----FRIENDS SECTION FUNCTIONS
---------------------------------------------------------------------------------

--[[ DRF - Displays entire 'Friends' section. ]]
function friendsDisplay(enable)
	local numFriends = math.min(#g_friends, MAX_FRIENDS)
	friendsEnableBtn(enable and (numFriends == 0))
	friendsEnableLnks(enable and (numFriends ~= 0))
end

--[[ DRF - Enables/disables 'Invite Friends' button. ]]
function friendsEnableBtn(enable)
	enable = enable and g_friendsEnabled
	local row = ROW_FRIENDS_DEFAULT
	setControlEnabled("btnRow"..row.."Default_Button", enable)
	setControlEnabled("stcRow"..row.."Default_SubTitle", enable)
	setControlEnabled("stcRow"..row.."Default_Title", enable)
	setControlEnabled("stcRow"..row.."Default_TitleShadow", enable)
	setControlEnabled("imgRow"..row.."Default_Icon", enable)
	setControlEnabled("imgRow"..row.."Default_LeftBG", enable)
	setControlEnabled("imgRow"..row.."Default_MiddleBG", enable)
	setControlEnabled("imgRow"..row.."Default_RightBG", enable)
	setControlText("stcRow"..row.."Default_Title", "Invite Friends")
	setControlText("stcRow"..row.."Default_TitleShadow", "Invite Friends")
	setControlText("stcRow"..row.."Default_SubTitle", "The World is More Fun with Friends!")
end

--[[ DRF - Enables/disables 'Friends' link (row, item). ]]
function friendsEnableLnk(enable, row, item)
	enable = enable and g_friendsEnabled
	setControlEnabled("stcRow"..row.."Item"..item.."_LocHeading", enable)
	setControlEnabled("btnRow"..row.."Item"..item.."_LocButton", enable)
	setControlEnabled("btnRow"..row.."Item"..item.."_Button", enable)
	setControlEnabled("imgRow"..row.."Item"..item.."_Pic", enable)
	setControlEnabled("imgRow"..row.."Item"..item.."_Frame", enable)
	setControlEnabled("stcRow"..row.."Item"..item.."_Status", enable)
	setControlEnabled("imgRow"..row.."Item"..item.."_StatusIcon", enable)
	setControlEnabled("stcRow"..row.."Item"..item.."_RealName", enable)
	setControlEnabled("stcRow"..row.."Item"..item.."_Name", enable)
end

--[[ DRF - Enables/disables 'Friends' links. ]]
function friendsEnableLnks(enable)
	enable = enable and g_worldsEnabled
	setControlEnabled("stcRow4Heading", enable)
	for row=ROW_FRIENDS_START,ROW_FRIENDS_END do
		for item=1,ITEMS_FRIENDS do
			if (enable) then
				local friend = g_friends[((row - ROW_FRIENDS_START) * ITEMS_FRIENDS) + item]
				if friend ~= nil then
					Static_SetText(Button_GetStatic(gHandles["btnRow"..row.."Item"..item.."_LocButton"]), friend.location)
					setControlText("stcRow"..row.."Item"..item.."_Name", friend.username) 
					setControlText("stcRow"..row.."Item"..item.."_RealName", friend.realname) 
					setControlText("stcRow"..row.."Item"..item.."_Status", "Online")
					setControlImage("imgRow"..row.."Item"..item, friend.filename)
					friendsEnableLnk(true, row, item)
				else
					friendsEnableLnk(false, row, item)
				end
			else
				friendsEnableLnk(false, row, item)
			end
		end
	end
end

--[[ DRF - Relocates entire 'Friends' section. ]]
function friendsRelocate(deltaY)
	
	-- Relocate 'Friends' Section Divider
	setControlRelocate("imgHorizRuler_2", 0, deltaY)

	-- Relocate 'Friends' Links
	setControlRelocate("stcRow4Heading", 0, deltaY)
	for row=ROW_FRIENDS_START,ROW_FRIENDS_END do
		for item=1,ITEMS_FRIENDS do
			setControlRelocate("imgRow"..row.."Item"..item.."_Frame", 0, deltaY)
			setControlRelocate("imgRow"..row.."Item"..item.."_Pic", 0, deltaY)
			setControlRelocate("btnRow"..row.."Item"..item.."_Button", 0, deltaY)
			setControlRelocate("stcRow"..row.."Item"..item.."_Name", 0, deltaY)
			setControlRelocate("stcRow"..row.."Item"..item.."_RealName", 0, deltaY)
			setControlRelocate("imgRow"..row.."Item"..item.."_StatusIcon", 0, deltaY)
			setControlRelocate("stcRow"..row.."Item"..item.."_Status", 0, deltaY)
			setControlRelocate("stcRow"..row.."Item"..item.."_LocHeading", 0, deltaY)
			setControlRelocate("btnRow"..row.."Item"..item.."_LocButton", 0, deltaY)
		end
	end

	-- Relocate 'Invite Friends' Button
	local row = ROW_FRIENDS_DEFAULT
	setControlRelocate("imgRow"..row.."Default_LeftBG", 0, deltaY)
	setControlRelocate("imgRow"..row.."Default_RightBG", 0, deltaY)
	setControlRelocate("imgRow"..row.."Default_MiddleBG", 0, deltaY)
	setControlRelocate("imgRow"..row.."Default_Icon", 0, deltaY)
	setControlRelocate("stcRow"..row.."Default_TitleShadow", 0, deltaY)
	setControlRelocate("stcRow"..row.."Default_Title", 0, deltaY)
	setControlRelocate("stcRow"..row.."Default_SubTitle", 0, deltaY)
	setControlRelocate("btnRow"..row.."Default_Button", 0, deltaY)
end

---------------------------------------------------------------------------------
-----SHORTCUTS CONTROL HANDLERS (Row 1)
---------------------------------------------------------------------------------

--[[ DRF - 'Shortcuts' -> 'Worlds' button handler. ]]
function btnRow1Item1_Button_OnButtonClicked( buttonHandle )
	Log("worldsClicked")
	if Control_GetVisible(buttonHandle) then
		MenuOpen("BrowsePlaces.xml")
	end
	sendQueryPlacesEvent( PlaceCategoryIds.APPS, -1 )
end

--[[ DRF - 'Shortcuts' -> 'HOME' button handler. ]]
function btnRow1Item5_Button_OnButtonClicked( buttonHandle )
	Log("mostVisitedClicked")
	if Control_GetVisible(buttonHandle) then
		MenuOpen("BrowsePlaces.xml")
	end

	ev = KEP_EventCreate("DisplayMostVisitedWorlds")
	KEP_EventQueue(ev)

	sendQueryPlacesEvent( PlaceCategoryIds.APPS, 0 )
end

--[[ DRF - 'Shortcuts' -> 'Friends' button handler. ]]
function btnRow1Item2_Button_OnButtonClicked( buttonHandle )
	Log("friendsClicked")

	if Control_GetVisible(buttonHandle) then
		MenuOpen("FriendsPeople.xml")
	end
	
	local ev = KEP_EventCreate("FriendsTabEvent")
	KEP_EventQueue(ev)
end

--[[ DRF - 'Shortcuts' -> 'Events' button handler. ]]
function btnRow1Item3_Button_OnButtonClicked( buttonHandle )
	Log("eventsClicked")
	if Control_GetVisible(buttonHandle) then
		MenuOpen("Events.xml")
	end
end

--[[ DRF - 'Shortcuts' -> 'Messages' button handler. ]]
function btnRow1Item4_Button_OnButtonClicked( buttonHandle )
	Log("messagesClicked")
	if Control_GetVisible(buttonHandle) then
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA.."mykaneva/mailbox.aspx")
	end
end

---------------------------------------------------------------------------------
-----WORLDS CONTROL HANDLERS (Rows 2&3)
---------------------------------------------------------------------------------

--[[ DRF - 'Explore Worlds' button handler. ]]
function btnRow2Default_Button_OnButtonClicked( buttonHandle )
	Log("worldsClicked")
	if Control_GetVisible(buttonHandle) then
		MenuOpen("BrowsePlaces.xml")
	end
	sendQueryPlacesEvent( PlaceCategoryIds.APPS, -1 )
end

--[[ DRF - 'Explore Communities' button handler. ]]
function btnRow3Default_Button_OnButtonClicked( buttonHandle )
	Log("communitiesClicked")
	if Control_GetVisible(buttonHandle) then
		MenuOpen("BrowsePlaces.xml")
	end
	sendQueryPlacesEvent( PlaceCategoryIds.HANGOUTS, -1 )
end

--[[ DRF - Generic 'Worlds' link button handler. ]]
function worlds_Button_OnButtonClicked( buttonHandle, buttonNum)
	Log("worldsClicked=" .. buttonNum)
	if Control_GetVisible(buttonHandle) then
	   local url = g_worlds[buttonNum].url
	   if url ~= nil then
			Log("worldUrl=" .. url)
			gotoURL(url)
	   end
	end
end

function btnRow2Item1_Button_OnButtonClicked( buttonHandle )
	worlds_Button_OnButtonClicked(buttonHandle, 1)
end

function btnRow2Item2_Button_OnButtonClicked( buttonHandle )
	worlds_Button_OnButtonClicked(buttonHandle, 2)
end

function btnRow2Item3_Button_OnButtonClicked( buttonHandle )
	worlds_Button_OnButtonClicked(buttonHandle, 3)
end

function btnRow2Item4_Button_OnButtonClicked( buttonHandle )
	worlds_Button_OnButtonClicked(buttonHandle, 4)
end

function btnRow2Item5_Button_OnButtonClicked( buttonHandle )
	worlds_Button_OnButtonClicked(buttonHandle, 5)
end

function btnRow3Item1_Button_OnButtonClicked( buttonHandle )
	worlds_Button_OnButtonClicked(buttonHandle, 6)
end

function btnRow3Item2_Button_OnButtonClicked( buttonHandle )
	worlds_Button_OnButtonClicked(buttonHandle, 7)
end

function btnRow3Item3_Button_OnButtonClicked( buttonHandle )
	worlds_Button_OnButtonClicked(buttonHandle, 8)
end

function btnRow3Item4_Button_OnButtonClicked( buttonHandle )
	worlds_Button_OnButtonClicked(buttonHandle, 9)
end

function btnRow3Item5_Button_OnButtonClicked( buttonHandle )
	worlds_Button_OnButtonClicked(buttonHandle, 10)
end

---------------------------------------------------------------------------------
-----FRIENDS CONTROL HANDLERS (Row 4)
---------------------------------------------------------------------------------

--[[ DRF - 'Invite Friends' button handler. ]]
function btnRow4Default_Button_OnButtonClicked( buttonHandle )
	Log("friendsClicked")
	if Control_GetVisible(buttonHandle) then
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA.."mykaneva/inviteFriend.aspx")
	end
end

--[[ DRF - Generic 'Friends' link button handler. ]]
function friends_Button_OnButtonClicked( buttonHandle, buttonNum )
	Log("friendsClicked=" .. buttonNum)
	if Control_GetVisible(buttonHandle) then
	   local friend = g_friends[buttonNum]
	   if friend ~= nil then
			local username = friend.username
			local url = GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_CHANNEL_PATH .. username .. ".people"
			Log("friendUrl=" .. url)
			KEP_LaunchBrowser(url)
	   end
	end
end

function btnRow4Item1_Button_OnButtonClicked( buttonHandle )
	friends_Button_OnButtonClicked(buttonHandle, 1)
end

function btnRow4Item2_Button_OnButtonClicked( buttonHandle )
	friends_Button_OnButtonClicked(buttonHandle, 2)
end

function btnRow4Item3_Button_OnButtonClicked( buttonHandle )
	friends_Button_OnButtonClicked(buttonHandle, 3)
end

--[[ DRF - Generic 'Friends' link location button handler. ]]
function friends_LocButton_OnButtonClicked( buttonHandle, buttonNum )
	Log("friendsLocClicked=" .. buttonNum)
	if Control_GetVisible(buttonHandle) then
	   local url = g_friends[buttonNum].url
	   if url ~= nil then
			Log("friendUrl=" .. url)
			gotoURL(url)
	   end
	end
end

function btnRow4Item1_LocButton_OnButtonClicked( buttonHandle )
	friends_LocButton_OnButtonClicked(buttonHandle, 1)
end

function btnRow4Item2_LocButton_OnButtonClicked( buttonHandle )
	friends_LocButton_OnButtonClicked(buttonHandle, 2)
end

function btnRow4Item3_LocButton_OnButtonClicked( buttonHandle )
	friends_LocButton_OnButtonClicked(buttonHandle, 3)
end

function btnCreditRedemptions_OnCheckBoxChanged( buttonHandle )
	launchWebBrowser(GameGlobals.WEB_SITE_PREFIX_KANEVA .. "mykaneva/my3dapps.aspx?sortby=World+Credit+Balance")
	MenuCloseThis()
end