--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

VISUAL_NAMES_EVENT = "VisualNamesEvent"
VISUAL_RESPONSE_EVENT = "VisualResponseEvent"

g_t_collide = {}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "visualsEventHandler", VISUAL_NAMES_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( VISUAL_NAMES_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( VISUAL_RESPONSE_EVENT, KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	local lh = Dialog_GetListBox(gDialogHandle, "ListBoxMain")
	if lh then
		ListBox_RemoveAllItems(lh)
	end
	-- setup look of listbox scrollbars
	local sbh = ListBox_GetScrollBar(lh)

	if sbh ~= 0 then
		local eh = ScrollBar_GetTrackDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 37, 17, 69)
		end

		eh = ScrollBar_GetButtonDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 19, 17, 35)
		end

		eh = ScrollBar_GetUpArrowDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 1, 17, 17)
		end

		eh = ScrollBar_GetDownArrowDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 71, 17, 87)
		end
	end
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnOK_OnButtonClicked( buttonHandle )
	
	local event = KEP_EventCreate( VISUAL_RESPONSE_EVENT )
	KEP_EventEncodeNumber( event, #g_t_collide)
	for i=1, #g_t_collide do
		KEP_EventEncodeNumber( event, g_t_collide[i])
	end
	KEP_EventQueue( event )

	MenuCloseThis()
end

function visualsEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local caption = KEP_EventDecodeString( event )
	local count = KEP_EventDecodeNumber( event )
	local lh = Dialog_GetListBox(gDialogHandle, "ListBoxMain")
	for i=1, count do
		local visName = KEP_EventDecodeString( event )
		g_t_collide[i] = KEP_EventDecodeNumber( event )
		ListBox_AddItem(lh, visName, i)
	end
	lblTitle = Dialog_GetStatic( gDialogHandle, "Menu_Title" )
	Static_SetText( lblTitle, caption )
	doAllChecking()
end

function ListBoxMain_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index, sel_text)
	local visIndex = sel_index + 1
	if bMouseDown == 1 then
		if g_t_collide[visIndex] == 0 then
			g_t_collide[visIndex] = 1
		else
			g_t_collide[visIndex] = 0
		end
		doAllChecking()
	end
end

function doAllChecking()
	if #g_t_collide > 0 then
		local lh = Dialog_GetListBox(gDialogHandle, "ListBoxMain")
		local sb = ListBox_GetScrollBar(lh)
		local spos = ScrollBar_GetTrackPos(sb)

		local loopEnd = math.min(#g_t_collide, 5)
		for i=1, loopEnd do
			local checkVal = g_t_collide[spos + i]
			cb = Dialog_GetCheckBox(gDialogHandle, "cbMeshRow_" .. tostring(i))
			Control_SetEnabled(cb, 1)
			Control_SetVisible(cb, 1)
			CheckBox_SetChecked( cb, checkVal )
		end

		if loopEnd < 5 then
			for i = loopEnd + 1, 5 do
				cb = Dialog_GetCheckBox(gDialogHandle, "cbMeshRow_" .. tostring(i))
				Control_SetEnabled(cb, 0)
				Control_SetVisible(cb, 0)
			end
		end
	end
end

function convertCheckToGlobal(checkIndex)
	local lh = Dialog_GetListBox(gDialogHandle, "ListBoxMain")
	local sb = ListBox_GetScrollBar(lh)
	local spos = ScrollBar_GetTrackPos(sb)
	if g_t_collide[spos + checkIndex] == 1 then
		g_t_collide[spos + checkIndex] = 0
	else
		g_t_collide[spos + checkIndex] = 1
	end
	doAllChecking()
end

function ListBoxMain_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	doAllChecking()
end

function cbSelectAll_OnCheckBoxChanged(checkBoxHandle)
	local curVal = CheckBox_GetChecked( checkBoxHandle )
	local newVal = 0
	if curVal == 0 then
		newVal = 0
	else
		newVal = 1
	end
	for i=1, #g_t_collide do
		g_t_collide[i] = newVal
	end
	doAllChecking()
end

function cbMeshRow_1_OnCheckBoxChanged(checkBoxHandle)
	convertCheckToGlobal(1)
end

function cbMeshRow_2_OnCheckBoxChanged(checkBoxHandle)
	convertCheckToGlobal(2)
end

function cbMeshRow_3_OnCheckBoxChanged(checkBoxHandle)
	convertCheckToGlobal(3)
end

function cbMeshRow_4_OnCheckBoxChanged(checkBoxHandle)
	convertCheckToGlobal(4)
end

function cbMeshRow_5_OnCheckBoxChanged(checkBoxHandle)
	convertCheckToGlobal(5)
end
