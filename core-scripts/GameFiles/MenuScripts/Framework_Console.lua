--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- FrameworkConsole
--
-- Displays events from the server on a 10 second countdown before fading out
-- 
-- TODO:
-- Edit Box input for player input (chat system) - communicated down to server and then to other clients
-- Scrollable
-- Minimizable
-- Resizability
-- Maximize (view previous messages)
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- -- -- -- -- --
-- Constants
-- -- -- -- -- --
-- Screen offset constants
local MENU_WIDTH = 300
local SCREEN_LEFT_BUFFER = 15
local SCREEN_BOTTOM_BUFFER = 94
local HUD_HEIGHT = 48
local STATIC_Y_BUFFER = 5
local STATIC_HEIGHT = 20
local STATIC_LEFT_BUFFER = 5

local ANIMATE_SPEED = 1

-- How many messages can display at once?
local MAX_MESSAGE_COUNT = 5
-- How many messages can be stored in s_writeEvent at any given time?
local MAX_MESSAGE_SAVE_COUNT = 100
-- How long will a write message display in seconds?
local WRITE_TIMEOUT = 5
-- How long will it take a fading message to fade out?
local FADE_TIME = 1

local DEFAULT_COLOR = {r=255, g=255, b=255}

-- -- -- -- -- --
-- Local vars
-- -- -- -- -- --
-- Table of events written from the server
local s_writeEvents = {} -- {text, displayTimer, fadeTimer, color={a,r,g,b}, control}
local s_statics = {}
-- Index of the latest static control being used
local s_staticIndex = 0
-- Up to what static is being displayed
local s_displayIndex = 0
local s_messageQueue = {}
local s_queueHeight

local s_animateHeight = 0
local s_animateMenu = false
local s_menuHeight = 0

local s_menuFilled = false

local s_screenHeight

-- -- -- -- -- --
-- Helper functions
-- -- -- -- -- --
local createMessage --(input)
local queueMessage --(message)
local orderMessages --()
local handleMessageTimeout --(elapsedTime)
local handleMenuAnimation --()

-- When the menu is created
function onCreate()
	-- Register event handler for framework events
	Events.registerHandler("FRAMEWORK_CONSOLE_WRITE", writeEvent)
	
	MenuSetSizeThis(MENU_WIDTH, 0)
	
	s_screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	s_menuHeight = Dialog_GetHeight(gDialogHandle)
	
	Dialog_OnMoved()
	
	-- Generate statics for max message count
	for i=1, MAX_MESSAGE_COUNT do
		local control = Dialog_AddStatic(gDialogHandle, "stcWrite"..tostring(i), "Doop"..tostring(i), STATIC_LEFT_BUFFER,
										 STATIC_Y_BUFFER + ((i-1)*(STATIC_Y_BUFFER+STATIC_HEIGHT)), Dialog_GetWidth(gDialogHandle)-(STATIC_LEFT_BUFFER*2), STATIC_HEIGHT)
		Control_SetVisible(control, false)
		-- Flush message left
		Element_SetTextFormat(Static_GetDisplayElement(control), 20)
		table.insert(s_statics, control)
	end
end

-- When client is moved
function Dialog_OnMoved(dialogHandle, x, y)
	-- Set menu in bottom-left
	MenuSetLocationThis(SCREEN_LEFT_BUFFER, (s_screenHeight - s_menuHeight - SCREEN_BOTTOM_BUFFER - HUD_HEIGHT))
end

-- On frame update
function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- Handle currently displaying messages and their timeouts
	handleMessageTimeout(fElapsedTime)
	
	-- Animate the menu accordingly
	handleMenuAnimation()
end

---------------------
-- Event Handlers
---------------------

-- Write event
function writeEvent(event)
	-- Create a new writeEvent entry based on input
	local message = createMessage(event)
	
	-- If the menu is filled, re-order messages
	if s_menuFilled then
		orderMessages()
	-- Otherwise queue message
	else
		queueMessage(message)
	end
	
	if s_staticIndex == MAX_MESSAGE_COUNT then
		s_menuFilled = true
	end
end

-- Creates a new message entry
createMessage = function(input)
	local color = input.color or {r = 255, g = 255, b = 255}
	local text = input.text or "Error: No text was passed to writeEvent()"
	
	local message = {displayTimer = WRITE_TIMEOUT,
					 fadeTimer = FADE_TIME,
					 text = text,
					 color = {a = 255,
							  r = color.r or DEFAULT_COLOR.r,
							  g = color.g or DEFAULT_COLOR.g,
							  b = color.b or DEFAULT_COLOR.b}
					}
	-- TODO: How many lines does this message need?
	
	s_staticIndex = math.min(s_staticIndex + 1, MAX_MESSAGE_COUNT)
	
	-- Apply message
	Static_SetText(s_statics[s_staticIndex], text)
	
	-- Set message color
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(s_statics[s_staticIndex])), 0, message.color)
	
	-- Insert new message into s_writeEvents
	table.insert(s_writeEvents, message)
	
	-- Clean up messages once our storage cap has been hit
	if #s_writeEvents > MAX_MESSAGE_SAVE_COUNT then
		table.remove(s_writeEvents, 1)
	end
	
	return message
end

-- Adds a message into the queue
queueMessage = function(message)
	-- Hide the control until queued
	-- If max value of message displays has been hit
	if s_staticIndex > MAX_MESSAGE_COUNT then
		-- Remove first message from the queue
		table.remove(s_messageQueue, 1)
	-- Otherwise, add to queue
	else
		-- Expand console for this message
		s_animateMenu = true
		s_animateHeight = math.min((s_staticIndex*(STATIC_Y_BUFFER+STATIC_HEIGHT)) + STATIC_Y_BUFFER, MAX_MESSAGE_COUNT*(STATIC_Y_BUFFER+STATIC_HEIGHT) + STATIC_Y_BUFFER)
		-- If we're currently not expecting any messages
		if not s_queueHeight then
			s_queueHeight = s_animateHeight
		end
	end
	-- Insert this static index for queueing
	table.insert(s_messageQueue, s_staticIndex)
end

-- Order the write messages in the console
orderMessages = function()
	-- Re-order messages according to number being displayed
	local staticIndex = 1
	for messageIndex = #s_writeEvents-s_staticIndex+1, #s_writeEvents do
		Static_SetText(s_statics[staticIndex], s_writeEvents[messageIndex].text)
		
		-- Set static's color
		BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(s_statics[staticIndex])), 0, s_writeEvents[messageIndex].color)
		
		staticIndex = staticIndex + 1
	end
end

-- Handle message timeout timers
handleMessageTimeout = function(elapsedTime)
	-- If messages are currently being displayed
	if s_staticIndex > 0 then
		-- Iterate over all active write entries
		local staticIndex = 1
		for messageIndex = #s_writeEvents-s_staticIndex+1, #s_writeEvents do
			-- If this writeEvent is fading out
			if s_writeEvents[messageIndex].displayTimer <= 0 then
				-- If fade has completed
				if s_writeEvents[messageIndex].fadeTimer <= 0 then
					-- Hide latest message control
					Control_SetVisible(s_statics[s_staticIndex], false)
					
					-- Shift messageQueue indices
					for i,v in pairs(s_messageQueue) do
						s_messageQueue[i] = v - 1
					end
					
					s_staticIndex = math.max(s_staticIndex - 1, 0)
					if s_staticIndex < MAX_MESSAGE_COUNT then
						s_menuFilled = false
					end
					
					-- If we're all out of messages to display
					if s_staticIndex <= 0 then
						-- Auto-minimize the menu
						s_animateMenu = true
						s_animateHeight = 0
					else
						-- Animate the menu down one display layer
						s_animateMenu = true
						s_animateHeight = math.min((s_staticIndex*(STATIC_Y_BUFFER+STATIC_HEIGHT)) + STATIC_Y_BUFFER, MAX_MESSAGE_COUNT*(STATIC_Y_BUFFER+STATIC_HEIGHT) + STATIC_Y_BUFFER)
						-- Update the queueHeight for more messages if there's a queue active
						if s_queueHeight then
							s_queueHeight = s_animateHeight
						end
					end
					
					orderMessages()
					
					-- Position menu
					Dialog_OnMoved()
					break
					
				-- Keep fading
				else
					-- Determine alpha for current fade value
					s_writeEvents[messageIndex].color.a = (s_writeEvents[messageIndex].fadeTimer / (FADE_TIME or 1)) * 255
					BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(s_statics[staticIndex])), 0, s_writeEvents[messageIndex].color)
					
					-- Decrement fadeTimer
					s_writeEvents[messageIndex].fadeTimer = s_writeEvents[messageIndex].fadeTimer - elapsedTime
				end
			else
				-- Decrement displayTimer
				s_writeEvents[messageIndex].displayTimer = s_writeEvents[messageIndex].displayTimer - elapsedTime
			end
			staticIndex = staticIndex + 1
		end
	end
end

-- Handle the menu as it animates
handleMenuAnimation = function()
	-- Animate menu's height
	if s_animateMenu then
		-- Which way are we animating?
		if s_menuHeight > s_animateHeight then
			-- Animate down
			s_menuHeight = math.max(s_menuHeight - ANIMATE_SPEED, s_animateHeight)
		elseif s_menuHeight < s_animateHeight then
			-- Animate up
			s_menuHeight = math.min(s_menuHeight + ANIMATE_SPEED, s_animateHeight)
		end
		if s_queueHeight then
			-- Are we ready to display the next queued message?
			if (s_menuHeight - s_queueHeight) >= 0 then
				orderMessages()
				
				-- Display queued message
				Control_SetVisible(s_statics[s_messageQueue[1]], true)
				s_messageDisplaying = true
				
				if #s_messageQueue > 1 then
					-- Update queue height?
					s_queueHeight = s_queueHeight + (STATIC_Y_BUFFER+STATIC_HEIGHT)
				else
					s_queueHeight = nil
				end
				-- Remove message from queue
				table.remove(s_messageQueue, 1)
			end
		end
		
		MenuSetLocationThis(nil, (s_screenHeight - s_menuHeight - SCREEN_BOTTOM_BUFFER - HUD_HEIGHT))
		
		MenuSetSizeThis(MENU_WIDTH, s_menuHeight)
		
		-- Shall we stop animating now?
		if s_menuHeight == s_animateHeight then
			s_animateMenu = false
		end
	end
end
