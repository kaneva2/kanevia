--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\Scripts\\CameraObjIds.lua")
dofile("..\\Scripts\\UGCImportState.lua")
dofile("..\\Scripts\\TaskState.lua")
dofile("..\\Scripts\\DynamicObjectCreateIds.lua")
dofile("..\\ClientScripts\\MenuGameIds.lua" )
dofile("UGCFunctions.lua")

g_placementId = nil
g_menuGameObj = nil
g_camera = {
	pos = { x = 0, y = 0, z = 0 },
	rot = { x = 0, y = 180, z = 0 },
	zoom = 100,
}
g_scaleBase = 1
g_uploadingFile = ""
g_progressBarWidth = 0
g_progressBarHeight = 0

g_uploadId = nil
g_isDerivative = nil -- deprecated
g_ugcType = nil
g_ugcObjId = nil
g_ugcObjName = nil
g_param1 = nil
g_param2 = nil
g_commission = 0
g_actorTypeIdx = 0				-- use first actor group by default
g_ugcGLID = 0
g_playerGender = "M"
g_stuntMounted = false
g_animInfo = {}
g_menuGameObjectAlwaysOnTop = true

g_animTimeMs = 0

g_ugcSoundPlaying = false

g_previewScreenShot = 1
gPreviewGen = {
	FileNameStem = "",
	InProgress = false,
	DelaySec = 0,
	CurrentFrame = 0,
	CurrentAnimMillis = 0,
	CompletionHandler = nil,
	ProgressHandler = nil,
	FrameRate = 1,
	PreviewDuration = 0,
	AnimDuration = 0,
}

-- Ankit ----- bool to let the script to know whether the image is available to be uploaded on the browser, for UGC content
g_prevImgForBrowserReady = false
g_ImageCaptureEvent = false
g_ImageCaptureCompleted = false

function gPreviewGen.Start()
	gPreviewGen.InProgress = true
	gPreviewGen.DelaySec = 0
	gPreviewGen.CurrentFrame = 0
	gPreviewGen.CurrentAnimMillis = 0
	-- reset progress bar
	displayFilePercent( "Generating preview", 0 )
end

function gPreviewGen.NextFrame()
	gPreviewGen.CurrentFrame = gPreviewGen.CurrentFrame + 1
	gPreviewGen.CurrentAnimMillis = gPreviewGen.CurrentAnimMillis + 1000 / gPreviewGen.FrameRate
	local percent = 100
	if gPreviewGen.PreviewDuration~=0 then
		percent = math.floor(gPreviewGen.CurrentAnimMillis / gPreviewGen.PreviewDuration * 100 + 0.5)
	end
	Log("NextFrame: frame="..gPreviewGen.CurrentFrame.." animMs="..gPreviewGen.CurrentAnimMillis.." pct="..percent)
	displayFilePercent( "Generating preview", percent )
end

function gPreviewGen.Reset()
	gPreviewGen.InProgress = false
	gPreviewGen.DelaySec = 0
	gPreviewGen.CurrentFrame = 0
	gPreviewGen.CurrentAnimMillis = 0
end

ICON_X_MAX = 360
ICON_X_MIN = -360
ICON_Y_MAX = 360
ICON_Y_MIN = -360
ZOOM_MIN = 50
ZOOM_MAX = 200

CONFIG_NECK		= 1
CONFIG_TORSO	= 2
CONFIG_CROTCH	= 3
CONFIG_ARMS		= 4
CONFIG_LEGS		= 5
CONFIG_HANDS	= 6
CONFIG_FEET		= 7
CONFIG_SHIRT	= 8
CONFIG_PANTS	= 9
CONFIG_BOOTS	= 10

FACE_MAIN		= 6
FACE_EYE_RIGHT	= 0
FACE_EYE_LEFT 	= 2
FACE_EYEBROW	= 7
FACE_FACECOVER	= 8

gHairIndex = { 1, 1 }
gHairColor = { 0, 0 }
gSkinColor = { 0, 0 }
gEyeColor = { 0, 0 }
gFacialHairColor = { 0, 0 }
gMakeupColor = { 0, 0 }

gActorTypes = {}
gActorTypes[0] = {}
gActorTypes[0].text = "Male"
gActorTypes[0].actorGroup = 1
gActorTypes[0].gender = "M"
gActorTypes[0].stuntId = 6				-- Not used here
gActorTypes[0].entityId = 14			-- Hondo
gActorTypes[0].config = {}
gActorTypes[0].config.faceGLID = 2988	-- Male Caucasian Head
gActorTypes[0].config.meshConfig = {
	{ slot=CONFIG_NECK,		index=0, color=gSkinColor[0] },
	{ slot=CONFIG_TORSO,	index=1, color=gSkinColor[0] },
	{ slot=CONFIG_CROTCH,	index=1, color=gSkinColor[0] },
	{ slot=CONFIG_ARMS,		index=1, color=gSkinColor[0] },
	{ slot=CONFIG_LEGS,		index=1, color=gSkinColor[0] },
	{ slot=CONFIG_HANDS,	index=0, color=gSkinColor[0] },
	{ slot=CONFIG_FEET,		index=0, color=gSkinColor[0] },
	{ slot=CONFIG_SHIRT,	index=1, color=0 },
	{ slot=CONFIG_PANTS,	index=1, color=0 },
	{ slot=CONFIG_BOOTS,	index=1, color=0 },
	{ slot=25,	index=gHairIndex[0], color=gHairColor[0] },
	{ slot=26,	index=gHairIndex[0], color=gHairColor[0] } }
gActorTypes[0].config.faceConfig = {
	{ slot=FACE_MAIN,		index=0, color=gSkinColor[0] },
	{ slot=FACE_EYE_LEFT,	index=0, color=gEyeColor[0] },
	{ slot=FACE_EYE_RIGHT,	index=0, color=gEyeColor[0] },
	{ slot=FACE_EYEBROW,	index=0, color=gFacialHairColor[0] },
	{ slot=FACE_FACECOVER,	index=0, color=gMakeupColor[0] } }

gActorTypes[1] = {}
gActorTypes[1].text = "Female"
gActorTypes[1].actorGroup = 2
gActorTypes[1].gender = "F"
gActorTypes[1].stuntId = 7				-- Not used here
gActorTypes[1].entityId = 11			-- Sofia
gActorTypes[1].config = {}
gActorTypes[1].config.faceGLID = 2991	-- Female Caucasian Head
gActorTypes[1].config.meshConfig = {
	{ slot=CONFIG_NECK,		index=0, color=gSkinColor[1] },
	{ slot=CONFIG_TORSO,	index=1, color=gSkinColor[1] },
	{ slot=CONFIG_CROTCH,	index=1, color=gSkinColor[1] },
	{ slot=CONFIG_ARMS,		index=0, color=gSkinColor[1] },
	{ slot=CONFIG_LEGS,		index=1, color=gSkinColor[1] },
	{ slot=CONFIG_HANDS,	index=0, color=gSkinColor[1] },
	{ slot=CONFIG_FEET,		index=0, color=gSkinColor[1] },
	{ slot=CONFIG_SHIRT,	index=1, color=0 },
	{ slot=CONFIG_PANTS,	index=1, color=0 },
	{ slot=CONFIG_BOOTS,	index=1, color=0 },
	{ slot=27,	index=gHairIndex[1], color=gHairColor[1] },
	{ slot=28,	index=gHairIndex[1], color=gHairColor[1] },
	{ slot=29,	index=gHairIndex[1], color=gHairColor[1] },
	{ slot=30,	index=gHairIndex[1], color=gHairColor[1] },
	{ slot=31,	index=gHairIndex[1], color=gHairColor[1] } }
gActorTypes[1].config.faceConfig = {
	{ slot=FACE_MAIN,		index=0, color=gSkinColor[1] },
	{ slot=FACE_EYE_LEFT,	index=0, color=gEyeColor[1] },
	{ slot=FACE_EYE_RIGHT,	index=0, color=gEyeColor[1] },
	{ slot=FACE_EYEBROW,	index=0, color=gFacialHairColor[1] },
	{ slot=FACE_FACECOVER,	index=0, color=gMakeupColor[1] } }

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "UGCUploadMenuEventHandler", "UGCUploadMenuEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UploadProgressHandler", "UGCUploadProgressEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "UploadCompletionHandler", "UGCUploadCompletionEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "UGCSubmissionFeedbackEventHandler", UGC_SUBMISSION_FEEDBACK_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "PrevImgAvailableEventHandler", "PrevImgAvailableEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "UGCUploadMenuEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "UGCUploadProgressEvent", KEP.MED_PRIO )
	KEP_EventRegister( "UGCUploadCompletionEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PrevImgAvailableEvent", KEP.MED_PRIO )
	UGCI.RegisterSubmissionEvents( )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	local SliderHandle handlesliderY = Dialog_GetSlider(gDialogHandle, "sldZoom")
	Slider_SetRange( handlesliderY, ZOOM_MIN, ZOOM_MAX )
	Slider_SetValue( handlesliderY, 100 )

	local ih = Dialog_GetControl(gDialogHandle, "imgFileProgressBack")
	g_progressBarWidth = Control_GetWidth( ih )
	g_progressBarHeight = Control_GetHeight( ih )

	-- get player gender
	g_playerGender = KEP_GetLoginGender()
end

function IsObjectTypePlayer(objId)
	return (objId == 0) or ObjectExists(objId, ObjectType.PLAYER)
end

function Dialog_OnDestroy(dialogHandle)

	if g_ugcSoundPlaying then
		KEP_RemoveSoundOnPlayer(0, g_ugcObjId) -- 0=me
		g_ugcSoundPlaying = false
	end

	-- DRF - ED-8019 - Importing animation on existing DO removes DO following import
	if (g_ugcType == UGC_TYPE.DYNOBJ) and (g_placementId ~= nil) then
		KEP_DeleteDynamicObjectPlacement(g_placementId)
	end

	if g_menuGameObj ~= nil then
		KEP_RemoveMenuGameObject( g_menuGameObj )
	end

	if g_stuntMounted then
		KEP_Unmount( )
	end

	Helper_Dialog_OnDestroy( dialogHandle )
end

-- Handler for receiving id from dynamicobjmenu
function UGCUploadMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local result, isOk
	g_uploadId = KEP_EventDecodeString( event )
	g_isDerivative = KEP_EventDecodeNumber( event )
	g_ugcType = KEP_EventDecodeNumber( event )
	g_ugcObjId = KEP_EventDecodeNumber( event )
	g_ugcObjName = KEP_EventDecodeString( event )
	g_param1 = KEP_EventDecodeNumber( event )
	g_param2 = KEP_EventDecodeNumber( event )
	local enableRefreshButton = KEP_EventDecodeNumber( event )
	g_commission = KEP_EventDecodeNumber( event )

	if g_ugcType==UGC_TYPE.DYNOBJ or g_ugcType==UGC_TYPE.DYNOBJ_DERIVATIVE or g_ugcType==UGC_TYPE.PARTICLE_EFFECT then
		g_placementId  = g_param1

	else
		local actorGroup = g_param1
		for actIndex, actRecord in pairs(gActorTypes) do
			if actorGroup==actRecord.actorGroup then
				g_actorTypeIdx = actIndex
			end
		end
		g_ugcGLID = g_ugcObjId

		if g_ugcType==UGC_TYPE.CHARANIM then
			g_placementId = g_param2

			-- DRF - ED-1666 - KEP_GetAnimationInfo() Only Works For My Avatar!
			local objType = ObjectType.DYNAMIC
			if IsObjectTypePlayer(g_placementId) then objType = ObjectType.PLAYER end
			local length, minPlay, looping, speedScale, cropStart, cropEnd = KEP_GetAnimationInfo(objType, g_placementId, g_ugcGLID )
			if length~=nil then
				if cropEnd==-1 then
					cropEnd = length
				end
				g_animInfo.length = length
				g_animInfo.minPlay = minPlay
				g_animInfo.looping = looping
				g_animInfo.speedScale = speedScale
				g_animInfo.cropStart = cropStart
				g_animInfo.cropEnd = cropEnd
				g_animInfo.currDuration = (cropEnd - cropStart) / speedScale	-- effective duration

				local slider = Dialog_GetSlider( gDialogHandle, "sldTime" )
				Slider_SetValue(slider, 0)
				updateAnimTimeMs(0)
			end
		end
	end

	-- change layout based on upload type
	if g_ugcType==UGC_TYPE.OGG_SOUND then
		hideControl( "imgIcon" )
		hideControl( "Static22" )
		hideControl( "btnRefresh" )
		hideControl( "Static23" )
		hideControl( "sldRotateY" )
		hideControl( "btnUp" )
		hideControl( "btnDown" )
		hideControl( "btnLeft" )
		hideControl( "btnRight" )
		hideControl( "Static24" )
		hideControl( "Static25" )
		hideControl( "sldZoom" )
		hideControl( "lblTime" )
		hideControl( "sldTime" )
		hideControl( "Static26" )
		hideControl( "icon_btnDown" )
		hideControl( "icon_btnUp" )
		for bgLoop=0,9 do
			hideControl( "btnBG" .. bgLoop )
			hideControl( "imgBGBorder" .. bgLoop )
		end
		showControl( "lblPreviewSound" )
		showControl( "imgSoundIcon" )
		showControl( "btnTestSound" )
		moveControl( "btnTestSound", 234, 10 )
		g_previewScreenShot = 0
	elseif g_ugcType==UGC_TYPE.CHARANIM then
		showControl( "lblTime" )
		showControl( "sldTime" )
	elseif g_ugcType==UGC_TYPE.PARTICLE_EFFECT then
		showControl( "imgParticleIcon" )
		g_menuGameObjectAlwaysOnTop = false
	end

	-- Load menu game objects
	if g_ugcType==UGC_TYPE.DYNOBJ or g_ugcType==UGC_TYPE.DYNOBJ_DERIVATIVE or g_ugcType==UGC_TYPE.PARTICLE_EFFECT then
		loadMenuDynamicObject()
	elseif g_ugcType==UGC_TYPE.CHARANIM then
		-- The animation could be for a player or dynamic object.
		-- We use the same UGC_TYPE for either, but the dynamic
		-- object has a placement id in param2.  This was set in
		-- AnimImports when the animation is submitted and the feedback
		-- event was created.
		if g_param1 == UGC_EQUIPPABLE_GROUP then
			loadMenuEquippableObject()
		elseif g_param1 == 0 then
			loadMenuDynamicObject()
		else
			loadMenuGameObject()
		end
	elseif g_ugcType==UGC_TYPE.EQUIPPABLE then
		loadMenuEquippableObject()
	end

	local sldRotateY = Dialog_GetControl( gDialogHandle, "sldRotateY" )
	if g_menuGameObj~=nil and sldRotateY~=nil then
		-- apply initial rotation
		sldRotateY_OnSliderValueChanged( sldRotateY )
	end
end

function loadMenuGameObject()

	if g_menuGameObj ~= nil then
		KEP_RemoveMenuGameObject( g_menuGameObj )
	end

	g_menuGameObj = KEP_AddMenuGameObject( gActorTypes[g_actorTypeIdx].entityId, g_ugcGLID )
	g_camera.rot.y = 20
	g_scaleBase = 0.6
	GetSet_SetVector( g_menuGameObj, MenuGameIds.POSITION, 0, 0, 0 )

	--Config the head
	KEP_ArmMenuGameObject( g_menuGameObj, gActorTypes[g_actorTypeIdx].config.faceGLID, 0 )

	--Config skinned meshes
	for idx, meshCfg in ipairs( gActorTypes[g_actorTypeIdx].config.meshConfig ) do
		KEP_SetCharacterAltConfig(g_menuGameObj, meshCfg.slot, meshCfg.index, meshCfg.color )
	end

	-- Invoke RealizeDimConfig on face APD to fix missing head issue
	for idx, faceCfg in ipairs( gActorTypes[g_actorTypeIdx].config.faceConfig ) do
		KEP_MenuGameObjectSetEquippableConfig(g_menuGameObj, gActorTypes[g_actorTypeIdx].config.faceGLID, faceCfg.slot, faceCfg.index, faceCfg.color )
	end

	KEP_MenuGameObjectSetViewportBackgroundColor(g_menuGameObj, 255, 255, 255 )	-- initialize background as white
	if g_menuGameObjectAlwaysOnTop then
		KEP_MenuGameObjectMoveToTop(g_menuGameObj)
	end
	UpdateMenuGameObjectPlacement()
end

function loadMenuDynamicObject()

	if g_menuGameObj ~= nil then
		KEP_RemoveMenuGameObject(g_menuGameObj )
	end

	g_menuGameObj = KEP_AddMenuDynamicObjectByPlacement(g_placementId )

	KEP_MenuGameObjectSetViewportBackgroundColor(g_menuGameObj, 255, 255, 255 )	-- initialize background as white

	if g_menuGameObjectAlwaysOnTop then
		KEP_MenuGameObjectMoveToTop(g_menuGameObj)
	end

	UpdateMenuGameObjectPlacement()

	if g_ugcType==UGC_TYPE.CHARANIM then
		KEP_MenuGameObjectSetAnimationSettings(g_menuGameObj, g_ugcGLID, 1, 0)
		KEP_MenuGameObjectSetAnimationTime(g_menuGameObj, g_animTimeMs)
	end
end

function loadMenuEquippableObject()

	if g_menuGameObj ~= nil then
		KEP_RemoveMenuGameObject(g_menuGameObj )
	end

	if g_ugcType==UGC_TYPE.CHARANIM then
		g_menuGameObj = KEP_AddMenuGameObjectByEquippableGlid(g_placementId )
		KEP_MenuGameObjectSetAnimationSettings(g_menuGameObj, g_ugcGLID, 1, 0)
		KEP_MenuGameObjectSetAnimationTime(g_menuGameObj, g_animTimeMs)
	else
		g_menuGameObj = KEP_AddMenuGameObjectByEquippableGlid( g_ugcGLID )
	end

	KEP_MenuGameObjectSetViewportBackgroundColor(g_menuGameObj, 255, 255, 255 )	-- initialize background as white

	if g_menuGameObjectAlwaysOnTop then
		KEP_MenuGameObjectMoveToTop(g_menuGameObj)
	end

	UpdateMenuGameObjectPlacement()
end

-- Update menu game object placement (menu game objects are not part of the dialog, we need to relocate them when user moves the menu)
-- This function also updates camera position, rotation and zoom.
function UpdateMenuGameObjectPlacement()
	local ih = Dialog_GetImage(gDialogHandle, "imgIcon")
	local ch = Image_GetControl(ih)
	local xPos = Dialog_GetLocationX(gDialogHandle) + Control_GetLocationX(ih)
	local yPos = Dialog_GetLocationY(gDialogHandle) + Control_GetLocationY(ih) + Dialog_GetCaptionHeight(gDialogHandle)
	local width = Control_GetWidth(ch)
	local height = Control_GetHeight(ch)

	local scale = g_scaleBase*(g_camera.zoom/100)

	GetSet_SetVector( g_menuGameObj, MenuGameIds.VP_POSITION, xPos, yPos, 0 )
	GetSet_SetVector( g_menuGameObj, MenuGameIds.VP_DIMENSIONS, width, height, 0 )
	GetSet_SetVector( g_menuGameObj, MenuGameIds.POSITION, g_camera.pos.x, g_camera.pos.y, g_camera.pos.z )
	GetSet_SetVector( g_menuGameObj, MenuGameIds.SCALING, scale, scale, scale )
	GetSet_SetVector( g_menuGameObj, MenuGameIds.ROTATION, g_camera.rot.x, g_camera.rot.y, g_camera.rot.z )
end

function btnLeft_OnButtonClicked( buttonHandle )
	g_camera.rot.y = g_camera.rot.y + 5
end

function btnRight_OnButtonClicked( buttonHandle )
	g_camera.rot.y = g_camera.rot.y - 5
end

function btnUp_OnButtonClicked( buttonHandle )
	g_camera.rot.x = g_camera.rot.x + 5
end

function btnDown_OnButtonClicked( buttonHandle )
	g_camera.rot.x = g_camera.rot.x - 5
end

function sldRotateY_OnSliderValueChanged( sh )
	local value = Slider_GetValue( sh )
	g_camera.rot.y = ICON_Y_MIN + (ICON_Y_MAX - ICON_Y_MIN) * value / 100
end

function sldZoom_OnSliderValueChanged( sh )
	g_camera.zoom = Slider_GetValue( sh )
	if g_menuGameObj~=nil then
		KEP_MenuGameObjectSetDOZoomFactor(g_menuGameObj, g_camera.zoom / 100.0)
	end
end

function updateAnimTimeMs(pct)
	g_animTimeMs = g_animInfo.cropStart + (pct / 100.0) * (g_animInfo.cropEnd - g_animInfo.cropStart)
end

function sldTime_OnSliderValueChanged( sh )
	local value = Slider_GetValue( sh )
	if g_ugcType==UGC_TYPE.CHARANIM then
		KEP_MenuGameObjectSetAnimationSettings(g_menuGameObj, g_ugcGLID, 1, 0)
		updateAnimTimeMs(value)
		KEP_MenuGameObjectSetAnimationTime(g_menuGameObj, g_animTimeMs)
	end
end

function BackgroundButton_OnButtonClicked( buttonHandle )
	local elem = Control_GetDisplayElement( Button_GetControl( buttonHandle ), "normalDisplay" )
	if elem~=nil then
		local blendColor = Element_GetTextureColor( elem )
		local color = BlendColor_GetColor( blendColor, 0 )
		KEP_MenuGameObjectSetViewportBackgroundColor(g_menuGameObj, color.r, color.g, color.b )
	end
end

btnBG0_OnButtonClicked = BackgroundButton_OnButtonClicked
btnBG1_OnButtonClicked = BackgroundButton_OnButtonClicked
btnBG2_OnButtonClicked = BackgroundButton_OnButtonClicked
btnBG3_OnButtonClicked = BackgroundButton_OnButtonClicked
btnBG4_OnButtonClicked = BackgroundButton_OnButtonClicked
btnBG5_OnButtonClicked = BackgroundButton_OnButtonClicked
btnBG6_OnButtonClicked = BackgroundButton_OnButtonClicked
btnBG7_OnButtonClicked = BackgroundButton_OnButtonClicked
btnBG8_OnButtonClicked = BackgroundButton_OnButtonClicked
btnBG9_OnButtonClicked = BackgroundButton_OnButtonClicked

function UploadProgressHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local isDerivative = KEP_EventDecodeNumber( event )
	local ugcType = KEP_EventDecodeNumber( event )
	local uploadId = KEP_EventDecodeString( event )

	if isDerivative==g_isDerivative and ugcType==g_ugcType then
		-- dynamic object
		local ugcObjId = KEP_EventDecodeNumber( event )

		if ugcObjId==g_ugcObjId then

			-- skip four items
			KEP_EventDecodeString( event )	-- origName
			KEP_EventDecodeNumber( event )	-- ugctype again
			KEP_EventDecodeString( event )	-- master upload Id again
			KEP_EventDecodeNumber( event )	-- ugc obj id again

			local percentage = KEP_EventDecodeNumber( event )

			displayFilePercent( "Uploading " .. g_uploadingFile, percentage )
		end
	end
end

-- Updates percent downloaded display for currently downloading file
function displayFilePercent( action, percent )
	local sh = Dialog_GetStatic(gDialogHandle, "lblFilePercent")
	if action~=nil and action~="" then
		Static_SetText(sh, action .. ": " .. tostring(percent).. "%")
	else
		Static_SetText(sh, tostring(percent).. "%")
	end
	local ih = Dialog_GetImage(gDialogHandle, "imgFileProgressFront")
	local progwid = g_progressBarWidth * percent / 100
	Control_SetSize(Image_GetControl(ih), progwid, g_progressBarHeight)
end

function btnSubmit_OnButtonClicked( buttonHandle )
	enableControl( "btnSubmit", false )

	gPreviewGen.FileNameStem = "NONE"
	gPreviewGen.PreviewDuration = 0
	gPreviewGen.ProgressHandler = PreviewGenProgressHandler_SingleIcon
	gPreviewGen.CompletionHandler = PreviewGenCompletionHandler_Default

	-- preview icon name
	local iconNamePrefix = GetPreviewIconNamePrefix( g_ugcType )
	if iconNamePrefix~=nil then
		gPreviewGen.FileNameStem = iconNamePrefix .. g_ugcGLID
	end

	-- new creation
	if g_ugcType==UGC_TYPE.DYNOBJ or g_ugcType==UGC_TYPE.DYNOBJ_DERIVATIVE or g_ugcType==UGC_TYPE.PARTICLE_EFFECT then
		gPreviewGen.FileNameStem = iconNamePrefix .. g_placementId
		gPreviewGen.CompletionHandler = PreviewGenCompletionHandler_DynObj
	elseif g_ugcType==UGC_TYPE.CHARANIM then
		gPreviewGen.FrameRate = 4
		gPreviewGen.PreviewDuration = 5000
		gPreviewGen.ProgressHandler = PreviewGenProgressHandler_IconList
	elseif g_ugcType==UGC_TYPE.OGG_SOUND then
		gPreviewGen.ProgressHandler = nil		-- no progress handler needed
	end

	-- start preview generation
	gPreviewGen.Start()
	if g_menuGameObj ~= nil then
		KEP_MenuGameObjectSetAnimationSettings(g_menuGameObj, g_ugcGLID, 1, 0)
		KEP_MenuGameObjectSetAnimationTime(g_menuGameObj, 0)
	end
end

function UploadCompletionHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local isDerivative = KEP_EventDecodeNumber( event )
	local ugcType = KEP_EventDecodeNumber( event )
	local uploadId = KEP_EventDecodeString( event )
	local ugcObjId = KEP_EventDecodeNumber( event )
	local ugcObjName = KEP_EventDecodeString( event )

	-- skip two values
	local ugcTypeFromUgcMgr = KEP_EventDecodeNumber( event )
	local uploadIdFromUgcMgr = KEP_EventDecodeString( event )

	-- cancelled/failed flag
	local cancelled = KEP_EventDecodeNumber( event )
	local failed = KEP_EventDecodeNumber( event )

	if cancelled~=0 then

		-- upload cancelled
		KEP_MessageBox( "Upload cancelled.", "Message", 0, 0, 200, 150, false, false )
		MenuCloseThis()

	elseif failed~=0 then

		-- upload failed
		KEP_MessageBox( "Upload failed. Please try again at a later time.", "Message", 0, 0, 250, 220, false, false )
		MenuCloseThis()

	else
		-- upload completed
		if IsMasterUGCType(ugcType) then

			local urlCreate = "UploadMemberDesigns.aspx?uploadid=" .. uploadId
			if g_commission~=0 then
				urlCreate = urlCreate .. "&commission=" .. tostring(math.floor(g_commission))
			end
			if GameGlobals.WEB_SITE_PREFIX_SHOP~=nil then
				-- final solution: using shop prefix
				urlCreate = GameGlobals.WEB_SITE_PREFIX_SHOP .. urlCreate
			else
				-- backward compatible url before we add shop prefix to url.lua
				urlCreate = GameGlobals.WEB_SITE_PREFIX_KANEVA .. "Shopping/" .. urlCreate
			end

			KEP_LaunchBrowser( urlCreate )
			KEP_MessageBox( "Upload completed.  Finish creation on web page.", "Message", 0, 0, 200, 150, false, false )
	
			MenuCloseThis()
		elseif ugcType==UGC_TYPE.DYNOBJ_CUSTOMIZATION or ugcType==UGC_TYPE.ZONEOBJ_CUSTOMIZATION then
			UGCI.DeployUGCUpload( uploadId, ugcType, ugcObjId )
		end
	end
end

-- UGC Submission feedback event handler (for DO derivation only)
function UGCSubmissionFeedbackEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter==UGC_TYPE.DYNOBJ_DERIVATIVE then		-- only check filter (ugcType) here, objectid (ugcObjId) is not previously known
		local sfe = UGCI.DecodeSubmissionFeedbackEvent( event, filter, objectid )
		if sfe.param1==g_placementId then
			if sfe.httpStatusCode==200 and sfe.result==SubmissionResult.SUCCEEDED then
				if sfe.isDerivative~=0 and sfe.uploadId~=nil then
					-- UGC derivation
					-- confirmation dialog has been displayed, go ahead and upload object
					-- create icon
					UGC_AddImageToUGCObj( sfe.ugcType, sfe.ugcObjId, UGC_TYPE.TEX_ICON, sfe.iconFullPath, 0 )
					UGCI.UploadUGCObj( 1, sfe.uploadId, sfe.ugcType, sfe.ugcObjId, sfe.objName )		-- 1: is derivation
				end
			end
			return KEP.EPR_CONSUMED
		end
	end
	return KEP.EPR_OK
end

function PrevImgAvailableEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if not g_ImageCaptureCompleted then return end

	g_prevImgForBrowserReady = true
	gPreviewGen.CompletionHandler();
	g_ImageCaptureCompleted = false;
end

function Dialog_OnRender( dialogHandle, elapsedSec )
	if g_menuGameObj ~= nil then
		UpdateMenuGameObjectPlacement()
	end

	if gPreviewGen.InProgress then

		-- Screenshot Capture Preview Frames (10/sec)
		gPreviewGen.DelaySec = gPreviewGen.DelaySec - elapsedSec
		if (gPreviewGen.DelaySec <= 0.0) then
			gPreviewGen.DelaySec = 0.1

			-- Set Preview Frame Animation Settings
			if g_ugcType==UGC_TYPE.CHARANIM then
				if g_animInfo.looping~=0 or g_animInfo.currDuration>gPreviewGen.CurrentAnimMillis then
					if g_menuGameObj~=nil then
						KEP_MenuGameObjectSetAnimationSettings(g_menuGameObj, g_ugcGLID, 1, 0)
						KEP_MenuGameObjectSetAnimationTime(g_menuGameObj, gPreviewGen.CurrentAnimMillis)
					end
				else
					if g_menuGameObj~=nil then
						KEP_MenuGameObjectSetAnimationSettings(g_menuGameObj, 0, 1, 0)
						KEP_MenuGameObjectSetAnimationTime(g_menuGameObj, -1)
					end
				end
			end

			-- Screenshot Preview Frame
			local iconFullPath = nil
			if g_menuGameObj~=nil and g_previewScreenShot ~= 0 then
				g_ImageCaptureEvent = true
				local filePath = gPreviewGen.FileNameStem .. "." .. gPreviewGen.CurrentFrame
				Log("ScreenshotCapture("..filePath..")")
				iconFullPath = KEP_MenuGameObjectScreenshotCapture(g_menuGameObj, filePath )
			end

			-- Update Preview Frame Progress
			if gPreviewGen.ProgressHandler~=nil then
				gPreviewGen.ProgressHandler( gPreviewGen.CurrentFrame, gPreviewGen.CurrentAnimMillis, iconFullPath )
			end

			-- Next Preview Frame
			gPreviewGen.NextFrame()

			-- Preview Generation Complete ?
			if gPreviewGen.CurrentAnimMillis >= gPreviewGen.PreviewDuration then
				gPreviewGen.Reset()
				g_ImageCaptureCompleted = true
				if gPreviewGen.CompletionHandler~=nil then
					gPreviewGen.CompletionHandler()
				end
				return
			end
		end
	end
end

function PreviewGenProgressHandler_SingleIcon( frame, millis, iconPath )
	gPreviewGen.PreviewIconPath = iconPath
end

function PreviewGenProgressHandler_IconList( frame, millis, iconPath )

	-- first icon as default
	if gPreviewGen.PreviewIconPath==nil then
		gPreviewGen.PreviewIconPath = iconPath
	end

	-- more icons
	if gPreviewGen.PreviewIconPaths==nil then
		gPreviewGen.PreviewIconPaths = {}
	end

	table.insert( gPreviewGen.PreviewIconPaths, iconPath )
end

function PreviewGenCompletionHandler_DynObj()
	if not g_prevImgForBrowserReady and g_ImageCaptureEvent then return	end

	PreviewGenCompletionHandler_Default()
	g_prevImgForBrowserReady = false
	g_ImageCaptureEvent = false
end

function PreviewGenCompletionHandler_Default()
	if not g_prevImgForBrowserReady and g_ImageCaptureEvent then return	end

	if gPreviewGen.PreviewIconPath==nil then
		gPreviewGen.PreviewIconPath = "NONE"
	else
		UGC_AddImageToUGCObj( g_ugcType, g_ugcObjId, UGC_TYPE.TEX_ICON, gPreviewGen.PreviewIconPath, 0 )
	end

	if gPreviewGen.PreviewIconPaths~=nil then
		for idx, path in ipairs( gPreviewGen.PreviewIconPaths ) do
			UGC_AddImageToUGCObj( g_ugcType, g_ugcObjId, UGC_TYPE.TEX_ICON, path, idx )
		end
	end

	-- reset progress bar
	displayFilePercent( nil, 0 )
	UGCI.UploadUGCObj( 0, g_uploadId, g_ugcType, g_ugcObjId, g_ugcObjName )	-- 0: not a derivation
	g_prevImgForBrowserReady = false
	g_ImageCaptureEvent = false
end

function btnTestSound_OnButtonClicked( buttonHandle )
	LogInfo("TestSound Clicked: objId="..g_ugcObjId)
	KEP_PlaySoundOnPlayer(0, g_ugcObjId) -- 0=me
	g_ugcSoundPlaying = true
end

function GetPreviewIconNamePrefix( ugcType )
	if ugcType==UGC_TYPE.DYNOBJ or ugcType==UGC_TYPE.DYNOBJ_DERIVATIVE then
		return "UGCDO"
	elseif ugcType==UGC_TYPE.CHARANIM then
		return "UANIM"
	elseif ugcType==UGC_TYPE.OGG_SOUND then
		return nil
	elseif ugcType==UGC_TYPE.EQUIPPABLE	then
		return "UGCEI"
	elseif ugcType==UGC_TYPE.PARTICLE_EFFECT then
		return "UGCPE"
	else
		return "UGCUT"
	end
end

function IsMasterUGCType( ugcType )
	return ugcType==UGC_TYPE.DYNOBJ or ugcType==UGC_TYPE.DYNOBJ_DERIVATIVE or ugcType==UGC_TYPE.CHARANIM or ugcType==UGC_TYPE.OGG_SOUND or ugcType==UGC_TYPE.EQUIPPABLE or ugcType==UGC_TYPE.PARTICLE_EFFECT
end
