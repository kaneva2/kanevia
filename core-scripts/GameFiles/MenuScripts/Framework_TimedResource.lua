--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_TimedResource.lua
--
-- Displays available items for timed resource vendor processing (by the ashmeister)
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_DragDropHelper.lua")
dofile("Easing.lua")
dofile("Framework_VendorHelper.lua")
dofile("Framework_ActorHelper.lua")
dofile("Framework_MenuShadowHelper.lua")
----------------------
-- LOCAL CONSTANTS
----------------------
ITEMS_PER_PAGE = 11
local FLASH_TIME = .4
local MINUTES_IN_WEEK = 10080
local MINUTES_IN_DAY = 1440
local MINUTES_IN_HOUR = 60

local ITEM_TIME_COORDS = {	["Ready"] = {left = 128, right = 153, top = 176, bottom = 201},
							["Cooking"] = {left = 3, right = 28, top = 176, bottom = 201}
						}

----------------------
-- GLOBAL VARIABLES
----------------------
g_displayTable = {}	-- items being displayed :: [{UNID, properties={name, GLID, itemType}, count}]

----------------------
-- MENU VARIABLES
----------------------

-- ITEM TABLES
 g_trades = {} 	-- THE HOLDER OF ALL THE THINGS!! (current trades from player should be stored in here)
						-- output, input, cost, and time :: [{output={UNID, properties={name, GLID, itemType}, count}, input={UNID, properties={name, GLID, itemType}, count}, cost, time}]

local m_playerInventoryByUNID = {}
m_inventory = {}			-- [{UNID, count, name, itemType}]
m_inventoryCounts = {}	-- {UNID:count}

local m_spawnerPID = 0
local m_displaying = false
local m_inputLocations = 	{ 	["txtCost"] = { x = 0, y = 0 },
								["txtCount"] = { x = 0, y = 0 },

								["imgItemBG11"] = { x = 0, y = 0 },
								["imgItem11"] = { x = 0, y = 0 },
								["stcItem11"] = { x = 0, y = 0 },
								["imgItemLevel11"] = { x = 0, y = 0 },
								["btnItem11"] = { x = 0, y = 0 },
								["stcItemCount11"] = { x = 0, y = 0 },
								["imgItemCountBG11"] = { x = 0, y = 0 }
							}
local m_outputLocations = 	{	["imgItemBG10"] = { x = 0, y = 0 },
								["imgItemCountBG10"] = { x = 0, y = 0 },
								["stcItemCount10"] = { x = 0, y = 0 },
								["imgItem10"] = { x = 0, y = 0 },
								["imgItemName10"] = { x = 0, y = 0 },
								["stcItem10"] = { x = 0, y = 0 },
								["imgItemName10"] = { x = 0, y = 0 },
								["stcItem10"] = { x = 0, y = 0 },
								["imgItemOverlay10"] = { x = 0, y = 0 },
								["imgItemLevel10"] = { x = 0, y = 0 },
								["btnItem10"] = { x = 0, y = 0 }
							}

local m_playerName = ""

local m_transactionState = nil
m_vendorPID = nil

local m_flashTime = 0
local m_flashing = "Open"

local m_currTime = 0

local m_elapsedTime = 0

m_gameItemNamesByUNID = {}

m_page = 1
m_selectedPage = 1
m_currTrade = 0
m_timed = true
----------------------
-- Local Functions
----------------------

local initalizeClickToResource -- (numButtons)
local getProcessLocations -- ()
local buyItem -- ()
local orderFinished -- ()
local switchProcessingVisibility -- ()

----------------------------
-- Create Method
----------------------------
function onCreate()
	--log("--- Framework_TimedResource created")
	CloseAllActorMenus()
	KEP_EventRegister("CLOSE_VENDOR", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("updateVendorClientFull", "UPDATE_VENDOR_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCloseMenu", "CLOSE_VENDOR", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "clientResizeEventHandler", "ClientResizeEvent", KEP.LOW_PRIO )


	Events.registerHandler("RETURN_CURRENT_TIME", onReturnCurrentTime)
	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)
	Events.registerHandler("UPDATE_VENDOR_HEADER", updateHeader)
	Events.registerHandler("FRAMEWORK_RETURN_TIMED_VENDS", returnTimedVends)
	Events.registerHandler("UPDATE_TIMED_SPAWNER_PID", returnSpawnerPID)

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()
	InventoryHelper.initializeInventory("g_displayTable", true, ITEMS_PER_PAGE)
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")
	
	-- Request player's inventory
	requestInventory(INVENTORY_PID)
	getProcessLocations()

	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")				
	end

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "player")
	KEP_EventQueue(ev)

	m_playerName = KEP_GetLoginName()

	MenuSetLocationThis(20, 99)
	resizeDropShadow( )
	moveElementsUp()

	for itemNumber=1, 13 do
		if gHandles["stcItemCount" .. itemNumber] and gHandles["imgItemCountBG" .. itemNumber] then
			local quantityLabel = gHandles["stcItemCount" .. itemNumber]

			Control_SetSize(gHandles["imgItemCountBG" .. itemNumber], Control_GetWidth(gHandles["imgItemCountBG" .. itemNumber]) + 5, Control_GetHeight(gHandles["imgItemCountBG" .. itemNumber]))
			Control_SetLocationX(gHandles["imgItemCountBG" .. itemNumber], Control_GetLocationX(gHandles["imgItemCountBG" .. itemNumber]) - 6 )
			Control_SetLocationX(quantityLabel, Control_GetLocationX(quantityLabel) - 3)

		end
	end
end

function onDestroy()
	local cooking = {}
	for i,v in pairs(g_trades) do
		if v.finishTime then
			-- Using the item's vend ID (VID) as an index
			cooking[tostring(v.VID)] = v.finishTime
		end
	end
	-- Always save off vends, even if nothing is cooking
	Events.sendEvent("FRAMEWORK_SAVE_TIMED_VENDS", {PID = m_spawnerPID, timedVends = cooking})
	InventoryHelper.destroy()
	Events.sendEvent("FRAMEWORK_CLOSED_VENDOR", {PID = m_vendorPID})
end

-- Called each frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- Increment time elapsed since menu opened or updated

	-- Check elapsed time and add to current time after a second has passed
	m_elapsedTime = m_elapsedTime + fElapsedTime
	if m_elapsedTime >= 1 then
		m_currTime = m_currTime + 1
		m_elapsedTime = m_elapsedTime - 1

		-- Update trades (menu elements)
		local startIndex = VENDOR_ITEMS_PER_PAGE * (m_page - 1) + 1
		local endIndex = VENDOR_ITEMS_PER_PAGE * m_page
		for i=startIndex, endIndex do
			local timeIndex = i -  (VENDOR_ITEMS_PER_PAGE * (m_page - 1))
			local element = Image_GetDisplayElement(gHandles["imgItemTime" .. timeIndex])
			if g_trades[i] and g_trades[i].finishTime then
				if g_trades[i].finishTime <= m_currTime then
					Element_SetCoords(element, ITEM_TIME_COORDS["Ready"].left, ITEM_TIME_COORDS["Ready"].top, ITEM_TIME_COORDS["Ready"].right, ITEM_TIME_COORDS["Ready"].bottom)
				elseif g_trades[i].finishTime > m_currTime then
					Element_SetCoords(element, ITEM_TIME_COORDS["Cooking"].left, ITEM_TIME_COORDS["Cooking"].top, ITEM_TIME_COORDS["Cooking"].right, ITEM_TIME_COORDS["Cooking"].bottom)
				end
			end
		end
		-- Update trades (timers)
		if g_trades[m_currTrade] then
			if g_trades[m_currTrade].finishTime then
				local time = convertTime(g_trades[m_currTrade].finishTime - m_currTime)

				if g_trades[m_currTrade].finishTime <= m_currTime then
					orderFinished()
				else
					if time.hour > 0 then
						setTime()
					elseif time.minute > 0 then
						setTime()
					elseif time.second > 0 then
						setTime()
					end
				end
			end
		end

	end

	if m_flashing == "Open" then
		local processBG = gHandles["imgProcessBG"]
		local border = gHandles["imgIconBorder"]
		m_flashTime = m_flashTime - fElapsedTime

		-- Are we still animating?
		if m_flashTime > 0 and Control_GetHeight(processBG) < 127 then
			-- Position of flash accounts for size of the bar + the flash size
			local flashHeight = Easing.outQuint(FLASH_TIME - m_flashTime, 0, 127, FLASH_TIME)
		 	Control_SetSize(processBG, Control_GetWidth(processBG), flashHeight)
		 	Control_SetSize(border, Control_GetWidth(border), 398 + flashHeight)
		 	MenuSetSizeThis(303, 445 + flashHeight)
		-- Done flashing
		else
			if Control_GetHeight(processBG) ~= 127 then
			 	Control_SetSize(processBG, Control_GetWidth(processBG), 127)
			 	Control_SetSize(border, Control_GetWidth(border), 525)
			 	MenuSetSizeThis(303, 572)
			end

			-- Set resource controls to visible
			switchProcessingVisibility(true)

			-- Set counts of resource after it has been tweened
			setCounts()
			updateItemContainer()

			--m_flashing = ""
		end
	elseif m_flashing == "Closed" then
		local processBG = gHandles["imgProcessBG"]
		local border = gHandles["imgIconBorder"]
		--local outerMenu = gHandles["Framework_TimedResource"]
		m_flashTime = m_flashTime - fElapsedTime

		-- Are we still animating?
		if m_flashTime > 0 and Control_GetHeight(processBG) > 0 then
			-- Position of flash accounts for size of the bar + the flash size
			local flashHeight = Easing.outQuint(FLASH_TIME - m_flashTime, 0, 127, FLASH_TIME)
		 	Control_SetSize(processBG, Control_GetWidth(processBG), 127 - flashHeight)
		 	Control_SetSize(border, Control_GetWidth(border), 525 - flashHeight)
		 	MenuSetSizeThis(303, 572 - flashHeight)
		-- Done flashing
		else
			if Control_GetHeight(processBG) ~= 0 then
			 	Control_SetSize(processBG, Control_GetWidth(processBG), 0)
			 	Control_SetSize(border, Control_GetWidth(border), 398)
			 	MenuSetSizeThis(303, 445)
			end
			--m_flashing = ""
		end
	end
end

----------------------------
-- Event Handlers
----------------------------
function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	if success and m_transactionState ~= nil then
		-- Reset the finish time on this trade
		if m_transactionState == "Pick Up" or m_transactionState == "Cancel" then
			g_trades[m_currTrade].finishTime = nil
		elseif m_transactionState == "Place Order" then
			local timeInSeconds = g_trades[m_currTrade].time * 60
			g_trades[m_currTrade].finishTime = m_currTime + timeInSeconds
		end
		switchProcessingVisibility(true)
		setCounts()
		updateItemContainer()
		updateInventory()
	end
	
	m_transactionState = nil
end

-- Called when a new container inventory is received.  Updates all items and re-displays.
function updateVendorClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local returnedPID = KEP_EventDecodeNumber(tEvent)
	if m_vendorPID == nil then
		m_vendorPID = returnedPID
	elseif m_vendorPID ~= returnedPID then
		return
	end
	g_trades = decompileInventory(tEvent)

	--printTable(g_trades)
	updateInventory()
	updateItemContainer()


	m_currTrade = 1

	updateHighlightPosition()

	switchProcessingVisibility(true)

	--Get current time from server and start checking player vends
	Events.sendEvent("FRAMEWORK_REQUEST_CURRENT_TIME", {playerName = KEP_GetLoginName(), PID = m_vendorPID})
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if m_inventory[updateIndex] then
		m_inventory[updateIndex] = updateItem
		m_playerInventoryByUNID[updateItem.UNID] = updateItem.count
		updateInventory()
		updateItemContainer()
		if m_currTrade ~= 0 then
			setCounts()
		end
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_inventory = decompileInventory(tEvent)
	
	for i = 1, #m_inventory do
		m_playerInventoryByUNID[m_inventory[i].UNID] = m_inventory[i].count
	end

	updateInventory()
	updateItemContainer()
	setCounts()
end

function onReturnCurrentTime(event)
	m_currTime = event.timestamp or m_currTime -- modding the time down for floating point calculation issues
end

-- Handles timed vends returned for this player
function returnTimedVends(event)
	if event.timedVends and type(event.timedVends) == "table" then
		for vidIndex, finishTime in pairs(event.timedVends) do
			if finishTime ~= 0 then
				for resourceIndex, resourceItem in pairs(g_trades) do
					if tonumber(resourceItem.VID) == tonumber(vidIndex) then
						g_trades[resourceIndex].finishTime = finishTime
						
						updateInventory()
						updateItemContainer()
					end
				end
			end
		end
	end
end

function returnSpawnerPID(event)
	m_spawnerPID = event.spawnPID

	-- Get the player's timed Vends info for this PID
	-- Request this player's timedVends
	--If no spawnerPID, then use Vendor's pid
	if m_spawnerPID == nil then
		m_spawnerPID = event.PID
	end
	
	Events.sendEvent("FRAMEWORK_GET_TIMED_VENDS", {PID = m_spawnerPID})
end
----------------------------
-- Local Functions
----------------------------

orderFinished = function()
	switchProcessingVisibility(true)
end

Dialog_OnLButtonDownInside = function(dialogHandle, x, y)
	local resourceClicked = false
	local prevResourceClicked = false -- temporary variable for tracking if a resource was already selected

	-- Another resource is already selected
	if m_currTrade ~= 0 then
		prevResourceClicked = true
	end
	
	-- If resource section active, don't close when clicking within this section
	if (Control_ContainsPoint(gHandles["btnPlaceOrder"], x, y) == 1) and Control_GetEnabled(gHandles["btnPlaceOrder"]) == 1 and m_transactionState == nil then
		m_transactionState = Static_GetText(gHandles["btnPlaceOrder"])

		-- Update current time
		Events.sendEvent("FRAMEWORK_REQUEST_CURRENT_TIME", {playerName = KEP_GetLoginName(), PID = m_vendorPID})

		local transactionID = KEP_GetCurrentEpochTime()
		if m_transactionState == "Exchange" then
			local addTable = {}
			local addItem = {UNID = g_trades[m_currTrade].output, count = g_trades[m_currTrade].outputCount, properties = g_trades[m_currTrade].outputProperties}
			--local addItem = deepCopy(g_trades[m_currTrade].output)
			addItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "TimedVendor", transactionID = transactionID}
			addItem.count = g_trades[m_currTrade].outputCount or 1
			table.insert(addTable, addItem)
			local removeTable = {}
			local removeItem = {UNID = g_trades[m_currTrade].input, count = g_trades[m_currTrade].cost}
			removeItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "TimedVendor", sourceSlot = m_currTrade, transactionID = transactionID}
			table.insert(removeTable, removeItem)

			processTransaction(removeTable, addTable)
		elseif m_transactionState == "Place Order" then
			local removeTable = {}
			local removeItem = {UNID = g_trades[m_currTrade].input, count = g_trades[m_currTrade].cost}
			removeItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "TimedVendor", sourceSlot = m_currTrade, transactionID = transactionID}
			table.insert(removeTable, removeItem)

			processTransaction(removeTable, {})
		elseif m_transactionState == "Cancel" then
			local addTable = {}
			local addItem = {UNID = g_trades[m_currTrade].input, count = g_trades[m_currTrade].cost, properties = g_trades[m_currTrade].inputProperties}
			addItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "TimedVendor", transactionID = transactionID}
			addItem.count =  g_trades[m_currTrade].cost
			addItem.randomizeDurability = true
			table.insert(addTable, addItem)
			
			processTransaction({}, addTable)
		elseif m_transactionState == "Pick Up" then
			local addTable = {}
			local addItem = {UNID = g_trades[m_currTrade].output, count = g_trades[m_currTrade].outputCount, properties = g_trades[m_currTrade].outputProperties}
			addItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "TimedVendor", transactionID = transactionID}
			addItem.count = g_trades[m_currTrade].outputCount or 1
			table.insert(addTable, addItem)
			
			processTransaction({}, addTable)
		end
	end

	-- If resource section active, don't close when clicking within this section
	if (Control_ContainsPoint(gHandles["imgProcessBG"], x, y) == 1) then
		if m_currTrade ~= 0 then
			resourceClicked = true
		end
	end

	for i=1, VENDOR_ITEMS_PER_PAGE do
		if (Control_ContainsPoint(gHandles["imgItem"..i], x, y) == 1) and g_displayTable[i].output ~= 0 then
			resourceClicked = true
			m_currTrade = i +  ((m_page-1) * VENDOR_ITEMS_PER_PAGE)
			m_selectedPage = m_page
		end
	end

	if (Control_ContainsPoint(gHandles["btnNext"], x, y) == 1) and Control_GetEnabled(gHandles["btnNext"]) == 1 then
		changePage(1)
	end
	if (Control_ContainsPoint(gHandles["btnBack"], x, y) == 1) and Control_GetEnabled(gHandles["btnBack"]) == 1 then
		changePage(-1)
	end

	if resourceClicked then
		-- Check if a resource has already been selected
		if prevResourceClicked then
			switchProcessingVisibility(true)
			setCounts()
			updateItemContainer()
		else
			--expandResource(true)
		end
		-- Update inventory and overlays on resource click
		updateInventory()
	end

	updateHighlightPosition()
end

updateHighlightPosition = function ()
	-- don't move the highlight if there isn't a current resource collected
	if g_displayTable[1].outPut == 0 then
		Control_SetVisible(gHandles['imgHighlight'], false)
		return
	else
		Control_SetVisible(gHandles['imgHighlight'], true)
	end 
	if m_currTrade <= 0 then
		return
	end

	-- update the highlight graphic 
	updateHighlightLocation()
end

switchProcessingVisibility = function(isVisible)
	-- bail out if there aren't any resources to choose from 
	if m_currTrade == 0 then
		isVisible = false 
	end 

	local currentOrdering = false

	-- Set purchasable item preview visibilities
	Control_SetVisible(gHandles["imgItemBG10"], isVisible)
	Control_SetVisible(gHandles["imgItemCountBG10"], isVisible)
	Control_SetVisible(gHandles["stcItemCount10"], isVisible)
	Control_SetVisible(gHandles["imgItem10"], isVisible)
	Control_SetVisible(gHandles["imgItemName10"], isVisible)
	Control_SetVisible(gHandles["stcItem10"], isVisible)
	Control_SetVisible(gHandles["imgItemOverlay10"], isVisible)
	Control_SetVisible(gHandles["imgItemLevel10"], isVisible)
	Control_SetVisible(gHandles["btnItem10"], isVisible)
	Control_SetVisible(gHandles["btnPlaceOrder"], isVisible)

	Control_SetEnabled(gHandles["imgItemName10"], isVisible)
	Control_SetEnabled(gHandles["stcItem10"], isVisible)

	-- If current queuedResource has a finish time then show 
	if isVisible then

		if  g_trades[m_currTrade].time == 0 then
			setCounts()
			updateItemContainer()

			for i,v in pairs(m_outputLocations) do
				Control_SetLocation(gHandles[i], m_outputLocations[i].x, m_outputLocations[i].y)
			end
			Static_SetText(gHandles["btnPlaceOrder"], "Exchange")

		elseif g_trades[m_currTrade].finishTime and g_trades[m_currTrade].finishTime > m_currTime then
			currentOrdering = true
			isVisible = false

			setTime()

			for i,v in pairs(m_outputLocations) do
				Control_SetLocation(gHandles[i], m_outputLocations[i].x, m_outputLocations[i].y)
			end

			Static_SetText(gHandles["btnPlaceOrder"], "Cancel")
		elseif g_trades[m_currTrade].finishTime and g_trades[m_currTrade].finishTime <= m_currTime then
			currentOrdering = false
			isVisible = false

			local offset = 93
			for i,v in pairs(m_outputLocations) do
				Control_SetLocation(gHandles[i], m_outputLocations[i].x+offset, m_outputLocations[i].y)
			end

			Static_SetText(gHandles["btnPlaceOrder"], "Pick Up")
		else
			setCounts()
			updateItemContainer()

			for i,v in pairs(m_outputLocations) do
				Control_SetLocation(gHandles[i], m_outputLocations[i].x, m_outputLocations[i].y)
			end

			Static_SetText(gHandles["btnPlaceOrder"], "Place Order")
		end
	end
	Control_SetVisible(gHandles["imgTimeBG"], currentOrdering)
	Control_SetVisible(gHandles["imgTimeLarge"], currentOrdering)
	Control_SetVisible(gHandles["txtMinutesRemaining"], currentOrdering)
	Control_SetVisible(gHandles["txtTimeCostRemaining"], currentOrdering)

	Control_SetVisible(gHandles["imgProcessCostBG"], isVisible)
	Control_SetVisible(gHandles["txtCount"], isVisible)
	Control_SetVisible(gHandles["txtCost"], isVisible)
	Control_SetVisible(gHandles["imgDivider"], isVisible)
	Control_SetVisible(gHandles["imgItemCountBG11"], isVisible)
	Control_SetVisible(gHandles["stcItemCount11"], isVisible)
	Control_SetVisible(gHandles["txtBackpackCount"], isVisible)
	Control_SetVisible(gHandles["imgItemBG11"], isVisible)
	Control_SetVisible(gHandles["imgItemName11"], isVisible)
	Control_SetVisible(gHandles["stcItem11"], isVisible)
	Control_SetVisible(gHandles["imgItem11"], isVisible)
	Control_SetVisible(gHandles["btnItem11"], isVisible)
	Control_SetVisible(gHandles["imgItemLevel11"], isVisible)
	Control_SetVisible(gHandles["imgProcessTimeBG"], isVisible)
	Control_SetVisible(gHandles["txtTimeCost"], isVisible)
	Control_SetVisible(gHandles["txtMinutes"], isVisible)
	Control_SetVisible(gHandles["imgTimeSmall"], isVisible)
end

setTime = function()
	local time = convertTime(g_trades[m_currTrade].finishTime - m_currTime)
	if time.day > 0 then
		Static_SetText(gHandles["txtTimeCostRemaining"], tostring(time.day))
		
		if time.day == 1 then
			Static_SetText(gHandles["txtMinutesRemaining"], "Day Remaining")
		else
			Static_SetText(gHandles["txtMinutesRemaining"], "Days Remaining")
		end 
	elseif time.hour > 0 then
		Static_SetText(gHandles["txtTimeCostRemaining"], tostring(time.hour))
		
		if time.hour == 1 then
			Static_SetText(gHandles["txtMinutesRemaining"], "Hour Remaining")
		else
			Static_SetText(gHandles["txtMinutesRemaining"], "Hours Remaining")
		end

	elseif time.minute > 0 then
		Static_SetText(gHandles["txtTimeCostRemaining"], tostring(time.minute))
		if time.minute == 1 then
			Static_SetText(gHandles["txtMinutesRemaining"], "Minute Remaining")
		else
			Static_SetText(gHandles["txtMinutesRemaining"], "Minutes Remaining")
		end
	elseif time.second > 0 then
		Static_SetText(gHandles["txtTimeCostRemaining"], tostring(time.second))
		if time.second == 1 then
			Static_SetText(gHandles["txtMinutesRemaining"], "Second Remaining")
		else
			Static_SetText(gHandles["txtMinutesRemaining"], "Seconds Remaining")
		end
	end
end

-- Converts time for readability
function convertTime(timestamp)
	local time = {}
	time["timestamp"] = timestamp
	-- time["year"]  = math.floor((timestamp / 31556926) + 1970) --date serials start at 1/1/1970
	-- time["month"] = math.floor((math.fmod(timestamp, 31556926) / 2629743) + 1)  --add 1 0 based
	time["day"]   =  math.floor(math.fmod(timestamp, 2629743) / 86400)
	
	time["hour"]   = math.floor(math.fmod(timestamp, 86400) / 3600)
	time["minute"] = math.floor(math.fmod(timestamp , 3600) / 60 )
	time["second"] = math.floor(math.fmod(timestamp, 60))
	
	return time
end

-- Sets the counts for the currently selected item
function setCounts()
	-- bail out if there aren't any resources to choose from 
	if m_currTrade == 0 then
		return
	end 

	-- Set required item count statics
	local hasNuf = true
	local color
	local offset = 12

	local reqItem = g_trades[m_currTrade].input
	local reqNeedCount = g_trades[m_currTrade].cost
	local reqTime = g_trades[m_currTrade].time
	
	-- Get how many of this item the player has
	local reqHasCount = m_inventoryCounts[reqItem] or 0
	
	-- Set the have color to red if you don't got nuf
	if reqNeedCount > reqHasCount then
		-- Set color to red
		hasNuf = false
		color = {a=255,r=255,g=0,b=0}
	else
		-- Set color to white
		color = {a=255,r=255,g=255,b=255}
	end	
	local stcElementHandle = Static_GetDisplayElement(gHandles["txtCount"])
	BlendColor_SetColor(Element_GetFontColor(stcElementHandle), 0, color)
	
	-- Set how many of this item the player has
	--log("--- does the player hasNuf? ".. hasNuf)

	if  g_trades[m_currTrade].finishTime then
		Control_SetEnabled(gHandles["imgItemOverlay10"], false)
		Control_SetVisible(gHandles["imgItemOverlay10"], false)
		Control_SetVisible(gHandles["txtBackpackCount"], false)
		Control_SetVisible(gHandles["imgDivider"], false)

	elseif not hasNuf   then		Control_SetEnabled(gHandles["imgItemOverlay10"], not hasNuf)
		Control_SetVisible(gHandles["imgItemOverlay10"], not hasNuf)

		Static_SetText(gHandles["txtBackpackCount"], formatNumberSuffix(reqHasCount).." In Backpack")
		Control_SetVisible(gHandles["txtBackpackCount"], true)
		Control_SetVisible(gHandles["imgDivider"], true)


		for i,v in pairs(m_inputLocations) do
			Control_SetLocation(gHandles[i], m_inputLocations[i].x, m_inputLocations[i].y)
		end
	else
		Control_SetEnabled(gHandles["imgItemOverlay10"], not hasNuf)
		Control_SetVisible(gHandles["imgItemOverlay10"], not hasNuf)
		
		Control_SetVisible(gHandles["txtBackpackCount"], false)
		Control_SetVisible(gHandles["imgDivider"], false)
		
		-- Move input to center of area
		for i,v in pairs(m_inputLocations) do
			Control_SetLocation(gHandles[i], m_inputLocations[i].x, m_inputLocations[i].y+offset)
		end
	end
	Static_SetText(gHandles["txtCount"], formatNumberSuffix(reqNeedCount))

	if reqTime >= MINUTES_IN_WEEK then 
		Static_SetText(gHandles["txtTimeCost"], reqTime/MINUTES_IN_WEEK)
		Static_SetText(gHandles["txtMinutes"], "Week")
	elseif reqTime >= MINUTES_IN_DAY then
		Static_SetText(gHandles["txtTimeCost"], reqTime/MINUTES_IN_DAY)
		Static_SetText(gHandles["txtMinutes"], "Day")
	elseif reqTime >= MINUTES_IN_HOUR then
		Static_SetText(gHandles["txtTimeCost"], reqTime/MINUTES_IN_HOUR)
		Static_SetText(gHandles["txtMinutes"], "Hours")
	else
		Static_SetText(gHandles["txtTimeCost"], reqTime)
		Static_SetText(gHandles["txtMinutes"], "Minutes")
	end

	
	-- Enable the crafting button?
	if g_trades[m_currTrade].finishTime then
		Control_SetEnabled(gHandles["btnPlaceOrder"], true)
	else
		Control_SetEnabled(gHandles["btnPlaceOrder"], hasNuf)
	end
	return hasNuf
end

-- Set if the designated item is craftable
function setPurchasableOverlays()

	-- Iterate of the recipes we'll be displaying
	for i=1, VENDOR_ITEMS_PER_PAGE do
		local hasNuf = true
		if g_displayTable[i] then
			if g_displayTable[i].cost > (m_inventoryCounts[g_displayTable[i].input] or 0) then
				hasNuf = false
			end
		
			-- If this item has a finish time, then it's overlay is invisible and time is visible
			local resourceIndex = i + ((m_page -1) * VENDOR_ITEMS_PER_PAGE)
			if g_trades[resourceIndex] and g_trades[resourceIndex].finishTime then --  and g_trades[i].finishTime >= m_currTime
				Control_SetEnabled(gHandles["imgItemTime"..i], true)
				Control_SetVisible(gHandles["imgItemTime"..i], true)

				Control_SetEnabled(gHandles["imgItemOverlay"..i], false)
				Control_SetVisible(gHandles["imgItemOverlay"..i], false)
			else
				Control_SetEnabled(gHandles["imgItemTime"..i], false)
				Control_SetEnabled(gHandles["imgItemTime"..i], false)

				Control_SetEnabled(gHandles["imgItemOverlay"..i], not hasNuf)
				Control_SetVisible(gHandles["imgItemOverlay"..i], not hasNuf)
			end
		end
	end
end

-- Get initial location of process output, input/cost objects in menu
getProcessLocations = function()
	for i,v in pairs(m_inputLocations) do
		m_inputLocations[i].x 			= Control_GetLocationX(gHandles[i])
		m_inputLocations[i].y 			= Control_GetLocationY(gHandles[i])
	end

	for i,v in pairs(m_outputLocations) do
		m_outputLocations[i].x 			= Control_GetLocationX(gHandles[i])
		m_outputLocations[i].y 			= Control_GetLocationY(gHandles[i])
	end
end