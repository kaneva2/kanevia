--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\DevToolsConstants.lua")
dofile("..\\MenuScripts\\UGCFunctions.lua")
dofile("..\\Scripts\\AppEditingImportStateIds.lua")

APP_EDIT_IMPORT_PROGRESS_EVENT = "AppEditImportProgressEvent"
PUBLISH_COMPLETE_EVENT = "3DAppAssetPublishCompleteEvent"

STATE_PENDING = 0
STATE_PUBLISHING = 1
STATE_FINISHED = 2

g_t_pending = {}
g_display_state = nil
g_cancel_publish = false

--A lookup table for filetypes to extensions
extensions = {}
extensions[0] = ""
extensions[1] = ".lua"
extensions[2] = ".xml"
extensions[3] = ".dds"
extensions[4] = ".tga"
extensions[5] = ".lua"
extensions[6] = ".lua"
extensions[7] = ".bmp"
extensions[8] = ".png"
extensions[9] = ".jpg"
extensions[10] = ".jpeg"

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "PublishCompleteHandler", PUBLISH_COMPLETE_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, APP_EDIT_IMPORT_PROGRESS_EVENT, KEP.HIGH_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, UGC_SUBMISSION_FEEDBACK_EVENT, KEP.HIGH_PRIO )
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, UGC_UPLOAD_COMPLETION_EVENT, KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	switchDisplay(STATE_PENDING)
end

function isSupportedType(type)
	if type==DevToolsUpload.ACTION_ITEM or type==DevToolsUpload.ACTION_INSTANCE or type==DevToolsUpload.UNKNOWN then
		return false
	end

	local ext = extensions[type]
	if ext==nil then
		-- invalid type: log error
		LogError( "isSupportedType: Error getting file name extension for asset type " .. tostring(type) .. " (" .. prettyUploadName(type) .. ")" )
		return false
	end

	return true
end

function getPendingFiles()
	for _, type in pairs(DevToolsUpload) do
		if isSupportedType(type) then		-- skip types like action items which will be handled by action item UI
			local count = KEP_Count3DAppUnpublishedAssets(type)
			for i=1,count do
				local name, fullpath, modified = KEP_Get3DAppUnpublishedAsset(type, i)
				if modified==1 then
					table.insert(g_t_pending, { name=name, path=fullpath, type=type })
				end
			end
		end
	end

	if #g_t_pending==0 then
		KEP_MessageBox( "Your app is up to date." )
		MenuCloseThis()
	end

	-- Keep getting unpublished files for each type and adding to g_t_pending
	gettinListyWithIt()
end

function gettinListyWithIt()
	local activeType = -1
	for _, item in ipairs(g_t_pending) do
		-- if new type, add a tag line to the list
		if item.type ~= activeType then
			ListBox_AddItem(gHandles.lbxPublishItem, prettyUploadName(item.type), 0)
		end

		-- add item to the list
		local ext = tostring(extensions[item.type])
		ListBox_AddItem(gHandles.lbxPublishItem, "      " .. item.name .. ext, 0)
		activeType = item.type
	end
end

function switchDisplay(state)
	clearDisplay()
	if state == STATE_PENDING then
		g_display_state = STATE_PENDING
		Static_SetText(gHandles["stcHeading"], "The following files will be published:")
		Static_SetText(Button_GetStatic(gHandles["btnCenter"]), "Publish")
		Control_SetVisible(Static_GetControl(gHandles["stcHeading"]), true)
		Control_SetVisible(ListBox_GetControl(gHandles["lbxPublishItem"]), true)
		Control_SetVisible(Button_GetControl(gHandles["btnCenter"]), true)
		Control_SetEnabled(Static_GetControl(gHandles["stcHeading"]), true)
		Control_SetEnabled(ListBox_GetControl(gHandles["lbxPublishItem"]), true)
		Control_SetEnabled(Button_GetControl(gHandles["btnCenter"]), true)
		ListBox_RemoveAllItems(gHandles["lbxPublishItem"])
		getPendingFiles()

	elseif state == STATE_PUBLISHING then
		g_display_state = STATE_PUBLISHING
		Static_SetText(gHandles["stcHeading"], "Publishing 3D App, please wait...")
		Static_SetText(Button_GetStatic(gHandles["btnCenter"]), "Cancel")
		Control_SetVisible(Static_GetControl(gHandles["stcHeading"]), true)
		Control_SetVisible(ListBox_GetControl(gHandles["lbxPublishItem"]), true)
		Control_SetVisible(Button_GetControl(gHandles["btnCenter"]), true)
		Control_SetEnabled(Static_GetControl(gHandles["stcHeading"]), true)
		Control_SetEnabled(ListBox_GetControl(gHandles["lbxPublishItem"]), true)
		Control_SetEnabled(Button_GetControl(gHandles["btnCenter"]), true)
		ListBox_RemoveAllItems(gHandles["lbxPublishItem"])
		ListBox_AddItem(gHandles["lbxPublishItem"], "Starting.", 0)

	elseif state == STATE_FINISHED then
		g_display_state = STATE_FINISHED
		Static_SetText(gHandles["stcHeading"], "Publish Complete!")
		Static_SetText(Button_GetStatic(gHandles["btnCenter"]), "Done")
		Control_SetVisible(Static_GetControl(gHandles["stcHeading"]), true)
		Control_SetVisible(ListBox_GetControl(gHandles["lbxPublishItem"]), true)
		Control_SetVisible(Button_GetControl(gHandles["btnCenter"]), true)
		Control_SetEnabled(Static_GetControl(gHandles["stcHeading"]), true)
		Control_SetEnabled(ListBox_GetControl(gHandles["lbxPublishItem"]), true)
		Control_SetEnabled(Button_GetControl(gHandles["btnCenter"]), true)
	end
end

function clearDisplay()
	Control_SetVisible(Static_GetControl(gHandles["stcHeading"]), false)
	Control_SetEnabled(Static_GetControl(gHandles["stcHeading"]), false)
	Control_SetVisible(Static_GetControl(gHandles["stcHeadingBold"]), false)
	Control_SetEnabled(Static_GetControl(gHandles["stcHeadingBold"]), false)
	Control_SetVisible(Static_GetControl(gHandles["stcBody"]), false)
	Control_SetEnabled(Static_GetControl(gHandles["stcBody"]), false)
	Control_SetVisible(Button_GetControl(gHandles["btnLeft"]), false)
	Control_SetEnabled(Button_GetControl(gHandles["btnLeft"]), false)
	Control_SetVisible(Button_GetControl(gHandles["btnRight"]), false)
	Control_SetEnabled(Button_GetControl(gHandles["btnRight"]), false)
	Control_SetVisible(Button_GetControl(gHandles["btnCenter"]), false)
	Control_SetEnabled(Button_GetControl(gHandles["btnCenter"]), false)
	Control_SetVisible(ListBox_GetControl(gHandles["lbxPublishItem"]), false)
	Control_SetEnabled(ListBox_GetControl(gHandles["lbxPublishItem"]), false)
end

function beginUploadToUGCServer(name, localPath, type)
	KEP_LogError( "trying to upload to UGC server: " .. localPath )
	local ImpSessionId = -1
	local ImpSession = nil
	ImpSessionId, ImpSession = UGC_CreateImportSession( UGC_TYPE.APP_EDIT )
	if ImpSession ~= nil then
		GetSet_SetNumber( ImpSession, AppEditingImportStateIds.STATE, UGCIS.NONE )
		local progressEventId = KEP_EventGetId( APP_EDIT_IMPORT_PROGRESS_EVENT )
		GetSet_SetNumber( ImpSession, AppEditingImportStateIds.EVENTID, progressEventId )
		GetSet_SetNumber( ImpSession, AppEditingImportStateIds.EVENTOBJID, ImpSessionId )
		GetSet_SetString( ImpSession, AppEditingImportStateIds.ASSETPATH, localPath )
		GetSet_SetNumber( ImpSession, AppEditingImportStateIds.TYPE, type )
		UGC_StartImportAsync( ImpSessionId )
	else
		LogError( "beginUploadToUGCServer: Create import session failed" )
		KEP_MessageBox( "Error importing script file file. Please restart Kaneva client and try again.", "Import failed" )
	end
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function UGCImportProgressHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local ImpSessionId = -1
	local ImpSession = nil
	local fullPath = ""

	if filter==UGCIS.DESTROYING or filter==UGCIS.DESTROYED then
		-- ImpSession might has been destroyed at this time, there is nothing we can do about this event.
		return
	end

	if objectid~=nil then
		ImpSessionId = objectid
		ImpSession = UGC_GetImportSession( ImpSessionId )
		if ImpSession ~= nil then
			fullPath = GetSet_GetString( ImpSession, AppEditingImportStateIds.ASSETPATH)
		end
	end

	if filter==UGCIS.PREVIEWED or filter==UGCIS.IMPORTED or filter==UGCIS.CONVERTED then
		-- tell state machine to continue import process
		UGC_ContinueImportAsync( ImpSessionId )
	elseif filter==UGCIS.PROCESSED then
		local internalError = false
		if ImpSession~=nil then
			-- apply imported texture to target object
			if fullPath==nil then
				LogError( "UGCImportProgressHandler: Error occurred after post-processing action item import, file path is null" )
				KEP_MessageBox( "Error importing script file.\n\n[code: -2]", "Import failed" )
				internalError = true
			end

			if not internalError then
				-- submit image to server
				if g_ugcType~=UGC_TYPE.INVALID then
					UGCI.SubmitUGCObj( UGC_TYPE.APP_EDIT, 0, 0, "", "", 0, 0 )
				end
			end
		end

		if internalError then
			--KEP_SendChatMessage( ChatType.System, "Import failed due to an internal error. You may close and reopen Kaneva Client and try it again." )
			local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeString(statusEvent, "Import failed due to an internal error. You may close and reopen Kaneva Client and try it again.")
			KEP_EventEncodeNumber(statusEvent, 3)
			KEP_EventEncodeNumber(statusEvent, 255)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventSetFilter(statusEvent, 4)
			KEP_EventQueue(statusEvent)
		end

		-- cleanup ImpSession
		UGC_CleanupImportSessionAsync( ImpSessionId )
	elseif filter==UGCIS.PREVIEWFAILED then
		LogError( "UGCImportProgressHandler: Preview failed: " .. fullPath )
		KEP_MessageBox( "Error importing script file.\n\n[code: 1]", "Import failed" )
		-- cleanup ImpSession
		UGC_CleanupImportSessionAsync( ImpSessionId )
	elseif filter==UGCIS.PROCESSINGFAILED then
		LogError( "UGCImportProgressHandler: Post-processing failed: " .. fullPath )
		KEP_MessageBox( "Error importing script file\n\n[code: 4]", "Import failed" )
		-- cleanup ImpSession
		UGC_CleanupImportSessionAsync( ImpSessionId )
	elseif filter==UGCIS.IMPORTFAILED then
		LogError( "UGCImportProgressHandler: Import failed: " .. fullPath )
		-- cleanup ImpSession
		UGC_CleanupImportSessionAsync( ImpSessionId )
	end
end

function UGCSubmissionFeedbackEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == UGC_TYPE.APP_EDIT then
		local sfe = UGCI.DecodeSubmissionFeedbackEvent( event, filter, objectid )
		if sfe.httpStatusCode==200 and sfe.result==SubmissionResult.SUCCEEDED then
			if sfe.uploadId ~= nil then
				Log( "UGCSubmissionFeedbackEventHandler: OK - uploadId=" .. sfe.uploadId )	-- no confirmation required, go ahead and upload
				UGCI.UploadUGCObj( 0, sfe.uploadId, sfe.ugcType, sfe.ugcObjId, sfe.objName )		-- 0: not a derivation
			end
		end
		return KEP.EPR_CONSUMED
	end
	return KEP.EPR_OK
end

function UploadCompletionHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local isDerivative = Event_DecodeNumber( event )
	local ugcType = Event_DecodeNumber( event )
	local uploadId = Event_DecodeString( event )
    local ugcObjId = Event_DecodeNumber( event )
    local ugcObjName = Event_DecodeString( event )

	if g_t_pending[1]["name"] == ugcObjName and g_t_pending[1]["type"] == ugcType then
		table.remove(g_t_pending, 1)
		local ext = tostring(extensions[type])
		ListBox_AddItem(gHandles["lbxPublishItem"], name..ext.." Successfully Published!", 0)
		if #g_t_pending > 0 and not g_cancel_publish then
			local ext = tostring(extensions[g_t_pending[1]["type"]])
			ListBox_AddItem(gHandles["lbxPublishItem"], "Publishing "..g_t_pending[1]["name"]..ext, 0)
			beginUploadToUGCServer(g_t_pending[1]["name"], g_t_pending[1]["path"], g_t_pending[1]["type"])
		end
	end

	-- check if done
	if #g_t_pending == 0 or g_cancel_publish then
		if g_cancel_publish then
			ListBox_AddItem(gHandles["lbxPublishItem"], "Publish Cancelled!", 0)
		else
			ListBox_AddItem(gHandles["lbxPublishItem"], "Publish Complete!", 0)
		end
		switchDisplay(STATE_FINISHED)
	end
end

function btnCenter_OnButtonClicked(buttonHandle)
	if g_display_state == STATE_PENDING then
		--Switch display to publishing
		switchDisplay(STATE_PUBLISHING)
		if #g_t_pending > 0 then
			local ext = tostring(extensions[g_t_pending[1]["type"]])
			ListBox_AddItem(gHandles["lbxPublishItem"], "Publishing "..g_t_pending[1]["name"]..ext, 0)
			-- TODO switch these when UGC publish ready
			KEP_UploadAssetTo3DApp( g_t_pending[1]["name"], g_t_pending[1]["path"], g_t_pending[1]["type"])
		end
	elseif g_display_state == STATE_PUBLISHING then
		g_cancel_publish = true
	elseif g_display_state == STATE_FINISHED then
		MenuCloseThis()
	end
end

-- TODO remove this when UGC publish complete
function PublishCompleteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local name = KEP_EventDecodeString(event)
	local type = tonumber(KEP_EventDecodeNumber(event))
	local success = tonumber(KEP_EventDecodeNumber(event))
	local errstr = KEP_EventDecodeString(event)
	if success == 0 then
		KEP_MessageBox( errstr )
		local ext = tostring(extensions[type])
		ListBox_AddItem(gHandles["lbxPublishItem"], name..ext.." Failed to Publish!", 0)
		switchDisplay(STATE_FINISHED)
	else
		if g_t_pending[1]["name"] == name and g_t_pending[1]["type"] == type then
			table.remove(g_t_pending, 1)
			local ext = tostring(extensions[type])
			ListBox_AddItem(gHandles["lbxPublishItem"], name..ext.." Successfully Published!", 0)
			if #g_t_pending > 0 and not g_cancel_publish then
				local ext = tostring(extensions[g_t_pending[1]["type"]])
				ListBox_AddItem(gHandles["lbxPublishItem"], "Publishing "..g_t_pending[1]["name"]..ext, 0)
				KEP_UploadAssetTo3DApp( g_t_pending[1]["name"], g_t_pending[1]["path"], g_t_pending[1]["type"])
			end
		end

		-- check if done
		if #g_t_pending == 0 or g_cancel_publish then
			if g_cancel_publish then
				ListBox_AddItem(gHandles["lbxPublishItem"], "Publish Cancelled!", 0)
			else
				ListBox_AddItem(gHandles["lbxPublishItem"], "Publish Complete!", 0)
			end
			switchDisplay(STATE_FINISHED)
		end
	end
end
