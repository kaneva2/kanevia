--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_NPCGifting.lua
--
-- Displays available interaction options with Actors
-------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\Lib_NpcDefinitions.lua")
dofile("Framework_ActorHelper.lua")
dofile("Framework_InventoryHelper.lua")

local NpcDefs = _G.NpcDefinitions -- Assign the global NpcDefinitions to a local variable for faster access

---------------
-- CONSTANTS --
---------------

-- Visuals

FONTS = {
	DEFAULT = "Verdana",
}

FONT_SIZES = {
	HEAD = 20,
	SUB = 16,
	TILL_NEXT = 16,
	GIFT_PTS = 16,
	GIFT_ICON = 14,
	GIFT_NAME = 16,
	GIFT_CNT = 14,
	BUTTON = 13,
}

local FONT_BUFFER = 4

COLORS = { -- frequently used colors
    WHITE = {a=255, r=255, g=255, b=255},
    BLACK = {a=255, r=0, g=0, b=0},
    AQUA = {a=255, r=0, g=246, b=255},
    GREY = {a=255, r=80, g=80, b=80},
    HEAD = {a=255, r=220, g=220, b=220},
    SUB = {a=255, r=220, g=220, b=220},
    TILL_NEXT = {a=255, r=255, g=192, b=0},
    GIFT_PTS = {a=255, r=255, g=255, b=255},
    GIFT_NAME = {a=255, r=220, g=220, b=220},
    GIFT_CNT_YES = {a=255, r=21, g=169, b=39},
    GIFT_CNT_NO = {a=255, r=239, g=72, b=72},
    BUTTON = {a=255, r=255, g=255, b=255},
}

-- Sizing constants

local BASE_MENU_SIZE = {X = 570, Y = 498}
local MENU_PAD = {TOP = 20, BOTTOM = 20, LEFT = 20, RIGHT = 20}

local HEAD_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT, Y = FONT_SIZES.HEAD + FONT_BUFFER}
local HEAD_PAD = {BOTTOM = 10}

local SUB_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT, Y = FONT_SIZES.SUB + FONT_BUFFER}
local SUB_PAD = {BOTTOM = 10}
local SUB_HEIGHT_MAX = 200
local SUB_HEIGHT_MIN = 20

local TILL_NEXT_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT, Y = FONT_SIZES.TILL_NEXT + FONT_BUFFER}
local TILL_NEXT_PAD = {BOTTOM = 10}
local TILL_NEXT_HEIGHT_MAX = 200
local TILL_NEXT_HEIGHT_MIN = 20

local GIFT_PTS_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT, Y = FONT_SIZES.GIFT_PTS + FONT_BUFFER}
local GIFT_PTS_PAD = {BOTTOM = 0}

local DIV_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT, Y = 1}
local DIV_PAD = {TOP = 0, BOTTOM = 5}

local BTN_GIFT_SIZE = {X = 80, Y = 20}
local BTN_GIFT_PAD = {BOTTOM = 4}

local BTN_CLOSE_SIZE = {X = 80, Y = 20}
local BTN_CLOSE_PAD = {BOTTOM = 0}

local GIFT_ICON_SIZE = {X = 50, Y = 50}
-- local GIFT_ICON_SIZE_BIG = {X = 70, Y = 70}
local GIFT_ICON_PAD = {BOTTOM = 20, RIGHT = 10}

local GIFT_NAME_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT - BTN_GIFT_SIZE.X - GIFT_ICON_SIZE.X - GIFT_ICON_PAD.RIGHT * 2, Y = FONT_SIZES.GIFT_NAME + FONT_BUFFER}
local GIFT_NAME_PAD = {BOTTOM = 1}

local GIFT_CNT_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT - BTN_GIFT_SIZE.X - GIFT_ICON_SIZE.X - GIFT_ICON_PAD.RIGHT * 2, Y = FONT_SIZES.GIFT_CNT + FONT_BUFFER}
local GIFT_CNT_PAD = {BOTTOM = 1}

local GIFT_TYPES = {
	"Basic",
	"Standard",
	"Special",
	-- "Follower",
	-- "Spouse",
}

local GIFT_PTS = {
	[GIFT_TYPES[1]] = 100,
	[GIFT_TYPES[2]] = 500,
	[GIFT_TYPES[3]] = 1000,
}

local MAX_GIFTS = 3

-- Menu modes
local MENU_MODE = {DEFAULT = "Default", PRE_FOLLOWER = NpcDefs.REP_LEVELS[NpcDefs.MAX_REP_LEVEL], FOLLOWER = NpcDefs.SPECIAL_LEVELS[1], SPOUSE = NpcDefs.SPECIAL_LEVELS[2]}

---------------------
-- LOCAL VARIABLES --
---------------------

local m_currPlayer = ""

local m_initMenuElements = false

local m_initNpcProperties = false
local m_initNpcCacheProperties = false
local m_initGameItemCache = false
local m_initPlayerInventory = false

local m_initGiftReady = false
local m_initFollowerCountReady = false
local m_initSpouseExistsReady = false

-- Sizing
local m_menuSize = {x = BASE_MENU_SIZE.X, y = BASE_MENU_SIZE.Y}

-- Game item cache
local m_gameItems = {}
local m_playerInventoryByUNID = {}
local m_playerInventory = {}
local m_inventoryCounts = {}

-- Mode-based
local m_currMode = nil
local m_currNpcName = ""
local m_currActorPID = nil
local m_currActorUNID = nil
local m_currActorBehavior = "actor"
local m_isSpouse = false
local m_isFollower = false
local m_isJango = false
local m_isRider = false

local m_spouseExists = false
local m_followerCount = 0
local m_giftReady = false
local m_giftWaitTime = 0

-- Reputation and gifting
local m_repScore = 0
local m_repScale = "Normal"
local m_gifts = {0, 0, 0}
local m_giftText = {[1] = "", [2] = "", [3] = ""}
local m_giftFollower = 0
local m_giftSpouse = 0
local m_pendingGiftIndex = -1

--------------------
-- CORE FUNCTIONS --
--------------------

function onCreate()
	m_currPlayer = KEP_GetLoginName()

	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	
	-- Register server event handlers
	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)
	Events.registerHandler("FRAMEWORK_INIT_NPC_GIFTING", initNpcGiftingHandler)
	Events.registerHandler("FRAMEWORK_RETURN_GIFT_READY", returnGiftReadyHandler)
	Events.registerHandler("FRAMEWORK_RETURN_FOLLOWER_COUNT", returnFollowerCountHandler)
	Events.registerHandler("FRAMEWORK_RETURN_NPC_SPOUSE", returnNpcSpouseHandler)
	-- Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)

	requestInventory(GAME_PID)
	requestInventory(INVENTORY_PID)
	Events.sendEvent("FRAMEWORK_REQ_FOLLOWER_COUNT", {}) -- Check the follower limit for confirming followers
	Events.sendEvent("FRAMEWORK_REQ_NPC_SPOUSE", {}) -- Check if already married for confirming marriage
end

function onDestroy()
	if m_isRider then
		Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = m_currActorPID, interacting = false})
	end
end

-- Called every frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_initNpcProperties and m_initGameItemCache and not m_initNpcCacheProperties then
		initNpcCacheProperties()
	elseif m_initNpcCacheProperties and m_initPlayerInventory and m_initGiftReady and m_initFollowerCountReady and m_initSpouseExistsReady and not m_initMenuElements then
		generateMenuElements()
	end
end

function Dialog_OnMoved()
	if m_initMenuElements then
	    recenterEverything() 
    end
end

-- Called when a key is pressed
function Dialog_OnKeyDown(dialogHandle, key, shift, ctrl, alt)
	-- 'X' pressed, reset disabled buttons (DEBUG USE ONLY)
	-- if (key == string.byte("X")) then
	-- 	local btnCtrl = Dialog_GetButton(gDialogHandle, "btnGift1")
	-- 	if btnCtrl then
	-- 		Control_SetEnabled(btnCtrl, true)
	-- 	end
	-- 	btnCtrl = Dialog_GetButton(gDialogHandle, "btnGift2")
	-- 	if btnCtrl then
	-- 		Control_SetEnabled(btnCtrl, true)
	-- 	end
	-- 	btnCtrl = Dialog_GetButton(gDialogHandle, "btnGift3")
	-- 	if btnCtrl then
	-- 		Control_SetEnabled(btnCtrl, true)
	-- 	end
	-- 	btnCtrl = Dialog_GetButton(gDialogHandle, "btnGift4")
	-- 	if btnCtrl then
	-- 		Control_SetEnabled(btnCtrl, true)
	-- 	end
	-- end
end

-------------------
-- GUI FUNCTIONS --
-------------------

function generateMenuElements()
	local insertPoint_y = MENU_PAD.TOP -- Tracks the Y-value insert point for new menu elements

	-- Add the background
	Control_SetVisible(gHandles["imgMenuBG"], true)
	Control_SetSize(gHandles["imgMenuBG"], m_menuSize.x, m_menuSize.y)
	Control_SetLocationX(gHandles["imgMenuBG"], 0)
    Control_SetLocationY(gHandles["imgMenuBG"], 0)
    Control_SetVisible(gHandles["imgMenuBG"], true)
	
	-- Add the header (npc/pet/player name)
	local headStr = getRepLabel(m_repScore, m_repScale)
	if headStr == "Acquaintance" then
		headStr = "an Acquaintance"
	else
		headStr = "a "..headStr
	end
	headStr = m_currNpcName.." is " .. headStr
	if m_isFollower then
		headStr = m_currNpcName.." is a Follower"
	elseif m_isSpouse then
		headStr = m_currNpcName.." is your Spouse"
	end
	addLabel("stcHead", headStr, rect(MENU_PAD.LEFT, insertPoint_y, HEAD_SIZE.X, HEAD_SIZE.Y), FONTS.DEFAULT, FONT_SIZES.HEAD, COLORS.HEAD, true)
	insertPoint_y = insertPoint_y + HEAD_SIZE.Y + HEAD_PAD.BOTTOM
	
	local subText = m_giftText[1] or " "
	if m_isSpouse then
		subText = m_giftText[3] or " "
	elseif m_isFollower then
		subText = m_giftText[2] or " "
	end

	local tempCtrl, tempHeight, tempHeightMod
	if subText and subText ~= "" then
		addLabel("stcSub", subText, rect(MENU_PAD.LEFT, insertPoint_y, SUB_SIZE.X, SUB_SIZE.Y), FONTS.DEFAULT, FONT_SIZES.SUB, COLORS.SUB, false)
		-- Resize subText to match text length
		tempCtrl = Dialog_GetControl(gDialogHandle, "stcSub")
		tempHeight = math.max((Static_GetStringWidth(tempCtrl, subText) / SUB_SIZE.Y), SUB_HEIGHT_MIN)
		-- Ensure height grows by unit sizes
		tempHeightMod = tempHeight % SUB_SIZE.Y
		if tempHeightMod ~= 0 then
			tempHeight = tempHeight + (SUB_SIZE.Y - tempHeightMod)
		end
		-- Ensure minimum height
		tempHeight = math.min(tempHeight, SUB_HEIGHT_MAX)
		-- Resize control
		Control_SetSize(tempCtrl, SUB_SIZE.X, tempHeight)

		insertPoint_y = insertPoint_y + tempHeight + SUB_PAD.BOTTOM
	end
	
	local tillNextStr = " "
	if m_currMode == MENU_MODE.DEFAULT then
		tillNextStr = "Give "..formatCommaValue(tostring(getNextRepDeficit())).." more points to earn the "..getNextRepLabel().." title!"
	elseif m_currMode == MENU_MODE.PRE_FOLLOWER then
		local requiredItem = m_gameItems[tostring(m_giftFollower)]
		if requiredItem then
			local tempName = requiredItem.name or ""
			if m_isJango then
				tillNextStr = m_currNpcName.." is already your Follower."
			else
				tillNextStr = "Give "..m_currNpcName.." a "..tempName.." to make them your Follower."
			end
		end
	elseif m_currMode == MENU_MODE.FOLLOWER then
		local requiredItem = m_gameItems[tostring(m_giftSpouse)]
		if requiredItem then
			local tempName = requiredItem.name
			tillNextStr = "Give "..m_currNpcName.." a "..tempName.." to make them your Spouse."
		end
	elseif m_currMode == MENU_MODE.SPOUSE then
		-- Nothing yet
	end
	
	addLabel("stcTillNext", tillNextStr, rect(MENU_PAD.LEFT, insertPoint_y, TILL_NEXT_SIZE.X, TILL_NEXT_SIZE.Y), FONTS.DEFAULT, FONT_SIZES.TILL_NEXT, COLORS.TILL_NEXT, false)
	
	-- Resize stcTillNext to match text length
	tempCtrl = Dialog_GetControl(gDialogHandle, "stcTillNext")
	tempHeight = math.max((Static_GetStringWidth(tempCtrl, tillNextStr) / TILL_NEXT_SIZE.Y), TILL_NEXT_HEIGHT_MIN)
	-- Ensure height grows by unit sizes
	tempHeightMod = tempHeight % TILL_NEXT_SIZE.Y
	-- tempHeight = tempHeight - tempHeightMod
	if tempHeightMod ~= 0 then
		tempHeight = tempHeight + (TILL_NEXT_SIZE.Y - tempHeightMod)
	end
	-- Ensure minimum height
	tempHeight = math.min(tempHeight, TILL_NEXT_HEIGHT_MAX)
	-- Resize control
	Control_SetSize(tempCtrl, TILL_NEXT_SIZE.X, tempHeight)

	insertPoint_y = insertPoint_y + tempHeight + TILL_NEXT_PAD.BOTTOM

	-- GIFTING UI: Basic gifts
	if m_currMode == MENU_MODE.DEFAULT or m_currMode == MENU_MODE.FOLLOWER or m_currMode == MENU_MODE.SPOUSE then
		for i=1,MAX_GIFTS do
			-- Check that each gift exists
			if m_gifts[i] and m_gifts[i] ~= 0 then
				addLabel("stcGiftPts"..tostring(i), "+"..formatCommaValue(tostring(GIFT_PTS[GIFT_TYPES[i]])).." Points", rect(MENU_PAD.LEFT, insertPoint_y, GIFT_PTS_SIZE.X, GIFT_PTS_SIZE.Y), FONTS.DEFAULT, FONT_SIZES.GIFT_PTS, COLORS.GIFT_PTS, true)
				insertPoint_y = insertPoint_y + GIFT_PTS_SIZE.Y

				-- Add a divider line
				addImage("imgDiv"..tostring(i), rect(MENU_PAD.LEFT, DIV_PAD.TOP + insertPoint_y, DIV_SIZE.X, DIV_SIZE.Y), COLORS.GREY, "white.tga", rect(0, 0, -1, -1))
				insertPoint_y = insertPoint_y + DIV_PAD.TOP + DIV_SIZE.Y + DIV_PAD.BOTTOM

				addImage("imgGiftIcon"..tostring(i).."BG", rect(MENU_PAD.LEFT, insertPoint_y, GIFT_ICON_SIZE.X, GIFT_ICON_SIZE.Y), COLORS.GREY, "white.tga", rect(0, 0, -1, -1))
				addImage("imgGiftIcon"..tostring(i), rect(MENU_PAD.LEFT + 1, insertPoint_y + 1, GIFT_ICON_SIZE.X - 2, GIFT_ICON_SIZE.Y - 2), COLORS.WHITE, "white.tga", rect(0, 0, -1, -1))
				
				local requiredItem = m_gameItems[tostring(m_gifts[i])]
				if requiredItem then
					local tempGLID = requiredItem.GLID
					local tempName = requiredItem.name
					
					-- Set item image
					local imgHandle = Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgGiftIcon"..tostring(i)))
					if imgHandle and gDialogHandle then
						KEP_LoadIconTextureByID(tonumber(tempGLID), imgHandle, gDialogHandle)
					end

					addLabel("stcGiftName"..tostring(i), "1 "..tempName, rect(MENU_PAD.LEFT + GIFT_ICON_SIZE.X + GIFT_ICON_PAD.RIGHT, insertPoint_y, GIFT_NAME_SIZE.X, GIFT_NAME_SIZE.Y), FONTS.DEFAULT, FONT_SIZES.GIFT_NAME, COLORS.GIFT_NAME, false)
					
					local backpackCount = m_inventoryCounts[m_gifts[i]] or 0
					local giftCntColor = COLORS.GIFT_CNT_NO
					if backpackCount > 0 then
						giftCntColor = COLORS.GIFT_CNT_YES
					end
					local giftCntStr = "You have "..formatCommaValue(tostring(backpackCount))
					if not m_giftReady then
						giftCntStr = "You must wait "..getTimeString(m_giftWaitTime).." before giving this gift."
						giftCntColor = COLORS.GIFT_CNT_NO
					end
					addLabel("stcGiftCnt"..tostring(i), giftCntStr, rect(MENU_PAD.LEFT + GIFT_ICON_SIZE.X + GIFT_ICON_PAD.RIGHT, GIFT_NAME_SIZE.Y + GIFT_NAME_PAD.BOTTOM + insertPoint_y, GIFT_CNT_SIZE.X, GIFT_CNT_SIZE.Y), FONTS.DEFAULT, FONT_SIZES.GIFT_CNT, giftCntColor, false)
					
					addButton("btnGift"..tostring(i), "Give Gift", rect(BASE_MENU_SIZE.X - MENU_PAD.RIGHT - BTN_GIFT_SIZE.X, GIFT_ICON_SIZE.Y/2 - BTN_GIFT_SIZE.Y/2 + insertPoint_y, BTN_GIFT_SIZE.X, BTN_GIFT_SIZE.Y), "button_18x150.tga", rect(1, 1, 151, 19), FONTS.DEFAULT, FONT_SIZES.BUTTON, COLORS.BUTTON)
					local btnCtrl = Dialog_GetButton(gDialogHandle, "btnGift"..tostring(i))
					Control_SetEnabled(btnCtrl, backpackCount > 0 and m_giftReady)
					_G["btnGift"..tostring(i).."_OnButtonClicked"] = function(btnHandle)
						btnGiftHandler(MENU_MODE.DEFAULT, i)
					end

					insertPoint_y = insertPoint_y + GIFT_ICON_SIZE.Y + GIFT_ICON_PAD.BOTTOM
				end
			end
		end
	end
	-- GIFTING UI: Special gifts
	if (m_currMode == MENU_MODE.PRE_FOLLOWER or m_currMode == MENU_MODE.FOLLOWER) and not m_isJango then
		local requiredItem = nil
		if m_currMode == MENU_MODE.PRE_FOLLOWER then
			requiredItem = m_gameItems[tostring(m_giftFollower)]
		elseif m_currMode == MENU_MODE.FOLLOWER then
			requiredItem = m_gameItems[tostring(m_giftSpouse)]
		end
		if requiredItem then
			local tempGLID = requiredItem.GLID
			local tempName = requiredItem.name

			-- Spouse or Follower?
			local giftLabelStr = ""
			if m_currMode == MENU_MODE.PRE_FOLLOWER then
				giftLabelStr = "Follower:"
			elseif m_currMode == MENU_MODE.FOLLOWER then
				giftLabelStr = "Spouse:"
			end
			addLabel("stcGiftPts"..tostring(MAX_GIFTS+1), giftLabelStr, rect(MENU_PAD.LEFT, insertPoint_y, GIFT_PTS_SIZE.X, GIFT_PTS_SIZE.Y), FONTS.DEFAULT, FONT_SIZES.GIFT_PTS, COLORS.GIFT_PTS, true)
			insertPoint_y = insertPoint_y + GIFT_PTS_SIZE.Y

			-- Add a divider line
			addImage("imgDiv"..tostring(i), rect(MENU_PAD.LEFT, DIV_PAD.TOP + insertPoint_y, DIV_SIZE.X, DIV_SIZE.Y), COLORS.GREY, "white.tga", rect(0, 0, -1, -1))
			insertPoint_y = insertPoint_y + DIV_PAD.TOP + DIV_SIZE.Y + DIV_PAD.BOTTOM

			addImage("imgGiftIcon"..tostring(i).."BG", rect(MENU_PAD.LEFT, insertPoint_y, GIFT_ICON_SIZE.X, GIFT_ICON_SIZE.Y), COLORS.GREY, "white.tga", rect(0, 0, -1, -1))
			addImage("imgGiftIcon"..tostring(i), rect(MENU_PAD.LEFT + 1, insertPoint_y + 1, GIFT_ICON_SIZE.X - 2, GIFT_ICON_SIZE.Y - 2), COLORS.WHITE, "white.tga", rect(0, 0, -1, -1))
			
			-- Set item image
			local imgHandle = Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgGiftIcon"..tostring(i)))
			if imgHandle and gDialogHandle then
				KEP_LoadIconTextureByID(tonumber(tempGLID), imgHandle, gDialogHandle)
			end

			addLabel("stcGiftName"..tostring(MAX_GIFTS+1), "1 "..tempName, rect(MENU_PAD.LEFT + GIFT_ICON_SIZE.X + GIFT_ICON_PAD.RIGHT, insertPoint_y, GIFT_NAME_SIZE.X, GIFT_NAME_SIZE.Y), FONTS.DEFAULT, FONT_SIZES.GIFT_NAME, COLORS.GIFT_NAME, false)
			local backpackCount = 0
			if m_currMode == MENU_MODE.PRE_FOLLOWER then
				backpackCount = m_playerInventoryByUNID[tostring(m_giftFollower)] or 0
			elseif m_currMode == MENU_MODE.FOLLOWER then
				backpackCount = m_playerInventoryByUNID[tostring(m_giftSpouse)] or 0
			end
			local giftCntColor = COLORS.GIFT_CNT_NO
			if backpackCount > 0 then
				giftCntColor = COLORS.GIFT_CNT_YES
			end

			local giftCntStr = "You have "..formatCommaValue(tostring(backpackCount))
			if m_currMode == MENU_MODE.PRE_FOLLOWER and m_followerCount >= NpcDefs.MAX_FOLLOWERS then
				giftCntStr = "You have too many followers."
				giftCntColor = COLORS.GIFT_CNT_NO
			elseif m_currMode == MENU_MODE.FOLLOWER and m_spouseExists then
				giftCntStr = "You already have a spouse."
				giftCntColor = COLORS.GIFT_CNT_NO
			end
			addLabel("stcGiftCnt"..tostring(MAX_GIFTS+1), giftCntStr, rect(MENU_PAD.LEFT + GIFT_ICON_SIZE.X + GIFT_ICON_PAD.RIGHT, GIFT_NAME_SIZE.Y + GIFT_NAME_PAD.BOTTOM + insertPoint_y, GIFT_CNT_SIZE.X, GIFT_CNT_SIZE.Y), FONTS.DEFAULT, FONT_SIZES.GIFT_CNT, giftCntColor, false)
			
			addButton("btnGift"..tostring(MAX_GIFTS+1), "Give Gift", rect(BASE_MENU_SIZE.X - MENU_PAD.RIGHT - BTN_GIFT_SIZE.X, GIFT_ICON_SIZE.Y/2 - BTN_GIFT_SIZE.Y/2 + insertPoint_y, BTN_GIFT_SIZE.X, BTN_GIFT_SIZE.Y), "button_18x150.tga", rect(1, 1, 151, 19), FONTS.DEFAULT, FONT_SIZES.BUTTON, COLORS.BUTTON)
			local btnCtrl = Dialog_GetButton(gDialogHandle, "btnGift"..tostring(MAX_GIFTS+1))
			if m_currMode == MENU_MODE.PRE_FOLLOWER then
				Control_SetEnabled(btnCtrl, backpackCount > 0 and m_followerCount < NpcDefs.MAX_FOLLOWERS)
			elseif m_currMode == MENU_MODE.FOLLOWER then
				Control_SetEnabled(btnCtrl, backpackCount > 0 and not m_spouseExists)
			end
			_G["btnGift"..tostring(MAX_GIFTS+1).."_OnButtonClicked"] = function(btnHandle)
				btnGiftHandler(m_currMode, -1)
			end

			insertPoint_y = insertPoint_y + GIFT_ICON_SIZE.Y + GIFT_ICON_PAD.BOTTOM
		end
	end

	addButton("btnClose", "Goodbye", rect(BASE_MENU_SIZE.X/2 - BTN_CLOSE_SIZE.X/2, insertPoint_y, BTN_CLOSE_SIZE.X, BTN_CLOSE_SIZE.Y), "button_18x150.tga", rect(1, 1, 151, 19), FONTS.DEFAULT, FONT_SIZES.BUTTON, COLORS.BUTTON)
	insertPoint_y = insertPoint_y  + BTN_CLOSE_SIZE.Y

	m_menuSize.y = insertPoint_y + MENU_PAD.BOTTOM -- Update menu size based on generated elements
	m_initMenuElements = true
	recenterEverything() -- Resize and reposition the menu
end

function recenterEverything()
    Dialog_SetSize(gDialogHandle, m_menuSize.x, m_menuSize.y)
   	MenuCenterThis()
    Control_SetSize(gHandles["imgMenuBG"], m_menuSize.x, m_menuSize.y)
end

----------------
-- USER INPUT --
----------------

function btnGiftHandler(giftMode, index)
	if giftMode == MENU_MODE.DEFAULT then
		m_pendingGiftIndex = index
		confirmGift()
	elseif giftMode == MENU_MODE.PRE_FOLLOWER then
		confirmFollower()
	elseif giftMode == MENU_MODE.FOLLOWER then
		confirmMarriage()
	end
end

function confirmFollower()
	requiredItem = m_gameItems[tostring(m_giftFollower)]
	if requiredItem then
		local removeTable = {}
		local addTable = {}
		local newItem = {UNID = m_giftFollower, count = 1, properties = requiredItem}
		table.insert(removeTable, newItem)

		processTransaction(removeTable, addTable)

		-- particle, etc.
		
		spawnFollower()

		Events.sendEvent("FRAMEWORK_UPDATE_FOLLOWER_COUNT", {changeAmt = 1, PID = m_currActorPID})

		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, m_currNpcName.." is now your follower")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)

		if m_followerCount + 1 >= NpcDefs.MAX_FOLLOWERS then
			local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeString(statusEvent, "You have reached your limit of Followers")
			KEP_EventEncodeNumber(statusEvent, 3)
			KEP_EventEncodeNumber(statusEvent, 255)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventSetFilter(statusEvent, 4)
			KEP_EventQueue(statusEvent)
		end

		MenuCloseThis()
	end
end

function confirmMarriage()
	requiredItem = m_gameItems[tostring(m_giftSpouse)]
	if requiredItem then
		local removeTable = {}
		local addTable = {}
		local newItem = {UNID = m_giftSpouse, count = 1, properties = requiredItem}
		table.insert(removeTable, newItem)
		processTransaction(removeTable, addTable)

		-- particle, etc.

		m_isSpouse = true

		Events.sendEvent("FRAMEWORK_UPDATE_NPC_SPOUSE", {PID = m_currActorPID})

		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, m_currNpcName.." is now your spouse")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
		
		MenuCloseThis()
	end
end

function confirmGift()
	local index = m_pendingGiftIndex
	m_pendingGiftIndex = -1
	local requiredItem = m_gameItems[tostring(m_gifts[index])]	
	if requiredItem then
		local removeTable = {}
		local addTable = {}
		local newItem = {UNID = m_gifts[index], count = 1, properties = requiredItem}
		table.insert(removeTable, newItem)

		checkForStatusShift(GIFT_PTS[GIFT_TYPES[index]])

		m_repScore = m_repScore + GIFT_PTS[GIFT_TYPES[index]]

		processTransaction(removeTable, addTable)

		Events.sendEvent("UPDATE_NPC_REP", {PID = m_currActorPID, repChange = GIFT_PTS[GIFT_TYPES[index]]})

		-- if not ToBool(KEP_IsKeyDown(Keyboard.SHIFT)) then -- TO DO: Cheat for speeding through gifting without closing menu (does not update properly when able to give follower gift)
		MenuCloseThis()
		-- end
	end
end

function spawnFollower()
	local x, y, z, r = KEP_GetPlayerPosition()
	local radian = (r * math.pi) / 180
	local rotX = math.sin(radian)
	local rotZ = math.cos(radian)
	local placeX = x - (10 * rotX)
	local placeZ = z - (10 * rotZ)
	local tempBehavior = nil
	if string.sub(m_currActorBehavior, -6, -1) == "_rider" then
		tempBehavior = string.gsub(m_currActorBehavior, "_rider", "_pet")
	else
		tempBehavior = m_currActorBehavior.."_pet"
	end
	
	-- GENERATE_NPC_FOLLOWER vs FRAMEWORK_PLAYER_PLACE_ITEM
	Events.sendEvent("FRAMEWORK_PLAYER_PLACE_ITEM", {	GLID = m_gameItems[tostring(m_currActorUNID)].GLID or 0, -- TO DO: Set up default GLID
														UNID = m_currActorUNID,
														behavior = tempBehavior,
														jango = m_currActorPID,
														x = placeX, y = y, z = placeZ, rx = rotX, ry = 0, rz = rotZ})
end

--------------------
-- EVENT HANDLERS --
--------------------

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems

	m_initGameItemCache = true
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)

	if m_playerInventory[updateIndex] then
		m_playerInventory[updateIndex] = updateItem
		m_playerInventoryByUNID[updateItem.UNID] = updateItem.count
		updateInventory()
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_playerInventory = decompileInventory(tEvent)

	for i=1,#m_playerInventory do
		m_playerInventoryByUNID[tostring(m_playerInventory[i].UNID)] = m_playerInventory[i].count
	end

	updateInventory()

	m_initPlayerInventory = true
end

function updateInventory()
	m_inventoryCounts = {}
	-- Compile UNID counts
	for i=1, #m_playerInventory do
		if m_playerInventory[i].UNID ~= 0 then
			-- New UNID entry
			if m_inventoryCounts[m_playerInventory[i].UNID] == nil then
				m_inventoryCounts[m_playerInventory[i].UNID] = m_playerInventory[i].count
			-- Duplicate entry. Increment UNID count
			else
				m_inventoryCounts[m_playerInventory[i].UNID] = m_inventoryCounts[m_playerInventory[i].UNID] + m_playerInventory[i].count
			end
		end
	end

	-- Update overlays
	-- setPurchasableOverlays()
end

function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
end

function initNpcGiftingHandler(event)
	m_isRider = event.isRider or false
	if m_isRider then
		Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = event.actorPID, interacting = true})
	end
	if event.actorPID == nil then return end
	if event.actorUNID == nil then return end
	if event.actorBehavior == nil then return end
	if event.repScore == nil then return end
	if event.isFollower == nil then return end
	if event.isSpouse == nil then return end
	if event.isJango == nil then return end
	
	m_currActorPID = event.actorPID
	m_currActorUNID = event.actorUNID
	m_currActorBehavior = event.actorBehavior
	m_repScore = event.repScore
	m_isFollower = event.isFollower
	m_isSpouse = event.isSpouse
	m_isJango = event.isJango

	Events.sendEvent("FRAMEWORK_REQ_GIFT_READY", {PID = m_currActorPID}) -- Check if ready to give another gift
			
	m_initNpcProperties = true
end

function returnGiftReadyHandler(event)
	if event.giftReady and event.giftReady == true then
		m_giftReady = true
	else
		m_giftWaitTime = event.timeRemaining
		m_giftReady = false
	end
	m_initGiftReady = true
end

function returnFollowerCountHandler(event)
	if event.followerCount ~= nil then
		m_followerCount = event.followerCount
	end
	m_initFollowerCountReady = true
end

function returnNpcSpouseHandler(event)
	if event.npcSpouse == nil or event.npcSpouse == -1 then
		m_spouseExists = false
	else
		m_spouseExists = true
	end
	m_initSpouseExistsReady = true
end

function initNpcCacheProperties()
	local actorItem = m_gameItems[tostring(m_currActorUNID)]
	if actorItem then
		m_currNpcName = actorItem.behaviorParams.actorName
		m_repScale = actorItem.behaviorParams.repScale
		m_gifts[1] = actorItem.behaviorParams.giftBasic
		m_gifts[2] = actorItem.behaviorParams.giftStandard
		m_gifts[3] = actorItem.behaviorParams.giftSpecial
		m_giftFollower = actorItem.behaviorParams.giftFollower
		m_giftSpouse = actorItem.behaviorParams.giftSpouse
		m_giftText[1] = actorItem.behaviorParams.msgGift or "Show me the gifts!" -- TO DO: Set up better default gifting text
		m_giftText[2] = actorItem.behaviorParams.msgFollow or "I will follow you to the end of the world."
		m_giftText[3] = actorItem.behaviorParams.msgSpouse or "Kiss me baby, one more time."
	end

	updateMenuMode()

	m_initNpcCacheProperties = true
end

function updateMenuMode()
	if m_isSpouse then
		m_currMode = MENU_MODE.SPOUSE
	elseif m_isFollower then
		m_currMode = MENU_MODE.FOLLOWER
	elseif m_repScore and m_repScale and getRepLabel(m_repScore, m_repScale) == MENU_MODE.PRE_FOLLOWER then
		m_currMode = MENU_MODE.PRE_FOLLOWER
	else
		m_currMode = MENU_MODE.DEFAULT
	end
end

function getRepLabel(iRep, sScale)
	if sScale == "Easy" or sScale == "Normal" or sScale == "Hard" then
		if iRep < NpcDefs.REP_SCALES[sScale][NpcDefs.REP_LEVELS[1]] then
			return NpcDefs.REP_LEVELS[0]
		elseif iRep < NpcDefs.REP_SCALES[sScale][NpcDefs.REP_LEVELS[2]] then
			return NpcDefs.REP_LEVELS[1]
		elseif iRep < NpcDefs.REP_SCALES[sScale][NpcDefs.REP_LEVELS[3]] then
			return NpcDefs.REP_LEVELS[2]
		elseif iRep < NpcDefs.REP_SCALES[sScale][NpcDefs.REP_LEVELS[4]] then
			return NpcDefs.REP_LEVELS[3]
		elseif iRep < NpcDefs.REP_SCALES[sScale][NpcDefs.REP_LEVELS[5]] then
			return NpcDefs.REP_LEVELS[4]
		elseif iRep >= NpcDefs.REP_SCALES[sScale][NpcDefs.REP_LEVELS[5]] then
			return NpcDefs.REP_LEVELS[5]
		else
			return ""
		end
	else
		return ""
	end
	-- To DO: Add special cases for follower, spouse
end

function getNextRepLabel()
	local nextRep = ""
	local currRep = getRepLabel(m_repScore, m_repScale)
	-- local i = 1
	for i=1,(NpcDefs.MAX_REP_LEVEL - 1) do
		if currRep == NpcDefs.REP_LEVELS[i] then
			nextRep = NpcDefs.REP_LEVELS[i+1]
		end
	end
	-- if currRep == NpcDefs.REP_LEVELS[NpcDefs.MAX_REP_LEVEL] then
	-- 	-- Max level
	-- 	nextRep = NpcDefs.REP_LEVELS_SPEC[1]
	-- end
	-- for i=1, i<(MAX_NpcDefs.SPECIAL_LEVELS - 1) then
	-- 	if currRep == NpcDefs.SPECIAL_LEVELS[i] then
	-- 		nextRep = NpcDefs.SPECIAL_LEVELS[i+1]
	-- 	end
	-- end
	-- if currRep == NpcDefs.SPECIAL_LEVELS[MAX_NpcDefs.SPECIAL_LEVELS] then
	-- 	-- Nothing after the last special level
	-- end
	return nextRep
end

function getNextRepDeficit()
	local currRep = getRepLabel(m_repScore, m_repScale)
	if currRep ~= NpcDefs.REP_LEVELS[NpcDefs.MAX_REP_LEVEL] then -- Check if already at max level
		local currScore = m_repScore
		local tarScore = NpcDefs.REP_SCALES[m_repScale][getNextRepLabel()]
		return tarScore - currScore
	else
		return 0
	end
end

function checkForStatusShift(pointGain)
	-- Update menu mode if you just gained enough rep to be the highest level before Follower
	local oldStatus = getRepLabel(m_repScore, m_repScale)
	local newStatus = getRepLabel(m_repScore + pointGain, m_repScale)
	if oldStatus ~= newStatus then
			if newStatus == "Acquaintance" then
				newStatus = "an Acquaintance"
			else
				newStatus = "a "..newStatus
			end
			local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeString(statusEvent, m_currNpcName.." is now "..newStatus)
			KEP_EventEncodeNumber(statusEvent, 3)
			KEP_EventEncodeNumber(statusEvent, 255)
			KEP_EventEncodeNumber(statusEvent, 255)
			KEP_EventEncodeNumber(statusEvent, 255)
			KEP_EventSetFilter(statusEvent, 4)
			KEP_EventQueue(statusEvent)
	end
end

---------------------
-- LOCAL FUNCTIONS --
---------------------

rect = function(x, y, w, h)
    return { x = x, y = y, w = w, h = h }
end

color = function(a, r, g, b)
    return { a = a, r = r, g = g, b = b }
end

addImage = function(name, rect, color, texName, texRect)
    local img = Dialog_GetImage(gDialogHandle, name)
    if img == nil then
        img = Dialog_AddImage(gDialogHandle, name, rect.x, rect.y, texName, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
        Image_SetColor(img, color)
        Control_SetSize(Image_GetControl(img), rect.w, rect.h)
    end
    return img
end

addButton = function(name, text, rect, texName, texRect, fontFace, fontSize, fontColor)
    local btn = Dialog_GetButton(gDialogHandle, name)
    if btn == nil then
        btn = Dialog_AddButton(gDialogHandle, name, text, rect.x, rect.y, rect.w, rect.h)
    end

    local elem, bc
    -- Disabled
    elem = Button_GetDisabledDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    bc = Element_GetFontColor(elem)
    Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
    BlendColor_SetColor(bc, 0, fontColor)
    -- Focused
    elem = Button_GetFocusedDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    bc = Element_GetFontColor(elem)
    Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
    BlendColor_SetColor(bc, 0, fontColor)
    -- MouseOver
    elem = Button_GetMouseOverDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    bc = Element_GetFontColor(elem)
    Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
    BlendColor_SetColor(bc, 0, fontColor)
    -- Normal
    elem = Button_GetNormalDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    bc = Element_GetFontColor(elem)
    Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
    BlendColor_SetColor(bc, 0, fontColor)
    -- Pressed
    elem = Button_GetPressedDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    bc = Element_GetFontColor(elem)
    Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
    BlendColor_SetColor(bc, 0, fontColor)

    return btn
end

addLabel = function(name, text, rect, fontFace, fontSize, fontColor, bold)
    local stc = Dialog_GetStatic(gDialogHandle, name)
    if stc == nil then
        local stcFontSize = fontSize + 2
        local stcW = stcFontSize*#text*0.8
        stc = Dialog_AddStatic(gDialogHandle, name, text, rect.x, rect.y, stcW, rect.h)
        local elem = Static_GetDisplayElement(stc)
        local bc = Element_GetFontColor(elem)
        local weight = 400
        if bold then
        	weight = 800
        end
        Element_AddFont(elem, gDialogHandle, fontFace, stcFontSize, weight, false)
        Element_SetTextFormat(elem, TEXT_HALIGN_LEFT + TEXT_VALIGN_TOP)
        BlendColor_SetColor(bc, 0, fontColor)
    end
end

-- Copy and paste controls
copyControl = function(menuControl)
    if menuControl then
        Dialog_CopyControl( gDialogHandle, menuControl )
        Dialog_PasteControl( gDialogHandle )

        local numControls = Dialog_GetNumControls( gDialogHandle )

        local newControl = Dialog_GetControlByIndex( gDialogHandle, numControls - 1 )
        local controlName = Control_GetName( newControl )

        gHandles[controlName] = newControl  

        return newControl
    end
end

formatCommaValue = function(value)
  	local formatted = value
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end

convertTime = function(timestamp)
    local time = {}
    time["timestamp"] = timestamp
    time["day"]   =  math.floor(math.fmod(timestamp, 2629743) / 86400)
    time["hour"]   = math.floor(math.fmod(timestamp, 86400) / 3600)
    time["minute"] = math.floor(math.fmod(timestamp , 3600) / 60 )
    time["second"] = math.floor(math.fmod(timestamp, 60))
    
    return time
end

getTimeString = function(timestamp)
	local tempTimestamp = math.floor(timestamp/1000) -- Convert from miliseconds to seconds
	local time = convertTime(tempTimestamp)
	local timeString = ""
	if time.day > 0 then
		timeString = tostring(time.day)
		if time.day == 1 then
			timeString = timeString.." day"
		else
			timeString = timeString.." days"
		end
	elseif time.hour > 0 then
		timeString = tostring(time.hour)
		if time.hour == 1 then
			timeString = timeString.." hour"
		else
			timeString = timeString.." hours"
		end 
	elseif time.minute > 0 then
		timeString = tostring(time.minute)
		if time.minute == 1 then
			timeString = timeString.." minute"
		else
			timeString = timeString.." minutes"
		end 
	elseif time.second > 0 then
		timeString = tostring(time.second)
		if time.second == 1 then
			timeString = timeString.." second"
		else
			timeString = timeString.." seconds"
		end 
	end
	return timeString
end