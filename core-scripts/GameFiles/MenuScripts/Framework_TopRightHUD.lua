--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_TopRightHUD.lua
-------------------------------------------------------------------------------

-- INCLUDES

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\MenuAnimation.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_InventoryHelper.lua")
dofile("..\\MenuScripts\\FrameworkTooltipHelper.lua")

-- LOCAL VARS

local m_username = ""

local m_gameItems = {}
local m_updatePending = false
local m_leaderboardEnabled = false
local m_leaderboardInitialized = false
local m_coinsExist = false
local m_flagsExist = false
local WORLD_COIN_INDEX = "140"

-- Fade Paramerters
FADE_SECONDS 	= .25
g_timer 		= 0
g_showTooltip 	= false

-- Flag properties
local flagInfo = {}
flagInfo.PID = -1
flagInfo.members = {}
flagInfo.ravers = {}

local ICON_SIZE = 64
local ICON_SPACING = 8 -- 48 << if there is a third icon below one with a text label
local ICON_LABEL_OVERLAP = 5
local ICON_LABEL_LEFT_SHIFT = 19
local ICON_GLOW_BUFFER = 2
local ICON_BTN_BUFFER = 4

local DEFAULT_FLAG_LOCATION = {x = -102, y = 90}

-- Hover Box
local HOVER_BOX_WIDTH = 300
local HOVER_BOX_TOP_HEIGHT = 72
local HOVER_BOX_BOTTOM_HEIGHT = 32
local HOVER_BOX_HEIGHT = HOVER_BOX_TOP_HEIGHT + HOVER_BOX_BOTTOM_HEIGHT
local HOVER_BOX_HEAD_HEIGHT = 16
local HOVER_BOX_PADDING = 8
m_hoverBoxEnabled = false

local m_mouseX = 0
local m_mouseY = 0
local MOUSE_HEIGHT = 16

local m_zoneInstanceId = 0
local m_zoneType       = 0

local VISIT_RESPONSES = {
	SUCCESS = 0,
	ALREADY_RAVED = -1,
	ERROR = -2,
	UNKNOWN_ERROR = -3
}

m_flickerMode = false
m_flickerRed = false
m_animCnt = 0

-- CORE FUNCTIONS

-- Called when the menu is created
function onCreate()
	m_username = KEP_GetLoginName()

	-- Event handlers 
	Events.registerHandler("FRAMEWORK_CLAIM_FLAG_STATUS_UPDATE", buildFlagStatusUpdateHandler)
	Events.registerHandler("FLAG_GROUP_INFO_RETURNED", flagGroupInfoReturnedHandler)
	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS", returnSettingsHandler)
	Events.registerHandler("FRAMEWORK_PLACE_GAME_ITEM_CONFIRMATION", frameworkPlaceGameItemConfirmHandler) -- Used to register flags for the leaderboard as they are built
	Events.registerHandler("LAST_CLAIM_FLAG_DELETED", lastClaimFlagDeletedHandler)
	Events.registerHandler("FIRST_CLAIM_FLAG_PLACED", firstClaimFlagPlacedHandler)


	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)

	KEP_EventRegister( "FLAG_GROUP_INFO_CLOSE", KEP.HIGH_PRIO )
	KEP_EventRegister( "INITIALIZE_FLAG_GROUP_INFO", KEP.MED_PRIO )

	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )

	m_zoneInstanceId = tostring(KEP_GetZoneInstanceId())
	m_zoneType       = tostring(KEP_GetZoneIndexType())

	Events.sendEvent("FRAMEWORK_REQUEST_SETTINGS")
	Events.sendEvent("REQUEST_ALL_FLAGS")
	requestInventory(GAME_PID)

	disableBuildFlag()

	-- Leaderboard
	setVisibleAndEnabled(gHandles["imgLeaderboard"], false)
	setVisibleAndEnabled(gHandles["btnLeaderboard"], false)
	setVisibleAndEnabled(gHandles["imgLeaderboardGlow"], false)

	-- Claim Flag
	setVisibleAndEnabled(gHandles["imgBuildFlagGlow"], false)

	_G["btnViewBuildFlag_OnButtonClicked"] = function(btnHandle)
		openFlagInfo()
	end

	-- add the tooltip 
	FWTooltipHelper.registerInventoryEvents()
	FWTooltipHelper.registerInventoryHandlers()
end

-- Called each frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_leaderboardEnabled and m_coinsExist and m_flagsExist and m_updatePending and not m_leaderboardInitialized then

		shiftFlagIconDown()
		initializeLeaderboard()

		-- Check to see if there is a reward pending
		local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=getAward&zoneInstanceId="..tostring(m_zoneInstanceId).."&zoneType="..tostring(m_zoneType)
		makeWebCall(address, WF.LB_REWARD_STATUS_GET)

		m_updatePending = false
		m_leaderboardInitialized = true
	end

	-- Flicker the flag icon red if almost due or past due
	if m_flickerMode then
		if m_animCnt >= 40 then
			m_animCnt = 0
			if m_flickerRed then
				m_flickerRed = false
				setVisibleAndEnabled(gHandles["imgBuildFlagProx"], true)
				setVisibleAndEnabled(gHandles["imgBuildFlagProxRed"], false)
			else
				m_flickerRed = true
				setVisibleAndEnabled(gHandles["imgBuildFlagProx"], false)
				setVisibleAndEnabled(gHandles["imgBuildFlagProxRed"], true)
			end
		else
			m_animCnt = m_animCnt + 1
		end
	end
end

--------------------
-- EVENT HANDLERS --
--------------------

function frameworkPlaceGameItemConfirmHandler(event)
	if event.success == true and event.PID then
		local tempPID = event.PID
		
		-- Update member list for leaderboard
		local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=teamUpdate&objPlacementId="..tostring(tempPID).."&usernames="..tostring(m_username)
		makeWebCall(address, WF.FLAG_TEAM_UPDATE)

		-- Update flag name for leaderboard
		local tempGroupName = tostring(m_username).."\'s Place"
		local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=setObjectName&objPlacementId="..tostring(tempPID).."&name="..tostring(tempGroupName)
		makeWebCall(address, WF.FLAG_NAME_UPDATE)
	end
end

function buildFlagStatusUpdateHandler(event)
	-- If player moved within range of a flag
	if MenuIsOpen("ProgressMenu.xml") then return end
	if event.buildFlag == true then
		-- Close menu if another flag is open
		if MenuIsOpen("Framework_FlagInfo.xml") then
			local event = KEP_EventCreate("FLAG_GROUP_INFO_CLOSE")
			KEP_EventQueue(event)
		end
		setVisibleAndEnabled(gHandles["imgBuildFlagProx"], true)
		flagInfo.PID = event.flagPID
		-- Request build flag info (don't enable button until it is received)
		Events.sendEvent("REQUEST_FLAG_GROUP_INFO", {playerName = KEP_GetLoginName(), flagPID = flagInfo.PID})
	-- If player moved out of range of the flag that enabled this
	elseif event.buildFlag == false and event.flagPID == flagInfo.PID then
		disableBuildFlag()
		flagInfo.PID = -1
	end
end

function lastClaimFlagDeletedHandler(event)
	m_flagsExist = false
	disableLeaderboard()
	shiftFlagIconUp()
	m_leaderboardInitialized = false
end

function firstClaimFlagPlacedHandler(event)
	m_flagsExist = true
	m_updatePending = true
end

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- If browser return for Location Data web call
	if filter == WF.FLAG_VISIT then
		local returnXML = KEP_EventDecodeString( tEvent )
		local returnCode = ParseXmlReturnCode(returnXML)
		local returnDescription = xmlGetString(returnXML, "ReturnDescription")
	elseif filter == WF.LB_REWARD_STATUS_GET then
		local returnXML = KEP_EventDecodeString( tEvent )
		local returnCode = ParseXmlReturnCode(returnXML)
		local returnDescription = xmlGetString(returnXML, "ReturnDescription")

		if returnCode == 0 then
			m_prizeData = {}

			local filterString = "<user_awards>(.-)</user_awards>"
			for item in string.gmatch(returnXML, filterString) do
				local tempusername = xmlGetString(item, "username")
				if tempusername == m_username then
					MenuOpen("Framework_LeaderboardCongrats.xml")
				end

			end
		end
	end
end

function flagGroupInfoReturnedHandler(event)
	m_flagsExist = true
	m_updatePending = true

	if event.flagInfo.PID == flagInfo.PID then
		-- Save values locally
		if event.flagInfo then
			flagInfo.raveCount = event.flagInfo.raveCount
			flagInfo.owner = event.flagInfo.owner
			flagInfo.members = event.flagInfo.members or {}
			flagInfo.ravers = event.flagInfo.ravers
			flagInfo.UNID = event.flagInfo.UNID

			flagInfo.feeDueDate = event.flagInfo.feeDueDate
			flagInfo.isFeeDue = event.flagInfo.isFeeDue or false
			flagInfo.isFeeDueSoon = event.flagInfo.isFeeDueSoon or false
			local isMember = false
			local playerName = KEP_GetLoginName()
			for index, member in pairs (event.flagInfo.members) do
				if member == playerName then
					isMember = true
				end
			end
			m_flickerMode = flagInfo.isFeeDueSoon and isMember

			if m_flickerMode then
				setVisibleAndEnabled(gHandles["imgBuildFlagProx"], false)
				setVisibleAndEnabled(gHandles["imgBuildFlagProxRed"], true)
			else
				setVisibleAndEnabled(gHandles["imgBuildFlagProx"], true)
				setVisibleAndEnabled(gHandles["imgBuildFlagProxRed"], false)
			end
			
			setVisibleAndEnabled(gHandles["btnViewBuildFlag"], true)

			flagInfo.groupName = event.flagInfo.groupName
			if flagInfo.groupName and flagInfo.groupName ~= "" then
				Static_SetText(gHandles["stcGroupName"], flagInfo.groupName)
				setVisibleAndEnabled(gHandles["stcGroupName"], true)
				Static_SetText(gHandles["stcGroupNameShadowUp"], flagInfo.groupName)
				setVisibleAndEnabled(gHandles["stcGroupNameShadowUp"], true)
				Static_SetText(gHandles["stcGroupNameShadowDown"], flagInfo.groupName)
				setVisibleAndEnabled(gHandles["stcGroupNameShadowDown"], true)
				Static_SetText(gHandles["stcGroupNameShadowLeft"], flagInfo.groupName)
				setVisibleAndEnabled(gHandles["stcGroupNameShadowLeft"], true)
				Static_SetText(gHandles["stcGroupNameShadowRight"], flagInfo.groupName)
				setVisibleAndEnabled(gHandles["stcGroupNameShadowRight"], true)
			end

			-- Update visit webcall
			if m_leaderboardEnabled and m_coinsExist and m_flagsExist then
				local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=visit&zoneInstanceId="..m_zoneInstanceId.."&zoneType="..m_zoneType.."&objPlacementId="..tostring(flagInfo.PID).."&name="..tostring(flagInfo.groupName)
				makeWebCall(address, WF.FLAG_VISIT)
			end

			local flagTooltip = {}
			flagTooltip["header"] = {label = flagInfo.groupName}
			flagTooltip["body"] = {label = "This area is owned by ".. flagInfo.owner.. "."}
			flagTooltip["brokenItem"] = {label = "Click button to see more information."}

			FWTooltipHelper.addFrameworkTooltip("btnViewBuildFlag", flagTooltip)
			--InventoryHelper.addFrameworkTooltip("btnViewBuildFlag", flagTooltip)
		end
		-- If flag menu already open, update it
		if MenuIsOpen("Framework_FlagInfo.xml") then
			initializeFlagInfo()
		end
		updateGlow()
	end
end

-- Handles zone settings
function returnSettingsHandler(event)
	m_leaderboardEnabled = event.leaderboard or false
	m_updatePending = true
end

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)
	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
	m_coinsExist = m_gameItems[WORLD_COIN_INDEX] ~= nil
	m_updatePending = m_updatePending or m_coinsExist
end

-- INTERACTION HANDLERS

function Dialog_OnMouseMove(dialogHandle, x, y)
	m_mouseX = x
	m_mouseY = y
end

-- LOCAL FUNCTIONS

--------
-- 1: PID
-- 2: groupName
-- 3: ravers
-- 4: members
-- 5: owner
--------
initializeFlagInfo = function()
	local event = KEP_EventCreate("INITIALIZE_FLAG_GROUP_INFO")
	KEP_EventEncodeString(event, flagInfo.PID)
	KEP_EventEncodeString(event, flagInfo.UNID)
	KEP_EventEncodeString(event, flagInfo.groupName)
	local tempRavers = flagInfo.ravers
	KEP_EventEncodeString(event, Events.encode(tempRavers))
	local tempMembers = flagInfo.members
	KEP_EventEncodeString(event, Events.encode(tempMembers))
	KEP_EventEncodeString(event, flagInfo.owner)

	KEP_EventEncodeString(event, flagInfo.isFeeDue)
	KEP_EventEncodeNumber(event, flagInfo.feeDueDate)
	
	KEP_EventQueue(event)
end

initializeLeaderboard = function()
	setVisibleAndEnabled(gHandles["imgLeaderboard"], true)
	setVisibleAndEnabled(gHandles["btnLeaderboard"], true)
	-- Leaderboard
	_G["btnLeaderboard_OnButtonClicked"] = function(btnHandle)
		local tempMenu = "Framework_PlayerLeaderboard.xml"
		if MenuIsOpen(tempMenu) then
			MenuClose(tempMenu)
		else
			MenuOpen(tempMenu)
		end
	end
	_G["btnLeaderboard_OnMouseEnter"] = function(buttonHandle)
		setVisibleAndEnabled(gHandles["imgLeaderboardGlow"], true)
	end

	_G["btnLeaderboard_OnMouseLeave"] = function(buttonHandle)
		setVisibleAndEnabled(gHandles["imgLeaderboardGlow"], false)
	end
	-- MenuOpen("Framework_PlayerLeaderboard.xml") -- First time load?
end

disableLeaderboard = function()
	setVisibleAndEnabled(gHandles["imgLeaderboard"], false)
	setVisibleAndEnabled(gHandles["btnLeaderboard"], false)
end

shiftFlagIconDown = function()
	newX = DEFAULT_FLAG_LOCATION.x
	newY = DEFAULT_FLAG_LOCATION.y + ICON_SIZE + ICON_SPACING
	Control_SetLocation(gHandles["imgBuildFlagProx"], newX, newY)
	Control_SetLocation(gHandles["imgBuildFlagProxRed"], newX, newY)
	Control_SetLocation(gHandles["imgBuildFlagGlow"], newX - ICON_GLOW_BUFFER, newY - ICON_GLOW_BUFFER)
	Control_SetLocation(gHandles["btnViewBuildFlag"], newX - ICON_BTN_BUFFER, newY - ICON_BTN_BUFFER)
	Control_SetLocation(gHandles["stcGroupName"], newX - ICON_LABEL_LEFT_SHIFT, newY + ICON_SIZE - ICON_LABEL_OVERLAP)
	Control_SetLocation(gHandles["stcGroupNameShadowUp"], newX - ICON_LABEL_LEFT_SHIFT, newY + ICON_SIZE - ICON_LABEL_OVERLAP - 1)
	Control_SetLocation(gHandles["stcGroupNameShadowDown"], newX - ICON_LABEL_LEFT_SHIFT, newY + ICON_SIZE - ICON_LABEL_OVERLAP + 1)
	Control_SetLocation(gHandles["stcGroupNameShadowLeft"], newX - ICON_LABEL_LEFT_SHIFT - 1, newY + ICON_SIZE - ICON_LABEL_OVERLAP)
	Control_SetLocation(gHandles["stcGroupNameShadowRight"], newX - ICON_LABEL_LEFT_SHIFT + 1, newY + ICON_SIZE - ICON_LABEL_OVERLAP)
end

shiftFlagIconUp = function()
	newX = DEFAULT_FLAG_LOCATION.x
	newY = DEFAULT_FLAG_LOCATION.y
	Control_SetLocation(gHandles["imgBuildFlagProx"], newX, newY)
	Control_SetLocation(gHandles["imgBuildFlagProxRed"], newX, newY)
	Control_SetLocation(gHandles["imgBuildFlagGlow"], newX - ICON_GLOW_BUFFER, newY - ICON_GLOW_BUFFER)
	Control_SetLocation(gHandles["btnViewBuildFlag"], newX - ICON_BTN_BUFFER, newY - ICON_BTN_BUFFER)
	Control_SetLocation(gHandles["stcGroupName"], newX - ICON_LABEL_LEFT_SHIFT, newY + ICON_SIZE - ICON_LABEL_OVERLAP)
	Control_SetLocation(gHandles["stcGroupNameShadowUp"], newX - ICON_LABEL_LEFT_SHIFT, newY + ICON_SIZE - ICON_LABEL_OVERLAP - 1)
	Control_SetLocation(gHandles["stcGroupNameShadowDown"], newX - ICON_LABEL_LEFT_SHIFT, newY + ICON_SIZE - ICON_LABEL_OVERLAP + 1)
	Control_SetLocation(gHandles["stcGroupNameShadowLeft"], newX - ICON_LABEL_LEFT_SHIFT - 1, newY + ICON_SIZE - ICON_LABEL_OVERLAP)
	Control_SetLocation(gHandles["stcGroupNameShadowRight"], newX - ICON_LABEL_LEFT_SHIFT + 1, newY + ICON_SIZE - ICON_LABEL_OVERLAP)
end

checkForInventoryIntersection = function()

	if MenuIsOpen("UnifiedInventory") then
		local menuLocation = MenuGetLocation("UnifiedInventory.xml")
		local menuSize = MenuGetSize("UnifiedInventory.xml")

		local thisLocation = MenuGetLocationThis()
		local thisControlX = Control_GetLocationX(gHandles["imgBuildFlagProx"]) -- X location of flag icon
		local thisWidth = HOVER_BOX_WIDTH
		
		local menuRightSide = menuLocation.x + menuSize.width
		local menuLeftSide = thisLocation.x + thisControlX - thisWidth

		if menuRightSide >= menuLeftSide then
			return true
		else
			return false
		end
	else
		return false
	end
end

-- Add the ability to glow
updateGlow = function()
	_G["btnViewBuildFlag_OnMouseEnter"] = function(buttonHandle)
	-- Control_SetFocus(gHandles["btnViewBuildFlag"])
		if flagInfo.PID >= 0 and not checkForInventoryIntersection() then
			setVisibleAndEnabled(gHandles["imgBuildFlagGlow"], true)

			if not MenuIsOpen("Framework_FlagInfo.xml") then
				g_showTooltip = true
			end
		end
	end

	_G["btnViewBuildFlag_OnMouseLeave"] = function(buttonHandle)
		setVisibleAndEnabled(gHandles["imgBuildFlagGlow"], false)
		g_showTooltip = false
	end

end

openFlagInfo = function()
	if not MenuIsOpen("Framework_FlagInfo.xml") then
		MenuOpen("Framework_FlagInfo.xml")
		initializeFlagInfo()
	else
		local event = KEP_EventCreate("FLAG_GROUP_INFO_CLOSE")
		KEP_EventQueue(event)
	end
end

disableBuildFlag = function()
	m_flickerMode = false
	setVisibleAndEnabled(gHandles["imgBuildFlagProx"], false)
	setVisibleAndEnabled(gHandles["imgBuildFlagProxRed"], false)
	setVisibleAndEnabled(gHandles["btnViewBuildFlag"], false)
	setVisibleAndEnabled(gHandles["btnViewBuildFlag"], false)
	setVisibleAndEnabled(gHandles["stcGroupName"], false)
	setVisibleAndEnabled(gHandles["stcGroupNameShadowUp"], false)
	setVisibleAndEnabled(gHandles["stcGroupNameShadowDown"], false)
	setVisibleAndEnabled(gHandles["stcGroupNameShadowLeft"], false)
	setVisibleAndEnabled(gHandles["stcGroupNameShadowRight"], false)
	setVisibleAndEnabled(gHandles["imgBuildFlagGlow"], false)
end 

-- HELPER FUNCTIONS

setVisibleAndEnabled = function(inputHandle, inputValue)
	Control_SetVisible(inputHandle, inputValue)
	Control_SetEnabled(inputHandle, inputValue)
end
