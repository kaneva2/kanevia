--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\Unlockables.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")

MIN_SIGNUP = {month = 7, day = 5, year = 2012}

g_kanevaUserNetId = 0
g_playerName = 0
g_myName = 0
g_myUserID = 0
g_userID = 0
g_playerID = 0
g_fileName = 0 --filename of avatar picture
g_age = 0
g_displayName = 0
g_location = 0
g_buttonPressed = nil
g_targetSignup = nil
g_menuCtrls = {"lblRealName", "stcAgeLabel", "lblUserAge", "imgFrame", "imgBackground", "imgObjectPic", "imgRave", "lblRaveCount", "imgFame", "lblFameLevel", "lblUserLocation", "imgHorDoubleTop", "btnP2P", "btnRave", "btnPrivateChat", "btnAddFriend", "btnCurrentWardrobe", "btnGiveCredits", "btnWebView", "imgHorDoubleBottom", "btnReportBlock"}
g_isBlockingUser = false
g_isGm = false
g_creditBalance = nil
g_rewardBalance = nil

g_metricsData = {
	menu = "RClickMember.xml",
	buttonsClicked = {}
}

BTN_RAVE = 1

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "PlayerTargetedMenuEventHandler", "PlayerTargetedMenuEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyEventHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "LooseTextureDownloadEventHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler( "RavePurchasedEventHandler", "RavePurchasedEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "p2pInfoEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PlayerUserIdEvent", KEP.MED_PRIO )
	KEP_EventRegister( "RavePurchasedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "GIVE_CREDITS_EVENT", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	
	g_myName = KEP_GetLoginName()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..g_myName, WF.MYFAME_USERID)

	-- DRF - Only Enable MemberAlts Link In DevMode
	setControlVisible("btnAlts", (KEP_DevMode() == 1))

	checkTestGroup()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

-- alters the menu for A/B testing
function checkTestGroup()
	local showCreditTrading = false

	if g_creditBalance then
		showCreditTrading = (g_creditBalance ~= 0)
	end

	-- Set enable and set visibility of credit trading according to their testing group
	Control_SetVisible(gHandles["btnGiveCredits"], showCreditTrading)
	Control_SetEnabled(gHandles["btnGiveCredits"], showCreditTrading)

	-- Reset Buttons Position to accomodate give credits change
	if showCreditTrading then
		Control_SetLocationX(gHandles["btnCurrentWardrobe"], 69)
		Control_SetLocationX(gHandles["btnWebView"], 126)
	else
		Control_SetLocationX(gHandles["btnCurrentWardrobe"], 41)
		Control_SetLocationX(gHandles["btnWebView"], 98)
	end
end

-- DRF - Update MemberAlts
function btnAlts_OnButtonClicked(buttonHandle)
	table.insert(g_metricsData.buttonsClicked, "btnAlts")
	SendMemberAltsEvent(g_playerName)
end

function RavePurchasedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	--update rave count
	requestPlayerRaves()
end

function PlayerTargetedMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
		
	resetGlobals()

	-- Decode Event Params
	g_kanevaUserNetId  = KEP_EventDecodeNumber( event )
	g_playerName = KEP_EventDecodeString( event )
	Log("PlayerTargetedMenuEventHandler: targetUserId="..g_kanevaUserNetId.." targetUserName="..g_playerName)

	g_metricsData.targetName = g_playerName

	-- Update Player Name Text
	Static_SetText(Button_GetStatic(gHandles["btnProfile"]), g_playerName)

	-- Get Player Info
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..g_playerName, WF.CONTEXT_USERID)
		
	-- DRF - Update MemberAlts If Already Open
	if MenuIsOpen("MemberAlts.xml") then
		SendMemberAltsEvent(g_playerName)
	end
end

function resetGlobals()
	g_kanevaUserNetId = 0
	g_playerName = 0
	g_userID = 0
	g_playerID = 0
	g_fileName = 0 --filename of avatar picture
	g_age = 0
	g_displayName = 0
	g_location = 0
	g_buttonPressed = nil
	g_targetSignup = nil
end

function BrowserPageReadyEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.CONTEXT_USERID then
		g_returnData = KEP_EventDecodeString( event )
		ParseUserID()
		return KEP.EPR_OK
	elseif filter == WF.TARGETED then
		g_returnData = KEP_EventDecodeString( event )
		ParseResults()
		return KEP.EPR_OK
	elseif (filter == WF.CONTEXT_FAME) then
		local returnData = KEP_EventDecodeString( event )
		s, e, fametype = string.find(returnData, "<FameTypeId>(%d+)</FameTypeId>")
		fametype = tonumber(fametype)
		s, e, curPoints = string.find(returnData, "<Points>(%d+)</Points>")
		curPoints = tonumber(curPoints)
		s, e, startPoints = string.find(returnData, "<StartPoints>(%d+)</StartPoints>")
		startPionts = tonumber(startPoints)
		s, e, endPoints = string.find(returnData, "<EndPoints>(%d+)</EndPoints>")
		endPoints = tonumber(endPoints)
		s, e, level = string.find(returnData, "<LevelNumber>(%d+)</LevelNumber>")
		
		Static_SetText(gHandles["lblFameLevel"], level)
		
		return KEP.EPR_OK
	elseif filter == WF.GET_USER_PROFILE then
		local returnData = KEP_EventDecodeString( event )
	    s, e, g_curRaves = string.find(returnData, "<Raves>(%d+)</Raves>")
	    s, e, g_alreadyRaved = string.find(returnData, "<AlreadyRaved>(%d+)</AlreadyRaved>")
	    g_alreadyRaved = tonumber(g_alreadyRaved)
	    
	    Static_SetText(gHandles["lblRaveCount"], g_curRaves)
	    
	    if g_alreadyRaved == 1 then
	    	--change icon to rave plus
	    	local element = Button_GetNormalDisplayElement(gHandles["btnRave"])
			Element_SetCoords(element, 0, 130, 51, 195)
			element = Button_GetFocusedDisplayElement(gHandles["btnRave"])
			Element_SetCoords(element, 0, 130, 51, 195)
			element = Button_GetMouseOverDisplayElement(gHandles["btnRave"])
			Element_SetCoords(element, 0, 130, 51, 195)
			element = Button_GetPressedDisplayElement(gHandles["btnRave"])
			Element_SetCoords(element, 0, 130, 51, 195)
	    end
	    
	    return KEP.EPR_OK
	elseif filter ==  WF.GET_PROFILE_INFO then
		local returnData = KEP_EventDecodeString( event )

		local s, e, requestedID = string.find(returnData, "<RequestedUserID>(.-)</RequestedUserID>")
		requestedID = tonumber(requestedID)

		--If reuqested ID matched the target
		if requestedID and requestedID == g_userID then
			s, e, showAge = string.find(returnData, "<ShowAge>(.-)</ShowAge>")
			s, e, showLocation = string.find(returnData, "<ShowLocation>(.-)</ShowLocation>", e)
			s, e, showGender = string.find(returnData, "<ShowGender>(.-)</ShowGender>", e)
		
			s, e, g_age = string.find(returnData, "<age>(%d+)</age>")
			s, e, g_location = string.find(returnData, "<location>(.-)</location>", e)
			s, e, g_displayName = string.find(returnData, "<display_name>(.-)</display_name>", e)
	    	s, e, g_targetSignup = string.find(returnData, "<signup_date>(.-)</signup_date>")

			if g_age == nil then g_age = "" end 
			if g_displayName == nil then g_displayName = "" end
			if g_location == nil then g_location = "" end
		
			g_displayName = stripTags(g_displayName)  -- remove "&#39;" type stuff since it won't display correctly anyway.

			setControlHeight("lblRealName", g_displayName)
			setControlHeight("lblUserAge", g_age, showAge, {"stcAgeLabel"})
			setControlHeight("lblUserLocation", g_location, showLocation)
			
			--If target is not a new player, get info on current player
			if not playerNewerThanDate(g_targetSignup, MIN_SIGNUP) then
				makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..g_myName, WF.CONTEXT_USERID)
			end
		end
	elseif filter == WF.FRIEND_THUMBNAIL_URL then
		local returnData = KEP_EventDecodeString( event )
		s, e, front, url = string.find(returnData, "(http://.-/)(.*)")
		s, e, front, file_name = string.find(returnData, "(.*/)(.*)")
		
		if ( file_name ~= nil ) then
			g_fileName = g_userID .. "_" .. file_name 
			KEP_DownloadTextureAsyncToGameId( g_fileName, url )
		end
	elseif filter == WF.BLOCK then
		local blockData = KEP_EventDecodeString( event )
		parseBlockData(blockData)
		return KEP.EPR_OK
	elseif filter == WF.MYFAME_GETBALANCE then
		local returnData = KEP_EventDecodeString( event )
		g_creditBalance, g_rewardBalance = ParseBalances(returnData)
		checkTestGroup()
		return KEP.EPR_OK
	elseif filter == WF.MYFAME_USERID then
		g_returnData = KEP_EventDecodeString( event )
		ParseUserID(returnData)
		return KEP.EPR_OK
	end
end

function LooseTextureDownloadEventHandler()		
	local gameId = KEP_GetGameId()
	local ihFore = Dialog_GetImage(gDialogHandle, "imgObjectPic")
	local ihBack = Dialog_GetImage(gDialogHandle, "imgBackground")			
	if ihFore then
		scaleImage(ihFore, ihBack, "../../CustomTexture/" .. gameId .. "/" .. g_fileName)
    end
end

function requestWorldFame()
	if(g_userID > 0) then
	    makeWebCall( GameGlobals.WEB_SITE_PREFIX .. GET_USER_FAME_SUFFIX .. "&userId=" .. g_userID .. "&fameTypeId=" .. FameIds.WORLD_FAME, WF.CONTEXT_FAME)
	end
end

function requestPlayerRaves()
	if(g_userID > 0) then
	    makeWebCall( GameGlobals.WEB_SITE_PREFIX .. PROFILE_COUNTS_SUFFIX.."&userId=" .. g_userID, WF.GET_USER_PROFILE)
	end
end

function requestPlayerProfile(userID)
	if(g_userID > 0) then
	    makeWebCall( GameGlobals.WEB_SITE_PREFIX .. GET_USER .. userID, WF.GET_PROFILE_INFO)
	end
end

function requestBlockInformation(blockingUserID, blockedUserID)
	if(g_userID > 0) then
		makeWebCall(GameGlobals.WEB_SITE_PREFIX..GET_USER_BLOCK_DATA_SUFFIX..blockedUserID.."&blockingUserId="..blockingUserID, WF.BLOCK)
	end
end

function ParseResults()

	local result = nil  -- result description
	local code = nil 	-- return code
	local s, e = 0      -- start and end of captured string
	
	s, e, code = string.find(g_returnData, "<ReturnCode>(.-)</ReturnCode>")
	s, e, result = string.find(g_returnData, "<ResultDescription>(.-)</ResultDescription>")
	
	if result ~= nil then
		Log("ParseResults: response=...\n"..result)
	
	    if g_buttonPressed == BTN_RAVE then
			if result == "already raved" then
				KEP_MessageBox(RM.ALREADY_RAVED..g_playerName)
	        end
	        if result == "success" then
	            KEP_MessageBox(RM.RAVE..g_playerName)
				requestPlayerRaves()
			end
		elseif g_buttonPressed == BTN_ADD_FRIEND then

        	if code ~= "-1" then
				if result == "already friends" then
					KEP_MessageBox(RM.ALREADY_FRIENDS..g_playerName)
				elseif result == "already requested to be friends" then
					KEP_MessageBox(RM.FRIEND_REQUEST_SENT..g_playerName)
			    elseif result == "success" then
					KEP_MessageBox(RM.FRIEND_REQUEST..g_playerName)
				end
			else
				KEP_MessageBox(RM.FRIEND_REQUEST_SENT..g_playerName)
			end
		elseif g_buttonPressed == BTN_IGNORE then

			if result == "success" then
				KEP_MessageBox(RM.ADD_IGNORE..g_playerName)
			else
				KEP_MessageBox(RM.ALREADY_IGNORE..g_playerName)
			end
			
		else
		   	KEP_MessageBox(result)
		end
	end

	g_buttonPressed = BTN_NONE
	g_resultParse = false
end

function ParseUserID()
	local result = nil  	-- result description
	local s, e = 0      	-- start and end of captured string
	local sh = nil			-- static handle
	local dbPath = ""			-- path of picture on website, returned from database	
	s, e, result = string.find(g_returnData, "<ReturnCode>(%d+)</ReturnCode>")
	if result == "0" then

		s, e, requestedName = string.find(g_returnData, "<RequestedAvatarName>(.-)</RequestedAvatarName>")
		
		--If other player's ID has already been set, it's calling for own player's ID
		if g_myName == requestedName then
			s, e, result = string.find(g_returnData, "<user_id>(%d+)</user_id>", e)
			g_myUserID = tonumber(result)
			requestPlayerProfile(result)

			-- If the user requested exists, get their balance
			if g_myUserID > 0 then
				getBalances(g_myUserID, WF.MYFAME_GETBALANCE)
			end

		elseif g_playerName == requestedName then
			-- Parse needed info from XML
			s, e, result = string.find(g_returnData, "<user_id>(%d+)</user_id>", e)
			g_userID = tonumber(result)
			
			s, e, result = string.find(g_returnData, "<player_id>(%d+)</player_id>", e)
			g_playerID = tonumber(result)
			
			s, e, dbPath = string.find(g_returnData, "<thumbnail_square_path>(.-)</thumbnail_square_path>", e)
			if not dbPath then
				dbPath = DEFAULT_AVATAR_PIC_PATH
			end		
			
			if g_userID > 0 and dbPath then
				makeWebCall(GameGlobals.WEB_SITE_PREFIX .. GET_FRIEND_THUMBNAIL_URL_SUFFIX .. g_userID, WF.FRIEND_THUMBNAIL_URL)
			
				requestWorldFame()
				requestPlayerRaves()
				requestPlayerProfile(g_userID)
				requestBlockInformation(g_userID, KEP_GetKanevaUserId())
			end
		end
	end
end

function parseBlockData( blockData )
	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local data = ""         --data segment
	s, strPos, result = string.find(blockData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
	
		-- Check to see if the target user is blocking this user
		s, strPos, data = string.find(blockData, "<comm_blocked>([0-9]+)</comm_blocked>")
		if data == "1" then
			g_isBlockingUser = true
		else
		    g_isBlockingUser = false
		end
	end

	-- Check to see if this user is a GM. This is returned even if result ~= 0.
	s, strPos, data = string.find(blockData, "<GM>([0-9]+)</GM>")
	if data == "1" then
		g_isGm = true

		Control_SetEnabled(gHandles["btnReportBlock"], false)
	end
end

function setControlHeight(handleName, text, showText, relatedControls)
	local handle = gHandles[handleName]
	local ctrlHeight = Control_GetHeight(handle)
	
	if (showText == "True" or showText == nil) and text and text ~= "" then


		local strHeight = Static_GetStringHeight(handle, text)
		if strHeight > ctrlHeight then
			local diff = strHeight-ctrlHeight
			local handleFound = false
			for i = 1, #g_menuCtrls do
				if not handleFound then
					handleFound = g_menuCtrls[i] == handleName
				else
					local currCtrl = gHandles[g_menuCtrls[i]]
					Control_SetLocationY(currCtrl, Control_GetLocationY(currCtrl)+diff)
				end
			end
			Control_SetSize(handle, Control_GetWidth(handle), strHeight)
		
			local size = MenuGetSizeThis()
			MenuSetSizeThis(size.width, size.height + diff)
		end

		Static_SetText(handle, text)

		if relatedControls and (type(relatedControls) == "table") then
			for i,v in pairs(relatedControls) do
				Control_SetVisible(gHandles[v], true)
			end
		end

	else
		Static_SetText(handle, "")
		local handleFound = false
		for i = 1, #g_menuCtrls do
			if not handleFound then
				handleFound = g_menuCtrls[i] == handleName
			else
				local currCtrl = gHandles[g_menuCtrls[i]]
				Control_SetLocationY(currCtrl, Control_GetLocationY(currCtrl)-ctrlHeight)
			end
		end

		Control_SetSize(handle, Control_GetWidth(handle), 0)
		
		local size = MenuGetSizeThis()
		MenuSetSizeThis(size.width, size.height - ctrlHeight)

		if relatedControls and (type(relatedControls) == "table") then
			for i,v in pairs(relatedControls) do
				Control_SetVisible(gHandles[v], false)
			end
		end
	end
end

function btnProfile_OnButtonClicked(buttonHandle)
	table.insert(g_metricsData.buttonsClicked, "btnProfile")

	if g_userID then
		if g_playerName ~= "" then  
			KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_CHANNEL_PATH..g_playerName..".people" )
		end
	end
end

function btnRave_OnButtonClicked(buttonHandle)
	table.insert(g_metricsData.buttonsClicked, "btnRave")

	-- This function is a no-op if the target user has blocked the targeting user
	if not g_isBlockingUser then
		if g_userID ~= nil then
			if g_alreadyRaved == 0 then
				Log("RaveClicked: OK userId="..g_userID)
				local web_address = GameGlobals.WEB_SITE_PREFIX..RAVE_SUFFIX..g_userID.."&raveType=single&currency=FREE"
				g_buttonPressed = BTN_RAVE
				-- Send rave to site, use 0 for current page and amount per page
				makeWebCall( web_address, WF.TARGETED)
			else
				Log("RaveClicked: ALREADY_RAVED userId="..g_userID)
				purchaseMenu = MenuOpen("Checkout.xml")
				
				local ev = KEP_EventCreate( "PurchaseRaveEvent" )
				KEP_EventEncodeNumber(ev, g_userID)
				KEP_EventEncodeNumber(ev, 0) --ravePlus or megarave?
				KEP_EventEncodeNumber(ev, 0) --ravePerson is 0, ravePlace is 1
				KEP_EventEncodeString(ev, g_playerName)
				KEP_EventEncodeNumber(ev, 0) --not rave back is 0, rave back is 1
				KEP_EventEncodeString(ev, MenuNameThis())
				KEP_EventQueue( ev)
			end
		else
			KEP_MessageBox("Unknown user")
		end
	end
end

function btnWebView_OnButtonClicked(buttonHandle)
	table.insert(g_metricsData.buttonsClicked, "btnWebView")

	if g_userID then
		if g_playerName ~= "" then  
			KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_CHANNEL_PATH..g_playerName..".people" )
		end
	end
end

function btnReportBlock_OnButtonClicked( buttonHandle)
	table.insert(g_metricsData.buttonsClicked, "btnReportBlock")

    --if I clicked on someone other than myself, go to block menu, else go to titles menu
	if g_myName ~= g_playerName then
		if g_userID then
			MenuOpenModal("BlockReportMenu.xml")
			local ev = KEP_EventCreate( "PlayerUserIdEvent" )
			KEP_EventEncodeNumber( ev, g_userID)
			KEP_EventEncodeString( ev, g_playerName)
			KEP_EventQueue( ev )
		end
	else
		KEP_MessageBox("You cannot block yourself.")
	end
end

function btnAddFriend_OnButtonClicked( buttonHandle)
	table.insert(g_metricsData.buttonsClicked, "btnAddFriend")

	-- This function is a no-op if the target user has blocked the targeting user
	if not g_isBlockingUser then

		if g_userID ~= nil then
			if g_myName ~= g_playerName then
				sendFriendRequest(g_playerName, MenuNameThis())
			else
				KEP_MessageBox("You cannot not send yourself a friend request.")
			end
		else
			KEP_MessageBox("Unknown user")
		end
	end
end 

function btnP2P_OnButtonClicked( buttonHandle)
	table.insert(g_metricsData.buttonsClicked, "btnP2P")

	-- This function is a no-op if the target user has blocked the targeting user
	if not g_isBlockingUser then
		if g_myName ~= g_playerName then
			MenuOpen("p2pEmoteMenu2.xml")
			local ev = KEP_EventCreate( "p2pInfoEvent" )
			KEP_EventEncodeNumber( ev, g_kanevaUserNetId )
			KEP_EventEncodeString( ev, g_playerName )
			KEP_EventQueue( ev )
 		else
			KEP_MessageBox("You must select another person to perform this emote.")
		end
	end
end

function btnPrivateChat_OnButtonClicked( buttonHandle)
	table.insert(g_metricsData.buttonsClicked, "btnPrivateChat")
end

function btnSendMessage_OnButtonClicked( buttonHandle)
	table.insert(g_metricsData.buttonsClicked, "btnSendMessage")
	
	if MenuIsClosed("SendMessage2.xml") then
        MenuOpen("SendMessage2.xml")	
			
		local ev = KEP_EventCreate( "PlayerUserIdEvent" )
		KEP_EventEncodeNumber( ev, g_userID)
		KEP_EventEncodeString( ev, g_playerName)
		KEP_EventEncodeString( ev, "Message from " .. g_myName)
		KEP_EventEncodeString( ev, "Type Message Here")
		KEP_EventQueue( ev )
    end
end

function btnCurrentWardrobe_OnButtonClicked( buttonHandle )
	table.insert(g_metricsData.buttonsClicked, "btnCurrentWardrobe")

	if MenuIsClosed("ContextMenuPurchasing2.xml") then
        MenuOpen("ContextMenuPurchasing2.xml")
		
		local ev = KEP_EventCreate( "PlayerUserIdEvent" )
		KEP_EventEncodeNumber( ev, g_userID)
		KEP_EventEncodeString( ev, g_playerName)
		KEP_EventQueue( ev )
    end
end

function btnGiveCredits_OnButtonClicked( buttonHandle )
	table.insert(g_metricsData.buttonsClicked, "btnGiveCredits")

	if MenuIsClosed("GiveCredits.xml") then
		MenuOpen("GiveCredits.xml")

		local ev = KEP_EventCreate( "GIVE_CREDITS_EVENT" )
		KEP_EventEncodeNumber( ev, g_userID )
		KEP_EventEncodeString( ev, g_playerName )
		KEP_EventQueue( ev )
	end
end