--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\PatchHelper.lua")

function Log(txt)
    ProgressUpdate("PatchProgress", "PatchProgress:: "..txt)
end

function StatusWndText(txt)
    Log("StatusWndText: '"..txt.."'")
end

function PatchProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    if filter == MSG_STATUS or 
	   filter == MSG_UNPACKING or 
	   filter == MSG_UNPACKINGGZ then 
		local url = KEP_EventDecodeString( event )
		local status = KEP_EventDecodeString( event )
		local totalUpdate = KEP_EventDecodeString( event )
		local totalSize = KEP_EventDecodeString( event )
		local totalSubSize = KEP_EventDecodeString( event )
		local totalComplete = KEP_EventDecodeNumber( event )
		local subComplete = KEP_EventDecodeNumber( event )

		if ( (status == "Processing...") or (status == "") ) then
			StatusWndText( "Processing...")
		elseif ( status == "Preparing to receive a changed file..." ) then
			StatusWndText( "Preparing for changed file...")
		else
			if ( string.sub(status, 1, 20) == "Downloading Updates:" ) then
				StatusWndText( string.sub(status, 21, -1))
			else
				StatusWndText( status )
			end
		end
	elseif filter == MSG_INIT then 
		local url = KEP_EventDecodeString( event )
		local totalUpdate = KEP_EventDecodeString( event )
		local totalSize = KEP_EventDecodeString( event )
		local totalSubSize = KEP_EventDecodeString( event )
		local totalComplete = KEP_EventDecodeNumber( event )
		local subComplete = KEP_EventDecodeNumber( event )
		StatusWndText( tostring(totalComplete).."% "..tostring(subComplete).."%")	
	elseif filter == MSG_PATCH_HANDLER_COMPLETE then 
        ProgressClose("PatchProgress_Patching")
		StatusWndText( "Complete.  Going to World..." )	
	elseif filter == MSG_ERROR then 
		local url = KEP_EventDecodeString( event )
		local errNum = KEP_EventDecodeNumber( event )
		local errMsg = "Unknown"
		if errNum == ERR_NOTCONNECTED then 
			errMsg = "Not connected"
		elseif errNum == ERR_NOVERSION then
			errMsg = "No version"
		elseif errNum == ERR_TIMEOUT then
			errMsg = "Timeout"
		end 
		local msg = "Error: "..errMsg
        StatusWndText(msg)	
        ProgressError("PatchProgress", msg)
    elseif filter == MSG_CONNECTED then
		local url = KEP_EventDecodeString( event )
		StatusWndText( "Connected" )	
	elseif filter == MSG_RECONNECT then
		local url = KEP_EventDecodeString( event )
		StatusWndText( "Lost connection" )	
	elseif filter == MSG_VERBOSE_ERROR then
		local url = KEP_EventDecodeString( event )
		local errNum = KEP_EventDecodeNumber( event )
		local errMsg = KEP_EventDecodeString( event )
		local msg = "Error: "..errMsg    -- .." ("..tostring(errNum)..")"
        StatusWndText(msg)	
        ProgressError("PatchProgress", msg)
    elseif filter == MSG_ABORT then
        ProgressError("PatchProgress", "Error: Aborting Patch")
	elseif filter == MSG_GOTOAPP then
		local url = KEP_EventDecodeString( event )
		local msg = KEP_EventDecodeString( event )
		StatusWndText( msg..": "..url )
	else
		StatusWndText( "Unknown message of id: "..tostring(filter) )	
	end

	return KEP.EPR_CONSUMED
end 

function ProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    if (filter == PROGRESS_CANCEL) then 
        Log("ProgressEventHandler: PROGRESS_CANCEL - Canceling Patch")
		KEP_CancelPatch();
        MenuCloseThis()
    elseif( filter == PROGRESS_ERROR) then
        Log("ProgressEventHandler: PROGRESS_ERROR - Canceling Patch")
		KEP_CancelPatch();
    end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "PatchProgressEventHandler", "PatchProgressEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "ProgressEventHandler", "ProgressEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "PatchProgressEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ProgressEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

    ProgressOptCancel("PatchProgress", PROGRESS_OPT_CANCEL_NO) -- DRF - TODO - Not Working
    ProgressOpen("PatchProgress")
    ProgressOptCancel("PatchProgress_Patching", PROGRESS_OPT_CANCEL_NO) -- DRF - TODO - Not Working
    ProgressOpen("PatchProgress_Patching")
end

function Dialog_OnDestroy(dialogHandle)
    ProgressClose("PatchProgress_Patching")
    ProgressClose("PatchProgress")
 	Helper_Dialog_OnDestroy( dialogHandle )
end
