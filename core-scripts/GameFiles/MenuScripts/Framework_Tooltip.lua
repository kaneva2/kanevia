--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\Framework_InventoryHelper.lua")

-- background on moreinfo - imgMoreInfoBG
-- text on moreinfo - stcMoreInfo (control)
-- img for info - imgMoreInfoInWorld

POINTER_OFFSET_X_RIGHT 	= 17
POINTER_OFFSET_X_LEFT 	= 6
POINTER_OFFSET_Y	= 18
WINDOW_BUFFER		= 10
HUD_HEIGHT			= 40

TOOLTIP_BORDER = 10
TEXT_PADDING_Y = 8
TEXT_PADDING_X = 10

local LOOT_ICON_COORDS = {
	[ITEM_TYPES.WEAPON] 		= {left = 354, right = 386, top = 59, bottom = 91},
	[ITEM_TYPES.ARMOR ] 		= {left = 418, right = 450, top = 59, bottom = 91},
	[ITEM_TYPES.TOOL ] 			= {left = 354, right = 386, top = 91, bottom = 122},
	[ITEM_TYPES.AMMO ]			= {left = 546, right = 578, top = 59, bottom = 91},
	[ITEM_TYPES.CONSUMABLE ] 	= {left = 386, right = 418, top = 59, bottom = 91},
	[ITEM_TYPES.GENERIC ] 		= {left = 610, right = 642, top = 59, bottom = 91},
	[ITEM_TYPES.PET ] 			= {left = 642, right = 674, top = 59, bottom = 91},
	[ITEM_TYPES.PLACEABLE ] 	= {left = 450, right = 482, top = 59, bottom = 91},
	[ITEM_TYPES.HARVESTABLE ] 	= {left = 578, right = 610, top = 59, bottom = 91},
	[ITEM_TYPES.RECIPE ] 		= {left = 514, right = 546, top = 59, bottom = 91},
	[ITEM_TYPES.BLUEPRINT ] 	= {left = 482, right = 514, top = 59, bottom = 91},
	[ITEM_TYPES.QUEST ] 		= {left = 386, right = 418, top = 91, bottom = 122},
	[ITEM_TYPES.ARMOR ] 		= {left = 418, right = 450, top = 59, bottom = 91},
	[ITEM_TYPES.GEMBOX ] 		= {left = 610, right = 642, top = 59, bottom = 91},
}

local ITEM_LEVEL_COORDS = {
							{left = 0, right = 15, top = 146, bottom = 161},
							{left = 16, right = 31, top = 146, bottom = 161},
							{left = 32, right = 47, top = 146, bottom = 161},
							{left = 0, right = 15, top = 162, bottom = 177},
							{left = 16, right = 31, top = 162, bottom = 177}
}

--FIELD_ORDER = {"header", "subHeader", "body", "durability", "moreInfo", "footer1", "footer2", "footer3"}
FIELD_ORDER = {"header", "body", "brokenItem", "damage", "uses", "healing", "armorrating", "energy", "health", "range", "requires1", "requires2", "level", "reward", "armorslot", "requires", "durability"}

FIELD_MAP = {
			header			= "stcHeader",
			body			= "stcBody",
			brokenItem 		= "stcBrokenItem",
			damage 			= "stcDamage",
			uses			= "stcUses",
			healing 		= "stcHealing",
			armorrating 	= "stcArmorRating",
			energy			= "stcEnergyBonus",
			health 			= "stcHealth",
			requires1 		= "stcRequires1",
			requires2		= "stcRequires2",
			level			= "stcLevel",
			reward 			= "stcReward",
			range			= "stcRange",
			armorslot		= "stcArmorSlot",
			requires		= "stcRequires",
			durability 		= "stcDurability"
			}




-- items under the first line
SECTION1_MAP = {
	damage 		= "stcDamage",
	uses 		= "stcUses",
	healing 	= "stcHealing",
	armorrating = "stcArmorRating",
	energy		= "stcEnergyBonus",
	health 		= "stcHealth",
	range		= "stcRange",
	requires1 	= "stcRequires1",
	requires2	= "stcRequires2",
	reward 		= "stcReward"
}

-- items under the second line 
SECTION2_MAP = {
	armorslot		= "stcArmorSlot",
	requires		= "stcRequires",
	durability 		= "stcDurability"
}

MORE_INFO_ORDER = { "sell", "brokenItem", "moreInfo", "loot", "weapon", "recipe" }

MORE_INFO_MAP = {
	sell = "stcMoreInfoBuy",
	moreInfo = "stcMoreInfo",
	loot = "stcLoot",
	weapon = "stcWeapon",
	recipe = "stcRecipe",
	brokenItem = "stcBrokenItem"
}

MORE_INFO_IMG = {
	sell = "imgMoreInfoBuy",
	moreInfo = "imgMoreInfoIcon",
	loot = "imgLoot",
	weapon = "imgWeapon",
	recipe = "imgRecipe",
	brokenItem = "imgBrokenItem"
}

-- Left, top, right, bottom
local DURABILITY_COLORS = {GREEN  = {19, 179, 20, 185},
						   YELLOW = {31, 179, 32, 185},
						   RED 	  = {25, 179, 26, 185}
						  }

local DURABILITY_BAR_WIDTH = 80

g_parentMenuName = nil

g_mousePositionX = 0
g_mousePositionY = 0

g_emptyTooltip = true -- tooltip is empty until UpdateFrameworkTooltipEvent()

g_tooltip = {}

-- Menu Location Parameters
g_sideChosen = false
g_dialogOffsetX = nil
g_dialogOffsetY = nil
g_dialogMinHeight = 0

-- Fade Enumeration
FADE_NO			= 0
FADE_IN 		= 1
FADE_OUT 		= -1
g_fadeDirection = FADE_NO

-- Fade Paramerters
FADE_SECONDS 	= .25
MAX_ALPHA 		= 255
g_alphaLevel 	= 0
g_timer 		= 0

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "updateFrameworkTooltipEventHandler", "UpdateFrameworkTooltipEvent", KEP.MED_PRIO ) 
	KEP_EventRegisterHandler( "closeFrameworkTooltipEventHandler", "CloseFrameworkTooltipEvent", KEP.MED_PRIO ) 
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "UpdateFrameworkTooltipEvent", KEP.MED_PRIO )
	KEP_EventRegister( "CloseFrameworkTooltipEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	local dispElement = Dialog_GetDisplayElement(dialogHandle, "backgroundDisplay")
	g_dialogMinHeight = Element_GetSliceTop(dispElement) + Element_GetSliceBottom(dispElement)

	-- Tooltip Cannot Be Clicked On
	MenuSetPassthroughThis(true)

	-- Begin With Tooltip Minimized
	minimizeTooltip()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnRender(dialogHandle, fTimeElapsed)
	-- Minimize On Parent Menu Close
	local parentOk = MenuNameOk(g_parentMenuName)
	local parentOpen = parentOk and MenuIsOpen(g_parentMenuName)
	if not parentOpen then 
		if parentOk then 
			minimizeTooltip()
		end
		return
	end

	if g_readyToShowTooltip then
		if not g_tooltipVisible then
			g_timer = g_timer + fTimeElapsed
			if g_timer >= FADE_SECONDS then
				setFade(255)
				formatTooltip()
				g_tooltipVisible = true
			else
				setFade(0)
			end
		end
	end
end

function easeIn(time, change, duration)
	time = time/duration;
	time = time - 1
	return change*(time*time*time + 1)
end

function setFade(alphaLevel)
	if alphaLevel <= 1 then
		alphaLevel = 1 -- Zero makes it white...
	elseif alphaLevel > MAX_ALPHA then
		alphaLevel = MAX_ALPHA
	end

	local textureColor = Element_GetTextureColor(Dialog_GetDisplayElement(gDialogHandle, "backgroundDisplay"))
	local color = BlendColor_GetColor(textureColor, ControlStates.DXUT_STATE_NORMAL)
	color.a = alphaLevel
	BlendColor_SetColor(textureColor, ControlStates.DXUT_STATE_NORMAL, color)
	local barColor = {r = 255, g = 255, b = 255, a = alphaLevel}
	
	-- Fade durability bars
	BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgDurabilityBG"])), 0, barColor)
	BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgDurabilityBar"])), 0, barColor)
	BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgDurabilityGradiant"])), 0, barColor)

	-- Fade uses bars
	BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgUsesBG"])), 0, barColor)
	BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgUsesBar"])), 0, barColor)
	BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgUsesGradiant"])), 0, barColor)	

	for name, handle in pairs(gHandles) do
		local fontColor = Element_GetFontColor(Static_GetDisplayElement(handle))
		local color = BlendColor_GetColor(fontColor, ControlStates.DXUT_STATE_NORMAL)
		color.a = alphaLevel
		BlendColor_SetColor(fontColor, ControlStates.DXUT_STATE_NORMAL, color)	
	end
end

-- Reset Tooltip Fade & State & Minimize It
function minimizeTooltip()
	-- Reset Fade
	setFade(0)
	
	-- Reset State
	g_parentMenuName = nil
	g_emptyTooltip = true
	for i, controlName in pairs(FIELD_MAP) do
		Static_SetText(gHandles[controlName], "")
	end
	resizeFields()

	g_readyToShowTooltip = false
	g_tooltipVisible = false
	g_timer = 0

	-- Minimize Tooltip
	MenuSetMinimizedThis(true)
end

-- Choose Tooltip Side & Maximize It
function maximizeTooltip()
	-- Choose Tooltip Side & Update Location
	g_sideChosen = false
	updateTooltipLocation(Cursor_GetLocationX(), Cursor_GetLocationY())

	-- Bring To Front & Un-Minimize Tooltip
	MenuBringToFrontThis()
	MenuSetMinimizedThis(false)
end

-- Called At The End Of Fade In Either Direction From OnRender()
-- function resetFade()
	
-- 	-- Minimize Tooltip At End Of Fade Out
-- 	if (g_fadeDirection == FADE_OUT) then
-- 		minimizeTooltip()
-- 	end
	 
-- 	-- Stop Fading
-- 	g_fadeDirection = 0
-- end

-- Called To Start Fade In Either Direction
function startFade(direction)
	-- Maximize Tooltip At Start Of Fade In
	if (direction == FADE_IN) then 
		maximizeTooltip()
	end

	-- Start Fading (via OnRender())
	g_fadeDirection = direction
end

-- Update Tooltip Location On Mouse Movement (only if not empty)
function Dialog_OnMouseMove(dialogHandle, x, y)
	if (g_emptyTooltip == true) then return end
	local loc = MenuGetLocationThis()
	x = loc.x + x
	y = loc.y + y
	if g_fadeDirection >= 0 then
		updateTooltipLocation(x, y)
	end
end

function closeFrameworkTooltipEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_tooltip = {}
	minimizeTooltip()
end

function updateFrameworkTooltipEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- Extract Parent Menu Name & Mouse Location
	g_parentMenuName		= KEP_EventDecodeString(event)
	local mouseX 			= KEP_EventDecodeNumber(event)
	local mouseY 			= KEP_EventDecodeNumber(event)

	g_tooltip = cjson.decode(KEP_EventDecodeString(event))

	g_readyToShowTooltip = true
end

function formatTooltip()
	if g_tooltip then
		-- Handle each tooltip
		for i, tooltipIndex in pairs(FIELD_ORDER) do
			local controlName = FIELD_MAP[tooltipIndex]
			local tooltipInfo = g_tooltip[tooltipIndex]

			if controlName then
				local control = gHandles[controlName]
				local controlLabel = gHandles[controlName.. "Label"]
				if control then
					-- Set tooltip info
					if tooltipInfo then
						-- Apply text label
						if tooltipInfo.label then
							Static_SetText(control, tostring(tooltipInfo.label))
						end
						
						-- Set custom color?
						if tooltipInfo.color then
							if validColorTable(tooltipInfo.color) then
								BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(control)), 0, tooltipInfo.color)
							end
						-- Reset default color
						else
							local customColor = {a=255, r=255, g=255, b=255}
							BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(control)), 0, customColor)
						end
						
						-- Custom durability bar? TODO: Handled better?
						if tooltipInfo.durability and tooltipInfo.label then
							local textWidth = Static_GetStringWidth(control, tooltipInfo.label)
							
							-- Set bar size
							Control_SetSize(gHandles["imgDurabilityBar"], (tooltipInfo.durability/100) * DURABILITY_BAR_WIDTH, 7)
							Control_SetSize(gHandles["imgUsesBar"], (tooltipInfo.durability/100) * DURABILITY_BAR_WIDTH, 7)
							
							-- Set bar color
							local durColor = getDurabilityColor(tooltipInfo.durability)
							Element_SetCoords(Image_GetDisplayElement(gHandles["imgDurabilityBar"]), unpack(durColor))
						elseif tooltipInfo.label and tooltipInfo.level then
							Element_SetCoords(Image_GetDisplayElement(gHandles["imgLevel"]), ITEM_LEVEL_COORDS[tooltipInfo.level].left, ITEM_LEVEL_COORDS[tooltipInfo.level].top, ITEM_LEVEL_COORDS[tooltipInfo.level].right, ITEM_LEVEL_COORDS[tooltipInfo.level].bottom)
						end

						-- show the label if there is information in the element 
						if controlLabel then Control_SetVisible(controlLabel, true) end
					-- Cleanup existing control data
					else
						Static_SetText(control, "")
						-- hide the label if there is no information present 
						if controlLabel then Control_SetVisible(controlLabel, false) end 
					end
				end
			end
		end

		-- add the text for the buy option  
		if g_tooltip["sell"] and g_tooltip["sell"].canSell == true then
			Static_SetText(gHandles["stcMoreInfoBuy"], tostring(g_tooltip["sell"].label))
		else
			Static_SetText(gHandles["stcMoreInfoBuy"], "")
		end

		-- set the text for the more info
		if g_tooltip["moreInfo"] and g_tooltip["moreInfo"].label then
			Static_SetText(gHandles["stcMoreInfo"], tostring(g_tooltip["moreInfo"].label))
			
			-- set the image in the bottom portion of the tooltip 
			if g_tooltip["moreInfo"].GLID then
				applyImage(gHandles["imgMoreInfoIcon"], g_tooltip["moreInfo"].GLID)
			end
		else
			Static_SetText(gHandles["stcMoreInfo"], "")
		end 

		-- showing the loot 
		if g_tooltip["loot"] and g_tooltip["loot"].label then
			Static_SetText(gHandles["stcLoot"], tostring(g_tooltip["loot"].label))

			if g_tooltip["loot"].type then
				local lootType = g_tooltip["loot"].type
				Element_SetCoords(Image_GetDisplayElement(gHandles["imgLoot"]), LOOT_ICON_COORDS[lootType].left, LOOT_ICON_COORDS[lootType].top, LOOT_ICON_COORDS[lootType].right, LOOT_ICON_COORDS[lootType].bottom)
			end 
		else
			Static_SetText(gHandles["stcLoot"], "")
		end

		-- show weapon information
		if g_tooltip["weapon"] and g_tooltip["weapon"].label then
			Static_SetText(gHandles["stcWeapon"], tostring(g_tooltip["weapon"].label))
		else
			Static_SetText(gHandles["stcWeapon"], "")
		end

		-- show recipe information
		if g_tooltip["recipe"] and g_tooltip["recipe"].label then
			Static_SetText(gHandles["stcRecipe"], tostring(g_tooltip["recipe"].label))
		else
			Static_SetText(gHandles["stcRecipe"], "")
		end

		-- Resize All Fields (returns true if tooltip is empty)
		g_emptyTooltip = resizeFields()

		-- Start Fading...
		if (g_emptyTooltip == true) then
			startFade(FADE_OUT)
		else
			startFade(FADE_IN)
		end
	end
end

function formatNumberCommas(inputStr)
	if tonumber(inputStr) == nil then
		return inputStr
	end

	local formatted = inputStr
	while true do  
		formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
		if (k==0) then
			break
		end
	end
	return formatted
end

function validColorTable(table)
	if 	table["a"] and table["a"] >= 0 and
		table["r"] and table["r"] >= 0 and
		table["g"] and table["g"] >= 0 and
		table["b"] and table["b"] >= 0 then
			return true
	end
	return false
end

-- Resizes all tooltip fields returning true if empty otherwise false.
function resizeFields()
	local emptyTooltip = resizeTable()
	resizeFooters()
	resizeMoreInfo()
	return emptyTooltip
end

-- Resizes all tooltip table fields returning true if empty otherwise false.
function resizeTable()
	-- Resize Body
	local bodyHandle = gHandles["stcBody"]
	local bodyHeight = Static_GetStringHeight(bodyHandle, Static_GetText(bodyHandle))
	Control_SetSize(bodyHandle, Control_GetWidth(bodyHandle), bodyHeight)

	-- set the location of the first line 
	local firstLinePosition = Control_GetLocationY(bodyHandle) + bodyHeight + TEXT_PADDING_Y
	Control_SetLocationY(gHandles["imgLine1"], firstLinePosition)

	-- will hold the location of elements 
	local yLocation = bodyHeight or TOOLTIP_BORDER

	--log("--- yLocation 1 ".. tostring(yLocation))

	local lastField = nil

	-- going to determine which lines need to display 
	local line1IsVisible = false
	local line2IsVisible = false

	-- location of items in the first section 
	local firstSectionPosition = firstLinePosition + Control_GetHeight(gHandles["imgLine1"]) + TOOLTIP_BORDER
	-- location of items in the second section 
	local secondSectionPosition = firstSectionPosition

	for i, tooltipIndex in pairs(FIELD_ORDER) do
		local controlName = FIELD_MAP[tooltipIndex]
		-- Do we have a tooltip field for this?
		if controlName then
			local control = gHandles[controlName]
			local controlLabel = gHandles[controlName.. "Label"]
			local fieldText = Static_GetText(control)
			
			-- Hide the field if it's empty, otherwise update it's location
			if fieldText == "" then
				Control_SetVisible(control, false)
				if controlLabel then Control_SetVisible(controlLabel, false) end 

				if controlName == "stcDurability" then
					Control_SetVisible(gHandles["imgDurabilityBG"], false)
					Control_SetVisible(gHandles["imgDurabilityBar"], false)
					Control_SetVisible(gHandles["imgDurabilityGradiant"], false)
				elseif controlName == "stcUses" then
					Control_SetVisible(gHandles["imgUsesBG"], false)
					Control_SetVisible(gHandles["imgUsesBar"], false)
					Control_SetVisible(gHandles["imgUsesGradiant"], false)
				elseif controlName == "stcLevel" then
					Control_SetVisible(gHandles["imgLevel"], false)
				end
			else
				-- this logic is gonna get cray because of the way the elements need to move on the new tooltip menu

				-- turn on the element and the label, if applicable 
				Control_SetVisible(control, true)
				if controlLabel then Control_SetVisible(controlLabel, true) end 

				-- check to see if the element is considered part of section 1 or 2 
				if SECTION1_MAP[tooltipIndex] then
					line1IsVisible = true

					-- edge case that needs to be accounted for with two lines in the first section (normally only one)
					if controlName == "stcRequires2" or controlName == "stcReward" then 
						
						-- set the location of the second element below the first element 
						local secondElementLocation = Control_GetLocationY(gHandles["stcRequires1"]) + Static_GetStringHeight(gHandles["stcRequires1"], Static_GetText(gHandles["stcRequires1"])) + TEXT_PADDING_Y
						Control_SetLocationY(control, secondElementLocation)
						Control_SetLocationY(controlLabel, secondElementLocation)

						-- set the position of the second line graphic and adjust 
						secondSectionPosition = secondElementLocation + Static_GetStringHeight(control, Static_GetText(control)) + TEXT_PADDING_Y
						if not controlName == "stcReward" then
							line2IsVisible = true
						end 
					else 
						-- move the control to be below the first line 
						Control_SetLocationY(control, firstSectionPosition)
						if controlLabel then Control_SetLocationY(controlLabel, firstSectionPosition) end 

						-- set the start of the second section 
						secondSectionPosition = firstSectionPosition + Control_GetHeight(control) + TEXT_PADDING_Y
					end

					-- set the location of the second line, if needed 
					Control_SetLocationY(gHandles["imgLine2"], secondSectionPosition)

					-- update the second section position to include padding and height of line graphic 
					secondSectionPosition = secondSectionPosition + Control_GetHeight(gHandles["imgLine2"]) + TEXT_PADDING_Y

					line2IsVisible = true

				elseif SECTION2_MAP[tooltipIndex] then
					line2IsVisible = true

					-- set the location of the last field 
					if lastField == nil then 
						Control_SetLocationY(control, secondSectionPosition)
						if controlLabel then Control_SetLocationY(controlLabel, secondSectionPosition) end 
						lastField = control
					else
						local newLocationY = Control_GetLocationY(lastField) + Control_GetHeight(lastField) + TEXT_PADDING_Y
						Control_SetLocationY(control, newLocationY)
						if controlLabel then Control_SetLocationY(controlLabel, newLocationY) end 
						lastField = control
					end
				end 

				if controlName == "stcDurability" and fieldText ~= "" then
					-- location of the durability text
					local textLocation = Control_GetLocationY(control)

					-- font color 
					local color = {r = 216, g = 216, b = 216, a = 255}

					-- set if the durability bars need to be visible
					if fieldText == "0%" then
						Static_SetText(control, "BROKEN - Needs repair.")
						color = {r = 255, g = 0, b = 0, a = 255}
						Control_SetVisible(gHandles["imgDurabilityBG"], false)
						Control_SetVisible(gHandles["imgDurabilityBar"], false)
						Control_SetVisible(gHandles["imgDurabilityGradiant"], false)
						Static_SetText(gHandles["stcBrokenItem"], "You can fix broken items at a repair station.")
					else
						-- make sure the durability bar is visible 
						Control_SetVisible(gHandles["imgDurabilityBG"], true)
						Control_SetVisible(gHandles["imgDurabilityBar"], true)
						Control_SetVisible(gHandles["imgDurabilityGradiant"], true)
						Static_SetText(gHandles["stcBrokenItem"], "")
					end

					-- set the color of the font 
					printTable(color)
					BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(control)), 0, color)

					-- Set the durability bar location 
					Control_SetLocationY(gHandles["imgDurabilityBG"], textLocation + 4)
					Control_SetLocationY(gHandles["imgDurabilityBar"], textLocation + 4)
					Control_SetLocationY(gHandles["imgDurabilityGradiant"], textLocation + 4)

					-- if there is a durablity displayed, make sure to have AT LEAST one of the lines displayed 
					line2IsVisible = true

				elseif controlName == "stcUses" and fieldText ~= "" then
					local textLocation = Control_GetLocationY(control)

					-- font color 
					local color = {r = 216, g = 216, b = 216, alphaLevel = 255}

					-- set if the uses bars need to be visible
					if fieldText == "0" then
						color = {r = 255, g = 0, b = 0, alphaLevel = 255}
					end

					Control_SetVisible(gHandles["imgUsesBG"], true)
					Control_SetVisible(gHandles["imgUsesBar"], true)
					Control_SetVisible(gHandles["imgUsesGradiant"], true)

					-- set the color of the font 
					BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(control)), 0, color)

					-- Set the durability bar location 
					Control_SetLocationY(gHandles["imgUsesBG"], textLocation + 5)
					Control_SetLocationY(gHandles["imgUsesBar"], textLocation + 5)
					Control_SetLocationY(gHandles["imgUsesGradiant"], textLocation + 5)
				elseif controlName == "stcLevel" and fieldText ~= "" then
					-- make sure the level img is visible 
					Control_SetVisible(gHandles["imgLevel"], true) 
				end

				-- set the yLocation to resize the menu 
				if not MORE_INFO_MAP[tooltipIndex] then
					yLocation = Control_GetLocationY(control) + Control_GetHeight(control) + TOOLTIP_BORDER
					--log("--- ".. controlName.. " ".. tostring(yLocation))
				end
			end
		end
	end

	-- remove the section lines if needed
	Control_SetVisible(gHandles["imgLine1"], (line1IsVisible or line2IsVisible))
	Control_SetVisible(gHandles["imgLine2"], (line1IsVisible and line2IsVisible))

	-- find if the level is visible 
	local levelIsVisible = not (Static_GetText(gHandles["stcLevel"]) == "")
	local rangeIsVisible = not (Static_GetText(gHandles["stcRange"]) == "")

	-- make line 2 visible if range and level both have information
	line2IsVisible = (levelIsVisible and rangeIsVisible) or line2IsVisible

	-- set the place of the levels 
	if (line1IsVisible and line2IsVisible) and levelIsVisible then
		-- turn the second line on for safe-measure
		Control_SetVisible(gHandles["imgLine2"], true)

		local newLocationY = Control_GetLocationY(gHandles["imgLine2"]) + Control_GetHeight(gHandles["imgLine2"]) + TEXT_PADDING_Y
		Control_SetLocationY(gHandles["stcLevel"], newLocationY)
		Control_SetLocationY(gHandles["imgLevel"], newLocationY)
		
		-- make sure the height of the tooltip includes the level 
		if (Control_GetLocationY(gHandles["imgLevel"]) + Control_GetHeight(gHandles["imgLevel"]) + TEXT_PADDING_Y) > yLocation then
			yLocation = Control_GetLocationY(gHandles["imgLevel"]) + Control_GetHeight(gHandles["imgLevel"]) + TEXT_PADDING_Y
		end 
	elseif (line1IsVisible or line2IsVisible) and levelIsVisible then
		local newLocationY = Control_GetLocationY(gHandles["imgLine1"]) + Control_GetHeight(gHandles["imgLine1"]) + TEXT_PADDING_Y
		Control_SetLocationY(gHandles["stcLevel"], newLocationY)
		Control_SetLocationY(gHandles["imgLevel"], newLocationY)
		
		-- make sure the height of the tooltip includes the level 
		if (Control_GetLocationY(gHandles["imgLevel"]) + Control_GetHeight(gHandles["imgLevel"]) + TEXT_PADDING_Y) > yLocation then
			yLocation = Control_GetLocationY(gHandles["imgLevel"]) + Control_GetHeight(gHandles["imgLevel"]) + TEXT_PADDING_Y
		end 
	end 

	--log("--- yLocation 2 ".. tostring(yLocation))

	-- Is Tooltip Empty ?
	if yLocation <= TOOLTIP_BORDER then
		return true
	else
		MenuSetSizeThis(nil, yLocation)
		return false
	end
end

-- Resizes all tooltip footer fields.
function resizeFooters()
	-- Reset Footers
	
end

function resizeMoreInfo()
	-- get the height of the details background 
	local height = Dialog_GetHeight(gDialogHandle)
	local backgroundY = height - 2
	local moreInfoY = backgroundY + 4
	Control_SetLocationY(gHandles["imgMoreInfoBG"], backgroundY)
	
	-- turn off the text fields
	Control_SetVisible(gHandles["stcMoreInfo"], false)
	Control_SetVisible(gHandles["stcMoreInfoBuy"], false)
	Control_SetVisible(gHandles["stcLoot"], false)
	Control_SetVisible(gHandles["stcWeapon"], false)
	Control_SetVisible(gHandles["stcRecipe"], false)
	Control_SetVisible(gHandles["stcBrokenItem"], false)

	-- turn off all the images in the secondary information 
	Control_SetVisible(gHandles["imgMoreInfoBG"], false)
	Control_SetVisible(gHandles["imgMoreInfoInWorld"], false)
	Control_SetVisible(gHandles["imgMoreInfoUnique"], false)
	Control_SetVisible(gHandles["imgMoreInfoIcon"], false)
	Control_SetVisible(gHandles["imgMoreInfoBuy"], false)
	Control_SetVisible(gHandles["imgLoot"], false)
	Control_SetVisible(gHandles["imgWeapon"], false)
	Control_SetVisible(gHandles["imgRecipe"], false)
	Control_SetVisible(gHandles["imgBrokenItem"], false)

	for i, tooltipIndex in pairs(MORE_INFO_ORDER) do
		local controlName = MORE_INFO_MAP[tooltipIndex]
		local controlImgName = MORE_INFO_IMG[tooltipIndex]

		local control = gHandles[controlName]
		local controlImg = gHandles[controlImgName]

		-- get the buy information
		local fieldText = Static_GetText(control)
	
		-- buy overrides other information 
		if not (fieldText == "") then 
			-- make sure the text is visible 
			Control_SetVisible(control, true)
			Control_SetLocationY(control, moreInfoY)

			-- make the background visible 
			Control_SetVisible(gHandles["imgMoreInfoBG"], true)

			-- set the location of the buy graphic and make it visible 
			if fieldText == "This item is unique to this world" then
				Control_SetLocationY(gHandles["imgMoreInfoUnique"], moreInfoY)
				Control_SetVisible(gHandles["imgMoreInfoUnique"], true)
			elseif fieldText == "This item is in the world" then
				Control_SetLocationY(gHandles["imgMoreInfoInWorld"], moreInfoY)
				Control_SetVisible(gHandles["imgMoreInfoInWorld"], true)
			else	
				Control_SetVisible(controlImg, true)
				Control_SetLocationY(controlImg, moreInfoY)
			end

			-- don't want to show any of the other texts 
			return
		end 
	end 
end

function updateTooltipLocation(mouseX, mouseY)
	g_mousePositionX = mouseX
	g_mousePositionY = mouseY
	
	-- Check x,y against the edges of the main window
	local winW, winH = KEP_GetClientRectSize()
	local size = MenuGetSizeThis()
	local dialogWidth = size.width
	local dialogHeight = size.height
	
	-- Choose Tooltip Side ? (stay within window)
	if not g_sideChosen then
		-- If the mouse is over the dialog on the X, set to left side
		if g_mousePositionX + POINTER_OFFSET_X_RIGHT + dialogWidth + WINDOW_BUFFER > winW then
			g_dialogOffsetX = 0 - POINTER_OFFSET_X_LEFT - dialogWidth
		else
			g_dialogOffsetX = POINTER_OFFSET_X_RIGHT
		end

		-- If mouse is over the dialog on the Y, set to above
		if g_mousePositionY + POINTER_OFFSET_Y + dialogHeight + WINDOW_BUFFER > (winH + HUD_HEIGHT) then
			g_dialogOffsetY = (0 - POINTER_OFFSET_Y - dialogHeight)/3 -- Divide offset by 2 because pointer location returns slightly above
		else
			g_dialogOffsetY = POINTER_OFFSET_Y
		end

		g_sideChosen = true
	end

	-- Set Dialog + Strings into position and size
	local x = math.min((g_mousePositionX + g_dialogOffsetX), (winW - dialogWidth))
	local y = math.min((g_mousePositionY + g_dialogOffsetY), (winH - dialogHeight))
	MenuSetLocationThis(x, y)
end

-- Returns the color for the durability bar based on percentage passed in
function getDurabilityColor(durabilityPercentage)
	-- Green bar
	if durabilityPercentage >= 50 then
		return DURABILITY_COLORS.GREEN
	-- Return yellow
	elseif durabilityPercentage >= 15 then
		return DURABILITY_COLORS.YELLOW
	-- Return red
	else
		return DURABILITY_COLORS.RED
	end
end

function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end