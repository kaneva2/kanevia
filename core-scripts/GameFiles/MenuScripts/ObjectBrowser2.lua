--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\MathHelper.lua")

ITEM_SELECTED_EVENT = "SelectedItemInfoEvent"
ENTER_BUILD_MODE_EVENT = "EnterBuildModeEvent"
DYNAMIC_OBJECT_INFO_EVENT = "DynamicObjectInfoEvent"
ADD_DYNAMIC_OBJECT_EVENT = "AddDynamicObjectEvent"
REMOVE_DYNAMIC_OBJECT_EVENT = "RemoveDynamicObjectEvent"
SCRIPT_SERVER_EVENT = "ScriptServerEvent"

g_listhandles = {}
g_listscrollbars = {}

g_objectCount = 0
g_objects = {}
g_objectsFind = {} -- drf - added 
g_ignoreSelection = false
g_selectCount = 0

g_queryString = ""

--Sorting
Sort_Name = 1
Sort_PID = 2
Sort_Type = 3
Sort_Animation = 4
Sort_Particle = 5
Sort_Complexity = 6

g_bSortAscending = true
g_SortingBy = Sort_Name
g_SortFuncs = {} --filled in in after the sort functions are declared

g_pageSize = 21
g_currentPage = 1
g_totalPages = 1
g_startIdx = 1

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "selectEventHandler", "SelectEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandlerV( "removeDynamicObjectEventHandler", REMOVE_DYNAMIC_OBJECT_EVENT, KEP.LOW_PRIO )
	KEP_EventRegisterHandlerV( "addDynamicObjectEventHandler", ADD_DYNAMIC_OBJECT_EVENT, KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.LOW_PRIO )
	KEP_EventRegisterHandler("objectsUpdatedEventHandler", "ObjectsUpdatedEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( DYNAMIC_OBJECT_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterSecureV( ADD_DYNAMIC_OBJECT_EVENT, KEP.HIGH_PRIO, KEP.ESEC_SERVER, KEP.ESEC_CLIENT )
	KEP_EventRegisterSecureV( REMOVE_DYNAMIC_OBJECT_EVENT, KEP.HIGH_PRIO, KEP.ESEC_SERVER + KEP.ESEC_CLIENT, KEP.ESEC_SERVER + KEP.ESEC_CLIENT )
	KEP_EventRegister("ObjectsUpdatedEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	ListBox_SetHTMLEnabled(gHandles["lbxName"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxType"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxAnimation"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxParticle"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxPos"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxComplexity"], true)
	
	g_listhandles = 
	{
		gHandles["lbxName"],
		gHandles["lbxType"],
		gHandles["lbxAnimation"],
		gHandles["lbxParticle"],
		gHandles["lbxPos"],
		gHandles["lbxComplexity"],
	}
	
	Dialog_SetCaptionText(gDialogHandle, "Object Browser")

	g_listscrollbars = {}
	for i,v in ipairs(g_listhandles) do
		table.insert(g_listscrollbars, ListBox_GetScrollBar(v))
	end

	updateObjects()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function objectsUpdatedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	Log("objectsUpdateEventHandler: Calling updateObjects()")
	updateObjects()
end

function removeDynamicObjectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	Log("removeDynamicObjectEventHandler: Calling updateObjects()")
	updateObjects()
end

function addDynamicObjectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	Log("addDynamicObjectEventHandler: Calling updateObjects()")
	updateObjects()	
end

function selectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	Log("selectEventHandler: selectCount="..g_selectCount.." - Calling UpdateList()")
	g_selectCount = g_selectCount - 1
	updateObjects()	
	MenuClearFocusThis()
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.OBJECT_BROWSER_INFO then
		local xml = KEP_EventDecodeString(event)
		ParseInfoResults(xml)
	end
end

function ParseInfoResults( browserString )
	local result = nil  -- result desc
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(browserString, "<ReturnCode>(%d+)</ReturnCode>")	
	if result == "0" then
		s, e, numRec = string.find(browserString, "<NumberRecords>(%d+)</NumberRecords>", e)
		numRec = tonumber(numRec)
		if numRec > 0 then
			for i = 1, numRec do
				s, e, globalID = string.find(browserString, "<global_id>(%d+)</global_id>", e)
				globalID = tonumber(globalID)
				s, e, name = string.find(browserString, "<name>(.-)</name>", e)
				s, e, inventoryType = string.find(browserString, "<inventory_type>(%d+)</inventory_type>", e)
				inventoryType = tonumber(inventoryType)
				if inventoryType ~= GameGlobals.IT_BOTH then
					g_inventoryType = inventoryType
				end
				for i,v in ipairs(g_objectsFind) do
					if globalID == v.particleGlid then
						v.particleName = name
					elseif globalID == v.animationGlid then
						v.animationName = name
					end
				end
			end
		end
	end

	populateList()
end

function updateObjects()
	
	-- Get All Objects (DOs + Sounds)
	local numDOs = KEP_GetDynamicObjectCount()
	local sounds = { KEP_GetSoundPlacementIdList() }
	local numSounds = #sounds
	Log("updateObjects: numDOs="..numDOs.." numSounds="..numSounds)

	-- Add DOs To Objects Table
	g_objects = {}
	for i = 1, numDOs do
		local objId = KEP_GetDynamicObjectPlacementIdByPos(i - 1)
		if (objId > 0) then -- drf - no local placements
			local obj = ObjectGetInfo(objId, ObjectType.DYNAMIC)
			if obj.exists then
				obj.typeName = "Object"
				obj.pos.x = round(obj.pos.x, 1)
				obj.pos.y = round(obj.pos.y, 1)
				obj.pos.z = round(obj.pos.z, 1)
				table.insert(g_objects, obj)
			end
		end
	end
    
    -- Add Sounds To Objects Table
	for i = 1, numSounds do     
        local objId = sounds[i]
		if (objId > 0) then -- drf - no local placements
			local obj = ObjectGetInfo(objId, ObjectType.SOUND)
			if obj.exists then
				obj.name = obj.name
				obj.typeName = "Sound"
				obj.pos.x = round(obj.pos.x, 1)
				obj.pos.y = round(obj.pos.y, 1)
				obj.pos.z = round(obj.pos.z, 1)
				table.insert(g_objects, obj)
			end
		end
    end

	-- Find Query Objects Only
	updateObjectsFind()

	sortObjectsFind(g_SortingBy)
	getParticleAnimationInfo()	
end

function updateObjectsFind()
	if g_queryString == nil then
		g_queryString = ""
	end

	g_objectsFind = {}
	for i = 1, #g_objects do
	 	local obj = g_objects[i]
		local findName = string.find(string.lower(obj.name), string.lower(g_queryString))
		local findType = string.find(string.lower(obj.typeName), string.lower(g_queryString))
		local findSing = (obj.name == "Singularity")
		if not findSing and (findName ~= nil or findType ~= nil) then
				
			-- Set Object Color And Display Name
			obj.fontColor = "FF000000"
			obj.displayName = obj.name.." ["..obj.id.."]"

			-- Insert Into List of Objects Found
			table.insert(g_objectsFind, obj)
		end
	end

	Log("updateObjectsFind: query='"..g_queryString.."' objects="..#g_objects.." objectsFind="..#g_objectsFind)
end

function getParticleAnimationInfo()
	
	-- Attach Particle Info To Objects Found
	local glids = ""
	for i,v in ipairs(g_objectsFind) do
		if v.animationGlid > 0 then
			glids = glids .. v.animationGlid .. ","
		end
		if v.particleGlid > 0 then
			glids = glids .. v.particleGlid .. ","
		end
	end
	if glids == "" then return end
	
	-- Perform Webcall To Get Information
	glids = string.sub(glids, 1, -2)
	local url = GameGlobals.WEB_SITE_PREFIX.."kgp/instantBuy.aspx?operation=info&items=" .. glids
	makeWebCall( url, WF.OBJECT_BROWSER_INFO)
end

function updateSelection()
	local count = ObjectsSelected()
	for i,h in ipairs(g_listhandles) do
		ListBox_ClearSelection(h)
	end
	deselectObject() -- drf - added
	for i = 1, count do
		local obj = ObjectSelected(i)
		local result = ListBox_FindItem(gHandles["lbxName"], obj.id)
		if result ~= -1 then
			g_ignoreSelection = true
			ListBox_SelectItem(gHandles["lbxName"], result)
			selectObject(obj.id, true)
		end
	end
	syncAllLists( g_listhandles, gHandles["lbxName"] )
end

function deselectObject()
	SetControlEnabled("btnAnimation", false)
	SetControlEnabled("btnParticle", false)
	SetControlEnabled("btnGoto", false)
end

function selectObject(objId, noHighlight)
	
	local showParticle = false
	local showAnimation = false
	local showGoto = false

	-- Get Object From Find List
	local obj = getObjectFind(objId)
	if obj ~= nil then
		Log("selectObject: "..ObjectToString(obj.id, obj.type))

		showParticle = true
		showAnimation = true
		showGoto = true

		if obj.type ~= ObjectType.NONE and noHighlight ~= true then
			KEP_SelectObject(obj.type, obj.id)
			g_selectCount = g_selectCount + 1
		end
	end
	
	SetControlEnabled("btnAnimation", showAnimation)
	SetControlEnabled("btnParticle", showParticle)
	SetControlEnabled("btnGoto", showGoto)
end

function updatePageNumber()
	
	g_currentPage = math.floor((g_startIdx - 1) / g_pageSize) + 1
	g_totalPages = math.ceil(#g_objectsFind / g_pageSize)
	
	-- Sanity Check
	if (g_currentPage < 1) or (g_currentPage > g_totalPages) then
		g_currentPage = 1
		g_startIdx = 1
	end
	
	local pageNumberTxt = "(" .. g_currentPage .. "/" .. g_totalPages .. ")"
	Static_SetText(gHandles["stcPageNumber"], pageNumberTxt)
	Control_SetEnabled(gHandles["btnPageUp"], g_currentPage < g_totalPages)
	Control_SetEnabled(gHandles["btnPageDown"], g_currentPage > 1)
end

function populateList()
	
	for i,v in ipairs(g_listhandles) do
		ListBox_RemoveAllItems(v)
	end

	updatePageNumber()
	
	-- DRF - Determine Objects Appearing On This Page
	local objects = #g_objectsFind
	local endIdx = g_startIdx + g_pageSize - 1
	if (endIdx > objects) then
		endIdx = objects
	end

	Log("populateList: objectsFind="..objects.." startIdx="..g_startIdx.." endIdx="..endIdx)

	for i = g_startIdx, endIdx do
		local obj = g_objectsFind[i]
		local fontColor = obj.fontColor		
		local displayName = obj.displayName				
		ListBox_AddItem(gHandles["lbxName"], setColor( displayName, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxType"], setColor( obj.typeName, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxAnimation"], setColor( obj.animationName, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxParticle"], setColor( obj.particleName, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxPos"], setColor( obj.pos.x..", "..obj.pos.y..", "..obj.pos.z, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxComplexity"], setColor( "", fontColor), obj.id)
	end

	--Update the column header images
	local btnHandles = {gHandles["colName"], gHandles["colType"], gHandles["colAnimation"], gHandles["colParticle"], gHandles["colPos"], gHandles["colComplexity"]}
	local sBlankTexture = "button9_LGray.tga"
	for i,h in pairs(btnHandles) do
		Element_AddTexture( Control_GetDisplayElement(h,    "normalDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h, "mouseOverDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,   "focusedDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,   "pressedDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,  "disabledDisplay"), gDialogHandle, sBlankTexture)
	end

	local sArrowTexture = "button9_LGrayUpArrow.tga"
	if g_bSortAscending then
		sArrowTexture = "button9_LGrayDownArrow.tga"
	end

	local btnHandle = btnHandles[g_SortingBy]
	if btnHandle ~= nil then
		Element_AddTexture( Control_GetDisplayElement(btnHandle,    "normalDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle, "mouseOverDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,   "focusedDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,   "pressedDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,  "disabledDisplay"), gDialogHandle, sArrowTexture)
	end

	updateSelection()
end

function setColor( text, color )
	return "<font color=" .. color .. ">" .. text .. "</font>"
end

function getObjectFind(objId)
    for i = 1, #g_objectsFind do
        if g_objectsFind[i].id == objId then
            return g_objectsFind[i]
        end
    end
	return nil
end

function getObjectFindType(objId)
    for i = 1, #g_objectsFind do
        if g_objectsFind[i].id == objId then
            return g_objectsFind[i].type
        end
    end
    return ObjectType.NONE
end

function getObjectFindSelected()
	local objId = ListBox_GetSelectedItemData(gHandles["lbxName"])
	for i,v in ipairs(g_objectsFind) do
		if v.id == objId then
			return v
		end
	end
end

function btnPageUp_OnButtonClicked(handler)
	local objects = #g_objectsFind
	local nextIdx = g_startIdx + g_pageSize
	if (nextIdx <= objects) then
		g_startIdx = nextIdx
		populateList()
	end
end

function btnPageDown_OnButtonClicked(handle)
	if (g_startIdx > 1) then
		local prevIdx = g_startIdx - g_pageSize
		if (prevIdx < 1) then
			prevIdx = 1
		end
		g_startIdx = prevIdx
		populateList()
	end
end

function lbxName_OnListBoxSelection(lbxHandle, bFromKeyboard, bMouseDown, sel_index)
	if g_ignoreSelection == false and ((bMouseDown == 1 or bFromKeyboard == 1) or g_bSyncAllLists == 1) then
		g_selectCount = 1
		KEP_ClearSelection()
		
		if ListBox_GetMultiSelection(gHandles["lbxName"]) == true then
			local index = ListBox_GetMultiSelectedItemIndex(gHandles["lbxName"], -1)
			while index ~= -1 do
				selectObject(index)
				index = ListBox_GetMultiSelectedItemIndex(gHandles["lbxName"], index)
			end
		else
			local index = ListBox_GetSelectedItemData(gHandles["lbxName"])
			selectObject(index)
		end
		
		if g_bSyncAllLists == 0 then
			syncAllLists( g_listhandles, lbxHandle )
		end
	end
	g_ignoreSelection = false
end

function menuSelected(lbxHandle, bFromKeyboard, bMouseDown)
	OpenListBoxPlaceableObj(lbxHandle)
	if g_ignoreSelection == false  and ((bMouseDown == 1 or bFromKeyboard == 1) or g_bSyncAllLists == 1) then
		if g_bSyncAllLists == 0 then
			syncAllLists( g_listhandles, lbxHandle )
		end
	end
end

function lbxName_OnListBoxSelection(lbxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(lbxHandle, bFromKeyboard, bMouseDown)
end

function lbxType_OnListBoxSelection(lbxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(lbxHandle, bFromKeyboard, bMouseDown)
end

function lbxAnimation_OnListBoxSelection(lbxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(lbxHandle, bFromKeyboard, bMouseDown)
end

function lbxParticle_OnListBoxSelection(lbxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(lbxHandle, bFromKeyboard, bMouseDown)
end

function lbxPos_OnListBoxSelection(lbxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(lbxHandle, bFromKeyboard, bMouseDown)
end

function lbxComplexity_OnListBoxSelection(lbxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(lbxHandle, bFromKeyboard, bMouseDown)
end

function scrollBarChanged(lbxHandle)
	if g_bSyncAllLists == 0 then
		syncAllScrollBars( g_listhandles, lbxHandle )
	end
end

function lbxName_OnScrollBarChanged(scrollBarHandle, lbxHandle, page_size, track_pos)
	scrollBarChanged(lbxHandle)
end

function lbxType_OnScrollBarChanged(scrollBarHandle, lbxHandle, page_size, track_pos)
	scrollBarChanged(lbxHandle)
end

function lbxAnimation_OnScrollBarChanged(scrollBarHandle, lbxHandle, page_size, track_pos)
	scrollBarChanged(lbxHandle)
end

function lbxParticle_OnScrollBarChanged(scrollBarHandle, lbxHandle, page_size, track_pos)
	scrollBarChanged(lbxHandle)
end

function lbxPos_OnScrollBarChanged(scrollBarHandle, lbxHandle, page_size, track_pos)
	scrollBarChanged(lbxHandle)
end

function lbxComplexity_OnScrollBarChanged(scrollBarHandle, lbxHandle, page_size, track_pos)
	scrollBarChanged(lbxHandle)
end

function OpenListBoxPlaceableObj(lbxHandle)
	local objId = ListBox_GetSelectedItemData(lbxHandle)
	local objType = getObjectFindType(objId)
	if objId ~= nil and objId ~= 0 then
		Log("OpenListBoxPlaceableObj: "..ObjectToString(objId, objType).." - Opening PlaceableObj Menu...")
		MenuOpen("PlaceableObj.xml")
		local e = KEP_EventCreate( DYNAMIC_OBJECT_INFO_EVENT )
		KEP_EventEncodeNumber(e, objId)
		KEP_EventEncodeNumber(e, objType)
		KEP_EventQueue(e)
	end
end

function btnGoto_OnButtonClicked( buttonHandle )
	local obj = getObjectFindSelected()
	if obj == nil then return end
	Log("GotoClicked: "..ObjectToString(obj.id, obj.type))
	KEP_SetPlayerPosition(obj.pos.x + .01, obj.pos.y + 1, obj.pos.z)
end

function btnParticle_OnButtonClicked( buttonHandle )
	local obj = getObjectFindSelected()
	if obj == nil then return end
	MenuOpen("ParticleBrowser.xml")
end

function setAnimation(obj)
	MenuOpen("ChooseAnimation.xml")
	local e = KEP_EventCreate( ITEM_SELECTED_EVENT )
	KEP_EventEncodeNumber(e, obj.invType )
	KEP_EventEncodeNumber(e, 1) -- num items
	KEP_EventEncodeNumber(e, obj.glid)
	KEP_EventEncodeNumber(e, obj.id)
	KEP_EventEncodeNumber(e, 1) --quantity of item
	KEP_EventEncodeString(e, obj.name ) -- name
	KEP_EventEncodeNumber(e, 0 ) -- price
	KEP_EventQueue(e)
end

function btnAnimation_OnButtonClicked( buttonHandle )
	local obj = getObjectFindSelected()
	if obj == nil then return end
	setAnimation(obj)
end

function editSearch_OnEditBoxChange( editBoxHandle )
	g_queryString = EditBox_GetText( editBoxHandle )
	if (g_queryString ~= nil) and (string.len(g_queryString)>= 0) then
		updateObjects()
	end
end

function editSearch_OnEditBoxString( editBoxHandle )
	MenuClearFocusThis()
end

function colName_OnButtonClicked( buttonHandle )
	if Sort_Name == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_Name)
end

function colType_OnButtonClicked( buttonHandle )
	if Sort_Type == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_Type)
end

function colAnimation_OnButtonClicked( buttonHandle )
	if Sort_Animation == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_Animation)
end

function btnParticle_OnButtonClicked( buttonHandle )
	if Sort_Particle == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_Particle)
end

function btnComplexity_OnButtonClicked( buttonHandle )
	if Sort_Complexity == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_Complexity)
end

function sortObjectsFind(sortType)	
	g_SortingBy = sortType
	table.sort(g_objectsFind, g_SortFuncs[sortType])	
	populateList()
end

function sortByName(itemA, itemB)
	local aName = string.lower(itemA.name)
	local bName = string.lower(itemB.name)
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	else
		if itemA.id == itemB.id then --lua compares the same script sometimes. This also catches the recursion when sortByObjectID() calls sortByScriptName().
			return false
		end
		return sortByPID(itemA, itemB)
	end
end

function sortByPID(itemA, itemB)
	if g_bSortAscending then
		return itemA.id < itemB.id
	else
		return itemA.id > itemB.id
	end
end

function sortByType(itemA, itemB)
	local aName = string.lower(itemA.typeName)
	local bName = string.lower(itemB.typeName)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	end
end

function sortByAnimation(itemA, itemB)
	local aName = string.lower(itemA.animationName)
	local bName = string.lower(itemB.animationName)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	end
end

function sortByParticle(itemA, itemB)
	local aName = string.lower(itemA.particleName)
	local bName = string.lower(itemB.particleName)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	else
		return sortByName(itemA, itemB)
	end
end

function sortByComplexity(itemA, itemB)
	local aName = string.lower(itemA.complexity)
	local bName = string.lower(itemB.complexity)
	if aName ~= bName then
		if g_bSortAscending then
			return tonumber(aName) < tonumber(bName)
		else
			return tonumber(aName) > tonumber(bName)
		end
	else
		return sortByName(itemA, itemB)
	end
end

g_SortFuncs =	
{
	sortByName,
	sortByPID,
	sortByType,
	sortByAnimation,
	sortByParticle,
	sortByComplexity
}

