--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

DYNAMIC_OBJECT_INFO_EVENT = "DynamicObjectInfoEvent"

g_objectCount = 0
g_objects = {}
g_listbox = nil
g_ignoreSelection = false
g_selectCount = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "selectEventHandler", "SelectEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( DYNAMIC_OBJECT_INFO_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	g_listbox = Dialog_GetControl( gDialogHandle, "lstObjects" )
	ListBox_SetHTMLEnabled(g_listbox, true)

	updateList()

	-- setup look of listbox scrollbars
	local lbh = Dialog_GetListBox(dialogHandle, "lstObjects")
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then
			local eh = ScrollBar_GetTrackDisplayElement(sbh)

			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end
end

function selectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if g_selectCount <= 0 then
		updateSelection()
	end
	g_selectCount = g_selectCount - 1
end

function updateList()
	g_objects = {}
	local doCount = KEP_GetDynamicObjectCount()
	for i = 0, (doCount - 1) do
		local dynObj = {}
		dynObj.placementId = KEP_GetDynamicObjectPlacementIdByPos(i)
		local name, description, canPlayMovie, isAttachableObject, isInteractive, isLocal, isDerivable, assetId, friendId, customTexture, globalId, invType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(dynObj.placementId)
		local x, y, z = KEP_DynamicObjectGetPosition(dynObj.placementId)
		dynObj.name = name
		dynObj.glid = globalId
		dynObj.tv = canPlayMovie
		dynObj.game = isInteractive
		dynObj.x = x
		dynObj.y = y
		dynObj.z = z
		dynObj.type = ObjectType.DYNAMIC
		if tonumber(isLocal) == 1 then
			table.insert(g_objects, dynObj)
		end
	end

	populateList()
	updateSelection()
end

function updateSelection()
	count = KEP_GetNumSelected()
	ListBox_ClearSelection(g_listbox)
	for i = 0,count-1 do
		local type, id = KEP_GetSelection(i)
		result = ListBox_FindItem(g_listbox, id)
		if result ~= -1 then
			g_ignoreSelection = true
			ListBox_SelectItem(g_listbox, result)
		end
	end
end

function populateList()
	ListBox_RemoveAllItems(g_listbox)
	for i = 1, #g_objects do
		local dynObj = g_objects[i]
		Log("populateList(): placementId="..dynObj.placementId.." name="..dynObj.name)
		ListBox_AddItem(g_listbox, dynObj.name, dynObj.placementId)
	end
end

function getType( placementId )
	for i = 1, #g_objects do
		if g_objects[i].placementId == placementId then
			return g_objects[i].type
		end
	end
	return ObjectType.NONE
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function lstObjects_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if g_ignoreSelection == false then
		g_selectCount = 1
		KEP_ClearSelection()
		local index = ListBox_GetMultiSelectedItemIndex(Dialog_GetControl(gDialogHandle, "lstObjects"), -1)
		while index ~= -1 do
			local placementId = ListBox_GetItemData(listBoxHandle, index)
			Log("OnListBoxSelection(): index="..index.." placementId="..placementId)
			-- Bah this is not working because we can't select anything when not in build mode lame.
			KEP_SelectObject(ObjectType.DYNAMIC, placementId)
			g_selectCount = g_selectCount + 1
			index = ListBox_GetMultiSelectedItemIndex(Dialog_GetControl(gDialogHandle, "lstObjects") , index)
		end
	end
	g_ignoreSelection = false
end

function doProp()
	local lh = Dialog_GetListBox(gDialogHandle, "lstObjects")
	local index = ListBox_GetSelectedItemIndex(lh)
	local placementId = ListBox_GetItemData(lh, index)
	if placementId ~= nil and placementId ~= 0 then
		Log("doProp(): index="..index.." placementId="..placementId)
		MenuOpen("DynamicObject.xml")
		local event = KEP_EventCreate( DYNAMIC_OBJECT_INFO_EVENT )
		KEP_EventEncodeNumber(event, placementId)
		KEP_EventQueue( event )
		MenuCloseThis()
	else
		KEP_SendChatMessage( ChatType.System, "Select an unsubmitted item from the List." )
	end
end

function lstObjects_OnListBoxItemDblClick(listboxHandle, selectedIndex, selectedText)
	doProp()
end

function btnSubmit_OnButtonClicked( buttonHandle )
	doProp()
end

function btnLeave_OnButtonClicked( buttonHandle )
	for i = 1, #g_objects do
		local dynObj = g_objects[i]
		ListBox_AddItem(g_listbox, dynObj.name, dynObj.placementId)
		KEP_DeleteDynamicObjectPlacement(dynObj.placementId)
	end
	MenuCloseThis()
end
