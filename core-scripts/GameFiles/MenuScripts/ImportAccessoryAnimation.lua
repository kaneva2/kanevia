--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")
dofile("CreditBalances.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

FILE_BROWSER_CLOSED_EVENT = "FileBrowserClosedEvent"

gTarget = {}
gTarget.valid = 0
gTarget.pos = {}
gTarget.pos.x = 0
gTarget.pos.y = 0
gTarget.pos.z = 0
gTarget.normal = {}
gTarget.normal.x = 0
gTarget.normal.y = 0
gTarget.normal.z = 0
gTarget.type = ObjectType.UNKNOWN
gTarget.id = -1
gTarget.name = ""

gNextDropId = 1000000	-- a separate drop ID segment (1000000-...) DragDropEvents use (0~999999)

gImportType = UGC_TYPE.UNKNOWN
gImportTarget = nil

g_t_items = {}
g_t_acc = {}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandlerFiltered( "attribEventHandler", "AttribEvent", AttribEvent.PLAYER_INVEN, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.HIGH_PRIO+1 )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( FILE_BROWSER_CLOSED_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- disable "Next" button before user makes a selection
	Control_SetEnabled( gHandles["btnNext"], false )

	ListBox_ClearSelection( gHandles["lstAnimChoices"] )
	gImportTarget = nil

	-- load player inventory for accessory animation import
	KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function lstAnimChoices_OnListBoxSelection( listboxHandle, x, y, index, text )
	gImportTarget = ListBox_GetSelectedItemData( listboxHandle )
	Control_SetEnabled( gHandles["btnNext"], index >= 0 )
end

function btnNext_OnButtonClicked(buttonHandle)
	if gImportTarget==nil then
		KEP_MessageBox( "Please select a target for animation import" )
		return
	end
	BrowseAndImportAsync( UGC_TYPE.CHARANIM, gImportTarget )
end

function BrowseAndImportAsync( ugcType, optionalGlid )
	local event = KEP_EventCreate( FILE_BROWSER_CLOSED_EVENT )
	if optionalGlid~=nil then
		KEP_EventSetObjectId( event, optionalGlid )
	end
	KEP_EventSetFilter( event, ugcType )
	UGC_BrowseFileByImportType( ugcType, event ) -- second param (initial folder) TBD
end

function ParsePlayerInventoryItems( xmlData )
	g_t_items = {}
	decodeInventoryItemsFromWeb( g_t_items, xmlData, false )
	g_t_acc = separateArmedAccessories(g_t_items)
	populateAccessories()
end

function separateArmedAccessories(items_table)
	local resultTable = {}
	local t_item = {}
	if items_table ~= nil then
		for i=1,#items_table do
			t_item = items_table[i]
			if t_item["is_armed"] then
				table.insert(resultTable, t_item)
			end
		end
	end
	return resultTable
end

function populateAccessories()
	local lbh = Dialog_GetListBox(gDialogHandle, "lstAnimChoices")
	ListBox_RemoveAllItems(lbh)
	for i=1, #g_t_acc do
		local item = g_t_acc[i]
		ListBox_AddItem(lbh, item["name"], item["GLID"])
	end
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == AttribEvent.PLAYER_INVEN then
		local lbh = Dialog_GetListBox(gDialogHandle, "lstAnimChoices")
		if ((lbh ~= nil) and (lbh ~= 0)) then
			ListBox_RemoveAllItems(lbh)
		end
		getPlayerInventory(WF.PLAYER_EQUIP_INVENTORY, nil, nil, nil, nil, nil, USE_TYPE.EQUIP)
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.PLAYER_EQUIP_INVENTORY then
		local xmlData = KEP_EventDecodeString( event )
		ParsePlayerInventoryItems( xmlData )
		return KEP.EPR_OK						-- don't consume this
	end
end
