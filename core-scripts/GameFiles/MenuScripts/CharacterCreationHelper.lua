--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------


FLAGS = {
	['FACE'] 		= 1,
	['BODY'] 		= 2,
	['EYECOLOR'] 	= 4,
	['BROW'] 		= 8,
	['FACECOVER'] 	= 16,
	['HAIR'] 		= 32,
	['BODY_TYPE'] 	= 64,
	['HEIGHT'] 		= 128,
	['WEIGHT'] 		= 256 }
FLAGS_ALL_SET		= 1023

ANIMODE_STARTUP 	= 1
ANIMODE_IDLE 		= 2

-- HUD_MINIMIZE_EVENT = "HudMinimizeEvent"
-- APT_SELECTION_EVENT = "ApartmentSelectionEvent"
-- KILL_BLACK_SCREEN = "KillBlackScreenEvent"
-- GUIDE_COMPLETE_TASK_EVENT = "GuideCompleteTaskEvent"

-- FORCED_STARTING_APARTMENT = 4649

SCALE_TOLERANCE 	= 3

-- Appearance Component Tables
ACTOR_ID		= {  4, 3, 10,  11, 12, 13, 14, 15 }
CONFIG_HEAD		= {  6, 6,  6,  6,  6,  6,  6,  6 }
CONFIG_NECK		= { 99, 99, 1,  1,  1,  1,  1,  1 }
CONFIG_TORSO	= {  7, 7,  2,  2,  2,  2,  2,  2 }
CONFIG_CROTCH	= { 99, 99, 3,  3,  3,  3,  3,  3 }
CONFIG_ARMS		= { 99, 99, 4,  4,  4,  4,  4,  4 }
CONFIG_LEGS		= {  8, 8,  5,  5,  5,  5,  5,  5 }
CONFIG_HANDS	= { 99, 99, 6,  6,  6,  6,  6,  6 }
CONFIG_FEET		= { 14, 9,  7,  7,  7,  7,  7,  7 }
CONFIG_SHIRT	= {  9, 10, 8,  8,  8,  8,  8,  8 }
CONFIG_PANTS	= { 11, 12, 9,  9,  9,  9,  9,  9 }
CONFIG_BOOTS	= { 12, 13, 10, 10, 10, 10, 10, 10 }
CONFIG_HAIR		= {  0, 1,  12, 12, 12, 12, 12, 12 }
CONFIG_FACE		= {  4, 5,  13, 13, 13, 13, 13, 13 }
FACES_MALE 		= { 2988, 2989, 2990, 3720, 3746 }
FACES_FEMALE 	= { 2991, 2992, 2993, 3744, 3745 }
HAIR_COLORS_MALE 	= { 8, 8, 13, 10, 8,
						8, 8, 8, 26, 26,
						26, 26, 26, 8, 12,
						8, 11, 8, 0, 8,
						8, 8, 8, 8, 8, 8}
HAIR_COLORS_FEM 	= { 26, 26, 26, 26, 26,
						26, 26, 26, 26, 26,
						26, 26, 26, 26, 26,
						26, 26, 26, 26, 8,
						8,	8,	8,	8,	8 }

-- Appearance Component Indices
FEMALE 			= 1
MALE 			= 2
BODY_FEM_THIN 	= 3
BODY_FEM_MED 	= 4
BODY_FEM_BIG 	= 5
BODY_MALE_THIN 	= 6
BODY_MALE_MED 	= 7
BODY_MALE_BIG 	= 8
FACE_EYE_RIGHT	= 0
FACE_EYE_LEFT 	= 2
FACE_EYEBROW	= 7
FACE_FACECOVER	= 8
DB_INDEX_OFFSET	= 7 -- Translates body type vales 3 - 8 into dbIndex values 10 - 15
HEIGHT_BASE_MALE	= 70
HEIGHT_BASE_FEMALE 	= 68
-- The numbers in this section are the max index for an array starting at 0 (i.e. MAX_BEARD 0 - 15)
MAX_BEARD 		= 15
MAX_MAKEUP		= 15
MAX_BROW_FEM	= 6
MAX_BROW_MALE	= 5 -- BROW_COLOR for female only
MAX_BROW_COLOR	= 3 -- BEARD_COLOR for male only, affects eye brow color
MAX_BEARD_COLOR = 4
MAX_SKIN_COLOR	= 11
MAX_EYE_COLOR 	= 14
IDLE_ANIM_ID 	= 0

HAIR_FOR_EYES_MALE	= 18
HAIR_FOR_EYES_FEM 	= 23

NUM_VIEWPORTS		= 9
-- NUM_COLUMNS			= 3
VPMODE_SKINCOLOR	= 1
VPMODE_FACE			= 2
VPMODE_FACECOVER	= 3
VPMODE_BEARDCOLOR	= 4
VPMODE_HAIR			= 5
VPMODE_HAIRCOLOR	= 6
VPMODE_EYECOLOR		= 7
VPMODE_BROW			= 8
VPMODE_BODYTYPE		= 9

INVENTORY_OFFSET = 488