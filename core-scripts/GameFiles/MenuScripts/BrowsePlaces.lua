--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")
dofile("Framework_MenuShadowHelper.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- button control array

gctrls_Button = {}

gctrls_Button["Common"] = 
{
"btnGo_Row",                    -- ok
"imgVertWh_Row",                -- ok
"imgGrad_Row",                  -- ok
"stcName_Row",                  -- ok
"stcOwnerName_Row",             -- ok
"stcOwnerLabel_Row",            -- ok
"stcDescription_Row",           -- ok
"imgObjectFrame_Row",           -- ok
"imgObjectBG_Row",              -- ok
"imgObjectPic_Row",             -- ok
}

gctrls_Button["Visits"] = 
{
"stcMonthlyVisits_Row",
"stcMonthlyVisitsCount_Row",
"stcManage"
}

gctrls_Button["Request"] = 
{
"imgRequestsBG_Row",
"stcRequestsCount_Row",
"stcRequestsLabel_Row",
}

gctrls_Button["Population"] = 
{
"stcPopulationLabel_Row",
"stcPopulationCount_Row",
}

gctrls_Button["Raves"] = 
{
"imgRavesBG_Row",
"stcRavesLabel_Row",
"stcRavesCount_Row",
}

gctrls_Button["Vip"] = 
{
"btnVIPOnly_Row"
}

gctrls_Button["Ap"] = 
{
"imgObjectAP_Row"
}

menuIcons = 
{
	"imghorLine",
	"btnHome",
	"btnEvents",
	"btnDashNew"
}

gBP_DisplayMode = {} 
gBP_DisplayMode["REQUESTS"] = {}
gBP_DisplayMode["REQUESTS"].Panels = {}
gBP_DisplayMode["REQUESTS"].Panels["show"] = { "Request" }
gBP_DisplayMode["REQUESTS"].Panels["hide"] = { "Visits", "Population", "Raves", "Vip", "Ap" }

gBP_DisplayMode["POPULATION"] = {}
gBP_DisplayMode["POPULATION"].Panels = {}
gBP_DisplayMode["POPULATION"].Panels["show"] = { "Population" }
gBP_DisplayMode["POPULATION"].Panels["hide"] = { "Visits", "Request", "Raves", "Vip", "Ap" }

gBP_DisplayMode["RAVES"] = {}
gBP_DisplayMode["RAVES"].Panels = {}
gBP_DisplayMode["RAVES"].Panels["show"] = { "Raves" }
gBP_DisplayMode["RAVES"].Panels["hide"] = { "Visits", "Request", "Population", "Vip", "Ap" }

gBP_DisplayMode["VISITS"] = {}
gBP_DisplayMode["VISITS"].Panels = {}
gBP_DisplayMode["VISITS"].Panels["show"] = { "Visits" }
gBP_DisplayMode["VISITS"].Panels["hide"] = { "Request", "Population", "Raves", "Vip", "Ap" }

gBP_DisplayMode["NONE"] = {}
gBP_DisplayMode["NONE"].Panels = {}
gBP_DisplayMode["NONE"].Panels["show"] = { }
gBP_DisplayMode["NONE"].Panels["hide"] = {"Visits",  "Request", "Population", "Raves", "Vip", "Ap" }

buttonIndex = nil --which button is highlighted 
buttonOn = false -- disable/enable zoning zoning
managePress = false 
TAB_NONE        = 0
TAB_WORLDS      = 1
TAB_MY_WORLDS   = 2
TAB_THIS_WORLD 	= 3

TAB_NAMES = {"Worlds", "My Worlds", "This World"}

TOTAL_ROWS = 9

FRAME_SIZE_X = 64
FRAME_SIZE_Y = 64
TOTAL_FRAMES = 12
TIME_BETWEEN_FRAMES = 0.09

--Showing recently visited worlds 
g_recentlyVisited = false 
g_worldsEnabled = true
g_gotWorlds = false
g_worlds = {}
MAX_WORLDS = 9
g_history = {}
ZONE_NAVIGATION_EVENT = "ZoneNavigationEvent"

g_lastSearchText = ""
g_mature = nil 

MIN_SIGNUP = {month = 10, day = 21, year = 2014}

g_currentTab = TAB_NONE
g_currentPage = 1
g_totalPages  = 1
g_totalRecs   = 0

g_iHasAccessPass = nil 
g_iHasVIPPass = nil

g_gradCoords = {0,0,0,0} -- Left, Top, Right, Bottom

g_placeData = {}

g_animationTimer = 0
g_currentFrame = 1

g_CategoryIDToText = {}
g_CategoryIDToText[PlaceCategoryIds.HANGOUTS]                 = "All Communities"
g_CategoryIDToText[PlaceCategoryIds.ARCADE_HANGOUTS]          = "Arcades"
g_CategoryIDToText[PlaceCategoryIds.ARTGALLERY_HANGOUTS]      = "Art Galleries"
g_CategoryIDToText[PlaceCategoryIds.CASINO_HANGOUTS]          = "Casinos"
g_CategoryIDToText[PlaceCategoryIds.DANCECLUB_HANGOUTS]       = "Dance Clubs"
g_CategoryIDToText[PlaceCategoryIds.FESTIVAL_HANGOUTS]        = "Festivals"
g_CategoryIDToText[PlaceCategoryIds.GAMELOUNGE_HANGOUTS]      = "Game Lounges"
g_CategoryIDToText[PlaceCategoryIds.MUSEUM_HANGOUTS]          = "Museums"
g_CategoryIDToText[PlaceCategoryIds.MUSICVENUE_HANGOUTS]      = "Music Venues"
g_CategoryIDToText[PlaceCategoryIds.PLACEOFWORSHIP_HANGOUTS]  = "Places of Worship"
g_CategoryIDToText[PlaceCategoryIds.SPORTSBAR_HANGOUTS]       = "Sports Bars"
g_CategoryIDToText[PlaceCategoryIds.STORE_HANGOUTS]           = "Stores"
g_CategoryIDToText[PlaceCategoryIds.THEATER_HANGOUTS]         = "Theaters"
g_CategoryIDToText[PlaceCategoryIds.THEMEPARK_HANGOUTS]       = "Theme Parks"
g_CategoryIDToText[PlaceCategoryIds.WEDDINGCHAPEL_HANGOUTS]   = "Wedding Chapels"
g_CategoryIDToText[PlaceCategoryIds.OTHER_HANGOUTS]           = "Other"

gBP_communityCategory = -1
gBP_ResyncTimer = 0

gBP_Actions = {}
gBP_Actions.MostActive    = "action=Browse3DAppsByRequest"
gBP_Actions.MostPopulated = "action=MostPopulated"
gBP_Actions.MostVisited   = "action=MostVisited"
gBP_Actions.MostRaved     = "action=MostRaved"
gBP_Actions.MostRequested = "action=MostRequested"
gBP_Actions.Search        = "action=search3DApps"

DEFAULT_WORLD_ACTION = "Browse3DAppsByRequest"
WORLD_CATEGORY_ACTION = "Browse3DAppsByCategory"

g_tab = nil

g_page = 1
g_maxPage = 1

CAT_OFFSET = 1
MAX_CATS = 5
g_category = nil 
g_categories = {}
g_startingCategory = nil 
g_startingTab = TAB_WORLDS
g_visited = 0

g_webID = 0 
local g_isTopWorlds = false

local DEFAULT_TOP_WORLD_LOOT = 100
local DEFAULT_TOP_WORLD_BOUNTY = 25000
local g_topWorldLoot = DEFAULT_TOP_WORLD_LOOT
local g_topWorldBounty = DEFAULT_TOP_WORLD_BOUNTY

local g_openWithTopWorlds = false

-- offsetting worlds displayed for Top Worlds and Recently Visited AB tests 
g_offset = 1

g_showLightbox = false
g_lightboxAlpha = nil

searchLogID = 0 -- returns only during searches, for AB test search conversion 

findAPworlds = false

g_searchText = "" 

g_noWorldsRV = 0 
g_RVinitial = 0

g_currTime = ""
g_signup = ""
g_newUser = false 

g_onlyHideControls = true  -- used for Page 1 of Most Popular to fix bug where prior page results would flash before current page results were shown
g_processMostVisitedHandler = true
g_externalCall = false

g_showGems = false -- ED-7696 A/B Experiment 4b959859-863f-11e6-84f2-a3cf223dc93b

g_categories[TAB_WORLDS] = {{name = "Most Popular"},							
							{},
							{name = "Most Populated", 	action = "MostPopulated"},
							{name = "Most Visited", 	action = "MostVisited"},
							{name = "Adult Pass",		other	= "&onlyAP=T", 	requiresAP = true},
							}
							
g_categories[TAB_MY_WORLDS] = {	{name = "My Worlds"} }

g_categories[TAB_THIS_WORLD] = {{name = ""}}

g_tOverviewSettings = {}

local BUTTON_MOVEMENT = 204
local OVERVIEW_OFFSET = 70

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler("queryPlacesHandler", "QueryPlacesEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("responseAccessPassInfoHandler", "ResponseAccessPassInfoEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("communityTabEventHandler", "CommunityTabEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "LightboxMenuEventHandler", "LightboxMenuEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "displayTopWorldsEventHandler", "DisplayTopWorlds", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "zoneNavigationEventHandler", ZONE_NAVIGATION_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "clientResizeEventHandler", "ClientResizeEvent", KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "displayMostVisitedWorldsHandler", "DisplayMostVisitedWorlds", KEP.MED_PRIO )
	KEP_EventRegisterHandler("returnOverviewSettingsHandler", "FRAMEWORK_RETURN_OVERVIEW_SETTINGS", KEP.HIGH_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister("TravelNoticeEvent", KEP.MED_PRIO)
	KEP_EventRegister("QueryPlacesEvent", KEP.MED_PRIO)
	KEP_EventRegister("RequestAccessPassInfoEvent", KEP.MED_PRIO)
	KEP_EventRegister("ResponseAccessPassInfoEvent", KEP.MED_PRIO)
	KEP_EventRegister("CommunityTabEvent", KEP.MED_PRIO)
	KEP_EventRegister( "LightboxMenuEvent", KEP.MED_PRIO )
	KEP_EventRegister( "DisplayTopWorlds", KEP.HIGH_PRIO)
	KEP_EventRegister( "DisplayMostVisitedWorlds", KEP.HIGH_PRIO)
end

function setControlTabs(ctrlNames, enable)
	for i, ctrlName in ipairs(ctrlNames) do
		-- trim all spaces from name
		local fmtCtrlName = string.gsub(ctrlName, "%s+", "")
		if (i == enable) then
			setControlTexture("imgTab"..fmtCtrlName, "tabSelected9.tga")
			Control_SetEnabled(gHandles["btn"..fmtCtrlName], false)
		else
			setControlTexture("imgTab"..fmtCtrlName, "tab9.tga")
			Control_SetEnabled(gHandles["btn"..fmtCtrlName], true)
		end
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	KEP_EventCreateAndQueue("FRAMEWORK_REQUEST_OVERVIEW_SETTINGS")
	-- Add "Top Worlds" category if user is in the variant of "No Top Worlds Link vs Top Worlds Link" test.
	MAX_CATS = 6
	g_categories[TAB_WORLDS] = {{name = "Most Popular"},
								{},
								{name = "Top Worlds",		max = 6, start = 1},
								{name = "Most Populated", 	action = "MostPopulated"},
								{name = "Most Visited", 	action = "MostVisited"},
								{name = "Adult Pass",		other	= "&onlyAP=T", 	requiresAP = true},
								}
	recentlyVisited(false, MAX_WORLDS)
	g_recentlyVisited = true

	clearPlaces()
	showAccessPass(false)

    initTooltips()

	local e = Image_GetDisplayElement(gHandles["imgGrad_Row1"])
	g_gradCoords = {}
	g_gradCoords[1] = Element_GetCoordLeft(e)
	g_gradCoords[2] = Element_GetCoordTop(e)
	g_gradCoords[3] = Element_GetCoordRight(e)
	g_gradCoords[4] = Element_GetCoordBottom(e)
	
	for i = 1, MAX_CATS do
		_G["stcCategory" .. tostring(i) .. "_OnLButtonUp"] = function(stcHandle)
			local s, e, n = string.find(Control_GetName(stcHandle), "(%d+)")
			selectCategory(tonumber(n))
		end
	end

	local username = KEP_GetLoginName()
	makeWebCall(GameGlobals.WEB_SITE_PREFIX..LOGIN_INFO_SUFFIX..username, WF.BROWSE_PLACES_USER_PROFILE)
		
	local ev = KEP_EventCreate("RequestAccessPassInfoEvent")
	KEP_EventQueue(ev)

	-- Reset Global World Requests
	GlobalWorldReqsReset()
	setTopWorldControlVisibility()

	resizeDropShadow( )

	m_lastPosition = MenuGetLocationThis()
	
end

function Dialog_OnDestroy(dialogHandle)
	if MenuIsOpen("Framework_Overview") then
		MenuClose("Framework_Overview")
	end
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnMoved(dialogHandle, x, y)
	showLightbox()
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	if MenuIsOpen("Framework_Overview") then
		curLoc = MenuGetLocationThis()
		if not m_dragging then
			if (curLoc.x ~= m_lastPosition.x or curLoc.y ~= m_lastPosition.y) then
				if Dialog_IsLMouseDown(dialogHandle) == 1 then
					m_dragging = true
				end

				MenuSetLocation("Framework_Overview", curLoc.x , curLoc.y + OVERVIEW_OFFSET)			
			end
		else
			MenuSetLocation("Framework_Overview", curLoc.x , curLoc.y + OVERVIEW_OFFSET)	

			if Dialog_IsLMouseDown(dialogHandle) == 0 then
				m_dragging = false
			end
		end
		m_lastPosition = curLoc
	end
end

function responseAccessPassInfoHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local apPass  = KEP_EventDecodeNumber(event)
	local vipPass = KEP_EventDecodeNumber(event)
	g_iHasAccessPass = (apPass == 1)
	g_iHasVIPPass = (vipPass == 1)
	initialPlacesLoad()

	return KEP.EPR_OK
end

function LightboxMenuEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == LBOXM.BROWSE_PLACES then
		g_showLightbox = true

		if KEP_EventMoreToDecode(event) == 1 then
			g_lightboxAlpha = KEP_EventDecodeNumber(event)
		end

		showLightbox()
	end
end 

-- Request to display the top worlds
function displayTopWorldsEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_startingCategory = PlaceCategoryIds.APPS
	g_openWithTopWorlds = true
end

-- Request to display the most visited
function displayMostVisitedWorldsHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_externalCall = true
end

function returnOverviewSettingsHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_tOverviewSettings = Events.decode(KEP_EventDecodeString(event))

	if g_tOverviewSettings.overviewEnabled then
		if g_tOverviewSettings.overviewImage and g_tOverviewSettings.overviewImage ~= "" and g_tOverviewSettings.overviewImage ~= "none" then
			enabledOverviewButton()
		end
	end
end


function enabledOverviewButton()

	g_startingTab = TAB_THIS_WORLD
	Control_SetEnabled(gHandles["btnThisWorld"], true)
	Control_SetVisible(gHandles["btnThisWorld"], true)

	Control_SetEnabled(gHandles["imgTabThisWorld"], true)
	Control_SetVisible(gHandles["imgTabThisWorld"], true)

	
	Control_SetLocationX(gHandles["btnWorlds"], Control_GetLocationX(gHandles["btnWorlds"]) + BUTTON_MOVEMENT)
	Control_SetLocationX(gHandles["imgTabMyWorlds"], Control_GetLocationX(gHandles["imgTabMyWorlds"]) + BUTTON_MOVEMENT)
	Control_SetLocationX(gHandles["btnMyWorlds"], Control_GetLocationX(gHandles["btnMyWorlds"]) + BUTTON_MOVEMENT)
	Control_SetLocationX(gHandles["imgTabWorlds"], Control_GetLocationX(gHandles["imgTabWorlds"]) + BUTTON_MOVEMENT)
	--Control_SetEnabled(gHandles["imgTabThisWorld"], true)

	changeTab(TAB_THIS_WORLD)
	setTab(TAB_THIS_WORLD)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_isTopWorlds = (g_tab == TAB_WORLDS and g_categories[g_tab][g_category].name == "Top Worlds")
	if filter == WF.BROWSE_PLACES_SEARCH then
		local responseXml = KEP_EventDecodeString(event)
		updatePages()
		parsePlaces(responseXml)
	elseif filter == WF.DASH_MOST_VISITED then
		if (g_processMostVisitedHandler) then
			local ret = KEP_EventDecodeString(event)
			parseMostVisitedWorlds(ret)
			g_processMostVisitedHandler = false
		end
	elseif filter == WF.DASHBOARD_GET_HISTORY then
		local ret = KEP_EventDecodeString(event)
		parseBrowsingHistory(ret)
	elseif filter == WF.BROWSE_PLACES_USER_PROFILE then
		local returnData = KEP_EventDecodeString( event )
		s, e, g_age = string.find(returnData, "<age>(%d+)</age>")
		s, e, g_signup = string.find(returnData, "<formatted_signup_date>(.-)</formatted_signup_date>")
		g_age = tonumber(g_age)
		if (g_age == nil) then 
			g_age = 0 
		end
		g_mature = (g_age >= 18)
		
		for tab, catList in pairs(g_categories) do
			for i = 1, MAX_CATS do
				if catList[i] and catList[i].requiresMature then
					removeCategory(tab, i)
				end
				if catList[i] and catList[i].requiresAP then
					removeCategory(tab, i)
				end
			end
		end 
		
		initialPlacesLoad()
		--get current time to determine old/new users
		makeWebCall(GameGlobals.WEB_SITE_PREFIX.."kgp/getcurrenttime.aspx", 0)
	else
		local ret = KEP_EventDecodeString(event)
		--is this the current time? 
		if ret ~= nil then 
			local s,e = 0
			local m,d,y = 0
			s, e, g_currTime = string.find(ret, "<CurrentTime>(.-)</CurrentTime>")
			if g_currTime ~= nil then 
				m, d, y = string.match(g_currTime, "(%d+)/(%d+)/(%d+)")
				
				-- Is this a new user? 
				if g_signup ~= "" then 
					local m_sign,d_sign,y_sign = 0
					m_sign,d_sign,y_sign = string.match(g_signup, "(%d+)/(%d+)/(%d+)")
					
					if y_sign == y and m_sign == m and d_sign == d then 
						g_newUser = true
						getPlaces()
					end 
					
				end 
			end 
		end 
	end
end

function showLightbox()
	if g_showLightbox then
		local blackOutN = Image_GetControl(gHandles["imgBlackoutN"])
		local blackOutS = Image_GetControl(gHandles["imgBlackoutS"])
		local blackOutE = Image_GetControl(gHandles["imgBlackoutE"])
		local blackOutW = Image_GetControl(gHandles["imgBlackoutW"])

		ShowMenuLightbox(gDialogHandle, blackOutN, blackOutS, blackOutW, blackOutE, g_lightboxAlpha)

		Dialog_SetMovable(gDialogHandle, false)
		Control_SetVisible(gHandles["btnClose"], false)
		Dialog_SetCloseOnESC(gDialogHandle, 0)
	end
end

--Ankit - call display places not just when #g_worlds > 0 but also when #g_placeData > 0
function textureDownloadHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	drain = KEP_EventDecodeNumber(event)
	textureDownloaded = KEP_EventDecodeString(event)
	if #g_worlds > 0 or #g_placeData > 0 then
		-- this extra call to displayPlaces() is causing the returned data to flash old page results
		-- before the new page results are returned. Removing for now to eliminate page flash.
		-- displayPlaces()
	end
end

function WebCallCached(url, filter, maxItems)
	makeWebCallCached(url, filter, 0, maxItems)
end

function parseMostVisitedWorlds(ret)
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		local s, e, numRecs = string.find(ret, "<TotalNumberRecords>(.-)</TotalNumberRecords>")
		numRecs = tonumber(numRecs)
		if numRecs > 0 then
			s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
			if result == "0" then
				local s, e, numRecs = string.find(ret, "<TotalNumberRecords>(.-)</TotalNumberRecords>")
				numRecs = tonumber(numRecs)
				if numRecs > 0 then
					g_worlds = {}
					local start = 0
					local last = 0
					for i=1,numRecs do
				
						-- Parse World Attributes
						s, e, visit = string.find(ret, "<Vists>(.-)</Vists>", e)
						start, last, location = string.find(visit, "<location>(.-)</location>")
						start, last, dbPath = string.find(visit, "<image_path>(.-)</image_path>")
						start, last, kurl = string.find(visit, "<STPURL>(.-)</STPURL>")
						start, last, gameId = string.find(visit, "<game_id>(.-)</game_id>")
						start, last, commId = string.find(visit, "<community_id>(.-)</community_id>")
						
						-- Get World Picture
						local filename = ""
						if not dbPath then
							dbPath = DEFAULT_KANEVA_PLACE_ICON
						end

						filename = tostring(split(GetFilenameFromPath(dbPath), ".")[1])
						filename = commId .. "_" .. filename .. CUSTOM_TEXTURE_THUMB
					
						KEP_DownloadTextureAsyncStr( filename, dbPath )
						filename = "../../CustomTexture/" .. filename
						
						-- Insert World Into Table
						local visitPlace = {}
						visitPlace.location = location
						visitPlace.url = kurl
						visitPlace.thumbnail = dbPath
						visitPlace.id = commId
						visitPlace.filename = filename
						visitPlace.gameId = tonumber(gameId) or 0
						if location ~= g_username then -- dont include user's home
							table.insert(g_worlds, visitPlace)
						end
					end
				end
			end
		end
 	end
	 
	-- Refresh Display or Get Local History
	if #g_worlds > 0 then
		-- World list obtained
		g_gotWorlds = true
		displayPlaces(g_onlyHideControls)
		g_onlyHideControls = 0
	else
		requestHistory()
	end
end

function requestHistory()
	local ev = KEP_EventCreate( ZONE_NAVIGATION_EVENT)
	KEP_EventSetFilter(ev, ZONE_NAV.HISTORY_REQUEST)
	KEP_EventQueue( ev)
end

function parseBrowsingHistory(ret)
	local result = string.match(ret, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		
		result = string.match(ret, "<ReturnCode>(.-)</ReturnCode>")
		if result == "0" then
			
			local numRecs = string.match(ret, "<NumberRecords>(.-)</NumberRecords>")
			numRecs = tonumber(numRecs)
			if numRecs > 0 then
				
				g_worlds = {}
				local tempWorlds = {}

				for visit in string.gmatch(ret, "<Communities>(.-)</Communities>") do
					local name = string.match(visit, "<name>(.-)</name>")
					local commId = string.match(visit, "<community_id>(.-)</community_id>")
					local dbPath = string.match(visit, "<thumbnail_small_path>(.-)</thumbnail_small_path>")

					local filename = ""
					if not dbPath then
						dbPath = DEFAULT_KANEVA_PLACE_ICON
					end

					filename = tostring(split(GetFilenameFromPath(dbPath), ".")[1])
					filename = commId .. "_" .. filename .. CUSTOM_TEXTURE_THUMB
					
					KEP_DownloadTextureAsyncStr( filename, dbPath )
					filename = "../../CustomTexture/" .. filename

					-- Insert World Into Temp Table
					local visitPlace = {}
					visitPlace.location = name
					visitPlace.thumbnail = dbPath
					visitPlace.id = commId
					visitPlace.filename = filename
					visitPlace.gameId = 0
						
					if name then
						tempWorlds[name] = visitPlace
					end
				end

				-- Insert into g_worlds, in correct historical order
				for i, v in ipairs(g_history) do
					if tempWorlds[v.name] then
						local tempPlace = tempWorlds[v.name]
						tempPlace.url = v.url
						table.insert(g_worlds, tempPlace)
					end
				end
			end
		end
	end
	-- Refresh Display
	-- Display what we have
	g_gotWorlds = true
	displayPlaces()
end

function zoneNavigationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ZONE_NAV.HISTORY_INFO then
		g_history = {}
		local usedNames = {}
		local recentHistory = readHistoryIntoTable(event)
		local nameString = ""

		for i = #recentHistory,1,-1 do
			local v = recentHistory[i]
			local historyItem = {}
			historyItem.url = v.url
			historyItem.type = parseZoneTypeFromURL(historyItem.url)
			
			if isURL3DApp(historyItem.url) then
				historyItem.name = parseGameNameFromURL(historyItem.url)
			else
				historyItem.name = parseNameFromURL(historyItem.url)
			end

			-- Select distinct items, order most recent, exclude user's home
			if #g_history >= MAX_WORLDS then
				break
			elseif g_username ~= historyItem.name and not usedNames[historyItem.name] then
				usedNames[historyItem.name] = historyItem.name
				local enc_historyItemName = KEP_UrlEncode(historyItem.name)
				nameString = nameString .. enc_historyItemName .. ","
				table.insert(g_history, historyItem)
			end
		end

		makeWebCall(GameGlobals.WEB_SITE_PREFIX .. GET_COMMUNITY_SUFFIX .. nameString, WF.DASHBOARD_GET_HISTORY, MAX_WORLDS)
	end
end

function queryPlacesHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local requestedType = KEP_EventDecodeNumber(event)
	local ignored = KEP_EventDecodeNumber(event) -- categoryFilter
	local searchKeyword = ""

	if filter == 2 then -- search word provided
		searchKeyword = KEP_EventDecodeString(event)
	end
	g_startingCategory = requestedType 
	if ignored == 1 then 
		g_startingTab = TAB_MY_WORLDS 
		changeTab(TAB_MY_WORLDS)
	elseif ignored == 0 then
		changeTab(TAB_WORLDS)
		g_visited = 1
		if g_mature ~= nil and g_iHasAccessPass ~= nil and g_startingCategory ~= nil then	
			setTab(TAB_WORLDS, 4) 
		end
	elseif g_tab ~= TAB_THIS_WORLD then
		changeTab(TAB_WORLDS)
		initialPlacesLoad()
	end 
end

function animateImage(control, timeElapsed, frameNumber, frameWidth, frameHeight, frameDelay)
	g_animationTimer = g_animationTimer - timeElapsed
	if g_animationTimer < 0 then
		g_animationTimer = TIME_BETWEEN_FRAMES
		g_currentFrame = math.fmod(g_currentFrame + 1, 12) + 1
		local element = Image_GetDisplayElement(control)
		Element_SetCoords(element, (g_currentFrame - 1) * frameWidth, 0, (g_currentFrame - 1) * frameWidth + frameWidth, frameHeight)
	end
end

function getPlaces()

	clearPlaces()

	setControlEnabled("stcLoadingAnim", true)
	setControlEnabled("imgLoadingAnim", true)

	local searchText = EditBox_GetText(gHandles["edSearch"])
	local checkedAP = false
	local url = ""
	local onlyMine = false 
	local onlyOwned = false
	local displayAP = false 
	local category = ""
	local getTopRandom = false
	local myCountryChecked = false
	local myAge = false 
	
	local category = g_categories[g_tab][g_category]
	g_webID = g_webID + 1
	if (g_currentTab == TAB_WORLDS) then
	
		showAccessPass(false)	
		onlyMine = false 

		if category.name == "Top Worlds" then
			url = GameGlobals.WEB_SITE_PREFIX .. "kgp/travelzonelist.aspx?action=TopWorldsTour"
		else
			url = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX .. "action="
		end
		displayAP = false
		checkedAP = false
		
		if category.name == "Top Worlds" then
			url = url .. "&start="..tostring(g_categories[g_tab][g_category].start).."&max="..tostring(g_categories[g_tab][g_category].max)
			url = url .. "&username=" .. KEP_GetLoginName()
		elseif category.name == "Most Popular" then 
			url = url .. "MostPopulatedCombined"
		elseif category and category.catName then
			url = url .. WORLD_CATEGORY_ACTION .. "&categoryname=" .. category.catName
		elseif category and category.action then
			url = url .. g_categories[g_tab][g_category].action
		else
			if category.name== "Adult Pass" then 
				displayAP = true
				checkedAP = true
			else
				url = url .. DEFAULT_WORLD_ACTION
			end 
		end

		local otherParams = g_categories[g_tab][g_category].other
		if otherParams then
			if category.name == "Adult Pass" then 
				displayAP = true
				checkedAP = true
				url = url .. "MostPopulatedCombined" 
			else	
				url = url .. otherParams
			end 

		end
		
		if category.name ~= "Top Worlds" and ((searchText ~= nil) and (searchText ~= "")) then
			g_lastSearchText = searchText
			if category.name == "Communities" then   
				url = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX .. "action=searchCommunities&placeType=-1"
			else  
				url = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX .. gBP_Actions.Search
			end 
			url = url .. "&search=" .. searchText
		else
			g_lastSearchText = ""
		end
				
	elseif (g_currentTab == TAB_MY_WORLDS) then
		showAccessPass(false)	
		onlyMine   = true
		onlyOwned = getControlChecked("chkOnlyMyCommunities")
		displayAP = true 
		checkedAP = false
		myCountryChecked = false
		myAge = false 
		url = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX .. gBP_Actions.Search
		
		if category.name == "My Communities" then 
			url = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX .. "action=searchCommunities"
		end 

		-- overide url if search text exists
		if ((searchText ~= nil) and (searchText ~= "")) then
			g_lastSearchText = searchText
			if category.name == "My Communities" then 
				url = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX .. "action=searchCommunities"
			else
			url = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX .. gBP_Actions.Search
			end 	
			url = url .. "&search=" .. searchText
		else
			g_lastSearchText = ""
		end

	end
	
	url = url.."&passthrough=".. g_webID

	if category.name == "Top Worlds" then
		makeWebCall(url, WF.BROWSE_PLACES_SEARCH)
	else
		-- If in Recently Traveled B test, parse this in. MUST make this recently visited webcall BEFORE the total worlds to avoid race condition
		-- Only need to call this when on page 1 of the Most Popular category
		if (g_recentlyVisited and category.name == "Most Popular") then 
			if g_currentPage ==1 then 
				makeWebCall(WEBCALL_MOST_VISITED_100, WF.DASH_MOST_VISITED, MAX_WORLDS)
			end 
		end 

		-- Add Only Mine
		if onlyMine then
			url = url .. "&myCommunitiesOnly=true"
		end

		-- Add Only Owned
		if onlyOwned then
			url = url .. "&onlyOwned=true"
		end

		-- Add AP Filter
		if checkedAP then
			url = url .. "&onlyAP=T"
		end

		if displayAP == false then 
			url = url .. "&hideAP=T" 
		end 
		
		if category.name == "Most Popular" then 
		--By Country and age Filter for all returning users
			if not g_newUser then 
				url = url .. "&country=MineFirst".. "&age=MineFirst"
			end 

		end 

		-- Request Places
		local totalWorlds = 9
		if (g_recentlyVisited and category.name == "Most Popular") then 
				if g_noWorldsRV == 0 and g_RVinitial ~= 1 then
					g_RVinitial = 1
				end 	
				if g_RVinitial ==1 then
					if #g_worlds >0 then 
						if g_currentPage > 1 then 
							totalWorlds = TOTAL_ROWS
							url = url .. "&offset="..tostring(6+9*(g_currentPage-2))
						else
							totalWorlds = 6
						end 
					else 
						totalWorlds = TOTAL_ROWS
					end 
				end 
				
				makeWebCall( url, WF.BROWSE_PLACES_SEARCH, g_currentPage, totalWorlds)

		else 
			makeWebCall( url, WF.BROWSE_PLACES_SEARCH, g_currentPage, totalWorlds)
		end 	
		
	end

end

function categoryIDtoText(categoryID)
	return g_CategoryIDToText[categoryID] or "All Hangouts"
end

function parsePlaces(ret)
	local s = 0        -- start and end index of substring
	local numRec = 0   -- number of records in this chunk
	local passThrough = 0 -- passthrough num
	local strPos = 0   -- current position in string to parse
	local result = ""  -- return string from find temp
	s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
	s, strPos, passThrough = string.find(ret,"<Passthrough>(.-)</Passthrough>")
	s, strPos, searchLogID = string.find(ret,"<SearchLogId>(.-)</SearchLogId>")

	searchLogID = tonumber(searchLogID)
	passThrough = tonumber(passThrough)
	if result == "0" and passThrough==g_webID then
		g_placeData = {}
	--	if g_isTopWorlds then g_placeData = {[1] = {}, [2] = {}, [3] = {}} end
		
				-- Request Places
		local totalWorlds = TOTAL_ROWS
		if (g_recentlyVisited and g_categories[g_tab][g_category].name == "Most Popular") or g_isTopWorlds then 
			totalWorlds = 6
		end 
		
		if g_currentPage ~= 1 then 
			totalWorlds = TOTAL_ROWS
		end 
		
		local s, e, numRecs = string.find(ret, "<TotalNumberRecords>(.-)</TotalNumberRecords>")
		g_totalRecs = tonumber(numRecs)
		g_totalPages = math.ceil(g_totalRecs / totalWorlds)

		local s, e, numRecs = string.find(ret, "<NumberRecords>(.-)</NumberRecords>")
		numRecs = tonumber(numRecs)
		if g_isTopWorlds then
			s, strPos, g_topWorldLoot = string.find(ret, "<TourRewardsLooter>(.-)</TourRewardsLooter>")
			s, strPos, g_topWorldBounty = string.find(ret, "<TourRewardsWorldOwner>(.-)</TourRewardsWorldOwner>")
			g_topWorldLoot = tonumber(g_topWorldLoot) or DEFAULT_TOP_WORLD_LOOT
			g_topWorldBounty = tonumber(g_topWorldBounty) or DEFAULT_TOP_WORLD_BOUNTY
			g_totalPages = 1
		end
		
		if numRecs > 0 then
			local start = 0
			local last = 0
			for i=1,numRecs do
				record = nil
				temps, tempe, record = string.find(ret, "<Communities>(.-)</Communities>", e)
				if record == nil then
					temps, tempe, record = string.find(ret, "<_x0033_DApps>(.-)</_x0033_DApps>", e)
				end
				if record == nil then
					temps, tempe, record = string.find(ret, "<MostPopulated>(.-)</MostPopulated>", e)
				end
				if record == nil then
					temps, tempe, record = string.find(ret, "<MostRaved>(.-)</MostRaved>", e)
				end
				if record == nil then
					temps, tempe, record = string.find(ret, "<MostVisited>(.-)</MostVisited>", e)
				end
				if record == nil then
					temps, tempe, record = string.find(ret, "<MostPopulatedCombined>(.-)</MostPopulatedCombined>", e)
				end
				if record == nil then
					temps, tempe, record = string.find(ret, "<MostRequested>(.-)</MostRequested>", e)
				end	

				if record ~= nil then
					s = temps
					e = tempe

					start, last, name           = string.find(record, "<name>(.-)</name>")
					start, last, name_no_spaces = string.find(record, "<name_no_spaces>(.-)</name_no_spaces>")
					start, last, description    = string.find(record, "<description>(.-)</description>")
					start, last, population     = string.find(record, "<population>(.-)</population>")
					start, last, commId         = string.find(record, "<community_id>(.-)</community_id>")
					start, last, numMembers     = string.find(record, "<number_of_members>(.-)</number_of_members>")
					start, last, numViews       = string.find(record, "<number_of_views>(.-)</number_of_views>")
					start, last, numRaves       = string.find(record, "<number_of_diggs_past_7_days>(.-)</number_of_diggs_past_7_days>")
					start, last, worldReqs      = string.find(record, "<number_of_requests>(.-)</number_of_requests>")
					start, last, isPublic       = string.find(record, "<is_public>(.-)</is_public>")
					start, last, isAP           = string.find(record, "<pass_group_id>([0-9]+)</pass_group_id>")
					start, last, creatorName    = string.find(record, "<username>(.-)</username>")
					start, last, creatorId      = string.find(record, "<creator_id>(.-)</creator_id>")
					start, last, kurl           = string.find(record, "<STPURL>(.-)</STPURL>")
					start, last, gameId         = string.find(record, "<game_id>(.-)</game_id>")
					start, last, topVisited		= string.find(record, "<user_rewarded>(.-)</user_rewarded>")
					start, last, canDropGems	= string.find(record, "<can_drop_gems>(.-)</can_drop_gems>")

					start, last, dbPath = string.find(record, "<thumbnail_small_path>(.-)</thumbnail_small_path>")
					if not dbPath then
						dbPath = DEFAULT_KANEVA_PLACE_ICON
					end
											
					-- Build Local Texture Filename
					local filename = tostring(split(GetFilenameFromPath(dbPath), ".")[1])
					filename, newnum = string.gsub(filename,"_sm" ,"_me")
					dbPath, newnum = string.gsub(dbPath,"_sm" ,"_me")
					filename = filename..".\jpg" 
					KEP_DownloadTextureAsyncStr( filename, dbPath )
					filename = "../../CustomTexture/" .. filename

					local place = {}
                    place.location = name
                    place.name_no_spaces = name_no_spaces
                    place.description = stripTags(description) or ""
                    place.thumbnail = dbPath
                    place.id = commId
					
                    place.filename = filename
                    place.memberCount = tonumber(numMembers) or 0
                    place.viewCount = tonumber(numViews)   or 0
                    place.raveCount = tonumber(numRaves)   or 0
                    place.worldReqs = tonumber(worldReqs)  or 0
                    place.isPublic = (isPublic == "Y")
                    place.isAP = (tonumber(isAP) == 1)
                    place.population = tonumber(population) or 0
                    place.gameId = tonumber(gameId)     or 0
                    place.ownerName = creatorName
                    place.ownerThumbnail = creatorPic
                    place.ownerId = creatorId
                    place.kurl = kurl
					place.topVisited = (topVisited and topVisited == "Y") or false
					place.canDropGems = canDropGems
					--Log("PLACE NAME: "..tostring(place.location).." and PLACE ID: "..tostring(place.id).." and PLACE URL: "..tostring(place.kurl).."i is: "..tostring(i))
					table.insert(g_placeData, place)
				end
			end
		end
	end
	displayPlaces()
	
end

function initialPlacesLoad()
	if g_mature ~= nil and g_iHasAccessPass ~= nil and g_startingCategory ~= nil then
		local categories = g_categories[TAB_WORLDS]
		if categories then 
			local catFilter = ""

			-- What category index are we on? (Based on running AB tests)
				for i = 1, MAX_CATS do
					local category = categories[i]
					if category and category.name ~= nil or category.name ~= "" then
						if g_openWithTopWorlds then
							catFilter = "Top Worlds"
						end

						if g_visited == 1 then 
							catFilter = "Most Visited"
						end 
						--If we are in any AB tests that load different categories on the Travel
						if catFilter ~= "" then 
							if category.name == catFilter then 
								g_startingCategory = i
							end 
						else
							g_startingCategory = 1
						end 
					end 
				end
		end 
		if g_openWithTopWorlds then
			changeTab(TAB_WORLDS)
			g_currentTab = TAB_WORLDS
			setCategories(g_currentTab)
			g_tab = TAB_WORLDS
			selectCategory(g_startingCategory)
		else
			if g_visited == 1 then
				setTab(TAB_WORLDS, g_startingCategory)
			else
				setTab(g_startingTab, g_startingCategory)
			end
		end
	end 
end

-- The Connie Method
function setTopWorldControlVisibility()
	local numControls = Dialog_GetNumControls(gDialogHandle)
	for i=1, numControls do
		local ch = Dialog_GetControlByIndex( gDialogHandle, i )
		if ch and ch ~= 0 then
			local controlName = Control_GetName(ch)
			if string.match(controlName, "TopWorlds") then
				Control_SetEnabled(ch, g_isTopWorlds)
				Control_SetVisible(ch, g_isTopWorlds)
			end
		end
	end
end

function setTab(selectedTab, category)
	if g_tab == selectedTab then return end

    --Reset Pages
	g_page = 1
	g_maxPage = 1
		
	local tabWorlds = (selectedTab == TAB_WORLDS)
	local tabMyWorlds = (selectedTab == TAB_MY_WORLDS)
	local tabThisWorld = (selectedTab == TAB_THIS_WORLD)
		
	--Enable/Disable top tabs
	Control_SetEnabled(gHandles["btnWorlds"], not tabWorlds)
	Control_SetEnabled(gHandles["btnMyWorlds"], not tabMyWorlds)
	Control_SetEnabled(gHandles["btnThisWorld"], not tabThisWorld)
			
	setCategories(selectedTab)
	g_tab = selectedTab
	selectCategory(category or 1)
end

function selectCategory(categoryNum, searching)
	local highlightHandle = gHandles["imgCategoryHighlight"]
	local category = g_categories[g_tab][categoryNum]
	
	if categoryNum and category and category.name and category.name ~= "" then

		setControlEnabled("chkOnlyMyCountry",   false)
		setControlEnabled("chkShowAllAge",   false)

        --Reset Pages
		g_page = 1
		g_maxPage = 1

		if not searching and g_searchText and g_searchText ~= "" then
			imgClearSearch_OnLButtonUp(gHandles["imgClearSearch"])
		end
		
		local catHandle = gHandles["stcCategory" .. categoryNum]
		local yLoc = Control_GetLocationY(catHandle) + CAT_OFFSET
		
		if g_category then
			local oldCatDisp = Static_GetDisplayElement(gHandles["stcCategory" .. g_category])
			Element_AddFont( oldCatDisp, gDialogHandle, "Verdana", -11, 400, false )
		end
		 
		local newCatDisp = Static_GetDisplayElement(catHandle)
		Element_AddFont( newCatDisp, gDialogHandle, "Verdana", -11, 700, false )
		
		Control_SetLocation(highlightHandle, Control_GetLocationX(highlightHandle), yLoc)
		
		g_category = categoryNum

		if category.name == "Adult Pass" then 
			if not g_iHasAccessPass then 
				showAP()
				findAPworlds = true
			else 
				findAPworlds = false
				DisplayPage(1)
			end 
		else 	
			findAPworlds = false
			DisplayPage(1)
		end 	
	end
end

function setCategories(tab)
	local categories = g_categories[tab]
	local menuStartName = ""
	if categories then
		for i = 1, MAX_CATS do
			local category = categories[i]
			local catName = ""
			if category then
				catName = category.name or ""
			end
			Static_SetText(gHandles["stcCategory" .. i], catName)
			if g_currentTab == TAB_WORLDS and g_mature ~= nil and g_iHasAccessPass ~= nil and g_startingCategory ~= nil then 
				for i,v in ipairs(menuIcons) do 
					Control_SetVisible(gHandles[v],true)
				end 
					
				--shift menu icons
				if g_mature == true then 
					if catName == "Adult Pass" then 
						menuStartName = "stcCategory" .. i
					end 
				else 
					menuStartName ="stcCategory" .. (MAX_CATS)					
				end
					
				-- leave the checkbox where it is 
				Control_SetLocation(gHandles["chkShowAllAge"], Control_GetLocationX(gHandles["chkShowAllAge"]),Control_GetLocationY(gHandles["chkShowAllAge"]))
					
				menuStartName ="stcCategory" .. (MAX_CATS)
					
				if menuStartName ~= "" then 
					local horLineY = Control_GetLocationY(gHandles["imghorLine"])
					local startY = Control_GetLocationY(gHandles[menuStartName]) + Control_GetHeight(gHandles[menuStartName]) +10
					local shift = horLineY - startY 
					for i,v in ipairs(menuIcons) do 
						Control_SetLocation(gHandles[v], Control_GetLocationX(gHandles[v]),Control_GetLocationY(gHandles[v]) - shift)
					end 
				end 
			else
				for i,v in ipairs(menuIcons) do 
					Control_SetVisible(gHandles[v],false) 
				end 
			end 
		end
	end
end

function updatePages(updateWorlds)
	-- Enable/Disable page buttons based on page we're on
	if g_page < 1 then
		g_page = 1
		getPlaces()
	end
	
	if g_page > g_maxPage then
		g_page = g_maxPage
		getPlaces()
	end
	
	--If Inventory update requested, do it
	if updateWorlds then
		getPlaces()
	end
end

function showAccessPass(enable)
	setControlEnabled("imgAccessPass", enable)
	setControlEnabled("chkAccessPass", enable)
end

function removeCategory(tab, index)
	if g_categories[tab] and g_categories[tab][index] then
		table.remove(g_categories[tab], index)
		MAX_CATS = MAX_CATS -1
	end
end

function changeTab(tab) -- functions for each tab 
	-- Same Tab ?
	if tab == g_currentTab then return end
	-- Reset shared Tab controls
	setControlText("edSearch", "")
	setControlChecked("chkAccessPass", false)

	if MenuIsOpen("Framework_Overview") then
		MenuClose("Framework_Overview")
	end
	-- Enable Tab Controls
	if (tab == TAB_WORLDS) then
		g_currentTab = TAB_WORLDS
		setControlText("Menu_Title", "Find Worlds")
		setControlEnabled("btnCreateWorld", true)
		setControlEnabled("btnCreateWorld2", false)
		setControlEnabled("stcSearch",      true)
		setControlEnabled("edSearch",       true)
		setControlEnabled("btnSearch",      true)
		setControlEnabled("stcWorldFilter", false)
		setControlEnabled("cboWorldFilter", false)
		setControlEnabled("stcCommunityCategories", false)
		setControlEnabled("cboCommunityCategories", false)
		setControlEnabled("chkOnlyMyCommunities",   false)
		setControlTabs(TAB_NAMES, TAB_WORLDS)
		setControlEnabled("stcLoadingAnim", false)
		setControlEnabled("imgLoadingAnim", false)
	elseif (tab == TAB_MY_WORLDS) then
		setControlEnabled("btnCreateWorld", true)
		setControlEnabled("btnCreateWorld2", false)
		g_currentTab = TAB_MY_WORLDS
		g_visited = 0
		setControlText("Menu_Title", "Find Worlds")
		setControlEnabled("stcSearch",          true)
		setControlEnabled("edSearch",           true)
		setControlEnabled("btnSearch",          true)
		setControlEnabled("stcWorldFilter",     false)
		setControlEnabled("cboWorldFilter",     false)
		setControlEnabled("stcCommunityCategories", false)
		setControlEnabled("cboCommunityCategories", false)
		setControlEnabled("chkOnlyMyCommunities",   true)
		setControlEnabled("chkOnlyMyCountry",   false)
		setControlEnabled("chkShowAllAge",   false)
		setControlTabs(TAB_NAMES, TAB_MY_WORLDS)
		setControlEnabled("stcLoadingAnim", false)
		setControlEnabled("imgLoadingAnim", false)
	elseif (tab == TAB_THIS_WORLD) then
		setControlEnabled("btnCreateWorld", true)
		setControlEnabled("btnCreateWorld2", false)
		g_currentTab = TAB_THIS_WORLD
		g_visited = 0
		setControlText("Menu_Title", "Find Worlds")
		setControlEnabled("stcSearch",          false)
		setControlEnabled("edSearch",           false)
		setControlEnabled("btnSearch",          false)
		setControlEnabled("stcWorldFilter",     false)
		setControlEnabled("cboWorldFilter",     false)
		setControlEnabled("stcCommunityCategories", false)
		setControlEnabled("cboCommunityCategories", false)
		setControlEnabled("chkOnlyMyCommunities",   false)
		setControlEnabled("chkOnlyMyCountry",   false)
		setControlEnabled("chkShowAllAge",   false)
		setControlTabs(TAB_NAMES, TAB_THIS_WORLD)
		setControlEnabled("stcLoadingAnim", false)
		setControlEnabled("imgLoadingAnim", false)
		MenuOpen("Framework_Overview")

		local curLoc = MenuGetLocationThis()
		MenuSetLocation("Framework_Overview", curLoc.x, curLoc.y + OVERVIEW_OFFSET)
		local event = KEP_EventCreate("FRAMEWORK_OPEN_OVERVIEW")
		KEP_EventEncodeString(event, Events.encode(g_tOverviewSettings))
		KEP_EventQueue(event)
	end

	if g_mature ~= nil and g_iHasAccessPass ~= nil and g_startingCategory ~= nil then
		if (tab == TAB_WORLDS) and g_visited == 1 then 
			setTab(g_currentTab,4) 
		else 
			setTab(g_currentTab) 
		end 
	end 
end

function recentlyVisited(enabled, worlds)
	Control_SetVisible(gHandles["RecentlyVisited"], enabled)
	Control_SetVisible(gHandles["RecentlyVisitedHeader"], enabled)
	Control_SetVisible(gHandles["imgVertLineRht"], not enabled)
	Control_SetVisible(gHandles["imgVertLineLft"], not enabled) 
	for i=1,worlds do 
		Control_SetVisible(gHandles["imgVisited"..i], enabled)
		Control_SetVisible(gHandles["stcVisited"..i], enabled)
		Control_SetVisible(gHandles["imgVisitedBG"..i], enabled)
		Control_SetVisible(gHandles["imgVisitedFrame"..i], enabled)
		Control_SetVisible(gHandles["btnVisited"..i], enabled)
	end 
end 

function displayPlaces(hideOnly)
	local onlyHideControls = true
	if (hideOnly ~= nil) then
		onlyHideControls = hideOnly
	end

	if not g_gotWorlds and not g_externalCall then
		-- Awaiting world list for recently visited
		return	
	end
	
	recentlyVisited(false, MAX_WORLDS)
	clearPlaces()
	setTopWorldControlVisibility()

	-- displayPlaces() is called twice on page 1 of Most Popular category. There are 2 web calls that are made on Page 1.
	-- First is travelzonelist.aspx?action=MostVisited and second is travelzonelist.aspx?action=MostPopulatedCombined
	-- MostVisited returns the Recently Visited Worlds list, MostPopulatedCombined returns the worlds list (up to 6 on Page 1)
	-- If we are only hiding the controls, then we don't want to hide the loading icon because this will cause it to disappear
	-- and then reappear on second call and then hide when worlds load, so it appears to flash to the user. 
	if (onlyHideControls) then
		setControlEnabled("stcLoadingAnim", false)
		setControlEnabled("imgLoadingAnim", false)
	end 

	--log("--- g_tab: ".. tostring(g_tab))
	--log("--- g_category: ".. tostring(g_category))
	local category = g_categories[g_tab][g_category]
	local initRecentlyVisited = (g_recentlyVisited and category.name == "Most Popular")
	
	local filteredResultset = false
	local searchText = EditBox_GetText(gHandles["edSearch"])
	if ((searchText ~= nil and searchText ~= "") or getControlChecked("chkAccessPass")) then
		filteredResultset = true
	end
	
	local numWorlds = #g_worlds
	local totalWorlds = TOTAL_ROWS
	if (initRecentlyVisited and numWorlds > 0) or g_isTopWorlds then 
		totalWorlds = 6
	end 
	if g_currentPage ~=1 then 
		totalWorlds = TOTAL_ROWS
	end 
	
	local numPlaces = math.min(totalWorlds, #g_placeData)
	--recently visited 
	if (not filteredResultset and numPlaces == 0 and g_currentTab == TAB_MY_WORLDS) then
		displayPageButtons(false)
		return
	else
		g_offset = 1
		if (initRecentlyVisited and numWorlds ~= 0) or g_isTopWorlds then  
				g_offset = 4
		end  
		if g_currentPage ~=1 then 
			g_offset = 1
		end 
		
		-- If we are only hiding controls, we do not want to set the visible
		if (onlyHideControls) then
			for i=1,(g_offset-1) do
				toggleButton(i, false)
			end
		end
		 
		local placeData = 1
        for i=g_offset,(g_offset+numPlaces-1) do

			if initRecentlyVisited or g_isTopWorlds then 
				if g_offset > 1 then 
					placeData = g_placeData[i-(g_offset-1)]
				else 
					placeData = g_placeData[i]
				end 
				if g_currentPage ~=1 then 
					placeData = g_placeData[i]
				end 
			else 
				placeData = g_placeData[i]
			end 
			
            -- If we are only hiding controls, we do not want to set the visible
			if (onlyHideControls) then
				toggleButton(i, true)
			end

			if placeData ~= nil then 
				setControlText("stcName_Row"        .. i, placeData.location)
				setControlText("stcOwnerName_Row"   .. i, placeData.ownerName)
				setControlText("stcDescription_Row" .. i, placeData.description)

				scaleImage(gHandles["imgObjectPic_Row" .. i], gHandles["imgObjectBG_Row" .. i], placeData.filename)
			
				if (placeData.canDropGems == "True") and g_showGems then
					Control_SetVisible(gHandles["btnGem_Row" .. i], true)
				end

				if (g_currentTab == TAB_WORLDS) then
					setControlEnabled("stcManage" ..i, false) 
					
				--hide recently visited if not in AB test 
					if initRecentlyVisited and numWorlds>0 then  
						recentlyVisited(true, numWorlds)
						for item=1,numWorlds do
								local world = g_worlds[item]
								if world ~= nil then
									setControlText("stcVisited"..item, world.location)
									scaleImage(gHandles["imgVisited" .. item], gHandles["imgVisitedFrame" .. item], world.filename)

								end
						end	
						if g_currentPage ~=1 then 
							recentlyVisited(false, MAX_WORLDS)
						end 
					else 
						recentlyVisited(false, MAX_WORLDS)
						-- for a new user who has never traveled w/ Recently Visited B/C test 
						if initRecentlyVisited and numWorlds==0 then 
							--only do this the first time
							if g_noWorldsRV == 0 then 
								g_noWorldsRV = g_noWorldsRV + 1
								getPlaces()
								return
							end 
						end 
						
					end 
					
					if placeData.worldReqs and (placeData.worldReqs > 0) and not g_isTopWorlds then 
						if (value > 99) then
							value = 99
						end
						toggleButtonDetails(i, gBP_DisplayMode["REQUESTS"])
						setControlText("stcRequestsCount_Row" .. i, value)
					elseif placeData.population and (placeData.population > 0) and not g_isTopWorlds then
						toggleButtonDetails(i, gBP_DisplayMode["POPULATION"])
						local vipOnly, population = GetPopulationText(placeData.population)
						setControlText("stcPopulationCount_Row"   .. i, population)
						if initRecentlyVisited and numWorlds>0 then 
							for j=1,(g_offset-1) do
								toggleButtonDetails(j, gBP_DisplayMode["NONE"])
							end
						end 
					elseif filteredResultset then
						toggleButtonDetails(i, gBP_DisplayMode["NONE"])
					else
						-- If we are only hiding controls, we do not want to set the visible
						if (onlyHideControls) then
							toggleButtonDetails(i, gBP_DisplayMode["RAVES"])
							setControlText("stcRavesCount_Row" .. i, placeData.raveCount)
						end
					end

					if g_categories[g_currentTab][g_category].name == "Most Visited" then 
						toggleButtonDetails(i, gBP_DisplayMode["VISITS"])
						setControlEnabled("stcManage" ..i, false) 
						setControlText("stcMonthlyVisitsCount_Row"    .. i, LongNumberToString(placeData.viewCount))	
					end 
					if g_categories[g_currentTab][g_category].name == "Most Requested" then 
						toggleButtonDetails(i, gBP_DisplayMode["REQUESTS"])
						setControlEnabled("stcManage" ..i, false) 
						setControlText("stcRequestsCount_Row" .. i, placeData.worldReqs)	
					end 
				elseif (g_currentTab == TAB_MY_WORLDS) then
					recentlyVisited(false, MAX_WORLDS)
					toggleButtonDetails(i, gBP_DisplayMode["VISITS"])
					setControlText("stcMonthlyVisitsCount_Row"    .. i, LongNumberToString(placeData.viewCount))
					setControlEnabled("stcManage" ..i, true) 
				end
				
				if gHandles["imgTopWorldVisited"..i] and gHandles["stcTopWorldVisited"..i] then
					setControlEnabled("imgTopWorldVisited"..i, placeData.topVisited)
					setControlEnabled("stcTopWorldVisited"..i, placeData.topVisited)
				end

				if placeData.isAP then
					setControlEnabled("imgObjectAP_Row"..i, true)
				end
			else
				clearPlaces()
			end
        end
		
		if g_isTopWorlds then
			recentlyVisited(false, MAX_WORLDS)
			if gHandles["stcTopWorldsRewardText"] and g_topWorldLoot then
				setControlText("stcTopWorldsRewardText", "Collect up to "..tostring(g_topWorldLoot).." rewards in each Top World")
			end
			
			if gHandles["stcTopWorldsRewardLabel"] and g_topWorldBounty then
				local bountyString = formatNumberSuffix(g_topWorldBounty)
				for i = 1, 4 do
					if gHandles["stcTopWorldsRewardLabelShadow"..i] then
						setControlText("stcTopWorldsRewardLabelShadow"..i, bountyString)
					end
				end
				setControlText("stcTopWorldsRewardLabel", bountyString)
			end
		end
        displayPageButtons(true)
	end
end

function toggleButton(i, enable)
	for _,ctrlName in pairs(gctrls_Button["Common"]) do
		setControlEnabled(ctrlName..i, enable)
	end
	setControlTexture("imgObjectPic_Row"   .. i, "loading.dds")
	Element_SetCoords(Image_GetDisplayElement(gHandles["imgObjectPic_Row" .. i]), 0, 0, -1, -1)		-- use entire image for texturing
end

function toggleButtonDetails(btnNum, ctrlArray)

	-- Hide
	for _,panelName in pairs(ctrlArray.Panels["hide"]) do
		for _,ctrlName in pairs(gctrls_Button[panelName]) do
			Control_SetVisible(gHandles[ctrlName .. tostring(btnNum)], false)
			Control_SetEnabled(gHandles[ctrlName .. tostring(btnNum)], false)
		end
	end

	-- Show
	for _,panelName in pairs(ctrlArray.Panels["show"]) do
		for _,ctrlName in pairs(gctrls_Button[panelName]) do
			Control_SetVisible(gHandles[ctrlName .. tostring(btnNum)], true)
			Control_SetEnabled(gHandles[ctrlName .. tostring(btnNum)], true)
		end
	end
end

function displayPageButtons(enable)

	-- Display 'Next' Button
	local enableNext = (enable and  (g_currentPage < g_totalPages))
	Control_SetEnabled(gHandles["btnNextPage"], enableNext)

	-- Display 'Back' Button
	local enableBack = (enable and (g_currentPage > 1))
	Control_SetEnabled(gHandles["btnPrevPage"], enableBack)		
	-- Display Current Page Number
	setControlEnabled("lblCurrentPageItems", enable)
	if (g_lastSearchText == "") then
		setControlText("lblCurrentPageItems", "Page "..LongNumberToString(g_currentPage))
	else
		local firstItem = ((g_currentPage - 1) * TOTAL_ROWS) + math.min(1, #g_placeData)
		setControlText("lblCurrentPageItems", LongNumberToString(firstItem) .. "-".. LongNumberToString(math.max(0,(firstItem + #g_placeData - 1))) .. " of " .. LongNumberToString(g_totalRecs) .. " found for \"" .. g_lastSearchText .. "\"")
	end
end

function clearPlaces()
	recentlyVisited(false, MAX_WORLDS)
	for i=1,TOTAL_ROWS do
		toggleButton(i, false)
		setControlEnabled("imgTopWorldVisited"..i, false)
		setControlEnabled("stcTopWorldVisited"..i, false)
		Control_SetVisible(gHandles["btnGem_Row" .. i], false)
		toggleButtonDetails(i, gBP_DisplayMode["NONE"])
	end
	displayPageButtons(false)
	setControlText("lblCurrentPageItems", "")
end

--Side Menus

function btnHome_OnButtonClicked(buttonHandle)
	if g_inTut then 
		clickedInTut()
	end
	gotoURL(GetHomeLink())
end

function btnEvents_OnButtonClicked(buttonHandle)
	if g_inTut then 
		clickedInTut()
	end
	MenuOpen("Events.xml")
end

function btnDash_OnButtonClicked(buttonHandle)
	MenuOpen("Dashboard.xml")
	if not g_showLightbox then
		DestroyThisMenu()
	end
end

function btnDashNew_OnButtonClicked(buttonHandle)
	MenuOpen("Dashboard.xml")
	if not g_showLightbox then
		DestroyThisMenu()
	end
end 

function btnCreateWorld_OnButtonClicked(buttonHandle)
	MenuOpen("CreateYourWorld.xml")
	if not g_showLightbox then
		DestroyAllMenus()
	end
end

function btnCreateWorld2_OnButtonClicked(buttonHandle)
	MenuOpen("CreateYourWorld.xml")
	if not g_showLightbox then
		DestroyAllMenus()
	end
end

function btnCreateWorldSplash_OnButtonClicked(buttonHandle)
	MenuOpen("CreateYourWorld.xml")
	if not g_showLightbox then
		DestroyAllMenus()
	end
end

function btnWorlds_OnButtonClicked(buttonHandle)
	changeTab(TAB_WORLDS)
end

function btnMyWorlds_OnButtonClicked(buttonHandle)
	changeTab(TAB_MY_WORLDS)
end

function btnThisWorld_OnButtonClicked(buttonHandle)
	changeTab(TAB_THIS_WORLD)
end

function showPassMessage(type, message)
	MenuOpen("TravelNotice.xml")

	local ev = KEP_EventCreate("TravelNoticeEvent")
	KEP_EventEncodeNumber(ev, type)
	KEP_EventEncodeString(ev, message)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeString(ev, "")
	KEP_EventQueue(ev)
end

function showAP()
	showPassMessage(2, "Access Pass allows you to explore content and features only available to an exclusive 18 and over community.")
end

function showVIP()
	showPassMessage(3, "The VIP Pass allows you to explore content and features only available to VIP members.")
end

function DestroyThisMenu()
    MenuCloseThis()
end

function DestroyAllMenus()
    MenuClose("Dashboard.xml") -- DRF - Close Dashboard Too
    DestroyThisMenu()
end

function goToLocation(index)
	if searchLogID~=nil and searchLogID~=0 then 
		local url = GameGlobals.WEB_SITE_PREFIX .. "kgp/travelZoneList.aspx?action=SearchConvert&SearchLogId="..tostring(searchLogID)
		makeWebCall(url,0)
	end 
				
	local placeData = g_placeData[index]
	if (g_recentlyVisited and g_categories[g_tab][g_category].name == "Most Popular") or g_isTopWorlds then 
		if g_offset>1 then 
			placeData = g_placeData[index-(g_offset-1)]
		end 
	end 
	if (placeData == nil) then
		return
	else
		DestroyAllMenus()
		gotoURL(placeData.kurl)
	end
end

function goToOwnerProfile(index)
	local placeData = g_placeData[index]
	local web_address = GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_CHANNEL_PATH .. placeData.ownerName .. ".people"
	KEP_LaunchBrowser( web_address)
end

----------- Per Row button click handlers --------------

------ GO buttons -------
function btnGo_Row1_OnButtonClicked(buttonHandle)
	if buttonOn == true then  
		goToLocation(1)
	else  
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid=" .. tostring(g_placeData[buttonIndex].id))
	end 
end

function btnGo_Row2_OnButtonClicked(buttonHandle)
	if buttonOn == true then 
		goToLocation(2)
	else
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid=" .. tostring(g_placeData[buttonIndex].id))
	end 
end

function btnGo_Row3_OnButtonClicked(buttonHandle)
	if buttonOn == true then 
		goToLocation(3)
	else
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid=" .. tostring(g_placeData[buttonIndex].id))
	end 
end

function btnGo_Row4_OnButtonClicked(buttonHandle)
	if buttonOn == true then 
		goToLocation(4)
	else
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid=" .. tostring(g_placeData[buttonIndex].id))
	end 
end

function btnGo_Row5_OnButtonClicked(buttonHandle)
	if buttonOn == true then 
		goToLocation(5)
	else
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid=" .. tostring(g_placeData[buttonIndex].id))
	end 
end

function btnGo_Row6_OnButtonClicked(buttonHandle)
	if buttonOn == true then 
		goToLocation(6)
	else
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid=" .. tostring(g_placeData[buttonIndex].id))
	end 
end

function btnGo_Row7_OnButtonClicked(buttonHandle)
	if buttonOn == true then 
		goToLocation(7)
	else
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid=" .. tostring(g_placeData[buttonIndex].id))
	end 
end

function btnGo_Row8_OnButtonClicked(buttonHandle)
	if buttonOn == true then 
		goToLocation(8)
	else
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid=" .. tostring(g_placeData[buttonIndex].id))
	end 
end

function btnGo_Row9_OnButtonClicked(buttonHandle)
	if buttonOn == true then 
		goToLocation(9)
	else
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid=" .. tostring(g_placeData[buttonIndex].id))
	end 
end


------- Gem Icons ---------

function btnGem_Row1_OnButtonClicked(buttonHandle)
	btnGo_Row1_OnButtonClicked(buttonHandle)
end

function btnGem_Row2_OnButtonClicked(buttonHandle)
	btnGo_Row2_OnButtonClicked(buttonHandle)
end

function btnGem_Row3_OnButtonClicked(buttonHandle)
	btnGo_Row3_OnButtonClicked(buttonHandle)
end

function btnGem_Row4_OnButtonClicked(buttonHandle)
	btnGo_Row4_OnButtonClicked(buttonHandle)
end

function btnGem_Row5_OnButtonClicked(buttonHandle)
	btnGo_Row5_OnButtonClicked(buttonHandle)
end

function btnGem_Row6_OnButtonClicked(buttonHandle)
	btnGo_Row6_OnButtonClicked(buttonHandle)
end

function btnGem_Row7_OnButtonClicked(buttonHandle)
	btnGo_Row7_OnButtonClicked(buttonHandle)
end

function btnGem_Row8_OnButtonClicked(buttonHandle)
	btnGo_Row8_OnButtonClicked(buttonHandle)
end

function btnGem_Row9_OnButtonClicked(buttonHandle)
	btnGo_Row9_OnButtonClicked(buttonHandle)
end

------- Owner Name ---------

function btnOwnerName_Row1_OnButtonClicked(buttonHandle)
	if buttonOn == false then 
		goToOwnerProfile(1)
	end 
end

function btnOwnerName_Row2_OnButtonClicked(buttonHandle)
	if buttonOn == false  then 
		goToOwnerProfile(2)
	end 
end

function btnOwnerName_Row3_OnButtonClicked(buttonHandle)
	if buttonOn == false  then 
		goToOwnerProfile(3)
	end 
end

function btnOwnerName_Row4_OnButtonClicked(buttonHandle)
	if buttonOn == false  then 
		goToOwnerProfile(4)
	end 
end

function btnOwnerName_Row5_OnButtonClicked(buttonHandle)
	if buttonOn == false  then 
		goToOwnerProfile(5)
	end 
end

function btnOwnerName_Row6_OnButtonClicked(buttonHandle)
	if buttonOn == false  then 
		goToOwnerProfile(6)
	end 
end

function btnOwnerName_Row7_OnButtonClicked(buttonHandle)
	if buttonOn == false  then 
		goToOwnerProfile(7)
	end 
end

function btnOwnerName_Row8_OnButtonClicked(buttonHandle)
	if buttonOn == false  then 
		goToOwnerProfile(8)
	end 
end

function btnOwnerName_Row9_OnButtonClicked(buttonHandle)
	if buttonOn == false  then 
		goToOwnerProfile(9)
	end 
end

------- VIP Only button -------

function btnVIPOnly_Row1_OnButtonClicked(buttonHandle)
	showVIP()
end

function btnVIPOnly_Row2_OnButtonClicked(buttonHandle)
	showVIP()
end

function btnVIPOnly_Row3_OnButtonClicked(buttonHandle)
	showVIP()
end

function btnVIPOnly_Row4_OnButtonClicked(buttonHandle)
	showVIP()
end

function btnVIPOnly_Row5_OnButtonClicked(buttonHandle)
	showVIP()
end

function btnVIPOnly_Row6_OnButtonClicked(buttonHandle)
	showVIP()
end

function btnVIPOnly_Row7_OnButtonClicked(buttonHandle)
	showVIP()
end

function btnVIPOnly_Row8_OnButtonClicked(buttonHandle)
	showVIP()
end

function btnVIPOnly_Row9_OnButtonClicked(buttonHandle)
	showVIP()
end

------------- Reasons to refresh the results -------------------

function DisplayPage(newPage)
	--Log("DisplayPage: page="..newPage)

	-- Update Current Page
	g_currentPage = newPage

	-- Get Places & Display
	getPlaces()
end

function btnPrevPage_OnButtonClicked(buttonHandle)
	--Log("PrevPageClicked")
	-- Only set variable if we are navigating go first page
	if (g_currentPage - 1 < 2) then
		g_onlyHideControls = true		
	end
	DisplayPage(g_currentPage - 1)
end

function btnNextPage_OnButtonClicked(buttonHandle)
	--Log("NextPageClicked")
	DisplayPage(g_currentPage + 1)
end

function btnTopWorldsTips_OnButtonClicked(buttonHandle)
	MenuToggleOpenClose("Framework_TopWorldInfo.xml")
end

function cboCommunityCategories_OnComboBoxSelectionChanged(comboBoxHandle, sel_index, sel_text)
	--Log("CategoriesComboChanged")
	DisplayPage(1)
end

function chkAccessPass_OnCheckBoxChanged(checkBoxHandle, checked)
	--Log("chkAccessPassChanged")
	if g_iHasAccessPass then
		DisplayPage(1)
	elseif checked then
		showAP()
		CheckBox_SetChecked(checkBoxHandle, false)
	end
end

function chkOnlyMyCommunities_OnCheckBoxChanged(checkBoxHandle, checked)
	--Log("OnlyMyCommunitiesClicked")
	DisplayPage(1)
end

function chkOnlyMyCountry_OnCheckBoxChanged(checkBoxHandle, checked)
	--Log("OnlyMyCountriesClicked")
	DisplayPage(1)
end

function chkShowAllAge_OnCheckBoxChanged(checkBoxHandle, checked)
	--Log("ShowAllAgeClicked")
	DisplayPage(1)
end

function edSearch_OnEditBoxString(editBoxHandle, text)
	btnSearch_OnButtonClicked(gHandles["btnSearch"])
end

function btnSearch_OnButtonClicked(buttonHandle)
	local searchText = EditBox_GetText(gHandles["edSearch"])
	--Log("SearchClicked: searchText='"..searchText.."'")
	if findAPworlds == true then
		Log("You don't have access to AP worlds.") 
	else 
		DisplayPage(1)
	end 	
end

function Dialog_OnMouseMove(dialogHandle, x, y)

	-- When the mouse moves over a place box, highlight it.
	for i=1,TOTAL_ROWS do
		if Control_ContainsPoint(gHandles["btnGo_Row"..i], x, y) == 0 then
			Element_SetCoords(Image_GetDisplayElement(gHandles["imgGrad_Row"..i]), g_gradCoords[1], g_gradCoords[2], g_gradCoords[3], g_gradCoords[4])
		else
			Element_SetCoords(Image_GetDisplayElement(gHandles["imgGrad_Row"..i]), g_gradCoords[1], g_gradCoords[4], g_gradCoords[3], g_gradCoords[2])
			buttonOn = true 
			buttonIndex = i 
			if g_tab == TAB_MY_WORLDS then 
				if Control_ContainsPoint(gHandles["stcManage"..i], x, y) == 1 then
					buttonOn = false 
				end 
			end 
		end
	end
end


function GetPopulationText(popCount)
	local vipOnly = false
	local popText = "Empty"
	if (popCount > 0 and popCount <= 2) then
		popText = "Low"
	elseif (popCount > 2 and popCount <= 6) then
		popText = "Medium"
	elseif (popCount > 6 and popCount <= 10) then
		popText = "High"
	elseif (popCount > 10) then
		--vipOnly = true
		popText = "High"
	end

	return vipOnly, popText
end

function formatNumberSuffix(count)
	-- If no need for formatting, then return
	if count < 1000 then return count end

	local strCount = tostring(count)
	local newCount = ""

	local format = { 	max = 0,
						min = 0,
					 	suffix = ""
					}

	-- Thousands
	if #strCount <= 6 then
		format.max = 6
		format.min = 3
		format.suffix = "k"
	-- Millions
	elseif #strCount <= 9 then
		format.max = 9 
		format.min = 6
		format.suffix = "M"
	-- Billions
	elseif #strCount <= 12 then
		format.max = 12
		format.min = 9
		format.suffix = "B"
	-- Trillions
	elseif #strCount <= 15 then
		format.max = 15
		format.min = 12
		format.suffix = "T"
	end

	for i = 1, #strCount do
		local diff = #strCount - i
		if #strCount <= format.max then
			if diff >= format.min then
				newCount = newCount..string.sub(strCount,i,i)
			else
				if tonumber(string.sub(strCount,i,i)) > 0 then
					newCount = newCount.."."..string.sub(strCount,i,i)
				end
				return newCount..format.suffix
			end
		end
	end
end

function visitedButton_OnButtonClicked( buttonHandle, buttonNum)
	if Control_GetVisible(buttonHandle) then
	   local url = g_worlds[buttonNum].url
	   if url ~= nil then
			gotoURL(url)
	   end
	end
end

function btnVisited1_OnButtonClicked( buttonHandle )
	visitedButton_OnButtonClicked(buttonHandle, 1)
end

function btnVisited2_OnButtonClicked( buttonHandle )
	visitedButton_OnButtonClicked(buttonHandle, 2)
end

function btnVisited3_OnButtonClicked( buttonHandle )
	visitedButton_OnButtonClicked(buttonHandle, 3)
end

function btnVisited4_OnButtonClicked( buttonHandle )
	visitedButton_OnButtonClicked(buttonHandle, 4)
end

function btnVisited5_OnButtonClicked( buttonHandle )
	visitedButton_OnButtonClicked(buttonHandle, 5)
end

function btnVisited6_OnButtonClicked( buttonHandle )
	visitedButton_OnButtonClicked(buttonHandle, 6)
end

function btnVisited7_OnButtonClicked( buttonHandle )
	visitedButton_OnButtonClicked(buttonHandle, 7)
end

function btnVisited8_OnButtonClicked( buttonHandle )
	visitedButton_OnButtonClicked(buttonHandle, 8)
end

function btnVisited9_OnButtonClicked( buttonHandle )
	visitedButton_OnButtonClicked(buttonHandle, 9)
end

function initTooltips()
    for i = 1, 9 do    
        Control_SetToolTip(gHandles["btnGem_Row" ..i], "Gems can be exchanged for Rewards or exciting stuff!")
    end
end