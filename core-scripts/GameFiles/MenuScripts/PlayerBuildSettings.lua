--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Menu.lua - Kaneva Menu Script Template
-- 
-- Template script for creating a new Kaneva client menu.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\MenuHelper.lua")

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) -- REQUIRED - DO NOT REMOVE
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle ) -- REQUIRED - DO NOT REMOVE
end

function cbGridSnapping_OnCheckBoxChanged( cbHandle, checked )
	updatePlayerSnapping()
end

function cbRotSnapping_OnCheckBoxChanged( cbHandle, checked )
	updatePlayerSnapping()
end

function cbObjectSnapping_OnCheckBoxChanged( cbHandle, checked )
	updatePlayerSnapping()
end

function edGridSpacing_OnEditBoxString(editBoxHandle, password)
	updatePlayerSnapping()
end

function edGridSpacing_OnFocusOut(editBoxHandle)
	updatePlayerSnapping()
end

function edRotSpacing_OnEditBoxString(editBoxHandle, password)
	updatePlayerSnapping()
end

function edRotSpacing_OnFocusOut(editBoxHandle)
	updatePlayerSnapping()
end

function edSnapDistance_OnEditBoxString(editBoxHandle, password)
	updatePlayerSnapping()
end

function edSnapDistance_OnFocusOut(editBoxHandle)
	updatePlayerSnapping()
end

function updatePlayerSnapping()

	--Get Snapping Settings (Selection.lua validates and enforces all setting values)
	local rotation = tonumber(EditBox_GetText(gHandles["edRotSpacing"])) or 0
	local spacing = tonumber(EditBox_GetText(gHandles["edGridSpacing"])) or 0
	local distance = tonumber(EditBox_GetText(gHandles["edSnapDistance"])) or 0
	local snapGrid = CheckBox_GetChecked(gHandles["cbGridSnapping"])
	local snapRot = CheckBox_GetChecked(gHandles["cbRotSnapping"])
	local snapObject = CheckBox_GetChecked(gHandles["cbObjectSnapping"])

	-- Send EnableGridEvent
	Log("updatePlayerSnapping: snapGrid="..tostring(snapGrid==1).." snapRot="..tostring(snapRot==1).." snapObject="..tostring(snapObject==1).." rotation="..rotation.." spacing="..spacing.." distance="..distance)
	ev = KEP_EventCreate("PlayerBuildSettingsEvent")
	KEP_EventEncodeNumber(ev, snapGrid)
	KEP_EventEncodeNumber(ev, snapRot)
	KEP_EventEncodeNumber(ev, snapObject)
	KEP_EventEncodeNumber(ev, spacing)
	KEP_EventEncodeNumber(ev, rotation)
	KEP_EventEncodeNumber(ev, distance)
	KEP_EventQueue(ev)
end
