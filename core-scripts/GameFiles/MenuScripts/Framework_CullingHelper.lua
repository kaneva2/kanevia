--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_CullingHelper.lua
-- 

--dofile("Lib_Debug.lua")

dofile("..\\Scripts\\LWG.lua")
dofile("..\\Scripts\\ObjectType.lua")
dofile("..\\MenuScripts\\Framework_InventoryHelper.lua")


CullingHelper = {}

--------------------------------------------------------------
-- Local variables
--------------------------------------------------------------

local s_geometries = {
	[1] = {
		geometryID = -1,
		boxAABB = {
			minX = -10,
			maxX = 10,
			minY = 0,
			maxY = 20,
			minZ = -10,
			maxZ = 10
		},
		colors = {
			ambient = { r = 0.0, g = 1.0, b = 0.0, a = 0.5 },
		    diffuse = { r = 0.0, g = 1.0, b = 0.0, a = 0.5 }
		}
	}
}

local s_boxAABB = {
	minX = -10,
	maxX = 10,
	minY = 0,
	maxY = 20,
	minZ = -10,
	maxZ = 10
}

local s_colors = {
	ambient = { r = 0.0, g = 1.0, b = 0.0, a = 0.5 },
	diffuse = { r = 0.0, g = 1.0, b = 0.0, a = 0.5 }
}

local s_geometryID = -1
local s_bBoxDisplayEnabled = false
local s_bIsInteriorObjectDisplayEnabled = true
local s_bIsExteriorObjectDisplayEnabled = true
local s_iSensitivity = 1
local s_bIsCullingInterior = true
--------------------------------------------------------------
-- Forward declarations for local functions
--------------------------------------------------------------
local generateGeometry
local DestroyGeometry
local IsDisplayEnabled 
local SetDisplayEnabled -- (bEnabled)

--------------------------------------------------------------
-- Interface functions
--------------------------------------------------------------


-- Multiple boxes?



function CullingHelper.Init()
end

function CullingHelper.DeInit()
	DestroyGeometry()
end

function CullingHelper.HandleInput(key, shift, ctrl, alt)
	if key == Keyboard.F4 then
		CullingHelper.ToggleBoxDisplay()
	end
	
	if key == Keyboard.F1 then
		CullingHelper.ToggleDynamicObjectVisibilityInBox()
	end
	
	if key == Keyboard.F3 then
		local x, y, z, r = KEP_GetPlayerPosition()
		CullingHelper.RecenterBox(x, y, z)
	end
	
	if key == Keyboard.F2 then
		CullingHelper.InvertCulling()
	end
	

	if key == Keyboard.INSERT then
		if ctrl then
			CullingHelper.ChangeSizeOfBoxAlongX(1.0)
		elseif shift then
			CullingHelper.SetSensitivity(4)
		else
			CullingHelper.MoveBoxAlongX(1.0)
		end
	end
	
	if key == Keyboard.DEL then
		if ctrl then
			CullingHelper.ChangeSizeOfBoxAlongX(-1.0)
		elseif shift then
			CullingHelper.SetSensitivity(1)
		else
			CullingHelper.MoveBoxAlongX(-1.0)
		end
	end
	
	if key == Keyboard.HOME then
		if ctrl then
			CullingHelper.ChangeSizeOfBoxAlongY(1.0)
		elseif shift then
			CullingHelper.SetSensitivity(5)
		else
			CullingHelper.MoveBoxAlongY(1.0)
		end
	end
	
	if key == Keyboard.END then
		if ctrl then
			CullingHelper.ChangeSizeOfBoxAlongY(-1.0)
		elseif shift then
			CullingHelper.SetSensitivity(2)
		else
			CullingHelper.MoveBoxAlongY(-1.0)
		end
	end
	
	if key == Keyboard.PAGE_UP then
		if ctrl then
			CullingHelper.ChangeSizeOfBoxAlongZ(1.0)
		elseif shift then
			CullingHelper.SetSensitivity(6)
		else
			CullingHelper.MoveBoxAlongZ(1.0)
		end
	end
	
	if key == Keyboard.PAGE_DOWN then
		if ctrl then
			CullingHelper.ChangeSizeOfBoxAlongZ(-1.0)
		elseif shift then
			CullingHelper.SetSensitivity(3)
		else
			CullingHelper.MoveBoxAlongZ(-1.0)
		end
	end
end


function CullingHelper.InvertCulling()
	s_bIsCullingInterior = not s_bIsCullingInterior
	
	if s_bIsCullingInterior then
		-- displayStatusMessage("Interior culling mode",  {r = 200, g = 200, b = 200})
	else
		-- displayStatusMessage("Exterior culling mode",  {r = 200, g = 200, b = 200})
	end
end

function CullingHelper.RecenterBox(x, y, z)
	local extentX = (s_geometries[1].boxAABB.maxX - s_geometries[1].boxAABB.minX) / 2
	local extentY = (s_geometries[1].boxAABB.maxY - s_geometries[1].boxAABB.minY) / 2
	local extentZ = (s_geometries[1].boxAABB.maxZ - s_geometries[1].boxAABB.minZ) / 2
	
	s_geometries[1].boxAABB.minX = x - extentX 
	s_geometries[1].boxAABB.maxX = x + extentX
	s_geometries[1].boxAABB.minY = y
	s_geometries[1].boxAABB.maxY = y + 2*extentY
	s_geometries[1].boxAABB.minZ = z - extentZ
	s_geometries[1].boxAABB.maxZ = z + extentZ
	
	if s_bBoxDisplayEnabled then
		-- Recreate the geometry
		GenerateGeometry()
	end
	
	if s_bIsCullingInterior then
		-- displayStatusMessage("Culling box recentered at player location", {r = 200, g = 200, b = 200})
	end
end

function CullingHelper.SetSensitivity(iSensitivity)
	--log("CullingHelper.SetSensitivity: " .. tostring(iSensitivity))
	iSensitivity = tonumber(iSensitivity) or 1
	
	-- displayStatusMessage("Culling sensitivity set to " .. tostring(iSensitivity), {r = 200, g = 200, b = 200})
	
	-- Cap the sensitivity between 1 and 10
	if iSensitivity < 1 then
		iSensitivity = 1
	elseif iSensitivity > 10 then
		iSensitivity = 10
	end
	
	-- Take the square of the value
	s_iSensitivity = iSensitivity * iSensitivity
end


function CullingHelper.MoveBoxAlongX(shiftAmount)
	shiftAmount = tonumber(shiftAmount) or 0
	shiftAmount = shiftAmount * s_iSensitivity
	
	s_geometries[1].boxAABB.minX = s_geometries[1].boxAABB.minX + shiftAmount
	s_geometries[1].boxAABB.maxX = s_geometries[1].boxAABB.maxX + shiftAmount
	
	if s_bBoxDisplayEnabled then
		-- Recreate the geometry
		GenerateGeometry()
	end
end

function CullingHelper.MoveBoxAlongY(shiftAmount)
	shiftAmount = tonumber(shiftAmount) or 0
	shiftAmount = shiftAmount * s_iSensitivity
	
	s_geometries[1].boxAABB.minY = s_geometries[1].boxAABB.minY + shiftAmount
	s_geometries[1].boxAABB.maxY = s_geometries[1].boxAABB.maxY + shiftAmount
	
	if s_bBoxDisplayEnabled then
		-- Recreate the geometry
		GenerateGeometry()
	end
end

function CullingHelper.MoveBoxAlongZ(shiftAmount)
	shiftAmount = tonumber(shiftAmount) or 0
	shiftAmount = shiftAmount * s_iSensitivity
	
	s_geometries[1].boxAABB.minZ = s_geometries[1].boxAABB.minZ + shiftAmount
	s_geometries[1].boxAABB.maxZ = s_geometries[1].boxAABB.maxZ + shiftAmount
	
	if s_bBoxDisplayEnabled then
		-- Recreate the geometry
		GenerateGeometry()
	end
end


function CullingHelper.ChangeSizeOfBoxAlongX(shiftAmount)
	shiftAmount = (tonumber(shiftAmount) / 2) or 0
	shiftAmount = shiftAmount * s_iSensitivity
	
	s_geometries[1].boxAABB.minX = s_geometries[1].boxAABB.minX - shiftAmount
	s_geometries[1].boxAABB.maxX = s_geometries[1].boxAABB.maxX + shiftAmount
	
	if s_geometries[1].boxAABB.maxX - s_geometries[1].boxAABB.minX < 2.0 then
		local centerX = (s_geometries[1].boxAABB.maxX + s_geometries[1].boxAABB.minX) / 2
		s_geometries[1].boxAABB.maxX = centerX + 1.0
		s_geometries[1].boxAABB.minX = centerX - 1.0
	end
	
	if s_bBoxDisplayEnabled then
		-- Recreate the geometry
		GenerateGeometry()
	end
end

function CullingHelper.ChangeSizeOfBoxAlongY(shiftAmount)
	shiftAmount = (tonumber(shiftAmount) / 2) or 0
	shiftAmount = shiftAmount * s_iSensitivity
	
	s_geometries[1].boxAABB.minY = s_geometries[1].boxAABB.minY - shiftAmount
	s_geometries[1].boxAABB.maxY = s_geometries[1].boxAABB.maxY + shiftAmount
	
	if s_geometries[1].boxAABB.maxY - s_geometries[1].boxAABB.minY < 2.0 then
		local centerY = (s_geometries[1].boxAABB.maxY + s_geometries[1].boxAABB.minY) / 2
		s_geometries[1].boxAABB.maxY = centerY + 1.0
		s_geometries[1].boxAABB.minY = centerY - 1.0
	end
	
	if s_bBoxDisplayEnabled then
		-- Recreate the geometry
		GenerateGeometry()
	end
end

function CullingHelper.ChangeSizeOfBoxAlongZ(shiftAmount)
	shiftAmount = (tonumber(shiftAmount) / 2) or 0
	shiftAmount = shiftAmount * s_iSensitivity
	
	s_geometries[1].boxAABB.minZ = s_geometries[1].boxAABB.minZ - shiftAmount
	s_geometries[1].boxAABB.maxZ = s_geometries[1].boxAABB.maxZ + shiftAmount
	
	if s_geometries[1].boxAABB.maxZ - s_geometries[1].boxAABB.minZ < 2.0 then
		local centerZ = (s_geometries[1].boxAABB.maxZ + s_geometries[1].boxAABB.minZ) / 2
		s_geometries[1].boxAABB.maxZ = centerZ + 1.0
		s_geometries[1].boxAABB.minZ = centerZ - 1.0
	end
	
	if s_bBoxDisplayEnabled then
		-- Recreate the geometry
		GenerateGeometry()
	end
end

function CullingHelper.SetSizeOfBox(newSizeX, newSizeY, newSizeZ)
	local tempNewSizeX = newSizeX or 10 -- TO DO: Configure these default values as constants
	local tempNewSizeY = newSizeY or 10
	local tempNewSizeZ = newSizeZ or 10

	extentX = tempNewSizeX/2 or 0
	extentY = tempNewSizeY/2 or 0
	extentZ = tempNewSizeZ/2 or 0
	
	local x = (s_geometries[1].boxAABB.minX + s_geometries[1].boxAABB.maxX)/2
	local y = (s_geometries[1].boxAABB.minY + s_geometries[1].boxAABB.maxY)/2
	local z = (s_geometries[1].boxAABB.minZ + s_geometries[1].boxAABB.maxZ)/2
	
	s_geometries[1].boxAABB.minX = x - extentX 
	s_geometries[1].boxAABB.maxX = x + extentX
	s_geometries[1].boxAABB.minY = y - extentY 
	s_geometries[1].boxAABB.maxY = y + extentY
	s_geometries[1].boxAABB.minZ = z - extentZ
	s_geometries[1].boxAABB.maxZ = z + extentZ
	
	if s_geometries[1].boxAABB.maxX - s_geometries[1].boxAABB.minX < 2.0 then
		local centerX = (s_geometries[1].boxAABB.maxX + s_geometries[1].boxAABB.minX) / 2
		s_geometries[1].boxAABB.maxX = centerX + 1.0
		s_geometries[1].boxAABB.minX = centerX - 1.0
	end
	
	if s_bBoxDisplayEnabled then
		-- Recreate the geometry
		GenerateGeometry()
	end
end

function CullingHelper.SetBoxColor(newRed, newGreen, newBlue)
	local redScaled = newRed / 255
	local greenScaled = newGreen / 255
	local blueScaled = newBlue / 255

	s_geometries[1].colors.ambient.r = redScaled
	s_geometries[1].colors.ambient.g = greenScaled
	s_geometries[1].colors.ambient.b = blueScaled
	s_geometries[1].colors.diffuse.r = redScaled
	s_geometries[1].colors.diffuse.g = greenScaled
	s_geometries[1].colors.diffuse.b = blueScaled

    if s_bBoxDisplayEnabled then
		-- Recreate the geometry
		GenerateGeometry()
	end
end

function CullingHelper.ToggleBoxDisplay(selected)
	local toggleState = false
	if selected then
		toggleState = selected
	else
		toggleState = not s_bBoxDisplayEnabled
	end
	CullingHelper.EnableBoxDisplay(toggleState)
	
	if s_bBoxDisplayEnabled then 
		-- displayStatusMessage("Culling box is displayed", {r = 200, g = 200, b = 200})
	else
		-- displayStatusMessage("Culling box is NOT displayed", {r = 200, g = 200, b = 200})
	end
end

function CullingHelper.EnableBoxDisplay(bEnabled)
	local bResult = bEnabled == true and s_bBoxDisplayEnabled == false
	if bEnabled == true and s_bBoxDisplayEnabled == false then
		GenerateGeometry()
		s_bBoxDisplayEnabled = true
	elseif bEnabled == false and s_bBoxDisplayEnabled == true then
		DestroyGeometry()
		s_bBoxDisplayEnabled = false
	end
end

function CullingHelper.IsBoxDisplayEnabled()
	return s_bBoxDisplayEnabled
end

function CullingHelper.ToggleDynamicObjectVisibilityInBox()
	CullingHelper.EnableDynamicObjectVisibilityInBox(not IsDisplayEnabled())
end

function CullingHelper.EnableDynamicObjectVisibilityInBox(bEnable)
	
	if bEnable then
		if s_bIsCullingInterior then
			-- displayStatusMessage("Enabled visibility inside the culling box", {r = 0, g = 255, b = 0})
		else
			-- displayStatusMessage("Enabled visibility outside the culling box", {r = 0, g = 255, b = 0})
		end
	else
		if s_bIsCullingInterior then
			-- displayStatusMessage("Disabled visibility inside the culling box", {r = 255, g = 0, b = 0})
		else
			-- displayStatusMessage("Disabled visibility outside the culling box", {r = 255, g = 0, b = 0})
		end
	end

	local uObjectCount = KEP_GetDynamicObjectCount()
	
	if uObjectCount > 0 then
		for i = 0, uObjectCount - 1 do
			local objectID = KEP_GetDynamicObjectPlacementIdByPos(i)
			local minX, minY, minZ, maxX, maxY, maxZ = KEP_DynamicObjectGetBoundingBox(objectID)
			
			local objectAABB = {minX = minX, minY = minY, minZ = minZ, maxX = maxX, maxY = maxY, maxZ = maxZ}
			
			local doesObjectIntersectBox = AABBTest(objectAABB, s_boxAABB)

			
			if s_bIsCullingInterior then
				if doesObjectIntersectBox then
					KEP_DynamicObjectSetVisibility(objectID, bEnable and 1.0 or 0.0)
				end
			else
				if not doesObjectIntersectBox then
					KEP_DynamicObjectSetVisibility(objectID, bEnable and 1.0 or 0.0)
				end
			end
			
		end
	end
	
	SetDisplayEnabled(bEnable)
end




--------------------------------------------------------------
-- Local functions
--------------------------------------------------------------


function IsDisplayEnabled()
	if s_bIsCullingInterior then
		return s_bIsInteriorObjectDisplayEnabled
	else
		return s_bIsExteriorObjectDisplayEnabled
	end
end

function SetDisplayEnabled(bEnabled)
	if s_bIsCullingInterior then
		s_bIsInteriorObjectDisplayEnabled = bEnabled
	else
		s_bIsExteriorObjectDisplayEnabled = bEnabled
	end
end

function AABBTest(AABB1, AABB2)
	if AABB1.minX > AABB2.maxX then
		return false
	elseif AABB1.maxX < AABB2.minX then
		return false
	elseif AABB1.minY > AABB2.maxY then
		return false
	elseif AABB1.maxY < AABB2.minY then
		return false
	elseif AABB1.minZ > AABB2.maxZ then
		return false
	elseif AABB1.maxZ < AABB2.minZ then
		return false
	end
	
	return true
end

function GenerateGeometry()

	-- Remove any previously created geometry
	DestroyGeometry()
	
	s_geometries[1].geometryID = KEP_CreateStandardGeometry{type=LWG.GT_BOX, min={x=s_geometries[1].boxAABB.minX, y=s_geometries[1].boxAABB.minY, z=s_geometries[1].boxAABB.minZ}, max={x=s_geometries[1].boxAABB.maxX, y=s_geometries[1].boxAABB.maxY, z=s_geometries[1].boxAABB.maxZ}}
	if s_geometries[1].geometryID ~= -1 then
    	-- Give this box a color tint
        KEP_SetGeometryMaterial(s_geometries[1].geometryID, {
            ambient = { r = s_geometries[1].colors.ambient.r, g = s_geometries[1].colors.ambient.g, b = s_geometries[1].colors.ambient.b, a = 0.5 },
            diffuse = { r = s_geometries[1].colors.diffuse.r, g = s_geometries[1].colors.diffuse.g, b = s_geometries[1].colors.diffuse.b, a = 0.5 },
        })
        KEP_SetGeometryVisible(s_geometries[1].geometryID, true)

	    -- Make this box solid
        KEP_SetGeometryDrawMode(s_geometries[1].geometryID, LWG.F_DRAW_SOLID + LWG.F_ALPHA_BLENDING)
	end
end


function DestroyGeometry()
	if s_geometries[1].geometryID ~= -1 then
		-- Destroy geometry
		KEP_DestroyGeometry(s_geometries[1].geometryID)
		s_geometries[1].geometryID = -1
	end
end