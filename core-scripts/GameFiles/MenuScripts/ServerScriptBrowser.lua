--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\DevToolsConstants.lua")

g_listhandles = {}
g_listscrollbars = {}

SOURCE_ASSET_DOWNLOAD_EVENT = "3DAppAssetDownloadCompleteEvent"
FILE_BROWSER_CLOSED_EVENT = "FileBrowserClosedEvent"
SCRIPT_SERVER_EVENT = "ScriptServerEvent"

g_ignoreSelection = false
g_selectCount = 0
g_searchString = ""

gScripts = {}

--SORT OPTIONS
Sort_ScriptName = 1
Sort_ObjectName = 2
Sort_ObjectId = 3

g_bSortAscending = true
g_SortingBy = Sort_ScriptName
g_SortFuncs = {} --filled in in after the sort functions are declared

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "scriptClientEventHandler", "ScriptClientEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "FileBrowserClosedHandler", FILE_BROWSER_CLOSED_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "selectEventHandler", "SelectEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "clickEventHandler", "ClickEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "AppAssetDownloadHandler", SOURCE_ASSET_DOWNLOAD_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "refreshScriptHandler", "refreshScriptList", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( FILE_BROWSER_CLOSED_EVENT, KEP.MED_PRIO )
	KEP_EventRegister("refreshScriptList", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	
	g_listhandles = 
	{
	gHandles["menuScriptNameDummy"],
	gHandles["menuScriptName"],
	gHandles["menuObjNameDummy"],
	gHandles["menuObjName"],
	gHandles["menuObjPIDDummy"],
	gHandles["menuObjPID"],
	}

	g_listscrollbars = {}
	for i,h in ipairs(g_listhandles) do
		table.insert(g_listscrollbars, ListBox_GetScrollBar(h))
	end

	selectScript(-1)
	requestScriptInfo()
	
	ListBox_SetHTMLEnabled(gHandles["menuScriptName"], true)
	ListBox_SetHTMLEnabled(gHandles["menuObjName"], true)
	
	--Fix the spacing on the title
	Dialog_SetCaptionText(gDialogHandle, "    "..Dialog_GetCaptionText(gDialogHandle))
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function AppAssetDownloadHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local path = KEP_EventDecodeString(event)
	local fileType = KEP_EventDecodeNumber(event)
	-- Make sure we got a menu script
	if fileType == DevToolsUpload.SAVED_SCRIPT then
		KEP_ShellOpen(path)
	end
end

function FileBrowserClosedHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local dialogResult = KEP_EventDecodeNumber( event )
	local filePath = ""
	if dialogResult~=0 then
		filePath = KEP_EventDecodeString( event )
	else
		--File Browser was cancelled
		return
	end

	if filter==UGC_TYPE.SCRIPT_DO then
		KEP_Import3DAppAsset(DevToolsUpload.SAVED_SCRIPT, filePath)
		requestScriptInfo()
	end
end

function clickEventHandler(dispatcher, fromNetId, event, eventId, filter, unused)
	local button	 = KEP_EventDecodeNumber(event)
	local objectType = KEP_EventDecodeNumber(event)
	local objectID	 = KEP_EventDecodeNumber(event)
	local control	 = KEP_EventDecodeNumber(event)
	local shift		 = KEP_EventDecodeNumber(event)
	local alt		 = KEP_EventDecodeNumber(event)
	if objectType == ObjectType.DYNAMIC then
		KEP_ReplaceSelection(objectType, objectID)
	end
end

function selectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if g_selectCount <= 0 then
		updateSelection()
	end
	g_selectCount = g_selectCount - 1
end

function scriptClientEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ScriptClientEventTypes.GetAllScripts then
		gScripts = {}
		
		local numRunningScripts = KEP_EventDecodeNumber(event)
		for i=1,numRunningScripts do
			local script = {}
			script.objectId = KEP_EventDecodeNumber(event)
			script.filename = KEP_EventDecodeString(event)
			script.scriptName = string.gsub(script.filename, "(.-\\).-", "")
			script.objectName = ""
			script.published = true
			
			if script.objectId > 0 then
				local name, description, canPlayMovie, isAttachableObject, isInteractive, isLocal, isDerivable, assetId, friendId, customTexture, globalId, invType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(script.objectId)
				script.objectName = name
			end
			script.unsortedIndex = #gScripts + 1
			if string.find(script.filename, "ActionItems\\") == nil and string.find(script.filename, "ActionInstances\\") == nil and string.find(script.filename, "ActionItems/") == nil and string.find(script.filename, "ActionInstances/") == nil then
				table.insert(gScripts, script)
			end
		end
		
		local numSavedScripts = KEP_EventDecodeNumber(event)
		for i=1,numSavedScripts do
			local script = {}
			script.objectId = 0
			script.filename = KEP_EventDecodeString(event)
			script.scriptName = string.gsub(script.filename, "(.-\\).-", "")
			script.objectName = ""
			script.published = true
			
			script.unsortedIndex = #gScripts + 1
			table.insert(gScripts, script)
		end
		
		local count = KEP_Count3DAppUnpublishedAssets(DevToolsUpload.SAVED_SCRIPT)
		local unPubIndex
		for unPubIndex=1,count do
			local entry = {}
			local name, fullpath, modified = KEP_Get3DAppUnpublishedAsset(DevToolsUpload.SAVED_SCRIPT, unPubIndex)
			name = name .. ".lua"
			local found = false
			for i,v in ipairs(gScripts) do
				if name  == v.scriptName then
					found = true
					break
				end
			end
			if found == false then
				local script = {}
				script.objectId = 0
				script.filename = name
				script.scriptName = string.gsub(name, "(.-\\).-", "")
				script.objectName = ""
				script.published = false
				script.unsortedIndex = #gScripts + 1
				table.insert(gScripts, script)			
			end
		end	
		
		sortScripts(Sort_ObjectId)
		updateSelection()
	end
end

function refreshScriptHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	requestScriptInfo()
end

function requestScriptInfo()
	local ev = KEP_EventCreate( SCRIPT_SERVER_EVENT)
	KEP_EventSetFilter(ev, ScriptServerEventTypes.GetAllScripts)
	KEP_EventAddToServer(ev)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventQueue( ev)
end

function updateSelection()
	count = KEP_GetNumSelected()
	for i,h in ipairs(g_listhandles) do
		ListBox_ClearSelection(h)
	end
	for i = 0,count-1 do
		local type, id = KEP_GetSelection(i)
		result = getItemIndex(id)
		if result ~= -1 then
			g_ignoreSelection = true
			ListBox_SelectItem(gHandles["menuScriptName"], result - 1)
			selectScript(id)
		else
			selectScript(-1)
		end
	end
	syncAllLists( g_listhandles, gHandles["menuScriptName"] )
end

function getSelectedScript()
	local index = ListBox_GetSelectedItemData(gHandles["menuScriptName"])
	for i,v in ipairs(gScripts) do
		if index == v.unsortedIndex then
			return v
		end
	end
	return nil
end

function getItemIndex(objectId)
	for i,v in ipairs(gScripts) do
		if v.objectId == objectId then
			return i
		end
	end
	return -1
end

function populateList()
	for i,v in ipairs(g_listhandles) do
		ListBox_RemoveAllItems(v)
	end

	for i,v in ipairs(gScripts) do
		local dispName = ""
		if g_searchString == "" then
			dispName = v.scriptName
		else
			local start, e = string.find(string.lower(v.scriptName), string.lower(g_searchString))
			if start ~= nil then
				dispName = string.sub(v.scriptName, 0, start - 1) .. "<font color=FFFF0000>" .. string.sub(v.scriptName, start, e) .. "</font>" .. string.sub(v.scriptName, e + 1, -1)
			end
		end
		if v.published == false then
			dispName = dispName .. " (unpublished)"
		end
		if dispName ~= "" then
			ListBox_AddItem(gHandles["menuScriptNameDummy"], "", v.unsortedIndex)
			ListBox_AddItem(gHandles["menuScriptName"], dispName, v.unsortedIndex)
			ListBox_AddItem(gHandles["menuObjNameDummy"], "", v.unsortedIndex)
			ListBox_AddItem(gHandles["menuObjName"], v.objectName, v.unsortedIndex)
			ListBox_AddItem(gHandles["menuObjPIDDummy"], "", v.unsortedIndex)
			ListBox_AddItem(gHandles["menuObjPID"], (v.objectId == 0) and "" or v.objectId, v.unsortedIndex)
		end
	end
	
	--Update the column header images
	local btnHandles = {gHandles["btnScriptName"], gHandles["btnObjName"], gHandles["btnObjPID"]}
	local sBlankTexture = "button9_LGray.tga"
	for i,h in pairs(btnHandles) do
		Element_AddTexture( Control_GetDisplayElement(h,    "normalDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h, "mouseOverDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,   "focusedDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,   "pressedDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,  "disabledDisplay"), gDialogHandle, sBlankTexture)
	end

	local sArrowTexture = "button9_LGrayUpArrow.tga"
	if g_bSortAscending then
		sArrowTexture = "button9_LGrayDownArrow.tga"
	end

	local btnHandle = btnHandles[g_SortingBy]
	if btnHandle ~= nil then
		Element_AddTexture( Control_GetDisplayElement(btnHandle,    "normalDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle, "mouseOverDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,   "focusedDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,   "pressedDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,  "disabledDisplay"), gDialogHandle, sArrowTexture)
	end
end

function attachScript(objectId, filename)
	local ev = KEP_EventCreate( SCRIPT_SERVER_EVENT)
	KEP_EventSetFilter(ev, ScriptServerEventTypes.AttachScript)
	KEP_EventAddToServer(ev)
	KEP_EventEncodeNumber(ev, objectId)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeString(ev, filename)
	KEP_EventQueue( ev)
end

function detachScript(objectId)
	local ev = KEP_EventCreate( SCRIPT_SERVER_EVENT)
	KEP_EventSetFilter(ev, ScriptServerEventTypes.DetachScript)
	KEP_EventAddToServer(ev)
	KEP_EventEncodeNumber(ev, objectId)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventQueue( ev)
end

function btnScriptName_OnButtonClicked( buttonHandle )
	if Sort_ScriptName == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortScripts(Sort_ScriptName)
end

function btnObjName_OnButtonClicked( buttonHandle )
	if Sort_ObjectName == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortScripts(Sort_ObjectName)
end

function btnObjPID_OnButtonClicked( buttonHandle )
	if Sort_ObjectId == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortScripts(Sort_ObjectId)
end

function sortScripts(sortType)
	g_SortingBy = sortType
	table.sort(gScripts, g_SortFuncs[sortType])
	populateList()
end

function sortByScriptName(itemA, itemB)
	local aName = string.lower(itemA.scriptName)
	local bName = string.lower(itemB.scriptName)
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	else
		if itemA.objectId == itemB.objectId then --lua compares the same script sometimes. This also catches the recursion when sortByObjectID() calls sortByScriptName().
			return false
		end
		return sortByObjectName(itemA, itemB)
	end
end

function sortByObjectName(itemA, itemB)
	local aName = string.lower(itemA.objectName)
	local bName = string.lower(itemB.objectName)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	else
		return sortByObjectID(itemA, itemB)
	end
end

function sortByObjectID(itemA, itemB)
	if itemA.objectId == 0 and itemB.objectId ~= 0 then
		return false
	elseif itemB.objectId == 0 and itemA.objectId ~= 0 then
		return true
	elseif itemA.objectId == 0 and itemB.objectId == 0 then
		return sortByScriptName(itemA, itemB)
	end
	if g_bSortAscending then
		return itemA.objectId < itemB.objectId
	else
		return itemA.objectId > itemB.objectId
	end
end

g_SortFuncs =
{
	sortByScriptName,
	sortByObjectName,
	sortByObjectID
}

function btnImport_OnButtonClicked( buttonHandle )
	local event = KEP_EventCreate( FILE_BROWSER_CLOSED_EVENT )
	KEP_EventSetFilter( event, UGC_TYPE.SCRIPT_DO )
	UGC_BrowseFileByImportType( UGC_TYPE.SCRIPT_DO, event ) -- second param (initial folder) TBD
end

function btnDelete_OnButtonClicked( buttonHandle )
	-- Delete Selected Script Object
	local script = getSelectedScript()
	if script == nil then return end
	local fname = script.filename
	Log("DeleteButtonClicked: file="..fname)
		
	local ev = KEP_EventCreate( SCRIPT_SERVER_EVENT)
	KEP_EventSetFilter(ev, ScriptServerEventTypes.DeleteScript)
	KEP_EventAddToServer(ev)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeString(ev, fname)
	KEP_EventQueue( ev)
		
	requestScriptInfo()
end

function btnNew_OnButtonClicked( buttonHandle )
	MenuOpen("NewScriptServer.xml")
end 

function btnEdit_OnButtonClicked( buttonHandle )
	-- Edit Selected Script Object
	local script = getSelectedScript()
	if script == nil then return end
	local fname = script.filename
	Log("EditButtonClicked: file="..fname)

	local baseName = string.sub(fname, 1, string.len(fname)-4)
	KEP_RequestSourceAssetFromAppServer(DevToolsUpload.SAVED_SCRIPT, baseName)
end

function btnAttach_OnButtonClicked( buttonHandle )
	-- Attach Selected Script Object
	local script = getSelectedScript()
	if script == nil then return end
	local fname = script.filename
	Log("AttachButtonClicked: file="..fname)
		
	--get placement object id
	local numSelected = KEP_GetNumSelected()
	if numSelected > 0 then
		for i = 0,numSelected-1 do
			local type, id = KEP_GetSelection(i)
			if type == ObjectType.DYNAMIC then
				attachScript(id, fname)
			end
		end
	end

	requestScriptInfo()
end

function btnDetach_OnButtonClicked( buttonHandle )
	-- Detach Selected Script Object
	local script = getSelectedScript()
	if script == nil or script.objectId <= 0 then return end
	Log("DetachButtonClicked: objId="..script.objectId)
	
	detachScript(script.objectId)
	
	requestScriptInfo()
end

function btnReloadScript_OnButtonClicked( buttonHandle )
	-- Attach Selected Script Object
	local script = getSelectedScript()
	if script == nil then return end
	local fname = script.filename
	Log("ReloadButtonClicked: file="..fname)

	--get placement object id
	local numSelected = KEP_GetNumSelected()
	if numSelected > 0 then
		for i = 0,numSelected-1 do
			local type, id = KEP_GetSelection(i)
			if type == ObjectType.DYNAMIC then
				attachScript(id, fname)
			end
		end
	end

	requestScriptInfo()
end

function btnFaceObj_OnButtonClicked( buttonHandle )
	-- Face Selected Script Object
	local script = getSelectedScript()
	if script == nil then return end
	local objId = script.objectId
	Log("FaceObjectButtonClicked: objId="..objId)
	FaceObject(objId)
end	

function selectScript(sel_index)
	if sel_index >= 0 then
		local script = getSelectedScript()
		if script.objectId > 0 then
			KEP_ReplaceSelection(ObjectType.DYNAMIC, script.objectId)
		end
		
		local isRunning = (script.objectId > 0)
		
		Control_SetEnabled(gHandles["btnEdit"], true)
		Control_SetEnabled(gHandles["btnAttach"], not isRunning)
		Control_SetEnabled(gHandles["btnDetach"], isRunning)
		Control_SetEnabled(gHandles["btnDelete"],  not isRunning)
		Control_SetEnabled(gHandles["btnFaceObj"],  isRunning)
		Control_SetEnabled(gHandles["btnReloadScript"], true)
		
	else
		Control_SetEnabled(gHandles["btnEdit"], false)
		Control_SetEnabled(gHandles["btnAttach"], false)
		Control_SetEnabled(gHandles["btnDetach"], false)
		Control_SetEnabled(gHandles["btnDelete"],  false)
		Control_SetEnabled(gHandles["btnFaceObj"],  false)
		Control_SetEnabled(gHandles["btnReloadScript"], false)
	end
end

function menuScriptName_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if g_ignoreSelection == false and ((bMouseDown == 1 or bFromKeyboard == 1) or g_bSyncAllLists == 1) then
		g_selectCount = 1
		selectScript( sel_index )
		if g_bSyncAllLists == 0 then
			syncAllLists( g_listhandles, listBoxHandle )
		end
	end
	g_ignoreSelection = false
end

function menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
	if g_ignoreSelection == false  and ((bMouseDown == 1 or bFromKeyboard == 1) or g_bSyncAllLists == 1) then
		if g_bSyncAllLists == 0 then
			syncAllLists( g_listhandles, listBoxHandle )
		end
	end
end

function menuScriptNameDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function menuObjPIDDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function menuObjPID_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function menuObjNameDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function menuObjName_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function scrollBarChanged(listBoxHandle)
	if g_bSyncAllLists == 0 then
		syncAllScrollBars( g_listhandles, listBoxHandle )
	end
end

function menuScriptNameDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function menuScriptName_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function menuObjPIDDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function menuObjPID_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function menuObjNameDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function menuObjName_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function editSearch_OnEditBoxChange(editBoxHandle, text)
	g_searchString = text
	populateList()
end
