--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

function BrowserCallbackEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local type = KEP_EventDecodeNumber( event )
	if gDialogHandle == nil then
		return KEP.EPR_OK
	end
	local st = Dialog_GetStatic( gDialogHandle, "statusMsg" )
	local text = KEP_EventDecodeString( event )
	local num = KEP_EventDecodeNumber( event )

	if type == BrowserIds.BROWSER_STATUSTEXT_CHANGE then -- status text change
		Static_SetText( st, text )
	elseif type == BrowserIds.BROWSER_UPDATE_PROGRESS then -- % complete
		Static_SetText( st, tostring(num).."% complete" )
	elseif type == BrowserIds.BROWSER_KGP_LINK then
		Dialog_MsgBox( "You clicked a kgp link: "..text )
		local br = Dialog_GetStatic( gDialogHandle, "browser" )
		Static_SetText( br, "http://www.kaneva.com" )
	else
		Static_SetText( st, "Huh?.  Type: "..tostring(type).." string "..st.." num "..num )
	end
	return KEP.EPR_OK
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserCallbackEventHandler", "BrowserCallbackEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserCallbackEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- set url
	local sh = Dialog_GetStatic(gDialogHandle, "ctrlBrowser")
	if sh ~= 0 then
		KEP_MessageBox("Got browser")
		Static_SetText(sh, "http://www.ibm.com")
	else
		KEP_MessageBox("Didn't get browser")
	end
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnNext_OnButtonClicked(buttonHandle)
	local brh = Dialog_GetBrowser(gDialogHandle, "browser")
	if brh ~= 0 then
		Browser_Navigate( brh, BrowserIds.BROWSER_FORWARD );
	end
end

function btnPrev_OnButtonClicked(buttonHandle)
	local brh = Dialog_GetBrowser(gDialogHandle, "browser")
	if brh ~= 0 then
		Browser_Navigate( brh, BrowserIds.BROWSER_BACK);
	end
end

function btnStop_OnButtonClicked(buttonHandle)
	local brh = Dialog_GetBrowser(gDialogHandle, "browser")
	if brh ~= 0 then
		Browser_Navigate( brh, BrowserIds.BROWSER_STOP );
	end
end

function btnReload_OnButtonClicked(buttonHandle)
	local brh = Dialog_GetBrowser(gDialogHandle, "browser")
	if brh ~= 0 then
		Browser_Navigate( brh, BrowserIds.BROWSER_RELOAD );
	end
end

