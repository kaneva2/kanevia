--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Welcome.lua
--
-- Welcoming players to Kaneva since 2016
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

--Settings
local m_settings = {}
local m_welcomeText = ""
local m_showWorldName = true
--CONSTANTS
local MAX_HEIGHT = 600
local MAX_IMAGE_HEIGHT = 300
local MAX_IMAGE_WIDTH = 600
--local BOTTOM_BUFFER = 82
local BOTTOM_BUFFER = 94
local TEXT_BUFFER = 20
local DESCRIPTION_BUFFER = 40
--local m_worldNameHeight = 60
local BUTTON_BUFFER = 26
--Other variables
local m_imageHeight = 0
local m_imageWidth = 0
local m_worldNameHeight = 60
local m_textHeight = 0
local m_worldName = ""
local m_textFormated = false

local m_preview = false

local m_imageSet = false

local GEMFILTER=666
local GEMWEBCALL = "kgp/worldInfo.aspx?action=CanWorldDropGems&worldName="

-- Called when the menu is created
function onCreate()
	--Menu will be off screen it finishes setting self up, then will reappear on screen
	MenuSetLocationThis(-1000, -1000)
	
	KEP_EventRegisterHandler("zoneNavigationEventHandler", "ZoneNavigationEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("welcomeHandler", "FRAMEWORK_OPEN_WELCOME", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler( "CloseProgressMenuHandler", "CloseProgressMenu", KEP.HIGH_PRIO)

	--Added this event for Gem
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.HIGH_PRIO )

	m_lstDescription = gHandles["lstDescription"]
	m_imgWelcome = gHandles["imgWelcome"] 
	m_imgWelcomeBG = gHandles["imgWelcomeBG"]
	m_imgWorldNameBG = gHandles["imgWorldBG"]
	m_imgDispElement = Image_GetDisplayElement(m_imgWelcome)

	ListBox_SetHTMLEnabled(gHandles["lstDescription"], true)

	--added for gems
	Control_SetVisible(gHandles["imgGem"], false)
	Control_SetVisible(gHandles["imgGreyBar"], false)
	Control_SetToolTip(gHandles["imgGem"], "This world can drop gems.")

end

function onDestroy( )
	MenuClose("TransparentBG")
end
function zoneNavigationEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    if filter == ZONE_NAV.CURRENT_URL_INFO then

    	local url = KEP_EventDecodeString(tEvent)

    	if m_showWorldName == false then return end
      	m_worldName = parseFriendlyNameFromURL(url)

        local trueTextHeight = Static_GetStringHeight(gHandles["stcWorldName"], m_worldName)

        if trueTextHeight > m_worldNameHeight then
        	m_worldNameHeight = m_worldNameHeight * 2
        	Control_SetSize(gHandles["stcWorldName"], MAX_IMAGE_WIDTH, m_worldNameHeight)
        	for i = 1, 4 do 
        		Control_SetSize(gHandles["stcWorldNameBG"..i], MAX_IMAGE_WIDTH, m_worldNameHeight)
        	end
        	Control_SetSize(gHandles["stcWorldNameShadow"], MAX_IMAGE_WIDTH, m_worldNameHeight)
        	Control_SetSize(m_imgWorldNameBG, MAX_IMAGE_WIDTH, m_worldNameHeight)
        	--setWelcomeImageY(m_imageWidth, m_imageHeight)

        	if m_worldNameHeight > m_imageHeight then
        		m_imageHeight = 0
        	end
        	updateDisplay()
        end

        Static_SetText(gHandles["stcWorldName"], m_worldName)
        for i = 1, 4 do 
        	Static_SetText(gHandles["stcWorldNameBG"..i], m_worldName)
        end
        Static_SetText(gHandles["stcWorldNameShadow"], m_worldName)
    end
end

function welcomeHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local settingString = KEP_EventDecodeString(tEvent)
	if KEP_EventMoreToDecode(tEvent) == 1 then
		m_preview = true
	end
	m_settings = Events.decode(settingString)
	m_showWorldName = m_settings.showWorldName
	m_welcomeText = m_settings.welcomeText


	local imageFile = m_settings.welcomeImage
	local lastSlashIndex = string.find(imageFile, "/[^/]*$")
	local periodIndex = string.find(imageFile,  '.', 1, true)
	if lastSlashIndex and periodIndex then
		local imageName =  string.sub(imageFile, lastSlashIndex + 1, periodIndex - 1) or "none"
		m_imageFile = downloadImage(imageFile, imageName.."_WelcomeScreen")
	else
		m_imageHeight = 0
		Control_SetVisible(m_imgWelcome, false)
		updateDisplay()
	end
end

function CloseProgressMenuHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	updateDisplay()
	MenuCenterThis()
	MenuBringToFrontThis()
end

--------------------
--IMAGE FUNCTIONS--
--------------------
function downloadImage(imgURL, chapterName)
	--the name the image will saved as

	local filename = tostring(chapterName)..".jpg"
	local path = KEP_DownloadTextureAsyncToGameId(filename, imgURL)
	
    local gameId = KEP_GetGameId()
	
	path = string.gsub(path,".-\\", "" )
	path = "../../CustomTexture/".. gameId .. "/" .. path
	return path
end

function textureDownloadHandler()
	updateImage()
end

function updateImage()
	if m_imageFile  then
		Element_AddTexture(m_imgDispElement, gDialogHandle,m_imageFile)
		local textureWidth = Element_GetTextureWidth(m_imgDispElement, gDialogHandle)
		local textureHeight = Element_GetTextureHeight(m_imgDispElement, gDialogHandle)

		
		if not m_imageSet then
			m_imageWidth = textureWidth
			scaleWelcomeImage(textureWidth, textureHeight)
		end
	else
		m_imageHeight = 0
	end
end

function scaleWelcomeImage(width, height)

	local newHeight = height
	if width ~= MAX_IMAGE_WIDTH then
		newHeight = height * (MAX_IMAGE_WIDTH / width) 
	end
	setWelcomeImageY(width, newHeight)

	Element_SetCoords(m_imgDispElement, 0, 0, LeastPowerofTwo(width), LeastPowerofTwo(height))
end

function setWelcomeImageY(width, height)
	Control_SetSize(m_imgWelcome, MAX_IMAGE_WIDTH - 2, height)
	m_imageHeight = height
	if height >= MAX_IMAGE_HEIGHT then
		local heightDifference  = height - MAX_IMAGE_HEIGHT
		local moveDistance = heightDifference / 2
		Control_SetLocation(m_imgWelcome, Control_GetLocationX(m_imgWelcome), Control_GetLocationY(m_imgWelcome)-moveDistance)

		local containerX, containerY = Control_GetLocationX(m_imgWelcomeBG), Control_GetLocationY(m_imgWelcomeBG)
		local containerW, containerH = Control_GetWidth(m_imgWelcomeBG), Control_GetHeight(m_imgWelcomeBG)
		Control_SetClipRect(m_imgWelcome, containerX, containerY, containerX+containerW, containerY+containerH)

		m_imageHeight = MAX_IMAGE_HEIGHT
	elseif height < m_worldNameHeight then
		m_imageHeight = 0
	end

	m_imageSet = true
	updateDisplay()
end

------------------
--Screen Display--
------------------
function  updateDisplay()
	local textBoxY = m_imageHeight + TEXT_BUFFER
	local menuHeight = m_imageHeight
	m_textHeight = MAX_HEIGHT - m_imageHeight - BOTTOM_BUFFER - TEXT_BUFFER


	if m_showWorldName then
		if m_worldName == "" then
			local ev = KEP_EventCreate("ZoneNavigationEvent")
			KEP_EventSetFilter(ev, ZONE_NAV.CURRENT_URL_REQUEST)
			KEP_EventQueue(ev)
		end
		if m_imageHeight == 0 then
			m_textHeight = m_textHeight - m_worldNameHeight
			textBoxY = textBoxY + m_worldNameHeight
			menuHeight = m_worldNameHeight
		end
		setWorldNameY()
	else
		Control_SetVisible(m_imgWorldNameBG, false)
	end

	formatDescription(textBoxY)

	local buttonLocationY = m_textHeight  + textBoxY + BUTTON_BUFFER
	Control_SetLocationY(gHandles["btnOK"], buttonLocationY)

	menuHeight = menuHeight + m_textHeight + BOTTOM_BUFFER + TEXT_BUFFER

	MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), menuHeight)

	if m_preview == true then
		MenuCenterThis()
	end
end


--WorldName--
function setWorldNameY()

	local worldNameY = 0
	if m_imageHeight ~= 0 then
		worldNameY = m_imageHeight - m_worldNameHeight
		--m_worldNameHeightworldNameY = 155
	end

	local YOffset = {-1, 1, -1, 1}
	Control_SetLocationY(gHandles["stcWorldName"], worldNameY)
	Control_SetLocationY(m_imgWorldNameBG, worldNameY)
	Control_SetLocationY(gHandles["stcWorldNameShadow"], worldNameY + 3)	
	for i = 1, 4 do 
    	Control_SetLocationY(gHandles["stcWorldNameBG"..i], worldNameY + YOffset[i])
   	end
end

--Text Description--
function formatDescription(locationY)
	local description = string.gsub(m_welcomeText, "\n", "<br>")
	Control_SetLocationY(m_lstDescription, locationY)
	Control_SetSize(m_lstDescription, Control_GetWidth(m_lstDescription), m_textHeight)

	--Don't add any new text if text was already formated
	if m_textFormated == false then
		lineBreak(description)
	end

	local trueTextHeight = Static_GetStringHeight(gHandles["stcDescription"], description) + TEXT_BUFFER
	if description == "" then
		trueTextHeight = 0
	end
	if m_textHeight > trueTextHeight then
		m_textHeight = trueTextHeight + DESCRIPTION_BUFFER
		Control_SetSize(m_lstDescription, Control_GetWidth(m_lstDescription), m_textHeight)
	end

	m_textFormated = true
end

--Formating the text
function lineBreak(text)
	local breaks = false
	for line in string.gmatch(text, "(.-)<br>") do
		breaks = true
		addTextLine(line)
	end

	if breaks == false then
		addTextLine(text)
	else
		local lastBreak = string.match(text, '^.*()<br>')
		if lastBreak then
			local lastString  = string.sub(text, lastBreak + 4)
			addTextLine(lastString)
		end
	
	end
end

function addTextLine(text)
	if textFitsDisplayLine(text) then
		ListBox_AddItem(m_lstDescription, text, 0)
		return
	end

	local remLine = text
	local curLine = ""
	local lstLine = ""
	local word = ""
	while textFitsDisplayLine(curLine) do
		lstLine = curLine
		word, remLine = breaktoken(remLine)
		curLine = curLine..word
	end 

	-- Is It The First Word ?
	if lstLine == "" then

		-- Insert First Word & Flow Recursively
		ListBox_AddItem(m_lstDescription, word, 0)
		addTextLine(remLine, type)
	else
		-- Insert What Fits & Flow Recursively
		ListBox_AddItem(m_lstDescription, lstLine, 0)
		addTextLine(word..remLine, type)
	end

end

function textFitsDisplayLine(text)
	return (Static_GetStringWidth(gHandles["stcDescription"], text) <= (Control_GetWidth(m_lstDescription) - 110))
end

function breaktoken(message)
	local spcpos = string.find(message, " ")
	local tokenpos
	local word = ""
	local restmessage
	if message == nil or message == "" then
		return "", ""
	end
	if spcpos == nil  then
		return message, ""
	elseif spcpos ~= nil then
		tokenpos = spcpos
		word = string.sub(message, 1, tokenpos)
		restmessage = string.sub(message, tokenpos + 1, -1)
	end
	return word, restmessage
end

--added callback for gem web call
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == GEMFILTER then
		local gemData = KEP_EventDecodeString( event )
		local s, e = 0      -- start and end of captured string
		local result = nil  -- result description
		s, e, result = string.find(gemData, "<CanDropGems>(.-)</CanDropGems>")

		if result ~= nil then
			if result == "true" then
				Control_SetVisible(gHandles["imgGem"], true)
			end
		end
	end
end