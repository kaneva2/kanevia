--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_MapIcon.lua
--
-- Allows players to view the world map
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

local iconSize = {x = 70, y = 70}
local iconBuffer = {x = 5, y = 8}
local iconOffset = {x = 62, y = -15}
local TOOLTIP_DELAY = .25
local m_tooltipTimer
local m_currTick = 0


-- Called when the menu is created
function onCreate()
	updateIcon(true)
	resetPosition()

	KEP_EventRegisterHandler( "playerSetModeHandler", "FRAMEWORK_SET_PLAYER_MODE", KEP.HIGH_PRIO)

	KEP_EventCreateAndQueue( "FRAMEWORK_MAP_ICON_OPENED" )

end

-- Called every frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- Delay before displaying tooltip
	if m_tooltipTimer then
		if m_tooltipTimer > 0 then
			m_tooltipTimer = m_tooltipTimer - fElapsedTime
		else
			if MenuIsClosed("Framework_Map.xml") then -- Check that map was not opened during the tooltip delay
				displayTooltip()
			end
			m_tooltipTimer = nil
		end
	end
end

function Dialog_OnMoved(dialogHandle, x, y)
	resetPosition()
end

function updateIcon(iconOn)
	Control_SetVisible(gHandles["imgMapIcon"], iconOn)
	Control_SetVisible(gHandles["btnMapIcon"], iconOn)
end

function btnMapIcon_OnButtonClicked()
	if MenuIsClosed("Framework_Map.xml") then
		MenuOpen("Framework_Map.xml")
		local tempColor = {a=180, r=255, g=255, b=255}
		BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgMapIcon"])), 0, tempColor)
		setVisibleAndEnabled(gHandles["imgHoverBoxBG"], false)
		setVisibleAndEnabled(gHandles["stcHoverBox"], false)
	end
end

function btnMapIcon_OnMouseEnter(btnHandle)
	local tempColor = {a=255, r=255, g=255, b=255}
    BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgMapIcon"])), 0, tempColor)
    m_tooltipTimer = TOOLTIP_DELAY
end

function btnMapIcon_OnMouseLeave(btnHandle)
	local tempColor = {a=180, r=255, g=255, b=255}
	BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgMapIcon"])), 0, tempColor)
	setVisibleAndEnabled(gHandles["imgHoverBoxBG"], false)
	setVisibleAndEnabled(gHandles["stcHoverBox"], false)
	m_tooltipTimer = nil
end

-- Sent from HUDBuild or Framework_Toolbar when a player changes modes
function playerSetModeHandler(dispatcher, fromNetId, event, filter, objectId)
    local playerMode = KEP_EventDecodeString(event)
    -- If we've entered God mode
    if playerMode == "God" then
        resetPosition()
    end
end

displayTooltip = function()
	setVisibleAndEnabled(gHandles["imgHoverBoxBG"], true)
	setVisibleAndEnabled(gHandles["stcHoverBox"], true)
end

setVisibleAndEnabled = function(inputHandle, inputValue)
	Control_SetVisible(inputHandle, inputValue)
	Control_SetEnabled(inputHandle, inputValue)
end

resetPosition = function()
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	MenuSetLocationThis(screenWidth - iconSize.x - iconOffset.x - iconBuffer.x, screenHeight - iconSize.y - iconOffset.y - iconBuffer.y)	
end