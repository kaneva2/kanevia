--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

VALIDATION_EVENT_NAME = "TourSignupValidation"
QUIT_EVENT_NAME       = "PlayerTourQuit"
TOUR_TIMER            = "TourTimerEvent"   --used to simply tick down the screen
TOUR_SPAWN_TIMER      = "TourSpawnTimer"   --used to send the spawn request
TOUR_TRAVEL_EVENT     = "TourTravelEvent"
TOUR_KICKOFF_EVENT    = "TourKickoffEvent"
SIGNUP_EVENT_NAME     = "PlayerTourSignup"
CREATE_NOTIFICATION_EVENT = "CreateNotificationEvent"

--global goodness
tourTimerEvent      = 0
tourTimerEvent2     = 0
TOUR_TIMER_INTERVAL = 1  --seconds
timeleft            = 0
zone_index          = 0
zone_instance       = 0
working             = 0
gDialogHandle       = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "tourTimerHandler", TOUR_TIMER, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "tourSpawnHandler", TOUR_SPAWN_TIMER, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "tourKickoffHandler", TOUR_KICKOFF_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( QUIT_EVENT_NAME, KEP.MED_PRIO )
	KEP_EventRegister( TOUR_TIMER, KEP.MED_PRIO )
	KEP_EventRegister( TOUR_SPAWN_TIMER, KEP.MED_PRIO )
	KEP_EventRegister( TOUR_KICKOFF_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( SIGNUP_EVENT_NAME, KEP.MED_PRIO )
end

function sendNotificationEvent(text, imageURL, imageType, assetID, thumbnail, linkURL, buttonText, duration)
	local ev = KEP_EventCreate( CREATE_NOTIFICATION_EVENT)
	KEP_EventEncodeString(ev, text)
	KEP_EventEncodeString(ev, imageURL)
	KEP_EventEncodeNumber(ev, imageType)
	KEP_EventEncodeNumber(ev, assetID)
	KEP_EventEncodeNumber(ev, thumbnail)
	KEP_EventEncodeString(ev, linkURL)
	KEP_EventEncodeString(ev, buttonText)
	KEP_EventEncodeNumber(ev, duration)
	KEP_EventQueue( ev)
end

function tourKickoffHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	tour_next_zone = KEP_EventDecodeNumber(event)
	header  = Dialog_GetStatic( gDialogHandle, "lblHeader1" )
	timebox = Dialog_GetStatic( gDialogHandle, "lblBody1"   )
	timeleft = tonumber(Static_GetText(timebox))
	if timeleft < 0 then
		return KEP.EPR_OK
	end

	--special case when you sign up exactly when the tour starts
	if timeleft == 0 then
		timeleft = 5
	end

	--timer for the countdown
	tourTimerEvent = KEP_EventCreate( TOUR_TIMER )
	KEP_EventQueueInFuture( tourTimerEvent, TOUR_TIMER_INTERVAL )

	--timer for the spawn event
	tourTimerEvent2 = KEP_EventCreate( TOUR_SPAWN_TIMER )
	KEP_EventQueueInFuture( tourTimerEvent2, timeleft )

	--popup the welcome menu
	if tour_next_zone == 1 then
		MenuOpen("TourConfirmationMenu.xml")
	end
	if tour_next_zone == 2 then
		MenuOpen("TourKickoffMenu.xml")
	end
end

function tourSpawnHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	zidx  = Dialog_GetStatic( gDialogHandle, "zone_index" )
	zist  = Dialog_GetStatic( gDialogHandle, "zone_instance" )
	zone_index    =  Static_GetText(zidx)
	zone_instance =  Static_GetText(zist)

	--this is weird but sometimes the queueeventinfuture queues up 2 events... no really it does when the cancel event method fails
	if working == 1 then
		return
	end
	working = 1
	msg = string.format("TourStatusMenu - Zoning to next place z:%s, inst:%s", zone_index, zone_instance )
	Log( msg )

	--spawn player to place
	local ev = KEP_EventCreate( KEP.GOTOPLACE_EVENT_NAME )
	KEP_EventSetFilter( ev, KEP.GOTO_ZONE ) -- We are going to a zone
	KEP_EventEncodeNumber( ev, zone_index )
	KEP_EventEncodeNumber( ev, zone_instance )
	KEP_EventEncodeNumber( ev, -1 ) -- selected character is unknown now
	KEP_EventAddToServer( ev )
	KEP_EventQueue( ev )

	timebox = Dialog_GetStatic( gDialogHandle, "lblBody1" )
	Static_SetText( timebox, 'Now!' )
	working = 0
end

function tourTimerHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	timeleft = timeleft - 1
	if timeleft <= 0 then
		return
	end

	if timeleft > 3600 then
		hrs = math.floor(timeleft / 3600)
		tl  = math.fmod(timeleft,3600)
		mns = math.floor(tl/60)
		sec = math.fmod(tl,60)
		msg =  string.format( "timeleft: %d, tl: %d, mns: %d, sec: %d", timeleft, tl, mns, sec)
		Log( msg )
	elseif timeleft > 60 then
		hrs = 0
		mns = math.floor(timeleft / 60)
		sec = math.fmod(timeleft,60)
	else
		hrs = 0
		mns = 0
		sec = timeleft
	end
	timeformat = string.format("%02d:%02d:%02d", hrs, mns, sec)
	timebox = Dialog_GetStatic( gDialogHandle, "lblBody1" )
	Static_SetText( timebox, timeformat )
	tourTimerEvent = KEP_EventCreate( TOUR_TIMER )
	KEP_EventQueueInFuture( tourTimerEvent, TOUR_TIMER_INTERVAL )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function btnQuitTour_OnButtonClicked( buttonHandle)
	-- Send the event that removed them from the tour
	local ev = KEP_EventCreate( QUIT_EVENT_NAME)
	KEP_EventAddToServer( ev )
	KEP_EventQueue( ev)
	MenuCloseThis()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )

	--cancel the pending timer events
	if tourTimerEvent ~= nil then
		KEP_EventCancel( tourTimerEvent )
	end
	if tourTimerEvent2 ~= nil then
		KEP_EventCancel( tourTimerEvent2 )
	end
end
