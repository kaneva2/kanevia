--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\MenuFunctions.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\Lib_GameDefinitions.lua")

dofile("..\\ClientScripts\\MathHelper.lua")

local m_selectedPathWaypointPID = nil
local m_waypointIndex = nil
local m_parentPathName = nil
local m_parentUNID = nil
local m_pauseTime = nil
local m_rotationType = nil
local m_easeIn = nil
local m_easeOut = nil
local m_collisionTrigger = nil
local m_triggerSize = nil

local m_mouseInsideSubMenu = nil

local m_isLastGameItem = nil

local m_bgrndImages = {"mUpperLeft_Slice", "mUpperCenter_Slice", "mUpperRight_Slice",
					   "mMiddleLeft_Slice", "mMiddleCenter_Slice", "mMiddleRight_Slice",
					   "mLowerLeft_Slice", "mLowerCenter_Slice", "mLowerRight_Slice"}
local m_addWaypointControls = {img = "imgAddIcon", stc = "stcAddWaypoint", btn = "btnAddWaypoint"}
local m_editWaypointControls = {img = "imgEditIcon", stc = "stcEditWaypoint", btn = "btnEditWaypoint"}
local m_delWaypointControls = {img = "imgDelIcon", stc = "stcDelWaypoint", btn = "btnDelWaypoint"}

local COLORS = {
	DefaultBlue = {a = 255, r=135, g=152, b=255},
    MousedOverWhite = {a=255, r=255, g=255, b=255},
}
local TEXCOORDS = {
	DefaultAdd = {left = 24, top = 72, right = 48, bot = 96},
	MousedOverAdd = {left = 144, top = 72, right = 168, bot = 96},
	DefaultEdit = {left = 0, top = 72, right = 24, bot = 96},
	MousedOverEdit = {left = 120, top = 72, right = 144, bot = 96},
	DefaultDel = {left = 72, top = 24, right = 96, bot = 48},
	MousedOverDel = {left = 192, top = 24, right = 216, bot = 48}
}

local m_xOffsetForNextWaypoint = 10
local m_yOffsetForNextWaypoint = 0
local m_zOffsetForNextWaypoint = 0

local m_startCamX = nil
local m_startCamY = nil
local m_startCamZ = nil

local CAM_PADDING = .95
local INTERACTION_RANGE_SQRD = 625
local CURSOR_OFFSET_Y = -24

local MENU_WIDTH = 200
local MENU_HEIGHT = 216

-- Dialog_OnCreate = Called upon menu creation.
function onCreate()
	KEP_EventRegister( "PathWaypointSelectedEvent", KEP.HIGH_PRIO )
	
	KEP_EventRegisterHandler( "PathWaypointSelectedEventHandler", "PathWaypointSelectedEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "onItemNameResponse", "INVENTORY_HANDLER_RETURN_ITEM_NAME", KEP.HIGH_PRIO )
	Events.registerHandler( "ResponseToPathWaypointPropertiesEvent", ResponseToPathWaypointPropertiesEventHandler )
	
	cursorXPos = Cursor_GetLocationX()
	cursorYPos = Cursor_GetLocationY() + CURSOR_OFFSET_Y
	local clientWidth, clientHeight = KEP_GetClientRectSize()
	menuXPos = (cursorXPos < (clientWidth - MENU_WIDTH)) and cursorXPos or (cursorXPos - MENU_WIDTH)
	menyYPos = (cursorYPos < (clientHeight - MENU_HEIGHT)) and cursorYPos or (cursorYPos - MENU_HEIGHT)
	
	MenuSetLocationThis(menuXPos, menyYPos)
	
	m_startCamX, m_startCamY, m_startCamZ = KEP_GetPlayerCameraOrientation()
	
	m_mouseInsideSubMenu = false
end

function Dialog_OnMouseMove( dialogHandle, x, y )
	for i, v in pairs(m_bgrndImages) do
		if Control_ContainsPoint(gHandles[v], x, y) == 0 then
			m_mouseInsideSubMenu = false
		else
			m_mouseInsideSubMenu = true
			break
		end
	end
end 
function Dialog_OnRender(dialogHandle, elapsedSec)
	if m_selectedPathWaypointPID == nil then
		return
	end
	
	if m_mouseInsideSubMenu == false and (ToBool(Dialog_IsLMouseDown(dialogHandle))) then
		KEP_ClearSelection()
		MenuCloseThis()
	end
	
	local camX, camY, camZ = KEP_GetPlayerCameraOrientation()
	local dotP = math.abs(dotProduct(camX, camY, camZ, m_startCamX, m_startCamY, m_startCamZ))
	if dotP < CAM_PADDING then
		KEP_ClearSelection()
		MenuCloseThis()
	end

	local posX, posY, posZ = KEP_GetPlayerPosition()
	local wpX, wpY, wpZ = KEP_DynamicObjectGetPositionAnim(m_selectedPathWaypointPID)
	local xDiff = posX - wpX
	local yDiff = posY - wpY
	local zDiff = posZ - wpZ
	local xyzDistanceSqrd = xDiff * xDiff + zDiff * zDiff + yDiff * yDiff
	if (INTERACTION_RANGE_SQRD <= xyzDistanceSqrd) then
		KEP_ClearSelection()
		MenuCloseThis()
	end
end

function PathWaypointSelectedEventHandler ( dispatcher, fromNetId, event, eventId, filter, objectId )
	m_selectedPathWaypointPID = KEP_EventDecodeNumber(event)
	m_isLastGameItem = (KEP_EventDecodeNumber(event) == 1)
	Events.sendEvent("RequestForPathWaypointPropertiesEvent", {pathWaypointPID = m_selectedPathWaypointPID})
end

function onItemNameResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    local UNID = KEP_EventDecodeNumber(tEvent)
    if m_parentUNID == UNID then
		m_parentPathName = KEP_EventDecodeString(tEvent)
		Static_SetText( gHandles["stcParentPathName"], m_parentPathName )
	end
end
function ResponseToPathWaypointPropertiesEventHandler ( event )
	m_waypointIndex = event.index
	Static_SetText( gHandles["stcPathWaypointNo"], "Waypoint " .. tostring(m_waypointIndex) )
	m_parentUNID = event.parentUNID
	local requestItemNameEvent = KEP_EventCreate("INVENTORY_HANDLER_REQUEST_ITEM_NAME")
	KEP_EventEncodeNumber(requestItemNameEvent, tonumber(m_parentUNID) or 0)
	KEP_EventSetFilter(requestItemNameEvent, 4)
	KEP_EventQueue(requestItemNameEvent)
	m_pauseTime = event.pauseTime
	m_rotationType = event.rotationType
	m_easeIn = event.easeIn
	m_easeOut = event.easeOut
	m_collisionTrigger = event.collisionTrigger
	m_triggerSize = event.triggerSize
end

function btnAddWaypoint_OnMouseEnter( buttonHandle )
	Element_SetCoords(Image_GetDisplayElement(gHandles[m_addWaypointControls.img]),
					  TEXCOORDS.MousedOverAdd.left, TEXCOORDS.MousedOverAdd.top, TEXCOORDS.MousedOverAdd.right, TEXCOORDS.MousedOverAdd.bot)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles[m_addWaypointControls.stc])), 0, COLORS.MousedOverWhite)
end
function btnAddWaypoint_OnMouseLeave( buttonHandle )
	Element_SetCoords(Image_GetDisplayElement(gHandles[m_addWaypointControls.img]),
					  TEXCOORDS.DefaultAdd.left, TEXCOORDS.DefaultAdd.top, TEXCOORDS.DefaultAdd.right, TEXCOORDS.DefaultAdd.bot)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles[m_addWaypointControls.stc])), 0, COLORS.DefaultBlue)
end
function btnAddWaypoint_OnButtonClicked( buttonHandle )
	_, radianY, _ = KEP_DynamicObjectGetRotation(m_selectedPathWaypointPID)
	rX = math.cos(radianY)
	rY = 0
	rZ = -math.sin(radianY)
	pX, pY, pZ = KEP_DynamicObjectGetPositionAnim(m_selectedPathWaypointPID)
	Events.sendEvent("PLACE_PATH_WAYPOINT", {GLID = GameDefinitions.PATHWAYPOINT_GLID,
											 UNID = GameDefinitions.PATHWAYPOINT_UNID,
											 behavior = "pathWaypoint",
											 x = pX + m_xOffsetForNextWaypoint, y = pY + m_yOffsetForNextWaypoint, z = pZ + m_zOffsetForNextWaypoint,
											 rx = rX, ry = rY, rz = rZ,
											 additionalPathInfo = {parentPathPID = nil, prevPathWaypointPID = m_selectedPathWaypointPID}})
	KEP_ClearSelection()
	MenuCloseThis()
end

function btnEditWaypoint_OnMouseEnter( buttonHandle )
	Element_SetCoords(Image_GetDisplayElement(gHandles[m_editWaypointControls.img]),
					  TEXCOORDS.MousedOverEdit.left, TEXCOORDS.MousedOverEdit.top, TEXCOORDS.MousedOverEdit.right, TEXCOORDS.MousedOverEdit.bot)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles[m_editWaypointControls.stc])), 0, COLORS.MousedOverWhite)
end
function btnEditWaypoint_OnMouseLeave( buttonHandle )
	Element_SetCoords(Image_GetDisplayElement(gHandles[m_editWaypointControls.img]),
					  TEXCOORDS.DefaultEdit.left, TEXCOORDS.DefaultEdit.top, TEXCOORDS.DefaultEdit.right, TEXCOORDS.DefaultEdit.bot)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles[m_editWaypointControls.stc])), 0, COLORS.DefaultBlue)
end
function btnEditWaypoint_OnButtonClicked( buttonHandle )
	MenuOpen("Framework_PathWaypointEditor.xml")
	local e = KEP_EventCreate("EditPathWaypointEvent")
	KEP_EventEncodeNumber(e, m_selectedPathWaypointPID)
	KEP_EventEncodeNumber(e, m_waypointIndex)
	KEP_EventEncodeString(e, m_parentPathName)
	KEP_EventEncodeNumber(e, m_pauseTime)
	KEP_EventEncodeString(e, m_rotationType)
	KEP_EventEncodeNumber(e, m_easeIn)
	KEP_EventEncodeNumber(e, m_easeOut)
	KEP_EventEncodeNumber(e, m_collisionTrigger)
	KEP_EventEncodeNumber(e, m_triggerSize)
	KEP_EventQueue(e)
	KEP_ClearSelection()
	MenuCloseThis()
end

function btnDelWaypoint_OnMouseEnter( buttonHandle )
	Element_SetCoords(Image_GetDisplayElement(gHandles[m_delWaypointControls.img]),
					  TEXCOORDS.MousedOverDel.left, TEXCOORDS.MousedOverDel.top, TEXCOORDS.MousedOverDel.right, TEXCOORDS.MousedOverDel.bot)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles[m_delWaypointControls.stc])), 0, COLORS.MousedOverWhite)
end
function btnDelWaypoint_OnMouseLeave( buttonHandle )
	Element_SetCoords(Image_GetDisplayElement(gHandles[m_delWaypointControls.img]),
					  TEXCOORDS.DefaultDel.left, TEXCOORDS.DefaultDel.top, TEXCOORDS.DefaultDel.right, TEXCOORDS.DefaultDel.bot)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles[m_delWaypointControls.stc])), 0, COLORS.DefaultBlue)
end
function btnDelWaypoint_OnButtonClicked( buttonHandle )
	if m_isLastGameItem then
		MenuOpen("Framework_LastGameItemWarning.xml")			
		local event = KEP_EventCreate("FrameworkLastGameItemPID")
		KEP_EventEncodeNumber(event, m_selectedPathWaypointPID)
		KEP_EventQueue(event)
		KEP_ClearSelection()
		MenuCloseThis()
	else
		Events.sendEvent("FRAMEWORK_PICKUP_GAME_ITEM", {PID = m_selectedPathWaypointPID})
		KEP_ClearSelection()
		MenuCloseThis()
	end
end