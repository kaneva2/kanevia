--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Crafting.lua
--
-- Displays available recipes for crafting
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_InventoryHelper.lua")
dofile("Framework_DragDropHelper.lua")

----------------------
-- Constants
----------------------
local KEY_C = 67
local ITEMS_PER_PAGE = 20
local BAR_WIDTH =  82
local YELLOW = {31, 179, 32, 185}

local TYPE_FILTER = "playerQuests"

local QUEST_ICON = {	COMPLETE   = {left = 474, top = 134, right = 500, bottom = 160},
						ACTIVE 	   = {left = 474, top = 82, right = 500, bottom = 108},
						REP_ACTIVE = {left = 474, top = 186, right = 500, bottom = 212}
					}

local TRACKING_QUEST_ICON = {	COMPLETE   = {left = 474, top = 439, right = 512, bottom = 475},
								ACTIVE	   = {left = 474, top = 399, right = 512, bottom = 437},
								REP_ACTIVE = {left = 474, top = 476, right = 512, bottom = 512}
								}

local TRACKING_QUEST_COLOR = {a = 255, r = 149, g = 126, b = 52}
----------------------
-- Local functions
----------------------
local customSort
local formatDisplayTable
local formatTooltips
local updateInventoryMenu
local trackQuest
local untrackQuest
local selectQuest
local updateSelectedQuest
local clearSelectedQuest
local getQuestState
local getQuestByIndex
local getQuestByUNID
local getIndexByUNID
local moveSelctedQuestBG
local updatePage

----------------------
-- Local vars
----------------------

-- Menu Display
g_displayTable = {}

-- Quests player has
local m_playerQuests = nil

-- Crafting Helpers Currently Useable
local m_gameQuests = {}

-- Drop-down Filter
local m_searchText = ""

-- Current page
local m_page = 1
local m_maxPage = 1

-- Selected Quest UNID
local m_selectedQuest

-- Tracked Quest UNID
local m_trackedQuest

-- Player name
local m_playerName
local m_gameItems = nil

local m_playerInventory = nil
local m_playerInventoryByUNID = {}


-- Called when the menu is created
function onCreate()
	-- Register event handlers
	-- Register handlers for client-to-client events
	KEP_EventRegisterHandler("updateQuestsClient", "UPDATE_QUEST_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateQuestsClientFull", "UPDATE_QUEST_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("returnTrackedQuest", "FRAMEWORK_RETURN_TRACKED_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCustomDrop", "DRAG_DROP_CUSTOM_DROP", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()	

	InventoryHelper.initializeInventory("g_displayTable", true, ITEMS_PER_PAGE)
	--InventoryHelper.initializeHover()
	--InventoryHelper.initializePageNumbers()
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")
	
	-- Request the player's inventory
	requestInventory(GAME_PID)
	requestInventory(INVENTORY_PID)
	requestInventory(QUEST_PID)
	
	-- Retrieve recipes from the server
	m_playerName = KEP_GetLoginName()	

	local ev = KEP_EventCreate("FRAMEWORK_GET_TRACKED_QUEST") -- Get Tracked quest from compass
	KEP_EventQueue(ev)

	-- Define item button click function handlers
	for i = 1, ITEMS_PER_PAGE do
		-- On recipe click
		_G["btnItem" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			local s, e, index = string.find(Control_GetName(btnHandle), "(%d+)")

			
			local questIndex = tonumber(index) + (ITEMS_PER_PAGE * (m_page - 1))

			local quest = getQuestByIndex(questIndex )

			if quest then
				local x = Control_GetLocationX(gHandles["btnItem" .. index])
				local y = Control_GetLocationY(gHandles["btnItem" .. index])
				moveSelctedQuestBG(x-1, y, true)
				selectQuest(quest.UNID)
			end
		end
	end
end

function onDestroy()
	InventoryHelper.destroy()
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
	InventoryHelper.onButtonDown(x, y)
	updateInventoryMenu()
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

-- View Quest
function btnView_OnButtonClicked(buttonHandle)
	-- Destroy all current quest buttons, images and statics
	if m_selectedQuest then
		local quest = getQuestByUNID(m_selectedQuest)

		if quest then
			MenuOpen("Framework_QuestInfo.xml")
			-- Send quest info over
			local ev = KEP_EventCreate("FRAMEWORK_QUEST_INFO") -- Handled by Framework_QuestInfo or Framework_QuestTurnIn
			KEP_EventEncodeString(ev, Events.encode(quest))
			KEP_EventEncodeString(ev, "false") -- Quest isn't acceptable
			KEP_EventEncodeString(ev, "false") -- Quest isn't modal
			KEP_EventQueue(ev)
		end
	end
end

-- View Quest
function btnTrack_OnButtonClicked(buttonHandle)
	-- Destroy all current quest buttons, images and statics
	if m_selectedQuest then
		if m_selectedQuest == m_trackedQuest then
			untrackQuest()
		else
			trackQuest(m_selectedQuest)
		end
		
		updateSelectedQuest()
	end
end

-- Delete Quest
function btnDelete_OnButtonClicked(buttonHandle)
	-- Destroy all current quest buttons, images and statics
	if m_selectedQuest then
		local quest = getQuestByUNID(m_selectedQuest)
		abandonQuest(quest.UNID)
		clearSelectedQuest()
	end
end

function btnBack_OnButtonClicked(buttonHandle)
	moveSelctedQuestBG(0, 0, false)
	m_page = m_page - 1
	updateInventoryMenu()
end

function btnNext_OnButtonClicked(buttonHandle)
	moveSelctedQuestBG(0, 0, false)
	m_page = m_page + 1
	updateInventoryMenu()
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

function updateQuestsClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateUNID = KEP_EventDecodeNumber(tEvent)
	local updateQuest = decompileInventoryItem(tEvent)	

	for i,info in pairs(m_playerQuests) do
	
	if tonumber(info.UNID) == tonumber(updateUNID) then
			info = updateQuest

			formatDisplayTable()
			updateInventoryMenu()

			if m_selectedQuest and m_selectedQuest == updateQuest.UNID and m_gameItems then
				updateSelectedQuest()
			end		

			return
		end
	end
end

function updateQuestsClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_playerQuests = decompileInventory(tEvent)

	if m_gameItems and m_playerInventory then
		formatDisplayTable()
		updateSelectedQuest()
	end

end

-- Event handlers
function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)

	local inventory = m_playerInventory
	if inventory[updateIndex] then
		inventory[updateIndex] = updateItem

		updateInventory(inventory)
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local inventory = decompileInventory(tEvent)
	updateInventory(inventory)
end


function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem

	if m_playerInventory and m_playerQuests then
		formatDisplayTable()
		updateSelectedQuest()
	end
	
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
	if m_playerInventory and m_playerQuests then
		formatDisplayTable()
		updateSelectedQuest()
	end

end

function onCustomDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	requestInventory(INVENTORY_PID)
end

function returnTrackedQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = tonumber(KEP_EventDecodeString(tEvent))

	if questUNID and not m_selectedQuest then	
		trackQuest(questUNID)
		selectQuest(questUNID)
	end
end

-- -- -- -- -- -- --
-- Local Display functions
-- -- -- -- -- -- --



formatDisplayTable = function()
	g_displayTable = {}

	for _, playerQuests in pairs(m_playerQuests) do
		if playerQuests.active then
			if m_trackedQuest and m_trackedQuest == playerQuests.UNID then
				table.insert(g_displayTable, 1, playerQuests)
			else
				table.insert(g_displayTable, playerQuests)
			end
			
		end
	end

	--InventoryHelper.setMaxPages(getQuestsCount(g_displayTable))

	--table.sort(g_displayTable, customSort)

	updateInventoryMenu()

	if m_trackedQuest then
		local quest = getQuestByUNID(m_trackedQuest)

		if quest == nil then
			untrackQuest()
		end
	end

	if m_selectedQuest then
		local quest = getQuestByUNID(m_selectedQuest)

		if quest == nil then
			clearSelectedQuest()
		end
	end
end

customSort = function(a, b)
	return tonumber(a.UNID) < tonumber(b.UNID)
end

formatTooltips = function(item)	
	if item then
		item.tooltip = {}

		item.tooltip["header"] = {label = tostring(item.properties.name)}

		if item.properties.description then
		 	item.tooltip["body"] = {label = item.properties.description}
		end

		if item.properties.requiredItem and item.properties.requiredItem.requiredCount > 0 then
			local questReqCount = item.properties.requiredItem.requiredCount or 0
			local questReqName = m_gameItems[tostring(item.properties.requiredItem.requiredUNID)].name or ""
			local playerItemCount = item.properties.requiredItem.progressCount or 0
			playerItemCount = math.min(playerItemCount, questReqCount)
			local questTrackInfo = ""
			if usesFulfillCount(item.properties.questType)  then
				playerItemCount = item.fulfillCount or 0
				playerItemCount = math.min(playerItemCount, questReqCount)
			end

			questTrackInfo = playerItemCount .. "/" .. questReqCount .. " " .. questReqName

			item.tooltip["requires1"] = {label = questTrackInfo}
		end


		if item.properties.rewardItem and item.properties.rewardItem.rewardCount > 0 then
			local questRewardCount = item.properties.rewardItem.rewardCount or 0
			local questRewardName = m_gameItems[tostring(item.properties.rewardItem.rewardUNID)].name or ""
			local questTrackInfo = ""

			questTrackInfo = tostring(questRewardCount) .. " " .. questRewardName

			item.tooltip["reward"] = {label = questTrackInfo}
		end

		item.tooltip["loot"] = {label = "Click to get more information.", type = ITEM_TYPES.QUEST}
	end
end

updateInventoryMenu = function()

	updatePage()
	local itemIndex = 1
	local startIndex = 1 + (ITEMS_PER_PAGE * (m_page - 1))
	local endIndex = startIndex + ITEMS_PER_PAGE-1

	InventoryHelper.setNameplates("name")

	--for i,info in pairs(g_displayTable) do
	for i=startIndex, endIndex do
		if itemIndex <= ITEMS_PER_PAGE then
			local quest = g_displayTable[i]

			
			local element
			local questIcon
			if quest then
				formatTooltips(quest)
				if m_trackedQuest == quest.UNID and m_page == 1 then
					element = Image_GetDisplayElement(gHandles["imgTrackingQuestIcon"])
					questIcon = TRACKING_QUEST_ICON[getQuestState(quest)]

					if questIcon then
						Element_SetCoords(element, questIcon.left, questIcon.top, questIcon.right, questIcon.bottom)
						Control_SetVisible( gHandles["imgTrackingQuestIcon"], true )
						--Control_SetVisible( gHandles["imgTrackingQuestText"], true )
					end

					Control_SetVisible( gHandles["imgQuestIcon" .. itemIndex], false )
					--Control_SetVisible( gHandles["imgItemName" .. i], false )
				else
					element = Image_GetDisplayElement(gHandles["imgQuestIcon" .. itemIndex])
					questIcon = QUEST_ICON[getQuestState(quest)]

					if questIcon then
						Element_SetCoords(element, questIcon.left, questIcon.top, questIcon.right, questIcon.bottom)
						Control_SetVisible( gHandles["imgQuestIcon" .. itemIndex], true )
					end

					if itemIndex == 1 then
						Control_SetVisible( gHandles["imgTrackingQuestIcon"], false )
						--moveSelctedQuestBG(0, 0, false)
						--Control_SetVisible( gHandles["imgTrackingQuestText"], false )
					end
				end			

				local playerProgress, questReqCount = getQuestProgress(quest) 
				
				Control_SetVisible ( gHandles["imgDurabilityBG"..itemIndex], true)
				Control_SetVisible ( gHandles["imgDurabilityGradiant"..itemIndex], true)
				Control_SetVisible ( gHandles["imgDurabilityBar"..itemIndex], true)
					--local questReqName = m_gameItems[tostring(quest.properties.requiredItem.requiredUNID)] or ""

				local questTrackPecent = playerProgress / questReqCount 

				Control_SetSize(gHandles["imgDurabilityBar" .. itemIndex], questTrackPecent * BAR_WIDTH, 8)

				Element_SetCoords(Image_GetDisplayElement(gHandles["imgDurabilityBar" .. itemIndex]), unpack(YELLOW))


				applyImage(gHandles["imgItem"..itemIndex], tonumber(quest.properties.GLID))
				Control_SetVisible( gHandles["imgItem" .. itemIndex], true )

				local questName = quest.properties.name or ""
				Static_SetText(gHandles["stcItem"..itemIndex], questName)
			else
				Control_SetVisible( gHandles["imgItem" .. itemIndex], false )
				Control_SetVisible( gHandles["imgItemBG" .. itemIndex], false )
				Control_SetVisible( gHandles["imgQuestIcon" .. itemIndex], false )
				Control_SetVisible ( gHandles["imgDurabilityBG"..itemIndex], false)
				Control_SetVisible ( gHandles["imgDurabilityGradiant"..itemIndex], false)
				Control_SetVisible ( gHandles["imgDurabilityBar"..itemIndex], false)
				Control_SetVisible( gHandles["imgItemName"..itemIndex], false)
				Control_SetVisible( gHandles["stcItem"..itemIndex], false)
			end
		end	
		itemIndex = itemIndex + 1	
	end
end

-- -- -- -- -- -- --
-- Local functions
-- -- -- -- -- -- --
function getQuestProgress(quest)

	-- Debug.printTableError("questProperties", questProperties)
	if quest.fulfilled then
		return 1, 1 -- If quest is fulfilled, then bar should aways be full
	end

	local questProperties = quest.properties
	if questProperties.questType == "goTo" or questProperties.questType == "talkTo" then
		return 0, 1  -- If these types of quest are not fulfilled, then there is 0 progress
	else
		local requiredItem = questProperties.requiredItem.requiredUNID
		local requiredCount = questProperties.requiredItem.requiredCount
		local progressCount = 0 
		local backpackCount = m_playerInventoryByUNID[tostring(requiredItem)] or 0

		if usesFulfillCount(questProperties.questType)  then
			progressCount = quest.fulfillCount or 0
			progressCount = math.min(progressCount, requiredCount) -- Don't exceed the amount needed
			return progressCount, requiredCount
		elseif questProperties.questType == "takeTo" then

			progressCount = math.min(backpackCount, requiredCount) / 2
			return progressCount, requiredCount
		else
			progressCount = math.min(backpackCount, requiredCount)
			return progressCount, requiredCount
		end
 	end

end

function updateInventory(inventory)
	if inventory then
		m_playerInventory = inventory
	end
 
 	m_playerInventoryByUNID = {}
	for i=1,#m_playerInventory do
		if m_playerInventoryByUNID[tostring(m_playerInventory[i].UNID)] == nil then
			m_playerInventoryByUNID[tostring(m_playerInventory[i].UNID)] = m_playerInventory[i].count
		else
			m_playerInventoryByUNID[tostring(m_playerInventory[i].UNID)] = m_playerInventoryByUNID[tostring(m_playerInventory[i].UNID)] + m_playerInventory[i].count
		end
	end 

	if m_gameItems and m_playerQuests then
		formatDisplayTable()
		updateSelectedQuest()
	end
end


trackQuest = function(UNID)
	local quest = getQuestByUNID(UNID)

	if quest then
		m_trackedQuest = UNID

		local index = getIndexByUNID(UNID) - (ITEMS_PER_PAGE * (m_page -1))

		local x = Control_GetLocationX(gHandles["btnItem1"])
		local y = Control_GetLocationY(gHandles["btnItem1"])
		moveSelctedQuestBG(x-1, y, true)

		-- Send quest info over to Quest Compass
		local ev = KEP_EventCreate("FRAMEWORK_TRACK_PLAYER_QUEST")
		KEP_EventEncodeString(ev, quest.UNID)		
		KEP_EventQueue(ev)

		m_page = 1

		formatDisplayTable()
		updateInventoryMenu()
	end	
end

untrackQuest = function()
	m_trackedQuest = nil
	moveSelctedQuestBG(0, 0, false)

	-- Send quest info over to Quest Compass
	local ev = KEP_EventCreate("FRAMEWORK_TRACK_PLAYER_QUEST")
	KEP_EventEncodeString(ev, 0)		
	KEP_EventQueue(ev)

	Control_SetVisible( gHandles["imgTrackingQuestIcon"], false )
	--Control_SetVisible( gHandles["imgTrackingQuestText"], false )

	formatDisplayTable()
end

selectQuest = function(UNID)
	if UNID then
		m_selectedQuest = UNID

		updateSelectedQuest()
	end	
end

updateSelectedQuest = function()
	local quest = getQuestByUNID(m_selectedQuest)

	if quest then
		applyImage(gHandles["imgItemSelect"], tonumber(quest.properties.GLID))
		Static_SetText(gHandles["stcItemSelect"], quest.properties.name)
		Static_SetText(gHandles["stcItemSelectName"], quest.properties.name)		

		if quest.properties.requiredItem and quest.properties.requiredItem.requiredCount > 0 then
			local questReqCount = quest.properties.requiredItem.requiredCount or 0
			local questReqName = m_gameItems[tostring(quest.properties.requiredItem.requiredUNID)].name or ""
			local playerItemCount = m_playerInventoryByUNID[tostring(quest.properties.requiredItem.requiredUNID)] or 0
			playerItemCount = math.min(playerItemCount, questReqCount)
			local questTrackInfo = ""

			if usesFulfillCount(quest.properties.questType) then
				playerItemCount = quest.fulfillCount or 0
			end

			questTrackInfo = playerItemCount .. "/" .. questReqCount .. " " .. questReqName

			Static_SetText(gHandles["stcItemSelectDesc"], questTrackInfo)
			Control_SetVisible( gHandles["stcItemSelectDesc"], true )	
		else
			Control_SetVisible( gHandles["stcItemSelectDesc"], false )		
		end

		local element
		local questIcon

		if m_trackedQuest == quest.UNID then
			element = Image_GetDisplayElement(gHandles["imgSelectTrackingIcon"])
			questIcon = TRACKING_QUEST_ICON[getQuestState(quest)]

			if questIcon then
				Element_SetCoords(element, questIcon.left, questIcon.top, questIcon.right, questIcon.bottom)
				Control_SetVisible( gHandles["imgSelectTrackingIcon"], true )
				Control_SetVisible( gHandles["imgSelectTrackingText"], true )
			end

			Control_SetVisible( gHandles["imgQuestSelectIcon"], false )
			--Control_SetVisible( gHandles["imgItemNameSelect"], false )

			Static_SetText(gHandles["btnTrack"], "Stop Tracking")
		else
			element = Image_GetDisplayElement(gHandles["imgQuestSelectIcon"])
			questIcon = QUEST_ICON[getQuestState(quest)]

			if questIcon then
				Element_SetCoords(element, questIcon.left, questIcon.top, questIcon.right, questIcon.bottom)
				Control_SetVisible( gHandles["imgQuestSelectIcon"], true )
			end

			Control_SetVisible( gHandles["imgSelectTrackingIcon"], false )
			Control_SetVisible( gHandles["imgSelectTrackingText"], false )

			Control_SetVisible( gHandles["imgItemNameSelect"], true )

			Static_SetText(gHandles["btnTrack"], "Show in Compass")
		end

		Control_SetVisible( gHandles["imgItemSelectBG"], true )
		Control_SetVisible( gHandles["imgItemSelect"], true )		
		Control_SetVisible( gHandles["stcItemSelect"], true )
		Control_SetVisible( gHandles["stcItemSelectName"], true )		
		Control_SetEnabled( gHandles["btnView"], true )
		Control_SetVisible( gHandles["btnView"], true )
		Control_SetEnabled( gHandles["btnTrack"], true )
		Control_SetVisible( gHandles["btnTrack"], true )

		local questItem = m_gameItems[tostring(quest.UNID)]
		if questItem then
			if questItem.autoAccept and questItem.autoAccept == "true" then -- Auto-accept quests cannot be deleted (FOR BRANDY: Comment this out if you need to delete quests)
				Control_SetEnabled( gHandles["btnDelete"], false )
				Control_SetVisible( gHandles["btnDelete"], false )
			else
				Control_SetEnabled( gHandles["btnDelete"], true )
				Control_SetVisible( gHandles["btnDelete"], true )
			end
		end	
	end
end

clearSelectedQuest = function()
	m_selectedQuest = nil
	Control_SetVisible( gHandles["imgSelectedBG"], false)
	Control_SetVisible( gHandles["stcItemSelectDesc"], false )
	Control_SetVisible( gHandles["imgQuestSelectIcon"], false )
	Control_SetVisible( gHandles["imgItemSelectBG"], false )
	Control_SetVisible( gHandles["imgItemSelect"], false )
	Control_SetVisible( gHandles["imgItemNameSelect"], false )
	Control_SetVisible( gHandles["stcItemSelect"], false )
	Control_SetVisible( gHandles["stcItemSelectName"], false )		
	Control_SetEnabled( gHandles["btnView"], false )
	Control_SetVisible( gHandles["btnView"], false )
	Control_SetEnabled( gHandles["btnTrack"], false )
	Control_SetVisible( gHandles["btnTrack"], false )
	Control_SetEnabled( gHandles["btnDelete"], false )
	Control_SetVisible( gHandles["btnDelete"], false )
	Control_SetVisible( gHandles["imgSelectTrackingIcon"], false )
	Control_SetVisible( gHandles["imgSelectTrackingText"], false )
end

moveSelctedQuestBG = function(x, y, visible)
	Control_SetVisible( gHandles["imgSelectedBG"], visible )
	Control_SetLocationX(gHandles["imgSelectedBG"], x)
	Control_SetLocationY(gHandles["imgSelectedBG"], y)
end

getQuestByIndex = function(index)
	if g_displayTable[index] then
		return g_displayTable[index]
	end

	return nil
end

getQuestByUNID = function(UNID)
	if UNID then
		for i,info in pairs(g_displayTable) do
			if info.UNID == UNID then
				return info
			end
		end
	end
end

getIndexByUNID = function(UNID)
	if UNID then
		for i,info in pairs(g_displayTable) do
			if info.UNID == UNID then
				return i
			end
		end
	end

	return nil
end

-- Gets the state of a quest for a player
getQuestState = function(quest)
	local questState = "NONE"
	-- Quest has a required item
	if quest.properties.requiredItem and quest.properties.requiredItem.requiredUNID ~= "0" then
		local questReqCount = quest.properties.requiredItem.requiredCount or 0
		local playerItemCount = m_playerInventoryByUNID[tostring(quest.properties.requiredItem.requiredUNID)] or 0
		
		-- Player's ready to complete this quest
		if playerItemCount >= questReqCount then
			questState = "COMPLETE"
			
		-- Quest isn't ready to turn in yet
		else
			-- Quest is repeatable
			if quest.properties.repeatable == "true" then
				questState = "REP_ACTIVE"
			else
				questState = "ACTIVE"
			end
		end
	-- No required item?
	else
		questState = "COMPLETE"
	end
	
	return questState
end

-- Gets the number of quests in an array (not counting empty indices)
getQuestsCount = function(questList)
	local questsCount = 0
	for i,info in pairs(questList) do
		if info ~= nil and info.UNID ~= nil then
			questsCount = questsCount + 1
		end
	end
	return questsCount
end

updatePage = function()
	local questCount = getQuestsCount(g_displayTable)
	local btnNext = gHandles["btnNext"]
	local btnBack = gHandles["btnBack"]
	local stcPage = gHandles["stcPage"]

	m_maxPage = math.ceil(questCount / ITEMS_PER_PAGE)

	local showArrows = m_maxPage > 1
	local showNext = (showArrows) and (m_page ~= m_maxPage)
	local showBack = (showArrows) and (m_page ~= 1)
	local showText = showArrows

	
	Control_SetVisible(stcPage, showText)
	Control_SetVisible(btnBack, showBack)
	Control_SetVisible(btnNext, showNext)
	

	Static_SetText(gHandles["stcPage"], "("..tostring(m_page).."/"..tostring(m_maxPage)..")")
end