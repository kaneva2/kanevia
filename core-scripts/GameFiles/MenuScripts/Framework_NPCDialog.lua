--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_ActorHelper.lua")

-- Constants
local BUTTON_SPACING = 20 -- How far the OK button is from the bottom of the dialog text
local CENTER_Y_OFFSET = 60

-- Local
local m_currentPage = 1
local m_name = "John Doe"
local m_dialogList = {}
local m_maxPages = 0
local m_maxHeight = 0
local m_isRider = false
local m_riderPID = nil

function onCreate()
	Events.registerHandler("SET_DIALOG_TEXT", onDialogSetText)
	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)
	
	-- Register client - client event handlers
	KEP_EventRegisterHandler("frameworkNPCDialogHandler", "FrameworkNPCDialog", KEP.HIGH_PRIO)
	CloseAllActorMenus()
end

function onDestroy()
	if m_isRider then
		Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = m_riderPID, interacting = false})
	end
end

function onCloseDialog(event)
	MenuCloseThis()
end

---- Button Handlers

-- Previous page
function btnBack_OnButtonClicked(buttonHandle)
	-- Destroy all current quest buttons, images and statics
	m_currentPage = m_currentPage - 1
	displayText()
end

-- Next page
function btnNext_OnButtonClicked(buttonHandle)
	-- Say Goodbye
	if m_maxPages == 1 or m_currentPage == m_maxPages then
		MenuCloseThis()
	else -- Destroy all current quest buttons, images and statics
		m_currentPage = m_currentPage + 1
		displayText()
	end
end

---- Event Handlers

function onDialogSetText(event)
	m_isRider = event.isRider
	if m_isRider then
		m_riderPID = event.riderPID
		Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = event.riderPID, interacting = true})
	end
	m_name = event.name
	m_dialogList = event.text
	
	m_maxPages = getDialogCount()
	m_maxHeight = getMaxHeight()
	displayText()
end

-- Handled when a client needs to use the NPC Dialog menu
function frameworkNPCDialogHandler(dispatcher, fromNetid, event, eventID, filter, objectID)
	if KEP_EventMoreToDecode(event) ~= 0 then
		m_name = KEP_EventDecodeString(event)
	end
	if KEP_EventMoreToDecode(event) ~= 0 then
		m_dialogList = KEP_EventDecodeString(event)
	end
	
	m_maxPages = getDialogCount()
	m_maxHeight = getMaxHeight()
	displayText()
end

function Dialog_OnMoved(dialogHandle, x, y)
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	
	local size = MenuGetSizeThis()
	local dialogWidth = size.width
	local dialogHeight = size.height

	MenuSetLocationThis((screenWidth/2)-(dialogWidth/2), ((screenHeight/2)-(dialogHeight/2)-CENTER_Y_OFFSET))
end

---- Local Functions

-- Returns the height of the dialog minus the height of the dialog text field
function getDialogRawHeight()
	return (Dialog_GetHeight(gDialogHandle) - Control_GetHeight(gHandles["stcDialog"]))
end

-- Returns the proper Y position of the OK button, taking into account the height of the dialog text field
function getButtonY(strHeight)
	return (Control_GetLocationY(gHandles["stcDialog"]) + strHeight + BUTTON_SPACING)
end

function updatePagination()
	-- Only show pagination if there is more than one page
	Control_SetVisible(gHandles["imgLine3"], m_maxPages > 1)
	Control_SetVisible(gHandles["stcPage"], m_maxPages > 1)
	Control_SetVisible(gHandles["btnBack"], m_maxPages > 1)
	Control_SetVisible(gHandles["btnNext"], true)
	Control_SetEnabled(gHandles["btnNext"], true)

	Static_SetText(gHandles["stcPage"], tostring(m_currentPage).."\/"..tostring(m_maxPages))

	if m_maxPages > 1 then
		-- Last page
		if m_currentPage >= m_maxPages then
			m_currentPage = m_maxPages
			Control_SetEnabled(gHandles["btnBack"], true)
			Control_SetVisible(gHandles["btnBack"], true)
			Static_SetText(gHandles["btnNext"], "Goodbye")
		-- First page
		elseif m_currentPage <= 1 then
			m_currentPage = 1
			Control_SetEnabled(gHandles["btnBack"], false)
			Control_SetVisible(gHandles["btnBack"], false)
			Static_SetText(gHandles["btnNext"], "Next >")
		-- All other pages
		else
			Control_SetEnabled(gHandles["btnBack"], true)
			Control_SetVisible(gHandles["btnBack"], true)
			Static_SetText(gHandles["btnNext"], "Next >")
		end
	else
		Static_SetText(gHandles["btnNext"], "Goodbye")
	end
end

function displayText()

	Static_SetText(gHandles["stcTitle"], m_name or "NPC Dialog")
	
	local ctrlWidth = Dialog_GetWidth(gDialogHandle)
	MenuSetSizeThis(ctrlWidth, m_maxHeight+getDialogRawHeight())
	
	ctrlWidth = Control_GetWidth(gHandles["stcDialog"])
	Control_SetSize(gHandles["stcDialog"], ctrlWidth, m_maxHeight)
	Static_SetText(gHandles["stcDialog"], m_dialogList[m_currentPage].dialogPage)
	
	-- Control_SetLocation(gHandles["btnOK"], Control_GetLocationX(gHandles["btnOK"]), getButtonY(m_maxHeight))
	Control_SetLocation(gHandles["imgLine3"], Control_GetLocationX(gHandles["imgLine3"]), getButtonY(m_maxHeight) - 10)
	Control_SetLocation(gHandles["stcPage"], Control_GetLocationX(gHandles["stcPage"]), getButtonY(m_maxHeight))
	Control_SetLocation(gHandles["btnNext"], Control_GetLocationX(gHandles["btnNext"]), getButtonY(m_maxHeight))
	if m_maxPages == 1 then -- Center the next button
		Control_SetLocation(gHandles["btnNext"], MenuGetSizeThis().width/2 - Control_GetWidth(gHandles["btnNext"])/2, Control_GetLocationY(gHandles["btnNext"]))
	end
	Control_SetLocation(gHandles["btnBack"], Control_GetLocationX(gHandles["btnBack"]), getButtonY(m_maxHeight))
	
	Dialog_OnMoved(gDialogHandle, 0, 0)

	updatePagination()

end

function getMaxHeight()
-- TO DO: write this
	local finalMaxHeight = 0
	local tempMaxHeight = 0
	for i,v in pairs(m_dialogList) do
		tempMaxHeight = Static_GetStringHeight(gHandles["stcDialog"], m_dialogList[i].dialogPage)
		finalMaxHeight = math.max(finalMaxHeight, tempMaxHeight)
	end
	return finalMaxHeight
end

function getDialogCount()
	local tempCount = 0
	for i,v in pairs(m_dialogList) do
		tempCount = tempCount + 1
	end
	return tempCount
end