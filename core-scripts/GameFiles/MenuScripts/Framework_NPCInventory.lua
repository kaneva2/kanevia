--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\CAM.lua")
dofile("..\\Scripts\\GetAttributeType.lua")
dofile("InventoryHelper.lua")
dofile("Framework_EventHelper.lua")

-- Enum menu tabs


local WEB_ADDRESS = "kgp/inventory.aspx?gzip=true&action=Player"
local CAT_AVATAR = "avatar"

local CAT_ALL_MEN = "For%20Men,Tops|For%20Men,Bottoms|For%20Men,Underwear|For%20Men,Shoes|For%20Men,Costumes|Accessories,For%20Men"
local CAT_ALL_WOMEN = "For%20Women,Tops|For%20Women,Bottoms|For%20Women,Lingerie|For%20Women,Dresses%20%26%20Skirts|For%20Women,Shoes|For%20Women,Costumes|Accessories,For%20Women"
local CATEGORIES = {avatar = "btnAvatar"}

local CLICKED_CONTROLS = {"btnUse"}

local SMALL_BUTTON_FONT = 9
local LARGE_BUTTON_FONT = 12
local BUTTON_NAME_LIMIT	= 8 -- #characters when to resize button name
local GREY_SEARCH_COLOR = {a = 255, r = 140, g = 140, b = 140}

local ITEMS_PER_PAGE = 20

local SUB_CATEGORIES = {
	avatar = {
		tabs = {
			btnTops 		= "Tops",
			btnBottoms 		= "Bottoms",
			btnShoes 		= "Shoes",
			btnCostumes 	= "Costumes",
			btnAccessories 	= "Accessories" 
		},

		list = {
			{name = "Emotes"},
			{name = "Underwear",		gender = "M"},
			{name = "Lingerie",			gender = "F"},
			{name = "Dresses &",		data = "Dresses & Skirts"},
			{name = "  Skirts",			data = "Dresses & Skirts"},
			{name = "Access Pass"},
			{name = " "},
			{name = "Storage"}
		}
	}
}


local INVENTORY_OFFSET = 488
NO_RESULTS_BUFFER 				= 30 -- Extra width for button around the length of the text
local m_shopCategoryLink		= "" -- Link to the subsection of the shop

local m_loading					= true --Loading the inventory?
local m_placedGameItem			= nil -- Game item that we're placing -- TODO: Remove and move to Framework_AllGameItems.lua

local m_YesNoQuestion 			= nil
local YES_NO_QUESTION_USE_DEED 	= 1
local m_pendingDeed 			= -1

g_t_items 						= {}
local m_pendingImages			= {}

local m_category 				= CAT_AVATAR
local m_subcategory 			= nil

local m_syncInventoryServer		= true

local m_timeStampOfLastPlacement= nil

local m_npcGender = 0   --0 for male, 1 for female
local m_glidList = {}
local m_npcID = 0
local m_npcPlayer = {}
local m_npcPID = nil

g_iHasAccessPass 				= 0
g_iHasVIPPass 					= 0
g_iZoneHasAccessPass 			= 0
g_iZoneHasVIPPass 				= 0

local m_clickedHeight			= 0
local m_passThrough 			= 1


function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "answerEventHandler", 					"YesNoAnswerEvent",					KEP.MED_PRIO )
	KEP_EventRegisterHandler( "attribEventHandler", 					"AttribEvent",						KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler",				"BrowserPageReadyEvent", 			KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ContentServiceCompletionHandler",		"ContentServiceCompletionEvent",	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "inventoryItemPlacedTimerEventHandler", 	"InventoryItemPlacedTimerEvent", 	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassInfoHandler", 			"ResponseAccessPassInfoEvent", 		KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassZoneInfoHandler", 		"ResponseAccessPassZoneInfoEvent", 	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "updateInventoryHandler",					"UpdateInventoryEvent", 			KEP.MED_PRIO )
	--KEP_EventRegisterHandler( "returnFrameworkStateHandler",			"FrameworkState", 					KEP.MED_PRIO )
	KEP_EventRegisterHandler( "wokInventoryFilterEventHandler",			"WOK_INVENTORY_FILTER", 			KEP.HIGH_PRIO )
	-- Handle ScriptClientEvent events. NOTE: Only needed for Framework events. Would be nice to get other client menus 
	-- integrated with LibClient_Common. Consequences currently unknown
	KEP_EventRegisterHandler( "npcInventoryCreationHandler",			"FRAMEWORK_OPEN_NPC_INVENTORY", 			KEP.HIGH_PRIO )

	KEP_EventRegisterHandler( "updateWOKInventoryHandler", "InventoryUpdateEvent", KEP.HIGH_PRIO)

	InventoryHelper.registerInventoryHandlers()
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( DISPLAY_ACTION_ITEM_ERRORS_EVENT, 	KEP.HIGH_PRIO )
	KEP_EventRegister( KEP.WORLD_FAME_EVENT, 				KEP.MED_PRIO )
	KEP_EventRegister( "BrowserPageReadyEvent",				KEP.MED_PRIO )
	KEP_EventRegister( "ChangeInventoryTabEvent",			KEP.MED_PRIO )
	KEP_EventRegister( "ContentServiceCompletionEvent", 	KEP.MED_PRIO )
	KEP_EventRegister( "DynamicObjectChangeEvent", 			KEP.MED_PRIO )
	KEP_EventRegister( "InventoryItemPlacedTimerEvent", 	KEP.MED_PRIO )
	KEP_EventRegister( "SelectedItemInfoEvent", 			KEP.MED_PRIO )
	KEP_EventRegister( "SendGLIDEvent",			 			KEP.MED_PRIO )
	KEP_EventRegister( "UpdateInventoryEvent",				KEP.MED_PRIO )
	KEP_EventRegister( "YesNoAnswerEvent", 					KEP.MED_PRIO )
	KEP_EventRegister( "YesNoBoxEvent", 					KEP.MED_PRIO )
	KEP_EventRegister( "FRAMEWORK_GAME_ITEM_LOAD_MESSAGE",  KEP.MED_PRIO ) -- Informs Framework_GameItemLoadWarning of what to display on Game Item load overflow
	InventoryHelper.registerInventoryEvents()
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate(dialogHandle)

	MenuSetMinimized("CharacterCreation3.xml", true)

	--get the player's passes
	KEP_EventCreateAndQueue( "RequestAccessPassInfoEvent" )

	--get passes for current zone
	KEP_EventCreateAndQueue( "RequestAccessPassZoneInfoEvent" )

	m_clickedHeight = Control_GetHeight(gHandles["imgClicked"])

	InventoryHelper.initializeInventory("g_t_items")
	InventoryHelper.initializeHover("it_use_type", USE_TYPE.ACTION_ITEM)
	InventoryHelper.initializeClicked(CLICKED_CONTROLS)
	InventoryHelper.initializePageNumbers()
	InventoryHelper.initializeCategories(CATEGORIES, CAT_AVATAR)


	EditBox_SetTextColor( gHandles["txtSearch"], GREY_SEARCH_COLOR )

	requestInventory(false)
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	if MenuIsOpen("CharacterCreation3") then
		if Control_ContainsPoint(gHandles["imgNav"], x, y) ~= 0 then
			Dialog_SetModal(gDialogHandle, false)
			MenuSetModal("UnifiedNavigation", true)
		end
	end
end

function Dialog_OnMoved(dialogHandle, x, y)
	--local pos = MenuGetLocation()
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local newX = screenWidth/2 - INVENTORY_OFFSET
	local screenHight = Dialog_GetScreenHeight(gDialogHandle)
	local menuSize = MenuGetSizeThis()
	local newY = screenHight/2 - menuSize.height/2
	MenuSetLocation("UnifiedInventory.xml", newX, newY)
	MenuSetMinimized("CharacterCreation3.xml", true)

end

function npcInventoryCreationHandler(  dispatcher, fromNetid, event, eventid, filter, objectid )

	local glidItems = KEP_EventDecodeString(event)
	--m_glidList = Events.decode(glidItems)
	m_npcID = KEP_EventDecodeNumber(event)
	m_npcGender = KEP_EventDecodeNumber(event)

	m_npcPlayer = getPlayerObjectFromNetId(m_npcID)
	m_glidList = KEP_GetPlayerArmedItems(m_npcPlayer)

	if KEP_EventMoreToDecode(event) == 1 then
		m_npcPID = KEP_EventDecodeNumber(event)
		Control_SetVisible(gHandles["btnClose"], false)
		Control_SetEnabled(gHandles["btnClose"], false)
	end
end


function responseAccessPassInfoHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iHasAccessPass 	= Event_DecodeNumber(event)
	g_iHasVIPPass 		= Event_DecodeNumber(event)
end

function responseAccessPassZoneInfoHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iZoneHasAccessPass 	= Event_DecodeNumber(event)
	g_iZoneHasVIPPass 		= Event_DecodeNumber(event)
end

----------------------------------------------------------------------
-- Page Updating
----------------------------------------------------------------------
function updateWOKInventoryHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	requestInventory(true)
end

function inventoryUpdated(page, search, category, updatedFeature)
	requestInventory(true)
end

function updateInventoryHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	InventoryHelper.clearClicked()
	requestInventory(true)
end


-- Handles the return for the unified inventory filter event
function wokInventoryFilterEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local filterString = Event_DecodeString(event)

	m_category = CAT_AVATAR
	m_subcategory = nil

	if string.match(filterString, ",") then
		m_category, m_subcategory = string.match(filterString, "([^,]+),([^,]+)")
	else
		m_category = filterString
	end
	clearPage(true)
	requestInventory(false)
end

function requestInventory(asynchronous)
	m_loading = true
	clearPage()

	if (m_syncInventoryServer == true) then
		m_syncInventoryServer = false
		KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
		if (asynchronous) then
			getInventoryFromWeb()
		end
	else
		getInventoryFromWeb()
	end
end

function getInventoryFromWeb()
	local category = m_category or CAT_AVATAR
	local webSubcategory, gameItemTypes = nil, nil	

	--local parentCategory,childCategory = nil, nil
	if m_subcategory and m_subcategory ~= "" then
		webSubcategory = getWebCategory(KEP_UrlEncode(m_subcategory))
		webSubcategory = string.gsub(webSubcategory, "&", "%26")
	end

	local url =	GameGlobals.WEB_SITE_PREFIX..WEB_ADDRESS
	if not ( next(m_glidList) ) then
		url = url .. "&sort=new"
	else
		url = url.. "&sort=manual&sort_glids="
		local count= 0
		local max = #m_glidList
		for index, GLID in pairs(m_glidList) do
			count = count + 1
			if count ~= max then
				url = url .. tostring(GLID).."|"
			else
				url = url ..tostring(GLID)
			end
		end
	end

	url = url .."&type=avatar&search=" .. tostring(InventoryHelper.searchText).."&categoryPairs="
	local genderUrl =""
	if m_npcGender == 1 then
		genderUrl = "For%20Women"
	else
		genderUrl = "For%20Men"
	end

	if not webSubcategory or webSubcategory == "Access+Pass" then
		if m_npcGender == 1 then
			url = url .. CAT_ALL_WOMEN
		else
			url = url .. CAT_ALL_MEN
		end
	elseif webSubcategory == "Accessories" then
		url = url ..webSubcategory ..","..genderUrl
	elseif webSubcategory == "Dresses" then
		if m_npcGender == 1 then
			url = url .. genderUrl .. ",Dresses%20%26%20Skirts"
		else
			url = url .. genderUrl .. ",Formal%20Wear"
		end
	elseif webSubcategory == "Underwear" and m_npcGender == 1 then
		url = url .. genderUrl ..",Lingerie"
	else
		url = url .. genderUrl ..","..webSubcategory
	end

	if webSubcategory and webSubcategory == "Access+Pass" then
		url = url .."&mature=true"
	end
	
	url = url .. "&passthrough="..tostring(m_passThrough)
	makeWebCall(url,WF.PLAYER_INVENTORY , InventoryHelper.page, InventoryHelper.itemsPerPage)
	m_passThrough = m_passThrough + 1
end


function attribEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	-- Updated Storage/armed item - request sync	
	if (filter == AttribEvent.GENERIC_TYPE or filter == AttribEvent.ARMITEM_TYPE) then
		m_syncInventoryServer = true
		requestInventory(false)
	
	-- Just synced, retrieve from web to update the page
	elseif (filter == AttribEvent.PLAYER_INVEN) then
		getInventoryFromWeb()
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.PLAYER_INVENTORY then
		local xmlData = KEP_EventDecodeString( event )

		-- Test to make sure valid results and results are up to date (for multiple fast calls)
		local result = string.match(xmlData, "<ReturnCode>(.-)</ReturnCode>")
		local passthrough = tonumber( string.match(xmlData,"<Passthrough>(.-)</Passthrough>") or -1 )
		if result ~= "0"  then
			MenuCloseThis()
			MenuClose("CharacterCreation3")
			MenuClose("UnifiedNavigation")
			return
		end

		-- Update the pages
		local totalItems = string.match(xmlData, "<TotalNumberRecords>(%d-)</TotalNumberRecords>")
		InventoryHelper.setMaxPages(totalItems)

		togglePageButtons(tonumber(totalItems) > ITEMS_PER_PAGE)

		g_t_items = {}

		decodeInventoryItemsFromWeb(g_t_items, xmlData, false)

		m_loading = false
		
		setEquipped()
		populateItems()
		return KEP.EPR_CONSUMED
	end
end

function togglePageButtons(toggle)
	Control_SetVisible(gHandles["stcPage"], toggle)
	Control_SetVisible(gHandles["btnBack"], toggle)
	Control_SetVisible(gHandles["btnNext"], toggle)
end

function populateItems()
	m_pendingImages = InventoryHelper.setIcons({"bundle_global_id", "default_glid", "GLID"}, "SmartObj_ScriptIcon_82x82.tga")

	-- Finish loading any images in ContentServiceCompletionHandler
	InventoryHelper.setNameplates("name")
	InventoryHelper.setHightlights("equipped")

	setNoResultsControls(not m_loading and #g_t_items == 0)
end

function ContentServiceCompletionHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter ~= 2) then return end
	local ignored = KEP_EventDecodeNumber(event)
	local glid    = KEP_EventDecodeNumber(event)
	if (glid ~= nil) then
		local iconImage = m_pendingImages[glid]
		if (iconImage ~= nil) then
			local texturePath = KEP_AddItemThumbnailToDatabase(glid)
			Element_AddExternalTexture(iconImage, gDialogHandle, texturePath, "loading.dds")
			Element_SetCoords(iconImage, 0, 0, -1, -1)
			m_pendingImages[glid] = nil
		end
	end
end

function clearPage(resetPages)
	if resetPages then
		InventoryHelper.page = 1
		InventoryHelper.pageMax = 1
		InventoryHelper.updatePageDisplay()
	end
	InventoryHelper.clearClicked()
	g_t_items = {}
	populateItems()
end

function setNoResultsControls(showNoResults)
	local stcCtrl = gHandles["stcNoSearch"]
	
	Static_SetText(stcCtrl, "No Results Found.")
	
	Control_SetVisible(stcCtrl, showNoResults)
	if showNoResults then
		Dialog_MoveControlToFront(gDialogHandle, stcCtrl)
	else
		Dialog_MoveControlToBack(gDialogHandle, stcCtrl)
	end
end

----------------------------------------------------------------------
-- Subcategories
----------------------------------------------------------------------

function getWebCategory(subcat)
	local webCat = subcat
	local webOverride = SUB_CATEGORIES[InventoryHelper.category]["web"]
	if webOverride and webOverride[subcat] then
		webCat = webOverride[subcat]
	end
	return webCat
end

----------------------------------------------------------------------
-- Clicked/Use
----------------------------------------------------------------------
function useItem(itemIndex)
	local item = g_t_items[itemIndex]
	local useType = item["it_use_type"]

	local equipped = isItemEquipped(item.GLID)
	if isAccessPassItem(item) and g_iZoneHasAccessPass == 0 then
		displayMessage("World requires access pass to use that item.")
		return
	end
	if not m_npcPlayer then return end
	if item.GLID and item.GLID >= 0 then
		if not equipped then
			KEP_ArmInventoryItemOnPlayer(m_npcPlayer, item.GLID)
		else
			KEP_DisarmInventoryItemOnPlayer(m_npcPlayer, item.GLID)
		end
	end
	
	m_glidList = KEP_GetPlayerArmedItems(m_npcPlayer)

	requestInventory(false)
end

function itemClicked(itemIndex)
	local item = g_t_items[itemIndex]	
	--Set Animation Button
	--Set Use Button
	local useText = "Wear"

	resetCollar()
	if isItemEquipped(item.GLID) then
		useText = "Remove"
	end

	Static_SetText(gHandles["btnUse"], useText)
	if string.len(useText) > BUTTON_NAME_LIMIT then
		changeButtonFont(gHandles["btnUse"], SMALL_BUTTON_FONT)
	else
		changeButtonFont(gHandles["btnUse"], LARGE_BUTTON_FONT)
	end	
end

--Reset from gaming tab
function resetCollar()
	Control_SetVisible(gHandles["btnCopy"], false)
	if Control_GetHeight(gHandles["imgClicked"]) > m_clickedHeight then
		resizeCollarHeight(-1 * Control_GetHeight(gHandles["btnCopy"]))
	end
end

function resizeCollarHeight(amount)
	local clickedCollar = gHandles["imgClicked"]
	Control_SetSize(clickedCollar, Control_GetWidth(clickedCollar), Control_GetHeight(clickedCollar) + amount)

	-- Ignore the first control (btnUse)
	for i = 2, #CLICKED_CONTROLS do
		local handle = gHandles[CLICKED_CONTROLS[i]]
		Control_SetLocationY(handle, Control_GetLocationY(handle) + amount)
	end
end

function btnUse_OnButtonClicked( buttonHandle )
	useItem(InventoryHelper.selectedIndex)
end

function btnClose_OnButtonClicked( buttonHandle )
	MenuSetMinimized("CharacterCreation3.xml", false)
	local npcClothingEvent = KEP_EventCreate("FRAMEWORK_NPC_SAVE_CLOTHING")
	KEP_EventEncodeString(npcClothingEvent, Events.encode(m_glidList))-- Equipped Glids
	KEP_EventQueue(npcClothingEvent)
	MenuClose("UnifiedInventory.xml")
	MenuCloseThis()
end

function imgClicked_OnLButtonDblClk(btnHandle, x, y)
	local controlContainsPt = Control_ContainsPoint(gHandles[("btnItem" .. InventoryHelper.selectedIndex)], x, y)
	if InventoryHelper.selectedIndex and controlContainsPt then
		useItem(InventoryHelper.selectedIndex)
	end
end

----------------------------------------------------------------------
-- Misc. Buttons
----------------------------------------------------------------------

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	InventoryHelper.onButtonDown(x, y)
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
end

----------------------------------------------------------------------
-- Destroy Functions
----------------------------------------------------------------------
function Dialog_OnDestroy(dialogHandle)
	if m_npcPID then
		KEP_npcDeSpawn({netId = m_npcID})
		Events.sendEvent("FRAMEWORK_UPDATE_NPC_CLOTHING", {PID = m_npcPID, GLIDS = m_glidList})
		--Events.sendEvent("FRAMEWORK_HIDE_FOLLOWER", {hide = false, PID = m_npcPID})
	end
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	InventoryHelper.OnKeyDown(key)
end

-- Returns a player object from a netID
getPlayerObjectFromNetId = function(netId)
	if netId == nil then
		LogWarn("getPlayerObjectFromNetId() Received nil netId")
		return nil
	end
	local playerObj = KEP_GetPlayerByNetworkDefinedID(netId)

	if playerObj then
		if KEP_ObjectIsValid(playerObj) ~= 0 then
			return playerObj
		else
			LogWarn("getPlayerObjectFromNetId() KEP_ObjectIsValid("..tostring(playerObj)..") returned 0 (invalid)")
			return nil
		end
	else
		LogWarn("getPlayerObjectFromNetId() No playerObj returned from KEP_GetPlayerByNetworkDefinedID("..tostring(netId)..")")
		return nil
	end
end

function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end

function isItemEquipped(GLID )
	for i, equippedGlid in pairs(m_glidList) do
		if tostring(equippedGlid) == tostring(GLID) then
			return true
		end
	end
	return false
end

function displayMessage(message)
	local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
	KEP_EventEncodeNumber(statusEvent, 0)
	KEP_EventEncodeString(statusEvent, message)
	KEP_EventEncodeNumber(statusEvent, 3)
	KEP_EventEncodeNumber(statusEvent, 255)
	KEP_EventEncodeNumber(statusEvent, 0)
	KEP_EventEncodeNumber(statusEvent, 0)
	KEP_EventSetFilter(statusEvent, 4)
	KEP_EventQueue(statusEvent)
	return 
end

function isAccessPassItem(item)
	if not ( next(item["pass_list"]) ) then
		return false
	else
		return true
	end
end

function setEquipped(  )
	for index, item in pairs(g_t_items) do
		if isItemEquipped(item["GLID"]) then
			item["equipped"] = true
		else
			item["equipped"] = false
		end
	end
end