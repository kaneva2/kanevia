--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile( "..\\Scripts\\KEP.lua")

---------------------------------------------------------------------
--  Wraps calls to UGCZoneLite
--
-- Intended Public functions start with UGCZL
--
---------------------------------------------------------------------

-- return structures from

-- array of "zone" populated by a call to UGCZLP_RequestZoneList
-- where "zone"
-- zone.zoneIndex        // number
-- zone.zoneName         // string
-- zone.exgName          // string
-- zone.isDefault        // boolean
-- zone.isKanevaSupplied // boolean
-- zone.lastOpResult     // UGCZLPLastOperation.Result
-- zone.lastOpMsg        // UGCZLPLastOperation.Msg

-- array of "spawnPoint" populated by by a call to UGCZLP_RequestSpawnList
-- where "spawnPoint"
-- spawnPoint.id           // number
-- spawnPoint.name         // string
-- spawnPoint.x            // number
-- spawnPoint.y            // number
-- spawnPoint.z            // number
-- spawnPoint.r            // number
-- spawnPoint.lastOpResult // UGCZLPLastOperation.Result
-- spawnPoint.lastOpMsg    // UGCZLPLastOperation.Msg



-- Some declarations
-- Internal note (needs to be in sync with CSZoneLiteProxy.lua)
UGCZLPLastOperation = {}
UGCZLPLastOperation.Result = {}
UGCZLPLastOperation.Result.NONE      = 0
UGCZLPLastOperation.Result.SUCCESS   = 1
UGCZLPLastOperation.Result.PENDING   = 2
UGCZLPLastOperation.Result.FAILED    = 3
UGCZLPLastOperation.Result.IMPORTING = 4
UGCZLPLastOperation.Msg = {}
UGCZLPLastOperation.Msg.NONE       = ""
UGCZLPLastOperation.Msg.CREATE     = "Create"
UGCZLPLastOperation.Msg.DELETE     = "Delete"
UGCZLPLastOperation.Msg.RENAME     = "Rename"
UGCZLPLastOperation.Msg.IMPORT     = "Import"
UGCZLPLastOperation.Msg.SETDEFAULT = "SetDefault"

-- Some declarations
UGCZLPZoneTypes = {}
UGCZLPZoneTypes.CURRENT_TYPE   = 0
UGCZLPZoneTypes.HOUSING_ZONE   = 3
UGCZLPZoneTypes.PERMANENT_ZONE = 4
UGCZLPZoneTypes.ARENA_ZONE     = 5
UGCZLPZoneTypes.CHANNEL_ZONE   = 6

UGCZLPSpawnLoc = {}
UGCZLPSpawnLoc.ZONE_DEFAULT  = 0
UGCZLPSpawnLoc.USER_POSITION = 1

UGCZLPDynamicObjects= {}
UGCZLPDynamicObjects.DELETE_ALL          = 0
UGCZLPDynamicObjects.DELETE_NON_SCRIPTED = 1

UGCZLPImportTypes = {}
UGCZLPImportTypes.ZONE      = 0
UGCZLPImportTypes.COMMUNITY = 1


-- Events/Handler Names
PROXYEvents = {}
PROXYEvents.Request = {}
PROXYEvents.Request.ZoneList        = "CSZLP_ZoneListRequestEvent"
PROXYEvents.Request.SpawnList       = "CSZLP_SpawnListRequestEvent"
PROXYEvents.Request.CreateZone      = "CSZLP_CreateZoneRequestEvent"
PROXYEvents.Request.DeleteZone      = "CSZLP_DeleteZoneRequestEvent"
PROXYEvents.Request.RenameZone      = "CSZLP_RenameZoneRequestEvent"
PROXYEvents.Request.SetDefaultZone  = "CSZLP_SetDefaultZoneRequestEvent"
PROXYEvents.Request.AddSpawn        = "CSZLP_AddSpawnRequestEvent"
PROXYEvents.Request.DeleteSpawn     = "CSZLP_DeleteSpawnRequestEvent"
PROXYEvents.Request.SetInitialArena = "CSZLP_SetInitialArenaRequestEvent"
PROXYEvents.Request.UpdateSpawn     = "CSZLP_UpdateSpawnRequestEvent"
PROXYEvents.Request.ImportZone      = "CSZLP_ImportZoneRequestEvent"
PROXYEvents.Request.ImportChannel   = "CSZLP_ImportChannelRequestEvent"
PROXYEvents.Reply = {}
PROXYEvents.Reply.ZoneList        = "CSZLP_ZoneListReplyEvent"
PROXYEvents.Reply.SpawnList       = "CSZLP_SpawnListReplyEvent"
PROXYEvents.Reply.CreateZone      = "CSZLP_CreateZoneReplyEvent"
PROXYEvents.Reply.DeleteZone      = "CSZLP_DeleteZoneReplyEvent"
PROXYEvents.Reply.RenameZone      = "CSZLP_RenameZoneReplyEvent"
PROXYEvents.Reply.SetDefaultZone  = "CSZLP_SetDefaultZoneReplyEvent"
PROXYEvents.Reply.AddSpawn        = "CSZLP_AddSpawnReplyEvent"
PROXYEvents.Reply.DeleteSpawn     = "CSZLP_DeleteSpawnReplyEvent"
PROXYEvents.Reply.SetInitialArena = "CSZLP_SetInitialArenaReplyEvent"
PROXYEvents.Reply.UpdateSpawn     = "CSZLP_UpdateSpawnReplyEvent"
PROXYEvents.Reply.ImportZone      = "CSZLP_ImportZoneReplyEvent"
PROXYEvents.Reply.ImportChannel   = "CSZLP_ImportChannelReplyEvent"
PROXYEvents.Reply.ZoneStateChange = "CSZLP_ZoneStateChangeReplyEvent"
PROXYEvents.Handlers = {}
PROXYEvents.Handlers.ZoneList        = "UGCLP_ZoneListReplyHandler"
PROXYEvents.Handlers.SpawnList       = "UGCLP_SpawnListReplyHandler"
PROXYEvents.Handlers.CreateZone      = "UGCLP_CreateZoneReplyHandler"
PROXYEvents.Handlers.DeleteZone      = "UGCLP_DeleteZoneReplyHandler"
PROXYEvents.Handlers.RenameZone      = "UGCLP_RenameZoneReplyHandler"
PROXYEvents.Handlers.SetDefaultZone  = "UGCLP_SetDefaultZoneReplyHandler"
PROXYEvents.Handlers.AddSpawn        = "UGCLP_AddSpawnReplyHandler"
PROXYEvents.Handlers.DeleteSpawn     = "UGCLP_DeleteSpawnReplyHandler"
PROXYEvents.Handlers.SetInitialArena = "UGCLP_SetInitialArenaReplyHandler"
PROXYEvents.Handlers.UpdateSpawn     = "UGCLP_UpdateSpawnReplyHandler"
PROXYEvents.Handlers.ImportZone      = "UGCLP_ImportZoneReplyHandler"
PROXYEvents.Handlers.ImportChannel   = "UGCLP_ImportChannelReplyHandler"
PROXYEvents.Handlers.ZoneStateChange = "UGCLP_ZoneStateChangeReplyHandler"

function UGCZLP_ReplyZoneList(blnRetval, zoneList)
--	LogWarn("Warning Unhandled UGCZLP_ReplyZoneList(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."]")
end

function UGCZLP_ReplySpawnList(blnRetval, zoneIndex, spawnPointList)
--	LogWarn("Warning Unhandled UGCZLP_ReplySpawnList(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] zoneIndex["..tostring(zoneIndex).."]")
end

function UGCZLP_ReplyCreateZoneEvent(blnRetval, zoneIndex, newZoneName, strMsg, lastOpResult, lastOpMsg)
--	LogWarn("Warning Unhandled UGCZLP_ReplyCreateZoneEvent(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] zoneIndex["..tostring(zoneIndex).."] strZoneName["..tostring(newZoneName).."] strMsg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
end

function UGCZLP_ReplyDeleteZoneEvent(blnRetval, zoneIndex, strMsg, lastOpResult, lastOpMsg)
--	LogWarn("Warning Unhandled UGCZLP_ReplyDeleteZoneEvent(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] zoneIndex["..tostring(zoneIndex).."] strMsg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]"")
end

function UGCZLP_ReplyRenameZoneEvent(blnRetval, zoneIndex, newZoneName, strMsg, lastOpResult, lastOpMsg)
--	LogWarn("Warning Unhandled UGCZLP_ReplyRenameZoneEvent(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] zoneIndex["..tostring(zoneIndex).."] newZoneName["..tostring(newZoneName).."] strMsg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
end

function UGCZLP_ReplySetDefaultZoneEvent(blnRetval, zoneIndex, strMsg, lastOpResult, lastOpMsg)
--	LogWarn("Warning Unhandled UGCZLP_ReplySetDefaultZoneEvent(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] zoneIndex["..tostring(zoneIndex).."] strMsg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
end

function UGCZLP_ReplySetInitialArenaEvent(blnRetval, strMsg)
--	LogWarn("Warning Unhandled UGCZLP_ReplySetInitialArenaEvent(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] strMsg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
end

function UGCZLP_ReplyImportZoneEvent(blnRetval, zoneIndex, newZoneIndex, strMsg, lastOpResult, lastOpMsg)
--	LogWarn("Warning Unhandled UGCZLP_ReplyImportZoneEvent(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] zoneIndex["..tostring(zoneIndex).."] newZoneIndex["..tostring(newZoneIndex).."] strMsg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
end

function UGCZLP_ReplyImportChannelEvent(blnRetval, zoneIndex, strMsg, lastOpResult, lastOpMsg, newZoneIndex)
--	LogWarn("Warning Unhandled UGCZLP_ReplyImportZoneEvent(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] zoneIndex["..tostring(zoneIndex).."] strMsg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."] newZoneIndex["..tostring(newZoneIndex).."]")
end

function UGCZLP_ReplyZoneStateChangeEvent(zoneIndex, lastOpResult, lastOpMsg)
--	LogWarn("Warning Unhandled UGCZLP_ReplyZoneStateChangeEvent(should be overwritten by caller) -- params: zoneIndex["..tostring(zoneIndex).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
end

function UGCZLP_ReplyAddSpawnEvent(blnRetval, zoneIndex, spawnId, spawnName, strMsg, lastOpResult, lastOpMsg)
--	LogWarn("Warning Unhandled UGCZLP_ReplyAddSpawnEvent(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] zoneIndex["..tostring(zoneIndex).."] spawnId["..tostring(spawnId).."] spawnName["..tostring(spawnName).."] strMsg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
end

function UGCZLP_ReplyDeleteSpawnEvent(blnRetval, zoneIndex, spawnId, strMsg, lastOpResult, lastOpMsg)
--	LogWarn("Warning Unhandled UGCZLP_ReplyDeleteSpawnEvent(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] oneIndex["..tostring(zoneIndex).."] spawnId["..tostring(spawnId).."] strMsg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
end

function UGCZLP_ReplyUpdateSpawnEvent(blnRetval, zoneIndex, spawnId, strMsg, lastOpResult, lastOpMsg)
--	LogWarn("Warning Unhandled UGCZLP_ReplyUpdateSpawnEvent(should be overwritten by caller) -- params: blnRetval["..tostring(blnRetval).."] zoneIndex["..tostring(zoneIndex).."] spawnId["..tostring(spawnId).."] strMsg["..tostring(strMsg).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
end

---------------------------------------------------------------------
--  working variables
local isInitializedEventHandlers = false
local isInitializedEvents        = false
---------------------------------------------------------------------

function UGCZoneLiteProxy_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.ZoneList,        PROXYEvents.Reply.ZoneList,        KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.SpawnList,       PROXYEvents.Reply.SpawnList,       KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.CreateZone,      PROXYEvents.Reply.CreateZone,      KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.DeleteZone,      PROXYEvents.Reply.DeleteZone,      KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.RenameZone,      PROXYEvents.Reply.RenameZone,      KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.SetDefaultZone,  PROXYEvents.Reply.SetDefaultZone,  KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.AddSpawn,        PROXYEvents.Reply.AddSpawn,        KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.DeleteSpawn,     PROXYEvents.Reply.DeleteSpawn,     KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.SetInitialArena, PROXYEvents.Reply.SetInitialArena, KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.UpdateSpawn,     PROXYEvents.Reply.UpdateSpawn,     KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.ImportZone,      PROXYEvents.Reply.ImportZone,      KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.ImportChannel,   PROXYEvents.Reply.ImportChannel,   KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.ZoneStateChange, PROXYEvents.Reply.ZoneStateChange, KEP.MED_PRIO)
	isInitializedEventHandlers = true
end

function UGCZoneLiteProxy_InitializeKEPEvents(dispatcher, handler, debugLevel)
	isInitializedEvents = true
end

function UGCZLP_RequestZoneList()
	local eventName = PROXYEvents.Request.ZoneList

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestZoneList - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	KEP_EventCreateAndQueue(eventName)
end

function UGCZLP_RequestSpawnList(zoneIndex)
	local eventName = PROXYEvents.Request.SpawnList

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestSpawnList - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(eventName)
	KEP_EventSetObjectId(event, zoneIndex)
	KEP_EventQueue(event)
end

function UGCZLP_RequestCreateZoneEvent(ugczlpImportType, baseZoneIndex, newZoneName, ugczlpZoneType, ugczlpSpawnLoc, occupancyLimit, copyZoneIndex, copyFlag, ugczlpImportZoneType, communityIndex, communityInstanceId)
	local eventName = PROXYEvents.Request.CreateZone

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestCreateZoneEvent - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(PROXYEvents.Request.CreateZone)
	KEP_EventSetObjectId(event, baseZoneIndex)    -- zoneIndex
	KEP_EventEncodeNumber(event, ugczlpImportType)
	KEP_EventEncodeString(event, newZoneName)          -- new name alphanum+space, must be unique
	KEP_EventEncodeNumber(event, ugczlpZoneType)       -- 4 (perm or arena) or 5 (arena only)
	KEP_EventEncodeNumber(event, ugczlpSpawnLoc)       -- 0 (use zone default) 1 (use player location)
	KEP_EventEncodeNumber(event, occupancyLimit)       -- occupancy limit, 0 to copy from baseZone
	KEP_EventEncodeNumber(event, copyZoneIndex)        -- zone to copy from (0 if no copy needed)
	KEP_EventEncodeNumber(event, copyFlag)             -- (1 All -- 2 Scripted -- 3 NonScripted)
	KEP_EventEncodeNumber(event, ugczlpImportZoneType) -- (4 zone -- 5 arena)
	KEP_EventEncodeNumber(event, communityIndex)
	KEP_EventEncodeNumber(event, communityInstanceId)
	KEP_EventQueue(event)
end

function UGCZLP_RequestDeleteZoneEvent(zoneIndex, ugczlpDynamicObjects)
	local eventName = PROXYEvents.Request.DeleteZone

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestDeleteZoneEvent - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(eventName)
	KEP_EventSetObjectId(event, zoneIndex)
	KEP_EventEncodeNumber(event, ugczlpDynamicObjects) -- (0 delete all dynamic objects) (1 delete only dynamic objects with NO scripts)
	KEP_EventQueue(event)
end

function UGCZLP_RequestRenameZoneEvent(zoneIndex, newZoneName)
	local eventName = PROXYEvents.Request.RenameZone

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestRenameZoneEvent - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(eventName)
	KEP_EventSetObjectId(event, zoneIndex)
	KEP_EventEncodeString(event, newZoneName)
	KEP_EventQueue(event)
end

function UGCZLP_RequestSetDefaultZoneEvent(zoneIndex)
	local eventName = PROXYEvents.Request.SetDefaultZone

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestSetDefaultZoneEvent - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(PROXYEvents.Request.SetDefaultZone)
	KEP_EventSetObjectId(event, zoneIndex)
	KEP_EventQueue(event)
end

function UGCZLP_RequestSetInitialArenaEvent(zoneIndex)
	local eventName = PROXYEvents.Request.SetInitialArena

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestSetInitialArenaEvent - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(eventName)
	KEP_EventSetObjectId(event, zoneIndex)
	KEP_EventQueue(event)
end

function UGCZLP_RequestImportZoneEvent(ugczlpImportType, zoneIndex, baseZoneIndex, ugczlpDynamicObjects, copyZoneIndex, copyFlag, ugczlpImportZoneType, communityIndex, communityInstanceId)
	local eventName = PROXYEvents.Request.ImportZone

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestImportZone - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(eventName)
	KEP_EventSetObjectId(event, zoneIndex)
	KEP_EventEncodeNumber(event, ugczlpImportType)
	KEP_EventEncodeNumber(event, baseZoneIndex)
	KEP_EventEncodeNumber(event, ugczlpDynamicObjects)
	KEP_EventEncodeNumber(event, copyZoneIndex)
	KEP_EventEncodeNumber(event, copyFlag)
	KEP_EventEncodeNumber(event, ugczlpImportZoneType)
	KEP_EventEncodeNumber(event, communityIndex)
	KEP_EventEncodeNumber(event, communityInstanceId)
	KEP_EventQueue(event)
end

function UGCZLP_RequestImportChannelEvent(dstZoneDef, srcZoneDef, baseZoneIndexPlain, ugczlpDynamicObjects)
	local eventName = PROXYEvents.Request.ImportChannel

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestImportChannel - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(eventName)
	KEP_EventSetObjectId(event,  dstZoneDef.zoneIndex)
	KEP_EventEncodeNumber(event, dstZoneDef.instanceId)
	KEP_EventEncodeNumber(event, baseZoneIndexPlain)
	KEP_EventEncodeNumber(event, srcZoneDef.zoneIndex)
	KEP_EventEncodeNumber(event, srcZoneDef.instanceId)
	KEP_EventEncodeNumber(event, ugczlpDynamicObjects)
	KEP_EventQueue(event)
end

function UGCZLP_RequestAddSpawnEvent(zoneIndex, name, blnUsePlayerPos, x, y, z, r)
	local eventName = PROXYEvents.Request.AddSpawn

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestAddSpawnEvent - ERROR invalid usage -> not initialized")
		return
	end

	-- convert parameter values
	local usePlayerPos = 0 -- false
	if (blnUsePlayerPos ~= nil and blnUsePlayerPos == true) then
		usePlayerPos = 1   -- true
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(eventName)
	KEP_EventSetObjectId(event, zoneIndex)
	KEP_EventEncodeString(event, name)
	KEP_EventEncodeNumber(event, usePlayerPos)
	KEP_EventEncodeNumber(event, round(x, 2))
	KEP_EventEncodeNumber(event, round(y, 2))
	KEP_EventEncodeNumber(event, round(z, 2))
	KEP_EventEncodeNumber(event, round(r, 2))
	KEP_EventQueue(event)
end

function UGCZLP_RequestDeleteSpawnEvent(spawnPtId, zoneIndex)
	local eventName = PROXYEvents.Request.DeleteSpawn

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestDeleteSpawnEvent - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(eventName)
	KEP_EventSetObjectId(event, zoneIndex)
	KEP_EventEncodeNumber(event, spawnPtId)
	KEP_EventQueue(event)
end

function UGCZLP_RequestUpdateSpawnEvent(spawnPointId, zoneIndex, zoneName, x, y, z, r)
	local eventName = PROXYEvents.Request.UpdateSpawn

	-- validate state
	if (isInitializedEventHandlers ~= true or isInitializedEvents ~= true) then
		LogError("UGCZLP_RequestUpdateSpawnEvent - ERROR invalid usage -> not initialized")
		return
	end

	-- craft event for clientScriptProxy
	local event = KEP_EventCreate(eventName)
	KEP_EventSetObjectId(event, spawnPointId)
	KEP_EventEncodeNumber(event, zoneIndex)
	KEP_EventEncodeString(event, zoneName)
	KEP_EventEncodeNumber(event, round(x, 2))
	KEP_EventEncodeNumber(event, round(y, 2))
	KEP_EventEncodeNumber(event, round(z, 2))
	KEP_EventEncodeNumber(event, round(r, 2))
	KEP_EventQueue(event)
end

function UGCLP_ZoneListReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack Event
	local blnRetval, zoneList = Proxy_UnpackZoneList(event)
	if (blnRetval == false) then
		LogError("UGCLP_ZoneListReplyHandler response returned[false]")
	end

	-- Update listeners
	UGCZLP_ReplyZoneList(blnRetval, zoneList)
end

function UGCLP_SpawnListReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack Event
	local blnRetval, spawnPointList = Proxy_UnpackSpawnList(event)
	if (blnRetval == false) then
		LogError("UGCLP_SpawnListReplyHandler response returned[false]")
	end

	-- Update listeners
	UGCZLP_ReplySpawnList(blnRetval, objectid, spawnPointList)
end

function UGCLP_CreateZoneReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack clientScriptProxy event
	local eventData = {}
	eventData.id = objectid
	eventData.retval       = KEP_EventDecodeNumber(event)
	eventData.zoneName     = KEP_EventDecodeString(event)
	eventData.msg          = KEP_EventDecodeString(event)
	eventData.lastOpResult = KEP_EventDecodeNumber(event)
	eventData.lastOpMsg    = KEP_EventDecodeString(event)
	if (eventData.retval == 0) then
		LogError("UGCLP_CreateZoneReplyHandler response returned[false] id["..tostring(eventData.id).."] zoneName["..tostring(eventData.zoneName).."] lastOpResult["..tostring(eventData.lastOpResult).."] lastOpMsg["..tostring(eventData.lastOpMsg).."] msg["..tostring(eventData.msg).."]")
	end

	-- Update listeners
	UGCZLP_ReplyCreateZoneEvent(Proxy_RetvalToBool(eventData.retval), eventData.id, eventData.zoneName, eventData.msg, eventData.lastOpResult, eventData.lastOpMsg)
end

function UGCLP_DeleteZoneReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack clientScriptProxy event
	local eventData = {}
	eventData.id = objectid
	eventData.retval       = KEP_EventDecodeNumber(event)
	eventData.msg          = KEP_EventDecodeString(event)
	eventData.lastOpResult = KEP_EventDecodeNumber(event)
	eventData.lastOpMsg    = KEP_EventDecodeString(event)
	if (eventData.retval == 0) then
		LogError("UGCLP_DeleteZoneReplyHandler response returned[false] id["..eventData.id.."] lastOpResult["..tostring(eventData.lastOpResult).."] lastOpMsg["..tostring(eventData.lastOpMsg).."] msg["..tostring(eventData.msg).."]")
	end

	-- Update listeners
	UGCZLP_ReplyDeleteZoneEvent(Proxy_RetvalToBool(eventData.retval), eventData.id, eventData.msg, eventData.lastOpResult, eventData.lastOpMsg)
end

function UGCLP_RenameZoneReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack clientScriptProxy event
	local eventData = {}
	eventData.id = objectid
	eventData.retval       = KEP_EventDecodeNumber(event)
	eventData.newZoneName  = KEP_EventDecodeString(event)
	eventData.msg          = KEP_EventDecodeString(event)
	eventData.lastOpResult = KEP_EventDecodeNumber(event)
	eventData.lastOpMsg    = KEP_EventDecodeString(event)
	if (eventData.retval == 0) then
		LogError("UGCLP_RenameZoneReplyHandler response returned[false] id["..eventData.id.."] lastOpResult["..tostring(eventData.lastOpResult).."] lastOpMsg["..tostring(eventData.lastOpMsg).."] msg["..tostring(eventData.msg).."]")
	end

	-- Update listeners
	UGCZLP_ReplyRenameZoneEvent(Proxy_RetvalToBool(eventData.retval), eventData.id, eventData.newZoneName, eventData.msg, eventData.lastOpResult, eventData.lastOpMsg)
end

function UGCLP_SetDefaultZoneReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack clientScriptProxy event
	local eventData = {}
	eventData.id = objectid
	eventData.retval       = KEP_EventDecodeNumber(event)
	eventData.msg          = KEP_EventDecodeString(event)
	eventData.lastOpResult = KEP_EventDecodeNumber(event)
	eventData.lastOpMsg    = KEP_EventDecodeString(event)
	if (eventData.retval == 0) then
		LogError("UGCLP_SetDefaultZoneReplyHandler response returned[false] id["..eventData.id.."] lastOpResult["..tostring(eventData.lastOpResult).."] lastOpMsg["..tostring(eventData.lastOpMsg).."] msg["..tostring(eventData.msg).."]")
	end

	-- Update listeners
	UGCZLP_ReplySetDefaultZoneEvent(Proxy_RetvalToBool(eventData.retval), eventData.id, eventData.msg, eventData.lastOpResult, eventData.lastOpMsg)
end

function UGCLP_AddSpawnReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack clientScriptProxy event
	local eventData = {}
	eventData.id = objectid
	eventData.retval       = KEP_EventDecodeNumber(event)
	eventData.spawnId      = KEP_EventDecodeNumber(event)
	eventData.spawnName    = KEP_EventDecodeString(event)
	eventData.msg          = KEP_EventDecodeString(event)
	eventData.lastOpResult = KEP_EventDecodeNumber(event)
	eventData.lastOpMsg    = KEP_EventDecodeString(event)
	if (eventData.retval == 0) then
		LogError("UGCLP_AddSpawnReplyHandler response returned[false] id["..eventData.id.."] lastOpResult["..tostring(eventData.lastOpResult).."] lastOpMsg["..tostring(eventData.lastOpMsg).."] msg["..tostring(eventData.msg).."]")
	end

	-- Update listeners
	UGCZLP_ReplyAddSpawnEvent(Proxy_RetvalToBool(eventData.retval), eventData.id, eventData.spawnId, eventData.spawnName, eventData.msg, eventData.lastOpResult, eventData.lastOpMsg)
end

function UGCLP_DeleteSpawnReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack clientScriptProxy event
	local eventData = {}
	eventData.id = objectid
	eventData.retval       = KEP_EventDecodeNumber(event)
	eventData.spawnId      = KEP_EventDecodeNumber(event)
	eventData.msg          = KEP_EventDecodeString(event)
	eventData.lastOpResult = KEP_EventDecodeNumber(event)
	eventData.lastOpMsg    = KEP_EventDecodeString(event)
	if (eventData.retval == 0) then
		LogError("UGCLP_DeleteSpawnReplyHandler response returned[false] id["..eventData.id.."] lastOpResult["..tostring(eventData.lastOpResult).."] lastOpMsg["..tostring(eventData.lastOpMsg).."] msg["..tostring(eventData.msg).."]")
	end

	-- Update listeners
	UGCZLP_ReplyDeleteSpawnEvent(Proxy_RetvalToBool(eventData.retval), eventData.id, eventData.spawnId, eventData.msg, eventData.lastOpResult, eventData.lastOpMsg)
end

function UGCLP_SetInitialArenaReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

function UGCLP_UpdateSpawnReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack clientScriptProxy event
	local eventData = {}
	eventData.id = objectid
	eventData.retval       = KEP_EventDecodeNumber(event)
	eventData.spawnId      = KEP_EventDecodeNumber(event)
	eventData.msg          = KEP_EventDecodeString(event)
	eventData.lastOpResult = KEP_EventDecodeNumber(event)
	eventData.lastOpMsg    = KEP_EventDecodeString(event)
	if (eventData.retval == 0) then
		LogError("UGCLP_UpdateSpawnReplyHandler response returned[false] id["..eventData.id.."] lastOpResult["..tostring(eventData.lastOpResult).."] lastOpMsg["..tostring(eventData.lastOpMsg).."] msg["..tostring(eventData.msg).."]")
	end

	-- Update listeners
	UGCZLP_ReplyUpdateSpawnEvent(Proxy_RetvalToBool(eventData.retval), eventData.id, eventData.spawnId, eventData.msg, eventData.lastOpResult, eventData.lastOpMsg)
end

function UGCLP_ImportZoneReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack clientScriptProxy event
	local eventData = {}
	eventData.id = objectid
	eventData.retval       = KEP_EventDecodeNumber(event)
	eventData.newZoneIndex = KEP_EventDecodeNumber(event)
	eventData.msg          = KEP_EventDecodeString(event)
	eventData.lastOpResult = KEP_EventDecodeNumber(event)
	eventData.lastOpMsg    = KEP_EventDecodeString(event)
	if (eventData.retval == 0) then
		LogError("UGCLP_ImportZoneReplyHandler request returned[false] objectid["..eventData.id .."] msg["..tostring(evtMsg).."]")
	end

	-- Update listeners
	UGCZLP_ReplyImportZoneEvent(Proxy_RetvalToBool(eventData.retval), eventData.id , eventData.newZoneIndex, eventData.msg, eventData.lastOpResult, eventData.lastOpMsg)
end

function UGCLP_ImportChannelReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack clientScriptProxy event
	local eventData = {}
	eventData.id = objectid
	eventData.retval       = KEP_EventDecodeNumber(event)
	eventData.newZoneIndex = KEP_EventDecodeNumber(event)
	eventData.msg          = KEP_EventDecodeString(event)
	eventData.lastOpResult = KEP_EventDecodeNumber(event)
	eventData.lastOpMsg    = KEP_EventDecodeString(event)
	if (eventData.retval == 0) then
		LogError("UGCLP_ImportChannelReplyHandler request returned[false] objectid["..eventData.id .."] msg["..tostring(eventData.msg).."]")
	end

	-- Update listeners
	UGCZLP_ReplyImportChannelEvent(Proxy_RetvalToBool(eventData.retval), eventData.id , eventData.msg, eventData.lastOpResult, eventData.lastOpMsg, eventData.newZoneIndex)
end

function UGCLP_ZoneStateChangeReplyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack clientScriptProxy event
	local eventData = {}
	eventData.id = objectid
	eventData.lastOpResult = KEP_EventDecodeNumber(event)
	eventData.lastOpMsg    = KEP_EventDecodeString(event)

	-- Update listeners
	UGCZLP_ReplyZoneStateChangeEvent(eventData.id, eventData.lastOpResult, eventData.lastOpMsg)
end

function UGCZLPLastOpResultToString(lastOpResult)
	local retval = ""
	if (lastOpResult == UGCZLPLastOperation.Result.NONE) then
		retval = ""
	elseif (lastOpResult == UGCZLPLastOperation.Result.SUCCESS) then
		retval= "Success"
	elseif (lastOpResult == UGCZLPLastOperation.Result.PENDING) then
		retval= "Pending"
	elseif (lastOpResult == UGCZLPLastOperation.Result.FAILED) then
		retval= "Failed"
	elseif (lastOpResult == UGCZLPLastOperation.Result.IMPORTING) then
		retval= "Importing"
	else
		retval= "Unknown OpResult"
	end
	return retval
end

function Proxy_UnpackZoneList(event)
	local zoneList = {}
	if (event == nil) then
		return false
	end

	local retval    = KEP_EventDecodeNumber(event)
	local listCount = KEP_EventDecodeNumber(event)

	for i=1,listCount do
		local newEntry = {}
		newEntry.zoneIndex        = KEP_EventDecodeNumber(event)
		newEntry.zoneName         = KEP_EventDecodeString(event)
		newEntry.exgName          = KEP_EventDecodeString(event)
		newEntry.isDefault        = KEP_EventDecodeNumber(event)
		newEntry.isKanevaSupplied = KEP_EventDecodeNumber(event)
		newEntry.lastOpResult     = KEP_EventDecodeNumber(event)
		newEntry.lastOpMsg        = KEP_EventDecodeString(event)

		-- convert numerics to boolean
		if (newEntry.isDefault == 1) then
			newEntry.isDefault = true
		else
			newEntry.isDefault = false
		end

		if (newEntry.isKanevaSupplied == 1) then
			newEntry.isKanevaSupplied = true
		else
			newEntry.isKanevaSupplied = false
		end
		table.insert(zoneList, newEntry)
	end
	return Proxy_RetvalToBool(retval), zoneList
end

function Proxy_UnpackSpawnList(event, zoneIndex)
	local spawnPointList = {}
	if (event == nil) then
		return false
	end
	local retval    = KEP_EventDecodeNumber(event)
	local listCount = KEP_EventDecodeNumber(event)
	for i=1,listCount do
		local newEntry = {}
		newEntry.id           = KEP_EventDecodeNumber(event)
		newEntry.name         = KEP_EventDecodeString(event)
		newEntry.x            = KEP_EventDecodeNumber(event)
		newEntry.y            = KEP_EventDecodeNumber(event)
		newEntry.z            = KEP_EventDecodeNumber(event)
		newEntry.r            = KEP_EventDecodeNumber(event)
		newEntry.lastOpResult = KEP_EventDecodeNumber(event)
		newEntry.lastOpMsg    = KEP_EventDecodeString(event)
		table.insert(spawnPointList, newEntry)
	end
	return Proxy_RetvalToBool(retval), spawnPointList
end

function Proxy_RetvalToBool(retval)
	if retval == 1 then
		return true
	else
		return false
	end
end

-- recursivly dump table
function Proxy_TraceDump(aVar, indent)
end
