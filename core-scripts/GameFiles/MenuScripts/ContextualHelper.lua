--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

------------------------------------------------------------
-- Contextual Help(er)
-- Creates all the events needed to display and update the 
-- contextual help bubbles. 
--
-- Edie Woelfle @ Kaneva
-- 6/20/2016
------------------------------------------------------------

ContextualHelper = {} -- holds all the variables and functions

-- sends the event to show a new contextual help
function ContextualHelper.showHelp(helpType, menuLocation, x, y, width, height, arrow, closeTime, bubbleWidth)
	local contextEvent = KEP_EventCreate("showContextualHelpEvent")
	local eventParams = {}
	eventParams.type = helpType
	eventParams.x = x + menuLocation.x
	eventParams.y = y + menuLocation.y
	eventParams.width = width
	eventParams.height = height
	eventParams.arrow = arrow
	if closeTime then eventParams.closeTime = closeTime end 
	if bubbleWidth then eventParams.bubbleWidth = bubbleWidth end
	KEP_EventEncodeString(contextEvent, Events.encode(eventParams))
	KEP_EventQueue(contextEvent)
end 

-- sends the event to update the position of the contextual help (currently open and menu changed location)
function ContextualHelper.updateHelp(helpType, menuLocation, x, y, width, height, arrow, bubbleWidth)
	local contextEvent = KEP_EventCreate("updateContextualHelpEvent")
	local eventParams = {}
	eventParams.type = helpType
	eventParams.x = x + menuLocation.x
	eventParams.y = y + menuLocation.y
	eventParams.width = width
	eventParams.height = height
	eventParams.arrow = arrow
	if bubbleWidth then eventParams.bubbleWidth = bubbleWidth end 
	KEP_EventEncodeString(contextEvent, Events.encode(eventParams))
	KEP_EventQueue(contextEvent)
end

-- sends the event to hide a contextual help (close, but not complete)
function ContextualHelper.hideHelp(helpType)
	local contextEvent = KEP_EventCreate("hideContextualHelpEvent")
	local eventParams = {}
	eventParams.type = helpType
	KEP_EventEncodeString(contextEvent, Events.encode(eventParams))
	KEP_EventQueue(contextEvent)
end

-- sends the event to complete a contextual help (seen and complete)
function ContextualHelper.completeHelp(helpType)
	local contextEvent = KEP_EventCreate("contextualHelpCompleteEvent")
	local eventParams = {}
	eventParams.type = helpType
	KEP_EventEncodeString(contextEvent, Events.encode(eventParams))
	KEP_EventQueue(contextEvent)
end

-- sends the event to force complete the contextual help (complete, even if not seen)
function ContextualHelper.forceCompleteHelp(helpType)
	local contextEvent = KEP_EventCreate("forceContextualHelpCompleteEvent")
	local eventParams = {}
	eventParams.type = helpType
	KEP_EventEncodeString(contextEvent, Events.encode(eventParams))
	KEP_EventQueue(contextEvent)
end