--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\ScriptData\\Zones.lua")
dofile("..\\ScriptData\\ZoneFiles.lua")

g_download_begun = false
g_t_file_list = {}
g_cur_file_num = 0
g_total_download_size = 0
g_zone_index = nil
g_zone_instance_id = nil
g_login_username = nil
g_login_password = nil
g_lastZone = -1

g_debug = false
g_runLocal = false

g_lstItemsHandle = nil

DL_STATUS = {}
DL_STATUS.WAITING = 0
DL_STATUS.DOWNLOADING = 1
DL_STATUS.COMPLETED = 2

g_startTime = nil

function ProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter == PROGRESS_CANCEL) then
		MenuCloseThis()
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "ZoneDownloadHandler", "ZoneDownloadEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "pendingSpawnHandler", KEP.PENDINGSPAWN_EVENT_NAME, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "loginDownloadHandler",  "DownloadForLoginEvent", KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "ProgressEventHandler", "ProgressEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "DownloadFinishedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "DownloadForLoginEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ProgressEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	ProgressOptCancel("ZoneDownloader", PROGRESS_OPT_CANCEL_YES)
	ProgressOpen("ZoneDownloader")
	checkForStarKep()
end

g_PendingSpawnEvent = nil

function pendingSpawnHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- Prepare another pending spawn event to be resent as a confirmation back to server once all downloadings are completed - YC Aug 2010
	g_PendingSpawnEvent = KEP_EventCreate( KEP.PENDINGSPAWN_EVENT_NAME )
	KEP_EventSetFilter( g_PendingSpawnEvent, filter )
	KEP_EventSetObjectId( g_PendingSpawnEvent, objectid )
	KEP_EventEncodeNumber( g_PendingSpawnEvent, KEP_EventDecodeNumber( event ) ) -- initialSpawn
	KEP_EventEncodeNumber( g_PendingSpawnEvent, KEP_EventDecodeNumber( event ) ) -- coverCharge
	KEP_EventEncodeNumber( g_PendingSpawnEvent, KEP_EventDecodeNumber( event ) ) -- instanceId
	KEP_EventAddToServer( g_PendingSpawnEvent )

	local urlParams = KEP_GetUrlParameters()
	if urlParams ~= nil then

		-- determine if we're tracking this request
		local trackingId = ""
		s, e, trackingId = string.find(urlParams, "RTSID=(.+)&")
		if trackingId == nil then
			s, e, trackingId = string.find(urlParams, "RTSID=(.+)$")
		end

		if trackingId ~= nil then
			Log("pendingSpawnHandler: trackingId="..trackingId)
			local web_address = GameGlobals.WEB_SITE_PREFIX..TRACKING_REQUEST_ACCEPT_SUFFIX..trackingId.."&zonestart=true"
			makeWebCall( web_address, WF.TRACKING_REQUEST_GAME_ACCEPT, 1, 1)
		end
	end

	downloadZone( InstanceId_GetId(filter) )
	return KEP.EPR_CONSUMED
end

function Dialog_OnDestroy(dialogHandle)
	ProgressClose("ZoneDownloader")
	Helper_Dialog_OnDestroy( dialogHandle )
end

function ZoneDownloadHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == 200 then
		nextFile()
	end
end

function loginDownloadHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_login_username = KEP_EventDecodeString(event)
	g_login_password = KEP_EventDecodeString(event)
	local fullid = KEP_EventDecodeNumber(event)
	g_zone_index = fullid
	g_zone_instance_id = InstanceId_GetInstanceId(fullid)
	downloadZone( InstanceId_GetId(g_zone_index) )
	local i = 1
	local file = g_t_file_list[i]
	while file ~= nil do
		i = i + 1
		file = g_t_file_list[i]
	end
	nextFile()
end

-- For now we assume if we have file it is up to date.
function verifyFile(filename)
	return KEP_TestPatchFileForUpdate( "\\GameFiles\\" .. filename)
end

function verifyTexFile(filename)
	return KEP_TestPatchFileForUpdate( "\\MapsModels\\" .. filename)
end

function prepareDownloadEntry(displayname, filename)
	if g_runLocal then
		return nil
	end
	local i = 0
	local j = 0
	local sizeNameB = 0
	local sizeNameE = 0
	local strippedName = filename

	sizeNameB, sizeNameE = string.find( filename, "\\" )

	if sizeNameB then
		strippedName = string.sub( filename, sizeNameE + 1, -1 )
	end

	i, j = string.find(filename, ".pak") -- assume if its a pak its a texture pak
	if i then
		if verifyTexFile(filename) == 0 then
			local file = {}
			file.display_name = displayname
			file.file_name = strippedName
			file.size = Zones[strippedName]
			file.fullname = filename
			return file
		else
			return nil
		end
	else
		if verifyFile(filename) == 0 then
			local file = {}
			file.display_name = displayname
			file.file_name = filename
			file.size = Zones[filename]
			return file
		else
			return nil
		end
	end
end

function addFileToDownloadList( file )
	if (file == nil) then return end
	if (file.size == nil) then file.size = 0 end
	table.insert( g_t_file_list, file )
	g_total_download_size = g_total_download_size + file.size
end

-- Ok for now lets hardcode our values, and figure out what we want to do later.
function downloadZone( zoneindex )

	-- Record metrics indicating that the download has started.
	if g_PendingSpawnEvent ~= nil then
		g_startTime = GetTimeMs()
	end

	-- check if zone index is valid
	if zoneindex==nil then
		LogError("downloadZone: zone index is nil")
		ProgressError("ZoneDownloader", "Error: Invalid Zone")
		return
	end
	if zoneindex<0 or zoneindex>=#ZoneFiles then
		LogError("downloadZone: invalid zone index "..zoneindex)
		ProgressError("ZoneDownloader", "Error: Invalid Zone")
		return
	end

	-- If g_lastZone is different from zoneindex, either this is the first download request since menu is opened,
	-- or the user is requesting a different destination.
	if g_lastZone~=zoneindex then
		Log("downloadZone: zoneIndex="..zoneindex)
		g_total_download_size = 0
		local fileList = ZoneFiles[zoneindex+1]
		local queueCount = 0

		-- Check if we are switching to a new zone while downloading
		if g_download_begun then

			-- We are currently dling a file - must cancel
			KEP_CancelAllDownloads( )

			-- Clear download list first
			g_t_file_list = {}
			g_cur_file_num = 0
		else
			g_download_begun = true
		end

		-- Appending all files from current zone
		for i,_ in pairs(fileList) do
			Log(" ... '"..tostring(fileList[i][1]).."' '"..tostring(fileList[i][2]).."'")
			local file = prepareDownloadEntry(fileList[i][1], fileList[i][2])
			if file~=nil then
				addFileToDownloadList( file )
				queueCount = queueCount + 1
			end
		end

		-- Add an entry that will monitor the progress of
		-- remaining pre-load assets.  The user won't be
		-- able to zone until these have been finished.
		local file = {}
		file.display_name = "Asset Pre-Load"
		file.file_name = "PreLoad"
		file.size = 1
		addFileToDownloadList( file )
		queueCount = queueCount + 1
		if queueCount > 0 then
			if g_cur_file_num==0 then
				nextFile()
			end
		else
			if g_PendingSpawnEvent ~= nil then
				sendSpawnConfirmationToServer()
			end
			MenuCloseThis()
		end
		g_lastZone = zoneindex
	end
end

function nextFile()

	g_cur_file_num = g_cur_file_num + 1
	local file = g_t_file_list[g_cur_file_num]
	if file ~= nil then
		local i = 0
		local j = 0
		i, j = string.find(file.file_name, ".pak") -- assume if its a pak its a texture pak
		if i then
			KEP_DownloadTexturePack( file.file_name, file.size )
		else
			i, j = string.find(file.file_name, "PreLoad")
			if i then
				KEP_ArmPreLoadProgress()
			else
				KEP_DownloadPatchFile( file.file_name, file.size )
			end
		end
	else
		if g_login_username ~= nil then
			MenuOpen("LoginMenu.xml")
			local evt = KEP_EventCreate( "DownloadFinishedEvent" )
			KEP_EventEncodeString(evt, g_login_username)
			KEP_EventEncodeString(evt, g_login_password)
			KEP_EventQueue( evt)
		else
			if g_PendingSpawnEvent ~= nil then
				sendSpawnConfirmationToServer()
			end
		end
		MenuCloseThis()
	end
end

function checkForStarKep()
	ret = KEP_TestPatchFileForUpdate("\\STAR.kep")
	if ret ~= 0 then
		g_runLocal = true
	end
end

function sendSpawnConfirmationToServer()
	KEP_EventQueue( g_PendingSpawnEvent )
	g_PendingSpawnEvent = nil
end
