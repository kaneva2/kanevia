--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua" )

g_objId = -1
g_objFriendId = -1
g_objName = nil
g_objCanPlayMovie = 0
g_objIsAttachable = 0
g_objDescription = "None"
g_objInteractive = 0
g_objIsLocal = 0
g_objAssetId = 0
g_objGlobalId = 0
g_objInvType = 0
g_objDerivable = 0
g_objCustomTexture = ""
g_isFlashFrame = false
g_dynObjType = DYNAMIC_OBJTYPE_INVALID

g_t_btnHandles = {}

DYNAMIC_OBJECT_INFO_EVENT = "DynamicObjectInfoEvent"
ENTER_BUILD_MODE_EVENT = "EnterBuildModeEvent"
DYNAMIC_OBJ_MENU_EVENT = "DynamicObjectMenuEvent"

BASE_HEIGHT = 29
BUTTON_LEFT = 35
BUTTON_HEIGHT = 18
ROW_SPACING = 5
OBJECT_NAME_MAX_LENGTH = 24

g_menuHeight = BASE_HEIGHT

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "updateIdHandler", DYNAMIC_OBJECT_INFO_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UGCSubmissionFeedbackEventHandler", UGC_SUBMISSION_FEEDBACK_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "SelectMediaEvent", KEP.MED_PRIO )
	UGCI.RegisterSubmissionEvents( )
	UGCI.RegisterUploadConfirmationMenuEvent( )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- collect button handles
	getButtonHandles()

	-- hide menu first
	Dialog_SetMinimized(gDialogHandle, true)
	Dialog_SetCaptionHeight(gDialogHandle, 0)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function updateMenu()

	-- hide menu first
	Dialog_SetMinimized(gDialogHandle, true)

	-- reset menu height
	g_menuHeight = BASE_HEIGHT

	-- reserve space for object name
	nextRow()

	-- hide all controls by default (except titlebar ctls)
	local ctlName, ctlHndl
	for ctlName, ctlHndl in pairs( g_t_btnHandles ) do
		enableControl( ctlHndl, false )
	end
	enableControl( Dialog_GetControl(gDialogHandle, "stVol"), false )

	if not KEP_IsOwnerOrModerator() then
		-- if not owned, check if we can go to instantbuy menu directly
		if g_objCanPlayMovie ~= 1 and g_objIsLocal ~= 1 and g_objDerivable ~= 1 then
			-- no other interaction available, launch instantbuy menu instead
			btnBuy_OnButtonClicked( Dialog_GetControl(gDialogHandle, "btnBuy") )
			MenuCloseThis()
			return
		end
	end
	setObjectName(g_objName)

	-- DRF - Changed
	g_isFlashFrame = (g_dynObjType == DYNAMIC_OBJTYPE_MEDIA_FRAME)

	if g_objIsLocal ~= 1 then
		-- if regular item (non-local-imports), show instantbuy button
		layoutControl( g_t_btnHandles.btnBuy, true )
		nextRow()
	end

	if KEP_IsOwnerOrModerator() then

		if g_objIsLocal~=1 then
			-- if non-local and owned, show "change pattern" button
			layoutControl( g_t_btnHandles.btnPattern, true )
			nextRow()
		end

		-- if owned, show "pick up" button
		layoutControl( g_t_btnHandles.btnPickUp, true )
		if g_objIsLocal==1 then
			-- cannot pick up local item, label it as "discard"
			Static_SetText( Button_GetStatic(g_t_btnHandles["btnPickUp"]), "Discard" )
		end
		nextRow()

		if g_objIsLocal==1 then
			-- if local object (imports), show "submit" button
			layoutControl( g_t_btnHandles.btnSubmit, true )
			nextRow()
		end
	end

	if g_objDerivable == 1 then
		layoutControl( g_t_btnHandles.btnRedesign, true )
		nextRow()
	end

	Dialog_SetCaptionHeight(gDialogHandle, 35)
	Dialog_SetMinimized(gDialogHandle, false)
end

function layoutControl( ctl, enable, xoffset, yoffset )
	if xoffset==nil then
		xoffset = 0
	end
	if yoffset==nil then
		yoffset = 0
	end
	Control_SetLocation( ctl, BUTTON_LEFT + xoffset, g_menuHeight - ROW_SPACING - BUTTON_HEIGHT + yoffset )
	enableControl( ctl, enable )
end

function nextRow( extraSpacing )
	if extraSpacing==nil then
		extraSpacing = 0
	end
	g_menuHeight = g_menuHeight + BUTTON_HEIGHT + ROW_SPACING + extraSpacing
	local imgMidBack = Dialog_GetControl(gDialogHandle, "imgMiddleBackground")
	Control_SetSize(imgMidBack, 199, g_menuHeight-20)
	local imgBotBack = Dialog_GetControl(gDialogHandle, "imgBottomBackground")
	Control_SetLocation(imgBotBack, 0, g_menuHeight-21)
end

function setObjectName(name)
	local objNameShort = name
	if string.len(objNameShort) > OBJECT_NAME_MAX_LENGTH then
		objNameShort = string.sub(objNameShort, 1, OBJECT_NAME_MAX_LENGTH) .. "..."
	end
	local lblName = Dialog_GetControl(gDialogHandle, "lblObjectName")
	Static_SetText(lblName, objNameShort)
end

function enableControl(ctrl, enable)
	Control_SetVisible(ctrl, enable)
	Control_SetEnabled(ctrl, enable)
end

function updateIdHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_objId  = KEP_EventDecodeNumber( event )
	g_objName, g_objDescription, g_objCanPlayMovie, g_objIsAttachable, g_objInteractive, g_objIsLocal, g_objDerivable, g_objAssetId, g_objFriendId, g_objCustomTexture, g_objGlobalId, g_objInvType, textureURL, gameItemId, playerId, g_dynObjType = KEP_DynamicObjectGetInfo(g_objId)
	if g_objName == "unknown" then
		requestDynamicObjectInfoFromWeb()
		-- wait until name is obtained before calling updateMenu
	else
		updateMenu()
	end
	return KEP.EPR_OK
end

-- Catch the xml from the server
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.GET_DYNAMIC_OBJECT_INFO then
		g_objectData = KEP_EventDecodeString( event )
		s, trash, g_objName = string.find(g_objectData, "<display_name>(.-)</display_name>", 1)
		if g_objName==nil then
			g_objName = "unknown"
		end
		updateMenu()
	end
end

function enterBuildMode(mode)
	local ev = KEP_EventCreate( ENTER_BUILD_MODE_EVENT)
	KEP_EventSetFilter(ev, 1)
	KEP_EventEncodeNumber(ev, mode)
	KEP_EventQueue( ev)
	MenuCloseThis()
end

function requestDynamicObjectInfoFromWeb()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..DYNAMIC_OBJECT_INFO_SUFFIX.."&objectId="..g_objId, WF.GET_DYNAMIC_OBJECT_INFO)
end

function btnBuy_OnButtonClicked( buttonhandle )
	g_ObjectPurchasingHandle = MenuOpen("ContextMenuObjectPurchasing.xml")

	--fire event to pass placement id to the menu script
	menuOpenEvent = KEP_EventCreate( DYNAMIC_OBJECT_INFO_EVENT )
	KEP_EventEncodeNumber( menuOpenEvent, g_objId )
	KEP_EventQueue( menuOpenEvent )
end

function btnEdit_OnButtonClicked( buttonhandle )
	enterBuildMode(1)
end

function btnPattern_OnButtonClicked( buttonhandle )
	enterBuildMode(3)
end

function btnPickUp_OnButtonClicked( buttonhandle )
	KEP_DeleteDynamicObjectPlacement(g_objId)
	MenuCloseThis()
end

function btnRedesign_OnButtonClicked( buttonhandle )
	UGCI.StartDynamicObjectDerivation( g_objId, g_objName )
end

function btnSubmit_OnButtonClicked( buttonhandle )
	UGCI.SubmitUGCDynamicObject( g_objId, g_objName)
	changeSubmitButtonState( true )
end

function btnChangeMedia_OnButtonClicked( buttonhandle )
	if g_isFlashFrame == true then
		MenuOpen("PlaylistSelect.xml")

		--fire event to pass placement id to the menu script
		local menuOpenEvent = KEP_EventCreate( "SelectMediaEvent" )
		KEP_EventEncodeString( menuOpenEvent, PLAYLIST_TYPE_FRAME)
		KEP_EventEncodeNumber( menuOpenEvent, g_objId )
		KEP_EventEncodeNumber( menuOpenEvent, 0 )
		KEP_EventEncodeNumber( menuOpenEvent, "" )
		KEP_EventQueue( menuOpenEvent )
	else
		MenuOpen("PlaylistSelect.xml")

		--fire event to pass placement id to the menu script
		local menuOpenEvent = KEP_EventCreate( "SelectMediaEvent" )
		KEP_EventEncodeString( menuOpenEvent, PLAYLIST_TYPE_TV)
		KEP_EventEncodeNumber( menuOpenEvent, g_objId )
		KEP_EventEncodeNumber( menuOpenEvent, 0 )
		KEP_EventEncodeNumber( menuOpenEvent, "" )
		KEP_EventQueue( menuOpenEvent )
	end
end

function getButtonHandles()
	local ch = Dialog_GetControl(gDialogHandle, "btnBuy")
	if ch ~= nil then
		g_t_btnHandles["btnBuy"] = ch
	end
	local ch = Dialog_GetControl(gDialogHandle, "btnChangeMedia")
	if ch ~= nil then
		g_t_btnHandles["btnChangeMedia"] = ch
	end
	local ch = Dialog_GetControl(gDialogHandle, "btnEdit")
	if ch ~= nil then
		g_t_btnHandles["btnEdit"] = ch
	end
	local ch = Dialog_GetControl(gDialogHandle, "btnPattern")
	if ch ~= nil then
		g_t_btnHandles["btnPattern"] = ch
	end
	local ch = Dialog_GetControl(gDialogHandle, "btnPickUp")
	if ch ~= nil then
		g_t_btnHandles["btnPickUp"] = ch
	end
	local ch = Dialog_GetControl(gDialogHandle, "btnSubmit")
	if ch ~= nil then
		g_t_btnHandles["btnSubmit"] = ch
	end
	local ch = Dialog_GetControl(gDialogHandle, "btnRedesign")
	if ch ~= nil then
		g_t_btnHandles["btnRedesign"] = ch
	end
end

function UGCSubmissionFeedbackEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter==UGC_TYPE.DYNOBJ then		-- only check filter (ugcType) here, objectid (ugcObjId) is not previously known
		local sfe = UGCI.DecodeSubmissionFeedbackEvent( event, filter, objectid )
		if sfe.param1==g_objId then
			if sfe.httpStatusCode==200 and sfe.result==SubmissionResult.SUCCEEDED and sfe.uploadId~=nil and sfe.basePrice~=nil and sfe.validationMsg~=nil and sfe.validationLog~=nil then

				-- display upload dialog directly
				UGCI.ShowUGCUploadDlg( sfe.uploadId, sfe.isDerivative, sfe.ugcType, sfe.ugcObjId, sfe.objName, sfe.param1, sfe.param2, 0, 0 )

				-- close current dialog
				MenuCloseThis()

				return KEP.EPR_CONSUMED
			else
				-- Reenable submit button and change text back to "Submit"
				changeSubmitButtonState( false )
			end
		end
	end
	return KEP.EPR_OK
end

function changeSubmitButtonState( inProg )
	local btn = Dialog_GetButton( gDialogHandle, "btnSubmit" )
	local ctl = Button_GetControl( btn )
	local sta = Button_GetStatic( btn )
	if inProg then
		-- submission request sent, pending response
		Static_SetText( sta, "Requesting..." )
		Control_SetEnabled( ctl, false )
	else
		-- normal state, ready to submit
		Static_SetText( sta, "Submit" )
		Control_SetEnabled( ctl, true )
	end
end
