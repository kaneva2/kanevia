--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Menu.lua - Kaneva Menu Script Template
-- 
-- Template script for creating a new Kaneva client menu.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

local m_item = {}

-- When the menu is created
function onCreate()
	KEP_EventRegisterHandler( "editParamHandler", "EDIT_PARAM_EVENT", KEP.MED_PRIO )

	local edDialogScrollBar  = EditBox_GetScrollBar(gHandles["edDialog"])
	
	local eh = ScrollBar_GetTrackDisplayElement(edDialogScrollBar)
	Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
	Element_SetCoords(eh, 1, 37, 17, 69)

	eh = ScrollBar_GetButtonDisplayElement(edDialogScrollBar)
	Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
	Element_SetCoords(eh, 1, 19, 17, 35)

	eh = ScrollBar_GetUpArrowDisplayElement(edDialogScrollBar)
	Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
	Element_SetCoords(eh, 1, 1, 17, 17)

	eh = ScrollBar_GetDownArrowDisplayElement(edDialogScrollBar)
	Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
	Element_SetCoords(eh, 1, 71, 17, 87)
	-- Shift out of the way of the main editor
	MenuOffsetLocationThis(400, 0)
end

-- Saves the dialog
function btnSave_OnButtonClicked(buttonHandle)
	local event = KEP_EventCreate( "CHANGED_PARAM_EVENT" )
	KEP_EventEncodeString(event, m_item.attribute)
	KEP_EventEncodeString(event, m_item.dialogPage)
	KEP_EventEncodeString(event, m_item.dialogPage)
	if m_item.index ~= "None" and m_item.baseAttribute ~= "None" then
		KEP_EventEncodeString(event, "TEXT")
		KEP_EventEncodeString(event, m_item.index)
		KEP_EventEncodeString(event, m_item.baseAttribute)
	end
	KEP_EventQueue( event )
	MenuCloseThis()
end

function edDialog_OnEditBoxChange(editBoxHandle, editBoxText)
	m_item.dialogPage = editBoxText
end

--Event Handlers
function editParamHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- load initial table values into g_param
	local event = {}
	event.attribute = KEP_EventDecodeString(tEvent)
	event.name = KEP_EventDecodeString(tEvent)
	event.dialogPage = KEP_EventDecodeString(tEvent)
	if KEP_EventMoreToDecode(tEvent) > 0 then
		event.index = KEP_EventDecodeString(tEvent) 
		event.baseAttribute = KEP_EventDecodeString(tEvent)
	end

	m_item.attribute = event.attribute
	m_item.name = event.name
	m_item.dialogPage = event.dialogPage
	
	m_item.index = event.index or "None"
	m_item.baseAttribute = event.baseAttribute or "None"
	
	Static_SetText(gHandles["stcTitle"], "Edit the dialog for " .. m_item.name)
	EditBox_SetText(gHandles["edDialog"], m_item.dialogPage, true)
end