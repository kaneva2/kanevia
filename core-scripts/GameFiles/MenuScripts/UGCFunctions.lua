--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\MenuHelper.lua") -- called from client script
dofile("..\\Scripts\\UGCImportState.lua")

UGCI = {}

UGC_SUBMISSION_FEEDBACK_EVENT = "UGCSubmissionFeedbackEvent"
UGC_UPLOAD_CONFIRMATION_MENU_EVENT = "UGCUploadConfirmationMenuEvent"
UGC_UPLOAD_PROGRESS_EVENT = "UGCUploadProgressEvent"
UGC_UPLOAD_COMPLETION_EVENT = "UGCUploadCompletionEvent"
UGC_UPLOAD_MENU_EVENT = "UGCUploadMenuEvent"
UGC_IMPORT_PROGRESS_EVENT = "UGCImportProgressEvent"
UGC_IMPORT_MENU_EVENT = "UGCImportMenuEvent"
UGC_IMPORT_OPTION_MENU_EVENT = "UGCImportOptionMenuEvent"
UGC_IMPORT_CONFIRMATION_MENU_EVENT = "UGCImportConfirmationMenuEvent"
UGC_IMPORT_CONFIRMATION_RESULT_EVENT = "UGCImportConfirmationResultEvent"
UGC_IMPORT_COMPLETION_EVENT = "UGCImportCompletionEvent"
UGC_IMPORT_FAILED_MENU_EVENT = "UGCImportFailedMenuEvent"
UGC_GLOBAL_OPTIONS_QUERY_EVENT = "UGCGlobalOptionsQueryEvent"
UGC_GLOBAL_OPTIONS_RESULT_EVENT = "UGCGlobalOptionsResultEvent"

-- global option types
UGCI.GOT = {}
UGCI.GOT.UNKNOWN = 0
UGCI.GOT.STRING = 1
UGCI.GOT.NUMERIC = 2

-- from UGCLink.aspx.cs
SubmissionResult = {}
SubmissionResult.INTERNAL_ERROR = -1
SubmissionResult.UNKNOWN_TYPE = -2
SubmissionResult.VALIDATION_FAILED = -3
SubmissionResult.TOO_MANY_FILES = 9999
SubmissionResult.NO_UPLOADS = 0
SubmissionResult.SUCCEEDED = 1

-- Import errors (from UGC_Globals.h)
UGCIE = {}
UGCIE.BAD_DAE_FILE = 100					-- DAE file is ill-formed
UGCIE.BAD_DAE_VERSION = 101					-- Unsupported COLLADA version
UGCIE.SIZE_TOO_BIG = 110					-- Invalid mesh dimension or total dimension
UGCIE.SIZE_TOO_SMALL = 111
UGCIE.NO_MESH = 120							-- No mesh
UGCIE.TOO_MANY_MESHES = 121					-- Too many meshes
UGCIE.TOO_FEW_TRIANGLES = 122 				-- Geometry too simple (degenerated)
UGCIE.TOO_MANY_TRIANGLES = 123				-- Geometry too complex
UGCIE.TOO_MANY_VERTICES = 124				-- Too many vertices
UGCIE.TOO_MANY_UVSETS = 125					-- Too many sets of UVs
UGCIE.TOO_MANY_TEXTURES = 126				-- Too many textures (per mesh or total)
UGCIE.TOO_MANY_BONES = 127					-- Too many bones in the skeleton
UGCIE.EXTRA_BONES = 130						-- Extra bone found while comparing to reference skeleton
UGCIE.MISSING_BONES = 131					-- Missing bone found while comparing to reference skeleton
UGCIE.DURATION_TOO_SHORT = 140				-- Duration is too short
UGCIE.DURATION_TOO_LONG = 141				-- Duration is too long
UGCIE.NO_ANIMATION = 150					-- No animation found while importing an animation
UGCIE.MANDATORY_VALIDATION_FAILED = 200		-- Client-side mandatory validation failed
UGCIE.OUT_OF_MEMORY = 900					-- Insufficient memory to process DAE file
UGCIE.UNSPECIFIED_ERROR = 1000				-- Exception (not a valid error code): import fails without explicit error code.
UGCIE.SESSION_NOT_FOUND = 1001				-- Exception (not a valid error code): import session not found

-- from validation.h
RecoveryAction = {}
RecoveryAction.NoRecovery = 0
RecoveryAction.Resize = 1

-- UGC options
UGCOPT_EI_MAXTRICOUNT	= "EI_MaxTriangleCount"
UGCOPT_EI_MAXTEXCOUNT	= "EI_MaxTextureCount"
UGCOPT_EI_MAXTEXSIZE	= "EI_MaxTextureSize"
UGCOPT_EI_MAXSIZE		= "EI_MaxSizeXYZSum"
UGCOPT_PERSONALSPACEMINX= "PersonalSpaceMinX"
UGCOPT_PERSONALSPACEMAXX= "PersonalSpaceMaxX"
UGCOPT_PERSONALSPACEMINY= "PersonalSpaceMinY"
UGCOPT_PERSONALSPACEMAXY= "PersonalSpaceMaxY"
UGCOPT_PERSONALSPACEMINZ= "PersonalSpaceMinZ"
UGCOPT_PERSONALSPACEMAXZ= "PersonalSpaceMaxZ"

function ugcLog(txt) KEP_Log(LogName().."UGCFunctions::"..txt) end
function ugcLogError(txt) KEP_LogError(LogName().."UGCFunctions::"..txt) end

function UGCI.SetDialogInstanceId( dialogHandle, id )
	ugcLog( "UGCI.SetDialogInstanceId" )
	local sh = Dialog_GetStatic( dialogHandle, "__InstanceId__" )
	if sh~=nil then
		Static_SetText( sh, tostring( id ) )
	end
end

function UGCI.GetDialogInstanceId( dialogHandle )
	ugcLog( "UGCI.GetDialogInstanceId" )
    local sh = Dialog_GetStatic( dialogHandle, "__InstanceId__" )
	if sh~=nil then
		local sID = Static_GetText( sh )
		return tonumber(sID)
	end
	return -1
end

function UGCI.CallUGCHandler(menu, bModal, dropId, importInfo, targetInfo, btnState, keyState )
	ugcLog( "UGCI.CallUGCHandler: " .. menu .. ", drop#" .. dropId )

	local dh = nil
	if (bModal == true or bModal == 1) then
		dh = MenuOpenModal(menu)
	else
		dh = MenuOpen(menu)
	end
	if dh==nil then
		ugcLogError( "UGCI.CallUGCHandler: MenuOpen(" .. menu .. ") FAILED" )
		return
	end

	-- pass drop ID to dialog to avoid multi-drop ambiguity
	UGCI.SetDialogInstanceId( dh, dropId )

	-- send parameters
	local eventName = UGC_IMPORT_MENU_EVENT
	local e = KEP_EventCreate( eventName )
	KEP_EventSetObjectId( e, dropId )
	KEP_EventSetFilter( e, Dialog_GetId( dh ) )		-- dedicate this event to a specified instance of a named dialog
	UGCI.ComposeUGCImportMenuEvent( e, importInfo, targetInfo, btnState, keyState )
	KEP_EventQueue( e )
end

function UGCI.ComposeUGCImportMenuEvent( event, importInfo, targetInfo, btnState, keyState )
	UGCI.EncodeImportInfo( event, importInfo )
	UGCI.EncodeTargetInfo( event, targetInfo )
	KEP_EventEncodeNumber( event, btnState )
	KEP_EventEncodeNumber( event, keyState )
end

function UGCI.ParseUGCImportMenuEvent( event )
	local result = {}
	result.importInfo = UGCI.DecodeImportInfo( event )
	result.targetInfo = UGCI.DecodeTargetInfo( event )
	result.btnState = KEP_EventDecodeNumber( event )
	result.keyState = KEP_EventDecodeNumber( event )
	return result
end

function UGCI.EncodeTargetInfo( e, targetInfo )
	KEP_EventEncodeNumber( e, targetInfo.valid )
	KEP_EventEncodeNumber( e, targetInfo.pos.x )
	KEP_EventEncodeNumber( e, targetInfo.pos.y )
	KEP_EventEncodeNumber( e, targetInfo.pos.z )
	KEP_EventEncodeNumber( e, targetInfo.normal.x )
	KEP_EventEncodeNumber( e, targetInfo.normal.y )
	KEP_EventEncodeNumber( e, targetInfo.normal.z )
	KEP_EventEncodeNumber( e, targetInfo.type )
	KEP_EventEncodeNumber( e, targetInfo.id )
	KEP_EventEncodeString( e, targetInfo.name )
end

function UGCI.DecodeTargetInfo( e )
	local targetInfo = {}
	targetInfo.valid = KEP_EventDecodeNumber( e )
	targetInfo.pos = {}
	targetInfo.pos.x = KEP_EventDecodeNumber( e )
	targetInfo.pos.y = KEP_EventDecodeNumber( e )
	targetInfo.pos.z = KEP_EventDecodeNumber( e )
	targetInfo.normal = {}
	targetInfo.normal.x = KEP_EventDecodeNumber( e )
	targetInfo.normal.y = KEP_EventDecodeNumber( e )
	targetInfo.normal.z = KEP_EventDecodeNumber( e )
	targetInfo.type = KEP_EventDecodeNumber( e )
	targetInfo.id = KEP_EventDecodeNumber( e )
	targetInfo.name = KEP_EventDecodeString( e )
	return targetInfo
end

function UGCI.EncodeImportInfo( e, importInfo )
	KEP_EventEncodeNumber( e, importInfo.category )
	KEP_EventEncodeNumber( e, importInfo.type )
	KEP_EventEncodeString( e, importInfo.fullPath )
	KEP_EventEncodeNumber( e, importInfo.sessionId )
end

function UGCI.DecodeImportInfo( e )
	local importInfo = {}
	importInfo.category = KEP_EventDecodeNumber( e )
	importInfo.type = KEP_EventDecodeNumber( e )
	importInfo.fullPath = KEP_EventDecodeString( e )
	importInfo.sessionId = KEP_EventDecodeNumber( e )
	return importInfo
end

-- Process reports received when a file is dragged over Client window
function UGCI.ReportPossibleImportAttempt( importInfo, target, btnState, keyState )
end

-- Process reports of invalid import requests
function UGCI.ReportInvalidImport( importInfo, target, btnState, keyState )
	local msg = "Unable to process dropped file: [" .. importInfo.fullPath .. "] -- unknown request."
	ugcLogError( msg )
	KEP_SendChatMessage( ChatType.System, msg )
end

function UGCI.ParseAndDisplayValidationError( validationMsg, validationLog )
	ugcLog( "UGCI.ParseAndDisplayValidationError: validationMsg=" .. validationMsg )

	local s, e, errMsg, validRangeMsg, propValueMsg, importTypeMsg, menu
	s, e, errMsg = string.find( validationMsg, "([^|]*)" )

	if errMsg~=nil or errMsg~="" then
		s, e, validRangeMsg = string.find( validationMsg, "([^|]*)", e + 2 )
		s, e, propValueMsg = string.find( validationMsg, "([^|]*)", e + 2 )
		s, e, importTypeMsg = string.find( validationMsg, "([^|]*)", e + 2 )
		s, e, menu = string.find( errMsg, "{#(.+)}" )
		if menu~=nil then
			if UGCI.DisplayImportFailedMenu( menu, { validRangeMsg, propValueMsg, importTypeMsg } ) then
				-- all done
				return
			else
				-- menu not found, display generic error instead
				errMsg = "Unhandled server error: " .. menu
			end
		end
	else
		errMsg = "Unknown server error."
	end

	-- display error in a generic message box
	errMsg = "Validation failed:\n\n" .. errMsg
	KEP_MessageBox( errMsg, "Submission failed" )
end

-- Sample Usage: UGCI.DisplayImportFailedMenu( "UGCImportTriangleCountWarning", { "100", "300", "Accessories" } )
function UGCI.DisplayImportFailedMenu( menuName, stringParameters )
	-- launch menu for error display
	local dh = MenuOpenModal(menuName .. ".xml")
	if dh~=nil then
		local e = KEP_EventCreate( UGC_IMPORT_FAILED_MENU_EVENT )
		for idx, val in ipairs( stringParameters ) do
			KEP_EventEncodeString( e, val )
		end
		KEP_EventQueue( e )
		return true
	end
	return false
end

-- Call to process UGC derivation request
function UGCI.StartDynamicObjectDerivation( placementId, objName )

	MenuOpenModal("UGCUpload.xml")
	local uploadId = ""	-- upload Id is not yet determined
	local ugcObjId = placementId -- use placement ID to submit UGC derivation

	local creationEvent = KEP_EventCreate( UGC_UPLOAD_MENU_EVENT )
	KEP_EventEncodeString(creationEvent, uploadId )
	KEP_EventEncodeNumber(creationEvent, 1)	-- is derivative
	KEP_EventEncodeNumber(creationEvent, UGC_TYPE.DYNOBJ_DERIVATIVE)
	KEP_EventEncodeNumber(creationEvent, ugcObjId)
	KEP_EventEncodeString(creationEvent, objName)
	KEP_EventEncodeNumber(creationEvent, placementId)	-- param1
	KEP_EventEncodeNumber(creationEvent, 0)			-- param2
	KEP_EventEncodeNumber(creationEvent, 1)			-- enable refresh button
	KEP_EventEncodeNumber(creationEvent, 0)			-- commission not defined
	KEP_EventQueue( creationEvent )
	
	MenuCloseThis()
end

-- Call to submit UGC DO for upload
function UGCI.SubmitUGCDynamicObject( placementId, objName )
	ugcLog( "UGCI.SubmitUGCDynamicObject: [" .. placementId .. "] = " .. objName  )

	local ugcType = UGC_TYPE.DYNOBJ

	-- compose submission feedback event
	local ev = KEP_EventCreate( UGC_SUBMISSION_FEEDBACK_EVENT )
	KEP_EventSetFilter( ev, ugcType )
	KEP_EventEncodeNumber( ev, 0 )				-- 0: not derivation
	KEP_EventEncodeNumber( ev, ugcType )
	KEP_EventEncodeString( ev, objName )
	KEP_EventEncodeString( ev, "" )			-- place holder for icon full path used in submitting ugc derivations
	KEP_EventEncodeNumber( ev, placementId )	-- param1
	KEP_EventEncodeNumber( ev, 0 )				-- param2

	-- Submit upload request now
	KEP_SubmitLocalDynamicObjectByPlacementId( placementId, ev )
end

-- ED-7498 - Deprecate UGC Upload DynamicObject Derivation
---- Call to submit UGC derivation for upload
--function UGCI.SubmitDynamicObjectDerivation( placementId, objName, iconFullPath )
--	ugcLog( "UGCI.SubmitDynamicObjectDerivation: [" .. placementId .. "] = " .. objName  )
--
--	local ugcType = UGC_TYPE.DYNOBJ_DERIVATIVE
--
--	-- compose submission feedback event
--	local ev = KEP_EventCreate( UGC_SUBMISSION_FEEDBACK_EVENT )
--	KEP_EventSetFilter( ev, ugcType )
--	KEP_EventEncodeNumber( ev, 1 )				-- 1: is derivation
--	KEP_EventEncodeNumber( ev, ugcType )
--	KEP_EventEncodeString( ev, objName )
--	KEP_EventEncodeString( ev, iconFullPath )
--	KEP_EventEncodeNumber( ev, placementId )	-- param1
--	KEP_EventEncodeNumber( ev, 0 )				-- param2
--
--	-- Submit upload request now
--	KEP_SubmitDynamicObjectDerivationByPlacementId( placementId, ev )
--end

-- Call to submit generic UGC object for upload
function UGCI.SubmitUGCObj( ugcType, ugcId, isDerivation, ugcObjName, iconPath, param1, param2 )

	-- compose submission feedback event
	local ev = KEP_EventCreate( UGC_SUBMISSION_FEEDBACK_EVENT )
	KEP_EventSetFilter( ev, ugcType )
	KEP_EventSetObjectId( ev, ugcId )
	KEP_EventEncodeNumber( ev, isDerivation )	-- 0: not derivation, 1: derivation
	KEP_EventEncodeNumber( ev, ugcType )
	KEP_EventEncodeString( ev, ugcObjName )
	KEP_EventEncodeString( ev, iconPath )
	KEP_EventEncodeNumber( ev, param1 )
	KEP_EventEncodeNumber( ev, param2 )

	-- Submit upload request now
	return UGC_SubmitUGCObj( ugcType, ugcId, ev )
end

-- Submission feedback event handler (generic UGC obj)
function UGCI.SubmissionFeedbackEventHandler( event, filter, objectid, ugcType, ugcObjId, showPricingDlg, showUploadDlg, closeCurrentDlg )
	if filter==ugcType or objectid==ugcObjId then
		local sfe = UGCI.DecodeSubmissionFeedbackEvent( event, filter, objectid )
		if sfe.httpStatusCode==200 and sfe.result==SubmissionResult.SUCCEEDED and sfe.uploadId~=nil then
			ugcLog( "UGCI.SubmissionFeedbackEventHandler: UGC submission succeeded, uID=" .. sfe.uploadId .. ", ugcId=" .. ugcObjId )

			if showPricingDlg then
				-- show pricing dialog for user confirmation
				if sfe.basePrice~=nil and sfe.validationMsg~=nil and sfe.validationLog~=nil then

					-- display confirmation dialog (disabled)
					UGCI.ShowUGCUploadConfirmationDlg( sfe.uploadId, sfe.ugcType, sfe.ugcObjId, sfe.basePrice, sfe.validationMsg, sfe.validationLog, sfe.objName, sfe.param1, sfe.param2 )
				else
				-- display some error
				end

			elseif showUploadDlg then
				-- display an upload dialog
				UGCI.ShowUGCUploadDlg( sfe.uploadId, sfe.isDerivative, sfe.ugcType, sfe.ugcObjId, sfe.objName, sfe.param1, sfe.param2, 0, 0 )
			else
				-- upload now without UI
				ugcLog( "UGCI.SubmissionFeedbackEventHandler: Submission succeeded, uID=" .. sfe.uploadId )	-- no confirmation required, go ahead and upload
				UGCI.UploadUGCObj( sfe.isDerivative, sfe.uploadId, sfe.ugcType, sfe.ugcObjId, sfe.objName )		-- 0: not a derivation
			end

			if closeCurrentDlg then
				MenuCloseThis()
			end
		end
		return true
	end
	return false
end

function UGCI.ShowUGCUploadConfirmationDlg( uploadId, ugcType, ugcObjId, basePrice, validationMsg, validationLog, ugcObjName, param1, param2 )
	ugcLog( "UGCI.ShowUGCUploadConfirmationDlg" )

	g_dlgULCfm = MenuOpen( "UGCUploadConfirmation.xml")

	-- fire event to pass parameters to the menu script
	menuOpenEvent = KEP_EventCreate( UGC_UPLOAD_CONFIRMATION_MENU_EVENT )

	KEP_EventEncodeString( menuOpenEvent, uploadId )
	KEP_EventEncodeNumber( menuOpenEvent, 0 )			-- 0: not derivative
	KEP_EventEncodeNumber( menuOpenEvent, ugcType )
	KEP_EventEncodeNumber( menuOpenEvent, ugcObjId )
	KEP_EventEncodeNumber( menuOpenEvent, basePrice )
	KEP_EventEncodeString( menuOpenEvent, validationMsg )
	KEP_EventEncodeString( menuOpenEvent, validationLog )
	KEP_EventEncodeString( menuOpenEvent, ugcObjName )
	KEP_EventEncodeNumber( menuOpenEvent, param1 )
	KEP_EventEncodeNumber( menuOpenEvent, param2 )
	KEP_EventQueue( menuOpenEvent )
	-- async: continue on UGCUploadConfirmation.lua
end

function UGCI.ShowUGCUploadDlg( uploadId, isDerivative, ugcType, ugcObjId, ugcObjName, param1, param2, useRefreshButton, commissionAmount )
	local menu = "UGCUpload.xml"
	local menuEventName = UGC_UPLOAD_MENU_EVENT
	if menu~=nil then
		MenuOpenModal(menu)
		local menuEvent = KEP_EventCreate( menuEventName )
		KEP_EventEncodeString(menuEvent, uploadId )
		KEP_EventEncodeNumber(menuEvent, isDerivative)
		KEP_EventEncodeNumber(menuEvent, ugcType)
		KEP_EventEncodeNumber(menuEvent, ugcObjId)
		KEP_EventEncodeString(menuEvent, ugcObjName)
		KEP_EventEncodeNumber(menuEvent, param1)
		KEP_EventEncodeNumber(menuEvent, param2)
		KEP_EventEncodeNumber(menuEvent, useRefreshButton)
		if commissionAmount==nil then
			KEP_EventEncodeNumber(menuEvent, 0)
		else
			KEP_EventEncodeNumber(menuEvent, commissionAmount)
		end
		KEP_EventQueue( menuEvent )
	end
	-- async: continue on UGCUpload.lua
end

-- Call to proceed UGC upload process
function UGCI.UploadUGCObj( isDerivation, uploadId, ugcType, ugcObjId, ugcObjName )
	ugcLog( "UGCI.UploadUGCObj: type=" .. ugcType .. ", Id=" .. ugcObjId .. ", uId=" .. uploadId )

	local evProgress = KEP_EventCreate( UGC_UPLOAD_PROGRESS_EVENT )
	KEP_EventEncodeNumber( evProgress, isDerivation )
	KEP_EventEncodeNumber( evProgress, ugcType )
	KEP_EventEncodeString( evProgress, uploadId )
	KEP_EventEncodeNumber( evProgress, ugcObjId )
	KEP_EventEncodeString( evProgress, ugcObjName )

	local evCompletion = KEP_EventCreate( UGC_UPLOAD_COMPLETION_EVENT )
	KEP_EventEncodeNumber( evCompletion, isDerivation )
	KEP_EventEncodeNumber( evCompletion, ugcType )
	KEP_EventEncodeString( evCompletion, uploadId )
	KEP_EventEncodeNumber( evCompletion, ugcObjId )
	KEP_EventEncodeString( evCompletion, ugcObjName )

	return UGC_UploadUGCObj( uploadId, ugcType, ugcObjId, evProgress, evCompletion )
end

-- Call to deploy uploaded file
function UGCI.DeployUGCUpload( uploadId, ugcType, ugcId )
	local e = KEP_EventCreate( UGC_DEPLOY_COMPLETION_EVENT )
	KEP_EventEncodeNumber(e, ugcType )
	KEP_EventEncodeNumber(e, ugcId )
	UGC_DeployUGCUpload( uploadId, ugcType, e )
end

function UGCI.DecodeSubmissionFeedbackEvent( event, filter, objectid )
	ugcLog( "UGCI.DecodeSubmissionFeedbackEvent" )

	local res = {}
	res.isDerivative = KEP_EventDecodeNumber( event )		-- encoded in current script
	res.ugcType = KEP_EventDecodeNumber( event )			-- encoded in current script
	res.ugcObjId = objectid								-- encoded in current script or set by UGCManager
	res.objName = KEP_EventDecodeString( event )			-- encoded in current script
	res.iconFullPath = KEP_EventDecodeString( event )		-- encoded in current script
	res.param1 = KEP_EventDecodeNumber( event )			-- encoded in current script
	res.param2 = KEP_EventDecodeNumber( event )			-- encoded in current script
	res.resultText = KEP_EventDecodeString( event )		-- padded by WebCallTask
	res.httpStatusCode = KEP_EventDecodeNumber( event )	-- padded by WebCallTask

	if res.httpStatusCode==200 then
		local s, e = 0      -- start and end of captured string
		local sResult
		s, e, sResult = string.find( res.resultText, "<ReturnCode>(-*%d+)</ReturnCode>")
		if sResult~=nil then
			res.result = tonumber(sResult)
			s, e, res.uploadId = string.find( res.resultText, "<UploadId>(%d+)</UploadId>")
			s, e, res.basePrice = string.find( res.resultText, "<BasePrice>([%d.]+)</BasePrice>")
			s, e, res.validationMsg = string.find( res.resultText, "<ValidationMessage>(.*)</ValidationMessage>")
			s, e, res.validationLog = string.find( res.resultText, "<ValidationLog>(.*)</ValidationLog>")

			if res.result==SubmissionResult.INTERNAL_ERROR or res.result==SubmissionResult.TOO_MANY_FILES or res.result==SubmissionResult.UNKNOWN_TYPE or result == SubmissionResult.NO_UPLOADS then
				ugcLogError( "UGCI.DecodeSubmissionFeedbackEvent: invalid request" )
				KEP_MessageBox( "Submission Failed - Invalid Request", "Error" )
			elseif res.result==SubmissionResult.VALIDATION_FAILED then
				ugcLogError( "UGCI.DecodeSubmissionFeedbackEvent: validation failed" )
				UGCI.ParseAndDisplayValidationError( res.validationMsg, res.validationLog )
			elseif res.result~=SubmissionResult.SUCCEEDED then
				if validationMsg~=nil then
					ugcLogError( "UGCI.DecodeSubmissionFeedbackEvent: UGC submission error: " .. res.validationMsg )
					KEP_MessageBox( "Submission Failed - " .. res.validationMsg, "Error" )
				else
					ugcLogError( "UGCI.DecodeSubmissionFeedbackEvent: UGC submission error: unknown" )
					KEP_MessageBox( "Submission Failed - Error 1", "Error" )
				end
			elseif res.uploadId==nil or res.basePrice==nil or res.validationMsg==nil or res.validationLog==nil then
				KEP_MessageBox( "Submission Failed - Error 2", "Error" )
			end
		else
			ugcLogError( "UGCI.DecodeSubmissionFeedbackEvent: UGC submission error: result message is null" )
			KEP_MessageBox( "Submission Failed - Error 3", "Error" )
		end
	else
		-- URL Too Long ? (too many textures)
		if (res.httpStatusCode==414) then
			ugcLogError( "UGCI.DecodeSubmissionFeedbackEvent: UGC submission error - 414 - too many textures")
			KEP_MessageBox( "Submission Failed - Too Many Textures", "Error" )
		else
			ugcLogError( "UGCI.DecodeSubmissionFeedbackEvent: UGC submission error: wok returned " .. res.httpStatusCode )
			KEP_MessageBox( "Submission Failed - httpCode=" .. res.httpStatusCode, "Error" )
		end
	end
	return res
end

-- DRF - Added
function UGCI.ImportAndSubmitMp3File( importInfo, target, btnState, keyState )
	UGCI.ImportAndSubmitWavFile( importInfo, target, btnState, keyState )
end

function UGCI.ImportAndSubmitWavFile( importInfo, target, btnState, keyState )
	-- should return id then menus then pass id to submit.  Hard coding to autosubmit for now
	local id = UGC_ImportSoundFile(importInfo.fullPath)
	if id < 0 then
		local msg = ""
		if id == UGC_ERROR.SOURCE_READ_ERROR then
			msg = "Unable to process dropped file: [" .. importInfo.fullPath .. "] -- Unable to read source file."
		elseif id == UGC_ERROR.UNRECOGNIZED_FILE_TYPE then
			msg = "Unable to process dropped file: [" .. importInfo.fullPath .. "] -- Unrecognized file format."
		elseif id == UGC_ERROR.MEMORY_ERROR then
			msg = "Unable to process dropped file: [" .. importInfo.fullPath .. "] -- Memory allocation Error."
		elseif id == UGC_ERROR.UNSUPPORTED_CODEC then
			msg = "Unable to process dropped file: [" .. importInfo.fullPath .. "] -- Unsupported Codec."
		elseif id == UGC_ERROR.VORBIS_ENCODE_ERROR then
			msg = "Unable to process dropped file: [" .. importInfo.fullPath .. "] -- Vorbis Encode Error."
		elseif id == UGC_ERROR.CHANNEL_ERROR then
			msg = "Unable to process dropped file: [" .. importInfo.fullPath .. "] -- Unsupported Format, Input sound must be mono."
		else
			msg = "Unable to process dropped file: [" .. importInfo.fullPath .. "] -- Unknown Error." -- should never happen
		end
		ugcLogError( "UGCI.ImportAndSubmitWavFile: "..msg )
		KEP_SendChatMessage( ChatType.System, msg )
	else
		UGCI.SubmitUGCObj( UGC_TYPE.OGG_SOUND, id, 0, "", "", 0, 0 )
	end
end

function UGCI.ImportAndSubmitOggFile( importInfo, target, btnState, keyState )
	-- should return id then menus then pass id to submit.  Hard coding to autosubmit for now
	local id = UGC_ImportSoundFile(importInfo.fullPath)
	UGCI.SubmitUGCObj( UGC_TYPE.OGG_SOUND, id, 0, "", "", 0, 0 )
end

function UGCI.RegisterSubmissionEvents( )
	KEP_EventRegister( UGC_SUBMISSION_FEEDBACK_EVENT, KEP.MED_PRIO )
end

function UGCI.RegisterUploadConfirmationMenuEvent( )
	KEP_EventRegister( UGC_UPLOAD_CONFIRMATION_MENU_EVENT, KEP.MED_PRIO )
end

function UGCI.RegisterUploadEvents( )
	KEP_EventRegister( UGC_UPLOAD_PROGRESS_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( UGC_UPLOAD_COMPLETION_EVENT, KEP.MED_PRIO )
end

function UGCI.RegisterDeployEvents( )
	KEP_EventRegister( UGC_DEPLOY_COMPLETION_EVENT, KEP.MED_PRIO )
end

-- Display import failed message
function UGCI.ImportFailed( ImpSessionId, message, msgBoxWidth, msgBoxHeight )
	-- show message
	KEP_MessageBox( message, "Import Failed", 0, 0, msgBoxWidth, msgBoxHeight )

	-- dispose import session
	UGC_CleanupImportSessionAsync( ImpSessionId )

	MenuCloseThis()
end

-- Import Completion Event (to be handled by UGCManager.lua)
function UGCI.SendImportCompletionEvent( ugcType, ugcObjId, isDerivation, ugcObjName, param1, param2, submitNow )
	local event = KEP_EventCreate( UGC_IMPORT_COMPLETION_EVENT )
	KEP_EventSetFilter( event, ugcType )
	KEP_EventSetObjectId( event, ugcObjId )
	KEP_EventEncodeNumber( event, isDerivation )
	KEP_EventEncodeString( event, ugcObjName )
	KEP_EventEncodeNumber( event, param1 )
	KEP_EventEncodeNumber( event, param2 )
	KEP_EventEncodeNumber( event, submitNow )
	KEP_EventQueue( event )
end

function UGCI.DecodeImportCompletionEvent( event, filter, objectid )
	local ice = {}
	ice.ugcType = filter
	ice.ugcObjId = objectid
	ice.isDerivation = KEP_EventDecodeNumber( event )
	ice.ugcObjName = KEP_EventDecodeString( event )
	ice.param1 = KEP_EventDecodeNumber( event )
	ice.param2 = KEP_EventDecodeNumber( event )
	ice.submitNow = KEP_EventDecodeNumber( event )
	return ice
end

------------------------------------------------------------------------
-- UGC Global Options
------------------------------------------------------------------------

function UGCI.LoadGlobalOptionsFromServer( filter )
	makeWebCall( GameGlobals.WEB_SITE_PREFIX .. "kgp/ugclink.aspx?action=config", filter, 0, -1 )
end

function UGCI.ParseGlobalOptionsXml( xml )
	local sResult
	if xml~=nil then
		local s, e = 0      -- start and end of captured string
		s, e, sResult = string.find( xml, "<ReturnCode>(-*%d+)</ReturnCode>" )
		if sResult~=nil then
			local result = tonumber(sResult)
			if result==SubmissionResult.SUCCEEDED then

				local sUGCOptions
				s, e, sUGCOptions = string.find( xml, "<UGCOptions>(.-)</UGCOptions>" )
				if sUGCOptions~=nil then

					local resultSet = {}
					local sOption
					e = 0

					repeat

						s, e, sOption = string.find( sUGCOptions, "<Option>(.-)</Option>", e+1 )
						if sOption~=nil then

							local ss, ee, sOptionName, sOptionValue

							ss, ee, sOptionName = string.find( sOption, "<OptionName>(.-)</OptionName>" )
							if sOptionName~=nil then

								ss, ee, sOptionValue = string.find( sOption, "<OptionValue>(.-)</OptionValue>" )
								if sOptionValue~=nil then
									resultSet[sOptionName] = sOptionValue
								end
							end
						end
					until sOption == nil

					return resultSet
				end
			end
		end
	end

	if sResult==nil then
		sResult = "undefined"
	end

	ugcLogError( "UGCI.ParseGlobalOptionsXml: UGC module config failed - wok returned " .. sResult )
	return nil
end

function UGCI.GetGlobalOptions( filter, optionNames )
	local queryEvent = UGCI.ComposeGlobalOptionsQueryEvent( filter, optionNames )
	KEP_EventQueue( queryEvent )
end

function UGCI.ComposeGlobalOptionsQueryEvent( filter, optNames )
	local queryEvent = KEP_EventCreate( UGC_GLOBAL_OPTIONS_QUERY_EVENT )

	KEP_EventSetFilter( queryEvent, filter )

	-- Encode total number of options requested
	local optCount = 0
	for idx, name in pairs( optNames ) do
		optCount = optCount + 1
	end
	KEP_EventEncodeNumber( queryEvent, optCount )

	-- Encode option names
	for idx, name in pairs( optNames ) do
		KEP_EventEncodeString( queryEvent, name )
	end

	return queryEvent
end

function UGCI.ParseGlobalOptionsQueryEvent( queryEvent )
	local optNames = {}
	local optCount = KEP_EventDecodeNumber( queryEvent )
	for idx = 1, optCount do
		local name = KEP_EventDecodeString( queryEvent )
		table.insert( optNames, name )
	end

	return optNames
end

function UGCI.ComposeGlobalOptionsResultEvent( filter, optNameValuePairs )
	local resultEvent = KEP_EventCreate( UGC_GLOBAL_OPTIONS_RESULT_EVENT )

	KEP_EventSetFilter( resultEvent, filter )

	-- Encode total number of options requested
	local optCount = 0
	for optName, optValue in pairs( optNameValuePairs ) do
		optCount = optCount + 1
	end
	KEP_EventEncodeNumber( resultEvent, optCount )

	-- Encode option names
	for optName, optValue in pairs( optNameValuePairs ) do
		KEP_EventEncodeString( resultEvent, optName )
		KEP_EventEncodeString( resultEvent, optValue )
	end

	return resultEvent
end

function UGCI.ParseGlobalOptionsResultEvent( resultEvent )
	local optNameValuePairs = {}
	local optCount = KEP_EventDecodeNumber( resultEvent )
	for idx = 1, optCount do
		local optName = KEP_EventDecodeString( resultEvent )
		local optValue = KEP_EventDecodeString( resultEvent )
		if UGCI.GetGlobalOptionType( optName )==UGCI.GOT.NUMERIC then
			optNameValuePairs[optName] = tonumber( optValue )
		else
			optNameValuePairs[optName] = optValue
		end
	end

	return optNameValuePairs
end

function UGCI.GetGlobalOptionType( optName )
	-- all options are numeric at this moment
	return UGCI.GOT.NUMERIC
end
