--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

PURCHASE_GROUP_ITEMS_EVENT = "PurchaseGroupItemsEvent"
RETRY_PASTE_EVENT = "RetryPasteEvent"
ITEM_INFO_EVENT = "ItemInfoEvent"
ITEM_PURCHASED_EVENT = "ItemPurchasedEvent"

PARSE_NONE = 0
PARSE_PURCHASE = 1
PARSE_INFO = 2

g_parse = PARSE_NONE
g_items = {}
g_inventoryType = GameGlobals.IT_BOTH

g_retryPaste = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "purchaseGroupItemsHandler", PURCHASE_GROUP_ITEMS_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "itemPurchasedHandler", ITEM_PURCHASED_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( ITEM_INFO_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( ITEM_PURCHASED_EVENT, KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	if KEP_IsWorld() == true then
		KEP_MessageBox("Some copied items are not in your inventory. You cannot purchase these items from within a World so please" ..
		" purchase from the Virtual World of Kaneva and try again.")

		local btnPurchase = Dialog_GetControl(gDialogHandle, "btnPurchase")
		Control_SetEnabled(btnPurchase, false)
	end
end

function populateList()
	g_lbName = Dialog_GetControl(gDialogHandle, "lstItemName")
	g_lbQty = Dialog_GetControl(gDialogHandle, "lstItemQty")
	g_lbPrice = Dialog_GetControl(gDialogHandle, "lstItemPrice")
	g_lblTotal = Dialog_GetControl(gDialogHandle, "lblTotalCost")

	g_totalPrice = 0

	ListBox_RemoveAllItems(g_lbName)
	ListBox_RemoveAllItems(g_lbQty)
	ListBox_RemoveAllItems(g_lbPrice)

	for i,v in ipairs(g_items) do
		KEP_Log("DISPLAYING INFO FOR  " .. v["GLID"] .. " with price " .. tostring(v["price"]))
		ListBox_AddItem(g_lbName, v["name"], v["GLID"])
		ListBox_AddItem(g_lbQty, v["quantity"], v["GLID"])
		ListBox_AddItem(g_lbPrice, v["price"], v["GLID"])
		g_totalPrice = g_totalPrice + v["price"] * v["quantity"]
	end

	Static_SetText(g_lblTotal, "Total: " .. g_totalPrice)
end

function Dialog_OnDestroy(dialogHandle)

	-- DRF - Added
	if (g_retryPaste == true) then
		KEP_EventCreateAndQueue( RETRY_PASTE_EVENT)
	end

	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnPurchase_OnButtonClicked( buttonHandle )
	purchaseItems(g_inventoryType)
end

function getPurchaseInfo()
	local glids = ""

	for i,v in ipairs(g_items) do
		glids = glids .. v["GLID"] .. ","
	end

	if glids == "" then
		KEP_MessageBox("No items to purchase")
		return
	end
	glids = string.sub(glids, 1, -2)
	url = GameGlobals.WEB_SITE_PREFIX.."kgp/instantBuy.aspx?operation=info&items=" .. glids
	makeWebCall( url, WF.INSTANTBUY_COPYITEMS)
	g_parse = PARSE_INFO
end

function purchaseItems(inventoryType)
	g_purchaseMenu = MenuOpen("PurchaseCreditsRewards.xml")

	local ev = KEP_EventCreate( ITEM_INFO_EVENT )
	KEP_EventEncodeNumber( ev, inventoryType)

	local num_items = #g_items
	KEP_EventEncodeNumber(ev, num_items) -- num items

	for i,v in ipairs(g_items) do
		KEP_EventEncodeNumber( ev, v["GLID"])
		KEP_EventEncodeNumber( ev, v["quantity"])
		KEP_EventEncodeString( ev, v["name"])
		KEP_EventEncodeNumber( ev, v["price"])
	end

	KEP_EventQueue( ev )
end

function purchaseGroupItemsHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local num_items = KEP_EventDecodeNumber(event)
	g_parse = PARSE_NONE
	g_items = {}
	for i = 1, num_items do
		glid = KEP_EventDecodeNumber(event)
		qty = KEP_EventDecodeNumber(event)
		local item = {}
		item["GLID"] = glid
		item["quantity"] = qty
		table.insert(g_items, item)
	end
	getPurchaseInfo()
end

function itemPurchasedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local numItems = KEP_EventDecodeNumber( event )
	local glids = {}
	for i = 1, numItems do
		local  glid = KEP_EventDecodeNumber( event )
		table.insert(glids, glid)
	end
	local iGlid = 0
	if numItems == 1 then
		iGlid = glids[1]
	end
	
	g_retryPaste = true
	MenuCloseThis()
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.INSTANTBUY_COPYITEMS then
		xml = KEP_EventDecodeString(event)
		if g_parse == PARSE_PURCHASE then
			ParsePurchaseResults(xml)
		elseif g_parse == PARSE_INFO then
			ParseInfoResults(xml)
		end
	end
end

function ParsePurchaseResults( browserString )
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(browserString, "<ReturnCode>(%d+)</ReturnCode>")
	if result == "0" then
		g_retryPaste = true
		MenuCloseThis()
	end
end

function ParseInfoResults( browserString )
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(browserString, "<ReturnCode>(%d+)</ReturnCode>")
	if result == "0" then
		s, e, numRec = string.find(browserString, "<NumberRecords>(%d+)</NumberRecords>", e)
		numRec = tonumber(numRec)
		if numRec > 0 then
			for i = 1, numRec do
				s, e, globalID = string.find(browserString, "<global_id>(%d+)</global_id>", e)

				globalID = tonumber(globalID)
				s, e, name = string.find(browserString, "<name>(.-)</name>", e)
				s, e, price = string.find(browserString, "<market_cost>(%d+)</market_cost>", e) --needs to be changed to WebPrice
				price = tonumber(price)

				s, e, inventoryType = string.find(browserString, "<inventory_type>(%d+)</inventory_type>", e)
				inventoryType = tonumber(inventoryType)
				if inventoryType ~= GameGlobals.IT_BOTH then
					g_inventoryType = inventoryType
				end

				s, e, active = string.find(browserString, "<item_active>(%d+)</item_active>", e)
				active = tonumber(active)
				if active == 0 then
					KEP_MessageBox("You are missing an item that cannot be purchased. Copying cannot continue.")
					MenuCloseThis()
					return
				end

				for i,v in ipairs(g_items) do

					if globalID == v["GLID"] then
						v["name"] = name
						v["price"] = price
					end
				end
			end
		end
	end
	if g_inventoryType == GameGlobals.IT_NORMAL then
		KEP_MessageBox("Some of the items listed can only be purchased with Credits. Credits will automatically be used when purchasing.")
	elseif g_inventoryType == GameGlobals.IT_GIFT then
		KEP_MessageBox("Some of the items listed can only be purchased with Rewards. Rewards will automatically be used when purchasing.")
	end
	populateList()
end
