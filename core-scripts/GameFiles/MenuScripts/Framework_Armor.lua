--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Menu.lua - Kaneva Menu Script Template
-- 
-- Template script for creating a new Kaneva client menu.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_DragDropHelper.lua")
dofile("Framework_ArmorHelper.lua")
dofile("Framework_InventoryHelper.lua")

local TOOLTIP_TABLE = {header = "name"}

local DURABILITY_BAR_WIDTH = 81


local armorHidden = 0
local m_playerName
local m_removeItem = {}
local m_removeIndex = 0
local m_requestHandle = nil
local m_overButton = false
local m_validArmor = false
local m_dropItem = {}

g_playerInventory = {}

local getDurabilityColor -- (durabilityPercentage) Gets the durability color by percentage

-- When the menu is created
function onCreate()
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_ARMORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_ARMORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCustomDrop", "DRAG_DROP_CUSTOM_DROP", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onReturnElement", "DRAG_DROP_RETURN_ELEMENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("startDragDrop", "DRAG_DROP_START", KEP.HIGH_PRIO)
	-- Register handler for server-to-client events

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()	
	InventoryHelper.initializeInventory("g_playerInventory", false, ARMOR_SLOTS)
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")
	
	requestInventory(ARMORY_PID)
	
	DragDrop.initializeDragDrop(4, 1, 0, ARMORY_PID)

	m_playerName 	 = KEP_GetLoginName()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		local ok
		ok, armorHidden =  KEP_ConfigGetNumber(config, "HideArmor"..tostring(m_playerName), 0)
	end
	hideBox = Dialog_GetCheckBox(gDialogHandle, "chkHideArmor")
	CheckBox_SetChecked( hideBox, armorHidden )
	Control_SetEnabled(gHandles["btnItemDrop1"], false)
end

function onDestroy()
	InventoryHelper.destroy()
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	local inventory = g_playerInventory
	if inventory[updateIndex] then
		inventory[updateIndex] = updateItem
		updateInventory(inventory)
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local inventory = decompileInventory(tEvent)
	updateInventory(inventory)
end

function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	m_removeItem = {}
	if success  then
		removeItem(m_removeIndex, ARMORY_PID)
		m_removeIndex = 0
		updateDirty()
	end
end

function DragDrop.buttonOnMouseEnterHandler(btnHandle)
	setRequestHandle(btnHandle)
	local requestEvent = KEP_EventCreate("DRAG_DROP_REQUEST_ELEMENT")
	KEP_EventSetFilter(requestEvent, 4)
	KEP_EventQueue(requestEvent)
	
	m_overButton = true
end

function DragDrop.buttonOnMouseLeaveHandler(btnHandle)

	m_overButton = false
	DragDrop.buttonOnMouseLeave(btnHandle)
end

function startDragDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	Dialog_MoveControlToFront(gDialogHandle, gHandles["btnItemDrop1"])
	Control_SetEnabled(gHandles["btnItemDrop1"], true)
end

function onCustomDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	Dialog_MoveControlToBack(gDialogHandle, gHandles["btnItemDrop1"])
	Control_SetEnabled(gHandles["btnItemDrop1"], false)

	local index = KEP_EventDecodeNumber(tEvent)
	local item = deepCopy(decompileInventoryItem(tEvent))
	local menuPID = KEP_EventDecodeNumber(tEvent)
	local originalIndex = KEP_EventDecodeNumber(tEvent)
	if m_validArmor == true  and m_overButton == true then
		
		local armorSlot = -1
		for i,v in pairs(ARMOR_SLOT_TYPES) do
			if v == item.properties.slotType then
				armorSlot = i
			end
		end
		if armorSlot ~= -1 then
			--g_playerInventory = inventory
			local replaceItem = g_playerInventory[armorSlot]
			updateItem(originalIndex, menuPID, replaceItem)
			updateItem(armorSlot, ARMORY_PID, item)
			updateDirty()
		end
	end
	m_validArmor = false
	m_overButton = false
end

-- Returned from drag drop handler
function onReturnElement(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	--log("--- onReturnElement")

	local elementExists = KEP_EventDecodeString(tEvent) == "true"

	if elementExists and m_requestHandle and m_overButton then
		local dragItem = KEP_EventDecodeString(tEvent)
		dragItem = Events.decode(dragItem)
		
		local canDrop = false
		if dragItem.properties.itemType == "armor" then
			canDrop = true
			m_validArmor = true
			DragDrop.buttonOnMouseEnter(m_requestHandle, canDrop)
			return
		end
	elseif m_requestHandle then
			DragDrop.buttonOnMouseEnter(m_requestHandle)
	end

	m_requestHandle = nil
end

function setRequestHandle(requestHandle)
	--log("--- setRequestHandle")
	m_requestHandle = requestHandle

end


-- Called when the inventory is updated
function updateInventory(inventory)
	if inventory then
		g_playerInventory = inventory
	end
	
	local armorRating = 0
	for i=1,#g_playerInventory and ARMOR_SLOTS do
		local item = g_playerInventory[i]
		if item.instanceData and item.instanceData.levelMultiplier and (item.instanceData.durability > 0) then
			armorRating = armorRating + item.properties.armorRating
		end
		
		local element
		if item.count <= 0 then
			item.hide = true
			Control_SetVisible(gHandles["imgItemLevel" .. i], false)
			
			Control_SetVisible(gHandles["imgDurabilityBar" .. i], false)
			Control_SetVisible(gHandles["imgDurabilityBG" .. i], false)
			Control_SetVisible(gHandles["imgDurabilityGradiant" .. i], false)
			
			Control_SetVisible(gHandles["imgItemBroken" .. i], false)
			Control_SetVisible(gHandles["imgItemOverlay" .. i], false)
		else
			Control_SetVisible(gHandles["imgItemLevel" .. i], true)
			element = Image_GetDisplayElement(gHandles["imgItemLevel" .. i])
			local itemLevel = item.properties.level
			local rarityOffset = ITEM_LEVEL_RARITY_OFFSET[item.properties.rarity] or 0
			Element_SetCoords(element, ITEM_LEVEL_COORDS[itemLevel].left + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].top, ITEM_LEVEL_COORDS[itemLevel].right + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].bottom)
			
			-- Set durability bar
			local durability = item.instanceData.durability
			if durability then
				local durabilityType = item.properties.durabilityType or "repairable"
				local durabilityPercentage = 0
				if durabilityType == "limited" then
					durabilityPercentage = math.min((durability/DURABILITY_BY_LEVEL[itemLevel]), 1)
				elseif durabilityType == "indestructible" then
					durabilityPercentage = 1
				else
					durabilityPercentage = math.min((durability/DEFAULT_MAX_DURABILITY), 1)
				end
				
				-- Set bar size
				Control_SetSize(gHandles["imgDurabilityBar" .. i], durabilityPercentage * DURABILITY_BAR_WIDTH, 7)
				
				-- Set bar color
				local durColor = getDurabilityColor(durabilityPercentage)
				Element_SetCoords(Image_GetDisplayElement(gHandles["imgDurabilityBar" .. i]), unpack(durColor))
				
				-- Set bar's visible
				Control_SetVisible(gHandles["imgDurabilityBar" .. i], true)
				Control_SetVisible(gHandles["imgDurabilityBG" .. i], true)
				Control_SetVisible(gHandles["imgDurabilityGradiant" .. i], true)
				
				-- Is the item broken?
				Control_SetVisible(gHandles["imgItemBroken" .. i], durability <= 0)
				Control_SetVisible(gHandles["imgItemOverlay" .. i], durability <= 0)
				
			end
		end

		local rarity = item.properties.rarity or "Common"
		element = Image_GetDisplayElement(gHandles["imgItemBG" .. i])
		Element_SetCoords(element, ITEM_RARITY_BG[rarity].left, ITEM_RARITY_BG[rarity].top, ITEM_RARITY_BG[rarity].right, ITEM_RARITY_BG[rarity].bottom)

		formatTooltips(item)

		item.properties.dropType = ITEM_TYPES.ARMOR
		item.properties.slotType = ARMOR_SLOT_TYPES[i]
		DragDrop.registerItem(i, ARMORY_PID, item)
		
		applyImage(gHandles["imgItem" .. i], item.properties.GLID)
		Control_SetVisible(gHandles["imgEmptyItem" .. i], item.count == 0)

		
		_G["btnItem" .. tostring(i) .. "_OnRButtonUp"] = function(btnHandle, x, y)
			if item.count > 0 then
				m_removeIndex = i
				local addTable = {}
				table.insert(addTable, item)
				m_removeItem = addTable
				processTransaction({}, addTable)
			end
		end
	end

	InventoryHelper.setNameplates("name", "hide")

	--InventoryHelper.setIcons("GLID")

	--updateArmorBar(armorRating)
end

function updateArmorBar(armorRating)
	local armorBarWidth = 0
	for i=1,#ARMOR_BASE do
		if armorRating >= ARMOR_BASE[i] + ARMOR_RANGE[i] then
			armorBarWidth = armorBarWidth + ARMOR_NOTCH_WIDTH
		else
			armorBarWidth = armorBarWidth + ( armorRating/(ARMOR_BASE[i] + ARMOR_RANGE[i]) * ARMOR_NOTCH_WIDTH )
			break
		end
	end
	
	armorBarWidth = armorBarWidth <= ARMOR_BAR_MAX_WIDTH and armorBarWidth or ARMOR_BAR_MAX_WIDTH
	
	Control_SetSize(gHandles["imgArmorBarFill"] , armorBarWidth, ARMOR_BAR_HEIGHT)
	
	local armorDisplay = formatCommaValue(armorRating)
	Static_SetText(gHandles["stcArmorRating"], tostring(armorDisplay))
end

function formatTooltips(item)
	if item then
		item.tooltip = {}

		if item.properties.rarity then
			item.tooltip["header"] = {label = item.properties.name,
									  color = COLORS_VALUES[item.properties.rarity]}
		end

		if item.properties.description then
			item.tooltip["body"] = {label = "\"" .. item.properties.description .. "\""}
		end

		if item.properties.slotType and item.properties.armorRating then
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}
			item.tooltip["armorslot"] = {label = tostring(item.properties.slotType:gsub("^%l", string.upper))}

			local armorRating
			if item.properties.bonusHealth then
				armorRating = item.properties.armorRating * item.properties.bonusHealth
			else
				armorRating = item.properties.armorRating * 3
			end

			item.tooltip["armorrating"] = {label = tostring(armorRating)}
		end
		
		if item.instanceData and item.instanceData.durability then
			local durability = item.instanceData.durability
			local durabilityType = item.properties.durabilityType or "repairable"
			local itemLevel = item.properties.level

			if durabilityType == "limited" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage),
												durability = durabilityPercentage}

			elseif durabilityType == "indestructible" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = "100%",
										  		durability = 100}
			else
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage).."%",
										 		durability = durabilityPercentage}
			end
		end

		item.tooltip["loot"] = {label = "To remove - drag to backpack.", type = item.properties.itemType}
	end
end

-- Add comma to separate thousands
function formatCommaValue(value)
  	local formatted = value
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end

-- Returns the color for the durability bar based on percentage passed in
getDurabilityColor = function(durabilityPercentage)
	-- Green bar
	if durabilityPercentage >= .5 then
		return DURABILITY_COLORS.GREEN
	-- Return yellow
	elseif durabilityPercentage >= .15 then
		return DURABILITY_COLORS.YELLOW
	-- Return red
	else
		return DURABILITY_COLORS.RED
	end
end

function chkHideArmor_OnCheckBoxChanged( checkboxHandle, checked )
	--Save the new config
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber( config, "HideArmor"..tostring(m_playerName), checked )
		KEP_ConfigSave( config )
		Events.sendEvent("UPDATE_PLAYER_HIDE_ARMOR", {hideArmor = checked})
	end

	local input = {reportName = "OnUseHideArmor", reportParams = {username = m_playerName, hideArmorChecked = checked}}
	Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {input = input, callType = "METRIC_GENERIC"})
end