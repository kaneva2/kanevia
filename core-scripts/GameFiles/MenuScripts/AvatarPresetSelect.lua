--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

TITLE_HEIGHT = 30

g_selectedIndex = nil

g_gender = nil

g_avatar_configs = {}

g_male_slots = {}

g_female_slots = {}

evData = {}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "HomePresetSelectEventHandler", "HomePresetSelectEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "AvatarSelectionEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "HomePresetSelectEvent", KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	Control_SetVisible(gHandles["btnNext"], true)
	deselectAllConfigs()

	g_gender = KEP_GetLoginGender()
	math.randomseed(KEP_CurrentTime())
	populateConfigs()

	MenuAlwaysBringToFrontThis()

	-- Record new user funnel progression
	makeWebCall(WEBCALL_NEW_USER_FUNNEL_PROGRESSION .. "entered_avatar_select", 0)
end

function Dialog_OnDestroy(dialogHandle)
	markGuideClosedInConfig()
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnMouseMove(dialogHandle, x, y)

	--Make sure we have the interface we need (stop errors in editor)
	if Control_ContainsPoint ~= nil then
		if Control_ContainsPoint(Dialog_GetControl(gDialogHandle, "btnNext"), x, y) ~= 0
		and Control_GetVisible(Dialog_GetControl(gDialogHandle, "btnNext")) == 1
		and g_selectedIndex == nil then
			Control_SetVisible(gHandles["stcMessage"], true)
			Control_SetVisible(gHandles["stcMessage2"], false)
		else
			Control_SetVisible(gHandles["stcMessage"], false)
			Control_SetVisible(gHandles["stcMessage2"], true)
		end
	end
end

function addNewConfig(name, gender, templateId, textureName, textureCoords)
	local avatarConfig = {}
	avatarConfig.id = templateId
	avatarConfig.name = name
	avatarConfig.textureName = textureName
	avatarConfig.textureCoords = textureCoords
	avatarConfig.gender = gender
	table.insert(g_avatar_configs, avatarConfig)
end

function addNewSlot(gender, name, configList)
	local configSlot = {}
	configSlot.name = name
	configSlot.gender = gender
	configSlot.configList = configList
	configSlot.chosenConfig = -1
	if gender == "M" then
		table.insert(g_male_slots, configSlot)
	else
		table.insert(g_female_slots, configSlot)
	end
end

function getConfig(templateId)
	for i,v in ipairs(g_avatar_configs) do
		if v.id == templateId then
			return v
		end
	end
	return nil
end

function getSlot(gender, index)
	local slots = g_female_slots
	if gender == "M" then
		slots = g_male_slots
	end
	return slots[index]
end

function populateConfigs()
	--female configs {name, gender, templateId, textureName, {textureCoords}}
	--texture 1, row 1
	addNewConfig("f_avatar_108", "F", 1021, "avatarPreconfig_1.tga", {0, 0, 110, 300})
	addNewConfig("f_avatar_103B", "F", 1020, "avatarPreconfig_1.tga", {110, 0, 220, 300})
	addNewConfig("f_avatar_38",  "F", 1019, "avatarPreconfig_1.tga", {220, 0, 330, 300})
	--addNewConfig("f_avatar_89", "F", 1030, "avatarPreconfig_1.tga", {330, 0, 440, 300})
	addNewConfig("f_avatar_85C", "F", 1028, "avatarPreconfig_1.tga", {440, 0, 550, 300})
	addNewConfig("f_avatar_85E", "F", 1029, "avatarPreconfig_1.tga", {550, 0, 660, 300})
	addNewConfig("f_avatar_103", "F", 1025, "avatarPreconfig_1.tga", {660, 0, 770, 300})
	addNewConfig("f_avatar_95A", "F", 1024, "avatarPreconfig_1.tga", {770, 0, 880, 300})
	addNewConfig("f_avatar_85G", "F", 1023, "avatarPreconfig_1.tga", {880, 0, 990, 300})

	--texture 1, row 2
	addNewConfig("f_avatar_70",  "F", 1022, "avatarPreconfig_1.tga", {0, 300, 110, 600})
	addNewConfig("f_avatar_103D", "F", 1027, "avatarPreconfig_1.tga", {110, 300, 220, 600})
	addNewConfig("f_avatar_7",   "F", 1026, "avatarPreconfig_1.tga", {220, 300, 330, 600})
	addNewConfig("f_avatar_95B", "F", 1018, "avatarPreconfig_1.tga", {330, 300, 440, 600})
	addNewConfig("f_avatar_85B", "F", 1016, "avatarPreconfig_1.tga", {440, 300, 550, 600})
	addNewConfig("f_avatar_85F", "F", 1017, "avatarPreconfig_1.tga", {550, 300, 660, 600})
	addNewConfig("f_avatar_110", "F", 1015, "avatarPreconfig_1.tga", {660, 300, 770, 600})
	addNewConfig("f_avatar_106", "F", 1014, "avatarPreconfig_1.tga", {770, 300, 880, 600})
	addNewConfig("f_avatar_85D", "F", 1013, "avatarPreconfig_1.tga", {880, 300, 990, 600})

	--texture 1, row 3
	addNewConfig("f_avatar_17", "F", 1012, "avatarPreconfig_1.tga", {0, 600, 110, 900})
	addNewConfig("f_avatar_85A", "F", 1031, "avatarPreconfig_1.tga", {110, 600, 220, 900})

	--male configs {name, gender, templateId, textureName, {textureCoords}}
	addNewConfig("m_avatar_75", "M", 1037, "avatarPreconfig_1.tga", {220, 600, 330, 900})
	addNewConfig("m_avatar_69", "M", 1036, "avatarPreconfig_1.tga", {330, 600, 440, 900})
	addNewConfig("m_avatar_14", "M", 1035, "avatarPreconfig_1.tga", {440, 600, 550, 900})
	addNewConfig("m_avatar_76", "M", 1040, "avatarPreconfig_1.tga", {550, 600, 660, 900})
	addNewConfig("m_avatar_90", "M", 1041, "avatarPreconfig_1.tga", {660, 600, 770, 900})
	addNewConfig("m_avatar_11", "M", 1038, "avatarPreconfig_1.tga", {770, 600, 880, 900})
	addNewConfig("m_avatar_12", "M", 1039, "avatarPreconfig_1.tga", {880, 600, 990, 900})

	--texture 2, row 1
	addNewConfig("m_avatar_74", "M", 1034, "avatarPreconfig_2.tga", {0, 0, 110, 300})
	addNewConfig("m_avatar_96A", "M", 1033, "avatarPreconfig_2.tga", {110, 0, 220, 300})
	addNewConfig("m_avatar_13", "M", 1032, "avatarPreconfig_2.tga", {220, 0, 330, 300})
	addNewConfig("m_avatar_10", "M", 1042, "avatarPreconfig_2.tga", {330, 0, 440, 300})

	--add female slots
	addNewSlot("F", "PREP WHITE",  {1019,1020,1021})
	addNewSlot("F", "URBAN BLACK", {1028,1029})
	addNewSlot("F", "ROCKER WHITE", {1022,1023,1024,1025})
	addNewSlot("F", "SEXY WHITE",  {1026,1027})
	addNewSlot("F", "PREP BLACK",  {1016,1017,1018})
	addNewSlot("F", "GOTH WHITE",  {1012,1013,1014,1015})
	addNewSlot("F", "URBAN WHITE", {1031})

	--add male slots
	addNewSlot("M", "PREP WHITE",  {1035,1036,1037})
	addNewSlot("M", "URBAN BLACK", {1040,1041})
	addNewSlot("M", "ROCKER WHITE", {1038})
	addNewSlot("M", "SEXY WHITE",  {1039})
	addNewSlot("M", "PREP BLACK",  {1034})
	addNewSlot("M", "GOTH WHITE",  {1032,1033})
	addNewSlot("M", "URBAN WHITE", {1042})

	local slots = g_female_slots
	if g_gender == "M" then
		slots = g_male_slots
	end

	for i,v in ipairs(slots) do
		local configList = v.configList
		if #configList > 0 then
			local rand = math.random(#configList)
			local randomConfig = configList[rand]
			v.chosenConfig = randomConfig
			local config = getConfig(randomConfig)
			local element = Image_GetDisplayElement(gHandles["imgAvatarPreset_"..i])
			Element_AddTexture(element, gDialogHandle, config.textureName)
			Element_SetCoords(element, config.textureCoords[1], config.textureCoords[2], config.textureCoords[3], config.textureCoords[4])
		end
	end
end

function deselectAllConfigs()
	for i=1,7 do
		Control_SetVisible(gHandles["imgAvatarPresetSelectedFrame_" .. i], false)
	end
	Control_SetEnabled(gHandles["btnNext"], false)
	g_selectedIndex = nil
end

function markGuideClosedInConfig()
	g_userName = KEP_GetLoginName()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetNumber(config, "GuideClosed"..g_userName, 0)
		KEP_ConfigSave( config )
	end
end

function selectPreset(index)
	deselectAllConfigs()
	Control_SetVisible(gHandles["imgAvatarPresetSelectedFrame_" .. index], true)

	local slot = getSlot(g_gender, index)
	g_selectedIndex = slot.chosenConfig
	Control_SetEnabled(gHandles["btnNext"], true)
end

function btnNext_OnButtonClicked( buttonHandle )
	
	if (g_selectedIndex ~= nil) and (evData.aptGlid ~= nil) and (evData.aptId ~= nil) then

		-- Create Avatar Selection Event Data (avatarId, aptGlid & aptId from HomePresetSelectEventHandler)
		evData.id = g_selectedIndex -- avatar id

		-- Send AvatarSelectionEvent
		Log("NextClicked: Sending AvatarSelectionEvent avatarId="..evData.id.." aptGlid="..evData.aptGlid.." aptId="..evData.aptId)
		local ev = KEP_EventCreate( "AvatarSelectionEvent"  )
		KEP_EventSetObjectId(ev, evData.id)
		KEP_EventEncodeNumber(ev, evData.aptGlid)
		KEP_EventEncodeNumber(ev, evData.aptId)
		KEP_EventAddToServer(ev)
		KEP_EventQueue( ev)

		sendNudge()

		-- Record new user funnel progression
		makeWebCall(WEBCALL_NEW_USER_FUNNEL_PROGRESSION .. "completed_avatar_select", 0)
		
		ProgressOptFade("Rezone", PROGRESS_OPT_FADE_MAX_NOW)
		ProgressTag("Rezone", GetHomeUrl())
		ProgressOpen("Rezone")

		-- Avatar Selected - All Done
		MenuCloseThis()
	end
end

--[[ DRF - This function handles the HomePresetSelectEvent coming from the HomePresetSelect menu selecting the avatar starter home.]]
function HomePresetSelectEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	evData.aptGlid = KEP_EventDecodeNumber(event) -- starter home glid
	evData.aptId = KEP_EventDecodeNumber(event) -- starter home id
	MenuClose("HomePresetSelect.xml");
end

function sendNudge()
end

function btnAvatarPreset_1_OnButtonClicked( buttonHandle )
	selectPreset(1)
end

function btnAvatarPreset_2_OnButtonClicked( buttonHandle )
	selectPreset(2)
end

function btnAvatarPreset_3_OnButtonClicked( buttonHandle )
	selectPreset(3)
end

function btnAvatarPreset_4_OnButtonClicked( buttonHandle )
	selectPreset(4)
end

function btnAvatarPreset_5_OnButtonClicked( buttonHandle )
	selectPreset(5)
end

function btnAvatarPreset_6_OnButtonClicked( buttonHandle )
	selectPreset(6)
end

function btnAvatarPreset_7_OnButtonClicked( buttonHandle )
	selectPreset(7)
end
