--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

g_selectionSetsMinimized = true

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler("selectionSetUpdateHandler", "SelectionSetUpdateEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	local ev = KEP_EventCreate("SelectionSetUpdateEvent")
	KEP_EventSetFilter(ev, 2)
	KEP_EventQueue(ev)

	-- setup look of listbox scrollbars
	local lbh = Dialog_GetListBox(dialogHandle, "ListBoxMain")
	if lbh ~= 0 then
		local sbh = ListBox_GetScrollBar(lbh)
		if sbh ~= 0 then
			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function selectionSetUpdateHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == 1 then
		numSets = KEP_EventDecodeNumber(event)
		local listbox = Dialog_GetControl(gDialogHandle, "ListBoxMain")
		ListBox_RemoveAllItems(listbox)
		ListBox_ClearSelection(listbox)
		for i = 1, numSets do
			local selection_name = KEP_EventDecodeString(event)
			ListBox_AddItem(listbox, selection_name, 0)
		end
	end
end

function ListBoxMain_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if bMouseDown == 1 or bFromKeyboard == 1 then
		local selectionName = ListBox_GetSelectedItemText(listBoxHandle)
		local editBox = Dialog_GetControl(gDialogHandle, "txtUsername")
		EditBox_SetText(editBox, selectionName, false)
		local ev = KEP_EventCreate("SelectionSetUpdateEvent")
		KEP_EventSetFilter(ev, 3)
		KEP_EventEncodeString(ev, selectionName)
		KEP_EventQueue(ev)
	end
end

function btnChooseSelectionSet_OnButtonClicked(buttonHandle)
	local listbox = Dialog_GetControl(gDialogHandle, "ListBoxMain")
	if g_selectionSetsMinimized == true then
		g_selectionSetsMinimized = false
		Control_SetEnabled(listbox, true)
		Control_SetVisible(listbox, true)
	else
		g_selectionSetsMinimized = true
		Control_SetEnabled(listbox, false)
		Control_SetVisible(listbox, false)
	end
end

function saveSelectionSet()
	local ev = KEP_EventCreate("SelectionSetUpdateEvent")
	KEP_EventSetFilter(ev, 4)
	local editBox = Dialog_GetControl(gDialogHandle, "txtUsername")
	local selectionName = EditBox_GetText(editBox)
	KEP_EventEncodeString(ev, selectionName)
	KEP_EventQueue(ev)
end

function btnNewGroup_OnButtonClicked(buttonHandle)
	saveSelectionSet()
end

function btnSave_OnButtonClicked(buttonHandle)
	saveSelectionSet()
end

function btnDelete_OnButtonClicked(buttonHandle)
	local ev = KEP_EventCreate("SelectionSetUpdateEvent")
	KEP_EventSetFilter(ev, 5)
	local editBox = Dialog_GetControl(gDialogHandle, "txtUsername")
	local selectionName = EditBox_GetText(editBox)
	KEP_EventEncodeString(ev, selectionName)
	KEP_EventQueue(ev)
end
