--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister("BuildKeyPressedEvent", KEP.MED_PRIO )
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "clientResizeEventHandler", "ClientResizeEvent", KEP.LOW_PRIO )
end 

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	updateLoc()
	updateBuildHelpStatus(1) -- True
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
	updateBuildHelpStatus(0) -- False
end

function updateLoc()
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	local dialogSize = MenuGetSizeThis()

	MenuSetLocationThis(screenWidth - dialogSize.width -3 , screenHeight - dialogSize.height - 150)
end 

function updateBuildHelpStatus(menuActive)
	local event = KEP_EventCreate( "FRAMEWORK_ON_BUILD_HELPER_CHANGE" )
	-- KEP_EventSetFilter(event, 4)
	KEP_EventEncodeNumber(event, menuActive)
	KEP_EventQueue(event)

end

function btnHelp_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser("http://docs.kaneva.com/mediawiki/index.php/Build_Mode")
end

-- DRF - Handled in Selection.lua
function sendKeyPressedEvent(key)
	local ev = KEP_EventCreate("BuildKeyPressedEvent")
	KEP_EventSetFilter(ev, key)
	KEP_EventQueue(ev)
end

function btnUp_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.UP)
end

function btnDown_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.DOWN)
end

function btnLeft_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.LEFT)
end

function btnRight_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.RIGHT)
end

function btnHome_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.HOME)
end

function btnEnd_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.END)
end

function btnInsert_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.INSERT)
end

function btnDelete_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.DEL)
end

-- Counter-Clockwise = PAGE_UP
function btnCCW_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.PAGE_UP)
end

-- Clockwise = PAGE_DOWN
function btnCW_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.PAGE_DOWN)
end

function clientResizeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	updateLoc()
end

