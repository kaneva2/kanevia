--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

GameZoneInfo = {}
GameZoneInfo.__index = GameZoneInfo

function GameZoneInfo.create(id, name, url)
	local game = {}             -- our new object
	setmetatable(game,GameZoneInfo)
	game.id = id
	game.name = name
	game.url = url
	return game
end

GameInfo = {}
GameInfo.__index = GameInfo

function GameInfo.create(id, name)
	local game = {}             -- our new object
	setmetatable(game,GameInfo)
	game.name = name
	game.id = id
	return game
end

GameInstanceInfo = {}
GameInstanceInfo.__index = GameInstanceInfo

function GameInstanceInfo.create(id, numplayers)
	local gameInstance = {}             -- our new object
	setmetatable(gameInstance,GameInstanceInfo)
	gameInstance.id = id
	gameInstance.players = numplayers
	return gameInstance
end

gameZones = {}
games = {}
instances = {}

--Filters
GSE_GAMES_INFO                  = 1
GSE_INSTANCES_INFO              = 2
GSE_CREATE_NEW_INSTANCE         = 3
GSE_JOIN_INSTANCE               = 4
GSE_EDIT_GAME             		= 5

GAME_EVENT_START = 1
GAME_EVENT_SPAWN_INFO = 2

function requestAvailableGameZones()

	url = GameGlobals.WEB_SITE_PREFIX .. "kgp/gamelist.aspx"
	makeWebCall( url, WF.GAMES_LIST, 0, 0)
end

function requestAvailableGames()
	local event = KEP_EventCreate( "GameSignUpEvent")
	KEP_EventSetFilter(event, GSE_GAMES_INFO)
	KEP_EventAddToServer(event)
	KEP_EventQueue( event)
end

function requestInstancesOfGame(gameId)
	local event = KEP_EventCreate( "GameSignUpEvent")
	KEP_EventSetFilter(event, GSE_INSTANCES_INFO)
	KEP_EventSetObjectId(event, gameId)
	KEP_EventAddToServer(event)
	KEP_EventQueue( event)
end

function createNewGameInstance(gameId)
	local event = KEP_EventCreate( "GameSignUpEvent")
	KEP_EventSetFilter(event, GSE_CREATE_NEW_INSTANCE)
	KEP_EventSetObjectId(event, gameId)
	KEP_EventAddToServer(event)
	KEP_EventQueue( event)
end

function joinGameInstance(gameId, instanceId)
	local event = KEP_EventCreate( "GameSignUpEvent")
	KEP_EventSetFilter(event, GSE_JOIN_INSTANCE)
	KEP_EventSetObjectId(event, gameId)
	KEP_EventEncodeNumber(event, instanceId)
	KEP_EventAddToServer(event)
	KEP_EventQueue( event)
end

function editGame(gameId)
	local event = KEP_EventCreate( "GameSignUpEvent")
	KEP_EventSetFilter(event, GSE_EDIT_GAME)
	KEP_EventSetObjectId(event, gameId)
	KEP_EventAddToServer(event)
	KEP_EventQueue( event)
end

function gotoGameLobby()
	local ev = KEP_EventCreate( KEP.GOTOPLACE_EVENT_NAME )
	KEP_EventSetFilter( ev, KEP.GOTO_ZONE ) -- We are going to a zone
	KEP_EventEncodeNumber( ev, GameGlobals.ZONE_INDEX_ARENALOBBY )
	KEP_EventEncodeNumber( ev, nil  )
	KEP_EventEncodeNumber( ev, -1 ) -- selected character is unknown now
	KEP_EventAddToServer( ev )
	KEP_EventQueue( ev )
end

function GameZoneHelper_scriptClientEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ScriptClientEventTypes.PlayerSendEvent then
		args = KEP_EventDecodeNumber(event)
	end
	return KEP.EPR_OK
end

function GameZoneHelper_gameSignUpEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == GSE_GAMES_INFO then
		numGames = KEP_EventDecodeNumber(event)
		for i=1,numGames do
			gameName = KEP_EventDecodeString(event)
			gameId = KEP_EventDecodeNumber(event)
			local gameInfo = GameInfo.create(gameId, gameName)
			table.insert(games, gameInfo)
		end
	elseif filter == GSE_INSTANCES_INFO then
		g_gameId = KEP_EventDecodeNumber(event)
		numGames = KEP_EventDecodeNumber(event)
		for i=1,numGames do
			gameId = KEP_EventDecodeNumber(event)
			numPlayers = KEP_EventDecodeNumber(event)
			local gameInstanceInfo = GameInstanceInfo.create(gameId, numPlayers)
			table.insert(instances, gameInstanceInfo)
		end
	end
end

function GameZoneHelper_InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "GameSignUpEvent", KEP.MED_PRIO )
end

function GameZoneHelper_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "GameZoneHelper_scriptClientEventHandler", "ScriptClientEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "GameZoneHelper_gameSignUpEventHandler", "GameSignUpEvent", KEP.MED_PRIO )
end
