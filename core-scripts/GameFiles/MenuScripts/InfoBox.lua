--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "InfoBoxEventHandler", "InfoBoxEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "InfoBoxEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

-- InfoBoxEvent { message, title, width, height, flushLeft }
function InfoBoxEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Display Sent Message
	local message = KEP_EventDecodeString( event ) or ""
	sh = Dialog_GetStatic( gDialogHandle, "lblMessage" )
	Static_SetText( sh, message )

	-- Display Sent Title
	local title = KEP_EventDecodeString( event ) or "Message"
	sh = Dialog_GetStatic( gDialogHandle, "lblTitle" )
	Static_SetText( sh, title )

	-- Set Size (suggested minimum dimensions = 200W x 250H)
	local width = KEP_EventDecodeNumber( event ) or -1
	local height = KEP_EventDecodeNumber( event ) or -1
	if width~=-1 or height~=-1 then
		local ctl, ctlW, ctlH, ctlX, ctlY
		local borderPadding = 10
		local titleBarHeight = 30
		local size = MenuGetSizeThis()
		if (width == -1) then width = size.width end
		if (height == -1) then height = size.height end

		-- 1. set dialog size
		MenuSetSizeThis( width, height )

		-- 2. adjust slice #2 ("mUpperCenter_Slice")
		ctl = Dialog_GetControl( gDialogHandle, "mUpperCenter_Slice" )
		ctlH = Control_GetHeight( ctl )
		Control_SetSize( ctl, (width - (borderPadding * 2)), ctlH )

		-- 3. adjust slide #3 ("mUpperRight_Slice")
		ctl = Dialog_GetControl( gDialogHandle, "mUpperRight_Slice" )
		ctlY = Control_GetLocationY( ctl )
		Control_SetLocation( ctl, (width - borderPadding), ctlY )

		-- 4. adjust slice #4 ("mMiddleLeft_Slice")
		ctl = Dialog_GetControl( gDialogHandle, "mMiddleLeft_Slice" )
		ctlW = Control_GetWidth( ctl )
		Control_SetSize( ctl, ctlW, (height - (borderPadding + titleBarHeight))  )

		-- 5. adjust slice #5 ("mMiddleCenter_Slice")
		ctl = Dialog_GetControl( gDialogHandle, "mMiddleCenter_Slice" )
		Control_SetSize( ctl, (width - (borderPadding * 2)), (height - (borderPadding + titleBarHeight))  )

		-- 6. adjust slice #6 ("mMiddleRight_Slice")
		ctl = Dialog_GetControl( gDialogHandle, "mMiddleRight_Slice" )
		ctlW = Control_GetWidth( ctl )
		Control_SetSize( ctl, ctlW, (height - (borderPadding + titleBarHeight)) )
		ctlY = Control_GetLocationY( ctl )
		Control_SetLocation( ctl, (width - borderPadding), ctlY )

		-- 7. adjust slide #7 ("mLowerLeft_Slice")
		ctl = Dialog_GetControl( gDialogHandle, "mLowerLeft_Slice" )
		ctlX = Control_GetLocationX( ctl )
		Control_SetLocation( ctl, ctlX, (height - (borderPadding + titleBarHeight)) )

		-- 8. adjust slide #8 ("mLowerCenter_Slice")
		ctl = Dialog_GetControl( gDialogHandle, "mLowerCenter_Slice" )
		ctlH = Control_GetHeight( ctl )
		Control_SetSize( ctl, (width - (borderPadding * 2)), ctlH )
		ctlX = Control_GetLocationX( ctl )
		Control_SetLocation( ctl, ctlX, (height - (borderPadding + titleBarHeight)) )

		-- 9. adjust slide #9 ("mLowerRight_Slice")
		ctl = Dialog_GetControl( gDialogHandle, "mLowerRight_Slice" )
		Control_SetLocation( ctl, (width - borderPadding), (height - (borderPadding + titleBarHeight)) )

		-- 10. adjust slide #10 ("btnClose")
		ctl = Dialog_GetControl( gDialogHandle, "btnClose" )
		ctlY = Control_GetLocationY( ctl )
		Control_SetLocation( ctl, (width - (borderPadding + 16)), ctlY )

		-- 6. adjust button ("btnOk")
		ctl = Dialog_GetControl( gDialogHandle, "btnOk" )
		Control_SetLocation( ctl, ((width - 75) * 0.5), (height - (borderPadding + titleBarHeight + 18)) )

		-- 7. adjust message ("lblMessage")
		ctl = Dialog_GetControl( gDialogHandle, "lblMessage" )
		Control_SetSize( ctl, (width - borderPadding), (height - ((borderPadding * 4) + titleBarHeight + 18)) )
	end

	-- restore dialog
	MenuSetMinimizedThis(false)
end
