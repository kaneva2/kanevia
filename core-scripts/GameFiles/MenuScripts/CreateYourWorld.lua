--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

-- List of Known templates
-- where "template"
-- template.templateId
-- template.defaultCategoryId
-- template.sortOrder
-- template.status
-- template.name
-- template.imageId
-- template.previewImagePathSmall
-- template.previewImagePathMedium  // image we display
-- template.previewImagePathLarge
-- template.fileName
-- template.featureList             // arrays of strings

g_templateList = {}
g_selectedTemplate = {}
g_formReady = false

gCYW_templateStatus = {}
gCYW_templateStatus.Archived = 0
gCYW_templateStatus.Active = 1
gCYW_templateStatus.Pending = 2

gCYW_textColors = {}
gCYW_textColors.Black = "#FF000000"
gCYW_textColors.Red   = "#FFFD4747"

gCYW_ResyncTimer = 0

gCYW_maxFeatures = 6

gCYW_DisplayMode = {}
gCYW_DisplayMode.SHOW_NONE   = 0
gCYW_DisplayMode.SHOW_MAIN   = 1
gCYW_DisplayMode.SHOW_STATUS = 2
gCYW_DisplayMode.SHOW_VIP	 = 3

gCYW_FeaturePositions = {}
gCYW_MinDescriptionChars = 30
gCYW_MaxDescriptionChars = 70
gCYW_DescriptionWrapDisplacement = 20

gCYW_MaxFeatureChars = 42
gCYW_FeatureWrapDisplacement = 12

gCYW_DropdownDisplacement = 31

-- Default to 5 minute timeout for world creation
gCYW_AsyncTimeout = 300000
gCYW_AsyncTimestamp = nil
gCYW_AsyncToken = nil

-- menu contorl arrays
-- 4 'panels' = [SelectTemplate / NewWorld / Progressbar / Status]
gctrls_Panels = {}

gctrls_Panels["SelectTemplate"] =
{
	"stcSelectTemplate",
	"cbSelectTemplate",
	"lblSelectTemplateDescription",
	"lblSelectTemplateFeatures1",
	"lblSelectTemplateFeatures2",
	"lblSelectTemplateFeatures3",
	"lblSelectTemplateFeatures4",
	"lblSelectTemplateFeatures5",
	"lblSelectTemplateFeatures6",
	"lblSelectTemplateBullet1",
	"lblSelectTemplateBullet2",
	"lblSelectTemplateBullet3",
	"lblSelectTemplateBullet4",
	"lblSelectTemplateBullet5",
	"lblSelectTemplateBullet6",
	"imgSelectTemplate",
}

gctrls_Panels["NewWorld"] =
{
	"stcNewWorldInformation",
	"stcNewWorldName",
	"edNewWorldName",
	"stcNewWorldDescription",
	"edNewWorldDescription",
	"btnCreateWorld",
}

gctrls_Panels["ProgressBar"] =
{
	"UpperLeft_Slice",
	"UpperCenter_Slice",
	"UpperRight_Slice",
	"MiddleLeft_Slice",
	"MiddleCenter_Slice",
	"MiddleRight_Slice",
	"LowerLeft_Slice",
	"LowerCenter_Slice",
	"LowerRight_Slice",
	"pbarStatus",
	"txtStatus",
}

gctrls_Panels["Status"] =
{
	"stcResult",
	"btnHide",
}

gctrls_Panels["VIPUpsell"] =
{
	"imgVIP",
	"stcVIP",
	"btnVIP",
}

gCYW_MAXTIME = 300 -- 5 mins

gCYW_ProgressBar = {}
gCYW_ProgressBar.EMPTY     =   0
gCYW_ProgressBar.COMPLETE  = 475
gCYW_ProgressBar.START_INCREMENT = 25
gCYW_ProgressBar.WAIT_INCREMENT   = (gCYW_ProgressBar.COMPLETE - gCYW_ProgressBar.START_INCREMENT) / gCYW_MAXTIME

gCYW_RequestPending = false
gCYW_RequestPendingMAXTime = gCYW_MAXTIME
gCYW_RequestPendingTimer   = 1   -- 1 sec
gCYW_VIPLink = GameGlobals.WEB_SITE_PREFIX_KANEVA .. "mykaneva/passDetails.aspx?pass=true&passId=74"
gCYW_HasAccessPass = false
gCYW_VIPPassGroupId = 2
gCYW_HasVIPPass = false
gCYW_HideDropdown = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("responseAccessPassInfoHandler", "ResponseAccessPassInfoEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler("CreateWorldStatusCheckEventHandler", "CreateWorldStatusCheckEvent", KEP.MED_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister("RequestAccessPassInfoEvent", KEP.MED_PRIO)
	KEP_EventRegister("ResponseAccessPassInfoEvent", KEP.MED_PRIO)
	KEP_EventRegister("CreateWorldStatusCheckEvent", KEP.MED_PRIO)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate(dialogHandle)
	initalizeForm()
	RequestTemplateList()
	KEP_EventQueue(KEP_EventCreate("RequestAccessPassInfoEvent"))
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function RequestTemplateList()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..GET_TEMPLATE_LIST, WF.TEMPLATE_LIST, 1, 100)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.TEMPLATE_LIST then
		local responseXML = Event_DecodeString(event)
		local code = parseResponseTemplateList(responseXML)
		if (code >= 0) then
			requestTemplateListSuccess()
		else
			requestTemplateListFailed()
		end
	elseif filter == WF.CREATE_WORLD then
		gCYW_RequestPending = false
		local responseXML = Event_DecodeString(event)
		local code, desc, url, token = parseResponseCreateWorld(responseXML)
		if (code >= 0) then
			if (token ~= nil) then
				-- If we get a token back, the world is being created asynchronously.
				-- We need to poll to check the status.
				gCYW_AsyncToken = token
				requestCreateWorldStatusCheck(code, desc, url, true)
			else
				requestCreateWorldSuccess(code, desc, url)
			end
		else
			requestCreateWorldFailed(code, desc, url)
		end
	elseif filter == WF.CREATE_WORLD_STATUS then
		local responseXML = Event_DecodeString(event)
		local code, desc, url, token = parseResponseCreateWorld(responseXML)
		if code == 0 then
			requestCreateWorldSuccess(code, desc, url)
		elseif code == -8 then
			requestCreateWorldStatusCheck(code, desc, url)
		else
			requestCreateWorldFailed(code, desc, url)
		end
	end
end

function textureDownloadHandler()
	-- if the download was delayed then refresh screen
	if (g_selectedTemplate.templateId ~= nil) then
		toogleSelectTemplate(g_selectedTemplate.templateId)
	end
end

function responseAccessPassInfoHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	gCYW_HasAccessPass = (KEP_EventDecodeNumber(event) == 1)
	gCYW_HasVIPPass = (KEP_EventDecodeNumber(event) == 1)
	return KEP.EPR_OK
end

function CreateWorldStatusCheckEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if gCYW_AsyncToken ~= nil then
		local url = GameGlobals.WEB_SITE_PREFIX .. CREATE_WORLD_SUFFIX .. "action=checkCreationStatus&token=" .. gCYW_AsyncToken
		makeWebCall( url, WF.CREATE_WORLD_STATUS)
	end
end

function Dialog_OnRender(dialogHandle, fElapsedTime)

	if (g_formReady == false) then return end

	-- combobox does not trigger event when user expands box and uses arrow keys
	-- for this we resync things here every 2 secs
	if (gCYW_ResyncTimer >=2) then
		gCYW_ResyncTimer = 0
		local currentId = ComboBox_GetSelectedData(gHandles["cbSelectTemplate"])
		if (currentId ~= g_selectedTemplate.templateId) then
			toogleSelectTemplate(currentId)
		end
	else
		gCYW_ResyncTimer =  gCYW_ResyncTimer + fElapsedTime
	end

	if (gCYW_RequestPending == true) then
		gCYW_RequestPendingTimer = gCYW_RequestPendingTimer + fElapsedTime
		if (gCYW_RequestPendingTimer >= 1) then
			gCYW_RequestPendingTimer = 0
			gCYW_RequestPendingMAXTime = gCYW_RequestPendingMAXTime - 1
			if (gCYW_RequestPendingMAXTime <= 0) then
				gCYW_RequestPending = false
				gCYW_RequestPendingMAXTime = gCYW_MAXTIME
				UIUpdateStatusPanel(nil, "", "<font color="..gCYW_textColors.Red..">Unable to get response from World host server</font>", 1)
			else
				UIUpdateStatusPanel(Control_GetWidth(gHandles["pbarStatus"]) + gCYW_ProgressBar.WAIT_INCREMENT, "Creating...", "", nil)
			end
		end
	end
end

function initalizeForm()

	-- reset any information left in menu.xml
	local ctl = gHandles["cbSelectTemplate"];
	if (ctl ~= nil) then
		ComboBox_RemoveAllItems(ctl)
		ComboBox_AddItem(ctl, "Loading templates...", 0)
	end

	Static_SetText(gHandles["lblSelectTemplateDescription"], "", false)

	for i=1,gCYW_maxFeatures do 
		local startX = Control_GetLocationX(gHandles["lblSelectTemplateFeatures" .. tostring(i)])
		local startY = Control_GetLocationY(gHandles["lblSelectTemplateFeatures" .. tostring(i)])
		gCYW_FeaturePositions["lblSelectTemplateFeatures" .. tostring(i)] = { x = startX, y = startY }
		Static_SetText(gHandles["lblSelectTemplateFeatures" .. tostring(i)], "", false)
		Control_SetVisible(gHandles["lblSelectTemplateBullet" .. tostring(i)], false)
	end

	EditBox_SetText(gHandles["edNewWorldName"],        "", false)
	EditBox_SetText(gHandles["edNewWorldDescription"], "", false)

	toggleDialogPanels(gCYW_DisplayMode.SHOW_NONE)

	Control_SetEnabled(gHandles["btnCreateWorld"], false)
	Control_SetFocus(gHandles["cbSelectTemplate"], true)
end

function getTemplateById(templateId)
	for _, item in pairs(g_templateList) do
		if (item.templateId == templateId) then
			return item
		end
	end

	return nil
end

function toogleSelectTemplate(templateId)

	-- show VIP upsell if necessary
	local template = getTemplateById(templateId) or {}
	if template.upsellPassGroupId == gCYW_VIPPassGroupId and not gCYW_HasVIPPass then 
		toggleDialogPanels(gCYW_DisplayMode.SHOW_VIP)
	else
		toggleDialogPanels(gCYW_DisplayMode.SHOW_MAIN)
	end

	Static_SetText(gHandles["lblSelectTemplateDescription"], "", false)

	for i=1,gCYW_maxFeatures do 
		Static_SetText(gHandles["lblSelectTemplateFeatures" .. tostring(i)], "", false)
		Control_SetVisible(gHandles["lblSelectTemplateBullet" .. tostring(i)], false)
	end

	-- update screen with selected template data
	for _, item in pairs(g_templateList) do
		if (item.templateId == templateId) then

			local numFeatures = #item.featureList
			if (numFeatures > gCYW_maxFeatures) then
				numFeatures = gCYW_maxFeatures
			end

			Static_SetText(gHandles["lblSelectTemplateDescription"], item.description, false)

			local displacement = 0
			if string.len(item.description) > gCYW_MaxDescriptionChars then
				displacement = gCYW_DescriptionWrapDisplacement
			elseif string.len(item.description) < gCYW_MinDescriptionChars then
				displacement = -gCYW_DescriptionWrapDisplacement
			end

			for i = 1, numFeatures do
				Static_SetText(gHandles["lblSelectTemplateFeatures" .. tostring(i)], item.featureList[i], false)
				local newX = gCYW_FeaturePositions["lblSelectTemplateFeatures" .. tostring(i)].x
				local newY = gCYW_FeaturePositions["lblSelectTemplateFeatures" .. tostring(i)].y
				newY = newY + displacement

				local newWidth = Control_GetWidth(gHandles["lblSelectTemplateFeatures" .. tostring(i)])
				local newHeight = Control_GetHeight(gHandles["lblSelectTemplateFeatures" .. tostring(i)])
				if string.len(item.featureList[i]) > gCYW_MaxFeatureChars then
					displacement = displacement + gCYW_FeatureWrapDisplacement
					newHeight = newHeight + gCYW_FeatureWrapDisplacement
				end
				
				Control_SetLocation(gHandles["lblSelectTemplateFeatures" .. tostring(i)], newX, newY)
				Control_SetSize(gHandles["lblSelectTemplateFeatures" .. tostring(i)], newWidth, newHeight)

				Control_SetLocationY(gHandles["lblSelectTemplateBullet" .. tostring(i)], newY)
				Control_SetVisible(gHandles["lblSelectTemplateBullet" .. tostring(i)], true)
			end

			local hctl = gHandles["imgSelectTemplate"]
			local elem   = Image_GetDisplayElement(hctl)
			local width  = Control_GetWidth(hctl)
			local height = Control_GetHeight(hctl)

			Element_AddTexture(elem, gDialogHandle, item.fileName)
			Element_SetCoords(elem, 0, 0, width, height)

			g_selectedTemplate = item
		end
	end

	-- set prgoress bar initial size
	Control_SetSize(gHandles["pbarStatus"], 0, Control_GetHeight(gHandles["pbarStatus"]))
end

function togglePanelSets(hide, show, disable) 
	-- Hide --
	for i,panelName in pairs(hide) do
		for i,ctrlName in pairs(gctrls_Panels[panelName]) do
			Control_SetVisible(gHandles[ctrlName], false)
			Control_SetEnabled(gHandles[ctrlName], false)
		end
	end

	-- Show --
	for i,panelName in pairs(show) do
		for i,ctrlName in pairs(gctrls_Panels[panelName]) do
			if not gCYW_HideDropdown or (ctrlName ~= "stcSelectTemplate" and ctrlName ~= "cbSelectTemplate") then
				Control_SetVisible(gHandles[ctrlName], true)
				Control_SetEnabled(gHandles[ctrlName], true)
			end
		end
	end

	-- Disable --
	for i,panelName in pairs(disable) do
		for i,ctrlName in pairs(gctrls_Panels[panelName]) do
			Control_SetEnabled(gHandles[ctrlName], false)
		end
	end
end

function toggleDialogPanels(displayMode)
	local hide = {}
	local show = {}
	local disable = {}

	-- SelectTemplate / NewWorld / ProgressBar / Status
	if (displayMode == gCYW_DisplayMode.SHOW_NONE) then
		hide = {"ProgressBar", "Status", "VIPUpsell", "SelectTemplate", "NewWorld"}

	elseif (displayMode == gCYW_DisplayMode.SHOW_MAIN) then
		hide = {"ProgressBar", "Status", "VIPUpsell"}
		show = {"SelectTemplate", "NewWorld"}
		
	elseif (displayMode == gCYW_DisplayMode.SHOW_STATUS) then
		hide = {"SelectTemplate", "VIPUpsell"}
		show = {"ProgressBar", "Status", "NewWorld"}

	elseif (displayMode == gCYW_DisplayMode.SHOW_VIP) then
		hide = {"ProgressBar", "Status", "NewWorld"}
		show = {"SelectTemplate", "VIPUpsell"}

	end

	togglePanelSets(hide, show, disable)
end

function UIUpdateStatusPanel(pbarXValue, statusMsg, resultMsg, statusCode)

	if (pbarXValue ~= nil) then
		Control_SetSize(gHandles["pbarStatus"], pbarXValue, Control_GetHeight(gHandles["pbarStatus"]))
		if (pbarXValue > 0) then
			toggleProgressBar(true)
		else
			toggleProgressBar(false)
		end
	else
		toggleProgressBar(false)
	end

	if (statusMsg ~= nil) then
		Static_SetText(gHandles["txtStatus"], tostring(statusMsg))
	end

	if (resultMsg ~= nil) then
		Static_SetText(gHandles["stcResult"], tostring(resultMsg))
	end

	if (statusCode == nil) then
		Control_SetEnabled(gHandles["btnCreateWorld"], false)
		Control_SetEnabled(gHandles["btnHide"],        false)
	else
		Control_SetEnabled(gHandles["btnCreateWorld"], true)
		Control_SetEnabled(gHandles["btnHide"],        true)
	end
end

function toggleInput(blnState)
	-- Disable all input controls
	Control_SetEnabled(gHandles["cbSelectTemplate"],      blnState)
	Control_SetEnabled(gHandles["edNewWorldName"],        blnState)
	Control_SetEnabled(gHandles["edNewWorldDescription"], blnState)
end

function toggleProgressBar(blnState)
	for i,ctrlName in pairs(gctrls_Panels["ProgressBar"]) do
		Control_SetVisible(gHandles[ctrlName], blnState)
		Control_SetEnabled(gHandles[ctrlName], blnState)
	end
end

--------------------------------------------------
-- Gui Controls in (top/left -- bottom/right) order
--------------------------------------------------

function cbSelectTemplate_OnComboBoxSelectionChanged(comboBoxHandle, selectedIndex, selectedText)
	toogleSelectTemplate(ComboBox_GetSelectedData(comboBoxHandle))
	setEditBoxText("edNewWorldName", "")
	setEditBoxText("edNewWorldDescription", "")
end

function btnVIP_OnButtonClicked(buttonHandle)
	launchWebBrowser(gCYW_VIPLink)
	MenuCloseThis()
end

function btnHide_OnButtonClicked(buttonHandle)
	toggleDialogPanels(gCYW_DisplayMode.SHOW_MAIN)
	toogleSelectTemplate(g_selectedTemplate.templateId)
end

function btnCreateWorld_OnButtonClicked(buttonHandle)

	if (gCYW_RequestPending) then
		LogWarn("CreateWorldClicked: nested request -- ignoring")
		return
	end

	UIUpdateStatusPanel(gCYW_ProgressBar.EMPTY, "", "", nil)
	toggleDialogPanels(gCYW_DisplayMode.SHOW_STATUS)
	toggleInput(false)

	local hctrl = nil
	local errorMsg = ""

	-- check for errors in reverse order to set focused field

	-- World Description
	local newWorldDesc = trim(EditBox_GetText(gHandles["edNewWorldDescription"]))
	if (string.len(newWorldDesc) <= 0) then
		hctrl = gHandles["edNewWorldDescription"]
		errorMsg = "<font color="..gCYW_textColors.Red..">Description is a required field.</font><br/><br/>"
	end

	-- World Name
	local newWorldName = trim(EditBox_GetText(gHandles["edNewWorldName"]))
	if (string.len(newWorldName) < 4 or string.len(newWorldName) > 50) then
		hctrl = gHandles["edNewWorldName"]
		errorMsg = "<font color="..gCYW_textColors.Red..">The name you entered must be between 4 - 50 characters in length.</font><br/><br/>" .. errorMsg
	end

	-- Prerequest errors
	if (hctrl) then
		toggleInput(true)
		errorMsg = "Please correct the following error(s):<br/><br/>" .. errorMsg
		UIUpdateStatusPanel(nil, "", errorMsg, 1)
		Control_SetFocus(hctrl, true)
		return
	else
		UIUpdateStatusPanel(gCYW_ProgressBar.START_INCREMENT, "Creating...", errorMsg, 1)
	end

	gCYW_RequestPending = true
	local enc_newWorldName = KEP_UrlEncode(newWorldName)
	local enc_newWorldDesc = KEP_UrlEncode(newWorldDesc)
	local url = GameGlobals.WEB_SITE_PREFIX .. CREATE_WORLD_SUFFIX .. "worldname=" .. enc_newWorldName .. "&worlddesc=" .. enc_newWorldDesc .. "&templateid=" .. g_selectedTemplate.templateId .. "&async=true"
	makeWebCall( url, WF.CREATE_WORLD)
	gCYW_AsyncTimestamp = GetTimeMs()
end

function stcResult_OnLinkClicked(controlHandle, link)
	if (link ~= nil) then KEP_LaunchBrowser(link) end
end

--------------------------------------------------
-- Internal helpers
--------------------------------------------------

function requestTemplateListSuccess()

	-- we have a new list of templates from server
	function CompareSortOrder(a,b) return a.sortOrder < b.sortOrder end
	table.sort(g_templateList, CompareSortOrder)
	local ctl = gHandles["cbSelectTemplate"]
	if (ctl ~= nil) then
		ComboBox_RemoveAllItems(ctl)
		for _, item in pairs(g_templateList) do
			ComboBox_AddItem(ctl, item.name, item.templateId)
			Control_SetEnabled(gHandles["btnCreateWorld"], true)
			toogleSelectTemplate(ComboBox_GetSelectedData(ctl))
			g_formReady = true
		end
		-- If we only have 1 template to show, don't show the dropdown.
		if #g_templateList == 1 then
			gCYW_HideDropdown = true
			for _, ctrlName in pairs(gctrls_Panels["SelectTemplate"]) do
				if ctrlName == "stcSelectTemplate" or ctrlName == "cbSelectTemplate" then
					Control_SetVisible(gHandles[ctrlName], false)
					Control_SetEnabled(gHandles[ctrlName], false)
				else
					Control_SetLocationY(gHandles[ctrlName], Control_GetLocationY(gHandles[ctrlName]) - gCYW_DropdownDisplacement)
					if gCYW_FeaturePositions[ctrlName] ~= nil then
						gCYW_FeaturePositions[ctrlName].y = gCYW_FeaturePositions[ctrlName].y - gCYW_DropdownDisplacement
					end
				end
			end
		end
	end
end

function requestTemplateListFailed()
	local errorMsg = "<font color="..gCYW_textColors.Red..">There was a problem retreiving world information from the server.  Please try again later.</font><br/><font color="..gCYW_textColors.Black.."><a href='http://support.kaneva.com/'>Click here</a> for support.</font>"
	toggleDialogPanels(gCYW_DisplayMode.SHOW_STATUS)
	UIUpdateStatusPanel(gCYW_ProgressBar.EMPTY, "", errorMsg, nil)
	toggleInput(false)
end

function requestCreateWorldSuccess(code, desc, url)
	gCYW_AsyncTimestamp = nil
	gCYW_AsyncToken = nil
	WorldsAnalytics_Track("KEPClient")
	UIUpdateStatusPanel(gCYW_ProgressBar.COMPLETE, "Creating...", "world created successfully", 1)
	gotoURL(url)
end

function requestCreateWorldStatusCheck(code, desc, url, immediate)
	if gCYW_AsyncTimestamp ~= nil and gCYW_AsyncToken ~= nil then
		local timeElapsed = GetTimeElapsedMs(gCYW_AsyncTimestamp)
		if timeElapsed < gCYW_AsyncTimeout then
			local ev = KEP_EventCreate( "CreateWorldStatusCheckEvent")
			KEP_EventEncodeString(ev, gCYW_AsyncToken)
			if immediate then
				KEP_EventQueueInFuture(ev, 5)
			else
				KEP_EventQueueInFuture(ev, 10)
			end
		else
			requestCreateWorldFailed(-9, desc, url)
		end
	else
		requestCreateWorldFailed(-9, desc, url)
	end
end

function requestCreateWorldFailed(code, desc, url)
	gCYW_AsyncTimestamp = nil
	gCYW_AsyncToken = nil

	--  1, "Invalid arguments"
	--  2, "You must first validate your email to create a World"
	--  3, "VIP Users are restricted to a maximum of [n] Worlds"
	--  4, "Non-VIP Users are restricted to a maximum of [n] Worlds"
	--  5, "World <originalName>worldName</originalName> already exists, please change the name. We recommend <recommendedName>worldName</recommendedName>]"
	--  6, "World host server busy, please try again later"
	--  7, "Name should be at least four characters and can contain only letters, numbers, spaces, dashes or underscore"
	--  8, "Still waiting"
	--  9, "Service error"
	--  10, "Error creating World" --> Actual[Error returned from IsTaskCompleted]
	--  11, "Error creating World" --> Actual[3Dapp could not be found]
	--  12, "Error creating World" --> Actual[Unable to get response from 3dapp host server"]
	--  13, "Error creating World" --> Actual[Error connecting to 3DApp host server]
	--  14, "Error creating World" --> Actual[Bad response from 3dapp host server]
	--  15, "Error creating World" --> Actual[Error returned from App Create]
	--  20  "Internal error"

	if (code == -1) then
		desc = "<font color="..gCYW_textColors.Red..">The World could not be created.  Please try again later.</font><br/><font color="..gCYW_textColors.Black.."><a href='http://support.kaneva.com/'>Click here</a> for support.</font>"
	elseif (code == -2) then
		desc = "<font color="..gCYW_textColors.Red..">You must first validate your email to create a World.</font><br/><font color="..gCYW_textColors.Black.."><a href='"..GameGlobals.WEB_SITE_PREFIX_KANEVA .. VALIDATE_EMAIL_SUFFIX.."'>Click here</a> to validate and receive 100 Rewards.</font>"
	elseif (code == -4) then
		desc = "<font color="..gCYW_textColors.Red..">You must be VIP to create more Worlds.</font><br/><font color="..gCYW_textColors.Black.."><a href='".. GameGlobals.WEB_SITE_PREFIX_KANEVA .. BUY_VIP_PASS_SUFFIX .. ">Click here</a> to upgrade to VIP.</font>"
	elseif (code == -5) then
		local originalName = nil
		local recommendedName = nil
		local s, e = 0
		s, e, originalName    = string.find(desc, "<originalName>(.-)</originalName>")
		s, e, recommendedName = string.find(desc, "<recommendedName>(.-)</recommendedName>")

		if (originalName ~= nil and recommendedName ~= nil) then
			desc = "<font color="..gCYW_textColors.Red..">The World name </font><font color="..gCYW_textColors.Black..">".. originalName.."</font><font color="..gCYW_textColors.Red.."> already exists and must be changed.</font><br/><font color="..gCYW_textColors.Black..">We recommend " .. recommendedName .. ".</font>"
		else
			desc = "<font color="..gCYW_textColors.Red..">".. desc.."</font>"
		end

	elseif (code < -10) then
		desc = "<font color="..gCYW_textColors.Red..">The World could not be created.  Please try again later.</font><br/><font color="..gCYW_textColors.Black.."><a href='http://support.kaneva.com/'>Click here</a> for support.</font>"
	end

	desc = "Please correct the following error(s):<br/><br/>" .. desc
	UIUpdateStatusPanel(nil, "", desc, 1)
	toggleInput(true)
end

function trim(s)
	return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

----------------------------------------------------------
-- returns 1 paramter code
-- -- code: 0 = success // < 0 error
-- populates global var g_templateList 
----------------------------------------------------------

function parseResponseTemplateList(responseXml)
	local code = nil  -- return code
	local ttlRecs = 0   -- number of records total 
	local numRecs = 0   -- number of records in this chunk

	local s, e, code = string.find(responseXml, "<ReturnCode>(.-)</ReturnCode>")

	code = tonumber(code)
	if (code == nil or code ~= 0) then
		LogError("parseResponseTemplateList: response returned error["..tostring(code).."]")
		code = -1
		return code
	end

	g_templateList = {}

	_, e, ttlRecs = string.find(responseXml, "<TotalNumberRecords>(.-)</TotalNumberRecords>")
	_, e, numRecs = string.find(responseXml, "<NumberRecords>(.-)</NumberRecords>")
	numRecs = tonumber(numRecs)
	if numRecs > 0 then

		local gie = 0
		for i=1,numRecs do
			local gameInfo = nil
			_, gie, gameInfo = string.find(responseXml, "<WorldTemplate>(.-)</WorldTemplate>", e)

			if gameInfo == nil then
				-- we didn't find the expected number of <WorldTemplate> records -- try "GameTemplate"
				_, gie, gameInfo = string.find(responseXml, "<GameTemplate>(.-)</GameTemplate>", e)

				-- still didn't find anything, break
				if gameInfo == nil then
					break;
				end
			end

			-- we found a <WorldTemplate> record
			e = gie

			_, _, templateId             = string.find(gameInfo, "<TemplateId>(.-)</TemplateId>")
			_, _, defaultCategoryId      = string.find(gameInfo, "<DefaultCategoryId>(.-)</DefaultCategoryId>")
			_, _, sortOrder              = string.find(gameInfo, "<SortOrder>(.-)</SortOrder>")
			_, _, status                 = string.find(gameInfo, "<Status>(.-)</Status>")
			_, _, name                   = string.find(gameInfo, "<Name>(.-)</Name>")
			_, _, imageId                = string.find(gameInfo, "<KEPThumbnailId>(.-)</KEPThumbnailId>")
			_, _, previewImagePathSmall  = string.find(gameInfo, "<PreviewImagePathSmall>(.-)</PreviewImagePathSmall>")
			_, _, previewImagePathMedium = string.find(gameInfo, "<PreviewImagePathMedium>(.-)</PreviewImagePathMedium>")
			_, _, previewImagePathLarge  = string.find(gameInfo, "<PreviewImagePathLarge>(.-)</PreviewImagePathLarge>")
			_, _, description			 = string.find(gameInfo, "<Description>(.-)</Description>")
			_, _, upsellPassGroupId	     = string.find(gameInfo, "<UpsellPassGroupId>(.-)</UpsellPassGroupId>")
			_, _, numFeatures            = string.find(gameInfo, "<FeatureCount>(.-)</FeatureCount>")
			numFeatures  = tonumber(numFeatures) or 0

			local featureList = {}
			if numFeatures > 0 then
				local fe = 0
				for j=1, numFeatures do
					local featureEntry = nil
					_, fe, featureEntry = string.find(gameInfo, "<Feature>(.-)</Feature>", fe)
					if featureEntry == nil then
						-- we didn't find the expected number of <Feature> records -- bail out
						break;
					end
					gie = fe
					table.insert(featureList, featureEntry)
				end
			end

			local fileName = KEP_DownloadTextureAsync(GameGlobals.CUSTOM_TEXTURE, imageId, previewImagePathMedium, GameGlobals.THUMBNAIL)
			fileName = string.gsub(fileName,".-\\", "" )
			fileName = "../../CustomTexture/".. fileName

			-- put all the temp variables into a struct
			local template = {}
			template.templateId             = tonumber(templateId)        or -1
			template.defaultCategoryId      = tonumber(defaultCategoryId) or -1
			template.sortOrder              = tonumber(sortOrder)         or -1
			template.status                 = tonumber(status)			  or -1
			template.name                   = name                        or ""
			template.imageId                = tonumber(imageId)           or -1
			template.previewImagePathSmall  = previewImagePathSmall       or ""
			template.previewImagePathMedium = previewImagePathMedium      or ""
			template.previewImagePathLarge  = previewImagePathLarge       or ""
			template.fileName               = fileName                    or ""
			template.description            = description                 or ""
			template.upsellPassGroupId      = tonumber(upsellPassGroupId) or 0
			template.featureList            = featureList                 or {}
			table.insert(g_templateList, template)
		end
	end

	if (#g_templateList < 0) then
		code = -1
	end

	return code
end

----------------------------------------------------------
-- returns two parameters code / description
-- -- code: 0 = success // < 0 error
-- -- desc: message for user
----------------------------------------------------------
function parseResponseCreateWorld(reponseXml)
	local code = nil  -- return code
	local desc = nil  -- result description
	local url  = nil  -- world url
	local token = nil -- async token

	if (reponseXml == nil) then
		LogError("parseResponseCreateWorld null response.")
		code = -99
		desc = "Unexpected response"
		url  = ""
		return code, desc, url, token
	end

	code = string.match(reponseXml, "<ReturnCode>(.-)</ReturnCode>")
	desc = string.match(reponseXml, "<ReturnDescription>(.-)</ReturnDescription>")
	url  = string.match(reponseXml, "<STPURL>(.-)</STPURL>") 
	token  = string.match(reponseXml, "<token>(.-)</token>") 

	if (code == nil) then
		LogError("parseResponseCreateWorld: unexpected code in response ["..tostring(code).."] -- desc["..tostring(desc).."] -- url["..tostring(url).."]")
		code = -99
		desc = "Unexpected response"
		url  = ""
		return code, desc, url, token
	else
		code = tonumber(code)
	end

	return code, desc, url, token
end
