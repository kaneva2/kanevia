--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--TextureBrowser2_ab117: Updated Patterns menu (9/24/2013)
dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\Selection.lua")

g_changeBuildModeEvent = true

WEB_ADDRESS = GameGlobals.WEB_SITE_PREFIX.."kgp/texturesList.aspx?start="
WEB_ADDRESS_ACTION = "&action=searchMedia"

TEXTURES_PER_PAGE = 16


g_t_textures = {}
g_selectedTexture = 0
g_curTextureList = 0


g_categoryName = ""


g_clearSearch = true
g_searching = false
g_searchString = ""

g_curPage = 1


g_time = 0
g_endTime = true

g_playerMode = "God" -- Assume god mode unless told otherwise
g_playerLocOnOpen = {} -- Player position when the menu is opened

local m_attribute = ""

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "textureSelectionEventHandler", "TextureSelectionEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler( "editParamHandler", "EDIT_PARAM_EVENT", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- Create the thumbnail functions
	for i = 1, TEXTURES_PER_PAGE do
		_G["btnThumb" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			local n = string.match(Control_GetName(btnHandle), "(%d+)")
			selectTexture(tonumber(n))
		end
	end
	
	-- Disable Apply Button
	enableApplyButton(false)
	
	changePage()

end


function Dialog_OnMouseMove(dialogHandle, x, y)

	-- Update tooltips based on texture name/author
	for i = 1,#g_t_textures do 
		if Control_ContainsPoint(gHandles["btnThumb" .. i], x, y) ~= 0 and Control_GetVisible(gHandles["btnThumb" .. i]) == 1 then 
			Control_SetToolTip(gHandles["btnThumb"..i],g_t_textures[i]["name"])
		end
	end 

	-- Highlight/unhighlight the Pagination on hover
	local edPage = gHandles["edPage"]
	if Control_GetFocus(edPage) ~= 1 then
		if Control_ContainsPoint(edPage, x, y) == 1 then
			Control_SetEnabled(edPage, true)
			EditBox_SetText(edPage, "Enter #", false)
		elseif Control_GetEnabled(edPage) == 1 then
			Control_SetEnabled(edPage, false)
			setPageText()
		end
	end
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	
	-- If clicking outside the categories dropdown, hide it

	-- If the edit box has focus, and you've clicked outside the search area, clear focus on the dialog so that the x shows properly
	if Control_GetFocus(gHandles["eBoxSearch"]) == 1 and Control_ContainsPoint(gHandles["eBoxSearch"], x, y) == 0 and Control_ContainsPoint(gHandles["btnSearch"], x, y) == 0 then
		MenuClearFocusThis()
	end		

	-- See Pagination functions - selecting pagination box. It is usually disabled to have to do here.
	if Control_ContainsPoint(gHandles["edPage"], x, y) == 1 then
		selectPageBox()
	end
end

-----------------------------------------------------------------------------------------
-- Web calls, Event Handlers, Parsing
-----------------------------------------------------------------------------------------

function changePage()
	clear()
	setPageText()
	
	Control_SetEnabled(gHandles["btnPrev"], g_curPage ~= 1)
	if g_searchString ~= "" then
		makeWebCall(WEB_ADDRESS .. g_curPage.."&max=" .. TEXTURES_PER_PAGE .. WEB_ADDRESS_ACTION .. "&ss=" .. g_searchString, WF.IMAGE_LIST)
	else
		makeWebCall(WEB_ADDRESS .. g_curPage.."&max=" .. TEXTURES_PER_PAGE .. WEB_ADDRESS_ACTION, WF.IMAGE_LIST)
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.IMAGE_LIST  then
		local xmlData = KEP_EventDecodeString( event )
		ParseTextureData(xmlData)
		--To textureDownloadHandler
		return KEP.EPR_CONSUMED
	end
end


function ParseTextureData(xmlData)

	local texType = "" 
	local result = string.match(xmlData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then

		g_t_textures = {}

		
		
		texType = "<Texture>.-</Texture>"
		
		local showNone = false
		for imagestr in string.gmatch(xmlData, texType) do


			local name = string.match(imagestr, "<name>(.-)</name>")												-- name of the asset
			local username = string.match(imagestr, "<username>(.-)</username>")									-- creator's username
		
			local file_size = string.match(imagestr, "<file_size>([0-9]+)</file_size>")				-- file size of the image
	
			local content_extension = string.match(imagestr, "<content_extension>(.-)</content_extension>")			-- the content extension of the image
			local asset_id = string.match(imagestr, "<asset_id>([0-9]+)</asset_id>")								-- what we are really looking for
			local created_date, created_time = string.match(imagestr, "<created_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)")-- Date this image was created
			local imageUrl = string.match(imagestr, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>")
			local imageFullURL = string.match(imagestr, "<image_full_path>(.-)</image_full_path>")
			
			local tex = {}
			
			if tonumber(asset_id) == 0 then
				tex["name"] = "none"
				tex["thumbnailFile"] = ""
				tex["asset_id"] = 0
				showNone = true
			else
		
				tex["name"] = name
				tex["username"] = username
				tex["file_size"] = file_size
				tex["content_extension"] = content_extension
				tex["asset_id"] = tonumber(asset_id)
				tex["created_date"] = created_date
				tex["full_image_url"] = imageFullURL
				local full_path = KEP_DownloadTextureAsync( GameGlobals.CUSTOM_TEXTURE, asset_id, imageUrl, GameGlobals.THUMBNAIL )

				-- Strip off begnning of file path leaving only file name
				tex["thumbnailFile"] = string.gsub(full_path,".-\\", "" )
			end

			table.insert(g_t_textures, tex)
			
			Control_SetVisible(gHandles["stcNoImage"], showNone)
		end
	end
end


function textureDownloadHandler()
	showThumbnails()
end


function textureSelectionEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local count = KEP_EventDecodeNumber( event ) or 0
	for i = 1, count do
		g_objIds[i] = KEP_EventDecodeNumber( event ) or 0
	end
end


function editParamHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_attribute = KEP_EventDecodeString(tEvent)

	if m_attribute == "greetingImage" or m_attribute == "goalImage" then
		Static_SetText(gHandles["stcMessage"], "Recommended image size: 100x100")
	elseif m_attribute == "overviewImage" then
		Static_SetText(gHandles["stcMessage"], "Recommended image size: 945x540")
	end
end

-----------------------------------------------------------------------------------------
-- Thumbnails/Frames
-----------------------------------------------------------------------------------------
function showThumbnails()
	
	if #g_t_textures > 0 then 
		showNoneSearched(false)

		for i = 1, #g_t_textures do
			if g_t_textures[i]["thumbnailFile"] ~= "" then 
				ih = gHandles["imgThumb" .. i]
				ihBack = gHandles["btnThumb" .. i]
				fileName = "../../CustomTexture/".. g_t_textures[i]["thumbnailFile"]
				scaleImage(ih, ihBack, fileName)
			end
			setTextureThumbnails(i, true)
		end
		
		local nextPage = true
		if #g_t_textures < TEXTURES_PER_PAGE then
			for i=#g_t_textures+1, TEXTURES_PER_PAGE do 
				setTextureThumbnails(i, false)
			end 
			nextPage = false
		end
		Control_SetEnabled(gHandles["btnNext"], nextPage)
	else
		for i= 1,TEXTURES_PER_PAGE do
			setTextureThumbnails(i, false)
		end
		
		showNoneSearched(true)
	end 
end

function setTextureThumbnails(index,enabled)
	Control_SetVisible(gHandles["imgThumbBG" .. index],	enabled)
	Control_SetVisible(gHandles["btnThumb" .. index],	enabled)
	Control_SetVisible(gHandles["imgThumb" .. index],	enabled)
	Control_SetVisible(gHandles["imgOutline" .. index],	enabled)
end 

function clear()
	for i = 1, TEXTURES_PER_PAGE do
		setTextureThumbnails(i, false)

		-- Reset the size and location of the image
		local ih = gHandles["imgThumb"..i]
		local ihBack = gHandles["btnThumb"..i]
		local xCoord = Control_GetLocationX(ihBack)
		local yCoord = Control_GetLocationY(ihBack)
		local width = Control_GetWidth(ihBack)
		local height = Control_GetHeight(ihBack)
		Control_SetLocation(ih, xCoord, yCoord)
		Control_SetSize(ih, width, height)
		element = Image_GetDisplayElement(ih)
		Element_AddTexture(element, gDialogHandle, "patterns2_assets(5).tga")
		Element_SetCoords(element, 147, 265,226,321)

		Control_SetVisible(gHandles["stcNoImage"], false)
	end

	Control_SetVisible(gHandles["imgSelectedTexture"], false)
end

function selectTexture(index)
	g_selectedTexture = index

	if index > 0 and index <= #g_t_textures then
		imgSelected = gHandles["imgSelectedTexture"]
		fileName = "../../CustomTexture/".. g_t_textures[index]["thumbnailFile"]


		enableApplyButton(true)
		
		--scaleImage(imgSelected,imgSelectedBack,fileName)
		
		Control_SetLocation(imgSelected, Control_GetLocationX(gHandles["btnThumb"..index]), Control_GetLocationY(gHandles["btnThumb"..index]))
		Control_SetVisible(imgSelected, true)

		Static_SetText(gHandles["btnApply"], "Apply")
		
	end



end

-------------------------------------------------------------------------------------
-- Pagination
-----------------------------------------------------------------------------------------
function btnPrev_OnButtonClicked( buttonHandle )
	if g_curPage > 1 then
		g_curPage = g_curPage - 1	
	else
		g_curPage = 1 --Just in case...
	end
	changePage()
end

function btnNext_OnButtonClicked( buttonHandle )

	g_curPage = g_curPage + 1
	
	changePage()

end

function edPage_OnFocusOut( buttonHandle )
	Control_SetEnabled(buttonHandle, false)
	setPageText()
end

function edPage_OnEditBoxString(editBoxHandle, someString)
	g_curPage = tonumber(someString) or g_curPage
	--g_curPage = 1

	changePage()
end

function setPageText()
	local pageText = "Page " .. g_curPage
	if g_curPage > 99 then
		pageText = "P. " .. g_curPage
	end
	EditBox_SetText(gHandles["edPage"], pageText, false)
end

function selectPageBox()
	local editBox = gHandles["edPage"]
	Control_SetEnabled(editBox, true)
	EditBox_ClearText(editBox)
	Control_SetFocus(editBox)
end

-----------------------------------------------------------------------------------------
-- Search
-----------------------------------------------------------------------------------------
function search()
	local searchString = EditBox_GetText(gHandles["eBoxSearch"])

	-- Do this check because if it is empty, OnFocusOut has already replaced the string with "Search"
	if g_clearSearch then
		clearSearch()
		changePage() -- Separate because it avoids double webcalls
		return
	end

	if searchString ~= g_searchString then
		g_searchString = searchString

		g_curPage = 1


		changePage()

		showSearchX(true)
	end 
	MenuClearFocusThis()
end

function clearSearch()
	EditBox_SetText( gHandles["eBoxSearch"], "Search", false)
	g_clearSearch = true
	showSearchX(false)	
	if g_searchString ~= "" then 
		g_searchString = ""
	end 
end

function showSearchX(show)
	Control_SetVisible(gHandles["btnSearchX"], show)
end

function btnSearch_OnButtonClicked( buttonHandle )
	search()
end

-- Hitting the "Enter" key in a search box
function eBoxSearch_OnEditBoxString(editBoxHandle, someString)
	search()
end

function btnSearchX_OnButtonClicked( buttonHandle )
	clearSearch()
	changePage() -- Separate because it avoids double webcalls
end 

function eBoxSearch_OnFocusIn(editBoxHandle)
	showSearchX(false)
	if g_clearSearch then
		EditBox_SetText(editBoxHandle, "", true)
	end
	g_clearSearch = false
end

-- Also see Dialog_OnLButtonDownInside - clears focus there when clicking anywhere else
function eBoxSearch_OnFocusOut(editBoxHandle)
	local search = EditBox_GetText(editBoxHandle)
	if search == "" or g_clearSearch then
		g_clearSearch = true
		EditBox_SetText(editBoxHandle, "Search", false)
	else
		g_clearSearch = false
		showSearchX(true)
	end
end

function showNoneSearched(enabled)
	Control_SetVisible(gHandles["stcNoneSearched1"], enabled)
	Control_SetVisible(gHandles["stcNoneSearched2"], enabled)
end 

-----------------------------------------------------------------------------------------
-- Apply/Copy/Reset
-----------------------------------------------------------------------------------------

function btnApply_OnButtonClicked( buttonHandle )

	if g_selectedTexture > 0 then
		local assetId = g_t_textures[g_selectedTexture]["asset_id"]
		if (assetId == nil) then return end 
		local texUrl = g_t_textures[g_selectedTexture]["full_image_url"]
		if (texUrl == nil) then texUrl = "" end
		Log("ApplyClicked: assetId="..assetId.." texUrl='"..texUrl.."'")

		local ev = KEP_EventCreate( "IMAGE_SELECT_EVENT" )
		KEP_EventEncodeString(ev, m_attribute)
		KEP_EventEncodeString(ev, g_t_textures[g_selectedTexture]["name"])
		KEP_EventEncodeString(ev, g_t_textures[g_selectedTexture]["full_image_url"])
		KEP_EventQueue(ev)

		MenuCloseThis()
	end
end

function enableApplyButton(enable)
	Control_SetVisible(gHandles["btnApply"], true) -- Always keep on, we are just enabling/disabling. May be off from timer though
	Control_SetEnabled(gHandles["btnApply"], enable)
end

-----------------------------------------------------------------------------------------
-- Misc Buttons
-----------------------------------------------------------------------------------------

function btnMorePatterns_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser(GameGlobals.WEB_SITE_PREFIX_KANEVA ..UPLOAD_PATTERNS_SUFFIX )
end
