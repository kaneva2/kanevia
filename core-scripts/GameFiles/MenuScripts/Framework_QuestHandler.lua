--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Questhandler.lua
-- Author: Daniel
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("Framework_ActorHelper.lua")

---------------
-- CONSTANTS --
---------------

local SECS_IN_MIN = 60
local CHECK_STATUS_INTERVAL = 10

---------------------
-- LOCAL VARS --
---------------------

local m_playerName = nil
local m_activeQuests = {}

-- Game item cache
local m_initGameItemCache = false
local m_gameItems = {}
local m_initPlayerInventory = false
local m_playerInventory = {}
local m_inventoryCounts = {}
local m_initActiveQuests = false
local m_playerQuests = {} -- Quests a player has

local m_menuReady = false
local m_pendingBarks = {}
local m_questQueueTimer = 0
local m_barkPending = false

local m_checkQuestsPending = false
local m_questCheckStatusTimer = 0
local m_pendingFinishedUNID = nil

--------------------
-- CORE FUNCTIONS --
--------------------

-- Called when the menu is created
function onCreate()
	-- Register client-to-client events
	KEP_EventRegister("CHECK_QUESTS_FOR_COMPLETE", KEP.MED_PRIO)
	KEP_EventRegister("QUEUE_QUEST_BARK", KEP.MED_PRIO)
	KEP_EventRegister("QUEST_HANDLER_ITEM_CRAFTED", KEP.MED_PRIO)
	-- KEP_EventRegister("FRAMEWORK_QUEST_TURNIN_CLOSING", KEP.HIGH_PRIO)

	-- Register handlers for client-to-client events
	KEP_EventRegisterHandler("onAddQuest", "INVENTORY_HANDLER_ADD_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCompleteQuest", "INVENTORY_HANDLER_COMPLETE_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onAbandonQuest", "INVENTORY_HANDLER_ABANDON_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCheckQuestsForComplete", "CHECK_QUESTS_FOR_COMPLETE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onQuestInfoClose", "FRAMEWORK_QUEST_INFO_CLOSING", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onItemCrafted", "QUEST_HANDLER_ITEM_CRAFTED", KEP.HIGH_PRIO)
	-- KEP_EventRegisterHandler("onQuestTurninClose", "FRAMEWORK_QUEST_TURNIN_CLOSING", KEP.HIGH_PRIO)

	-- Register handlers for server-to-client events
	Events.registerHandler("UPDATE_QUESTS", updateQuests) -- Event handler when receiving a quest log from server
	Events.registerHandler("FRAMEWORK_BARK_TRIGGERED", onBarkTriggered)
	Events.registerHandler("FRAMEWORK_WAYPOINT_TRIGGERED", onWaypointTriggered)
	Events.registerHandler("FULFILL_ACTOR_TALK_TO", onActorTalkToFulfilled)
	Events.registerHandler("ACTOR_DELIVERY_TRIGGERED", onActorDeliveryTriggered)
	Events.registerHandler("FRAMEWORK_PLACE_GAME_ITEM_CONFIRMATION", placeGameItemConfirmation)
	Events.registerHandler("FRAMEWORK_QUEST_HANDLER_RECORD_KILL", onRecordKill)

	-- Game Item Cache
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateQuestsClient", "UPDATE_QUEST_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateQuestsClientFull", "UPDATE_QUEST_CLIENT_FULL", KEP.HIGH_PRIO)
	
	m_menuReady = false
	requestInventory(GAME_PID)
	requestInventory(INVENTORY_PID)

	m_playerName = KEP_GetLoginName() -- Get the local client name
end

-- Called when menu is destroyed
function onDestroy()
end

-- Called each frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_initGameItemCache and m_initPlayerInventory and m_initActiveQuests and not m_menuReady then
		m_menuReady = true
		Events.sendEvent("QUEST_HANDLER_READY")
	end

	if m_menuReady and (m_barkPending ~= true) and getFirstEntry(m_pendingBarks) ~= nil and not isActorMenuOpen(false) and not MenuIsOpen("Framework_Welcome.xml") then
		if m_questQueueTimer > 0 then
			m_questQueueTimer = m_questQueueTimer - 1
		else
			local tempQuestIndex = getFirstEntry(m_pendingBarks)
			m_questQueueTimer = 0

			local questUNID = getFirstEntry(m_pendingBarks)

			clearBark(questUNID)

			m_barkPending = true
			openQuest(tempQuestIndex)
		end
	end

	if m_menuReady and m_checkQuestsPending and not isActorMenuOpen(false) then -- Use a flag to do this ONCE instead of queueing multiple copies of the function
		if m_questCheckStatusTimer > 0 then
			m_questCheckStatusTimer = m_questCheckStatusTimer - 1
		else
			m_checkQuestsPending = false
			checkQuestsForComplete()
		end
	end
end

--------------------
-- EVENT HANDLERS --
--------------------

-- Get the full list of quests
function updateQuests(event)
	-- Handle the incomplete quests
	if event.questsIncomplete then
		for k, questInfo in pairs(event.questsIncomplete) do
			local questProps = m_gameItems[tostring(questInfo.UNID)]
			if questProps then
				local duplicateQuest = false
				for i, playerQuest in pairs(m_activeQuests) do
					-- If we have a duplicate quest, simply update its complete status
					if playerQuest.UNID == questInfo.UNID then
						m_activeQuests[i].complete = false
						m_activeQuests[i].fulfilled = questInfo.fulfilled
						m_activeQuests[i].fulfillCount = questInfo.fulfillCount
						m_activeQuests[i].active = true
						duplicateQuest = true
						break
					end
				end
				-- If unique quest, insert it
				if not duplicateQuest then
					local tempQuest = {}
					tempQuest = questInfo
					tempQuest.properties = deepCopy(questProps)
					tempQuest.complete = false
					tempQuest.fulfilled = questInfo.fulfilled
					tempQuest.fulfillCount = questInfo.fulfillCount
					tempQuest.active = questInfo.active
					table.insert(m_activeQuests, tempQuest)
					
				end
			end
		end
	end

	-- TO DO: UN-fulfilling quest?

	m_initActiveQuests = true

	m_checkQuestsPending = true
	m_questCheckStatusTimer = CHECK_STATUS_INTERVAL
end

-- Start tracking a quest
function onAddQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local event = {}

	event.quest = decompileInventoryItem(tEvent)
	
	if event.quest.complete == true then
		return -- We only care about unfinished quests here
	else
		event.quest.complete = false
		event.quest.active = true
	end

	-- Check for duplicate quest (if so, update it)
	local duplicateQuest = false
	for index, quest in pairs(m_activeQuests) do
		if tonumber(event.quest.UNID) and tonumber(quest.UNID) == event.quest.UNID then
			duplicateQuest = true
			m_activeQuests[index] = event.quest
			break
		end
	end

	-- If not a duplicate, add a new quest
	if not duplicateQuest then
	
		table.insert(m_activeQuests, event.quest)
		m_checkQuestsPending = true
		m_questCheckStatusTimer = CHECK_STATUS_INTERVAL
	end
end

-- Stop tracking a quest
function onAbandonQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = KEP_EventDecodeNumber(tEvent)

	for index, quest in pairs(m_activeQuests) do
		if tonumber(quest.UNID) and tonumber(quest.UNID) == questUNID then
			table.remove(m_activeQuests, index)
		end
	end
end

function onCompleteQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = KEP_EventDecodeNumber(tEvent)
	
	-- Complete quest
	if m_activeQuests then
		for index, quest in pairs(m_activeQuests) do
			if tonumber(quest.UNID) and tonumber(quest.UNID) == questUNID then
				table.remove(m_activeQuests, index)
			end
		end
		clearBark(questUNID)

		-- Check for one more, just in case
		m_checkQuestsPending = true
		m_questCheckStatusTimer = CHECK_STATUS_INTERVAL
	end

	m_pendingFinishedUNID = questUNID
end

function onQuestInfoClose(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	resetBark()
	m_checkQuestsPending = true
	m_questCheckStatusTimer = CHECK_STATUS_INTERVAL
end

function onQuestTurninClose(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	resetBark()
	m_checkQuestsPending = true
	m_questCheckStatusTimer = CHECK_STATUS_INTERVAL
end

function onCheckQuestsForComplete(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_checkQuestsPending = true
	m_questCheckStatusTimer = CHECK_STATUS_INTERVAL
end

function onItemCrafted(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local itemUNID = KEP_EventDecodeNumber(tEvent)
	local itemCount = KEP_EventDecodeNumber(tEvent)
	
	if itemUNID == nil then return end
	if itemCount == nil or itemCount == 0 then return end

	for index, quest in pairs(m_playerQuests) do
		if quest.properties and quest.properties.requiredItem and quest.properties.requiredItem.requiredUNID then -- If has a target actor UNID
			if tostring(quest.properties.requiredItem.requiredUNID) == tostring(itemUNID) then -- If UNIDs match
				if quest.properties.questType and quest.properties.questType == "craft" then -- If uses "talkTo" quest type
					updateFulfillCount(quest.UNID, itemCount, quest)
				end
			end
		end
	end
end

-- function onItemRemoved(event)
-- 	local itemUNID = KEP_EventDecodeNumber(tEvent)
-- 	local itemCount = KEP_EventDecodeNumber(tEvent)
	
-- 	if itemUNID == nil then return end
-- 	if itemCount == nil or itemCount ~= 0 then return end

-- 	for index, quest in pairs(m_playerQuests) do
-- 		if quest.properties and quest.properties.requiredUNID then -- If has a target actor UNID
-- 			if quest.properties.requiredUNID == itemUNID then -- If UNIDs match
-- 				if quest.properties.questType and quest.properties.questType == "collect" then -- If uses "talkTo" quest type
-- 					updateFulfillCount(quest.UNID, itemCount, quest)
-- 				end
-- 			end
-- 		end
-- 	end
-- end

function onBarkTriggered(event)
	local tempQuestIndex = event.questIndex
	table.insert(m_pendingBarks, tempQuestIndex)
	m_questQueueTimer = 1 -- Wait exactly 1 frame before loading (allow data to update)
end

function onWaypointTriggered(event)
	if event.PID == nil then return end
	for index, quest in pairs(m_playerQuests) do
		if quest.properties and quest.properties.waypointPID then -- If has a waypoint PID
			if quest.properties.waypointPID == event.PID then -- If PIDs match
				if quest.properties.questType and quest.properties.questType == "goTo" then -- If uses "goTo" quest type
					fulfillQuest(quest.UNID, quest)
				end
			end
		end
	end
end

function onActorTalkToFulfilled(event)
	if event.UNID == nil then return end
	for index, quest in pairs(m_playerQuests) do
		if quest.properties and quest.properties.targetUNID then -- If has a target actor UNID
			if tonumber(quest.properties.targetUNID) == tonumber(event.UNID) then -- If UNIDs match
				if quest.properties.questType and quest.properties.questType == "talkTo" then -- If uses "talkTo" quest type
					fulfillQuest(quest.UNID, quest)
				end
			end
		end
	end
end

function onActorDeliveryTriggered(event)
	if event.UNID == nil then return end

	for index, quest in pairs(m_playerQuests) do
		if quest.UNID and quest.properties and quest.properties.targetUNID then -- If has a target actor UNID
			if tonumber(quest.properties.targetUNID) == tonumber(event.UNID) then -- If UNIDs match
				if quest.properties.questType and quest.properties.questType == "takeTo" and tostring(quest.complete) ~= "true" and tostring(quest.fulfilled) ~= "true"  then -- If uses "takeTo" quest type
					local requiredUNID = quest.properties.requiredItem.requiredUNID
					local requiredCount = quest.properties.requiredItem.requiredCount
					local playerCount = m_inventoryCounts[tonumber(requiredUNID)] or 0
					if tonumber(playerCount) >= tonumber(requiredCount) then
						MenuOpen("Framework_QuestDeliver.xml")
						local ev = KEP_EventCreate("FRAMEWORK_QUEST_INFO") -- Handled by Framework_QuestDeliver
			       		KEP_EventEncodeString(ev, Events.encode(quest))
			        	KEP_EventQueue(ev)
			        end
				end
			end
		end
	end
end

function placeGameItemConfirmation(event)
	if event.success == nil then return end
	if event.PID == nil then return end

	local _, _, _, _, _, _, _, _, _, _, _, _, _, placedUNID, _ = KEP_DynamicObjectGetInfo(event.PID)

	if placedUNID and event.success == true then
		for index, quest in pairs(m_playerQuests) do
			if quest.properties and quest.properties.requiredItem and quest.properties.requiredItem.requiredUNID then -- If has a target actor UNID
				if tostring(quest.properties.requiredItem.requiredUNID) == tostring(placedUNID) then -- If UNIDs match
					if quest.properties.questType and quest.properties.questType == "place" then -- If uses "place" quest type
						updateFulfillCount(quest.UNID, 1, quest)
					end
				end
			end
		end
	end
end

function onRecordKill(event)
	if event.UNID == nil then return end

	for index, quest in pairs(m_playerQuests) do
		if quest.properties and quest.properties.requiredItem and quest.properties.requiredItem.requiredUNID then -- If has a target actor UNID
			if tostring(quest.properties.requiredItem.requiredUNID) == tostring(event.UNID) then -- If UNIDs match
				if quest.properties.questType and quest.properties.questType == "kill" then -- If uses "kill" quest type
					updateFulfillCount(quest.UNID, 1, quest)
				end
			end
		end
	end
end

---------------------
-- GAME ITEM CACHE --
---------------------

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems

	m_initGameItemCache = true
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)

	if m_playerInventory[updateIndex] then
		m_playerInventory[updateIndex] = updateItem
		updateInventory()
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_playerInventory = decompileInventory(tEvent)

	updateInventory()

	requestInventory(QUEST_PID)
	Events.sendEvent("REQUEST_PLAYER_QUESTS")

	m_initPlayerInventory = true
end

function updateInventory()
	m_inventoryCounts = {}
	-- Compile UNID counts
	for i=1, #m_playerInventory do
		if m_playerInventory[i].UNID ~= 0 then
			-- New UNID entry
			if m_inventoryCounts[m_playerInventory[i].UNID] == nil then
				m_inventoryCounts[m_playerInventory[i].UNID] = m_playerInventory[i].count
			-- Duplicate entry. Increment UNID count
			else
				m_inventoryCounts[m_playerInventory[i].UNID] = m_inventoryCounts[m_playerInventory[i].UNID] + m_playerInventory[i].count
			end
			m_checkQuestsPending = true
			m_questCheckStatusTimer = CHECK_STATUS_INTERVAL
		end
	end	
	-- m_checkQuestsPending = true
	-- m_questCheckStatusTimer = CHECK_STATUS_INTERVAL
end

-- Update a specific quest
function updateQuestsClient(dispatcher, fromNetid, event, eventid, filter, objectid)
	local updateUNID = KEP_EventDecodeNumber(event)
	local updateQuest = decompileInventoryItem(event)
	
	for i=1,#m_playerQuests do
		if m_playerQuests[i] and tonumber(m_playerQuests[i].UNID) == tonumber(updateUNID) then
			m_playerQuests[i] = updateQuest
		end
	end

	m_checkQuestsPending = true
	m_questCheckStatusTimer = CHECK_STATUS_INTERVAL
end

-- Receives all of a player's quests from InventoryHandler
function updateQuestsClientFull(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_displayingQuest = nil
	m_playerQuests = decompileInventory(event)

	m_checkQuestsPending = true
	m_questCheckStatusTimer = CHECK_STATUS_INTERVAL

	if m_pendingFinishedUNID then
		Events.sendEvent("QUEST_COMPLETE_CHECK_NEXT", {questUNID = m_pendingFinishedUNID})
		m_pendingFinishedUNID = nil
	end
end

---------------------
-- LOCAL FUNCTIONS --
---------------------

checkQuestsForComplete = function()
	for index, quest in pairs(m_activeQuests) do
		-- Get the quest properties
		if quest.UNID then
			local questItem
			questItem = m_gameItems[tostring(quest.UNID)]
			-- Check that properties exist
			if questItem and questItem.autoComplete and tostring(questItem.autoComplete) == "true" then
				-- Check that the quest is autocomplete
				if tostring(questItem.autoComplete) == "true" then
					if questItem.questType == "collect" then
						-- Check that more properties exist
						if questItem.requiredItem and questItem.requiredItem.requiredUNID and questItem.requiredItem.requiredCount then
							-- Get the required items
							local requiredItem = questItem.requiredItem.requiredUNID
							local requiredCount = questItem.requiredItem.requiredCount
							-- Compare required items to player inventory
							local backCnt = m_inventoryCounts[tonumber(requiredItem)] or 0
							-- m_inventoryCounts[m_playerInventory[i].UNID]
							if backCnt >= requiredCount then
								autoCompleteQuest(quest)
							end
						end
					elseif questItem.questType == "goTo" or questItem.questType == "talkTo" or questItem.questType == "takeTo" or questItem.questType == "craft" or questItem.questType == "place" or questItem.questType == "kill" then
						if quest.fulfilled and quest.fulfilled == true then
							autoCompleteQuest(quest)
						end
					end
				end
			end
		end
	end
end

autoCompleteQuest = function(quest)
	if not MenuIsOpen("Framework_QuestInfo.xml") and not MenuIsOpen("Framework_QuestTurnIn.xml") then		

		-- open the quest info menu
		MenuOpen("Framework_QuestTurnIn.xml")

		local questItem = quest

		local tempRewardItem = {}
		local questReward = questItem.properties.rewardItem
		tempRewardItem.properties = {}
		tempRewardItem.properties.name = ""
		tempRewardItem.properties.GLID = 0
		if questReward.rewardUNID ~= 0 then
			tempRewardItem.properties = m_gameItems[tostring(questReward.rewardUNID)]
		end
		tempRewardItem.UNID = questReward.rewardUNID or 0
		tempRewardItem.count = questReward.rewardCount or 0
		
		-- Format quest properties properly for the QuestTurnIn menu
		questItem.properties.rewardItem = {}
		questItem.properties.rewardItem.rewardUNID = tempRewardItem.UNID or 0
		questItem.properties.rewardItem.rewardCount = tempRewardItem.count or 0
		questItem.properties.rewardItem.name = ""
		questItem.properties.rewardItem.GLID = 0

		if tempRewardItem.properties then
			if tempRewardItem.properties.name then
				questItem.properties.rewardItem.name = tempRewardItem.properties.name
			end
			if tempRewardItem.properties.GLID then
				questItem.properties.rewardItem.GLID = tempRewardItem.properties.GLID
			end
		end

		local ev = KEP_EventCreate("FRAMEWORK_QUEST_INFO") -- Handled by Framework_QuestInfo or Framework_QuestTurnIn
		KEP_EventEncodeString(ev, Events.encode(questItem))
		KEP_EventEncodeString(ev, "false")
		KEP_EventEncodeString(ev, "true") -- Make the window modal
		KEP_EventEncodeString(ev, Events.encode(tempRewardItem))
		KEP_EventEncodeString(ev, 0) -- TO DO: Get the actual quest giver ID	
		KEP_EventQueue(ev)
	end
end

resetBark = function()
	m_barkPending = false
	m_questQueueTimer = 1
end

--Clear a quest from barking again
clearBark = function(questUNID)
	for barkIndex, barkUNID in pairs(m_pendingBarks) do
		if tonumber(barkUNID) == tonumber(questUNID) then
			m_pendingBarks[barkIndex] = nil
		end
	end
end

openQuest = function(questIndex)
	-- local menuHeight = Dialog_GetHeight(m_questInfoDialog)
	-- local clientHeight = Dialog_GetScreenHeight(m_questInfoDialog)
	-- local menuY = math.max(0,(clientHeight/2) - (menuHeight/2))
	-- Dialog_SetLocationY(m_questInfoDialog, menuY)
	
	--log("--- menuY: ".. tostring(menuY))
	--log("--- Dialog_SetLocation: ".. tostring(Dialog_GetLocationY(m_questInfoDialog)))

	local tempQuestItem = m_gameItems[tostring(questIndex)]

	if tempQuestItem then
		local questItem = {}
		questItem.properties = tempQuestItem
		questItem.UNID = questIndex

		local tempRewardItem = {}
		local questReward = questItem.properties.rewardItem
		local questType = questItem.properties.questType 
		tempRewardItem.properties = {}

		if tonumber(questReward.rewardUNID) > 0 then
			tempRewardItem.properties = m_gameItems[tostring(questReward.rewardUNID)]
		end
		tempRewardItem.UNID = questReward.rewardUNID
		tempRewardItem.count = questReward.rewardCount or 0
		
		-- Format quest properties properly for the QuestTurnIn menu
		questItem.properties.rewardItem = {}
		questItem.properties.rewardItem.rewardCount = tempRewardItem.count
		questItem.properties.rewardItem.name = tempRewardItem.properties.name
		questItem.properties.rewardItem.rewardUNID = tempRewardItem.UNID
		questItem.properties.rewardItem.GLID = tempRewardItem.properties.GLID

		local questState = getQuestState(questItem)
		local acceptable = false
		if questState == "COMPLETE" then
			resetBark()
			return
		elseif questState == "ACTIVE" or questState == "REP_ACTIVE" then
			-- menuToOpen = "Framework_QuestInfo.xml"
			resetBark()
			return
		elseif questState == "AVAILABLE" or questState == "REP_AVAILABLE" then
			if questType == "collect" or questType == "craft" or questType == "place" or questType == "kill" then -- Collection/Craft/Place/Kill quest (have a required item)
				if questItem.properties.requiredItem.requiredUNID ~= "0" then -- Required item exists
					acceptable = true
					menuToOpen = "Framework_QuestInfo.xml"
				else -- Requirements not defined, so let it be completed right away
					menuToOpen = "Framework_QuestTurnIn.xml"
				end
			elseif questType == "talkTo" then -- Talk to quest (has a target UNID)
				if questItem.properties.targetUNID ~= "0" then -- Target UNID exists
					acceptable = true
					menuToOpen = "Framework_QuestInfo.xml"
				else -- Requirements not defined, so let it be completed right away
					menuToOpen = "Framework_QuestTurnIn.xml"
				end
			elseif questType == "takeTo" then -- Take to quest (has required item AND target UNID)
				if questItem.properties.requiredItem.requiredUNID ~= "0" and questItem.properties.targetUNID ~= "0" then -- Required item and target UNID exist
					acceptable = true
					menuToOpen = "Framework_QuestInfo.xml"
				else -- Requirements not defined, so let it be completed right away
					menuToOpen = "Framework_QuestTurnIn.xml"
				end
			elseif questType == "goTo" then -- Go to quest (has waypointPID)
				if questItem.properties.waypointPID ~= "0" then -- Waypoint PID defined
					acceptable = true
					menuToOpen = "Framework_QuestInfo.xml"
				else -- Requirements not defined, so let it be completed right away
					menuToOpen = "Framework_QuestTurnIn.xml"
				end
			else -- If undefined, just open the quest info
				menuToOpen = "Framework_QuestInfo.xml"
			end
		else
			resetBark()
			return
		end

		MenuOpen(menuToOpen)

		-- Send quest info over
		local isModal = "false"
		if questItem.properties.autoAccept == "true"  or (menuToOpen == "Framework_QuestTurnIn.xml" and questItem.properties.autoComplete == "true") then
			isModal = "true"
		end

		local ev = KEP_EventCreate("FRAMEWORK_QUEST_INFO") -- Handled by Framework_QuestInfo or Framework_QuestTurnIn
		KEP_EventEncodeString(ev, Events.encode(questItem))
		KEP_EventEncodeString(ev, tostring(acceptable))
		KEP_EventEncodeString(ev, isModal) -- Make the window modal
		KEP_EventEncodeString(ev, Events.encode(tempRewardItem))
		KEP_EventEncodeNumber(ev, 0) -- pass along m_actorPID?
		KEP_EventQueue(ev)

		resetBark()
	end
end

-- Gets the state of a quest for a player
-- NOTE: Duplicate function from Framework_QuestGiver.lua
getQuestState = function(quest)
	local questState = "NONE"
	local playerQuest
	for i, quest2 in pairs(m_playerQuests) do
		-- Got a match
		if quest2.UNID == quest.UNID then
			playerQuest = quest2
			break
		end
	end

	-- Player is actively on this quest
	if playerQuest then
		if playerQuest.active then
			-- Quest has a required item
			if playerQuest.properties.requiredItem and playerQuest.properties.requiredItem.requiredUNID ~= "0" then
				local questReqCount = playerQuest.properties.requiredItem.requiredCount or 0
				local playerItemCount = playerQuest.properties.requiredItem.progressCount or 0
				
				-- Player's ready to complete this quest
				if playerItemCount >= questReqCount then
					questState = "COMPLETE"
					
				-- Quest isn't ready to turn in yet
				else
					-- Quest is repeatable
					if quest.properties.repeatable == "true" then
						questState = "REP_ACTIVE"
					else
						questState = "ACTIVE"
					end
				end
			-- No required item?
			else
				questState = "COMPLETE"
			end
		
		-- Player has completed this quest before
		elseif playerQuest.complete then
			-- Repeatable quests have the opportunity to be re-acceptable if the player's off his/her cooldown
			if quest.properties.repeatable == "true" and (playerQuest.repeatTimerUp or (playerQuest.repeatTime and playerQuest.repeatTime == 0)) then
				questState = "REP_AVAILABLE"
			end
		end
	else
		-- Player has never seen this quest before
		-- Quest has no prerequisite
		if tonumber(quest.properties.prerequisiteQuest) == 0 then
			-- Quest is repeatable
			if quest.properties.repeatable == "true" then
				questState = "REP_AVAILABLE"
			else
				questState = "AVAILABLE"
			end
		-- Quest has a prerequisite
		else
			for j, playerQuest2 in pairs(m_playerQuests) do
				-- Player has completed the prerequisite quest
				if (tonumber(quest.properties.prerequisiteQuest) == tonumber(playerQuest2.UNID)) and (playerQuest2.complete or playerQuest2.completeTime) then
					-- Quest is repeatable
					if quest.properties.repeatable == "true" then
						questState = "REP_AVAILABLE"
					else
						questState = "AVAILABLE"
					end
					break
				end
			end
		end
	end
	return questState
end

-- Typical Quest Item:
-- { 
--     'UNID' : 2008 
--     'properties' : table: 29727F60 
--     { 
--         'questPostText' : You completed the quest. Now go away. 
--         'inventoryCompatible' : false 
--         'behavior' : placeable_loot 
--         'prerequisiteQuest' : 0 
--         'autoComplete' : nil 
--         'rewardItem' : table: 29728028 
--         { 
--             'rewardCount' : 0 
--             'rewardUNID' : 0 
--         } 
--         'level' : 1 
--         'GIGLID' : 0 
--         'GIID' : 2008 
--         'itemType' : quest 
--         'GLID' : 4503530 
--         'repeatTime' : 0 
--         'barkRange' : 0 
--         'questPreText' : This is an example quest 
--         'autoAccept' : nil 
--         'requiredItem' : table: 29728118 
--         { 
--             'requiredCount' : 0 
--             'requiredUNID' : 0 
--         } 
--         'description' : Example quest. 
--         'name' : New Quest(1) 
--         'rarity' : Common 
--         'repeatable' : true 
--         'destroyOnCompletion' : true 
--         'waypointInfo' : table: 29728550 
--         { 
--             'height' : 0 
--             'radius' : 0 
--         } 
--         'waypointPID' : 0 
--     } 
-- } 

-- Returns the first entry of a table or nil if empty
getFirstEntry = function(inputTable)
	for i,v in pairs(inputTable) do
		return v
	end
	return nil
end