--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Framework_GemBox.lua
--
-- Displays gem rewarded through gembox open
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- -- -- -- -- -- --
-- CONSTANTS
-- -- -- -- -- -- --


-- -- -- -- -- -- --
-- LOCAL FUNCTIONS
-- -- -- -- -- -- --
local parseGemData --(gemString)
local populateGemData --()

-- -- -- -- -- -- --
-- LOCAL VARIABLES
-- -- -- -- -- -- --
local m_currGems = {}

-- Called when the menu is created
function onCreate()
	KEP_EventRegister("FRAMEWORK_CURRENCY_TAB_OPEN", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "tabOpenHandler", "FRAMEWORK_CURRENCY_TAB_OPEN", KEP.MED_PRIO )
	
	Events.registerHandler("FRAMEWORK_UPDATE_GEM_COUNTS", updateGemCounts)

	m_currGems = {topaz = 0, sapphire = 0, emerald = 0, ruby = 0, diamond = 0}
	
	requestGemData()
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
end

-- When the ok button is clicked
function btn_Ok_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

-- -- -- -- -- -- --
-- EVENT HANDLERS
-- -- -- -- -- -- --
function updateGemCounts(event)
	local success = parseGemData(event.text)
	if success then
		populateGemData()
	end
end

function tabOpenHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	requestGemData()
end

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- If browser return for Location Data web call
	if filter == WF.GEM_AMOUNTS or filter == WF.GEM_CONVERT or filter == WF.GEM_REWARD then
		local estr = KEP_EventDecodeString( tEvent )
		local success = parseGemData(estr)
		
		if success then
			populateGemData()
		end

	   	return
	end
end

-- Web call to receive the current gem data
function requestGemData()
	local address = GameGlobals.WEB_SITE_PREFIX.."/kgp/metaGameItems.aspx?action=getBalances&itemType=Gem"
	makeWebCall( address , WF.GEM_AMOUNTS)
end

-- -- -- -- -- -- --
-- LOCAL FUNCTIONS
-- -- -- -- -- -- --
function parseGemData(gemString)
	local returnCode = ParseXmlReturnCode(gemString)
	if (returnCode == nil or returnCode ~= 0) then
		return false --Failure!
	end
	
	for item in string.gmatch(gemString, "<Item>(.-)</Item>") do
		local itemName = xmlGetString(item, "Name")
		local itemBalance = xmlGetInteger(item, "Balance")
		
		m_currGems[string.lower(itemName)] = itemBalance
	end
	
	return true
end


function populateGemData()
	Static_SetText(gHandles["stcDiamondCount"], tostring(m_currGems.diamond))
	Static_SetText(gHandles["stcRubyCount"], tostring(m_currGems.ruby))
	Static_SetText(gHandles["stcSapphireCount"], tostring(m_currGems.sapphire))
	Static_SetText(gHandles["stcEmeraldCount"], tostring(m_currGems.emerald))
	Static_SetText(gHandles["stcTopazCount"], tostring(m_currGems.topaz))
end