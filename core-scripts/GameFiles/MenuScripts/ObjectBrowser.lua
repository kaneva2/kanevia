--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\MathHelper.lua")

ITEM_SELECTED_EVENT = "SelectedItemInfoEvent"
ENTER_BUILD_MODE_EVENT = "EnterBuildModeEvent"
DYNAMIC_OBJECT_INFO_EVENT = "DynamicObjectInfoEvent"
ADD_DYNAMIC_OBJECT_EVENT = "AddDynamicObjectEvent"
REMOVE_DYNAMIC_OBJECT_EVENT = "RemoveDynamicObjectEvent"
SCRIPT_SERVER_EVENT = "ScriptServerEvent"

g_listhandles = {}
g_listscrollbars = {}

g_objectCount = 0
g_objects = {}
g_objectsFind = {} -- drf - added 
g_ignoreSelection = false
g_selectCount = 0

g_queryString = ""

--Sorting
Sort_ObjectName = 1
Sort_ObjectPID = 2
Sort_ScriptName = 3
Sort_AnimationName = 4
Sort_ParticleName = 5
Sort_PositionX = 6
Sort_PositionY = 7
Sort_PositionZ = 8

g_bSortAscending = true
g_SortingBy = Sort_ScriptName
g_SortFuncs = {} --filled in in after the sort functions are declared

g_pageSize = 21
g_currentPage = 1
g_totalPages = 1
g_startIdx = 1

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "selectEventHandler", "SelectEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandlerV( "removeDynamicObjectEventHandler", REMOVE_DYNAMIC_OBJECT_EVENT, KEP.LOW_PRIO )
	KEP_EventRegisterHandlerV( "addDynamicObjectEventHandler", ADD_DYNAMIC_OBJECT_EVENT, KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "scriptClientEventHandler", "ScriptClientEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.LOW_PRIO )

	-- DRF - Added
	KEP_EventRegisterHandler("objectsUpdatedEventHandler", "ObjectsUpdatedEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( DYNAMIC_OBJECT_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterSecureV( ADD_DYNAMIC_OBJECT_EVENT, KEP.HIGH_PRIO, KEP.ESEC_SERVER, KEP.ESEC_CLIENT )
	KEP_EventRegisterSecureV( REMOVE_DYNAMIC_OBJECT_EVENT, KEP.HIGH_PRIO, KEP.ESEC_SERVER + KEP.ESEC_CLIENT, KEP.ESEC_SERVER + KEP.ESEC_CLIENT )

	-- DRF - Added
	KEP_EventRegister("ObjectsUpdatedEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	ListBox_SetHTMLEnabled(gHandles["lbxName"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxPID"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxScript"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxAnim"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxParticle"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxXPos"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxYPos"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxZPos"], true)
	
	g_listhandles = 
	{ --TODO: uncomment the Dummy lines when Eric checks them in
		gHandles["lbxNameDummy"],
		gHandles["lbxName"],
		gHandles["lbxPIDDummy"],
		gHandles["lbxPID"],
		gHandles["lbxScriptDummy"],
		gHandles["lbxScript"],
		gHandles["lbxAnimDummy"],
		gHandles["lbxAnim"],
		gHandles["lbxParticleDummy"],
		gHandles["lbxParticle"],
		gHandles["lbxXPosDummy"],
		gHandles["lbxXPos"],
		gHandles["lbxYPosDummy"],
		gHandles["lbxYPos"],
		gHandles["lbxZPosDummy"],
		gHandles["lbxZPos"]
	}
	
	--Fix the spacing on the title
	Dialog_SetCaptionText(gDialogHandle, "    Object Browser")

	g_listscrollbars = {}
	for i,v in ipairs(g_listhandles) do
		table.insert(g_listscrollbars, ListBox_GetScrollBar(v))
	end

	updateObjects()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function scriptClientEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter ~= ScriptClientEventTypes.GetAllScripts then return end

	for i,v in ipairs(g_objectsFind) do
		v.scriptName = ""
	end
		
	local numRunningScripts = KEP_EventDecodeNumber(event)
	for i=1,numRunningScripts do
		local script = {}
		script.objectId = KEP_EventDecodeNumber(event)
		script.filename = KEP_EventDecodeString(event)
		script.scriptName = string.gsub(script.filename, "(.-\\).-", "")
		script.objectName = ""
			
		local obj = getObjectFind(script.objectId)
		if obj ~= nil then
			obj.scriptName = script.scriptName
			obj.filename = script.filename
		end
	end
		
	Log("scriptClientEventHandler: numRunningScripts="..numRunningScripts.." - Calling populateList()")
	populateList()
--	updateSelection()
end

-- DRF - Added
function objectsUpdatedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	Log("objectsUpdateEventHandler: Calling updateObjects()")
	updateObjects()
end

function removeDynamicObjectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	Log("removeDynamicObjectEventHandler: Calling updateObjects()")
	updateObjects()
end

function addDynamicObjectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	Log("addDynamicObjectEventHandler: Calling updateObjects()")
	updateObjects()	
end

function selectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	Log("selectEventHandler: selectCount="..g_selectCount.." - Calling UpdateList()")
--	if g_selectCount <= 0 then
--		updateSelection()
--	end
	g_selectCount = g_selectCount - 1
	updateObjects()	
	MenuClearFocusThis()
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.OBJECT_BROWSER_INFO then
		local xml = KEP_EventDecodeString(event)
		ParseInfoResults(xml)
	end
end

function ParseInfoResults( browserString )
	local result = nil  -- result desc
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(browserString, "<ReturnCode>(%d+)</ReturnCode>")	
	if result == "0" then
		s, e, numRec = string.find(browserString, "<NumberRecords>(%d+)</NumberRecords>", e)
		numRec = tonumber(numRec)
		if numRec > 0 then
			for i = 1, numRec do
				s, e, globalID = string.find(browserString, "<global_id>(%d+)</global_id>", e)
				globalID = tonumber(globalID)
				s, e, name = string.find(browserString, "<name>(.-)</name>", e)
				s, e, inventoryType = string.find(browserString, "<inventory_type>(%d+)</inventory_type>", e)
				inventoryType = tonumber(inventoryType)
				if inventoryType ~= GameGlobals.IT_BOTH then
					g_inventoryType = inventoryType
				end
				for i,v in ipairs(g_objectsFind) do
					if globalID == v.particleGlid then
						v.particleName = name
					elseif globalID == v.animationGlid then
						v.animationName = name
					end
				end
			end
		end
	end

	Log("ParseInfoResults: populateList()")
	populateList()
--	updateSelection()
end

function updateObjects()
	
	-- Get All Objects (DOs + Sounds)
	local numDOs = KEP_GetDynamicObjectCount()
	local sounds = { KEP_GetSoundPlacementIdList() }
	local numSounds = #sounds
	Log("updateObjects: numDOs="..numDOs.." numSounds="..numSounds)

	-- Add DOs To Objects Table
	g_objects = {}
	for i = 1, numDOs do
		local objId = KEP_GetDynamicObjectPlacementIdByPos(i - 1)
		if (objId > 0) then -- drf - no local placements
			local obj = ObjectGetInfo(objId, ObjectType.DYNAMIC)
			if obj.exists then
				obj.pos.x = round(obj.pos.x, 3)
				obj.pos.y = round(obj.pos.y, 3)
				obj.pos.z = round(obj.pos.z, 3)
				table.insert(g_objects, obj)
			end
		end
	end
    
    -- Add Sounds To Objects Table
	for i = 1, numSounds do     
        local objId = sounds[i]
		if (objId > 0) then -- drf - no local placements
			local obj = ObjectGetInfo(objId, ObjectType.SOUND)
			if obj.exists then
				obj.name = obj.name.." [sound]"
				obj.pos.x = round(obj.pos.x, 3)
				obj.pos.y = round(obj.pos.y, 3)
				obj.pos.z = round(obj.pos.z, 3)
				table.insert(g_objects, obj)
			end
		end
    end

	-- Find Query Objects Only
	updateObjectsFind()

	sortObjectsFind(g_SortingBy)
	getParticleAnimationInfo()	
	requestScriptInfo()
--    updateSelection()
--	SelectedObjectsLog()
end

function updateObjectsFind()
	if g_queryString == nil then
		g_queryString = ""
	end

	g_objectsFind = {}
	for i = 1, #g_objects do
	 	local obj = g_objects[i]
		local start = string.find(string.lower(obj.name), string.lower(g_queryString))
		if start ~= nil then
			local smartObjChecked = Control_GetVisible(gHandles["boxSmartObj"]) and CheckBox_GetChecked(gHandles["boxSmartObj"]) == 1
			local smartObjScript = obj.filename and obj.filename ~= "" and ( string.find(obj.filename, "ActionItems\\") or string.find(obj.filename, "ActionInstances\\") or string.find(obj.filename, "ActionItems/") or string.find(obj.filename, "ActionInstances/"))			
			if not smartObjChecked or smartObjScript then
				
				-- Set Object Color And Display Name
				obj.fontColor = (smartObjScript and "FF336633") or "FF000000"
				obj.displayName = string.sub(obj.name, 0, start - 1) .. string.sub(obj.name, start, start + string.len(g_queryString) - 1) .. string.sub(obj.name, start + string.len(g_queryString), -1)
				if smartObjScript then
					obj.displayName = obj.displayName .. " (Smart Object)"
				end

				-- Insert Into List of Objects Found
				table.insert(g_objectsFind, obj)
			end
		end
	end

	Log("updateObjectsFind: query='"..g_queryString.."' objects="..#g_objects.." objectsFind="..#g_objectsFind)
end

function getParticleAnimationInfo()
	
	-- Attach Particle Info To Objects Found
	local glids = ""
	for i,v in ipairs(g_objectsFind) do
		if v.animationGlid > 0 then
			glids = glids .. v.animationGlid .. ","
		end
		if v.particleGlid > 0 then
			glids = glids .. v.particleGlid .. ","
		end
	end
	if glids == "" then return end
	
	-- Perform Webcall To Get Information
	glids = string.sub(glids, 1, -2)
	local url = GameGlobals.WEB_SITE_PREFIX.."kgp/instantBuy.aspx?operation=info&items=" .. glids
	makeWebCall( url, WF.OBJECT_BROWSER_INFO)
end

function updateSelection()
	local count = ObjectsSelected()
	for i,h in ipairs(g_listhandles) do
		ListBox_ClearSelection(h)
	end
	deselectObject() -- drf - added
	for i = 1, count do
		local obj = ObjectSelected(i)
		local result = ListBox_FindItem(gHandles["lbxName"], obj.id)
		if result ~= -1 then
			g_ignoreSelection = true
			ListBox_SelectItem(gHandles["lbxName"], result)
			selectObject(obj.id, true)
		end
	end
	syncAllLists( g_listhandles, gHandles["lbxName"] )
end

function deselectObject()

	-- Disable Bottom Buttons
	Control_SetEnabled(gHandles["btnProperties"], false)
	Control_SetEnabled(gHandles["btnGotoObj"], false)
	Control_SetEnabled(gHandles["btnAttachScript"], false)
	Control_SetEnabled(gHandles["btnDetachScript"], false)
	Control_SetEnabled(gHandles["btnAttachParticle"], false)
	Control_SetEnabled(gHandles["btnDetachParticle"], false)
	Control_SetEnabled(gHandles["btnSetAnim"], false)
end

function selectObject(objId, noHighlight)
	
	local showProperties = false
	local showScriptOptions = false
	local showParticleOptions = false
	local showAnimationOptions = false
	
	-- Get Object From Find List
	local obj = getObjectFind(objId)
	if obj ~= nil then
		Log("selectObject: "..ObjectToString(obj.id, obj.type))

		showProperties = true
		showScriptOptions = (obj.scriptName ~= "")
		showParticleOptions = (obj.particleGlid > 0)
		showAnimationOptions = (obj.animationGlid > 0)
		
		if obj.type ~= ObjectType.NONE and noHighlight ~= true then
			KEP_SelectObject(obj.type, obj.id)
			g_selectCount = g_selectCount + 1
		end
	end
	
	-- Enable Bottom Buttons
	Control_SetEnabled(gHandles["btnProperties"], showProperties)
	Control_SetEnabled(gHandles["btnGotoObj"], showProperties)
	Control_SetEnabled(gHandles["btnAttachScript"], showProperties)
	Control_SetEnabled(gHandles["btnDetachScript"], showScriptOptions)
	Control_SetEnabled(gHandles["btnAttachParticle"], showProperties)
	Control_SetEnabled(gHandles["btnDetachParticle"], showParticleOptions)
	Control_SetEnabled(gHandles["btnSetAnim"], showProperties)
end

function updatePageNumber()
	
	g_currentPage = math.floor((g_startIdx - 1) / g_pageSize) + 1
	g_totalPages = math.ceil(#g_objectsFind / g_pageSize)
	
	-- Sanity Check
	if (g_currentPage < 1) or (g_currentPage > g_totalPages) then
		g_currentPage = 1
		g_startIdx = 1
	end
	
	local pageNumberTxt = "(" .. g_currentPage .. "/" .. g_totalPages .. ")"
	Static_SetText(gHandles["stcPageNumber"], pageNumberTxt)
	Control_SetEnabled(gHandles["btnPageUp"], g_currentPage < g_totalPages)
	Control_SetEnabled(gHandles["btnPageDown"], g_currentPage > 1)
end

function populateList()
	
	for i,v in ipairs(g_listhandles) do
		ListBox_RemoveAllItems(v)
	end

	updatePageNumber()
	
	-- DRF - Determine Objects Appearing On This Page
	local objects = #g_objectsFind
	local endIdx = g_startIdx + g_pageSize - 1
	if (endIdx > objects) then
		endIdx = objects
	end

	Log("populateList: objectsFind="..objects.." startIdx="..g_startIdx.." endIdx="..endIdx)

	for i = g_startIdx, endIdx do
		local obj = g_objectsFind[i]
		local fontColor = obj.fontColor		
		local displayName = obj.displayName				
		ListBox_AddItem(gHandles["lbxNameDummy"], "", obj.id)
		ListBox_AddItem(gHandles["lbxName"], setColor( displayName, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxPIDDummy"], "", obj.id)
		ListBox_AddItem(gHandles["lbxPID"], setColor( obj.id, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxScriptDummy"], "", obj.id)
		ListBox_AddItem(gHandles["lbxScript"], setColor( obj.scriptName, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxAnimDummy"], "", obj.id)
		ListBox_AddItem(gHandles["lbxAnim"], setColor( obj.animationName, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxParticleDummy"], "", obj.id)
		ListBox_AddItem(gHandles["lbxParticle"], setColor( obj.particleName, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxXPosDummy"], "", obj.id)
		ListBox_AddItem(gHandles["lbxXPos"], setColor( obj.pos.x, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxYPosDummy"], "", obj.id)
		ListBox_AddItem(gHandles["lbxYPos"], setColor( obj.pos.y, fontColor), obj.id)
		ListBox_AddItem(gHandles["lbxZPosDummy"], "", obj.id)
		ListBox_AddItem(gHandles["lbxZPos"], setColor( obj.pos.z, fontColor), obj.id)
	end

	--Update the column header images
	local btnHandles = {gHandles["btnName"], gHandles["btnPID"], gHandles["btnScript"], gHandles["btnAnim"], gHandles["btnParticle"], gHandles["btnXPos"], gHandles["btnYPos"], gHandles["btnZPos"],}
	local sBlankTexture = "button9_LGray.tga"
	for i,h in pairs(btnHandles) do
		Element_AddTexture( Control_GetDisplayElement(h,    "normalDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h, "mouseOverDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,   "focusedDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,   "pressedDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,  "disabledDisplay"), gDialogHandle, sBlankTexture)
	end

	local sArrowTexture = "button9_LGrayUpArrow.tga"
	if g_bSortAscending then
		sArrowTexture = "button9_LGrayDownArrow.tga"
	end

	local btnHandle = btnHandles[g_SortingBy]
	if btnHandle ~= nil then
		Element_AddTexture( Control_GetDisplayElement(btnHandle,    "normalDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle, "mouseOverDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,   "focusedDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,   "pressedDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,  "disabledDisplay"), gDialogHandle, sArrowTexture)
	end

	updateSelection()
end

function setColor( text, color )
	return "<font color=" .. color .. ">" .. text .. "</font>"
end

function getObjectFind(objId)
    for i = 1, #g_objectsFind do
        if g_objectsFind[i].id == objId then
            return g_objectsFind[i]
        end
    end
	return nil
end

function getObjectFindType(objId)
    for i = 1, #g_objectsFind do
        if g_objectsFind[i].id == objId then
            return g_objectsFind[i].type
        end
    end
    return ObjectType.NONE
end

function getObjectFindSelected()
	local objId = ListBox_GetSelectedItemData(gHandles["lbxName"])
	for i,v in ipairs(g_objectsFind) do
		if v.id == objId then
			return v
		end
	end
end

function btnPageUp_OnButtonClicked(handler)
	local objects = #g_objectsFind
	local nextIdx = g_startIdx + g_pageSize
	if (nextIdx <= objects) then
		Log("PageUpClicked: Calling populateList()")
		g_startIdx = nextIdx
		populateList()
	end
end

function btnPageDown_OnButtonClicked(handle)
	if (g_startIdx > 1) then
		local prevIdx = g_startIdx - g_pageSize
		if (prevIdx < 1) then
			prevIdx = 1
		end
		Log("PageDownClicked: Calling populateList()")
		g_startIdx = prevIdx
		populateList()
	end
end

function lbxName_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if g_ignoreSelection == false and ((bMouseDown == 1 or bFromKeyboard == 1) or g_bSyncAllLists == 1) then
		g_selectCount = 1
		KEP_ClearSelection()
		
		if ListBox_GetMultiSelection(gHandles["lbxName"]) == true then
			local index = ListBox_GetMultiSelectedItemIndex(gHandles["lbxName"], -1)
			while index ~= -1 do
				selectObject(index)
				index = ListBox_GetMultiSelectedItemIndex(gHandles["lbxName"], index)
			end
		else
			local index = ListBox_GetSelectedItemData(gHandles["lbxName"])
			selectObject(index)
		end
		
		if g_bSyncAllLists == 0 then
			syncAllLists( g_listhandles, listBoxHandle )
		end
	end
	g_ignoreSelection = false
end

function boxSmartObj_OnCheckBoxChanged(checkboxHandle, checked)
	if (checked == 1) then
		Log("OnlySmartObjectsChecked: Calling populateList()")
	else
		Log("OnlySmartObjectsUnChecked: Calling populateList()")
	end
	populateList()
end

function menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
	if g_ignoreSelection == false  and ((bMouseDown == 1 or bFromKeyboard == 1) or g_bSyncAllLists == 1) then
		if g_bSyncAllLists == 0 then
			syncAllLists( g_listhandles, listBoxHandle )
		end
	end
end

function lbxNameDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxPIDDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxPID_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxScriptDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxScript_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxAnimDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxAnim_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxParticleDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxParticle_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxXPosDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxXPos_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxYPosDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxYPos_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxZPosDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxZPos_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function scrollBarChanged(listBoxHandle)
	if g_bSyncAllLists == 0 then
		syncAllScrollBars( g_listhandles, listBoxHandle )
	end
end

function lbxNameDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxName_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxPIDDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxPID_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxScriptDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxScript_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxAnimDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxAnim_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxParticleDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxParticle_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxXPosDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxXPos_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxYPosDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxYPos_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxZPosDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxZPos_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function requestScriptInfo()
	local ev = KEP_EventCreate( SCRIPT_SERVER_EVENT)
	KEP_EventSetFilter(ev, ScriptServerEventTypes.GetAllScripts)
	KEP_EventAddToServer(ev)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventQueue( ev)
end

function OpenListBoxPlaceableObj()
	local objId = ListBox_GetSelectedItemData(gHandles["lbxName"])
	local objType = getObjectFindType(objId)
	if objId ~= nil and objId ~= 0 then
		Log("OpenListBoxPlaceableObj: "..ObjectToString(objId, objType).." - Opening PlaceableObj Menu...")
		MenuOpen("PlaceableObj.xml")
		local e = KEP_EventCreate( DYNAMIC_OBJECT_INFO_EVENT )
		KEP_EventEncodeNumber(e, objId)
		KEP_EventEncodeNumber(e, objType)
		KEP_EventQueue(e)
	end
end

function lbxName_OnListBoxItemDblClick(listboxHandle, selectedIndex, selectedText)
	Log("ObjectDoubleClicked")
	OpenListBoxPlaceableObj()
end

function btnProperties_OnButtonClicked( buttonHandle )
	Log("ObjectPropertiesClicked")
	OpenListBoxPlaceableObj()
end

function btnRename_OnButtonClicked( buttonHandle )
--This functionality doesn't exist yet
end

function btnGotoObj_OnButtonClicked( buttonHandle )
	local obj = getObjectFindSelected()
	if obj == nil then return end
	Log("GotoObject: "..ObjectToString(obj.id, obj.type))
	KEP_SetPlayerPosition(obj.pos.x + .01, obj.pos.y + 1, obj.pos.z)
end

function attachScript()
	MenuOpen("ServerScriptBrowser.xml")
end

function detachScript(objId)
	
	local e = KEP_EventCreate( SCRIPT_SERVER_EVENT)
	KEP_EventSetFilter(e, ScriptServerEventTypes.DetachScript)
	KEP_EventAddToServer(e)
	KEP_EventEncodeNumber(e, objId)
	KEP_EventEncodeNumber(e, 0)
	KEP_EventEncodeNumber(e, 0)
	KEP_EventQueue(e)
	
	Log("detachScript: objId="..objId.." - Calling updateObjects()")
	updateObjects()
end

function btnAttachScript_OnButtonClicked( buttonHandle )
	attachScript()
end

function btnDetachScript_OnButtonClicked( buttonHandle )
	local obj = getObjectFind()
	if obj == nil then return end
	detachScript(obj.id)
end

function attachParticle()
	MenuOpen("ParticleBrowser.xml")
end

function detachParticle(objId)
	
	KEP_RemoveParticleSystemFromDynamicObject(objId)
	
	local e = KEP_EventCreate( "ControlDynamicObjectEvent")
	KEP_EventSetObjectId(e, objId)
	KEP_EventSetFilter(e, KEP.PARTICLE_STOP)
	KEP_EventAddToServer(e)
	KEP_EventEncodeNumber(e, 1) -- argument count
	KEP_EventEncodeNumber(e, 0) -- no particle
	KEP_EventQueue(e)

	Log("detachParticle: objId="..objId.." - Calling updateObjects()")
	updateObjects()
end

function btnAttachParticle_OnButtonClicked( buttonHandle )
	attachParticle()
end

function btnDetachParticle_OnButtonClicked( buttonHandle )
	local obj = getObjectFindSelected()
	if obj == nil then return end
	detachParticle(obj.id)
end

function setAnimation(obj)
	MenuOpen("ChooseAnimation.xml")
	local e = KEP_EventCreate( ITEM_SELECTED_EVENT )
	KEP_EventEncodeNumber(e, obj.invType )
	KEP_EventEncodeNumber(e, 1) -- num items
	KEP_EventEncodeNumber(e, obj.glid)
	KEP_EventEncodeNumber(e, obj.id)
	KEP_EventEncodeNumber(e, 1) --quantity of item
	KEP_EventEncodeString(e, obj.name ) -- name
	KEP_EventEncodeNumber(e, 0 ) -- price
	KEP_EventQueue(e)
end

function btnSetAnim_OnButtonClicked( buttonHandle )
	local obj = getObjectFindSelected()
	if(obj == nil) then return end
	setAnimation(obj)
end

function editSearch_OnEditBoxChange( editBoxHandle )
	g_queryString = EditBox_GetText( editBoxHandle )
	if (g_queryString ~= nil) and (string.len(g_queryString)>= 0) then
		updateObjects()
	end
end

function editSearch_OnEditBoxString( editBoxHandle )
	MenuClearFocusThis()
end

function btnName_OnButtonClicked( buttonHandle )
	if Sort_ObjectName == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_ObjectName)
end

function btnPID_OnButtonClicked( buttonHandle )
	if Sort_ObjectPID == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_ObjectPID)
end

function btnScript_OnButtonClicked( buttonHandle )
	if Sort_ScriptName == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_ScriptName)
end

function btnAnim_OnButtonClicked( buttonHandle )
	if Sort_AnimationName == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_AnimationName)
end

function btnParticle_OnButtonClicked( buttonHandle )
	if Sort_ParticleName == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_ParticleName)
end

function btnXPos_OnButtonClicked( buttonHandle )
	if Sort_PositionX == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_PositionX)
end

function btnYPos_OnButtonClicked( buttonHandle )
	if Sort_PositionY == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_PositionY)
end

function btnZPos_OnButtonClicked( buttonHandle )
	if Sort_PositionZ == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjectsFind(Sort_PositionZ)
end

function sortObjectsFind(sortType)	
	g_SortingBy = sortType
	table.sort(g_objectsFind, g_SortFuncs[sortType])	
	populateList()
end

function sortByObjectName(itemA, itemB)
	local aName = string.lower(itemA.name)
	local bName = string.lower(itemB.name)
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	else
		if itemA.id == itemB.id then --lua compares the same script sometimes. This also catches the recursion when sortByObjectID() calls sortByScriptName().
			return false
		end
		return sortByObjectPID(itemA, itemB)
	end
end

function sortByObjectPID(itemA, itemB)
	if g_bSortAscending then
		return itemA.id < itemB.id
	else
		return itemA.id > itemB.id
	end
end

function sortByScriptName(itemA, itemB)
	local aName = string.lower(itemA.scriptName)
	local bName = string.lower(itemB.scriptName)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	else
		return sortByAnimationName(itemA, itemB)
	end
end

function sortByAnimationName(itemA, itemB)
	local aName = string.lower(itemA.animationName)
	local bName = string.lower(itemB.animationName)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	else
		return sortByParticleName(itemA, itemB)
	end
end

function sortByParticleName(itemA, itemB)
	local aName = string.lower(itemA.particleName)
	local bName = string.lower(itemB.particleName)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	else
		return sortByObjectName(itemA, itemB)
	end
end

function sortByObjectX(itemA, itemB)
	local aName = string.lower(itemA.pos.x)
	local bName = string.lower(itemB.pos.x)
	if aName ~= bName then
		if g_bSortAscending then
			return tonumber(aName) < tonumber(bName)
		else
			return tonumber(aName) > tonumber(bName)
		end
	else
		return sortByScriptName(itemA, itemB)
	end
end

function sortByObjectY(itemA, itemB)
	local aName = string.lower(itemA.pos.y)
	local bName = string.lower(itemB.pos.y)
	if aName ~= bName then
		if g_bSortAscending then
			return tonumber(aName) < tonumber(bName)
		else
			return tonumber(aName) > tonumber(bName)
		end
	else
		return sortByScriptName(itemA, itemB)
	end
end

function sortByObjectZ(itemA, itemB)
	local aName = string.lower(itemA.pos.z)
	local bName = string.lower(itemB.pos.z)
	if aName ~= bName then
		if g_bSortAscending then
			return tonumber(aName) < tonumber(bName)
		else
			return tonumber(aName) > tonumber(bName)
		end
	else
		return sortByScriptName(itemA, itemB)
	end
end

g_SortFuncs =	
{
	sortByObjectName,
	sortByObjectPID,
	sortByScriptName,
	sortByAnimationName,
	sortByParticleName,
	sortByObjectX,
	sortByObjectY,
	sortByObjectZ
}
