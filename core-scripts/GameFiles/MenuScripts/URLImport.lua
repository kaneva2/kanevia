--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")

gDropId = -1
gImportInfo = nil
gTargetInfo = nil
gBtnState = 0
gKeyState = 0

PARSE_URL_PAGE = "kgp/parseurl.aspx?"
PARAM_URL = "requrl="
WEB_ADDRESS_PLAYLIST_MEDIUM_DROP = GameGlobals.WEB_SITE_PREFIX.."kgp/playList.aspx?type=d"

function UGCImportMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	gDropId = UGCI.GetDialogInstanceId( gDialogHandle )
	local dlgId = Dialog_GetId( gDialogHandle )

	if filter~=dlgId or objectid~=gDropId then
		-- Ignore event with mismatched IDs
		return
	end

	local eventData = UGCI.ParseUGCImportMenuEvent( event )
	gImportInfo = eventData.importInfo
	gTargetInfo = eventData.targetInfo
	gBtnState = eventData.btnState
	gKeyState = eventData.keyState

	Log( "UGCImportMenuEventHandler: dlgId=" .. dlgId .. " dropId=" .. gDropId .. " type=" .. gImportInfo.type .. " path=" .. gImportInfo.fullPath )

	if gImportInfo.category==UGC_TYPE.URL then
		URLImportAsync( gImportInfo, gTargetInfo, gBtnState, gKeyState )
	else
		MenuCloseThis()
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "UGCImportMenuEventHandler", UGC_IMPORT_MENU_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

URLParsingRequests = {}
URLParsingRequestId = 0

function url_encode(str)
	if str then
		str = string.gsub (str, "([^%w ])",
		function (c) return string.format ("%%%02X", string.byte(c)) end)
		str = string.gsub (str, " ", "+")
	end
	return str
end

function URLImportAsync( importInfo, target, btnState, keyState )

	local msg = "Importing " .. importInfo.fullPath
	Log( "URLImportAsync: "..msg )
	KEP_SendChatMessage( ChatType.System, msg )

	-- record as pending request
	URLParsingRequestId = URLParsingRequestId + 1
	local sRequestId = tostring(URLParsingRequestId)
	URLParsingRequests[sRequestId] = {}
	URLParsingRequests[sRequestId].state = 0		-- pending
	URLParsingRequests[sRequestId].url = importInfo.fullPath
	URLParsingRequests[sRequestId].targetId = target.id
	URLParsingRequests[sRequestId].targetType = target.type

	local urlParseUrl = GameGlobals.WEB_SITE_PREFIX .. PARSE_URL_PAGE .. PARAM_URL .. url_encode(importInfo.fullPath) .. "&reqid=" .. sRequestId
	Log( "URLImportAsync: Request url=" .. urlParseUrl )
	makeWebCall( urlParseUrl, WF.PARSE_ASSET_URL, 1, 1 )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == WF.PARSE_ASSET_URL then

		local parseResult = KEP_EventDecodeString( event )

		local s, e = 1
		local resultCode = ""
		local requestId = ""
		local sUrlType = ""
		local urlProvider = ""

		s, e, resultCode = string.find( parseResult, "<ReturnCode>(.-)</ReturnCode>")
		s, e, requestId = string.find( parseResult, "<RequestId>(.-)</RequestId>")
		s, e, resultUrl = string.find( parseResult, "<ReturnUrl>(.-)</ReturnUrl>")
		s, e, sUrlType = string.find( parseResult, "<UrlType>(.-)</UrlType>")
		s, e, urlProvider = string.find( parseResult, "<UrlProvider>(.-)</UrlProvider>")

		if resultUrl ==nil then
			resultUrl = ""
		end

		if urlProvider==nil then
			urlProvider = ""
		end

		local urlType = -1
		if sUrlType~=nil and sUrlType~="" then
			urlType = tonumber( sUrlType )
		end

		local internalError = true

		if requestId~="" and requestId~=nil and resultCode~="" and resultCode~=nil then

			if URLParsingRequests[requestId]~=nil then

				if URLParsingRequests[requestId].state==0 then

					local targetId, targetType, url

					targetId = URLParsingRequests[requestId].targetId
					targetType = URLParsingRequests[requestId].targetType
					url = URLParsingRequests[requestId].url

					URLParsingRequests[requestId].state = 1

					if resultCode == "0" then

						LogError( "BrowserPageReadyHandler: External URL parsing failed: " .. url )

					elseif resultCode == "1" then

						if urlType==URLTYPE.SupportedVideoUrl and urlProvider=="youtube" then

							msg = "Accepting URL as external video, provider: " .. urlProvider
							KEP_SendChatMessage( ChatType.System, msg )

							if targetType == ObjectType.DYNAMIC then
								local offsiteAssetId = ""
								s, e, offsiteAssetId = string.find( parseResult, "<OffsiteAssetId>(.-)</OffsiteAssetId>")
								local playlistDropUrl = WEB_ADDRESS_PLAYLIST_MEDIUM_DROP
								playlistDropUrl = playlistDropUrl .. "&tt=" .. targetType .. "&ti=" .. targetId		-- target type and target Id (garbage-in garbage-out)
								playlistDropUrl = playlistDropUrl .. "&at=" .. eASSET_TYPE.VIDEO .. "&as=" .. eASSET_SUBTYPE.YOUTUBE .. "&xa=" .. offsiteAssetId
								playlistDropUrl = playlistDropUrl .. "&t=untitled&pv=1&r=0&ts=untitled&c=0"
								playlistDropUrl = playlistDropUrl .. "&p=" .. GameGlobals.UNFILED_MEDIA_PLAYLIST_NAME
								makeWebCall( playlistDropUrl, WF.INSTANT_PLAYBACK, 1, 1)
							else
								KEP_MessageBox( "Cannot update video for selected object", "Error" )
							end
							internalError = false
						else
							LogError( "BrowserPageReadyHandler: Unsupported external URL - type=" .. urlType .. " provider=" .. urlProvider )
						end
					end
				end
			end
		end

		if internalError then
			KEP_MessageBox( "Unknown URL: " .. gImportInfo.fullPath, "Error" )
		end

	elseif filter == WF.INSTANT_PLAYBACK then
		-- Play returned playlist now
		local parseResult = KEP_EventDecodeString( event )

		-- parse returned XML to retrieve asset_id
		local asset_group_id, targetType, targetId
		asset_group_id, targetType, targetId = ParsePlaylistXML( parseResult )

		if asset_group_id ~= nil then

			if targetType==ObjectType.DYNAMIC then

				-- send change play list request to server
				local e = KEP_EventCreate( BrowserIds.BROWSER_REQUEST_EVENT_NAME )
				local req = "changePlaylist?plId="..tostring(asset_group_id).."&objId=" .. targetId -- tack on the objectId
				KEP_EventEncodeString( e, req )
				KEP_EventAddToServer( e )
				KEP_EventQueue( e )
			else
				-- print an error message
				LogError( "BrowserPageReadyHandler: Error processing dropped URL: unsupported target type " .. targetType )
				KEP_MessageBox( "Unable to process dropped URL: " .. gImportInfo.fullPath, "Error" )
			end
		else
			-- print an error message
			LogError( "BrowserPageReadyHandler: Error parsing returned XML" )
			KEP_MessageBox( "Unable to process dropped URL: " .. gImportInfo.fullPath, "Error" )
		end

		MenuCloseThis()
	end
end

-- Process returned XML from playlist.aspx
--	 return asset_group_id of first record (should be only one record returned), targetType, targetId
function ParsePlaylistXML( xmlMessage )
	local s = 0
	local numRec = 0
	local strPos = 0
	local result = ""
	local playlistURL = ""
	local targetId = 0
	local targetType = ObjectType.UNKNOWN

	s, e, result = string.find( xmlMessage, "<ReturnCode>([0-9]+)</ReturnCode>" )

	if result == "0" then
		local sTargetType = nil
		local sTargetId = nil
		s, strPos, g_totalNumRec = string.find(xmlMessage, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
		s, strPos, numRec = string.find(xmlMessage, "<NumberRecords>([0-9]+)</NumberRecords>")
		s, strPos, playlistURL = string.find(xmlMessage, "<PlayListURL>(.-)</PlayListURL>")
		s, strPos, sTargetType = string.find(xmlMessage, "<targetType>([0-9]+)</targetType>")
		s, strPos, sTargetId = string.find(xmlMessage, "<targetId>([0-9]+)</targetId>")

		local playliststr = ""
		s, strPos, playliststr = string.find(xmlMessage, "<PlayList>(.-)</PlayList>", strPos)

		local asset_group_id = -1
		s, strPos, asset_group_id = string.find(playliststr, "<asset_group_id>([-]?[0-9]+)</asset_group_id>", 1)
		local name = ""
		s, strPos, name = string.find(playliststr, "<name>(.-)</name>", 1)
		local asset_count = 0
		s, strPos, asset_count = string.find(playliststr, "<asset_count>([0-9]+)</asset_count>", 1)

		if sTargetId~=nil then
			targetId = tonumber(sTargetId)
		end
		if sTargetType~=nil then
			targetType =tonumber(sTargetType)
		end

		return asset_group_id, targetType, targetId
	end

	return nil
end
