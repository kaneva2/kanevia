--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("MenuTemplateHelper.lua")
dofile("MenuLocation.lua")

CFG_SHOW_WELCOME = "showWelcome"
CFG_SIZE_WELCOME = "sizeWelcome"

g_userClose = true

gMaximize 		= true
gMaxiElements 	= {}
gLeftButton 	= 0
gPrevMouseX		= 0
gPrevMouseY		= 0

function playerGoSubZone( urlSuffix )
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		local ok, url = KEP_ConfigGetString( config, "CurrentZoneURL", "" )
		if url ~= nil and url ~= "" then
			url = url .. urlSuffix
			local ev = KEP_EventCreate( KEP.GOTOPLACE_EVENT_NAME )
			KEP_EventSetFilter( ev, KEP.GOTO_URL )
			KEP_EventEncodeNumber( ev, -1 )
			KEP_EventEncodeString ( ev, url )
			KEP_EventAddToServer( ev )
			KEP_EventQueue( ev )
			g_userClose = false
			MenuCloseThis()
		end
	end
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "PersistMenuEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	setupElements()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		local ok1, name = KEP_ConfigGetString( config, CFG_SHOW_WELCOME, "true" )
		local chkDontShow = Dialog_GetCheckBox(gDialogHandle, "cbDontShow")
		if name == "true" then
			CheckBox_SetChecked( chkDontShow, false )
		else
			MenuCloseThis()
		end
		local ok2, size = KEP_ConfigGetString( config, CFG_SIZE_WELCOME, "max" )

		if size == "min" then
			gMaximize = false
			Dialog_SetModal( dialogHandle, 0 )
			setupDialogBase( dialogHandle, 337, 400, true, false, false )
			loadLocation(dialogHandle, "Welcome" )
			Dialog_SetCaptionText( dialogHandle, "Get started..." )

			-- Ensure that resize button is "+" when the window is minimized.
			local sh = Dialog_GetStatic(gDialogHandle, "btnResize")
			Static_SetText(sh, "+")
		else
			gMaximize = true
			Dialog_SetModal( dialogHandle, 1 )
			setupDialogBase( dialogHandle, 658, 499, true, true, true )

			-- Ensure that resize button is "_" when the window is maximized.
			local sh = Dialog_GetStatic(gDialogHandle, "btnResize")
			Static_SetText(sh, "_")
		end
	end
	setMaximize()
end

function Dialog_OnDestroy(dialogHandle)
	if g_userClose == false then
		local ev = KEP_EventCreate( "PersistMenuEvent" )
		KEP_EventEncodeString ( ev, "Welcome.xml" )
		KEP_EventQueue( ev )
	end
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnMoved( dialogHandle, x, y )
	if gMaximize == false then
		saveLocation( dialogHandle, "Welcome" )
	end
end

function Dialog_OnLButtonUp( dialogHandle, x, y )
	if gMaximize == false then
		saveLocation(dialogHandle, "Welcome" )
	end
end

function Dialog_OnRender( dialogHandle, elapsedSec )
	local down = Dialog_IsLMouseDown( dialogHandle )
	if gLeftButton == 0 and down == 1 then
		gLeftButton = 1
	elseif gLeftButton == 1 and down == 0 then
		gLeftButton = 0
		Dialog_OnLButtonUp( dialogHandle, gPrevMouseX, gPrevMouseY )
	end
end

function Dialog_OnMouseMove( dialogHandle, x, y )
	gPrevMouseX = x
	gPrevMouseY = y
end

function btnResize_OnButtonClicked( buttonHandle )
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		gMaximize = not gMaximize
		if gMaximize then
			KEP_ConfigSetString( config, CFG_SIZE_WELCOME, "max" )
		else
			KEP_ConfigSetString( config, CFG_SIZE_WELCOME, "min" )
		end
		KEP_ConfigSave( config )
		MenuOpen("Welcome.xml")

		MenuCloseThis()
	end
end

function cbDontShow_OnCheckBoxChanged()
	local config = KEP_ConfigOpenWOK()
	local chkDontShow = Dialog_GetCheckBox( gDialogHandle, "cbDontShow" )
	if CheckBox_GetChecked(chkDontShow) == 1 then
		if config ~= nil then
			KEP_ConfigSetString( config, CFG_SHOW_WELCOME, "false" )
			KEP_ConfigSave( config )
		end
	else
		if config ~= nil then
			KEP_ConfigSetString( config, CFG_SHOW_WELCOME, "true" )
			KEP_ConfigSave( config )
		end
	end
end

function btnLink0_OnButtonClicked( buttonHandle )
	MenuOpen("CharacterCreation3.xml")
	local ev = KEP_EventCreate("FRAMEWORK_INIT_CHAR_CREATOR")
	KEP_EventEncodeString(ev, "player")
	KEP_EventQueue(ev)
	g_userClose = false
	MenuCloseThis()
end

function btnLink1_OnButtonClicked( buttonHandle )
	playerGoSubZone( "Mall.zone#mall_west_shop6" ) -- "Avenak Furniture"
end

function btnLink2_OnButtonClicked( buttonHandle )
	playerGoSubZone( "Mall.zone#mall_default" )
end

function btnLink3_OnButtonClicked( buttonHandle )
	playerGoSubZone( "Kaneva City.zone#city_default" )
end

function btnLink4_OnButtonClicked( buttonHandle )
	playerGoSubZone( "Employment Agency.zone#employment_default" )
end

function btnLink5_OnButtonClicked( buttonHandle )
	playerGoSubZone( "Ka-Ching! Lobby.zone#kaching_default" )
end

function btnLink6_OnButtonClicked( buttonHandle )
	playerGoSubZone( "The Third Dimension.zone#dance_default" )
end

function btnLink7_OnButtonClicked( buttonHandle )
	MenuOpen("WorldMap.xml")
end

function btnLink8_OnButtonClicked( buttonHandle )
	KEP_LaunchBrowser(GameGlobals.WEB_SITE_PREFIX_KANEVA .. "/sharekaneva" )
end

function setupElements()
	local name = ""
	local c = nil
	for i = 0, 8 do
		name = "imgFooter" .. tostring( i )
		c = Dialog_GetControl( gDialogHandle, name )
		table.insert( gMaxiElements, c )
	end
	for i = 1, 2 do
		name = "txtFooter" .. tostring( i )
		c = Dialog_GetControl( gDialogHandle, name )
		table.insert( gMaxiElements, c )
	end

	c = Dialog_GetControl( gDialogHandle, "cbDontShow" )
	table.insert( gMaxiElements, c )

	c = Dialog_GetControl( gDialogHandle, "txtWelcome1" )
	table.insert( gMaxiElements, c )

	for i = 0, 8 do
		name = "btnLink" .. tostring( i )
		c = Dialog_GetControl( gDialogHandle, name )
		local x = Control_GetLocationX( c )
		local y = Control_GetLocationY( c )
		gMaxiElements[ name ] = { x, y }
	end
end

function setMaximize()
	for i, c in ipairs( gMaxiElements ) do
		Control_SetVisible( c, gMaximize )
		Control_SetEnabled( c, gMaximize )
	end
	setLinkLocations()
end

function setLinkLocations()
	for i = 0, 8 do
		local name = "btnLink" .. tostring( i )
		local c = Dialog_GetControl( gDialogHandle, name )
		local x, y = 25, 45
		if gMaximize then
			x = gMaxiElements[ name ][ 1 ]
			y = gMaxiElements[ name ][ 2 ]
		else
			y = y + ( 25 * ( i - 1 ) )
		end
		Control_SetLocation( c, x, y )
	end
end
