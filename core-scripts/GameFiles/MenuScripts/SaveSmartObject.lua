--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- SmartSmartObject.lua - Name Input for Saving As
-- 
-- Opened from PresetEditor.lua. On Save As, sends an event to PresetEditor to let it know the name.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\MenuHelper.lua")

local m_type = nil --nil until type set via SetupSaveEventHandler

local g_cancel = true

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "SetupSaveEventHandler", "SetupSaveEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "SmartObjectSaveName", KEP.MED_PRIO )
	KEP_EventRegister( "SetupSaveEvent", KEP.MED_PRIO )
	KEP_EventRegister( "SavePlaylistNameEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	if g_cancel == true and m_type == "SmartObject" then
		sendSmartObjectSaveNameEvent(0)
	end
	Helper_Dialog_OnDestroy( dialogHandle )
end

function SetupSaveEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	MenuBringToFrontThis()

	m_type = KEP_EventDecodeString(event)

	if m_type == "Playlist" then
		Dialog_SetCaptionText(gDialogHandle, "New Playlist")
	
	elseif m_type == "SmartObject" then
		local name = KEP_EventDecodeString(event)
		EditBox_SetText(gHandles["edName"], name .. "Copy", true)
	end

	return KEP.EPR_CONSUMED
end

function btnSave_OnButtonClicked( dialogHandle )
	local name = EditBox_GetText(gHandles["edName"])
	if m_type == "Playlist" then
		savePlaylist(name)
	elseif m_type == "SmartObject" then
		saveSmartObject(name)
	end
end

function savePlaylist(name)

	-- Check to make sure name inputted
	if name == nil or name == "" then
		KEP_MessageBox("Please enter a name for your new Playlist.")
		return
	end

	local event = KEP_EventCreate("SavePlaylistNameEvent")
	KEP_EventEncodeString(event, name)
	KEP_EventQueue(event)

	g_cancel = false
	MenuCloseThis()
end

function saveSmartObject(name)

	-- Check to make sure name inputted
	if name == nil or name == "" then
		KEP_MessageBox("Please enter a name for your new Smart Object.")
		return
	end

	-- Check to make sure name is only alphanumeric
	if string.find(name, "%W") then
		KEP_MessageBox("Smart Object names my only contain letters and numbers.")
		return
	end

	-- Check to make sure name isn't only numbers
	if string.find(name, "%a") == nil then
		KEP_MessageBox("Smart Object names must contain at least one letter.")
		return
	end

	sendSmartObjectSaveNameEvent(1, name)

	g_cancel = false
	MenuCloseThis()
end

function sendSmartObjectSaveNameEvent(res, name)
	local event = KEP_EventCreate("SmartObjectSaveName")
	KEP_EventSetFilter(event, res)
	if name then
		KEP_EventEncodeString(event, name)
	end
	KEP_EventQueue(event)
end
