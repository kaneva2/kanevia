--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")
dofile("..\\Scripts\\LWG.lua")
dofile("..\\Scripts\\EquippableImportStateIds.lua")
dofile("..\\Scripts\\CoreItems.lua")
dofile("MountSlots.lua")

-- DRF - Added ability to toggle Mark's axis fixes
-- Fixes mirror X & Y axis so upright bone slots (eg. head) import accessories
-- in the same upright orientation as an import as an object would for the same model.
-- The legacy bug is that these avatar bone slots have flipped X/Y axis orientation.
-- World Axis: Green=Up, Red=Left, Blue=Forward
-- Bone Axis: Red=Up, Green=Left, Blue=Forward
g_fixAxis = true -- mark's axis fixes

-- DRF - ED-1937 - Accessories Need 3 Decimals
-- Setting Limits
DEC_SETTING = 3 -- 3 decimals precision (1/80")
MIN_SETTING = 0.001
MAX_SETTING = 1000000.0

-- Scale Factor Limits (scale% / 100.0)
SCALEFACTOR_DEF = (100.0 / 100.0)
SCALEFACTOR_MIN = (MIN_SETTING / 100.0)
SCALEFACTOR_MAX = (MAX_SETTING / 100.0)

SIZE_MIN = 0.1	-- Warn if any of the three dimension is measured less than 0.1 unit

g_actorScale = { x=1.0, y=1.0, z=1.0 }

-- Camera Default Settings
CAM_DIST = 5.0 -- default distance
CAM_MIN_DIST = 2.0 -- default minimum distance (distance camera enters 1st person mode)
CAM_DIST_STEP = 0.01 -- default mouse wheel distance step (one click is 10x this value)
CAM_NEAR_PLANE_DIST = 0.1 -- default near plane distance (closer then this isn't rendered)

g_mountSlotIsSet = false

g_continueImport = false

gAdjustCamera = true -- enable/disable camera adjustments

gShowPersonalSpaceBoundBox = 0
gDefaultMountSlot = gMountSlotIDs.HEADWEAR.ID

gOptionNames = { 
	UGCOPT_EI_MAXTRICOUNT, 
	UGCOPT_EI_MAXSIZE, 
	UGCOPT_PERSONALSPACEMINX, 
	UGCOPT_PERSONALSPACEMAXX, 
	UGCOPT_PERSONALSPACEMINY, 
	UGCOPT_PERSONALSPACEMAXY, 
	UGCOPT_PERSONALSPACEMINZ, 
	UGCOPT_PERSONALSPACEMAXZ 
}
gOptions = nil

gActorTypes = actorTypes	-- see UGCGlobals.lua

gImportStateGetSetIds = EquippableImportStateIds

gDropId = -1
gImportInfo = nil
gTargetInfo = nil
gCurrNPCGender = nil
gAIMounted = 0

g_widgetEnable = true
g_updateWidgetEnable = 0.0

gLocalGLID = 0
gPlayerGender = "M"
gBoneIndex = nil

g_originalSizeX = 0
g_originalSizeY = 0
g_originalSizeZ = 0
g_currentSizeX = 0
g_currentSizeY = 0
g_currentSizeZ = 0

g_currentSizeStr = nil
g_importBoundBoxGeomId = -1
g_axisXGeomId = -1
g_axisYGeomId = -1
g_axisZGeomId = -1
g_personalSpaceBoundBoxId = -1

WHITE = {r = 1, g = 1, b = 1, a = 1}
RED = {r = 1, g = 0, b = 0, a = 1}
GRN = {r = 0, g = 1, b = 0, a = 1}
BLU = {r = 0, g = 0, b = 1, a = 1}

g_boundBoxMaterial = { ambient = WHITE, diffuse = WHITE, specular = WHITE, emissive = WHITE, power = 0.4 } -- Make it pure white regardless of lighting
g_axisXMaterial = { ambient = RED, diffuse = RED, specular = RED, emissive = RED, power = 0.4 }
g_axisYMaterial = { ambient = GRN, diffuse = GRN, specular = GRN, emissive = GRN, power = 0.4 }
g_axisZMaterial = { ambient = BLU, diffuse = BLU, specular = BLU, emissive = BLU, power = 0.4 }
g_personalSpaceBoxMaterial = { -- translucent purple
    ambient = { r = 0.8, g = 0.2, b = 0.8, a = 0.5 },
    diffuse = { r = 0.2, g = 0.0, b = 0.2, a = 0.5 },
}

-- Cached control handles
rbAttachRight = nil
rbAttachLeft = nil

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "UGCImportMenuEventHandler", UGC_IMPORT_MENU_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "YesNoBoxHandler", "YesNoAnswerEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "EquippableItemWidgetTranslateHandler", EQUIPPABLE_ITEM_TRANSLATE_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UGCGlobalOptionsResultHandler", UGC_GLOBAL_OPTIONS_RESULT_EVENT, KEP.MED_PRIO )

	KEP_EventRegisterHandler( "selectEventHandler", "SelectEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( UGC_IMPORT_MENU_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( YES_NO_BOX_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "YesNoAnswerEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( CHANGE_BUILD_MODE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( EQUIPPABLE_ITEM_TRANSLATE_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- DRF - Build Mode Must Be Closed While Importing Accessories!
	local e = KEP_EventCreate("EnterBuildModeEvent")
	KEP_EventSetFilter(e, 0) -- close
	KEP_EventQueue(e)

	-- Lock Build Mode
	local e = KEP_EventCreate("EnterBuildModeEvent")
	KEP_EventSetFilter(e, 4) -- lock
	KEP_EventQueue(e)

	-- Use Legacy Axis? (Shift key down on menu launch)
	if ToBool(KEP_IsKeyDown(Keyboard.SHIFT)) then
		LogWarn("USING LEGACY AXIS")
		g_fixAxis = false
	end

	-- Save current camera settings
	SaveCamera()

	-- get player gender
	gPlayerGender = KEP_GetLoginGender()

	local ch = Dialog_GetComboBox(dialogHandle, "cbPredefinedUnitConversion")
	for idx, rec in ipairs(predefinedUnits) do
		ComboBox_AddItem(ch, rec.name, math.floor( rec.ratio*1000 ))	-- ComboBox_AddItem only takes integer data, need to /1000 when used
	end
	setupComboBoxScrollBarDefaultTextures( "cbMountSlots" )

	local ch = Dialog_GetComboBox(dialogHandle, "cbMountSlots")
	for idx, slot in ipairs(gMountSlots) do
		ComboBox_AddItem(ch, slot.Name, idx )
	end
	ComboBox_SetScrollBarWidth( ch, 16 )

	UpdateMountSlotsParameters()

	rbAttachRight = Dialog_GetRadioButton( gDialogHandle, "rbAttachRight" )
	rbAttachLeft = Dialog_GetRadioButton( gDialogHandle, "rbAttachLeft" )
	enableControl( "rbAttachRight", false )
	enableControl( "rbAttachLeft", false )

	-- reset adjustments
	fixEditBoxNumber("edTX")
	fixEditBoxNumber("edTY")
	fixEditBoxNumber("edTZ")
	fixEditBoxNumber("edRX")
	fixEditBoxNumber("edRY")
	fixEditBoxNumber("edRZ")
end

function Dialog_OnDestroy(dialogHandle)

	if (gImportInfo and gImportInfo.sessionId and (gImportInfo.sessionId ~= -1)) then
		if (g_continueImport == true) then
			UGC_ContinueImportAsync( gImportInfo.sessionId )
		else
			UGC_CancelImportAsync( gImportInfo.sessionId )
		end
	end
		
	DisposeImportBoundBox( )
	UnloadNPC()
	DisarmImportedItem()

	-- Restore original camera settings
	RestoreCamera()
	KEP_LockSelection( 0) -- re-enable selection
	KEP_ClearSelection( )
	
	-- Turn Off Build Widget
	KEP_TurnOffWidget()
	
	-- Unlock Build Mode
	local e = KEP_EventCreate("EnterBuildModeEvent")
	KEP_EventSetFilter(e, 5) -- unlock
	KEP_EventQueue(e)

	Helper_Dialog_OnDestroy( dialogHandle )
end

function updateWidgetEnable()
	Log("updateWidgetEnable: widget="..tostring(g_widgetEnable))
	if g_widgetEnable then
		KEP_WidgetWorldSpace()
		KEP_DisplayTranslationWidget(ObjectType.EQUIPPABLE)
	else
		KEP_TurnOffWidget()
	end
end

function updateWidgetPosition()
	if not g_widgetEnable or (ObjectsSelected() < 1) then return end
	local pos = ObjectsSelectedGetPosition()
	KEP_SetWidgetPosition(pos.x, pos.y, pos.z)
end

function UGCImportMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	gDropId = UGCI.GetDialogInstanceId( gDialogHandle )
	local dlgId = Dialog_GetId( gDialogHandle )

	-- Ignore event with mismatched IDs
	if filter~=dlgId or objectid~=gDropId then return end

	local eventData = UGCI.ParseUGCImportMenuEvent( event )
	gImportInfo = eventData.importInfo
	gTargetInfo = eventData.targetInfo
	gBtnState = eventData.btnState
	gKeyState = eventData.keyState

	Log( "UGCImportMenuEventHandler: '"..gImportInfo.fullPath.."' type=" .. gImportInfo.type)
	Log( "UGCImportMenuEventHandler: ... dlgId=" .. dlgId .. " dropId=" .. gDropId .. " sessionId=" .. gImportInfo.sessionId)

	if gImportInfo.type ~= UGC_TYPE.EQUIPPABLE then
		LogError( "UGCImportMenuEventHandler: don't know how to handle import type=" .. gImportInfo.type )
		MenuCloseThis()
		return
	end

	if gImportInfo.sessionId == -1 then
		LogError( "UGCImportMenuEventHandler: no import session" )
		MenuCloseThis()
		return
	end

	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then
		LogError( "UGCImportMenuEventHandler: no import session" )
		MenuCloseThis()
		return
	end

	gLocalGLID = GetSet_GetNumber( impSession, gImportStateGetSetIds.LOCALGLID )
	gAIMounted = GetSet_GetNumber( impSession, gImportStateGetSetIds.AIMOUNTED )
	gActorGroup = GetSet_GetNumber( impSession, gImportStateGetSetIds.TARGETACTORTYPE )

	Log("UGCImportMenuEventHandler: ... impSession="..tostring(impSession).." glid<"..gLocalGLID.."> aiMounted="..gAIMounted.." actorGroup="..gActorGroup)

	if gActorGroup==0 and gActorTypes~=actorTypesBoth then
		-- if "BOTH" gender option is disabled: default to player gender
		SetActorGroup(gPlayerGender)
	end

	-- Check "Auto-Correct Ambient" option by default				
	ch = Dialog_GetCheckBox(gDialogHandle, "cbAutoCorrectAmbient")
	CheckBox_SetChecked( ch, 1 )
	GetSet_SetNumber( impSession, gImportStateGetSetIds.AUTOCORRECTAMBIENT, 1 )

	-- Get original bounding box
	local minX, minY, minZ
	minX, minY, minZ = GetSet_GetVector( impSession, gImportStateGetSetIds.BOUNDBOXMIN )
	local maxX, maxY, maxZ
	maxX, maxY, maxZ = GetSet_GetVector( impSession, gImportStateGetSetIds.BOUNDBOXMAX )

	-- Set Original Size
	g_originalSizeX = maxX - minX;
	g_originalSizeY = maxY - minY;
	g_originalSizeZ = maxZ - minZ;

	-- Update Scale (validates & updates dimensions)
	local scale = GetSet_GetNumber( impSession, gImportStateGetSetIds.SCALEFACTOR )
	UpdateScale(scale)

	Log("UGCImportMenuEventHandler: ... size=["..g_originalSizeX.." "..g_originalSizeY.." "..g_originalSizeZ.."] scale="..scale)

	-- setup mounting slot / mirroring
	local currSlot = 0
	local useMirror = 0
	if g_mountSlotIsSet then
		currSlot, ok = GetSet_GetNumberByName( impSession, "MountSlot", 0 )
		if (ok == 0) then 
			LogError("UGCImportMenuEventHandler: FAILED GetSet_GetNumberByName(MountSlot)")
		end

		useMirror, ok = GetSet_GetNumberByName( impSession, "UseMirror", 0 )
		if (ok == 0) then 
			LogError("UGCImportMenuEventHandler: FAILED GetSet_GetNumberByName(UseMirror)")
		end
	end
				
	-- First Time ?
	if currSlot==0 then
		if gDefaultMountSlot~=nil then
			-- pre set mounting slot to default selection
			currSlot, useMirror = GetMountSlotIndexByExclusionGroup( gDefaultMountSlot )
		else
			-- otherwise, use first mount slot
			currSlot, useMirror = 1, 0
		end
	end

	Log("UGCImportMenuEventHandler: ... currSlot="..currSlot.." useMirror="..useMirror)

	-- select mounting slot
	selectComboBoxItemByData( "cbMountSlots", currSlot )

	-- select side for symmetric slots
	if (gDefaultSide == 'R') and (useMirror == 0) or (gDefaultSide == 'L') and (useMirror == 1) then
		CheckBox_SetChecked( RadioButton_GetCheckBox( rbAttachRight ), 1 )
		CheckBox_SetChecked( RadioButton_GetCheckBox( rbAttachLeft ), 0 )
	else
		CheckBox_SetChecked( RadioButton_GetCheckBox( rbAttachRight ), 0 )
		CheckBox_SetChecked( RadioButton_GetCheckBox( rbAttachLeft ), 1 )
	end

	SetImportAnim()

	UpdateMountSlot()

	ArmImportedItem()

	-- Request UGC global options for EI
	UGCI.GetGlobalOptions( UGC_FILTER.EI_IMPORT_OPTS, gOptionNames )
end

function btnConfirm_OnButtonClicked( buttonHandle )
	if not CheckPersonalSpaceConstraint() then
		LogError("CheckPersonalSpaceConstraint() FAILED")
		local psbSizeX = gOptions[UGCOPT_PERSONALSPACEMAXX] - gOptions[UGCOPT_PERSONALSPACEMINX]
		local psbSizeY = gOptions[UGCOPT_PERSONALSPACEMAXY] - gOptions[UGCOPT_PERSONALSPACEMINY]
		local psbSizeZ = gOptions[UGCOPT_PERSONALSPACEMAXZ] - gOptions[UGCOPT_PERSONALSPACEMINZ]
		local maxSizeMsg = "" .. psbSizeX .. "x" .. psbSizeY .. "x" .. psbSizeZ
		UGCI.DisplayImportFailedMenu( "UGCImportRadiusWarning", { maxSizeMsg, "", "Accessories" } )
		return
	end

	if not FinalCheckScaleFactor() then
		LogError("FinalCheckScaleFactor() FAILED")
		return
	end

	g_continueImport = true

	MenuCloseThis()
end

-- Select a mount slot from list
function cbMountSlots_OnComboBoxSelectionChanged(comboBoxHandle, sel_index, sel_text)
	DisarmImportedItem()
	UpdateMountSlot()
	ArmImportedItem()
	MenuClearFocusThis()
end

-- Change mounting side
function OnAttachSideChanged( radioButtonHandle )
	DisarmImportedItem()
	UpdateMountSlot()
	ArmImportedItem()
end

rbAttachLeft_OnRadioButtonChanged = OnAttachSideChanged
rbAttachRight_OnRadioButtonChanged = OnAttachSideChanged

-- Selected a predefined unit conversion
function cbPredefinedUnitConversion_OnComboBoxSelectionChanged(comboBoxHandle, sel_index, sel_text)
	local scaleInt = ComboBox_GetItemDataByIndex( comboBoxHandle, sel_index )
	if scaleInt == 0 then return end
	Log("predefinedUnitScale="..scaleInt)
	UpdateScale(scaleInt / 1000)
	MenuClearFocusThis()
end

function ApplyDimension(ctlName, handleDontUpdate)

	-- change selection to "Enter Custom Ratio"
	useCustomRatio()

	-- Get Size (x, y, z)
	local newSize = getEditBoxNumber(ctlName)

	-- Update Scale From New Size
	local newScale = 0
	if (ctlName == "edSizeX") then
		if g_fixAxis then
			newScale = newSize / g_originalSizeY
		else
			newScale = newSize / g_originalSizeX
		end
	elseif (ctlName == "edSizeY") then
		if g_fixAxis then
			newScale = newSize / g_originalSizeX
		else
			newScale = newSize / g_originalSizeY
		end
	elseif (ctlName == "edSizeZ") then
		newScale = newSize / g_originalSizeZ
	end

	-- Update Scale (validates & updates dimensions)
	UpdateScale(newScale, handleDontUpdate)
end

function ApplyScaleFactor(handleDontUpdate)

	-- change selection to "Enter Custom Ratio"
	useCustomRatio()

	-- Get Scale As Factor (0.0 -> 1.0)
	local newScale = getEditBoxNumber("edScaleFactor", 100.0)
	newScale = newScale / 100.0 -- convert percentage to factor
	
	-- Update Scale (validates & updates dimensions)
	UpdateScale(newScale, handleDontUpdate)
end

function ValidateScale(scale)
	scale = tonumber(scale) or SCALEFACTOR_DEF
	local neg = (scale < 0)
	local scaleAbs = math.abs(scale)
	if (scaleAbs == 0) then 
		scaleAbs = SCALEFACTOR_DEF
	elseif (scaleAbs < SCALEFACTOR_MIN) then
		scaleAbs = SCALEFACTOR_MIN
	elseif (scaleAbs > SCALEFACTOR_MAX) then 
		scaleAbs = SCALEFACTOR_MAX 
	end
	if neg then
		return -scaleAbs
	else
		return scaleAbs
	end
end

function UpdateScale(scale, handleDontUpdate)

	-- Get Import Session
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession==nil then return end

	-- Validate Scale (only while not entering data)
	if (handleDontUpdate == nil) then
		scale = ValidateScale(scale)
	end

	-- Update New Scale
--	local prevScale = GetSet_GetNumber( impSession, gImportStateGetSetIds.SCALEFACTOR )
	GetSet_SetNumber( impSession, gImportStateGetSetIds.SCALEFACTOR, scale )
	GetSet_SetNumber( impSession, gImportStateGetSetIds.UPDATEMATRIX, 1 )	-- trigger matrix update

	-- Update Dimensions With New Scale
	g_currentSizeX = g_originalSizeX * scale
	g_currentSizeY = g_originalSizeY * scale
	g_currentSizeZ = g_originalSizeZ * scale

	-- Update Scale & Dimensions Edit Boxes
	if (handleDontUpdate ~= "edScaleFactor") then
		setEditBoxNumber("edScaleFactor", scale * 100.0, 100.0)
	end
	if g_fixAxis then
		if (handleDontUpdate ~= "edSizeX") then
			setEditBoxNumber("edSizeX", g_currentSizeY, g_originalSizeY)
		end
		if (handleDontUpdate ~= "edSizeY") then
			setEditBoxNumber("edSizeY", g_currentSizeX, g_originalSizeX)
		end
	else
		if (handleDontUpdate ~= "edSizeX") then
			setEditBoxNumber("edSizeX", g_currentSizeX, g_originalSizeX)
		end
		if (handleDontUpdate ~= "edSizeY") then
			setEditBoxNumber("edSizeY", g_currentSizeY, g_originalSizeY)
		end
	end
	if (handleDontUpdate ~= "edSizeZ") then
		setEditBoxNumber("edSizeZ", g_currentSizeZ, g_originalSizeZ)
	end

	-- Log New Scale
	g_currentSizeStr = "( "
		.. "L=" .. MathRound(g_currentSizeX, DEC_SETTING) .. " "
		.. "H=" .. MathRound(g_currentSizeY, DEC_SETTING) .. " "
		.. "W=" .. MathRound(g_currentSizeZ, DEC_SETTING) .. " "
		.. ")"
    if (scale ~= SCALEFACTOR_DEF) then
        Log( "UpdateScale: scale=" .. scale.." ".. g_currentSizeStr )
    end
end

function editBoxSelectAll( handle )
	local string = EditBox_GetText(handle)
	EditBox_SetText( handle, string, true )
end

function cbAutoCorrectAmbient_OnCheckBoxChanged(checkBoxHandle)

	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end

	local correctAmbient = CheckBox_GetChecked( checkBoxHandle )
	GetSet_SetNumber( impSession, gImportStateGetSetIds.AUTOCORRECTAMBIENT, correctAmbient )
end

-- Handle mount slot changes
function UpdateMountSlot( slotIndex )

	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end

	local text, slotIndex = getComboBoxSelectedItem( "cbMountSlots" )
	local slot = gMountSlots[slotIndex]
	if slot == nil then return end

	local attachRight = CheckBox_GetChecked( RadioButton_GetCheckBox(rbAttachRight) )
	local bUseMirror = slot.Mirror~=nil and gDefaultSide=="R" and attachRight==0 or gDefaultSide=="L" and attachRight==1
	enableControl( "rbAttachRight", slot.Mirror~=nil )
	enableControl( "rbAttachLeft", slot.Mirror~=nil )

	local slotUse = slot
	local ok = GetSet_SetNumberByName( impSession, "MountSlot", slotIndex )
	if (ok == 0) then
		LogError("UpdateMountSlot: FAILED GetSet_SetNumberByName(MountSlot)")
	end

	if bUseMirror then
		slotUse = slot.Mirror
		ok = GetSet_SetNumberByName( impSession, "UseMirror", 1 )
	else
		ok = GetSet_SetNumberByName( impSession, "UseMirror", 0 )
	end
	if (ok == 0) then
		LogError("UpdateMountSlot: FAILED GetSet_SetNumberByName(UseMirror)")
	end

	GetSet_SetString( impSession, gImportStateGetSetIds.ACTORMOUNTBONE, slotUse.Bone )
	GetSet_SetNumber( impSession, gImportStateGetSetIds.EXCLUSIONGROUP, slotUse.Exclusion )

	g_mountSlotIsSet = true

	-- setup camera
	AdjustCamera()

	-- Also update transformations based on bone mirroring
	ApplyXFormParams()

	-- BEGIN AXIS
	local boneIndex
	local scaleX = 1
	local scaleY = 1
	local scaleZ = 1
	gBoneIndex = slotUse.BoneIndex
	if slotUse.Scale~=nil then
		scaleX = slotUse.Scale[1]
		scaleY = slotUse.Scale[2]
		scaleZ = slotUse.Scale[3]
	end

	if g_axisXGeomId~=-1 then
		KEP_SetGeometryRefObj( g_axisXGeomId, ObjectType.BONE, gBoneIndex )
		KEP_MoveGeometry(g_axisXGeomId, 0, 0, 0, false)
		KEP_RotateGeometry(g_axisXGeomId, 0, 0, 0)
		KEP_ScaleGeometry(g_axisXGeomId, scaleX, scaleX, scaleX)
		KEP_SetGeometryRefObj( g_axisYGeomId, ObjectType.BONE, gBoneIndex)
		KEP_MoveGeometry(g_axisYGeomId, 0, 0, 0, false)
		KEP_RotateGeometry(g_axisYGeomId, 0, 0, 0)
		KEP_ScaleGeometry(g_axisYGeomId, scaleY, scaleY, scaleY)
		KEP_SetGeometryRefObj(g_axisZGeomId, ObjectType.BONE, gBoneIndex)
		KEP_MoveGeometry(g_axisZGeomId, 0, 0, 0, false)
		KEP_RotateGeometry(g_axisZGeomId, 0, 0, 0)
		KEP_ScaleGeometry(g_axisZGeomId, scaleZ, scaleZ, scaleZ)
	end

	local moveEvent = KEP_EventCreate( CHANGE_BUILD_MODE_EVENT)
	KEP_EventSetFilter(moveEvent, MODE_MOVE)
	KEP_EventQueue( moveEvent)
end

function FinalCheckScaleFactor()

	-- Get Import Session
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return true end

	-- Update Scale (validates & updates dimensions)
	local scale = GetSet_GetNumber( impSession, gImportStateGetSetIds.SCALEFACTOR )
	UpdateScale(scale)

	-- Valid Dimensions ?
	if (math.abs(g_currentSizeX) < SIZE_MIN) 
		or (math.abs(g_currentSizeY) < SIZE_MIN) 
		or (math.abs(g_currentSizeZ) < SIZE_MIN) then
		LogError( "FinalCheckScaleFactor: TOO SMALL - "..g_currentSizeStr)
		KEP_YesNoBox( "The mesh being imported might become too small to be seen with the scaling factor you've chosen. Continue scaling?", "Warning", WF.CONFIRM_MESH_SCALING, 0 )
		return false
	end

	return true
end

function YesNoBoxHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter ~= WF.CONFIRM_MESH_SCALING then return end

	local answer = KEP_EventDecodeNumber( event )	-- 1: Yes, 0 = No
	if answer==1 then
		g_continueImport = true
		MenuCloseThis()
	end

	return KEP.EPR_CONSUMED
end

-- change selection to "Enter Custom Ratio"
function useCustomRatio()
	local selStr, data = getComboBoxSelectedItem( "cbPredefinedUnitConversion" )
	if data == nil or data == 0 then return end
	selectComboBoxItemByData( "cbPredefinedUnitConversion", 0 )
end

function InitImportBoundBox()

	-- Get Import Session
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end

	-- Get original bounding box
	local minX, minY, minZ = GetSet_GetVector( impSession, gImportStateGetSetIds.BOUNDBOXMIN )
	local maxX, maxY, maxZ = GetSet_GetVector( impSession, gImportStateGetSetIds.BOUNDBOXMAX )
	local currScale = GetSet_GetNumber( impSession, gImportStateGetSetIds.SCALEFACTOR )

	-- create indicator geom if not already
	if g_importBoundBoxGeomId==-1 then

		-- EI BoundBox
		g_importBoundBoxGeomId = KEP_CreateStandardGeometry{type=LWG.GT_BOX, min={x=minX, y=minY, z=minZ}, max={x=maxX, y=maxY, z=maxZ}}
		if g_importBoundBoxGeomId~=-1 then
            KEP_SetGeometryMaterial(g_importBoundBoxGeomId, g_boundBoxMaterial)
    		-- Attach to EI
            KEP_SetGeometryRefObj(g_importBoundBoxGeomId, ObjectType.EQUIPPABLE, gLocalGLID)
            KEP_SetGeometryVisible(g_importBoundBoxGeomId, true)
		end

        -- Axis: Attach to root bone initially	
        -- X
		if g_fixAxis then 
			g_axisXGeomId = KEP_CreateStandardGeometry{type=LWG.GT_BOX, min={x=0, y=0, z=0}, max={x=0.001, y=-1.0, z=0.001}}
		else
            g_axisXGeomId = KEP_CreateStandardGeometry{type=LWG.GT_BOX, min={x=0, y=0, z=0}, max={x=-1.0, y=0.001, z=0.001}}
		end
        KEP_SetGeometryMaterial(g_axisXGeomId, g_axisXMaterial)
        KEP_SetGeometryRefObj(g_axisXGeomId, ObjectType.BONE, 0)
        KEP_SetGeometryVisible(g_axisXGeomId, true)

        -- Y
        if g_fixAxis then 
            g_axisYGeomId = KEP_CreateStandardGeometry{type=LWG.GT_BOX, min={x=0, y=0, z=0}, max={x=1.0, y=0.001, z=0.001}}
		else
            g_axisYGeomId = KEP_CreateStandardGeometry{type=LWG.GT_BOX, min={x=0, y=0, z=0}, max={x=0.001, y=1.0, z=0.001}}
		end
        KEP_SetGeometryMaterial(g_axisYGeomId, g_axisYMaterial)
        KEP_SetGeometryRefObj(g_axisYGeomId, ObjectType.BONE, 0)
        KEP_SetGeometryVisible(g_axisYGeomId, true)

        -- Z
		g_axisZGeomId = KEP_CreateStandardGeometry{type=LWG.GT_BOX, min={x=0, y=0, z=0}, max={x=0.001, y=0.001, z=1.0}}
        KEP_SetGeometryMaterial(g_axisZGeomId, g_axisZMaterial)
        KEP_SetGeometryRefObj(g_axisZGeomId, ObjectType.BONE, 0)
        KEP_SetGeometryVisible(g_axisZGeomId, true)
	end

	if g_personalSpaceBoundBoxId==-1 and gOptions~=nil then
        local min = {}
		min.x = gOptions[UGCOPT_PERSONALSPACEMINX]
		min.y = gOptions[UGCOPT_PERSONALSPACEMINY]
		min.z = gOptions[UGCOPT_PERSONALSPACEMINZ]
        local max = {}
		max.x = gOptions[UGCOPT_PERSONALSPACEMAXX]
		max.y = gOptions[UGCOPT_PERSONALSPACEMAXY]
		max.z = gOptions[UGCOPT_PERSONALSPACEMAXZ]

		if minX~=nil and minY~=nil and minZ~=nil and maxX~=nil and maxY~=nil and maxZ~=nil then

			-- BEGIN Personal Space BoundBox
			g_personalSpaceBoundBoxId = KEP_CreateStandardGeometry{type=LWG.GT_BOX, min=min, max=max}
            KEP_SetGeometryMaterial(g_personalSpaceBoundBoxId, g_personalSpaceBoxMaterial)
            KEP_SetGeometryDrawMode(g_personalSpaceBoundBoxId, LWG.F_DRAW_SOLID + LWG.F_ALPHA_BLENDING + LWG.F_CULL_BACK)
		end
	end
end

function DisposeImportBoundBox()

	if g_importBoundBoxGeomId~=-1 then
		KEP_DestroyGeometry(g_importBoundBoxGeomId)
		g_importBoundBoxGeomId = -1
	end

	if g_axisXGeomId~=-1 then
		KEP_DestroyGeometry(g_axisXGeomId)
		g_axisXGeomId = -1
	end

	if g_axisYGeomId~=-1 then
		KEP_DestroyGeometry(g_axisYGeomId)
		g_axisYGeomId = -1
	end

	if g_axisZGeomId~=-1 then
		KEP_DestroyGeometry(g_axisZGeomId)
		g_axisZGeomId = -1
	end

	if g_personalSpaceBoundBoxId~=-1 then
		KEP_DestroyGeometry(g_personalSpaceBoundBoxId)
		g_personalSpaceBoundBoxId = -1
	end
end

function ApplyXFormParams()

	if gLocalGLID == 0 then return end

	local tx, ty, tz
	if g_fixAxis then
		tx = getEditBoxNumber( "edTY" )
		ty = -getEditBoxNumber( "edTX" )
	else
		tx = getEditBoxNumber( "edTX" )
		ty = getEditBoxNumber( "edTY" )
	end
	tz = getEditBoxNumber( "edTZ" )

	local rx, ry, rz
	if g_fixAxis then
		rx = getEditBoxNumber( "edRY" )
		ry = -getEditBoxNumber( "edRX" )
		rz = getEditBoxNumber( "edRZ" ) - 90
	else
		rx = getEditBoxNumber( "edRX" )
		ry = getEditBoxNumber( "edRY" )
		rz = getEditBoxNumber( "edRZ" )
	end

	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end
	
	local text, slotIndex = getComboBoxSelectedItem( "cbMountSlots" )
	local slot = gMountSlots[slotIndex]
	local slotUse = slot

	local useMirror, ok = GetSet_GetNumberByName( impSession, "UseMirror", 0 )
	if (ok == 0) then
		LogError("ApplyXFormParams: FAILED GetSet_GetNumberByName(UseMirror)")
	end

	if useMirror==1 and slot.Mirror~=nil then
		slotUse = slot.Mirror
	end

	if slotUse.Offset~=nil then
		tx = tx + slotUse.Offset[1]
		ty = ty + slotUse.Offset[2]
		tz = tz + slotUse.Offset[3]
	end

	if slotUse.Scale~=nil then
		tx = tx * slotUse.Scale[1]
		ty = ty * slotUse.Scale[2]
		tz = tz * slotUse.Scale[3]
	end

	GetSet_SetVector( impSession, gImportStateGetSetIds.POSITION, tx, ty, tz )
	GetSet_SetVector( impSession, gImportStateGetSetIds.ROTATION, rx, ry, rz )
	GetSet_SetNumber( impSession, gImportStateGetSetIds.UPDATEMATRIX, 1 )	-- trigger matrix update

	-- Update Widget Enable
	updateWidgetEnable()
end

function cbActorType_OnComboBoxSelectionChanged(comboBoxHandle, sel_index, sel_text)

	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end

	local text, data = getComboBoxSelectedItem( "cbActorType" )
	Log("ActorChanged: ".. text)

	DisarmImportedItem()

	if text == "Male" then
		LoadNPC( "M" )
	elseif text == "Female" then
		LoadNPC( "F" )
	elseif text == "Your Avatar" then
		UnloadNPC()
		SetImportAnim()
		SetActorGroup(gPlayerGender)
	end

	ArmImportedItem()
	UpdateMountSlot()
	local mountBone = GetSet_GetString( impSession, gImportStateGetSetIds.ACTORMOUNTBONE )-- refresh bone index
	GetSet_SetString( impSession, gImportStateGetSetIds.ACTORMOUNTBONE, mountBone )

	MenuClearFocusThis()
end

function LoadNPC( gender )
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end

	Log("LoadNPC: gender="..gender)

	-- No Gender Change ?
	if gCurrNPCGender == gender then 
		LogWarn("LoadNPC: SAME GENDER - Ignoring")
		return 
	end

	for idx, actorType in ipairs(gActorTypes) do
		if actorType.gender == gender then
			
			-- Unload Existing NPC
			UnloadNPC()

			KEP_Mount( gActorTypes[idx].stuntId, MT_STUNT, 1 )
			
			SetActorGroup(gender)
			gCurrNPCGender = gender
			
			gAIMounted = 1
			GetSet_SetNumber( impSession, gImportStateGetSetIds.AIMOUNTED, gAIMounted )

			-- update skeleton-specific parameters like scaling and bone indexes
			UpdateMountSlotsParameters()	

			return
		end
	end
end

function UnloadNPC()
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end

	-- Already Unloaded ?
	if gAIMounted ~= 1 then 
		return 
	end

	Log("UnloadNPC")

	KEP_Unmount( )

	gCurrNPCGender = nil

	gAIMounted = 0
	GetSet_SetNumber( impSession, gImportStateGetSetIds.AIMOUNTED, gAIMounted )

	-- update skeleton-specific parameters like scaling and bone indexes
	UpdateMountSlotsParameters()
end

function ArmImportedItem()

	local player = KEP_GetPlayer( )
	if player == nil then return end

	if gLocalGLID~=0 then
		Log( "ArmImportedItem: glid=" .. gLocalGLID )
		KEP_ArmEquippableItem( player, gLocalGLID, 0 )
		KEP_LockSelection( 0)
		KEP_ClearSelection( )
		KEP_SelectObject( ObjectType.EQUIPPABLE, gLocalGLID )
		KEP_LockSelection( 1) -- lock down selection so only EI is selected.
		bsEvent = KEP_EventCreate( BUILD_SETTINGS_EVENT )
		KEP_EventSetFilter(bsEvent, BUILD_SETTINGS_SELECTION_LOCKED)
		KEP_EventEncodeNumber(bsEvent, 1)
		KEP_EventQueue( bsEvent )

		local moveEvent = KEP_EventCreate( CHANGE_BUILD_MODE_EVENT)
		KEP_EventSetFilter(moveEvent, MODE_MOVE)
		KEP_EventQueue( moveEvent)
	else
		LogError( "ArmImportedItem: no GLID" )
	end

	-- change animation to "Equippable Item Import Pose"
	SetImportAnim()

	-- Update Widget Enable
	updateWidgetEnable()
end

function DisarmImportedItem()

	local player = KEP_GetPlayer()
	if player == nil then return end

	if gLocalGLID ~= 0 then
		Log( "DisarmImportedItem: glid=" .. gLocalGLID )
		KEP_DisarmEquippableItem( player, gLocalGLID )
	else
		LogError( "DisarmImportedItem: no GLID" )
	end

	-- If disarming head/hair slot, reload NPC to bring back original head/hair
	if gCurrNPCGender~=nil then
		local impSession = UGC_GetImportSession( gImportInfo.sessionId )
		local exclusion = GetSet_GetNumber( impSession, gImportStateGetSetIds.EXCLUSIONGROUP )
		if exclusion==gMountSlotIDs.HEAD_ONLY.ID or exclusion==gMountSlotIDs.HAIR.ID or exclusion==gMountSlotIDs.HEAD_W_HAIR.ID then
			local savedNPCGender = gCurrNPCGender
			UnloadNPC()
			LoadNPC(savedNPCGender)
		end
	end

	-- change animation back to "stand"
	KEP_SetCurrentAnimation( 1, 0 )
end

function UpdateMountSlotsParameters()

	local gsPlayer = KEP_GetPlayer(  )
	if gsPlayer == nil then return end

	if gAIMounted==0 then
		g_actorScale = KEP_GetRuntimePlayerScale(gsPlayer)
	else
		g_actorScale = { x=1.0, y=1.0, z=1.0 }
	end
	Log( "UpdateMountSlotsParameters: actorScale="..VecToString(g_actorScale))

	for idx, slot in ipairs(gMountSlots) do
		for mirror = 0, 1 do
			if mirror==1 then
				slot = slot.Mirror
			end
			if slot~=nil then
				slot.BoneIndex = KEP_GetBoneIndexByName( gsPlayer, slot.Bone )
				if slot.Camera~=nil and slot.Camera.LookAt~=nil then
					-- backup LookAt point if not already
					if slot.Camera.LookAtBackup==nil then
						slot.Camera.LookAtBackup = {}
						slot.Camera.LookAtBackup[1] = slot.Camera.LookAt[1]
						slot.Camera.LookAtBackup[2] = slot.Camera.LookAt[2]
						slot.Camera.LookAtBackup[3] = slot.Camera.LookAt[3]
					end
					slot.Camera.LookAt[1] = slot.Camera.LookAtBackup[1] * g_actorScale.x
					slot.Camera.LookAt[2] = slot.Camera.LookAtBackup[2] * g_actorScale.y
					slot.Camera.LookAt[3] = slot.Camera.LookAtBackup[3] * g_actorScale.z
				end
			end
		end
	end
end

function SaveCamera()
	local camera = KEP_GetPlayersActiveCamera( )
	gOrigCamFocusX, gOrigCamFocusY, gOrigCamFocusZ = GetSet_GetVector( camera, CameraIds.ORBITFOCUS )
	gOrigCamDist = GetSet_GetNumber( camera, CameraIds.ORBITDIST )
	gOrigCamMinDist = GetSet_GetNumber( camera, CameraIds.ORBITMINDIST )
	gOrigCamDistStep = GetSet_GetNumber( camera, CameraIds.ORBITDISTSTEP )
	gOrigCamNearPlaneDist = GetSet_GetNumber( camera, CameraIds.NEARPLANEDIST )
end

function RestoreCamera()
	local camera = KEP_GetPlayersActiveCamera(  )
	if gOrigCamFocusX~=nil then
		GetSet_SetVector( camera, CameraIds.ORBITFOCUS, gOrigCamFocusX, gOrigCamFocusY, gOrigCamFocusZ )
	end
	if gOrigCamDist~=nil then
		GetSet_SetNumber( camera, CameraIds.ORBITDIST, gOrigCamDist )
	end
	if gOrigCamMinDist~=nil then
		GetSet_SetNumber( camera, CameraIds.ORBITMINDIST, gOrigCamMinDist )
	end
	if gOrigCamDistStep~=nil then
		GetSet_SetNumber( camera, CameraIds.ORBITDISTSTEP, gOrigCamDistStep )
	end
	if gOrigCamNearPlaneDist~=nil then
		GetSet_SetNumber( camera, CameraIds.NEARPLANEDIST, gOrigCamNearPlaneDist )
	end
end

function AdjustCamera()
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end

	if (not gAdjustCamera) then return end

	local camera = KEP_GetPlayersActiveCamera(  )

	local text, slotIndex = getComboBoxSelectedItem( "cbMountSlots" )
	local slot = gMountSlots[slotIndex]
	local slotUse = slot

	local useMirror, ok = GetSet_GetNumberByName( impSession, "UseMirror", 0 )
	if useMirror==1 and slot.Mirror~=nil then
		slotUse = slot.Mirror
	end

	if slotUse.Camera==nil or slotUse.Camera.LookAt==nil then
		if slotUse.Camera == nil then slotUse.Camera = {} end
		if slotUse.Camera.LookAt==nil then 
			slotUse.Camera.LookAt[1] = gOrigCamFocusX
			slotUse.Camera.LookAt[2] = gOrigCamFocusY
			slotUse.Camera.LookAt[3] = gOrigCamFocusZ
		end
		LogInfo("AdjustCamera - focus=("..slotUse.Camera.LookAt[1]..","..slotUse.Camera.LookAt[2]..","..slotUse.Camera.LookAt[3]..")")
		GetSet_SetVector( camera, CameraIds.ORBITFOCUS, slotUse.Camera.LookAt[1], slotUse.Camera.LookAt[2], slotUse.Camera.LookAt[3] )
	else
		if slotUse.Camera.Distance==nil then
			slotUse.Camera.Distance = CAM_DIST
		end

		if slotUse.Camera.MinDistance==nil then
			slotUse.Camera.MinDistance = CAM_MIN_DIST
		end

		if slotUse.Camera.DistStep==nil then
			slotUse.Camera.DistStep = CAM_DIST_STEP
		end

		if slotUse.Camera.NearPlaneDist==nil then
			slotUse.Camera.NearPlaneDist = CAM_NEAR_PLANE_DIST
		end

		LogInfo("AdjustCamera - focus=("..slotUse.Camera.LookAt[1]..","..slotUse.Camera.LookAt[2]..","..slotUse.Camera.LookAt[3]..")"
			.." dist="..slotUse.Camera.Distance
			.." distMin="..slotUse.Camera.MinDistance
			.." distStep="..slotUse.Camera.DistStep
			.." distNear="..slotUse.Camera.NearPlaneDist
		)

		-- use mount slot specific look at point
		GetSet_SetVector( camera, CameraIds.ORBITFOCUS, slotUse.Camera.LookAt[1], slotUse.Camera.LookAt[2], slotUse.Camera.LookAt[3] )
		GetSet_SetNumber( camera, CameraIds.ORBITDIST, slotUse.Camera.Distance )
		GetSet_SetNumber( camera, CameraIds.ORBITMINDIST, slotUse.Camera.MinDistance )
		GetSet_SetNumber( camera, CameraIds.ORBITDISTSTEP, slotUse.Camera.DistStep )
		GetSet_SetNumber( camera, CameraIds.NEARPLANEDIST, slotUse.Camera.NearPlaneDist )
	end
end

function Dialog_OnRender(dialogHandle, elapsedSec)

	-- Update Widget Position
	updateWidgetPosition()

	-- Update Widget Enable
	if g_updateWidgetEnable > 0 then
		g_updateWidgetEnable = g_updateWidgetEnable - elapsedSec
		if (g_updateWidgetEnable <= 0) then
			g_updateWidgetEnable = 0
			updateWidgetEnable()
		end
	end

	-- if the personal space box is checked
	if gShowPersonalSpaceBoundBox == 1 then 
		SetImportAnim()
	end

	CheckPersonalSpaceConstraint()
	CheckMaxSize()
end

function SetImportAnim()

	local correctGender
	if gCurrNPCGender == nil then
		correctGender = gPlayerGender
	else
		correctGender = gCurrNPCGender
	end

	if gShowPersonalSpaceBoundBox == 0 then
		if correctGender == "M" then
			KEP_SetCurrentAnimationByGLID( CoreItems.EIPoseMale )
		else
			KEP_SetCurrentAnimationByGLID( CoreItems.EIPoseFemale )
		end
	else
		if correctGender == "M" then
			KEP_SetCurrentAnimationByGLID( CoreItems.RestingPoseMale )
		else
			KEP_SetCurrentAnimationByGLID( CoreItems.RestingPoseFemale )
		end
	end
end

function EquippableItemWidgetTranslateHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local x = KEP_EventDecodeNumber( event )
	local y = KEP_EventDecodeNumber( event )
	local z = KEP_EventDecodeNumber( event )
	local relX, relY, relZ = KEP_GetTransformRelativeToBone(gBoneIndex, x, y, z)
	updateRelativePosition(relX, relY, relZ)
	return KEP.EPR_CONSUMED
end

function updateRelativePosition(x, y, z)

	-- Get Import Session
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end

	-- Update Relative Position
	GetSet_SetVector( impSession, gImportStateGetSetIds.POSITION, x, y, z )
	GetSet_SetNumber( impSession, gImportStateGetSetIds.UPDATEMATRIX, 1 )
	
	-- Update Position Edit Boxes
	updateRelativePositionText(x, y, z)
end

function updateRelativePositionText(x, y, z)
	
	-- Get Import Session
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end

	local text, slotIndex = getComboBoxSelectedItem( "cbMountSlots" )
	local slot = gMountSlots[slotIndex]
	local slotUse = slot
	local useMirror, ok = GetSet_GetNumberByName( impSession, "UseMirror", 0 )
	if (ok == 0) then
		LogError("updateRelativePositionText: FAILED GetSet_GetNumberByName(UseMirror)")
	end

	if useMirror==1 and slot.Mirror~=nil then
		slotUse = slot.Mirror
	end

	if slotUse.Offset~=nil then
		x = x - slotUse.Offset[1]
		y = y - slotUse.Offset[2]
		z = z - slotUse.Offset[3]
	end

	-- Update Position Edit Boxes
	if g_fixAxis then
		setEditBoxNumber("edTX", -y)
		setEditBoxNumber("edTY", x)
	else
		setEditBoxNumber("edTX", x)
		setEditBoxNumber("edTY", y)
	end
	setEditBoxNumber("edTZ", z)
end

function cbShowPersonalSpace_OnCheckBoxChanged( checkBoxHandle, check )
	if check==1 then
		if g_personalSpaceBoundBoxId~=-1 then
            KEP_SetGeometryRefObj(g_personalSpaceBoundBoxId, ObjectType.PLAYER, 0)
            KEP_SetGeometryVisible(g_personalSpaceBoundBoxId, true)
			gShowPersonalSpaceBoundBox = 1
			SetImportAnim()
		end
	else
        KEP_SetGeometryVisible(g_personalSpaceBoundBoxId, false)
		gShowPersonalSpaceBoundBox = 0
		SetImportAnim()
	end
end

function UGCGlobalOptionsResultHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter ~= UGC_FILTER.EI_IMPORT_OPTS then return end

	gOptions = UGCI.ParseGlobalOptionsResultEvent( event )

	--Init asset statistics labels
	InitAssetStatLabels()

	-- init geometries with received global options
	InitImportBoundBox()

	-- update position for geometries
	UpdateMountSlot()
end

function CheckPersonalSpaceConstraint()
	if gImportInfo == nil or gOptions == nil then return true end

	-- Get Import Session
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return true end

	-- constraint
	local constraint = {}
	constraint.minX = gOptions[UGCOPT_PERSONALSPACEMINX]
	constraint.maxX = gOptions[UGCOPT_PERSONALSPACEMAXX]
	constraint.minY = gOptions[UGCOPT_PERSONALSPACEMINY]
	constraint.maxY = gOptions[UGCOPT_PERSONALSPACEMAXY]
	constraint.minZ = gOptions[UGCOPT_PERSONALSPACEMINZ]
	constraint.maxZ = gOptions[UGCOPT_PERSONALSPACEMAXZ]

	if (
		constraint.minX==nil 
		or constraint.maxX==nil 
		or constraint.minY==nil 
		or constraint.maxY==nil 
		or constraint.minZ==nil 
		or constraint.maxZ==nil 
	) then return true end

	local boundBoxInActorSpace = {}
	boundBoxInActorSpace.minX = GetSet_GetNumber( impSession, gImportStateGetSetIds.BOUNDBOXINACTORSPACE_MINX )
	boundBoxInActorSpace.minY = GetSet_GetNumber( impSession, gImportStateGetSetIds.BOUNDBOXINACTORSPACE_MINY )
	boundBoxInActorSpace.minZ = GetSet_GetNumber( impSession, gImportStateGetSetIds.BOUNDBOXINACTORSPACE_MINZ )
	boundBoxInActorSpace.maxX = GetSet_GetNumber( impSession, gImportStateGetSetIds.BOUNDBOXINACTORSPACE_MAXX )
	boundBoxInActorSpace.maxY = GetSet_GetNumber( impSession, gImportStateGetSetIds.BOUNDBOXINACTORSPACE_MAXY )
	boundBoxInActorSpace.maxZ = GetSet_GetNumber( impSession, gImportStateGetSetIds.BOUNDBOXINACTORSPACE_MAXZ )

	-- validate against constraint
	if ( true
		and (constraint.minX <= boundBoxInActorSpace.minX)
		and (constraint.maxX >= boundBoxInActorSpace.minX)
		and (constraint.minX <= boundBoxInActorSpace.maxX)
		and (constraint.maxX >= boundBoxInActorSpace.maxX)
		and (constraint.minY <= boundBoxInActorSpace.minY)
		and (constraint.maxY >= boundBoxInActorSpace.minY)
		and (constraint.minY <= boundBoxInActorSpace.maxY)
		and (constraint.maxY >= boundBoxInActorSpace.maxY)
		and (constraint.minZ <= boundBoxInActorSpace.minZ)
		and (constraint.maxZ >= boundBoxInActorSpace.minZ)
		and (constraint.minZ <= boundBoxInActorSpace.maxZ) 
		and (constraint.maxZ >= boundBoxInActorSpace.maxZ)
	) then 
		-- constraint satisfied
		showControl( "lblPersonalSpaceConstraintOK" )
		hideControl( "lblPersonalSpaceConstraintViolated" )
		return true
	else
		-- invalid
		hideControl( "lblPersonalSpaceConstraintOK" )
		showControl( "lblPersonalSpaceConstraintViolated" )
		return false
	end
end

function GetMountSlotIndexByExclusionGroup( exclusionGroup )
	local idx, slot
	for idx, slot in ipairs(gMountSlots) do
		if slot.Exclusion==exclusionGroup then
			return idx, 0
		elseif slot.Mirror~=nil and slot.Mirror.Exclusion==exclusionGroup then
			return idx, 1
		end
	end
	-- if not found, return first slot
	return 0, 0
end

function InitAssetStatLabels()

	-- Hide them all by default
	hideControl( "lblTriCountDefault" )
	hideControl( "lblTriCount" )
	hideControl( "lblTriCountValue" )
	hideControl( "lblTriCountValueBad" )

	if gImportStateGetSetIds.TRIANGLECOUNT == nil or gImportInfo == nil then
		-- not implemented yet
		LogError( "InitAssetStatLabels: in-menu triangle count validation not implemented" )
		return
	end

	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return end

	local triCountValue = GetSet_GetNumber( impSession, gImportStateGetSetIds.TRIANGLECOUNT )
	if triCountValue==-1 then
		LogError( "InitAssetStatLabels: triangle count not available" )
		return
	end

	-- update labels
	if gOptions[UGCOPT_EI_MAXTRICOUNT]~=nil then
		local triCountLabel = getStaticText( "lblTriCount" )
		triCountLabel = string.gsub( triCountLabel, "{MaxTris}", gOptions[UGCOPT_EI_MAXTRICOUNT] )
		setStaticText( "lblTriCount", triCountLabel )
		hideControl( "lblTriCountDefault" )
		showControl( "lblTriCount" )

		if triCountValue > tonumber( gOptions[UGCOPT_EI_MAXTRICOUNT] ) then
			hideControl( "lblTriCountValue" )
			showControl( "lblTriCountValueBad" )
		else
			hideControl( "lblTriCountValueBad" )
			showControl( "lblTriCountValue" )
		end
	end

	setStaticText( "lblTriCountValue", "" .. triCountValue )
	setStaticText( "lblTriCountValueBad", "" .. triCountValue )

	if gOptions[UGCOPT_EI_MAXTRICOUNT]~=nil then
		Log( "InitAssetStatLabels: " .. triCountValue .. " triangles, max = " .. gOptions[UGCOPT_EI_MAXTRICOUNT] )
	else
		LogError( "InitAssetStatLabels: " .. triCountValue .. " triangles, max unknown" )
	end
end

function cbShowTranslationWidget_OnCheckBoxChanged(checkBoxHandle, check)
	g_widgetEnable = (check == 1)
	updateWidgetEnable()
end

function selectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	g_updateWidgetEnable = 0.1 -- 0.1 sec from now seems to work
end

function CheckMaxSize()
	if (gImportInfo == nil) or (gOptions == nil) or (gOptions.EI_MaxSizeXYZSum == nil) then return true end

	-- Get Import Session
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession == nil then return true end

	-- Validate Max Size (abs(x) + abs(y) + abs(z))
	local Max_Size_Label = getStaticText( "lblMaxSize" )
	Max_Size_Label = string.gsub( Max_Size_Label, "{MaxSize}", gOptions.EI_MaxSizeXYZSum )
	setStaticText( "lblMaxSize", Max_Size_Label )
	local Current_Size = math.abs(g_currentSizeX) + math.abs(g_currentSizeY) + math.abs(g_currentSizeZ)
	Rounded_Current_Size = MathRound(Current_Size, DEC_SETTING)
	if Current_Size <= gOptions.EI_MaxSizeXYZSum then
		setStaticText( "lblMaxSizeValue", Rounded_Current_Size )
		showControl( "lblMaxSizeValue" )
		hideControl( "lblMaxSizeValueBad" )
		return true
	else
		setStaticText( "lblMaxSizeValueBad", Rounded_Current_Size )
		hideControl( "lblMaxSizeValue" )
		showControl( "lblMaxSizeValueBad" )
		return false
	end
end

function SetActorGroup(gender)
	Log("SetActorGroup: gender="..gender)

	-- Get Import Session
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession==nil then return end
	
	if gCurrNPCGender == gender then 
		LogWarn("SetActorGroup: SAME GENDER - Ignoring")
		return 
	end

	for idx, actorType in ipairs(gActorTypes) do
		if actorType.gender == gender then
			gActorGroup = gActorTypes[idx].actorGroup
		end
	end
	
	GetSet_SetNumber( impSession, gImportStateGetSetIds.TARGETACTORTYPE, gActorGroup )
end

function getEditBoxNumber(ctlName, default)
	local txt = getEditBoxText(ctlName)
	local num = tonumber(txt) or tonumber(default) or 0.0
	return MathRound(num, DEC_SETTING)
end

function setEditBoxNumber(ctlName, num, default)
	local num = tonumber(num) or tonumber(default) or 0.0
	setEditBoxText(ctlName, MathRound(num, DEC_SETTING))
end

function fixEditBoxNumber(ctlName, default)
	local num = getEditBoxNumber(ctlName, default)
	setEditBoxNumber(ctlName, num, default)
end

-- All OnLButtonDblClk
function edScaleFactor_OnLButtonDblClk(handle) editBoxSelectAll(handle) end
function edSizeX_OnLButtonDblClk(handle) editBoxSelectAll(handle) end
function edSizeY_OnLButtonDblClk(handle) editBoxSelectAll(handle) end
function edSizeZ_OnLButtonDblClk(handle) editBoxSelectAll(handle) end
function edTX_OnLButtonDblClk(handle) editBoxSelectAll(handle) end
function edTY_OnLButtonDblClk(handle) editBoxSelectAll(handle) end
function edTZ_OnLButtonDblClk(handle) editBoxSelectAll(handle) end
function edRX_OnLButtonDblClk(handle) editBoxSelectAll(handle) end
function edRY_OnLButtonDblClk(handle) editBoxSelectAll(handle) end
function edRZ_OnLButtonDblClk(handle) editBoxSelectAll(handle) end

-- All OnEditBoxChange (updates while typing into editbox)
function edScaleFactor_OnEditBoxChange(handle) ApplyScaleFactor("edScaleFactor") end
function edSizeX_OnEditBoxChange(handle) ApplyDimension("edSizeX", "edSizeX") end
function edSizeY_OnEditBoxChange(handle) ApplyDimension("edSizeY", "edSizeY") end
function edSizeZ_OnEditBoxChange(handle) ApplyDimension("edSizeZ", "edSizeZ") end
function edTX_OnEditBoxChange(handle) ApplyXFormParams() end
function edTY_OnEditBoxChange(handle) ApplyXFormParams() end
function edTZ_OnEditBoxChange(handle) ApplyXFormParams() end
function edRX_OnEditBoxChange(handle) ApplyXFormParams() end
function edRY_OnEditBoxChange(handle) ApplyXFormParams() end
function edRZ_OnEditBoxChange(handle) ApplyXFormParams() end

-- DRF - ED-1939 - Accessory Edit Boxes Do Not Lose Focus 
-- All OnEditBoxString (updates on typing return into editbox)
function edScaleFactor_OnEditBoxString(handle, val) MenuClearFocusThis() end
function edSizeX_OnEditBoxString(handle, val) MenuClearFocusThis() end
function edSizeY_OnEditBoxString(handle, val) MenuClearFocusThis() end
function edSizeZ_OnEditBoxString(handle, val) MenuClearFocusThis() end
function edTX_OnEditBoxString(handle, val) MenuClearFocusThis() end
function edTY_OnEditBoxString(handle, val) MenuClearFocusThis() end
function edTZ_OnEditBoxString(handle, val) MenuClearFocusThis() end
function edRX_OnEditBoxString(handle, val) MenuClearFocusThis() end
function edRY_OnEditBoxString(handle, val) MenuClearFocusThis() end
function edRZ_OnEditBoxString(handle, val) MenuClearFocusThis() end

--All OnFocusOut
function edScaleFactor_OnFocusOut(handle) fixEditBoxNumber("edScaleFactor", 100.0) ApplyScaleFactor() end 
function edSizeX_OnFocusOut(handle) ApplyDimension("edSizeX") end
function edSizeY_OnFocusOut(handle) ApplyDimension("edSizeY") end
function edSizeZ_OnFocusOut(handle) ApplyDimension("edSizeZ") end
function edTX_OnFocusOut(handle) fixEditBoxNumber("edTX") ApplyXFormParams() end 
function edTY_OnFocusOut(handle) fixEditBoxNumber("edTY") ApplyXFormParams() end 
function edTZ_OnFocusOut(handle) fixEditBoxNumber("edTZ") ApplyXFormParams() end 
function edRX_OnFocusOut(handle) fixEditBoxNumber("edRX") ApplyXFormParams() end 
function edRY_OnFocusOut(handle) fixEditBoxNumber("edRY") ApplyXFormParams() end 
function edRZ_OnFocusOut(handle) fixEditBoxNumber("edRZ") ApplyXFormParams() end
