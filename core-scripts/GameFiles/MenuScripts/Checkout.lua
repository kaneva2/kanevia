--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua" )
dofile("CreditBalances.lua")

PLAYER_INFO_EVENT = "PlayerInfoEvent"
PLAYER_INFO_USERID_REQUEST = 1
PLAYER_INFO_USERID_RESPONSE = 2
PLAYER_INFO_PLAYERID_REQUEST = 3
PLAYER_INFO_PLAYERID_RESPONSE = 4
PLAYER_INFO_CREDITS_REWARDS_REQUEST = 5
PLAYER_INFO_CREDITS_REWARDS_RESPONSE = 6

PURCHASE_RAVE_EVENT = "PurchaseRaveEvent"
ITEM_INFO_EVENT = "ItemInfoEvent"
ITEM_PURCHASED_EVENT = "ItemPurchasedEvent"
RAVE_PURCHASED_EVENT = "RavePurchasedEvent"
UPDATE_HUD_EVENT = "UpdateHUDEvent"

INVENTORY_TYPE = {}
INVENTORY_TYPE.CREDITS = 256
INVENTORY_TYPE.REWARDS = 512
INVENTORY_TYPE.CREDITS_OR_REWARDS = 768

g_CurrencyType = {}
g_CurrencyType.Rewards = "GPOINT"
g_CurrencyType.Credits = "KPOINT"

g_playerCredits = 0
g_playerRewards = 0

g_RavePlusPrice = 0

g_userID = 0 
g_username = ""

g_itemGlid = nil
g_itemQty = nil
g_itemPrice = nil
g_itemName = nil
g_totalPrice = "..."

g_ravedID = nil
g_raveType = nil
g_raveTarget = nil

g_inventoryType = nil

RAVE_PURCHASE = 1
ITEM_OBJECT_PURCHASE = 2
g_purchaseType = ITEM_OBJECT_PURCHASE

g_tblObjectData = {}

g_raveTargetName = nil
g_isRaveBack = false
g_raveSource = nil

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "itemInfoHandler", ITEM_INFO_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "PurchaseRaveEventHandler", PURCHASE_RAVE_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( ITEM_PURCHASED_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( RAVE_PURCHASED_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( ITEM_INFO_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	g_username = KEP_GetLoginName()
	makeWebCall(GameGlobals.WEB_SITE_PREFIX .. USERID_SUFFIX .. g_username, WF.HUDPROFILE_INFO)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == WF.HUDPROFILE_INFO then
		local estr = KEP_EventDecodeString( event )
		parseUserInfo(estr)
		return KEP.EPR_OK
		
	elseif filter == WF.BUY_DYNAMIC_OBJECT then
		local strXmlData = KEP_EventDecodeString( event )

		local result = parseEventHeader(strXmlData, g_tblObjectData)
		MenuBringToFrontThis()

		if result == 0 then
			-- Item was purchased successfully
			sendItemsPurchasedEvent(g_itemGlid)
			fireUpdateHudEvent()
			MenuCloseThis()
		elseif result == 1 then
			-- Not Enough Rewards
			KEP_MessageBox("Not Enough Rewards")

		elseif result == 2 then
			-- Not Enough Credits
			KEP_MessageBox("Not Enough Credits")
		else
			KEP_MessageBox(tostring(g_tblObjectData.ResultDescription))
		end
	elseif filter == WF.HUD_BALANCES then
		local estr = KEP_EventDecodeString( event )
		local regCredits, giftCredits = ParseBalances(estr)
		if regCredits and giftCredits then
			g_playerCredits = regCredits
			g_playerRewards = giftCredits
			setBalances()
		end 
	elseif filter == WF.BUY_RAVE then
		local strXmlData = KEP_EventDecodeString( event )
		local result = parseEventHeader(strXmlData, g_tblObjectData)
		if result == 0 then
			-- Item was purchased successfully
			KEP_MessageBox("Purchase Successful!")
			local ev = KEP_EventCreate( RAVE_PURCHASED_EVENT)
			KEP_EventQueue( ev)
			fireUpdateHudEvent()
			MenuCloseThis()
		elseif result == 1 or result == 2 then
			-- Not Enough Rewards
			KEP_MessageBox("Not Enough Rewards")
		else
			-- Not Enough Credits
			KEP_MessageBox("Not Enough Credits")
		end

	elseif filter == WF.RAVE_INFO then
		local strXmlData = KEP_EventDecodeString( event )
		local result = parseEventHeader(strXmlData, g_tblObjectData)

		if result == 0 then
			local raveTypeXML = ""
			local nodeStart = 0
			local nodeEnd = 0

			while nodeEnd ~= nil do
				raveTypeXML, nodeStart, nodeEnd = parseDataByTag(strXmlData, "RaveType", "(.-)", nodeEnd)

				if nodeEnd ~= nil then
					raveName = parseDataByTag(raveTypeXML, "Name", "(.-)", 0)
					if ( raveName == "Rave" ) then
						g_RavePlusPrice = parseDataByTag(raveTypeXML, "PriceSingleRave", "(%d+)", 0)
						Static_SetText( gHandles["lblTotalNum"], g_RavePlusPrice )
						g_raveType = 0
					end
				end
			end
		end
	end
end

function fireUpdateHudEvent()
	KEP_EventCreateAndQueue( UPDATE_HUD_EVENT )
end

function WebCallCached(url, filter, maxItems)
	makeWebCallCached(url, filter, 0, maxItems)
end

function PurchaseRaveEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_purchaseType = RAVE_PURCHASE
	setConfiguration()
	g_ravedID = KEP_EventDecodeNumber(event)
	g_raveType = KEP_EventDecodeNumber(event)
	g_raveTarget = KEP_EventDecodeNumber(event)
	if KEP_EventMoreToDecode(event) == 1 then
		g_raveTargetName = KEP_EventDecodeString(event)
		g_isRaveBack = KEP_EventDecodeNumber(event) == 1
		g_raveSource = KEP_EventDecodeString(event)
	end
	g_inventoryType = INVENTORY_TYPE.CREDITS_OR_REWARDS
	makeWebCall( GameGlobals.WEB_SITE_PREFIX .. RAVE_INFORMATION_SUFFIX, WF.RAVE_INFO)
	return KEP.EPR_CONSUMED
end

function itemInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_purchaseType = ITEM_OBJECT_PURCHASE
	setConfiguration()

	g_itemGlid = 0
	g_itemQty = 0
	g_itemPrice = 0
	g_itemName = 0
	g_totalPrice = "..."

	g_inventoryType = KEP_EventDecodeNumber( event )
	local num_items = KEP_EventDecodeNumber( event )

	-- NOTE: Just a heads-up, but this whole menu totally won't work if more than 1 item is requested since it uses a single set of globals.
	if num_items > 1 then
		LogError( "itemInfoHandler: Can't handle > 1 item at a time. Changing num_items to 1.")
		num_items = 1
	end
	for i = 1, num_items do
		g_itemGlid = KEP_EventDecodeNumber( event )
		g_itemQty = KEP_EventDecodeNumber( event )
		g_itemName = KEP_EventDecodeString( event )
		g_itemPrice = KEP_EventDecodeNumber( event )
		if g_itemQty < 0 then
			g_itemQty = 0
		end
		if g_itemPrice >= 0 then
			g_totalPrice = g_itemQty * g_itemPrice
		else
			g_itemPrice = "..."
		end
	end

	Static_SetText( gHandles["lblItemNameORChooseOne"], g_itemName)
	Static_SetText( gHandles["lblTotalNum"], g_totalPrice )
	EditBox_SetText( gHandles["eBoxQuantity"], g_itemQty, false )

	if g_inventoryType == INVENTORY_TYPE.CREDITS then
		Control_SetEnabled(gHandles["btnRewards"], false)
	elseif g_inventoryType == INVENTORY_TYPE.REWARDS then
		Control_SetEnabled(gHandles["btnCredits"], false)
	end

	return KEP.EPR_CONSUMED
end

function parseUserInfo( txtParse )
	g_userID = ParseTagNum(txtParse, "user_id")
	WebCallCached(GameGlobals.WEB_SITE_PREFIX..BALANCE_SUFFIX..g_userID, WF.HUD_BALANCES)
end

function setBalances()
	Static_SetText(gHandles["txtCredits"], g_playerCredits)
	Static_SetText(gHandles["txtRewards"], g_playerRewards)
end

function setConfiguration()
	setControl(gHandles["lblTotal"], true)
	setControl(gHandles["lblTotalNum"], true)
	setControl(gHandles["lblPurchaseThisWith"], true)
	Static_SetText(gHandles["lblTotalNum"], "")
	setControl(gHandles["iconRavePlus"], false)
	setControl(gHandles["lblRavePlus"], false)
	setControl(gHandles["lblQuantity"], false)
	setControl(gHandles["eBoxQuantity"], false)
	setControl(gHandles["btnUpdate"], false)

	if (g_purchaseType == RAVE_PURCHASE) then
		setControl(gHandles["iconRavePlus"], true)
		setControl(gHandles["lblRavePlus"], true)
		setControl(gHandles["lblItemNameORChooseOne"], true)
	elseif (g_purchaseType == ITEM_OBJECT_PURCHASE) then
		setControl(gHandles["lblItemNameORChooseOne"], true)
		Static_SetText(gHandles["lblItemNameORChooseOne"], "")
		setControl(gHandles["lblQuantity"], true)
		setControl(gHandles["eBoxQuantity"], true)
		setControl(gHandles["btnUpdate"], true)
	end
end

function purchaseItem(currencyType)
	if g_purchaseType == ITEM_OBJECT_PURCHASE then
		sendBuyItemRequest(g_itemGlid, g_itemQty, currencyType)
	elseif g_purchaseType == RAVE_PURCHASE then
		sendBuyRaveRequest(g_ravedID, g_raveType, g_raveTarget, currencyType)
	end
end

function sendBuyItemRequest(glid, qty, strCurrencyType)
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..BUY_DYNAMIC_OBJECT_SUFFIX.."&credit="..strCurrencyType.."&items="..glid.."&qnty="..qty, WF.BUY_DYNAMIC_OBJECT)
end

function sendBuyRaveRequest(ravedID, raveType, raveTarget, strCurrencyType)
	local g_web_address = ""
	strRaveType = "single"
	if raveType == 1 then
		strRaveType = "mega"
	end

	if raveTarget == 0 then --raving player
		g_web_address = GameGlobals.WEB_SITE_PREFIX..RAVE_SUFFIX..ravedID.."&raveType="..strRaveType.."&currency="..strCurrencyType
	else --raving current zone
		g_web_address = GameGlobals.WEB_SITE_PREFIX..SET_RAVES_SUFFIX.."&raveType="..strRaveType.."&currency="..strCurrencyType
	end

	makeWebCall( g_web_address, WF.BUY_RAVE)
end

function sendItemsPurchasedEvent(glid)
	local ev = KEP_EventCreate( ITEM_PURCHASED_EVENT )
	KEP_EventEncodeNumber(ev, 1)
	KEP_EventEncodeNumber( ev, glid)
	KEP_EventQueue( ev )
end

function parseEventHeader(strXmlData, objectContainer)
	objectContainer.ReturnCode = parseDataByTag(strXmlData, "ReturnCode", "(%d+)", 0)
	objectContainer.ResultDescription = parseDataByTag(strXmlData, "ResultDescription", "(.-)", 0)
	return tonumber(objectContainer.ReturnCode)
end

function parseDataByTag(strEventData, strTag, strPattern, iStart)
	local strStartTag = "<" .. strTag .. ">"
	local strEndTag = "</" .. strTag .. ">"
	local strPattern = strStartTag..strPattern..strEndTag
	local retStart, retEnd, strData = string.find(strEventData, strPattern, iStart)
	return strData, retStart, retEnd
end

function btnUpdate_OnButtonClicked( buttonHandle )
	local num = EditBox_GetText( gHandles["eBoxQuantity"] )
	g_itemQty = tonumber(num) or 0
	if g_itemQty < 0 then
		g_itemQty = 0
	end
	if type(g_itemPrice) == "number" and g_itemPrice >= 0 then
		g_totalPrice = g_itemQty * g_itemPrice
	end
	EditBox_SetText( gHandles["eBoxQuantity"], g_itemQty, false )
	Static_SetText( gHandles["lblTotalNum"], g_totalPrice )
end

function btnCredits_OnButtonClicked( buttonHandle )
	purchaseItem(g_CurrencyType.Credits)
end

function btnRewards_OnButtonClicked( buttonHandle )
	purchaseItem(g_CurrencyType.Rewards)
end

function rbRavePlus_OnRadioButtonChanged( rbHandle )
	local cb = RadioButton_GetCheckBox( rbHandle )
	if (cb ~= nil) then
		local checked = CheckBox_GetChecked( cb )
		if (checked) then
			Static_SetText( gHandles["lblTotalNum"], g_RavePlusPrice )
			g_raveType = 0
		end
	end
end


function btnBuyCredits_OnButtonClicked( buttonHandle )
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .. BUY_CREDIT_PACKAGES_SUFFIX )
end
