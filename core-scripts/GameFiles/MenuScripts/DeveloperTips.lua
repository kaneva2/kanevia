--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

DEVELOPER_TIP_CENTER_FOCUS_EVENT = "DeveloperTipCenterFocusEvent"

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "developerTipCenterFocusHandler", DEVELOPER_TIP_CENTER_FOCUS_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( DEVELOPER_TIP_CENTER_FOCUS_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function developerTipCenterFocusHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	MenuBringToFrontThis()
end

function btnChangeZone_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://docs.kaneva.com/mediawiki/index.php/Change_the_Look_of_Your_Zone" )
end

function btnPlaceObjects_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://docs.kaneva.com/mediawiki/index.php/Place_Objects_in_Your_Zone" )
end

function btnAttachScripts_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://docs.kaneva.com/mediawiki/index.php/Attach_Scripts_to_Objects" )
end

function btnEditMenus_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://docs.kaneva.com/mediawiki/index.php/Edit_Your_3D_App_Menus" )
end

function btnPublish_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://docs.kaneva.com/mediawiki/index.php/Update_Your_3D_App_Profile_and_Publish" )
end

function btnImportObjects_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://docs.kaneva.com/mediawiki/index.php/Importing_3D_Objects_in_Your_App" )
end

function btnUsingEmotes_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://docs.kaneva.com/mediawiki/index.php/Using_Emotes_in_Your_App" )
end

function btnAdditionalHelp_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://docs.kaneva.com/" )
end
