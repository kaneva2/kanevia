--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("MenuLocation.lua")

ALLOW_ALL = 1
ALLOW_PRIVATE = 2

ZONE_FILTER = 1
PLAYER_FILTER = 2
GAME_FILTER = 3
GAME_SET_FILTER = 4

g_checkBoxChanged = false

PASSLIST_SUFFIX = "kgp/passlist.aspx?type="
WEB_ADDRESS = GameGlobals.WEB_SITE_PREFIX .. PASSLIST_SUFFIX

g_t_AccessSettings = {}

g_waitingOnServerPassValidation = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "PassListHandler", "PassListEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PassListEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	if not KEP_IsWorld() then
		KEP_MessageBox("World Menu Closed")
		MenuCloseThis()
		return
	end

	Control_SetEnabled( gHandles["rdoEveryone"], false )
	Control_SetEnabled( gHandles["rdoPrivate"], false )

	local cbh = Dialog_GetCheckBox( gDialogHandle, "cbRequireAccess" )
	Control_SetEnabled( cbh, false )

	--Get Private/Public
	local gameId = KEP_GetGameId()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..GET_ACCESS_SUFFIX .. "&gameId=" .. tostring(gameId), WF.ZONE_ACCESS, 1, 1)

	-- get the player's passlists
	makeWebCall( WEB_ADDRESS.."u", PLAYER_FILTER, 1, 1000 )

	--get the current game's passlists
	makeWebCall( WEB_ADDRESS .. "g&gi=" .. tostring(gameId), GAME_FILTER, 1, 1000 )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.ZONE_ACCESS then
		g_accessData = KEP_EventDecodeString( event )
		ParseAccessData()
		return KEP.EPR_CONSUMED
	elseif filter == PLAYER_FILTER then
		local placeData = KEP_EventDecodeString( event )
		s, e, result = string.find(placeData, "<ReturnCode>([0-9]+)</ReturnCode>")
		if result == "0" then
			s, e, totalNumRec = string.find(placeData, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
			s, e, numRec = string.find(placeData, "<NumberRecords>([0-9]+)</NumberRecords>")
			local cbh = Dialog_GetCheckBox(gDialogHandle, "cbRequireAccess")
			if cbh ~= 0 then
				if numRec == "0" then
					Control_SetEnabled( cbh, false )
				else
					for i=1, numRec do
						local strPos = 0
						s, strPos, passIdStr = string.find(placeData, "<PassId>(.-)</PassId>", strPos)
						s, trash, name = string.find(passIdStr, "<name>(.-)</name>", 1)
						s, trash, desc = string.find(passIdStr, "<description>(.-)</description>", 1)
						s, trash, passId = string.find(passIdStr, "<pass_group_id>([0-9]+)</pass_group_id>", 1)
						if passId == "1" then
							Control_SetEnabled( cbh, true )
							break
						end
					end
				end
			end
		end
		return KEP.EPR_CONSUMED
	elseif filter == GAME_FILTER then
		local placeData = KEP_EventDecodeString( event )
		s, e, result = string.find(placeData, "<ReturnCode>([0-9]+)</ReturnCode>")
		if result == "0" then
			s, e, totalNumRec = string.find(placeData, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
			s, e, numRec = string.find(placeData, "<NumberRecords>([0-9]+)</NumberRecords>")
			local cbh = Dialog_GetCheckBox(gDialogHandle, "cbRequireAccess")
			if cbh ~= 0 then
				if numRec == "0" then
					CheckBox_SetChecked( cbh, false )
				else
					for i=1, numRec do
						local strPos = 0
						s, strPos, passIdStr = string.find(placeData, "<Table>(.-)</Table>", strPos)
						s, trash, passId = string.find(passIdStr, "<pass_group_id>([0-9]+)</pass_group_id>", 1)
						s, trash, setByAdmin = string.find(passIdStr, "<set_by_admin>([Y|N])</set_by_admin>", 1)
						if passId == "1" then
							CheckBox_SetChecked( cbh, true )
							if setByAdmin == "Y" and g_t_AccessSettings["IsAdmin"] == false then
								Control_SetEnabled( cbh, false )
							end
							break
						end
					end
				end
			end
		end
		return KEP.EPR_CONSUMED
	end
end

function ParseAccessData()
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	s, strPos, result = string.find(g_accessData, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		s, strPos, result = string.find(g_accessData, "<isPrivate>(.-)</isPrivate>", strPos)
		g_t_AccessSettings["isPrivate"] = strToBool(result)

		s, strPos, result = string.find(g_accessData, "<GM>(.-)</GM>", strPos)
		g_t_AccessSettings["GM"] = strToBool(result)

		s, strPos, result = string.find(g_accessData, "<Moderator>(.-)</Moderator>", strPos)
		g_t_AccessSettings["Moderator"] = strToBool(result)

		s, strPos, result = string.find(g_accessData, "<Owner>(.-)</Owner>", strPos)
		g_t_AccessSettings["Owner"] = strToBool(result)

		s, strPos, result = string.find(g_accessData, "<IsAdmin>(.-)</IsAdmin>", strPos)
		g_t_AccessSettings["IsAdmin"] = strToBool(result)

		if g_t_AccessSettings["Owner"] or g_t_AccessSettings["Moderator"] or g_t_AccessSettings["GM"] then

			Control_SetEnabled( gHandles["rdoEveryone"], true )
			Control_SetEnabled( gHandles["rdoPrivate"], true )

			if g_t_AccessSettings["isPrivate"] then
				g_currentAccessSetting = ALLOW_PRIVATE
				RadioButton_SetChecked(gHandles["rdoPrivate"], true)
			else
				g_currentAccessSetting = ALLOW_ALL
				RadioButton_SetChecked(gHandles["rdoEveryone"], true)
			end
		else
			displayNoRightsMsg()
		end
	elseif result == "-1" then
		displayNoRightsMsg()
	end
end

function displayNoRightsMsg()
	KEP_MessageBox("You cannot change access rights here")
	MenuCloseThis()
end

function PassListHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	-- only handle this if this menu initiated the change
	if ( g_waitingOnServerPassValidation ) then
		g_waitingOnServerPassValidation = false

		-- first set of data contains the passes the player has
		local playerPassCount =	KEP_EventDecodeNumber( event )
		for i = 1, playerPassCount do
			local passId = KEP_EventDecodeNumber( event )
		end

		local webExt = "gd"

		-- the second set of data contains what the zone passes have been set to
		local passCount = KEP_EventDecodeNumber( event )
		if ( passCount ~= 0 ) then
			for i = 1, passCount do
				local passId = KEP_EventDecodeNumber( event )
				if ( passId == 1 ) then
					webExt = "ga"
					break
				end
			end
		end

		-- we have been waiting on server validation of access pass change before telling WOK and closing the menu
		local gameId = KEP_GetGameId()
		makeWebCall( WEB_ADDRESS .. webExt .. "&gi=" .. tostring(gameId) .. "&pi=" .. tostring(1), GAME_SET_FILTER)

		MenuCloseThis()
	end
end

function btnAccept_OnButtonClicked(buttonHandle)
	
	if g_currentAccessSetting ~= nil then
		local gameId = KEP_GetGameId()
		local web_address = GameGlobals.WEB_SITE_PREFIX..SET_ACCESS_SUFFIX..g_currentAccessSetting.."&cover="..0 .. "&gameId=" .. tostring(gameId)
		makeWebCall( web_address, WF.SET_ZONE_ACCESS)
	else
		KEP_MessageBox("Please select an access level")
	end

	if g_checkBoxChanged then
		local cbh = Dialog_GetCheckBox( gDialogHandle, "cbRequireAccess" )
		local apString = "passList?del=1"
		if CheckBox_GetChecked( cbh ) == 1 then
			apString = "passList?add=1"
		end

		e = KEP_EventCreate( BrowserIds.BROWSER_REQUEST_EVENT_NAME )
		KEP_EventEncodeString( e, apString)
		KEP_EventAddToServer( e )
		KEP_EventQueue( e )

		g_waitingOnServerPassValidation = true
	else
		-- no need to wait on server validation of access pass change, close the menu
		MenuCloseThis()
	end
end

function rdoEveryone_OnRadioButtonChanged( radioBtnHandle )
	g_currentAccessSetting = ALLOW_ALL
end

function rdoPrivate_OnRadioButtonChanged( radioBtnHandle )
	g_currentAccessSetting = ALLOW_PRIVATE
end

function cbRequireAccess_OnCheckBoxChanged()
	g_checkBoxChanged = true
end
