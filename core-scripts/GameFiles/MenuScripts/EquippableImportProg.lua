--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("UGCImportProgCommon.lua")
dofile("..\\Scripts\\EquippableImportStateIds.lua")

gModuleName = "EquippableObjectImport"	-- used in UGCImportProgCommon.lua
gUgcTypeDesc = "Equippable Object"		-- used in UGCImportProgCommon.lua
gImportStateGetSetIds = EquippableImportStateIds
gOrigBoundBox = nil

gOptionNames = { UGCOPT_EI_MAXTRICOUNT, UGCOPT_EI_MAXSIZE, UGCOPT_EI_MAXTEXCOUNT, UGCOPT_EI_MAXTEXSIZE }
gOptions = nil

function UGCI.RegisterEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "UGCImportConfirmationHandler", UGC_IMPORT_CONFIRMATION_RESULT_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UGCGlobalOptionsResultHandler", UGC_GLOBAL_OPTIONS_RESULT_EVENT, KEP.MED_PRIO )
end

function UGCI.RegisterEvents(dispatcher, handler, debugLevel )
	KEP_EventRegister( UGC_IMPORT_CONFIRMATION_RESULT_EVENT, KEP.MED_PRIO )
end

function UGCI.OnCreateDialog()
	-- Request UGC global options for EI
	UGCI.GetGlobalOptions( UGC_FILTER.EI_IMPORT_PROG, gOptionNames )
end

function UGCI.OnDestroyDialog()
-- do nothing
end

function UGCI.OnPreviewing( impSession, impSessionId )
	changeDialogState( STATE_ANALYZING )
	Dialog_SetMinimized( gDialogHandle, 0 )
end

function UGCI.OnPreviewed( impSession, impSessionId )
	-- tell state machine to continue import process
	UGC_ContinueImportAsync( impSessionId )
end

function UGCI.OnImporting( impSession, impSessionId )
	changeDialogState( STATE_IMPORTING )
	Dialog_SetMinimized( gDialogHandle, 0 )
end

function UGCI.OnImported( impSession, impSessionId )
	-- tell state machine to continue import process
	UGC_ContinueImportAsync( impSessionId )
end

function UGCI.OnConverting( impSession, impSessionId )
	changeDialogState( STATE_IMPORTING )
	Dialog_SetMinimized( gDialogHandle, 0 )
end

function UGCI.OnConverted( impSession, impSessionId )

	-- client-side validation for triangle count and texture count
	local triCount = GetSet_GetNumber( impSession, gImportStateGetSetIds.TRIANGLECOUNT )
	local texCount = GetSet_GetNumber( impSession, gImportStateGetSetIds.TEXTURECOUNT )

	if gOptions==nil or gOptions[UGCOPT_EI_MAXTRICOUNT]==nil or gOptions[UGCOPT_EI_MAXTEXCOUNT]==nil then
		-- warn about missing global UGC options
		LogError( "UGCI.OnConverted: import options not available for client-side validation" )
	end

	-- bring up information dialog if error found
	if gOptions~=nil then
		local cancelImport = false
		if gOptions[UGCOPT_EI_MAXTRICOUNT]~=nil and triCount>gOptions[UGCOPT_EI_MAXTRICOUNT] then
			UGCI.DisplayImportFailedMenu( "UGCImportTriangleCountWarning", { tostring(gOptions[UGCOPT_EI_MAXTRICOUNT]), tostring(triCount), "Accessories" } )
			cancelImport = true
		else
			if gOptions[UGCOPT_EI_MAXTEXCOUNT]~=nil and texCount>gOptions[UGCOPT_EI_MAXTEXCOUNT] then
				UGCI.DisplayImportFailedMenu( "UGCImportTextureCountWarning", { tostring(gOptions[UGCOPT_EI_MAXTEXCOUNT]), tostring(texCount), "Accessories" } )
				cancelImport = true
			end
		end
		if cancelImport then
			gImportCancelledMsgOnBottom = true
			UGC_CancelImportAsync( impSessionId )
			return
		end
	end

	-- obtain original boundbox size (after initial conversion)
	if gOrigBoundBox==nil then
		gOrigBoundBox = {}
		local minx, miny, minz = GetSet_GetVector( impSession, gImportStateGetSetIds.BOUNDBOXMIN )
		local maxx, maxy, maxz = GetSet_GetVector( impSession, gImportStateGetSetIds.BOUNDBOXMAX )
		gOrigBoundBox.minX = minx
		gOrigBoundBox.minY = miny
		gOrigBoundBox.minZ = minz
		gOrigBoundBox.maxX = maxx
		gOrigBoundBox.maxY = maxy
		gOrigBoundBox.maxZ = maxz
	end

	local ignoreMissingTexture = GetSet_GetNumber( impSession, gImportStateGetSetIds.IGNOREMISSINGTEXTURES )

	if ignoreMissingTexture==0 then
		local missingTextureCount = UGC_GetMissingTextureCount( impSessionId )
		if missingTextureCount > 0 then
			-- hide progress menu
			Dialog_SetMinimized( gDialogHandle, 1 )
			-- display missing texture confirmation
			MenuOpenModal("UGCImportMissingTextures.xml")
			-- pass ImpSessionId to confirmation menu
			local e = KEP_EventCreate( UGC_IMPORT_CONFIRMATION_MENU_EVENT )
			KEP_EventSetFilter( e, UIC.MISSING_TEXTURE )
			KEP_EventSetObjectId( e, impSessionId )
			KEP_EventQueue( e )
			return
		end
	end

	-- bring up import options dialog
	Dialog_SetMinimized( gDialogHandle, 1 )
	UGCI.CallUGCHandler( "EquippableImportOptions.xml", 0, gDropId, gImportInfo, gTargetInfo, gBtnState, gKeyState )
end

function UGCI.OnProcessing( impSession, impSessionId )
	changeDialogState( STATE_IMPORTING )
	Dialog_SetMinimized( gDialogHandle, 0 )
end

function UGCI.OnProcessed( impSession, impSessionId )
	local internalError = true

	if impSession~=nil then

		--KEP_SendChatMessage( ChatType.System, "Import succeeded" )
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, "Import succeeded!")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)


		internalError = false

		MenuCloseThis()

		local localGLID = GetSet_GetNumber( impSession, gImportStateGetSetIds.LOCALGLID )

		local s, e, fileName
		s, e, fileName = string.find( gImportInfo.fullPath, ".*\\(.+)" )
		if fileName==nil then
			fileName = gImportInfo.fullPath
		end

		-- Notify UGCManager.lua
		UGCI.SendImportCompletionEvent( UGC_TYPE.EQUIPPABLE, localGLID, 0, fileName, 0, 0, 1 )
	end

	if internalError then
		--KEP_SendChatMessage( ChatType.System, "Import failed due to an internal error. You may close and reopen Kaneva Client and try it again." )
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, "Import failed due to an internal error. You may close and reopen Kaneva Client and try it again.")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
	end
end

function UGCI.OnPreviewFailed( impSession, impSessionId )
	local errorCount = UGC_GetErrorCount( impSessionId )

	if errorCount > 0 then
		local composedErrorMsg = ""
		for vId=0,errorCount-1 do
			local desc, action = UGC_GetErrorInfo( impSessionId, vId )
			LogError( "EquippableImportProg: " ..desc .. ". Suggested action: " .. action )
			composedErrorMsg = composedErrorMsg .. "\n" .. desc
		end

		return composedErrorMsg
	end
end

function UGCI.OnImportFailed( impSession, impSessionId )
-- do nothing
end

function UGCI.OnProcessingFailed( impSession, impSessionId )
-- do nothing
end

function UGCI.OnCancelled( impSession, impSessionId )
-- do nothing
end

function UGCImportConfirmationHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local answer = KEP_EventDecodeNumber( event )
	local ImpSessionId = objectid
	local ImpSession = UGC_GetImportSession( ImpSessionId )
	if answer~=0 then

		-- ignore missing texture error
		GetSet_SetNumber( ImpSession, gImportStateGetSetIds.IGNOREMISSINGTEXTURES, 1 )

		-- bring up import options dialog
		UGCI.CallUGCHandler("EquippableImportOptions.xml", 0, gDropId, gImportInfo, gTargetInfo, gBtnState, gKeyState )

	else
		-- cancel import
		UGC_CancelImportAsync( ImpSessionId )
	end
end

function UGCGlobalOptionsResultHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter==UGC_FILTER.EI_IMPORT_PROG then
		gOptions = UGCI.ParseGlobalOptionsResultEvent( event )
	end
	-- do not consume event
end
