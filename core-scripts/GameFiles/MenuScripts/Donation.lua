--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Donation.lua
-- 
-- Donation selection menu
-------------------------------------------------------------------------------
dofile("HUDHelper.lua")

local m_donations = {}
local m_playerName = ""
local m_communityId = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister ("UPDATE_CREDIT_TRANSACTION", KEP.MED_PRIO)
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	requestLocationData() -- get zone information
	requestDonationData() -- get donation amounts and item information
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

-- Dialog_OnRender - Called upon screen render. Animate your script here.
function Dialog_OnRender(dialogHandle, elapsedSec)
end

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- If browser return for Location Data web call
	if filter == WF.PLACE_INFO then
		local estr = KEP_EventDecodeString( tEvent )
		ParseLocationData(estr)
	   	return KEP.EPR_CONSUMED
	-- If browser return for Donation Data web call
	elseif filter == WF.DONATION_INFO then
		local returnData = KEP_EventDecodeString( tEvent )
		ParseDonationData( returnData )
	   	return KEP.EPR_CONSUMED
	end
end

-- Web call to receive current information for available donation options
function requestDonationData()
	local address = GameGlobals.WEB_SITE_PREFIX .. GET_PREMIUM_ITEMS_SUFFIX .. "1"
	makeWebCall( address, WF.DONATION_INFO )
end

-- Web call to receive the current zone's location data
function requestLocationData()
	local address = GameGlobals.WEB_SITE_PREFIX..GET_PLACE_INFORMATION_SUFFIX
	makeWebCall( address , WF.PLACE_INFO)
end

-- Read donation data web call return
function ParseDonationData(xmlData)
	local result = string.match(xmlData, "<ReturnCode>([0-9]+)</ReturnCode>")
	-- if the web call is a success
	if result == "0" then
		local donation_count = 1 -- used to index current table location of item
		-- for each donation item returned, store the GLID, name, and price
		for donationItem in string.gmatch(xmlData, "<Table>(.-)</Table>") do
			local t_item = {}
			t_item["GLID"] = tonumber(string.match(donationItem,"<global_id>(.-)</global_id>"))
			t_item["name"] = string.match(donationItem,"<name>(.-)</name>")
			t_item["price"] = tonumber(string.match(donationItem, "<selling_price>(.-)</selling_price>"))

			table.insert(m_donations,donation_count,t_item)
			donation_count = donation_count + 1
		end
	end
	for i=1, 4 do
		if m_donations[i] == nil then
			LogWarn("Donation value ["..tostring(i).."] was not returned from the web")
			MenuCloseThis()
			break
		end
	end
end

-- Read location data web call return
function ParseLocationData(locationData)
    local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	s, strPos, m_playerName = string.find(locationData, "<creator_username>(.-)</creator_username>")
	s, strPos, m_communityId = string.find(locationData, "<community_id>(.-)</community_id>")
	-- Nil check returns
	if m_playerName == nil then
		LogWarn("No player name returned from GetPlaceInformation web call Return xml["..tostring(locationData).."]")
		MenuCloseThis()
	elseif m_communityId == nil then
		LogWarn("No community ID returned from GetPlaceInformation web call Return xml["..tostring(locationData).."]")
		MenuCloseThis()
	end
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

-- Donation button visibility changes
function btn10000_OnMouseEnter(buttonHandle)
	Control_SetVisible(gHandles["img10000Light"], true)
end
function btn10000_OnMouseLeave(buttonHandle)
	Control_SetVisible(gHandles["img10000Light"], false)
end

function btn2000_OnMouseEnter(buttonHandle)
	Control_SetVisible(gHandles["img2000Light"], true)
end
function btn2000_OnMouseLeave(buttonHandle)
	Control_SetVisible(gHandles["img2000Light"], false)
end

function btn1000_OnMouseEnter(buttonHandle)
	Control_SetVisible(gHandles["img1000Light"], true)
end
function btn1000_OnMouseLeave(buttonHandle)
	Control_SetVisible(gHandles["img1000Light"], false)
end

function btn200_OnMouseEnter(buttonHandle)
	Control_SetVisible(gHandles["img200Light"], true)
end
function btn200_OnMouseLeave(buttonHandle)
	Control_SetVisible(gHandles["img200Light"], false)
end

-- Donation button clicks
function btn200_OnButtonClicked(controlHandle)
	if m_donations[1] and m_communityId and m_playerName then
		local event = KEP_EventCreate("UPDATE_CREDIT_TRANSACTION")
		KEP_EventEncodeString(event, m_donations[1].price)
		KEP_EventEncodeString(event, m_donations[1].GLID)
		KEP_EventEncodeString(event, m_communityId)
		KEP_EventEncodeString(event, "Donation")
		KEP_EventEncodeString(event, m_playerName)
		KEP_EventQueue(event)
		toggleMenu("CreditTransaction")
		MenuCloseThis()
	end
end

function btn1000_OnButtonClicked(controlHandle)
	if m_donations[2] and m_communityId and m_playerName then
		local event = KEP_EventCreate("UPDATE_CREDIT_TRANSACTION")
		KEP_EventEncodeString(event, m_donations[2].price)
		KEP_EventEncodeString(event, m_donations[2].GLID)
		KEP_EventEncodeString(event, m_communityId)
		KEP_EventEncodeString(event, "Donation")
		KEP_EventEncodeString(event, m_playerName)
		KEP_EventQueue(event)
		toggleMenu("CreditTransaction")
		MenuCloseThis()
	end
end

function btn2000_OnButtonClicked(controlHandle)
	if m_donations[3] and m_communityId and m_playerName then
		local event = KEP_EventCreate("UPDATE_CREDIT_TRANSACTION")
		KEP_EventEncodeString(event, m_donations[3].price)
		KEP_EventEncodeString(event, m_donations[3].GLID)
		KEP_EventEncodeString(event, m_communityId)
		KEP_EventEncodeString(event, "Donation")
		KEP_EventEncodeString(event, m_playerName)
		KEP_EventQueue(event)
		toggleMenu("CreditTransaction")
		MenuCloseThis()
	end
end

function btn10000_OnButtonClicked(controlHandle)
	if m_donations[4] and m_communityId and m_playerName then
		local event = KEP_EventCreate("UPDATE_CREDIT_TRANSACTION")
		KEP_EventEncodeString(event, m_donations[4].price)
		KEP_EventEncodeString(event, m_donations[4].GLID)
		KEP_EventEncodeString(event, m_communityId)
		KEP_EventEncodeString(event, "Donation")
		KEP_EventEncodeString(event, m_playerName)
		KEP_EventQueue(event)
		toggleMenu("CreditTransaction")
		MenuCloseThis()
	end
end