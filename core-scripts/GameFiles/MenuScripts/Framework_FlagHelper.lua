--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--------------------------------------
-- FLAG FUNCTIONALITY
-- This class works in conjunction with the FRAMEWORK_RETURN_FLAGS event to maintain a list of flags in the current zone
-- Some expediency functions are provided to cater for commonly used functionality.
--------------------------------------

---------------------------------
----DOMINANT FLAG RULES:
--If building or destruction, Land Claim flag setting is top priority 
--If point is only in one Flag, use that flag's setting
--If point is in overlapping flags, the flag with the smallest radius is the dominant flag
--If the radii are the same for the two flags, the setting falls to the world setting
---------------------------------

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox = _G.Toolbox


-----------------------------------------------------------------
-- Define the library
-----------------------------------------------------------------
Flag = {}

local BOUND_CHK_BUFFER = 0.001 -- Amount of leeway for "is X within bounds of Y" checks
local BOUND_CHK_BUFFER_Y = 1.5 -- Amount of leeway for checks BELOW the Y value (accounts for slight slopes and the fact that the player avatar is technically slightly beneath the ground)

-- Add a convenience function to the flag, get some abstraction in there..
function Flag.isPlayerMember(self, playerName)
	return self.membersBag and self.membersBag[playerName] == true
end

function Flag.isPointInBoundary(self, x, y, z)
	-- OLD SPHERE CHECK
	-- CJW, NOTE: Multiplication is more efficient than taking the power or squarerooting
	-- local xDiff = x - self.position.x
	-- local yDiff = y - self.position.y
	-- local zDiff = z - self.position.z
	-- local xyzDistanceSqrd = xDiff * xDiff + zDiff * zDiff + yDiff * yDiff
	-- local radiusSqrd = self.radius * self.radius
	-- return (radiusSqrd >= xyzDistanceSqrd)

	-- NEW CUBE CHECK
	local xMin = self.position.x - (self.depth /2) - BOUND_CHK_BUFFER
	local xMax = self.position.x + (self.depth /2) + BOUND_CHK_BUFFER
	local yMin = self.position.y - BOUND_CHK_BUFFER_Y
	local yMax = self.position.y + self.height + BOUND_CHK_BUFFER
	local zMin = self.position.z - (self.width /2) - BOUND_CHK_BUFFER
	local zMax = self.position.z + (self.width / 2) + BOUND_CHK_BUFFER

	if  x > xMin and x < xMax and
		y > yMin and y < yMax and
		z > zMin and z < zMax then
		return true
	end
	return false
end

function Flag.isFlagInBoundary(self, otherFlag)
	-- OLD SPHERE CHECK
	-- CJW, NOTE: Multiplication is more efficient than taking the power or squarerooting
	-- local xDiff = otherFlag.position.x - self.position.x
	-- local yDiff = otherFlag.position.y - self.position.y
	-- local zDiff = otherFlag.position.z - self.position.z
	-- local xyzDistanceSqrd = xDiff * xDiff + zDiff * zDiff + yDiff * yDiff
	-- local radiusSum = self.radius + otherFlag.radius
	-- local radiusSqrd = radiusSum * radiusSum
	-- return (radiusSqrd >= xyzDistanceSqrd)


	-- NEW CUBE CHECK
	local flagDepth 		= (self.depth /2 ) or 0
	local otherFlagDepth 	= (otherFlag.depth / 2) or 0
	local flagHeight 		= self.height or 0
	local otherFlagHeight 	= otherFlag.height or 0
	local flagWidth 		= (self.width / 2) or 0
	local otherFlagWidth 	= (otherFlag.width /2) or 0
	-- flag max/min values
	local xMin = self.position.x - flagDepth - BOUND_CHK_BUFFER
	local xMax = self.position.x + flagDepth + BOUND_CHK_BUFFER
	local yMin = self.position.y - BOUND_CHK_BUFFER_Y
	local yMax = self.position.y + flagHeight + BOUND_CHK_BUFFER
	local zMin = self.position.z - flagWidth - BOUND_CHK_BUFFER
	local zMax = self.position.z + flagWidth + BOUND_CHK_BUFFER
	-- otherFlag max/min values
	local xMin2 = otherFlag.position.x - otherFlagDepth - BOUND_CHK_BUFFER
	local xMax2 = otherFlag.position.x + otherFlagDepth + BOUND_CHK_BUFFER
	local yMin2 = otherFlag.position.y - BOUND_CHK_BUFFER_Y
	local yMax2 = otherFlag.position.y + otherFlagHeight + BOUND_CHK_BUFFER
	local zMin2 = otherFlag.position.z - otherFlagWidth - BOUND_CHK_BUFFER
	local zMax2 = otherFlag.position.z + otherFlagWidth + BOUND_CHK_BUFFER

	if  (xMin < xMax2 and xMax > xMin2) and
		(yMin < yMax2 and yMax > yMin2) and
		(zMin < zMax2 and zMax > zMin2) then
		return true
	end
	return false
end


FlagSystem = {}	-- Pseudo-class system for retrieving flag info from a central point (implemented so as to keep related game logic in one place...)

function FlagSystem.create(self)

	self.m_buildFlags = {}  --used to track "playerbuild"  flags 
	self.m_destructionFlags = {} --used to track "destruction"  flags
	self.m_combatFlags = {} -- used to track "pvp" flags
	self.m_claimFlags = {} --used to claim "claim" flags
	self.m_flagsMap = {} -- Map the PIDs of the flags to be able to efficiently check whether or not a PID belongs to a flag.
end

-- Resets the flag information in the world
function FlagSystem.resetFlags(self, flags)
	if flags then
		for i, flag in pairs(flags) do
			self:addFlag(flag)
		end
	end
end

function FlagSystem.addFlag(self, flag)
	if flag and flag.flagType then
		local PID = flag.PID
		local FlagTemplate = _G.Flag -- Assign to local variable to reduce performance hit for referring to global variable.
		self.m_flagsMap[PID] = flag
		--if self.m_flagsMap[PID] then
		if flag.flagType == "claimFlag" then
			flag.membersBag = Toolbox.convertContainerToSet(flag.members)
			self.m_claimFlags[PID] = flag
			if flag.noDestruct and flag.noDestruct ~= false and flag.noDestruct ~= "false" then
				self.m_destructionFlags[PID] = flag
			elseif self.m_destructionFlags[PID] then
				self.m_destructionFlags[PID] = nil
			end
		elseif flag.flagType == "playerBuild" then
			self.m_buildFlags[PID] = flag
		elseif flag.flagType == "pvp"  then
			self.m_combatFlags[PID] = flag
		elseif flag.flagType == "playerDestruction" then
			self.m_destructionFlags[PID] = flag
		end 
		setmetatable(flag, {__index = FlagTemplate})
	end
end

function FlagSystem.removeFlag(self, PID)
	if PID then
		local objectPID = tonumber(PID)
		if self.m_flagsMap[objectPID] then
			self.m_flagsMap[objectPID] = nil
			self.m_buildFlags[objectPID] = nil
			self.m_combatFlags[objectPID] = nil
			self.m_claimFlags[objectPID] = nil
			self.m_destructionFlags[objectPID] = nil
		end
	end
end

-- Tells you whether a given object is within the boundaries of a claim flag that the player is a member of
-- Note: The "objectPos" argument is optionally provided by the user to speed up the function 
function FlagSystem.checkLandClaimFlagMembershipForPlayer(self, playerName, objectPID, objectPos)
	-- Do we have a position provided?
	local pos = objectPos
	if pos == nil then 
		pos = {}
		pos.x, pos.y, pos.z = KEP_DynamicObjectGetPosition(objectPID)
	end

	local flags = self:getLandClaimFlagsAtPoint(pos.x, pos.y, pos.z)
	local retValue = false
	
	-- Check all flags the object is in and fail if player is not a member of either one
	for i, flag in pairs(flags) do
		if not flag:isPlayerMember(playerName) then
			retValue = false
			break
		else
			retValue = true
		end
	end
	return retValue
end
	
-- Get a list of all claim flags whose boundaries encompass the given location
function FlagSystem.getLandClaimFlagsAtPoint(self, x, y, z)
	local flags = {}

	for i, flag in pairs(self.m_claimFlags) do
		if flag.flagType == "claimFlag" then
			if flag:isPointInBoundary(x, y, z) then
				table.insert(flags, flag)
			end
		end
	end
	
	return flags
end

-- Is the placeable object whose ID is given a claim flag?
function FlagSystem.isPlaceableObjectLandClaimFlag(self, objectPID)
	local flag = self.m_flagsMap and self.m_flagsMap[objectPID]
	return flag and flag.flagType == "claimFlag"
end

--Return setting for flags
function FlagSystem.getDominantFlagSetting(self, setting, defaultSetting, x, y, z )
	local flagTable = {}
	local dominantFlag = nil
	if setting == "destruction" then
		flagTable = self.m_destructionFlags
	elseif setting == "pvp" then
		flagTable = self.m_combatFlags
	end

	for i, flag in pairs(flagTable) do
		local isLocationWithinFlag = flag:isPointInBoundary(x, y, z)
		-- If within a player mode enabled flag
		if isLocationWithinFlag then
			if setting  == "destruction" and flag.flagType == "claimFlag" then
				return false
			end

			if dominantFlag then
				local dominantArea = dominantFlag.width * dominantFlag.height * dominantFlag.depth
				local flagArea = flag.width * flag.height * flag.depth
				if( dominantArea > flagArea ) then
					dominantFlag = flag
				elseif( dominantArea == flagArea ) then
					-- If the radii are the same, we should refer the matter to the world setting
					if flag.enabled == defaultSetting then
						dominantFlag = flag
					end
				end
			else
				dominantFlag = flag
			end
		end
	end

	if( not dominantFlag ) then
		return defaultSetting
	else
		return dominantFlag.enabled
	end
end

function FlagSystem.getBuildFlags(self)
	return self.m_buildFlags
end

function FlagSystem.getCombatFlags(self )
	return self.m_combatFlags
end

function FlagSystem.getDestructionFlags(self)
	return self.m_destructionFlags
end

function FlagSystem.getClaimFlags(self )
	return self.m_claimFlags
end