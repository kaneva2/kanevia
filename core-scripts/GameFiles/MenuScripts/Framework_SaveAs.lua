--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_SaveAs.lua
-- 
-- Displays name for Save As confirmation
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- When the menu is created
function onCreate()
	KEP_EventRegisterHandler("saveAsNameHandler", "FRAMEWORK_SAVEASNAME", KEP.HIGH_PRIO)
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Catches relevant data for this menu to display
function saveAsNameHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local name, GLID
	-- Better safe than sorry
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		name = KEP_EventDecodeString(tEvent)
	end
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		GLID = KEP_EventDecodeNumber(tEvent)
	end
	if name and GLID then
		EditBox_SetText(gHandles["edNewName"], tostring(name), false)
		
		local eh = Image_GetDisplayElement(gHandles["imgNewItem"])
		KEP_LoadIconTextureByID(GLID, eh, gDialogHandle)
	end
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

-- Save
function btnSave_OnButtonClicked(buttonHandle)
	-- Let the Game Editor pack the Game Item up and try to save it
	local event = KEP_EventCreate("FRAMEWORK_SAVEAS")
	local name = EditBox_GetText(gHandles["edNewName"])
	KEP_EventEncodeString(event, name)
	KEP_EventQueue(event)
	
	MenuCloseThis()
end
