--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

------------------------------------------------------------
--- ED-3827: Event processing time experiment
--- c1e8b9d5-b4a0-11e5-bd39-002219919c2a
------------------------------------------------------------

-- Singleton
local s_currentExperiment

-- Experiment groups
local GROUPS = { 
	A = { 
		GUID = "f4e3ba1e-b4a0-11e5-bd39-002219919c2a", 
		inGameTimeMs = 1, loadingTimeMs = 1000, legacyMode = 1,
	},
	B = { 
		GUID = "322da5d1-b4a1-11e5-bd39-002219919c2a", 
		inGameTimeMs = 8, loadingTimeMs = 1000, legacyMode = 0,
	},
}

--------------------------------------------
-- Experiment class
EventProcTimeExperiment = {}

-- Allocate experiment
function EventProcTimeExperiment.create()
	local exp = {}
	setmetatable(exp, { __index = EventProcTimeExperiment })
	exp:init(EventProcTimeExperiment.getExperimentGroup())
	s_currentExperiment = exp
	return exp
end

-- Determine the experiment group current user is in
function EventProcTimeExperiment.getExperimentGroup()
	local lookupByGUID = {}
	for k, v in pairs(GROUPS) do
		lookupByGUID[v.GUID] = k
	end

	for GUID in string.gmatch(KEP_GetABGroupList(), "([^,]+),") do
		if lookupByGUID[GUID] then
			return GUID, lookupByGUID[GUID]
		end
	end
end

-- Current experiment object
function EventProcTimeExperiment.get() 
	return s_currentExperiment
end

-- Constructor
function EventProcTimeExperiment.init(self, GUID, group)
	KEP_Log("EventProcTimeExperiment::init({" .. tostring(GUID) .. "}, " .. tostring(group) .. ")")

	-- Construct current experiment
	self.m_group = group or ""
	if self.m_group == "" then
		KEP_Log("EventProcTimeExperiment::init - experiment group not assigned, use control group")
		self.m_group = "A"	-- control group
	end

	-- Apply configurations
	local params = GROUPS[self.m_group]
	if params == nil then
		KEP_LogError("EventProcTimeExperiment::init: no definitions for group [" .. tostring(self.m_group) .. "]")
		return
	end

	KEP_ConfigEventDispatchTime(params.inGameTimeMs, params.loadingTimeMs, params.legacyMode)
end
