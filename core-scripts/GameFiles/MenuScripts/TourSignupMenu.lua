--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

VALIDATION_EVENT_NAME = "TourSignupValidation"
SIGNUP_EVENT_NAME = "PlayerTourSignup"

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( VALIDATION_EVENT_NAME, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function btnSignup_OnButtonClicked( buttonHandle)
	local ev = KEP_EventCreate( SIGNUP_EVENT_NAME)
	KEP_EventAddToServer( ev )
	KEP_EventQueue( ev)
	MenuCloseThis()
end

function btnHelp_OnButtonClicked( buttonHandle)
	MenuOpen("TourHelpMenu.xml")
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
