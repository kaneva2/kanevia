--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Menu.lua - Kaneva Menu Script Template
-- 
-- Template script for creating a new Kaneva client menu.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_DragDropHelper.lua")
dofile("MenuAnimation.lua")

local SYSTEM_MENUS = {	"Framework_Backpack.xml", --1
						"Framework_PlayerQuests.xml", --2
						"Framework_PlayerCrafting.xml", --3
						"Framework_PlayerMeta.xml"
						}
local ARMOR_MENU = "Framework_Armor.xml"

local GAME_SYSTEM_SNAP_T0 = {x = 10, y = 60}
local SHOW_MENU_SYSTEM_SNAP_T0 = {x = 115, y = 29}
local ARMOR_WIDTH = 160

local m_armorOpen = false
local m_selectedIndex = 0
local m_dragging = false
local m_snapToRight = false
local m_lastPosition

--Tweens
local m_tweening = false

-- When the menu is created
function onCreate()	
	KEP_EventRegister("FRAMEWORK_OPEN_ARMOR_MENU", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("toggleArmorMenu", "FRAMEWORK_OPEN_ARMOR_MENU", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("openCraftingMenu", "FRAMEWORK_OPEN_CRAFTING_MENU", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("openQuestMenu", "FRAMEWORK_OPEN_QUEST_MENU", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("closeBackpack", "CLOSE_BACKPACK", KEP.HIGH_PRIO)

	local menuSize = MenuGetSizeThis() 
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local snapToX = screenWidth - menuSize.width

	--Offset Menu to Righ side
	MenuSetLocationThis(snapToX, GAME_SYSTEM_SNAP_T0.y)

	for i = 1, #SYSTEM_MENUS do
		-- On system click
		_G["btnSystem" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			local s, e, index = string.find(Control_GetName(btnHandle), "(%d+)")			
			toggleSystem(tonumber(index))
		end

		_G["btnSystem" .. tostring(i) .. "_OnMouseEnter"] = function(btnHandle)
			local s, e, index = string.find(Control_GetName(btnHandle), "(%d+)")
			Control_SetVisible(gHandles["imgItemOver"..index], true)
		end

		_G["btnSystem" .. tostring(i) .. "_OnMouseLeave"] = function(btnHandle)
			local s, e, index = string.find(Control_GetName(btnHandle), "(%d+)")
			Control_SetVisible(gHandles["imgItemOver"..index], false)
		end
	end	

	m_lastPosition = MenuGetLocationThis()

	--Hopefully Temp fix to race condition
	local tweenDelay = Tween(gDialogHandle)
	tweenDelay:on(CALLBACK.STOP, function() toggleSystem(1) end)
	tweenDelay:start()
end

function onDestroy()
	for i=1,#SYSTEM_MENUS do
		if MenuIsOpen(SYSTEM_MENUS[i]) then
			MenuClose(SYSTEM_MENUS[i])
		end
	end

	if MenuIsOpen(ARMOR_MENU) then
		MenuClose(ARMOR_MENU)
	end
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	MenuAnimation_OnRender(fElapsedTime)

	if m_selectedIndex > 0 then
		curLoc = MenuGetLocationThis()
		if not m_dragging then
			if (curLoc.x ~= m_lastPosition.x or curLoc.y ~= m_lastPosition.y) and Dialog_IsLMouseDown(dialogHandle) == 1 then
				m_dragging = true
				MenuBringToFrontThis()

				local loc = getMenuSnapLocation()
				if m_armorOpen then
					MenuSetLocation(ARMOR_MENU, loc.x, loc.y)
					MenuSetPassthrough(ARMOR_MENU, true)
					MenuBringToFront(ARMOR_MENU)
					loc.x = loc.x + ARMOR_WIDTH		
				end

				MenuSetLocation(SYSTEM_MENUS[m_selectedIndex], loc.x, loc.y)
				MenuSetPassthrough(SYSTEM_MENUS[m_selectedIndex], true)				
				MenuBringToFront(SYSTEM_MENUS[m_selectedIndex])

				m_snapToRight = false
			end
		else
			local loc = getMenuSnapLocation()

			if m_armorOpen then
				MenuSetLocation(ARMOR_MENU, loc.x, loc.y)
				loc.x = loc.x + ARMOR_WIDTH	
			end

			MenuSetLocation(SYSTEM_MENUS[m_selectedIndex], loc.x, loc.y)

			if Dialog_IsLMouseDown(dialogHandle) == 0 then
				m_dragging = false
				MenuSetPassthrough(SYSTEM_MENUS[m_selectedIndex], false)
			end
		end

		m_lastPosition = curLoc
	end	
end

function Dialog_OnMoved(dialogHandle, x, y)
	if m_selectedIndex > 0 then
		local loc = getMenuSnapLocation()
		if m_armorOpen then
			MenuSetLocation(ARMOR_MENU, loc.x, loc.y)
			MenuSetLocation(SYSTEM_MENUS[m_selectedIndex], loc.x + ARMOR_WIDTH, loc.y)
		else
			MenuSetLocation(SYSTEM_MENUS[m_selectedIndex], loc.x, loc.y)
		end
		
	end
end

function closeBackpack(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	MenuCloseThis()
end

function toggleSystem(index)
	if m_selectedIndex > 0 then
		Control_SetVisible(gHandles["imgItemSelect"..m_selectedIndex], false)
		closeSystem()
	end	

	m_selectedIndex = index

	Control_SetVisible(gHandles["imgItemSelect"..m_selectedIndex], true)

	openSystem()

	if m_armorOpen then
		m_systemMove = nil
		closeArmorMenu()
	end
end

function openSystem()
	if MenuIsOpen(SYSTEM_MENUS[m_selectedIndex]) then
		MenuSetMinimized(SYSTEM_MENUS[m_selectedIndex], false)
		if m_selectedIndex == 4 then
			KEP_EventCreateAndQueue("FRAMEWORK_CURRENCY_TAB_OPEN")
		end
	else
		MenuOpen(SYSTEM_MENUS[m_selectedIndex])		
	end

	MenuBringToFrontThis()
	MenuBringToFront(SYSTEM_MENUS[m_selectedIndex])
	local loc = getMenuSnapLocation()
	if m_armorOpen then
		MenuSetLocation(SYSTEM_MENUS[m_selectedIndex], loc.x + ARMOR_WIDTH, loc.y)
	else
		MenuSetLocation(SYSTEM_MENUS[m_selectedIndex], loc.x, loc.y)
	end
end

function closeSystem()
	if m_selectedIndex > 0 then
		MenuSetMinimized(SYSTEM_MENUS[m_selectedIndex], true)
		m_selectedIndex = 0
	end
end

function toggleArmorMenu()	
	if m_armorOpen then
		closeArmorMenu()
	else
		openArmorMenu()
	end
end

function openCraftingMenu()	
	toggleSystem(3)
end

function openQuestMenu()	
	toggleSystem(2)
end

function openArmorMenu()
	if not m_armorOpen and not m_tweening then
		m_tweening = true	
		MenuBringToFrontThis()

		--Determine location of menu
		local menuShift = 0
		local menuLocataion = MenuGetLocationThis()
		local menuSize = MenuGetSizeThis() 
		local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
		local screenHeight = Dialog_GetScreenHeight(gDialogHandle)		

		if (menuLocataion.x + ARMOR_WIDTH + menuSize.width) >= screenWidth then
			menuShift = menuLocataion.x - (screenWidth - ARMOR_WIDTH - menuSize.width)
			m_snapToRight = true
		end

		local tweenMenuShift = Tween(gDialogHandle)
		tweenMenuShift:move(-menuShift, 0, .10)
		tweenMenuShift:on(CALLBACK.STOP,	function() 
												--Open Armor Menu
												local loc = getMenuSnapLocation()
												MenuOpen(ARMOR_MENU)	
												MenuSetLocation(ARMOR_MENU, loc.x, loc.y)
												MenuBringToFront(ARMOR_MENU)
												m_armorOpen = true
												m_tweening = false
											end)
		tweenMenuShift:start()

		local tweenDialogResize = Tween(gDialogHandle)
		tweenDialogResize:resize(ARMOR_WIDTH, 0, .10)
		tweenDialogResize:start()

		local tweenBGResize = Tween(nil, {"imgFauxBG"})
		tweenBGResize:resize(ARMOR_WIDTH, 0, .10)
		tweenBGResize:start()

		local tweenButtonMove = Tween(nil, {"btnClose", "btnMinimize"})
		tweenButtonMove:move(ARMOR_WIDTH, 0, .10)
		tweenButtonMove:start()

		local systemShift = ARMOR_WIDTH - menuShift
		local tweenSystemMove = Tween(SYSTEM_MENUS[m_selectedIndex])
		tweenSystemMove:move(systemShift, 0, .15)
		tweenSystemMove:start()

		MenuBringToFront(SYSTEM_MENUS[m_selectedIndex])		
	end
end

function closeArmorMenu()
	if m_armorOpen and not m_tweening then
		MenuClose(ARMOR_MENU)
		m_tweening = true

		--Determine location of menu
		local menuShift = 0
		local menuLocataion = MenuGetLocationThis()
		local menuSize = MenuGetSizeThis()
		local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
		local screenHeight = Dialog_GetScreenHeight(gDialogHandle)	
		if m_snapToRight then
			if (menuLocataion.x - ARMOR_WIDTH + menuSize.width) < screenWidth then
				menuShift = menuLocataion.x - (screenWidth + ARMOR_WIDTH - menuSize.width)
			end
		end

		local tweenMenuShift = Tween(gDialogHandle)
		tweenMenuShift:move(-menuShift, 0, .10)
		tweenMenuShift:on(CALLBACK.STOP,	function()
												m_armorOpen = false
												m_tweening = false
											end)
		tweenMenuShift:start()

		local tweenDialogResize = Tween(gDialogHandle)
		tweenDialogResize:resize(-ARMOR_WIDTH, 0, .10)
		tweenDialogResize:start()

		local tweenBGResize = Tween(nil, {"imgFauxBG"})
		tweenBGResize:resize(-ARMOR_WIDTH, 0, .10)
		tweenBGResize:start()

		local tweenButtonMove = Tween(nil, {"btnClose", "btnMinimize"})
		tweenButtonMove:move(-ARMOR_WIDTH, 0, .10)
		tweenButtonMove:start()		

		local systemShift = ARMOR_WIDTH + menuShift
		local tweenSystemMove = Tween(SYSTEM_MENUS[m_selectedIndex])
		tweenSystemMove:move(-systemShift, 0, .15)
		tweenSystemMove:start()
		
		MenuBringToFront(SYSTEM_MENUS[m_selectedIndex])
	end
end

function getMenuSnapLocation()
	local loc = MenuGetLocationThis()
	loc.x = loc.x + SHOW_MENU_SYSTEM_SNAP_T0.x
	loc.y = loc.y + SHOW_MENU_SYSTEM_SNAP_T0.y
	return loc
end