--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- HUDKaneva.lua 
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\PopupMenuHelper.lua")
dofile("..\\MenuScripts\\Framework_EventHelper.lua")
dofile("..\\MenuScripts\\CAM.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")

dofile("..\\MenuScripts\\DevMode.lua")

ASCII_NUMBER_START = 47

SCREENSHOT_NONE = 0
SCREENSHOT_MINIMIZING = 1
SCREENSHOT_WAITING = 2
SCREENSHOT_TAKING = 3
SCREENSHOT_COMPLETE = 4

local m_screenshotState = SCREENSHOT_NONE
local m_screenshotPath = ""

g_iZoneHasAccessPass = 0
g_locationIsAdult = false

g_currentURL = nil

g_lastZoneChangeEventMsg = nil
local m_timer = 0 -- until show status message
local m_chatTimer = 10 --max amount of time to wait before opening KIM

gPlayerGender = ""

local m_searchURLs				= {}
local m_searchString			= ""
local m_searchResultsEnabled	= false

local m_locationWebName = ""
local m_locationRaved	= false

local m_username = ""
local m_userID = 0
local m_profilePicFilename = ""
local m_playerLevel = nil
local m_friendCount = 0;

--Camera variables 
g_worldfameLevel = 0
MIN_CAMERA_FAME_LEVEL = 3
g_cameraVisible = false
counter = 0 --Hack: keep checking to see if user is truly Level 0. All users get returned Level 0 in the beginning 

local m_devMode = false
local m_devModeTimer = 0

local m_frameworkEnabled = false
local m_buildMode = false

local FLASH_DURATION_TIME	= 10 -- seconds

local m_chatFlashing		= false
local m_chatFlashCounter	= 0

local VERTICAL_HIT_RANGE = 30
local m_hudMinimized = false
local m_minimizedSetting = false
local m_minimizedSettingChanged = false
local m_minimizeAnimation = nil
local m_minimizeMovement = nil
local m_cursorInBounds = false

local m_lightboxAlpha = 225

--new variables for ABC test with Credit/Rewards
local m_rewards = 0
local m_credits = 0
local m_levelProgress = 0
local AB_BUTTON_SIZE = 247

-- Dynamic toolbelt A/B test
local m_toolbeltEnabled = false
local MINIMIZE_INDICATORS_FILTER = 2

local m_parentURL = nil
local m_parentZoneType = nil
local m_parentZoneID = nil
local m_parentZoneCount = 0

local m_bBuildDisabled = false
--local NO_BUILD_TEMPLATE = "31"

PADDING = 12
userNameW = 193
iconEnd = 0
urlBarW = 163
iconW = 50
numIcons = 14
grid = 62

maxURL = 310--5 grid spaces
minURL = 186-- 3 grid spaces

-- When screen width can accomodate to widest length 
minCENTER = 741--12 grid spaces
maxCENTER = 868 -- 15 grid spaces 

LINK_IMG_OFFSET = 7

-- Popup menus 
g_closedLargeContents = 
{
	"edURL",
	"imgURLClosed"
}

g_MenuStruct = {}

--Dropdown Menu
g_MenuContents = 
{
	"imgBG",
	"imgProfile",
	"imgVertLine1",
	"btnMyKaneva",
	"btnAccntSettings",
	"btnAddCred",
	"imgShop",
	"imgVertLine2",
	"btnShopLink",
	"imgSettings",
	"imgVertLine3",
	"btnPlayerSettings",
	"imgTips",
	"imgVertLine4",
	"btnTip",
	"btnResources",
	"btnReportIssue"
}

g_Icons = 
{
	"btnBuild", 
	"btnEmotes",
	"btnShop",
	"btnInventory",
	"btnCamera",
	"btnChat",
	"btnFriends",
	"btnRave"

}

--Stores offsets of dropdown controls vs BG
g_IconsOffSET = {}


g_URLIcons = 
{
	"btnFavorites",
	"btnZoneInfo"
}

g_Links = 
{
	"btnMyKaneva",
	"btnAccntSettings",
	"btnAddCred",
	"btnShopLink",
	"btnResources"
}

g_cameraInsertAfter = "btnInventory"

g_t_LocationSettings = {}

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "MyPlacesEvent",				KEP.MED_PRIO )
	KEP_EventRegister( "QueryPlacesEvent",			KEP.MED_PRIO )
	KEP_EventRegister( "RefreshEvent",				KEP.MED_PRIO )
	KEP_EventRegister( "ShotEvent",					KEP.MED_PRIO )
	KEP_EventRegister( "HudMinimizeEvent",			KEP.MED_PRIO )
	KEP_EventRegister( "HideHomeIconSettingEvent",  KEP.MED_PRIO )
	KEP_EventRegister( "BuildOpenMenuEvent",		KEP.MED_PRIO )
	KEP_EventRegister( "BuildModeChangedEvent",		KEP.MED_PRIO )
	KEP_EventRegister( "UnReadChatMessagesEvent",	KEP.MED_PRIO )
	KEP_EventRegister( "RoomChatViewedEvent",		KEP.MED_PRIO )
	KEP_EventRegister( "ChatFlashEvent",			KEP.MED_PRIO )
	KEP_EventRegister( "FriendsTabEvent",			KEP.MED_PRIO )
	KEP_EventRegister( "EnterBuildModeEvent",		KEP.MED_PRIO )
	KEP_EventRegister( "FRAMEWORK_MINIMIZE_HUD",	KEP.HIGH_PRIO )
	KEP_EventRegister( "FRAMEWORK_OPEN_OVERVIEW",   KEP.HIGH_PRIO)
	
	--Parent info saved here and sent to menues needing it.. Not to be confused with server event FRAMEWORK_RETURN_PARENT_DATA
	--PARENT_INFO = client <-> client. PARENT_DATA = server <-> client
	KEP_EventRegister( "FRAMEWORK_GET_PARENT_INFO",		KEP.HIGH_PRIO ) --Parent info is saved here
	KEP_EventRegister( "FRAMEWORK_RETURN_PARENT_INFO",	KEP.HIGH_PRIO ) --Parent info is saved here
	

	-- Ankit ----- events to support SATFramework
	KEP_EventRegister( "RequestForMenuStructPoppedUpValueEvent", KEP.MED_PRIO )
	KEP_EventRegister( "RcvdMenuStructPoppedUpValueEvent", KEP.MED_PRIO )
end

 
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "RefreshEventHandler",				"RefreshEvent",						KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "frameworkMinimizeHUDHandler",		"FRAMEWORK_MINIMIZE_HUD",			KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler",			"BrowserPageReadyEvent",			KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassZoneInfoHandler",  "ResponseAccessPassZoneInfoEvent",	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "zoneNavigationEventHandler",			"ZoneNavigationEvent",				KEP.MED_PRIO )
	KEP_EventRegisterHandler( "keyChangedEventHandler",				"KeyChangedEvent",					KEP.MED_PRIO )
	KEP_EventRegisterHandler( "returnFrameworkStateHandler",		"FrameworkState",					KEP.MED_PRIO )
	KEP_EventRegisterHandler( "buildModeChangedEventHandler",		"BuildModeChangedEvent",			KEP.MED_PRIO )
	KEP_EventRegisterHandler( "zoneCustomizationDLCompleteHandler", "ZoneCustomizationDLCompleteEvent",	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BalanceChangedEventHandler",			"BalanceChangedEvent",				KEP.MED_PRIO )
	KEP_EventRegisterHandler( "unreadChatHandler",					"UnReadChatMessagesEvent",			KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "readChatHandler",					"RoomChatViewedEvent",				KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "chatFlashHandler",					"ChatFlashEvent",					KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "yesNoAnswerHandler",					"YesNoAnswerEvent",					KEP.MED_PRIO )
	KEP_EventRegisterHandler( "hideHUDSettingEventHandler",			"HideHUDSettingEvent",				KEP.MED_PRIO )
	KEP_EventRegisterHandler( "HideHomeIconSettingEventHandler",	"HideHomeIconSettingEvent",			KEP.MED_PRIO )
	KEP_EventRegisterHandler( "HUDHiddenEventHandler",				"HUDHiddenEvent",					KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ImportZoneSuccessEventHandler",		"CSZLP_ImportZoneSuccessEvent",		KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "ImportChannelSuccessEventHandler",	"CSZLP_ImportChannelSuccessEvent",	KEP.LOW_PRIO )

	KEP_EventRegisterHandler( "getParentInfoHandler",				"FRAMEWORK_GET_PARENT_INFO",		KEP.HIGH_PRIO)

	-- Ankit ----- eventHandlers to support SATFramework
	KEP_EventRegisterHandler( "RequestForMenuStructPoppedUpValueEventHandler", "RequestForMenuStructPoppedUpValueEvent", KEP.MED_PRIO )
	
	-- DRF - Added
	DevMode_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
end

function Dialog_OnExit(dialogHandle)
end

function Dialog_OnCreate(dialogHandle)

	Helper_Dialog_OnCreate( dialogHandle ) 

	Events.registerHandler("FRAMEWORK_RETURN_PARENT_DATA", parentDataHandler)
	--Set Min Size: for 1024x 768 window 	
	g_MenuStruct = PopupMenuHelper_InitPopupMenu(gHandles["btnSettings"], g_MenuContents, nil)
	PopupMenuHelper_ShowPopupMenu(g_MenuStruct, false)

	m_minimizeMovement = getMinimizeMovement()
	m_minimizeAnimation = getMinimizeAnimation()
	m_minimizedSetting = GetHideHUDSetting()

	if GetHideHomeSetting() then
		Control_SetVisible(gHandles["btnHome"], false)
		Control_SetEnabled(gHandles["btnHome"], false)
	end

	requestCurrentURL()
	getPlaceInformation()
	setUserInfo()

	gPlayerGender = KEP_GetLoginGender()

	-- Is the Framework active in this world? Let's find out
	local event = KEP_EventCreate("GetFrameworkState")
	KEP_EventQueue(event)
		
	for i,v in ipairs(g_Links) do 
		_G[v.."_OnMouseEnter"] = function(buttonHandle)
			Control_SetLocationX(gHandles["imgLink"],(Control_GetLocationX(gHandles[v])+Control_GetWidth(gHandles[v]) + LINK_IMG_OFFSET))
			Control_SetLocationY(gHandles["imgLink"],Control_GetLocationY(gHandles[v]))
			Control_SetVisible(gHandles["imgLink"], true)
		end

		_G[v .. "_OnMouseLeave"] = function(buttonHandle)
			Control_SetVisible(gHandles["imgLink"], false)
		end
	end

	m_devMode = (KEP_DevMode() == 1)
	Control_SetVisible(gHandles["stcDevModeFps"], m_devMode)
    
    -- DRF - Bug Fix
    resize()
    setCamera()
    setBuildMode()
end

function checkCursorInBounds()
	local cursor = MenuGetLocationThis()
	cursor.x = Cursor_GetLocationX() - cursor.x
	cursor.y = Cursor_GetLocationY() - cursor.y

	local inBounds = Cursor_GetLocationY() <= VERTICAL_HIT_RANGE
	inBounds = inBounds or MenuContainsPointRelThis(cursor.x, cursor.y)
	inBounds = inBounds or (g_MenuStruct.PoppedUp and PopupMenuHelper_ContainsPoint(g_MenuStruct, cursor.x, cursor.y))

	return inBounds
end

function getMinimizeAnimation()
	local animation = Tween(gDialogHandle)

	animation:on(CALLBACK.STOP, onMinimizeStop)
	animation:move(m_minimizeMovement.x, m_minimizeMovement.y, m_minimizeMovement.length)

	return animation
end

function getMinimizeMovement()
	local moveAmts = {}
	moveAmts.x = 0
	moveAmts.y = -60
	moveAmts.length = .10
	moveAmts.lengthLong = .15

	return moveAmts
end

function resize()

	local screenHeight = Dialog_GetScreenHeight (gDialogHandle)
	local screenWidth = Dialog_GetScreenWidth (gDialogHandle)
	local dialogSize = MenuGetSizeThis()
	local dialogLoc = MenuGetLocationThis()
	MenuSetLocationThis(0, 0)
	MenuSetSizeThis(screenWidth, dialogSize.height )

	--GRAY BG
	Control_SetSize(gHandles["imgDialogBackground"], screenWidth, dialogSize.height)
	Control_SetLocation(gHandles["imgDialogBackground"], 0, 0)
	
    --Set Vert Bar 
    local hudW = Control_GetWidth(gHandles["imgDialogBackground"])
    
    Control_SetLocation(gHandles["btnSettings"], hudW - 22 - iconW, Control_GetLocationY(gHandles["btnSettings"]))
    Control_SetLocation(gHandles["vertRule1"], Control_GetLocationX(gHandles["btnSettings"])-6, Control_GetLocationY(gHandles["vertRule1"]))
    iconEnd = Control_GetLocationX(gHandles["vertRule1"])
    
    local iconSpace = Control_GetLocationX(gHandles["vertRule1"]) - Control_GetLocationX(gHandles["vertRule"])

    for i,v in ipairs(g_URLIcons) do 
        Control_SetVisible(gHandles[v], true)
    end 
    for i,v in ipairs(g_Icons) do 
        Control_SetVisible(gHandles[v], true)
    end 
    Control_SetVisible(gHandles["imgUrlBackFill"],true)
    Control_SetVisible(gHandles["btnRefresh"],true)
    Control_SetVisible(gHandles["edURL"],true)	
    
    local buttonOffest = 6
    if iconSpace <= maxCENTER then 
        --Set First two icons before URL Bar
        
        Control_SetLocation(gHandles["btnHome"], userNameW + buttonOffest, Control_GetLocationY(gHandles["btnHome"]))
        
        Control_SetLocation(gHandles["btnPlaces"], Control_GetLocationX(gHandles["btnHome"]) -6 + grid, Control_GetLocationY(gHandles["btnPlaces"]))
        
        --Set Icons after URL Bar
        for i,v in ipairs(g_Icons) do 
            Control_SetLocation(gHandles[v], iconEnd - (i*grid) +6, Control_GetLocationY(gHandles[v]))
        end	
        
        --Stretch URL Bar
        local urlStartX = Control_GetLocationX(gHandles["btnPlaces"]) -6 + grid

        urlIcon = "btnFriends"
        --If btnRave is visble, url bar should use it as stopping location instead of btnFriends
        if Control_GetVisible(gHandles["btnRave"]) == 1  then
            urlIcon = "btnRave"
        end
        
        local urlSize =(Control_GetLocationX(gHandles[urlIcon])-6) - urlStartX
        
        -- 
        if urlSize > grid then 
            Control_SetLocation(gHandles["imgUrlBackFill"],urlStartX, Control_GetLocationY(gHandles["imgUrlBackFill"]) )
            Control_SetSize(gHandles["imgUrlBackFill"], urlSize, Control_GetHeight(gHandles["imgUrlBackFill"])) 
        else 
            for i,v in ipairs(g_URLIcons) do 
                Control_SetVisible(gHandles[v], false)
            end 
            Control_SetVisible(gHandles["imgUrlBackFill"], false)
            Control_SetVisible(gHandles["btnRefresh"],false)
            Control_SetVisible(gHandles["edURL"],false)
            --COMPARE HOW MUCH ICONSPACE WE HAVE VS NUMBER OF ICONS SHOULD BE SHOWN; TAKE OUT ICONS IF SMALL ENOUGH 
            for i,v in ipairs(g_Icons) do 
                if (iconSpace - (2*62)) < (i*grid) then 
                    Control_SetVisible(gHandles[v],false)
                end 
            end   
        end 
        
        --DROP OFF ICONS AS THE WINDOW GETS SMALLER
    elseif iconSpace > maxCENTER then
    --	TOO BIG
        --SET URL BAR TO MAX SIZE 
        Control_SetSize(gHandles["imgUrlBackFill"], maxURL, Control_GetHeight(gHandles["imgUrlBackFill"]))
        
        --INSERT ALL CENTER ICONS INTO g_bigIcons ARRAY
        local g_bigIcons = {}
        local int = 1 
            g_bigIcons[int] = "btnHome" 
            g_bigIcons[int+1] = "btnPlaces"
            g_bigIcons[int+2] = "imgUrlBackFill"
        
        for i,v in ipairs (g_Icons) do 
            g_bigIcons[3+i] = g_Icons[(#g_Icons -(i-1))]
        end 
        
        for i,v in ipairs(g_bigIcons) do 	
            Control_SetLocation(gHandles[v],0, Control_GetLocationY(gHandles[v]))
        end 
        
        for i,v in ipairs(g_bigIcons) do 
            local shift = screenWidth/2 - maxCENTER/2
            if shift < userNameW then 
                shift = userNameW
            end 
            if i<4 then 
                Control_SetLocation(gHandles[v],shift + Control_GetLocationX(gHandles[v]) + ((i-1)*grid) + buttonOffest, Control_GetLocationY(gHandles[v]))	
            else 
                Control_SetLocation(gHandles[v],shift +(4*grid) + Control_GetLocationX(gHandles[v]) + ((i-1)*grid) + buttonOffest, Control_GetLocationY(gHandles[v]))
            end
        end
        
    end 
    -- SET URL BUTTONS
        
    for j,k in ipairs(g_URLIcons) do 
        if j == 1 then 
            Control_SetLocation(gHandles[k], Control_GetLocationX(gHandles["imgUrlBackFill"]) + Control_GetWidth(gHandles["imgUrlBackFill"]) - (j*Control_GetWidth(gHandles[k])) - 5, Control_GetLocationY(gHandles[k]))
        else 
            Control_SetLocation(gHandles[k], Control_GetLocationX(gHandles["imgUrlBackFill"]) + Control_GetWidth(gHandles["imgUrlBackFill"]) - (1.25*j*Control_GetWidth(gHandles[k])), Control_GetLocationY(gHandles[k]))
        end
    end 
    
    Control_SetLocation(gHandles["btnRefresh"], Control_GetLocationX(gHandles["imgUrlBackFill"]),  Control_GetLocationY(gHandles["btnRefresh"]))
    Control_SetLocation(gHandles["edURL"],Control_GetLocationX(gHandles["btnRefresh"]) + Control_GetWidth(gHandles["btnRefresh"]) + 5, Control_GetLocationY(gHandles["edURL"]) )
    Control_SetSize(gHandles["edURL"], Control_GetWidth(gHandles["imgUrlBackFill"]) - 3* Control_GetWidth(gHandles["btnRefresh"]), Control_GetHeight(gHandles["edURL"])) 
    
    -- Set Dropdown menu location 
    local hudEndX = Control_GetLocationX(gHandles["imgDialogBackground"]) + Control_GetWidth(gHandles["imgDialogBackground"])
    local imgW = Control_GetWidth(gHandles["imgBG"])
    local imgH = Control_GetHeight(gHandles["imgBG"])
    for i,v in pairs(g_MenuContents) do 
        local diff = Control_GetLocationX(gHandles[v])-Control_GetLocationX(gHandles["imgBG"])
        g_IconsOffSET[i] = diff
    end
    
    for i,v in pairs(g_MenuContents) do 
        Control_SetLocation(gHandles[v], (screenWidth - imgW)+ g_IconsOffSET[i], Control_GetLocationY(gHandles[v]))
    
    end
    -- TODO: Temporary for functionality, make part of the rest
    Control_SetLocation(gHandles["imgChatHighlight"], Control_GetLocationX(gHandles["btnChat"]), Control_GetLocationY(gHandles["btnChat"]))
    Control_SetLocation(gHandles["lstURLSearch"], Control_GetLocationX(gHandles["imgUrlBackFill"]), Control_GetLocationY(gHandles["lstURLSearch"]))
    Control_SetSize(gHandles["lstURLSearch"], Control_GetWidth(gHandles["imgUrlBackFill"]), Control_GetHeight(gHandles["lstURLSearch"]))
    
    setBackgroundLocation() -- update background location in case minimized

    if m_minimizedSetting then
        local ev = KEP_EventCreate("HudMinimizeEvent")
        KEP_EventEncodeNumber(ev, 1)
        KEP_EventEncodeNumber(ev, 1)
        KEP_EventQueue( ev)
    elseif not m_hudMinimized then
        m_minimizeAnimation:stop()
        m_minimizeAnimation:move(m_minimizeMovement.x, m_minimizeMovement.y, m_minimizeMovement.length)
    end
end 

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle ) 
end


function Dialog_OnMoved(dialogHandle, x, y)
	setCamera()
	setBuildMode()
end

function buildModeChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	

	if m_bBuildDisabled then
		if m_frameworkEnabled then
			local ev = KEP_EventCreate("FRAMEWORK_SET_PLAYER_MODE")
			KEP_EventEncodeString(ev, "Player")
			KEP_EventQueue(ev) -- NOTE: Handled in Framework_PlayerHandler
		else
			local ev = KEP_EventCreate("BuildExitEvent")
			KEP_EventQueue(ev)
		end
		m_buildMode = false
	else
		m_buildMode = filter == 1
	end
end

--- HANDLERS -- 
-- Called when a player changes Framework mode or when the Framework changes states (enabled/disabled)
function returnFrameworkStateHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local frameworkState = cjson.decode(Event_DecodeString(event))
	if frameworkState.enabled ~= nil then
		m_frameworkEnabled = (frameworkState.enabled == true)
	end
end

-- Handles the "frameworkMinimizeHUD" event triggered when hud is hidden.
function frameworkMinimizeHUDHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == MINIMIZE_INDICATORS_FILTER then
		local checked = Event_DecodeNumber(event)
		local instant = Event_DecodeNumber(event)
		local dynamicEnable = Event_DecodeNumber(event)
		m_toolbeltEnabled = m_toolbeltEnabled or dynamicEnable == 1
	end
end

function hideHUDSettingEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local checked = KEP_EventDecodeNumber(event)	
	m_minimizedSetting = checked == 1

	if m_minimized ~= m_minimizedSetting then
		if m_minimizedSetting then
			m_minimizedSettingChanged = true
			m_minimizeAnimation:move(m_minimizeMovement.x, m_minimizeMovement.y, m_minimizeMovement.lengthLong, nil, nil, true)
		end
	end
end

function HUDHiddenEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local checked = Event_DecodeNumber(event)
	local instant = Event_DecodeString(event) == "true"
	local minimized = checked == 1

	m_chatFlashing = false
	Control_SetVisible(gHandles["imgChatHighlight"], false)

	openMyKaneva(false)

	if instant then
		m_minimizeAnimation:stop()
		MenuSetLocationThis(0, 0)	
		m_minimizeAnimation:move(m_minimizeMovement.x, m_minimizeMovement.y, m_minimizeMovement.length)

		MenuSetLocationThis(0, (minimized and m_minimizeMovement.y or 0))	

	elseif m_hudMinimized ~= minimized then
		m_minimizeAnimation:reverse(not minimized)
		m_minimizeAnimation:start()
	end

	m_hudMinimized = minimized

	if m_hudMinimized and m_screenshotState == SCREENSHOT_MINIMIZING then
		m_screenshotState = SCREENSHOT_WAITING
		-- We need to wait one frame because minimize doesn't take into effect until next frame
		-- To Dialog_OnRender!
	end
end

function HideHomeIconSettingEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local checked = KEP_EventDecodeNumber(event)	
	local homeVisible = checked ~= 1

	Control_SetVisible(gHandles["btnHome"], homeVisible)
	Control_SetEnabled(gHandles["btnHome"], homeVisible)
end

function onMinimizeStop()
	if m_minimizedSettingChanged then
		m_minimizeAnimation:move(m_minimizeMovement.x, m_minimizeMovement.y, m_minimizeMovement.length, nil, nil, true)
	end

	m_minimizedSettingChanged = false
	setBackgroundLocation()
end

function setBackgroundLocation()
	Control_SetLocation(gHandles["imgDialogBackground"], 0, 0)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.PLACE_INFO then
		local xmlData = KEP_EventDecodeString(event)
		if (not ParseTagReturnCodeOk(xmlData)) then return end

		m_locationWebName	= ParseTagStr(xmlData, "name")
		m_locationRaved		= (ParseTagNum(xmlData, "player_rave_count") > 0)
		updateRaveButton()

		-- For camera
		g_locationIsAdult 	= (ParseTagStr(xmlData, "is_adult") == "Y")

		return KEP.EPR_OK

	elseif filter == WF.LOC_RAVE then
		local txtParse = KEP_EventDecodeString(event)
		local returnCode = ParseTagStr(txtParse, "ReturnCode")

		if returnCode == "0" then
			KEP_MessageBox("You raved <br>" .. m_locationWebName)
			getPlaceInformation()
		elseif returnCode == "-2" then
			KEP_MessageBox("You cannot rave this area")
		elseif returnCode == "-3" then
			KEP_MessageBox("You have already raved this location")
		end

		return KEP.EPR_OK

	elseif filter == WF.HUDPROFILE_INFO then
		local estr = KEP_EventDecodeString( event )
		parseUserInfo(estr)
		return KEP.EPR_OK

	elseif filter == WF.TRY_ON_ITEM then
		parseTryOnItemResult( event )
		return KEP.EPR_CONSUMED
	
	elseif filter == WF.PROFILE_COUNTS_SUFFIX then
		m_friendCount = ParseTagNum(xmlData, "FriendCount")
		return KEP.EPR_OK

	elseif filter == WF.MYFAME_GETBALANCE then
		local returnData = KEP_EventDecodeString( event )
		m_credits, m_rewards = ParseBalances(returnData)
	elseif filter == WF.GEM_CONVERT then
		local returnData = KEP_EventDecodeString(event)
		m_rewards = ParseTagStr(returnData, "UserRewardBalance")
	elseif filter == WF.LINKED_ZONES then
		local returnData = KEP_EventDecodeString(event)
		ParseParentData(returnData)
	   	return KEP.EPR_OK
	end
end

function unreadChatHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if m_chatFlashing == false and not m_hudMinimized then
		m_chatFlashing = true
		m_chatFlashCounter = 0
		local ev = KEP_EventCreate( "ChatFlashEvent")
		KEP_EventQueueInFuture( ev, 0)
	end
end

function readChatHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	m_chatFlashing = false
	Control_SetVisible(gHandles["imgChatHighlight"], false)
end

function chatFlashHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if m_chatFlashing == true and not m_hudMinimized then
		Control_SetVisible(gHandles["imgChatHighlight"], m_chatFlashCounter % 2 == 0)

		if m_chatFlashCounter >= FLASH_DURATION_TIME then
			Control_SetVisible(gHandles["imgChatHighlight"], true)
		else
			m_chatFlashCounter = m_chatFlashCounter + 1
			local ev = KEP_EventCreate( "ChatFlashEvent")
			KEP_EventQueueInFuture( ev, 1)
		end
	end
end

function parseUserInfo( txtParse )
	m_userID = ParseTagNum(txtParse, "user_id")
	dbPath = ParseTagStr(txtParse, "thumbnail_square_path", DEFAULT_AVATAR_PIC_PATH, true) -- silent
	local filename = KEP_DownloadTextureAsync( GameGlobals.FRIEND_PICTURE, m_userID, dbPath, GameGlobals.FULLSIZE )
	m_profilePicFilename = string.gsub(filename,".-\\", "" )
	m_profilePicFilename = "../../CustomTexture/".. m_profilePicFilename
	scaleImage(gHandles["imgObject_Pic"], gHandles["imgObject_BG"], m_profilePicFilename)
	requestCreditsRewards()
end

function responseAccessPassZoneInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iZoneHasAccessPass = KEP_EventDecodeNumber(event)
	return KEP.EPR_OK
end

function RefreshEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	gotoSTP(g_currentURL)
end


--- BUTTON HANDLERS ---
function btnMyFame_OnButtonClicked(buttonHandle)
	MenuToggleOpenClose("MyFame.xml")
end

function btnHome_OnButtonClicked(buttonHandle)
	if checkUnsubmittedImports() then
		return
	end

	-- Home Button Options A/B Test
	gotoURL(GetHomeLink())
end 

function btnPlaces_OnButtonClicked( buttonHandle )
	if checkUnsubmittedImports() then
		return
	end

	if MenuIsOpen("BrowsePlaces") then return end

    MenuOpen("BrowsePlaces.xml")
    ev = KEP_EventCreate( "QueryPlacesEvent")
    KEP_EventEncodeNumber(ev, PlaceCategoryIds.APPS)
    KEP_EventEncodeNumber(ev, -1)
    KEP_EventQueue(ev)
end

function btnZoneInfo_OnButtonClicked(buttonHandle)
	MenuToggleOpenClose("RClickPlaces.xml")
end

function btnFavorites_OnButtonClicked(buttonHandle)
	if checkUnsubmittedImports() then
		return
	end
	
	MenuToggleOpenClose("MyPlaces2")

	local ev = KEP_EventCreate("MyPlacesEvent")
	KEP_EventEncodeNumber(ev, 1)
	KEP_EventQueue(ev)
end

function btnRave_OnButtonClicked( buttonHandle )
	if m_locationRaved then 
		updateRaveButton()
		KEP_MessageBox("You have already raved this location.") 
	else
		makeWebCall(GameGlobals.WEB_SITE_PREFIX..SET_RAVES_SUFFIX.."&raveType=single&currency=FREE", WF.LOC_RAVE)
	end
end

function btnFriends_OnButtonClicked( buttonHandle )
	MenuToggleOpenClose("FriendsPeople.xml")
	local ev = KEP_EventCreate("FriendsTabEvent")
	KEP_EventQueue(ev)
end


function btnChat_OnMouseEnter(buttonHandle)
	Control_SetVisible(gHandles["imgChatHighlight"], false)
end

function btnChat_OnButtonClicked( buttonHandle )
	if MenuIsClosed("ChatMenu.xml") then
--		KEP_PopUpRoomChat()
	else
		local sendEvent = KEP_EventCreate("SendPrivateMessageEvent")
		KEP_EventEncodeString(sendEvent, "No Player Name")
		KEP_EventQueue( sendEvent )
	end
end

function btnCamera_OnButtonClicked( buttonHandle )
	if g_locationIsAdult or g_iZoneHasAccessPass == 1 then 
		KEP_MessageBox("Pictures cannot be taken in Adult Pass Worlds.")
	else
	

		-- Hide hud so it's not in view in screenshot
		local ev = KEP_EventCreate("HudMinimizeEvent")
		KEP_EventEncodeNumber(ev, 1)
		KEP_EventEncodeNumber(ev, 1)
		KEP_EventQueue(ev)

		m_screenshotState = SCREENSHOT_MINIMIZING

		-- To HUDHiddenEventHandler to avoid race condition with minimize not finishing before screenshot
	end 
end

function btnInventory_OnButtonClicked( buttonHandle )

	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")
	end

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "clothing")
	KEP_EventQueue(ev)
end

function btnShop_OnButtonClicked( buttonHandle )
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_SHOP )
end

function btnEmotes_OnButtonClicked( buttonHandle )
	MenuToggleOpenClose("Emotes.xml")
end

function btnBuild_OnButtonClicked( buttonHandle )

	local ev = KEP_EventCreate( "EnterBuildModeEvent")
	KEP_EventSetFilter(ev, 1)
	KEP_EventQueue( ev)
		
	--if in Unified Build experiement and in game world
	if m_frameworkEnabled then
	--in Player or God? 
		if MenuIsOpen("Framework_Toolbar.xml") then 
			KEP_ClearSelection() 
			local ev = KEP_EventCreate("FRAMEWORK_SET_PLAYER_MODE")
			KEP_EventEncodeString(ev, "God")
			KEP_EventQueue(ev) -- NOTE: Handled in Framework_PlayerHandler
		end 			
	end 
end

function btnRefresh_OnButtonClicked(buttonHandle)
	if checkUnsubmittedImports() then
		return
	end

	if m_frameworkEnabled then
		Framework.sendEvent("FRAMEWORK_RESPAWN_REQUEST", {})
	else
		gotoSTP(g_currentURL)
	end
end

function btnSettings_OnButtonClicked( buttonHandle )
	openMyKaneva(not g_MenuStruct.PoppedUp)
end

function btnMyKaneva_OnRadioButtonChanged(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA..MY_KANEVA_SUFFIX)
	openMyKaneva(false)
end 

function btnAccntSettings_OnRadioButtonChanged(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA.."mykaneva/general.aspx")
	openMyKaneva(false)
end 

function btnAddCred_OnRadioButtonChanged(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA.."mykaneva/buyCreditPackages.aspx")
	openMyKaneva(false)
end 

function btnShopLink_OnRadioButtonChanged(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_SHOP )
	openMyKaneva(false)
end

---Button control functions
function btnPlayerSettings_OnRadioButtonChanged(buttonHandle)
	MenuOpen("Settings.xml")
	local ev = KEP_EventCreate("SETTINGS_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "Basic")
	
	KEP_EventQueue(ev)	
	openMyKaneva(false)
end

function btnTip_OnRadioButtonChanged(buttonHandle)
	MenuOpen("FameTips.xml")
	openMyKaneva(false)
end

function btnResources_OnRadioButtonChanged( buttonHandle )
	KEP_LaunchBrowser("http://docs.kaneva.com/mediawiki/index.php/Main_Page")
	openMyKaneva(false)
end

function btnReportIssue_OnRadioButtonChanged(buttonHandle)
	MenuOpen("ReportAnIssue.xml")
	openMyKaneva(false)
end

--- WEBCALLS & REQUESTS --- 
function getPlaceInformation()
	makeWebCall(GameGlobals.WEB_SITE_PREFIX..GET_PLACE_INFORMATION_SUFFIX, WF.PLACE_INFO)
end

function requestCurrentURL()
	local ev = KEP_EventCreate( "ZoneNavigationEvent")
	KEP_EventSetFilter(ev, ZONE_NAV.CURRENT_URL_REQUEST)
	KEP_EventQueue( ev)
end

function updateRaveButton()
	Control_SetEnabled(gHandles["btnRave"], not m_locationRaved)
end

function ravePlace()
	if g_t_LocationSettings["hasRaved"] then 
		KEP_MessageBox("You have already raved this location.") 
	else
		WebCall(GameGlobals.WEB_SITE_PREFIX..SET_RAVES_SUFFIX.."&raveType=single&currency=FREE", WF.LOC_RAVE, 0)
	end
end

function checkUnsubmittedImports()
	local doCount = KEP_GetDynamicObjectCount()
	local unsubmittedUGC = false
	for i = 0, (doCount - 1) do
		local dynObj = {}
		local placementId = KEP_GetDynamicObjectPlacementIdByPos(i)
		local name, description, canPlayMovie, isAttachableObject, isInteractive, isLocal, isDerivable, assetId, friendId, customTexture, globalId, invType, textureUrl, gameItemId, playerId = KEP_DynamicObjectGetInfo(placementId)
		if tonumber(isLocal) == 1 and globalId < 0 then
            Log("checkUnsubmittedImports: Unsubmitted Object - placementId="..placementId.." name="..name.." glid="..globalId)
			unsubmittedUGC = true
		end
    end
	if unsubmittedUGC then
		MenuOpenModal("UnsubmittedUGC.xml")
		return true		
	else
		return false
	end
end

--------------------------------------------------------------------------------------------------
-- URL Bar
--------------------------------------------------------------------------------------------------
function zoneNavigationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ZONE_NAV.CURRENT_URL_INFO then
		local url = KEP_EventDecodeString( event )
		g_currentURL = url
		setURLString(url, true)

		local zoneType = parseZoneTypeFromURL(url)	

		-- if zoneType == "home" then
		-- 	local zoneInstanceID = KEP_GetZoneInstanceId() 
		-- 	local zoneType = KEP_GetZoneIndexType()
		-- 	local webCall = GameGlobals.WEB_SITE_PREFIX .. GET_WORLD_TEMPLATE_ID .. "zoneInstanceId="..tostring(zoneInstanceID) .. "&zoneType="..tostring(zoneType)
		-- 	makeWebCall(webCall, WF.GET_ZONE_TEMPLATE)
		-- end

		local startIndex = GetStartingOption()
		if tonumber(startIndex) == 2 then
			SetStartingLink(url)
		end

	elseif filter == ZONE_NAV.SEARCH_RESPONSE then
		m_searchString = KEP_EventDecodeString(event)
		m_searchURLs = readFavoritesIntoTable(event)
		displaySearchResults(searchURLs)
	end
end

function edURL_OnFocusIn(editBoxHandle)
	enableURLEdit(true)
end

function edURL_OnFocusOut(editBoxHandle)
	enableURLEdit(false)
end

function enableURLEdit(enable)
	setURLString(g_currentURL, not enable)
end

function setURLString(url, userFriendly)
	if userFriendly then
		url = parseFriendlyNameFromURL(url)
	end
	EditBox_SetText(gHandles["edURL"], url, false)
end

function edURL_OnEditBoxString(editBoxHandle, text)
	if (text == "?") then 
		local str = "kaneva://"..KEP_GetGameId()
		setURLString(str, false)
		return
	end

    gotoSTP(text)
end

function edURL_OnEditBoxChange(editBoxHandle, text)
	requestURLSearch(text, 8)
end

function gotoSTP(url, import)
	if checkUnsubmittedImports() then
		return
	end

    -- Assume Rezone If No Url Is Given
	if (url == "") then
		url = g_currentURL
    end

	setURLString(url, false)

	gotoURL(url, import)

end

function requestURLSearch(searchString, numURLs)
	if searchString == "" or searchString == nil then
		m_searchURLs = {}
		enableSearchResults(false)
	else
		local ev = KEP_EventCreate( "ZoneNavigationEvent")
		KEP_EventSetFilter(ev, ZONE_NAV.SEARCH_REQUEST )
		KEP_EventEncodeString(ev, searchString)
		KEP_EventEncodeNumber(ev, numURLs)
		KEP_EventQueue( ev)
	end
end

function displaySearchResults(searchURLs)
	if #m_searchURLs > 0 then
		enableSearchResults(true)

		local list = gHandles["lstURLSearch"]
		ListBox_RemoveAllItems(list)
		ListBox_ClearSelection(list)
		for i,v in pairs(m_searchURLs) do
			ListBox_AddItem(list, parseFriendlyNameFromURL(v.url), i)
		end

		-- Number of items times the height of the rows, plus the border on top and bottom
		local height = (#m_searchURLs * ListBox_GetRowHeight(list)) + (ListBox_GetBorder(list) * 2)
		Control_SetSize(list, Control_GetWidth(list), height)
	else
		enableSearchResults(false)
	end
end

function enableSearchResults(enable)
	m_searchResultsEnabled = enable
	Control_SetVisible(gHandles["lstURLSearch"], enable)
end

function ListBoxMain_OnFocusOut(listBoxHandle)
	enableSearchResults(false)
end

function lstURLSearch_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	local data = ListBox_GetSelectedItemData(listBoxHandle)
	local zoneURL = m_searchURLs[data]
	if zoneURL ~= nil then
		setURLString(zoneURL.url, false)
	end
end

function lstURLSearch_OnListBoxItemDblClick(listBoxHandle, selectedIndex, selectedText)
	local data = ListBox_GetSelectedItemData(listBoxHandle)
	gotoSearchItem(data)
end

-- When typing in the URL bar, if you press 'down' and there are search results move into the box
-- When search focused use enter to travel to, and esc to close
-- Have to use this because key down doesn't register inside edit box
function keyChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	isKeyDown = KEP_EventDecodeNumber(event)
	if isKeyDown == 1 then
		local searchListBox = gHandles["lstURLSearch"]

		if filter == Keyboard.DOWN then
			local edURL = gHandles["edURL"]
			if Control_GetFocus(edURL) == 1 and Control_GetVisible(searchListBox) == 1 and #m_searchURLs > 0 then
				ListBox_SelectItem(searchListBox, 0)
				Control_SetFocus(searchListBox, true)
			end

		elseif Control_GetVisible(searchListBox) == 1 and Control_GetFocus(searchListBox) == 1 then
			if filter == Keyboard.ENTER then
				local data = ListBox_GetSelectedItemData(searchListBox)
				gotoSearchItem(data)

			elseif filter == Keyboard.ESC then
				enableSearchResults(false)
			end
		end
	end
end

function gotoSearchItem(index)
	local zoneURL = m_searchURLs[index]
	if zoneURL ~= nil then
		gotoSTP(zoneURL.url)
	end
end

function setUserInfo()
	m_username = KEP_GetLoginName() or ""
	Static_SetText(gHandles["stcUserName"], m_username)
	
	if m_playerLevel then
		Static_SetText(gHandles["stcKanevaLevel"], "Kaneva Level " .. tostring(m_playerLevel))
	end

	-- Get profile info for picture
	local url = GameGlobals.WEB_SITE_PREFIX .. USERID_SUFFIX .. m_username
	makeWebCall(url, WF.HUDPROFILE_INFO)
end

function setCamera()
	-- Set Camera Button
	if (m_playerLevel~= nil and m_playerLevel~= 0) or counter > 3 then
		g_cameraVisible = (m_playerLevel >= MIN_CAMERA_FAME_LEVEL)

		for i,v in ipairs(g_Icons) do
			if g_cameraVisible == false then
				if v == "btnCamera" then
					g_cameraInsertAfter = g_Icons[i - 1]
					table.remove(g_Icons, i)
					break
				end

			else
				if v == g_cameraInsertAfter and g_Icons[i+1] ~= "btnCamera" then
					table.insert(g_Icons, i+1, "btnCamera")
					break
				end
			end
		end
		
		Control_SetVisible(gHandles["btnCamera"], g_cameraVisible)
	end
	if m_hudMinimized == true then
			Control_SetVisible(gHandles["btnCamera"], false)
	end
	resize()
	if m_playerLevel == 0 and counter <= 3 then 
		counter = counter + 1
	end 
end

function setBuildMode()
		
	for i,v in ipairs(g_Icons) do
		if not KEP_IsOwnerOrModerator() then 
			if v == "btnBuild"then
                Log("BUILD DISABLED - NOT OWNER OR MODERATOR")
				table.remove(g_Icons, i)
				break
			end 
		else 
			if v == "btnRave"then
				table.remove(g_Icons, i)
				break
			end 

			if m_bBuildDisabled then
                Log("BUILD DISABLED - OWNER OR MODERATOR")
				if v == "btnBuild"then
					table.remove(g_Icons, i)
					break
				end
			end
		end 			
	end

	Control_SetVisible(gHandles["btnBuild"], (KEP_IsOwnerOrModerator() and not (m_bBuildDisabled == true)))
	Control_SetVisible(gHandles["btnRave"], not KEP_IsOwnerOrModerator())

	resize()
end 

function Dialog_OnRender(dialogHandle, fTimeElapsed)
	MenuAnimation_OnRender(fTimeElapsed)

	-- If the popup menu is popped up and we click outside of it, close it
	-- NOTE: THIS IS A HACK! Sorry :(
	-- We currently have no event when a mouse is clicked anywhere - Dialog_OnLButtonDown does not fire over other menus
	-- If we gain said event move this to there
	if Dialog_IsLMouseDown(gDialogHandle) ~= 0 then
		-- Convert cursor location to local space
		local cursor = MenuGetLocationThis()
		cursor.x = Cursor_GetLocationX() - cursor.x
		cursor.y = Cursor_GetLocationY() - cursor.y

		if g_MenuStruct.PoppedUp and not PopupMenuHelper_ContainsPoint(g_MenuStruct, cursor.x, cursor.y) then
			openMyKaneva(false)
		end
	
		if not m_hudMinimized and m_minimizedSetting and not checkCursorInBounds() then
			local ev = KEP_EventCreate("HudMinimizeEvent")
			KEP_EventEncodeNumber(ev, 1)
			KEP_EventQueue( ev)
		end

		-- Clear focus if editing url and click outside
		if (Control_GetFocus(gHandles["edURL"]) == 1) or (Control_GetFocus(gHandles["lstURLSearch"]) == 1) then
			if (Control_ContainsPoint(gHandles["edURL"], cursor.x, cursor.y) == 0) 
			and (Control_ContainsPoint(gHandles["lstURLSearch"], cursor.x, cursor.y) == 0) then
				enableSearchResults(false)
				MenuClearFocusThis()
			end
		end

		-- Clear focus if clicked outside button Build
		if (Control_GetFocus(gHandles["btnBuild"]) == 1) then
			if (Control_ContainsPoint(gHandles["btnBuild"], cursor.x, cursor.y) == 0) then
				MenuClearFocusThis()
			end
		end
	end

	-- Dev Mode FPS Display
	if m_devMode then
		m_devModeTimer = m_devModeTimer + fTimeElapsed
		if (m_devModeTimer > 1) then
			m_devModeTimer = 0
			local fpsRender = math.floor(KEP_MetricsFpsRender() + 0.5)
			local worldIsAP = KEP_WorldIsAP()
			if (worldIsAP == 1) then 
				Static_SetText(gHandles["stcDevModeFps"], "AP "..fpsRender)
			else
				Static_SetText(gHandles["stcDevModeFps"], fpsRender)
			end
		end
	end

	if m_parentURL == nil then
		m_chatTimer = m_chatTimer - fTimeElapsed
		if m_chatTimer <= 0 then
			m_parentURL = g_currentURL
		end
	end

	local playerInfo = { KEP_GetPlayerInfo() }

	if playerInfo[10+1]~=nil and playerInfo[10+1]~= "_____" and g_currentURL~=nil and m_parentURL ~= nil then	
		local zoneIndex = KEP_GetCurrentZoneIndex( )
    	local zoneType = InstanceId_GetType( zoneIndex )
		local instanceId = KEP_GetZoneInstanceId()
    		
 		-- Use 0 for non-instanced zones, homes and hangouts as well as first instance of any other zones
    		if instanceId==1 or zoneType==KEP.CI_NOT_INSTANCED or zoneType==KEP.CI_HOUSING or zoneType==KEP.CI_CHANNEL then
    		    instanceId = 0
    		end
    		
    	--TODO: For linked world, change the first g_currentURL to parent's URL so all linked worlds share a chat
		local eventMsg = "-room" .. playerInfo[10+1] .. " " .. string.gsub(m_parentURL, " ", "%%20") .. "?" .. tostring(instanceId) .. " " .. parseFriendlyNameFromURL(g_currentURL)
		if eventMsg~=g_lastZoneChangeEventMsg then
			g_lastZoneChangeEventMsg = eventMsg
		end
	end

	-- "Press tab to interact"
	m_timer = m_timer - fTimeElapsed
	if m_timer <= 0 then
		if playerInfo ~= 0 then
			-- index 14 = helperString
			local interactString = playerInfo[14+1] or ""
			Static_SetText(gHandles["stcInteract"], interactString)
			Control_SetVisible(gHandles["stcInteract"], interactString ~= "")
		end

		m_timer = 1
	end
	
	-- Have to make sure we wait a frame before taking a screenshot for SetMinimized to take effect
	if m_screenshotState == SCREENSHOT_WAITING then
		m_screenshotState = SCREENSHOT_TAKING

	elseif m_screenshotState == SCREENSHOT_TAKING then
		m_screenshotState = SCREENSHOT_COMPLETE

		-- Take screenshot before opening menu so it's not visible, but only send after opening
		-- Ankit ----- replacing single call to SaveScreenshot with two calls, one for screen capture request and other to get the path name
		m_screenshotPath = KEP_ScreenshotCapture()

		-- Reopen hud now that we know we have the screenshot
		local ev = KEP_EventCreate("HudMinimizeEvent")
		KEP_EventEncodeNumber(ev, 0)
		KEP_EventEncodeNumber(ev, 1)
		KEP_EventQueue(ev)

	elseif m_screenshotState == SCREENSHOT_COMPLETE then
		m_screenshotState = SCREENSHOT_NONE

		MenuOpenModal("Camera.xml")

		ev = KEP_EventCreate("ShotEvent")
		KEP_EventEncodeString(ev, m_screenshotPath)
		KEP_EventQueue(ev)

	elseif m_minimizedSetting and not MenuIsOpen("Framework_Map.xml") then

		if m_buildMode and (Dialog_IsLMouseDown(gDialogHandle) == 1 or Dialog_IsRMouseDown(gDialogHandle) == 1 ) then
			return
		end
		local isCursorInBounds = checkCursorInBounds()
		if isCursorInBounds ~= m_cursorInBounds then

			if isCursorInBounds then
				local ev = KEP_EventCreate("HudMinimizeEvent")
				KEP_EventEncodeNumber(ev, 0)
				KEP_EventQueue(ev)
			else
				local ev = KEP_EventCreate("HudMinimizeEvent")
				KEP_EventEncodeNumber(ev, 1)
				KEP_EventQueue(ev)
			end

			m_cursorInBounds = isCursorInBounds
		end
	end
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown, bCtrlDown, bAltDown)

	-- REPL console activation with Ctrl-~
	if DevMode()>0 and key==192 and bCtrlDown then
		-- Activate REPL console
		local BAD_EVENT_ID = 0xffff
		if KEP_EventGetId("REPLEnableConsoleEvent") == BAD_EVENT_ID then
			Log("REPL blade not loaded")
		else
			local e = KEP_EventCreate("REPLEnableConsoleEvent")
			KEP_EventQueue(e)
		end
		return
	end

	-- Emote hotkeys
	local offsetKey = key - ASCII_NUMBER_START
	if offsetKey >= 1 and offsetKey <= 10 then
		-- if you're in a game world or in build mode, you have to be using the shift key to emote
		if (m_buildMode or (m_frameworkEnabled and m_toolbeltEnabled)) then
			-- ED-2630 - A/B test for hiding toolbelt numbers when the emote menu is enabled
			if (not bShiftDown and MenuIsClosed("Emotes")) or
			   (bShiftDown and MenuIsOpen("Emotes")) or
			   (not bShiftDown) then
				return
			end
		end

		local ev = KEP_EventCreate( "PlayerEmoteEvent")
		KEP_EventEncodeNumber(ev, offsetKey)
		KEP_EventQueue( ev)
	end

	-- DRF - Added
	DevMode_OnKeyDown(dialogHandle, key, bShiftDown, bCtrlDown, bAltDown)
end

function openMyKaneva(open)
	MenuSendToBack("Framework_TopRightHud")
	PopupMenuHelper_ShowPopupMenu(g_MenuStruct, open)
end

function parseTryOnItemResult( event )
	local xmlData = KEP_EventDecodeString( event )
	local httpStatusCode = KEP_EventDecodeNumber( event )
	
	if httpStatusCode==200 and xmlData~=nil and xmlData~="" then
	
		xmlData = stripTagsXML( xmlData )
		
		local result = nil  -- element value
		local s, e = 0      -- start and end of captured string
		
		s, e, result = string.find( xmlData, "<ReturnCode>(%d+)</ReturnCode>")
		local nResult = -1
		
		if result~=nil and tonumber(result)~=nil then
		      nResult = tonumber(result)
		end
		
		if nResult ~= 0 then
			LogWarn( "ugcTryOnEventHandler: result = " .. nResult )
			KEP_MessageBox( "Try on failed: bad parameter." )
			return KEP.EPR_CONSUMED
		end
		
		-- gfind renamed gmatch in future versions of LUA
		for block_text in string.gmatch( xmlData, "<Item_x0020_Info>(.-)</Item_x0020_Info>") do
			local t_item = {}
			
			s, e, result = string.find(block_text, "<global_id>(%d+)</global_id>")
			if result ~= nil then
			     t_item["GLID"] = tonumber(result)
			end
			
			s, e, result = string.find(block_text, "<name>(.+)</name>")
			if result ~= nil then
			     t_item["name"] = tostring(result)
			end
			
			s, e, result = string.find(block_text, "<use_type>(%d+)</use_type>")
			if result ~= nil then
			     t_item["use_type"] = tonumber(result)
			end

			-- some number of item passes may be given for this item
			local itemPassList = {}
			for pass_block_text in string.gmatch( block_text, "<Pass>(.-)</Pass>") do
				s, e, result = string.find(pass_block_text, "<ID>(%d+)</ID>")
				if result ~= nil then
				     table.insert(itemPassList, tonumber(result))
				end
			end
			t_item["pass_list"] = itemPassList
			
			s, e, result = string.find(block_text, "<inventory_type>(%d+)</inventory_type>")
			if result ~= nil then
			     t_item["type"] = tonumber(result)
			end
			
			s, e, result = string.find(block_text, "<description>(.+)</description>")
			if result ~= nil then
			     t_item["description"] = tostring(result)
			end
			
			s, e, result = string.find(block_text, "<actor_group>(.+)</actor_group>")
			if result ~= nil then
			     t_item["actor_group"] = tonumber(result)
			end
			
			s, e, result = string.find(block_text, "<use_value>(.+)</use_value>")
			if result ~= nil then
			     t_item["use_value"] = tonumber(result)
			end
			
			-- use first item only (should only return one)
			if tryOnUGCItem( t_item )==false then
				KEP_MessageBox( "Try on failed: bad parameter." )
			end
			return KEP.EPR_CONSUMED
		end
	else
		LogWarn( "ugcTryOnEventHandler: " .. httpStatusCode )
	end
	
	return KEP.EPR_CONSUMED
end

function tryOnUGCItem( item )

	if item.use_type==USE_TYPE.ANIMATION then      -- animation
		TryUGCAnimation( item )
	else
		LogWarn( "tryOnUGCItem: unknown use type " .. item.use_type )
		return false
	end
	
	-- schedule a removal event locally
	local event = KEP_EventCreate( "TryOnRemoveEvent" )
	KEP_EventEncodeNumber( event, 0 )  -- todo: player id
	KEP_EventEncodeNumber( event, item.GLID )
	KEP_EventEncodeNumber( event, 0 )  -- todo: base GLID
	KEP_EventEncodeNumber( event, item.use_type )
	KEP_EventEncodeNumber( event, 0 )  -- placement ID -- not used now
	KEP_EventQueueInFuture( event, 300 ) -- remove in 300 seconds ( hard-coded for now )
end

--------------------------------------------------------------------------------------------------
-- Import Zone
--------------------------------------------------------------------------------------------------
function ImportZoneSuccessEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- unpack event from CSZoneLiteProxy
	local zoneIndex    = objectid
	local newZoneIndex = KEP_EventDecodeNumber(event)

	-- if we are standing in the zone that completed import consume the event
	local currentZoneIndex = InstanceId_GetId(KEP_GetCurrentZoneIndex())
	if (currentZoneIndex == zoneIndex) then
		Log("ImportZoneSuccessEventHandler: zoneIndex["..tostring(zoneIndex).."] -- newZoneIndex["..tostring(newZoneIndex).."] -- consuming event")
		KEP_YesNoBox(HUD.REZONE_MSG, "Confirm", HUD.REZONE_FILTER)
		return KEP.EPR_CONSUMED
	else
		Log("ImportZoneSuccessEventHandler: zoneIndex["..tostring(zoneIndex).."] -- newZoneIndex["..tostring(newZoneIndex).."] event ignored/not applicable CurrentZoneIndex["..tostring(currentZoneIndex).."]")
		return KEP.EPR_OK
	end

end

function ImportChannelSuccessEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- unpack event from CSZoneLiteProxy
	local zoneIndex = objectid
	local newZoneIndex = KEP_EventDecodeNumber(event)

	-- if we are standing in the zone that completed import consume the event
	local currentZoneIndex = KEP_GetCurrentZoneIndex()

	if (currentZoneIndex == zoneIndex) then
		Log("ImportChannelSuccessEventHandler: zoneIndex["..tostring(zoneIndex).."] -- newZoneIndex["..tostring(newZoneIndex).."] -- consuming event")
		KEP_YesNoBox(HUD.REZONE_MSG, "Confirm", HUD.REZONE_FILTER)
		return KEP.EPR_CONSUMED
	else
		Log("ImportChannelSuccessEventHandler: zoneIndex["..tostring(zoneIndex).."] -- newZoneIndex["..tostring(newZoneIndex).."] event ignored/not applicable CurrentZoneIndex["..tostring(currentZoneIndex).."]")
		return KEP.EPR_OK
	end

end

function yesNoAnswerHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter == HUD.REZONE_FILTER) then
		local answer = tonumber(KEP_EventDecodeNumber(event))
		if answer == 1 then
			-- append instance id if arena
			local e = string.match(g_currentURL, "%.arena")
			if (e == string.len(g_currentURL)) then
				g_currentURL = string.gsub(g_currentURL, "%.arena", ".arena.1")
			end

			-- DRF - Close All Menus (rezone is restricted while framework menus are open)
			MenuCloseAllExceptThis()

			-- Rezone To Refresh Newly Imported Zone
			gotoSTP(g_currentURL, true)
		end
	end

end

-- Confirms invite accepted and traveled to zone ??
function zoneCustomizationDLCompleteHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

    local urlParams = KEP_GetUrlParameters()
	if urlParams ~= nil then
		-- parse out the tracking id. will either be book-ended by another param (&) or the end of the string ($)
		local trackingId = string.match(urlParams, "RTSID=(.+)&")
		if trackingId == nil then
			trackingId = string.match(urlParams, "RTSID=(.+)$")
		end

		if trackingId ~= nil then
			local replacementUrlParams = string.gsub(urlParams,"RTSID=" .. trackingId,"")
			-- Take out the trackId from the urlParams
			if replacementUrlParams ~= nil then

				-- step past a potential "&"
				replacementUrlParams = string.sub(replacementUrlParams,s+1)
				KEP_SetUrlParameters(replacementUrlParams)

				-- record that we actually made it in the game
				local web_address = GameGlobals.WEB_SITE_PREFIX..TRACKING_REQUEST_ACCEPT_SUFFIX..trackingId.."&ingame=true"
				makeWebCall( web_address, WF.TRACKING_REQUEST_GAME_ACCEPT, 1, 1)
			else
				KEP_SetUrlParameters("")
			end
		end
	end
end

function BalanceChangedEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	m_credits = KEP_EventDecodeNumber(event)
	m_rewards = KEP_EventDecodeNumber(event)
end

function applyTextureToElement(element, textureName, x, y, x2, y2, color)
	Element_AddTexture(element, gDialogHandle, textureName)
	Element_SetCoords(element, x, y, x2, y2)
	local blendColor = Element_GetTextureColor(element)
end

-- Ankit ----- event handler to return the value of g_MenuStruct.PoppedUp back to SATFramework
function RequestForMenuStructPoppedUpValueEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local ev = KEP_EventCreate( "RcvdMenuStructPoppedUpValueEvent" )
	KEP_EventEncodeNumber( ev, g_MenuStruct.PoppedUp and 1 or 0 )
	KEP_EventQueue( ev )
end

----------------------------------------------------------
--New functions for ABC test with rewards and credits ----
----------------------------------------------------------
function requestCreditsRewards()

	if(m_userID > 0) then
		getBalances(m_userID, WF.MYFAME_GETBALANCE)
	end
end

function displayCreditsAndRewards(controlGroup)
	local rewardString = formatNumberCommas(m_rewards)
	local creditString = formatNumberCommas(m_credits)
	local imgCreditControl = gHandles["imgCredits"]
	local stcCreditControl = gHandles["stcCredits"]
	local stcRewardsControl = gHandles["stcRewards"]
	local imgRewardsControl = gHandles["imgRewards"]

	Control_SetVisible(imgCreditControl, true)
	Control_SetVisible(imgRewardsControl, true)
	Control_SetVisible(stcCreditControl, true)
	Control_SetVisible(stcRewardsControl, true)

	Static_SetText(stcRewardsControl, rewardString)
	Static_SetText(stcCreditControl, creditString)

	local newCreditLocationX = Control_GetLocationX(stcRewardsControl) + string.len(rewardString) * 8
	Control_SetLocationX(imgCreditControl, newCreditLocationX)
	Control_SetLocationX(stcCreditControl, newCreditLocationX + Control_GetWidth(imgCreditControl) + 5)

	if controlGroup == "C" then
		local newLocationY = Control_GetLocationY(gHandles["stcKanevaLevel"])
		Control_SetLocationY(imgCreditControl, newLocationY )
		Control_SetLocationY(stcCreditControl, newLocationY)
		Control_SetLocationY(imgRewardsControl, newLocationY)
		Control_SetLocationY(stcRewardsControl, newLocationY)
	end
end

function updateLevel()
	--Control Handles
	local stcLevelControl = gHandles["stcKanevaLevel"]
	local imgLevelBarControl = gHandles["imgLevelProgress"]
	local imgLevelBarControlBG = gHandles["imgLevelProgressBG"]

	Control_SetVisible(imgLevelBarControl, true)
	Control_SetVisible(imgLevelBarControlBG, true)
	if m_playerLevel == nil then
		m_playerLevel = 0
	end

	Static_SetText(stcLevelControl, "Level "..tostring(m_playerLevel))
	Control_SetLocationY(stcLevelControl, Control_GetLocationY(imgLevelBarControl) - 3)
	local progressWidth = Control_GetWidth(imgLevelBarControlBG) * (m_levelProgress / 100)
	local progressHeight = Control_GetHeight(imgLevelBarControlBG)
	Control_SetSize(imgLevelBarControl, progressWidth, progressHeight)
end

function formatNumberCommas(inputStr)
    if tonumber(inputStr) == nil then
        return inputStr
    end

    local formatted = inputStr
    while true do  
        formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
        if (k==0) then
            break
        end
    end
    return formatted
end

function updateFameButton()
	Control_SetSize(gHandles["btnMyFame"], AB_BUTTON_SIZE, Control_GetHeight(gHandles["btnMyFame"]))
	Control_SetLocationX(gHandles["vertRule"], AB_BUTTON_SIZE)
end

-----------------------
--Linked zone functions
-----------------------

function parentDataHandler(event)
	local zoneID =  event.zoneID
	local zoneType = event.zoneType
	m_parentZoneCount = tonumber(event.availableZones) or 2

	if m_bBuildDisabled == false then
		m_bBuildDisabled = event.gameHome
	end

	if (zoneID and zoneID ~= 0) and (zoneType and zoneType ~= 0) and not (m_bBuildDisabled == true) then
		local url = GameGlobals.WEB_SITE_PREFIX..GET_LINKED_ZONES_SUFFIX.."&zoneInstanceID="..tostring(zoneID).."&zoneType="..tostring(zoneType)
		makeWebCall(url, WF.LINKED_ZONES)
	else
		if g_currentURL and g_currentURL ~= "" then
			m_parentURL = g_currentURL
			m_parentZoneID = KEP_GetZoneInstanceId()
			m_parentZoneType = KEP_GetZoneIndexType()
		end
	end	

	setBuildMode()
end

function ParseParentData(eventData)

	if m_bBuildDisabled then return end --If in a game home world, do not do anything with this call!

    local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	s, strPos, result = string.find(eventData, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		for zoneData in string.gmatch(eventData, "<Table>(.-)</Table>") do
			s, strPos, m_parentZoneID = string.find(zoneData, "<zone_instance_id>(.-)</zone_instance_id>")
			s, strPos, m_parentZoneType = string.find(zoneData, "<zone_type>(.-)</zone_type>")
			s, strPos, url = string.find(zoneData, "<STPURL>(.-)</STPURL>")

			url = string.gsub(url, "3296", "World of Kaneva")
			url = string.gsub(url, "3298", "Kaneva Preview")
			url = string.gsub(url, "5316", "Dev Star")
			url = string.gsub(url, "5310", "RC Kaneva")

			local playerInfo = { KEP_GetPlayerInfo() }
			

			m_parentURL = url
			return
		end
	end
end

function getParentInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local ev = KEP_EventCreate("FRAMEWORK_RETURN_PARENT_INFO")
	KEP_EventEncodeNumber(ev, m_parentZoneType)
	KEP_EventEncodeNumber(ev, m_parentZoneID)
	KEP_EventEncodeNumber(ev, m_parentZoneCount)
	KEP_EventQueue( ev)
end