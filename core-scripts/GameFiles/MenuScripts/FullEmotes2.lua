--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\MenuLocation.lua")
dofile("..\\MenuScripts\\CAM.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

g_tabs = {}
g_tabs["Default"] = { button = "btnDefaultEmotes", controls = { "lbDefaultEmotes" }, anims = {} }
g_tabs["My Emotes"] = { button = "btnMyEmotes", controls = { "lbMyEmotes" }, anims = {} }

g_currTabName = "Default"

g_t_items = nil

numAnims = 39
g_pageindex = 1
g_maxpages = 1

g_emoteselectedindex=0
g_emotename=""
g_animationindex=-1
gPlayerGender = ""

g_nextpage = nil
g_previouspage = nil

--for AP and VIP passes
REQUEST_ACCESS_PASS_INFO_EVENT = "RequestAccessPassInfoEvent"
RESPONSE_ACCESS_PASS_INFO_EVENT = "ResponseAccessPassInfoEvent"
REQUEST_ACCESS_PASS_ZONE_INFO_EVENT = "RequestAccessPassZoneInfoEvent"
RESPONSE_ACCESS_PASS_ZONE_INFO_EVENT = "ResponseAccessPassZoneInfoEvent"
REQUEST_UNLOCKABLE_EVENT = "RequestUnlockableEvent"

g_iHasAccessPass = 0
g_iHasVIPPass = 0
g_iZoneHasAccessPass = 0
g_iZoneHasVIPPass = 0

ACCESS_PASS_ID = 1
VIP_PASS_ID = 2

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "responseAccessPassInfoHandler", RESPONSE_ACCESS_PASS_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassZoneInfoHandler", RESPONSE_ACCESS_PASS_ZONE_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFiltered( "attribEventHandler", "AttribEvent", AttribEvent.PLAYER_INVEN, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "requestUnlockableEventHandler", REQUEST_UNLOCKABLE_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "EmoteAddEvent",KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function responseAccessPassInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iHasAccessPass = KEP_EventDecodeNumber(event)
	g_iHasVIPPass = KEP_EventDecodeNumber(event)
	return KEP.EPR_OK
end

function responseAccessPassZoneInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iZoneHasAccessPass = KEP_EventDecodeNumber(event)
	g_iZoneHasVIPPass = KEP_EventDecodeNumber(event)
	return KEP.EPR_OK
end

function requestUnlockableEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == UnlockableRequestFilter.EmotesResponse then
		local numAnims = KEP_EventDecodeNumber(event)
		for i=1,numAnims do
			local type = KEP_EventDecodeNumber(event)
			local id = KEP_EventDecodeNumber(event)
			local gender = KEP_EventDecodeString(event)
			local locked = KEP_EventDecodeNumber(event)
			local fameType = KEP_EventDecodeNumber(event)
			local levelNumber = KEP_EventDecodeNumber(event)
			for key, emote in pairs(g_tabs["Default"].anims) do
				if emote.id==id then
					emote.locked = locked
				end
			end
		end
		listDefaultEmotes()
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	--get the player's passes
	local ev = KEP_EventCreate( REQUEST_ACCESS_PASS_INFO_EVENT )
	KEP_EventQueue( ev )

	--get passes for current zone
	local evt = KEP_EventCreate( REQUEST_ACCESS_PASS_ZONE_INFO_EVENT )
	KEP_EventQueue( evt )

	-- get player gender
	gPlayerGender = KEP_GetLoginGender()
	if gPlayerGender=="M" then
		SetUpMaleAnimations()
	else
		SetUpFemaleAnimations()
	end

	addListBoxItem( "lbMyEmotes", "Loading, please wait...", -1 )
	addListBoxItem( "lbDefaultEmotes", "Loading, please wait...", -1 )

	requestUnlockedEmotes()
	RequestInventory()

	-- setup look of listbox scrollbars
	local lbh = Dialog_GetListBox(dialogHandle, "lbDefaultEmotes")
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then
			local eh = ScrollBar_GetTrackDisplayElement(sbh)

			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end

	-- setup look of listbox scrollbars
	local lbh = Dialog_GetListBox(dialogHandle, "lbMyEmotes")
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then
			local eh = ScrollBar_GetTrackDisplayElement(sbh)

			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end
end

function requestUnlockedEmotes()
	local ev = KEP_EventCreate( REQUEST_UNLOCKABLE_EVENT)
	KEP_EventSetFilter(ev, UnlockableRequestFilter.EmotesRequest)
	KEP_EventQueue( ev)
end

function SetUpMaleAnimations()

	for i=1,numAnims do
		g_tabs["Default"].anims[i] = {}
	end

	--For now all the animations are hard coded until, we can figure out
	--how we are going to get them from the database.
	g_tabs["Default"].anims[ 1].name="Laugh"
	g_tabs["Default"].anims[ 2].name="Cheer"
	g_tabs["Default"].anims[ 3].name="Cry"
	g_tabs["Default"].anims[ 4].name="Kneel"
	g_tabs["Default"].anims[ 5].name="Yes"
	g_tabs["Default"].anims[ 6].name="No"
	g_tabs["Default"].anims[ 7].name="Wave"
	g_tabs["Default"].anims[ 8].name="Rude Gesture"
	g_tabs["Default"].anims[ 9].name="Point"
	g_tabs["Default"].anims[10].name="Blow a Kiss"
	g_tabs["Default"].anims[11].name="Clap"
	g_tabs["Default"].anims[12].name="Wink"
	g_tabs["Default"].anims[13].name="Sit"
	g_tabs["Default"].anims[14].name="Lay Down"
	g_tabs["Default"].anims[15].name="Flex Muscles"
	g_tabs["Default"].anims[16].name="Pound Chest"
	g_tabs["Default"].anims[17].name="Scream"
	g_tabs["Default"].anims[18].name="I Don't Know"
	g_tabs["Default"].anims[19].name="Head Bang"
	g_tabs["Default"].anims[20].name="Break Dance"
	g_tabs["Default"].anims[21].name="Two Steps"
	g_tabs["Default"].anims[22].name="Country"
	g_tabs["Default"].anims[23].name="Disco"
	g_tabs["Default"].anims[24].name="Techno"
	g_tabs["Default"].anims[25].name="Car Wash"
	g_tabs["Default"].anims[26].name="Turntable"
	g_tabs["Default"].anims[27].name="Bump N Grind"
	g_tabs["Default"].anims[28].name="Boot Scoot"
	g_tabs["Default"].anims[29].name="Cha Cha"
	g_tabs["Default"].anims[30].name="Rumba"
	g_tabs["Default"].anims[31].name="Groovin"
	g_tabs["Default"].anims[32].name="Swish"
	g_tabs["Default"].anims[33].name="Disco Fever"
	g_tabs["Default"].anims[34].name="Zoom Zoom"
	g_tabs["Default"].anims[35].name="Bustamove"
	g_tabs["Default"].anims[36].name="Sprinkler"
	g_tabs["Default"].anims[37].name="Lawnmower"
	g_tabs["Default"].anims[38].name="Robot"
	g_tabs["Default"].anims[39].name="Special (VIP)"

	--Saving all the animation id's using the emote name as reference
	--Once we can retrive it from the database this will change.
	g_tabs["Default"].anims[ 1].id=anim_laugh
	g_tabs["Default"].anims[ 2].id=anim_cheer
	g_tabs["Default"].anims[ 3].id=anim_cry
	g_tabs["Default"].anims[ 4].id=anim_kneel
	g_tabs["Default"].anims[ 5].id=anim_yes
	g_tabs["Default"].anims[ 6].id=anim_no
	g_tabs["Default"].anims[ 7].id=anim_wave
	g_tabs["Default"].anims[ 8].id=anim_male_rude
	g_tabs["Default"].anims[ 9].id=anim_point
	g_tabs["Default"].anims[10].id=anim_blow
	g_tabs["Default"].anims[11].id=anim_clap
	g_tabs["Default"].anims[12].id=anim_flirt
	g_tabs["Default"].anims[13].id=anim_sit
	g_tabs["Default"].anims[14].id=anim_laydown
	g_tabs["Default"].anims[15].id=anim_flexing_muscles
	g_tabs["Default"].anims[16].id=anim_chest_pound
	g_tabs["Default"].anims[17].id=anim_scream
	g_tabs["Default"].anims[18].id=anim_give_up
	g_tabs["Default"].anims[19].id=anim_head_bang
	g_tabs["Default"].anims[20].id=anim_break_dance
	g_tabs["Default"].anims[21].id=anim_hip_hop
	g_tabs["Default"].anims[22].id=anim_male_country
	g_tabs["Default"].anims[23].id=anim_male_disco
	g_tabs["Default"].anims[24].id=anim_male_techno
	g_tabs["Default"].anims[25].id=anim_male_breakdance_1
	g_tabs["Default"].anims[26].id=anim_male_breakdance_2
	g_tabs["Default"].anims[27].id=anim_sexy_07
	g_tabs["Default"].anims[28].id=anim_country_1
	g_tabs["Default"].anims[29].id=anim_latin_1
	g_tabs["Default"].anims[30].id=anim_latin_2
	g_tabs["Default"].anims[31].id=anim_pop_2
	g_tabs["Default"].anims[32].id=anim_pop_5
	g_tabs["Default"].anims[33].id=anim_disco_1
	g_tabs["Default"].anims[34].id=anim_hiphop_2
	g_tabs["Default"].anims[35].id=anim_hiphop_6
	g_tabs["Default"].anims[36].id=anim_sprinkler_dance
	g_tabs["Default"].anims[37].id=anim_lawnmower_dance
	g_tabs["Default"].anims[38].id=anim_robot_dance
	g_tabs["Default"].anims[39].id=anim_celebration_dance_1

	for i = 1, numAnims do --add 0 for pass restrictions for all anims first
		g_tabs["Default"].anims[i].pass = 0
		g_tabs["Default"].anims[i].locked = 1

		--enable default emotes
		if g_tabs["Default"].anims[i].name == "Sit" or
			g_tabs["Default"].anims[i].name == "Clap" or
			g_tabs["Default"].anims[i].name == "Wave" or
			g_tabs["Default"].anims[i].name == "Laugh" or
			g_tabs["Default"].anims[i].name == "Yes" or
			g_tabs["Default"].anims[i].name == "No" or
			g_tabs["Default"].anims[i].name == "Bustamove" or
			g_tabs["Default"].anims[i].name == "Disco Fever" or
			g_tabs["Default"].anims[i].name == "Bump N Grind" or
			g_tabs["Default"].anims[i].id == anim_celebration_dance_1 then
			g_tabs["Default"].anims[i].locked = 0
		end
	end

	--now hardcode animations that actually require a pass :(
	g_tabs["Default"].anims[39].pass = VIP_PASS_ID
end

function SetUpFemaleAnimations()

	for i=1,numAnims do
		g_tabs["Default"].anims[i] = {}
	end

	--For now all the animations are hard coded until, we can figure out
	--how we are going to get them from the database.
	g_tabs["Default"].anims[ 1].name="Laugh"
	g_tabs["Default"].anims[ 2].name="Cheer"
	g_tabs["Default"].anims[ 3].name="Cry"
	g_tabs["Default"].anims[ 4].name="Kneel"
	g_tabs["Default"].anims[ 5].name="Yes"
	g_tabs["Default"].anims[ 6].name="No"
	g_tabs["Default"].anims[ 7].name="Wave"
	g_tabs["Default"].anims[ 8].name="Rude Gesture"
	g_tabs["Default"].anims[ 9].name="Point"
	g_tabs["Default"].anims[10].name="Blow a Kiss"
	g_tabs["Default"].anims[11].name="Clap"
	g_tabs["Default"].anims[12].name="Wink"
	g_tabs["Default"].anims[13].name="Sit"
	g_tabs["Default"].anims[14].name="Lay Down"
	g_tabs["Default"].anims[15].name="Flex Muscles"
	g_tabs["Default"].anims[16].name="Pound Chest"
	g_tabs["Default"].anims[17].name="Scream"
	g_tabs["Default"].anims[18].name="I Don't Know"
	g_tabs["Default"].anims[19].name="Head Bang"
	g_tabs["Default"].anims[20].name="Break Dance"
	g_tabs["Default"].anims[21].name="Two Steps"
	g_tabs["Default"].anims[22].name="Country"
	g_tabs["Default"].anims[23].name="Disco"
	g_tabs["Default"].anims[24].name="Techno"
	g_tabs["Default"].anims[25].name="Car Wash"
	g_tabs["Default"].anims[26].name="Turntable"
	g_tabs["Default"].anims[27].name="Bump N Grind"
	g_tabs["Default"].anims[28].name="Boot Scoot"
	g_tabs["Default"].anims[29].name="Cha Cha"
	g_tabs["Default"].anims[30].name="Rumba"
	g_tabs["Default"].anims[31].name="Groovin"
	g_tabs["Default"].anims[32].name="Swish"
	g_tabs["Default"].anims[33].name="Disco Fever"
	g_tabs["Default"].anims[34].name="Zoom Zoom"
	g_tabs["Default"].anims[35].name="Bustamove"
	g_tabs["Default"].anims[36].name="Sprinkler"
	g_tabs["Default"].anims[37].name="Lawnmower"
	g_tabs["Default"].anims[38].name="Robot"
	g_tabs["Default"].anims[39].name="Special (VIP)"

	--Saving all the animation id's using the emote name as reference
	--Once we can retrive it from the database this will change.
	g_tabs["Default"].anims[ 1].id=anim_laugh
	g_tabs["Default"].anims[ 2].id=anim_cheer
	g_tabs["Default"].anims[ 3].id=anim_cry
	g_tabs["Default"].anims[ 4].id=anim_kneel
	g_tabs["Default"].anims[ 5].id=anim_yes
	g_tabs["Default"].anims[ 6].id=anim_no
	g_tabs["Default"].anims[ 7].id=anim_wave
	g_tabs["Default"].anims[ 8].id=anim_male_rude
	g_tabs["Default"].anims[ 9].id=anim_point
	g_tabs["Default"].anims[10].id=anim_blow
	g_tabs["Default"].anims[11].id=anim_clap
	g_tabs["Default"].anims[12].id=anim_flirt
	g_tabs["Default"].anims[13].id=anim_sit
	g_tabs["Default"].anims[14].id=anim_laydown
	g_tabs["Default"].anims[15].id=anim_flexing_muscles
	g_tabs["Default"].anims[16].id=anim_chest_pound
	g_tabs["Default"].anims[17].id=anim_scream
	g_tabs["Default"].anims[18].id=anim_give_up
	g_tabs["Default"].anims[19].id=anim_head_bang
	g_tabs["Default"].anims[20].id=anim_break_dance
	g_tabs["Default"].anims[21].id=anim_hip_hop
	g_tabs["Default"].anims[22].id=anim_male_country
	g_tabs["Default"].anims[23].id=anim_male_disco
	g_tabs["Default"].anims[24].id=anim_male_techno
	g_tabs["Default"].anims[25].id=anim_female_popdance_1
	g_tabs["Default"].anims[26].id=anim_female_popdance_3
	g_tabs["Default"].anims[27].id=anim_sexy_01
	g_tabs["Default"].anims[28].id=anim_country_1
	g_tabs["Default"].anims[29].id=anim_latin_1
	g_tabs["Default"].anims[30].id=anim_latin_2
	g_tabs["Default"].anims[31].id=anim_pop_2
	g_tabs["Default"].anims[32].id=anim_pop_5
	g_tabs["Default"].anims[33].id=anim_disco_1
	g_tabs["Default"].anims[34].id=anim_hiphop_2
	g_tabs["Default"].anims[35].id=anim_hiphop_6
	g_tabs["Default"].anims[36].id=anim_sprinkler_dance
	g_tabs["Default"].anims[37].id=anim_lawnmower_dance
	g_tabs["Default"].anims[38].id=anim_robot_dance
	g_tabs["Default"].anims[39].id=anim_celebration_dance_1

	for i = 1, numAnims do --add 0 for pass restrictions for all anims first
		g_tabs["Default"].anims[i].pass = 0
		g_tabs["Default"].anims[i].locked = 1

		--enable default emotes
		if g_tabs["Default"].anims[i].name == "Sit" or
			g_tabs["Default"].anims[i].name == "Clap" or
			g_tabs["Default"].anims[i].name == "Wave" or
			g_tabs["Default"].anims[i].name == "Laugh" or
			g_tabs["Default"].anims[i].name == "Yes" or
			g_tabs["Default"].anims[i].name == "No" or
			g_tabs["Default"].anims[i].name == "Bustamove" or
			g_tabs["Default"].anims[i].name == "Disco Fever" or
			g_tabs["Default"].anims[i].name == "Bump N Grind" or
			g_tabs["Default"].anims[i].id == anim_celebration_dance_1 then
			g_tabs["Default"].anims[i].locked = 0
		end
	end

	--now hardcode animations that actually require a pass :(
	g_tabs["Default"].anims[39].pass = VIP_PASS_ID
end

function listDefaultEmotes()
	ListBox_RemoveAllItems(gHandles["lbDefaultEmotes"])
	--Fill the emote buttons with the correct text.
	for i = 1, numAnims do
		if g_tabs["Default"].anims[i].locked == 0 then
			addListBoxItem( "lbDefaultEmotes", g_tabs["Default"].anims[i].name, g_tabs["Default"].anims[i].id )
		end
	end
end

function Dialog_OnDestroy(dialogHandle)
	saveLocation(dialogHandle, "Emotes")
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnAddEmote_OnButtonClicked(buttonHandle)
	if(ValidatePass(g_animationindex) == false) then
		return false
	end
	local event = KEP_EventCreate("EmoteAddEvent")
	KEP_EventEncodeNumber(event,pageindex)
	KEP_EventEncodeNumber(event,g_animationindex)
	KEP_EventEncodeString(event,g_emotename)
	KEP_EventQueue( event )
	KEP_Log( "SetEmote: animID=" .. g_animationindex .. ", emote=" .. g_emotename )
	MenuOpen("SetEmote.xml")
end

function OnAnimListSelection( listBoxHandle, x, y, sel, selText )
	local data = ListBox_GetItemData( listBoxHandle, sel )
	g_animationindex = data
	for key, emote in pairs(g_tabs[g_currTabName].anims) do
		if emote.id==g_animationindex then
			g_emotename = emote.name
			break
		end
	end
	if data~=-1 then
		ChangeAnimation(data)
	end
end

lbDefaultEmotes_OnListBoxSelection = OnAnimListSelection
lbMyEmotes_OnListBoxSelection = OnAnimListSelection

function TabOnButtonClicked( buttonHandle )

	g_currTabName = Static_GetText( Button_GetStatic( buttonHandle ) )
	for tabName, tab in pairs( g_tabs ) do
		if g_currTabName==tabName then
			enableControl( g_tabs[tabName].button, 0 )
			for idxCtl, ctlName in ipairs( g_tabs[tabName].controls ) do
				showControl( ctlName )
			end
		else
			enableControl( g_tabs[tabName].button, 1 )
			for idxCtl, ctlName in ipairs( g_tabs[tabName].controls ) do
				hideControl( ctlName )
			end
		end
	end
end

btnDefaultEmotes_OnButtonClicked = TabOnButtonClicked
btnMyEmotes_OnButtonClicked = TabOnButtonClicked

-- temp: get from inventory for now -- eventually we will have a separate wok service
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	if filter ~= WF.PLAYER_EMOTES then return end

	local xmlData = KEP_EventDecodeString( event )
	g_t_items 	= {}
	decodeInventoryItemsFromWeb( g_t_items, xmlData, true )
	g_tabs["My Emotes"].anims = {}
	removeAllListBoxItems( "lbMyEmotes" )
	for idx, t_item in ipairs( g_t_items ) do
		if t_item["actor_group"]==0 or t_item["actor_group"]==1 and gPlayerGender=="M" or t_item["actor_group"]==2 and gPlayerGender=="F" then
			addListBoxItem( "lbMyEmotes", t_item.name, t_item.GLID )
			g_tabs["My Emotes"].anims[idx] = {}
			g_tabs["My Emotes"].anims[idx].name = t_item.name
			g_tabs["My Emotes"].anims[idx].id = t_item.GLID
		end
	end

	return KEP.EPR_OK	-- don't consume this, leave it for inventory menu
end

-- request inventory from server
function RequestInventory()
	g_t_items 	= {}

	-- request inventory from game server so that game server can perform inventory
	-- processing work, such as, moving items from the pending adds table into the user's
	-- inventory. TODO: create a seperate event to accomplish this work without actually
	-- getting the user's inventory which is now fetched from the web. Abandon returning
	-- inventory glids from the server, rework streamable icon system to be done in script.
	KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == AttribEvent.PLAYER_INVEN then
		getPlayerInventory(WF.PLAYER_EMOTES, 1, 100000, "avatar", "", "emotes")
	end
end
