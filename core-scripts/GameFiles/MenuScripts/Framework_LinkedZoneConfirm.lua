--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")
dofile("Framework_DragDropHelper.lua")
dofile("Easing.lua")
dofile("HUDHelper.lua")

local m_sParentName = ""

local m_iChildZoneID = nil
local m_iChildZoneType = nil

function onCreate()
	KEP_EventRegisterHandler("addNewChild", "FRAMEWORK_BUY_NEW_CHILD", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler("deleteChild", "FRAMEWORK_DELETE_CHILD", KEP.HIGH_PRIO)

	m_playerName = KEP_GetLoginName()
end

function btnAdd_OnButtonClicked(buttonHandle)
	local playerName = KEP_GetLoginName()
	local event = KEP_EventCreate("UPDATE_CREDIT_TRANSACTION")
	KEP_EventEncodeString(event, 1000)
	KEP_EventEncodeString(event, playerName)
	KEP_EventEncodeString(event, m_sParentName)
	KEP_EventEncodeString(event, "Zone")
	KEP_EventQueue(event)
	toggleMenu("CreditTransaction")
	MenuCloseThis()
end

function btnCancel_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

function btnDelete_OnButtonClicked(buttonHandle)
	local url = GameGlobals.WEB_SITE_PREFIX..DELETE_LINKED_ZONE.."&zoneInstanceID="..tostring(m_iChildZoneID).."&zoneType="..tostring(m_iChildZoneType)
	makeWebCall(url, WF.LINKED_ZONES_DELETE)
	MenuCloseThis()
end

function chkHideMenu_OnCheckBoxChanged( checkboxHandle, checked )
	--Save the new config
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber( config, "HideAddChildMenu"..tostring(m_playerName), checked )
		KEP_ConfigSave( config )
	end
end

function onConfirmDropHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)

end

function addNewChild(dispatcher, fromNetid, tEvent, eventid, filter, objectid)

	m_sParentName = KEP_EventDecodeString(tEvent)
	local dbPath = DEFAULT_KANEVA_PLACE_ICON

	Static_SetText(gHandles["stcMessage"], "Adding a zone cost 1000 credits. V.I.P. Members get 4 free zones.")

	setControlEnabledVisible("btnAdd", true)
	setControlEnabledVisible("btnDelete", false)
	setControlEnabledVisible("chkHideMenu", true)
	setControlEnabledVisible("imgZoneName", false)
end

function deleteChild(dispatcher, fromNetid, tEvent, eventid, filter, objectid)

	local childName = KEP_EventDecodeString(tEvent)
	local fileName = KEP_EventDecodeString(tEvent)
	m_iChildZoneID = KEP_EventDecodeNumber(tEvent)
	m_iChildZoneType = KEP_EventDecodeNumber(tEvent)
	

	scaleImage(gHandles["imgZone"], gHandles["imgZoneBG"], fileName)      

	Static_SetText(gHandles["stcMessage"], "Delete "..tostring(childName).."?")
	Static_SetText(gHandles["stcZone"], tostring(childName))

	setControlEnabledVisible("btnAdd", false)
	setControlEnabledVisible("btnDelete", true)
	setControlEnabledVisible("chkHideMenu", false)
	setControlEnabledVisible("stcTip", true)


end

function setControlEnabledVisible(controlName, bool)
	Control_SetVisible(gHandles[controlName], bool)
	Control_SetEnabled(gHandles[controlName], bool)
end