--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_QuestTurnIn.lua
--
-- Displays info for a completeable quest
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")

-- Constants
local ITEM_BUFFER = 20
local QUEST_DESCRIPTION_Y_BUFFER = 8
local COMPLETION_ANIMATION_M = 128
local COMPLETION_ANIMATION_F = 52

-- Local
local m_questInfo = {}
local m_rewardItem = {}
local m_playerInventory = {}
local m_actorPID = nil
m_updatedQuestsPending = false -- Should the QuestGiver menu delay showing this quest again (in case we need to run some stuff by the server first)

-- Called when the menu is created
function onCreate()
	-- Register client-client event handlers
	KEP_EventRegisterHandler("onQuestInfoHandler", "FRAMEWORK_QUEST_INFO", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCheckFullResponse", "INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	
	requestInventory(INVENTORY_PID)
end

function onDestroy()
	-- Inform QuestGiver that you've closed
	local ev = KEP_EventCreate("FRAMEWORK_QUEST_INFO_CLOSING") -- Handled by Framework_QuestGiver
	KEP_EventEncodeString(ev, tostring(m_updatedQuestsPending))
	KEP_EventQueue(ev)
end

-- Button Handlers

-- Complete quest
function btnComplete_OnButtonClicked(buttonHandle)
	preprocessQuestCompletion()
end

-- Event handlers

-- Handled quest info passed from Framework_QuestGiver
function onQuestInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_questInfo  	 = Events.decode(KEP_EventDecodeString(event))
	local acceptable = KEP_EventDecodeString(event) -- Un-needed
	local isModal 	 = KEP_EventDecodeString(event) == "true"
	m_rewardItem 	 = Events.decode(KEP_EventDecodeString(event))
	m_actorPID		 = KEP_EventDecodeNumber(event)

	if isModal then
		MenuSetModalThis(true)
		Dialog_SetCloseOnESC(gDialogHandle, 0)
		Control_SetVisible(gHandles["btnClose"], false)
		Control_SetEnabled(gHandles["btnClose"], false)
		preprocessCheck()
	end
		
	-- Set image
	local imgHandle = Image_GetDisplayElement(gHandles["imgQuestIcon"])
	if imgHandle and gDialogHandle then
		KEP_LoadIconTextureByID(tonumber(m_questInfo.properties.GLID), imgHandle, gDialogHandle)
	end
	Control_SetVisible(gHandles["imgQuestIcon"], true)
	
	-- Set quest name
	local nameHeight = Static_GetStringHeight(gHandles["stcQuestName"], tostring(m_questInfo.properties.name))
	Static_SetText(gHandles["stcQuestName"], tostring(m_questInfo.properties.name))
	Control_SetSize(gHandles["stcQuestName"], Control_GetWidth(gHandles["stcQuestName"]), nameHeight)
	Control_SetVisible(gHandles["stcQuestName"], true)
	
	-- Set quest description
	local descriptionHeight = Static_GetStringHeight(gHandles["stcQuestText"], tostring(m_questInfo.properties.questPostText))
	Static_SetText(gHandles["stcQuestText"], tostring(m_questInfo.properties.questPostText))
	Control_SetSize(gHandles["stcQuestText"], Control_GetWidth(gHandles["stcQuestText"]), descriptionHeight)
	local descriptionY = Control_GetLocationY(gHandles["stcQuestName"]) + nameHeight + QUEST_DESCRIPTION_Y_BUFFER
	Control_SetSize(gHandles["stcQuestText"], Control_GetWidth(gHandles["stcQuestText"]), descriptionHeight)
	Control_SetLocationY(gHandles["stcQuestText"], descriptionY)
	Control_SetVisible(gHandles["stcQuestText"], true)
	
	local placementY = math.max(Control_GetLocationY(gHandles["imgQuestIcon"]) + Control_GetHeight(gHandles["imgQuestIcon"]), descriptionY+descriptionHeight) + ITEM_BUFFER
	
	local reward = m_questInfo.properties.rewardItem
	local returnItem = m_questInfo.properties.requiredItem

	if reward.rewardUNID ~= "0" then
		Control_SetLocationY(gHandles["stcRewardLine"], placementY)
		Control_SetVisible(gHandles["stcRewardLine"], true)
		placementY = placementY + ITEM_BUFFER
		Control_SetLocationY(gHandles["imgLine2"], placementY)
		Control_SetVisible(gHandles["imgLine2"], true)
		
		placementY = placementY + 10
		-- Populate the quest reward
		Control_SetLocationY(gHandles["imgReward"], placementY)
		Control_SetLocationY(gHandles["stcReward"], placementY)
		if reward.rewardCount == 1 then
			Static_SetText(gHandles["stcReward"], tostring(reward.name))
		else
			Static_SetText(gHandles["stcReward"], tostring(reward.name).." (x"..tostring(reward.rewardCount)..")")
		end
		Control_SetVisible(gHandles["stcReward"], true)
		-- Set reward item image
		local imgHandle = Image_GetDisplayElement(gHandles["imgReward"])
		if imgHandle and gDialogHandle then
			KEP_LoadIconTextureByID(tonumber(reward.GLID), imgHandle, gDialogHandle)
		end
		Control_SetVisible(gHandles["imgReward"], true)
		
		placementY = placementY + Control_GetHeight(gHandles["imgReward"]) + 20

		if returnItem.requiredUNID == "0" then
			Static_SetText(gHandles["btnComplete"], "Collect Reward")
		end 
	end
	
	Control_SetLocationY(gHandles["btnComplete"], placementY)
	
	local menuHeight = Control_GetLocationY(gHandles["btnComplete"]) + Control_GetHeight(gHandles["btnComplete"]) + 20
	Dialog_SetSize(gDialogHandle, Dialog_GetWidth(gDialogHandle), menuHeight)
	MenuCenterThis()
end

-- A single Inventory item has been updated
function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	local inventory = m_playerInventory
	if inventory[updateIndex] then
		inventory[updateIndex] = updateItem
		m_playerInventory = inventory
	end
end

-- Whole inventory update
function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local inventory = decompileInventory(tEvent)
	m_playerInventory = inventory
end

-- Preprocess check
function onCheckFullResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local lootValid = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	if not lootValid then -- Unlock the menu so the player can create room in their backpack
		MenuSetModalThis(false)
		-- Dialog_SetCloseOnESC(gDialogHandle, 1)
		-- Control_SetVisible(gHandles["btnClose"], true)
		-- Control_SetEnabled(gHandles["btnClose"], true)
	end
end

-- Response for loot acquisition event
function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	if success then
		handleQuestCompletion()
	end
end

-- Attempt to add / remove rewards
function preprocessQuestCompletion()
	local requiredUNID = tonumber(m_questInfo.properties.requiredItem.requiredUNID)
	local requiredCount = m_questInfo.properties.requiredItem.requiredCount
	local rewardUNID = tonumber(m_questInfo.properties.rewardItem.rewardUNID)
	local rewardCount = m_questInfo.properties.rewardItem.rewardCount

	local requiredItemExists = false
	local rewardExists = false
	
	local transactionID = KEP_GetCurrentEpochTime()	
	local removeTable = {}
	if m_questInfo.properties.questType == "collect" and (requiredUNID > 0) and m_questInfo.properties.destroyOnCompletion == "true" then
		local removeItem = {UNID = requiredUNID, count = requiredCount}
		removeItem.lootInfo = {sourcePID = m_actorPID, lootSource = "Quest", transactionID = transactionID, questUNID = m_questInfo.UNID}
		table.insert(removeTable, removeItem)
		requiredItemExists = true
	end
	
	local addTable = {}
	m_rewardItem.UNID = rewardUNID
	if m_rewardItem.UNID and tonumber(m_rewardItem.UNID) > 0 then
		local addItem = deepCopy(m_rewardItem)
		addItem.lootInfo = {sourcePID = m_actorPID, lootSource = "Quest", transactionID = transactionID}
		addItem.count = rewardCount or 1
		table.insert(addTable, addItem)
		rewardExists = true
	end

	if requiredItemExists or rewardExists then
		processTransaction(removeTable, addTable)
	else
		handleQuestCompletion()
	end
end

function handleQuestCompletion()
	completeQuest(m_questInfo.UNID, m_questInfo)

	Events.sendEvent("QUEST_COMPLETE_CHECK_NEXT", {questUNID = m_questInfo.UNID})
	
	--Animation and particle effect for completing a quest
	local player = KEP_GetPlayer()
	local netId = GetSet_GetNumber(player, MovementIds.NETWORKDEFINEDID)
	Events.sendEvent("FRAMEWORK_QUEST_PLAY_SOUND")
	if KEP_IsMale() then
		KEP_SetCurrentAnimationByGLID(COMPLETION_ANIMATION_M)
	else
		KEP_SetCurrentAnimationByGLID(COMPLETION_ANIMATION_F)
	end
	m_updatedQuestsPending = true
	MenuCloseThis()
end

-- Disable modal if backpack is full
function preprocessCheck()
	local requiredUNID = tonumber(m_questInfo.properties.requiredItem.requiredUNID)
	local requiredCount = m_questInfo.properties.requiredItem.requiredCount
	local rewardUNID = tonumber(m_questInfo.properties.rewardItem.rewardUNID)
	local rewardCount = m_questInfo.properties.rewardItem.rewardCount
	
	local transactionID = KEP_GetCurrentEpochTime()	
	local removeTable = {}
	if (requiredUNID > 0) and m_questInfo.properties.destroyOnCompletion == "true" then
		local removeItem = {UNID = requiredUNID, count = requiredCount}
		removeItem.lootInfo = {sourcePID = m_actorPID, lootSource = "Quest", transactionID = transactionID, questUNID = m_questInfo.UNID}
		table.insert(removeTable, removeItem)
	end
	
	local addTable = {}
	m_rewardItem.UNID = rewardUNID
	if m_rewardItem.UNID and tonumber(m_rewardItem.UNID) > 0 then
		local addItem = deepCopy(m_rewardItem)
		addItem.lootInfo = {sourcePID = m_actorPID, lootSource = "Quest", transactionID = transactionID}
		addItem.count = rewardCount or 1
		table.insert(addTable, addItem)
	end
	processTransaction(removeTable, addTable, true)
end