--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")

local sendCreateDragElementEvent --(item)

local ELEMENT_STATES = {INVALID = 0, VALID = 1, DROP = 2}

local m_dragItem = nil
local m_originalItem = nil
local m_dragElement = nil
local m_dragElementName = nil
local m_hoverElement = nil
local m_dropIntoWorld = false
local m_dragging = false

local m_mouseX = 0
local m_mouseY = 0

local m_shifted = false

local m_offsetX = 0
local m_offsetY = 0

local m_playerName = nil

function onCreate()	
	KEP_EventRegister("DRAG_DROP_START", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_MOUSE_ENTER", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_MOUSE_LEAVE", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_MOUSE_ENTER_DIALOG", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_MOUSE_LEAVE_DIALOG", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_CREATE_ELEMENT", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_SET_ELEMENT_STATE", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_CANCEL", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_CANCEL_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_CUSTOM_DROP", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_REQUEST_ELEMENT", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_RETURN_ELEMENT", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_UPDATE_ELEMENT", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_CONFIRM_DROP", KEP.HIGH_PRIO)
	KEP_EventRegister("DRAG_DROP_CONFIRM_SELL", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("startDragDrop", "DRAG_DROP_START", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("cancelDragDrop", "DRAG_DROP_CANCEL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onMouseEnter", "DRAG_DROP_MOUSE_ENTER", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onMouseLeave", "DRAG_DROP_MOUSE_LEAVE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onMouseEnterDialog", "DRAG_DROP_MOUSE_ENTER_DIALOG", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onMouseLeaveDialog", "DRAG_DROP_MOUSE_LEAVE_DIALOG", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onRequestElement", "DRAG_DROP_REQUEST_ELEMENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onUpdateElement", "DRAG_DROP_UPDATE_ELEMENT", KEP.HIGH_PRIO)
	
	m_playerName = KEP_GetLoginName()
	
	MenuSetLocationThis(0, 0)
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	m_mouseX = Cursor_GetLocationX()
	m_mouseY = Cursor_GetLocationY()
	
	if m_dragItem and m_dragElement then
		MenuSetLocation(m_dragElementName, m_mouseX - m_offsetX, m_mouseY - m_offsetY)
		local mouseDown = Dialog_IsLMouseDown(dialogHandle)
		if mouseDown == 0 then
			dropElement(m_dragItem, m_hoverElement)
			MenuClose(m_dragElementName)
			m_dragElementName = nil
			m_dragElement = nil
		end
	end
end

function Dialog_OnKeyDown(dialogHandle, key, shift, control, alt)
	m_shifted = shift
end

function Dialog_OnKeyUp(dialogHandle, key, shift, control, alt)
	m_shifted = shift
end

function Dialog_OnMoved(dialogHandle, x, y)
	MenuSetLocationThis(0, 0)
end

function startDragDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)

	local event = {}
	
	event.index = KEP_EventDecodeNumber(tEvent)
	event.pid = KEP_EventDecodeNumber(tEvent)
	event.item = decompileInventoryItem(tEvent)
	
	event.item.properties.glow = false
	
	m_offsetX = KEP_EventDecodeNumber(tEvent)
	m_offsetY = KEP_EventDecodeNumber(tEvent)
	
	m_dragElementName = KEP_EventDecodeString(tEvent)
	
	local split = m_shifted

	if m_dragging == false and m_dragItem == nil and m_dragElement == nil then
		m_dragItem = event
		if split then
			m_originalItem = cloneItem(m_dragItem)
			local splitCount = m_originalItem.item.count
			m_dragItem.item.count = math.ceil(splitCount/2)
			if splitCount > 1 then
				m_originalItem.item.count = math.floor(splitCount/2)
				if m_hoverElement then
					m_hoverElement.item.count = m_originalItem.item.count
				end
				updateItem(event.index, event.pid, m_originalItem.item)
			else
				m_originalItem = nil
				removeItem(event.index, event.pid)
			end
		else
			removeItem(event.index, event.pid)
		end

		m_dragElement = createDragElement(m_dragItem)

		if event.pid > 0 then
			updateDirty()
		end
	end
end

function cancelDragDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local cancelResponseEvent = KEP_EventCreate("DRAG_DROP_CANCEL_RESPONSE")
	if m_dragItem and m_dragElement then
		-- Log("Compiling inventory item to drop! "..tostring(m_dragItem.item))

		cancelResponseEvent = compileInventoryItem(m_dragItem.item, cancelResponseEvent)
	end
	KEP_EventSetFilter(cancelResponseEvent, 4)
	KEP_EventQueue(cancelResponseEvent)
	
	if m_dragElementName then
		MenuClose(m_dragElementName)
	end
	m_dragElementName = nil
	m_dragElement = nil
	m_originalItem = nil
	m_dragItem = nil

	log("--- CANCEL DRAG DROP")
end

function onMouseEnter(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local event = {}
	event.index = KEP_EventDecodeNumber(tEvent)
	event.pid = KEP_EventDecodeNumber(tEvent)
	event.item = decompileInventoryItem(tEvent)
	event.dropOnly = KEP_EventDecodeString(tEvent) == "true"
	event.isValid = KEP_EventDecodeString(tEvent)
	
	if event.isValid == "true" then event.isValid = true
	elseif event.isValid == "false" then event.isValid = false
	else event.isValid = nil end
	
	event.occupied = (event.item.UNID ~= 0 and m_dragItem and event.item.UNID ~= m_dragItem.item.UNID)
	
	m_hoverElement = event
	
	if event.isValid == false or (event.isValid == nil and m_dragItem and m_dragItem.item and m_hoverElement and m_hoverElement.item and checkSlotPrerequisites(m_dragItem.item, m_hoverElement.item) == false) then
		if event.isValid == nil then m_hoverElement = nil end
		setDragElementState(ELEMENT_STATES.INVALID)
	else
		setDragElementState(ELEMENT_STATES.VALID)
	end
end

function onMouseLeave(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_hoverElement = nil
	setDragElementState(ELEMENT_STATES.DROP)
end

function onMouseEnterDialog(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	setDragElementState(ELEMENT_STATES.INVALID)
end

function onMouseLeaveDialog(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	setDragElementState(ELEMENT_STATES.DROP)
end

function dropElement(dragItem, hoverElement)
	local itemsToAdd = {}
	
	if m_originalItem ~= nil and m_dropIntoWorld == false then
		m_originalItem.dropOnly = hoverElement.dropOnly
		if hoverElement == nil then
			hoverElement = m_originalItem
		elseif checkForSwap(dragItem, hoverElement) then
			hoverElement = m_originalItem
		end
	end
	
	local customDropIndex = 0
	
	-- Loot comes from a loot container
	if dragItem.pid > 0 then
		m_dragItem.item.lootSource = "Chest"
	-- Loot comes from the Inventory
	elseif dragItem.pid == INVENTORY_PID then
		m_dragItem.item.lootSource = "Inventory"
	-- Loot comes from the Armory tab
	elseif dragItem.pid == ARMORY_PID then
		m_dragItem.item.lootSource = "Armor"
	end
	
	dragItem.item.prevPID = dragItem.pid
	dragItem.item.prevIndex = dragItem.index
	
	if hoverElement then
		hoverElement.item.prevPID = hoverElement.pid
		hoverElement.item.prevIndex = hoverElement.index
	end

	if hoverElement == nil then
		if m_dropIntoWorld == false then
			table.insert(itemsToAdd, {pid = dragItem.pid, index = dragItem.index, item = dragItem.item})
		else
			sendCustomDropEvent(-1, dragItem.item, dragItem.pid, dragItem.index)
			dropItem(dragItem)
		end
	elseif hoverElement.dropOnly then
		customDropIndex = hoverElement.index
	elseif checkForSwap(dragItem, hoverElement) then
		table.insert(itemsToAdd, {pid = dragItem.pid, index = dragItem.index, item = hoverElement.item})
		table.insert(itemsToAdd, {pid = hoverElement.pid, index = hoverElement.index, item = dragItem.item})
		m_hoverElement.item = m_dragItem.item
	elseif dragItem.pid == hoverElement.pid and dragItem.index == hoverElement.index and m_originalItem == nil then		
		table.insert(itemsToAdd, {pid = dragItem.pid, index = dragItem.index, item = dragItem.item})
		m_hoverElement.item = m_dragItem.item
	elseif dragItem.item.UNID == hoverElement.item.UNID then
		hoverElement.item.lootSource = dragItem.item.lootSource
		
		local stackSize = hoverElement.item.properties.stackSize or DEFAULT_STACK_SIZE
		hoverElement.item.count = hoverElement.item.count + dragItem.item.count

		if stackSize and hoverElement.item.count > stackSize then
			local remainder = hoverElement.item.count - stackSize
			hoverElement.item.count = stackSize
			dragItem.item.count = remainder
			if m_originalItem then dragItem.item.count = dragItem.item.count + m_originalItem.item.count end
			table.insert(itemsToAdd, {pid = dragItem.pid, index = dragItem.index, item = dragItem.item})
		end
		table.insert(itemsToAdd, {pid = hoverElement.pid, index = hoverElement.index, item = hoverElement.item})
		if m_hoverElement then
			m_hoverElement.item = m_dragItem.item
		end
	else
		table.insert(itemsToAdd, {pid = hoverElement.pid, index = hoverElement.index, item = dragItem.item})
	end
	
	if (hoverElement) then
		sendCustomDropEvent(hoverElement.index, dragItem.item, dragItem.pid, dragItem.index)
	end
	
	m_originalItem = nil
	
	for i = 1, #itemsToAdd do
		itemsToAdd[i].item.properties.dropType = nil
		updateItem(itemsToAdd[i].index, itemsToAdd[i].pid, itemsToAdd[i].item)
	end
	
	m_dragItem = nil
	updateDirty()
end

function onRequestElement(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local elementEvent = KEP_EventCreate("DRAG_DROP_RETURN_ELEMENT")
	if m_dragItem and m_dragElement then
		KEP_EventEncodeString(elementEvent, tostring(true))
		elementEvent = compileInventoryItem(m_dragItem.item, elementEvent)
	else
		KEP_EventEncodeString(elementEvent, tostring(false))
	end
	
	KEP_EventSetFilter(elementEvent, 4)
	KEP_EventQueue(elementEvent)
end

function onUpdateElement(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local item = {}
	item = decompileInventoryItem(tEvent)

	if m_dragItem and m_dragItem.item and item.properties and item.properties.GLID and item.UNID and item.properties.name and item.count then
		for k, v in pairs(item) do
			m_dragItem.item[k] = v
		end
		
		sendCreateDragElementEvent(m_dragItem.item)	
	end
end

function createDragElement(dragItem)
	local dragElement = MenuOpen(m_dragElementName)
	if dragElement then
		MenuSetLocation(m_dragElementName, m_mouseX - m_offsetX, m_mouseY - m_offsetY)
		sendCreateDragElementEvent(dragItem.item)
	end
	
	return dragElement
end

function setDragElementState(elementState)
	if elementState then
		m_dropIntoWorld = elementState == ELEMENT_STATES.DROP
		local stateEvent = KEP_EventCreate("DRAG_DROP_SET_ELEMENT_STATE")
		KEP_EventEncodeNumber(stateEvent, elementState)
		KEP_EventSetFilter(stateEvent, 4)
		KEP_EventQueue(stateEvent)		
	end
end

function sendCustomDropEvent(dropIndex, item, sourcePID, sourceIndex)
	local dropEvent = KEP_EventCreate("DRAG_DROP_CUSTOM_DROP")
	KEP_EventEncodeNumber(dropEvent, dropIndex)
	dropEvent = compileInventoryItem(item, dropEvent)
	KEP_EventEncodeNumber(dropEvent, sourcePID)
	KEP_EventEncodeNumber(dropEvent, sourceIndex)
	KEP_EventSetFilter(dropEvent, 4)
	KEP_EventQueue(dropEvent)
end

function checkForSwap(dragItem, hoverElement)
	if hoverElement.occupied or (tablePopulated(hoverElement.item.instanceData) or (hoverElement.item.properties.stackSize and hoverElement.item.properties.stackSize == 1)) or
	   (tablePopulated(dragItem.item.instanceData) or (dragItem.item.properties.stackSize and dragItem.item.properties.stackSize == 1)) then
		return true
	end
	
	return false
end

function dropItem(dragItem)
	
	
	if(dragItem.properties) then
		dragItem.properties.glow = false
	end
	
	if dragItem.item.properties.behavior and (dragItem.item.properties.behavior == "wheeled_vehicle" or dragItem.item.properties.behavior == "pet") then
		placeItemInWorld(dragItem)
		dragItem.item.count = dragItem.item.count - 1
		if m_dragItem.item.count > 0 then
			updateItem(dragItem.index, dragItem.pid, dragItem.item)
			
			m_dragItem = nil
			updateDirty()
		end
		return
	end

	local config = KEP_ConfigOpenWOK()
	local menuHidden
	if config ~= nil and config ~= 0 then
		
		 _, menuHidden =  KEP_ConfigGetNumber(config, "HideDropMenu"..tostring(m_playerName), 0)
	end

	if menuHidden and menuHidden == 1 then
		dragItem = dragItem.item
		local position = {}
		local rotation = 0
		position.x, position.y, position.z, rotation = KEP_GetPlayerPosition()
		rotation = (rotation * math.pi) / 180
		position.rx = math.sin(rotation)
		position.ry = 0
		position.rz = math.cos(rotation)
		position.x = position.x - (3 * position.rx)
		position.z = position.z - (3 * position.rz)

		local items = {}
		table.insert(items, {UNID = dragItem.UNID, count = dragItem.count, instanceData = dragItem.instanceData})

		--Code to make  sack instead look like  dropped item.
		--Accessories are not even close being ready
		-- local spawnInput = {items = items, despawnTime = 300, autoLoot = true}
		-- Events.sendEvent("OBJECT_SPAWN", {spawnerPID = 0, spawnType = "loot", GLID = dragItem.properties.GLID, location = position, spawnInput = spawnInput})
		local spawnInput = {items = items, despawnTime = 300}
		Events.sendEvent("OBJECT_SPAWN", {spawnerPID = 0, spawnType = "loot", GLID = 3234144, location = position, spawnInput = spawnInput})
	else
		MenuOpen("Framework_DropConfirm")
		local dropItemEvent = KEP_EventCreate("DRAG_DROP_CONFIRM_DROP")
		dropItemEvent = compileInventoryItem(dragItem.item, dropItemEvent)
		KEP_EventEncodeNumber(dropItemEvent, dragItem.pid)
		KEP_EventEncodeNumber(dropItemEvent, dragItem.index)
		KEP_EventSetFilter(dropItemEvent, 4)
		KEP_EventQueue(dropItemEvent)
	end
end

function sendCreateDragElementEvent(item)
	local createEvent = KEP_EventCreate("DRAG_DROP_CREATE_ELEMENT")
	createEvent = compileInventoryItem(item, createEvent)
	KEP_EventSetFilter(createEvent, 4)
	KEP_EventQueue(createEvent)
end

placeItemInWorld = function(dragItem)
	log("--- placeItemInWorld")
	dragItem = dragItem.item

	local position = {}
	local rotation = 0
	position.x, position.y, position.z, rotation = KEP_GetPlayerPosition()
	rotation = (rotation * math.pi) / 180
	position.rx = math.sin(rotation)
	position.ry = 0
	position.rz = math.cos(rotation)
	position.x = position.x - (10 * position.rx)
	position.z = position.z - (10 * position.rz)
	
	Events.sendEvent("FRAMEWORK_PLAYER_PLACE_ITEM", {GLID = dragItem.properties.GLID,
													 UNID = dragItem.UNID,
													 behavior = dragItem.properties.behavior,
													 x = position.x, y = position.y, z = position.z, rx = position.rx, ry = position.ry, rz = position.rz})
end