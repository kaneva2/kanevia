--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

ENTER_BUILD_MODE_EVENT = "EnterBuildModeEvent"

QUICK_MOVE_EVENT = "QuickMoveEvent"
BUILD_MODE_RESET_ITEM_PLACEMENT = "BuildModeItemResetEvent"
UNDO_ONCE = 1
UNDO_BUILD_EVENT = "UndoBuildEvent"
DISPLAY_RCLICK = "DisplayRightClickEvent"

g_minimized = false
g_helpDetailsMenu = nil

g_openBuildHelp = true
default_help_text = ""
g_t_btnHandles = {}
g_placementId = nil
g_objName = nil
g_creatorName = nil

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "quickMoveInfoHandler", QUICK_MOVE_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( BUILD_MODE_RESET_ITEM_PLACEMENT, KEP.MED_PRIO )
	KEP_EventRegister( DISPLAY_RCLICK, KEP.MED_PRIO )
	KEP_EventRegister("BuildKeyPressedEvent", KEP.MED_PRIO )
end

function quickMoveInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_placementId  = KEP_EventDecodeNumber( event )
	g_objName = KEP_EventDecodeString( event )
	g_creatorName = KEP_EventDecodeString( event )
	Static_SetText( gHandles["lblHeader1"], g_objName)
	Static_SetText( gHandles["lblUserName"], g_creatorName)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	getButtonHandles()
	default_help_text = Static_GetText(Dialog_GetControl(gDialogHandle, "lblStatus"))
end

function Dialog_OnDestroy(dialogHandle)
	DestroyMenu(g_helpDetailsMenu)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnMinMax_OnButtonClicked(buttonHandle)
	if g_minimized == false then
		minimize()
	else
		maximize()
	end
end

function minimize()
	g_minimized = true
	Dialog_SetAllControls(gDialogHandle, false, false)
	btnClose = Dialog_GetControl(gDialogHandle, "btnClose")
	Control_SetVisible(btnClose, true)
	Control_SetEnabled(btnClose, true)
	btnMinMax = Dialog_GetControl(gDialogHandle, "btnMinMax")
	Control_SetVisible(btnMinMax, true)
	Control_SetEnabled(btnMinMax, true)

	mUpperLeft_Slice = Dialog_GetControl(gDialogHandle, "mUpperLeft_Slice")
	Control_SetVisible(mUpperLeft_Slice, true)
	Control_SetEnabled(mUpperLeft_Slice, true)
	mUpperCenter_Slice = Dialog_GetControl(gDialogHandle, "mUpperCenter_Slice")
	Control_SetVisible(mUpperCenter_Slice, true)
	Control_SetEnabled(mUpperCenter_Slice, true)
	mUpperRight_Slice = Dialog_GetControl(gDialogHandle, "mUpperRight_Slice")
	Control_SetVisible(mUpperRight_Slice, true)
	Control_SetEnabled(mUpperRight_Slice, true)
	Menu_Title = Dialog_GetControl(gDialogHandle, "Menu_Title")
	Control_SetVisible(Menu_Title, true)
	Control_SetEnabled(Menu_Title, true)
end

function maximize()
	g_minimized = false
	Dialog_SetAllControls(gDialogHandle, true, true)
end

function btnHelp_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://docs.kaneva.com/mediawiki/index.php/Build_Mode")
end

-- DRF - Handled in Selection.lua
function sendKeyPressedEvent(key)
	local ev = KEP_EventCreate( "BuildKeyPressedEvent")
	KEP_EventSetFilter(ev, key)
	KEP_EventQueue( ev)
end

function btnUp_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.UP)
end

function btnDown_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.DOWN)
end

function btnLeft_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.LEFT)
end

function btnRight_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.RIGHT)
end

function btnHome_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.HOME)
end

function btnEnd_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.END)
end

function btnInsert_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.INSERT)
end

function btnDelete_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.DEL)
end

-- Counter-Clockwise
function btnPageUp_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.PAGE_UP)
end

-- Clockwise
function btnPageDown_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(Keyboard.PAGE_DOWN)
end

function btnClose_OnButtonClicked(buttonHandle)
	KEP_CancelDynamicObjectPlacementChanges( g_placementId )

	local ev = KEP_EventCreate( UNDO_BUILD_EVENT)
	KEP_EventSetFilter(ev, UNDO_ONCE)
	KEP_EventQueue( ev)

	local ev = KEP_EventCreate( ENTER_BUILD_MODE_EVENT)
	KEP_EventSetFilter(ev, 0)
	KEP_EventQueue( ev)

	KEP_EventCreateAndQueue( DISPLAY_RCLICK)
end

function btnCancel_OnButtonClicked(buttonHandle)
	KEP_CancelDynamicObjectPlacementChanges( g_placementId )

	local ev = KEP_EventCreate( UNDO_BUILD_EVENT)
	KEP_EventSetFilter(ev, UNDO_ONCE)
	KEP_EventQueue( ev)
end

function btnOK_OnButtonClicked(buttonHandle)
	local ev = KEP_EventCreate( ENTER_BUILD_MODE_EVENT)
	KEP_EventSetFilter(ev, 0)
	KEP_EventQueue( ev)

	KEP_EventCreateAndQueue( DISPLAY_RCLICK)
end

function getButtonHandles()
	local ch = Dialog_GetControl(gDialogHandle, "btnUp")
	if ch ~= nil then
		g_t_btnHandles["btnUp"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnDown")
	if ch ~= nil then
		g_t_btnHandles["btnDown"] = ch
	end

	local ch = Dialog_GetControl(gDialogHandle, "btnLeft")
	if ch ~= nil then
		g_t_btnHandles["btnLeft"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnRight")
	if ch ~= nil then
		g_t_btnHandles["btnRight"] = ch
	end

	local ch = Dialog_GetControl(gDialogHandle, "btnHome")
	if ch ~= nil then
		g_t_btnHandles["btnHome"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnEnd")
	if ch ~= nil then
		g_t_btnHandles["btnEnd"] = ch
	end

	local ch = Dialog_GetControl(gDialogHandle, "btnInsert")
	if ch ~= nil then
		g_t_btnHandles["btnInsert"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnDelete")
	if ch ~= nil then
		g_t_btnHandles["btnDelete"] = ch
	end

	local ch = Dialog_GetControl(gDialogHandle, "btnPageUp")
	if ch ~= nil then
		g_t_btnHandles["btnPageUp"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnPageDown")
	if ch ~= nil then
		g_t_btnHandles["btnPageDown"] = ch
	end
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	--Make sure we have the interface we need (stop errors in editor)
	if Control_ContainsPoint ~= nil then
		local sh = Dialog_GetStatic(dialogHandle, "lblStatus")
		Static_SetText(sh, default_help_text)
		if sh ~= nil then
			if Control_ContainsPoint(g_t_btnHandles["btnUp"], x, y) ~= 0
			and Control_GetVisible(g_t_btnHandles["btnUp"]) == 1 then
				Static_SetText(sh, "Move selected object(s) forward")
			elseif Control_ContainsPoint(g_t_btnHandles["btnDown"], x, y) ~= 0
			and Control_GetVisible(g_t_btnHandles["btnDown"]) == 1 then
				Static_SetText(sh, "Move selected object(s) backward")
			elseif Control_ContainsPoint(g_t_btnHandles["btnLeft"], x, y) ~= 0
			and Control_GetVisible(g_t_btnHandles["btnLeft"]) == 1 then
				Static_SetText(sh, "Move selected object(s) left")
			elseif Control_ContainsPoint(g_t_btnHandles["btnRight"], x, y) ~= 0
			and Control_GetVisible(g_t_btnHandles["btnRight"]) == 1 then
				Static_SetText(sh, "Move selected object(s) right")
			elseif Control_ContainsPoint(g_t_btnHandles["btnHome"], x, y) ~= 0
			and Control_GetVisible(g_t_btnHandles["btnHome"]) == 1 then
				Static_SetText(sh, "Raise selected object(s) height")
			elseif Control_ContainsPoint(g_t_btnHandles["btnEnd"], x, y) ~= 0
			and Control_GetVisible(g_t_btnHandles["btnEnd"]) == 1 then
				Static_SetText(sh, "Lower selected object(s) height")
			elseif Control_ContainsPoint(g_t_btnHandles["btnInsert"], x, y) ~= 0
			and Control_GetVisible(g_t_btnHandles["btnInsert"]) == 1 then
				Static_SetText(sh, "Rotate selected object(s) left")
			elseif Control_ContainsPoint(g_t_btnHandles["btnDelete"], x, y) ~= 0
			and Control_GetVisible(g_t_btnHandles["btnDelete"]) == 1 then
				Static_SetText(sh, "Rotate selected object(s) right")
			elseif Control_ContainsPoint(g_t_btnHandles["btnPageUp"], x, y) ~= 0
			and Control_GetVisible(g_t_btnHandles["btnPageUp"]) == 1 then
				Static_SetText(sh, "Rotate selected object(s) left")
			elseif Control_ContainsPoint(g_t_btnHandles["btnPageDown"], x, y) ~= 0
			and Control_GetVisible(g_t_btnHandles["btnPageDown"]) == 1 then
				Static_SetText(sh, "Rotate selected object(s) right")
			end
		end
	end
end
