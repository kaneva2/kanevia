--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Vendor.lua
--
-- Displays available items for vending (by the ashmeister)
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_DragDropHelper.lua")
dofile("Easing.lua")
dofile("Framework_VendorHelper.lua")
dofile("Framework_ActorHelper.lua")
----------------------
-- Local Constants
----------------------
local MENU_OFFSET = 8 -- Menu offset from edge of screen

ITEMS_PER_PAGE = 4
VENDOR_ITEMS_PER_PAGE = 1




g_displayTable = {}	-- items being displayed :: [{UNID, properties={name, GLID, itemType}, count}]
g_trades = {} -- output, input, and cost :: [{output={UNID, properties={name, GLID, itemType}, count}, input={UNID, properties={name, GLID, itemType}, count}, cost}]

----------------------
-- Local Variables
----------------------
local m_displaying = false


local m_playerName = ""
m_currTrade = 1
m_sellFlashing = false
m_dropFlashed = false
m_sellFlashed = false
m_sellFlashTime = 0
m_dropFlashTime = 0
m_sellItem = {}
m_sellItemOutput = {}
m_sellItemOutputCount = 0
m_sellIndex = 0
m_sellItemOrigIndex = 0
m_sellItemPID = 0
m_overButton = false
m_validTrade = false
m_vendorPID = nil

m_requestHandle = nil
m_pendingTrade = false

m_page = 1
m_selectedPage = 1
m_playerInventoryByUNID = {}
m_inventory = {}			-- [{UNID, count, name, itemType}]
m_inventoryCounts = {}	-- {UNID:count}
m_inputLocations = {}

m_gameItemNamesByUNID = {}

----------------------
-- Local Functions
----------------------

local initalizeClickToTrade -- (numButtons)

local buyItem -- ()
----------------------------
-- Create Method
----------------------------
function onCreate()
	CloseAllActorMenus()
	KEP_EventRegister("CLOSE_VENDOR", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("updateVendorClientFull", "UPDATE_VENDOR_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCloseMenu", "CLOSE_VENDOR", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onReturnElement", "DRAG_DROP_RETURN_ELEMENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCustomDrop", "DRAG_DROP_CUSTOM_DROP", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onDropConfirmSell", "DRAG_DROP_CONFIRM_SELL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("startDragDrop", "DRAG_DROP_START", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCheckFullResponse", "INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("returnVendorItems", "FRAMEWORK_REQUEST_VENDOR_ITEMS", KEP.HIGH_PRIO)


	
	Events.registerHandler("FRAMEWORK_CLOSE_VENDOR", onCloseMenu)
	Events.registerHandler("UPDATE_VENDOR_HEADER", updateHeader)

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()
	InventoryHelper.initializeInventory("g_displayTable", true, ITEMS_PER_PAGE)
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")
	
	-- Request player's inventory
	requestInventory(INVENTORY_PID)
	--getInputLocations()

	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")				
	end

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "player")
	KEP_EventQueue(ev)

	m_playerName = KEP_GetLoginName()

	DragDrop.initializeDragDrop(0, 1, 0, INVENTORY_PID)

	for itemNumber=1, ITEMS_PER_PAGE do
		if gHandles["stcItemCount" .. itemNumber] and gHandles["imgItemCountBG" .. itemNumber] then
			local quantityLabel = gHandles["stcItemCount" .. itemNumber]

			Control_SetSize(gHandles["imgItemCountBG" .. itemNumber], Control_GetWidth(gHandles["imgItemCountBG" .. itemNumber]) + 5, Control_GetHeight(gHandles["imgItemCountBG" .. itemNumber]))

		end
	end

	m_lastPosition = MenuGetLocationThis()
	MenuSetLocationThis(m_lastPosition.x + MENU_OFFSET, m_lastPosition.y)

	setSellEnabledVisible(false)

end

function onDestroy()
	if m_sellItem.UNID and m_sellItem.UNID ~= 0 and m_sellItem.UNID then
		returnItem()
	end

	InventoryHelper.destroy()

	-- notify the backpack that it is closed 
	KEP_EventCreateAndQueue("FRAMEWORK_REMOVE_VENDOR_ITEMS")
	Events.sendEvent("FRAMEWORK_CLOSED_VENDOR", {PID = m_vendorPID})
end

-- Called each frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_validTrade then
		if m_pendingTrade and Control_GetEnabled(gHandles["btnBuy"]) == 1 then
			Control_SetEnabled(gHandles["btnBuy"], not m_pendingTrade)
		elseif not m_pendingTrade and Control_GetEnabled(gHandles["btnBuy"]) == 0 then
			Control_SetEnabled(gHandles["btnBuy"], not m_pendingTrade)
		end
	end

	if m_sellFlashing and m_sellFlashed then
		m_sellFlashTime = m_sellFlashTime - fElapsedTime

		-- Are we still animating?
		if m_sellFlashTime > 0 then
			-- Position of flash accounts for size of the bar + the flash size
			local flashX = Easing.outQuint(SELL_FLASH_TIME - m_sellFlashTime, 1, 67, SELL_FLASH_TIME)
			local flashColor = Easing.linear(SELL_FLASH_TIME - m_sellFlashTime, 0, 255, SELL_FLASH_TIME)
			setSellColorAlpha(flashColor)
		-- Done flashing
		else
			setSellColorAlpha(255)
			m_sellFlashing = false
			setSellEnabledVisible(true)
		end
	end
end
----------------------------
-- Event Handlers
----------------------------


function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	m_pendingTrade = false
	if success then
		--m_sellItem = {}
		closeSell()
	end
end

function onCheckFullResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local lootValid = (tostring(KEP_EventDecodeString(tEvent)) == "true")

	-- Enable / Disable purchasing random loot if the backpack is full
	if lootValid and m_sellItem and m_sellItem.UNID then
		local addTable = {}
		local newItem = {}
		newItem.UNID = m_sellItemOutput.output
		newItem.properties = m_sellItemOutput.outputProperties
		newItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "Vendor"}
		newItem.count = m_sellItemOutputCount
		table.insert(addTable, newItem)
		
		m_pendingTrade = true
		m_sellItem = {}
		processTransaction({}, addTable)
	else
		displayStatusMessage(INVENTORY_ERRORS.BACKPACK_FULL)
	end
end

-- Called when a new container inventory is received.  Updates all items and re-displays.
function updateVendorClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local returnedPID = KEP_EventDecodeNumber(tEvent)
	if m_vendorPID == nil then
		m_vendorPID = returnedPID
	elseif m_vendorPID ~= returnedPID then
		return
	end
	
	g_trades = decompileInventory(tEvent)

	updateInventory()
	updateItemContainer()
	m_currTrade = 1
	updateTrade()

	returnVendorItems()
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if m_inventory[updateIndex] then
		m_inventory[updateIndex] = updateItem
		m_playerInventoryByUNID[updateItem.UNID] = updateItem.count
		updateInventory()
		updateItemContainer()
		if m_currTrade ~= 0 then
			setCounts()
		end
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_inventory = decompileInventory(tEvent)
	
	for i = 1, #m_inventory do
		m_playerInventoryByUNID[m_inventory[i].UNID] = m_inventory[i].count
	end

	updateInventory()
	updateItemContainer()
	setCounts()
end

function returnVendorItems(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local returnEvent = KEP_EventCreate("FRAMEWORK_RETURN_VENDOR_ITEMS")
	local inventoryString = Events.encode(g_trades)
	
	if inventoryString then
		KEP_EventEncodeString(returnEvent, inventoryString)
		KEP_EventQueue(returnEvent)
	end
end

----------------------------
-- Local Functions
----------------------------

Dialog_OnLButtonDownInside = function(dialogHandle, x, y)
	local tradeClicked = false
	
	-- If trade section active, don't close when clicking within this section

	if (Control_ContainsPoint(gHandles["btnBuy"], x, y) == 1) and Control_GetEnabled(gHandles["btnSell"]) ~= 1 then
		local transactionID = KEP_GetCurrentEpochTime()
		-- Create new item purchased
		local removeTable = {}
		local removeDetails = {UNID = g_trades[1].input, count = g_trades[1].cost or 0}
		removeDetails.lootInfo = {sourcePID = m_vendorPID, lootSource = "Vendor", sourceSlot = 1, transactionID = transactionID}
		table.insert(removeTable, removeDetails)
		
		local addTable = {}
		local newItem = {UNID = g_trades[1].output, count = g_trades[1].outputCount, properties = g_trades[1].outputProperties}

		-- Assign a loot source for metric detection
		newItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "Vendor", transactionID = transactionID}
		newItem.count = g_trades[1].outputCount or 1
		table.insert(addTable, newItem)
		
		m_pendingTrade = true
		processTransaction(removeTable, addTable)
	end

	if (Control_ContainsPoint(gHandles["btnSell"], x, y) == 1) and Control_GetEnabled(gHandles["btnSell"]) == 1 then
		-- Create new item purchased
		-- Create new item purchased
		local addTable = {}
		local newItem = {}
		newItem.UNID = m_sellItemOutput.output
		newItem.properties = m_sellItemOutput.outputProperties
		newItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "Vendor"}
		newItem.count = m_sellItemOutputCount
		table.insert(addTable, newItem)
		
		m_pendingTrade = true
		processTransaction({}, addTable, true)
	end

	if (Control_ContainsPoint(gHandles["btnCancel"], x, y) == 1) and Control_GetEnabled(gHandles["btnCancel"]) == 1 then
		returnItem()
		closeSell()
	end
	
	if (Control_ContainsPoint(gHandles["btnOk"], x, y) == 1) and Control_GetEnabled(gHandles["btnOk"]) == 1 then
		Control_SetVisible(gHandles["txtNotBuy"], false)
		Control_SetEnabled(gHandles["txtNotBuy"], false)
		Control_SetVisible(gHandles["btnOk"], false)
		Control_SetEnabled(gHandles["btnOk"], false)

		closeSell()
	end

	-- If trade section active, don't close when clicking within this section
	if (Control_ContainsPoint(gHandles["imgPurchaseBG"], x, y) == 1) or (Control_ContainsPoint(gHandles["btnItemDrop1"], x, y) == 1) then
		if m_currTrade ~= 0 then
			tradeClicked = true
		end
	end

end

updateTrade = function ()

	g_displayTable[1] = copyTable(g_trades[1], true)

	g_displayTable[2] = copyTable(g_trades[1], false)
	updateItemContainer()

	-- Set input/output tooltip, image, nameplate, and cost.
	Control_SetVisible(gHandles["txtSell"], false)
	Control_SetVisible(gHandles["txtNotBuy"], false)
	Control_SetVisible(gHandles["btnOk"], false)
	Control_SetVisible(gHandles["btnItemDrop1"], false)
end
