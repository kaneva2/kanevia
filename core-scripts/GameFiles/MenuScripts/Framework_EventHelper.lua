--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_EventHelper.lua
--
-- Helps non-Framework-enabled MenuScripts get events down to the server
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

local encodeTable -- (input, event)

-- Framework object
Framework = {}

-- Packs an event up to be passed to Framework_EventBroker
function Framework.sendEvent(eventName, eventData)
	if eventName == nil then
		LogWarn("Framework.sendEvent() eventName is nil"); return
	end
	if type(eventName) ~= "string" then
		LogWarn("Framework.sendEvent() eventName is of invalid type. Input["..tostring(eventName).."]"); return
	end
	if eventData == nil then
		LogWarn("Framework.sendEvent() eventData is nil"); return
	end
	if type(eventData) ~= "table" then
		LogWarn("Framework.sendEvent() eventData must be a table. Input["..tostring(eventData).."]"); return
	end
	
	-- Compile event for Framework_EventBroker
	local event = KEP_EventCreate("FrameworkIntendedEvent")
	KEP_EventEncodeString(event, eventName)
	KEP_EventEncodeString(event, JSON.encode(eventData))
	KEP_EventQueue(event)
end
