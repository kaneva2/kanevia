--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_CursorHighlightHandler.lua
--
-- Client-side custom cursor and highlight handler
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\Framework_FlagHelper.lua")

------------------------------------------------------------------------------------------
-- Helper references
------------------------------------------------------------------------------------------
local FlagSystem = _G.FlagSystem	-- Assign the global FlagSystem to a local variable for faster access

------------------------------------------------------------------------------------------
-- Constant definitions
------------------------------------------------------------------------------------------
local PROXIMITY_HIGHLIGHT	= {1, 1, 0}
local LOOT_HIGHLIGHT		= {1.0, 0.7, 0, 0.3}	--> R, G, B, Strength [0.0 - 1.0]
local TINT_COLOR 			= {1.0, 0.7, 0, 0.45}

-- Highlight constants
local MIN_INTERACT_DISTANCE = 90000 -- How far away can the player interact with objects? (squared for cheaper comparison point)

local HIGHLIGHT_COLOR = {	
	OWNED = {
		SELECTED	= {0.3, 0.3, 1},
		MOUSEOVER	= {0, 0, 0.5},
		ASSOCIATED	= {0.5, 0, 0.5},
		DISABLED	= {1, 0, 0}
	},
	INTERACTIVE = {	
		IN_RANGE		= {0, 1, 0},
		OUT_OF_RANGE	= {0, 0.3, 0}
	}
}

-- 
local ICON_OBJECT_FILTER	= 3
local ICON_SPEED			= 0.2
local ICON_TRAVEL			= 0.2
local ICON_OFFSET_X			= 0
local ICON_OFFSET_Y			= 0.6
local ICON_OFFSET_Z			= 0
local ICON_GLID				= 4673860

local NET_ID_DELAY			 = .5 -- Time to wait after a player arrival/departure before updating netIds

------------------------------------------------------------------------------------------
-- Static variables with file scope
------------------------------------------------------------------------------------------
local s_tWorldSettings 				= {}

-- Highlight vars
local s_sPlayerName
local s_bIsInHandMode
-- Registered objects
local s_tRegisteredObjects			= {}
local s_tObjectAssociations			= {}  -- object associations table from inventory controller
local s_tAssociatedHighlights		= {}

local s_tProximityTriggers			= {}
local s_tHighlightedLootObjects		= {}

-- What object is currently being moused over
local s_sMousedOverObjectID
local s_iHighlightedObjectID
local s_sSelectedObjectID


-- Cursor vars
local s_bIsWeaponEquipped			= false
local s_bDoesWeaponHeal				= false
local s_bDoesWeaponPaint			= false
local s_bDoesWeaponRepair			= false
local s_bDoesWeaponEmote			= false
local s_bDoesWeaponRequireTarget	= false
local s_bIsWeaponGeneric			= false

local s_bIsPlayerListUpdatePending	= false
local s_dNetIDTimer					= 0
local s_tPendingPlayers				= {}

--Object Highlights
local s_bIsProxyGlowEnabled			= true

local s_tOccupiedVehicles			= {}

------------------------------------------------------------------------------------------
-- Local function declarations
------------------------------------------------------------------------------------------

-- Highlight functions
local setHighlight -- (PID)
local highlightAssociatedObjects -- (PID, select)
local getObjectDistanceSquared -- (PID)
local checkFlagLocation --(targetObject)

-- Cursor functions
local setMouseOver -- (ID)
local setMouseOverMode -- ()
local getPlayerNameByNetID -- (netId)
local getPlayerObjectFromNetId -- (netId)


------------------------------------------------------------------------------------------
-- Framework function definitions
------------------------------------------------------------------------------------------

-- Called when the menu is created
function onCreate()
	-- NOTE: Framework_BattleHandler will request battle objects and we'll piggy-back the return here
	-- Register for targetable entitty event
	Events.registerHandler("FRAMEWORK_REGISTER_BATTLE_OBJECT_LIST", registerBattleObjectListHandler)
	Events.registerHandler("FRAMEWORK_REGISTER_BATTLE_OBJECT", registerBattleObjectHandler)
	Events.registerHandler("FRAMEWORK_UNREGISTER_BATTLE_OBJECT", unregisterBattleObjectHandler)
	
	-- Client copy of object association table from inventory controller
	Events.registerHandler("FRAMEWORK_CLIENT_UPDATE_OBJECT_ASSOCIATIONS", onUpdateObjectAssociations)
	Events.registerHandler("FRAMEWORK_MODIFY_BATTLE_OBJECT", modifyBattleObjectHandler)
	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS", returnSettingsHandler)
	Events.registerHandler("FRAMEWORK_RETURN_VEHICLE_OCCUPIED", onOccupedVehicleHandler)

	--Regiester for flag updates
	KEP_EventRegisterHandler( "onFlagsReturnedFull", "UPDATE_FLAGS_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onFlagReturned", "UPDATE_FLAG_CLIENT", KEP.MED_PRIO)
	
	Events.registerHandler("FRAMEWORK_UPDATE_TIMED_STATE", onTimedStateUpdated)

	 -- Get message whenever a player leaves or enters the world
	Events.registerHandler("FRAMEWORK_PLAYER_LIST_UPDATE", playerListUpdateHandler)
	
	
	-- Register for MouseOver events
	KEP_EventRegisterHandler("mouseOverEventHandler", "MouseOverEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("handModeHandler", "FRAMEWORK_BUILD_HAND_MODE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("setSelectedObjectHandler", "FRAMEWORK_SET_SELECTED_OBJECT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("objectOffendingFlagHandler", "FRAMEWORK_OBJECT_OFFENDING_FLAG", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("weaponEquippedHandler", "FRAMEWORK_WEAPON_EQUIPPED", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler( "clientTriggerHandler", "TriggerEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "onHighlightablesUpdated", "FRAMEWORK_RETURN_HIGHLIGHTABLES", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "onTimedVendorUpdated", "FRAMEWORK_UPDATE_TIMED_VENDOR", KEP.HIGH_PRIO )

	KEP_EventRegisterHandler("hideObjectGlowHandler", "HideObjectGlowSettingEvent", KEP.HIGH_PRIO)
	
	-- Get an update of all highlightables
	KEP_EventCreateAndQueue("FRAMEWORK_REQUEST_HIGHLIGHTABLES")
	
	-- Get player's name
	s_sPlayerName = KEP_GetLoginName()

	--Get if objects should glow 
	s_bIsProxyGlowEnabled = not GetHideGlowSetting()
	
	-- Get object associations
	Events.sendEvent("FRAMEWORK_CLIENT_REQUEST_OBJECT_ASSOCIATIONS")
	
	-- Initialise the flag system
	FlagSystem:create()
	updateNetIds()
end

-- Called when the menu is destroyed (when changing to Creator mode)
function onDestroy()
	if s_iHighlightedObjectID then
		KEP_SetOutlineState(ObjectType.DYNAMIC, s_iHighlightedObjectID, 0)
		KEP_SetOutlineZSorted(ObjectType.DYNAMIC, s_iHighlightedObjectID, 0)
	end
	
	for PID, value in pairs(s_tProximityTriggers) do
		KEP_SetOutlineState(ObjectType.DYNAMIC, PID, 0)
		KEP_SetOutlineZSorted(ObjectType.DYNAMIC, PID, 0)
	end
	
	for PID, value in pairs(s_tHighlightedLootObjects) do
		KEP_SetOutlineState(ObjectType.DYNAMIC, PID, 0)
		KEP_SetObjectHighlight(PID, 0)
		KEP_SetOutlineZSorted(ObjectType.DYNAMIC, PID, 0)
	end
	
	-- Clear mouseover settings
	KEP_ClearMouseOverSettings()
	
	-- Set the world cursor mode to default (mouse pointer)
	KEP_SetCursorModeType(CURSOR_MODE_WORLD, CURSOR_TYPE_DEFAULT)
end

-- On dialog render
function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- Update pending netIds when ready
	if s_bIsPlayerListUpdatePending == true then
		if s_dNetIDTimer > 0 then
			s_dNetIDTimer = s_dNetIDTimer - fElapsedTime
		else -- Timer is done
			local tryAgain = false
			local updateDirty = false
			for i, name in pairs(s_tPendingPlayers) do
				if checkForPlayer(name) == true then
					updateDirty = true
					table.remove(s_tPendingPlayers, i)
				else
					tryAgain = true
				end
			end
			-- If at least one player is ready, update netIds			
			if updateDirty then
				updateNetIds()
			end
			-- If at least one player is NOT ready, keep trying
			if tryAgain then
				s_dNetIDTimer = NET_ID_DELAY
				s_bIsPlayerListUpdatePending = true
			else
				s_dNetIDTimer = 0
				s_bIsPlayerListUpdatePending = false
			end
		end
	end
end

-- -- -- -- -- -- --
-- Client Event Handlers
-- -- -- -- -- -- --

function playerListUpdateHandler(event) -- Register netIDs here
	-- log("\nPlayer "..tostring(event.playerName).." arriving = "..tostring(event.arriving == 1))
	local arriving = (event.arriving == 1)
	local playerRegistered = checkForPlayer(event.playerName)
	
	if arriving then
		if playerRegistered then
			updateNetIds()
		else
			table.insert(s_tPendingPlayers, event.playerName)
			s_bIsPlayerListUpdatePending = true
			s_dNetIDTimer = NET_ID_DELAY
		end
	else -- Player is leaving
		updateNetIds()
	end
end

function clientTriggerHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter ~= 0) then return end
	local action  = KEP_EventDecodeNumber( event )
	
	--enter trigger
	if (action == 1)  then
		if not KEP_DynamicObjectGetVisibility(objectid) then log("Object not visibile!") return end
		enableProximityHighlightForObject(objectid, true)
	elseif (action == 2) then
		enableProximityHighlightForObject(objectid, false)
	end
end

function onHighlightablesUpdated(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- Update proximity triggers that need highlighting
	s_tProximityTriggers = Events.decode(KEP_EventDecodeString(event))
	for PID, value in pairs(s_tProximityTriggers) do
		enableProximityHighlightForObject(PID, true)
	end
	
	-- Update lootable objects that need highlighting
	s_tHighlightedLootObjects = Events.decode(KEP_EventDecodeString(event))
	for PID, value in pairs(s_tHighlightedLootObjects) do
		enableLootHighlightForObject(PID, true)
	end
end

function onTimedVendorUpdated(dispatcher, fromNetid, event, eventid, filter, objectid)
	local PID = KEP_EventDecodeString(event)
	local enabled = KEP_EventDecodeNumber(event) == 1
	enableLootHighlightForObject(PID, enabled)
end

function hideObjectGlowHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local checked = Event_DecodeNumber(event)
	s_bIsProxyGlowEnabled = checked ~= 1

	for PID, value in pairs(s_tProximityTriggers) do
		if value then
			enableProximityHighlightForObject(PID, s_bIsProxyGlowEnabled, true)
		end
	end
end

-- Handles mouse over events
function mouseOverEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	objectId = tostring(objectId)
	-- If a player was moused over
	if filter == ObjectType.PLAYER then
		local netID = objectId
		objectId = getPlayerNameByNetID(objectId)
		if s_tRegisteredObjects[objectId] then
			s_tRegisteredObjects[objectId].netId = tonumber(netID)
		end
	end
	local prevMouseOver = s_sMousedOverObjectID
	-- Handle highlighting
	-- If we have SOMETHING highlighted and we just moused off what we're currently mousing over
	if s_iHighlightedObjectID and objectId ~= prevMouseOver and #s_tAssociatedHighlights> 0 then
		-- Make sure that object wasn't associated with the highlighted PID
		for i, v in ipairs(s_tAssociatedHighlights) do
			local associatedPID = tonumber(v)
			if tonumber(prevMouseOver) == associatedPID then
				KEP_SetOutlineState(ObjectType.DYNAMIC, associatedPID, 1)
				KEP_SetOutlineColor(ObjectType.DYNAMIC, associatedPID, unpack(HIGHLIGHT_COLOR.OWNED.ASSOCIATED))
				KEP_SetOutlineZSorted(ObjectType.DYNAMIC, associatedPID, 1)
			end
		end
	-- If we just moused off a highlighted object
	elseif (objectId ~= s_iHighlightedObjectID) and (prevMouseOver == s_iHighlightedObjectID) and (prevMouseOver ~= s_sSelectedObjectID) then
		if isProximityHighlightOn(s_iHighlightedObjectID) and s_bIsProxyGlowEnabled then
			KEP_SetOutlineState(ObjectType.DYNAMIC, s_iHighlightedObjectID, 1)
			KEP_SetOutlineColor(ObjectType.DYNAMIC, s_iHighlightedObjectID, unpack(PROXIMITY_HIGHLIGHT))
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, s_iHighlightedObjectID, 1)
		-- elseif isLootHighlightOn(s_iHighlightedObjectID) then
		-- 	KEP_SetOutlineState(ObjectType.DYNAMIC, s_iHighlightedObjectID, 1)
		-- 	KEP_SetOutlineColor(ObjectType.DYNAMIC, s_iHighlightedObjectID, unpack(LOOT_HIGHLIGHT))
		-- 	KEP_SetOutlineZSorted(ObjectType.DYNAMIC, s_iHighlightedObjectID, 1)
		else
			KEP_SetOutlineState(ObjectType.DYNAMIC, s_iHighlightedObjectID, 0)
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, s_iHighlightedObjectID, 0)
		end
		s_iHighlightedObjectID = nil
	end
	if filter == ObjectType.DYNAMIC then
		-- If this object has already been highlighted and is still moused over, update highlight based on distance
		if (prevMouseOver == s_iHighlightedObjectID) and (objectId == s_iHighlightedObjectID) and (objectId ~= s_sSelectedObjectID) then
			setHighlight(s_iHighlightedObjectID)
		-- If this object isn't currently build selected and is registered, highlight it
		elseif (objectId ~= s_sSelectedObjectID) and s_tRegisteredObjects[objectId] then
			setHighlight(objectId)
		end
	end
	
	-- Does this object need it's cursor set?
	if s_tRegisteredObjects[objectId] and (s_tRegisteredObjects[objectId].cursorSet ~= true) then
		setMouseOver(objectId)
	end
	
	-- send client event containing s_tRegisteredObjects[objectId].behavior
	local behaviorEvent = KEP_EventCreate("ITEM_MOUSE_OVER_BEHAVIOR")
	if s_tRegisteredObjects[objectId] and tostring(s_tRegisteredObjects[objectId].behavior) then
		KEP_EventEncodeString(behaviorEvent, tostring(s_tRegisteredObjects[objectId].behavior))
	else
		KEP_EventEncodeString(behaviorEvent, "")
	end 
	KEP_EventQueue(behaviorEvent)

	s_sMousedOverObjectID = objectId
end

-- Enter or exit hand mode
function handModeHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	s_bIsInHandMode = KEP_EventDecodeString(event) == "true"
	
	-- Leaving hand mode
	if not s_bIsInHandMode then
		if s_sSelectedObjectID then
			KEP_SetOutlineState(ObjectType.DYNAMIC, s_sSelectedObjectID, 0)
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, s_sSelectedObjectID, 0)
			s_sSelectedObjectID = nil
			-- Clean up associated object highlights if needed
			if #s_tAssociatedHighlights > 0 then
				for _, PID in pairs(s_tAssociatedHighlights) do
					KEP_SetOutlineState(ObjectType.DYNAMIC, PID, 0)
					KEP_SetOutlineZSorted(ObjectType.DYNAMIC, PID, 0)
				end
				s_tAssociatedHighlights = {}
			end
		end
	end
end

-- Object selected in PlayerBuild mode
function setSelectedObjectHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local selectedObject = tostring(KEP_EventDecodeNumber(event))
	if selectedObject ~= "0" then
		-- Select this object if we're inside minimum interaction range
		local distToObject = getObjectDistanceSquared(selectedObject)
		-- Are we withing minimum interaction range?
		if distToObject <= MIN_INTERACT_DISTANCE then
			KEP_SetOutlineState(ObjectType.DYNAMIC, selectedObject, 1)
			KEP_SetOutlineColor(ObjectType.DYNAMIC, selectedObject, unpack(HIGHLIGHT_COLOR.OWNED.SELECTED))
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, selectedObject, 0)
			
			s_sSelectedObjectID = selectedObject
			highlightAssociatedObjects(s_sSelectedObjectID, true)
		end
	else
		if isProximityHighlightOn(s_sSelectedObjectID) then
			KEP_SetOutlineState(ObjectType.DYNAMIC, s_sSelectedObjectID, 1)
			KEP_SetOutlineColor(ObjectType.DYNAMIC, s_sSelectedObjectID, unpack(PROXIMITY_HIGHLIGHT))
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, s_sSelectedObjectID, 1)
		-- elseif isLootHighlightOn(s_sSelectedObjectID) then
		-- 	KEP_SetOutlineState(ObjectType.DYNAMIC, s_sSelectedObjectID, 1)
		-- 	KEP_SetOutlineColor(ObjectType.DYNAMIC, s_sSelectedObjectID, unpack(LOOT_HIGHLIGHT))
		-- 	KEP_SetOutlineZSorted(ObjectType.DYNAMIC, s_sSelectedObjectID, 1)
		else
			KEP_SetOutlineState(ObjectType.DYNAMIC, s_sSelectedObjectID, 0)
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, s_sSelectedObjectID, 0)
		end
		highlightAssociatedObjects(s_sSelectedObjectID, false)
		s_sSelectedObjectID = nil
	end
end

-- An object is offending the flag
function objectOffendingFlagHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local PID 		= KEP_EventDecodeNumber(event)
	local offending = KEP_EventDecodeString(event) == "true"

	-- If this object currently build selected?
	if tonumber(PID) == tonumber(s_sSelectedObjectID) then
		-- Highlight this object red
		if offending then
			KEP_SetOutlineState(ObjectType.DYNAMIC, s_sSelectedObjectID, 1)
			KEP_SetOutlineColor(ObjectType.DYNAMIC, s_sSelectedObjectID, unpack(HIGHLIGHT_COLOR.OWNED.SELECTED))
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, s_sSelectedObjectID, 0)
		-- Highlight this object default build select
		else
			KEP_SetOutlineState(ObjectType.DYNAMIC, s_sSelectedObjectID, 1)
			KEP_SetOutlineColor(ObjectType.DYNAMIC, s_sSelectedObjectID, unpack(HIGHLIGHT_COLOR.OWNED.DISABLED))
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, s_sSelectedObjectID, 0)
		end
	end
end

-- Called when a weapon is equipped
function weaponEquippedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- Better safe than sorry
	local eventOutput
	if KEP_EventMoreToDecode(event) ~= 0 then
		eventOutput = Events.decode(KEP_EventDecodeString(event))
	end
	
	s_bIsWeaponEquipped = eventOutput.equipped and (eventOutput.equipped == true)
	s_bDoesWeaponHeal = false
	if eventOutput.weapon and eventOutput.weapon.properties then
		s_bDoesWeaponHeal = eventOutput.weapon.properties.healingWeapon == "true"
	end
	s_bDoesWeaponRepair = false
	if eventOutput.weapon and eventOutput.weapon.properties then
		s_bDoesWeaponRepair = tostring(eventOutput.weapon.properties.repairTool) == "true" or false
	end
	s_bDoesWeaponPaint = false
	if eventOutput.weapon and eventOutput.weapon.properties then
		s_bDoesWeaponPaint = eventOutput.weapon.properties.paintWeapon == "true"
	end
	s_bDoesWeaponEmote = false
	if eventOutput.weapon and eventOutput.weapon.properties then
		s_bDoesWeaponEmote = eventOutput.weapon.properties.toolType == "p2pEmoteItem"
	end
	s_bIsWeaponGeneric = false
	if eventOutput.weapon and eventOutput.weapon.properties then
		s_bIsWeaponGeneric = eventOutput.weapon.properties.itemType == "generic"
	end
	-- Does the equipped item require a target?
	if eventOutput.weapon and eventOutput.weapon.properties and eventOutput.weapon.properties.requiresTarget then
		if eventOutput.weapon.properties.requiresTarget == "false" then
			s_bDoesWeaponRequireTarget = false
		else
			s_bDoesWeaponRequireTarget = true
		end
	else
		s_bDoesWeaponRequireTarget = true
	end
	
	-- Set cursor modes accordingly
	if s_bIsWeaponEquipped and s_bDoesWeaponRequireTarget and not s_bIsWeaponGeneric then
		KEP_SetCursorModeType(CURSOR_MODE_WORLD, CURSOR_TYPE_ATTACKABLE)
	else
		KEP_SetCursorModeType(CURSOR_MODE_WORLD, CURSOR_TYPE_DEFAULT)
	end
	
	setMouseOverMode()
end

-- -- -- -- -- -- --
-- Server Event Handlers
-- -- -- -- -- -- --

-- Handles the battle object list event from the server
function registerBattleObjectListHandler(event)
	-- Objects
	if event.objects then
		-- Extract all targetable objects
		for i, tObject in ipairs(event.objects) do
			tObject.type = "Object"
			registerBattleObjectHandler(tObject)
		end
	end
	
	-- Players
	if event.players then
		-- Extract all targetable players
		for i, tPlayer in ipairs(event.players) do
			tPlayer.type = "Player"
			registerBattleObjectHandler(tPlayer)
		end
	end
end

-- Retrieves new battle object from the server
function registerBattleObjectHandler(event)
	local type  	  = event.type
	local ID 		  = tostring(event.ID)
	local alive 	  = event.alive
	local itemType	  = event.itemType
	local health 	  = event.health
	local maxHealth   = event.maxHealth
	local targetable  = event.targetable
	
	-- New object
	if type == "Object" then
		local owner				 = event.owner
		local tooltip			 = event.tooltip
		local level				 = event.level
		local isLoot			 = event.isLoot
		local master			 = event.master
		local behavior			 = event.behavior
		local interactionRange 	 = event.interactionRange
		local spawned			 = event.spawned
		
		-- Update as many fields as were passed
		local tObject = s_tRegisteredObjects[ID]
		if tObject == nil then
			tObject = {
				type		= ObjectType.DYNAMIC,
				cursorSet	= false
			}
			s_tRegisteredObjects[ID] = tObject
		end
		
		if alive 			~= nil then tObject.alive 			 = alive end
		if itemType 		~= nil then tObject.itemType 		 = itemType end
		if owner 			~= nil then tObject.owner 			 = owner end
		if tooltip 			~= nil then tObject.tooltip 		 = tooltip end
		if health 			~= nil then tObject.health 			 = health end
		if maxHealth 		~= nil then tObject.maxHealth 		 = maxHealth end
		if targetable 		~= nil then tObject.targetable 		 = targetable end
		if level 			~= nil then tObject.level 			 = level end
		if isLoot 			~= nil then tObject.isLoot 			 = isLoot end
		if master 			~= nil then tObject.master 			 = master end
		if behavior 		~= nil then tObject.behavior 		 = behavior end
		if interactionRange ~= nil then tObject.interactionRange = interactionRange end
		if spawned	 	 	~= nil then tObject.spawned   		 = spawned end
		
	-- New Player
	elseif type == "Player" then
		local mode  			= event.mode or "Player"
		local pvpEnabled 		= event.pvpEnabled
		local playerDestruction = event.playerDestruction
		local armorRating 		= event.armorRating
		
		-- Update as many fields as were passed
		local tObject = s_tRegisteredObjects[ID]
		if tObject == nil then
			tObject = {
				type		= ObjectType.PLAYER,
				cursorSet	= false
			}
			s_tRegisteredObjects[ID] = tObject
		end
		if alive 			 ~= nil then tObject.alive 			   = alive end
		if mode 			 ~= nil then tObject.mode 			   = mode end
		if pvpEnabled 		 ~= nil then tObject.pvpEnabled 	   = pvpEnabled end
		if playerDestruction ~= nil then tObject.playerDestruction = playerDestruction end
		if health 			 ~= nil then tObject.health 		   = health end
		if maxHealth 		 ~= nil then tObject.maxHealth 		   = maxHealth end
		if armorRating 		 ~= nil then tObject.armorRating 	   = armorRating end
		if targetable 		 ~= nil then tObject.targetable 	   = targetable end
	end
end

-- Removes references to battle objects that are no longer in game.
function unregisterBattleObjectHandler(event)
	local ID = tonumber(event.ID)
	
	if ID then
		s_tRegisteredObjects[ID] = nil
	end
end



function onUpdateObjectAssociations(event)
	s_tObjectAssociations = event.objectAssociations or {}
end

-- Called when a registered object has been modified
function modifyBattleObjectHandler(event)
	local ID = tostring(event.ID)
	local modifiedFields = event.modifiedFields
	-- Is this item a registered target?
	local tObject = s_tRegisteredObjects[ID]
	if tObject then
		for index, value in pairs(modifiedFields) do
			-- We're talkin bout you son
			if (s_sPlayerName == ID) then
				-- Did you just die?
				if (index == "alive" and value == false) and s_bIsWeaponEquipped then
					s_bIsWeaponEquipped = false
				-- Invalidate cursorSet objects
				elseif (index == "playerDestruction" and value ~= s_tRegisteredObjects[s_sPlayerName].playerDestruction) or
					   (index == "pvpEnabled" 		 and value ~= s_tRegisteredObjects[s_sPlayerName].allowPvP) then
					setMouseOverMode()
				end

			-- If this a different object than you
			elseif s_sPlayerName ~= ID then
				-- Do we need to modify the mouseOver state?
				if (index == "alive" 	  and value ~= tObject[index]) or
				   (index == "health" 	  and value == 0) or 
				   (index == "mode" 	  and value ~= tObject[index]) or
				   (index == "tooltip" 	  and value ~= tObject[index]) or
				   (index == "pvpEnabled" and value ~= tObject.allowPvP) then
					-- Unregister cursor
					tObject.cursorSet = false
				end
			end
			tObject[index] = value
		end
	end
end

-- Handles world settings passed up from the server
function returnSettingsHandler(event)
	local destructibleObjects = event.destructibleObjects
	local allowPvP 			  = event.allowPvP
	
	-- Do mouseOver settings need to be reset?
	if ((s_tWorldSettings.destructibleObjects ~= nil) and (s_tWorldSettings.destructibleObjects ~= destructibleObjects)) or
	   ((s_tWorldSettings.allowPvP ~= nil) 			 and (s_tWorldSettings.allowPvP ~= allowPvP)) then
		setMouseOverMode()
	end
	
	-- Set world settings
	s_tWorldSettings = {destructibleObjects = destructibleObjects,
					   allowPvP 		   = allowPvP}
end

function onOccupedVehicleHandler(event)
	if event.PID then
		local PID = tostring(event.PID)
		if s_tRegisteredObjects[PID] then
			s_tRegisteredObjects[PID].cursorSet = false
		end
		if event.occupied == 1 then
			s_tOccupiedVehicles[PID] = true
		else
			s_tOccupiedVehicles[PID] = nil
		end
	end
end

-- Regenerate the list of flag objects using info from the server
function onFlagsReturnedFull(dispatcher, fromNetid, event, eventid, filter, objectid)
	local flags = Events.decode(KEP_EventDecodeString(event))
	FlagSystem:resetFlags(flags)

end

function onFlagReturned(dispatcher, fromNetid, event, eventid, filter, objectid )
	local PID = KEP_EventDecodeNumber(event)
	if KEP_EventMoreToDecode(event) ~= 0 then
		flag = Events.decode(KEP_EventDecodeString(event))
		FlagSystem:addFlag(flag)
		return
	end
	FlagSystem:removeFlag(PID)
end



function onTimedStateUpdated(event)
	enableLootHighlightForObject(event.PID, event.enable)
end


-- -- -- -- -- -- --
-- Local Functions
-- -- -- -- -- -- --

setHighlight = function(PID)
	local distToObject, objectLoc, playerLoc = getObjectDistanceSquared(PID)
	-- Are we withing minimum interaction range?
	if distToObject <= MIN_INTERACT_DISTANCE then
		local object = s_tRegisteredObjects[PID]
		-- If this item is owned by you and you're in hand mode and not currently selecting this object
		local shouldGlow = s_bIsInHandMode and (object.owner and object.owner == s_sPlayerName)
		if( not shouldGlow ) then
			local locationHint = {x = objectLoc[1], y = objectLoc[2], z = objectLoc[3]}
			shouldGlow = FlagSystem:checkLandClaimFlagMembershipForPlayer(s_sPlayerName, PID, locationHint)
		end
		
		-- BLUE GLOW

		if shouldGlow then
			KEP_SetOutlineState(ObjectType.DYNAMIC, PID, 1)
			KEP_SetOutlineColor(ObjectType.DYNAMIC, PID, unpack(HIGHLIGHT_COLOR.OWNED.MOUSEOVER))
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, PID, 1)
			s_iHighlightedObjectID = PID
			return
		end

		-- GREEEN GLOW
		
		local interactionRange = object.interactionRange 
		if interactionRange == nil then
			interactionRange = object.craftingProximity
		end

		-- Don't highlight a loot-generating placeable that does NOT belong to you
		local lootGenPlaceableCheck = false
		if object.behavior and object.behavior == "placeable_loot" then
			if object.owner and object.owner ~= s_sPlayerName then
				lootGenPlaceableCheck = true
			end
		end

		if interactionRange and not lootGenPlaceableCheck then
			-- Are you within interactionRange?
			if distToObject <= interactionRange * interactionRange then
				KEP_SetOutlineState(ObjectType.DYNAMIC, PID, 1)
				KEP_SetOutlineColor(ObjectType.DYNAMIC, PID, unpack(HIGHLIGHT_COLOR.INTERACTIVE.IN_RANGE))
				KEP_SetOutlineZSorted(ObjectType.DYNAMIC, PID, 1)
			else
				KEP_SetOutlineState(ObjectType.DYNAMIC, PID, 1)
				KEP_SetOutlineColor(ObjectType.DYNAMIC, PID, unpack(HIGHLIGHT_COLOR.INTERACTIVE.OUT_OF_RANGE))
				KEP_SetOutlineZSorted(ObjectType.DYNAMIC, PID, 1)
			end
			s_iHighlightedObjectID = PID
		end
	-- If this object was highlighted before, un-highlight it now
	elseif s_iHighlightedObjectID == PID then
		KEP_SetOutlineState(ObjectType.DYNAMIC, PID, 0)
		KEP_SetOutlineZSorted(ObjectType.DYNAMIC, PID, 0)
		s_iHighlightedObjectID = nil
	end
end

-- Highlight objects associated to the input PID
highlightAssociatedObjects = function(PID, select)
	PID = tonumber(PID)
	local objectAssociations = {}
	for _, testAssociations in pairs(s_tObjectAssociations) do
		for index, associatedPID in pairs(testAssociations) do
			if PID == associatedPID then
				objectAssociations = testAssociations
				break
			end
		end
		if #objectAssociations > 0 then
			break
		end
	end
	
	s_tAssociatedHighlights = {}
	for index, associatedPID in pairs(objectAssociations) do
		-- Highlight associated PIDs with the input PID
		if associatedPID ~= PID and associatedPID ~= s_sSelectedObjectID then
			-- Highlight an associated PID
			if select then
				KEP_SetOutlineState(ObjectType.DYNAMIC, associatedPID, 1)
				KEP_SetOutlineColor(ObjectType.DYNAMIC, associatedPID, unpack(HIGHLIGHT_COLOR.OWNED.ASSOCIATED))
				KEP_SetOutlineZSorted(ObjectType.DYNAMIC, associatedPID, 1)
				table.insert(s_tAssociatedHighlights, associatedPID)
			-- Un-highlight an associated PID
			else
				KEP_SetOutlineState(ObjectType.DYNAMIC, associatedPID, 0)
				KEP_SetOutlineZSorted(ObjectType.DYNAMIC, associatedPID, 0)
			end
		end
	end
end

-- Get the squared distance of the object
getObjectDistanceSquared = function(PID)
	local objLoc = {KEP_DynamicObjectGetPositionAnim(PID)}
	local playerLoc = {KEP_GetPlayerPosition()}
	
	local xDiff = objLoc[1] - playerLoc[1]
	local yDiff = objLoc[2] - playerLoc[2]
	local zDiff = objLoc[3] - playerLoc[3]
	local result = xDiff * xDiff + yDiff * yDiff + zDiff * zDiff
	return result, objLoc, playerLoc
end

-- Sets the mouse over mode for all objects in the zone
function setMouseOverMode()
	-- Set mouseover mode for all objects
	for ID, object in pairs(s_tRegisteredObjects) do
		if object.cursorSet then
			-- Invalidate all cursorSets
			object.cursorSet = false
			
			local newID = ID
			local itemType = ObjectType.DYNAMIC
			local cursorType = CURSOR_TYPE_DEFAULT
			
			if s_bIsWeaponEquipped and s_bDoesWeaponRequireTarget then
				cursorType = CURSOR_TYPE_ATTACKABLE
			end
			if object.type == ObjectType.PLAYER then
				-- ID used for cursors requires a netId
				newID = s_tRegisteredObjects[ID].netId
				itemType = ObjectType.ENTITY
			end
			
			KEP_AddMouseOverSetting(itemType, newID, object.mode or MOUSEOVER_CURSOR+MOUSEOVER_LIGHTEN, object.tooltip or "", cursorType)
		end
	end
end

-- Sets a mouseover state for an object
function setMouseOver(ID)
	local newID = ID
	local cursorType = CURSOR_TYPE_DEFAULT

	local tMousedOverObject	= s_tRegisteredObjects[ID]
	local tPlayerObject		= s_tRegisteredObjects[s_sPlayerName]
	
	tMousedOverObject.cursorSet = true
	local seedInteraction = false
	if tMousedOverObject.behavior and tMousedOverObject.behavior == "seed" then
		seedInteraction = true
	end

	local itemType
	-- Handle object
	if tMousedOverObject.type == ObjectType.DYNAMIC then
		-- Chests get a finger
		local occupiedVehicle = false
		if s_tOccupiedVehicles[ID] then
			occupiedVehicle = true
		end
		if tMousedOverObject.isLoot then
			cursorType = CURSOR_TYPE_CLICKABLE
		-- check for tool here
		elseif s_bDoesWeaponEmote then
			cursorType = CURSOR_TYPE_ATTACKABLE
		elseif s_bIsWeaponGeneric then
			if tMousedOverObject.behavior and (seedInteraction or tMousedOverObject.behavior == "pet" or tMousedOverObject.behavior == "wheeled_vehicle") then
				cursorType = CURSOR_TYPE_HEALABLE
			else
				cursorType = CURSOR_TYPE_DEFAULT
			end
		elseif s_bIsWeaponEquipped and s_bDoesWeaponRequireTarget then
			local destructionEnabled = false
			destructionEnabled = checkFlagLocation(ID)
			if occupiedVehicle then
				destructionEnabled = checkPVPFlagLocation(ID)
			end
			-- Healing weapons can't attack ANY objects
			if (s_bDoesWeaponHeal and not s_bDoesWeaponRepair) then
				cursorType = CURSOR_TYPE_ATTACKABLE
			elseif s_bDoesWeaponPaint then
				local owner = tMousedOverObject.owner
				if owner == s_sPlayerName then
					cursorType = CURSOR_TYPE_HEALABLE
				else
					cursorType = CURSOR_TYPE_ATTACKABLE2
				end
			elseif s_bDoesWeaponRepair then
				if tMousedOverObject.itemType == "placeable" then
					cursorType = CURSOR_TYPE_HEALABLE
				else
					cursorType = CURSOR_TYPE_ATTACKABLE
				end
			else
				
				-- PlayerDestruction disabled
				if ((tPlayerObject.playerDestruction == false and occupiedVehicle == false) or destructionEnabled == false) and not seedInteraction then
					--local owner = tMousedOverObject.owner
					-- If this object isn't owned by anyone
					if tMousedOverObject.spawned then
						-- TODO: Account for chests (finger)
						local bIsPetOwnedByPlayer = (tMousedOverObject.behavior == "pet" and tMousedOverObject.owner == s_sPlayerName)
						local bIsFollowerOwnedByPlayer = ((tMousedOverObject.behavior == "actor" or tMousedOverObject.behavior == "vendor" or tMousedOverObject.behavior == "shop_vendor" or tMousedOverObject.behavior == "quest_giver") and tMousedOverObject.owner == s_sPlayerName)
						if 	tMousedOverObject.targetable == "true" and 
							tMousedOverObject.alive and 
							not ((tPlayerObject.pvpEnabled == false or checkPVPFlagLocation(ID) == false) and (tMousedOverObject.behavior == "pet" or tMousedOverObject.behavior == "actor" or tMousedOverObject.behavior == "vendor" or tMousedOverObject.behavior == "shop_vendor" or tMousedOverObject.behavior == "quest_giver")) and
							not bIsPetOwnedByPlayer and
							not bIsFollowerOwnedByPlayer then
							
							cursorType = CURSOR_TYPE_ATTACKABLE2
						else
							-- TODO: Account for chests (finger)
							cursorType = CURSOR_TYPE_ATTACKABLE
						end
					else
						-- TODO: Account for chests (finger)
						cursorType = CURSOR_TYPE_ATTACKABLE
					end
				-- PlayerDestruction enabled
				else
					-- TODO: Account for chests (finger)
					local bIsPetOwnedByPlayer = (tMousedOverObject.behavior == "pet" and tMousedOverObject.owner == s_sPlayerName)
					local bIsFollowerOwnedByPlayer = ((tMousedOverObject.behavior == "actor" or tMousedOverObject.behavior == "vendor" or tMousedOverObject.behavior == "shop_vendor" or tMousedOverObject.behavior == "quest_giver") and tMousedOverObject.owner == s_sPlayerName)
					if	tMousedOverObject.targetable == "true" and 
						tMousedOverObject.alive and 
						not ((tPlayerObject.pvpEnabled == false or checkPVPFlagLocation(ID) == false) and (tMousedOverObject.behavior == "pet" or tMousedOverObject.behavior == "actor" or tMousedOverObject.behavior == "vendor" or tMousedOverObject.behavior == "shop_vendor" or tMousedOverObject.behavior == "quest_giver")) and
						not bIsPetOwnedByPlayer and
						not bIsFollowerOwnedByPlayer then
						cursorType = CURSOR_TYPE_ATTACKABLE2
					else
						-- TODO: Account for chests (finger)
						cursorType = CURSOR_TYPE_ATTACKABLE
					end
				end
			end
		-- All objects in non-object mode get a finger
		else
			cursorType = CURSOR_TYPE_CLICKABLE
		end
		itemType = ObjectType.DYNAMIC

	-- Handle player
	elseif tMousedOverObject.type == ObjectType.PLAYER then
		-- Modify yourself specially
		if ID == s_sPlayerName then
			if s_bIsWeaponEquipped and s_bDoesWeaponRequireTarget then
				cursorType = CURSOR_TYPE_ATTACKABLE
			end
		else
			if s_bIsWeaponEquipped and s_bDoesWeaponRequireTarget then
				if tMousedOverObject.mode == "Player" then
					if tMousedOverObject.targetable and
					   tMousedOverObject.alive and
					   ((tPlayerObject.pvpEnabled and tMousedOverObject.pvpEnabled) or s_bDoesWeaponEmote or s_bDoesWeaponHeal) then
						if s_bDoesWeaponHeal then
							cursorType = CURSOR_TYPE_HEALABLE
						elseif s_bDoesWeaponRepair or s_bDoesWeaponPaint then
							cursorType = CURSOR_TYPE_ATTACKABLE
						else
							cursorType = CURSOR_TYPE_ATTACKABLE2
						end
					else
						cursorType = CURSOR_TYPE_ATTACKABLE
					end

				elseif tMousedOverObject.mode == "God" then
					cursorType = CURSOR_TYPE_ATTACKABLE
				end
			end
		end
		
		itemType = ObjectType.ENTITY
		-- ID used for cursors requires a netId
		newID = tMousedOverObject.netId
		if newID == nil then 
			LogWarn("setMouseOver() Player["..tostring(ID).."] has no netId") 
			return 
		end
	end
	KEP_AddMouseOverSetting(itemType, newID, MOUSEOVER_CURSOR+MOUSEOVER_LIGHTEN, tMousedOverObject.tooltip or "", cursorType)
end

-- Gets a player by a netID
getPlayerNameByNetID = function(netID)
	if netID == nil then return nil end
	local playerObj = getPlayerObjectFromNetId(netID)
	
	-- Validate check this playerObj
	if KEP_ObjectIsValid(playerObj) == 0 then
		LogWarn("getPlayerNameByNetID() -  KEP_ObjectIsValid("..tostring(playerObj)..") returned 0 (invalid)")
		return nil
	end

	local playerName = GetSet_Safe_GetString(playerObj, MovementIds.NAME)
	if playerName == nil then
		LogWarn("getPlayerNameByNetID() - Failed to return playerName from GetSet_Safe_GetString("..tostring(playerObj)..", "..tostring(MovementIds.NAME)..")")
		return nil
	else
		return playerName
	end
end

-- Returns a player object from a netID
getPlayerObjectFromNetId = function(netId)
	if netId == nil then
		LogWarn("getPlayerObjectFromNetId() Received nil netId")
		return nil
	end
	local playerObj = KEP_GetPlayerByNetworkDefinedID(netId)

	if playerObj then
		if KEP_ObjectIsValid(playerObj) ~= 0 then
			return playerObj
		else
			LogWarn("getPlayerObjectFromNetId() KEP_ObjectIsValid("..tostring(playerObj)..") returned 0 (invalid)")
			return nil
		end
	else
		LogWarn("getPlayerObjectFromNetId() No playerObj returned from KEP_GetPlayerByNetworkDefinedID("..tostring(netId)..")")
		return nil
	end
end

-- Find a player's netId based on their name
function updateNetIds()
	-- Get all net Ids in the current zone
	local netIds = KEP_GetAllNetIds()
	local checkedPlayers = {}
	
	local valueStart, value
	local valueEnd = 0
	
	-- Extract all netIds
	valueStart, valueEnd, value = string.find(netIds, "[^%s]+", valueEnd)
	if valueEnd == nil then return end
	-- For each netId, check if it's currently registered
	while valueEnd <= string.len(netIds) do
		local netId = (tostring(string.sub(netIds, valueStart, valueEnd)))
		local playerName = getPlayerNameByNetID(netId)
		-- Ensure a playerName was returned
		if playerName and playerName ~= "" then
			if s_tRegisteredObjects[playerName] then
				-- Add playerName to checked players
				checkedPlayers[playerName] = true
			
				s_tRegisteredObjects[playerName].netId = tonumber(netId)
			end
		else
			LogError("updateNetIds() - getPlayerNameByNetID("..tostring(netId).." failed to return a valid playerName Return["..tostring(playerName).."]")
		end
		
		-- Get the next netId if one exists
		valueStart, valueEnd, value = string.find(netIds, "[^%s]+", valueEnd+1)
		-- Fail check
		if valueEnd == nil then break end
	end
	
	-- For every player that wasn't checked, clear out their now invalid netId
	for ID, object in pairs(s_tRegisteredObjects) do
		if (object.type == PLAYER_OBJECT) and (checkedPlayers[ID] == nil) then
			Log("updateNetIds() Player["..tostring(ID).."] has exited your netID withdrawal distance. Clearing their NetID")
			-- This player's netId is no longer valid. Clear it out
			s_tRegisteredObjects[name].netId = nil
		end
	end
end

checkFlagLocation = function(targetObject)
	local targetPos = {x = 0, y = 0, z = 0}
	targetPos.x, targetPos.y, targetPos.z = KEP_DynamicObjectGetPositionAnim(targetObject)

	return FlagSystem:getDominantFlagSetting("destruction", s_tWorldSettings.destructibleObjects, targetPos.x, targetPos.y, targetPos.z )
end

checkPVPFlagLocation = function(targetObject)
	local targetPos = {x = 0, y = 0, z = 0}
	targetPos.x, targetPos.y, targetPos.z = KEP_DynamicObjectGetPositionAnim(targetObject)

	return FlagSystem:getDominantFlagSetting("pvp", s_tWorldSettings.allowPvP, targetPos.x, targetPos.y, targetPos.z )
end




function isSelectionHighlightOn(objectPID)
	return s_iHighlightedObjectID and tonumber(s_iHighlightedObjectID) == objectPID
end

function isLootHighlightOn(objectPID)
	return s_tHighlightedLootObjects[tostring(objectPID)]
end

function isProximityHighlightOn(objectPID)
	return s_tProximityTriggers[tostring(objectPID)]
end


function enableLootHighlightForObject(objectPID, enable)
	-- Set the state to pass into the API
	local state = enable and 1 or 0

	if( enable ) then
		s_tHighlightedLootObjects[tostring(objectPID)] = true
		
		-- Turn on the golden outline
		--if not isSelectionHighlightOn(objectPID) then
		--	KEP_SetOutlineState(ObjectType.DYNAMIC, objectPID, state)
		--	KEP_SetOutlineColor(ObjectType.DYNAMIC, objectPID, unpack(LOOT_HIGHLIGHT))
		--	KEP_SetOutlineZSorted(ObjectType.DYNAMIC, objectPID, 1)
		--end
		
		-- Turn on the tint and set its colour to gold
		KEP_SetObjectHighlight(objectPID, state, unpack(TINT_COLOR))
	else
		s_tHighlightedLootObjects[tostring(objectPID)] = nil
		
		-- Turn off the golden outline
		if not isSelectionHighlightOn(objectPID) then
			KEP_SetOutlineState(ObjectType.DYNAMIC, objectPID, state)
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, objectPID, 0)
		end

		-- Turn off the tint
		KEP_SetObjectHighlight(objectPID, state)
	end
end


function enableProximityHighlightForObject(objectPID, enable, setting)
	-- Set the state to pass into the API

	
	local state = enable and 1 or 0
	--if s_tHighlightedLootObjects[tostring(objectPID)] then return end
	if( enable ) then
		s_tProximityTriggers[tostring(objectPID)] = true
		if not isSelectionHighlightOn(objectPID) and s_bIsProxyGlowEnabled then

			KEP_SetOutlineState(ObjectType.DYNAMIC, objectPID, state)
			KEP_SetOutlineColor(ObjectType.DYNAMIC, objectPID, unpack(PROXIMITY_HIGHLIGHT))
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, objectPID, 1)
		end
	else
		if not setting then
			s_tProximityTriggers[tostring(objectPID)] = false
		end
		if not isSelectionHighlightOn(objectPID) then
			KEP_SetOutlineState(ObjectType.DYNAMIC, objectPID, state)
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, objectPID, 0)
		end
	end
end

-- Returns whether a player is registered in the object list
function checkForPlayer(playerName)
	for index,object in pairs(s_tRegisteredObjects) do
		if object.type == ObjectType.PLAYER then
			if index == playerName then
				return true
			end
		end
	end
	return false
end

-- Returns whether a search item exists in a table
function checkForEntry(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return true
		end
	end
	return false
end

-- Returns the index of a search item within a table (if any)
function getIndex(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return i
		end
	end
	return -1
end