--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua" )
dofile("Framework_EventHelper.lua")

gGameId = nil
g_locationType = nil
g_frameworkEnabled = nil
g_playerModeActive = nil

g_t_LocationSettings = {}
g_filename = ""
g_username = ""
g_owner = false
g_devMode = false

HOME = 1
COMMUNITY_HANGOUT = 2
APP_3D = 3
PUBLIC = 4

g_defaultToolTip = ""

PLACE_NAME_MAX_LENGTH = 24

PURCHASE_RAVE_EVENT = "PurchaseRaveEvent"
ADD_FAVORITE_EVENT = "AddFavoriteEvent"
RAVE_PURCHASED_EVENT = "RavePurchasedEvent"

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ravePurchasedEventHandler", RAVE_PURCHASED_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler( "returnFrameworkStateHandler", "FrameworkState", KEP.MED_PRIO )
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( ADD_FAVORITE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( RAVE_PURCHASED_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( ADD_FAVORITE_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	
	gGameId = KEP_GetParentGameId()
	g_username = KEP_GetLoginName()
	if KEP_IsOwner() then
		g_owner = true
	end

	g_devMode = (KEP_DevMode() == 1)
	
	if (gGameId ~= 0) then
		g_locationType = APP_3D
	end

	-- Is the Framework active in this world? Let's find out
	local event = KEP_EventCreate("GetFrameworkState")
	KEP_EventQueue(event)
	
	requestLocationData()
end

function Dialog_OnDestroy(dialogHandle)	
	Helper_Dialog_OnDestroy( dialogHandle )
end

-- Handles the return for the current Framework state
function returnFrameworkStateHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local frameworkState = cjson.decode(Event_DecodeString(event))
	
	if frameworkState.enabled ~= nil then
		g_frameworkEnabled = frameworkState.enabled 
	end

	if frameworkState.playerMode ~= nil then
		g_playerModeActive = frameworkState.playerMode == "Player"
	end

	enableObjectList()
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.PLACE_INFO then
		local estr = KEP_EventDecodeString( event )
		ParseLocationData(estr)
	   	return KEP.EPR_OK
	elseif filter == WF.RCLICK_PLACE_RAVE then
		local estr = KEP_EventDecodeString( event )
		ParseLocationRaveResults(estr)
	   	return KEP.EPR_OK
	end
end

function ravePurchasedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	requestLocationData()
end

function requestLocationData()
	-- PLACE_INFO must be used to share with Hud2.lua so both are updated on raves
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..GET_PLACE_INFORMATION_SUFFIX, WF.PLACE_INFO)
end

function ParseLocationData(g_locationData)
    local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	s, strPos, result = string.find(g_locationData, "<ReturnCode>(.-)</ReturnCode>")

	if result == "0" then

		s, strPos, result = string.find(g_locationData, "<rave_count>(.-)</rave_count>")
		g_t_LocationSettings["NumRaves"] = tonumber(result)

		s, strPos, result = string.find(g_locationData, "<name>(.-)</name>")
		g_t_LocationSettings["Name"] = result
				
		s, strPos, result = string.find(g_locationData, "<player_rave_count>(.-)</player_rave_count>")
		g_t_LocationSettings["hasRaved"] = (tonumber(result) > 0)

		s, strPos, result = string.find(g_locationData, "<current_zone_type>(.-)</current_zone_type>")
        g_t_LocationSettings["ZoneType"] = tonumber(result)
        
        s, strPos, result = string.find(g_locationData, "<creator_username>(.-)</creator_username>")
        if result == nil then
        	result = ""
        end
		g_t_LocationSettings["Owner"] = result
		
		s, strPos, result = string.find(g_locationData, "<population_count>(.-)</population_count>")
		g_t_LocationSettings["Population"] = result
		
		s, strPos, result = string.find(g_locationData, "<community_id>(.-)</community_id>")
		g_t_LocationSettings["CommunityId"] = tonumber(result)
		
		s, e, dbPath = string.find(g_locationData, "<thumbnail_small_path>(.-)</thumbnail_small_path>")
		if not dbPath then
			dbPath = DEFAULT_KANEVA_PLACE_ICON
		end
		
		g_fileName = tostring(split(GetFilenameFromPath(dbPath), ".")[1])
		g_fileName = g_t_LocationSettings["CommunityId"] .. "_" .. g_fileName .. CUSTOM_TEXTURE_THUMB
					
		KEP_DownloadTextureAsyncStr( g_fileName, dbPath )
		
		local ihFore = Dialog_GetImage(gDialogHandle, "imgObjectPic")
		local ihBack = Dialog_GetImage(gDialogHandle, "imgBackground")
		
		if ihFore then
			g_fileName = "../../CustomTexture/" .. g_fileName
			-- Calls the scaleImage function from CommonFunctions in order to scale the profile pics				
			scaleImage(ihFore, ihBack, g_fileName)	        		
        end
				
 	end
    if (g_locationType == nil) then
		if (g_t_LocationSettings["ZoneType"] == 3) then
			g_locationType = HOME
		elseif(g_t_LocationSettings["ZoneType"] == 6) then
			g_locationType = COMMUNITY_HANGOUT
		elseif(g_t_LocationSettings["ZoneType"] == 4) then
			g_locationType = PUBLIC
		end	
	end
	setupMenu()
end

function textureDownloadHandler()		
	local ihFore = Dialog_GetImage(gDialogHandle, "imgObjectPic")
	local ihBack = Dialog_GetImage(gDialogHandle, "imgBackground")			
	if ihFore then
		scaleImage(ihFore, ihBack, g_fileName)	        		
    end
end

function ParseLocationRaveResults(g_locationData)
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local code = ""  		-- return code frome query
	local desc = ""         -- return description of query
	s, strPos, code = string.find(g_locationData, "<ReturnCode>(.-)</ReturnCode>")
	s, strPos, desc = string.find(g_locationData, "<ReturnDescription>(.-)</ReturnDescription>")
	if code == "0" then
		requestLocationData()
		KEP_MessageBox("You raved <br>"..g_t_LocationSettings["Name"])
	elseif code == "-2" then
		KEP_MessageBox("You cannot rave this area")
	elseif code == "-3" then
	    KEP_MessageBox("You have already raved this location")
	end
end

-- Called to enable the make deed button. Ensures location info is fully loaded.
function enableMakeDeed()
	if (g_locationType == nil) or (g_frameworkEnabled == nil) then
		return
	end
	
	if g_owner and (not g_frameworkEnabled or not g_playerModeActive) and (g_locationType == HOME or g_locationType == COMMUNITY_HANGOUT) then
		Control_SetEnabled(gHandles["btnMakeDeed"], true)
		Control_SetEnabled(gHandles["imgButtonBG7"], true)
	else
		Control_SetEnabled(gHandles["btnMakeDeed"], false)
		Control_SetEnabled(gHandles["imgButtonBG7"], false)
	end
end

function enableObjectList()
	local enableList = not (g_frameworkEnabled and g_playerModeActive)
	Control_SetEnabled(gHandles["btnItemList"], enableList)
	Control_SetEnabled(gHandles["imgButtonBG3"], enableList)
end

function setupMenu()
	if (g_locationType == nil) then
		return
	end

	hideControl("imgButtonBG7")

	if (g_locationType == HOME) then
		resize_9Slice( 178, 243 )
		
		hideControl("btnJoinCommunity")
		hideControl("btnAdd3DApp")
		hideControl("btnReportBlock")
		
		if g_owner == true then
			Control_SetVisible(gHandles["btnMakeDeed"], true)
			moveControl("btnMakeDeed", 45, 153)
			Control_SetVisible(gHandles["imgButtonBG7"], true)
			enableMakeDeed()
		end
		
		Static_SetText( gHandles["Menu_Title"], "Home")
	elseif (g_locationType == COMMUNITY_HANGOUT) then
		resize_9Slice( 178, 203 )
		
		moveControl("btnAdd2Favorites", 140, 113)
		hideControl("horizRuler_3")
		hideControl("btnAddFriend")
		hideControl("imgButtonBG6")
		hideControl("btnAdd3DApp")
		hideControl("btnReportBlock")
		moveControl("lblToolTip", 10, 150)
		
		if g_owner == true then
			--Add a 2nd row of buttons to the menu
			resize_9Slice( 178, 243 )
			Control_SetVisible(gHandles["horizRuler_3"], true)
			Control_SetEnabled(gHandles["horizRuler_3"], true)
			moveControl("lblToolTip", 10, 191)
			
			--Move the zone settings button to the 2nd line
			moveControl("btnZoneSettings", 12, 153)
			Control_SetEnabled(gHandles["imgButtonBG6"], true)
			Control_SetVisible(gHandles["imgButtonBG6"], true)
			
			Control_SetVisible(gHandles["btnMakeDeed"], true)
			moveControl("btnMakeDeed", 45, 153)
			Control_SetVisible(gHandles["imgButtonBG7"], true)
			enableMakeDeed()
		end
		
		Static_SetText( gHandles["Menu_Title"], "Community")
	elseif (g_locationType == APP_3D) then
		resize_9Slice( 178, 203 )
		
		hideControl("btnItemList")
		hideControl("horizRuler_3")
		hideControl("btnAddFriend")
		hideControl("imgButtonBG6")
		hideControl("btnReportBlock")
		hideControl("btnJoinCommunity")
		moveControl("lblToolTip", 10, 150)
		Static_SetText( gHandles["Menu_Title"], "World")
	elseif (g_locationType == PUBLIC) then
		resize_9Slice( 178, 203 )
		
		moveControl("btnAdd2Favorites", 110, 113)
		hideControl("horizRuler_3")
		hideControl("btnAddFriend")
		hideControl("imgButtonBG6")
		hideControl("btnAdd3DApp")
		hideControl("btnReportBlock")
		hideControl("btnJoinCommunity")
		moveControl("lblToolTip", 10, 150)
		Static_SetText( gHandles["Menu_Title"], "Public")
	end
	
	--Set the number of members and the number of raves
	if ( g_t_LocationSettings["NumRaves"] ~= nil) then
		Static_SetText( gHandles["lblRaveCount"], g_t_LocationSettings["NumRaves"])	
	end
	
	if (g_t_LocationSettings["Name"] ~= nil) then
		local str = g_t_LocationSettings["Name"]
		if string.len(str) > PLACE_NAME_MAX_LENGTH then
			str = string.sub(str, 1, PLACE_NAME_MAX_LENGTH) .. "..."
		end	
		Static_SetText( gHandles["lblObjectName"], str)	
	end

	if g_owner == true then
		Control_SetVisible(gHandles["btnZoneSettings"], true)
		Control_SetEnabled(gHandles["btnZoneSettings"], true)
		Control_SetVisible(gHandles["imgButtonBG5"], true)
		Control_SetEnabled(gHandles["imgButtonBG5"], true)
	else
		hideControl("btnZoneSettings")
		hideControl("imgButtonBG5")
	end
	
	--lblNumMembers
	Static_SetText(gHandles["lblUserName"], g_t_LocationSettings["Owner"])
end

function btnWebView_OnButtonClicked( buttonHandle )
	gotoWebProfile()
end

function btnRave_OnButtonClicked( buttonHandle )
	if Control_GetVisible(buttonHandle) then    
	   ravePlace()
	end
end

function btnItemList_OnButtonClicked( buttonHandle )
	if Control_GetVisible(buttonHandle) then    
	   displayItems()
	end
end

function btnZoneSettings_OnButtonClicked( buttonHandle )
	if g_t_LocationSettings["ZoneType"] == 4 then
		KEP_MessageBox("Public Zones do not have a web profile.")
	elseif KEP_IsWorld() then
		MenuOpenModal("WorldAccessSettings.xml")
	else
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid="..g_t_LocationSettings["CommunityId"] )
	end
end

function btnAdd2Favorites_OnButtonClicked( buttonHandle )
	if Control_GetVisible(buttonHandle) and MenuIsClosed("AddFavorite.xml") then  
		MenuOpen("AddFavorite.xml")
		local ev= KEP_EventCreate( ADD_FAVORITE_EVENT)
		KEP_EventSetFilter(ev, 2)
		KEP_EventQueue( ev)
	end
end

function btnAddFriend_OnButtonClicked( buttonHandle )
	if Control_GetVisible(buttonHandle) then
		sendFriendRequest(g_t_LocationSettings["Owner"], MenuNameThis())
	end
end

function btnMakeDeed_OnButtonClicked( buttonHandle )
	if Control_GetVisible(buttonHandle) then
		local zoneIndex = tostring(KEP_GetCurrentZoneIndex())
		local zoneIndexPlain = tostring(InstanceId_GetId(zoneIndex))
		local instanceId = tostring(KEP_GetZoneInstanceId())
		local zoneIndexCleared = tostring(InstanceId_GetClearedInstanceId(zoneIndex))
		launchWebBrowser(GameGlobals.WEB_SITE_PREFIX_SHOP .. "deed.aspx?zoneIndex=" .. zoneIndexCleared .. "&instanceId=" .. instanceId .. "&zone_index_plain=" .. zoneIndexPlain)
		MenuCloseThis()
	end
end

function btnJoinCommunity_OnButtonClicked( buttonHandle )
	joinCommunity()	
end

function btnAdd3DApp_OnButtonClicked( buttonHandle )
	joinCommunity()
end

function joinCommunity()
	gotoWebProfile()
end

function gotoWebProfile()
	if g_t_LocationSettings["ZoneType"] == 6 or g_t_LocationSettings["ZoneType"] == 3 or g_t_LocationSettings["ZoneType"] == 4 then
		local communityName = GetCommunityName() .. ".channel"
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."channel/"..communityName )
	elseif KEP_IsWorld() == true then
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."channel/"..KEP_GetGameName()..".channel" )
	end
end

function displayItems()
	MenuOpen("DynamicObjectList.xml")
end

function ravePlace()
	if g_t_LocationSettings["hasRaved"] then
		KEP_MessageBox("You have already raved this location.") 
	else
		g_web_address = GameGlobals.WEB_SITE_PREFIX..SET_RAVES_SUFFIX.."&raveType=single&currency=FREE"
		makeWebCall( g_web_address, WF.RCLICK_PLACE_RAVE)
	end
end

function GetCommunityName()
	local g_communityname=g_t_LocationSettings["Name"]
	return g_communityname     
end

function Dialog_OnMouseMove(dialogHandle, x, y)

	--Make sure we have the interface we need (stop errors in editor)
	if Control_ContainsPoint ~= nil then
		--tooltip
		local sh = gHandles["lblToolTip"]
		Static_SetText(sh, g_defaultToolTip)
            
		if sh ~= nil then
			if Control_ContainsPoint(gHandles["btnRave"], x, y) ~= 0 
			and Control_GetVisible(gHandles["btnRave"]) == 1 then
				Static_SetText(sh, "Rave this World")
						
			elseif Control_ContainsPoint(gHandles["btnWebView"], x, y) ~= 0 
			and Control_GetVisible(gHandles["btnWebView"]) == 1 then
				Static_SetText(sh, "View this World on the Web")
                  
			elseif Control_ContainsPoint(gHandles["btnAdd2Favorites"], x, y) ~= 0 
			and Control_GetVisible(gHandles["btnAdd2Favorites"]) == 1 then
				Static_SetText(sh, "Add to your favorites")
                  
			elseif Control_ContainsPoint(gHandles["btnItemList"], x, y) ~= 0 
			and Control_GetVisible(gHandles["btnItemList"]) == 1 then
				Static_SetText(sh, "View the items in this World")
                  
			elseif Control_ContainsPoint(gHandles["btnAddFriend"], x, y) ~= 0 
			and Control_GetVisible(gHandles["btnAddFriend"]) == 1 then
				Static_SetText(sh, "Add the owner as a friend")

			elseif Control_ContainsPoint(gHandles["btnJoinCommunity"], x, y) ~= 0 
			and Control_GetVisible(gHandles["btnJoinCommunity"]) == 1 then
				Static_SetText(sh, "Join this World")
                  
			elseif Control_ContainsPoint(gHandles["btnAdd3DApp"], x, y) ~= 0 
			and Control_GetVisible(gHandles["btnAdd3DApp"]) == 1 then
				Static_SetText(sh, "Become a fan")
				
			elseif Control_ContainsPoint(gHandles["btnMakeDeed"], x, y) ~= 0 
			and Control_GetVisible(gHandles["btnMakeDeed"]) == 1 then
				Static_SetText(sh, "Add this World to the shop")
						
			elseif Control_ContainsPoint(gHandles["btnZoneSettings"], x, y) ~= 0 
			and Control_GetVisible(gHandles["btnZoneSettings"]) == 1 then
				Static_SetText(sh, "Change World settings")
			end
		end
	end
end
