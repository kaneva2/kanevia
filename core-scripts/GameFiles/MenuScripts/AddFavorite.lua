--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

ADD_FAVORITE_EVENT    = "AddFavoriteEvent"
ZONE_NAVIGATION_EVENT = "ZoneNavigationEvent"

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler("zoneNavigationEventHandler", ZONE_NAVIGATION_EVENT, KEP.MED_PRIO)
	KEP_EventRegisterHandler("addFavoriteHandler",         ADD_FAVORITE_EVENT,    KEP.MED_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister(ADD_FAVORITE_EVENT, KEP.MED_PRIO)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate(dialogHandle)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy(dialogHandle)
end

function addFavoriteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == 2 then
		requestCurrentURL()
	else
		local url = KEP_EventDecodeString(event)
		changeURLString(url)
	end
end

function zoneNavigationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ZONE_NAV.CURRENT_URL_INFO then
		local url = KEP_EventDecodeString( event )
		changeURLString(url)
	end
end

function btnOK_OnButtonClicked(buttonHandle)

	local txtName = Dialog_GetControl(gDialogHandle, "edName")
	local txtURL  = Dialog_GetControl(gDialogHandle, "edURL")

	local name = EditBox_GetText(txtName)
	local url  = EditBox_GetText(txtURL)

	local ev = KEP_EventCreate(ZONE_NAVIGATION_EVENT)
	KEP_EventSetFilter(ev, ZONE_NAV.ADD_FAVORITE)
	KEP_EventEncodeString(ev, name)
	KEP_EventEncodeString(ev, url)
	KEP_EventEncodeString(ev, "") --should be category TODO
	KEP_EventQueue(ev)
	
	requestFavorites()

	MenuCloseThis()
end

function changeURLString(url)
	local name    = parseFriendlyNameFromURL(url)
	local txtName = Dialog_GetControl(gDialogHandle, "edName")
	local txtURL  = Dialog_GetControl(gDialogHandle, "edURL")
	EditBox_SetText(txtName, name, false)
	EditBox_SetText(txtURL,  url,  false)
end

function requestFavorites()
	local ev = KEP_EventCreate(ZONE_NAVIGATION_EVENT)
	KEP_EventSetFilter(ev, ZONE_NAV.FAVORITES_REQUEST)
	KEP_EventQueue(ev)
end

function requestCurrentURL()
	local ev = KEP_EventCreate(ZONE_NAVIGATION_EVENT)
	KEP_EventSetFilter(ev, ZONE_NAV.CURRENT_URL_REQUEST)
	KEP_EventQueue(ev)
end
