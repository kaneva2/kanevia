--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\ActionItemFunctions.lua")
dofile("..\\MenuScripts\\UGCFunctions.lua")
dofile("..\\Scripts\\TextureImportStateIds.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

FILE_BROWSER_CLOSED_EVENT = "FileBrowserClosedEvent"
TEXTURE_IMPORT_PROGRESS_EVENT = "TextureImportProgressEvent"
UGC_DEPLOY_COMPLETION_EVENT = "UGCDeployCompletionEvent"

ITEMS_PER_PAGE = 5
TEXTURES_PER_PAGE = 12
NUM_LEN = 60
DEFAULT_GLID_KEY = "___DefaultGlid___"
WEB_ADDRESS = GameGlobals.WEB_SITE_PREFIX.."kgp/textureslist.aspx?"

g_paramKey = ""		-- The [identifier] for this AID.params.
g_param = {}		-- Table to param properties that can be modified by this menu
g_t_items = {}		-- Table to hold inventory info for USE_TYPE specific params
g_page = 1
g_maxPage = 1
g_use_type = 0
g_category = nil
g_clearSearch = true
g_searchText = ""
g_gotoLastPage = false
g_nextUploadID = 0
glidON = false 

-- for populating Listbox 
g_selections = 0
g_midpts = {}
LIST_PADDING = 6
FONT_SIZE = 12
MARGIN = 7

--setTime
SET_TIME = 2
g_time = 0 
showSet = false 

--currently selected (Listbox)
g_selected = 0 

g_searchON = false  

g_radValue = nil 

g_choicesNum = 0 

g_textureSelect = ""
g_totalTex = 0 

--resizing menus
PADDING_SMALL = 8
PADDING_LARGE = 18 

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "editParamHandler", EDIT_PARAM_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler( "FileBrowserClosedHandler", FILE_BROWSER_CLOSED_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "TextureImportProgressHandler", TEXTURE_IMPORT_PROGRESS_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UGCDeployCompletionHandler", UGC_DEPLOY_COMPLETION_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister(EDIT_PARAM_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister(CHANGE_PARAM_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister("BrowserPageReadyEvent", KEP.MED_PRIO )

	-- Handler for close of image selection menu
	KEP_EventRegister(FILE_BROWSER_CLOSED_EVENT, KEP.MED_PRIO )
	KEP_EventRegister(TEXTURE_IMPORT_PROGRESS_EVENT, KEP.MED_PRIO )
	UGCI.RegisterDeployEvents()
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	if glidON == true then
		setActivated(gHandles["imglistHoverBG"], false)
		setActivated(gHandles["imglistHover"], false)
		local hoverx = x+5
		local hovery = y - 34
		if Control_ContainsPoint(gHandles["lstItems"], x, y) ~= 0 then 
			for i,v in ipairs(g_midpts) do
				if y <= (g_midpts[i]+5) and y>=(g_midpts[i]-5) then 
				
					local itemIndex = ListBox_GetItemData( gHandles["lstItems"], (i-1) )
					local eh = Image_GetDisplayElement(gHandles["imglistHover"])
					if eh ~= 0 then
						KEP_LoadIconTextureByID(itemIndex, eh, gDialogHandle)
					end
					
					Control_SetLocation(gHandles["imglistHoverBG"],hoverx,hovery)
					Control_SetLocation(gHandles["imglistHover"],(hoverx+24),(hovery+9))
					setActivated(gHandles["imglistHoverBG"],true)
					setActivated(gHandles["imglistHover"], true)	
				end 
			end 	
		end 
	end 
end 

function Dialog_OnRender(dialogHandle, fElapsedTime)

	-- Controls how long Pattern copied & Paste appears 
	if showSet == true then 	
		if g_time < SET_TIME then
			if g_time <= 0 then
				setActivated(gHandles["stcSetLoc"],true)
			end
			g_time = g_time +fElapsedTime
		else 
			setActivated(gHandles["stcSetLoc"],false)
			showSet = false 
		end		
	end 
end

function inventoryWebCall()
	local itemsPP = ITEMS_PER_PAGE
	if g_paramKey ~= DEFAULT_GLID_KEY then
		 if (g_page <= 1 or g_maxPage == 1) and g_searchON == false then
			itemsPP = ITEMS_PER_PAGE - 1
		 end 
	end
	getPlayerInventory(WF.ACTION_ITEMS_PARAM, g_page, itemsPP, nil, g_searchText, g_category, g_use_type)
end

function textureWebCall()
	makeWebCall( WEB_ADDRESS, WF.TEXTURE_LIST, g_page, TEXTURES_PER_PAGE)
end

function BrowserPageReadyHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.ACTION_ITEMS_PARAM then
		local xmlData = KEP_EventDecodeString( event )
		
		-- Get the page number and search returned, and make sure it's still correct. 
		-- Otherwise, don't bother to load
		local s, e, pageReturned = string.find(xmlData, "<Page>(%d-)</Page>")
		pageReturned = tonumber(pageReturned) or 0
		
		local s, e, searchReturned = string.find(xmlData, "<Search>(.-)</Search>")
		searchReturned = searchReturned or ""
		
		-- and check search on this if later if added.
		if pageReturned == g_page then
			--Get the total number of items from the XML data and divide by items/page, update page numbers
			local s, e, totalItems = string.find(xmlData, "<TotalNumberRecords>(%d-)</TotalNumberRecords>")
			totalItems = tonumber(totalItems or 1)
			g_maxPage = math.ceil(totalItems/ITEMS_PER_PAGE)
			if (g_maxPage <= 1) then
			    setActivated(gHandles["btnPageNext"], false)
			    setActivated(gHandles["imgPageNext"], true)
			end
			g_t_items 	= {}
			decodeInventoryItemsFromWeb( g_t_items, xmlData, false )
			displayItemResultsWithIcon()
		end
		return KEP.EPR_CONSUMED
	elseif filter == WF.TEXTURE_LIST then
		local xmlData = KEP_EventDecodeString( event )
		
		--Get the total number of items from the XML data and divide by items/page, update page numbers
		local s, e, totalItems = string.find(xmlData, "<TotalNumberRecords>(%d-)</TotalNumberRecords>")
		totalItems = tonumber(totalItems or 1)
		g_totalTex = totalItems
		g_maxPage = math.ceil(totalItems/TEXTURES_PER_PAGE)
		g_t_items 	= {}
		decodeTextureData( g_t_items, xmlData)
		displayTextureResultsWithIcon()

		if(g_gotoLastPage) then
		   g_gotoLastPage = false
           g_page = g_maxPage
		   setActivated(gHandles["btnPagePrev"], true)
           setActivated(gHandles["imgPagePrev"], false)
		   setActivated(gHandles["btnPageNext"], false)
		   setActivated(gHandles["imgPageNext"], true)
		   textureWebCall()
		end

		if (g_maxPage <= 1) then
		    setActivated(gHandles["btnPageNext"], false)
		    setActivated(gHandles["imgPageNext"], true)
		end

		showUploadPage(false)

		return KEP.EPR_CONSUMED
	end
end

function FileBrowserClosedHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local dialogResult = KEP_EventDecodeNumber( event )
	local filePath = ""
	if dialogResult~=0 then
		filePath = KEP_EventDecodeString( event )
	else
		--File Browser was cancelled
		return
	end

	showUploadPage(true)

	local dropID = g_nextUploadID + 1
	local gImportInfo = {}
	gImportInfo.category = UGC_TYPE.TEXTURE
	gImportInfo.type = UGC_TYPE.TEXTURE
	gImportInfo.fullPath = filePath
	gImportInfo.sessionId = dropId

	gTarget = {}
	gTarget.valid = 0
	gTarget.pos = {}
	gTarget.pos.x = 0
	gTarget.pos.y = 0
	gTarget.pos.z = 0
	gTarget.normal = {}
	gTarget.normal.x = 0
	gTarget.normal.y = 0
	gTarget.normal.z = 0
	gTarget.type = ObjectType.DYNAMIC
	gTarget.id = -1
	gTarget.name = "Media Library"
	gBtnState = 0
	gKeyState = 0

	UGCI.CallUGCHandler( "TextureImport.xml", 0, dropID, gImportInfo, gTarget, gBtnState, gKeyState )
end

function TextureImportProgressHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local ImpSessionId = -1
	local ImpSession = nil
	local fullPath = ""

	if filter==UGCIS.DESTROYING or filter==UGCIS.DESTROYED then
		-- The filter is being set to destroyed by an event in textureimport.lua so a return is the best option
		return
	end

	if objectid~=nil then
		ImpSessionId = objectid
		ImpSession = UGC_GetImportSession( ImpSessionId )
		if ImpSession~=nil then
		 	fullPath = GetSet_GetString( ImpSession, TextureImportStateIds.SOURCEIMAGEPATH )
		end
	end

	if filter==UGCIS.PROCESSED then
		local internalError = true
		if ImpSession~=nil then
			local targetType = GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETTYPE )
			if fullPath==nil then
				LogError( "TextureImportProgressHandler::fullPath is nil"  )	        
				showUploadPage(false)
			else
				if targetType==ObjectType.DYNAMIC then
					internalError = false
				elseif targetType==ObjectType.WORLD then
					internalError = false

				elseif targetType==ObjectType.UNKNOWN then
					internalError = false
				else
					LogError( "TextureImportProgressHandler::can't determine target type"  )	        
					showUploadPage(false)
				end
			end
		end

		if internalError then
			LogError( "TextureImportProgressHandler::internalError is true"  )	        
			showUploadPage(false)			
		end

	elseif filter==UGCIS.PREVIEWFAILED then
		LogError( "TextureImportProgressHandler::UGCIS.PREVIEWFAILED"  )	        
		showUploadPage(false)
	elseif filter==UGCIS.PROCESSINGFAILED then
		LogError( "TextureImportProgressHandler::UGCIS.PROCESSINGFAILED"  )	        
		showUploadPage(false)
	elseif filter==UGCIS.IMPORTFAILED then
		LogError( "TextureImportProgressHandler::UGCIS.IMPORTFAILED"  )	        
		showUploadPage(false)
	end
end


function UGCDeployCompletionHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local ugcType = KEP_EventDecodeNumber( event )
	local ugcId = KEP_EventDecodeNumber( event )
	local resultText = KEP_EventDecodeString( event )		-- padded by WebCallTask
	local httpStatusCode = KEP_EventDecodeNumber( event )	-- padded by WebCallTask

	if httpStatusCode==200 then

		local s, e = 0      -- start and end of captured string
		local sResult = nil
		local sURL = nil
		s, e, sResult = string.find( resultText, "<ReturnCode>(-*%d+)</ReturnCode>")
		s, e, sAssetId = string.find( resultText, "<AssetId>(%d+)</AssetId>")
		s, e, sURL = string.find(resultText, "<TextureUrl>(.+)</TextureUrl>")

        if sURL == nil then
			showUploadPage(false)
		elseif g_param["type"] == "IMAGE" then
			setNewImageURL(sURL)
		end
	else
		showUploadPage(false)
    end
end

function editParamHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- load initial table values into g_param
	g_paramKey, g_param = ActionItemFunctions.decodeParam(event)

	if g_param["type"] == "MENS_EMOTE" or g_param["type"] == "WOMENS_EMOTE" or g_param["type"] == "OBJECT_ANIMATION" then
		g_use_type = USE_TYPE.ANIMATION
		if g_param["type"] == "MENS_EMOTE" then
			g_category = "Emotes For Men"
		elseif g_param["type"] == "WOMENS_EMOTE" then
			g_category = "Emotes For Women"
		else
			g_category = "Object Animations"
		end
		inventoryWebCall()
    elseif g_param["type"] == "ACCESSORY" then
		g_use_type = USE_TYPE.EQUIP
		inventoryWebCall()
	elseif g_param["type"] == "MENS_CLOTHING" then
		g_category = "For Men|Accessories For Men"
		inventoryWebCall()
	elseif g_param["type"] == "WOMENS_CLOTHING" then
		g_category = "For Women|Accessories For Women"
		inventoryWebCall()
	elseif g_param["type"] == "OBJECT" then
		g_use_type = USE_TYPE.ADD_DYN_OBJ
		inventoryWebCall()
	elseif g_param["type"] == "PARTICLE" then
		g_use_type = USE_TYPE.PARTICLE
		inventoryWebCall()
	elseif g_param["type"] == "IMAGE" then
		g_textureSelect = g_param["value"] 
		textureWebCall()
	elseif g_param["type"] == "NUMBER" then
		EditBox_SetText( gHandles["edValue"], tostring(g_param["value"]), false )
	elseif g_param["type"] == "STRING" then
		EditBox_SetText( gHandles["edValue"], g_param["value"], false )	
	elseif g_param["type"] == "LONG_STRING" then
		EditBox_SetText( gHandles["edValueThreeLine"], g_param["value"], false )
		-- make the box bigger here
	elseif g_param["type"] == "BOOL" then
		if g_param["value"] == true then
		btnOn_OnButtonClicked(gHandles["btnOn"])
		elseif g_param["value"] == false then
			btnOff_OnButtonClicked(gHandles["btnOff"])
		end
	elseif g_param["type"] == "PARTICLE_OFFSET" then
		EditBox_SetText( gHandles["edX"], tostring(g_param["x"]), false )
		EditBox_SetText( gHandles["edY"], tostring(g_param["y"]), false )
		EditBox_SetText( gHandles["edZ"], tostring(g_param["z"]), false )
	elseif g_param["type"] == "COORDINATE" then
		EditBox_SetText( gHandles["edX"], tostring(g_param["x"]), false )
		EditBox_SetText( gHandles["edY"], tostring(g_param["y"]), false )
		EditBox_SetText( gHandles["edZ"], tostring(g_param["z"]), false )
	elseif g_param["type"] == "SOUND" then
		EditBox_SetText( gHandles["edValue"], tostring(g_param["value"]), false )
		g_use_type = USE_TYPE.SOUND
		inventoryWebCall() 
	elseif g_param["type"] == "RADIAL" then
		Log("RAD VALUE: "..g_param["value"])
		g_radValue = g_param["value"]
		-- handle radio buttons here
	elseif g_param["type"] == "DROPDOWN" then
		Log("RAD VALUE: "..g_param["value"])
		g_radValue = g_param["value"]
		Static_SetText(gHandles["stcDropdown"],g_radValue)

	end
	handleParamType()
end

function textureDownloadHandler()
  	displayTextureResultsWithIcon()
end

function setActivated(controlHandle, value)
	if controlHandle ~= nil then
		Control_SetVisible(controlHandle, value)
		Control_SetEnabled(controlHandle, value)
	end
end

function hideAll()
	setActivated(gHandles["stcSubType1"], false)
	setActivated(gHandles["stcSubType2"], false)
	setActivated(gHandles["edValue"], false)
	setActivated(gHandles["edValueThreeLine"], false)
	setActivated(gHandles["radTrue"], false)
	setActivated(gHandles["radFalse"], false)
	setActivated(gHandles["btnOn"], false)
	setActivated(gHandles["imgOnClicked"], false)
	setActivated(gHandles["stcOnClicked"], false)
	setActivated(gHandles["btnOff"], false)
	setActivated(gHandles["imgOffClicked"], false)
	setActivated(gHandles["stcOffClicked"], false)
	setActivated(gHandles["edX"], false)
	setActivated(gHandles["edY"], false)
	setActivated(gHandles["edZ"], false)
	setActivated(gHandles["stcCoordDir"], false)
	setActivated(gHandles["stcSetLoc"], false)
	setActivated(gHandles["dirX"], false)
	setActivated(gHandles["dirY"], false)
	setActivated(gHandles["dirZ"], false)
	setActivated(gHandles["upX"], false)
	setActivated(gHandles["upY"], false)
	setActivated(gHandles["upZ"], false)
	setActivated(gHandles["btnCurrLoc"], false)
	setActivated(gHandles["lstItems"], false)
	setActivated(gHandles["imglistBG"], false)
	setActivated(gHandles["imglistBG2"], false)
	setActivated(gHandles["imglistBGWhite"], false)
	for i=1,6 do 
		setActivated(gHandles["imgDivHor"..i], false)
	end 
	for i=1,2 do 
		setActivated(gHandles["imgDivVert"..i], false)
	end 
	setActivated(gHandles["imgCurrentBG"], false)	
	setActivated(gHandles["imgCurrent"], false)
	setActivated(gHandles["stcCurrentName"], false)
	setActivated(gHandles["imglistHoverBG"], false)
	setActivated(gHandles["imglistHover"], false)
	setActivated(gHandles["imgSearchBG"], false)
	setActivated(gHandles["radioButton1"], false)
	setActivated(gHandles["radioButton2"], false)
	setActivated(gHandles["radioButton3"], false)
	setActivated(gHandles["radioButton4"], false)
	setActivated(gHandles["radioButton5"], false)	
	setActivated(gHandles["txtSearch"], false)
	setActivated(gHandles["imgClearSearchBG"], false)
	setActivated(gHandles["imgClearSearch"], false)
	setActivated(gHandles["btnPageNext"], false)
	setActivated(gHandles["btnPagePrev"], false)
	setActivated(gHandles["imgNextFade"], false)
	setActivated(gHandles["imgPrevFade"], false)
	setActivated(gHandles["btnUploadTxt"], false)
	setActivated(gHandles["stcUpload"], false)
	setActivated(gHandles["imgDropdown"], false)
	setActivated(gHandles["stcDropdown"], false)
	setActivated(gHandles["btnDropdown"], false)
	setActivated(gHandles["lstDropdown"], false)
	setActivated(gHandles["imgCurrentFrame"], false)
	for i = 1, TEXTURES_PER_PAGE do
		setActivated(gHandles["txtIcon" .. tostring(i)], false)
		setActivated(gHandles["btnIcon" .. tostring(i)], false)
		setActivated(gHandles["imgFrame"..tostring(i)], false)
	end
end

function resize(num) --resize the window based on parameter type 
	local line = gHandles["imgLine4"]
	local ok = gHandles["btnOk"]
	local cancel = gHandles["btnCancel"]
	local bg = gHandles["imgBackground"]
	
	local lineY = num+ PADDING_SMALL
	Control_SetLocation(line,Control_GetLocationX(line),lineY )
	
	local okY = lineY + Control_GetHeight(line) + PADDING_LARGE
	Control_SetLocation(ok,Control_GetLocationX(ok),okY)
	
	local cancelY =lineY + Control_GetHeight(line) + PADDING_LARGE
	Control_SetLocation(cancel,Control_GetLocationX(cancel),cancelY)
	
	local bgY = okY + Control_GetHeight(ok) + PADDING_LARGE
	Control_SetSize(bg, Control_GetWidth(bg),bgY)
	
	MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), bgY + PADDING_SMALL +33) 
end 

function enableGlidEntry()

	glidON = true 
	
	local eh = Image_GetDisplayElement(gHandles["imgCurrent"])
	if eh ~= 0 then
		KEP_LoadIconTextureByID(g_param["value"], eh, gDialogHandle)
	end
	Control_SetLocation(gHandles["imgLine4"], Control_GetLocationX(gHandles["imgLine4"]) + 7, Control_GetLocationY (gHandles["imgLine4"]))
	local descX = Control_GetLocationX(gHandles["stcDescription"])
	local descY = Control_GetLocationY (gHandles["stcDescription"])
	Control_SetLocation(gHandles["stcDescription"], Control_GetLocationX(gHandles["stcCurrentName"])-10 , Control_GetLocationY (gHandles["stcCurrentName"])-20)
	Control_SetSize(gHandles["stcDescription"], Control_GetWidth(gHandles["stcDescription"])-75, Control_GetHeight(gHandles["imgCurrent"]))
	Control_SetLocation(gHandles["stcCurrentName"],descX +8,descY )
		
	Static_SetText(gHandles["stcCurrentName"], g_param["name"])
	setActivated(gHandles["imgCurrentFrame"],true)
	setActivated(gHandles["lstItems"], true)
	setActivated(gHandles["btnPageNext"], true)
	setActivated(gHandles["imgPagePrev"], true)
	setActivated(gHandles["imglistBG"], true)
	setActivated(gHandles["imglistBG2"], true)
	setActivated(gHandles["imglistBGWhite"], true)
	for i=1,6 do 
		setActivated(gHandles["imgDivHor"..i], true)
	end 
	for i=1,2 do 
		setActivated(gHandles["imgDivVert"..i], true)
	end 
	setActivated(gHandles["imglistHoverBG"], false)
	setActivated(gHandles["imglistHover"], false)
	setActivated(gHandles["imgCurrentBG"], true)	
	setActivated(gHandles["imgCurrent"], true)
	setActivated(gHandles["stcCurrentName"], true)
	
	if g_param["type"] ~= "OBJECT_PLACEMENT" then
		setActivated(gHandles["imgSearchBG"], true)
		setActivated(gHandles["txtSearch"], true)
		setActivated(gHandles["imgClearSearchBG"], true)
		Control_SetEnabled(gHandles["imgClearSearch"],true) 
		Control_SetVisible(gHandles["imgClearSearch"],false)
	end
end

-- Adjust the UI according to the "type" value of the param we are editing
function handleParamType()
	Dialog_SetCaptionText( gDialogHandle, "  "..g_param["name"] )
	Static_SetText( gHandles["stcDescription"], g_param["description"] )
	hideAll()
	if g_param["type"] == "MENS_EMOTE" then
		enableGlidEntry()
	elseif g_param["type"] == "WOMENS_EMOTE" then
		enableGlidEntry()
	elseif g_param["type"] == "MENS_CLOTHING" then
		enableGlidEntry()
	elseif g_param["type"] == "WOMENS_CLOTHING" then
		enableGlidEntry()
	elseif g_param["type"] == "ACCESSORY" then
		enableGlidEntry()
	elseif g_param["type"] == "OBJECT_ANIMATION" then
		enableGlidEntry()
	elseif g_param["type"] == "SOUND" then
		enableGlidEntry()
	elseif g_param["type"] == "OBJECT" then
		enableGlidEntry()
	elseif g_param["type"] == "PARTICLE" then
		enableGlidEntry()
	elseif g_param["type"] == "OBJECT_PLACEMENT" then
		local doCount = KEP_GetDynamicObjectCount()
		g_maxPage = math.ceil(doCount / ITEMS_PER_PAGE)
		populateObjectPlacements(1)
		enableGlidEntry()
		if (g_maxPage <= 1) then
			setActivated(gHandles["btnPageNext"], false)
			setActivated(gHandles["imgPageNext"], true)
		end
	elseif g_param["type"] == "IMAGE" then
		Control_SetLocation(gHandles["btnPageNext"],286,Control_GetLocationY(gHandles["btnPageNext"])+20)
		Control_SetLocation(gHandles["imgPageNext"],286,Control_GetLocationY(gHandles["imgPageNext"])+20)
		Control_SetLocation(gHandles["imgPagePrev"],261,Control_GetLocationY(gHandles["imgPagePrev"])+20)
		Control_SetLocation(gHandles["btnPagePrev"],261,Control_GetLocationY(gHandles["btnPagePrev"])+20)
		Control_SetLocation(gHandles["btnUploadTxt"],Control_GetLocationX(gHandles["btnUploadTxt"]),Control_GetLocationY(gHandles["btnUploadTxt"])+20)
		setActivated(gHandles["btnPageNext"], true)
		setActivated(gHandles["imgPagePrev"], true)
		setActivated(gHandles["btnUploadTxt"], true)
		Control_SetSize(gHandles["imgCurrentBG"], math.ceil(Control_GetWidth(gHandles["imgCurrentBG"]) * 1.25), math.ceil(Control_GetHeight(gHandles["imgCurrentBG"]) * 1.25))
		Control_SetSize(gHandles["imgCurrent"], math.ceil(Control_GetWidth(gHandles["imgCurrent"]) * 1.25), math.ceil(Control_GetHeight(gHandles["imgCurrent"]) * 1.25))
		Control_SetLocation(gHandles["imgCurrentBG"],18,Control_GetLocationY(gHandles["imgCurrentBG"]))
		Control_SetLocation(gHandles["imgCurrent"],18,Control_GetLocationY(gHandles["imgCurrent"]))
		for i=1, TEXTURES_PER_PAGE do 
			Control_SetLocation(gHandles["txtIcon" .. tostring(i)],Control_GetLocationX(gHandles["txtIcon" .. tostring(i)]),Control_GetLocationY(gHandles["txtIcon" .. tostring(i)])+ 20)
			Control_SetLocation(gHandles["btnIcon" .. tostring(i)],Control_GetLocationX(gHandles["btnIcon" .. tostring(i)]) ,Control_GetLocationY(gHandles["btnIcon" .. tostring(i)])+ 20)
			Control_SetLocation(gHandles["imgFrame" .. tostring(i)],Control_GetLocationX(gHandles["imgFrame" .. tostring(i)]) ,Control_GetLocationY(gHandles["imgFrame" .. tostring(i)])+ 20)
		end 
		setActivated(gHandles["imgCurrentBG"], true)	
		setActivated(gHandles["imgCurrent"], true)
		resize(Control_GetLocationY(gHandles["btnUploadTxt"]) + Control_GetHeight(gHandles["btnUploadTxt"]))
	elseif g_param["type"] == "NUMBER" then
		local edVal = gHandles["edValue"]
		setActivated(edVal, true)
		Control_SetSize(edVal, NUM_LEN, Control_GetHeight(edVal))
		resize(Control_GetLocationY(gHandles["edValue"]) + Control_GetHeight(gHandles["edValue"]))
	elseif g_param["type"] == "STRING" then
		setActivated(gHandles["edValue"], true)
		resize(Control_GetLocationY(gHandles["edValue"]) + Control_GetHeight(gHandles["edValue"]))
	elseif g_param["type"] == "LONG_STRING" then
		setActivated(gHandles["edValueThreeLine"], true)
		resize(Control_GetLocationY(gHandles["edValueThreeLine"]) + Control_GetHeight(gHandles["edValueThreeLine"]))
	elseif g_param["type"] == "BOOL" then
		setActivated(gHandles["btnOn"], true)
		setActivated(gHandles["btnOff"], true)
		if g_param["value"] == true then
			btnOn_OnButtonClicked(gHandles["btnOn"])
		else 
			btnOff_OnButtonClicked(gHandles["btnOff"])	
		end
		resize(Control_GetLocationY(gHandles["btnOn"]) + Control_GetHeight(gHandles["btnOn"]))
	elseif g_param["type"] == "COORDINATE" then
		setActivated(gHandles["edX"], true)
		setActivated(gHandles["edY"], true)
		setActivated(gHandles["edZ"], true)
		setActivated(gHandles["btnCurrLoc"], true)
		setActivated(gHandles["stcCoordDir"], true)
		resize(Control_GetLocationY(gHandles["edX"]) + Control_GetHeight(gHandles["edX"]))
	elseif g_param["type"] == "PARTICLE_OFFSET" then
		setActivated(gHandles["edX"], true)
		setActivated(gHandles["edY"], true)
		setActivated(gHandles["edZ"], true)
	elseif g_param["type"] == "RADIAL" then
		g_choicesNum = #g_param["choices"]
		if g_choicesNum <=5 then 
			for i=1, g_choicesNum do
				local radctl = gHandles["radioButton"..tostring(i)]
				Static_SetText(radctl,g_param["choices"][i] )
				setActivated(radctl,true)
				if g_param["choices"][i] == g_radValue then
					RadioButton_SetChecked(radctl, true)
				else 
					RadioButton_SetChecked(radctl, false)
				end 
			end 
			resize(Control_GetLocationY(gHandles["radioButton"..tostring(g_choicesNum)]) + Control_GetHeight(gHandles["radioButton"..tostring(g_choicesNum)]))
		else 
			g_param["type"] = "DROPDOWN"
			Static_SetText(gHandles["stcDropdown"],g_radValue)
			dropDownActions()
		end 
	elseif g_param["type"] == "DROPDOWN" then
		dropDownActions()
	end
end

function dropDownActions()
	g_choicesNum = #g_param["choices"]
	for i=1, g_choicesNum do
		ListBox_AddItem(gHandles["lstDropdown"],tostring(g_param["choices"][i]),0)
	end 
		
	local dropDownctl = gHandles["lstDropdown"]
	local dropWidth = Control_GetWidth(dropDownctl)
		
	Control_SetSize(dropDownctl,dropWidth,(g_choicesNum*FONT_SIZE)+(LIST_PADDING*2) + MARGIN*(g_choicesNum-1))  
	setActivated(gHandles["imgDropdown"], true)
	setActivated(gHandles["stcDropdown"], true)	
	setActivated(gHandles["btnDropdown"], true)
	resize(Control_GetLocationY(gHandles["imgDropdown"]) + Control_GetHeight(gHandles["imgDropdown"]))
end 

function showUploadPage(show)
    if show == true then
		setActivated(gHandles["stcUpload"], true) 
		for i = 1, TEXTURES_PER_PAGE do
			setActivated(gHandles["txtIcon" .. tostring(i)], false)
			setActivated(gHandles["btnIcon" .. tostring(i)], false)
			setActivated(gHandles["imgFrame" .. tostring(i)], false)
		end
	else
		setActivated(gHandles["stcUpload"], false) 
		for i = 1, g_totalTex do
			setActivated(gHandles["txtIcon" .. tostring(i)], true)
			setActivated(gHandles["btnIcon" .. tostring(i)], true)
			setActivated(gHandles["imgFrame" .. tostring(i)], true)
		end
	end
end

function displayItemResultsWithIcon()
	ListBox_RemoveAllItems(gHandles["lstItems"])
	local listStart = 1
	
	if g_paramKey ~= DEFAULT_GLID_KEY then
		 if (g_page <= 1 or g_maxPage == 1) and g_searchON == false then
			-- Allow selecting no item for everything except default glid
			ListBox_AddItem(gHandles["lstItems"], "None", 0)
			setActivated(gHandles[iconName], true)
			local iconName = "imglistHover"
			local eh = Image_GetDisplayElement(gHandles[iconName])
			if eh ~= 0 then
				KEP_LoadIconTextureByID(0, eh, gDialogHandle)
			end
			listStart = 2
			g_selections = g_selections + 1
		end 
	end

	for i = listStart, ITEMS_PER_PAGE do
		local item = g_t_items[i + 1 - listStart]
		local iconName = "imglistHover"--"imgIcon" .. tostring(i)
		if item ~= nil then
			local displayName = item["name"]
			local iconPath = item["thumbnail_medium"]
			ListBox_AddItem(gHandles["lstItems"], displayName, item["GLID"])
			setActivated(gHandles[iconName], true)
			local eh = Image_GetDisplayElement(gHandles[iconName])
			if eh ~= 0 then
				KEP_LoadIconTextureByID(item.GLID, eh, gDialogHandle)
			end
		
			g_selections = g_selections + 1
			setActivated(gHandles[iconName], false)
		end
	end	
	
	local list = gHandles["lstItems"]
	local lstWidth = Control_GetWidth(list) 
	local lstHeight = Control_GetHeight(list)
	local points = {} 	
	local start = 1
	for j =1,(g_selections*2) do
		if (j%2)==1 then
			points[start] = j
			start = start + 1
		end 
	end

	for j = 1,g_selections do 
		g_midpts[j] = Control_GetLocationY(list) + (math.floor(lstHeight*(points[j]/10)))
	end 
end

function displayTextureResultsWithIcon()
	local listStart = 1
	local eh = Dialog_GetControl(gDialogHandle,"imgCurrent")
	if eh ~= nil then
	   ehB = Dialog_GetControl(gDialogHandle,"imgCurrentBG")
	   scaleImage(eh,ehB,g_textureSelect)
	end
	  
	local texStartY = Control_GetLocationY(eh) + Control_GetHeight(eh) + PADDING_SMALL

	for i = listStart, TEXTURES_PER_PAGE do
		local item = g_t_items[i]
		local iconName = "txtIcon" .. tostring(i)
		local btnName = "btnIcon" .. tostring(i)
		if item ~= nil then
            local fileName =  "../../CustomTexture/".. item["full_path"]
			setActivated(gHandles[iconName], true)
			setActivated(gHandles[btnName], true)
			setActivated(gHandles["imgFrame"..tostring(i)], true)

			local eh = Dialog_GetImage(gDialogHandle, iconName)
		        if eh ~= nil then
		           ehB = Dialog_GetControl(gDialogHandle, btnName)
		           scaleImage(eh,ehB,fileName)
			end
		else
			setActivated(gHandles[iconName], false)
			setActivated(gHandles[btnName], false)
			setActivated(gHandles["imgFrame"..tostring(i)], false)
		end
	end	
end

function populateObjectPlacements(page)
	ListBox_RemoveAllItems(gHandles["lstItems"])
	local startIndex = (page - 1) * ITEMS_PER_PAGE
	local doCount = KEP_GetDynamicObjectCount()
	local listStart = 1
	
	for i = startIndex, (startIndex + ITEMS_PER_PAGE - 1) do
		local iconName = "imglistHover"--"imgIcon" .. tostring(i - startIndex + 1)
		if i < doCount then
			local placementId = KEP_GetDynamicObjectPlacementIdByPos(i)
			local name, description, canPlayMovie, isAttachableObject, isInteractive, isLocal, isDerivable, assetId, friendId, customTexture, globalId, invType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(placementId)
			ListBox_AddItem(gHandles["lstItems"],name, globalId)
			local eh = Image_GetDisplayElement(gHandles[iconName])
			if eh ~= 0 then
				KEP_LoadIconTextureByID(globalId, eh, gDialogHandle)
			end
        	g_selections = g_selections + 1
			setActivated(gHandles[iconName], true)
			
		else
			setActivated(gHandles[iconName], false)
		end
	end
	
	local list = gHandles["lstItems"]
	local lstWidth = Control_GetWidth(list) 
	local lstHeight = Control_GetHeight(list)
	local points = {} 	
	local start = 1
	for j =1,(g_selections*2) do
		if (j%2)==1 then
			points[start] = j
			start = start + 1
		end 
	end

	for j = 1,g_selections do 
		g_midpts[j] = Control_GetLocationY(list) + (math.floor(lstHeight*(points[j]/10)))
	end 
end

function decodeTextureData( item_table, xmlData)
	
	-- Parse XML Return Code
	local returnCode = ParseXmlReturnCode(xmlData)
	if (returnCode == nil or returnCode ~= 0) then 
		LogError("decodeTextureData: Unable to obtain inventory from web")
		-- InfoBoxMsg("Unable to obtain inventory from web", returnCode)
		return
	end

	-- gfind renamed gmatch in future versions of LUA
	local s, e = 0
	local result = nil
	for block_text in string.gmatch( xmlData, "<Texture>(.-)</Texture>") do

		local t_item = {}
		local s, e, result = string.find(block_text, "<name>(.+)</name>")
		if result ~= nil then
			t_item["name"] = tostring(result)
		end

		s, e, result = string.find(block_text, "<thumbnail_medium_path>(.+)</thumbnail_medium_path>")
		if result ~= nil then
			t_item["thumbnail_medium"] = tostring(result)
		end

		local s, trash, asset_id = string.find(block_text, "<asset_id>([0-9]+)</asset_id>", 1)
		local s, trash, imageUrl = string.find(block_text, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>", 1)

		local full_path = KEP_DownloadTextureAsync( GameGlobals.CUSTOM_TEXTURE, asset_id, imageUrl, GameGlobals.THUMBNAIL )
		t_item["full_path"] = string.gsub(full_path,".-\\", "" )

		table.insert(item_table, t_item)
	end
end

function selectTexture(index)
        if g_param["type"] == "IMAGE" then
		g_selectedTexture = index
		if index > 0 and index <= TEXTURES_PER_PAGE then
			local item = g_t_items[index]
			local fileName =  "../../CustomTexture/".. item["full_path"]
			local iconPath = GameGlobals.WEB_SITE_PREFIX_IMAGES .. item["thumbnail_medium"]
			local eh = Dialog_GetControl(gDialogHandle,"imgCurrent")
		        if eh ~= nil then
		           ehB = Dialog_GetControl(gDialogHandle,"imgCurrentBG")
		           scaleImage(eh,ehB,fileName)
			end
			Static_SetText(gHandles["stcCurrentName"], string.match(iconPath,".*/(.-)$") )
			g_textureSelect = fileName
		end
	end
end

function setNewImageURL(imageURL)
	EditBox_SetText( gHandles["edValue"], GameGlobals.WEB_SITE_PREFIX_IMAGES .. imageURL, false )
	g_gotoLastPage = true
	textureWebCall()
end

function btnOn_OnButtonClicked(buttonHandle)
	setActivated(gHandles["imgOnClicked"],true)
	setActivated(gHandles["stcOnClicked"],true)
	setActivated(gHandles["imgOffClicked"],false)
	setActivated(gHandles["stcOffClicked"],false)
	g_param["value"] = true
end 

function btnOff_OnButtonClicked(buttonHandle)
	setActivated(gHandles["imgOnClicked"],false)
	setActivated(gHandles["stcOnClicked"],false)
	setActivated(gHandles["imgOffClicked"],true)
	setActivated(gHandles["stcOffClicked"],true)
	g_param["value"] = false 
end 

function btnOk_OnButtonClicked(buttonHandle)

	-- TODO complete list of params...
	if g_param["type"] == "MENS_EMOTE" or g_param["type"] == "WOMENS_EMOTE" or g_param["type"] == "ACCESSORY" or g_param["type"] == "OBJECT_ANIMATION" or g_param["type"] == "OBJECT" or g_param["type"] == "SOUND" or g_param["type"] == "MENS_CLOTHING" or g_param["type"] == "WOMENS_CLOTHING" or g_param["type"] == "PARTICLE" then
		g_param["value"] = g_selected
	elseif g_param["type"] == "IMAGE" then
		g_param["value"] = g_textureSelect -- as lua string
	elseif g_param["type"] == "STRING"  then 
		g_param["value"] = EditBox_GetText(gHandles["edValue"]) -- as lua string
	elseif g_param["type"] == "LONG_STRING" then 
		g_param["value"] = EditBox_GetText(gHandles["edValueThreeLine"]) -- as lua string
	elseif g_param["type"] == "NUMBER" then
		g_param["value"] = tonumber(EditBox_GetText(gHandles["edValue"]))
	elseif g_param["type"] == "BOOL" then
		if g_param["value"] == true then
			btnOn_OnButtonClicked(gHandles["btnOn"])
		else
			g_param["value"] = false 
			btnOff_OnButtonClicked(gHandles["btnOff"])
		end
	elseif g_param["type"] == "COORDINATE" then
		g_param["x"] = tonumber(EditBox_GetText(gHandles["edX"]))
		g_param["y"] = tonumber(EditBox_GetText(gHandles["edY"]))
		g_param["z"] = tonumber(EditBox_GetText(gHandles["edZ"]))
	elseif g_param["type"] == "PARTICLE_OFFSET" then
		g_param["x"] = tonumber(EditBox_GetText(gHandles["edX"]))
		g_param["y"] = tonumber(EditBox_GetText(gHandles["edY"]))
		g_param["z"] = tonumber(EditBox_GetText(gHandles["edZ"]))
	elseif g_param["type"] == "OBJECT_PLACEMENT" then
		g_param["value"] = g_selected -- global Id
	elseif g_param["type"] == "RADIAL" then
		for i = 1, g_choicesNum do 
			if CheckBox_GetChecked(RadioButton_GetCheckBox(gHandles["radioButton"..tostring(i)])) == 1 then 
				g_param["value"] = g_param["choices"][i]
			end 
		end 
		
	elseif g_param["type"] == "DROPDOWN" then
		g_param["value"] = Static_GetText(gHandles["stcDropdown"])
	end

	local event = KEP_EventCreate( CHANGE_PARAM_EVENT )
	ActionItemFunctions.encodeParam(event, g_param, g_paramKey)
	KEP_EventQueue( event )

	MenuCloseThis()
end

function btnIcon1_OnButtonClicked( buttonHandle )
	selectTexture(1)
end

function btnIcon2_OnButtonClicked( buttonHandle )
	selectTexture(2)
end

function btnIcon3_OnButtonClicked( buttonHandle )
	selectTexture(3)
end

function btnIcon4_OnButtonClicked( buttonHandle )
	selectTexture(4)
end

function btnIcon5_OnButtonClicked( buttonHandle )
	selectTexture(5)
end

function btnIcon6_OnButtonClicked( buttonHandle )
	selectTexture(6)
end

function btnIcon7_OnButtonClicked( buttonHandle )
	selectTexture(7)
end

function btnIcon8_OnButtonClicked( buttonHandle )
	selectTexture(8)
end

function btnIcon9_OnButtonClicked( buttonHandle )
	selectTexture(9)
end

function btnIcon10_OnButtonClicked( buttonHandle )
	selectTexture(10)
end

function btnIcon11_OnButtonClicked( buttonHandle )
	selectTexture(11)
end

function btnIcon12_OnButtonClicked( buttonHandle )
	selectTexture(12)
end

function btnUploadTxt_OnButtonClicked( buttonHandle )
	local event = KEP_EventCreate( FILE_BROWSER_CLOSED_EVENT )
	KEP_EventSetObjectId( event, 0 )
	KEP_EventSetFilter( event, UGC_TYPE.TEXTURE)
	UGC_BrowseFileByImportType( UGC_TYPE.TEXTURE, event )
end

function lstDropdown_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	local name = ListBox_GetItemText( listBoxHandle, sel_index )
	Static_SetText(gHandles["stcDropdown"], name)
	setActivated(gHandles["lstDropdown"], false)
end

function btnDropdown_OnButtonClicked(buttonHandle)
	local dropdown = gHandles["lstDropdown"]
	setActivated(dropdown, true)
	Control_SetFocus(dropdown)
end 

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	if Control_ContainsPoint(gHandles["lstDropdown"], x, y) == 0 then
		setActivated(gHandles["lstDropdown"], false)
	end
	if Control_ContainsPoint(gHandles["txtSearch"], x, y) == 0 then
		txtSearch_OnFocusOut(gHandles["txtSearch"])
	end
end

function btnPageNext_OnButtonClicked(buttonHandle)

	ListBox_ClearSelection(gHandles["lstItems"])
	g_selections = 0 
	g_midpts = {}
	g_page = g_page + 1
    if g_page <= 1 or g_maxPage == 1 then
		setActivated(gHandles["btnPagePrev"], false)
		setActivated(gHandles["imgPagePrev"], true)
    else
        setActivated(gHandles["btnPagePrev"], true)
		setActivated(gHandles["imgPagePrev"], false)
    end

	if g_page >= g_maxPage then
		g_page = g_maxPage
		setActivated(gHandles["btnPageNext"], false)
		setActivated(gHandles["imgPageNext"], true)
	end

	if g_param["type"] == "OBJECT_PLACEMENT" then
		populateObjectPlacements(g_page)
	elseif g_param["type"] == "IMAGE" then
		textureWebCall()
	else
		inventoryWebCall()
	end
end

function btnPagePrev_OnButtonClicked(buttonHandle)

	ListBox_ClearSelection(gHandles["lstItems"])
	g_selections = 0 
	g_midpts = {}
	g_page = g_page - 1
    if g_page < g_maxPage then 
        setActivated(gHandles["btnPageNext"], true)
		setActivated(gHandles["imgPageNext"], false)
    end

	if g_page <= 1 then
		g_page = 1
		setActivated(gHandles["btnPagePrev"], false)
		setActivated(gHandles["imgPagePrev"], true)
	end

	if g_param["type"] == "OBJECT_PLACEMENT" then
		populateObjectPlacements(g_page)
	elseif g_param["type"] == "IMAGE" then
		textureWebCall()
	else
		inventoryWebCall()
	end
end

function lstItems_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	local itemIndex = ListBox_GetItemData( listBoxHandle, sel_index )
	local eh = Image_GetDisplayElement(gHandles["imgCurrent"])
	if eh ~= 0 then
		KEP_LoadIconTextureByID(itemIndex, eh, gDialogHandle)
	end
	local name = ListBox_GetItemText( listBoxHandle, sel_index )
	Static_SetText(gHandles["stcCurrentName"], name)
	g_selected = itemIndex
end

function btnCurrLoc_OnButtonClicked(btnHandle)
	if Control_GetVisible(btnHandle) then
		local x,y,z,rot = KEP_GetPlayerPosition()
		EditBox_SetText( gHandles["edX"], string.format("%2.4g", x), false )
		EditBox_SetText( gHandles["edY"], string.format("%2.4g", y), false )
		EditBox_SetText( gHandles["edZ"], string.format("%2.4g", z), false )
	end
	showSet = true 
	g_time = 0
end

function txtSearch_OnEditBoxChange( editboxHandle, text)
	g_searchText = text
	if g_searchText then
		local searchLen = string.len(g_searchText)
		if searchLen > 0 then
			setActivated(gHandles["imgClearSearch"],true)
			g_searchON = true  
			g_page = 1
			setActivated(gHandles["btnPagePrev"], false)
			setActivated(gHandles["imgPagePrev"], true)
			
			if g_page == g_maxPage then 
				setActivated(gHandles["btnPageNext"], false)
				setActivated(gHandles["imgPageNext"], true)
			else 
				setActivated(gHandles["btnPageNext"], true)
				setActivated(gHandles["imgPageNext"], false)
			end 
		else
			g_searchON = false 
		end
		if searchLen ~= 1 then
			inventoryWebCall()			
		end
	end
end

function txtSearch_OnFocusIn(editBoxHandle)

	-- On enter, set the text to blank
	if g_clearSearch then
		EditBox_SetText(editBoxHandle, "", true)
	end
	g_clearSearch = false
end

function txtSearch_OnFocusOut(editBoxHandle)
	
	-- On exit, if blank, reset the text to "Search|"
	local search = EditBox_GetText(editBoxHandle)
	if search == "" or g_clearSearch then
		g_clearSearch = true
		EditBox_SetText(editBoxHandle, "Search", false)
	else
		g_clearSearch = false
	end
end

function imgClearSearch_OnLButtonUp(ctrlHandle)
	if Control_GetVisible(ctrlHandle) then
		Control_SetVisible(ctrlHandle, false)
		g_clearSearch = true
		local searchBox = gHandles["txtSearch"]
		EditBox_ClearText(searchBox)
		txtSearch_OnFocusOut(searchBox)
		txtSearch_OnEditBoxChange( searchBox, "")
	end
end
