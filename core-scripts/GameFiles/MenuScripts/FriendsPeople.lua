--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("PageButtons.lua")
dofile("..\\Scripts\\Progressive.lua")
dofile("MenuLocation.lua")

BUTTON_PADDING = 8.5

PRIVATE_MESSAGE_EVENT = "PrivateMessageEvent"
PLAYER_USERID_EVENT = "PlayerUserIdEvent"
YES_NO_BOX_EVENT = "YesNoBoxEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"
IN_WORLD_INVITE_EVENT = "InWorldInviteEvent"
TYPE_START_EVENT = "StartTypingEvent"

-- URLs for website queries
IN_ZONE_SUFFIX = "kgp/peopleInGame.aspx?&action=getZone&zid="
IN_ZONE_CIZ_SUFFIX = "&czi="
IN_ZONE_SEARCH_SUFFIX = "&search="
IN_ZONE_SORTNAME_SUFFIX = "&sortbyname=T"
IN_ZONE_SORTLOGON_SUFFIX = "&sortbylogon=T"
FRIENDS_SUFFIX = "kgp/friendsList.aspx"
SEARCH_SUFFIX = "kgp/friendsList.aspx?search="

-- Sorted by zone then name
PEOPLE_ZONE_SUFFIX = "kgp/peopleInGame.aspx"
-- Sort by name only
PEOPLE_NAME_SUFFIX = "kgp/peopleInGame.aspx?sortbyname=T"
PEOPLE_LOGIN_SUFFIX = "kgp/peopleInGame.aspx?sort=login"

-- Search for user
PEOPLE_SEARCH_SUFFIX = "kgp/peopleInGame.aspx?search="
UPDATE_SUFFIX = "kgp/updateFriend.aspx?userId="
ACTION_SUFFIX = "&action="
ONLINE_SUFFIX = "?online=T"
LOGON_SORT_SUFFIX = "?sortbylogon=T"

-- Params
USER_ID = "&userId="

-- Menu Tabs
TAB_FRIENDS = 0
TAB_PEOPLE = 1
TAB_IGNORE = 2
TAB_INZONE = 3

-- Buttons requiring a message box
BTN_NONE = 0
BTN_FRIEND = 1
BTN_IGNORE = 2

-- Web address for query
g_web_address = nil

-- Current tab selected (Friends, People, or Ignore)
g_current_tab = TAB_PEOPLE

-- Hold current friend data returned from server
g_friendData = ""

-- Total number of friend records
g_totalNumRec = 0

-- Maximum number of friends for server to return in one request friends per page
g_friendsPerPage = 10

g_selectedIndex = -1
g_t_friends = {}

-- Some calls to get browser page will return only success or failure info.
-- This is used to pick our parse function.
g_resultParse = false

-- Which button was pressed.  This is used to decide on the status message after an action
g_buttonPressed = BTN_NONE

-- My character's name
g_myName = ""
g_currentZoneInstanceId = 0
g_currentZoneIndex = 0
g_currentGameId = 0
g_isGM = false

g_sortType = LOGON_SORT_SUFFIX

g_Tooltips = {}

g_trackingData = ""
g_trackingRequestId = ""

g_friendsTab = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "textHandler", "RenderTextEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "FriendsTabEventHandler", "FriendsTabEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "answerEventHandler", YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( TYPE_START_EVENT, KEP.LOW_PRIO )
	KEP_EventRegister( YES_NO_BOX_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
end

function FriendsTabEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

end 

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = KEP_EventDecodeNumber( event )
	if answer ~= 0 then
		if g_buttonPressed == BTN_FRIEND then
			if g_current_tab == TAB_FRIENDS then
           		sendFriendAction("Remove")
           	elseif g_current_tab == TAB_PEOPLE or g_current_tab == TAB_INZONE then
           		sendFriendAction("Add")
           	end
  		end
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX

	local t = { KEP_GetPlayerInfo() }
	if t ~= 0  then
	 	-- index 10 = name
		temp = t[10+1]
		if temp ~= nil then
			g_myName = temp
		end
	end

    g_currentZoneInstanceId = KEP_GetZoneInstanceId()
    g_currentZoneIndex = KEP_GetZoneIndex()
    g_currentGameId = KEP_GetGameId()
	g_currentZoneIndex = InstanceId_GetClearedInstanceId(g_currentZoneIndex)

	g_sortType = getSettings()

	loadLocation(gDialogHandle, "FP")

	initButtons() -- pagination

	-- setSelectionColor is called from PageButtons.lua to highlight the first button when the menu is opened.
	setSelectionColor(gHandles["btnRange1"])

	hideNavControls()

	btnPeople_OnButtonClicked( gHandles["btnPeople"] )
	Dialog_SetCaptionText(gDialogHandle, "    "..Dialog_GetCaptionText(gDialogHandle))

	initTooltips()
	
	makeWebCall( GameGlobals.WEB_SITE_PREFIX.."kgp/userProfile.aspx?action=getOwnershipSummary", WF.GET_USER, 0, 8)
end

-- return boolean noting whether all friends or
-- only online friends should be shown
function getSettings()
	local sortType = ""
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		local okfilter, checked = KEP_ConfigGetString( config, "FPShowOnline", "false" )
		if okfilter then
			if checked == "true" then
			    sortType = ONLINE_SUFFIX
			    checked = true
			else
			    sortType = LOGON_SORT_SUFFIX
			    checked = false
			end
			CheckBox_SetChecked( gHandles["cbOffline"], checked )
		end
	end
	return sortType
end

function saveSettings()

	-- Get current state of checkbox
	local cbState = CheckBox_GetChecked( gHandles["cbOffline"] )
	if cbState ~= 1 then
		cbState = false
	else
	   	cbState = true
	end

	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetString( config, "FPShowOnline", tostring(cbState) )
		KEP_ConfigSave( config )
	end
end

function Dialog_OnDestroy(dialogHandle)
	saveSettings()
	saveLocation(dialogHandle, "FP")
	Helper_Dialog_OnDestroy( dialogHandle )
end

function setStatusMessage(msg)
	Static_SetText(gHandles["lblLoading"], msg)
	Control_SetVisible(gHandles["lblLoading"], n ~= "")
end

-- clearResultSet
--   Empties out the rows of the 2 listboxes which show search results
function clearResultSet()
	ListBox_RemoveAllItems(gHandles["lstFriendsDummy"])
	ListBox_RemoveAllItems(gHandles["lstFriends"])
	ListBox_RemoveAllItems(gHandles["lstOnlineDummy"])
	ListBox_RemoveAllItems(gHandles["lstOnline"])
	Static_SetText(gHandles["lblResults"], "")
	setStatusMessage("Loading ...")
	hideNavControls()
end

function textHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local msg   = KEP_EventDecodeString( event )
	local type  = KEP_EventDecodeNumber( event )
	s, e = string.find(msg, "Travel")
	if not(s == 1 and type == ChatType.System) then
		local bh = Dialog_GetButton(gDialogHandle, "btnTravel")
		setButtonState(bh, true)
	end
	return KEP.EPR_OK
end

function hideNavControls()
	for iRangeBtnIndex=1,4 do
		Control_SetVisible(gHandles["btnRange"..iRangeBtnIndex], false)
	end
	Control_SetVisible(gHandles["btnFirst"], false)
	Control_SetVisible(gHandles["btnLast"], false)
	Control_SetVisible(gHandles["btnResultsNext"], false)
	Control_SetVisible(gHandles["btnResultsPrev"], false)
end

function handlePages()
	local iTotalNumPages = math.ceil(tonumber(g_totalNumRec/g_friendsPerPage))
	local btn = nil
	hideNavControls()
	if(iTotalNumPages > 1) then
		Control_SetVisible(gHandles["btnRange1"], true)
		Control_SetVisible(gHandles["btnRange2"], true)
		Control_SetVisible(gHandles["btnResultsPrev"], true)
		Control_SetVisible(gHandles["btnResultsNext"], true)
	end

	if(iTotalNumPages > 2) then
		Control_SetVisible(gHandles["btnRange3"], true)
	end

	if (iTotalNumPages > 3) then
		Control_SetVisible(gHandles["btnRange4"], true)
	end

	if(iTotalNumPages > 4) then
		Control_SetVisible(gHandles["btnFirst"], true)
		Control_SetVisible(gHandles["btnLast"], true)
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == WF.FRIENDS then
		setStatusMessage("")
		g_friendData = KEP_EventDecodeString( event )
		if g_resultParse then
			ParseResults()
		else
			if g_current_tab == TAB_FRIENDS then
		    	ParseFriendData()
			elseif g_current_tab == TAB_PEOPLE then
			    ParsePeopleData()
			elseif g_current_tab == TAB_IGNORE then
			    ParseIgnoreData()
			elseif g_current_tab == TAB_INZONE then
				ParseInZoneData()
			end

			clearSelection()
		end
	    handlePages()
		return KEP.EPR_CONSUMED
	elseif filter ==  WF.TRACKING_REQ_GAME then
		g_trackingData = KEP_EventDecodeString( event )
		ParseTrackingData()
		return KEP.EPR_CONSUMED
	elseif filter == WF.GET_USER then
		local accessData = KEP_EventDecodeString( event )
		local s, strPos, result
		s, strPos, result = string.find(accessData, "<ReturnCode>(.-)</ReturnCode>")
		if result == "0" then
			s, strPos, result = string.find(accessData, "<GM>(.-)</GM>", strPos)
			g_isGM = strToBool(result)
			s, strPos, result = string.find(accessData, "<i_own_moderate>(.-)</i_own_moderate>", strPos)		
			if g_current_tab == TAB_PEOPLE then
			    ParsePeopleData()
			end 
		end

	elseif filter == WF.BLOCK then
		local s, e = 1
		local resultCode = ""
		blockData = KEP_EventDecodeString( event )
		s, e, resultCode = string.find(blockData, "<ReturnCode>(.-)</ReturnCode>")
		parseBlockData(blockData)
		btnIgnored_OnButtonClicked(gHandles["btnIgnored"])
		return KEP.EPR_CONSUMED
	end
end

function ParseResults()
	local resultDesc = ""  -- result description
	local s, e = 0      -- start and end of captured string
	local resulCode = -1
	local name = g_t_friends[g_selectedIndex]["name"]
	local userId = g_t_friends[g_selectedIndex]["userID"]
	s, e, resultCode = string.find(g_friendData, "<ReturnCode>(.-)</ReturnCode>")
	s, e, resultDesc = string.find(g_friendData, "<ResultDescription>(.-)</ResultDescription>")

	if resultCode == "0" then
		if g_buttonPressed == BTN_FRIEND then
		    if g_current_tab == TAB_FRIENDS then
				KEP_MessageBox(name..RM.REMOVE_FRIEND..".")
				clearSelection()
		    else
				KEP_MessageBox(RM.FRIEND_REQUEST..name..".")
		    end
		elseif g_buttonPressed == BTN_IGNORE then
			if g_current_tab == TAB_IGNORE then
				KEP_MessageBox(RM.REMOVE_IGNORE..name..".")
				--Refresh ignore list after removing someone
				clearSelection()
				local web_address = GameGlobals.WEB_SITE_PREFIX..LIST_BLOCKED_USERS_SUFFIX
				clearResultSet()
				makeWebCall( web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
			else
				KEP_MessageBox(RM.ADD_IGNORE..name..".")
			end
		end
	elseif resultCode == "1" and g_current_tab == TAB_PEOPLE then
		if resultDesc == "already friends" then
			KEP_MessageBox(RM.ALREADY_FRIENDS..g_t_friends[g_selectedIndex]["name"]..".")
		elseif resultDesc == "already requested to be friends" then
			KEP_MessageBox(RM.FRIEND_REQUEST_SENT..g_t_friends[g_selectedIndex]["name"]..".")
		end
	end

    g_resultParse = false
    g_buttonPressed = nil
end

function ParseIgnoreData()
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local user = "" 		-- username
	local playerID = 0    -- WOK id of player
	local userID = 0      -- Kaneva id of player
	local blockDate = "" 	-- date user blocked
	local blockTime = "" 	-- time user blocked
	s, strPos, result = string.find(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		numRec = calculateRanges()

		local lbhD = gHandles["lstFriendsDummy"]
		local lbh = gHandles["lstFriends"]
		local lbhOnD = gHandles["lstOnlineDummy"]
		local lbhOn = gHandles["lstOnline"]

		if (lbhD ~= nil) and (lbh ~= nil) and (lbhOnD ~= nil) and (lbhOn ~= nil) then
			ListBox_RemoveAllItems(lbhD)
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOnD)
			ListBox_RemoveAllItems(lbhOn)

			g_t_friends = {}
			for i=1,numRec do
				s, strPos, userID = string.find(g_friendData, "<block_owner>([0-9]+)</block_owner>", strPos)
				s, strPos, blockeeID = string.find(g_friendData, "<blocked_user_id>([0-9]+)</blocked_user_id>", strPos)
				s, strPos, blockDate = string.find(g_friendData, "<blocked_date>(%w+%-%w+%-%w+)T%w+:%w+:%w+", strPos)
				s, strPos, user = string.find(g_friendData, "<username>(.-)</username>", strPos)

				local friend = {}
				friend["name"] = user
				friend["userID"] = blockeeID
				friend["blockDate"] = blockDate
				table.insert(g_t_friends, friend)

                local blockSettings = blockDate

				ListBox_AddItem(lbhD, "", blockeeID)
				ListBox_AddItem(lbh, user, blockeeID)
                ListBox_AddItem(lbhOnD, "", blockeeID)
                ListBox_AddItem(lbhOn, blockSettings, blockeeID)
			end
  			alignButtons()
		end
	else
	  	-- Go back to beginning to grab description
		s, e, result = string.find(g_friendData, "<ReturnDescription>(%w+)</ReturnDescription>", 1)
		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")
		if sh ~= nil then
			Static_SetText(sh, result)
	    end
	end
end

-- Parse people in zone data
function ParsePeopleData()
	local numRec = 0   	 	-- number of records in this chunk
	local result = ""  		-- return string from find temp
	local user = "" 		-- username
	local playerID = 0    -- WOK id of player
	local userID = 0      -- Kaneva id of player
	local zone = ""	-- Zone player is currently located in
	local gameID = 0
	local gameName = ""
	local online = ""
	local imOnline = ""
	local blocked = ""
	local isGm = ""

	result = string.match(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")

	if result == "0" then
		numRec = calculateRanges()

		local lbhD = gHandles["lstFriendsDummy"]
		local lbh = gHandles["lstFriends"]
		local lbhOnD = gHandles["lstOnlineDummy"]
		local lbhOn = gHandles["lstOnline"]

		if (lbhD ~= nil) and (lbh ~= nil) and (lbhOnD ~= nil) and (lbhOn ~= nil) then
			g_t_friends = {}
			ListBox_RemoveAllItems(lbhD)
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOnD)
			ListBox_RemoveAllItems(lbhOn)

			for userdata in string.gmatch(g_friendData, "<GameUser>(.-)</GameUser>") do
				user = string.match(userdata, "<name>(.-)</name>")
				playerID = string.match(userdata, "<player_id>([0-9]+)</player_id>")
				userID = string.match(userdata, "<kaneva_user_id>([0-9]+)</kaneva_user_id>")
				zone = string.match(userdata, "<display_name>(.-)</display_name>")
				blocked = string.match(userdata, "<comm_blocked>([0-9]+)</comm_blocked>")
				gameID = string.match(userdata, "<game_id>(.-)</game_id>")
				gameName = string.match(userdata, "<game_name>(.-)</game_name>")
  				online = string.match(userdata, "<online>(%w+)</online>")
  				imOnline = string.match(userdata, "<im_online>(%w+)</im_online>")
				isGm = string.match(userdata, "<is_gm>(%w+)</is_gm>")

				local location = ""
				if online == "T" then
	    			online = "online"
					location = zone
		    	elseif imOnline == "1" then
	    			online = "IM only"
					location = online
				else 
					online = ""
				end

				-- Sanity check to make sure we don't have an invalid user.
				if online == "" then
					LogError("ParsePeopleData: Unknown user status.")
				else
					local friend = {}
					friend["ID"] = tonumber(playerID)
					friend["name"] = user
					friend["userID"] = tonumber(userID)
					friend["zone"] = zone
					friend["gameID"] = tonumber(gameID)
					friend["gameName"] = gameName
					friend["online"] = online
					friend["imOnline"] = imOnline
					friend["blocked"] = ((blocked or "0") == "1")
					friend["isGm"] = ((isGm or "F") == "T")

					table.insert(g_t_friends, friend)

					ListBox_AddItem(lbhD, "", userID)
					ListBox_AddItem(lbh, user, userID)
					ListBox_AddItem(lbhOnD, "", userID)
					ListBox_AddItem(lbhOn, location, userID)
				end
			end
      		alignButtons()
		end
   else
	  	-- Go back to beginning to grab description
		result = string.match(g_friendData, "<ReturnDescription>(%w+)</ReturnDescription>")
		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")
		if sh ~= nil then
			Static_SetText(sh, result)
	    end
	end
end

-- Parse people in zone data
function ParseInZoneData()
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local user = "" 		-- username
	local playerID = 0    -- WOK id of player
	local userID = 0      -- Kaneva id of player
	local zone = ""	-- Zone player is currently located in
	local blocked = "" -- Whether or not the user is blocked (0 or 1)
	local isGm = "" -- Whether or not the user is a GM
	s, strPos, result = string.find(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		numRec = calculateRanges()
		local lbhD = gHandles["lstFriendsDummy"]
		local lbh = gHandles["lstFriends"]
		local lbhOnD = gHandles["lstOnlineDummy"]
		local lbhOn = gHandles["lstOnline"]

		if (lbhD ~= nil) and (lbh ~= nil) and (lbhOnD ~= nil) and (lbhOn ~= nil) then
			g_t_friends = {}
			ListBox_RemoveAllItems(lbhD)
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOnD)
			ListBox_RemoveAllItems(lbhOn)

			for i=1,numRec do
				s, strPos, user = string.find(g_friendData, "<name>(.-)</name>", strPos)
				s, strPos, playerID = string.find(g_friendData, "<player_id>([0-9]+)</player_id>", strPos)
				s, strPos, userID = string.find(g_friendData, "<kaneva_user_id>([0-9]+)</kaneva_user_id>", strPos)
				--s, strPos, zone = string.find(g_friendData, "<current_zone>(.-)%.", strPos)
				s, strPos, zone = string.find(g_friendData, "<display_name>(.-)</display_name>", strPos)
				s, strPos, blocked = string.find(g_friendData, "<comm_blocked>([0-9]+)</comm_blocked>", strPos)
				s, strPos, isGm = string.find(g_friendData, "<is_gm>(%w+)</is_gm>", strPos)

				local friend = {}
				friend["ID"] = tonumber(playerID)
				friend["name"] = user
				friend["userID"] = tonumber(userID)
				friend["zone"] = zone
				friend["blocked"] = ((blocked or "0") == "1")
				friend["isGm"] = ((isGm or "F") == "T")
				table.insert(g_t_friends, friend)

				ListBox_AddItem(lbhD, "", userID)
				ListBox_AddItem(lbh, user, userID)
				ListBox_AddItem(lbhOnD, "", userID)
				ListBox_AddItem(lbhOn, zone, userID)
			end
			alignButtons()
		end
	else
		-- Go back to beginning to grab description
		s, e, result = string.find(g_friendData, "<ReturnDescription>(%w+)</ReturnDescription>", 1)
		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")
		if sh ~= nil then
			Static_SetText(sh, result)
		end
	end
end

function ParseTrackingData()

	s, strPos, requestId = string.find(g_trackingData, "<Result><RequestId>(.-)</RequestId></Result>")
	s2, strPos2, requestIdSent = string.find(g_trackingData, "<Result><RequestTypeSentId>(.-)</RequestTypeSentId></Result>")

	if requestId == "ERROR" then
		LogError( "ParseTrackingData: Unable to obtain tracking request for invite")
		g_trackingRequestId = tostring(0)
		return
	elseif requestIdSent == "ERROR" then
		LogError( "ParseTrackingData: Unable to obtain tracking request for invite sent")
		g_trackingRequestId = tostring(0)
		return
	end

	if requestId ~= nil then
		g_trackingRequestId = requestId
		Log( "ParseTrackingData: trackingId="..g_trackingRequestId)
		local userId = g_t_friends[g_selectedIndex]["userID"] -- sending the invite to this user
		local web_address = GameGlobals.WEB_SITE_PREFIX..TRACKING_REQUEST_SENT_SUFFIX..TRACKING_REQUEST_INWORLD_INVITE..TRACKING_REQUEST_REQUESTID_PARAM..g_trackingRequestId..TRACKING_REQUEST_USERID_PARAM..userId.."&route=true"
		makeWebCall( web_address, WF.TRACKING_REQ_GAME, 1, 1)
		Log( "ParseTrackingData: Invite sent tracking:"..web_address)
	elseif requestIdSent ~= nil then
		g_trackingRequestId = requestIdSent
		Log( "ParseTrackingData: Sent trackingId="..g_trackingRequestId)
		local ev = KEP_EventCreate( IN_WORLD_INVITE_EVENT)
		KEP_EventEncodeString( ev, g_t_friends[g_selectedIndex]["name"])
		KEP_EventEncodeString( ev, "")
		KEP_EventEncodeString( ev, "")
		KEP_EventEncodeString( ev, g_trackingRequestId)
		KEP_EventEncodeNumber( ev, g_currentGameId)
		KEP_EventQueue( ev )
	end
end

-- Parse friend data from server
function ParseFriendData()
	local numRec = 0   	 	-- number of records in this chunk
	local result = ""  		-- return string from find temp
	local user = "" 		-- username
	local playerID = 0    -- WOK id of player
	local userID = 0      -- Kaneva id of player
	local gameID = 0      -- Game id that the player is on
	local glueDate = "" 	-- date user became friend
	local glueTime = "" 	-- time user became friend
	local online = ""   	-- online status of user
	local imOnline = ""
	local lastOnline = "" 	-- last time user was online
	local isGm = ""			-- Whether or not the user is a GM
	result = string.match(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
	    numRec = calculateRanges()
		local lbhD = gHandles["lstFriendsDummy"]
		local lbh = gHandles["lstFriends"]
		local lbhOnD = gHandles["lstOnlineDummy"]
		local lbhOn = gHandles["lstOnline"]

		if (lbhD ~= nil) and (lbh ~= nil) and (lbhOnD ~= nil) and (lbhOn ~= nil) then

			g_t_friends = {}

			ListBox_RemoveAllItems(lbhD)
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOnD)
			ListBox_RemoveAllItems(lbhOn)

			for userdata in string.gmatch(g_friendData, "<Friend>(.-)</Friend>") do
  				online = string.match(userdata, "<online>(%w+)</online>")
  				imOnline = string.match(userdata, "<im_online>(%w+)</im_online>")
  				gameID = string.match(userdata, "<game_id>([0-9]+)</game_id>")
				user = string.match(userdata, "<username>(.-)</username>")
				userID = string.match(userdata, "<user_id>([0-9]+)</user_id>")
				playerID = string.match(userdata, "<player_id>([0-9]+)</player_id>")
				glueDate, glueTime = string.match(userdata, "<glued_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)")
				isGm = string.match(userdata, "<is_gm>(%w+)</is_gm>")
				zone = string.match(userdata, "<zone_display_name>(.-)</zone_display_name>")

				if playerID ~= "0"  then
					lastOnline = string.match(userdata, "<hours_since_last_logon>(%d+)</hours_since_last_logon>")
					lastOnline = tonumber(lastOnline)
					if (lastOnline ~= nil) and (lastOnline > 24) then
					    lastOnline = math.floor(lastOnline/24)
						if lastOnline == 1 then
							lastOnline = "Yesterday"
						else
						    lastOnline = lastOnline.." days ago"
						end
					elseif lastOnline == 1 then
					    	lastOnline = lastOnline.." hour ago"
					elseif lastOnline == 0 then
							lastOnline = "Under 1 hour ago"
					else
						if(lastOnline == nil) then
							lastOnline = "No logons"
						else
					    	lastOnline = lastOnline.." hours ago"
					    end
					end
				else
					lastOnline = "No logons"
				end

				local location = ""
				if online == "T" then
	    			online = "online"
					location = zone
		    	elseif imOnline == "1" then
	    			online = "IM only"
					location = online
				else 
					online = lastOnline
					location = online
				end

				local friend = {}
				friend["ID"] = playerID
				friend["name"] = user
				friend["date"] = glueDate
				friend["time"] = glueTime
				friend["online"] = online
				friend["imOnline"] = imOnline
				friend["userID"] = tonumber(userID)
				friend["gameID"] = tonumber(gameID)
				friend["isGm"] = ((isGm or "F") == "T")
				friend["zone"] = zone

				table.insert(g_t_friends, friend)

				if (userID ~= nil) then
					ListBox_AddItem(lbhD, "", userID)
					ListBox_AddItem(lbh, user, userID)
					ListBox_AddItem(lbhOnD, "", userID)
					ListBox_AddItem(lbhOn, location, userID)
				end
			end
  			alignButtons()
		end
	else
	  	-- Go back to beginning to grab description
		s, e, result = string.match(g_friendData, "<ReturnDescription>(%w+)</ReturnDescription>")
		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")
		if sh ~= nil then
			Static_SetText(sh, result)
	    end
	end

end

function parseBlockData( blockData )
	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	s, strPos, result = string.find(blockData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then	
		Log("parseBlockData: Unblock OK")
	end
end

-- Used to fill the buttons with the correct numerical text
function calculateRanges()
	local numRec = 0  --number of records returned
	local s, e  -- start and end indices of found string
	local resultsstr = "Showing " -- text for result label
	local resultslbl = Dialog_GetStatic( gDialogHandle, "lblResults")
    local high = g_curPage * g_friendsPerPage
	local low = ((g_curPage - 1) * g_friendsPerPage) + 1
	s, e, g_totalNumRec = string.find(g_friendData, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
	s, e, numRec = string.find(g_friendData, "<NumberRecords>([0-9]+)</NumberRecords>")
	g_totalNumRec = tonumber(g_totalNumRec)
	g_pageCap = math.ceil(g_totalNumRec / g_friendsPerPage)
	g_setCap = math.ceil(g_pageCap / g_window)

    if high > g_totalNumRec then
	    high = g_totalNumRec
	end

	if numRec == "0" then
	    low = numRec
	end

	if g_isGM then 
		if resultslbl ~= nil then
			local resultStr = ""
			if (g_totalNumRec > 0) then
				resultStr = resultStr..low.." - "..high.." of "..g_totalNumRec
			end
			Static_SetText(resultslbl, resultStr)
		end
	end 
	
	if (g_totalNumRec == 0) then
		setStatusMessage("No Results")
	end

	if g_InitBtnText then
		setButtonText()
		g_InitBtnText = false
	end
	
	return numRec
	
end

-- List Box selection
function updateSelection(sel_index)
	g_selectedIndex = sel_index + 1
	if (ListBox_GetSelectedItemIndex(gHandles["lstFriendsDummy"]) ~= sel_index) then
		ListBox_SelectItem( gHandles["lstFriendsDummy"], sel_index )
	end
	if (ListBox_GetSelectedItemIndex(gHandles["lstFriends"]) ~= sel_index) then
		ListBox_SelectItem( gHandles["lstFriends"], sel_index )
	end
	if (ListBox_GetSelectedItemIndex(gHandles["lstOnlineDummy"]) ~= sel_index) then
		ListBox_SelectItem( gHandles["lstOnlineDummy"], sel_index )
	end
	if (ListBox_GetSelectedItemIndex(gHandles["lstOnline"]) ~= sel_index) then
		ListBox_SelectItem( gHandles["lstOnline"], sel_index )
	end

	updateOnlineButton()
	updateIgnoreButton()
end

-- Disable the "Go" button if player is not online
function updateOnlineButton()
	--We only see offline people in friends tab
	local offline = (g_selectedIndex >= 0) and (g_t_friends[g_selectedIndex]["online"] ~= "online")
	local offChat = (g_selectedIndex >= 0) and (g_t_friends[g_selectedIndex]["imOnline"] ~= "1")
	Control_SetEnabled(gHandles["btnTravel"], not offline)
	Control_SetEnabled(gHandles["btnChat"], not (offline and offChat))
end

function updateIgnoreButton()
	local isBlocked = (g_selectedIndex >= 0 and g_t_friends[g_selectedIndex]["blocked"])
	local isGm = (g_selectedIndex >= 0 and g_t_friends[g_selectedIndex]["isGm"])
	local hasUserID = (g_selectedIndex >= 0 and g_t_friends[g_selectedIndex]["userID"])
	local isMyUser = (g_selectedIndex >= 0 and g_t_friends[g_selectedIndex]["name"] == g_myName)
	Control_SetEnabled(gHandles["btnIgnore"], (hasUserID and not isGm and not isBlocked and not isMyUser))
end

function lstFriendsDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	updateSelection(sel_index)
end

function lstFriends_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	updateSelection(sel_index)
end

function lstOnlineDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	updateSelection(sel_index)
end

function lstOnline_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	updateSelection(sel_index)
end

function btnSortOnline_OnButtonClicked( buttonHandle )
	if g_current_tab == TAB_IGNORE then
		--TODO: Implement SortOnline for Blocked tab
		return
	end

	clearSelection()

	if g_current_tab == TAB_FRIENDS then
	    local cbh = gHandles["cbOffline"]
	   	if cbh ~= nil then

			-- CheckBox_GetChecked returns 0 or 1, lua treats 0 as empty string
			-- which evaluates to true.  Decide to sort by last online time
			if CheckBox_GetChecked( cbh ) ~= 1 then
		       	g_curPage = 1
	    	   	g_set = 1

				g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..LOGON_SORT_SUFFIX

				clearResultSet()
				makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
			end
		end
	elseif g_current_tab == TAB_PEOPLE then
		g_curPage = 1
		g_set = 1

		g_web_address = GameGlobals.WEB_SITE_PREFIX..PEOPLE_ZONE_SUFFIX

		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	elseif g_current_tab == TAB_INZONE then
		g_curPage = 1
		g_set = 1

		g_web_address = GameGlobals.WEB_SITE_PREFIX..IN_ZONE_SUFFIX..g_currentZoneInstanceId..IN_ZONE_CIZ_SUFFIX..g_currentZoneIndex..IN_ZONE_SORTLOGON_SUFFIX

		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end

    resetPageButtonSelection()
end

function btnSortName_OnButtonClicked( buttonHandle )
	if g_current_tab == TAB_IGNORE then
		--TODO: Implement SortName for Blocked tab
		return
	end

	clearSelection()

	if g_current_tab == TAB_FRIENDS then
	    local cbh = gHandles["cbOffline"]
	   	if cbh ~= nil then
	       	g_curPage = 1
		   	g_set = 1

			-- CheckBox_GetChecked returns 0 or 1, lua treats 0 as empty string
			-- which evaluates to true.  Decide to sort by last online time
			if CheckBox_GetChecked( cbh ) == 1 then
				g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..ONLINE_SUFFIX
			else
				g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX
			end

			clearResultSet()
			makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end
	elseif g_current_tab == TAB_PEOPLE then
		g_curPage = 1
		g_set = 1

		g_web_address = GameGlobals.WEB_SITE_PREFIX..PEOPLE_NAME_SUFFIX

		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	elseif g_current_tab == TAB_INZONE then
		g_curPage = 1
		g_set = 1

		g_web_address = GameGlobals.WEB_SITE_PREFIX..IN_ZONE_SUFFIX..g_currentZoneInstanceId..IN_ZONE_CIZ_SUFFIX..g_currentZoneIndex..IN_ZONE_SORTNAME_SUFFIX

		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end

    resetPageButtonSelection()
end

function btnMessages_OnButtonClicked( buttonHandle )
	if g_selectedIndex >= 0 then
	    name = g_t_friends[g_selectedIndex]["name"]
	    if g_myName ~= name then
			MenuOpenModal("SendMessage2.xml")
			local ev = KEP_EventCreate( PLAYER_USERID_EVENT )
			KEP_EventEncodeNumber( ev, g_t_friends[g_selectedIndex]["userID"])
			KEP_EventEncodeString( ev, g_t_friends[g_selectedIndex]["name"])
			KEP_EventEncodeString( ev, "" ) -- Subject of message
			KEP_EventEncodeString( ev, "" ) -- Body of message
			KEP_EventQueue( ev )
		else
		    KEP_MessageBox("You cannot send yourself a message.")
		end
	else
		KEP_MessageBox("Please select a person to send a message.")
	end
end

function btnIgnore_OnButtonClicked( buttonHandle )
	local userID = nil
	local name = "" --selected name

	g_buttonPressed = BTN_IGNORE

	if g_selectedIndex >= 0 then

	   	userID = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0
		name = g_t_friends[g_selectedIndex]["name"]

	    if name ~=  g_myName and userID then
			MenuOpenModal("BlockReportMenu.xml")
			local ev = KEP_EventCreate( PLAYER_USERID_EVENT )
			KEP_EventEncodeNumber( ev, userID)
			KEP_EventEncodeString( ev, name)
			KEP_EventEncodeNumber( ev, 0 ) -- dont kick if use friends menu
			KEP_EventQueue( ev )
			MenuCloseThis()
		else
		    KEP_MessageBox("You cannot block yourself.")
		end
	else
	    KEP_MessageBox("Please select a user.")
	end
end

function btnBlockSettings_OnButtonClicked( buttonHandle )
	local userID = nil
	local name = "" --selected name

	g_buttonPressed = BTN_IGNORE

	if g_selectedIndex >= 0 then
		--set to 0 since we are unblocking
		local zoneBlock = 0
		local block = 0 

	   	userID = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0
		name = g_t_friends[g_selectedIndex]["name"]
		
		Log("UnBlockButtonClicked: UnBlocking name="..name.." userId="..userID)
		g_web_address = GameGlobals.WEB_SITE_PREFIX..UNBLOCK_USER_SUFFIX..userID
		makeWebCall( g_web_address, WF.BLOCK)
	else
	    KEP_MessageBox("Please select a user.")
	end
end

-- Sends add or remove friend message to web page
-- action:  string of either "Add" or "Remove"
function sendFriendAction( action )
	userID = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0
	if userID ~= nil then
		local web_address = GameGlobals.WEB_SITE_PREFIX..UPDATE_SUFFIX..userID..ACTION_SUFFIX..action

		-- Tell browser handler to use parse result function
		g_resultParse = true

		-- Send request to website, use 0 for current page and amount per page
		makeWebCall( web_address, WF.FRIENDS)

		if g_current_tab == TAB_FRIENDS and action == "Remove" then
			clearResultSet()
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX, WF.FRIENDS, 1, g_friendsPerPage)
		end
	end
end

function btnAddFriend_OnButtonClicked( buttonHandle )
	if g_selectedIndex >= 0 then
        local name = g_t_friends[g_selectedIndex]["name"]

        if g_myName ~= name then
			g_buttonPressed = BTN_FRIEND
			MenuOpenModal("YesNoBox.xml")
			local yesnoEvent = KEP_EventCreate( YES_NO_BOX_EVENT )
			KEP_EventEncodeString( yesnoEvent, "Are you sure you want to add "..name.." as a friend?" )
			KEP_EventEncodeString( yesnoEvent, "Confirm" )
			KEP_EventQueue( yesnoEvent )
		else
		    KEP_MessageBox("You cannot add yourself as a friend.")
		end
	else
		KEP_MessageBox("Please select a friend to add.")
	end
end

function btnRemoveFriend_OnButtonClicked( buttonHandle )
	if g_selectedIndex >= 0 then
        local name = g_t_friends[g_selectedIndex]["name"]
        if g_myName ~= name then
			g_buttonPressed = BTN_FRIEND
			MenuOpenModal("YesNoBox.xml")
			local yesnoEvent = KEP_EventCreate( YES_NO_BOX_EVENT )
			KEP_EventEncodeString( yesnoEvent, "Are you sure you want to remove "..name.." as a friend?" )
			KEP_EventEncodeString( yesnoEvent, "Confirm" )
			KEP_EventQueue( yesnoEvent )
		end
	else
		KEP_MessageBox("Please select a friend to remove.")
	end
end

function btnChat_OnButtonClicked( buttonHandle)
end

function btnProfile_OnButtonClicked( buttonHandle)
	if g_selectedIndex >= 0 then
		-- Web Profile
		local playerName = g_t_friends[g_selectedIndex ]["name"]
		if playerName ~= "" then
			KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_CHANNEL_PATH..playerName..".people" )
		end
	else
		KEP_MessageBox("Please select a person to view their profile.")
	end
end

function btnInvite_OnButtonClicked( buttonHandle)
	if g_selectedIndex >= 0 then
		Log("InviteButtonClicked")
		local gameID = 0
		-- Get a tracking ID for this invite
		makeWebCall( GameGlobals.WEB_SITE_PREFIX..TRACKING_REQUEST_SUFFIX..tonumber(TRACKING_REQUEST_GAME_CLIENT)..TRACKING_REQUEST_GAMEID_PARAM..tonumber(g_currentGameId), WF.TRACKING_REQ_GAME, 1, 1)
	else
		KEP_MessageBox("Please select a person to invite here.")
	end
end

function btnTravel_OnButtonClicked( buttonHandle )
	local friendId = nil
	local gameId = nil
	if g_selectedIndex >= 0 then
	   	friendId = g_t_friends[g_selectedIndex]["userID"]	--listboxes start at 0
		gameId = g_t_friends[g_selectedIndex]["gameID"]

		-- "In Zone" wokweb call does not return gameId info and does not return anything for 3d apps, default to 3296
		if gameId == nil then
			gameId = KEP.GID_PRO
		end

	   	if friendId ~= nil then
			Log( "GoToFriendClicked" )
			gotoURL( ZoneURL.build( tostring(gameId) ) .. "/" .. g_t_friends[g_selectedIndex]["name"] .. ".people")

		    setButtonState(buttonHandle, false)

			-- close this dialog
			if PROGRESSIVE == 1 then
				MenuCloseThis()
			end
		end
	else
		KEP_MessageBox("Please select a person to travel to.")
	end
end

function btnFriendsList_OnButtonClicked( buttonHandle )
end

function btnResultsPrev_OnButtonClicked( buttonHandle )
	if g_pageCap > g_window then
		calcResultsPrev()
	    alignButtons()
		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	clearSelection()
end

function btnResultsNext_OnButtonClicked( buttonHandle )
	if g_pageCap > g_window then
        calcResultsNext()
  		alignButtons()
		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	clearSelection()
end

function btnFirst_OnButtonClicked( buttonHandle )
	if g_pageCap > g_window then
		calcFirst()
	 	alignButtons()
		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	clearSelection()
end

function btnLast_OnButtonClicked( buttonHandle )
	if g_pageCap > g_window then
		calcLast()
  		alignButtons()
		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
	clearSelection()
end

function btnRange1_OnButtonClicked( buttonHandle )
	calcRange1()
	clearResultSet()
	makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	clearSelection()
end

function btnRange2_OnButtonClicked( buttonHandle )
	calcRange2()
	clearResultSet()
	makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	clearSelection()
end

function btnRange3_OnButtonClicked( buttonHandle )
	calcRange3()
	clearResultSet()
	makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	clearSelection()
end

function btnRange4_OnButtonClicked( buttonHandle )
    calcRange4()
	clearResultSet()
	makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	clearSelection()
end

function showControlAtX( handle, show, x, inc )
	Control_SetVisible(handle, show)
	Control_SetEnabled(handle, show)
	if show then
		Control_SetLocationX( handle, x )
		if inc == nil then
			inc = Control_GetWidth( handle )
		end
		x = x + inc + BUTTON_PADDING
	end
	return x
end

function btnFriends_OnButtonClicked( buttonHandle )
	g_current_tab = TAB_FRIENDS
	clearSelection()
	Control_SetEnabled(gHandles["btnFriends"], false)
	Control_SetEnabled(gHandles["btnPeople" ],  true)
	Control_SetEnabled(gHandles["btnInZone" ],  true)
	Control_SetEnabled(gHandles["btnIgnored"],  true)

	local x = 0
	x = showControlAtX(gHandles["btnTravel"       ],  true, x)
	x = showControlAtX(gHandles["btnProfile"      ],  true, x)
	x = showControlAtX(gHandles["btnAddFriend"    ], false, x)
	x = showControlAtX(gHandles["btnMessages"     ],  true, x)
	x = showControlAtX(gHandles["btnChat"         ],  true, x)
	x = showControlAtX(gHandles["btnInvite"       ],  true, x)
	x = x + 73 -- leave a gap before the Remove Friend button
	x = showControlAtX(gHandles["btnRemoveFriend" ],  true, x)
	x = showControlAtX(gHandles["btnIgnore"       ],  true, x)
	x = showControlAtX(gHandles["btnBlockSettings"], false, x)

	Control_SetEnabled(gHandles["btnSortOnline"], true)
	Static_SetText(gHandles["btnSortOnline"], " Location")

	EditBox_ClearText(gHandles["editSearchField"])
	
	Control_SetEnabled(gHandles["cbOffline"], true)
	Control_SetVisible(gHandles["cbOffline"], true)

	g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..g_sortType

	if CheckBox_GetChecked( gHandles["cbOffline"] ) == 1 then
		g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..ONLINE_SUFFIX
	end

	g_curPage = 1
	g_set = 1

    g_InitBtnText = true

    resetPageButtonSelection()

	clearResultSet()
	makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
end

-- Need a push to Dev before People Suffix will work
function btnPeople_OnButtonClicked( buttonHandle )
	g_current_tab = TAB_PEOPLE
	clearSelection()
    Control_SetEnabled(gHandles["btnFriends"],  true)
	Control_SetEnabled(gHandles["btnPeople" ], false)
	Control_SetEnabled(gHandles["btnInZone" ],  true)
	Control_SetEnabled(gHandles["btnIgnored"],  true)

	local x = 0
	x = showControlAtX(gHandles["btnTravel"       ],  true, x)
	x = showControlAtX(gHandles["btnProfile"      ],  true, x)
	x = showControlAtX(gHandles["btnAddFriend"    ],  true, x)
	x = showControlAtX(gHandles["btnMessages"     ],  true, x)
	x = showControlAtX(gHandles["btnChat"         ],  true, x)
	x = showControlAtX(gHandles["btnInvite"       ],  true, x)
	x = showControlAtX(gHandles["btnRemoveFriend" ], false, x)
	x = x + 73 -- leave a gap where the Remove Friend button was
	x = showControlAtX(gHandles["btnIgnore"       ],  true, x)
	x = showControlAtX(gHandles["btnBlockSettings"], false, x)

    Control_SetEnabled(gHandles["btnSortOnline"], true)
    Static_SetText(gHandles["btnSortOnline"], " Location")

	EditBox_ClearText(gHandles["editSearchField"])
	
	Control_SetEnabled(gHandles["cbOffline"], false)
	Control_SetVisible(gHandles["cbOffline"], false)

	g_curPage = 1
	g_set = 1

    g_InitBtnText = true

    resetPageButtonSelection()

	g_web_address = GameGlobals.WEB_SITE_PREFIX..PEOPLE_LOGIN_SUFFIX 

	clearResultSet()
	makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
end

-- Switch state to Ignore tab functions
function btnIgnored_OnButtonClicked( buttonHandle )

	g_current_tab = TAB_IGNORE

	clearSelection()

    Control_SetEnabled(gHandles["btnFriends"],  true)
	Control_SetEnabled(gHandles["btnPeople" ],  true)
	Control_SetEnabled(gHandles["btnInZone" ],  true)
	Control_SetEnabled(gHandles["btnIgnored"], false)

	local x = 0

	x = showControlAtX(gHandles["btnTravel"       ], false, x)
	x = showControlAtX(gHandles["btnProfile"      ],  true, x)
	x = showControlAtX(gHandles["btnAddFriend"    ], false, x)
	x = showControlAtX(gHandles["btnMessages"     ], false, x)
	x = showControlAtX(gHandles["btnChat"         ], false, x)
	x = showControlAtX(gHandles["btnInvite"       ], false, x)
	x = showControlAtX(gHandles["btnRemoveFriend" ], false, x)
	x = showControlAtX(gHandles["btnIgnore"       ], false, x)
	x = showControlAtX(gHandles["btnBlockSettings"],  true, x)

    Control_SetEnabled(gHandles["btnSortOnline"], true)
	Static_SetText(gHandles["btnSortOnline"], " Date")

	EditBox_ClearText(gHandles["editSearchField"])
	
	Control_SetEnabled(gHandles["cbOffline"], false)
	Control_SetVisible(gHandles["cbOffline"], false)

	g_curPage = 1
	g_set = 1

	g_InitBtnText = true

    resetPageButtonSelection()

	g_web_address = GameGlobals.WEB_SITE_PREFIX..LIST_BLOCKED_USERS_SUFFIX

	clearResultSet()
	makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
end

-- Need a push to Dev before People Suffix will work
function btnInZone_OnButtonClicked( buttonHandle )

	g_current_tab = TAB_INZONE

	clearSelection()

    Control_SetEnabled(gHandles["btnFriends"],  true)
	Control_SetEnabled(gHandles["btnPeople" ],  true)
	Control_SetEnabled(gHandles["btnInZone" ], false)
	Control_SetEnabled(gHandles["btnIgnored"],  true)

	local x = 0

	x = showControlAtX(gHandles["btnTravel"       ],  true, x)
	x = showControlAtX(gHandles["btnProfile"      ],  true, x)
	x = showControlAtX(gHandles["btnAddFriend"    ],  true, x)
	x = showControlAtX(gHandles["btnMessages"     ],  true, x)
	x = showControlAtX(gHandles["btnChat"         ],  true, x)
	x = showControlAtX(gHandles["btnInvite"       ],  true, x)
	x = showControlAtX(gHandles["btnRemoveFriend" ], false, x)
	x = x + 73 -- leave a gap where the Remove Friend button was
	x = showControlAtX(gHandles["btnIgnore"       ],  true, x)
	x = showControlAtX(gHandles["btnBlockSettings"], false, x)


	Control_SetEnabled(gHandles["btnSortOnline"], true)
	Static_SetText(gHandles["btnSortOnline"], " Location")

	EditBox_ClearText(gHandles["editSearchField"])

	Control_SetEnabled(gHandles["cbOffline"], false)
	Control_SetVisible(gHandles["cbOffline"], false)

	g_curPage = 1
	g_set = 1

	g_InitBtnText = true

	resetPageButtonSelection()

	g_web_address = GameGlobals.WEB_SITE_PREFIX..IN_ZONE_SUFFIX..g_currentZoneInstanceId..IN_ZONE_CIZ_SUFFIX..g_currentZoneIndex

	clearResultSet()
	makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
end

function btnFindFriends_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA.."mykaneva/inviteFriend.aspx")
end 

-- Ensure the search string is more than 3 characters long.
function preProcessSearch(strSearchtext)

	local ebh = Dialog_GetEditBox( gDialogHandle, "editSearchField" )

	if(ebh ~= nil) then
    	if (EditBox_GetTextLength(ebh) < 3) then
	    	KEP_MessageBox("Please enter at least three characters to search for friends.")
		else
			initSearch( strSearchtext )
 		end
 	end
end

-- Grab text from edit box and pass off to search function
function btnSearch_OnButtonClicked( buttonHandle )

    local searchText = ""
	KEP_SetCurrentAnimation(119, 0)
	ebh = Dialog_GetEditBox( gDialogHandle, "editSearchField" )

   	if ebh ~= nil then
		searchText = EditBox_GetText( ebh )
   	end

    preProcessSearch(searchText)
end

-- When the check box is checked or unchecked, change the web query we use to
-- display friends
function cbOffline_OnCheckBoxChanged( )

	clearSelection()

    local cbh = Dialog_GetCheckBox( gDialogHandle, "cbOffline" )

   	if cbh ~= nil then

   	    local ebh = Dialog_GetEditBox( gDialogHandle, "editSearchField" )
		EditBox_ClearText( ebh )

       	g_curPage = 1
       	g_set = 1

		-- CheckBox_GetChecked returns 0 or 1, lua treats 0 as empty string
		-- which evaluates to true.  Decide if offline friends should be shown
		if CheckBox_GetChecked( cbh ) == 1 then
			g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..ONLINE_SUFFIX
		else
			g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX
		end

		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end

 	g_InitBtnText = true
    resetPageButtonSelection()
end

-- Grabs text from edit box after enter pressed.  Sends to search function
function editSearchField_OnEditBoxString(ebh, searchText)
	preProcessSearch(searchText)
end

-- Send search request to server
function initSearch( searchText )
	if g_current_tab == TAB_FRIENDS then
	    local cbh = Dialog_GetCheckBox( gDialogHandle, "cbOffline" )
		if cbh ~= nil then

			-- If checkbox selected then show only friends online
			if (searchText ~= nil) and (CheckBox_GetChecked( cbh ) == 1) then
				g_web_address = GameGlobals.WEB_SITE_PREFIX..FRIENDS_SUFFIX..ONLINE_SUFFIX
			else
				g_web_address = GameGlobals.WEB_SITE_PREFIX..SEARCH_SUFFIX..searchText
			end

			g_curPage = 1
			g_set = 1

			clearResultSet()
			makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
		end
	elseif g_current_tab == TAB_PEOPLE then
		g_curPage = 1
		g_set = 1

        g_web_address = GameGlobals.WEB_SITE_PREFIX..PEOPLE_SEARCH_SUFFIX..searchText

		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	elseif g_current_tab == TAB_INZONE then
		g_curPage = 1
		g_set = 1

		g_web_address = GameGlobals.WEB_SITE_PREFIX..IN_ZONE_SUFFIX..g_currentZoneInstanceId..IN_ZONE_CIZ_SUFFIX..g_currentZoneIndex..IN_ZONE_SEARCH_SUFFIX..searchText

		clearResultSet()
		makeWebCall( g_web_address, WF.FRIENDS, g_curPage, g_friendsPerPage)
	end
end

-- Clear listbox selections
function clearSelection()
 	g_selectedIndex = -1
	if g_totalNumRec > 0 then
		ListBox_ClearSelection(gHandles["lstFriendsDummy"])
		ListBox_ClearSelection(gHandles["lstFriends"])
		ListBox_ClearSelection(gHandles["lstOnlineDummy"])
		ListBox_ClearSelection(gHandles["lstOnline"])
	end
end

-- Added by Jonny to handle alignment of the page buttons at the bottom.
-- mbiggs 05/16/07 modified to handle new menus, images are not sent
-- to the rightAlignPageButtons function, the right set of images are
-- hardcoded and the left set of images are located based on the x position
-- the rightAlignPageButtons function returns
function alignButtons()
	local btnRightRightArrow = Dialog_GetButton(gDialogHandle, "btnLast")
	Control_SetLocation(btnRightRightArrow, 446, 235)
	local btnRightArrow = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	Control_SetLocation(btnRightArrow, 431, 235)

	local t = {}		-- new table
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange1"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange2"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange3"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange4"))
	rightAlignPageButtons(t, 427, 237, 5)

	local leftMostXPos = Control_GetLocationX(Dialog_GetButton(gDialogHandle, "btnRange1"))

	local btnLeftArrow = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	local leftArrowWidth = Control_GetWidth(btnLeftArrow)
	leftMostXPos = leftMostXPos - leftArrowWidth - 4
	Control_SetLocation(btnLeftArrow, leftMostXPos, 235)

	local btnLeftLeftArrow = Dialog_GetButton(gDialogHandle, "btnFirst")
	local leftLeftArrowWidth = Control_GetWidth(btnLeftLeftArrow)
	leftMostXPos = leftMostXPos - leftLeftArrowWidth + 1
	Control_SetLocation(btnLeftLeftArrow, leftMostXPos, 235)
end

g_Tooltips = {}
function initTooltips()
	g_Tooltips = {}
	g_Tooltips[gHandles["btnInZone"]] = "View People In This Zone"
	g_Tooltips[gHandles["btnPeople"]] = "View People Online Now"
	g_Tooltips[gHandles["btnFriends"]] = "View Your Friends"
	g_Tooltips[gHandles["btnIgnored"]] = "View People on Your Block List"

	g_Tooltips[gHandles["btnSearch"]] = "Search for Friends or People"
	g_Tooltips[gHandles["cbOffline"]] = "Only Show Online Friends"
	g_Tooltips[gHandles["btnFriendsList"]] = "Show Friends List"

	g_Tooltips[gHandles["btnSortName"]] = "Sort Results by Name"
	g_Tooltips[gHandles["btnSortOnline"]] = "Sort Results by Online Status"

	g_Tooltips[gHandles["btnTravel"]] = "Travel to This Person's Location"
	g_Tooltips[gHandles["btnProfile"]] = "View This Person's Profile"
	g_Tooltips[gHandles["btnAddFriend"]] = "Add This Person as a Friend"
	g_Tooltips[gHandles["btnMessages"]] = "Send Selected Person a Message"
	g_Tooltips[gHandles["btnChat"]] = "Chat With This Person"
	g_Tooltips[gHandles["btnInvite"]] = "Invite this person your location"

	g_Tooltips[gHandles["btnIgnore"]] = "Change the Block Settings for this Person"
	g_Tooltips[gHandles["btnRemoveFriend"]] = "Remove This Person as a Friend"
	g_Tooltips[gHandles["btnBlockSettings"]] = "Change the Block Settings for this Person"

	g_Tooltips[gHandles["btnClose"]] = "Close This Menu"
end
