--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("MenuStyles.lua")
dofile("CreditBalances.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

g_canTrade = true -- drf - get only inventory that can be traded

MAX_BUY_ITEMS = 10000

REG_CREDITS = 2
GIFT_CREDITS = 1

GIFT_ICON_INV_NAME = "imgGiftIconInv"

LIST_BOX_SIZE = 8

g_mouseDown = false -- drf - added

-- Player Inventory Items
g_itemsPlayer = {}

-- Vendor Inventory Items
g_itemsVendor = {}

-- boolean used to keep the handler, lstInventory_OnScrollBarChanged, from being called
-- when attribEventHandler() calls ListBox_AddItem()
g_b_loading_items = false

g_credits 	= 0
g_regCredits = 0
g_giftCredits = 0

g_helpVisible = false

RequestForInventoryID 	= 51
VendorInventoryID 	= 52
UpdateCashID		= 19

g_last_inv_track_pos = 0
g_last_vendor_track_pos = 0

g_showIcons = true

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "attribEventHandler", "AttribEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "RenderTextEventHandler", "RenderTextEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( UPDATE_HUD_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- DRF - Added - Capture All Keys
	MenuSetCaptureKeysThis(true)

	-- setup look of listbox scrollbars
	SetupListBoxScrollBars("lstInventory")
	SetupListBoxScrollBars("lstVendorInventory")

	resetInvGiftIcons()

	setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstInventory"))
	setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstVendorInventory"))

	g_showIcons = getIconPreference()

	showHelpMenus( false )
end

function Dialog_OnDestroy(dialogHandle)
	KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function setImageState( imageHandle, state )
	if imageHandle ~= nil then
		ch = Image_GetControl(imageHandle)
		if ch ~= nil then
			Control_SetEnabled(ch, state)
			Control_SetVisible(ch, state)
		end
	end
end

function EnableSellButton(bEnable)
	Control_SetEnabled(Dialog_GetButton(gDialogHandle, "btnSellItem"), bEnable)
end

function EnableBuyButton(bEnable)
	Control_SetEnabled(Dialog_GetButton(gDialogHandle, "btnBuyItemWithCredits"), bEnable)
	Control_SetEnabled(Dialog_GetButton(gDialogHandle, "btnBuyItemWithRewards"), bEnable)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.VENDOR then
		local xmlData = KEP_EventDecodeString( event )
		local cred, giftCred = ParseBalances(xmlData)
		if cred ~= nil then
			g_regCredits = cred
		end
		if giftCred ~= nil then
			g_giftCredits = giftCred
		end
		UpdateCredits()
		return KEP.EPR_CONSUMED
	elseif filter == WF.VENDOR_INVENTORY then
		local xmlData = KEP_EventDecodeString( event )
		VendorInventoryItems( xmlData )
		return KEP.EPR_CONSUMED
	elseif filter == WF.PLAYER_INVENTORY then
		local xmlData = KEP_EventDecodeString( event )
		PlayerInventoryItems( xmlData )
		return KEP.EPR_CONSUMED
	end
end

-- Switch credits used for calaculations to the desired type for purchase
function assignCreditType( credit_type )
	if credit_type == REG_CREDITS then
		g_credits = g_regCredits
	elseif credit_type == GIFT_CREDITS then
		g_credits = g_giftCredits
	end
end

function GetCredits()
	getBalances(0, WF.VENDOR)
end

function UpdateCredits()
	local sh = Dialog_GetStatic(gDialogHandle, "lblGiftCardCredits")
	if sh ~= 0 then
		Static_SetText(sh, tostring(g_giftCredits))
	end

	local sh = Dialog_GetStatic(gDialogHandle, "lblCreditAmount")
	if sh ~= 0 then
		Static_SetText(sh, tostring(g_regCredits))
	end
	updateHUDBalance(g_regCredits, g_giftCredits, 0)
end

function UpdatePlayerInventory()
	Log("UpdatePlayerInventory")
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_RemoveAllItems(lbh)
	if (g_canTrade == true) then 
		getPlayerInventoryCanTrade(WF.PLAYER_INVENTORY) -- only trade-able inventory
	else
		getPlayerInventory(WF.PLAYER_INVENTORY) -- all inventory (very large and confusing)
	end

	-- Get User Credits
	GetCredits()
end

function UpdateVendorInventory(storeId)
	g_storeId = storeId or g_storeId
	Log("UpdateVendorInventory - storeId="..g_storeId)
	local lbh = Dialog_GetListBox(gDialogHandle, "lstVendorInventory")
	ListBox_RemoveAllItems(lbh)
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..VENDOR_INVENTORY_SUFFIX..g_storeId, WF.VENDOR_INVENTORY, 1, 50)
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == RequestForInventoryID then
		UpdatePlayerInventory()
	elseif filter == VendorInventoryID then
		UpdateVendorInventory(objectid)
	elseif filter == UpdateCashID then
		g_credits = KEP_EventDecodeNumber( event )
		UpdateCredits()
	end
end

function PlayerInventoryItems(xmlData )

	-- Clear Existing Items
	g_itemsPlayer = {}

	-- DRF - Added - Clear Player Inventory Selection
	PlayerInventoryClearSelection()

	-- DRF - Added - Remove All Player Inventory ListBox Items
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	ListBox_RemoveAllItems(lbh)

	-- Decode All Items From Web
	decodeInventoryItemsFromWeb( g_itemsPlayer, xmlData )
	
	-- Sort Items
	g_itemsPlayer = sortTableByField(g_itemsPlayer, "name")

	-- Insert All Items Into Player Inventory Item ListBox
	Log("PlayerInventoryItems: items="..#g_itemsPlayer)
	g_b_loading_items = true
	for i=1, #g_itemsPlayer do
		local text = "\nQuantity: " .. tostring(g_itemsPlayer[i]["quantity"])
		if g_itemsPlayer[i]["can_trade"] then
			text = text.."  Sell Price: "..tostring(g_itemsPlayer[i]["sell_price"])
		end
		ListBox_AddItem(lbh, g_itemsPlayer[i]["name"]..text, g_itemsPlayer[i]["GLID"])
	end
	g_b_loading_items = false

	resetInvGiftIcons()

	-- Refresh List Starting At Top
	g_last_inv_track_pos = 0
	refreshInventory(g_last_inv_track_pos)

	-- DRF - Set Quantity To 1
	SetQuantity("txtQuantityInv", 1 )

	-- DRF - Added - Enable Sell Button
	EnableSellButton(true)
end

function VendorInventoryItems( xmlData )
	xmlData = stripTagsXML( xmlData )
	
	-- Clear Existing Items
	g_itemsVendor = {}

	-- DRF - Added - Clear Vendor Inventory Selection
	VendorInventoryClearSelection()

	-- DRF - Added - Remove All Vendor Inventory ListBox Items
	local lbh = Dialog_GetListBox(gDialogHandle, "lstVendorInventory")
	ListBox_RemoveAllItems(lbh)

	-- Decode All Items From Web
	for block_text in string.gmatch( xmlData, "<StoreInventory>(.-)</StoreInventory>") do
		local t_item = {}
		local result = nil  -- element value
		local s, e = 0      -- start and end of captured string
		s, e, result = string.find(block_text, "<name>(.+)</name>")
		if result ~= nil then
			t_item["name"] = tostring(result);
		end

		s, e, result = string.find(block_text, "<market_cost>(%d+)</market_cost>")
		if result ~= nil then
			t_item["cost"] = tonumber(result);
		end

		s, e, result = string.find(block_text, "<global_id>(%d+)</global_id>")
		if result ~= nil then
			t_item["GLID"] = tonumber(result);
		end

		s, e, result = string.find(block_text, "<selling_price>(%d+)</selling_price>")
		if result ~= nil then
			t_item["sell_price"] = tonumber(result);
		end

		s, e, result = string.find(block_text, "<use_type>(%d+)</use_type>")
		if result ~= nil then
			t_item["use_type"] = tonumber(result);
		end

		s, e, result = string.find(block_text, "<required_skill>(%d+)</required_skill>")
		if result ~= nil then
			t_item["req_skill_type"] = tonumber(result);
		end

		s, e, result = string.find(block_text, "<required_skill_level>(%d+)</required_skill_level>")
		if result ~= nil then
			t_item["req_skill_level"] = tonumber(result);
		end

		s, e, result = string.find(block_text, "<description>(.+)</description>")
		if result ~= nil then
			t_item["description"] = tostring(result);
		end

		s, e, result = string.find(block_text, "<inventory_type>(%d+)</inventory_type>")
		if result ~= nil then
			t_item["item_type"] = tonumber(result);
		end

		-- stats for item not implemented
		t_item["req_dex"] = 0
		t_item["req_knowledge"]	= 0
		t_item["req_str"] = 0
		t_item["quantity"] = 1
		table.insert(g_itemsVendor, t_item)
	end

	-- Sort Items
	g_itemsVendor = sortTableByField(g_itemsVendor, "name")

	-- Insert All Items Into Vendor Inventory Item ListBox
	Log("VendorInventoryItems: items="..#g_itemsVendor)
	g_b_loading_items = true
	for i=1, #g_itemsVendor do
		ListBox_AddItem(lbh, g_itemsVendor[i]["name"].."\nCost: "..tostring(g_itemsVendor[i]["cost"]), g_itemsVendor[i]["GLID"])
	end
	g_b_loading_items = false

	-- Refresh List Starting At Top
	g_last_vendor_track_pos = 0
	refreshVendor(g_last_vendor_track_pos)

	-- DRF - Set Quantity To 1
	SetQuantity("txtQuantityStore", 1 )

	-- DRF - Added - Enable Buy Button
	EnableBuyButton(true)
end

function SetupListBoxScrollBars(listbox_name)
	local lbh = Dialog_GetListBox(gDialogHandle, listbox_name)
	local sbh = ListBox_GetScrollBar(lbh)
	local eh = ScrollBar_GetTrackDisplayElement(sbh)
	if eh ~= 0 then
		--Blue body
		Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
		Element_SetCoords(eh, 361, 579, 390, 586)
	end

	eh = ScrollBar_GetButtonDisplayElement(sbh)
	if eh ~= 0 then
		--Grey lined
		Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
		Element_SetCoords(eh, 395, 573, 409, 588)
	end

	eh = ScrollBar_GetUpArrowDisplayElement(sbh)
	if eh ~= 0 then
		--Up arrow button
		Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
		Element_SetCoords(eh, 395, 547, 409, 561)
	end

	eh = ScrollBar_GetDownArrowDisplayElement(sbh)
	if eh ~= 0 then
		--Down Arrow Button
		Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
		Element_SetCoords(eh, 395, 642, 409, 656)
	end
end

function lstInventory_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	g_last_inv_track_pos = track_pos
	if g_b_loading_items == false then
		refreshInventory(track_pos)
	end
end

function refreshInventory(track_pos)

	-- Reset The Cannot Trade (lock) Icons
	resetInvGiftIcons()

	-- determine how many icons need to be shown
	local listBoxHandle = Dialog_GetListBox(gDialogHandle, "lstInventory")
	local num_items = ListBox_GetSize(listBoxHandle)

	-- For Each Of 8 PlayerInventory ListBox Rows
	for i = 0, 7 do
		local index = i + track_pos + 1
		local item = g_itemsPlayer[index]

		-- show list icons
		local icon_name = "imgIcon" .. tostring(i + 1)
		local ich = Dialog_GetImage(gDialogHandle, icon_name)
		if (item ~= nil) and g_showIcons then
			Control_SetVisible(ich, true)
			local eh = Image_GetDisplayElement(ich)
			KEP_LoadIconTextureByID(item.GLID, eh, gDialogHandle);
			ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_INV_NAME..i+1)
			setGiftIcon(item, ih)
		else
			Control_SetVisible(ich, false)
		end

		-- show list labels (second line)
		local label_name = "lblInventory" .. tostring(i + 1)
		local sh = Dialog_GetStatic(gDialogHandle, label_name)
		if sh ~= 0 then
			Control_SetVisible(sh, false)
		end
	end
end

function lstVendorInventory_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	g_last_vendor_track_pos = track_pos
	if g_b_loading_items == false then
		refreshVendor(track_pos)
	end
end

function refreshVendor(track_pos)
	local listBoxHandle = Dialog_GetListBox(gDialogHandle, "lstVendorInventory")
	local num_items = 0

	-- determine how many icons need to be shown
	if listBoxHandle ~= 0 then
		num_items = ListBox_GetSize(listBoxHandle)
	end

	-- For Each Of 8 Vendor Inventory ListBox Rows
	for i = 0, 7 do
		local index = i + track_pos + 1
		local item = g_itemsVendor[index]

		-- show list icons
		local icon_name = "imgVendorIcon" .. tostring(i + 1)
		local ich = Dialog_GetImage(gDialogHandle, icon_name)
		if (item ~= nil) and g_showIcons then
			Control_SetVisible(ich, true)
			local eh = Image_GetDisplayElement(ich)
			KEP_LoadIconTextureByID(item.GLID, eh, gDialogHandle);
		else
			Control_SetVisible(ich, false)
		end

		-- show list labels (second line)
		local label_name = "lblVendor" .. tostring(i + 1)
		local sh = Dialog_GetStatic(gDialogHandle, label_name)
		if sh ~= 0 then
			Control_SetVisible(sh, false)
		end
	end
end

function SetQuantity(ebName, value)
	local ebh = Dialog_GetEditBox( gDialogHandle, ebName )
	EditBox_SetText( ebh, tostring(value or 1), false )
end

function GetQuantity( ebName )
	local quantity = 1
	local ebh = Dialog_GetEditBox(gDialogHandle, ebName)
	quantity_text = EditBox_GetText(ebh)
	if quantity_text ~= "" then
		quantity  = tonumber(quantity_text)
		if not quantity then
			quantity = 0
		end
	end
	return quantity
end

function resetInvGiftIcons()
	for i = 1,LIST_BOX_SIZE  do
		ih = Dialog_GetImage(gDialogHandle, GIFT_ICON_INV_NAME..i)
		if ih ~= nil then
			Control_SetVisible(Image_GetControl(ih), false)
		end
	end
end

function setGiftIcon(item, icon_handle)
	if icon_handle ~= nil then
		if item["can_trade"] then
			Control_SetVisible(Image_GetControl(icon_handle), false)
		else
			Control_SetVisible(Image_GetControl(icon_handle), true)
		end
	end
end

function btnBuyItemWithCredits_OnButtonClicked(buttonHandle)
	Log("BuyItemWithCreditsClicked")

	-- try to buy item
	local lbh = Dialog_GetListBox(gDialogHandle, "lstVendorInventory")
	local index = ListBox_GetSelectedItemIndex(lbh)
	if index > -1 then

		-- get the quantity to be purchased
		local quantity = GetQuantity("txtQuantityStore")
		if quantity > 0 then
			if quantity <= MAX_BUY_ITEMS then
				buyItem(index, quantity, REG_CREDITS)
			else
				KEP_MessageBox(MAX_BUY_ITEMS.." is the maximum number of items you can purchase in one transaction")
			end
		else
			KEP_MessageBox("Please enter a positive quantity")
		end
	else
		KEP_MessageBox("Please select an item to purchase")
	end
end

function btnSellItem_OnButtonClicked(buttonHandle)
	Log("SellItemClicked")

	-- try to sell item
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	local index = ListBox_GetSelectedItemIndex(lbh)
	if index >= 0 then
		local item = index + 1
		local canTrade = g_itemsPlayer[item]["can_trade"]
		if canTrade then
			local reqQty = GetQuantity("txtQuantityInv")  --requested quantity
			local actQty = g_itemsPlayer[index + 1]["quantity"] --actual
			if (reqQty <= actQty) and (reqQty > 0) then
				assignCreditType( REG_CREDITS )
				sellItem()
			else
				KEP_MessageBox("You do not have that many items")
			end
		else
			KEP_MessageBox("This item cannot be traded or sold")
		end
	else
		KEP_MessageBox("Please select an item to sell")
	end
end

function buyItem(index, quantity, cred_type)

	local GLID = g_itemsVendor[index+1]["GLID"]
	local cost = g_itemsVendor[index+1]["cost"]
	local sell_price = g_itemsVendor[index+1]["sell_price"]

	-- get the quantity to be purchased
	local quantity		= GetQuantity("txtQuantityStore")
	local total_cost	= quantity * cost
	assignCreditType( cred_type )

	-- if there are enough credits, then buy it (them)
	if total_cost <= g_credits and quantity > 0 then
        KEP_BuyItemFromStore(GLID, quantity, cred_type)
	else
		if cred_type == GIFT_CREDITS then
			KEP_MessageBox( "You can't afford this item with REWARDS.  Try purchasing with regular credits.")
		else
			createLinkMessage( "You do not have enough Credits to buy this item, but you may be able to buy it with Rewards.", "Purchase Credits", GameGlobals.WEB_SITE_PREFIX_KANEVA..BUY_CREDITS_SUFFIX)
		end
	end
end

function btnBuyItemWithRewards_OnButtonClicked(buttonHandle)
	Log("BuyItemWithRewardsClicked")

	local lbh = Dialog_GetListBox(gDialogHandle, "lstVendorInventory")
	local index = ListBox_GetSelectedItemIndex(lbh)
	if index > -1 then
		-- get the quantity to be purchased
		local quantity		= GetQuantity("txtQuantityStore")
		if quantity > 0 then
			if quantity <= MAX_BUY_ITEMS then
				buyItem(index, quantity, GIFT_CREDITS)
			else
				KEP_MessageBox(MAX_BUY_ITEMS.." is the maximum number of items you can purchase in one transaction")
			end
		else
			KEP_MessageBox("Please enter a positive quantity")
		end
	else
		KEP_MessageBox("Please select an item to purchase")
	end
end

function sellItem()
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInventory")
	local index = ListBox_GetSelectedItemIndex(lbh)
	if index >= 0 then
		local item = index + 1
		local GLID = g_itemsPlayer[item]["GLID"]
		local tot_quantity = g_itemsPlayer[item]["quantity"] or 1
		local sell_price = g_itemsPlayer[item]["sell_price"]
		local can_trade = g_itemsPlayer[item]["can_trade"]

		-- get the quantity to be sold
		local quantity = GetQuantity("txtQuantityInv")

		Log("sellItem: item="..item.." glid="..GLID.." numOwn="..tot_quantity.." numSell="..quantity.." canTrade="..tostring(can_trade))

		-- if there is more than 1
		if tot_quantity > 1 then
			if quantity <= tot_quantity and quantity > 0 then
                KEP_SellItemToStore(GLID, quantity)
			end
		else
			if quantity > 0 then
				KEP_SellItemToStore(GLID, 1)
			end
		end
	end
end

-- Update Player Inventory Item Selected (updates large image and description)
function PlayerInventoryUpdate()
	local index = g_prev_inventory_index
	local hImg = Dialog_GetImage(gDialogHandle, "imgItemPicInv")
	local hImgE = Image_GetDisplayElement(hImg)
	local hDesc = Dialog_GetStatic(gDialogHandle, "lblInvDesc")
	if (index ~= nil) and (#g_itemsPlayer >= (index + 1)) then
		local item = index + 1 
		local itemGlid = g_itemsPlayer[item].GLID or 0
		local itemDesc = g_itemsPlayer[item].description or ""
		if (g_showIcons) then KEP_LoadIconTextureByID(itemGlid, hImgE, gDialogHandle) end
		setImageState(hImg, g_showIcons)
		Static_SetText(hDesc, itemDesc)
	else
		setImageState(hImg, false)
		Static_SetText(hDesc, "")
	end
end

-- Clear Player Inventory Item Selected & Update
function PlayerInventoryClearSelection()
	ListBox_ClearSelection(Dialog_GetListBox(gDialogHandle, "lstInventory"))
	g_prev_inventory_index = nil
	g_prev_inventory_sel_GLID = nil
	PlayerInventoryUpdate()
end

-- Sets Player Inventory Item Selected & Update 
function PlayerInventorySelect(index, glid)
	g_prev_inventory_index = index
	g_prev_inventory_sel_GLID = glid
	PlayerInventoryUpdate()
end

-- Player Inventory Item Listbox Icon Selection
function PlayerInventoryItemClicked(index)
	local item = (index - 1) + g_last_inv_track_pos
	ListBox_SelectItem(Dialog_GetListBox(gDialogHandle, "lstInventory"), item)
end
function imgIcon1_OnLButtonDown( dialogHandle, x, y ) PlayerInventoryItemClicked(1) end
function imgIcon2_OnLButtonDown( dialogHandle, x, y ) PlayerInventoryItemClicked(2) end
function imgIcon3_OnLButtonDown( dialogHandle, x, y ) PlayerInventoryItemClicked(3) end
function imgIcon4_OnLButtonDown( dialogHandle, x, y ) PlayerInventoryItemClicked(4) end
function imgIcon5_OnLButtonDown( dialogHandle, x, y ) PlayerInventoryItemClicked(5) end
function imgIcon6_OnLButtonDown( dialogHandle, x, y ) PlayerInventoryItemClicked(6) end
function imgIcon7_OnLButtonDown( dialogHandle, x, y ) PlayerInventoryItemClicked(7) end
function imgIcon8_OnLButtonDown( dialogHandle, x, y ) PlayerInventoryItemClicked(8) end

-- Player Inventory Item Listbox Selection
function lstInventory_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	local mouseDown = ToBool(bMouseDown)
	local item = sel_index + 1
	local glid = ListBox_GetItemData(listBoxHandle, sel_index)	
	Log("PlayerInventoryItemSelected: item="..item.." glid="..glid.." mouseDown="..tostring(mouseDown))

	-- Toggle Same Selection Or Select New
	if glid == g_prev_inventory_sel_GLID and g_prev_inventory_index == sel_index then
		if (not g_mouseDown) then
			PlayerInventoryClearSelection()
		end
	else
		PlayerInventorySelect(sel_index, glid)
	end
	g_mouseDown = mouseDown
end

-- Update Vendor Inventory Item Selected (updates large image and description)
function VendorInventoryUpdate()
	local index = g_prev_Vendor_inventory_index
	local hImg = Dialog_GetImage(gDialogHandle, "imgItemPicStore")
	local hImgE = Image_GetDisplayElement(hImg)
	local hDesc = Dialog_GetStatic(gDialogHandle, "lblStoreDesc")
	if (index ~= nil) and (#g_itemsVendor >= (index + 1)) then
		local item = index + 1 
		local itemGlid = g_itemsVendor[item].GLID or 0
		local itemDesc = g_itemsVendor[item].description or ""
		if (g_showIcons) then KEP_LoadIconTextureByID(itemGlid, hImgE, gDialogHandle) end
		setImageState(hImg, g_showIcons)
		Static_SetText(hDesc, itemDesc)
	else
		setImageState(hImg, false)
		Static_SetText(hDesc, "")
	end
end

-- Clear Vendor Inventory Item Selected & Update
function VendorInventoryClearSelection()
	ListBox_ClearSelection(Dialog_GetListBox(gDialogHandle, "lstVendorInventory"))
	g_prev_Vendor_inventory_index = nil
	g_prev_Vendor_inventory_sel_GLID = nil
	VendorInventoryUpdate()
end

-- Sets Vendor Inventory Item Selected & Update 
function VendorInventorySelect(index, glid)
	g_prev_Vendor_inventory_index = index
	g_prev_Vendor_inventory_sel_GLID = glid
	VendorInventoryUpdate()
end

-- Vendor Inventory Item Listbox Icon Selection
function VendorInventoryItemClicked(index)
	local item = (index - 1) + g_last_vendor_track_pos
	ListBox_SelectItem(Dialog_GetListBox(gDialogHandle, "lstVendorInventory"), item)
end
function imgVendorIcon1_OnLButtonDown( dialogHandle, x, y ) VendorInventoryItemClicked(1) end
function imgVendorIcon2_OnLButtonDown( dialogHandle, x, y ) VendorInventoryItemClicked(2) end
function imgVendorIcon3_OnLButtonDown( dialogHandle, x, y ) VendorInventoryItemClicked(3) end
function imgVendorIcon4_OnLButtonDown( dialogHandle, x, y ) VendorInventoryItemClicked(4) end
function imgVendorIcon5_OnLButtonDown( dialogHandle, x, y ) VendorInventoryItemClicked(5) end
function imgVendorIcon6_OnLButtonDown( dialogHandle, x, y ) VendorInventoryItemClicked(6) end
function imgVendorIcon7_OnLButtonDown( dialogHandle, x, y ) VendorInventoryItemClicked(7) end
function imgVendorIcon8_OnLButtonDown( dialogHandle, x, y ) VendorInventoryItemClicked(8) end

-- Vendor Inventory Item Listbox Selection
function lstVendorInventory_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	local mouseDown = ToBool(bMouseDown)
	local item = sel_index + 1
	local glid = ListBox_GetItemData(listBoxHandle, sel_index)	
	Log("VendorInventoryItemSelected: item="..item.." glid="..glid.." mouseDown="..tostring(mouseDown))

	-- Toggle Same Selection Or Select New
	if glid == g_prev_Vendor_inventory_sel_GLID and g_prev_Vendor_inventory_index == sel_index then
		if (not g_mouseDown) then
			VendorInventoryClearSelection()
		end
	else
		VendorInventorySelect(sel_index, glid)
	end
	g_mouseDown = mouseDown
end

function showHelpMenus( switch )
	sc = Dialog_GetControl( gDialogHandle, "Static2")
	Control_SetVisible(sc, switch)
	sc = Dialog_GetControl( gDialogHandle, "Static3")
	Control_SetVisible(sc, switch)
	sc = Dialog_GetControl( gDialogHandle, "Static4")
	Control_SetVisible(sc, switch)
	sc = Dialog_GetControl( gDialogHandle, "Static5")
	Control_SetVisible(sc, switch)

	sc = Dialog_GetControl( gDialogHandle, "Image5")
	Control_SetVisible(sc, switch)
	sc = Dialog_GetControl( gDialogHandle, "Image2")
	Control_SetVisible(sc, switch)
	sc = Dialog_GetControl( gDialogHandle, "Image3")
	Control_SetVisible(sc, switch)
	sc = Dialog_GetControl( gDialogHandle, "Image4")
	Control_SetVisible(sc, switch)

	g_helpVisible = switch
end

function btnHelp_OnButtonClicked( buttonhandle )
	Log("HelpClicked")
	if g_helpVisible then
		showHelpMenus ( false )
	else
		showHelpMenus ( true )
	end
end

-- DRF - Added
function RenderTextEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local msg = KEP_EventDecodeString( event )
	Log("RenderTextEventHandler: '"..msg.."'")

    -- Update Inventory On Transaction Complete
	if (string.find(msg, "Transaction complete")) then 
		KEP_MessageBox("Transaction Complete")	
		g_updateInventorySec = 1.0 -- update inventory after 1 second
		return
    end

    -- Message Box Transaction Errors
	if (string.find(msg, "You can't carry anything else")) then 
		KEP_MessageBox(msg)	
		return
	end
end

-- DRF - Added
g_updateInventorySec = 0
function Dialog_OnRender(dialogHandle, elapsedSec)
	
    -- Update Dialog Time
    if (g_updateInventorySec > 0) then 
		g_updateInventorySec = g_updateInventorySec - elapsedSec
		if (g_updateInventorySec <= 0) then
			g_updateInventorySec = 0
			UpdateVendorInventory()
			UpdatePlayerInventory()
		end
	end
end
