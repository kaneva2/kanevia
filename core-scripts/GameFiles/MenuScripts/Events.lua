--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

-- Number of events visible on one page
VISIBLE_EVENTS = 6

-- Max events to get in the reugular webcall
MAX_EVENTS = 1000

-- Statics for status of event
NOT_INVITED = "NOT_INVITED"
INVITED = "INVITED"
ACCEPTED = "ACCEPTED"
DECLINED = "DECLINED"

-- Text for status string
NO_RESULTS_TEXT = "No events found at this time."
LOADING_TEXT = "Loading...."

-- List holding all of the events
g_eventsList = {}

g_listHandles = {} -- Table of handles of all of the lists to be synced
g_bSyncAllLists = 0 -- 1 if currently synchronizing list boxes
g_lastTrackPos = 0 -- Last position of the scrollbar. Used to avoid updating without actual changes
g_loading = false -- Currently loading places>

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	--Create functions for Attending buttons
	for i = 1, VISIBLE_EVENTS do
		_G["btnAttend" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			local s, e, n = string.find(Control_GetName(btnHandle), "(%d+)")
			
			-- Get track position of the listbox, add the attending number pressed
			local eventNum = tonumber(ScrollBar_GetTrackPos(ListBox_GetScrollBar(gHandles["lstAttending"])))
			eventNum = eventNum + tonumber(n)

			attendEvent(g_eventsList[eventNum])
		end
	end

	-- All of the list handles together for syncing purposes
	g_listHandles = { 	gHandles["lstEvent"],
						gHandles["lstWhen"],
						gHandles["lstWhere"],
						gHandles["lstAttending"],
						gHandles["lstDecline"] }

	getEvents()
end

---------------------------------------------------------------------
-- Loading and Displaying Events
---------------------------------------------------------------------
function getEvents()
	resetEventList() -- Clear current events list

	local webAddress = GameGlobals.WEB_SITE_PREFIX .. "kgp/eventList.aspx"
	makeWebCall(webAddress, WF.EVENT_LIST, 1, MAX_EVENTS)

	-- Next Stop: Browser Page Ready Handler, filter WF.EVENT_LIST
end

-- Catch the xml from the server
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.EVENT_LIST or filter == WF.ACCEPT_EVENT or filter == WF.DECLINE_EVENT then
		local xmlData = KEP_EventDecodeString( event )

		-- If there are events returned, parse them and add to global events table
		local s, e, records = string.find(xmlData, "<NumberRecords>(.-)</NumberRecords>")
		if tonumber(records) > 0 then
			g_eventsList = parseEvents(xmlData, false)
		end

		-- Get 3 premium events to add to bottom
		local webAddress = GameGlobals.WEB_SITE_PREFIX .. "kgp/eventList.aspx?onlyPremium=Y"
		makeWebCall(webAddress, WF.PREMIUM_EVENT_LIST, 1, 3)
		-- Next Stop: BrowserPageReadyHandler, filter WF.PREMIUM_EVENT_LIST

		return KEP.EPR_CONSUMED
	elseif filter == WF.PREMIUM_EVENT_LIST then
		local xmlData = KEP_EventDecodeString ( event )

		-- If there are events returned, parse them and add to global events table
		local s, e, records = string.find(xmlData, "<NumberRecords>(.-)</NumberRecords>")
		if tonumber(records) > 0 then
			local premiumEvents = parseEvents(xmlData, true)

			for i, v in ipairs(premiumEvents) do
				table.insert(g_eventsList, v)
			end
			
		end

		-- Done getting events. If the table has any events in it, display them, otherwise display No Results
		if #g_eventsList > 0 then
			displayEvents()
		else
			setNoResults()
		end

		return KEP.EPR_CONSUMED
	end
end

-- Display the g_eventsList table on the lists
function displayEvents()

	-- For each event in the table, add elements to each list
	for i, v in ipairs(g_eventsList) do

		-- Event Name
		ListBox_AddItem(gHandles["lstEvent"], v.name, v.eventID)

		-- When Happening
		-- If Currently happening, display "Happening Now", otherwise display amount of time until
		local when = v.happeningNow and "Happening Now" or timeToEvent(v.minutesToEvent or 0)
		ListBox_AddItem(gHandles["lstWhen"], when, v.eventID)

		-- Location of Event
		ListBox_AddItem(gHandles["lstWhere"], v.location, v.eventID)

		-- If attending, write "Attending", otherwise keep blank because button will be over it
		local attending = v.inviteStatus == ACCEPTED and not v.happeningNow
		ListBox_AddItem(gHandles["lstAttending"], attending and "Attending" or "", v.eventID)

		-- If not accepted, display a decline link
		local decline = v.inviteStatus == INVITED or v.inviteStatus == NOT_INVITED
		ListBox_AddItem(gHandles["lstDecline"], decline and "Decline" or "", v.eventID)
	end

	-- Update controls outside of  (attedning buttons, div lines, highlights)
	local track_pos = tonumber(ScrollBar_GetTrackPos(ListBox_GetScrollBar(gHandles["lstDecline"])))
	updateExtraListControls(track_pos)

	-- Reenable lists, hide loading status
	setLoading(false)
end

-- Display time to event in minutes if less than an hour, hours if less than a day, or days
function timeToEvent(minutes)
	local hours = math.floor(minutes/60)
	local days = math.floor(minutes/60/24)

	if minutes <= 0 then
		return "Happening Soon"
	elseif minutes <= 1 then
		return "In 1 Minute"
	elseif hours < 1 then
		return "In " .. tostring(minutes) .. " Minutes"
	elseif hours <= 1 then
		return "In 1 Hour"
	elseif days < 1 then
		return "In " .. tostring(hours) .. " Hours"
	elseif days <= 1 then
		return "In 1 day"
	else
		return "In " .. tostring(days) .. " Days"
	end
end

-- Parse Events to a table from XML
-- Takes in XML string, and premium if part of the premium call
function parseEvents(xml, premium)
	local eventsList = {}

	for eventBlock in string.gmatch(xml, "<Event>(.-)</Event>") do
		local event = {}

		local s, e, eventID = string.find(eventBlock, "<event_id>(.-)</event_id>")
		local s, e, userID = string.find(eventBlock, "<user_id>(.-)</user_id>")
		local s, e, communityID = string.find(eventBlock, "<community_id>(.-)</community_id>")
		local s, e, name = string.find(eventBlock, "<Name>(.-)</Name>")
		local s, e, location = string.find(eventBlock, "<location>(.-)</location>")
		local s, e, minutesToEvent = string.find(eventBlock, "<minutes_to_event>(.-)</minutes_to_event>")
		local s, e, trackingID = string.find(eventBlock, "<tracking_request_GUID>(.-)</tracking_request_GUID>")
		local s, e, happeningNow = string.find(eventBlock, "<happening_now>(.-)</happening_now>")
		local s, e, isPremium = string.find(eventBlock, "<is_premium>(.-)</is_premium>")
		local s, e, mine = string.find(eventBlock, "<mine>(.-)</mine>")
		local s, e, inviteStatus = string.find(eventBlock, "<invite_status>(.-)</invite_status>")
		local s, e, STPURL = string.find(eventBlock, "<STPURL>(.-)</STPURL>")

		event["eventID"] = tonumber(eventID or 0)
		event["userID"] = tonumber(userID or 0)
		event["communityID"] = tonumber(userID or 0)
		event["name"] = HTML_Escape(name) or ""
		event["location"] = HTML_Escape(location) or ""
		event["minutesToEvent"] = tonumber(minutesToEvent or 0)
		event["trackingID"] = trackingID or ""
		event["happeningNow"] = happeningNow == "Y"
		event["isPremium"] = isPremium == "Y"
		event["mine"] = mine == "Y"
		event["inviteStatus"] = inviteStatus or ""
		event["STPURL"] = STPURL or ""

		-- If the event is yours, treat it as accepted
		if event.mine then
			event.inviteStatus = ACCEPTED
		end

		-- For preimum list
		if premium and (event.inviteStatus == NOT_INVITED and event.isPremium) then
			table.insert(eventsList, event)
		-- For non premium list
		elseif not premium and (event.inviteStatus == INVITED or event.inviteStatus == ACCEPTED) then
			table.insert(eventsList, event)
		end
	end

	return eventsList
end

-- Remove all events
function resetEventList()

	--Remove items from all lsits
	for i, v in ipairs(g_listHandles) do
		ListBox_RemoveAllItems(v)
	end

	-- Turn off extra controls not on lists
	for i = 1, VISIBLE_EVENTS do
		if (i <= 5) then 
			Control_SetVisible(gHandles["imgLstDiv" .. i], false)
		end
		Control_SetVisible(gHandles["btnAttend" .. i], false)
		Control_SetVisible(gHandles["imgHighlight" .. i], false)
	end

	-- Reset global events list
	g_eventsList = {}

	-- Turn loading status on
	setLoading(true)
end

-- Set status text to no results
function setNoResults()
	Static_SetText(gHandles["stcLoading"], NO_RESULTS_TEXT)
end

function setLoading(loading)
	-- Set if loading
	g_loading = loading

	-- Turn on and set status text
	Control_SetVisible(gHandles["stcLoading"], loading)
	if loading then
		Static_SetText(gHandles["stcLoading"], LOADING_TEXT)
	end

	-- Turn on or off each list handle
	for i, v in pairs(g_listHandles) do
		Control_SetVisible(v, not loading)
	end
end

---------------------------------------------------------------------
-- Actions
---------------------------------------------------------------------
-- When Event name selected, send to event page on web
function lstEvent_OnListBoxSelection(listBoxHandle, x, y, index, name)
	local eventID = ListBox_GetItemData(listBoxHandle, index)
	local web_address = GameGlobals.WEB_SITE_PREFIX_KANEVA .. "mykaneva/events/eventDetail.aspx?event=" .. tostring(eventID)
	launchWebBrowser(web_address)
end

-- If Decline visible, send a Decline to web
function lstDecline_OnListBoxSelection(listBoxHandle, x, y, index, name)
	if name and name ~= "" then
		local eventID = ListBox_GetItemData(listBoxHandle, index)

		resetEventList()

		local webAddress = GameGlobals.WEB_SITE_PREFIX .. "kgp/eventList.aspx?action=DeclineEvent&eventId=" .. eventID
		makeWebCall(webAddress, WF.DECLINE_EVENT, 1, MAX_EVENTS)
	end
end

-- Go to event if happening and accepted, otherwise accept
function attendEvent(event)
	if event then
		if event.inviteStatus == INVITED or event.inviteStatus == NOT_INVITED then
			resetEventList()

			-- Send tracking ID to web
			local webAddress = GameGlobals.WEB_SITE_PREFIX..TRACKING_REQUEST_ACCEPT_SUFFIX.. event.trackingID
			makeWebCall(webAddress, WF.TRACKING_EVENT_ACCEPT, 1, 1)

			-- Accept event
			webAddress = GameGlobals.WEB_SITE_PREFIX .. "kgp/eventList.aspx?action=AcceptEvent&eventId=" .. event.eventID
			makeWebCall(webAddress, WF.ACCEPT_EVENT, 1, MAX_EVENTS)
		end

		-- Go to event if it's happening now, regardless of status (declined never gets here)
		if event.happeningNow then
			gotoURL(event.STPURL)
		end
	end
end

---------------------------------------------------------------------
-- List Box Syncing
---------------------------------------------------------------------
-- Update the parts of the menu that aren't actually list boxes and sync with them
function updateExtraListControls(track_pos)

	-- For each visible event
	for i=track_pos + 1, (track_pos + VISIBLE_EVENTS) do
		local event = g_eventsList[i]
		local listPosition = i - track_pos
		
		-- Set Attend button
		local btnHandle = gHandles["btnAttend" .. listPosition]
		Control_SetVisible(btnHandle, event ~= nil)

		-- Set Divider (1-5 only)
		if (listPosition >= 1 and listPosition <= 5) then 
			Control_SetVisible(gHandles["imgLstDiv" .. listPosition], event ~= nil)
		end

		-- Set Highlight
		Control_SetVisible(gHandles["imgHighlight" .. listPosition], false)

		--If there is actually an event at this index
		if event then

			-- Set button text/visibility
			if event.happeningNow then
				Static_SetText(Button_GetStatic(btnHandle), "GO")
			elseif event.inviteStatus == INVITED or event.inviteStatus == NOT_INVITED then
				Static_SetText(Button_GetStatic(btnHandle), "Attend")
			else
				Control_SetVisible(btnHandle, false)
			end

			-- If premium and not invited, add highlight
			if event.isPremium and event.inviteStatus == NOT_INVITED then
				Control_SetVisible(gHandles["imgHighlight" .. listPosition], true)
			end
		end
	end
end

-- Sync lists when scroll bar changed
function lstEvent_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	if g_bSyncAllLists == 0 then syncAllScrollBars( g_listHandles, listBoxHandle ) end
end

function lstWhen_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	if g_bSyncAllLists == 0 then syncAllScrollBars( g_listHandles, listBoxHandle ) end
end

function lstWhere_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	if g_bSyncAllLists == 0 then syncAllScrollBars( g_listHandles, listBoxHandle ) end
end

function lstAttending_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	if g_bSyncAllLists == 0 then syncAllScrollBars( g_listHandles, listBoxHandle ) end
end

function lstDecline_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	if g_bSyncAllLists == 0 then 
		syncAllScrollBars( g_listHandles, listBoxHandle ) 
		
		-- Update extra controls if actual change has happened
		if not g_loading and track_pos ~= g_lastTrackPos then
			updateExtraListControls(track_pos)
		end

		g_lastTrackPos = track_pos -- Used to avoid extraneous updating
	end
end

---------------------------------------------------------------------
-- Misc Buttons/Closing Functions
---------------------------------------------------------------------
-- Create New event button, sends to Create Event webpage
function btnCreate_OnButtonClicked(dialogHandle)
	local web_address = GameGlobals.WEB_SITE_PREFIX_KANEVA .. "mykaneva/events/eventForm.aspx"
	launchWebBrowser( web_address)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function HTML_Escape(msg)
    msg = string.gsub(msg, "&amp;", "&")
    msg = string.gsub(msg, "&gt;", ">")
    msg = string.gsub(msg, "&lt;", "<")
    msg = string.gsub(msg, "&#263A;", ";%)")
    msg = string.gsub(msg, "&#263A;", ":%)")
    msg = string.gsub(msg, "&#2639;", ";%(")
    msg = string.gsub(msg, "&#2639;", ":%(")
    msg = string.gsub(msg, "&#2620;",";x")
    msg = string.gsub(msg, "&#2620;", ":x")
    return msg
end