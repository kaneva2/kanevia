--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\ActionItemFunctions.lua")
dofile("..\\MenuScripts\\DevToolsConstants.lua")
dofile("..\\MenuScripts\\UGCFunctions.lua")

--[[
% NOTES: smart objects with JSON param files - YC 201403
+ Environment-specific:
  (*) WOK: only instances can be edited and saved; no script editing is allowed.
  (*) APP: both items and instances can be edited. However, certain items cannot be saved over. Must do a "save as" instead.
+ Types of smart objects:
  (1) Original shop item : "Save As" only (to a new item of type 3)
  (2) Derived shop item  : "Save As" only (to a new item of type 3)
  (3) Custom shop item with local JSON : no limitations                              (APP ONLY)
  (4) Custom item with local script    : no limitations                              (APP ONLY)
  (5) Placed instance of an item       : "Save" JSON
  (6) Orphaned instance                : *DEPRECATED*
+ Special handling regarding "Edit Script"
  (*) Type 1/2/3/5 : script cannot be edited directly. User will be prompted a "save as" dialog and generate a new type 4 item upon save.
  (*) Type 4/6     : no limitations
+ Legacy conversion
  (*) All existing shop items will be treated as type 1
  (*) All existing  app items will be treated as type 4
  (*) All existing  instances will be treated as type 6
+ Benefits by keeping derivation relationship
  (*) Can always track derivatives back to the original item
  (*) Bugs in original item can be fixed along with all derivatives
  (*) Attributes of the original item (such as "Certified" state) can be easily shared by all derivatives through the common "script GLID"
  (*) Instances with parameter changes are no longer orphaned. Basically there is no reason to orphan any instance any more.
  (*) It's now possible to revert a customized item back to its original state by deleting the JSON parameter file.
  (*) For derivatives, only parameters need to be saved instead of the entire script. This will reduce storage space requirements.
+ Potential issues
  (*) Deleting a custom script item (4) can be problematic if there are instances out there
]]

local DEFAULT_GLID_KEY = "___DefaultGlid___"
local DESCRIPTION_KEY = "___Description___"

local PADDING = 10

local MAX_PARAMS = 8
local m_numParams = 0
local m_visibleParams = 0

local m_requestingScripts = false -- Did we request scripts? Done because a single request for scripts returns the info multiple times
local m_publishedScriptRelPath = nil

local m_smartObj = nil 		-- See newObject()
local m_task = ActionItemFunctions.initPendingTask()	-- extra parameters: uploadPath. See ActionItemFunctions.lua/TaskAction for detail

local m_advanced = false 	-- Advanced Mode
local m_paramMapping = {} 	-- Maps selection index to parameter ID

local LIST_HANDLES = { }	-- Initialized in Create since has to be handles

g_bSyncAllLists = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "scriptNameHandler", ACTION_ITEM_SCRIPT_NAME_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "changeParamHandler", CHANGE_PARAM_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "SmartObjectSaveNameHandler", "SmartObjectSaveName", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "saveResultHandler", "SaveActionItemResultEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "PublishCompleteHandler", "3DAppAssetPublishCompleteEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "scriptClientEventHandler", "ScriptClientEvent", KEP.HIGH_PRIO )

	-- From UGCFunctions.lua
	KEP_EventRegisterHandler( "UGCSubmissionFeedbackEventHandler", UGC_SUBMISSION_FEEDBACK_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UploadCompletionHandler", UGC_UPLOAD_COMPLETION_EVENT, KEP.MED_PRIO )

end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( ACTION_ITEM_SCRIPT_NAME_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( DISPLAY_ACTION_ITEM_ERRORS_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( EDIT_PARAM_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( CHANGE_PARAM_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( "SelectSmartObject", KEP.MED_PRIO )
	KEP_EventRegister( "SmartObjectSaveName", KEP.MED_PRIO )
	KEP_EventRegister( "SaveActionItemEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "SaveActionItemResultEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "RefreshActionItemListEvent", KEP.MED_PRIO )
	KEP_EventRegister( "SetupSaveEvent", KEP.MED_PRIO )

	-- From UGCFunctions.lua
	KEP_EventRegister( UGC_SUBMISSION_FEEDBACK_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( UGC_UPLOAD_COMPLETION_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( UGC_UPLOAD_PROGRESS_EVENT, KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	LIST_HANDLES = {
						gHandles["lstEnabled"],
						gHandles["lstProperties"],
						gHandles["lstEdit"]
					}
	ListBox_SetHTMLEnabled(gHandles["lstEnabled"], true)
end

-- This function creates an empty table. Table members listed here are for documentation purposes. Do not remove.
function newObject()
	return {
				name		= nil,	-- item name
				type        = nil,  -- AAType
				glid        = nil,	-- item GLID
				scriptName 	= nil,	-- script name (if different)
				scriptGlid 	= nil,	-- script GLID (if different)
				scriptPath 	= nil,	-- script local path
				jsonPath    = nil,	-- json local path (if any)
				placementId = nil,	-- placement ID: instance or item
				paramKeys 	= nil, 	-- A table of strings with the [identifiers] for AID.params
			}
end

-----------------------------------------------------------------------
-- Event Handlers
-----------------------------------------------------------------------

-- Decode parameters from the calling menu
function scriptNameHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	m_smartObj = newObject()

	m_smartObj.name = KEP_EventDecodeString(event)		-- AAName
	m_smartObj.type = KEP_EventDecodeNumber(event)		-- AAType
	m_smartObj.scriptPath = KEP_EventDecodeString(event)
	m_smartObj.jsonPath = KEP_EventDecodeString(event)
	m_smartObj.placementId = KEP_EventDecodeNumber(event)

	-- Open script editor automatically if requested
	local editScriptNow = KEP_EventDecodeNumber(event)==1
	if editScriptNow then
		m_advanced = true
	end

	m_smartObj.scriptName = string.match(m_smartObj.scriptPath, "([^\\/.]+)%.[^\\/.]+$")

	if m_smartObj.jsonPath=="" then
		m_smartObj.jsonPath = nil
	end

	if m_smartObj.placementId==-1 then
		m_smartObj.placementId = nil
	end

	refreshHelper(true)

	if editScriptNow then
		KEP_ShellOpen( m_smartObj.scriptPath)
	end
end

function changeParamHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local changedKey, changedParam = ActionItemFunctions.decodeParam(event)
	if changedKey == DEFAULT_GLID_KEY then
		-- special case changing default glid
		AID.defaultGLID = changedParam.value
		local iconImage = Image_GetDisplayElement(gHandles["imgThumbNail"])
		KEP_LoadIconTextureByID( AID.defaultGLID, iconImage, gDialogHandle)
	elseif changedKey == DESCRIPTION_KEY then
		-- special case changing description
		AID.description = changedParam.value
	else
		AID.params[changedKey] = changedParam
	end
end

-- User renamed current smart object
function SmartObjectSaveNameHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter==0 then
		-- cancelled by user
		m_task.clear()
		return
	end

	local oldScriptPath = m_smartObj.scriptPath

	-- the new item is now neither a placement or a shop item
	m_smartObj.name = KEP_EventDecodeString(event)				-- Update item name
	m_smartObj.glid = nil										-- Disassociate from shop item (if any)
	m_smartObj.placementId = nil								-- Disassociate from DO placement (if any)

	if m_smartObj.jsonPath==nil or m_task.pending("bake") then				-- If JSON is not used
		-- update path
		local newScriptPath, matched = string.gsub(oldScriptPath, "\\%w+%.[^.]+$", "\\" .. m_smartObj.name .. "%.lua")
		if matched==0 then
			-- substitution failed
			LogError("SmartObjectSaveNameHandler: invalid old script path " .. tostring(oldScriptPath))
			m_task.clear()
			return
		end

		-- If it was a placed smart object instance then change to ActionItems
		newScriptPath = string.gsub(newScriptPath, "\\ActionInstances\\", "\\ActionItems\\")

		m_smartObj.type       = DevToolsUpload.ACTION_ITEM		-- Standalone item
		m_smartObj.scriptPath = newScriptPath
		m_smartObj.jsonPath   = nil								-- Clear JSON path if baking
		m_smartObj.scriptName = m_smartObj.name					-- Also update script name
		m_smartObj.scriptGlid = nil								-- and disassociate script from shop item (if any)

		-- Generate and save new script file to LocalDev
		ActionItemFunctions.saveLua(oldScriptPath, m_smartObj.scriptPath)
		-- next stop: saveResultHandler
	else
		local savedPath = ActionItemFunctions.saveJson(m_smartObj.name, m_smartObj.scriptGlid or m_smartObj.scriptName, m_smartObj.placementId~=nil)
		if savedPath~="" then
			validateAndPublish()
		else
			KEP_MessageBox( "Error saving smart object [" .. tostring(m_smartObj.name) .. "]", "Error" )
		end
	end
end

-- catches the "SaveActionItemResultEvent"
-- it contains a string with the result
function saveResultHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local result = KEP_EventDecodeString(event)
	if result=="Success" then

		if m_task.pending("bake") then
			-- Re-open menu in advanced mode and launch script editor
			m_task.clear()
			ActionItemFunctions.OpenPreset(m_smartObj.name, m_smartObj.type, m_smartObj.scriptPath, m_smartObj.jsonPath, -1, true)
			MenuCloseThis()
		elseif m_task.pending("upload") then
			-- Import UGC and upload to shop
			local uploadPath = m_task.uploadPath
			m_task.clear()
			importAndUpload(uploadPath)
		else
			-- Standard Save/SaveAs
			validateAndPublish()
		end
	else
		m_task.clear()
		KEP_MessageBox( result, "Error" )

		-- Close preset editor to clear any intermediate state
		MenuCloseThis()
	end
end

function validateAndPublish()
	-- Make sure the saved script can be reloaded
	if refreshHelper(true) then
		-- Send the changed script to the 3d app server
		local path
		if m_smartObj.type==DevToolsUpload.ACTION_ITEM or m_smartObj.type==DevToolsUpload.ACTION_INSTANCE then
			-- Upload LUA script
			if m_smartObj.scriptPath==nil then
				LogError("validateAndPublish: scriptPath is nil for [" .. tostring(m_smartObj.name) .. "]")
				return
			end
			path = m_smartObj.scriptPath
		else
			-- Upload JSON file
			if m_smartObj.jsonPath==nil then
				LogError("validateAndPublish: jsonPath is nil for [" .. tostring(m_smartObj.name) .. "]")
				return
			end
			path = m_smartObj.jsonPath
		end

		KEP_UploadAssetTo3DApp(m_smartObj.name, path, m_smartObj.type)
		-- next stop: PublishCompleteHandler
	end
end

-- called when response recieved from server after upload completed
function PublishCompleteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local name = KEP_EventDecodeString(event)
	local publishType = tonumber(KEP_EventDecodeNumber(event))
	local success = tonumber(KEP_EventDecodeNumber(event))
	local errstr = KEP_EventDecodeString(event)
	if success == 0 then
		ActionItemFunctions.openErrorDialog(errors, m_smartObj.scriptPath, m_smartObj.type)
	else
		local relPath = ActionItemFunctions.getRelPathByScriptInfo(publishType, name)
		if publishType==DevToolsUpload.ACTION_INSTANCE or publishType==DevToolsUpload.ACTION_INSTANCE_PARAMS then
			ActionItemFunctions.attachScript(m_smartObj.placementId, relPath)
		else
			local event = KEP_EventCreate( "RefreshActionItemListEvent" )
			KEP_EventQueue( event )
		end

		-- If the item is not placed, then check to see if it has children
		if m_smartObj.placementId==nil then
			m_requestingScripts = true
			m_publishedScriptRelPath = relPath
			ActionItemFunctions.requestServerActionItemList()	-- Retrieve a list of attached scripts
			-- next stop: scriptClientEventHandler / GetAllScripts
		else
			MenuCloseThis()
		end
	end
end

function UGCSubmissionFeedbackEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == UGC_TYPE.ACTION_ITEM then
		local sfe = UGCI.DecodeSubmissionFeedbackEvent( event, filter, objectid )
		if sfe.httpStatusCode==200 and sfe.result==SubmissionResult.SUCCEEDED then
			if sfe.uploadId ~= nil then
				Log("UGCSubmissionFeedbackEvent: succeeded, uID=" .. sfe.uploadId )	-- no confirmation required, go ahead and upload
				UGCI.UploadUGCObj( 0, sfe.uploadId, sfe.ugcType, sfe.ugcObjId, sfe.objName )		-- 0: not a derivation
			end
		end
		return KEP.EPR_CONSUMED
	end
	return KEP.EPR_OK
end

function UploadCompletionHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local isDerivative = KEP_EventDecodeNumber( event )
	local ugcType = KEP_EventDecodeNumber( event )
	local uploadId = KEP_EventDecodeString( event )
	local ugcObjId = KEP_EventDecodeNumber( event )
	local ugcObjName = KEP_EventDecodeString( event )

	if ugcType == UGC_TYPE.ACTION_ITEM then
		local bundleGlids = ActionItemFunctions.CalculateBundleGlids()
		local enc_aidName = KEP_UrlEncode(AID.name or "")
		local enc_aidDescription = KEP_UrlEncode(AID.description or "")
		local urlCreate = "UploadMemberDesigns.aspx?uploadid=" .. uploadId .. "&defaultGlid=" .. tostring(AID.defaultGLID) .. "&bundleGlids=" .. bundleGlids .. "&name=" .. enc_aidName .. "&description=" .. enc_aidDescription
		urlCreate = GameGlobals.WEB_SITE_PREFIX_SHOP .. urlCreate
		KEP_LaunchBrowser( urlCreate )
	end
end

function scriptClientEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == ScriptClientEventTypes.GetAllScripts and m_requestingScripts then

		-- Server returned a list of attached scripts
		local relPath = m_publishedScriptRelPath
		m_requestingScripts = false
		m_publishedScriptRelPath = nil

		local numRunningScripts = KEP_EventDecodeNumber(event)
		for i=1,numRunningScripts do
			local objectId = KEP_EventDecodeNumber(event)
			local filename = KEP_EventDecodeString(event)

			-- If the scriptName matches the scriptName of the smart object we're editing, it's a child
			if relPath and filename==relPath then
				-- Restart changed script on this placement
				ActionItemFunctions.attachScript(objectId, filename)
			end
		end

		MenuCloseThis()
	end
end

-----------------------------------------------------------------------
-- Setup Menu
-----------------------------------------------------------------------
function refreshHelper(refreshItem)
	if refreshItem then
		-- Check LUA script for errors
		local errors = ActionItemFunctions.errorCheck(m_smartObj.type, m_smartObj.scriptPath)
		if errors then
			LogWarn("refreshHelper: errorCheck: " .. tostring(errors))
			if m_smartObj.jsonPath~=nil then
				-- If this is a derivative item, display a different error. Basically this should not have happened.
				errors = "Error loading parent smart object [" .. m_smartObj.scriptName .. "]."
			end
			ActionItemFunctions.openErrorDialog(errors, m_smartObj.jsonPath or m_smartObj.scriptPath, m_smartObj.type)
			return false
		end

		m_smartObj.glid = nil
		if string.match(m_smartObj.name, "^%d+$") and (m_smartObj.type == DevToolsUpload.ACTION_ITEM or m_smartObj.type == DevToolsUpload.ACTION_ITEM_PARAMS) then
			local glidTest = tonumber(m_smartObj.name)
			if glidTest ~= 0 then
				m_smartObj.glid = glidTest
			end
		end

		m_smartObj.scriptGlid = nil
		if string.match(m_smartObj.scriptName, "^%d+$") then
			local glidTest = tonumber(m_smartObj.scriptName)
			if glidTest ~= 0 then
				m_smartObj.scriptGlid = glidTest
			end
		end

		ActionItemFunctions.getActionItemTable(m_smartObj.scriptPath)
		m_smartObj.paramKeys = ActionItemFunctions.GetOrderedParams(m_smartObj.scriptPath)

		if m_smartObj.jsonPath~=nil then
			-- Merge JSON data
			errors = ActionItemFunctions.mergeJsonData(m_smartObj.type, m_smartObj.jsonPath, m_smartObj.paramKeys)
			if errors then
				LogWarn("refreshHelper: errors=[" .. tostring(errors) .. "]")
				ActionItemFunctions.openErrorDialog(errors, m_smartObj.jsonPath, m_smartObj.type)
				return false
			end
		end
	end

	setBasicInfo()

	-- If advanced, show all properties, otherwise it's the number of basic props
	m_numParams = m_advanced and #m_smartObj.paramKeys or numBasicProperties()
	m_visibleParams = math.min(m_numParams, MAX_PARAMS)

	-- If no properties or in basic mode and no basic properties
	if m_numParams <= 0 then
		showNoBasic(true)
	elseif not m_advanced then
		displayBasicMenu()
	else
		displayAdvancedMenu()
	end

	return true
end

-- Set up name, image, and description
function setBasicInfo()
	EditBox_SetText(gHandles["edName"], AID.name, false)
	EditBox_SetText(gHandles["edDescription"], AID.description, false)
	local iconImage = Image_GetDisplayElement(gHandles["imgThumbNail"])
	KEP_LoadIconTextureByID( AID.defaultGLID, iconImage, gDialogHandle)
end

-- Does the action item have any basic properties?
function numBasicProperties()
	local numBasic = 0

	-- Check to make sure definitions valid
	if AID ~= nil and AID.params ~= nil then
		local key, param
		local i = 1
		-- Go through each param
		for i, key in ipairs(m_smartObj.paramKeys) do
			local param = AID.params[key]

			-- if any param does not have an advanced property, it is basic and we're done here.
			if not param.advanced then
				numBasic = numBasic + 1
			end
		end
	end

	return numBasic
end

function setControlLocation ( handleName, location )
	if type(handleName) == "string" then
		Control_SetLocationY(gHandles[handleName], location)

	elseif type(handleName) == "table" and #handleName > 0 then
		for i, v in ipairs(handleName) do
			Control_SetLocationY(gHandles[v], location)
		end
		handleName = handleName[1]
	else
		Log("setControlLocation: input must be a string or non-empty table of strings")
		return
	end

	return ( location + Control_GetHeight(gHandles[handleName]))
end

function showNoBasic( show )
	Control_SetVisible(gHandles["stcNoBasic"], show)
	Control_SetVisible(gHandles["btnClose2"], show and not m_advanced)
	Control_SetVisible(gHandles["btnAdvanced"], show and not m_advanced)

	if show then
		showProperties( ) -- no properties so will hide
		showAdvancedTable( false )
		local location = Control_GetLocationY(gHandles["stcNoBasic"]) + Control_GetHeight(gHandles["stcNoBasic"])

		if not m_advanced then
			location = setControlLocation ( "btnAdvanced", location + PADDING)
			Static_SetText(gHandles["stcNoBasic"], "This object has no basic properties")
		else
			location = showAdvancedTable(true, location + PADDING)
			Static_SetText(gHandles["stcNoBasic"], "This object has no properties")
		end

		location = setControlLocation ( "imgHorizLine", location + PADDING)

		if not m_advanced then
			location = setControlLocation ( "btnClose2", location + PADDING)
		else
			location = showSaveButtons( true, location + PADDING )
		end

		MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), location + Dialog_GetCaptionHeight(gDialogHandle) + PADDING)
	end
end

function displayBasicMenu()
	showNoBasic(false)
	showAdvancedTable(false)
	showProperties()
	Control_SetVisible(gHandles["btnAdvanced"], true)

	-- Start resizing at the bottom of the properties menu
	local location = Control_GetLocationY(gHandles["imgLstBG"]) + Control_GetHeight(gHandles["imgLstBG"])
	location = setControlLocation ( "btnAdvanced", location + PADDING )
	location = setControlLocation ( "imgHorizLine", location + PADDING )
	location = showSaveButtons( true, location + PADDING )

	MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), location + Dialog_GetCaptionHeight(gDialogHandle) + PADDING)
end

function displayAdvancedMenu(  )
	showNoBasic(false)
	Control_SetVisible(gHandles["btnAdvanced"], false)
	showProperties()

	-- Start resizing at the bottom of the properties menu
	local location = Control_GetLocationY(gHandles["imgLstBG"]) + Control_GetHeight(gHandles["imgLstBG"])
	location = showAdvancedTable(true, location + PADDING)
	location = setControlLocation ( "imgHorizLine", location + PADDING )
	location = showSaveButtons( true, location + PADDING )

	MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), location + Dialog_GetCaptionHeight(gDialogHandle) + PADDING)
end

function showProperties()
	local display = m_numParams and m_numParams > 0
	Control_SetVisible(gHandles["imgProperties"], display)
	Control_SetVisible(gHandles["stcProperties"], display)
	Control_SetVisible(gHandles["imgLstBG"], display)
	Control_SetVisible(gHandles["lstEnabled"], display)
	Control_SetVisible(gHandles["lstProperties"], display)
	Control_SetVisible(gHandles["lstEdit"], display)

	if display then
		-- Sorry for the magic numbers. Will clean up. It's all resizing related.
		local rowHeight = ListBox_GetRowHeight(gHandles["lstEnabled"])
		local listHeight = rowHeight * m_visibleParams + 6
		Control_SetSize(gHandles["imgLstBG"], Control_GetWidth(gHandles["imgLstBG"]), listHeight + 2)
		Control_SetSize(gHandles["lstEnabled"], Control_GetWidth(gHandles["lstEnabled"]), listHeight)
		Control_SetSize(gHandles["lstProperties"], Control_GetWidth(gHandles["lstProperties"]), listHeight)
		Control_SetSize(gHandles["lstEdit"], Control_GetWidth(gHandles["lstEdit"]), listHeight)

		Control_SetLocationY(gHandles["imgLstBG9"], Control_GetLocationY(gHandles["imgLstBG"]) + Control_GetHeight(gHandles["imgLstBG"]) - Control_GetHeight(gHandles["imgLstBG9"]) -1)

		populateTheList()
	end
end

function populateTheList()
	ListBox_RemoveAllItems(gHandles["lstProperties"])
	ListBox_RemoveAllItems(gHandles["lstEnabled"])
	ListBox_RemoveAllItems(gHandles["lstEdit"])

	if AID ~= nil and AID.params ~= nil then
		local key, param
		local i = 1
		local idxMapping = 0
		for i, key in ipairs(m_smartObj.paramKeys) do
			local param = AID.params[key]

			-- Only add if basic or not in advanced mode
			if m_advanced or not param.advanced then
				if param.name ~= nil then
					ListBox_AddItem(gHandles["lstProperties"], param.name, i)
				else
					ListBox_AddItem(gHandles["lstProperties"], "unknown", i)
				end

				local fontStart = "<font color="
				local body = "FFFFFFFF>"
				local fontEnd = "</font>"

				if param.enabled == true then
					body = "FF00AA00>ON"
				elseif param.enabled == false then
					body = "FFAA0000>OFF"
				end

				ListBox_AddItem(gHandles["lstEnabled"], fontStart.. body .. fontEnd, i)
				ListBox_AddItem(gHandles["lstEdit"], "EDIT", i)

				-- Store the index -> parameter map entry.
				m_paramMapping[idxMapping] = i
				idxMapping = idxMapping + 1
			end
		end
	end
	updateDivLines()
end

-- Starting at the top, for each line if there is a param, add a div line
-- If it's an odd line, also add a background
function updateDivLines()
	local startPos = ScrollBar_GetTrackPos(ListBox_GetScrollBar(gHandles["lstProperties"]))
	local displayedItems = #m_smartObj.paramKeys - startPos
	for i = 1, m_visibleParams do
		local lstDiv = "imgLstDiv" .. tostring(i)
		local lstBG = "imgLstBG" .. tostring(i)
		local vertDiv = "imgVertDiv" .. tostring(i)

		local itemDisplayed = i <= displayedItems
		local oddItem = ((startPos + i) % 2) == 0

		Control_SetVisible(gHandles[lstDiv], itemDisplayed)
		Control_SetVisible(gHandles[vertDiv], itemDisplayed)

		Control_SetVisible(gHandles[lstBG], itemDisplayed and oddItem)
	end

	-- Show the last alternating line
	local showBottomBG = (displayedItems >= m_visibleParams) and ((startPos + m_visibleParams + 1) % 2 == 0)
	Control_SetVisible(gHandles["imgLstBG9"], showBottomBG)
end

-- Show the advanced table that contains editing buttons and GLIDs/PIDs
function showAdvancedTable( show, location )
	local showDevTools = show and KEP_IsWorld() -- Only PID/GLID show in a world
	local placedObject = m_smartObj.placementId~=nil
	local showUpload = showDevTools and DevMode() > 0

	Control_SetVisible(gHandles["btnOriginal"], show)
	Control_SetVisible(gHandles["btnEditScript"], showDevTools)
	Control_SetVisible(gHandles["imgIDBG"], showDevTools)
	Control_SetVisible(gHandles["stcGLID"], showDevTools)
	Control_SetVisible(gHandles["stcPID"], showDevTools and placedObject)
	Control_SetVisible(gHandles["imgDot"], showDevTools and placedObject)
	Control_SetVisible(gHandles["btnEditModel"], show)
	Control_SetVisible(gHandles["btnUpload"], showUpload)

	if show then
		location = setControlLocation({"btnOriginal", "btnEditModel"}, location)

		if showDevTools then
			location = setControlLocation({"imgIDBG", "stcGLID", "stcPID", "imgDot"}, location - 1)
			location = setControlLocation({"btnEditScript", "btnUpload"}, location - 1)
		end

		displayUpload( showUpload )
		displayPID( placedObject )

		if placedObject then
			Static_SetText(gHandles["btnOriginal"], "Show Original")
			Static_SetText(gHandles["stcPID"], "PID " .. tostring(m_smartObj.placementId) )
		else
			Static_SetText(gHandles["btnOriginal"], "Original Version")
		end

		-- If editing a script by placement ID, the edit model feature should be disabled.
		-- You cannot change the model on something that has already been placed.
		Control_SetEnabled(gHandles["btnEditModel"], show and not placedObject)
		Control_SetEnabled(gHandles["btnOriginal"], placedObject)
		Static_SetText(gHandles["stcGLID"], "GLID " .. AID.defaultGLID)
	end

	return location
end

-- Display the pid or stretch the glid to full width
function displayPID ( show )
	local glid = gHandles["stcGLID"]
	local width = show and Control_GetWidth(gHandles["stcPID"]) or Control_GetWidth(gHandles["imgIDBG"])
	Control_SetSize(glid, width, Control_GetHeight(glid))
end

function displayUpload( show )
	local editScript = gHandles["btnEditScript"]
	local width = show and Control_GetWidth(gHandles["btnUpload"]) or Control_GetWidth(gHandles["imgIDBG"])
	Control_SetSize(editScript, width, Control_GetHeight(editScript))
end

function showSaveButtons( show, location )
	Control_SetVisible(gHandles["btnSave"], show)
	Control_SetVisible(gHandles["btnSaveArrow"], show)
	Control_SetVisible(gHandles["btnCancel"], show)
	Control_SetVisible(gHandles["btnSaveAs"], false) -- always false because hidden at the beginning

	if show then
		if m_smartObj.placementId~=nil then
			Control_SetToolTip(gHandles["btnSave"], "Saves this object only")
		else
			Control_SetToolTip(gHandles["btnSave"], "Saves this and all objects in world")
		end

		location = setControlLocation ( {"btnSave", "btnSaveArrow", "btnCancel"}, location)
		setControlLocation ( "btnSaveAs", location - 1) --Needs to be right up against the Save. Don't save location

		-- If it is a shop item in the inventory, we need to disable "Save" and only show "Save As"
		showOnlySaveAs( m_smartObj.glid and m_smartObj.placementId==nil )
	end

	return location
end

-- Hide the save button when just Save As is avaiable
function showOnlySaveAs( showSaveAs )
	local saveArrow = gHandles["btnSaveArrow"]
	local saveButton = gHandles["btnSave"]

	if showSaveAs then
		Control_SetToolTip(gHandles["btnSave"], "Save a new copy of this smart object")
	end

	local width = Control_GetWidth(gHandles["btnCancel"])
	local text = "Save As..."
	if not showSaveAs then
		width = width - Control_GetWidth(saveArrow) + 1
		text = "Save"
	end

	Control_SetVisible(saveArrow, not showSaveAs)
	Control_SetSize(saveButton, width, Control_GetHeight(saveButton))
	Static_SetText(saveButton, text)
end

-----------------------------------------------------------------------
-- Buttons
-----------------------------------------------------------------------
function btnAdvanced_OnButtonClicked( buttonHandle )
	m_advanced = true
	refreshHelper(false)
end

function toggleEnabled(index, listBoxHandle)
	local paramId = m_paramMapping[index]
	if AID.params[m_smartObj.paramKeys[paramId]].enabled then
		AID.params[m_smartObj.paramKeys[paramId]].enabled = false
		ListBox_SetItemText( listBoxHandle, index, "<font color=FFAA0000>OFF</font>")
	elseif AID.params[m_smartObj.paramKeys[paramId]].enabled == false then
		AID.params[m_smartObj.paramKeys[paramId]].enabled = true
		ListBox_SetItemText( listBoxHandle, index, "<font color=FF00AA00>ON</font>")
	end
end

function editParam(index)

	MenuOpenModal("ParamEditor.xml")

	local paramId = m_paramMapping[index]
	local param = AID.params[m_smartObj.paramKeys[paramId]]
	local event = KEP_EventCreate( EDIT_PARAM_EVENT )
	ActionItemFunctions.encodeParam(event, param, m_smartObj.paramKeys[paramId])
	KEP_EventQueue( event )
end

function btnEditModel_OnButtonClicked(buttonHandle)

	MenuOpenModal("ParamEditor.xml")

	local fakeParam = {}
	fakeParam.enabled = true
	fakeParam.name = "Default object"
	fakeParam.description = "Choose a default object for this action item"
	fakeParam.type = "OBJECT"
	fakeParam.value = AID.defaultGLID
	local event = KEP_EventCreate( EDIT_PARAM_EVENT )
	ActionItemFunctions.encodeParam(event, fakeParam, DEFAULT_GLID_KEY)
	KEP_EventQueue( event )
end

function btnEditScript_OnButtonClicked(buttonHandle)
	if m_task.pending() then
		LogWarn("btnEditScript: cannot proceed. Task pending: " .. tostring(m_task.action))
		return
	end

	if m_smartObj.jsonPath~=nil or m_smartObj.glid~=nil or m_smartObj.scriptGlid~=nil then
		-- Cannot edit script on an param-only item/instance. Must save as a new item first
		m_task.set("bake", m_smartObj)
		btnSaveAs_OnButtonClicked()
		return
	end

	KEP_ShellOpen( m_smartObj.scriptPath)
end

function btnUpload_OnButtonClicked(buttonHandle)
	if m_task.pending() then
		LogWarn("btnUpload: cannot proceed. Task pending: " .. tostring(m_task.action))
		return
	end

	if m_smartObj.glid ~= nil then
		LogWarn("btnUpload: cannot upload a shop item: " .. tostring(m_smartObj.name))
		return
	end

	local ugcID, uploadPath

	if m_smartObj.scriptGlid ~= nil then
		-- params upload only (item or instance)
		uploadPath = m_smartObj.jsonPath

	elseif m_smartObj.jsonPath == nil then
		-- custom script item or orphaned instance
		uploadPath = m_smartObj.scriptPath

	else
		-- derivative of a local server item - parent not in shop - bake script with params before upload
		-- TODO: delete tmp file after upload
		local tmpPath, matched = string.gsub(m_smartObj.jsonPath, "%.json$", ".tmp")	-- save temporary LUA in .tmp file to stay out of inventory menu
		if matched == 0 then
			LogWarn("btnUpload: invalid JSON path [" .. tostring(m_smartObj.jsonPath) .. "]")
		else
			m_task.set("upload", m_smartObj)
			m_task.uploadPath = tmpPath
			ActionItemFunctions.saveLua(m_smartObj.scriptPath, tmpPath)
			-- next stop: saveResultHandler
		end
		
		return
	end

	-- Ready
	importAndUpload(uploadPath)
end

-- Import UGC and upload to shop
function importAndUpload(uploadPath)
	
	local bundleGlids = ActionItemFunctions.CalculateBundleGlids()
	ugcID = UGC_ImportActionItem( uploadPath, AID.defaultGLID, bundleGlids, m_smartObj.scriptGlid or -1 )		-- -1 INVALID_GLID
	if ugcID<0 then
		LogWarn("importAndUpload: failed: " .. tostring(uploadPath) )
		KEP_MessageBox( "Error importing script file.\n\n[code: 1]", "Import failed" )
	else
		-- TODO: submit with ugcID
		UGCI.SubmitUGCObj( UGC_TYPE.ACTION_ITEM, 0, 0, "", "", 0, 0 )
	end
end

function btnOriginal_OnButtonClicked(buttonHandle)

	MenuOpen("Inventory.xml")

	--Tell which item to select
	local ev = KEP_EventCreate("SelectSmartObject")
	if m_smartObj.type == DevToolsUpload.ACTION_INSTANCE or m_smartObj.type == DevToolsUpload.ACTION_INSTANCE_PARAMS then
		KEP_EventEncodeString(ev, m_smartObj.scriptName)
	else
		KEP_EventEncodeString(ev, m_smartObj.name)
	end
	KEP_EventQueue(ev)

	MenuCloseThis()
end

function showSaveAs(show)
	Control_SetEnabled(gHandles["btnSaveArrow"], not show)
	Control_SetVisible(gHandles["btnSaveAs"], show)
end

function btnSave_OnButtonClicked(buttonHandle)
	-- If it is a shop item in the inventory, perform save as
	if m_smartObj.glid and m_smartObj.placementId==nil then
		MenuOpen("SaveSmartObject.xml")
	else
		if m_smartObj.jsonPath==nil then
			-- Generate and save new script file to LocalDev
			ActionItemFunctions.saveLua(m_smartObj.scriptPath, m_smartObj.scriptPath)
			-- next stop: saveResultHandler
		else
			local savedPath = ActionItemFunctions.saveJson(m_smartObj.name, m_smartObj.scriptGlid or m_smartObj.scriptName, m_smartObj.placementId~=nil)
			if savedPath~="" then
				validateAndPublish()
			else
				KEP_MessageBox( "Error saving smart object" ) -- [" .. tostring(m_smartObj.name) .. "]", "Error" )
			end
		end
	end
end

function btnSaveArrow_OnButtonClicked(buttonHandle)
	showSaveAs(true)
end

function btnSaveAs_OnButtonClicked(buttonHandle)

	MenuOpen("SaveSmartObject.xml")

	local event = KEP_EventCreate("SetupSaveEvent")
	KEP_EventEncodeString(event, "SmartObject")
	KEP_EventEncodeString(event, m_smartObj.name)		-- Use script base name instead of display name
	KEP_EventQueue(event)
	-- next stop: SmartObjectSaveNameHandler

	showSaveAs(false)
end

function Dialog_OnLButtonUp(dialogHandle, x, y)
	if Control_GetVisible(gHandles["btnSaveAs"]) == 1 and Control_ContainsPoint(gHandles["btnSaveAs"], x, y == 0) then
		showSaveAs(false)
	end
end

-----------------------------------------------------------------------
-- Misc Functions
-----------------------------------------------------------------------

function Dialog_OnMouseMove(dialogHandle, x, y)
	-- Highlight/unhighlight the Description on hover
	local edDesc = gHandles["edDescription"]
	if Control_GetFocus(edDesc) ~= 1 then
		if Control_ContainsPoint(edDesc, x, y) == 1 then
			showEditBox("Description", true)
		elseif Control_GetEnabled(edDesc) == 1 then
			showEditBox("Description", false)
		end
	end

	-- Highlight/unhighlight the Name on hover
	local edName = gHandles["edName"]
	if Control_GetFocus(edName) ~= 1 then
		if Control_ContainsPoint(edName, x, y) == 1 then
			showEditBox("Name", true)
		elseif Control_GetEnabled(edName) == 1 then
			showEditBox("Name", false)
		end
	end
end

function showEditBox(name, show)
	Control_SetEnabled(gHandles["ed" .. name], show)
	Control_SetVisible(gHandles["btnEdit" .. name], show)
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	-- selecting description box. It is usually disabled to have to do here.
	local edDesc = gHandles["edDescription"]
	if Control_ContainsPoint(edDesc, x, y) == 1 then
		setEditBox(edDesc)
	end

	-- selecting description box. It is usually disabled to have to do here.
	local edName = gHandles["edName"]
	if Control_ContainsPoint(edName, x, y) == 1 then
		setEditBox(edName)
	end
end

function edDescription_OnFocusOut( edHandle )
	changeItemInfo("description", edHandle)
	Control_SetVisible(gHandles["btnEditDescription"], false)
end

function edName_OnFocusOut( edHandle )
	changeItemInfo("name", edHandle)
	Control_SetVisible(gHandles["btnEditName"], false)
end

function changeItemInfo(infoType, handle)
	local text = EditBox_GetText(handle)
	if text and text ~="" then
		AID[infoType] = text
	end

	Control_SetEnabled(handle, false)
	EditBox_SetText(handle, AID[infoType], false)
end

function setEditBox(handle)
	Control_SetEnabled(handle, true)
	Control_SetFocus(handle)
end

-----------------------------------------------------------------------
-- Scroll Bar Syncing
-----------------------------------------------------------------------
function bringParamFront()
	local dh = MenuHandle("ParamEditor.xml")
	if MenuHandleOk(dh) then
		Dialog_BringToFront(dh)
	end
end

function lstEnabled_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if g_bSyncAllLists == 0 then
		toggleEnabled(sel_index, listBoxHandle)
		syncAllLists( LIST_HANDLES, listBoxHandle )
	end
	bringParamFront()
end

function lstProperties_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if g_bSyncAllLists == 0 then syncAllLists( LIST_HANDLES, listBoxHandle ) end
	bringParamFront()
end

function lstEdit_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if g_bSyncAllLists == 0 then
		syncAllLists( LIST_HANDLES, listBoxHandle )
		editParam(sel_index)
	end
	bringParamFront()
end

function lstEnabled_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	if g_bSyncAllLists == 0 then syncAllScrollBars( LIST_HANDLES, listBoxHandle ) end
	bringParamFront()
end

function lstProperties_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	if g_bSyncAllLists == 0 then syncAllScrollBars( LIST_HANDLES, listBoxHandle ) end
	bringParamFront()
end

function lstEdit_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	if g_bSyncAllLists == 0 then
		updateDivLines()
		syncAllScrollBars( LIST_HANDLES, listBoxHandle )
	end
	bringParamFront()
end

function btnClose2_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
