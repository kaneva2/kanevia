--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Backpack.lua
-- 
-- Handles player's backpack items
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_DragDropHelper.lua")
dofile("Framework_ArmorHelper.lua")
dofile("ContextualHelper.lua")

local ITEMS_PER_PAGE = 25

local DURABILITY_BAR_WIDTH = 82

local TOOLTIP_TABLE = {header = "name"}


g_displayTable = {}
local m_playerInventoryByUNID = {}
local m_playerInventory = {}
local m_playerArmory = {}
local m_gameItems = {} -- added for the game item cache, keys are UNID as a string 
local m_gameItemNamesByUNID = {}
local m_page = 1
local m_tooltipsEnabled = true
local m_selectedIndex = nil
local m_pendingToolbarIndex = nil

local decrementItem -- () Decrement an item from the designated slot
local activateItem -- () Activate an item
local consumableItemActivated -- () Called when a consumable is activated
local getDurabilityColor -- (durabilityPercentage) Gets the durability color by percentage

local getArmorSlot -- (item) Return the slot that an armor item fits into

local m_craftingProximity = false

local m_isDragging = false


-- table of backpack item index's that should glow
local glowingItems = {}


-- When the menu is created
function onCreate()
	KEP_EventRegister("UNIFIED_ENABLE_TOOLTIPS", HIGH_PRIO)
	
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateArmoryClient", "UPDATE_ARMORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateArmoryClientFull", "UPDATE_ARMORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateVendorItems", "FRAMEWORK_RETURN_VENDOR_ITEMS", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateVendorClosed", "FRAMEWORK_REMOVE_VENDOR_ITEMS", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("startDragDrop", "DRAG_DROP_START", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("enableTooltipsHandler", "UNIFIED_ENABLE_TOOLTIPS", KEP.HIGH_PRIO)

	-- getting the item name by the unid 
	KEP_EventRegisterHandler("onItemNameResponse", "INVENTORY_HANDLER_RETURN_ITEM_NAME", KEP.HIGH_PRIO)

	-- getting drop event for the contextual help 
	KEP_EventRegisterHandler("onCustomDrop", "DRAG_DROP_CUSTOM_DROP", KEP.HIGH_PRIO)

	-- show green highlight for contextual help
	KEP_EventRegisterHandler("openContextualMenuEventHandler", "openContextualMenuEvent", KEP.HIGH_PRIO)

	-- remove green highlight for contextual help 
	KEP_EventRegisterHandler("closeContextualMenuEventHandler", "closeContextualMenuEvent", KEP.HIGH_PRIO)

	-- getting close event for contextual help
	--KEP_EventRegisterHandler("onCloseBackpackContextualHelp", "CLOSE_BACKPACK_CONTEXTUAL_HELP", KEP.HIGH_PRIO)

	-- Events.registerHandler("PLAYER_IN_CRAFTING_PROXIMITY", playerInCraftingProximity)
	-- Events.registerHandler("PLAYER_OUT_CRAFTING_PROXIMITY", playerOutCraftingProximity)

	-- item placed on the ground 
	Events.registerHandler("FRAMEWORK_PLACE_GAME_ITEM_CONFIRMATION", placeGameItemConfirmation)

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()	
	InventoryHelper.initializeInventory("g_displayTable", false, ITEMS_PER_PAGE)
	InventoryHelper.initializePageNumbers()
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")
	
	requestInventory(INVENTORY_PID)
	requestInventory(ARMORY_PID)
	requestInventory(GAME_PID)
	KEP_EventCreateAndQueue("FRAMEWORK_REQUEST_VENDOR_ITEMS")

	local ev = KEP_EventCreate("FRAMEWORK_UNLOCK_TOOLBAR")
	KEP_EventEncodeString(ev, "true")
	KEP_EventQueue(ev)
	
	-- Get Armor Slot function
	getArmorSlot = generateArmorSlotFunction()
	
	DragDrop.initializeDragDrop(ITEMS_PER_PAGE)

	MenuBringToFrontThis()

	for itemNumber=1, 30 do
		if gHandles["stcItemCount" .. itemNumber] and gHandles["imgItemCountBG" .. itemNumber] then
			local quantityLabel = gHandles["stcItemCount" .. itemNumber]
			Control_SetSize(gHandles["imgItemCountBG" .. itemNumber], Control_GetWidth(gHandles["imgItemCountBG" .. itemNumber]) + 5, Control_GetHeight(gHandles["imgItemCountBG" .. itemNumber]))
			Control_SetLocationX(gHandles["imgItemCountBG" .. itemNumber], Control_GetLocationX(gHandles["imgItemCountBG" .. itemNumber]) - 6 )
			Control_SetLocationX(quantityLabel, Control_GetLocationX(quantityLabel) - 4)
			Control_SetVisible(quantityLabel, false)
		end
	end
	
end

function onDestroy()

	hideEmptyBackpackContextualHelp()
	hideRolloverContextualHelp()
	hideDragContextualHelp()
	completeCloseXContextualHelp()

	-- CONTEXTUAL HELP ESC BACKPACK COMPLETE 
	ContextualHelper.completeHelp(ContextualHelp.CLOSE_BACKPACK)
	
	
	InventoryHelper.destroy()
	local ev = KEP_EventCreate("FRAMEWORK_UNLOCK_TOOLBAR")
	KEP_EventEncodeString(ev, "false")
	KEP_EventQueue(ev)
	MenuClose("Framework_ItemContainer.xml")
end

function Dialog_OnMoved(dialogHandle, x, y)
	updateCloseContextualHelp()
	updateRolloverContextualHelp()
	updateToolbeltItemContextualHelp()
	updateEmptyBackpackContextualHelp()
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
	updateCloseContextualHelp()
	updateRolloverContextualHelp()
	updateToolbeltItemContextualHelp()
	updateEmptyBackpackContextualHelp()
end

function inventoryUpdated(page, search, category)
	m_page = page
	DragDrop.setIndexOffset( (m_page-1) * (ITEMS_PER_PAGE-TOOLBAR_SLOTS), TOOLBAR_SLOTS)
	updateInventory()
end

-- Called when a new container inventory is received.  Updates all items and re-displays.
function updateVendorItems(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	g_trades = decompileInventory(tEvent)

	-- go through the items in the trades to see what can get sold 
	for i = 1,#g_trades do

		-- the item UNID is the key to the cache 
		local itemUNID = tostring(g_trades[i].output)

		-- we only want to update the description for trades that are legitimate and exist in the cache 
		if not (tonumber(itemUNID) == 0) and m_gameItems[itemUNID] then 
			m_gameItems[itemUNID].canSell = true
			m_gameItems[itemUNID].sellCost = g_trades[i].cost
			m_gameItems[itemUNID].sellName = g_trades[i].inputProperties.name
		end
	end

	-- update the display table with the vendor items 
	formatDisplayTable()
end

-- remove the green sell arrows when the vendor is closed 
function updateVendorClosed(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	formatDisplayTable()
end 

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	

	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)

	if(updateItem and updateItem.properties ) then
		if(updateItem.properties.glow ~= true or updateItem.properties.name == "EMPTY") then
			updateItem.properties.glow = false
		else
			updateItem.properties.glow = true
		end
	end
	
	local inventory = m_playerInventory
	if inventory[updateIndex] then
		inventory[updateIndex] = updateItem
		m_playerInventoryByUNID[updateItem.UNID] = updateItem.count
		updateInventory(inventory)
	end

	-- CONTEXTUAL HELP EMPTY BACKPACK COMPLETE
	ContextualHelper.completeHelp(ContextualHelp.EMPTY_BACKPACK) 

	-- CONTEXTUAL HELP EMPTY BACKPACK COMPLETE 
	ContextualHelper.completeHelp(ContextualHelp.LOOT_CLICKED) 

	showRolloverContextualHelp()
	--showToolbeltItemContextualHelp()

	if m_pendingToolbarIndex then
		
		local selectEvent = KEP_EventCreate("FRAMEWORK_SELECT_TOOLBELT_INDEX")
		KEP_EventEncodeNumber(selectEvent,m_pendingToolbarIndex)
		KEP_EventQueue(selectEvent)
		m_pendingToolbarIndex = nil
		MenuClose("UnifiedInventory.xml")
		MenuCloseThis()
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)

	local inventory = decompileInventory(tEvent)
	
	updateInventory(inventory)

	-- reopen these contextual events (in order of development)
	showEmptyBackpackContextualHelp()
	showRolloverContextualHelp()
	--showToolbeltItemContextualHelp()
end

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
end

function updateArmoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	local inventory = m_playerArmory
	if inventory[updateIndex] then
		inventory[updateIndex] = updateItem
		updateArmory(inventory)
	end
end

-- getting the item name from the 
function onItemNameResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    local UNID = KEP_EventDecodeNumber(tEvent)
    local itemName = KEP_EventDecodeString(tEvent)
    
    if not m_gameItemNamesByUNID[tostring(UNID)] then
    	m_gameItemNamesByUNID[tostring(UNID)] = tostring(itemName)
    
    	formatDisplayTable()
    end
end

function updateArmoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local inventory = decompileInventory(tEvent)
	updateArmory(inventory)
end

function enableTooltipsHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_tooltipsEnabled = true
	formatDisplayTable()
end

function startDragDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_isDragging = true
end

-- find item dropped for contextual help 
function onCustomDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    local dropIndex = KEP_EventDecodeNumber(tEvent)
    local dropItem = decompileInventoryItem(tEvent)

	if isToolbeltItem(dropItem) and dropIndex < 6 and dropIndex ~= -1 then
    	closeDragContextualHelp()
    end

    m_isDragging = false

    updateToolbeltItemContextualHelp()
    updateRolloverContextualHelp()
end

-- Called when a Game Item placement has been confirmed or denied from load
function placeGameItemConfirmation(event)
	if m_selectedIndex then
		-- If placement successful
		if event.success then
			local ev = KEP_EventCreate("FRAMEWORK_BUILD_PLACE_ITEM")
			--KEP_EventEncodeNumber(ev, m_toolbarInventory[slot].properties.GLID)
			KEP_EventQueue(ev)
		
			-- Remove this item from your inventory
			-- i, i + ((ITEMS_PER_PAGE-TOOLBAR_SLOTS) * (m_page-1))
			decrementItem(m_selectedIndex, m_selectedIndex + ((ITEMS_PER_PAGE-TOOLBAR_SLOTS) * (m_page-1)))
		-- Failed
		else
			-- Open the GameItemLoadWarning menu and pass it a message
			if MenuIsClosed("Framework_GameItemLoadWarning.xml") then
				MenuOpen("Framework_GameItemLoadWarning.xml")
			end
			local ev = KEP_EventCreate("FRAMEWORK_GAME_ITEM_LOAD_MESSAGE")
			KEP_EventEncodeString(ev, GI_LOAD_MESSAGE1)
			KEP_EventEncodeString(ev, GI_LOAD_MESSAGE2)
			KEP_EventQueue(ev)
		end

		m_selectedIndex = nil
		--m_placingItem = false
	end
end

-- show the green highlight for the contextual help 
function openContextualMenuEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local eventParams = KEP_EventDecodeString(tEvent)
	eventParams = Events.decode(eventParams)
	local helpID = eventParams.type

	if helpID == ContextualHelp.TOOLBELT_OVERVIEW then
		--log("--- show the contextual help glow")

		-- show the contextual help glow 
		-- local contextualGlow = gHandles["imgContextualHelp"]
		-- Control_SetEnabled(contextualGlow, true)
		-- Control_SetVisible(contextualGlow, true)

		-- -- position the contextual help glow 
		-- local firstToolbeltItemNumber = findFirstToolbeltItem()
		-- local ftiX = Control_GetLocationX(gHandles["btnItem".. tostring(firstToolbeltItemNumber)])
		-- local ftiY = Control_GetLocationY(gHandles["btnItem".. tostring(firstToolbeltItemNumber)])
		-- local ftiWidth = Control_GetWidth(gHandles["btnItem".. tostring(firstToolbeltItemNumber)])
		-- local ftiHeight = Control_GetHeight(gHandles["btnItem".. tostring(firstToolbeltItemNumber)])
		-- local glowHeight = Control_GetWidth(contextualGlow)
		-- local glowWidth = Control_GetWidth(contextualGlow)
		-- local positionX = ftiX + ((ftiWidth - glowWidth)/ 2)
		-- Control_SetLocationX(contextualGlow, positionX)
		-- local positionY = ftiY + ((ftiHeight - glowHeight)/ 2)
		-- Control_SetLocationY(contextualGlow, positionY)
	end 
end 

-- hide the green highlight for the contextual help 
function closeContextualMenuEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local eventParams = KEP_EventDecodeString(tEvent)
	eventParams = Events.decode(eventParams)
	local helpID = eventParams.type

	if helpID == ContextualHelp.TOOLBELT_OVERVIEW then
		-- hide the contextual help glow
		local contextualGlow = gHandles["imgContextualHelp"]
		Control_SetEnabled(contextualGlow, false)
		Control_SetVisible(contextualGlow, false)
	end 
end

-- When the crafting button is clicked
function btnCrafting_OnButtonClicked(buttonHandle)
	MenuOpen("Framework_Crafting.xml")
end

-- When the armor button is clicked
function btnArmor_OnButtonClicked(buttonHandle)
	toggleArmorMenu()
end

function btnArmor_OnMouseEnter(buttonHandle)
	Control_SetVisible(gHandles["imgNavTrayTopBG"], false)
	Control_SetVisible(gHandles["imgNavTrayTopBGRollOver"], true)
end

function btnArmor_OnMouseLeave(buttonHandle)
	Control_SetVisible(gHandles["imgNavTrayTopBG"], true)
	Control_SetVisible(gHandles["imgNavTrayTopBGRollOver"], false)
end

function isVisibleInventoryButton(index)
	if(index) then return index >5 and index < 26 end
	return false
end


-- override the mouse enter for contextual help 
function DragDrop.buttonOnMouseEnterHandler(btnHandle)
    DragDrop.buttonOnMouseEnter(btnHandle)

    local buttonName = Control_GetName(btnHandle)
    local buttonIndexStr = string.sub(buttonName, 8)
    local buttonIndex = tonumber(buttonIndexStr)
	
	-- extract this out to a function and just notify
	if(isVisibleInventoryButton(buttonIndex)) then	
		item = m_playerInventory[buttonIndex + ((ITEMS_PER_PAGE-TOOLBAR_SLOTS) * (m_page-1))]
		if(item and item.properties) then
			if(item.properties.glow == true) then
				item.properties.glow = false
				setGlowForInventoryItem(false, buttonIndex)
				log("glow off ")
				updateItem(buttonIndex + ((ITEMS_PER_PAGE-TOOLBAR_SLOTS) * (m_page-1)), 0, item)
				updateDirty()
			end
		end
	end

    if isToolbeltItem(m_playerInventory[buttonIndex]) then
    	ContextualHelper.completeHelp(ContextualHelp.OBJECT_ROLLOVER)
    	showToolbeltItemContextualHelp(buttonIndex)
    end 
end

-- override the mouse leave for contextual help 
function DragDrop.buttonOnMouseLeaveHandler(btnHandle)
    DragDrop.buttonOnMouseLeave(btnHandle)

    if not m_isDragging then
    	hideDragContextualHelp()
	end 

    -- CONTEXTUAL HELP DRAG TO TOOLBELT SHOW
    local buttonName = Control_GetName(btnHandle)
    local buttonIndexStr = string.sub(buttonName, 8)
    local buttonIndex = tonumber(buttonIndexStr)

    local firstItem = findFirstItemSlot()
    if buttonIndex == firstItem and firstItem ~= -1 then
    	-- CONTEXTUAL HELP OBJECT ROLLOVER COMPLETE / HIDE
    	ContextualHelper.completeHelp(ContextualHelp.OBJECT_ROLLOVER)

		--showToolbeltItemContextualHelp()
		--log("--------- showCloseXContextualHelp 3")
		--showCloseXContextualHelp()
	elseif firstItem == -1 then
		updateRolloverContextualHelp()
		updateToolbeltItemContextualHelp()
		--showCloseXContextualHelp()
    end 
end

function showCloseXContextualHelp()
	if not vendorsAreClosed() then
		return
	end 

	-- CONTEXTUAL HELP 
	ContextualHelper.showHelp(ContextualHelp.CLOSE_BACKPACK_X,
		MenuGetLocationThis(),
		Control_GetLocationX(gHandles["imgItem1"]),
		Control_GetLocationY(gHandles["imgItem1"]),
		Control_GetWidth(gHandles["imgItem1"]),
		Control_GetHeight(gHandles["imgItem1"]),
		ContextualHelpFormat.RIGHT_NO_ARROW)
end

function updateCloseXContextualHelp()
	if not vendorsAreClosed() then
		return
	end 

	-- CONTEXTUAL HELP 
	ContextualHelper.updateHelp(ContextualHelp.CLOSE_BACKPACK_X,
		MenuGetLocationThis(),
		Control_GetLocationX(gHandles["imgItem1"]),
		Control_GetLocationY(gHandles["imgItem1"]),
		Control_GetWidth(gHandles["imgItem1"]),
		Control_GetHeight(gHandles["imgItem1"]),
		ContextualHelpFormat.RIGHT_NO_ARROW)
end

function completeCloseXContextualHelp()
	-- CONTEXTUAL HELP 
	ContextualHelper.completeHelp(ContextualHelp.CLOSE_BACKPACK_X)
end

function showRolloverContextualHelp()
	local firstItem = findFirstItemSlot()
	if firstItem > 0 and vendorsAreClosed() then
		-- CONTEXTUAL HELP OBJECT ROLLOVER SHOW
		ContextualHelper.showHelp(ContextualHelp.OBJECT_ROLLOVER,
			MenuGetLocationThis(),
			Control_GetLocationX(gHandles["imgItem".. tostring(firstItem)]),
			Control_GetLocationY(gHandles["imgItem".. tostring(firstItem)]),
			Control_GetWidth(gHandles["imgItem".. tostring(firstItem)]),
			Control_GetHeight(gHandles["imgItem".. tostring(firstItem)]),
			ContextualHelpFormat.RIGHT_ARROW)
	end 
end

function updateRolloverContextualHelp()
	local firstItem = findFirstItemSlot()
	if firstItem > 0 and vendorsAreClosed() then
		-- CONTEXTUAL HELP OBJECT ROLLOVER SHOW
		ContextualHelper.updateHelp(ContextualHelp.OBJECT_ROLLOVER,
			MenuGetLocationThis(),
			Control_GetLocationX(gHandles["imgItem".. tostring(firstItem)]),
			Control_GetLocationY(gHandles["imgItem".. tostring(firstItem)]),
			Control_GetWidth(gHandles["imgItem".. tostring(firstItem)]),
			Control_GetHeight(gHandles["imgItem".. tostring(firstItem)]),
			ContextualHelpFormat.RIGHT_ARROW)
	else 
		hideRolloverContextualHelp()
	end 
end

function hideRolloverContextualHelp()
	-- CONTEXTUAL HELP OBJECT ROLLOVER HIDE
	ContextualHelper.hideHelp(ContextualHelp.OBJECT_ROLLOVER)
end

-- helper function for showing the contextual help for a toolbelt item 
function showToolbeltItemContextualHelp(itemNumber)
	log("--- itemNumber ".. tostring(itemNumber))
	-- find an object that is "toolbelt able"
	local firstToolbeltSlot = getFirstFreeSlotOnToolbar()
	local firstToolbeltItemNumber = itemNumber or findFirstToolbeltItem()

	if firstToolbeltSlot and firstToolbeltItemNumber > 0 and vendorsAreClosed() then
		-- CONTEXTUAL HELP 
		ContextualHelper.forceCompleteHelp(ContextualHelp.CLOSE_BACKPACK_X)

		-- CONTEXTUAL HELP OBJECT ROLLOVER SHOW
		ContextualHelper.showHelp(ContextualHelp.TOOLBELT_OVERVIEW,
			MenuGetLocationThis(),
			Control_GetLocationX(gHandles["imgItem".. tostring(firstToolbeltSlot)]),
			Control_GetLocationY(gHandles["imgItem".. tostring(firstToolbeltSlot)]),
			Control_GetWidth(gHandles["imgItem".. tostring(firstToolbeltSlot)]),
			Control_GetHeight(gHandles["imgItem".. tostring(firstToolbeltSlot)]),
			ContextualHelpFormat.RIGHT_ARROW)
	end 
end

function updateToolbeltItemContextualHelp()
	-- find an object that is "toolbelt able"
	local firstToolbeltSlot = getFirstFreeSlotOnToolbar()
	local firstToolbeltItemNumber = findFirstToolbeltItem()

	if firstToolbeltSlot and firstToolbeltItemNumber > 0 and vendorsAreClosed() then
		-- CONTEXTUAL HELP OBJECT ROLLOVER SHOW
		ContextualHelper.updateHelp(ContextualHelp.TOOLBELT_OVERVIEW,
			MenuGetLocationThis(),
			Control_GetLocationX(gHandles["imgItem".. tostring(firstToolbeltSlot)]),
			Control_GetLocationY(gHandles["imgItem".. tostring(firstToolbeltSlot)]),
			Control_GetWidth(gHandles["imgItem".. tostring(firstToolbeltSlot)]),
			Control_GetHeight(gHandles["imgItem".. tostring(firstToolbeltSlot)]),
			ContextualHelpFormat.RIGHT_ARROW)
	else
		hideDragContextualHelp()
	end 
end

-- find the first item that is usable in the toolbelt for the contextual help 
function findFirstToolbeltItem()
	for i = 6, #m_playerInventory do
		if isToolbeltItem(m_playerInventory[i]) then
			return i
		end 
	end 

	return -1
end

function hideDragContextualHelp()
	-- CONTEXTUAL HELP TOOLBELT OVERVIEW HIDE
	ContextualHelper.hideHelp(ContextualHelp.TOOLBELT_OVERVIEW)
end

-- complete the drag contextual help and show the close 
function closeDragContextualHelp()
	-- CONTEXTUAL HELP DRAG TOOLBELT COMPLETE
	ContextualHelper.completeHelp(ContextualHelp.TOOLBELT_OVERVIEW)

	showCloseContextualHelp()
end

function hideCloseContextualHelp()
	-- CONTEXTUAL HELP CLOSE BACKPACK HIDE
	ContextualHelper.hideHelp(ContextualHelp.CLOSE_BACKPACK)
end

function updateCloseContextualHelp()
	local firstItem = findFirstItemSlot()

	if firstItem > 0  and vendorsAreClosed() then 
		-- CONTEXTUAL HELP OBJECT ROLLOVER SHOW
		ContextualHelper.updateHelp(ContextualHelp.CLOSE_BACKPACK,
			MenuGetLocationThis(),
			Control_GetLocationX(gHandles["imgItem1"]),
			Control_GetLocationY(gHandles["imgItem1"]),
			Control_GetWidth(gHandles["imgItem1"]),
			Control_GetHeight(gHandles["imgItem1"]),
			ContextualHelpFormat.RIGHT_NO_ARROW)
	end 
end

function findFirstItemSlot()
	for i = 6, ITEMS_PER_PAGE do
		if m_playerInventory[i].UNID ~= 0 then return i end 
	end 

	return -1 
end

function showCloseContextualHelp()
	if not vendorsAreClosed() then
		return
	end

	-- CONTEXTUAL HELP OBJECT ROLLOVER SHOW
	ContextualHelper.showHelp(ContextualHelp.CLOSE_BACKPACK,
			MenuGetLocationThis(),
			Control_GetLocationX(gHandles["imgItem1"]),
			Control_GetLocationY(gHandles["imgItem1"]),
			Control_GetWidth(gHandles["imgItem1"]),
			Control_GetHeight(gHandles["imgItem1"]),
			ContextualHelpFormat.RIGHT_NO_ARROW)
end

-- show the backpack empty help 
function showEmptyBackpackContextualHelp()
	if isBackpackEmpty()  and vendorsAreClosed() then
		-- CONTEXTUAL HELP EMPTY BACKPACK SHOW
		ContextualHelper.showHelp(ContextualHelp.EMPTY_BACKPACK,
			MenuGetLocationThis(),
			Control_GetLocationX(gHandles["imgItem6"]),
			Control_GetLocationY(gHandles["imgItem6"]),
			Control_GetWidth(gHandles["imgItem6"]),
			Control_GetHeight(gHandles["imgItem6"]),
			ContextualHelpFormat.RIGHT_NO_ARROW)
	end 
end

function updateEmptyBackpackContextualHelp()
	if isBackpackEmpty() and vendorsAreClosed() then
		-- CONTEXTUAL HELP EMPTY BACKPACK SHOW
		ContextualHelper.updateHelp(ContextualHelp.EMPTY_BACKPACK,
			MenuGetLocationThis(),
			Control_GetLocationX(gHandles["imgItem6"]),
			Control_GetLocationY(gHandles["imgItem6"]),
			Control_GetWidth(gHandles["imgItem6"]),
			Control_GetHeight(gHandles["imgItem6"]),
			ContextualHelpFormat.RIGHT_NO_ARROW)
	end 
end

-- helper function for contextual help 
function isBackpackEmpty()
	for i = 1, #m_playerInventory do
		-- player inventory is NOT empty 
		if not (m_playerInventory[i].UNID == 0) then
			return false
		end 
	end 

	-- default to true 
	return true
end

-- helper function for the contextual help 
function vendorsAreClosed()
	if MenuIsOpen("Framework_Vendor.xml") then 
		return false
	elseif MenuIsOpen ("Framework_TimedResource.xml") then
		return false
	elseif MenuIsOpen ("Framework_RandomVendor.xml") then
		return false
	end

	return true
end

function hideEmptyBackpackContextualHelp()
	-- CONTEXTUAL HELP EMPTY BACKPACK HIDE
	ContextualHelper.hideHelp(ContextualHelp.EMPTY_BACKPACK)
end

-- is this item able to be used in the toolbelt?
-- is this item able to be used in the toolbelt?
function isToolbeltItem(item)
	local itemType = item.properties.itemType
	
	-- only show the contextual help on a weapon that has ammo 
	if itemType == ITEM_TYPES.WEAPON then
		local ammoType = item.properties.ammoType
		for i = 1, #m_playerInventory do
			if m_playerInventory[i].UNID == ammoType then
				if m_playerInventory[i].count > 0 then
					return true 
				end 
			end 
		end
		return true
	elseif itemType == ITEM_TYPES.CHARACTER then
		if item.properties.behavior == "pet" then
			return true
		end 
	elseif itemType == ITEM_TYPES.CONSUMABLE or itemType == ITEM_TYPES.PLACEABLE or itemType == ITEM_TYPES.HARVESTABLE or itemType == ITEM_TYPES.TOOL or itemType == ITEM_TYPES.TUTORIAL then
		return true
	end

	return false
end


function setGlowForInventoryItem(isGlowing, index)
	local contextualGlow = gHandles["imgContextualHelp" .. index]
	if(contextualGlow) then
		Control_SetEnabled(contextualGlow, isGlowing)
		Control_SetVisible(contextualGlow, isGlowing)
	else
		-- handle bad input gracefully
		local val = ""
		if(isGlowing) then val = "on" else val = "off" end
		log("ERROR - attempt to turn ".. val .." glow for an inventory slot that does not exist. Slot id = [" .. tostring(index).. "]" )
	end
end

function updateInventory(inventory)
	if inventory then
		-- nil inventory means a page switch
		m_playerInventory = inventory
	else
		-- Clear the page of all glow
		for index = 6, ITEMS_PER_PAGE,1  do
			setGlowForInventoryItem(false, index)
		end
	end
	
	for index=6,ITEMS_PER_PAGE, 1 do
		item = m_playerInventory[index + ((ITEMS_PER_PAGE-TOOLBAR_SLOTS) * (m_page-1))]
		if(item and item.properties and item.properties.glow ) then
			if(item.properties.glow == true) then
				setGlowForInventoryItem(true, index)
			else
				setGlowForInventoryItem(false, index)
			end
		end
	end
	
	formatDisplayTable()
	
	InventoryHelper.setNameplates("name", "hide")
	--InventoryHelper.setIcons("GLID")
	InventoryHelper.setQuantities("count")
end

function updateArmory(armory)
	if armory then
		m_playerArmory = armory
	end
	
	local armorRating = 0
	for i=1,#m_playerArmory and ARMOR_SLOTS do
		local item = m_playerArmory[i]
		if item.instanceData and item.instanceData.levelMultiplier and (item.instanceData.durability > 0) then
			armorRating = armorRating + item.properties.armorRating
		end
	end

	updateArmorBar(armorRating)
end

function updateArmorBar(armorRating)
	local armorBarWidth = 0
	for i=1,#ARMOR_BASE do
		if armorRating >= ARMOR_BASE[i] + ARMOR_RANGE[i] then
			armorBarWidth = armorBarWidth + ARMOR_NOTCH_WIDTH
		else
			armorBarWidth = armorBarWidth + ( armorRating/(ARMOR_BASE[i] + ARMOR_RANGE[i]) * ARMOR_NOTCH_WIDTH )
			break
		end
	end
	
	armorBarWidth = armorBarWidth <= ARMOR_BAR_MAX_WIDTH and armorBarWidth or ARMOR_BAR_MAX_WIDTH
	
	Control_SetSize(gHandles["imgArmorBarFill"] , armorBarWidth, ARMOR_BAR_HEIGHT)
	
	local armorDisplay = formatCommaValue(armorRating)
	Static_SetText(gHandles["stcArmorRating"], tostring(armorDisplay))
end

-- Add comma to separate thousands
function formatCommaValue(value)
  	local formatted = value
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end

function formatDisplayTable()
	g_displayTable = {}

	InventoryHelper.setMaxPages(#m_playerInventory)
	
	
	for i=1,#m_playerInventory do

		m_playerInventoryByUNID[m_playerInventory[i].UNID] = m_playerInventory[i].count

		local index
		if i <= TOOLBAR_SLOTS then
			index = i
		else
			index = i + ((ITEMS_PER_PAGE-TOOLBAR_SLOTS) * (m_page-1))
			--log("--- index = ".. tostring(index))
		end
	
	
		
		if index <= #m_playerInventory then
			g_displayTable[i] = deepCopy(m_playerInventory[index])

			if g_displayTable[i].count <= 0 then
				g_displayTable[i].hide = true				
			elseif tablePopulated(g_displayTable[i].instanceData) or (g_displayTable[i].properties.stackSize and g_displayTable[i].properties.stackSize <= 1) then
				g_displayTable[i].count = 0
			end
		end

		formatTooltips(g_displayTable[i], (i <= 5))		
		
		if i <= ITEMS_PER_PAGE then
			_G["btnItem" .. tostring(i) .. "_OnRButtonUp"] = function(btnHandle, x, y)
				-- Check if index is outside toolbar, account for toolbar slots in page calculation
				if i > TOOLBAR_SLOTS then
					activateItem(i, i + ((ITEMS_PER_PAGE-TOOLBAR_SLOTS) * (m_page-1)))
				else
					activateItem(i, i)
				end
			end

			local itemUNID = tostring(m_playerInventory[index].UNID)

			-- remove the item buy graphic, for now
			Control_SetVisible(gHandles["imgBuy".. i], false)

			applyImage(gHandles["imgItem"..i], g_displayTable[i].properties.GLID)

			local element
			if g_displayTable[i].properties.itemType == ITEM_TYPES.WEAPON or g_displayTable[i].properties.itemType == ITEM_TYPES.ARMOR or g_displayTable[i].properties.itemType == ITEM_TYPES.TOOL or (g_displayTable[i].properties.itemType == ITEM_TYPES.CHARACTER and g_displayTable[i].properties.behavior == "pet") then
				Control_SetVisible(gHandles["imgItemLevel" .. i], true)
				element = Image_GetDisplayElement(gHandles["imgItemLevel" .. i])
				local itemLevel = g_displayTable[i].properties.level
				local rarityOffset = ITEM_LEVEL_RARITY_OFFSET[g_displayTable[i].properties.rarity] or 0
				Element_SetCoords(element, ITEM_LEVEL_COORDS[itemLevel].left + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].top, ITEM_LEVEL_COORDS[itemLevel].right + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].bottom)
				-- Handle durability?
				if g_displayTable[i].instanceData and g_displayTable[i].instanceData.durability then
					-- Does durability remain?
					local durability = g_displayTable[i].instanceData.durability
					local durabilityType = g_displayTable[i].properties.durabilityType or "repairable"
					local durabilityPercentage = 0
					local itemLevel = g_displayTable[i].properties.level
					
					if durabilityType == "limited" then
						durabilityPercentage = math.min((durability/DURABILITY_BY_LEVEL[itemLevel]), 1)
					elseif durabilityType == "indestructible" then
						durabilityPercentage = 1
					else
						durabilityPercentage = math.min((durability/DEFAULT_MAX_DURABILITY), 1)
					end
					
					-- Set bar size
					Control_SetSize(gHandles["imgDurabilityBar" .. i], durabilityPercentage * DURABILITY_BAR_WIDTH, 7)
					
					-- Set bar color
					local durColor = getDurabilityColor(durabilityPercentage)
					Element_SetCoords(Image_GetDisplayElement(gHandles["imgDurabilityBar" .. i]), unpack(durColor))
					
					-- Set bar's visible
					Control_SetVisible(gHandles["imgDurabilityBar" .. i], true)
					Control_SetVisible(gHandles["imgDurabilityBG" .. i], true)
					Control_SetVisible(gHandles["imgDurabilityGradiant" .. i], true)
					
					-- Is the item broken?
					Control_SetVisible(gHandles["imgItemBroken" .. i], durability <= 0)
					Control_SetVisible(gHandles["imgItemOverlay" .. i], durability <= 0)
				end
			else
				Control_SetVisible(gHandles["imgItemLevel" .. i], false)
				
				Control_SetVisible(gHandles["imgDurabilityBar" .. i], false)
				Control_SetVisible(gHandles["imgDurabilityBG" .. i], false)
				Control_SetVisible(gHandles["imgDurabilityGradiant" .. i], false)
				Control_SetVisible(gHandles["imgItemBroken" .. i], false)
				Control_SetVisible(gHandles["imgItemOverlay" .. i], false)
			end

			-- format what items are eligible to be sold back to the vendor 
			if m_gameItems[itemUNID] and canSellBack(itemUNID)   then
				-- make the buy graphic visible 
				Control_SetVisible(gHandles["imgBuy".. i], true)

				-- update position of buy graphic
				local bottomOfItem = Control_GetLocationY(gHandles["imgItem"..i]) + Control_GetHeight(gHandles["imgItem"..i])
				local heightOfName = Control_GetHeight(gHandles["stcItem".. i]) or 0
				local heightOfDurability = Control_GetHeight(gHandles["imgDurabilityBar" .. i]) or 0
				local heightOfBuy = Control_GetHeight(gHandles["imgBuy".. i]) or 0 

				if Control_GetVisible(gHandles["imgDurabilityBar" .. i]) == 1 then
					Control_SetLocationY(gHandles["imgBuy".. i], bottomOfItem - (heightOfName + heightOfDurability + heightOfBuy))
				else
					Control_SetLocationY(gHandles["imgBuy".. i], bottomOfItem - (heightOfName + heightOfBuy))
				end 

			else
				-- remove the item buy graphic, for now
				Control_SetVisible(gHandles["imgBuy".. i], false)
			end 
			
			local rarity = g_displayTable[i].properties.rarity or "Common"
			element = Image_GetDisplayElement(gHandles["imgItemBG" .. i])
			Element_SetCoords(element, ITEM_RARITY_BG[rarity].left, ITEM_RARITY_BG[rarity].top, ITEM_RARITY_BG[rarity].right, ITEM_RARITY_BG[rarity].bottom)
		end
		DragDrop.registerItem(i, INVENTORY_PID, m_playerInventory[i])
		
		if i <= 5 then
			if g_displayTable[i].UNID ==0 then
				Control_SetVisible(gHandles["stcToolbelt"..i], true)
			else
				Control_SetVisible(gHandles["stcToolbelt"..i], false)			
			end
		end
	end
	
	if g_displayTable[1].UNID ==0 and g_displayTable[2].UNID ==0 and g_displayTable[3].UNID ==0 and g_displayTable[4].UNID == 0 and g_displayTable[5].UNID == 0 then
		Control_SetVisible(gHandles["imgToolbarBackground"], true)
		Control_SetVisible(gHandles["stcToolbeltHelp"], true)
	else
		Control_SetVisible(gHandles["imgToolbarBackground"], false)
		Control_SetVisible(gHandles["stcToolbeltHelp"], false)
	end
end

function formatTooltips(item, isEquipped)
	if item then
		local itemUNID = tostring(item.UNID)

		item.tooltip = {}

		if not m_tooltipsEnabled then return item.tooltip end

		item.tooltip["header"] = {label = item.properties.name}

		if item.properties.rarity then
			item.tooltip["header"].color = COLORS_VALUES[item.properties.rarity]
		end

		if item.properties.description then
			item.tooltip["body"] = {label = "\"" .. item.properties.description .. "\""}
		end

		if item.properties.itemType == ITEM_TYPES.WEAPON then

			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}

			-- change the description when the 
			if item.properties.healingWeapon and item.properties.healingWeapon == true then
				item.tooltip["healing"] = {label = tostring(item.properties.damage * item.properties.bonusDamage)}
			else 
				item.tooltip["damage"] = {label = tostring(item.properties.damage * item.properties.bonusDamage)}
			end 

			item.tooltip["range"] = {label = tostring(item.properties.range)}

			if item.properties.ammoType and m_gameItemNamesByUNID[tostring(item.properties.ammoType)] then 
				if m_playerInventoryByUNID[item.properties.ammoType] == nil then
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.ammoType)]), color = {r = 255, g = 0, b = 0, a = 255}}
				else
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.ammoType)])}
				end
			elseif item.properties.ammoType then 
				getItemNameByUNID(item.properties.ammoType)
			end 

			local durability = item.instanceData.durability
			local durabilityType = item.properties.durabilityType or "repairable"
			local itemLevel = item.properties.level
			
			if durabilityType == "limited" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				if durabilityPercentage == 0 and durability > 0 then
					durabilityPercentage = 1
				end
				item.tooltip["durability"] = {label = tostring(durabilityPercentage),
												durability = durabilityPercentage}
			elseif durabilityType == "indestructible" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = "100%",
										  		durability = 100}
			else
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				if durabilityPercentage == 0 and durability > 0 then
					durabilityPercentage = 1
				end
				item.tooltip["durability"] = {label = tostring(durabilityPercentage).."%",
										 		durability = durabilityPercentage}
			end

			-- change the tooltip text if the weapon is equipped 
			if isEquipped then 
				item.tooltip["weapon"] = {label = "To attack - select in main toolbelt and left click a target."}
			else 
				item.tooltip["loot"] = {label = "To equip - drag to your toolbelt.", type = item.properties.itemType}
			end

		elseif item.properties.itemType == ITEM_TYPES.TOOL then
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}

			local durability = item.instanceData.durability
			local durabilityType = item.properties.durabilityType or "repairable"
			local itemLevel = item.properties.level

			if item.properties.paintWeapon then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["uses"] = {label = tostring(durability), 
											durability = durabilityPercentage}
			elseif durabilityType == "limited" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage),
												durability = durabilityPercentage}

			elseif durabilityType == "indestructible" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = "100%",
										  		durability = 100}
			else
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage).."%",
										 		durability = durabilityPercentage}
			end
			
			if isEquipped then
				item.tooltip["loot"] = {label = "To place - select in main toolbelt and left click a target.", type = item.properties.itemType}
			else 
				item.tooltip["loot"] = {label = "To use - drag to your toolbelt.", type = item.properties.itemType}
			end
		elseif item.properties.itemType == ITEM_TYPES.ARMOR then
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}
			item.tooltip["armorslot"] = {label = tostring(item.properties.slotType:gsub("^%l", string.upper))}

			local armorRating
			if item.properties.bonusHealth then
				armorRating = item.properties.armorRating * item.properties.bonusHealth
			else
				armorRating = item.properties.armorRating * 3
			end

			item.tooltip["armorrating"] = {label = tostring(armorRating)}
			
			local durability = item.instanceData.durability
			local durabilityType = item.properties.durabilityType or "repairable"
			local itemLevel = item.properties.level

			if durabilityType == "limited" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage),
												durability = durabilityPercentage}

			elseif durabilityType == "indestructible" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = "100%",
										  		durability = 100}
			else
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage).."%",
										 		durability = durabilityPercentage}
			end

			item.tooltip["loot"] = {label = "Right-click to equip.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.CONSUMABLE then
			if item.properties.consumeType == "energy" then
				item.tooltip["energy"] = {label = tostring(item.properties.consumeValue) .. "%"}--, data = "consumeValueDisplay"}
			else
				item.tooltip["health"] = {label = tostring(item.properties.consumeValue) .. "%"}--, data = "consumeValueDisplay"}
			end
			if isEquipped then
				item.tooltip["loot"] = {label = "To consume - select in main toolbelt and left-click in the world.", type = item.properties.itemType}
			else
				item.tooltip["loot"] = {label = "Right-click to consume.", type = item.properties.itemType}
			end
		elseif item.properties.itemType == ITEM_TYPES.PLACEABLE or  item.properties.itemType == ITEM_TYPES.TUTORIAL then
			if isEquipped then
				item.tooltip["loot"] = {label = "To place - select in main toolbelt and left-click in the world.", type = item.properties.itemType}
			else
				item.tooltip["loot"] = {label = "To use - drag to your toolbelt.", type = item.properties.itemType}
			end
		elseif item.properties.itemType == ITEM_TYPES.GENERIC then
			item.tooltip["loot"] = {label = "Generic loot.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.AMMO then
			item.tooltip["loot"] = {label = "Ammo is used by weapons.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.HARVESTABLE then
			if isEquipped then
				item.tooltip["loot"] = {label = "To place - select in main toolbelt and left-click in the world.", type = item.properties.itemType}
			else 
				item.tooltip["loot"] = {label = "To use - drag to your toolbelt.", type = item.properties.itemType}
			end

			-- get food properties 
			--log("--- food : ".. tostring(item.properties.food))
			if item.properties.food and m_gameItemNamesByUNID[tostring(item.properties.food)] then 
				item.tooltip["requires1"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.food)]).. " to grow"}
			elseif item.properties.food then 
				getItemNameByUNID(item.properties.food)
			end 

			-- get tool from properties 
			--log("--- requiredTool : ".. tostring(item.properties.requiredTool))
			if item.properties.requiredTool and m_gameItemNamesByUNID[tostring(item.properties.requiredTool)] then 
				item.tooltip["requires2"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.requiredTool)]).. " to harvest"}
			elseif item.properties.food then 
				getItemNameByUNID(item.properties.requiredTool)
			end 

			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}
		elseif item.properties.itemType == ITEM_TYPES.BLUEPRINT then
			item.tooltip["loot"] = {label = "Right-click to learn Recipe.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.CHARACTER and item.properties.behavior == "pet" then
			if item.properties.food and m_gameItemNamesByUNID[tostring(item.properties.food)] then 
				if m_playerInventoryByUNID[item.properties.food] == nil then
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.food)]), color = {r = 255, g = 0, b = 0, a = 255}}
				else
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.food)])}
				end
			elseif item.properties.food then 
				getItemNameByUNID(item.properties.food)
			end
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}
			if isEquipped then
				item.tooltip["loot"] = {label = "To place this pet - select it in the main toolbelt and left-click in the world.", type = ITEM_TYPES.PET}
			else
				item.tooltip["loot"] = {label = "To use this pet - drag it to your toolbelt.", type = ITEM_TYPES.PET}
			end
		end

		-- add tooltip information for selling items 
		if m_gameItems[itemUNID] and m_gameItems[itemUNID].canSell and MenuIsOpen("Framework_Vendor.xml") then
			-- hack because sometimes the item count is 0???? 
			local itemCount = item.count
			if item.count == 0 then
				itemCount = 1
			end 
			item.tooltip["sell"] = {canSell = true, label = "To sell this for ".. tostring(math.floor((m_gameItems[itemUNID].sellCost / 2) * itemCount)).. " ".. tostring(m_gameItems[itemUNID].sellName).. ", drag to vendor window."}
		else 
			item.tooltip["sell"] = {canSell = false, label = ""}
		end 
	end
end

function toggleArmorMenu()
	KEP_EventCreateAndQueue("FRAMEWORK_OPEN_ARMOR_MENU")
end

function openArmorMenu()	
	if not MenuIsOpen("Framework_Armor.xml") then
		toggleArmorMenu()
	end
end

-- Decrement an item from the designated slot
decrementItem = function(index, slot)
	if g_displayTable[index] then
		g_displayTable[index].count = g_displayTable[index].count - 1
		if g_displayTable[index].count >= 1 then
			updateItem(slot, 0, g_displayTable[index])
		else
			removeItem(slot, 0)
		end
		updateDirty()
	end
end

-- Activate an item
activateItem = function(index, slot)
	--log("Framework_Backpack - activateItem, index: " .. tostring(index) .. ", slot: " .. tostring(slot))

	local item = getObjectAtSlotIndex(index)
	
	if item then
		local itemType = item.properties.itemType
		
		-- Activate consumable
		if itemType == ITEM_TYPES.CONSUMABLE or itemType == ITEM_TYPES.BLUEPRINT then
			consumableItemActivated(index, slot)
		-- Open gembox
		elseif itemType == ITEM_TYPES.GEMBOX then
			MenuClose("Framework_Gembox.xml")
			MenuOpen("Framework_Gembox.xml")
			Events.sendEvent("FRAMEWORK_REDEEM_GEM_CHEST", {slot = tonumber(slot)})
		-- Move armor to armory
		elseif itemType == ITEM_TYPES.ARMOR then
			placeArmorInArmory(index, slot)
			openArmorMenu()
		-- Move weapon/tool to toolbar
		elseif itemType == ITEM_TYPES.WEAPON or itemType == ITEM_TYPES.TOOL or itemType == ITEM_TYPES.PLACEABLE or itemType == ITEM_TYPES.TUTORIAL then
			if item.properties.behavior == "wheeled_vehicle" then
				placeItemInWorld(index, slot)
			elseif index >= TOOLBAR_SLOTS then
				placeItemInToolbar(index, slot)
				
			end
		elseif itemType == ITEM_TYPES.CHARACTER and item.properties.behavior == "pet" then
			--log("--- we need to set the dog down here!!!!!!!!!!")
			placeItemInWorld(index, slot)
		else
			LogWarn("Framework_Backpack.lua - activateItem() Attempt to activate item not of type Consumable or Placeable")
		end
	end
end


-- Returns the item at index if there is an actual object attached to that slot
function getObjectAtSlotIndex(index)
	local item = g_displayTable[index]
	if item and (item.count > 0 or (item.properties and item.properties.name ~= "EMPTY")) then
		return item
	else
		return nil
	end
end


-- Generate the armor type to slot index mapping
function generateArmorSlotFunction()
	local reverseArmorSlots = {}
	for i, v in ipairs(ARMOR_SLOT_TYPES) do
		reverseArmorSlots[v] = i
	end
	-- This code assumes that the user did pass in an armor item.
	return function(item)
		local slotType = item.properties.slotType
		return reverseArmorSlots[slotType]
	end
end

-- Place the armor item at the given index of the display table into 
-- the appropriate armory slot.
function placeArmorInArmory(index, inventorySlot)
	local item = g_displayTable[index]
	
	-- HACKY-WACKY: It turns out InventoryHandler requires the item count to be non-zero. 
	-- If this is left unchecked, we could end up with interesting artifacts...
	if item.count == 0 then
		item.count = 1
	end
	
	local armorSlot = getArmorSlot(item)
	local itemToReplace = m_playerArmory[armorSlot]
	
	-- Switch the items between the armory and the backpack
	updateItem(armorSlot, ARMORY_PID, item)
	updateItem(inventorySlot, INVENTORY_PID, itemToReplace)
	
	-- Update the inventory
	updateDirty()
end

-- Move the backpack item at the given index into a free slot on the toolbar.
function placeItemInToolbar(index, inventorySlot)
	local emptySlotIndex = getFirstFreeSlotOnToolbar()
	if emptySlotIndex then
		local item = g_displayTable[index]
		
		-- HACKY-WACKY: It turns out InventoryHandler requires the item count to be non-zero. 
		-- If this is left unchecked, the inventory handler ends up corrupting the item resulting in strange artifacts...
		if item.count == 0 then
			item.count = 1
		end
		local emptySlotItem = g_displayTable[emptySlotIndex]
		
		-- Place weapon/tool item in the toolbar
		updateItem(emptySlotIndex, INVENTORY_PID, item)
		-- Place an empty item in the old backpack position, essentially removing what was there.
		updateItem(inventorySlot, INVENTORY_PID, emptySlotItem)
		
		-- Update the inventory
		updateDirty()	

		closeDragContextualHelp()

		if item.properties and (item.properties.itemType == ITEM_TYPES.PLACEABLE or item.properties.itemType == ITEM_TYPES.TUTORIAL) then
			m_pendingToolbarIndex = emptySlotIndex
		end
	end
end

-- Retrieve the first free slot on the toolbar. 
-- Doesn't return anything if there are no free slots.
function getFirstFreeSlotOnToolbar()
	local item = nil
	for i=1, TOOLBAR_SLOTS do
		item = getObjectAtSlotIndex(i)
		if item == nil then
			return i
		end
	end
end

-- Called when a consumable is activated
consumableItemActivated = function(index, slot)
	local eventName
	local itemEffects = {}
	if g_displayTable[index].properties.itemType == ITEM_TYPES.BLUEPRINT then
		eventName = "FRAMEWORK_CONSUME_BLUEPRINT" --why?
	else
		itemEffects.emoteM = g_displayTable[index].properties.emoteM
		itemEffects.emoteF = g_displayTable[index].properties.emoteF
		itemEffects.soundGLID = g_displayTable[index].properties.soundGLID
		itemEffects.effect = g_displayTable[index].properties.effect
		if g_displayTable[index].properties.consumeType == "health" then
			eventName = "FRAMEWORK_CONSUME_HEALTH_ITEM"
		elseif g_displayTable[index].properties.consumeType == "energy" then
			eventName = "FRAMEWORK_CONSUME_ENERGY_ITEM"
		end
	end
	-- Are we sending a valid event?
	if eventName then
		if eventName == "FRAMEWORK_CONSUME_BLUEPRINT" then
			updateItem(0, BLUEPRINT_PID, g_displayTable[index])
			local newItemEvent = KEP_EventCreate("FRAMEWORK_NEW_ITEM_MESSAGE")
			KEP_EventEncodeString(newItemEvent,"Recipe")
			KEP_EventQueue(newItemEvent)
		else
			Events.sendEvent(eventName, {consumeValue = g_displayTable[index].properties.consumeValue, itemEffects = itemEffects})
		end
		decrementItem(index, slot)
	else
		LogWarn("Invalid consumeType "..tostring(g_displayTable[index].properties.consumeType).." encountered when consuming an item!")
	end
end

-- Returns the color for the durability bar based on percentage passed in
getDurabilityColor = function(durabilityPercentage)
	-- Green bar
	if durabilityPercentage >= .5 then
		return DURABILITY_COLORS.GREEN
	-- Return yellow
	elseif durabilityPercentage >= .15 then
		return DURABILITY_COLORS.YELLOW
	-- Return red
	else
		return DURABILITY_COLORS.RED
	end
end

-- Called when a pet is activated
placeItemInWorld = function(index, slot)
	if m_selectedIndex then return end
	m_selectedIndex = index

	-- Ask server to place this game item at your feet
	-- Your World Position
	local x, y, z, r = KEP_GetPlayerPosition()
	local radian = (r * math.pi) / 180
	local rotX = math.sin(radian)
	local rotZ = math.cos(radian)
	local placeX = x - (10 * rotX)
	local placeZ = z - (10 * rotZ)
	
	Events.sendEvent("FRAMEWORK_PLAYER_PLACE_ITEM", {GLID = m_playerInventory[slot].properties.GLID,
													 UNID = m_playerInventory[slot].UNID,
													 behavior = m_playerInventory[slot].properties.behavior,
													 x = placeX, y = y, z = placeZ, rx = rotX, ry = 0, rz = rotZ})

end

canSellBack = function(UNID)
  return m_gameItems[UNID].canSell and m_gameItems[UNID].canSell == true and (MenuIsOpen("Framework_Vendor") or MenuIsOpen("Framework_SingleItemVendor"))
end

function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end