--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua" )
dofile("..\\MenuScripts\\DevToolsConstants.lua")
dofile("..\\MenuScripts\\ActionItemFunctions.lua")
dofile("Framework_EventHelper.lua")
dofile("..\\MenuScripts\\Lib_Web.lua")
dofile("Framework_InventoryHelper.lua")


--[[
% NOTES: smart objects with JSON param files - YC 201403
+ Scenarios
  (+) Script is ActionItems\*.lua -> type = ACTION_ITEM												[TESTED OK]
	  [*] m_scriptPending = true, m_paramsPending = false
	  [*] Download script completed -> Duplicate script item into ACTION_INSTANCE_PARAMS (JSON)
	  [*] Open PresetEditor with script + JSON
  (*) Script is ActionItems\*.json -> type = ACTION_ITEM_PARAMS										[TESTED OK]
	  [*] m_scriptPending = true, m_paramsPending = true
	  [*] Download JSON completed -> localJsonPath
	  [*] Download script completed -> duplicate JSON item into ACTION_INSTANCE_PARAMS (JSON)
	  [*] Open PresetEditor with script + JSON
  (*) Script is ActionInstance\*.lua -> type = ACTION_INSTANCE										[TESTED OK]
	  [*] m_scriptPending = true, m_paramsPending = false
	  [*] Download script completed
	  [*] Open PresetEditor with script only
  (*) Script is ActionInstance\*.json -> type = ACTION_INSTANCE_PARAMS								[TESTED OK]
	  [*] m_scriptPending = true, m_paramsPending = true
	  [*] Download JSON completed -> localJsonPath
	  [*] Download script completed
	  [*] Open PresetEditor with script + JSON
]]

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Web			= _G.Web

-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local FIRST_OFFSET_CONTROL = "imgFrame"
local HORIZONTAL_RULE_PREFIX = "imgHorDoubleBottom"

local CONTROL_PADDING = 5
local DIALOG_PADDING = 3


-- Each table is a row with CONTROL_PADDING pixels between. A table within a table are two controls on the same row.
local CONTROL_SETS = {
	PLAY_GAME	= {"btnPlay"},
	OWNER		= { {"btnMove", "btnRotate", "btnPickup","imgCase","imgTrash"} },
	SET_GAME	= {"btnChangeGame"},
	ANIMATION	= {"btnSetAnim", "lblAnimHeading", "lblAnimTitle"},
	MOVIE		= {"btnChangeMedia", "lblMediaHeading", "lblMediaTitle"},
	GAME_ITEM	= {"btnActionItemParams"}, -- GAME ITEMS NOW, NOT SMART OBJECTS/ACTION ITEMS
	SHOW_MORE	= {"btnShowMore"},
	WORLD		= {{"stcPID","lblPid"},{"stcGLID", "lblGlid"},{"stcBuy", "stcShop"}},
	COMMUNITY	= { {"stcBuy", "stcShop"} }
}

-----------------------------------------------------------------
-- Variables
-----------------------------------------------------------------
local m_numElements = 0
local m_showPidGlid = false
local m_scriptPending = false	-- waiting for LUA file
local m_paramsPending = false	-- waiting for JSON parameter file

local m_obj = nil -- Initialized in newObject()

g_AdvBuild = 0 -- Does Edit Properties show if Adv Build is checked

local m_currMoveMode

local m_playerMode = false

local m_gameItems = {}

-----------------------------------------------------------------
-- Local function declarations
-----------------------------------------------------------------
local setMoveMode -- function(mode)



-----------------------------------------------------------------
-- Function definitions
-----------------------------------------------------------------

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "DynamicObjectInfoEventHandler", "DynamicObjectInfoEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ActionItemListResponseHandler", "ScriptClientEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "animUpdatedEvent", "UpdateAnimation", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "flashGameUpdatedEventHandler", "FlashGameUpdatedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ScriptDownloadCompletedHandler", "3DAppAssetDownloadCompleteEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "changeBuildModeEventHandler", CHANGE_BUILD_MODE_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandlerV( "removeDynamicObjectEventHandler", "RemoveDynamicObjectEvent", KEP.HIGH_PRIO )

	KEP_EventRegisterHandler( "UpdateMediaEventHandler", "UpdateMediaEvent", KEP.MED_PRIO )

	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "DynamicObjectChangeEvent", KEP.MED_PRIO )
	KEP_EventRegister( "SelectMediaEvent", KEP.MED_PRIO )
	KEP_EventRegister( "StartFlashGameEvent", KEP.MED_PRIO )
	KEP_EventRegister( "UpdateAnimation", KEP.MED_PRIO )
	KEP_EventRegister( "FlashGameUpdatedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "UpdateBuildToolbarEvent", KEP.MED_PRIO )
	
	KEP_EventRegister( "FRAMEWORK_UNSELECT_ITEM", KEP.HIGH_PRIO )
	KEP_EventRegister( "FRAMEWORK_DYNAMIC_OBJECT_SAVE", KEP.HIGH_PRIO )
	KEP_EventRegister( "FRAMEWORK_DYNAMIC_OBJECT_SELECT", KEP.HIGH_PRIO )
	KEP_EventRegister( "FRAMEWORK_DYNAMIC_OBJECT_DESELECT", KEP.HIGH_PRIO )
	KEP_EventRegister( "FRAMEWORK_GOTO_GAME_ITEM", KEP.HIGH_PRIO )
	

	-- DRF - Added
	KEP_EventRegister( "UpdateMediaEvent", KEP.MED_PRIO )
	
	UGCI.RegisterSubmissionEvents( )
	UGCI.RegisterUploadConfirmationMenuEvent( )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	
	Control_SetVisible(gHandles["lblObjectName"],false)
	Control_SetVisible(gHandles["imgFrame"],false)
	Control_SetVisible(gHandles["imgBackground"],false)
	Control_SetVisible(gHandles["imgObjectPic"],false)
	Control_SetVisible(gHandles["imgBorder"],false)
	
	--Is this in advanced build? 
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		local advBuildOK, advBuildON =  KEP_ConfigGetNumber( config, "AdvBuild", g_AdvBuild)
		g_AdvBuild = advBuildON
	end 
	
	m_obj = newObject()
	resetMenu()

	requestInventory(GAME_PID)
	-- The menu is updated via DynamicObjectInfoEventHandler which is sent
	-- automatically from Selection.lua when a dynamic object is selected.
end

--------------------------------------------------------------------------------------
-- Event Handlers/Related
--------------------------------------------------------------------------------------

function DynamicObjectInfoEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local PID = KEP_EventDecodeNumber(event)
	local type = nil
	if KEP_EventMoreToDecode(event) ~= 0 then
		type = KEP_EventDecodeNumber(event)
	end
	if KEP_EventMoreToDecode(event) ~= 0 then
		m_playerMode = (KEP_EventDecodeString(event) == "true")
	end

	Log("DynamicObjectInfoEventHandler: "..ObjectToString(PID, type))

	-- Same Item Selected ?
	if m_obj.PID == nil or PID == m_obj.PID then return end

	-- Update Object Being Shown In Menu
	if (m_obj.PID ~= -1) then
		Log("DynamicObjectInfoEventHandler: SaveDynamicObjectPlacementChanges("..ObjStr()..")")
		notifyFrameworkOnObjectDeselect(m_obj.PID, false)
		dynamicObjectSavePlacement(m_obj)
	end
	KEP_ReplaceSelection(type, PID)
	notifyFrameworkOnObjectSelect(PID)
	updateProperties(PID, type)
	
	local moveMode = m_currMoveMode or MODE_MOVE
	m_currMoveMode = nil
	if g_AdvBuild ~= 1 or ((g_AdvBuild == 1 ) and MenuIsOpen("DynamicObjectList.xml")) then 
		local ev = KEP_EventCreate( CHANGE_BUILD_MODE_EVENT)
		KEP_EventSetFilter(ev, mode)
		KEP_EventQueue( ev)
	end 
end

function changeBuildModeEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	setMoveMode(filter)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.GET_DYNAMIC_OBJECT_INFO then
		local objectData = KEP_EventDecodeString( event )
		local returnCode = string.match(objectData, "<ReturnCode>(.-)</ReturnCode>")
		if tonumber(returnCode) ~= 0 then
			return
		end

		m_obj.name 				= Web.getDataByTag(objectData, "display_name", 		"(.-)")
		m_obj.creatorName 		= Web.getDataByTag(objectData, "creator", 			"(.-)")
		m_obj.raveCount 		= Web.getDataByTag(objectData, "rave_count", 		"(.-)")
		m_obj.webPrice 			= Web.getDataByTag(objectData, "web_price", 		"(.-)")

		local playerRaveCount 	= Web.getDataByTag(objectData, "player_rave_count",	"(.-)")
		m_obj.playerRaveCount 	= tonumber(playerRaveCount)

		local invType 			= Web.getDataByTag(objectData, "inventory_type", 	"(.-)")
		m_obj.invType 			= tonumber(invType)

		Control_SetVisible(gHandles["lblObjectName"],true)
		Control_SetVisible(gHandles["imgFrame"],true)
		Control_SetVisible(gHandles["imgBackground"],true)
		Control_SetVisible(gHandles["imgObjectPic"],true)
		Control_SetVisible(gHandles["imgBorder"],true)
		displayMenu()
	
	elseif filter == WF.FLASH_GAME then
	    local browserString = KEP_EventDecodeString( event )
        local result = string.match(browserString, "<ReturnCode>([0-9]+)</ReturnCode>")

        if result == "0" then
			m_obj.swfName = string.match(browserString, "<swf_name>(.-)</swf_name>", 1)
			m_obj.swfParams = string.match(browserString, "<swf_parameter>(.-)</swf_parameter>", 1)
			local mediaPath = string.match(browserString, "<media_path>(.-)</media_path>", 1)

			if m_obj.swfName == nil then
				if mediaPath == nil then
					m_obj.swfName = ""
				else
					m_obj.swfName = mediaPath
				end
			end

			if m_obj.swfParams == nil then
				m_obj.swfParams = ""
			end

			Log("BrowserPageReadyHandler: swfName='"..m_obj.swfName.."' swfParams='"..m_obj.swfParams.."'")
         end

         displayMenu()

	elseif filter ==  WF.RAVE then
		local returnString = KEP_EventDecodeString( event )
		local result = string.match(returnString, "<ReturnCode>([0-9]+)</ReturnCode>")

		if result == "0" then
			local numRaves = string.match(returnString, "<NumRaves>(.-)</NumRaves>", 1)
			m_obj.raveCount = tonumber(numRaves)
		end

	elseif filter == WF.OBJECT_BROWSER_INFO then
		local returnString = KEP_EventDecodeString(event)
		m_obj.animName = string.match(returnString, "<display_name>(.-)</display_name>")

		if m_obj.animName == "" then
			Static_SetText(gHandles["lblAnimTitle"], "None")
		else
			m_obj.animName = truncateStaticToControl( m_obj.animName, gHandles["lblAnimTitle"], true)
			Static_SetText(gHandles["lblAnimTitle"], m_obj.animName)
		end

	elseif filter == WF.GET_SCRIPT_BUNDLE then
		local returnString = KEP_EventDecodeString( event )

		if string.find(returnString, "<ReturnCode>0</ReturnCode>") then

			local bundleGlid 		= string.match(returnString, "<global_id>(%d-)</global_id>")
			local bundleInvType 	= string.match(returnString, "<inventory_type>(%d-)</inventory_type>")
			local bundleName	 	= string.match(returnString, "<name>(.-)</name>")
			local bundleWebPrice	= string.match(returnString, "<market_cost>(%d-)</market_cost>")

			m_obj.bundleGlid 		= tonumber( bundleGlid )
			m_obj.bundleInvType 	= tonumber( bundleInvType )
			m_obj.bundleName 		= bundleName
			m_obj.bundleWebPrice 	= tonumber( bundleWebPrice )
		end
	end
end

function ActionItemListResponseHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ScriptClientEventTypes.GetAllScripts and m_obj.actionItem == nil then

		local numRunningScripts = KEP_EventDecodeNumber(event)
		for i=1,numRunningScripts do
			local placementId, scriptName
			placementId = KEP_EventDecodeNumber(event)
			scriptName = KEP_EventDecodeString(event)

			if placementId == m_obj.PID then
				-- Convert forward slashes
				if string.find(scriptName, "ActionItems/") or string.find(scriptName, "ActionInstances/") then
					scriptName = string.gsub(scriptName, "/", "\\")
				end

				-- Make sure it's an action item
				if string.find(scriptName, "ActionItems\\") ~= nil or string.find(scriptName, "ActionInstances\\") ~= nil then
					m_obj.actionItem = {}
					m_obj.scriptName = scriptName

					local assetType, baseName = ActionItemFunctions.getScriptInfoByRelPath( m_obj.scriptName )
					if assetType==DevToolsUpload.UNKNOWN or baseName==nil then
						LogError("ActionItemListResponseHandler: Invalid smart object script name: " .. tostring(m_obj.scriptName))
					else
						m_obj.actionItem.type = assetType
						m_obj.actionItem.editType = assetType
						m_obj.actionItem.name = baseName
						m_obj.actionItem.editName = baseName
						m_obj.actionItem.glid = nil

						if assetType==DevToolsUpload.ACTION_ITEM or assetType==DevToolsUpload.ACTION_ITEM_PARAMS then
							local scriptGlid = string.match(baseName, "^(%d+)$")
							if scriptGlid then
								m_obj.actionItem.glid = tonumber(scriptGlid)
								if m_obj.actionItem.glid==0 then
									LogError("ActionItemListResponseHandler: Error parsing script GLID: " .. tostring(baseName))
									m_obj.actionItem.glid = nil
								end
							end
						end

						if m_obj.actionItem.glid then
							getBundleGlid( m_obj.actionItem.glid )
						end
					end
				end
			end
		end

		displayMenu()

		return KEP.EPR_CONSUMED
	end
end

function animUpdatedEvent(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_obj.animGLID = KEP_EventDecodeNumber( event )
	if not m_obj.animGLID or m_obj.animGLID <= 0 then
		Static_SetText(gHandles["lblAnimTitle"], "None")
	else
		makeWebCall(GameGlobals.WEB_SITE_PREFIX.."kgp/instantBuy.aspx?operation=info&items="..m_obj.animGLID, WF.OBJECT_BROWSER_INFO)
	end
end

function flashGameUpdatedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	getFlashGame()
end

function SetMediaTitle(title)
	title = truncateStaticToControl( title, gHandles["lblMediaTitle"], true)
	Static_SetText(gHandles["lblMediaTitle"], title)
end

function UpdateMediaEventHandler(dispatcher, fromNetid, event, eventid, filter, objId)

    if (filter == 5) then
		objId = KEP_EventDecodeNumber(event)
		if (objId ~= m_obj.PID) then return end
		m_obj.mediaPlaying = KEP_EventDecodeString(event) or "<None>"
		SetMediaTitle(m_obj.mediaPlaying)
	elseif( filter == 2) then
        getFlashGame()
	end
end

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
end

--------------------------------------------------------------------------------------
-- Updating data/menu
--------------------------------------------------------------------------------------
function newObject()
	return { PID = -1,

			-- From KEP_DynamicObjectGetInfo
			name 			= nil,
			description 	= nil,
			canPlayMovie 	= 0,
			isAttachable 	= 0,
			interactive 	= 0,
			isLocal 		= 0,
			derivable 		= 0,
			assetId 		= 0,
			friendId 		= -1,
			customTexture 	= "",
			GLID 			= 0,
			invType 		= 0,
			textureURL 		= "",
			gameItemId		= 0,
			playerId		= 0,
			dynObjType = DYNAMIC_OBJTYPE_INVALID,

			-- true if dynObjType == DYNAMIC_OBJTYPE_MEDIA_FRAME
			isFlashFrame = false,

			-- KEP_CanDynamicObjectSupportAnimation
			isAnimatiable = 0,

			-- KEP_GetDynamicObjectAnimation and BrowserPageReadyHandler WF.OBJECT_BROWSER_INFO
			animGlid = nil,
			animName = "",

			-- UpdateMediaEventHandler
			mediaPlaying = "<None>";

			-- From BrowserPageReadyHandler WF.GET_DYNAMIC_OBJECT_INFO
			raveCount 		= 0,
			playerRaveCount = 0,
			webPrice 		= 0,
			creatorName 	= nil,

			-- From ActionItemListResponseHandler
			actionItem 		= nil,
			scriptName 		= nil,
			bundleGlid 		= nil,
			bundleInvType 	= nil,
			bundleName 		= nil,
			bundleWebPrice 	= nil,

			-- From getFlashGame
			swfName 	= "",
			swfParams 	= ""
		}
end

function resetMenu()
	Static_SetText(gHandles["lblMediaTitle"], "None")
	Static_SetText(gHandles["lblAnimTitle"], "None")

	hideControls(CONTROL_SETS)

	-- Hide horizonal rules
	for i = 1, m_numElements do
		Control_SetVisible(gHandles[HORIZONTAL_RULE_PREFIX .. i], false)
	end
	m_numElements = 0

	-- Reset height of  menu
	local startingControl = gHandles[FIRST_OFFSET_CONTROL]
	local dialogHeight = Control_GetLocationY(startingControl) + Control_GetHeight(startingControl) + CONTROL_PADDING + DIALOG_PADDING
	MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), dialogHeight + Dialog_GetCaptionHeight(gDialogHandle))
end

function hideControls(varName)
	if type(varName) == "string" then
		Control_SetVisible(gHandles[varName], false)
	elseif type(varName) == "table" then
		for i, v in pairs(varName) do
			hideControls(v)
		end
	end
end

function ObjStr()
	return ObjectToString(m_obj.PID, m_obj.objectType)
end

function updateProperties(objPID, objectType)

	-- Replace Current Object
	m_obj = nil
	m_obj = newObject()
	m_obj.PID  = objPID
	m_obj.scriptName = nil
	m_obj.objectType = objectType
	m_obj.name, m_obj.description, m_obj.canPlayMovie, m_obj.isAttachable, m_obj.interactive, m_obj.isLocal, m_obj.derivable, m_obj.assetId, m_obj.friendId, m_obj.customTexture, m_obj.GLID, m_obj.invType, m_obj.textureURL, m_obj.gameItemId, m_obj.playerId, m_obj.dynObjType = KEP_DynamicObjectGetInfo(m_obj.PID)
	m_obj.isAnimatiable = KEP_CanDynamicObjectSupportAnimation(objPID)
	Log("updateProperties: "..ObjStr())

	if m_obj.isAnimatiable == 1 then
		m_obj.animGlid, m_obj.animName = KEP_GetDynamicObjectAnimation( objPID )
		if m_obj.animGlid and m_obj.animGlid > 0 then
			makeWebCall(GameGlobals.WEB_SITE_PREFIX.."kgp/instantBuy.aspx?operation=info&items="..m_obj.animGlid, WF.OBJECT_BROWSER_INFO)
		end
	end

	m_obj.isFlashFrame = (m_obj.dynObjType == DYNAMIC_OBJTYPE_MEDIA_FRAME)

	makeWebCall(GameGlobals.WEB_SITE_PREFIX..DYNAMIC_OBJECT_INFO_SUFFIX.."&globalId="..m_obj.GLID, WF.GET_DYNAMIC_OBJECT_INFO)

	-- ask server for list of objects with scripts
	ActionItemFunctions.requestServerActionItemList()

	if ( m_obj.interactive == 1 ) then
        getFlashGame()
    elseif ( m_obj.canPlayMovie == 1 ) then
		Log("updateProperties: ->UpdateMediaEvent(filter=1 objId="..m_obj.PID..")")
		local evt = KEP_EventCreate("UpdateMediaEvent" )
		KEP_EventSetFilter( evt, 1 )
		KEP_EventEncodeString( evt,  m_obj.PID )
		KEP_EventQueue( evt )
    end

	displayMenu() --also called in all the event handlers as they come in
end

function displayMenu()
	resetMenu()
	-- Load basic info at top

	if m_obj.GLID > 0 then
		setObjectName()
		showImage()
	else
		displayPaperDoll()
	end

	local startingControl = gHandles[FIRST_OFFSET_CONTROL]
	local offset = Control_GetLocationY(startingControl) + Control_GetHeight(startingControl) + CONTROL_PADDING

	-- Add "Play Game" button
	local swfOk = (m_obj.interactive == 1) and (m_obj.swfName ~= "")
	if (swfOk == true) then
		offset = addControlSet("PLAY_GAME", offset, true)
	end

	offset = addControlSet("OWNER", offset)
	if m_playerMode then 
		Control_SetVisible(gHandles["imgCase"], false)
		Control_SetVisible(gHandles["imgTrash"], true)
	else
		Control_SetVisible(gHandles["imgCase"], true)
		Control_SetVisible(gHandles["imgTrash"], false)	
	end 
	
	-- Add set movie button
	if m_obj.interactive == 1 then
		offset = addControlSet("SET_GAME", offset)
	end

	-- Add animation button
	if m_obj.isAnimatiable == 1 and (m_obj.gameItemId == 0 or m_obj.gameItemId == nil) then
		offset = addControlSet("ANIMATION", offset)
		if m_obj.animGlid and m_obj.animGlid > 0 then
			Static_SetText(gHandles["lblAnimTitle"], m_obj.animName)
		end
	end

	-- Add Set movie button
	if m_obj.interactive ~= 1 and m_obj.canPlayMovie == 1 then
		offset = addControlSet("MOVIE", offset)
		SetMediaTitle(m_obj.mediaPlaying)
	end

	if not m_playerMode and m_obj.gameItemId and m_obj.gameItemId ~= 0 then
		offset = addControlSet("GAME_ITEM", offset)
	end

	-- Add PID/GLID buttons (only for non-game items, unless DevMode)
	if g_AdvBuild == 1 and (DevMode() > 0 or m_obj.gameItemId == nil or m_obj.gameItemId == 0) then 
		if m_showPidGlid then
			setPidGlid()
			offset = addControlSet("WORLD", offset)
		else
			offset = addControlSet("SHOW_MORE", offset)
		end
	end 
end

function setObjectName()
	m_obj.name = truncateStaticToControl(m_obj.name, gHandles["lblObjectName"], true)
	Static_SetText(gHandles["lblObjectName"], m_obj.name)
end

function showImage()
	local eh = Image_GetDisplayElement(gHandles["imgObjectPic"])
	KEP_LoadIconTextureByID(m_obj.GLID, eh, gDialogHandle)
end

function displayPaperDoll()

	local paperDoll = m_gameItems[tostring(m_obj.gameItemId)]

	if paperDoll and paperDoll.GLID then
		Control_SetVisible(gHandles["lblObjectName"],true)
		Control_SetVisible(gHandles["imgFrame"],true)
		Control_SetVisible(gHandles["imgBackground"],true)
		Control_SetVisible(gHandles["imgObjectPic"],true)
		Control_SetVisible(gHandles["imgBorder"],true)

		
		Static_SetText(gHandles["lblObjectName"], "Custom Avatar")
		local eh = Image_GetDisplayElement(gHandles["imgObjectPic"])
		KEP_LoadIconTextureByID(paperDoll.GLID, eh, gDialogHandle)
	end
end

function addControlSet(setName, offset)
	local controlSet = CONTROL_SETS[setName]
	local controlHeight = 0

	-- Set the next horizontal rule
	m_numElements = m_numElements + 1
	controlHeight = setControlLocation(HORIZONTAL_RULE_PREFIX .. m_numElements, offset)
	offset = offset + controlHeight + CONTROL_PADDING

	-- Set the controls in the set visble and to the correct location
	for i, controlBlock in ipairs(controlSet) do
		-- If it's a table, that means they should be on the same Y heigh so set without updating offset
		if type(controlBlock) == "table" then
			for j, control in ipairs(controlBlock) do
				controlHeight = setControlLocation(control, offset)
			end
		else
			controlHeight = setControlLocation(controlBlock, offset)
		end
		offset = offset + controlHeight + CONTROL_PADDING
	end

	-- Set the height of the menu
	MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), offset + DIALOG_PADDING + Dialog_GetCaptionHeight(gDialogHandle))

	--return the offset for future use
	return offset
end

function setControlLocation(controlName, location)
	local control = gHandles[controlName]
	Control_SetLocationY(control, location)
	Control_SetVisible(control, true)
	return Control_GetHeight(control)
end

function getFlashGame()
	local ok, mUrl, mParams, mVolPct, mRadiusAudio, mRadiusVideo = KEP_GetMediaParamsOnDynamicObject(m_obj.PID)
	if ok then
		m_obj.swfName = mUrl
		m_obj.swfParams = mParams
		makeWebCall(GameGlobals.WEB_SITE_PREFIX.."kgp/dynamicObjectInfo.aspx?type=g&placementId="..m_obj.PID, WF.FLASH_GAME, 1)
	end
end

function setPidGlid()
	Static_SetText(gHandles["lblPid"], tostring(m_obj.PID))
	Static_SetText(gHandles["lblGlid"], tostring(m_obj.GLID))
end

function getBundleGlid( scriptGlid )
	makeWebCall(GameGlobals.WEB_SITE_PREFIX.."kgp/instantBuy.aspx?gzip=true&operation=info".."&getScriptBundle=true&items="..scriptGlid, WF.GET_SCRIPT_BUNDLE)
end

--------------------------------------------------------------------------------------
-- Buttons
--------------------------------------------------------------------------------------

function btnMove_OnButtonClicked( buttonHandle )
	setMoveMode(MODE_MOVE)
	enterBuildMode(MODE_MOVE)
end

function btnRotate_OnButtonClicked(buttonHandle)
	Log("RotateClicked")
	setMoveMode(MODE_ROTATE)
	enterBuildMode(MODE_ROTATE)
end 

function btnPickup_OnButtonClicked( buttonHandle )
	KEP_EventCreateAndQueue("PickUpSelectedObjectsEvent")

	-- Tell DynamicObjectList Menu To Refresh
	KEP_EventCreateAndQueue("DynamicObjectChangeEvent")
end

function btnPlay_OnButtonClicked( buttonHandle )

	-- Get Flash Game Attached To Object
	local swfOk = (m_obj.interactive == 1) and (m_obj.swfName ~= "")
	if (swfOk == false) then
		return
	end

    -- Open Flash Game Menu & Play
    MenuOpen("FlashGame.xml")
	local ev = KEP_EventCreate( "StartFlashGameEvent" )
	KEP_EventEncodeString( ev, m_obj.swfName )
	KEP_EventEncodeString( ev, m_obj.swfParams )
	KEP_EventQueue( ev )
end

function btnChangeGame_OnButtonClicked( buttonHandle )
	MenuClose("PlaylistSelect.xml")
	MenuOpen("PlaylistSelect.xml")
	local menuOpenEvent = KEP_EventCreate( "SelectMediaEvent" )
	KEP_EventEncodeString( menuOpenEvent, PLAYLIST_TYPE_GAME)
	KEP_EventEncodeNumber( menuOpenEvent, m_obj.PID )
	KEP_EventEncodeNumber( menuOpenEvent, 0 )
	KEP_EventEncodeNumber( menuOpenEvent, "" )
	KEP_EventQueue( menuOpenEvent )
end

function btnSetAnim_OnButtonClicked( buttonHandle )
    MenuOpen("ChooseAnimation.xml")
	local ev = KEP_EventCreate( "SelectedItemInfoEvent" )
	KEP_EventEncodeNumber( ev, m_obj.invType )
	KEP_EventEncodeNumber( ev, 1 ) -- num items
	KEP_EventEncodeNumber( ev, m_obj.GLID )
	KEP_EventEncodeNumber( ev, m_obj.PID )
	KEP_EventEncodeNumber( ev, 1 ) --quantity of item
	KEP_EventEncodeString( ev, m_obj.name ) -- name
	KEP_EventEncodeNumber( ev, m_obj.webPrice ) -- price
	KEP_EventQueue( ev )
end

function btnChangeMedia_OnButtonClicked( buttonHandle )
	MenuClose("PlaylistSelect.xml")

	-- if it's a flash frame, set to that type otherwise it's media
	local playlistType = m_obj.isFlashFrame and PLAYLIST_TYPE_FRAME or PLAYLIST_TYPE_TV

	MenuOpen("PlaylistSelect.xml")

	--fire event to pass placement id to the menu script
	local menuOpenEvent = KEP_EventCreate( "SelectMediaEvent" )
	KEP_EventEncodeString( menuOpenEvent, playlistType)
	KEP_EventEncodeNumber( menuOpenEvent, m_obj.PID )
	KEP_EventEncodeNumber( menuOpenEvent, 0 )
	KEP_EventEncodeNumber( menuOpenEvent, "" )
	KEP_EventQueue( menuOpenEvent )
end

function btnActionItemParams_OnButtonClicked( buttonHandle )
	Framework.sendEvent("FRAMEWORK_GOTO_ITEM", {UNID = tonumber(m_obj.gameItemId), PID = tonumber(m_obj.PID)})
end

function ScriptDownloadCompletedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local path = KEP_EventDecodeString(event)
	local assetType = KEP_EventDecodeNumber(event)

	if not m_scriptPending or not m_obj.actionItem or
	   (assetType~=DevToolsUpload.ACTION_ITEM and assetType~=DevToolsUpload.ACTION_INSTANCE and
		assetType~=DevToolsUpload.ACTION_ITEM_PARAMS and assetType~=DevToolsUpload.ACTION_INSTANCE_PARAMS)
	then
		-- Triggered by other menu - ignore
		return KEP.EPR_OK
	end

	if m_paramsPending then
		-- Pending JSON download
		-- Possible asset types: ACTION_ITEM_PARAMS, ACTION_INSTANCE_PARAMS
		if assetType~=m_obj.actionItem.type then
			-- type mismatched
			LogError( "ScriptDownloadCompletedHandler: asset type received is " .. tostring(assetType) .. ", expected " .. tostring(m_obj.actionItem.type) )
			return KEP.EPR_OK
		end

		m_obj.localJsonPath = path

		-- Branch if necessary
		if m_obj.actionItem.type==DevToolsUpload.ACTION_ITEM_PARAMS then
			local newName = tostring(m_obj.PID)
			m_obj.localJsonPath = ActionItemFunctions.duplicate(newName, path, DevToolsUpload.ACTION_INSTANCE_PARAMS, m_obj.actionItem.type, m_obj.actionItem.glid or m_obj.actionItem.name)
			m_obj.actionItem.editType = DevToolsUpload.ACTION_INSTANCE_PARAMS
			m_obj.actionItem.editName = newName
		end

		m_paramsPending = false

		local jsonTable = ActionItemFunctions.parseJson( m_obj.actionItem.editType, m_obj.localJsonPath )
		if jsonTable and jsonTable.script then
			local scriptName = jsonTable.script
			if type(jsonTable.script)=="number" then
				scriptName = ActionItemFunctions.formatShopItemName(jsonTable.script)
			end
			KEP_RequestSourceAssetFromAppServer(DevToolsUpload.ACTION_ITEM, scriptName)
			return
			-- next stop: ScriptDownloadCompletedHandler / ACTION_ITEM
		else
			LogError( "ScriptDownloadCompletedHandler: error reading JSON file [" .. tostring(m_obj.localJsonPath) .. "]" )
			m_obj.localJsonPath = nil
			m_scriptPending = false
		end

	elseif m_scriptPending then
		-- Pending LUA download
		-- Possible asset types: ACTION_ITEM, ACTION_INSTANCE
		if assetType~=DevToolsUpload.ACTION_ITEM and assetType~=DevToolsUpload.ACTION_INSTANCE then
			-- type mismatched
			LogError( "ScriptDownloadCompletedHandler: asset type received is " .. tostring(assetType) .. " while waiting for a script" )
			return KEP.EPR_OK
		end

		-- Branch if necessary
		if m_obj.actionItem.type==DevToolsUpload.ACTION_ITEM then
			local newName = tostring(m_obj.PID)
			m_obj.localJsonPath = ActionItemFunctions.duplicate(newName, path, DevToolsUpload.ACTION_INSTANCE_PARAMS, m_obj.actionItem.type, m_obj.actionItem.glid or m_obj.actionItem.name)
			m_obj.actionItem.editType = DevToolsUpload.ACTION_INSTANCE_PARAMS
			m_obj.actionItem.editName = newName
		end

		local localJsonPath = m_obj.localJsonPath
		m_obj.localJsonPath = nil
		m_scriptPending = false

		-- Start editing
		ActionItemFunctions.OpenPreset(m_obj.actionItem.editName, m_obj.actionItem.editType, path, localJsonPath, m_obj.PID)
	end

	MenuCloseThis()
end

-- Called when an object is removed from the world
function removeDynamicObjectEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- If the PID removed was the one we're editing, close the menu
	if objectid == m_obj.PID then
		Log("removeDynamicObjectEventHandler: Removed Object Is This - Closing Menu...")
		m_obj.PID = nil
		KEP_TurnOffWidget()
		MenuCloseThis()
	end
end

function btnShowMore_OnButtonClicked()
	m_showPidGlid = true
	displayMenu()
end

function stcBuy_OnLButtonUp( stcHandle )
	Log("BuyItemClicked")
	MenuOpen("Checkout.xml")
	local ev = KEP_EventCreate( "ItemInfoEvent" )
	KEP_EventEncodeNumber( ev, m_obj.bundleInvType or m_obj.invType )
	KEP_EventEncodeNumber( ev, 1 ) -- num items
	KEP_EventEncodeNumber( ev, m_obj.bundleGlid or m_obj.GLID )
	KEP_EventEncodeNumber( ev, 1 ) --quantity of item
	KEP_EventEncodeString( ev, m_obj.bundleName or m_obj.name ) -- name
	KEP_EventEncodeNumber( ev, m_obj.bundleWebPrice or m_obj.webPrice ) -- price
	KEP_EventQueue( ev )
end

function stcShop_OnLButtonUp( stcHandle )
	Log("ViewItemInShopClicked")
	local shopGlid = m_obj.bundleGlid or m_obj.GLID
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_SHOP .. IMAGE_DETAILS_SUFFIX .. shopGlid )
end

function enterBuildMode(mode)
	local ev = KEP_EventCreate( "EnterBuildModeEvent")
	KEP_EventSetFilter(ev, 1)
	KEP_EventEncodeNumber(ev, mode)
	KEP_EventQueue(ev)
end

--------------------------------------------------------------------------------------
-- Closing Functions
--------------------------------------------------------------------------------------
function Dialog_OnDestroy(dialogHandle)
	KEP_EventQueue(KEP_EventCreate("UpdateBuildToolbarEvent"))
	Helper_Dialog_OnDestroy( dialogHandle )
end

-- Sets movement mode
setMoveMode = function(mode)
	-- Was a unique mode passed in?
	if m_currMoveMode ~= mode then

		-- If we're using advanced build, let Selection.lua handle this
		if g_AdvBuild == 1 then
			local ev = KEP_EventCreate( CHANGE_BUILD_MODE_EVENT)
			KEP_EventSetFilter(ev, mode)
			KEP_EventQueue( ev)
		else
			-- If not in advanced build, we have to do this manually
			-- Cleanup old mode
			-- Disable translate widget
			if m_currMoveMode == MODE_MOVE then
				KEP_TurnOffWidget()
			-- Disable rotate widget
			elseif m_currMoveMode == MODE_ROTATE then
				KEP_TurnOffWidget()
			end
		

			-- Select current ghost
			Log("SelectObject("..tostring(m_obj.objectType)..", "..tostring(m_obj.PID)..")")
			KEP_SelectObject(m_obj.objectType, m_obj.PID)
		
			-- Set translate mode
			if mode == MODE_MOVE then
				KEP_DisplayTranslationWidget(m_obj.objectType)
		
			-- Set rotate mode
			elseif mode == MODE_ROTATE then
				local rotX, rotY, rotZ = KEP_DynamicObjectGetRotation(m_obj.PID)
				KEP_SetWidgetRotation(0, rotY - g_PI, 0)
				KEP_DisplayRotationWidget(m_obj.objectType)
			end
		end
	end

	m_currMoveMode = mode
end

-----------------------------------------------------------------------------
-- Dynamic Object Save Helper
-- PJD - Determines context and lets the Framework know something has moved
-----------------------------------------------------------------------------
function dynamicObjectSavePlacement(objectInfo)
	
	-- If this object is not a game item, then save it off as normal
	if objectInfo.gameItemId == nil or objectInfo.gameItemId == 0 then
		KEP_SaveDynamicObjectPlacementChanges(objectInfo.PID)
	end
	
	-- Get the object position and rotation on client to send to the framework
	local objPos = {}
	local radian = 0
	objPos.x, objPos.y, objPos.z, radian = KEP_DynamicObjectGetPositionAnim(objectInfo.PID)
	objPos.rx = math.cos(radian)
	objPos.ry = 0
	objPos.rz = -math.sin(radian)
	
	-- Dynamic objects with game item IDs will be saved by the framework
	local saveEvent = KEP_EventCreate("FRAMEWORK_DYNAMIC_OBJECT_SAVE")
	KEP_EventEncodeNumber(saveEvent, objectInfo.PID)
	KEP_EventEncodeNumber(saveEvent, objectInfo.gameItemId)
	KEP_EventEncodeNumber(saveEvent, objPos.x)
	KEP_EventEncodeNumber(saveEvent, objPos.y)
	KEP_EventEncodeNumber(saveEvent, objPos.z)
	KEP_EventEncodeNumber(saveEvent, objPos.rx)
	KEP_EventEncodeNumber(saveEvent, objPos.ry)
	KEP_EventEncodeNumber(saveEvent, objPos.rz)	
	KEP_EventQueue(saveEvent)
end

function notifyFrameworkOnObjectSelect(objectPID)
	if objectPID then
		local notifyEvent = KEP_EventCreate("FRAMEWORK_DYNAMIC_OBJECT_SELECT")
		KEP_EventEncodeNumber(notifyEvent, objectPID)
		KEP_EventQueue(notifyEvent)
	end
end

function notifyFrameworkOnObjectDeselect(objectPID, onDestroy)
	if objectPID then
		local notifyEvent = KEP_EventCreate("FRAMEWORK_DYNAMIC_OBJECT_DESELECT")
		KEP_EventEncodeNumber(notifyEvent, objectPID)
		KEP_EventEncodeString(notifyEvent, tostring(onDestroy))
		KEP_EventQueue(notifyEvent)
	end
end
