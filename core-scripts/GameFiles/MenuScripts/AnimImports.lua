--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")

g_ugcType = nil
g_ugcObjId = nil
g_animIdSubmitted = nil

ADD_ANIM_IMPORT_EVENT = "AddAnimImportEvent"
SHOW_IMPORT_LISTS_EVENT = "ShowImportListEvent"
UGC_SUBMISSION_FEEDBACK_EVENT = "UGCSubmissionFeedbackEvent"

AnimList = {}
animCount = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "AddAnimImportEventHandler", ADD_ANIM_IMPORT_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "ShowImportListEventHandler", SHOW_IMPORT_LISTS_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UGCSubmissionFeedbackEventHandler", UGC_SUBMISSION_FEEDBACK_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( ADD_ANIM_IMPORT_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( SHOW_IMPORT_LISTS_EVENT, KEP.MED_PRIO )
	UGCI.RegisterSubmissionEvents( )
	UGCI.RegisterUploadConfirmationMenuEvent( )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- setup look of listbox scrollbars
	local lbh = Dialog_GetListBox(dialogHandle, "lbAnims")
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then
			local eh = ScrollBar_GetTrackDisplayElement(sbh)

			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end

	-- hide menu by default
	Dialog_SetMinimized( gDialogHandle, 1 )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function AddAnimImportEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local animGLID = KEP_EventDecodeNumber( event )
	local animIndex = animCount
	AnimList[animIndex] = {}
	AnimList[animIndex].glid = animGLID
	AnimList[animIndex].name = KEP_EventDecodeString( event )
	AnimList[animIndex].genderId = KEP_EventDecodeNumber( event )
	AnimList[animIndex].placementId = KEP_EventDecodeNumber( event )
	addListBoxItem( "lbAnims", AnimList[animCount].name, animIndex )
	animCount = animCount + 1

	SubmitAnimation( animIndex )
end

function ShowImportListEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	if filter==1 then
		-- show menu
		Dialog_SetMinimized( gDialogHandle, 0 )
	else
		-- hide menu
		Dialog_SetMinimized( gDialogHandle, 1 )
	end

end

function ChangeAnimation( animGLID )
	KEP_SetCurrentAnimationByGLID(animGLID)
end

function btnDelete_OnButtonClicked( buttonHandle )
	local sel, text, animIndex = getListBoxSelectedItem( "lbAnims" )
	if sel~=nil then
		AnimList[animIndex] = {}
		removeListBoxItem( "lbAnims", sel )
	end
end

function btnUpload_OnButtonClicked( buttonHandle )
	local sel, text, animIndex = getListBoxSelectedItem( "lbAnims" )
	if sel~=nil then
		if AnimList[animIndex].glid~=nil then
			SubmitAnimation( animIndex )
		else
			LogError( "UploadClicked: Submit animation error - record not found for [" .. text .. "]" )
		end
	end
end

function lbAnims_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index, sel_text)
	enableControl( "btnUpload", sel_index~=-1 )
	enableControl( "btnDelete", sel_index~=-1 )
	if sel_index~=-1 then
		
		--KEP_SendChatMessage( ChatType.System, "Play animation [" .. sel_text .. "]" )
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, "Play animation [" .. sel_text .. "]")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
		
		local animIndex = ListBox_GetItemData( listBoxHandle, sel_index )
		ChangeAnimation( AnimList[animIndex].glid )
	end
end

function UGCSubmissionFeedbackEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter==g_ugcType or objectid==g_ugcObjId then

		local sfe = UGCI.DecodeSubmissionFeedbackEvent( event, filter, objectid )

		if sfe.httpStatusCode==200 and sfe.result==SubmissionResult.SUCCEEDED then

			if sfe.uploadId~=nil then
				Log( "UGCSubmissionFeedbackEventHandler: OK - uploadId=" .. sfe.uploadId )

				-- display upload dialog directly (skip pricing)
				UGCI.ShowUGCUploadDlg( sfe.uploadId, sfe.isDerivative, sfe.ugcType, sfe.ugcObjId, sfe.objName, sfe.param1, sfe.param2, 0, 0 )
			end
		end

		g_ugcType = nil	-- reset pending ID
		g_ugcObjId = nil

		return KEP.EPR_CONSUMED
	end

	return KEP.EPR_OK
end

function SubmitAnimation( animIndex )
	local animGLID = AnimList[animIndex].glid
	g_ugcType, g_ugcObjId, g_animIdSubmitted = UGC_TYPE.CHARANIM, animGLID, animIndex
	UGCI.SubmitUGCObj( UGC_TYPE.CHARANIM, animGLID, 0, AnimList[animIndex].name, "", AnimList[animIndex].genderId, AnimList[animIndex].placementId )
end
