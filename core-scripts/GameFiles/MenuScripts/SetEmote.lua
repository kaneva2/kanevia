--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("MenuLocation.lua")
dofile("CAM.lua")

g_pageindex=0
g_animationindex=-1
g_animname=""

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "AddEmoteToListHandler", "EmoteAddEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "MyCustomEvents",KEP.MED_PRIO )
end

--Save the animation name and animation index pass from FullEmote.xm
function AddEmoteToListHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_pageindex = KEP_EventDecodeNumber(event)
	g_animationindex = KEP_EventDecodeNumber(event)
	g_animname = KEP_EventDecodeString(event)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnAddEmote_OnButtonClicked( buttonHandle )
	addEmoteToFavorites()
end

function bntIndexBox_OnEditBoxString(editBoxHandle, strTxt)
	addEmoteToFavorites()
end

function addEmoteToFavorites()
	local bh = Dialog_GetEditBox(gDialogHandle,"bntIndexBox")
	local txt = EditBox_GetText(bh)
	if txt~="" then
		local n = tonumber(txt)
		if(n>=0 and n<=9) then
			local event = KEP_EventCreate("MyCustomEvents")
			KEP_EventEncodeNumber(event,n)
			KEP_EventEncodeNumber(event,g_animationindex)
			KEP_EventEncodeString(event,g_animname)
			KEP_EventQueue( event )
			MenuCloseThis()
		end
	end
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	--Make sure we have the interface we need (stop errors in editor)
	if Control_ContainsPoint ~= nil then
		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")
		if sh ~= nil then
			if Control_ContainsPoint(Dialog_GetButton(gDialogHandle, "btnEmoteClose"), x, y) ~= 0 then
				Static_SetText(sh, "Close This Menu")
			else
				Static_SetText(sh, " ")
			end
		end
	end
end
