--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- NewItemMenu.lua - Kaneva Menu Script Template
-- 
-- Template script for creating a new Kaneva client menu.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("LibClient_Common.lua")

local m_itemTypes = {}
m_itemTypes[1] = "weapon"
m_itemTypes[2] = "armor"
m_itemTypes[3] = "consumable"
m_itemTypes[4] = "placeable"
m_itemTypes[5] = "generic"

local m_newItem = {}

function onCreate()
	for data,itemType in pairs(m_itemTypes) do
		ListBox_AddItem( gHandles["lstType"], string.upper(itemType), data )
	end
end

function edLevel_OnEditBoxChange(editBoxHandle, editBoxText)
	if tonumber(editBoxText) then
		Slider_SetValue( gHandles["sldrLevel"], tonumber(editBoxText) )
	end
end

function sldrLevel_OnSliderValueChanged(sliderHandle, sliderValue)
	if tonumber(sliderValue) then
		EditBox_SetText( gHandles["edLevel"], tostring(sliderValue), false )
	end
end

function btnCreate_OnButtonClicked(buttonHandle)
	addNewItem()
end

function addNewItem()
	if tonumber(EditBox_GetText(gHandles["edGLID"])) and tonumber(EditBox_GetText(gHandles["edLevel"])) then
		m_newItem.name = EditBox_GetText(gHandles["edName"])
		m_newItem.GLID = tonumber(EditBox_GetText(gHandles["edGLID"]))
		m_newItem.level = Slider_GetValue(gHandles["sldrLevel"])
		local itemValue = ListBox_GetSelectedItemData(gHandles["lstType"])
		m_newItem.itemType = m_itemTypes[itemValue]
		Events.sendEvent("CREATE_INVENTORY_ITEM", {properties = m_newItem})
		MenuCloseThis()
	end
end
