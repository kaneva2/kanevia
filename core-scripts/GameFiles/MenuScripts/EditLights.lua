--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

EDIT_LIGHT_MENU_CREATION_EVENT = "EditLightMenuCreationEvent"
LIGHT_MODIFY_COMPLETE_EVENT = "ModifyLightsCompleteEvent"
COLOR_PICKER_EVENT = "ColorSelectedEvent"
COLOR_START_EVENT = "ColorStartEvent"

LIGHT_PICKER_COLOR_FILTER = 2
DXUT_STATE_NORMAL = 0

gLights = {}
g_lightIndex = nil

gHandles = {}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "menuCreationHandler", EDIT_LIGHT_MENU_CREATION_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "colorPickerEventHandler", COLOR_PICKER_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( EDIT_LIGHT_MENU_CREATION_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( COLOR_PICKER_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( LIGHT_MODIFY_COMPLETE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( COLOR_START_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	scrollBarShenanigans()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function menuCreationHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	gLights = {}
	local count = KEP_EventDecodeNumber(event)
	for i=1,count do
		local oneLight = {}
		oneLight["type"] = KEP_EventDecodeNumber(event)
		oneLight["positionX"] = KEP_EventDecodeNumber(event)
		oneLight["positionY"] = KEP_EventDecodeNumber(event)
		oneLight["positionZ"] = KEP_EventDecodeNumber(event)
		oneLight["diffuseR"] = KEP_EventDecodeNumber(event)
		oneLight["diffuseG"] = KEP_EventDecodeNumber(event)
		oneLight["diffuseB"] = KEP_EventDecodeNumber(event)
		oneLight["range"] = KEP_EventDecodeNumber(event)
		oneLight["attenuation1"] = KEP_EventDecodeNumber(event)
		oneLight["attenuation2"] = KEP_EventDecodeNumber(event)
		oneLight["attenuation3"] = KEP_EventDecodeNumber(event)
		table.insert(gLights, oneLight)
	end
	populateListBox()
	Control_SetVisible(gHandles["imgCover"], true)
	Control_SetEnabled(gHandles["imgCover"], true)
end

function colorPickerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == LIGHT_PICKER_COLOR_FILTER then
		if g_lightIndex ~= nil then
			if gLights[g_lightIndex] ~= nil then
				local tempTrash = KEP_EventDecodeNumber(event)
				gLights[g_lightIndex]["diffuseR"] = KEP_EventDecodeNumber(event)
				gLights[g_lightIndex]["diffuseG"] = KEP_EventDecodeNumber(event)
				gLights[g_lightIndex]["diffuseB"] = KEP_EventDecodeNumber(event)
				setDiffuseButtonColor(gLights[g_lightIndex]["diffuseR"], gLights[g_lightIndex]["diffuseG"], gLights[g_lightIndex]["diffuseB"])
				populateListBox()
			end
		end
	end
end

function setDiffuseButtonColor(red, green, blue)
	local diffuseColor	= {}
	diffuseColor.a = 255
	diffuseColor.r = red
	diffuseColor.g = green
	diffuseColor.b = blue
	local btnCtrl = gHandles["imgDiffuse"]
	local btnElm = Image_GetDisplayElement( Control_GetImage( btnCtrl ) )
	local textureColor = Element_GetTextureColor(btnElm)
	BlendColor_SetColor(textureColor, DXUT_STATE_NORMAL, diffuseColor)
end

function btnDiffuse_OnButtonClicked(buttonHandle)
	if g_lightIndex ~= nil then
		if gLights[g_lightIndex] ~= nil then
			local oneLight = gLights[g_lightIndex]
			gColorPicker = MenuOpenModal("ColorPicker.xml")

			--Pass up the color data to the colorpicker
			local ev = KEP_EventCreate( COLOR_START_EVENT)
			KEP_EventSetFilter(ev, LIGHT_PICKER_COLOR_FILTER)
			KEP_EventEncodeNumber(ev, 255)
			KEP_EventEncodeNumber(ev, oneLight["diffuseR"])
			KEP_EventEncodeNumber(ev, oneLight["diffuseG"])
			KEP_EventEncodeNumber(ev, oneLight["diffuseB"])
			KEP_EventEncodeNumber(ev, TEXTURE_COLOR_TARGET)
			KEP_EventQueue( ev)
		end
	end
end

function btnSave_OnButtonClicked(buttonHandle)
	local ev = KEP_EventCreate( LIGHT_MODIFY_COMPLETE_EVENT)
	local lightCount = #gLights
	KEP_EventEncodeNumber(ev, lightCount)
	for i=1,lightCount do
		local oneLight = gLights[i]
		KEP_EventEncodeNumber(ev, oneLight["type"])
		KEP_EventEncodeNumber(ev, oneLight["positionX"])
		KEP_EventEncodeNumber(ev, oneLight["positionY"])
		KEP_EventEncodeNumber(ev, oneLight["positionZ"])
		KEP_EventEncodeNumber(ev, oneLight["diffuseR"])
		KEP_EventEncodeNumber(ev, oneLight["diffuseG"])
		KEP_EventEncodeNumber(ev, oneLight["diffuseB"])
		KEP_EventEncodeNumber(ev, oneLight["range"])
		KEP_EventEncodeNumber(ev, oneLight["attenuation1"])
		KEP_EventEncodeNumber(ev, oneLight["attenuation2"])
		KEP_EventEncodeNumber(ev, oneLight["attenuation3"])
	end
	KEP_EventQueue( ev)

	MenuCloseThis()
end

function btnAddLight_OnButtonClicked(buttonHandle)
	local newLight = {}
	newLight["type"] = 0
	newLight["positionX"] = 0
	newLight["positionY"] = 0
	newLight["positionZ"] = 0
	newLight["diffuseR"] = 255
	newLight["diffuseG"] = 255
	newLight["diffuseB"] = 255
	newLight["range"] = 100
	newLight["attenuation1"] = 1
	newLight["attenuation2"] = 1
	newLight["attenuation3"] = 1
	table.insert(gLights, newLight)
	populateListBox()
end

function btnDeleteLight_OnButtonClicked(buttonHandle)
end

function lstLights_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index, sel_text)
	local listIndex = sel_index + 1
	if gLights[listIndex] ~= nil then
		g_lightIndex = listIndex
		Control_SetVisible(gHandles["imgCover"], false)
		Control_SetEnabled(gHandles["imgCover"], false)
		updateDisplayOfLights()
	end
end

function edPosX_OnEditBoxChange(editboxHandle, editboxText)
	if g_lightIndex ~= nil then
		if gLights[g_lightIndex] ~= nil then
			local val = tonumber(EditBox_GetText(gHandles["edPosX"]))
			if val ~= nil then
				gLights[g_lightIndex]["positionX"] = val
			end
		end
	end
end

function edPosY_OnEditBoxChange(editboxHandle, editboxText)
	if g_lightIndex ~= nil then
		if gLights[g_lightIndex] ~= nil then
			local val = tonumber(EditBox_GetText(gHandles["edPosY"]))
			if val ~= nil then
				gLights[g_lightIndex]["positionY"] = val
			end
		end
	end
end

function edPosZ_OnEditBoxChange(editboxHandle, editboxText)
	if g_lightIndex ~= nil then
		if gLights[g_lightIndex] ~= nil then
			local val = tonumber(EditBox_GetText(gHandles["edPosZ"]))
			if val ~= nil then
				gLights[g_lightIndex]["positionZ"] = val
			end
		end
	end
end

function edRange_OnEditBoxChange(editboxHandle, editboxText)
	if g_lightIndex ~= nil then
		if gLights[g_lightIndex] ~= nil then
			local val = tonumber(EditBox_GetText(gHandles["edRange"]))
			if val ~= nil then
				gLights[g_lightIndex]["range"] = val
			end
		end
	end
end

function edAtt1_OnEditBoxChange(editboxHandle, editboxText)
	if g_lightIndex ~= nil then
		if gLights[g_lightIndex] ~= nil then
			local val = tonumber(EditBox_GetText(gHandles["edAtt1"]))
			if val ~= nil then
				gLights[g_lightIndex]["attenuation1"] = val
			end
		end
	end
end

function edAtt2_OnEditBoxChange(editboxHandle, editboxText)
	if g_lightIndex ~= nil then
		if gLights[g_lightIndex] ~= nil then
			local val = tonumber(EditBox_GetText(gHandles["edAtt2"]))
			if val ~= nil then
				gLights[g_lightIndex]["attenuation2"] = val
			end
		end
	end
end

function edAtt3_OnEditBoxChange(editboxHandle, editboxText)
	if g_lightIndex ~= nil then
		if gLights[g_lightIndex] ~= nil then
			local val = tonumber(EditBox_GetText(gHandles["edAtt3"]))
			if val ~= nil then
				gLights[g_lightIndex]["attenuation3"] = val
			end
		end
	end
end

function populateListBox()
	local lightCount = #gLights
	local lh = gHandles["lstLights"]
	ListBox_RemoveAllItems(lh)
	for i=1,lightCount do
		local oneLight = gLights[i]
		local displayName = "" .. i .. ": (R:" .. oneLight["diffuseR"] .. " G:" .. oneLight["diffuseG"] .. " B:" .. oneLight["diffuseB"] .. ")"
		ListBox_AddItem(lh, displayName, i)
	end
end

function scrollBarShenanigans()
	local lh = gHandles["lstLights"]
	if lh then
		ListBox_RemoveAllItems(lh)
	end
	local sbh = ListBox_GetScrollBar(lh)
	if sbh ~= 0 then
		local eh = ScrollBar_GetTrackDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 37, 17, 69)
		end
		eh = ScrollBar_GetButtonDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 19, 17, 35)
		end
		eh = ScrollBar_GetUpArrowDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 1, 17, 17)
		end
		eh = ScrollBar_GetDownArrowDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
			Element_SetCoords(eh, 1, 71, 17, 87)
		end
	end
end

function updateDisplayOfLights()
	if g_lightIndex ~= nil then
		if gLights[g_lightIndex] ~= nil then
			local selLight = gLights[g_lightIndex]
			EditBox_SetText(gHandles["edPosX"], tostring(selLight["positionX"]), false)
			EditBox_SetText(gHandles["edPosY"], tostring(selLight["positionY"]), false)
			EditBox_SetText(gHandles["edPosZ"], tostring(selLight["positionZ"]), false)
			EditBox_SetText(gHandles["edRange"], tostring(selLight["range"]), false)
			EditBox_SetText(gHandles["edAtt1"], tostring(selLight["attenuation1"]), false)
			EditBox_SetText(gHandles["edAtt2"], tostring(selLight["attenuation2"]), false)
			EditBox_SetText(gHandles["edAtt3"], tostring(selLight["attenuation3"]), false)
			setDiffuseButtonColor(selLight["diffuseR"], selLight["diffuseG"], selLight["diffuseB"])
		end
	end
end
