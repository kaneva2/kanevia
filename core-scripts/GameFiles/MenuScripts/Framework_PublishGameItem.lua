--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_PublishGameItem.lua
-- 
-- Helps publish Game Items to a user's Inventory or to the Shop
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- Organizes items by type
local DISPLAY_TYPES = {["ammo"] 		 = "loot",
					   ["armor"] 		 = "loot",
					   ["character"]	 = "character",
					   ["consumable"] 	 = "loot",
					   --["flag"] 		 = TBD,
					   ["generic"] 		 = "loot",
					   --["harvestable"] = TBD,
					   ["placeable"] 	 = "placeables",
					   --["recipe"] 	 = NEVER,
					   --["settings"] 	 = TBD,
					   --["spawner"] 	 = NEVER,
					   --["waypoint"] 	 = TBD,
					   ["weapon"] 		 = "loot"
					  }

----------------------
-- Globals
----------------------
local m_gameItem = {}
local m_newName, m_oldName, m_UNID
local m_pendingWebCheck = false

-- When the menu is created
function onCreate()
	-- Register client event handlers
	KEP_EventRegisterHandler("publishGameItemHandler", "FRAMEWORK_PUBLISH_GAME_ITEM", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.HIGH_PRIO)
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Catches relevant data for this menu to display
function publishGameItemHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- Better safe than sorry
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		m_gameItem = JSON.decode(KEP_EventDecodeString(tEvent))
		m_UNID = m_gameItem.GIID
	end
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		m_oldName = KEP_EventDecodeString(tEvent)
	end
	
	-- Iterate over each bundledGlids entry and convert them to numbers
	for i,GLID in pairs(m_gameItem.bundledGlids) do
		m_gameItem.bundledGlids[i] = tonumber(GLID)
	end
	
	-- Set GLID icon
	local eh = Image_GetDisplayElement(gHandles["imgNewItem"])
	KEP_LoadIconTextureByID(m_gameItem.GLID, eh, gDialogHandle)
	
	-- Detect the corresponding tab for the Game Item type passed
	m_gameItem.displayType = DISPLAY_TYPES[m_gameItem.itemType] or m_gameItem.itemType
	-- Capitilize first letter of the system
	local itemSystem = m_gameItem.displayType:gsub("^%l", string.upper)
	
	Static_SetText(gHandles["stcText"], tostring(m_oldName).." will be copied to the "..tostring(itemSystem).." section of your Inventory with the following name:")
	EditBox_SetText(gHandles["edNewName"], m_gameItem.name, false)
end

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- Unique Game Item check return
	if (filter == WF.UNIQUE_GAME_ITEM_CHECK) and m_pendingWebCheck then
		m_pendingWebCheck = false
		local xmlData = KEP_EventDecodeString(tEvent)
		
		local suggestedName = xmlGetString(xmlData, "SuggestedName")
		
		-- If a new suggested name was passed, we had a conflict
		if suggestedName ~= m_newName then
			Dialog_SetCaptionText(gDialogHandle, "     Can't Copy to Inventory")
			Static_SetText(gHandles["stcText"], "An item named "..tostring(m_newName).." already exists in your Inventory.")
			Control_SetVisible(gHandles["stcEnterName"], true)
			EditBox_SetText(gHandles["edNewName"], suggestedName, false)
		-- Carry own my wayward son
		else
			-- Save off Game Item name so we can revert it after we check this new name
			local tempName = m_gameItem.name
			m_gameItem.name = suggestedName
			
			local address = GameGlobals.WEB_SITE_PREFIX..SNAPSHOT_GAME_ITEM_TO_INVENTORY.."["..tostring(JSON.encode(m_gameItem)).."]"
			makeWebCall(address, WF.COPY_GAME_ITEM_TO_INVENTORY, 1, 1)

			-- Revert name field so we don't lose our place
			m_gameItem.name = tempName
		end
	-- Game Item upload return
	elseif filter == WF.COPY_GAME_ITEM_TO_INVENTORY then
		local xmlData = KEP_EventDecodeString(tEvent)
		
		local success = xmlGetInteger(xmlData, "ReturnCode")
		local affectedItemCount = xmlGetInteger(xmlData, "AffectedItemCount")

		if success == 0 and affectedItemCount == 1 then
			m_gameItem.GIGLID = xmlGetInteger(xmlData, "GIGLID")

			-- Make sure to preserve game item's original name before saving the GIGLID
			m_gameItem.name = m_oldName

			-- Edit this Game Item so that it references the new generated GIGLID
			Events.sendEvent("EDIT_GAME_ITEM", {UNID = m_UNID, properties = m_gameItem, preserveGIGLID = true})

			-- Open copy confirmation menu
			MenuOpen("Framework_SaveVerify.xml")
			local event = KEP_EventCreate("FRAMEWORK_SAVE_VERIFY")
			KEP_EventEncodeString(event, JSON.encode(m_gameItem))
			KEP_EventEncodeString(event, "Inventory")
			KEP_EventQueue(event)
			
			MenuCloseThis()
		else
			LogWarn("BrowserPageReadyHandler() Copy to Inventory of Game Item (UNID["..tostring(m_UNID).."] Name["..tostring(m_gameItem.name).."]) failed returnXML["..tostring(xmlData).."]")
		end
	end
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

-- Publish item to shop
function btnUpload_OnButtonClicked(buttonHandle)
	Log("PUBLISH!")
end

-- Copy to Inventory
function btnCopyToInventory_OnButtonClicked(buttonHandle)
	m_newName = EditBox_GetText(gHandles["edNewName"])
	
	-- Check if the user has this Game Item by name and type in their Inventory already
	local address = GameGlobals.WEB_SITE_PREFIX..PLAYER_INVENTORY_CHECK_SUFFIX.."211&countGINameUse=true&search="..tostring(m_newName).."&gameItemTypes="..tostring(m_gameItem.itemType)
	m_pendingWebCheck = true
	makeWebCall(address, WF.UNIQUE_GAME_ITEM_CHECK, 1, 1)
end