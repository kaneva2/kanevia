--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Framework_BuildActionHelper.lua
-- Pseudo-class system for collecting ghost update/movement actions (implemented so as to keep related game logic in one place...)


--------------------------------------
-- BUILD ACTION FUNCTIONALITY
--------------------------------------

local BuildAction = {}

BuildAction.Definitions = {}
BuildAction.Definitions.ObjectOverlap	= 1
BuildAction.Definitions.FlagOverlap		= 2
BuildAction.Definitions.FlagMaxReached  = 3

local DEFAULT_MAX_FLAGS = 3

-- Checks whether placing a new claim flag is allowed
function BuildAction.checkClaimFlagBuilding(self)
	local isBuildingClaimFlag = false
	local ghostFlag = nil
	
	-- First, determine if we're building a claim flag.
	if( self.ghostObject ) then
		local ghostObjectProperties = self.ghostObject.properties
		isBuildingClaimFlag =  self:isPlacingClaimFlag()
		if( isBuildingClaimFlag ) then
			ghostFlag = {			
				radius		= ghostObjectProperties.radius or 0, 
				height		= ghostObjectProperties.height or 0,
				width		= ghostObjectProperties.width or 0,
				depth		= ghostObjectProperties.depth or 0,
				position	= self.location
			}
							
		end
	end

	-- Are we building a claim flag?
	if( isBuildingClaimFlag ) then 
		local flags = self.flagSystem:getClaimFlags()
		for i, flag in pairs(flags) do
			-- CJW, NOTE: For now we're not allowing claim flags to overlap regardless of the owner.
			--local isPlayerOwner = self.placedObjectList[flag.PID].owner == self.playerName
			--if( not isPlayerOwner ) then
			
				-- Do flag boundaries overlap? If so, then don't allow.
				local doBoundariesOverlap = flag:isFlagInBoundary(ghostFlag)
				if( doBoundariesOverlap ) then
					return false, BuildAction.Definitions.FlagOverlap
				end
			--end
		end
		
		-- Check for ghost flags intersecting with already existing objects that are not owned by the local player
		for i, object in pairs(self.placedObjectList) do
			if(object.owner and object.owner ~= self.playerName )then
				local pos = object.position

				local doesObjectOverlapWithFlag = Flag.isPointInBoundary(ghostFlag, pos.x, pos.y, pos.z)
				if( doesObjectOverlapWithFlag ) then
					return false, BuildAction.Definitions.ObjectOverlap
				end
			end
		end

		-- Check if the player has placed their maximum number of allowed claim flags
		local flagCount = self:getPlayerFlagCount()
		local maxFlagCount = self.maxFlags or DEFAULT_MAX_FLAGS -- Might be nil if not configured for older worlds
		if flagCount >= maxFlagCount then
			return false, BuildAction.Definitions.FlagMaxReached
		end
	end
	
	return true
end

-- Returns whether item Move/Place/Remove is permitted by the claim flags in this zone
function BuildAction.checkClaimFlagPermissions(self)
	local flags = self.flagSystem:getClaimFlags()
	local location = self.location
	
	-- Does the position fall inside a claim flag?
	-- If so, does the flag permission allow this?
	for i, flag in pairs(flags) do
		local isLocationWithinFlag = flag:isPointInBoundary(location.x, location.y, location.z)

		-- If within a player mode enabled flag
		if isLocationWithinFlag then
			-- Handle claim flags
			self.claimFlagFound = true
			
			-- If this player is the owner of the flag OR is a member of the claim flag then give him a pass.
			-- Otherwise, do not give him allowance.
			local isPlayerOwner = self.placedObjectList[flag.PID].owner == self.playerName
			local isPlayerMember = flag:isPlayerMember(self.playerName)
			if not (isPlayerOwner or isPlayerMember)  then
				return false
			end
		end
	end
	
	return true
end

-- Returns whether a player is inside a claim flag where they are allowed to perform actions
function BuildAction.checkClaimFlagPermissionsInverse(self)
	local flags = self.flagSystem:getClaimFlags()
	local location = self.location
	for i, flag in pairs(flags) do
		local isLocationWithinFlag = flag:isPointInBoundary(location.x, location.y, location.z)

		-- If within a player mode enabled flag
		if isLocationWithinFlag == true then
			-- Handle claim flags
			self.claimFlagFound = true
			
			-- If this player is the owner of the flag OR is a member of the claim flag then give him a pass.
			-- Otherwise, do not give him allowance.
			local isPlayerOwner = self.placedObjectList[flag.PID].owner == self.playerName
			local isPlayerMember = flag:isPlayerMember(self.playerName)
			if (isPlayerOwner or isPlayerMember)  then
				return true
			end
		end
	end
	return false
end

-- Returns the flag (if any) that encompasses the area where the player is trying to build
function BuildAction.checkForNearestClaimFlag(self)
	local flags = self.flagSystem:getClaimFlags()
	local location = self.location
	
	-- Does the position fall inside a claim flag?
	-- If so, does the flag permission allow this?
	for i, flag in pairs(flags) do
		
		local isLocationWithinFlag = flag:isPointInBoundary(location.x, location.y, location.z)
		-- If within a player mode enabled flag
		if isLocationWithinFlag then
			return flag
		end
	end
	
	return nil
end

-- Returns the number of objects encompasses by a given flag
function BuildAction.getFlagObjectCount(self, flag)
	local count = -1 -- Account for the fact that the flag itself is a placed object within the radius
	if flag and flag.position then
		for i, placedObject in pairs(self.placedObjectList) do
			-- Debug.printTable("", placedObject)
			if flag:isPointInBoundary(placedObject.position.x, placedObject.position.y, placedObject.position.z) then
				count = count + 1
			end
		end
	end
	return count
end

-- Returns whether item Move/Place/Remove is permitted by the Player Build flags in this zone
function BuildAction.checkPlayerBuildFlagPermissions(self) -- 1234
	local flags = self.flagSystem:getBuildFlags()
	local dominantFlag = nil
	local location = self.location

	-- Sift through all player build flags that reside over this location,
	-- and find the dominant one. The dominant flag is chosen to be the 
	-- flag with the smallest radius. If two flags with the same radii exist, then
	-- the ultimate arbiter is the world build setting
	for i, flag in pairs(flags) do
		
		local isLocationWithinFlag = flag:isPointInBoundary(location.x, location.y, location.z)
		-- If within a player mode enabled flag
		if isLocationWithinFlag then
		
			self.playerBuildFlagFound = true
			if( dominantFlag ) then
				-- Dominant flag is the flag with the smaller radius
				if( dominantFlag.radius > flag.radius ) then
					dominantFlag = flag
				elseif( dominantFlag.radius == flag.radius ) then
					-- If the radii are the same, we should refer the matter to the world build setting
					if flag.enabled == self.isBuildingAllowedInWorld then
						dominantFlag = flag
					end
				end
			else
				dominantFlag = flag
			end
		end
	end

	-- If no flag was found, we let the zone setting decide the restriction
	-- If a dominant flag was found, we return that flag's build permission.
	if( not dominantFlag ) then
		return self.isBuildingAllowedInWorld
	else
		return dominantFlag.enabled
	end
end



-- Filter to check whether a media being examined is not owned by the current player
local generateMediaPlayerPredicate = function(placedObjects, playerName)
	return function(media)
		local placedObject = placedObjects[media.PID]
		local isOwnedByAnother = placedObject and placedObject.owner ~= playerName
		return isOwnedByAnother
	end
end

function BuildAction.checkMediaPlayerPlacement(self)
	-- Media players cannot be placed too close to one another, unless 
	-- 1) both the media players are owned by the same player, OR 
	-- 2) the media player being placed is within the bounds of a land claim flag that the current player is a member of...
	
	
	local ghostObjectProperties = self.ghostObject and self.ghostObject.properties
	if ghostObjectProperties and ghostObjectProperties.behavior == "media" then
		-- We're trying to place a media player
		
		-- Conflict-range is not checked if we're within a claim flag.
--		local isPlayerClaimFlagMember = self.flagSystem:checkLandClaimFlagMembershipForPlayer(self.playerName, nil, self.location)
--		if not isPlayerClaimFlagMember then
			-- Get the first media object in conflict-range of the ghost media.
			ghostMediaObject = {	radius = ghostObjectProperties.radius or 0,
									-- height = ghostObjectProperties.height or 0,
									-- width = ghostObjectProperties.width or 0,
									-- depth = ghostObjectProperties.depth or 0,
									position = self.location}
			local isMediaOwnedByAnotherPlayerPredicate = generateMediaPlayerPredicate(self.placedObjectList, self.playerName)
			local media = self.mediaSystem:getMediaInRangeOfMedia(ghostMediaObject, isMediaOwnedByAnotherPlayerPredicate)
			if media then
				return false
			end
--		end
	end
	
	return true
end


function BuildAction.checkMediaPlayerMovement(self)
	-- Media players cannot be moved too close to one another, unless 
	-- 1) both the media players are owned by the same player, OR 
	-- 2) the media player being manipulated is within the bounds of a land claim flag that the current player is a member of...
	
	-- If there is one, get the media object that's being moved
	local currentMediaObject = self.mediaSystem:getMediaWithPID(self.objectPID)
	if currentMediaObject ~= nil then
		-- We're trying to move a media player
		
		-- Is the media player within the bounds of a land claim flag that the current player is a member of?
		local isPlayerClaimFlagMember = self.flagSystem:checkLandClaimFlagMembershipForPlayer(self.playerName, currentMediaObject.PID, currentMediaObject.position)
		if not isPlayerClaimFlagMember then

			-- Get the first media object in conflict-range of the media that we're moving.
			local isMediaOwnedByAnotherPlayerPredicate = generateMediaPlayerPredicate(self.placedObjectList, self.playerName)
			local media = self.mediaSystem:getMediaInRangeOfMedia(currentMediaObject, isMediaOwnedByAnotherPlayerPredicate)
			if media then
				return false
			end
		end
	end
	
	return true
end


function BuildAction.isPlacingVehicle(self)
	return (self.ghostObject ~= nil and 
			self.ghostObject.properties ~= nil and 
			self.ghostObject.properties.behavior == "wheeled_vehicle")
			--or isLastClickedObjectVehicle()
end

function BuildAction.isPlacingClaimFlag(self)
	return (self.ghostObject ~= nil and 
			self.ghostObject.properties ~= nil and 
			self.ghostObject.properties.behavior == "claim")
end


function BuildAction.getPlayerFlagCount(self)
	local flagCount = 0
	local flags = self.flagSystem:getClaimFlags()
	for i, flag in pairs(flags) do
		if flag.playername == self.playerName then
			flagCount = flagCount + 1
		end
	end
	return flagCount
end


function BuildAction.checkActionPermissibility(self)
	-- CJW, TODO: Do we check whether the player can move a player build flag?

	-- Check that we're not trying to move an existing flag
	if self.flagSystem:isPlaceableObjectLandClaimFlag(self.objectPID) then
		return "You cannot move a land claim flag."
	end
	
	-- Check whether we're attempting to build a claim flag
	local canBuild, reason = self:checkClaimFlagBuilding()
	if( not canBuild ) then
		if reason == BuildAction.Definitions.FlagOverlap then
			return "Flag boundaries cannot overlap."
		elseif reason == BuildAction.Definitions.FlagMaxReached then
			return "You have already placed the maximum number of Land Claim Flags allowed."
		else
			return "Object already placed in flag area."
		end
	end

	local isBuildingClaimFlag =  self:isPlacingClaimFlag()
	if self.flagBuildOnly and not isBuildingClaimFlag then
		-- If the player is NOT within a claim flag, but build is restricted to JUST claim flags
		if (not self:checkClaimFlagPermissionsInverse()) then
			return "You cannot build here."
		end
		-- Check whether claim flags permit object placement for the local player
	elseif (not self:checkClaimFlagPermissions()) then
		return "You cannot build here. Land already claimed."
	end
	
	-- Check whether player build flags permit object placement at this location
	if( not self:checkPlayerBuildFlagPermissions() ) then
		local isPlacingVehicle = self:isPlacingVehicle()
		--log("--- isPlacingVehicle ".. tostring(isPlacingVehicle))
		if( not isPlacingVehicle ) then
			return "You cannot build here."
		end
	end
		
	-- Are we placing a ghost object?
	if self.isPlacementAction then
		
		-- Check whether a media player can be placed or moved
		if( not self:checkMediaPlayerPlacement() ) then
			return "Too close to media player owned by another"
		-- If placing anything else within a claim flag
		else
			local flag = self:checkForNearestClaimFlag()
			if flag then
				-- Debug.printTable("", flag)
				local objectCount = self:getFlagObjectCount(flag)
				if objectCount > self.maxFlagObjects then
					return "No more objects can be placed within this Land Claim Flag."
				end
			end			
		end

	else -- We must be moving an existing object
		
		-- CJW, TODO: This check should be placed inside a function of its own
		-- Make sure the player is not trying to move a placed object that he/she doesn't own outside of the boundaries of a claim flag
		if not self.claimFlagFound then
			local placedObject = self.placedObjectList[self.objectPID]
			local isPlacedObjectOwnedByAnother = placedObject and placedObject.owner ~= self.playerName
			if isPlacedObjectOwnedByAnother then
				return "You cannot move a claimed object you don't own to a location outside the flag's boundary."
			end
		end		
		
		if( not self:checkMediaPlayerMovement() ) then
			return "Too close to media player owned by another"
		end
		
		-- Make sure the player is not trying to manipulate an object from outside of the allowed range.
		if not self.fInteractionRangeTester(self.objectPID) then
			return "You've moved the object too far."
		end
	end
	
	return nil
end



--------------------------------------
-- BUILD ACTION FACTORY FUNCTIONALITY
--------------------------------------

BuildActionFactory = {} -- Pseudo-class system for collecting ghost update/movement actions (implemented so as to keep related game logic in one place...)

-- ghostObject is a table containing the PID and properties of the item we want to place.
-- position is where you want to place the object or move it to, depending on the function you're calling
-- worldInfo carries the following info:
--		name of player who's doing the placing/moving
--		an up-to-date list of placed objects in the world (each placed object comprised of the owner name and position info)
--		whether the world build setting is turned on
--		an up-to-date Flag System
--		an up-to-date Media System
function BuildActionFactory.createPlacementAction(ghostObject, position, worldInfo) -- 1234
	local action = { 	
		location = position,
		objectPID = ghostObject.PID,
		ghostObject = ghostObject,
		isPlacementAction = true,
		playerName = worldInfo.playerName,
		placedObjectList = worldInfo.placedObjectList,
		isBuildingAllowedInWorld = worldInfo.isBuildingAllowedInWorld,
		maxFlags = worldInfo.maxFlags,
		maxFlagObjects = worldInfo.maxFlagObjects,
		flagBuildOnly = worldInfo.flagBuildOnly,
		flagSystem = worldInfo.flagSystem,
		mediaSystem = worldInfo.mediaSystem,
		fInteractionRangeTester = worldInfo.fInteractionRangeTester,
	}
	
	return setmetatable(action, {__index = BuildAction})
end

function BuildActionFactory.createMovementAction(objectPID, position, worldInfo)
	local action = {
		location = position,
		objectPID = objectPID,
		ghostObject = nil,
		isPlacementAction = false,
		playerName = worldInfo.playerName,
		placedObjectList = worldInfo.placedObjectList,
		isBuildingAllowedInWorld = worldInfo.isBuildingAllowedInWorld,
		maxFlags = worldInfo.maxFlags,
		maxFlagObjects = worldInfo.maxFlagObjects,
		flagBuildOnly = worldInfo.flagBuildOnly,
		flagSystem = worldInfo.flagSystem,
		mediaSystem = worldInfo.mediaSystem,
		fInteractionRangeTester = worldInfo.fInteractionRangeTester,
	}
	
	return setmetatable(action, {__index = BuildAction})
end
