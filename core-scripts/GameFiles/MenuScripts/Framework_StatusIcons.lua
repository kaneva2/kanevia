--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_BottomRightHUD.lua
--
-- Allows players to toggle between God and Player Mode
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\MenuAnimation.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("ContextualHelper.lua")

-- Local vars
local m_playerName

local m_minimized = false

local m_showingContextualHelp = false

local statusImages = {"imgPVPEnabled", "imgPVPDisabled", "imgBuildEnabled", "imgBuildDisabled", "imgDestructionEnabled", "imgDestructionDisabled"}

-- Called when the menu is created
function onCreate()
	-- Event handlers
	KEP_EventRegister( "FRAMEWORK_HIDE_ICONS", KEP.HIGH_PRIO )

	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS", returnSettingsHandler)
	Events.registerHandler("FRAMEWORK_CHANGE_STATUS_ICON", changeIcon)
	Events.registerHandler("FRAMEWORK_RETURN_FLAG_ZONE_SETTINGS", flagZoneSettingsHandler)

	KEP_EventRegisterHandler("openContextualMenuEventHandler", "openContextualMenuEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("closeContextualMenuEventHandler", "closeContextualMenuEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("bottomHUDMinimizedEventHandler", "bottomHUDMinimizedEvent", KEP.HIGH_PRIO)

	Events.sendEvent("FRAMEWORK_REQUEST_FLAG_ZONE_SETTINGS")
	--Events.sendEvent("FRAMEWORK_REQUEST_SETTINGS")

	m_playerName 	 = KEP_GetLoginName()

	Control_SetToolTip(gHandles["imgPVPEnabled"], "Player Combat Area")
	Control_SetToolTip(gHandles["imgPVPDisabled"], "No Player Combat Area")
	Control_SetToolTip(gHandles["imgBuildEnabled"], "Player Building Area")
	Control_SetToolTip(gHandles["imgBuildDisabled"], "No Player Building Area")
	Control_SetToolTip(gHandles["imgDestructionEnabled"], "Player Destruction Area")
	Control_SetToolTip(gHandles["imgDestructionDisabled"], "No Player Destruction Area")
end

function changeIcon(event)
	local status = event.status
	local enabled = event.enabled

	if status == "PVP" then
		Control_SetVisible(gHandles["imgPVPEnabled"], enabled)
		Control_SetVisible(gHandles["imgPVPDisabled"], not enabled)
	elseif status == "Build" then
		Control_SetVisible(gHandles["imgBuildEnabled"], enabled)
		Control_SetVisible(gHandles["imgBuildDisabled"], not enabled)
	elseif status == "Destruction" then
		Control_SetVisible(gHandles["imgDestructionEnabled"], enabled)
		Control_SetVisible(gHandles["imgDestructionDisabled"], not enabled)
	end

	-- B: Show the status contextual help
	if canShowContextualHelp() then
		ContextualHelper.showHelp(ContextualHelp.STATUS_AREA,
			MenuGetLocationThis(),
			Control_GetLocationX(gHandles["imgPVPEnabled"]),
			Control_GetLocationY(gHandles["imgPVPEnabled"]),
			Control_GetWidth(gHandles["imgPVPEnabled"]),
			Control_GetHeight(gHandles["imgPVPEnabled"]),
			ContextualHelpFormat.RIGHT_ARROW,
			30)
	end
end

function flagZoneSettingsHandler(event)
	updateIcons(event.PVP, event.build, event.destruction)
end

function returnSettingsHandler(event)

	destructibleObjects = event.destructibleObjects
	allowPVP = event.allowPvP
	playerBuild = event.playerBuild
	updateIcons(allowPVP, playerBuild, destructibleObjects)
	Events.sendEvent("FRAMEWORK_REQUEST_FLAG_ZONE_SETTINGS")

end

function updateIcons(allowPVP, playerBuild, destructibleObjects)
	Control_SetVisible(gHandles["imgPVPEnabled"], allowPVP)
	Control_SetVisible(gHandles["imgPVPDisabled"], not allowPVP)
	Control_SetVisible(gHandles["imgBuildEnabled"], playerBuild)
	Control_SetVisible(gHandles["imgBuildDisabled"], not playerBuild)
	Control_SetVisible(gHandles["imgDestructionEnabled"], destructibleObjects)
	Control_SetVisible(gHandles["imgDestructionDisabled"], not destructibleObjects)
end

-- Mouse moved hack to catch mouse down events.
function Dialog_OnMouseMove( dialogHandle, x, y )
	local pos = { x = x, y = y }

	if MenuContainsPointRelThis(pos.x , pos.y + 20) then
		ContextualHelper.completeHelp(ContextualHelp.STATUS_AREA)
	end 
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_showingContextualHelp then
		ContextualHelper.updateHelp(ContextualHelp.STATUS_AREA,
			MenuGetLocationThis(),
			Control_GetLocationX(gHandles["imgPVPEnabled"]),
			Control_GetLocationY(gHandles["imgPVPEnabled"]),
			Control_GetWidth(gHandles["imgPVPEnabled"]),
			Control_GetHeight(gHandles["imgPVPEnabled"]),
			ContextualHelpFormat.RIGHT_ARROW)
	end 
end

-- Handles the "openContextualMenuEvent" sent when contextual help is started.
function openContextualMenuEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local eventParams = KEP_EventDecodeString(tEvent)
	eventParams = Events.decode(eventParams)
	local helpID = eventParams.type
	if helpID == ContextualHelp.STATUS_AREA then
		m_showingContextualHelp = true
	end 
end

-- Handles the "closeContextualMenuEvent" sent when contextual help is closed.
function closeContextualMenuEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local eventParams = KEP_EventDecodeString(tEvent)
	eventParams = Events.decode(eventParams)
	local helpID = eventParams.type
	if helpID == ContextualHelp.STATUS_AREA then
		m_showingContextualHelp = false
	end 
end

function bottomHUDMinimizedEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local eventParams = KEP_EventDecodeString(tEvent)
	m_minimized = eventParams.minimized
end

function canShowContextualHelp()
	return (not GetHideToolbeltSetting() or not m_minimized)
end