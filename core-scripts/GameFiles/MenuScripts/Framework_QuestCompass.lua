--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Framework_Crafting.lua
--
-- Displays available recipes for crafting
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_InventoryHelper.lua")
dofile("MenuAnimation.lua")
dofile("..\\MenuScripts\\Lib_Vecmath.lua")

local Math = _G.Math

-- -- -- -- -- -- --
-- CONSTANTS
-- -- -- -- -- -- --

local BLIP_COUNT = 16
local BLIP_SPACING = 20
local BLIP_BOUNDARY_LEFT = -160
local BLIP_BOUNDARY_RIGHT = 160

local TRACKING_MIN_POSITION = -106
local TRACKING_MAX_POSITION = 148

local ICON_BOUNDARY_LEFT = -115
local ICON_BOUNDARY_RIGHT = 134

local TRACKING_SPEED_MOD = 2

local QUEST_STATE = {IN_PROGRESS = "inProgress", RETURNING = "returning"}
local COMPASS_LAYOUT = {DEFAULT = "compass", NO_WAPOINT = "noWaypoint", IN_AREA = "inArea"}

COMPASS_FLASH_DURATION = 10
COMPASS_FLASH_COUNT = 10

local MINIMIZE_DISPLACEMENT = 54
local MINIMIZE_DURATION = .18
local MINIMIZE_BOTTOM_ALL_FILTER = 0
local MINIMIZE_INDICATORS_FILTER = 2

-- -- -- -- -- -- --
-- LOCAL FUNCTIONS
-- -- -- -- -- -- --

local positionTracker
local positionBlips
local updateQuestInfo
local clearQuestInfo
local getQuestReturnLocation
local setCompassLayout
local showBlips
local hideBlips
local updateQuestState
local toggleHUDMinimized
local toggleCompassMinimized
local canMinimizeCompass

-- -- -- -- -- -- --
-- LOCAL VARIABLES
-- -- -- -- -- -- --

local m_blipAnchor = BLIP_BOUNDARY_LEFT

local m_playerPosition = {}
local m_questLocations = {}
local m_targetPosition

local m_playerQuests = {}
local m_questInfo
local m_questState
local m_compassLayout
local m_tweening = false
local m_tweenThrob
local m_tweenFlash
local m_flashCounts = 0
local m_hiddenForBuild = false

local m_messageDisplayed = false

local m_dynamicEnable = false
local m_minimized = false
local m_minimizeAnimation = nil
local m_toolbeltMinimized = false

local m_gameItems = {}

-- Called when the menu is created
function onCreate()
	-- Register events
	KEP_EventRegister( "FRAMEWORK_TRACK_PLAYER_QUEST", KEP.HIGH_PRIO )
	KEP_EventRegister( "FRAMEWORK_GET_TRACKED_QUEST", KEP.HIGH_PRIO )
	KEP_EventRegister( "FRAMEWORK_RETURN_TRACKED_QUEST", KEP.HIGH_PRIO )
	KEP_EventRegister( "FRAMEWORK_GET_COMPASS_DATA", KEP.HIGH_PRIO ) 
	KEP_EventRegister( "QUEST_COMPASS_TRACK_NEW_QUEST", KEP.HIGH_PRIO )

	-- Register handlers for client-to-client events
	KEP_EventRegisterHandler("updateQuestsClient", "UPDATE_QUEST_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateQuestsClientFull", "UPDATE_QUEST_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("returnTrackedQuest", "FRAMEWORK_GET_TRACKED_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("returnCompassData", "FRAMEWORK_GET_COMPASS_DATA", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("trackPlayerQuest", "FRAMEWORK_TRACK_PLAYER_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("trackNewQuest", "QUEST_COMPASS_TRACK_NEW_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCompleteQuest", "INVENTORY_HANDLER_COMPLETE_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("frameworkMinimizeHUDHandler", "FRAMEWORK_MINIMIZE_HUD", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("frameworkOnBuildHelperChangeHandler", "FRAMEWORK_ON_BUILD_HELPER_CHANGE", KEP.HIGH_PRIO )

	-- game item cache 
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)

	-- Register handler for server-to-client events
	Events.registerHandler("FRAMEWORK_RETURN_QUEST_GIVER_SPAWNERS", returnQuestGiverSpawners)
	Events.registerHandler("FRAMEWORK_RETURN_TRACKED_QUEST", returnCurrentQuest)

	-- Send Client-to-Server Event
	Events.sendEvent("FRAMEWORK_GET_QUEST_GIVER_SPAWNERS")

	requestInventory(QUEST_PID)
	requestInventory(GAME_PID)

	m_playerPosition.x, m_playerPosition.y, m_playerPosition.z, m_playerPosition.r = KEP_GetPlayerPosition()

	setCompassLayout(COMPASS_LAYOUT.DEFAULT)

	m_blipAnchor = BLIP_BOUNDARY_LEFT
	positionBlips()

	-- Dynamic HUD A/B test. If in the original, default to showing.
	m_dynamicEnable = false
	m_minimizeAnimation = Tween(MenuHandleThis())
	m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT, MINIMIZE_DURATION, nil, Easing.inOutQuad)
	toggleCompassMinimized(true, true)

	MenuSetMinimizedThis(true)
end

function onDestroy()

	Events.sendEvent("UPDATE_PLAYER_TRACKED_QUEST", {quest = m_questInfo})
end

-- Called when the dialog is moved
function Dialog_OnMoved(dialogHandle, x, y)
	toggleCompassMinimized(m_minimized, true)
end

-- On frame update
function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- If we're currently crafting something	
	if m_questInfo then
		MenuAnimation_OnRender(fElapsedTime)
		m_playerPosition.x, m_playerPosition.y, m_playerPosition.z, m_playerPosition.r = KEP_GetPlayerPosition()

		if m_compassLayout == COMPASS_LAYOUT.DEFAULT then
			local offset
			local prevR = m_playerPosition.r
			local angleDiff = prevR - m_playerPosition.r

			if angleDiff > 180 then
				angleDiff = angleDiff - 360
			elseif angleDiff < -180 then
				angleDiff = 360 + angleDiff
			end

			m_blipAnchor = ( m_blipAnchor + ( angleDiff * TRACKING_SPEED_MOD ) )

			if m_blipAnchor > BLIP_BOUNDARY_RIGHT then
				m_blipAnchor = BLIP_BOUNDARY_LEFT + (m_blipAnchor - BLIP_BOUNDARY_RIGHT)
			elseif m_blipAnchor <= BLIP_BOUNDARY_LEFT then
				m_blipAnchor = BLIP_BOUNDARY_RIGHT - (BLIP_BOUNDARY_LEFT - m_blipAnchor)
			end

			positionBlips()
			positionTracker()

			if m_questState == QUEST_STATE.IN_PROGRESS and m_questInfo.properties.waypointInfo and  m_questInfo.properties.waypointPID then
				if m_targetPosition then
					local radius = m_questInfo.properties.waypointInfo.radius
					local distanceSq = Math.getVectorDistanceSq(m_playerPosition, m_targetPosition)

					if radius * radius >= distanceSq then
						setCompassLayout(COMPASS_LAYOUT.IN_AREA)
					end
				end
			elseif m_questState == QUEST_STATE.RETURNING and m_questInfo.properties.waypointInfo and m_questInfo.properties.waypointPID then
				if not m_tweening then
					flashCallback()
					m_tweening = true
				end 
			end

		elseif m_compassLayout == COMPASS_LAYOUT.IN_AREA and m_questInfo.properties.waypointInfo and  m_questInfo.properties.waypointPID then
			if m_targetPosition then
				local radius = m_questInfo.properties.waypointInfo.radius
				local distanceSq = Math.getVectorDistanceSq(m_playerPosition, m_targetPosition)

				if radius * radius < distanceSq then
					setCompassLayout(COMPASS_LAYOUT.DEFAULT)
				else
					if not m_tweening then
						m_tweenThrob = Tween(nil, {"imgInQuestArea"})
						m_tweenThrob:fade(-75, .75)
						m_tweenThrob:on(CALLBACK.STOP, throbCallback)
						m_tweenThrob:start()
						m_tweening = true
					end
				end
			end
		end		
	end
end

function throbCallback()
	if m_tweenThrob then
		m_tweenThrob:reverse(not m_tweenThrob:getReversed())
		m_tweenThrob:start()
	end
end

function flashCallback()
	if m_flashCounts < COMPASS_FLASH_COUNT then
		if m_flashCounts % 2 == 0 then
			m_tweenFlash = Tween(nil, {"imgQuestGiverGlow"})
			m_tweenFlash:fade(-255, COMPASS_FLASH_DURATION / COMPASS_FLASH_COUNT, 0)
			m_tweenFlash:on(CALLBACK.STOP, flashCallback)
			m_tweenFlash:start()
		else
			m_tweenFlash = Tween(nil, {"imgQuestGiverGlow"})
			m_tweenFlash:fade(255, COMPASS_FLASH_DURATION / COMPASS_FLASH_COUNT, 0)
			m_tweenFlash:on(CALLBACK.STOP, flashCallback)
			m_tweenFlash:start()
		end
		m_flashCounts = m_flashCounts + 1
	end
end

-- Handles the "frameworkMinimizeHUD" event triggered when hud is hidden.
function frameworkMinimizeHUDHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == MINIMIZE_INDICATORS_FILTER then
		local checked = Event_DecodeNumber(event)
		local instant = Event_DecodeNumber(event)
		local dynamicEnable = Event_DecodeNumber(event)
		m_dynamicEnable = m_dynamicEnable or dynamicEnable == 1
		m_toolbeltMinimized = checked == 1
		toggleCompassMinimized(m_toolbeltMinimized, instant == 1)
	end
end

-- Handles the "FRAMEWORK_ON_BUILD_HELPER_CHANGE" event triggered when build helper is opened or closed
function frameworkOnBuildHelperChangeHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local buildHelpActive = Event_DecodeNumber(event)
	if buildHelpActive == 1 and m_hiddenForBuild == false and m_questInfo then
		MenuSetMinimizedThis(true)
		m_hiddenForBuild = true
	elseif buildHelpActive == 0 and m_hiddenForBuild == true then
		MenuSetMinimizedThis(false)
		m_hiddenForBuild = false
	end
end

--------------------
-- EVENT HANDLERS --
--------------------

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
end

function updateQuestsClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateUNID = KEP_EventDecodeNumber(tEvent)
	local updateQuest = decompileInventoryItem(tEvent)

	for i,info in pairs(m_playerQuests) do
		if info ~= nil and info.UNID ~= nil then
			if tonumber(m_playerQuests[i].UNID) == tonumber(updateUNID) then
				m_playerQuests[i] = updateQuest
				if m_questInfo and tonumber(m_questInfo.UNID) == tonumber(m_playerQuests[i].UNID) then
					m_questInfo = m_playerQuests[i]
					updateQuestInfo()
				end
			end
		end
	end
end

function updateQuestsClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_playerQuests = decompileInventory(tEvent)
		
	if m_questInfo then
		local questExists = false

		for i,info in pairs(m_playerQuests) do
			if info ~= nil and info.UNID ~= nil then
				if tonumber(m_questInfo.UNID) == tonumber(m_playerQuests[i].UNID) then
					questExists = true
				end
			end
		end

		if not questExists then
			clearQuestInfo()
		end
	end
end

function onCompleteQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = KEP_EventDecodeNumber(tEvent)
	if m_questInfo and m_questInfo.UNID and tonumber(m_questInfo.UNID) == tonumber(questUNID) then
		clearQuestInfo()
	end
end

function returnTrackedQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	if m_questInfo then
		-- Send quest info over to Quest Compass
		local ev = KEP_EventCreate("FRAMEWORK_RETURN_TRACKED_QUEST")
		KEP_EventEncodeString(ev, m_questInfo.UNID)
		KEP_EventQueue(ev)
	end	
end

function returnCompassData(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- Relevant compass info over to Framework_Map
	local ev = KEP_EventCreate("FRAMEWORK_RETURN_COMPASS_DATA")
	local questUNID = nil
	local questName = nil
	if m_questInfo then
		questUNID = m_questInfo.UNID
		questName = m_questInfo.properties.name
	end
	KEP_EventEncodeString(ev, questUNID)	-- UNID of current quest (for finding relevant quest giver)
	KEP_EventEncodeString(ev, questName)	-- Name of current quest
	local readyForTurnIn = "inProgress"
	if m_questState == QUEST_STATE.RETURNING then	-- Quest is ready for turn-in
		readyForTurnIn = "returning"
	end
	KEP_EventEncodeString(ev, readyForTurnIn)
	local tempX, tempY, tempZ = nil
	if m_targetPosition then
		tempX = m_targetPosition.x
		tempY = m_targetPosition.y
		tempZ = m_targetPosition.z
	end
	KEP_EventEncodeString(ev, tempX)	-- Compass target x
	KEP_EventEncodeString(ev, tempY)	-- Compass target y
	KEP_EventEncodeString(ev, tempZ)	-- Compass target z
	KEP_EventQueue(ev)
end

function trackPlayerQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = KEP_EventDecodeString(tEvent)

	if tonumber(questUNID) and tonumber(questUNID) > 0 then
		for i,info in pairs(m_playerQuests) do
			if info ~= nil and info.UNID ~= nil then
				if tonumber(m_playerQuests[i].UNID) == tonumber(questUNID) then
					clearQuestInfo()

					m_questInfo = m_playerQuests[i]
					updateQuestInfo()
					MenuSetMinimizedThis(false)
					toggleHUDMinimized(false)
				end
			end
		end
	else
		clearQuestInfo()
	end
end

function returnQuestGiverSpawners(event)
	m_questLocations = event.questLocations

	if m_questInfo then
		updateQuestInfo()
		MenuSetMinimizedThis(false)
	end
end

function returnCurrentQuest(event)
	if event.quest then
		m_questInfo = event.quest
		updateQuestInfo()
		MenuSetMinimizedThis(false)
	end
end

function trackNewQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = KEP_EventDecodeNumber(tEvent)
	local questItem = decompileInventoryItem(tEvent)

	if m_questInfo == nil or m_questInfo.UNID == nil then
		m_questInfo = questItem
		updateQuestInfo()
		MenuSetMinimizedThis(false)
		toggleHUDMinimized(false)
	end
end

----------------
-- QUEST INFO --
----------------

-- Update current quests info
updateQuestInfo = function()
	if m_questInfo then
		local questTitle = m_questInfo.properties.name
		local questReqCount
		local playerItemCount
		local questType = m_questInfo.properties.questType
		local isAutoComplete = m_questInfo.properties.autoComplete or "false"

		if questType == "collect" or questType == "takeTo" then
			if m_questInfo.properties.requiredItem and m_questInfo.properties.requiredItem.requiredCount and m_questInfo.properties.requiredItem.requiredCount > 0 then
				questReqCount = m_questInfo.properties.requiredItem.requiredCount or 0

				playerItemCount = m_questInfo.properties.requiredItem.progressCount or 0
				playerItemCount = math.min(playerItemCount, questReqCount)
				questTitle = questTitle .. " (" .. playerItemCount .. "/" .. questReqCount .. ")"
			end
		elseif usesFulfillCount(questType) then
			questReqCount = m_questInfo.properties.requiredItem.requiredCount or 0

			playerItemCount = m_questInfo.fulfillCount or 0
			playerItemCount = math.min(playerItemCount, questReqCount)
			questTitle = questTitle .. " (" .. playerItemCount .. "/" .. questReqCount .. ")"
		elseif m_questInfo.fulfilled and m_questInfo.fulfilled == true then
			if questType == "goTo" or questType == "takeTo" or questType == "talkTo" then
				questTitle = questTitle .. " (1/1)"
			end
		end

		Static_SetText( gHandles["stcQuestTitle"], questTitle)
		Static_SetText( gHandles["stcQuestTitleCenter"], questTitle)
		applyImage(gHandles["imgQuestIcon"], tonumber(m_questInfo.properties.GLID))
		Control_SetVisible(gHandles["imgQuestIcon"], true)

		local target

		if (questReqCount and questReqCount == playerItemCount and questType ~= "takeTo") or m_questInfo.fulfilled then
			-- (m_questInfo.fulfillCount and questReqCount and m_questInfo.fulfillCount >= questReqCount) then
			--Track nearest quest giver			
			updateQuestState(QUEST_STATE.RETURNING)
			target = getQuestReturnLocation(tostring(m_questInfo.UNID))

			local questGiverName = nil

			local objName, objDescription, objCanPlayMovie, objIsAttachable, objInteractive, objIsLocal, objDerivable, objAssetId, objFriendId, objCustomTexture, objGlobalId, objInvType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(target)
			-- actual actors UNID 
			--log("--- gameItemId: ".. tostring(gameItemId))

			local targetActorSpawner = m_gameItems[tostring(gameItemId)]
			--log("--- targetActor: ".. tostring(targetActor))
			
			if targetActorSpawner then 
				local targetActor = targetActorSpawner
				if targetActorSpawner.behavior and targetActorSpawner.behavior == "spawner_character" then
					targetActor = m_gameItems[tostring(targetActorSpawner.behaviorParams.spawnUNID)]
				end

				local targetActorName = targetActor.behaviorParams.actorName
				if targetActorName then
					questGiverName = tostring(targetActorName)
				end
			end
						
			if questGiverName then
				local questText = ""
				if questType == "collect" and tostring(isAutoComplete) == "false" then
					questText = "Return items to ".. questGiverName.. "!"
				elseif m_questInfo.fulfilled and tostring(m_questInfo.fulfilled) == "true" and tostring(isAutoComplete) == "false" then
					questText = "Return to ".. questGiverName.. "!"
				end

				-- show a status event to return the items one time
				if not m_messageDisplayed then
					local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
					KEP_EventEncodeNumber(statusEvent, 0)
					KEP_EventEncodeString(statusEvent, questText)
					KEP_EventEncodeNumber(statusEvent, 3)
					KEP_EventEncodeNumber(statusEvent, 0)
					KEP_EventEncodeNumber(statusEvent, 255)
					KEP_EventEncodeNumber(statusEvent, 0)
					KEP_EventSetFilter(statusEvent, 4)
					KEP_EventQueue(statusEvent)
					m_messageDisplayed =  true
				end

				-- set the compass text
				Static_SetText( gHandles["stcQuestTitle"], questText)
			end
		else
			m_messageDisplayed = false
			if m_questInfo.properties.waypointPID and m_questInfo.properties.waypointPID > 0 then
				target = m_questInfo.properties.waypointPID
				updateQuestState(QUEST_STATE.IN_PROGRESS)
			end
		end

		local objName, objDescription, objCanPlayMovie, objIsAttachable, objInteractive, objIsLocal, objDerivable, objAssetId, objFriendId, objCustomTexture, objGlobalId, objInvType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(tostring(target))

		if target and gameItemId ~= 0 then
			m_targetPosition = {}
			m_targetPosition.x, m_targetPosition.y, m_targetPosition.z = KEP_DynamicObjectGetPositionAnim(target)
			positionTracker()
			setCompassLayout(COMPASS_LAYOUT.DEFAULT)
		else
			m_targetPosition = nil
			setCompassLayout(COMPASS_LAYOUT.NO_WAPOINT)
		end
	end
end

-- Clear quest compass
clearQuestInfo = function()
	m_questInfo = nil

	Static_SetText( gHandles["stcQuestTitle"], "")
	Control_SetVisible(gHandles["stcQuestTitle"], false)
	Static_SetText( gHandles["stcQuestTitleCenter"], "")
	Control_SetVisible(gHandles["stcQuestTitleCenter"], false)
	Control_SetVisible(gHandles["imgQuestIcon"], false)
	Control_SetVisible(gHandles["imgQuestLoc"], false)
	Control_SetVisible(gHandles["imgQuestGiver"], false)
	Control_SetVisible(gHandles["imgQuestGiverGlow"], false)

	MenuSetMinimizedThis(true)
	toggleCompassMinimized(false)

	Events.sendEvent("UPDATE_PLAYER_TRACKED_QUEST", {quest = false})
end

-----------------------------
-- COMPASS STATE FUNCTIONS --
-----------------------------

setCompassLayout = function(state)
	if state == COMPASS_LAYOUT.DEFAULT then
		showBlips()

		Control_SetVisible(gHandles["imgHeading"], true)
		Control_SetVisible(gHandles["stcQuestTitle"], true)
		Control_SetVisible(gHandles["imgHorizontal"], true)
		Control_SetVisible(gHandles["stcQuestTitleCenter"], false)
		Control_SetVisible(gHandles["imgInQuestArea"], false)
		updateQuestState(m_questState)

	elseif state == COMPASS_LAYOUT.NO_WAPOINT then
		hideBlips()

		Control_SetVisible(gHandles["imgHeading"], false)
		Control_SetVisible(gHandles["stcQuestTitle"], false)
		Control_SetVisible( gHandles["imgQuestGiver"], false )
		Control_SetVisible( gHandles["imgQuestGiverGlow"], false )
		Control_SetVisible( gHandles["imgQuestLoc"], false )
		Control_SetVisible(gHandles["imgHorizontal"], false)
		Control_SetVisible( gHandles["imgLeftArrow"], false )
		Control_SetVisible( gHandles["imgRightArrow"], false )
		Control_SetVisible(gHandles["imgInQuestArea"], false)
		Control_SetVisible(gHandles["stcQuestTitleCenter"], true)

	elseif state == COMPASS_LAYOUT.IN_AREA then
		hideBlips()

		Control_SetVisible(gHandles["imgHeading"], false)
		Control_SetVisible(gHandles["stcQuestTitle"], false)
		Control_SetVisible( gHandles["imgQuestGiver"], false )
		Control_SetVisible( gHandles["imgQuestGiverGlow"], false )
		Control_SetVisible( gHandles["imgQuestLoc"], false )
		Control_SetVisible(gHandles["imgHorizontal"], false)
		Control_SetVisible( gHandles["imgLeftArrow"], false )
		Control_SetVisible( gHandles["imgRightArrow"], false )
		Control_SetVisible(gHandles["imgInQuestArea"], true)
		Control_SetVisible(gHandles["stcQuestTitleCenter"], true)
	end

	m_compassLayout = state
end

updateQuestState = function(state)
	if state == QUEST_STATE.IN_PROGRESS then
		Control_SetVisible( gHandles["imgQuestGiver"], false )
		Control_SetVisible( gHandles["imgQuestGiverGlow"], false )
		Control_SetVisible( gHandles["imgQuestLoc"], true )
	elseif state == QUEST_STATE.RETURNING then
		Control_SetVisible( gHandles["imgQuestGiver"], true )
		Control_SetVisible( gHandles["imgQuestGiverGlow"], true )
		Control_SetVisible( gHandles["imgQuestLoc"], false )
	end

	m_questState = state
end
	
showBlips = function()
	for i=1,BLIP_COUNT do
		Control_SetVisible( gHandles["imgBlip" .. i], true )
	end
end

hideBlips = function()
	for i=1,BLIP_COUNT do
		Control_SetVisible( gHandles["imgBlip" .. i], false )
	end
end

-----------------------
-- DISPLAY FUNCTIONS --
-----------------------

positionTracker = function()
	if m_targetPosition then
		local offset = 0
		local targetAngle
		local trackerPosition

		-- If quest is being turned in
		if m_questState == QUEST_STATE.RETURNING then
			-- Track nearest quest giver
			local PID = getQuestReturnLocation(tostring(m_questInfo.UNID))
			m_targetPosition.x, m_targetPosition.y, m_targetPosition.z = KEP_DynamicObjectGetPositionAnim(PID)
		end

		local deltaX = m_playerPosition.x - m_targetPosition.x
		local deltaZ = m_playerPosition.z - m_targetPosition.z

		local angle = math.abs((math.atan2(deltaX, deltaZ) / math.pi * 180) + 180)
		local playerAngle = math.fmod(m_playerPosition.r + 180, 360)

		targetAngle = -(playerAngle - angle)

		if targetAngle > 180 then
			targetAngle = targetAngle - 360
		elseif targetAngle < -180 then
			targetAngle = 360 + targetAngle
		end

		trackerPosition = (targetAngle * TRACKING_SPEED_MOD)

		if trackerPosition > TRACKING_MAX_POSITION then
			trackerPosition = TRACKING_MAX_POSITION
			Control_SetVisible( gHandles["imgLeftArrow"], false )
			Control_SetVisible( gHandles["imgRightArrow"], true )
		elseif trackerPosition < TRACKING_MIN_POSITION then
			trackerPosition = TRACKING_MIN_POSITION
			Control_SetVisible( gHandles["imgLeftArrow"], true )
			Control_SetVisible( gHandles["imgRightArrow"], false )
		else
			Control_SetVisible( gHandles["imgRightArrow"], false )
			Control_SetVisible( gHandles["imgLeftArrow"], false )
		end	

		if m_questState == QUEST_STATE.RETURNING then
			Control_SetLocationX(gHandles["imgQuestGiver"], trackerPosition - 12)
			Control_SetLocationX(gHandles["imgQuestGiverGlow"], trackerPosition - 21)
		else
			Control_SetLocationX(gHandles["imgQuestLoc"], trackerPosition - 12)
		end
	end
end

positionBlips = function()
	local blipLoc = m_blipAnchor
	for i=1,BLIP_COUNT do
		if blipLoc > BLIP_BOUNDARY_RIGHT then
			blipLoc = BLIP_BOUNDARY_LEFT + (blipLoc - BLIP_BOUNDARY_RIGHT)
		elseif blipLoc < BLIP_BOUNDARY_LEFT then
			blipLoc = BLIP_BOUNDARY_RIGHT - (BLIP_BOUNDARY_LEFT - blipLoc)
		end

		Control_SetLocationX(gHandles["imgBlip" .. i], blipLoc)
		
		blipLoc = blipLoc + BLIP_SPACING
	end
end

getQuestReturnLocation = function(UNID)
	if m_questLocations[UNID] then
		local closestPID
		local closestDistance

		local currentLocation = {x = 0, y = 0, z = 0}
		local currentDistance

		for _,PID in pairs(m_questLocations[UNID]) do
			currentLocation.x, currentLocation.y, currentLocation.z  = KEP_DynamicObjectGetPositionAnim(PID)
			currentDistance = Math.getVectorDistance(m_playerPosition, currentLocation)

			if closestPID == nil then							
				closestDistance = currentDistance
				closestPID = PID
			else
				if currentDistance < closestDistance then
					closestDistance = currentDistance
					closestPID = PID
				end
			end			
		end

		--local objName, objDescription, objCanPlayMovie, objIsAttachable, objInteractive, objIsLocal, objDerivable, objAssetId, objFriendId, objCustomTexture, objGlobalId, objInvType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(closestPID)
		--log("--- gameItemId: ".. tostring(gameItemId))

		return closestPID
	end
end


function btnCompass_OnButtonClicked(buttonHandle)
	openQuestInfo()
end

function btnCompass_OnRButtonUp(buttonHandle)
	openQuestInfo()
end

function openQuestInfo()
	MenuOpen("Framework_QuestInfo.xml")
	local ev = KEP_EventCreate("FRAMEWORK_QUEST_INFO") -- Handled by Framework_QuestInfo or Framework_QuestTurnIn
	KEP_EventEncodeString(ev, Events.encode(m_questInfo))
	KEP_EventEncodeString(ev, "false")
	KEP_EventEncodeString(ev, "false") -- Quest isn't modal
	KEP_EventQueue(ev)
end

toggleHUDMinimized = function(minimized)
	local ev = KEP_EventCreate("FRAMEWORK_MINIMIZE_HUD")
	KEP_EventSetFilter(ev, MINIMIZE_BOTTOM_ALL_FILTER)
	KEP_EventEncodeNumber(ev, minimized and 1 or 0)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventEncodeNumber(ev, minimized and 0 or 1)
	KEP_EventQueue(ev)
end

toggleCompassMinimized = function(minimized, instant)
	if minimized and not canMinimizeCompass() then return end
	-- Dynamic HUD A/B test, override min/max if dynamic rules not met.
	if not minimized and (not m_dynamicEnable or MenuIsOpen("ProgressMenu.xml")) and canMinimizeCompass() then
		minimized = true
	end

	if instant then
		local menuLoc = MenuGetLocationThis()
		local screenHeight = Dialog_GetScreenHeight(gDialogHandle)

		m_minimizeAnimation:stop()
		MenuSetLocationThis(menuLoc.x, screenHeight)	
		m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT, MINIMIZE_DURATION, nil, Easing.inOutQuad)

		MenuSetLocationThis(menuLoc.x, screenHeight + (minimized and MINIMIZE_DISPLACEMENT or 0))	

	elseif m_minimized ~= minimized then
		m_minimizeAnimation:stop()
		if minimized then
			m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT, MINIMIZE_DURATION * 2.6, nil, Easing.inOutQuad, true)
		else
			m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT, MINIMIZE_DURATION, nil, Easing.inOutQuad, true)
		end
		m_minimizeAnimation:reverse(not minimized)
		m_minimizeAnimation:start()
	end

	m_minimized = minimized
end

-- Called to determine if the toolbar is in a valid state to minimize.
canMinimizeCompass = function()
	return m_toolbeltMinimized or MenuIsOpen("ProgressMenu.xml")
end