--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")
dofile("..\\Scripts\\TextureImportStateIds.lua")
dofile("..\\Scripts\\UGCImportState.lua")

g_ugcType = nil
g_ugcObjId = nil

gDropId = -1
gImportInfo = nil
gTargetInfo = nil
gBtnState = 0
gKeyState = 0

TEXTURE_IMPORT_PROGRESS_EVENT = "TextureImportProgressEvent"
UGC_DEPLOY_COMPLETION_EVENT = "UGCDeployCompletionEvent"

function UGCImportMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	gDropId = UGCI.GetDialogInstanceId( gDialogHandle )
	local dlgId = Dialog_GetId( gDialogHandle )

	if filter~=dlgId or objectid~=gDropId then
		-- Ignore event with mismatched IDs
		return
	end

	local eventData = UGCI.ParseUGCImportMenuEvent( event )
	gImportInfo = eventData.importInfo
	gTargetInfo = eventData.targetInfo
	gBtnState = eventData.btnState
	gKeyState = eventData.keyState

	Log( "UGCImportMenuEventHandler: dlgId=" .. dlgId .. " dropId=" .. gDropId .. " type=" .. gImportInfo.type .. " path=" .. gImportInfo.fullPath )

	if gImportInfo.category==UGC_TYPE.TEXTURE then
		CustomizeObjectTextureAsync( gImportInfo, gTargetInfo, gBtnState, gKeyState )
	else
		MenuCloseThis()
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "UGCImportMenuEventHandler", UGC_IMPORT_MENU_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "TextureImportProgressEventHandler", TEXTURE_IMPORT_PROGRESS_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UGCSubmissionFeedbackEventHandler", UGC_SUBMISSION_FEEDBACK_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UploadCompletionHandler", UGC_UPLOAD_COMPLETION_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "UGCDeployCompletionHandler", UGC_DEPLOY_COMPLETION_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( TEXTURE_IMPORT_PROGRESS_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "PhotoUploadEvent", KEP.MED_PRIO )

	-- Submission and upload events
	UGCI.RegisterSubmissionEvents( )
	UGCI.RegisterUploadEvents( )
	UGCI.RegisterDeployEvents( )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

-- Asynchronous texture import
function CustomizeObjectTextureAsync( importInfo, target, btnState, keyState )
	Log( "CustomizeObjectTextureAsync: Process custom texture path=" .. importInfo.fullPath .. " target.id=" .. target.id  )

	local customizable = false
	Log( "target.type = " .. ObjectTypeString(target) )

	if (target.type==ObjectType.WORLD) then
		if KEP_IsModelCustomizable( target.id ) ~= 0 then
			customizable = true
		end
	elseif target.type==ObjectType.DYNAMIC then
		if (KEP_IsDynamicPlacementObjectCustomizable( target.id ) ~= 0) or (target.id == -1) then
			customizable = true
		end
	elseif target.type==ObjectType.UNKNOWN then
		customizable = true
	end

	if customizable then

		Log( "CustomizeObjectTextureAsync: Target Is Customizable" )
		local msg = "Importing texture: [" .. importInfo.fullPath .. "] "
		if (target.type==ObjectType.ZONE) or (target.type==ObjectType.WORLD) then
			msg = msg .. "to world object '" .. target.name .. "' target.id=" .. target.id
		elseif target.type==ObjectType.DYNAMIC then
			msg = msg .. "to dynamic object '" .. target.name .. "' target.id=" .. target.id
		elseif target.type~=ObjectType.UNKNOWN then
			msg = msg .. "-- unsupported target object"
		end

		if ( target.type==ObjectType.DYNAMIC and target.id == -1 ) then
			Log("CustomizeObjectTextureAsync: Uploading photo")
		else
			Log("CustomizeObjectTextureAsync: "..msg)
		end

		local ImpSessionId = -1
		local ImpSession = nil

		ImpSessionId, ImpSession = UGC_CreateImportSession( UGC_TYPE.TEXTURE )
		if ImpSession ~= nil then

			-- GetSet_SetNumber( ImpSession, TextureImportStateIds.IMPORTFLAGS, flags )
			GetSet_SetNumber( ImpSession, TextureImportStateIds.STATE, UGCIS.NONE )
			GetSet_SetString( ImpSession, TextureImportStateIds.SOURCEIMAGEPATH, importInfo.fullPath )
			local progressEventId = KEP_EventGetId( TEXTURE_IMPORT_PROGRESS_EVENT )
			GetSet_SetNumber( ImpSession, TextureImportStateIds.EVENTID, progressEventId )
			GetSet_SetNumber( ImpSession, TextureImportStateIds.EVENTOBJID, ImpSessionId )
			GetSet_SetNumber( ImpSession, TextureImportStateIds.TARGETTYPE, target.type )
			GetSet_SetNumber( ImpSession, TextureImportStateIds.TARGETID, target.id )
			GetSet_SetNumber( ImpSession, TextureImportStateIds.TARGETPOS_X, target.pos.x )
			GetSet_SetNumber( ImpSession, TextureImportStateIds.TARGETPOS_Y, target.pos.y )
			GetSet_SetNumber( ImpSession, TextureImportStateIds.TARGETPOS_Z, target.pos.z )

			if keyState==KeyStates.SHIFT then
				GetSet_SetNumber( ImpSession, TextureImportStateIds.USEIMPORTDLG, 0 )
			else
				GetSet_SetNumber( ImpSession, TextureImportStateIds.USEIMPORTDLG, 1 )
			end

			UGC_StartImportAsync( ImpSessionId )
		else
			LogError( "CustomizeObjectTextureAsync: create import session failed" )
			KEP_MessageBox( "Error importing image file. Please restart Kaneva client and try again.", "Import failed" )

		end
	else
		Log( "CustomizeObjectTextureAsync: Target is not customizable - name= " .. target.name .. " type= " .. ObjectTypeString(target)                                                                              )
		KEP_MessageBox( "Cannot customize this object with dropped image.", "Message" )
	end
end

function TextureImportProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local ImpSessionId = -1
	local ImpSession = nil
	local fullPath = ""

	if filter==UGCIS.DESTROYING or filter==UGCIS.DESTROYED then
		-- ImpSession might has been destroyed at this time, there is nothing we can do about this event.
		return
	end

	if objectid~=nil then
		ImpSessionId = objectid
		ImpSession = UGC_GetImportSession( ImpSessionId )
		if ImpSession~=nil then
			fullPath = GetSet_GetString( ImpSession, TextureImportStateIds.SOURCEIMAGEPATH )
		end
	end

	if filter==UGCIS.PREVIEWED or filter==UGCIS.IMPORTED or filter==UGCIS.CONVERTED then

		-- tell state machine to continue import process
		UGC_ContinueImportAsync( ImpSessionId )

	elseif filter==UGCIS.PROCESSED then
		local internalError = true
		if ImpSession~=nil then

			-- apply imported texture to target object
			local targetType = GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETTYPE )
			local targetObjId= GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETID )
			local targetX = GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETPOS_X )
			local targetY = GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETPOS_Y )
			local targetZ = GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETPOS_Z )
			local convertedImagePath = GetSet_GetString( ImpSession, TextureImportStateIds.CONVERTEDIMAGEPATH )

			if fullPath==nil then
				LogError( "TextureImportProgressEventHandler: Error occurred after post-processing image import, file path is null" )
				KEP_MessageBox( "Error importing image file.\n\n[code: -2]", "Import failed" )
			else
				if targetType==ObjectType.DYNAMIC then
					KEP_ApplyCustomTextureLocal( targetObjId, convertedImagePath )
					internalError = false
				elseif (targetType==ObjectType.WORLD) then
					KEP_ApplyCustomTextureWldObjectLocal( targetObjId, convertedImagePath )
					internalError = false
				elseif targetType==ObjectType.UNKNOWN then
					-- Import request from other modules
					internalError = false
				else
					LogError( "TextureImportProgressEventHandler: Error occurred after post-processing image import, unknown target type: " .. ObjectTypeNumberValueString(targetType) )
					KEP_MessageBox( "Error importing image file.\n\n[code: -1]", "Import failed" )
				end
			end

			if not internalError and targetType~=ObjectType.UNKNOWN then

				-- submit image to server
				Log( "TextureImportProgressEventHandler: Submit custom texture targetType=" .. ObjectTypeNumberValueString(targetType) .. " objId=" .. targetObjId )

				g_ugcType = UGC_TYPE.INVALID
				if targetType==ObjectType.DYNAMIC then
					g_ugcType = UGC_TYPE.DYNOBJ_CUSTOMIZATION
				elseif (targetType==ObjectType.WORLD) then
					g_ugcType = UGC_TYPE.ZONEOBJ_CUSTOMIZATION
				else
					LogError( "TextureImportProgressEventHandler: unsupported targetType=" .. ObjectTypeNumberValueString(targetType) )
				end

				if g_ugcType~=UGC_TYPE.INVALID then
					g_ugcObjId = targetObjId
					UGCI.SubmitUGCObj( g_ugcType, g_ugcObjId, 0, "", "", 0, 0 )
				end
			else
				MenuCloseThis()
			end
		end

		if internalError then
			LogError("Import Failed - Internal Error")
			KEP_MessageBox( "Import failed due to an internal error.\n\n[code:5]", "Import failed" )
		end

		UGC_CleanupImportSessionAsync( ImpSessionId )

	elseif filter==UGCIS.PREVIEWFAILED then

		LogError( "TextureImportProgressEventHandler: Preview failed - path=" .. fullPath )
		KEP_MessageBox( "Error importing image file.\n\n[code: 1]", "Import failed" )
		UGC_CleanupImportSessionAsync( ImpSessionId )
		MenuCloseThis()

	elseif filter==UGCIS.PROCESSINGFAILED then

		LogError( "TextureImportProgressEventHandler: Post-processing failed - path=" .. fullPath )
		KEP_MessageBox( "Error importing image file\n\n[code: 4]", "Import failed" )
		UGC_CleanupImportSessionAsync( ImpSessionId )
		MenuCloseThis()

	elseif filter==UGCIS.IMPORTFAILED then

		LogError( "TextureImportProgressEventHandler: Import failed - path=" .. fullPath )
		KEP_MessageBox( "Unsupported image format use jpg instead.", "Import failed" )
		UGC_CleanupImportSessionAsync( ImpSessionId )
		MenuCloseThis()

	end
end

function UGCSubmissionFeedbackEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter==g_ugcType or objectid==g_ugcObjId then
		local sfe = UGCI.DecodeSubmissionFeedbackEvent( event, filter, objectid )
		if sfe.httpStatusCode==200 and sfe.result==SubmissionResult.SUCCEEDED then
			if sfe.uploadId~=nil then
				Log( "UGCSubmissionFeedbackEventHandler: Submission succeeded - uploadId=" .. sfe.uploadId )	-- no confirmation required, go ahead and upload
				UGCI.UploadUGCObj( 0, sfe.uploadId, sfe.ugcType, sfe.ugcObjId, sfe.objName )		-- 0: not a derivation
			end
		end
		g_ugcType = nil	-- reset pending ID
		g_ugcObjId = nil
		return KEP.EPR_CONSUMED
	end
	return KEP.EPR_OK
end

function UploadCompletionHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local isDerivative = KEP_EventDecodeNumber( event )
	local ugcType = KEP_EventDecodeNumber( event )
	local uploadId = KEP_EventDecodeString( event )
	local ugcObjId = KEP_EventDecodeNumber( event )
	local ugcObjName = KEP_EventDecodeString( event )
	if ugcType==UGC_TYPE.DYNOBJ_CUSTOMIZATION or ugcType==UGC_TYPE.ZONEOBJ_CUSTOMIZATION then
		UGCI.DeployUGCUpload( uploadId, ugcType, ugcObjId )
	end
end

function UGCDeployCompletionHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local ugcType = KEP_EventDecodeNumber( event )
	local ugcId = KEP_EventDecodeNumber( event )
	local resultText = KEP_EventDecodeString( event )		-- padded by WebCallTask
	local httpStatusCode = KEP_EventDecodeNumber( event )	-- padded by WebCallTask

	if httpStatusCode==200 then

		local s, e = 0      -- start and end of captured string
		local sResult = nil
		local sURL = nil
		s, e, sResult = string.find( resultText, "<ReturnCode>(-*%d+)</ReturnCode>")
		s, e, sAssetId = string.find( resultText, "<AssetId>(%d+)</AssetId>")
		s, e, sURL = string.find(resultText, "<TextureUrl>(.+)</TextureUrl>")
		local assetId = -1
		if sAssetId~=nil and sAssetId~="" then
			assetId = tonumber( sAssetId )
		end

		local uploadEvent = KEP_EventCreate( "PhotoUploadEvent" )
		KEP_EventEncodeNumber( uploadEvent, assetId )
		KEP_EventQueueInFuture( uploadEvent, 1 )

		if sResult~=nil then

			local result = tonumber(sResult)

			if result==SubmissionResult.SUCCEEDED and assetId~=-1 then

				if ugcType==UGC_TYPE.DYNOBJ_CUSTOMIZATION then
					-- update pattern on dynamic object
					KEP_ApplyCustomTexture( ugcId, assetId, 0, sURL )

				elseif ugcType==UGC_TYPE.ZONEOBJ_CUSTOMIZATION then
					-- update pattern on zone object
					KEP_ApplyCustomTextureWldObject( ugcId, assetId, 0)

					-- Additional processing for zone objects: make texture change permanent
					KEP_SaveWldObjectChanges( gTargetInfo.id, sURL )
				else
					KEP_MessageBox( "Unknown background operation: scope=UGC, action=deploy, type=" .. ugcType .. ", ID=" .. ugcId )
				end
			else
				if ugcType==UGC_TYPE.DYNOBJ_CUSTOMIZATION or ugcType==UGC_TYPE.ZONEOBJ_CUSTOMIZATION then
					KEP_MessageBox( "Unable to add image to your media library, please try again." )
				else
					KEP_MessageBox( "Background processing failed: scope=UGC, action=deploy, type=" .. ugcType .. ", ID=" .. ugcId )
				end
			end
		end
	end

	MenuCloseThis()
end
