--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("MenuHelper.lua")


local m_teleportURL = nil
local m_imagePath = ""
local m_teleportPID = nil

function onCreate()

	Events.registerHandler("UPDATE_WORLD_TELEPORTER", updateTeleporterMenu)
	KEP_EventRegisterHandler( "textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)

end

function btnGo_OnButtonClicked(btnHandle)
	if m_teleportURL then
		gotoURL(m_teleportURL)
		MenuCloseThis()
	else
		Events.sendEvent("FRAMEWORK_GOTO_LINKED_ZONED", {PID = m_teleportPID})
	end
end

----------------------------
-- Event Handlers
---------------------------
function updateTeleporterMenu(event)
	--world to world
	if event.placeInfo then
		local placeInfo = event.placeInfo
		
		local s, e, worldname = string.find(placeInfo, "<name>(.-)</name>")
		s, e, m_teleportURL = string.find(placeInfo, "<url>(.-)</url>")
		local s, e, fileName = string.find(placeInfo, "<fileName>(.-)</fileName>")
		local s, e, dbPath = string.find(placeInfo, "<dbPath>(.-)</dbPath>")

		
		local path = KEP_DownloadTextureAsyncToGameId(fileName, dbPath)

		local gameId = KEP_GetGameId()

		path = string.gsub(path,".-\\", "" )
		path = "../../CustomTexture/".. gameId .. "/" .. path
		m_imagePath = path

		Static_SetText(gHandles["stcWorld"], worldname)
		Static_SetText(gHandles["stcHeader"], "Travel to ".. worldname)
	--linked
	elseif event.zoneInfo then
		local zoneInfo = event.zoneInfo
		m_teleportPID = event.PID

		local fileName = tostring(split(GetFilenameFromPath(zoneInfo.thumbnail), ".")[1])
		fileName = zoneInfo.zoneID .. "_" .. fileName .. CUSTOM_TEXTURE_THUMB
					
		KEP_DownloadTextureAsyncToGameId( fileName, zoneInfo.thumbnail )
		
		local gameId = KEP_GetGameId()
		fileName = "../../CustomTexture/".. gameId .. "/" .. fileName

		m_imagePath = fileName

		Static_SetText(gHandles["stcWorld"], zoneInfo.name)
		Static_SetText(gHandles["stcHeader"], "Travel to ".. zoneInfo.name)
	end
end

function textureDownloadHandler()
	local displayImage =  Image_GetDisplayElement(gHandles["imgWorld"])
	Element_AddTexture(displayImage, gDialogHandle, m_imagePath)
	scaleImage(gHandles["imgWorld"], gHandles["imgWorldBG"], m_imagePath)
end