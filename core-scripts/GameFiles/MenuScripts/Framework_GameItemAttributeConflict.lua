--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_GameItemAttributeConflict.lua
-- 
-- Warns users of an impending name conflict on save from
-- Framework_GameItemEditor
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

local itemType = ""
-- When the menu is created
function onCreate()
	-- Register client - client event handlers
	KEP_EventRegisterHandler("attributeConflictMessageHandler", "FRAMEWORK_ATTRIBUTE_CONFLICT_MESSAGE", KEP.HIGH_PRIO)
end

-- Handles name conflict message from Framework_GameItemEditor
function attributeConflictMessageHandler(dispatcher, fromNetid, event, eventID, filter, objectID)
	local error = KEP_EventDecodeString(event)

	Static_SetText(gHandles["stcError"], tostring(error))
	Control_SetVisible(gHandles["stcError"], true)
	itemType = KEP_EventDecodeString(event)
end


function btnNew_OnButtonClicked(buttonHandle)
MenuOpen("Framework_GameItemEditor.xml")

	local itemInfo = Events.encode({name = "New Item", itemType = itemType})

	local event = KEP_EventCreate("EDIT_GAME_ITEM_INFO")
	KEP_EventEncodeString(event, 0)
	KEP_EventEncodeString(event, itemInfo) -- default item info
	KEP_EventEncodeString(event, "New Item") -- Editor title
	KEP_EventEncodeString(event, itemType) -- prefabType
	KEP_EventEncodeString(event, "newItem")	-- menu mode
	KEP_EventQueue(event)
	MenuCloseThis()
end