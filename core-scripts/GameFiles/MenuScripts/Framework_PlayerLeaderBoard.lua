--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_PlayerLeaderboard.lua
-------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
-- dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_InventoryHelper.lua")

dofile("MenuHelper.lua")
dofile("CreditBalances.lua")
-- dofile("CT_MenuConstants.lua")

g_userID = 0
g_filename = ""

-- TO DO: Sort leaderboard after all the elements are added (sort as it's adding, change display methodology)
local m_leaderboard = {} -- Local copy of flag info (including score)
local m_flagCount = 0 -- Number of flags in the m_leaderboard list
local INDEX_DNE = -1
local DISPLAY_MODES = {LEADERBOARD_NOW = 0, LEADERBOARD_PRE = 1}
local m_displayMode = DISPLAY_MODES.LEADERBOARD_NOW
local m_playerIcons = {}
local m_preWeekString = ""
local m_leaderboardGenerated = false

local LIST_ELEMENTS = { "stcListRank", "stcListPlace", "btnMembersList", "stcListMembers", "stcListScore", "imgMemberStar","imgListPositionBG"}
local REWARD_ITEMS = { {UNID = 140, quantity = 10000}, {UNID = 140, quantity = 2000},{UNID = 140, quantity = 1000}}

local m_leaderList = {}
local m_rewards = {}
local m_gameItems = {} -- Added for the game item cache, keys are UNID as a string

local LIST_ROW_HEIGHT = 30 -- List height properties

local LIST_ANCHOR_POINT = {x = 24, y = 225} -- Anchor point for attaching properties

-- Endzones and height of window
local LIST_ENDZONE_TOP = 200
local LIST_ENDZONE_BOTTOM = 525
local LIST_HEIGHT = 302

local MENU_INSERT_INDEX = 1 -- Control insert index

-- Scroll bar measurements
local SCROLL_BAR_WIDTH = 16
local SCROLL_BAR_MAX = 271
local SCROLL_BAR_TOP = 240
local SCROLL_MOVE_DISTANCE = 5

--Member list measurements
local MEMBER_AVAILABLE_HEIGHT = 90
local MEMBER_NAME_HEIGHT =  50
local MEMBER_BACKGROUND_OFFSET_X = 5
local MEMBER_BACKGROUND_OFFSET_Y = 10
local MEMBER_IMAGE_OFFSET = 21
local MEMBER_NAME_OFFSET = 46
local MAX_MEMBERS = 5

local PROPERTY_BACKGROUND_COLORS = {{a=255, r=230, g=230, b=230}, {a=255, r=255, g=255, b=255}}
local RANK_COLORS = {{a=255, r=255, g=162, b=00}, {a=255, r=139, g=146, b=185}, {a=255, r=196, g=142, b=88}}

local m_playerName
local m_zoneInstanceId
local m_zoneType

local m_listAnchor = LIST_ANCHOR_POINT.y -- Current property/category insertion location
local m_insertLocation = 0
local m_listHeight = 0 -- Total height of the properties
local m_scrollBarMod = nil -- Scroll bar position distance modifier
local m_barScrolling = false -- Is the bar scrolling?
local m_scrollPrevPos = nil -- Scroll bar previous position
local m_memberNumber = 0 -- Count members
local m_propertyRows = 0 -- Used to count rows to set colors
local m_allFlags = {}

-- Default picture for profile
g_defaultIcon = ""
local DEFAULT_AVATAR_PIC_PATH_SQ = "images/KanevaIconMaleSquare.jpg" -- defaults to standard male silhouette

local m_currTime = 0
local SECONDS_PER_DAY = 86400
local DAY_NAMES = {"Thursday", "Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday"}

-- When the menu is created
function onCreate()

	-- Client event handlers
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("zoneNavigationEventHandler", "ZoneNavigationEvent", KEP.MED_PRIO)
	Events.registerHandler("LAST_CLAIM_FLAG_DELETED", lastClaimFlagDeletedHandler)
	
	Events.registerHandler("RETURN_CURRENT_TIME", onReturnCurrentTime)

	-- Register the download texture events
	KEP_EventRegisterHandler("textureDownloadHandlerLB", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	g_defaultIcon = downloadImage(DEFAULT_AVATAR_PIC_PATH_SQ, "default_leader")

	-- Server event handlers
	Events.sendEvent("REQUEST_ALL_FLAGS")
	Events.registerHandler("FLAG_GROUP_INFO_RETURNED", flagGroupInfoReturnedHandler)

	-- Get User's Profile
	-- g_username = KEP_GetLoginName()
	-- getUserID(g_username, WF.HUD_USER_ID)
	-- KEP_EventRegisterHandler( "textureDownloadHandler", "LooseTextureDownloadEvent", KEP.MED_PRIO)

	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	m_zoneInstanceId = tostring(KEP_GetZoneInstanceId())
	m_zoneType       = tostring(KEP_GetZoneIndexType())

	--Needed to retrieve info
	local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=getLeaderboard&zoneInstanceId="..tostring(m_zoneInstanceId).."&zoneType="..tostring(m_zoneType)
	makeWebCall(address, WF.LEADERBOARD_GET)

	requestInventory(GAME_PID)
	local ev = KEP_EventCreate("ZoneNavigationEvent")
	KEP_EventSetFilter(ev, ZONE_NAV.CURRENT_URL_REQUEST)
	KEP_EventQueue(ev)

	fillData()
	m_insertLocation = m_listAnchor
	m_playerName = KEP_GetLoginName()

	-- local timestamp = KEP_CurrentTime()
	-- timestamp = timestamp * 1000
	-- m_preWeekString = convertTime(timestamp)
	Events.sendEvent("FRAMEWORK_REQUEST_CURRENT_TIME", {playerName = KEP_GetLoginName(), PID = m_containerPID})

	updateList()

	_G["btnLastWeek_OnButtonClicked"] = function(btnHandle)
		toggleMode()
	end

end

-- Note: will not return accurate results between Sunday 9:01pm EST and 12:00am Monday
function getLastWeekString(timestamp)
	local tempTime = timestamp
	local dayOfWeek = DAY_NAMES[math.fmod(math.floor(tempTime/SECONDS_PER_DAY), 7)+1]
	if dayOfWeek ~= "Sunday" then
		tempTime = getLastWeek(tempTime, dayOfWeek)
	end

	local tempYear   = math.floor((tempTime / 31556926) + 1970) -- date serials start at 1/1/1970
	local tempMonth  = math.floor((math.fmod(tempTime, 31556926) / 2629743) + 1)  -- add 1 0 based
	local tempDay	 =  math.floor(math.fmod(tempTime, 2629743) / 86400)
	-- local tempHour   = math.floor(math.fmod(tempTime, 86400) / 3600)
	-- local tempMin = math.floor(math.fmod(tempTime , 3600) / 60 )
	-- local tempSec = math.floor(math.fmod(tempTime, 60))
	
	return tempMonth.."\/"..tempDay.."\/"..tempYear -- "1/31/2016"
end

getLastWeek = function(timestamp, dayOfWeek)
	local dayValues = {["Monday"] = 1, ["Tuesday"] = 2, ["Wednesday"] = 3, ["Thursday"] = 4, ["Friday"] = 5, ["Saturday"] = 6}
	local newTime =  timestamp - (86400 * (dayValues[dayOfWeek] + 7))
	return newTime
end

-- Note: will not return accurate results between Sunday 9:01pm EST and 12:00am Monday
getDaysRemainingString = function(timestamp)
	local tempTime = timestamp
	local dayOfWeek = DAY_NAMES[math.fmod(math.floor(tempTime/SECONDS_PER_DAY), 7)+1]
	local dayValues = {["Monday"] = 7, ["Tuesday"] = 6, ["Wednesday"] = 5, ["Thursday"] = 4, ["Friday"] = 3, ["Saturday"] = 2}

	local returnString = ""
	if dayOfWeek == "Sunday" then
		returnString = "Ends today"
	else
		returnString = "Ends in "..tostring(dayValues[dayOfWeek]).." days"
	end
	return returnString
end

function onReturnCurrentTime(event)
	m_currTime = event.timestamp
	if m_displayMode == DISPLAY_MODES.LEADERBOARD_PRE then
		Static_SetText(gHandles["stcWeek"], "For the week of "..tostring(getLastWeekString(m_currTime)))
	else
		Static_SetText(gHandles["stcEndDate"], tostring(getDaysRemainingString(m_currTime)))
	end
end

-- -- -- -- -- -- -- --
-- Dialog Functions  --
-- -- -- -- -- -- -- --

function Dialog_OnRender(dialogHandle, fElapsedTime)

	-- If scroll bar moved, then update list
	if m_scrollBarMod then
		updateListPosition(m_scrollBarMod)
	end

end

function Dialog_OnMouseMove(dialogHandle, x, y)
	-- If scrolling, then update list
	if m_barScrolling then
		scrollMod = (m_scrollPrevPos - y) * (m_listHeight/LIST_HEIGHT)
		
		scrollMod = round(scrollMod)

		updateListPosition(scrollMod)
		m_scrollPrevPos = y
	end
end

--------------------
-- Event Handlers --
--------------------

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)
	
	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
	displayRewards()
end

function zoneNavigationEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	if filter == ZONE_NAV.CURRENT_URL_INFO then
		local url = KEP_EventDecodeString(tEvent)
		local worldName = parseFriendlyNameFromURL(url)
		Static_SetText(gHandles["stcWorldName"], worldName)
	end
end

function flagGroupInfoReturnedHandler(event)
	local tempFlagInfo = {}
	-- Save values locally
	if event.flagInfo then
		tempFlagInfo.PID = event.flagInfo.PID
		tempFlagInfo.owner = event.flagInfo.owner
		-- flagInfo.raveCount = event.flagInfo.raveCount
		-- flagInfo.members = event.flagInfo.members
		-- flagInfo.ravers = event.flagInfo.ravers
		-- flagInfo.groupName = event.flagInfo.groupName
		table.insert(m_allFlags, tempFlagInfo)
	end
	
	-- If data received after m_leaderboard is initialized, be sure to update member list order for this flag in m_Leaderboard
	for i,v in pairs(m_leaderboard) do
		if v and v.PID and v.members then
			if v.PID == tempFlagInfo.PID then
				v.members = updateMemberListOrder(tempFlagInfo.PID, v.members)
			end
		end
	end
end

-- Catches WEB CALLS
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- If browser return for Location Data web call
	if filter == WF.LEADERBOARD_GET or filter == WF.LEADERBOARD_GET_PRE then
		local returnXML = KEP_EventDecodeString( tEvent )
		local returnCode = ParseXmlReturnCode(returnXML)
		local returnDescription = xmlGetString(returnXML, "ReturnDescription")

		local filterString = "<current_leaderboard>(.-)</current_leaderboard>"
		if filter == WF.LEADERBOARD_GET_PRE then
			filterString = "<final_leaderboard>(.-)</final_leaderboard>"
		end

		-- Parse for team members
		-- If no team members, don't add
		-- Remove adding from flag info return handler

		for item in string.gmatch(returnXML, filterString) do
			local tempFlagInfo = {}
			tempFlagInfo.PID = xmlGetInteger(item, "obj_placement_id")
			tempFlagInfo.name = xmlGetString(item, "name")
			tempFlagInfo.members = {} -- handle members here. also icons.
			tempFlagInfo.ranking = 0
			if filter == WF.LEADERBOARD_GET_PRE then
				tempFlagInfo.ranking = xmlGetString(item, "ranking")
			end
			local tempMembers = xmlGetString(item, "team_members")
			if tempMembers and tempMembers ~= "" then
				-- Get player names
				for member in string.gmatch(tempMembers, "<team_member>(.-)</team_member>") do
					local tempName = xmlGetString(member, "username")
					table.insert(tempFlagInfo.members, tempName)
					local tempID = xmlGetString(member, "user_id")
					local tempURL = xmlGetString(member, "thumbnail_square_path")
					if not checkForUsername(m_playerIcons, tempName) then
						local tempImageData = {username = tempName, user_id = tempID, url = tempURL, imagePath = ""}
						table.insert(m_playerIcons, tempImageData)
					end
				end
				-- Make sure the owner is the first entry
				tempFlagInfo.members = updateMemberListOrder(tempFlagInfo.PID, tempFlagInfo.members)

				-- Cache player icons (AFTER re-ordering list)
				for newMemberIndex,newMember in pairs(tempFlagInfo.members) do
					for member in string.gmatch(tempMembers, "<team_member>(.-)</team_member>") do
						local tempName = xmlGetString(member, "username")
						if tempName == newMember then
							local tempID = xmlGetString(member, "user_id")
							local tempURL = xmlGetString(member, "thumbnail_square_path")
							if not checkForUsername(m_playerIcons, tempName) then
								local tempImageData = {username = tempName, user_id = tempID, url = tempURL, imagePath = ""}
								table.insert(m_playerIcons, tempImageData)
							end
						end
					end
				end

			end

			tempFlagInfo.score = xmlGetInteger(item, "points")

			-- Add the flag to list if it does not exist already. Also check that members exist.
			tempIndex = getFlagIndex(m_leaderboard, tempFlagInfo.PID)
			if tempIndex == INDEX_DNE and tempFlagInfo.members[1] and tempFlagInfo.members[1] ~= "" then
				table.insert(m_leaderboard, tempFlagInfo)
				m_flagCount = m_flagCount + 1
				-- createList(m_flagCount)
				-- updateList()
			end
		end

		if not m_leaderboardGenerated then
			fillData()
			for i,v in pairs(m_leaderboard) do
				createList(i)
				updateList()
			end
			m_leaderboardGenerated = true
		end

		-- Start caching icons from m_playerIcons
		cachePlayerIcons()
	end
end

function lastClaimFlagDeletedHandler(event)
	MenuCloseThis()
end

---------------------
-- Button Handlers --
---------------------

-- ======= Scroll bar input handling ======= --
function btnScrollBar_OnLButtonDown(buttonHandle, x, y)
	m_barScrolling = true
	m_scrollPrevPos = y
end

function btnScrollBar_OnLButtonUp(controlHandle, x, y)
	m_scrollBarMod = nil
	m_scrollPrevPos = nil
	m_barScrolling = false
	updateScrollBar()
end

function btnScrollBar_OnMouseLeave(controlHandle)
	m_scrollBarMod = nil
	m_scrollPrevPos = nil
	m_barScrolling = false
	updateScrollBar()
end

function btnScrollUp_OnLButtonDown(buttonHandle)
	m_scrollBarMod = 1 * SCROLL_MOVE_DISTANCE
	Control_SetFocus( gHandles["btnScrollUp"] )
end

function btnScrollUp_OnLButtonUp(controlHandle, x, y)
	m_scrollBarMod = nil
end

function btnScrollUp_OnMouseLeave(controlHandle)
	m_scrollBarMod = nil
end

function btnScrollDown_OnLButtonDown(buttonHandle)
	m_scrollBarMod = -1 * SCROLL_MOVE_DISTANCE
	Control_SetFocus( gHandles["btnScrollDown"] )
end

function btnScrollDown_OnLButtonUp(controlHandle, x, y)
	m_scrollBarMod = nil
end

function btnScrollDown_OnMouseLeave(controlHandle)
	m_scrollBarMod = nil
end
-- ======= Scroll bar input handling ======= --

--Updates list control positions
function updateListPosition(scrollMod)
	local x, y
	local itemControl	

	if m_listHeight < LIST_HEIGHT then
		scrollMod = 0
	elseif (m_listAnchor + scrollMod) > LIST_ANCHOR_POINT.y then
		scrollMod = LIST_ANCHOR_POINT.y - m_listAnchor
	elseif (m_listAnchor + scrollMod) + m_listHeight < LIST_ANCHOR_POINT.y + LIST_HEIGHT then
		scrollMod = (LIST_ANCHOR_POINT.y + LIST_HEIGHT) - m_listHeight - m_listAnchor
	end

	if scrollMod ~= 0 then
		m_listAnchor = m_listAnchor + scrollMod

		for i=1,#m_leaderList do
			local baseCategoryControl = nil
			local baseCategoryLocation = {}

			for j=1,#m_leaderList[i] do			
				local controlName = m_leaderList[i][j]
				itemControl = gHandles[controlName]
				y = Control_GetLocationY( itemControl ) + scrollMod
				Control_SetLocationY( itemControl, y )
				Control_SetVisible( itemControl, checkEndzone(y))
			end
		end

		updateScrollBar()
	end
end

-- Create the property row elements
function createList(categoryIndex)
	--Duplicate property controls based on item property type
	local listItem = {}

	for i=1, #LIST_ELEMENTS do
		local control = Dialog_GetControl( gDialogHandle, LIST_ELEMENTS[i])
		local newControl = copyControl(control)
		local newControlName = Control_GetName( newControl )

		local itemRank = 0
		if m_displayMode == DISPLAY_MODES.LEADERBOARD_NOW then
			itemRank = categoryIndex
		else
			itemRank = tonumber(m_leaderboard[categoryIndex].ranking)
		end
		local topThree = false
		if itemRank >= 1 and itemRank <= 3 then
			topThree = true
		end

		--Set property Title
		if string.find(newControlName, "stcListRank") then
			local rankText = tostring(itemRank)
			if topThree then
                BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(newControl)), 0, RANK_COLORS[itemRank])
			end		
			Static_SetText(newControl, tostring(rankText))
		elseif string.find(newControlName, "stcListPlace") then
			local placeText = m_leaderboard[categoryIndex].name
			if topThree then 
				BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(newControl)), 0, RANK_COLORS[itemRank])
			end
			Static_SetText(newControl, placeText)
		elseif string.find(newControlName, "stcListMembers") then
			local memberCountText = "members: "..tostring(#m_leaderboard[categoryIndex].members).."/5"
			if topThree then 
				BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(newControl)), 0, RANK_COLORS[itemRank])
			end
			Static_SetText(newControl, memberCountText)
		elseif string.find(newControlName, "stcListScore") then
			local scoreText = formatNumberCommas(tostring(m_leaderboard[categoryIndex].score))
			if topThree then 
				BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(newControl)), 0, RANK_COLORS[itemRank])
			end
			Static_SetText(newControl, scoreText)
		elseif string.find(newControlName, "imgListPositionBG") then
			if height then
				local width = Control_GetWidth( newControl )
				Control_SetSize( newControl, width, height )
			end

			local backgroundColor =  Element_GetTextureColor(Image_GetDisplayElement(gHandles[newControlName]))
			if m_propertyRows%2 == 0 then
				BlendColor_SetColor(backgroundColor, 0, PROPERTY_BACKGROUND_COLORS[1])
			else
				BlendColor_SetColor(backgroundColor, 0, PROPERTY_BACKGROUND_COLORS[2])
			end
			
			m_propertyRows = m_propertyRows + 1

		elseif string.find(newControlName, "imgMemberStar") then
			local noFlag = true
			for j = 1, #m_leaderboard[categoryIndex].members do
				if m_leaderboard[categoryIndex].members[j] == m_playerName then
					Static_SetText(gHandles["stcMemberMessage"], "You are a member of this place ")
					Control_SetLocation(gHandles["imgMemberMessageStar"], 230, Control_GetLocationY(gHandles["imgMemberMessageStar"]))
					noFlag = false
				end
			end
			if noFlag then 
				applyTextureToElement(Image_GetDisplayElement(gHandles[newControlName]), "Leaderboard_men_star.tga", 20, 0, 38, 18, COLOR_WHITE)
			end

		elseif string.find(newControlName, "btnMembersList") then

			_G[newControlName .. "_OnMouseEnter"] = function(btnHandle)
				displayMembers(categoryIndex, i)
			end

			_G[newControlName .. "_OnMouseLeave"] = function(btnHandle)
				hideMembers()
			end

		end

		table.insert(listItem, newControlName)
	end

	movePropertiesToList(listItem, categoryIndex)
end

--Move a set of properties to main list
function movePropertiesToList(listItem, categoryIndex)
	
	moveControlSet(listItem)

	table.insert(m_leaderList, listItem)
end

--Relocate control set based on list location
function moveControlSet(controlSet)
	local baseControl = nil
	local baseLocation = {}
	for _,controlName in pairs(controlSet) do
		if baseControl == nil then
			baseControl = controlName
			baseLocation.x = Control_GetLocationX( gHandles[controlName] )
			baseLocation.y = Control_GetLocationY( gHandles[controlName] )
			Control_SetLocation( gHandles[controlName], LIST_ANCHOR_POINT.x, m_insertLocation )
			
			m_insertLocation = m_insertLocation + Control_GetHeight( gHandles[controlName] )

		else
			local xDiff = baseLocation.x - Control_GetLocationX( gHandles[controlName] )
			local yDiff = baseLocation.y - Control_GetLocationY( gHandles[controlName] )
			local newX = Control_GetLocationX( gHandles[baseControl] ) - xDiff
			local newY = Control_GetLocationY( gHandles[baseControl] ) - yDiff

			Control_SetLocation( gHandles[controlName],newX, newY )
		end
	end
end

--Update list's control location
function updateList()
	local indexInsert = MENU_INSERT_INDEX
	m_insertLocation = m_listAnchor
	m_listHeight = 0
	for i=1,#m_leaderList do
		local baseCategoryControl = nil
		local baseCategoryLocation = {}
		local indexOffset = 0

		for j=1,#m_leaderList[i] do
			local controlName = m_leaderList[i][j]
			if baseCategoryControl == nil then
				baseCategoryControl = controlName
				baseCategoryLocation.x = Control_GetLocationX( gHandles[controlName] )
				baseCategoryLocation.y = Control_GetLocationY( gHandles[controlName] )

				Control_SetLocation( gHandles[controlName], LIST_ANCHOR_POINT.x, m_insertLocation )
				m_insertLocation = m_insertLocation + LIST_ROW_HEIGHT
				m_listHeight = m_listHeight + LIST_ROW_HEIGHT
			else
				local xDiff = baseCategoryLocation.x - Control_GetLocationX( gHandles[controlName] )
				local yDiff = baseCategoryLocation.y - Control_GetLocationY( gHandles[controlName] )
				local newX = Control_GetLocationX( gHandles[baseCategoryControl] ) - xDiff
				local newY = Control_GetLocationY( gHandles[baseCategoryControl] ) - yDiff
				Control_SetLocation( gHandles[controlName],newX, newY )
			end

			Dialog_MoveControlToIndex( gDialogHandle, gHandles[controlName], 1 )
		end
	end
	updateScrollBar()
	checkVisibility()
end

--Update the size and location of the scroll bar
function updateScrollBar()
	local listPer = math.min(LIST_HEIGHT/m_listHeight, 1)
	local barHeight = listPer * SCROLL_BAR_MAX

	local buttonWidth = m_barScrolling and SCROLL_BAR_WIDTH + 200 or SCROLL_BAR_WIDTH
	local buttonHeight = m_barScrolling and barHeight + 50 or barHeight	

	Control_SetSize(gHandles["imgScrollBar"], SCROLL_BAR_WIDTH, barHeight)
	Control_SetSize(gHandles["btnScrollBar"], buttonWidth, buttonHeight)

	local listDiff = LIST_ANCHOR_POINT.y - m_listAnchor
	local barDiff = SCROLL_BAR_MAX - barHeight

	local barPosX = Control_GetLocationX(gHandles["imgScrollBar"])
	local barPosY = SCROLL_BAR_TOP + math.ceil((barHeight + barDiff) * (listDiff/m_listHeight))

	local buttonBarPosX = m_barScrolling and barPosX - 100 or barPosX
	local buttonBarPosY = m_barScrolling and barPosY - 25 or barPosY	

	Control_SetLocation( gHandles["imgScrollBar"], barPosX, barPosY )
	Control_SetLocation( gHandles["btnScrollBar"], buttonBarPosX, buttonBarPosY )
end

--Copy and paste controls
function copyControl(menuControl)
	if menuControl then
		Dialog_CopyControl( gDialogHandle, menuControl )
		Dialog_PasteControl( gDialogHandle )

		local numControls = Dialog_GetNumControls( gDialogHandle )

		local newControl = Dialog_GetControlByIndex( gDialogHandle, numControls - 1 )
		local controlName = Control_GetName( newControl )

		gHandles[controlName] = newControl	

		return newControl
	end
end

--Check the position of the list
function checkPosition()
	if m_listHeight < LIST_HEIGHT then
		jumpToTop()
	elseif m_listAnchor < LIST_ANCHOR_POINT.y - (m_listHeight - LIST_HEIGHT) then
		jumpToBottom()		
	end
end

--Check whether a property should be hidden
function checkVisibility()
	for i=1,#m_leaderList do
		local baseCategoryControl = nil
		local baseCategoryLocation = {}

		for j=1,#m_leaderList[i] do			
			local controlName = m_leaderList[i][j]
			if baseCategoryControl == nil then
				baseCategoryControl = controlName
				baseCategoryLocation.y = Control_GetLocationY( gHandles[controlName] )
			end
			Control_SetVisible( gHandles[controlName], checkEndzone(baseCategoryLocation.y) )
		end
	end	
end

--Check the location of a control against the top and bottom endzone masks
function checkEndzone(controlLocation, controlHeight)
	if controlLocation < LIST_ENDZONE_TOP then
		return false
	elseif controlLocation > LIST_ENDZONE_BOTTOM then
		return false
	else
		return true
	end
end

--Jump list to the top
function jumpToTop()
	m_listAnchor = LIST_ANCHOR_POINT.y
	updateList()
end

--Jump list to the bottom
function jumpToBottom()
	m_listAnchor = LIST_ANCHOR_POINT.y - (m_listHeight - LIST_HEIGHT)
	updateList()
end

--Jump list to a given point
function jumpToPoint(point)
	m_listAnchor = LIST_ANCHOR_POINT.y - (point - LIST_HEIGHT)
	updateList()
end

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function fillData()
	table.sort(m_leaderboard, customSort)
end

function customSort(a, b)
	return a.score > b.score
end

function displayRewards()
	for i = 1, #REWARD_ITEMS do
		Static_SetText(gHandles["stcPrize"..i], formatNumberCommas(tostring(REWARD_ITEMS[i].quantity)).." ".. tostring(m_gameItems[tostring(REWARD_ITEMS[i].UNID)].name).."s")
		applyImage(gHandles["imgPrize"..i], m_gameItems[tostring(REWARD_ITEMS[i].UNID)].GLID)
	end
end

function applyTextureToElement(element, textureName, x, y, x2, y2, color)
	Element_AddTexture(element, gDialogHandle, textureName)
	Element_SetCoords(element, x, y, x2, y2)
	local blendColor = Element_GetTextureColor(element)
end

function displayMembers(categoryIndex, index)	
	local member = false

	for i,v in pairs (m_leaderboard[categoryIndex].members) do
		if m_leaderboard[categoryIndex].members[j] == m_playerName then
			member = true
		end
	end

	local backgroundHeight  = MEMBER_NAME_HEIGHT * #m_leaderboard[categoryIndex].members
	if member == false and  #m_leaderboard[categoryIndex].members < 5 then 
		backgroundHeight = backgroundHeight + MEMBER_AVAILABLE_HEIGHT
	end
	local backgroundCtrl = gHandles["imgMemberList"]
	local availableCtrl = gHandles["stcAvailable"]
	local buttonCtrl = gHandles["btnMembersList"..categoryIndex]

	Control_SetVisible(backgroundCtrl, true)
	Control_SetSize(backgroundCtrl, Control_GetWidth(backgroundCtrl), backgroundHeight)

	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	local menuLocataion = MenuGetLocationThis()

	if Control_GetLocationY(buttonCtrl) + backgroundHeight  + menuLocataion.y < screenHeight then
		Control_SetLocation(backgroundCtrl, Control_GetLocationX(buttonCtrl) - Control_GetWidth(backgroundCtrl) + MEMBER_BACKGROUND_OFFSET_X, Control_GetLocationY(buttonCtrl))
	else
		Control_SetLocation(backgroundCtrl,Control_GetLocationX(buttonCtrl) - Control_GetWidth(backgroundCtrl) + MEMBER_BACKGROUND_OFFSET_X, Control_GetLocationY(buttonCtrl) - Control_GetHeight(backgroundCtrl) + MEMBER_BACKGROUND_OFFSET_Y)
	end

	local locX = Control_GetLocationX(backgroundCtrl) + MEMBER_IMAGE_OFFSET
	local locY = MEMBER_BACKGROUND_OFFSET_Y + Control_GetLocationY(backgroundCtrl)

	for i = 1, #m_leaderboard[categoryIndex].members do
	
		local memberImageCtrl = gHandles["imgMember"..i]
		local memberImageCtrlBG = gHandles["imgMemberBG"..i]
		local ownerCtrl = gHandles["stcFlagOwner"]
		local memberNameCtrl = gHandles["stcFlagMember"..(i-1)]

		-- Get the name of the member
		local tempMemberName = m_leaderboard[categoryIndex].members[i]
		-- Check that the member is in the icon list
		if checkForUsername(m_playerIcons, tempMemberName) then
			-- Check that the member 
			local tempNameIndex = getUsernameIndex(m_playerIcons, tempMemberName)
			local tempPath = m_playerIcons[tempNameIndex].imagePath
			-- Check that a cached image exists
			if tempPath ~= "" then
				-- Scale the image
				scaleImage(gHandles["imgMember"..tostring(i)], gHandles["imgMemberBG"..tostring(i)], tempPath)
			end
		end

		if i == 1 then
			Static_SetText(ownerCtrl, m_leaderboard[categoryIndex].members[i].." (Owner)")
			Control_SetLocation(ownerCtrl, locX + MEMBER_NAME_OFFSET, locY)
			Control_SetVisible(ownerCtrl, true)
		else
			Static_SetText(memberNameCtrl, m_leaderboard[categoryIndex].members[i])
			Control_SetLocation(memberNameCtrl, locX + MEMBER_NAME_OFFSET, locY)
			Control_SetVisible(memberNameCtrl, true)
		end	

		-- downloadImage(DEFAULT_AVATAR_PIC_PATH_SQ, "default_leader")

		Control_SetLocation(memberImageCtrlBG, locX, locY)
		Control_SetVisible(memberImageCtrlBG, true)
		Control_SetLocation(memberImageCtrl, locX, locY)
		Control_SetVisible(memberImageCtrl, true)

		locY = locY + MEMBER_NAME_HEIGHT
		m_memberNumber = m_memberNumber + 1

	end

	local memberCount = #m_leaderboard[categoryIndex].members
	if memberCount < MAX_MEMBERS then
		locY = MEMBER_BACKGROUND_OFFSET_Y + Control_GetLocationY(backgroundCtrl) + (MEMBER_NAME_HEIGHT * memberCount)
		if not checkForEntry(m_leaderboard[categoryIndex].members, m_playerName) then
			Static_SetText(gHandles["stcAvailable"], "Apply for membership to this Place by going to it and clicking on its 2D screen icon and then Raving it!")
			Control_SetVisible(gHandles["stcAvailable"], true)
			Control_SetLocation(gHandles["stcAvailable"], locX, locY)
		else
			Control_SetVisible(gHandles["stcAvailable"], false)
		end
		
	end
end

function hideMembers()
	local backgroundCtrl = gHandles["imgMemberList"]
	--local availableCtrl = gHandles["stcAvailable"]
	--local buttonCtrl = gHandles["btnMembersList"..categoryIndex]

	Control_SetVisible(backgroundCtrl, false)
	Control_SetVisible(gHandles["stcAvailable"], false)

	for i = 1, m_memberNumber do
	-- for i,v in pairs m_members do
		-- nil check here (bc member size varies)
		Control_SetVisible(gHandles["imgMember"..i], false)
		Control_SetVisible(gHandles["imgMemberBG"..i], false)
		if i < 5 then
			Control_SetVisible(gHandles["stcFlagMember"..i], false)
		end
			Control_SetVisible(gHandles["stcFlagOwner"], false)
		--locY = locY + MEMBER_NAME_HEIGHT
	end
	m_memberNumber = 0
end

toggleMode = function()
	if m_displayMode == DISPLAY_MODES.LEADERBOARD_NOW then
		enablePastDisplay()
	else
		enableCurrentDisplay()
	end
end

enablePastDisplay = function() -- TO DO: Cache this locally so that it does not need to be requested each time

	resetLeaderboard()
	local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=getLeaderboardFinal&zoneInstanceId="..tostring(m_zoneInstanceId).."&zoneType="..tostring(m_zoneType)
	makeWebCall(address, WF.LEADERBOARD_GET_PRE)

	m_displayMode = DISPLAY_MODES.LEADERBOARD_PRE
	Static_SetText(gHandles["stcColumn1"], "Final Rank")
	Static_SetText(gHandles["btnLastWeek"], "Back to Current Standings")
	if m_currTime > 0 then
		Static_SetText(gHandles["stcWeek"], "For the week of "..tostring(getLastWeekString(m_currTime)))
	end
	Control_SetVisible(gHandles["stcWorldName"], false)
	Control_SetVisible(gHandles["stcEndDate"], false)
	Control_SetVisible(gHandles["imgTopBar"], false)
	Control_SetVisible(gHandles["imgTopBar2"], true)
end

enableCurrentDisplay = function()
	
	resetLeaderboard()
	local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=getLeaderboard&zoneInstanceId="..tostring(m_zoneInstanceId).."&zoneType="..tostring(m_zoneType)
	makeWebCall(address, WF.LEADERBOARD_GET)

	m_displayMode = DISPLAY_MODES.LEADERBOARD_NOW
	Static_SetText(gHandles["stcColumn1"], "Rank")
	Static_SetText(gHandles["btnLastWeek"], "View Last Week's Winners")
	Static_SetText(gHandles["stcWeek"], "Weekly Leaderboards")
	Control_SetVisible(gHandles["stcWorldName"], true)
	if m_currTime > 0 then
		Control_SetVisible(gHandles["stcEndDate"], true)
		Static_SetText(gHandles["stcEndDate"], tostring(getDaysRemainingString(m_currTime)))
	end
	Control_SetVisible(gHandles["imgTopBar"], true)
	Control_SetVisible(gHandles["imgTopBar2"], false)
	
end

resetLeaderboard = function()
	hideOldControls() -- TO DO: Look into actually REMOVING these
	m_leaderList = {}

	m_leaderboard ={}
	m_playerIcons = {}
	m_flagCount = 0

	m_listAnchor = LIST_ANCHOR_POINT.y
	m_insertLocation = m_listAnchor
	m_listHeight = 0
	m_scrollBarMod = nil
	m_barScrolling = false
	m_scrollPrevPos = nil
	m_leaderboardGenerated = false

	updateList()

	Control_SetFocus(gHandles["btnScrollDown"])

end

--Check whether a property should be hidden
function hideOldControls()
	for i=1,#m_leaderList do
		for j=1,#m_leaderList[i] do			
			local controlName = m_leaderList[i][j]
			Control_SetVisible( gHandles[controlName], false )
		end
	end	
end

----------------------
-- Helper Functions --
----------------------

-- Returns whether a search item exists in a table
checkForEntry = function(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return true
		end
	end
	return false
end

-- Returns whether a search item exists in a table
checkForUsername = function(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v.username and v.username == searchItem then
			return true
		end
	end
	return false
end

-- Returns the index of a search item within a table (if any)
getUsernameIndex = function(inputTable, inputPID)
	for i,v in pairs(inputTable) do
		if v.username and tostring(v.username) == tostring(inputPID) then
			return i
		end
	end
	return -1
end

getFlagIndex = function(inputTable, inputPID)
	for i,v in pairs(inputTable) do
		if v.PID and tonumber(v.PID) == tonumber(inputPID) then
			return i
		end
	end
	return -1
end

checkForDuplicateFlag = function(searchTable, searchItem)
	for i,v in pairs(searchTable) do
		if v.PID == searchItem then
			return true
		end
	end
	return false
end

function formatNumberCommas(inputStr)
   if tonumber(inputStr) == nil then
       return inputStr
   end

   local formatted = inputStr
   while true do  
       formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
       if (k==0) then
           break
       end
   end
   return formatted
end

------------------
-- Player Icons --
------------------

cachePlayerIcons = function()

	for i,v in pairs(m_playerIcons) do
		cacheImage(i)
	end

end

cacheImage = function(inputIndex)

	local tempURL = m_playerIcons[inputIndex].url -- Might be empty. This is caught by extractImageName()
	-- if tempURL == nil or tempURL == "" then
	-- 	tempURL = DEFAULT_AVATAR_PIC_PATH_SQ
	-- end

	local tempUsername = m_playerIcons[inputIndex].username
	local imageName = extractImageName(tempURL)

	-- If we extracted a valid imageName
	if imageName then
		m_playerIcons[inputIndex].imagePath = downloadImage(tempURL, tostring(tempUsername).."_"..tostring(imageName))
	else
		-- Use default image
		if g_defaultIcon then
			m_playerIcons[inputIndex].imagePath = g_defaultIcon
		end
	end

	local tempImagePath = m_playerIcons[inputIndex].imagePath
end

--downloads an image as stores it in user's CustomTexture directory and sets to the menu
function downloadImage(imgURL, chapterName)
	if KEP_DownloadTextureAsyncToGameId ~= nil then
		--the name the image will saved as
		local filename = chapterName..".jpg"
		local path = KEP_DownloadTextureAsyncToGameId(filename, imgURL)
		
        local gameId = KEP_GetGameId()
		
		-- Strip off beginning of file path leaving only file name
		path = string.gsub(path,".-\\", "" )
		path = "../../CustomTexture/".. gameId .. "/" .. path
		return path
	end
end

-- Extracts a helpful image name from a URL
extractImageName = function(imageURL)
	local imageName
	-- Check for valid URL
	local sStart, sEnd = string.find(imageURL, "filestore")
	if sEnd then
		imageName = string.sub(imageURL, sEnd+4, string.len(imageURL))
		if string.find(imageName, ".") then
			-- Check url for valid image extension
			if string.find(imageName, ".jpg") then
				imageName = string.sub(imageName, 0, string.len(imageName)-4)
			else
				return false
			end
		else
			return false
		end
		
		-- Continue to parse out forward slashes until they're all gone
		local imageCopy = imageName
		while string.find(imageName, "/") do
			-- Parse out up to the "/"
			local sStart, sEnd, _ = string.find(imageName, "/")
			if sEnd then
				imageName = string.sub(imageName, sEnd+1, string.len(imageName))
			end
		end
		return imageName
	else
		return false
	end
end

-- Called when a texture has been downloaded from the web
function textureDownloadHandlerLB(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- local _ 	   = KEP_EventDecodeString(event)
	-- local imageLoc = KEP_EventDecodeString(event)
end

function reverseList(inputTable)
	local tempTable = {}
	for i = #inputTable, 1, -1 do
		table.insert(tempTable, inputTable[i])
	end
	return tempTable
end

-- Checks a list of members associated with a flag PID against data received from all flags to make sure the owner is listed first, returning the re-ordered list
function updateMemberListOrder(inputPID, inputList)
	local newMemberList = {}
	for flagIndex,flag in pairs(m_allFlags) do
		if flag.PID == inputPID then
			-- Get the owner and add him first
			for memberIndex,member in pairs(inputList) do
				if tostring(member) == tostring(flag.owner) then
					if not checkForEntry(newMemberList, member) then -- Don't add duplicates
						table.insert(newMemberList, member)
					end
				end
			end
			-- Add everyone else
			for memberIndex,member in pairs(inputList) do
				if tostring(member) ~= tostring(flag.owner) then
					if not checkForEntry(newMemberList, member) then -- Don't add duplicates
						table.insert(newMemberList, member)
					end
				end
			end
		end
	end
	if #newMemberList == 0 then
		newMemberList = inputList
	end
	return newMemberList
end