--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

---------------------------------------------------------------------
-- DRF - Logging Functions (moved from CommonFunctions)
---------------------------------------------------------------------

LOG_DEC_DEFAULT = 3 -- default decimals for logging

-- Set Log=LogOff To Disable Logging In Your Script
function LogOff(txt) end

-- Returns the Script Name For Log Text Prefix
function LogName() 
	return KEP.scriptName.."::" 
end

-- Returns Script Name Log Text Prefixed (framework logs also get zoneInstanceId)
function LogText(txt)
	txt = txt or ""
	txt = LogName()..txt
	if (string.find(txt, "Framework") ~= nil) then
		local instanceId = KEP_GetZoneInstanceId()
		txt = "<zoneInstanceId="..instanceId.."> "..txt
	end
	return txt
end

-- Returns string representation of numeric values decimal rounded
function LogVal(val, dec)
	if (val == nil) then return "<nil>" end
	if (tonumber(val) == nil) then return "<nan>" end
	return MathRoundStr(val, dec or LOG_DEC_DEFAULT)
end

-- Log Functions - Replaces deprecated log() and logThis().
function Log(txt) KEP_Log(LogText(txt)) end
function LogInfo(txt) KEP_LogInfo(LogText(txt)) end
function LogWarn(txt) KEP_LogWarn(LogText(txt)) end
function LogError(txt) KEP_LogError(LogText(txt)) end

-- Legacy Framework Log Function - DEPRECATED - Use Log() Instead!
function log(txt) Log(txt) end
