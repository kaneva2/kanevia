--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- ItemContainer.lua - Kaneva Menu Script Template
-- 
-- Template script for creating a new Kaneva client menu.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("Framework_DragDropHelper.lua")
dofile("Framework_MenuShadowHelper.lua")
dofile("ContextualHelper.lua")

-- local Constants
local ITEMS_PER_PAGE = 20

local TOOLTIP_TABLE = {header = "name"}

local COLORS_VALUES =	{	["Common"] = {a = 255, r = 255, g = 255, b = 255},
							["Rare"] = {a = 255, r = 0, g = 150, b = 255},
							["Very Rare"] = {a = 255, r = 174, g = 0, b = 255},
							["Extremely Rare"] = {a = 255, r = 255, g = 144, b = 0},
							["Never"] = {a = 255, r = 255, g = 144, b = 0}
						}

local ITEM_LEVEL_COORDS = {
							{left = 0, right = 15, top = 146, bottom = 161},
							{left = 16, right = 31, top = 146, bottom = 161},
							{left = 32, right = 47, top = 146, bottom = 161},
							{left = 0, right = 15, top = 162, bottom = 177},
							{left = 16, right = 31, top = 162, bottom = 177}
}

local ITEM_LEVEL_RARITY_OFFSET = {["Common"] = 0, ["Rare"] = 48, ["Very Rare"] = 96, ["Extremely Rare"] = 144, ["Never"] = 144}

local ITEM_RARITY_BG = {	["Common"] = {left = 208, right = 292, top = 167, bottom = 251},
							["Rare"] = {left = 293, right = 377, top = 167, bottom = 251},
							["Very Rare"] = {left = 378, right = 462, top = 167, bottom = 251},
							["Extremely Rare"] = {left = 378, right = 462, top = 82, bottom = 166},
							["Never"] = {left = 378, right = 462, top = 82, bottom = 166}
						}

local DEFAULT_MAX_DURABILITY = 1000 -- 1,000 hits or attacks per weapon/armor
local DURABILITY_BY_LEVEL = { [1]=10, [2]=100, [3]=250, [4]=500, [5]=1000 }
local DURABILITY_BAR_WIDTH = 82
-- Left, top, right, bottom
local DURABILITY_COLORS = {GREEN  = {19, 179, 20, 185},
						   YELLOW = {31, 179, 32, 185},
						   RED 	  = {25, 179, 26, 185}
						  }
						  
local OCCUPIED_ANIMATION_GLID = 4546442

g_displayTable = {}
local g_items = {}

-- Local Variables
local m_containerPID = nil

local m_dragDropInitialized = false
local m_containerSize = 0
local m_totalPages = 1
local m_page = 1
local m_awaitingTransaction = false
local m_pendingTransaction = nil

local m_gameItemNamesByUNID = {} -- added as a item name cache 

local m_playerInventoryByUNID = {}

local m_rightButtonIndex = nil
local m_queuedIndex = nil

local m_inventory = {}			-- [{UNID, count, name, itemType}]
local m_inventoryCounts = {}	-- {UNID:count}

local allItemsLooted = false

-- Local Function Declarations
local updateItemContainer -- ()
local initializeClickToLoot -- (numButtons)
local buttonOnRButtonDown
local buttonOnRButtonUp
local lootAll --()

local getDurabilityColor -- (durabilityPercentage) Gets the durability color by percentage

----------------------------
-- Create Method
----------------------------
function onCreate()
	KEP_EventRegisterHandler("updateContainerClient", "UPDATE_CONTAINER_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateContainerClientFull", "UPDATE_CONTAINER_CLIENT_FULL", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCheckLootResponse", "INVENTORY_HANDLER_CHECK_LOOT_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onPreprocessTransactionResponse", "INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "clientResizeEventHandler", "ClientResizeEvent", KEP.LOW_PRIO )
	
	KEP_EventRegisterHandler("closeChest", "CLOSE_CHEST", KEP.HIGH_PRIO)

	-- getting the item name by the unid 
	KEP_EventRegisterHandler("onItemNameResponse", "INVENTORY_HANDLER_RETURN_ITEM_NAME", KEP.HIGH_PRIO)

	Events.registerHandler("FRAMEWORK_CLOSE_ITEM_CONTAINER", closeChest)

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()	
	InventoryHelper.initializeInventory("g_displayTable", true, ITEMS_PER_PAGE)
	InventoryHelper.initializePageNumbers()
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")

	-- Request player's inventory
	requestInventory(INVENTORY_PID)

	Events.sendEvent("FRAMEWORK_CHECK_TOP_WORLD_REWARD")

	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")				
	end

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "player")
	KEP_EventQueue(ev)

	resizeDropShadow( )
end

function onDestroy()
	hideLootContextualHelp()
	
	KEP_SetCurrentAnimationByGLID(0)
	Events.sendEvent("ITEM_CONTAINER_CLOSED", {playerName = KEP_GetLoginName(), PID = m_containerPID})
	InventoryHelper.destroy()

	-- CONTEXTUAL HELP LOOT CLICKED HIDE
	ContextualHelper.hideHelp(ContextualHelp.LOOT_CLICKED)
end

function inventoryUpdated(page, search, category)
	m_page = page
	DragDrop.setIndexOffset((m_page-1)*ITEMS_PER_PAGE)
	updateItemContainer()
	updateLootContextualHelp()
end

function btnLootAll_OnButtonClicked(btnHandle)
	-- CONTEXTUAL HELP LOOT CLICKED COMPLETE 
	ContextualHelper.completeHelp(ContextualHelp.LOOT_CLICKED)
	
	lootAll()

	allItemsLooted = true
end

----------------------------
-- Event Handlers
----------------------------
function Dialog_OnRender(dialogHandle, felapsedTime)
	KEP_SetCurrentAnimationByGLID(OCCUPIED_ANIMATION_GLID)
end

function Dialog_OnMoved(dialogHandle, x, y)
	updateLootContextualHelp()
end

function updateLootContextualHelp()
	local firstItem = getFirstItemSlot()
	if firstItem >= 1 then
		ContextualHelper.updateHelp(ContextualHelp.LOOT_CLICKED,
				MenuGetLocationThis(),
				Control_GetLocationX(gHandles["imgItem".. tostring(firstItem)]),
				Control_GetLocationY(gHandles["imgItem".. tostring(firstItem)]),
				Control_GetWidth(gHandles["imgItem".. tostring(firstItem)]),
				Control_GetHeight(gHandles["imgItem".. tostring(firstItem)]) * 1.75,
				ContextualHelpFormat.LEFT_ARROW)
	else
		hideLootContextualHelp()
	end 
end

-- Called when auto-looting to check if the player has space for the item
function onCheckLootResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_awaitingTransaction = false
	local lootValid = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	
	if lootValid and m_queuedIndex then --If the looting is valid, move the item to the player inventory
		
		removeItem(m_queuedIndex, m_containerPID)
		-- g_items[m_queuedIndex].lootSource = "Chest"
		updateItem(0, INVENTORY_PID, g_items[m_queuedIndex])
		updateDirty()
		
		-- ED-7151 - Clear out the current moused-over slot when right-clicking to empty
		local emptyItem = deepCopy(EMPTY_ITEM)
		emptyItem.pid = m_containerPID
		DragDrop.sendMouseEnterEvent(m_queuedIndex, emptyItem)

		local newItemEvent = KEP_EventCreate("FRAMEWORK_NEW_ITEM_MESSAGE")
		KEP_EventEncodeString(newItemEvent,"Item")
		KEP_EventQueue(newItemEvent)
	else --Otherwise display a status message with the error
		displayStatusMessage(INVENTORY_ERRORS.BACKPACK_FULL)
	end
	-- No longer awaiting response
	m_queuedIndex = nil
end

function onPreprocessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	if m_awaitingTransaction and m_pendingTransaction then
        if success then
		    for i = 1, #g_items do
			    if g_items[i].count > 0 then
				    removeItem(i, m_containerPID)
			    end
		    end
		    updateDirty()
		    processTransaction({}, m_pendingTransaction)
		    m_pendingTransaction = nil
		    return
        end
        displayStatusMessage(INVENTORY_ERRORS.BACKPACK_FULL)
	end
	m_awaitingTransaction = false
end

function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))

	if menuName ~= MenuNameThis() then return end

	--log("--- on onProcessTransactionResponse ".. tostring(allItemsLooted))
	
	MenuCloseThis()

	if MenuIsOpen("UnifiedInventory") then
		MenuClose("UnifiedInventory")
	end 
end

-- Called when an item inside this container has changed.  Updates the slot and re-displays items.
function updateContainerClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updatePID = KEP_EventDecodeNumber(tEvent)
	if m_containerPID ~= updatePID then return end
	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)

	if g_items[updateIndex] then
		g_items[updateIndex] = updateItem
		updateItemContainer()
		updateLootContextualHelp()
	end
end

-- Called when a new container inventory is received.  Updates all items and re-displays.
function updateContainerClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	if m_containerPID ~= nil then Events.sendEvent("ITEM_CONTAINER_CLOSED", {playerName = KEP_GetLoginName(), PID = m_containerPID}) end
	m_containerPID = KEP_EventDecodeNumber(tEvent)
	g_items = decompileInventory(tEvent)
	
	m_containerSize = #g_items
	m_totalPages = math.ceil(m_containerSize/ITEMS_PER_PAGE)
	--updatePageIndex(1)
	
	if not m_dragDropInitialized then
		DragDrop.initializeDragDrop(ITEMS_PER_PAGE)
		initializeClickToLoot(ITEMS_PER_PAGE)
		m_dragDropInitialized = true

		-- CONTEXTUAL HELP LOOT CLICKED OPEN
		local firstItem = getFirstItemSlot()
		if firstItem >= 1 then	
			ContextualHelper.showHelp(ContextualHelp.LOOT_CLICKED,
				MenuGetLocationThis(),
				Control_GetLocationX(gHandles["imgItem".. tostring(firstItem)]),
				Control_GetLocationY(gHandles["imgItem".. tostring(firstItem)]),
				Control_GetWidth(gHandles["imgItem".. tostring(firstItem)]),
				Control_GetHeight(gHandles["imgItem".. tostring(firstItem)]) * 1.75,
				ContextualHelpFormat.LEFT_ARROW)

			-- CONTEXTUAL HELP EMPTY BACKPACK HIDE
			ContextualHelper.forceCompleteHelp(ContextualHelp.EMPTY_BACKPACK)
		end
	end	
	
	-- If we've retrieved our images and we're not displaying yet
	updateItemContainer()
end

function getFirstItemSlot()
	for i = 1, #g_items do
		if g_items[i].UNID ~= 0 then return i end 
	end 

	return -1
end

-- getting the item name from the 
function onItemNameResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    local UNID = KEP_EventDecodeNumber(tEvent)
    local itemName = KEP_EventDecodeString(tEvent)
    
    if not m_gameItemNamesByUNID[tostring(UNID)] then
    	m_gameItemNamesByUNID[tostring(UNID)] = tostring(itemName)
    
    	updateItemContainer()
    end
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if m_inventory[updateIndex] then
		m_inventory[updateIndex] = updateItem
		m_playerInventoryByUNID[updateItem.UNID] = updateItem.count
		updateItemContainer()
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_inventory = decompileInventory(tEvent)
	updateInventory(m_inventory)
	updateItemContainer()
end

function updateInventory(m_playerInventory)
	for i = 1, #m_playerInventory do
		m_playerInventoryByUNID[m_playerInventory[i].UNID] = m_playerInventory[i].count
	end
end

function closeChest(event)
	if not MenuIsOpen("UnifiedInventory") then
		MenuCloseThis()
	end
end

function hideLootContextualHelp()
	-- CONTEXTUAL HELP LOOT CLICKED HIDE
	ContextualHelper.hideHelp(ContextualHelp.LOOT_CLICKED)
end

----------------------------
-- Private Functions
----------------------------

-- Displays all items inside the container and registers them with the drag / drop system.
updateItemContainer = function()
	--m_displaying = true
	if g_items == nil or #g_items == 0 then return end
	for i=1, #g_items do
		if g_items[i] and tonumber(g_items[i].UNID) and tonumber(g_items[i].UNID) > 0 then
			g_items[i].lootInfo = {sourcePID = m_containerPID, lootSource = "Chest"}
		end
	end
	g_displayTable = deepCopy(g_items)
	local numItems = 0
	for i=1,m_containerSize and ITEMS_PER_PAGE do
		local index = i + (m_page-1)*ITEMS_PER_PAGE

		if g_displayTable[index].count <= 0 then
			g_displayTable[index].hide = true
		elseif tablePopulated(g_displayTable[index].instanceData)  or (g_displayTable[index].properties.stackSize and g_displayTable[index].properties.stackSize <= 1) then
			g_displayTable[index].count = 0
		end
		
		if not g_displayTable[index].hide then
			numItems = numItems + 1
		end
		
		formatTooltips(g_displayTable[index])

		applyImage(gHandles["imgItem"..i], g_items[index].properties.GLID)

		local element
		if g_items[index].properties.itemType == ITEM_TYPES.WEAPON or g_items[index].properties.itemType == ITEM_TYPES.ARMOR or g_items[index].properties.itemType == ITEM_TYPES.TOOL or (g_items[index].properties.itemType == ITEM_TYPES.CHARACTER and g_items[index].properties.behavior == "pet") then
			Control_SetVisible(gHandles["imgItemLevel" .. i], true)
			element = Image_GetDisplayElement(gHandles["imgItemLevel" .. i])
			local itemLevel = g_items[index].properties.level
			local rarityOffset = ITEM_LEVEL_RARITY_OFFSET[g_items[index].properties.rarity] or 0
			Element_SetCoords(element, ITEM_LEVEL_COORDS[itemLevel].left + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].top, ITEM_LEVEL_COORDS[itemLevel].right + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].bottom)

			-- Set durability bar			
			if g_items[index].instanceData and g_items[index].instanceData.durability then
				-- Get item durability
				local durability = g_items[index].instanceData.durability
				local durabilityType = g_items[index].properties.durabilityType or "repairable"
				local durabilityPercentage = 0

				if durabilityType == "limited" then
					durabilityPercentage = math.min((durability/DURABILITY_BY_LEVEL[itemLevel]), 1)
				elseif durabilityType == "indestructible" then
					durabilityPercentage = 1
				else
					durabilityPercentage = math.min((durability/DEFAULT_MAX_DURABILITY), 1)
				end
				
				-- Set bar size
				Control_SetSize(gHandles["imgDurabilityBar" .. i], durabilityPercentage * DURABILITY_BAR_WIDTH, 7)
				
				-- Set bar color
				local durColor = getDurabilityColor(durabilityPercentage, durabilityType)
				Element_SetCoords(Image_GetDisplayElement(gHandles["imgDurabilityBar" .. i]), unpack(durColor))
				-- Set bar's visible
				Control_SetVisible(gHandles["imgDurabilityBar" .. i], true)
				Control_SetVisible(gHandles["imgDurabilityBG" .. i], true)
				Control_SetVisible(gHandles["imgDurabilityGradiant" .. i], true)
				-- Is the item broken?
				Control_SetVisible(gHandles["imgItemBroken" .. i], durability <= 0)
				Control_SetVisible(gHandles["imgItemOverlay" .. i], durability <= 0)
			end
		else
			Control_SetVisible(gHandles["imgItemLevel" .. i], false)
			
			Control_SetVisible(gHandles["imgDurabilityBar" .. i], false)
			Control_SetVisible(gHandles["imgDurabilityBG" .. i], false)
			Control_SetVisible(gHandles["imgDurabilityGradiant" .. i], false)
			
			Control_SetVisible(gHandles["imgItemBroken" .. i], false)
			Control_SetVisible(gHandles["imgItemOverlay" .. i], false)
		end

		local rarity = g_items[index].properties.rarity or "Common"
		element = Image_GetDisplayElement(gHandles["imgItemBG" .. i])
		Element_SetCoords(element, ITEM_RARITY_BG[rarity].left, ITEM_RARITY_BG[rarity].top, ITEM_RARITY_BG[rarity].right, ITEM_RARITY_BG[rarity].bottom)

		DragDrop.registerItem(index, m_containerPID, g_items[index])
	end
	
	if numItems == 0 then
		Control_SetEnabled(gHandles["btnLootAll"], false)
	else
		Control_SetEnabled(gHandles["btnLootAll"], true)
	end

	InventoryHelper.setMaxPages(#g_displayTable)
	InventoryHelper.setNameplates("name", "hide")
	--InventoryHelper.setIcons("GLID")
	InventoryHelper.setQuantities("count")
end

-- Initializes button handlers for right-click auto-loot functionality.
initializeClickToLoot = function(numButtons)
	for i = 1, numButtons do		
		_G["btnItem" .. tostring(i) .. "_OnRButtonDown"] = function(btnHandle, x, y)
			buttonOnRButtonDown(btnHandle, x, y)
		end
		
		_G["btnItem" .. tostring(i) .. "_OnRButtonUp"] = function(btnHandle, x, y)
			buttonOnRButtonUp(btnHandle, x, y)
		end
	end
end

function formatTooltips(item)
	if item then
		local itemUNID = tostring(item.UNID)

		item.tooltip = {}

		item.tooltip["header"] = {label = item.properties.name}

		if item.properties.rarity then
			item.tooltip["header"].color = COLORS_VALUES[item.properties.rarity]
		end

		if item.properties.description then
			item.tooltip["body"] = {label = "\"" .. item.properties.description .. "\""}
		end

		if item.properties.itemType == ITEM_TYPES.WEAPON then
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}

			-- change the description when the 
			if item.properties.healingWeapon and item.properties.healingWeapon == true then
				item.tooltip["healing"] = {label = tostring(item.properties.damage * item.properties.bonusDamage)}
			else 
				item.tooltip["damage"] = {label = tostring(item.properties.damage * item.properties.bonusDamage)}
			end 

			item.tooltip["range"] = {label = tostring(item.properties.range)}

			if item.properties.ammoType and m_gameItemNamesByUNID[tostring(item.properties.ammoType)] then 
				if m_playerInventoryByUNID[item.properties.ammoType] == nil then
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.ammoType)]), color = {r = 255, g = 0, b = 0, a = 255}}
				else
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.ammoType)])}
				end
			elseif item.properties.ammoType then 
				getItemNameByUNID(item.properties.ammoType)
			end 

			local durability = item.instanceData.durability
			local durabilityType = item.properties.durabilityType or "repairable"
			local itemLevel = item.properties.level
			
			if durabilityType == "limited" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage),
												durability = durabilityPercentage}
			elseif durabilityType == "indestructible" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = "100%",
										  		durability = 100}
			else
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage).."%",
										 		durability = durabilityPercentage}
			end

			--item.tooltip["loot"] = {label = "Loot Weapons by right clicking on it or dragging it to your backpack.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.TOOL then
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}

			local durability = item.instanceData.durability
			local durabilityType = item.properties.durabilityType or "repairable"
			local itemLevel = item.properties.level

			if item.properties.paintWeapon then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["uses"] = {label = tostring(durability), 
											durability = durabilityPercentage}
			elseif durabilityType == "limited" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				if durabilityPercentage == 0 and durability > 0 then
					durabilityPercentage = 1
				end
				item.tooltip["durability"] = {label = tostring(durabilityPercentage),
												durability = durabilityPercentage}

			elseif durabilityType == "indestructible" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = "100%",
										  		durability = 100}
			else
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				if durabilityPercentage == 0 and durability > 0 then
					durabilityPercentage = 1
				end
				item.tooltip["durability"] = {label = tostring(durabilityPercentage).."%",
										 		durability = durabilityPercentage}
			end

			--item.tooltip["loot"] = {label = "Loot Tools by right clicking on it or dragging it to your backpack.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.ARMOR then
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}
			item.tooltip["armorslot"] = {label = tostring(item.properties.slotType:gsub("^%l", string.upper))}

			local armorRating
			if item.properties.bonusHealth then
				armorRating = item.properties.armorRating * item.properties.bonusHealth
			else
				armorRating = item.properties.armorRating * 3
			end

			item.tooltip["armorrating"] = {label = tostring(armorRating)}
			
			local durability = item.instanceData.durability
			local durabilityType = item.properties.durabilityType or "repairable"
			local itemLevel = item.properties.level

			if durabilityType == "limited" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage),
												durability = durabilityPercentage}

			elseif durabilityType == "indestructible" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = "100%",
										  		durability = 100}
			else
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage).."%",
										 		durability = durabilityPercentage}
			end

			--item.tooltip["loot"] = {label = "Loot Armor by right clicking on it or dragging it to your backpack.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.CONSUMABLE then
			if item.properties.consumeType == "energy" then
				item.tooltip["energy"] = {label = tostring(item.properties.consumeValue) .. "%"}--, data = "consumeValueDisplay"}
			else
				item.tooltip["health"] = {label = tostring(item.properties.consumeValue) .. "%"}--, data = "consumeValueDisplay"}
			end
			--item.tooltip["loot"] = {label = "Loot Consumables by right clicking on it or dragging it to your backpack.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.PLACEABLE then
			--item.tooltip["loot"] = {label = "Loot Placeables by right clicking on it or dragging it to your backpack.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.GENERIC then
			--item.tooltip["loot"] = {label = "Loot Generic Items by right clicking on it or dragging it to your backpack.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.AMMO then
			--item.tooltip["loot"] = {label = "Loot Ammo by right clicking on it or dragging it to your backpack.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.HARVESTABLE then
			-- get food properties 
			--log("--- food : ".. tostring(item.properties.food))
			if item.properties.food and m_gameItemNamesByUNID[tostring(item.properties.food)] then 
				item.tooltip["requires1"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.food)]).. " to grow"}
			elseif item.properties.food then 
				getItemNameByUNID(item.properties.food)
			end 

			-- get tool from properties 
			--log("--- requiredTool : ".. tostring(item.properties.requiredTool))
			if item.properties.requiredTool and m_gameItemNamesByUNID[tostring(item.properties.requiredTool)] then 
				item.tooltip["requires2"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.requiredTool)]).. " to harvest"}
			elseif item.properties.food then 
				getItemNameByUNID(item.properties.requiredTool)
			end 

			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}
			
			--item.tooltip["loot"] = {label = "Loot Harvestables by right clicking on it or dragging it to your backpack.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.BLUEPRINT then
			--item.tooltip["loot"] = {label = "Loot Blueprints by right clicking on it or dragging it to your backpack.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.CHARACTER and item.properties.behavior == "pet" then
			if item.properties.food and m_gameItemNamesByUNID[tostring(item.properties.food)] then 
				if m_playerInventoryByUNID[item.properties.food] == nil then
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.food)]), color = {r = 255, g = 0, b = 0, a = 255}}
				else
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.food)])}
				end
			elseif item.properties.food then 
				getItemNameByUNID(item.properties.food)
			end
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}
			item.tooltip["loot"] = {label = "Loot Pets by right clicking on it or dragging it to your backpack.", type = ITEM_TYPES.PET}
		end

		if not item.tooltip["loot"] then 
			item.tooltip["loot"] = {label = "Right-click to take.", type = item.properties.itemType}
		end 
	end
end

-- Right mouse button down handler for auto-loot.
function buttonOnRButtonDown(btnHandle, x, y)
	local s, e, n = string.find(Control_GetName(btnHandle), "(%d+)")
	m_rightButtonIndex = tonumber(n) + (m_page-1)*ITEMS_PER_PAGE
end

-- Right mouse button up handler for auto-loot.
function buttonOnRButtonUp(btnHandle, x, y)
	local s, e, n = string.find(Control_GetName(btnHandle), "(%d+)")
	if tonumber(n) + (m_page-1)*ITEMS_PER_PAGE == m_rightButtonIndex and g_items[m_rightButtonIndex] and g_items[m_rightButtonIndex].UNID ~= 0 and m_queuedIndex == nil and m_awaitingTransaction == false then
		m_awaitingTransaction = true
		m_queuedIndex = m_rightButtonIndex
		local checkEvent = KEP_EventCreate("INVENTORY_HANDLER_CHECK_LOOT")
		KEP_EventEncodeNumber(checkEvent, 0)
		checkEvent = compileInventoryItem(g_items[m_queuedIndex], checkEvent)
		KEP_EventSetFilter(checkEvent, 4)
		KEP_EventQueue(checkEvent)

		-- CONTEXTUAL HELP LOOT CLICKED COMPLETE 
		ContextualHelper.completeHelp(ContextualHelp.LOOT_CLICKED)
	end
	m_rightButtonIndex = nil
end

lootAll = function()
	if m_awaitingTransaction then return end
	local addTable = {}
	for i = 1, #g_items do
		table.insert(addTable, g_items[i])
	end
	
	if tablePopulated(addTable) then
		m_awaitingTransaction = true
		m_pendingTransaction = addTable
		processTransaction({}, addTable, true)
	end

	-- if MenuIsOpen("UnifiedInventory") then
	-- 	MenuClose("UnifiedInventory")
	-- end
end

-- Returns the color for the durability bar based on percentage passed in
getDurabilityColor = function(durabilityPercentage)
	-- Green bar
	if durabilityPercentage >= .5 then
		return DURABILITY_COLORS.GREEN
	-- Return yellow
	elseif durabilityPercentage >= .15 then
		return DURABILITY_COLORS.YELLOW
	-- Return red
	else
		return DURABILITY_COLORS.RED
	end
end

function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end