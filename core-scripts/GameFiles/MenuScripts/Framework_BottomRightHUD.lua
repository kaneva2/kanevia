--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_BottomRightHUD.lua
--
-- Allows players to toggle between God and Player Mode
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\MenuAnimation.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("ContextualHelper.lua")

-- Local vars
local m_armorEnabled = false
local m_armorRating = 0

local m_healthEnabled = false
local m_currHealth = 0
local m_oldHealth = 0 -- For animating the health bar
local m_maxHealth = 0

local m_energyEnabled = false
local m_currEnergy = 0
local m_maxEnergy = 0
local m_starvationEnabled = false

--vehcile properites
local m_currVehicleHealth = 50
local m_maxVehicleHealth = 100
local m_currFuel = 75
local m_maxFuel = 100

local m_healthFlashTime = 0
local m_flashing = false

local m_animateHealthTime = 0
local m_animateHealth = false

local m_dynamicEnable = false
local m_minimizeTime = nil
local m_minimized = false
local m_minimizeAnimation = nil
local m_isAnimating = false
local m_toolbeltMinimized = false

local m_vehicleMode = false

local m_showingContextualHelp = false


-- Statics
local STATUS_ICONS = "Framework_StatusIcons.xml"
local MAP_ICON = "Framework_MapIcon.xml"
local DANCE_ICON = "kdp_wanttoplay.xml"
local ICONS_OFFSET = 30 -- 40
local MAP_OFFSET = 70
local DANCE_OFFSET_X = 75
local DANCE_OFFSET_Y = 100
local BAR_OFFSET = 34


local MINIMIZE_DISPLACEMENT = 118
local MINIMIZE_DURATION = .20
local MINIMIZE_INDICATORS_FILTER = 2
local MINIMIZE_HEALTH_THRESHOLD = 0.5
local MINIMIZE_DELAY_LONG = 3

local BAR_HEIGHT = 14
local BAR_WIDTH = 100



local HEALTH_FLASH_TIME = .3 -- How long in seconds it takes for the health bar flash to complete its animation
local HEALTH_ANIMATE_TIME = .3 -- How long in seconds it takes for the health bar to animate to its new value
local HEALTH_FLASH_WIDTH = 40

-- Base damage for each armor by level
local ARMOR_BASE = {1,11,101,1001,10001}
-- ARMOR ranges available for each armor by level
local ARMOR_RANGE = {9,89,899,8999,89999}
local ARMOR_NOTCH_WIDTH = 20

-- Local functions
local setArmorBar -- ()
local setHealthBar -- ()
local setEnergyBar -- ()
local setVehicleHealthBar -- ()
local setFuelBar -- ()
local toggleArmor -- (toggle)
local toggleHealth -- (toggle)
local toggleEnergy -- (toggle)
local toggleVehicleHealth -- (toggle)
local toggleFuel -- (toggle)
local toggleArmorText -- (toggle)
local toggleHealthText -- (toggle)
local toggleEnergyText -- (toggle)
local toggleVehicleHealthText -- (toggle)
local toggleFuelText -- (toggle)
local toggleHUDMinimized -- (toggle)
local toggleHUDMode -- ()
local canMinimizeToolbar

-- Called when the menu is created
function onCreate()
	-- Event handlers
	Events.registerHandler("FRAMEWORK_ENABLE_ARMOR", enableArmorEvent)
	Events.registerHandler("FRAMEWORK_ENABLE_HEALTH", enableHealthEvent)
	Events.registerHandler("FRAMEWORK_ENABLE_ENERGY", enableEnergyEvent)
	Events.registerHandler("FRAMEWORK_SET_ARMOR", 	  setArmor)
	Events.registerHandler("FRAMEWORK_SET_HEALTH", 	  setHealth)
	Events.registerHandler("FRAMEWORK_SET_ENERGY", 	  setEnergy)
	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS", returnSettingsEvent)
	Events.registerHandler("FRAMEWORK_SET_VEHICLE_FUEL", setFuel)
	Events.registerHandler("FRAMEWORK_SET_VEHICLE_HEALTH", setVehicleHealth)

	KEP_EventRegister("bottomHUDMinimizedEvent", KEP.MED_PRIO)

	KEP_EventRegisterHandler("frameworkMinimizeHUDHandler", "FRAMEWORK_MINIMIZE_HUD", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "hideMapIconSettingEventHandler", "HideMapIconSettingEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "mapOpenHandler", "FRAMEWORK_MAP_ICON_OPENED", KEP.MED_PRIO )
	KEP_EventRegisterHandler("openContextualMenuEventHandler", "openContextualMenuEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("closeContextualMenuEventHandler", "closeContextualMenuEvent", KEP.HIGH_PRIO)

	Events.sendEvent("FRAMEWORK_REQUEST_BR_HUD_INFO")
	
	MenuOpen("Framework_StatusIcons.xml")

	toggleArmorText(false)
	toggleHealthText(false)
	toggleEnergyText(false)

	toggleArmor(false)
	toggleHealth(false)
	toggleEnergy(false)

	-- Dynamic HUD A/B test. If in the original, default to showing.
	m_dynamicEnable = false
	m_minimizeAnimation = Tween(MenuHandleThis())
	m_minimizeAnimation:on(CALLBACK.STOP, onMinimizeStop)
	m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT, MINIMIZE_DURATION, nil, Easing.inOutQuad)
	toggleHUDMinimized(true, true)
end

-- Called each frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	MenuAnimation_OnRender(fElapsedTime)

	if m_flashing then
		-- Position and set the size of the animation flash
		local healthFlash = gHandles["imgHealthFlash"]
		m_healthFlashTime = m_healthFlashTime - fElapsedTime
		
		-- Are we still animating?
		if m_healthFlashTime > 0 then
			local healthBar = gHandles["imgHealthBGSlice2"]
			local menuWidth = Control_GetLocationX(healthBar) + Control_GetWidth(healthBar)
			
			-- Position of flash accounts for size of the bar + the flash size
			local flashX = Easing.outQuad(m_healthFlashTime, -HEALTH_FLASH_WIDTH, (menuWidth + HEALTH_FLASH_WIDTH), HEALTH_FLASH_TIME)
			-- Don't position yourself any less than 0 (left boundary)
			Control_SetLocationX(healthFlash, math.max(flashX,0))
			
			-- Adjust flash size based on location
			-- Flash overflowing on left
			if flashX < 0 then
				Control_SetSize(healthFlash, HEALTH_FLASH_WIDTH + flashX, Control_GetHeight(healthFlash))
			-- Flash overflowing on right
			elseif (flashX + HEALTH_FLASH_WIDTH) > menuWidth then
				Control_SetSize(healthFlash, menuWidth - flashX , Control_GetHeight(healthFlash))
			end
			-- Turn on the flash if needed
			if Control_GetVisible(healthFlash) == 0 then
				Control_SetVisible(healthFlash, true)
			end
		-- Done flashing (teehee)
		else
			m_flashing = false
			Control_SetVisible(healthFlash, false)
		end
	end
	
	-- Are we animating a health change?
	if m_animateHealth then
		m_animateHealthTime = m_animateHealthTime - fElapsedTime
		-- Still animating
		if m_animateHealthTime > 0 then
			local displayedHealth = Easing.outQuart(m_animateHealthTime, m_currHealth, (m_oldHealth - m_currHealth), HEALTH_ANIMATE_TIME)
			Control_SetSize(gHandles["imgHealthBar"], BAR_WIDTH*(displayedHealth/m_maxHealth), BAR_HEIGHT)
		-- Done animating
		else
			m_animateHealth = false
			-- Set the width of the bar
			Control_SetSize(gHandles["imgHealthBar"], BAR_WIDTH*(m_currHealth/m_maxHealth), BAR_HEIGHT)
		end
	end

	-- Minimize on timer.
	if m_minimizeTime then
		m_minimizeTime = m_minimizeTime - fElapsedTime

		if m_minimizeTime <= 0 then
			m_minimizeTime = nil
			toggleHUDMinimized(true)
		end
	end


	if m_isAnimating or MenuIsOpen(DANCE_ICON) then
		toggleIconsLocation()
	end
end

-- Called when the dialog is moved
function Dialog_OnMoved(dialogHandle, x, y)
	toggleHUDMinimized(m_minimized, true)
	toggleIconsLocation()
end

-- Called when the mouse enters the bar section
function btnBarSwitch_OnMouseEnter(buttonHandle)
	toggleArmorText(m_armorEnabled and m_armorRating > 0 and not m_vehicleMode)

	if not m_vehicleMode then
		toggleHealthText(m_healthEnabled)
		toggleEnergyText(m_energyEnabled)
	else
		toggleVehicleHealthText(true)
		toggleFuelText(true)
	end
	toggleIconsLocation()
end

-- Called when the mouse leaves the bar section
function btnBarSwitch_OnMouseLeave(buttonHandle)
	toggleArmorText(false)
	toggleHealthText(false)
	toggleEnergyText(false)
	toggleVehicleHealthText(false)
	toggleFuelText(false)
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Enable health bar
function enableArmorEvent(event)
	m_armorEnabled = true
	if m_armorRating and m_armorRating > 0 then
		toggleArmor(m_armorEnabled)
		setArmorBar()
	end
end

-- Enable health bar
function enableHealthEvent(event)
	m_healthEnabled = true

	m_maxHealth = math.max(event.maxHealth, 1) -- No less than 1
	local newHealth = event.health
	if newHealth < 0 then m_currHealth = 0 end
	if newHealth > m_maxHealth then m_currHealth = m_maxHealth end
	m_currHealth = newHealth

	if not m_vehicleMode then
		toggleHealth(m_healthEnabled)
		setHealthBar()
	end
end

-- Enable energy bar
function enableEnergyEvent(event)
	m_energyEnabled = true

	m_maxEnergy = math.max(event.maxEnergy, 1) -- No less than 1
	local newEnergy = event.energy
	if newEnergy < 0 then m_currEnergy = 0 end
	if newEnergy > m_maxEnergy then m_currEnergy = m_maxEnergy end
	m_currEnergy = newEnergy

	if not m_vehicleMode then
		toggleEnergy(m_energyEnabled)
		setEnergyBar()
	end
end

-- Sets health value
function setArmor(event)
	m_armorRating = event.armorRating	

	toggleArmor(m_armorEnabled and m_armorRating > 0)

	setArmorBar()
end

-- Sets health value
function setHealth(event)
	m_maxHealth = math.max(event.maxHealth, 1)
	m_oldHealth = m_currHealth
	local newHealth = event.health
	if newHealth < 0 then m_currHealth = 0 end
	if newHealth > m_maxHealth then m_currHealth = m_maxHealth end
	m_currHealth = newHealth
	
	if event.alert then
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, "You are out of energy!")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
	end

	-- Set a flash if requested and if health dropped
	if event.flash and not m_flashing and (m_oldHealth > m_currHealth) then
		local flashElement = Image_GetDisplayElement(gHandles["imgHealthFlash"])
		local alpha = 255 -- Default flash alpha is 255
		m_flashing = true
		m_healthFlashTime = HEALTH_FLASH_TIME
		local damagePercentage = (m_oldHealth-m_currHealth) / m_maxHealth
		-- If damage was less than 10 percent of the player's max health
		if damagePercentage < .1 then
			alpha = math.max(25.5,255 * (damagePercentage/m_maxHealth))
		end
		BlendColor_SetColor(Element_GetTextureColor(flashElement), 0, {a = alpha,
																	   r = 255,
																	   g = 266,
																	   b = 255})
		
	end
	
	-- Queue the animation
	m_animateHealthTime = HEALTH_ANIMATE_TIME
	m_animateHealth = true

	setHealthBar()

	if m_oldHealth > m_currHealth then
		-- Dynamic HUD A/B test. Show if already showing, or if the user has an item in their inventory.
		m_dynamicEnable = true
		toggleHUDMinimized(false)
		m_minimizeTime = MINIMIZE_DELAY_LONG

		-- B: Show the health bar contextual help
		if canShowContextualHelp() then
			ContextualHelper.showHelp(ContextualHelp.STATUS_HEALTH,
				MenuGetLocationThis(),
				Control_GetLocationX(gHandles["imgHealthIcon"]),
				Control_GetLocationY(gHandles["imgHealthIcon"]),
				Control_GetWidth(gHandles["imgHealthIcon"]),
				Control_GetHeight(gHandles["imgHealthIcon"]),
				ContextualHelpFormat.RIGHT_ARROW, 
				30)
		end 
	elseif m_oldHealth < m_currHealth or m_currHealth == 0 then
		toggleHUDMinimized(true)
	end
end

-- Sets energy value
function setEnergy(event)
	m_maxEnergy = math.max(event.maxEnergy, 1)
	local oldEnergy = m_currEnergy
	local newEnergy = event.energy
	if newEnergy < 0 then m_currEnergy = 0 end
	if newEnergy > m_maxEnergy then m_currEnergy = m_maxEnergy end
	
	-- B: show the help for the energy 
	if canShowContextualHelp() then
		if newEnergy / m_maxEnergy <= 0.5 or newEnergy > m_currEnergy then
			ContextualHelper.showHelp(ContextualHelp.STATUS_ENERGY,
				MenuGetLocationThis(),
				Control_GetLocationX(gHandles["imgEnergyIcon"]),
				Control_GetLocationY(gHandles["imgEnergyIcon"]),
				Control_GetWidth(gHandles["imgEnergyIcon"]),
				Control_GetHeight(gHandles["imgEnergyIcon"]),
				ContextualHelpFormat.BOTTOM_ARROW, 
				30)
		end
	end

	m_currEnergy = newEnergy
	
	setEnergyBar()

	if m_starvationEnabled then
		if m_maxEnergy > 0 and m_currEnergy / m_maxEnergy < MINIMIZE_HEALTH_THRESHOLD then
			-- Dynamic HUD A/B test. Show if already showing, or if the user has an item in their inventory.
			m_dynamicEnable = true
			toggleHUDMinimized(false)
		elseif oldEnergy < newEnergy or newEnergy == 0 then
			toggleHUDMinimized(true)
		end
	end 
end

function setFuel(event)
	-- set the current vehicle fuel here
	m_currFuel = event.fuelTime
	m_maxFuel = event.maxFuelTime
	-- update the UI 
	setFuelBar()
end

function setVehicleHealth(event)
	-- set the current vehicle health
	m_currVehicleHealth = event.health
	m_maxVehicleHealth = event.maxHealth
	-- update the UI 
	setVehicleHealthBar()
end

-- Handles zone settings
function returnSettingsEvent(event)
	m_starvationEnabled = event.starvation
end

-- Handles the "frameworkMinimizeHUD" event triggered when hud is hidden.
function frameworkMinimizeHUDHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == MINIMIZE_INDICATORS_FILTER then
		local checked = Event_DecodeNumber(event)
		local instant = Event_DecodeNumber(event)
		local dynamicEnable = Event_DecodeNumber(event)

		 local forceSwitch = m_vehicleMode
		if KEP_EventMoreToDecode(event) ~= 0 then
			m_vehicleMode =  Event_DecodeNumber(event) == 1
			forceSwitch = (forceSwitch ~= m_vehicleMode) and not m_minimized
		end
		if (forceSwitch and m_minimized) or (m_vehicleMode and m_minimized) then
			--toggleHUDMode()
		end
		m_dynamicEnable = m_dynamicEnable or dynamicEnable == 1
		m_toolbeltMinimized = checked == 1
		toggleHUDMinimized(m_toolbeltMinimized, instant == 1, forceSwitch)
	end
end

-- Handles the "HideMapIconSettingEvent" triggered when hud is hidden.
function hideMapIconSettingEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local checked = Event_DecodeNumber(event)
	m_hideSetting = checked == 1


	if not m_hideSetting then
		toggleIconsLocation()
	end
end

function mapOpenHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	toggleIconsLocation()
end

function onMinimizeStop()
	toggleHUDMode()

	m_isAnimating = false
	toggleIconsLocation()

	updateContextualHelp()
end

function updateContextualHelp()
	ContextualHelper.updateHelp(ContextualHelp.STATUS_ENERGY,
				MenuGetLocationThis(),
				Control_GetLocationX(gHandles["imgEnergyIcon"]),
				Control_GetLocationY(gHandles["imgEnergyIcon"]),
				Control_GetWidth(gHandles["imgEnergyIcon"]),
				Control_GetHeight(gHandles["imgEnergyIcon"]),
				ContextualHelpFormat.BOTTOM_ARROW)
	ContextualHelper.updateHelp(ContextualHelp.STATUS_HEALTH,
				MenuGetLocationThis(),
				Control_GetLocationX(gHandles["imgHealthIcon"]),
				Control_GetLocationY(gHandles["imgHealthIcon"]),
				Control_GetWidth(gHandles["imgHealthIcon"]),
				Control_GetHeight(gHandles["imgHealthIcon"]),
				ContextualHelpFormat.RIGHT_ARROW)
end

-- Handles the "openContextualMenuEvent" sent when contextual help is started.
function openContextualMenuEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local eventParams = KEP_EventDecodeString(tEvent)
	eventParams = Events.decode(eventParams)
	local helpID = eventParams.type
	if helpID == ContextualHelp.STATUS_AREA
			or helpID == ContextualHelp.STATUS_ENERGY
			or helpID == ContextualHelp.STATUS_HEALTH then
		m_showingContextualHelp = true
		toggleHUDMinimized(false, true)
		updateContextualHelp()
	end 
end

-- Handles the "closeContextualMenuEvent" sent when contextual help is closed.
function closeContextualMenuEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local eventParams = KEP_EventDecodeString(tEvent)
	eventParams = Events.decode(eventParams)
	local helpID = eventParams.type
	if helpID == ContextualHelp.STATUS_AREA
			or helpID == ContextualHelp.STATUS_ENERGY
			or helpID == ContextualHelp.STATUS_HEALTH then
		m_showingContextualHelp = false
		m_minimizeTime = MINIMIZE_DELAY_SHORT
	end 
end

-- -- -- -- -- -- --
-- Local Functions
-- -- -- -- -- -- --

setArmorBar = function()
	local armorBarWidth = 0
	for i=1,#ARMOR_BASE do
		if m_armorRating >= ARMOR_BASE[i] + ARMOR_RANGE[i] then
			armorBarWidth = armorBarWidth + ARMOR_NOTCH_WIDTH
		else
			armorBarWidth = armorBarWidth + ( m_armorRating/(ARMOR_BASE[i] + ARMOR_RANGE[i]) * ARMOR_NOTCH_WIDTH )
			break
		end
	end
	
	armorBarWidth = armorBarWidth <= BAR_WIDTH and armorBarWidth or BAR_WIDTH

	Control_SetSize(gHandles["imgArmorBar"] , armorBarWidth, BAR_HEIGHT)

	-- Set the hover text of the bar
	Static_SetText(gHandles["stcArmorText"], " "..comma_value(math.floor(m_armorRating)))
end

-- Set the health bar
setHealthBar = function()
	-- Set the hover text of the bar
	Static_SetText(gHandles["stcHealthText"], " "..comma_value(math.floor(m_currHealth)))
end

-- Set the energy bar
setEnergyBar = function()
	-- Set the width of the bar
	Control_SetSize(gHandles["imgEnergyBar"], BAR_WIDTH*(m_currEnergy/m_maxEnergy), BAR_HEIGHT)

	-- Set the hover text of the bar
	Static_SetText(gHandles["stcEnergyText"], " "..comma_value(math.floor(m_currEnergy)).."/"..comma_value(math.floor(m_maxEnergy)))
end

setVehicleHealthBar = function()
	-- Set the width of the bar
	Control_SetSize(gHandles["imgVehicleHealthBar"], BAR_WIDTH*(m_currVehicleHealth/m_maxVehicleHealth), BAR_HEIGHT)

	Static_SetText(gHandles["stcVehicleHealthText"], " "..comma_value(math.floor(m_currVehicleHealth)))
end

setFuelBar = function()
	-- Set the width of the bar
	Control_SetSize(gHandles["imgFuelBar"], BAR_WIDTH*(m_currFuel/m_maxFuel), BAR_HEIGHT)

	-- Set the hover text of the bar
	Static_SetText(gHandles["stcFuelText"], " "..comma_value(math.floor(m_currFuel)).."/"..comma_value(math.floor(m_maxFuel)))
end

toggleArmor = function(toggle)
	if toggle and m_vehicleMode then return end
	Control_SetVisible(gHandles["imgArmorBGSlice1"], toggle)
	Control_SetVisible(gHandles["imgArmorBGSlice2"], toggle)
	Control_SetVisible(gHandles["imgArmorBGSlice3"], toggle)
	Control_SetVisible(gHandles["imgArmorBar"], toggle)
	Control_SetVisible(gHandles["imgArmorSlice1"], toggle)
	Control_SetVisible(gHandles["imgArmorSlice2"], toggle)
	Control_SetVisible(gHandles["imgArmorSlice3"], toggle)
	Control_SetVisible(gHandles["imgArmorSlice4"], toggle)
	Control_SetVisible(gHandles["imgArmorIcon"], toggle)
	toggleIconsLocation()
end

toggleHealth = function(toggle)
	if toggle and m_vehicleMode then return end
	Control_SetVisible(gHandles["imgHealthBGSlice1"], toggle)
	Control_SetVisible(gHandles["imgHealthBGSlice2"], toggle)
	Control_SetVisible(gHandles["imgHealthBGSlice3"], toggle)
	Control_SetVisible(gHandles["imgHealthBar"], toggle)
	Control_SetVisible(gHandles["imgHealthIcon"], toggle)
	toggleIconsLocation()
end

toggleEnergy = function(toggle)
	if toggle and m_vehicleMode then return end
	Control_SetVisible(gHandles["imgEnergyBGSlice1"], toggle)
	Control_SetVisible(gHandles["imgEnergyBGSlice2"], toggle)
	Control_SetVisible(gHandles["imgEnergyBGSlice3"], toggle)
	Control_SetVisible(gHandles["imgEnergyBar"], toggle)
	Control_SetVisible(gHandles["imgEnergyIcon"], toggle)
	toggleIconsLocation()
end

toggleArmorText = function(toggle)
	if toggle and m_vehicleMode then return end
	Control_SetVisible(gHandles["imgArmorText"], toggle)
	Control_SetVisible(gHandles["stcArmorText"], toggle)
end

toggleHealthText = function(toggle)
	if toggle and m_vehicleMode then return end
	Control_SetVisible(gHandles["imgHealthText"], toggle)
	Control_SetVisible(gHandles["stcHealthText"], toggle)
end

toggleEnergyText = function(toggle)
	if toggle and m_vehicleMode then return end
	Control_SetVisible(gHandles["imgEnergyText"], toggle)
	Control_SetVisible(gHandles["stcEnergyText"], toggle)
end

toggleVehicleHealth = function(toggle)
	if toggle and not m_vehicleMode then return end
	Control_SetVisible(gHandles["imgVehicleHealthBGSlice1"], toggle)
	Control_SetVisible(gHandles["imgVehicleHealthBGSlice2"], toggle)
	Control_SetVisible(gHandles["imgVehicleHealthBGSlice3"], toggle)
	Control_SetVisible(gHandles["imgVehicleHealthBar"], toggle)
	Control_SetVisible(gHandles["imgVehicleHealthIcon"], toggle)
end

toggleFuel = function(toggle)
	if toggle and not m_vehicleMode then return end
	Control_SetVisible(gHandles["imgFuelBGSlice1"], toggle)
	Control_SetVisible(gHandles["imgFuelBGSlice2"], toggle)
	Control_SetVisible(gHandles["imgFuelBGSlice3"], toggle)
	Control_SetVisible(gHandles["imgFuelBar"], toggle)
	Control_SetVisible(gHandles["imgFuelIcon"], toggle)
end

toggleVehicleHealthText = function(toggle)
	if toggle and not m_vehicleMode then return end
	Control_SetVisible(gHandles["imgVehicleHealthText"], toggle)
	Control_SetVisible(gHandles["stcVehicleHealthText"], toggle)
end

toggleFuelText = function(toggle)
	if toggle and not m_vehicleMode then return end
	Control_SetVisible(gHandles["imgFuelText"], toggle)
	Control_SetVisible(gHandles["stcFuelText"], toggle)
end

-- Add comma to separate thousands
function comma_value(amount)
  	local formatted = amount
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end

-- Called when the toolbar is hidden or revealed due via animation.
toggleHUDMinimized = function(minimized, instant, forceSwitch)
	if minimized and not canMinimizeToolbar() and not forceSwitch then return end

	-- Dynamic HUD A/B test, override min/max if dynamic rules not met.
	if not minimized and not m_dynamicEnable and ( canMinimizeToolbar() or forceSwitch) then
		minimized = true
	end

	if instant then
		local menuLoc = MenuGetLocationThis()
		local screenHeight = Dialog_GetScreenHeight(gDialogHandle)

		m_minimizeAnimation:stop()
		MenuSetLocationThis(menuLoc.x, screenHeight - MINIMIZE_DISPLACEMENT)	
		m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT, MINIMIZE_DURATION, nil, Easing.inOutQuad)

		MenuSetLocationThis(menuLoc.x, screenHeight + (minimized and 0 or -MINIMIZE_DISPLACEMENT))	
		m_minimized = minimized
		toggleIconsLocation()

	elseif m_minimized ~= minimized then
		m_minimizeAnimation:stop()
		if minimized then
			ContextualHelper.hideHelp(ContextualHelp.STATUS_AREA)
			ContextualHelper.hideHelp(ContextualHelp.STATUS_HEALTH)
			ContextualHelper.hideHelp(ContextualHelp.STATUS_ENERGY)
			m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT + ICONS_OFFSET, MINIMIZE_DURATION * 2, nil, Easing.inOutQuad, true)
		else
			m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT - ICONS_OFFSET, MINIMIZE_DURATION, nil, Easing.inOutQuad, true)
		end
		m_minimizeAnimation:reverse(not minimized)
		m_minimizeAnimation:start()
		m_isAnimating = true
	end

	m_minimized = minimized
	
end

-- Called to determine if the toolbar is in a valid state to minimize.
canMinimizeToolbar = function()
	return (not m_starvationEnabled or m_maxEnergy == 0 or m_currEnergy / m_maxEnergy >= MINIMIZE_HEALTH_THRESHOLD) 
			and (m_maxHealth == 0 or m_currHealth / m_maxHealth >= MINIMIZE_HEALTH_THRESHOLD) 
			and (m_toolbeltMinimized or MenuIsOpen("ProgressMenu.xml"))
			and not m_showingContextualHelp
end

toggleIconsLocation = function( )
	curLoc = MenuGetLocationThis()
	-- Adjust positioning based on what is visible
	local tempOffset = 0
	if  Control_GetVisible(gHandles["imgArmorBar"]) == 0  then tempOffset = tempOffset + BAR_OFFSET end
	if not m_vehicleMode and not m_healthEnabled then tempOffset = tempOffset + BAR_OFFSET end		
	if not m_vehicleMode and not m_energyEnabled and not m_healthEnabled then tempOffset = tempOffset + BAR_OFFSET end

	if m_minimized and not m_isAnimating then 
		tempOffset = BAR_OFFSET * 3
	end
	local statusIconHeight = ICONS_OFFSET
	local mapIconHeight = MAP_OFFSET
	MenuSetLocation(STATUS_ICONS, curLoc.x + 5, curLoc.y - statusIconHeight + tempOffset) -- TO DO: Adjust these numbers based on whether armor, etc. is visible
	local mapIconBuffer = {x = 6, y = 15}
	local tempMapY = curLoc.y - statusIconHeight - mapIconHeight + mapIconBuffer.y + tempOffset
	local maxMapY = Dialog_GetScreenHeight(gDialogHandle) - mapIconHeight + 5
	if tempMapY > maxMapY then
		tempMapY = maxMapY
	end
	MenuSetLocation(MAP_ICON, curLoc.x + mapIconBuffer.x, tempMapY)

	Control_SetLocationY(gHandles["btnBarSwitch"], tempOffset)

	if MenuIsOpen(DANCE_ICON) then
		MenuSetLocation(DANCE_ICON, curLoc.x - DANCE_OFFSET_X, tempMapY - DANCE_OFFSET_Y)
	end

	if tempOffset > 0 then
		MenuSendToBackThis()
	end

	-- send an event to the menu with all the information 
	local eventParams = {}
	local menuEvent = KEP_EventCreate("bottomHUDMinimizedEvent")
	eventParams.minimized = m_minimized
	KEP_EventEncodeString(menuEvent, Events.encode(eventParams))
	KEP_EventQueue(menuEvent)

end

toggleHUDMode = function()

	if m_vehicleMode then
		toggleArmor(false)
		toggleEnergy(false)
		toggleHealth(false)
		toggleArmorText(false)
		toggleHealthText(false)
		toggleEnergyText(false)
		toggleVehicleHealth(true)
		toggleFuel(true)

		setFuelBar()
		setVehicleHealthBar()
	else
		toggleArmor(m_armorRating > 0 and m_armorEnabled)
		toggleEnergy(m_energyEnabled)
		toggleHealth(m_healthEnabled)
		toggleVehicleHealthText(false)
		toggleFuelText(false)
		toggleVehicleHealth(false)
		toggleFuel(false)
	end
end

function canShowContextualHelp()
	return (not GetHideToolbeltSetting() or not m_minimized) 
end