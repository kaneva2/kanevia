--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("ChatHelper.lua")

g_game_id = 0

g_MenuDestroyed = false

g_chatEnabled = false
g_pauseBtnHandle = 0

g_replyTo=""

KDP_GAMEOVER_EVENT = "KDPGameOverEvent"
INFO_BOX_EVENT = "InfoBoxEvent"

KDP_MENU_CLOSE_EVENT = "KDPCloseEvent"

REPLY_UPDATE_EVENT = "ReplyUpdateEvent"
REPLY_REQUEST_EVENT = "ReplyRequestEvent"

ORIGINAL_PROGRESS_WIDTH = 388

function postedScoreHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local playerId = KEP_EventDecodeNumber( event );
	local gameId = KEP_EventDecodeNumber( event );
	local levelId = KEP_EventDecodeNumber( event );
	local gameInstanceId = KEP_EventDecodeNumber( event );
	local score = KEP_EventDecodeNumber( event );
	local highscore = KEP_EventDecodeNumber( event );
	local fameGainAmount = KEP_EventDecodeNumber( event )
	local newFameLevel = KEP_EventDecodeNumber( event )
	local percentToNextLevel = KEP_EventDecodeNumber( event )
	local rewardsAtNextLevel = KEP_EventDecodeString( event )
	local gainMessage = KEP_EventDecodeString( event )
	local totalFame = KEP_EventDecodeNumber( event )

	if ( totalFame ~= 0 ) then
		Static_SetText(Dialog_GetStatic(gDialogHandle, "lblMyHighScore"), totalFame)
	end

	if ( levelId == 10 ) then
		makeWebCall( GameGlobals.WEB_SITE_PREFIX .. DANCE_TO_LEVEL10, WF.KDP)
	end

	local ich = Dialog_GetImage(gDialogHandle, "imgProgressBar")
	if ich ~= 0 then
		Control_SetVisible(ich, true)
		progressWidth = ORIGINAL_PROGRESS_WIDTH * ( percentToNextLevel / 100 )
		Control_SetSize(ich, progressWidth, 20)
	end

	return KEP.EPR_OK
end

function readyToPlayHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	-- clear the Press TAB to play text
--	g_game_id = KEP_EventDecodeNumber( event )
	Static_SetText(Dialog_GetStatic(gDialogHandle, "GamePlay"), "")
	return KEP.EPR_OK
end

function closeDialogHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
--	local gameId = KEP_EventDecodeNumber( event );
	return KEP.EPR_OK
end

function gameLimitExceededHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	dh = MenuOpen("InfoBox.xml")
	if dh ~= 0 then
		local ev = KEP_EventCreate( INFO_BOX_EVENT )
		KEP_EventEncodeString( ev, "The dance floor is currently at max capacity. Please try again later." )
		KEP_EventQueue( ev )
	end

	if ( g_MenuDestroyed == false) then
		g_MenuDestroyed = true
		MenuCloseThis()
	end
end

function gameOverDialogHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	MenuOpenModal("kdp_gameover.xml")
	
	local yesnoEvent = KEP_EventCreate( KDP_GAMEOVER_EVENT )
	KEP_EventSetFilter( yesnoEvent, KEP.FILTER_KDP_ANSWER_PLAY)
	gameId = KEP_EventDecodeNumber( event )
	KEP_EventEncodeNumber( yesnoEvent, KEP_EventDecodeNumber(event) )
	KEP_EventEncodeNumber( yesnoEvent, KEP_EventDecodeNumber(event) )
	KEP_EventEncodeString( yesnoEvent, KEP_EventDecodeString(event) )
	KEP_EventEncodeString( yesnoEvent, KEP_EventDecodeString(event) )
	KEP_EventQueue( yesnoEvent )

	-- answer caught in kdp_worldobject1:answerEventHandler
	g_MenuDestroyed = true
	MenuCloseThis()

	return KEP.EPR_OK
end

function postedScores(dispatcher, fromNetId, event, eventid, filter, objectid)

	local player_id = KEP_EventDecodeNumber( event )
	local game_id   = KEP_EventDecodeNumber( event )
	local level_id  = KEP_EventDecodeNumber( event )
	local num_players = KEP_EventDecodeNumber( event )

	local highScore_name  = KEP_EventDecodeString( event )
	local highScore_score = Event_DecodeNumver( event )

	local g_t_scores = {}

	-- TODO: remove line, testing purposes only
	player_id = 1010

	local sh = Dialog_GetStatic(gDialogHandle, "lblHighName")
	if sh ~= 0 then
		Static_SetText(sh, player_id)
	end

	local sh = Dialog_GetStatic(gDialogHandle, "lblHighScore")
	if sh ~= 0 then
		Static_SetText(sh, game_id)
	end

	local lbhScoreNames = Dialog_GetListBox(gDialogHandle, "lstScoreNames")
	local lbhScores = Dialog_GetListBox(gDialogHandle, "lstScores")

	if lbhScoreNames ~= 0 and lbhScores ~= 0 then
		ListBox_RemoveAllItems(lbhScoreNames)
		ListBox_RemoveAllItems(lbhScores)

		if num_players > 0 then
			debug_string = ""
			for i = 0, num_players - 1  do
				local current_player_id     = KEP_EventDecodeNumber( event )
				local current_player_name   = KEP_EventDecodeString( event )
				local current_player_score  = KEP_EventDecodeNumber( event )
				local current_player_status = KEP_EventDecodeNumber( event )

				local t_score = {}
				t_score["player_id"]     = current_player_id
				t_score["player_name"]   = current_player_name
				t_score["player_score"]  = current_player_score
				t_score["player_status"] = current_player_status

				table.insert(g_t_scores, t_score)
			end

			table.sort(g_t_scores, function(a, b) return a["player_score"] > b["player_score"] end)

			for i = 1, #g_t_scores do
				colorString = "FF000000"

				current_score = g_t_scores[i]
				if current_score["player_id"] == player_id then
					if current_score["player_status"] == 0 then
						colorString = "FFFFFFFF"  -- White: current player is active
					else
						colorString = "FFFF0000"  -- Red: current player is inactive
					end
				else
					if current_score["player_status"] == 0 then
						colorString = "FF000000" -- Black: other player is active
					else
						colorString = "FFFCFF00" -- Gold: other player is inactive
					end
				end

				ListBox_AddItem(lbhScoreNames, "<font color=" .. colorString .. ">" .. current_score["player_id"] .. "   " .. current_score["player_score"] .. "</font>", current_score["player_id"])
			end
		end
	end

	return KEP.EPR_OK
end

function Dialog_OnMoved( )
	local resizeKDP1Game = KEP_EventCreate( KEP.KDP1_MENU_EVENT )
	KEP_EventEncodeNumber( resizeKDP1Game, g_game_id )
	KEP_EventEncodeNumber( resizeKDP1Game, Dialog_GetLocationX(gDialogHandle) )
	KEP_EventEncodeNumber( resizeKDP1Game, Dialog_GetLocationY(gDialogHandle) )
	KEP_EventQueue( resizeKDP1Game )
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandlerFiltered( "postedScoreHandler", KEP.KDP1_EVENT, KEP.FILTER_POSTED_SCORE, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFiltered( "readyToPlayHandler", KEP.KDP1_EVENT, KEP.FILTER_READY_TO_PLAY, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFiltered( "closeDialogHandler", KEP.KDP1_EVENT, KEP.FILTER_CLOSE_DIALOG, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFiltered( "gameOverDialogHandler", KEP.KDP1_EVENT, KEP.FILTER_GAME_OVER_DIALOG, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFiltered( "gameLimitExceededHandler", KEP.KDP1_EVENT, KEP.FILTER_ACCESS_DENIED, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFiltered( "pauseResponseHandler", KEP.KDP1_EVENT, KEP.FILTER_PAUSE_RESPONSE, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ReplyUpdateHandler", REPLY_UPDATE_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegisterSecure( KEP.KDP1_EVENT, KEP.MED_PRIO, KEP.ESEC_SERVER + KEP.ESEC_CLIENT, KEP.ESEC_SERVER + KEP.ESEC_CLIENT )
	KEP_EventRegister( REPLY_UPDATE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( REPLY_REQUEST_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( KEP.WORLD_FAME_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	g_pauseBtnHandle = Dialog_GetButton( dialogHandle, "btnPause" )
	RequestLastReplyFromChat()
end

function Dialog_OnDestroy(dialogHandle)

	--let client script managing game know I left
	local endKDP1Game = KEP_EventCreate( KDP_MENU_CLOSE_EVENT )
	if endKDP1Game ~= 0 then
		KEP_EventEncodeNumber( endKDP1Game, dialogHandle )
		KEP_EventQueue( endKDP1Game )
	end

	--let ddr know menu closed so game over
	endKDP1Game = KEP_EventCreate( KEP.KDP1_EVENT )
	if endKDP1Game ~= 0 then
		KEP_EventEncodeNumber( endKDP1Game, g_game_id )
		KEP_EventSetFilter( endKDP1Game, KEP.FILTER_END_GAME)
		KEP_EventQueue( endKDP1Game )
	end

	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnCancel_OnButtonClicked( buttonHandle )
	if ( g_MenuDestroyed == false) then
		g_MenuDestroyed = true
		MenuCloseThis()
	end
end

function ReplyUpdateHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local reply = KEP_EventDecodeString( event )
	if reply ~= "" and reply ~= nil then
		g_replyTo = reply
	end
	return KEP.EPR_OK
end

--Fire event to main chat menu to see who the last person we replied to was
function RequestLastReplyFromChat()
	e = KEP_EventCreate( REPLY_REQUEST_EVENT )
	if e ~= 0 then
		KEP_EventEncodeNumber( e, gDialogHandle )
		KEP_EventQueue( e )
	end
end

function btnPause_OnButtonClicked( buttonHandle )
	e = KEP_EventCreate( KEP.KDP1_EVENT )
	if e~= 0 then
		KEP_EventSetFilter( e, KEP.FILTER_PAUSE_REQUEST )
		KEP_EventQueue( e )
	end
end

function pauseResponseHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = KEP_EventDecodeNumber( event )
	sh = Button_GetStatic( g_pauseBtnHandle )
	if sh ~= 0 then
		if answer == 1 then
			Static_SetText( sh, "Resume" )
		else
			Static_SetText( sh, "Pause" )
		end
	end
end
