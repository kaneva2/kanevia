--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")

local ADVANCED_MENU_ID = 10685
local DISABLED_ADJUST_ANGLE = (math.pi/2) -- 90 degrees

function onCreate()

	KEP_EventRegisterHandler( "volumeChangedEventHandler", "VolumeChangedEvent", KEP.HIGH_PRIO )
	showVolume()
	
	-- ED-7826 Hide camera level slider for duration of A/B test.
	--showCameraLevel()
	-- ED-8005 - Show a checkbox to enable / disable camera autoadjust
	showCameraSetting()
	setControlEnabled( "lblCameraLevel", false )
	setControlEnabled( "sldCameraLevel", false )
	--Control_SetLocationY(gHandles["btnBuildSettings"], Control_GetLocationY(gHandles["btnBuildSettings"]) - 76)

-- ED-7572 - Deprecate Multiple Cameras
--	Control_SetEnabled( Dialog_GetControl( gDialogHandle, "btnView" ), KEP_GetCameraLock( )==0 )
	setControlEnabled( "btnView", false )
	
	CheckBox_SetChecked(gHandles["chkMute"], KEP_GetMute())
end

function Dialog_OnDestroy(dialogHandle)
	KEP_WriteConfigSettings()
	Helper_Dialog_OnDestroy( dialogHandle )
end

function chkMute_OnCheckBoxChanged( checkboxHandle, checked )
	KEP_SetMute(checked)
end

function chkCameraLevel_OnCheckBoxChanged( checkboxHandle, checked )
	if checked == 1 then
		SetCameraLevel(1)
	else
		SetCameraLevel(0)
	end
end

function volumeChangedEventHandler()
	Slider_SetValue( gHandles["sldAudioVolume"], KEP_GetMasterVolume())
	CheckBox_SetChecked(gHandles["chkMute"], KEP_GetMute())
end

function showVolume()
	Slider_SetRange( gHandles["sldAudioVolume"], 0, 100)
	Slider_SetValue( gHandles["sldAudioVolume"], KEP_GetMasterVolume())
end

function sldAudioVolume_OnSliderValueChanged( slider, val )
	KEP_SetMasterVolume(val)
end

-- ED-7612 - Player Camera Auto-Elevation
function showCameraLevel()
	Slider_SetRange( gHandles["sldCameraLevel"], 0, 100)
	local val = KEP_GetPlayerCameraAutoElevation()
	local pct = 100 - (val / (math.pi/2)) * 100
	Slider_SetValue( gHandles["sldCameraLevel"], pct)
end
function sldCameraLevel_OnSliderValueChanged( slider, val )
	local radians = ((100 - val) / 100) * (math.pi/2)
	KEP_SetPlayerCameraAutoElevation(radians)
end

function showCameraSetting()
	local val = KEP_GetPlayerCameraAutoElevation()
	CheckBox_SetChecked(gHandles["chkCameraLevel"], (val < DISABLED_ADJUST_ANGLE))
end

function btnBuildSettings_OnButtonClicked( buttonhandle )
	local ev = KEP_EventCreate( "BuildOpenMenuEvent")
	KEP_EventSetFilter(ev, ADVANCED_MENU_ID)
	KEP_EventQueue( ev)
end
