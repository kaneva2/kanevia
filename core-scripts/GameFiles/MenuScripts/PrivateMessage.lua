--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("ChatHelper.lua")

g_chat_type = ctPrivate
PRIVATE_MESSAGE_EVENT = "PrivateMessageEvent"
PLAYER_INFO_NAME = 11 -- Jonny loves magik numbers
g_netID = -1
g_targetPlayer = ""

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "PMInitHandler", PRIVATE_MESSAGE_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( PRIVATE_MESSAGE_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	Helper_Chat_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

---------------------------------------------------------------------
-- 'ENTER' key was pressed within txtChat
--  not using txtChat because diff event for
--  private.  This should work -- Jonny
---------------------------------------------------------------------
function txtPrivate_OnEditBoxString(editBoxHandle, password)
	local playerId = g_netID -- net assigned ID of recipient
	local to = g_targetPlayer -- name of recipient
	local msg = EditBox_GetText(editBoxHandle)

	-- send private message because g_chat_type == 3, private.
	if msg ~= "" then
		local from = ""
		local distribution = "R" .. playerId .. ":" .. to
		local t = { KEP_GetPlayerInfo() }
		if t ~= 0  then
			local num_args = #t
			from = t[PLAYER_INFO_NAME]
		end
		KEP_SendChatMessageBroadcast( g_chat_type, msg, distribution, from )
	end

	MenuCloseThis()
end

function PMInitHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_netID = KEP_EventDecodeNumber( event )
	g_targetPlayer = KEP_EventDecodeString( event )
	if gDialogHandle ~= nil then
		if g_targetPlayer ~= nil and g_targetPlayer ~= "" then
			Dialog_SetCaptionText(gDialogHandle, "Private Message to " .. g_targetPlayer )
		end
	end
end
