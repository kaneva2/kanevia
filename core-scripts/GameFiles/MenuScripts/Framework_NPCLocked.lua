--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_ActorHelper.lua")

function onCreate()
	-- Register client - client event handlers
	KEP_EventRegisterHandler("lockedHandler", "FRAMEWORK_NPC_LOCKED", KEP.HIGH_PRIO)
	CloseAllActorMenus()
end

-- Handled when a client needs to use the NPC Dialog menu
function lockedHandler(dispatcher, fromNetid, event, eventID, filter, objectID)
	
	local line1 = KEP_EventDecodeString(event)
	local line2 = KEP_EventDecodeString(event)
	

	Static_SetText(gHandles["stcActor"], line1)
	Static_SetText(gHandles["stcNeed"], line2)
end
