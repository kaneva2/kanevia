--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\KEP.lua")

-- Default starting coords
DEFAULT_X  = 300
DEFAULT_Y  = 1

function loadLocation(dh, tag )
	local success = false

	--Width and height of the game window.
	--Used to make sure windows dont open off screen.
	local screenHeight = Dialog_GetScreenHeight( dh )
	local screenWidth  = Dialog_GetScreenWidth( dh )
	local config = KEP_ConfigOpenWOK()
	if config ~= 0 then
		local okfilterx, x = KEP_ConfigGetNumber( config, tag.."X", DEFAULT_X )
		local okfiltery, y = KEP_ConfigGetNumber( config, tag.."Y", DEFAULT_Y )
		if okfilterx == 0 then
			okfilterx = false
		end
		if okfiltery == 0 then
			okfiltery = false
		end
		if okfilterx and okfiltery then
			if x > screenWidth or x < 0 or y > screenHeight or y < 0 then
				x = DEFAULT_X
				y = DEFAULT_Y
			end
			Dialog_SetLocation( dh, x, y )
			success = true
		end
	end
	return success
end

function saveLocation( dh, tag )
	local x = DEFAULT_X
	local y = DEFAULT_Y
	x = Dialog_GetLocationX( dh )
	y = Dialog_GetLocationY( dh )
	local config = KEP_ConfigOpenWOK()
	if config ~= 0 then
		KEP_ConfigSetNumber( config, tag.."X", x )
		KEP_ConfigSetNumber( config, tag.."Y", y )
		KEP_ConfigSave( config )
	end
end
