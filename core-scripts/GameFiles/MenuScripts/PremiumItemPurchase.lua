--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("CreditBalances.lua")
dofile("..\\ClientScripts\\PlayerInformation.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

PREMIUM_ITEM_EVENT = "PremiumItemEvent"
PLAYER_INFO_EVENT = "PlayerInfoEvent"
PLAYER_INFO_USERID_REQUEST = 1
PLAYER_INFO_USERID_RESPONSE = 2
PLAYER_INFO_PLAYERID_REQUEST = 3
PLAYER_INFO_PLAYERID_RESPONSE = 4
PREMIUM_ITEM_PURCHASE_SCRIPT = 30000

g_globalId = -1
g_quantity = -1

g_price = 0
g_appName = 0
g_playerCredits = 0
g_userId = 0

g_result_text = ""
g_received_premium_item_info = false
g_received_user_balance_info = false

g_not_enough_credits = false

g_successful_purchase = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandlerFiltered( "attribEventHandler", "AttribEvent", 51, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "premiumItemEventHandler", PREMIUM_ITEM_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "PlayerInfoEventHandler", PLAYER_INFO_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( PREMIUM_ITEM_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == 51 then
		if g_successful_purchase then
			KEP_SendMenuEvent( 4, PREMIUM_ITEM_PURCHASE_SCRIPT, 0, g_globalId, g_quantity )
			MenuCloseThis()
		end
	end
end

function premiumItemEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == 3024 then
		g_globalId = KEP_EventDecodeNumber(event)
		g_quantity = KEP_EventDecodeNumber(event)
		local web_address = GameGlobals.WEB_SITE_PREFIX .. "kgp/premiumitems.aspx?gid=" .. KEP_GetGameId() .. "&glid=" .. g_globalId .. "&qty=" ..  g_quantity
		makeWebCall( web_address, WF.PREMIUM_ITEM_INFO)
	end
end

function PlayerInfoEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter == PLAYER_INFO_USERID_RESPONSE) then
		g_userId = KEP_EventDecodeNumber(event)
		getBalances(g_userId, WF.HUD_BALANCES)
	end
end

function parseDataByTag(strEventData, strTag, strPattern, startPos)
	local strStartTag = "<" .. strTag .. ">"
	local strEndTag = "</" .. strTag .. ">"
	local strPattern = strStartTag..strPattern..strEndTag
	local iStart, iEnd, strData = string.find(strEventData, strPattern, startPos)
	return iStart, iEnd, strData
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.PREMIUM_ITEM_INFO then
		g_result_text = KEP_EventDecodeString( event )
		g_received_premium_item_info = true
		displayData()
	elseif filter == WF.PREMIUM_ITEM_PURCHASE then
		result_text = KEP_EventDecodeString( event )
		parseResult( result_text )
	elseif filter == WF.HUD_BALANCES then
		local estr = KEP_EventDecodeString( event )
		local regCredits, giftCredits = ParseBalances(estr)
		g_playerCredits = regCredits
		g_received_user_balance_info = true
		displayData()
	end
end

function displayData()
	if ( g_received_premium_item_info and g_received_user_balance_info ) then

		local tableStart, tableEnd, item_text = parseDataByTag(g_result_text, "Table", "(.-)", 1)
		if (item_text == nil) then return end
		glidStart, glidEnd, glid  = parseDataByTag(item_text, "global_id", "(%d+)")
		nameStart, nameEnd, name  = parseDataByTag(item_text, "display_name", "(.-)")
		priceStart, priceEnd, g_price = parseDataByTag(item_text, "total_cost", "(.-)")

		sh = Dialog_GetStatic( gDialogHandle, "stcCredits_Quantity" )
		Static_SetText( sh, g_playerCredits )

		sh = Dialog_GetStatic( gDialogHandle, "stcLine2_Dynamic" )
		Static_SetText( sh, name )

		sh = Dialog_GetStatic( gDialogHandle, "stcLine3_Dynamic" )
		Static_SetText( sh, g_price .. " Credits" )

		local gamename = KEP_GetGameName()

		sh = Dialog_GetStatic( gDialogHandle, "stcLine5_Dynamic" )
		Static_SetText( sh, gamename )

		if ( tonumber(g_playerCredits) < tonumber(g_price) ) then
			g_not_enough_credits = true

			local btn = Dialog_GetControl(gDialogHandle, "btnYes")
			local txt = Button_GetStatic(btn)
			Static_SetText(txt, "Buy Credits")

			sh = Dialog_GetStatic( gDialogHandle, "stcError" )
			Static_SetText( sh, "Not enough Credits to purchase that item." )
			Control_SetVisible( sh, true )
		end

		local bh = Dialog_GetButton(gDialogHandle, "btnYes")
		if bh ~= 0 then
			Control_SetEnabled(bh, true)
			Control_SetVisible(bh, true)
		end

		bh = Dialog_GetButton(gDialogHandle, "btnNo")
		if bh ~= 0 then
			Control_SetEnabled(bh, true)
			Control_SetVisible(bh, true)
		end
	end
end

function parseResult( strXmlData )
	local result = parseEventHeader(strXmlData)
	if result == 0 then
		-- Item was purchased successfully
		local premiumItemEvent = KEP_EventCreate( PREMIUM_ITEM_EVENT)
		KEP_EventEncodeNumber(premiumItemEvent, g_globalId )
		KEP_EventEncodeNumber(premiumItemEvent, g_quantity )
		KEP_EventSetFilter(premiumItemEvent, 2)
		KEP_EventQueue( premiumItemEvent )
		KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
		g_successful_purchase = true
	elseif result == 1 then
		-- Not Enough Rewards
		KEP_MessageBox("Not Enough Rewards!")
		KEP_SendMenuEvent( 4, PREMIUM_ITEM_PURCHASE_SCRIPT, result, 0, 0 )
		MenuCloseThis()
	elseif result == 2 then
		-- Not Enough Credits
		KEP_MessageBox("Not Enough Credits!")
		KEP_SendMenuEvent( 4, PREMIUM_ITEM_PURCHASE_SCRIPT, result, 0, 0 )
		MenuCloseThis()
	else
		-- Some other reason
		KEP_MessageBox("Error Purchasing Item!")
		KEP_SendMenuEvent( 4, PREMIUM_ITEM_PURCHASE_SCRIPT, result, 0, 0 )
		MenuCloseThis()
	end
end

function parseEventHeader(strXmlData)
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(strXmlData, "<ReturnCode>(%d+)</ReturnCode>")
	if (result == nil) then
		return -1
	else
		return tonumber(result)
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	sh = Dialog_GetStatic( gDialogHandle, "stcError" )
	Control_SetVisible( sh, false )

	local evt = KEP_EventCreate( PLAYER_INFO_EVENT)
	KEP_EventSetFilter(evt, PLAYER_INFO_USERID_REQUEST)
	KEP_EventQueue( evt)
end

function btnYes_OnButtonClicked( buttonHandle)
	if g_not_enough_credits then
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA ..BUY_CREDITS_SUFFIX )
		KEP_SendMenuEvent( 4, PREMIUM_ITEM_PURCHASE_SCRIPT, 3, 0, 0 )
		MenuCloseThis()
	else
		local web_address = GameGlobals.WEB_SITE_PREFIX.."kgp/instantBuy.aspx?operation=buy&credit=KPOINT&premium=true" .."&items="..g_globalId.."&qnty="..g_quantity
		makeWebCall( web_address, WF.PREMIUM_ITEM_PURCHASE)
		local bh = Dialog_GetButton(gDialogHandle, "btnYes")
		if bh ~= 0 then
			Control_SetEnabled(bh, false)
		end
	end
end

function btnNo_OnButtonClicked( buttonHandle)
	KEP_SendMenuEvent( 4, PREMIUM_ITEM_PURCHASE_SCRIPT, 3, 0, 0 )
	MenuCloseThis()
end

function btnClose_OnButtonClicked( buttonHandle)
	KEP_SendMenuEvent( 4, PREMIUM_ITEM_PURCHASE_SCRIPT, 3, 0, 0 )
	MenuCloseThis()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

