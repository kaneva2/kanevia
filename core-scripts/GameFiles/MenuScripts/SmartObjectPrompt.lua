--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- SmartObjectPrompt.lua - Smart Object Prompt
-- 
-- Opens when you try to place a smart object in a zone that's not a world.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")

FULL_MENU_HEIGHT = 410
WORLD_CONTROLS = {"stcLine3", "stcLine4", "lstWorlds", "btnGo"}

g_myApps = {}

-- InitializeKEPEventHandlers - Register all script event handlers here. 
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) -- REQUIRED - DO NOT REMOVE

	ListBox_SetHTMLEnabled(gHandles["lstWorlds"], true)
	requestApps()

end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle ) -- REQUIRED - DO NOT REMOVE
end

function btnCreateWorld_OnButtonClicked(btnHandle)
	MenuOpenModal("CreateYourWorld.xml")
	MenuCloseThis()
end

function btnGo_OnButtonClicked(buttonHandle)
	local selectedIndex = ListBox_GetSelectedItemIndex(gHandles["lstWorlds"]) + 1
	if selectedIndex ~= nil and selectedIndex > 0 then
		local appData = g_myApps[selectedIndex]
		if appData ~= nil and (appData.serverStatus > 0 or appData.incubatorHosted == 1) then
			local url = "kaneva://" .. appData.gameId .. "/"
			gotoURL(url)
		else
			KEP_MessageBox("This app is not available.")
		end
	end
end

function requestApps()
	web_address = GameGlobals.WEB_SITE_PREFIX .. GET_MY_3DAPPS_SUFFIX
	makeWebCall(web_address, WF.GET_MY_3DAPPS_PROMPT, 1, 1000000) --NOTE: PLEASE don't have more than 1 million apps!
	-- TO BrowserPageReadyHandler
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.GET_MY_3DAPPS_PROMPT then
		local eventData = KEP_EventDecodeString(event)
		parseMy3DApps(eventData)
	end
end

function parseMy3DApps(ret)

	g_myApps = {}
	s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		local s, e, numRecs = string.find(ret, "<NumberRecords>(.-)</NumberRecords>")
		numRecs = tonumber(numRecs)
		if numRecs > 0 then
			local start = 0
			local last = 0
			for i=1,numRecs do
				s, e, appRecord = string.find(ret, "<3DApp (.-) />", e)
				local appData = {}
				start, last, appData.name = string.find(appRecord, 'Name="(.-)" Description')
				start, last, appData.description = string.find(appRecord, 'Description="(.-)" GameId')
				start, last, appData.gameId = string.find(appRecord, 'GameId="(.-)" CommunityId')
				start, last, appData.communityId = string.find(appRecord, 'CommunityId="(.-)" ThumbnailURL')
				start, last, appData.thumbnail = string.find(appRecord, 'ThumbnailURL="(.-)" OwnerUsername')
				start, last, appData.owner = string.find(appRecord, 'OwnerUsername="(.-)" IsPublic')
				start, last, appData.isPublic = string.find(appRecord, 'IsPublic="(.-)" ServerStatusId')
				start, last, appData.serverStatus = string.find(appRecord, 'ServerStatusId="(.-)" GameStatusId')
				start, last, appData.gameStatus = string.find(appRecord, 'GameStatusId="(.-)" GameAccessId')
				start, last, appData.gameAccess = string.find(appRecord, 'GameAccessId="(.-)" IncubatorHosted')
				start, last, appData.incubatorHosted = string.find(appRecord, 'IncubatorHosted="(.-)"')
				appData.gameId = tonumber(appData.gameId)
				appData.communityId = tonumber(appData.communityId)
				appData.isPublic = (appData.isPublic == "Y")
				appData.serverStatus = tonumber(appData.serverStatus)
				appData.gameStatus = tonumber(appData.gameStatus)
				appData.gameAccess = tonumber(appData.gameAccess)
				appData.incubatorHosted = tonumber(appData.incubatorHosted)		
				table.insert(g_myApps, appData)
			end
		else
			return
		end
	else
		return
	end

	populateWorlds()
	expandMenu()
end

function tableToString( tbl, header )
	local returnString = "\n"

	if not header then
		header = ""
	end

	for i, v in pairs(tbl) do
		returnString = returnString .. header .. tostring(i) .. ": " .. tostring(v) .. "\n"

		if type(v) == "table" then
			returnString = returnString .. tableToString(v, "\t" .. header .. tostring(i) .. " ")
		end
	end

	return returnString
end

function populateWorlds()
	ListBox_RemoveAllItems(gHandles["lstWorlds"])
	for i,v in ipairs(g_myApps) do
		fontStart = "<font color=FF000000>"
		fontEnd = "</font>"
		extraStatus = ""
		if v.gameAccess == 2 then
			extraStatus = " (private)"
		elseif v.gameAccess == 4 then
			fontStart = "<font color=FFFF0000><i>"
			fontEnd = "</i></font>"
			extraStatus = " (inaccessible - connectivity issues)"
		end
		if v.serverStatus == 0 and v.incubatorHosted == 0 then
			fontStart = "<font color=FF999999><i>"
			fontEnd = "</i></font>"
			extraStatus = " (offline)"
		end
		ListBox_AddItem(gHandles["lstWorlds"], fontStart .. v.name .. extraStatus .. fontEnd, v.communityId)
	end
end

function expandMenu()
	local width = Dialog_GetWidth(gDialogHandle)
	Dialog_SetSize(gDialogHandle, width, FULL_MENU_HEIGHT)

	for i,v in ipairs(WORLD_CONTROLS) do
		Control_SetVisible(gHandles[v], true)
	end

	Dialog_SetLocation(gDialogHandle, (Dialog_GetScreenWidth(gDialogHandle) - width) / 2, (Dialog_GetScreenHeight(gDialogHandle) - FULL_MENU_HEIGHT) / 2 )
end

function lstWorlds_OnListBoxSelection(listBoxHandle, x, y, selectedIndex, selectedText)
	Control_SetEnabled(gHandles["btnGo"], true)
end

function btnClose_OnButtonClicked(btnHandle)
	DestroyMenu(gDialogHandle)
end