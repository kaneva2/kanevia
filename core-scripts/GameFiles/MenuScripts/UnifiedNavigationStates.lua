--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- UnifiedNavigationStates.lua 
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

SYSTEM_MENUS = { WOK = "Inventory.xml", GAMING = "Framework_AllGameItems.xml", BACKPACK = "Framework_Backpack.xml", QUESTS = "Framework_PlayerQuests.xml", CRAFTING = "Framework_PlayerCrafting.xml", GEMS = "Framework_PlayerMeta", NPC = "Framework_NPCInventory.xml", ZONES = "Framework_LinkedZones.xml"}

--NAVIGATION BAR STATES
NAV_STATES = 	{
					-- Inventory.xml
					CLOTHING = "clothing",
					BUILD = "build",

					-- Framework_AllGameItems.xml
					GAMING = "gaming",
					WOK_GAMING = "wokGaming",

					-- Framework_Backpack.xml
					PLAYER_BACKPACK = "playerBackpack",
					-- Framework_Quests.xml
					PLAYER_QUESTS = "playerQuests",
					-- Framework_PlayerCrafting.xml
					PLAYER_CRAFTING = "playerCrafting",
					-- Framework_PlayerMeta.xml
					GEMS = "gems",

					--Framework_NPCInventory.xml
					NPC = "npc",

					ZONES = "zone"
				}

NAV_STATE_LAYOUTS = {}

-------------------------------------------------
-- Gems
-------------------------------------------------

NAV_STATE_LAYOUTS[NAV_STATES.GEMS] = 	
	{	title = "Gems",
		coord = {left=0,right=36,top=184,bottom=220},
		actionList = 
			{
				{text = "Items", stateChange = NAV_STATES.PLAYER_BACKPACK, coord = {left=396,right=432,top=148,bottom=184}},
				{text = "Quests", stateChange = NAV_STATES.PLAYER_QUESTS, coord = {left=144,right=180,top=220,bottom=254}},
				{text = "Crafting", stateChange = NAV_STATES.PLAYER_CRAFTING, coord = {left=216,right=252,top=220,bottom=254}},
				{text = "Gems", stateChange = NAV_STATES.GEMS, coord = {left=0,right=36,top=184,bottom=220}},
				{skip = true},
				{skip = true},
				{skip = true},
				{skip = true},
				{skip = true}

			}
	}
-------------------------------------------------
-- Clothing
-------------------------------------------------
NAV_STATE_LAYOUTS[NAV_STATES.CLOTHING] = 	
	{	title = "Clothing",
		coord = {left=540,right=576,top=148,bottom=184},
		actionList = 
			{
				{text = "All", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar", coord = {left=540,right=576,top=148,bottom=184}},
				{text = "Tops", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Tops", coord = {left=180,right=216,top=184,bottom=220}},
				{text = "Bottoms", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Bottoms", coord = {left=216,right=252,top=184,bottom=220}},
				{text = "Shoes", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Shoes", coord = {left=252,right=288,top=184,bottom=220}},
				{text = "Costumes", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Costumes", coord = {left=288,right=324,top=184,bottom=220}},
				{text = "Accessories", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Accessories", coord = {left=324,right=360,top=184,bottom=220}},
				{text = "More", coord = {left=612,right=648,top=220,bottom=256}, moreOptions = { 
																								{text = "Dresses & Skirts", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Dresses & Skirts"},
																								{text = "Underwear", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Underwear"},
																								{text = "Lingerie", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Lingerie"},
																								{text = "Emotes", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Emotes"},
																								{text = "Access Pass", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Access Pass"},
																								{skip = true},
																								{skip = true},
																								{skip = true}
																								}
				},
				{skip = true},
				{skip = true}
			}
	}

NAV_STATE_LAYOUTS[NAV_STATES.NPC] = 	
	{	title = "NPC Clothing",
		coord = {left=540,right=576,top=148,bottom=184},
		actionList = 
			{
				{text = "All", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar", coord = {left=540,right=576,top=148,bottom=184}},
				{text = "Tops", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Tops", coord = {left=180,right=216,top=184,bottom=220}},
				{text = "Bottoms", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Bottoms", coord = {left=216,right=252,top=184,bottom=220}},
				{text = "Shoes", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Shoes", coord = {left=252,right=288,top=184,bottom=220}},
				{text = "Costumes", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Costumes", coord = {left=288,right=324,top=184,bottom=220}},
				{text = "Accessories", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Accessories", coord = {left=324,right=360,top=184,bottom=220}},
				{text = "Dresses", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Dresses", coord = {left=360,right=396,top=184,bottom=220}},
				{text = "Underwear", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Underwear", coord = {left=396,right=432,top=184,bottom=220}},
				{text = "Access Pass", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "avatar,Access Pass", coord = {left=288,right=323,top=148,bottom=183}},
			}
	}

-------------------------------------------------
-- Player Systems
-------------------------------------------------
NAV_STATE_LAYOUTS[NAV_STATES.PLAYER_BACKPACK] = 	
	{	title = "Items",
		coord = {left=396,right=432,top=148,bottom=184},
		actionList = 	
			{
				{text = "Items", stateChange = NAV_STATES.PLAYER_BACKPACK, coord = {left=396,right=432,top=148,bottom=184}},
				{text = "Quests", stateChange = NAV_STATES.PLAYER_QUESTS, coord = {left=144,right=180,top=220,bottom=254}},
				{text = "Crafting", stateChange = NAV_STATES.PLAYER_CRAFTING, coord = {left=216,right=252,top=220,bottom=254}},
				{text = "Gems", stateChange = NAV_STATES.GEMS, coord = {left=0,right=36,top=184,bottom=220}},
				{skip = true},
				{skip = true},
				{skip = true},
				{skip = true},
				{skip = true}
			}
	}

NAV_STATE_LAYOUTS[NAV_STATES.PLAYER_QUESTS] = 	
	{	title = "Quests",
		coord = {left=144,right=180,top=220,bottom=254},
		actionList = 	
			{
				{text = "Items", stateChange = NAV_STATES.PLAYER_BACKPACK, coord = {left=396,right=432,top=148,bottom=184}},
				{text = "Quests", stateChange = NAV_STATES.PLAYER_QUESTS, coord = {left=144,right=180,top=220,bottom=254}},
				{text = "Crafting", stateChange = NAV_STATES.PLAYER_CRAFTING, coord = {left=216,right=252,top=220,bottom=254}},
				{text = "Gems", stateChange = NAV_STATES.GEMS, coord = {left=0,right=36,top=184,bottom=220}},
				{skip = true},
				{skip = true},
				{skip = true},
				{skip = true},
				{skip = true}
			}
	}

NAV_STATE_LAYOUTS[NAV_STATES.PLAYER_CRAFTING] = 	
	{	title = "Crafting",
		coord = {left=216,right=252,top=220,bottom=254},
		actionList = 	
			{
				{text = "Items", stateChange = NAV_STATES.PLAYER_BACKPACK, coord = {left=396,right=432,top=148,bottom=184}},
				{text = "Quests", stateChange = NAV_STATES.PLAYER_QUESTS, coord = {left=144,right=180,top=220,bottom=254}},
				{text = "Crafting", stateChange = NAV_STATES.PLAYER_CRAFTING, coord = {left=216,right=252,top=220,bottom=254}},
				{text = "Gems", stateChange = NAV_STATES.GEMS, coord = {left=0,right=36,top=184,bottom=220}},
				{skip = true},
				{skip = true},
				{skip = true},
				{skip = true},
				{skip = true}
			}
	}

-------------------------------------------------
-- Building
-------------------------------------------------
NAV_STATE_LAYOUTS[NAV_STATES.BUILD] = 	
	{	title = "Building",
		coord = {left=432,right=468,top=148,bottom=184},
		actionList = 
			{
				{text = "All", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building", coord = {left=432,right=468,top=148,bottom=184}},
				{text = "Materials", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Building Materials", coord = {left=468,right=504,top=184,bottom=220}},
				{text = "Architecture", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Architecture", coord = {left=504,right=540,top=184,bottom=220}},
				{text = "Furnishing", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Furnishings", coord = {left=540,right=576,top=184,bottom=220}},
				{text = "Home", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Home and Garden", coord = {left=576,right=612,top=184,bottom=220}},
				{text = "Electronics", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Electronics & Games", coord = {left=612,right=648,top=184,bottom=220}},
				{text = "More", coord = {left=612,right=648,top=220,bottom=256}, moreOptions = { 
																								{text = "Vehicles", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Vehicles"},
																								{text = "Statues", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Statues"},
																								{text = "Gifts", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Gifts"},
																								{text = "Particles", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Particle Effects"},
																								{text = "Deeds", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Deeds"},
																								{text = "Sounds", filterEvent = "WOK_INVENTORY_FILTER", filterParams = "building,Sound Effects"},
																								{skip = true}
																								}
				},
				{skip = true},
				{skip = true}
			}
	}

-------------------------------------------------
-- Gaming System
-------------------------------------------------

--tables in tables for filtering
--The tabbed tables will populate the drop down 
NAV_STATE_LAYOUTS[NAV_STATES.GAMING] = 	
	{	title = "Gaming",
		coord = {left=504,right=540,top=148,bottom=184},
		actionList = 
			{
				{text = "All", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "all", newItemType = "none"}, coord = {left=504,right=540,top=148,bottom=184}},
				{text = "Zones", stateChange = NAV_STATES.ZONES, coord = {left=360,right=396,top=220,bottom=256}},
				{text = "Loot",  filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "weapon|ammo|armor|tool|consumable|generic", newItemType = "loot"}, coord = {left=324,right=360,top=220,bottom=256}, gamingFilter = {
						{text = "All Loot", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "weapon|ammo|armor|tool|consumable|generic"},
						{text = "Weapons", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "weapon"},
						{text = "Ammo", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "ammo"},
						{text = "Armor", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "armor"},
						{text = "Tools", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "tool"},
						{text = "Consumables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "consumable"},
						{text = "Generic", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "generic"}}
				},
				{text = "Placeables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "flag|shortcut|settings|placeable", newItemType = "placeable"}, coord = {left=396,right=432,top=220,bottom=256}, gamingFilter = {
					{text = "All Placeables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "flag|shortcut|settings|placeable"},
					{text = "Construction Item", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,placeable_loot"},
                    {text = "Seating Item", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,seat"},
					{text = "Crafting Helper", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,campfire"},
					{text = "Doors", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,door"},
					{text = "Containers", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,loot"},
					{text = "Traps", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,trap"},
					{text = "Repair Stations", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,repair"},
					{text = "Local Media Players", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,media"},
					{text = "Vehicles", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,wheeled_vehicle"},
					{text = "Flags", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,claim|placeable,eventFlag|flag"},
					{text = "Teleporters", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,teleporter"},
					{text = "Platforms", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,mover"},
					{text = "Menu Objects", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "shortcut|settings"}}
				},
				{text = "Characters",  filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "character", newItemType = "character"}, coord = {left=432,right=468,top=220,bottom=256}, gamingFilter = {
					{text = "All Characters", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character"},
					{text = "Monsters", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,monster"},
					{text = "NPCs", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,actor"},
					{text = "Vendors", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,vendor|character,shop_vendor"},
					{text = "Animals", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,animal"},
					{text = "Pets", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,pet"}}
				},
				{text = "Quests",  filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "character,quest_giver|quest", newItemType = "quest"}, coord = {left=468,right=504,top=220,bottom=256}, gamingFilter = {
					{text = "All Quests", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,quest_giver|quest"},
					{text = "Quests", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "quest"},
					{text = "Quest Givers", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,quest_giver"}}
				},
				{text = "Harvestables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "harvestable|seed", newItemType = "harvestable"}, coord = {left=504,right=540,top=220,bottom=256}},
				{text = "Crafting",  filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "recipe|blueprint", newItemType = "recipe"}, coord = {left=540,right=576,top=220,bottom=256}, gamingFilter = {
					{text = "All Recipes", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "recipe|blueprint"},
					{text = "Recipes", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "recipe"},
					{text = "Blueprints", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "blueprint"}}
				},
				{text = "Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "spawner", newItemType = "spawner"}, coord = {left=576,right=612,top=220,bottom=256}, gamingFilter = {
					{text = "All Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner"},
					{text = "Loot Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_loot"},
					{text = "Character Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_character"},
					{text = "Harvestable Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_harvestable"},
					{text = "Player Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_player"}}
				}, 
				{text = "Dynamics",  filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "path", newItemType = "pathing"}, coord = {left=792,right=827,top=148,bottom=183}}
			}
	}

NAV_STATE_LAYOUTS[NAV_STATES.WOK_GAMING] = 	
	{	title = "Gaming",
		coord = {left=468,right=504,top=148,bottom=184},
		actionList = 
			{
				{text = "All", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "all", newItemType = "none"}, coord = {left=468,right=504,top=148,bottom=184}},
				{text = "Loot",  filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "weapon|ammo|armor|tool|consumable|generic", newItemType = "loot"}, coord = {left=324,right=360,top=220,bottom=256}, gamingFilter = {
						{text = "All Loot", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "weapon|ammo|armor|tool|consumable|generic"},
						{text = "Weapons", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "weapon"},
						{text = "Ammo", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "ammo"},
						{text = "Armor", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "armor"},
						{text = "Tools", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "tool"},
						{text = "Consumables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "consumable"},
						{text = "Generic", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "generic"}}
				},
				{text = "Placeables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "placeable", newItemType = "placeable"}, coord = {left=396,right=432,top=220,bottom=256}, gamingFilter = {
					{text = "All Placeables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable"},
					{text = "Construction Item", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable"},
                    {text = "Seating Item", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,seat"},
					{text = "Crafting Helper", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,campfire"},
					{text = "Doors", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,door"},
					{text = "Containers", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,loot"},
					{text = "Traps", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,trap"},
					{text = "Repair Stations", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,repair"},
					{text = "Local Media Players", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,media"},
					{text = "Vehicles", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,wheeled_vehicle"},
					{text = "Flags", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,claim|placeable,eventFlag"}}
				},
				{text = "Characters",  filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "character", newItemType = "character"}, coord = {left=432,right=468,top=220,bottom=256}, gamingFilter = {
					{text = "All Characters", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character"},
					{text = "Monsters", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,monster"},
					{text = "NPCs", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,actor"},
					{text = "Vendors", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,vendor|character,shop_vendor"},
					{text = "Animals", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,animal"},
					{text = "Pets", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,pet"}}
				}
			}
	}


NAV_STATE_LAYOUTS[NAV_STATES.ZONES] = 	
	{	title = "Zones",
		coord = {left=504,right=540,top=148,bottom=184},
		actionList = 
			{
				{text = "All", stateChange = NAV_STATES.GAMING, filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "all", newItemType = "none"}, coord = {left=504,right=540,top=148,bottom=184}},
				{text = "Zones", stateChange = NAV_STATES.ZONES, coord = {left=360,right=396,top=220,bottom=256}},
				{text = "Loot", stateChange = NAV_STATES.GAMING, filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "weapon|ammo|armor|tool|consumable|generic", newItemType = "loot"}, coord = {left=324,right=360,top=220,bottom=256}, gamingFilter = {
						{text = "All Loot", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "weapon|ammo|armor|tool|consumable|generic"},
						{text = "Weapons", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "weapon"},
						{text = "Ammo", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "ammo"},
						{text = "Armor", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "armor"},
						{text = "Tools", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "tool"},
						{text = "Consumables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "consumable"},
						{text = "Generic", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "generic"}}
				},
				{text = "Placeables", stateChange = NAV_STATES.GAMING, filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "flag|shortcut|settings|placeable", newItemType = "placeable"}, coord = {left=396,right=432,top=220,bottom=256}, gamingFilter = {
					{text = "All Placeables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable"},
					{text = "Construction Item", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable"},
                    {text = "Seating Item", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,seat"},
					{text = "Crafting Helper", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,campfire"},
					{text = "Doors", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,door"},
					{text = "Containers", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,loot"},
					{text = "Traps", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,trap"},
					{text = "Repair Stations", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,repair"},
					{text = "Local Media Players", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,media"},
					{text = "Vehicles", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,wheeled_vehicle"},
					{text = "Flags", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,claim|placeable,eventFlag|flag"},
					{text = "Teleporters", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,teleporter"},
					{text = "Platforms", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,mover"},
					{text = "World", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "shortcut|settings"}}
				},
				{text = "Characters", stateChange = NAV_STATES.GAMING,  filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "character", newItemType = "character"}, coord = {left=432,right=468,top=220,bottom=256}, gamingFilter = {
					{text = "All Characters", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character"},
					{text = "Monsters", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,monster"},
					{text = "NPCs", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,actor"},
					{text = "Vendors", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,vendor|character,shop_vendor"},
					{text = "Animals", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,animal"},
					{text = "Pets", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,pet"}}
				},
				{text = "Quests", stateChange = NAV_STATES.GAMING,  filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "character,quest_giver|quest", newItemType = "quest"}, coord = {left=468,right=504,top=220,bottom=256}, gamingFilter = {
					{text = "All Quests", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,quest_giver|quest"},
					{text = "Quests", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "quest"},
					{text = "Quest Givers", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,quest_giver"}}
				},
				{text = "Harvestables", stateChange = NAV_STATES.GAMING, filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "harvestable|seed", newItemType = "harvestable"}, coord = {left=504,right=540,top=220,bottom=256}},
				{text = "Crafting", stateChange = NAV_STATES.GAMING, filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "recipe|blueprint", newItemType = "recipe"}, coord = {left=540,right=576,top=220,bottom=256}, gamingFilter = {
					{text = "All Recipes", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "recipe|blueprint"},
					{text = "Recipes", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "recipe"},
					{text = "Blueprints", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "blueprint"}}
				},
				{text = "Spawners", stateChange = NAV_STATES.GAMING, filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "spawner", newItemType = "spawner"}, coord = {left=576,right=612,top=220,bottom=256}, gamingFilter = {
					{text = "All Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner"},
					{text = "Loot Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_loot"},
					{text = "Character Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_character"},
					{text = "Harvestable Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_harvestable"},
					{text = "Player Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_player"}}
				},
				{text = "Dynamos", stateChange = NAV_STATES.GAMING, filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = {filter = "path", newItemType = "pathing"}, coord = {left=468,right=504,top=220,bottom=256}, gamingFilter = {
					{text = "All", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "path"},
					{text = "Pathing Objects", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "path"}}
				}
			}
	}
-- NAV_STATE_LAYOUTS[NAV_STATES.GAMING_SETTINGS] = 	
-- 	{	title = "Settings",
-- 		coord = {left=252,right=288,top=220,bottom=256},
-- 		actionList = 
-- 			{
-- 				{text = "Settings", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "setting", coord = {left=252,right=288,top=220,bottom=256}}
-- 			}
-- 	}

-- NAV_STATE_LAYOUTS[NAV_STATES.GAMING_LOOT] = 	
-- 	{	title = "Loot",
-- 		coord = {left=36,right=72,top=220,bottom=256},
-- 		actionList = 
-- 			{
-- 				{text = "All Loot", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "weapon|ammo|armor|tool|consumable|generic", coord = {left=36,right=72,top=220,bottom=256}},
-- 				{text = "Weapons", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "weapon", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Ammo", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "ammo", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Armor", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "armor", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Tools", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "tool", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Consumables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "consumable", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Generic", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "generic", coord = {left=0,right=0,top=0,bottom=0}}
-- 			}
-- 	}

-- NAV_STATE_LAYOUTS[NAV_STATES.GAMING_PLACEABLES] = 	
-- 	{	title = "Placeables",
-- 		coord = {left=72,right=108,top=220,bottom=256},
-- 		actionList = 
-- 			{
-- 				{text = "All Placeables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable", coord = {left=72,right=108,top=220,bottom=256}},
-- 				{text = "Construction Item", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Crafting Helper", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,campfire", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Doors", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,door", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Containers", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,loot", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Traps", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,trap", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Repair Stations", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,repair", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Local Media Players", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,media", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Vehicles", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,wheeled_vehicle", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Flags", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "placeable,claim|placeable,eventFlag", coord = {left=0,right=0,top=0,bottom=0}}
-- 			}
-- 	}

-- NAV_STATE_LAYOUTS[NAV_STATES.GAMING_CHARACTERS] = 	
-- 	{	title = "Characters",
-- 		coord = {left=108,right=144,top=220,bottom=256},
-- 		actionList = 
-- 			{
-- 				{text = "All Characters", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character", coord = {left=108,right=144,top=220,bottom=256}},
-- 				{text = "Monsters", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,monster", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "NPCs", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,actor", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Vendors", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,vendor|character,shop_vendor", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Animals", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,animal", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Pets", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,pet", coord = {left=0,right=0,top=0,bottom=0}}
-- 			}
-- 	}

-- NAV_STATE_LAYOUTS[NAV_STATES.GAMING_QUESTS] = 	
-- 	{	title = "Quests",
-- 		coord = {left=144,right=180,top=220,bottom=256},
-- 		actionList = 
-- 			{
-- 				{text = "Quests", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "quest", coord = {left=144,right=180,top=220,bottom=256}},
-- 				{text = "Quest Givers", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "character,quest_giver", coord = {left=0,right=0,top=0,bottom=0}}
-- 			}
-- 	}

-- NAV_STATE_LAYOUTS[NAV_STATES.GAMING_HARVESTABLE] = 	
-- 	{	title = "Harvestables",
-- 		coord = {left=180,right=216,top=220,bottom=256},
-- 		actionList = 
-- 			{
-- 				{text = "Harvestables", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "harvestable", coord = {left=180,right=216,top=220,bottom=256}}
-- 			}
-- 	}

-- NAV_STATE_LAYOUTS[NAV_STATES.GAMING_CRAFTING] = 	
-- 	{	title = "Crafting",
-- 		coord = {left=216,right=252,top=220,bottom=256},
-- 		actionList = 
-- 			{
-- 				{text = "All Recipes", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "recipe|blueprint", coord = {left=216,right=252,top=220,bottom=256}},
-- 				{text = "Recipes", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "recipe", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Blueprints", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "blueprint", coord = {left=0,right=0,top=0,bottom=0}}
-- 			}
-- 	}

-- NAV_STATE_LAYOUTS[NAV_STATES.GAMING_SPAWNERS] = 	
-- 	{	title = "Spawners",
-- 		coord = {left=288,right=324,top=220,bottom=256},
-- 		actionList = 
-- 			{
-- 				{text = "All Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner", coord = {left=288,right=324,top=220,bottom=256}},
-- 				{text = "Loot Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_loot", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Character Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_character", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Harvestable Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_harvestable", coord = {left=0,right=0,top=0,bottom=0}},
-- 				{text = "Player Spawners", filterEvent = "FRAMEWORK_GAMING_FILTER", filterParams = "spawner,spawner_player", coord = {left=0,right=0,top=0,bottom=0}},
-- 			}
-- 	}

function getMenuForState(state)
	if state == NAV_STATES.CLOTHING or state == NAV_STATES.BUILD then
		return SYSTEM_MENUS.WOK	
	elseif state == NAV_STATES.GAMING or state == NAV_STATES.WOK_GAMING then
		return SYSTEM_MENUS.GAMING
	elseif state == NAV_STATES.PLAYER_BACKPACK then
		return SYSTEM_MENUS.BACKPACK
	elseif state == NAV_STATES.PLAYER_QUESTS then
		return SYSTEM_MENUS.QUESTS
	elseif state == NAV_STATES.PLAYER_CRAFTING then
		return SYSTEM_MENUS.CRAFTING
	elseif state == NAV_STATES.GEMS or state == NAV_STATES.PLAYER_GEMS then
		return SYSTEM_MENUS.GEMS
	elseif state == NAV_STATES.NPC then
		return SYSTEM_MENUS.NPC
	elseif state == NAV_STATES.ZONES then
		return SYSTEM_MENUS.ZONES
	else
		return SYSTEM_MENUS.WOK
	end
end

-- function getPreviousState(state)
-- 	if state == NAV_STATES.CLOTHING or state == NAV_STATES.BUILD or state == NAV_STATES.GAMING then
-- 		return NAV_STATES.TOP_LEVEL
-- 	elseif state == NAV_STATES.GAMING_LOOT or state == NAV_STATES.GAMING_PLACEABLES or state == NAV_STATES.GAMING_CHARACTERS or state == NAV_STATES.GAMING_QUESTS or 
-- 			state == NAV_STATES.GAMING_CRAFTING or state == NAV_STATES.GAMING_SPAWNERS or state == NAV_STATES.GAMING_SETTINGS or state == NAV_STATES.GAMING_HARVESTABLE then
-- 		return NAV_STATES.GAMING
-- 	elseif state == NAV_STATES.PLAYER_BACKPACK or state == NAV_STATES.PLAYER_QUESTS or state == NAV_STATES.PLAYER_GEMS or state == NAV_STATES.PLAYER_CRAFTING then
-- 		return  NAV_STATES.PLAYER_BACKPACK
-- 	else
-- 		return NAV_STATES.TOP_LEVEL	
-- 	end
-- end