--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\MenuHelper.lua") -- called from client script
dofile("..\\MenuScripts\\CommonFunctions.lua")

g_removeTryOnItem = true

REMOVE_DYNAMIC_OBJECT_EVENT = "RemoveDynamicObjectEvent"

g_try_on_glid = 0
g_try_on_type = 0
g_try_on_object_id = 0

g_player = 0
g_playerId = 0

g_recieveData = true

g_t_items = {}

function expiredTryOnHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Leave the handler if it has already been invoked once for this menu instance.
	-- This solves the problem where if you placed 2 dynamic objects in succession
	-- the second time the expiration menu appeared it would overwrite the data of the
	-- first if it had not been addressed.
	if not g_recieveData then
		return
	end

	g_try_on_glid = KEP_EventDecodeNumber( event );
	g_try_on_type = KEP_EventDecodeNumber( event );
	g_try_on_object_id = KEP_EventDecodeNumber( event );
	Log("expiredTryOnHandler: glid="..g_try_on_glid.." type="..g_try_on_type.." objId="..g_try_on_object_id)

	g_recieveData = false
	if g_try_on_type==200 then
		-- if animation, check if user already purchased it
		getPlayerInventory(WF.PLAYER_ANIMATIONS, nil, nil, nil, nil, nil, USE_TYPE.ANIMATION)
	else
		-- for all others, unhide menu
		MenuSetMinimizedThis(false)
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandlerFiltered( "expiredTryOnHandler", KEP.TRYON_INVENTORY_EXPIRE_EVENT, KEP.FILTER_TRYON_EXPIRE, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegisterSecureV( REMOVE_DYNAMIC_OBJECT_EVENT, KEP.HIGH_PRIO, KEP.ESEC_SERVER + KEP.ESEC_CLIENT, KEP.ESEC_SERVER + KEP.ESEC_CLIENT )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	g_player = KEP_GetPlayer( )
	g_playerId = GetSet_GetNumber( g_player, MovementIds.NETWORKDEFINEDID )

	local label_handle = Dialog_GetStatic(dialogHandle, "TryOnInventoryLabelExpire")
	Static_SetText(label_handle, "Try on trial period has expired. This item will now be removed.")

	-- hide menu by default
	MenuSetMinimizedThis(true)
end

function Dialog_OnDestroy(dialogHandle)

	-- DRF - Added
	if (g_removeTryOnItem == true) then
		RemoveTryOnItem( g_playerId, g_try_on_type, g_try_on_object_id)
	end
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

function RemoveTryOnItem( playerId, tryOnType, tryOnObjectId)
	gsGame = KEP_GetGameAsGetSet( )
	if tryOnType == USE_TYPE.NONE or tryOnType == USE_TYPE.EQUIP then
		-- event = KEP_EventCreate( "TextEvent" )

		local ev = KEP_EventCreate( "ValidateURLEvent" )
		KEP_EventEncodeString ( ev, GetHomeUrl())
		KEP_EventEncodeString (ev,"Rezone")
		KEP_EventQueue( ev )
	elseif tryOnType == USE_TYPE.ADD_DYN_OBJ then
		ObjectDel(tryOnObjectId, ObjectType.DYNAMIC)
	elseif tryOnType == USE_TYPE.ADD_ATTACH_OBJ then
		ObjectDel(tryOnObjectId, ObjectType.DYNAMIC)
	elseif tryOnType == USE_TYPE.ANIMATION then
		-- Animation try-on: stop animation override
		KEP_SetCurrentAnimationByGLID( 0 )
	elseif tryOnType == USE_TYPE.SOUND  then
		ObjectDel(tryOnObjectId, ObjectType.SOUND)
	end

	KEP_SetTryOnState("F")
	KEP_SetTryOnObjPlacementId(0)
	KEP_SetTryOnOnStartup(0)
end

function cbDontShow_OnCheckBoxChanged()
	local config = KEP_ConfigOpenWOK()
	local chkDontShow = Dialog_GetCheckBox(gDialogHandle, "cbDontShow")

	if CheckBox_GetChecked(chkDontShow) == 1 then
		if config ~= nil then
			KEP_ConfigSetString( config, "showTryOnExpire", "false")
			KEP_ConfigSave( config )
		end
	else
		local config = KEP_ConfigOpenWOK()
		if config ~= nil then
			KEP_ConfigSetString( config, "showTryOnExpire", "true")
			KEP_ConfigSave( config )
		end
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.PLAYER_ANIMATIONS then
		local xmlData = KEP_EventDecodeString( event )
		g_t_items 	= {}
		decodeInventoryItemsFromWeb( g_t_items, xmlData, true )

		-- check if try on animation already in inventory
		if g_try_on_type==200 then
			local found = false
			for idx, t_item in ipairs( g_t_items ) do
				if t_item.GLID==g_try_on_glid then
					found = true
					break
				end
			end

			if found then
				Log("Found In Inventory - Canceling...")
				g_removeTryOnItem = false
				MenuCloseThis()
			else
				-- not found, display this menu
				MenuSetMinimizedThis(false)
			end
		else
			MenuSetMinimizedThis(false)
		end
		return KEP.EPR_OK	-- don't consume this, leave it for inventory menu
	end
end
