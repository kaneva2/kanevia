--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

SEND_GLID_EVENT = "SendGLIDEvent"

g_itemGLID = nil
g_name = nil
g_desc = nil
g_creator = nil
g_categories = nil
g_displayShopLink = true

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "itemGLIDHandler", SEND_GLID_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- Set Location Near Clicked Inventory Item
	if MenuIsOpen("Inventory.xml") then 
		setLocation("Inventory.xml")
	elseif MenuIsOpen("Storage.xml") then
		setLocation("Storage.xml")
	end
end

function itemGLIDHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	
	g_itemGLID = tonumber(KEP_EventDecodeNumber(event))
	local invType = tonumber(KEP_EventDecodeNumber(event))
	g_name = KEP_EventDecodeString(event)
	g_desc = KEP_EventDecodeString(event)
	g_creator = KEP_EventDecodeString(event)
	if KEP_EventMoreToDecode( event ) ~= 0 then
		local menuSent = KEP_EventDecodeString(event)
		if MenuIsOpen(menuSent) then
			setLocation(menuSent)
		end
	end
	
	if KEP_EventMoreToDecode( event ) ~= 0 then
		g_displayShopLink = (KEP_EventDecodeString(event) == "true")
	end
	
	setInfo()
end

function setInfo()
	EditBox_SetText(gHandles["edGLID"], tostring(g_itemGLID), true)
	Static_SetText(gHandles["stcName"], g_name or "")
	Static_SetText(gHandles["stcDesc"], g_desc or "")
	Static_SetText(gHandles["stcCreatedBy"], "Created By: " .. (g_creator or ""))
	Control_SetVisible(gHandles["btnShop"], g_displayShopLink)
	makeWebCall(GameGlobals.WEB_SITE_PREFIX..PLAYER_INVENTORY_SUFFIX.."new&glid=" .. g_itemGLID .. "&categories=true", WF.ITEM_CATEGORIES, 1, 50)
end

function btnShop_OnButtonClicked(btnHandle)
	if g_itemGLID then
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_SHOP .. IMAGE_DETAILS_SUFFIX .. g_itemGLID)
	end
end

function setLocation(menuName)
	if MenuIsClosed(menuName) then return end

	local menuLocation = MenuGetLocation(menuName)
	local menuSize = MenuGetSize(menuName)

	local thisSize = MenuGetSizeThis()

	local x = (menuSize.width - thisSize.width)/2 + menuLocation.x
	local y = (menuSize.height - thisSize.height)/2 + menuLocation.y

	MenuSetLocationThis(x, y)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.ITEM_CATEGORIES then
		local xmlData = KEP_EventDecodeString( event )
		g_categories = ""
		for block_text in string.gmatch( xmlData, "<Inventory>(.-)</Inventory>") do
			local s, e, result = string.find(block_text, "<global_id>(%d+)</global_id>")

			if tonumber(result) == g_itemGLID then
				s, e, result = string.find(block_text, "<name>(.-)</name>")

				if g_categories == "" then
					g_categories = result
				else
					g_categories = g_categories .. ", " .. result
				end
			end
		end
		Static_SetText(gHandles["stcCategories"], "Categories: " .. (g_categories or ""))
	end
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
