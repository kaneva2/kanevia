--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("MenuHelper.lua")

--Constants
local EMPTY_HEIGHT = 100


--variables
local m_zoneInstanceId = 0
local m_zoneType = 0
local m_flagPID = 0
local m_eventID = 0


function onCreate()

	Events.registerHandler("UPDATE_EVENT_FLAG_MENU", updateEventFlagMenu)
	

	 m_zoneInstanceId = KEP_GetZoneInstanceId()
	 m_zoneType = KEP_GetZoneIndexType()
end

function btnCreate_OnButtonClicked(btnHandle)
	local url = "mykaneva/events/eventform.aspx?zid="..tostring(m_zoneInstanceId).."&zonetype="..tostring(m_zoneType).."&objPlacementId="..tostring(m_flagPID)
	local web_address = GameGlobals.WEB_SITE_PREFIX_KANEVA ..url
	launchWebBrowser( web_address)
	MenuCloseThis()
end

function btnEdit_OnButtonClicked(btnHandle)
	local url = "mykaneva/events/eventform.aspx?event="..tostring(m_eventID)
	local web_address = GameGlobals.WEB_SITE_PREFIX_KANEVA ..url
	launchWebBrowser( web_address)
	MenuCloseThis()
end
----------------------------
-- Event Handlers
---------------------------
function updateEventFlagMenu(event)

	m_flagPID = event.PID
	local message = event.info
	if message then
		if event.status == "none" then
			Control_SetVisible(gHandles["btnCreate"], true)
			Control_SetEnabled(gHandles["btnCreate"], true)
			Dialog_SetSize(gDialogHandle, Dialog_GetWidth(gDialogHandle), EMPTY_HEIGHT)
			Control_SetVisible(gHandles["stcMessage"], true)
			Control_SetLocation(gHandles["imgMoreInfoBG"], Control_GetLocationX(gHandles["imgMoreInfoBG"]), Dialog_GetHeight(gDialogHandle) - 30)
			Control_SetLocation(gHandles["btnCreate"], Control_GetLocationX(gHandles["btnCreate"]), Dialog_GetHeight(gDialogHandle) - 20)
		elseif event.status == "Scheduled" then
			Control_SetVisible(gHandles["btnEdit"], true)
			Control_SetEnabled(gHandles["btnEdit"], true)

			m_eventID = message.eventID

			local eventTitle = UnescapeHTML(message.title)
			Static_SetText(gHandles["stcEventName"], eventTitle)
			Control_SetVisible(gHandles["stcEventName"], true)

			Static_SetText(gHandles["stcDate"], message.dayOfWeek..", "..message.month.." "..message.day)
			Control_SetVisible(gHandles["stcDate"], true)

			Static_SetText(gHandles["stcTime"], message.hour..":"..message.minute..message.PMorAM.." EST")
			Control_SetVisible(gHandles["stcTime"], true)

			local eventDescription = UnescapeHTML(message.description)
			Static_SetText(gHandles["stcDescription"], eventDescription)
			Control_SetVisible(gHandles["stcDescription"], true)
		elseif event.status == "Occurring" then
			Dialog_SetSize(gDialogHandle, Dialog_GetWidth(gDialogHandle), EMPTY_HEIGHT)
			Control_SetVisible(gHandles["stcMessage"], true)
			Static_SetText(gHandles["stcMessage"], message.remainingTime.." minutes remaining in Event")
			Control_SetVisible(gHandles["imgMoreInfoBG"], false)
		end
	end
end