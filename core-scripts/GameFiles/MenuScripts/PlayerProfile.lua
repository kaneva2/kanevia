--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("PageButtons.lua")

PLAYER_USERID_EVENT = "PlayerUserIdEvent"
PURCHASE_RAVE_EVENT = "PurchaseRaveEvent"
RAVE_PURCHASED_EVENT = "RavePurchasedEvent"

-- Used to decide on parse function
PARSE_FRIENDS = 0
PARSE_PROFILE = 1
PARSE_STAT = 2
PARSE_ABOUT = 3
PARSE_RESULT = 4
PARSE_COUNTS = 5

-- Menu tabs
TAB_FRIENDS = 0
TAB_STAT = 1
TAB_ABOUT = 2

-- Table indexes
INDEX_PLAYERID = "playerID"
INDEX_NAME = "name"
INDEX_USERID = "userID"
INDEX_ISMATURE = "isMature"
INDEX_ISONLINE = "isOnline"
INDEX_APT_ID = "aptID"         -- apartment id

BTN_RAVE = "rave"
BTN_STATS = "stats"
g_raves = 0

-- Not one of the buttons that requires a message box
BTN_NONE = "none"

g_buttonPressed = BTN_NONE

-- Used to filter browser event.  Set once at menu creation.
g_PLAYER_PROFILE = WF.MINI_PROFILE

-- Web address for query
g_web_address = nil

-- Current tab selected (Friends, People, or Ignore)
g_current_tab = nil

-- Hold current friend data returned from server
g_friendData = ""

-- Total number of friend records
g_totalNumRec = 0

-- Maximum number of friends for server to return in one request
g_friendsPerPage = 8

g_selectedIndex = -1
g_t_friends = {}

-- id numbers of the currently selected player in profile window
g_userID = -1
g_playerID = -1
g_aptID = -1
g_userName = ""
g_alreadyRaved = 0

-- Which parser we will be using
g_parseSelection = PARSE_PROFILE

-- Current info tab user selected
g_currentTab = TAB_FRIENDS

-- Alert to a new profile load
g_newProfile = true

-- Used to copy profile data to use in stats
g_statData = ""

-- Current location within friend history
g_currentFriend = 0

-- Table to hold our path when drilling through friends
g_Friend_History = {}

-- lets us know if we are loading a history profile
g_thumbPressed = false

-- Store button handles
g_t_btnHandles = {}

g_countData = ""

g_items = {}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "PlayerUserIdEventHandler", PLAYER_USERID_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ravePurchasedEventHandler", RAVE_PURCHASED_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( PLAYER_USERID_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO)
end

function ravePurchasedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	--update rave count
	requestPlayerRaves()
end

-- Catch the userId and name of the person selected from the friends and people menu
function PlayerUserIdEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_userID = KEP_EventDecodeNumber( event )
	g_userName = KEP_EventDecodeString( event )
	addFriendToHistory( g_userID, g_userName )
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..PROFILE_SUFFIX..g_userID, g_PLAYER_PROFILE)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == g_PLAYER_PROFILE then
		g_friendData = KEP_EventDecodeString( event )
		if g_parseSelection == PARSE_RESULT then
			ParseResults()
		elseif g_parseSelection == PARSE_PROFILE then
			ParseProfileData()
		elseif g_parseSelection == PARSE_FRIENDS then
			ParseFriendData()
			requestPlayerRaves()
		elseif g_parseSelection == PARSE_ABOUT then
			ParseAboutData()
		elseif g_parseSelection == PARSE_STAT then
			ParseStatData()
		elseif g_parseSelection == PARSE_COUNTS then
			ParseCountData()
		end
		return KEP.EPR_CONSUMED
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- setup look of listbox scrollbars and populate list
	local lbh = Dialog_GetListBox(dialogHandle, "lstInterests")
	if lbh ~= 0 then
		local sbh = ListBox_GetScrollBar(lbh)
		ListBox_SetHTMLEnabled(lbh, true)
		if sbh ~= 0 then

			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				--Blue body
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 361, 579, 390, 586)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				--Grey lined
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 573, 409, 588)
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Up arrow button
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 547, 409, 561)
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Down Arrow Button
				Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
				Element_SetCoords(eh, 395, 642, 409, 656)
			end
		end
	end

	setFriendMenuState( true )

	initButtons()

	getButtonHandles()

	-- setSelectionColor is called from PageButtons.lua to highlight the first
	-- button when the menu is opend.
	local bh = Dialog_GetButton(dialogHandle, "btnRange1")
	if bh ~= nil then
		setSelectionColor( bh )
	end

	local cbh = Dialog_GetCheckBox( gDialogHandle, "cbRaveSort" )
	if cbh ~= nil then
		CheckBox_SetChecked( cbh, true )
	end
end

function ParseResults()
	s, e, code = string.find(g_friendData, "<ReturnCode>(.-)</ReturnCode>")
	s, e, result = string.find(g_friendData, "<ResultDescription>(.-)</ResultDescription>")
	if result ~= nil then
		if g_buttonPressed == BTN_RAVE then
			if result == "already raved" then
				KEP_MessageBox(RM.ALREADY_RAVED..g_userName)
			end
			if result == "success" then
				sh = Dialog_GetStatic(gDialogHandle, "lblRaves")
				if sh ~= nil then
					Static_SetText(sh, tostring(g_raves + 1))
					requestPlayerRaves()
				end
				KEP_MessageBox(RM.RAVE..g_userName)
			end
		end
	end

	g_buttonPressed = BTN_NONE
	g_resultParse = false
end

function ParseCountData()
	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local countData = ""		-- counts to display

	-- get saved stat data
	countData = g_countData
	s, strPos, result = string.find(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then

		s, strPos, friendReqs = string.find(g_friendData, "<FriendReqs>(.-)</FriendReqs>", strPos)
		s, strPos, friendCnt = string.find(g_friendData, "<FriendCount>(.-)</FriendCount>", strPos)
		s, strPos, channelReqs = string.find(g_friendData, "<ChannelReqs>(.-)</ChannelReqs>", strPos)
		s, strPos, numViews = string.find(g_friendData, "<NumberOfViews>(.-)</NumberOfViews>", strPos)
		s, strPos, newGifts = string.find(g_friendData, "<NewGifts>(.-)</NewGifts>", strPos)
		s, strPos, giftsRecv = string.find(g_friendData, "<GiftsToMe>(.-)</GiftsToMe>", strPos)
		s, strPos, giftsSent = string.find(g_friendData, "<GiftsFromMe>(.-)</GiftsFromMe>", strPos)
		s, strPos, raves = string.find(g_friendData, "<Raves>(.-)</Raves>", strPos)
		s, strPos, g_alreadyRaved = string.find(g_friendData, "<AlreadyRaved>(%d+)</AlreadyRaved>", strPos)
		g_alreadyRaved = tonumber(g_alreadyRaved)
		if g_alreadyRaved == 1 then
			Static_SetText(g_t_btnHandles["btnRave"], "Rave +")
		else
			Static_SetText(g_t_btnHandles["btnRave"], "Rave")
		end

		if friendReqs then
			countData = countData.."Friend Requests: "..friendReqs.."\n"
		end
		if friendCnt then
			countData = countData.."Friend Count: "..friendCnt.."\n"
		end
		if channelReqs then
			countData = countData.."Channel Requests: "..channelReqs.."\n"
		end
		if numViews then
			countData = countData.."Number of Web Views: "..numViews.."\n"
		end
		if newGifts then
			countData = countData.."New Gifts: "..newGifts.."\n"
		end
		if giftsRecv then
			countData = countData.."Gifts Received: "..giftsRecv.."\n"
		end
		if giftsSent then
			countData = countData.."Gifts Sent: "..giftsSent.."\n"
		end
		if raves then
			countData = countData.."Raves: "..raves.."\n"
		end
	end

	sh = Dialog_GetStatic(gDialogHandle, "lblRaves")
	if sh ~= nil then
		Static_SetText(sh, raves)
	end

	if g_buttonPressed == BTN_STATS then
		fillListBox( countData )
	end
end

function ParseStatData()
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local statData = ""		-- Stats to display
	s, strPos, result = string.find(g_statData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		s, strPos, fname = string.find(g_statData, "<first_name>(.-)</first_name>", strPos)
		s, strPos, lname = string.find(g_statData, "<last_name>(.-)</last_name>", strPos)
		s, strPos, gender = string.find(g_statData, "<gender>(.-)</gender>", strPos)
		if gender == "M" then
			gender = "Male"
		else
			gender = "Female"
		end
		s, strPos, signUp_date, signUp_time = string.find(g_statData, "<signup_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)", strPos)
		s, strPos, numLogins = string.find(g_statData, "<number_of_logins>(%d+)</number_of_logins>", strPos)
		s, strPos, country = string.find(g_statData, "<country_name>(.-)</country_name>", strPos)
		s, strPos, lastLog_date, lastLog_time = string.find(g_statData, "<last_login>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)", strPos)
		s, strPos, secondToLastLog_date, secondToLastLog_time = string.find(g_statData, "<second_to_last_login>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)", strPos)
		s, strPos, showOnline = string.find(g_statData, "<show_online>(%d+)</show_online>", strPos)
		if showOnline == "1" then
			s, strPos, online = string.find(g_statData, "<online>(%d+)</online>", strPos)
		end

		if online == "0" then
			online = "Offline"
		else
			online = "Online"
		end

		s, strPos, isMature = string.find(g_statData, "<mature_profile>(%d+)</mature_profile>", strPos)
		if isMature ~= "0" then
			isMature = "Mature Profile \n"
		else
			isMature = ""
		end
		if isMature ~= ""then
			statData = statData..isMature
		end
		if gender then
			statData = statData..gender.."\n"
		end
		if country then
			statData = statData..country.."\n"
		end
		if signUp_date and signUp_time then
			statData = statData.."Signed up: "..signUp_date.." "..signUp_time.."\n"
		end
		if numLogins then
			statData = statData.."Number of logins: "..numLogins.."\n"
		end
		if lastLog_date and lastLog_time then
			statData = statData.."Last login: "..lastLog_date.." "..lastLog_time.."\n"
		end
		if online then
			statData = statData.."Website status: "..online.."\n"
		end

		g_countData = statData

		requestPlayerRaves()
	else
		-- Go back to beginning to grab description
		s, e, result = string.find(g_friendData, "<ReturnDescription>(%w+)</ReturnDescription>", 1)

		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")

		if sh ~= nil then
			Static_SetText(sh, result)
		end
	end
end

function ParseAboutData()
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInterests")
	local startPos, endPos = 0 -- start and end index of substring
	local result = ""  		-- return string from find temp
	local tag = ""          -- xml tag name
	local aboutData = ""	-- Stats to display
	local firstCategoryAdded = false
	startPos, endPos, result = string.find(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		startPos, endPos, tag = string.find(g_friendData, "(%b<>)", endPos)

		while tag ~= "</User>" do
			local s, e, r = string.find(tag, "<Category name=%\"(.*)%\">")

			if (r ~= nil) then
				aboutData = aboutData .. "<strong>" .. r .. "</strong>" .. "\n"

				if firstCategoryAdded then
					ListBox_AddItem(lbh, "", nil)
				else
					firstCategoryAdded = true
				end

				ListBox_AddItem(lbh, "<strong>"  .. r .. "</strong>", nil)
			else
				s, e, r = string.find(tag, "<Interest common=%\"(.*)%\">")
				if (r ~= nil) then
					if (r == "Y") then
						startPos, endPos, r = string.find(g_friendData, ">([^<]+)</Interest>", endPos)
						if (r ~= nil) then
							aboutData = aboutData .. r .. "\n"
							table.insert(g_items, r)
							ListBox_AddItem(lbh, "\t\t<font color=\"#FFFCFF00\">"  .. r .. "</font>", #g_items)
						end
					else
						startPos, endPos, r = string.find(g_friendData, ">([^<]+)</Interest>", endPos)
						if (r ~= nil) then
							aboutData = aboutData .. r .. "\n"
							table.insert(g_items, r)
							ListBox_AddItem(lbh, "\t\t" .. r .. "", #g_items)
						end
					end
				end
			end
			startPos, endPos, tag = string.find(g_friendData, "(%b<>)", endPos)
		end
	end
end

function ParseProfileData()
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local filepath = ""     -- Path of the thumbnail for profile
	local user = ""         -- Name of user
	local userID = 0        -- User's kaneva id
	local isMature = false  -- Mature profile idication
	local balance = 0       -- Number of K points player has
	local raves = 0         -- number of raves a player has
	local playerID = 0        -- player id number in WOK db
	local aptID = 0         -- id of apartment

	-- copy so stat section can use as needed
	g_statData = g_friendData
	s, strPos, result = string.find(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		s, strPos, balance = string.find(g_friendData, "<Balance>([0-9]+)</Balance>", strPos)
		s, strPos, playerID = string.find(g_friendData, "<player_id>([0-9]+)</player_id>", strPos)
		s, strPos, aptID = string.find(g_friendData, "<housing_zone_index>([0-9]+)</housing_zone_index>", strPos)
		s, strPos, userID = string.find(g_friendData, "<user_id>([0-9]+)</user_id>", strPos)
		s, strPos, g_userName = string.find(g_friendData, "<username>(.-)</username>", strPos)
		s, strPos, isMature = string.find(g_friendData, "<mature_profile>([0-9]+)</mature_profile>", strPos)
		s, strPos, dbPath = string.find(g_friendData, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>", strPos)
		s, strPos, raves = string.find(g_friendData, "<number_of_diggs>([0-9]+)</number_of_diggs>", strPos)
		g_raves = raves

		if not dbPath then
			dbPath = DEFAULT_AVATAR_PIC_PATH
		end

		g_userID = tonumber(userID)
		g_playerID = tonumber(playerID)
		g_aptID = tonumber(aptID)

		-- If friend thumbnail is pressd, add it to history
		if g_thumbPressed then
			addFriendToHistory( g_userID, g_userName )
			g_thumbPressed = false
		end

		if isMature ~= "0" then
			isMature = true
		end

		sh = Dialog_GetStatic(gDialogHandle, "lblCredits")
		if sh ~= nil then
			Static_SetText(sh, balance)
		end

		sh = Dialog_GetStatic(gDialogHandle, "lblRaves")
		if sh ~= nil then
			Static_SetText(sh, raves)
		end

		local sh = Dialog_GetStatic( gDialogHandle, "lblTitle" )
		if sh ~= nil then
			Static_SetText( sh, g_userName )
		end

		sh = Dialog_GetStatic( gDialogHandle, "lblMiniProfile" )
		if sh ~= nil then
			Static_SetText( sh, g_userName.."'s Profile" )
		end

		if g_userID > 0 and dbPath then

			local fileName = KEP_DownloadTexture(GameGlobals.FRIEND_PICTURE, g_userID, dbPath, GameGlobals.FULLSIZE )

			-- Strip off begnning of file path leaving only file name
			fileName = string.gsub(fileName,".-\\", "" )

			local ihFore = Dialog_GetImage(gDialogHandle, "imgAvatar")
			local ihBack = Dialog_GetImage(gDialogHandle, "imgAvatarBack")

			fileName = "../../CustomTexture/".. fileName
			scaleImage(ihFore, ihBack,fileName)
		end

		if g_currentTab == TAB_FRIENDS then
			g_parseSelection = PARSE_FRIENDS
			local cbh = Dialog_GetCheckBox( gDialogHandle, "cbRaveSort" )
			if cbh ~= nil then

				-- CheckBox_GetChecked returns 0 or 1, lua treats 0 as empty string
				-- which evaluates to true.  Decide if offline friends should be shown
				if CheckBox_GetChecked( cbh ) == 1 then
					g_web_address = GameGlobals.WEB_SITE_PREFIX..USERID_FRIENDS_SUFFIX..g_userID..RAVE_SORT_SUFFIX
				else
					g_web_address = GameGlobals.WEB_SITE_PREFIX..USERID_FRIENDS_SUFFIX..g_userID
				end
				g_curPage = 1

				-- Grab friends
				makeWebCall( g_web_address, g_PLAYER_PROFILE, g_curPage, g_friendsPerPage)
			end
		end
	else
		-- Go back to beginning to grab description
		s, e, result = string.find(g_friendData, "<ReturnDescription>(%w+)</ReturnDescription>", 1)

		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")

		if sh ~= nil then
			Static_SetText(sh, result)
		end
	end
end

function ParseFriendData()
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local friendPos = 1		-- current position in main string
	local strPos = 1    	-- current position in friend block to parse
	local result = ""  		-- return string from find temp
	local user = "" 		-- username
	local playerID = 0    -- WOK id of player
	local userID = 0      -- Kaneva id of player
	local isMature = false  	-- Profile marked mature or not
	local next = 0			-- next friend button to be configured
	local isOnline = false -- current online status of friend
	local dbPath = DEFAULT_AVATAR_PIC_PATH  --default pic
	s, strPos, result = string.find(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		numRec = calculateRanges()
		g_t_friends = {}
		clearFriendImages()
		resetOnlineIcons()
		for i=1,numRec do
			strPos = 1
			s, friendPos, friendStr = string.find(g_friendData, "<Friend>(.-)</Friend>", friendPos)
			s, strPos, user = string.find(friendStr, "<username>(.-)</username>", strPos)
			s, strPos, userID = string.find(friendStr, "<user_id>([0-9]+)</user_id>", strPos)
			s, strPos, isMature = string.find(friendStr, "<mature_profile>([0-9]+)</mature_profile>", strPos)
			s, strPos, playerID = string.find(friendStr, "<player_id>([0-9]+)</player_id>", strPos)
			s, strPos, isOnline = string.find(friendStr, "<online>(.-)</online>", strPos)
			s, strPos, dbPath = string.find(friendStr, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>", strPos)

			if not dbPath then
				dbPath = DEFAULT_AVATAR_PIC_PATH
			end

			if isMature ~= "0" then
				isMature = true
			else
				isMature = false
			end

			if isOnline == "T" then
				isOnline = true
			else
				isOnline = false
			end

			local friend = {}
			friend[INDEX_PLAYERID] = playerID
			friend[INDEX_NAME] = user
			friend[INDEX_USERID] = tonumber(userID)
			friend[INDEX_ISMATURE] = isMature
			friend[INDEX_ISONLINE] = isOnline
			table.insert(g_t_friends, friend)

			-- Set the text and state for the active buttons
			bh = Dialog_GetButton(gDialogHandle, "btnFriendBox"..i)
			sh = Dialog_GetStatic(gDialogHandle, "lblFriendName"..i)
			if bh ~= nil and sh ~= nil then
				setButtonState(bh, true)
				Static_SetText(sh, user)
			end

			if friend[INDEX_USERID] > 0 and dbPath then
				local fileName = KEP_DownloadTexture(GameGlobals.FRIEND_PICTURE, friend[INDEX_USERID], dbPath, GameGlobals.FULLSIZE )

				-- Strip off begnning of file path leaving only file name
				fileName = string.gsub(fileName,".-\\", "" )

				local ihFore = Dialog_GetImage(gDialogHandle, "imgFriendPic"..i)
				local ihBack = Dialog_GetImage(gDialogHandle, "imgFriendPicBack"..i)
				fileName = "../../CustomTexture/".. fileName
				scaleImage(ihFore,ihBack,fileName)
			end

			if isOnline then
				ih = Dialog_GetImage(gDialogHandle, "imgOnline"..i)
				if ih ~= nil then
					Control_SetVisible(Image_GetControl(ih), true)
				end
			end

			next = i
		end -- for loop

		-- Set the text and state for the left over buttons (inactive)
		next = next + 1
		if next <= g_friendsPerPage then
			for i=next, g_friendsPerPage do
				bh = Dialog_GetButton(gDialogHandle, "btnFriendBox"..i)
				sh = Dialog_GetStatic(gDialogHandle, "lblFriendName"..i)
				if bh ~= nil and sh ~= nil then
					setButtonState(bh, false)
					Static_SetText(sh, "")
				end
			end
		end

		alignButtons()
	else
		-- Go back to beginning to grab description
		s, e, result = string.find(g_friendData, "<ReturnDescription>(%w+)</ReturnDescription>", 1)
		local sh = Dialog_GetStatic(gDialogHandle, "lblStatus")
		if sh ~= nil then
			Static_SetText(sh, result)
		end
	end
end

-- Function to fill our listbox with the parsed data.
-- Used for the interests and stats tab
-- msg: string of parsed message data
function fillListBox( msg )
	local t_msgParse = letsGetFunky(msg, "lblInfoText")
	local lb = Dialog_GetListBox(gDialogHandle, "lstInterests")
	if ListBox_GetSize(lb) > 0 then
		ListBox_RemoveAllItems(lb)
	end
	for i=1,#t_msgParse do
		ListBox_AddItem(lb, t_msgParse[i], 1)
	end
end

function lstInterests_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if (bMouseDown == 0) then
		local itemIndex = ListBox_GetItemData(listBoxHandle, sel_index)
		if itemIndex > 0 then
			KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA..INTEREST_SUFFIX..URLSafe(g_items[itemIndex]) )
		end
		ListBox_ClearSelection(listBoxHandle)
	end
end

function URLSafe(s)
	-- first replace the % for obvious reasons
	s = string.gsub(s, "%%", "%%25")
	-- Reserved characters for URLS
	s = string.gsub(s, "\n", "%%3Cbr%%3E")
	s = string.gsub(s, "+", "%%2B")
	s = string.gsub(s, ",", "%%2C")
	s = string.gsub(s, "/", "%%2F")
	s = string.gsub(s, ":", "%%3A")
	s = string.gsub(s, ";", "%%3B")
	s = string.gsub(s, "=", "%%3D")
	s = string.gsub(s, "?", "%%3F")
	s = string.gsub(s, "@", "%%40")
	s = string.gsub(s, " ", "%%20")
	-- Unsafe characters
	s = string.gsub(s, "<", "%%3C")
	s = string.gsub(s, ">", "%%3E")
	s = string.gsub(s, '"', "%%22")
	s = string.gsub(s, "#", "%%23")
	return s
end

-- Create a table to hold our friend's name and id
-- Add friend table to global history and increment position in table
-- params: number, string
function addFriendToHistory( userid, name )
	local friend = {}
	friend[INDEX_USERID] = userid
	friend[INDEX_NAME] = name
	table.insert(g_Friend_History, friend)
	g_currentFriend = g_currentFriend + 1
end

function removeFriendFromHistory( )
	-- Remove last friend
	table.remove(g_Friend_History)
	g_currentFriend = g_currentFriend - 1
end

-- Turn off all online icons
function resetOnlineIcons()
	for i = 1,g_friendsPerPage  do
		ih = Dialog_GetImage(gDialogHandle, "imgOnline"..i)
		if ih ~= nil then
			Control_SetVisible(Image_GetControl(ih), false)
		end
	end
end

-- Used to fill the buttons with the correct numerical text
function calculateRanges()
	local numRec = 0  --number of records returned
	local s, e  -- start and end indices of found string
	local resultsstr = "Showing "
	local resultslbl = Dialog_GetStatic( gDialogHandle, "lblResults")
	local high = g_curPage * g_friendsPerPage
	local low = ((g_curPage - 1) * g_friendsPerPage) + 1
	s, e, g_totalNumRec = string.find(g_friendData, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
	s, e, numRec = string.find(g_friendData, "<NumberRecords>([0-9]+)</NumberRecords>")
	g_totalNumRec = tonumber(g_totalNumRec)
	g_pageCap = math.ceil(g_totalNumRec / g_friendsPerPage)
	g_setCap = math.ceil(g_pageCap / g_window)
	if high > g_totalNumRec then
		high = g_totalNumRec
	end

	if numRec == "0" then
		low = numRec
	end

	resultsstr = resultsstr..tostring(low).." - "..tostring(high).." of "..tostring(g_totalNumRec)
	if resultslbl ~= nil then
		Static_SetText(resultslbl, resultsstr)
	end

	if g_InitBtnText then
		setButtonText()
		g_InitBtnText = false
	end
	return numRec
end

function btnPrevFriend_OnButtonClicked( buttonHandle )
	local friend = g_Friend_History[g_currentFriend - 1]
	if friend ~= nil then
		g_currentTab = TAB_FRIENDS
		g_parseSelection = PARSE_FRIENDS
		local bh = Dialog_GetButton(gDialogHandle, "btnStats")
		setButtonState( bh, true )

		bh = Dialog_GetButton(gDialogHandle, "btnAbout")
		setButtonState( bh, true )

		setFriendMenuState( true )

		removeFriendFromHistory( )

		g_userID = friend[INDEX_USERID]
		g_userName = friend[INDEX_NAME]

		sh = Dialog_GetStatic( gDialogHandle, "lblTitle" )
		if sh ~= nil then
			Static_SetText( sh, g_userName )
		end

		sh = Dialog_GetStatic( gDialogHandle, "lblMiniProfile" )
		if sh ~= nil then
			Static_SetText( sh, g_userName.."'s Profile" )
		end

		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgAvatar")), false)

		g_parseSelection = PARSE_PROFILE

		g_curPage = 1
		g_set = 1

		g_InitBtnText = true

		resetPageButtonSelection()

		g_newProfile = true

		makeWebCall(GameGlobals.WEB_SITE_PREFIX..PROFILE_SUFFIX..g_userID, g_PLAYER_PROFILE, g_curPage, g_friendsPerPage)
	end
end

function btnGotoPlayer_OnButtonClicked( buttonHandle )
	if g_userName ~= nil then
		local parentGameId = KEP_GetParentGameId()
		local gameId = KEP_GetGameId()
		local url = g_userName .. ".people"

		if KEP_IsWorld() == true then
			url = "kaneva://" .. parentGameId .. "/" .. url
		else
			url = "kaneva://" .. gameId .. "/" .. url
		end

		gotoURL(url)
	else
		KEP_MessageBox("Cannot travel to person.")
	end
end

function btnGotoApartment_OnButtonClicked( buttonHandle )
	if g_userName ~= nil then
		local parentGameId = KEP_GetParentGameId()
		local gameId = KEP_GetGameId()
		local url = g_userName .. ".home"
		if KEP_IsWorld() == true then
			url = "kaneva://" .. parentGameId .. "/" .. url
		else
			url = "kaneva://" .. gameId .. "/" .. url
		end
		gotoURL(url)
	else
		KEP_MessageBox("Cannot travel to apartment.")
	end
end

function btnGotoWeb_OnButtonClicked( buttonHandle )
	if g_userName ~= "" then
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_CHANNEL_PATH..g_userName..".people" )
	end
end

function btnFriendBox1_OnButtonClicked( buttonHandle )
	friendButtonFunction( buttonHandle, 1 )
end

function btnFriendBox2_OnButtonClicked( buttonHandle )
	friendButtonFunction( buttonHandle, 2 )
end

function btnFriendBox3_OnButtonClicked( buttonHandle )
	friendButtonFunction( buttonHandle, 3 )
end

function btnFriendBox4_OnButtonClicked( buttonHandle )
	friendButtonFunction( buttonHandle, 4 )
end

function btnFriendBox5_OnButtonClicked( buttonHandle )
	friendButtonFunction( buttonHandle, 5 )
end

function btnFriendBox6_OnButtonClicked( buttonHandle )
	friendButtonFunction( buttonHandle, 6 )
end

function btnFriendBox7_OnButtonClicked( buttonHandle )
	friendButtonFunction( buttonHandle, 7 )
end

function btnFriendBox8_OnButtonClicked( buttonHandle )
	friendButtonFunction( buttonHandle, 8 )
end

function friendButtonFunction( buttonHandle, index )
	g_thumbPressed = true
	local friend = g_t_friends[index]
	if friend ~= nil then
		g_userID = friend[INDEX_USERID]
		g_userName = friend[INDEX_NAME]

		sh = Dialog_GetStatic( gDialogHandle, "lblTitle" )
		if sh ~= nil then
			Static_SetText( sh, g_userName )
		end

		sh = Dialog_GetStatic( gDialogHandle, "lblMiniProfile" )
		if sh ~= nil then
			Static_SetText( sh, g_userName.."'s Profile" )
		end

		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgAvatar")), false)

		g_parseSelection = PARSE_PROFILE

		g_curPage = 1
		g_set = 1

		g_InitBtnText = true

		resetPageButtonSelection()

		g_newProfile = true

		makeWebCall( GameGlobals.WEB_SITE_PREFIX..PROFILE_SUFFIX..g_userID, g_PLAYER_PROFILE, g_curPage, g_friendsPerPage)
	else
		setButtonState(buttonHandle, false)
	end
end

-- When the check box is checked or unchecked, change the web query we use to
-- display friends
function cbRaveSort_OnCheckBoxChanged( )
	local cbh = Dialog_GetCheckBox( gDialogHandle, "cbRaveSort" )
	if cbh ~= nil then
		g_curPage = 1
		g_set = 1

		-- CheckBox_GetChecked returns 0 or 1, lua treats 0 as empty string
		-- which evaluates to true.  Decide if offline friends should be shown
		if CheckBox_GetChecked( cbh ) == 1 then
			g_web_address = GameGlobals.WEB_SITE_PREFIX..USERID_FRIENDS_SUFFIX..g_userID..RAVE_SORT_SUFFIX
		else
			g_web_address = GameGlobals.WEB_SITE_PREFIX..USERID_FRIENDS_SUFFIX..g_userID
		end
		g_parseSelection = PARSE_FRIENDS
		makeWebCall( g_web_address, g_PLAYER_PROFILE, g_curPage, g_friendsPerPage)
	end
	resetPageButtonSelection()
end

-- Sets the state of the buttons and images of the friend menu to either
-- enabled (true) or disabled (false)
-- Parameter: boolean
function setFriendMenuState( state )
	for i=1, g_friendsPerPage do
		bh = Dialog_GetButton(gDialogHandle, "btnFriendBox"..i)
		if bh ~= nil then
			setButtonState(bh, state)
		end

		if state == false then
			Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgFriendPic"..i)), state)
			sh = Dialog_GetStatic(gDialogHandle, "lblFriendName"..i)
			if sh ~= nil then
				Static_SetText(sh, "")
			end
		end
	end

	sh = Dialog_GetStatic(gDialogHandle, "lblTotal")
	if sh ~= nil then
		Control_SetVisible(Static_GetControl(sh), state)
	end

	sh = Dialog_GetStatic(gDialogHandle, "lblOf")
	if sh ~= nil then
		Control_SetVisible(Static_GetControl(sh), state)
	end

	sh = Dialog_GetStatic(gDialogHandle, "lblRangeHigh")
	if sh ~= nil then
		Control_SetVisible(Static_GetControl(sh), state)
	end

	sh = Dialog_GetStatic(gDialogHandle, "lblDash")
	if sh ~= nil then
		Control_SetVisible(Static_GetControl(sh), state)
	end

	sh = Dialog_GetStatic(gDialogHandle, "lblRangeLow")
	if sh ~= nil then
		Control_SetVisible(Static_GetControl(sh), state)
	end

	sh = Dialog_GetStatic(gDialogHandle, "lblResults")
	if sh ~= nil then
		Control_SetVisible(Static_GetControl(sh), state)
	end

	Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgControlBar")), state)

	bh = Dialog_GetButton(gDialogHandle, "btnFirst")
	setButtonState( bh, state )

	bh = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	setButtonState( bh, state )

	bh = Dialog_GetButton(gDialogHandle, "btnRange1")
	setButtonState( bh, state )

	bh = Dialog_GetButton(gDialogHandle, "btnRange2")
	setButtonState( bh, state )

	bh = Dialog_GetButton(gDialogHandle, "btnRange3")
	setButtonState( bh, state )

	bh = Dialog_GetButton(gDialogHandle, "btnRange4")
	setButtonState( bh, state )

	bh = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	setButtonState( bh, state )

	bh = Dialog_GetButton(gDialogHandle, "btnLast")
	setButtonState( bh, state )

	bh = Dialog_GetButton(gDialogHandle, "btnFriends")
	setButtonState( bh, not(state) )

	sh = Dialog_GetStatic(gDialogHandle, "lblInfoText")
	Static_SetText(sh, "")

	lbh = Dialog_GetListBox(gDialogHandle, "lstInterests")
	if sh ~= nil then
		Control_SetVisible(Static_GetControl(lbh), not state)
	end

	cbh = Dialog_GetCheckBox(gDialogHandle, "cbRaveSort")
	setButtonState(cbh, state)
end

function setAboutMenuState()
	sh = Dialog_GetStatic(gDialogHandle, "lblInfoText")
	Static_SetText(sh, "")
	bh = Dialog_GetButton(gDialogHandle, "btnStats")
	setButtonState( bh, true )
	bh = Dialog_GetButton(gDialogHandle, "btnAbout")
	setButtonState( bh, false )
	setFriendMenuState( false )
	ch	= Image_GetControl(Dialog_GetImage(gDialogHandle, "left arrow bar"))
	Control_SetVisible(ch, false)
	ch	= Image_GetControl(Dialog_GetImage(gDialogHandle, "right arrow bar"))
	Control_SetVisible(ch, false)
	bh = Dialog_GetButton(gDialogHandle, "btnFirst")
	Control_SetVisible( bh, false )
	bh = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	Control_SetVisible( bh, false )
	bh = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	Control_SetVisible( bh, false )
	bh = Dialog_GetButton(gDialogHandle, "btnLast")
	Control_SetVisible( bh, false )
end

function setStatMenuState()
	sh = Dialog_GetStatic(gDialogHandle, "lblInfoText")
	Static_SetText(sh, "")
	local bh = Dialog_GetButton(gDialogHandle, "btnStats")
	setButtonState( bh, false )
	bh = Dialog_GetButton(gDialogHandle, "btnAbout")
	setButtonState( bh, true )
	setFriendMenuState( false )
end

function btnRave_OnButtonClicked( buttonHandle )
	if g_userID ~= nil then
		if g_alreadyRaved == 0 then

			-- Tell browser handler to use parse result function
			g_parseSelection = PARSE_RESULT
			g_buttonPressed = BTN_RAVE

			-- Send rave to site, use 0 for current page and amount per page
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..RAVE_SUFFIX..g_userID.."&raveType=single&currency=FREE", g_PLAYER_PROFILE)
		else
			purchaseMenu = MenuOpen("PurchaseCreditsRewards.xml")
			local ev = KEP_EventCreate( PURCHASE_RAVE_EVENT )
			KEP_EventEncodeNumber(ev, g_userID)
			KEP_EventEncodeNumber(ev, 0)
			KEP_EventEncodeNumber(ev, 0)
			KEP_EventQueue( ev)
		end
	else
		KEP_MessageBox("Unknown user")
	end
end

function btnFriends_OnButtonClicked( buttonHandle )
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInterests")
	if lbh ~= 0 then
		ListBox_RemoveAllItems(lbh)
		g_items = {}
	end
	g_buttonPressed = BTN_NONE
	g_currentTab = TAB_FRIENDS
	g_parseSelection = PARSE_FRIENDS

	local bh = Dialog_GetButton(gDialogHandle, "btnStats")
	setButtonState( bh, true )

	bh = Dialog_GetButton(gDialogHandle, "btnAbout")
	setButtonState( bh, true )

	setFriendMenuState( true )

	local cbh = Dialog_GetCheckBox( gDialogHandle, "cbRaveSort" )

	-- CheckBox_GetChecked returns 0 or 1, lua treats 0 as empty string
	-- which evaluates to true.  Decide if offline friends should be shown
	if CheckBox_GetChecked( cbh ) == 1 then
		g_web_address = GameGlobals.WEB_SITE_PREFIX..USERID_FRIENDS_SUFFIX..g_userID..RAVE_SORT_SUFFIX
	else
		g_web_address = GameGlobals.WEB_SITE_PREFIX..USERID_FRIENDS_SUFFIX..g_userID
	end

	g_curPage = 1
	g_set = 1
	g_InitBtnText = true
	resetPageButtonSelection()
	makeWebCall( g_web_address, g_PLAYER_PROFILE, g_curPage, g_friendsPerPage)
end

function btnStats_OnButtonClicked( buttonHandle )
	g_buttonPressed = BTN_STATS
	g_currentTab = TAB_STAT
	g_parseSelection = PARSE_STAT
	for i=1, 8 do
		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgFriendPicBack"..i)), false)
	end
	setStatMenuState()
	resetOnlineIcons()
	ParseStatData()
end

function btnMegaRave_OnButtonClicked( buttonHandle )
	purchaseMenu = MenuOpen("PurchaseCreditsRewards.xml")
	local ev = KEP_EventCreate( PURCHASE_RAVE_EVENT )
	KEP_EventEncodeNumber(ev, g_userID)
	KEP_EventEncodeNumber(ev, 1)
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventQueue( ev)
end

function btnAbout_OnButtonClicked( buttonHandle )

	g_currentTab = TAB_ABOUT
	g_parseSelection = PARSE_ABOUT
	local lbh = Dialog_GetListBox(gDialogHandle, "lstInterests")
	if lbh ~= 0 then
		ListBox_RemoveAllItems(lbh)
		g_items = {}
	end

	setAboutMenuState()
	resetOnlineIcons()

	makeWebCall(GameGlobals.WEB_SITE_PREFIX..ABOUT_SUFFIX..g_userID, g_PLAYER_PROFILE)

	for i=1, 8 do
		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgFriendPicBack"..i)), false)
	end
end

function clearFriendImages()
	for i = 0, g_friendsPerPage do
		Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, "imgFriendPic"..i)), false)
	end
end

function requestPlayerRaves()
	if(g_userID > 0) then
		g_parseSelection = PARSE_COUNTS
		makeWebCall( GameGlobals.WEB_SITE_PREFIX..COUNTS_SUFFIX..g_userID, g_PLAYER_PROFILE)
	end
end

function Dialog_OnDestroy(dialogHandle)
	if g_menu_game_obj~= nil then
		KEP_RemoveMenuGameObject(g_menu_game_obj )
	end
	Helper_Dialog_OnDestroy( dialogHandle )
end

function getButtonHandles()
	local ch = Dialog_GetControl(gDialogHandle, "btnClose")
	if ch ~= nil then
		g_t_btnHandles["btnClose"] = ch
	end
	ch = Dialog_GetControl(gDialogHandle, "btnRave")
	if ch ~= nil then
		g_t_btnHandles["btnRave"] = ch
	end
	ch = Dialog_GetControl(gDialogHandle, "btnMegaRave")
	if ch ~= nil then
		g_t_btnHandles["btnMegaRave"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnStats")
	if ch ~= nil then
		g_t_btnHandles["btnStats"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnAbout")
	if ch ~= nil then
		g_t_btnHandles["btnAbout"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnGifts")
	if ch ~= nil then
		g_t_btnHandles["btnGifts"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriends")
	if ch ~= nil then
		g_t_btnHandles["btnFriends"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriendBox1")
	if ch ~= nil then
		g_t_btnHandles["btnFriendBox1"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriendBox2")
	if ch ~= nil then
		g_t_btnHandles["btnFriendBox2"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriendBox3")
	if ch ~= nil then
		g_t_btnHandles["btnFriendBox3"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriendBox4")
	if ch ~= nil then
		g_t_btnHandles["btnFriendBox4"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriendBox5")
	if ch ~= nil then
		g_t_btnHandles["btnFriendBox5"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriendBox6")
	if ch ~= nil then
		g_t_btnHandles["btnFriendBox6"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriendBox7")
	if ch ~= nil then
		g_t_btnHandles["btnFriendBox7"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriendBox8")
	if ch ~= nil then
		g_t_btnHandles["btnFriendBox8"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnGotoPlayer")
	if ch ~= nil then
		g_t_btnHandles["btnGotoPlayer"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnGotoApartment")
	if ch ~= nil then
		g_t_btnHandles["btnGotoApartment"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnGotoWeb")
	if ch ~= nil then
		g_t_btnHandles["btnGotoWeb"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnPrevFriend")
	if ch ~= nil then
		g_t_btnHandles["btnPrevFriend"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "lstInterests")
	if ch ~= nil then
		g_t_btnHandles["lstInterests"] = ch
	end
end

function Dialog_OnMouseMove(dialogHandle, x, y)

	--Make sure we have the interface we need (stop errors in editor)
	if Control_ContainsPoint ~= nil then

		local txtRaveToolTip = Dialog_GetControl(dialogHandle, "txtRaveToolTip")
		local txtMegaRaveToolTip = Dialog_GetControl(dialogHandle, "txtMegaRaveToolTip")
		local imgRaveToolTip = Dialog_GetControl(dialogHandle, "RaveToolTip")
		local imgMegaRaveToolTip = Dialog_GetControl(dialogHandle, "MegaRaveToolTip")

		Control_SetVisible(txtRaveToolTip, false)
		Control_SetVisible(txtMegaRaveToolTip, false)
		Control_SetVisible(imgRaveToolTip, false)
		Control_SetVisible(imgMegaRaveToolTip, false)

		if Control_ContainsPoint(g_t_btnHandles["btnRave"], x, y) ~= 0
		and Control_GetVisible(g_t_btnHandles["btnRave"]) == 1 then
			Control_SetVisible(txtRaveToolTip, true)
			Control_SetVisible(imgRaveToolTip, true)
		elseif Control_ContainsPoint(g_t_btnHandles["btnMegaRave"], x, y) ~= 0
		and Control_GetVisible(g_t_btnHandles["btnMegaRave"]) == 1 then
			Control_SetVisible(txtMegaRaveToolTip, true)
			Control_SetVisible(imgMegaRaveToolTip, true)
		end
	end

	local sh = Dialog_GetStatic(dialogHandle, "lblStatus")
	if sh ~= nil then

		if Control_ContainsPoint(g_t_btnHandles["btnClose"], x, y) ~= 0 then
			Static_SetText(sh, "Close This Menu")
		elseif Control_ContainsPoint(g_t_btnHandles["btnRave"], x, y) ~= 0 then
			Static_SetText(sh, "Rave This Person")
		elseif Control_ContainsPoint(g_t_btnHandles["btnStats"], x, y) ~= 0 then
			Static_SetText(sh, "View "..g_userName.."'s Stats")
		elseif Control_ContainsPoint(g_t_btnHandles["btnAbout"], x, y) ~= 0 then
			Static_SetText(sh, "View "..g_userName.."'s Interests")
		elseif Control_ContainsPoint(g_t_btnHandles["btnFriends"], x, y) ~= 0 then
			Static_SetText(sh, "View "..g_userName.."'s Friends")
		elseif Control_ContainsPoint(g_t_btnHandles["btnFriendBox1"], x, y) ~= 0 then
			if Control_GetEnabled(g_t_btnHandles["btnFriendBox1"]) ~= 0 then
				Static_SetText(sh, "View Profile")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnFriendBox2"], x, y) ~= 0 then
			if Control_GetEnabled(g_t_btnHandles["btnFriendBox2"]) ~= 0 then
				Static_SetText(sh, "View Profile")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnFriendBox3"], x, y) ~= 0 then
			if Control_GetEnabled(g_t_btnHandles["btnFriendBox3"]) ~= 0 then
				Static_SetText(sh, "View Profile")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnFriendBox4"], x, y) ~= 0 then
			if Control_GetEnabled(g_t_btnHandles["btnFriendBox4"]) ~= 0 then
				Static_SetText(sh, "View Profile")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnFriendBox5"], x, y) ~= 0 then
			if Control_GetEnabled(g_t_btnHandles["btnFriendBox5"]) ~= 0 then
				Static_SetText(sh, "View Profile")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnFriendBox6"], x, y) ~= 0 then
			if Control_GetEnabled(g_t_btnHandles["btnFriendBox6"]) ~= 0 then
				Static_SetText(sh, "View Profile")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnFriendBox7"], x, y) ~= 0 then
			if Control_GetEnabled(g_t_btnHandles["btnFriendBox7"]) ~= 0 then
				Static_SetText(sh, "View Profile")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnFriendBox8"], x, y) ~= 0 then
			if Control_GetEnabled(g_t_btnHandles["btnFriendBox8"]) ~= 0 then
				Static_SetText(sh, "View Profile")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnGotoPlayer"], x, y) ~= 0 then
			Static_SetText(sh, "Travel to This Player")
		elseif Control_ContainsPoint(g_t_btnHandles["btnGotoApartment"], x, y) ~= 0 then
			Static_SetText(sh, "Travel to Their Apartment")
		elseif Control_ContainsPoint(g_t_btnHandles["btnGotoWeb"], x, y) ~= 0 then
			Static_SetText(sh, "View Their Kaneva Web Profile")
		elseif Control_ContainsPoint(g_t_btnHandles["btnPrevFriend"], x, y) ~= 0 then
			Static_SetText(sh, "Previous Friend")
		elseif Control_ContainsPoint(g_t_btnHandles["lstInterests"], x, y) ~= 0 then
			if g_currentTab == TAB_ABOUT then
				Static_SetText(sh, "Click on Interest to View More Information")
			end
		else
			Static_SetText(sh, "Mouse Over a Button for Information")
		end
	end
end

function alignButtons()
	local imgRightBar = Dialog_GetImage(gDialogHandle, "right arrow bar")
	Control_SetLocation(imgRightBar, 490, 235)
	local btnRightRightArrow = Dialog_GetButton(gDialogHandle, "btnLast")
	Control_SetLocation(btnRightRightArrow, 472, 235)
	local btnRightArrow = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	Control_SetLocation(btnRightArrow, 452, 235)

	local t = {}		-- new table
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange1"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange2"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange3"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange4"))
	rightAlignPageButtons(t, 447, 237, 5)

	local leftMostXPos = Control_GetLocationX(Dialog_GetButton(gDialogHandle, "btnRange1"))

	local btnLeftArrow = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	local leftArrowWidth = Control_GetWidth(btnLeftArrow)
	leftMostXPos = leftMostXPos - leftArrowWidth - 5
	Control_SetLocation(btnLeftArrow, leftMostXPos, 235)

	local btnLeftLeftArrow = Dialog_GetButton(gDialogHandle, "btnFirst")
	local leftLeftArrowWidth = Control_GetWidth(btnLeftLeftArrow)
	leftMostXPos = leftMostXPos - leftLeftArrowWidth - 5
	Control_SetLocation(btnLeftLeftArrow, leftMostXPos, 235)

	local imgLeftBar = Dialog_GetImage(gDialogHandle, "left arrow bar")
	local leftBarWidth = Control_GetWidth(imgLeftBar)
	leftMostXPos = leftMostXPos - leftBarWidth - 2
	Control_SetLocation(imgLeftBar, leftMostXPos, 235)
end
