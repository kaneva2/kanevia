--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------
---Vendor Constants---
-----------------------

VENDOR_ITEMS_PER_PAGE = 9
SELL_FLASH_TIME = .4
TOOLTIP_TABLE = {header = "name"}

COLORS_VALUES =	{	["Common"] = {a = 255, r = 255, g = 255, b = 255},
							["Rare"] = {a = 255, r = 0, g = 150, b = 255},
							["Very Rare"] = {a = 255, r = 174, g = 0, b = 255},
							["Extremely Rare"] = {a = 255, r = 255, g = 144, b = 0},
							["Never"] = {a = 255, r = 255, g = 144, b = 0}
						}

ITEM_LEVEL_COORDS = {
							{left = 0, right = 15, top = 146, bottom = 161},
							{left = 16, right = 31, top = 146, bottom = 161},
							{left = 32, right = 47, top = 146, bottom = 161},
							{left = 0, right = 15, top = 162, bottom = 177},
							{left = 16, right = 31, top = 162, bottom = 177}
}

ITEM_RARITY_BG = {	["Common"] = {left = 208, right = 292, top = 167, bottom = 251},
							["Rare"] = {left = 293, right = 377, top = 167, bottom = 251},
							["Very Rare"] = {left = 378, right = 462, top = 167, bottom = 251},
							["Extremely Rare"] = {left = 378, right = 462, top = 82, bottom = 166},
							["Never"] = {left = 378, right = 462, top = 82, bottom = 166}
						}

ITEM_LEVEL_RARITY_OFFSET = {["Common"] = 0, ["Rare"] = 48, ["Very Rare"] = 96, ["Extremely Rare"] = 144}


EMPTY_ITEM = {	output = 0, 
						outputProperties = {name = "EMPTY", GLID = 0, itemType = "generic"}, 
						input = 0, 
						inputProperties = {name = "EMPTY", GLID = 0, itemType = "generic"}, 
						outputCount = 0, 
						cost = 0,
						time = 0 --Only used for timed Vendor, but doesnt hurt others
					}







------------------------
----Event Handlers------
------------------------
function closeVendor(event)
	if not MenuIsOpen("UnifiedInventory") then
		MenuCloseThis()
	end
end

function updateHeader(event)
	--local header = event.header
	local name = event.name
	m_vendorPID = event.PID

	if name and name ~= "" then
		Static_SetText(gHandles["Menu_Title"], name)
	end
end




--------------------
-----Functions------
--------------------
function moveElementsUp()
	local moveHeight = 40
	for i = 1, VENDOR_ITEMS_PER_PAGE do
		Control_SetLocationY(gHandles["imgItemBG"..i], Control_GetLocationY(gHandles["imgItemBG"..i]) - moveHeight)
		Control_SetLocationY(gHandles["imgItem"..i], Control_GetLocationY(gHandles["imgItem"..i]) - moveHeight)
		Control_SetLocationY(gHandles["imgItemCountBG"..i], Control_GetLocationY(gHandles["imgItemCountBG"..i]) - moveHeight)
		Control_SetLocationY(gHandles["stcItemCount"..i], Control_GetLocationY(gHandles["stcItemCount"..i]) - moveHeight)
		Control_SetLocationY(gHandles["imgItemName"..i], Control_GetLocationY(gHandles["imgItemName"..i]) - moveHeight)
		Control_SetLocationY(gHandles["stcItem"..i], Control_GetLocationY(gHandles["stcItem"..i]) - moveHeight)
		Control_SetLocationY(gHandles["imgItemOverlay"..i], Control_GetLocationY(gHandles["imgItemOverlay"..i]) - moveHeight)
		Control_SetLocationY(gHandles["imgItemLevel"..i], Control_GetLocationY(gHandles["imgItemLevel"..i]) - moveHeight)
		Control_SetLocationY(gHandles["btnItem"..i], Control_GetLocationY(gHandles["btnItem"..i]) - moveHeight)
		if m_timed then
			Control_SetLocationY(gHandles["imgItemTime"..i], Control_GetLocationY(gHandles["imgItemTime"..i]) - moveHeight)
		end
	end
end


function copyTable(copiedTable, output )
	local newTable = {}
	if output then
		newTable.output = copiedTable.output
		newTable.outputCount = copiedTable.outputCount
		newTable.outputProperties = deepCopy(copiedTable.outputProperties)
	else
		newTable.output = copiedTable.input
		newTable.outputCount = copiedTable.cost
		newTable.outputProperties =  deepCopy(copiedTable.inputProperties)
	end

	return newTable
end

function updatePage()
	local itemCount = #g_trades
	local btnNext = gHandles["btnNext"]
	local btnBack = gHandles["btnBack"]
	local stcPage = gHandles["stcPage"]

	if btnNext == nil then return end

	local maxpage = math.ceil(itemCount / 9)

	local showArrows = maxpage > 1
	local showNext = (showArrows) and (m_page ~= maxpage)
	local showBack = (showArrows) and (m_page ~= 1)
	local showText = showArrows

	
	Control_SetVisible(stcPage, showText)
	Control_SetVisible(btnBack, showBack)
	Control_SetVisible(btnNext, showNext)
	Control_SetEnabled(btnBack, showBack)
	Control_SetEnabled(btnNext, showNext)
	
	if m_page  == m_selectedPage then
		Control_SetVisible(gHandles["imgHighlight"], true)
	else
		Control_SetVisible(gHandles["imgHighlight"], false)
	end
	Static_SetText(gHandles["stcPage"], "("..tostring(m_page).."/"..tostring(maxpage)..")")
end

function formatNumberSuffix(count)
	-- If no need for formatting, then return
	if count < 10000 then return count end

	local strCount = tostring(count)
	local newCount = ""

	local format = { 	max = 0,
						min = 0,
					 	suffix = ""
					}

	-- Thousands
	if #strCount <= 6 then
		format.max = 6
		format.min = 3
		format.suffix = "K"
	-- Millions
	elseif #strCount <= 9 then
		format.max = 9
		format.min = 6
		format.suffix = "M"
	-- Billions
	elseif #strCount <= 12 then
		format.max = 12
		format.min = 9
		format.suffix = "B"
	-- Trillions
	elseif #strCount <= 15 then
		format.max = 15
		format.min = 12
		format.suffix = "T"
	end

	for i = 1, #strCount do
		local diff = #strCount - i
		if #strCount <= format.max then
			if diff >= format.min then
				newCount = newCount..string.sub(strCount,i,i)
			else
				return newCount..format.suffix
			end
		end
	end
end

function updateHighlightLocation( )
	local highlightIndex = m_currTrade - ((m_page - 1) * VENDOR_ITEMS_PER_PAGE)
	if m_page == m_selectedPage then
		Control_SetLocation(gHandles["imgHighlight"], Control_GetLocationX(gHandles["imgItemBG".. highlightIndex]), Control_GetLocationY(gHandles["imgItemBG".. highlightIndex]))
	else
		Control_SetVisible(gHandles["imgHighlight"], false)
	end
end

function updateItemContainer()
	--log("--- updateItemContainer")
	m_displaying = true
	updatePage()
	local startIndex =  VENDOR_ITEMS_PER_PAGE * (m_page - 1) + 1
	local endIndex = VENDOR_ITEMS_PER_PAGE * m_page
	local displayIndex = 1
	for i = startIndex, endIndex do
		if g_trades[i] then
			g_displayTable[displayIndex] = deepCopy(g_trades[i])
		else
			g_displayTable[displayIndex] = deepCopy(EMPTY_ITEM)
		end
		displayIndex = displayIndex + 1
	end

	if g_displayTable[1] then
		for i=1, ITEMS_PER_PAGE do
			if not g_displayTable[i] then
				g_displayTable[i] = deepCopy(EMPTY_ITEM)
			end
			
			if i == 10 and g_trades[m_currTrade]  then
				g_displayTable[i] = deepCopy(g_trades[m_currTrade])
			elseif i == 11 and g_trades[m_currTrade] then
				g_displayTable[i] = copyTable(g_trades[m_currTrade], false)
			end

			if g_displayTable[i].outputCount <= 0 then
				g_displayTable[i].hide = true
			elseif g_displayTable[i].outputCount >= 1 then
				g_displayTable[i].count = g_displayTable[i].outputCount 
			end

			if g_displayTable[i].outputCount <= 1 then
				g_displayTable[i].hideCount = true
			end
			
			formatTooltips(g_displayTable[i])


			applyImage(gHandles["imgItem"..i], g_displayTable[i].outputProperties.GLID)

			local element
			if g_displayTable[i].outputProperties.itemType == ITEM_TYPES.WEAPON or g_displayTable[i].outputProperties.itemType == ITEM_TYPES.ARMOR or g_displayTable[i].outputProperties.itemType == ITEM_TYPES.TOOL then
				Control_SetVisible(gHandles["imgItemLevel" .. i], true)
				element = Image_GetDisplayElement(gHandles["imgItemLevel" .. i])
				local itemLevel = g_displayTable[i].outputProperties.level
				local rarityOffset = ITEM_LEVEL_RARITY_OFFSET[g_displayTable[i].outputProperties.rarity] or 0
				Element_SetCoords(element, ITEM_LEVEL_COORDS[itemLevel].left + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].top, ITEM_LEVEL_COORDS[itemLevel].right + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].bottom)
			else
				Control_SetVisible(gHandles["imgItemLevel" .. i], false)
			end

			local rarity = g_displayTable[i].outputProperties.rarity or "Common"
			element = Image_GetDisplayElement(gHandles["imgItemBG" .. i])

			Element_SetCoords(element, ITEM_RARITY_BG[rarity].left, ITEM_RARITY_BG[rarity].top, ITEM_RARITY_BG[rarity].right, ITEM_RARITY_BG[rarity].bottom)
		end

		InventoryHelper.setNameplates("name", "hide")
		InventoryHelper.setQuantities("count", "hideCount")
	end
end

-- Populates the menu
 function updateInventory()
	m_inventoryCounts = {}
	-- Compile UNID counts
	for i=1, #m_inventory do
		if m_inventory[i].UNID ~= 0 then
			-- New UNID entry
			if m_inventoryCounts[m_inventory[i].UNID] == nil then
				m_inventoryCounts[m_inventory[i].UNID] = m_inventory[i].count
			-- Duplicate entry. Increment UNID count
			else
				m_inventoryCounts[m_inventory[i].UNID] = m_inventoryCounts[m_inventory[i].UNID] + m_inventory[i].count
			end
		end
	end
	-- Update overlays
	setPurchasableOverlays()
end

function setPurchasableOverlays()
	--Overridden in each vendor
end

function setCounts( )
	--Overridden in each Credits and Timed
	-- Set required item count statics
	local hasNuf = true
	local color

	local reqItem = g_trades[m_currTrade].input
	local reqNeedCount = g_trades[m_currTrade].cost or 0
	
	-- Get how many of this item the player has
	local reqHasCount = m_inventoryCounts[reqItem] or 0
	
	-- Set the have color to red if you don't got nuf
	if reqNeedCount > reqHasCount then
		-- Set color to red
		hasNuf = false
		color = {a=255,r=255,g=0,b=0}
	else
		-- Set color to white
		color = {a=255,r=255,g=255,b=255}
	end	
	local stcElementHandle = Static_GetDisplayElement(gHandles["txtCount"])
	BlendColor_SetColor(Element_GetFontColor(stcElementHandle), 0, color)
	
	local itemIndex = 10
	if #g_trades == 1 then
		itemIndex = 1
	end
	Control_SetEnabled(gHandles["imgItemOverlay"..itemIndex], not hasNuf)
	Control_SetVisible(gHandles["imgItemOverlay"..itemIndex], not hasNuf)

	Static_SetText(gHandles["txtBackpackCount"], formatNumberSuffix(reqHasCount).." in backpack")

	for i,v in pairs(m_inputLocations) do
		Control_SetLocation(gHandles[i], m_inputLocations[i].x, m_inputLocations[i].y)
	end

	Static_SetText(gHandles["txtCount"], formatNumberSuffix(g_trades[m_currTrade].cost).. " ".. g_trades[m_currTrade].inputProperties.name)

	local tradeIndex = m_currTrade - ((m_page-1) * VENDOR_ITEMS_PER_PAGE)
	-- add extra information about the item 
	
	if m_page == m_selectedPage then
		if g_displayTable[tradeIndex].tooltip.footer1 then
			Control_SetLocationY(gHandles["txtFooter1"], Control_GetLocationY(gHandles["stcItem"..itemIndex]) + Static_GetStringHeight(gHandles["stcItem"..itemIndex], Static_GetText(gHandles["stcItem"..itemIndex])) + 2)
			Static_SetText(gHandles["txtFooter1"], g_displayTable[tradeIndex].tooltip.footer1.label)
		else
			Static_SetText(gHandles["txtFooter1"], "")
		end 
		if g_displayTable[tradeIndex].tooltip.footer2 then
			Control_SetLocationY(gHandles["txtFooter2"], Control_GetLocationY(gHandles["txtFooter1"]) + Control_GetHeight(gHandles["txtFooter1"]))
			Static_SetText(gHandles["txtFooter2"], g_displayTable[tradeIndex].tooltip.footer2.label)
		else
			Static_SetText(gHandles["txtFooter2"], "")
		end
	end
	-- Enable the crafting button?
	Control_SetEnabled(gHandles["btnBuy"], hasNuf)
	m_validTrade = hasNuf

	return hasNuf
end

function changePage(value)
	m_page = m_page + value
	updateItemContainer()
	setCounts()
	setPurchasableOverlays()
end

function formatTooltips(item)
	if item then
		item.tooltip = {}

		item.tooltip["header"] = {label = tostring(item.outputProperties.name)}
		
		if item.outputProperties.rarity then
			item.tooltip["header"].color = COLORS_VALUES[item.outputProperties.rarity]
		end

		if item.outputProperties.description then
			item.tooltip["body"] = {label = "\"" .. item.outputProperties.description .. "\""}
		end

		if item.outputProperties.itemType == ITEM_TYPES.WEAPON then

			item.tooltip["level"] = {label = "Level "..tostring(item.outputProperties.level), level = item.outputProperties.level}

			-- change the description when the 
			if item.outputProperties.healingWeapon and item.outputProperties.healingWeapon == "true" then
				item.tooltip["healing"] = {label = item.outputProperties.damage or "Unknown"}
			else 
				item.tooltip["damage"] = {label = item.outputProperties.damage or "Unknown"}
			end 

			item.tooltip["range"] = {label = tostring(item.outputProperties.range)}

			if item.outputProperties.ammoType and m_gameItemNamesByUNID[tostring(item.outputProperties.ammoType)] then 
				if m_playerInventoryByUNID[item.outputProperties.ammoType] == nil then
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.outputProperties.ammoType)]), color = {r = 255, g = 0, b = 0, a = 255}}
				else
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.outputProperties.ammoType)])}
				end
			elseif item.outputProperties.ammoType then 
				getItemNameByUNID(item.outputProperties.ammoType)
			end 
			
			item.tooltip["footer1"] = {label = "Weapon Level: "..tostring(item.outputProperties.level)}
			item.tooltip["footer2"] = {label = "Range: "..tostring(item.outputProperties.range)}
		elseif item.outputProperties.itemType == ITEM_TYPES.ARMOR then
			item.tooltip["level"] = {label = "Level "..tostring(item.outputProperties.level), level = item.outputProperties.level}
			item.tooltip["armorslot"] = {label = tostring(item.outputProperties.slotType:gsub("^%l", string.upper))}

			item.tooltip["armorrating"] = {label = "Unknown"}
		
			item.outputProperties.slotTypeDisplay = (item.outputProperties.slotType:gsub("^%l", string.upper)) 
			item.tooltip["footer1"] = {label = "Armor Level: "..tostring(item.outputProperties.level)}
			item.tooltip["footer2"] = {label = "Slot: "..tostring(item.outputProperties.slotType:gsub("^%l", string.upper))}
		elseif item.outputProperties.itemType == ITEM_TYPES.TOOL then
			item.tooltip["level"] = {label = "Level "..tostring(item.outputProperties.level), level = item.outputProperties.level}
		
			item.tooltip["footer1"] = {label = "Tool Level: "..tostring(item.outputProperties.level)}
		elseif item.outputProperties.itemType == ITEM_TYPES.CONSUMABLE then
			item.outputProperties.consumeValueDisplay = item.outputProperties.consumeValue .. "%" 

			if item.outputProperties.consumeType == "energy" then
				item.tooltip["energy"] = {label = tostring(item.outputProperties.consumeValue) .. "%"}
				item.tooltip["footer1"] = {label = "Energy Bonus: "..tostring(item.outputProperties.consumeValue) .. "%"}
			else
				item.tooltip["health"] = {label = tostring(item.outputProperties.consumeValue) .. "%"}
				item.tooltip["footer1"] = {label = "Health Bonus: "..tostring(item.outputProperties.consumeValue) .. "%"}
			end
		elseif item.outputProperties.itemType == ITEM_TYPES.PLACEABLE then
		elseif item.outputProperties.itemType == ITEM_TYPES.GENERIC then
		elseif item.outputProperties.itemType == ITEM_TYPES.AMMO then
		elseif item.outputProperties.itemType == ITEM_TYPES.HARVESTABLE then
			-- get food properties 
			--log("--- food : ".. tostring(item.outputProperties.food))
			if item.outputProperties.food and m_gameItemNamesByUNID[tostring(item.outputProperties.food)] then 
				item.tooltip["requires1"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.outputProperties.food)]).. " to grow"}
			elseif item.outputProperties.food then 
				getItemNameByUNID(item.outputProperties.food)
			end 

			-- get tool from properties 
			--log("--- requiredTool : ".. tostring(item.outputProperties.requiredTool))
			if item.outputProperties.requiredTool and m_gameItemNamesByUNID[tostring(item.outputProperties.requiredTool)] then 
				item.tooltip["requires2"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.outputProperties.requiredTool)]).. " to harvest"}
			elseif item.outputProperties.food then 
				getItemNameByUNID(item.outputProperties.requiredTool)
			end 

			item.tooltip["level"] = {label = "Level "..tostring(item.outputProperties.level), level = item.outputProperties.level}
		elseif item.outputProperties.itemType == ITEM_TYPES.CHARACTER and item.outputProperties.behavior == "pet" then
			if item.outputProperties.food and m_gameItemNamesByUNID[tostring(item.outputProperties.food)] then 
				if m_playerInventoryByUNID[item.outputProperties.food] == nil then
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.outputProperties.food)]), color = {r = 255, g = 0, b = 0, a = 255}}
				else
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.outputProperties.food)])}
				end
			elseif item.outputProperties.food then 
				getItemNameByUNID(item.outputProperties.food)
			end
			item.tooltip["level"] = {label = "Level "..tostring(item.outputProperties.level), level = item.outputProperties.level}
		end

		
		if item.inputProperties then
			item.tooltip["moreInfo"] = {label = "Price: ".. tostring(item.cost).. " ".. tostring(item.inputProperties.name),
			GLID = item.inputProperties.GLID }
		end
	end
end

function updateTheSelectedItem( )
	-- To be overriden
end
-----------------------
--Drag Drop functions--
-------------------------
-- On mouseover sell drop button
function DragDrop.buttonOnMouseEnterHandler(btnHandle)
	-- m_requestHandle = Control_GetName(btnHandle)
	--log("--- buttonOnMouseEnterHandler")
	setRequestHandle(btnHandle)
	local requestEvent = KEP_EventCreate("DRAG_DROP_REQUEST_ELEMENT")
	KEP_EventSetFilter(requestEvent, 4)
	KEP_EventQueue(requestEvent)
	
	m_overButton = true
end

function DragDrop.buttonOnMouseLeaveHandler(btnHandle)
	--log("--- buttonOnMouseLeaveHandler")

	m_overButton = false
	DragDrop.buttonOnMouseLeave(btnHandle)
end

function setRequestHandle(requestHandle)
	--log("--- setRequestHandle")
	m_requestHandle = requestHandle
end

function startDragDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	--m_currTrade = 0

	updateTheSelectedItem()

	if m_sellFlashed then
		returnItem()
		m_sellFlashed = false
		setSellEnabledVisible(false)
	end

	if Control_GetVisible(gHandles["txtNotBuy"]) then
		setEnabledWontBuy(false)
	end

	Control_SetVisible(gHandles["txtSell"], true)
	Control_SetEnabled(gHandles["txtSell"], true)
	Control_SetVisible(gHandles["btnItemDrop1"], true)
	Control_SetVisible(gHandles["imgSellBG"], true)

	Control_SetVisible(gHandles["imgDialogBG"], true)
	Control_SetEnabled(gHandles["imgDialogBG"], true)

	m_dropFlashTime = SELL_FLASH_TIME
	if Control_GetHeight(gHandles["imgSellBG"]) < 490 then
		m_dropFlashing = false
		m_dropFlashed = false
	end
end

-- Returned from drag drop handler
function onReturnElement(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	--log("--- onReturnElement")

	local elementExists = KEP_EventDecodeString(tEvent) == "true"
	local sellItem = {}

	if elementExists and m_requestHandle and m_overButton then
		local sellItemString = KEP_EventDecodeString(tEvent)
		sellItem = Events.decode(sellItemString)
		
		local canDrop = false
		for i=1, #g_trades do
			-- Item dropped found in tradables sold
			if sellItem.UNID == g_trades[i].output then
				canDrop = true
				break
			end
		end		

		DragDrop.buttonOnMouseEnter(m_requestHandle, canDrop)
	end
	m_requestHandle = nil
end

-- Returned from custom drag drop handler
function onCustomDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	Control_SetVisible(gHandles["btnItemDrop1"], false)
	Control_SetVisible(gHandles["imgDialogBG"], false)
	Control_SetEnabled(gHandles["imgDialogBG"], false)
	Control_SetVisible(gHandles["txtSell"], false)
	Control_SetEnabled(gHandles["txtSell"], false)

	m_sellIndex = KEP_EventDecodeNumber(tEvent)

	if m_sellIndex == 0 or not m_overButton then
		if not m_sellFlashed or not m_sellFlashing then
			setEnabledWontBuy(false)
			closeSell()
		end
	elseif m_overButton then
		m_sellItem = deepCopy(decompileInventoryItem(tEvent))
		m_sellItemPID = KEP_EventDecodeNumber(tEvent)
		m_sellItemOrigIndex = KEP_EventDecodeNumber(tEvent)
		m_sellIndex = 0
		for i=1, #g_trades do
			-- Item dropped found in tradables sold
			if m_sellItem.UNID == g_trades[i].output then
				m_sellIndex = i
				m_sellItemOutput = copyTable(g_trades[i], false)
				log("*** m_sellItem.count = "..tostring(m_sellItem.count))
				m_sellItemOutputCount = math.floor((m_sellItem.count/g_trades[i].outputCount)*(g_trades[i].cost/2))
				break
			end
		end

		if m_sellIndex ~= 0 then
			m_sellFlashTime = SELL_FLASH_TIME
			setSellColorAlpha(0)

			Static_SetText(gHandles["txtSellCount"], formatNumberSuffix(m_sellItem.count))
			Static_SetText(gHandles["txtReceiveCount"], formatNumberSuffix(m_sellItemOutputCount))

			local index = ITEMS_PER_PAGE - 1
			g_displayTable[index] = {}
			g_displayTable[index].outputCount = m_sellItem.count
			g_displayTable[index].output = m_sellItem.UNID
			g_displayTable[index].outputProperties = m_sellItem.properties
			g_displayTable[ITEMS_PER_PAGE] = deepCopy(m_sellItemOutput)

			updateItemContainer()

			m_sellFlashing = true
			m_sellFlashed = true

			setSellEnabledVisible(true)

			-- if m_sellItemOutputCount == 0 then
			-- 	Control_SetVisible(gHandles["btnSell"], false)
			-- end 

			setEnabledWontBuy(false)
		else
			returnItem()
			setSellEnabledVisible(false)
			setEnabledWontBuy(true)
		end
	end
end

---Returned when from dropped confirm menu-------
function onDropConfirmSell(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	Control_SetVisible(gHandles["imgSellBG"], true)
	m_sellItem = deepCopy(decompileInventoryItem(tEvent))
	m_sellItemPID = KEP_EventDecodeNumber(tEvent)
	m_sellItemOrigIndex = KEP_EventDecodeNumber(tEvent)
	
	for i=1, ITEMS_PER_PAGE do
		-- Item dropped found in tradables sold
		if m_sellItem.UNID == g_trades[i].output.UNID then
			m_sellIndex = i
			m_sellItemOutput = {}
			m_sellItemOutput = copyTable(g_trades[i], false)
			m_sellItemOutputCount = math.floor((m_sellItem.count/g_trades[i].outputCount)*(g_trades[i].cost/2))
			break
		end
	end

	m_sellFlashTime = SELL_FLASH_TIME
	setSellColorAlpha(0)

	Static_SetText(gHandles["txtSellCount"], formatNumberSuffix(m_sellItem.count))
	Static_SetText(gHandles["txtReceiveCount"], formatNumberSuffix(m_sellItemOutputCount))

	local index = ITEMS_PER_PAGE - 1
	g_displayTable[index] = {}
	g_displayTable[index].outputCount = m_sellItem.count
	g_displayTable[index].output = m_sellItem.UNID
	g_displayTable[index].outputProperties = m_sellItem.properties
	g_displayTable[ITEMS_PER_PAGE] = deepCopy(m_sellItemOutput)

	updateItemContainer()

	m_sellFlashing = true
	m_sellFlashed = true

	setSellEnabledVisible(true)

	setEnabledWontBuy(false)
end

------------------------------
-------Selling Functions------
------------------------------
function setEnabledWontBuy(bool)
	Control_SetVisible(gHandles["txtNotBuy"], bool)
	Control_SetEnabled(gHandles["txtNotBuy"], bool)
	Control_SetVisible(gHandles["btnOk"], bool)
	Control_SetEnabled(gHandles["btnOk"], bool)
end

function closeSell()
	--log("--- closeSell")
	m_sellFlashed = false
	m_dropFlashed = false
	setSellEnabledVisible(false)
	Control_SetVisible(gHandles["txtSell"], false)
	Control_SetEnabled(gHandles["txtSell"], false)
	Control_SetVisible(gHandles["imgSellBG"], false)
end

function returnItem()
	updateItem(m_sellItemOrigIndex, m_sellItemPID, m_sellItem, true)
	updateDirty()

	m_sellItem = {}
	m_sellItemPID = 0
	m_sellItemOrigIndex = 0
end

setSellEnabledVisible = function(isVisible)
	--log("--- setSellEnabledVisible")
	----------------------------
	-- SET VISIBILITY
	----------------------------
	-- Set Backdrop
	local item1 = tostring(ITEMS_PER_PAGE - 1)
	local item2 = tostring(ITEMS_PER_PAGE)
	Control_SetVisible(gHandles["imgSellCostBG1"], isVisible)
	Control_SetVisible(gHandles["imgSellCostBG2"], isVisible)

	-- Set Receiving Item
	Control_SetVisible(gHandles["imgItemBG"..item2], isVisible)
	--Control_SetVisible(gHandles["imgItemCount"..item2], isVisible)
	--Control_SetVisible(gHandles["stcItemCount"..item2], isVisible)
	Control_SetVisible(gHandles["imgItem"..item2], isVisible)
	Control_SetVisible(gHandles["imgItemName"..item2], isVisible)
	Control_SetVisible(gHandles["stcItem"..item2], isVisible)
	Control_SetVisible(gHandles["btnItem"..item2], isVisible)

	-- Set Selling Item
	Control_SetVisible(gHandles["imgItemBG"..item1], isVisible)
	-- Control_SetVisible(gHandles["imgItemCountBG12"], isVisible)
	-- Control_SetVisible(gHandles["stcItemCount12"], isVisible)
	Control_SetVisible(gHandles["imgItem"..item1], isVisible)
	Control_SetVisible(gHandles["imgItemName"..item1], isVisible)
	Control_SetVisible(gHandles["stcItem"..item1], isVisible)
	Control_SetVisible(gHandles["btnItem"..item1], isVisible)

	-- Set buttons and labels
	Control_SetVisible(gHandles["txtSellHeader"], isVisible)
	Control_SetVisible(gHandles["txtReceiveHeader"], isVisible)
	Control_SetVisible(gHandles["txtSellCount"], isVisible)
	Control_SetVisible(gHandles["txtReceiveCount"], isVisible)
	Control_SetVisible(gHandles["btnSell"], (isVisible and m_sellItemOutputCount > 0))
	Control_SetVisible(gHandles["btnCancel"], isVisible)
	
	----------------------------
	-- SET ENABLED
	----------------------------
	-- Set Backdrop
	Control_SetEnabled(gHandles["imgSellCostBG1"], isVisible)
	Control_SetEnabled(gHandles["imgSellCostBG2"], isVisible)

	-- Set Receiving Item
	Control_SetEnabled(gHandles["imgItemBG"..item2], isVisible)
	Control_SetEnabled(gHandles["imgItemCountBG"..item2], false)
	Control_SetEnabled(gHandles["stcItemCount"..item2], false)
	Control_SetEnabled(gHandles["imgItem"..item2], isVisible)
	Control_SetEnabled(gHandles["imgItemName"..item2], isVisible)
	Control_SetEnabled(gHandles["stcItem"..item2], isVisible)
	Control_SetEnabled(gHandles["imgItemLevel"..item2], isVisible)
	Control_SetEnabled(gHandles["btnItem"..item2], isVisible)

	-- Set Selling Item
	Control_SetEnabled(gHandles["imgItemBG"..item1], isVisible)
	Control_SetEnabled(gHandles["imgItemCountBG"..item1], false)
	Control_SetEnabled(gHandles["stcItemCount"..item1], false)
	Control_SetEnabled(gHandles["imgItem"..item1], isVisible)
	Control_SetEnabled(gHandles["imgItemName"..item1], isVisible)
	Control_SetEnabled(gHandles["stcItem"..item1], isVisible)
	Control_SetEnabled(gHandles["imgItemLevel"..item1], isVisible)
	Control_SetEnabled(gHandles["btnItem"..item1], isVisible)

	-- Set buttons and labels
	Control_SetEnabled(gHandles["txtSellHeader"], isVisible)
	Control_SetEnabled(gHandles["txtReceiveHeader"], isVisible)
	Control_SetEnabled(gHandles["txtSellCount"], isVisible)
	Control_SetEnabled(gHandles["txtReceiveCount"], isVisible)
	Control_SetEnabled(gHandles["btnSell"], (isVisible and m_sellItemOutputCount > 0))
	Control_SetEnabled(gHandles["btnCancel"], isVisible)

	if isVisible == false then
		g_displayTable[tonumber(item1)] = nil
		g_displayTable[tonumber(item2)] = nil
	end
end

setSellColorAlpha = function(alpha)
	local item1 = tostring(ITEMS_PER_PAGE - 1)
	local item2 = tostring(ITEMS_PER_PAGE)
	color = {a=alpha,r=255,g=255,b=255}

	-- Set Backdrop
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgSellCostBG1"])), 0, color)
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgSellCostBG2"])), 0, color)

	-- Set Receiving Item
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItemBG"..item2])), 0, color)
	-- BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItemCountBG13"])), 0, color)
	-- BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["stcItemCount13"])), 0, color)
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItem"..item2])), 0, color)
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItemName"..item2])), 0, color)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["stcItem"..item2])), 0, color)
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItemOverlay"..item2])), 0, color)
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItemLevel"..item2])), 0, color)
	--BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["btnItem13"])), 0, color)

	-- Set Selling Item
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItemBG"..item1])), 0, color)
	-- BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItemCountBG12"])), 0, color)
	-- BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["stcItemCount12"])), 0, color)
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItem"..item1])), 0, color)
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItemName"..item1])), 0, color)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["stcItem"..item1])), 0, color)
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItemOverlay"..item1])), 0, color)
	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgItemLevel"..item1])), 0, color)
	--BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["btnItem12"])), 0, color)

	-- Set Labels
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["txtSellHeader"])), 0, color)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["txtReceiveHeader"])), 0, color)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["txtSellCount"])), 0, color)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["txtReceiveCount"])), 0, color)
end