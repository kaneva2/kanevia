--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- NOTE: If you are using InventoryHelper.lua, consider using its built in Framework Tooltip support!
FIELDS = {"header", "subheader", "body", "footer1", "footer2", "footer3"}

--[[ tooltipTable is set up as such:
{
		header 		= "This is a Title, it will be set based on the input",
		subheader 	= { data 	= "you can set any field to have a color by creating a subtable",
						color 	= {a = 255, r = 255, g = 255, b = 255} },
		body 		= "Colors are based on ARGB",
		footer1 	= "if there is no value or data for a field it will be hidden",
		footer2		= {	label 	= "A label is a string that is appended to the front of the data"
						data 	= "If there is a label ': ' will be added to the front of it"
						suffix	= "A suffix is also a string added to the end of the data"},
		footer3		= {	data 	= "labels and suffixes can be combined in any pattern, one does not need the other",
						suffix 	= "The format is label .. ': ' .. data .. suffix. Only data is required"}
	}--]]
	
--[[ To set up: 
		* Call FWTooltipHelper.registerInventoryHandlers() in handler registration
		* Call FWTooltipHelper.registerInventoryEvents() in event registration
		* Call FWTooltipHelper.destroy() in Dialog_OnDestroy
		* Call FWTooltipHelper.addFrameworkTooltip(controlName, tooltipTable) where you want to enable
		* Call FWTooltipHelper.removeFrameworkTooltip(controlName) to disable
		* Call FWTooltipHelper.removeAllFrameworkTooltips() to remove all
		* Call FWTooltipHelper.setDelay(delay) to set the delay
--]]
FWTooltipHelper 			= {}
FWTooltipHelper.tooltips 	= {} -- a table of tooltipTables stored by controlName
FWTooltipHelper.tooltipEvent = nil
FWTooltipHelper.tooltipDelay = 0
FWTooltipHelper.currentTooltip = nil

local g_tooltipControl

function FWTooltipHelper.registerInventoryHandlers()
	KEP_EventRegisterHandler( "showFrameworkTooltipEventHandler", 	"ShowFrameworkTooltipEvent", 	KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "showToolTipEventHandler", 			"ShowToolTipEvent", 			KEP.HIGH_PRIO )
end

function FWTooltipHelper.registerInventoryEvents()
	KEP_EventRegister( "UpdateFrameworkTooltipEvent", 	KEP.MED_PRIO )
	KEP_EventRegister( "ShowFrameworkTooltipEvent", 	KEP.MED_PRIO )
	KEP_EventRegister( "CloseFrameworkTooltipEvent", KEP.MED_PRIO )
end

function FWTooltipHelper.destroy()
--	if FWTooltipHelper.currentTooltip ~= nil then
--		MenuClose("Framework_Tooltip.xml")
--	end
end

-- Setup Tooltip on a single object
function FWTooltipHelper.addFrameworkTooltip(controlName, tooltipTable)
	--log("--- FWTooltipHelper.addFrameworkTooltip")
	FWTooltipHelper.tooltips[controlName] = tooltipTable
	Control_SetToolTip(gHandles[controlName], Dialog_GetXmlName(gDialogHandle) .. "FRAMEWORKTOOLTIP" .. controlName)
end

function FWTooltipHelper.setDelay(delay)
	FWTooltipHelper.tooltipDelay = delay or FWTooltipHelper.tooltipDelay
end

function FWTooltipHelper.removeFrameworkTooltip(controlName)
	Control_SetToolTip(gHandles[controlName], "")
	table.remove(FWTooltipHelper.tooltips, controlName)
end

function FWTooltipHelper.removeAllFrameworkTooltips()
	for controlName in pairs(FWTooltipHelper.tooltips) do
		Control_SetToolTip(gHandles[controlName], "")
	end

	FWTooltipHelper.tooltips = {}
end

function showToolTipEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local title = KEP_EventDecodeString(event)
	local body = KEP_EventDecodeString(event)
	local mouseX = KEP_EventDecodeNumber(event)
	local mouseY = KEP_EventDecodeNumber(event)

	local menuName, controlName = string.match(body, "^(.-)FRAMEWORKTOOLTIP(.-)$")
	--log("--- FrameworkTooltipHelper showToolTipEventHandler ".. tostring(body))

	-- if we're hiding the tooltip, the title/body will be blank so we can't rely on them to determine if FW.
	-- Must rely on tooltipEvent being not nil. Since only 1 tooltip at a time, this will only pass to correct menu
	local hidingFWTooltip = title == "" and body == "" and FWTooltipHelper.tooltipEvent ~= nil

	-- If a shotToolTipEvent was fired for a menu other than me
	if menuName ~= Dialog_GetXmlName(gDialogHandle) then
		if g_tooltipControl then
			if FWTooltipHelper.tooltipEvent then
				KEP_EventCancel(FWTooltipHelper.tooltipEvent)
			end
			FWTooltipHelper.currentTooltip = nil
			KEP_EventCreateAndQueue("CloseFrameworkTooltipEvent")
			g_tooltipControl = nil
			--log("--- FrameworkTooltipHelper showToolTipEventHandler CloseFrameworkTooltipEvent ".. tostring(menuName).. " ".. tostring(Dialog_GetXmlName(gDialogHandle)))
		end
	else
		-- Are we mousing over a valid item for tooltip display?
		if controlName then
			-- Are we mousing over a new item?
			if controlName ~= g_tooltipControl then
				g_tooltipControl = controlName
				
				-- Queue up a ShowToolTipNowEvent in a second
				FWTooltipHelper.tooltipEvent = KEP_EventCreate( "ShowFrameworkTooltipEvent")
				KEP_EventEncodeString(FWTooltipHelper.tooltipEvent, menuName)
				KEP_EventEncodeString(FWTooltipHelper.tooltipEvent, controlName)
				KEP_EventEncodeNumber(FWTooltipHelper.tooltipEvent, mouseX)
				KEP_EventEncodeNumber(FWTooltipHelper.tooltipEvent, mouseY)
				KEP_EventQueueInFutureMS(FWTooltipHelper.tooltipEvent, FWTooltipHelper.tooltipDelay)
			end
		-- If we're leaving a valid tooltip item
		elseif g_tooltipControl then
			if FWTooltipHelper.tooltipEvent then
				KEP_EventCancel(FWTooltipHelper.tooltipEvent)
			end
			FWTooltipHelper.currentTooltip = nil
			KEP_EventCreateAndQueue("CloseFrameworkTooltipEvent")
			g_tooltipControl = nil
		end

		return KEP.EPR_CONSUMED		
	end

	--log("--- FrameworkTooltipHelper showToolTipEventHandler CONSUMED ".. tostring(menuName))
	--return KEP.EPR_CONSUMED
end

function showFrameworkTooltipEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local menuName = KEP_EventDecodeString(event)
	local controlName = KEP_EventDecodeString(event)
	local mouseX = KEP_EventDecodeNumber(event)
	local mouseY = KEP_EventDecodeNumber(event)
	
	if menuName ~= Dialog_GetXmlName(gDialogHandle) then
		--log("--- showFrameworkTooltipEventHandler ".. tostring(menuName))
		return KEP.EPR_OK
	end

	if FWTooltipHelper.currentTooltip == controlName then
	-- request to show same stuff, so ignore it
	else
		-- request to update
		FWTooltipHelper.currentTooltip = controlName
		FWTooltipHelper.updateTooltip(menuName, controlName, mouseX, mouseY)
	end

	--log("--- showFrameworkTooltipEventHandler CONSUMED ".. tostring(menuName))
	return KEP.EPR_CONSUMED
end

function FWTooltipHelper.updateTooltip(menuName, controlName, mouseX, mouseY)
	local event = KEP_EventCreate("UpdateFrameworkTooltipEvent")

	-- if it contains backslash it's a full directory location, pull out just the xml
	if string.find(menuName, "\\") then
		menuName = string.match(menuName, "^.*\\([^\\]-%.xml)$")
	end

	KEP_EventEncodeString(event, menuName)
	KEP_EventEncodeNumber(event, x)
	KEP_EventEncodeNumber(event, y)
	
	KEP_EventEncodeString(event, cjson.encode(FWTooltipHelper.tooltips[controlName]))
	
	MenuOpen("Framework_Tooltip.xml")
	KEP_EventQueue(event)
end