--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

function Log(txt)
	ProgressUpdate("Loading", "Loading::"..txt)
end

function ProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter == PROGRESS_CANCEL) then
		MenuCloseThis()
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "ProgressEventHandler", "ProgressEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "ChatMenuConfigEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ProgressEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate(dialogHandle)
	ProgressOptCancel("Loading", PROGRESS_OPT_CANCEL_NO)
	ProgressOpen("Loading")
end

function Dialog_OnDestroy(dialogHandle)
	ProgressClose("Loading")
	Helper_Dialog_OnDestroy( dialogHandle )
end
