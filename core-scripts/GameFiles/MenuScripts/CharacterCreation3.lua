--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--[[
--Part 3 of the character creation 3. Now....WITH PAPER DOLLS!
]]

dofile("..\\MenuScripts\\LibClient_Common.lua") -- Need this to receive KEP events
dofile("..\\MenuScripts\\MenuTemplateHelper.lua") -- Need this for setupScrollBarTextures
dofile("..\\ClientScripts\\MenuGameIds.lua" ) -- Need this to index MenuGameIds

-- dofile("MenuHelper.lua")
-- dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\ClientScripts\\Utils.lua") -- Need this to do BitFlag operations
dofile("CharacterCreationHelper.lua")

----------------------
-- GLOBAL VARIABLES --
----------------------
g_npcCustoms = {}  --GLIDs and current customizations
g_npcGender = 0
g_saveNPC = false

g_currentURL 		= "" -- Tracks the current URL to be used when rezoning when character customization is finished (PLAYER ONLY)
g_player 			= nil -- Tracks the player object or NPC object that this menu will be customizing
g_netId				= nil
gDirtyFlag 			= 0
gGracefulCleanup	= false
gCancelUpdate 		= false

g_ElapsedTime		= 0
g_TotalTime			= 0
g_LoadedFace 		= false
gCurAnimMode		= ANIMODE_STARTUP
gAnimatedElements	= {}

gGenderIndex		= BODY_MALE_MED
gFaceIndex			= 1
gCurHairIndex		= 1
gCurEyebrow			= 0
gCurFaceCover		= 0
gCurSkinColor		= 0
gCurEyeColor		= 0
gCurBeardColor		= 0
gCurHairColor		= 0

gOrigGenderIndex	= BODY_MALE_MED
gOrigFaceIndex		= 1
gOrigCurHairIndex	= 1
gOrigCurEyebrow		= 0
gOrigCurFaceCover	= 0
gOrigCurSkinColor	= 0
gOrigCurEyeColor	= 0
gOrigCurBeardColor	= 0
gOrigCurHairColor	= 0

gCurrentScaleY		= 100
gCurrentScaleXZ		= 100
gOrigCurrentScaleXZ = 100
gOrigCurrentScaleY 	= 100
-- Big ole hack for beefy not being the same height as other male actors
gBeefyMod = 0

-- The base config sent to the server
gCharBaseConfig 	= nil

g_DontZoom = true
gViewport 			= {}
gViewportDirtyFlag 	= 0
gViewportsClean 	= false
gDoViewportUpdate 	= true -- Used to turn the update function on and off. The Dialog_OnUpdate is constantly called and when we close
-- the menu the mini viewports are destroyed but it tries to update them causing a crash.
gCurViewportMode	= VPMODE_HAIR
gPreviousNoSelDirty = 0 -- Used if we select a menu but dont select an option and move to a new menu.  Update viewport with old values
gViewportScroll		= nil
gViewportFirstIndex = 1
gCurVPpos 			= {x = 0, 	y = 0, 	 z = 0}
gCurVPscale			= {x = 100, y = 100, z = 100}
gCurVProt			= {x = 0, 	y = 0, 	 z = 0}

gLabelMode			= nil

MENU_VERSIONS = {
	PLAYER = "player",
	NPC = "npc"
}

g_menuVersion = nil

function onCreate()

	-- Register Events
	KEP_EventRegister("MenuMovedEvent", KEP.MED_PRIO)
	-- KEP_EventRegister(CHAT_MINIMIZE_EVENT, KEP.MED_PRIO)
	-- KEP_EventRegister("ValidateURLEvent", KEP.MED_PRIO)
	-- KEP_EventRegister("ZoneNavigationEvent", KEP.MED_PRIO)

	-- Register Event Handlers
	KEP_EventRegister("FRAMEWORK_NPC_SAVE_CLOTHING")
	KEP_EventRegisterHandler("SaveClothingHandler", "FRAMEWORK_NPC_SAVE_CLOTHING", KEP.MED_PRIO)
	KEP_EventRegisterHandler("zoneNavigationEventHandler", "ZoneNavigationEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("frameworkInitCharCreatorHandler", "FRAMEWORK_INIT_CHAR_CREATOR", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("menuMovedEventHandler", "MenuMovedEvent", KEP.MED_PRIO)

	-- Get the URL to use for rezoning when character customization is finished (PLAYER ONLY)
	local event = KEP_EventCreate( "ZoneNavigationEvent")
	KEP_EventSetFilter(event, ZONE_NAV.CURRENT_URL_REQUEST)
	KEP_EventQueue(event)
	
	-- Seed the random number generator - is this necessary?
	math.randomseed(KEP_CurrentTime())
 
	-- Turn off camera collision to avoid any weird camera issue during avatar customization.
	-- Since player will respawn when this menu is closed, there is no need to restore this flag.
	KEP_SetCameraCollision(0)
end

function onDestroy()
	if g_DontZoom then
		resetCamera() -- Save this position when create, reset to it when destroy. Also, reset player visibility?
	end

	if g_menuVersion == MENU_VERSIONS.NPC then
		-- Despawn the placeholder NPC
		KEP_npcDeSpawn({netId = g_netId})
		g_netId = nil
		-- Show the player again
		Events.sendEvent("SHOW_PLAYER", {playerName = KEP_GetLoginName()})
	end

	-- WARNING: Remove the viewports or they will persists across worlds
	if gGracefulCleanup == false then
		-- Make sure menugameobjects are removed even under extreme conditions
		disableViewportUpdate()
		if (gViewportsClean == false) then
			cleanupMenuViewports()
		end
	end

	-- Is this bit necessary?

	-- local ev = KEP_EventCreate( HUD_MINIMIZE_EVENT)
	-- if(ev ~= 0) then
	-- 	KEP_EventEncodeNumber(ev, 0)
	-- 	KEP_EventQueue( ev)
	-- end
end

function Dialog_OnRender(dialogHandle, elapsedTime)
	
	if (g_LoadedFace) then
		updateAnimation(elapsedTime)
		if gDoViewportUpdate then
			moveViewPorts()
		end
	end

	if g_LoadedFace == true then
		return
	end
	g_TotalTime = g_TotalTime + elapsedTime
	if g_TotalTime > 1 then
		g_TotalTime = 0
		if getCurFace() ~= 0 and g_menuVersion ~= nil then
			g_LoadedFace = true
			finalizeInit()

			-- Is this bit necessary?

			-- local ev = KEP_EventCreate(KILL_BLACK_SCREEN)
			-- if(ev ~= 0) then
			-- 	KEP_EventQueue( ev)
			-- end
		end
	end
end


function Dialog_OnMoved(dialogHandle, ptX, ptY)
	updateMenuOnMoved()
end

--------------------
-- EVENT HANDLERS --
--------------------

function frameworkInitCharCreatorHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local tempMenuVersion = KEP_EventDecodeString(event)
	if tempMenuVersion == MENU_VERSIONS.PLAYER then
		g_menuVersion = MENU_VERSIONS.PLAYER
		initPlayerMode()
	elseif tempMenuVersion == MENU_VERSIONS.NPC then
		g_menuVersion = MENU_VERSIONS.NPC
		g_npcGender = KEP_EventDecodeNumber(event)
		if KEP_EventMoreToDecode(event) == 1 then
			local eventData = KEP_EventDecodeString(event)
			eventData = string.gsub(eventData, "\'", "\"")
			if eventData then
				g_npcCustoms = Events.decode(eventData)
			end
		end
		--KEP_SetCameraLock(1)
		MenuSetCaptureKeysThis(true)
		--gDialog_SetModal(gDialogHandle, false)

		initNpcMode()
		
	else
		LogWarn("frameworkInitCharCreatorHandler: INVALID TYPE - menuVersion="..tostring(tempMenuVersion))
		return
	end
end

function zoneNavigationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ZONE_NAV.CURRENT_URL_INFO then
		local url = KEP_EventDecodeString( event )
		g_currentURL = url
	end
end

function SaveClothingHandler( dispatcher, fromNetid, event, eventid, filter, objectid)
	local eventData = KEP_EventDecodeString(event)
	g_npcCustoms["GLIDS"] = Events.decode(eventData)
	finalizeInit()
	updateMenuOnMoved()
end

function menuMovedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	updateMenuOnMoved()
end

---------------------
-- LOCAL FUNCTIONS --
---------------------

-- Initialization functions --
------------------------------

function initNpcMode()
	-- Hide the player avatar
	Events.sendEvent("HIDE_PLAYER", {playerName = KEP_GetLoginName()})
	MenuCloseAllNonEssentialExceptThis(true)
	-- Generate a placeholder NPC to dress up
	spawnNPC()
	postModeSelectInit()
	Control_SetEnabled(gHandles["btnClothing"], true)
	Control_SetVisible(gHandles["btnClothing"], true)
end

function initPlayerMode()
	-- Set up the player to dress up
	MenuCloseAllNonEssentialExceptThis(false)
	g_player = KEP_GetPlayer()
	postModeSelectInit()
	Control_SetEnabled(gHandles["btnClothing"], false)
	Control_SetVisible(gHandles["btnClothing"], false)
end

function postModeSelectInit()
	readOriginalConfigs()

	local temp = getCurFace()
	if (temp == 1 or temp == nil ) then
		return
	end
	g_LoadedFace = true
	finalizeInit()
end

-- Shut down functions --
-------------------------

function cancelUpdate()
	-- CALL disableViewportUpdate FIRST or cleanupMenuViewports will destroy the viewport objects Dialog_OnUpdate is trying to manipulate
	gCancelUpdate = true
	disableViewportUpdate()
	cleanupMenuViewports()
	gGracefulCleanup = true
	setOriginalConfigs()
	gDirtyFlag = FLAGS_ALL_SET
	updateBodyParts()

	if g_menuVersion == MENU_VERSIONS.PLAYER then
		local player = KEP_GetPlayer()
		if player ~= nil then
			zone_index = KEP_GetCurrentZoneIndex()
			zone_index = InstanceId_GetId(zone_index)

			if zone_index == GameGlobals.CHARACTER_CREATION_ZI then
				local e = KEP_EventCreate(APT_SELECTION_EVENT)
				if e ~= 0 then
					KEP_EventSetFilter(e, FORCED_STARTING_APARTMENT)
					KEP_EventAddToServer( e )
					KEP_EventQueue( e )
					markGuideClosedInConfig()
				end
				-- Hide menu to make sure no other buttons will be clicked while waiting for server response
				Dialog_SetMinimized(gDialogHandle, 1)
				return
			end
		end
	end

	MenuCloseThis()

	if g_menuVersion == MENU_VERSIONS.PLAYER then
		-- Rezone To Refresh Avatar, Zone & Menus
		Rezone()
	else
		local ev = KEP_EventCreate("FRAMEWORK_SAVE_NPC_DATA")
		if g_saveNPC then
			KEP_EventEncodeString(ev, Events.encode(g_npcCustoms))
			KEP_EventEncodeNumber(ev, g_npcGender)
		end
		KEP_EventQueue(ev)
	end
end

-- Sends RefreshEvent To Hud2 Who Rezones Us
function Rezone()
	if g_currentURL ~= "" then 
		local ev = KEP_EventCreate("ValidateURLEvent")
		KEP_EventEncodeString (ev, g_currentURL)
		KEP_EventEncodeString (ev,"Rezone")
		KEP_EventQueue(ev)
	end
end

function markGuideClosedInConfig()
	g_userName = KEP_GetLoginName()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetNumber(config, "GuideClosed"..g_userName, 0)
		KEP_ConfigSave( config )
	end
end

-- Player data functions --
---------------------------

function spawnNPC()
	-- Set up NPC basics
	--local genderIndex = math.random(1, 2) -- Gender passed in? -- 1 = male, 2 = female -> turn into constants
	g_netId = 1000 -- Use a better method of determining this
	local name = "npc"..g_netId
	local posX, posY, posZ, rotDeg = KEP_GetPlayerPosition()
	local dbIndex = 0

	if not ( next(g_npcCustoms) ) then
		if (g_npcGender == FEMALE) then
			dbIndex = math.random(BODY_FEM_THIN + DB_INDEX_OFFSET, BODY_FEM_BIG + DB_INDEX_OFFSET)
			g_npcCustoms["dbIndex"] = dbIndex
			if not g_npcCustoms["GLIDS"] then
				g_npcCustoms["GLIDS"] = {1247, 1248, 1249}
			end
		else
			dbIndex = math.random(BODY_MALE_THIN + DB_INDEX_OFFSET, BODY_MALE_BIG + DB_INDEX_OFFSET)
			g_npcCustoms["dbIndex"] = dbIndex
			if not g_npcCustoms["GLIDS"] then
				g_npcCustoms["GLIDS"] = {1172, 1173, 1174}
			end
		end
	else
		dbIndex = g_npcCustoms["dbIndex"]
	end
		gGenderIndex = dbIndex - DB_INDEX_OFFSET -- i.e. body type
		gOrigGenderIndex = gGenderIndex

	-- Spawn the NPC

	--Log("spawnNPC - \'"..name.."\'")
	
	-- Randomize NPC appearance
	if g_npcCustoms["faceIndex"] then
		configureNPC()
	else
		randomizeNPC()
	end
	
	local newNPC = KEP_npcSpawn( {
		name=name, 
		netId=g_netId,
		charConfigId=g_npcGender, -- Gender index
		dbIndex=dbIndex, -- Actor ID
		pos={x=posX, y=posY, z=posZ},
		rotDeg=rotDeg, 
		glidAnimSpecial= 0, 
		glidsArmed= g_npcCustoms["GLIDS"]
	} )

	local faceGLID
	if g_npcGender == FEMALE then
		faceGLID = FACES_FEMALE[gFaceIndex]
	else
		faceGLID = FACES_MALE[gFaceIndex]
	end
	-- Prepare NPC for further customization
	KEP_SetActorTypeOnPlayer(newNPC, ACTOR_ID[g_npcGender])
	-- Face
	KEP_ArmEquippableItem(newNPC, faceGLID, gCurSkinColor)
	KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, CONFIG_HEAD[3], 0, gCurSkinColor)
	-- Eyes
	KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, FACE_EYE_LEFT, 0, gCurEyeColor)
	KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, FACE_EYE_RIGHT, 0, gCurEyeColor)
	-- Eyebrows
	KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, FACE_EYEBROW, gCurEyebrow, gCurBeardColor)
	-- Beard and makeup
	if g_npcGender == FEMALE then
		KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, FACE_FACECOVER, 0, gCurFaceCover)
	else
		KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, FACE_FACECOVER, gCurFaceCover, gCurBeardColor)
	end
	-- Hair
	if g_npcGender == MALE then
		KEP_SetCharacterAltConfigOnPlayer(newNPC, 25, gCurHairIndex, gCurHairColor)
		KEP_SetCharacterAltConfigOnPlayer(newNPC, 26, gCurHairIndex, gCurHairColor)
	end
	KEP_SetCharacterAltConfigOnPlayer(newNPC, 27, gCurHairIndex, gCurHairColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, 28, gCurHairIndex, gCurHairColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, 29, gCurHairIndex, gCurHairColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, 30, gCurHairIndex, gCurHairColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, 31, gCurHairIndex, gCurHairColor)

	-- Body
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_FEET[3], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_TORSO[3], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_LEGS[3], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_NECK[3], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_CROTCH[3], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_HANDS[3], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_ARMS[3], -1, gCurSkinColor)
	-- Scale

	-- Apply scale
	--KEP_ScaleRuntimePlayer(newNPC, gCurrentScaleXZ, gCurrentScaleY, gCurrentScaleXZ)

	-- Set the pointer to the npc object for future editing
	g_player = newNPC
end

function randomizeNPC()
	local faceIndex
	local faceGLID
	if genderIndex == FEMALE then
		faceIndex = math.random(#FACES_FEMALE)
		faceGLID = FACES_FEMALE[faceIndex]
	else
		faceIndex = math.random(#FACES_MALE)
		faceGLID = FACES_MALE[faceIndex]
	end
	gFaceIndex	= faceIndex
	
	local skinColor = math.random(0, MAX_SKIN_COLOR)
	local eyebrow
	local faceCover
	local hairIndex
	local hairColor
	local beardColor

	if g_npcGender == FEMALE then
		-- gGenderIndex = math.random( BODY_FEM_THIN, BODY_FEM_BIG )
		-- gFaceIndex = math.random( #FACES_FEMALE )
		eyebrow = math.random(0, MAX_BROW_FEM)
		faceCover = math.random(0, MAX_MAKEUP)
		hairIndex = math.random(#HAIR_COLORS_FEM)
		hairColor = math.random(0, HAIR_COLORS_FEM[hairIndex])
		beardColor	= math.random(0, MAX_BROW_COLOR)
		gCurHairColor = hairColor
		gCurHairIndex = hairIndex
		gCurFaceCover = faceCover
		gCurBeardColor = beardColor
	else
		-- gGenderIndex = math.random( BODY_MALE_THIN, BODY_MALE_BIG )
		-- gFaceIndex = math.random( #FACES_MALE )
		eyebrow	= math.random(0, MAX_BROW_MALE)
		faceCover = math.random(0, MAX_BEARD)
		hairIndex = math.random(#HAIR_COLORS_MALE)
		hairColor = math.random(0, HAIR_COLORS_MALE[hairIndex])
		beardColor	= math.random(0, MAX_BEARD_COLOR)
		gCurHairColor = hairColor
		gCurHairIndex = hairIndex
		gCurFaceCover = faceCover
	end

	skinColor	= math.random(0, MAX_SKIN_COLOR)
	eyeColor	= math.random(0, MAX_EYE_COLOR)

	local MIN_Y_SCALE = 50 
	local MAX_Y_SCALE = 150
	local MIN_XZ_SCALE = 75
	local MAX_XZ_SCALE = 125
	local SCALE_INTERVAL = 5
	-- Generate random percent in SCALE_INTERVAL
	-- Slider: MIN = 0 = 5'3", MAX = 100 = 6'5", DEF 50 = 5'10"
	local scaleXZ = math.random(SCALE_INTERVAL*MIN_Y_SCALE, SCALE_INTERVAL*MAX_Y_SCALE) / (SCALE_INTERVAL*100) -- Height
	-- Slider: 0 - 100, DEF 50
	local scaleY = math.random(SCALE_INTERVAL*MIN_XZ_SCALE, SCALE_INTERVAL*MAX_XZ_SCALE) / (SCALE_INTERVAL*100) -- Weight
	-- Keep scales within a reasonable range of each other or else it starts to look weird
	local diff = math.abs(scaleY - scaleXZ)
	if diff > (SCALE_TOLERANCE/100) then
		if scaleY > scaleXZ then
			scaleXZ = scaleY - (SCALE_TOLERANCE/100)
		else
			scaleXZ = scaleY + (SCALE_TOLERANCE/100)
		end
	end

	gCurrentScaleXZ = scaleXZ
	gCurrentScaleY = scaleY

	saveNPCData()
	saveOriginalData()
end

function configureNPC()
	gFaceIndex = g_npcCustoms["faceIndex"] 	
	gCurEyebrow = g_npcCustoms["eyebrowIndex"] 
	gCurSkinColor = g_npcCustoms["skinColor"]
	gCurEyeColor = g_npcCustoms["eyeColor"]
	gCurHairIndex = g_npcCustoms["hairIndex"]
	gCurHairColor = g_npcCustoms["hairColor"]
	gCurFaceCover = g_npcCustoms["faceCover"]
	gCurBeardColor = g_npcCustoms["beardColor"] 
	gCurrentScaleXZ = g_npcCustoms["scaleXZ"]
	gCurrentScaleY = g_npcCustoms["scaleY"]
	saveOriginalData()
end

function saveNPCData()
	g_npcCustoms["faceIndex"] = gFaceIndex	
	g_npcCustoms["eyebrowIndex"] = gCurEyebrow
	g_npcCustoms["skinColor"] = gCurSkinColor
	g_npcCustoms["eyeColor"] = gCurEyeColor
	g_npcCustoms["hairIndex"] = gCurHairIndex
	g_npcCustoms["hairColor"] = gCurHairColor
	g_npcCustoms["faceCover"] = gCurFaceCover
	g_npcCustoms["beardColor"] = gCurBeardColor
	g_npcCustoms["scaleXZ"] = gCurrentScaleXZ
	g_npcCustoms["scaleY"] = gCurrentScaleY
end

function saveOriginalData()
	gOrigCurEyebrow = gCurEyebrow
	gOrigCurSkinColor = gCurSkinColor
	gOrigCurEyeColor = gCurEyeColor
	gOrigCurHairIndex = gCurHairIndex
	gOrigCurHairColor = gCurHairColor
	gOrigCurBeardColor = gCurBeardColor
	gOrigCurFaceCover = gCurFaceCover
	gOrigFaceIndex = gFaceIndex
	gOrigCurrentScaleXZ = gCurrentScaleXZ
	gOrigCurrentScaleY = gCurrentScaleY
end


function readOriginalConfigs()
	local player = g_player
	if g_menuVersion == MENU_VERSIONS.PLAYER then
	 	player = KEP_GetPlayer() -- Need to re-grab player object in case his body changed
	end

	if player ~= nil and g_menuVersion == MENU_VERSIONS.PLAYER then -- Original configs for NPC are saved at time of NPC spawn
		local bodyIndex = GetSet_GetNumber(player, MovementIds.IDXREFMODEL)
		gGenderIndex = findIndexByValue(ACTOR_ID, bodyIndex)
		gOrigGenderIndex = gGenderIndex
		gFaceIndex	= getCurFace()
		gOrigFaceIndex = gFaceIndex

		gCurEyebrow = KEP_GetPlayerConfigIndex(player, getFaceAssByIndex(), FACE_EYEBROW)
		gOrigCurEyebrow = gCurEyebrow

		gCurSkinColor = KEP_GetPlayerConfigMaterial(player, 0, CONFIG_CROTCH[gGenderIndex])
		gOrigCurSkinColor = gCurSkinColor

		gCurEyeColor = KEP_GetPlayerConfigMaterial(player, getFaceAssByIndex(), FACE_EYE_LEFT)
		gOrigCurEyeColor = gCurEyeColor

		if not isFemale() then
			gCurHairColor = KEP_GetPlayerConfigMaterial(player, 0, 25)
			gCurHairIndex = KEP_GetPlayerConfigIndex(player, 0, 25)
			gCurFaceCover = KEP_GetPlayerConfigIndex(player, getFaceAssByIndex(), FACE_FACECOVER)
			gCurBeardColor = KEP_GetPlayerConfigMaterial(player, getFaceAssByIndex(), FACE_FACECOVER)
		else
			gCurHairColor = KEP_GetPlayerConfigMaterial(player, 0, 27)
			gCurHairIndex = KEP_GetPlayerConfigIndex(player, 0, 27)
			gCurFaceCover = KEP_GetPlayerConfigMaterial(player, getFaceAssByIndex(), FACE_FACECOVER)
		end

		gOrigCurHairIndex = gCurHairIndex
		gOrigCurHairColor = gCurHairColor
		gOrigCurBeardColor = gCurBeardColor
		gOrigCurFaceCover = gCurFaceCover

		local scale = KEP_GetRuntimePlayerScale(player)

		gCurrentScaleXZ = math.floor(scale.x*100)
		gCurrentScaleY = math.floor(scale.y*100)
		gOrigCurrentScaleXZ = gCurrentScaleXZ
		gOrigCurrentScaleY = gCurrentScaleY
	end
end

function setOriginalConfigs()
	gGenderIndex = gOrigGenderIndex
	gFaceIndex	= gOrigFaceIndex
	gCurHairIndex = gOrigCurHairIndex
	gCurEyebrow = gOrigCurEyebrow
	gCurFaceCover = gOrigCurFaceCover
	gCurSkinColor = gOrigCurSkinColor
	gCurEyeColor = gOrigCurEyeColor
	gCurBeardColor = gOrigCurBeardColor
	gCurHairColor = gOrigCurHairColor
	gCurrentScaleXZ = gOrigCurrentScaleXZ
	gCurrentScaleY = gOrigCurrentScaleY
end

function updateBodyParts()
	local player = g_player
	if g_menuVersion == MENU_VERSIONS.PLAYER then
	 	player = KEP_GetPlayer() -- Need to re-grab player object in case his body changed
	end
	if player ~= nil and gDirtyFlag ~= 0 then
		local newBody = false
		-- !!! NOTE: until Jonny's fix is in configBody MUST come before configFace !!!
		if BitFlag.test(gDirtyFlag, FLAGS.BODY) then
			configBody(player)
			-- update player
			if g_menuVersion == MENU_VERSIONS.PLAYER then
			 	player = KEP_GetPlayer() -- Need to re-grab player object in case his body changed
			end
			gDirtyFlag = BitFlag.clear(gDirtyFlag, FLAGS.BODY)
		end
		if BitFlag.test(gDirtyFlag, FLAGS.FACE) then
			configFace(player)
			gDirtyFlag = BitFlag.clear(gDirtyFlag, FLAGS.FACE)
		end
		if BitFlag.test(gDirtyFlag, FLAGS.EYECOLOR) then
			configEyeColor(player)
			gDirtyFlag = BitFlag.clear(gDirtyFlag, FLAGS.EYECOLOR)
		end
		if BitFlag.test(gDirtyFlag, FLAGS.BROW) then
			configEyebrow(player)
			gDirtyFlag = BitFlag.clear(gDirtyFlag, FLAGS.BROW)
		end
		if BitFlag.test(gDirtyFlag, FLAGS.FACECOVER) then
			configBeardAndMakeupColors(player)
			gDirtyFlag = BitFlag.clear(gDirtyFlag, FLAGS.FACECOVER)
		end
		if BitFlag.test(gDirtyFlag, FLAGS.HAIR) then
			configHair(player)
			gDirtyFlag = BitFlag.clear(gDirtyFlag, FLAGS.HAIR)
		end
		if BitFlag.test(gDirtyFlag, FLAGS.BODY_TYPE) then
			newBody = true -- What does this accomplish?
			gDirtyFlag = BitFlag.clear(gDirtyFlag, FLAGS.BODY_TYPE)
		end
		if BitFlag.test(gDirtyFlag, FLAGS.HEIGHT) then
			newBody = true -- What does this accomplish?
			gDirtyFlag = BitFlag.clear(gDirtyFlag, FLAGS.HEIGHT)
		end
		if BitFlag.test(gDirtyFlag, FLAGS.WEIGHT) then
			newBody = true -- What does this accomplish?
			gDirtyFlag = BitFlag.clear(gDirtyFlag, FLAGS.WEIGHT)
		end
	end
end

function configBody(oldPlayer)
	KEP_SetActorTypeOnPlayer(oldPlayer, ACTOR_ID[gGenderIndex])
	local player = g_player
	if g_menuVersion == MENU_VERSIONS.PLAYER then
	 	player = KEP_GetPlayer() -- Need to re-grab player object in case his body changed
	end
	configScale()
	KEP_SetCharacterAltConfigOnPlayer(player, CONFIG_TORSO[gGenderIndex], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(player, CONFIG_LEGS[gGenderIndex], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(player, CONFIG_CROTCH[gGenderIndex], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(player, CONFIG_NECK[gGenderIndex], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(player, CONFIG_HANDS[gGenderIndex], -1, gCurSkinColor)
	KEP_SetCharacterAltConfigOnPlayer(player, CONFIG_FEET[gGenderIndex], -1, gCurSkinColor)
	if isFemale() then
		KEP_SetCharacterAltConfigOnPlayer(player, CONFIG_ARMS[gGenderIndex], -1, gCurSkinColor)
	else
		KEP_SetCharacterAltConfigOnPlayer(player, CONFIG_ARMS[gGenderIndex], -1, gCurSkinColor)
	end
end

function configFace(player)
	local faceGLID = getFaceByIndex()
	KEP_ArmEquippableItem(player, faceGLID, gCurSkinColor)
	KEP_PlayerObjectSetEquippableConfig(player, faceGLID, CONFIG_HEAD[gGenderIndex], 0, gCurSkinColor)
end

function configEyeColor(player)
	local faceGLID = getFaceByIndex()
	KEP_PlayerObjectSetEquippableConfig(player, faceGLID, FACE_EYE_LEFT, 0, gCurEyeColor)
	KEP_PlayerObjectSetEquippableConfig(player, faceGLID, FACE_EYE_RIGHT, 0, gCurEyeColor)
end

function configEyebrow(player)
	local faceGLID = getFaceByIndex()
	KEP_PlayerObjectSetEquippableConfig(player, faceGLID, FACE_EYEBROW, gCurEyebrow, gCurBeardColor)
end

function configBeardAndMakeupColors(player)
	local faceGLID = getFaceByIndex()
	if isFemale() then
		KEP_PlayerObjectSetEquippableConfig(player, faceGLID, FACE_FACECOVER, 0, gCurFaceCover)
	else
		KEP_PlayerObjectSetEquippableConfig(player, faceGLID, FACE_FACECOVER, gCurFaceCover, gCurBeardColor)
	end
end

function configHair(player)
	hairColor = gCurHairColor
	-- Do this because not all hair styles (meshes) have the same amount of colors (materials)
	local colorIndex = HAIR_COLORS_MALE[gCurHairIndex + 1]
	if isFemale() then
		colorIndex = HAIR_COLORS_FEM[gCurHairIndex + 1]
	end
	if (not hairColor or not colorIndex )  or hairColor > colorIndex then
		hairColor = 0
	end
	if not isFemale() then
		KEP_SetCharacterAltConfigOnPlayer(player, 25, gCurHairIndex, hairColor)
		KEP_SetCharacterAltConfigOnPlayer(player, 26, gCurHairIndex, hairColor)
	end
	KEP_SetCharacterAltConfigOnPlayer(player, 27, gCurHairIndex, hairColor)
	KEP_SetCharacterAltConfigOnPlayer(player, 28, gCurHairIndex, hairColor)
	KEP_SetCharacterAltConfigOnPlayer(player, 29, gCurHairIndex, hairColor)
	KEP_SetCharacterAltConfigOnPlayer(player, 30, gCurHairIndex, hairColor)
	KEP_SetCharacterAltConfigOnPlayer(player, 31, gCurHairIndex, hairColor)
end

function configScale()
	local sliderXZ = Dialog_GetSlider(gDialogHandle, "sldScaleXZ")
	local sliderY = Dialog_GetSlider(gDialogHandle, "sldScaleY")
	Slider_SetValue(sliderY, gCurrentScaleY)
	Slider_SetValue(sliderXZ, gCurrentScaleXZ)
	sldScaleY_OnSliderValueChanged(sliderY) -- TO DO: Wrap this to a function rather than calling a button handler
	sldScaleXZ_OnSliderValueChanged(sliderXZ) -- TO DO: Wrap this to a function rather than calling a button handler
end

function validateCharacter()
	local team = 2
    putOnWizardsRobeAndHat()
    if gCharBaseConfig ~= 0 and gCharBaseConfig ~= nil then
        KEP_CreateCharacter( gCharBaseConfig, GameGlobals.STARTER_HOME_ID, KEP_GetLoginName(), team, gCurrentScaleXZ, gCurrentScaleY, gCurrentScaleXZ )
    end
end


-- This function configures a generic menu game object with the specifics chosen in character creation and sends you to the server nekkid.
--	This is done so that the players base config will not have any clothing that would prevent them from disrobing.
function putOnWizardsRobeAndHat()
	gCharBaseConfig = KEP_AddMenuGameObject( ACTOR_ID[gGenderIndex], IDLE_ANIM_ID)
	if gCharBaseConfig ~= 0 and gCharBaseConfig ~= nil then

		--Put move the render window offscreen, make 2 x 2 so you wont see
		--nekkid people if something messes up
		GetSet_SetVector(gCharBaseConfig, MenuGameIds.VP_POSITION, -10, -10, 0)
		GetSet_SetVector(gCharBaseConfig, MenuGameIds.VP_DIMENSIONS, 2, 2, 0)

		local faceGLID = getFaceByIndex()

		-- Config the head
		KEP_ArmMenuGameObject(gCharBaseConfig, faceGLID, 0)
		KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, CONFIG_HEAD[gGenderIndex], 0, gCurSkinColor)
		KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, FACE_EYE_LEFT, 0, gCurEyeColor)
		KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, FACE_EYE_RIGHT, 0, gCurEyeColor)
		KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, FACE_EYEBROW, gCurEyebrow, gCurBeardColor)
		if isFemale() then
			KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, FACE_FACECOVER, 0, gCurFaceCover)
		else
			KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, FACE_FACECOVER, gCurFaceCover, gCurBeardColor)
		end

		-- Config the eyes
		KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, FACE_EYE_LEFT, 0, gCurEyeColor)
		KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, FACE_EYE_RIGHT, 0, gCurEyeColor)

		-- Config the eyebrows
		KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, FACE_EYEBROW, gCurEyebrow, gCurBeardColor)

		-- Config the face cover
		if isFemale() then
			KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, FACE_FACECOVER, 0, gCurFaceCover)
		else
			KEP_MenuGameObjectSetEquippableConfig(gCharBaseConfig, faceGLID, FACE_FACECOVER, gCurFaceCover, gCurBeardColor)
		end

		-- Config the hair
		if not isFemale() then
			KEP_SetCharacterAltConfig(gCharBaseConfig, 25, gCurHairIndex, gCurHairColor)
			KEP_SetCharacterAltConfig(gCharBaseConfig, 26, gCurHairIndex, gCurHairColor)
		end
		KEP_SetCharacterAltConfig(gCharBaseConfig, 27, gCurHairIndex, gCurHairColor)
		KEP_SetCharacterAltConfig(gCharBaseConfig, 28, gCurHairIndex, gCurHairColor)
		KEP_SetCharacterAltConfig(gCharBaseConfig, 29, gCurHairIndex, gCurHairColor)
		KEP_SetCharacterAltConfig(gCharBaseConfig, 30, gCurHairIndex, gCurHairColor)
		KEP_SetCharacterAltConfig(gCharBaseConfig, 31, gCurHairIndex, gCurHairColor)

		-- Config the skin tone
		KEP_SetCharacterAltConfig(gCharBaseConfig, CONFIG_NECK[gGenderIndex],  0, gCurSkinColor)
		KEP_SetCharacterAltConfig(gCharBaseConfig, CONFIG_TORSO[gGenderIndex], 0, gCurSkinColor)
		KEP_SetCharacterAltConfig(gCharBaseConfig, CONFIG_HANDS[gGenderIndex], 0, gCurSkinColor)
		KEP_SetCharacterAltConfig(gCharBaseConfig, CONFIG_CROTCH[gGenderIndex], 0, gCurSkinColor)
		KEP_SetCharacterAltConfig(gCharBaseConfig, CONFIG_LEGS[gGenderIndex],  0, gCurSkinColor)
		KEP_SetCharacterAltConfig(gCharBaseConfig, CONFIG_FEET[gGenderIndex],  0, gCurSkinColor)
		if isFemale() then
			KEP_SetCharacterAltConfig(gCharBaseConfig, CONFIG_ARMS[gGenderIndex], 0, gCurSkinColor)
		else
			KEP_SetCharacterAltConfig(gCharBaseConfig, CONFIG_ARMS[gGenderIndex], 0, gCurSkinColor)
		end
	end
end

-- Display Functions --
-----------------------

function finalizeInit()
	readOriginalConfigs()
	setupMenu()
	initViewPorts()
	setupActor()
	bntVPBodyType_OnButtonClicked(nil) -- TO DO: Wrap this to a function rather than calling a button handler

	if isFemale() then
		local bh = Dialog_GetButton( gDialogHandle, "btnVPBeardColor" )
		local s_bh = Button_GetStatic(bh)
		Static_SetText(s_bh, "Eyebrow Color")
		bh = Dialog_GetButton( gDialogHandle, "btnVPFaceCover" )
		s_bh = Button_GetStatic(bh)
		Static_SetText(s_bh, "Makeup")
	end
end

function setupMenu()
	local hud_height 		= 83
	local hud_x_from_mid 	= 407
	local screenW = Dialog_GetScreenWidth(gDialogHandle)
	local screenH = Dialog_GetScreenHeight(gDialogHandle)
	local menuH = Dialog_GetHeight(gDialogHandle)
	local x = (screenW / 2) - hud_x_from_mid
	Dialog_SetLocation(gDialogHandle, x, screenH - menuH - hud_height)

	local slider = nil
	slider = Dialog_GetSlider(gDialogHandle, "sldScaleY")
	Slider_SetRange(slider, 90, 110)
	Slider_SetValue(slider, 100)
	slider = Dialog_GetSlider(gDialogHandle, "sldScaleXZ")
	Slider_SetRange(slider, 90, 110)
	Slider_SetValue(slider, 100)
	gLabelMode = Dialog_GetStatic(gDialogHandle, "lblMode")
	showControlList(
		{"btnVPBack", "btnVPForward", "bntVPBodyType",
		"btnVPSkin", "btnVPFace", "btnVPEyes",
		"btnVPEyebrow", "btnVPHair", "btnVPHairColor",
		"btnVPFaceCover", "btnVPBeardColor", "btnRandomize",
		"btnSave", "sldScaleXZ", "sldScaleY", "lblHeightCurrent", "btnCancel"}, false)
	gViewportScroll = Dialog_GetListBox(gDialogHandle, "lstViewportScroll")
	setupScrollBarTextures(gViewportScroll)
end

function updateMenuOnMoved()
	setupMenu()
	setupMenuViewports()
	showControlList( { "bntVPBodyType", "btnVPSkin", "btnVPFace", "btnVPEyes",
		"btnVPEyebrow", "btnVPHair", "btnVPHairColor",
		"btnVPFaceCover", "btnVPBeardColor", "btnRandomize",
		"btnSave", "btnCancel" }, true )
	if gCurViewportMode == VPMODE_BODYTYPE then
		showControl( "sldScaleXZ", true )
		showControl( "sldScaleY", true )
	end
end

function updateAnimation(elapsedTime)
	if gCurAnimMode == ANIMODE_STARTUP then
		updateMenuElements(elapsedTime)
	end
end

function updateMenuElements(elapsedTime)
	local allFinished = true
	for i, v in ipairs(gAnimatedElements) do
		if (v.time - v.startDelay) < v.duration then
			allFinished = false
			v.time = v.time + elapsedTime
			local val =  (v.time - v.startDelay) / v.duration
			if val < 0 then val = 0 end
			if val > 1 then val = 1 end
			local animPoint = MathFX.easeOut(0, 1, val)
			v.size = v.sizeStart + ((v.sizeEnd - v.sizeStart) * animPoint)
			Control_SetSize(v.control, v.size, Control_GetHeight(v.control))
		end
	end
	if allFinished then
		gCurAnimMode = ANIMODE_IDLE
		showControlList({"bntVPBodyType", "btnVPSkin", "btnVPFace", "btnVPEyes",
			"btnVPEyebrow", "btnVPHair", "btnVPHairColor",
			"btnVPFaceCover", "btnVPBeardColor", "btnRandomize",
			"btnSave", "sldScaleXZ", "sldScaleY", "lblHeightCurrent", "btnCancel"}, true)
	end
end

function updateHeightLabel()
	local startHeight = 0
	if isFemale() then
		startHeight = HEIGHT_BASE_FEMALE
	else
		startHeight = HEIGHT_BASE_MALE
	end
	local height = ((gCurrentScaleY - 100) / 100) * startHeight + startHeight
	local feet = math.floor(height / 12)
	local inches = math.floor(height - (feet * 12) + 0.5)
	local sh = Dialog_GetStatic(gDialogHandle, "lblHeightCurrent")
	if sh ~= nil then
		Static_SetText(sh, feet .. "\' " .. inches .. "\"")
	end
end

-- Viewport functions --
------------------------

function initViewPorts()
	if gDialogHandle ~= nil then
		local max, rem = getViewportOptionLimits()
		local rowHeight = 0
		for i = 0, NUM_VIEWPORTS - 1 do
			local button = Dialog_GetControl(gDialogHandle, "btnViewport"..tostring(i))
			local dialogX = Dialog_GetLocationX(gDialogHandle)
			local dialogY = Dialog_GetLocationY(gDialogHandle)
			if button ~= nil then
				local GUTTER = 5
				local buttonBounds = {
					x = Control_GetLocationX(button),
					y = Control_GetLocationY(button),
					w = Control_GetWidth(button),
					h = Control_GetHeight(button) }
				local viewport = {}
				local genderIndex = BODY_MALE_MED

				-- Initialize first 3 windows to different body types since you can't see body in other views
				if i < 3 then
					genderIndex = BODY_MALE_THIN + i
					if isFemale() then
						genderIndex = BODY_FEM_THIN + i
					end
				else
					if isFemale() then
						genderIndex = BODY_FEM_MED
					end
				end

				viewport['actor'] = KEP_AddMenuGameObject(ACTOR_ID[genderIndex], IDLE_ANIM_ID)
				viewport['x'] = dialogX + buttonBounds.x + GUTTER
				viewport['y'] = dialogY + buttonBounds.y + GUTTER
				-- Right here is where images get put in the viewports
				GetSet_SetVector(viewport.actor, MenuGameIds.POSITION, gCurVPpos.x, gCurVPpos.y, gCurVPpos.z)
				GetSet_SetVector(viewport.actor, MenuGameIds.SCALING, gCurVPscale.x, gCurVPscale.y, gCurVPscale.z)
				GetSet_SetVector(viewport.actor, MenuGameIds.ROTATION, gCurVProt.x, gCurVProt.y, gCurVProt.z)
				GetSet_SetVector(viewport.actor, MenuGameIds.VP_POSITION, viewport.x, viewport.y, 0)
				GetSet_SetVector(viewport.actor, MenuGameIds.VP_DIMENSIONS, buttonBounds.w - (GUTTER * 2), buttonBounds.h - (GUTTER * 2), 0)
			-- 	-- init here so not updated with every time body is changed.
				KEP_SetCharacterAltConfig(viewport.actor, CONFIG_SHIRT[genderIndex], 1, 0)
				KEP_SetCharacterAltConfig(viewport.actor, CONFIG_PANTS[genderIndex], 1, 0)
				KEP_SetCharacterAltConfig(viewport.actor, CONFIG_BOOTS[genderIndex], 1, 0)
				gViewport[i + 1] = viewport
				rowHeight = buttonBounds.h
			end
		end
	end

	gViewportDirtyFlag = FLAGS_ALL_SET
end

function setupMenuViewports()
	if gDialogHandle ~= nil then
		local max, rem = getViewportOptionLimits()
		local rowHeight = 0
		for i = 0, NUM_VIEWPORTS - 1 do
			local button = Dialog_GetControl(gDialogHandle, "btnViewport"..tostring(i))
			local dialogX = Dialog_GetLocationX(gDialogHandle)
			local dialogY = Dialog_GetLocationY(gDialogHandle)
			if button ~= nil then
				if i + gViewportFirstIndex <= max then
					Control_SetEnabled(button, true)
				else
					Control_SetEnabled(button, false)
				end
			end
			-- Attach click handlers
			_G["btnViewport"..tostring(i).."_OnButtonClicked"] = function(buttonHandle)
			   viewPortOnButtonClicked(i)
			end
		end
	end
end

function getViewportOptionLimits()
	local max, rem = NUM_VIEWPORTS, 0
	if gCurViewportMode	== VPMODE_HAIR then
		if isFemale() then
			max = #HAIR_COLORS_FEM
		else
			max = #HAIR_COLORS_MALE
		end
	elseif gCurViewportMode	== VPMODE_HAIRCOLOR then
		-- Lua indexing from 1, so increment to get the right number of hair colors from the table
		if isFemale() then
			max = HAIR_COLORS_FEM[gCurHairIndex + 1] + 1
		else
			max = HAIR_COLORS_MALE[gCurHairIndex + 1] + 1
		end
	elseif gCurViewportMode	== VPMODE_EYECOLOR then
		max = MAX_EYE_COLOR + 1
	elseif gCurViewportMode	== VPMODE_SKINCOLOR then
		max = MAX_SKIN_COLOR + 1
	elseif gCurViewportMode	== VPMODE_FACE then
		if isFemale() then
			max = #FACES_FEMALE
		else
			max = #FACES_MALE
		end
	elseif gCurViewportMode	== VPMODE_FACECOVER then
		if isFemale() then
			max = MAX_MAKEUP + 1
		else
			max = MAX_BEARD + 1
		end
	elseif gCurViewportMode	== VPMODE_BEARDCOLOR then
		if isFemale() then
			max = MAX_BROW_COLOR + 1
		else
			max = MAX_BEARD_COLOR + 1
		end
	elseif gCurViewportMode	== VPMODE_EYEBROW then
		if isFemale() then
			max = MAX_BROW_FEM + 1
		else
			max = MAX_BROW_MALE + 1
		end
	elseif gCurViewportMode	== VPMODE_BODYTYPE then
		max = 3
	else
		gViewportFirstIndex = 1
	end
	rem = math.fmod(max, NUM_VIEWPORTS)
	return max, rem
end

function setViewportDirty(category)
	if gCurViewportMode == VPMODE_BODYTYPE then
		-- This does not need to be done since we never change menu viewport actor body types
		-- gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.BODY_TYPE)
		return
	end

	if gCurViewportMode == VPMODE_SKINCOLOR then
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.FACE) -- What is this even doing?
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.BODY)
		return
	end

	if gCurViewportMode == VPMODE_FACE then
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.FACE)
	end

	if gCurViewportMode == VPMODE_FACECOVER	then
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.FACECOVER)
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.BROW)
		return
	end

	if gCurViewportMode == VPMODE_HAIR or gCurViewportMode == VPMODE_HAIRCOLOR then
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.HAIR)
		return
	end

	if gCurViewportMode == VPMODE_EYECOLOR	then
		--update hair to make sure it doesnt cover eye color
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.HAIR)
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.EYECOLOR)
		return
	end

	if gCurViewportMode == VPMODE_EYEBROW or gCurViewportMode == VPMODE_BEARDCOLOR then
		--update hair to make sure it doesnt cover makeup or eyebrow
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.HAIR)
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.BROW)
		gViewportDirtyFlag = BitFlag.set(gViewportDirtyFlag, FLAGS.FACECOVER)
		return
	end
end

function updateViewportBodyParts()
	if gViewport ~= nil and gViewportDirtyFlag ~= 0 then
		for index, viewport in pairs(gViewport) do
			local vpa = viewport.actor
			if vpa ~= nil then
				if BitFlag.test(gViewportDirtyFlag, FLAGS.FACE) then
					configViewportFace(vpa, index)
				end
				if BitFlag.test(gViewportDirtyFlag, FLAGS.BODY) then
					configViewportBody(vpa, index)
				end
				if BitFlag.test(gViewportDirtyFlag, FLAGS.EYECOLOR) then
					configViewportEyeColor(vpa, index)
				end
				if BitFlag.test(gViewportDirtyFlag, FLAGS.BROW) then
					configViewportEyebrow(vpa, index)
				end
				if BitFlag.test(gViewportDirtyFlag, FLAGS.FACECOVER) then
					configViewportBeardAndMakeupColors(vpa, index)
				end
				if BitFlag.test(gViewportDirtyFlag, FLAGS.HAIR) then
					configViewportHair(vpa, index)
				end
			end
		end
		gViewportDirtyFlag = 0
	end
end

-- Because of lua indexing from 1 and c++ indexing from 0
-- 2 must be subracted from the addition of two 1 based indexes
function configViewportFace(vpa, index)
	local faceGLID = getFaceByIndex(index)
	if faceGLID~=nil then
		local skinColor = gCurSkinColor
		if gCurViewportMode	== VPMODE_SKINCOLOR then
			skinColor = index + gViewportFirstIndex - 2
		end
		KEP_ArmMenuGameObject(vpa, faceGLID, 0)
		KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, CONFIG_HEAD[gGenderIndex], 0, skinColor)
		KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, FACE_EYE_LEFT, 0, gCurEyeColor)
		KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, FACE_EYE_RIGHT, 0, gCurEyeColor)
		KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, FACE_EYEBROW, gCurEyebrow, gCurBeardColor)
		if isFemale() then
			KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, FACE_FACECOVER, 0, gCurFaceCover)
		else
			KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, FACE_FACECOVER, gCurFaceCover, gCurBeardColor)
		end
	end
end

function configViewportBody(vpa, index)
	local skinColor = gCurSkinColor
	if gCurViewportMode	== VPMODE_SKINCOLOR then
		skinColor = index + gViewportFirstIndex - 2
	end
	KEP_SetCharacterAltConfig(vpa, CONFIG_NECK[gGenderIndex], 0, skinColor)
	if isFemale() then
		KEP_SetCharacterAltConfig(vpa, CONFIG_ARMS[gGenderIndex], 0, skinColor)
	else
		KEP_SetCharacterAltConfig(vpa, CONFIG_ARMS[gGenderIndex], 1, skinColor)
	end
	if gCurViewportMode	== VPMODE_SKINCOLOR then
		-- you only see face and neck in window so dont bother changing the rest.
		return
	end
	KEP_SetCharacterAltConfig(vpa, CONFIG_TORSO[gGenderIndex], 1, skinColor)
	KEP_SetCharacterAltConfig(vpa, CONFIG_HANDS[gGenderIndex], 0, skinColor)
	KEP_SetCharacterAltConfig(vpa, CONFIG_CROTCH[gGenderIndex], 1, skinColor)
end

function configViewportEyeColor(vpa, index)
	local faceGLID = getFaceByIndex(index)
	local eyeColor = gCurEyeColor
	if gCurViewportMode	== VPMODE_EYECOLOR then
		eyeColor = index + gViewportFirstIndex - 2
	end
	KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, FACE_EYE_LEFT, 0, eyeColor)
	KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, FACE_EYE_RIGHT, 0, eyeColor)
end

function configViewportEyebrow(vpa, index)
	local faceGLID = getFaceByIndex(index)
	local eyebrow, beardColor = gCurEyebrow, gCurBeardColor
	if gCurViewportMode	== VPMODE_EYEBROW then
		eyebrow = index + gViewportFirstIndex - 2
	elseif gCurViewportMode == VPMODE_BEARDCOLOR then
		beardColor = index + gViewportFirstIndex - 2
	end
	KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, FACE_EYEBROW, eyebrow, beardColor)
end

function configViewportBeardAndMakeupColors(vpa, index)
	local faceGLID = getFaceByIndex(index)
	local faceCover, beardColor = gCurFaceCover, gCurBeardColor
	if gCurViewportMode	== VPMODE_FACECOVER then
		faceCover = index + gViewportFirstIndex - 2
	elseif gCurViewportMode == VPMODE_BEARDCOLOR then
		beardColor = index + gViewportFirstIndex - 2
	end
	if isFemale() then
		KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, FACE_FACECOVER, 0, faceCover)
	else
		KEP_MenuGameObjectSetEquippableConfig(vpa, faceGLID, FACE_FACECOVER, faceCover, beardColor)
	end
end

function configViewportHair(vpa, index)
	local hairIndex, hairColor = gCurHairIndex, gCurHairColor
	if  gCurViewportMode	== VPMODE_EYECOLOR
	or  gCurViewportMode 	== VPMODE_EYEBROW
	or (gCurViewportMode 	== VPMODE_BEARDCOLOR and isFemale()) then
		if isFemale() then
			hairIndex = HAIR_FOR_EYES_FEM
		else
			hairIndex = HAIR_FOR_EYES_MALE
		end
	elseif gCurViewportMode	== VPMODE_HAIR then
		hairIndex = index + gViewportFirstIndex - 2
	elseif gCurViewportMode	== VPMODE_HAIRCOLOR then
		hairColor = index + gViewportFirstIndex - 2
	end
	-- Do this because not all hair styles (meshes) have the same amount of colors (materials)

	local colorIndex = 0
	if isFemale() then
		colorIndex = HAIR_COLORS_FEM[ gCurHairIndex + 1 ]
	else 
		colorIndex = HAIR_COLORS_MALE[ gCurHairIndex + 1 ]
	end

	if not colorIndex or hairColor > colorIndex then
		hairColor = 0
	end

	if not isFemale() then
		KEP_SetCharacterAltConfig(vpa, 25, hairIndex, hairColor)
		KEP_SetCharacterAltConfig(vpa, 26, hairIndex, hairColor)
	end
	KEP_SetCharacterAltConfig(vpa, 27, hairIndex, hairColor)
	KEP_SetCharacterAltConfig(vpa, 28, hairIndex, hairColor)
	KEP_SetCharacterAltConfig(vpa, 29, hairIndex, hairColor)
	KEP_SetCharacterAltConfig(vpa, 30, hairIndex, hairColor)
	KEP_SetCharacterAltConfig(vpa, 31, hairIndex, hairColor)
end

function transformVPActor(posX, posY, posZ, scaleX, scaleY, scaleZ, rotX, rotY, rotZ)
	if gViewport ~= nil then
		gCurVPpos 	= {x = posX, y = posY, z = posZ}
		gCurVPscale	= {x = scaleX, y = scaleY,	z = scaleZ}
		gCurVProt	= {x = rotX, y = rotY, z = rotZ}
		for index, viewport in pairs(gViewport) do
			local vpa = viewport.actor
			if vpa ~= nil and vpa ~= 0 then
				--This is a huge hack for meaty not being the same height as the other male actors. Lame.
				local mod = 0
				if (index == 3) and (not isFemale()) then -- Where does index == 3 come from?
					mod = gBeefyMod
				end
				GetSet_SetVector(vpa, MenuGameIds.POSITION, posX, posY + mod, posZ)
				GetSet_SetVector(vpa, MenuGameIds.SCALING, scaleX, scaleY, scaleZ)
				GetSet_SetVector(vpa, MenuGameIds.ROTATION, rotX, rotY, rotZ)
			end
		end
	end
end

function moveViewPorts()
	local max, rem = getViewportOptionLimits()
	local rowHeight = 0
	for i = 1, NUM_VIEWPORTS do
		local button = Dialog_GetControl(gDialogHandle, "btnViewport" .. tostring(i - 1))
		local dialogX = Dialog_GetLocationX(gDialogHandle)
		local dialogY = Dialog_GetLocationY(gDialogHandle)
		if button ~= nil then
			if gViewport[i].actor then
				local GUTTER = 5
				local buttonBounds = {
					x = Control_GetLocationX(button),
					y = Control_GetLocationY(button),
					w = Control_GetWidth(button),
					h = Control_GetHeight(button) }

				gViewport[i]['x'] = dialogX + buttonBounds.x + GUTTER
				gViewport[i]['y'] = dialogY + buttonBounds.y + GUTTER
				GetSet_SetVector(gViewport[i].actor, MenuGameIds.VP_POSITION, gViewport[i]['x'], gViewport[i]['y'], 0)
				rowHeight = buttonBounds.h
			end
		end
	end
end

function resetViewportOptions()
	local max, rem = getViewportOptionLimits()
	showControl("btnVPBack", false)
	showControl("btnVPForward", max > NUM_VIEWPORTS)
	if gCurViewportMode	~= VPMODE_BODYTYPE then
		showControl("lblWeight", false)
		showControl("lblHeight", false)
		showControl("sldScaleXZ", false)
		showControl("sldScaleY", false)
		showControl("lblHeightCurrent", false)
	end
	gViewportFirstIndex = 1
end

---------------------
-- BUTTON HANDLERS --
---------------------

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	if key == Keyboard.ESC then
		-- Close this menu
		-- CALL disableViewportUpdate FIRST or cleanupMenuViewports will destroy the viewport objects Dialog_OnUpdate is trying to manipulate
	cancelUpdate()

	end
end

function btnCancel_OnButtonClicked( buttonHandle )
	-- Cancel Avatar Changes
	cancelUpdate()
end

-- TO DO: Come back to this
function btnSave_OnButtonClicked( buttonHandle )
	if g_menuVersion == MENU_VERSIONS.PLAYER then
		-- Save Avatar Changes
		disableViewportUpdate()
		validateCharacter()

		-- Clean Things Up Ready For Rezone
		cleanupMenuViewports()
		gGracefulCleanup = true
		g_DontZoom = false

		-- Rezone To Refresh Avatar, Zone & Menus
		Rezone()
	else
		-- TO DO: Add a function for saving NPC customization data
		saveNPCData()
		g_saveNPC = true
		cancelUpdate()
	end
end

function btnClothing_OnButtonClicked( buttonHandle)

	setClothingCamera()
	MenuOpen("UnifiedNavigation.xml")	
	MenuOpenModal("Framework_NPCInventory.xml")			
	
	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "npc")
	KEP_EventQueue(ev)
	
	local npcEvent = KEP_EventCreate("FRAMEWORK_OPEN_NPC_INVENTORY")
	KEP_EventEncodeString(npcEvent, Events.encode(g_npcCustoms["GLIDS"]))-- Equipped Glids
	KEP_EventEncodeNumber(npcEvent, g_netId)	--Net ID
	KEP_EventEncodeNumber(npcEvent, g_npcGender)	--Gender: 1 = female, 0 = male
	KEP_EventQueue(npcEvent)

	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local newX = screenWidth/2 - INVENTORY_OFFSET
	local screenHight = Dialog_GetScreenHeight(gDialogHandle)
	local menuSize = MenuGetSize("Framework_NPCInventory")
	local newY = screenHight/2 - menuSize.height/2
	MenuSetLocation("UnifiedInventory.xml",	newX, newY)

	disableViewportUpdate()
	cleanupMenuViewports()
end
-- NOTE: This button gets "pressed" as part of initialization
-- NOTE 2: Does not work for NPCs - will need to respawn the NPC with the same parameters
function bntVPBodyType_OnButtonClicked(buttonHandle)
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty(gPreviousNoSelDirty)
	end
	gCurViewportMode = VPMODE_BODYTYPE
	gBeefyMod = .25
	Static_SetText(gLabelMode, "Body Size")
	transformVPActor(0, -5, 0, 3, 3, 3, 0, 20, 0) -- Where did these numbers come from?
	resetViewportOptions()
	setupMenuViewports()
	showControl("lblWeight", true)
	showControl("lblHeight", true)
	showControl("sldScaleXZ", true)
	showControl("sldScaleY", true)
	showControl("lblHeightCurrent", true)
	setViewportDirty(gCurViewportMode)
	updateAppearance()
	zoomBody()
	gPreviousNoSelDirty = gCurViewportMode
end

-- NOTE: This button gets "pressed" as part of initialization
function sldScaleY_OnSliderValueChanged(sh)
	local sliderXZ = Dialog_GetSlider(gDialogHandle, "sldScaleXZ")
	local scaleY = Slider_GetValue(sh)
	local scaleXZ = Slider_GetValue(sliderXZ)
	if not gCancelUpdate then
		local diff = math.abs(scaleY - scaleXZ)
		if diff > SCALE_TOLERANCE then
			if scaleY > scaleXZ then
				scaleXZ = scaleY - SCALE_TOLERANCE
			else
				scaleXZ = scaleY + SCALE_TOLERANCE
			end
			Slider_SetValue(sliderXZ, scaleXZ)
		end
		gCurrentScaleXZ = scaleXZ
		gCurrentScaleY = scaleY
	end
	local player = g_player
	if g_menuVersion == MENU_VERSIONS.PLAYER then
	 	player = KEP_GetPlayer() -- Need to re-grab player object in case his body changed
	end
	KEP_ScaleRuntimePlayer(player, (gCurrentScaleXZ / 100), (gCurrentScaleY / 100), (gCurrentScaleXZ / 100))
	updateHeightLabel()
end

-- NOTE: This button gets "pressed" as part of initialization
function sldScaleXZ_OnSliderValueChanged(sh)
	local sliderY = Dialog_GetSlider(gDialogHandle, "sldScaleY")
	local scaleY = Slider_GetValue(sliderY)
	local scaleXZ = Slider_GetValue(sh)
	if not gCancelUpdate then
		local diff = math.abs(scaleY - scaleXZ)
		if diff > SCALE_TOLERANCE then
			if scaleY < scaleXZ then
				scaleY = scaleXZ - SCALE_TOLERANCE
			else
				scaleY = scaleXZ + SCALE_TOLERANCE
			end
			Slider_SetValue(sliderY, scaleY)
		end
		gCurrentScaleXZ = scaleXZ
		gCurrentScaleY = scaleY
	end
	local player = g_player
	if g_menuVersion == MENU_VERSIONS.PLAYER then
	 	player = KEP_GetPlayer() -- Need to re-grab player object in case his body changed
	end
	KEP_ScaleRuntimePlayer(player, (gCurrentScaleXZ / 100), (gCurrentScaleY / 100), (gCurrentScaleXZ / 100))
	updateHeightLabel()
end

function viewPortOnButtonClicked(index)
	viewportButtonHandler(gViewportFirstIndex + index)
	updateAppearance()
	gPreviousNoSelDirty = 0
	setViewportDirty(gCurViewportMode)
end

function viewportButtonHandler(index)
	if gCurViewportMode	== VPMODE_HAIR then
		gCurHairIndex = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.HAIR )
	elseif gCurViewportMode	== VPMODE_HAIRCOLOR then
		gCurHairColor = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.HAIR )
	elseif gCurViewportMode	== VPMODE_EYECOLOR then
		gCurEyeColor = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.EYECOLOR )
	elseif gCurViewportMode	== VPMODE_SKINCOLOR then
		gCurSkinColor = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACE )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.BODY )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACECOVER )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.BROW )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.EYECOLOR )
	elseif gCurViewportMode	== VPMODE_FACE then
		gFaceIndex = index
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACE )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACECOVER )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.BROW )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.EYECOLOR )
	elseif gCurViewportMode	== VPMODE_FACECOVER then
		gCurFaceCover = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACECOVER )
	elseif gCurViewportMode	== VPMODE_BEARDCOLOR then
		gCurBeardColor = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACECOVER )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.BROW )
	elseif gCurViewportMode	== VPMODE_EYEBROW then
		gCurEyebrow = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.BROW )
	elseif gCurViewportMode	== VPMODE_BODYTYPE then
		if g_menuVersion == MENU_VERSIONS.NPC then
			saveNPCData()
			if g_npcGender == FEMALE then
				g_npcCustoms["dbIndex"] = BODY_FEM_THIN + index - 1 + DB_INDEX_OFFSET
			else
				g_npcCustoms["dbIndex"] = BODY_MALE_THIN + index - 1 + DB_INDEX_OFFSET
			end
			KEP_npcDeSpawn({netId = g_netId})
			spawnNPC()
		else
			if isFemale() then
				gGenderIndex = BODY_FEM_THIN + index - 1
			else
				gGenderIndex = BODY_MALE_THIN + index - 1
			end
		end
		gDirtyFlag = FLAGS_ALL_SET
	end
end

function btnVPForward_OnButtonClicked(buttonHandle)
	local max, rem = getViewportOptionLimits()
	local showFwd = true
	if (gViewportFirstIndex + NUM_VIEWPORTS) - max <= 0 then
		gViewportFirstIndex = gViewportFirstIndex + NUM_VIEWPORTS
	end
	showControl("btnVPBack", true)
	showControl("btnVPForward", (gViewportFirstIndex + NUM_VIEWPORTS) - max <= 0)
	setupMenuViewports()
	setViewportDirty(gCurViewportMode)
	updateAppearance()
end

function btnVPBack_OnButtonClicked(buttonHandle)
	local max, rem = getViewportOptionLimits()
	gViewportFirstIndex = gViewportFirstIndex - NUM_VIEWPORTS
	if gViewportFirstIndex < 1 then
		gViewportFirstIndex = 1
	end
	showControl("btnVPBack", gViewportFirstIndex ~= 1)
	showControl("btnVPForward", (gViewportFirstIndex + NUM_VIEWPORTS) - max <= 0)
	setupMenuViewports()
	setViewportDirty(gCurViewportMode)
	updateAppearance()
end

function btnVPSkin_OnButtonClicked(buttonHandle)
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty(gPreviousNoSelDirty)
	end
	gCurViewportMode = VPMODE_SKINCOLOR
	gBeefyMod = .35
	Static_SetText(gLabelMode, "Skin Tone")
	if isFemale() then
		transformVPActor(0, -26.5, 0, 10.5, 10.5, 10.5, 0, 15, 0) -- Where do these numbers come from???
	else
		transformVPActor(0, -22.0, 0, 8.5, 8.5, 8.5, 0, 10, 0) -- Where do these numbers come from???
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty(gCurViewportMode)
	updateAppearance()
	zoomBody()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPHair_OnButtonClicked(buttonHandle)
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty(gPreviousNoSelDirty)
	end
	gCurViewportMode = VPMODE_HAIR
	gBeefyMod = .25
	Static_SetText(gLabelMode, "Hair Style")
	if isFemale() then
		transformVPActor(-0.5, -16.5, 0, 6.5, 6.5, 6.5, 0, 10, 0) -- Where do these numbers come from???
	else
		transformVPActor(0, -13.5, 0, 5, 5, 5, 0, 10, 0) -- Where do these numbers come from???
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty(gCurViewportMode)
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPHairColor_OnButtonClicked(buttonHandle)
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty(gPreviousNoSelDirty)
	end
	gCurViewportMode = VPMODE_HAIRCOLOR
	gBeefyMod = .25
	Static_SetText(gLabelMode, "Hair Color")
	if isFemale() then
		transformVPActor(-0.5, -16.5, 0, 6.5, 6.5, 6.5, 0, 10, 0) -- Where do these numbers come from???
	else
		transformVPActor(0, -13.5, 0, 5, 5, 5, 0, 10, 0) -- Where do these numbers come from???
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty(gCurViewportMode)
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPEyes_OnButtonClicked(buttonHandle)
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty(gPreviousNoSelDirty)
	end
	gCurViewportMode = VPMODE_EYECOLOR

	gBeefyMod = .75
	Static_SetText(gLabelMode, "Eye Color")
	if isFemale() then
		transformVPActor(-3.75, -77, 0, 29, 29, 29, 0, 4, 0) -- Where do these numbers come from???
	else
		transformVPActor(-1.5, -52, 0, 19, 19, 19, 0, 2, 0) -- Where do these numbers come from???
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty(gCurViewportMode)
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPFace_OnButtonClicked(buttonHandle)
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty(gPreviousNoSelDirty)
	end
	gCurViewportMode = VPMODE_FACE
	gBeefyMod = .25
	Static_SetText(gLabelMode, "Face Style")
	if isFemale() then
		transformVPActor(0, -26.5, 0, 10.5, 10.5, 10.5, 0, 15, 0) -- Where do these numbers come from???
	else
		transformVPActor(0, -22.0, 0, 8.5, 8.5, 8.5, 0, 10, 0) -- Where do these numbers come from???
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty(gCurViewportMode)
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPFaceCover_OnButtonClicked(buttonHandle)
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty( gPreviousNoSelDirty )
	end
	gCurViewportMode = VPMODE_FACECOVER
	gBeefyMod = .25
	if isFemale() then
		Static_SetText(gLabelMode, "Makeup Style")
		transformVPActor(0, -25.0, 0, 10, 10, 10, 0, 10, 0) -- Where do these numbers come from???
	else
		Static_SetText( gLabelMode, "Beard Style" )
		transformVPActor(0, -21.5, 0, 9, 9, 9, 0, 20, 0) -- Where do these numbers come from???
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty(gCurViewportMode)
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPBeardColor_OnButtonClicked(buttonHandle)
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty(gPreviousNoSelDirty)
	end
	gCurViewportMode = VPMODE_BEARDCOLOR
	gBeefyMod = .35
	if isFemale() then
		Static_SetText(gLabelMode, "Eyebrow Color")
		transformVPActor(-3.75, -77, 0, 29, 29, 29, 0, 4, 0) -- Where do these numbers come from???
	else
		Static_SetText(gLabelMode, "Beard and Eyebrow Color")
		transformVPActor(0, -21, 0, 8, 8, 8, 0, 20, 0) -- Where do these numbers come from???
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty(gCurViewportMode)
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPEyebrow_OnButtonClicked(buttonHandle)
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty(gPreviousNoSelDirty)
	end
	gCurViewportMode = VPMODE_EYEBROW
	gBeefyMod = .75
	Static_SetText(gLabelMode, "Eyebrow Style")
	if isFemale() then
		transformVPActor(-3.75, -77, 0, 29, 29, 29, 0, 4, 0) -- Where do these numbers come from???
	else
		transformVPActor(-1.5, -53, 0, 19, 19, 19, 0, 2, 0) -- Where do these numbers come from???
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty(gCurViewportMode)
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnRandomize_OnButtonClicked(buttonHandle)

	if isFemale() then
		gGenderIndex 	= math.random(BODY_FEM_THIN, BODY_FEM_BIG)
		gFaceIndex 		= math.random(#FACES_FEMALE)
		gCurEyebrow 	= math.random(0, MAX_BROW_FEM)
		gCurFaceCover 	= math.random(0, MAX_MAKEUP)
		gCurHairIndex 	= math.random(#HAIR_COLORS_FEM)
		gCurHairColor 	= math.random(0, HAIR_COLORS_FEM[gCurHairIndex])
		gCurBeardColor	= math.random(0, MAX_BROW_COLOR)
	else
		gGenderIndex 	= math.random(BODY_MALE_THIN, BODY_MALE_BIG)
		gFaceIndex 		= math.random(#FACES_MALE)
		gCurEyebrow		= math.random(0, MAX_BROW_MALE)
		gCurFaceCover 	= math.random(0, MAX_BEARD)
		gCurHairIndex 	= math.random(#HAIR_COLORS_MALE)
		gCurHairColor 	= math.random(0, HAIR_COLORS_MALE[gCurHairIndex])
		gCurBeardColor	= math.random(0, MAX_BEARD_COLOR)
	end
	gCurSkinColor		= math.random(0, MAX_SKIN_COLOR)
	gCurEyeColor		= math.random(0, MAX_EYE_COLOR)
	
	Log("RandomizeClicked: hairIndex="..gCurHairIndex.." faceIndex="..gFaceIndex)

	setupActor()
	resetViewportOptions()
	setupMenuViewports()
	gViewportDirtyFlag = FLAGS_ALL_SET
	updateAppearance()
end

----------------------
-- HELPER FUNCTIONS --
----------------------

findIndexByValue = function(aTable, value)
	for i=1, #aTable do
		if aTable[i] == value then
			return i
		end
	end
end

isFemale = function()
	if gGenderIndex == BODY_FEM_THIN or gGenderIndex == BODY_FEM_MED or gGenderIndex == BODY_FEM_BIG then
		return true
	end
	return false
end

getCurFace = function()
	local player = g_player
	if g_menuVersion == MENU_VERSIONS.PLAYER then
	 	player = KEP_GetPlayer() -- Need to re-grab player object in case his body changed
	end
	local facemax = 0
	local oldface = gFaceIndex
	if isFemale() then
		facemax = #FACES_FEMALE
	else
		facemax = #FACES_MALE
	end
	local t = {KEP_GetCharacterAttachmentTypes(player)}
	for i = 1, #t do
		for j = 1, facemax do
			gFaceIndex = j
			if tonumber(t[i]) == getFaceAssByIndex() then
				gFaceIndex = oldface
				return j
			end
		end
	end
	gFaceIndex = oldface
	return 1
end

getFaceAssByIndex = function(index)
	local faceIndex = gFaceIndex
	if isFemale() then
		return FACES_FEMALE[faceIndex]
	else
		return FACES_MALE[faceIndex]
	end
end

showControl = function(ctlName, enabled, visible)
	local c = Dialog_GetControl(gDialogHandle, ctlName)
	if visible == nil then
		Control_SetVisible(c, enabled)
	else
		Control_SetVisible(c, visible)
	end
	Control_SetEnabled(c, enabled)
end

showControlList = function(list, enabled, visible)
	for i, v in ipairs(list) do
		showControl(v, enabled, visible)
	end
end

disableViewportUpdate = function()
	gDoViewportUpdate = false
end

enableViewportUpdate = function()
	gDoViewportUpdate = true
end

cleanupMenuViewports = function()
	if gViewport ~= nil and gViewport ~= 0 then
		for index, viewport in pairs(gViewport) do
			local vpa = viewport.actor
			if vpa ~= nil and vpa ~= 0 then
				if (KEP_ObjectIsValid(vpa) == 1) then
					KEP_RemoveMenuGameObject(vpa)
				end
			end
		end
	end

	if gCharBaseConfig ~= nil and gCharBaseConfig ~= 0 then
		if (KEP_ObjectIsValid(gCharBaseConfig) == 1) then
			KEP_RemoveMenuGameObject(gCharBaseConfig)
		end
	end
	gViewportsClean = true
end

setupActor = function()
	gDirtyFlag = FLAGS_ALL_SET
end

updateAppearance = function()
	updateBodyParts()
	updateViewportBodyParts()
end

getFaceByIndex = function(index)
	local faceIndex = gFaceIndex
	if index ~= nil and gCurViewportMode == VPMODE_FACE then
		faceIndex = index
	end
	if isFemale() then
		return FACES_FEMALE[faceIndex]
	else
		return FACES_MALE[faceIndex]
	end
end

--------------------------------------------------------- UNADAPTED





-----------------------------------------------





function resetCamera()
	zoomBody()
	--TODO: Get the players original camera configuration and resets
end

function zoomBody()
	KEP_SetCameraCollision(0)	-- call again to ensure no one overrides the flag after menu is opened
	local cam = KEP_GetPlayersActiveCamera(gGame)
	GetSet_SetNumber(cam, CameraIds.ORBITAZIMUTH, -3)
	GetSet_SetNumber(cam, CameraIds.ORBITELEVATION, .16)
	GetSet_SetNumber(cam, CameraIds.ORBITDIST, 12)
	GetSet_SetVector(cam, CameraIds.ORBITFOCUS, 0, 5, 0)
end

function zoomFace()
	KEP_SetCameraCollision(0)	-- call again to ensure no one overrides the flag after menu is opened
	local cam = KEP_GetPlayersActiveCamera(gGame)
	GetSet_SetNumber(cam, CameraIds.ORBITAZIMUTH, -3)
	GetSet_SetNumber(cam, CameraIds.ORBITELEVATION, .2)
	GetSet_SetNumber(cam, CameraIds.ORBITDIST, 1)
	AVATAR_HEIGHT_MAX = 110
	AVATAR_HEIGHT_MIN = 90
	CAMY_HEIGHT_MAX = 6.5 --this is the optimal camera height for max avatar height
	CAMY_HEIGHT_MIN = 5.5 --this is the optimal camera height for min avatar height
	--this sets the focus of the camera depending on teh characters height
	GetSet_SetVector(cam, CameraIds.ORBITFOCUS, 0, CAMY_HEIGHT_MAX - (AVATAR_HEIGHT_MAX - gCurrentScaleY)/(AVATAR_HEIGHT_MAX - AVATAR_HEIGHT_MIN) * (CAMY_HEIGHT_MAX - CAMY_HEIGHT_MIN), -3)
end

function setClothingCamera( )
	KEP_SetCameraCollision(0)	-- call again to ensure no one overrides the flag after menu is opened
	local cam = KEP_GetPlayersActiveCamera(gGame)
	GetSet_SetNumber(cam, CameraIds.ORBITAZIMUTH, -3)
	GetSet_SetNumber(cam, CameraIds.ORBITELEVATION, .16)
	GetSet_SetNumber(cam, CameraIds.ORBITDIST, 12)
	GetSet_SetVector(cam, CameraIds.ORBITFOCUS, -3, 3, 0)
end