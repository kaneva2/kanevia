--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\Scripts\\LWG.lua")

YES_NO_BOX_EVENT = "YesNoBoxEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"
VISUAL_NAMES_EVENT = "VisualNamesEvent"
VISUAL_RESPONSE_EVENT = "VisualResponseEvent"
EDIT_LIGHT_MENU_CREATION_EVENT = "EditLightMenuCreationEvent"
LIGHT_MODIFY_COMPLETE_EVENT = "ModifyLightsCompleteEvent"

COLLISION_RESPONSE = 1
CUSTOMIZABLE_RESPONSE = 2

g_ugcType = 0
g_ugcObjId = 0
g_min = {x = 0, y = 0, z = 0}
g_max = {x = 0, y = 0, z = 0}
g_targetType = ObjectType.UNKNOWN
g_targetId = -1
g_targetX = 0
g_targetY = 0
g_targetZ = 0
g_targetOverride = 0
g_originalWidth = 0
g_originalHeight = 0
g_originalDepth = 0
g_currentWidth = 0
g_currentHeight = 0
g_currentDepth = 0
g_scaleFactor = 0
g_correctAmbient = 0

g_dimensionMessage = nil
g_importBoundBoxGeomId = -1

g_t_visuals = {}
g_t_lights = {}
g_visual_awaiting = 0

SCALE_DEF = (100.0 / 100.0)
SCALE_MIN = (0.01 / 100.0)

SIZE_MIN = 0.0

function MeshImportOptionsMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	g_ugcType = KEP_EventDecodeNumber( event )
	g_ugcObjId = KEP_EventDecodeNumber( event )
	g_min.x = KEP_EventDecodeNumber( event )
	g_min.y = KEP_EventDecodeNumber( event )
	g_min.z = KEP_EventDecodeNumber( event )
	g_max.x = KEP_EventDecodeNumber( event )
	g_max.y = KEP_EventDecodeNumber( event )
	g_max.z = KEP_EventDecodeNumber( event )
	g_targetType = KEP_EventDecodeNumber( event )
	g_targetId = KEP_EventDecodeNumber( event )
	g_targetX = KEP_EventDecodeNumber( event )
	g_targetY = KEP_EventDecodeNumber( event )
	g_targetZ = KEP_EventDecodeNumber( event )
	numberOfVisuals = KEP_EventDecodeNumber(event)
	for i=1, numberOfVisuals do
		local visual = {}
		visual.name = KEP_EventDecodeString(event)
		visual.collide = 1 -- collide by default
		visual.customizable = 1 -- be custom by default
		visual.playFlash = 0 -- don't play flash by default
		g_t_visuals[i] = visual
	end
	populateList()

	g_originalWidth = math.abs(g_max.x - g_min.z)
	g_originalHeight = math.abs(g_max.y - g_min.y)
	g_originalDepth = math.abs(g_max.z - g_min.z)

	updateScale( SCALE_DEF, nil )

	g_dimensionMessage = getDimensionStr(g_originalWidth, g_originalHeight, g_originalDepth)
	Log( "MeshImportOptionsMenuEventHandler: targetType="..g_targetType.." targetId="..g_targetId.." [" .. g_dimensionMessage.."]" )
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "MeshImportOptionsMenuEventHandler", "MeshImportOptionsMenuEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "YesNoBoxHandler", YES_NO_ANSWER_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "visualsResponseHandler", VISUAL_RESPONSE_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "lightModificationsCompleteHandler", LIGHT_MODIFY_COMPLETE_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "MeshImportOptionsMenuEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "MeshImportOptionsMenuResultEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( VISUAL_NAMES_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( EDIT_LIGHT_MENU_CREATION_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( VISUAL_RESPONSE_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( LIGHT_MODIFY_COMPLETE_EVENT, KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	ch = Dialog_GetComboBox(dialogHandle, "cbPredefinedUnitConversion")

	-- ComboBox_AddItem only takes integer data, need to /1000 when used
	for idx, rec in ipairs(predefinedUnits) do
		ComboBox_AddItem(ch, rec.name, math.floor( rec.ratio*1000 ))	
	end

	-- Check "Auto-Correct Ambient" option by default
	ch = Dialog_GetCheckBox(dialogHandle, "cbAutoCorrectAmbient")
	CheckBox_SetChecked( ch, 0 )
	g_correctAmbient = 0
end

function Dialog_OnDestroy(dialogHandle)
	sendResultMessageAndCloseMenu()
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnConfirm_OnButtonClicked( buttonHandle )
	if g_scaleFactor<=SCALE_MIN then
		local editBoxHandle = Dialog_GetEditBox("edScaleFactor")
		local scaleFactorString = EditBox_GetText(editBoxHandle)
		LogError( "btnConfirm_OnButtonClicked: Invalid Scale" )
		KEP_MessageBox( "Please enter a valid scaling factor" )
	else 
		MenuCloseThis()
	end
end

function btnCancel_OnButtonClicked( buttonHandle )
	g_scaleFactor = -1
	MenuCloseThis()
end

function btnClose_OnButtonClicked( buttonHandle )
	g_scaleFactor = -1
	MenuCloseThis()
end

function btnReset_OnButtonClicked( buttonHandle )
	-- change selection to "Enter Custom Ratio"
	useCustomRatio()
	updateScale( SCALE_DEF, nil )
end

function cbPredefinedScale_OnComboBoxSelectionChanged(comboBoxHandle, sel_index, sel_text)
	local scaleInt = ComboBox_GetItemDataByIndex( comboBoxHandle, sel_index )
	scaleInt = tonumber(scaleInt)
	if scaleInt>SCALE_MIN then
		updateScale( scaleInt / 1000, nil )
	end
	usePredefinedScale()
end

function cbPredefinedUnitConversion_OnComboBoxSelectionChanged(comboBoxHandle, sel_index, sel_text)
	local scaleInt = ComboBox_GetItemDataByIndex( comboBoxHandle, sel_index )
	scaleInt = tonumber(scaleInt)
	if scaleInt>SCALE_MIN then
		updateScale( scaleInt / 1000, nil )
	end
end

function edSizeGet(ch, axis, updScale, updText)
	
	-- change selection to "Enter Custom Ratio"
	useCustomRatio()

	local editText = EditBox_GetText(ch)
	editText = string.gsub(editText, "^%s*(.-)%s*$", "%1")	-- trim white spaces

	local newSize = SIZE_MIN
	if editText~="." then	-- handle floating point
		newSize = tonumber(editText) or SIZE_MIN
		if (newSize == nil) or (newSize < SIZE_MIN) then
			LogError( "edSizeGet: Invalid Size" )
			newSize = SIZE_MIN
		end
	end

	-- Update Scale ?
	if updScale then
		local origSize = SIZE_MIN
		if axis=='x' then 
			origSize = g_originalWidth
		elseif axis == 'y' then 
			origSize = g_originalHeight
		elseif axis == 'z' then 
			origSize = g_originalDepth
		end

		local newScale = SCALE_DEF
		if newSize >= SIZE_MIN then
			newScale = newSize / origSize
		end

		if newScale < SCALE_MIN then
			newScale = SCALE_DEF
		end

		newSize = newScale * origSize

		updateScale( newScale, ch )
	end

	-- Update Editbox Text ?
	if updText then
		EditBox_SetText(ch, string.format("%.2f", newSize), false )
	end
end

function edSizeX_OnEditBoxString(ch) edSizeGet(ch, "x", true, true) MenuClearFocusThis() end
function edSizeX_OnEditBoxChange(ch) edSizeGet(ch, "x", true, false) end
function edSizeY_OnEditBoxString(ch) edSizeGet(ch, "y", true, true) MenuClearFocusThis() end
function edSizeY_OnEditBoxChange(ch) edSizeGet(ch, "y", true, false) end
function edSizeZ_OnEditBoxString(ch) edSizeGet(ch, "z", true, true) MenuClearFocusThis() end
function edSizeZ_OnEditBoxChange(ch) edSizeGet(ch, "z", true, false) end

function edScaleFactorGet(ch, updScale, updText)
	
	-- change selection to "Enter Custom Ratio"
	useCustomRatio()

	-- Get Edit Box Scale Factor
	local scaleFactorString = EditBox_GetText(ch)
	scaleFactorString = string.gsub(scaleFactorString, "^%s*(.-)%s*$", "%1")

	local newScale = SCALE_DEF
	if scaleFactorString ~= "." then	-- handle floating point
		local newScalePct = tonumber(scaleFactorString) or (100.0 * SCALE_DEF)
		newScale = newScalePct / 100.0;
	end

	if newScale < SCALE_MIN then
		newScale = SCALE_DEF
	end

	if updText then
		EditBox_SetText(ch, string.format("%.2f", 100.0 * newScale), false )
	end

	if updScale then
		updateScale( newScale, ch )
	end

	return newScale
end

function edScaleFactor_OnEditBoxString(ch) edScaleFactorGet(ch, true, true) MenuClearFocusThis() end
function edScaleFactor_OnEditBoxChange(ch) edScaleFactorGet(ch, true, false) end

function edScaleFactor_OnLButtonDblClk(ch) editBoxSelectAll(ch) end
function edSizeX_OnLButtonDblClk(ch) editBoxSelectAll(ch) end
function edSizeY_OnLButtonDblClk(ch) editBoxSelectAll(ch) end
function edSizeZ_OnLButtonDblClk(ch) editBoxSelectAll(ch) end

function editBoxSelectAll(ch) EditBox_SetText(ch, EditBox_GetText(ch), true ) end

function cbAutoCorrectAmbient_OnCheckBoxChanged(ch) g_correctAmbient = CheckBox_GetChecked(ch) end

function updateScale( scale, sourceEditControlHandle )

	if scale < SCALE_MIN or g_scaleFactor==scale then
		return
	end

	g_scaleFactor = scale
	local ed = Dialog_GetEditBox(gDialogHandle, "edScaleFactor")
	if ed~=sourceEditControlHandle then		-- do not update editbox control currently edited by user
		EditBox_SetText( ed, string.format("%.2f", g_scaleFactor*100), false )
	end

	g_currentWidth = g_originalWidth * g_scaleFactor
	g_currentHeight = g_originalHeight * g_scaleFactor
	g_currentDepth = g_originalDepth * g_scaleFactor

	ed = Dialog_GetEditBox(gDialogHandle, "edSizeX")
	if ed~=sourceEditControlHandle then		-- do not update editbox control currently edited by user
		EditBox_SetText( ed, string.format("%.2f", g_currentWidth), false )
	end
	ed = Dialog_GetEditBox(gDialogHandle, "edSizeY")
	if ed~=sourceEditControlHandle then		-- do not update editbox control currently edited by user
		EditBox_SetText( ed, string.format("%.2f", g_currentHeight), false )
	end
	ed = Dialog_GetEditBox(gDialogHandle, "edSizeZ")
	if ed~=sourceEditControlHandle then		-- do not update editbox control currently edited by user
		EditBox_SetText( ed, string.format("%.2f", g_currentDepth), false )
	end

	g_dimensionMessage = getDimensionStr( g_currentWidth, g_currentHeight, g_currentDepth )
	
    if (g_scaleFactor ~= SCALE_DEF) then
        Log( "updateScale: scale=" .. g_scaleFactor.."  [" .. g_dimensionMessage.."]" )
    end

	DrawImportBoundBox( )
end

function getDimensionStr( width, height, depth )

	local WStr, HStr, DStr
	WStr = string.format("%.2f", width)
	HStr = string.format("%.2f", height)
	DStr = string.format("%.2f", depth)

	local dimensionStr = "Width: " .. WStr .. "'   Height: " .. HStr .. "'   Depth: " .. DStr .. "'"
	return dimensionStr
end

function sendResultMessageAndCloseMenu()
	local event = KEP_EventCreate( "MeshImportOptionsMenuResultEvent" )
	KEP_EventEncodeNumber( event, g_ugcType )
	KEP_EventEncodeNumber( event, g_ugcObjId )
	KEP_EventEncodeNumber( event, g_scaleFactor ) 	-- a scaling factor of -1 means to cancel
	KEP_EventEncodeNumber( event, g_correctAmbient )
	KEP_EventEncodeNumber( event, g_targetOverride )
	if g_targetOverride~=0 then
		KEP_EventEncodeNumber( event, g_targetType )
		KEP_EventEncodeNumber( event, g_targetId )
		KEP_EventEncodeNumber( event, g_targetX )
		KEP_EventEncodeNumber( event, g_targetY )
		KEP_EventEncodeNumber( event, g_targetZ )

		-- Append visual collision settings
		KEP_EventEncodeNumber(event, #g_t_visuals)
		for i=1, #g_t_visuals do
			local visual = g_t_visuals[i]
			KEP_EventEncodeNumber(event, visual.collide)
			KEP_EventEncodeNumber(event, visual.playFlash)
			KEP_EventEncodeNumber(event, visual.customizable)
		end
		KEP_EventEncodeNumber(event, #g_t_lights)
		for i=1, #g_t_lights do
			local light = g_t_lights[i]
			KEP_EventEncodeNumber(event, light["type"])
			KEP_EventEncodeNumber(event, light["positionX"])
			KEP_EventEncodeNumber(event, light["positionY"])
			KEP_EventEncodeNumber(event, light["positionZ"])
			KEP_EventEncodeNumber(event, light["diffuseR"])
			KEP_EventEncodeNumber(event, light["diffureG"])
			KEP_EventEncodeNumber(event, light["diffuseB"])
			KEP_EventEncodeNumber(event, light["range"])
			KEP_EventEncodeNumber(event, light["attenuation1"])
			KEP_EventEncodeNumber(event, light["attenuation2"])
			KEP_EventEncodeNumber(event, light["attenuation3"])
		end
	end

	KEP_EventQueue( event )

	DisposeImportBoundBox( )
end

function YesNoBoxHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = KEP_EventDecodeNumber( event )	-- 1: Yes, 0 = No
	if filter == WF.CONFIRM_MESH_SCALING then
		if answer==1 then
			MenuCloseThis()
		end
		return KEP.EPR_CONSUMED
	end
end

-- change selection to "Enter Custom Ratio"
function useCustomRatio()
	local selStr, data = getComboBoxSelectedItem( "cbPredefinedUnitConversion" )
	if data~=nil and data~=0 then
		selectComboBoxItemByData( "cbPredefinedUnitConversion", 0 )
	end
end

function usePredefinedScale()
	local comboBoxHandle = Dialog_GetComboBox(gDialogHandle, "cbPredefinedUnitConversion")
	if comboBoxHandle~=0 then
		local selStr = ComboBox_GetSelectedText( comboBoxHandle )
		if selStr~=nil and selStr~=predefinedUnits[0].name then
			ComboBox_SetSelectedByIndex( comboBoxHandle, 0 )
		end
	end
end

function DrawImportBoundBox( )

	-- create indicator geom if not already
	if g_importBoundBoxGeomId==-1 then

		g_importBoundBoxGeomId = KEP_CreateStandardGeometry{type=LWG.GT_BOX, min=g_min, max=g_max}
		if g_importBoundBoxGeomId~=-1 then
            local WHITE = { r = 1.0, g = 1.0, b = 1.0, a = 1.0 }
            KEP_SetGeometryMaterial(g_importBoundBoxGeomId, {ambient = WHITE, diffuse = WHITE, specular = WHITE, emissive = WHITE, power = 0.4})
			if g_targetType==ObjectType.UNKNOWN then
				LogError("DrawImportBoundBox: ObjectType.UNKNOWN - Overriding With Player Position")
				local gsPlayer = KEP_GetPlayer( )
				g_targetOverride = 1
				g_targetType = ObjectType.PLAYER
				g_targetId = -1
				g_targetX, g_targetY, g_targetZ = GetSet_GetVector( gsPlayer, MovementIds.LASTPOSITION )
			end
            KEP_SetGeometryVisible(g_importBoundBoxGeomId, true)
			KEP_MoveGeometry(g_importBoundBoxGeomId, g_targetX, g_targetY, g_targetZ, false)
		else
			LogError("DrawImportBoundBox: FAILED KEP_CreateStandardGeometry()")
		end
	end

	-- scale boundbox
	if g_importBoundBoxGeomId~=-1 then
		KEP_ScaleGeometry(g_importBoundBoxGeomId, g_scaleFactor, g_scaleFactor, g_scaleFactor)
	end
end

function DisposeImportBoundBox( )
	if g_importBoundBoxGeomId~=-1 then
		KEP_DestroyGeometry(g_importBoundBoxGeomId)
		g_importBoundBoxGeomId = -1
	end
end

function populateList()
	local cbh = Dialog_GetComboBox(gDialogHandle, "cmbFlash")
	if cbh then
		ComboBox_AddItem(cbh, "None", 0)
		for i=1, #g_t_visuals do
			local visual = g_t_visuals[i]
			ComboBox_AddItem(cbh, visual.name, i)
		end
	end
end

function cmbFlash_OnComboBoxSelectionChanged(comboBoxHandle, sel_index, sel_text)
	for i=1, #g_t_visuals do
		local visual = g_t_visuals[i]
		visual.playFlash = 0
	end
	if sel_index > 0 then
		g_t_visuals[sel_index].playFlash = 1
		g_targetOverride = 1
	end
end

function btnPickColMeshes_OnButtonClicked( buttonHandle )
	MenuOpenModal("CollisionMeshPicker.xml")
	local event = KEP_EventCreate( VISUAL_NAMES_EVENT )
	KEP_EventEncodeString( event, "Select Meshes for Collision" )
	KEP_EventEncodeNumber( event, #g_t_visuals)
	for i=1, #g_t_visuals do
		local visual = g_t_visuals[i]
		KEP_EventEncodeString( event, visual.name)
		KEP_EventEncodeNumber( event, visual.collide)
	end
	KEP_EventQueue( event )
	g_visual_awaiting = COLLISION_RESPONSE
end

function btnModifyLights_OnButtonClicked( buttonHandle )
	MenuOpenModal("EditLights.xml")
	local event = KEP_EventCreate( EDIT_LIGHT_MENU_CREATION_EVENT )
	local lightCount = #g_t_lights
	KEP_EventEncodeNumber(event, lightCount)
	for i=1,lightCount do
		local oneLight = g_t_lights[i]
		KEP_EventEncodeNumber(event, oneLight["type"])
		KEP_EventEncodeNumber(event, oneLight["positionX"])
		KEP_EventEncodeNumber(event, oneLight["positionY"])
		KEP_EventEncodeNumber(event, oneLight["positionZ"])
		KEP_EventEncodeNumber(event, oneLight["diffuseR"])
		KEP_EventEncodeNumber(event, oneLight["diffureG"])
		KEP_EventEncodeNumber(event, oneLight["diffuseB"])
		KEP_EventEncodeNumber(event, oneLight["range"])
		KEP_EventEncodeNumber(event, oneLight["attenuation1"])
		KEP_EventEncodeNumber(event, oneLight["attenuation2"])
		KEP_EventEncodeNumber(event, oneLight["attenuation3"])
	end
	g_targetOverride = 1
	KEP_EventQueue( event )
end

function btnPickCusMeshes_OnButtonClicked( buttonHandle )
	MenuOpenModal("CollisionMeshPicker.xml")
	local event = KEP_EventCreate( VISUAL_NAMES_EVENT )
	KEP_EventEncodeString( event, "Select Customizable Meshes" )
	KEP_EventEncodeNumber( event, #g_t_visuals)
	for i=1, #g_t_visuals do
		local visual = g_t_visuals[i]
		KEP_EventEncodeString( event, visual.name)
		KEP_EventEncodeNumber( event, visual.customizable)
	end
	KEP_EventQueue( event )
	g_visual_awaiting = CUSTOMIZABLE_RESPONSE
end

function visualsResponseHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local count = KEP_EventDecodeNumber( event )
	if g_visual_awaiting == COLLISION_RESPONSE then
		for i=1, count do
			g_t_visuals[i].collide = KEP_EventDecodeNumber( event )
			if g_t_visuals[i].collide == 0 then
				g_targetOverride = 1
			end
		end
	elseif g_visual_awaiting == CUSTOMIZABLE_RESPONSE then
		for i=1, count do
			g_t_visuals[i].customizable = KEP_EventDecodeNumber( event )
			if g_t_visuals[i].customizable == 0 then
				g_targetOverride = 1
			end
		end
	end
end

function lightModificationsCompleteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local count = KEP_EventDecodeNumber(event)
	g_t_lights = {}
	for i=1,count do
		local oneLight = {}
		oneLight["type"] = KEP_EventDecodeNumber(event)
		oneLight["positionX"] = KEP_EventDecodeNumber(event)
		oneLight["positionY"] = KEP_EventDecodeNumber(event)
		oneLight["positionZ"] = KEP_EventDecodeNumber(event)
		oneLight["diffuseR"] = KEP_EventDecodeNumber(event)
		oneLight["diffureG"] = KEP_EventDecodeNumber(event)
		oneLight["diffuseB"] = KEP_EventDecodeNumber(event)
		oneLight["range"] = KEP_EventDecodeNumber(event)
		oneLight["attenuation1"] = KEP_EventDecodeNumber(event)
		oneLight["attenuation2"] = KEP_EventDecodeNumber(event)
		oneLight["attenuation3"] = KEP_EventDecodeNumber(event)
		table.insert(g_t_lights, oneLight)
	end
end
