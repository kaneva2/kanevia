--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\Scripts\\FameIds.lua")

--system defined animations
-- See http://wiki.intranet.kaneva.com/twiki/bin/view/Engineering/AnimationSlotMapping
-- for mapping definitions
anim_stand  = 1
anim_run    = 2
anim_walk   = 3
anim_head_bang = 4
anim_jump   = 5
anim_ssleft = 6
anim_ssright= 7
anim_die    = 8
anim_hip_hop = 9
anim_break_dance = 10
anim_unused4= 11
anim_fire   = 12
anim_unused5= 13
anim_swimi  = 14
anim_swima  = 15
anim_jet    = 16
anim_pain   = 17
anim_dance1 = 21
anim_dance2 = 22
anim_dance3 = 23
anim_wave   = 24
anim_unused5 = 25
anim_laugh  = 27--32
anim_sit = 28
anim_kneel  = 29
anim_smile  = 33
anim_surprize = 34
anim_frown  = 36
anim_rude   = 38
anim_mega_rave = 48 --must be version 500 to play
anim_laydown = 58
anim_female_disco = 59
anim_yes    = 64 --new: 29 + user_035 = 64, previous: 19
anim_no     = 65 --new: 29 + user_036 = 65, previous: 20
anim_point  = 66 -- new: 29 + user_037 = 66, previous: 35
anim_lol    = 67  -- new: 29 + user_038 = 67, previous: 27
anim_clap   = 69   -- new: 29 + user_040 = 69, previous: 31
anim_cheer  = 70 --new: 29 + user_041 = 70, previous: 18
anim_cry    = 71  -- new: 29 + user_042 = 71, previous: 26
anim_flirt  = 72 -- new: 29 + user_043 = 72, previous: 37
anim_male_rude = 75 --new: 29 + user_046 = 75, previous: 39
anim_blow   = 79 -- new: 29 + user_050 = 79, previous: 30
anim_sprinkler_dance   = 81 -- new: 29 + user_052 = 81
anim_lawnmower_dance   = 82 -- new: 29 + user_053 = 82
anim_robot_dance   = 83 -- new: 29 + user_054 = 83
anim_flexing_muscles   = 84 -- new: 29 + user_055 = 84
anim_chest_pound   = 85 -- new: 29 + user_056 = 85
anim_scream   = 86 -- new: 29 + user_057 = 86
anim_give_up   = 87 -- new: 29 + user_058 = 87
-- dance game animations
anim_male_country = 110 -- new: 29 + user_081 = 110, previous: 42
anim_male_disco = 111 -- new: 29 + user_082 = 111, previous: 41
anim_break_dance_3 = 112
anim_male_techno =  113 -- new: 29 + 84 = 113, previous: 40
anim_break_dance_2 = 114
anim_M_F_Idle_001  = 119
anim_M_F_Idle_002  = 120
anim_M_F_Idle_003  = 121
anim_M_F_Idle_004  = 122
anim_M_F_Idle_005  = 123
anim_M_F_Idle_006  = 124
anim_M_F_Idle_007  = 125
anim_M_F_Idle_008  = 126
anim_M_F_Flare_001 = 127
anim_M_F_Flare_002 = 128
anim_M_F_Flare_003 = 129
anim_M_F_Flare_004 = 130
anim_M_F_Flare_005 = 131
anim_M_F_Flare_006 = 132
anim_M_F_Flare_007 = 133
anim_M_F_Flare_008 = 134
anim_M_F_Flare_009 = 135
anim_male_disco = 136
anim_dance_backflip = 138 --must be version 500 to play
anim_dance_perfect = 139 -- must be version 500 to play
anim_sexy_01 = 149
anim_sexy_07 = 155
anim_latin_1 = 156
anim_latin_2 = 157
anim_male_breakdance_1 = 163
anim_male_breakdance_2 = 164
anim_country_1 = 170
anim_female_popdance_1 = 177
anim_pop_2 = 178
anim_female_popdance_3 = 179
anim_pop_5 = 181
anim_disco_1 = 184
anim_hiphop_2 = 192
anim_hiphop_6 = 196
anim_celebration_dance_1 = 234
anim_male_default_walk = 508
anim_male_default_run = 438
anim_male_default_stand = 208
anim_male_default_jump = 509
anim_female_default_walk = 383
anim_female_default_run = 21
anim_female_default_stand = 18
anim_female_default_jump = 339


-- list of animations available for the user to play from the emotes menu
valid_animations = {
	anim_stand,
	anim_run,
	anim_walk,
	anim_head_bang,
	anim_jump,
	anim_ssleft,
	anim_ssright,
	anim_die,
	anim_hip_hop,
	anim_break_dance,
	anim_unused4,
	anim_fire,
	anim_unused5,
	anim_swimi,
	anim_swima,
	anim_jet,
	anim_pain,
	anim_dance1,
	anim_dance2,
	anim_dance3,
	anim_wave,
	anim_unused5,
	anim_laugh,
	anim_sit,
	anim_kneel,
	anim_smile,
	anim_surprize,
	anim_frown,
	anim_rude,
	anim_mega_rave,
	anim_laydown,
	anim_yes,
	anim_no,
	anim_point,
	anim_lol,
	anim_clap,
	anim_cheer,
	anim_cry,
	anim_flirt,
	anim_male_rude,
	anim_blow,
	anim_sprinkler_dance,
	anim_lawnmower_dance,
	anim_robot_dance,
	anim_flexing_muscles,
	anim_chest_pound,
	anim_scream,
	anim_give_up,
	anim_male_country,
	anim_male_disco,
	anim_break_dance_3,
	anim_male_techno,
	anim_break_dance_2,
	anim_dance_backflip,
	anim_dance_perfect,
	anim_sexy_01,
	anim_sexy_07,
	anim_latin_1,
	anim_latin_2,
	anim_male_breakdance_1,
	anim_male_breakdance_2,
	anim_country_1,
	anim_female_popdance_1,
	anim_pop_2,
	anim_female_popdance_3,
	anim_pop_5,
	anim_disco_1,
	anim_hiphop_2,
	anim_hiphop_6,
	anim_celebration_dance_1
}

vip_animations = {
	anim_celebration_dance_1
}

ap_animations = {
}

faux_ugc_animations = {
	anim_male_disco,
	anim_female_disco,
	anim_male_default_walk,
	anim_male_default_run,
	anim_male_default_stand,
	anim_male_default_jump,
	anim_female_default_walk,
	anim_female_default_run,
	anim_female_default_stand,
	anim_female_default_jump
}

function isVIPPassRequired( animation )
	local table_length = #vip_animations

	for i = 1, table_length do
		if ( vip_animations[i] == animation ) then
			return true
		end
	end

	-- check inventory list
	if animation>=3000000 and g_t_items~=nil then
		for idx, t_item in ipairs( g_t_items ) do
			if t_item.GLID==animation and t_item.pass_list~=nil then
				local idx, passId
				for idx, passId in ipairs(t_item.pass_list) do
					if passId==2 then	-- VIP pass
						return true
					end
				end
			end
		end
	end
	return false
end

function isAccessPassRequired( animation )
	local table_length = #ap_animations
	for i = 1, table_length do
		if ( ap_animations[i] == animation ) then
			return true
		end
	end

	-- check inventory list
	if animation>=3000000 and g_t_items~=nil then
		for idx, t_item in ipairs( g_t_items ) do
			if t_item.GLID==animation and t_item.pass_list~=nil then
				local idx, passId
				for idx, passId in ipairs(t_item.pass_list) do
					if passId==1 then	-- access pass
						return true
					end
				end
			end
		end
	end

	return false
end

function isValidAnimation( animation )
	local table_length = #valid_animations
	for i = 1, table_length do
		if ( valid_animations[i] == animation ) then
			return true
		end
	end
	return false
end

function isValidFauxUGCAnimation( animation )
	local table_length = #faux_ugc_animations
	for i = 1, table_length do
		if ( faux_ugc_animations[i] == animation ) then
			return true
		end
	end
	return false
end

function isValidUGCAnimation( animGLID )
	if g_t_items==nil then
		return false, "Emotes loading in progress. Please try again later. "..KEP_GetVMFileName()	-- must have inventory list handy
	end

	local itemFound = nil
	local genderMatched = false
	local errMsg = nil
	for idx, t_item in ipairs( g_t_items ) do
		-- @Acampbell
		-- OR STATEMENT TEMPORARY FOR AB TEST OF NEW INVENTORY
		-- NEW INVENTORY IS THE FIRST STATEMENT, OLD INVENTORY IS THE SECOND STATEMENT
		if t_item.it_use_type==USE_TYPE.ANIMATION or t_item.use_type==USE_TYPE.ANIMATION then-- animation

			if t_item.GLID==animGLID then
				itemFound = t_item
				if t_item["actor_group"]==0 or t_item["actor_group"]==1 and gPlayerGender=="M" or t_item["actor_group"]==2 and gPlayerGender=="F" then
					genderMatched = true
					return true, nil, t_item
				end
			end
		end
	end

	if itemFound~=nil then
		LogError( "isValidUGCAnimation: Gender Mismatch - animGLID=" .. animGLID )
		if itemFound.actor_group==1 then
			errMsg = "Cannot use an emote designed for male avatars."
		else
			errMsg = "Cannot use an emote designed for female avatars."
		end
	else
		LogError( "isValidUGCAnimation: Not Currently Owned - animGLID=" .. animGLID )
		errMsg = "Cannot use an emote you no longer own."
	end
	return false, errMsg
end

function ChangeAnimation( animation )
	Log( "ChangeAnimation: animGLID=" .. animation )
	if animation >= 3000000 or isValidFauxUGCAnimation(animation) then
		ChangeAnimationByGLID( animation )
		return
	end
	if ( isValidAnimation( animation ) == false ) then
		return false
	end
	if ( ValidatePass(animation) == false ) then
		return false
	end
	KEP_SetCurrentAnimation(animation, version)
end

function ChangeAnimationByGLID( animGLID )
	local isValid, errMsg = isValidUGCAnimation( animGLID )
	if ( isValid == false ) then
		KEP_MessageBox( errMsg )
		return false
	end
	if ( ValidatePass( animGLID ) == false ) then
		return false
	end
	KEP_SetCurrentAnimationByGLID(animGLID)
end

function ValidatePass(animation, item)
	local apRequired = false
	local vipRequired = false

	-- get pass from item if provided
	if item~=nil and item.pass_list~=nil then
		for idx, pass_id in ipairs(item.pass_list) do
			if pass_id==1 then
				apRequired = true
			elseif pass_id==2 then
				vipRequired = true
			end
		end
	end

	if item==nil then
		-- if no item provided, try get it from inventory
		apRequired = isAccessPassRequired( animation )
	end
	if apRequired then
		if g_iHasAccessPass==nil or g_iHasAccessPass==0 then
			createLinkMessage( "You do not have the passes required to do that.", "Purchase Access Pass", GameGlobals.WEB_SITE_PREFIX_KANEVA..BUY_ACCESS_PASS_SUFFIX)
			return false
		elseif g_iZoneHasAccessPass==nil or g_iZoneHasAccessPass==0 then
			KEP_MessageBox( "You cannot use this emote here. Please try it in an access-pass enabled zone." )
			return false
		end
	end

	if item==nil then
		-- if no item provided, try get it from inventory
		vipRequired = isVIPPassRequired( animation )
	end
	if vipRequired then
		if g_iHasVIPPass==nil or g_iHasVIPPass == 0 then
			createLinkMessage( "You do not have the passes required to do that.", "Purchase VIP Pass", GameGlobals.WEB_SITE_PREFIX_KANEVA..BUY_ACCESS_PASS_SUFFIX)
			return false
		else
		-- we should allow using VIP animation in a non-VIP zone, shouldn't we?
		end
	end
	return true
end

function TryUGCAnimation( item )
	Log( "TryUGCAnimation: animGLID=" .. item.GLID )

	if item.GLID<3000000 then
		-- not an UGC item
		LogError( "TryUGCAnimation: Bad Parameter - animGLID=" .. item.GLID )
		KEP_MessageBox( "Try on failed: bad parameter." )
		return false
	end

	-- gender verification
	if item.actor_group==1 and gPlayerGender~="M" or item.actor_group==2 and gPlayerGender~="F" then
		LogError( "TryUGCAnimation: Gender Mismatch - animGLID=" .. item.GLID)
		if item.actor_group==1 then
			KEP_MessageBox( "Cannot try on emote designed for male avatars." )
		else
			KEP_MessageBox( "Cannot try on emote designed for female avatars." )
		end
		return false
	end

	-- pass verification
	if ValidatePass(animation, item) == false then
		return false
	end
	KEP_SetCurrentAnimationByGLID(item.GLID)
end
