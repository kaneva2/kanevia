--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\Scripts\\Progressive.lua")

YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"

USERID_PARSE = 1
BALANCE_PARSE = 2

g_debug = false
g_firstTimeSpawn = 0

g_parseType = 0
g_regCredits = 0
g_coverCharge = 0
g_userID = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "pendingSpawnHandler", KEP.PENDINGSPAWN_EVENT_NAME, KEP.HIGH_PRIO - 1 )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( Yes_NO_ANSWER_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	local t = { KEP_GetPlayerInfo() }
	if t ~= 0  then
		g_parseType = USERID_PARSE
		if t[11] ~= nil then
			getUserID(t[11], WF.COVER_CHARGE)
		else
			name = KEP_GetLoginName()
			getUserID( name, WF.COVER_CHARGE )
		end
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.COVER_CHARGE then
		if g_parseType == USERID_PARSE then
			local result = nil  -- result description
			local s, e = 0      -- start and end of captured string
			local sh = nil		-- static handle
			local userData = KEP_EventDecodeString( event )
			s, e, result = string.find(userData, "<ReturnCode>(%d+)</ReturnCode>")
			if result == "0" then
				s, e, result = string.find(userData, "<user_id>(.-)</user_id>", 1)
				g_userID = tonumber(result)
				g_parseType = BALANCE_PARSE
				getBalances( g_userID, WF.COVER_CHARGE )
			end
		elseif g_parseType == BALANCE_PARSE then
			local result = nil  -- result description
			local s, e = 0      -- start and end of captured string
			local sh = nil		-- static handle
			local userData = KEP_EventDecodeString( event )

			s, e, result = string.find(userData, "<ReturnCode>(%d+)</ReturnCode>")

			if result == "0" then
				s, strPos, regCredits = string.find(userData, "<Balance>(.-)</Balance>", 1)
				if regCredits ~= nil then
					g_regCredits = tonumber(regCredits)

					sh = Dialog_GetStatic(gDialogHandle, "lblBalance")
					if sh ~= nil then
						Static_SetText(sh, tostring(g_regCredits))
					end
				end
			end
		end
	end
end

function getUserID( playerName, menu_filter )
	if playerName ~= nil and playerName ~= "" then
		makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..playerName, menu_filter)
	end
end

function getBalances(userID, menu_filter)
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..BALANCE_SUFFIX..userID, menu_filter)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

g_PendingSpawnEvent = nil

function btnOk_OnButtonClicked( buttonHandle )
	if g_PendingSpawnEvent ~= nil then

		if g_debug then
			Dialog_MsgBox( "CoverCharge accepted" )
		end

		if g_regCredits < 0 or g_regCredits >= g_coverCharge then
			if PROGRESSIVE then
--				ProgressOpen("Rezone")   -- DRF - Reopen Rezone On Cover Charge Accepted
				MenuOpen("ZoneDownloader.xml")
			else
				KEP_EventAddToServer( g_PendingSpawnEvent ) -- send to server if not progressive
			end
			KEP_EventQueue( g_PendingSpawnEvent )
			g_PendingSpawnEvent = nil
		else
			createLinkMessage( "You can't afford this cover charge with CREDITS.  Click below to purchase more.", "Purchase Credits", GameGlobals.WEB_SITE_PREFIX_KANEVA..BUY_CREDITS_SUFFIX)

			notGoingThere();
		end
	end
	
	MenuCloseThis()
end

function btnCancel_OnButtonClicked( buttonHandle )
	notGoingThere()
end

function notGoingThere()

	-- Cancel this pending spawn
	KEP_EventSetObjectId( g_PendingSpawnEvent, 0 )
	KEP_EventAddToServer( g_PendingSpawnEvent )
	KEP_EventQueue( g_PendingSpawnEvent )
	g_PendingSpawnEvent = nil

	MenuCloseThis()
end

function pendingSpawnHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- remake the event to send to server if/when we complete download
	g_PendingSpawnEvent = KEP_EventCreate( KEP.PENDINGSPAWN_EVENT_NAME )
	KEP_EventSetFilter( g_PendingSpawnEvent, filter )
	KEP_EventSetObjectId( g_PendingSpawnEvent, objectid )
	g_firstTimeSpawn = KEP_EventDecodeNumber( event ) -- initialSpawn
	KEP_EventEncodeNumber( g_PendingSpawnEvent, g_firstTimeSpawn  )
	g_coverCharge = KEP_EventDecodeNumber( event )
	KEP_EventEncodeNumber( g_PendingSpawnEvent, g_coverCharge ) -- coverCharge
	KEP_EventEncodeNumber( g_PendingSpawnEvent, KEP_EventDecodeNumber( event ) ) -- instanceId
	sh = Dialog_GetStatic(gDialogHandle, "lblKpts")
	if sh ~= nil then
		Static_SetText(sh, tostring(g_coverCharge))
	end
	return KEP.EPR_CONSUMED
end
