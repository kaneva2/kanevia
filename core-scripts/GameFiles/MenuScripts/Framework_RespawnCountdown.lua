--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_RespawnCountdown.lua
-- 
-- Displays respawn time for players trying to respawn
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

----------------------
-- Globals
----------------------
local OCCUPIED_ANIMATION_GLID = 0 --TODO: Need asset GLID from Dixon

local m_respawnTime
local m_currTick = 0

-- When the menu is created
function onCreate()
	-- Register handlers for server events
	Events.registerHandler("FRAMEWORK_CANCEL_RESPAWN", cancelRespawn)
	Events.registerHandler("FRAMEWORK_RESPAWN_TIME", respawnTime)
end

-- Called every frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_respawnTime then
		if m_respawnTime > 0 then
			KEP_SetCurrentAnimationByGLID(OCCUPIED_ANIMATION_GLID)
			if math.ceil(m_respawnTime) > math.ceil(m_respawnTime - fElapsedTime) and (m_currTick <= 16) then
				Control_SetVisible(gHandles["imgTick"..tostring(m_currTick)], false)
				m_currTick = m_currTick + 1

				if m_currTick == 16 then
					Static_SetText(gHandles["stcSeconds"], "Second")
				end
			end
			Static_SetText(gHandles["stcCountdown"], math.ceil(m_respawnTime))
			m_respawnTime = m_respawnTime - fElapsedTime
		else
			KEP_SetCurrentAnimationByGLID(0)
			m_respawnTime = nil
			Events.sendEvent("FRAMEWORK_RESPAWN_PLAYER")
			MenuCloseThis()
		end
	end
end

-- Called when the menu is destroyed for whatever reason
function onDestroy()
	KEP_SetCurrentAnimationByGLID(0)
	Events.sendEvent("FRAMEWORK_MANUAL_RESPAWN_CANCELLED")
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Here's the respawn time
function respawnTime(event)
	m_respawnTime = event.respawnTime
	m_currTick = 2
	
	Static_SetText(gHandles["stcCountdown"], math.ceil(m_respawnTime))
	Control_SetVisible(gHandles["stcCountdown"], true)
	Control_SetVisible(gHandles["imgTick1"], false)
end

-- Called when the server wants to cancel the respawn
function cancelRespawn(event)
	MenuCloseThis()
end