--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_LoadProgress.lua
--
-- Tracks the loading progress for the Framework during 
-- instantiation and de-instantiation
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- Animation rate (width/second)
local ANIMATE_SPEED = 200

local BAR_WIDTH = 314
local m_animating = false
local m_currBarWidth = 0
local m_totalBarWidth = 0
local m_queueClose = false
local m_playerName, m_zoneInstanceId, m_zoneType

-- All known Framework xml menus to be closed on Framework de-activation
local FRAMEWORK_MENUS = {"Framework_BattleHandler.xml",
						 "Framework_BottomRightHUD.xml",
						 "Framework_Characters.xml",
						 "Framework_Quests.xml",
						 "Framework_Console.xml",
						 "Framework_CraftingRecipes.xml",
						 "Framework_Tester.xml",
						 "Framework_DialogEditor.xml",
						 "Framework_DragDropElement.xml",
						 "Framework_DragDropHandler.xml",
						 "Framework_GameItemEditor.xml",
						 "Framework_GameItemLoadWarning.xml",
						 "Framework_GameItemNameConflict.xml",
						 "Framework_GameSystems.xml",
						 "Framework_Harvestables.xml",
						 "Framework_ImageHandler.xml",
						 "Framework_Indicators.xml",
						 "Framework_InventoryHandler.xml",
						 "Framework_ItemContainer.xml",
						 "Framework_ItemSelectEditor.xml",
						 "Framework_JetPack.xml",
						 "Framework_NameResolver.xml",
						 "Framework_NPCDialog.xml",
						 "Framework_OverwriteConfirmation.xml",
						 "Framework_Placeables.xml",
						 "Framework_PlayerBuild.xml",
						 "Framework_PublishGameItem.xml",
						 "Framework_QuestHandler.xml",
						 "Framework_SaveAs.xml",
						 "Framework_SaveVerify.xml",
						 "Framework_StatusIcons.xml",
						 "Framework_Spawners.xml",
						 "Framework_Toolbar.xml",
						 "Framework_Tooltip.xml",
						 "Framework_WorldInventory.xml",
						 "Framework_WorldSettings.xml",
						 "Framework_PlayerSystems.xml"}

-- Called on create
function onCreate()
	-- Register server event handlers
	Events.registerHandler("FRAMEWORK_LOG_PROGRESS",  logProgress)
	Events.registerHandler("FRAMEWORK_CLOSE_PROGRESS",  closeProgress)
	Events.registerHandler("FRAMEWORK_CLOSE_ALL_FRAMEWORK_MENUS",  closeAllFrameworkMenus)
	
	m_playerName = KEP_GetLoginName()
	m_zoneInstanceId = KEP_GetZoneInstanceId()
	m_zoneType = KEP_GetZoneIndexType()
end

-- Handles load progress
function logProgress(event)
	if event.loadPercentage then
		m_animating = true
		m_totalBarWidth = ((event.loadPercentage/100) * BAR_WIDTH)
		m_currBarWidth = Control_GetWidth(gHandles["imgLoadingBar"])
	end
	if event.display then
		Static_SetText(gHandles["stcLoadMessage"], event.display)
		Control_SetVisible(gHandles["stcLoadMessage"], true)
	end
	KEP_Log("           Progress: "..tostring(event.log))
end

-- Called each frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_animating then
		if m_currBarWidth < m_totalBarWidth then
			local barHeight = Control_GetHeight(gHandles["imgLoadingBar"])
			m_currBarWidth = math.min(m_totalBarWidth, m_currBarWidth + (fElapsedTime*ANIMATE_SPEED))
			Control_SetSize(gHandles["imgLoadingBar"], m_currBarWidth, barHeight)
		elseif m_queueClose then
			MenuCloseThis()
		end
	end
end

-- Closes all Framework menus (on Framework de-instantiation
function closeAllFrameworkMenus(event)
	for i=1, #FRAMEWORK_MENUS do
		MenuClose(FRAMEWORK_MENUS[i])
	end
end

-- Close the progress menu
function closeProgress(event)
	m_queueClose = true
end