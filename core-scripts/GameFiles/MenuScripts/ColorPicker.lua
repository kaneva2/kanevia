--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

COLOR_PICKER_EVENT = "ColorSelectedEvent"
COLOR_START_EVENT = "ColorStartEvent"

MENU_EDITOR_COLOR_FILTER = 1
TEXTURE_PICKER_COLOR_FILTER = 2
FONT_PICKER_COLOR_FILTER = 3

FONT_COLOR_TARGET = 11
TEXTURE_COLOR_TARGET = 12

gColorPickedA = 0
gColorPickedR = 0
gColorPickedG = 0
gColorPickedB = 0
gColorTarget = nil
gEventFilter = nil

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "colorStartEventHandler", COLOR_START_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( COLOR_START_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( COLOR_PICKER_EVENT, KEP.MED_PRIO )
end

function colorStartEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	gEventFilter = filter
	gColorPickedA = KEP_EventDecodeNumber(event)
	gColorPickedR = KEP_EventDecodeNumber(event)
	gColorPickedG = KEP_EventDecodeNumber(event)
	gColorPickedB = KEP_EventDecodeNumber(event)
	gColorTarget = KEP_EventDecodeNumber(event)
	local color = { a=gColorPickedA, r=gColorPickedR, g = gColorPickedG, b=gColorPickedB  }

	Slider_SetValue( gHandles["aSlider"], gColorPickedA )
	Slider_SetValue( gHandles["rSlider"], gColorPickedR )
	Slider_SetValue( gHandles["gSlider"], gColorPickedG )
	Slider_SetValue( gHandles["bSlider"], gColorPickedB )

	EditBox_SetText(gHandles["edAlpha"], gColorPickedA, false)
	EditBox_SetText(gHandles["edRed"], gColorPickedR, false)
	EditBox_SetText(gHandles["edGreen"], gColorPickedG, false)
	EditBox_SetText(gHandles["edBlue"], gColorPickedB, false)

	Image_SetColor( gHandles["testImg"], color )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnSetColor_OnButtonClicked( buttonHandle )
	local ev = KEP_EventCreate( COLOR_PICKER_EVENT)
	KEP_EventSetFilter(ev, gEventFilter)
	KEP_EventEncodeNumber(ev, gColorPickedA)
	KEP_EventEncodeNumber(ev, gColorPickedR)
	KEP_EventEncodeNumber(ev, gColorPickedG)
	KEP_EventEncodeNumber(ev, gColorPickedB)
	KEP_EventEncodeNumber(ev, gColorTarget)
	KEP_EventQueue( ev)
	MenuCloseThis()
end

function aSlider_OnSliderValueChanged( slider, value )
	EditBox_SetText(gHandles["edAlpha"], value, false)
	testColor()
end

function rSlider_OnSliderValueChanged( slider, value )
	EditBox_SetText(gHandles["edRed"], value, false)
	testColor()
end

function gSlider_OnSliderValueChanged( slider, value )
	EditBox_SetText(gHandles["edGreen"], value, false)
	testColor()
end

function bSlider_OnSliderValueChanged( slider, value )
	EditBox_SetText(gHandles["edBlue"], value, false)
	testColor()
end

function edAlpha_OnEditBoxChange( editBox, value )
	Slider_SetValue( gHandles["aSlider"], value )
	testColor()
end

function edRed_OnEditBoxChange( editBox, value )
	Slider_SetValue( gHandles["rSlider"], value )
	testColor()
end

function edGreen_OnEditBoxChange( editBox, value )
	Slider_SetValue( gHandles["gSlider"], value )
	testColor()
end

function edBlue_OnEditBoxChange( editBox, value )
	Slider_SetValue( gHandles["bSlider"], value )
	testColor()
end

function testColor()
	local a = tonumber(Slider_GetValue( gHandles["aSlider"] ))
	local r = tonumber(Slider_GetValue( gHandles["rSlider"] ))
	local g = tonumber(Slider_GetValue( gHandles["gSlider"] ))
	local b = tonumber(Slider_GetValue( gHandles["bSlider"] ))
	gColorPickedA = a
	gColorPickedR = r
	gColorPickedG = g
	gColorPickedB = b
	local color = {  a=gColorPickedA, r=gColorPickedR, g = gColorPickedG, b=gColorPickedB }
	Image_SetColor( gHandles["testImg"], color )
end

function DEC_HEX(IN)
	if (IN == nil or IN == 0) then
		return "00"
	end
	local B,K,OUT,I,D=16,"0123456789ABCDEF","",0
	while IN>0 do
		I=I+1
		IN,D=math.floor(IN/B),math.fmod(IN,B)+1
		OUT=string.sub(K,D,D)..OUT
	end
	if (string.len(OUT) == 1) then
		return "0" .. OUT
	end
	return OUT
end
