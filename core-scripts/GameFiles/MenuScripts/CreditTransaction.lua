--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- CreditTransaction.lua
-- 
-- Credit Transaction menu for donations, etc
-------------------------------------------------------------------------------
dofile("MenuHelper.lua")
dofile("HUDHelper.lua")
dofile("CreditBalances.lua")

----------------------
-- Statics
----------------------
local PREMIUM_ITEM_PURCHASE_SUFFIX1 = "kgp/instantbuy.aspx?operation=buy&credit=KPOINT&qnty=1&premium=true&items="
local PREMIUM_ITEM_PURCHASE_SUFFIX2 = "&communityId="
local CURRENCY_TRADING_SUFFIX1 = "kgp/tradeItems.aspx?action=giftCreditsToUser&amount="
local CURRENCY_TRADING_SUFFIX2 = "&username="

----------------------
-- Globals
----------------------
local m_canPurchase = true 	-- set to false if credit balance < purchase amount
local g_id = "" 			-- ID of item to be purchased

----------------------
-- Locals
----------------------
local username = "" 		-- to obtain userID
local userID = 0 			-- to obtain current balances
local credits = 0			-- current credits balance
local rewards = 0			-- current rewards balance
local price = "0"			-- price of item to be purchased
local worldName = ""		-- world name of item to be purchased
local communityId = ""		-- to purchase premium items
local sourceMenu = ""		-- menu source of credit transaction
local receivingPlayerName = "" -- name of player to receive credits

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegister ("CREDIT_TRANSACTION_COMPLETE", KEP.MED_PRIO)
	KEP_EventRegister ("CONFIRM_CREDIT_TRANSACTION", KEP.MED_PRIO)

	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "getCreditTransaction", "UPDATE_CREDIT_TRANSACTION", KEP.HIGH_PRIO)
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	username = KEP_GetLoginName()
	requestUserId()
end

-- Dialog_OnDestroy = Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

-- Gets current player balance through CreditBalances.lua
function requestCreditsRewards()
	-- If the user requested exists, get their balance
	if(userID > 0) then
		getBalances(userID, WF.MYFAME_GETBALANCE)
	end
end

-- Web call to receive userID from username
function requestUserId()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..username, WF.MYFAME_USERID)
end

-- Add comma to separate thousands
function commaValue(amount)
  	local formatted = amount
  	-- Loop through string...
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
    	-- until we reach the end of the string
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end

-- Gets userID from web call
function ParseUserID(returnData)
	local result = nil  	-- result description
	local s, e = 0      	-- start and end of captured string
	s, e, result = string.find(returnData, "<ReturnCode>(%d+)</ReturnCode>")
	
	-- If the user ID returns true
	if result == "0" then
		-- Parse needed info from XML
		s, e, result = string.find(returnData, "<user_id>(%d+)</user_id>", e)
		userID = tonumber(result)
		requestCreditsRewards()
	end
end

-- Reads result of purchase web call and alerts player of success or failure
function ParsePurchaseSuccess(returnData)
	local result = nil  	-- result description
	local s, e = 0      	-- start and end of captured string
	s, e, result = string.find(returnData, "<ReturnCode>(%d+)</ReturnCode>")
	
	-- If the purchase is successful, alert the player
	if result == "0" then
		local event = KEP_EventCreate("CREDIT_TRANSACTION_COMPLETE")
		KEP_EventEncodeString(event, commaValue(tonumber(price)))
		KEP_EventEncodeString(event, sourceMenu)
		if sourceMenu == "GiveCredits" then
			KEP_EventEncodeString(event, receivingPlayerName)
		else
			KEP_EventEncodeString(event, worldName)
		end
		KEP_EventQueue(event)
		toggleMenu("TransactionComplete")

	-- If the purchase failed, alert the player and log the specific error result
	else
		Log("ParsePurchaseSuccess: FAILED - result="..result)
	end

	MenuCloseThis()
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- If browser return for Balance Data web call
	if filter == WF.MYFAME_GETBALANCE then
		Static_SetText(gHandles["lblAmountNum"], commaValue(tonumber(price)).." credits")
		
		if sourceMenu == "Donation" then
			Static_SetText(gHandles["lblPurchaseText"], "Donation to "..tostring(worldName))
		elseif sourceMenu == "GiveCredits" then
			Static_SetText(gHandles["lblPurchaseText"], "Gift to "..tostring(receivingPlayerName))
		elseif sourceMenu == "Vendor" then
			Static_SetText(gHandles["lblPurchaseText"], "Purchase "..tostring(gameItem) .." in "..tostring(communityId))
		elseif sourceMenu == "Framework_ClothingVendor" then
			Static_SetText(gHandles["lblPurchaseText"], "Purchase "..tostring(gameItem))
		elseif sourceMenu == "Zone" then
			Static_SetText(gHandles["lblPurchaseText"], "New Zone for "..tostring(communityId))
		else
			Static_SetText(gHandles["lblPurchaseText"], "Credit Purchase")
		end

		local returnData = KEP_EventDecodeString( tEvent )
		credits, rewards = ParseBalances( returnData )
		Static_SetText( gHandles["lblCreditBalance"], "    Your Credit Balance "..tostring(commaValue(credits)))
		-- measures credit length and moves gold icon over to compensate
		Control_SetLocation(gHandles["imgWindowBarIcon1"], 97 - (string.len(credits)*5), 91)

		local currDisp = Static_GetDisplayElement(gHandles["lblCreditBalance"])
		local color = {}
		local fontWeight = 0
		-- If there are enough credits for the transaction, set canpurchase to true
		if tonumber(price) <= credits then
			color = {a=255,r=0,g=0,b=0}
			fontWeight = 400
			Static_SetText( gHandles["btnSubmitORBuyCredits"], "Submit" )
			m_canPurchase = true
		-- If not enough credits, set can purchase to false
		else
			color = {a=255,r=255,g=0,b=0}
			fontWeight = 700
			Static_SetText( gHandles["btnSubmitORBuyCredits"], "Buy Credits" )
			m_canPurchase = false
		end
		Element_AddFont( currDisp, gDialogHandle, "Verdana", -11, fontWeight, false )
		BlendColor_SetColor(Element_GetFontColor(currDisp), 0, color)

	   	return KEP.EPR_OK
	-- If browser return for User ID web call
	elseif filter == WF.MYFAME_USERID then
		local returnData = KEP_EventDecodeString( tEvent )
		ParseUserID( returnData )
	   	return KEP.EPR_OK
	-- If browser return for Premium Item Purchase web call
	elseif filter == WF.PURCHASE_SUCCESS then
		local returnData = KEP_EventDecodeString( tEvent )
		ParsePurchaseSuccess( returnData )
	   	return KEP.EPR_OK
	elseif filter == WF.CURRENCY_TRADING then
		local returnData = KEP_EventDecodeString( tEvent )
		ParsePurchaseSuccess( returnData )
		return KEP.EPR_OK
	end
end

-- Handles transaction passed from previous menu choice (e.g. Donation menu choice)
function getCreditTransaction(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	price = KEP_EventDecodeString(tEvent)
	g_id = KEP_EventDecodeString(tEvent)
	communityId = KEP_EventDecodeString(tEvent)
	sourceMenu = KEP_EventDecodeString(tEvent)
	if sourceMenu == "Donation" then
		worldName = KEP_EventDecodeString(tEvent)
	elseif sourceMenu == "GiveCredits" then
		receivingPlayerName = KEP_EventDecodeString(tEvent)
	elseif sourceMenu == "Vendor" or sourceMenu == "Framework_ClothingVendor" then
		gameItem = KEP_EventDecodeString(tEvent)
	end
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

-- Submit purchase or buy credits
function btnSubmitORBuyCredits_OnButtonClicked(buttonHandle)
	-- If not enough credits, send to credit purchase page
	if not m_canPurchase then
		KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .. BUY_CREDIT_PACKAGES_SUFFIX)
		toggleMenu(sourceMenu)
		MenuCloseThis()
	-- If can purchase, call purchase of premium item
	else
		if sourceMenu == "GiveCredits" then
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..CURRENCY_TRADING_SUFFIX1..price..CURRENCY_TRADING_SUFFIX2..receivingPlayerName, WF.CURRENCY_TRADING )
		elseif sourceMenu == "Vendor"  or sourceMenu == "Framework_ClothingVendor" then
			local event = KEP_EventCreate("CONFIRM_CREDIT_TRANSACTION")
			KEP_EventQueue(event)

			--toggleMenu(sourceMenu)
			MenuCloseThis()
		elseif sourceMenu == "Zone" then
			local event = KEP_EventCreate("CONFIRM_CREDIT_ZONE_TRANSACTION")
			KEP_EventQueue(event)
			MenuCloseThis()
		else
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..PREMIUM_ITEM_PURCHASE_SUFFIX1..g_id..PREMIUM_ITEM_PURCHASE_SUFFIX2..communityId, WF.PURCHASE_SUCCESS)
		end
	end
end
