--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

PURCHASE_RAVE_EVENT = "PurchaseRaveEvent"
ITEM_INFO_EVENT = "ItemInfoEvent"
ITEM_PURCHASED_EVENT = "ItemPurchasedEvent"
RAVE_PURCHASED_EVENT = "RavePurchasedEvent"

g_strSelectedPlayerName = nil

INSTANT_BUY_PAYMENT_TYPE = {}
INSTANT_BUY_PAYMENT_TYPE.NONE = 0
INSTANT_BUY_PAYMENT_TYPE.REWARDS = 1
INSTANT_BUY_PAYMENT_TYPE.CREDITS = 2
INSTANT_BUY_PAYMENT_TYPE.ALWAYS_REWARDS = 3
INSTANT_BUY_PAYMENT_TYPE.ALWAYS_CREDITS = 4

PARSE_SAVE_PREFERENCES = 0
PARSE_GET_PREFERENCES = 1

g_paymentPreference = INSTANT_BUY_PAYMENT_TYPE.NONE
g_parsePaymentSettings = PARSE_GET_PREFERENCES
g_CurrencyType = {}
g_CurrencyType.Rewards = "GPOINT"
g_CurrencyType.Credits = "KPOINT"

INVENTORY_TYPE = {}
INVENTORY_TYPE.CREDITS = 256
INVENTORY_TYPE.REWARDS = 512
INVENTORY_TYPE.CREDITS_OR_REWARDS = 768

ITEM_TYPE_ITEM = 0
ITEM_TYPE_RAVE = 1

g_itemType = ITEM_TYPE_ITEM
g_raveType = 0 -- 0 is single rave, 1 is megarave
g_raveTarget = 0 -- 0 is ravePlayer, 1 is ravePlace
g_ravedID = 0 --userID of player being raved
g_itemGlids = nil
g_itemQty = nil
g_inventoryType = nil

g_tblObjectData = {}

bVal = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel )
    KEP_EventRegisterHandler( "itemInfoHandler", ITEM_INFO_EVENT, KEP.HIGH_PRIO )
    KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
    KEP_EventRegisterHandler( "PurchaseRaveEventHandler", PURCHASE_RAVE_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( ITEM_PURCHASED_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( ITEM_INFO_EVENT, KEP.HIGH_PRIO )
end

function PurchaseRaveEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    g_ravedID = KEP_EventDecodeNumber(event)
    g_raveType = KEP_EventDecodeNumber(event)
    g_raveTarget = KEP_EventDecodeNumber(event)
    g_itemType = ITEM_TYPE_RAVE
    g_inventoryType = INVENTORY_TYPE.CREDITS_OR_REWARDS
    checkPurchasePreference()
    return KEP.EPR_CONSUMED	
end

function itemInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_itemGlids = {}
	g_itemQty = {}
	g_inventoryType = KEP_EventDecodeNumber( event )
	local num_items = KEP_EventDecodeNumber( event )
	for i = 1, num_items do
		local glid = KEP_EventDecodeNumber( event )
		local qty = KEP_EventDecodeNumber( event )
		table.insert(g_itemGlids, glid)
		table.insert(g_itemQty, qty)
	end
	return KEP.EPR_CONSUMED	
end

function purchaseItem(currencyType)
	if g_itemType == ITEM_TYPE_ITEM then
		sendBuyItemRequest(g_itemGlids, g_itemQty, currencyType)
	elseif g_itemType == ITEM_TYPE_RAVE then
		sendBuyRaveRequest(g_ravedID, g_raveType, g_raveTarget, currencyType)
	end
end

function checkPurchasePreference()
	-- Take care of the case where an item can only be purchased using a certain currency type.
	if g_inventoryType == INVENTORY_TYPE.CREDITS then
		purchaseItem(g_CurrencyType.Credits)
	elseif g_inventoryType == INVENTORY_TYPE.REWARDS then
		purchaseItem(g_CurrencyType.Rewards)
	else
		-- When any currency type is acceptable, use the player's currency preference if one exists.
		if g_paymentPreference == INSTANT_BUY_PAYMENT_TYPE.ALWAYS_CREDITS then
			purchaseItem(g_CurrencyType.Credits)
		elseif g_paymentPreference == INSTANT_BUY_PAYMENT_TYPE.ALWAYS_REWARDS then
			purchaseItem(g_CurrencyType.Rewards)
		elseif g_paymentPreference == INSTANT_BUY_PAYMENT_TYPE.NONE then
			MenuSetMinimizedThis(false)
		end
	end
end

function btnCredits_OnButtonClicked( buttonHandle )
	KEP_Log( "Purchasing with Credits "..g_ravedID)
	if (g_itemGlids ~= nil and g_itemQty ~= nil) or (g_ravedID ~= 0) or (g_raveTarget == 1) then
	    saveInstantBuyPaymentSettings()
	    
	    -- Take care of the case where an item can only be purchased using a certain currency type.
		if g_inventoryType == INVENTORY_TYPE.REWARDS then
  			KEP_LogDebug( "Rewards only item... using rewards!")
			purchaseItem(g_CurrencyType.Rewards)
		else
	    	purchaseItem(g_CurrencyType.Credits)
		end
	end
end

function btnBuyNow_OnButtonClicked( buttonHandle )
    btnCredits_OnButtonClicked(nil)
end

function btnRewards_OnButtonClicked( buttonHandle )

	if (g_itemGlids ~= nil and g_itemQty ~= nil) or (g_ravedID ~= 0) or (g_raveTarget == 1) then
	    saveInstantBuyPaymentSettings()
	    
	    -- Take care of the case where an item can only be purchased using a certain currency type.
		if g_inventoryType == INVENTORY_TYPE.CREDITS then
   			purchaseItem(g_CurrencyType.Credits)
		else
      		purchaseItem(g_CurrencyType.Rewards)
		end
	end
end

function btnBuyMore_OnButtonClicked( buttonHandle )
	KEP_LaunchBrowser(GameGlobals.WEB_SITE_PREFIX_KANEVA..BUY_SPECIAL_SUFFIX)
end

function sendBuyItemRequest(iGlids, iQtys, strCurrencyType)
	local glids = ""
	local qtys = ""
	for i = 0, #iGlids do
		glids = glids .. tostring(iGlids[i]) .. ","
		qtys = qtys .. tostring(iQtys[i]) .. ","
	end
	glids = string.sub(glids, 1, -2)
	qtys = string.sub(qtys, 1, -2)
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..BUY_DYNAMIC_OBJECT_SUFFIX.."&credit="..strCurrencyType.."&items="..glids.."&qnty="..qtys, WF.BUY_DYNAMIC_OBJECT, 0, 0)
end

function sendBuyRaveRequest(ravedID, raveType, raveTarget, strCurrencyType)
	local g_web_address = ""
	strRaveType = "single"
	if raveType == 1 then
		strRaveType = "mega"
	end
	if raveTarget == 0 then --raving player
		g_web_address = GameGlobals.WEB_SITE_PREFIX..RAVE_SUFFIX..ravedID.."&raveType="..strRaveType.."&currency="..strCurrencyType
	else --raving current zone
		g_web_address = GameGlobals.WEB_SITE_PREFIX..SET_RAVES_SUFFIX.."&raveType="..strRaveType.."&currency="..strCurrencyType
	end
	makeWebCall( g_web_address, WF.BUY_RAVE)
end

function parseEventHeader(strXmlData, objectContainer)
   objectContainer.ReturnCode = parseDataByTag(strXmlData, "ReturnCode", "(%d+)")
   objectContainer.ResultDescription = parseDataByTag(strXmlData, "ResultDescription", "(.-)")
   return tonumber(objectContainer.ReturnCode)
end

function parseDataByTag(strEventData, strTag, strPattern)
	local strStartTag = "<" .. strTag .. ">"
	local strEndTag = "</" .. strTag .. ">"
	local strPattern = strStartTag..strPattern..strEndTag
	local iStart, iEnd, strData = string.find(strEventData, strPattern)
	return strData
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.BUY_DYNAMIC_OBJECT then
	    local strXmlData = KEP_EventDecodeString( event )
	    local result = parseEventHeader(strXmlData, g_tblObjectData)
	    MenuBringToFrontThis()
     	if result == 0 then
     		-- Item was purchased successfully
            sendItemsPurchasedEvent(g_itemGlids)
			MenuCloseThis()
		elseif result == 1 then
		    -- Not Enough Rewards
		    MenuSetMinimizedThis(false)
		    hideMenu()
		    showNotEnoughMoneyTypeMenu("rewards")
		elseif result == 2 then
		    -- Not Enough Credits
		    MenuSetMinimizedThis(false)
		    hideMenu()
		    showNotEnoughMoneyTypeMenu("credits")
		else
		    -- Some other reason
		    KEP_MessageBox(tostring(g_tblObjectData.ResultDescription))
		    MenuCloseThis()
		end
	elseif filter == WF.BUY_RAVE then
		local strXmlData = KEP_EventDecodeString( event )
		local result = parseEventHeader(strXmlData, g_tblObjectData)
	 	if result == 0 then
		 	-- Item was purchased successfully
		 	KEP_MessageBox("Purchase Successful!")
		 	KEP_EventCreateAndQueue( RAVE_PURCHASED_EVENT)
		    MenuCloseThis()
		elseif result == 1 or result == 2 then
			-- Not Enough Rewards
			MenuSetMinimizedThis(false)
			hideMenu()
			showNotEnoughMoneyTypeMenu("rewards")
		else
			-- Not Enough Credits
			MenuSetMinimizedThis(false)
			hideMenu()
			showNotEnoughMoneyTypeMenu("credits")
		end
	elseif filter == WF.PLAYER_PREFERENCES then
		local strXmlData = KEP_EventDecodeString( event )
		if g_parsePaymentSettings == PARSE_GET_PREFERENCES then
			ParseGetPreferences(strXmlData)
		else
			ParseSetPreferences(strXmlData)
		end
	end
end

function ParseGetPreferences(strXmlData)
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local paymentPreference = ""  		--paymentPreference of player
	s, strPos, paymentPreference = string.find(strXmlData, "<AlwaysPurchaseWithCurrencyType>(.-)</AlwaysPurchaseWithCurrencyType>")
	if paymentPreference == "KPOINT" then
		g_paymentPreference = INSTANT_BUY_PAYMENT_TYPE.ALWAYS_CREDITS
	elseif paymentPreference == "GPOINT" then
		g_paymentPreference = INSTANT_BUY_PAYMENT_TYPE.ALWAYS_REWARDS
	else
		g_paymentPreference = INSTANT_BUY_PAYMENT_TYPE.NONE	
	end
	if g_paymentPreference == INSTANT_BUY_PAYMENT_TYPE.NONE then
		showPurchaseTypeMenu()
	end
    checkPurchasePreference()
end

function ParseSetPreferences(strXmlData)
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local code = ""  		-- return code
	s, strPos, code = string.find(strXmlData, "<ReturnCode>(.-)</ReturnCode>")
end

function sendItemsPurchasedEvent(iGlids)
	local ev = KEP_EventCreate( ITEM_PURCHASED_EVENT )
	local num_items = #iGlids
	KEP_EventEncodeNumber(ev, num_items)
	for i,v in ipairs(iGlids) do 
    	KEP_EventEncodeNumber( ev, v)
	end
	KEP_EventQueue( ev )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	hideMenu()
	local g_x_pos = Dialog_GetScreenWidth(gDialogHandle) - Dialog_GetWidth( gDialogHandle )
	MenuSetLocationThis(g_x_pos, 305)
	getInstantBuyPaymentSettings()
    MenuSetMinimizedThis(true)
end

function hideMenu()

	 -- Purchase this item menu
     hideControl("btnClose2")
     hideControl("stTitlePurchaseCredRewards")
     hideControl("btnCredits")
     hideControl("stOr1")
     hideControl("btnRewards")
     hideImage("imgPurchase")
     hideImage("imgBottomPurchase")
     hideImage("imgTopPurchase")
     hideImage("imgBottomPurchase")
     
     -- Not enough
     hideControl("stMsgTitle")
     hideImage("imgTopNotEnough")
	 hideControl("stMsgBody")
	 hideControl("btnBuyNow")
	 hideControl("btnBuyMore")
	 hideControl("stOr2")
	 
	 hideImage("imgNotEnough")
	 hideImage("imgBottomNotEnough")
	 
	 hideControl("stMsgTitle")
	 hideControl("stNudge")
	 hideImage("imgNudge")
	 hideControl("btnClose1")  
	 hideImage("imgBottomNudge")
	 
	 hideControl("rbCreditsAlways")
	 hideControl("rbRewardsAlways")
end

function showPurchaseTypeMenu()
	showControl("btnClose2")
	showImage("imgTopPurchase")
	showImage("imgPurchase")
	showControl("stTitlePurchaseCredRewards")
	showControl("btnCredits")
	showControl("stOr1")
	showControl("btnRewards")
	showImage("imgBottomPurchase")
	showControl("rbCreditsAlways")
	showControl("rbRewardsAlways")
end

function showNotEnoughMoneyTypeMenu(strCreditType)
	 showControl("btnClose1")
     showControl("stMsgTitle")
     showImage("imgTopNotEnough")
	 showControl("stMsgBody")
	 showControl("btnBuyNow")
	 showControl("btnBuyMore")
	 showControl("stOr2")
	 
	 showImage("imgNotEnough")
	 showImage("imgBottomNotEnough")
	 
	 showControl("stMsgTitle")
	 showControl("stNudge")
	 showImage("imgNudge")  
	 showImage("imgBottomNudge")
	 
	 hideControl("rbCreditsAlways")
	 hideControl("rbRewardsAlways")
	 
	 local st = Dialog_GetStatic(gDialogHandle, "stMsgBody")
	 if st ~= nil then
	 	if strCreditType == "credits" then
	    	Static_SetText(st, "You don't have enough Credits to purchase this item.")
			st = Dialog_GetStatic(gDialogHandle, "stMsgTitle")
			if st ~= nil then
				Static_SetText(st, "Not Enough Credits")
			end
	 	else
	 	    Static_SetText(st, "You don't have enough Rewards to purchase this item.")
			st = Dialog_GetStatic(gDialogHandle, "stMsgTitle")
			if st ~= nil then
				Static_SetText(st, "Not Enough Rewards")
			end
	 	end
	end
end

function hideControl(ctrlName)
	Control_SetVisible(Dialog_GetControl(gDialogHandle, ctrlName), false)	
end

function showControl(ctrlName)
	Control_SetVisible(Dialog_GetControl(gDialogHandle, ctrlName), true)	
end

function hideImage(ctrlName)
	Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, ctrlName)),false)
end

function showImage(ctrlName)
	Control_SetVisible(Image_GetControl(Dialog_GetImage(gDialogHandle, ctrlName)),true)
end

function btnClose1_OnButtonClicked(buttonHandle)
     MenuCloseThis()
end

function btnClose2_OnButtonClicked(buttonHandle)
     MenuCloseThis()
end

function getInstantBuyPaymentSettings()
	g_parsePaymentSettings = PARSE_GET_PREFERENCES
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..GET_USER_PREFERENCES, WF.PLAYER_PREFERENCES)
end

function saveInstantBuyPaymentSettings()
	g_parsePaymentSettings = PARSE_SAVE_PREFERENCES
    local chkCreditsAlways = RadioButton_GetCheckBox(Dialog_GetRadioButton(gDialogHandle, "rbCreditsAlways"))
    local chkRewardsAlways = RadioButton_GetCheckBox(Dialog_GetRadioButton(gDialogHandle, "rbRewardsAlways"))
    local currency = "NONE"
	if chkCreditsAlways ~= nil and chkRewardsAlways ~= nil then
    	if CheckBox_GetChecked(chkCreditsAlways) == 1 then
           currency = "KPOINT"
		elseif CheckBox_GetChecked(chkRewardsAlways) == 1 then
	       currency = "GPOINT"
		else
	    	currency = "NONE"
		end
    	makeWebCall( GameGlobals.WEB_SITE_PREFIX..SET_USER_PREFERENCES..currency, WF.PLAYER_PREFERENCES)
    end
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
