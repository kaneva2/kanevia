--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\ObjectType.lua")

function getDropDistance()
	local config = KEP_ConfigOpenBuild()
	local retValue = 0
	if config ~= nil and config ~= 0 then
		local dropDistanceOk, dropDistance = KEP_ConfigGetNumber( config, "DropDistance2", 0 )
		if dropDistanceOk == 1 then
			retValue = dropDistance
		end
	end
	return retValue
end

function placeObjectInFrontOfPlayer(glid, invType, useType)
	-- Note this does not work for sounds, so use the old function
	if useType == USE_TYPE.SOUND then
		KEP_UseInventoryItem(glid, invType)
	else
		-- calculate object position and rotation based on player
		local targetPosX, targetPosY, targetPosZ, targetRot = KEP_GetPlayerPosition()
		targetRot = 360 - targetRot - 90
		targetRot = targetRot * (math.pi / 180.0)
		local objectRot = targetRot + math.pi
		local placeDistance = getDropDistance()
		local dirX = math.cos(targetRot)
		local dirZ = math.sin(targetRot)
		local objXDir = math.cos(objectRot)
		local objZDir = math.sin(objectRot)
					
		-- Determine how far away from player to place object by testing for walls and other objects at various heights
		local testHeight
		for testHeight = 0, 6, 2 do
			local type, id, intX, intY, intZ, intDist = KEP_RayTrace(targetPosX, targetPosY + testHeight, targetPosZ, dirX, 0, dirZ, ObjectType.WORLD + ObjectType.DYNAMIC)
			if intDist < placeDistance then
				-- something is between object and where we want to put it
				placeDistance = intDist
			end
		end
		
		-- move object away from player by placeDistnace
		targetPosX = targetPosX + (placeDistance * dirX)
		targetPosZ = targetPosZ + (placeDistance * dirZ)
		
		-- place the object
		KEP_PlaceDynamicObjectAtPosition(glid, invType, targetPosX, targetPosY, targetPosZ, objXDir, 0, objZDir)
	end
end
