--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------------
--
-- Web Query Suffixes
--
----------------------------------------------
dofile("..\\Scripts\\GameGlobals.lua")

GAMES_LIST_SUFFIX = "kgp/gamelist.aspx?gzip=true"
APP_INFO_SUFFIX = "kgp/appInfo.aspx?gzip=true&action=GetAppInfo&gameId="

USERID_SUFFIX = "kgp/userProfile.aspx?gzip=true&action=getUserIdFromAvatarName&avatar="
LOGIN_INFO_SUFFIX = "kgp/userProfile.aspx?gzip=true&action=getUserLoginInfoFromUsername&avatar="
BALANCE_SUFFIX = "kgp/userProfile.aspx?gzip=true&action=getUserBalance&userId="
WEBCALL_USER_BALANCE = GameGlobals.WEB_SITE_PREFIX.."kgp/userProfile.aspx?gzip=true&action=getUserBalance&userId="
BALANCE_SINCE_LAST_LOGIN_SUFFIX = "kgp/userProfile.aspx?gzip=true&action=getUserBalanceLastLogin&userId="
GET_USER = "kgp/userProfile.aspx?gzip=true&action=getUser&userId="
BLOCK_USER_SUFFIX = "kgp/blockUser.aspx?gzip=true&action=Block&source=wok&userId="
UNBLOCK_USER_SUFFIX = "kgp/blockUser.aspx?gzip=true&action=UnBlock&source=wok&userId="
LIST_BLOCKED_USERS_SUFFIX = "kgp/blockUser.aspx?gzip=true&action=List"
GET_USER_BLOCK_DATA_SUFFIX = "kgp/blockUser.aspx?gzip=true&action=GetUserBlockData&userId="
WEB_FEEDBACK_SUFFIX = "suggestions.aspx?mode=F&category=Feedback"

DESIGNER_COLLADA_URL = "http://designer.kaneva.com/stepbystep.html#collada"

PROFILE_COUNTS_SUFFIX = "kgp/userProfile.aspx?gzip=true&action=getMyCounts"
PEOPLE_SUFFIX = "people/people.kaneva"
COMMUNITIES_SUFFIX = "community/channel.kaneva"
GET_COMMUNITY_SUFFIX = "kgp/getCommunity.aspx?action=getCommunityByNames&cNames="
GET_ACCESS_SUFFIX = "kgp/userAccess.aspx?gzip=true&action=GetAccessRights"
SET_ACCESS_SUFFIX = "kgp/userAccess.aspx?gzip=true&action=SetAccess&access="
PROFILE_EDIT_SUFFIX = "community/ProfileEdit.aspx?gzip=true&communityId="

RAVE_SUFFIX = "kgp/ravePlayer.aspx?gzip=true&userId="
RAVE_INFORMATION_SUFFIX = "kgp/ravePlayer.aspx?gzip=true&action=GetRaveInformation"
GET_RAVES_SUFFIX = "kgp/ravePlace.aspx?gzip=true&action=GetRaves"
SET_RAVES_SUFFIX = "kgp/ravePlace.aspx?gzip=true&action=SetRave"
REQUEST_ACCESS_SUFFIX = "kgp/ravePlace.aspx?gzip=true&action=RequestAccess&communityId="
GET_PLACE_INFORMATION_SUFFIX = "kgp/ravePlace.aspx?gzip=true&action=GetPlaceInformation"

GET_LINKED_ZONES_SUFFIX = "kgp/worldInfo.aspx?action=GetWorldZones"
BUY_LINKED_ZONE_SLOT = "kgp/worldInfo.aspx?action=BuyZoneSlot"
DELETE_LINKED_ZONE = "kgp/worldInfo.aspx?action=DeleteWorld"

GET_WORLD_TEMPLATE_ID = "kgp/worldInfo.aspx?action=GetWorldTemplate&"
GET_URL_SUFFIX = "kgp/worldInfo.aspx?action=GetWorldUrl"

GET_ITEM_RAVES_SUFFIX = "kgp/raveObject.aspx?gzip=true&action=GetRaves"
SET_ITEM_RAVES_SUFFIX = "kgp/raveObject.aspx?gzip=true&action=SetRave"

TRAVEL_LIST_SUFFIX = "kgp/travelzonelist.aspx?gzip=true&"
WEBCALL_GAME_NOTIFICATION_COUNT = GameGlobals.WEB_SITE_PREFIX .. "kgp/travelzonelist.aspx?gzip=true&action=GameNotificationCount"
WEBCALL_MOST_VISITED_100 = GameGlobals.WEB_SITE_PREFIX .. "kgp/travelzonelist.aspx?gzip=true&action=getMostVisited&constrainCount=100"
WEBCALL_RANDOM_ONLINE_FRIENDS = GameGlobals.WEB_SITE_PREFIX .. "kgp/travelzonelist.aspx?gzip=true&action=getRandomOnlineFriends"

GET_PREMIUM_ITEMS_SUFFIX = "kgp/premiumitems.aspx?type="
WEBCALL_GET_AVAILABLE_CREDIT_REDEMPTIONS = GameGlobals.WEB_SITE_PREFIX .. "kgp/premiumitems.aspx?action=getAvailableCreditRedemptions"

BUY_CREDITS_SUFFIX = "myKaneva/managecredits.aspx"
BUY_CREDIT_PACKAGES_SUFFIX = "mykaneva/buyCreditPackages.aspx"
TRANSACTION_LOG_SUFFIX = "mykaneva/transactions.aspx"
MESSAGE_CENTER_SUFFIX = "myKaneva/mailbox.aspx"
MY_KANEVA_SUFFIX = "default.aspx"
MY_PROFILE_SUFFIX = "channel/channelPage.aspx"
MY_PROFILE_EDIT_SUFFIX = "community/ProfilePage.aspx"
SHOPPING_PREFIX = "http://pv-shopping." -- You probably want to use GameGlobals.WEB_SITE_PREFIX_SHOP instead
SHOPPING_EMOTES_SUFFIX = "CatalogLanding.aspx?cId=73"
SHOPPING_CLOTHING_SUFFIX = "CatalogLanding.aspx?cId=3"
IMAGE_DETAILS_SUFFIX = "ItemDetails.aspx?gId="
MY_MEDIA_LIBRARY_SUFFIX = "asset/publishedItemsNew.aspx"

WORLD_ITEM_CONVERSION_SUFFIX = "kgp/worlditemconversion.aspx"

USERID_FRIENDS_SUFFIX = "kgp/friendsList.aspx?gzip=true&userId="
WEBCALL_DAILY_BONUS_DATA = GameGlobals.WEB_SITE_PREFIX.."kgp/userProfile.aspx?gzip=true&action=getDailyBonusData"
WEBCALL_DAILY_BONUS_RESET = GameGlobals.WEB_SITE_PREFIX.."kgp/userProfile.aspx?gzip=true&action=resetDailyBonus"
USER_PROFILE_SUFFIX = "kgp/userProfile.aspx?gzip=true&action=getUserProfile&userId="
PROFILE_SUFFIX = "kgp/userProfile.aspx?gzip=true&action=getUserIdFromId&userId="
ABOUT_SUFFIX = "kgp/userProfile.aspx?gzip=true&action=getUserInterests&userId="
COUNTS_SUFFIX = "kgp/userProfile.aspx?gzip=true&action=getMyCounts&userId="
WEBCALL_USER_COUNTS = GameGlobals.WEB_SITE_PREFIX .. "kgp/userProfile.aspx?gzip=true&action=getMyCounts&userId="
GET_USER_PREFERENCES = "kgp/userProfile.aspx?gzip=true&action=getUserPreferences"
SET_USER_PREFERENCES = "kgp/userProfile.aspx?gzip=true&action=setUserPreferences&currency="
WEB_CHANNEL_PATH = "channel/"
RAVE_SORT_SUFFIX = "&sortbyraves=T"

BUY_ACCESS_PASS_SUFFIX = "myKaneva/buyaccess.aspx"
BUY_VIP_PASS_SUFFIX = "mykaneva/passDetailsVIPb.aspx?pass=true&passId=74"

NEW_AVATAR_SUFFIX = "kgp/newAvatar.aspx?gzip=true&action=NewAvatar"
INTEREST_SUFFIX = "people/people.kaneva?int="

AD_BANNER_SUFFIX = "3d-virtual-world/virtual-life.kaneva"
FAME_SUFFIX = "3d-virtual-world/world-of-kaneva-fame.kaneva"
BUY_SPECIAL_SUFFIX = "myKaneva/buycredits.aspx"
DANCEFAME_BLAST_SUFFIX = "kgp/userProfile.aspx?gzip=true&action=sendDanceFameBlast&text=New Dance Fame Level!"

REDEEM_PACKET_SUFFIX = "kgp/redeemPacket.aspx?gzip=true&action=redeemPacket"
GET_USER_FAME_SUFFIX = "kgp/fameInfo.aspx?gzip=true&action=GetUserFame"
GET_LEVEL_AWARDS_SUFFIX = "kgp/fameInfo.aspx?gzip=true&action=GetLevelAwards"

VISIT_PLACE_SUFFIX = "kgp/visitPlace.aspx?gzip=true&action=visitPlace"
VISIT_DANCEFLOOR_SUFFIX = "kgp/visitPlace.aspx?gzip=true&action=visitDanceFloor"

DANCE_TO_LEVEL10 = "kgp/userProfile.aspx?gzip=true&action=danceToLevel10"
KACHING_MAIN_SUFFIX = "3d-virtual-world/kaneva-ka-ching.kaneva"
KACHING_HOWTOPLAY_SUFFIX = "3d-virtual-world/play-ka-ching.kaneva"
KACHING_STATS_SUFFIX = "3d-virtual-world/ka-ching-leaderboard.kaneva"

VENDOR_INVENTORY_SUFFIX = "kgp/vendorInventory.aspx?gzip=true&storeId="
PLAYER_INVENTORY_SUFFIX = "kgp/inventory.aspx?gzip=true&action=Player&sort="
BANK_INVENTORY_SUFFIX = "kgp/inventory.aspx?gzip=true&action=Bank&sort="
PLAYER_INVENTORY_CHECK_SUFFIX = "kgp/inventory.aspx?action=Player&use_type="
SNAPSHOT_GAME_ITEM_TO_INVENTORY = "kgp/snapshotGameItems.aspx?gameItems="
GET_MAX_GAME_ITEM_LOAD = "kgp/scriptGameItems.aspx?action=getGlobalConfig&propertyName="
TRADE_ITEMS_SUFFIX = "kgp/tradeItems.aspx?gzip=true"
ITEM_ANIMATIONS_SUFFIX = "kgp/getitemanimations.aspx?gzip=true&id="

CREATE_EVENT_SUFFIX = "mykaneva/events/eventForm.aspx" --"kgp/eventCreate.aspx"
DYNAMIC_OBJECT_INFO_SUFFIX = "kgp/instantBuy.aspx?gzip=true&operation=object"
BUY_DYNAMIC_OBJECT_SUFFIX = "kgp/instantBuy.aspx?gzip=true&operation=buy"
PLAYERS_ARMED_ITEM_LIST_SUFFIX = "kgp/instantBuy.aspx?gzip=true&operation=list"
TRY_ON_ITEM_SUFFIX = "kgp/instantBuy.aspx?gzip=true&operation=try"

WHAT_ARE_PATTERNS_SUFFIX = "3d-virtual-world/create-3-d-objects.kaneva"
UPLOAD_PATTERNS_SUFFIX = "mykaneva/upload.aspx"

SEND_CHAT_BLAST_SUFFIX = "kgp/sendBlasts.aspx?gzip=true&"

GET_FRIEND_IMAGE_URL_SUFFIX = "kgp/getImageUrl.aspx?gzip=true&userId="
GET_FRIEND_THUMBNAIL_URL_SUFFIX = "kgp/getImageUrl.aspx?gzip=true&thumb=1&userId="
WEBCALL_USER_THUMBNAIL = GameGlobals.WEB_SITE_PREFIX .. "kgp/getImageUrl.aspx?gzip=true&thumb=1&userId="

GET_MY_3DAPPS_SUFFIX = "kgp/get3DAppList.aspx?gzip=true"

CATALOG_SUFFIX = "Catalog.aspx?"
CATALOG_LANDING_SUFFIX = "CatalogLanding.aspx?"


GET_TEMPLATE_LIST = "kgp/worldTemplates.aspx?gzip=true&action=getTemplateList"
WEBCALL_DEFAULT_HOME_TEMPLATE = GameGlobals.WEB_SITE_PREFIX.."kgp/worldTemplates.aspx?gzip=true&action=getDefaultHomeTemplate"

CREATE_WORLD_SUFFIX   = "kgp/createWorld.aspx?gzip=true&"
VALIDATE_EMAIL_SUFFIX = "myKaneva/general.aspx?"

TRACKING_REQUEST_SUFFIX = "/kgp/tracking.aspx?gzip=true&action=InsertTrackingRequest&route=true&requestTypeId="
TRACKING_REQUEST_SENT_SUFFIX = "/kgp/tracking.aspx?gzip=true&action=InsertTrackingTypeSent&messageTypeId="
TRACKING_REQUEST_ACCEPT_SUFFIX = "/kgp/tracking.aspx?gzip=true&action=AcceptTrackingRequest&requestTypeSentId="
TRACKING_REQUEST_GAMEID_PARAM = "&gameId="
TRACKING_REQUEST_REQUESTID_PARAM = "&requestId="
TRACKING_REQUEST_USERID_PARAM = "&userId="
TRACKING_REQUEST_GAME_CLIENT = "2"
TRACKING_REQUEST_INWORLD_INVITE = "4"

WEBCALL_SCRIPT_GAME_ITEM_METRICS = GameGlobals.WEB_SITE_PREFIX .. "kgp/scriptGameItemMetrics.aspx?action="

WEBCALL_PROGRESS_METRICS = GameGlobals.WEB_SITE_PREFIX.."kgp/userProfile.aspx?gzip=true&action=progressMetrics"

WEBCALL_NEW_USER_FUNNEL_PROGRESSION = GameGlobals.WEB_SITE_PREFIX .. "kgp/recordNewUserFunnel.aspx?funnelStep="

WEBCALL_GET_PLAYER_INFO = GameGlobals.WEB_SITE_PREFIX .. "kgp/getPlayerInfo.aspx?action="

