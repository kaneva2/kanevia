--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("PopupMenuHelper.lua")
dofile("UGCFunctions.lua")
dofile("..\\MenuScripts\\UGCZoneLiteProxy.lua")

designIcons =
{
	"imgDesignBG",
	"imgImport",
	"btn3DObjects",
	"btnAccessories",
	"btnEmotes",
	"btnClothing",
	"btnAnimation",
	"btnSoundEffect",
	"btnImportZone",
	"imgManage",
	"btnMedia",
	"btnParticles",
	"btnDesignHelp",
	"img3DObjectsRollover",
	"imgAccessoriesRollover",
	"imgEmotesRollover",
	"imgClothingRollover",
	"imgAnimationRollover",
	"imgSoundEffectRollover",
	"imgImportZoneRollover",
	"imgMediaRollover",
	"imgParticlesRollover",
	"imgDesignHelpRollover"
}

grayOutIcons = 
{
	"btnZones",
	"btnMenus",
	"btnMenuScripts",
	"btnServerScripts",
	"btnPublish"
}

grayOutIconRollovers = 
{
	"imgZonesRollover",
	"imgMenusRollover",
	"imgMenuScriptsRollover",
	"imgServerScriptsRollover",
	"imgPublishRollover"	
} 

designHighlights = 
{
	"img3DObjectsRollover",
	"imgAccessoriesRollover",
	"imgEmotesRollover",
	"imgClothingRollover",
	"imgAnimationRollover",
	"imgSoundEffectRollover",
	"imgImportZoneRollover",
	"imgMediaRollover",
	"imgParticlesRollover",
	"imgDesignHelpRollover"
}

developIcons = 
{
	"btnMyWorlds",
	"btnObjects",
	"btnZones",
	"btnMenus",
	"btnMenuScripts",
	"btnServerScripts",
	"btnRefresh",
	"btnPublish",
	"imgMyWorldsRollover",
	"imgObjectsRollover",
	"imgZonesRollover",
	"imgMenusRollover",
	"imgMenuScriptsRollover",
	"imgServerScriptsRollover",
	"imgRefreshRollover",
	"imgPublishRollover",
	"imgDevelopHelpRollover"
}

developerHighlights = 
{
	"imgMyWorldsRollover",
	"imgObjectsRollover",
	"imgZonesRollover",
	"imgMenusRollover",
	"imgMenuScriptsRollover",
	"imgServerScriptsRollover",
	"imgRefreshRollover",
	"imgPublishRollover",
	"imgDevelopHelpRollover"
}

CHANGE_INVENTORY_TAB_EVENT = "ChangeInventoryTabEvent"
FILE_BROWSER_CLOSED_EVENT = "FileBrowserClosedEvent"
REFRESH_EVENT = "RefreshEvent"
IMPORT_ZONE_EVENT = "ImportZoneEvent"
FILE_BROWSER_CLOSED_EVENT = "FileBrowserClosedEvent"
ENTER_BUILD_MODE_EVENT = "EnterBuildModeEvent"

canDevelop = false
designON = false 
developON = false 
creatormenuX= 0
creatormenuY = 0
screenWidth = 0
menuWidth = 0
screenHeight = 0
menuHeight = 0
newScreenWidth = 0
newScreenHeight = 0
ratioX = 0
ratioY = 0

gNextDropId = 1000000

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( CHANGE_INVENTORY_TAB_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( REFRESH_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( IMPORT_ZONE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( FILE_BROWSER_CLOSED_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "ZoneLoadedEvent", KEP.MED_PRIO )
	KEP_EventRegister( ENTER_BUILD_MODE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "HUDClosedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "CreateZoneMenuSetupEvent", KEP.MED_PRIO )

	UGCZoneLiteProxy_InitializeKEPEvents(dispatcher, handler, debugLevel)
end

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "FileBrowserClosedHandler", FILE_BROWSER_CLOSED_EVENT, KEP.HIGH_PRIO )	
	KEP_EventRegisterHandler( "ZoneLoadedEventHandler", "ZoneLoadedEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "HUDClosedEventHandler", "HUDClosedEvent", KEP.HIGH_PRIO )

	UGCZoneLiteProxy_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) 
	
	Control_SetVisible(gHandles["btnClose"],true)

	--Menu positioning 
	screenWidth = Dialog_GetScreenWidth(dialogHandle) 
	menuWidth = Dialog_GetWidth(dialogHandle)
	screenHeight = Dialog_GetScreenHeight(dialogHandle) 
	menuHeight = Dialog_GetHeight(dialogHandle)
	
	-- Can User Develop?
	canDevelop = ((KEP_IsOwnerOrModerator() and KEP_IsWorld()) or (DevMode() > 0) or KEP_IsDev())
	
	-- If Not Grey Out Develop Icons
	--3/10/14: Develop Tab only shows up in Develop Worlds(Previously Worlds/3DApps), not in Worlds(Previously Communities/Homes/Hangouts)
	if not canDevelop then 
	
		for i,v in ipairs(grayOutIcons) do
			Control_SetEnabled(gHandles[v],false)
		end 			
	 
		Control_SetVisible(gHandles["btnDevelop"],false)
		Control_SetVisible(gHandles["imgDevelopBG"],false)
		Control_SetVisible(gHandles["btnDevelopPressed"],false)
		table.insert(developIcons, "btnBuildHelp")
	else
		table.insert(developIcons, "btnDevelopHelp")
	end 

	Control_SetVisible(gHandles["btnBuildHelp"], not canDevelop)
	Control_SetVisible(gHandles["btnDevelopHelp"], canDevelop)
	
	PopupMenuHelper_AdditionalHighlightsOff(designHighlights)
	PopupMenuHelper_AdditionalHighlightsOff(developerHighlights)
	
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber( config, "CreatorToolbox", 1 )
		KEP_ConfigSave( config )
		
		local ToolBoxXOK, ToolWidgetX =  KEP_ConfigGetNumber( config, "ToolboxX", widgetX)
		local ToolBoxYOK, ToolWidgetY =  KEP_ConfigGetNumber( config, "ToolboxY", 100)
		local ratioCTXOK, ratioCTX =  KEP_ConfigGetNumber( config, "CreatorMenuRatioX", ratioX)
		local ratioCTYOK, ratioCTY =  KEP_ConfigGetNumber( config, "CreatorMenuRatioY", ratioY)
		local buttonStateOK, buttonState =  KEP_ConfigGetString( config, "ButtonState", "Design")
		
		if buttonStateOK== 1 then 
			if buttonState == "Develop" then
				if not canDevelop then 
					btnDesign_OnButtonClicked(gHandles["btnDesign"])	
					Control_SetVisible(gHandles["imgDevelopBG"],false)
					Control_SetVisible(gHandles["btnDevelopPressed"],false)
				else 
					btnDevelop_OnButtonClicked(gHandles["btnDevelop"])
					Control_SetVisible(gHandles["imgDevelopBG"],true)
					Control_SetVisible(gHandles["btnDevelopPressed"],true)
				end 
			else
				btnDesign_OnButtonClicked(gHandles["btnDesign"])
			end 
		else 
			btnDesign_OnButtonClicked(gHandles["btnDesign"])
		end
		
		if ratioCTXOK== 1 then 
			ratioX = ratioCTX
			creatormenuX = ratioX * screenWidth
		else 
			creatormenuX = ToolWidgetX - Dialog_GetWidth( dialogHandle )
			ratioX  = creatormenuX/screenWidth
		end
		
		if ratioCTYOK == 1 then 
			ratioY = ratioCTY
			creatormenuY = ratioY * screenHeight
		else 
			creatormenuY = ToolWidgetY 
			ratioY  = creatormenuY/screenHeight
		end 
	end
	Dialog_SetLocation(dialogHandle, creatormenuX, creatormenuY) 
	writetoConfig(ratioX,ratioY)
end 

function enterBuild()
	if not KEP_IsOwnerOrModerator() then return end

	local ev = KEP_EventCreate( "EnterBuildModeEvent")
	KEP_EventSetFilter(ev, 1)
	KEP_EventEncodeNumber(ev, MODE_RETAIN)
	KEP_EventQueue( ev)
end

function KEP_IsDev()
	return false
end 

function HUDClosedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	MenuCloseThis()
end 

function ZoneLoadedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	MenuCloseThis()
end

function btnClose_OnButtonClicked(buttonHandle) 
	
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetNumber( config, "CreatorToolbox", 0)
		KEP_ConfigSave( config )
	end

	MenuCloseThis()
end 

function Dialog_OnMoved(dialogHandle)
	--Shift menu based on window resizing 
	local newScreenWidth = Dialog_GetScreenWidth(dialogHandle)
	local newScreenHeight = Dialog_GetScreenHeight(dialogHandle)
		
	if newScreenWidth ~= screenWidth then
		if creatormenuX > (newScreenWidth - menuWidth) then
			creatormenuX = newScreenWidth - menuWidth
		else
			if creatormenuX < 0 then 
				creatormenuX = 0
			else
				creatormenuX = ratioX * newScreenWidth
			end
		end
		screenWidth = newScreenWidth
	end 

	if newScreenHeight ~= screenHeight then 
		if creatormenuY > (newScreenHeight - menuHeight - 65) then 
			creatormenuY = newScreenHeight - menuHeight - 65 
		else 
			if creatormenuY < 0 then 
				creatormenuY = 0
			else
				creatormenuY = ratioY * newScreenHeight
			end
		end
		screenHeight = newScreenHeight
	end 

	Dialog_SetLocation(dialogHandle, creatormenuX, creatormenuY) 
	writetoConfig(ratioX,ratioY)
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	PopupMenuHelper_OnMouseMove(dialogHandle, x, y, true)
	if designON then 
		PopupMenuHelper_AdditionalHighlightsMouseMove(designHighlights, x, y) 
	elseif developON then 
		PopupMenuHelper_AdditionalHighlightsMouseMove(developerHighlights, x, y)
	end 
	
	--No Rollovers for grayed out icons
	if not canDevelop then 
		PopupMenuHelper_AdditionalHighlightsOff(grayOutIconRollovers)
	end
	--Moving box within borders of client window
		
	local is_down = Dialog_IsLMouseDown( dialogHandle )
	
	if is_down then 
		if creatormenuX < 0 then 
			creatormenuX = 0
		else
			if creatormenuX > (screenWidth - menuWidth) then 
				creatormenuX = screenWidth - menuWidth
			else 
			creatormenuX = Dialog_GetLocationX(dialogHandle)
			end 
		end 
		
		if creatormenuY < 0 then 
			creatormenuY = 0
		else
			if creatormenuY > (screenHeight - menuHeight - 65) then 
				creatormenuY = screenHeight - menuHeight - 65 
			else 
			creatormenuY = Dialog_GetLocationY(dialogHandle)
			end 
		end 
		
		ratioX = creatormenuX/screenWidth
		ratioY = creatormenuY/screenHeight
		
		Dialog_SetLocation(dialogHandle, creatormenuX, creatormenuY) 
		writetoConfig(ratioX,ratioY)
	end 
end 

function writetoConfig(x,y)
	--write position to config file 
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber( config, "CreatorMenuRatioX", ratioX )
		KEP_ConfigSetNumber( config, "CreatorMenuRatioY", ratioY )
		KEP_ConfigSave( config )
	end
end 

function writeButtontoConfig(state)
	--write button state to config file 
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		KEP_ConfigSetString( config, "ButtonState", state )
		KEP_ConfigSave( config )
	end
end 

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle ) 
end

function btnMinimize_OnButtonClicked(buttonHandle) 
	-- Determine if toolbox widget should show based on settings 
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		local ToolboxOk, showToolbox = KEP_ConfigGetString( config, "ShowToolBox", "true")
			
		if showToolbox == "true" then 
			MenuOpen("ToolboxWidget.xml")
		end
		KEP_ConfigSetNumber( config, "CreatorToolbox", 0)
		KEP_ConfigSave( config )
	end

	MenuCloseThis()
end

function btnDesign_OnButtonClicked(buttonHandle) 
	designON = true 
	developON = false 
	for i,v in ipairs(designIcons) do
		Control_SetVisible(gHandles[v],	true)
	end  	
	--stops highlights flashing in menu initiation
	for i,v in ipairs(designHighlights) do
		Control_SetVisible(gHandles[v],	false)
	end 
	for i,v in ipairs(developIcons) do
		Control_SetVisible(gHandles[v],	false)
	end

	if not canDevelop then 
		Control_SetVisible(gHandles["btnDevelop"],false)
		Control_SetVisible(gHandles["imgDevelopBG"],false)
		Control_SetVisible(gHandles["btnDevelopPressed"],false)
	else 
		Control_SetVisible(gHandles["btnDevelop"],true)
		Control_SetVisible(gHandles["imgDevelopBG"],true)
		Control_SetVisible(gHandles["btnDevelopPressed"],true)
	end 
	Control_SetVisible(gHandles["btnDesign"],false) 
	writeButtontoConfig("Design")
end

function btnDevelop_OnButtonClicked(buttonHandle) 
	designON = false
	developON = true 
	
	for i,v in ipairs(designIcons) do
		Control_SetVisible(gHandles[v],	false)
	end  
	
	for i,v in ipairs(developerHighlights) do
		Control_SetVisible(gHandles[v],	false)
	end
	
	for i,v in ipairs(developIcons) do
		Control_SetVisible(gHandles[v],	true)
	end  
	
	Control_SetVisible(gHandles["btnDevelop"],false) 
	Control_SetVisible(gHandles["btnDesign"],true) 
	
	writeButtontoConfig("Develop")
end

function BrowseAndImportAsync( ugcType, optionalGlid )
	local event = KEP_EventCreate( FILE_BROWSER_CLOSED_EVENT )
	if optionalGlid~=nil then
		KEP_EventSetObjectId( event, optionalGlid )
	end
	KEP_EventSetFilter( event, ugcType )
	UGC_BrowseFileByImportType( ugcType, event ) -- second param (initial folder) TBD
end

--Design Buttons 

function btn3DObjects_OnButtonClicked(buttonHandle)
	BrowseAndImportAsync( UGC_TYPE.DYNOBJ, nil )
end

function btnAnimation_OnButtonClicked(buttonHandle)
	MenuOpen("ImportAccessoryAnimation.xml")
end

function btnAccessories_OnButtonClicked(buttonHandle)
	BrowseAndImportAsync( UGC_TYPE.EQUIPPABLE, nil )
end

function btnEmotes_OnButtonClicked(buttonHandle)
	BrowseAndImportAsync( UGC_TYPE.CHARANIM, 0 )
end

function btnClothing_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_SHOP.."Upload.aspx" )
end

function btnSoundEffect_OnButtonClicked(buttonHandle)
	BrowseAndImportAsync( UGC_TYPE.SOUND, 0 )
end

function btnImportZone_OnButtonClicked(buttonHandle)
	if (KEP_bulkZoneOpsEnabled()) then
		if MenuIsClosed("CreateZone.xml") then
			MenuOpenModal("CreateZone.xml")
			setupCreateZoneMenu()
		end 
	end
end 

function setupCreateZoneMenu()

	local currentZoneIndex = KEP_GetCurrentZoneIndex()
	local currentZoneName  = KEP_GetCurrentZoneName()
	 
	Log("setupCreateZoneMenu: currentZoneIndex="..currentZoneIndex) 

	if currentZoneIndex == nil then
		LogError("setupCreateZoneMenu: ERROR(unknown zoneIndex["..tostring(currentZoneIndex).."])")
		return 
	end

	local event = KEP_EventCreate("CreateZoneMenuSetupEvent")
	KEP_EventSetFilter(event, 2)                               -- (2 import)
	KEP_EventEncodeNumber(event, 1)                            -- showNewSection       -- (1 false -- 0 true)
	KEP_EventEncodeNumber(event, 0)                            -- trackStatus          -- (0 false -- 1 true)
	KEP_EventEncodeNumber(event, 1)                            -- (1 All -- 2 ScriptedOnly --3 NonScriptedOnly)
	KEP_EventEncodeNumber(event, UGCZLPZoneTypes.CURRENT_TYPE) -- importzonetype       - (3 home / 4 zone / 5 arena / 6 channel)
	KEP_EventEncodeNumber(event, currentZoneIndex)             -- zoneIndex            -- (imports will apply to this zone)
	KEP_EventEncodeNumber(event, 1)                            -- deleteDynamicObjects -- (0 delete all dynamic objects) (1 delete only dynamic objects with NO scripts)
	KEP_EventEncodeString(event, currentZoneName)              -- zoneName             -- (for display)
	KEP_EventQueue(event)

	Log("setupCreateZoneMenu: Dispatched Request[CreateZoneMenuSetupEvent] for zoneIndex["..tostring(currentZoneIndex).."] currentZoneName["..tostring(currentZoneName).."]")
end

function btnMedia_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://www.kaneva.com/asset/publishedItemsNew.aspx")
end

function btnParticles_OnButtonClicked(buttonHandle)
	MenuOpen("ParticleBrowser.xml")
end

-- Develop Buttons 

function btnMyWorlds_OnButtonClicked(buttonHandle)
	MenuOpen("MyApps.xml")
end 

function btnObjects_OnButtonClicked(buttonHandle)
	MenuOpen("ObjectBrowser.xml")
end

function btnZones_OnButtonClicked(buttonHandle)
	MenuOpen("ZoneEditor.xml")
end

function btnServerScripts_OnButtonClicked(buttonHandle)
	MenuOpen("ServerScriptBrowser.xml")
end

function btnMenus_OnButtonClicked(buttonHandle)
	MenuOpen("MenuBrowser.xml")
end

function btnMenuScripts_OnButtonClicked(buttonHandle)
	MenuOpen("MenuScriptBrowser.xml")
end

function btnRefresh_OnButtonClicked(buttonHandle)
	local ev = KEP_EventCreate( "RefreshEvent")
	KEP_EventQueue( ev)
end 

function btnPublish_OnButtonClicked(buttonHandle)
	MenuOpen("DevToolsPublish.xml")
end

function btnDesignHelp_OnButtonClicked(buttonHandle)

	if MenuIsClosed("HUDBuild") then
		enterBuild()
	end 
	
	KEP_LaunchBrowser("http://docs.kaneva.com/mediawiki/index.php/Designer_Area")
end 

function btnDevelopHelp_OnButtonClicked(buttonHandle)

	if MenuIsClosed("HUDBuild") then
		enterBuild()
	end 

	KEP_LaunchBrowser("http://docs.kaneva.com/mediawiki/index.php/Bug_Bounty")
end 

function btnBuildHelp_OnButtonClicked(buttonHandle)

	if MenuIsClosed("HUDBuild") then
		enterBuild()
	end 

	KEP_LaunchBrowser("http://docs.kaneva.com/mediawiki/index.php/Building_Area")
end 

function FileBrowserClosedHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local dialogResult = KEP_EventDecodeNumber( event )
	if dialogResult==0 then
		return
	end
	local filePath = KEP_EventDecodeString( event )

	-- Note: This global used to be totally worthless in ImportMenu.lua,
	-- since it was reinitialized each time the menu was loaded.
	-- Now it'll actually change for each object being added. I have no
	-- idea if that's a good thing or not. -danderson 2012-03-16
	local dropId = gNextDropId
	gNextDropId = gNextDropId + 1

	local importTarget = {}
	importTarget.valid = 0
	importTarget.pos = {}
	importTarget.pos.x = 0
	importTarget.pos.y = 0
	importTarget.pos.z = 0
	importTarget.normal = {}
	importTarget.normal.x = 0
	importTarget.normal.y = 0
	importTarget.normal.z = 0
	importTarget.type = ObjectType.UNKNOWN
	importTarget.id = -1
	importTarget.name = ""

	local importInfo = {}
	importInfo.fullPath = filePath
	importInfo.sessionId = -1

	if filter==UGC_TYPE.DYNOBJ then
		importInfo.category = UGC_TYPE.DYNOBJ
		importInfo.type = UGC_TYPE.DYNOBJ
		UGCI.CallUGCHandler("DaeImport.xml", 1, dropId, importInfo, importTarget, 0, 0 )
	elseif filter==UGC_TYPE.CHARANIM then
		if objectid~=0 then
			-- Equippable animation import
			importTarget.type = ObjectType.EQUIPPABLE
			importTarget.id = objectid
		else
			importTarget.type = ObjectType.PLAYER
			importTarget.id = 0
		end
		importInfo.category = UGC_TYPE.CHARANIM
		importInfo.type = UGC_TYPE.CHARANIM
		UGCI.CallUGCHandler("DaeImport.xml", 1, dropId, importInfo, importTarget, 0, 0 )
	elseif filter==UGC_TYPE.SOUND then
		if string.lower(string.sub(filePath, -4))==".wav" then
			importInfo.category = UGC_TYPE.SOUND
			importInfo.type = UGC_TYPE.WAV_SOUND
			UGCI.ImportAndSubmitWavFile( importInfo, importTarget, 0, 0 )
		elseif string.lower(string.sub(filePath, -4))==".ogg" then
			importInfo.category = UGC_TYPE.SOUND
			importInfo.type = UGC_TYPE.OGG_SOUND
			UGCI.ImportAndSubmitOggFile(importInfo, importTarget, 0, 0 )
		else
		-- Error message (TBD)
		end
	elseif filter==UGC_TYPE.EQUIPPABLE then
		importInfo.category = UGC_TYPE.EQUIPPABLE
		importInfo.type = UGC_TYPE.EQUIPPABLE	-- actual type (rigid/skeletal) to be determined in import process
		UGCI.CallUGCHandler("DaeImport.xml", 1, dropId, importInfo, importTarget, 0, 0 )
	end
end
