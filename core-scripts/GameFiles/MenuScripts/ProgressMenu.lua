--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("ProgressMenuMessages.lua")
dofile("..\\Scripts\\PlacementSource.lua")

-- progress dialog variables
g_dialogSec = 0 -- dialog elapsed time (sec)

-- progress special events
g_special = PROGRESS_NO -- progress special event occured (error or cancel)  

-- progress option variables 
g_optCancel = PROGRESS_OPT_CANCEL_DEFAULT
g_optFade = PROGRESS_OPT_FADE_DEFAULT
g_optMsg = PROGRESS_OPT_MSG_DEFAULT

-- progress cancel variables
g_canCancel = false
g_canMsg = false

-- progress spinner variables
g_framesPerSec = 10.0 -- progress spinner frames per second

-- progress fade variables
g_fadeAlpha = 0.0 -- fade alpha current value
g_fadeAlphaTarget = 0.0 -- fade alpha target value
g_fadeAlphaInc = 5.0 -- fade alpha increment (per OnRender)
g_fadeAlphaMin = 200.0 -- fade min alpha value (0.0=clear)
g_fadeAlphaMax = 255.0 -- fade max alpha value (255.0=black)

-- progress message variables
g_msgSec = -1000.0 -- last message time (sec)
g_msgsPerSec = 0.25 -- messages per second
 
local m_frameworkEnabled = nil

local ANIMATE_SPEED = 200

local BAR_WIDTH = 314
local m_animating = false
local m_currBarWidth = 0
local m_totalBarWidth = 0
g_currProgress = 0

--constants
local OBJECT_PERCENT = 50
local ZONE_LOAD_PERCENT = 15
local FRAMEWORK_LOAD_PERCENT = 10
local FRAMEWORK_COMPLETE_PERCENT = 5
local COMPLETE_LOAD_PERCENT = 35 --When progress controller says world is finished loading. This will automatically be 100 in non game worlds

local m_fakeLoadMax = 50 --Fake pretend load max...

g_objStart = nil
g_frameworkStart = nil
g_dataStart = nil
g_zoningStart = nil

g_captureOnce = false

g_PIDTotal = 0 --total number of DO
g_PIDTable = {}
g_gIDTable = {}
local g_soundPlacements = {}

local m_ZoneLoad = false
local m_ZoneDownload = false
local m_PatchProgress = false
local m_display = false
local m_playerName = ""

local m_fakeLoadProgress = 0 --going to fake load...
local m_fakeLoadSec = 0

local m_showWelcome = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
    KEP_EventRegisterHandler("ProgressEventHandler", "ProgressEvent", KEP.MED_PRIO)
    KEP_EventRegisterHandler("CompleteLoadHandler", "CompleteLoad", KEP.HIGH_PRIO)
    KEP_EventRegisterHandler("ZoneCustomizationSummaryHandler", "ZoneCustomizationSummaryEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("ZoneCustomizationDLCompleteHandler", "ZoneCustomizationDLCompleteEvent",KEP.MED_PRIO)
    KEP_EventRegisterHandlerV("AddDynamicObjectEventHandler", "AddDynamicObjectEvent", KEP.HIGH_PRIO + 100)
    KEP_EventRegisterHandler("DynamicObjectInitializedEventHandler", "DynamicObjectInitializedEvent", KEP.HIGH_PRIO)
    KEP_EventRegisterHandler("DynamicObjectInitDelayedEventHandler", "DynamicObjectInitDelayedEvent", KEP.HIGH_PRIO)
    KEP_EventRegisterHandler("DynamicObjectLoadingFailedEventHandler", "DynamicObjectLoadingFailedEvent", KEP.HIGH_PRIO)
    KEP_EventRegisterHandler("zoneNavigationEventHandler", "ZoneNavigationEvent", KEP.MED_PRIO)
    KEP_EventRegisterHandler("AddSoundEventHandler", "AddSoundEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("frameworkComplete", "FRAMEWORK_LOAD_COMPLETE", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
    KEP_EventRegister("ProgressEvent", KEP.MED_PRIO)
    KEP_EventRegister( "ZoneNavigationEvent",       KEP.MED_PRIO )
	KEP_EventRegister("FRAMEWORK_LOAD_COMPLETE", KEP.MED_PRIO )
end

function Dialog_OnExit(dialogHandle)
end

function Dialog_OnCreate(dialogHandle)
    Helper_Dialog_OnCreate(dialogHandle) 

	-- Tell engine that zoning progress UI is active
	KEP_SetZoneLoadingUIActive(1)

	-- Initialize the progress menu tips system
	ProgressMenuMessages:init()

    -- Capture All Keys During Progress
    MenuSetCaptureKeysThis(true)

	-- Disable some UI components if we are visualizing downloading progress
	if KEP_IsDownloadPriorityVisualizationEnabled()~=0 then
		Dialog_SetModal(dialogHandle, false)
		Control_SetVisible(Dialog_GetControl(dialogHandle, "imgBackground"), false)
		Control_SetVisible(Dialog_GetControl(dialogHandle, "imgFade"), false)
		SetFadeCoords = function() end
		SetFadeAlpha = function() end
		MenuSetCaptureKeysThis(false)
	end
    
    -- Disable Metrics During Progress
    KEP_MetricsEnable(0)

    -- Flag Menu Opened
    ProgressOpened()
    
    -- Always Bring To Front This Menu
    MenuAlwaysBringToFrontThis()
    
    -- Reset Fade Coordinates
    SetFadeCoords()

    -- Enable Debug ?
    if (g_progressDebug) then
        g_optMsg = PROGRESS_OPT_MSG_DEBUG
    end

    m_playerName     = KEP_GetLoginName()
	
	Events.registerHandler("FRAMEWORK_ACTIVATE_PLAYER",  frameworkProgress)

    --Get welcome screen settings
    Events.registerHandler("FRAMEWORK_RETURN_WELCOME", returnWelcomeHandler)
	
	local currTime = GetTimeMs()
	g_zoningStart = currTime
	
	g_zoneDataStart = currTime

    -- Test Special Events
    --g_special = PROGRESS_ERROR
    --SetMsgSpecial("Error Test - Click Close To Go Home")
end

function Dialog_OnDestroy(dialogHandle)

	-- Tell engine that zoning progress UI is closed
	KEP_SetZoneLoadingUIActive(0)

    -- Enable Cursor
    Cursor_SetEnabled(1)

    Helper_Dialog_OnDestroy(dialogHandle)

    -- Re-Enable Metrics 
    KEP_MetricsEnable(1)

    Events.sendEvent("FRAMEWORK_LOADING_COMPLETE", {player = m_playerName, tutorial=false})

    -- Flag Menu Closed
    ProgressClosed()
end

function Dialog_OnMoved(dialogHandle, x, y)

    -- Reset Fade Coordinates
    SetFadeCoords()
end

function Dialog_OnRender(dialogHandle, elapsedSec)
    
    -- Update Dialog Time
    g_dialogSec = g_dialogSec + elapsedSec

     -- Update Spinner Frame (no special event occured) or Flash Message & Cancel (special event occured)
    if (g_special == PROGRESS_NO) then
        SetSpinnerFrame(math.floor(g_dialogSec * g_framesPerSec))
    else
        local blinkRate = math.floor(g_dialogSec * 2.0) -- 2 flashes per second
        local blinkRateHalf = math.floor(blinkRate / 2)
        local blink = ((blinkRateHalf * 2) == blinkRate)
        SetCancelAlpha(blink and 255 or 0)
        SetSpinnerSpecial()
    end
    
    -- Update Fade Alpha (settle on target value)
    if(g_optFade == PROGRESS_OPT_FADE_MAX) then
        g_fadeAlphaTarget = g_fadeAlphaMax
    elseif(g_optFade == PROGRESS_OPT_FADE_MIN) then
        g_fadeAlphaTarget = g_fadeAlphaMin
    end
    if (g_fadeAlpha < g_fadeAlphaTarget) then
        g_fadeAlpha = g_fadeAlpha + g_fadeAlphaInc
        if (g_fadeAlpha > g_fadeAlphaTarget) then 
            g_fadeAlpha = g_fadeAlphaTarget
        end
    end
    if (g_fadeAlpha > g_fadeAlphaTarget) then
        g_fadeAlpha = g_fadeAlpha - g_fadeAlphaInc
        if (g_fadeAlpha < g_fadeAlphaTarget) then 
            g_fadeAlpha = g_fadeAlphaTarget
        end
    end
    SetFadeAlpha(g_fadeAlpha)
    
   if m_animating then
        if m_currBarWidth <=  m_totalBarWidth then
            local barHeight = Control_GetHeight(gHandles["imgLoadingBar"])
            m_currBarWidth = math.min(m_totalBarWidth, m_currBarWidth + (elapsedSec*ANIMATE_SPEED))
            
			-- DRF - Progress Bar Overshoot Bug Fix
			local barWidth = math.min(m_currBarWidth, BAR_WIDTH)
			Control_SetSize(gHandles["imgLoadingBar"], barWidth, barHeight)
           
            if m_currBarWidth >= BAR_WIDTH or (((BAR_WIDTH - m_currBarWidth) <= 10) and (100 - g_currProgress <= 3)) then
                m_animating = false
                OpenWelcomeMenu()
				captureZoningMetricComplete()
                CloseProgressMenu("Completed")
                KEP_EventCreateAndQueue("CloseProgressMenu")
            end
        end
    end


    if m_fakeLoadMax > m_fakeLoadProgress and ((m_fakeLoadSec == 0) or (g_dialogSec > (m_fakeLoadSec + 1))) then
        m_fakeLoadProgress = m_fakeLoadProgress + 1
        updateProgressBar(0)
        m_fakeLoadSec = g_dialogSec
    end

	-- Have we passed the threshold time for refreshing the message? 
    if ((g_msgSec == 0) or (g_dialogSec > (g_msgSec + (1.0 / g_msgsPerSec)))) then
		local message = ""		
		message = ProgressMenuMessages:getRandomPhrase()
        SetMsgRandom(message)
        g_msgSec = g_dialogSec
    end
end

--[[ DRF - Handle all progress events.  PROGRESS_CANCEL & PROGRESS_ERROR are considered special events. ]]
function ProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

    -- Handle All Progress Events
    if (filter == PROGRESS_UPDATE) then
        local id = KEP_EventDecodeString( event )
        local msg = KEP_EventDecodeString( event )
        SetMsgUpdate(msg)
    elseif (filter == PROGRESS_ERROR) then
        local id = KEP_EventDecodeString( event )
        local msg = KEP_EventDecodeString( event )
        g_special = PROGRESS_ERROR
        SetMsgSpecial(msg)
    elseif (filter == PROGRESS_CANCEL) then
        local id = KEP_EventDecodeString( event )
        local msg = KEP_EventDecodeString( event )
        g_special = PROGRESS_CANCEL
        SetMsgSpecial(msg)
    elseif (filter == PROGRESS_OPT_CANCEL) then
        local id = KEP_EventDecodeString( event )
        local optCancel = KEP_EventDecodeNumber( event )
        local optChange = ((g_optCancel ~= PROGRESS_OPT_CANCEL_NEVER) and (g_optCancel ~= PROGRESS_OPT_CANCEL_ALWAYS))
        if (optChange) then
            g_optCancel = optCancel 
        end
    elseif (filter == PROGRESS_OPT_FADE) then
        local id = KEP_EventDecodeString( event )
        local optFade = KEP_EventDecodeNumber( event )
        g_optFade = optFade
        if (g_optFade == PROGRESS_OPT_FADE_NO) then
            g_fadeAlphaTarget = 0.0
            g_fadeAlpha = g_fadeAlphaTarget
        elseif (g_optFade == PROGRESS_OPT_FADE_MIN_NOW) then
            g_fadeAlphaTarget = g_fadeAlphaMin
            g_fadeAlpha = g_fadeAlphaTarget
        elseif (g_optFade == PROGRESS_OPT_FADE_MAX_NOW) then
            g_fadeAlphaTarget = g_fadeAlphaMax
            g_fadeAlpha = g_fadeAlphaTarget
        end
    elseif (filter == PROGRESS_OPT_MSG) then
        local id = KEP_EventDecodeString( event )
        local optMsg = KEP_EventDecodeNumber( event )
        local optChange = (g_optMsg ~= PROGRESS_OPT_MSG_DEBUG)
        if (optChange) then
            g_optMsg = optMsg
        end
    end
    
	local canCancel = (g_optCancel == PROGRESS_OPT_CANCEL_YES) or (g_optCancel == PROGRESS_OPT_CANCEL_ALWAYS) or (g_special ~= PROGRESS_NO)
	
    if canCancel then
        SetControlEnabled("btnCancel", true)
        m_fakeLoadMax = 0
    end
	
    g_canMsg = ((g_optMsg ~= PROGRESS_OPT_MSG_NO) or  (g_special ~= PROGRESS_NO))
    SetControlEnabled("txtMsg", g_canMsg)
end

function captureZoningMetricComplete()
	if g_zoningStart and g_captureOnce == false then
		g_captureOnce = true
	end
end

--[[ DRF - Set txtMsg text to display random messages if enabled except during special events. ]]
function SetMsgRandom(msg)
    if ((g_special ~= PROGRESS_NO) or (g_optMsg ~= PROGRESS_OPT_MSG_RANDOM)) then return end
    SetControlText("txtMsg", msg)
end

--[[ DRF - Set txtMsg text to display update messages if enabled except during special events. ]]
function SetMsgUpdate(msg)
    if ((g_special ~= PROGRESS_NO) or ((g_optMsg ~= PROGRESS_OPT_MSG_UPDATE) and (g_optMsg ~= PROGRESS_OPT_MSG_DEBUG))) then return end
    SetControlText("txtMsg", msg)
end

--[[ DRF - Set txtMsg text to display special messages and enable always. ]]
function SetMsgSpecial(msg)
    SetControlText("txtMsg", msg)
end

--[[ DRF - Set imgSpinner image frame for spinner animation. ]]
function SetSpinnerFrame(frame)
    local trash, f = MathDivRem(frame, 24) -- total 24 frames
    local frameY, frameX = MathDivRem(f, 5) -- layout 5 x 5 frames of 90 x 90 pixels 
    local img = Dialog_GetImage(gDialogHandle, "imgSpinner" )
    local elem = Image_GetDisplayElement( img )
    Element_SetCoords( elem, 90 * frameX, 90 * frameY, 90 * (frameX + 1), 90 * (frameY + 1) )
    SetControlEnabled("imgSpinner", true)
end

--[[ DRF - Set imgSpinner image to special event icon. ]]
function SetSpinnerSpecial()
    local frameY = 4
    local frameX = 4
    local img = Dialog_GetImage(gDialogHandle, "imgSpinner" )
    local elem = Image_GetDisplayElement( img )
    Element_SetCoords( elem, 90 * frameX, 90 * frameY, 90 * (frameX + 1), 90 * (frameY + 1) )
    SetControlEnabled("imgSpinner", true)
end

--[[ DRF - Set imgFade image coordinates to fill entire screen for background fade. ]]
function SetFadeCoords()
    local ctrl = gHandles["imgFade"]
    local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
    local screenHeight = Dialog_GetScreenHeight(gDialogHandle) + 32
    local dialogX = Dialog_GetLocationX(gDialogHandle)
    local dialogY = Dialog_GetLocationY(gDialogHandle) + 32
    Control_SetLocation(ctrl, 0 - dialogX, 0 - dialogY)
    Control_SetSize(ctrl, screenWidth, screenHeight)
    MenuBringToFrontThis()
end

--[[ DRF - Set imgFade image alpha value for background fade. ]]
function SetFadeAlpha(alpha)
    local ctrl = gHandles["imgFade"]
    color = {a = alpha, r = 0, g = 0, b = 0}
    BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(ctrl)), 0, color)
    SetControlEnabled("imgFade", true)
end

--[[ DRF - Set btnCancel normal state alpha value. ]]
function SetCancelAlpha(alpha)
    local ctrl = gHandles["btnCancel"]
    color = {a = alpha, r = 255, g = 255, b = 255}
    for i=0,4 do -- for all button states
        BlendColor_SetColor(Element_GetTextureColor(Button_GetNormalDisplayElement(ctrl)), i, color)
        BlendColor_SetColor(Element_GetTextureColor(Button_GetFocusedDisplayElement(ctrl)), i, color)
        BlendColor_SetColor(Element_GetTextureColor(Button_GetDisabledDisplayElement(ctrl)), i, color)
        BlendColor_SetColor(Element_GetTextureColor(Button_GetMouseOverDisplayElement(ctrl)), i, color)
        BlendColor_SetColor(Element_GetTextureColor(Button_GetPressedDisplayElement(ctrl)), i, color)
    end
end

--[[ DRF - btnCancel cancels progress if enabled or during special events. ]]
function btnCancel_OnButtonClicked( buttonHandle )
    local input = {reportName = "OnLoadingBarCancel", reportParams = {username = m_playerName, cancelPressed = g_dialogSec}}
    Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {input = input, callType = "METRIC_GENERIC"})
    CloseProgressMenu(g_special == PROGRESS_ERROR and "Errored" or "Cancelled", Static_GetText(gHandles["txtMsg"]))
    goHome()
    --MenuCloseThis()
end

function CompleteLoadHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
    if m_frameworkEnabled and m_frameworkEnabled ~= 0 then
        updateProgressBar(COMPLETE_LOAD_PERCENT)
    else
        updateProgressBar(100)
    end
end

function updateProgressBar(loadProgress)
	m_animating = true
	g_currProgress = g_currProgress + loadProgress

    local progress = g_currProgress
    if progress < m_fakeLoadProgress then
        progress = m_fakeLoadProgress
    end

	-- DRF - ED-5456 - Tell Engine Progress Percent
	local progressPct = math.min(progress, 100.0)
	KEP_SetZoneLoadingProgressPercent(progressPct)

	m_totalBarWidth = ((progress / 100.0) *  BAR_WIDTH)
	m_currBarWidth = Control_GetWidth(gHandles["imgLoadingBar"])
end

function ZoneCustomizationSummaryHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    if m_display == false then
        local ev = KEP_EventCreate("ZoneNavigationEvent")
        KEP_EventSetFilter(ev, ZONE_NAV.CURRENT_URL_REQUEST)
        KEP_EventQueue(ev)
        m_display = true
    end

    g_PIDTotal = Event_DecodeNumber(event)
    local numWldObjSettings = Event_DecodeNumber(event)
    m_frameworkEnabled = Event_DecodeNumber(event)

    if g_PIDTotal == 0 then
        updateProgressBar(OBJECT_PERCENT)
	end
end

function ZoneCustomizationDLCompleteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

function AddDynamicObjectEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    local placementSource = objectid
	local playerID = filter
	local positionX = KEP_EventDecodeNumber(event)
	local positionY = KEP_EventDecodeNumber(event)
	local positionZ = KEP_EventDecodeNumber(event)
	local rotationX = KEP_EventDecodeNumber(event)
	local rotationY = KEP_EventDecodeNumber(event)
	local rotationZ = KEP_EventDecodeNumber(event)
	local slideX = KEP_EventDecodeNumber(event)
	local slideY = KEP_EventDecodeNumber(event)
	local slideZ = KEP_EventDecodeNumber(event)
	local placementId = KEP_EventDecodeNumber(event)
    local animationId = KEP_EventDecodeNumber(event)
    local assetId = KEP_EventDecodeNumber(event)
    local texture = KEP_EventDecodeString(event)
    local friendId = KEP_EventDecodeNumber(event)
    local swf = KEP_EventDecodeString(event)
    local swfParams = KEP_EventDecodeString(event)
    local globalId = KEP_EventDecodeNumber(event)
    local attachable = KEP_EventDecodeNumber(event)
    local interactive = KEP_EventDecodeNumber(event)
    local playFlash = KEP_EventDecodeNumber(event)
    local collision = KEP_EventDecodeNumber(event)
    local min_x = KEP_EventDecodeNumber(event)
    local min_y = KEP_EventDecodeNumber(event)
    local min_z = KEP_EventDecodeNumber(event)
    local max_x = KEP_EventDecodeNumber(event)
    local max_y = KEP_EventDecodeNumber(event)
    local max_z = KEP_EventDecodeNumber(event)
    local derivable = KEP_EventDecodeNumber(event)
    local inventory_sub_type = KEP_EventDecodeNumber(event)

	if placementId and globalId and placementSource == PLACEMENT_ZONECUSTDL then
		g_PIDTable[placementId] = 1
        g_gIDTable[globalId] = placementId --Needed for later event
	end
end

function HandleDynamicObjectEvent(pid, reason)
    if g_PIDTable[pid] then
        local loadIncrese =   (1/g_PIDTotal) * OBJECT_PERCENT
        updateProgressBar(loadIncrese)
        g_PIDTable[pid] = nil
		local lastItem = next(g_PIDTable) == nil
    end
end

function DynamicObjectInitializedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    -- DO inited
    HandleDynamicObjectEvent(objectid, "initialized")
end

function DynamicObjectInitDelayedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    -- DO init delayed - probably NPC
    HandleDynamicObjectEvent(objectid, "initialization delayed")
end

function DynamicObjectLoadingFailedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    -- DO failed - treat it the same as completion for now
    HandleDynamicObjectEvent(objectid, "loading failed")
end

function zoneNavigationEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    if filter == ZONE_NAV.CURRENT_URL_INFO then
        local url = KEP_EventDecodeString(tEvent)
        m_worldName = parseFriendlyNameFromURL(url)
        SetControlText("txtWorld", "Loading "..tostring(m_worldName))
        SetControlEnabled("btnCancel", false)
        SetControlVisible("btnCancel", false)
		if g_objStart == nil then
			local currTime = GetTimeMs()
			g_objStart = currTime			
			g_frameworkStart = currTime
		end
    end
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
    -- ESC key
    if key == Keyboard.ESC then
        if m_display == true then
            local input = {reportName = "OnLoadingBarCancel", reportParams = {username = m_playerName, cancelPressed = g_dialogSec}}
            Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {input = input, callType = "METRIC_GENERIC"})
            CloseProgressMenu(g_special == PROGRESS_ERROR and "Errored" or "Cancelled", Static_GetText(gHandles["txtMsg"]))
            goHome()
        end
    end
end

-- Framework Only - Called on player activation.  Starts player data aquisition.
function frameworkProgress(event)
	g_dataStart = GetTimeMs()
    updateProgressBar(FRAMEWORK_LOAD_PERCENT)
end

-- Framework Only - Called when all player data has been obtained and the user can be freed for movement.
function frameworkComplete(event)
	if g_dataStart then
		g_waitStart = currTime
	end			
    updateProgressBar(FRAMEWORK_COMPLETE_PERCENT)
end

function AddSoundEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    KEP_EventDecodeNumber(event)  -- "parentType": ignore for now
    local placementId = KEP_EventDecodeNumber(event)
    KEP_EventDecodeNumber(event)    -- "slot": ignore for now
    local placementSource = filter    -- See enum PlacementSource

    if g_soundPlacements[placementId]~=nil then
        LogWarn("AddSoundEventHandler: ALREADY ADDED - SP<"..pid..">")
        return
    end
    if placementSource == PLACEMENT_ZONECUSTDL then
        local loadIncrese =   (1/g_PIDTotal) * OBJECT_PERCENT
        updateProgressBar(loadIncrese)
        g_soundPlacements[placementId] = true
        g_PIDTable[placementId] = nil
		local lastItem = next(g_PIDTable) == nil
    end
   
    return KEP.EPR_CONSUMED -- Dont let client engine get sound placements
end

---------------------------
--Welcome screen Settings--
---------------------------

function returnWelcomeHandler(event)
    if event.showMenu == true then
        MenuOpen("Framework_Welcome")
        local welcomeEvent = KEP_EventCreate("FRAMEWORK_OPEN_WELCOME")
        KEP_EventEncodeString(welcomeEvent, Events.encode(event))
        KEP_EventQueue(welcomeEvent)
        m_showWelcome = true
    end
end

function OpenWelcomeMenu( )
    if m_showWelcome == true then
        MenuOpen("TransparentBG")
    end
end
