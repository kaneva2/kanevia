--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

EDGE_OFFSET = 10
HUD_HEIGHT = 48

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister("BuildKeyPressedEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	local screenWidth= Dialog_GetScreenWidth(dialogHandle)
	local screenHeight = Dialog_GetScreenHeight(dialogHandle)
	local size = MenuGetSizeThis()

	MenuSetLocationThis(screenWidth - size.width - EDGE_OFFSET, screenHeight - size.height - HUD_HEIGHT - EDGE_OFFSET)

	-- Update arrows because this menu may have been opened outside of selection.lua
	updateArrows()
end

function updateArrows()
	local numSelected = KEP_GetNumSelected()
	local arrowsOn = numSelected > 0 and selectedObjectsDynamic(numSelected)
	Control_SetEnabled(gHandles["btnUp"], arrowsOn)
	Control_SetEnabled(gHandles["btnDown"], arrowsOn)
end

function selectedObjectsDynamic(numObjects)
	for i = 1, numObjects do
		local objType, id = KEP_GetSelection(i-1) --GetSelection starts at 0
		if objType ~= ObjectType.DYNAMIC and objType ~= ObjectType.SOUND then
			return false
		end
	end
	return true
end

-- DRF - Handled in Selection.lua
function sendKeyPressedEvent(key)
	local ev = KEP_EventCreate("BuildKeyPressedEvent")
	KEP_EventSetFilter(ev, key)
	KEP_EventQueue( ev)
end

function btnUp_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(36) --Home
end

function btnDown_OnButtonClicked(buttonHandle)
	sendKeyPressedEvent(35) --End
end

--Prevent from going off screen
function Dialog_OnMoved(dialogHandle, x, y)

	local screenWidth = Dialog_GetScreenWidth(dialogHandle)
	local screenHeight = Dialog_GetScreenHeight(dialogHandle)
	local loc = MenuGetLocationThis()
	local size = MenuGetSizeThis()

	if (loc.x + size.width > screenWidth) then
		loc.x = screenWidth - size.width - EDGE_OFFSET
	end

	if (loc.y + size.height > screenHeight - HUD_HEIGHT) then
		loc.y = screenHeight - size.height - HUD_HEIGHT - EDGE_OFFSET
	end

	MenuSetLocationThis(loc.x, loc.y)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
