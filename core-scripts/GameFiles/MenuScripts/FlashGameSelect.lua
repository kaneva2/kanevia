--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("URLImport.lua")

g_objId 	 = -1
g_message 	 = nil
g_t_playlists  = nil

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "SelectFlashGameMenuEventHandler", "SelectFlashGameMenuEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "SelectFlashGameMenuEvent", KEP.MED_PRIO )
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( "FlashGameUpdatedEvent", KEP.MED_PRIO )
end

function SelectFlashGameMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)	
	g_objId  = KEP_EventDecodeNumber( event )
	return KEP.EPR_OK
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	-- DRF - Bug Fix - was not filtering 
    if (filter == WF.PLAYLIST) then
		g_message = KEP_EventDecodeString( event )
		ParsePlayList(gDialogHandle)
		return KEP.EPR_OK
	end
end

-- Populates the playlist with the items in g_message
function ParsePlayList(dialogHandle)
	local s = 0 		-- start index of the substring
	local e = 0 		-- end index of the substring
	local result = ""   -- subtring
	local strPos = 0	-- current position in string to parse
	
	-- ReturnCode of 0 means there was data sent back
	s, e, result = string.find(g_message, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		g_t_playlists = {}
		
		local lh = Dialog_GetListBox(gDialogHandle, "lstPlaylists")
		ListBox_RemoveAllItems(lh)
		ListBox_AddItem(lh, "<None>", 0) -- For no playlist
	
		-- Parse Playlist XML
		s, strPos, numRec = string.find(g_message, "<NumberRecords>([0-9]+)</NumberRecords>")
		s, strPos, playlistURL = string.find(g_message, "<PlayListURL>(.-)</PlayListURL>")
        if (numRec == nil) then return end
		for i=1, numRec do
			local playliststr = ""
			local trash = 0
			s, strPos, playliststr = string.find(g_message, "<PlayList>(.-)</PlayList>", strPos)
            if (playliststr ~= nil) then 
				s, trash, asset_id = string.find(playliststr, "<asset_id>([-]?[0-9]+)</asset_id>", 1)
				s, trash, name = string.find(playliststr, "<name>(.-)</name>", 1)
				s, trash, offsite_url = string.find(playliststr, "<asset_offsite_id>(.-)</asset_offsite_id>", 1)
				s, trash, mature = string.find(playliststr, "<mature>(.-)</mature>", 1)
				s, trash, asset_type = string.find(playliststr, "<asset_type_id>(.-)</asset_type_id>", 1)
			
				-- Build Playlist Structure
				local play = {}
				play["asset_group_id"] = asset_id
				play["name"] = name -- DRF - Bug Fix
				play["offsite_url"] = offsite_url
				play["mature"] = mature
				play["asset_type"] = asset_type
				play["asset_count"] = 1
				play["URL"] = string.gsub(playlistURL, "ASSET_ID", asset_id)
				play["playlistId"] = asset_id;
				table.insert(g_t_playlists, play)

				-- Add Playlist To ListBox
				ListBox_AddItem(lh, name, asset_id)
			end
		end
	end
end

function btnOk_OnButtonClicked(buttonHandle)
	local lh = Dialog_GetListBox(gDialogHandle, "lstPlaylists")
	local index = ListBox_GetSelectedItemIndex(lh) 
	local playlistIndex = 0 -- no playlist selected
	local playlistId = 0 -- causes stop

	-- <0=NoSelection, 0=<None>, { ,1=playlists[1] ,2=playlists[2], ... } <- user playlists
	if index < 0 then
	
		-- Nothing Selected
		Log("OkButtonClicked: Nothing Selected")
		MenuCloseThis()
		return
	elseif index == 0 then
	
		-- Selected <None>
		Log("OkButtonClicked: Selected '<None>'")
		playlistId = 0
	elseif index > 0 then
	
		-- Selected User Defined Playlist
		playlistIndex = index
		playlistId = g_t_playlists[playlistIndex]["playlistId"]
		
		-- the source of this playlist was from Kaneva\WOK. A 3dapp DB does not have
		-- these playlist assets and can't look up additional information about the
		-- asset in their local DB. Thus, we provide additional information about the asset that
		-- the 3dapp DB does not have. Brett 04/15/10.
		offsite_url = g_t_playlists[playlistIndex]["offsite_url"]
		offsite_url = url_encode(offsite_url)
		if offsite_url == nil then
			offsite_url = ""
		end

		name = g_t_playlists[playlistIndex]["name"]
		mature = g_t_playlists[playlistIndex]["mature"]
		asset_type = g_t_playlists[playlistIndex]["asset_type"]

		Log("OkButtonClicked: Selected Playlist["..playlistIndex.."]")
		Log(" ... name='"..name.."'")
		Log(" ... id="..tostring(playlistId).." mature="..tostring(mature).." assetType="..asset_type)
		Log(" ... url='"..offsite_url.."'")
	end

	e = KEP_EventCreate( BrowserIds.BROWSER_REQUEST_EVENT_NAME )
	if playlistIndex > 0 then
		KEP_EventEncodeString( e, "changePlaylist?assetId="..tostring(playlistId).."&objId="..g_objId.."&mature="..mature.."&asset_type="..asset_type.."&stop=0&offsite_url="..offsite_url) 
	else
		KEP_EventEncodeString( e, "changePlaylist?assetId="..tostring(playlistId).."&objId=" .. g_objId) 
	end
	KEP_EventAddToServer( e )
	KEP_EventQueue( e )
	e = KEP_EventCreate( "FlashGameUpdatedEvent" )
	KEP_EventQueue(e)

	MenuCloseThis()
end

function btnSearch_OnButtonClicked( buttonHandle )
	local ebh = Dialog_GetEditBox(gDialogHandle, "txtSearch")
	if ebh ~= nil then
		local searchString = EditBox_GetText(ebh)
        if searchString ~= "" then
			makeWebCall( GameGlobals.WEB_SITE_PREFIX.."kgp/playList.aspx?type=g&ss=" .. searchString, WF.PLAYLIST,  1, 50)
		end
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	makeWebCall( GameGlobals.WEB_SITE_PREFIX.."kgp/playList.aspx?type=g", WF.PLAYLIST,  1, 50)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnUpload_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .. "asset/publishedItemsNew.aspx" )
end
