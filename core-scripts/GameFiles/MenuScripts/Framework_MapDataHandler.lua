--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_MapDataHandler.lua
-------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
--dofile("..\\MenuScripts\\Framework_FlagHelper.lua")


-- Debugging Helpers
local Debug = _G.Debug -- Bind to a local variable for efficiency's sake
local LOG_FUNCTION_CALL, LOG_FILTER_FUNCTION_CALL = Debug.generateCustomLog("MapDataHandler - ")
Debug.enableLogForFilter(LOG_FILTER_FUNCTION_CALL, false) -- Set this to true if you want to get a complete log of Controller_Inventory function calls

------------------------------
-- Local variables
------------------------------

local s_mapData = {}

------------------------------
-- Event Handlers
------------------------------

function onCreate()
	LOG_FUNCTION_CALL("onCreate")
	
	-- Register Server Event Handlers
    Events.registerHandler("FRAMEWORK_WORLD_MAP_RETURNED_FROM_SERVER", onWorldMapDataReturned)
	Events.registerHandler("FRAMEWORK_DYNAMIC_OBJECT_SAVED", onDynamicObjectMoved)
	Events.registerHandler("FRAMEWORK_WORLD_MAP_OBJECT_ADDED", onSingleWorldMapObjectAdded)
	Events.registerHandler("FRAMEWORK_WORLD_MAP_OBJECT_REMOVED", onSingleWorldMapObjectRemoved)
	--Events.registerHandler("FRAMEWORK_RETURN_FLAGS", onFlagsUpdated)
	--Get the current Event Flag
	Events.registerHandler("FRAMEWORK_EVENT_FLAG_START", eventFlagStartHandler)

	-- Register New Client Events
	KEP_EventRegister("FRAMEWORK_WORLD_MAP_REQUESTED_BY_CLIENT", KEP.HIGH_PRIO) 
	KEP_EventRegister("FRAMEWORK_WORLD_MAP_RETURNED_TO_CLIENT", KEP.HIGH_PRIO)
	
	
	-- Register Client Handlers
	KEP_EventRegisterHandler("onWorldMapRequestedByClient", "FRAMEWORK_WORLD_MAP_REQUESTED_BY_CLIENT", KEP.HIGH_PRIO)

	-- Request Map Data from the server
    Events.sendEvent("FRAMEWORK_REQUEST_WORLD_MAP", {})
end

function onDestroy()
	LOG_FUNCTION_CALL("onDestroy")
end



function onWorldMapDataReturned(event)
	LOG_FUNCTION_CALL("onWorldMapDataReturned")
	
	for i, v in pairs(event.mapData) do
		s_mapData[i] = v
	end
end


function onSingleWorldMapObjectAdded(event)
	LOG_FUNCTION_CALL("onSingleWorldMapObjectAdded")
	
	local mapObject = event.mapObject
	local objectPID = mapObject and mapObject.PID
	
	if( objectPID ) then
		s_mapData[tostring(objectPID)] = mapObject
	end
end


function onSingleWorldMapObjectRemoved(event)
	LOG_FUNCTION_CALL("onSingleWorldMapObjectRemoved")
	
	local strObjectPID = tostring(event.PID)
	local mapObject = s_mapData[strObjectPID]
	if mapObject then
		s_mapData[strObjectPID] = nil
	end
end



function onDynamicObjectMoved(event)
	LOG_FUNCTION_CALL("onDynamicObjectMoved")
	
	local objectPID = event.PID
	local mapObject = s_mapData[tostring(objectPID)]
	
	-- Is the object whose position changed of any concern to us?
	if mapObject then
		-- Update the object position
		local x, y, z = KEP_DynamicObjectGetPosition(objectPID)
		mapObject.position = {x = x, y = y, z = z}
	end
end

function onWorldMapRequestedByClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	LOG_FUNCTION_CALL("onWorldMapRequestedByClient")
	
	local event = KEP_EventCreate("FRAMEWORK_WORLD_MAP_RETURNED_TO_CLIENT")
	
	if s_mapData then
		local strData = Events.encode(s_mapData)

		if strData then
			KEP_EventEncodeString(event, strData)
		end
		
		KEP_EventQueue(event)
		
		--Debug.printTable("s_mapData", s_mapData)
	end
end


function eventFlagStartHandler(event )

	local tempTable = {}

	tempTable.name = event.info.title
	local x, y, z = KEP_DynamicObjectGetPosition(event.PID)
	tempTable.position = {x = x, y = y, z = z}
	tempTable.type = 2
	s_mapData[tostring(event.PID)] = tempTable
end