--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

SEND_GLID_EVENT = "SendGLIDEvent"
UPDATE_INVENTORY_EVENT = "UpdateInventoryEvent"

AEGenericTypeID     = 142
DELETED_ITEM_FILTER = 5

g_itemGLID = nil
g_invType  = nil

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "itemGLIDHandler",         SEND_GLID_EVENT,         KEP.MED_PRIO)
	KEP_EventRegisterHandler( "attribEventHandler",      "AttribEvent",           KEP.MED_PRIO)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister(SEND_GLID_EVENT,        KEP.MED_PRIO)
	KEP_EventRegister(UPDATE_INVENTORY_EVENT, KEP.MED_PRIO)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	local invMenu = MenuHandle("Storage.xml")
	setLocation(invMenu)
end

-- Received when this menu is opened
function itemGLIDHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_itemGLID = tonumber(KEP_EventDecodeNumber(event))
	g_invType  = tonumber(KEP_EventDecodeNumber(event))
end

function btnYes_OnButtonClicked(btnHandle)

	-- Only work if we've received the glid and inv type
	if g_itemGLID and g_invType then

		--Get quantity because we deposit it all
		getItemQuantities(WF.STORAGE_QUANTITY, {g_itemGLID})
		-- To BrowserPageReadyHandler

		--Don't let them click again while waiting for a response
		Control_SetEnabled(gHandles["btnYes"], false)
		Control_SetEnabled(gHandles["btnNo"], false)
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.STORAGE_QUANTITY and g_itemGLID then
		local returnData = KEP_EventDecodeString( event )
		local s, e, glid = string.find(returnData, "<global_id>(" .. tostring(g_itemGLID) .. ")</global_id>" )
		if glid then
			local s, e, quantity = string.find(returnData, "<quantity>(%d-)</quantity>")
			quantity = tonumber(quantity or 0)
			KEP_RemoveBankItem( g_itemGLID, quantity, g_invType )
			-- To attribEventHandler
		end
	end
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	if filter == AEGenericTypeID and objectid == 92 then
		-- Update Invnentory now that bank deletion occured
		local ev = KEP_EventCreate(UPDATE_INVENTORY_EVENT)
		KEP_EventSetFilter(ev, DELETED_ITEM_FILTER)
		KEP_EventQueue(ev)

		MenuCloseThis()
	end
end

function setLocation(invMenu)
	local invX = Dialog_GetLocationX(invMenu)
	local invY = Dialog_GetLocationY(invMenu)
	local invWidth = Dialog_GetWidth(invMenu)
	local invHeight = Dialog_GetHeight(invMenu)

	local width = Dialog_GetWidth(gDialogHandle)
	local height = Dialog_GetHeight(gDialogHandle)

	local x = (invWidth - width)/2 + invX
	local y = (invHeight - height)/2 + invY

	MenuSetLocationThis(x, y)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnNo_OnButtonClicked(btnHandle)
	MenuCloseThis()
end
