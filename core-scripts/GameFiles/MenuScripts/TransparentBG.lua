--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")


function onCreate(dialogHandle)
	SetFadeCoords()

	KEP_EventRegisterHandler( "clientResizeEventHandler", "ClientResizeEvent", KEP.LOW_PRIO )
end


function SetFadeCoords()
    local ctrl = gHandles["imgFade"]
    local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	Control_SetSize(ctrl, screenWidth, screenHeight)
    MenuSetLocationThis(0,0)
end

function clientResizeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	SetFadeCoords()
end