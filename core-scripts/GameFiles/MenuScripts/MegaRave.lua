--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("CAM.lua")

PLAYER_ANIMATION_EVENT = "PlayerAnimationEvent"

MEGA_RAVE_ANIMATION = anim_mega_rave
MEGA_RAVE_GLID = 4942

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function btnOk_OnButtonClicked( buttonHandle)

--[[ DEPRECATED
	--play MegaRave Animation
	KEP_SetCurrentAnimation(MEGA_RAVE_ANIMATION, 500)

	--queue Event in future so when animation is over we can reset animation
	--to version 0 or else player will not walk correctly
	local ev = KEP_EventCreate( PLAYER_ANIMATION_EVENT )
	KEP_EventEncodeNumber( ev, anim_stand )
	KEP_EventEncodeNumber( ev, 0 )
	KEP_EventQueueInFuture( ev, 8 )
]]

	--Remove Item From Inventory
	KEP_RemoveInventoryItem( 4942, 1, GameGlobals.IT_NORMAL)

	MenuCloseThis()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
