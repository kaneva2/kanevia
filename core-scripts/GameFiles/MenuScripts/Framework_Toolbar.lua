--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Toolbar.lua
--
-- Handles slotted inventory items for the framework with item activation
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("..\\Scripts\\BuildMode.lua")
dofile("..\\MenuScripts\\MenuAnimation.lua")
dofile("Framework_DragDropHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("ContextualHelper.lua")
dofile("Framework_FlagHelper.lua")

-- Globals
----CLICK IDS----------------------------------------------------
LEFT_CLICK  = 0
RIGHT_CLICK = 1

local ATTACK_FLASH_TIME = .5 -- seconds for attack flash time
local WEAPON_SWAP_COOLDOWN = 1 -- Milliseconds it takes for a wepaon swap to allow attacking again
local TOOLBAR_HELP_BUFFER = 20

local GI_LOAD_MESSAGE1 = "The limit of game objects in this world has been reached."
local GI_LOAD_MESSAGE2 = "Free some space by removing any objects you have placed."

local DEFAULT_MAX_DURABILITY = 1000 -- 1,000 hits or attacks per weapon/armor
local DURABILITY_BY_LEVEL = { [1]=10, [2]=100, [3]=250, [4]=500, [5]=1000 }
local DURABILITY_BAR_WIDTH = 48

local usableItems = {"door", "repair", "campfire", "seat" , "eventFlag", "media"}

-- Left, top, right, bottom
local DURABILITY_COLORS = {GREEN  = {19, 179, 20, 185},
						   YELLOW = {31, 179, 32, 185},
						   RED 	  = {25, 179, 26, 185}
						  }

local TOOLTIP_TABLE = {header = "name"}

m_ammoCount = {}
m_weaponSelected = false
m_toolbarInventory = {}
m_netID = 0
m_selfObject = ""
m_allowsPIN = false
m_selectedPID = nil
m_selectedName = ""

-- Local vars
local m_gameItems = {}

local m_attackFlashTime
local m_timeStamp = 0

local m_buildSettingToolbar
local m_noBuildReason
local m_previousIndex = 0
local m_selectedIndex = 0
local m_placingItem = false
local m_equippedUNID
local m_buildMode = MODE_MOVE
local m_permissions = 3
local m_hideNumeric = false
local m_eventFlags = {}
local m_armorRating = 0

local m_growBackpack = false
local m_showNewMessage	= false
local m_hideNewMessage = false
local m_NewMessageHidden = false
local m_playerOccupied = false
local m_controlsShowing = false
local m_swappingToolbelt = false
local m_newItemType = "item"

local m_tweenList = {"stcNewItemBG1", "stcNewItemBG2","stcNewItemBG3","stcNewItemBG4","stcNewItem","stcNewItemCountBG1","stcNewItemCountBG2","stcNewItemCountBG3","stcNewItemCountBG4","stcNewItemCount"}
local m_buildPopupControls = {"imgBuildBG", "btnUse", "btnMove", "btnRotate", "btnPickup", "btnShare"}

local m_creatorModeEnabled = false
local m_buildSelected = nil --PID
local m_gameItemSelected = nil --UNID
local m_invalidPlacement = false --used for flag 
local m_requestHandle = nil
local m_showingContextualHelp = false
local m_elementExists = false
local m_lastGameItem = nil
local m_pendingTransaction = nil
local m_pendingPID = nil
local m_flagBuildOnly = false

local m_alive = true
local m_dynamicEnable = false
local m_minimized = false
local m_minimizedSetting = false
local m_minimizeAnimation = nil
local m_minimizeTime = nil
local m_shownAfterProgress = false
local MINIMIZE_DISPLACEMENT = 70
local MINIMIZE_DURATION = .20
local MINIMIZE_DELAY_LONG = 1.25
local MINIMIZE_DELAY_MEDIUM = 1.0
local MINIMIZE_DELAY_SHORT = .5
local MINIMIZE_TOOLBELT_FILTER = 1
local MINIMIZE_INDICATORS_FILTER = 2

local m_keyPressBufferTimer = 0
local KEY_PRESS_BUFFER_TIME = 10

-- Local Functions
local updateInventory -- (inventory)
local updateToolbar -- ()
local selectHand -- ()
local selectItem -- (index)
local deSelectItem -- (index)
local activateItem -- (slot)
local deactivateItem -- (slot)
local consumableItemActivated -- (slot)
local placeableItemActivated -- (slot)
local setCountBG -- (imgHandle, stcHandle, offsetX)
local updateBuildHighlights --()
local getDurabilityColor -- (durabilityPercentage) Gets the durability color by percentage
local detectSimilarTables -- (table1, table2) Detect if all contained values of a table are similar
local petItemActivated -- (slot) Called when a pet is activated
local toggleToolbarMinimized -- (toggle)
local canMinimizeToolbar
local formatNumberSuffix --(count)
local getMaximizedLocation -- ()

local FlagSystem			= _G.FlagSystem			-- Assign the global FlagSystem to a local variable for faster access

-- When the menu is created
function onCreate()
	KEP_EventRegister("CLOSE_BACKPACK_CONTEXTUAL_HELP", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_SELECT_TOOLBELT_INDEX", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_INITIALIZE_PIN", KEP.HIGH_PRIO)

	-- Register client event handlers
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateQuestsClientFull", "UPDATE_QUEST_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("hideToolbeltSettingEventHandler", "HideToolbeltSettingEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler("ProgressEventHandler", "ProgressEvent", KEP.MED_PRIO)
	-- Register for Click events
	KEP_EventRegisterHandler("clickEventHandler", "ClickEvent", KEP.HIGH_PRIO)
	-- Register for Build Mode events
	KEP_EventRegisterHandler("frameworkBuildModeHandler", "FRAMEWORK_BUILD_SETTING", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("selectHandHandler", "FRAMEWORK_SELECT_HAND", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("selectIndexHandler", "FRAMEWORK_SELECT_TOOLBELT_INDEX", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("frameworkShowBuildToolsHandler", "FRAMEWORK_SHOW_BUILD_TOOLS", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("setSelectedObjectHandler", "FRAMEWORK_SET_SELECTED_OBJECT", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("frameworkMinimizeHUDHandler", "FRAMEWORK_MINIMIZE_HUD", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("attackFlashHandler", "FRAMEWORK_ATTACK_FLASH", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("onReturnElement", "DRAG_DROP_RETURN_ELEMENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCustomDrop", "DRAG_DROP_CUSTOM_DROP", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("unlockToolbarHandler", "FRAMEWORK_UNLOCK_TOOLBAR", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("openContextualMenuEventHandler", "openContextualMenuEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("closeContextualMenuEventHandler", "closeContextualMenuEvent", KEP.HIGH_PRIO)

	--KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onPreprocessTransactionResponse", "INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)

	--Register message event
	KEP_EventRegister("FRAMEWORK_NEW_ITEM_MESSAGE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onNewItemHandler", "FRAMEWORK_NEW_ITEM_MESSAGE", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler( "onEventFlagsReturnedFull", "UPDATE_EVENT_FLAGS_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onEventFlagReturned", "UPDATE_EVENT_FLAGS_CLIENT", KEP.MED_PRIO)

	--Regiester for flag updates
	KEP_EventRegisterHandler( "onFlagsReturnedFull", "UPDATE_FLAGS_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onFlagReturned", "UPDATE_FLAG_CLIENT", KEP.MED_PRIO)
	
	Events.registerHandler("FRAMEWORK_TOOLBELT_SET_NUMERIC", setNumericHandler)

	Events.registerHandler("FRAMEWORK_PLACE_GAME_ITEM_CONFIRMATION", placeGameItemConfirmation)
	Events.registerHandler("FRAMEWORK_RETURN_PERM_AND_CREATOR_MODE", permissionsAndCreatorEvent)
	Events.registerHandler("RETURN_CURRENT_TIME", onReturnCurrentTime)
	Events.registerHandler("FRAMEWORK_PICKUP_RETURN", onPickUpReturn)

	Events.sendEvent("FRAMEWORK_REQUEST_PERM_AND_CREATOR_MODE")
	Events.sendEvent("FRAMEWORK_REQUEST_CURRENT_TIME")
	Events.sendEvent("REQUEST_PLAYER_TRACKED_QUEST")

	Events.registerHandler("FRAMEWORK_PLACE_CLAIM_FLAG_CONFIRMATION", placeClaimFlagConfirmation) --Taking this from Framework_Playerbuild 
	Events.registerHandler("FRAMEWORK_INVALID_MEDIA_PLACMENT", mediaHandler) --Also taking from Franework_Playerbuild
	Events.registerHandler("FRAMEWORK_PLAYER_OCCUPIED", playerOccupiedHandler)
	Events.registerHandler("FRAMEWORK_ONE_GAME_ITEM_DO_LEFT", lastGameItemHandler)
	Events.registerHandler("FRAMEWORK_SET_ARMOR", setArmor)
	Events.registerHandler("FRAMEWORK_MODIFY_BATTLE_OBJECT", modifyBattleObjectHandler)
	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS", onWorldSettingsChanged)
	
	
	requestEventFlags()
	
	
	-- NOTE: Framework_BattleHandler piggy-backs on this Inventory request. Do not remove
	requestInventory(INVENTORY_PID)
	requestInventory(GAME_PID)
	requestInventory(QUEST_PID)
	
	-- Register Hand button
	_G["btnItem1_OnButtonClicked"] = function(btnHandle)
		-- Enter hand mode
		selectHand()
	end

	-- Register button event handlers
	for i = 2, TOOLBAR_SLOTS+1 do
		_G["btnItem" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			local player = KEP_GetPlayer()
			if m_alive then
				local s, e, index = string.find(Control_GetName(btnHandle), "(%d+)")
				-- Don't re-select same item
				--if m_selectedIndex ~= tonumber(index) and (index == 1 or (m_toolbarInventory[index-1] and m_toolbarInventory[index-1].UNID ~= 0)) then
				if (index == 1 or (m_toolbarInventory[index-1] and m_toolbarInventory[index-1].UNID ~= 0)) then
					selectItem(tonumber(index))
				else
					selectHand()
				end
			end
		end
	end

	-- Enter hand mode
	-- TODO: Select previously selected item? (since the toolbar's now closed on mode change)
	m_playerOccupied = false
	selectHand()
	
	if MenuIsOpen("Emotes") then
		setNumericHandler({hideNumeric = true})
	end
	
	-- Get some info for this player
	local player = KEP_GetPlayer()
	if player ~= nil then
		m_netID = GetSet_Safe_GetNumber(player, MovementIds.NETWORKDEFINEDID)
	end
	m_selfObject = KEP_GetPlayerByNetworkDefinedID(m_netID)

	-- Dynamic HUD A/B test. If in the original, default to showing.
	m_dynamicEnable = false
	m_minimizeAnimation = Tween(MenuHandleThis())
	m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT, MINIMIZE_DURATION, nil, Easing.inOutQuad)
	m_minimizeAnimation:on(CALLBACK.STOP, onMinimizeStop)
	m_minimizedSetting = GetHideToolbeltSetting()
	toggleToolbarMinimized(m_minimizedSetting, true)

	DragDrop.initializeDragDrop(TOOLBAR_SLOTS + 1, 0, TOOLBAR_SLOTS + 1)
	DragDrop.setIndexOffset(-1)

	--DragDrop.buttonOnMouseEnter(m_requestHandle)

	-- added for the tooltips
	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()	
	-- InventoryHelper.initializeInventory("g_displayTable", false, TOOLBAR_SLOTS + 1)
	-- InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")

	FlagSystem:create()
	KEP_EventCreateAndQueue("FRAMEWORK_REQUEST_FLAGS_LOCALLY")
end

-- Modify an existing Game Item
function modifyBattleObjectHandler(event)
	local ID = tostring(event.ID)
	local modifiedFields = event.modifiedFields
	if KEP_GetLoginName() == ID and modifiedFields then
		for index, value in pairs(modifiedFields) do
			if index == "alive" then
				m_alive = value
			end
		end
	end
end

function onDestroy()
	MenuClose("Framework_BattleHandler.xml")
	
	-- CONTEXTUAL HELP HIDE SELECT TOOLBELT
	ContextualHelper.hideHelp(ContextualHelp.SELECT_TOOLBELT)

	closeUseToolbeltContextualHelp()
	deSelectItem(m_selectedIndex, false)
end

-- Called every frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	MenuAnimation_OnRender(fElapsedTime)

	if m_showNewMessage and growTween == nil and  Control_GetVisible(gHandles["imgBackpackGlow"]) == 0 and  Control_GetVisible(gHandles["stcNewItem"]) == 0 then
		m_showNewMessage = false
		Control_SetEnabled(gHandles["imgBackpackGlow"], true)
		Control_SetVisible(gHandles["imgBackpackGlow"], true)
		Control_SetVisible(gHandles["btnBackpack"], false)

		growTween = Tween(nil, {"imgBackpackGlow"})
		growTween:resize(10,10, .7, 0, Easing.outQuad)
		growTween:move(-5,-5, .7, 0, Easing.outQuad)
		growTween:start()

		for i =1, 4 do
			Control_SetEnabled(gHandles["stcNewItemBG"..i], true)
			Control_SetEnabled(gHandles["stcNewItemCountBG"..i], true)
			Control_SetVisible(gHandles["stcNewItemBG"..i], true)
			Control_SetVisible(gHandles["stcNewItemCountBG"..i], true)

			Static_SetText(gHandles["stcNewItemBG"..i], "Added!")
			Static_SetText(gHandles["stcNewItemCountBG"..i], m_newItemType)
		end
		
		Control_SetEnabled(gHandles["stcNewItem"], true)
		Control_SetEnabled(gHandles["stcNewItemCount"], true)
		Control_SetVisible(gHandles["stcNewItem"], true)
		Control_SetVisible(gHandles["stcNewItemCount"], true)
		Static_SetText(gHandles["stcNewItem"], "Added!")
		Static_SetText(gHandles["stcNewItemCount"], m_newItemType)

		showMessageTween = Tween(nil, m_tweenList)
		showMessageTween:move(0,-40,.75, 0,Easing.linear)
		showMessageTween:fade(255, .75, 0)
		showMessageTween:start()
		
		m_growBackpack = true
		m_hideNewMessage = true
	end

	if m_growBackpack and growTween:getStopped() then

		shrinkTween = Tween(nil, {"imgBackpackGlow"})
		shrinkTween:resize(-10,-10, .7, 0, Easing.outQuad)
		shrinkTween:move(5,5, .7, 0, Easing.outQuad)
		shrinkTween:start()
		
		m_growBackpack = false
		growTween = nil
	end

	if shrinkTween and shrinkTween:getStopped() then
		Control_SetVisible(gHandles["btnBackpack"], true)

		if not MenuIsOpen("UnifiedNavigation.xml") then
			showLootAddedContextualHelp()
		end 

		shrinkTween = nil
	end

	if m_hideNewMessage and showMessageTween and showMessageTween:getStopped() then
		hideMessageTween = Tween(nil, m_tweenList)
		hideMessageTween:move(0, 40,.75, 1,Easing.linear)
		hideMessageTween:fade(-255, .75, 1)
		hideMessageTween:start()
		m_hideNewMessage = false
		showMessageTween = nil
	end

	if hideMessageTween and hideMessageTween:getStopped() then
		for i =1, 4 do
			Control_SetEnabled(gHandles["stcNewItemBG"..i], false)
			Control_SetEnabled(gHandles["stcNewItemCountBG"..i], false)
			Control_SetVisible(gHandles["stcNewItemBG"..i], false)
			Control_SetVisible(gHandles["stcNewItemCountBG"..i], false)
			Control_SetEnabled(gHandles["imgBackpackGlow"], false)
			Control_SetVisible(gHandles["imgBackpackGlow"], false)
			
		end
		
		Control_SetEnabled(gHandles["stcNewItem"], false)
		Control_SetEnabled(gHandles["stcNewItemCount"], false)
		Control_SetVisible(gHandles["stcNewItem"], false)
		Control_SetVisible(gHandles["stcNewItemCount"], false)
		hideMessageTween = nil
		toggleToolbarMinimized(true)
	end

	if m_timeStamp then
		m_timeStamp = m_timeStamp + fElapsedTime

		if m_attackFlashTime then
			local alpha = math.max(0, 255*(math.sin(math.pi*(m_attackFlashTime/ATTACK_FLASH_TIME))))
			local color = {a = alpha, r = 255, g = 255, b = 255}
			BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgAttackFlash"])), 0, color)
			
			m_attackFlashTime = m_attackFlashTime - fElapsedTime
			
			if m_attackFlashTime <= 0 or m_minimized then
				m_attackFlashTime = nil
				Control_SetVisible(gHandles["imgAttackFlash"], false)
			end
		end
		-- Set cooldowns for toolbar elements
		for i = 2, TOOLBAR_SLOTS+1 do
			local item = m_toolbarInventory[i-1]
			-- Is this item a weapon?
			if item and ( (item.cooldownTime and item.fireTime) or (item.instanceData and item.instanceData.cooldownTime and item.instanceData.fireTime) ) then
				local cooldownTime = item.cooldownTime or item.instanceData.cooldownTime
				local fireTime = item.fireTime or item.instanceData.fireTime
				if fireTime then
					local cooldownHandle = gHandles["imgCooldown"..tostring(i)]
					-- Is it on cooldown?
					if cooldownTime > m_timeStamp then
						local cooldownSize = math.min(48 * ((cooldownTime - m_timeStamp) / (cooldownTime - fireTime)), 48)
						Control_SetSize(cooldownHandle, Control_GetWidth(cooldownHandle), cooldownSize)
						Control_SetVisible(cooldownHandle, true)
					else
						if item.instanceData and item.instanceData.fireTime then
							item.instanceData.fireTime = nil
						else
							item.fireTime = nil
						end

						Control_SetVisible(cooldownHandle, false)
					end
				end
			end
		end
		-- Minimize on timer.
		if m_minimizeTime then
			m_minimizeTime = m_minimizeTime - fElapsedTime

			if m_minimizeTime <= 0 and (not MenuIsOpen("Welcome.xml") and not MenuIsOpen("Dashboard.xml"))then
				m_minimizeTime = nil
				toggleToolbarMinimized(true)
			end
		end
	end

	if m_keyPressBufferTimer > 0 then
		m_keyPressBufferTimer = m_keyPressBufferTimer - 1
	end
	
	if MenuIsOpen("Emotes") then
		if m_hideNumeric == false then
			setNumericHandler({hideNumeric = true})
		end
	elseif m_hideNumeric == true then
		setNumericHandler({hideNumeric = false})
	end
end

-- Called when a key is pressed
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	local keyPressed = key-48
	-- If key 1-5 is pressed and shift is NOT pressed
	if (keyPressed>=1 and keyPressed<=6 and m_dynamicEnable) and m_alive then
		-- ED-2630 - A/B test for hiding toolbelt numbers when the emote menu is enabled
		if MenuIsOpen("Emotes") and not bShiftDown or
			MenuIsClosed("Emotes") and bShiftDown or
			bShiftDown then
			return
		end
		m_minimizeTime = MINIMIZE_DELAY_LONG
		toggleToolbarMinimized(false)
		-- Don't re-select same item
		--log("m_selectedIndex = "..tostring(m_selectedIndex)..", keyPressed = "..tostring(keyPressed))
		if (keyPressed == 1 or (m_toolbarInventory[keyPressed-1] and m_toolbarInventory[keyPressed-1].UNID ~= 0)) and m_keyPressBufferTimer <= 0 then
			m_keyPressBufferTimer = KEY_PRESS_BUFFER_TIME
			KEP_ClearSelection() -- Deselect all world items. TODO: Make sure this doesn't break anything else.
			selectItem(keyPressed)
		end
	end
end

-- Called when the dialog is moved
function Dialog_OnMoved(dialogHandle, x, y)
	toggleToolbarMinimized(m_minimized, true)

	updateLootAddedContextialHelp()
	updateSelectToolbeltContextualHelp()
	updateUseToolbeltContextualHelp()
	updateShowToolbeltContextualHelp()
end

-- show the loot added contextual help 
function showLootAddedContextualHelp()
	--- CONTEXTUAL HELP SHOW OPEN BACKPACK"
	ContextualHelper.showHelp(ContextualHelp.ADD_BACKPACK, MenuGetLocationThis(), Control_GetLocationX(gHandles["btnBackpack"]), Control_GetLocationY(gHandles["btnBackpack"]), Control_GetWidth(gHandles["btnBackpack"]), Control_GetHeight(gHandles["btnBackpack"]), ContextualHelpFormat.BOTTOM_ARROW)
end

function updateLootAddedContextialHelp()
	--- CONTEXTUAL HELP SHOW OPEN BACKPACK"
	ContextualHelper.updateHelp(ContextualHelp.ADD_BACKPACK, MenuGetLocationThis(), Control_GetLocationX(gHandles["btnBackpack"]), Control_GetLocationY(gHandles["btnBackpack"]), Control_GetWidth(gHandles["btnBackpack"]), Control_GetHeight(gHandles["btnBackpack"]), ContextualHelpFormat.BOTTOM_ARROW)
end

-- close the loot added contextual help 
function closeLootAddedContextualHelp()
	-- CONTEXTUAL HELP BACKPACK BUTTON CLICKED 
	ContextualHelper.completeHelp(ContextualHelp.ADD_BACKPACK)
end

-- show the select toolbelt contextual help
function showSelectToolbeltContextualHelp()
	local firstToolbeltItem = findFirstToolbeltItem()

	if firstToolbeltItem > 0 and not MenuIsOpen("Framework_Backpack.xml") then
		--log("--- showSelectToolbeltContextualHelp")
		-- CONTEXTUAL HELP USE TIEM ON TOOLBELT SHOW
		ContextualHelper.showHelp(ContextualHelp.SELECT_TOOLBELT, MenuGetLocationThis(), Control_GetLocationX(gHandles["btnItem".. tostring(firstToolbeltItem)]), Control_GetLocationY(gHandles["btnItem".. tostring(firstToolbeltItem)]), Control_GetWidth(gHandles["btnItem".. tostring(firstToolbeltItem)]), Control_GetHeight(gHandles["btnItem".. tostring(firstToolbeltItem)]), ContextualHelpFormat.BOTTOM_ARROW)
	end 
end

function updateSelectToolbeltContextualHelp()
	local firstToolbeltItem = findFirstToolbeltItem()

	if firstToolbeltItem > 0 and not MenuIsOpen("Framework_Backpack.xml") then
		-- CONTEXTUAL HELP USE TIEM ON TOOLBELT SHOW
		ContextualHelper.updateHelp(ContextualHelp.SELECT_TOOLBELT, MenuGetLocationThis(), Control_GetLocationX(gHandles["btnItem".. tostring(firstToolbeltItem)]), Control_GetLocationY(gHandles["btnItem".. tostring(firstToolbeltItem)]), Control_GetWidth(gHandles["btnItem".. tostring(firstToolbeltItem)]), Control_GetHeight(gHandles["btnItem".. tostring(firstToolbeltItem)]), ContextualHelpFormat.BOTTOM_ARROW)
	else 
		ContextualHelper.hideHelp(ContextualHelp.SELECT_TOOLBELT)
	end 
end

function updateShowToolbeltContextualHelp()
	-- B: update the contextual help for the auto-hide 
    local menuSize = MenuGetSizeThis()
    ContextualHelper.updateHelp(ContextualHelp.SHOW_TOOLBELT, MenuGetLocationThis(), 0, Control_GetHeight(gHandles["imgRolloverHighlight"]) * -1 + TOOLBAR_HELP_BUFFER, menuSize.width, menuSize.height, ContextualHelpFormat.BOTTOM_ARROW)
end

-- complete the select toolbelt contextual help
function closeSelectToolbeltContextualHelp()
	if isToolbeltItem(m_toolbarInventory[m_selectedIndex - 1]) then
		-- CONTEXTUAL HELP SELECTED ITEM ON TOOLBELT COMPLETE 
		ContextualHelper.completeHelp(ContextualHelp.SELECT_TOOLBELT)
	end

	-- show the contextual help 
	showUseToolbeltContextualHelp()
end

-- find the first item that is usable in the toolbelt for the contextual help 
function findFirstToolbeltItem()
	for i = 1, 5 do
		if isToolbeltItem(m_toolbarInventory[i]) then
			--log("--- firstToolbeltItem ".. tostring(i).. "-----------------------------------------------")
			return i + 1
		end 
	end 

	return -1
end

-- is this item able to be used in the toolbelt?
function isToolbeltItem(item)
	if not item or not item.properties or not item.properties.itemType then
		return false
	end 
	
	local itemType = item.properties.itemType
	
	-- only show the contextual help on a weapon that has ammo 
	if itemType == ITEM_TYPES.WEAPON then
		--printTable(item)
		if m_ammoCount[item.properties.ammoType] and m_ammoCount[item.properties.ammoType] > 0 then
			return true
		elseif item.properties.ammoType == nil or item.properties.ammoType == 0 then
			return true 
		end 
	elseif itemType == ITEM_TYPES.CHARACTER then
		if item.properties.behavior == "pet" then
			return true
		end
	elseif itemType == ITEM_TYPES.CONSUMABLE or itemType == ITEM_TYPES.PLACEABLE or itemType == ITEM_TYPES.HARVESTABLE or itemType == ITEM_TYPES.TOOL or itemType == ITEM_TYPES.TUTORIAL then
		return true
	end

	return false
end

function formatTooltips(item, isEquipped)
	if item then
		local itemUNID = tostring(item.UNID)

		item.tooltip = {}

		if not m_tooltipsEnabled then return item.tooltip end

		item.tooltip["header"] = {label = item.properties.name}

		if item.properties.rarity then
			item.tooltip["header"].color = COLORS_VALUES[item.properties.rarity]
		end

		if item.properties.description then
			item.tooltip["body"] = {label = "\"" .. item.properties.description .. "\""}
		end

		if item.properties.itemType == ITEM_TYPES.WEAPON then

			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}

			-- change the description when the 
			if item.properties.healingWeapon and item.properties.healingWeapon == true then
				item.tooltip["healing"] = {label = tostring(item.properties.damage * item.properties.bonusDamage)}
			else 
				item.tooltip["damage"] = {label = tostring(item.properties.damage * item.properties.bonusDamage)}
			end 

			item.tooltip["range"] = {label = tostring(item.properties.range)}

			if item.properties.ammoType and m_gameItemNamesByUNID[tostring(item.properties.ammoType)] then 
				if m_playerInventoryByUNID[item.properties.ammoType] == nil then
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.ammoType)]), color = {r = 255, g = 0, b = 0, a = 255}}
				else
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.ammoType)])}
				end
			elseif item.properties.ammoType then 
				getItemNameByUNID(item.properties.ammoType)
			end 

			local durability = item.instanceData.durability
			local durabilityType = item.properties.durabilityType or "repairable"
			local itemLevel = item.properties.level
			
			if durabilityType == "limited" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				if durabilityPercentage == 0 and durability > 0 then
					durabilityPercentage = 1
				end
				item.tooltip["durability"] = {label = tostring(durabilityPercentage),
												durability = durabilityPercentage}
			elseif durabilityType == "indestructible" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = "100%",
										  		durability = 100}
			else
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				if durabilityPercentage == 0 and durability > 0 then
					durabilityPercentage = 1
				end
				item.tooltip["durability"] = {label = tostring(durabilityPercentage).."%",
										 		durability = durabilityPercentage}
			end

			-- change the tooltip text if the weapon is equipped 
			if isEquipped then 
				item.tooltip["weapon"] = {label = "To attack - select in main toolbelt and left click a target."}
			else 
				item.tooltip["loot"] = {label = "To equip - drag to your toolbelt.", type = item.properties.itemType}
			end

		elseif item.properties.itemType == ITEM_TYPES.TOOL then
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}

			local durability = item.instanceData.durability
			local durabilityType = item.properties.durabilityType or "repairable"
			local itemLevel = item.properties.level

			if item.properties.paintWeapon then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["uses"] = {label = tostring(durability), 
											durability = durabilityPercentage}
			elseif durabilityType == "limited" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage),
												durability = durabilityPercentage}

			elseif durabilityType == "indestructible" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = "100%",
										  		durability = 100}
			else
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage).."%",
										 		durability = durabilityPercentage}
			end
			
			if isEquipped then
				item.tooltip["loot"] = {label = "To place - select in main toolbelt and left click a target.", type = item.properties.itemType}
			else 
				item.tooltip["loot"] = {label = "To use - drag to your toolbelt.", type = item.properties.itemType}
			end
		elseif item.properties.itemType == ITEM_TYPES.ARMOR then
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}
			item.tooltip["armorslot"] = {label = tostring(item.properties.slotType:gsub("^%l", string.upper))}

			local armorRating
			if item.properties.bonusHealth then
				armorRating = item.properties.armorRating * item.properties.bonusHealth
			else
				armorRating = item.properties.armorRating * 3
			end

			item.tooltip["armorrating"] = {label = tostring(armorRating)}
			
			local durability = item.instanceData.durability
			local durabilityType = item.properties.durabilityType or "repairable"
			local itemLevel = item.properties.level

			if durabilityType == "limited" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage),
												durability = durabilityPercentage}

			elseif durabilityType == "indestructible" then
				local durabilityPercentage = math.min(math.floor((durability/DURABILITY_BY_LEVEL[itemLevel]) * 100), 100)
				item.tooltip["durability"] = {label = "100%",
										  		durability = 100}
			else
				local durabilityPercentage = math.min(math.floor((durability/DEFAULT_MAX_DURABILITY) * 100), 100)
				item.tooltip["durability"] = {label = tostring(durabilityPercentage).."%",
										 		durability = durabilityPercentage}
			end

			item.tooltip["loot"] = {label = "Right-click to equip.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.CONSUMABLE then
			if item.properties.consumeType == "energy" then
				item.tooltip["energy"] = {label = tostring(item.properties.consumeValue) .. "%"}--, data = "consumeValueDisplay"}
			else
				item.tooltip["health"] = {label = tostring(item.properties.consumeValue) .. "%"}--, data = "consumeValueDisplay"}
			end
			if isEquipped then
				item.tooltip["loot"] = {label = "To consume - select in main toolbelt and left-click in the world.", type = item.properties.itemType}
			else
				item.tooltip["loot"] = {label = "Right-click to consume.", type = item.properties.itemType}
			end
		elseif item.properties.itemType == ITEM_TYPES.PLACEABLE or item.properties.itemType == ITEM_TYPES.TUTORIAL then
			if isEquipped then
				item.tooltip["loot"] = {label = "To place - select in main toolbelt and left-click in the world.", type = item.properties.itemType}
			else
				item.tooltip["loot"] = {label = "To use - drag to your toolbelt.", type = item.properties.itemType}
			end
		elseif item.properties.itemType == ITEM_TYPES.GENERIC then
			item.tooltip["loot"] = {label = "Generic loot.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.AMMO then
			item.tooltip["loot"] = {label = "Ammo is used by weapons.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.HARVESTABLE then
			if isEquipped then
				item.tooltip["loot"] = {label = "To place - select in main toolbelt and left-click in the world.", type = item.properties.itemType}
			else 
				item.tooltip["loot"] = {label = "To use - drag to your toolbelt.", type = item.properties.itemType}
			end

			-- get food properties 
			--log("--- food : ".. tostring(item.properties.food))
			if item.properties.food and m_gameItemNamesByUNID[tostring(item.properties.food)] then 
				item.tooltip["requires1"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.food)]).. " to grow"}
			elseif item.properties.food then 
				getItemNameByUNID(item.properties.food)
			end 

			-- get tool from properties 
			--log("--- requiredTool : ".. tostring(item.properties.requiredTool))
			if item.properties.requiredTool and m_gameItemNamesByUNID[tostring(item.properties.requiredTool)] then 
				item.tooltip["requires2"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.requiredTool)]).. " to harvest"}
			elseif item.properties.food then 
				getItemNameByUNID(item.properties.requiredTool)
			end 

			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}
		elseif item.properties.itemType == ITEM_TYPES.BLUEPRINT then
			item.tooltip["loot"] = {label = "Right-click to learn Recipe.", type = item.properties.itemType}
		elseif item.properties.itemType == ITEM_TYPES.CHARACTER and item.properties.behavior == "pet" then
			if item.properties.food and m_gameItemNamesByUNID[tostring(item.properties.food)] then 
				if m_playerInventoryByUNID[item.properties.food] == nil then
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.food)]), color = {r = 255, g = 0, b = 0, a = 255}}
				else
					item.tooltip["requires"] = {label = tostring(m_gameItemNamesByUNID[tostring(item.properties.food)])}
				end
			elseif item.properties.food then 
				getItemNameByUNID(item.properties.food)
			end
			item.tooltip["level"] = {label = "Level "..tostring(item.properties.level), level = item.properties.level}
			if isEquipped then
				item.tooltip["loot"] = {label = "To place this pet - select it in the main toolbelt and left-click in the world.", type = ITEM_TYPES.PET}
			else
				item.tooltip["loot"] = {label = "To use this pet - drag it to your toolbelt.", type = ITEM_TYPES.PET}
			end
		end

		-- add tooltip information for selling items 
		if m_gameItems[itemUNID] and m_gameItems[itemUNID].canSell and MenuIsOpen("Framework_Vendor.xml") then
			-- hack because sometimes the item count is 0???? 
			local itemCount = item.count
			if item.count == 0 then
				itemCount = 1
				--printTable(item)
			end 
			item.tooltip["sell"] = {canSell = true, label = "To sell this for ".. tostring(math.floor((m_gameItems[itemUNID].sellCost / 2) * itemCount)).. " ".. tostring(m_gameItems[itemUNID].sellName).. ", drag to vendor window."}
		else 
			item.tooltip["sell"] = {canSell = false, label = ""}
		end 
	end
end

-- show the use toolbelt contextual help
function showUseToolbeltContextualHelp()
	hideUseToolbeltContextualHelp()

	-- don't show if the selected item is not a toolbelt item 
	if not isToolbeltItem(m_toolbarInventory[m_selectedIndex - 1]) then
		return
	end 

	local bubbleWidth = Control_GetLocationX(gHandles["btnItem6"]) + Control_GetWidth(gHandles["btnItem6"]) - Control_GetLocationX(gHandles["btnItem1"])

	local menuSize = MenuGetSizeThis()
	ContextualHelper.showHelp(ContextualHelp.USE_TOOLBELT, MenuGetLocationThis(), 0, 0, menuSize.width, menuSize.height, ContextualHelpFormat.TOP_NO_ARROW, 20, bubbleWidth)
end

function updateUseToolbeltContextualHelp()
	local bubbleWidth = Control_GetLocationX(gHandles["btnItem6"]) + Control_GetWidth(gHandles["btnItem6"]) - Control_GetLocationX(gHandles["btnItem1"])

	local menuSize = MenuGetSizeThis()
	ContextualHelper.updateHelp(ContextualHelp.USE_TOOLBELT, MenuGetLocationThis(), 0, 0, menuSize.width, menuSize.height, ContextualHelpFormat.TOP_NO_ARROW, bubbleWidth)
end

-- hide the use toolbelt contextual help 
function hideUseToolbeltContextualHelp()
	-- CONTEXTUAL HELP USE ITEM ON TOOLBELT COMPLETE 
	ContextualHelper.hideHelp(ContextualHelp.USE_TOOLBELT)
end

-- complete the use toolbelt contextual help 
function closeUseToolbeltContextualHelp()
	-- CONTEXTUAL HELP USE ITEM ON TOOLBELT COMPLETE 
	ContextualHelper.completeHelp(ContextualHelp.USE_TOOLBELT)
end

-- Button to enter Creator mode
function btnCreatorMode_OnButtonClicked(buttonHandle)
	KEP_ClearSelection() -- Deselect all world items. TODO: Make sure this doesn't break anything else.
	local ev = KEP_EventCreate("FRAMEWORK_SET_PLAYER_MODE")
	KEP_EventEncodeString(ev, "God")
	KEP_EventQueue(ev) -- NOTE: Handled in Framework_PlayerHandler
end

function btnBackpack_OnButtonClicked(buttonHandle)
	-- Close Player System if it's open
	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")
	end

	closeLootAddedContextualHelp()

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "backpack")
	KEP_EventQueue(ev)
end

function btnMove_OnButtonClicked(buttonHandle)
	m_buildMode = MODE_MOVE

	local ev = KEP_EventCreate("FRAMEWORK_PLAYER_BUILD_CHANGE_MODE")
	KEP_EventEncodeNumber(ev, m_buildMode)
	KEP_EventQueue(ev)

	updateBuildHighlights()
end

function btnRotate_OnButtonClicked(buttonHandle)
	m_buildMode = MODE_ROTATE

	local ev = KEP_EventCreate("FRAMEWORK_PLAYER_BUILD_CHANGE_MODE")
	KEP_EventEncodeNumber(ev, m_buildMode)
	KEP_EventQueue(ev)

	updateBuildHighlights()
end

function btnUse_OnButtonClicked(buttonHandle)
	local ev = KEP_EventCreate("FRAMEWORK_USE_SELECTED_ITEM")
	KEP_EventQueue(ev)

	local selectedItem = m_gameItems[tostring(m_gameItemSelected)]
	if(selectedItem and selectedItem.behavior == "seat") then
		-- seats are special and have trigger the toolbar to dissapear when they are 'used'
		
		local showNum = 0
		local showEvent = KEP_EventCreate("FRAMEWORK_SHOW_BUILD_TOOLS")
		local allowsPIN = false
		
		KEP_EventEncodeNumber(showEvent, showNum) -- Whether or not to show build tools
		KEP_EventEncodeString(showEvent, tostring(allowsPIN)) -- Whether or not to show PIN
		KEP_EventEncodeNumber(showEvent, tonumber(selectedPID)) -- PID of selected object
		KEP_EventEncodeString(showEvent, nil) -- Name of selected object
		KEP_EventQueue(showEvent)
	end
end

function btnPickup_OnButtonClicked(buttonHandle)
	if m_permissions and m_buildSelected and m_buildSelected ~= 0 and m_gameItemSelected and m_pendingTransaction == nil and m_pendingPID ~= m_buildSelected then
		-- Owners, moderators and admins can pick up items straight out
		m_pendingPID = m_buildSelected
		local selectedItem = m_gameItems[tostring(m_gameItemSelected)]
		local removeTable = {}
		local addTable = {}

		local addItem = {}
		addItem.UNID = m_gameItemSelected
		addItem.properties = deepCopy(selectedItem)

		addItem.count = 1
		addItem.lootInfo = {sourcePID = m_buildSelected, lootSource = "PickUp"}

		table.insert(addTable, addItem)

		local isEventFlag = m_eventFlags[tostring(m_buildSelected)]

		local isClaimFlag = FlagSystem:isPlaceableObjectLandClaimFlag(m_buildSelected)

		if isEventFlag then
			MenuOpen("Framework_DeleteFlag.xml")
			local event = KEP_EventCreate("FRAMEWORK_DELETE_FLAG")
			KEP_EventEncodeNumber(event, m_buildSelected)
			KEP_EventEncodeString(event, "event")
			KEP_EventQueue(event)
			m_pendingPID = nil
		elseif isClaimFlag then
			MenuOpen("Framework_DeleteFlag.xml")
			local event = KEP_EventCreate("FRAMEWORK_DELETE_FLAG")
			KEP_EventEncodeNumber(event, m_buildSelected)
			if m_flagBuildOnly then
				KEP_EventEncodeString(event, "claimOnly")
			else
				KEP_EventEncodeString(event, "claim")
			end
			KEP_EventQueue(event)
			m_pendingPID =nil
		else
			m_pendingTransaction = addTable
			if selectedItem.behavior == "loot"  then
				Events.sendEvent("FRAMEWORK_ATTEMPT_PICKUP_SAFE", {PID = m_buildSelected})
			elseif selectedItem.behavior == "wheeled_vehicle" then
				displayStatusMessage("Cannot pick up vehicles.")
			else
				
				processTransaction(removeTable, addTable, true)
			end
		end
	end
end

function btnShare_OnButtonClicked(buttonHandle)
	if MenuIsClosed("Framework_PINCode.xml") then
		MenuOpen("Framework_PINCode.xml")

		local ev = KEP_EventCreate("FRAMEWORK_INITIALIZE_PIN")
		KEP_EventEncodeNumber(ev, m_selectedPID)
		KEP_EventEncodeString(ev, m_selectedName)
		KEP_EventQueue(ev)

	else
		MenuClose("Framework_PINCode.xml")
	end
end

-------------------
-- Event Handlers
-------------------
local NUM_KEYS = 6
local NUM_KEY_BGS = 4
function setNumericHandler(event)
	if event.hideNumeric == nil then return end
	
	if m_hideNumeric ~= event.hideNumeric then
		m_hideNumeric = event.hideNumeric
		-- ED-2630 - A/B test for hiding toolbelt numbers when the emote menu is enabled
		for i = 1, NUM_KEYS do
			if gHandles["stcKey"..tostring(i)] then
				Control_SetVisible(gHandles["stcKey"..tostring(i)], not m_hideNumeric)
				for j = 1, NUM_KEY_BGS do
					if gHandles["stcKey"..tostring(i).."BG"..tostring(j)] then
						Control_SetVisible(gHandles["stcKey"..tostring(i).."BG"..tostring(j)], not m_hideNumeric)
					end
				end
			end
		end
	end
end

function playerOccupiedHandler(event)
	if event.occupied and not m_playerOccupied then
		m_playerOccupied = true
		m_previousIndex = m_selectedIndex
		selectNone()
		m_controlsShowing = true
		swapToolbarAnimation(false)
	elseif not event.occupied and m_playerOccupied then
		m_controlsShowing = false
		swapToolbarAnimation(true)
		m_playerOccupied = false
		selectItem(m_previousIndex)
	end
end

function lastGameItemHandler(event)
	if event.lastGameItem then
		m_lastGameItem = event.lastGameItem
	else
		m_lastGameItem = nil
	end
end

function permissionsAndCreatorEvent(event)
	m_permissions = tonumber(event.perm)
	m_creatorModeEnabled = event.creatorModeEnabled
	
	Control_SetVisible(gHandles["btnCreatorMode"], false)
end

function onReturnCurrentTime(event)
	if event.timestamp then
		m_timeStamp = tonumber(event.timestamp)
		m_timeStamp = math.fmod(m_timeStamp, 604800) --modded away weeks
	end
end

-- Called when a Game Item placement has been confirmed or denied from load
function placeGameItemConfirmation(event)
	--log("---- Framework_Toolbar placeGameItemConfirmation")
	if m_placingItem then
		-- If placement successful
		if event.success then
			local ev = KEP_EventCreate("FRAMEWORK_BUILD_PLACE_ITEM")
			--KEP_EventEncodeNumber(ev, m_toolbarInventory[slot].properties.GLID)
			KEP_EventQueue(ev)
		
			-- Remove this item from your inventory
			decrementItem(m_selectedIndex-1)
		
			selectHand()
		-- Failed
		else
			-- Open the GameItemLoadWarning menu and pass it a message
			if MenuIsClosed("Framework_GameItemLoadWarning.xml") then
				MenuOpen("Framework_GameItemLoadWarning.xml")
			end
			local ev = KEP_EventCreate("FRAMEWORK_GAME_ITEM_LOAD_MESSAGE")
			KEP_EventEncodeString(ev, GI_LOAD_MESSAGE1)
			KEP_EventEncodeString(ev, GI_LOAD_MESSAGE2)
			KEP_EventQueue(ev)
		end
		m_placingItem = false
	end
	m_invalidPlacement = false
end

function onPickUpReturn(event)
	--Sanity check to make sure didn't switch selections between events
	if event.PID and m_buildSelected == event.PID and m_pendingTransaction and event.success == true then
		processTransaction({}, m_pendingTransaction, true)
	else
		m_pendingTransaction = nil
		m_pendingPID = nil
	end
end

function setArmor(event)
	m_armorRating = event.armorRating	
end

-- Called when a specific item has been modified from Framework_InventoryHandler
function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	-- Keep previous selected item for comparing to new for swap detection
	local tempItem = {}
	local swapped = false
	
	if m_selectedIndex > 1 then
		tempItem = deepCopy(m_toolbarInventory[m_selectedIndex-1])
	end
	
	local inventory = m_toolbarInventory
	if inventory[updateIndex] then
		inventory[updateIndex] = updateItem
		updateInventory(inventory)
		
		-- Swap compare check
		if tempItem and m_toolbarInventory[m_selectedIndex-1] then
			local identical = detectSimilarTables(tempItem, m_toolbarInventory[m_selectedIndex-1])
			if not identical then
				swapped = true
			end
		end
		
		-- If swap detected
		if swapped then
			selectItem(m_selectedIndex)
		end
	end

	updateSelectToolbeltContextualHelp()
end

-- Called when the entire Inventory is sent fromFramework_InventoryHandler
function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local inventory = decompileInventory(tEvent)
	updateInventory(inventory)

	updateSelectToolbeltContextualHelp()
end

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
end


function updateQuestsClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- Dynamic HUD A/B test. Just checking to see if the user has an incomplete quest.
	if not m_dynamicEnable then
		local quests = decompileInventory(tEvent)
		for i,info in pairs(quests) do
			if info ~= nil and info.UNID ~= nil then
				if not info.complete then
					m_dynamicEnable = true
					if not m_shownAfterProgress then
						m_shownAfterProgress = true
						toggleToolbarMinimized(false)
						m_minimizeTime = MINIMIZE_DELAY_MEDIUM
					elseif not m_minimizedSetting then
						toggleToolbarMinimized(false)
					end
					return
				end
			end
		end
	end
end

-- Called when a click event is received
function clickEventHandler(dispatcher, fromNetid, event, eventID, filter, objectID)

	Debug.printTable("Framework_Toolbar clickEventHandler ", event)
	
	
	local button = KEP_EventDecodeNumber(event)
	-- Don't consume clicks if the toolbar's minimized or if a right click was fired
	if (button == RIGHT_CLICK) then return end
	
	-- Activate item types that aren't weapons if one's selected
	if (m_selectedIndex > 1) and (not m_weaponSelected) then
		activateItem(m_selectedIndex - 1)
	end
	
	-- NOTE: Framework_BattleHandler handles battle-related clicks itself (targeting and such)
end

-- Event entry into Framework.  Sent from PlaceableObj.lua; fowards to the inventory controller.
function frameworkShowBuildToolsHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	
	m_buildVisible = KEP_EventDecodeNumber(tEvent) == 1 
	m_allowsPIN = KEP_EventDecodeString(tEvent) == "true"
	m_selectedPID = KEP_EventDecodeNumber(tEvent)
	m_selectedName = KEP_EventDecodeString(tEvent)
	
	local show = m_buildVisible and m_selectedIndex == 1
	
	-- don't show btnShare unless relevant
	for i, name in pairs(m_buildPopupControls) do
		local tempShow = show and ((name == "btnShare" and m_allowsPIN) or name ~= "btnShare")
		
		if(name == "btnUse") then
			tempShow = show and checkIfItemIsUsable()
		end
		
		Control_SetVisible(gHandles[name], tempShow)
		Control_SetEnabled(gHandles[name], tempShow)
		if tempShow then
			Dialog_MoveControlToFront(gDialogHandle, gHandles[name])
		else
			Dialog_MoveControlToBack(gDialogHandle, gHandles[name])
		end
		--log("--- frameworkShowBuildToolsHandler ".. tostring(name))
	end

	if show then 
		local ev = KEP_EventCreate("FRAMEWORK_PLAYER_BUILD_CHANGE_MODE")
		KEP_EventEncodeNumber(ev, m_buildMode)
		KEP_EventQueue(ev)

		toggleToolbarMinimized(false, true)
	else
		toggleToolbarMinimized(true)
	end

	updateBuildHighlights()
end

-- Set the selected object
function setSelectedObjectHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_buildSelected = KEP_EventDecodeNumber(event)
	if KEP_EventMoreToDecode(event) ~= 0 then
		m_gameItemSelected = KEP_EventDecodeNumber(event)
	end
end

-- Check to see if user's currently selected item is defined as usable
function checkIfItemIsUsable()
	local selectedItem = m_gameItems[tostring(m_gameItemSelected)]
		
	if(selectedItem) then
		-- verify that the selected object's behavior is usable
		for _,itemBehavior in pairs(usableItems) do
			if selectedItem.behavior == itemBehavior then
				return true	
			end
		end
		
		-- Containers and Safes are special
		if(selectedItem.behavior == "loot") then
			if(selectedItem.name == "Chest") then return true end
			if(selectedItem.behaviorParams and selectedItem.behaviorParams.locked) then return true end
		end
		
		return false
	else
		return false
	end

end


-- Called when the player enters or exits god mode
function frameworkBuildModeHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_buildSettingToolbar = KEP_EventDecodeString(event) == "true"
	m_noBuildReason = KEP_EventDecodeString(event)
	Log("new build mode : build mode handler activated : "..tostring(m_buildSettingToolbar))
end

-- Handles requests to select hand mode (sent from Framework_BattleHandler.lua when ammo has been depleted)
function selectHandHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	selectHand()
end

function selectIndexHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local index = KEP_EventDecodeNumber(event)
	selectItem(index + 1)
end

-- Handles weapon attacks. Queues the attack flash
function attackFlashHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local weapon = Events.decode(KEP_EventDecodeString(event))
	if m_toolbarInventory[m_selectedIndex-1].UNID == weapon.UNID then
		local rateOfFire = weapon.properties.rateOfFire or 1
		m_toolbarInventory[m_selectedIndex-1].cooldownTime = m_timeStamp + rateOfFire
		m_toolbarInventory[m_selectedIndex-1].fireTime = m_timeStamp
		updateItemLocal(m_selectedIndex-1, INVENTORY_PID, m_toolbarInventory[m_selectedIndex-1])
		local cooldownHandle = gHandles["imgCooldown"..tostring(m_selectedIndex)]
		Control_SetSize(cooldownHandle, Control_GetWidth(cooldownHandle), 0)
		Control_SetVisible(cooldownHandle, true)
	end

	-- Queue the cooldown for currently selected item
	m_attackFlashTime = ATTACK_FLASH_TIME
	
	Control_SetVisible(gHandles["imgAttackFlash"], not m_minimized)

	closeUseToolbeltContextualHelp()
end

function unlockToolbarHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local glow = KEP_EventDecodeString(event) == "true"
	toggleToolbarMinimized(not glow, glow)

	for i = 2, TOOLBAR_SLOTS+1 do
		Control_SetVisible(gHandles["imgItemGlow"..i], glow)
		BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgItemGlow"..i])), 0, {a = 140, r = 255, g = 255, b = 255})
	end

	showSelectToolbeltContextualHelp()
end

-- Handles the "openContextualMenuEvent" sent when contextual help is started.
function openContextualMenuEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local eventParams = KEP_EventDecodeString(tEvent)
	eventParams = Events.decode(eventParams)
	local helpID = eventParams.type
	if helpID == ContextualHelp.SELECT_TOOLBELT
			or helpID == ContextualHelp.ADD_BACKPACK
			or helpID == ContextualHelp.USE_TOOLBELT 
			or helpID == ContextualHelp.TOOLBELT_VEHICLE
			or helpID == ContextualHelp.TOOLBELT_PET
			or helpID == ContextualHelp.TOOLBELT_PLACEABLE 
			or helpID == ContextualHelp.TOOLBELT_CONSUMABLE 
			or helpID == ContextualHelp.TOOLBELT_TOOL
			or helpID == ContextualHelp.TOOLBELT_WEAPON then
		m_showingContextualHelp = true
		toggleToolbarMinimized(false, true)
	elseif helpID == ContextualHelp.SHOW_TOOLBELT then
		local imgHighlight = gHandles["imgRolloverHighlight"]
		-- turn the highlight on
		Control_SetVisible(imgHighlight, true)
		Control_SetEnabled(imgHighlight, true)
		-- get the width of the window 
		local windowWidth = Dialog_GetScreenWidth(gDialogHandle)
		local windowHeight = Dialog_GetScreenHeight(gDialogHandle)
		-- make the highlight the size of the window
		Control_SetSize(imgHighlight, windowWidth + 20, Control_GetHeight(imgHighlight))
		-- make it streach across the screen
		local  menuLocation = MenuGetLocationThis()
		local menuSize = MenuGetSizeThis()
		local highlightX = menuLocation.x - windowWidth + menuSize.width
		local highlightY = (menuLocation.y - windowHeight) - Control_GetHeight(imgHighlight)
		Control_SetLocation(imgHighlight, highlightX, highlightY)
	end 
end

-- Handles the "closeContextualMenuEvent" sent when contextual help is closed.
function closeContextualMenuEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local eventParams = KEP_EventDecodeString(tEvent)
	eventParams = Events.decode(eventParams)
	local helpID = eventParams.type
	if helpID == ContextualHelp.SELECT_TOOLBELT
			or helpID == ContextualHelp.ADD_BACKPACK
			or helpID == ContextualHelp.USE_TOOLBELT 
			or helpID == ContextualHelp.TOOLBELT_VEHICLE
			or helpID == ContextualHelp.TOOLBELT_PET
			or helpID == ContextualHelp.TOOLBELT_PLACEABLE 
			or helpID == ContextualHelp.TOOLBELT_CONSUMABLE 
			or helpID == ContextualHelp.TOOLBELT_TOOL
			or helpID == ContextualHelp.TOOLBELT_WEAPON then
		m_showingContextualHelp = false
		m_minimizeTime = MINIMIZE_DELAY_SHORT
	elseif helpID == ContextualHelp.SHOW_TOOLBELT then
		-- turn the highlight off 
		Control_SetVisible(gHandles["imgRolloverHighlight"], false)
		Control_SetEnabled(gHandles["imgRolloverHighlight"], false)
	end 
end

-- Handles the "frameworkMinimizeHUD" event triggered when hud is hidden.
function frameworkMinimizeHUDHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == MINIMIZE_TOOLBELT_FILTER then
		local checked = Event_DecodeNumber(event)
		local instant = Event_DecodeNumber(event)
		local dynamicEnable = Event_DecodeNumber(event)
		m_dynamicEnable = m_dynamicEnable or m_dynamicEnable == 1
		toggleToolbarMinimized(checked == 1, instant == 1)
	end
end

-- Handles the "HideToolbeltSettingEvent" triggered when hud is hidden.
function hideToolbeltSettingEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local checked = Event_DecodeNumber(event)
	
	m_minimizedSetting = checked == 1

	if m_minimized ~= m_minimizedSetting then
		toggleToolbarMinimized(m_minimizedSetting)
	end
end

function onPreprocessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	if success then
		if m_permissions < 3 then
			KEP_EventCreateAndQueue("PickUpSelectedObjectsEvent")
			if not m_lastGameItem then
				processTransaction({}, m_pendingTransaction)
			end

			--Events.sendEvent("FRAMEWORK_PICKUP_GAME_ITEM", {PID = m_buildSelected})
		else
			processTransaction({}, m_pendingTransaction)
			KEP_ClearSelection()
			Events.sendEvent("FRAMEWORK_PICKUP_GAME_ITEM", {PID = m_buildSelected})
				
			local ev = KEP_EventCreate("SelectEvent")
			KEP_EventEncodeNumber(ev, 1)
			KEP_EventEncodeNumber(ev, ObjectType.DYNAMIC)
			KEP_EventEncodeNumber(ev, m_buildSelected)
			KEP_EventEncodeNumber(ev, 0)
			KEP_EventQueue(ev)
		end
	else
		displayStatusMessage(INVENTORY_ERRORS.BACKPACK_FULL)
		m_pendingPID = nil
	end

	m_pendingTransaction = nil
end

-- Callback function triggered when minimize animation completes.
function onMinimizeStop()
	if m_swappingToolbelt == true then
		showToolbeltButtons(not m_controlsShowing)
		m_swappingToolbelt = false
		toggleToolbarMinimized(false)
		-- B: hide the contextual help for the autohide
		ContextualHelper.hideHelp(ContextualHelp.SHOW_TOOLBELT)
	-- B: show the contextual help for the autohide 
	elseif m_minimized then
		local menuSize = MenuGetSizeThis()
		ContextualHelper.showHelp(ContextualHelp.SHOW_TOOLBELT, MenuGetLocationThis(), 0, Control_GetHeight(gHandles["imgRolloverHighlight"]) * -1 + TOOLBAR_HELP_BUFFER, menuSize.width, menuSize.height, ContextualHelpFormat.BOTTOM_ARROW, 20)
	end

	updateLootAddedContextialHelp()
	updateSelectToolbeltContextualHelp()
	updateUseToolbeltContextualHelp()
	updateShowToolbeltContextualHelp()
end

function placeClaimFlagConfirmation(event)
	m_invalidPlacement = true
	m_placingItem = false
	placeGameItemConfirmation({success = false})
end

function mediaHandler(event)
	m_invalidPlacement = true
	m_placingItem = false
	placeGameItemConfirmation({success = false})
end


function onEventFlagsReturnedFull(dispatcher, fromNetId, event, eventId, filter, objectId)
	m_eventFlags = Events.decode(Event_DecodeString(event))
end

function onEventFlagReturned(dispatcher, fromNetId, event, eventId, filter, objectId)
	local PID =  KEP_EventDecodeString(event)
	m_eventFlags[PID] = 1
end

function requestEventFlags()
	local event = KEP_EventCreate("FRAMEWORK_REQUEST_EVENT_FLAGS_LOCALLY")
	KEP_EventQueue(event)
end

function onFlagsReturnedFull(dispatcher, fromNetid, event, eventid, filter, objectid)

	local flags = Events.decode(KEP_EventDecodeString(event))
	FlagSystem:resetFlags(flags)

end

function onFlagReturned(dispatcher, fromNetid, event, eventid, filter, objectid )
	local PID = KEP_EventDecodeNumber(event)
	if KEP_EventMoreToDecode(event) ~= 0 then
		flag = Events.decode(KEP_EventDecodeString(event))
		FlagSystem:addFlag(flag)
		return
	end
	FlagSystem:removeFlag(PID)
end


function DragDrop.dialogOnMouseMoveHandler(dialogHandle, x, y)
	if MenuIsOpen("Framework_Backpack.xml") then
   	 	DragDrop.dialogOnMouseMove(dialogHandle, x, y)

   	 	if m_elementExists then
   	 		if Control_ContainsPoint(gHandles["imgDialogBG"], x, y) == 0  then
				MenuSendToBackThis()
			end
			for i = 2, TOOLBAR_SLOTS+1 do
				if Control_ContainsPoint(gHandles["imgItemGlow"..i], x, y) ~= 0 then 
					BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgItemGlow"..i])), 0, {a = 255, r = 255, g = 255, b = 255})
				else
					BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgItemGlow"..i])), 0, {a = 140, r = 255, g = 255, b = 255})
				end
			end

   	 	end
   	end
end

function DragDrop.buttonOnMouseEnterHandler(btnHandle)
	m_requestHandle = btnHandle
	local requestEvent = KEP_EventCreate("DRAG_DROP_REQUEST_ELEMENT")
	KEP_EventSetFilter(requestEvent, 4)
	KEP_EventQueue(requestEvent)
end

function onReturnElement(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_elementExists = KEP_EventDecodeString(tEvent) == "true"
	if m_elementExists then
		MenuBringToFrontThis()
	end
	if m_requestHandle then
		DragDrop.buttonOnMouseEnter(m_requestHandle)
	end
end

function  onNewItemHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_newItemType = KEP_EventDecodeString(tEvent)

	m_showNewMessage = true
	toggleToolbarMinimized(false)
end

function onCustomDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	--log("--- onCustomDrop")
	m_elementExists = false
	for i = 2, TOOLBAR_SLOTS+1 do
		BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgItemGlow"..i])), 0, {a = 140, r = 255, g = 255, b = 255})			
	end
	MenuSendToBackThis()
end

function ProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if (filter == PROGRESS_CLOSED) then
		if m_dynamicEnable then
			toggleToolbarMinimized(false)
			m_minimizeTime = MINIMIZE_DELAY_MEDIUM
		else
			toggleToolbarMinimized(true, true)
		end
	end
end

-------------------
-- Local Functions
-------------------
getMaximizedLocation = function()
	local menuLoc = MenuGetLocationThis()
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	return {x = menuLoc.x, y = screenHeight - MINIMIZE_DISPLACEMENT}
end

-- Called when the toolbar is hidden or revealed due via animation.
toggleToolbarMinimized = function(minimized, instant)

	if minimized and not canMinimizeToolbar() and m_swappingToolbelt == false then return end
	
	-- Dynamic HUD A/B test, override min/max if dynamic rules not met.
	if not minimized and (not m_dynamicEnable or MenuIsOpen("ProgressMenu.xml")) and canMinimizeToolbar() then
		minimized = true
	end

	if instant then
		local menuLoc = MenuGetLocationThis()
		local screenHeight = Dialog_GetScreenHeight(gDialogHandle)

		m_minimizeAnimation:stop()
		MenuSetLocationThis(menuLoc.x, screenHeight - MINIMIZE_DISPLACEMENT)	
		m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT, MINIMIZE_DURATION, nil, Easing.inOutQuad)

		MenuSetLocationThis(menuLoc.x, screenHeight + (minimized and 0 or -MINIMIZE_DISPLACEMENT))	


	elseif m_minimized ~= minimized then
		m_minimizeAnimation:stop()
		if minimized then
			m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT, MINIMIZE_DURATION * 2, nil, Easing.inOutQuad, true)
		else
			m_minimizeAnimation:move(0, MINIMIZE_DISPLACEMENT, MINIMIZE_DURATION, nil, Easing.inOutQuad, true)
		end
		m_minimizeAnimation:reverse(not minimized)
		m_minimizeAnimation:start()
	end

	updateLootAddedContextialHelp()
	updateSelectToolbeltContextualHelp()
	updateUseToolbeltContextualHelp()
	updateShowToolbeltContextualHelp()
	
	-- Let others know that the toolbelt's status has changed 
	local ev = KEP_EventCreate("FRAMEWORK_MINIMIZE_HUD")
	KEP_EventSetFilter(ev, MINIMIZE_INDICATORS_FILTER)
	KEP_EventEncodeNumber(ev, minimized and 1 or 0)
	KEP_EventEncodeNumber(ev, instant and 1 or 0)
	KEP_EventEncodeNumber(ev, m_dynamicEnable and 1 or 0)
	local showVehicleInsctions = 0
	if m_controlsShowing then
		showVehicleInsctions = 1
	end
	KEP_EventEncodeNumber(ev, showVehicleInsctions)
	KEP_EventQueue(ev)

	m_minimized = minimized

	-- B: complete the contextual help for the auto hide 
	if not minimized then
		ContextualHelper.completeHelp(ContextualHelp.SHOW_TOOLBELT)
	end 
end

-- Called to determine if the toolbar is in a valid state to minimize.
canMinimizeToolbar = function()
	return not (m_buildVisible and m_selectedIndex == 1) 
			and m_minimizeTime == nil
			and (m_minimizedSetting or not m_dynamicEnable)
			and not MenuIsOpen("Framework_Backpack.xml")
			and not m_showingContextualHelp
			and hideMessageTween == nil 
			and showMessageTween == nil
			and not m_controlsShowing
end

-- Called when the Inventory is updated from the local handler
updateInventory = function(inventory)
	if #inventory < TOOLBAR_SLOTS then Console.Write(Console.DEBUG, "Toolbar received invalid inventory!") return end
	
	m_toolbarInventory = inventory
	m_ammoCount = {}
	local inventoryItemCount = 0

	-- Extract toolbar inventory info
	for i = 1, #m_toolbarInventory do
		local item = m_toolbarInventory[i]
		
		-- Check if this index had a cooldown ticking on it
		if i <= 5 then
			local cooldownHandle = gHandles["imgCooldown"..tostring(i+1)]
			if Control_GetVisible(cooldownHandle) ~= 0 and not item.fireTime then
				Control_SetVisible(cooldownHandle, false)
			elseif Control_GetVisible(cooldownHandle) == 0 and (item.itemType == ITEM_TYPES.WEAPON) and item.fireTime then
				Control_SetVisible(cooldownHandle, true)
			end
		end
		
		-- Create ammoCount fields for weapons with valid ammoTypes
		if item.properties.itemType == ITEM_TYPES.WEAPON and item.properties.ammoType and item.properties.ammoType ~= 0 then
			m_ammoCount[item.properties.ammoType] = 0
		end

		if item.UNID and item.UNID > 0 then
			inventoryItemCount = inventoryItemCount + 1
		end

		-- format the tooltips 
		formatTooltips(item, true)
	end
	
	-- Extract ammo counts for each weapon
	for ammoUNID,count in pairs(m_ammoCount) do
		for i=1, #m_toolbarInventory do
			if m_toolbarInventory[i].UNID and m_toolbarInventory[i].UNID == ammoUNID then
				m_ammoCount[ammoUNID] = m_ammoCount[ammoUNID] + m_toolbarInventory[i].count
			end
		end
	end

	updateToolbar()

	-- Dynamic HUD A/B test. Show if already showing, or if the user has an item in their inventory.
	m_dynamicEnable = m_dynamicEnable or inventoryItemCount > 0 or m_armorRating > 0
	if m_dynamicEnable then
		if not m_shownAfterProgress then
			m_shownAfterProgress = true
			toggleToolbarMinimized(false)
			m_minimizeTime = MINIMIZE_DELAY_MEDIUM
		elseif not m_minimizedSetting then
			toggleToolbarMinimized(false)
		end
	end
end

-- Set toolbar accordingly
updateToolbar = function()
	for i=1, TOOLBAR_SLOTS do
		local toolbarIndex = i+1
		local item = m_toolbarInventory[i]
		if item then
			-- Set itemCount icons
			if item.count > 0 then
				local singleItem = tablePopulated(item.instanceData) or (item.properties.stackSize and item.properties.stackSize <= 1)
				

				Static_SetText(gHandles["stcItemCount" .. toolbarIndex], formatNumberSuffix(item.count))
				Control_SetVisible(gHandles["stcItemCount" .. toolbarIndex], not singleItem)
				setCountBG(gHandles["imgCountBG" .. toolbarIndex], gHandles["stcItemCount" .. toolbarIndex], true)
				Control_SetVisible(gHandles["imgCountBG" .. toolbarIndex], not singleItem)
				
				-- Set ammoCounts if this item is a weapon
				if item.properties.itemType == ITEM_TYPES.WEAPON and item.properties.ammoType and item.properties.ammoType ~= 0 then
					Static_SetText(gHandles["stcItemAmmo" .. toolbarIndex], m_ammoCount[item.properties.ammoType])
					setCountBG(gHandles["imgAmmoBG" .. toolbarIndex], gHandles["stcItemAmmo" .. toolbarIndex], false)
					Control_SetVisible(gHandles["imgAmmoBG" .. toolbarIndex], true)
				else
					Static_SetText(gHandles["stcItemAmmo" .. toolbarIndex], "")
					Control_SetVisible(gHandles["imgAmmoBG" .. toolbarIndex], false)
				end
				
				-- Set Durability bar?
				if item.instanceData and item.instanceData.durability then
					-- Set durability bar
					local durability = item.instanceData.durability
					local durabilityType = item.properties.durabilityType or "repairable"
					local durabilityPercentage = 0
					local itemLevel = item.properties.level

					if durabilityType == "limited" then
						durabilityPercentage = math.min((durability/DURABILITY_BY_LEVEL[itemLevel]), 1)
					elseif durabilityType == "indestructible" then
						durabilityPercentage = 1
					else
						durabilityPercentage = math.min((durability/DEFAULT_MAX_DURABILITY), 1)
					end
					
					-- Set bar size
					Control_SetSize(gHandles["imgDurabilityBar" .. toolbarIndex], durabilityPercentage * DURABILITY_BAR_WIDTH, 6)
					
					-- Set bar color
					local durColor = getDurabilityColor(durabilityPercentage)
					Element_SetCoords(Image_GetDisplayElement(gHandles["imgDurabilityBar" .. toolbarIndex]), unpack(durColor))
					
					-- Set bar's visible
					Control_SetVisible(gHandles["imgDurabilityBar" .. toolbarIndex], true)
					Control_SetVisible(gHandles["imgDurabilityBG" .. toolbarIndex], true)
					Control_SetVisible(gHandles["imgDurabilityGradiant" .. toolbarIndex], true)
					
					-- Is the item broken?
					Control_SetVisible(gHandles["imgItemBroken" .. toolbarIndex], durability <= 0)
					
					-- Push the ammoCount image and static up
					Control_SetLocationY(gHandles["imgAmmoBG" .. toolbarIndex], 28)
					Control_SetSize(gHandles["stcItemAmmo" .. toolbarIndex], 44, 40)
				else
					-- Hide durability bar
					Control_SetVisible(gHandles["imgDurabilityBar" .. toolbarIndex], false)
					Control_SetVisible(gHandles["imgDurabilityBG" .. toolbarIndex], false)
					Control_SetVisible(gHandles["imgDurabilityGradiant" .. toolbarIndex], false)
					-- Hide broken image
					Control_SetVisible(gHandles["imgItemBroken" .. toolbarIndex], false)
				
					-- Push the ammoCount image and static up
					Control_SetLocationY(gHandles["imgAmmoBG" .. toolbarIndex], 34)
					Control_SetSize(gHandles["stcItemAmmo" .. toolbarIndex], 44, 46)
				end
				
				Control_SetVisible(gHandles["imgItem"..toolbarIndex], true)
				
				applyImage(gHandles["imgItem" .. toolbarIndex], item.properties.GLID)

			-- Empty item
			else
				Static_SetText(gHandles["stcItemCount" .. toolbarIndex], "")
				Control_SetVisible(gHandles["imgCountBG" .. toolbarIndex], false)
				Control_SetVisible(gHandles["imgAmmoBG" .. toolbarIndex], false)
				Static_SetText(gHandles["stcItemAmmo" .. toolbarIndex], "")
				
				-- Hide durability bar visible
				Control_SetVisible(gHandles["imgDurabilityBar" .. toolbarIndex], false)
				Control_SetVisible(gHandles["imgDurabilityBG" .. toolbarIndex], false)
				Control_SetVisible(gHandles["imgDurabilityGradiant" .. toolbarIndex], false)
				-- Hide broken image
				Control_SetVisible(gHandles["imgItemBroken" .. toolbarIndex], false)
				
				-- Hide the imgItem to display the background default image
				Control_SetVisible(gHandles["imgItem"..toolbarIndex], false)
				
				-- Ensure this item wasn't previously selected
				if (m_selectedIndex > 1) and (m_selectedIndex == i+1) then
					selectHand()
				end
			end
		else
			Static_SetText(gHandles["stcItemCount" .. toolbarIndex], "")
			Control_SetVisible(gHandles["imgCountBG" .. toolbarIndex], false)
			Control_SetVisible(gHandles["imgAmmoBG" .. toolbarIndex], false)
			Static_SetText(gHandles["stcItemAmmo" .. toolbarIndex], "")
			
			-- Hide durability bar visible
			Control_SetVisible(gHandles["imgDurabilityBar" .. toolbarIndex], false)
			Control_SetVisible(gHandles["imgDurabilityBG" .. toolbarIndex], false)
			Control_SetVisible(gHandles["imgDurabilityGradiant" .. toolbarIndex], false)
			-- Hide broken image
			Control_SetVisible(gHandles["imgItemBroken" .. toolbarIndex], false)
			
			-- Hide the imgItem to display the background default image
			Control_SetVisible(gHandles["imgItem"..toolbarIndex], false)
			
			-- Ensure this item wasn't previously selected
			if (m_selectedIndex > 1) and (m_selectedIndex == toolbarIndex) then
				selectHand()
			end
		end
	
		DragDrop.registerItem(i, INVENTORY_PID, m_toolbarInventory[i])
	end
end

selectNone = function()
	deSelectItem(m_selectedIndex, false)
	if m_selectedIndex > 1 then
		Control_SetVisible(gHandles["imgItemEquipped" .. m_selectedIndex], false)
	end
	-- Inform the Player Build Mode menu that we're no longer ghosting objects around
	local ev = KEP_EventCreate("FRAMEWORK_BUILD_HAND_MODE")
	KEP_EventEncodeString(ev, "false")
	KEP_EventQueue(ev)
	
	m_selectedIndex = 0
	m_weaponSelected = false

	hideUseToolbeltContextualHelp()
end

-- Select the hand icon
selectHand = function()
	if m_playerOccupied then 
		displayOccupiedError()
		return
	end
	KEP_ClearSelection() -- Deselect all world items. TODO: Make sure this doesn't break anything else.
	local ev = KEP_EventCreate("FRAMEWORK_BUILD_HAND_MODE")
	KEP_EventEncodeString(ev, "true")
	KEP_EventQueue(ev)
	
	Events.sendEvent("PLAYER_TARGET", {type = "Clear"})
	
	deSelectItem(m_selectedIndex, false)
	
	if m_selectedIndex > 1 then
		Control_SetVisible(gHandles["imgItemEquipped" .. m_selectedIndex], false)
	end
	m_selectedIndex = 1
	Control_SetVisible(gHandles["imgItemEquipped1"], true)
	m_weaponSelected = false

	hideUseToolbeltContextualHelp()
end

-- Select an item
selectItem = function(index)
	if m_playerOccupied then 
		displayOccupiedError()
		return
	end
	KEP_ClearSelection() -- Deselect all world items. TODO: Make sure this doesn't break anything else.
	if m_placingItem then log("Can't select another item until the placement request has been processed"); return end
	
	index = tonumber(index)
	
	-- Select the hand?
	if index == 1 then
		selectHand()
		return
	end
	
	local item = m_toolbarInventory[index-1]
	local equippingWeapon = false
	if item.properties.itemType == ITEM_TYPES.WEAPON or item.properties.itemType == ITEM_TYPES.TOOL or item.properties.itemType == ITEM_TYPES.GENERIC then
		equippingWeapon = true
	end
	
	-- Do we have a valid count for this item?
	if (m_toolbarInventory[index-1] == nil) or (m_toolbarInventory[index-1].count <= 0) then
		return
	end
	
	-- Inform the Player Build Mode menu that we're no longer ghosting objects around
	local ev = KEP_EventCreate("FRAMEWORK_BUILD_HAND_MODE")
	KEP_EventEncodeString(ev, "false")
	KEP_EventQueue(ev)

	-- use the consumable when selected twice 
	-- log("--- m_selectedIndex ".. tostring(m_selectedIndex).. "\tindex ".. tostring(index))
	-- log("--- itemType ".. tostring(item.properties.itemType))
	if m_selectedIndex == index then
		if item.properties.itemType == ITEM_TYPES.CONSUMABLE then
			consumableItemActivated(index - 1)
			closeUseToolbeltContextualHelp()
			return
		elseif item.properties.itemType == ITEM_TYPES.BLUEPRINT then
			consumableItemActivated(index - 1)
			return
		elseif item.properties.itemType == ITEM_TYPES.CHARACTER and item.properties.behavior == "pet" then
			placeItemInWorld(index - 1)
			closeUseToolbeltContextualHelp()
			return
		elseif item.properties.itemType == ITEM_TYPES.PLACEABLE or item.properties.itemType == ITEM_TYPES.TUTORIAL then
			activateItem(index - 1)
			closeUseToolbeltContextualHelp()
			return
		elseif item.properties.itemType == ITEM_TYPES.TOOL and item.properties.toolType == "emoteItem" then
			KEP_EventCreateAndQueue("FRAMEWORK_ATTEMPT_TOOL")
			--Events.sendEvent("FRAMEWORK_ATTACK", {type = "Emote", attacker = KEP_GetLoginName(), attacked = KEP_GetLoginName(), weapon = item, weaponSlot = index - 1, itemType = item.properties.itemType})
			closeUseToolbeltContextualHelp()
			return
		end 
		--return
	end 

	deSelectItem(m_selectedIndex, equippingWeapon)
	
	m_selectedIndex = index
	closeSelectToolbeltContextualHelp()

	-- Select new item
	if m_selectedIndex > 0 then
		Control_SetVisible(gHandles["imgItemEquipped" .. m_selectedIndex], true)
	end
	
	-- If we've selected a new weapon or un-equipping a weapon
	if equippingWeapon then
		Control_SetLocation(gHandles["imgAttackFlash"], Control_GetLocationX(gHandles["btnItem" .. m_selectedIndex])-11, -10)

		-- Inform BattleHandler of what's going on
		local ev = KEP_EventCreate("FRAMEWORK_WEAPON_EQUIPPED")
		local eventInfo = {["equipped"] = equippingWeapon,
						   slot = m_selectedIndex}
		if equippingWeapon then
			eventInfo.weapon = item
		end
		KEP_EventEncodeString(ev, Events.encode(eventInfo))
		KEP_EventQueue(ev)
		
		-- Default fireTime and cooldownTime
		local fireTime = m_timeStamp -- You fired now (or swapped weapon)
		local cooldownTime = m_timeStamp + WEAPON_SWAP_COOLDOWN -- Default cooldownTime accounts for weapon swap
		-- Is this weapon already on a cooldown?
		if m_toolbarInventory[m_selectedIndex-1] and m_toolbarInventory[m_selectedIndex-1].fireTime and m_toolbarInventory[m_selectedIndex-1].cooldownTime then
			-- Is the current cooldown greater than the weapon cooldown
			if (m_toolbarInventory[m_selectedIndex-1].cooldownTime - m_timeStamp) > WEAPON_SWAP_COOLDOWN then
				-- Use existing cooldown
				fireTime = m_toolbarInventory[m_selectedIndex-1].fireTime
				cooldownTime = m_toolbarInventory[m_selectedIndex-1].cooldownTime
			end
		end
		-- Update Inventory item with these new fireTimes and cooldownTimes
		m_toolbarInventory[m_selectedIndex-1].cooldownTime = cooldownTime
		m_toolbarInventory[m_selectedIndex-1].fireTime = fireTime
		updateItemLocal(m_selectedIndex-1, INVENTORY_PID, m_toolbarInventory[m_selectedIndex-1])
		
		local cooldownHandle = gHandles["imgCooldown"..tostring(m_selectedIndex)]
		Control_SetSize(cooldownHandle, Control_GetWidth(cooldownHandle), 0)
		Control_SetVisible(cooldownHandle, true)
	end
	m_weaponSelected = equippingWeapon
	
	Events.sendEvent("PLAYER_EQUIP_ITEM", {UNID = item.UNID})
	
	-- Placeable
	if item.properties.itemType == ITEM_TYPES.PLACEABLE or item.properties.itemType == ITEM_TYPES.TUTORIAL or (item.properties.itemType == ITEM_TYPES.CHARACTER and item.properties.behavior == "pet") or (item.properties.itemType == ITEM_TYPES.HARVESTABLE and item.properties.behavior == "seed") then
			local itemInfo = {mode = "Placement", items = {{UNID = item.UNID, GLID = item.properties.GLID, properties = deepCopy(item.properties)}}}

		-- Prefab dataset example
		--[[local itemInfo = {mode = "Prefab", items = {{GLID = 2818, 	 offset = {x = 0, y = 0, z = -10}},
														{GLID = 2811, 	 offset = {x = 10, y = 10, z = -5}},
														{GLID = 3279765, offset = {x = 0, y = 0, z = 0}}
													   }}]]

		-- Place the ghosts
		if item.properties.behavior == "claim" and m_gameItems[tostring(item.UNID)] and m_gameItems[tostring(item.UNID)].behaviorParams then
			for attribute, value in pairs(m_gameItems[tostring(item.UNID)].behaviorParams) do
				itemInfo.items[1].properties[attribute] = value
			end
		end
		
		local ev = KEP_EventCreate("FRAMEWORK_BUILD_ITEM_INFO")
		KEP_EventEncodeString(ev, Events.encode(itemInfo))
		KEP_EventQueue(ev)
	end
	m_equippedUNID = item.UNID
end

-- Deselects an item by index (When ssewapping selected indices)
deSelectItem = function(index, equippingWeapon)
	-- Valid index?
	if (index > 0) then
		local item = m_toolbarInventory[index - 1]
		Control_SetVisible(gHandles["imgItemEquipped" .. index], false)
		if item then
			local itemType = item.properties.itemType
			if m_attackFlashTime then
				m_attackFlashTime = nil
				Control_SetVisible(gHandles["imgAttackFlash"], false)
			end
			
			if m_equippedUNID then
				Events.sendEvent("UNEQUIP_PLAYER_ITEM", {UNID = m_equippedUNID})
				m_equippedUNID = nil
			end
			
			-- Did we un-select a weapon and if we're not swapping to a different weapon
			if (itemType == ITEM_TYPES.WEAPON or itemType == ITEM_TYPES.TOOL or itemType == ITEM_TYPES.GENERIC) and not equippingWeapon then
				-- Inform BattleHandler that we're no longer using a weapon
				local ev = KEP_EventCreate("FRAMEWORK_WEAPON_EQUIPPED")
				local eventInfo = {["equipped"] = false}
				KEP_EventEncodeString(ev, Events.encode(eventInfo))
				KEP_EventQueue(ev)				
			end
		end
	end
end

activateItem = function(index)
	local item = m_toolbarInventory[tonumber(index)]

	local fireTime = item.fireTime

	if fireTime == nil and item.instanceData and item.instanceData.fireTime then
		fireTime = item.instanceData.fireTime
	end
	
	if item and item.count > 0 and fireTime == nil then
		local itemType = item.properties.itemType
		
		-- Activate consumable
		if itemType == ITEM_TYPES.CONSUMABLE or itemType == ITEM_TYPES.BLUEPRINT then
			consumableItemActivated(index)
		-- Activate placeable if not currently attempting to place an object
		elseif itemType == ITEM_TYPES.CHARACTER then
			if item.properties.behavior == "pet" then
				petItemActivated(index)
			end
		elseif itemType == ITEM_TYPES.PLACEABLE or itemType == ITEM_TYPES.TUTORIAL or (itemType == ITEM_TYPES.HARVESTABLE and item.properties.behavior == "seed") then
			if m_buildSettingToolbar then
				Log("new build mode : current mode in placeing = "..tostring(m_buildSettingToolbar))
				placeableItemActivated(index)
			else
				local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventEncodeString(statusEvent, m_noBuildReason)
				KEP_EventEncodeNumber(statusEvent, 3)
				KEP_EventEncodeNumber(statusEvent, 255)
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventSetFilter(statusEvent, 4)
				KEP_EventQueue(statusEvent)
				selectHand()
				selectItem(index + 1)
				Log("new build mode : current mode in placeing = "..tostring(m_buildSettingToolbar))
			end
		elseif itemType == ITEM_TYPES.GEMBOX then
			MenuClose("Framework_Gembox.xml")
			MenuOpen("Framework_Gembox.xml")
			Events.sendEvent("FRAMEWORK_REDEEM_GEM_CHEST", {slot = tonumber(index)})
		else
			LogWarn("Framework_Toolbar.lua - activateItem() Attempt to activate item of invalid type")
		end
	end

	closeUseToolbeltContextualHelp()
end

-- Deactivate an item
deactivateItem = function(index, prevIndex, prevPID)
	local item = m_toolbarInventory[index]
	
	if item and item.count > 0 then
		local itemType = item.properties.itemType
	end
end

-- Item activation handlers

-- Called when a pet is activated
petItemActivated = function(slot)
	if m_placingItem then return end
	m_placingItem = true
	
	-- Ask server to place this game item at your feet
	-- Your World Position
	local x, y, z, r = KEP_GetPlayerPosition()
	local radian = (r * math.pi) / 180
	local rotX = math.sin(radian)
	local rotZ = math.cos(radian)
	local placeX = x - (10 * rotX)
	local placeZ = z - (10 * rotZ)
	
	Events.sendEvent("FRAMEWORK_PLAYER_PLACE_ITEM", {GLID = m_toolbarInventory[slot].properties.GLID,
													 UNID = m_toolbarInventory[m_selectedIndex-1].UNID,
													 behavior = m_toolbarInventory[m_selectedIndex-1].properties.behavior,
													 x = placeX, y = y, z = placeZ, rx = rotX, ry = 0, rz = rotZ})
end

-- Called when a consumable is activated
consumableItemActivated = function(slot)
	--log("--- consumableItemActivated")
	

	local eventName
	local itemEffects = {}
	if m_toolbarInventory[slot].properties.itemType == ITEM_TYPES.BLUEPRINT then
		eventName = "FRAMEWORK_CONSUME_BLUEPRINT"
	else
		itemEffects.emoteM = m_toolbarInventory[slot].properties.emoteM
		itemEffects.emoteF = m_toolbarInventory[slot].properties.emoteF
		itemEffects.soundGLID = m_toolbarInventory[slot].properties.soundGLID
		itemEffects.effect = m_toolbarInventory[slot].properties.effect
		if m_toolbarInventory[slot].properties.consumeType == "health" then
			eventName = "FRAMEWORK_CONSUME_HEALTH_ITEM"
		elseif m_toolbarInventory[slot].properties.consumeType == "energy" then
			eventName = "FRAMEWORK_CONSUME_ENERGY_ITEM"	
		end
	end
	
	-- Are we sending a valid event?
	if eventName then
		if eventName =="FRAMEWORK_CONSUME_BLUEPRINT" then
			updateItem(0, BLUEPRINT_PID, m_toolbarInventory[slot])
		else
			Events.sendEvent(eventName, {consumeValue = m_toolbarInventory[slot].properties.consumeValue, itemEffects = itemEffects})
		end
		decrementItem(slot)
	else
		LogWarn("Invalid consumeType "..tostring(m_toolbarInventory[slot].properties.consumeType).." encountered when consuming an item!")
	end
end

-- Called when a pet is activated
placeItemInWorld = function(slot)
	m_placingItem = true

	-- Ask server to place this game item at your feet
	-- Your World Position
	local x, y, z, r = KEP_GetPlayerPosition()
	local radian = (r * math.pi) / 180
	local rotX = math.cos(radian-(math.pi/2))
	local rotZ = -math.sin(radian-(math.pi/2))
	local placeX = x - (10 * rotX)
	local placeZ = z - (10 * rotZ)
	
	Events.sendEvent("FRAMEWORK_PLAYER_PLACE_ITEM", {GLID = m_toolbarInventory[slot].properties.GLID,
													 UNID = m_toolbarInventory[slot].UNID,
													 behavior = m_toolbarInventory[slot].properties.behavior,
													 x = placeX, y = y, z = placeZ, rx = rotX, ry = 0, rz = rotZ})
end

-- Called when a placeable item is activated
placeableItemActivated = function(slot)
	if m_placingItem then return end
	m_placingItem = true
	
	-- Ask server to place this game item at your feet
	-- Your World Position
	local x, y, z, r = KEP_GetPlayerPosition()
	local radian = (r * math.pi) / 180
	local rotX = math.cos(radian-(math.pi/2))
	local rotZ = -math.sin(radian-(math.pi/2))
	local placeX = x - (10 * rotX)
	local placeZ = z - (10 * rotZ)

	
	Events.sendEvent("FRAMEWORK_PLAYER_PLACE_ITEM", {GLID = m_toolbarInventory[slot].properties.GLID,
													 UNID = m_toolbarInventory[m_selectedIndex-1].UNID,
													 behavior = m_toolbarInventory[m_selectedIndex-1].properties.behavior,
													 x = placeX, y = y, z = placeZ, rx = rotX, ry = 0, rz = rotZ})
end

-- Decrement an item from the designated slot
function decrementItem(slot)
	if m_toolbarInventory[slot] then
		m_toolbarInventory[slot].count = m_toolbarInventory[slot].count - 1
		if m_toolbarInventory[slot].count >= 1 then
			updateItem(slot, 0, m_toolbarInventory[slot])
		else
			removeItem(slot, 0)
		end
		updateDirty()
		
		-- Pop to Hand mode if you've used the last of this item
		if m_toolbarInventory[slot].count <= 0 then
			selectHand()
		end
	end
end

-- Sets an image handle to the size of a given static's width
setCountBG = function(imgHandle, stcHandle, offsetX)
	local stcText = Static_GetText(stcHandle)
	local strWidth = Static_GetStringWidth(stcHandle,stcText)
	Control_SetSize(imgHandle, strWidth+4, Control_GetHeight(imgHandle))
	
	-- Adjust control's x location if requested (itemCount)
	if offsetX then
		local x 	= Control_GetLocationX(stcHandle)
		local width = Control_GetWidth(stcHandle)
		local y 	= Control_GetLocationY(stcHandle)
		
		Control_SetLocation(imgHandle, (x+width) - (strWidth+2), y)
	end
end

updateBuildHighlights = function()
	local show = m_buildVisible and m_selectedIndex == 1
	Control_SetVisible(gHandles["imgMoveActive"], show and m_buildMode == MODE_MOVE)
	Control_SetVisible(gHandles["imgRotateActive"], show and m_buildMode == MODE_ROTATE)
end

-- Returns the color for the durability bar based on percentage passed in
getDurabilityColor = function(durabilityPercentage)
	-- Green bar
	if durabilityPercentage >= .5 then
		return DURABILITY_COLORS.GREEN
	-- Return yellow
	elseif durabilityPercentage >= .15 then
		return DURABILITY_COLORS.YELLOW
	-- Return red
	else
		return DURABILITY_COLORS.RED
	end
end

-- Detect if 2 tables are completely identical (minus count, cooldown, and fire time, as they are not valid detectors of the same item)
detectSimilarTables = function(table1, table2)
	-- Fact check types of each entry
	if type(table1) ~= "table" or type(table2) ~= "table" then
		return false
	end
	-- Compare all entries
	for i,v in pairs(table1) do
		-- Does table2 have an entry for comparison?
		if table2 and table2[i] then
			-- Compare nested table entries?
			if type(table1[i]) == "table" and type(table2[i]) == "table" then
				return detectSimilarTables(table1[i], table2[i])
			elseif i ~= "count" and i ~= "cooldownTime" and i ~= "fireTime" then
				if v ~= table2[i] then
					return false
				end
			end
		else
			return false
		end
	end
	return true
end

function formatNumberSuffix(count)
	-- If no need for formatting, then return
	if count < 10000 then return count end

	local strCount = tostring(count)
	local newCount = ""

	local format = { 	max = 0,
						min = 0,
					 	suffix = ""
					}

	-- Thousands
	if #strCount <= 6 then
		format.max = 6
		format.min = 3
		format.suffix = "K"
	-- Millions
	elseif #strCount <= 9 then
		format.max = 9
		format.min = 6
		format.suffix = "M"
	-- Billions
	elseif #strCount <= 12 then
		format.max = 12
		format.min = 9
		format.suffix = "B"
	-- Trillions
	elseif #strCount <= 15 then
		format.max = 15
		format.min = 12
		format.suffix = "T"
	end

	for i = 1, #strCount do
		local diff = #strCount - i
		if #strCount <= format.max then
			if diff >= format.min then
				newCount = newCount..string.sub(strCount,i,i)
			else
				return newCount..format.suffix
			end
		end
	end
end

displayOccupiedError = function()
	local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
	KEP_EventEncodeNumber(statusEvent, 0)
	KEP_EventEncodeString(statusEvent, "Toolbelt unavailable while driving!")
	KEP_EventEncodeNumber(statusEvent, 3)
	KEP_EventEncodeNumber(statusEvent, 255)
	KEP_EventEncodeNumber(statusEvent, 0)
	KEP_EventEncodeNumber(statusEvent, 0)
	KEP_EventSetFilter(statusEvent, 4)
	KEP_EventQueue(statusEvent)
end

function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end

function showToolbeltButtons(bool)
	for i = 1, TOOLBAR_SLOTS+1 do
		--if bool == false then
			setControlEnabledVisible("btnItem"..i, bool, true)
			setControlEnabledVisible("imgItemGlow"..i, false)
			setControlEnabledVisible("btnItemRestricted"..i, bool, true)
			setControlEnabledVisible("imgItemEquipped"..i, false)
			setControlEnabledVisible("imgItemBG"..i, bool)
			setControlEnabledVisible("imgItem"..i, bool)
			setControlEnabledVisible("imgCountBG"..i, bool)
			setControlEnabledVisible("stcItemCount"..i, bool)
			setControlEnabledVisible("imgAmmoBG"..i, bool)
			setControlEnabledVisible("stcItemAmmo"..i, bool)
			setControlEnabledVisible("imgCooldown"..i, false)
			setControlEnabledVisible("imgDurabilityBG"..i, bool)
			setControlEnabledVisible("imgDurabilityBar"..i, bool)
			setControlEnabledVisible("imgDurabilityGradiant"..i, bool)
			setControlEnabledVisible("imgItemBroken"..i, false)
			setControlEnabledVisible("stcKey"..i.."BG1", bool)
			setControlEnabledVisible("stcKey"..i.."BG2", bool)
			setControlEnabledVisible("stcKey"..i.."BG3", bool)
			setControlEnabledVisible("stcKey"..i.."BG4", bool)
			setControlEnabledVisible("stcKey"..i, bool)
		--else
			--setControlEnabledVisible("btnItem"..i, true, true)
			--setControlEnabledVisible("btnItemRestricted"..i, true, true)
			--setControlEnabledVisible("imgItemBG"..i, true)
		--end
	end

	setControlEnabledVisible("imgVehicleInstructions", not bool)
	setControlEnabledVisible("stcInstructions", not bool, true)

	if bool == true then
		updateToolbar()
	end
end

function setControlEnabledVisible(controlName, bool, button)
	if gHandles[controlName] then
		Control_SetVisible(gHandles[controlName], bool)
		if button == true then
			Control_SetEnabled(gHandles[controlName], bool)
		end
	end
end

function swapToolbarAnimation(bool)
	if m_minimized then
		showToolbeltButtons(bool)
		toggleToolbarMinimized(bool)
	else
		m_swappingToolbelt = true
		toggleToolbarMinimized(true)
	end
end

-- Retrieves new world settings from the server
function onWorldSettingsChanged(event)
	m_flagBuildOnly				= event.flagBuildOnly
end

