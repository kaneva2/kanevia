--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\DevToolsConstants.lua")
dofile("..\\MenuScripts\\PopupMenuHelper.lua")

YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"
MENU_LOAD_EVENT = "MenuLoadEvent"
MENU_CREATE_EVENT = "MenuCreateEvent"
COLOR_PICKER_EVENT = "ColorSelectedEvent"
COLOR_START_EVENT = "ColorStartEvent"
TEXTURE_PICKER_EVENT = "TextureSelectedEvent"
TEXTURE_LOAD_EVENT = "TextureLoadEvent"
TEXTURE_COPY_EVENT = "CopyTextureEvent"
TEXTURE_PASTE_EVENT = "RequestPasteEvent"
LIST_EDITOR_START_EVENT = "ListEditorStartEvent"
LIST_EDITOR_DONE_EVENT = "ListEditorDoneEvent"
SOURCE_ASSET_DOWNLOAD_EVENT = "3DAppAssetDownloadCompleteEvent"

MENU_EDITOR_TEXTURE_FILTER = 1
MENU_EDITOR_COLOR_FILTER = 1
MENU_EDITOR_LIST_EDITOR_FILTER = 1

ColorTarget = { CORNERS = 1, ELEMENT=2, FONT = 3 }

CLOSE_MSG = "Are you sure you want to close this menu? Any unsaved changes will be lost."

FileMenu = {NEW = 0, OPEN = 1, RELOAD = 2, SAVE = 3, EXIT = 4}
EditMenu = {UNDO = 0, REDO = 1}

MENU_TITLE = "    "

gNextMenu = "" -- filename of the menu that's about to be opened
gMenuName = "" -- filename without ".xml" of the currently opened menu
gXmlName = ""
gCurrentMenuScriptName = "" -- filename of the .lua file for the current menu

gCurrentMenu = nil --handle to current dialog being edited
gCurrentControl = nil --handle to the current control being edited
gSortOrder = "z"
gControlProperties = {} -- a map from control name to {handle, type}

-- shortcut handles for commonly-used controls
glstControls = nil -- handle to the Controls ListBox
gCreatedNewMenu = false
gModifiedScriptName = false

DXUT_CONTROL_DIALOG = 14

gElementTags = {}-- a list of all the element tags for each controltypename
gElementTags["Dialog"]		= { "display", "backgroundDisplay", "iconDisplay" }
gElementTags["Button"]		= { "normalDisplay", "mouseOverDisplay", "focusedDisplay", "pressedDisplay", "disabledDisplay" }
gElementTags["Static"]		= { "display" }
gElementTags["CheckBox"]	= { "boxDisplay", "checkDisplay" }
gElementTags["RadioButton"]	= { "boxDisplay", "checkDisplay" }
gElementTags["ComboBox"]	= { "mainDisplay", "buttonDisplay", "dropdownDisplay", "selectionDisplay" }
gElementTags["Slider"]		= { "trackDisplay", "buttonDisplay" }
gElementTags["EditBox"]		= { "textAreaDisplay", "Caret Color", "Selected Color", "Selected BG" }
-- NOTE: Removed these cuz I think they can be accomplished with 9-slices on the textAreaDisplay.
-- "topLeftBorderDisplay", "topBorderDisplay", "topRightBorderDisplay", "leftBorderDisplay", "rightBorderDisplay", "lowerLeftBorderDisplay", "lowerBorderDisplay", "lowerRightBorderDisplay" }
gElementTags["ListBox"]		= { "mainDisplay", "selectionDisplay" }
gElementTags["ScrollBar"]	= { "trackDisplay", "upArrowDisplay", "downArrowDisplay", "buttonDisplay" }
gElementTags["Image"]		= { "display" }
gElementTags["Comment"]		= { }

gElementStateNames =
{
	{ state=ControlStates.DXUT_STATE_NORMAL,		name="Normal"		},
	{ state=ControlStates.DXUT_STATE_DISABLED,		name="Disabled"		},
	{ state=ControlStates.DXUT_STATE_HIDDEN,		name="Hidden"		},
	{ state=ControlStates.DXUT_STATE_FOCUS,			name="Focused"		},
	{ state=ControlStates.DXUT_STATE_MOUSEOVER,		name="MouseOver"	},
	{ state=ControlStates.DXUT_STATE_PRESSED,		name="Pressed"		},
}

gBuildElementsBlock = {}
--					Type			= {		Elements, 	SelectTexture,	States,		ListEditor,		Swatch	},
gBuildElementsBlock["Dialog"]		= {		true,  	true,			false,		false,			true,		}
gBuildElementsBlock["Button"]		= {		true,  	true,			true,		false,			true,		} --TODO: Button States is true, but I'd like to hide it.
gBuildElementsBlock["Static"]		= {		false,  	false,			false,		false,			false,		}
gBuildElementsBlock["CheckBox"]		= {		true,  	true,			true,		false,			true,		}
gBuildElementsBlock["RadioButton"]	= {		true,  	true,			true,		false,			true,		}
gBuildElementsBlock["ComboBox"]		= {		true,  	true,			true,		true,			false,		} -- ListEditor blocks Swatch
gBuildElementsBlock["Slider"]		= {		true,  	true,			true,		false,			true,		}
gBuildElementsBlock["EditBox"]		= {		true,  	true,			true,		false,			true,		}
gBuildElementsBlock["ListBox"]		= {		true,  	true,			true,		true,			false,		} -- ListEditor blocks Swatch
gBuildElementsBlock["ScrollBar"]	= {		true,  	true,			true,		false,			true,		}
gBuildElementsBlock["Image"]		= {		false,  	true,			true,		false,			true,		}
gBuildElementsBlock["Comment"]		= {		false,  	false,			false,		false,			false,		}

gCornerLabels = {"All Corners", "Top Left", "Bottom Left", "Top Right", "Bottom Right"}
gCornerFunctions = {"TopLeft", "TopLeft", "BottomLeft", "TopRight", "BottomRight"}

gFontBlockNames =
{
	"imgHorizRuler_Font",
	"lblFontFace",
	"cmbFontFace",
	"lblFontWeight",
	"cmbFontWeight",
	"boxFontItalic",
	"lblFontSize",
	"edFontSize",
	"lblFontColor",
	"imgFontColorSwatchBG",
	"btnFontColorSwatch",
	"boxWordWrap",
	"lblFontAlignH",
	"cmbFontAlignH",
	"lblFontAlignV",
	"cmbFontAlignV",
}

gFonts =
{
	"Arial",
	"Bookman Old Style",
	"Century Gothic",
	"Comic Sans",
	"Courier",
	"Courier New",
	"Garamond",
	"Georgia",
	"Gill Sans",
	"Impact",
	"Lucida Sans",
	"Palatino Linotype",
	"Tahoma",
	"Times New Roman",
	"Trebuchet",
	"Verdana",
}

gFontWeights =
{
	{"Thin",100},
	{"Extra Light",200},
	{"Light",300},
	{"Normal",400},
	{"Medium",500},
	{"SemiBold",600},
	{"Bold",700},
	{"Extra Bold",800},
	{"Heavy",900},
}

gFontAlignV =
{
	{"Top", TEXT_VALIGN_TOP},
	{"Center", TEXT_VALIGN_CENTER},
	{"Bottom", TEXT_VALIGN_BOTTOM},
}

gFontAlignH =
{
	{"Left", TEXT_HALIGN_LEFT},
	{"Center", TEXT_HALIGN_CENTER},
	{"Right", TEXT_HALIGN_RIGHT},
}

-- A restricted math expression evaulator. I don't want to unleash full loadstring().
function eval(expr)
	-- Do the quick version for most strings
	local result = tonumber(expr)
	if result ~= nil then
		return result
	end

	-- strip out any non-math characters
	expr = string.gsub(expr, "[^%d%s%.-+/*]", "")

	-- let Lua handle the parsing for us.
	local f = loadstring("return "..expr)
	return tonumber(f())
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "yesNoAnswerHandler", YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "menuLoadEventHandler", MENU_LOAD_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "menuCreateEventHandler", MENU_CREATE_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "colorPickerEventHandler", COLOR_PICKER_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "texturePickerEventHandler", TEXTURE_PICKER_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "textureCopyEventHandler", TEXTURE_COPY_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "texturePasteEventHandler", TEXTURE_PASTE_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "listEditorDoneEventHandler", LIST_EDITOR_DONE_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AppAssetDownloadHandler", SOURCE_ASSET_DOWNLOAD_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( MENU_LOAD_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( MENU_CREATE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( COLOR_PICKER_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( TEXTURE_PICKER_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( TEXTURE_LOAD_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( TEXTURE_COPY_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( TEXTURE_PASTE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( LIST_EDITOR_START_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( LIST_EDITOR_DONE_EVENT, KEP.MED_PRIO )
end

function BringToFront()
	if (gMenuName ~= "") then
		MenuBringToFront(gMenuName)
	end
	MenuBringToFrontThis()
	if (MenuHandleOk("TexturePicker")) then
		MenuBringToFront("TexturePicker")
	end
	if (MenuHandleOk("ColorPicker")) then
		MenuBringToFront("ColorPicker")
	end
	if (MenuHandleOk("InfoBox")) then
		MenuBringToFront("InfoBox")
	end
	if (MenuHandleOk("YesNoBox")) then
		MenuBringToFront("YesNoBox")
	end
end

--[[ DRF - Executes Fade ]]
function Dialog_OnRender(dialogHandle, elapsedSec)
	BringToFront()
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	
	-- Cleanup A Little
	KEP_ClearSelection()
	MenuClose("Dashboard")
	MenuClose("TextureBrowser2")
	MenuClose("PlaceableObj")
	MenuClose("ChatMenu")

	-- Drop Black Screen (behind this)
	MenuOpen("BlackScreen")

	g_FileMenuStruct = PopupMenuHelper_InitPopupMenu(gHandles["btnMenuFile"], g_FileMenuContents, nil, gHandles["imgFileMenuItemHighlight"], g_FileMenuItems)
	g_EditMenuStruct = PopupMenuHelper_InitPopupMenu(gHandles["btnMenuEdit"], g_EditMenuContents, nil, gHandles["imgEditMenuItemHighlight"], g_EditMenuItems)
	g_AddMenuStruct  = PopupMenuHelper_InitPopupMenu(gHandles["btnMenuAdd"], g_AddMenuContents, nil, gHandles["imgAddMenuItemHighlight"], g_AddMenuItems)

	glstControls = gHandles["lstControls"]

	local cmbDialogAlignV = gHandles["cmbDialogAlignV"]
	ComboBox_RemoveAllItems(cmbDialogAlignV)
	ComboBox_AddItem(cmbDialogAlignV, "None", IMDL_NONE)
	ComboBox_AddItem(cmbDialogAlignV, "Top", IMDL_TOP)
	ComboBox_AddItem(cmbDialogAlignV, "Center", IMDL_VCENTER)
	ComboBox_AddItem(cmbDialogAlignV, "Bottom", IMDL_BOTTOM)

	local cmbDialogAlignH = gHandles["cmbDialogAlignH"]
	ComboBox_RemoveAllItems(cmbDialogAlignH)
	ComboBox_AddItem(cmbDialogAlignH, "None", IMDL_NONE)
	ComboBox_AddItem(cmbDialogAlignH, "Left", IMDL_LEFT)
	ComboBox_AddItem(cmbDialogAlignH, "Center", IMDL_CENTER)
	ComboBox_AddItem(cmbDialogAlignH, "Right", IMDL_RIGHT)

	local cmbCorners = gHandles["cmbCorners"]
	for i,v in pairs(gCornerLabels) do
		ComboBox_AddItem(cmbCorners, v, i)
	end

	local cmbFontFace = gHandles["cmbFontFace"]
	for i,v in pairs(gFonts) do
		ComboBox_AddItem(cmbFontFace, v, i)
	end

	local cmbFontWeight = gHandles["cmbFontWeight"]
	for i,v in pairs(gFontWeights) do
		ComboBox_AddItem(cmbFontWeight, v[1], v[2])
	end

	local cmbFontAlignV = gHandles["cmbFontAlignV"]
	for i,v in pairs(gFontAlignV) do
		ComboBox_AddItem(cmbFontAlignV, v[1], v[2])
	end

	local cmbFontAlignH = gHandles["cmbFontAlignH"]
	for i,v in pairs(gFontAlignH) do
		ComboBox_AddItem(cmbFontAlignH, v[1], v[2])
	end

	-- Store a bunch of offsets relative to their section parent
	local parents =
	{
		gHandles["imgHorizRuler_Text"],
		gHandles["imgHorizRuler_Positions"],
		gHandles["imgHorizRuler_Checkboxes"],
		gHandles["imgHorizRuler_Elements"],
		gHandles["imgHorizRuler_Font"],
	}
	local parentIndex = 0
	local parent = nil
	local nextParent = parents[parentIndex+1]

	-- Loop through all the controls in this dialog
	local numControls = Dialog_GetNumControls(dialogHandle)
	for i=0,numControls-1 do
		local control = Dialog_GetControlByIndex(dialogHandle, i)
		-- if it matches the next parent, increment parentIndex
		if control == nextParent then
			parent = nextParent
			parentIndex = parentIndex + 1
			nextParent = parents[parentIndex+1] -- will be nil when we reach off the end
		else
			-- Otherwise, store this control's offset info relative to the parent
			StoreControlOffset(control, parent)
		end
	end

	-- Put all the rules at the right X coord
	local rulerOffsetX = 2
	for i,v in ipairs(parents) do
		Control_SetLocationX(v, rulerOffsetX)
	end

	ResetControlProperties()
end

function Dialog_OnDestroy(dialogHandle)

	-- Lift Black Screen
	MenuClose("BlackScreen")

	-- Restore Cleaned Up Menus
	--MenuOpen("ChatMenu")

	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	PopupMenuHelper_OnMouseMove(dialogHandle, x, y)
end

function AppAssetDownloadHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local path = KEP_EventDecodeString(event)
	local fileType = KEP_EventDecodeNumber(event)
	if fileType == DevToolsUpload.MENU_SCRIPT then
		KEP_ShellOpen(path)
	end
end

function menuLoadEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	gNextMenu = KEP_EventDecodeString(event)
	if gCurrentMenu then
		KEP_YesNoBox( CLOSE_MSG, "Confirm", WF.MENUEDITOR_OPEN_FILTER)
	else
		MenuOpenCurrent(gNextMenu)
	end
end

function menuCreateEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if gCurrentMenu then
		KEP_YesNoBox( CLOSE_MSG, "Confirm", WF.MENUEDITOR_NEW_FILTER)
	else
		MenuCreateCurrent()
	end
end

function yesNoAnswerHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = tonumber(KEP_EventDecodeNumber(event))
	if answer == 1 then
		if filter == WF.MENUEDITOR_EXIT_FILTER then
			MenuCloseCurrent()
			MenuCloseThis()
		elseif filter == WF.MENUEDITOR_CLOSE_FILTER then
			MenuCloseCurrent()
		elseif filter == WF.MENUEDITOR_RELOAD_FILTER then
			local filePath = KEP_Get3DAppUnpublishedAssetPath(DevToolsUpload.MENU_XML)
			if filePath and FileExist( filePath..gXmlName) then
				MenuCloseCurrent()
				MenuOpenCurrent(gXmlName)
			end
		elseif filter == WF.MENUEDITOR_NEW_FILTER then
			MenuCloseCurrent()
			MenuCreateCurrent()
		elseif filter == WF.MENUEDITOR_OPEN_FILTER then
			MenuCloseCurrent()
			MenuOpenCurrent(gNextMenu)
		elseif filter == WF.MENUEDITOR_BROWSE_FILTER then
			MenuCloseCurrent()
			MenuCloseThis()
			BrowseMenus()
		elseif filter == WF.MENUEDITOR_MISSING_SCRIPT_FILTER then
			Dialog_CopyTemplate(gCurrentMenu)
		end
	end
end

function MenuSaveCurrent()
	if gCurrentMenu then
		-- ensure required local dev folders have been created
		KEP_Count3DAppUnpublishedAssets(DevToolsUpload.MENU_XML)
		KEP_Count3DAppUnpublishedAssets(DevToolsUpload.MENU_SCRIPT)

		local filePath = KEP_Get3DAppUnpublishedAssetPath(DevToolsUpload.MENU_SCRIPT)

		if not filePath or not FileExist(filePath..gCurrentMenuScriptName) then
			if gCreatedNewMenu then
				Dialog_CopyTemplate(gCurrentMenu)
				gCreatedNewMenu = false
			elseif gModifiedScriptName then
				local msgText = "Script File " .. gCurrentMenuScriptName .. " not found.  Would you like to create this script?"
				KEP_YesNoBox( msgText, "Confirm", WF.MENUEDITOR_MISSING_SCRIPT_FILTER)
			end
		end
		if (Dialog_Save(gCurrentMenu, gXmlName) == 0) then --Save Failed
			KEP_MessageBox( "Unable to save menu. Is it set to Read-Only?","Error")
		else --Save Successful
			KEP_MessageBox( "Menu saved successfully.", "Success!")
			KEP_ReloadMenus()
		end
	end
end

function MenuOpenCurrent(menuName)
	local i,j
	gXmlName = menuName
	i, j, gMenuName = string.find(gXmlName, "(.*)%.xml")
	Dialog_SetCaptionText( gDialogHandle, MENU_TITLE..gMenuName )
	gCurrentMenu = Dialog_CreateAndEdit(menuName, true)
	BringToFront()
	gCurrentMenuScriptName = Dialog_GetScriptName(gCurrentMenu)
	gActions = {}
	gRedoActions = {}
	g_ElementsBlockBuiltFor = nil
	MenuCenterCurrent()
	ResetControlList()
	ListBox_SelectItem(glstControls, 0)
	ResetControlProperties()
end

function MenuCreateCurrent()
	gXmlName = "temp.xml"
	gMenuName = "temp"
	Dialog_SetCaptionText( gDialogHandle, MENU_TITLE..gMenuName )
	gCurrentMenuScriptName = "temp.lua"
	gCurrentMenu = Dialog_New()
	BringToFront()
	Dialog_SetCaptionText(gCurrentMenu, gMenuName)
	Dialog_SetCaptionEnabled(gCurrentMenu, true)
	Dialog_SetScriptName(gCurrentMenu, gCurrentMenuScriptName)
	Dialog_SetLocationFormat(gCurrentMenu, IMDL_CENTER + IMDL_VCENTER)
	Dialog_SetEdit(gCurrentMenu, true)
	gActions = {}
	gRedoActions = {}
	g_ElementsBlockBuiltFor = nil
	-- Setup some defaults for new menus that don't match what's in defaultcontrols.xml

	-- Make the backgroundDisplay element white.tga
	local element = Dialog_GetDisplayElement(gCurrentMenu, "backgroundDisplay")
	Element_AddTexture(element, gCurrentMenu, "white.tga")
	Element_SetCoords(element, 0, 0, -1, -1)
	Element_SetSlices(element, 0, 0, 0, 0)
	local blendColor = Element_GetTextureColor(element)
	BlendColor_SetColor(blendColor, ControlStates.DXUT_STATE_NORMAL, {a=255, r=255, g=255, b=255} )

	--EnableMovable(gCurrentMenu, true)
	ResetControlList()
	ListBox_SelectItem(glstControls, 0)
	ResetControlProperties()
	gCreatedNewMenu = true
end

function MenuCloseCurrent()
	DestroyMenu(gCurrentMenu)
	gCurrentMenu = nil
	gActions = {}
	gRedoActions = {}
	g_ElementsBlockBuiltFor = nil
	ResetControlProperties()
end

function BrowseMenus()
	MenuOpenModal("MenuBrowser.xml")
end

function GetMovable(menu)
	local text = Dialog_GetCaptionText(menu)
	local enabled = Dialog_GetCaptionEnabled(menu)
	--Menu is only movable if caption is enabled, regardless of movable attribute
	return string.len(text) > 0 and enabled == 1
end

function EnableMovable(menu, initialize)
	local enabled = Dialog_GetCaptionEnabled(menu)
	local text = Dialog_GetCaptionText(menu)
	local height = Dialog_GetCaptionHeight(menu)
	Dialog_SetMovable(menu, true)
	if string.len(text) == 0 then
		Dialog_SetCaptionText(menu, ".")
	end
	if enabled == 0 then
		Dialog_SetCaptionEnabled(menu, true)
	end
	if height < 1 then
		Dialog_SetCaptionHeight(menu, defaultCaptionH)
	end
	if initialize then
		local element = Dialog_GetCaptionElement(menu, gDialogHandle)
		if element ~= 0 then
			local color = 0 --invisible
			local bch = Element_GetFontColor(element)
			Element_AddTexture(element, menu, "invisible.tga")
			Element_SetCoords(element, 1, 1, 3, 3)
			if bch ~= 0 then
				--captions only have one state
				BlendColor_SetColor(bch, ControlStates.DXUT_STATE_NORMAL, color)
			end
		end
	end
end

function undoableSetCornerColor(handle, iCornerIndex, tl, tr, bl, br)
	Dialog_SetBackgroundColors(handle, tl, tr, bl, br)
	ComboBox_SetSelectedByIndex(gHandles["cmbCorners"], iCornerIndex-1)
end

function colorPickerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == MENU_EDITOR_COLOR_FILTER then
		local a,r,g,b = KEP_EventDecodeNumber(event),KEP_EventDecodeNumber(event),KEP_EventDecodeNumber(event),KEP_EventDecodeNumber(event)
		local colorTarget = KEP_EventDecodeNumber(event)
		local color = {a=a, r=r, g=g, b=b}

		if colorTarget == ColorTarget.CORNERS then
			local tl = Dialog_GetColorTopLeft(gCurrentMenu)
			local tr = Dialog_GetColorTopRight(gCurrentMenu)
			local bl = Dialog_GetColorBottomLeft(gCurrentMenu)
			local br = Dialog_GetColorBottomRight(gCurrentMenu)

			local newTL, newTR, newBL, newBR = tl, tr, bl, br

			local iCornerIndex = ComboBox_GetSelectedData(gHandles["cmbCorners"])
			if iCornerIndex == 1 then
				newTL, newTR, newBL, newBR = color, color, color, color
			elseif iCornerIndex == 2 then
				newTL = color
			elseif iCornerIndex == 3 then
				newBL = color
			elseif iCornerIndex == 4 then
				newTR = color
			elseif iCornerIndex == 5 then
				newBR = color
			end

			doAction( gCurrentMenu, "undoableSetCornerColor", {iCornerIndex, newTL, newTR, newBL, newBR}, "undoableSetCornerColor", {iCornerIndex, tl, tr, bl, br}, false)

		elseif colorTarget == ColorTarget.ELEMENT then
			local currentControlTypeName = GetControlTypeName(gCurrentControl)
			local elementTagIndex = ComboBox_GetSelectedData(gHandles["cmbElement"])
			local elementTag = gElementTags[currentControlTypeName][ elementTagIndex ]
			if currentControlTypeName == "EditBox" then
				if elementTag == "Caret Color" then
					doAction( gCurrentControl, "EditBox_SetCaretColor", {color}, "EditBox_SetCaretColor", {EditBox_GetCaretColor(gCurrentControl)}, false)
				elseif elementTag == "Selected Color" then
					doAction( gCurrentControl, "EditBox_SetSelectedTextColor", {color}, "EditBox_SetSelectedTextColor", {EditBox_GetSelectedTextColor(gCurrentControl)}, false)
				elseif elementTag == "Selected BG" then
					doAction( gCurrentControl, "EditBox_SetSelectedBackColor", {color}, "EditBox_SetSelectedBackColor", {EditBox_GetSelectedBackColor(gCurrentControl)}, false)
				else
					local element = Control_GetDisplayElement(gCurrentControl, elementTag)
					local blendColor = Element_GetTextureColor(element)
					local state = ComboBox_GetSelectedData(gHandles["cmbState"])
					local index = ListBox_GetSelectedItemIndex(glstControls)
					doAction( blendColor, "BlendColor_SetColor", { state, color }, "BlendColor_SetColor", { state, BlendColor_GetColor(blendColor, state) }, false, index, index)
				end
			else
				local element
				if currentControlTypeName == "Dialog" then
					element = Dialog_GetDisplayElement(gCurrentControl, elementTag)
				else
					element = Control_GetDisplayElement(gCurrentControl, elementTag)
				end
				local blendColor = Element_GetTextureColor(element)
				local state = ComboBox_GetSelectedData(gHandles["cmbState"])
				local index = ListBox_GetSelectedItemIndex(glstControls)
				doAction( blendColor, "BlendColor_SetColor", { state, color }, "BlendColor_SetColor", { state, BlendColor_GetColor(blendColor, state) }, false, index, index)
			end
		elseif colorTarget == ColorTarget.FONT then
			local element = GetCurrentFontElement()
			if element ~= nil then
				local state = ComboBox_GetSelectedData(gHandles["cmbState"])
				local blendColor = Element_GetFontColor(element)
				local index = ListBox_GetSelectedItemIndex(glstControls)
				doAction( blendColor, "BlendColor_SetColor", { state, color }, "BlendColor_SetColor", { state, BlendColor_GetColor(blendColor, state) }, false, index, index)
			end
		end
	end
end

function undoableSetTexture(element, textureName, coords, slices, color, state )
	Element_AddTexture(element, gCurrentMenu, textureName)
	Element_SetCoords(element, coords.l, coords.t, coords.r, coords.b)
	Element_SetSlices(element, slices.l, slices.t, slices.r, slices.b)
	BlendColor_SetColor( Element_GetTextureColor(element), state, color)
end

g_LastTexturePickerEventCurrValues = nil

function texturePickerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter ~= MENU_EDITOR_TEXTURE_FILTER then return end

	-- New values
	local textureName = KEP_EventDecodeString(event)
	local coords = {}
	coords.l, coords.t, coords.r, coords.b = KEP_EventDecodeNumber(event), KEP_EventDecodeNumber(event), KEP_EventDecodeNumber(event), KEP_EventDecodeNumber(event)
	local color = {}
	color.a, color.r, color.g, color.b = KEP_EventDecodeNumber(event), KEP_EventDecodeNumber(event), KEP_EventDecodeNumber(event), KEP_EventDecodeNumber(event)
	local slices = {}
	slices.l, slices.t, slices.r, slices.b = KEP_EventDecodeNumber(event), KEP_EventDecodeNumber(event), KEP_EventDecodeNumber(event), KEP_EventDecodeNumber(event)

	local element = GetCurrentTextureElement()
	local state = ComboBox_GetSelectedData(gHandles["cmbState"])

	-- Current values
	local curName = Element_GetTextureName(element, gCurrentMenu)
	local curCoords = {}
	curCoords.l, curCoords.t, curCoords.r, curCoords.b = Element_GetCoordLeft(element), Element_GetCoordTop(element), Element_GetCoordRight(element), Element_GetCoordBottom(element)
	local curColor = BlendColor_GetColor(Element_GetTextureColor(element), state)
	local curSlices = {}
	curSlices.l, curSlices.t, curSlices.r, curSlices.b = Element_GetSliceLeft(element), Element_GetSliceTop(element), Element_GetSliceRight(element), Element_GetSliceBottom(element)

	if (curName ~= textureName) or
		(curCoords.l ~= coords.l) or (curCoords.t ~= coords.t) or (curCoords.r ~= coords.r) or (curCoords.b ~= coords.b) or
		(curSlices.l ~= slices.l) or (curSlices.t ~= slices.t) or (curSlices.r ~= slices.r) or (curSlices.b ~= slices.b) or
		(curColor.a  ~= color.a)  or (curColor.r  ~= color.r)  or (curColor.g  ~= color.g)  or (curColor.b  ~= color.b)  then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction(element, "undoableSetTexture", {textureName, coords, slices, color, state}, "undoableSetTexture", {curName, curCoords, curSlices, curColor, state}, false, index, index )
	end
end

g_copiedTexture = nil
g_copiedLeft = nil
g_copiedTop = nil
g_copiedRight = nil
g_copiedBottom = nil
g_copiedColor = {}
g_copiedSliceLeft = nil
g_copiedSliceTop = nil
g_copiedSliceRight = nil
g_copiedSliceBottom = nil

function textureCopyEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_copiedTexture = KEP_EventDecodeString(event)
	g_copiedLeft = KEP_EventDecodeNumber(event)
	g_copiedTop = KEP_EventDecodeNumber(event)
	g_copiedRight = KEP_EventDecodeNumber(event)
	g_copiedBottom = KEP_EventDecodeNumber(event)
	g_copiedColor = {}
	g_copiedColor.a = KEP_EventDecodeNumber(event)
	g_copiedColor.r = KEP_EventDecodeNumber(event)
	g_copiedColor.g = KEP_EventDecodeNumber(event)
	g_copiedColor.b = KEP_EventDecodeNumber(event)
	g_copiedSliceLeft = KEP_EventDecodeNumber(event)
	g_copiedSliceTop = KEP_EventDecodeNumber(event)
	g_copiedSliceRight = KEP_EventDecodeNumber(event)
	g_copiedSliceBottom = KEP_EventDecodeNumber(event)
end

function texturePasteEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if not g_copiedTexture then return end
	local ev = KEP_EventCreate( TEXTURE_LOAD_EVENT)
	KEP_EventSetFilter(ev, MENU_EDITOR_TEXTURE_FILTER)
	KEP_EventEncodeString(ev, g_copiedTexture)
	KEP_EventEncodeNumber(ev, g_copiedLeft)
	KEP_EventEncodeNumber(ev, g_copiedTop)
	KEP_EventEncodeNumber(ev, g_copiedRight)
	KEP_EventEncodeNumber(ev, g_copiedBottom)
	KEP_EventEncodeNumber(ev, g_copiedColor.a)
	KEP_EventEncodeNumber(ev, g_copiedColor.r)
	KEP_EventEncodeNumber(ev, g_copiedColor.g)
	KEP_EventEncodeNumber(ev, g_copiedColor.b)
	KEP_EventEncodeNumber(ev, g_copiedSliceLeft)
	KEP_EventEncodeNumber(ev, g_copiedSliceTop)
	KEP_EventEncodeNumber(ev, g_copiedSliceRight)
	KEP_EventEncodeNumber(ev, g_copiedSliceBottom)
	KEP_EventEncodeString(ev, "") -- optionally blank

	KEP_EventQueue( ev)
end

function listEditorDoneEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == MENU_EDITOR_LIST_EDITOR_FILTER then
		local currentControlTypeName = GetControlTypeName(gCurrentControl)
		if currentControlTypeName == "ListBox" then
			ListBox_RemoveAllItems(gCurrentControl)

			local size = KEP_EventDecodeNumber(event)
			for i=0,size-1 do
				ListBox_AddItem(gCurrentControl, KEP_EventDecodeString(event), KEP_EventDecodeNumber(event))
			end
		elseif currentControlTypeName == "ComboBox" then
			ComboBox_RemoveAllItems(gCurrentControl)

			local size = KEP_EventDecodeNumber(event)
			for i=0,size-1 do
				ComboBox_AddItem(gCurrentControl, KEP_EventDecodeString(event), KEP_EventDecodeNumber(event))
			end
		end
	end
end

function ResetControlList()
	ListBox_RemoveAllItems(glstControls)
	gControlProperties = {}
	local controlNameList = {}
	local controlHandle, controlName, controlType

	-- Always add the dialog as the first control
	controlHandle =	gCurrentMenu
	controlName = gMenuName
	controlType = DXUT_CONTROL_DIALOG
	gControlProperties[controlName] = {handle=controlHandle, type=controlType}
	table.insert(controlNameList, controlName)

	local size = Dialog_GetNumControls(gCurrentMenu)
	for i = 0, size - 1 do
		controlHandle = Dialog_GetControlByIndex(gCurrentMenu, i)
		controlName = Control_GetName(controlHandle)
		controlType = Control_GetType(controlHandle)
		if controlType == ControlTypes.DXUT_CONTROL_COMMENT then
			-- Comment Controls use their comment as a name, so set it now.
			controlName = Control_GetComment(controlHandle)
			Control_SetName(controlHandle, controlName )
		end

		gControlProperties[controlName] = {handle=controlHandle, type=controlType}
		table.insert(controlNameList, controlName)
	end

	if gSortOrder == "a" then
		table.sort(controlNameList)
	end

	for i,v in ipairs(controlNameList) do
		ListBox_AddItem(glstControls, v, gControlProperties[v].type)
	end
end

function ResetControlProperties()

	-- Hide everything and blank the EditBoxes
	for controlName, controlHandle in pairs(gHandles) do
		local controlType = Control_GetType(controlHandle)
		Control_SetVisible(controlHandle, false)
		if controlType == ControlTypes.DXUT_CONTROL_EDITBOX or controlType == ControlTypes.DXUT_CONTROL_SIMPLEEDITBOX then
			EditBox_SetText(controlHandle, "", false)
		end
	end
	
	-- Build up the menu editor options
	local currentControlTypeName = "None"
	if gCurrentMenu then
		currentControlTypeName = GetControlTypeName(gCurrentControl)
	end

	-- Always show these
	Control_SetVisible(gHandles["btnClose"], true)
	Control_SetVisible(gHandles["imgTitleIcon"], true)

	if gCurrentMenu and Dialog_GetEdit(gCurrentMenu) == 0 then
		setControl(gHandles["btnExitPreview"], true)
		setControl(gHandles["stcPreviewIntro"], true)
		setControl(gHandles["stcPreviewBody"], true)
		setControl(gHandles["stcPreviewEnd"], true)
		return
	else
		Control_SetVisible(gHandles["imgMenuBackground"], true)
		Control_SetVisible(gHandles["btnPreview"], true)
		Control_SetVisible(gHandles["btnMenuFile"], true)
		Control_SetVisible(gHandles["btnMenuEdit"], true)
	end

	-- Only show these if a menu is loaded
	if gCurrentMenu then
		local isDialog = (currentControlTypeName == "Dialog")

		-- Show all the top stuff
		Control_SetVisible(gHandles["lblControls"], true)
		Control_SetVisible(gHandles["lblSort"], true)
		Control_SetVisible(gHandles["radioZOrder"], true)
		Control_SetVisible(gHandles["radioAOrder"], true)
		Control_SetVisible(gHandles["lstControls"], true)

		Control_SetVisible(gHandles["lblControls2"], true)
		Control_SetVisible(gHandles["btnMenuAdd"], true)
		Control_SetVisible(gHandles["btnDelete"], true)
		Control_SetEnabled(gHandles["btnDelete"], not isDialog)
		Control_SetVisible(gHandles["btnCopy"], true)
		Control_SetEnabled(gHandles["btnCopy"], not isDialog)
		Control_SetVisible(gHandles["btnPaste"], true)
		Control_SetEnabled(gHandles["btnPaste"], Dialog_IsSomethingInClipboard())

		-- Z-order buttons: Always show them, but only enable
		-- them if we're sorting by Z-order.
		local enable = (gSortOrder == "z") and not isDialog
		Control_SetVisible(gHandles["lblZOrder2"], true)
		Control_SetVisible(gHandles["btnUp"], true)
		Control_SetEnabled(gHandles["btnUp"], enable)
		Control_SetVisible(gHandles["btnDown"], true)
		Control_SetEnabled(gHandles["btnDown"], enable)
		Control_SetVisible(gHandles["btnTop"], true)
		Control_SetEnabled(gHandles["btnTop"], enable)
		Control_SetVisible(gHandles["btnBottom"], true)
		Control_SetEnabled(gHandles["btnBottom"], enable)

		local ySoFar = Control_GetLocationY(gHandles["imgHorizRuler_Text"])

		local lineHeight = 21
		local pre = 6
		local post = 2
		ySoFar = BuildGeneralBlock(    ySoFar, currentControlTypeName, lineHeight, pre, post )
		ySoFar = BuildPositionBlock(   ySoFar, currentControlTypeName, lineHeight, pre, post )
		ySoFar = BuildCheckboxesBlock( ySoFar, currentControlTypeName, lineHeight, pre, post )
		ySoFar = BuildElementsBlock(   ySoFar, currentControlTypeName, lineHeight, pre, post )
		ySoFar = BuildFontBlock(       ySoFar, currentControlTypeName, lineHeight, pre, post )
	end
end

function BuildGeneralBlock( StartPositionY, currentControlTypeName, lineHeight, preSpacing, postSpacing )
	local height = StartPositionY

	ShowControlAtY(gHandles["imgHorizRuler_Text"], height)
	height = height + preSpacing

	if currentControlTypeName == "Dialog" then

		ShowControlAtY(gHandles["lblFilename"], height)
		ShowControlAtY(gHandles["edFilename"], height)
		EditBox_SetText( gHandles["edFilename"], gXmlName or "", false )
		height = height + lineHeight

		ShowControlAtY(gHandles["lblScript"], height)
		ShowControlAtY(gHandles["edScript"], height)
		ShowControlAtY(gHandles["btnEdit"], height)
		EditBox_SetText( gHandles["edScript"], Dialog_GetScriptName(gCurrentControl) or "", false )
		height = height + lineHeight

		ShowControlAtY(gHandles["lblText"], height)
		ShowControlAtY(gHandles["edText"], height)
		EditBox_SetText( gHandles["edText"], Dialog_GetCaptionText(gCurrentControl) or "", false )
		height = height + lineHeight

	elseif currentControlTypeName == "Comment" then

		ShowControlAtY(gHandles["lblComment"], height)
		ShowControlAtY(gHandles["edComment"], height)
		EditBox_SetText( gHandles["edComment"], Control_GetComment(gCurrentControl) or "", false )
		height = height + lineHeight

	else

		ShowControlAtY(gHandles["lblName"], height)
		ShowControlAtY(gHandles["edName"], height)
		EditBox_SetText( gHandles["edName"], Control_GetName(gCurrentControl) or "", false )
		height = height + lineHeight

		ShowControlAtY(gHandles["lblComment"], height)
		ShowControlAtY(gHandles["edComment"], height)
		EditBox_SetText( gHandles["edComment"], Control_GetComment(gCurrentControl) or "", false )
		height = height + lineHeight

		if  currentControlTypeName == "Button" or
		currentControlTypeName == "Static" or
		currentControlTypeName == "CheckBox" or
		currentControlTypeName == "RadioButton" then

			ShowControlAtY(gHandles["lblText"], height)
			ShowControlAtY(gHandles["edText"], height)
			EditBox_SetText( gHandles["edText"], Static_GetText(gCurrentControl) or "", false )
			height = height + lineHeight

		elseif currentControlTypeName == "EditBox" then

			ShowControlAtY(gHandles["lblText"], height)
			ShowControlAtY(gHandles["edText"], height)
			EditBox_SetText( gHandles["edText"], EditBox_GetText(gCurrentControl) or "", false )
			height = height + lineHeight

		end

	end

	height = height + postSpacing

	return height
end

function BuildPositionBlock( StartPositionY, currentControlTypeName, lineHeight, preSpacing, postSpacing )
	local height = StartPositionY

	if currentControlTypeName == "Comment" then
		return height
	end

	ShowControlAtY(gHandles["imgHorizRuler_Positions"], height)
	height = height + preSpacing

	ShowControlAtY(gHandles["lblPosition"], height)
	ShowControlAtY(gHandles["lblSize"], height)
	height = height + lineHeight

	local strPosX, strPosY, strSizeW, strSizeH
	if currentControlTypeName == "Dialog" then
		strPosX = tostring(Dialog_GetLocationX(gCurrentControl))
		strPosY = tostring(Dialog_GetLocationY(gCurrentControl))
		strSizeW = tostring(Dialog_GetWidth(gCurrentControl))
		strSizeH = tostring(Dialog_GetHeight(gCurrentControl))
	else
		strPosX = tostring(Control_GetLocationX(gCurrentControl))
		strPosY = tostring(Control_GetLocationY(gCurrentControl))
		strSizeW = tostring(Control_GetWidth(gCurrentControl))
		strSizeH = tostring(Control_GetHeight(gCurrentControl))
	end

	ShowControlAtY(gHandles["lblPositionX"], height)
	ShowControlAtY(gHandles["edPositionX"], height)
	ShowControlAtY(gHandles["lblPositionY"], height)
	ShowControlAtY(gHandles["edPositionY"], height)
	ShowControlAtY(gHandles["lblSizeW"], height)
	ShowControlAtY(gHandles["edSizeW"], height)
	ShowControlAtY(gHandles["lblSizeH"], height)
	ShowControlAtY(gHandles["edSizeH"], height)
	EditBox_SetText( gHandles["edPositionX"], strPosX, false )
	EditBox_SetText( gHandles["edPositionY"], strPosY, false )
	EditBox_SetText( gHandles["edSizeW"], strSizeW, false )
	EditBox_SetText( gHandles["edSizeH"], strSizeH, false )
	height = height + lineHeight

	if currentControlTypeName == "Dialog" then

		local lFormat = Dialog_GetLocationFormat(gCurrentControl)
		local valign, halign = decodeDialogFormat(lFormat)

		ShowControlAtY(gHandles["lblDialogAlignH"], height)
		ShowControlAtY(gHandles["cmbDialogAlignH"], height)
		ComboBox_SetSelectedByData(gHandles["cmbDialogAlignH"], halign)
		height = height + lineHeight

		ShowControlAtY(gHandles["lblDialogAlignV"], height)
		ShowControlAtY(gHandles["cmbDialogAlignV"], height)
		ComboBox_SetSelectedByData(gHandles["cmbDialogAlignV"], valign)
		height = height + lineHeight

		ShowControlAtY(gHandles["lblTitlebarHeight"], height)
		ShowControlAtY(gHandles["edTitlebarHeight"], height)
		EditBox_SetText(gHandles["edTitlebarHeight"], Dialog_GetCaptionHeight(gCurrentControl), false)
		height = height + lineHeight

	elseif currentControlTypeName == "ComboBox" then

		ShowControlAtY(gHandles["lblScrollBarWidth"], height)
		ShowControlAtY(gHandles["edScrollBarWidth"], height)
		EditBox_SetText(gHandles["edScrollBarWidth"], ComboBox_GetScrollBarWidth(gCurrentControl), false)
		ShowControlAtY(gHandles["lblScrollStep"], height)
		ShowControlAtY(gHandles["edScrollStep"], height)
		EditBox_SetText(gHandles["edScrollStep"], ScrollBar_GetPageSize( ComboBox_GetScrollBar(gCurrentControl) ), false)
		height = height + lineHeight

		ShowControlAtY(gHandles["lblDropHeight"], height)
		ShowControlAtY(gHandles["edDropHeight"], height)
		EditBox_SetText(gHandles["edDropHeight"], ComboBox_GetDropHeight(gCurrentControl), false)
		height = height + lineHeight

	elseif currentControlTypeName == "Slider" then

		ShowControlAtY(gHandles["lblSliderValue"], height)
		ShowControlAtY(gHandles["edSliderValue"], height)
		EditBox_SetText(gHandles["edSliderValue"], Slider_GetValue(gCurrentControl), false)
		ShowControlAtY(gHandles["lblSliderRange"], height)
		ShowControlAtY(gHandles["edSliderRangeMin"], height)
		EditBox_SetText(gHandles["edSliderRangeMin"], Slider_GetRangeMin(gCurrentControl), false)
		ShowControlAtY(gHandles["lblSliderRangeTo"], height)
		ShowControlAtY(gHandles["edSliderRangeMax"], height)
		EditBox_SetText(gHandles["edSliderRangeMax"], Slider_GetRangeMax(gCurrentControl), false)
		height = height + lineHeight

	elseif currentControlTypeName == "EditBox" then

		ShowControlAtY(gHandles["lblSpacing"], height)
		ShowControlAtY(gHandles["edSpacing"], height)
		EditBox_SetText(gHandles["edSpacing"], EditBox_GetSpacing(gCurrentControl), false)
		ShowControlAtY(gHandles["lblBorderWidth"], height)
		ShowControlAtY(gHandles["edBorderWidth"], height)
		EditBox_SetText(gHandles["edBorderWidth"], EditBox_GetBorderWidth(gCurrentControl), false)
		height = height + lineHeight

	elseif currentControlTypeName == "ListBox" then

		ShowControlAtY(gHandles["lblScrollBarWidth"], height)
		ShowControlAtY(gHandles["edScrollBarWidth"], height)
		EditBox_SetText(gHandles["edScrollBarWidth"], ListBox_GetScrollBarWidth(gCurrentControl), false)
		ShowControlAtY(gHandles["lblScrollStep"], height)
		ShowControlAtY(gHandles["edScrollStep"], height)
		EditBox_SetText(gHandles["edScrollStep"], ScrollBar_GetPageSize( ListBox_GetScrollBar(gCurrentControl) ), false)
		height = height + lineHeight

		ShowControlAtY(gHandles["lblRowHeight"], height)
		ShowControlAtY(gHandles["edRowHeight"], height)
		EditBox_SetText(gHandles["edRowHeight"], ListBox_GetRowHeight(gCurrentControl), false)
		ShowControlAtY(gHandles["lblBorderWidth"], height)
		ShowControlAtY(gHandles["edBorderWidth"], height)
		EditBox_SetText(gHandles["edBorderWidth"], ListBox_GetBorder(gCurrentControl), false)
		height = height + lineHeight

	end

	height = height + postSpacing

	return height
end

function BuildCheckboxesBlock( StartPositionY, currentControlTypeName, lineHeight, preSpacing, postSpacing )
	local height = StartPositionY

	if currentControlTypeName == "Comment" then
		return height
	end

	ShowControlAtY(gHandles["imgHorizRuler_Checkboxes"], height)
	height = height + preSpacing

	if currentControlTypeName == "Dialog" then

		ShowControlAtY(gHandles["boxMovable"], height)
		CheckBox_SetChecked(gHandles["boxMovable"], Dialog_GetMovable(gCurrentControl))
		ShowControlAtY(gHandles["boxMinimized"], height)
		CheckBox_SetChecked(gHandles["boxMinimized"], Dialog_GetMinimized(gCurrentControl))
		height = height + lineHeight

		ShowControlAtY(gHandles["boxSnapToEdges"], height)
		CheckBox_SetChecked(gHandles["boxSnapToEdges"], Dialog_GetSnapToEdges(gCurrentControl))
		ShowControlAtY(gHandles["boxModal"], height)
		CheckBox_SetChecked(gHandles["boxModal"], Dialog_GetModal(gCurrentControl))
		height = height + lineHeight

		-- DRF - Change To CloseOnEsc
		ShowControlAtY(gHandles["boxCloseOnESC"], height)
		CheckBox_SetChecked(gHandles["boxCloseOnESC"], Dialog_GetCloseOnESC(gCurrentControl))

		ShowControlAtY(gHandles["boxEnableKeyboard"], height)
		CheckBox_SetChecked(gHandles["boxEnableKeyboard"], Dialog_GetEnableKeyboardInput(gCurrentControl))
		height = height + lineHeight

	elseif currentControlTypeName == "Button" then

		ShowControlAtY(gHandles["boxEnabled"], height)
		CheckBox_SetChecked(gHandles["boxEnabled"], Control_GetEnabled(gCurrentControl))
		ShowControlAtY(gHandles["boxVisible"], height)
		CheckBox_SetChecked(gHandles["boxVisible"], Control_GetVisible(gCurrentControl))
		height = height + lineHeight
	elseif currentControlTypeName == "Static" then

		local elementTag = gElementTags[currentControlTypeName][1]
		local element = Control_GetDisplayElement(gCurrentControl, elementTag)
		local textFormat = Element_GetTextFormat(element)

		ShowControlAtY(gHandles["boxEnabled"], height)
		CheckBox_SetChecked(gHandles["boxEnabled"], Control_GetEnabled(gCurrentControl))
		ShowControlAtY(gHandles["boxVisible"], height)
		CheckBox_SetChecked(gHandles["boxVisible"], Control_GetVisible(gCurrentControl))
		ShowControlAtY(gHandles["boxUseHTML"], height)
		CheckBox_SetChecked(gHandles["boxUseHTML"], Static_GetUseHTML(gCurrentControl))
		height = height + lineHeight
	elseif currentControlTypeName == "CheckBox" then

		ShowControlAtY(gHandles["boxEnabled"], height)
		CheckBox_SetChecked(gHandles["boxEnabled"], Control_GetEnabled(gCurrentControl))
		ShowControlAtY(gHandles["boxVisible"], height)
		CheckBox_SetChecked(gHandles["boxVisible"], Control_GetVisible(gCurrentControl))
		ShowControlAtY(gHandles["boxSelected"], height)
		CheckBox_SetChecked(gHandles["boxSelected"], CheckBox_GetChecked(gCurrentControl))
		height = height + lineHeight
	elseif currentControlTypeName == "RadioButton" then

		ShowControlAtY(gHandles["boxEnabled"], height)
		CheckBox_SetChecked(gHandles["boxEnabled"], Control_GetEnabled(gCurrentControl))
		ShowControlAtY(gHandles["boxVisible"], height)
		CheckBox_SetChecked(gHandles["boxVisible"], Control_GetVisible(gCurrentControl))
		ShowControlAtY(gHandles["boxSelected"], height)
		CheckBox_SetChecked(gHandles["boxSelected"], CheckBox_GetChecked(gCurrentControl))
		height = height + lineHeight

		ShowControlAtY(gHandles["lblGroupID"], height)
		ShowControlAtY(gHandles["edGroupID"], height)
		EditBox_SetText(gHandles["edGroupID"], tostring(RadioButton_GetButtonGroup(gCurrentControl)), false )
		height = height + lineHeight
	elseif currentControlTypeName == "ComboBox" then

		ShowControlAtY(gHandles["boxEnabled"], height)
		CheckBox_SetChecked(gHandles["boxEnabled"], Control_GetEnabled(gCurrentControl))
		ShowControlAtY(gHandles["boxVisible"], height)
		CheckBox_SetChecked(gHandles["boxVisible"], Control_GetVisible(gCurrentControl))
		height = height + lineHeight
	elseif currentControlTypeName == "Slider" then

		ShowControlAtY(gHandles["boxEnabled"], height)
		CheckBox_SetChecked(gHandles["boxEnabled"], Control_GetEnabled(gCurrentControl))
		ShowControlAtY(gHandles["boxVisible"], height)
		CheckBox_SetChecked(gHandles["boxVisible"], Control_GetVisible(gCurrentControl))
		height = height + lineHeight
	elseif currentControlTypeName == "EditBox" then

		ShowControlAtY(gHandles["boxEnabled"], height)
		CheckBox_SetChecked(gHandles["boxEnabled"], Control_GetEnabled(gCurrentControl))
		ShowControlAtY(gHandles["boxVisible"], height)
		CheckBox_SetChecked(gHandles["boxVisible"], Control_GetVisible(gCurrentControl))
		height = height + lineHeight
		ShowControlAtY(gHandles["boxEditable"], height)
		CheckBox_SetChecked(gHandles["boxEditable"], EditBox_GetEditable(gCurrentControl))
		ShowControlAtY(gHandles["boxPassword"], height)
		CheckBox_SetChecked(gHandles["boxPassword"], EditBox_GetPassword(gCurrentControl))
		height = height + lineHeight
	elseif currentControlTypeName == "ListBox" then

		ShowControlAtY(gHandles["boxEnabled"], height)
		CheckBox_SetChecked(gHandles["boxEnabled"], Control_GetEnabled(gCurrentControl))
		ShowControlAtY(gHandles["boxVisible"], height)
		CheckBox_SetChecked(gHandles["boxVisible"], Control_GetVisible(gCurrentControl))
		ShowControlAtY(gHandles["boxMultiSelect"], height)
		CheckBox_SetChecked(gHandles["boxMultiSelect"], ListBox_GetMultiSelection(gCurrentControl))
		height = height + lineHeight
	elseif currentControlTypeName == "Image" then

		ShowControlAtY(gHandles["boxEnabled"], height)
		CheckBox_SetChecked(gHandles["boxEnabled"], Control_GetEnabled(gCurrentControl))
		ShowControlAtY(gHandles["boxVisible"], height)
		CheckBox_SetChecked(gHandles["boxVisible"], Control_GetVisible(gCurrentControl))
		height = height + lineHeight
	end

	height = height + postSpacing

	return height
end

g_ElementsBlockBuiltFor = nil

function BuildElementsBlock( StartPositionY, currentControlTypeName, lineHeight, preSpacing, postSpacing )
	local height = StartPositionY

	local uiNeeded = gBuildElementsBlock[currentControlTypeName]
	if uiNeeded == nil then
		return height
	end

	if (uiNeeded[1] or uiNeeded[2] or uiNeeded[3] or uiNeeded[4] or uiNeeded[5]) then

		ShowControlAtY(gHandles["imgHorizRuler_Elements"], height)
		height = height + preSpacing

		if uiNeeded[1] or uiNeeded[2] then
			if uiNeeded[1] then
				ShowControlAtY(gHandles["lblElement"], height)
				ShowControlAtY(gHandles["cmbElement"], height)
			end
			if uiNeeded[2] then
				ShowControlAtY(gHandles["btnSelectTexture"], height)
			end
			height = height + lineHeight
		end

		if uiNeeded[3] or uiNeeded[4] or uiNeeded[5] then
			if uiNeeded[3] then
				ShowControlAtY(gHandles["lblState"], height)
				ShowControlAtY(gHandles["cmbState"], height)
			end
			if uiNeeded[4] then
				ShowControlAtY(gHandles["btnListEditor"], height)
			end
			if uiNeeded[5] then
				ShowControlAtY(gHandles["lblElementColor"], height)
				ShowControlAtY(gHandles["imgElementColorSwatchBG"], height)
				ShowControlAtY(gHandles["btnElementColorSwatch"], height)
			end
			height = height + lineHeight
		end

		height = height + postSpacing

	end

	-- fill in the Elements combo box for the current control
	local cmbElement = gHandles["cmbElement"]
	if g_ElementsBlockBuiltFor ~= gCurrentControl then
		ComboBox_RemoveAllItems(cmbElement)
		for i,elementTag in ipairs(gElementTags[currentControlTypeName]) do
			ComboBox_AddItem(cmbElement, elementTag, i)
		end
		ComboBox_SetSelectedByIndex(cmbElement, 0)
		g_ElementsBlockBuiltFor = gCurrentControl
	end
	OnElementBoxChanged(cmbElement)

	return height
end

function BuildFontBlock( StartPositionY, currentControlTypeName, lineHeight, preSpacing, postSpacing )
	local height = StartPositionY

	Control_SetLocationY(gHandles["imgHorizRuler_Font"], height)

	if gElementTags[currentControlTypeName] == nil or #gElementTags[currentControlTypeName] == 0 then
		return height
	end

	local element = GetCurrentFontElement()
	if element == nil then
		return height
	end

	ShowControlAtY(gHandles["imgHorizRuler_Font"], height)
	height = height + preSpacing

	ShowControlAtY(gHandles["lblFontFace"], height)
	ShowControlAtY(gHandles["cmbFontFace"], height)
	ComboBox_SetSelectedByText(gHandles["cmbFontFace"], Element_GetFontFace(element, gCurrentMenu ) )
	height = height + lineHeight

	ShowControlAtY(gHandles["lblFontWeight"], height)
	ShowControlAtY(gHandles["cmbFontWeight"], height)
	ComboBox_SetSelectedByData(gHandles["cmbFontWeight"], Element_GetFontWeight(element, gCurrentMenu ) )
	ShowControlAtY(gHandles["boxFontItalic"], height)
	CheckBox_SetChecked(gHandles["boxFontItalic"], Element_GetFontItalic(element, gCurrentMenu) )
	height = height + lineHeight

	local textFormat = Element_GetTextFormat(element)
	local wordWrap, valign, halign = decodeTextFormat(textFormat)

	ShowControlAtY(gHandles["lblFontSize"], height)
	ShowControlAtY(gHandles["edFontSize"], height)
	EditBox_SetText(gHandles["edFontSize"], tostring( math.abs(Element_GetFontHeight(element, gCurrentMenu)) ) or "", false )
	ShowControlAtY(gHandles["lblFontColor"], height)
	ShowControlAtY(gHandles["imgFontColorSwatchBG"], height)
	ShowControlAtY(gHandles["btnFontColorSwatch"], height)

	local state = ComboBox_GetSelectedData(gHandles["cmbState"])
	local color = BlendColor_GetColor(Element_GetFontColor(element), state)
	SetAllButtonColors(gHandles["btnFontColorSwatch"], color)

	ShowControlAtY(gHandles["boxWordWrap"], height)
	CheckBox_SetChecked(gHandles["boxWordWrap"], wordWrap == TEXT_WORDWRAP)
	height = height + lineHeight

	ShowControlAtY(gHandles["lblFontAlignH"], height)
	ShowControlAtY(gHandles["cmbFontAlignH"], height)
	ComboBox_SetSelectedByData(gHandles["cmbFontAlignH"], halign)
	height = height + lineHeight

	ShowControlAtY(gHandles["lblFontAlignV"], height)
	ShowControlAtY(gHandles["cmbFontAlignV"], height)
	ComboBox_SetSelectedByData(gHandles["cmbFontAlignV"], valign)
	height = height + lineHeight

	height = height + postSpacing

	return height
end

function btnClose_OnButtonClicked(buttonHandle)
	if gCurrentMenu then
		KEP_YesNoBox( CLOSE_MSG, "Confirm", WF.MENUEDITOR_EXIT_FILTER)
	else
		MenuCloseThis()
	end
end

g_WasModal = false

function btnExitPreview_OnButtonClicked(buttonHandle)
	if gCurrentMenu then
		if Dialog_GetEdit(gCurrentMenu) == 0 then
			Dialog_SetEdit(gCurrentMenu, true)
			Dialog_SetModal(gCurrentMenu, g_WasModal)
			MenuCenterCurrent()

			ResetControlProperties()
		end
	end
end

function btnPreview_OnButtonClicked(buttonHandle)
	if gCurrentMenu then
		if Dialog_GetEdit(gCurrentMenu) ~= 0 then
			local format = Dialog_GetLocationFormat(gCurrentMenu)
			Dialog_SetLocationFormat(gCurrentMenu, format)
			Dialog_SetEdit(gCurrentMenu, false)

			-- When previewing, force the dialog to be non-modal, since
			-- you could never leave Preview if it's stealing all mouse inputs
			-- and not running a script to handle its close button.
			g_WasModal = Dialog_GetModal(gCurrentMenu)
			Dialog_SetModal(gCurrentMenu, false)

			ResetControlProperties()
		end
	end
end

function radioZOrder_OnRadioButtonChanged( radioButtonHandle )
	gSortOrder = "z"

	local controlName = ListBox_GetSelectedItemText(glstControls)
	ResetControlList()
	local newIndex = findListBoxItem(glstControls, controlName)
	ListBox_SelectItem(glstControls, newIndex)
end

function radioAOrder_OnRadioButtonChanged( radioButtonHandle )
	gSortOrder = "a"

	local controlName = ListBox_GetSelectedItemText(glstControls)
	ResetControlList()
	local newIndex = findListBoxItem(glstControls, controlName)
	ListBox_SelectItem(glstControls, newIndex)
end

function lstControls_OnListBoxSelection(listBoxHandle, x, y)
	g_controlName = ListBox_GetSelectedItemText(listBoxHandle) or "nil"
	gCurrentControl = gControlProperties[g_controlName].handle or nil
	if gCurrentControl == nil then return end
	if g_controlName ~= gMenuName then
		Control_SetEditing(gCurrentControl, true)
	end
	ResetControlProperties()
end

function btnDelete_OnButtonClicked(buttonHandle)
	deleteControl()
end

function btnCopy_OnButtonClicked(buttonHandle)
	copyControls()
end

function btnPaste_OnButtonClicked(buttonHandle)
	pasteControls()
end

function btnUp_OnButtonClicked(buttonHandle)
	local index = ListBox_GetSelectedItemIndex(glstControls)
	if (index == nil) or (index <= 1) then return end
	local controlName = ListBox_GetSelectedItemText(glstControls)
	local controlHandle = gControlProperties[controlName].handle
	doAction( gCurrentMenu, "Dialog_MoveControlBackward", {controlHandle}, "Dialog_MoveControlForward", {controlHandle}, true, index-1, index)
end

function btnDown_OnButtonClicked(buttonHandle)
	local index = ListBox_GetSelectedItemIndex(glstControls)
	local size = ListBox_GetSize(glstControls)
	if (index == nil) or (index == 0) then return end
	if (index >= size - 1) then return end
	local controlName = ListBox_GetSelectedItemText(glstControls)
	local controlHandle = gControlProperties[controlName].handle
	doAction( gCurrentMenu, "Dialog_MoveControlForward", {controlHandle}, "Dialog_MoveControlBackward", {controlHandle}, true, index+1, index)
end

function btnTop_OnButtonClicked(buttonHandle)
	local index = ListBox_GetSelectedItemIndex(glstControls)
	if (index == nil) or (index == 0) then return end
	local controlName = ListBox_GetSelectedItemText(glstControls)
	local controlHandle = gControlProperties[controlName].handle
	doAction( gCurrentMenu, "Dialog_MoveControlToBack", {controlHandle}, "Dialog_MoveControlToIndex", {controlHandle, index-1}, true, 1, index)
end

function btnBottom_OnButtonClicked(buttonHandle)
	local index = ListBox_GetSelectedItemIndex(glstControls)
	if (index == nil) or (index == 0) then return end
	local controlName = ListBox_GetSelectedItemText(glstControls)
	local controlHandle = gControlProperties[controlName].handle
	local lastIndex = ListBox_GetSize(glstControls) - 1
	doAction( gCurrentMenu, "Dialog_MoveControlToFront", {controlHandle}, "Dialog_MoveControlToIndex", {controlHandle, index-1}, true, lastIndex, index)
end

function btnEdit_OnButtonClicked(buttonHandle)
	local fname = gCurrentMenuScriptName
	local baseName = string.sub(fname, 1, string.len(fname)-4)
	KEP_RequestSourceAssetFromAppServer(DevToolsUpload.MENU_SCRIPT, baseName)
end

-----------------------------------------------------------------------
-- Each EditBox needs to set the dialog to Modal while being edited
-- to prevent input from reaching the avatar controls.
-----------------------------------------------------------------------

function undoableSetFilename(handle, newName)
	local i, j
	gXmlName = newName
	i, j, gMenuName = string.find(gXmlName, "(.*)%.xml")
	Dialog_SetCaptionText( gDialogHandle, MENU_TITLE..gMenuName )
	local filePath = KEP_Get3DAppUnpublishedAssetPath(DevToolsUpload.MENU_XML)
	if filePath then
		Dialog_SetXmlName(gCurrentMenu, filePath..gXmlName)
	end
end

function edFilename_OnFocusOut(editBoxHandle)
	edFilename_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edFilename_OnEditBoxString(editBoxHandle, text)
	local name = EditBox_GetText(editBoxHandle)
	if name ~= gXmlName then
		doAction( gCurrentMenu, "undoableSetFilename", {name}, "undoableSetFilename", {gXmlName}, true )
	end
end

function edName_OnFocusOut(editBoxHandle)
	edName_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edName_OnEditBoxString(editBoxHandle, text)
	local name = EditBox_GetText(editBoxHandle)
	local currentName = Control_GetName(gCurrentControl)
	if name ~= currentName then
		doAction( gCurrentControl, "Control_SetNameUnconflicting", {name}, "Control_SetName", { currentName }, true )
	end
end

function edComment_OnFocusOut(editBoxHandle)
	edComment_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edComment_OnEditBoxString(editBoxHandle, text)
	local comment = EditBox_GetText(editBoxHandle)
	local currentComment = Control_GetComment(gCurrentControl)
	if comment ~= currentComment then
		doAction( gCurrentControl, "Control_SetComment", {comment}, "Control_SetComment", { currentComment }, true )
	end
end

function undoableSetText(handle, text)
	local controlTypeName = GetControlTypeName(handle)
	if controlTypeName == "Dialog" then
		Dialog_SetCaptionText(handle, text)
		Dialog_SetCaptionEnabled(handle, text ~= "" ) -- enable the caption if there's any caption text
	elseif controlTypeName == "EditBox" then
		EditBox_SetText(handle, text, false)
	else
		-- Most other types use this, since they derive from Static
		Static_SetText(handle, text)
	end
end

function edText_OnFocusOut(editBoxHandle)
	edText_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edText_OnEditBoxString(editBoxHandle, text)
	local text = EditBox_GetText(editBoxHandle)
	local currentText = ""
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	if currentControlTypeName == "Dialog" then
		currentText = Dialog_GetCaptionText(gCurrentControl)
	elseif currentControlTypeName == "EditBox" then
		currentText = EditBox_GetText(gCurrentControl)
	else
		-- Most other types use this, since they derive from Static
		currentText = Static_GetText(gCurrentControl)
	end
	if text ~= currentText then
		doAction( gCurrentControl, "undoableSetText", {text}, "undoableSetText", { currentText }, false )
	end
end

function undoableSetScriptname(handle, str)
	gCurrentMenuScriptName = str
	Dialog_SetScriptName( gCurrentMenu, gCurrentMenuScriptName )
end

function edScript_OnFocusOut(editBoxHandle)
	edScript_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edScript_OnEditBoxString(editBoxHandle, text)
	local scriptName = EditBox_GetText(editBoxHandle)
	if scriptName ~= gCurrentMenuScriptName then
		gModifiedScriptName = true
		doAction( gCurrentMenu, "undoableSetScriptname", {scriptName}, "undoableSetScriptname", { gCurrentMenuScriptName }, false )
	end
end

function edPositionX_OnFocusOut(editBoxHandle)
	edPositionX_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edPositionX_OnEditBoxString(editBoxHandle, text)
	local pos = eval(EditBox_GetText(editBoxHandle))
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	if currentControlTypeName == "Dialog" then
		local currentPos = Dialog_GetLocationX(gCurrentControl)
		if pos ~= currentPos then
			doAction( gCurrentControl, "Dialog_SetLocationX", {pos}, "Dialog_SetLocationX", { currentPos }, false )
		end
	else
		local currentPos = Control_GetLocationX(gCurrentControl)
		if pos ~= currentPos then
			doAction( gCurrentControl, "Control_SetLocationX", {pos}, "Control_SetLocationX", { currentPos }, false )
		end
	end
end

function edPositionY_OnFocusOut(editBoxHandle)
	edPositionY_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edPositionY_OnEditBoxString(editBoxHandle, text)
	local pos = eval(EditBox_GetText(editBoxHandle))
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	if currentControlTypeName == "Dialog" then
		local currentPos = Dialog_GetLocationY(gCurrentControl)
		if pos ~= currentPos then
			doAction( gCurrentControl, "Dialog_SetLocationY", {pos}, "Dialog_SetLocationY", { currentPos }, false )
		end
	else
		local currentPos = Control_GetLocationY(gCurrentControl)
		if pos ~= currentPos then
			doAction( gCurrentControl, "Control_SetLocationY", {pos}, "Control_SetLocationY", { currentPos }, false )
		end
	end
end

function edSizeW_OnFocusOut(editBoxHandle)
	edSizeW_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edSizeW_OnEditBoxString(editBoxHandle, text)
	local size = eval(EditBox_GetText(editBoxHandle))
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	if currentControlTypeName == "Dialog" then
		local currentSize = Dialog_GetWidth(gCurrentControl)
		if size ~= currentSize then
			doAction( gCurrentControl, "Dialog_SetSize", {size, Dialog_GetHeight(gCurrentControl)}, "Dialog_SetSize", { currentSize, Dialog_GetHeight(gCurrentControl) }, false )
		end
	else
		local currentSize = Control_GetWidth(gCurrentControl)
		if size ~= currentSize then
			doAction( gCurrentControl, "Control_SetSize", {size, Control_GetHeight(gCurrentControl)}, "Control_SetSize", { currentSize, Control_GetHeight(gCurrentControl) }, false )
		end
	end
end

function edSizeH_OnFocusOut(editBoxHandle)
	edSizeH_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edSizeH_OnEditBoxString(editBoxHandle, text)
	local size = eval(EditBox_GetText(editBoxHandle))
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	if currentControlTypeName == "Dialog" then
		local currentSize = Dialog_GetHeight(gCurrentControl)
		if size ~= currentSize then
			doAction( gCurrentControl, "Dialog_SetSize", {Dialog_GetWidth(gCurrentControl), size}, "Dialog_SetSize", { Dialog_GetWidth(gCurrentControl), currentSize }, false )
		end
	else
		local currentSize = Control_GetHeight(gCurrentControl)
		if size ~= currentSize then
			doAction( gCurrentControl, "Control_SetSize", {Control_GetWidth(gCurrentControl), size}, "Control_SetSize", { Control_GetWidth(gCurrentControl), currentSize }, false )
		end
	end

end

function edScrollBarWidth_OnFocusOut(editBoxHandle)
	edScrollBarWidth_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edScrollBarWidth_OnEditBoxString(editBoxHandle, text)
	local width = eval(EditBox_GetText(editBoxHandle))
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	if currentControlTypeName == "ListBox" then
		local currentWidth = ListBox_GetScrollBarWidth(gCurrentControl)
		if width ~= currentWidth then
			doAction( gCurrentControl, "ListBox_SetScrollBarWidth", {width}, "ListBox_SetScrollBarWidth", {currentWidth}, false )
		end
	elseif currentControlTypeName == "ComboBox" then
		local currentWidth = ComboBox_GetScrollBarWidth(gCurrentControl)
		if width ~= currentWidth then
			doAction( gCurrentControl, "ComboBox_SetScrollBarWidth", {width}, "ComboBox_SetScrollBarWidth", {currentWidth}, false )
		end
	end
end

function edScrollStep_OnFocusOut(editBoxHandle)
	edScrollStep_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edScrollStep_OnEditBoxString(editBoxHandle, text)
	local step = eval(EditBox_GetText(editBoxHandle))
	local scrollBar = nil
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	if currentControlTypeName == "ListBox" then
		scrollBar = ListBox_GetScrollBar(gCurrentControl)
	elseif currentControlTypeName == "ComboBox" then
		scrollBar = ComboBox_GetScrollBar(gCurrentControl)
	end

	local currentStep = ScrollBar_GetPageSize(scrollBar)
	if step ~= currentStep then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction( scrollBar, "ScrollBar_SetPageSize", {step}, "ScrollBar_SetPageSize", {currentStep}, false, index, index )
	end
end

function edDropHeight_OnFocusOut(editBoxHandle)
	edDropHeight_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edDropHeight_OnEditBoxString(editBoxHandle, text)
	local value = eval(EditBox_GetText(editBoxHandle))
	local current = ComboBox_GetDropHeight(gCurrentControl)
	if value ~= current then
		doAction( gCurrentControl, "ComboBox_SetDropHeight", {value}, "ComboBox_SetDropHeight", {current}, false )
	end
end

function edSliderValue_OnFocusOut(editBoxHandle)
	edSliderValue_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edSliderValue_OnEditBoxString(editBoxHandle, text)
	local value = eval(EditBox_GetText(editBoxHandle))
	local current = Slider_GetValue(gCurrentControl)
	if value ~= current then
		doAction( gCurrentControl, "Slider_SetValue", {value}, "Slider_SetValue", {current}, false )
	end
end

function edSliderRangeMin_OnFocusOut(editBoxHandle)
	edSliderRangeMin_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edSliderRangeMin_OnEditBoxString(editBoxHandle, text)
	local value = eval(EditBox_GetText(editBoxHandle))
	local current = Slider_GetRangeMin(gCurrentControl)
	if value ~= current then
		local max = Slider_GetRangeMax(gCurrentControl)
		doAction( gCurrentControl, "Slider_SetRange", {value, max}, "Slider_SetValue", {current, max}, false )
	end
end

function edSliderRangeMax_OnFocusOut(editBoxHandle)
	edSliderRangeMax_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edSliderRangeMax_OnEditBoxString(editBoxHandle, text)
	local value = eval(EditBox_GetText(editBoxHandle))
	local current = Slider_GetRangeMax(gCurrentControl)
	if value ~= current then
		local min = Slider_GetRangeMin(gCurrentControl)
		doAction( gCurrentControl, "Slider_SetRange", {min, value}, "Slider_SetValue", {min, current}, false )
	end
end

function edRowHeight_OnFocusOut(editBoxHandle)
	edRowHeight_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edRowHeight_OnEditBoxString(editBoxHandle, text)
	local value = eval(EditBox_GetText(editBoxHandle))
	local current = ListBox_GetRowHeight(gCurrentControl)
	if value ~= current then
		doAction( gCurrentControl, "ListBox_SetRowHeight", {value}, "ListBox_SetRowHeight", {current}, false )
	end
end

function edSpacing_OnFocusOut(editBoxHandle)
	edSpacing_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edSpacing_OnEditBoxString(editBoxHandle, text)
	local value = eval(EditBox_GetText(editBoxHandle))
	local current = EditBox_GetSpacing(gCurrentControl)
	if value ~= current then
		doAction( gCurrentControl, "EditBox_SetSpacing", {value}, "EditBox_SetSpacing", {current}, false )
	end
end

function edBorderWidth_OnFocusOut(editBoxHandle)
	edBorderWidth_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edBorderWidth_OnEditBoxString(editBoxHandle, text)
	local value = eval(EditBox_GetText(editBoxHandle))
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	if currentControlTypeName == "EditBox" then
		local current = EditBox_GetBorderWidth(gCurrentControl)
		if value ~= current then
			doAction( gCurrentControl, "EditBox_SetBorderWidth", {value}, "EditBox_SetBorderWidth", {current}, false )
		end
	elseif currentControlTypeName == "ListBox" then
		local current = ListBox_GetBorder(gCurrentControl)
		if value ~= current then
			doAction( gCurrentControl, "ListBox_SetBorder", {value}, "ListBox_SetBorder", {current}, false )
		end
	end
end

function edGroupID_OnFocusOut(editBoxHandle)
	edGroupID_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edGroupID_OnEditBoxString(editBoxHandle, text)
	local groupID = eval(EditBox_GetText(editBoxHandle))
	local currentGroupID = RadioButton_GetButtonGroup(gCurrentControl)
	if groupID ~= currentGroupID then
		doAction( gCurrentControl, "RadioButton_SetButtonGroup", {groupID}, "RadioButton_SetButtonGroup", {currentGroupID}, false )
	end
end

function edTitlebarHeight_OnFocusOut(editBoxHandle)
	edTitlebarHeight_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edTitlebarHeight_OnEditBoxString(editBoxHandle, text)
	local titlebarHeight = eval(EditBox_GetText(editBoxHandle))
	local currentTitlebarHeight = Dialog_GetCaptionHeight(gCurrentControl)
	if titlebarHeight ~= currentTitlebarHeight then
		doAction( gCurrentControl, "Dialog_SetCaptionHeight", {titlebarHeight}, "Dialog_SetCaptionHeight", {currentTitlebarHeight}, false )
	end
end

g_StateBoxBuiltForControl = nil
g_StateBoxBuiltForElementTag = nil

function OnElementBoxChanged(cmbElement)
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	local cmbState = gHandles["cmbState"]
	local elementTagIndex = ComboBox_GetSelectedData(cmbElement)
	local elementTag = gElementTags[currentControlTypeName][ elementTagIndex ]

	local GetElementTagUsesFontState = Control_GetElementTagUsesFontState
	local GetElementTagUsesTextureState = Control_GetElementTagUsesTextureState
	if currentControlTypeName == "Dialog" then
		GetElementTagUsesFontState = Dialog_GetElementTagUsesFontState
		GetElementTagUsesTextureState = Dialog_GetElementTagUsesTextureState
	end

	if g_StateBoxBuiltForControl ~= gCurrentControl or g_StateBoxBuiltForElementTag ~= elementTag then
		-- fill in the State combo box for the current Element
		ComboBox_RemoveAllItems(cmbState)
		for i,v in ipairs(gElementStateNames) do
			if GetElementTagUsesFontState(gCurrentControl, elementTag, v.state) ~= 0 or
			GetElementTagUsesTextureState(gCurrentControl, elementTag, v.state) ~= 0 then
				ComboBox_AddItem(cmbState, v.name, v.state)
			end
		end
		g_StateBoxBuiltForControl = gCurrentControl
		g_StateBoxBuiltForElementTag = elementTag
	end

	local state = ComboBox_GetSelectedData(cmbState)

	if ComboBox_GetNumItems(cmbState) <= 1 or not gBuildElementsBlock[currentControlTypeName][3] then
		Control_SetVisible(gHandles["lblState"], false)
		Control_SetVisible(cmbState, false)
	else
		Control_SetVisible(gHandles["lblState"], true)
		Control_SetVisible(cmbState, true)
	end

	if GetElementTagUsesTextureState(gCurrentControl, elementTag, state) ~= 0 then
		Control_SetVisible(gHandles["btnSelectTexture"], true)
	else
		Control_SetVisible(gHandles["btnSelectTexture"], false)
	end

	local color = nil
	if currentControlTypeName == "EditBox" then
		-- Special case stuff for EditBox
		--   Element combobox (modified to contain the various text modes)
		--   Select Texture button (only for Text Area)

		if elementTag == "textAreaDisplay" then
			Control_SetVisible(gHandles["btnSelectTexture"], true)
		else
			Control_SetVisible(gHandles["btnSelectTexture"], false)
		end

		if elementTag == "Caret Color" then
			color = EditBox_GetCaretColor(gCurrentControl)
		elseif elementTag == "Selected Color" then
			color = EditBox_GetSelectedTextColor(gCurrentControl)
		elseif elementTag == "Selected BG" then
			color = EditBox_GetSelectedBackColor(gCurrentControl)
		end
	end
	if color == nil then
		local element = GetCurrentTextureElement()
		if (element ~= nil) then
			local blendColor = Element_GetTextureColor(element)
			if (blendColor ~= nil) then
				color = BlendColor_GetColor(blendColor, state)
			end
		end
	end
	SetAllButtonColors(gHandles["btnElementColorSwatch"], color)

	-- NOTE: This next block relies on BuildFontBlock() setting imgHorizRuler_Font
	-- to the right position, even if it's not going to be shown.
	local ySoFar = Control_GetLocationY(gHandles["imgHorizRuler_Font"])
	local lineHeight = 21
	local pre = 6
	local post = 2
	local yAfter = BuildFontBlock( ySoFar, currentControlTypeName, lineHeight, pre, post )

	if yAfter == ySoFar then
		-- No font block created, so hide all the elements between the font block comments
		for i,controlName in ipairs(gFontBlockNames) do
			Control_SetVisible(gHandles[controlName], false)
		end
	end
end

function cmbElement_OnComboBoxSelectionChanged(cmbElement, index, text)
	OnElementBoxChanged(cmbElement)
end

function cmbState_OnComboBoxSelectionChanged(cmbState, index, text)
	local currentControlTypeName = GetControlTypeName(gCurrentControl)

	-- update the color swatch for this state
	local element = GetCurrentTextureElement()
	local blendColor = Element_GetTextureColor(element)
	local state = ComboBox_GetSelectedData(gHandles["cmbState"])
	local color = BlendColor_GetColor(blendColor, state)
	SetAllButtonColors(gHandles["btnElementColorSwatch"], color)

	-- NOTE: This next block relies on BuildFontBlock() setting imgHorizRuler_Font
	-- to the right position, even if it's not going to be shown.
	local ySoFar = Control_GetLocationY(gHandles["imgHorizRuler_Font"])
	local lineHeight = 21
	local pre = 6
	local post = 2
	local yAfter = BuildFontBlock( ySoFar, currentControlTypeName, lineHeight, pre, post )

	if yAfter == ySoFar then
		-- No font block created, so hide all the elements between the font block comments
		for i,controlName in ipairs(gFontBlockNames) do
			Control_SetVisible(gHandles[controlName], false)
		end
	end

end

function btnElementColorSwatch_OnButtonClicked(buttonHandle)
	MenuOpenModal("ColorPicker.xml")

	local color = BlendColor_GetColor(Element_GetTextureColor(Control_GetDisplayElement(gHandles["btnElementColorSwatch"], "normalDisplay")), ControlStates.DXUT_STATE_NORMAL)

	--Pass up the color data to the colorpicker
	local ev = KEP_EventCreate( COLOR_START_EVENT)
	KEP_EventSetFilter(ev, MENU_EDITOR_COLOR_FILTER)
	KEP_EventEncodeNumber(ev, color.a)
	KEP_EventEncodeNumber(ev, color.r)
	KEP_EventEncodeNumber(ev, color.g)
	KEP_EventEncodeNumber(ev, color.b)
	KEP_EventEncodeNumber(ev, ColorTarget.ELEMENT)
	KEP_EventQueue( ev)
end

function btnSelectTexture_OnButtonClicked(buttonHandle)
	MenuOpenModal("TexturePicker.xml")

	local element = GetCurrentTextureElement()
	local state = ComboBox_GetSelectedData(gHandles["cmbState"])
	local elementTagIndex = ComboBox_GetSelectedData(gHandles["cmbElement"])
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	local elementTag = gElementTags[currentControlTypeName][ elementTagIndex ]

	local tc = Element_GetTextureColor(element)
	local color = BlendColor_GetColor(tc, state)

	local ev = KEP_EventCreate( TEXTURE_LOAD_EVENT)
	KEP_EventSetFilter(ev, MENU_EDITOR_TEXTURE_FILTER)
	KEP_EventEncodeString(ev, Element_GetTextureName(element, gCurrentMenu))
	KEP_EventEncodeNumber(ev, Element_GetCoordLeft(element))
	KEP_EventEncodeNumber(ev, Element_GetCoordTop(element))
	KEP_EventEncodeNumber(ev, Element_GetCoordRight(element))
	KEP_EventEncodeNumber(ev, Element_GetCoordBottom(element))
	KEP_EventEncodeNumber(ev, color.a)
	KEP_EventEncodeNumber(ev, color.r)
	KEP_EventEncodeNumber(ev, color.g)
	KEP_EventEncodeNumber(ev, color.b)
	KEP_EventEncodeNumber(ev, Element_GetSliceLeft(element))
	KEP_EventEncodeNumber(ev, Element_GetSliceTop(element))
	KEP_EventEncodeNumber(ev, Element_GetSliceRight(element))
	KEP_EventEncodeNumber(ev, Element_GetSliceBottom(element))
	KEP_EventEncodeString(ev, elementTag) -- optionally blank
	KEP_EventQueue( ev)
	--end
end

function btnListEditor_OnButtonClicked(buttonHandle)
	MenuOpenModal("ListEditor.xml")

	local ev = KEP_EventCreate( LIST_EDITOR_START_EVENT)
	KEP_EventSetFilter(ev, MENU_EDITOR_LIST_EDITOR_FILTER)

	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	KEP_EventEncodeString(ev, currentControlTypeName)

	if currentControlTypeName == "ComboBox" then
		local size = ComboBox_GetNumItems(gCurrentControl)
		KEP_EventEncodeNumber(ev, size)
		for i=0,size-1 do
			KEP_EventEncodeString(ev, ComboBox_GetItemText(gCurrentControl, i))
			KEP_EventEncodeNumber(ev, ComboBox_GetItemData(gCurrentControl, i))
		end
	elseif currentControlTypeName == "ListBox" then
		local size = ListBox_GetSize(gCurrentControl)
		KEP_EventEncodeNumber(ev, size)
		for i=0,size-1 do
			KEP_EventEncodeString(ev, ListBox_GetItemText(gCurrentControl, i))
			KEP_EventEncodeNumber(ev, ListBox_GetItemData(gCurrentControl, i))
		end
	else
		KEP_EventEncodeNumber(ev, 0)
	end

	KEP_EventQueue( ev)
end

function undoableSetLocationFormat(handle, format)
	Dialog_SetLocationFormat(handle, format)
	MenuCenterCurrent()
end

function cmbDialogAlignH_OnComboBoxSelectionChanged(cmbDialogAlignH, index, text)
	local currentFormat = Dialog_GetLocationFormat(gCurrentMenu)
	local valign, halign = decodeDialogFormat(currentFormat)
	halign = ComboBox_GetSelectedData(cmbDialogAlignH)
	local newFormat = halign + valign
	doAction(gCurrentMenu, "undoableSetLocationFormat", {newFormat}, "undoableSetLocationFormat", {currentFormat}, false)
end

function cmbDialogAlignV_OnComboBoxSelectionChanged(cmbDialogAlignV, index, text)
	local currentFormat = Dialog_GetLocationFormat(gCurrentMenu)
	local valign, halign = decodeDialogFormat(currentFormat)
	valign = ComboBox_GetSelectedData(cmbDialogAlignV)
	local newFormat = halign + valign
	doAction(gCurrentMenu, "undoableSetLocationFormat", {newFormat}, "undoableSetLocationFormat", {currentFormat}, false)
end

function OnCornersBoxChanged(cmbCorners)
	local iCornerIndex = ComboBox_GetSelectedData(cmbCorners)
	local color = _G[ "Dialog_GetColor"..gCornerFunctions[iCornerIndex] ](gCurrentMenu)  --Dialog_GetColorTopLeft()

	SetAllButtonColors(gHandles["btnCornersSwatch"], color)
end

function cmbCorners_OnComboBoxSelectionChanged(cmbCorners, index, text)
	OnCornersBoxChanged(cmbCorners)
end

function btnCornersSwatch_OnButtonClicked(buttonHandle)
	MenuOpenModal("ColorPicker.xml")

	local color = BlendColor_GetColor(Element_GetTextureColor(Control_GetDisplayElement(gHandles["btnCornersSwatch"], "normalDisplay")), ControlStates.DXUT_STATE_NORMAL)

	--Pass up the color data to the colorpicker
	local ev = KEP_EventCreate( COLOR_START_EVENT)
	KEP_EventSetFilter(ev, MENU_EDITOR_COLOR_FILTER)
	KEP_EventEncodeNumber(ev, color.a)
	KEP_EventEncodeNumber(ev, color.r)
	KEP_EventEncodeNumber(ev, color.g)
	KEP_EventEncodeNumber(ev, color.b)
	KEP_EventEncodeNumber(ev, ColorTarget.CORNERS)
	KEP_EventQueue( ev)
end

function boxMovable_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentMenu, "Dialog_SetMovable", { checked }, "Dialog_SetMovable", { Dialog_GetMovable(gCurrentMenu) }, false)
end

function boxMinimized_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentMenu, "Dialog_SetMinimized", { checked }, "Dialog_SetMinimized", { Dialog_GetMinimized(gCurrentMenu) }, false)
end

function boxSnapToEdges_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentMenu, "Dialog_SetSnapToEdges", { checked }, "Dialog_SetSnapToEdges", { Dialog_GetSnapToEdges(gCurrentMenu) }, false)
end

function boxModal_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentMenu, "Dialog_SetModal", { checked }, "Dialog_SetModal", { Dialog_GetModal(gCurrentMenu) }, false)
end

function boxCloseOnESC_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentMenu, "Dialog_SetCloseOnESC", { checked }, "Dialog_SetCloseOnESC", { Dialog_GetCloseOnESC(gCurrentMenu) }, false)
end

function boxEnableKeyboard_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentMenu, "Dialog_SetKeyboardInputEnabled", { checked }, "Dialog_SetKeyboardInputEnabled", { Dialog_GetEnableKeyboardInput(gCurrentMenu) }, false)
end

function boxEnabled_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentControl, "Control_SetEnabled", { checked }, "Control_SetEnabled", { Control_GetEnabled(gCurrentControl) }, false)
end

function boxVisible_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentControl, "Control_SetVisible", { checked }, "Control_SetVisible", { Control_GetVisible(gCurrentControl) }, false)
end

function boxUseHTML_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentControl, "Static_SetUseHTML", { checked }, "Static_SetUseHTML", { Static_GetUseHTML(gCurrentControl) }, false)
end

function boxSelected_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentControl, "CheckBox_SetChecked", { checked }, "CheckBox_SetChecked", { CheckBox_GetChecked(gCurrentControl) }, false)
end

function boxEditable_OnCheckBoxChanged( chkHandle, checked )
	Log("editable="..tostring(checked))
	doAction(gCurrentControl, "EditBox_SetEditable", { checked }, "EditBox_SetEditable", { EditBox_GetEditable(gCurrentControl) }, false)
end

function boxPassword_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentControl, "EditBox_SetPassword", { checked }, "EditBox_SetPassword", { EditBox_GetPassword(gCurrentControl) }, false)
end

function boxMultiSelect_OnCheckBoxChanged( chkHandle, checked )
	doAction(gCurrentControl, "ListBox_SetMultiSelection", { checked }, "ListBox_SetMultiSelection", { ListBox_GetMultiSelection(gCurrentControl) }, false)
end

function getFont(element)
	local face = Element_GetFontFace(element, gCurrentMenu)
	local height = Element_GetFontHeight(element, gCurrentMenu)
	local weight = Element_GetFontWeight(element, gCurrentMenu)
	local italics = Element_GetFontItalic(element, gCurrentMenu)
	return face, height, weight, italics
end

function cmbFontFace_OnComboBoxSelectionChanged(comboBoxHandle, index, text)
	local element = GetCurrentFontElement()
	if element == nil then
		return
	end

	local face, height, weight, italics = getFont(element)
	if text ~= face then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction(element, "Element_AddFont", {gCurrentMenu, text, height, weight, italics}, "Element_AddFont", {gCurrentMenu, face, height, weight, italics}, false, index, index)
	end
end

function cmbFontWeight_OnComboBoxSelectionChanged(comboBoxHandle, index, text)
	local element = GetCurrentFontElement()
	if element == nil then
		return
	end

	local face, height, weight, italics = getFont(element)
	local new = ComboBox_GetSelectedData(comboBoxHandle)
	if new ~= weight then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction(element, "Element_AddFont", {gCurrentMenu, face, height, new, italics}, "Element_AddFont", {gCurrentMenu, face, height, weight, italics}, false, index, index)
	end
end

function boxFontItalic_OnCheckBoxChanged(chkHandle, checked)
	local element = GetCurrentFontElement()
	if element == nil then
		return
	end

	local face, height, weight, italics = getFont(element)
	if italics ~= checked then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction(element, "Element_AddFont", {gCurrentMenu, face, height, weight, checked}, "Element_AddFont", {gCurrentMenu, face, height, weight, italics}, false, index, index)
	end
end

function edFontSize_OnFocusOut(editBoxHandle)
	edFontSize_OnEditBoxString(editBoxHandle, EditBox_GetText(editBoxHandle))
end

function edFontSize_OnEditBoxString(editBoxHandle, text)
	local element = GetCurrentFontElement()
	if element == nil then
		return
	end

	local face, height, weight, italics = getFont(element)
	local abs = math.abs(eval(EditBox_GetText(editBoxHandle)) or height)
	local new = abs * -1
	EditBox_SetText(editBoxHandle, tostring(abs), false)
	if new ~= height then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction(element, "Element_AddFont", {gCurrentMenu, face, new, weight, italics}, "Element_AddFont", {gCurrentMenu, face, height, weight, italics}, false, index, index)
	end
end

function btnFontColorSwatch_OnButtonClicked(buttonHandle)
	MenuOpenModal("ColorPicker.xml")

	local color = BlendColor_GetColor(Element_GetTextureColor(Control_GetDisplayElement(gHandles["btnFontColorSwatch"], "normalDisplay")), ControlStates.DXUT_STATE_NORMAL)

	--Pass up the color data to the colorpicker
	local ev = KEP_EventCreate( COLOR_START_EVENT)
	KEP_EventSetFilter(ev, MENU_EDITOR_COLOR_FILTER)
	KEP_EventEncodeNumber(ev, color.a)
	KEP_EventEncodeNumber(ev, color.r)
	KEP_EventEncodeNumber(ev, color.g)
	KEP_EventEncodeNumber(ev, color.b)
	KEP_EventEncodeNumber(ev, ColorTarget.FONT)
	KEP_EventQueue( ev)
end

function boxWordWrap_OnCheckBoxChanged(chkHandle, checked)
	local element = GetCurrentFontElement()
	if element == nil then
		return
	end

	local textFormat = Element_GetTextFormat(element)
	local wordWrap, valign, halign = decodeTextFormat(textFormat)
	if checked == 1 then
		checked = TEXT_WORDWRAP
	else
		checked = 0
	end
	if checked ~= wordWrap then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction(element, "Element_SetTextFormat", { checked + valign + halign }, "Element_SetTextFormat", { wordWrap + valign + halign }, false, index, index )
	end
end

function cmbFontAlignV_OnComboBoxSelectionChanged(comboBoxHandle, index, text)
	local element = GetCurrentFontElement()
	if element == nil then
		return
	end

	local textFormat = Element_GetTextFormat(element)
	local wordWrap, valign, halign = decodeTextFormat(textFormat)
	local new = ComboBox_GetSelectedData(comboBoxHandle)
	if new ~= valign then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction(element, "Element_SetTextFormat", { wordWrap + new + halign }, "Element_SetTextFormat", { wordWrap + valign + halign }, false, index, index )
	end
end

function cmbFontAlignH_OnComboBoxSelectionChanged(comboBoxHandle, index, text)
	local element = GetCurrentFontElement()
	if element == nil then
		return
	end

	local textFormat = Element_GetTextFormat(element)
	local wordWrap, valign, halign = decodeTextFormat(textFormat)
	local new = ComboBox_GetSelectedData(comboBoxHandle)
	if new ~= halign then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction(element, "Element_SetTextFormat", { wordWrap + valign + new }, "Element_SetTextFormat", { wordWrap + valign + halign }, false, index, index )
	end
end

---------------------------------------------------------------------
-- File Menu
---------------------------------------------------------------------

g_FileMenuStruct = {}
g_FileMenuContents =
{
	"imgFileMenu_BG",   -- First name listed must be the background
	"btnFileMenu_New",
	"btnFileMenu_Open",
	"btnFileMenu_Reload",
	"btnFileMenu_Save",
	"btnFileMenu_Exit",
}
g_FileMenuItems =
{
	"btnFileMenu_New",
	"btnFileMenu_Open",
	"btnFileMenu_Reload",
	"btnFileMenu_Save",
	"btnFileMenu_Exit",
}

function btnMenuFile_OnButtonClicked(buttonHandle)
	PopupMenuHelper_ShowPopupMenu(g_FileMenuStruct, true)
end

function btnFileMenu_New_OnRadioButtonChanged(buttonHandle)
	if gCurrentMenu then
		KEP_YesNoBox( CLOSE_MSG, "Confirm", WF.MENUEDITOR_NEW_FILTER)
	else
		MenuCreateCurrent()
	end

	PopupMenuHelper_ShowPopupMenu(g_FileMenuStruct, false)
end

function btnFileMenu_Open_OnRadioButtonChanged(buttonHandle)
	if gCurrentMenu then
		KEP_YesNoBox( CLOSE_MSG, "Confirm", WF.MENUEDITOR_BROWSE_FILTER)
	else
		MenuCloseThis()
		BrowseMenus()
	end

	PopupMenuHelper_ShowPopupMenu(g_FileMenuStruct, false)
end

function btnFileMenu_Reload_OnRadioButtonChanged(buttonHandle)
	if gCurrentMenu then
		KEP_YesNoBox( CLOSE_MSG, "Confirm", WF.MENUEDITOR_RELOAD_FILTER)
	end

	PopupMenuHelper_ShowPopupMenu(g_FileMenuStruct, false)
end

function btnFileMenu_Save_OnRadioButtonChanged(buttonHandle)
	if gCurrentMenu then
		MenuSaveCurrent()
	end

	PopupMenuHelper_ShowPopupMenu(g_FileMenuStruct, false)
end

function btnFileMenu_Exit_OnRadioButtonChanged(buttonHandle)
	if gCurrentMenu then
		KEP_YesNoBox( CLOSE_MSG, "Confirm", WF.MENUEDITOR_EXIT_FILTER)
	end

	PopupMenuHelper_ShowPopupMenu(g_FileMenuStruct, false)
end

---------------------------------------------------------------------
-- Edit Menu
---------------------------------------------------------------------

g_EditMenuStruct = {}
g_EditMenuContents =
{
	"imgEditMenu_BG",   -- First name listed must be the background
	"btnEditMenu_Undo",
	"btnEditMenu_Redo",
}
g_EditMenuItems =
{
	"btnEditMenu_Undo",
	"btnEditMenu_Redo",
}

function btnMenuEdit_OnButtonClicked(buttonHandle)
	PopupMenuHelper_ShowPopupMenu(g_EditMenuStruct, true)
end

function btnEditMenu_Undo_OnRadioButtonChanged(buttonHandle)
	undoAction()
	PopupMenuHelper_ShowPopupMenu(g_EditMenuStruct, false)
end

function btnEditMenu_Redo_OnRadioButtonChanged(buttonHandle)
	redoAction()
	PopupMenuHelper_ShowPopupMenu(g_EditMenuStruct, false)
end

---------------------------------------------------------------------
-- Add Menu
---------------------------------------------------------------------

g_AddMenuStruct = {}
g_AddMenuContents =
{
	"imgAddMenu_BG",   -- First name listed must be the background
	"btnAddMenu_Static",
	"btnAddMenu_Image",
	"btnAddMenu_Button",
	"btnAddMenu_CheckBox",
	"btnAddMenu_ComboBox",
	"btnAddMenu_ListBox",
	"btnAddMenu_EditBox",
	"btnAddMenu_RadioButton",
	"btnAddMenu_Slider",
}
g_AddMenuItems =
{
	"btnAddMenu_Static",
	"btnAddMenu_Image",
	"btnAddMenu_Button",
	"btnAddMenu_CheckBox",
	"btnAddMenu_ComboBox",
	"btnAddMenu_ListBox",
	"btnAddMenu_EditBox",
	"btnAddMenu_RadioButton",
	"btnAddMenu_Slider",
}

function btnMenuAdd_OnButtonClicked( buttonHandle )
	PopupMenuHelper_ShowPopupMenu(g_AddMenuStruct, true)
end

function btnAddMenu_Static_OnRadioButtonChanged(buttonHandle)
	AddNewControl(ControlTypes.DXUT_CONTROL_STATIC)
	PopupMenuHelper_ShowPopupMenu(g_AddMenuStruct, false)
end

function btnAddMenu_Image_OnRadioButtonChanged(buttonHandle)
	AddNewControl(ControlTypes.DXUT_CONTROL_IMAGE)
	PopupMenuHelper_ShowPopupMenu(g_AddMenuStruct, false)
end

function btnAddMenu_Button_OnRadioButtonChanged(buttonHandle)
	AddNewControl(ControlTypes.DXUT_CONTROL_BUTTON)
	PopupMenuHelper_ShowPopupMenu(g_AddMenuStruct, false)
end

function btnAddMenu_CheckBox_OnRadioButtonChanged(buttonHandle)
	AddNewControl(ControlTypes.DXUT_CONTROL_CHECKBOX)
	PopupMenuHelper_ShowPopupMenu(g_AddMenuStruct, false)
end

function btnAddMenu_ComboBox_OnRadioButtonChanged(buttonHandle)
	AddNewControl(ControlTypes.DXUT_CONTROL_COMBOBOX)
	PopupMenuHelper_ShowPopupMenu(g_AddMenuStruct, false)
end

function btnAddMenu_ListBox_OnRadioButtonChanged(buttonHandle)
	AddNewControl(ControlTypes.DXUT_CONTROL_LISTBOX)
	PopupMenuHelper_ShowPopupMenu(g_AddMenuStruct, false)
end

function btnAddMenu_EditBox_OnRadioButtonChanged(buttonHandle)
	AddNewControl(ControlTypes.DXUT_CONTROL_SIMPLEEDITBOX)
	PopupMenuHelper_ShowPopupMenu(g_AddMenuStruct, false)
end

function btnAddMenu_RadioButton_OnRadioButtonChanged(buttonHandle)
	AddNewControl(ControlTypes.DXUT_CONTROL_RADIOBUTTON)
	PopupMenuHelper_ShowPopupMenu(g_AddMenuStruct, false)
end

function btnAddMenu_Slider_OnRadioButtonChanged(buttonHandle)
	AddNewControl(ControlTypes.DXUT_CONTROL_SLIDER)
	PopupMenuHelper_ShowPopupMenu(g_AddMenuStruct, false)
end

---------------------------------------------------------------------
-- Keyboard shortcuts
---------------------------------------------------------------------

function Dialog_OnKeyDown(dialogHandle, key, shift, control, alt)
	if control then
		if key == Keyboard.U then
			undoAction()
		elseif key == Keyboard.Y then
			redoAction()
		elseif key == Keyboard.C then
			copyControls()
		elseif key == Keyboard.V then
			pasteControls()
		elseif key == Keyboard.S then
			if gCurrentMenu then
				MenuSaveCurrent()
			end
		end
	elseif key == Keyboard.DEL then
		if Control_GetFocus(glstControls) == 1 then
			deleteControl()
		end
	elseif key == Keyboard.ESC then
		-- close this menu
		if gCurrentMenu then
			KEP_YesNoBox(CLOSE_MSG, "Confirm", WF.MENUEDITOR_EXIT_FILTER)
		else
			MenuCloseThis()
		end
	end
end

---------------------------------------------------------------------
-- Helper Functions
---------------------------------------------------------------------

gControlTypeNames = {}
gControlTypeNames[ControlTypes.DXUT_CONTROL_BUTTON]      = "Button"
gControlTypeNames[ControlTypes.DXUT_CONTROL_STATIC]      = "Static"
gControlTypeNames[ControlTypes.DXUT_CONTROL_CHECKBOX]    = "CheckBox"
gControlTypeNames[ControlTypes.DXUT_CONTROL_RADIOBUTTON] = "RadioButton"
gControlTypeNames[ControlTypes.DXUT_CONTROL_COMBOBOX]    = "ComboBox"
gControlTypeNames[ControlTypes.DXUT_CONTROL_SLIDER]      = "Slider"
gControlTypeNames[ControlTypes.DXUT_CONTROL_EDITBOX]     = "EditBox"
gControlTypeNames[ControlTypes.DXUT_CONTROL_SIMPLEEDITBOX]= "EditBox"
gControlTypeNames[ControlTypes.DXUT_CONTROL_LISTBOX]     = "ListBox"
gControlTypeNames[ControlTypes.DXUT_CONTROL_SCROLLBAR]   = "ScrollBar"
gControlTypeNames[ControlTypes.DXUT_CONTROL_IMAGE]       = "Image"
gControlTypeNames[ControlTypes.DXUT_CONTROL_COMMENT]     = "Comment"

-- Convert a handle into a string describing the control type
function GetControlTypeName(control)
	local type="None"
	if control == nil then
		type = "None"
	elseif control == gCurrentMenu then
		type = "Dialog"
	else
		type = gControlTypeNames[Control_GetType(control)] or "Unknown"
	end
	return type
end

function GetCurrentFontElement()
	local element = nil

	-- Get the current element selected in cmbElement
	local elementTagIndex = ComboBox_GetSelectedData(gHandles["cmbElement"])
	if elementTagIndex == nil then
		return element
	end

	-- if it doesn't require a font, return early.
	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	local elementTag = gElementTags[currentControlTypeName][ elementTagIndex ]

	-- Get the current state selected in cmbState
	-- if it doesn't require a font, return early
	local state = ComboBox_GetSelectedData(gHandles["cmbState"])
	if (state == nil) or
	(currentControlTypeName == "Dialog" and Dialog_GetElementTagUsesFontState(gCurrentControl, elementTag, state) == 0) or
	(currentControlTypeName ~= "Dialog" and Control_GetElementTagUsesFontState(gCurrentControl, elementTag, state) == 0) then
		return element
	end

	if currentControlTypeName == "Dialog" then
		element = Dialog_GetDisplayElement(gCurrentControl, elementTag)
	else
		element = Control_GetDisplayElement(gCurrentControl, elementTag)
	end
	return element
end

function GetCurrentTextureElement()
	local element = nil

	-- Get the current element selected in cmbElement
	local elementTagIndex = ComboBox_GetSelectedData(gHandles["cmbElement"])
	if elementTagIndex == nil then
		return element
	end

	local currentControlTypeName = GetControlTypeName(gCurrentControl)
	local elementTag = gElementTags[currentControlTypeName][ elementTagIndex ]

	-- Get the current state selected in cmbState
	-- if it doesn't require a texture, return early
	local state = ComboBox_GetSelectedData(gHandles["cmbState"])
	if (state == nil) or
	(currentControlTypeName == "Dialog" and Dialog_GetElementTagUsesTextureState(gCurrentControl, elementTag, state) == 0) or
	(currentControlTypeName ~= "Dialog" and Control_GetElementTagUsesTextureState(gCurrentControl, elementTag, state) == 0) then
		return element
	end

	if currentControlTypeName == "Dialog" then
		element = Dialog_GetDisplayElement(gCurrentControl, elementTag)
	else
		element = Control_GetDisplayElement(gCurrentControl, elementTag)
	end
	return element
end

gControlOffsets = {}
function StoreControlOffset( handle, parentHandle )
	if handle ~= nil and parentHandle ~= nil then
		local offsetX = Control_GetLocationX(handle) - Control_GetLocationX(parentHandle)
		gControlOffsets[handle] = {parentHandle=parentHandle, offsetX=offsetX }
	end
end

function ShowControlAtY( handle, y )
	Control_SetVisible(handle, true)
	Control_SetLocationY(handle, y)
	local offsets = gControlOffsets[handle]
	if offsets ~= nil and offsets.parentHandle ~= nil then
		Control_SetLocationX(handle, offsets.offsetX + Control_GetLocationX(offsets.parentHandle))
	end
end

function SetAllButtonColors( buttonHandle, color )
	if (color == nil) then return end -- drf - lua error fix
	BlendColor_SetColor(Element_GetTextureColor(Control_GetDisplayElement(buttonHandle,   "normalDisplay")), ControlStates.DXUT_STATE_NORMAL,   color)
	BlendColor_SetColor(Element_GetTextureColor(Control_GetDisplayElement(buttonHandle, "disabledDisplay")), ControlStates.DXUT_STATE_DISABLED, color)
	BlendColor_SetColor(Element_GetTextureColor(Control_GetDisplayElement(buttonHandle,  "focusedDisplay")), ControlStates.DXUT_STATE_FOCUS,    color)
	BlendColor_SetColor(Element_GetTextureColor(Control_GetDisplayElement(buttonHandle, "mouseOverDisplay")), ControlStates.DXUT_STATE_MOUSEOVER, color)
	BlendColor_SetColor(Element_GetTextureColor(Control_GetDisplayElement(buttonHandle,  "pressedDisplay")), ControlStates.DXUT_STATE_PRESSED,  color)
end

function findListBoxItem( listbox, itemname )
	local size = ListBox_GetSize(listbox)
	for i=0,size do
		if itemname == ListBox_GetItemText(listbox, i) then
			return i
		end
	end
	return -1
end

function decodeDialogFormat( lFormat )
	local valign, halign

	lFormat = lFormat - IMDL_BOTTOM
	if lFormat < 0 then
		lFormat = lFormat + IMDL_BOTTOM
	else
		valign = IMDL_BOTTOM
	end

	if not valign then
		lFormat = lFormat - IMDL_VCENTER
		if lFormat < 0 then
			lFormat = lFormat + IMDL_VCENTER
		else
			valign = IMDL_VCENTER
		end
	end

	lFormat = lFormat - IMDL_RIGHT
	if lFormat < 0 then
		lFormat = lFormat + IMDL_RIGHT
	else
		halign = IMDL_RIGHT
	end

	if not halign then
		lFormat = lFormat - IMDL_CENTER
		if lFormat < 0 then
			lFormat = lFormat + IMDL_CENTER
		else
			halign = IMDL_CENTER
		end
	end

	if not halign then
		lFormat = lFormat - IMDL_LEFT
		if lFormat < 0 then
			lFormat = lFormat + IMDL_LEFT
			halign = IMDL_NONE
		else
			halign = IMDL_LEFT
		end
	end

	if not valign then
		lFormat = lFormat - IMDL_TOP
		if lFormat < 0 then
			valign = IMDL_NONE
		else
			valign = IMDL_TOP
		end
	end

	return valign, halign
end

function decodeTextFormat( lFormat )
	local wordwrap, valign, halign

	lFormat = lFormat - TEXT_WORDWRAP
	if lFormat < 0 then
		lFormat = lFormat + TEXT_WORDWRAP
		wordwrap = 0
	else
		wordwrap = TEXT_WORDWRAP
	end

	lFormat = lFormat - TEXT_VALIGN_BOTTOM
	if lFormat < 0 then
		lFormat = lFormat + TEXT_VALIGN_BOTTOM
	else
		valign = TEXT_VALIGN_BOTTOM
	end

	if not valign then
		lFormat = lFormat - TEXT_VALIGN_CENTER
		if lFormat < 0 then
			lFormat = lFormat + TEXT_VALIGN_CENTER
			valign = TEXT_VALIGN_TOP
		else
			valign = TEXT_VALIGN_CENTER
		end
	end

	lFormat = lFormat - TEXT_HALIGN_RIGHT
	if lFormat < 0 then
		lFormat = lFormat + TEXT_HALIGN_RIGHT
	else
		halign = TEXT_HALIGN_RIGHT
	end

	if not halign then
		lFormat = lFormat - TEXT_HALIGN_CENTER
		if lFormat < 0 then
			halign = TEXT_HALIGN_LEFT
		else
			halign = TEXT_HALIGN_CENTER
		end
	end

	return wordwrap, valign, halign
end

function MenuCenterCurrent()
	--Center Dialogs for editing that will be auto-aligned anyway
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	local width = Dialog_GetWidth(gCurrentMenu)
	local height = Dialog_GetHeight(gCurrentMenu)
	local y = Dialog_GetLocationY(gCurrentMenu)
	local x = Dialog_GetLocationX(gCurrentMenu)
	local lFormat = Dialog_GetLocationFormat(gCurrentMenu)
	local valign, halign = decodeDialogFormat(lFormat)
	if valign ~= IMDL_NONE then
		y = screenHeight/2 - height/2
	end
	if halign ~= IMDL_NONE then
		x = screenWidth/2 - width/2
	end
	Dialog_SetLocation(gCurrentMenu, x, y)
end


gNamePrefix = {}
gNamePrefix[ControlTypes.DXUT_CONTROL_BUTTON] = "btn"
gNamePrefix[ControlTypes.DXUT_CONTROL_STATIC] = "stc"
gNamePrefix[ControlTypes.DXUT_CONTROL_CHECKBOX] = "box"
gNamePrefix[ControlTypes.DXUT_CONTROL_RADIOBUTTON] = "radio"
gNamePrefix[ControlTypes.DXUT_CONTROL_COMBOBOX] = "cmb"
gNamePrefix[ControlTypes.DXUT_CONTROL_SLIDER] = "sldr"
gNamePrefix[ControlTypes.DXUT_CONTROL_EDITBOX] = "ed"
gNamePrefix[ControlTypes.DXUT_CONTROL_LISTBOX] = "lst"
gNamePrefix[ControlTypes.DXUT_CONTROL_SCROLLBAR] = "bar"
gNamePrefix[ControlTypes.DXUT_CONTROL_IMAGE] = "img"

--Defaults
defaultButtonW = 50
defaultButtonH = 50
defaultStaticW = 100
defaultStaticH = 25
defaultCheckboxW = 25
defaultCheckboxH = 25
defaultRadioW = 25
defaultRadioH = 25
defaultComboW = 50
defaultComboH = 25
defaultSliderW = 150
defaultSliderH = 15
defaultEditW = 50
defaultEditH = 25
defaultListBoxW = 75
defaultListBoxH = 100
defaultScrollBarW = 75
defaultScrollBarH = 100
defaultImgW = 50
defaultImgH = 50
defaultCaptionH = 18

function AddNewControl(controlType)
	-- Figure out a good name, one that's not taken yet
	local nameBase = (gNamePrefix[controlType] or "").."Temp"
	local name = nameBase
	if Dialog_GetControl(gCurrentMenu, nameBase) ~= nil then
		local count = 0
		while true do
			count = count + 1
			name = nameBase..tostring(count)
			if Dialog_GetControl(gCurrentMenu, name) == nil then
				break
			end
		end
	end

	local x = 0
	local y = 0
	local tempControl = nil
	if (controlType == ControlTypes.DXUT_CONTROL_BUTTON) then
		tempControl = Dialog_AddButton( gCurrentMenu, name, "temp", x, y, defaultButtonW, defaultButtonH )
	elseif (controlType == ControlTypes.DXUT_CONTROL_STATIC) then
		tempControl = Dialog_AddStatic( gCurrentMenu, name, "temp", x, y, defaultStaticW, defaultStaticH )
		local elementTag = gElementTags[GetControlTypeName(tempControl)][1]
		local displayElement = Control_GetDisplayElement(tempControl, elementTag )

		--default background color is white, so default text color is black
		local color = {a = 255, r = 0, g = 0, b = 0}
		for i,v in ipairs(gElementStateNames) do
			if Control_GetElementTagUsesFontState(tempControl, elementTag, v.state) ~= 0 then
				BlendColor_SetColor(Element_GetFontColor(displayElement), v.state, color)
			end
		end

		--turn on wordwrap by default
		local textformat = TEXT_HALIGN_LEFT + TEXT_VALIGN_TOP + TEXT_WORDWRAP
		Element_SetTextFormat(displayElement, textformat)
	elseif (controlType == ControlTypes.DXUT_CONTROL_CHECKBOX) then
		tempControl = Dialog_AddCheckBox( gCurrentMenu, name, "", x, y, defaultCheckboxW, defaultCheckboxH, true )
	elseif (controlType == ControlTypes.DXUT_CONTROL_RADIOBUTTON) then
		tempControl = Dialog_AddRadioButton( gCurrentMenu, name, 0, "", x, y, defaultRadioW, defaultRadioH, true )
	elseif (controlType == ControlTypes.DXUT_CONTROL_COMBOBOX) then
		tempControl = Dialog_AddComboBox( gCurrentMenu, name, x, y, defaultComboW, defaultComboH )
	elseif (controlType == ControlTypes.DXUT_CONTROL_SLIDER) then
		tempControl = Dialog_AddSlider( gCurrentMenu, name, x, y, defaultSliderW, defaultSliderH )
	elseif (controlType == ControlTypes.DXUT_CONTROL_EDITBOX) then
		tempControl = Dialog_AddEditBox( gCurrentMenu, name, "temp", x, y, defaultEditW, defaultEditH )
	elseif (controlType == ControlTypes.DXUT_CONTROL_SIMPLEEDITBOX) then
		tempControl = Dialog_AddSimpleEditBox( gCurrentMenu, name, "temp", x, y, defaultEditW, defaultEditH )
	elseif (controlType == ControlTypes.DXUT_CONTROL_LISTBOX) then
		tempControl = Dialog_AddListBox( gCurrentMenu, name, x, y, defaultListBoxW, defaultListBoxH )
		--elseif (controlType == ControlTypes.DXUT_CONTROL_SCROLLBAR) then
		--	tempControl = Dialog_AddScrollBar( gCurrentMenu, name, x, y, defaultScrollBarW, defaultScrollBarH ) -- TODO: Not implemented
	elseif (controlType == ControlTypes.DXUT_CONTROL_IMAGE) then
		tempControl = Dialog_AddImage( gCurrentMenu, name, x, y, "defaultImg.tga", 0, 0, 32, 32 )
	end

	--repopulate the list of controls in this menu
	--TODO: Consider moving the newly created control to just below the
	--current selection instead of way down at the bottom of the list.
	ResetControlList()
	ListBox_SelectItem(glstControls, ListBox_GetSize(glstControls) - 1)
end

function LoadControlFromXMLToIndex(dialogHandle, xml, index)
	local controlHandle = Dialog_LoadControlByXml(dialogHandle, xml)
	Dialog_MoveControlToIndex(dialogHandle, controlHandle, index)
end

function deleteControl()
	local name = ListBox_GetSelectedItemText(glstControls)
	local index = ListBox_GetSelectedItemIndex(glstControls)
	doAction( gCurrentMenu, "Dialog_RemoveControl", {name}, "LoadControlFromXMLToIndex", { Dialog_GetControlXml( gCurrentMenu, gCurrentControl ), index-1 }, true, index-1, index )
end

function copyControls()
	Dialog_CopyControl( gCurrentMenu, gCurrentControl )
	Control_SetEnabled(gHandles["btnPaste"], Dialog_IsSomethingInClipboard())
end

function pasteControls()
	Dialog_PasteControl( gCurrentMenu )
	--TODO: Consider moving the newly created control to just below the
	--current selection instead of way down at the bottom of the list.
	ResetControlList()
	ListBox_SelectItem(glstControls, ListBox_GetSize(glstControls) - 1)
end

---------------------------------------------------------------------
-- Callbacks from MenuHelpers
---------------------------------------------------------------------

function Dialog_OnSelected(dialogHandle)
	local selectedName = ListBox_GetSelectedItemText(glstControls)
	if selectedName ~= gMenuName then
		ListBox_SelectItem(glstControls, 0)
	end
end

function OnControlSelected(controlHandle)
	local controlName = Control_GetName(controlHandle)
	local selectedName = ListBox_GetSelectedItemText(glstControls)
	if controlName ~= selectedName then
		ListBox_SelectItem(glstControls, findListBoxItem(glstControls, controlName) )
	end
end

function Dialog_OnMoved( dialogHandle )
	if dialogHandle ~= gCurrentMenu then
		return
	end
	if dialogHandle ~= gCurrentControl then
		-- I hope this isn't possible, since when you click on the
		-- dialog to move it, it should be selected in the Controls list.
		-- If it does happen, I'd rather we just silently return.
		return
	end

	local currentX = eval(EditBox_GetText( gHandles["edPositionX"] ))
	local currentY = eval(EditBox_GetText( gHandles["edPositionY"] ))
	local newX = Dialog_GetLocationX( gCurrentMenu )
	local newY = Dialog_GetLocationY( gCurrentMenu )
	if (newX ~= currentX) or (newY ~= currentY) then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction( gCurrentMenu, "Dialog_SetLocation", { newX, newY }, "Dialog_SetLocation", { currentX, currentY }, false, 0, index)
	end
end

function Dialog_OnResized( dialogHandle )
	if dialogHandle ~= gCurrentMenu then
		return
	end
	if dialogHandle ~= gCurrentControl then
		-- I hope this isn't possible, since when you click on the
		-- dialog's resize handle, it should be selected in the Controls list.
		-- If it does happen, I'd rather we just silently return.
		return
	end

	local currentW = eval(EditBox_GetText( gHandles["edSizeW"] ))
	local currentH = eval(EditBox_GetText( gHandles["edSizeH"] ))
	local newW = Dialog_GetWidth( gCurrentMenu )
	local newH = Dialog_GetHeight( gCurrentMenu )
	if (newW ~= currentW) or (newH ~= currentH) then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		doAction( gCurrentMenu, "Dialog_SetSize", { newW, newH }, "Dialog_SetSize", { currentW, currentH }, false, 0, index)
	end
end

function OnControlMoved( controlHandle, x, y )
	if controlHandle ~= gCurrentControl then
		-- I hope this isn't possible, since when you click on the
		-- control to move it, it should be selected in the Controls list.
		-- If it does happen, I'd rather we just silently return.
		return
	end

	local currentX = eval(EditBox_GetText( gHandles["edPositionX"] ))
	local currentY = eval(EditBox_GetText( gHandles["edPositionY"] ))
	local newX = Control_GetLocationX( controlHandle )
	local newY = Control_GetLocationY( controlHandle )
	if (newX ~= currentX) or (newY ~= currentY) then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		local controlIndex = findListBoxItem(glstControls, Control_GetName(controlHandle))
		doAction( controlHandle, "Control_SetLocation", { newX, newY }, "Control_SetLocation", { currentX, currentY }, false, controlIndex, index)
	end
end

function OnControlResized( controlHandle, x, y )
	if controlHandle ~= gCurrentControl then
		-- I hope this isn't possible, since when you click on the
		-- control's resize handle, it should be selected in the Controls list.
		-- If it does happen, I'd rather we just silently return.
		return
	end

	local currentW = eval(EditBox_GetText( gHandles["edSizeW"] ))
	local currentH = eval(EditBox_GetText( gHandles["edSizeH"] ))
	local newW = Control_GetWidth( controlHandle )
	local newH = Control_GetHeight( controlHandle )
	if (newW ~= currentW) or (newH ~= currentH) then
		local index = ListBox_GetSelectedItemIndex(glstControls)
		local controlIndex = findListBoxItem(glstControls, Control_GetName(controlHandle))
		doAction( controlHandle, "Control_SetSize", { newW, newH }, "Control_SetSize", { currentW, currentH }, false, controlIndex, index)
	end
end

---------------------------------------------------------------------
-- Undo/Redo
---------------------------------------------------------------------
gActions = {}
gRedoActions = {}
--TODO: Make sure to clear these every time gCurrentMenu changes

Action = {}
Action.__index = Action

function Action.create( control, callStr, callParams, undoStr, undoParams, resetControlList, selectIndex, undoSelectIndex )
	local action = {}
	setmetatable(action,Action)
	action.controlHandle = control
	action.call = callStr
	action.params = callParams
	action.undoCall = undoStr
	action.undoParams = undoParams
	action.resetControlList = resetControlList
	action.selectControlIndex = selectIndex
	action.undoSelectControlIndex = undoSelectIndex

	return action
end

-- NOTE: If control is a control or gCurrentMenu, you can omit selectIndex and undoSelectIndex and probably get the result you want.
-- If you're manipulating the order of controls, or modifying a non-control, you should probably pass selectIndex and undoSelectIndex.
function doAction( control, callStr, callParams, undoStr, undoParams, resetControlList, selectIndex, undoSelectIndex )

	local action = Action.create( control, callStr, callParams, undoStr, undoParams, resetControlList, selectIndex, undoSelectIndex )
	table.insert(gRedoActions, action)
	local result = redoAction()

	gRedoActions = {}

	return result
end

function undoAction()
	local result = nil

	local len = #gActions
	if (len == 0) then
		return result
	end
	local action = gActions[len]

	local numParams = #action.undoParams
	if (numParams == 0) then
		result = _G[action.undoCall](action.controlHandle)
	elseif (numParams == 1) then
		result = _G[action.undoCall](action.controlHandle, action.undoParams[1])
	elseif (numParams == 2 ) then
		result = _G[action.undoCall](action.controlHandle, action.undoParams[1], action.undoParams[2])
	elseif (numParams == 3 ) then
		result = _G[action.undoCall](action.controlHandle, action.undoParams[1], action.undoParams[2] ,action.undoParams[3])
	elseif (numParams == 4 ) then
		result = _G[action.undoCall](action.controlHandle, action.undoParams[1], action.undoParams[2], action.undoParams[3], action.undoParams[4])
	elseif (numParams == 5 ) then
		result = _G[action.undoCall](action.controlHandle, action.undoParams[1], action.undoParams[2], action.undoParams[3], action.undoParams[4], action.undoParams[5])
	else
		LogError("undoAction: Need to add some more parameters to support "..tostring(action.undoCall))
	end

	table.insert(gRedoActions, action)
	table.remove(gActions, len)

	if action.resetControlList then
		ResetControlList()
	end

	if action.undoSelectControlIndex ~= nil then
		ListBox_SelectItem(glstControls, action.undoSelectControlIndex)
	else
		-- select the modified control by name
		local name
		if action.controlHandle == gCurrentMenu then
			name = gMenuName
		else
			name = Control_GetName(action.controlHandle)
		end
		ListBox_SelectItem(glstControls, findListBoxItem(glstControls, name) )

	end

	return result
end

function redoAction()
	local result = nil

	local len = #gRedoActions
	if (len == 0) then
		return result
	end
	local action = gRedoActions[len]

	local numParams = #action.params
	if (numParams == 0) then
		result = _G[action.call](action.controlHandle)
	elseif (numParams == 1) then
		result = _G[action.call](action.controlHandle, action.params[1])
	elseif (numParams == 2 ) then
		result = _G[action.call](action.controlHandle, action.params[1], action.params[2])
	elseif (numParams == 3 ) then
		result = _G[action.call](action.controlHandle, action.params[1], action.params[2] ,action.params[3])
	elseif (numParams == 4 ) then
		result = _G[action.call](action.controlHandle, action.params[1], action.params[2], action.params[3], action.params[4])
	elseif (numParams == 5 ) then
		result = _G[action.call](action.controlHandle, action.params[1], action.params[2], action.params[3], action.params[4], action.params[5])
	else
		LogError("redoAction: Need to add some more parameters to support "..tostring(action.call))
	end

	table.insert(gActions, action)
	table.remove(gRedoActions, len)

	if action.resetControlList then
		ResetControlList()
	end

	if action.selectControlIndex ~= nil then
		ListBox_SelectItem(glstControls, action.selectControlIndex)
	else
		-- select the modified control by name
		local name
		if action.controlHandle == gCurrentMenu then
			name = gMenuName
		else
			name = Control_GetName(action.controlHandle)
		end
		ListBox_SelectItem(glstControls, findListBoxItem(glstControls, name) )
	end

	return result
end

