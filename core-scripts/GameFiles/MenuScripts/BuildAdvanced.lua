--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

WIDGET_ALIGN_EVENT = "WidgetAlignEvent"
ENABLE_GRID_EVENT = "EnableGridEvent"
BUILD_SETTINGS_EVENT = "BuildSettingsEvent"

g_changedPosRot = false
g_openBuildHelp = false
g_showXYZCoordinates = false
g_showUndoRedo = false
g_showSounds = false
g_showMediaTriggers = false

m_frameworkEnabled = false
g_advbuildset = 0

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "returnFrameworkStateHandler",		"FrameworkState",					KEP.MED_PRIO )
end 
	
	
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	-- Is the Framework active in this world? Let's find out
	local event = KEP_EventCreate("GetFrameworkState")
	KEP_EventQueue(event)
	
	getConfig()
	CheckBox_SetChecked( gHandles["cbShowMovementHelper"], g_openBuildHelp )
	CheckBox_SetChecked( gHandles["cbShowXYZCoordinates"], g_showXYZCoordinates )
	CheckBox_SetChecked( gHandles["cbShowUndoRedo"], g_showUndoRedo)
	CheckBox_SetChecked( gHandles["cbSoundTrigger"], g_showSounds )
	CheckBox_SetChecked( gHandles["cbMediaTrigger"], g_showMediaTriggers )
	
	setAdvancedBuildButton()		
end

function setAdvancedBuildButton()
	--is Advanced Settings checked? 
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		local advBuildOK, advBuildON =  KEP_ConfigGetNumber( config, "AdvBuild", g_advbuildset)
		g_advbuildset = advBuildON
		if g_advbuildset == 0 then 
			CheckBox_SetChecked(gHandles["chkAdvBuild"], false)
		else 
			CheckBox_SetChecked(gHandles["chkAdvBuild"], true)
		end
	end 
end 

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

-- Called when a player changes Framework mode or when the Framework changes states (enabled/disabled)
function returnFrameworkStateHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local frameworkState = cjson.decode(Event_DecodeString(event))
	if frameworkState.enabled ~= nil then
		m_frameworkEnabled = (frameworkState.enabled == true)
		
		-- set to advanced build if in a framework world as an owner
		local config = KEP_ConfigOpenWOK()
		if m_frameworkEnabled and g_advbuildset == 0 and KEP_IsOwnerOrModerator() then 
			if config ~= nil and config ~= 0 then
				CheckBox_SetChecked(gHandles["chkAdvBuild"], true) 
				KEP_ConfigSetNumber( config, "AdvBuild", 1 )
				KEP_ConfigSave( config )
				g_advbuildset = 1
			end
		end
	end
end

function chkAdvBuild_OnCheckBoxChanged( checkboxHandle, checked )
	--Is Advanced Build Checked? 
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		KEP_ConfigSetNumber( config, "AdvBuild", checked )
		KEP_ConfigSave( config )
	end
	
	g_advbuildset = checked

	local ev = KEP_EventCreate( BUILD_SETTINGS_EVENT)
	KEP_EventSetFilter(ev, BUILD_SETTINGS_ADVANCED_BUILD)
	KEP_EventEncodeNumber(ev, checked)
	KEP_EventQueue( ev)
end


function cbShowMovementHelper_OnCheckBoxChanged( checkboxHandle, checked )
	g_openBuildHelp = (checked == 1)
	if g_openBuildHelp then
		MenuOpen("BuildHelp.xml")
	else
		MenuClose("BuildHelp.xml")
	end

	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetString( config, "BuildHelpOpen", tostring(g_openBuildHelp) )
		KEP_ConfigSave( config )
	end
end

function cbAdvancedMovement_OnCheckBoxChanged( checkboxHandle, checked )
	local ev = KEP_EventCreate(BUILD_SETTINGS_EVENT)	
	KEP_EventSetFilter(ev, BUILD_SETTINGS_SELECTION_ADVANCED_MOVEMENT)
	KEP_EventEncodeNumber(ev, checked)
	KEP_EventQueue( ev)
end

function cbPlaceAtAvatar_OnCheckBoxChanged( checkboxHandle, checked )
	local ev = KEP_EventCreate(BUILD_SETTINGS_EVENT)	
	KEP_EventSetFilter(ev, BUILD_SETTINGS_DROP_DISTANCE)
	if checked == 1 then
		KEP_EventEncodeNumber(ev, 0)
	else
		KEP_EventEncodeNumber(ev, 10)
	end
	KEP_EventQueue( ev)
end

function cbShowWidget_OnCheckBoxChanged( checkboxHandle, checked )
	local ev = KEP_EventCreate(BUILD_SETTINGS_EVENT)	
	KEP_EventSetFilter(ev, BUILD_SETTINGS_SELECTION_SHOW_WIDGET)
	KEP_EventEncodeNumber(ev, checked)
	KEP_EventQueue( ev)
end

function UpdateSnapping()

	--Get Snapping Settings (Selection.lua validates and enforces all setting values)
	local rotation = tonumber(EditBox_GetText(Dialog_GetControl(gDialogHandle, "edGridRotation"))) or 0
	local spacing = tonumber(EditBox_GetText(Dialog_GetControl(gDialogHandle, "edGridSpacing"))) or 0
	local distance = tonumber(EditBox_GetText(Dialog_GetControl(gDialogHandle, "edGridSnapDistance"))) or 0
	local snapGrid = CheckBox_GetChecked(Dialog_GetCheckBox(gDialogHandle, "cbGridSnapping"))
	local snapRot = CheckBox_GetChecked(Dialog_GetCheckBox(gDialogHandle, "cbRotSnapping"))
	local snapObject = CheckBox_GetChecked(Dialog_GetCheckBox(gDialogHandle, "cbObjectSnapping"))

	-- Send EnableGridEvent
	Log("UpdateSnapping: snapGrid="..tostring(snapGrid==1).." snapRot="..tostring(snapRot==1).." snapObject="..tostring(snapObject==1).." rotation="..rotation.." spacing="..spacing.." distance="..distance)
	ev = KEP_EventCreate( ENABLE_GRID_EVENT)
	KEP_EventEncodeNumber(ev, snapGrid)
	KEP_EventEncodeNumber(ev, snapRot)
	KEP_EventEncodeNumber(ev, snapObject)
	KEP_EventEncodeNumber(ev, spacing)
	KEP_EventEncodeNumber(ev, rotation)
	KEP_EventEncodeNumber(ev, distance)
	KEP_EventQueue(ev)
end

function cbGridSnapping_OnCheckBoxChanged( cbHandle, checked )
	Log("Snapping Grid Clicked")
	UpdateSnapping()
end

function cbRotSnapping_OnCheckBoxChanged( cbHandle, checked )
	Log("Snapping Rot Clicked")
	UpdateSnapping()
end

function cbObjectSnapping_OnCheckBoxChanged( cbHandle, checked )
	Log("Snapping Object Clicked")
	UpdateSnapping()
end

function rbObjectSpace_OnRadioButtonChanged( radioButtonHandle )
	changeAxisAlignment(0)
end

function rbWorldSpace_OnRadioButtonChanged( radioButtonHandle )
	changeAxisAlignment(2)
end

function cbCameraCollision_OnCheckBoxChanged( cbHandle, checked )
	changeCameraCollision(checked)
end

function cbSelectionGlow_OnCheckBoxChanged( cbHandle, checked )
	local ev = KEP_EventCreate( BUILD_SETTINGS_EVENT)
	KEP_EventSetFilter(ev, BUILD_SETTINGS_SELECTION_GLOW)
	KEP_EventEncodeNumber(ev, checked)
	KEP_EventQueue( ev)
end

function cbShowXYZCoordinates_OnCheckBoxChanged( cbHandle, checked )
	g_showXYZCoordinates = (checked == 1)

	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetString( config, "BuildModeShowXYZCoordinates", tostring(g_showXYZCoordinates) )
		KEP_ConfigSave( config )
	end

	local ev = KEP_EventCreate( BUILD_SETTINGS_EVENT)
	KEP_EventSetFilter(ev, BUILD_SETTINGS_SELECTION_SHOW_XYZ)
	KEP_EventEncodeNumber(ev, checked)
	KEP_EventQueue( ev)
end

function cbShowUndoRedo_OnCheckBoxChanged( cbHandle, checked )
	g_showUndoRedo = (checked == 1)

	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetString( config, "BuildModeShowUndoRedo", tostring(g_showUndoRedo) )
		KEP_ConfigSave( config )
	end

	local ev = KEP_EventCreate( BUILD_SETTINGS_EVENT)
	KEP_EventSetFilter(ev, BUILD_SETTINGS_SELECTION_SHOW_UNDO_REDO)
	KEP_EventEncodeNumber(ev, checked)
	KEP_EventQueue( ev)
end

function cbSoundTrigger_OnCheckBoxChanged( cbHandle, checked )
	g_showSounds = (checked == 1)

	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetString( config, "BuildModeShowSoundTriggers", tostring(g_showSounds) )
		KEP_ConfigSave( config )
	end

	local ev = KEP_EventCreate( BUILD_SETTINGS_EVENT)
	KEP_EventSetFilter(ev, BUILD_SETTINGS_SELECTION_SHOW_SOUNDS)
	KEP_EventEncodeNumber(ev, checked)
	KEP_EventQueue( ev)
end

function cbMediaTrigger_OnCheckBoxChanged( cbHandle, checked )
	g_showMediaTriggers = (checked == 1)

	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetString( config, "BuildModeShowMediaTriggers", tostring(g_showMediaTriggers) )
		KEP_ConfigSave( config )
	end

	local ev = KEP_EventCreate( BUILD_SETTINGS_EVENT)
	KEP_EventSetFilter(ev, BUILD_SETTINGS_SELECTION_SHOW_MEDIA)
	KEP_EventEncodeNumber(ev, checked)
	KEP_EventQueue( ev)
end

function edGridSpacing_OnEditBoxString(editBoxHandle, password)
	Log("Snapping Spacing Changed")
	UpdateSnapping()
end

function edGridSpacing_OnFocusOut(editBoxHandle)
	Log("Snapping Spacing FocusOut")
	UpdateSnapping()
end

function edGridRotation_OnEditBoxString(editBoxHandle, password)
	Log("Snapping Rotation Changed")
	UpdateSnapping()
end

function edGridRotation_OnFocusOut(editBoxHandle)
	Log("Snapping Rotation FocusOut")
	UpdateSnapping()
end

function edGridSnapDistance_OnEditBoxString(editBoxHandle, password)
	Log("Snapping Distance Changed")
	UpdateSnapping()
end

function edGridSnapDistance_OnFocusOut(editBoxHandle)
	Log("Snapping Distance FocusOut")
	UpdateSnapping()
end

function changeAxisAlignment(axis)
	ev = KEP_EventCreate( WIDGET_ALIGN_EVENT)
	KEP_EventEncodeNumber(ev, axis )
	KEP_EventQueue( ev)
end

function changeCameraCollision(value)
	local ev = KEP_EventCreate( BUILD_SETTINGS_EVENT)
	KEP_EventSetFilter(ev, BUILD_SETTINGS_CAMERA_COLLISION)
	KEP_EventEncodeNumber(ev, value)
	KEP_EventQueue( ev)
end

function getConfig()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		local ok, buildHelp = KEP_ConfigGetString( config, "BuildHelpOpen", "false" )
		local xyzOk, showXYZ = KEP_ConfigGetString( config, "BuildModeShowXYZCoordinates", "false")
		local undoBool = "true"
		local undoRedoOk, showUndoRedo = KEP_ConfigGetString( config, "BuildModeShowUndoRedo", undoBool)
		local _, showSounds = KEP_ConfigGetString( config, "BuildModeShowSoundTriggers", "false")
		local _, showMediaTriggers = KEP_ConfigGetString( config, "BuildModeShowMediaTriggers", "false")
		if buildHelp == "true" then
			g_openBuildHelp = true
		else
			g_openBuildHelp = false
		end
		g_showXYZCoordinates = (showXYZ == "true")
		g_showUndoRedo = (showUndoRedo == "true")
		g_showSounds = (showSounds == "true")
		g_showMediaTriggers = (showMediaTriggers == "true")
	end
end
