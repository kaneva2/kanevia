--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("PageButtons.lua")
dofile("MenuLocation.lua")

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnConfirm_OnButtonClicked( buttonHandle )
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .. "mykaneva/invitefriend.aspx" )
	MenuCloseThis()
end
