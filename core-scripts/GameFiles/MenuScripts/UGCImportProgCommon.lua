--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")
dofile("..\\Scripts\\UGCImportState.lua")

gDropId = -1
gImportInfo = nil
gTargetInfo = nil
gBtnState = 0
gKeyState = 0
gImportCancelledMsgOnBottom = false		-- set to true if you want "Import Cancelled" message to stay under any other messages

STATE_NONE = 0
STATE_ANALYZING = 1
--STATE_VALID_CONTINUE = 2
--STATE_ERROR_CANCEL = 3
--STATE_WARN_CONTINUE = 4
STATE_IMPORTING = 5

gDialogState = STATE_NONE
lblStatusTitle = nil
lblProgress = nil

gShowProgress = false
gProgressTimer = 0
gImpSessionId = -1

----------------------------------
-- abstract functions
----------------------------------
-- 1. Operations
-- ...
-- 2. Events
UGCI.RegisterEventHandlers=nil
UGCI.RegisterEvents=nil
-- 3. Import progress handlers
UGCI.OnCreateDialog=nil
UGCI.OnDestroyDialog=nil
UGCI.OnPreviewing=nil
UGCI.OnPreviewed=nil
UGCI.OnImporting=nil
UGCI.OnImported=nil
UGCI.OnConverting=nil
UGCI.OnConverted=nil
UGCI.OnProcessing=nil
UGCI.OnProcessed=nil
UGCI.OnPreviewFailed=nil
UGCI.OnImportFailed=nil
UGCI.OnProcessingFailed=nil
UGCI.OnCancelled=nil

function UGCTypeDesc()
	if gUgcTypeDesc~=nil then
		return gUgcTypeDesc
	else
		return ""
	end
end

function UGCImportMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	gDropId = UGCI.GetDialogInstanceId( gDialogHandle )
	local dlgId = Dialog_GetId( gDialogHandle )

	local eventData = UGCI.ParseUGCImportMenuEvent( event )
	gImportInfo = eventData.importInfo
	gTargetInfo = eventData.targetInfo
	gBtnState = eventData.btnState
	gKeyState = eventData.keyState

	if filter~=dlgId or objectid~=gDropId then
		-- Ignore event with mismatched IDs
		return
	end

	Log( "UGCImportMenuEventHandler: dlgId=" .. dlgId .. " dropId=" .. gDropId .. " type=" .. gImportInfo.type .. " path=" .. gImportInfo.fullPath )

	local sessionId = StartImportAsync()
	if sessionId~=nil then
		gImpSessionId = sessionId
		gImportInfo.sessionId = sessionId
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "UGCImportMenuEventHandler", UGC_IMPORT_MENU_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UGCImportProgressEventHandler", UGC_IMPORT_PROGRESS_EVENT, KEP.HIGH_PRIO )
	if UGCI.RegisterEventHandlers~=nil then
		UGCI.RegisterEventHandlers(dispatcher, handler, debugLevel)
	end
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( UGC_IMPORT_PROGRESS_EVENT, KEP.MED_PRIO )
	if UGCI.RegisterEvents~=nil then
		UGCI.RegisterEvents(dispatcher, handler, debugLevel)
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	lblStatusTitle = Dialog_GetStatic( gDialogHandle, "lblStatusTitle" )
	lblProgress = Dialog_GetStatic( gDialogHandle, "lblProgress" )

	if UGCI.OnCreateDialog~=nil then
		UGCI.OnCreateDialog()
	end
end

function Dialog_OnDestroy(dialogHandle)
	if UGCI.OnDestroyDialog~=nil then
		UGCI.OnDestroyDialog()
	end
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnRender( dialogHandle, nElapsed )
	gProgressTimer = gProgressTimer + nElapsed
	if gShowProgress and gProgressTimer>0.5 then
		gProgressTimer = 0
		local progText = Static_GetText( lblProgress )
		if string.len(progText) > 20 then
			progText = "."
		else
			progText = progText .. " ."
		end
		Static_SetText( lblProgress, progText )
	end
end

-- Fired when import progress reported by Observer
function UGCImportProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	if objectid~=gImpSessionId then
		-- skip if the event is for a different import session
		return
	end

	local ImpSessionId = -1
	local ImpSession = nil

	if filter==UGCIS.DESTROYING or filter==UGCIS.DESTROYED then
		-- ImpSession might has been destroyed at this time, there is nothing we can do about this event.
		return
	end

	if objectid~=nil then
		ImpSessionId = objectid
		ImpSession = UGC_GetImportSession( ImpSessionId )
	end

	if filter==UGCIS.PREVIEWING then
		if UGCI.OnPreviewing~=nil then
			UGCI.OnPreviewing( ImpSession, ImpSessionId )
		end
	elseif filter==UGCIS.PREVIEWED then
		if UGCI.OnPreviewed~=nil then
			UGCI.OnPreviewed( ImpSession, ImpSessionId )
		end
	elseif filter==UGCIS.IMPORTING then
		if UGCI.OnImporting~=nil then
			UGCI.OnImporting( ImpSession, ImpSessionId )
		end
	elseif filter==UGCIS.IMPORTED then
		if UGCI.OnImported~=nil then
			UGCI.OnImported( ImpSession, ImpSessionId )
		end
	elseif filter==UGCIS.CONVERTING then
		if UGCI.OnConverting~=nil then
			UGCI.OnConverting( ImpSession, ImpSessionId )
		end
	elseif filter==UGCIS.CONVERTED then
		if UGCI.OnConverted~=nil then
			UGCI.OnConverted( ImpSession, ImpSessionId )
		end
	elseif filter==UGCIS.PROCESSING then
		if UGCI.OnProcessing~=nil then
			UGCI.OnProcessing( ImpSession, ImpSessionId )
		end
	elseif filter==UGCIS.PROCESSED then
		if UGCI.OnProcessed~=nil then
			UGCI.OnProcessed( ImpSession, ImpSessionId )
		end
	elseif filter==UGCIS.PREVIEWFAILED then
		LogError("UGCImportProgressEventHandler: Preview Failed - path=" .. gImportInfo.fullPath )
		local errMsg = nil
		if UGCI.OnPreviewFailed~=nil then
			errMsg = UGCI.OnPreviewFailed( ImpSession, ImpSessionId )
		end
		if errMsg==nil then
			errMsg = "[code: 1]"
		end
		UGCI.ImportFailed(ImpSessionId, "Error importing " .. UGCTypeDesc() .. ".\n\n" .. errMsg )
	elseif filter==UGCIS.IMPORTFAILED then
		LogError("UGCImportProgressEventHandler: Import Failed - path=" .. gImportInfo.fullPath )
		local errMsg = nil
		if UGCI.OnImportFailed~=nil then
			errMsg = UGCI.OnImportFailed( ImpSession, ImpSessionId )
		end
		if errMsg==nil then
			errMsg = "[code: 5]"
		end
		UGCI.ImportFailed(ImpSessionId, "Error importing " .. UGCTypeDesc() .. ".\n\n" .. errMsg )
	elseif filter==UGCIS.PROCESSINGFAILED then
		LogError("UGCImportProgressEventHandler: Post-processing Failed - path=" .. gImportInfo.fullPath )
		local errMsg = nil
		if UGCI.OnProcessingFailed~=nil then
			errMsg = UGCI.OnProcessingFailed( ImpSession, ImpSessionId )
		end
		if errMsg==nil then
			errMsg = "[code: 4]"
		end
		UGCI.ImportFailed( ImpSessionId, "Error importing " .. UGCTypeDesc() .. ".\n\n" .. errMsg )
	elseif filter==UGCIS.CANCELED then
		Log( "UGCImportProgressEventHandler: Import Cancelled - path=" .. gImportInfo.fullPath )
		DestroyMenu( gDialogHandle )
		local ret, dh = KEP_MessageBox( "Import cancelled.", "Information" )
		if dh~=nil and gImportCancelledMsgOnBottom then
			-- Display "Import cancelled" message under any other existing error messages (be sure to center all of them)
			Dialog_SendToBack( dh )
		end
		if UGCI.OnCancelled~=nil then
			UGCI.OnCancelled( ImpSession, ImpSessionId )
		end
		UGC_CleanupImportSessionAsync( ImpSessionId )
	end
end

function changeDialogState( newState )
	local oldState = gDialogState

	if newState == STATE_ANALYZING then
		-- analyzing...
		showProgress( true )
		Static_SetText( lblStatusTitle, "Analyzing file, please wait..." )
	elseif newState == STATE_IMPORTING then
		-- importing
		showProgress( true )
		Static_SetText( lblStatusTitle, "Import in progress, please wait... " )
	end

	gDialogState = newState
end

function showProgress( show )
	Control_SetVisible( Static_GetControl( lblProgress ), show )
	gShowProgress = show
end

function resetProgress()
	showProgress( false )
	Static_SetText( lblProgress, "" )
end

-- Asynchronous import
function StartImportAsync()

	local progressEventId = KEP_EventGetId( UGC_IMPORT_PROGRESS_EVENT )
	local ImpSession = nil
	local ImpSessionId = -1

	Log("StartImportAsync: '"..gImportInfo.fullPath.."'")

	ImpSessionId, ImpSession = UGC_CreateImportSession( gImportInfo.type )

	if ImpSession==nil or ImpSessionId==-1 then
		LogError( "StartImportAsync: unable to create import session" )
		UGCI.ImportFailed( ImpSessionId, "Error importing " .. UGCTypeDesc() .. ". Please restart Kaneva client and try again." )
		return
	end

	GetSet_SetString( ImpSession, gImportStateGetSetIds.ROOTURI, gImportInfo.fullPath )
	GetSet_SetNumber( ImpSession, gImportStateGetSetIds.STATE, UGCIS.NONE )
	GetSet_SetNumber( ImpSession, gImportStateGetSetIds.EVENTID, progressEventId )
	GetSet_SetNumber( ImpSession, gImportStateGetSetIds.EVENTOBJID, ImpSessionId )
	GetSet_SetNumber( ImpSession, gImportStateGetSetIds.TARGETTYPE, gTargetInfo.type )
	GetSet_SetNumber( ImpSession, gImportStateGetSetIds.TARGETID, gTargetInfo.id )
	GetSet_SetNumber( ImpSession, gImportStateGetSetIds.TARGETPOS_X, gTargetInfo.pos.x )
	GetSet_SetNumber( ImpSession, gImportStateGetSetIds.TARGETPOS_Y, gTargetInfo.pos.y )
	GetSet_SetNumber( ImpSession, gImportStateGetSetIds.TARGETPOS_Z, gTargetInfo.pos.z )
	if gImportStateGetSetIds.USEIMPORTDLG~=nil then
		if gKeyState==KeyStates.SHIFT then
			GetSet_SetNumber( ImpSession, gImportStateGetSetIds.USEIMPORTDLG, 0 )
		else
			GetSet_SetNumber( ImpSession, gImportStateGetSetIds.USEIMPORTDLG, 1 )
		end
	end

	Machine = UGC_StartImportAsync( ImpSessionId )

	return ImpSessionId
end
