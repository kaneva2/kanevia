--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")
dofile("..\\Scripts\\UGCImportState.lua")
dofile("..\\Scripts\\UGCImportState.lua")
dofile("..\\Scripts\\DynamicObjectCreateIds.lua")
dofile("..\\Scripts\\DynamicVisCreateIds.lua")
dofile("..\\Scripts\\LightCreateIds.lua")
dofile("..\\Scripts\\StreamableDynamicIds.lua")

DYNAMIC_OBJECT_IMPORT_PROGRESS_EVENT = "DynamicObjectImportProgressEvent"
UGC_IMPORT_CONFIRMATION_MENU_EVENT = "UGCImportConfirmationMenuEvent"
UGC_IMPORT_CONFIRMATION_RESULT_EVENT = "UGCImportConfirmationResultEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"

gDropId = -1
gImportInfo = nil
gTargetInfo = nil
gBtnState = 0
gKeyState = 0

STATE_NONE = 0
STATE_ANALYZING = 1
--STATE_VALID_CONTINUE = 2
--STATE_ERROR_CANCEL = 3
--STATE_WARN_CONTINUE = 4
STATE_IMPORTING = 5

gDialogState = STATE_NONE
lblStatusTitle = nil
lblProgress = nil

gShowProgress = false
gProgressTimer = 0
gImpSessionId = -1
gOrigBoundBox = nil

function UGCImportMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	gDropId = UGCI.GetDialogInstanceId( gDialogHandle )
	local dlgId = Dialog_GetId( gDialogHandle )

	if filter~=dlgId or objectid~=gDropId then
		-- Ignore event with mismatched IDs
		return
	end

	local eventData = UGCI.ParseUGCImportMenuEvent( event )
	gImportInfo = eventData.importInfo
	gTargetInfo = eventData.targetInfo
	gBtnState = eventData.btnState
	gKeyState = eventData.keyState

	Log( "UGCImportMenuEventHandler: dlgId=" .. dlgId .. " dropId=" .. gDropId .. " type=" .. gImportInfo.type .. " path=" .. gImportInfo.fullPath )

	if gImportInfo.type==UGC_TYPE.DYNOBJ then

		local progressEventId = KEP_EventGetId( DYNAMIC_OBJECT_IMPORT_PROGRESS_EVENT )
		local sessionId = ImportDynamicObjectAsync( gImportInfo, gTargetInfo, gBtnState, gKeyState, progressEventId )
		if sessionId~=nil then
			gImpSessionId = sessionId
			gImportInfo.sessionId = sessionId
		end
	else
		LogError( "UGCImportMenuEventHandler: Don't know how to handle import type=" .. gImportInfo.type )
		MenuCloseThis()
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "UGCImportMenuEventHandler", UGC_IMPORT_MENU_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "DynamicObjectImportProgressEventHandler", DYNAMIC_OBJECT_IMPORT_PROGRESS_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "YesNoBoxHandler", YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "UGCImportConfirmationHandler", UGC_IMPORT_CONFIRMATION_RESULT_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "MeshImportOptionsMenuResultEventHandler", "MeshImportOptionsMenuResultEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( DYNAMIC_OBJECT_IMPORT_PROGRESS_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( UGC_IMPORT_CONFIRMATION_RESULT_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "MeshImportOptionsMenuEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "MeshImportOptionsMenuResultEvent", KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	lblStatusTitle = Dialog_GetStatic( gDialogHandle, "lblStatusTitle" )
	lblProgress = Dialog_GetStatic( gDialogHandle, "lblProgress" )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnRender( dialogHandle, nElapsed )
	gProgressTimer = gProgressTimer + nElapsed
	if gShowProgress and gProgressTimer>0.5 then
		gProgressTimer = 0
		local progText = Static_GetText( lblProgress )
		if string.len(progText) > 20 then
			progText = "."
		else
			progText = progText .. " ."
		end
		Static_SetText( lblProgress, progText )
	end
end

function ImportDynamicObjectAsync( importInfo, target, btnState, keyState, progressEventId )
	local bNewImport = false
	local ImpSession = nil
	local ImpSessionId = -1

	ImpSessionId, ImpSession = UGC_CreateImportSession( UGC_TYPE.DYNOBJ )
	if ImpSession==nil or ImpSessionId == -1 then
		LogError( "ImportDynamicObjectAsync: Unable to create import session" )
		KEP_MessageBox( "Error importing 3D model. Please restart Kaneva client and try again.", "Import failed" )
		return
	end

	Log("ImportDynamicObjectAsync:"
		.." impSession="..tostring(ImpSession)
		.." impSessionId="..ImpSessionId
		.." targetType="..target.type
		.." targetId="..target.id
		.." '" .. importInfo.fullPath .. "'"
		.." pos=["..target.pos.x.." "..target.pos.y.." "..target.pos.z.."]" 
	)

	bNewImport = true

	local s, e = 0, 0
	local path, name = "", ""

	s, e, path, name = string.find( importInfo.fullPath, "^(.-[\\/]?)([^\\/]+)$" )

	if name=="" then
		name = string.sub(importInfo.fullPath, -10)	-- extract last 10 characters if parsing failed
	end

	GetSet_SetString( ImpSession, DynamicObjectCreateIds.OBJECTNAME, name )
	GetSet_SetString( ImpSession, DynamicObjectCreateIds.ROOTURI, importInfo.fullPath )
	GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.STATE, UGCIS.NONE )
	GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.EVENTID, progressEventId )
	GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.EVENTOBJID, ImpSessionId )
	GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.TARGETTYPE, target.type )
	GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.TARGETID, target.id )
	GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_X, target.pos.x )
	GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_Y, target.pos.y )
	GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_Z, target.pos.z )
	if keyState==KeyStates.SHIFT then
		GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.USEIMPORTDLG, 0 )
	else
		GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.USEIMPORTDLG, 1 )
	end

	Machine = UGC_StartImportAsync( ImpSessionId )
end

-- Fired when import progress reported by Observer
function DynamicObjectImportProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local ImpSessionId = -1
	local ImpSession = nil
	local fullPath = ""
	if filter==UGCIS.DESTROYING or filter==UGCIS.DESTROYED then
		-- ImpSession might has been destroyed at this time, there is nothing we can do about this event.
		return
	end

	if objectid~=nil then
		ImpSessionId = objectid
		ImpSession = UGC_GetImportSession( ImpSessionId )
		if ImpSession~=nil then
			fullPath = GetSet_GetString( ImpSession, DynamicObjectCreateIds.ROOTURI )
		end
	end

	if filter==UGCIS.PREVIEWING then
		changeDialogState( STATE_ANALYZING )
		Dialog_SetMinimized( gDialogHandle, 0 )
	elseif filter==UGCIS.IMPORTING or filter==UGCIS.CONVERTING or filter==UGCIS.PROCESSING then
		changeDialogState( STATE_IMPORTING )
		Dialog_SetMinimized( gDialogHandle, 0 )
	elseif filter==UGCIS.PREVIEWED or filter==UGCIS.IMPORTED then
		-- tell state machine to continue import process
		UGC_ContinueImportAsync( ImpSessionId )
	elseif filter==UGCIS.CONVERTED then

		-- obtain original boundbox size (after initial conversion)
		if gOrigBoundBox==nil then
			gOrigBoundBox = {}
			gOrigBoundBox.minX, gOrigBoundBox.minY, gOrigBoundBox.minZ = -1, -1, -1
			gOrigBoundBox.maxX, gOrigBoundBox.maxY, gOrigBoundBox.maxZ = 1, 1, 1
			if ImpSession~=nil then
				gOrigBoundBox.minX, gOrigBoundBox.minY, gOrigBoundBox.minZ = GetSet_GetVector( ImpSession, DynamicObjectCreateIds.BOUNDBOXMIN )
				gOrigBoundBox.maxX, gOrigBoundBox.maxY, gOrigBoundBox.maxZ = GetSet_GetVector( ImpSession, DynamicObjectCreateIds.BOUNDBOXMAX )
			end
		end

		local ignoreMissingTexture = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.IGNOREMISSINGTEXTURES )

		if ignoreMissingTexture==0 then
			local missingTextureCount = UGC_GetMissingTextureCount( ImpSessionId )
			if missingTextureCount > 0 then
				-- hide progress menu
				Dialog_SetMinimized( gDialogHandle, 1 )

				-- display missing texture confirmation
                local dh = MenuOpenModal("UGCImportMissingTextures.xml")
                if dh == nil then
                    LogError("DynamicObjectImportProgressEventHandler: Failed Loading UGCImportMissingTextures.xml")
                    return
                end

				-- pass ImpSessionId to confirmation menu
				local e = KEP_EventCreate( UGC_IMPORT_CONFIRMATION_MENU_EVENT )
				KEP_EventSetFilter( e, UIC.MISSING_TEXTURE )
				KEP_EventSetObjectId( e, ImpSessionId )
				KEP_EventQueue( e )
				return
			end
		end

		local useImportDlg
		useImportDlg = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.USEIMPORTDLG )

		if useImportDlg~=0 then

			-- hide progress menu
			Dialog_SetMinimized( gDialogHandle, 1 )
			-- display a scaling dialog
			ShowMeshImportOptionsDialog( UGC_TYPE.DYNOBJ, ImpSessionId )
			-- disable useimportdlg flag so we won't scale again (unless validation failed)
			GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.USEIMPORTDLG, 0 )
		else
			-- otherwise, tell state machine to continue import process
			UGC_ContinueImportAsync( ImpSessionId )
		end
	elseif filter==UGCIS.PROCESSED then
		local internalError = true
		if ImpSession~=nil then
			local importedObj
			importedObj = GetSet_GetObject( ImpSession, DynamicObjectCreateIds.IMPORTEDOBJECTGETSET )

			local targetType, targetId, targetPosX, targetPosY, targetPosZ
			targetType = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.TARGETTYPE )
			targetId = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.TARGETID )
			targetPosX = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_X )
			targetPosY = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_Y )
			targetPosZ = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_Z )

			if importedObj~=nil then
				local localGLID = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.LOCALGLID )
				if localGLID~=0 then
					internalError = false
					local numMeshes = GetSet_GetArrayCount( ImpSession, DynamicObjectCreateIds.GSMESHES )
					local numTextures  = GetSet_GetArrayCount( ImpSession, DynamicObjectCreateIds.GSTEXTURES )
					local scaleFactor = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.SCALEFACTOR )

					Log("DynamicObjectImportProgressEventHandler: Import Complete")
					Log("... posX=" .. targetPosX .. " posY=" .. targetPosY .. " posZ=" .. targetPosZ .. " scale=" .. scaleFactor)

					local placementId
					if targetType==ObjectType.UNKNOWN then
						-- no target info available, place at character position
						placementId = KEP_PlaceLocalDynamicObj(localGLID, 1.0, 0, 0 )
					else
						placementId = KEP_PlaceLocalDynamicObj(localGLID, 1.0, 1, 0, targetPosX, targetPosY, targetPosZ )
					end

					-- Automatically load dynamic object menu
					local s, e, importFileName
					s, e, importFileName = string.find( fullPath, ".*[\\/:]([^\\/:]+)" )
					if importFileName == nil then
						importFileName = fullPath
					end
					SelectLocalDynamicObject(placementId, importFileName, "" )

					-- To display this infobox on top of DO menu, we have to do it asynchronously otherwise DO menu will be obstructing it.
					-- This infobox is triggered thru event system. This event will be processed after SelectDynamicObject event, hence allowing infobox displayed over DO menu.
					-- UGCI.ShowGenericInfoBox( "Import completed.\nMeshes: " .. numMeshes .. "\nTextures: " .. numTextures )
					UGC_CleanupImportSessionAsync( ImpSessionId )
				end
			end
		end

		if internalError then
			LogError("Import Failed - Internal Error")
			KEP_MessageBox( "Import failed due to an internal error.\n\n[code:5]", "Import failed" )
		end

		MenuCloseThis()

	elseif filter==UGCIS.PREVIEWFAILED or filter==UGCIS.IMPORTFAILED or filter==UGCIS.CONVERSIONFAILED or filter==UGCIS.PROCESSINGFAILED then

		local failAtStage = ""
		if filter==UGCIS.PREVIEWFAILED then
			failAtStage = "Preview"
		elseif filter==UGCIS.IMPORTFAILED then
			failAtStage = "Import"
		elseif filter==UGCIS.CONVERSIONFAILED then
			failAtStage = "Conversion"
		elseif filter==UGCIS.PROCESSINGFAILED then
			failAtStage = "Post-processing"
		end

		local composedErrorMsg = "Import failed due to following reasons:\n"

		-- display errors
		if ImpSession~=nil then

			LogError( "DynamicObjectImportProgressEventHandler: Import Failed During "..failAtStage .. " path=" .. fullPath )

			local isRecoverable = true
			local requiresResizing = false

			local errorCount = UGC_GetErrorCount( ImpSessionId )
			local resizeMsg = "Your mesh design cannot be processed because its dimension is beyond range. "

			if errorCount > 0 then

				for vId=0,errorCount-1 do
					local desc, action = UGC_GetErrorInfo( ImpSessionId, vId )
					LogError( "DynamicObjectImportProgressEventHandler: "..desc .. ". Suggested action: " .. action )
					composedErrorMsg = composedErrorMsg .. "\n" .. desc

					if action==RecoveryAction.NoRecovery then
						isRecoverable = false
					elseif action==RecoveryAction.Resize then
						requiresResizing = true
						resizeMsg = resizeMsg .. "\n" .. desc
					end

				end

				if isRecoverable then
					Log( "DynamicObjectImportProgressEventHandler: Import errors are recoverable" )
					if requiresResizing then
						Log( "DynamicObjectImportProgressEventHandler: 3D model resizing is suggested" )
					end
				else
					LogError( "DynamicObjectImportProgressEventHandler: Import errors are NOT recoverable" )
				end

				if isRecoverable and requiresResizing then
					-- show resizing confirmation
					KEP_YesNoBox( resizeMsg .. "\n\nDo you want to resize it?", "Confirm", WF.IMPORT_MESH_RESIZING, ImpSessionId, false, 450, 220, true, false )
					return
				end

			else
				LogError( "DynamicObjectImportProgressEventHandler: Import failed due to unspecified reason." )
				composedErrorMsg = composedErrorMsg .. "\n" .. "Unknown error [" .. UGCIE.UNSPECIFIED_ERROR .. "] occurred at " .. failAtStage
			end
		else
			LogError( "DynamicObjectImportProgressEventHandler: Import failed due to fatal error: import session not found." )
			composedErrorMsg = composedErrorMsg .. "\n" .. "Unknown error [" .. UGCIE.SESSION_NOT_FOUND .. "] occurred at " .. failAtStage
		end

		UGCI.ImportFailed( ImpSessionId, composedErrorMsg, 350, 250 )

	elseif filter==UGCIS.CANCELED then

		UGC_CleanupImportSessionAsync( ImpSessionId )

		KEP_MessageBox( "Import cancelled.", "Information" )

		Log( "DynamicObjectImportProgressEventHandler: Import Cancelled - path=" .. fullPath )
		MenuCloseThis()
	end
end

function changeDialogState( newState )
	local oldState = gDialogState

	if newState == STATE_ANALYZING then
		-- analyzing...
		showProgress( true )
		Static_SetText( lblStatusTitle, "Analyzing file, please wait..." )

	elseif newState == STATE_IMPORTING then
		-- importing
		showProgress( true )
		Static_SetText( lblStatusTitle, "Import in progress, please wait... " )
	end

	gDialogState = newState
end

function showProgress( show )
	Control_SetVisible( Static_GetControl( lblProgress ), show )
	gShowProgress = show
end

function resetProgress()
	showProgress( false )
	Static_SetText( lblProgress, "" )
end

function YesNoBoxHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = KEP_EventDecodeNumber( event )	-- 1: Yes, 0 = No
	if filter==WF.IMPORT_MESH_RESIZING then
		ImpSessionId = objectid
		if answer==1 then
			-- hide progress menu
			Dialog_SetMinimized( gDialogHandle, 1 )
			-- display a scaling dialog
			ShowMeshImportOptionsDialog( UGC_TYPE.DYNOBJ, ImpSessionId )
		else
			UGCI.ImportFailed( ImpSessionId, "Unable to import 3D model: dimension beyond limit." )
		end
		return KEP.EPR_CONSUMED
	end
end

function UGCImportConfirmationHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local answer = KEP_EventDecodeNumber( event )
	local ImpSessionId = objectid
	local ImpSession = UGC_GetImportSession( ImpSessionId )
	if answer~=0 then

		-- ignore missing texture error
		GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.IGNOREMISSINGTEXTURES, 1 )

		-- confirm import
		local useImportDlg
		useImportDlg = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.USEIMPORTDLG )
		if useImportDlg~=0 then
			-- display a scaling dialog
			ShowMeshImportOptionsDialog( UGC_TYPE.DYNOBJ, ImpSessionId )
			-- disable useimportdlg flag so we won't scale again (unless validation failed)
			GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.USEIMPORTDLG, 0 )
		else
			-- otherwise, tell state machine to continue import process
			UGC_ContinueImportAsync( ImpSessionId )
		end
	else
		-- cancel import
		UGC_CancelImportAsync( ImpSessionId )
	end
end

function ShowMeshImportOptionsDialog( ugcType, createStructId )
	--local minX, minY, minZ, maxX, maxY, maxZ
	local targetType, targetId, targetX, targetY, targetZ
	local collisionType
	local t_names = {}

	if ugcType==UGC_TYPE.DYNOBJ then

		local ImpSessionId = createStructId
		local ImpSession = UGC_GetImportSession( ImpSessionId )
		if ImpSession==nil then
			-- session not found
			LogError( "ShowMeshImportOptionsDialog: session not found: " .. ImpSessionId .. ", type: " .. ugcType )
		end
		Log("ShowMeshImportOptionsDialog: impSession="..tostring(ImpSession).." impSessionId="..ImpSessionId)

		local importedObj = GetSet_GetObject( ImpSession, DynamicObjectCreateIds.IMPORTEDOBJECTGETSET )

		if importedObj==nil then
			-- mesh not imported
			LogError( "ShowMeshImportOptionsDialog: not imported: " .. ImpSessionId .. ", type: " .. ugcType )
		end

		targetType = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.TARGETTYPE )
		targetId = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.TARGETID ) -- drf - bug fix was TARGETTYPE
		targetX = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_X )
		targetY = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_Y )
		targetZ = GetSet_GetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_Z )
		local meshCount = GetSet_GetArrayCount(ImpSession, DynamicObjectCreateIds.GSMESHES)
		for i = 1, meshCount do
			local mesh = GetSet_GetObjectInArray(ImpSession, DynamicObjectCreateIds.GSMESHES, i - 1)
			t_names[i] = GetSet_GetString(mesh, DynamicVisCreateIds.VISNAME)
		end
		Log("ShowMeshImportOptionsDialog: targetType="..targetType.." targetId="..targetId)
	else
		-- other types, not supported for now
		LogError( "ShowMeshImportOptionsDialog: unsupported UGC type: " .. ugcType )
		return
	end

	g_dlgMeshScaling = MenuOpen("MeshImportOptions.xml")

	-- fire event to pass parameters to the menu script
	menuOpenEvent = KEP_EventCreate( "MeshImportOptionsMenuEvent" )

	KEP_EventEncodeNumber( menuOpenEvent, ugcType )
	KEP_EventEncodeNumber( menuOpenEvent, createStructId )
	KEP_EventEncodeNumber( menuOpenEvent, gOrigBoundBox.minX )
	KEP_EventEncodeNumber( menuOpenEvent, gOrigBoundBox.minY )
	KEP_EventEncodeNumber( menuOpenEvent, gOrigBoundBox.minZ )
	KEP_EventEncodeNumber( menuOpenEvent, gOrigBoundBox.maxX )
	KEP_EventEncodeNumber( menuOpenEvent, gOrigBoundBox.maxY )
	KEP_EventEncodeNumber( menuOpenEvent, gOrigBoundBox.maxZ )
	KEP_EventEncodeNumber( menuOpenEvent, targetType )
	KEP_EventEncodeNumber( menuOpenEvent, targetId )
	KEP_EventEncodeNumber( menuOpenEvent, targetX )
	KEP_EventEncodeNumber( menuOpenEvent, targetY )
	KEP_EventEncodeNumber( menuOpenEvent, targetZ )
	KEP_EventEncodeNumber( menuOpenEvent, #t_names )
	for i = 1, #t_names do
		KEP_EventEncodeString( menuOpenEvent, t_names[i])
	end
	KEP_EventQueue( menuOpenEvent )
	-- Aynchronous: waiting for "MeshImportOptionsMenuResultEvent"
end

-- Select an local dynamic object (and display DO menu)
function SelectLocalDynamicObject(placementId, name, desc )
	-- Send a message to DynamicObject.lua to trigger DO selection
	local ev = KEP_EventCreate( "DynamicObjectSelectedEvent" )
	KEP_EventEncodeNumber( ev, placementId )
	KEP_EventEncodeNumber( ev, 0 )	-- friendId
	KEP_EventEncodeNumber( ev, 0 ) -- x
	KEP_EventEncodeNumber( ev, 0 ) -- y
	KEP_EventEncodeNumber( ev, 0 ) -- isAttachableObject
	KEP_EventEncodeString( ev, name )
	KEP_EventEncodeNumber( ev, 0 ) -- canPlayMovie
	KEP_EventEncodeNumber( ev, 0 ) -- isInteractive
	KEP_EventEncodeString( ev, desc ) -- description
	KEP_EventEncodeNumber( ev, 1 )	-- rightClick
	KEP_EventEncodeNumber( ev, 0 )	-- leftClick
	KEP_EventEncodeNumber( ev, 1 )	-- isLocal
	KEP_EventEncodeNumber( ev, 0 ) -- isDerivable
	KEP_EventQueue( ev )
end

function MeshImportOptionsMenuResultEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local ugcType = KEP_EventDecodeNumber( event )
	local createStructId = KEP_EventDecodeNumber( event )
	local scaleFactor = KEP_EventDecodeNumber( event )
	local correctAmbient = KEP_EventDecodeNumber( event )
	local targetOverride = KEP_EventDecodeNumber( event )

	local cancelImport = false
	local reconvert = false

	if scaleFactor < 0 then
		-- negative scale factor is used to indicating cancellation
		cancelImport = true
	elseif scaleFactor ~= 1 or correctAmbient ~= 0 then
		-- reconvert if scaling factor or ambient changed
		-- we only display import option menu once so change detection is based on deviations from the default values
		-- same applies to collision /flash / custom texture flag changes
		reconvert = true	
	end

	-- call client engien to revalidate this
	if ugcType==UGC_TYPE.DYNOBJ then

		local ImpSessionId = createStructId
		local ImpSession = UGC_GetImportSession( ImpSessionId )
		GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.AUTOCORRECTAMBIENT, correctAmbient )

		if targetOverride~=0 then
			-- overwrite target position in ImpSession
			local targetType = KEP_EventDecodeNumber( event )
			local targetId = KEP_EventDecodeNumber( event )
			local targetPosX = KEP_EventDecodeNumber( event )
			local targetPosY = KEP_EventDecodeNumber( event )
			local targetPosZ = KEP_EventDecodeNumber( event )
			Log( "MeshImportOptionsMenuResultEventHandler: Override drop target - pos=[" .. targetPosX .. " " .. targetPosY .. " " .. targetPosZ.."]" )

			local numberOfVisuals = KEP_EventDecodeNumber(event)
			Log("numVisuals="..numberOfVisuals)

			for i=1, numberOfVisuals do
				local collision = KEP_EventDecodeNumber(event)
				local playFlash = KEP_EventDecodeNumber(event)
				if (playFlash == 1) then Log("PLAYS FLASH = ".. (i-1)) end
				local customizable = KEP_EventDecodeNumber(event)
				local mesh = GetSet_GetObjectInArray(ImpSession, DynamicObjectCreateIds.GSMESHES, i - 1)
				if collision == 0 then
					-- reconvert if collision flag changed
					reconvert = true
					GetSet_SetNumber(mesh, DynamicVisCreateIds.COLLISIONTYPE, collision)
				end
				if playFlash == 1 then
					-- reconvert if flash flag changed
					reconvert = true
					GetSet_SetNumber(mesh, DynamicVisCreateIds.CANPLAYFLASH, playFlash)
				end
				if customizable == 1 then
					-- reconvert if custom texture flag changed
					reconvert = true
					GetSet_SetNumber(mesh, DynamicVisCreateIds.ISCUSTOMIZABLE, customizable)
				end
			end

			local numberOfLights = KEP_EventDecodeNumber(event)
			for i=1, numberOfLights do
				-- for now all lights except last one in list get overwritten
				local lightObj = GetSet_GetObject(ImpSession, DynamicObjectCreateIds.LIGHT)
				local lightType = KEP_EventDecodeNumber(event)
				local lightPosX = KEP_EventDecodeNumber(event)
				local lightPosY = KEP_EventDecodeNumber(event)
				local lightPosZ = KEP_EventDecodeNumber(event)
				local lightDiffR = KEP_EventDecodeNumber(event)
				local lightDiffG = KEP_EventDecodeNumber(event)
				local lightDiffB = KEP_EventDecodeNumber(event)
				local lightRange = KEP_EventDecodeNumber(event)
				local att0 = KEP_EventDecodeNumber(event)
				local att1 = KEP_EventDecodeNumber(event)
				local att2 = KEP_EventDecodeNumber(event)
				GetSet_SetNumber(lightObj, LightCreateIds.ENABLED, 1)
				GetSet_SetNumber(lightObj, LightCreateIds.POSITION_X, lightPosX)
				GetSet_SetNumber(lightObj, LightCreateIds.POSITION_Y, lightPosY)
				GetSet_SetNumber(lightObj, LightCreateIds.POSITION_Z, lightPosZ)
				GetSet_SetNumber(lightObj, LightCreateIds.COLOR_R, lightDiffR)
				GetSet_SetNumber(lightObj, LightCreateIds.COLOR_G, lightDiffG)
				GetSet_SetNumber(lightObj, LightCreateIds.COLOR_B, lightDiffB)
				GetSet_SetNumber(lightObj, LightCreateIds.RANGE, lightRange)
				GetSet_SetNumber(lightObj, LightCreateIds.ATTENUATION0, att0)
				GetSet_SetNumber(lightObj, LightCreateIds.ATTENUATION1, att1)
				GetSet_SetNumber(lightObj, LightCreateIds.ATTENUATION2, att2)
			end

			GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.TARGETTYPE, targetType )
			GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.TARGETID, targetId )
			GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_X, targetPosX )
			GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_Y, targetPosY )
			GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.TARGETPOS_Z, targetPosZ )
		end

		if cancelImport then
			-- cancel import
			LogInfo( "MeshImportOptionsMenuResultEventHandler: cancel import" )
			UGC_CancelImportAsync( ImpSessionId )
		elseif not reconvert then
			-- proceed with what we have
			GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.SCALEFACTOR, 1 )
			-- this call will be ignored if current state is VALIDATION_FAILED, client engine will directly fire another VALIDATION_FAILED state report in response.
			UGC_ContinueImportAsync( ImpSessionId )
		else
			-- reconvert with new flags
			LogInfo( "MeshImportOptionsMenuResultEventHandler: reconvert" )
			GetSet_SetNumber( ImpSession, DynamicObjectCreateIds.SCALEFACTOR, scaleFactor )
			UGC_ClearErrors( ImpSessionId )
			UGC_ReconvertAsync( ImpSessionId )
		end
	else
		-- other types, not supported for now
		LogError( "MeshImportOptionsMenuResultEventHandler: unsupported UGC type: " .. ugcType )
		return
	end
end
