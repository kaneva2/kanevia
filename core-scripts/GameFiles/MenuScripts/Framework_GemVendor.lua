--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_GemVendor.lua
--
-- Displays available items for vending
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")
dofile("Framework_DragDropHelper.lua")
dofile("Easing.lua")
dofile("HUDHelper.lua")
dofile("Framework_VendorHelper.lua")
dofile("Framework_ActorHelper.lua")
dofile("Framework_MenuShadowHelper.lua")
----------------------
-- Local Constants
----------------------
 ITEMS_PER_PAGE = 10
 local NUM_GEM_TYPES = 5

g_displayTable = {}	-- items being displayed :: [{UNID, properties={name, GLID, itemType}, count}]
g_trades = {} -- output, input, and cost :: [{output={UNID, properties={name, GLID, itemType}, count}, input={UNID, properties={name, GLID, itemType}, count}, cost}]

----------------------
-- Local Variables
----------------------
local m_displaying = false
local m_inputLocations = 	{ 	["txtCost"] = { x = 0, y = 0 },
								["txtCount"] = { x = 0, y = 0 },

								-- ["imgItemBG11"] = { x = 0, y = 0 },
								-- ["imgItem11"] = { x = 0, y = 0 },
								-- ["stcItem11"] = { x = 0, y = 0 },
								-- ["imgItemLevel11"] = { x = 0, y = 0 },
								-- ["btnItem11"] = { x = 0, y = 0 },
								-- ["stcItemCount11"] = { x = 0, y = 0 },
								-- ["imgItemCountBG11"] = { x = 0, y = 0 }
							}

local m_playerName = ""
m_currTrade = 0
local m_dropFlashing = false
local m_dropFlashed = false
local m_dropFlashTime = 0
local m_overButton = false
local m_validTrade = false
m_vendorPID = nil

local m_requestHandle = nil
local m_pendingTrade = false

m_inventory = {}			-- [{UNID, count, name, itemType}]
m_inventoryCounts = {}	-- {UNID:count}
m_gameItemNamesByUNID = {}

local m_gemNames = {"diamond", "ruby", "emerald", "sapphire", "topaz"}
local m_currGems = {topaz = 0, sapphire = 0, emerald = 0, ruby = 0, diamond = 0}
local m_gemValues = {topaz = 0, sapphire = 0, emerald = 0, ruby = 0, diamond = 0}
local m_exchangeRemaining = 0

local g_credits = 0
local g_rewards = 0
local g_userID = 0
local m_worldName

m_page = 1
m_selectedPage = 1


----------------------
-- Local Functions
----------------------

local initalizeClickToTrade -- (numButtons)
local getInputLocations -- ()
local buyItem -- ()

--------------------------
-- Create Method
----------------------------
function onCreate()

	CloseAllActorMenus()
	m_playerName = KEP_GetLoginName()
	requestUserId()
	requestLocationData() 


	KEP_EventRegister("CLOSE_VENDOR", KEP.HIGH_PRIO)
	KEP_EventRegister("CONFIRM_CREDIT_TRANSACTION", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler("updateVendorClientFull", "UPDATE_VENDOR_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler("onCloseMenu", "CLOSE_VENDOR", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCheckFullResponse", "INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("zoneNavigationEventHandler", "ZoneNavigationEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onConfirmationResponse", "CONFIRM_CREDIT_TRANSACTION", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "clientResizeEventHandler", "ClientResizeEvent", KEP.LOW_PRIO )
	
	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)
	Events.registerHandler("UPDATE_VENDOR_HEADER", updateHeader)
	Events.registerHandler("FRAMEWORK_UPDATE_GEM_COUNTS", updateGemCounts)

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()
	InventoryHelper.initializeInventory("g_displayTable", true, ITEMS_PER_PAGE)
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")

	local ev = KEP_EventCreate("ZoneNavigationEvent")
	KEP_EventSetFilter(ev, ZONE_NAV.CURRENT_URL_REQUEST)
	KEP_EventQueue(ev)
	
	-- Request player's inventory
	requestInventory(INVENTORY_PID)
	getInputLocations()


	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")				
	end

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "player")
	KEP_EventQueue(ev)

	MenuSetLocationThis(20, 99)
	resizeDropShadow( )
	moveElementsUp()
	--DragDrop.initializeDragDrop(0, 1, 0, INVENTORY_PID)

	for itemNumber=1, 13 do
		if gHandles["stcItemCount" .. itemNumber] and gHandles["imgItemCountBG" .. itemNumber] then
			local quantityLabel = gHandles["stcItemCount" .. itemNumber]

			Control_SetSize(gHandles["imgItemCountBG" .. itemNumber], Control_GetWidth(gHandles["imgItemCountBG" .. itemNumber]) + 5, Control_GetHeight(gHandles["imgItemCountBG" .. itemNumber]))
			Control_SetLocationX(gHandles["imgItemCountBG" .. itemNumber], Control_GetLocationX(gHandles["imgItemCountBG" .. itemNumber]) - 6 )
			Control_SetLocationX(quantityLabel, Control_GetLocationX(quantityLabel) - 3)
		end
	end

	populateGemData()
	requestGemData()
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister ("UPDATE_CREDIT_TRANSACTION", KEP.MED_PRIO)
	
end

function onDestroy()
	InventoryHelper.destroy()
	Events.sendEvent("FRAMEWORK_CLOSED_VENDOR", {PID = m_vendorPID})
end

-- Called each frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_validTrade then
		if m_pendingTrade and Control_GetEnabled(gHandles["btnBuy"]) == 1 then
			Control_SetEnabled(gHandles["btnBuy"], not m_pendingTrade)
		elseif not m_pendingTrade and Control_GetEnabled(gHandles["btnBuy"]) == 0 then
			Control_SetEnabled(gHandles["btnBuy"], not m_pendingTrade)
		end
	end
end


function setRequestHandle(requestHandle)
	m_requestHandle = requestHandle
end

----------------------------
-- Event Handlers
----------------------------

function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	m_pendingTrade = false
end

function onCheckFullResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local lootValid = (tostring(KEP_EventDecodeString(tEvent)) == "true")

	-- Enable / Disable purchasing random loot if the backpack is full
	if lootValid and m_pendingTrade then
		local tradeInfo = g_trades[m_currTrade]
		local newItem = tradeInfo.outputProperties
		local gem = tradeInfo.input
		local cost = tradeInfo.cost
		local newItemName = newItem.name
		local newItemType = newItem.itemType

		exchangeGems(gem, cost, newItemName, newItemType)
	else
		displayStatusMessage(INVENTORY_ERRORS.BACKPACK_FULL)
		m_pendingTrade = false
	end
	
end

-- Called when a new container inventory is received.  Updates all items and re-displays.
function updateVendorClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local returnedPID = KEP_EventDecodeNumber(tEvent)
	if m_vendorPID == nil then
		m_vendorPID = returnedPID
	elseif m_vendorPID ~= returnedPID then
		return
	end
	
	g_trades = decompileInventory(tEvent)

	updateInventory()
	updateItemContainer()

	m_currTrade = 1
	updateHighlight()
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if m_inventory[updateIndex] then
		m_inventory[updateIndex] = updateItem
		updateInventory()
		updateItemContainer()
		if m_currTrade ~= 0 then
			setCounts()
		end
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_inventory = decompileInventory(tEvent)
	updateInventory()
	updateItemContainer()
end


function ParseUserID(returnData)
	local result = nil  	-- result description
	local s, e = 0      	-- start and end of captured string
	local sh = nil			-- static handle
	s, e, result = string.find(returnData, "<ReturnCode>(%d+)</ReturnCode>")
	if result == "0" then

		-- Parse needed info from XML
		s, e, result = string.find(returnData, "<user_id>(%d+)</user_id>", e)
		g_userID = tonumber(result)

		s, e, result = string.find(returnData, "<player_id>(%d+)</player_id>", e)
		g_playerID = tonumber(result)
	end
	requestCreditsRewards()
end

function zoneNavigationEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	if filter == ZONE_NAV.CURRENT_URL_INFO then
		local url = KEP_EventDecodeString(tEvent)
		m_worldName = parseFriendlyNameFromURL(url)
		if m_item then
			setDisplay()
		end
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.MYFAME_USERID then
		local returnData = KEP_EventDecodeString( event )
		ParseUserID(returnData)
	elseif filter == WF.MYFAME_GETBALANCE then
		local returnData = KEP_EventDecodeString( event )
		g_credits, g_rewards = ParseBalances(returnData)
		setPurchasableOverlays()
		if m_currTrade ~= 0 then
			setCounts()
		end
	elseif filter == WF.GEM_VENDOR_SPEND then
		local returnData = KEP_EventDecodeString( event )
		local s = 0 		-- start and end index of substring
		local strPos = 0    	-- current position in string to parse
		local result = ""
		s, strPos, result = string.find(returnData, "<ReturnCode>([0-9]+)</ReturnCode>")
		if result == "0" then
			makeTrade()
		elseif returnData == -4 then
			log("Could not find Zone")
		elseif returnData == -6 then
			log("Payment Failure")
		end
	elseif filter == WF.GEM_AMOUNTS or filter == WF.GEM_CONVERT or filter == WF.GEM_REWARD then
		local returnData = KEP_EventDecodeString( event )
		local success = parseGemData(returnData)
		if success then
			populateGemData()
		end
	end
end

function onConfirmationResponse(event)
	local addTable = {}
	local newItem = {UNID = g_trades[m_currTrade].output, count = g_trades[m_currTrade].outputCount, properties = g_trades[m_currTrade].outputProperties}


	newItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "Vendor"}
	newItem.count = g_trades[m_currTrade].outputCount or 1
	table.insert(addTable, newItem)

	m_pendingTrade = true
	exchangeCredits(g_trades[m_currTrade].cost)
	m_pendingTrade = false
end

function updateGemCounts(event)
	local success = parseGemData(event.text)
	if success then
		populateGemData()
	end
end

----------------------------
-- Local Functions
----------------------------
function requestUserId()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..m_playerName, WF.MYFAME_USERID)
end

function requestCreditsRewards()
	if(g_userID > 0) then
		getBalances(g_userID, WF.MYFAME_GETBALANCE)
	end
end

function requestLocationData()
	local address = GameGlobals.WEB_SITE_PREFIX..GET_PLACE_INFORMATION_SUFFIX
	makeWebCall( address , WF.PLACE_INFO)
end


function exchangeGems(gem, cost, newItemName, newItemType)
	-- gem, gem amount, purchase item name, purchase item type
	local m_zoneInstanceId  = KEP_GetZoneInstanceId()
    local m_zoneType        = KEP_GetZoneIndexType()
    local itemAmounts 		= tostring(gem)..","..tostring(cost)
	local address = GameGlobals.WEB_SITE_PREFIX.."kgp/metaGameItems.aspx?action=spendItems"
	address = address.."&itemType=Gem" -- Should always be "Gem"
	address = address.."&itemAmounts="..tostring(itemAmounts) -- Pipe-delimited string of comma-delimited tuples. Indicates what gems were spent, and how many. Example: Topaz,1|Diamond,4
	address = address.."&zoneInstanceId="..tostring(m_zoneInstanceId) -- The zone instance id of the world the gems were spent in.
	address = address.."&zoneType="..tostring(m_zoneType) -- The zone type of the world the gems were spent in.
	address = address.."&gameItemName="..tostring(newItemName) -- The name of the game item that the user got in return for spending the gems.
	address = address.."&gameItemType="..tostring(newItemType) -- The item type (weapon, placeable, etc.) of the item that the user got in return for spending the gems.
	makeWebCall(address, WF.GEM_VENDOR_SPEND)
end

Dialog_OnLButtonDownInside = function(dialogHandle, x, y)
	local tradeClicked = false
	-- If trade section active, don't close when clicking within this section
	if (Control_ContainsPoint(gHandles["btnBuy"], x, y) == 1) and Control_GetEnabled(gHandles["btnBuy"]) == 1 then
		-- Create new item purchased
		local removeTable = {}
		
		local addTable = {}
		local newItem = {UNID = g_trades[m_currTrade].output, count = g_trades[m_currTrade].outputCount, properties = g_trades[m_currTrade].outputProperties}
		-- Assign a loot source for metric detection
		newItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "Vendor"}
		newItem.count = g_trades[m_currTrade].outputCount or 1
		table.insert(addTable, newItem)
		
		m_pendingTrade = true

		if g_trades[m_currTrade].input == "Topaz" then
			processTransaction(removeTable, addTable, true)
		elseif g_trades[m_currTrade].input == "Sapphire" then
			processTransaction(removeTable, addTable, true)
		elseif g_trades[m_currTrade].input == "Emerald" then
			processTransaction(removeTable, addTable, true)
		elseif g_trades[m_currTrade].input == "Ruby" then
			processTransaction(removeTable, addTable, true)
		elseif g_trades[m_currTrade].input == "Diamond" then
			processTransaction(removeTable, addTable, true)
		end	
	end

	-- If trade section active, don't close when clicking within this section
	if (Control_ContainsPoint(gHandles["imgPurchaseBG"], x, y) == 1) or (Control_ContainsPoint(gHandles["btnItemDrop1"], x, y) == 1) then
		if m_currTrade ~= 0 then
			tradeClicked = true
		end
	end

	if (Control_ContainsPoint(gHandles["btnNext"], x, y) == 1) and Control_GetEnabled(gHandles["btnNext"]) == 1 then
		changePage(1)
	end
	if (Control_ContainsPoint(gHandles["btnBack"], x, y) == 1) and Control_GetEnabled(gHandles["btnBack"]) == 1 then
		changePage(-1)
	end

	for i=1, VENDOR_ITEMS_PER_PAGE do
		if (Control_ContainsPoint(gHandles["imgItem"..i], x, y) == 1) and g_displayTable[i].output ~= 0 then
				tradeClicked = true
				m_currTrade = i + ((m_page - 1) * VENDOR_ITEMS_PER_PAGE)
				m_selectedPage = m_page
		end
	end

	updateHighlight()
end

updateHighlight = function ()

	if m_currTrade <= 0 then
		Control_SetVisible(gHandles['imgHighlight'], false)
		return
	else
		Control_SetVisible(gHandles['imgHighlight'], true)
	end 

	if m_currTrade <= 0 then 
		return
	end 

	setCounts()

	g_displayTable[10] = deepCopy(g_trades[m_currTrade])
	updateItemContainer()

	-- Set input/output tooltip, image, nameplate, and cost.
	Control_SetVisible(gHandles["btnItemDrop1"], false)
	Control_SetVisible(gHandles["imgSellBG"], false)

	local highlightIndex = m_currTrade - ((m_page -1 ) * VENDOR_ITEMS_PER_PAGE)

	if m_page == m_selectedPage then
		Control_SetLocation(gHandles["imgHighlight"], Control_GetLocationX(gHandles["imgItemBG".. highlightIndex]), Control_GetLocationY(gHandles["imgItemBG".. highlightIndex]))
	end
end

-- Sets the counts for the currently selected item
setCounts = function()
	-- Set required item count statics
	local hasNuf = true
	local color
	local offset = 12
	local reqHasCount = 0

	local reqNeedCount = g_trades[m_currTrade].cost
	local reqItem = g_trades[m_currTrade].input
	--input={UNID, properties={name, GLID, itemType}, count}
	-- Get how many of this item the player has

	if reqItem == "Topaz" or reqItem == "Sapphire" or reqItem == "Emerald" or reqItem == "Ruby" or reqItem == "Diamond" then
		reqHasCount = tonumber(m_currGems[string.lower(reqItem)])
	end	
	
	Control_SetEnabled(gHandles["imgItemOverlay10"], not hasNuf)
	Control_SetVisible(gHandles["imgItemOverlay10"], not hasNuf)
	-- Set the have color to red if you don't got nuf
	if reqNeedCount > reqHasCount then
		-- Set color to red
		hasNuf = false
		color = {a=255,r=255,g=0,b=0}
		Control_SetEnabled(gHandles["imgItemOverlay10"], not hasNuf)
		Control_SetVisible(gHandles["imgItemOverlay10"], not hasNuf)
	else
		-- Set color to white
		color = {a=255,r=255,g=255,b=255}
	end	
	local stcElementHandle = Static_GetDisplayElement(gHandles["txtCount"])
	BlendColor_SetColor(Element_GetFontColor(stcElementHandle), 0, color)
	
	-- Set how many of this item the player has

		Static_SetText(gHandles["txtBackpackCount"], "You have "..formatNumberSuffix(reqHasCount))
		Control_SetVisible(gHandles["txtBackpackCount"], true)
		Control_SetVisible(gHandles["imgDivider"], true)

	if g_trades[m_currTrade].input then
		local gemName = tostring(g_trades[m_currTrade].input)
		
		if gemName == "Topaz" then
			Control_SetVisible(gHandles["imgTopaz"], true)
			Control_SetVisible(gHandles["imgSapphire"], false)
			Control_SetVisible(gHandles["imgEmerald"], false)
			Control_SetVisible(gHandles["imgRuby"], false)
			Control_SetVisible(gHandles["imgDiamond"], false)
			Static_SetText(gHandles["txtBackpackCount"], "You have "..formatNumberSuffix(m_currGems[string.lower(gemName)]))
		elseif gemName == "Sapphire" then
			Control_SetVisible(gHandles["imgSapphire"], true)
			Control_SetVisible(gHandles["imgTopaz"], false)
			Control_SetVisible(gHandles["imgEmerald"], false)
			Control_SetVisible(gHandles["imgRuby"], false)
			Control_SetVisible(gHandles["imgDiamond"], false)
			Static_SetText(gHandles["txtBackpackCount"], "You have "..formatNumberSuffix(m_currGems[string.lower(gemName)]))
		elseif gemName == "Emerald" then
			Control_SetVisible(gHandles["imgEmerald"], true)
			Control_SetVisible(gHandles["imgTopaz"], false)
			Control_SetVisible(gHandles["imgSapphire"], false)
			Control_SetVisible(gHandles["imgRuby"], false)
			Control_SetVisible(gHandles["imgDiamond"], false)
			Static_SetText(gHandles["txtBackpackCount"], "You have "..formatNumberSuffix(m_currGems[string.lower(gemName)]))
		elseif gemName == "Ruby" then
			Control_SetVisible(gHandles["imgRuby"], true)
			Control_SetVisible(gHandles["imgTopaz"], false)
			Control_SetVisible(gHandles["imgSapphire"], false)
			Control_SetVisible(gHandles["imgEmerald"], false)
			Control_SetVisible(gHandles["imgDiamond"], false)
			Static_SetText(gHandles["txtBackpackCount"], "You have "..formatNumberSuffix(m_currGems[string.lower(gemName)]))
		elseif gemName == "Diamond" then
			Control_SetVisible(gHandles["imgDiamond"], true)
			Control_SetVisible(gHandles["imgTopaz"], false)
			Control_SetVisible(gHandles["imgSapphire"], false)
			Control_SetVisible(gHandles["imgEmerald"], false)
			Control_SetVisible(gHandles["imgRuby"], false)
			Static_SetText(gHandles["txtBackpackCount"], "You have "..formatNumberSuffix(m_currGems[string.lower(gemName)]).." Diamonds")
		end

		if reqNeedCount > 1 then
			gemName = formatGemName(gemName)
		end
		Static_SetText(gHandles["txtRewards"], gemName)

		-- REMOVE THESE ELEMENTS:
		-- Control_SetVisible(gHandles["imgRewards"], true)
		-- Control_SetVisible(gHandles["imgCredits"], false)
		-- Control_SetVisible(gHandles["txtRewards"], true)
		-- Control_SetVisible(gHandles["txtCredits"], false)
	end

	for i,v in pairs(m_inputLocations) do
		Control_SetLocation(gHandles[i], m_inputLocations[i].x, m_inputLocations[i].y)
	end
	Static_SetText(gHandles["txtCount"], formatNumberSuffix(g_trades[m_currTrade].cost))
	
	-- Enable the crafting button?
	Control_SetEnabled(gHandles["btnBuy"], hasNuf)
	m_validTrade = hasNuf

	return hasNuf
end

-- Set if the designated item is craftable
setPurchasableOverlays = function()
	-- Iterate of the recipes we'll be displaying

	for i=1, VENDOR_ITEMS_PER_PAGE  do
		local hasNuf = true
		if g_displayTable[i] then
			if g_displayTable[i].input == "Rewards" then
				if g_displayTable[i].cost > (tonumber(g_rewards)) then
					hasNuf = false
				end
			elseif g_displayTable[i].input == "Credits" then
				if g_displayTable[i].cost > (tonumber(g_credits)) then
					hasNuf = false
				end
			end
		end
		
		Control_SetEnabled(gHandles["imgItemOverlay"..i], not hasNuf)
		Control_SetVisible(gHandles["imgItemOverlay"..i], not hasNuf)
	end
end

-- Get initial location of purchase input/cost objects in menu
getInputLocations = function()
	for i,v in pairs(m_inputLocations) do
		m_inputLocations[i].x 			= Control_GetLocationX(gHandles[i])
		m_inputLocations[i].y 			= Control_GetLocationY(gHandles[i])
	end
end

makeTrade = function()
		local addTable = {}
		local newItem = {UNID = g_trades[m_currTrade].output, count = g_trades[m_currTrade].outputCount, properties = g_trades[m_currTrade].outputProperties}
		newItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "Vendor"}
		newItem.count = g_trades[m_currTrade].outputCount or 1
		table.insert(addTable, newItem)
		processTransaction({}, addTable)
		requestCreditsRewards()
		updateItemContainer()
		updateInventory()
		-- Update gem count and menu display of gem count
		requestGemData()
		deductGems()
		setCounts()
end

function deductGems()
	local reqNeedCount = g_trades[m_currTrade].cost
	local reqItem = g_trades[m_currTrade].input
	if reqItem == "Topaz" or reqItem == "Sapphire" or reqItem == "Emerald" or reqItem == "Ruby" or reqItem == "Diamond" then
		m_currGems[string.lower(reqItem)] = tonumber(m_currGems[string.lower(reqItem)]) - reqNeedCount
	end
end

function populateGemData()
	-- for i = 1, NUM_GEM_TYPES do
	-- 	local gemName = m_gemNames[i]
		
	-- 	Static_SetText(gHandles["stcGemCount"..tostring(i)], tostring(m_currGems[gemName]))
	-- 	Static_SetText(gHandles["stcGemValue"..tostring(i)], formatCommaValue(m_gemValues[gemName])..REWARDS_EACH_STRING)
		
	-- 	Control_SetEnabled(gHandles["btnCountDown"..tostring(i)], (m_exchangeGems[gemName] > 0))
	-- 	EditBox_SetText(gHandles["editGemCount"..tostring(i)], tostring(m_exchangeGems[gemName]), false)
	-- 	Control_SetEnabled(gHandles["btnGemCountUp"..tostring(i)], (m_exchangeGems[gemName] < m_currGems[gemName] and m_exchangeAmount + m_gemValues[gemName] <= m_exchangeRemaining))
	-- end
	
	-- Static_SetText(gHandles["txtRewards"], "Gems")
	Control_SetVisible(gHandles["txtCredits"], false)

	-- Static_SetText(gHandles["stcExchangeShadow"], formatCommaValue(m_exchangeAmount))
	-- Static_SetText(gHandles["stcExchangeCount"], formatCommaValue(m_exchangeAmount))
	-- Static_SetText(gHandles["stcRewardsRemaining"], formatCommaValue(m_exchangeRemaining)..REWARDS_REMAINING_STRING)
	
	-- Control_SetVisible(gHandles["btnmenuExchange"], (m_exchangeAmount > 0))
	-- Control_SetVisible(gHandles["btnmenuCancel1"], (m_exchangeAmount == 0))
	-- Control_SetVisible(gHandles["btnmenuCancel2"], (m_exchangeAmount > 0))
end

function parseGemData(gemString)
	local returnCode = ParseXmlReturnCode(gemString)
	if (returnCode == nil or returnCode ~= 0) then
		return false --Failure!
	end
	
	m_exchangeRemaining = xmlGetInteger(gemString, "DailyConversionsRemaining")
	
	for item in string.gmatch(gemString, "<Item>(.-)</Item>") do
		local itemName = string.lower(xmlGetString(item, "Name"))
		local itemBalance = xmlGetInteger(item, "Balance")
		local itemValue = xmlGetInteger(item, "ConversionValueRewards")
		
		m_currGems[itemName] = itemBalance
		m_gemValues[itemName] = itemValue
	end
	
	return true
end

-- Web call to receive the current gem data
function requestGemData()
	local address = GameGlobals.WEB_SITE_PREFIX.."/kgp/metaGameItems.aspx?action=getBalances&itemType=Gem"
	makeWebCall(address, WF.GEM_AMOUNTS)
end

formatGemName = function(gemName)
	if gemName == nil then return "" end
	if gemName == "Topaz" then
		return "Topazes"
	elseif gemName == "Sapphire" then
		return "Sapphires"
	elseif gemName == "Emerald" then
		return "Emeralds"
	elseif gemName == "Ruby" then
		return "Rubies"
	elseif gemName == "Diamond" then
		return "Diamonds"
	else
		return ""
	end
end