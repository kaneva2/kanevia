--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_PINCode.lua
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")

---------------
-- CONSTANTS --
---------------

local MENU_VERSION = {
    PENDING = 0,
    SET = 1,
    ACCESS = 2
}

local MENU_SIZE = { x = 320, y = 410 }
COLORS = { -- frequently used colors
    WHITE = {a=255, r=255, g=255, b=255},
    BLACK = {a=255, r=0, g=0, b=0},
    HEAD_GREY = {a = 255, r=200, g=200, b=200},
    BODY_GREY = {a = 255, r=160, g=160, b=160},
    GREEN = {a = 255, r=20, g=130, b=40},
    OUTLINE = "#FF000000"
}
local DEFAULT_PADDING = 6
local FONT_SIZE_MOD = 4

local CLOSE_BTN_SIZE = { x = 16, y = 16 }
local CLOSE_BTN_MARGIN = 4

local HEAD_FONT_SIZE = 18 + FONT_SIZE_MOD
local HEAD_MARGIN = 20
local HEAD_SIZE = { x = MENU_SIZE.x - HEAD_MARGIN * 2, y = HEAD_FONT_SIZE}
local HEAD_MAX_ROW_SIZE = HEAD_FONT_SIZE

local SUBHEAD_FONT_SIZE = 12 + FONT_SIZE_MOD
local SUBHEAD_SIZE = { x = MENU_SIZE.x - HEAD_MARGIN * 2, y = SUBHEAD_FONT_SIZE * 2.5 }
local DEFAULT_FONT = "Verdana"
local SUBHEAD_MARGIN = 16

local PIN_DIG_FONT_SIZE = 48 + FONT_SIZE_MOD
local PIN_DIG_SIZE = { x = PIN_DIG_FONT_SIZE, y = PIN_DIG_FONT_SIZE }
local PIN_DIG_CNT = 4
local PIN_DIG_SPACING = 0.5

local PIN_BOX_SIZE = { x = 168, y = 60 }

local PIN_TIP_SIZE = { x = 168, y = 20 }
local PIN_TIP_TEXT = "No Code Set"
local PIN_TIP_FONT_SIZE = 12 + FONT_SIZE_MOD
local PIN_TIP_PAD = 4

local PIN_DIG_PER_ROW = 3
local PIN_DIG_BTN_ROW_CNT = 4
local PIN_DIG_BTN_CNT = 10
local PIN_DIG_BTN_PAD = 12

local BTN_SIZE = { x = 42, y = 42 }
local BTN_FONT_SIZE = 18 + FONT_SIZE_MOD

-- ELEMENT_Y_OFFSET.PIN_DIGIT

local ELEMENT_Y_OFFSET       = {}
ELEMENT_Y_OFFSET.HEAD        = 24
ELEMENT_Y_OFFSET.SUBHEAD     = ELEMENT_Y_OFFSET.HEAD + HEAD_SIZE.y + 4
ELEMENT_Y_OFFSET.PIN_BOX     = ELEMENT_Y_OFFSET.SUBHEAD + SUBHEAD_SIZE.y + SUBHEAD_MARGIN + 2
ELEMENT_Y_OFFSET.PIN_DIG     = ELEMENT_Y_OFFSET.PIN_BOX + PIN_BOX_SIZE.y/2 - PIN_DIG_SIZE.y/2
ELEMENT_Y_OFFSET.PIN_TIP     = ELEMENT_Y_OFFSET.PIN_BOX + PIN_BOX_SIZE.y - PIN_TIP_SIZE.y - PIN_TIP_PAD
ELEMENT_Y_OFFSET.BTN_DIGIT   = ELEMENT_Y_OFFSET.PIN_BOX + PIN_BOX_SIZE.y + PIN_DIG_BTN_PAD * 2

local HEAD_TEXT = {
    SET = "Share ", -- Object name to be added
    ACCESS = "Unlock " -- Object name to be added
}
local SUBHEAD_TEXT = {
    SET = "Set your code below then share it with your friends so they can use it too.",
    ACCESS = "Owned by " -- Owner name to be added
}

local KEY_ENTER = 13

----------------
-- LOCAL VARS --
----------------

local m_menuVersion = MENU_VERSION.PENDING
local m_currDigit = 1
local m_PID = nil
local m_objectName = ""
local m_ownerName = ""
local m_pinCode = ""

------------------
-- CORE METHODS --
------------------

function onCreate()
    KEP_EventRegisterHandler("frameworkInitializePinHandler", "FRAMEWORK_INITIALIZE_PIN", KEP.HIGH_PRIO)
    Events.registerHandler("RETURN_PIN_CODE", returnPinCodeHandler)
    Events.registerHandler("OBJECT_ACTIVATE_PIN", objectActivatePinHandler)
end

function Dialog_OnMoved()
    MenuSetSizeThis(MENU_SIZE.x, MENU_SIZE.y)
    MenuCenterThis()
end

---------------
-- FUNCTIONS --
---------------

initializeGUIElements = function()
    -- Background image
    -- addImage("imgMenuBG", rect(0, 0, MENU_SIZE.x, MENU_SIZE.y), COLORS.BLACK, "white.tga", rect(0, 0, 1, 1))

    -- Header
    local stc = Dialog_GetStatic(gDialogHandle, "stcHeader")
    local tempStr = ""
    if m_menuVersion == MENU_VERSION.SET then
        tempStr = HEAD_TEXT.SET..m_objectName
    else
        tempStr = HEAD_TEXT.ACCESS..m_objectName
    end

    if stc == nil then
        stc = Dialog_AddStatic(gDialogHandle, "stcHeader", tempStr, HEAD_MARGIN, ELEMENT_Y_OFFSET.HEAD, HEAD_SIZE.x, HEAD_SIZE.y)
        local elem = Static_GetDisplayElement(stc)
        local bc = Element_GetFontColor(elem)
        Element_AddFont(elem, gDialogHandle, DEFAULT_FONT, HEAD_FONT_SIZE, 800, false)
        Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
        BlendColor_SetColor(bc, 0, COLORS.HEAD_GREY)
        if m_objectName ~= nil then
            updateHeaderSize()
        end
    end
    
    -- Subheader
    if m_menuVersion == MENU_VERSION.SET then
        tempStr = SUBHEAD_TEXT.SET
    else
        tempStr = SUBHEAD_TEXT.ACCESS..m_ownerName
    end
    addLabel("stcSubheader", tempStr, rect(HEAD_MARGIN, ELEMENT_Y_OFFSET.SUBHEAD, SUBHEAD_SIZE.x, SUBHEAD_SIZE.y), DEFAULT_FONT, SUBHEAD_FONT_SIZE, COLORS.BODY_GREY)
    
    -- Box displaying entered digits
    local pinBoxLoc = {x = MENU_SIZE.x/2 - PIN_BOX_SIZE.x/2, y = ELEMENT_Y_OFFSET.PIN_BOX}
    addImage("imgPinBoxMid", rect(pinBoxLoc.x + 1, pinBoxLoc.y, PIN_BOX_SIZE.x - 2, PIN_BOX_SIZE.y), COLORS.WHITE, "pincode_menu_elements.dds", rect(92, 0, 34, 60))
    addImage("imgPinBoxL", rect(pinBoxLoc.x, pinBoxLoc.y, 1, PIN_BOX_SIZE.y), COLORS.WHITE, "pincode_menu_elements.dds", rect(90, 0, 1, 60))
    addImage("imgPinBoxR", rect(pinBoxLoc.x + PIN_BOX_SIZE.x - 1, pinBoxLoc.y, 1, PIN_BOX_SIZE.y), COLORS.WHITE, "pincode_menu_elements.dds", rect(127, 0, 1, 60))
    
    -- Entered digits
    local pinDigitLoc = {x = pinBoxLoc.x + PIN_BOX_SIZE.x/2 - (PIN_DIG_SIZE.x * PIN_DIG_CNT * PIN_DIG_SPACING)/2, y = ELEMENT_Y_OFFSET.PIN_DIG}
    -- addImage("imgPinBoxBG", rect(pinDigitLoc.x, pinDigitLoc.y, PIN_DIG_SIZE.x * PIN_DIG_CNT * PIN_DIG_SPACING, PIN_DIG_SIZE.y), COLORS.GREEN, "white.tga", rect(0, 0, 1, 1))
    for i=1,PIN_DIG_CNT do
        addLabel("stcPinDigit"..tostring(i), " ", rect(pinDigitLoc.x + PIN_DIG_SIZE.x * (i-1) * PIN_DIG_SPACING - (PIN_DIG_SIZE.x * PIN_DIG_SPACING)/2, pinDigitLoc.y, PIN_DIG_SIZE.x, PIN_DIG_SIZE.y), DEFAULT_FONT, PIN_DIG_FONT_SIZE, COLORS.WHITE)
    end

    -- Tip text below digits
    addLabel("stcPinTip", " ", rect(pinBoxLoc.x + PIN_BOX_SIZE.x/2 - PIN_TIP_SIZE.x/2, ELEMENT_Y_OFFSET.PIN_TIP, PIN_TIP_SIZE.x, PIN_TIP_SIZE.y), DEFAULT_FONT, PIN_TIP_FONT_SIZE, COLORS.BODY_GREY)

    -- Digit Buttons
    local xOffset = MENU_SIZE.x/2 - ((BTN_SIZE.x * PIN_DIG_PER_ROW + PIN_DIG_BTN_PAD * 2) / (PIN_DIG_PER_ROW-1))
    local yOffset = {} -- {0,0,0,1,1,1,2,2,2}
    for i=0,(PIN_DIG_BTN_ROW_CNT-1) do
        for j=1,PIN_DIG_PER_ROW do
            table.insert(yOffset, i)
        end
    end
    for i=1,(PIN_DIG_BTN_CNT-1) do
        local tempName = "btnDigit"..tostring(i)
        addButton(tempName, tostring(i), rect(xOffset + BTN_SIZE.x * ((i+(PIN_DIG_PER_ROW-1))%PIN_DIG_PER_ROW) + PIN_DIG_BTN_PAD * ((i+(PIN_DIG_PER_ROW-1))%PIN_DIG_PER_ROW), ELEMENT_Y_OFFSET.BTN_DIGIT + BTN_SIZE.y * yOffset[i] + PIN_DIG_BTN_PAD * yOffset[i], BTN_SIZE.x, BTN_SIZE.y), "pincode_menu_elements.dds", rect(0, 0, 42, 42), DEFAULT_FONT, BTN_FONT_SIZE, COLORS.BLACK)
        setButtonActiveState(tempName, "pincode_menu_elements.dds", rect(44, 0, 41, 41))
        -- Add button functionality
        _G[tempName.."_OnButtonClicked"] = function(buttonHandle)
            addDigit(i)
        end
    end

    -- Zero button (on its own row)
    addButton("btnDigit0", "0", rect(xOffset + BTN_SIZE.x * 1 + PIN_DIG_BTN_PAD * 1, ELEMENT_Y_OFFSET.BTN_DIGIT + BTN_SIZE.y * 3 + PIN_DIG_BTN_PAD * 3, BTN_SIZE.x, BTN_SIZE.y), "pincode_menu_elements.dds", rect(0, 0, 42, 42), DEFAULT_FONT, BTN_FONT_SIZE, COLORS.BLACK)
    setButtonActiveState("btnDigit0", "pincode_menu_elements.dds", rect(44, 0, 41, 41))
    _G["btnDigit0_OnButtonClicked"] = function(buttonHandle)
        addDigit(0)
    end
    
    -- Clear button
    local clearBtnRect = rect(xOffset + BTN_SIZE.x * 0 + PIN_DIG_BTN_PAD * 0, ELEMENT_Y_OFFSET.BTN_DIGIT + BTN_SIZE.y * 3 + PIN_DIG_BTN_PAD * 3, BTN_SIZE.x, BTN_SIZE.y)
    addButton("btnClear", " ", clearBtnRect, "pincode_menu_elements.dds", rect(0, 44, 41, 41), DEFAULT_FONT, BTN_FONT_SIZE, COLORS.BLACK)
    setButtonActiveState("btnClear", "pincode_menu_elements.dds", rect(44, 44, 41, 41))
    -- Add button functionality
    _G["btnClear_OnButtonClicked"] = function(buttonHandle)
        clearPassword()
    end

    -- Submit button
    addButton("btnSubmit", "OK", rect(xOffset + BTN_SIZE.x * 2 + PIN_DIG_BTN_PAD * 2, ELEMENT_Y_OFFSET.BTN_DIGIT + BTN_SIZE.y * 3 + PIN_DIG_BTN_PAD * 3, BTN_SIZE.x, BTN_SIZE.y), "pincode_menu_elements.dds", rect(0, 0, 42, 42), DEFAULT_FONT, BTN_FONT_SIZE, COLORS.GREEN)
    setButtonActiveState("btnSubmit", "pincode_menu_elements.dds", rect(44, 0, 41, 41))
    -- Add button functionality
    _G["btnSubmit_OnButtonClicked"] = function(buttonHandle)
        if m_currDigit > PIN_DIG_CNT then
            submitPassword()
        -- elseif m_currDigit == 1 and m_menuVersion == MENU_VERSION.SET then
        --     removePassword()
        else
            clearPassword()
            ctrl = Dialog_GetControl(gDialogHandle, "stcPinTip")
            Static_SetText(ctrl, "PIN Code Too Short")
        end
    end
    
    -- Close button
    addButton("btnClose", " ", rect(MENU_SIZE.x - CLOSE_BTN_SIZE.x - CLOSE_BTN_MARGIN, CLOSE_BTN_MARGIN, CLOSE_BTN_SIZE.x, CLOSE_BTN_SIZE.y), "close.tga", rect(1, 1, CLOSE_BTN_SIZE.x, CLOSE_BTN_SIZE.y), DEFAULT_FONT, BTN_FONT_SIZE, COLORS.BLACK) -- Close button (x)
    _G["btnCloseMapText_OnButtonClicked"] = function(buttonHandle)
       btnClose_OnButtonClicked()
    end
end

addDigit = function(inputDigit)
    if m_currDigit <= PIN_DIG_CNT then
        local ctrl = Dialog_GetControl(gDialogHandle, "stcPinDigit"..tostring(m_currDigit))
        Static_SetText(ctrl, tostring(inputDigit))
        m_currDigit = m_currDigit + 1
        ctrl = Dialog_GetControl(gDialogHandle, "stcPinTip")
        Static_SetText(ctrl, " ")
    end
end

clearPassword = function()
    m_currDigit = 1
    for i=1,PIN_DIG_CNT do
        local ctrl = Dialog_GetControl(gDialogHandle, "stcPinDigit"..tostring(i))
        Static_SetText(ctrl, "-")
        if m_menuVersion == MENU_VERSION.SET then
            ctrl = Dialog_GetControl(gDialogHandle, "stcPinTip")
            Static_SetText(ctrl, "No Code Set")
        end
    end
end

submitPassword = function()
    local tempPinCode = ""
    for i=1,math.max(m_currDigit-1,1) do
        local ctrl = Dialog_GetControl(gDialogHandle, "stcPinDigit"..tostring(i))
        local digit = Static_GetText(ctrl)
        tempPinCode = tempPinCode..digit
    end

    if m_menuVersion == MENU_VERSION.SET then
        -- Save the PIN
        tempPinCode = tostring(tempPinCode)
        Events.sendEvent("SAVE_PIN_CODE", {pinCode = tempPinCode, PID = m_PID})
        -- Show a status message
        local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
        KEP_EventEncodeNumber(statusEvent, 0)
        KEP_EventEncodeString(statusEvent, m_objectName.." PIN code has been set!")
        KEP_EventEncodeNumber(statusEvent, 3)
        KEP_EventEncodeNumber(statusEvent, 255)
        KEP_EventEncodeNumber(statusEvent, 0)
        KEP_EventEncodeNumber(statusEvent, 0)
        KEP_EventSetFilter(statusEvent, 4)
        KEP_EventQueue(statusEvent)
        -- Close the menu
        MenuCloseThis()
    else
        attemptPin(tempPinCode)
    end
end

-- removePassword = function()
--     if m_menuVersion == MENU_VERSION.SET then
--         -- Save the PIN
--         tempPinCode = tostring(tempPinCode)
--         Events.sendEvent("SAVE_PIN_CODE", {pinCode = nil, PID = m_PID})
--         -- Show a status message
--         local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
--         KEP_EventEncodeNumber(statusEvent, 0)
--         KEP_EventEncodeString(statusEvent, m_objectName.." has no PIN code!")
--         KEP_EventEncodeNumber(statusEvent, 3)
--         KEP_EventEncodeNumber(statusEvent, 255)
--         KEP_EventEncodeNumber(statusEvent, 0)
--         KEP_EventEncodeNumber(statusEvent, 0)
--         KEP_EventSetFilter(statusEvent, 4)
--         KEP_EventQueue(statusEvent)
--         -- Close the menu
--         MenuCloseThis()
--     end
-- end

attemptPin = function(attemptedPin)
    if tostring(attemptedPin) == tostring(m_pinCode) then
        Events.sendEvent("PIN_CODE_ACCEPTED", {PID = m_PID})
        MenuCloseThis()
    else
        clearPassword()
        ctrl = Dialog_GetControl(gDialogHandle, "stcPinTip")
        Static_SetText(ctrl, "Incorrect PIN Code")
    end
end

updatePinCode = function(inputPin)
    m_currDigit = 1
    if inputPin and string.len(inputPin) then
        local pinString = tostring(inputPin)
        for i=1,string.len(pinString) do
            local ctrl = Dialog_GetControl(gDialogHandle, "stcPinDigit"..tostring(i))
            local digit = string.sub(pinString,i,i)
            Static_SetText(ctrl, digit)
            m_currDigit = m_currDigit + 1
        end
        ctrl = Dialog_GetControl(gDialogHandle, "stcPinTip")
        Static_SetText(ctrl, " ")
    else
        for i=1,PIN_DIG_CNT do
            local ctrl = Dialog_GetControl(gDialogHandle, "stcPinDigit"..tostring(i))
            Static_SetText(ctrl, "-")
        end
        ctrl = Dialog_GetControl(gDialogHandle, "stcPinTip")
        Static_SetText(ctrl, PIN_TIP_TEXT)
    end
end

--------------------
-- EVENT HANDLERS --
--------------------

function frameworkInitializePinHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    m_PID = KEP_EventDecodeNumber(tEvent)
    m_objectName = KEP_EventDecodeString(tEvent)

    m_menuVersion = MENU_VERSION.SET
    initializeGUIElements()
    MenuSetSizeThis(MENU_SIZE.x, MENU_SIZE.y)
    MenuCenterThis()

    -- Request saved PIN, if it exists
    Events.sendEvent("REQUEST_PIN_CODE", {PID = m_PID})
end

function objectActivatePinHandler(event)
    m_PID = event.PID
    m_objectName = event.name
    m_ownerName = event.owner

    m_menuVersion = MENU_VERSION.ACCESS
    initializeGUIElements()
    MenuSetSizeThis(MENU_SIZE.x, MENU_SIZE.y)
    MenuCenterThis()

    -- Request saved PIN, if it exists
    Events.sendEvent("REQUEST_PIN_CODE", {PID = m_PID})
end

function returnPinCodeHandler(event)
    local tempPinCode = event.pinCode
    m_pinCode = tostring(tempPinCode)
    -- Update PIN
    if m_menuVersion == MENU_VERSION.SET then
        updatePinCode(tempPinCode)
    end
end

----------------------
-- HELPER FUNCTIONS --
----------------------

rect = function(x, y, w, h)
    return { x = x, y = y, w = w, h = h }
end

color = function(a, r, g, b)
    return { a = a, r = r, g = g, b = b }
end

-- round = function(x)
--   if x%2 ~= 0.5
--     return math.floor(x+0.5)
--   end
--   return x-0.5
-- end

addImage = function(name, rect, color, texName, texRect)
    local img = Dialog_GetImage(gDialogHandle, name)
    if img == nil then
        img = Dialog_AddImage(gDialogHandle, name, rect.x, rect.y, texName, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
        Image_SetColor(img, color)
        Control_SetSize(Image_GetControl(img), rect.w, rect.h)
    end
    return img
end

addButton = function(name, text, rect, texName, texRect, fontFace, fontSize, fontColor)
    local btn = Dialog_GetButton(gDialogHandle, name)
    if btn == nil then
        btn = Dialog_AddButton(gDialogHandle, name, text, rect.x, rect.y, rect.w, rect.h)
    end

    local elem, bc
    -- Disabled
    elem = Button_GetDisabledDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    bc = Element_GetFontColor(elem)
    Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
    BlendColor_SetColor(bc, 0, fontColor)
    -- Focused
    elem = Button_GetFocusedDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    bc = Element_GetFontColor(elem)
    Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
    BlendColor_SetColor(bc, 0, fontColor)
    -- MouseOver
    elem = Button_GetMouseOverDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    bc = Element_GetFontColor(elem)
    Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
    BlendColor_SetColor(bc, 0, fontColor)
    -- Normal
    elem = Button_GetNormalDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    bc = Element_GetFontColor(elem)
    Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
    BlendColor_SetColor(bc, 0, fontColor)
    -- Pressed
    elem = Button_GetPressedDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    bc = Element_GetFontColor(elem)
    Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_CENTER)
    BlendColor_SetColor(bc, 0, fontColor)

    return btn
end

addLabel = function(name, text, rect, fontFace, fontSize, fontColor)
    local stc = Dialog_GetStatic(gDialogHandle, name)
    if stc == nil then
        -- local stcFontSize = fontSize + 2
        -- local stcW = stcFontSize*#text*0.8
        stc = Dialog_AddStatic(gDialogHandle, name, text, rect.x, rect.y, rect.w, rect.h)
        local elem = Static_GetDisplayElement(stc)
        local bc = Element_GetFontColor(elem)
        Element_AddFont(elem, gDialogHandle, fontFace, fontSize, 0, false)
        Element_SetTextFormat(elem, TEXT_HALIGN_CENTER + TEXT_VALIGN_TOP)
        BlendColor_SetColor(bc, 0, fontColor)
    end
end

setButtonActiveState = function(name, texName, texRect)
-- Format button label fonts
    local btn = Dialog_GetButton(gDialogHandle, name)
    local stc = Button_GetStatic(btn)
    -- Mouseover
    elem = Button_GetMouseOverDisplayElement(stc)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    -- Pressed
    elem = Button_GetPressedDisplayElement(stc)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
end

updateHeaderSize = function()
    -- Resize the header box to fit the object name
    -- local stc = gHandles["stcHeader"]
    local stc = Dialog_GetStatic(gDialogHandle, "stcHeader")
    -- local ctrl =  Static_GetControl(stc)
    local w = HEAD_SIZE.x
    local h = HEAD_SIZE.y
    local s = ""
    if m_menuVersion == MENU_VERSION.SET then
        s = tostring(HEAD_TEXT.SET..m_objectName)
    else
        s = tostring(HEAD_TEXT.ACCESS..m_objectName)
    end
    local newH = math.ceil(Static_GetStringWidth(stc, s) / w) * h -- GetStringHeight doesn't work, so add a new row for every for each time width extends one unit beyond itself
    Control_SetSize(stc, w, newH)
    local hDiff = newH - HEAD_SIZE.y
    -- Shift everything down
    MENU_SIZE.y = MENU_SIZE.y + hDiff
    HEAD_SIZE.y = HEAD_SIZE.y + hDiff
    ELEMENT_Y_OFFSET.SUBHEAD     = ELEMENT_Y_OFFSET.HEAD + HEAD_SIZE.y + 4
    if m_menuVersion == MENU_VERSION.SET then
        ELEMENT_Y_OFFSET.PIN_BOX = ELEMENT_Y_OFFSET.SUBHEAD + SUBHEAD_SIZE.y + SUBHEAD_MARGIN
    else
        ELEMENT_Y_OFFSET.PIN_BOX = ELEMENT_Y_OFFSET.SUBHEAD + SUBHEAD_SIZE.y/2 + SUBHEAD_MARGIN
    end
    ELEMENT_Y_OFFSET.PIN_DIG     = ELEMENT_Y_OFFSET.PIN_BOX + PIN_BOX_SIZE.y/2 - PIN_DIG_SIZE.y/2
    ELEMENT_Y_OFFSET.PIN_TIP     = ELEMENT_Y_OFFSET.PIN_BOX + PIN_BOX_SIZE.y - PIN_TIP_SIZE.y - PIN_TIP_PAD
    ELEMENT_Y_OFFSET.BTN_DIGIT   = ELEMENT_Y_OFFSET.PIN_BOX + PIN_BOX_SIZE.y + PIN_DIG_BTN_PAD * 2
    -- Resize the menu
    ctrl = Dialog_GetControl(gDialogHandle, "imgMenuBG")
    Control_SetSize(ctrl, MENU_SIZE.x, MENU_SIZE.y)
    -- Recenter the menu
    MenuSetSizeThis(MENU_SIZE.x, MENU_SIZE.y)
    MenuCenterThis()
end
