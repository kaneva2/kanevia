--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_JetPack.lua
--
-- Lets the Creator FLY!!!
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- Local vars
local g_currKey = nil

-- Statics
local HUD_HEIGHT = 48
local SCREEN_BOTTOM_BUFFER = 28

local KEY_SPACE = 32
local KEY_L_SHIFT = 16

-- Called when the menu is created
function onCreate()
	Dialog_OnMoved(gDialogHandle)
	
	Events.sendEvent("FRAMEWORK_SET_CREATOR_BOOST", {boost = "Active"})
end

-- Called when a key is pressed
function Dialog_OnKeyDown(dialogHandle, key, shift, ctrl, alt)
	if (key == KEY_SPACE) and (g_currKey ~= key) then
		g_currKey = key
		Events.sendEvent("FRAMEWORK_SET_CREATOR_BOOST", {boost = "Boost"})
	elseif (key == KEY_L_SHIFT) and (g_currKey ~= key) then
		g_currKey = key
		Events.sendEvent("FRAMEWORK_SET_CREATOR_BOOST", {boost = "Fall"})
	end
end

-- Called when a key is released
function Dialog_OnKeyUp(dialogHandle, key, shift, ctrl, alt)
	if (key == KEY_SPACE) and (g_currKey == KEY_SPACE) then
		g_currKey = nil
		Events.sendEvent("FRAMEWORK_SET_CREATOR_BOOST", {boost = "Active"})
	elseif (key == KEY_L_SHIFT) and (g_currKey == KEY_L_SHIFT) then
		g_currKey = nil
		Events.sendEvent("FRAMEWORK_SET_CREATOR_BOOST", {boost = "Active"})
	end
end

-- Called when the client is resized
function Dialog_OnMoved(dialogHandle, x, y)
	-- Place menu in bottom-left
	local x = (Dialog_GetScreenWidth(dialogHandle)/2) - (Dialog_GetWidth(dialogHandle)/2)
	local y = Dialog_GetScreenHeight(dialogHandle) - SCREEN_BOTTOM_BUFFER - HUD_HEIGHT - Dialog_GetHeight(dialogHandle)
	
	MenuSetLocationThis(x, y)
end

-- Called when this menu is destroyed
function onDestroy()
	Events.sendEvent("FRAMEWORK_SET_CREATOR_BOOST", {boost = "Off"})
end