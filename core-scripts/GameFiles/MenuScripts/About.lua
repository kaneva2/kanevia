--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	local ver = KEP_GetFileVersion()
	sh = Dialog_GetStatic(dialogHandle, "lblBuildNumber")
	if sh ~= nil then
		Static_SetText(sh, "Version "..ver)
	end
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
