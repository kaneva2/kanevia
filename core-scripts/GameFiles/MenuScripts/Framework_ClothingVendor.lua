--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_CreditsVendor.lua
--
-- Displays available items for vending
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")
dofile("Framework_InventoryHelper.lua")
dofile("Easing.lua")
dofile("HUDHelper.lua")
dofile("Framework_ActorHelper.lua")
dofile("Framework_MenuShadowHelper.lua")

UPDATE_HUD_EVENT = "UpdateHUDEvent"
ITEM_PURCHASED_EVENT = "ItemPurchasedEvent"

INVENTORY_TYPE_CREDITS = "256"
INVENTORY_TYPE_REWARDS = "512"
INVENTORY_TYPE_BOTH = "768"

----------------------
-- Local Constants
----------------------
local ITEMS_PER_PAGE = 10
local VENDOR_ITEMS_PER_PAGE = 9
local TOOLTIP_TABLE = {header = "name"}

local COLORS_VALUES =	{	["Common"] = {a = 255, r = 255, g = 255, b = 255},
							["Rare"] = {a = 255, r = 0, g = 150, b = 255},
							["Very Rare"] = {a = 255, r = 174, g = 0, b = 255},
							["Extremely Rare"] = {a = 255, r = 255, g = 144, b = 0},
							["Never"] = {a = 255, r = 255, g = 144, b = 0}
						}

local ITEM_LEVEL_COORDS = {
							{left = 0, right = 15, top = 146, bottom = 161},
							{left = 16, right = 31, top = 146, bottom = 161},
							{left = 32, right = 47, top = 146, bottom = 161},
							{left = 0, right = 15, top = 162, bottom = 177},
							{left = 16, right = 31, top = 162, bottom = 177}
}

local ITEM_RARITY_BG = {	["Common"] = {left = 208, right = 292, top = 167, bottom = 251},
							["Rare"] = {left = 293, right = 377, top = 167, bottom = 251},
							["Very Rare"] = {left = 378, right = 462, top = 167, bottom = 251},
							["Extremely Rare"] = {left = 378, right = 462, top = 82, bottom = 166},
							["Never"] = {left = 378, right = 462, top = 82, bottom = 166}
						}

local ITEM_LEVEL_RARITY_OFFSET = {["Common"] = 0, ["Rare"] = 48, ["Very Rare"] = 96, ["Extremely Rare"] = 144}

g_displayTable = {}	-- items being displayed :: [{UNID, properties={name, GLID, itemType}, count}]
local m_items = {}	-- output items :: [{UNID, properties={name, GLID, itemType}, count}]
local g_trades = {} -- output, input, and cost :: [{output={UNID, properties={name, GLID, itemType}, count}, input={UNID, properties={name, GLID, itemType}, count}, cost}]
local g_tblObjectData = {}
local m_trades = {}

----------------------
-- Local Variables
----------------------
local m_displaying = false
local m_inputLocations = 	{ 	--["txtCost"] = { x = 0, y = 0 },
								--["txtCount"] = { x = 0, y = 0 },
							}

local m_playerName = ""
local m_currTrade = 0
local m_dropFlashing = false
local m_dropFlashed = false
local m_dropFlashTime = 0
local m_overButton = false
local m_validTrade = false
m_vendorPID = nil

local m_requestHandle = nil
local m_pendingTrade = false

local m_inventory = {}			-- [{UNID, count, name, itemType}]
local m_inventoryCounts = {}	-- {UNID:count}

local g_credits = 0
local g_rewards = 0
local g_userID = 0
local m_worldName

local m_page = 1
local m_selectedPage = 1

----------------------
-- Local Functions
----------------------

local initalizeClickToTrade -- (numButtons)
local updateItemContainer -- ()
local setCounts -- ()
local setPurchasableOverlays -- ()
local getInputLocations -- ()
local buyItemCredits -- ()
local formatNumberSuffix -- ()
local moveElementsUp -- ()
local updatePage -- ()

----------------------------
-- Create Method
----------------------------
function onCreate()
	CloseAllActorMenus()
	m_playerName = KEP_GetLoginName()
	requestUserId()

	KEP_EventRegister("CLOSE_VENDOR", KEP.HIGH_PRIO)
	KEP_EventRegister(ITEM_PURCHASED_EVENT, KEP.HIGH_PRIO)
	KEP_EventRegister("CONFIRM_CREDIT_TRANSACTION", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.HIGH_PRIO )
	--KEP_EventRegisterHandler("updateVendorClientFull", "UPDATE_VENDOR_CLIENT_FULL", KEP.MED_PRIO)
	--KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	--KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler("onCloseMenu", "CLOSE_VENDOR", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	--KEP_EventRegisterHandler("onCheckFullResponse", "INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	--KEP_EventRegisterHandler("zoneNavigationEventHandler", "ZoneNavigationEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onConfirmatonResponse", "CONFIRM_CREDIT_TRANSACTION", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "clientResizeEventHandler", "ClientResizeEvent", KEP.LOW_PRIO )
	
	Events.registerHandler("UPDATE_SHOP_VENDOR_ITEMS", populateInventory)

	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)
	Events.registerHandler("UPDATE_VENDOR_HEADER", updateHeader)

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()
	InventoryHelper.initializeInventory("g_displayTable", true, ITEMS_PER_PAGE)
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")
	
	getInputLocations()


	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")				
	end

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "clothing")
	KEP_EventQueue(ev)


	MenuSetLocationThis(20, 99)
	resizeDropShadow( )
	moveElementsUp()
	for itemNumber=1, 13 do
		if gHandles["stcItemCount" .. itemNumber] and gHandles["imgItemCountBG" .. itemNumber] then
			local quantityLabel = gHandles["stcItemCount" .. itemNumber]

			Control_SetSize(gHandles["imgItemCountBG" .. itemNumber], Control_GetWidth(gHandles["imgItemCountBG" .. itemNumber]) + 5, Control_GetHeight(gHandles["imgItemCountBG" .. itemNumber]))
			Control_SetLocationX(gHandles["imgItemCountBG" .. itemNumber], Control_GetLocationX(gHandles["imgItemCountBG" .. itemNumber]) - 6 )
			Control_SetLocationX(quantityLabel, Control_GetLocationX(quantityLabel) - 3)
		end
	end

	updateHighlight()
	updateItemContainer()
	setCounts()
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister ("UPDATE_CREDIT_TRANSACTION", KEP.MED_PRIO)
end


function onDestroy()
	InventoryHelper.destroy()
	Events.sendEvent("FRAMEWORK_CLOSED_VENDOR", {PID = m_vendorPID})
end

function updateBackpack()
	--if MenuIsClosed("UnifiedNavigation.xml") then
		MenuClose("UnifiedNavigation.xml")
		MenuClose("UnifiedInventory.xml")
		--MenuClose("Inventory.xml")

		--MenuOpen("UnifiedNavigation.xml")				
	--end
	MenuOpen("UnifiedNavigation.xml")
	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "clothing")
	KEP_EventQueue(ev)

	--local ev = KEP_EventCreate("UpdateInventoryEvent")
	--KEP_EventEncodeString(ev, "clothing")
	--KEP_EventQueue(ev)
end

-- Called each frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	--[[
	if m_validTrade then
		if m_pendingTrade and Control_GetEnabled(gHandles["btnBuyCredits"]) == 1 then
			--Control_SetEnabled(gHandles["btnBuyCredits"], not m_pendingTrade)
		elseif m_pendingTrade and Control_GetEnabled(gHandles["btnBuyRewards"]) == 1 then
			--Control_SetEnabled(gHandles["btnBuyRewards"], not m_pendingTrade)
		else
			--Control_SetEnabled(gHandles["btnBuyCredits"], not m_pendingTrade)
			--Control_SetEnabled(gHandles["btnBuyRewards"], not m_pendingTrade)
		end
	end
	]]--
end


function setRequestHandle(requestHandle)
	m_requestHandle = requestHandle
end

----------------------------
-- Event Handlers
----------------------------
function closeVendor(event)
	if not MenuIsOpen("UnifiedInventory") then
		MenuCloseThis()
	end
end

function populateInventory( event )

	m_trades = deepCopy(event.trades)
	m_items = deepCopy(event.trades)
	g_displayTable = deepCopy(event.trades)

	local itemwebs = ""
	for i = 1, #m_trades do
		if i ~= 1 then
			itemwebs = itemwebs.. ","
		end 
		itemwebs = itemwebs.. tostring(m_trades[i].output)
	end 

	-- make the webcall to get the item information 
	makeWebCall(GameGlobals.WEB_SITE_PREFIX.. "kgp/instantbuy.aspx?operation=info&items=".. itemwebs, 105)
end

-- tooltips 
function formatTooltips(item)
	if item then

		item.tooltip = {}
		
		if item.name then 
			item.tooltip["header"] = {label = tostring(item.name)} 
		end 

		if item.description and item.description ~= "None" then
			item.tooltip["body"] = {label = tostring(item.description)}
		else
			item.tooltip["body"] = {label = "No description found.", font = { italic = "1"}}
		end
	end
end

function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	m_pendingTrade = false
	if success and m_sellItem and m_sellItem.GLID then
		m_sellItem = {}
		closeSell()
	end
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	log("--- updateInventoryClient")
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if m_inventory[updateIndex] then
		m_inventory[updateIndex] = updateItem
		--updateInventory()
		updateItemContainer()
		if m_currTrade ~= 0 then
			setCounts()
		end
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_inventory = decompileInventory(tEvent)
	--updateInventory()
	updateItemContainer()
end

-- updates the text on the header of the vendor 
function updateHeader(event)
	local header = event.header
	local name = event.name
	m_vendorPID = event.PID

	if name and name ~= "" then
		Static_SetText(gHandles["Menu_Title"], name)
	end
end

-- needed to get player information 
function requestUserId()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..m_playerName, WF.MYFAME_USERID)
end

-- gets the player's credit balance? 
function requestCreditsRewards()
	if(g_userID > 0) then
		getBalances(g_userID, WF.MYFAME_GETBALANCE)
	end
end

-- gets the user's information and requests the player's credit balance 
function ParseUserID(returnData)
	local result = nil  	-- result description
	local s, e = 0      	-- start and end of captured string
	local sh = nil			-- static handle
	s, e, result = string.find(returnData, "<ReturnCode>(%d+)</ReturnCode>")
	if result == "0" then

		-- Parse needed info from XML
		s, e, result = string.find(returnData, "<user_id>(%d+)</user_id>", e)
		g_userID = tonumber(result)

		s, e, result = string.find(returnData, "<player_id>(%d+)</player_id>", e)
		g_playerID = tonumber(result)
	end
	requestCreditsRewards()
end

-- parse the data for the items
function ParseClothingData( returnData )
	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""
	s, strPos, result = string.find(returnData, "<ReturnCode>([0-9]+)</ReturnCode>")

	--m_trades = {}

	if result == "0" then
		local itemData = ""
		local name = ""
		local description = ""
		local sellingPrice = ""
		local gender = ""
		local itemGlid = ""
		local inventory_type = ""
		local i = 1

		-- parse all the items in the return string 
		for itemData in string.gmatch(returnData, "<Item_x0020_Info>(.-)</Item_x0020_Info>") do
			--itemStart, strPos, itemData = string.find(returnData, "<Item_x0020_Info>(.-)</Item_x0020_Info>", strPos)
			if itemData then 
				--log("---------------------------- START NEW ITEM")

				-- get the glid of the items 
				s, strPos, itemGlid = string.find(itemData, "<global_id>(.-)</global_id>")
				m_trades[i].output = itemGlid
				applyImage(gHandles["imgItem"..i], m_trades[i].output)
				--log("--- glid: ".. tostring(itemGlid))

				-- find the name of the item
				s, strPos, name = string.find(itemData, "<name>(.-)</name>")
				m_trades[i].name = name
				--log("--- name: ".. tostring(name))

				-- find the selling price of the items 
				s, strPos, sellingPrice = string.find(itemData, "<market_cost>(.-)</market_cost>")
				m_trades[i].creditCost = sellingPrice
				m_trades[i].rewardCost = sellingPrice
				--log("--- market_cost: ".. tostring(sellingPrice))

				-- find the description of the items 
				s, strPos, description = string.find(itemData, "<description>(.-)</description>")
				m_trades[i].description = description

				-- find the gender of the items 
				--s, strPos, gender = string.find(itemData, "<description>(.-)</description>")

				s, strPos, inventory_type = string.find(itemData, "<inventory_type>(.-)</inventory_type>")
				m_trades[i].inventory_type = inventory_type

				--log("---------------------------- END NEW ITEM")

				i = i + 1
			end 
		end 

		m_currTrade = 1
	end
end

-- gets the information from the browser, handles getting player currency and other things 
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.MYFAME_USERID then
		local returnData = KEP_EventDecodeString( event )
		ParseUserID(returnData)
		--log("--- BrowserPageReadyHandler filter: MYFAME_USERID")
	elseif filter == WF.MYFAME_GETBALANCE then
		local returnData = KEP_EventDecodeString( event )
		g_credits, g_rewards = ParseBalances(returnData)
		--setPurchasableOverlays()
		if m_currTrade ~= 0 then
			setCounts()
		end
		--log("--- BrowserPageReadyHandler filter: MYFAME_GETBALANCE")
	elseif filter == WF.BUY_DYNAMIC_OBJECT then
		local strXmlData = KEP_EventDecodeString( event )

		local result = parseEventHeader(strXmlData, g_tblObjectData)
		MenuBringToFrontThis()

		if result == 0 then
			-- Item was purchased successfully
			sendItemsPurchasedEvent(m_trades[m_currTrade].output)
			fireUpdateHudEvent()
			updateBackpack()
		elseif result == 1 then
			-- Not Enough Rewards
			KEP_MessageBox("Not Enough Rewards")

		elseif result == 2 then
			-- Not Enough Credits
			KEP_MessageBox("Not Enough Credits")
		else
			KEP_MessageBox(tostring(g_tblObjectData.ResultDescription))
		end
	elseif filter == 105 then
		local returnData = KEP_EventDecodeString( event )
		-- parse through the clothing data to populate the inventory 
		ParseClothingData(returnData)

		-- update the UI 
		updateHighlight()
		updateItemContainer()
	end
end


function sendItemsPurchasedEvent(glid)
	log("--- sendItemsPurchasedEvent glid: ".. tostring(glid))
	local ev = KEP_EventCreate( ITEM_PURCHASED_EVENT )
	KEP_EventEncodeNumber(ev, 1)
	KEP_EventEncodeNumber( ev, glid)
	KEP_EventQueue( ev )
end

-- needed, don't remove 
function onConfirmatonResponse(event)
	local addTable = {}
	local newItem = deepCopy(m_trades[m_currTrade])

	newItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "ShopVendor"}
	table.insert(addTable, newItem)

	m_pendingTrade = true
	exchangeCredits(m_trades[m_currTrade].creditCost)
	m_pendingTrade = false
end

function exchangeRewards(cost)
	log("--- exchangeRewards")
	local m_zoneInstanceId = KEP_GetZoneInstanceId()
    local m_zoneType          = KEP_GetZoneIndexType()
    local address = GameGlobals.WEB_SITE_PREFIX..BUY_DYNAMIC_OBJECT_SUFFIX.."&credit=GPOINT&items=".. m_trades[m_currTrade].output .."&qnty=1"
    --local address =  GameGlobals.WEB_SITE_PREFIX.."kgp/buyGameItem.aspx?action=BuyGameItem&KEPPointId=GPOINT&zoneInstanceId="..m_zoneInstanceId.."&zoneType="..m_zoneType.."&amount="..cost

    makeWebCall(address, WF.BUY_DYNAMIC_OBJECT)
end

function exchangeCredits(cost)
	log("--- exchangeCredits")
	local m_zoneInstanceId = KEP_GetZoneInstanceId()
    local m_zoneType          = KEP_GetZoneIndexType()
    local address = GameGlobals.WEB_SITE_PREFIX..BUY_DYNAMIC_OBJECT_SUFFIX.."&credit=KPOINT&items=".. m_trades[m_currTrade].output .."&qnty=1"
    --local address =  GameGlobals.WEB_SITE_PREFIX.."kgp/buyGameItem.aspx?action=BuyGameItem&KEPPointId=KPOINT&zoneInstanceId="..m_zoneInstanceId.."&zoneType="..m_zoneType.."&amount="..cost

    makeWebCall(address, WF.BUY_DYNAMIC_OBJECT)
end

-- opens a confirmation menu when the user purchases an item using credits 
function buyItemCredits( item )
	MenuOpen("Checkout.xml")
	local ev = KEP_EventCreate( ITEM_INFO_EVENT )
	KEP_EventEncodeNumber( ev, 0 ) -- this does not matter for purchasing items
	KEP_EventEncodeNumber(ev, 1) -- num items
	KEP_EventEncodeNumber( ev, item.glid)
	KEP_EventEncodeNumber( ev, 1) --quantity of item
	KEP_EventEncodeString( ev, item.name ) -- name
	KEP_EventEncodeNumber( ev, item.price ) -- price
	KEP_EventQueue( ev )
end

function parseEventHeader(strXmlData, objectContainer)
	objectContainer.ReturnCode = parseDataByTag(strXmlData, "ReturnCode", "(%d+)", 0)
	objectContainer.ResultDescription = parseDataByTag(strXmlData, "ResultDescription", "(.-)", 0)
	return tonumber(objectContainer.ReturnCode)
end

function parseDataByTag(strEventData, strTag, strPattern, iStart)
	local strStartTag = "<" .. strTag .. ">"
	local strEndTag = "</" .. strTag .. ">"
	local strPattern = strStartTag..strPattern..strEndTag
	local retStart, retEnd, strData = string.find(strEventData, strPattern, iStart)
	return strData, retStart, retEnd
end

function fireUpdateHudEvent()
	KEP_EventCreateAndQueue( UPDATE_HUD_EVENT )
end

----------------------------
-- Local Functions
----------------------------

Dialog_OnLButtonDownInside = function(dialogHandle, x, y)
	local tradeClicked = false
	
	-- purchasing items with credits 
	if (Control_ContainsPoint(gHandles["btnBuyCredits"], x, y) == 1) and Control_GetEnabled(gHandles["btnBuyCredits"]) == 1 then
		local event = KEP_EventCreate("UPDATE_CREDIT_TRANSACTION")
		KEP_EventEncodeString(event, m_trades[m_currTrade].creditCost) -- cost of item in credits 
		KEP_EventEncodeString(event, m_playerName) -- player 
		KEP_EventEncodeString(event, "") -- world... remnants of Vendor
		KEP_EventEncodeString(event, "Framework_ClothingVendor") -- location of the event 
		KEP_EventEncodeString(event, m_trades[m_currTrade].name or "New Clothing Item") -- name of item 
		KEP_EventQueue(event)
		toggleMenu("CreditTransaction")
		m_pendingTrade = false
		return 
	end

	-- purchasing items with rewards 
	if (Control_ContainsPoint(gHandles["btnBuyRewards"], x, y) == 1) and Control_GetEnabled(gHandles["btnBuyRewards"]) == 1 then
		exchangeRewards(m_trades[m_currTrade].creditCost)
		return
	end

	-- If trade section active, don't close when clicking within this section
	if (Control_ContainsPoint(gHandles["imgPurchaseBG"], x, y) == 1) or (Control_ContainsPoint(gHandles["btnItemDrop1"], x, y) == 1) then
		if m_currTrade ~= 0 then
			tradeClicked = true
		end
	end

	for i=1, VENDOR_ITEMS_PER_PAGE do
		if (Control_ContainsPoint(gHandles["imgItem"..i], x, y) == 1) and m_trades[i] then
			tradeClicked = true
			m_currTrade = i + ((m_page-1) * VENDOR_ITEMS_PER_PAGE)
			m_selectedPage = m_page
		end
	end

	if (Control_ContainsPoint(gHandles["btnNext"], x, y) == 1) and Control_GetEnabled(gHandles["btnNext"]) == 1 then
		m_page = m_page + 1
		updateItemContainer()
		setCounts()
		
	end
	if (Control_ContainsPoint(gHandles["btnBack"], x, y) == 1) and Control_GetEnabled(gHandles["btnBack"]) == 1 then
		m_page = m_page - 1
		updateItemContainer()
		setCounts()
	end

	-- EDIE: look into this... 
	if tradeClicked then
		--m_trades[10] = deepCopy(m_trades[m_currTrade])
		--updateItemContainer()
	else
		--m_currTrade = 0
		Control_SetVisible(gHandles["btnItemDrop1"], true)
	end

	updateHighlight()
end

-- updates the location of the highlight 
updateHighlight = function ()

	-- checks to see if the first item is empty, and turs off the highlight if so
	if m_trades == nil or #m_trades < 1 or m_trades[1].output == 0 then
		Control_SetVisible(gHandles['imgHighlight'], false)
		return
	else
		Control_SetVisible(gHandles['imgHighlight'], true)
	end 

	if m_currTrade <= 0 then 
		return
	end 

	setCounts()

	updateItemContainer()

	-- Set input/output tooltip, image, nameplate, and cost.
	Control_SetVisible(gHandles["btnItemDrop1"], false)
	Control_SetVisible(gHandles["imgSellBG"], false)

	--log("--- m_currTrade ".. tostring(m_currTrade))

	if m_selectedPage == m_page then
		local highlightIndex = m_currTrade - ((m_page - 1) * VENDOR_ITEMS_PER_PAGE)
		local newX = Control_GetLocationX(gHandles["imgItemBG".. highlightIndex])
		local newY = Control_GetLocationY(gHandles["imgItemBG".. highlightIndex])
		Control_SetLocation(gHandles["imgHighlight"], newX, newY)
	else
		Control_SetVisible(gHandles['imgHighlight'], false)
	end
end

-- Displays all items inside the container and registers them with the drag / drop system.
updateItemContainer = function()

	m_displaying = true

	updatePage()
	local startIndex =  VENDOR_ITEMS_PER_PAGE * (m_page - 1) + 1
	local endIndex = VENDOR_ITEMS_PER_PAGE * m_page
	local displayIndex = 1
	for i = startIndex, endIndex do
		if m_trades[i] then
			g_displayTable[displayIndex] = deepCopy(m_trades[i])
		else
			g_displayTable[displayIndex] = nil
		end
		displayIndex = displayIndex + 1
	end

	if m_trades and m_trades[m_currTrade] then
		g_displayTable[10] = deepCopy(m_trades[m_currTrade]) 
	end

	if g_displayTable[1] then
		for i=1, ITEMS_PER_PAGE do

			-- updates the tooltips
			formatTooltips(g_displayTable[i])

			if i == 10 then
				applyImage(gHandles["imgItem"..i], m_trades[m_currTrade].output)
			elseif g_displayTable[i] then 
				applyImage(gHandles["imgItem"..i], g_displayTable[i].output)
			else
				applyImage(gHandles["imgItem".. i], 0)
			end 
		end

		InventoryHelper.setNameplates("name", "hide")
		--InventoryHelper.setQuantities("count")
	else
		for i=1, ITEMS_PER_PAGE do
			applyImage(gHandles["imgItem".. i], 0)
		end
	end
end

-- Sets the counts for the currently selected item
setCounts = function()
	

	-- Set required item count statics
	local hasNuf = true
	local color
	local offset = 12

	-- the costs in rewards and credits 
	local rewardCost = 0
	local creditCost = 0
	local inventory_type = ""

	if m_trades and m_trades[m_currTrade] ~= nil then
		if m_trades[m_currTrade].rewardCost then rewardCost = m_trades[m_currTrade].rewardCost end
		if m_trades[m_currTrade].creditCost then creditCost = m_trades[m_currTrade].creditCost end
		if m_trades[m_currTrade].inventory_type then inventory_type = m_trades[m_currTrade].inventory_type end
	end 

	-- set the labels 
	Static_SetText(gHandles["txtCountRewards"], formatNumberSuffix(rewardCost))
	Static_SetText(gHandles["txtCountCredits"], formatNumberSuffix(creditCost))
	Static_SetText(gHandles["txtBackpackCountRewards"], "You have "..formatNumberSuffix(g_rewards))
	Static_SetText(gHandles["txtBackpackCountCredits"], "You have "..formatNumberSuffix(g_credits))

	-- make sure the images are visible 
	Control_SetVisible(gHandles["imgRewards"], true)
	Control_SetVisible(gHandles["txtRewards"], true)
	Control_SetVisible(gHandles["imgCredits"], true)
	Control_SetVisible(gHandles["txtCredits"], true)
	Control_SetEnabled(gHandles["imgItemOverlay10"], false)
	Control_SetVisible(gHandles["imgItemOverlay10"], false)
	
	-----------------------------
	-- REWARDS 
	-----------------------------

	-- Set the have color to red if you don't got nuf rewards 
	--local stcElementRewardCount = 
	Control_SetEnabled(gHandles["btnBuyRewards"], false)
	if tonumber(rewardCost) > tonumber(g_rewards) and (inventory_type == INVENTORY_TYPE_BOTH or inventory_type == INVENTORY_TYPE_REWARDS) then
		-- Set color to red
		hasNuf = false
		color = {a=255,r=255,g=0,b=0}
	elseif not (inventory_type == INVENTORY_TYPE_BOTH or inventory_type == INVENTORY_TYPE_REWARDS) then 
		-- set the color to grey 
		color = {a = 100, r = 100, g = 100, b = 100}
		Static_SetText(gHandles["txtCountRewards"], "N/A")
	else
		-- Set color to white, we can buy stuff 
		color = {a=255,r=255,g=255,b=255}
		Control_SetEnabled(gHandles["btnBuyRewards"], true)
	end	
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["txtCountRewards"])), 0, color)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["txtRewards"])), 0, color)

	-----------------------------
	-- CREDITS
	-----------------------------

	-- Set the have color to red if you don't got nuf credits
	Control_SetEnabled(gHandles["btnBuyCredits"], true)
	color = {a=255,r=255,g=255,b=255}
	if tonumber(creditCost) > tonumber(g_credits) and (inventory_type == INVENTORY_TYPE_BOTH or inventory_type == INVENTORY_TYPE_CREDITS) then
		-- Set color to red
		color = {a=255,r=255,g=0,b=0}
	elseif not (inventory_type == INVENTORY_TYPE_BOTH or inventory_type == INVENTORY_TYPE_CREDITS) then 
		-- set the color to grey 
		color = {a = 100, r = 100, g = 100, b = 100}
		Static_SetText(gHandles["txtCountCredits"], "N/A")
		Control_SetEnabled(gHandles["btnBuyCredits"], false)
	end	

	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["txtCountCredits"])), 0, color)
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["txtCredits"])), 0, color)

	m_validTrade = hasNuf

	return hasNuf
end

-- makes the number shorter to read -_-, which needs to be moved elsewhere 
function formatNumberSuffix(count)
	if count == nil then
		return "N/A"
	end 

	count = tonumber(count)

	-- If no need for formatting, then return
	if count < 10000 then return count end

	local strCount = tostring(count)
	local newCount = ""

	local format = { 	max = 0,
						min = 0,
					 	suffix = ""
					}

	-- Thousands
	if #strCount <= 6 then
		format.max = 6
		format.min = 3
		format.suffix = "K"
	-- Millions
	elseif #strCount <= 9 then
		format.max = 9
		format.min = 6
		format.suffix = "M"
	-- Billions
	elseif #strCount <= 12 then
		format.max = 12
		format.min = 9
		format.suffix = "B"
	-- Trillions
	elseif #strCount <= 15 then
		format.max = 15
		format.min = 12
		format.suffix = "T"
	end

	for i = 1, #strCount do
		local diff = #strCount - i
		if #strCount <= format.max then
			if diff >= format.min then
				newCount = newCount..string.sub(strCount,i,i)
			else
				return newCount..format.suffix
			end
		end
	end
end



-- Get initial location of purchase input/cost objects in menu
getInputLocations = function()
	for i,v in pairs(m_inputLocations) do
		m_inputLocations[i].x 			= Control_GetLocationX(gHandles[i])
		m_inputLocations[i].y 			= Control_GetLocationY(gHandles[i])
	end
end


makeTrade = function()
	local addTable = {}
	local newItem = deepCopy(m_trades[m_currTrade])
	newItem.lootInfo = {sourcePID = m_vendorPID, lootSource = "ShopVendor"}
	table.insert(addTable, newItem)
	processTransaction({}, addTable)
	requestCreditsRewards()
	updateItemContainer()
	--updateInventory()	
end

-- prints the table (use for debugging)
function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end

function moveElementsUp( )
	local moveHeight = 40
	for i = 1, VENDOR_ITEMS_PER_PAGE do
		Control_SetLocationY(gHandles["imgItemBG"..i], Control_GetLocationY(gHandles["imgItemBG"..i]) - moveHeight)
		Control_SetLocationY(gHandles["imgItem"..i], Control_GetLocationY(gHandles["imgItem"..i]) - moveHeight)
		Control_SetLocationY(gHandles["imgItemName"..i], Control_GetLocationY(gHandles["imgItemName"..i]) - moveHeight)
		Control_SetLocationY(gHandles["stcItem"..i], Control_GetLocationY(gHandles["stcItem"..i]) - moveHeight)
		Control_SetLocationY(gHandles["imgItemOverlay"..i], Control_GetLocationY(gHandles["imgItemOverlay"..i]) - moveHeight)
		Control_SetLocationY(gHandles["btnItem"..i], Control_GetLocationY(gHandles["btnItem"..i]) - moveHeight)
	end
end

function updatePage()

	local itemCount = #m_trades
	local btnNext = gHandles["btnNext"]
	local btnBack = gHandles["btnBack"]
	local stcPage = gHandles["stcPage"]

	if btnNext == nil then return end

	local maxpage = math.ceil(itemCount / VENDOR_ITEMS_PER_PAGE)

	local showArrows = maxpage > 1
	local showNext = (showArrows) and (m_page ~= maxpage)
	local showBack = (showArrows) and (m_page ~= 1)
	local showText = showArrows

	
	Control_SetVisible(stcPage, showText)
	Control_SetVisible(btnBack, showBack)
	Control_SetVisible(btnNext, showNext)
	Control_SetEnabled(btnBack, showBack)
	Control_SetEnabled(btnNext, showNext)
	
	Static_SetText(gHandles["stcPage"], "("..tostring(m_page).."/"..tostring(maxpage)..")")
end