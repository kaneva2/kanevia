--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_TopRightHUD.lua
-------------------------------------------------------------------------------

--------------
-- INCLUDES --
--------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\MenuAnimation.lua")
dofile("Framework_InventoryHelper.lua")

-----------------
-- STATIC VARS --
-----------------

local MENU_SIZE = {x = 320, y = 400}
local TOP_ICON_OFFSET = 80
local MARGIN_WIDTH = 17

local DEFAULT_MEMBER_LIST_Y = -40
-- Anchor point for attaching properties
local LIST_ANCHOR_POINT = {x = 11, y = 136}
local MAX_MEMBER_COUNT = 5
-- List height properties
local LIST_ITEM_HEIGHT = 30
local LIST_ITEM_WIDTH = MENU_SIZE.x - 2*MARGIN_WIDTH
local SCROLL_BAR_WIDTH = 16
local LIST_HEADER_HEIGHT = 20
local LIST_HEADER_WIDTH = MENU_SIZE.x - MARGIN_WIDTH - SCROLL_BAR_WIDTH - 1
local LIST_BOX_HEIGHT = LIST_HEADER_HEIGHT*2 + LIST_ITEM_HEIGHT*6
local SOFT_PADDING = 4
local TEXT_SPACING = 2
local ADD_BUTTON_WIDTH = 20 -- Also remove button width
local BUTTON_BUFFER = (LIST_ITEM_HEIGHT - ADD_BUTTON_WIDTH) / 2
-- Scrolling list properties
local DEFAULT_RAVE_LIST_MIN = 1
local DEFAULT_RAVE_LIST_MAX = 6
local LIST_HEADER_COUNT = 2
local SCROLL_BUTTON_SIZE = 16
local SCROLL_BAR_HEIGHT = LIST_BOX_HEIGHT - 2*SCROLL_BUTTON_SIZE
local SCROLL_ANCHOR_POINT = {x = 127, y = -40}
local SCROLL_BAR_MIN_HEIGHT = 8
local RAVE_BUTTON_HEIGHT = 32
local CLOSE_BUTTON_HEIGHT = 24
local RAVE_LABEL_HEIGHT = 14
local BELOW_LIST_HEIGHT = 58
local LOWER_BG_HEIGHT = 40
local ARROW_BTN_BUFFER = 5

local MAX_GROUPNAME_LEN = 30

local COLORS = { -- frequently used colors
    WHITE = {a=255, r=255, g=255, b=255},
    BLACK = {a=255, r=0, g=0, b=0},
    -- AQUA = {a=255, r=0, g=246, b=255},
    -- GREY = {a=255, r=80, g=80, b=80},
    -- HEAD = {a=255, r=220, g=220, b=220},
    -- SUB = {a=255, r=220, g=220, b=220},
    GOOD = {a=255, r=21, g=169, b=39},
    BAD = {a=255, r=239, g=72, b=72},
}

local DAYS_PER_WEEK = 7
local DAYS_PER_MONTH = 30

local SEC_PER_MIN = 60
local SEC_PER_HOUR = SEC_PER_MIN * 60
local SEC_PER_DAY = SEC_PER_HOUR * 24
local SEC_PER_WEEK = DAYS_PER_WEEK * SEC_PER_DAY
local SEC_PER_MONTH = 2629743
local SEC_PER_YEAR = 31556926

local DEFAULT_FEE_INTERVAL = 2 * DAYS_PER_WEEK * SEC_PER_DAY
local FEE_BUFFER_CNT = 2

----------------
-- LOCAL VARS --
----------------

-- Game item cache
local m_initGameItemCache = false
local m_gameItems = {}
local m_initPlayerInventory = false
local m_playerInventoryByUNID = {}
local m_playerInventory = {}
local m_inventoryCounts = {}

local m_initMenuElements = false
local m_flagGroupInfoInit = false

-- Scroll bar
local m_scrollAmtPending = nil -- Scroll bar position distance modifier
local m_scrollIncrement = 0
local m_barScrolling = false -- Is the bar scrolling?
local m_scrollPrevPos = nil -- Scroll bar previous position

-- GUI properties
local m_memberCollapsed = false
local m_raveCollapsed = false
local m_addButtons = {}
local m_removeButtons = {}
local m_memberGreyBars = {}
local m_raveGreyBars = {}

local m_memberNames = {}
local m_raveNames = {}
local m_pendingRaveName = ""

local m_raveListMin = DEFAULT_RAVE_LIST_MIN
local m_raveListMax = DEFAULT_RAVE_LIST_MAX
local m_memberListMin = 0
local m_memberListMax = MAX_MEMBER_COUNT
local m_scrollOffset = 0
local m_scrollOffsetMax = 0

-- Flag properties
local m_flagInfo = {}
-- flagInfo.ravers = {}
m_flagInfo.members = {} -- name, membership boolean
m_flagInfo.ravers = {}
m_flagInfo.raveCount = 0
local m_isOwner = false
-- Collapsable categories list 
local m_categoryList = {}

local m_zoneInstanceId = 0
local m_zoneType       = 0
-- PID???

local RAVE_RESPONSES = {
	SUCCESS = 0,
	ALREADY_RAVED = -1,
	ERROR = -2,
	UNKNOWN_ERROR = -3
}

local m_closePending = false

local m_currTime = 0

--------------------
-- CORE FUNCTIONS --
--------------------

-- Called when the menu is created
function onCreate()
	-- Event handlers
	Events.registerHandler("FRAMEWORK_CLAIM_FLAG_STATUS_UPDATE", buildFlagStatusUpdateHandler)
	Events.registerHandler("RETURN_CURRENT_TIME", onReturnCurrentTime)

	KEP_EventRegisterHandler( "initiateMenuCloseHandler", "FLAG_GROUP_INFO_CLOSE", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "initializeFlagGroupInfo", "INITIALIZE_FLAG_GROUP_INFO", KEP.MED_PRIO )

	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	m_zoneInstanceId = tostring(KEP_GetZoneInstanceId())
	m_zoneType       = tostring(KEP_GetZoneIndexType())

	requestInventory(GAME_PID)
	requestInventory(INVENTORY_PID)

	Events.sendEvent("FRAMEWORK_REQUEST_CURRENT_TIME", {playerName = KEP_GetLoginName(), PID = m_containerPID})

	-- Register buttons
	initializeButtons() -- wait for game item cache, inventory

	MenuCenterThis()
end

-- Called each frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_initPlayerInventory and m_initGameItemCache and m_flagGroupInfoInit and not m_initMenuElements then
		updateDisplay()
	end

	if m_initMenuElements and m_barScrolling and m_scrollAmtPending then
		if m_scrollAmtPending < 0 then
			scrollDownSingle()
		else
			scrollUpSingle()
		end
	end
end

initiateMenuClose = function()
	 m_closePending = true
	-- Save off values to associated flag (send via event)
	saveFlagProperties()
end

--------------------
-- EVENT HANDLERS --
--------------------

function buildFlagStatusUpdateHandler(event)
	if m_flagInfo.PID ~= nil then
		if event.buildFlag == false and event.flagPID == m_flagInfo.PID then
			initiateMenuClose()
		end
	end
end

function initializeFlagGroupInfo(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_flagInfo = {}

	m_flagInfo.PID = tonumber(KEP_EventDecodeString(tEvent))
	m_flagInfo.UNID = tonumber(KEP_EventDecodeString(tEvent))
	m_flagInfo.groupName = KEP_EventDecodeString(tEvent)

	local tempRavers = KEP_EventDecodeString(tEvent)
	local raversToAdd = Events.decode(tempRavers)
	m_flagInfo.ravers = {}
	for i,v in pairs(raversToAdd) do
		if v ~= nil then
			table.insert(m_flagInfo.ravers, v)
		end
	end

	m_flagInfo.raveCount = tableCount(m_flagInfo.ravers)

	local tempMembers = KEP_EventDecodeString(tEvent)
	local membersToAdd = Events.decode(tempMembers)
	m_flagInfo.members = {}
	for i,v in pairs(membersToAdd) do
		if v ~= nil then
			table.insert(m_flagInfo.members, v)
		end
	end
		
	m_flagInfo.owner = KEP_EventDecodeString(tEvent)
	m_isOwner = m_flagInfo.owner == tostring(KEP_GetLoginName())

	-- Add the owner to the member list
	if tableCount(m_flagInfo.members) == 0 then
		table.insert(m_flagInfo.members, m_flagInfo.owner)
		if m_isOwner then
			initializeCollapsibleLists()
			initializeAddRemove()
		end
	end

	table.sort(m_flagInfo.members, memberSort)
	table.sort(m_memberNames, memberSort)

	m_flagInfo.isFeeDue = KEP_EventDecodeString(tEvent) == "true"
	m_flagInfo.feeDueDate = KEP_EventDecodeNumber(tEvent)

	if m_flagGroupInfoInit == false then
		m_flagGroupInfoInit = true
	else
		updateDueDate()
		updateFeePayCtrls()
	end
end

function initiateMenuCloseHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	initiateMenuClose()
end

function onReturnCurrentTime(event)
	m_currTime = event.timestamp
	updateDueDate()
	updateFeePayCtrls()
end

---------------------
-- GAME ITEM CACHE --
---------------------

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems

	m_initGameItemCache = true
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)

	if m_playerInventory[updateIndex] then
		m_playerInventory[updateIndex] = updateItem
		m_playerInventoryByUNID[updateItem.UNID] = updateItem.count
		updateInventory()
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_playerInventory = decompileInventory(tEvent)

	for i=1,#m_playerInventory do
		m_playerInventoryByUNID[tostring(m_playerInventory[i].UNID)] = m_playerInventory[i].count
	end

	updateInventory()

	m_initPlayerInventory = true
end

function updateInventory()
	m_inventoryCounts = {}
	-- Compile UNID counts
	for i=1, #m_playerInventory do
		if m_playerInventory[i].UNID ~= 0 then
			-- New UNID entry
			if m_inventoryCounts[m_playerInventory[i].UNID] == nil then
				m_inventoryCounts[m_playerInventory[i].UNID] = m_playerInventory[i].count
			-- Duplicate entry. Increment UNID count
			else
				m_inventoryCounts[m_playerInventory[i].UNID] = m_inventoryCounts[m_playerInventory[i].UNID] + m_playerInventory[i].count
			end
		end
	end

	-- Update overlays
	-- setPurchasableOverlays()
end

------------------
-- GUI HANDLERS --
------------------

function btnScrollBar_OnLButtonDown(buttonHandle, x, y)
	m_barScrolling = true
	m_scrollPrevPos = y
end

function btnScrollBar_OnLButtonUp(controlHandle, x, y)
	m_scrollAmtPending = nil
	m_scrollPrevPos = nil
	m_barScrolling = false
	updateScrollBar()
end

function btnScrollBar_OnMouseLeave(controlHandle)
	m_scrollPrevPos = nil
	m_barScrolling = false
	updateScrollBar()
end

function Dialog_OnMoved()
    MenuCenterThis()
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	--If scrolling, then update list
	if m_barScrolling then

		local m_scrollAmtPending = 0
		if m_scrollPrevPos then
			m_scrollAmtPending = math.floor(m_scrollPrevPos - y)
		end
		if m_scrollAmtPending < 0 and y > Control_GetLocationY(gHandles["imgScrollBar"]) + Control_GetHeight(gHandles["imgScrollBar"])/2 then
			scrollDownSingle()
		elseif m_scrollAmtPending > 0 and y < Control_GetLocationY(gHandles["imgScrollBar"]) + Control_GetHeight(gHandles["imgScrollBar"])/2 then
			scrollUpSingle()
		end

		m_scrollPrevPos = y
	end
end

function btnScrollUpArrow_OnLButtonDown(buttonHandle)
	m_barScrolling = true
	m_scrollAmtPending = 1
end

function btnScrollUpArrow_OnLButtonUp(controlHandle, x, y)
	m_barScrolling = nil
	m_scrollAmtPending = nil
end

function btnScrollUpArrow_OnMouseLeave(controlHandle)
	m_barScrolling = nil
	m_scrollAmtPending = nil
end

function btnScrollDownArrow_OnLButtonDown(buttonHandle)
	m_barScrolling = true
	m_scrollAmtPending = -1
end

function btnScrollDownArrow_OnLButtonUp(controlHandle, x, y)
	m_barScrolling = nil
	m_scrollAmtPending = nil
end

function btnScrollDownArrow_OnMouseLeave(controlHandle)
	m_barScrolling = nil
	m_scrollAmtPending = nil
end

function btnEditGroupName_OnButtonClicked(buttonHandle)
	local handle = gHandles["edGroupName"]
	Control_SetEnabled( handle, true )
	Control_SetFocus( handle )
	Dialog_SetModal(gDialogHandle, true)
end

function edGroupName_OnFocusOut(controlHandle)
	local handle = gHandles["edGroupName"]
	Control_SetEnabled(handle, false)
	Dialog_SetModal(gDialogHandle, false)
	updateDisplay()
end

function btnEditGroupName_OnButtonClicked(buttonHandle)
	local handle = gHandles["edGroupName"]
	Control_SetEnabled( handle, true )
	Control_SetFocus( handle )
	Dialog_SetModal(gDialogHandle, true)
end

function edGroupName_OnFocusOut(controlHandle)
	local handle = gHandles["edGroupName"]
	Control_SetEnabled(handle, false)
	Dialog_SetModal(gDialogHandle, false)
	updateDisplay()
end

-- Called when the editbox containing an item's name has been changed
function edGroupName_OnEditBoxChange(editBoxHandle, editBoxText)
	-- Enforce a maximum length
	local newValue = editBoxText
	if string.len(newValue) > MAX_GROUPNAME_LEN then
		newValue = string.sub(newValue, 0, MAX_GROUPNAME_LEN)
		EditBox_SetText(gHandles["edGroupName"], newValue, false)
	end

	-- If the name is different from the one originally displayed
	if editBoxText ~= m_flagInfo.name then
		-- New group name
		m_flagInfo.groupName = editBoxText
		m_nameChanged = true
	else
		m_nameChanged = false
	end
end

initializeButtons = function()
	-- Close button
	_G["btnClose_OnButtonClicked"] = function(btnHandle)
		initiateMenuClose()
	end
	-- Rave button
	_G["btnRave_OnButtonClicked"] = function(btnHandle)
		raveFlag(KEP_GetLoginName())
	end
	-- Collapse members button
	_G["btnMemberCollapse_OnButtonClicked"] = function(btnHandle)
		collapseMemberList()
	end
	-- Expand members button
	_G["btnMemberExpand_OnButtonClicked"] = function(btnHandle)
		expandMemberList()
	end
	-- Collapse raved button
	_G["btnRaveCollapse_OnButtonClicked"] = function(btnHandle)
		collapseRaveList()
	end
	-- Expand raved button
	_G["btnRaveExpand_OnButtonClicked"] = function(btnHandle)
		expandRaveList()
	end
	-- Scroll up
	_G["btnScrollUpArrow_OnButtonClicked"] = function(btnHandle)
		scrollUpSingle()
	end
	-- Scroll down
	_G["btnScrollDownArrow_OnButtonClicked"] = function(btnHandle)
		scrollDownSingle()
	end
end

-- LOCAL FUNCTIONS

-- INITIALIZE

initializeCollapsibleLists = function()
	toggleMemberView(true)
	m_memberCollapsed = false
	toggleRaveView(true)
	m_raveCollapsed = false
end

initializeAddRemove = function()

	local addButtonIndex = 0
	for i,v in pairs(m_flagInfo.ravers) do
		if v and v ~= "" then
			addAddButton(addButtonIndex, "Rave")
			addButtonIndex = addButtonIndex + 1
		end
	end

	local removeButtonIndex = 0
	for i,v in pairs(m_flagInfo.members) do
		if v and v ~= "" then
			addRemoveButton(removeButtonIndex, "Member")
			removeButtonIndex = removeButtonIndex + 1
		end
	end

end

-- UPDATE

-- -- Updates GUI elements. Do NOT call before initializeFlagGroupInfo has received m_flagInfo data.
updateDisplay = function()
	toggleOwnerElements(m_isOwner)

	local tempName = tostring(m_flagInfo.groupName)
	if tempName == "" or tempName == " " then
		tempName = m_flagInfo.owner.." Place"
	end
	Static_SetText(gHandles["stcGroupName"], tempName)
	EditBox_SetText(gHandles["edGroupName"], m_flagInfo.groupName, false)
	if m_isOwner then
		Static_SetText(gHandles["stcOwner"], "Owned by you")
	else
		Static_SetText(gHandles["stcOwner"], "Owned by "..m_flagInfo.owner)
	end

	Static_SetText(gHandles["stcRaves"], tostring(m_flagInfo.raveCount).." Raves")
	Static_SetText(gHandles["stcMemberCount"], tostring(tableCount(m_flagInfo.members)))
	if tableCount(m_flagInfo.members) >= MAX_MEMBER_COUNT then
		local color = { a=255, r=255, g=0, b=0}
		BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["stcMemberCount"])), 0, color)
	else
		local color = { a=255, r=255, g=255, b=255}
		BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["stcMemberCount"])), 0, color)
	end

	if m_isOwner then
		updateScrollOffset()
		updateScrollBar()
		updateMemberList()
		updateRaveList()
	end
	updateGreyBars()
	updateNameLabels()

	-- Maintenance elements for owners and members
	if m_isOwner or isAMember(KEP_GetLoginName()) then
		local flagItem, requiredItem
		flagItem = m_gameItems[tostring(m_flagInfo.UNID)]
		if flagItem and flagItem.behaviorParams then
			if flagItem.behaviorParams.feesEnabled and flagItem.behaviorParams.feesEnabled == true then
		
				-- Hide rave button
				setVisibleAndEnabled(gHandles["stcRaveMe"], false)
				setVisibleAndEnabled(gHandles["btnRave"], false)

				-- Enable maintenance controls
				Control_SetVisible(gHandles["stcMainFees"], true)
				Control_SetVisible(gHandles["imgFeeDiv"], true)
				Control_SetVisible(gHandles["imgFeeItemBG"], true)
				Control_SetVisible(gHandles["imgFeeItem"], true)
				Control_SetVisible(gHandles["stcFeeName"], true)
				Control_SetVisible(gHandles["stcFeeCount"], true)
				Control_SetVisible(gHandles["stcFeeDueDate"], true)
				Control_SetVisible(gHandles["stcMainFees"], true)
				-- Enable pay button if ready/able to pay
				Control_SetVisible(gHandles["btnPayFee"], true)
				-- Control_SetEnabled(gHandles["btnPayFee"], true)

				-- Get anchor point
				local insertX = Control_GetLocationX(gHandles["imgListBox"])
				local insertY = Control_GetLocationY(gHandles["imgListBox"]) + Control_GetHeight(gHandles["imgListBox"]) + 10
				local insertYStart = insertY
				-- Position maintenance fee title
				Control_SetLocation(gHandles["stcMainFees"], insertX, insertY)
				insertY = insertY + Control_GetHeight(gHandles["stcMainFees"]) + 4
				-- Position divider line
				Control_SetLocation(gHandles["imgFeeDiv"], insertX, insertY)
				insertY = insertY + 10

				updateFeePayCtrls()

				-- Position image of payment item
				Control_SetLocation(gHandles["imgFeeItemBG"], insertX, insertY)
				Control_SetLocation(gHandles["imgFeeItem"], insertX + 1, insertY + 1)


				--local tempInsertX = Control_GetLocationX(gHandles["imgBG"]) + MENU_SIZE.x - Control_GetWidth(gHandles["btnPayFee"]) - 25
				--local tempYBuffer = Control_GetHeight(gHandles["stcFeeName"]) + Control_GetHeight(gHandles["stcFeeCount"]) + Control_GetHeight(gHandles["stcFeeDueDate"]) + 4
				--local tempInsertY = insertY + tempYBuffer - Control_GetHeight(gHandles["btnPayFee"]) -- + Control_GetHeight(gHandles["imgFeeItemBG"])
					
				-- Position payment button
				--Control_SetLocation(gHandles["btnPayFee"], tempInsertX, tempInsertY)

				_G["btnPayFee_OnButtonClicked"] = function(btnHandle)
					btnPayFeeHandler()
				end

				insertX = insertX + Control_GetWidth(gHandles["imgFeeItemBG"]) + 10
				-- Position payment item name
				Control_SetLocation(gHandles["stcFeeName"], insertX, insertY)
				insertY = insertY + Control_GetHeight(gHandles["stcFeeName"]) + 2
				-- Position payment item count
				Control_SetLocation(gHandles["stcFeeCount"], insertX, insertY)
				insertY = insertY + Control_GetHeight(gHandles["stcFeeCount"]) 
				-- Position due date
				Control_SetLocation(gHandles["stcFeeDueDate"], insertX, insertY)
				updateDueDate()
				insertY = insertY + Control_GetHeight(gHandles["stcFeeDueDate"]) + 5

				-- Update bottom panel of menu to fit
				local menuGrowSizeY = insertY - insertYStart

				Control_SetSize(gHandles["imgBelowList"], Control_GetWidth(gHandles["imgBelowList"]), LIST_ITEM_HEIGHT + menuGrowSizeY)
				Control_SetLocationY(gHandles["imgBGLower"], 	insertY + SOFT_PADDING)	

				--position close button
				local tempInsertY = insertY + SOFT_PADDING + LOWER_BG_HEIGHT/2 - CLOSE_BUTTON_HEIGHT/2
				local tempInsertX =  Control_GetLocationX(gHandles["imgBG"])+MENU_SIZE.x/2

				Control_SetLocation(gHandles["btnClose"], tempInsertX, tempInsertY)
				

				--position pay button
				tempInsertX = Control_GetLocationX(gHandles["btnClose"]) - Control_GetWidth(gHandles["btnPayFee"]) - 3
				tempInsertY = Control_GetLocationY(gHandles["btnClose"])
				Control_SetLocation(gHandles["btnPayFee"], tempInsertX, tempInsertY)

				--repostion 
				Control_SetSize(gHandles["imgBG"], MENU_SIZE.x, Control_GetHeight(gHandles["imgBG"]) + menuGrowSizeY - 10)

				MenuCenterThis()
			end
		end
	else
		-- Update "Rave" status
		if checkForEntry(m_flagInfo.ravers, KEP_GetLoginName()) then
			Static_SetText(gHandles["stcRaveMe"], "Already Raved")
			Control_SetEnabled(gHandles["btnRave"], false)
		end
	end

	m_initMenuElements = true
end

function btnPayFeeHandler()
	local flagItem, requiredItem
	flagItem = m_gameItems[tostring(m_flagInfo.UNID)]
	if flagItem then
		local requiredItem = m_gameItems[tostring(flagItem.behaviorParams.feeObject)]
		if requiredItem then
			local removeTable = {}
			local addTable = {}
			local newItem = {UNID = flagItem.behaviorParams.feeObject, count = flagItem.behaviorParams.feeCount, properties = requiredItem}
			table.insert(removeTable, newItem)
			processTransaction(removeTable, addTable) -- TO DO: Check that a payment is necessary

			Events.sendEvent("FLAG_FEE_PAYMENT", {PID = m_flagInfo.PID})
		end
	end
end

function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end

	updateFeePayCtrls()
end

updateScrollOffset = function()
	m_scrollOffsetMax = 0
	if m_raveCollapsed == false then
		m_scrollOffsetMax = m_scrollOffsetMax + tableCount(m_flagInfo.ravers)
	end

	if m_memberCollapsed == false then
		m_scrollOffsetMax = m_scrollOffsetMax + tableCount(m_flagInfo.members)
	end

	-- If there are are enough ravers, then the range of the scrollable list increases
	local variableBuffer = 0
	if tableCount(m_flagInfo.ravers) > DEFAULT_RAVE_LIST_MAX + 1 then
		variableBuffer = 1
	end

	m_scrollOffsetMax = math.max(0, m_scrollOffsetMax - DEFAULT_RAVE_LIST_MAX + variableBuffer)

	if math.abs(m_scrollOffset) > math.abs(m_scrollOffsetMax) then
		m_scrollOffset = -1*m_scrollOffsetMax
	end	
end

updateScrollBar = function()
	-- Scroll bar height range: 1 - SCROLL_BAR_HEIGHT
	-- Size is relative to the number of clicks
	-- Divide height by number of clicks
	-- Height Ratio = height / # clicks
	-- Height = height * height ratio = height * (height/clicks)
	-- Adjust # per click
	local maxScrollSize = 0
	if not m_raveCollapsed then
		maxScrollSize = math.abs(m_scrollOffsetMax)
	end
	m_scrollIncrement = math.min(SCROLL_BAR_HEIGHT / maxScrollSize, SCROLL_BAR_MIN_HEIGHT) -- Scroll amount of a single click
	local scaledBarHeight = math.max(SCROLL_BAR_HEIGHT - m_scrollIncrement*maxScrollSize, SCROLL_BAR_MIN_HEIGHT)
	local anchorPoint = {x = Control_GetLocationX(gHandles["btnMemberCollapse"]) + LIST_HEADER_WIDTH - SCROLL_BUTTON_SIZE, y = Control_GetLocationY(gHandles["btnMemberCollapse"]) + SCROLL_BUTTON_SIZE}
	local barPosX = anchorPoint.x
	local barPosY = anchorPoint.y + math.floor((math.abs(m_scrollOffset) * m_scrollIncrement))
	-- Close the gap for the last position
	if math.abs(m_scrollOffset) == math.abs(m_scrollOffsetMax) then
		barPosY = anchorPoint.y + SCROLL_BAR_HEIGHT - scaledBarHeight
	end

	Control_SetSize(gHandles["imgScrollBar"], SCROLL_BAR_WIDTH, scaledBarHeight)
	Control_SetLocation(gHandles["imgScrollBar"], barPosX, barPosY)
	Control_SetSize(gHandles["imgScrollBG"], SCROLL_BAR_WIDTH, SCROLL_BAR_HEIGHT + SCROLL_BUTTON_SIZE)
	Control_SetLocation(gHandles["imgScrollBG"], anchorPoint.x, anchorPoint.y)
	Control_SetLocation(gHandles["btnScrollUpArrow"], anchorPoint.x, anchorPoint.y - SCROLL_BUTTON_SIZE)
	Control_SetLocation(gHandles["btnScrollDownArrow"], anchorPoint.x, anchorPoint.y + SCROLL_BAR_HEIGHT)

	-- Make button fatter while scrolling
	if m_barScrolling then
		Control_SetSize(gHandles["btnScrollBar"], SCROLL_BAR_WIDTH*2, SCROLL_BAR_HEIGHT)
		Control_SetLocation(gHandles["btnScrollBar"], anchorPoint.x - SCROLL_BAR_WIDTH/4, anchorPoint.y)
	else
		Control_SetSize(gHandles["btnScrollBar"], SCROLL_BAR_WIDTH, SCROLL_BAR_HEIGHT)
		Control_SetLocation(gHandles["btnScrollBar"], anchorPoint.x, anchorPoint.y)
	end
end

updateRaveList = function()

	if m_flagInfo.ravers then
		table.sort(m_flagInfo.ravers, customSort)
		table.sort(m_raveNames, customSort)
	end
	updateRaveListMinMax()
	updateAddRemove()

	updateRaveListPosition()
end

function customSort(a, b)
	return string.upper(a) < string.upper(b)
end

updateMemberList = function()

	if m_flagInfo.ravers and m_flagInfo.owner then
		table.sort(m_flagInfo.members, memberSort)
		table.sort(m_memberNames, memberSort)
	end

	updateMemberListPosition()

end

function memberSort(a, b)
	return string.upper(sortCheckIfOwner(a)) < string.upper(sortCheckIfOwner(b))
end

sortCheckIfOwner = function(inputString)
	if inputString == m_flagInfo.owner then
		return ""
	else
		return inputString
	end
end

updateNameLabels = function()
	-- Owner version
	if m_isOwner then
		for i,v in pairs(m_flagInfo.ravers) do
			if m_raveNames[i-1] == nil then
				addNameLabel(i-1, m_raveNames, "Rave", m_flagInfo.ravers[i])
			end
		end
		
		for i,v in pairs(m_flagInfo.members) do
			if m_memberNames[i-1] == nil then
				addNameLabel(i-1, m_memberNames, "Member", m_flagInfo.members[i])
			end
		end

		for i,v in pairs(m_raveNames) do
			-- If corresponding list item does not exist anymore, remove self
			if m_flagInfo.ravers[i+1] == nil then
				if tableCount(m_raveNames) == 1 then
					m_raveNames = {} -- Can't remove the last element of the table, so reset it
				else
					table.remove(m_raveNames, i)
				end
				Dialog_RemoveControl(gDialogHandle, v)
			else
				local buttonOffset = 0
				if m_memberCollapsed and m_scrollOffset < -1 * LIST_HEADER_COUNT then
					buttonOffset = m_scrollOffset + LIST_HEADER_COUNT
				elseif not m_memberCollapsed and m_scrollOffset < -1 * tableCount(m_flagInfo.members) - 1 * LIST_HEADER_COUNT then
					buttonOffset = m_scrollOffset + tableCount(m_flagInfo.members) + LIST_HEADER_COUNT
				end
				-- If section collapsed or outside the display range, hide self
				if m_raveCollapsed or i+1 < m_raveListMin - buttonOffset or i+1 > m_raveListMax - buttonOffset then
					setVisibleAndEnabled(gHandles[v], false)
				else
					Static_SetText(gHandles[v], m_flagInfo.ravers[i+1])
					setVisibleAndEnabled(gHandles[v], true)
				end
			end
		end

		for i,v in pairs(m_memberNames) do
			if m_flagInfo.members[i+1] == nil then
				if tableCount(m_memberNames) == 1 then
					m_memberNames = {} -- Can't remove the last element of the table, so reset it
				else
					table.remove(m_memberNames, i)
				end
				Dialog_RemoveControl(gDialogHandle, v)
			else
				-- If section collapsed
				if m_memberCollapsed or i+1 < m_memberListMin - m_scrollOffset or i+1 > m_memberListMax - m_scrollOffset then
					setVisibleAndEnabled(gHandles[v], false)
				else
					Static_SetText(gHandles[v], m_flagInfo.members[i+1])
					setVisibleAndEnabled(gHandles[v], true)
				end
			end
		end
	-- Non-owner version
	else
		-- Add new names
		for i,v in pairs(m_flagInfo.members) do
			if m_memberNames[i-1] == nil then
				addNameLabel(i-1, m_memberNames, "Member", m_flagInfo.members[i])
			end
		end
		-- Update old names
		for i,v in pairs(m_memberNames) do
			if m_flagInfo.members[i+1] then
				Static_SetText(gHandles[v], m_flagInfo.members[i+1])
				setVisibleAndEnabled(gHandles[v], true)
				Control_SetSize(gHandles[v], LIST_ITEM_WIDTH - 2, LIST_ITEM_HEIGHT)
			else
				Static_SetText(gHandles[v], " ")
			end
		end
	end
end

updateGreyBars = function()
	-- Owner version
	if m_isOwner then
		for i,v in pairs(m_flagInfo.ravers) do
			if m_raveGreyBars[i-1] == nil then
				addGreyBar(i-1, m_raveGreyBars, "Rave")
			end
		end
		
		for i,v in pairs(m_flagInfo.members) do
			if m_memberGreyBars[i-1] == nil then
				addGreyBar(i-1, m_memberGreyBars, "Member")
			end
		end

		for i,v in pairs(m_raveGreyBars) do
			-- If corresponding list item does not exist anymore, remove self
			if m_flagInfo.ravers[i+1] == nil then
				if tableCount(m_raveGreyBars) == 1 then
					m_raveGreyBars = {} -- Can't remove the last element of the table, so reset it
				else
					table.remove(m_raveGreyBars, i)
				end
				Dialog_RemoveControl(gDialogHandle, v)
			else
				local buttonOffset = 0
				if m_memberCollapsed and m_scrollOffset < -1 * LIST_HEADER_COUNT then
					buttonOffset = m_scrollOffset + LIST_HEADER_COUNT
				elseif not m_memberCollapsed and m_scrollOffset < -1 * tableCount(m_flagInfo.members) - 1 * LIST_HEADER_COUNT then
					buttonOffset = m_scrollOffset + tableCount(m_flagInfo.members) + LIST_HEADER_COUNT
				end
				-- If section collapsed or outside the display range, hide self
				if m_raveCollapsed or i+1 < m_raveListMin - buttonOffset or i+1 > m_raveListMax - buttonOffset then
					setVisibleAndEnabled(gHandles[v], false)
				elseif i%2 == 0 then
					setVisibleAndEnabled(gHandles[v], false)
				else
					setVisibleAndEnabled(gHandles[v], true)
				end
			end
		end

		for i,v in pairs(m_memberGreyBars) do
			if m_flagInfo.members[i+1] == nil then
				if tableCount(m_memberGreyBars) == 1 then
					m_memberGreyBars = {} -- Can't remove the last element of the table, so reset it
				else
					table.remove(m_memberGreyBars, i)
				end
				Dialog_RemoveControl(gDialogHandle, v)
			else
				-- If section collapsed
				if m_memberCollapsed or i+1 < m_memberListMin - m_scrollOffset or i+1 > m_memberListMax - m_scrollOffset then
					setVisibleAndEnabled(gHandles[v], false)
				elseif i%2 == 0 then
					setVisibleAndEnabled(gHandles[v], false)
				else
					setVisibleAndEnabled(gHandles[v], true)
				end
			end
		end
	-- Non-owner version
	else
		for i=1, MAX_MEMBER_COUNT do
			if m_memberGreyBars[i-1] == nil then
				addGreyBar(i-1, m_memberGreyBars, "Member")
			end
		end
		for i,v in pairs(m_memberGreyBars) do
			if i%2 == 0 then
				setVisibleAndEnabled(gHandles[v], false)
			else
				setVisibleAndEnabled(gHandles[v], true)
			end
			Control_SetSize(gHandles[v], LIST_ITEM_WIDTH - 2, LIST_ITEM_HEIGHT)
		end
	end
end

updateAddRemove = function()
	-- NOTE: the button lists (m_addButtons and m_removeButtons) are zero-indexed
	--   while the members and ravers lists (m_flagInfo.ravers and m_flagInfo.members) are 1-indexed.
	--   This explains the indexing adjustments of +1 and -1.

	-- If no corresponding add/remove button, add one
	for i,v in pairs(m_flagInfo.ravers) do
		if m_addButtons[i-1] == nil then
			addAddButton(i-1, "Rave")
		end
	end

	for i,v in pairs(m_flagInfo.members) do
		if m_removeButtons[i-1] == nil then
			addRemoveButton(i-1, "Member")
		end
	end

	for i,v in pairs(m_addButtons) do
		-- If corresponding list item does not exist anymore, remove self
		if m_flagInfo.ravers[i+1] == nil then
			if tableCount(m_addButtons) == 1 then
				m_addButtons = {} -- Can't remove the last element of the table, so reset it
			else
				table.remove(m_addButtons, i)
			end
			Dialog_RemoveControl(gDialogHandle, v)
		else
			local buttonOffset = 0
			if m_memberCollapsed and m_scrollOffset < -1 * LIST_HEADER_COUNT then
				buttonOffset = m_scrollOffset + LIST_HEADER_COUNT
			elseif not m_memberCollapsed and m_scrollOffset < -1 * tableCount(m_flagInfo.members) - 1 * LIST_HEADER_COUNT then
				buttonOffset = m_scrollOffset + tableCount(m_flagInfo.members) + LIST_HEADER_COUNT
			end
			-- If section collapsed or outside the display range, hide self
			if m_raveCollapsed or i+1 < m_raveListMin - buttonOffset or i+1 > m_raveListMax - buttonOffset then
				setVisibleAndEnabled(gHandles[v], false)
			-- If raver in the members list already, disable add button
			elseif checkForEntry(m_flagInfo.members, m_flagInfo.ravers[i+1]) then
				Control_SetEnabled(gHandles[v], false)
				Control_SetVisible(gHandles[v], true)
			elseif tableCount(m_flagInfo.members) >= MAX_MEMBER_COUNT then
				Control_SetEnabled(gHandles[v], false)
				Control_SetVisible(gHandles[v], true)
			else
				setVisibleAndEnabled(gHandles[v], true)
			end
		end		

	end

	for i,v in pairs(m_removeButtons) do
		if m_flagInfo.members[i+1] == nil then
			if tableCount(m_removeButtons) == 1 then
				m_removeButtons = {} -- Can't remove the last element of the table, so reset it
			else
				table.remove(m_removeButtons, i)
			end
			Dialog_RemoveControl(gDialogHandle, v)
		else
			-- If section collapsed
			if m_memberCollapsed or i+1 < m_memberListMin - m_scrollOffset or i+1 > m_memberListMax - m_scrollOffset then
				setVisibleAndEnabled(gHandles[v], false)
			elseif m_flagInfo.members[i+1] == m_flagInfo.owner then
				setVisibleAndEnabled(gHandles[v], false)
			else
				setVisibleAndEnabled(gHandles[v], true)
			end
		end
	end
end

updateRaveListPosition = function()
	local tempOffset = 0
	if not m_memberCollapsed then
		tempOffset = (tableCount(m_flagInfo.members) * LIST_ITEM_HEIGHT) + LIST_HEADER_HEIGHT --  + (math.min(tableCount(m_flagInfo.members), 1) * SOFT_PADDING)
	else
		tempOffset = LIST_HEADER_HEIGHT
	end

	local raveListOffset = m_scrollOffset*LIST_ITEM_HEIGHT
	tempOffset = math.max(tempOffset + raveListOffset, -1 * LIST_HEADER_HEIGHT)

	local referencePoint = Control_GetLocationY(gHandles["stcMemberHead"])
	Control_SetLocationY(gHandles["imgRaveHeadBG"], referencePoint + tempOffset)
	Control_SetLocationY(gHandles["btnRaveExpand"], referencePoint + tempOffset)
	Control_SetLocationY(gHandles["btnRaveCollapse"], referencePoint + tempOffset)
	Control_SetLocationY(gHandles["imgRaveExpand"], referencePoint + ARROW_BTN_BUFFER + tempOffset)
	Control_SetLocationY(gHandles["imgRaveCollapse"], referencePoint + ARROW_BTN_BUFFER + tempOffset)
	Control_SetLocationY(gHandles["stcRaveHead"], referencePoint + tempOffset)

	local scrollTooFarCheck = ((not m_memberCollapsed) and m_scrollOffset <= -1 * tableCount(m_flagInfo.members) - LIST_HEADER_COUNT) or (m_memberCollapsed and m_scrollOffset <= -1 * LIST_HEADER_COUNT)

	setVisibleAndEnabled(gHandles["stcRaveHead"], not scrollTooFarCheck)
	setVisibleAndEnabled(gHandles["imgRaveHeadBG"], not scrollTooFarCheck)
	setVisibleAndEnabled(gHandles["btnRaveExpand"], m_raveCollapsed and not scrollTooFarCheck)
	setVisibleAndEnabled(gHandles["btnRaveCollapse"], not m_raveCollapsed and not scrollTooFarCheck)
	setVisibleAndEnabled(gHandles["imgRaveExpand"], m_raveCollapsed and not scrollTooFarCheck)
	setVisibleAndEnabled(gHandles["imgRaveCollapse"], not m_raveCollapsed and not scrollTooFarCheck)

	-- Update add buttons
	local buttonOffset = 0
	if m_memberCollapsed and m_scrollOffset < -1 * LIST_HEADER_COUNT then
		buttonOffset = m_scrollOffset + LIST_HEADER_COUNT
	elseif not m_memberCollapsed and m_scrollOffset < -1 * tableCount(m_flagInfo.members) - 1 * LIST_HEADER_COUNT then
		buttonOffset = m_scrollOffset + tableCount(m_flagInfo.members) + LIST_HEADER_COUNT
	end
	for i,v in pairs(m_addButtons) do
		Control_SetLocationY(gHandles[v], referencePoint + tempOffset + ((i + buttonOffset) * LIST_ITEM_HEIGHT) + LIST_HEADER_HEIGHT + BUTTON_BUFFER)
		Control_SetLocationX(gHandles[v], Control_GetLocationX(gHandles["btnRaveExpand"]) + LIST_ITEM_WIDTH - SCROLL_BAR_WIDTH - ADD_BUTTON_WIDTH - BUTTON_BUFFER)
	end
	for i,v in pairs(m_raveGreyBars) do
		Control_SetLocationY(gHandles[v], referencePoint + tempOffset + ((i + buttonOffset) * LIST_ITEM_HEIGHT) + LIST_HEADER_HEIGHT)
	end
	for i,v in pairs(m_raveNames) do
		Control_SetLocationY(gHandles[v], referencePoint + tempOffset + ((i + buttonOffset) * LIST_ITEM_HEIGHT) + LIST_HEADER_HEIGHT)
	end

end

updateMemberListPosition = function()
	-- Move everything relative to the STARTING position of stcMemberHead
	-- The rave list moves relative to the CURRENT position of stcMemberHead
	local tempOffset = 0

	Control_SetLocationY(gHandles["stcMemberHead"], DEFAULT_MEMBER_LIST_Y + tempOffset)
	Control_SetLocationY(gHandles["imgMemberHeadBG"], DEFAULT_MEMBER_LIST_Y + tempOffset)
	Control_SetLocationY(gHandles["stcMemberCount"], DEFAULT_MEMBER_LIST_Y + tempOffset)
	Control_SetLocationY(gHandles["stcMemberHead2"], DEFAULT_MEMBER_LIST_Y + tempOffset)
	Control_SetLocationY(gHandles["btnMemberExpand"], DEFAULT_MEMBER_LIST_Y + tempOffset)
	Control_SetLocationY(gHandles["btnMemberCollapse"], DEFAULT_MEMBER_LIST_Y + tempOffset)
	Control_SetLocationY(gHandles["imgMemberExpand"], DEFAULT_MEMBER_LIST_Y + ARROW_BTN_BUFFER + tempOffset)
	Control_SetLocationY(gHandles["imgMemberCollapse"], DEFAULT_MEMBER_LIST_Y + ARROW_BTN_BUFFER + tempOffset)

	setVisibleAndEnabled(gHandles["stcMemberHead"], (m_scrollOffset >= 0))
	setVisibleAndEnabled(gHandles["imgMemberHeadBG"], (m_scrollOffset >= 0))
	setVisibleAndEnabled(gHandles["stcMemberCount"], (m_scrollOffset >= 0))
	setVisibleAndEnabled(gHandles["stcMemberHead2"], (m_scrollOffset >= 0))
	setVisibleAndEnabled(gHandles["btnMemberExpand"], m_memberCollapsed and (m_scrollOffset >= 0))
	setVisibleAndEnabled(gHandles["btnMemberCollapse"], not m_memberCollapsed and (m_scrollOffset >= 0))
	setVisibleAndEnabled(gHandles["imgMemberExpand"], m_memberCollapsed and (m_scrollOffset >= 0))
	setVisibleAndEnabled(gHandles["imgMemberCollapse"], not m_memberCollapsed and (m_scrollOffset >= 0))

	-- Update remove buttons
	for i,v in pairs(m_removeButtons) do
		local buttonOffset = i
		if m_scrollOffset < 0 then
			buttonOffset = i + m_scrollOffset
		end
		Control_SetLocationY(gHandles[v], DEFAULT_MEMBER_LIST_Y + tempOffset + (buttonOffset * LIST_ITEM_HEIGHT) + LIST_HEADER_HEIGHT + BUTTON_BUFFER)
		Control_SetLocationX(gHandles[v], Control_GetLocationX(gHandles["btnMemberExpand"]) + LIST_ITEM_WIDTH - SCROLL_BAR_WIDTH - ADD_BUTTON_WIDTH - BUTTON_BUFFER)
	end
	for i,v in pairs(m_memberGreyBars) do
		local buttonOffset = i
		if m_scrollOffset < 0 then
			buttonOffset = i + m_scrollOffset
		end
		Control_SetLocationY(gHandles[v], DEFAULT_MEMBER_LIST_Y + tempOffset + (buttonOffset * LIST_ITEM_HEIGHT) + LIST_HEADER_HEIGHT)
	end
	for i,v in pairs(m_memberNames) do
		local buttonOffset = i
		if m_scrollOffset < 0 then
			buttonOffset = i + m_scrollOffset
		end
		Control_SetLocationY(gHandles[v], DEFAULT_MEMBER_LIST_Y + tempOffset + (buttonOffset * LIST_ITEM_HEIGHT) + LIST_HEADER_HEIGHT)
	end
end

updateRaveListMinMax = function()
	m_raveListMin = DEFAULT_RAVE_LIST_MIN
	m_raveListMax = DEFAULT_RAVE_LIST_MAX
	if not m_memberCollapsed and not m_raveCollapsed then
		m_raveListMax = m_raveListMax - tableCount(m_flagInfo.members)
	end

	m_raveListMax = math.min(m_raveListMax - m_scrollOffset, DEFAULT_RAVE_LIST_MAX + 1)
end

updateFeePayCtrls = function()
	local flagItem, requiredItem
	flagItem = m_gameItems[tostring(m_flagInfo.UNID)]
	if flagItem and flagItem.behaviorParams then

		local tempFeeInterval = flagItem.behaviorParams.feeInterval or DEFAULT_FEE_INTERVAL
		tempFeeInterval = tempFeeInterval * DAYS_PER_WEEK * SEC_PER_DAY -- Convert from weeks to seconds

		-- Debug.printTable("", flagItem)
		local requiredItem = m_gameItems[tostring(flagItem.behaviorParams.feeObject)]
		if requiredItem then
			-- Get required item name and count
			local tempFeeCount = flagItem.behaviorParams.feeCount or 1
			local tempReqItemName = requiredItem.name or ""
			Static_SetText(gHandles["stcFeeName"], tempFeeCount.." "..tempReqItemName)
			-- Set required item image
			local tempReqItemGLID = requiredItem.GLID
			local imgHandle = Image_GetDisplayElement(Dialog_GetImage(gDialogHandle, "imgFeeItem"))
			if imgHandle and gDialogHandle then
				KEP_LoadIconTextureByID(tonumber(tempReqItemGLID), imgHandle, gDialogHandle)
			end
			-- Get player backpack count
			local tempReqItemUNID = flagItem.behaviorParams.feeObject or 0
			local backCnt = m_inventoryCounts[tempReqItemUNID] or 0
			if backCnt == nil or backCnt == "" then backCnt = 0 end
			local cntColor = COLORS.BAD
			local hasNuf = false
			if backCnt > flagItem.behaviorParams.feeCount then
				cntColor = COLORS.GOOD
				hasNuf = true
			end

			local tempReqItemCntStr = "You have "..formatCommaValue(tostring(backCnt).."")

			Static_SetText(gHandles["stcFeeCount"], tempReqItemCntStr)

			local elem = Static_GetDisplayElement(gHandles["stcFeeCount"])
	        local bc = Element_GetFontColor(elem)
			BlendColor_SetColor(bc, 0, cntColor)

			if m_currTime > 0 and tempFeeInterval then
				-- Can pay for up to TWO pay periods if you have enough to pay
				Control_SetEnabled(gHandles["btnPayFee"], (m_flagInfo.feeDueDate < m_currTime + FEE_BUFFER_CNT * tempFeeInterval) and hasNuf)
			end

			-- TO DO: Set the due date (first, track and get the due date...)

		end
	end
end

updateDueDate = function() -- 1234
	if m_currTime > 0 and m_flagInfo and m_flagInfo.feeDueDate then
		local timeRemaining = 0

		if m_currTime < m_flagInfo.feeDueDate then
			timeRemaining = math.max(0, m_flagInfo.feeDueDate - m_currTime)

			timeRemaining = getTimeString(timeRemaining, 2)
			Static_SetText(gHandles["stcFeeDueDate"], "Due in "..tostring(timeRemaining)..".")

			local elem = Static_GetDisplayElement(gHandles["stcFeeDueDate"])
	        local bc = Element_GetFontColor(elem)
			BlendColor_SetColor(bc, 0, COLORS.WHITE)
		elseif m_currTime < m_flagInfo.feeDueDate + (1 * SEC_PER_DAY) then
			timeRemaining = math.max(0, m_flagInfo.feeDueDate + (1 * SEC_PER_DAY) - m_currTime)

			timeRemaining = addLinebreakToTime( tostring(getTimeString(timeRemaining, 2)) )
			Static_SetText(gHandles["stcFeeDueDate"], "Item removal in "..tostring(timeRemaining)..".")

			local elem = Static_GetDisplayElement(gHandles["stcFeeDueDate"])
	        local bc = Element_GetFontColor(elem)
			BlendColor_SetColor(bc, 0, COLORS.BAD)
		elseif m_currTime < m_flagInfo.feeDueDate + (2 * SEC_PER_DAY) then
			timeRemaining = math.max(0, m_flagInfo.feeDueDate + (2 * SEC_PER_DAY) - m_currTime)

			timeRemaining = addLinebreakToTime( tostring(getTimeString(timeRemaining, 2)) )
			Static_SetText(gHandles["stcFeeDueDate"], "Flag removal in "..tostring(timeRemaining)..".")

			local elem = Static_GetDisplayElement(gHandles["stcFeeDueDate"])
	        local bc = Element_GetFontColor(elem)
			BlendColor_SetColor(bc, 0, COLORS.BAD)
		end
	end
end

addLinebreakToTime = function(timeRemaining)
	local tmpPos = string.find(timeRemaining, " and")
	if tmpPos ~= nil then
		local tmpStr = string.sub(timeRemaining,0,tmpPos)
		tmpStr = tmpStr .. "<br>" .. string.sub(timeRemaining, tmpPos+1) 
		return tmpStr 
	end
	return timeRemaining
end
-- TOGGLE

-- TRUE: owner mode, FALSE: player mode
toggleOwnerElements = function(toggle)
	
	local anchorPoint = Control_GetLocationX(gHandles["imgMemberHeadBG"])
	local shiftUpAmt = 0

	Control_SetSize(gHandles["imgMemberHeadBG"], LIST_HEADER_WIDTH, LIST_HEADER_HEIGHT)
	if toggle then
		Control_SetSize(gHandles["imgBG"], MENU_SIZE.x, MENU_SIZE.y + SOFT_PADDING + RAVE_LABEL_HEIGHT + RAVE_BUTTON_HEIGHT) -- Extra padding to make rounded corners go BENEATH the bottom BG elements
		-- Headers for the list box
		Static_SetText(gHandles["stcMemberHead"], "Members:")
		Control_SetLocationX(gHandles["stcMemberHead"], anchorPoint + BUTTON_BUFFER + ADD_BUTTON_WIDTH)
		Control_SetLocationX(gHandles["stcRaveHead"], 	anchorPoint + BUTTON_BUFFER + ADD_BUTTON_WIDTH)
		-- List box
		Control_SetSize(gHandles["imgListBox"], LIST_ITEM_WIDTH, LIST_BOX_HEIGHT)
		anchorPoint = Control_GetLocationY(gHandles["imgListBox"]) + LIST_BOX_HEIGHT
		-- NO RAVE BUTTON OR GREY PANEL BELOW LIST BOX
		Control_SetLocationY(gHandles["imgBelowList"], 	anchorPoint)
		Control_SetSize(gHandles["imgBelowList"], Control_GetWidth(gHandles["imgBelowList"]), LIST_ITEM_HEIGHT)
		-- Bottom panel
		Control_SetLocationY(gHandles["imgBGLower"], 	anchorPoint + LIST_ITEM_HEIGHT + SOFT_PADDING)		
		Control_SetLocationY(gHandles["btnClose"], 		anchorPoint + LIST_ITEM_HEIGHT + SOFT_PADDING + LOWER_BG_HEIGHT/2 - CLOSE_BUTTON_HEIGHT/2)
	-- If NOT the owner
	else
		shiftUpAmt = LIST_BOX_HEIGHT - (MAX_MEMBER_COUNT * LIST_ITEM_HEIGHT + LIST_HEADER_HEIGHT)
		Control_SetSize(gHandles["imgBG"], MENU_SIZE.x, MENU_SIZE.y - shiftUpAmt + SOFT_PADDING + RAVE_LABEL_HEIGHT + RAVE_BUTTON_HEIGHT) -- Extra padding to make rounded corners go BENEATH the bottom BG elements
		-- Headers for the list box
		Static_SetText(gHandles["stcMemberHead"], "Members")
		Control_SetLocationX(gHandles["stcMemberHead"], anchorPoint + BUTTON_BUFFER) -- Shift left to fill the space where the collapse arrow would be
		-- List box
		Control_SetSize(gHandles["imgListBox"], LIST_ITEM_WIDTH, MAX_MEMBER_COUNT * LIST_ITEM_HEIGHT + LIST_HEADER_HEIGHT)
		anchorPoint = Control_GetLocationY(gHandles["imgListBox"]) + Control_GetHeight(gHandles["imgListBox"])
		-- Immediately below the list box
		Control_SetLocationY(gHandles["imgBelowList"], 	anchorPoint)
		-- Above the bottom panel
		Control_SetLocationY(gHandles["btnRave"], 		anchorPoint + SOFT_PADDING)
		Control_SetLocationY(gHandles["stcRaveMe"], 	anchorPoint + SOFT_PADDING	+ RAVE_BUTTON_HEIGHT)
		-- Bottom panel
		Control_SetLocationY(gHandles["imgBGLower"], 	anchorPoint + 2*SOFT_PADDING	+ RAVE_LABEL_HEIGHT	+ RAVE_BUTTON_HEIGHT)		
		Control_SetLocationY(gHandles["btnClose"], 		anchorPoint + 2*SOFT_PADDING	+ RAVE_LABEL_HEIGHT	+ RAVE_BUTTON_HEIGHT + LOWER_BG_HEIGHT/2 - CLOSE_BUTTON_HEIGHT/2)
	end

	-- Shared elements
	setVisibleAndEnabled(gHandles["imgBG"], true)
	setVisibleAndEnabled(gHandles["imgFlag"], true)
	setVisibleAndEnabled(gHandles["imgListBox"], true)
	setVisibleAndEnabled(gHandles["stcOwner"], true)
	setVisibleAndEnabled(gHandles["stcGroupName"], true)
	setVisibleAndEnabled(gHandles["imgBGLower"], true)
	setVisibleAndEnabled(gHandles["btnClose"], true)

	-- Owner elements
	Control_SetEnabled(gHandles["edGroupName"], false)
	Control_SetVisible(gHandles["edGroupName"], toggle)
	setVisibleAndEnabled(gHandles["btnEditGroupName"], toggle)
	setVisibleAndEnabled(gHandles["imgEditGroupName"], toggle)
	-- Scroll bar
	setVisibleAndEnabled(gHandles["imgScrollBG"], toggle)
	setVisibleAndEnabled(gHandles["imgScrollBar"], toggle)
	setVisibleAndEnabled(gHandles["btnScrollBar"], toggle)
	setVisibleAndEnabled(gHandles["btnScrollUpArrow"], toggle)
	setVisibleAndEnabled(gHandles["btnScrollDownArrow"], toggle)
	-- Rave management
	setVisibleAndEnabled(gHandles["imgRaveHeadBG"], toggle)
	setVisibleAndEnabled(gHandles["btnRaveExpand"], toggle)
	setVisibleAndEnabled(gHandles["btnRaveCollapse"], toggle)
	setVisibleAndEnabled(gHandles["imgRaveExpand"], toggle)
	setVisibleAndEnabled(gHandles["imgRaveCollapse"], toggle)
	setVisibleAndEnabled(gHandles["stcRaveHead"], toggle)
	-- Member management
	setVisibleAndEnabled(gHandles["btnMemberExpand"], toggle)
	setVisibleAndEnabled(gHandles["btnMemberCollapse"], toggle)
	setVisibleAndEnabled(gHandles["imgMemberExpand"], toggle)
	setVisibleAndEnabled(gHandles["imgMemberCollapse"], toggle)
	setVisibleAndEnabled(gHandles["stcMemberCount"], toggle)
	setVisibleAndEnabled(gHandles["stcMemberHead2"], toggle)

	-- Non-owner elements
	setVisibleAndEnabled(gHandles["stcGroupName"], not toggle)
	setVisibleAndEnabled(gHandles["btnRave"], not toggle)
	setVisibleAndEnabled(gHandles["stcRaveMe"], not toggle)
end

toggleMemberView = function()
	setVisibleAndEnabled(gHandles["btnMemberExpand"], m_memberCollapsed and not (m_scrollOffset < 0))
	setVisibleAndEnabled(gHandles["btnMemberCollapse"], not m_memberCollapsed and not (m_scrollOffset < 0))
	setVisibleAndEnabled(gHandles["imgMemberExpand"], m_memberCollapsed and not (m_scrollOffset < 0))
	setVisibleAndEnabled(gHandles["imgMemberCollapse"], not m_memberCollapsed and not (m_scrollOffset < 0))

	for i,v in pairs(m_removeButtons) do
		setVisibleAndEnabled(gHandles[v], not m_memberCollapsed)
	end
end

toggleRaveView = function()
	setVisibleAndEnabled(gHandles["btnRaveExpand"], m_raveCollapsed)
	setVisibleAndEnabled(gHandles["btnRaveCollapse"], not m_raveCollapsed)
	setVisibleAndEnabled(gHandles["imgRaveExpand"], m_raveCollapsed)
	setVisibleAndEnabled(gHandles["imgRaveCollapse"], not m_raveCollapsed)

	for i,v in pairs(m_addButtons) do
		setVisibleAndEnabled(gHandles[v], not m_raveCollapsed)
	end
end

-- OTHER

collapseMemberList = function()
	m_memberCollapsed = true
	toggleMemberView()
	updateDisplay()
end

expandMemberList = function()
	m_memberCollapsed = false
	toggleMemberView()
	updateDisplay()
end

collapseRaveList = function()
	m_raveCollapsed = true
	toggleRaveView(false)
	m_scrollOffset = 0
	updateDisplay()
end

expandRaveList = function()
	m_raveCollapsed = false
	toggleRaveView(true)
	updateDisplay()
end

scrollUpSingle = function()
	if m_scrollOffset < 0 then
		m_scrollOffset = m_scrollOffset + 1
		updateDisplay()
	end
end

scrollDownSingle = function()
	if m_scrollOffset > -1*m_scrollOffsetMax then
		m_scrollOffset = m_scrollOffset - 1
		updateDisplay()
	end
end

addNameLabel = function(insertIndex, inputTable, inputType, inputString)
	anchorPoint = gHandles["btn"..inputType.."Expand"]

	local newX = Control_GetLocationX(anchorPoint) + SOFT_PADDING
	local newY = Control_GetLocationY(anchorPoint) + LIST_HEADER_HEIGHT + LIST_ITEM_HEIGHT * (insertIndex + m_scrollOffset)

	-- Create a copy
	local newControl = copyControl(gHandles["stcTextBar"])
	local newControlName = "stc"..inputType.."NameLabel"..insertIndex
	Control_SetName(newControl, newControlName)
	gHandles[newControlName] = newControl
	table.insert(inputTable, insertIndex, newControlName)

	-- Update position (base off the LAST control added)
	Control_SetLocation(newControl, newX, newY)
	Static_SetText(newControl, inputString, true)

	-- Enable
	setVisibleAndEnabled(newControl, true)
	Dialog_MoveControlToBack(gDialogHandle, gHandles[newControlName])
	for i,v in pairs(m_raveGreyBars) do
		Dialog_MoveControlToBack(gDialogHandle, gHandles[v])
	end
	for i,v in pairs(m_memberGreyBars) do
		Dialog_MoveControlToBack(gDialogHandle, gHandles[v])
	end
	Dialog_MoveControlToBack(gDialogHandle, gHandles["imgListBox"])
	Dialog_MoveControlToBack(gDialogHandle, gHandles["imgBG"])
end

addGreyBar = function(insertIndex, inputTable, inputType)
	anchorPoint = gHandles["btn"..inputType.."Expand"]

	local newX = Control_GetLocationX(anchorPoint) + 1
	local newY = Control_GetLocationY(anchorPoint) + LIST_HEADER_HEIGHT + LIST_ITEM_HEIGHT * (insertIndex + m_scrollOffset)

	-- Create a copy
	local newControl = copyControl(gHandles["imgGreyBar"])
	local newControlName = "img"..inputType.."GreyBar"..insertIndex
	Control_SetName(newControl, newControlName)
	gHandles[newControlName] = newControl
	table.insert(inputTable, insertIndex, newControlName)

	-- Update position (base off the LAST control added)
	Control_SetLocation(newControl, newX, newY)

	-- Enable
	setVisibleAndEnabled(newControl, true)
	Dialog_MoveControlToBack(gDialogHandle, gHandles[newControlName])
	Dialog_MoveControlToBack(gDialogHandle, gHandles["imgListBox"])
	Dialog_MoveControlToBack(gDialogHandle, gHandles["imgBG"])
end

addAddButton = function(insertIndex, inputType)
	local newX = Control_GetLocationX(gHandles["btnRaveExpand"]) + LIST_ITEM_WIDTH - SCROLL_BAR_WIDTH - ADD_BUTTON_WIDTH - SOFT_PADDING
	local newY = Control_GetLocationY(gHandles["btnRaveExpand"]) + LIST_HEADER_HEIGHT * (1 + tableCount(m_addButtons)) + SOFT_PADDING
	-- Create a copy of the add button
	local newControl = copyControl(gHandles["btnAddMember"])
	local newControlName = "btnAddMember"..insertIndex
	Control_SetName(newControl, newControlName)
	gHandles[newControlName] = newControl
	table.insert(m_addButtons, insertIndex, newControlName)

	-- Attach functionality
	_G[tostring(newControlName).."_OnButtonClicked"] = function(btnHandle)
		local tempIndex = getIndex(m_addButtons, newControlName) + 1
		addMember(tempIndex) -- Add member at the corresponding index in the rave list
	end

	-- Update position (base off the LAST control added)
	Control_SetLocation(newControl, newX, newY)

	-- Enable
	setVisibleAndEnabled(newControl, true)
	Dialog_MoveControlToBack(gDialogHandle, gHandles[newControlName])
	for i,v in pairs(m_raveGreyBars) do
		Dialog_MoveControlToBack(gDialogHandle, gHandles[v])
	end
	for i,v in pairs(m_memberGreyBars) do
		Dialog_MoveControlToBack(gDialogHandle, gHandles[v])
	end
	Dialog_MoveControlToBack(gDialogHandle, gHandles["imgListBox"])
	Dialog_MoveControlToBack(gDialogHandle, gHandles["imgBG"])
end

addRemoveButton = function(removeIndex, inputType)
	local newX = Control_GetLocationX(gHandles["btnMemberExpand"]) + LIST_ITEM_WIDTH - SCROLL_BAR_WIDTH - ADD_BUTTON_WIDTH - BUTTON_BUFFER
	local newY = Control_GetLocationY(gHandles["btnMemberExpand"]) + LIST_HEADER_HEIGHT + LIST_ITEM_HEIGHT * (m_scrollOffset + tableCount(m_removeButtons)) + BUTTON_BUFFER
	-- Create a copy of the add button
	local newControl = copyControl(gHandles["btnDeleteMember"])
	local newControlName = "btnDeleteMember"..removeIndex
	Control_SetName(newControl, newControlName)
	gHandles[newControlName] = newControl
	table.insert(m_removeButtons, removeIndex, newControlName)

	-- Attach functionality
	_G[tostring(newControlName).."_OnButtonClicked"] = function(btnHandle)
		local tempIndex = getIndex(m_removeButtons, newControlName) + 1
		removeMember(tempIndex) -- Add member at the corresponding index in the rave list
	end

	-- Update position (base off the LAST control added)
	Control_SetLocation(newControl, newX, newY)
	newY = newY + LIST_ITEM_HEIGHT

	-- Enable
	setVisibleAndEnabled(newControl, true)
	Dialog_MoveControlToBack(gDialogHandle, gHandles[newControlName])
	for i,v in pairs(m_raveGreyBars) do
		Dialog_MoveControlToBack(gDialogHandle, gHandles[v])
	end
	for i,v in pairs(m_memberGreyBars) do
		Dialog_MoveControlToBack(gDialogHandle, gHandles[v])
	end
	Dialog_MoveControlToBack(gDialogHandle, gHandles["imgListBox"])
	Dialog_MoveControlToBack(gDialogHandle, gHandles["imgBG"])
end

saveFlagProperties = function()
	-- Web call update
	if tableCount(m_flagInfo.members) > 0 then

		-- These need to be IDS not NAMES
		local tempIDs = ""
		for i,v in pairs(m_flagInfo.members) do
			if tempIDs == "" then
				tempIDs = tostring(v)
			else
				tempIDs = tempIDs.."|"..tostring(v)
			end
		end

		-- Update team members
		local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=teamUpdate&objPlacementId="..tostring(m_flagInfo.PID).."&usernames="..tostring(tempIDs)
		makeWebCall(address, WF.FLAG_TEAM_UPDATE)

		-- Update name (TO DO: check if it changed, check for duplicate, check for profanity)
		local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=setObjectName&objPlacementId="..tostring(m_flagInfo.PID).."&name="..tostring(m_flagInfo.groupName)
		makeWebCall(address, WF.FLAG_NAME_UPDATE)
	end

	-- Save change to flag
	Events.sendEvent("SAVE_FLAG_GROUP_INFO", {playerName = KEP_GetLoginName(), flagInfo = deepCopy(m_flagInfo)})
end

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- If browser return for Location Data web call
	if filter == WF.FLAG_RAVE then
		local returnXML = KEP_EventDecodeString( tEvent )
		local returnCode = ParseXmlReturnCode(returnXML)
		local returnDescription = xmlGetString(returnXML, "ReturnDescription")

		if m_pendingRaveName ~= "" and (tonumber(returnCode) == RAVE_RESPONSES.SUCCESS) or (returnDescription == "User has already raved this item" and not checkForEntry(m_flagInfo.ravers, m_pendingRaveName))  then -- Check if already raved (if yes, change button state -> do this on create)
			table.insert(m_flagInfo.ravers, m_pendingRaveName)
			m_pendingRaveName = ""
			m_flagInfo.raveCount = m_flagInfo.raveCount + 1
			updateDisplay()
			-- Broadcast changes to flag
			Events.sendEvent("SAVE_FLAG_GROUP_INFO", {playerName = KEP_GetLoginName(), flagInfo = deepCopy(m_flagInfo)})
		end
	elseif filter == WF.FLAG_TEAM_UPDATE then
		local returnXML = KEP_EventDecodeString( tEvent )
		local returnCode = ParseXmlReturnCode(returnXML)
		local returnDescription = xmlGetString(returnXML, "ReturnDescription")

		-- Data is saved, so now safe to close menu
		-- Prevent close on ESC to prevent data from saving? Or save data as it goes.
		if m_closePending == true then
			MenuCloseThis()
		end
	end
end

raveFlag = function(playerName)
	m_pendingRaveName = playerName
	if not checkForEntry(m_flagInfo.ravers, playerName) then -- Check if already raved (if yes, change button state -> do this on create)
		-- Web call
		local address = GameGlobals.WEB_SITE_PREFIX.."kgp/worldLeaderboard.aspx?action=rave&zoneInstanceId="..m_zoneInstanceId.."&zoneType="..m_zoneType.."&objPlacementId="..tostring(m_flagInfo.PID).."&name="..tostring(m_flagInfo.groupName)
		makeWebCall(address, WF.FLAG_RAVE)
	end
end

addMember = function(raverIndex)
	if m_flagInfo.ravers[raverIndex] then
		if tableCount(m_flagInfo.members) < MAX_MEMBER_COUNT and not checkForEntry(m_flagInfo.members, m_flagInfo.ravers[raverIndex]) then
			local nameToAdd = m_flagInfo.ravers[raverIndex]
			table.insert(m_flagInfo.members, nameToAdd) -- Check if already there?
			-- m_raveListMax = m_raveListMax - 1 -- Adding a member pushes the rave list down, reducing the maximum display range
			updateDisplay()
			saveFlagProperties()
		end
	end
end

removeMember = function(memberIndex)
	if m_flagInfo.members[memberIndex] then
		table.remove(m_flagInfo.members, memberIndex)
		-- m_raveListMax = m_raveListMax + 1  -- Removing a member pushes the rave list up, increasing the maximum display range
		updateDisplay()
		saveFlagProperties()
	end
end

-- HELPER FUNCTIONS

-- Returns a string with line breaks after each entry
tableToString = function(inputTable)
	local tempString = ""

	for i,v in pairs(inputTable) do
		if tempString == "" then
			tempString = tempString..v
		else
			tempString = tempString.."\n"..v		
		end
	end

	return tempString
end

-- Returns the number of non-empty items in a table
tableCount = function(inputTable)
	local tempCount = 0

	for i,v in pairs(inputTable) do
		if v and v ~= "" then
			tempCount = tempCount + 1
		end
	end

	return tempCount
end

-- Returns whether a search item exists in a table
checkForEntry = function(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return true
		end
	end
	return false
end

isAMember = function(playerName)
	return checkForEntry(m_flagInfo.members, playerName)
end

-- Returns the index of a search item within a table (if any)
getIndex = function(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return i
		end
	end
	return -1
end

setVisibleAndEnabled = function(inputHandle, inputValue)
	Control_SetVisible(inputHandle, inputValue)
	Control_SetEnabled(inputHandle, inputValue)
end

formatCommaValue = function(value)
  	local formatted = value
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
   		if (k==0) then
      		break
   		end
 	end

 	return formatted
end

convertTime = function(timestamp)
    local time = {}
    time["timestamp"] = timestamp
    -- time["month"] = math.floor((math.fmod(timestamp, SEC_PER_YEAR) / SEC_PER_MON) + 1) -- Add 1 (0 based)
    time["week"]   = math.floor( timestamp / SEC_PER_WEEK)
    time["day"]    = math.floor( math.fmod(timestamp, SEC_PER_WEEK) / SEC_PER_DAY)
    time["hour"]   = math.floor( math.fmod(timestamp, SEC_PER_DAY) / SEC_PER_HOUR)
    time["minute"] = math.floor( math.fmod(timestamp, SEC_PER_HOUR) / SEC_PER_MIN)
    time["second"] = math.floor( math.fmod(timestamp, SEC_PER_MIN))
    
    return time
end

getTimeString = function(timestamp, maxConcat)
	local tempTimestamp = math.floor(timestamp) -- /1000 -- Convert from miliseconds to seconds
	local time = convertTime(tempTimestamp)
	local timeString = ""
	local concatCnt = 0

	-- if time.month > 0 then
	-- 	if concatCnt > 0 then
	-- 		timeString = timeString..","
	-- 	end
	-- 	timeString = timeString..tostring(time.month)
	-- 	if time.month == 1 then
	-- 		timeString = timeString.." month"
	-- 	else
	-- 		timeString = timeString.." months"
	-- 	end
	-- 	concatCnt = concatCnt + 1
	-- end
	if time.week > 0 then
		if concatCnt > 0 then
			timeString = timeString.." and "
		end
		timeString = timeString..tostring(time.week)
		if time.week == 1 then
			timeString = timeString.." week"
		else
			timeString = timeString.." weeks"
		end
		concatCnt = concatCnt + 1
	end
	if time.day > 0 and concatCnt < maxConcat then
		if concatCnt > 0 then
			timeString = timeString.." and "
		end
		timeString = timeString..tostring(time.day)
		if time.day == 1 then
			timeString = timeString.." day"
		else
			timeString = timeString.." days"
		end
		concatCnt = concatCnt + 1
	end
	if time.hour > 0 and concatCnt < maxConcat then
		if concatCnt > 0 then
			timeString = timeString.." and "
		end
		timeString = timeString..tostring(time.hour)
		if time.hour == 1 then
			timeString = timeString.." hour"
		else
			timeString = timeString.." hours"
		end
		concatCnt = concatCnt + 1
	end
	if time.minute > 0 and concatCnt < maxConcat then
		if concatCnt > 0 then
			timeString = timeString..timeString.." and "
		end
		timeString = tostring(time.minute)
		if time.minute == 1 then
			timeString = timeString.." minute"
		else
			timeString = timeString.." minutes"
		end
		concatCnt = concatCnt + 1
	end
	if time.second > 0 and concatCnt < maxConcat then
		if concatCnt == 0 then
			timeString = " <1 minute"
		end
	-- 	if concatCnt > 0 then
	-- 		timeString = timeString.."<br>and "
	-- 	end
	-- 	timeString = timeString..tostring(time.second)
	-- 	if time.second == 1 then
	-- 		timeString = timeString.." second"
	-- 	else
	-- 		timeString = timeString.." seconds"
	-- 	end
	-- 	concatCnt = concatCnt + 1

	end
	return timeString
end

--Copy and paste controls
function copyControl(menuControl)
	if menuControl then
		Dialog_CopyControl( gDialogHandle, menuControl )
		Dialog_PasteControl( gDialogHandle )

		local numControls = Dialog_GetNumControls( gDialogHandle )

		local newControl = Dialog_GetControlByIndex( gDialogHandle, numControls - 1 )
		local controlName = Control_GetName( newControl )

		gHandles[controlName] = newControl	

		return newControl
	end
end