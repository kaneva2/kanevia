--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Menu.lua - Kaneva Menu Script Template
-- 
-- Template script for creating a new Kaneva client menu.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_InventoryHelper.lua")
dofile("..\\MenuScripts\\Lib_Web.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Web			= _G.Web

-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local ITEMS_PER_PAGE	= 5
local KEY_ESC			= 27
local ITEMS_TYPE		= {WEB = "WEB", FRAMEWORK = "FRAMEWORK"}
local LOOT_ITEMS		= {}


-----------------------------------------------------------------
-- Variables
-----------------------------------------------------------------
local m_item = {}

local m_displayItems = {}
local m_worldItems = {}

local m_useType = 0
local m_itemsType = ITEMS_TYPE.WEB
local m_itemCategory = nil

local m_page = 1
local m_maxPage = 1

local m_listItems = {}

local m_clearSearch = true
local m_searchText = ""

local m_midpts = {}

local m_gameItems = nil

local m_avatarItems = nil


-----------------------------------------------------------------
-- Function definitions
-----------------------------------------------------------------

-- When the menu is created
function onCreate()
	KEP_EventRegisterHandler( "editParamHandler", "EDIT_PARAM_EVENT", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "updateWOKInventoryHandler", "InventoryUpdateEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)

	-- Shift out of the way of the main editor
	MenuOffsetLocationThis(400, 0)
	requestInventory(GAME_PID)
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	setActivated(gHandles["imglistHoverBG"], false)
	setActivated(gHandles["imglistHover"], false)
	local hoverx = x+5
	local hovery = y - 34
	if Control_ContainsPoint(gHandles["lstItems"], x, y) ~= 0 then 
		for i,v in pairs(m_midpts) do
			if y <= (m_midpts[i]+5) and y>=(m_midpts[i]-5) then 
			
				local GLID = 0

				if m_itemsType == ITEMS_TYPE.WEB then
					GLID = ListBox_GetItemData( gHandles["lstItems"], (i-1) )
				else
					local value = ListBox_GetItemData( gHandles["lstItems"], (i-1) )

					if m_worldItems[tostring(value)] then
						GLID = m_worldItems[tostring(value)].GLID
					else
						return
					end					
				end

				if GLID ~= 0 then
					local eh = Image_GetDisplayElement(gHandles["imglistHover"])
					if eh ~= 0 then
						KEP_LoadIconTextureByID(GLID, eh, gDialogHandle)
					end
					
					Control_SetLocation(gHandles["imglistHoverBG"],hoverx,hovery)
					Control_SetLocation(gHandles["imglistHover"],(hoverx+24),(hovery+9))
					setActivated(gHandles["imglistHoverBG"],true)
					setActivated(gHandles["imglistHover"], true)
				end
			end 
		end
	end 
end

--Webcalls 
function inventoryWebCall()

	getPlayerInventory(WF.ACTION_ITEMS_PARAM, m_page, ITEMS_PER_PAGE, m_avatarItems, m_searchText, m_itemCategory, m_useType, nil, true)
	--log("--- m_item.value: ".. tostring(m_item.value))
	local web_address = GameGlobals.WEB_SITE_PREFIX..DYNAMIC_OBJECT_INFO_SUFFIX.."&globalId="..tostring(m_item.value)
	--log("--- web_address: ".. tostring(web_address).. "\nm_item.value: ".. tostring(m_item.value))
	makeWebCall(web_address, WF.GET_DYNAMIC_OBJECT_INFO)
end

function updateWOKInventoryHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if m_item.type ~= "UNID" then
		getPlayerInventory(WF.ACTION_ITEMS_PARAM, m_page, ITEMS_PER_PAGE, avatar, m_searchText, m_itemCategory, m_useType, nil, true)
	end
end

function BrowserPageReadyHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.ACTION_ITEMS_PARAM then
		local xmlData = KEP_EventDecodeString( event )
		
		-- Get the page number and search returned, and make sure it's still correct. 
		-- Otherwise, don't bother to load
		local s, e, pageReturned = string.find(xmlData, "<Page>(%d-)</Page>")
		pageReturned = tonumber(pageReturned) or 0
		
		local s, e, searchReturned = string.find(xmlData, "<Search>(.-)</Search>")
		searchReturned = searchReturned or ""
		
		-- and check search on this if later if added.
		if pageReturned == m_page then
			--Get the total number of items from the XML data and divide by items/page, update page numbers
			local s, e, totalItems = string.find(xmlData, "<TotalNumberRecords>(%d-)</TotalNumberRecords>")
			totalItems = tonumber(totalItems or 1)
			m_maxPage = math.ceil(totalItems/ITEMS_PER_PAGE)
			if (m_maxPage <= 1) then
			    setActivated(gHandles["btnPageNext"], false)
			end
			m_displayItems = {}

			if m_page == 1 and m_item.attribute ~= "GLID" and m_item.attribute ~= "meshGLID" and m_item.attribute ~= "middleStages" and m_item.attribute ~= "finalGLID" then
				--Insert NONE option at top of list
				table.insert(m_displayItems, {name = m_item.defaultSelect, GLID = 0})
			end
			

			---Hard code for media items until API to find if it can play media based on GLID is createds
			if m_item.type ~= "MEDIAPLAYER" then
			decodeInventoryItemsFromWeb( m_displayItems, xmlData, false )
			else 
				totalItems = 8
				m_maxPage = math.ceil(totalItems/ITEMS_PER_PAGE)
				if m_page == 1 then
					m_displayItems = {}
					table.insert(m_displayItems, {name = '13" Television', GLID = 750})
					table.insert(m_displayItems, {name = 'Flat Screen TV no stand', GLID = 4562959})
					table.insert(m_displayItems, {name = '56" Television', GLID = 753})
					table.insert(m_displayItems, {name = '600" Television', GLID = 3002})
					table.insert(m_displayItems, {name = "Boombox", GLID = 754})
				elseif m_page == 2 then
					m_displayItems = {}
					table.insert(m_displayItems, {name = "Large Light-Up Stereo", GLID = 2809})
					table.insert(m_displayItems, {name = "{DFDs} Big Screen Tv with Tunnel", GLID = 4612067})
					table.insert(m_displayItems, {name = "TV Cylinder 0005 H 16 BY__FANCY__", GLID = 4514586})
					table.insert(m_displayItems, {name = "TV 0600 16X9", GLID = 4606442})
					table.insert(m_displayItems, {name = "KTown Stage v3", GLID = 4701490})
				else
					m_displayItems = {}
				end
			end
			
			displayItems()
		end

		local eh = Image_GetDisplayElement(gHandles["imgCurrent"])
		if eh ~= 0 then
			KEP_LoadIconTextureByID(m_item.value, eh, gDialogHandle)
		end

		return KEP.EPR_CONSUMED

	elseif filter == WF.GET_DYNAMIC_OBJECT_INFO then
		local objectData = KEP_EventDecodeString( event )
		local returnCode = string.match(objectData, "<ReturnCode>(.-)</ReturnCode>")
		if tonumber(returnCode) ~= 0 then
			return
		end

		local name = Web.getDataByTag(objectData, "display_name", 		"(.-)")
		Static_SetText(gHandles["stcCurrentName"], name)
	end
end

--Framework event
function worldInventoryCall()	

	
	if not m_gameItems then return end

	local typeFilters = nil
	local behaviorTypes = nil

	if m_item.typeFilter and m_item.typeFilter ~= "all" then
		typeFilters = {}
		for k, v in string.gmatch(m_item.typeFilter, "[^,]+") do
			table.insert(typeFilters, k)
		end
	else

	end

	if m_item.behaviorFilter and m_item.behaviorFilter ~= "all" then
		behaviorTypes = {}
		for k, v in string.gmatch(m_item.behaviorFilter, "[^,]+") do
			table.insert(behaviorTypes, k)
		end
	end

	for UNID, properties in pairs(m_gameItems) do
		
		if not typeFilters and not behaviorTypes then
			if isLootItem(properties.itemType, properties.behavior) then
				m_worldItems[UNID] = properties
			end
		elseif typeFilters and not behaviorTypes then
			if isValidItem(properties.itemType, typeFilters) then
				m_worldItems[UNID] = properties
			end
		elseif not typeFilters and behaviorTypes then
			if isValidItem(properties.behavior, behaviorTypes) then
				m_worldItems[UNID] = properties
			end
		else
			if isValidItem(properties.itemType, typeFilters) and isValidItem(properties.behavior, behaviorTypes) then
				m_worldItems[UNID] = properties
			end
		end
	end

	if m_worldItems[tostring(m_item.value)] then
		local eh = Image_GetDisplayElement(gHandles["imgCurrent"])
		if eh ~= 0 then
			KEP_LoadIconTextureByID(m_worldItems[tostring(m_item.value)].GLID, eh, gDialogHandle)		
		end

		Static_SetText(gHandles["stcCurrentName"], m_worldItems[tostring(m_item.value)].name)
	end
	
	updateDisplayList()
end

function isValidItem(itemProperty, filters)
	for i, filter in pairs(filters) do
		if itemProperty == filter then
			return true
		end
	end
	return false
end

function isLootItem(itemType, behavior)
	if itemType == ITEM_TYPES.CHARACTER and behavior == "pet" then -- Pets are loot items
		return true
	elseif itemType == ITEM_TYPES.HARVESTABLE and behavior == "seed" then -- Seeds are loot items
		return true
	elseif	itemType == ITEM_TYPES.WEAPON or 
			itemType == ITEM_TYPES.AMMO or 
			itemType == ITEM_TYPES.ARMOR or 
			itemType == ITEM_TYPES.TOOL or 
			itemType == ITEM_TYPES.BLUEPRINT or 
			itemType == ITEM_TYPES.CONSUMABLE or 
			itemType == ITEM_TYPES.GENERIC or 
			itemType == ITEM_TYPES.PLACEABLE then
		return true
	end

	return false
end

--Event Handlers
function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem	
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems

	if m_itemsType == ITEMS_TYPE.FRAMEWORK then
		worldInventoryCall()
	end
end

function editParamHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- load initial table values into g_param
	local event = {}
	event.type = KEP_EventDecodeString(tEvent)
	event.typeFilter = KEP_EventDecodeString(tEvent)
	event.behaviorFilter = KEP_EventDecodeString(tEvent)
	event.attribute = KEP_EventDecodeString(tEvent)
	event.property = KEP_EventDecodeString(tEvent)
	event.itemName = KEP_EventDecodeString(tEvent)
	event.defaultSelect = KEP_EventDecodeString(tEvent)
	event.value = KEP_EventDecodeString(tEvent)

	--If this attribute has an index to return
	if KEP_EventMoreToDecode(tEvent) == 1 then
		event.index = KEP_EventDecodeString(tEvent)
	end

	--If this attribute has an index to return
	if KEP_EventMoreToDecode(tEvent) == 1 then
		event.baseAttribute = KEP_EventDecodeString(tEvent)
	end	
	
	m_item.type = event.type
	m_item.typeFilter = event.typeFilter
	m_item.behaviorFilter = event.behaviorFilter
	m_item.attribute = event.attribute
	m_item.property = event.property
	m_item.itemName = event.itemName
	m_item.defaultSelect = event.defaultSelect
	m_item.value = tonumber(event.value)
	m_item.index = event.index
	m_item.baseAttribute = event.baseAttribute

	--log("--- Framework_ItemSelectEditor editParamHandler m_item.type: ".. tostring(m_item.type).. "\nm_item.value: ".. tostring(m_item.value))

	if m_item.type == "MENS_EMOTE" or m_item.type == "WOMENS_EMOTE" or m_item.type == "OBJECT_ANIMATION" or m_item.type == "ALL_ANIMATIONS" then
		m_useType = USE_TYPE.ANIMATION
		if m_item.type == "MENS_EMOTE" then
			m_itemCategory = "Emotes For Men"
		elseif m_item.type == "WOMENS_EMOTE" then
			m_itemCategory = "Emotes For Women"
		elseif m_item.type == "OBJECT_ANIMATION" then
			m_itemCategory = "Object Animations"
		else
			m_itemCategory = ""
		end
		inventoryWebCall()
    elseif m_item.type == "ACCESSORY" then
		m_useType = USE_TYPE.EQUIP
		inventoryWebCall()
	elseif m_item.type == "CLOTHING" then
		m_itemCategory = ""
		m_avatarItems = "avatar"
		inventoryWebCall()
	elseif m_item.type == "MENS_CLOTHING" then
		m_itemCategory = "For Men|Accessories For Men"
		inventoryWebCall()
	elseif m_item.type == "WOMENS_CLOTHING" then
		m_itemCategory = "For Women|Accessories For Women"
		inventoryWebCall()
	elseif m_item.type == "OBJECT" or m_item.type == "MEDIAPLAYER" then
		m_useType = USE_TYPE.ADD_DYN_OBJ
		inventoryWebCall()
	elseif m_item.type == "PARTICLE" then
		m_useType = USE_TYPE.PARTICLE
		inventoryWebCall()
	elseif m_item.type == "SOUND" then
		m_useType = USE_TYPE.SOUND
		inventoryWebCall()
	elseif m_item.type == "UNID" then
		m_itemsType = ITEMS_TYPE.FRAMEWORK
		worldInventoryCall()
	elseif m_item.type == "OBJECT_ACCESSORY" then
		m_useType = USE_TYPE.ADD_DYN_OBJ.."|"..USE_TYPE.EQUIP
		inventoryWebCall()
	end
	
	Dialog_SetCaptionText( gDialogHandle, "  "..m_item.itemName )	
	
	Static_SetText(gHandles["stcDescription"], m_item.property)
end

function displayItems()
	ListBox_RemoveAllItems(gHandles["lstItems"])	
	local selections = 0
	local startIndex = 1
	local endIndex = ITEMS_PER_PAGE

	if m_itemsType == ITEMS_TYPE.FRAMEWORK then
		startIndex = ( startIndex + (ITEMS_PER_PAGE * (m_page - 1)) ) > 1 and startIndex + (ITEMS_PER_PAGE * (m_page - 1)) or 1
		endIndex = startIndex + ITEMS_PER_PAGE
	end

	for i = startIndex, endIndex do
		local item = m_displayItems[i]
		if item ~= nil then
			local displayName = item.name

			if m_itemsType == ITEMS_TYPE.WEB  and m_item.type ~= "MEDIAPLAYER" then
				ListBox_AddItem(gHandles["lstItems"], displayName, item.GLID)
			elseif  m_item.type == "MEDIAPLAYER" then
				--call from finding canPlayMovie might go here
				ListBox_AddItem(gHandles["lstItems"], displayName,  item.GLID)
			else
				ListBox_AddItem(gHandles["lstItems"], displayName, item.UNID)
			end	
		
			selections = selections + 1
		end
	end

	local list = gHandles["lstItems"]
	local lstWidth = Control_GetWidth(list) 
	local lstHeight = Control_GetHeight(list)
	local points = {}
	local start = 1
		for j =1,(selections*2) do
			if (j%2)==1 then
				points[start] = j
				start = start + 1
			end 
		end
	for j = 1,selections do
		m_midpts[j] = Control_GetLocationY(list) + (math.floor(lstHeight*(points[j]/10)))
	end
end

function btnCancel_OnButtonClicked(buttonHandle)
	DestroyMenu(gDialogHandle)
end

function btnClose_OnButtonClicked(buttonHandle)
	DestroyMenu(gDialogHandle)
end

function btnOk_OnButtonClicked(buttonHandle)
	local event = KEP_EventCreate( "CHANGED_PARAM_EVENT" )
	KEP_EventEncodeString(event, m_item.attribute)
	KEP_EventEncodeString(event, m_item.value)
	KEP_EventEncodeString(event, m_item.valueName)

	if m_item.type == "UNID" then
		KEP_EventEncodeString(event, "UNID")
	else
		KEP_EventEncodeString(event, "GLID")
	end

	if m_item.index then
		KEP_EventEncodeString(event, m_item.index)

		if m_item.baseAttribute then
			KEP_EventEncodeString(event, m_item.baseAttribute)
		end
	elseif m_item.type == "UNID" then
		local newGLID = m_worldItems[tostring(m_item.value)] and m_worldItems[tostring(m_item.value)].GLID or 0
		local description =  m_worldItems[tostring(m_item.value)] and m_worldItems[tostring(m_item.value)].description or ""

		if newGLID ~= nil and description ~= nil then
			KEP_EventEncodeString(event, newGLID)
			KEP_EventEncodeString(event, description)
		end
	end

	KEP_EventQueue( event )
	MenuCloseThis()
end

function btnPageNext_OnButtonClicked(buttonHandle)
	ListBox_ClearSelection(gHandles["lstItems"])
	m_midpts = {}
	m_page = m_page + 1
        if m_page <= 1 or m_maxPage == 1 then
			setActivated(gHandles["btnPagePrev"], false)
        else
            setActivated(gHandles["btnPagePrev"], true)
        end

	if m_page >= m_maxPage then
		m_page = m_maxPage
		setActivated(gHandles["btnPageNext"], false)
	end

	if m_itemsType == ITEMS_TYPE.WEB then
		inventoryWebCall()
	else
		displayItems()
	end
end

function btnPagePrev_OnButtonClicked(buttonHandle)
	ListBox_ClearSelection(gHandles["lstItems"])
	m_midpts = {}
	m_page = m_page - 1
        if m_page < m_maxPage then 
           setActivated(gHandles["btnPageNext"], true)
        end

	if m_page <= 1 then
	   m_page = 1
	   setActivated(gHandles["btnPagePrev"], false)
	end

	if m_itemsType == ITEMS_TYPE.WEB then
		inventoryWebCall()
	else
		displayItems()
	end
end

function lstItems_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	local GLID
	local itemValue
	if m_itemsType == ITEMS_TYPE.WEB then
		GLID = ListBox_GetItemData( gHandles["lstItems"], (sel_index) )
		itemValue = GLID
	else
		local value = ListBox_GetItemData( gHandles["lstItems"], (sel_index) )

		if m_worldItems[tostring(value)] then
			GLID = m_worldItems[tostring(value)].GLID
			itemValue = value
		else
			GLID = 0
			itemValue = 0
		end
	end

	local eh = Image_GetDisplayElement(gHandles["imgCurrent"])
	if eh ~= 0 then
		KEP_LoadIconTextureByID(GLID, eh, gDialogHandle)
	end
	
	local name = ListBox_GetItemText( listBoxHandle, sel_index )
	Static_SetText(gHandles["stcCurrentName"], name)
	m_item.valueName = name
	Dialog_ClearFocus(gDialogHandle)
	m_item.value = itemValue
end

----------------------------------------------------------------------
-- Search-Related Functions
----------------------------------------------------------------------
function updateDisplayList()
	m_displayItems = {}

	table.insert(m_displayItems, {UNID = 0, name = m_item.defaultSelect, GLID = 0})

	for itemUNID,item in pairs(m_worldItems) do		
		if string.find(string.lower(item.name), string.lower(m_searchText)) then
			table.insert(m_displayItems, {UNID = itemUNID, name = item.name, GLID = item.GLID})
		end
	end
	m_maxPage = math.ceil(#m_displayItems/ITEMS_PER_PAGE)

	displayItems()
end

function txtSearch_OnEditBoxChange( editboxHandle, text)
	m_searchText = text
	if m_searchText then
		local searchLen = string.len(m_searchText)		

		if searchLen ~= 1 then
			if m_itemsType == ITEMS_TYPE.WEB then
				inventoryWebCall()	
			else
				updateDisplayList()
			end					
		end

		if searchLen > 0 then
		--	Control_SetVisible(gHandles["imgClearSearch"], true)
			setActivated(gHandles["imgClearSearch"],true)
			m_page = 1
			setActivated(gHandles["btnPagePrev"], false)
			
			if m_page == m_maxPage then 
				setActivated(gHandles["btnPageNext"], false)
			else 
				setActivated(gHandles["btnPageNext"], true)
			end 
		end
	end
end

function txtSearch_OnFocusIn(editBoxHandle)
	-- On enter, set the text to blank
	if m_clearSearch then
		EditBox_SetText(editBoxHandle, "", true)
	end
	m_clearSearch = false
end

function txtSearch_OnFocusOut(editBoxHandle)
	local search = EditBox_GetText(editBoxHandle)
	
	-- On exit, if blank, reset the text to "Search|"
	if search == "" or m_clearSearch then
		m_clearSearch = true
		EditBox_SetText(editBoxHandle, "Search", false)
	else
		m_clearSearch = false
	end
end

function imgClearSearch_OnLButtonUp(ctrlHandle)
	if Control_GetVisible(ctrlHandle) then
		Control_SetVisible(ctrlHandle, false)
		m_clearSearch = true

		local searchBox = gHandles["txtSearch"]
		EditBox_ClearText(searchBox)
		txtSearch_OnFocusOut(searchBox)
		txtSearch_OnEditBoxChange( searchBox, "")
		Dialog_ClearFocus(gDialogHandle)

		if m_page < m_maxPage then
			setActivated(gHandles["btnPageNext"], true)
		end
	end
end

function setActivated(controlHandle, value)
	if controlHandle ~= nil then
		Control_SetVisible(controlHandle, value)
		Control_SetEnabled(controlHandle, value)
	end
end