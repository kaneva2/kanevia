--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

-- HomePresetSelectEvent Data (g_aptGlid, g_aptId)
g_aptGlid = 4518970 -- green matrix zone glid
g_aptId = 54 -- matrix zone id

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "HomePresetSelectEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- Request the default home template info from the web
	makeWebCall(WEBCALL_DEFAULT_HOME_TEMPLATE, WF.DEFAULT_HOME_TEMPLATE_INFO)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.DEFAULT_HOME_TEMPLATE_INFO then
		local responseXml = KEP_EventDecodeString(event)
		local returnCode = tonumber(string.match(responseXml, "<ReturnCode>(.-)</ReturnCode>") or "-1")
		local returnDesc = string.match(responseXml, "<ReturnDescription>(.-)</ReturnDescription>")

		-- Grab the zone info if we can. Otherwise we'll fall back to defaults.
		if returnCode == 0 then
			g_aptGlid = tonumber(string.match(responseXml, "<GlobalId>(.-)</GlobalId>"))
			g_aptId = tonumber(string.match(responseXml, "<ZoneIndexPlain>(.-)</ZoneIndexPlain>"))

			Log("HomePresetSelect.lua::BrowserPageReadyHandler: Retrieved home template defaults successfully. g_aptGlid=" .. g_aptGlid .. " g_aptId=" .. g_aptId)
		else
			Log("HomePresetSelect.lua::BrowserPageReadyHandler: Failed to retrieve home template defaults. Falling back to g_aptGlid=" .. g_aptGlid .. " g_aptId=" .. g_aptId)
		end

		-- Launch AvatarPresetSelect Menu
		MenuOpen("AvatarPresetSelect.xml")

		-- Send HomePresetSelectEvent to AvatarPresetSelect Menu
		local ev = KEP_EventCreate( "HomePresetSelectEvent")
		KEP_EventEncodeNumber(ev, g_aptGlid)
		KEP_EventEncodeNumber(ev, g_aptId)
		KEP_EventQueue( ev)
	end
end