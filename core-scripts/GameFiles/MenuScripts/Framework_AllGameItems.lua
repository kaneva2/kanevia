--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_AllGameItems.lua
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_InventoryHelper.lua")
dofile("..\\MenuScripts\\Lib_GameItemBootstrap.lua")
dofile("BuildPlace.lua")
dofile("..\\MenuScripts\\Lib_GameDefinitions.lua")

CLICKED_CONTROLS = {"btnAction1", "btnAction2", "btnPublish", "btnInfo", "btnDuplicate", "btnDelete"}

local CLICKED_MAIN_ACTIONS = {EDIT = "edit", SPAWN = "spawn", PLACE = "place"}

local ITEMS_PER_PAGE = 20

local INVENTORY_MODES = {WOK = "wok", WORLD = "world"}

local NOT_IN_WORLD_COLOR = {a = 255, r = 255, g = 255, b = 255}
local IN_WORLD_COLOR = {a = 255, r = 122, g = 146, b = 197}
local GREY_SEARCH_COLOR = {a = 255, r = 140, g = 140, b = 140}

local COLORS_VALUES =	{	["Common"] = {a = 255, r = 255, g = 255, b = 255},
							["Rare"] = {a = 255, r = 0, g = 150, b = 255},
							["Very Rare"] = {a = 255, r = 174, g = 0, b = 255},
							["Extremely Rare"] = {a = 255, r = 255, g = 144, b = 0},
							["Never"] = {a = 255, r = 255, g = 144, b = 0}
						}

local ITEM_LEVEL_COORDS = {
							{left = 0, right = 15, top = 146, bottom = 161},
							{left = 16, right = 31, top = 146, bottom = 161},
							{left = 32, right = 47, top = 146, bottom = 161},
							{left = 0, right = 15, top = 162, bottom = 177},
							{left = 16, right = 31, top = 162, bottom = 177}
}

local ITEM_LEVEL_RARITY_OFFSET = {["Common"] = 0, ["Rare"] = 48, ["Very Rare"] = 96, ["Extremely Rare"] = 144, ["Never"] = 144}

local ITEM_RARITY_BG = {	["Common"] = {left = 208, right = 292, top = 167, bottom = 251},
							["Rare"] = {left = 293, right = 377, top = 167, bottom = 251},
							["Very Rare"] = {left = 378, right = 462, top = 167, bottom = 251},
							["Extremely Rare"] = {left = 378, right = 462, top = 82, bottom = 166},
							["Never"] = {left = 378, right = 462, top = 82, bottom = 166}
						}


--New Item Configs
local NEW_ITEM_CONFIGS = {}

NEW_ITEM_CONFIGS["loot"] = {buttonText = "New Loot", types = { {name = "New Weapon", prefabType = "weapon"}, {name = "New Paintbrush", prefabType = "paintbrush"}, {name = "New 2-Person Emote Item", prefabType = "p2pEmoteItem"}, {name = "New Emote Item", prefabType = "emoteItem"}, {name = "New Ammo", prefabType = "ammo"}, {name = "New Armor", prefabType = "armor"},  {name = "New Consumable", prefabType = "consumable"}, {name = "New Generic Item", prefabType = "generic"}} }
NEW_ITEM_CONFIGS["placeable"] = {buttonText = "New Placeable", types = { {name = "New Construction Item", prefabType = "placeable_loot"}, {name = "New Crafting Helper", prefabType = "campfire"}, {name = "New Door", prefabType = "door"}, {name = "New Container", prefabType = "loot"}, {name = "New Trap", prefabType = "trap"}, {name = "New Repair Station", prefabType = "repair"}, {name = "New Land Claim Flag", prefabType = "claim"}, {name = "New Media Player", prefabType = "media"}, {name = "New Seating Item", prefabType = "seat"}, {name = "New Platform System", prefabType = "mover"},{name = "New Teleporter", prefabType = "teleporter"},{name = "New World to World Teleporter", prefabType = "worldTeleporter"}, {name = "New Linked Zone Teleporter", prefabType = "zoneTeleporter"}}} --, {name = "New Vehicle", prefabType = "wheeled_vehicle"}}} -- Car stuff to be added
NEW_ITEM_CONFIGS["character"] = {buttonText = "New Character", types = { {name = "New Monster", prefabType = "monster"}, {name = "New Animal", prefabType = "animal"}, {name = "New NPC", prefabType = "actor"}, {name = "New Vendor", prefabType = "vendor"}, {name = "New Timed Vendor", prefabType = "timedVendor"}, {name = "New Random Vendor", prefabType = "randomVendor"}, {name = "New Credits/Rewards Vendor", prefabType = "creditsVendor"}, {name = "New Gem Vendor", prefabType = "gemVendor"}, {name = "New Clothing Vendor", prefabType = "clothing"}, {name = "New Pet", prefabType = "pet"}}}
NEW_ITEM_CONFIGS["quest"] = {buttonText = "New Quest", types = { {name = "New Quest", prefabType = "quest"}, {name = "New Quest Giver", prefabType = "quest_giver"}} }
NEW_ITEM_CONFIGS["recipe"] = {buttonText = "New Recipe", types = { {name = "New Recipe", prefabType = "recipe"}, {name = "New Blueprint", prefabType = "blueprint"}} }
NEW_ITEM_CONFIGS["spawner"] = {buttonText = "New Spawner", types = { {name = "New Loot Spawner", prefabType = "spawner_loot"}, {name = "New Character Spawner", prefabType = "spawner_character"}, {name = "New Harvestable Spawner", prefabType = "spawner_harvestable"}, {name = "New Single Item Spawner", prefabType = "spawner_loot_limit"}} }
NEW_ITEM_CONFIGS["harvestable"] = {buttonText = "New Harvestable", types = { {name = "New Harvestable", prefabType = "harvestable"}, {name = "New Seed", prefabType = "seed"} }}
NEW_ITEM_CONFIGS["pathing"] = {buttonText = "New Pathing Object", types = { {name = "New Pathing object", prefabType = "path"} }}

local m_flagCount = {}
m_flagCount["playerBuild enabled"] = 0
m_flagCount["playerBuild disabled"] = 0
m_flagCount["pvp enabled"] = 0
m_flagCount["pvp disabled"] = 0
m_flagCount["playerDestruction enabled"] = 0
m_flagCount["playerDestruction disabled"] = 0

local TOOLTIP_TABLE = {header = "name", body = "description"}
local TYPE_FILTER = "allGameItems"

local GI_LOAD_MESSAGE1 = "You have reached the limit of game objects in your world."
local GI_LOAD_MESSAGE2 = "Free some space by removing game objects."

g_displayTable = {}
local m_mode = INVENTORY_MODES.WOK
local m_worldInventory = {}
local m_fullDisplayTable = {}
local m_tooltipsEnabled = true

local m_searchText = ""
local m_page = 1
local m_filter = "all"

local m_newItemType = "none"
local m_newItemTrayOpen = false
local m_newItemTypeMouse = false

local m_itemGoto = nil
local m_pendingWebCheck = false
local m_placingGameItemUNID

local m_frameworkActivated
local m_playerModeActivated

local m_batchFinished = false

local FLASH_TIME 				= 0.15
local m_flashCount				= 0
local m_flashing				= false
local m_elapsedTime 			= 0

local m_worldCheckBox

-- Check-list before an Item can be queued for publishing / Copying to Inventory
local m_publishQueue = {["GLIDsextracted"] = false,
						["extractedGLIDs"] = {},
						["nameChecked"] = false,
						["UNID"] = 0,
						["item"] = {}
					   }


local m_editorName = {[ITEM_TYPES.WEAPON] = "Weapon",
					  [ITEM_TYPES.ARMOR ] = "Armor",
					  [ITEM_TYPES.TOOL ] = "Tool",
					  [ITEM_TYPES.AMMO ] = "Ammo",
					  [ITEM_TYPES.CONSUMABLE ] = "Consumable",
					  [ITEM_TYPES.GENERIC ] = "Generic",
					  [ITEM_TYPES.CHARACTER ] = "Character",
					  [ITEM_TYPES.PLACEABLE ] = "Placeable",
					  [ITEM_TYPES.FLAG ] = "Flag",
					  [ITEM_TYPES.SHORTCUT ] = "Shortcut",
					  [ITEM_TYPES.SPAWNER ] = "Spawner",
					  [ITEM_TYPES.HARVESTABLE ] = "Harvestable",
					  [ITEM_TYPES.RECIPE ] = "Recipe",
					  [ITEM_TYPES.BLUEPRINT ] = "Blueprint",
					  [ITEM_TYPES.QUEST ] = "Quest",
					  [ITEM_TYPES.ARMOR ] = "Armor"
					}
					

-- When the menu is created
function onCreate()
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )

	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("gameFilterHandler", "FRAMEWORK_GAMING_FILTER", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onGotoItem", "UNIFIED_GOTO_GAME_ITEM", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("enableTooltipsHandler", "UNIFIED_ENABLE_TOOLTIPS", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("disableTooltipsHandler", "UNIFIED_DISABLE_TOOLTIPS", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("returnFrameworkStateHandler", "FrameworkState", KEP.MED_PRIO )
	KEP_EventRegisterHandler("setGameItemMode", "UNIFIED_SET_GAME_ITEM_MODE", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)

	Events.registerHandler("FRAMEWORK_RETURN_GAME_ITEM_GLIDS", handleExtractedGameItemGLIDs)
	Events.registerHandler("FRAMEWORK_PLACE_GAME_ITEM_CONFIRMATION", placeGameItemConfirmation)

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()	
	InventoryHelper.initializeInventory("g_displayTable", false, ITEMS_PER_PAGE)
	InventoryHelper.initializeHover()
	InventoryHelper.initializeClicked(CLICKED_CONTROLS)
	InventoryHelper.initializePageNumbers()
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")

	requestInventory(GAME_PID)
	KEP_EventCreateAndQueue("GetFrameworkState")

	m_worldCheckBox = Dialog_GetControl(gDialogHandle, "chkViewGameSystem")
	inventoryUpdated(InventoryHelper.page, InventoryHelper.searchText, InventoryHelper.category, InventoryHelper.subCategory)

	EditBox_SetTextColor( gHandles["txtSearch"], GREY_SEARCH_COLOR )

	-- Register New Item Button Handlers
	for i = 1, 13 do
		_G["btnNewItemSub" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			local s, e, index = string.find(Control_GetName(btnHandle), "(%d+)")
			
			if m_newItemTrayOpen and m_newItemType and NEW_ITEM_CONFIGS[m_newItemType] then
				if NEW_ITEM_CONFIGS[m_newItemType].types and NEW_ITEM_CONFIGS[m_newItemType].types[tonumber(index)] then
					local prefabType = NEW_ITEM_CONFIGS[m_newItemType].types[tonumber(index)].prefabType
					createNewItem(prefabType)
				end
			end
		end
		_G["btnNewItemSub" .. tostring(i) .. "_OnMouseEnter"] = function(btnHandle)

			m_newItemTypeMouse = true
		end
	end
end

function onDestroy()
	InventoryHelper.destroy()
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
	InventoryHelper.onButtonDown(x, y)
end

function Dialog_OnRender(dialogHandle, fElapsedTime)

	m_elapsedTime = m_elapsedTime + fElapsedTime
	if m_flashing and m_flashCount < 3 then
		if m_elapsedTime >  FLASH_TIME  * 2 then
			Control_SetVisible(gHandles["imgItemBroken"], true)
			m_elapsedTime = 0
			m_flashCount = m_flashCount + 1
		elseif m_elapsedTime > FLASH_TIME then
			Control_SetVisible(gHandles["imgItemBroken"], false)
		end
	else
		if m_elapsedTime > FLASH_TIME then
			m_flashing = false
			m_flashCount = 0
			Control_SetVisible(gHandles["imgItemBroken"], false)
		end
	end
end

-------------------------------------------------
-------------------------------------------------
-- Input Functions
-------------------------------------------------
-------------------------------------------------

function chkViewGameSystem_OnCheckBoxChanged( checkboxHandle, checked )
	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	if checked == 1 then
		KEP_EventEncodeString(ev, "gaming")
	elseif checked == 0 then
		m_mode = INVENTORY_MODES.WOK		
		InventoryHelper.gotoPage(1)
		InventoryHelper.clearClicked()		
			--Control_SetVisible(gHandles["imgTopBarBlue"], false)
		updateNewItemDisplay(m_newItemType)
		KEP_EventEncodeString(ev, "wokGaming")
		requestWebInventory()
	end
	KEP_EventQueue(ev)

	local event = KEP_EventCreate("FRAMEWORK_WORLD_GAME_MODE")
	KEP_EventEncodeNumber(event, checked)
	KEP_EventQueue(event)
end

function btnAction1_OnButtonClicked(buttonHandle)
	if allowAction(true) == false then return end

	if m_mode == INVENTORY_MODES.WOK then
		local itemUNID = getItemUNIDInWorld(InventoryHelper.selectedIndex)

		if itemUNID then
			local action = getMainAction(itemUNID)
			if action == CLICKED_MAIN_ACTIONS.SPAWN then
				placeWorldItemSpawner(itemUNID)
			elseif action == CLICKED_MAIN_ACTIONS.PLACE then
				placeWorldItem(itemUNID)
			end
		else
			local item = g_displayTable[InventoryHelper.selectedIndex]
			addGameItemFromWOK(item)
		end
	elseif m_mode == INVENTORY_MODES.WORLD then
		local itemUNID = getUNIDFromIndex(InventoryHelper.selectedIndex)
		local action = getMainAction(itemUNID)
		if action == CLICKED_MAIN_ACTIONS.EDIT then
			editWorldItem(itemUNID)
		elseif action == CLICKED_MAIN_ACTIONS.SPAWN then
			placeWorldItemSpawner(itemUNID)
		elseif action == CLICKED_MAIN_ACTIONS.PLACE then
			placeWorldItem(itemUNID)
		end
	end
end

function btnAction2_OnButtonClicked(buttonHandle)
	if allowAction(true) == false then return end

	if m_mode == INVENTORY_MODES.WOK then
		local itemUNID = getItemUNIDInWorld(InventoryHelper.selectedIndex)
		editWorldItem(itemUNID)
	elseif m_mode == INVENTORY_MODES.WORLD then
		local itemUNID = getUNIDFromIndex(InventoryHelper.selectedIndex)
		editWorldItem(itemUNID)
	end
end

function btnInfo_OnButtonClicked(buttonHandle)
	if MenuIsClosed("InventoryInfo.xml") then
		local handle = MenuOpenModal("InventoryInfo.xml")
		item = g_displayTable[InventoryHelper.selectedIndex]

		local ev = KEP_EventCreate("SendGLIDEvent")
		KEP_EventEncodeNumber(ev, item.GIGLID or item.GLID)
		KEP_EventEncodeNumber(ev, item.inventory_sub_type)
		KEP_EventEncodeString(ev, item.display_name or item.name)
		KEP_EventEncodeString(ev, item.description or "")
		KEP_EventEncodeString(ev, item.creator or "")
		KEP_EventEncodeString(ev, "") -- Menu sent param

		if item.existsOnShop ~= nil then
			KEP_EventEncodeString(ev, tostring(item.existsOnShop))
		end

		KEP_EventQueue(ev)
	end
end

function btnPublish_OnButtonClicked(buttonHandle)
	if allowAction(true) == false then return end
	publishWorldItem()
end

function btnDuplicate_OnButtonClicked(buttonHandle)
	if allowAction(true) == false then return end
	duplicateWorldItem()
end

function btnDelete_OnButtonClicked(buttonHandle)
	if allowAction(true) == false then return end
	deleteWorldItem()
end

function btnNewItem_OnButtonClicked(buttonHandle)
	if allowAction(true) == false then return end
	if m_newItemType and m_newItemType ~= "none" and NEW_ITEM_CONFIGS[m_newItemType] then
		if NEW_ITEM_CONFIGS[m_newItemType].types then
			if m_newItemTrayOpen then
				closeNewItemTray()
			else
				openNewItemTray()
			end
		else
			createNewItem(m_newItemType)
		end		
	end
end

function btnNewItem_OnMouseEnter(buttonHandle)
	Control_SetVisible(gHandles["imgPlusIconGlow"], true)
end

function btnNewItem_OnMouseLeave(buttonHandle)
	Control_SetVisible(gHandles["imgPlusIconGlow"], false)
end

function btnNewItemRollOff_OnMouseEnter(btnHandle)
	if m_newItemTrayOpen  and m_newItemTypeMouse then
		closeNewItemTray()
	end
end

function btnShop_OnButtonClicked( buttonHandle )
	if Control_GetVisible(buttonHandle) then
		MenuClose("UnifiedInventory.xml")
		MenuCloseThis()
		launchWebBrowser(GameGlobals.WEB_SITE_PREFIX_SHOP)
	end
end

-------------------------------------------------
-------------------------------------------------
-- World Permissions Checking
-------------------------------------------------
-------------------------------------------------


-- Handles the return for the current Framework state
function returnFrameworkStateHandler( dispatcher, fromNetid, event, eventid, filter, objectid )	
	local frameworkState = cjson.decode(Event_DecodeString(event))
	if frameworkState.enabled ~= nil then
		local frameworkEnabled = frameworkState.enabled

		m_frameworkActivated = frameworkEnabled
	end

	if not m_frameworkActivated then
		Control_SetVisible(gHandles["chkViewGameSystem"], false)
		Control_SetEnabled(gHandles["chkViewGameSystem"], false)

		m_mode = INVENTORY_MODES.WOK		
		InventoryHelper.gotoPage(1)
		InventoryHelper.clearClicked()		
		Control_SetVisible(gHandles["imgTopBarBlue"], false)
		updateNewItemDisplay(m_newItemType)
	else
		Control_SetVisible(gHandles["chkViewGameSystem"], true)
		Control_SetEnabled(gHandles["chkViewGameSystem"], true)
			
		CheckBox_SetChecked( m_worldCheckBox, m_mode == INVENTORY_MODES.WORLD )
	end


	-- Player mode set
	if frameworkState.playerMode ~= nil then
		m_playerModeActivated = frameworkState.playerMode == "Player"		
	end

	handleBorderColor()
end

function handleBorderColor()
	local showRed = false
	--local showBlue = false

	showRed = allowAction() == false

	Control_SetVisible(gHandles["imgTopBarRed"], showRed)
	Control_SetEnabled(gHandles["stcMessage"], showRed)
	Control_SetVisible(gHandles["stcMessage"], showRed)

end

function allowAction(message)
	local allowed = KEP_IsOwnerOrModerator() --and not m_playerModeActivated

	if not allowed and message then
		KEP_MessageBox("You must be a moderator or owner in creator mode")
	end
	
	return allowed
end

-------------------------------------------------
-------------------------------------------------
-- Inventory Helper Functions
-------------------------------------------------
-------------------------------------------------

function inventoryUpdated(page, search, category)
	m_page = page
	m_searchText = search

	if m_mode == INVENTORY_MODES.WOK then
		requestWebInventory()
	elseif m_mode == INVENTORY_MODES.WORLD then
		formatDisplayTable()
		updateInventoryMenu()
	end

	if m_newItemTrayOpen then
		closeNewItemTray()
	end
end

function itemClicked(itemNumber)

	if KEP_IsOwnerOrModerator() then
		if m_mode == INVENTORY_MODES.WOK then
			updateClickedControlsWOKItem(itemNumber)
		elseif m_mode == INVENTORY_MODES.WORLD then
			updateClickedControlsWorldItem(itemNumber)
		end
	else
		Control_SetVisible(gHandles["btnInfo"], false)
		Control_SetEnabled(gHandles["btnInfo"], false)

		Control_SetVisible(gHandles["btnDelete"], false)
		Control_SetEnabled(gHandles["btnDelete"], false)

		Control_SetVisible(gHandles["btnPublish"], false)
		Control_SetEnabled(gHandles["btnPublish"], false)

		Control_SetVisible(gHandles["btnDuplicate"], false)
		Control_SetEnabled(gHandles["btnDuplicate"], false)

		Control_SetVisible(gHandles["imgClicked"], false)

		Control_SetVisible(gHandles["btnAction1"], false)
		Control_SetEnabled(gHandles["btnAction1"], false)

		Control_SetVisible(gHandles["btnAction2"], false)
		Control_SetEnabled(gHandles["btnAction2"], false)

		m_flashing = true
		Control_SetLocationY(gHandles["imgItemBroken"], Control_GetLocationY(gHandles["imgClicked"]) + 5)
		Control_SetLocationX(gHandles["imgItemBroken"], Control_GetLocationX(gHandles["imgClicked"]) + 6)
		Control_SetVisible(gHandles["imgItemBroken"], true)
	end

	if m_newItemTrayOpen then
		closeNewItemTray()
	end
end

function updateClickedControlsWOKItem(itemNumber)
	local item = g_displayTable[itemNumber]
	local inWorld = getItemUNIDInWorld(itemNumber) ~= nil

	Control_SetVisible(gHandles["btnInfo"], true)
	Control_SetEnabled(gHandles["btnInfo"], true)

	Control_SetVisible(gHandles["btnDelete"], false)
	Control_SetEnabled(gHandles["btnDelete"], false)

	Control_SetVisible(gHandles["btnPublish"], false)
	Control_SetEnabled(gHandles["btnPublish"], false)

	Control_SetVisible(gHandles["btnDuplicate"], false)
	Control_SetEnabled(gHandles["btnDuplicate"], false)

	setclickedControlMode(1)
	Static_SetText(gHandles["btnAction1"], "Place")
	changeButtonFont(gHandles["btnAction1"], 12)
end

function updateClickedControlsWorldItem(itemNumber)
	local UNID = getUNIDFromIndex(itemNumber)
	local item = m_worldInventory[UNID]
	local properties = item.properties

	if item then
		Control_SetVisible(gHandles["btnInfo"], false)
		Control_SetEnabled(gHandles["btnInfo"], false)

		Control_SetVisible(gHandles["btnDelete"], true)
		Control_SetEnabled(gHandles["btnDelete"], true)

		Control_SetVisible(gHandles["btnPublish"], true)
		Control_SetEnabled(gHandles["btnPublish"], true)

		Control_SetVisible(gHandles["btnDuplicate"], true)
		Control_SetEnabled(gHandles["btnDuplicate"], true)

		if item.itemType == ITEM_TYPES.WEAPON or item.itemType == ITEM_TYPES.ARMOR or item.itemType == ITEM_TYPES.TOOL or item.itemType == ITEM_TYPES.AMMO or item.itemType == ITEM_TYPES.CONSUMABLE or item.itemType == ITEM_TYPES.GENERIC  then

			setclickedControlMode(2)
			Static_SetText(gHandles["btnAction1"], "Create Spawn")
			changeButtonFont(gHandles["btnAction1"], 10)

		elseif item.itemType == ITEM_TYPES.CHARACTER then
			if item.behavior == "monster" or 
				item.behavior == "pet" or
				item.behavior == "animal" then

				setclickedControlMode(2)
				Static_SetText(gHandles["btnAction1"], "Create Spawn")
				changeButtonFont(gHandles["btnAction1"], 10)
			else
				setclickedControlMode(2)
				Static_SetText(gHandles["btnAction1"], "Place")
				changeButtonFont(gHandles["btnAction1"], 12)
				Control_SetVisible(gHandles["btnPublish"], false)
				Control_SetEnabled(gHandles["btnPublish"], false)
				Control_SetVisible(gHandles["btnDuplicate"], true)
				Control_SetEnabled(gHandles["btnDuplicate"], true)
				Control_SetEnabled(gHandles["btnDelete"], true)
				Control_SetVisible(gHandles["btnDelete"], true)
			end 
		elseif item.itemType == ITEM_TYPES.PLACEABLE then

			--if item.behavior == "wheeled_vehicle" or item.behavior == "eventFlag" then 
			if item.behavior == "eventFlag" then
				setclickedControlMode(1)

				Control_SetVisible(gHandles["btnPublish"], false)
				Control_SetEnabled(gHandles["btnPublish"], false)

				Control_SetVisible(gHandles["btnDuplicate"], false)
				Control_SetEnabled(gHandles["btnDuplicate"], false)
			elseif item.behavior == "wheeled_vehicle" then
				setclickedControlMode(2)
				Control_SetVisible(gHandles["btnPublish"], false)
				Control_SetEnabled(gHandles["btnPublish"], false)
			else
				setclickedControlMode(2)
			end
			Static_SetText(gHandles["btnAction1"], "Place")
			changeButtonFont(gHandles["btnAction1"], 12)

		elseif item.itemType == ITEM_TYPES.SPAWNER then

			if item.behavior == "spawner_player" then
				setclickedControlMode(1)

				Control_SetVisible(gHandles["btnDuplicate"], false)
				Control_SetEnabled(gHandles["btnDuplicate"], false)

				Control_SetVisible(gHandles["btnDelete"], false)
				Control_SetEnabled(gHandles["btnDelete"], false)
			else
				setclickedControlMode(2)
			end

			Static_SetText(gHandles["btnAction1"], "Place")
			changeButtonFont(gHandles["btnAction1"], 12)

			Control_SetVisible(gHandles["btnPublish"], false)
			Control_SetEnabled(gHandles["btnPublish"], false)

		elseif item.itemType == ITEM_TYPES.FLAG then

			local lastOfItsKind = true
			local flagIndex = getFlagType(item)
			if flagIndex ~= "invalid flag" and m_flagCount[flagIndex] > 1 then
				lastOfItsKind = false
			else
				lastOfItsKind = true
			end

			setclickedControlMode(2)
			Static_SetText(gHandles["btnAction1"], "Place")
			changeButtonFont(gHandles["btnAction1"], 12)
			Control_SetVisible(gHandles["btnPublish"], false)
			Control_SetEnabled(gHandles["btnPublish"], false)
			Control_SetVisible(gHandles["btnDuplicate"], true)
			Control_SetEnabled(gHandles["btnDuplicate"], true)

			if lastOfItsKind == false then
				Control_SetEnabled(gHandles["btnDelete"], true)
				Control_SetVisible(gHandles["btnDelete"], true)
			else
				Control_SetEnabled(gHandles["btnDelete"], false)
				Control_SetVisible(gHandles["btnDelete"], false)
			end

		elseif item.itemType == ITEM_TYPES.SHORTCUT then
			setclickedControlMode(1)
			Static_SetText(gHandles["btnAction1"], "Place")
			changeButtonFont(gHandles["btnAction1"], 12)
			Control_SetVisible(gHandles["btnPublish"], false)
			Control_SetEnabled(gHandles["btnPublish"], false)
			Control_SetVisible(gHandles["btnDuplicate"], false)
			Control_SetEnabled(gHandles["btnDuplicate"], false)
			Control_SetVisible(gHandles["btnDelete"], false)
			Control_SetEnabled(gHandles["btnDelete"], false)
		elseif item.itemType == ITEM_TYPES.HARVESTABLE then

			if item.behavior == "seed" then
				setclickedControlMode(2)
				Static_SetText(gHandles["btnAction1"], "Create Spawn")
				changeButtonFont(gHandles["btnAction1"], 10)
			else
				setclickedControlMode(2)
				Static_SetText(gHandles["btnAction1"], "Create Spawn")
				changeButtonFont(gHandles["btnAction1"], 10)
				Control_SetVisible(gHandles["btnPublish"], false)
				Control_SetEnabled(gHandles["btnPublish"], false)
			end

		elseif item.itemType == ITEM_TYPES.RECIPE or item.itemType == ITEM_TYPES.BLUEPRINT then

			setclickedControlMode(1)
			Static_SetText(gHandles["btnAction1"], "Edit")
			changeButtonFont(gHandles["btnAction1"], 12)
			Control_SetVisible(gHandles["btnPublish"], false)
			Control_SetEnabled(gHandles["btnPublish"], false)

		elseif item.itemType == ITEM_TYPES.QUEST then

			setclickedControlMode(1)
			Static_SetText(gHandles["btnAction1"], "Edit")
			changeButtonFont(gHandles["btnAction1"], 12)
			Control_SetVisible(gHandles["btnPublish"], false)
			Control_SetEnabled(gHandles["btnPublish"], false)
			Control_SetVisible(gHandles["btnDuplicate"], false)
			Control_SetEnabled(gHandles["btnDuplicate"], false)

		elseif item.itemType == ITEM_TYPES.PATH then
			setclickedControlMode(2)
			Static_SetText(gHandles["btnAction1"], "Create Path")
			changeButtonFont(gHandles["btnAction1"], 10)
			Control_SetVisible(gHandles["btnPublish"], false)
			Control_SetEnabled(gHandles["btnPublish"], false)
		end
	end
end

function setclickedControlMode(buttonNumber, button1Name, button1Size, button2Name, button2Size)
	local element = Image_GetDisplayElement(gHandles["imgClicked"])

	Control_SetVisible(gHandles["btnAction1"], true)
	Control_SetEnabled(gHandles["btnAction1"], true)

	if buttonNumber == 1 then
		Element_SetCoords(element,1,1,94,114)

		Control_SetVisible(gHandles["btnAction2"], false)
		Control_SetEnabled(gHandles["btnAction2"], false)

		Control_SetSize(gHandles["imgClicked"], Control_GetWidth(gHandles["imgClicked"]), 114)

		Control_SetLocation(gHandles["btnInfo"], Control_GetLocationX(gHandles["btnInfo"]), Control_GetLocationY(gHandles["imgClicked"])+95)
		Control_SetLocation(gHandles["btnDelete"], Control_GetLocationX(gHandles["btnDelete"]), Control_GetLocationY(gHandles["imgClicked"])+95)
		Control_SetLocation(gHandles["btnPublish"], Control_GetLocationX(gHandles["btnPublish"]), Control_GetLocationY(gHandles["imgClicked"])+95)
		Control_SetLocation(gHandles["btnDuplicate"], Control_GetLocationX(gHandles["btnDuplicate"]), Control_GetLocationY(gHandles["imgClicked"])+95)
		
	elseif buttonNumber == 2 then
		Element_SetCoords(element,101,1,194,138)

		Control_SetVisible(gHandles["btnAction2"], true)
		Control_SetEnabled(gHandles["btnAction2"], true)

		Control_SetSize(gHandles["imgClicked"], Control_GetWidth(gHandles["imgClicked"]), 137)

		Control_SetLocation(gHandles["btnInfo"], Control_GetLocationX(gHandles["btnInfo"]), Control_GetLocationY(gHandles["imgClicked"])+117)
		Control_SetLocation(gHandles["btnDelete"], Control_GetLocationX(gHandles["btnDelete"]), Control_GetLocationY(gHandles["imgClicked"])+117)
		Control_SetLocation(gHandles["btnPublish"], Control_GetLocationX(gHandles["btnPublish"]), Control_GetLocationY(gHandles["imgClicked"])+117)
		Control_SetLocation(gHandles["btnDuplicate"], Control_GetLocationX(gHandles["btnDuplicate"]), Control_GetLocationY(gHandles["imgClicked"])+117)		
	end
end

function getMainAction(itemUNID)
	if itemUNID and m_worldInventory[tostring(itemUNID)] then
		local item = m_worldInventory[tostring(itemUNID)]
		if item.itemType == ITEM_TYPES.WEAPON or item.itemType == ITEM_TYPES.ARMOR or item.itemType == ITEM_TYPES.TOOL or item.itemType == ITEM_TYPES.AMMO or item.itemType == ITEM_TYPES.CONSUMABLE or item.itemType == ITEM_TYPES.GENERIC  then
			return CLICKED_MAIN_ACTIONS.SPAWN
		elseif item.itemType == ITEM_TYPES.CHARACTER then
			return spawnOrPlace(item.behavior)
		elseif item.itemType == ITEM_TYPES.PLACEABLE then
			return CLICKED_MAIN_ACTIONS.PLACE
		elseif item.itemType == ITEM_TYPES.FLAG then
			return CLICKED_MAIN_ACTIONS.PLACE
		elseif item.itemType == ITEM_TYPES.SHORTCUT then
			return CLICKED_MAIN_ACTIONS.PLACE
		elseif item.itemType == ITEM_TYPES.SPAWNER then
			return CLICKED_MAIN_ACTIONS.PLACE
		elseif item.itemType == ITEM_TYPES.HARVESTABLE then
			return CLICKED_MAIN_ACTIONS.SPAWN
		elseif item.itemType == ITEM_TYPES.RECIPE or item.itemType == ITEM_TYPES.BLUEPRINT then
			return CLICKED_MAIN_ACTIONS.EDIT
		elseif item.itemType == ITEM_TYPES.QUEST then
			return CLICKED_MAIN_ACTIONS.EDIT
		elseif item.itemType == ITEM_TYPES.SETTINGS or item.itemType == ITEM_TYPES.MAIN_SETTINGS or item.itemType == ITEM_TYPES.ENVIRONMENT_SETTINGS  or item.itemType == ITEM_TYPES.WELCOME_SETTINGS then
			return CLICKED_MAIN_ACTIONS.EDIT
		elseif item.itemType == ITEM_TYPES.PATH then
			return CLICKED_MAIN_ACTIONS.PLACE
		end
	end

	return nil
end

function spawnOrPlace(behavior)
	if behavior == "monster" or 
		behavior == "pet" or
		behavior == "animal" then
		return CLICKED_MAIN_ACTIONS.SPAWN
	end

	-- EDIE: Change this to "Place" when we get the kinks worked out 
	return CLICKED_MAIN_ACTIONS.PLACE
end

--So many special cases...
function getEditorPrefabType(itemUNID)
	if itemUNID and m_worldInventory[tostring(itemUNID)] then
		local item = m_worldInventory[tostring(itemUNID)]
		if item.itemType == ITEM_TYPES.WEAPON or item.itemType == ITEM_TYPES.ARMOR or item.itemType == ITEM_TYPES.TOOL or item.itemType == ITEM_TYPES.AMMO or item.itemType == ITEM_TYPES.CONSUMABLE or item.itemType == ITEM_TYPES.GENERIC then
			if item.itemType == ITEM_TYPES.TOOL or item.itemType == ITEM_TYPES.WEAPON then
				if item.paintWeapon and item.paintWeapon == "true" then
					return "paintbrush"
				elseif item.toolType and item.toolType == tostring("emoteItem") then
					return "emoteItem"
				elseif item.toolType and item.toolType == tostring("p2pEmoteItem") then
					return "p2pEmoteItem"
				else
					return item.itemType
				end
			else
				return item.itemType
			end
		elseif item.itemType == ITEM_TYPES.CHARACTER then
			
			-- logs for debugging the clothing vendor 
			if item.behaviorParams and item.behaviorParams.clothing then 
				log("--- Framework_AllGameItems ITEM_TYPES.CHARACTER ".. tostring(item.behavior).. " clothing: ".. tostring(item.behaviorParams.clothing))
			else
				log("--- Framework_AllGameItems ITEM_TYPES.CHARACTER ".. tostring(item.behavior))
			end 

			if item.behaviorParams and item.behaviorParams.timed and item.behaviorParams.timed == true then
				return "timedVendor"
			elseif item.behaviorParams and item.behaviorParams.random and item.behaviorParams.random == true then
				return "randomVendor"
			elseif item.behaviorParams and item.behaviorParams.credits and item.behaviorParams.credits == true then
				return "creditsVendor"
			elseif item.behaviorParams and item.behaviorParams.gem and item.behaviorParams.gem == true then
				return "gemVendor"
			elseif item.behaviorParams and item.behaviorParams.clothing and item.behaviorParams.clothing == true then
				return "clothing"
			else
				--log("--- Framework_AllGameItems ITEM_TYPES.CHARACTER ".. tostring(item.behavior))
				return item.behavior
			end
		elseif item.itemType == ITEM_TYPES.PLACEABLE then
			if item.behavior then
				if item.behavior == "teleporter" and item.behaviorParams.worldDestination then
					return "worldTeleporter"
				elseif item.behavior == "teleporter" and item.zoneDestination then
					return "zoneTeleporter"
				else
					return item.behavior
				end
			else
				return item.itemType
			end
		elseif item.itemType == ITEM_TYPES.SPAWNER then

			if item.behavior == "spawner_loot" and item.behaviorParams and item.behaviorParams.loot and item.behaviorParams.loot.limitItems then
				return "spawner_loot_limit"
			else
				return item.behavior or item.itemType
			end
		elseif item.itemType == ITEM_TYPES.HARVESTABLE then
			if item.behavior == "seed" then 
				return "seed"
			else
				return item.itemType
			end
		elseif item.itemType == ITEM_TYPES.RECIPE or item.itemType == ITEM_TYPES.BLUEPRINT then
			return item.itemType
		elseif item.itemType == ITEM_TYPES.QUEST then
			return item.itemType
		elseif item.itemType == ITEM_TYPES.FLAG then
			return item.itemType
		elseif item.itemType == ITEM_TYPES.SEED then
			return item.itemType
		elseif item.itemType == ITEM_TYPES.PATH then
			return item.itemType
		end
	end

	return nil
end

-------------------------------------------------
-------------------------------------------------
-- Unified Inventory Functions
-------------------------------------------------
-------------------------------------------------

function gameFilterHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local filterEvent = KEP_EventDecodeString(tEvent)
	local filter = Events.decode(filterEvent)

	if m_newItemTrayOpen then
		closeNewItemTray()
	end

	if filter ~= nil and type(filter) == "table" then
		m_filter = filter.filter
		m_newItemType = filter.newItemType
		updateNewItemDisplay(m_newItemType)
	else
		m_filter = filterEvent
	end	

	--If the filter is spawners
	--Should never come here in new Unified
	if (string.find(m_filter, "spawner")) and m_mode ~= INVENTORY_MODES.WORLD then
		m_mode = INVENTORY_MODES.WORLD
		Control_SetVisible(gHandles["imgTopBarBlue"], allowAction())
	end 

	InventoryHelper.gotoPage(1)
	InventoryHelper.clearClicked()
	if m_mode == INVENTORY_MODES.WOK then
		requestWebInventory()
	elseif m_mode == INVENTORY_MODES.WORLD then
		formatDisplayTable()
		updateInventoryMenu()
	end

	handleBorderColor()
end

function enableTooltipsHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_tooltipsEnabled = true

	if m_mode == INVENTORY_MODES.WOK then
		populateItems()
	elseif m_mode == INVENTORY_MODES.WORLD then
		formatDisplayTable()
	end
	
end

function disableTooltipsHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_tooltipsEnabled = false

	if m_mode == INVENTORY_MODES.WOK then
		populateItems()
	elseif m_mode == INVENTORY_MODES.WORLD then
		formatDisplayTable()
	end
end

-------------------------------------------------
-------------------------------------------------
-- WOK Game Items Functions
-------------------------------------------------
-------------------------------------------------

function requestWebInventory()
	if m_mode == INVENTORY_MODES.WOK then
		getInventoryFromWeb()
	end	
end

function getInventoryFromWeb()
	local category = "gaming"
	local webSubcategory, gameItemTypes = nil, nil	

	local useType = USE_TYPE.GAME_ITEM

	gameItemTypes = getWOKWebcallfilter()

	getPlayerInventory(WF.PLAYER_INVENTORY, InventoryHelper.page, InventoryHelper.itemsPerPage, category, InventoryHelper.searchText, webSubcategory, useType, gameItemTypes)
end

function getWOKWebcallfilter()
	if m_filter == "all" then
		return nil
	else
		return m_filter
	end	
end

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	-- Unique Game Item check return
	if (filter == WF.UNIQUE_GAME_ITEM_CHECK) and m_pendingWebCheck then
		m_pendingWebCheck = false
		local xmlData = KEP_EventDecodeString(tEvent)
		
		handleUniqueNameWebCall(xmlData)
	elseif filter == WF.PLAYER_INVENTORY and m_mode == INVENTORY_MODES.WOK then
		local xmlData = KEP_EventDecodeString( tEvent )

		-- Test to make sure valid results and results are up to date (for multiple fast calls)
		local result = string.match(xmlData, "<ReturnCode>(.-)</ReturnCode>")
		local passthrough = tonumber( string.match(xmlData,"<Passthrough>(.-)</Passthrough>") or -1 )
		if result ~= "0" or passthrough ~= cf_inventoryPassthrough then
			return
		end

		-- Update the pages
		local totalItems = string.match(xmlData, "<TotalNumberRecords>(%d-)</TotalNumberRecords>")
		InventoryHelper.setMaxPages(totalItems)

		togglePageButtons(tonumber(totalItems) > ITEMS_PER_PAGE)

		g_displayTable = {}

		decodeGameItemsFromWeb(g_displayTable, xmlData)

		populateItems()

		--return KEP.EPR_CONSUMED
	end
end

function populateItems()

	if m_mode == INVENTORY_MODES.WORLD then return end
	m_pendingImages = InventoryHelper.setIcons({"bundle_global_id", "default_glid", "GLID"}, "SmartObj_ScriptIcon_82x82.tga")

	-- Finish loading any images in ContentServiceCompletionHandler
	InventoryHelper.setNameplates("name")
	for i, item in pairs(g_displayTable) do
		g_displayTable[i].tooltip = formatWOKTooltips(item, i)
	end

	for i=1,ITEMS_PER_PAGE do

		if g_displayTable[i] then
			local item = g_displayTable[i]

			local element
			if item.itemType == ITEM_TYPES.WEAPON or item.itemType == ITEM_TYPES.ARMOR or item.itemType == ITEM_TYPES.TOOL then
				Control_SetVisible(gHandles["imgItemLevel" .. i], true)
				element = Image_GetDisplayElement(gHandles["imgItemLevel" .. i])
				local itemLevel = item.metadata.level
				local rarityOffset = ITEM_LEVEL_RARITY_OFFSET[item.metadata.rarity] or 0
				Element_SetCoords(element, ITEM_LEVEL_COORDS[itemLevel].left + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].top, ITEM_LEVEL_COORDS[itemLevel].right + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].bottom)
			else
				Control_SetVisible(gHandles["imgItemLevel" .. i], false)
			end

			local rarity = item.metadata.rarity or "Common"
			element = Image_GetDisplayElement(gHandles["imgItemBG" .. i])
			Element_SetCoords(element, ITEM_RARITY_BG[rarity].left, ITEM_RARITY_BG[rarity].top, ITEM_RARITY_BG[rarity].right, ITEM_RARITY_BG[rarity].bottom)

			local inWorld = getItemUNIDInWorld(i) ~= nil
			local textColor = inWorld and IN_WORLD_COLOR or NOT_IN_WORLD_COLOR

			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["stcItem" .. i])), 0, textColor)

			Control_SetVisible( gHandles["stcRefCount" .. i], false ) --TODO - remove from XML
			Control_SetVisible( gHandles["imgRefCountBG" .. i], false )
		else
			Control_SetVisible( gHandles["stcRefCount" .. i], false ) --TODO - remove from XML
			Control_SetVisible( gHandles["imgRefCountBG" .. i], false )
			Control_SetVisible(gHandles["imgItemLevel" .. i], false)
		end		
	end		
end

function formatWOKTooltips(item, index)
	if item == nil or not item then return end

	local tooltip = {}

	if not m_tooltipsEnabled then return tooltip end

	tooltip["header"] = {label = item.metadata.name}

	local inWorld = getItemUNIDInWorld(index)

	if inWorld then
		tooltip["moreInfo"] = {label = "This item is in the world"}
		tooltip["moreInfo"].color = GREY_SEARCH_COLOR
	end	
	
	if item.metadata and item.metadata.rarity then
		tooltip["header"].color = COLORS_VALUES[item.metadata.rarity]
	end
	
	if item.metadata.description then
		tooltip["body"] = {label = "\"" .. item.metadata.description .. "\""}
	end

	if item.itemType == "weapon" then
		tooltip["footer1"] = {label = "Weapon Level: "..tostring(item.metadata.level)}
		tooltip["footer2"] = {label = "Range: "..tostring(item.metadata.range)}
	elseif item.itemType == "armor" then
		tooltip["footer1"] = {label = "Armor Level: "..tostring(item.metadata.level)}
		tooltip["footer2"] = {label = "Slot: "..tostring(item.metadata.slotType:gsub("^%l", string.upper))}
	elseif item.itemType == "consumable" then
		if item.metadata.consumeType == "energy" then
			tooltip["footer1"] = {label = "Energy Bonus: "..tostring(item.metadata.consumeValue).."%"}
		else
			tooltip["footer1"] = {label = "Health Bonus: "..tostring(item.metadata.consumeValue).."%"}
		end
	elseif item.itemType == "placeable" then
		tooltip["footer1"] = {label = "Level: "..tostring(item.metadata.level)}
	elseif item.itemType == "generic" then
		-- todo
	elseif item.itemType == "ammo" then
		-- todo
	end

	return tooltip
end

-------------------------------------------------
-------------------------------------------------
-- World Game Items Functions
-------------------------------------------------
-------------------------------------------------

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	if InventoryFull() then return end
	local gameItems = decompileInventory(tEvent)
	m_worldInventory = {}

	for UNID,properties in pairs(gameItems) do
		m_worldInventory[UNID] = properties
		-- update number of flags
		if properties.itemType == ITEM_TYPES.FLAG then
			local flagIndex = getFlagType(properties)
			if flagIndex ~= "invalid flag" then
				m_flagCount[flagIndex] = m_flagCount[flagIndex] + 1
			end
		end
	end

	if m_mode == INVENTORY_MODES.WORLD then
		formatDisplayTable()
		updateInventoryMenu()
	else
		populateItems()
	end
	m_batchFinished = true
end

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)



	if tablePopulated(updateItem) then
		m_worldInventory[updateIndex] = updateItem
	else
		m_worldInventory[updateIndex] = nil
	end

	if m_mode == INVENTORY_MODES.WORLD then
		formatDisplayTable()
		updateInventoryMenu()
	else
		populateItems()
	end
end

function formatDisplayTable()
	m_fullDisplayTable = {}
	for itemUNID,item in pairs(m_worldInventory) do
		if filterGameItem(item) then
			local strStart, strEnd = string.find(string.lower(item.name), string.lower(m_searchText))
			if strStart ~= nil then
				table.insert(m_fullDisplayTable, {UNID = itemUNID, properties = item})				
			end
		end
	end

	InventoryHelper.setMaxPages(#m_fullDisplayTable)

	togglePageButtons(#m_fullDisplayTable > ITEMS_PER_PAGE)

	table.sort(m_fullDisplayTable, customSort)

	g_displayTable = {}

	local startIndex = ((m_page - 1) * ITEMS_PER_PAGE)

	for i=1, ITEMS_PER_PAGE do
		g_displayTable[i] = m_fullDisplayTable[i + startIndex]

		if g_displayTable[i] then
			g_displayTable[i].tooltip = formatTooltips(g_displayTable[i])

			if g_displayTable[i].properties.itemType == ITEM_TYPES.RECIPE then
				if g_displayTable[i].properties.output and m_worldInventory[tostring(g_displayTable[i].properties.output)] then
					g_displayTable[i].properties.GLID = m_worldInventory[tostring(g_displayTable[i].properties.output)].GLID
				end
			elseif g_displayTable[i].properties.itemType == ITEM_TYPES.PATH then
				if g_displayTable[i].properties.behaviorParams and g_displayTable[i].properties.behaviorParams.pathRider_parentUNID  and g_displayTable[i].properties.behaviorParams.pathRider_parentUNID > 0 then
					g_displayTable[i].properties.GLID = m_worldInventory[tostring(g_displayTable[i].properties.behaviorParams.pathRider_parentUNID)].GLID
				end
			end
		end
	end

	--gotoItem()
end

function filterGameItem(item)
	--Filter out game items that should never show
	if item.itemType == ITEM_TYPES.GEMBOX  or item.itemType == "waypoint" or item.itemType == "tutorial" then
		return false
	end

	if m_filter == "all" then
		return true
	else
	    for filter in string.gmatch(m_filter, "([^".."|".."]+)") do
	    	local typeFilter, behaviorFilter
	    	if string.match(filter, ",") then
				typeFilter, behaviorFilter = string.match(filter, "([^,]+),([^,]+)")
			else
				typeFilter = filter
			end

	        if item.itemType == typeFilter then
	        	if behaviorFilter then
	        		if item.behavior == behaviorFilter then
	        			return true
	        		end
	        	else
	        		return true
	       		end	       		
	        end
	    end
	end

	return false
end

function customSort(a, b)
	return a.properties.name < b.properties.name
end

function updateInventoryMenu()
	InventoryHelper.setNameplates("name")
	for i=1,ITEMS_PER_PAGE do
		if g_displayTable[i] then
			local item = g_displayTable[i]

			Control_SetVisible( gHandles["stcRefCount" .. i], false) --TODO - remove from XML
			Control_SetVisible( gHandles["imgRefCountBG" .. i], false)

			Control_SetEnabled(gHandles["btnItem" .. i], true)
			
			Control_SetVisible( gHandles["imgItem" .. i], true )
			Control_SetVisible( gHandles["imgItemBG" .. i], true )

			applyImage(gHandles["imgItem"..i], item.properties.GLID)

			local element
			if item.properties.itemType == ITEM_TYPES.WEAPON or item.properties.itemType == ITEM_TYPES.ARMOR then
				Control_SetVisible(gHandles["imgItemLevel" .. i], true)
				
				element = Image_GetDisplayElement(gHandles["imgItemLevel" .. i])
				local itemLevel = item.properties.level
				local rarityOffset = ITEM_LEVEL_RARITY_OFFSET[item.properties.rarity] or 0
				Element_SetCoords(element, ITEM_LEVEL_COORDS[itemLevel].left + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].top, ITEM_LEVEL_COORDS[itemLevel].right + rarityOffset, ITEM_LEVEL_COORDS[itemLevel].bottom)
			else
				Control_SetVisible(gHandles["imgItemLevel" .. i], false)
			end

			local rarity = item.properties.rarity or "Common"
			element = Image_GetDisplayElement(gHandles["imgItemBG" .. i])
			Element_SetCoords(element, ITEM_RARITY_BG[rarity].left, ITEM_RARITY_BG[rarity].top, ITEM_RARITY_BG[rarity].right, ITEM_RARITY_BG[rarity].bottom)

			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["stcItem" .. i])), 0, IN_WORLD_COLOR)

		else
			Control_SetVisible( gHandles["stcRefCount" .. i], false ) --TODO - remove from XML
			Control_SetVisible( gHandles["imgRefCountBG" .. i], false )
			Control_SetVisible( gHandles["imgItem" .. i], false )
			Control_SetVisible( gHandles["imgItemBG" .. i], false )
			Control_SetVisible(gHandles["imgItemLevel" .. i], false)
			Control_SetEnabled(gHandles["btnItem" .. i], false)
		end
	end
end

function formatTooltips(item)
	if item then
		tooltip = {}

		if not m_tooltipsEnabled then return tooltip end

		tooltip["header"] = {label = tostring(item.properties.name)}

		if not item.properties.GIGLID then
			tooltip["moreInfo"] = {label = "This item is unique to this world"}
			tooltip["moreInfo"].color = GREY_SEARCH_COLOR
		end
		
		if item.properties.rarity then
			tooltip["header"].color = COLORS_VALUES[item.properties.rarity]
		end

		if item.properties.description then
			tooltip["body"] = {label = "\"" .. item.properties.description .. "\""}
		end

		if item.properties.itemType == ITEM_TYPES.WEAPON then
			tooltip["footer1"] = {label = "Weapon Level: "..tostring(item.properties.level)}
			tooltip["footer2"] = {label = "Range: "..tostring(item.properties.range)}
		elseif item.properties.itemType == ITEM_TYPES.ARMOR then
			tooltip["footer1"] = {label = "Armor Level: "..tostring(item.properties.level)}
			tooltip["footer2"] = {label = "Slot: "..tostring(item.properties.slotType:gsub("^%l", string.upper))}
		elseif item.properties.itemType == ITEM_TYPES.CONSUMABLE then
			if item.properties.consumeType == "energy" then
				tooltip["footer1"] = {label = "Energy Bonus: "..tostring(item.properties.consumeValue).."%"}
			else
				tooltip["footer1"] = {label = "Health Bonus: "..tostring(item.properties.consumeValue).."%"}
			end
		elseif item.properties.itemType == ITEM_TYPES.PLACEABLE then

		elseif item.properties.itemType == ITEM_TYPES.GENERIC then

		elseif item.properties.itemType == ITEM_TYPES.AMMO then

		end

		return tooltip
	end
end

-------------------------------------------------
-------------------------------------------------
-- Button Actions
-------------------------------------------------
-------------------------------------------------

-------------------------------------------------
-- Publishing Game Items
-------------------------------------------------

-- Helps push a Game Item over to the Framework_PublishGameItem menu
function publishWorldItem()
	if m_mode == INVENTORY_MODES.WORLD and InventoryHelper.selectedIndex then
		local UNID = getUNIDFromIndex(InventoryHelper.selectedIndex)
		local item = m_worldInventory[UNID]
		
		-- Don't proceed if this item is not inventoryCompatible
		if item.inventoryCompatible then
			-- Check if the user has this Game Item by name and type in their Inventory already
			local address = GameGlobals.WEB_SITE_PREFIX..PLAYER_INVENTORY_CHECK_SUFFIX.."211&countGINameUse=true&search="..tostring(item.name).."&gameItemTypes="..tostring(item.itemType)
			m_publishQueue.item = item
			m_pendingWebCheck = true
			makeWebCall(address, WF.UNIQUE_GAME_ITEM_CHECK, 1, 1)
			
			-- Get required GLIDs for this Game Item from the server
			Events.sendEvent("FRAMEWORK_EXTRACT_GAME_ITEM_GLIDS", {UNID = UNID, returnType = "web"})
			
			m_publishQueue.GLIDsextracted = false
			m_publishQueue.nameChecked = false
		else
			LogWarn("publishWorldItem() Item(UNID["..tostring(item.UNID).."] Name["..tostring(item.name).."]) is not inventoryCompatible")
		end
	end
end

-- Handles extracted GLIDs for a Game Item intended for the Inventory
function handleExtractedGameItemGLIDs(event)
	local GLIDs = event.GLIDs
	m_publishQueue.GLIDsextracted = true
	m_publishQueue.extractedGLIDs = event.GLIDs
	
	-- Are we ready to go?
	if m_publishQueue.nameChecked then
		continueWithPublish()
	end
end

-- Handle Game Item unique name web call
function handleUniqueNameWebCall(xmlData)
	local s, e, suggestedName = string.find(xmlData, "<SuggestedName>(.-)</SuggestedName>")
	local item = m_publishQueue.item
	local originalName = item.name

	if suggestedName then
		-- If the user already has a Game Item owned by this name, use the suggested name
		if suggestedName ~= item.name then
			m_publishQueue.suggestedName = suggestedName
		end
		
		m_publishQueue.nameChecked = true
		
		-- Are we ready to go?
		if m_publishQueue.GLIDsextracted then
			continueWithPublish()
		end
	else
		LogError("handleUniqueNameWebCall() No SuggestedName returned from web call ReturnXML["..tostring(xmlData).."]")
	end
end

-- Continue with the publish
function continueWithPublish()
	local item = deepCopy(m_publishQueue.item)
	local originalName = item.name
	-- Set uploaded item's name to the suggested name if we extracted one
	item.name = m_publishQueue.suggestedName or item.name
	item.bundledGlids = m_publishQueue.extractedGLIDs
	
	-- Open the PublishGameItem menu and pass it this item
	MenuOpen("Framework_PublishGameItem.xml")
	local event = KEP_EventCreate("FRAMEWORK_PUBLISH_GAME_ITEM")
	KEP_EventEncodeString(event, JSON.encode(item))
	KEP_EventEncodeString(event, originalName)
	KEP_EventQueue(event)
	
	-- Reset publishQueue
	m_publishQueue = {["GLIDsextracted"] = false,
					  ["extractedGLIDs"] = {},
					  ["nameChecked"] = false,
					  ["UNID"] = 0,
					  ["item"] = {}
				     }
end

-------------------------------------------------
-- Edit/Create/Duplicate/Delete Game Items
-------------------------------------------------

function editWorldItem(itemUNID)
	if itemUNID and m_worldInventory[tostring(itemUNID)] then
		MenuOpen("Framework_GameItemEditor.xml")
		local item = m_worldInventory[tostring(itemUNID)]

		local flatItem = {}

		flatenItemData(item, item, flatItem)

		local prefabType = getEditorPrefabType(itemUNID)

		--Debug.printTable("", flatItem)
		local itemInfo = Events.encode(flatItem)
		
		local event = KEP_EventCreate("EDIT_GAME_ITEM_INFO")
		KEP_EventEncodeString(event, itemUNID) -- UNID
		KEP_EventEncodeString(event, itemInfo) -- item info
		KEP_EventEncodeString(event, m_editorName[item.itemType]) -- Editor title
		KEP_EventEncodeString(event, prefabType) -- prefabType
		KEP_EventEncodeString(event, "editItem")	-- menu mode
		
		KEP_EventQueue(event)
	end
end

function flatenItemData(baseData, itemData, flatItem)
	for attribute,value in pairs(itemData) do
		if type(value) == "table" then 
			--if attribute == "singles" or attribute == "trades" or attribute == "quests" or attribute == "quest"  or attribute == "particleEffects" or attribute == "particleEffectsMine" or attribute == "particleEffectsTheirs" or attribute == "middleStages" or attribute == "inputs" or attribute == "proxReq" then
			if (isNestedKey(attribute) or attribute == "singles") and attribute ~= "behaviorParams" and attribute ~= "loot" and attribute ~= "rewardItem" and attribute ~= "requiredItem" and attribute ~= "waypointInfo"  and attribute ~= "trade" then
				flatItem[attribute] = value
			else
				flatenItemData(baseData, value, flatItem)
			end
		else
			if flatItem[attribute] == nil then
				flatItem[attribute] = value
			end
		end
	end
end

function duplicateWorldItem(item)
	if m_mode == INVENTORY_MODES.WORLD and InventoryHelper.selectedIndex then
		local UNID = getUNIDFromIndex(InventoryHelper.selectedIndex)
		local item = m_worldInventory[UNID]

		-- update flag count if applicable
		if item.itemType == ITEM_TYPES.FLAG then
			local flagIndex = getFlagType(item)
			if flagIndex ~= "invalid flag" then
				m_flagCount[flagIndex] = m_flagCount[flagIndex] + 1
			end
		end

		Events.sendEvent("CREATE_NEW_GAME_ITEM", {properties = item})
		Events.sendEvent("DOWNLOAD_GAME_ITEM_ICON", {UNID = UNID, broadcast = true})
		InventoryHelper.clearClicked()
	end
end

function deleteWorldItem(item)
	if InventoryHelper.selectedIndex then
		local itemUNID = getUNIDFromIndex(InventoryHelper.selectedIndex)
		local item = m_worldInventory[itemUNID]

		-- update flag count if applicable
		if item.itemType == ITEM_TYPES.FLAG then
			local flagIndex = getFlagType(item)
			if flagIndex ~= "invalid flag" then
				m_flagCount[flagIndex] = m_flagCount[flagIndex] - 1
			end
		end

		m_worldInventory[itemUNID] = nil
		InventoryHelper.clearClicked()
		formatDisplayTable()

		updateInventoryMenu()
		Events.sendEvent("DELETE_GAME_ITEM", {UNID = itemUNID})
	end
end

function createNewItem(itemType)
	MenuOpen("Framework_GameItemEditor.xml")
	local itemInfo = Events.encode({name = "New Item", itemType = itemType})

	local event = KEP_EventCreate("EDIT_GAME_ITEM_INFO")
	KEP_EventEncodeString(event, 0)
	KEP_EventEncodeString(event, itemInfo) -- default item info
	KEP_EventEncodeString(event, "New Item") -- Editor title
	KEP_EventEncodeString(event, itemType) -- prefabType
	KEP_EventEncodeString(event, "newItem")	-- menu mode
	KEP_EventQueue(event)
end

function updateNewItemDisplay(newItemType)
	if newItemType == "none"  or m_mode == INVENTORY_MODES.WOK then
		Control_SetVisible(gHandles["imgPlusIcon"],false)
		--Control_SetVisible(gHandles["stcNewItemTitle"],false)
		Control_SetEnabled(gHandles["btnNewItem"],false)
		Control_SetVisible(gHandles["stcNew"], false)
	else
		Control_SetVisible(gHandles["imgPlusIcon"],true)
		--Control_SetVisible(gHandles["stcNewItemTitle"],true)
		Control_SetVisible(gHandles["btnNewItem"],true)
		Control_SetEnabled(gHandles["btnNewItem"],true)
		--Static_SetText(gHandles["stcNewItemTitle"], NEW_ITEM_CONFIGS[newItemType].buttonText)
		local itemTypeText = newItemType:gsub("^%l", string.upper)
		Control_SetToolTip(gHandles["btnNewItem"], "Create "..tostring(itemTypeText).." Item")
		Control_SetVisible(gHandles["stcNew"], true)
	end	
end

function openNewItemTray()
	if NEW_ITEM_CONFIGS[m_newItemType] and NEW_ITEM_CONFIGS[m_newItemType].types then
		local numOfTypes = #NEW_ITEM_CONFIGS[m_newItemType].types

		local width = Control_GetWidth( gHandles["imgNewItemBG"] )
		local height = numOfTypes * 40
		Control_SetSize( gHandles["imgNewItemBG"], width, height )
		Control_SetLocation(gHandles["imgNewItemBG"], Control_GetLocationX(gHandles["imgNewItemBG"]), Control_GetLocationY(gHandles["imgNewItemBG"]) - height)

		Control_SetSize( gHandles["imgNewItemOutline"], width + 2, height + 2 )
		Control_SetLocation(gHandles["imgNewItemOutline"], Control_GetLocationX(gHandles["imgNewItemBG"]) - 1, Control_GetLocationY(gHandles["imgNewItemBG"]) - 1)

		Control_SetVisible(gHandles["imgNewItemBG"],true)
		Control_SetEnabled(gHandles["imgNewItemBG"],true)

		Control_SetVisible(gHandles["imgNewItemOutline"],true)
		Control_SetEnabled(gHandles["imgNewItemOutline"],true)

		Control_SetVisible(gHandles["btnNewItemRollOff"],true)
		Control_SetEnabled(gHandles["btnNewItemRollOff"],true)

		for i=1,numOfTypes do
			Control_SetVisible(gHandles["btnNewItemSub" .. i],true)
			Control_SetEnabled(gHandles["btnNewItemSub" .. i],true)
			Static_SetText(gHandles["btnNewItemSub" .. i], "  " .. NEW_ITEM_CONFIGS[m_newItemType].types[i].name)
		end

		m_newItemTrayOpen = true
		InventoryHelper.clearClicked()
	end
end

function closeNewItemTray()
	if NEW_ITEM_CONFIGS[m_newItemType] and NEW_ITEM_CONFIGS[m_newItemType].types then
		local numOfTypes = #NEW_ITEM_CONFIGS[m_newItemType].types

		local width = Control_GetWidth( gHandles["imgNewItemBG"] )
		local height = Control_GetHeight( gHandles["imgNewItemBG"] )
		Control_SetSize( gHandles["imgNewItemBG"], width, 0 )
		Control_SetLocation(gHandles["imgNewItemBG"], Control_GetLocationX(gHandles["imgNewItemBG"]), Control_GetLocationY(gHandles["imgNewItemBG"]) + height)

		Control_SetVisible(gHandles["imgNewItemBG"],false)
		Control_SetEnabled(gHandles["imgNewItemBG"],false)

		Control_SetVisible(gHandles["imgNewItemOutline"],false)
		Control_SetEnabled(gHandles["imgNewItemOutline"],false)

		Control_SetVisible(gHandles["btnNewItemRollOff"],false)
		Control_SetEnabled(gHandles["btnNewItemRollOff"],false)

		for i=1,numOfTypes do
			Control_SetVisible(gHandles["btnNewItemSub" .. i],false)
			Control_SetEnabled(gHandles["btnNewItemSub" .. i],false)
		end

		m_newItemTrayOpen = false
		m_newItemTypeMouse = false
	end
end

-------------------------------------------------
-- Place Spawner/Game Items
-------------------------------------------------

function addGameItemFromWOK(item)
	if item then
		local event = {item = item.metadata}
		-- Send event down to Framework

		if item.itemType == "placeable" or (item.itemType == "character" and spawnOrPlace(item.behavior) == CLICKED_MAIN_ACTIONS.PLACE) then
			event.mode = "Place"
			Events.sendEvent("WOK_PLACE_GAME_ITEM", event)
		else
			event.mode = "CreateSpawner"
			Events.sendEvent("WOK_CREATE_SPAWNER", event)
		end
		enterBuildMode()
	end
end

function placeWorldItem(itemUNID)
	if itemUNID then
		local gameItem = m_worldInventory[itemUNID]
		
		-- Ask server to place this game item at your feet
		-- Your World Position
		local x, y, z, r = KEP_GetPlayerPosition()
		local placeDistance = getDropDistance()
		local radian = (r * math.pi) / 180
		local rotX = math.sin(radian)
		local rotZ = math.cos(radian)
		local placeX = x - (placeDistance * rotX)
		local placeZ = z - (placeDistance * rotZ)		
		if gameItem.behavior == "path" then
			gameItem.GLID = GameDefinitions.PATH_GLID
		end
		Events.sendEvent("FRAMEWORK_PLAYER_PLACE_ITEM", {GLID = gameItem.GLID,
														 UNID = tonumber(itemUNID),
														 behavior = gameItem.behavior,
														 x = placeX, y = y, z = placeZ, rx = rotX, ry = 0, rz = rotZ})
		m_placingGameItemUNID = itemUNID

		enterBuildMode()
	end	
end

function placeWorldItemSpawner(itemUNID)
	if itemUNID then
		local x, y, z, r = KEP_GetPlayerPosition()
		local placeDistance = getDropDistance()
		local radian = (r * math.pi) / 180
		local rotX = math.sin(radian)
		local rotZ = math.cos(radian)
		local placeX = x - (placeDistance * rotX)
		local placeZ = z - (placeDistance * rotZ)
		Events.sendEvent("FRAMEWORK_CREATE_PLACE_SPAWN", {	spawnUNID = itemUNID,
															x = placeX, y = y, z = placeZ, rx = rotX, ry = 0, rz = rotZ})
		m_placingGameItemUNID = itemUNID
		enterBuildMode()
	end	
end

-- Called on Placement attempt
function placeGameItemConfirmation(event)
	if m_placingGameItemUNID then
		-- Failed
		if not event.success then
			-- Open the GameItemLoadWarning menu and pass it a message
			if MenuIsClosed("Framework_GameItemLoadWarning.xml") then
				MenuOpen("Framework_GameItemLoadWarning.xml")
			end
			local ev = KEP_EventCreate("FRAMEWORK_GAME_ITEM_LOAD_MESSAGE")
			KEP_EventEncodeString(ev, GI_LOAD_MESSAGE1)
			KEP_EventEncodeString(ev, GI_LOAD_MESSAGE2)
			KEP_EventQueue(ev)
		-- Placement succeeded
		end
		m_placingGameItemUNID = nil
	end
end


function enterBuildMode()
	local ev = KEP_EventCreate("EnterBuildModeEvent")
	KEP_EventSetFilter(ev, 1)
	KEP_EventQueue(ev)
	g_playerModeActive = false
	if MenuIsOpen("Framework_Toolbar.xml") then 
		KEP_ClearSelection() 
		local event = KEP_EventCreate("FRAMEWORK_SET_PLAYER_MODE")
		KEP_EventEncodeString(event, "God")
		KEP_EventQueue(event) -- NOTE: Handled in Framework_PlayerHandler
	end
end
-------------------------------------------------
-------------------------------------------------
-- Utility Functions
-------------------------------------------------
-------------------------------------------------

function togglePageButtons(toggle)
	Control_SetVisible(gHandles["stcPage"], toggle)
	Control_SetVisible(gHandles["btnBack"], toggle)
	Control_SetVisible(gHandles["btnNext"], toggle)
end

function getUNIDFromIndex(index)
	if g_displayTable[index] and g_displayTable[index].UNID then		
		return tostring(g_displayTable[index].UNID)
	end
end

function getItemUNIDInWorld(index)
	if g_displayTable[index] and g_displayTable[index].GIGLID then		
		for UNID,gameItem in pairs(m_worldInventory) do
			if gameItem.GIGLID and tonumber(gameItem.GIGLID) == tonumber(g_displayTable[index].GIGLID) then
				return UNID
			end
		end
	end

	return nil
end

function onGotoItem(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_itemGoto = tonumber(KEP_EventDecodeString(tEvent))

	if m_mode ~= INVENTORY_MODES.WORLD then
		m_mode = INVENTORY_MODES.WORLD
		InventoryHelper.gotoPage(1)
		InventoryHelper.clearClicked()
		Control_SetVisible(gHandles["imgTopBarBlue"], allowAction())

		CheckBox_SetChecked(m_worldCheckBox, true)
	end

	gotoItem()

	if MenuIsOpen("Framework_GameItemEditor.xml") then
	 	MenuBringToFront("Framework_GameItemEditor.xml")
	end 

end

function gotoItem()
	--printTable(m_fullDisplayTable)
	if m_mode == INVENTORY_MODES.WORLD and m_itemGoto and #m_fullDisplayTable > 0  and m_batchFinished then
		local itemIndex = 0
		for i=1,#m_fullDisplayTable do
			if tonumber(m_fullDisplayTable[i].UNID) == tonumber(m_itemGoto) then
				itemIndex = i
				break
			end
		end

		if itemIndex > 0 then
			local jumpToPage = math.ceil(itemIndex/ITEMS_PER_PAGE)
			local jumpToIndex = itemIndex%ITEMS_PER_PAGE == 0 and ITEMS_PER_PAGE or itemIndex%ITEMS_PER_PAGE
			m_itemGoto = nil
			InventoryHelper.gotoPage(jumpToPage)
			InventoryHelper.setClicked(jumpToIndex)
		end
	end
end

function  setGameItemMode( dispatcher, fromNetid, tEvent, eventid, filter, objectid )
	local mode = tonumber(KEP_EventDecodeString(tEvent))

	if mode == 1 then
		if m_mode ~= INVENTORY_MODES.WORLD then
			m_mode = INVENTORY_MODES.WORLD
			InventoryHelper.gotoPage(1)
			InventoryHelper.clearClicked()
			Control_SetVisible(gHandles["imgTopBarBlue"], allowAction())

			CheckBox_SetChecked(m_worldCheckBox, true)
		end
	else
		m_mode = INVENTORY_MODES.WOK
		requestWebInventory()
		InventoryHelper.gotoPage(1)
		InventoryHelper.clearClicked()		
		Control_SetVisible(gHandles["imgTopBarBlue"], false)
		CheckBox_SetChecked(m_worldCheckBox, false)
	end
end

function getFlagType(properties)
	if properties == nil or properties.itemType == nil or properties.flagType == nil or properties.enabled == nil then return "invalid flag" end
	local flagType = "invalid flag"
	if properties.itemType == "flag" then
		flagType = tostring(properties.flagType)
		if properties.enabled == "false" then
			flagType = flagType.." disabled"
		else
			flagType = flagType.." enabled"
		end
	end	
	return flagType
end

--We need to check if the inventory is empty
function InventoryFull( )
	local count = 0
	for i, v in pairs(m_worldInventory) do
		count = count + 1
		if count > 3 then
			return true
		end
	end
	return false
end