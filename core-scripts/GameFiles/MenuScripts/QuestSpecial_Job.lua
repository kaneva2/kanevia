--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("MenuTemplateHelper.lua")
dofile("Quest_Simple.lua")

QID_ODDJOB = 1001
STATE_PREWORK   = 0
STATE_INVITE 	= 1
STATE_TOP_RANK	= 2
STATE_CLOSED	= 3
STATE_GOOD_JOB 	= 4

gCurrentState = STATE_PREWORK
gSaveInviteX = -999

GUIDE_COMPLETE_TASK_EVENT = "GuideCompleteTaskEvent"

function SetTextEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local msgTitle = KEP_EventDecodeString( event )
	local msgSubtitle = KEP_EventDecodeString( event )
	local msgBody = KEP_EventDecodeString( event )
	local sh = Dialog_GetStatic( gDialogHandle, "lblTitle" )
	Static_SetText( sh, msgTitle )
	sh = Dialog_GetStatic( gDialogHandle, "lblSubtitle" )
	Static_SetText( sh, msgSubtitle )
	sh = Dialog_GetStatic( gDialogHandle, "lblBody" )
	Static_SetText( sh, msgBody )
	return KEP.EPR_OK
end

function SetJobRankTableEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local numRows = KEP_EventDecodeNumber( event )
	local message = ""
	for i = 1, numRows do
		message = message .. KEP_EventDecodeString( event )
		if i ~= numRows then
			message = message .. "\n"
		end
	end
	local sh = Dialog_GetStatic( gDialogHandle, "lblTable" )
	Static_SetText( sh, message )
	return KEP.EPR_OK
end

function SetJobMenuStateEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	gCurrentState = KEP_EventDecodeNumber( event )
	local bh = Dialog_GetControl( gDialogHandle, "btnSkip" )
	Control_SetEnabled( bh, gCurrentState == STATE_INVITE )
	Control_SetVisible( bh, gCurrentState == STATE_INVITE )
	bh = Dialog_GetControl( gDialogHandle, "btnHire" )
	Control_SetEnabled( bh, gCurrentState ~= STATE_PREWORK )
	Control_SetVisible( bh, gCurrentState ~= STATE_PREWORK )
	if gCurrentState ~= STATE_INVITE then
		if gSaveInviteX < 0 then
			gSaveInviteX = Control_GetLocationX( bh )
		end
		local dWid = Dialog_GetWidth( gDialogHandle )
		local bWid = Control_GetWidth( bh )
		local xLoc = ( dWid / 2 ) - ( bWid / 2 )
		Control_SetLocation( bh, xLoc, Control_GetLocationY( bh ) )
	elseif gCurrentState ~= STATE_PREWORK then
		if gSaveInviteX > 0 then
			Control_SetLocation( bh, gSaveInviteX, Control_GetLocationY( bh ) )
		end
	end

	return KEP.EPR_OK
end

function buttonHandler()
	if gCurrentState == STATE_INVITE then
		questOptionSelection( QID_ODDJOB, STATE_INVITE )
	else
		MenuCloseThis()
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandlerFiltered( "SetTextEventHandler", "SetTextEvent", QID_ODDJOB, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFiltered( "SetJobRankTableEventHandler", "SetJobRankTableEvent", QID_ODDJOB, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFiltered( "SetJobMenuStateEventHandler", "SetJobMenuStateEvent", QID_ODDJOB, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	setupDialogBase(gDialogHandle, 662, 502, true, true, false )
	questOptionSelection( QID_ODDJOB, gCurrentState )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnHire_OnButtonClicked( buttonHandle )
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .. "mykaneva/invitefriend.aspx" )
	buttonHandler()
end

function btnSkip_OnButtonClicked( buttonHandle )
	buttonHandler()
end

function btnEarnMore_OnButtonClicked( buttonHandle )
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA ..BUY_CREDIT_PACKAGES_SUFFIX )
end
