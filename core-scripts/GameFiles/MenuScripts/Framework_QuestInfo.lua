--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_QuestInfo.lua
--
-- Displays active quest info
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("Framework_ActorHelper.lua")

-- Constants
local ITEM_BUFFER = 20
local QUEST_DESCRIPTION_Y_BUFFER = 6
local CLOSE_X = 270
local COMPLETION_ANIMATION_M = 128
local COMPLETION_ANIMATION_F = 52

local FONT_WHITE 	= "<font color=FFDCDCDC>"
local FONT_YELLOW 	= "<font color=FFFFC000>"
local FONT_GREEN 	= "<font color=FF15A927>"
local FONT_RED 		= "<font color=FFEF4848>"
local FONT_END 		= "</font>"

local WAYPOINT_GLID = 4563344
local IMAGE_SIZE = 48 --Image size: Both width and height
-- Tracked Quest UNID
local m_trackedQuest

-- Local
m_questInfo = {}
m_rewardItem = {}
m_questAcceptable = false
m_autoAccepted = false
m_updatedQuestsPending = false -- Should the QuestGiver menu delay showing this quest again (in case we need to run some stuff by the server first)
m_fromQuestGiver = false --If this menu is opened from the quest giver
m_actorPID = nil

local m_image = nil

local m_playerInventoryByUNID = {} -- game item cache 
local m_playerInventory = {} -- game item cache 
local m_gameItems = {}

--Make sure all are done before populating menu
local m_gameItemCacheInit = false
local m_inventoryInit = false
local m_populateMenuPending = false

--Am I modal?
local m_isModal = false

-- Called when the menu is created
function onCreate()
	CloseAllActorMenus()
	-- Register client-client event handlers
	KEP_EventRegisterHandler("onQuestInfoHandler", "FRAMEWORK_QUEST_INFO", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("returnTrackedQuest", "FRAMEWORK_RETURN_TRACKED_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCheckFullResponse", "INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)

	-- game item cache 
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	requestInventory(GAME_PID)
	requestInventory(INVENTORY_PID)
	
	-- Register server event handlers
	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)

	local ev = KEP_EventCreate("FRAMEWORK_GET_TRACKED_QUEST") -- Get Tracked quest from compass
	KEP_EventQueue(ev)
end

function onCloseMenu()
	if m_gameItemCacheInit and m_inventoryInit and m_populateMenuPending then
		onDestroy()
	end
end

function onDestroy()
	log("--- Framework_QuestInfo onDestroy")
	-- Inform QuestGiver that you've closed
	local ev = KEP_EventCreate("FRAMEWORK_QUEST_INFO_CLOSING") -- Handled by Framework_QuestGiver
	KEP_EventEncodeString(ev, tostring(m_updatedQuestsPending))
	KEP_EventQueue(ev)
end
-- Button Handlers

function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_gameItemCacheInit and m_inventoryInit and m_populateMenuPending then
		m_populateMenuPending = false
		populateMenu()
	end
end

-- Accept Quest
function btnAccept_OnButtonClicked(buttonHandle)
	local canCompleteQuest = Static_GetText(gHandles["btnAccept"]) == "Complete Quest"

	if canCompleteQuest then

		-- m_questInfoDialog = MenuOpen("Framework_QuestTurnIn.xml")
		-- -- Send quest info over
		-- local ev = KEP_EventCreate("FRAMEWORK_QUEST_INFO") -- Handled by Framework_QuestInfo or Framework_QuestTurnIn
		-- KEP_EventEncodeString(ev, Events.encode(m_questInfo))
		-- KEP_EventEncodeString(ev, tostring(true))
		-- KEP_EventEncodeString(ev, Events.encode(m_questInfo))
		-- KEP_EventEncodeNumber(ev, m_actorPID)
		-- KEP_EventQueue(ev)

		handleQuestCompletion()
	else 
		m_updatedQuestsPending = true
		addQuest(m_questInfo)
		local newItemEvent = KEP_EventCreate("FRAMEWORK_NEW_ITEM_MESSAGE")
		KEP_EventEncodeString(newItemEvent,"Quest")
		KEP_EventQueue(newItemEvent)

		-- close the menu 
		MenuCloseThis()
	end 
end

function btnCancel_OnButtonClicked(buttonHandle)
	local questItem = m_gameItems[tostring(m_questInfo.UNID)]
	local canCompleteQuest = Static_GetText(gHandles["btnAccept"]) == "Complete Quest"
	if questItem  and m_questAcceptable and not canCompleteQuest then
		if questItem.autoAccept and questItem.autoAccept == "true" then
			-- untrackQuest()
			m_trackedQuest = m_questInfo.UNID
			trackQuest()
		end
	end
	MenuCloseThis()
end

function btnTrack_OnButtonClicked(buttonHandle)
	if m_trackedQuest == m_questInfo.UNID then
		untrackQuest()
	else
		trackQuest()
	end
end

function btnDelete_OnButtonClicked(buttonHandle)
	abandonQuest(m_questInfo.UNID)
	MenuCloseThis()
end

function btnQuests_OnButtonClicked(buttonHandle)

	MenuCloseThis()

	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")				
	end

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "quests")
	KEP_EventQueue(ev)
	MenuClose("Framework_QuestGiver.xml")
end

-- Event handlers
function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)

	local inventory = m_playerInventory
	if inventory[updateIndex] then
		inventory[updateIndex] = updateItem
		updateInventory(inventory)
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local inventory = decompileInventory(tEvent)

	m_inventoryInit = true
	updateInventory(inventory)
end

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems

	m_gameItemCacheInit = true
	m_populateMenuPending = true
end

function updateInventory(inventory)
	if inventory then
		m_playerInventory = inventory
	end

	m_playerInventoryByUNID = {}
	for i=1,#m_playerInventory do
		if m_playerInventoryByUNID[tostring(m_playerInventory[i].UNID)] == nil then
			m_playerInventoryByUNID[tostring(m_playerInventory[i].UNID)] = m_playerInventory[i].count
		else
			m_playerInventoryByUNID[tostring(m_playerInventory[i].UNID)] = m_playerInventoryByUNID[tostring(m_playerInventory[i].UNID)] + m_playerInventory[i].count
		end
	end 

	m_populateMenuPending = true
end

function returnTrackedQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = tonumber(KEP_EventDecodeString(tEvent))

	if questUNID then	
		m_trackedQuest = questUNID
		--if  m_fromQuestGiver then
			m_populateMenuPending = true
		--end
	end
end


-- Handled quest info passed from Framework_QuestGiver
function onQuestInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_questInfo = Events.decode(KEP_EventDecodeString(event))
	m_questAcceptable = KEP_EventDecodeString(event) == "true"
	m_isModal = KEP_EventDecodeString(event) == "true"
	if KEP_EventMoreToDecode(event) ~= 0 then
		m_rewardItem 	 = Events.decode(KEP_EventDecodeString(event))
		m_actorPID		 = KEP_EventDecodeNumber(event)
		m_fromQuestGiver = true
	end

	m_populateMenuPending = true
end

-- Preprocess check
function onCheckFullResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local lootValid = (KEP_EventDecodeString(tEvent) == "true")
	local menuName = KEP_EventDecodeString(tEvent)
	local removeValid = (KEP_EventDecodeString(tEvent) == "true")
	local addValid = (KEP_EventDecodeString(tEvent) == "true")
	if not lootValid  and not addValid then -- Unlock the menu so the player can create room in their backpack
		MenuSetModalThis(false)
		-- Dialog_SetCloseOnESC(gDialogHandle, 1)
		-- Control_SetVisible(gHandles["btnClose"], true)
		-- Control_SetEnabled(gHandles["btnClose"], true)
	end
end

-- Response for loot acquisition event
function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	if success then
		local requiredUNID = tonumber(m_questInfo.properties.requiredItem.requiredUNID)
		-- No required item? Add AND complete this quest in one pass
		--if (requiredUNID == 0) then
			-- log("--- m_questInfo is complete")
			-- m_questInfo.complete = true
			-- addQuest(m_questInfo)
		--else
			completeQuest(m_questInfo.UNID, m_questInfo)		
		--end
		
		--Animation and particle effect for completing a quest
		local player = KEP_GetPlayer()
		local netId = GetSet_GetNumber(player, MovementIds.NETWORKDEFINEDID)
		Events.sendEvent("FRAMEWORK_QUEST_PLAY_SOUND")
		if KEP_IsMale() then
			KEP_SetCurrentAnimationByGLID(COMPLETION_ANIMATION_M)
		else
			KEP_SetCurrentAnimationByGLID(COMPLETION_ANIMATION_F)
		end
		m_updatedQuestsPending = true
		MenuCloseThis()
	end
end

--------------------
----Menu Display----
--------------------
function populateMenu()
	local questProperties = m_questInfo.properties
	local requiredItem = m_gameItems[tostring(questProperties.requiredItem.requiredUNID)]
	local rewardItem = m_gameItems[tostring(questProperties.rewardItem.rewardUNID)]

	if requiredItem then
		questProperties.requiredItem.GLID = requiredItem.GLID
		questProperties.requiredItem.name = requiredItem.name
	end
	if rewardItem then
		questProperties.rewardItem.GLID = rewardItem.GLID
		questProperties.rewardItem.name = rewardItem.name
	end

	local imgHandle = Image_GetDisplayElement(gHandles["imgQuestIcon"])
	if imgHandle and gDialogHandle then
		KEP_LoadIconTextureByID(tonumber(questProperties.GLID), imgHandle, gDialogHandle)
	end
	Control_SetVisible(gHandles["imgQuestIcon"], true)
	
	-- Set quest name
	local nameHeight = Static_GetStringHeight(gHandles["stcQuestName"], tostring(questProperties.name))
	Static_SetText(gHandles["stcQuestName"], tostring(m_questInfo.properties.name))
	Control_SetSize(gHandles["stcQuestName"], Control_GetWidth(gHandles["stcQuestName"]), nameHeight)
	Control_SetVisible(gHandles["stcQuestName"], true)
	
	-- Set quest description
	local descriptionHeight = Static_GetStringHeight(gHandles["stcQuestText"], tostring(m_questInfo.properties.questPreText))
	Static_SetText(gHandles["stcQuestText"], tostring(m_questInfo.properties.questPreText))
	Control_SetSize(gHandles["stcQuestText"], Control_GetWidth(gHandles["stcQuestText"]), descriptionHeight)
	local descriptionY = Control_GetLocationY(gHandles["stcQuestName"]) + nameHeight + QUEST_DESCRIPTION_Y_BUFFER
	Control_SetSize(gHandles["stcQuestText"], Control_GetWidth(gHandles["stcQuestText"]), descriptionHeight)
	Control_SetLocationY(gHandles["stcQuestText"], descriptionY)
	Control_SetVisible(gHandles["stcQuestText"], true)

	local placementY = math.max(Control_GetLocationY(gHandles["imgQuestIcon"]) + Control_GetHeight(gHandles["imgQuestIcon"]), descriptionY+descriptionHeight) + ITEM_BUFFER
	Control_SetLocationY(gHandles["stcQuestObjective"], placementY)
	Control_SetVisible(gHandles["stcQuestObjective"], true)
	placementY = placementY + ITEM_BUFFER
	Control_SetLocationY(gHandles["imgLine2"], placementY)
	Control_SetVisible(gHandles["imgLine2"], true)
	
	placementY = placementY + 10
	
	-- Populate the quest objective
	Control_SetLocationY(gHandles["imgObjective"], placementY)
	Control_SetLocationY(gHandles["imgObjectiveBG"], placementY - ITEM_BUFFER)

	Control_SetLocationY(gHandles["stcObjective"], placementY)
	Control_SetLocationY(gHandles["stcObjectiveCompleted"], Control_GetLocationY(gHandles["stcObjective"]) + Control_GetHeight(gHandles["stcObjective"]))
	
	--Quest text
	local questObjective = getQuestObjectiveText(questProperties)
	Static_SetText(gHandles["stcObjective"], questObjective)
	Control_SetVisible(gHandles["stcObjective"], true)
	
	--Always Accept quest except for one use case (Collect quest and I have all items)
	Static_SetText(gHandles["btnAccept"], "Accept Quest")
	-- tell the user they already have the items needed for this quest
	if requiredItem then
		setQuestProgressText(questProperties, requiredItem)
	end
	
	-- Set item image
	local imgHandle = Image_GetDisplayElement(gHandles["imgObjective"])
	if imgHandle and gDialogHandle then
		if questProperties.goalImage and questProperties.goalImage ~= "" and questProperties.goalImage ~= "none" then
			-- Set image. This part only needs to be done once, even if this function is called again
			if m_image == nil then
				m_image = questProperties.goalImage
				m_imgDispElement = imgHandle
				local lastSlashIndex = string.find(m_image, "/[^/]*$")
				local periodIndex = string.find(m_image,  '.', 1, true)
				if lastSlashIndex and periodIndex then
					local imageName =  string.sub(m_image, lastSlashIndex + 1, periodIndex - 1) or "none"
					m_imageFile = downloadImage(m_image, imageName.."_QuestGoal")
				end
			end
		else

			local imageGLID = getQuestObjectiveGLID(questProperties)
			KEP_LoadIconTextureByID(tonumber(imageGLID), imgHandle, gDialogHandle)
		end
	end
	Control_SetVisible(gHandles["imgObjective"], true)
	
	
	local reward = m_questInfo.properties.rewardItem
	if reward.rewardUNID ~= "0" then
		placementY = placementY + Control_GetHeight(gHandles["imgObjective"]) + 20
		
		Control_SetLocationY(gHandles["stcRewardLine"], placementY)
		Control_SetVisible(gHandles["stcRewardLine"], true)
		placementY = placementY + ITEM_BUFFER
		Control_SetLocationY(gHandles["imgLine3"], placementY)
		Control_SetVisible(gHandles["imgLine3"], true)
		
		placementY = placementY + 10
		-- Populate the quest reward
		Control_SetLocationY(gHandles["imgReward"], placementY)
		Control_SetLocationY(gHandles["stcReward"], placementY)
		
		
		Static_SetText(gHandles["stcReward"], "(".. tostring(formatCommaValue(reward.rewardCount))..") "..tostring(reward.name))
		
		
		Control_SetVisible(gHandles["stcReward"], true)
		-- Set reward item image
		local imgHandle = Image_GetDisplayElement(gHandles["imgReward"])
		if imgHandle and gDialogHandle then
			KEP_LoadIconTextureByID(tonumber(reward.GLID), imgHandle, gDialogHandle)
		end
		Control_SetVisible(gHandles["imgReward"], true)
	end
	
	placementY = placementY + Control_GetHeight(gHandles["imgReward"]) + 20
	
	local menuWidth = Dialog_GetWidth(gDialogHandle)

	if m_questInfo.UNID then
		local questItem = m_gameItems[tostring(m_questInfo.UNID)]
		if questItem then
			if m_isModal then
				MenuSetModalThis(true)
				Dialog_SetCloseOnESC(gDialogHandle, 0)
				Control_SetVisible(gHandles["btnClose"], false)
				Control_SetEnabled(gHandles["btnClose"], false)
				local canCompleteQuest = Static_GetText(gHandles["btnAccept"]) == "Complete Quest"
				if canCompleteQuest then
					preprocessCheck()
				end
			end

			if m_questAcceptable then -- If not already on the quest
				if questItem.autoAccept and questItem.autoAccept == "true" then -- If auto-accept
					local canCompleteQuest = Static_GetText(gHandles["btnAccept"]) == "Complete Quest"
					if canCompleteQuest then -- If able to complete right away
						local placementX = Control_GetLocationX(gHandles["btnDelete"]) + Control_GetWidth(gHandles["btnDelete"])/2 - Control_GetWidth(gHandles["btnAccept"])/2
						Control_SetLocation(gHandles["btnAccept"], placementX, placementY)
						Control_SetVisible(gHandles["btnAccept"], true)
						Control_SetVisible(gHandles["btnCancel"], false)
					else -- If NOT able to complete right away
						Static_SetText(Button_GetStatic(gHandles["btnCancel"]), "OK")
						Control_SetLocation(gHandles["btnCancel"], Control_GetLocationX(gHandles["btnDelete"]), placementY)
						Control_SetVisible(gHandles["btnCancel"], true)
					end
					autoAccept(questItem)
					-- Control_SetVisible(gHandles["btnTrack"],false)
					-- Control_SetVisible(gHandles["btnDelete"],false)
					-- Control_SetVisible(gHandles["btnQuests"],false)
					-- Control_SetVisible(gHandles["btnAccept"], false)
				else -- If NOT auto-accept
					Control_SetLocationY(gHandles["btnAccept"], placementY)
					Control_SetVisible(gHandles["btnAccept"], true)
					Control_SetLocationY(gHandles["btnCancel"], placementY)
					Control_SetVisible(gHandles["btnCancel"], true)
				end
			else -- If already on the quest
				if questItem.autoAccept and questItem.autoAccept == "true" then -- If auto-accept
					Static_SetText(Button_GetStatic(gHandles["btnCancel"]), "OK")
					Control_SetLocation(gHandles["btnCancel"], Control_GetLocationX(gHandles["btnDelete"]), placementY)
					Control_SetVisible(gHandles["btnCancel"], true)
					Control_SetVisible(gHandles["btnDelete"], false)  -- Auto-accept quests cannot be deleted
				else -- If NOT auto-accept
					Static_SetText(Button_GetStatic(gHandles["btnCancel"]), "Close")
					Control_SetLocation(gHandles["btnCancel"], CLOSE_X, placementY)
					Control_SetVisible(gHandles["btnCancel"], false)
					Control_SetLocationY(gHandles["btnDelete"], placementY)
					Control_SetVisible(gHandles["btnDelete"],true)
				end

				Control_SetLocationY(gHandles["btnQuests"], placementY)
				Control_SetVisible(gHandles["btnQuests"],true)
				Control_SetLocationY(gHandles["btnTrack"], placementY)
				Control_SetVisible(gHandles["btnTrack"],true)

				if m_questInfo.UNID == m_trackedQuest then
					Static_SetText(Button_GetStatic(gHandles["btnTrack"]), "Stop Tracking")
				else
					Static_SetText(Button_GetStatic(gHandles["btnTrack"]), "Show in Compass")
				end
			end
		end
	end

	local menuHeight = Control_GetLocationY(gHandles["btnCancel"]) + Control_GetHeight(gHandles["btnCancel"]) + 20
	Dialog_SetSize(gDialogHandle, menuWidth, menuHeight)
	MenuCenterThis()
end

--Get the quest obejective quest
function getQuestObjectiveText(questProperties)
	local objectiveString = ""
	local questType = questProperties.questType

	if questType == "goTo" then
		local goToDescription = questProperties.goToDescription or "target location"
		if goToDescription == "" then
			goToDescription =  "target location"
		end
		objectiveString = FONT_WHITE.."Go to"..FONT_END..FONT_YELLOW.." "..goToDescription..FONT_END
	elseif questType == "collect"  or usesFulfillCount(questType) then
		local requiredCount = tostring(questProperties.requiredItem.requiredCount or 0)
		local requiredItem = m_gameItems[tostring(questProperties.requiredItem.requiredUNID)]
		local requiredItemName = "object"
		if requiredItem then
			requiredItemName = requiredItem.name
		end
		local goal = string.gsub(questType, "^%l", string.upper)
		
		objectiveString = FONT_WHITE..goal.." ("..requiredCount..")"..FONT_END..FONT_YELLOW.." "..requiredItemName..FONT_END
	elseif questType == "talkTo" then
		local target = m_gameItems[tostring(questProperties.targetUNID or 0)]
		local targetName = "character"
		if target then
			targetName = target.behaviorParams and target.behaviorParams.actorName
			if targetName == nil or targetName == "" then
				targetName = target.name
			end
		end
		objectiveString = FONT_WHITE.."Talk to"..FONT_END..FONT_YELLOW.." "..targetName..FONT_END
	elseif questType == "takeTo" then
		local requiredCount = tostring(questProperties.requiredItem.requiredCount or 0)
		local requiredItem = m_gameItems[tostring(questProperties.requiredItem.requiredUNID)]
		local requiredItemName = "object"
		if requiredItem then
			requiredItemName = requiredItem.name
		end
		local target = m_gameItems[tostring(questProperties.targetUNID or 0)]
		local targetName = "character"
		if target then
			targetName = target.behaviorParams and target.behaviorParams.actorName
			if targetName == nil or targetName == "" then
				targetName = target.name
			end
		end
		objectiveString = FONT_WHITE.."Take ("..requiredCount..")"..FONT_END..FONT_YELLOW.." "..requiredItemName..FONT_END..FONT_WHITE.." to"..FONT_END..FONT_YELLOW.." "..targetName..FONT_END
	end

	if tostring(questProperties.autoComplete) ~= "true" then
		objectiveString = objectiveString .. FONT_WHITE.." then return"..FONT_END
	end

	return objectiveString
end

function getQuestObjectiveGLID(questProperties)
	local questType = questProperties.questType
	if questType == "collect" or usesFulfillCount(questType) then
		local requiredItem = m_gameItems[tostring(questProperties.requiredItem.requiredUNID)]
		if requiredItem then
			return requiredItem.GLID
		end
	elseif questType == "talkTo" or questType == "takeTo" then
		local target = m_gameItems[tostring(questProperties.targetUNID or 0)]
		if target then
			return target.GLID
		end
	end

	return WAYPOINT_GLID
end

function setQuestProgressText(questProperties, requiredItemProperties)
	local questType = questProperties.questType
	local requiredItemName = requiredItemProperties.name
	local requiredCount = questProperties.requiredItem.requiredCount or 0
	local progressCount = 0
	local progressText = ""

	if usesFulfillCount(questType) then
		if m_questAcceptable then return end
		progressCount = m_questInfo.fulfillCount or 0
		progressText = "You "
		--Find quest progress for crafting
		if questType == "place" then
			progressText = progressText .."placed ("..tostring(progressCount)..") "..requiredItemName
		else
			progressText = progressText..questType.."ed ("..tostring(progressCount)..") "..requiredItemName
		end

		Control_SetVisible(gHandles["stcObjectiveCompleted"], true)
	else
		Control_SetVisible(gHandles["stcObjectiveCompleted"], true)
		progressCount = m_playerInventoryByUNID[tostring(questProperties.requiredItem.requiredUNID)] or 0	
		progressText = "You have ("..tostring(progressCount)..") "..requiredItemName
	end

	if tonumber(progressCount) >= tonumber(requiredCount) then

		progressText = FONT_GREEN..progressText..FONT_END
		if questType == "collect" then
			Static_SetText(gHandles["btnAccept"], "Complete Quest")
		end
       -- BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["stcObjectiveItemCount"])), 0, customColor)
	else 
		progressText = FONT_RED..progressText..FONT_END
	end 

	Static_SetText(gHandles["stcObjectiveCompleted"], progressText)
		
end

-- Attempt to add / remove rewards
function handleQuestCompletion()
	local requiredUNID = tonumber(m_questInfo.properties.requiredItem.requiredUNID)
	local requiredCount = m_questInfo.properties.requiredItem.requiredCount
	local rewardUNID = tonumber(m_questInfo.properties.rewardItem.rewardUNID)
	local rewardCount = m_questInfo.properties.rewardItem.rewardCount
	
	local transactionID = KEP_GetCurrentEpochTime()	
	local removeTable = {}
	if (requiredUNID > 0) and m_questInfo.properties.destroyOnCompletion == "true" then
		local removeItem = {UNID = requiredUNID, count = requiredCount}
		removeItem.lootInfo = {sourcePID = m_actorPID, lootSource = "Quest", transactionID = transactionID, questUNID = m_questInfo.UNID}
		table.insert(removeTable, removeItem)
	end
	
	local addTable = {}
	m_rewardItem.properties = m_gameItems[tostring(rewardUNID)]
	m_rewardItem.UNID = rewardUNID
	if m_rewardItem.UNID and tonumber(m_rewardItem.UNID) > 0 then
		local addItem = deepCopy(m_rewardItem)
		addItem.lootInfo = {sourcePID = m_actorPID, lootSource = "Quest", transactionID = transactionID}
		addItem.count = rewardCount or 1
		table.insert(addTable, addItem)
	end

	processTransaction(removeTable, addTable)
end

function formatCommaValue(value)
  	local formatted = value
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end

trackQuest = function()
	local quest = m_questInfo

	if quest then
		-- Send quest info over to Quest Compass
		local ev = KEP_EventCreate("FRAMEWORK_TRACK_PLAYER_QUEST")
		KEP_EventEncodeString(ev, quest.UNID)		
		KEP_EventQueue(ev)
		--formatDisplayTable()
		Static_SetText(Button_GetStatic(gHandles["btnTrack"]), "Stop Tracking")

		m_trackedQuest = m_questInfo.UNID
	end	
end

untrackQuest = function()
	m_trackedQuest = nil

	-- Send quest info over to Quest Compass
	local ev = KEP_EventCreate("FRAMEWORK_TRACK_PLAYER_QUEST")
	KEP_EventEncodeString(ev, 0)		
	KEP_EventQueue(ev)
	Static_SetText(Button_GetStatic(gHandles["btnTrack"]), "Show in Compass")
end

function autoAccept(questItem)
	if not m_autoAccepted then
		m_autoAccepted = true
		m_updatedQuestsPending = true
		addQuest(m_questInfo)
		local newItemEvent = KEP_EventCreate("FRAMEWORK_NEW_ITEM_MESSAGE")
		KEP_EventEncodeString(newItemEvent,"Quest")
		KEP_EventQueue(newItemEvent)
	end
end

function preprocessCheck()
	local requiredUNID = tonumber(m_questInfo.properties.requiredItem.requiredUNID)
	local requiredCount = m_questInfo.properties.requiredItem.requiredCount
	local rewardUNID = tonumber(m_questInfo.properties.rewardItem.rewardUNID)
	local rewardCount = m_questInfo.properties.rewardItem.rewardCount
	
	local transactionID = KEP_GetCurrentEpochTime()	
	local removeTable = {}
	if (requiredUNID > 0) and m_questInfo.properties.destroyOnCompletion == "true" then
		local removeItem = {UNID = requiredUNID, count = requiredCount}
		removeItem.lootInfo = {sourcePID = m_actorPID, lootSource = "Quest", transactionID = transactionID, questUNID = m_questInfo.UNID}
		table.insert(removeTable, removeItem)
	end
	
	local addTable = {}

	m_rewardItem.properties = m_gameItems[tostring(rewardUNID)]
	m_rewardItem.UNID = rewardUNID
	if m_rewardItem.UNID and tonumber(m_rewardItem.UNID) > 0 then
		local addItem = deepCopy(m_rewardItem)
		addItem.lootInfo = {sourcePID = m_actorPID, lootSource = "Quest", transactionID = transactionID}
		addItem.count = rewardCount or 1
		table.insert(addTable, addItem)
	end


	processTransaction(removeTable, addTable, true)
end

--------------------------
-----Images
--------------------------

function downloadImage(imgURL, chapterName)
	--the name the image will saved as

	local filename = tostring(chapterName)..".jpg"
	local path = KEP_DownloadTextureAsyncToGameId(filename, imgURL)
	
    local gameId = KEP_GetGameId()
	
	path = string.gsub(path,".-\\", "" )
	path = "../../CustomTexture/".. gameId .. "/" .. path
	return path
end


function textureDownloadHandler()
	updateImage()
end

function updateImage()
	if m_imgDispElement then
		Element_AddTexture(m_imgDispElement, gDialogHandle,m_imageFile)
		local textureWidth = Element_GetTextureWidth(m_imgDispElement, gDialogHandle)
		local textureHeight = Element_GetTextureHeight(m_imgDispElement, gDialogHandle)

		m_imageWidth = textureWidth
		scaleQuestImage(textureWidth, textureHeight)
	end
end

function scaleQuestImage(width, height)
	local newWidth = width
	local newHeight = height

	if width <= height then
		if width ~= IMAGE_SIZE then
			newHeight = height * (IMAGE_SIZE / width)
			newWidth = IMAGE_SIZE
		end
	else
		if height ~= IMAGE_SIZE then
			newWidth = width * (IMAGE_SIZE / height) 
			newHeight = IMAGE_SIZE
		end
	end

	setQuestImage(newWidth, newHeight)
	Element_SetCoords(m_imgDispElement, 0, 0, LeastPowerofTwo(width), LeastPowerofTwo(height))
end

function setQuestImage(width, height)
	local imageHandle = gHandles["imgObjective"]
	local imageBGHandle = gHandles["imgObjectiveBG"]
	Control_SetSize(imageHandle, width, height)
	if height > IMAGE_SIZE then
		local heightDifference  = height - IMAGE_SIZE
		local moveDistance = heightDifference / 2
		Control_SetLocation(imageHandle, Control_GetLocationX(imageHandle), Control_GetLocationY(imageHandle)-moveDistance)
	end

	if width > IMAGE_SIZE then
		local widthDifference  = width - IMAGE_SIZE
		local moveDistance = widthDifference / 2
		Control_SetLocation(imageHandle, Control_GetLocationX(imageHandle) - moveDistance, Control_GetLocationY(imageHandle))
	end

	local containerX, containerY = Control_GetLocationX(imageBGHandle), Control_GetLocationY(imageBGHandle)
	local containerW, containerH = Control_GetWidth(imageBGHandle), Control_GetHeight(imageBGHandle)

	Control_SetClipRect(imageHandle, containerX, containerY, containerX+containerW, containerY+containerH)
end