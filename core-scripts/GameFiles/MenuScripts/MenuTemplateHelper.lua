--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

CAPTION_HEIGHT 	= 30
FOOTER_HEIGHT 	= 66
INFO_HEIGHT 	= 28

BTN_CLOSE_Y		= 9
BTN_CLOSE_X		= 34
CONTENT_Y		= 42
CONTENT_H		= 145
FRAME_TOP_Y		= -2
FRAME_SIDE_Y	= 3
FRAME_SIDE_H	= -145

gCaptionHeight 	= CAPTION_HEIGHT
gFooterHeight 	= FOOTER_HEIGHT
gInfoHeight 	= INFO_HEIGHT

DialogBase = {}
DialogBase.OuterFrame = {}
DialogBase.Content = {}

---------------------------------------------------------------------
-- Scrollbar Setup
---------------------------------------------------------------------

function setupScrollBarTextures( lbh )
	-- setup look of listbox scrollbars and populate list
	if lbh ~= 0 then
		local sbh = ListBox_GetScrollBar(lbh)
		if sbh ~= 0 then
			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end
			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end
			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end
			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end
end

---------------------------------------------------------------------
-- Backround Setup
---------------------------------------------------------------------

function setupDialogBase( dialogHandle, wid, hgt, useCaption, useFooter, useInfo )
	if useCaption == false then
		gCaptionHeight = 0
	end
	if useFooter == false then
		gFooterHeight = 0
	end
	if useInfo == false then
		gInfoHeight = 0
	end

	Dialog_SetCaptionEnabled( dialogHandle, useCaption )

	DialogBase.hdl = dialogHandle
	DialogBase.setupContentArea( dialogHandle )
	DialogBase.setupOuterFrame( dialogHandle )
	ResizeDialogBase( wid, hgt )
end

function DialogBase.setupContentArea( dialogHandle )

	DialogBase.Content.Right = {}
	DialogBase.Content.Right.x = 1
	DialogBase.Content.Right.y = 1
	DialogBase.Content.Right.w = 8
	DialogBase.Content.Right.h = 1
	DialogBase.Content.Right.hdl = Dialog_AddImageToBack(dialogHandle,
		"contentRight",
		DialogBase.Content.Right.x,
		DialogBase.Content.Right.y,
		"gameUI.tga", 42, 355, 50, 708
	)
	Control_SetSize( DialogBase.Content.Right.hdl, DialogBase.Content.Right.w, DialogBase.Content.Right.h )

	DialogBase.Content.Mid = {}
	DialogBase.Content.Mid.x = 19
	DialogBase.Content.Mid.y = 1
	DialogBase.Content.Mid.w = 1
	DialogBase.Content.Mid.h = 1
	DialogBase.Content.Mid.hdl = Dialog_AddImageToBack(dialogHandle,
		"contentMid",
		DialogBase.Content.Mid.x,
		DialogBase.Content.Mid.y,
		"gameUI.tga", 36, 355, 39, 708
	)
	Control_SetSize( DialogBase.Content.Mid.hdl, DialogBase.Content.Mid.w, DialogBase.Content.Mid.h )

	DialogBase.Content.Left = {}
	DialogBase.Content.Left.x = 11
	DialogBase.Content.Left.y = 1
	DialogBase.Content.Left.w = 8
	DialogBase.Content.Left.h = 1
	DialogBase.Content.Left.hdl = Dialog_AddImageToBack(dialogHandle,
		"contentLeft",
		DialogBase.Content.Left.x,
		DialogBase.Content.Left.y,
		"gameUI.tga", 26, 355, 33, 708
	)
	Control_SetSize( DialogBase.Content.Left.hdl, DialogBase.Content.Left.w, DialogBase.Content.Left.h )
end

function DialogBase.setupOuterFrame( dialogHandle )
	DialogBase.OuterFrame.Right = {}
	DialogBase.OuterFrame.Right.x = 1
	DialogBase.OuterFrame.Right.y = FRAME_SIDE_Y - gCaptionHeight
	DialogBase.OuterFrame.Right.w = 4
	DialogBase.OuterFrame.Right.h = 1
	DialogBase.OuterFrame.Right.hdl = Dialog_AddImageToBack(dialogHandle,
		"outerFrameRight",
		DialogBase.OuterFrame.Right.x,
		DialogBase.OuterFrame.Right.y,
		"gameUI.tga", 56, 507, 59, 509
	)
	Control_SetSize(DialogBase.OuterFrame.Right.hdl, DialogBase.OuterFrame.Right.w, DialogBase.OuterFrame.Right.h)

	DialogBase.OuterFrame.Bottom = {}
	DialogBase.OuterFrame.Bottom.x = 3
	DialogBase.OuterFrame.Bottom.y = 1
	DialogBase.OuterFrame.Bottom.w = 1
	DialogBase.OuterFrame.Bottom.h = 4
	DialogBase.OuterFrame.Bottom.hdl = Dialog_AddImageToBack(dialogHandle,
		"outerFrameBottom",
		DialogBase.OuterFrame.Bottom.x,
		DialogBase.OuterFrame.Bottom.y,
		"gameUI.tga", 56, 507, 59, 509
	)
	Control_SetSize(DialogBase.OuterFrame.Bottom.hdl, DialogBase.OuterFrame.Bottom.w, DialogBase.OuterFrame.Bottom.h)

	DialogBase.OuterFrame.Left = {}
	DialogBase.OuterFrame.Left.x = -2
	DialogBase.OuterFrame.Left.y = FRAME_SIDE_Y - gCaptionHeight
	DialogBase.OuterFrame.Left.w = 4
	DialogBase.OuterFrame.Left.h = 1
	DialogBase.OuterFrame.Left.hdl = Dialog_AddImageToBack(dialogHandle,
		"outerFrameLeft",
		DialogBase.OuterFrame.Left.x,
		DialogBase.OuterFrame.Left.y,
		"gameUI.tga", 56, 507, 59, 509
	)
	Control_SetSize(DialogBase.OuterFrame.Left.hdl, DialogBase.OuterFrame.Left.w, DialogBase.OuterFrame.Left.h)

	DialogBase.OuterFrame.Top = {}
	DialogBase.OuterFrame.Top.x = 3
	DialogBase.OuterFrame.Top.y = FRAME_TOP_Y - gCaptionHeight
	DialogBase.OuterFrame.Top.w = 1
	DialogBase.OuterFrame.Top.h = 4
	DialogBase.OuterFrame.Top.hdl = Dialog_AddImageToBack(dialogHandle,
		"outerFrameTop",
		DialogBase.OuterFrame.Top.x,
		DialogBase.OuterFrame.Top.y,
		"gameUI.tga", 56, 507, 59, 509
	)
	Control_SetSize(DialogBase.OuterFrame.Top.hdl, DialogBase.OuterFrame.Top.w, DialogBase.OuterFrame.Top.h)

	DialogBase.OuterFrame.BottomRight = {}
	DialogBase.OuterFrame.BottomRight.x = 1
	DialogBase.OuterFrame.BottomRight.y = 1
	DialogBase.OuterFrame.BottomRight.w = 5
	DialogBase.OuterFrame.BottomRight.h = 5
	DialogBase.OuterFrame.BottomRight.hdl = Dialog_AddImageToBack(dialogHandle,
		"outerFrameBR",
		DialogBase.OuterFrame.BottomRight.x,
		DialogBase.OuterFrame.BottomRight.y,
		"gameUI.tga", 60, 498, 65, 503
	)
	Control_SetSize(DialogBase.OuterFrame.BottomRight.hdl, DialogBase.OuterFrame.BottomRight.w, DialogBase.OuterFrame.BottomRight.h)

	DialogBase.OuterFrame.BottomLeft = {}
	DialogBase.OuterFrame.BottomLeft.x = -2
	DialogBase.OuterFrame.BottomLeft.y = 1
	DialogBase.OuterFrame.BottomLeft.w = 5
	DialogBase.OuterFrame.BottomLeft.h = 5
	DialogBase.OuterFrame.BottomLeft.hdl = Dialog_AddImageToBack(dialogHandle,
		"outerFrameBL",
		DialogBase.OuterFrame.BottomLeft.x,
		DialogBase.OuterFrame.BottomLeft.y,
		"gameUI.tga", 54, 498, 59, 503
	)
	Control_SetSize(DialogBase.OuterFrame.BottomLeft.hdl, DialogBase.OuterFrame.BottomLeft.w, DialogBase.OuterFrame.BottomLeft.h)

	DialogBase.OuterFrame.TopRight = {}
	DialogBase.OuterFrame.TopRight.x = 1
	DialogBase.OuterFrame.TopRight.y = FRAME_TOP_Y - gCaptionHeight
	DialogBase.OuterFrame.TopRight.w = 5
	DialogBase.OuterFrame.TopRight.h = 5
	DialogBase.OuterFrame.TopRight.hdl = Dialog_AddImageToBack(dialogHandle,
		"outerFrameTR",
		DialogBase.OuterFrame.TopRight.x,
		DialogBase.OuterFrame.TopRight.y,
		"gameUI.tga", 60, 492, 65, 497
	)
	Control_SetSize(DialogBase.OuterFrame.TopRight.hdl, DialogBase.OuterFrame.TopRight.w, DialogBase.OuterFrame.TopRight.h)

	DialogBase.OuterFrame.TopLeft = {}
	DialogBase.OuterFrame.TopLeft.x = -2
	DialogBase.OuterFrame.TopLeft.y = FRAME_TOP_Y - gCaptionHeight
	DialogBase.OuterFrame.TopLeft.w = 5
	DialogBase.OuterFrame.TopLeft.h = 5
	DialogBase.OuterFrame.TopLeft.hdl = Dialog_AddImageToBack(dialogHandle,
		"outerFrameTL",
		DialogBase.OuterFrame.TopLeft.x,
		DialogBase.OuterFrame.TopLeft.y,
		"gameUI.tga", 54, 492, 59, 497
	)
	Control_SetSize(DialogBase.OuterFrame.TopLeft.hdl, DialogBase.OuterFrame.TopLeft.w, DialogBase.OuterFrame.TopLeft.h)

	if gInfoHeight > 0 then
		DialogBase.OuterFrame.WhiteBar = {}
		DialogBase.OuterFrame.WhiteBar.x = 0
		DialogBase.OuterFrame.WhiteBar.y = 1
		DialogBase.OuterFrame.WhiteBar.w = 1
		DialogBase.OuterFrame.WhiteBar.h = 28
		DialogBase.OuterFrame.WhiteBar.hdl = Dialog_AddImageToBack(dialogHandle,
			"outerFrameWhiteBar",
			DialogBase.OuterFrame.WhiteBar.x,
			DialogBase.OuterFrame.WhiteBar.y,
			"gameUI.tga", 788, 133, 795, 137
		)
		Control_SetSize(DialogBase.OuterFrame.WhiteBar.hdl, DialogBase.OuterFrame.WhiteBar.w, DialogBase.OuterFrame.WhiteBar.h)
	end

	if gFooterHeight > 0 then
		DialogBase.OuterFrame.Footer = {}
		DialogBase.OuterFrame.Footer.x = 0
		DialogBase.OuterFrame.Footer.y = 1
		DialogBase.OuterFrame.Footer.w = 1
		DialogBase.OuterFrame.Footer.h = 66
		DialogBase.OuterFrame.Footer.hdl = Dialog_AddImageToBack(dialogHandle,
			"outerFrameFooter",
			DialogBase.OuterFrame.Footer.x,
			DialogBase.OuterFrame.Footer.y,
			"gameUI.tga", 58, 396, 61, 462
		)
		Control_SetSize(DialogBase.OuterFrame.Footer.hdl, DialogBase.OuterFrame.Footer.w, DialogBase.OuterFrame.Footer.h)
	end

	if gCaptionHeight == 0 then
		DialogBase.OuterFrame.Header = {}
		DialogBase.OuterFrame.Header.x = 0
		DialogBase.OuterFrame.Header.y = 2
		DialogBase.OuterFrame.Header.w = 1
		DialogBase.OuterFrame.Header.h = 40
		DialogBase.OuterFrame.Header.hdl = Dialog_AddImageToBack(dialogHandle,
			"outerFrameHeader",
			DialogBase.OuterFrame.Header.x,
			DialogBase.OuterFrame.Header.y,
			"gameUI.tga", 58, 355, 61, 395
		)
		Control_SetSize(DialogBase.OuterFrame.Header.hdl, DialogBase.OuterFrame.Header.w, DialogBase.OuterFrame.Header.h)
	end

	DialogBase.OuterFrame.Body = {}
	DialogBase.OuterFrame.Body.x = 0
	DialogBase.OuterFrame.Body.y = 0
	DialogBase.OuterFrame.Body.w = 1
	DialogBase.OuterFrame.Body.h = 1
	DialogBase.OuterFrame.Body.hdl = Dialog_AddImageToBack(dialogHandle,
		"outerFrameBody",
		DialogBase.OuterFrame.Body.x,
		DialogBase.OuterFrame.Body.y,
		"gameUI.tga", 56, 484, 59, 487
	)
	Control_SetSize(DialogBase.OuterFrame.Body.hdl, DialogBase.OuterFrame.Body.w, DialogBase.OuterFrame.Body.h)
end

function DialogBase.removeDynamicElements( dialogHandle )
end

---------------------------------------------------------------------
-- Member Functions
---------------------------------------------------------------------
function DialogBase.resize( wid, hgt )
	-- Resize from Center
	DialogBase.centerX = Dialog_GetLocationX( DialogBase.hdl ) + ( Dialog_GetWidth( DialogBase.hdl ) / 2 )
	DialogBase.centerY = Dialog_GetLocationY( DialogBase.hdl ) + ( Dialog_GetHeight( DialogBase.hdl ) / 2 )

	DialogBase.w = wid
	DialogBase.h = hgt
	DialogBase.OuterFrame.resize( DialogBase.OuterFrame )
	DialogBase.Content.resize( DialogBase.Content )
	DialogBase.resizeCaptionArea( DialogBase.hdl )
	Dialog_SetSize( DialogBase.hdl, DialogBase.w, DialogBase.h )

	-- Resize from Center
	Dialog_SetLocation( DialogBase.hdl, DialogBase.centerX - ( DialogBase.w / 2 ), DialogBase.centerY - ( DialogBase.h / 2 ) )
end

function DialogBase.OuterFrame.resize( self )

	local infoFooterMod = ( gInfoHeight - INFO_HEIGHT ) + ( gFooterHeight - FOOTER_HEIGHT )

	self.TopRight.x = DialogBase.w - 1
	self.BottomLeft.y = DialogBase.h + 2 - gCaptionHeight + infoFooterMod
	self.BottomRight.x = DialogBase.w - 1
	self.BottomRight.y = DialogBase.h + 2 - gCaptionHeight + infoFooterMod
	self.Top.w = DialogBase.w - 2
	self.Left.h = DialogBase.h + infoFooterMod
	self.Bottom.y = DialogBase.h + 3 - gCaptionHeight + infoFooterMod
	self.Bottom.w = DialogBase.w - 2
	self.Right.x = DialogBase.w
	self.Right.h = DialogBase.h + infoFooterMod

	if self.Header ~= nil then
		self.Header.w = DialogBase.w
	end

	self.Body.w = DialogBase.w
	self.Body.h = DialogBase.h + 3 - gCaptionHeight + infoFooterMod

	if self.Footer ~= nil then
		self.Footer.y = DialogBase.h - 90 - gCaptionHeight
		self.Footer.w = DialogBase.w
	end
	if self.WhiteBar ~= nil then
		self.WhiteBar.y = DialogBase.h - 25 - gCaptionHeight + ( gFooterHeight - FOOTER_HEIGHT )
		self.WhiteBar.w = DialogBase.w + 2
	end
	for k, v in pairs( self ) do
		if v ~= DialogBase.OuterFrame.resize then	--This table (self) contains elements and functions. Only process the elements.
			Control_SetLocation( Image_GetControl(v.hdl), v.x, v.y )
			Control_SetSize( Image_GetControl(v.hdl), v.w, v.h )
		end
	end
end

function DialogBase.Content.resize( self )
	self.Left.y = CONTENT_Y - gCaptionHeight
	self.Left.h = DialogBase.h - CONTENT_H
	self.Mid.y = CONTENT_Y - gCaptionHeight
	self.Mid.w = DialogBase.w - 34
	self.Mid.h = DialogBase.h - CONTENT_H
	self.Right.x = DialogBase.w - 15
	self.Right.y = CONTENT_Y - gCaptionHeight
	self.Right.h = DialogBase.h - CONTENT_H
	for k, v in pairs( self ) do
		if v ~= DialogBase.Content.resize then	--This table (self) contains elements and functions. Only process the elements.
			Control_SetLocation( Image_GetControl(v.hdl), v.x, v.y )
			Control_SetSize( Image_GetControl(v.hdl), v.w, v.h )
		end
	end
end

function DialogBase.resizeCaptionArea( dialoghandle )
	local c = nil
	c = Dialog_GetControl( dialoghandle, "btnClose" )
	if c ~= nil then
		local y = BTN_CLOSE_Y - gCaptionHeight
		local x = DialogBase.w - BTN_CLOSE_X
		Control_SetLocation( c, x, y )
	end
	c = Dialog_GetControl( dialoghandle, "btnResize" )
	if c ~= nil then
		local y = BTN_CLOSE_Y - gCaptionHeight
		local x = DialogBase.w - BTN_CLOSE_X - 36
		Control_SetLocation( c, x, y )
	end
end

function ResizeDialogBase(wid, hgt )
	DialogBase.resize( wid, hgt )
	return KEP.EPR_OK
end
