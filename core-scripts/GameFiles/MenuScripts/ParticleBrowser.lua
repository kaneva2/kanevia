--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\MathHelper.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

DYNAMIC_OBJECT_INFO_EVENT = "DynamicObjectInfoEvent"

g_particles = {}

g_userID = 0
g_username = ""

--sorting
Sort_ParticleName = 1
Sort_ObjectPID = 2
Sort_ObjectName = 3
Sort_PositionX = 4
Sort_PositionY = 5
Sort_PositionZ = 6

g_bSortAscending = true
g_SortingBy = Sort_ObjectPID
g_SortFuncs = {} --filled in in after the sort functions are declared

g_queryString = ""

g_listhandles = {}
g_listscrollbars = {}

g_inventoryItems = {}

g_selectCount = 0
g_ignoreSelection = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "selectEventHandler", "SelectEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "clickEventHandler", "ClickEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "particleEffectCreatedEventHandler", "ParticleEffectCreatedEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister(DYNAMIC_OBJECT_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegister("ParticleEffectCreatedEvent", KEP.MED_PRIO )

	-- DRF - Added
	KEP_EventRegister("ObjectsUpdatedEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	g_listhandles =
	{
		gHandles["lbxNameDummy"],
		gHandles["lbxName"],
		gHandles["lbxPIDDummy"],
		gHandles["lbxPID"],
		gHandles["lbxObjectDummy"],
		gHandles["lbxObject"],
		gHandles["lbxXPosDummy"],
		gHandles["lbxXPos"],
		gHandles["lbxYPosDummy"],
		gHandles["lbxYPos"],
		gHandles["lbxZPosDummy"],
		gHandles["lbxZPos"]
	}

	g_listscrollbars = {}
	for i,v in ipairs(g_listhandles) do
		table.insert(g_listscrollbars, ListBox_GetScrollBar(v))
	end

	--Fix the spacing on the title
	Dialog_SetCaptionText(gDialogHandle, "    Particle Effects")

	selectObject(-1)

	ListBox_SetHTMLEnabled(gHandles["lbxObject"], true)
	ListBox_SetHTMLEnabled(gHandles["lbxName"], true)
	updateParticleList()
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.PARTICLE_INVENTORY then
		local xmlData = KEP_EventDecodeString( event )
		parseInventory(xmlData)
	elseif filter == WF.PARTICLE_BROWSER_INFO then
		local xmlData = KEP_EventDecodeString( event )
		ParseInfoResults(xmlData)
	end
end

function clickEventHandler(dispatcher, fromNetId, event, eventId, filter, unused)
	local button	 = KEP_EventDecodeNumber(event)
	local objectType = KEP_EventDecodeNumber(event)
	local objectID	 = KEP_EventDecodeNumber(event)
	local control	 = KEP_EventDecodeNumber(event)
	local shift		 = KEP_EventDecodeNumber(event)
	local alt		 = KEP_EventDecodeNumber(event)

	if objectType == ObjectType.DYNAMIC then
		KEP_ReplaceSelection(objectType, objectID)
	end
end

function selectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if g_selectCount <= 0 then
		updateSelection()
	end
	g_selectCount = g_selectCount - 1
end

function particleEffectCreatedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)

	-- DRF - Added
	sendObjectsUpdatedEvent()

	updateParticleList()
end

function parseInventory(xmlData)
	g_inventoryItems = {}
	decodeInventoryItemsFromWeb( g_inventoryItems, xmlData, false )
	for i,v in ipairs(g_inventoryItems) do
		local particle = {}
		particle.placementId = 0
		particle.objectName = ""
		particle.objectGlid = 0
		particle.glid = v.GLID
		particle.name = v.name
		particle.x = ""
		particle.y = ""
		particle.z = ""
		particle.invType = (v.is_gift == true) and GameGlobals.IT_GIFT or GameGlobals.IT_NORMAL
		particle.invSubType = v.inventory_sub_type
		particle.unsortedIndex = #g_particles + 1
		table.insert(g_particles, particle)
	end
	sortObjects(g_SortingBy)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function updateParticleList()
	Log("updateParticleList")

	g_particles = {}
	updateActiveParticles()
	updateInventoryParticles()
	sortObjects(g_SortingBy)
end

function updateActiveParticles()
	local doCount = KEP_GetDynamicObjectCount()
	for i = 0, (doCount - 1) do
		local particle = {}
		particle.placementId = KEP_GetDynamicObjectPlacementIdByPos(i)
		local partGlid, partName = KEP_GetDynamicObjectParticleSystem( particle.placementId )
		if partGlid ~= nil and partGlid > 0 then
			local name, description, canPlayMovie, isAttachableObject, isInteractive, isLocal, isDerivable, assetId, friendId, customTexture, globalId, invType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(particle.placementId)
			local x, y, z = KEP_DynamicObjectGetPosition(particle.placementId)

			particle.objectName = name
			particle.objectGlid = globalId
			particle.glid = partGlid
			particle.name = partName
			particle.x = round(x, 3)
			particle.y = round(y, 3)
			particle.z = round(z, 3)
			particle.invType = GameGlobals.IT_GIFT
			particle.unsortedIndex = #g_particles + 1
			table.insert(g_particles, particle)
		end
	end
	getParticleInfo()
end

function updateInventoryParticles()
	Log("updateInventoryParticles")

	KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
	getPlayerInventory(WF.PARTICLE_INVENTORY, nil, nil, "building", nil, "Particle Effects")
end

function getParticleInfo()
	local glids = ""

	for i,v in ipairs(g_particles) do
		if v.glid > 0 then
			glids = glids .. v.glid .. ","
		end
	end

	if glids == "" then
		return
	end
	glids = string.sub(glids, 1, -2)

	makeWebCall(GameGlobals.WEB_SITE_PREFIX.."kgp/instantBuy.aspx?operation=info&items=" .. glids, WF.PARTICLE_BROWSER_INFO)
end

function getParticleByPlacementId(placementId)
	for i,v in ipairs(g_particles) do
		if v.placementId == placementId then
			return v
		end
	end
end

function updateSelection()
	local count = KEP_GetNumSelected()
	for i,h in ipairs(g_listhandles) do
		ListBox_ClearSelection(h)
	end
	for i = 0,count-1 do
		local type, id = KEP_GetSelection(i)
		local particle = getParticleByPlacementId(id)

		local result = -1
		if particle ~= nil then
			result = ListBox_FindItem(gHandles["lbxName"], particle.unsortedIndex)
		end
		if result ~= -1 then
			g_ignoreSelection = true
			ListBox_SelectItem(gHandles["lbxName"], result)
			selectObject(result+1, true)
		end
	end
	syncAllLists( g_listhandles, gHandles["lbxName"] )
end

function selectObject(index, nohighlight)
	local particle = getObjectByUnsortedIndex(index)
	local showProperties = false
	if particle ~= nil then
		if particle.placementId > 0 then
			showProperties = true
		end

		if particle.placementId ~= 0 and nohighlight ~= true then
			KEP_SelectObject(ObjectType.DYNAMIC, particle.placementId)
			g_selectCount = g_selectCount + 1
		end
	end

	Control_SetEnabled(gHandles["btnAttachParticle"], not showProperties and index > 0)
	Control_SetEnabled(gHandles["btnDetachParticle"], showProperties)
	Control_SetEnabled(gHandles["btnProperties"], showProperties)
end

function getSelectedObject()
	local index = ListBox_GetSelectedItemData(gHandles["lbxName"])

	Log("getSelectedObject index = "..index);

	return getObjectByUnsortedIndex(index)
end

function getObjectByUnsortedIndex(index)

	Log("getObjectByUnsortedIndex index = "..index.."#g_particles = "..#g_particles)

	for i,v in ipairs(g_particles) do

		Log("v.unsortedIndex = "..v.unsortedIndex)

		if v.unsortedIndex == index then
			return v
		end
	end
end

function populateList()
	for i,v in ipairs(g_listhandles) do
		ListBox_RemoveAllItems(v)
	end

	if queryString == nil then
		queryString = ""
	end
	for i = 1, #g_particles do
		local particle = g_particles[i]
		local start = string.find(string.lower(particle.name), string.lower(g_queryString))
		if start ~= nil then
			local displayName = string.sub(particle.name, 0, start - 1) .. "<font color=FFFF0000>" .. string.sub(particle.name, start, start + string.len(g_queryString) - 1) .. "</font>" .. string.sub(particle.name, start + string.len(g_queryString), -1)
			ListBox_AddItem(gHandles["lbxNameDummy"], "", particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxName"], (displayName == "") and particle.glid or displayName, particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxPIDDummy"], "", particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxPID"], (particle.placementId == 0) and "" or particle.placementId, particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxObjectDummy"], "", particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxObject"], particle.objectName, particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxXPosDummy"], "", particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxXPos"], particle.x, particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxYPosDummy"], "", particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxYPos"], particle.y, particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxZPosDummy"], "", particle.unsortedIndex)
			ListBox_AddItem(gHandles["lbxZPos"], particle.z, particle.unsortedIndex)
		end
	end

	--Update the column header images
	local btnHandles = {gHandles["btnName"], gHandles["btnPID"], gHandles["btnObject"], gHandles["btnXPos"], gHandles["btnYPos"], gHandles["btnZPos"],}
	local sBlankTexture = "button9_LGray.tga"
	for i,h in pairs(btnHandles) do
		Element_AddTexture( Control_GetDisplayElement(h,   "normalDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h, "mouseOverDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,  "focusedDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h,  "pressedDisplay"), gDialogHandle, sBlankTexture)
		Element_AddTexture( Control_GetDisplayElement(h, "disabledDisplay"), gDialogHandle, sBlankTexture)
	end

	local sArrowTexture = "button9_LGrayUpArrow.tga"
	if g_bSortAscending then
		sArrowTexture = "button9_LGrayDownArrow.tga"
	end

	local btnHandle = btnHandles[g_SortingBy]
	if btnHandle ~= nil then
		Element_AddTexture( Control_GetDisplayElement(btnHandle,   "normalDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle, "mouseOverDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,  "focusedDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle,  "pressedDisplay"), gDialogHandle, sArrowTexture)
		Element_AddTexture( Control_GetDisplayElement(btnHandle, "disabledDisplay"), gDialogHandle, sArrowTexture)
	end

	updateSelection()
end

function sortObjects(sortType)
	g_SortingBy = sortType
	table.sort(g_particles, g_SortFuncs[sortType])
	populateList()
end

function OpenListBoxPlaceableObj()
	local particle = getSelectedObject()
	if (particle == nil) then return end
	local objId = particle.placementId
	local objType = ObjectType.DYNAMIC
	if objId ~= nil and objId ~= 0 then
		Log("OpenListBoxPlaceableObj: "..ObjectToString(objId, objType).." - Opening PlaceableObj Menu...")
		MenuOpen("PlaceableObj.xml")
		local e = KEP_EventCreate( DYNAMIC_OBJECT_INFO_EVENT )
		KEP_EventEncodeNumber(e, objId)
		KEP_EventEncodeNumber(e, objType)
		KEP_EventQueue(e)
	end
end

function lbxName_OnListBoxItemDblClick(listboxHandle, selectedIndex, selectedText)
	Log("NameDoubleClicked")
	OpenListBoxPlaceableObj()
end

function lbxObject_OnListBoxItemDblClick(listboxHandle, selectedIndex, selectedText)
	Log("ObjectDoubleClicked")
	OpenListBoxPlaceableObj()
end

function btnProperties_OnButtonClicked( buttonHandle )
	Log("ObjectPropertiesClicked")
	OpenListBoxPlaceableObj()
end

function btnNew_OnButtonClicked( buttonHandle )
	Log("NewClicked")
	MenuOpen("ParticleEditor.xml")
	MenuCloseThis()
end

-- DRF - Send ObjectsUpdatedEvent (allows ObjectBrowser to update list)
function sendObjectsUpdatedEvent()
	KEP_EventCreateAndQueue("ObjectsUpdatedEvent")
end

function btnAttachParticle_OnButtonClicked( buttonHandle )
	Log("AttachClicked")
	local particle = getSelectedObject()
	if particle ~= nil and particle.placementId == 0 then
		local selCount = KEP_GetNumSelected( )
		if selCount == 0 then
			LogError("AttachClicked: No Objects Selected")
			return
		end
		local type, id = KEP_GetSelection( 0 )

		Log("AttachClicked: "..particle.glid.." "..type.." "..id)

		KEP_UseInventoryItemOnTarget( particle.glid, type, id, particle.invSubType  )
	end
	updateParticleList()
end

function btnDetachParticle_OnButtonClicked( buttonHandle )
	Log("DetachClicked")
	local particle = getSelectedObject()

	if particle == nil then
		Log("particle == nil")
	else
		Log("particle.placmentId = "..particle.placementId)
	end

	if particle ~= nil and particle.placementId > 0 then

		Log("DetachClicked: "..particle.placementId)

		KEP_RemoveParticleSystemFromDynamicObject(particle.placementId)
		local e = KEP_EventCreate( "ControlDynamicObjectEvent")
		KEP_EventSetObjectId(e, particle.placementId)
		KEP_EventSetFilter(e, KEP.PARTICLE_STOP)
		KEP_EventEncodeNumber(e, 1) -- argument count
		KEP_EventEncodeNumber(e, 0) -- no particle
		KEP_EventAddToServer(e)
		KEP_EventQueue( e)

		-- DRF - Added
		sendObjectsUpdatedEvent()
	end
	updateParticleList()
end

function btnName_OnButtonClicked( buttonHandle )
	if Sort_ParticleName == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjects(Sort_ParticleName)
end

function btnPID_OnButtonClicked( buttonHandle )
	if Sort_ObjectPID == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjects(Sort_ObjectPID)
end

function btnObject_OnButtonClicked( buttonHandle )
	if Sort_ObjectName == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjects(Sort_ObjectName)
end

function btnXPos_OnButtonClicked( buttonHandle )
	if Sort_PositionX == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjects(Sort_PositionX)
end

function btnYPos_OnButtonClicked( buttonHandle )
	if Sort_PositionY == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjects(Sort_PositionY)
end

function btnZPos_OnButtonClicked( buttonHandle )
	if Sort_PositionZ == g_SortingBy then
		g_bSortAscending = not g_bSortAscending
	end
	sortObjects(Sort_PositionZ)
end

function sortByParticleName(itemA, itemB)
	local aName = string.lower(itemA.name)
	local bName = string.lower(itemB.name)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	if aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	else
		if itemA.objectName == "" and itemB.objectName == "" then
			return false
		end
		return sortByObjectName(itemA, itemB)
	end
end

function sortByObjectPID(itemA, itemB)
	if itemA.placementId == 0 and itemB.placementId ~= 0 then
		return false
	elseif itemB.placementId == 0 and itemA.placementId ~= 0 then
		return true
	elseif itemA.placementId == 0 and itemB.placementId == 0 then
		return sortByParticleName(itemA, itemB)
	else
		if g_bSortAscending then
			return itemA.placementId < itemB.placementId
		else
			return itemA.placementId > itemB.placementId
		end
	end
end

function sortByObjectName(itemA, itemB)
	local aName = string.lower(itemA.objectName)
	local bName = string.lower(itemB.objectName)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	elseif aName ~= bName then
		if g_bSortAscending then
			return aName < bName
		else
			return aName > bName
		end
	else
		if itemA.unsortedIndex == itemB.unsortedIndex then --lua compares the same script sometimes
			return false
		end
		return sortByObjectPID(itemA, itemB)
	end
end

function sortByObjectX(itemA, itemB)
	local aName = string.lower(itemA.x)
	local bName = string.lower(itemB.x)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	if aName ~= bName then
		if g_bSortAscending then
			return tonumber(aName) < tonumber(bName)
		else
			return tonumber(aName) > tonumber(bName)
		end
	else
		return sortByParticleName(itemA, itemB)
	end
end

function sortByObjectY(itemA, itemB)
	local aName = string.lower(itemA.y)
	local bName = string.lower(itemB.y)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	if aName ~= bName then
		if g_bSortAscending then
			return tonumber(aName) < tonumber(bName)
		else
			return tonumber(aName) > tonumber(bName)
		end
	else
		return sortByParticleName(itemA, itemB)
	end
end

function sortByObjectZ(itemA, itemB)
	local aName = string.lower(itemA.z)
	local bName = string.lower(itemB.z)
	if aName == "" and bName ~= "" then
		return false
	elseif bName == "" and aName ~= "" then
		return true
	end
	if aName ~= bName then
		if g_bSortAscending then
			return tonumber(aName) < tonumber(bName)
		else
			return tonumber(aName) > tonumber(bName)
		end
	else
		return sortByParticleName(itemA, itemB)
	end
end

g_SortFuncs =
{
	sortByParticleName,
	sortByObjectPID,
	sortByObjectName,
	sortByObjectX,
	sortByObjectY,
	sortByObjectZ
}

function lbxName_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if g_ignoreSelection == false and ((bMouseDown == 1 or bFromKeyboard == 1) or g_bSyncAllLists == 1) then
		g_selectCount = 1
		local index = ListBox_GetSelectedItemData(gHandles["lbxName"])
		selectObject(index)

		if g_bSyncAllLists == 0 then
			syncAllLists( g_listhandles, listBoxHandle )
		end
	end
	g_ignoreSelection = false
end

function menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
	if g_ignoreSelection == false and ((bMouseDown == 1 or bFromKeyboard == 1) or g_bSyncAllLists == 1) then
		if g_bSyncAllLists == 0 then
			syncAllLists( g_listhandles, listBoxHandle )
		end
	end
end

function lbxNameDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxPIDDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxPID_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxObjectDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxObject_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxXPosDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxXPos_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxYPosDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxYPos_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxZPosDummy_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function lbxZPos_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
end

function scrollBarChanged(listBoxHandle)
	if g_bSyncAllLists == 0 then
		syncAllScrollBars( g_listhandles, listBoxHandle )
	end
end

function lbxNameDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxName_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxPIDDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxPID_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxObjectDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxObject_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxXPosDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxXPos_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxYPosDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxYPos_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxZPosDummy_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function lbxZPos_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end

function editSearch_OnEditBoxChange(editBoxHandle, text)
	g_queryString = text
	populateList()
end

function ParseInfoResults( browserString )
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(browserString, "<ReturnCode>(%d+)</ReturnCode>")

	if result == "0" then
		s, e, numRec = string.find(browserString, "<NumberRecords>(%d+)</NumberRecords>", e)
		numRec = tonumber(numRec)
		if numRec > 0 then
			for i = 1, numRec do
				s, e, globalID = string.find(browserString, "<global_id>(%d+)</global_id>", e)

				globalID = tonumber(globalID)
				s, e, name = string.find(browserString, "<name>(.-)</name>", e)
				s, e, inventoryType = string.find(browserString, "<inventory_type>(%d+)</inventory_type>", e)
				inventoryType = tonumber(inventoryType)
				if inventoryType ~= GameGlobals.IT_BOTH then
					g_inventoryType = inventoryType
				end

				s, e, price = string.find(browserString, "<market_cost>(%d+)</market_cost>", e) --needs to be changed to WebPrice
				price = tonumber(price)

				s, e, active = string.find(browserString, "<item_active>(%d+)</item_active>", e)
				active = tonumber(active)

				for i,v in ipairs(g_particles) do
					if globalID == v.glid then
						v.name = name
					end
				end
			end
		end
	end

	sortObjects(g_SortingBy)

end
