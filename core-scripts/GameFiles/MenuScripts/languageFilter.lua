--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

explitives={"fuck",
	"shit",
	"bastard",
	"bitch",
	"cunt",
	"twat",
	"fag",
	"cocksuck",
	"titties",
	"nigg",
	"gook",
	"wop",
	"kike",
	"chink",
	"dago",
	"wetback",
	"faggot"
}

function langFilter( message )

	local temp = ""
	local start = 1
	local pos = 1 --position within string of dirty word (if any)
	local lmsg = string.lower(message) --lower case message

	for i=1,#explitives do

		while pos ~= nil do
			--find a dirty word and save its indices in the string
			start, pos, temp = string.find(lmsg, explitives[i], pos)
			if start ~= nil then
				--Grab everything before and after the dirty word
				local prefix = string.sub(message, 1, start-1)
				local suffix = string.sub(message, pos+1, -1)
				message = prefix.."!#@&%"..suffix
				lmsg = string.lower(message)
			end

		end
		--print(message)
		-- start again from beginning
		pos = 1
	end

	return message
end
