--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------


dofile("MenuHelper.lua")

P2P_INFO_EVENT = "p2pInfoEvent"
P2P_ANIM_REQUEST = "P2pAnimRequestEvent"
REQUEST_ACCESS_PASS_INFO_EVENT = "RequestAccessPassInfoEvent"
RESPONSE_ACCESS_PASS_INFO_EVENT = "ResponseAccessPassInfoEvent"
PLAYER_INFO_EVENT = "PlayerInfoEvent"

PLAYER_INFO_USERID_REQUEST = 1
PLAYER_INFO_USERID_RESPONSE = 2

--Maximum distance to start a p2p animtion
P2P_MAX_DIST = 16

g_pageIndex = 1
g_nextPage = 1
g_previousPage = 1
g_maxPages = nil
g_emoteListHandle = nil

g_targetUserID = nil
g_targetName = nil
g_animIndex = nil

g_iHasAccessPass = 0
g_iHasVIPPass = 0

ACCESS_PASS_ID = 1
VIP_PASS_ID = 2

g_passRequirements = {}
g_passRequirements[1] = 0
g_passRequirements[2] = 0
g_passRequirements[3] = 0
g_passRequirements[4] = 0
g_passRequirements[5] = 0
g_passRequirements[6] = 0
g_passRequirements[7] = 0
g_passRequirements[8] = 0
g_passRequirements[9] = 0

--Statics for different animations
SHAKE_HANDS = 1
HIGH_FIVE = 2
HUG = 3
KISS = 4
SEATED_EMBRACE = 9
CHIT_CHAT = 6
STANDING_EMBRACE = 7
SLOW_DANCE = 8
MAKEOUT = 5

--Request sent stuffs
g_reqSent = false
g_time = 0
REQUESTED_TIME = 3

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "p2pInfoEventHandler", P2P_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassInfoHandler", RESPONSE_ACCESS_PASS_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "playerInfoHandler", PLAYER_INFO_EVENT, KEP.MED_PRIO )
end

function responseAccessPassInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iHasAccessPass = KEP_EventDecodeNumber(event)
	g_iHasVIPPass = KEP_EventDecodeNumber(event)

end

function p2pInfoEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_targetUserID = KEP_EventDecodeNumber(event)
	g_targetName = KEP_EventDecodeString(event)
	Log("p2pInfoEventHandler: targetUserId="..g_targetUserID.." targetUserName="..g_targetName)

	--Send to see if user is >18
	if g_targetName  then
		web_address = GameGlobals.WEB_SITE_PREFIX..LOGIN_INFO_SUFFIX..g_targetName
		makeWebCallCached(web_address, WF.BROWSE_PLACES_USER_PROFILE)
	end

	local sh = Dialog_GetStatic(gDialogHandle, "lblPartner")
	if (sh ~= 0)  and (g_targetName ~= nil) then
		Static_SetText(sh, g_targetName)
	end

	return KEP.EPR_CONSUMED
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	--Checks age when called, if age has already been checked the compare
	if filter == WF.BROWSE_PLACES_USER_PROFILE then
		local returnData = KEP_EventDecodeString( event )
		local s, e, age = string.find(returnData, "<age>(%d+)</age>")
		age = tonumber(age)
		if age == nil then age = 0 end

		if g_mature == nil then
			g_mature = (age >= 18)
		else
			local mature2 = (age >=18)

			if (g_mature and mature2) or (g_mature == false and mature2 == false) then
				Control_SetEnabled(g_makeoutButton, true)
				Static_SetText( g_makeoutButton, "Makeout" )
			else
				Control_SetEnabled(g_makeoutButton, false)
				Static_SetText( g_makeoutButton, "")
			end
		end
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	g_nextPage = Dialog_GetButton(gDialogHandle,"btnNextPage")
	g_previousPage = Dialog_GetButton(gDialogHandle,"btnPrevPage")
	g_emoteListHandle = Dialog_GetListBox(gDialogHandle, "lstEmotes" )
	g_makeoutButton = Dialog_GetButton(gDialogHandle, "btnEmote9")

	Control_SetVisible(g_previousPage,false)
	Control_SetEnabled(g_makeoutButton, false)
	Static_SetText( g_makeoutButton, "" )
	
	Control_SetEnabled(gHandles["btnSendRequest"], false)

	KEP_EventCreateAndQueue( REQUEST_ACCESS_PASS_INFO_EVENT )

	--Send to see if user >18
	local username = KEP_GetLoginName()
	makeWebCallCached(GameGlobals.WEB_SITE_PREFIX..LOGIN_INFO_SUFFIX..username, WF.BROWSE_PLACES_USER_PROFILE)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	if g_reqSent then 	
		if g_time < REQUESTED_TIME then
			if g_time <= 0 then
				Static_SetText(gHandles["btnSendRequest"], "Request Sent!")
			end
			g_time = g_time +fElapsedTime
		else 
			Static_SetText(gHandles["btnSendRequest"], "Send Request")
			g_reqSent = false 
			g_time = 0
			Control_SetEnabled(gHandles["btnSendRequest"], false)
			Static_SetText(gHandles["lblSelectedAnim"], "")
			g_animIndex = nil
		end
	end
	
end

function btnSendRequest_OnButtonClicked( buttonHandle )
	
	distance = KEP_GetRuntimeObjDistance(g_targetUserID)
	Log("SendRequestClicked: targetUserId="..g_targetUserID.." targetDistance="..distance)

	if distance <= P2P_MAX_DIST then

		if g_animIndex ~= nil and g_animIndex > 0 then
			if(g_passRequirements[g_animIndex] == ACCESS_PASS_ID and g_iHasAccessPass == 0) then
				createLinkMessage( "You do not have the passes required to do that.", "Purchase Access Pass", GameGlobals.WEB_SITE_PREFIX_KANEVA..BUY_ACCESS_PASS_SUFFIX)
				return false
			elseif (g_passRequirements[g_animIndex] == VIP_PASS_ID and g_iHasVIPPass == 0 and g_iHasAccessPass == 0) then
				createLinkMessage( "You do not have the passes required to do that.", "Purchase VIP Pass", GameGlobals.WEB_SITE_PREFIX_KANEVA..BUY_ACCESS_PASS_SUFFIX)
				return false
			end

			local e = KEP_EventCreate( P2P_ANIM_REQUEST )
			if e ~= 0 then
				KEP_EventSetFilter( e, g_targetUserID )
				KEP_EventSetObjectId( e, g_animIndex )
				KEP_EventAddToServer( e )
				KEP_EventQueue( e )
				g_reqSent = true 
				
			end
		else
			KEP_MessageBox("Please select an animation.")
		end
	else
		KEP_MessageBox("You must be closer to perform the animation.")
	end
end

--Click on this button to go the previous emote page
function btnPrevPage_OnButtonClicked(buttonHandle)
	g_pageIndex=g_pageIndex-1
	if g_pageIndex<=1 then
		g_pageIndex=1
		Control_SetVisible(g_nextPage,true)
		Control_SetVisible(g_previousPage,false)
	else
		Control_SetVisible(g_nextPage,true)
		Control_SetVisible(g_previousPage,true)
	end
end

function btnNextPage_OnButtonClicked(buttonHandle)
	g_pageIndex=g_pageIndex+1
	if (g_pageIndex>g_maxPages) then
		g_maxPages=g_pageIndex
		CreateBlankPage(g_pageIndex)
	end
end

function btnEmote1_OnButtonClicked(buttonHandle)
	g_animIndex = SHAKE_HANDS
	local sh = Button_GetStatic(buttonHandle)
	animName = Static_GetText(sh)
	local shAnimName = Dialog_GetStatic(gDialogHandle, "lblSelectedAnim")
	Static_SetText(shAnimName, animName)
	Control_SetEnabled(gHandles["btnSendRequest"], true)
end

function btnEmote2_OnButtonClicked(buttonHandle)
	g_animIndex = HIGH_FIVE
	local sh = Button_GetStatic(buttonHandle)
	animName = Static_GetText(sh)
	local shAnimName = Dialog_GetStatic(gDialogHandle, "lblSelectedAnim")
	Static_SetText(shAnimName, animName)
	Control_SetEnabled(gHandles["btnSendRequest"], true)
end

function btnEmote3_OnButtonClicked(buttonHandle)
	g_animIndex = HUG
	local sh = Button_GetStatic(buttonHandle)
	animName = Static_GetText(sh)
	local shAnimName = Dialog_GetStatic(gDialogHandle, "lblSelectedAnim")
	Static_SetText(shAnimName, animName)
	Control_SetEnabled(gHandles["btnSendRequest"], true)
end

function btnEmote4_OnButtonClicked(buttonHandle)
	g_animIndex = KISS
	local sh = Button_GetStatic(buttonHandle)
	animName = Static_GetText(sh)
	local shAnimName = Dialog_GetStatic(gDialogHandle, "lblSelectedAnim")
	Static_SetText(shAnimName, animName)
	Control_SetEnabled(gHandles["btnSendRequest"], true)
end

function btnEmote5_OnButtonClicked(buttonHandle)
	g_animIndex = SEATED_EMBRACE
	local sh = Button_GetStatic(buttonHandle)
	animName = Static_GetText(sh)
	local shAnimName = Dialog_GetStatic(gDialogHandle, "lblSelectedAnim")
	Static_SetText(shAnimName, animName)
	Control_SetEnabled(gHandles["btnSendRequest"], true)
end

function btnEmote6_OnButtonClicked(buttonHandle)
	g_animIndex = CHIT_CHAT
	local sh = Button_GetStatic(buttonHandle)
	animName = Static_GetText(sh)
	local shAnimName = Dialog_GetStatic(gDialogHandle, "lblSelectedAnim")
	Static_SetText(shAnimName, animName)
	Control_SetEnabled(gHandles["btnSendRequest"], true)
end

function btnEmote7_OnButtonClicked(buttonHandle)
	g_animIndex = STANDING_EMBRACE
	local sh = Button_GetStatic(buttonHandle)
	animName = Static_GetText(sh)
	local shAnimName = Dialog_GetStatic(gDialogHandle, "lblSelectedAnim")
	Static_SetText(shAnimName, animName)
	Control_SetEnabled(gHandles["btnSendRequest"], true)
end

function btnEmote8_OnButtonClicked(buttonHandle)
	g_animIndex = SLOW_DANCE
	local sh = Button_GetStatic(buttonHandle)
	animName = Static_GetText(sh)
	local shAnimName = Dialog_GetStatic(gDialogHandle, "lblSelectedAnim")
	Static_SetText(shAnimName, animName)
	Control_SetEnabled(gHandles["btnSendRequest"], true)
end

function btnEmote9_OnButtonClicked(buttonHandle)
	g_animIndex = MAKEOUT
	local sh = Button_GetStatic(buttonHandle)
	animName = Static_GetText(sh)
	local shAnimName = Dialog_GetStatic(gDialogHandle, "lblSelectedAnim")
	Static_SetText(shAnimName, animName)
	Control_SetEnabled(gHandles["btnSendRequest"], true)
end
