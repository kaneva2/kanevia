--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

g_userID = 0
g_userName = 0

gAlreadyBlocked = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "PlayerUserIdEventHandler", "PlayerUserIdEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function PlayerUserIdEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_userID = KEP_EventDecodeNumber( event )
	g_userName = KEP_EventDecodeString( event )
 	local sh = Dialog_GetStatic( gDialogHandle, "cbBlock" )
	if sh ~= 0 then
		Static_SetText( sh, "Block "..g_userName )
	end

	Log("PlayerUserIdEventHandler: Getting Block Data For userId="..g_userID)
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..GET_USER_BLOCK_DATA_SUFFIX..g_userID, WF.BLOCK)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local s, e = 1
	local resultCode = ""
	if filter == WF.BLOCK then
		blockData = KEP_EventDecodeString( event )
		s, e, resultCode = string.find(blockData, "<ReturnCode>(.-)</ReturnCode>")
		if resultCode == "-1" then
			--no previous records
		end
		parseBlockData(blockData)
		return KEP.EPR_CONSUMED
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function blockUser()
	if g_userID > 0 then
		Log("blockUser: userId="..g_userID)
		makeWebCall( GameGlobals.WEB_SITE_PREFIX..BLOCK_USER_SUFFIX..g_userID, WF.BLOCK)
		kickUser()
  	end
end

function kickUser()
	if KEP_IsOwnerOrModerator() and (CheckBox_GetChecked(gHandles["cbBlock"]) == 1) then
		local player = KEP_GetPlayer( )
		local playerId = GetSet_GetNumber( player, MovementIds.NETWORKDEFINEDID )
		local event = KEP_EventCreate( "TextEvent" )
		local reason = "Blocked"

		Log("kickUser: playerId="..playerId.." userName="..g_userName)
		KEP_EventEncodeNumber( event, playerId )
		KEP_EventEncodeString( event, g_userName.." "..reason ) -- who to kick
		KEP_EventEncodeNumber( event, ChatType.Talk )
		KEP_EventSetFilter( event, CMD.KICK )
		KEP_EventAddToServer( event )
		KEP_EventQueue( event )
	end
end

function reportUser()
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA..WEB_FEEDBACK_SUFFIX )
end

function parseBlockData( blockData )
	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local data = ""         --data segment
	s, strPos, result = string.find(blockData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		s, strPos, data = string.find(blockData, "<comm_blocked>([0-9]+)</comm_blocked>")
		if data == "1" then
			gAlreadyBlocked = true
		else
		    gAlreadyBlocked = false
		end
		CheckBox_SetChecked( gHandles["cbBlock"], gAlreadyBlocked )
	end
end

function btnOK_OnButtonClicked(buttonHandle)
	local reportChecked = CheckBox_GetChecked(gHandles["cbReport"])
	if reportChecked == 1 then
		reportUser()
	end
	
	local blockChecked = CheckBox_GetChecked(gHandles["cbBlock"])
	if blockChecked == 1 then
		blockUser()	
	end

	MenuCloseThis()
end
