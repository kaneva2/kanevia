--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

TOTAL_NAV_BUTTONS = 25
gMapNavParts = {}
gMapNavButtons = {}

gMapNavVisible = false

---------------------------------------------------------------------
-- Functions
---------------------------------------------------------------------
function mapNavInit( gWorldMapHandle )
	gMapNavParts.top = Dialog_GetControl( gWorldMapHandle, "popTop" )
	gMapNavParts.mid = Dialog_GetControl( gWorldMapHandle, "popMid" )
	gMapNavParts.bottom = Dialog_GetControl( gWorldMapHandle, "popBottom" )
	gMapNavParts.left = Dialog_GetControl( gWorldMapHandle, "popLeftArrow" )
	gMapNavParts.right = Dialog_GetControl( gWorldMapHandle, "popRightArrow" )

	for i = 1, TOTAL_NAV_BUTTONS do
		gMapNavButtons[ i ] = Dialog_GetControl( gWorldMapHandle, "btnSub" .. tostring( i ) )
	end
	gMapNavButtons.title = Dialog_GetControl( gWorldMapHandle, "btnTitle" )

	mapNavShowAll( false )
end

function mapNavShowAll( show )
	for k, v in pairs( gMapNavParts ) do
		Control_SetVisible( v, show )
	end
	for i, v in ipairs( gMapNavButtons ) do
		Control_SetVisible( v, show )
	end
	Control_SetVisible( gMapNavButtons.title, show )
	gMapNavVisible = show
end

function mapNavShow( data, mapX, mapY, mapW, mapH, x, y )
	mapNavShowAll( false )
	local numLocs = # data  - 1

	for index, value in ipairs( data ) do
		if index > 1 then
			sh = Button_GetStatic( gMapNavButtons[ index - 1 ] )
			Control_SetVisible( gMapNavButtons[ index - 1 ], true )
			Static_SetText( sh, value[ 2 ] )
		else
			sh = Button_GetStatic( gMapNavButtons.title )
			Control_SetVisible( gMapNavButtons.title, true )
			Static_SetText( sh, value[ 2 ] )
		end
	end

	Control_SetVisible( gMapNavParts.top, true )
	Control_SetVisible( gMapNavParts.bottom, true )

	local midHeight = numLocs * Control_GetHeight( gMapNavButtons[ 1 ] )
	local topY = 0
	local leftX = x - 50
	if x > mapW / 2 then
		leftX = x - 200
	end
	if midHeight > 0 then
		topY = y - 50
		Control_SetSize( gMapNavParts.mid, 250, midHeight )
		Control_SetVisible( gMapNavParts.mid, true )
		Control_SetLocation( gMapNavParts.mid, leftX, topY + 50 )
		Control_SetLocation( gMapNavParts.bottom, leftX, topY + 50 + midHeight )
	else
		topY = y - 25
		Control_SetLocation( gMapNavParts.mid, -999, -999 )
	end
	Control_SetLocation( gMapNavParts.top, leftX, topY )
	Control_SetLocation( gMapNavButtons.title, leftX + 15, topY + 15 )
	Control_SetLocation( gMapNavParts.bottom, leftX, topY + 50 + midHeight )

	for i, v in ipairs( gMapNavButtons ) do
		Control_SetLocation( v, leftX + 15, topY + 60 + ( i - 2 ) * 12 )
	end
	gMapNavVisible = true
end

function mapNavContainsPoint( x, y )
	for k, v in pairs( gMapNavParts ) do
		if Control_ContainsPoint( v, x, y ) == 1 then
			--KEP_Log( "\n true...." )
			return true
		end
	end
	return false
end

function mapNavUpdate( elapsedTime )
end
