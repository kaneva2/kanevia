--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("MenuLocation.lua")
dofile("MenuStyles.lua")

g_btnMinEnabled = true -- optional maximize button support
g_btnMaxEnabled = false -- optional maximize button support

-- DRF - Do Not Flatten While Moving
g_moving = false
g_movingTimeSec = 0
g_doFlattenWindow = false

MIN_X = 100
MIN_Y = 25

BTN_X = 5
BTN_Y = -14

BTN_MIN_X = BTN_X
BTN_MIN_Y = BTN_Y

BTN_INC_TRANS_X = BTN_X + 20
BTN_INC_TRANS_Y = BTN_Y 

BTN_DEC_TRANS_X = BTN_X + 40
BTN_DEC_TRANS_Y = BTN_Y

BTN_MAX_X = BTN_X + 60
BTN_MAX_Y = BTN_Y

chatcolor         = {}
chatcolor.talk    = "FFFFFFFF"
chatcolor.clan    = "FF00FF00"
chatcolor.group   = "FF00FF00"
chatcolor.whisper = "C0C0C0C0"
chatcolor.shout   = "FFFF0000"
chatcolor.system  = "FFFFFF00"
chatcolor.private = "FF00FFFF"
chatcolor.emote   = "FF3DF500"

g_chatlines = {}
g_chattypes = {}

g_displaylines = {}
g_displaylinecount = 0

-- Chat history globals
g_chatlinecnt = 10
g_chatlinelen = 58
g_chatbuffersize = 200
g_chatscrollpos = 0

-- Transparency Levels
g_translevels = {}
g_translevels[0]  = tonumber("08000000", 16)
g_translevels[1]  = tonumber("19000000", 16)
g_translevels[2]  = tonumber("33000000", 16)
g_translevels[3]  = tonumber("4C000000", 16)
g_translevels[4]  = tonumber("66000000", 16)
g_translevels[5]  = tonumber("7F000000", 16)
g_translevels[6]  = tonumber("98000000", 16)
g_translevels[7]  = tonumber("B2000000", 16)
g_translevels[8]  = tonumber("CB000000", 16)
g_translevels[9]  = tonumber("E5000000", 16)
g_translevels[10] = tonumber("FF000000", 16)
MIN_ALPHA = g_translevels[0]
DEFAULT_ALPHA = g_translevels[2]

-- Window States
WIN_STATE_MIN = 0
WIN_STATE_NORMAL = 1
WIN_STATE_MAX = 2

-- State Defaults
DEF_TRANS_LEVEL = 2
DEF_WIN_STATE = WIN_STATE_MIN
DEF_H_SIZE = 210
DEF_V_SIZE = 80
DEF_IS_FLAT = 0
DEF_IS_STICKY = 0

-- State Configuration (saved to wokconfig.xml)
g_transLevel = DEF_TRANS_LEVEL
g_winState = DEF_WIN_STATE
g_hSize = DEF_H_SIZE
g_vSize = DEF_V_SIZE
g_isFlat = DEF_IS_FLAT
g_isSticky = DEF_IS_STICKY

-- Positioning globals
g_x_resizeoffset = 0
g_y_resizeoffset = 0
g_sizing = false

g_cmdBuffer = {}
g_bufferPos = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "textHandler", "RenderTextEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "attribEventHandler", "AttribEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "renderLocaleTextHandler", "RenderLocaleTextEvent", KEP.HIGH_PRIO )
end

function renderLocaleTextHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local stringID = KEP_EventDecodeNumber( event )
	if ( stringID == 120 ) then
		local chatType = KEP_EventDecodeNumber( event )
		local msg = KEP_EventDecodeString( event )
		--string is empty so fill with message
		createLinkMessage( "You do not have the passes required to do that.", "Purchase Access Pass", GameGlobals.WEB_SITE_PREFIX_KANEVA..BUY_ACCESS_PASS_SUFFIX)
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- ChatMenu Replaces DevConsole 
	if MenuIsOpen("ChatMenu.xml")  then
		MenuCloseThis()
		return
	end

	-- Get All Controls
	g_btnMin = Dialog_GetButton(dialogHandle, "btnMinimize")
	g_btnMax = Dialog_GetButton(dialogHandle, "btnMaximize")
	g_lblChat = Dialog_GetStatic(dialogHandle, "lblChat")
	g_imgMainFrame = Dialog_GetImage(dialogHandle, "imgMainFrame")
	g_imgResize = Dialog_GetImage(dialogHandle, "imgResize")
	g_btnIncTrans = Dialog_GetButton(dialogHandle, "btnIncTrans")
	g_btnDecTrans = Dialog_GetButton(dialogHandle, "btnDecTrans")
	g_lstChatHist = Dialog_GetListBox(dialogHandle, "lstChatHist")
	g_lstLeftScrollBar = Dialog_GetListBox(dialogHandle, "lstLeftScrollBar")

	-- DRF - Added Mouse Passthrough Controls
	Control_SetPassthrough( g_lstChatHist, 1 )
	Control_SetPassthrough( g_imgMainFrame, 1 )
	Control_SetPassthrough( g_lblChat, 1 )

	Control_SetLocation( g_btnMin, BTN_MIN_X, BTN_MIN_Y)
	Control_SetLocation( g_btnMax, BTN_MAX_X, BTN_MAX_Y)
	Control_SetLocation( g_btnIncTrans, BTN_INC_TRANS_X, BTN_INC_TRANS_Y)
	Control_SetLocation( g_btnDecTrans, BTN_DEC_TRANS_X, BTN_DEC_TRANS_Y)

	for i = 1, g_chatbuffersize do
		g_chatlines[i] = " "
		g_chattypes[i] = 0
	end
	width  = Dialog_GetWidth( dialogHandle )

	ListBox_SetHTMLEnabled( g_lstChatHist, true)
	ListBox_SetHTMLEnabled( g_lstLeftScrollBar, true)

	-- Restyle Left Scroll Bar
	local sbh = ListBox_GetScrollBar(g_lstLeftScrollBar)
	if sbh ~= 0 then
		local eh = ScrollBar_GetTrackDisplayElement(sbh)
		if eh ~= 0 then
			--Blue body
			Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
			Element_SetCoords(eh, 185, 55, 186, 56)
		end
		eh = ScrollBar_GetButtonDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
			Element_SetCoords(eh, 395, 572, 409, 589)
		end
		eh = ScrollBar_GetUpArrowDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
			Element_SetCoords(eh, 395, 547, 409, 561)
		end
		eh = ScrollBar_GetDownArrowDisplayElement(sbh)
		if eh ~= 0 then
			Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
			Element_SetCoords(eh, 395, 642, 409, 656)
		end
	end
	Control_SetLocation(g_lstLeftScrollBar, BTN_X - 10, BTN_Y + 20)

	Static_SetText( g_lblChat, "")

	genDisplayLines()

	-- Load Window Configuration
	LoadWindowInfo()
end

--reconstruct the chat buffer with the history from the config file
function regenChatBuf( strHist )
	-- previous position of tag found
	local lastPos = 1
	-- beginning and end of found string
	local start = 1
	local stop = 1
	local chatLine = ""
	local tag = ""
	local type = ""

	for i = 1, g_chatbuffersize do
		lastPos = stop  -- remember where we were in string

		--get location of tag that delimits new line of text and chat type
		start, stop, type = string.find(strHist, "<kbr (%d+)>", lastPos)

		--check to make sure we found the next tag in the string
		if start == nil or stop == nil then
			return 0
		else
			g_chattypes[i] = tonumber(type)
			--take text from between end of previous tag and just before start of next tag
			chatLine = string.sub(strHist, lastPos, start-1)
			g_chatlines[i] = chatLine
			stop = stop + 1
		end
	end
	genDisplayLines()
end

function SaveWindowInfo()
	Log("SaveWindowInfo: "
		.." size="..g_hSize.."x"..g_vSize
		.." transLevel="..g_transLevel
		.." isSticky="..g_isSticky
		.." winState="..g_winState
	) 

	saveLocation(gDialogHandle, "DevCon")

	local config = KEP_ConfigOpenWOK()
	if config == nil then return end
	KEP_ConfigSetNumber( config, "DevConHSize", g_hSize )
	KEP_ConfigSetNumber( config, "DevConVSize", g_vSize )
	KEP_ConfigSetNumber( config, "DevConTransLevel", g_transLevel )
	KEP_ConfigSetNumber( config, "DevConWinState", g_winState)
	KEP_ConfigSetNumber( config, "DevConIsSticky", g_isSticky )
	KEP_ConfigSave( config )
end

function LoadWindowInfo()
	
	loadLocation(gDialogHandle, "DevCon")

	local hSize = DEF_H_SIZE
	local vSize = DEF_V_SIZE
	local transLevel = DEF_TRANS_LEVEL
	local isSticky = DEF_IS_STICKY
	local winState = DEF_WIN_STATE
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		ok, hSize = KEP_ConfigGetNumber( config, "DevConHSize", hsize )
		ok, vSize = KEP_ConfigGetNumber( config, "DevConVSize", vsize )
		ok, transLevel = KEP_ConfigGetNumber(config, "DevConTransLevel", translevel)
		ok, isSticky = KEP_ConfigGetNumber(config, "DevConIsSticky", isSticky)
		ok, winState = KEP_ConfigGetNumber( config, "DevConWinState", winState)
		history = ""
		regenChatBuf( history )
	end
	g_hSize = hSize
	g_vSize = vSize
	setChatPosition()
	g_isSticky = isSticky
	TransSetVisible()
	g_winState = winState
	doUnMinimize()
	if (g_isSticky == 1) then
		revealWindow()
	else
		flattenWindow()
	end
	if (winState == WIN_STATE_MIN) then
		doMinimize()
	end
	g_transLevel = transLevel
	updateTransparency()

	Log("LoadWindowInfo: "
		.." size="..g_hSize.."x"..g_vSize
		.." transLevel="..g_transLevel
		.." isSticky="..g_isSticky
		.." winState="..g_winState
	) 
end

function Dialog_OnDestroy(dialogHandle)
	SaveWindowInfo()
	Helper_Dialog_OnDestroy( dialogHandle )
end

--Concate all strings in buffer into onestring to save.
function genChatHistStr()
	local chatString = ""
	local typeString = ""
	for i = 1, g_chatbuffersize do
		if g_chatlines[ i ] ~= "" and g_chattypes[i] ~= nil then
			chatString = chatString..g_chatlines[i].."<kbr "..g_chattypes[i]..">"
		end
	end
	return chatString
end

function breaktoken(message)
	local spcpos = string.find(message, " ")
	local brackpos = string.find(message, "<")
	local tokenpos
	local work
	local restmessage
	local tag = false
	if message == nil then
		return nil, nil
	end
	if spcpos == nil and brackpos == nil then
		return message, nil
	elseif spcpos ~= nil and brackpos == nil then
		tokenpos = spcpos
		tag = false
	elseif spcpos == nil and brackpos ~= nil then
		if brackpos == 1 then
			tag = true
		else
			tag = false
		end
		tokenpos = brackpos
	else
		if brackpos == 1 then
			tag = true
		else
			tag = false
		end
		tokenpos = math.min(spcpos, brackpos)
	end
	if tag == true then
		local closepos = string.find(message, ">")
		if closepos ~= nil then
			-- Hey this is an HTML tag... the entire tag is one token
			word = string.sub(message, 1, closepos)
			restmessage = string.sub(message, closepos + 1, -1)
		else
			-- Some loose bracket was in the string
			word = string.sub(message, 1, tokenpos)
			restmessage = string.sub(message, tokenpos + 1, -1)
		end
	else
		word = string.sub(message, 1, tokenpos)
		restmessage = string.sub(message, tokenpos + 1, -1)
	end

	return word, restmessage
end

function getColorText(type)
	if type == ChatType.Clan then
		colortext = "<font color=" .. chatcolor.clan ..">"
	elseif type == ChatType.Group then
		colortext = "<font color=" .. chatcolor.group ..">"
	elseif type == ChatType.System then
		colortext = "<font color=" .. chatcolor.system ..">"
	elseif type == ChatType.Whisper then
		colortext = "<font color=" .. chatcolor.whisper ..">"
	elseif type == ChatType.Shout then
		colortext = "<font color=" .. chatcolor.shout ..">"
	elseif type == ChatType.Private then
		colortext = "<font color=" .. chatcolor.private ..">"
	elseif type == ChatType.Emote then
		colortext = "<font color=" .. chatcolor.emote ..">"
	else
		colortext = "<font color=" .. chatcolor.talk ..">"
	end
	return colortext
end

function getNameColorText(type)
	return "<font color=\"#FFA0A0FF\">"
end

function getText( text, colorNameText, colortext, type )
	local newtext = text
	local endIndex
	if ( type == ChatType.Talk or type == ChatType.Whisper or type == ChatType.Shout or type == ChatType.Private ) then
		if ( string.find(text,">") ~= nil ) then
			endIndex = string.find(text,">")
			newtext = colorNameText .. string.sub(text,1, endIndex) .. "</font>" .. colortext .. string.sub(text,endIndex + 1) .. " </font>" .. "<br>"
		end
	elseif ( type == ChatType.Emote ) then
		endIndex = string.find(text," ")
		newtext = colorNameText .. string.sub(text,1, endIndex) .. "</font>" .. colortext .. string.sub(text,endIndex + 1) .. " </font>" .. "<br>"
	else
		newtext = colortext .. text .. " </font>" .. "<br>"
	end
	return newtext
end

function genDisplayLines()
	g_displaylines = {}
	g_displaylinecount = 0
	local chatwidth = Control_GetWidth(Static_GetControl(g_lblChat))
	g_chatlinecnt = (g_vSize / 15.5) - .5
	-- Loop through all of the messages in our chat history-
	for i = 1, g_chatbuffersize do
		-- sanity check
		if g_chatlines[ i ] ~= "" and g_chattypes[ i ] ~= nil then
			-- now identify the color of the text.
			local colortext = getColorText(g_chattypes[i])
			local colorNameText = getNameColorText(g_chattypes[i])
			local currentlineonthisline = ""
			-- Now the magic
			if Static_GetStringWidth(g_lblChat, g_chatlines[i]) < chatwidth then
				-- This message will fit on one line
				local text =  getText( g_chatlines[i], colorNameText, colortext, g_chattypes[ i ] )
				table.insert( g_displaylines, 1, text)
				g_displaylinecount = g_displaylinecount + 1
			else
				-- This is not going to fit on one line.
				local wholemessage = getText( g_chatlines[i], colorNameText, colortext, g_chattypes[ i ] )
				local lineover = false
				while wholemessage ~= nil do
					lineover = false
					local finishedline = false
					-- loop through entire message
					word, wholemessage = breaktoken(wholemessage)
					currentlineonthisline = ""
					while finishedline == false do
						-- loop through a line
						if word == nil then
							word = ""
						end
						if wholemessage == nil then
							wholemessage = ""
						end
						if Static_GetStringWidth(g_lblChat, currentlineonthisline .. word) > chatwidth then
							-- need to wrap
							if string.len( currentlineonthisline ) == 0 then
								-- one loooong line, truncate it
								currentlineonthisline = word
								lineover = true
								finishedline = true
							else
								lineover = true
								finishedline = true
								wholemessage = word .. wholemessage
							end
						else
							currentlineonthisline = currentlineonthisline .. word
							lineover = false
							finishedline = false
							word, wholemessage = breaktoken(wholemessage)
						end
						if wholemessage == nil then
							finishedline = true
						end
					end
					if lineover == true then
						local text =  colortext .. currentlineonthisline .. "</font>" .. "<br>"
						table.insert(g_displaylines, 1, text)
						g_displaylinecount = g_displaylinecount + 1
					else
						if word ~= nil then
							local text =  colortext .. currentlineonthisline .. word .. "</font>" .. "<br>"
							table.insert(g_displaylines, 1, text)
							g_displaylinecount = g_displaylinecount + 1
						end
					end
				end
			end
		end
	end

	-- disable scrollbar synchronization
	lstLeftScrollBar_OnScrollBarChanged = nil
	lstChatHist_OnScrollBarChanged = nil

	-- Ok we just computed the lines as they should be displayed
	ListBox_RemoveAllItems(g_lstChatHist)
	ListBox_RemoveAllItems(g_lstLeftScrollBar)
	for i = 1, g_chatbuffersize do
		local line = g_displaylines[g_chatbuffersize + 1 - i]
		ListBox_AddItem(g_lstChatHist, line, 0)
		ListBox_AddItem(g_lstLeftScrollBar, "", 0)
	end
	ListBox_SelectItem(g_lstChatHist, g_chatbuffersize)
	ListBox_SelectItem(g_lstLeftScrollBar, g_chatbuffersize)

	-- reenable scrollbar synchronization
	lstLeftScrollBar_OnScrollBarChanged = OnScrollBarChanged
	lstChatHist_OnScrollBarChanged = OnScrollBarChanged
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	-- 4096 Magic Number = Do Text Render
	if filter == 4096 then
		local msg   = KEP_EventDecodeString( event )
		local red   = KEP_EventDecodeNumber( event )
		local green = KEP_EventDecodeNumber( event )
		local blue  = KEP_EventDecodeNumber( event )
		local type  = KEP_EventDecodeNumber( event )
		table.insert( g_chatlines, msg)
		table.remove( g_chatlines, 1)
		table.insert( g_chattypes, type)
		table.remove( g_chattypes, 1)
		genDisplayLines()
		return KEP.EPR_OK --KEP.EPR_CONSUMED
	end
end

function textHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local msg   = KEP_EventDecodeString( event )
	local type  = KEP_EventDecodeNumber( event )
	local lmsg = string.lower(msg)

	if type == ChatType.System then
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, msg)
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
		return 
	end 

	-- DRF - No Chat In Dev Console
	local hasArrow = string.find(msg, " > ")
	if hasArrow ~= nil then return end

	table.insert( g_chatlines, msg)
	table.remove( g_chatlines, 1)
	table.insert( g_chattypes, type)
	table.remove( g_chattypes, 1)
	genDisplayLines()
	return KEP.EPR_CONSUMED
end

function doMinimize()
	Dialog_SetCaptionEnabled( gDialogHandle, 1 )
	Dialog_SetCaptionHeight( gDialogHandle, 18 )
	
	Control_SetVisible(g_lblChat, false)
	
	Control_SetLocation(g_btnMin, BTN_MIN_X, BTN_MIN_Y)
	Control_SetVisible(g_btnMin, true and g_btnMinEnabled)
	
	Control_SetLocation(g_btnMax, BTN_MAX_X, BTN_MAX_Y)
	Control_SetVisible(g_btnMax, true and g_btnMaxEnabled)
	
	TransSetVisible()

	Control_SetVisible(g_imgResize, false)
	
	Control_SetVisible(g_lstChatHist, false)
	Control_SetEnabled(g_lstChatHist, false)
	
	Control_SetVisible(g_lstLeftScrollBar, false)
	Control_SetEnabled(g_lstLeftScrollBar, false)

	-- Minimize Main Frame
	Dialog_SetSize(gDialogHandle, MIN_X, MIN_Y)
	Control_SetSize(g_imgMainFrame, MIN_X, MIN_Y)
	Control_SetVisible(g_imgMainFrame, true )

	g_winState = WIN_STATE_MIN
end

function doUnMinimize()
	Dialog_SetCaptionEnabled( gDialogHandle, 1 )
	Dialog_SetCaptionHeight( gDialogHandle, 18 )

	Control_SetVisible(g_btnMin, true and g_btnMinEnabled)
	Control_SetVisible(g_btnMax, true and g_btnMaxEnabled)

	TransSetVisible()

	Control_SetVisible(g_lblChat, true)
	Control_SetVisible(g_imgResize, true)

	Control_SetVisible(g_lstChatHist, true)
	Control_SetEnabled(g_lstChatHist, true)

	Control_SetVisible(g_lstLeftScrollBar, true)
	Control_SetEnabled(g_lstLeftScrollBar, true)
	
	-- Restore Main Frame
	setChatPosition()

	g_winState = WIN_STATE_NORMAL
end

function imgResize_OnLButtonDown(dialogHandle, x, y)
	g_x_resizeoffset = x - Control_GetLocationX(Image_GetControl(g_imgResize))
	g_y_resizeoffset = y - Control_GetLocationY(Image_GetControl(g_imgResize))
	g_sizing = true
	updateTransparency()
	ListBox_RemoveAllItems(g_lstChatHist)
end

function Dialog_OnMouseMove( dialogHandle, x, y )

	-- Flag Moving
	local mouseOver = (Control_ContainsPoint( g_imgMainFrame, x, y ) == 1)
	if (mouseOver) then
		SetMoving()
	end

	if g_sizing then
		if Dialog_IsLMouseDown(dialogHandle) ~= 0 then
			if x + 7 < Dialog_GetScreenWidth(dialogHandle) and y + 22 < Dialog_GetScreenHeight(dialogHandle) then
				local hsize = x - g_x_resizeoffset - 7
				local vsize = y - g_y_resizeoffset - 1
				g_chatscrollpos = 0
				if hsize > 200 then
					g_hSize = hsize
				else
					g_hSize = 200
				end
				if vsize > 75 then
					g_vSize = vsize
				else
					g_vSize = 75
				end
				setChatPosition()
			end
		else
			g_sizing = false
			updateTransparency()
			setChatPosition()
			genDisplayLines()
		end
	elseif (g_winState == WIN_STATE_NORMAL) then
		if (mouseOver or (g_isSticky == 1)) then
			revealWindow()
		else
			g_doFlattenWindow = true
		end
	end
end

function Dialog_OnLButtonUp( dialogHandle, x, y )
	Dialog_OnMoved()
end

function setChatPosition()
	local tempwidth
	-- Keep size from being bigger than the whole screen
	if g_vSize > Dialog_GetScreenHeight(gDialogHandle) - 100 then
		g_vSize = Dialog_GetScreenHeight(gDialogHandle) - 100
	end
	if g_hSize > Dialog_GetScreenWidth(gDialogHandle) - 30 then
		g_hSize = Dialog_GetScreenWidth(gDialogHandle) - 30
	end

	if g_vSize < MIN_Y then
		g_vSize = MIN_Y
	end
	if g_hSize < MIN_X then
		g_hSize = MIN_X
	end

	-- ChatHist Area
	Control_SetSize(Static_GetControl(g_lblChat), g_hSize - 55, g_vSize)
	Control_SetSize(ListBox_GetControl(g_lstChatHist), g_hSize + 33, g_vSize + 7)
	Control_SetSize(ListBox_GetControl(g_lstLeftScrollBar), 26, g_vSize + 7)

	-- OuterFrame
	Control_SetSize(g_imgMainFrame, g_hSize + 40, g_vSize + 40)

	-- Stretch dialog horizontally to allow titlebar to work properly. Dialog height remains minimal for future "mouse-click-thru" enhancement.
	Dialog_SetSize(gDialogHandle, g_hSize + 40, 40)

	-- Resize and minimize controls
	Control_SetLocation(Image_GetControl(g_imgResize), g_hSize + 15, g_vSize + 1)
end

function btnMinimize_OnButtonClicked()
	
	-- Toggle Window Between Minimized And Normal State
	if (g_winState ~= WIN_STATE_MIN) then
		Log("MinimizeClicked")
		doMinimize()
	else
		Log("UnMinimizeClicked")
		doUnMinimize()
	end
end

function btnMaximize_OnButtonClicked()
	Log("MaximizeClicked - TODO")
end

function SetMoving()
	g_moving = true
	g_movingTimeSec = 0
end

g_xPosWas = 0
g_yPosWas = 0
function Dialog_OnRender(dialogHandle, timeSec)
	
	-- DRF - Dialog Moved?
	local xPos = Dialog_GetLocationX(gDialogHandle)
	local yPos = Dialog_GetLocationY(gDialogHandle)
	if (xPos ~= g_xPosWas or yPos ~= g_yPosWas) then
		g_xPosWas = xPos
		g_yPosWas = yPos
		g_doFlattenWindow = false
	end

	-- Movement Ends After 1 Second
	if (g_moving) then
		g_movingTimeSec = g_movingTimeSec + timeSec
		if (g_movingTimeSec > 0.5) then
			g_moving = false
		end
	end	

	-- Only Flatten If Not Moving
	if (g_doFlattenWindow and not g_moving) then
		g_doFlattenWindow = false
		flattenWindow()
	end
end

function Dialog_OnMoved()

	-- send dialog to bottom if not active
	if (g_isSticky == 0) then
		Dialog_SendToBack(gDialogHandle)
	end

	-- keep dialog within client borders
	if Dialog_GetLocationX(gDialogHandle) < 9 then
		Dialog_SetLocation(gDialogHandle, 9, Dialog_GetLocationY(gDialogHandle))
	end
	if Dialog_GetLocationY(gDialogHandle) < 62 then
		Dialog_SetLocation(gDialogHandle, Dialog_GetLocationX(gDialogHandle), 62)
	end
	if Dialog_GetLocationX(gDialogHandle) > Dialog_GetScreenWidth(gDialogHandle) - g_hSize - 49 then
		Dialog_SetLocation(gDialogHandle, Dialog_GetScreenWidth(gDialogHandle) - g_hSize - 50, Dialog_GetLocationY(gDialogHandle))
	end
	if Dialog_GetLocationY(gDialogHandle) > Dialog_GetScreenHeight(gDialogHandle) - g_vSize - 49 then
		Dialog_SetLocation(gDialogHandle, Dialog_GetLocationX(gDialogHandle), Dialog_GetScreenHeight(gDialogHandle) - g_vSize - 50)
	end
end

function btnIncTrans_OnButtonClicked( buttonHandle )
	
	-- Increment Transparency Level
	g_transLevel = g_transLevel + 1
	if g_transLevel>10 then
		g_transLevel = 0
	end
	Log("IncTransClicked: level="..g_transLevel)

	updateTransparency()
end

function btnDecTrans_OnButtonClicked( buttonHandle )
	
	-- Decrement Transparency Level
	g_transLevel = g_transLevel - 1
	if g_transLevel<0 then
		g_transLevel = 10
	end
	Log("DecTransClicked: level="..g_transLevel)

	updateTransparency()
end

function flattenWindow()
	
	-- Already Flat ?
	if g_isFlat == 1 then return end
	g_isFlat = 1
	Log("flattenWindow")
		
	Control_SetVisible( g_btnMin, false and g_btnMinEnabled)
	Control_SetVisible( g_btnMax, false and g_btnMaxEnabled)

	Control_SetVisible( g_lblChat, true ) 
	Control_SetVisible( g_imgMainFrame, false )
	Control_SetVisible( g_imgResize, false )

	Control_SetVisible( g_lstChatHist, true )
	Control_SetEnabled( g_lstChatHist, true )

	Control_SetVisible( g_lstLeftScrollBar, false )
	Control_SetEnabled( g_lstLeftScrollBar, false )
end

function revealWindow()

	-- Already Revealed ?
	if g_isFlat == 0 then return end
	g_isFlat = 0
	Log("revealWindow")

	Control_SetVisible( g_btnMin, true and g_btnMinEnabled)
	Control_SetVisible( g_btnMax, true and g_btnMaxEnabled)

	Control_SetVisible( g_lblChat, true )
	Control_SetVisible( g_imgMainFrame, true )
	Control_SetVisible( g_imgResize, true )

	Control_SetVisible( g_lstChatHist, true )
	Control_SetEnabled( g_lstChatHist, true )

	Control_SetVisible( g_lstLeftScrollBar, true )
	Control_SetEnabled( g_lstLeftScrollBar, true )
end

function OnScrollBarChanged( scrollbar )
	local trackPos = ScrollBar_GetTrackPos( scrollbar )
	if trackPos~=g_chatscrollpos then
		g_chatscrollpos = trackPos
		if scrollbar == g_lstChatHist then
			-- if chat list changed, sync left-handed scroll bar
			ScrollBar_SetTrackPos( ListBox_GetScrollBar( g_lstLeftScrollBar ), trackPos )
		else
			-- otherwise sync chat list with left-handed scroll bar
			ScrollBar_SetTrackPos( ListBox_GetScrollBar( g_lstChatHist ), trackPos )
		end
	end
end

lstLeftScrollBar_OnScrollBarChanged = OnScrollBarChanged
lstChatHist_OnScrollBarChanged = OnScrollBarChanged

function TransSetVisible()
	Control_SetVisible( g_btnIncTrans, g_isSticky )
	Control_SetVisible( g_btnDecTrans, g_isSticky )
end

function updateTransparency()
	local mytemptex = Element_GetTextureColor(Image_GetDisplayElement(g_imgMainFrame))
	local opacity
	if (g_isSticky == 1) then
		-- if sticky: use user transparency
		opacity = g_translevels[g_transLevel]
	elseif g_sizing then
		-- if resizing or moving: use default transparency
		opacity = DEFAULT_ALPHA
	else
		-- if idle: use full transparency
		opacity = MIN_ALPHA
	end
	BlendColor_SetColorRaw( mytemptex, DXUT_STATE_NORMAL, opacity )
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown, bCtrlDown, bAltDown)

	-- Toggle Sticky (apostrophe)
	if key==192 and not bCtrlDown then
		g_isSticky = 1 - g_isSticky
		Log("StickyClicked: isSticky="..g_isSticky)

		TransSetVisible()
		updateTransparency()
		revealWindow()
	end
end
