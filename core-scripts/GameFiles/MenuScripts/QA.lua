--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
--	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- This Menu Only Allowed In DevMode!
	local devMode = ToBool(KEP_DevMode())
	if (not devMode) then
		MenuCloseThis()
	end

	-- Display Statics As HTML
	Static_SetUseHTML(gHandles["stcSingleLine"], 1)
	Static_SetUseHTML(gHandles["stcMultiLine"], 1)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function ebSingleLine_OnEditBoxChange(handle, text)
	Static_SetText(gHandles["stcSingleLine"], text)
end

function ebMultiLine_OnEditBoxChange(handle, text)
	Static_SetText(gHandles["stcMultiLine"], text)
end
