--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\DevToolsConstants.lua")

TEXTURE_PICKER_EVENT = "TextureSelectedEvent"
TEXTURE_LOAD_EVENT = "TextureLoadEvent"
TEXTURE_COPY_EVENT = "CopyTextureEvent"
TEXTURE_PASTE_EVENT = "RequestPasteEvent"
COLOR_START_EVENT = "ColorStartEvent"
TEXTURE_COLOR_TARGET = 12
TEXTURE_PICKER_COLOR_FILTER = 2
COLOR_PICKER = "ColorPicker.xml"
COLOR_PICKER_EVENT = "ColorSelectedEvent"
DXUT_STATE_NORMAL = 0
FILE_BROWSER_CLOSED_EVENT = "FileBrowserClosedEvent"
SOURCE_ASSET_DOWNLOAD_EVENT = "3DAppAssetDownloadCompleteEvent"

gHandles = {}
gColor = {}
gSenderFilter = nil
gTextureName = nil
gPreviewElement = nil
gColorElement = nil
gColorPicker = nil
gL = nil
gT = nil
gR = nil
gB = nil
gSliceLeft = nil
gSliceTop = nil
gSliceRight = nil
gSliceBottom = nil
gElementTag = ""
gShowingFullSize = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "textureLoadEventHandler", TEXTURE_LOAD_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "colorPickerEventHandler", COLOR_PICKER_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "FileBrowserClosedHandler", FILE_BROWSER_CLOSED_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "AppAssetDownloadHandler", SOURCE_ASSET_DOWNLOAD_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( TEXTURE_LOAD_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( TEXTURE_COPY_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( TEXTURE_PASTE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( COLOR_PICKER_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( FILE_BROWSER_CLOSED_EVENT, KEP.MED_PRIO )
end

function colorPickerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == TEXTURE_PICKER_COLOR_FILTER then
		gColor.a = KEP_EventDecodeNumber(event)
		gColor.r = KEP_EventDecodeNumber(event)
		gColor.g = KEP_EventDecodeNumber(event)
		gColor.b = KEP_EventDecodeNumber(event)
		local textureColor = Element_GetTextureColor(gColorElement)
		BlendColor_SetColor(textureColor, DXUT_STATE_NORMAL, gColor)
		local previewColor = Element_GetTextureColor(gPreviewElement)
		BlendColor_SetColor(previewColor, DXUT_STATE_NORMAL, gColor)
	end
end

function checkTexture()
	gPreviewElement = Image_GetDisplayElement(gHandles["imgPreview"])
	local height = Element_GetTextureHeight(gPreviewElement, gDialogHandle)
	local width = Element_GetTextureWidth(gPreviewElement, gDialogHandle)
	local logHeight = math.log10(height)/math.log10(2)
	local logWidth = math.log10(width)/math.log10(2)
	if logHeight ~= math.ceil(logHeight) or logWidth ~= math.ceil(logWidth) then
		success, dialogHandle = KEP_MessageBox( "The dimensions of this texture are not a power of two and may behave unexpectedly.", "Warning")
	end
end

function UpdatePreview()
	local imgPreview = gHandles["imgPreview"]
	local imgPreviewX = Control_GetLocationX(imgPreview)
	local imgPreviewY = Control_GetLocationY(imgPreview)
	local imgPreviewW = Control_GetWidth(imgPreview)
	local imgPreviewH = Control_GetHeight(imgPreview)

	local scaleX = imgPreviewW / math.abs(gR - gL)
	local scaleY = imgPreviewH / math.abs(gB - gT)
	if gShowingFullSize then
		local textureW = Element_GetTextureWidth(gPreviewElement, gDialogHandle)
		local textureH = Element_GetTextureHeight(gPreviewElement, gDialogHandle)
		scaleX = imgPreviewW / textureW
		scaleY = imgPreviewH / textureH
		-- TODO: Maintain the aspect ratio by resizing imgPreview so that the largest
		-- dimension is set to imgPreview's original size, and the other dimension
		-- is scaled to match the aspect ratio.

		local cL, cR, cT, cB = gL, gR, gT, gB
		if gR == -1 then
			cR = textureW
		end
		if gB == -1 then
			cB = textureH
		end

		Control_SetVisible(gHandles["imgCoordLeft"], true)
		Control_SetVisible(gHandles["imgCoordRight"], true)
		Control_SetVisible(gHandles["imgCoordTop"], true)
		Control_SetVisible(gHandles["imgCoordBottom"], true)
		Control_SetLocationX( gHandles["imgCoordLeft"], cL * scaleX + imgPreviewX -1 )
		Control_SetLocationX( gHandles["imgCoordRight"], cR * scaleX + imgPreviewX -1 )
		Control_SetLocationY( gHandles["imgCoordTop"], cT * scaleY + imgPreviewY -1 )
		Control_SetLocationY( gHandles["imgCoordBottom"], cB * scaleY + imgPreviewY -1 )
		Control_SetLocationX( gHandles["imgSliceLeft"], (cL + gSliceLeft) * scaleX + imgPreviewX -1 )
		Control_SetLocationX( gHandles["imgSliceRight"], (cR - gSliceRight) * scaleX + imgPreviewX -1 )
		Control_SetLocationY( gHandles["imgSliceTop"], (cT + gSliceTop) * scaleY + imgPreviewY -1 )
		Control_SetLocationY( gHandles["imgSliceBottom"], (cB - gSliceBottom) * scaleY + imgPreviewY -1 )
		Element_SetCoords(gPreviewElement, 0, 0, -1, -1)
	else
		Control_SetVisible(gHandles["imgCoordLeft"], false)
		Control_SetVisible(gHandles["imgCoordRight"], false)
		Control_SetVisible(gHandles["imgCoordTop"], false)
		Control_SetVisible(gHandles["imgCoordBottom"], false)
		Control_SetLocationX( gHandles["imgSliceLeft"], gSliceLeft * scaleX + imgPreviewX -1 )
		Control_SetLocationX( gHandles["imgSliceRight"], imgPreviewW - gSliceRight * scaleX + imgPreviewX -1 )
		Control_SetLocationY( gHandles["imgSliceTop"], gSliceTop * scaleY + imgPreviewY -1 )
		Control_SetLocationY( gHandles["imgSliceBottom"], imgPreviewH - gSliceBottom * scaleY + imgPreviewY -1 )
		Element_SetCoords(gPreviewElement, gL, gT, gR, gB)
	end
end

function textureLoadEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	gSenderFilter = filter
	gTextureName = KEP_EventDecodeString(event)
	Static_SetText(gHandles["stcName"], gTextureName)
	gL = KEP_EventDecodeNumber(event)
	gT = KEP_EventDecodeNumber(event)
	gR = KEP_EventDecodeNumber(event)
	gB = KEP_EventDecodeNumber(event)

	gPreviewElement = Image_GetDisplayElement(gHandles["imgPreview"])
	Element_AddTexture(gPreviewElement, gDialogHandle, gTextureName)
	Element_SetCoords(gPreviewElement, gL,gT,gR,gB)
	checkTexture()

	gColor.a = KEP_EventDecodeNumber( event )
	gColor.r = KEP_EventDecodeNumber( event )
	gColor.g = KEP_EventDecodeNumber( event )
	gColor.b = KEP_EventDecodeNumber( event )

	gSliceLeft   = KEP_EventDecodeNumber(event)
	gSliceTop    = KEP_EventDecodeNumber(event)
	gSliceRight  = KEP_EventDecodeNumber(event)
	gSliceBottom = KEP_EventDecodeNumber(event)

	local elementTag = KEP_EventDecodeString(event)
	if elementTag ~= "" then
		gElementTag = elementTag
		Static_SetText(gHandles["stcElementName"], gElementTag)
	end

	gColorElement = Image_GetDisplayElement( gHandles["imgColor"] )
	local textureColor = Element_GetTextureColor(gColorElement)
	BlendColor_SetColor(textureColor, DXUT_STATE_NORMAL, gColor)

	local previewColor = Element_GetTextureColor(gPreviewElement)
	BlendColor_SetColor(previewColor, DXUT_STATE_NORMAL, gColor)

	EditBox_SetText( gHandles["edtBoxLeft"], gL, false )
	EditBox_SetText( gHandles["edtBoxTop"], gT, false )
	EditBox_SetText( gHandles["edtBoxRight"], gR, false )
	EditBox_SetText( gHandles["edtBoxBottom"], gB, false )

	EditBox_SetText( gHandles["edtBoxSliceLeft"], gSliceLeft, false )
	EditBox_SetText( gHandles["edtBoxSliceTop"], gSliceTop, false )
	EditBox_SetText( gHandles["edtBoxSliceRight"], gSliceRight, false )
	EditBox_SetText( gHandles["edtBoxSliceBottom"], gSliceBottom, false )

	-- Try to select the texture in the listbox
	local listBoxTextures = gHandles["listBoxTextures"]
	local count = ListBox_GetSize(listBoxTextures)
	for i=0,count-1 do
		if ListBox_GetItemText(listBoxTextures, i) == gTextureName then
			ListBox_SelectItem(listBoxTextures, i)
			break
		end
	end

	boxZoomPreview_OnCheckBoxChanged(gHandles["boxZoomPreview"], CheckBox_GetChecked(gHandles["boxZoomPreview"]))
end

function btnColor_OnButtonClicked(buttonHandle)
	gColorPicker = MenuOpenModal(COLOR_PICKER)

	--Pass up the color data to the colorpicker
	local ev = KEP_EventCreate( COLOR_START_EVENT)
	KEP_EventSetFilter(ev, TEXTURE_PICKER_COLOR_FILTER)
	KEP_EventEncodeNumber(ev, gColor.a)
	KEP_EventEncodeNumber(ev, gColor.r)
	KEP_EventEncodeNumber(ev, gColor.g)
	KEP_EventEncodeNumber(ev, gColor.b)
	KEP_EventEncodeNumber(ev, TEXTURE_COLOR_TARGET)
	KEP_EventQueue( ev)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	populateTextures()
end

function populateTextures()
	local listBoxTextures = gHandles["listBoxTextures"]
	ListBox_RemoveAllItems(listBoxTextures)
	local l_t_finalTexList = {}
	local l_t_names = {}
	local num = Dialog_GetNumTextures()
	for i=1,num do
		texEntry = {}
		local name = Dialog_GetTextureByIndex( i - 1 )
		texEntry["name"] = name
		texEntry["path"] = "default"
		texEntry["type"] = codeFromExtension(name)
		texEntry["modified"] = 0
		l_t_finalTexList[name] = texEntry
		table.insert(l_t_names, name)
	end

	local l_t_codes = {DevToolsUpload.MENU_DDS, DevToolsUpload.MENU_TGA, DevToolsUpload.MENU_BMP, DevToolsUpload.MENU_PNG, DevToolsUpload.MENU_JPG, DevToolsUpload.MENU_JPEG}
	for codeIndex,code in ipairs(l_t_codes) do
		local localDevCount = KEP_Count3DAppUnpublishedAssets(code)
		local unPubIndex
		for unPubIndex=1, localDevCount do
			entry = {}
			local name, fullpath, modified = KEP_Get3DAppUnpublishedAsset(code, unPubIndex)
			entry["name"] = name .. extensionFromCode(code)
			entry["path"] = fullpath
			entry["type"] = type
			entry["modified"] = modified
			if l_t_finalTexList[entry["name"]] == nil then
				table.insert(l_t_names, entry["name"])
			end
			l_t_finalTexList[entry["name"]] = entry
		end
	end

	table.sort(l_t_names, function(a,b) return string.lower(a) < string.lower(b) end)
	for i, name in ipairs(l_t_names) do
		ListBox_AddItem( listBoxTextures, name, i)
	end
end

function Dialog_OnDestroy(dialogHandle)
	DestroyMenu(gColorPicker)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnPickTexture_OnButtonClicked( buttonHandle )
	checkTexture()
	local ev = KEP_EventCreate( TEXTURE_PICKER_EVENT)
	KEP_EventSetFilter(ev, gSenderFilter)
	KEP_EventEncodeString(ev, gTextureName)
	KEP_EventEncodeNumber(ev, gL)
	KEP_EventEncodeNumber(ev, gT)
	KEP_EventEncodeNumber(ev, gR)
	KEP_EventEncodeNumber(ev, gB)
	KEP_EventEncodeNumber(ev, gColor.a)
	KEP_EventEncodeNumber(ev, gColor.r)
	KEP_EventEncodeNumber(ev, gColor.g)
	KEP_EventEncodeNumber(ev, gColor.b)
	KEP_EventEncodeNumber(ev, gSliceLeft)
	KEP_EventEncodeNumber(ev, gSliceTop)
	KEP_EventEncodeNumber(ev, gSliceRight)
	KEP_EventEncodeNumber(ev, gSliceBottom)
	KEP_EventQueue( ev)
	
	MenuCloseThis()
end

function edtBoxLeft_OnEditBoxChange(editboxHandle, editboxText)
	gL = tonumber(editboxText) or 0
	UpdatePreview()
end

function edtBoxTop_OnEditBoxChange(editboxHandle, editboxText)
	gT = tonumber(editboxText) or 0
	UpdatePreview()
end

function edtBoxRight_OnEditBoxChange(editboxHandle, editboxText)
	gR = tonumber(editboxText) or 0
	UpdatePreview()
end

function edtBoxBottom_OnEditBoxChange(editboxHandle, editboxText)
	gB = tonumber(editboxText) or 0
	UpdatePreview()
end

function edtBoxSliceLeft_OnEditBoxChange(editboxHandle, editboxText)
	gSliceLeft = tonumber(editboxText) or 0
	UpdatePreview()
end

function edtBoxSliceRight_OnEditBoxChange(editboxHandle, editboxText)
	gSliceRight = tonumber(editboxText) or 0
	UpdatePreview()
end

function edtBoxSliceTop_OnEditBoxChange(editboxHandle, editboxText)
	gSliceTop = tonumber(editboxText) or 0
	UpdatePreview()
end

function edtBoxSliceBottom_OnEditBoxChange(editboxHandle, editboxText)
	gSliceBottom = tonumber(editboxText) or 0
	UpdatePreview()
end

function listBoxTextures_OnListBoxSelection(handle, x, y)
	gTextureName = ListBox_GetSelectedItemText(handle)
	Static_SetText(gHandles["stcName"], gTextureName)
	Element_AddTexture(gPreviewElement, gDialogHandle, gTextureName)
	UpdatePreview()
end

function boxZoomPreview_OnCheckBoxChanged(chkHandle, checked)
	gShowingFullSize = (checked == 1)
	UpdatePreview()
end

function btnCopyTexture_OnButtonClicked(buttonHandle)
	if not (gTextureName and gL and gT and gR and gB) then return end
	local ev = KEP_EventCreate( TEXTURE_COPY_EVENT)
	KEP_EventEncodeString(ev, gTextureName)
	KEP_EventEncodeNumber(ev, gL)
	KEP_EventEncodeNumber(ev, gT)
	KEP_EventEncodeNumber(ev, gR)
	KEP_EventEncodeNumber(ev, gB)
	KEP_EventEncodeNumber(ev, gColor.a)
	KEP_EventEncodeNumber(ev, gColor.r)
	KEP_EventEncodeNumber(ev, gColor.g)
	KEP_EventEncodeNumber(ev, gColor.b)
	KEP_EventEncodeNumber(ev, gSliceLeft)
	KEP_EventEncodeNumber(ev, gSliceTop)
	KEP_EventEncodeNumber(ev, gSliceRight)
	KEP_EventEncodeNumber(ev, gSliceBottom)
	KEP_EventQueue( ev)
end

function btnPasteTexture_OnButtonClicked(buttonHandle)
	local ev = KEP_EventCreate( TEXTURE_PASTE_EVENT)
	KEP_EventQueue( ev)
end

function btnImport_OnButtonClicked(buttonHandle)
	local event = KEP_EventCreate( FILE_BROWSER_CLOSED_EVENT )
	KEP_EventSetFilter( event, UGC_TYPE.TEXTURE )
	UGC_BrowseFileByImportType( UGC_TYPE.TEXTURE, event )
end

function btnEdit_OnButtonClicked(buttonHandle)
	local type = codeFromExtension(gTextureName)
	local baseName = string.sub(gTextureName, 1, string.len(gTextureName)-4)
	KEP_RequestSourceAssetFromAppServer(type, baseName)
end

function FileBrowserClosedHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local dialogResult = KEP_EventDecodeNumber( event )
	local filePath = ""
	if dialogResult~=0 then
		filePath = KEP_EventDecodeString( event )
	else
		--File Browser was cancelled
		return
	end

	local type = codeFromExtension(filePath)
	if filter==UGC_TYPE.TEXTURE and type ~= DevToolsUpload.UNKNOWN then
		KEP_Import3DAppAsset(type, filePath)
		populateTextures()
	end
end

function codeFromExtension(filePath)
	local lower = string.lower(filePath)
	local type = DevToolsUpload.UNKNOWN

	if string.find(lower, ".dds") ~= nil then
		type = DevToolsUpload.MENU_DDS
	elseif string.find(lower, ".tga") ~= nil then
		type = DevToolsUpload.MENU_TGA
	elseif string.find(lower, ".bmp") ~= nil then
		type = DevToolsUpload.MENU_BMP
	elseif string.find(lower, ".png") ~= nil then
		type = DevToolsUpload.MENU_PNG
	elseif string.find(lower, ".jpg") ~= nil or string.find(lower, ".jpeg")then
		type = DevToolsUpload.MENU_JPG
	elseif string.find(lower, ".jpeg") ~= nil or string.find(lower, ".jpeg")then
		type = DevToolsUpload.MENU_JPEG
	end
	return type
end

function extensionFromCode(code)
	local extension = ".dds"
	if code == DevToolsUpload.MENU_DDS then
		extension = ".dds"
	elseif code == DevToolsUpload.MENU_TGA then
		extension = ".tga"
	elseif code == DevToolsUpload.MENU_BMP then
		extension = ".bmp"
	elseif code == DevToolsUpload.MENU_PNG then
		extension = ".png"
	elseif code == DevToolsUpload.MENU_JPG then
		extension = ".jpg"
	elseif code == DevToolsUpload.MENU_JPEG then
		extension = ".jpg"
	else
		LogWarn( "Invalid asset code" )
	end
	return extension
end

function AppAssetDownloadHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local path = KEP_EventDecodeString(event)
	local fileType = KEP_EventDecodeNumber(event)
	if fileType == DevToolsUpload.MENU_DDS then
		KEP_ShellOpen(path)
	elseif fileType == DevToolsUpload.MENU_TGA then
		KEP_ShellOpen(path)
	elseif fileType == DevToolsUpload.MENU_BMP then
		KEP_ShellOpen(path)
	elseif fileType == DevToolsUpload.MENU_PNG then
		KEP_ShellOpen(path)
	elseif fileType == DevToolsUpload.MENU_JPG then
		KEP_ShellOpen(path)
	elseif fileType == DevToolsUpload.MENU_JPEG then
		KEP_ShellOpen(path)
	else
		LogWarn( "Invalid asset code" )
	end
end
