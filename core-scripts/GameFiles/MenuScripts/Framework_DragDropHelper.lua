--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("Framework_InventoryHelper.lua")

DragDrop = {}

local DEFAULT_ELEMENT = "Framework_DragDropElement.xml"
local DRAG_DROP_BUFFER = 3

local m_mouseOverIndex = nil
local m_dialogOverIndex = nil
local m_menuPID = nil
local m_mouseDown = false

local m_dragItems = {}
local m_dragElement = DEFAULT_ELEMENT
local m_indexOffset = 0
local m_indexOffsetExclusion = 0

local m_mouseX = 0
local m_mouseY = 0

local btnInfo = {btnItem = 0, btnItemDrop = 0, btnItemRestricted = 0} 

function DragDrop.initializeDragDrop(numFullButtons, numDropButtons, numRestrictedButtons, menuPID, dragElement)
	m_menuPID = menuPID
	btnInfo.btnItem = numFullButtons or 0
	btnInfo.btnItemDrop = numDropButtons or 0
	btnInfo.btnItemRestricted = numRestrictedButtons or 0
	
	for btnName, count in pairs(btnInfo) do
		for i = 1, count do
			if gHandles[btnName .. tostring(i)] then
				_G[btnName .. tostring(i) .. "_OnMouseEnter"] = function(btnHandle)
					DragDrop.buttonOnMouseEnterHandler(btnHandle)
				end
				
				_G[btnName .. tostring(i) .. "_OnMouseLeave"] = function(btnHandle)
					DragDrop.buttonOnMouseLeaveHandler(btnHandle)
				end
				
				_G[btnName .. tostring(i) .. "_OnLButtonDown"] = function(btnHandle, x, y)
					DragDrop.buttonOnLButtonDownHandler(btnHandle, x, y)
				end
				
				_G[btnName .. tostring(i) .. "_OnLButtonUp"] = function(btnHandle, x, y)
					DragDrop.buttonOnLButtonUpHandler(btnHandle, x, y)
				end				
			end
		end
	end
	
	if gHandles["imgDialogBG"] then
		_G["imgDialogBG_OnMouseEnter"] = function(imgHandle)
			DragDrop.dialogOnMouseEnterHandler(imgHandle)
		end
		
		_G["imgDialogBG_OnMouseLeave"] = function(imgHandle)
			DragDrop.dialogOnMouseLeaveHandler(imgHandle)
		end
	end
	
	_G["Dialog_OnMouseMove"] = function(dialogHandle, x, y)
		DragDrop.dialogOnMouseMoveHandler(dialogHandle, x, y)
	end
	
	if dragElement ~= nil then
		DragDrop.setDragElement(dragElement)
	end
end

function DragDrop.buttonOnMouseEnterHandler(btnHandle)
	DragDrop.buttonOnMouseEnter(btnHandle)
end

function DragDrop.buttonOnMouseLeaveHandler(btnHandle)
	DragDrop.buttonOnMouseLeave(btnHandle)
end

function DragDrop.buttonOnLButtonDownHandler(btnHandle, x, y)
	DragDrop.buttonOnLButtonDown(btnHandle, x, y)
end

function DragDrop.buttonOnLButtonUpHandler(btnHandle, x, y)
	DragDrop.buttonOnLButtonUp(btnHandle, x, y)
end

function DragDrop.dialogOnMouseEnterHandler(imgHandle)
	DragDrop.dialogOnMouseEnter(imgHandle)
end

function DragDrop.dialogOnMouseLeaveHandler(imgHandle)
	DragDrop.dialogOnMouseLeave(imgHandle)
end

function DragDrop.dialogOnMouseMoveHandler(dialogHandle, x, y)
	DragDrop.dialogOnMouseMove(dialogHandle, x, y)
end

function DragDrop.buttonOnMouseEnter(btnHandle, isValid)
	local s, e, n = string.find(Control_GetName(btnHandle), "(%d+)")
	local dropOnly = string.find(Control_GetName(btnHandle), "Drop") ~= nil
	m_mouseOverIndex = tonumber(n)
	if m_mouseOverIndex > m_indexOffsetExclusion then m_mouseOverIndex = m_mouseOverIndex + m_indexOffset end
	local item = m_dragItems[m_mouseOverIndex] or {}
	if dropOnly or (item and item.properties and item.properties.GLID) then
		DragDrop.sendMouseEnterEvent(m_mouseOverIndex, item, dropOnly, isValid)
	end
end

function DragDrop.buttonOnMouseLeave(btnHandle)
	local item = m_dragItems[m_mouseOverIndex]
	local dropOnly = string.find(Control_GetName(btnHandle), "Drop") ~= nil
	if dropOnly or (item and item.properties and item.properties.GLID) then
		DragDrop.sendMouseLeaveEvent(m_mouseOverIndex, item or {})
	end
	m_mouseOverIndex = nil
	m_mouseDown = false
end

function DragDrop.buttonOnLButtonDown(btnHandle, x, y)
	m_mouseDown = true
	m_mouseX = x
	m_mouseY = y
end

function DragDrop.buttonOnLButtonUp(btnHandle, x, y)
	if m_mouseDown then
		m_mouseDown = false

		if math.abs(x - m_mouseX) < DRAG_DROP_BUFFER and math.abs(y - m_mouseY) < DRAG_DROP_BUFFER then
			local onButtonClicked = _G[Control_GetName(btnHandle) .. "_OnButtonClicked"]
			if onButtonClicked then	onButtonClicked(btnHandle) end
		end
	end
end

function DragDrop.dialogOnMouseEnter(imgHandle)
	if m_menuPID then
		DragDrop.sendMouseEnterDialogEvent(m_menuPID)
	end
end

function DragDrop.dialogOnMouseLeave(imgHandle)
	if m_menuPID then
		DragDrop.sendMouseLeaveDialogEvent(m_menuPID)
	end
end

function DragDrop.dialogOnMouseMove(dialogHandle, x, y)
	local item = m_dragItems[m_mouseOverIndex]
	if m_mouseDown and m_mouseOverIndex and item and item.properties and item.properties.GLID and (item.UNID ~= 0 and item.properties.GLID ~= 0) then
		local mouseMoved = math.abs(x - m_mouseX) >= DRAG_DROP_BUFFER or math.abs(y - m_mouseY) >= DRAG_DROP_BUFFER
		if mouseMoved then
			local buttonID = m_mouseOverIndex
			if buttonID > m_indexOffsetExclusion then buttonID = buttonID - m_indexOffset end
			local offsetX = m_mouseX - Control_GetLocationX(gHandles["btnItem"..tostring(buttonID)])
			local offsetY = m_mouseY - Control_GetLocationY(gHandles["btnItem"..tostring(buttonID)])
			DragDrop.sendDragDropEvent(m_mouseOverIndex, item, offsetX, offsetY, m_dragElement)
			m_mouseDown = false
		end
	end
end

function DragDrop.setDragElement(dragElement)
	m_dragElement = dragElement
end

function DragDrop.registerItem(index, pid, item)
	m_menuPID = pid
	item.pid = pid
	m_dragItems[index] = item
end

function DragDrop.setIndexOffset(offset, exclusion)
	m_indexOffset = offset or 0
	m_indexOffsetExclusion = exclusion or 0
end

function DragDrop.sendDragDropEvent(index, item, offsetX, offsetY, dragElement)
	local event = KEP_EventCreate("DRAG_DROP_START")
	KEP_EventEncodeNumber(event, index)
	KEP_EventEncodeNumber(event, item.pid)
	event = compileInventoryItem(item, event)
	KEP_EventEncodeNumber(event, offsetX)
	KEP_EventEncodeNumber(event, offsetY)
	KEP_EventEncodeString(event, dragElement)
	KEP_EventSetFilter(event, 4)
	KEP_EventQueue(event)
end

function DragDrop.sendMouseEnterEvent(index, item, dropOnly, isValid)
	local event = KEP_EventCreate("DRAG_DROP_MOUSE_ENTER")
	KEP_EventEncodeNumber(event, index)
	KEP_EventEncodeNumber(event, item.pid)
	event = compileInventoryItem(item, event)
	KEP_EventEncodeString(event, tostring(dropOnly or false))
	KEP_EventEncodeString(event, tostring(isValid))
	KEP_EventSetFilter(event, 4)
	KEP_EventQueue(event)	
end

function DragDrop.sendMouseLeaveEvent(index, item)
	local event = KEP_EventCreate("DRAG_DROP_MOUSE_LEAVE")
	KEP_EventEncodeNumber(event, index)
	KEP_EventEncodeNumber(event, item.pid)
	event = compileInventoryItem(item, event)
	KEP_EventSetFilter(event, 4)
	KEP_EventQueue(event)
end

function DragDrop.sendMouseEnterDialogEvent(pid)
	local event = KEP_EventCreate("DRAG_DROP_MOUSE_ENTER_DIALOG")
	KEP_EventEncodeNumber(event, pid)
	KEP_EventSetFilter(event, 4)
	KEP_EventQueue(event)	
end

function DragDrop.sendMouseLeaveDialogEvent(pid)
	local event = KEP_EventCreate("DRAG_DROP_MOUSE_LEAVE_DIALOG")
	KEP_EventEncodeNumber(event, pid)
	KEP_EventSetFilter(event, 4)
	KEP_EventQueue(event)
end
