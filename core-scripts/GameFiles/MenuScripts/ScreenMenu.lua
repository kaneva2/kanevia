--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

PLAYER_TARGETED_MENU_EVENT = "PlayerTargetedMenuEvent"
CLOSE_CONTEXT_EVENT = "CloseContextEvent"
SEND_SCREEN_COORDS_EVENT = "SendScreenCoordsEvent"

CLOSE_CONTEXT_MENUS_EVENT = "CloseContextMenusEvent"

g_ContextMenuDialog = nil
g_PlayerOptionsMenuDialog = nil

g_ContextMenuID = 0
g_PlayerOptionsID = 0

g_kanevaUserNetId  = nil
g_playerName = nil

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "playerTargetedMenuEventHandler", PLAYER_TARGETED_MENU_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "SendScreenCoordsHandler", SEND_SCREEN_COORDS_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "CloseContextMenusHandler", CLOSE_CONTEXT_MENUS_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "CloseContextHandler", CLOSE_CONTEXT_EVENT, KEP.HIGH_PRIO )
end

function CloseContextMenusHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	sendCloseEvent(g_PlayerOptionsMenuDialog)
	MenuCloseThis()
end

function CloseContextHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	MenuCloseThis()
end

function SendScreenCoordsHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local x = KEP_EventDecodeNumber(event)
	local y = KEP_EventDecodeNumber(event)
	g_PlayerOptionsMenuDialog = Dialog_CreateAtCoordinates( "ContextMenuActions.xml", x+65, y-30 ) --Dialog_Create("PlayerOptions.xml")
	local ev = KEP_EventCreate( PLAYER_TARGETED_MENU_EVENT )
	KEP_EventEncodeNumber( ev, g_kanevaUserNetId  )
	KEP_EventEncodeString( ev, g_playerName )
	KEP_EventQueue( ev )
end

function playerTargetedMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_kanevaUserNetId  = KEP_EventDecodeNumber( event )
	g_playerName = KEP_EventDecodeString( event )
	return KEP.EPR_OK
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( CLOSE_CONTEXT_MENUS_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( CLOSE_CONTEXT_EVENT, KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- Set the dialog to take up the full screen area
	local screenWidth = Dialog_GetScreenWidth(dialogHandle)
	local screenHeight = Dialog_GetScreenHeight(dialogHandle)
	MenuSetLocationThis(0, 0)
	MenuSetSizeThis(screenWidth, screenHeight)

	-- Resize the btnOk control to be as large as the screen also
	local btnOk = Dialog_GetControl(dialogHandle, "btnOk")
	Control_SetLocation(btnOk, 0, 0)
	Control_SetSize(btnOk, screenWidth, screenHeight)

	-- Create the context menu dialog
	g_ContextMenuDialog = MenuOpen("ContextMenu.xml")
end

function Dialog_OnDestroy(dialogHandle)
	DestroyMenu(g_PlayerOptionsMenuDialog)
	DestroyMenu(g_ContextMenuDialog)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function sendCloseEvent( dh )
	local ev = KEP_EventCreate( CLOSE_CONTEXT_EVENT )
	KEP_EventEncodeNumber( ev, dh )
	KEP_EventQueue( ev )
end
