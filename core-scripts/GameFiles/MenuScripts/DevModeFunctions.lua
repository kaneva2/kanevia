--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- dofiled from DevMode & DevModeMenu

function DevMode_ShowPhysicsWindow(newShow)
	Log("DevMode - Show Physics Window")
	local oldShow = KEP_ShowPhysicsWindow( -1 )
	if newShow == nil then return oldShow end
	if oldShow ~= newShow then
		KEP_ShowPhysicsWindow( newShow )
		Log( " ... physicsWindow=" .. newShow )
	end
	return newShow
end

function DevMode_MemberAlts()
	Log( "DevMode - My MemberAlts" )
	SendMemberAltsEvent(GetUserName(), GetUserId())
end

function DevMode_LogDialogs()
	Log( "DevMode - Log Dialogs" )
	Dialog_Log()
end

function DevMode_LogMsgGzip()
	Log( "DevMode - Log MsgGzip" )
	local enabled = KEP_IsMsgGzipLogEnabled()
	KEP_SetMsgGzipLogEnabled(not enabled)
end

function DevMode_LogMetrics()
	Log( "DevMode - Log Metrics" )
	KEP_LogMetrics()
	collectgarbage("collect") -- perform lua garbage collection
	KEP_LogMemory()
	Log("LogMemory: luaMemoryUsage="..collectgarbage("count").." kb")
end

function DevMode_LogScriptRefs()
	Log( "DevMode - Log Script Refs" )
	KEP_LogObjects()
end

g_devModeMsgProc = false
function DevMode_LogMsgProc()
	Log( "DevMode - Log MsgProc" )
	g_devModeMsgProc = not g_devModeMsgProc
	if ( g_devModeMsgProc ) then 
		KEP_LogMsgProc( 1 )
	else
		KEP_LogMsgProc( 0 )
	end
end

function DevMode_OpenQAMenu()
	Log( "DevMode - Open QA Menu" )
	MenuOpen("QA.xml")
end

function DevMode_LogResources()
	Log( "DevMode - Log Resources" )
	KEP_LogResources( 31 )
end

function DevMode_LogWebcalls()
	Log( "DevMode - Log WebCalls" )
	KEP_LogWebCalls()
end

function DevMode_CrashClient()
	Log( "DevMode - Crash Client" )
	KEP_Crash(3)
end
