--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- InventoryHandler.lua
-- Author: Paul!
-- She's a beast really. She holds all the inventories and informs the
-- world of where it fits inside itself. Quite existential really. . .
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")

------------------------
-- Local vars
------------------------
-- Base damage for each armor by level
local ITEM_VALUE_BASE = {1,11,101,1001,10001}
-- ARMOR ranges available for each armor by level
local ITEM_VALUE_RANGE = {9,89,899,8999,89999}
-- Despawn time for player loot drops
local DEFAULT_DESPAWN_TIME = 600
-- GLID of the container to drop on death (TODO: Should this be configured elsewhere, per game?)
local DEFAULT_DROP_CONTAINER = 3402
-- Maximum durability
local DEFAULT_MAX_DURABILITY = 1000
-- Minimum random durability
local DEFAULT_MIN_DURABILITY = 100

local TYPE_FILTER = "allGameItems"

local SECS_IN_MIN = 60


local glowingItems = {}

local m_playerName = nil
local m_inventory = {}
local m_dropLocation = nil
local m_requestQuests = false
local m_currTime = 0
local m_elapsedTime = 0
local m_activeCompleteTimer = {}
local m_processingGameItems = false
local m_processingQuestItems = false
local m_initialRequest = true
local m_dropItems = false
local m_deathCount = 0

function onCreate()
	-- Initialize the drag and drop system, if not already running
	MenuOpen("Framework_DragDropHandler.xml")
	-- Open the Image handler for Game Items
	--MenuOpen("Framework_ImageHandler.xml")

	KEP_EventRegister("INVENTORY_HANDLER_PROCESS_TRANSACTION", KEP.HIGH_PRIO)
	KEP_EventRegister("INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegister("INVENTORY_HANDLER_PREPROCESS_TRANSACTION", KEP.HIGH_PRIO)
	KEP_EventRegister("INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegister("INVENTORY_HANDLER_ADD_ITEM", KEP.HIGH_PRIO) -- Event to add or update an item in a given inventory slot
	KEP_EventRegister("INVENTORY_HANDLER_ADD_ITEM_LOCAL", KEP.HIGH_PRIO) -- Event to add or update an item in a given inventory slot locally
	KEP_EventRegister("INVENTORY_HANDLER_REMOVE_ITEM", KEP.HIGH_PRIO) -- Event to remove an item from a given inventory slot
	KEP_EventRegister("INVENTORY_HANDLER_ADD_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegister("INVENTORY_HANDLER_COMPLETE_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegister("INVENTORY_HANDLER_ABANDON_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegister("INVENTORY_HANDLER_FULFILL_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegister("INVENTORY_HANDLER_UPDATE_FULFILL_COUNT", KEP.HIGH_PRIO)
	KEP_EventRegister("INVENTORY_HANDLER_UPDATE_DIRTY", KEP.HIGH_PRIO) -- Event to send all dirty inventories to the server
	KEP_EventRegister("INVENTORY_HANDLER_REQUEST_INVENTORY", KEP.HIGH_PRIO) -- Event to request cached or server inventory (if no cache)
	KEP_EventRegister("INVENTORY_HANDLER_CHECK_LOOT", KEP.HIGH_PRIO) -- Event to check to see if there is room in player inventory for loot
	KEP_EventRegister("INVENTORY_HANDLER_CHECK_LOOT_RESPONSE", KEP.HIGH_PRIO) -- Response event for inventory loot space check
	
	KEP_EventRegister("INVENTORY_HANDLER_REQUEST_ITEM_NAME", KEP.HIGH_PRIO) -- Get the item name from cache, given a UNID
	KEP_EventRegister("INVENTORY_HANDLER_RETURN_ITEM_NAME", KEP.HIGH_PRIO) -- Get the item name from cache, given a UNID
	
	KEP_EventRegister("CLOSE_BACKPACK", KEP.HIGH_PRIO) -- Event to close the Framework_PlayerSystems menu
	KEP_EventRegister("CLOSE_CHEST", KEP.HIGH_PRIO) -- Event to close the Framework_ItemContainer menu
	KEP_EventRegister("CLOSE_VENDOR", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_SEND_DEATH_COUNT", KEP.MED_PRIO) -- Event to tell the DeathWarn menu which variation to display
	
	
	-- Register handlers for client-to-client events
	KEP_EventRegisterHandler("onProcessTransaction", "INVENTORY_HANDLER_PROCESS_TRANSACTION", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onPreprocessTransaction", "INVENTORY_HANDLER_PREPROCESS_TRANSACTION", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onAddItem", "INVENTORY_HANDLER_ADD_ITEM", KEP.HIGH_PRIO) -- Event handler to add or update an item
	KEP_EventRegisterHandler("onAddItemLocal", "INVENTORY_HANDLER_ADD_ITEM_LOCAL", KEP.HIGH_PRIO) -- Event handler to add or update an item locally
	KEP_EventRegisterHandler("onRemoveItem", "INVENTORY_HANDLER_REMOVE_ITEM", KEP.HIGH_PRIO) -- Event handler to remove an item
	KEP_EventRegisterHandler("onAddQuest", "INVENTORY_HANDLER_ADD_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onCompleteQuest", "INVENTORY_HANDLER_COMPLETE_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onAbandonQuest", "INVENTORY_HANDLER_ABANDON_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onFulfillQuest", "INVENTORY_HANDLER_FULFILL_QUEST", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onUpdateFulfillCount", "INVENTORY_HANDLER_UPDATE_FULFILL_COUNT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onUpdateDirtyInventory", "INVENTORY_HANDLER_UPDATE_DIRTY", KEP.HIGH_PRIO) -- Event handler to send dirty to server
	KEP_EventRegisterHandler("onInventoryRequest", "INVENTORY_HANDLER_REQUEST_INVENTORY", KEP.HIGH_PRIO) -- Event handler to request inventory
	KEP_EventRegisterHandler("onInventoryCheckLoot", "INVENTORY_HANDLER_CHECK_LOOT", KEP.HIGH_PRIO) -- Event handler to check available space
	
	KEP_EventRegisterHandler("onRequestItemName", "INVENTORY_HANDLER_REQUEST_ITEM_NAME", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler("onCancelDragDrop", "DRAG_DROP_CANCEL_RESPONSE", KEP.HIGH_PRIO) -- Event handler for drag / drop cancel response (on player death)
	
	-- Register handler for server-to-client events
	Events.registerHandler("UPDATE_INVENTORY", updateInventory) -- Event handler when receiving an inventory from server
	Events.registerHandler("UPDATE_QUESTS", updateQuests) -- Event handler when receiving a quest log from server
	Events.registerHandler("UPDATE_ITEM_CONTAINER", updateItemContainer) -- Event handler when receiving item container contents from server
	Events.registerHandler("UPDATE_VENDOR_ITEMS", updateVendorItems)
	Events.registerHandler("DROP_PLAYER_INVENTORY", dropPlayerInventory) -- Event handler to drop player inventory on death
	Events.registerHandler("UPDATE_ITEM_PROPERTIES", updateItemProperties) -- Event handler to update items in inventory in real time when they are edited.
	Events.registerHandler("UPDATE_ITEM_INSTANCE_DATA", updateItemInstanceData) -- Event handler to update item instanceData
	Events.registerHandler("RETURN_CURRENT_TIME", returnCurrentTime) -- Handles the current server time from Controller_Game
	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS", returnSettingsHandler) -- Handles receiving the current zone world settings from Controller_Game 
	Events.registerHandler("RETURN_DEATH_COUNT", returnDeathCountHandler) -- Gets # player deaths in the current world before from Controller_Game

	Events.registerHandler("FRAMEWORK_ADD_SINGLE_LOOT", onAddSingleLootItem)
	Events.registerHandler("UPDATE_ALL_GAME_ITEMS", onUpdateGameItems)
	Events.registerHandler("FRAMEWORK_GAME_ITEM_UPDATED", onGameItemUpdated)
	
	m_playerName = KEP_GetLoginName() -- Get the local client name
	
	MenuSetLocationThis(0, 0) -- Handler menus are located in the upper left
	
	Events.sendEvent("GET_ALL_GAME_ITEMS")
	m_processingGameItems = true
	
	Events.sendEvent("FRAMEWORK_REQUEST_CURRENT_TIME")
	Events.sendEvent("FRAMEWORK_REQUEST_SETTINGS") -- Get the current zone world settings from Controller_Game
	Events.sendEvent("FRAMEWORK_REQUEST_DEATH_COUNT")
end

-- OnMoved handler for this Dialog
function Dialog_OnMoved(dialogHandle, x, y)
	MenuSetLocationThis(0, 0) -- Handler menus are located in the upper left
end

-- Called every frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- Check elapsed time and add to current time after a second has passed
	m_elapsedTime = m_elapsedTime + fElapsedTime
	
	if m_elapsedTime >= 1 then
		m_currTime = m_currTime + 1
		m_elapsedTime = m_elapsedTime - 1
		
		-- Update each quest's timer
		if m_inventory[QUEST_PID] and m_inventory[QUEST_PID].inventory then
			for index, quest in pairs(m_inventory[QUEST_PID].inventory) do
				if m_activeCompleteTimer[quest.UNID] then
					if quest.properties.repeatable == "true" and quest.complete and quest.completeTime and quest.properties.repeatTime then
						if quest.completeTime + (quest.properties.repeatTime * SECS_IN_MIN) <= m_currTime then
							quest.repeatTimerUp = true
							-- Turn off m_activeCompleteTimer?
							setActiveCompleteTimer()
							
							Events.sendEvent("UPDATE_PLAYER_QUEST_PROGRESS", {updateIcons = true})
							updateClientInventoryItem(quest.UNID, QUEST_PID, quest) -- Update item to interested parties
							break
						end
					end
				end
			end
		end
	end
end

-- Called when a player's inventory has been updated
-- Handles main inventory (backpack and toolbar), armory, and blueprints
function updateInventory(event)
	
	-- Handle the main inventory
	if event.updatedInventory then
		m_inventory[INVENTORY_PID] = {}
		m_inventory[INVENTORY_PID].inventory = event.updatedInventory

		-- Calculate damage / armor rating metadata from item level and item instance multiplier if this is a weapon or armor
		for slot,item in pairs(m_inventory[INVENTORY_PID].inventory) do
			if isInstancedItem(item.properties.itemType) and item.instanceData.durability == nil then
				item.instanceData = {levelMultiplier = getLevelMultiplier(), durability = getDurability()}
			end
			if item.properties.itemType == ITEM_TYPES.WEAPON then
				item.properties.damage = calculateWeaponDamage(item.properties.level, item.instanceData.levelMultiplier)
			elseif item.properties.itemType == ITEM_TYPES.ARMOR then
				item.properties.armorRating = calculateArmorRating(item.properties.level, item.instanceData.levelMultiplier)
			end
		end

		-- This inventory is authoritative, clear out dirty slots
		m_inventory[INVENTORY_PID].dirtySlots = {}
		-- Send inventory to any client menus that are interested in this data
		updateClientInventoryFull(m_inventory[INVENTORY_PID].inventory, INVENTORY_PID)
		
	end
	
	-- Handles the armory
	if event.updatedArmor then
		m_inventory[ARMORY_PID] = {}
		m_inventory[ARMORY_PID].inventory = event.updatedArmor
		
		-- Calculate armor rating metadata from item level and item instance multiplier
		for slot,item in pairs(m_inventory[ARMORY_PID].inventory) do
			item.properties.armorRating = calculateArmorRating(item.properties.level, item.instanceData.levelMultiplier)
		end
		
		-- This inventory is authoritative, clear out dirty slots
		m_inventory[ARMORY_PID].dirtySlots = {}
		-- Send inventory to any client menus that are interested in this data
		updateClientInventoryFull(m_inventory[ARMORY_PID].inventory, ARMORY_PID)
	end
	
	-- Handles player blueprints (blueprint inventory is indexed)
	if event.updatedBlueprints then
		m_inventory[BLUEPRINT_PID] = {}
		m_inventory[BLUEPRINT_PID].inventory = {}
		
		-- Copy blueprints into indexed array
		for k, v in pairs(event.updatedBlueprints) do
			m_inventory[BLUEPRINT_PID].inventory[tonumber(k)] = v
		end
		
		-- This inventory is authoritative, clear out dirty slots
		m_inventory[BLUEPRINT_PID].dirtySlots = {}
		-- Send inventory to any client menus that are interested in this data
		updateClientInventoryFull(m_inventory[BLUEPRINT_PID].inventory, BLUEPRINT_PID)		
	end
	
	if m_requestQuests then
		m_requestQuests = false
		if not m_processingQuestItems then
			m_processingQuestItems = true
			Events.sendEvent("REQUEST_PLAYER_QUESTS")
		end
	end
end

-- Called when a player's quest log has been updated
-- Handles quest inventory
function updateQuests(event)
	m_inventory[QUEST_PID] = {}
	m_inventory[QUEST_PID].inventory = {}
	
	if m_inventory[GAME_PID].inventory == nil then
		log("ERROR: updateQuests detected a nil game item cache!")
		return
	end
	
	Events.sendEvent("UPDATE_PLAYER_QUEST_PROGRESS") --, {updateIcons = true})
	
	local questCount = 0
	-- Handle the completed quests
	if event.questsComplete then
		for k, questInfo in pairs(event.questsComplete) do
			local questProps = m_inventory[GAME_PID].inventory[tostring(questInfo.UNID)]
			if questProps then
				questCount = questCount + 1
				
				questInfo.properties = deepCopy(questProps)
				
				m_inventory[QUEST_PID].inventory[questCount] = questInfo
				m_inventory[QUEST_PID].inventory[questCount].complete = true
				m_inventory[QUEST_PID].inventory[questCount].active = false
				
				if m_currTime and questInfo.properties.repeatable == "true" then
					m_inventory[QUEST_PID].inventory[questCount].repeatTimerUp = questInfo.completeTime + (questInfo.properties.repeatTime * SECS_IN_MIN) <= m_currTime
				end
			end
		end
	end
	
	-- Handle the incomplete quests
	if event.questsIncomplete then
		for k, questInfo in pairs(event.questsIncomplete) do
			local questProps = m_inventory[GAME_PID].inventory[tostring(questInfo.UNID)]
			if questProps then
				local duplicateQuest = false
				for i, playerQuest in pairs(m_inventory[QUEST_PID].inventory) do
					-- If we have a duplicate quest, simply update it's complete status
					if playerQuest.UNID == questInfo.UNID then
						m_inventory[QUEST_PID].inventory[i].complete = false
						m_inventory[QUEST_PID].inventory[i].active = true
						m_inventory[QUEST_PID].inventory[i].fulfilled = questInfo.fulfilled
						m_inventory[QUEST_PID].inventory[i].fulfillCount = questInfo.fulfillCount
						duplicateQuest = true
						updateQuestProgress(m_inventory[QUEST_PID].inventory[i], true)
						break
					end
				end
				-- If unique quest, insert it
				if not duplicateQuest then
					questCount = questCount + 1
					questInfo.properties = deepCopy(questProps)
					m_inventory[QUEST_PID].inventory[questCount] = questInfo
					m_inventory[QUEST_PID].inventory[questCount].complete = false
					m_inventory[QUEST_PID].inventory[questCount].active = true
					m_inventory[QUEST_PID].inventory[questCount].fulfilled = questInfo.fulfilled
					m_inventory[QUEST_PID].inventory[questCount].fulfillCount = questInfo.fulfillCount
					updateQuestProgress(m_inventory[QUEST_PID].inventory[questCount], true)
				end
			end
		end
	end
	setActiveCompleteTimer()
	m_processingQuestItems = false
	
	if MenuIsOpen("ProgressMenu") then
		KEP_EventCreateAndQueue("FRAMEWORK_LOAD_COMPLETE") -- Finalize loading
	end

	-- Send inventory to any client menus that are interested in this data
	updateClientInventoryFull(m_inventory[QUEST_PID].inventory, QUEST_PID)
end

-- Handler for when an item has been changed in the Game System editor
-- Finds cached instances of updated item and updates them before sending the new data to other client menus
-- TODO: Does this affect blueprint inventories?  If so, the logic may need to be adjusted, as that table is indexed.
function updateItemProperties(event)
	if event.UNID then -- Is this a valid item?
		for PID,table in pairs(m_inventory) do -- Iterate over main inventory categories, including all vendor and container inventories
			if PID > GAME_PID then
				for _,item in pairs(table.inventory) do -- Iterate over the inventory tables for each category
					if item and type(item) == "table" and tostring(item.UNID) == tostring(event.UNID) then -- Does the UNID for this item match the UNID of the updated item?
						if event.properties then -- Is this an item with properties?
							-- Copy the updated item properties into the cached item properties
							for key,value in pairs(event.properties) do
								item.properties[key] = value
							end						
						else -- Item was deleted, insert empty item
							item = EMPTY_ITEM
						end
					elseif item and type(item) ~= "table" then
						log("ERROR - updateItemProperties found an invalid item in inventory with PID = "..tostring(PID).." at index "..tostring(index).."!")
					end
				end

				-- Let interested menus know of the inventory update
				updateClientInventoryFull(table.inventory, PID)			
			end
		end
	end
end

-- Update item's instanceData
function updateItemInstanceData(event)
	local pid 			  = event.pid
	local index 		  = event.index
	local newInstanceData = event.instanceData
	
	if pid and index and newInstanceData then -- Is this a valid item?
		if m_inventory[pid] then
			local inventory = m_inventory[pid].inventory
			if inventory and inventory[index] and inventory[index].instanceData then
				-- Copy the updated item properties into the cached item properties
				for key,value in pairs(newInstanceData) do
					inventory[index].instanceData[key] = value
				end
			end
			-- Let interested menus know of the inventory update
			updateClientInventoryFull(inventory, pid)
		end
	end
end

-- Handles the current server time (Used for updating quest's completion time)
function returnCurrentTime(event)
	m_currTime = event.timestamp or m_currTime
	
	if #m_activeCompleteTimer == 0 then
		setActiveCompleteTimer()
	end
	if m_inventory[QUEST_PID] and m_inventory[QUEST_PID].inventory then
		-- Update all quest item repeat timers
		for index, quest in pairs(m_inventory[QUEST_PID].inventory) do
			if quest.complete and quest.repeatable == "true" then
				quest.repeatTimerUp = questInfo.completeTime + (questInfo.properties.repeatTime * SECS_IN_MIN) <= m_currTime
			end
		end
	end
end

-- Handles current zone world settings
function returnSettingsHandler(event)
	m_dropItems = event.dropItems or false
end

-- Gets # player deaths in the current world before from Controller_Game
function returnDeathCountHandler(event)
	m_deathCount = event.deathCount or 0
end

function onAddSingleLootItem(event )
	if event and event.itemList then
		local gameItemPropertiesList = m_inventory[GAME_PID].inventory
	
		-- Extract the container information
		for slot, item in pairs(event.itemList) do
			if item then
				local gameItemProperties = gameItemPropertiesList[tostring(item.UNID)]
				if gameItemProperties then
					item.properties = deepCopy(gameItemProperties)
					item.instanceData = item.instanceData or {}
					durability = getMaxDurabilty(item)

					local instanceData = {levelMultiplier = getLevelMultiplier(), durability = durability}
					item.properties.instanceData = item.instanceData
				end
			end
		end
		
		success, removeTable, addTable = processTransactionInternal({}, event.itemList, false)
		Events.sendEvent("FRAMEWORK_DESELECT_SINGLE_LOOT", {PID = event.PID, success = success})

		if success then
			for i, v  in pairs(addTable) do
				local transactionID = KEP_GetCurrentEpochTime()
				addTable[i].lootInfo = {sourcePID = event.PID, lootSource = "Chest", transactionID = transactionID}
				updateInventoryItem(0, INVENTORY_PID, addTable[i], addTable[i].isCancel or false, false, m_inventory, true)
			end	
			local newItemEvent = KEP_EventCreate("FRAMEWORK_NEW_ITEM_MESSAGE")
			KEP_EventEncodeString(newItemEvent,"Item")
			KEP_EventQueue(newItemEvent)
			updateDirty()

			--Events.sendEvent("OBJECT_DESPAWN", {spawnerPID = event.spawnerPID, despawnPID = event.PID, spawnType = "loot"})
		end
	end
end

function onUpdateGameItems(event)
	if event then
		m_inventory[GAME_PID] = {inventory = {}}
		
		for UNID, properties in pairs(event.items) do
			m_inventory[GAME_PID].inventory[UNID] = properties
		end

		m_processingGameItems = false
		if m_initialRequest then
			m_initialRequest = false
			Events.sendEvent("REQUEST_PLAYER_INVENTORY")
			m_requestQuests = true
		end
		updateClientInventoryFull(m_inventory[GAME_PID].inventory, GAME_PID)
	end
end

function onGameItemUpdated(event)
	if event and m_inventory[GAME_PID] ~= nil and m_inventory[GAME_PID].inventory then
		local UNID = tostring(event.itemUNID)

		m_inventory[GAME_PID].inventory[UNID] = event.itemInfo
		updateClientInventoryItem(UNID, GAME_PID, event.itemInfo or {}) -- Update item to interested parties
	end
end

-- Event handler when an item container inventory is received from the server
-- Event should contain the PID of the object being updated
function updateItemContainer(event)
	event.PID = tonumber(event.PID) -- PID of the container associated with this inventory
	
	local gameItemPropertiesList = m_inventory[GAME_PID].inventory
	
	-- CJW, NOTE: I am not sure why the instanceData needs to be copied inside the properties table as well. Looks buggy, will investigate...
	
	-- Extract the container information
	local containerInventory = {}
	for slot = 1, event.containerSize do
		local item = event.updatedItems[tostring(slot)]
		if item then
			local gameItemProperties = gameItemPropertiesList[tostring(item.UNID)]
			if gameItemProperties == nil then
				containerInventory[slot] = deepCopy(EMPTY_ITEM)
			else
				item.properties = deepCopy(gameItemProperties)
				item.instanceData = item.instanceData or {}
				item.properties.instanceData = item.instanceData
				containerInventory[slot] = item
			end
		else
			containerInventory[slot] = deepCopy(EMPTY_ITEM)
		end
	end
	
	-- Calculate damage / armor rating metadata from item level and item instance multiplier if this is a weapon or armor
	for slot, item in pairs(containerInventory) do
		if isInstancedItem(item.properties.itemType) and item.instanceData.durability == nil then
			durability = getMaxDurabilty(item)
			local instanceData = {levelMultiplier = getLevelMultiplier(), durability = durability}
			item.instanceData = instanceData
			item.properties.instanceData = instanceData
		end
		if item.properties.itemType == ITEM_TYPES.WEAPON then
			item.properties.damage = calculateWeaponDamage(item.properties.level, item.instanceData.levelMultiplier)
		elseif item.properties.itemType == ITEM_TYPES.ARMOR then
			item.properties.armorRating = calculateArmorRating(item.properties.level, item.instanceData.levelMultiplier)
		end
	end
	
	-- Copy updated items into associated inventory
	m_inventory[event.PID] = {
		inventory	= containerInventory,
		dirtySlots	= {} --> This inventory is authoritative, clear out dirty slots
	}
	-- Send inventory to any client menus that are interested in this data
	updateClientInventoryFull(containerInventory, event.PID)
end

-- Local cache of all vendor items that we know about
function updateVendorItems(event)
	event.PID = tonumber(event.PID)
	m_inventory[event.PID] = {}
	m_inventory[event.PID].inventory = event.updatedItems
	

	
	--Attach properties here
	for index, item in pairs(m_inventory[event.PID].inventory) do
		if tostring(index) == "input" then
			m_inventory[event.PID].inventory.properties = m_inventory[GAME_PID].inventory[tostring(item)] or EMPTY_ITEM.properties
		elseif type(index) == "number" then
			if type(item.input) == "number" then
				m_inventory[event.PID].inventory[index].inputProperties = m_inventory[GAME_PID].inventory[tostring(item.input)] or EMPTY_ITEM.properties
			end
			m_inventory[event.PID].inventory[index].outputProperties = m_inventory[GAME_PID].inventory[tostring(item.output)] or EMPTY_ITEM.properties
		end
	end
	-- not dirty because its brand new (dirty: client side, when you call update item or remove item, 
	-- it adds that index to the dirty slots array, when update dirty, it sends all items in those slots to the server.)
	m_inventory[event.PID].dirtySlots = {}	

	-- Send the updated item array to Framework_ItemContainer with a PID > 0
	updateClientInventoryFull(event.updatedItems, event.PID, true)
end

-- Calculate armor rating metadata from item level and instance multiplier
function calculateArmorRating(level, multiplier)
	if level and multiplier then
		return math.floor((ITEM_VALUE_BASE[level] + (ITEM_VALUE_RANGE[level] * multiplier) / 4))
	end
end

-- Calculate weapon damage metadata from item level and instance multiplier
function calculateWeaponDamage(level, multiplier)
	if level and multiplier then
		return math.floor( ITEM_VALUE_BASE[level] + (ITEM_VALUE_RANGE[level] * multiplier) )
	end
end

-- Event handler to drop player inventory on death
-- Inventory is not actually dropped until response from drag / drop handler, in case the player is actively dragging an item
-- Also closes any backpack or item container menus that may be opened at the time of death
function dropPlayerInventory(event)
	if event.dropLocation == nil then return end -- Drop location is required
	m_dropLocation = event.dropLocation -- Save off the drop location for the drag / drop cancel callback
	
	-- Send a message to the drag / drop handler and cancel any active drag events
	-- Inventory is dropped on response to this event
	local cancelEvent = KEP_EventCreate("DRAG_DROP_CANCEL")
	KEP_EventSetFilter(cancelEvent, 4)
	KEP_EventQueue(cancelEvent)	
	
	-- Client to client event to close the backpack menu (and all associated menus)
	local closeBackpackEvent = KEP_EventCreate("CLOSE_BACKPACK")
	KEP_EventSetFilter(closeBackpackEvent, 4)
	KEP_EventQueue(closeBackpackEvent)

	-- Client to client event to close the item container menu
	local closeChestEvent = KEP_EventCreate("CLOSE_CHEST")
	KEP_EventSetFilter(closeChestEvent, 4)
	KEP_EventQueue(closeChestEvent)

	local closeVendorEvent = KEP_EventCreate("CLOSE_VENDOR")
	KEP_EventSetFilter(closeVendorEvent, 4)
	KEP_EventQueue(closeVendorEvent)
end

function onAddQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local event = {}
	
	event.quest = decompileInventoryItem(tEvent)
	
	if m_inventory[QUEST_PID].inventory then
		-- TODO: Cross-reference quest completion progress from main inventory
		if event.quest.complete == true then
			event.quest.active = false
			event.quest.fulfilled = false
			event.quest.fulfillCount = 0
			event.quest.completeTime = m_currTime
		else
			event.quest.complete = false
			event.quest.active = true
		end
		-- Update an existing or insert a new complete quest
		local duplicateQuest = false
		for index, quest in pairs(m_inventory[QUEST_PID].inventory) do
			if tonumber(event.quest.UNID) and tonumber(quest.UNID) == event.quest.UNID then
				duplicateQuest = true
				--[[for i,v in pairs(event.quest) do
					m_inventory[QUEST_PID].inventory[index][i] = v
				end]]
				m_inventory[QUEST_PID].inventory[index] = event.quest
				-- Send inventory to any client menus that are interested in this data
				updateClientInventoryItem(event.quest.UNID, QUEST_PID, quest)
				break
			end
		end
		if not duplicateQuest then
			event.quest.fulfilled = false
			event.quest.fulfillCount = 0
			table.insert(m_inventory[QUEST_PID].inventory, event.quest)
			updateClientInventoryFull(m_inventory[QUEST_PID].inventory, QUEST_PID)
		end

		-- Track quest acceptance
		local questName = ""
		if m_inventory[GAME_PID] and m_inventory[GAME_PID].inventory and m_inventory[GAME_PID].inventory[tostring(event.quest.UNID)] then
			questName = m_inventory[GAME_PID].inventory[tostring(event.quest.UNID)].name or ""
		end
		
		-- Send new quest down to compass for tracking if no other quests are being tracked
		if event.quest.complete ~= true then
			local trackQuestEvent = KEP_EventCreate("QUEST_COMPASS_TRACK_NEW_QUEST")
			
			KEP_EventEncodeNumber(trackQuestEvent, event.quest.UNID)
			trackQuestEvent = compileInventoryItem(event.quest, trackQuestEvent)
			
			KEP_EventSetFilter(trackQuestEvent, 4)
			KEP_EventQueue(trackQuestEvent)
		end
		
		updateQuestLog()
	end
end

function onCompleteQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = KEP_EventDecodeNumber(tEvent)
	local questItem = nil
	if KEP_EventMoreToDecode(tEvent) == 1 then
		questItem = decompileInventoryItem(tEvent)
	end
	-- Complete quest
	if m_inventory[QUEST_PID].inventory then
		for index, quest in pairs(m_inventory[QUEST_PID].inventory) do
			if tonumber(quest.UNID) and tonumber(quest.UNID) == questUNID then
				quest.complete = true
				quest.active = false
				quest.fulfilled = false
				quest.fulfillCount = 0
				quest.completeTime = m_currTime
				
				updateClientInventoryItem(index, QUEST_PID, quest) -- Update item to interested parties
				updateQuestLog()
				return
			end
		end
	end
	
	if questItem then
		questItem.complete = true
		addQuest(questItem)
	end
end

function onAbandonQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = KEP_EventDecodeNumber(tEvent)
	if m_inventory[QUEST_PID].inventory then
		for index, quest in pairs(m_inventory[QUEST_PID].inventory) do
			if tonumber(quest.UNID) and tonumber(quest.UNID) == questUNID then
				if quest.active then
					if quest.complete then
						quest.active = false
						quest.fulfilled = false
						quest.fulfillCount = 0
						updateClientInventoryItem(index, QUEST_PID, quest)
					else
						table.remove(m_inventory[QUEST_PID].inventory, index)
						updateClientInventoryFull(m_inventory[QUEST_PID].inventory, QUEST_PID)
					end
					updateQuestLog()
				end
				break
			end
		end
	end
end

function onFulfillQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = KEP_EventDecodeNumber(tEvent)
	local questItem = nil
	if KEP_EventMoreToDecode(tEvent) == 1 then
		questItem = decompileInventoryItem(tEvent)
	end
	-- Fulfill quest
	if m_inventory[QUEST_PID].inventory then
		for index, quest in pairs(m_inventory[QUEST_PID].inventory) do
			if tonumber(quest.UNID) and tonumber(quest.UNID) == questUNID then
				quest.fulfilled = true
				updateClientInventoryItem(questUNID, QUEST_PID, quest) -- Update item to interested parties
				updateQuestLog() -- Move this later?
				return
			end
		end
	end

	if questItem then
		questItem.fulfilled = true
	end
end

function onUpdateFulfillCount(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local questUNID = KEP_EventDecodeNumber(tEvent)
	local updateAmt = KEP_EventDecodeNumber(tEvent)
	local questItem = nil
	if KEP_EventMoreToDecode(tEvent) == 1 then
		questItem = decompileInventoryItem(tEvent)
	end

	-- Fulfill quest
	if m_inventory[QUEST_PID].inventory then
		for index, quest in pairs(m_inventory[QUEST_PID].inventory) do
			if tonumber(quest.UNID) and tonumber(quest.UNID) == questUNID then
				quest.fulfillCount = quest.fulfillCount or 0
				quest.fulfillCount = quest.fulfillCount + updateAmt
				-- Check if fulfillCount was just met
				if quest.properties and quest.properties.requiredItem and quest.properties.requiredItem.requiredCount and
					quest.fulfillCount and quest.fulfillCount ~= 0 and
					quest.fulfillCount >= quest.properties.requiredItem.requiredCount and
					quest.properties.questType and usesFulfillCount(quest.properties.questType) then -- Note: "takeTo" quests are ONLY fulfilled on delivery
						quest.fulfilled = true
				end
				updateClientInventoryItem(questUNID, QUEST_PID, quest) -- Update item to interested parties
				updateQuestLog() -- Move this later?
				return
			end
		end
	end
end

function onProcessTransaction(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local removeTable = decompileInventory(tEvent)
	local addTable = decompileInventory(tEvent)	
	local menuName = KEP_EventDecodeString(tEvent)

	local success = false
	
	processTransactionMetadata(removeTable, addTable)
	
	success, removeTable, addTable = processTransactionInternal(removeTable, addTable, false)
	
	if success then
		removeItemsByTable(removeTable, m_inventory, true)
		
		local addItem = false
		for i = 1, #addTable do
			updateInventoryItem(0, INVENTORY_PID, addTable[i], addTable[i].isCancel or false, false, m_inventory, true)
			if addTable[i].count > 0 then
				addItem = true
			end
		end	

		if addItem and menuName ~=  "Framework_Armor.xml" then
			local newItemEvent = KEP_EventCreate("FRAMEWORK_NEW_ITEM_MESSAGE")
			KEP_EventEncodeString(newItemEvent,"Item")
			KEP_EventQueue(newItemEvent)
		end
		
		updateDirty()
	end
	
	local responseEvent = KEP_EventCreate("INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE")
	KEP_EventEncodeString(responseEvent, tostring(success))
	KEP_EventEncodeString(responseEvent, tostring(menuName))
	KEP_EventSetFilter(responseEvent, 4)
	KEP_EventQueue(responseEvent)	
end

function processTransactionMetadata(removeTable, addTable)
	removeTable = removeTable or {}
	addTable = addTable or {}
	
	local transactionID = KEP_GetCurrentEpochTime()
	
	for _, item in pairs(removeTable) do
		if item.lootInfo and item.lootInfo.transactionID == nil then
			item.lootInfo.transactionID = transactionID
		end
	end
	
	for _, item in pairs(addTable) do
		if item.lootInfo and item.lootInfo.transactionID == nil then
			item.lootInfo.transactionID = transactionID
		end
	end
end

function onPreprocessTransaction(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local removeTable = decompileInventory(tEvent)
	local addTable = decompileInventory(tEvent)	
	local menuName = KEP_EventDecodeString(tEvent)

	local success = false
	local successRemove = false
	local successAdd = false
	
	success, _, _, successRemove, successAdd = processTransactionInternal(removeTable, addTable, true)
	
	local responseEvent = KEP_EventCreate("INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE")
	KEP_EventEncodeString(responseEvent, tostring(success))
	KEP_EventEncodeString(responseEvent, tostring(menuName))
	KEP_EventEncodeString(responseEvent, tostring(successRemove))
	KEP_EventEncodeString(responseEvent, tostring(successAdd))
	KEP_EventSetFilter(responseEvent, 4)
	KEP_EventQueue(responseEvent)	
end

function processTransactionInternal(removeTable, addTable, preproccess)
	local inventoryTemp = deepCopy(m_inventory)
	
	local success = validateStacks(addTable)
	local successRemove = success
	local successAdd = success
	
	if success and not removeItemsByTable(removeTable, inventoryTemp, false) then
		success = false
		successRemove = false
		if not preproccess then
			displayStatusMessage(INVENTORY_ERRORS.MISSING_ITEM)
		end
	elseif not success then
		if not preproccess then
			displayStatusMessage(INVENTORY_ERRORS.BACKPACK_FULL)
		end
	end
	
	if success then
		for i, v in pairs (addTable) do
			addTable[i].UNID = tonumber(addTable[i].UNID)
			if addTable[i].UNID ~= 0 then
				local insertIndex = getBestInsertSlot(INVENTORY_PID, addTable[i], inventoryTemp)
				if insertIndex == 0 then
					success = false
					successAdd = false
					if not preproccess then
						displayStatusMessage(INVENTORY_ERRORS.BACKPACK_FULL)
					end
					break
				else
					updateInventoryItem(insertIndex, addTable[i].inventoryPID, addTable[i], addTable[i].isCancel or false, false, inventoryTemp, false)
				end
			end 
		end
	end
	
	return success, removeTable, addTable, successRemove, successAdd
end

function removeItemsByTable(removeTable, inventoryTable, immediate)
	for i = 1, #removeTable do
		local removeUNID = tonumber(removeTable[i].UNID)
		local removeCount = tonumber(removeTable[i].count)
		local lootInfo = removeTable[i].lootInfo
		
		for j = #inventoryTable[INVENTORY_PID].inventory, 1 , -1 do
			if inventoryTable[INVENTORY_PID].inventory[j].UNID == removeUNID then
				if tablePopulated(inventoryTable[INVENTORY_PID].inventory[j].instanceData) then
					removeItemInternal(j, INVENTORY_PID, inventoryTable, immediate, lootInfo)
					removeCount = removeCount - 1
				else
					-- Decrement as many of this UNID as you can
					local tempCount = inventoryTable[INVENTORY_PID].inventory[j].count
					inventoryTable[INVENTORY_PID].inventory[j].count = inventoryTable[INVENTORY_PID].inventory[j].count - removeCount
					if inventoryTable[INVENTORY_PID].inventory[j].count <= 0 then
						inventoryTable[INVENTORY_PID].inventory[j].count = tempCount
						removeItemInternal(j, INVENTORY_PID, inventoryTable, immediate, lootInfo)
					else
						updateInventoryItem(j, INVENTORY_PID, inventoryTable[INVENTORY_PID].inventory[j], false, false, inventoryTable, immediate)
						if immediate then
							if lootInfo then
								local input = {itemUNID = removeUNID, itemCount = -removeCount, sourcePID = lootInfo.sourcePID or INVENTORY_PID, lootSource = lootInfo.lootSource, recipeGIID = lootInfo.recipeGIID, questUNID = lootInfo.questUNID, lootSlot = lootInfo.sourceSlot, transactionID = lootInfo.transactionID}
								Events.sendEvent("FRAMEWORK_LOOT_ACQUIRED", input)
							end
						end
					end
					removeCount = removeCount - tempCount
				end
				if removeCount <= 0 then break end
			end
		end
		
		if removeCount > 0 then
			return false
		end
	end
	
	return true
end

function validateStacks(itemTable, startIndex)
	if startIndex == nil then startIndex = 1 end
	for i = startIndex, #itemTable do
		local instanced = isInstancedItem(itemTable[i].properties.itemType)
		if (not instanced and (itemTable[i].count > (itemTable[i].properties.stackSize or DEFAULT_STACK_SIZE))) or
           (instanced and (itemTable[i].count > 1)) then
			local stackSize = itemTable[i].properties.stackSize or DEFAULT_STACK_SIZE
			if tablePopulated(itemTable[i].instanceData) then
				stackSize = 1
			end
			local totalCount = itemTable[i].count
			itemTable[i].count = stackSize
			local newStack = deepCopy(itemTable[i])
			newStack.count = totalCount - stackSize
			table.insert(itemTable, (i+1), newStack)
			if #itemTable > MAX_INVENTORY_SIZE then
				return false
			else
				return validateStacks(itemTable, i)
			end
		end
	end
	
	return true
end

-- Event handler when an item is added or updated in inventory on the client side
-- Framework_InventoryHelper - updateItem(<inventory slot>, <inventory PID>, <item>)
function onAddItem(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local event = {} -- Build a fake Framework-style event from client to client event
	
	event.index = KEP_EventDecodeNumber(tEvent) -- Slot index of this item
	event.pid = KEP_EventDecodeNumber(tEvent) -- PID of the affected inventory
	event.item = decompileInventoryItem(tEvent) -- Inventory item
	
	event.isCancel = false
	if KEP_EventMoreToDecode(tEvent) == 1 then
		event.isCancel = KEP_EventDecodeString(tEvent) == "true"
	end
	
	updateInventoryItem(event.index, event.pid, event.item, event.isCancel, false, m_inventory, true)
end

-- Event handler when an item is added or updated in inventory on the client side, but does not need to notify the server
-- Framework_InventoryHelper - updateItemLocal(<inventory slot>, <inventory PID>, <item>)
function onAddItemLocal(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local event = {} -- Build a fake Framework-style event from client to client event
	
	event.index = KEP_EventDecodeNumber(tEvent) -- Slot index of this item
	event.pid = KEP_EventDecodeNumber(tEvent) -- PID of the affected inventory
	event.item = decompileInventoryItem(tEvent) -- Inventory item
	
	event.isCancel = false
	if KEP_EventMoreToDecode(tEvent) == 1 then
		event.isCancel = KEP_EventDecodeString(tEvent) == "true"
	end
	
	updateInventoryItem(event.index, event.pid, event.item, event.isCancel, true, m_inventory, true)
end

function updateInventoryItem(index, PID, item, isCancel, isLocal, inventoryTable, immediate)

	local event = {index = index, pid = PID, item = item, isCancel = isCancel, isLocal = isLocal, inventoryTable = inventoryTable or m_inventory, immediate = immediate}

	if event.immediate or event.inventoryTable == nil then event.inventoryTable = m_inventory end
	
	if event.pid == nil or event.pid == 0 then event.pid = INVENTORY_PID end -- Default to main inventory
	
	-- If this is the blueprint inventory, forward to blueprint handler function
	if event.pid == BLUEPRINT_PID then
		onAddBlueprint(event.item)
		return
	elseif event.pid == QUEST_PID then
		log("ERROR: API not for use with quest items.")
		return
	end
	
	if (event.item.properties.itemType == ITEM_TYPES.WEAPON or event.item.properties.itemType == ITEM_TYPES.ARMOR or item.properties.itemType == ITEM_TYPES.TOOL) and (event.item.instanceData == nil or event.item.instanceData.durability == nil) then
		local durability = getMaxDurabilty(event.item)
		event.item.instanceData = {levelMultiplier = getLevelMultiplier(), durability = durability}
	end
	
	if (event.item.lootSource) then
		log("updateInventoryItem - Got an item with an old loot source: "..tostring(event.item.name))
	end
	event.item.lootSource = nil --strip old-style loot events
	
	if event.immediate then
		local origCount = nil
		if (tablePopulated(item.instanceData) and item.count ~= 1) then
			origCount = item.count
			item.count = 1
		elseif (item.count > (item.properties.stackSize or DEFAULT_STACK_SIZE)) then
			origCount = item.count
			item.count = item.properties.stackSize or DEFAULT_STACK_SIZE
		end
		local lootInfo = event.item.lootInfo
		if lootInfo and (event.pid == INVENTORY_PID or event.pid == ARMORY_PID) then
			local sourcePID = lootInfo.sourcePID or event.pid
			local lootSource = lootInfo.lootSource
			local recipeGIID = lootInfo.recipeGIID
			if sourcePID and event.item.count and event.item.count ~= 0 then
				Events.sendEvent("FRAMEWORK_LOOT_ACQUIRED", {itemUNID = event.item.UNID, itemCount = event.item.count, sourcePID = sourcePID, lootSource = lootSource, recipeGIID = recipeGIID, origCount = origCount, lootSlot = lootInfo.sourceSlot, transactionID = lootInfo.transactionID})
				if lootSource == "Crafting" then
					local ev = KEP_EventCreate("QUEST_HANDLER_ITEM_CRAFTED")
					KEP_EventEncodeNumber(ev, tonumber(event.item.UNID))
					KEP_EventEncodeNumber(ev, tonumber(event.item.count))
					KEP_EventQueue(ev)
				end
			end
		end
		event.item.lootInfo = nil
	end		
	
	-- Calculate damage / armor rating metadata from item level and item instance multiplier if this is a weapon or armor
	if event.item.properties.itemType == ITEM_TYPES.WEAPON and event.item.properties.damage == nil then
		event.item.properties.damage = calculateWeaponDamage(event.item.properties.level, event.item.instanceData.levelMultiplier)
	elseif event.item.properties.itemType == ITEM_TYPES.ARMOR and event.item.properties.armorRating == nil then
		event.item.properties.armorRating = calculateArmorRating(event.item.properties.level, event.item.instanceData.levelMultiplier)
	end
	
	-- Slot index of 0 means to find the best spot for this item in main inventory
	-- TODO: Should this enforce the main inventory PID only?
	if event.index <= 0 then
		-- Assign the index to the best slot for this item
		event.index = getBestInsertSlot(event.pid, event.item)
		-- If this item is stacking with another, increment the count
		if event.inventoryTable[event.pid].inventory[event.index] and event.inventoryTable[event.pid].inventory[event.index].UNID == event.item.UNID and event.immediate then
			event.item.count = event.item.count + event.inventoryTable[event.pid].inventory[event.index].count
		end
		
		if event.item.count and (event.item.count > (event.item.properties.stackSize or DEFAULT_STACK_SIZE)) then
			event.item.count = event.item.properties.stackSize or DEFAULT_STACK_SIZE
		end
	end
	
	-- TODO: Cross-reference and update related quest inventory here!
	
	-- If everything is valid, update the item, mark as dirty, and send new item data to other client menus
	if event.inventoryTable[event.pid] and event.index > 0 then
		if event.isCancel and event.inventoryTable[event.pid].inventory[event.index].UNID == event.item.UNID then
			local dragCount = event.item.count
			event.item = event.inventoryTable[event.pid].inventory[event.index]
			event.item.count = event.item.count + dragCount
		end
		event.inventoryTable[event.pid].inventory[event.index] = event.item -- Update the item in cache
		
		if event.immediate then
			if not event.isLocal then
				table.insert(event.inventoryTable[event.pid].dirtySlots, event.index) -- Add to dirty inventory
			end
			updateClientInventoryItem(event.index, event.pid, event.item) -- Update item to interested parties
		end
	else -- An invalid inventory or slot, log an error
		log("Got add request for invalid inventory - PID = "..tostring(event.pid))
	end
end

-- Event handler when an item is removed from inventory on the client side
-- Framework_InventoryHelper - removeItem(<inventory index>, <inventory PID>)
function onRemoveItem(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local event = {} -- Build a fake Framework-style event from client to client event
	
	event.index = KEP_EventDecodeNumber(tEvent) -- Slot index of this item
	event.pid = KEP_EventDecodeNumber(tEvent) -- PID of the affected inventory
	event.lootInfo = decompileInventory(tEvent)
	
	removeItemInternal(event.index, event.pid, m_inventory, true, event.lootInfo)
end

function removeItemInternal(index, pid, inventoryTable, immediate, lootInfo)
	local item = EMPTY_ITEM -- When an item is removed, an empty item is placed in the slot
	if lootInfo == nil then lootInfo = {} end
	
	if pid == nil then pid = INVENTORY_PID end -- Default to main inventory
	
	-- If this is the blueprint inventory, forward to blueprint handler function
	if pid == BLUEPRINT_PID then
		onRemoveBlueprint(index)
		return
	elseif pid == QUEST_PID then
		log("ERROR: API not for use with quest items.")
		return
	end
	
	if inventoryTable == nil or immediate then inventoryTable = m_inventory end
	
	-- If the item exists in cache, update the slot
	if inventoryTable[pid] then
		local replacedUNID = inventoryTable[pid].inventory[index].UNID
		
		if immediate then
			local item = inventoryTable[pid].inventory[index]
			local origCount = nil
			if (tablePopulated(item.instanceData) and item.count ~= 1) then
				origCount = item.count
				item.count = 1
			elseif (item.count > (item.properties.stackSize or DEFAULT_STACK_SIZE)) then
				origCount = item.count
				item.count = item.properties.stackSize or DEFAULT_STACK_SIZE
			end
			if pid == INVENTORY_PID or pid == ARMORY_PID then
				if item.count and item.count ~= 0 then
					local input = {itemUNID = item.UNID, itemCount = -item.count, sourcePID = lootInfo.sourcePID, lootSource = lootInfo.lootSource,  questUNID = lootInfo.questUNID, origCount = origCount, sourceSlot = lootInfo.sourceSlot, transactionID = lootInfo.transactionID}
					Events.sendEvent("FRAMEWORK_LOOT_ACQUIRED", input)
				end
			end
		end			
		
		inventoryTable[pid].inventory[index] = item -- Set item at slot to the empty item
		
		if immediate then
			table.insert(inventoryTable[pid].dirtySlots, index) -- Mark slot as dirty for server updating
			updateClientInventoryItem(index, pid, item) -- Update item to interested parties
		end
	else -- An invalid inventory or slot, log an error
		log("Got remove request for invalid inventory - PID = "..tostring(pid))
	end	
end

-- Event handler when a blueprint is added or updated in inventory on the client side
-- Forwarded from onAddItem if the affected inventory is the blueprint inventory
function onAddBlueprint(newBlueprint)
	local inInventory = false -- Is this blueprint already in inventory?
	-- Iterate over blueprint cache to see if the player already owns a copy of this blueprint
	for index, blueprint in pairs(m_inventory[BLUEPRINT_PID].inventory) do
		if newBlueprint.UNID == blueprint.UNID then -- Is this the blueprint UNID in question?
			-- If this is a single use blueprint, increment the count
			if newBlueprint.properties.oneUse == "true" then
				blueprint.count = blueprint.count + 1
			end
			table.insert(m_inventory[BLUEPRINT_PID].dirtySlots, index) -- Mark slot as dirty
			inInventory = true -- Blueprint was found
			break -- Our work in here is done!
		end
	end
	-- If the blueprint was not found in inventory, we need to add it to the indexed table
	if not inInventory then
		-- Single use blueprints have a starting count of 1
		if newBlueprint.properties.oneUse == "true" then newBlueprint.count = 1 
													 else newBlueprint.count = -1 end

		table.insert(m_inventory[BLUEPRINT_PID].inventory, newBlueprint) -- Insert into indexed table
		table.insert(m_inventory[BLUEPRINT_PID].dirtySlots, #m_inventory[BLUEPRINT_PID].inventory) -- Mark slot as dirty
	end
	-- Send inventory to any client menus that are interested in this data
	updateClientInventoryFull(m_inventory[BLUEPRINT_PID].inventory, BLUEPRINT_PID)
end

-- Event handler when a blueprint is removed from inventory on the client side
-- Should only be called for single use blueprints
-- Forwarded from onRemoveItem if the affected inventory is the blueprint inventory
function onRemoveBlueprint(index)
	if m_inventory[BLUEPRINT_PID].inventory[index] then -- Is there a blueprint at this index?
		if m_inventory[BLUEPRINT_PID].inventory[index].count <= 1 then -- If this is the last copy of this blueprint, remove it from the inventory
			table.remove(m_inventory[BLUEPRINT_PID].inventory, index)
			table.insert(m_inventory[BLUEPRINT_PID].dirtySlots, -index) -- A negative index means the blueprint was removed
		else -- Otherwise, decrement the count by one
			m_inventory[BLUEPRINT_PID].inventory[index].count = m_inventory[BLUEPRINT_PID].inventory[index].count - 1
			table.insert(m_inventory[BLUEPRINT_PID].dirtySlots, index)
		end
		
		-- TODO: Do we need to align the dirty slots table here for added items?
		
		-- Send inventory to any client menus that are interested in this data
		updateClientInventoryFull(m_inventory[BLUEPRINT_PID].inventory, BLUEPRINT_PID)
	end
end

-- Sends updated quest log information to the server
-- For now, this is sending the raw complete / incomplete array data
function updateQuestLog()
	if m_inventory[QUEST_PID] == nil or m_inventory[QUEST_PID].inventory == nil then return end
	local questsComplete = {}
	local questsIncomplete = {}
	
	for index, quest in pairs(m_inventory[QUEST_PID].inventory) do
		if quest.complete then
			table.insert(questsComplete, {UNID = tonumber(quest.UNID),
										  completeTime = quest.completeTime})
		else
			table.insert(questsIncomplete, {UNID = tonumber(quest.UNID),
										  fulfilled = quest.fulfilled or false,
										  fulfillCount = quest.fulfillCount or 0})
		end
	end
	
	Events.sendEvent("UPDATE_PLAYER_QUEST_DATA", {playerName = m_playerName, questsComplete = questsComplete, questsIncomplete = questsIncomplete})
end

-- getBestInsertSlot - Attempt to stack items with like items, if none exist then attempt to find the first open slot
-- Returns the index of the found slot or index 0 if no suitable slots were found
function getBestInsertSlot(pid, insertItem, inventoryTable)
	if inventoryTable == nil then inventoryTable = m_inventory end
	local bestSlot = 0 -- The index for our slot
	for index, item in pairs(inventoryTable[pid].inventory) do -- Iterate over the inventory indicated
		if item.UNID == insertItem.UNID and (item.count + insertItem.count <= (item.properties.stackSize or DEFAULT_STACK_SIZE)) and tablePopulated(item.instanceData) == false then
			return index -- If this item fits on a stack of like items, then return that as the index
		elseif index > TOOLBAR_SLOTS and (item.UNID == 0 or item.count == 0) and bestSlot == 0 then
			bestSlot = index -- Keep track of the first empty slot, in the case that this item does not stack
		end
	end
	-- Return the slot found or 0
	return bestSlot
end

-- Send an event to a registered menu with the compiled item at the given slot
function updateClientInventoryItem(index, pid, item)

	local updateEvent = nil
	if pid > INVENTORY_PID then -- PIDs greater than 0 are world objects with inventories (item containers and vendors)
		updateEvent = KEP_EventCreate("UPDATE_CONTAINER_CLIENT")
		KEP_EventEncodeNumber(updateEvent, pid) -- World object inventories include the PID of the object in question
	elseif pid == ARMORY_PID then -- Send an armory update event if this is the armory
		updateEvent = KEP_EventCreate("UPDATE_ARMORY_CLIENT")
	elseif pid == INVENTORY_PID then -- Send a main inventory update event if this is the main inventory
		updateEvent = KEP_EventCreate("UPDATE_INVENTORY_CLIENT")
		if(item.properties) then
			if(item.properties.name ~= "EMPTY" and item.properties and item.properties.glow ~= false) then
				item.properties.glow = true
			else
				item.properties.glow = false
			end
		end
	elseif pid == QUEST_PID then
		updateEvent = KEP_EventCreate("UPDATE_QUEST_CLIENT")
	elseif pid == GAME_PID then
		updateEvent = KEP_EventCreate("UPDATE_GAME_CLIENT")
	end
	
	if updateEvent then -- We have a valid event
		KEP_EventEncodeNumber(updateEvent, index) -- The item slot being updated
		
		updateEvent = compileInventoryItem(item, updateEvent) -- The item being updated
		
		if updateEvent then -- Event is still valid after attaching item
			KEP_EventSetFilter(updateEvent, 4)
			KEP_EventQueue(updateEvent) -- Send to registered menus
		end
	end
end

-- Send an event to a registered menu with the compiled inventory
function updateClientInventoryFull(inventoryTable, pid, isVendor)

	local updateEvent = nil
	if pid > INVENTORY_PID then -- PIDs greater than 0 are world objects with inventories (item containers and vendors)
		if isVendor then
			updateEvent = KEP_EventCreate("UPDATE_VENDOR_CLIENT_FULL") -- sent to Framework_ItemContainer > updateVendorClientFull
		else
			updateEvent = KEP_EventCreate("UPDATE_CONTAINER_CLIENT_FULL") -- sent to Framework_ItemContainer > updateContainerClientFull
		end
		KEP_EventEncodeNumber(updateEvent, pid) -- World object inventories include the PID of the object in question
	elseif pid == ARMORY_PID then -- Send an armory update event if this is the armory
		updateEvent = KEP_EventCreate("UPDATE_ARMORY_CLIENT_FULL")
	elseif pid == INVENTORY_PID then -- Send a main inventory update event if this is the main inventory
		updateEvent = KEP_EventCreate("UPDATE_INVENTORY_CLIENT_FULL")
	elseif pid == BLUEPRINT_PID then -- Send a blueprint update event if this is the blueprint inventory
		updateEvent = KEP_EventCreate("UPDATE_BLUEPRINTS_CLIENT_FULL")
	elseif pid == QUEST_PID then -- Send a quest update event if this is the quest inventory
		updateEvent = KEP_EventCreate("UPDATE_QUEST_CLIENT_FULL")
	elseif pid == GAME_PID then
		updateEvent = KEP_EventCreate("UPDATE_GAME_CLIENT_FULL")
	end
	
	if updateEvent then  -- We have a valid event
		updateEvent = compileInventory(inventoryTable, updateEvent, inventorySize) -- The full inventory being updated
	
		if updateEvent then -- Event is still valid after attaching inventory
			KEP_EventSetFilter(updateEvent, 4)
			KEP_EventQueue(updateEvent) -- Send to registered menus
		end
	end
end

-- Event handler to process all dirty inventory slots
-- Framework_InventoryHelper - updateDirty()
function onUpdateDirtyInventory(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	for pid, data in pairs(m_inventory) do -- Iterate over all know inventories
		local updatedSlots = {} -- Temporary table to hold updated slot information for sending
		local questUpdated = {}
		-- TODO: In testing, the dirtySlots table for an unknown inventory was nil.  We need to find out what triggered that event.

		if data.dirtySlots and #data.dirtySlots > 0 then -- If this inventory has dirty, dirty slots
			if pid == BLUEPRINT_PID then -- The blueprint inventory is treated differently, as they it is indexed tables
				for i = 1, #data.dirtySlots do -- Iterate over blueprint dirty slots
					local index = data.dirtySlots[i]
					local slotInfo = {index = index}
					if index > 0 then -- Indices greater than 0 mean add / update as opposed to remove and contain item data
						slotInfo.item = stripItemForSend(data.inventory[index]) -- Only send item data unique to this client
					end
					table.insert(updatedSlots, slotInfo) -- Add index information to updatedSlots table
				end
				-- Send updated slot information for the blueprint inventory to the server
				if pid == BLUEPRINT_PID then
					Events.sendEvent("UPDATE_PLAYER_BLUEPRINT_SLOTS", {playerName = m_playerName, updatedSlots = updatedSlots})
				end
				data.dirtySlots = {} -- Clear out dirty slots table for this inventory
			else -- Inventory is not blueprints
				for i = 1, #data.dirtySlots do -- Iterate over inventory dirty slots
					local index = data.dirtySlots[i]
					
					if m_inventory[QUEST_PID] == nil then
						log("ERROR DETECTED - ED-5303 : m_inventory[QUEST_PID] = "..tostring(m_inventory[QUEST_PID]))
					else
						-- Evaluate slot for potential quest update ramifications
						for _, quest in pairs(m_inventory[QUEST_PID].inventory) do
							if tonumber(quest.UNID) and questUpdated[quest.UNID] == nil and quest.properties and quest.properties.requiredItem and quest.properties.requiredItem.requiredUNID and
							(tonumber(quest.properties.requiredItem.requiredUNID) == tonumber(data.inventory[index].UNID) or (pid == INVENTORY_PID and tonumber(data.inventory[index].UNID) == 0)) then
								questUpdated[quest.UNID] = true
								updateQuestProgress(quest, true)
							end
						end
					end

					local slotInfo = {index = index, item = stripItemForSend(data.inventory[index])} -- Format index and item information for sending to the server
					table.insert(updatedSlots, slotInfo) -- Add index information to updatedSlots table
				end
				
				if pid == INVENTORY_PID then
				
					Events.sendEvent("UPDATE_PLAYER_INVENTORY_SLOTS", {playerName = m_playerName, updatedSlots = updatedSlots}) -- Update server main inventory
				elseif pid == ARMORY_PID then
					Events.sendEvent("UPDATE_PLAYER_ARMORY_SLOTS", {playerName = m_playerName, updatedSlots = updatedSlots}) -- Update server armory
				elseif pid > INVENTORY_PID then
					Events.sendEvent("UPDATE_CONTAINER_ITEM_SLOTS", {PID = pid, updatedSlots = updatedSlots}) -- Update item container inventory
				end
			end
			data.dirtySlots = {} -- Clear out dirty slots table for this inventory
		elseif data.dirtySlots == nil and pid ~= QUEST_PID and pid ~= GAME_PID then -- Log out error.  This should never be the case!
			log("Inventory with PID = "..tostring(pid).." had a nil dirtySlots table!")
			data.dirtySlots = {} -- Clear out dirty slots table for this inventory
		end
	end
end

-- Removes properties table to send back to server
function stripItemForSend(item)
	local strippedItem = {}
	strippedItem.UNID = item.UNID
	strippedItem.count = item.count
	strippedItem.instanceData = item.instanceData
	
	return strippedItem
end

-- Send a given inventory to an interested client menu.
-- Checks if inventory exists if local cache and returns that, if so.  Otherwise sends an event to the server requesting the inventory in question.
-- Framework_InventoryHelper - requestInventory(<inventory PID>)
function onInventoryRequest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local PID = KEP_EventDecodeNumber(tEvent) -- The PID of the inventory to send
	
	if PID > INVENTORY_PID then -- World object inventories send data when the menu is opened
		return --Nothing to do here!
	elseif PID == ARMORY_PID then -- Armory
		if m_inventory[ARMORY_PID] == nil and not m_initialRequest then
			-- No inventory cached, ask the server
			Events.sendEvent("REQUEST_PLAYER_INVENTORY", {playerName = m_playerName})
		else
			-- Send inventory to any client menus that are interested in this data
			updateClientInventoryFull(m_inventory[ARMORY_PID].inventory, ARMORY_PID)
		end
	elseif PID == INVENTORY_PID and not m_initialRequest then -- Main Inventory
		if m_inventory[INVENTORY_PID] == nil then
			-- No inventory cached, ask the server
			Events.sendEvent("REQUEST_PLAYER_INVENTORY", {playerName = m_playerName})
		else
			-- Send inventory to any client menus that are interested in this data
			updateClientInventoryFull(m_inventory[INVENTORY_PID].inventory, INVENTORY_PID)
		end
	elseif PID == BLUEPRINT_PID and not m_initialRequest then -- Blueprint Inventory
		if m_inventory[BLUEPRINT_PID] == nil then
			-- No inventory cached, ask the server
			Events.sendEvent("REQUEST_PLAYER_INVENTORY", {playerName = m_playerName})
		else
			-- Send inventory to any client menus that are interested in this data
			updateClientInventoryFull(m_inventory[BLUEPRINT_PID].inventory, BLUEPRINT_PID)
		end
	elseif PID == QUEST_PID and m_processingQuestItems == false and not m_initialRequest and not m_requestQuests then -- Quest Inventory
		if m_inventory[QUEST_PID] == nil then
			-- No inventory cached, ask the server
			m_processingQuestItems = true
			Events.sendEvent("REQUEST_PLAYER_QUESTS", {playerName = m_playerName})
		else
			-- Send inventory to any client menus that are interested in this data
			updateClientInventoryFull(m_inventory[QUEST_PID].inventory, QUEST_PID)
		end
	elseif PID == GAME_PID and m_processingGameItems == false then
		if m_inventory[GAME_PID] == nil then
			-- No inventory cached, ask the server
			m_processingGameItems = true
			Events.sendEvent("GET_ALL_GAME_ITEMS")
		else
			-- Send inventory to any client menus that are interested in this data
			updateClientInventoryFull(m_inventory[GAME_PID].inventory, GAME_PID)
		end		
	end
end

-- Event handler when checking to see if an item will fit into a given inventory before attempting to add anything
-- TODO: Should this be wrapped in the helper and utilized in more places? (crafting etc.)
function onInventoryCheckLoot(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local checkPID = KEP_EventDecodeNumber(tEvent) or INVENTORY_PID -- Inventory PID to check against
	local checkItem = decompileInventoryItem(tEvent) -- Item to check against
	
	local checkResponse = (getBestInsertSlot(checkPID, checkItem) ~= 0) -- This check will return 0 if no available slots were found
	
	-- Send response to script.  TODO: Does this need to only respond to querying menu somehow?
	local responseEvent = KEP_EventCreate("INVENTORY_HANDLER_CHECK_LOOT_RESPONSE")
	KEP_EventEncodeString(responseEvent, tostring(checkResponse))
	KEP_EventSetFilter(responseEvent, 4)
	KEP_EventQueue(responseEvent)	
end

-- Response from drag / drop handler from event sent on player death
-- Contains the item being dragged (if it exists) and compiles all player inventories into a table before spawning a chest with those items inside and clearing player inventories
function onCancelDragDrop(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- Only track deaths for drop items on death worlds
	if m_dropItems == true then
		Events.sendEvent("FRAMEWORK_INCREMENT_DEATH_COUNT")
		m_deathCount = m_deathCount + 1
	end

	if MenuIsOpen("UnifiedInventory") then
		MenuClose("UnifiedInventory")
	end
	if m_dropItems == true and m_deathCount >= 2 then -- Respect world setting
		-- Death second time warning
		if m_deathCount == 2 then
			MenuOpen("Framework_DeathWarn.xml")
			local event = KEP_EventCreate( "FRAMEWORK_SEND_DEATH_COUNT" )
			KEP_EventEncodeNumber(event, m_deathCount)
			KEP_EventQueue(event)
		end
		local dropInventory = {} -- Temporary table of items to be dropped on death

		local dragDropItem = nil -- Potential item being dragged
		
		if tEvent and KEP_EventMoreToDecode(tEvent) ~= 0 then
			dragDropItem = decompileInventoryItem(tEvent) -- Item being dragged, to be placed on corpse
		end
		
		local transactionID = KEP_GetCurrentEpochTime()
		local sourcePID = KEP_GetLoginName()
		-- Traverse the armory table, adding all items (except empty) to the drop table and clearing out those slots
		for index, item in pairs(m_inventory[ARMORY_PID].inventory) do
			if item.UNID > 0 and item.count > 0 then
				local dropItem = {UNID = item.UNID, count = item.count, instanceData = item.instanceData}
				table.insert(dropInventory, dropItem)
				removeItem(index, ARMORY_PID, {sourcePID = sourcePID, lootSource = "Death", transactionID = transactionID})
			end
		end
		
		-- Traverse the main inventory table, adding all items (except empty) to the drop table and clearing out those slots
		for index, item in pairs(m_inventory[INVENTORY_PID].inventory) do
			if item.UNID > 0 and item.count > 0 then
				table.insert(dropInventory, {UNID = item.UNID, count = item.count, instanceData = item.instanceData})
				removeItem(index, INVENTORY_PID, {sourcePID = sourcePID, lootSource = "Death", transactionID = transactionID})
			end		
		end
		
		if dragDropItem then -- Add drag / drop item to table, if it exists
			table.insert(dropInventory, {UNID = dragDropItem.UNID, count = dragDropItem.count, instanceData = dragDropItem.instanceData})
		end
		
		updateDirty() -- Send new (empty) inventories to server
		
		if #dropInventory > 0 then -- Are we dropping any items on the ground?
			-- Spawn an item container object with the items found above at the location the player died
			local spawnInput = {items = dropInventory, despawnTime = DEFAULT_DESPAWN_TIME, isCorpse = true, corpseOwner = m_playerName}
			Events.sendEvent("OBJECT_SPAWN", {spawnerPID = 0, spawnType = "loot", GLID = DEFAULT_DROP_CONTAINER, location = m_dropLocation, spawnInput = spawnInput})
		end
	else
		-- Death first time warning
		if m_dropItems == true and m_deathCount == 1 then
			MenuOpen("Framework_DeathWarn.xml")
			local event = KEP_EventCreate( "FRAMEWORK_SEND_DEATH_COUNT" )
			KEP_EventEncodeNumber(event, m_deathCount)
			KEP_EventQueue(event)
		end
		local deadPlayer = {loot = {weapon = true, tool = true, consumable = true, ammo = true, blueprint = true, generic = true}, level = 1, spawnGLID = DEFAULT_DROP_CONTAINER, position = m_dropLocation, noCollision = true}
		-- Drop random loot
		Events.sendEvent("GENERATE_LOOT_CHEST", {object = deadPlayer, despawnTime = 300, canDropGemBox = true})

		-- If a drag / drop item exists, make sure it gets added back to the player inventory
		local dragDropItem = nil -- Potential item being dragged
		if tEvent and KEP_EventMoreToDecode(tEvent) ~= 0 then
			dragDropItem = decompileInventoryItem(tEvent) -- Item being dragged
		end
		if dragDropItem then
			local addTable = {}
			table.insert(addTable, dragDropItem)
			processTransaction({}, addTable)
		end
	end

	m_dropLocation = nil -- Clear our saved drop location
end

function onRequestItemName(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local UNID = KEP_EventDecodeNumber(tEvent)
	
	local itemName = ""
	
	if m_inventory[GAME_PID] and m_inventory[GAME_PID].inventory and m_inventory[GAME_PID].inventory[tostring(UNID)] then
		itemName = m_inventory[GAME_PID].inventory[tostring(UNID)].name or ""
	end
	
	local responseEvent = KEP_EventCreate("INVENTORY_HANDLER_RETURN_ITEM_NAME")
	KEP_EventEncodeNumber(responseEvent, UNID)
	KEP_EventEncodeString(responseEvent, tostring(itemName))
	KEP_EventSetFilter(responseEvent, 4)
	KEP_EventQueue(responseEvent)	
end

function updateQuestProgress(quest, updateIcons)
	if quest == nil or quest.active == false then return end -- quest exists & is active
	local progressCount = 0
	local fulfilled = false
	local fulfillCount = 0
	
	if  quest.properties and quest.properties.questType then -- quest properties & type exist
		if quest.properties.questType == "collect" then -- Collection quest
			if quest.properties.requiredItem and -- requiredUNID & requiredCount exist
			quest.properties.requiredItem.requiredUNID and
			tonumber(quest.properties.requiredItem.requiredUNID) > 0 and
			quest.properties.requiredItem.requiredCount and
			tonumber(quest.properties.requiredItem.requiredCount) > 0 then
				for slot, item in pairs(m_inventory[INVENTORY_PID].inventory) do
					if tonumber(item.UNID) == tonumber(quest.properties.requiredItem.requiredUNID) then
						local itemCount = item.count or 1
						if tablePopulated(item.instanceData) then itemCount = 1 end
						progressCount = progressCount + itemCount
					end
				end
				quest.properties.requiredItem.progressCount = progressCount
			end
		else -- All other quest types
			if quest.fulfilled then
				fulfilled = quest.fulfilled
			end
			-- if quest.fulfillCount then
			-- 	fulfillCount = quest.fulfillCount
			-- end
		end
		
		updateClientInventoryItem(tonumber(quest.UNID), QUEST_PID, quest)
		Events.sendEvent("UPDATE_PLAYER_QUEST_PROGRESS", {questUNID = tonumber(quest.UNID), progressCount = progressCount, updateIcons = updateIcons, fulfilled = fulfilled})
	end
end

function getLevelMultiplier(levelMultiplier)
	levelMultiplier = levelMultiplier or math.random()
	levelMultiplier = math.ceil(levelMultiplier*10000)*0.0001
	return levelMultiplier
end

-- Generates a random durability
function getDurability()
	local durability = math.max(math.floor(math.random(DEFAULT_MAX_DURABILITY)), DEFAULT_MIN_DURABILITY)
	return durability
end

-- Generates durability based on level
function getDurabilityByLevel(level)
	local durability = DEFAULT_MAX_DURABILITY
	if level == 1 then
		durability = 10
	elseif level == 2 then
		durability = 100
	elseif level == 3 then
		durability = 250
	elseif level == 4 then
		durability = 500
	elseif level == 5 then
		durability = 1000
	end
	return durability
end

function getMaxDurabilty(item)
	local durability = DEFAULT_MAX_DURABILITY
	local durabilityType = item.properties.durabilityType or "repairable"

	if durabilityType == "limited" then
		durability = getDurabilityByLevel(item.properties.level)
	elseif durabilityType == "indestructible" then
		durability = 100
	elseif item.randomizeDurability then
		durability = getDurability()
	end
	return durability
end

-- Should we be tracking a pending completion quest?
function setActiveCompleteTimer()
	m_activeCompleteTimer = {}
	if m_inventory[QUEST_PID] and m_inventory[QUEST_PID].inventory and m_currTime > 0 then
		for index, quest in pairs(m_inventory[QUEST_PID].inventory) do
			if quest.complete and quest.completeTime and (quest.completeTime + ((quest.properties.repeatTime or 0) * 60) > m_currTime) then
				m_activeCompleteTimer[quest.UNID] = true
			end
		end
	end
end

function isInstancedItem(itemType)
	if (itemType == ITEM_TYPES.WEAPON or itemType == ITEM_TYPES.ARMOR or itemType == ITEM_TYPES.TOOL) then
		return true
	end
	
	return false
end