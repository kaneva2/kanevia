--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_QuestTurnIn.lua
--
-- Displays info for a completeable quest
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")

-- Constants
local IMAGE_SIZE = 106

local BUTTON_QUESTS_X = 180
local BUTTON_OKAY_X = 282

-- Local
local m_questInfo = {}
local m_gameItems = {}

local m_bGameItemsInitialized = false
local m_bQuestInfoInitialized = false
local m_actorPID = nil
m_updatedQuestsPending = false -- Should the QuestGiver menu delay showing this quest again (in case we need to run some stuff by the server first)

-- Called when the menu is created
function onCreate()
	-- Register client-client event handlers
	KEP_EventRegisterHandler("onQuestInfoHandler", "FRAMEWORK_QUEST_INFO", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onProcessTransactionResponse", "INVENTORY_HANDLER_PROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onPreprocessTransactionResponse", "INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)

	
	requestInventory(GAME_PID)
end



-- Button Handlers


function btnDecline_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

function btnQuests_OnButtonClicked(buttonHandle)
	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")				
	end

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "quests")
	KEP_EventQueue(ev)
end


-- Event handlers

-- Handled quest info passed from Framework_QuestGiver
function onQuestInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_questInfo  	 = Events.decode(KEP_EventDecodeString(event))

	m_bQuestInfoInitialized = true
	if m_bGameItemsInitialized then
		populateMenu()
	end
end

-- A single Inventory item has been updated
function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
	if m_bGameItemsInitialized and m_bQuestInfoInitialized then
		populateMenu()
	end
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems

	m_bGameItemsInitialized = true
	if m_bQuestInfoInitialized then
		populateMenu()
	end
end


--Create the menu now

function populateMenu()		
	local questProperties = m_questInfo.properties
	local targetUNID = tostring(questProperties.targetUNID)
	local targetItem = m_gameItems[targetUNID]
	local destroyOnCompletion = tostring(questProperties.destroyOnCompletion) == "true"

	local requiredProperties = questProperties.requiredItem
	local requiredUNID = tostring(requiredProperties.requiredUNID)
	local requiredCount = requiredProperties.requiredCount

	local requiredItem = m_gameItems[requiredUNID]

	if not targetItem or not requiredItem then
		--MISSING VERY IMPORTANT INFO. ABANDON SHIP
		MenuCloseThis()
		return
	end

	-- Set target name
	local targetName = targetItem.behaviorParams and targetItem.behaviorParams.actorName
	if targetName == nil or targetName == "" then
		targetName = targetItem.name
	end
	Static_SetText(gHandles["stcTargetName"], targetName.." Says:")

	--Set Thank you message
	local thanksMessage = '"Thanks for bringing me ('..tostring(requiredCount)..') '..requiredItem.name..'."'
	if not destroyOnCompletion then
		thanksMessage = string.gsub(thanksMessage, "bringing me", "collecting")
		Static_SetText(gHandles["btnDeliver"], "OK")
		Control_SetLocationX(gHandles["btnDeliver"], BUTTON_OKAY_X)
		Control_SetLocationX(gHandles["btnQuests"], BUTTON_QUESTS_X)
		Control_SetEnabled(gHandles["btnDecline"], false)
		Control_SetVisible(gHandles["btnDecline"], false)

	end
	Static_SetText(gHandles["stcThanks"], thanksMessage)

	-- Set item image
	local imgHandle = Image_GetDisplayElement(gHandles["imgQuestIcon"])
	if imgHandle and gDialogHandle then
		if questProperties.goalImage and questProperties.goalImage ~= "" and questProperties.goalImage ~= "none" then
			-- Set image. This part only needs to be done once, even if this function is called again
			if m_image == nil then
				m_image = questProperties.goalImage
				m_imgDispElement = imgHandle
				local lastSlashIndex = string.find(m_image, "/[^/]*$")
				local periodIndex = string.find(m_image,  '.', 1, true)
				if lastSlashIndex and periodIndex then
					local imageName =  string.sub(m_image, lastSlashIndex + 1, periodIndex - 1) or "none"
					m_imageFile = downloadImage(m_image, imageName.."_DeliverGoal")
				end
			end
		else

			local imageGLID = targetItem.GLID
			KEP_LoadIconTextureByID(tonumber(imageGLID), imgHandle, gDialogHandle)
		end
	end

	_G["btnDeliver_OnButtonClicked"] = function(buttonHandle)
		if destroyOnCompletion then
			local removeTable = {}

			local removeItem = {UNID = tonumber(requiredUNID), count = tonumber(requiredCount)}
			removeItem.lootInfo = {sourcePID = m_actorPID, lootSource = "Quest", transactionID = transactionID, questUNID = m_questInfo.UNID}
			table.insert(removeTable, removeItem)
			
			local addTable = {}
			processTransaction(removeTable, addTable)
		else
			--Don't need to preprocess. If menu was opened, user had enough of the item to fulfill quest
			fulfillQuest(tonumber(m_questInfo.UNID), m_questInfo)
			MenuCloseThis()
		end
	end
end


-- Response for loot acquisition event
function onProcessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
	
	if success then
		fulfillQuest(tonumber(m_questInfo.UNID), m_questInfo)
		MenuCloseThis()
	end
end

--------------------------
-----Images
--------------------------

function downloadImage(imgURL, chapterName)
	--the name the image will saved as
	local filename = tostring(chapterName)..".jpg"
	local path = KEP_DownloadTextureAsyncToGameId(filename, imgURL)
	
    local gameId = KEP_GetGameId()
	
	path = string.gsub(path,".-\\", "" )
	path = "../../CustomTexture/".. gameId .. "/" .. path
	return path
end


function textureDownloadHandler()
	updateImage()
end

function updateImage()
	if m_imgDispElement then
		Element_AddTexture(m_imgDispElement, gDialogHandle,m_imageFile)
		local textureWidth = Element_GetTextureWidth(m_imgDispElement, gDialogHandle)
		local textureHeight = Element_GetTextureHeight(m_imgDispElement, gDialogHandle)

		m_imageWidth = textureWidth
		scaleQuestImage(textureWidth, textureHeight)
	end
end

function scaleQuestImage(width, height)
	local newWidth = width
	local newHeight = height

	if width <= height then
		if width ~= IMAGE_SIZE then
			newHeight = height * (IMAGE_SIZE / width)
			newWidth = IMAGE_SIZE
		end
	else
		if height ~= IMAGE_SIZE then
			newWidth = width * (IMAGE_SIZE / height) 
			newHeight = IMAGE_SIZE
		end
	end

	setQuestImage(newWidth, newHeight)
	Element_SetCoords(m_imgDispElement, 0, 0, LeastPowerofTwo(width), LeastPowerofTwo(height))
end

function setQuestImage(width, height)
	local imageHandle = gHandles["imgQuestIcon"]
	local imageBGHandle = gHandles["imgQuestIconBG"]
	Control_SetSize(imageHandle, width, height)
	if height > IMAGE_SIZE then
		local heightDifference  = height - IMAGE_SIZE
		local moveDistance = heightDifference / 2
		Control_SetLocation(imageHandle, Control_GetLocationX(imageHandle), Control_GetLocationY(imageHandle)-moveDistance)
	end

	if width > IMAGE_SIZE then
		local widthDifference  = width - IMAGE_SIZE
		local moveDistance = widthDifference / 2
		Control_SetLocation(imageHandle, Control_GetLocationX(imageHandle) - moveDistance, Control_GetLocationY(imageHandle))
	end

	local containerX, containerY = Control_GetLocationX(imageBGHandle), Control_GetLocationY(imageBGHandle)
	local containerW, containerH = Control_GetWidth(imageBGHandle), Control_GetHeight(imageBGHandle)

	Control_SetClipRect(imageHandle, containerX, containerY, containerX+containerW, containerY+containerH)
end