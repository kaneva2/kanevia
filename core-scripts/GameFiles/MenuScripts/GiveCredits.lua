--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- GiveCredits.lua
-- Author: ARA 2015
-- Menu script for player credit gifting
-------------------------------------------------------------------------------
dofile("MenuHelper.lua")
dofile("HUDHelper.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")

----------------------
-- Globals
----------------------
-- user name, id, and avatar picture to recieve credits gift
g_userGiftedID = 0				-- user ID to be gifted
g_userGiftedName = nil			-- user name to be gifted
g_fileName = 0 					-- filename of avatar picture

-- self user's id
g_userSelfID = 0				-- self user ID
g_userSelfName = ""

----------------------
-- Locals
----------------------
local m_creditBalance = 0 		-- current credits balance
local m_giftAmount = 0			-- amount of credits to be gifted
local SLIDER_NAME = "sldrCreditAmount"
local m_rangeMax = 0
local m_currCredits = 0

local m_YourCreditsOriginalX = 0
local m_YourCreditsOriginalWidth = 0

-- InitializeKEPEvents
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister ("UPDATE_CREDIT_TRANSACTION", KEP.MED_PRIO)
end

-- InitializeKEPEventHandlers
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "GiveCreditsEventHandler", "GIVE_CREDITS_EVENT", KEP.MED_PRIO ) 
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "LooseTextureDownloadEventHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) -- REQUIRED - DO NOT REMOVE

	m_YourCreditsOriginalWidth = Static_GetStringWidth(gHandles["stcBalance"], "Your Credits: 9,999,999")
	m_YourCreditsOriginalX = Control_GetLocationX(gHandles["stcBalance"])

	m_sliderHandle = gHandles[SLIDER_NAME]
	m_rangeMax = Slider_GetRangeMax(m_sliderHandle)

	-- Set slider to 1
	m_currCredits = 1
	local m_sliderValue = m_rangeMax / m_creditBalance
	EditBox_SetText(gHandles["editBox"], 1, false)
	Slider_SetValue(m_sliderHandle, m_sliderValue)

	g_userSelfName = KEP_GetLoginName()
	requestUserId()

	closeSlider(false)
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle ) -- REQUIRED - DO NOT REMOVE
end

-- Dialog_OnRender - Called upon screen render.
function Dialog_OnRender(dialogHandle, elapsedSec)
end

-- Add comma to separate thousands
function commaValue(amount)
  	local formatted = amount
  	-- Loop through string...
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
    	-- until we reach the end of the string
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end

-- Gets current player balance through CreditBalances.lua
function requestCreditsRewards()
	-- If the user requested exists, get their balance
	if g_userSelfID > 0 then
		getBalances(g_userSelfID, WF.MYFAME_GETBALANCE)
	end
end

function calculateGift(curValue)
	m_currCredits = math.ceil((curValue * m_creditBalance) / m_rangeMax)

	if m_currCredits < 1 then
		m_currCredits = 1
		local m_sliderValue = m_rangeMax / m_creditBalance
		EditBox_SetText(gHandles["editBox"], 1, false)
		Slider_SetValue(m_sliderHandle, m_sliderValue)
		Control_SetEnabled(gHandles["btnGive"],true)
	else
		EditBox_SetText(gHandles["editBox"], tostring(m_currCredits), false)
		Control_SetEnabled(gHandles["btnGive"],true)
	end
end

function requestUserId()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..g_userSelfName, WF.MYFAME_USERID)
end

function closeSlider(isClosing)	
	Control_SetEnabled(gHandles["stcNoCreditsMain"], isClosing)
	Control_SetEnabled(gHandles["stcNoCreditsDetail"], isClosing)
	Control_SetVisible(gHandles["stcNoCreditsMain"], isClosing)
	Control_SetVisible(gHandles["stcNoCreditsDetail"], isClosing)


	Control_SetEnabled(m_sliderHandle, not isClosing)
	Control_SetEnabled(gHandles["editBox"], not isClosing)
	Control_SetEnabled(gHandles["imgUnderline"], not isClosing)
	Control_SetEnabled(gHandles["imgLink"], not isClosing)
	Control_SetEnabled(gHandles["btnGetCredits"], not isClosing)
	Control_SetEnabled(gHandles["stcSliderSubtext"], not isClosing)
end

function centerYourCredits()
	local currString = Static_GetText(gHandles["stcBalance"])
	local currWidth = Static_GetStringWidth(gHandles["stcBalance"], currString)
	local currX = Control_GetLocationX(gHandles["stcBalance"])
	local imgX = Control_GetLocationX(gHandles["imgCredits"])

	if currWidth ~= m_YourCreditsOriginalWidth then
		local diffWidth = (currWidth - m_YourCreditsOriginalWidth)/4

		Control_SetLocationX(gHandles["stcBalance"], currX - diffWidth)
		Control_SetLocationX(gHandles["imgCredits"], imgX - diffWidth)
	end
end

-- Add comma to separate thousands
function formatCommaValue(value)
  	local formatted = value
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end
-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- TODO: parse player trading information
	-- TODO: parse player balance information
	if filter == WF.FRIEND_THUMBNAIL_URL then
		local returnData = KEP_EventDecodeString( tEvent )
		s, e, front, url = string.find(returnData, "(http://.-/)(.*)")
		s, e, front, file_name = string.find(returnData, "(.*/)(.*)")
		
		if ( file_name ~= nil ) then
			g_fileName = g_userGiftedID .. "_" .. file_name 
			KEP_DownloadTextureAsyncToGameId( g_fileName, url )
		end
	elseif filter == WF.MYFAME_GETBALANCE then
		local returnData = KEP_EventDecodeString( tEvent )
		m_creditBalance, m_rewards = ParseBalances(returnData)
		Static_SetText(gHandles["stcBalance"], "Your Credits: "..formatCommaValue(m_creditBalance))
		centerYourCredits()

		if m_creditBalance == 0 then
			closeSlider(true)

			EditBox_SetText(gHandles["editBox"], "", false)
			Static_SetText(gHandles["btnGive"], "Get Credits")
			_G["btnGive_OnButtonClicked"] = function(btnHandle)
				KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .. BUY_CREDIT_PACKAGES_SUFFIX)
				MenuCloseThis()
			end
		else
			closeSlider(false)

			EditBox_SetText(gHandles["editBox"], "1", false)
			Static_SetText(gHandles["btnGive"], "Give")
			_G["btnGive_OnButtonClicked"] = function(btnHandle)
				if m_currCredits > 0 then
					local event = KEP_EventCreate("UPDATE_CREDIT_TRANSACTION")
					KEP_EventEncodeString(event, m_currCredits)
					KEP_EventEncodeString(event, 0) -- GLID not needed for this purchase
					KEP_EventEncodeString(event, 0) -- community ID not needed for this purchase
					KEP_EventEncodeString(event, "GiveCredits")
					KEP_EventEncodeString(event, g_userGiftedName)
					KEP_EventQueue(event)
					toggleMenu("CreditTransaction")
					MenuCloseThis()
				end
			end
		end
	elseif filter == WF.MYFAME_USERID then
		local returnData = KEP_EventDecodeString( tEvent )
		ParseUserID(returnData)
	end
end

-- GiveCreditsEventHandler - Called upon receipt of GIVE_CREDITS_EVENT
function GiveCreditsEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_userGiftedID = KEP_EventDecodeNumber( event )
	g_userGiftedName = KEP_EventDecodeString( event )
	ebh = Dialog_GetStatic( gDialogHandle, "stcMemberName" )
	if ebh ~= nil then
		Static_SetText( gHandles["stcMemberName"], g_userGiftedName, false )
		makeWebCall(GameGlobals.WEB_SITE_PREFIX .. GET_FRIEND_THUMBNAIL_URL_SUFFIX .. g_userGiftedID, WF.FRIEND_THUMBNAIL_URL)
	end

	local myName = KEP_GetLoginName()
end

function LooseTextureDownloadEventHandler()		
	local gameId = KEP_GetGameId()
	local ihFore = Dialog_GetImage(gDialogHandle, "imgMemberImage")
	local ihBack = Dialog_GetImage(gDialogHandle, "imgMemberImage")			
	if ihFore then
		scaleImage(ihFore, ihBack, "../../CustomTexture/" .. gameId .. "/" .. g_fileName)
    end
end

function ParseUserID(returnData)
	local result = nil  	-- result description
	local s, e = 0      	-- start and end of captured string
	s, e, result = string.find(returnData, "<ReturnCode>(%d+)</ReturnCode>")
	if result == "0" then
		-- Parse needed info from XML
		s, e, result = string.find(returnData, "<user_id>(%d+)</user_id>", e)
		g_userSelfID = tonumber(result)

		requestCreditsRewards()
	end
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

function sldrCreditAmount_OnSliderValueChanged(sliderHandle, sliderValue)
	calculateGift(sliderValue)
end

function editBox_OnEditBoxChange(editBoxHandle)
	local editText = EditBox_GetText(gHandles["editBox"])

	if tonumber(editText) then
		-- Clean the editbox to contain only numbers (which are positive as well)
		EditBox_SetText(gHandles["editBox"], tonumber(editText), false)

		if tonumber(editText) <= m_creditBalance and tonumber(editText) >= 1 then
			m_currCredits = tonumber(editText)
			local m_sliderValue = (m_currCredits * m_rangeMax) / m_creditBalance
			Slider_SetValue(m_sliderHandle, m_sliderValue)
			Control_SetEnabled(gHandles["btnGive"],true)
		elseif tonumber(editText) > m_creditBalance then
			EditBox_SetText(gHandles["editBox"], m_creditBalance, false)
			Slider_SetValue(m_sliderHandle, m_rangeMax)
			Control_SetEnabled(gHandles["btnGive"],true)
		elseif tonumber(editText) < 1 then
			m_currCredits = 0
			local m_sliderValue = m_rangeMax / m_creditBalance
			EditBox_SetText(gHandles["editBox"], 0, false)
			Slider_SetValue(m_sliderHandle, m_sliderValue)
			Control_SetEnabled(gHandles["btnGive"],false)
		end
	elseif editText ~= "" then
		m_currCredits = 1
		local m_sliderValue = m_rangeMax / m_creditBalance
		EditBox_SetText(gHandles["editBox"], 1, false)
		Slider_SetValue(m_sliderHandle, m_sliderValue)
	end
end

function btnGetCredits_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .. BUY_CREDIT_PACKAGES_SUFFIX)
	MenuCloseThis()
end