--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")

gDropId = -1
gImportInfo = nil
gTargetInfo = nil
gBtnState = 0
gKeyState = 0

function UGCImportMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	gDropId = UGCI.GetDialogInstanceId( gDialogHandle )
	local dlgId = Dialog_GetId( gDialogHandle )
	if filter~=dlgId or objectid~=gDropId then
		-- Ignore event with mismatched IDs
		return
	end

	local eventData = UGCI.ParseUGCImportMenuEvent( event )
	gImportInfo = eventData.importInfo
	gTargetInfo = eventData.targetInfo
	gBtnState = eventData.btnState
	gKeyState = eventData.keyState
	
    Log( "UGCImportMenuEventHandler: dlgId=" .. dlgId .. " dropId=" .. gDropId .. " type=" .. gImportInfo.type .. " path=" .. gImportInfo.fullPath )

	if gImportInfo.type==UGC_TYPE.COLLADA then
		-- need user interaction: unhide menu
		Dialog_SetMinimized( gDialogHandle, 0 )
	else
		-- no UI needed, proceed with import
		if gImportInfo.type==UGC_TYPE.DYNOBJ then
			ImportDynamicObject()
		elseif gImportInfo.type==UGC_TYPE.CHARANIM then
			ImportAnimation()
		elseif gImportInfo.type==UGC_TYPE.EQUIPPABLE then
			ImportEquippableObject()
		end
		MenuCloseThis()
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "UGCImportMenuEventHandler", UGC_IMPORT_MENU_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( UGC_IMPORT_MENU_EVENT, KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	Dialog_SetMinimized( dialogHandle, 1 )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnImpAnim_OnButtonClicked( buttonHandle )
	Log( "Import Animation Clicked" )
	ImportAnimation()
	MenuCloseThis()
end

function btnImpMesh_OnButtonClicked( buttonHandle)
	Log( "Import Mesh Clicked" )
	ImportDynamicObject()
	MenuCloseThis()
end

function btnImpAccessory_OnButtonClicked( buttonHandle )
	Log( "Import Accessory Clicked" )
	ImportEquippableObject()
	MenuCloseThis()
end

function ImportAnimation()
	-- Update import type
	gImportInfo.type = UGC_TYPE.CHARANIM
	gImportInfo.category = UGC_TYPE.CHARANIM
	-- call handler
	if gTargetInfo.type ~= ObjectType.WORLD then
		UGCI.CallUGCHandler("AnimImportProg.xml", 0, gDropId, gImportInfo, gTargetInfo, gBtnState, gKeyState )
	else
		KEP_MessageBox( "Error importing Animation.\n\nInvalid target.", "Import failed" )
	end
end

function ImportDynamicObject()
	-- Update import type
	gImportInfo.type = UGC_TYPE.DYNOBJ
	gImportInfo.category = UGC_TYPE.DYNOBJ
	-- call handler
	UGCI.CallUGCHandler("MeshImportProg.xml", 0, gDropId, gImportInfo, gTargetInfo, gBtnState, gKeyState )
end

function ImportEquippableObject()
	-- Update import type
	gImportInfo.type = UGC_TYPE.EQUIPPABLE	-- actual type (rigid/skeletal) to be determined during import
	gImportInfo.category = UGC_TYPE.EQUIPPABLE
	-- call handler
	UGCI.CallUGCHandler("EquippableImportProg.xml", 0, gDropId, gImportInfo, gTargetInfo, gBtnState, gKeyState )
end
