--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------------------------------------
----Framework_TeleportHandler
--Handler for teleportation events as well as the Overview
----------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")

local s_tOverviewSettings = {}

local s_bShouldTeleport = false

function onCreate( )

	Events.registerHandler("FRAMEWORK_GOTO_ZONE", gotoZoneHandler) 
	Events.registerHandler("FRAMEWORK_GOTO_HOME", goHomeHandler)

	Events.registerHandler("FRAMEWORK_RETURN_OVERVIEW", returnOverviewHandler)
	Events.sendEvent("FRAMEWORK_REQUEST_OVERVIEW")

	KEP_EventRegister("FRAMEWORK_REQUEST_OVERVIEW_SETTINGS", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_RETURN_OVERVIEW_SETTINGS", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("requestOverviewSettingsHandler", "FRAMEWORK_REQUEST_OVERVIEW_SETTINGS", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("parentInfoHandler", "FRAMEWORK_RETURN_PARENT_INFO", KEP.HIGH_PRIO)
	

	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
end

-----------------------------
---SERVER EVENT HANDLERS-----
-----------------------------

function gotoZoneHandler(event)
	if event.zoneID and event.zoneType then
		s_bShouldTeleport = true
		local url = GameGlobals.WEB_SITE_PREFIX..GET_URL_SUFFIX.."&zoneInstanceID="..tostring(event.zoneID).."&zoneType="..tostring(event.zoneType)
		makeWebCall(url, WF.GET_URL)
	end
end

function goHomeHandler(event)
	goHome()
end

-----------------------------
----CLIENT EVENT HANDLERS----
-----------------------------

function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	if filter == WF.GET_URL then
		local estr = KEP_EventDecodeString( tEvent )
		ParseLocationData(estr)
	elseif filter == WF.LINKED_ZONES then
		local estr = KEP_EventDecodeString( tEvent )
		ParseLinkedZoneData(estr)
	end
end

function ParseLocationData(locationData)

	if s_bShouldTeleport == false then return end

    local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	s, strPos, result = string.find(locationData, "<ReturnCode>(.-)</ReturnCode>")

	if result == "0" then
		s, strPos, url = string.find(locationData, "<STPUrl>(.-)</STPUrl>")
		gotoURL(url)
	end

	s_bShouldTeleport = false
end

function parentInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local parentZoneType = KEP_EventDecodeNumber(event)
	local parentZoneInstanceID = KEP_EventDecodeNumber(event)
	local url = GameGlobals.WEB_SITE_PREFIX..GET_LINKED_ZONES_SUFFIX.."&zoneInstanceID="..tostring(parentZoneInstanceID).."&zoneType="..tostring(parentZoneType)
	makeWebCall(url, WF.LINKED_ZONES)
end

function ParseLinkedZoneData(locationData)
    local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	s, strPos, result = string.find(locationData, "<ReturnCode>(.-)</ReturnCode>")

	if result == "0" then
		--Did one of the zone names change? If so, update the overview settings
		local nameChanged = false
		for zoneData in string.gmatch(locationData, "<Table>(.-)</Table>") do

			s, strPos, name = string.find(zoneData, "<name>(.-)</name>")

			s, strPos, zoneInstanceId = string.find(zoneData, "<zone_instance_id>(.-)</zone_instance_id>")
			for index, marker in pairs(s_tOverviewSettings.markers) do
				local destination = marker.destination
				if destination and destination.zoneID and tostring(destination.zoneID) == tostring(zoneInstanceId) then
					if destination.name ~= name then
						destination.name = name
						nameChanged = true
					end
				end
			end
		end
		if nameChanged then
			Events.sendEvent("FRAMEWORK_UPDATE_OVERVIEW", {overview = s_tOverviewSettings})
		end
	end
end
-----------------------
---Overview Handlers---
-----------------------

function returnOverviewHandler(event )
	s_tOverviewSettings = {
					overviewEnabled 	= event.overviewEnabled or false,
					overviewImage 		= event.overviewImage,
					markers 			= event.markers,
				   }

	KEP_EventCreateAndQueue("FRAMEWORK_GET_PARENT_INFO")
end

function requestOverviewSettingsHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local event = KEP_EventCreate("FRAMEWORK_RETURN_OVERVIEW_SETTINGS")
	KEP_EventEncodeString(event, Events.encode(s_tOverviewSettings))
		
	KEP_EventQueue(event)
end