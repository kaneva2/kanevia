--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

ENTER_BUILD_MODE_EVENT = "EnterBuildModeEvent"

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnCloseBig_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

function btnBeAFashionDesigner_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://docs.kaneva.com/mediawiki/index.php/Designer_Area" )
	MenuCloseThis()
end

function btnAddNewObjects_OnButtonClicked(buttonHandle)

	-- Enter Build Mode and open Import Objects menu
	if MenuIsClosed("ImportMenu.xml") then
		if KEP_IsOwnerOrModerator() then
			local ev = KEP_EventCreate( ENTER_BUILD_MODE_EVENT)
			KEP_EventSetFilter(ev, 1)
			KEP_EventEncodeNumber(ev, -1) -- Don't change mode
			KEP_EventQueue( ev)
			MenuOpenModal("ImportMenu.xml")
		else
			KEP_MessageBox( "You can only Build and Import Objects in a space you own, like your home or your World.", "Error" )
		end
	end

	MenuCloseThis()
end

function btnStartACommunity_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .. "community/CommunityEdit.aspx")
	MenuCloseThis()
end

function btnMakeAGame_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( "http://www.kaneva.com/community/start3dappb.aspx")
	MenuCloseThis()
end
