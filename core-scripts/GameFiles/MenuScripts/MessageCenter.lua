--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("PageButtons.lua")
dofile("MenuLocation.lua")
dofile("MenuStyles.lua")

FILTER_SUCCESS = 0

PLAYER_USERID_EVENT = "PlayerUserIdEvent"
YES_NO_BOX_EVENT = "YesNoBoxEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"

SUPPORT_ACCT_NAME = "support"

MESSAGE_INBOX = "kgp/messagecenter.aspx?"
MESSAGE_READ = "action=GetInboxMessages"
MESSAGE_DELETE = "action=DeleteInboxMessage&messageId="
FRIEND_REQUESTS = "action=GetFriendRequests"
GIFTS_RECIEVED = "action=ListPendingGifts"
ACCEPT_GIFT = "action=AcceptGift&messageId="
REJECT_GIFT = "action=RejectGift&messageId="
ACCEPT_FRIEND = "action=AcceptFriend&friendId="
REJECT_FRIEND = "action=DenyFriend&friendId="
READ_GIFT_MESSAGE = "action=ReadGiftMessage&messageId="

--Ignore
IGNORE_SUFFIX = "kgp/blockUser.aspx?action="
ADD_IGNORE_SUFFIX = "Block&userId="

--Send Message
MESSAGE_CENTER_SUFFIX = "kgp/messagecenter.aspx?action="
SEND_MESSAGE_SUFFIX = "SendMessage&type=Member&username="
SUBJECT_SUFFIX = "&subject="
MESSAGE_SUFFIX = "&message="

-- Menu Tabs
TAB_INBOX = 0
TAB_REQUESTS = 1
TAB_GIFTS = 2
TAB_MESSAGE = 3
TAB_VIEW_GIFT = 4

-- Buttons
BTN_NONE = 0
BTN_IGNORE = 1
BTN_SPAM = 2
BTN_REJECT = 3
BTN_ACCEPT = 4
BTN_REPLY = 5

INFO_BOX_EVENT = "InfoBoxEvent"

-- Web address for query
g_web_address = nil

-- Current tab selected (Friends, People, or Ignore)
g_current_tab = nil

-- Hold current friend data returned from server
g_message = ""

-- Total number of friend records
g_totalNumRec = 0

-- Maximum number of friends for server to return in one request
-- friends per page
g_msgPerPage = 8

g_selectedIndex = -1
g_t_data = {}

-- Some calls to get browser page will return only success or failure info.
-- This is used to pick our parse function.
g_resultParse = false
g_btnPressed = BTN_NONE
g_targetPerson = "nobody"

-- table for button handles
g_t_btnHandles = {}

g_isReject = false

--Number of records returned from web query (i.e. Inbox Messages)
--A string
g_numRec = 0

g_pageCapLowered = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "answerEventHandler", YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserReplyEventHandler", BrowserIds.BROWSER_REPLY_EVENT_NAME, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( YES_NO_BOX_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( BrowserIds.BROWSER_REQUEST_EVENT_NAME, KEP.MED_PRIO )
end

function BrowserReplyEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter == FILTER_SUCCESS then
		local strip = KEP_EventDecodeNumber( event )
		local message = KEP_EventDecodeString( event )
		KEP_MessageBox(message)
	end

	ChangeMessageViewMode(0)
	getGifts()
	Static_SetText(Dialog_GetStatic(gDialogHandle, "lblTitle"), "Personal Messages")
	ch	= Image_GetControl(Dialog_GetImage(gDialogHandle, "imgGiftPic"))
	Control_SetVisible(ch, false)
end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = KEP_EventDecodeNumber( event )

	if answer ~= 0 then
		if g_btnPressed == BTN_IGNORE then
			ignoreUser()
		elseif g_btnPressed == BTN_SPAM then
			reportSpam()
		end
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	ChangeMessageViewMode(0)
	getInbox()

	initButtons()

	getButtonHandles()

	loadLocation(gDialogHandle, "MessageCenter")

	-- setSelectionColor is called from PageButtons.lua to highlight the first
	-- button when the menu is opend.
	local bh = Dialog_GetButton(dialogHandle, "btnRange1")
	if bh ~= nil then
		setSelectionColor( bh )
	end

	bh = Dialog_GetButton(gDialogHandle, "btnInbox")
	setButtonState(bh, false)

	local sh = Dialog_GetStatic(dialogHandle, "lblStatus")

	if sh ~= nil then
		Static_SetText(sh, "Choose a Message to View or Delete")
	end

	setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstList1"))
	setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstList2"))
	setListBoxStyle(Dialog_GetListBox(dialogHandle, "lstList3"))
end

function Dialog_OnDestroy(dialogHandle)
	saveLocation(dialogHandle, "MessageCenter")
	Helper_Dialog_OnDestroy( dialogHandle )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.MESSAGES then
		g_message = KEP_EventDecodeString( event )
		if g_resultParse then
			ParseResults()
		elseif g_current_tab == TAB_INBOX then
			if g_btnPressed == BTN_REJECT then
				g_btnPressed = BTN_NONE
				--Rerequest the inbox

				--If after the last delete message query the total number of
				--records was 1 then delete would take us to the previous page.
				--Also if deleting a message brings us to a total count that
				--is divisible by 8 then we will need to load a new page
				if ((g_numRec == "1") and (g_curPage > 0)) or (math.fmod(g_totalNumRec, g_msgPerPage) == 0) then
					decrementPage()
					clearSelection()
				end

				makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
			else
				parseInbox()
			end
		elseif g_current_tab == TAB_REQUESTS then
			if g_btnPressed == BTN_REJECT or g_btnPressed == BTN_ACCEPT then
				g_btnPressed = BTN_NONE
			else
				parseRequests()
			end
		elseif g_current_tab == TAB_GIFTS then
			if g_btnPressed == BTN_REPLY or g_btnPressed == BTN_REJECT then
				g_btnPressed = BTN_NONE
				if ((g_numRec == "1") and (g_curPage > 0)) or (math.fmod(g_totalNumRec, g_msgPerPage) == 0) then
					decrementPage()
					clearSelection()
				end
				makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
			else
				parseGifts()
			end
		elseif g_current_tab == TAB_VIEW_GIFT then
			parseSelectedGift()
		elseif g_current_tab == TAB_MESSAGE then
			if g_btnPressed == BTN_REJECT then
				getInbox()
				g_btnPressed = BTN_NONE
			end
		end
		return KEP.EPR_CONSUMED
	end
end

function ParseResults()
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(g_message, "<ReturnCode>(%--%w+)</ReturnCode>")
	if result == "0" then
		if g_btnPressed == BTN_SPAM then
			KEP_MessageBox("You have reported "..g_targetPerson..".  Spam report sent to Kaneva.")
		elseif g_btnPressed == BTN_IGNORE then
			KEP_MessageBox("You will now ignore "..g_targetPerson..".  To unignore them go to the Friends and People menu.")
			makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..REJECT_FRIEND..g_t_data[g_selectedIndex + 1]["from_id"], WF.MESSAGES)
		elseif g_btnPressed == BTN_REJECT or g_btnPressed == BTN_ACCEPT then
			if g_current_tab == TAB_REQUESTS then
				if ((g_numRec == "1") and (g_curPage > 0)) or (math.fmod(g_totalNumRec, g_msgPerPage) == 0) then
					decrementPage()
					clearSelection()
				end
				makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
			end
		end
	elseif result == "-1" then
		if g_btnPressed == BTN_IGNORE then
			KEP_MessageBox("You are already ignoring "..g_targetPerson..".  See the Friends and People menu if you would like to unignore them.")
		end
	else
		KEP_MessageBox("Unable to send request to Kaneva.")
	end

	g_btnPressed = BTN_NONE
	g_resultParse = false
end

function prettyDateFormat(datestr)
	if datestr ~=nil then
		local year = string.sub(datestr, 1, 4)
		local month = string.sub(datestr, 6, 7)
		local day = string.sub(datestr, 9, 10)
		local monthname = "January"
		if month == "01" then
			monthname = "Jan."
		elseif month == "02" then
			monthname = "Feb."
		elseif month == "03" then
			monthname = "Mar."
		elseif month == "04" then
			monthname = "Apr."
		elseif month == "05" then
			monthname = "May"
		elseif month == "06" then
			monthname = "Jun."
		elseif month == "07" then
			monthname = "Jul."
		elseif month == "08" then
			monthname = "Aug."
		elseif month == "09" then
			monthname = "Sept."
		elseif month == "10" then
			monthname = "Oct."
		elseif month == "11" then
			monthname = "Nov."
		elseif month == "12" then
			monthname = "Dec."
		end
		return monthname .. " " .. day .. ", " .. year
	else
		return "Unknown"
	end
end

function prettyTimeFormat(timestr)
	if timestr ~= nil then
		local hour = string.sub(timestr, 1, 2)
		local min = string.sub(timestr, 4, 5)
		local ampm = ""
		if tonumber(hour) == 0 then
			hour = 12
			ampm = "AM"
		elseif tonumber(hour) < 12 then
			hour = tonumber(hour)
			ampm = "AM"
		else
			hour = tonumber(hour) - 12
			ampm = "PM"
		end
		return hour .. ":" .. min .. " " .. ampm
	else
		return "Uknown"
	end
end

function parseSelectedGift()
	local s = 0
	local strPos = 1	--beginning of string
	local msg_id = 0
	local gif_id = 0
	local result = nil  --result code of query
	s, strPos, result = string.find(g_message, "<ReturnCode>(.-)</ReturnCode>")
	if tonumber(result) == 0 then
		g_t_data = {}
		s, strPos, msg_id = string.find(g_message, "<message_id>(.-)</message_id>", strPos)
		s, strPos, gift_id = string.find(g_message, "<gift_id>(.-)</gift_id>", strPos)
		local msg = {}
		local giftInfo = { KEP_GetItemInfo(tonumber(gift_id)) }
		msg["message_id"] = tonumber(msg_id)
		msg["gift_id"] = tonumber(gift_id)
		msg["icon"] = giftInfo

		table.insert(g_t_data, msg)

		setGiftIcon("imgGiftPic", msg["gift_id"])
		ch	= Image_GetControl(Dialog_GetImage(gDialogHandle, "imgGiftPic"))
		Control_SetVisible(ch, true)
	end
end

function parseGifts()
	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local trash = 0

	local whole_message = ""
	local thumb_path = ""
	local name_no_spc = ""
	local message_id = 0
	local from_id = 0
	local from_id_status = 0
	local to_id = 0
	local subject = ""
	local message_body = ""
	local message_date = ""
	local message_time = ""
	local replied = 0
	local type = 0
	local username = ""

	s, strPos, result = string.find(g_message, "<ReturnCode>([0-9]+)</ReturnCode>")

	if result == "0" then

		g_numRec = calculateRanges()

		local sh = Dialog_GetStatic(gDialogHandle, "lblEmpty")
		if g_numRec == "0" then
			Control_SetVisible(Static_GetControl(sh), true)
			Static_SetText(sh, "You have not recieved any gifts")
		else
			Control_SetVisible(Static_GetControl(sh), false)
		end

		local lbh = Dialog_GetListBox(gDialogHandle, "lstList1")
		local lbhOn = Dialog_GetListBox(gDialogHandle, "lstList2")
		local lbhOn2 = Dialog_GetListBox(gDialogHandle, "lstList3")

		if (lbh ~= nil) and (lbhOn ~= nil) then
			g_t_data = {}
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOn)
			ListBox_RemoveAllItems(lbhOn2)

			for i=1,g_numRec do
				s, strPos, whole_message = string.find(g_message, "<PendingGifts>(.-)</PendingGifts>", strPos)

				s, trash, thumb_path         = string.find(whole_message, "<thumbnail_small_path>(.-)</thumbnail_small_path>"  , 1)
				s, trash, name_no_spc        = string.find(whole_message, "<name_no_spaces>(.-)</name_no_spaces>"              , 1)
				s, trash, message_id         = string.find(whole_message, "<message_id>([0-9]+)</message_id>"                  , 1)
				s, trash, from_id            = string.find(whole_message, "<from_id>([0-9]+)</from_id>"                        , 1)
				s, trash, from_id_status     = string.find(whole_message, "<from_id_status>([0-9]+)</from_id_status>"          , 1)
				s, trash, to_id              = string.find(whole_message, "<to_id>([0-9]+)</to_id>"                            , 1)
				s, trash, subject            = string.find(whole_message, "<subject>(.-)</subject>"                            , 1)
				s, trash, message_body       = string.find(whole_message, "<message>(.-)</message>"                            , 1)
				s, trash, message_date, message_time = string.find(whole_message, "<message_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)", 1)
				s, trash, replied            = string.find(whole_message, "<replied>([0-9]+)</replied>"                        , 1)
				s, trash, type               = string.find(whole_message, "<type>([0-9]+)</type>"                              , 1)
				s, trash, username           = string.find(whole_message, "<username>(.-)</username>"                          , 1)

				local msg = {}
				message_date = prettyDateFormat(message_date)
				msg["thumb_path"] = thumb_path
				msg["name_no_spc"] = name_no_spc
				msg["message_id"] = message_id
				msg["from_id"] = from_id
				msg["from_id_status"] = from_id_status
				msg["to_id"] = to_id
				msg["subject"] = subject
				msg["message_body"] = message_body
				msg["message_date"] = message_date
				msg["replied"] = replied
				msg["type"] = type
				msg["username"] = username
				table.insert(g_t_data, msg)

				ListBox_AddItem(lbh, username, message_id)
				ListBox_AddItem(lbhOn, subject, message_id)
				ListBox_AddItem(lbhOn2, message_date, message_id)
			end

			alignButtons()
		end
	end
end

function parseRequests()
	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local trash = 0

	local whole_message = ""
	local thumb_path = ""
	local name_no_spc = ""
	local username = ""
	local from_id = 0
	local country = ""
	local country_name = ""
	local gender = ""
	local birth_date = ""
	local friend_status_id = 0
	local glued_date = ""
	local glued_time = ""
	local zip_code = 0

	s, strPos, result = string.find(g_message, "<ReturnCode>([0-9]+)</ReturnCode>")

	if result == "0" then
		g_numRec = calculateRanges()

		local sh = Dialog_GetStatic(gDialogHandle, "lblEmpty")
		if g_numRec == "0" then
			Control_SetVisible(Static_GetControl(sh), true)
			Static_SetText(sh, "No pending Friend Requests")
		else
			Control_SetVisible(Static_GetControl(sh), false)
		end

		local lbh = Dialog_GetListBox(gDialogHandle, "lstList1")
		local lbhOn = Dialog_GetListBox(gDialogHandle, "lstList2")
		local lbhOn2 = Dialog_GetListBox(gDialogHandle, "lstList3")

		if (lbh ~= nil) and (lbhOn ~= nil) then
			g_t_data = {}
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOn)
			ListBox_RemoveAllItems(lbhOn2)

			for i=1,g_numRec do
				s, strPos, whole_message = string.find(g_message, "<PendingFriend>(.-)</PendingFriend>", strPos)

				s, trash, thumb_path         = string.find(whole_message, "<thumbnail_small_path>(.-)</thumbnail_small_path>"  , 1)
				s, trash, name_no_spc        = string.find(whole_message, "<name_no_spaces>(.-)</name_no_spaces>"              , 1)
				s, trash, username           = string.find(whole_message, "<username>(.-)</username>"                          , 1)
				s, trash, age				 = string.find(whole_message, "<age>([0-9]+)</age>"								   , 1)
				s, trash, from_id            = string.find(whole_message, "<user_id>([0-9]+)</user_id>"                        , 1)
				s, trash, country            = string.find(whole_message, "<country>(.-)</country>"                            , 1)
				s, trash, country_name       = string.find(whole_message, "<country_name>(.-)</country_name>"                  , 1)
				s, trash, gender             = string.find(whole_message, "<gender>(.-)</gender>"                              , 1)
				s, trash, birth_date         = string.find(whole_message, "<birth_date>(.-)</birth_date>"                      , 1)
				s, trash, friend_status_id   = string.find(whole_message, "<friend_status_id>([0-9]+)</friend_status_id>"      , 1)
				s, trash, glued_date, glued_time = string.find(whole_message, "<request_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)"	   , 1)
				s, trash, zip_code           = string.find(whole_message, "<zip_code>([0-9]+)</zip_code>"                      , 1)

				local msg = {}
				glued_date = prettyDateFormat(glued_date) .. " - " .. prettyTimeFormat(glued_time)
				msg["thumb_path"] = thumb_path
				msg["name_no_spc"] = name_no_spc
				msg["username"] = username
				msg["age"] = age
				msg["from_id"] = from_id
				msg["country"] = country
				msg["country_name"] = country_name
				msg["gender"] = gender
				msg["birth_date"] = birth_date
				msg["friend_status_id"] = friend_status_id
				msg["glued_date"] = glued_date
				msg["zip_code"] = zip_code
				table.insert(g_t_data, msg)

				num_age = tonumber(age)

				if num_age then
					if num_age < 18 then
						glued_date = "<18          "..glued_date
					else
						glued_date = "  "..age.."          "..glued_date
					end
				end

				ListBox_AddItem(lbh, username, from_id)
				ListBox_AddItem(lbhOn, glued_date, from_id)
				ListBox_AddItem(lbhOn2, "", from_id)
			end

			alignButtons()
		end
	end
end

function parseInbox()
	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local trash = 0

	local whole_message = ""
	local thumb_path = ""
	local name_no_spc = ""
	local message_id = 0
	local from_id = 0
	local from_id_status = 0
	local to_id = 0
	local to_id_status = 0
	local subject = ""
	local message_body = ""
	local message_date = ""
	local message_time = ""
	local replied = 0
	local type = 0
	local username = ""

	s, strPos, result = string.find(g_message, "<ReturnCode>([0-9]+)</ReturnCode>")

	if result == "0" then
		g_numRec = calculateRanges()

		local sh = Dialog_GetStatic(gDialogHandle, "lblEmpty")
		if g_numRec == "0" then
			Control_SetVisible(Static_GetControl(sh), true)
			Static_SetText(sh, "No Messages in Inbox")
		else
			Control_SetVisible(Static_GetControl(sh), false)
		end

		local lbh = Dialog_GetListBox(gDialogHandle, "lstList1")
		local lbhOn = Dialog_GetListBox(gDialogHandle, "lstList2")
		local lbhOn2 = Dialog_GetListBox(gDialogHandle, "lstList3")

		if (lbh ~= nil) and (lbhOn ~= nil) then
			g_t_data = {}
			ListBox_RemoveAllItems(lbh)
			ListBox_RemoveAllItems(lbhOn)
			ListBox_RemoveAllItems(lbhOn2)

			for i=1,g_numRec do
				s, strPos, whole_message = string.find(g_message, "<Message>(.-)</Message>", strPos)

				s, trash, thumb_path         = string.find(whole_message, "<thumbnail_small_path>(.-)</thumbnail_small_path>"  , 1)
				s, trash, name_no_spc        = string.find(whole_message, "<name_no_spaces>(.-)</name_no_spaces>"              , 1)
				s, trash, message_id         = string.find(whole_message, "<message_id>([0-9]+)</message_id>"                  , 1)
				s, trash, from_id            = string.find(whole_message, "<from_id>([0-9]+)</from_id>"                        , 1)
				s, trash, from_id_status     = string.find(whole_message, "<from_id_status>([0-9]+)</from_id_status>"          , 1)
				s, trash, to_id              = string.find(whole_message, "<to_id>([0-9]+)</to_id>"                            , 1)
				s, trash, to_id_status       = string.find(whole_message, "<to_id_status>([0-9]+)</to_id_status>"              , 1)
				s, trash, subject            = string.find(whole_message, "<subject>(.-)</subject>"                            , 1)
				s, trash, message_body       = string.find(whole_message, "<message>(.-)</message>"                            , 1)
				s, trash, message_date, message_time = string.find(whole_message, "<message_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)"	   , 1)
				s, trash, replied            = string.find(whole_message, "<replied>([0-9]+)</replied>"                        , 1)
				s, trash, type               = string.find(whole_message, "<type>([0-9]+)</type>"                              , 1)
				s, trash, username           = string.find(whole_message, "<username>(.-)</username>"                          , 1)


				local msg = {}
				message_date = prettyDateFormat(message_date)
				msg["thumb_path"] = thumb_path
				msg["name_no_spc"] = name_no_spc
				msg["message_id"] = message_id
				msg["from_id"] = from_id
				msg["from_id_status"] = from_id_status
				msg["to_id"] = to_id
				msg["to_id_status"] = to_id_status
				msg["subject"] = subject
				msg["from_id"] = from_id

				message_body = stripTags(message_body, true)

				msg["message_body"] = message_body
				msg["message_date"] = message_date
				msg["replied"] = replied
				msg["type"] = type
				msg["username"] = username
				table.insert(g_t_data, msg)

				ListBox_AddItem(lbh, username, message_id)
				ListBox_AddItem(lbhOn, subject, message_id)
				ListBox_AddItem(lbhOn2, message_date, message_id)
			end

			alignButtons()
		end
	end
end

-- Used to fill the buttons with the correct numerical text
function calculateRanges()
	local resultsstr = "Showing "
	local numRec = 0  		--number of records returned
	local s, e  			-- start and end indices of found string
	local resultslbl = Dialog_GetStatic( gDialogHandle, "lblResults")
	local pageCap = 0		-- new page cap

	local high = g_curPage * g_msgPerPage
	local low = ((g_curPage - 1) * g_msgPerPage) + 1

	s, e, g_totalNumRec = string.find(g_message, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
	s, e, numRec = string.find(g_message, "<NumberRecords>([0-9]+)</NumberRecords>")

	g_totalNumRec = tonumber(g_totalNumRec)
	
	pageCap = math.ceil(g_totalNumRec / g_msgPerPage)
	g_setCap = math.ceil(g_pageCap / g_window)

	-- If our page cap changes from our last query we might have to update our page buttons
	if g_pageCap ~= pageCap then
		g_pageCap = pageCap
		g_InitBtnText = true
	end

	if numRec == "0" then
		low = numRec
	end
	resultsstr = resultsstr .. low

	if high > g_totalNumRec then
		high = g_totalNumRec
	end
	resultsstr = resultsstr .. " - " .. high
	resultsstr = resultsstr .. " of " .. g_totalNumRec
	Static_SetText(resultslbl, resultsstr)

	if g_InitBtnText then
		setButtonText()
		g_InitBtnText = false
	end

	return numRec
end

--  Apply gift icon to image
--  input:  icon_name - string, giftData - table
--  t_giftData[1] - texture name
--  t_giftData[2 - 5] - left, top, right, bottom coords
function setGiftIcon( icon_name, id )

	local ich = Dialog_GetImage(gDialogHandle, icon_name)
	if ich ~= 0 then
		Control_SetVisible(ich, true)
		local eh = Image_GetDisplayElement(ich)
		if eh ~= 0 then
			KEP_LoadIconTextureByID(id, eh, gDialogHandle)
		end
	end
end

function reportSpam()
	if g_selectedIndex >= 0 then
		local index = g_selectedIndex + 1
		if g_t_data[index] ~= nil then

			--Spam message details
			local s_username = "From: "..g_t_data[index]["username"].."</br>"
			local s_subject = "Subject: "..g_t_data[index]["subject"].."</br></br>"
			local s_date = "Date: "..g_t_data[index]["message_date"].."</br>"
			local s_body = g_t_data[index]["message_body"]
			local spam_report = s_username..s_date..s_subject..s_body

			--Support account details
			local name = SUPPORT_ACCT_NAME
			local subject = "Spam report notification"

			-- create really long url
			local web_address = GameGlobals.WEB_SITE_PREFIX..MESSAGE_CENTER_SUFFIX..SEND_MESSAGE_SUFFIX..name..SUBJECT_SUFFIX..subject..MESSAGE_SUFFIX..spam_report
			g_resultParse = true
			g_btnPressed = BTN_SPAM
			g_targetPerson = g_t_data[index]["username"]
			makeWebCall( web_address, WF.MESSAGES)
		end
	end
end

function ignoreUser()

	if g_selectedIndex >= 0 then
		local index = g_selectedIndex + 1

		if g_t_data[index] ~= nil then

			userID = g_t_data[index]["from_id"]

			local web_address = GameGlobals.WEB_SITE_PREFIX..IGNORE_SUFFIX

			if userID ~= nil then

				web_address = web_address..ADD_IGNORE_SUFFIX..userID

				-- Tell browser handler to use parse result function
				g_resultParse = true
				g_btnPressed = BTN_IGNORE
				g_targetPerson = g_t_data[index]["username"]

				-- Send request to website, use 0 for current page and amount per page
				makeWebCall( web_address, WF.MESSAGES)
			end
		end
	end
end

function setItemSelect( index )
	if (index >= 0) and (index < g_msgPerPage) then
		local lbh1 = Dialog_GetListBox(gDialogHandle, "lstList1")
		local lbh2 = Dialog_GetListBox(gDialogHandle, "lstList2")
		local lbh3 = Dialog_GetListBox(gDialogHandle, "lstList3")
		ListBox_SelectItem( lbh1, index )
		ListBox_SelectItem( lbh2, index )
		ListBox_SelectItem( lbh3, index )
	end
end

function lstList1_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	g_selectedIndex = sel_index
	local lbh = Dialog_GetListBox(gDialogHandle, "lstList2")
	if (ListBox_GetSelectedItemIndex(lbh) ~= sel_index) and (lbh ~= nil) then
		ListBox_SelectItem( lbh, sel_index )
	end
	lbh = Dialog_GetListBox(gDialogHandle, "lstList3")
	if (ListBox_GetSelectedItemIndex(lbh) ~= sel_index) and (lbh ~= nil) then
		ListBox_SelectItem( lbh, sel_index )
	end
end

function lstList2_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	g_selectedIndex = sel_index
	local lbh = Dialog_GetListBox(gDialogHandle, "lstList1")
	if (ListBox_GetSelectedItemIndex(lbh) ~= sel_index) and (lbh ~= nil) then
		ListBox_SelectItem( lbh, sel_index )
	end
	lbh = Dialog_GetListBox(gDialogHandle, "lstList3")
	if (ListBox_GetSelectedItemIndex(lbh) ~= sel_index) and (lbh ~= nil) then
		ListBox_SelectItem( lbh, sel_index )
	end
end

function lstList3_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	g_selectedIndex = sel_index
	local lbh = Dialog_GetListBox(gDialogHandle, "lstList1")
	if (ListBox_GetSelectedItemIndex(lbh) ~= sel_index) and (lbh ~= nil) then
		ListBox_SelectItem( lbh, sel_index )
	end
	lbh = Dialog_GetListBox(gDialogHandle, "lstList2")
	if (ListBox_GetSelectedItemIndex(lbh) ~= sel_index) and (lbh ~= nil) then
		ListBox_SelectItem( lbh, sel_index )
	end
end

function ChangeMessageViewMode(mymode)
	lblBody		= Dialog_GetControl(gDialogHandle, "lblBody")
	lblSubTag	= Static_GetControl(Dialog_GetStatic(gDialogHandle, "lblSubjectTag"))
	lblFrmTag	= Static_GetControl(Dialog_GetStatic(gDialogHandle, "lblFromTag"))
	lblDatTag	= Static_GetControl(Dialog_GetStatic(gDialogHandle, "lblDateTag"))
	imgDark	= Image_GetControl(Dialog_GetImage(gDialogHandle, "imgCenterBlocker"))
	imgDark2	= Image_GetControl(Dialog_GetImage(gDialogHandle, "imgtophider"))
	imgBody	= Image_GetControl(Dialog_GetImage(gDialogHandle, "imgBody"))
	local lb = Dialog_GetListBox(gDialogHandle, "lstBody")
	local enabled = (mymode == 1)
	Control_SetVisible(lblBody, 	enabled)
	Control_SetVisible(lblSubTag, enabled)
	Control_SetVisible(lblFrmTag, enabled)
	Control_SetVisible(lblDatTag, enabled)
	Control_SetVisible(imgDark, enabled)
	Control_SetVisible(imgDark2, enabled)
	Control_SetVisible(imgBody, enabled)
	Control_SetEnabled(ListBox_GetControl(lb), enabled)
	Control_SetVisible(ListBox_GetControl(lb), enabled)
end

function btnGift_OnButtonClicked( buttonHandle )
	local id = 0
	local name = ""
	if g_selectedIndex >= 0 then
		id = g_t_data[g_selectedIndex + 1]["from_id"]
		name = g_t_data[g_selectedIndex + 1]["username"]
	end
	MenuOpenModal("SendGift2.xml")
	local ev = KEP_EventCreate( PLAYER_USERID_EVENT )
	KEP_EventEncodeNumber( ev, id)
	KEP_EventEncodeString( ev, name)
	KEP_EventQueue( ev )
end

function btnSpam_OnButtonClicked( buttonHandle )
	if g_current_tab ~=  TAB_GIFTS then
		if g_selectedIndex >= 0 then
			g_btnPressed = BTN_SPAM
			MenuOpenModal("YesNoBox.xml")
			local yesnoEvent = KEP_EventCreate( YES_NO_BOX_EVENT )
			KEP_EventEncodeString( yesnoEvent, "Are you sure you want to report this message as spam?" )
			KEP_EventEncodeString( yesnoEvent, "Confirm" )
			KEP_EventQueue( yesnoEvent )
		else
			KEP_MessageBox("Please select an item to report as spam")
		end
	end
end

function btnIgnore_OnButtonClicked( buttonHandle )
	if g_selectedIndex >= 0 then
		local index = g_selectedIndex + 1
		if g_t_data[index] ~= nil then
			g_targetPerson = g_t_data[index]["username"]
			if g_targetPerson ~= nil then
				-- Tell browser handler to use parse result function
				g_btnPressed = BTN_IGNORE
				MenuOpenModal("YesNoBox.xml")
				local yesnoEvent = KEP_EventCreate( YES_NO_BOX_EVENT )
				KEP_EventEncodeString( yesnoEvent, "Are you sure you want to ignore "..g_targetPerson.."?" )
				KEP_EventEncodeString( yesnoEvent, "Confirm" )
				KEP_EventQueue( yesnoEvent )
			end
		end
	end
end

function btnReply_OnButtonClicked( buttonHandle )
	local id = 0
	local name = ""
	local subject = ""

	if g_current_tab == TAB_INBOX or g_current_tab == TAB_MESSAGE then

		if g_selectedIndex >= 0 then

			id = g_t_data[g_selectedIndex + 1]["from_id"]
			name = g_t_data[g_selectedIndex + 1]["username"]
			subject = g_t_data[g_selectedIndex + 1]["subject"]
			subject = "Re: " .. subject

			MenuOpenModal("SendMessage2.xml")

			local ev = KEP_EventCreate( PLAYER_USERID_EVENT )
			KEP_EventEncodeNumber( ev, id)
			KEP_EventEncodeString( ev, name)
			KEP_EventEncodeString( ev, subject )
			KEP_EventEncodeString( ev, "none" )
			KEP_EventQueue( ev )
		else
			KEP_MessageBox("Please select a message.")
		end

	elseif g_current_tab == TAB_REQUESTS then
		MenuCloseThis()

	elseif g_current_tab == TAB_GIFTS then
		g_btnPressed = BTN_REPLY

		if g_selectedIndex >= 0 then

			local lastItem = false
			if ( g_totalNumRec == (g_msgPerPage*(g_curPage-1)+g_selectedIndex+1) )  then
				lastItem = true
			end

			makeWebCall( GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..REJECT_GIFT..g_t_data[g_selectedIndex + 1]["message_id"], WF.MESSAGES)
			makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)

			if lastItem then
				if (g_selectedIndex ~= 0) then
					g_selectedIndex = g_selectedIndex - 1
					local lbh = Dialog_GetListBox(gDialogHandle, "lstList3")
					if (lbh ~= nil) then
						ListBox_SelectItem( lbh, g_selectedIndex )
					end
				end
			end

		else
			KEP_MessageBox("Please select a gift to reject")
		end

	elseif g_current_tab == TAB_VIEW_GIFT then

		--At this point we know the data we want is in the first position of the table
		makeWebCall( GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..REJECT_GIFT..g_t_data[1]["message_id"], WF.MESSAGES)
		g_resultParse = true
		ChangeMessageViewMode(0)
		getGifts()
		Static_SetText(Dialog_GetStatic(gDialogHandle, "lblTitle"), "Personal Messages")
		ch	= Image_GetControl(Dialog_GetImage(gDialogHandle, "imgGiftPic"))
		Control_SetVisible(ch, false)
	end
end

function fillMessageView()
	Static_SetText(Dialog_GetStatic(gDialogHandle, "lblTitle"), "Message Detail")
	Static_SetText(Dialog_GetStatic(gDialogHandle, "lblSubjectTag"), "<b>Subject:</b> " .. g_t_data[g_selectedIndex + 1]["subject"])
	Static_SetText(Dialog_GetStatic(gDialogHandle, "lblFromTag"), "<b>From:</b> " .. g_t_data[g_selectedIndex + 1]["username"])
	Static_SetText(Dialog_GetStatic(gDialogHandle, "lblDateTag"), "<b>Date:</b> " .. g_t_data[g_selectedIndex + 1]["message_date"])
	g_t_data[g_selectedIndex + 1]["message_body"] = string.gsub(g_t_data[g_selectedIndex + 1]["message_body"], "<[bB][rR]%s*/?>", "\n") -- replace <br>, <BR>, <br/> with \n
	g_t_data[g_selectedIndex + 1]["message_body"] = string.gsub(g_t_data[g_selectedIndex + 1]["message_body"], "%b<>", "") -- remove anything else that looks like html
	local prsdmsgtbl = letsGetFunky(g_t_data[g_selectedIndex + 1]["message_body"], "lblEmpty")
	local lb = Dialog_GetListBox(gDialogHandle, "lstBody")
	if ListBox_GetSize(lb) > 0 then
		ListBox_RemoveAllItems(lb)
	end
	for i=1,#prsdmsgtbl do
		ListBox_AddItem(lb, prsdmsgtbl[i], 1)
	end
end

function btnAccept_OnButtonClicked( buttonHandle )
	if g_selectedIndex >= 0 then
		if g_current_tab == TAB_MESSAGE then
			ChangeMessageViewMode(0)
			getInbox()
			Static_SetText(Dialog_GetStatic(gDialogHandle, "lblTitle"), "Personal Messages")
		elseif g_current_tab == TAB_INBOX then
			getMessage(TAB_MESSAGE)
			fillMessageView()
		elseif g_current_tab == TAB_REQUESTS then
			g_btnPressed = BTN_ACCEPT
			g_resultParse = true
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..ACCEPT_FRIEND..g_t_data[g_selectedIndex + 1]["from_id"], WF.MESSAGES)
		elseif g_current_tab == TAB_GIFTS then
			getMessage(TAB_VIEW_GIFT)
			fillMessageView()
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..READ_GIFT_MESSAGE..g_t_data[g_selectedIndex + 1]["message_id"], WF.MESSAGES)
			Static_SetText(Dialog_GetStatic(gDialogHandle, "lblTitle"), "Gift Details")
			g_current_tab = TAB_VIEW_GIFT
		elseif g_current_tab == TAB_VIEW_GIFT then
			ChangeMessageViewMode(0)
			getGifts()
			Static_SetText(Dialog_GetStatic(gDialogHandle, "lblTitle"), "Personal Messages")
			ch	= Image_GetControl(Dialog_GetImage(gDialogHandle, "imgGiftPic"))
			Control_SetVisible(ch, false)
			g_current_tab = TAB_GIFTS
		end
	else
		if g_current_tab == TAB_INBOX then
			KEP_MessageBox("Please select a message")
		elseif g_current_tab == TAB_REQUESTS then
			KEP_MessageBox("Please select a friend request")
		elseif g_current_tab == TAB_GIFTS then
			KEP_MessageBox("Please select a gift")
		end
	end
end

function btnReject_OnButtonClicked( buttonHandle )
	if g_selectedIndex >= 0 then
		if g_current_tab == TAB_INBOX then
			g_btnPressed = BTN_REJECT
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..MESSAGE_DELETE..g_t_data[g_selectedIndex + 1]["message_id"], WF.MESSAGES)
		elseif g_current_tab == TAB_REQUESTS then
			g_btnPressed = BTN_REJECT
			g_resultParse = true
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..REJECT_FRIEND..g_t_data[g_selectedIndex + 1]["from_id"], WF.MESSAGES)
		elseif g_current_tab == TAB_GIFTS or g_current_tab == TAB_VIEW_GIFT then
			g_btnPressed = BTN_REJECT
			local selectGift = "0"
			if g_current_tab == TAB_VIEW_GIFT then
				selectGift = tostring(g_t_data[1]["message_id"])
			else
				selectGift = tostring(g_t_data[g_selectedIndex + 1]["message_id"])
			end
			e = KEP_EventCreate( BrowserIds.BROWSER_REQUEST_EVENT_NAME );
			-- after selecting a gift to view the g_t_data table is cleared and filled with the data of the selected gift
			KEP_EventEncodeString( e, "acceptGift?message="..selectGift )
			KEP_EventAddToServer( e )
			KEP_EventQueue( e )
		elseif g_current_tab == TAB_MESSAGE then
			makeWebCall( GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..MESSAGE_DELETE..g_t_data[g_selectedIndex + 1]["message_id"], WF.MESSAGES)
			g_btnPressed = BTN_REJECT
			ChangeMessageViewMode(0)
			Static_SetText(Dialog_GetStatic(gDialogHandle, "lblTitle"), "Personal Messages")
		end
		g_totalNumRec = g_totalNumRec - 1
	else
		if g_current_tab == TAB_INBOX then
			KEP_MessageBox("Please select a message")
		elseif g_current_tab == TAB_REQUESTS then
			KEP_MessageBox("Please select a friend request")
		elseif g_current_tab == TAB_GIFTS then
			KEP_MessageBox("Please select a gift")
		end
	end
	local lb = Dialog_GetListBox(gDialogHandle, "lstBody")
	Control_SetEnabled(ListBox_GetControl(lb), false)
	Control_SetVisible(ListBox_GetControl(lb), false)
end

function btnResultsPrev_OnButtonClicked( buttonHandle )
	if g_pageCap > g_window then
		calcResultsPrev()
		alignButtons()
		makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
	end
	clearSelection()
end

function btnResultsNext_OnButtonClicked( buttonHandle )
	if g_pageCap > g_window then
		calcResultsNext()
		alignButtons()
		makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
	end
	clearSelection()
end

function btnFirst_OnButtonClicked( buttonHandle )
	if g_pageCap > g_window then
		calcFirst()
		alignButtons()
		makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
	end
	clearSelection()
end

function btnLast_OnButtonClicked( buttonHandle )
	if g_pageCap > g_window then
		calcLast()
		alignButtons()
		makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
	end
	clearSelection()
end

function btnRange1_OnButtonClicked( buttonHandle )
	calcRange1()
	makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
	clearSelection()
end

function btnRange2_OnButtonClicked( buttonHandle )
	calcRange2()
	makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
	clearSelection()
end

function btnRange3_OnButtonClicked( buttonHandle )
	calcRange3()
	makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
	clearSelection()
end

function btnRange4_OnButtonClicked( buttonHandle )
	calcRange4()
	makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
	clearSelection()
end

function getGifts()
	local sh = nil
	local bh = nil

	g_current_tab = TAB_GIFTS
	clearSelection()

	bh = Dialog_GetButton(gDialogHandle, "btnSortFrom")
	sh = Button_GetStatic(bh)
	Static_SetText(sh, "From")

	bh = Dialog_GetButton(gDialogHandle, "btnSortSubject")
	sh = Button_GetStatic(bh)
	Static_SetText(sh, "Gift they gave you")

	bh = Dialog_GetButton(gDialogHandle, "btnSortSent")
	sh = Button_GetStatic(bh)
	Static_SetText(sh, "Sent")

	sh = Dialog_GetStatic(gDialogHandle, "lblAcceptMessage")
	Static_SetText(sh, "Accept Gift")

	sh = Dialog_GetStatic(gDialogHandle, "lblHelpMessage")
	Static_SetText(sh, "Click a line, and then Accept or Deny the gift.")

	sh = Dialog_GetStatic(gDialogHandle, "lblRejectMessage")
	Static_SetText(sh, "Deny Gift")
	butt = Dialog_GetButton(gDialogHandle, "btnReject")
	de = Button_GetNormalDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)
	de = Button_GetMouseOverDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)
	de = Button_GetFocusedDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)
	de = Button_GetPressedDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)

	butt = Dialog_GetButton(gDialogHandle, "btnReply")
	de = Button_GetNormalDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)
	de = Button_GetMouseOverDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)
	de = Button_GetFocusedDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)
	de = Button_GetPressedDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)

	sh = Dialog_GetStatic(gDialogHandle, "Static91")
	Control_SetVisible(Static_GetControl(sh), false)

	bh = Dialog_GetButton(gDialogHandle, "btnReply")
	Control_SetVisible(Button_GetControl(bh), true)

	bh = Dialog_GetButton(gDialogHandle, "btnSpam")
	Control_SetVisible(Button_GetControl(bh), false)

	bh = Dialog_GetButton(gDialogHandle, "btnIgnore")
	Control_SetVisible(Button_GetControl(bh), false)

	bh = Dialog_GetButton(gDialogHandle, "btnGift")
	setButtonState(bh, true)

	sh = Dialog_GetStatic(gDialogHandle, "lblSpam")
	Control_SetVisible(sh, false)

	sh = Dialog_GetStatic(gDialogHandle, "lblIgnore")
	Control_SetVisible(sh, false)

	g_web_address = GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..GIFTS_RECIEVED

	g_curPage = 1
	g_set = 1

	g_InitBtnText = true

	resetPageButtonSelection()

	makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
end

function getMessage( cur_tab )
	g_current_tab = cur_tab
	ChangeMessageViewMode(1)
	sh = Dialog_GetStatic(gDialogHandle, "lblAcceptMessage")
	Static_SetText(sh, "Back to Inbox")
	sh = Dialog_GetStatic(gDialogHandle, "lblRejectMessage")
	Static_SetText(sh, "Delete")
end

function getInbox()
	local sh = nil
	local bh = nil

	g_current_tab = TAB_INBOX
	clearSelection()

	bh = Dialog_GetButton(gDialogHandle, "btnSortFrom")
	sh = Button_GetStatic(bh)
	Static_SetText(sh, "From")

	bh = Dialog_GetButton(gDialogHandle, "btnSortSubject")
	sh = Button_GetStatic(bh)
	Static_SetText(sh, "Subject")

	bh = Dialog_GetButton(gDialogHandle, "btnSortSent")
	sh = Button_GetStatic(bh)
	Static_SetText(sh, "Sent")

	sh = Dialog_GetStatic(gDialogHandle, "lblAcceptMessage")
	Static_SetText(sh, "View Message")
	butt = Dialog_GetButton(gDialogHandle, "btnReject")
	de = Button_GetNormalDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)
	de = Button_GetMouseOverDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)
	de = Button_GetFocusedDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)
	de = Button_GetPressedDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)

	butt = Dialog_GetButton(gDialogHandle, "btnReply")
	de = Button_GetNormalDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)
	de = Button_GetMouseOverDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)
	de = Button_GetFocusedDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)
	de = Button_GetPressedDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)

	sh = Dialog_GetStatic(gDialogHandle, "lblHelpMessage")
	Static_SetText(sh, "Choose a message to view or delete.")

	sh = Dialog_GetStatic(gDialogHandle, "lblRejectMessage")
	Static_SetText(sh, "Delete Message")

	sh = Dialog_GetStatic(gDialogHandle, "Static91")
	Static_SetText(sh, "Reply to Message")
	Control_SetVisible(Static_GetControl(sh), true)

	bh = Dialog_GetButton(gDialogHandle, "btnReply")
	Control_SetVisible(Button_GetControl(bh), true)

	bh = Dialog_GetButton(gDialogHandle, "btnSpam")
	Control_SetVisible(Button_GetControl(bh), true)

	bh = Dialog_GetButton(gDialogHandle, "btnIgnore")
	Control_SetVisible(Button_GetControl(bh), true)

	bh = Dialog_GetButton(gDialogHandle, "btnGift")
	setButtonState(bh, false)

	sh = Dialog_GetStatic(gDialogHandle, "lblSpam")
	Control_SetVisible(sh, true)

	sh = Dialog_GetStatic(gDialogHandle, "lblIgnore")
	Control_SetVisible(sh, true)

	g_curPage = 1
	g_set = 1

	g_InitBtnText = true

	resetPageButtonSelection()

	g_web_address = GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..MESSAGE_READ

	makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
end

function getFriendReq()
	local sh = nil
	local bh = nil
	g_current_tab = TAB_REQUESTS
	clearSelection()

	bh = Dialog_GetButton(gDialogHandle, "btnSortFrom")
	sh = Button_GetStatic(bh)
	Static_SetText(sh, "From")

	bh = Dialog_GetButton(gDialogHandle, "btnSortSubject")
	sh = Button_GetStatic(bh)
	Static_SetText(sh, "Sent on")

	bh = Dialog_GetButton(gDialogHandle, "btnSortSent")
	sh = Button_GetStatic(bh)
	Static_SetText(sh, "")

	sh = Dialog_GetStatic(gDialogHandle, "lblAcceptMessage")
	Static_SetText(sh, "Accept Friend")

	butt = Dialog_GetButton(gDialogHandle, "btnReject")
	de = Button_GetNormalDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)
	de = Button_GetMouseOverDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)
	de = Button_GetFocusedDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)
	de = Button_GetPressedDisplayElement(butt)
	Element_SetCoords(de, 8, 342, 44, 375)

	butt = Dialog_GetButton(gDialogHandle, "btnReply")
	de = Button_GetNormalDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)
	de = Button_GetMouseOverDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)
	de = Button_GetFocusedDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)
	de = Button_GetPressedDisplayElement(butt)
	Element_SetCoords(de, 49, 356, 83, 384)

	sh = Dialog_GetStatic(gDialogHandle, "lblRejectMessage")
	Static_SetText(sh, "Deny as Friend")

	sh = Dialog_GetStatic(gDialogHandle, "lblHelpMessage")
	Static_SetText(sh, "Click a line, and then Accept or Deny request.")

	bh = Dialog_GetButton(gDialogHandle, "btnReply")
	Control_SetVisible(Button_GetControl(bh), true)

	sh = Dialog_GetStatic(gDialogHandle, "Static91")
	Static_SetText(sh, "Friends List")
	Control_SetVisible(Static_GetControl(sh), true)

	bh = Dialog_GetButton(gDialogHandle, "btnSpam")
	Control_SetVisible(Button_GetControl(bh), false)

	bh = Dialog_GetButton(gDialogHandle, "btnIgnore")
	Control_SetVisible(Button_GetControl(bh), true)

	bh = Dialog_GetButton(gDialogHandle, "btnGift")
	setButtonState(bh, false)

	sh = Dialog_GetStatic(gDialogHandle, "lblSpam")
	Control_SetVisible(sh, false)

	sh = Dialog_GetStatic(gDialogHandle, "lblIgnore")
	Control_SetVisible(sh, true)

	g_curPage = 1
	g_set = 1

	g_InitBtnText = true

	resetPageButtonSelection()

	g_web_address = GameGlobals.WEB_SITE_PREFIX..MESSAGE_INBOX..FRIEND_REQUESTS

	makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
end

function btnInbox_OnButtonClicked( buttonHandle )
	bh = Dialog_GetButton(gDialogHandle, "btnInbox")
	setButtonState(bh, false)

	bh = Dialog_GetButton(gDialogHandle, "btnFriendRequests")
	setButtonState(bh, true)

	bh = Dialog_GetButton(gDialogHandle, "btnGifts")
	setButtonState(bh, true)

	sh = Dialog_GetStatic(gDialogHandle, "lblAge")
	Static_SetText(sh, "")

	getInbox()
end

function btnFriendRequests_OnButtonClicked( buttonHandle )
	bh = Dialog_GetButton(gDialogHandle, "btnInbox")
	setButtonState(bh, true)

	bh = Dialog_GetButton(gDialogHandle, "btnFriendRequests")
	setButtonState(bh, false)

	bh = Dialog_GetButton(gDialogHandle, "btnGifts")
	setButtonState(bh, true)

	sh = Dialog_GetStatic(gDialogHandle, "lblAge")
	Static_SetText(sh, "Age")

	getFriendReq()
end

function btnGifts_OnButtonClicked( buttonHandle )
	bh = Dialog_GetButton(gDialogHandle, "btnInbox")
	setButtonState(bh, true)

	bh = Dialog_GetButton(gDialogHandle, "btnFriendRequests")
	setButtonState(bh, true)

	bh = Dialog_GetButton(gDialogHandle, "btnGifts")
	setButtonState(bh, false)

	sh = Dialog_GetStatic(gDialogHandle, "lblAge")
	Static_SetText(sh, "")

	getGifts()
end

-- Grab text from edit box and pass off to search function
function btnSearch_OnButtonClicked( buttonHandle )
	local searchText = ""
	ebh = Dialog_GetEditBox( gDialogHandle, "editSearchField" )
	if ebh ~= nil then
		searchText = EditBox_GetText( ebh )
	end
	initSearch( searchText )
end

-- Send search request to server
function initSearch( searchText )
	local cbh = Dialog_GetCheckBox( gDialogHandle, "cbOffline" )
	if cbh == nil then
		return
	end

	if (searchText ~= nil) and (CheckBox_GetChecked( cbh ) == 1) then
		g_web_address = GameGlobals.WEB_SITE_PREFIX..SEARCH_SUFFIX..searchText
	end

	g_curPage = 1
	g_set = 1

	setButtonText(gDialogHandle, g_set, g_window, g_pageCap)
	alignButtons()
	makeWebCall( g_web_address, WF.MESSAGES, g_curPage, g_msgPerPage)
end

-- Clear listbox selections
function clearSelection()
	g_selectedIndex = -1
	if g_totalNumRec > 0 then
		lh = Dialog_GetListBox(gDialogHandle, "lstList1")
		lhO = Dialog_GetListBox(gDialogHandle, "lstList2")
		lhO2 = Dialog_GetListBox(gDialogHandle, "lstList3")
		if lh ~= nil then
			ListBox_ClearSelection(lh)
		end
		if lhO ~= nil then
			ListBox_ClearSelection(lhO)
		end
		if lhO2 ~= nil then
			ListBox_ClearSelection(lhO2)
		end
	end
end

-- Added by Jonny to handle alignment of the page buttons at the bottom.
function alignButtons()
	local imgRightBar = Dialog_GetImage(gDialogHandle, "right arrow bar")
	Control_SetLocation(imgRightBar, 450, 177)
	local btnRightRightArrow = Dialog_GetButton(gDialogHandle, "btnLast")
	Control_SetLocation(btnRightRightArrow, 432, 177)
	local btnRightArrow = Dialog_GetButton(gDialogHandle, "btnResultsNext")
	Control_SetLocation(btnRightArrow, 412, 177)

	local t = {}		-- new table
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange1"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange2"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange3"))
	table.insert(t, Dialog_GetButton(gDialogHandle, "btnRange4"))
	rightAlignPageButtons(t, 407, 179, 5)

	local leftMostXPos = Control_GetLocationX(Dialog_GetButton(gDialogHandle, "btnRange1"))

	local btnLeftArrow = Dialog_GetButton(gDialogHandle, "btnResultsPrev")
	local leftArrowWidth = Control_GetWidth(btnLeftArrow)
	leftMostXPos = leftMostXPos - leftArrowWidth - 5
	Control_SetLocation(btnLeftArrow, leftMostXPos, 177)

	local btnLeftLeftArrow = Dialog_GetButton(gDialogHandle, "btnFirst")
	local leftLeftArrowWidth = Control_GetWidth(btnLeftLeftArrow)
	leftMostXPos = leftMostXPos - leftLeftArrowWidth - 5
	Control_SetLocation(btnLeftLeftArrow, leftMostXPos, 177)

	local imgLeftBar = Dialog_GetImage(gDialogHandle, "left arrow bar")
	local leftBarWidth = Control_GetWidth(imgLeftBar)
	leftMostXPos = leftMostXPos - leftBarWidth - 2
	Control_SetLocation(imgLeftBar, leftMostXPos, 177)
end

function getButtonHandles()
	local ch = Dialog_GetControl(gDialogHandle, "btnClose")
	if ch ~= nil then
		g_t_btnHandles["btnClose"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnInbox")
	if ch ~= nil then
		g_t_btnHandles["btnInbox"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnFriendRequests")
	if ch ~= nil then
		g_t_btnHandles["btnFriendRequests"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnGifts")
	if ch ~= nil then
		g_t_btnHandles["btnGifts"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSortFrom")
	if ch ~= nil then
		g_t_btnHandles["btnSortFrom"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSortSubject")
	if ch ~= nil then
		g_t_btnHandles["btnSortSubject"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSortSent")
	if ch ~= nil then
		g_t_btnHandles["btnSortSent"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnAccept")
	if ch ~= nil then
		g_t_btnHandles["btnAccept"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnReject")
	if ch ~= nil then
		g_t_btnHandles["btnReject"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnReply")
	if ch ~= nil then
		g_t_btnHandles["btnReply"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnIgnore")
	if ch ~= nil then
		g_t_btnHandles["btnIgnore"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnSpam")
	if ch ~= nil then
		g_t_btnHandles["btnSpam"] = ch
	end

	ch = Dialog_GetControl(gDialogHandle, "btnGift")
	if ch ~= nil then
		g_t_btnHandles["btnGift"] = ch
	end
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	local sh = Dialog_GetStatic(dialogHandle, "lblStatus")
	if sh ~= nil then

		if Control_ContainsPoint(g_t_btnHandles["btnClose"], x, y) ~= 0 then
			Static_SetText(sh, "Close This Menu")
		elseif Control_ContainsPoint(g_t_btnHandles["btnInbox"], x, y) ~= 0 then
			Static_SetText(sh, "View Your Inbox Messages")
		elseif Control_ContainsPoint(g_t_btnHandles["btnGift"], x, y) ~= 0 then
			if g_current_tab == TAB_GIFTS then
				Static_SetText(sh, "Send a Gift to Someone")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnFriendRequests"], x, y) ~= 0 then
			Static_SetText(sh, "View Your Friend Requests")
		elseif Control_ContainsPoint(g_t_btnHandles["btnGifts"], x, y) ~= 0 then
		--Static_SetText(sh, "View Your Gifts")
		elseif Control_ContainsPoint(g_t_btnHandles["btnSortFrom"], x, y) ~= 0 then
			Static_SetText(sh, "Sort List by Who Sent This Item")
		elseif Control_ContainsPoint(g_t_btnHandles["btnSortSubject"], x, y) ~= 0 then
			Static_SetText(sh, "Sort List by the Item Subject")
		elseif Control_ContainsPoint(g_t_btnHandles["btnSortSent"], x, y) ~= 0 then
			Static_SetText(sh, "Sort List by When the Item Was Sent")
		elseif Control_ContainsPoint(g_t_btnHandles["btnAccept"], x, y) ~= 0 then
			if g_current_tab == TAB_MESSAGE then
				Static_SetText(sh, "Go Back to Your Inbox")
			elseif g_current_tab == TAB_INBOX then
				Static_SetText(sh, "View the Selected Message")
			elseif g_current_tab == TAB_REQUESTS then
				Static_SetText(sh, "Accept This Person's Friend Request")
			elseif g_current_tab == TAB_GIFTS then
				Static_SetText(sh, "Open the Selected Gift")
			elseif g_current_tab == TAB_VIEW_GIFT then
				Static_SetText(sh, "Go Back to Gift Inbox")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnReject"], x, y) ~= 0 then
			if g_current_tab == TAB_MESSAGE then
				Static_SetText(sh, "Delete This Message")
			elseif g_current_tab == TAB_INBOX then
				Static_SetText(sh, "Delete the Selected Message")
			elseif g_current_tab == TAB_REQUESTS then
				Static_SetText(sh, "Deny this Person's Friend Request")
			elseif g_current_tab == TAB_GIFTS or g_current_tab == TAB_VIEW_GIFT then
				Static_SetText(sh, "Accept the Selected Gift")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnReply"], x, y) ~= 0 then
			if g_current_tab == TAB_MESSAGE or g_current_tab == TAB_INBOX then
				Static_SetText(sh, "Reply to This Message")
			elseif g_current_tab == TAB_REQUESTS then
				Static_SetText(sh, "Open the Friends & People Menu")
			elseif g_current_tab == TAB_GIFTS or g_current_tab == TAB_VIEW_GIFT then
				Static_SetText(sh, "Reject the Selected Gift")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnIgnore"], x, y) ~= 0 then
			if g_current_tab ~= TAB_GIFTS then
				Static_SetText(sh, "Ignore the Person that Sent This Item")
			end
		elseif Control_ContainsPoint(g_t_btnHandles["btnSpam"], x, y) ~= 0 then
			if g_current_tab == TAB_MESSAGE or g_current_tab == TAB_INBOX then
				Static_SetText(sh, "Report This Item as Spam")
			end
		else
			Static_SetText(sh, "Choose a Message to View or Delete")
		end
	end
end
