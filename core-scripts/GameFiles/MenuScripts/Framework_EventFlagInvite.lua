--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("MenuHelper.lua")




function onCreate()

	Events.registerHandler("FRAMEWORK_EVENT_FLAG_START", updateEventFlagMenu)
end

function  onDestroy( )
	Events.sendEvent("FRAMEWORK_EVENT_FLAG_MENU", {open = false})
end

----------------------------
-- Button Handlers
---------------------------

function btnAttend_OnButtonClicked(btnHandle)
	Events.sendEvent("FRAMEWORK_ACCEPT_EVENT")
	MenuCloseThis()
end

function btnDecline_OnButtonClicked(btnHandle)
	Events.sendEvent("FRAMEWORK_DECLINE_EVENT")
	MenuCloseThis()
end
----------------------------
-- Event Handlers
---------------------------
function updateEventFlagMenu(event)


	local message = event.info
	if message then

		Static_SetText(gHandles["stcHostMessage"], message.owner.." is hosting an event in this world!")
		Control_SetVisible(gHandles["stcHostMessage"], true)

		local eventTitle = UnescapeHTML(message.title)
		Static_SetText(gHandles["stcEventName"],eventTitle)
		Control_SetVisible(gHandles["stcEventName"], true)

		local eventDescription = UnescapeHTML(message.description)
		Static_SetText(gHandles["stcDescription"], eventDescription)
		Control_SetVisible(gHandles["stcDescription"], true)
	end

	Events.sendEvent("FRAMEWORK_EVENT_FLAG_MENU", {open = true})
end