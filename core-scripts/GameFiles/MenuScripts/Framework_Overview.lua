--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Welcome.lua
--
-- Welcoming players to Kaneva since 2016
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("MenuAnimation.lua")

--Settings of the overview
local m_settings = {}

--CONSTANTS
local MAX_IMAGE_HEIGHT = 537
local MAX_IMAGE_WIDTH = 945


--Other variables
local m_imageHeight = 0
local m_imageWidth = 0

local m_preview = false

local m_imageSet = false

local MARKER_SIZE = 63
local MARKER_OFFSET_X = 32
local MARKER_OFFSET_Y = 55

--Do not let the marker appear outside the menu
local POS_X_MAX = 895
local POS_Y_MAX = 478
local POS_X_MIN = -13
local POS_Y_MIN = -2

-- Called when the menu is created
function onCreate()
	
	KEP_EventRegisterHandler("openOverviewHandler", "FRAMEWORK_OPEN_OVERVIEW", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)

	m_lstDescription = gHandles["lstDescription"]
	m_imgOverview = gHandles["imgOverview"] 
	m_imgOverviewBG = gHandles["imgOverviewBG"]
	m_imgDispElement = Image_GetDisplayElement(m_imgOverview)
end


function openOverviewHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local settingString = KEP_EventDecodeString(tEvent)
	if KEP_EventMoreToDecode(tEvent) == 1 then
		m_preview = true
	end

	Control_SetVisible(gHandles["btnClose"], m_preview)
	Control_SetEnabled(gHandles["btnClose"], m_preview)

	m_settings = Events.decode(settingString)

	local imageFile = m_settings.overviewImage
	local lastSlashIndex = string.find(imageFile, "/[^/]*$")
	local periodIndex = string.find(imageFile,  '.', 1, true)
	if lastSlashIndex and periodIndex then
		local imageName =  string.sub(imageFile, lastSlashIndex + 1, periodIndex - 1) or "none"
		m_imageFile = downloadImage(imageFile, imageName.."_OverviewScreen")
	else
		updateDisplay()
	end
end


-- function Dialog_OnRender(dialogHandle, fElapsedTime)
-- 	if MenuIsOpen("BrowsePlaces") then
-- 		local travelLoc = MenuGetLocation("BrowsePlaces")

-- 		MenuSetLocationThis( travelLoc.x, travelLoc.y + 70)
-- 	end
-- end
--------------------
--IMAGE FUNCTIONS--
--------------------
function downloadImage(imgURL, chapterName)
	--the name the image will saved as
	local filename = tostring(chapterName)..".jpg"
	local path = KEP_DownloadTextureAsyncToGameId(filename, imgURL)
	
    local gameId = KEP_GetGameId()
	
	path = string.gsub(path,".-\\", "" )
	path = "../../CustomTexture/".. gameId .. "/" .. path
	return path
end

function textureDownloadHandler()
	updateImage()
end

function updateImage()
	if m_imageFile  then
		Element_AddTexture(m_imgDispElement, gDialogHandle,m_imageFile)
		local textureWidth = Element_GetTextureWidth(m_imgDispElement, gDialogHandle)
		local textureHeight = Element_GetTextureHeight(m_imgDispElement, gDialogHandle)

		
		if not m_imageSet then
			m_imageWidth = textureWidth
			scaleWelcomeImage(textureWidth, textureHeight)
		end
	else
		m_imageHeight = 0
	end
end

function scaleWelcomeImage(width, height)
	local newHeight = height
	if width ~= MAX_IMAGE_WIDTH then
		newHeight = height * (MAX_IMAGE_WIDTH / width) 
	end
	setWelcomeImageY(width, newHeight)

	Element_SetCoords(m_imgDispElement, 0, 0, LeastPowerofTwo(width), LeastPowerofTwo(height))
end

function setWelcomeImageY(width, height)
	Control_SetSize(m_imgOverview, MAX_IMAGE_WIDTH - 2, height)
	m_imageHeight = height
	if height > MAX_IMAGE_HEIGHT then
		local heightDifference  = height - MAX_IMAGE_HEIGHT
		local moveDistance = heightDifference / 2
		Control_SetLocation(m_imgOverview, Control_GetLocationX(m_imgOverview), Control_GetLocationY(m_imgOverview)-moveDistance)

		local containerX, containerY = Control_GetLocationX(m_imgOverviewBG), Control_GetLocationY(m_imgOverviewBG)
		local containerW, containerH = Control_GetWidth(m_imgOverviewBG), Control_GetHeight(m_imgOverviewBG)
		Control_SetClipRect(m_imgOverview, containerX, containerY, containerX+containerW, containerY+containerH)

		m_imageHeight = MAX_IMAGE_HEIGHT
	end

	m_imageSet = true
	updateDisplay()
end

------------------
--Screen Display--
------------------
function  updateDisplay()
	addMarkers()
end

function addMarkers()
	if m_settings.markers then
		for index, marker in pairs(m_settings.markers) do

			if marker.destination and marker.destination.name then
				local posX = (marker.positionX or MARKER_OFFSET_X) - MARKER_OFFSET_X
				local posY = (marker.positionY or MARKER_OFFSET_Y)  - MARKER_OFFSET_Y

				posX = math.max(posX, POS_X_MIN)
				posY = math.max(posY,POS_Y_MIN)

				posX = math.min(posX, POS_X_MAX)
				posY = math.min(posY,POS_Y_MAX)

				local markerColor = {a = 255, r = marker.colorR, g = marker.colorG, b = marker.colorB}
				addImage("imgMarker"..tostring(index), rect(posX, posY, MARKER_SIZE, MARKER_SIZE), markerColor, "mapMarkerTest.tga", rect(0, 0, MARKER_SIZE, MARKER_SIZE))
				addImage("imgMarkerHightlight"..tostring(index), rect(posX, posY, MARKER_SIZE, MARKER_SIZE), markerColor, "mapMarkerTest.tga", rect(MARKER_SIZE+1, 0, MARKER_SIZE, MARKER_SIZE))

				local markerImg = Dialog_GetImage(gDialogHandle,"imgMarker"..tostring(index))
				local markerImgHighlight = Dialog_GetImage(gDialogHandle,"imgMarkerHightlight"..tostring(index))
				Control_SetVisible(markerImgHighlight, false)

				local btnMarker = addButton("btnMarker"..tostring(index), " ", rect(posX, posY, MARKER_SIZE, MARKER_SIZE), "invisible.tga", rect(0, 0, 1, 1))

				_G["btnMarker" .. tostring(index) .. "_OnMouseEnter"] = function(buttonHandle)	
					Control_SetVisible(markerImg, false)
					Control_SetVisible(markerImgHighlight, true)
				end

				_G["btnMarker" .. tostring(index) .. "_OnMouseLeave"] = function(buttonHandle)
					Control_SetVisible(markerImg, true)
					Control_SetVisible(markerImgHighlight, false)
				end
				
				Control_SetToolTip(btnMarker, "Click to travel to " .. marker.destination.name)
				if m_preview == false then
					_G["btnMarker" .. tostring(index) .. "_OnButtonClicked"] = function(buttonHandle)
						Events.sendEvent("FRAMEWORK_LINKED_ZONE_TELEPORT", {destination = marker.destination})
					end
				else
					Dialog_MoveControlToFront(gDialogHandle, gHandles["btnClose"])
				end

			end
		end
	end
end



----------------------------
-----Control Functions------
----------------------------

rect = function(x, y, w, h)
    return { x = x, y = y, w = w, h = h }
end

addImage = function(name, rect, color, texName, texRect)
    local img = Dialog_GetImage(gDialogHandle, name)
    if img == nil then
        img = Dialog_AddImage(gDialogHandle, name, rect.x, rect.y, texName, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
        Image_SetColor(img, color)
        Control_SetSize(Image_GetControl(img), rect.w, rect.h)
    end
    return img
end

addButton = function(name, text, rect, texName, texRect)
    local btn = Dialog_GetButton(gDialogHandle, name)
    if btn == nil then
        btn = Dialog_AddButton(gDialogHandle, name, text, rect.x, rect.y, rect.w, rect.h)
    end

    local elem
    elem = Button_GetDisabledDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetFocusedDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetMouseOverDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetNormalDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetPressedDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)

    return btn
end