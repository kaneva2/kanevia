--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_OverwriteConfirmation.lua
-- 
-- Overwrite confirmation menu
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

----------------------
-- Globals
----------------------
local m_name, m_conflictingUNID, m_mode, m_itemData

-- When the menu is created
function onCreate()
	-- Register client event handlers
	KEP_EventRegisterHandler("overwriteConfirmationHandler", "FRAMEWORK_OVERWRITE_CONFIRMATION", KEP.HIGH_PRIO)
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Catches relevant data for this menu to display
function overwriteConfirmationHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local itemType, GLID
	-- Better safe than sorry
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		m_name = KEP_EventDecodeString(tEvent)
	end
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		itemType = KEP_EventDecodeString(tEvent)
	end
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		GLID = KEP_EventDecodeNumber(tEvent)
	end
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		m_conflictingUNID = KEP_EventDecodeNumber(tEvent)
	end
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		m_mode = KEP_EventDecodeString(tEvent)
	end
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		m_itemData = JSON.decode(KEP_EventDecodeString(tEvent))
	end
	
	-- Set GLID icon
	local eh = Image_GetDisplayElement(gHandles["imgNewItem"])
	KEP_LoadIconTextureByID(GLID, eh, gDialogHandle)
	
	-- Capitilize first letter of the system
	local itemSystem = itemType:gsub("^%l", string.upper)
	Static_SetText(gHandles["stcText"], "Are you sure you want to overwrite "..tostring(m_name).." in your "..tostring(itemSystem).." systems?")
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

-- Save
function btnOverwrite_OnButtonClicked(buttonHandle)
	-- Close the Name Resolver dialog
	MenuClose("Framework_NameResolver.xml")
	
	-- Overwrite the passed Game Item
	Events.sendEvent("WOK_ADD_TO_GAME", {item = m_itemData, mode = m_mode, overwrite = true, UNIDToOverwrite = m_conflictingUNID})
	
	MenuCloseThis()
end