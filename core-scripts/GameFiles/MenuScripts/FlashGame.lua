--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

g_dialogSec = 0 -- dialog elapsed time (sec)

g_fadeAlpha = 0 -- fade to black alpha value
g_fadeAlphaMax = 230.0 -- fade to black alpha max value (255.0=black)
g_fadeAlphaInc = 5.0 -- fade to black alpha increment 

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "StartFlashGameEventHandler", "StartFlashGameEvent", KEP.HIGH_PRIO ) 
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "StartFlashGameEvent", KEP.MED_PRIO ) 
end

function Dialog_OnCreate( dialogHandle )
	Helper_Dialog_OnCreate( dialogHandle )
	KEP_SetMenuSelectMode( 1.0 )
	SetFadeCoords()
end

function Dialog_OnDestroy( dialogHandle )
	Dialog_RemoveControl(gDialogHandle, "flashControl")
	KEP_SetMenuSelectMode( 0.0 )
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnMoved(dialogHandle, x, y)
    SetFadeCoords()
end

function Dialog_OnRender(dialogHandle, elapsedSec)
	
    -- Update Dialog Time
    g_dialogSec = g_dialogSec + elapsedSec

    -- Update Fade Alpha
    g_fadeAlpha = g_fadeAlpha + g_fadeAlphaInc
    if (g_fadeAlpha > g_fadeAlphaMax) then
        g_fadeAlpha = g_fadeAlphaMax
    end
    SetFadeAlpha(g_fadeAlpha)
end

--[[ DRF - Set imgFade image alpha value for background fade. ]]
function SetFadeAlpha(alpha)
    local ctrl = gHandles["imgFade"]
    color = {a = alpha, r = 0, g = 0, b = 0}
    BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(ctrl)), 0, color)
    SetControlEnabled("imgFade", true)
end

--[[ DRF - Set imgFade image coordinates to fill entire screen for background fade. ]]
function SetFadeCoords()
    local ctrl = gHandles["imgFade"]
    local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle) + 32
	local dialogX = Dialog_GetLocationX(gDialogHandle)
	local dialogY = Dialog_GetLocationY(gDialogHandle) + 32
	Control_SetLocation(ctrl, 0 - dialogX, 0 - dialogY)
	Control_SetSize(ctrl, screenWidth, screenHeight)
    MenuBringToFrontThis()
end

function StartFlashGameEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )	
	
	-- Decode Flash Parameters
	swfName = KEP_EventDecodeString( event ) or ""
	swfParam = KEP_EventDecodeString( event ) or ""
	fh = Dialog_GetStatic( gDialogHandle, "flashControl" )
	if (fh == nil) or (swfName == "") then
		LogWarn("StartFlashGameEventHandler: Invalid Parameters - swfName='"..swfName.."' swfParam='"..swfParam.."'")
		MenuCloseThis()
		return KEP.EPR_OK
	end
	
	-- Start Flash Game
	Log("StartFlashGameEventHandler: OK - '"..swfName.." "..swfParam.."'")
	Static_SetText(	fh, swfName )

	return KEP.EPR_OK
end
