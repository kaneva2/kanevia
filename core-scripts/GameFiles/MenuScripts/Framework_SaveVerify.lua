--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_SaveVerify.lua
-- 
-- Verifies a Game Item Save / Copy to Game / Create Spawner
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
--dofile("..\\Scripts\\ZoneNavIds.lua") -- drf - dofiled by LibClient_Common->LibClient_Events->MenuHelpers

----------------------
-- Statics
----------------------
local MENU_WIDTH = 323

----------------------
-- Globals
----------------------
local m_item, m_mode, m_itemCreated, m_spawner, m_worldName, m_spawnerUNID

----------------------
-- Local Functions
----------------------
local setDisplay -- ()
local checkDisplayConfig -- (mode)

-- When the menu is created
function onCreate()
	-- Register events from the server
	Events.registerHandler("FRAMEWORK_SAVE_INFO", saveInfoHandler)

	-- Register client event handlers
	KEP_EventRegisterHandler("saveVerifyHandler", "FRAMEWORK_SAVE_VERIFY", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("zoneNavigationEventHandler", "ZoneNavigationEvent", KEP.MED_PRIO)
	
	-- Request the current URL
	local ev = KEP_EventCreate("ZoneNavigationEvent")
	KEP_EventSetFilter(ev, ZONE_NAV.CURRENT_URL_REQUEST)
	KEP_EventQueue(ev)
end

-- Called when the menu is destroyed
function onDestroy()
	local config = KEP_ConfigOpenWOK()
	if CheckBox_GetChecked(gHandles["boxDontShowAgain"]) == 1 then
		if m_mode == "Inventory" then
			KEP_ConfigSetNumber(config, "DoNotShowCopyToInventoryConfirmation", 1)
		elseif m_mode == "AddToGame" then
			KEP_ConfigSetNumber(config, "DoNotShowCopyToGameConfirmation", 1)
		elseif m_mode == "CreateSpawner" then
			KEP_ConfigSetNumber(config, "DoNotShowCreateSpawnerConfirmation", 1)
		elseif m_mode == "Place" then
			KEP_ConfigSetNumber(config, "DoNotShowPlaceObjectConfirmation", 1)
		end
		KEP_ConfigSave(config)
	end
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Catches save verify info from Controller_Inventory
function saveInfoHandler(event)
	m_UNID		  = event.UNID
	m_item 		  = event.item
	m_mode 		  = event.mode
	m_itemCreated = event.itemCreated
	m_spawner 	  = event.spawner
	m_spawnerUNID = event.spawnerUNID
	
	-- Destroy the menu based on config or display?
	if checkDisplayConfig(m_mode) then
		MenuCloseThis()
	else
		if m_worldName then
			setDisplay()
		end
	end
end

-- Catches relevant data for this menu to display from Framework_PublishGameItem.lua
function saveVerifyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)

	-- Better safe than sorry
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		m_item = JSON.decode(KEP_EventDecodeString(tEvent))
	end
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		m_mode = KEP_EventDecodeString(tEvent)
	end
	
	-- Destroy the menu based on config or display?
	if checkDisplayConfig(m_mode) then
		MenuCloseThis()
	else
		if m_worldName then
			setDisplay()
		end
	end
end

-- Zone navigation events
function zoneNavigationEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	if filter == ZONE_NAV.CURRENT_URL_INFO then
		local url = KEP_EventDecodeString(tEvent)
		m_worldName = parseFriendlyNameFromURL(url)
		if m_item then
			setDisplay()
		end
	end
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

-- View properties of the generated spawner
function btnSpawnerProperties_OnButtonClicked(buttonHandle)
	-- Open Spawner menu
	Events.sendEvent("FRAMEWORK_GOTO_ITEM", {UNID = tonumber(m_spawnerUNID)})
	MenuCloseThis()
end

-- Inspect added Game Item
function btnAddedGameItem1_OnButtonClicked(buttonHandle)
	Events.sendEvent("FRAMEWORK_GOTO_ITEM", {UNID = tonumber(m_UNID)})
	--[[-- Open Systems menu by type
	if m_item.itemType == "character" then
		MenuOpen("Framework_Characters.xml")
	elseif m_item.itemType == "placeable" then
		MenuOpen("Framework_Placeables.xml")
	elseif m_item.itemType == "armor" or m_item.itemType == "generic" or
		   m_item.itemType == "weapon" or m_item.itemType == "consumable" or 
		   m_item.itemType == "ammo" then
		MenuOpen("Framework_WorldInventory.xml")
	elseif m_item.itemType == "harvestable" then
		MenuOpen("Framework_Harvestables.xml")
	end]]

	-- TODO: Select given item

	MenuCloseThis()
end

-- View Game Item in the Inventory
function btnViewInSystem_OnButtonClicked(buttonHandle)
	-- View added Game Item in WOK Inventory
	if m_mode == "Inventory" then

		if MenuIsClosed("UnifiedNavigation.xml") then
			MenuOpen("UnifiedNavigation.xml")
		end

		local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
		KEP_EventEncodeString(ev, "wokGaming")
		KEP_EventQueue(ev)
	-- View Game Item added to System
	elseif (m_mode == "AddToGame") or (m_mode == "Overwrite") or (m_mode == "Place") then
		Events.sendEvent("FRAMEWORK_GOTO_ITEM", {UNID = tonumber(m_UNID)})

		--[[-- Open Systems menu by type
		if m_item.itemType == "character" then
			MenuOpen("Framework_Characters.xml")
		elseif m_item.itemType == "placeable" then
			MenuOpen("Framework_Placeables.xml")
		elseif m_item.itemType == "armor" or m_item.itemType == "generic" or
			   m_item.itemType == "weapon" or m_item.itemType == "consumable" or 
			   m_item.itemType == "ammo" then
			MenuOpen("Framework_WorldInventory.xml")
		elseif m_item.itemType == "harvestable" then
			MenuOpen("Framework_Harvestables.xml")
		end]]
		
		-- TODO: Select given item
	end

	MenuCloseThis()
end

-- Sets the menu display based on mode
setDisplay = function()
	-- Set GLID icon
	local eh = Image_GetDisplayElement(gHandles["imgNewItem"])
	KEP_LoadIconTextureByID(tonumber(m_item.GLID), eh, gDialogHandle)
	
	Control_SetVisible(gHandles["imgToolbarIcon"], true)
	Control_SetVisible(gHandles["btnClose"], true)
	Control_SetVisible(gHandles["imgNewItem"], true)
	
	-- Capitilize first letter of the system
	local itemSystem
	if m_item.displayType then
		itemSystem = m_item.displayType:gsub("^%l", string.upper)
	elseif m_item.itemType then
		itemSystem = m_item.itemType:gsub("^%l", string.upper)
	end
	-- Item is being copied up to the Inventory
	if m_mode == "Inventory" then
		Dialog_SetCaptionText(gDialogHandle, "     Copy to Inventory")
		-- Set text
		Static_SetText(gHandles["stcText"], "A copy of "..tostring(m_item.name).." has been created in the "..tostring(itemSystem).." tab of your Inventory.")
		Control_SetVisible(gHandles["stcText"], true)
		
		Control_SetLocationY(gHandles["boxDontShowAgain"], 72)
		Control_SetVisible(gHandles["boxDontShowAgain"], true)
		Control_SetLocationY(gHandles["stcDoNotShowAgain"], 72)
		Control_SetVisible(gHandles["stcDoNotShowAgain"], true)
		--Static_SetText(gHandles["stcDoNotShowAgain"], "Do not show the Copy to Inventory window again")
		Control_SetLocation(gHandles["btnViewInSystem"], 61, 108)
		Static_SetText(gHandles["btnViewInSystem"], "View in Inventory")
		Control_SetSize(gHandles["btnViewInSystem"], 122, Control_GetHeight(gHandles["btnViewInSystem"]))
		Control_SetVisible(gHandles["btnViewInSystem"], true)
		Control_SetLocationY(gHandles["imgLine2"], 98)
		Control_SetVisible(gHandles["imgLine2"], true)
		Control_SetLocation(gHandles["btnOK"], 186, 108)
		Control_SetVisible(gHandles["btnOK"], true)
		
		MenuSetSizeThis(nil, 167)
	
	-- New Game Item created
	elseif m_mode == "AddToGame" then
		Dialog_SetCaptionText(gDialogHandle, "     Copy to Game")
		
		-- Set text
		Static_SetText(gHandles["stcText"], "A copy of "..tostring(m_item.name).." has been created in your "..tostring(itemSystem).." System.")
		Control_SetVisible(gHandles["stcText"], true)
		
		Control_SetLocationY(gHandles["boxDontShowAgain"], 72)
		Control_SetVisible(gHandles["boxDontShowAgain"], true)
		Control_SetLocationY(gHandles["stcDoNotShowAgain"], 72)
		Control_SetVisible(gHandles["stcDoNotShowAgain"], true)
		--Static_SetText(gHandles["stcDoNotShowAgain"], "Do not show the Copy to Game window again")
		Control_SetLocationY(gHandles["btnViewInSystem"], 108)
		Control_SetVisible(gHandles["btnViewInSystem"], true)
		Control_SetLocationY(gHandles["imgLine2"], 98)
		Control_SetVisible(gHandles["imgLine2"], true)
		Control_SetLocationY(gHandles["btnOK"], 108)
		Control_SetVisible(gHandles["btnOK"], true)
		
		MenuSetSizeThis(nil, 167)
		
	-- Spawner placed
	elseif m_mode == "CreateSpawner" then
		Dialog_SetCaptionText(gDialogHandle, "     Create Spawn")
		
		-- Set text
		Static_SetText(gHandles["stcText"], "A new "..tostring(itemSystem).." Spawner, "..tostring(m_spawner.name)..", was created in "..tostring(m_worldName).." and was assigned "..tostring(m_item.name).." as its spawn item.")
		Control_SetVisible(gHandles["stcText"], true)
		Control_SetLocationY(gHandles["stcSpawnerProperties"], 64)
		Control_SetVisible(gHandles["stcSpawnerProperties"], true)
		Control_SetLocationY(gHandles["btnSpawnerProperties"], 64)
		Control_SetVisible(gHandles["btnSpawnerProperties"], true)
		Control_SetEnabled(gHandles["btnSpawnerProperties"], true)
		
		-- If a new item was created, display the expanded menu with links to the new item
		if m_itemCreated then
			Control_SetLocationY(gHandles["imgLine1"], 93)
			Control_SetVisible(gHandles["imgLine1"], true)
			
			Control_SetLocationY(gHandles["stcCopiedItems"], 110)
			Control_SetVisible(gHandles["stcCopiedItems"], true)
			
			Static_SetText(gHandles["stcAddedGameItem1"], tostring(m_item.name))
			Control_SetLocationY(gHandles["stcAddedGameItem1"], 126)
			Control_SetVisible(gHandles["stcAddedGameItem1"], true)
			
			Control_SetLocationY(gHandles["btnAddedGameItem1"], 126)
			Control_SetVisible(gHandles["btnAddedGameItem1"], true)
			Control_SetEnabled(gHandles["btnAddedGameItem1"], true)
			
			Control_SetLocationY(gHandles["stcCopiedItems"], 110)
			Control_SetVisible(gHandles["stcCopiedItems"], true)
			
			Control_SetLocationY(gHandles["boxDontShowAgain"], 161)
			Control_SetLocationY(gHandles["stcDoNotShowAgain"], 161)
			
			Control_SetLocationY(gHandles["imgLine2"], 187)
			
			Control_SetLocationY(gHandles["btnOK"], 198)
			
			MenuSetSizeThis(nil, 255)
		else
			Control_SetLocationY(gHandles["boxDontShowAgain"], 101)
			Control_SetLocationY(gHandles["stcDoNotShowAgain"], 101)
			
			Control_SetLocationY(gHandles["imgLine2"], 127)
			
			Control_SetLocationY(gHandles["btnOK"], 138)
			
			MenuSetSizeThis(nil, 198)
		end
		
		Control_SetVisible(gHandles["imgLine2"], true)
		Control_SetVisible(gHandles["boxDontShowAgain"], true)
		--Static_SetText(gHandles["stcDoNotShowAgain"], "Do not show the Create Spawn window again")
		Control_SetVisible(gHandles["stcDoNotShowAgain"], true)
		Control_SetLocationX(gHandles["btnOK"], (Dialog_GetWidth(gDialogHandle)/2)-(Control_GetWidth(gHandles["btnOK"])/2))
		Control_SetVisible(gHandles["btnOK"], true)
		
	-- Overwrite
	elseif m_mode == "Overwrite" then
		Dialog_SetCaptionText(gDialogHandle, "     Overwrite Item")
		
		-- Set text
		Static_SetText(gHandles["stcText"], tostring(m_item.name).." in your "..tostring(itemSystem).." System has been overwritten.")
		Control_SetVisible(gHandles["stcText"], true)
		Control_SetLocationY(gHandles["imgLine2"], 73)
		Control_SetVisible(gHandles["imgLine2"], true)
		Control_SetLocationY(gHandles["btnViewInSystem"], 87)
		Control_SetVisible(gHandles["btnViewInSystem"], true)
		Control_SetLocationY(gHandles["btnOK"], 87)
		Control_SetVisible(gHandles["btnOK"], true)
		
		MenuSetSizeThis(nil, 144)
	
	-- Place
	elseif m_mode == "Place" then
		Dialog_SetCaptionText(gDialogHandle, "     Place Object")
		
		-- Set text
		Static_SetText(gHandles["stcText"], tostring(m_item.name).." has been added to your Placeables System.")
		Control_SetVisible(gHandles["stcText"], true)
		Control_SetVisible(gHandles["boxDontShowAgain"], true)
		--Static_SetText(gHandles["stcDoNotShowAgain"], "Do not show the Place Object window again")
		Control_SetVisible(gHandles["stcDoNotShowAgain"], true)
		Control_SetLocationY(gHandles["boxDontShowAgain"], 72)
		Control_SetLocationY(gHandles["stcDoNotShowAgain"], 72)
		Control_SetLocationY(gHandles["imgLine2"], 98)
		Control_SetVisible(gHandles["imgLine2"], true)
		Control_SetLocationY(gHandles["btnViewInSystem"], 108)
		Control_SetVisible(gHandles["btnViewInSystem"], true)
		Control_SetLocationY(gHandles["btnOK"], 108)
		Control_SetVisible(gHandles["btnOK"], true)
		
		MenuSetSizeThis(nil, 167)
	end
	
	MenuCenterThis()
end

-- Check if this menu should even be open from the player's config
checkDisplayConfig = function()
	local config = KEP_ConfigOpenWOK()
	-- Load config to check if we should notify the user of the save
	local config = KEP_ConfigOpenWOK()
	local configFound, displaySaveVerify
	if m_mode == "Inventory" then
		configFound, displaySaveVerify = KEP_ConfigGetNumber(config, "DoNotShowCopyToInventoryConfirmation", 0)
	elseif m_mode == "AddToGame" then
		configFound, displaySaveVerify = KEP_ConfigGetNumber(config, "DoNotShowCopyToGameConfirmation", 0)
	elseif m_mode == "CreateSpawner" then
		configFound, displaySaveVerify = KEP_ConfigGetNumber(config, "DoNotShowCreateSpawnerConfirmation", 0)
	elseif m_mode == "Place" then
		configFound, displaySaveVerify = KEP_ConfigGetNumber(config, "DoNotShowPlaceObjectConfirmation", 0)
	end
	
	-- If the user has prompted NOT to see these confirmations, ABORT
	if displaySaveVerify == 1 then
		return true
	else
		return false
	end
end