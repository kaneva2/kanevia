--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("MenuHelper.lua")



local WOK_FAME_KILLED_1ST_MONSTER		= 592
local WOK_FAME_CRAFT_1ST_ITEM			= 596
local WOK_FAME_PLACE_1ST_ITEM			= 600
local WOK_FAME_HARVEST_1ST_ITEM			= 612

function onCreate()
	KEP_EventRegister( "FRAMEWORK_TRACK_CRAFTING_COUNT", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler("trackCraftingCountHandler", "FRAMEWORK_TRACK_CRAFTING_COUNT", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("placeItemHandler", "FRAMEWORK_BUILD_PLACE_ITEM", KEP.HIGH_PRIO)

	Events.registerHandler("FRAMEWORK_TRACK_MONSTER_KILL", trackMonsterKillHandler)
	Events.registerHandler("FRAMEWORK_TRACK_HARVEST_COUNT", trackHarvestCountHandler)
	
	-- Disable mouse input to prevent mouse over visual gitches
	MenuSetPassthroughThis(true)
end


function trackMonsterKillHandler(event)
end

function trackCraftingCountHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
end

function placeItemHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
end

function trackHarvestCountHandler(event)
end