--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\DevModeFunctions.lua")
dofile("..\\MenuScripts\\PopupMenuHelper.lua")
dofile("..\\MenuScripts\\ScrollPane.lua")

XpEO = {
    MENU_TEXTURE_ATLAS = 4,					-- Use texture atlas for 2D menus
    FRAME_TIME_PROFILER = 6,                -- Frame time profiler
}

--Helper
function OnOff(input)
	if input == nil then
		return "(nil)"
	elseif input == true or input == "1" or input == "Y" or input == "T" or type(input)=="number" and input~=0 then 
		return "ON"
	elseif input == false or input == "0" or input == "N" or input == "F" or input == 0 then
		return "OFF"
	else
		return "???"
	end
end

function ToggleMsgGzipLogEnabled (toggleHandle)
	local oldMode = KEP_IsMsgGzipLogEnabled()
	if toggleHandle == nil then
		return oldMode
	end
	local newMode = Slider_GetValue(Control_GetSlider(toggleHandle))
	return KEP_SetMsgGzipLogEnabled(ToBool(newMode))
end

function ToggleMsgMoveEnabled (toggleHandle)
	local oldMode = KEP_IsMsgMoveEnabled()
	if toggleHandle == nil then
		return oldMode
	end
	local newMode = Slider_GetValue(Control_GetSlider(toggleHandle))
	return KEP_SetMsgMoveEnabled(ToBool(newMode))
end

function ToggleLimitOnRender (toggleHandle)
	local oldMode = KEP_GetOnRenderTimeMs()
	if toggleHandle == nil then
		return oldMode
	end
	local newMode = Slider_GetValue(Control_GetSlider(toggleHandle))
	return KEP_SetOnRenderTimeMs(newMode * 1000.0)
end

function ToggleLimitFPS (toggleHandle)
	local oldMode = KEP_GetLimitFPS()
	if toggleHandle == nil then
		return oldMode
	end
	local newMode = Slider_GetValue(Control_GetSlider(toggleHandle))
	return KEP_SetLimitFPS(ToBool(newMode))
end

function ToggleAnimations (toggleHandle)
	local oldMode = KEP_GetAnimationsEnabled()
	if toggleHandle == nil then
		return oldMode
	end
	local newMode = Slider_GetValue(Control_GetSlider(toggleHandle))
	return KEP_SetAnimationsEnabled(ToBool(newMode))
end

function ToggleParticles (toggleHandle)
	local oldMode = KEP_GetParticlesEnabled()
	if toggleHandle == nil then
		return oldMode
	end
	local newMode = Slider_GetValue(Control_GetSlider(toggleHandle))
	return KEP_SetParticlesEnabled(ToBool(newMode))
end

function ToggleMediaScrape (toggleHandle)
	local oldMode = KEP_GetMediaScrapeEnabled()
	if toggleHandle == nil then
		return oldMode
	end
	local newMode = Slider_GetValue(Control_GetSlider(toggleHandle))
	return KEP_SetMediaScrapeEnabled(ToBool(newMode))
end

function TogglePhysicsWindow (toggleHandle)
	local oldShow = KEP_ShowPhysicsWindow( -1 )
	if toggleHandle == nil then
		return oldShow
	end
	local newShow = Slider_GetValue(Control_GetSlider(toggleHandle))
	return DevMode_ShowPhysicsWindow(newShow)
end

function ToggleHandlerGeneric(toggleHandle, toggleDebugName, queryFunc, setFunc)
	local oldVal = queryFunc()
	if not toggleHandle then
		-- query only
		return oldVal
	end

	local newVal = Slider_GetValue(Control_GetSlider(toggleHandle))
	if oldVal ~= newVal then
		setFunc(newVal)
		local message = tostring(toggleDebugName) .. ": " .. OnOff(newVal)
		Log(message)
		local e = KEP_EventCreate("RenderTextEvent")
		if e ~= nil then 
			KEP_EventEncodeString(e, message)
			KEP_EventEncodeNumber(e, ChatType.System)
			KEP_EventQueue(e)
		end
	end
	return newVal
end

function ToggleOfflineMode(toggleHandle)
	return ToggleHandlerGeneric(toggleHandle, "Off-line Mode", 
		function() if KEP_IsClientEnabled() then return 0 else return 1 end end, 
		function(offline) if offline == 1 then KEP_DisableClient() else KEP_EnableClient() end end)
end

function ToggleExperimentalEngineOption(toggleHandle, toggleDebugName, optionKey)
	return ToggleHandlerGeneric(toggleHandle, toggleDebugName, 
		function() return KEP_GetExperimentalEngineOption(optionKey) end, 
		function(enable) KEP_SetExperimentalEngineOption(optionKey, enable) end)
end

function ToggleFrameTimeProfiler(toggleHandle)
	return ToggleExperimentalEngineOption(toggleHandle, "Frame Time Profiler", XpEO.FRAME_TIME_PROFILER)
end

function ToggleTextureAtlasForMenus(toggleHandle)
	return ToggleExperimentalEngineOption(toggleHandle, "Use Texture Atlas for Menus", XpEO.MENU_TEXTURE_ATLAS)
end

function ToggleDownloadVisualization(toggleHandle)
	return ToggleHandlerGeneric(toggleHandle, "Download Visualization", KEP_IsDownloadPriorityVisualizationEnabled, KEP_EnableDownloadPriorityVisualization)
end

local triggerRenderEnabled = 0
function ToggleTriggerVisualization(toggleHandle)
	return ToggleHandlerGeneric(toggleHandle, "Trigger Visualization", function() return triggerRenderEnabled end, function(enable) triggerRenderEnabled = enable KEP_EnableTriggerRender(enable, 0) end)
end

local bboxRenderEnabled = 0
function ToggleBoundingBoxVisualization(toggleHandle)
	return ToggleHandlerGeneric(toggleHandle, "Bounding Box Visualization", function() return bboxRenderEnabled end, function(enable) bboxRenderEnabled = enable KEP_EnableDynamicObjectBoundingBoxRender(0, enable) end)
end

--Action type
Action = {
	NONE = 0, EXECUTE = 1, TOGGLE = 2,
}

--Map matching the label name presented in the menu vs the actual option
DevModeFunctions = {
	{ "MsgGzip Log", ToggleMsgGzipLogEnabled,   Action.TOGGLE },
	{ "MsgMove Enabled", ToggleMsgMoveEnabled,   Action.TOGGLE },
	{ "Limit OnRender", ToggleLimitOnRender,   Action.TOGGLE },
	{ "Limit FPS", ToggleLimitFPS,   Action.TOGGLE },
	{ "Animations", ToggleAnimations,   Action.TOGGLE },
	{ "Particles", ToggleParticles,   Action.TOGGLE },
	{ "Media Scrape", ToggleMediaScrape,   Action.TOGGLE },
	{ "Physics Window", TogglePhysicsWindow, Action.TOGGLE },

	{ "Off-line Mode", ToggleOfflineMode, Action.TOGGLE },
   	{ "Frame Time Profiler", ToggleFrameTimeProfiler, Action.TOGGLE },

	{ "Use Texture Atlas for Menus",ToggleTextureAtlasForMenus,     Action.TOGGLE },

	{ "Download Visualization",     ToggleDownloadVisualization,    Action.TOGGLE },
	{ "Trigger Visualization",      ToggleTriggerVisualization,     Action.TOGGLE },
	{ "Bounding Box Visualization", ToggleBoundingBoxVisualization, Action.TOGGLE },
	
	{ "Member Alts", DevMode_MemberAlts, Action.EXECUTE },
	{ "Log Dialogs", DevMode_LogDialogs, Action.EXECUTE },
	{ "Log Metrics", DevMode_LogMetrics, Action.EXECUTE },
	{ "Log Script Refs", DevMode_LogScriptRefs, Action.EXECUTE },
	{ "Log Msg Proc", DevMode_LogMsgProc, Action.EXECUTE },
	{ "Open QA Menu", DevMode_OpenQAMenu, Action.EXECUTE },
	{ "Log Resources", DevMode_LogResources, Action.EXECUTE },
	{ "Log Webcalls", DevMode_LogWebcalls, Action.EXECUTE },
	{ "Crash Client", DevMode_CrashClient, Action.EXECUTE },
}

g_devOptionScrollPane = nil
g_sampleLabelHandle  = nil
g_sampleButtonHandle = nil
g_sampleToggleHandle = nil

-- Duplicate an existing control and assign the new copy with a new name (thus the dynamically constructed control can inherit the style defined in menu XML)
function copyControl(dialogHandle, controlHandle, newName)
	Dialog_CopyControl(dialogHandle, controlHandle)
	Dialog_PasteControl(dialogHandle)
	local handle = Dialog_GetControlByIndex(dialogHandle, Dialog_GetNumControls(dialogHandle) - 1)
	Control_SetName(handle, newName)
	return handle
end

function addFunctionToList(dialogHandle, index, devModeFunc, yOffset)
	-- See `DevModeFunctions' above
	local funcName = devModeFunc[1]
	local func = devModeFunc[2]
	local action = devModeFunc[3]

	-- Create a label by copying the template DXUT control
	local labelHandle = copyControl(dialogHandle, g_sampleLabelHandle, "stcItem_" .. tostring(index))
	Static_SetText(Control_GetStatic(labelHandle), funcName)
	Control_SetLocationY(labelHandle, yOffset)
	Control_SetVisible(labelHandle, 1)
	g_devOptionScrollPane:addChild(labelHandle, false)
	devModeFunc[4] = labelHandle

	-- Create appropriate button by copying one of the template DXUT controls
	local buttonName = "btnItem_" .. tostring(index)
	local buttonHandle = nil
	if action == Action.EXECUTE then
		buttonHandle = copyControl(dialogHandle, g_sampleButtonHandle, buttonName)		-- a push <button>
	elseif action == Action.TOGGLE then
		buttonHandle = copyControl(dialogHandle, g_sampleToggleHandle, buttonName)		-- a <slider> to simulate toggle button
	end
	devModeFunc[5] = buttonHandle

	if buttonHandle~=nil then
		Control_SetLocationY(buttonHandle, yOffset)
		Control_SetVisible(buttonHandle, 1)
		g_devOptionScrollPane:addChild(buttonHandle, false)

		-- Hook up event handler
		if action == Action.EXECUTE then
			_G[buttonName .. "_OnButtonClicked"] = function()
				func(buttonHandle)
			end
		elseif action == Action.TOGGLE then
			-- Set initial state for toggle buttons
			local curVal = func()
			Slider_SetValue(buttonHandle, curVal)
			_G[buttonName .. "_OnSliderValueChanged"] = function()
				func(buttonHandle)
			end
		end
	end

	g_devOptionScrollPane:recalcLayout()
end

function getDevModeFuncByName(name)
	for _, devModeFunc in ipairs(DevModeFunctions) do
		if devModeFunc[1] == name then
			return devModeFunc
		end
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- Invisible menu controls used as styling template
	g_sampleLabelHandle  = Dialog_GetControl(dialogHandle, "stcLabel")
	g_sampleButtonHandle = Dialog_GetControl(dialogHandle, "btnButton")
	g_sampleToggleHandle = Dialog_GetControl(dialogHandle, "sldrToggle")

	-- Construct a new scroll pane controller
	g_devOptionScrollPane = ScrollPane.new("DevOptionsList", dialogHandle, "imgScrollPane", "lbScrollbar", {})

	-- Generate and insert DXUT controls into scroll pane controller
	local yOffset = Control_GetLocationY(g_sampleLabelHandle)
	for index, devModeFunc in ipairs( DevModeFunctions ) do
		addFunctionToList(dialogHandle, index, devModeFunc, yOffset)
		yOffset = yOffset + Control_GetHeight(g_sampleLabelHandle) + 10
	end

	---------------------------------------------------
	-- NOTE: following code is not tested or verified
	---------------------------------------------------
	local stringOfDevOptionsInEngine = KEP_GetDevOptionsDefinedInEngine()
	Log("GetDevOptionsDefinedInEngine: " .. tostring(stringOfDevOptionsInEngine))
	s = stringOfDevOptionsInEngine

	local index = #DevModeFunctions + 1
	while (s ~= "") do
		local i, j = string.find(s, "&&")
		local devOption = string.sub(s, 1, i - 1)
		local devModeFunc = { devOption, function() KEP_ExecuteFunction(devOption) end, Action.EXECUTE }
		table.insert(DevModeFunctions, devModeFunc)
		addFunctionToList(dialogHandle, index, devModeFunc, yOffset)

		yOffset = yOffset + Control_GetHeight(g_sampleLabelHandle) + 10
		index = index + 1
		s = string.sub(s, j + 1, -1)
	end
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )

    -- Call cleanup functions
    for _, func in ipairs(DevModeFunctions) do
        if type(func.cleanup)=="function" then
            func.cleanup()
        end
    end
end

function btnClose_OnButtonClicked( buttonHandle )
	MenuCloseThis()
end
