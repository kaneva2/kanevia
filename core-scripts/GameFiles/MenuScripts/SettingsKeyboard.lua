--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

-- global handle to controls listbox
g_lbh_controls = nil

-- global boolean to denote when setting a key
g_b_defaults = false
g_b_ok = false
g_sel_index = 0

g_t_btnHandles = {}

local MODAL_TIMER = .25
local m_timerOn = false
local m_elapsedTime = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "controlsUpdatedHandler", "ControlsUpdatedEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- setup look of listbox scrollbars
	SetupListBoxScrollBars("lstControls")

	g_lbh_controls = Dialog_GetListBox(gDialogHandle, "lstControls")

	UpdateControlConfig(0)

	getButtonHandles()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_timerOn == true then
		m_elapsedTime = m_elapsedTime + fElapsedTime
		if m_elapsedTime > MODAL_TIMER then
			Dialog_SetModal(gDialogHandle, false)
			EnableButtonsDuringSetKey(true)
			m_timerOn = false
		end
	end
end

function SetupListBoxScrollBars(listbox_name)
	local lbh = Dialog_GetListBox(gDialogHandle, listbox_name)
	if lbh ~= 0 then

		local sbh = ListBox_GetScrollBar(lbh)

		if sbh ~= 0 then

			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				--Blue body
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				--Grey lined
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Up arrow button
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Down Arrow Button
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end
end

function UpdateControlConfig(sel_index)
	ListBox_RemoveAllItems(g_lbh_controls)
	local config_string = KEP_GetControlConfiguration()
	local t = split(config_string, ",")
	if t ~= 0  then
		local num_args = #t
		if math.fmod(num_args,2) ~= 0 then
			return
		end
		for i = 1, num_args, 2 do
			--log("\nt[i] = "..tostring(t[i]))
			ListBox_AddItem(g_lbh_controls, t[i], tonumber(t[i+1]))
		end
	end
	ListBox_SelectItem(g_lbh_controls, sel_index)
end

function controlsUpdatedHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local sel_index = KEP_EventDecodeNumber( event )
	UpdateControlConfig(g_sel_index)
	KEP_SaveControlConfiguration()
	m_elapsedTime = 0
	m_timerOn = true
	
end

function btnSetKey_OnButtonClicked(buttonHandle)
	Dialog_SetModal(gDialogHandle, true)
	EnableButtonsDuringSetKey(false)
	g_sel_index = ListBox_GetSelectedItemIndex(g_lbh_controls)
	KEP_PendingControlAssignment(ListBox_GetSelectedItemData(g_lbh_controls))
end

function btnDefaults_OnButtonClicked(buttonHandle)
	g_b_defaults = true
	KEP_RestoreDefaultControlConfiguration()
	UpdateControlConfig(0)
	KEP_SaveControlConfiguration()
end

function EnableButtonsDuringSetKey(bEnable)
	--local bh = Dialog_GetButton(gDialogHandle, "btnDefaults")
	--if bh ~= 0 then
	setControlEnabledVisible("btnDefaults", bEnable)
	setControlEnabledVisible("btnSetKey", bEnable)
	setControlEnabledVisible("stcMessage", not bEnable)
end

function getButtonHandles()
	local ch = Dialog_GetControl(gDialogHandle, "btnSetKey")
	if ch ~= nil then
		g_t_btnHandles["btnSetKey"] = ch
	end
	ch = Dialog_GetControl(gDialogHandle, "btnDefaults")
	if ch ~= nil then
		g_t_btnHandles["btnDefaults"] = ch
	end
end

function setControlEnabledVisible(controlName, bool)
	Control_SetVisible(gHandles[controlName], bool)
	Control_SetEnabled(gHandles[controlName], bool)
end