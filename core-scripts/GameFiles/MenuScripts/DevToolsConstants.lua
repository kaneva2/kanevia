--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

DevToolsUpload					= {}
DevToolsUpload.UNKNOWN			= 0
DevToolsUpload.SERVER_SCRIPT	= 1
DevToolsUpload.MENU_XML			= 2
DevToolsUpload.MENU_DDS			= 3
DevToolsUpload.MENU_TGA			= 4
DevToolsUpload.MENU_SCRIPT		= 5
DevToolsUpload.SAVED_SCRIPT		= 6
DevToolsUpload.MENU_BMP			= 7
DevToolsUpload.MENU_PNG			= 8
DevToolsUpload.MENU_JPG			= 9
DevToolsUpload.MENU_JPEG		= 10
DevToolsUpload.ACTION_ITEM		= 11
DevToolsUpload.ACTION_INSTANCE	= 12
DevToolsUpload.ACTION_ITEM_PARAMS		= 13
DevToolsUpload.ACTION_INSTANCE_PARAMS	= 14

function prettyUploadName(type)
	if (type == DevToolsUpload.SERVER_SCRIPT) then
		return "Server LUA Script"
	elseif (type == DevToolsUpload.MENU_XML) then
		return "Menu XML Data"
	elseif (type == DevToolsUpload.MENU_DDS) then
		return "Menu DDS Texture"
	elseif (type == DevToolsUpload.MENU_TGA) then
		return "Menu TGA Texture"
	elseif (type == DevToolsUpload.MENU_BMP) then
		return "Menu BMP Texture"
	elseif (type == DevToolsUpload.MENU_PNG) then
		return "Menu PNG Texture"
	elseif (type == DevToolsUpload.MENU_JPG) then
		return "Menu JPG Texture"
	elseif (type == DevToolsUpload.MENU_JPEG) then
		return "Menu JPEG Texture"
	elseif (type == DevToolsUpload.MENU_SCRIPT) then
		return "Menu LUA Script"
	elseif (type == DevToolsUpload.SAVED_SCRIPT) then
		return "Server Saved Script"
	elseif (type == DevToolsUpload.ACTION_ITEM) then
		return "Action Item Script"
	elseif (type == DevToolsUpload.ACTION_INSTANCE) then
		return "Action Item Instance"
	elseif (type == DevToolsUpload.ACTION_ITEM_PARAMS) then
		return "Action Item Parameters"
	elseif (type == DevToolsUpload.ACTION_INSTANCE_PARAMS) then
		return "Action Item Instance"
	else
		return "Unknown"
	end
end
