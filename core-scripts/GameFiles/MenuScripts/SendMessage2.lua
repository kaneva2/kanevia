--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

-- Catch the event from the friends and people menu telling us who the gift is for
PLAYER_USERID_EVENT = "PlayerUserIdEvent"
INFO_BOX_EVENT = "InfoBoxEvent"

-- website addresses for queries
MESSAGE_CENTER_SUFFIX = "kgp/messagecenter.aspx?action="
SEND_MESSAGE_SUFFIX = "SendMessage&type=Member&username="
SUBJECT_SUFFIX = "&subject="
MESSAGE_SUFFIX = "&message="

GET_BALANCE_SUFFIX = "kgp/userProfile.aspx?action=getUserBalance"
GET_USERID_SUFFIX = "kgp/userProfile.aspx?action=getUserIdFromAvatarName&avatar="

-- Constants for gifts
CASH = 8
DEFAULT_SUBJECT = ""
DEFAULT_MESSAGE = ""

CREDITS_PARSE = 0
RESULT_PARSE = 1
GIFT_PARSE = 2
USERID_PARSE = 3

-- Global max/min for speed boost
g_minL = 0
g_maxR = 0
g_minT = 0
g_maxB = 0

g_mouseovertable = {}
g_mouseovertablesize = 0

-- User name and id to recieve gift
g_userID = -1
g_userName = nil
g_subject = ""
g_msgData = ""
g_body = nil

-- Number of credits player has
g_credits = -1

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "PlayerUserIdEventHandler", PLAYER_USERID_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( PLAYER_USERID_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function PlayerUserIdEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_userID = KEP_EventDecodeNumber( event )
	g_userName = KEP_EventDecodeString( event )
	g_subject = KEP_EventDecodeString( event )
	g_body = KEP_EventDecodeString( event )

	EditBox_SetText( gHandles["editSendTo"], g_userName, false )
	EditBox_SetText( gHandles["editSubject"], g_subject, false )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_msgData = KEP_EventDecodeString( event )
	if g_useParse == RESULT_PARSE then
		ParseResults(g_msgData)
	end
	return KEP.EPR_OK
end

function btnSend_OnButtonClicked( buttonHandle )
	sendMessage()
end

function sendMessage()
	local text = nil
	local uh = Dialog_GetEditBox(gDialogHandle, "editSendTo")
	local sh = Dialog_GetEditBox(gDialogHandle, "editSubject")
	local bh = Dialog_GetEditBox(gDialogHandle, "editMsgBody")
	local username = EditBox_GetText(uh)
	local subject = EditBox_GetText(sh)
	local body = EditBox_GetText(bh)

	if subject == "" then
		subject = "No Subject"
	end

	if username ~= nil then
		-- create really long url
		body = URLSafe(body)
		makeWebCall(GameGlobals.WEB_SITE_PREFIX..MESSAGE_CENTER_SUFFIX..SEND_MESSAGE_SUFFIX..username..SUBJECT_SUFFIX..subject..MESSAGE_SUFFIX..body, 0)
		g_useParse = RESULT_PARSE
	end
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function URLSafe(s)
	-- first replace the % for obvious reasons
	s = string.gsub(s, "%%", "%%25")
	-- Reserved characters for URLS
	s = string.gsub(s, "\n", "%%3Cbr%%3E")
	s = string.gsub(s, "+", "%%2B")
	s = string.gsub(s, ",", "%%2C")
	s = string.gsub(s, "/", "%%2F")
	s = string.gsub(s, ":", "%%3A")
	s = string.gsub(s, ";", "%%3B")
	s = string.gsub(s, "=", "%%3D")
	s = string.gsub(s, "?", "%%3F")
	s = string.gsub(s, "@", "%%40")
	-- Unsafe characters
	s = string.gsub(s, "<", "%%3C")
	s = string.gsub(s, ">", "%%3E")
	s = string.gsub(s, '"', "%%22")
	s = string.gsub(s, "#", "%%23")
	return s
end

function ParseResults(xmlData)
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(xmlData, "<ReturnCode>(.-)</ReturnCode>")
	if result ~= nil then
		if result == "0" then
			KEP_MessageBox("Your message was successfully sent.")
			MenuCloseThis()
		else
			KEP_MessageBox("There was a problem sending your message. Please try again.")
		end
	end
end
