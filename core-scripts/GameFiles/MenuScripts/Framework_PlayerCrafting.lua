--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Crafting.lua
--
-- Displays available recipes for crafting
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
--dofile("..\\MenuScripts\\FrameworkTooltipHelper.lua")
dofile("Framework_InventoryHelper.lua")

----------------------
-- Statics
----------------------
local KEY_C = 67
local ITEMS_PER_PAGE = 20
local TYPE_FILTER = "playerCrafting"

local CRAFT_TIME = 5

local DEFAULT_MAX_DURABILITY = 1000 -- 1,000 hits or attacks per weapon/armor
local DURABILITY_BY_LEVEL = { [1]=10, [2]=100, [3]=250, [4]=500, [5]=1000 }

local COLORS_VALUES =	{	["Common"] = {a = 255, r = 255, g = 255, b = 255},
							["Rare"] = {a = 255, r = 0, g = 150, b = 255},
							["Very Rare"] = {a = 255, r = 174, g = 0, b = 255},
							["Extremely Rare"] = {a = 255, r = 255, g = 144, b = 0}
						}

local DROPDOWN_FILTERS = {"all", "recipe", "blueprint"}

local CLICKED_CONTROLS = {"imgSelectedBG"}
						
local OCCUPIED_ANIMATION_GLID = 4546763

----------------------
-- Local functions
----------------------
local customSort
local formatDisplayTable
local formatTooltips
local updateInventoryMenu
local selectRecipe
local clearSelectedRecipe
local getRecipeFromIndex
local moveRecipeSelectedBG
local updateSelectedRecipe
local maximumCraftCount
local startCrafting
local stopCrafting
local completeCraftingItem
local updateRecipeCounts
local getLevelMultiplier
local nearProximityReq

----------------------
-- Local vars
----------------------

-- Menu Display
g_displayTable = {}

-- All available recipes
local m_recipes = {} -- [{inputs:{UNID,count},output:{GLID,UNID,name}]
local m_blueprints = {}

-- Player's inventory
local m_inventory = {} --[{UNID, count, name, itemType}]
local m_playerBlueprints = {}

-- Current Recipe Info
local m_selectedRecipe = 1
local m_totalCraftCount = 1
local m_currentCraftCount = 1
local m_maxCraftCount = 1
local m_craftingHelperTooltip = {}

-- Crafting Order
local m_crafting = false
local m_craftTime = 0
local m_craftedCount = 0

-- Table of unique UNIDs and their total counts
local m_inventoryCounts = {} -- {UNID:count}

-- Crafting Helpers Currently Useable
local m_craftingHelpers = {}

-- Drop-down Filter
local m_dropDownFilter = DROPDOWN_FILTERS[1]
local m_searchText = ""

-- Current page
local m_page = 1

-- Crafting helpers to use as filters
local m_sortUNIDS = {}
local m_lastSortUNID = -1
local m_sortPendingUNID = -1
local m_gameItems = {}

-- Called when the menu is created
function onCreate()
	KEP_EventRegister("FRAMEWORK_PLAYER_CRAFTING_FILTER", HIGH_PRIO)
	
	-- Register event handlers
	-- Register handlers for client-to-client events
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateBlueprintsClientFull", "UPDATE_BLUEPRINTS_CLIENT_FULL", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("playerCraftingFilterHandler", "FRAMEWORK_PLAYER_CRAFTING_FILTER", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onPreprocessTransactionResponse", "INVENTORY_HANDLER_PREPROCESS_TRANSACTION_RESPONSE", KEP.HIGH_PRIO)
	-- Register handler for server-to-client events
	Events.registerHandler("FRAMEWORK_RETURN_RECIPES", recipeReturnHandler)
	Events.registerHandler("FRAMEWORK_RECIPE_UPDATED", onRecipeUpdated)
	--Events.registerHandler("UPDATE_GAME_ITEMS_BY_TYPE", returnCraftingHelpers)
	Events.registerHandler("PLAYER_IN_CRAFTING_PROXIMITY", playerInCraftingProximity)
	Events.registerHandler("PLAYER_OUT_CRAFTING_PROXIMITY", playerOutCraftingProximity)
	Events.registerHandler("DROP_PLAYER_INVENTORY", onPlayerKilled)
	Events.registerHandler("FRAMEWORK_PLAYER_CRAFTING_HELPER_CLICK", playerCraftingHelperClickHandler)
	
	--FWTooltipHelper.registerInventoryEvents()
	--FWTooltipHelper.registerInventoryHandlers()	

	m_craftingHelperTooltip["header"] = {label = "Crafting Helpers:"}

	--FWTooltipHelper.addFrameworkTooltip("stcReqProximity", m_craftingHelperTooltip)

	InventoryHelper.registerInventoryEvents()
	InventoryHelper.registerInventoryHandlers()	

	InventoryHelper.initializeInventory("g_displayTable", true, ITEMS_PER_PAGE)
	-- InventoryHelper.initializeHover() -- disable this - inconsistent with backpack UI and looks bad with red bar and text resizing
	InventoryHelper.initializePageNumbers()
	InventoryHelper.initializeTooltips(TOOLTIP_TABLE, "hide")
	
	-- Request the player's inventory
	requestInventory(GAME_PID)
	requestInventory(INVENTORY_PID)
	requestInventory(BLUEPRINT_PID)
	
	-- Retrieve recipes from the server
	local playerName = KEP_GetLoginName()

	Events.sendEvent("FRAMEWORK_GET_RECIPES")	

	-- Define item button click function handlers
	for i = 1, ITEMS_PER_PAGE do
		-- On recipe click
		_G["btnItem" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			local s, e, index = string.find(Control_GetName(btnHandle), "(%d+)")
			index = tonumber(index)
			local recipe = getRecipeFromIndex(index)
			selectRecipe(recipe)

			if recipe then
				local x = Control_GetLocationX(btnHandle)
				local y = Control_GetLocationY(btnHandle)
				moveRecipeSelectedBG(x-1, y)
			end
		end
	end

	Events.sendEvent("CRAFTING_MENU_READY", {})
end

function onDestroy()
	KEP_SetCurrentAnimationByGLID(0)
	InventoryHelper.destroy()
	--FWTooltipHelper.destroy()
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
	InventoryHelper.onButtonDown(x, y)
	updateInventoryMenu()
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

-- Quantity Up
function btnCountUp_OnButtonClicked(buttonHandle)
	m_totalCraftCount = m_totalCraftCount + 1
	updateSelectedRecipe()
	EditBox_SetText(gHandles["edCount"], tostring(m_totalCraftCount), 0)
end

-- Called when an edit box is changed
function edCount_OnEditBoxChange(editBoxHandle, editBoxText)
	local newValue = editBoxText
	local resetValue = false

	if tonumber(newValue) ~= nil then
		newValue = tonumber(newValue)

		local min = 1

		if min and newValue < min then -- Input value is less than the minimum
			newValue = min
			resetValue = true
		end

		local max = 999

		if max and newValue > max then -- Input value is greater than the maximum 
			newValue = max
			resetValue = true
		end						
	elseif newValue == "" then
		--An invalid non-number character has been inputted
		resetValue = true				
	end

	if resetValue then
		EditBox_SetText(gHandles["edCount"], newValue, false)
	end

	m_totalCraftCount = tonumber(newValue) or 0

	updateSelectedRecipe()
end

-- Quantity Down
function btnCountDown_OnButtonClicked(buttonHandle)
	m_totalCraftCount = m_totalCraftCount - 1
	updateSelectedRecipe()
	EditBox_SetText(gHandles["edCount"], tostring(m_totalCraftCount), 0)
end

-- On craft
function btnCraft_OnButtonClicked(buttonHandle)
	if m_crafting then
		stopCrafting()
	else
		local addTable = {}
		local recipe = m_recipes[m_selectedRecipe] or m_blueprints[m_selectedRecipe]
		local inputs = recipe.inputs
		local output = recipe.output
		output.count = 1

		table.insert(addTable, output)

		processTransaction(inputs, addTable, true)	
	end
end

-- On frame update
function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- If we're currently crafting something
	if m_crafting then
		-- Crafting complete?
		if m_craftTime >= CRAFT_TIME then
			completeCraftingItem()
			Control_SetSize(gHandles["imgCraftProgress"], 0, 84)

			local newItemEvent = KEP_EventCreate("FRAMEWORK_NEW_ITEM_MESSAGE")
			KEP_EventEncodeString(newItemEvent,"Item")
			KEP_EventQueue(newItemEvent)
		-- Keep crafting
		else
			KEP_SetCurrentAnimationByGLID(OCCUPIED_ANIMATION_GLID)
			m_craftTime = math.min(m_craftTime + fElapsedTime, CRAFT_TIME)
			
			local craftPercent = m_craftTime / CRAFT_TIME
			Control_SetSize(gHandles["imgCraftProgress"], 84*craftPercent, 84)
		end
	end
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Called when crafting to check if the player has space for the item
function playerCraftingFilterHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local filter = KEP_EventDecodeString(tEvent)

	InventoryHelper.gotoPage(1)
	m_dropDownFilter = filter
	clearSelectedRecipe()
	resetRecipeCounts()
	if m_crafting then
		stopCrafting()
	end
	formatDisplayTable()
end

function playerCraftingHelperClickHandler(event)

	-- queue this up, wait till received all item information
	if event.UNID then
		-- update the last helper clicked
		m_lastSortUNID = event.UNID
		m_sortPendingUNID = event.UNID
		table.insert(m_sortUNIDS, m_lastSortUNID)
		-- update the display
		playerCraftingSort("craftingHelper", event.UNID)
		-- formatDisplayTable() -- 1234
	end

end

-- Call to manually change the sorting of the crafting list
playerCraftingSort = function(sortType, sortUNID)
	if sortType == nil then return end
	if sortType == "craftingHelper" and sortUNID then
		table.insert(m_sortUNIDS, sortUNID)
		table.sort(g_displayTable, proxReqSort)
		updateInventoryMenu()
	else -- default sort
		table.sort(g_displayTable, customSort)
		updateInventoryMenu()
	end
	Events.sendEvent("CRAFTING_MENU_SORTED", {})
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if m_inventory[updateIndex] then
		m_inventory[updateIndex] = updateItem
		updateInventoryCounts()
		updateInventoryMenu()

		if m_selectedRecipe then
			updateSelectedRecipe()
		end
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_inventory = decompileInventory(tEvent)
	updateInventoryCounts()
	updateInventoryMenu()
	if m_selectedRecipe then
		updateSelectedRecipe()
	end
end

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem	

	if updateItem.behavior == "campfire" then
		m_craftingHelpers[updateIndex] = updateItem
	end
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems

	getCraftingHelpers()
end

function updateBlueprintsClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_playerBlueprints = decompileInventory(tEvent)
	if tablePopulated(g_displayTable) then
		formatDisplayTable()
	end
end

-- Returns all available recipes from the server
function recipeReturnHandler(event)
	if event.initial then
		m_recipes = {}
		m_blueprints = {}
	end
	if event.recipes and type(event.recipes) == "table" then
		for UNID, properties in pairs(event.recipes) do
			m_recipes[UNID] = properties
		end
	end
	
	if event.blueprints and type(event.blueprints) == "table" then
		for UNID, properties in pairs(event.blueprints) do
			m_blueprints[UNID] = properties
		end
	end

	formatDisplayTable()
end

function onRecipeUpdated(event)
	--Cancel crafting when ANY recipe is updated
	cancelCraft()
	
	if event and m_recipes ~= nil then
		local UNID = tostring(event.itemUNID)
		m_recipes[UNID] = event.itemInfo

		-- Populate g_displayTable with these recipes
		formatDisplayTable()
	end
end

function getCraftingHelpers()

	for UNID, properties in pairs (m_gameItems) do

		if properties.behavior == "campfire" and properties.itemType == "placeable" then
			m_craftingHelpers[UNID] = properties
		end
	end

	m_sortUNIDS = {}
	Events.sendEvent("CHECK_PLAYER_CRAFTING_PROXIMITY")
end

function playerInCraftingProximity(event)
	m_craftingHelpers[tostring(event.UNID)].inProximity = true

	-- playerCraftingSort("craftingHelper", event.UNID)
	table.insert(m_sortUNIDS, event.UNID)

	updateInventoryMenu()
	updateSelectedRecipe()
end

function playerOutCraftingProximity(event)
	m_craftingHelpers[tostring(event.UNID)].inProximity = false

	if event.UNID == m_lastSortUNID then
		m_lastSortUNID = -1
		m_sortUNIDS = {}
	end

	updateInventoryMenu()
	updateSelectedRecipe()

end

-- If this player's been killed
function onPlayerKilled(event)
	MenuCloseThis()
end

-- When the player changes pages of the recipe list or changes the search terms
function inventoryUpdated(page, search, category)
	if m_page ~= page or m_searchTexk ~= search then
		clearSelectedRecipe()
		resetRecipeCounts()
		if m_crafting then
			stopCrafting()
		end
	end

	m_page = page
	m_searchText = search
	formatDisplayTable()
end

function onPreprocessTransactionResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local success = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	local menuName = tostring(KEP_EventDecodeString(tEvent))
	
	if menuName ~= MenuNameThis() then return end
    if success  then
    	startCrafting()
    else
    	displayStatusMessage(INVENTORY_ERRORS.BACKPACK_FULL)
	end
end
-- -- -- -- -- -- --
-- Display functions
-- -- -- -- -- -- --

formatDisplayTable = function()
	g_displayTable = {}
	if m_dropDownFilter == DROPDOWN_FILTERS[1] or m_dropDownFilter == DROPDOWN_FILTERS[2] then
		for UNID,recipe in pairs(m_recipes) do
			if not recipe.invalid then
				local strStart, strEnd = string.find(string.lower(recipe.output.properties.name), string.lower(m_searchText))
				if strStart ~= nil then
					recipe.name = recipe.output.properties.name
					table.insert(g_displayTable, recipe)
				end				
			end
		end		
	end

	if m_dropDownFilter == DROPDOWN_FILTERS[1] or m_dropDownFilter == DROPDOWN_FILTERS[3] then	
		for _, playerBlueprint in pairs(m_playerBlueprints) do
			for UNID,blueprint in pairs(m_blueprints) do
				if tostring(playerBlueprint.UNID) == tostring(UNID) and not blueprint.invalid then
					local strStart, strEnd = string.find(string.lower(blueprint.output.properties.name), string.lower(m_searchText))
					if strStart ~= nil then
						blueprint.name = blueprint.output.properties.name
						table.insert(g_displayTable, blueprint)
						break
					end								
				end
			end
		end
	end

	InventoryHelper.setMaxPages(#g_displayTable)

	if m_sortPendingUNID >= 0 then
		local sortUNID = m_sortPendingUNID
		-- m_lastSortUNID = m_sortPendingUNID
		-- m_sortPendingUNID = -1
		playerCraftingSort("craftingHelper", sortUNID)
	elseif m_lastSortUNID >= 0 then
		local sortUNID = m_lastSortUNID
		playerCraftingSort("craftingHelper", sortUNID)
	else
		table.sort(g_displayTable, customSort)
	
	end

	updateInventoryMenu()
end

customSort = function(a, b)
	if a.name and b.name then -- sort by name (within crafting helper)
		return a.name < b.name
	else
		return tonumber(a.UNID) < tonumber(b.UNID) -- sort by UNID if no name
	end
end

proxReqSort = function(a, b)
	if checkForProxReqIndex(a) ~= checkForProxReqIndex(b) then 
		return checkForProxReqIndex(a) > checkForProxReqIndex(b) -- sort by crafting helper
	else
		if a.name and b.name then -- sort by name (within crafting helper)
			return a.name < b.name
		else
			return tonumber(a.UNID) < tonumber(b.UNID) -- sort by UNID if no name
		end
	end
end

-- Gets whether an item requires a crafting helper; returns index value to be used with m_sortUNIDS
-- NOTE: commented out ability to sort by MULTIPLE crafting helpers. UI was confusing and list did not always update in time to display properly.
checkForProxReqIndex = function(gameItem)
	if gameItem.proxReq == nil then return 0 end
	for requiredIndex,requiredValue in pairs(gameItem.proxReq) do -- list of item's required crafting helpers
		-- return index -- if ANY proximity items exists
		for proxIndex,proxValue in pairs(m_sortUNIDS) do -- list of crafting helpers within range
			if m_sortPendingUNID >= 0 and gameItem.proxReq[requiredIndex] == m_sortPendingUNID then -- pending gets priority
				return (getTableCount(m_sortUNIDS) + 1)
			elseif m_lastSortUNID >= 0 and gameItem.proxReq[requiredIndex] == m_lastSortUNID then -- whatever you clicked on gets second priority
				return (getTableCount(m_sortUNIDS) + 1)
			-- elseif gameItem.proxReq[requiredIndex] == m_sortUNIDS[proxIndex] then -- everything else
			-- 	 return proxIndex
			end
		end
	end
	return 0
end

-- Gets whether an item requires a crafting helper that matches the last one clicked on
checkForProxReqLastSort = function(gameItem)
	if gameItem.proxReq == nil then return false end
	for requiredIndex,requiredValue in pairs(gameItem.proxReq) do -- list of item's required crafting helpers
		-- return UNID -- if ANY proximity items exists
		for proxIndex,proxValue in pairs(m_sortUNIDS) do -- list of crafting helpers within range
			if m_sortPendingUNID >= 0 and gameItem.proxReq[requiredIndex] == m_sortPendingUNID then -- pending gets priority
				return true
			elseif m_lastSortUNID >= 0 and gameItem.proxReq[requiredIndex] == m_lastSortUNID then -- whatever you clicked on gets second priority
				return true
			end
		end
	end
	return false
end

-- Gets whether an item requires a crafting helper; returns boolean
checkForProxReq = function(gameItem)
	if gameItem.proxReq == nil then return false end
	for requiredIndex,requiredValue in pairs(gameItem.proxReq) do -- list of item's required crafting helpers
		for proxIndex,proxValue in pairs(m_sortUNIDS) do -- list of crafting helpers within range
			-- return true if a proximity items exists
			if gameItem.proxReq[requiredIndex] == m_sortUNIDS[proxIndex] then
				return true
			end
		end
	end
	return false
end

-- Get size of a table with possible nil values
getTableCount = function(table)
	local count = 0
	for i,info in pairs(table) do
		if info then
			count = count + 1
		end
	end
	return count
end

formatTooltips = function(item)	
	if item then
		item.tooltip = {}

		item.tooltip["header"] = {label = item.output.properties.name}

		if item.output.properties.rarity then
			item.tooltip["header"].color = COLORS_VALUES[item.output.properties.rarity]
		end
		
		item.tooltip["body"] = {label = "\"" .. item.output.properties.description .. "\""}

		if item.output.properties.itemType == ITEM_TYPES.WEAPON then
			local level = tostring(item.output.properties.level)
			local range = tostring(item.output.properties.range)
			
			if item.itemType == ITEM_TYPES.BLUEPRINT then
				item.tooltip["footer1"] = {label = "Blueprint: "..tostring(item.output.properties.name)}
				item.tooltip["footer2"] = {label = "Level: "..tostring(level)}
				item.tooltip["footer3"] = {label = "Range: "..tostring(range)}
			else
				item.tooltip["footer1"] = {label = "Weapon Level: "..tostring(level)}
				item.tooltip["footer2"] = {label = "Range: "..tostring(range)}
			end
		elseif item.output.properties.itemType == ITEM_TYPES.ARMOR then
			local slotTypeDisplay = (item.output.properties.slotType:gsub("^%l", string.upper)) 
			local level = tostring(item.output.properties.level)
			
			if item.itemType == ITEM_TYPES.BLUEPRINT then 
				item.tooltip["footer1"] = {label = "Blueprint: "..tostring(item.output.properties.name)}
				item.tooltip["footer2"] = {label = "Armor Level: "..tostring(level)}
				item.tooltip["footer3"] = {label = "Slot: "..tostring(slotTypeDisplay)}
			else
				item.tooltip["footer1"] = {label = "Armor Level: "..tostring(level)}
				item.tooltip["footer2"] = {label = "Slot: "..tostring(slotTypeDisplay)}
			end
		elseif item.output.properties.itemType == ITEM_TYPES.TOOL then
			local level = tostring(item.output.properties.level)
			
			if item.itemType == ITEM_TYPES.BLUEPRINT then
				item.tooltip["footer1"] = {label = "Blueprint: "..tostring(item.output.properties.name)}
				item.tooltip["footer2"] = {label = "Level: "..tostring(level)}
			else
				item.tooltip["footer1"] = {label = "Tool Level: "..tostring(level)}
			end
		elseif item.output.properties.itemType == ITEM_TYPES.CONSUMABLE then
			local consumeValueDisplay = item.output.properties.consumeValue .. "%" 

			if item.output.properties.consumeType == "energy" then
				item.tooltip["footer1"] = {label = "Energy Bonus: "..tostring(consumeValueDisplay)}
			else
				item.tooltip["footer1"] = {label = "Health Bonus: "..tostring(consumeValueDisplay)}
			end
		elseif item.output.properties.itemType == ITEM_TYPES.PLACEABLE then

		elseif item.output.properties.itemType == ITEM_TYPES.GENERIC then

		elseif item.output.properties.itemType == ITEM_TYPES.AMMO then

		end
	end
end

updateInventoryMenu = function()
	local itemIndex = 1
	local startIndex = 1 + (ITEMS_PER_PAGE * (m_page - 1))

	InventoryHelper.setNameplates("name")
	--InventoryHelper.setIcons("GLID")

	-- Remove anvil icons
	for i=1,ITEMS_PER_PAGE do
		Control_SetVisible( gHandles["imgItemHighlight" .. i], false)
	end

	for i=startIndex,#g_displayTable do
		if itemIndex <= ITEMS_PER_PAGE then
			local item = g_displayTable[i]		

			formatTooltips(item)

			applyImage(gHandles["imgItem"..itemIndex], tonumber(item.output.properties.GLID))
			Control_SetVisible( gHandles["imgItem" .. itemIndex], true )

			local maxCraftCount = maximumCraftCount(item.UNID, 1)
			local nearHelper = nearCraftingHelper(item.UNID)
			local requiresHelper = checkForProxReq(item)

			local helperMatchesLastClick = checkForProxReqLastSort(item)
			
			Control_SetVisible( gHandles["imgItemOverlay" .. itemIndex], (maxCraftCount <= 0 or not nearHelper))
			Control_SetVisible( gHandles["imgItemHighlight" .. itemIndex], (requiresHelper and nearHelper and helperMatchesLastClick))

			if item.itemType == ITEM_TYPES.BLUEPRINT then
				Control_SetVisible(gHandles["imgBlueprint"..itemIndex], true)
				for _, blueprint in pairs(m_playerBlueprints) do
						if tostring(item.UNID) == tostring(blueprint.UNID) then
							if item.oneUse == "true" then 
								if blueprint.count >= 0 then
									local quantityLabel = gHandles["stcItemCount" ..itemIndex]
									local blueprintCount = tonumber(blueprint.count)
									
									Control_SetVisible(gHandles["imgItemCountBG"..itemIndex], true)
									Control_SetVisible(quantityLabel, blueprintCount > 0)
									Static_SetText(quantityLabel, blueprintCount)
								else
									Control_SetVisible(gHandles["imgItemCountBG"..itemIndex], false)
									Control_SetVisible(gHandles["stcItemCount"..itemIndex], false)	
								end
							else
								Control_SetVisible(gHandles["imgItemCountBG"..itemIndex], false)
								Control_SetVisible(gHandles["stcItemCount"..itemIndex], false)	
							end
							
						end
				end	
			else
				Control_SetVisible( gHandles["imgBlueprint" .. itemIndex], false )
				Control_SetVisible(gHandles["imgItemCountBG"..itemIndex], false)
				Control_SetVisible(gHandles["stcItemCount"..itemIndex], false)
			end
		end	
		itemIndex = itemIndex + 1	
	end
	
	for i=itemIndex ,ITEMS_PER_PAGE do
		Control_SetVisible( gHandles["imgItem" .. i], false )
		Control_SetVisible( gHandles["imgItemBG" .. i], false )
		Control_SetVisible( gHandles["imgBlueprint" .. i], false )
		Control_SetVisible( gHandles["imgItemOverlay" .. i], false )
		Control_SetVisible(gHandles["imgItemCountBG"..i], false)
		Control_SetVisible(gHandles["stcItemCount"..i], false)	
	end	
end

selectRecipe = function(UNID)	
	m_selectedRecipe = UNID

	if m_crafting then
		stopCrafting()
	end

	resetRecipeCounts()
	updateSelectedRecipe()
	Control_SetVisible( gHandles["btnCraft"], true )
end

resetRecipeCounts = function()
	m_craftedCount = 0
	m_totalCraftCount = 1
	m_currentCraftCount = 1
	EditBox_SetText(gHandles["edCount"], tostring(m_totalCraftCount), 0)
end

clearSelectedRecipe = function()
	m_selectedRecipe = 0
	Control_SetVisible( gHandles["imgCraftingToolbarCover"], true )
	Control_SetVisible( gHandles["imgSelectedBG"], false )
	Control_SetVisible( gHandles["btnCraft"], false )
end

updateSelectedRecipe = function()
	local recipe = m_recipes[m_selectedRecipe] or m_blueprints[m_selectedRecipe]
	if recipe then
		Static_SetText(gHandles["stcCurrRecipe"], recipe.output.properties.name)
		applyImage(gHandles["imgCurrRecipe"], recipe.output.properties.GLID)

		for i=1,3 do
			if recipe.inputs[i] then
				local itemNeedCount = recipe.inputs[i].count * m_totalCraftCount
				local itemHaveCount = m_inventoryCounts[tostring(recipe.inputs[i].UNID)] or 0

				Static_SetText(gHandles["stcReqItem" .. i], recipe.inputs[i].name)
				Static_SetText(gHandles["stcReqItemCount" .. i], itemNeedCount)

				Static_SetText(gHandles["stcHaveItemCount"..i], itemHaveCount)

				if itemHaveCount >= itemNeedCount then
					color = {a=255,r=255,g=255,b=255}
				else
					color = {a=255,r=255,g=0,b=0}
				end

				local stcHaveCount = Static_GetDisplayElement(gHandles["stcHaveItemCount"..i])
				BlendColor_SetColor(Element_GetFontColor(stcHaveCount), 0, color)

				Control_SetVisible( gHandles["stcReqItem" .. i], true )
				Control_SetVisible( gHandles["stcReqItemCount" .. i], true )
				Control_SetVisible( gHandles["stcHaveItemCount" .. i], true )
			else
				Control_SetVisible( gHandles["stcReqItem" .. i], false )
				Control_SetVisible( gHandles["stcReqItemCount" .. i], false )
				Control_SetVisible( gHandles["stcHaveItemCount" .. i], false )
			end
		end

		local nearProximityReq = true
		if recipe.proxReq then
			local proxReqName

			for i=1,#recipe.proxReq do
				if m_craftingHelpers[tostring(recipe.proxReq[i])] then
					if proxReqName then
						proxReqName = proxReqName .. ", " .. m_craftingHelpers[tostring(recipe.proxReq[i])].behaviorParams.craftingResource
					else
						proxReqName = m_craftingHelpers[tostring(recipe.proxReq[i])].behaviorParams.craftingResource
					end					
				end
			end
			
			m_craftingHelperTooltip["body"] = {label = proxReqName}

			local tempProxReqName = "Needs: "..(proxReqName or "")

			if tempProxReqName and string.len(tempProxReqName) > 34 then
				tempProxReqName = string.sub(tempProxReqName, 1, 31)
				tempProxReqName = tempProxReqName .. "..."
			end

			Static_SetText(gHandles["stcReqProximity"], tempProxReqName)
			Control_SetVisible(gHandles["stcReqProximity"], true)

			nearProximityReq = nearCraftingHelper(recipe.UNID)			

			if nearProximityReq then
				color = {a=255,r=255,g=255,b=255}
			else
				color = {a=255,r=255,g=0,b=0}
			end

			m_craftingHelperTooltip["header"].color = color
			
			local stcProxReqHandle = Static_GetDisplayElement(gHandles["stcReqProximity"])
			BlendColor_SetColor(Element_GetFontColor(stcProxReqHandle), 0, color)
			
		else
			Control_SetVisible(gHandles["stcReqProximity"], false)	
		end

		m_maxCraftCount = maximumCraftCount(m_selectedRecipe, m_totalCraftCount)
		updateRecipeCounts()

		Control_SetVisible( gHandles["imgCraftingToolbarCover"], false )
		
		Control_SetEnabled( gHandles["btnCraft"], m_maxCraftCount > 0 and nearProximityReq)
	end
end

moveRecipeSelectedBG = function(x, y)
	Control_SetVisible( gHandles["imgSelectedBG"], true )
	Control_SetLocationX(gHandles["imgSelectedBG"], x)
	Control_SetLocationY(gHandles["imgSelectedBG"], y)
end

updateRecipeCounts = function()
	local recipe = m_recipes[m_selectedRecipe] or m_blueprints[m_selectedRecipe]

	if m_totalCraftCount <= 1 then
		Control_SetEnabled(gHandles["btnCountDown"], false)
	else
		Control_SetEnabled(gHandles["btnCountDown"], true)
	end

	Static_SetText(gHandles["stcCraftCount"], tostring(m_craftedCount).." of "..tostring(m_totalCraftCount))
end

maximumCraftCount = function(UNID, multiplier)
	local recipe = m_recipes[UNID] or m_blueprints[UNID]
	local max

	for i=1,3 do
		local itemHaveCount = 0
		local itemNeedCount = 0
		if recipe.inputs[i] then
			itemHaveCount = m_inventoryCounts[tostring(recipe.inputs[i].UNID)]
			if itemHaveCount then
				itemNeedCount = recipe.inputs[i].count * multiplier
				local maxForInput
				maxForInput = math.floor(itemHaveCount/itemNeedCount)
				
				if max then
					max = maxForInput < max and maxForInput or max
				else
					max = maxForInput
				end
			else
				return 0
			end			
		end
	end

	if recipe.itemType == ITEM_TYPES.BLUEPRINT then
		for _, blueprint in pairs(m_playerBlueprints) do
			if tostring(recipe.UNID) == tostring(blueprint.UNID) then
				if recipe.oneUse == "true" then 
					if blueprint.count >= 0 then
						max = blueprint.count < max and blueprint.count or max
					else
						return 0
					end
				end				
			end
		end	
	else
	end

	return max or 0
end

nearCraftingHelper = function(UNID)
	local recipe = m_recipes[tostring(UNID)] or m_blueprints[tostring(UNID)]
	if recipe.proxReq and #recipe.proxReq > 0 then		
		for i=1,#recipe.proxReq do
			if tonumber(recipe.proxReq[i]) == 0 or (m_craftingHelpers[tostring(recipe.proxReq[i])] and m_craftingHelpers[tostring(recipe.proxReq[i])].inProximity) then
				return true
			end
		end
		return false
	end

	return true
end

-- -- -- -- -- -- --
-- Local functions
-- -- -- -- -- -- --

startCrafting = function()
	if not m_crafting then
		local recipe = m_recipes[m_selectedRecipe] or m_blueprints[m_selectedRecipe]
		if recipe then
			m_crafting = true
			m_craftedCount = 1
			updateSelectedRecipe()
		end

		Control_SetEnabled(gHandles["btnCountUp"], false)
		Control_SetEnabled(gHandles["btnCountDown"], false)

		Static_SetText(gHandles["btnCraft"], "Cancel")

		Control_SetEnabled(gHandles["edCount"], false)

		Dialog_SetModal(gDialogHandle, true)
	end
end

stopCrafting = function()
	m_crafting = false
	m_craftTime = 0
	Control_SetSize(gHandles["imgCraftProgress"], 0, 84)

	Control_SetEnabled(gHandles["btnCountUp"], true)
	Control_SetEnabled(gHandles["btnCountDown"], true)
	Control_SetEnabled(gHandles["edCount"], true)

	Static_SetText(gHandles["btnCraft"], "Craft")

	KEP_SetCurrentAnimationByGLID(0)
	Dialog_SetModal(gDialogHandle, false)
end

completeCraftingItem = function()
	local recipe = m_recipes[m_selectedRecipe] or m_blueprints[m_selectedRecipe]
	local inputs = recipe.inputs
	local output = recipe.output	
	
	-- Create new item purchased
	local transactionID = KEP_GetCurrentEpochTime()
	local removeTable = {}
	for i = 1, #inputs do
		local input = inputs[i]
		local removeDetails = {UNID = input.UNID, count = input.count or 1}
		removeDetails.lootInfo = {sourcePID = KEP_GetLoginName(), lootSource = "Crafting", transactionID = transactionID}
		table.insert(removeTable, removeDetails)
	end
		
	local addTable = {}
	local newItem = recipe.output
	newItem.count = 1
	if newItem.properties.itemType == ITEM_TYPES.WEAPON or newItem.properties.itemType == ITEM_TYPES.ARMOR or newItem.properties.itemType == ITEM_TYPES.TOOL then
		local durabilityType = newItem.properties.durabilityType
		local itemLevel = newItem.properties.itemLevel
		local instanceDurability = DEFAULT_MAX_DURABILITY

		if durabilityType == "limited" then
			instanceDurability = DURABILITY_BY_LEVEL[itemLevel]
		end
		
		newItem.instanceData = {levelMultiplier = getLevelMultiplier(), durability = instanceDurability}
	end
	-- Assign a loot source for metric detection
	newItem.lootInfo = {sourcePID = KEP_GetLoginName(), lootSource = "Crafting", transactionID = transactionID}
	table.insert(addTable, newItem)

	processTransaction(removeTable, addTable)

	KEP_EventCreateAndQueue("FRAMEWORK_TRACK_CRAFTING_COUNT")

	if recipe.oneUse == "true" then
		for index, blueprint in pairs(m_playerBlueprints) do
			if tostring(recipe.UNID) == tostring(blueprint.UNID) then
				if blueprint.count <= 1 then
					clearSelectedRecipe()
				end				

				removeItem(index, BLUEPRINT_PID)
			end
		end		
		updateDirty()
	end

	m_craftedCount = m_craftedCount + 1
	m_craftTime = 0

	if m_craftedCount > m_totalCraftCount then
		--Crafting batch has finished
		stopCrafting()
		resetRecipeCounts()
	end

	updateInventoryCounts()
	updateSelectedRecipe()
end

getRecipeFromIndex = function(index)
	local itemIndex = index + ( ITEMS_PER_PAGE * (m_page - 1) )
	if g_displayTable[itemIndex] and g_displayTable[itemIndex].UNID then
		return tostring(g_displayTable[itemIndex].UNID)
	end
end

-- Populates the menu
updateInventoryCounts = function()
	m_inventoryCounts = {}
	-- Compile UNID counts
	for i=1, #m_inventory do
		if m_inventory[i].UNID ~= 0 then
			-- New UNID entry
			if m_inventoryCounts[tostring(m_inventory[i].UNID)] == nil then
				m_inventoryCounts[tostring(m_inventory[i].UNID)] = m_inventory[i].count
			-- Duplicate entry. Increment UNID count
			else
				m_inventoryCounts[tostring(m_inventory[i].UNID)] = m_inventoryCounts[tostring(m_inventory[i].UNID)] + m_inventory[i].count
			end
		end
	end
end

getLevelMultiplier = function(levelMultiplier)
	levelMultiplier = levelMultiplier or math.random()
	levelMultiplier = math.ceil(levelMultiplier*10000)*0.0001
	return levelMultiplier
end