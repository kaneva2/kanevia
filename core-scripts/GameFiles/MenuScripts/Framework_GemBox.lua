--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Framework_GemBox.lua
--
-- Displays gem rewarded through gembox open
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("Easing.lua")

-- -- -- -- -- -- --
-- CONSTANTS
-- -- -- -- -- -- --

local GEM_COUNT = 5
local GEM_SPACING = 10
local GEM_POS_DIFF = 165

local GEM_BOUNDARIES = {10, 195, 380} -- Left, Center, Right
local GEM_LAYOUT = {SPIN = "spin", REWARD = "reward"}
local GEM_NAMES = {"a Topaz", "a Sapphire", "an Emerald", "a Ruby", "a Diamond"}

local DEFAULT_FLASH_TIME = 0.05
local DEFAULT_GEM_SIZE = 256
local DEFAULT_SIZE_MOD = 30

local DEFAULT_Y_POSS = 50


-- -- -- -- -- -- --
-- LOCAL FUNCTIONS
-- -- -- -- -- -- --

local setGemLayout -- ()

-- -- -- -- -- -- --
-- LOCAL VARIABLES
-- -- -- -- -- -- --

local m_gemAnchor = GEM_BOUNDARY_LEFT

local m_currGems = {} -- At most, 3 gems visible at a time

local m_tweening = false
local m_tweenThrob
local m_gemType = 0
local m_gemUNID
local m_gemSpin

local m_gemCount = 30 -- default gem count starts at 30 + (# of reward gem to spin)
local m_currGemCount = 0
local m_flashTime = 0
local m_slowRate = 0.0001

local m_gemCheck = 3

-- Called when the menu is created
function onCreate()
	Events.registerHandler("FRAMEWORK_UPDATE_GEM_COUNTS", updateGemCounts)

	m_gemSpin = true
	m_gemAnchor = GEM_BOUNDARY_LEFT

	m_flashTime = DEFAULT_FLASH_TIME

	m_currGems = {0, 0, 1} -- Set gems to first 3
	m_currGemCount = 1
	m_gemCount = m_gemCount + m_gemType

	for i=1, #m_currGems do
		if m_currGems[i] > 0 then
			Control_SetEnabled(gHandles["imgGem"..m_currGems[i]], true)
			Control_SetLocation(gHandles["imgGem"..m_currGems[i]], GEM_BOUNDARIES[i], Control_GetLocationY(gHandles["imgGem"..m_currGems[i]]))
		end
	end
end

function onDestroy()
	if m_gemSpin == false then
		local newItemEvent = KEP_EventCreate("FRAMEWORK_NEW_ITEM_MESSAGE")
		KEP_EventEncodeString(newItemEvent,"Gem")
		KEP_EventQueue(newItemEvent)
	end
end

-- On frame update : To whomever that has to use this in the future.. I'm sorry..
function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- Animate the gems for a total of 3 seconds (30 + gem# / 3 = time per gem)
	m_flashTime = m_flashTime - fElapsedTime
	
	if m_gemSpin then
		-- Are we still animating? Check the first gem for it's position		
		if m_currGemCount <= m_gemCount+1 then
			local flashLocation = Easing.linear(DEFAULT_FLASH_TIME - m_flashTime, 0, GEM_POS_DIFF, DEFAULT_FLASH_TIME)
			local flashColorIn = Easing.inQuart((DEFAULT_FLASH_TIME) - m_flashTime, 0, 255, DEFAULT_FLASH_TIME)
			local flashColorOut = Easing.outQuart((DEFAULT_FLASH_TIME) - m_flashTime, 0, 255, DEFAULT_FLASH_TIME)
			local flashSizeIn = Easing.inQuad((DEFAULT_FLASH_TIME) - m_flashTime, 0, DEFAULT_SIZE_MOD-5, DEFAULT_FLASH_TIME)
			local flashSizeOut = Easing.outQuad((DEFAULT_FLASH_TIME) - m_flashTime, 0, DEFAULT_SIZE_MOD-5, DEFAULT_FLASH_TIME)

			if m_gemCheck == 1 then
				-- LENS FLARE! PEW PEW FLASH!
				local flashFlareSize = Easing.linear(DEFAULT_FLASH_TIME*3 - m_flashTime, 0, 400, DEFAULT_FLASH_TIME*3)
				local flashFlareColor = Easing.outQuart(DEFAULT_FLASH_TIME - m_flashTime, 0, 255, DEFAULT_FLASH_TIME)

				Control_SetSize(gHandles["imgAbramsFlare"], flashFlareSize, flashFlareSize)
				Control_SetLocation(gHandles["imgAbramsFlare"], 270-(flashFlareSize/2), 120-(flashFlareSize/2))
				if flashFlareColor >= 0 and flashFlareColor <= 255 then
					BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgAbramsFlare"])), 0, {a=(255-flashFlareColor),r=255,g=255,b=255})
				end
			end

			-- Spin the gems' locations
			for i=1, #m_currGems do
				if m_currGems[i] == m_gemType and m_gemCheck == 1 and (GEM_BOUNDARIES[i] - flashLocation + 75) < 163 then
					-- Do nothing! Final Gem!!
				elseif m_currGems[i] > 0 then
					Control_SetLocation(gHandles["imgGem"..m_currGems[i]], GEM_BOUNDARIES[i] - flashLocation + 75, Control_GetLocationY(gHandles["imgGem"..m_currGems[i]]))
				end
			end

			-- Set alpha and size on each, specific gem (only if they're value is greater than 0)
			if m_currGems[1] > 0 then
				BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgGem"..m_currGems[1]])), 0, {a=(255-flashColorOut),r=255,g=255,b=255})
				if flashSizeOut <= DEFAULT_SIZE_MOD then
					Control_SetSize(gHandles["imgGem"..m_currGems[1]], DEFAULT_GEM_SIZE-flashSizeOut, DEFAULT_GEM_SIZE-flashSizeOut)
					Control_SetLocation(gHandles["imgGem"..m_currGems[1]], Control_GetLocationX(gHandles["imgGem"..m_currGems[1]])-(flashSizeOut/2), DEFAULT_Y_POSS+(flashSizeOut/2))
				end
			end
			if m_currGems[2] > 0 and m_gemCheck ~= 1 then
				BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgGem"..m_currGems[2]])), 0, {a=255,r=255,g=255,b=255})
				Control_SetSize(gHandles["imgGem"..m_currGems[2]], DEFAULT_GEM_SIZE, DEFAULT_GEM_SIZE)
				Control_SetLocation(gHandles["imgGem"..m_currGems[2]], Control_GetLocationX(gHandles["imgGem"..m_currGems[2]]), DEFAULT_Y_POSS)
			end
			if m_currGems[3] > 0 then
				BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles["imgGem"..m_currGems[3]])), 0, {a=flashColorIn,r=255,g=255,b=255})
				if flashSizeIn <= DEFAULT_SIZE_MOD then
					Control_SetSize(gHandles["imgGem"..m_currGems[3]], DEFAULT_GEM_SIZE-DEFAULT_SIZE_MOD+flashSizeIn, DEFAULT_GEM_SIZE-DEFAULT_SIZE_MOD+flashSizeIn)
					Control_SetLocation(gHandles["imgGem"..m_currGems[3]], Control_GetLocationX(gHandles["imgGem"..m_currGems[3]]), DEFAULT_Y_POSS+15-(flashSizeIn/2))
				end
			end

			-- If gem locations are past their designated location, then switch slots
			if Control_GetLocationX(gHandles["imgGem"..m_currGems[m_gemCheck]]) <= (GEM_BOUNDARIES[m_gemCheck]-115) then
				-- If a gem type has been passed in, start decrementing down to the reward gem!
				if m_gemType ~= 0 then
					-- first gem after gem is passed in, reset gems to begin spinning toward reward
					if m_currGemCount == 1 then
						-- m_currGems[1] = 4
						-- m_currGems[2] = 5
						m_currGems[3] = 1
					-- If gem count is near the end, change textures
					elseif m_currGemCount == 27 then
						Element_AddTexture(Image_GetDisplayElement(gHandles["imgGem1"]), gDialogHandle, "topaz.dds")
						Element_AddTexture(Image_GetDisplayElement(gHandles["imgGem2"]), gDialogHandle, "sapphire.dds")
						Element_AddTexture(Image_GetDisplayElement(gHandles["imgGem3"]), gDialogHandle, "emerald.dds")
						Element_AddTexture(Image_GetDisplayElement(gHandles["imgGem4"]), gDialogHandle, "ruby.dds")
						Element_AddTexture(Image_GetDisplayElement(gHandles["imgGem5"]), gDialogHandle, "diamond.dds")
					else
						-- Reset flashtime
						if m_currGemCount > (5 + m_gemType) then
							m_slowRate = m_slowRate * 1.35
							DEFAULT_FLASH_TIME = DEFAULT_FLASH_TIME + m_slowRate
						end
					end
					-- Increment gem count
					m_currGemCount = m_currGemCount + 1
				end
				m_flashTime = DEFAULT_FLASH_TIME

				-- Disable gem that is leaving
				if m_currGems[1] > 0 then
					Control_SetEnabled(gHandles["imgGem"..m_currGems[1]], false)
				end

				-- Check if we've reached the final gem
				if m_currGemCount == m_gemCount+1 then
					m_currGems[1] = m_currGems[2]
					m_currGems[2] = m_currGems[3]
					m_currGems[3] = 0

					m_gemCheck = 1

					Control_SetEnabled(gHandles["imgAbramsFlare"], true)
				-- Move all gems over a slot
				else
					m_currGems[1] = m_currGems[2]
					m_currGems[2] = m_currGems[3]
					if m_currGems[3] >= 5 then
						m_currGems[3] = 1
					else
						m_currGems[3] = m_currGems[3] + 1
					end

					-- Enable gem that is entering
					Control_SetEnabled(gHandles["imgGem"..m_currGems[3]], true)
				end
			end
		-- Done flashing
		else
			m_gemSpin = false

			-- Ensure gem position
			Control_SetLocation(gHandles["imgGem"..m_gemType], 165, DEFAULT_Y_POSS)
			Static_SetText(gHandles["stc_rewardText"],"You got "..GEM_NAMES[m_gemType].."!")

			Control_SetEnabled(gHandles["stc_rewardText"], true)
			Control_SetEnabled(gHandles["btn_Ok"], true)
			Control_SetEnabled(gHandles["imgAbramsFlare"], false)
		end
	end
end

-- When the ok button is clicked
function btn_Ok_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

function openGem()
	-- Initialize spin with reward gem as final goal
	m_gemCount = m_gemCount + m_gemType
	m_flashTime = DEFAULT_FLASH_TIME
end

-- -- -- -- -- -- --
-- EVENT HANDLERS
-- -- -- -- -- -- --
function updateGemCounts(event)
	m_gemType = event.gemType or 1
	ParseGemRewardData(event.text)
end

-- Read gem reward web call return
function ParseGemRewardData(xmlData)
	local result = string.match(xmlData, "<ReturnCode>([0-9]+)</ReturnCode>")
	-- if the web call is a success
	if result == "0" then
		s, strPos, m_rewardSuccess = string.find(xmlData, "<ResultDescription>(.-)</ResultDescription>")
	end
	if m_rewardSuccess == nil then
		LogWarn("Success gem reward data was not returned from the web call Return xml["..tostring(xmlData).."]")
		MenuCloseThis()
	end

	if m_rewardSuccess then
		openGem()
	end
end