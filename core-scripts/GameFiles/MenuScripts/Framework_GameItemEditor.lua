--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Menu.lua - Kaneva Menu Script Template
-- 
-- Template script for creating a new Kaneva client menu.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_GameItemEditorPrefabs.lua")
dofile("Framework_InventoryHelper.lua")
dofile("..\\MenuScripts\\Lib_Web.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Web			= _G.Web

-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------

-- List height properties
local CATEGORY_ROW_HEIGHT = 20
local PROPERTY_ROW_HEIGHT = 30

-- Anchor point for attaching properties
local LIST_ANCHOR_POINT = {x = 11, y = 136}

--Endzones and height of window
local LIST_ENDZONE_TOP = 100
local LIST_ENDZONE_BOTTOM = 445
local LIST_HEIGHT = 310

--Control insert index
local MENU_INSERT_INDEX = 1

--Scroll bar measurements
local SCROLL_BAR_WIDTH = 16
local SCROLL_BAR_MAX = 278
local SCROLL_BAR_TOP = 151
local SCROLL_MOVE_DISTANCE = 5

local GI_LOAD_MESSAGE1 = "Saving these changes would put your world over the object limit."
local GI_LOAD_MESSAGE2 = "Free some space by removing game objects."

-- Y-offset for item selector and dialog editor
local EDITOR_LOCATION_OFFSET_Y = 163

local QUEST_EDITOR_OFFSET_Y = 100

local PROPERTY_BACKGROUND_COLORS = {{a=255, r=230, g=230, b=230}, {a=255, r=255, g=255, b=255}}

local MENU_MODE = {EDIT_ITEM = "editItem", NEW_ITEM = "newItem"}

local PLAYLIST_TYPE_MEDIA	= "m"

local DEFAULT_NPC_MALE_GLID = 4708107
local DEFAULT_NPC_FEMALE_GLID = 4708106
local DEFAULT_NPC_FEMALE =  '{"scaleY":101,"skinColor":0,"faceIndex":1,"GLIDS":[1247,1248,1249],"beardColor":0,"faceCover":0,"scaleXZ":101,"dbIndex":11,"hairColor":0,"hairIndex":0,"eyebrowIndex":0,"eyeColor":0}'

local DEFAULT_NPC_MALE = '{"scaleY":100,"skinColor":0,"faceIndex":1,"GLIDS":[1172,1173,1174],"beardColor":0,"faceCover":0,"scaleXZ":100,"dbIndex":14,"hairColor":0,"hairIndex":0,"eyebrowIndex":0,"eyeColor":0}'

local ALLOW_ALL			= 1
local ALLOW_PRIVATE		= 2
local ALLOW_OWNER_ONLY	= 3
-----------------------------------------------------------------
-- Variables
-----------------------------------------------------------------

local m_menuMode = MENU_MODE.EDIT_ITEM
local m_playerName

local m_pendingItemEvent

--UNID for item being edited
local m_itemUNID = 0
--Item properties
local m_item
--Prefab type for item (See Framework_GameItemEditorPrefabs)
local m_prefabType

--Item values for attributes. Used to help track, update, and tie attributes to certain pieces of the editor
local m_itemValues = {}
--UNID name lookup
local m_gameItemNames = {}
--GLID name lookup
local m_glidNames = {}
--Custom drop down values - Sent from opening menu
local m_dropDownParams = {}
--Has name been changed? Check duplicate
local m_nameChanged = false
--Collapsable categories list 
local m_categoryList = {}
--Sub-category properties list
local m_propertyLists = {}
-- added for the game item cache, keys are UNID as a string 
local m_gameItems = {} 

--Playlist name lookup
local m_playlistNames = {}

--Current property/category insertion location
local m_listAnchor = LIST_ANCHOR_POINT.y
--Total height of the properties
local m_listHeight = 0
--Scroll bar position distance modifier
local m_scrollBarMod = nil
--Is the bar scrolling?
local m_barScrolling = false
--Scroll bar previous position
local m_scrollPrevPos = nil

local m_editingGameItem = false

local m_imageName = nil

--Used to count rows to set colors
local m_propertyRows = 0

--needed for Playlist
local playlistID = 0

--control for the meshtype dropdown
local m_npcDropdown = nil

-----------------------------------------------------------------
-- Function definitions
-----------------------------------------------------------------

-- When the menu is created
function onCreate()
	KEP_EventRegister("SAVE_GAME_ITEM_INFO", KEP.HIGH_PRIO)
	KEP_EventRegister("PLAYLIST_SELECT_EVENT", KEP.HIGH_PRIO)
	KEP_EventRegister("IMAGE_SELECT_EVENT", KEP.HIGH_PRIO)
	KEP_EventRegister("WORLD_SELECT_EVENT", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_EDIT_POPOUT_SAVED", KEP.HIGH_PRIO)
	KEP_EventRegister("FRAMEWORK_EDIT_POPOUT_OPEN", KEP.HIGH_PRIO)

	-- Client event handlers
	KEP_EventRegisterHandler("getGameItemInfo", "EDIT_GAME_ITEM_INFO", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("changeGameItemParam", "CHANGED_PARAM_EVENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("colorPickerEventHandler", "COLOR_PICKER_EVENT", KEP.MED_PRIO )
	KEP_EventRegisterHandler("playlistSelectEventHandler", "PLAYLIST_SELECT_EVENT", KEP.MED_PRIO )
	KEP_EventRegisterHandler("imageSelectEventHandler", "IMAGE_SELECT_EVENT", KEP.MED_PRIO )
	KEP_EventRegisterHandler("worldSelectEventHandler", "WORLD_SELECT_EVENT", KEP.MED_PRIO )
	KEP_EventRegisterHandler("genericPromptReturnHandler", "FRAMEWORK_GENERIC_PROMPT_RETURN", KEP.MED_PRIO )
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateNPCData", "FRAMEWORK_SAVE_NPC_DATA", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onEditPopoutSaved", "FRAMEWORK_EDIT_POPOUT_SAVED", KEP.HIGH_PRIO)

	Events.registerHandler("FRAMEWORK_UNIQUE_NAME_RETURN", uniqueNameReturnHandler)
	Events.registerHandler("FRAMEWORK_EDIT_GAME_ITEM_CONFIRMATION", editGameItemConfirmation)
	Events.registerHandler("FRAMEWORK_ATTRIBUTE_RETURN", attributeValidationHandler)

	Events.registerHandler("FRAMEWORK_WAYPOINT_DELTED", questWaypointDeleted)

	requestInventory(GAME_PID)
	
	m_insertLocation = m_listAnchor
	m_playerName = KEP_GetLoginName()
end

function onDestroy()
	if m_item.pendingWaypoint then
		Events.sendEvent("FRAMEWORK_REMOVE_QUEST_WAYPOINT", {waypointPID = m_item.pendingWaypoint})
	end
end
-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	log("--- Framework_GameItemEditor updateGameClient")
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)

	if ( next(m_gameItems) ) then return end
	log("--- Framework_GameItemEditor updateGameClientFull")
	if ( next(m_gameItems) ) then return end
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems


	initiateItemValues() -- Hookup GLID/UNID item names

	log("--- initializeItem m_prefabType: ".. tostring(m_prefabType))
	createListCategory(PROPERTIES_PREFABS[m_prefabType]) -- Create collapsable categories	
	createPropertiesList(1) -- Open initial properties for first category
	updateList() -- Update list display
	updateGameItem(false) -- Update item values
	displayItem()
	updatePropertyValueNames()
end

function getGameItemInfo(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_pendingItemEvent = {}
	m_pendingItemEvent.UNID = KEP_EventDecodeString(tEvent) -- UNID of item
	m_pendingItemEvent.itemInfo = KEP_EventDecodeString(tEvent) -- Item properties
	m_pendingItemEvent.editorType = KEP_EventDecodeString(tEvent) -- Used to set menu name
	m_pendingItemEvent.prefabType = KEP_EventDecodeString(tEvent) -- Prefab type for the item



	if KEP_EventMoreToDecode(tEvent) == 1 then
		m_pendingItemEvent.menuMode = KEP_EventDecodeString(tEvent) -- New Item?
	end

	if KEP_EventMoreToDecode(tEvent) == 1 then
		m_pendingItemEvent.dropDownParams = KEP_EventDecodeString(tEvent) --Custom drop down values for given attributes
	end

	if m_item == nil then
		initializeItem(m_pendingItemEvent)		
	else
		MenuOpenModal("Framework_GenericPrompt.xml")
		local promptEvent = KEP_EventCreate( "FRAMEWORK_GENERIC_PROMPT_INIT" )
		KEP_EventEncodeString(promptEvent, "yesNo")
		KEP_EventEncodeString(promptEvent, "Are you sure you want to stop editing this game item? You will lose all changes.")
		KEP_EventEncodeString(promptEvent, "Warning")
		KEP_EventQueue( promptEvent )
	end
end

--Response from the warning prompt
function genericPromptReturnHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local response = KEP_EventDecodeString(tEvent)

	if response == "yes" then
		clearCurrentItem()
		initializeItem(m_pendingItemEvent)
		displayItem()
		initiateItemValues() -- Hookup GLID/UNID item names
		createListCategory(PROPERTIES_PREFABS[m_prefabType]) -- Create collapsable categories	
		createPropertiesList(1) -- Open initial properties for first category
		updateList() -- Update list display
		updateGameItem(false) -- Update item values
	elseif response == "no"	then

	end	
end

-- Attribute updated from item selector
function changeGameItemParam(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local event = {}
	event.attribute = KEP_EventDecodeString(tEvent) -- Updated attribute
	event.value = KEP_EventDecodeString(tEvent) -- Updated value
	event.name = KEP_EventDecodeString(tEvent) -- Update name

	if m_itemValues[event.attribute] then
		-- If accessories list at specific index

		local itemValueType = "None"

		if KEP_EventMoreToDecode(tEvent) == 1 then -- If there is a new GLID or UNID name to assign to this value
			itemValueType = KEP_EventDecodeString(tEvent)

			--Set the value/name pair to the correct table (UNID or GLID)
			if itemValueType == "UNID" then
				m_gameItemNames[tostring(event.value)] = event.name
			elseif itemValueType == "GLID" then
				m_glidNames[tostring(event.value)] = event.name
			end
		end

		if type(m_item[event.attribute]) == "table" then
			local index = tonumber(KEP_EventDecodeString(tEvent))

			if KEP_EventMoreToDecode(tEvent) == 1 then -- If there is a base attribute to assign to
				event.baseAttribute = KEP_EventDecodeString(tEvent)
				if itemValueType == "TEXT" then
					m_item[event.attribute][index][event.baseAttribute] = tostring(event.value)
				else
					if event.baseAttribute ~= "" then
						m_item[event.attribute][index][event.baseAttribute] = tonumber(event.value)
					else
						m_item[event.attribute][index] = tonumber(event.value)
					end
				end
				if event.baseAttribute ~= "" then
					Static_SetText(gHandles[m_itemValues[event.baseAttribute .. index].control], event.name)
				else
					Static_SetText(gHandles[m_itemValues[event.attribute .. index].control], event.name)
				end
			else
				if tostring(itemValueType) == "TEXT" then
					m_item[event.attribute][index] = tostring(event.value)
				else
					m_item[event.attribute][index] = tonumber(event.value)
				end
				Static_SetText(gHandles[m_itemValues[event.attribute .. index].control], event.name)
			end			
		else
			m_item[event.attribute] = event.value
			Static_SetText(gHandles[m_itemValues[event.attribute].control], event.name)
			m_itemValues[event.attribute].value = event.value

			if m_prefabType == "recipe" and event.attribute == "output" then -- is this for recipe (special case for image and description)
				local outputGLID = tonumber(KEP_EventDecodeString(tEvent))

				if outputGLID and m_prefabType == "recipe" then -- Only for recipes
					local eh = Image_GetDisplayElement(gHandles["imgItemThumbnail"])
					KEP_LoadIconTextureByID(outputGLID, eh, gDialogHandle)

					m_item.description = KEP_EventDecodeString(tEvent)
					EditBox_SetText( gHandles["edDescription"], m_item.description, false)
				end
			end
		end
		
		-- Update thumbnail?
		if m_itemValues[event.attribute].thumbnail then
			if event.attribute ==  "GLID" or event.attribute == "meshGLID" then
				local eh = Image_GetDisplayElement(gHandles["imgItemThumbnail"])
				KEP_LoadIconTextureByID(tonumber(event.value), eh, gDialogHandle)
			elseif event.attribute == "pathRider_parentUNID" and tonumber(event.value) > 0 then
				local eh = Image_GetDisplayElement(gHandles["imgItemThumbnail"])
				KEP_LoadIconTextureByID(m_gameItems[event.value].GLID, eh, gDialogHandle)
			end
		end
	end
end

-- Update color from Color Picker
function colorPickerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local attribute = KEP_EventDecodeString(event)
	m_item[attribute.."Red"] = KEP_EventDecodeNumber(event)
	m_item[attribute.."Green"] = KEP_EventDecodeNumber(event)
	m_item[attribute.."Blue"] = KEP_EventDecodeNumber(event)

	local color = { a=255, r=m_item[attribute.."Red"], g=m_item[attribute.."Green"], b=m_item[attribute.."Blue"]}

	BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles[m_itemValues[attribute].control])), 0, color)
end

function playlistSelectEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	local attribute = KEP_EventDecodeString(event)
	m_itemValues[attribute].name = KEP_EventDecodeString(event)
	m_itemValues[attribute].value = KEP_EventDecodeString(event)
	playlistID = string.match(m_itemValues[attribute].value, "<asset_id>(%d-)</asset_id>") or string.match(m_itemValues[attribute].value, "<asset_group_id>(%d-)</asset_group_id>") or "0"
	playlistID = tonumber(playlistID)
	m_playlistNames[playlistID] = m_itemValues[attribute].name
	updatePropertyValueNames()
end

function imageSelectEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	local attribute = KEP_EventDecodeString(event)
	if m_itemValues[attribute] then
		m_imageName = KEP_EventDecodeString(event)
		m_itemValues[attribute].value = KEP_EventDecodeString(event)
		m_item[attribute] = m_itemValues[attribute].value
		updatePropertyValueNames()
	end
end

function worldSelectEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local attribute = KEP_EventDecodeString(event)
	m_itemValues[attribute].value = KEP_EventDecodeString(event)
	m_item[attribute] = m_itemValues[attribute].value
	updatePropertyValueNames()
end

-- Return from the server if a name was unique
function uniqueNameReturnHandler(event)
	local original 	 = event.original
	local isUnique 	 = event.isUnique
	local uniqueName = event.uniqueName
	local mode 		 = event.mode
	
	if isUnique then
		-- Save away
		updateGameItem(true)

		if validateGameItem(m_item, m_pendingItemEvent.UNID) then
			saveGameItem()
		end		
	else
		-- Capitilize first letter of the system
		local itemSystem = m_item.itemType:gsub("^%l", string.upper)
		-- Open the name conflict menu
		MenuOpen("Framework_GameItemNameConflict.xml")
		local errorMessage = "A game item \""..tostring(EditBox_GetText(gHandles["edName"])).."\" already exists in your "..tostring(itemSystem).." System."
		local event = KEP_EventCreate("FRAMEWORK_NAME_CONFLICT_MESSAGE")
		KEP_EventEncodeString(event, errorMessage)
		KEP_EventQueue(event)
		
		if m_menuMode == MENU_MODE.EDIT_ITEM then
			EditBox_SetText(gHandles["edName"], m_item.name, false)
		elseif m_menuMode == MENU_MODE.NEW_ITEM then
			EditBox_SetText(gHandles["edName"], uniqueName, false)
		end
	end
end

function attributeValidationHandler(event)
	local invalid = event.valid
	local attribute = event.attribute
	local prevValue = event.value
	if invalid then
		local messageAttribute = attribute
		if attribute == "meshType" then
			m_itemValues[attribute].value = prevValue
			ComboBox_SetSelectedByIndex(m_npcDropdown, prevValue - 1)
			messageAttribute = "character mesh"
		end	
		MenuOpen("Framework_GameItemAttributeConflict.xml")
		local errorMessage = "You cannot change the "..tostring(messageAttribute).." for this object because it has already been placed in the world. Please make a new "..tostring(m_item.name).." if you wish to change that field."
		local event = KEP_EventCreate("FRAMEWORK_ATTRIBUTE_CONFLICT_MESSAGE")
		KEP_EventEncodeString(event, errorMessage)
		KEP_EventEncodeString(event,  m_prefabType)
		KEP_EventQueue(event)
	else
		if attribute == "flag area" then
			saveGameItem()
		else
			--Paperdoll stuff
			if m_itemValues[attribute].value == 1 then
				m_itemValues["meshGLID"].value = m_item.lastGLID or 0
				m_item.GLID = tonumber(m_item.lastGLID) or 0
				--webcall
				local glidList = tostring(m_itemValues["meshGLID"].value)
				local web_address = GameGlobals.WEB_SITE_PREFIX .. "kgp/instantBuy.aspx?operation=info&items=" .. glidList
				makeWebCall(web_address, WF.GET_SCRIPT_BUNDLE)
				--Icon
				local eh = Image_GetDisplayElement(gHandles["imgItemThumbnail"])
				KEP_LoadIconTextureByID(tonumber(m_itemValues["meshGLID"].value), eh, gDialogHandle)
			elseif m_itemValues[attribute].value == 2 then
				Static_SetText(gHandles[m_itemValues["meshGLID"].control], "Female Avatar")
				if tonumber(m_itemValues["meshGLID"].value) then
					m_item.lastGLID = m_itemValues["meshGLID"].value
				end

				--Set the default values on change
				m_itemValues["meshGLID"].value = DEFAULT_NPC_FEMALE
				m_item.meshGLID = DEFAULT_NPC_FEMALE
				m_item.GLID = DEFAULT_NPC_FEMALE_GLID

			else
				Static_SetText(gHandles[m_itemValues["meshGLID"].control], "Male Avatar")
				if tonumber(m_itemValues["meshGLID"].value) then
					m_item.lastGLID = m_itemValues["meshGLID"].value
				end

				--Set the default values on change
				m_itemValues["meshGLID"].value = DEFAULT_NPC_MALE
				m_item.meshGLID = DEFAULT_NPC_MALE
				m_item.GLID = DEFAULT_NPC_MALE_GLID
			end
			displayItem()
		end
	end
end

function questWaypointDeleted(event )
	if m_item.waypointPID and tonumber(event.PID) == tonumber( m_item.waypointPID) then
		m_item.waypointPID = 0
		m_item.pendingWaypoint = nil
	end
end

-- Called when a Game Item edit attempt has been resolved
function editGameItemConfirmation(event)
	if m_editingGameItem then
		if event.success then
			Events.sendEvent("DOWNLOAD_GAME_ITEM_ICON", {UNID = m_itemUNID, broadcast = true})
			MenuCloseThis()
		else
			-- Open the GameItemLoadWarning menu and pass it a message
			if MenuIsClosed("Framework_GameItemLoadWarning.xml") then
				MenuOpen("Framework_GameItemLoadWarning.xml")
			end
			local ev = KEP_EventCreate("FRAMEWORK_GAME_ITEM_LOAD_MESSAGE")
			KEP_EventEncodeString(ev, GI_LOAD_MESSAGE1)
			KEP_EventEncodeString(ev, GI_LOAD_MESSAGE2)
			KEP_EventQueue(ev)
		end
		m_editingGameItem = false
	end
end

--Webcall return for GLID names
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == 179 then
		local objectData = KEP_EventDecodeString( event )
		local returnCode = string.match(objectData, "<ReturnCode>(.-)</ReturnCode>")
		if tonumber(returnCode) ~= 0 then
			return
		end

		local name
		local glid
		for object in string.gmatch(objectData, "<Item_x0020_Info>(.-)</Item_x0020_Info>") do
			name = Web.getDataByTag(object, "display_name", "(.-)")
			glid = Web.getDataByTag(object, "global_id", 	"(.-)")

			m_glidNames[glid] = name or "None"
		end

		updatePropertyValueNames()
	elseif filter == WF.CREATE_WORLD then
		local responseXML = Event_DecodeString(event)
		local code, desc, url = parseResponseCreateWorld(responseXML)
		if (code >= 0) then
			requestCreateWorldSuccess(code, desc, url)
		else
			requestCreateWorldFailed(code, desc, url)
		end
	end
end


-- -- -- -- -- -- --
-- Dialog Functions
-- -- -- -- -- -- --

function Dialog_OnRender(dialogHandle, fElapsedTime)
	local x, y
	local itemControl

	-- If scroll bar moved, then update list
	if m_scrollBarMod then
		updateListPosition(m_scrollBarMod)
	end
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	--If scrolling, then update list
	if m_barScrolling then
		scrollMod = (m_scrollPrevPos - y) * (m_listHeight/LIST_HEIGHT)
		
		scrollMod = round(scrollMod)

		updateListPosition(scrollMod)
		m_scrollPrevPos = y
	end
end

function Dialog_OnMoved(dialogHandle, x, y)
	if m_menuHidden then
		MenuSetMinimized("HudBuild.xml", true)
		MenuSetLocationThis(-1000, -1000)
	end
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	MenuBringToFrontThis()	
end
-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

function btnEditName_OnButtonClicked(buttonHandle)
	local handle = gHandles["edName"]
	Control_SetEnabled( handle, true )
	Control_SetFocus( handle )
	Dialog_SetModal(gDialogHandle, true)
end

function edName_OnFocusOut(controlHandle)
	local handle = gHandles["edName"]
	Control_SetEnabled(handle, false)
	Dialog_SetModal(gDialogHandle, false)
end

-- Called when the editbox containing an item's name has been changed
function edName_OnEditBoxChange(editBoxHandle, editBoxText)
	-- If the name is different from the one originally displayed
	if editBoxText ~= m_item.name then
		m_nameChanged = true
	else
		m_nameChanged = false
	end
end

function btnEditDescription_OnButtonClicked(buttonHandle)
	local handle = gHandles["edDescription"]
	Control_SetEnabled( handle, true )
	Control_SetFocus( handle )
	Dialog_SetModal(gDialogHandle, true)
end

function edDescription_OnFocusOut(controlHandle)
	local handle = gHandles["edDescription"]
	Control_SetEnabled( handle, false )
	Dialog_SetModal(gDialogHandle, false)
end

-- Saves item and closes the menu
function btnSave_OnButtonClicked(buttonHandle)
	-- Editing an item
	if m_menuMode == MENU_MODE.EDIT_ITEM then
		-- Name changed check
		if m_nameChanged then
			-- Check if this name is unique
			local name = EditBox_GetText(gHandles["edName"])
			Events.sendEvent("UNIQUE_NAME_CHECK", {originalName = m_item.name, name = name, item = m_item, mode = "Save"})
		else
			updateGameItem(true)
			
			if validateGameItem(m_item, m_pendingItemEvent.UNID) then
				saveGameItem()
			end		
		end
	-- New Item
	elseif m_menuMode == MENU_MODE.NEW_ITEM then
		-- Check if this name is unique
		local name = EditBox_GetText(gHandles["edName"])
		Events.sendEvent("UNIQUE_NAME_CHECK", {originalName = m_item.name,name = name, item = m_item, mode = "Save"})
	end
end

--Only handling the welcome screen preview. If any other item will use the preview button, then this will need to be reworked
function btnPreview1_OnButtonClicked(buttonHandle)
	updateGameItem(true)
	if m_item.showWelcome ~= nil then
		
		MenuOpen("Framework_Welcome")

		local previewEvent = KEP_EventCreate("FRAMEWORK_OPEN_WELCOME")
		KEP_EventEncodeString(previewEvent, Events.encode(m_item))
		KEP_EventEncodeString(previewEvent, "preview")
		
		KEP_EventQueue(previewEvent)
	elseif m_item.overviewEnabled ~= nil then
		MenuOpen("Framework_Overview")
		local previewEvent = KEP_EventCreate("FRAMEWORK_OPEN_OVERVIEW")
		KEP_EventEncodeString(previewEvent, Events.encode(m_item))
		KEP_EventEncodeString(previewEvent, "preview")
		
		KEP_EventQueue(previewEvent)
	end
end

-- ======= Scroll bar input handling ======= --
function btnScrollBar_OnLButtonDown(buttonHandle, x, y)
	m_barScrolling = true
	m_scrollPrevPos = y
end

function btnScrollBar_OnLButtonUp(controlHandle, x, y)
	m_scrollBarMod = nil
	m_scrollPrevPos = nil
	m_barScrolling = false
	updateScrollBar()
end

function btnScrollBar_OnMouseLeave(controlHandle)
	m_scrollBarMod = nil
	m_scrollPrevPos = nil
	m_barScrolling = false
	updateScrollBar()
end

function btnScrollUp_OnLButtonDown(buttonHandle)
	m_scrollBarMod = 1 * SCROLL_MOVE_DISTANCE
	Control_SetFocus( gHandles["btnScrollUp"] )
end

function btnScrollUp_OnLButtonUp(controlHandle, x, y)
	m_scrollBarMod = nil
end

function btnScrollUp_OnMouseLeave(controlHandle)
	m_scrollBarMod = nil
end

function btnScrollDown_OnLButtonDown(buttonHandle)
	m_scrollBarMod = -1 * SCROLL_MOVE_DISTANCE
	Control_SetFocus( gHandles["btnScrollDown"] )
end

function btnScrollDown_OnLButtonUp(controlHandle, x, y)
	m_scrollBarMod = nil
end

function btnScrollDown_OnMouseLeave(controlHandle)
	m_scrollBarMod = nil
end
-- ======= Scroll bar input handling ======= --

-- -- -- -- -- -- --
-- Local Functions
-- -- -- -- -- -- --

--Display item icon, name, and description
function displayItem()
	local eh = Image_GetDisplayElement(gHandles["imgItemThumbnail"])

	if m_item.pathRider_parentUNID  and  tonumber(m_item.pathRider_parentUNID) > 0 and m_gameItems[tostring(m_item.pathRider_parentUNID)]  then
		KEP_LoadIconTextureByID(tonumber(m_gameItems[tostring(m_item.pathRider_parentUNID)].GLID), eh, gDialogHandle)
	else 
		KEP_LoadIconTextureByID(tonumber(m_item.GLID), eh, gDialogHandle)
	end
	

	EditBox_SetText(gHandles["edName"], m_item.name, false)

	if m_item.description then
		EditBox_SetText( gHandles["edDescription"], m_item.description, false)
	end
end

function initializeItem(event)

	m_itemUNID = event.UNID
	m_item = Events.decode(event.itemInfo)

	

	m_prefabType = event.prefabType

	if event.dropDownParams then
		m_dropDownParams = Events.decode(event.dropDownParams)
	end

	m_menuMode = event.menuMode or MENU_MODE.EDIT_ITEM	

	if m_menuMode == MENU_MODE.NEW_ITEM then
		m_item = deepCopy(NEW_ITEM_DEFAULTS[m_prefabType])
	else
		-- Make sure old items are compatable with changes made to the menus
		enforceOldItemCompatibility(m_prefabType, m_item)
	end

	
	
	--Special cases for prefab types
	if m_prefabType == "mainSettings" or m_prefabType == "environmentSettings" or m_prefabType == "welcomeSettings" or m_prefabType == "overviewSettings" then
		--Don't allow save-as or editing name/description
		Control_SetEnabled( gHandles["imgEditName"], false)
		Control_SetVisible( gHandles["imgEditName"], false)
		Control_SetEnabled( gHandles["btnEditName"], false)
		Control_SetVisible( gHandles["btnEditName"], false)
		Control_SetEnabled( gHandles["imgEditDescription"], false)
		Control_SetVisible( gHandles["imgEditDescription"], false)
		Control_SetEnabled( gHandles["btnEditDescription"], false)
		Control_SetVisible( gHandles["btnEditDescription"], false)
		Control_SetVisible( gHandles["edDescription"], false)		
	elseif m_prefabType == "recipe" then
		--Don't allow editing description
		Control_SetEnabled( gHandles["btnEditDescription"], false)
		Control_SetVisible( gHandles["btnEditDescription"], false)	
		Control_SetEnabled( gHandles["imgEditDescription"], false)
		Control_SetVisible( gHandles["imgEditDescription"], false)
	end

	Dialog_SetCaptionText(gDialogHandle, " " .. event.editorType)
	MenuBringToFrontThis()
end

function clearCurrentItem()

	for i=1,#m_categoryList do
		for j=1,#m_categoryList[i] do
			Dialog_RemoveControl( gDialogHandle, m_categoryList[i][j] )
		end

		if m_propertyLists[i] then
			for j=1,#m_propertyLists[i] do
				for k=1,#m_propertyLists[i][j] do
					Dialog_RemoveControl( gDialogHandle, m_propertyLists[i][j][k] )
				end
			end				
		end
	end	

	--Reset variables
	m_propertyLists = {}
	m_categoryList = {}
	m_itemValues = {}
	m_dropDownParams = {}
	m_gameItemNames = {}
	m_glidNames = {}
	m_nameChanged = false
	m_listAnchor = LIST_ANCHOR_POINT.y
	m_listHeight = 0
end

--Saves/Creates the game item
function saveGameItem()

	--Update name
	--Debug.printTable("", m_item)
	if m_item.name then
		local newValue = EditBox_GetText(gHandles["edName"])
		m_item.name = newValue
	end

	--Update description
	local newValue = EditBox_GetText(gHandles["edDescription"])
	m_item.description = newValue

	-- limit the item count to stack size
	--log("-------- limiting the stack size")
	if m_item.behaviorParams and m_item.behaviorParams.loot and m_item.behaviorParams.loot.limitItems then
		--log("--- single item loot spawner ")
		m_item.behaviorParams.loot.limitItems.limit = limitStackSize(m_item.behaviorParams.loot.limitItems.limit, m_item.behaviorParams.loot.limitItems.UNID)
		
	end
	
	-- Hack to limit the drop rate of non-stackable items on Loot Generating Objects
	if( m_item.singles ) then
		for i, loot in ipairs(m_item.singles) do
			local stackSize = 100000
			if loot.dropRate and loot.maxDropCount then
				loot.dropRate, stackSize = limitStackSize(loot.dropRate, loot.UNID)
				local maxDrop = stackSize * 20
				if maxDrop < loot.maxDropCount then
					loot.maxDropCount = maxDrop
					KEP_MessageBox("Loot quanities have been adjusted automatically to conform to stack size limitations.")

				end
			end
		end
	end

	if( m_item.loot and m_item.loot.singles ) then
		for i, loot in ipairs(m_item.loot.singles) do
			if loot.quantity then
				loot.quantity = limitStackSize(loot.quantity, loot.UNID)
			end
		end
	end

	-- metrics
	if m_prefabType == "seed" then
		local input = {
			reportName = "OnInteractSeed",
			reportParams = {
				username = m_playerName, 
				interactType = "create"
			}
		}
		Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {input = input, callType = "METRIC_GENERIC"})
	end

	--Saving waypoint properly, safe to remove pending
	if m_item.pendingWaypoint then
		m_item.pendingWaypoint = nil
	end
	-- If editing item
	if m_menuMode == MENU_MODE.EDIT_ITEM then
		-- Quest Giver should know about updated quest item
		if m_item.itemType == "quest" then
			Events.sendEvent("FRAMEWORK_QUEST_GAME_ITEM_EDITED", {UNID = m_itemUNID})
		end

		-- World Settings modified
		if m_item.itemType == "mainSettings" then
			
			local privacy = ALLOW_ALL

			if m_item.privacy == "Private" then 
				privacy = ALLOW_PRIVATE
			elseif m_item.privacy == "Owner Only" then
				privacy = ALLOW_OWNER_ONLY
			end

			local web_address = GameGlobals.WEB_SITE_PREFIX..SET_ACCESS_SUFFIX..privacy.."&cover=0&gameId="..tostring(KEP_GetGameId()).."&zoneInstanceId="..tostring(m_item.zoneID).."&zoneType="..tostring(m_item.zoneType)
			makeWebCall( web_address, WF.SET_ZONE_ACCESS)

			m_item.privacy = nil

			Events.sendEvent("FRAMEWORK_UPDATE_SETTINGS", {UNID = m_itemUNID, settings = m_item})
			
			MenuCloseThis()
		-- Environment Settings modified
		elseif m_item.itemType == "environmentSettings" then
			Events.sendEvent("FRAMEWORK_UPDATE_ENVIRONMENT", {UNID = m_itemUNID, environment = m_item})
			
			MenuCloseThis()
		-- Editing Game Item
		elseif m_item.itemType == "welcomeSettings" then
			Events.sendEvent("FRAMEWORK_UPDATE_WELCOME", {UNID = m_itemUNID, welcome = m_item})
			
			MenuCloseThis()
		-- Editing Game Item
		elseif m_item.itemType == "overviewSettings" then
			Events.sendEvent("FRAMEWORK_UPDATE_OVERVIEW", {UNID = m_itemUNID, overview = m_item})
			
			MenuCloseThis()
		elseif m_item.itemType == "Zone" then
			createNewChildZone()
		else
			m_editingGameItem = true
			Events.sendEvent("EDIT_GAME_ITEM", {UNID = m_itemUNID, properties = m_item})
		end

	-- New Game Item
	elseif m_menuMode == MENU_MODE.NEW_ITEM then
		Events.sendEvent("CREATE_NEW_GAME_ITEM", {properties = m_item})
		Events.sendEvent("DOWNLOAD_GAME_ITEM_ICON", {UNID = m_itemUNID, broadcast = true})
		
		MenuCloseThis()
	end
end

--Updates list control positions
function updateListPosition(scrollMod)
	local x, y
	local itemControl	

	if m_listHeight < LIST_HEIGHT then
		scrollMod = 0
	elseif (m_listAnchor + scrollMod) > LIST_ANCHOR_POINT.y then
		scrollMod = LIST_ANCHOR_POINT.y - m_listAnchor
	elseif (m_listAnchor + scrollMod) + m_listHeight < LIST_ANCHOR_POINT.y + LIST_HEIGHT then
		scrollMod = (LIST_ANCHOR_POINT.y + LIST_HEIGHT) - m_listHeight - m_listAnchor
	end

	if scrollMod ~= 0 then
		m_listAnchor = m_listAnchor + scrollMod

		for i=1,#m_categoryList do
			local baseCategoryControl = nil
			local baseCategoryLocation = {}

			for j=1,#m_categoryList[i] do			
				local controlName = m_categoryList[i][j]
				itemControl = gHandles[controlName]
				y = Control_GetLocationY( itemControl ) + scrollMod
				Control_SetLocationY( itemControl, y )
				Control_SetVisible( itemControl, checkEndzone(y))

				if m_propertyLists[i] and #m_propertyLists[i] > 0 then
					if string.find(controlName, "imgListItemCategoryArrowClosed") then
						Control_SetVisible(gHandles[controlName], false)
					elseif string.find(controlName, "imgListItemCategoryArrowOpen") then
						Control_SetVisible( gHandles[controlName], checkEndzone(y))
					end					
				else
					if string.find(controlName, "imgListItemCategoryArrowClosed") then
						Control_SetVisible(gHandles[controlName], checkEndzone(y))
					elseif string.find(controlName, "imgListItemCategoryArrowOpen") then
						Control_SetVisible( gHandles[controlName], false)
					end					
				end
			end

			if m_propertyLists[i] then
				for j=1,#m_propertyLists[i] do
					local basePropertyControl = nil
					local baseCategoryLocation = {}
					for _,controlName in pairs(m_propertyLists[i][j]) do
						itemControl = gHandles[controlName]
						y = Control_GetLocationY( itemControl ) + scrollMod
						Control_SetLocationY( itemControl, y )
						Control_SetVisible(gHandles[controlName], checkEndzone(y))
					end
				end				
			end
		end

		updateScrollBar()
	end
end

--Sets up the item value pairs for each attribute
function initiateItemValues()
	local gameItemList = {}
	local glidList

	for _,prefabProps in pairs(PROPERTIES_PREFABS[m_prefabType]) do
		for _,property in pairs(prefabProps.properties) do

			--Set up item property values (Some of these will be nil depending)
			local attribute = property.attribute
			m_itemValues[attribute] = {}

			--General values
			m_itemValues[attribute].value = m_item[attribute]
			m_itemValues[attribute].text = property.text
			m_itemValues[attribute].propertyType = property.propType
			m_itemValues[attribute].parent = property.parent
			m_itemValues[attribute].addByKey = property.addByKey			
			m_itemValues[attribute].thumbnail = property.thumbnail
			m_itemValues[attribute].width = property.width
			m_itemValues[attribute].number = property.number
			m_itemValues[attribute].suffix = property.suffix
			m_itemValues[attribute].permissionRequired = property.permissionRequired
			m_itemValues[attribute].name = property.name

			m_itemValues[attribute].default = property.default
			m_itemValues[attribute].defaultSelect = property.defaultSelect

			--Add default item if there is none
			if m_item[attribute] == nil and property.default then
				m_item[attribute] = property.default
				m_itemValues[attribute].value = property.default
			end

			--Values for Edit Boxes
			m_itemValues[attribute].min = property.min
			m_itemValues[attribute].max = property.max

			--Values for Button Selects
			m_itemValues[attribute].selectType = property.selectType
			m_itemValues[attribute].typeFilter = property.typeFilter
			m_itemValues[attribute].behaviorFilter = property.behaviorFilter

			--Values for Drop Down
			m_itemValues[attribute].displayValues = property.displayValues
			m_itemValues[attribute].trueValues = property.trueValues

			--Values for Dynamic Lists
			m_itemValues[attribute].maxCount = property.maxCount
			m_itemValues[attribute].defaultDataStuct = property.defaultDataStuct
			
			if m_itemValues[attribute].propertyType == PROPERTY_TYPES.BUTTON_SELECT or m_itemValues[attribute].propertyType == PROPERTY_TYPES.BUTTON_SELECT_TEST then -- Look up names for GLIDS and Game Items for the button select type
				local selectType = m_itemValues[attribute].selectType
				local itemLookup = tostring(m_item[attribute])
				if selectType == "DIALOG" then
					--Do nothing
				elseif selectType == "CHARACTER_MESH" then

					itemLookup = m_item.GLID
					if m_item.meshType == 1  then
						m_itemValues[attribute].value = itemLookup
						m_item.lastGLID = tonumber(itemLookup)
						m_itemValues["meshGLID"].value = tonumber(itemLookup)
					end

					m_glidNames[tostring(itemLookup)] = ""
					if glidList then
						glidList = glidList .. "," .. itemLookup
					else
						glidList = itemLookup
					end
				elseif selectType == "UNID" and itemLookup ~= "nil" then
					if m_gameItems[itemLookup] then
						m_gameItemNames[itemLookup] = m_gameItems[itemLookup].name
					else
						m_gameItemNames[itemLookup] = "None"
					end
				elseif itemLookup ~= "nil" then-- else GLID
					m_glidNames[itemLookup] = ""
					if glidList then
						glidList = glidList .. "," .. itemLookup
					else
						glidList = itemLookup
					end
				end
			elseif m_itemValues[attribute].propertyType == PROPERTY_TYPES.PLAYLIST_SELECT then -- Look up names for GLIDS and Game Items for the button select type
				local playlistString = tostring(m_item[attribute])
				playlistID = string.match(playlistString, "<asset_id>(%d-)</asset_id>") or string.match(playlistString, "<asset_group_id>(%d-)</asset_group_id>") or "0"
				playlistID = tonumber(playlistID)
						m_playlistNames[playlistID] = ""
						m_playlistNames[playlistID] = string.gsub((string.match(playlistString, "<name>(.-)</name>") or ""), "&amp;", "&")
						updatePropertyValueNames()
			elseif m_itemValues[attribute].propertyType == PROPERTY_TYPES.IMAGE_SELECT then
				local imageString = tostring(m_item[attribute])
				local lastSlashIndex = string.find(imageString, "/[^/]*$")
				local periodIndex = string.find(imageString,  '.', 1, true)
				if lastSlashIndex and periodIndex then
					m_imageName =  string.sub(imageString, lastSlashIndex + 1, periodIndex - 1) or "none"
				else
					m_imageName = "none"
				end
				updatePropertyValueNames()
			elseif m_itemValues[attribute].propertyType == PROPERTY_TYPES.WORLD_SELECT then
				updatePropertyValueNames()
			elseif m_itemValues[attribute].propertyType == PROPERTY_TYPES.ACCESSORIES then -- Look up names for the GLIDS for the accessories list
				if type(m_item[attribute]) == "table" then
					for i=1,#m_item[attribute] do
						local itemLookup = tostring(m_item[attribute][i])
						if itemLookup ~= "nil" then
							m_glidNames[itemLookup] = ""

							if glidList then
								glidList = glidList .. "," .. itemLookup
							else
								glidList = itemLookup
							end
						end
					end
				end
			elseif m_itemValues[attribute].propertyType == PROPERTY_TYPES.LOOT then -- Look up names for the UNIDS for the loot list
				if type(m_item[attribute]) == "table" then
					for i=1,#m_item[attribute] do
						local itemLookup = tostring(m_item[attribute][i].UNID)
						if m_gameItems[itemLookup] then
							m_gameItemNames[itemLookup] = m_gameItems[itemLookup].name
						else
							m_gameItemNames[itemLookup] = "None"
						end
					end
				end
			elseif m_itemValues[attribute].propertyType == PROPERTY_TYPES.QUESTS or m_itemValues[attribute].propertyType == PROPERTY_TYPES.SINGLE_DYNAMIC_ITEMS then -- Look up names for the UNIDS for the quest list
				if type(m_item[attribute]) == "table" then
					for i=1,#m_item[attribute] do
						local itemLookup = tostring(m_item[attribute][i])
						if m_gameItems[itemLookup] then
							m_gameItemNames[itemLookup] = m_gameItems[itemLookup].name
						else
							m_gameItemNames[itemLookup] = "None"
						end
					end
				end
			elseif m_itemValues[attribute].propertyType == PROPERTY_TYPES.PARTICLE_EFFECTS then -- Look up names for the GLIDs 
				if type(m_item[attribute]) == "table" then
					for i=1,#m_item[attribute] do
						local itemLookup = tostring(m_item[attribute][i].effect)
						if itemLookup ~= "nil" then
							m_glidNames[itemLookup] = ""

							if glidList then
								glidList = glidList .. "," .. itemLookup
							else
								glidList = itemLookup
							end
						end
					end
				end
			elseif m_itemValues[attribute].propertyType == PROPERTY_TYPES.STAGES then -- Look up names for the UNIDS for the loot list
				if type(m_item[attribute]) == "table" then
					for i=1,#m_item[attribute] do
						local itemLookup = tostring(m_item[attribute][i].GLID)
						if itemLookup ~= "nil" then
							m_glidNames[itemLookup] = ""

							if glidList then
								glidList = glidList .. "," .. itemLookup
							else
								glidList = itemLookup
							end
						end
					end
				end
			elseif m_itemValues[attribute].propertyType == PROPERTY_TYPES.NODES then -- Look up names for the UNIDS for the loot list
				if type(m_item[attribute]) == "table" then
					for i=1,#m_item[attribute] do
						local itemLookup = tostring(m_item[attribute][i].maleAnim)
						if itemLookup ~= "nil" then
							m_glidNames[itemLookup] = ""

							if glidList then
								glidList = glidList .. "," .. itemLookup
							else
								glidList = itemLookup
							end
						end
						
						itemLookup = tostring(m_item[attribute][i].femaleAnim)
						if itemLookup ~= "nil" then
							m_glidNames[itemLookup] = ""

							if glidList then
								glidList = glidList .. "," .. itemLookup
							else
								glidList = itemLookup
							end
						end
					end
				end
			elseif m_itemValues[attribute].propertyType == PROPERTY_TYPES.TRADES then -- Look up names for the UNIDS for the trades list
				if type(m_item[attribute]) == "table" then
					for i=1,#m_item[attribute] do
						if m_item.clothing then
							local itemLookup = tostring(m_item[attribute][i].output)
							if itemLookup ~= "nil" then
								m_glidNames[itemLookup] = ""
								if glidList then
									glidList = glidList .. "," .. itemLookup
								else
									glidList = itemLookup
								end
							end
						else
							local inputLookup = tostring(m_item[attribute][i].input)
							if m_gameItems[inputLookup] then
								m_gameItemNames[inputLookup] = m_gameItems[inputLookup].name
							else
								m_gameItemNames[inputLookup] = "None"
							end

							local outputLookup = tostring(m_item[attribute][i].output)
							if m_gameItems[outputLookup] then
								m_gameItemNames[outputLookup] = m_gameItems[outputLookup].name
							else
								m_gameItemNames[outputLookup] = "None"
							end
						end
					end
				end
			end
		end
	end

	updatePropertyValueNames()
	if glidList then
		local web_address = GameGlobals.WEB_SITE_PREFIX .. "kgp/instantBuy.aspx?operation=info&items=" .. glidList
		makeWebCall(web_address, WF.GET_SCRIPT_BUNDLE)
	end
end

--Update attribute names
function updatePropertyValueNames()
	for attribute,itemValues in pairs(m_itemValues) do
		if ( itemValues.propertyType == PROPERTY_TYPES.BUTTON_SELECT or itemValues.propertyType == PROPERTY_TYPES.PLAYLIST_SELECT or itemValues.propertyType == PROPERTY_TYPES.IMAGE_SELECT or itemValues.propertyType == PROPERTY_TYPES.WORLD_SELECT or itemValues.propertyType == PROPERTY_TYPES.BUTTON_SELECT_TEST or itemValues.propertyType == PROPERTY_TYPES.ACCESSORIES or itemValues.propertyType == PROPERTY_TYPES.PARTICLE_EFFECTS or itemValues.propertyType == PROPERTY_TYPES.STAGES or propertyType == PROPERTY_TYPES.NODES or itemValues.propertyType == PROPERTY_TYPES.DIALOG_PAGES or itemValues.propertyType == PROPERTY_TYPES.LOOT or itemValues.propertyType == PROPERTY_TYPES.QUESTS or itemValues.propertyType == PROPERTY_TYPES.SINGLE_DYNAMIC_ITEMS) and itemValues.control and itemValues.value then
			if itemValues.selectType == "DIALOG" then
				Static_SetText(gHandles[itemValues.control], tostring(itemValues.value))
			elseif itemValues.selectType == "UNID" then
				Static_SetText(gHandles[itemValues.control], m_gameItemNames[tostring(itemValues.value)])
			elseif itemValues.selectType == "PLAYLIST" then
					if tostring(m_playlistNames[itemValues.value]) ~= "nil" then
						Static_SetText(gHandles[itemValues.control], m_playlistNames[itemValues.value])
					else
					Static_SetText(gHandles[itemValues.control], m_playlistNames[playlistID])
					end
			elseif itemValues.selectType == "IMAGE" then
				Static_SetText(gHandles[itemValues.control], m_imageName or "none")
			elseif itemValues.selectType == "WORLD" then
				local startPos, endPos, worldName = string.find(itemValues.value,  "<name>(.-)</name>")
				if worldName == nil then
					worldName = ""
				end
				Static_SetText(gHandles[itemValues.control], worldName)
			elseif itemValues.selectType == "ZONE" then

				 itemValues.value = string.gsub(itemValues.value, "\'", "\"")	
				local zoneTable = Events.decode(itemValues.value)
				if zoneTable then
					Static_SetText(gHandles[itemValues.control], zoneTable["name"])
				end
			elseif itemValues.selectType == "CHARACTER_MESH" then
				if m_itemValues["meshType"].value == 1 then
					Static_SetText(gHandles[itemValues.control], m_glidNames[tostring(itemValues.value)])
				elseif m_itemValues["meshType"].value == 2 then
					Static_SetText(gHandles[itemValues.control], "Female Avatar")
				else
					Static_SetText(gHandles[itemValues.control], "Male Avatar")
				end
			else
				Static_SetText(gHandles[itemValues.control], m_glidNames[tostring(itemValues.value)])
			end
		end
	end
end

--Update game item with item values
function updateGameItem(finalize)
	for attribute,value in pairs(m_itemValues) do

		if m_itemValues[attribute].parent and m_item[m_itemValues[attribute].parent] == nil then
			m_item[m_itemValues[attribute].parent] = {}
		end

		local newValue = m_itemValues[attribute].value
		if type(m_item[attribute]) == "number" or m_itemValues[attribute].number then
			newValue = tonumber(newValue)
		elseif type(m_item[attribute]) == "string" and type(newValue) == "string" and finalize and attribute ~= "meshGLID"  and attribute ~= "zoneDestination" then --Paper doll meshGLIDS need single quotes!
			--REPLACE MYSQL ESCAPE CHARACTER
			newValue = string.gsub(newValue, "'", "")
			newValue = string.gsub(newValue, '"', "")
		elseif attribute == "dialogPages" then
			if m_itemValues[attribute].value then
				for index, dialog in pairs(m_itemValues[attribute].value) do
					for page, text in pairs(dialog) do
						text = string.gsub(text, "'", "")
						text = string.gsub(text, '"', "")
						m_itemValues[attribute].value[index][page] = text
					end
				end
			end
			newValue = m_itemValues[attribute].value
		elseif attribute == "markers" then
			newValue = m_item[attribute]
		end

		if newValue ~= nil then
			if m_itemValues[attribute].parent and finalize then
				if m_itemValues[attribute].addByKey then
					m_item[m_itemValues[attribute].parent][attribute] = newValue
				else
					table.insert(m_item[m_itemValues[attribute].parent], newValue)
				end

				--Eliminate the value from the flat table
				m_item[attribute] = nil
			else
				m_item[attribute] = newValue
			end
		end
	end

	if finalize then

		m_item = formatGameItem(m_item)
	end	
end	

--Creates collapsable categories
function createListCategory(itemProperties)
	for i=1,#itemProperties do
		local baseControl = nil
		local baseLocation = {}
		m_categoryList[i] = {}

		--Duplicate category prefab controls
		for j=1,#LIST_ITEM_CATEGORY_CONTROLS do
			local control = Dialog_GetControl( gDialogHandle, LIST_ITEM_CATEGORY_CONTROLS[j] )
			local newControl = copyControl(control)
			local newControlName = LIST_ITEM_CATEGORY_CONTROLS[j] .. i
			Control_SetName( newControl, newControlName )		

			table.insert(m_categoryList[i], newControlName)			
			if baseControl == nil then
				--Set base control point location for this category
				baseControl = newControlName
				baseLocation.x = Control_GetLocationX( gHandles[newControlName] )
				baseLocation.y = Control_GetLocationY( gHandles[newControlName] )

				Control_SetLocation( gHandles[newControlName], LIST_ANCHOR_POINT.x, m_insertLocation )
				m_insertLocation = m_insertLocation + CATEGORY_ROW_HEIGHT				
			else
				--Move control to new position based on diff from base control location
				local xDiff = baseLocation.x - Control_GetLocationX( gHandles[newControlName] )
				local yDiff = baseLocation.y - Control_GetLocationY( gHandles[newControlName] )
				local newX = Control_GetLocationX( gHandles[baseControl] ) - xDiff
				local newY = Control_GetLocationY( gHandles[baseControl] ) - yDiff

				if string.find(newControlName, "stcListItemCategory") then
					--Set the name of the category
					Static_SetText(newControl, itemProperties[i].text)
				elseif string.find(newControlName, "btnListItemCategory") then
					--Category bar click
					_G[newControlName .. "_OnButtonClicked"] = function(btnHandle)
						local index = tonumber(string.match(Control_GetName(btnHandle), "(%d+)"))

						if m_propertyLists[index] and #m_propertyLists[index] > 0 then
							updateGameItem(false)
							for i=1,#m_propertyLists[index] do
								for j=1,#m_propertyLists[index][i] do
									Dialog_RemoveControl( gDialogHandle, m_propertyLists[index][i][j] )
								end								
							end

							m_propertyLists[index] = {}
							updateList()
							checkPosition()						
						else
							createPropertiesList(index)
							updateList()
							checkPosition()
						end	
					end
				end

				Control_SetLocation( gHandles[newControlName],newX, newY )
			end
		end
	end	
end

--Create properties list for given category
function createPropertiesList(categoryIndex)
	if m_categoryList[categoryIndex] then
		local propertySelection = PROPERTIES_PREFABS[m_prefabType][categoryIndex]
		local categoryLocation = Control_GetLocationY( gHandles[m_categoryList[categoryIndex][1]] )
		m_insertLocation = categoryLocation + CATEGORY_ROW_HEIGHT
		m_propertyLists[categoryIndex] = {}	

		--Create properties for given index
		for i=1,#propertySelection.properties do
			local propertyType = propertySelection.properties[i].propType
			local attribute = propertySelection.properties[i].attribute

			if userCanViewProperty(propertySelection.properties[i].permissionRequired) then
				if (propertyType == PROPERTY_TYPES.ACCESSORIES or propertyType == PROPERTY_TYPES.LOOT or propertyType == PROPERTY_TYPES.PARTICLE_EFFECTS or propertyType == PROPERTY_TYPES.STAGES or propertyType == PROPERTY_TYPES.NODES or propertyType == PROPERTY_TYPES.DIALOG_PAGES or propertyType == PROPERTY_TYPES.TRADES or propertyType == PROPERTY_TYPES.QUESTS or propertyType == PROPERTY_TYPES.SINGLE_DYNAMIC_ITEMS or propertyType == PROPERTY_TYPES.MARKER) then

					--If attribute doesn't exist, make new table
					if m_item[attribute] == nil or type(m_item[attribute]) ~= "table" then
						m_item[attribute] = {}
					end

					--If property type is dynamic accessories list
					createDynamicList(propertySelection.properties[i], categoryIndex, m_item[attribute], attribute, propertyType)
					-- addProperty(propertySelection.properties[i].attribute, categoryIndex)			
				else
					addProperty(propertySelection.properties[i].attribute, categoryIndex)			
				end
			end
		end
	end
end

-- Create the property row elements
function addProperty(attribute, categoryIndex)
	--Duplicate property controls based on item property type
	local listItem = {}

	local properties = m_itemValues[attribute]

	local propertyType = properties.propertyType
	local propertyName = properties.text	
	local width = properties.width
	local height = properties.height

	for j=1, #PROPERTY_TYPE_CONTROLS[propertyType] do
		local control = Dialog_GetControl( gDialogHandle, PROPERTY_TYPE_CONTROLS[propertyType][j])
		local newControl = copyControl(control)
		local newControlName = Control_GetName( newControl )

		--Set property Title
		if string.find(newControlName, "stcListItemName") or string.find(newControlName, "stcListItemTitle") then
			Static_SetText(newControl, propertyName)
		end
		--Set the value of the edit box
		if ( (propertyType == PROPERTY_TYPES.BUTTON_SELECT or propertyType == PROPERTY_TYPES.BUTTON_SELECT_TEST  or  propertyType == PROPERTY_TYPES.PARTICLE_ITEM or propertyType == PROPERTY_TYPES.PLAYLIST_SELECT or propertyType == PROPERTY_TYPES.IMAGE_SELECT or propertyType == PROPERTY_TYPES.WORLD_SELECT) and ( string.find(newControlName, "btnSelect") or string.find(newControlName, "btnJumpToItem") ) ) or ( propertyType == PROPERTY_TYPES.COLOR_SELECT and string.find(newControlName, "btnEditColor") ) then
			_G[newControlName .. "_OnButtonClicked"] = function(btnHandle)
				if MenuIsClosed("Framework_ItemSelectEditor.xml") then
					local selectType = properties.selectType
					local typeFilter = properties.typeFilter
					local behaviorFilter = properties.behaviorFilter

					if selectType then
						if selectType == "DIALOG" then
							menuName = "Framework_DialogEditor.xml"
							MenuOpen(menuName)
							local event = KEP_EventCreate( "EDIT_PARAM_EVENT" )
							KEP_EventEncodeString(event, attribute)
							KEP_EventEncodeString(event, m_item.name)
							local value = m_item[attribute] or 0
							KEP_EventEncodeString(event, value)
							KEP_EventQueue( event )
						elseif selectType == "PLAYLIST" then
							menuName = "PlaylistSelect.xml"
							MenuOpen(menuName)
							local event = KEP_EventCreate( "SelectMediaEvent" )
							local playlistType = PLAYLIST_TYPE_MEDIA
							local editPlaylist = 1
							KEP_EventEncodeString(event, playlistType)
							KEP_EventEncodeNumber(event, m_item.GLID)
							KEP_EventEncodeNumber(event, editPlaylist)
							KEP_EventEncodeString(event, attribute)
							KEP_EventQueue( event )
						elseif selectType == "IMAGE" then
							menuName = "Framework_ImageSelect.xml"
							local event = KEP_EventCreate( "EDIT_PARAM_EVENT" )
							KEP_EventEncodeString(event, attribute)
							KEP_EventQueue( event )
							MenuOpen(menuName)
						elseif selectType == "WORLD" then
							menuName = "Framework_WorldSelectEditor.xml"
							MenuOpen(menuName)
						elseif selectType == "ZONE" then
							menuName = "Framework_LinkedZoneSelectEditor.xml"
							local event = KEP_EventCreate( "EDIT_PARAM_EVENT" )
							local value = m_item[attribute] or ""
							KEP_EventEncodeString(event, value)
							KEP_EventQueue( event )
							MenuOpen(menuName)
						elseif selectType == "COLOR" then
							menuName = "Framework_ColorPicker.xml"
							MenuOpenModal(menuName)
							local event = KEP_EventCreate( "COLOR_START_EVENT" )
							KEP_EventEncodeNumber(event, m_item[attribute.."Red"])
							KEP_EventEncodeNumber(event, m_item[attribute.."Green"])
							KEP_EventEncodeNumber(event, m_item[attribute.."Blue"])
							KEP_EventEncodeString(event, attribute)
							KEP_EventQueue( event )
						elseif selectType == "CHARACTER_MESH" then
							if m_itemValues["meshType"].value == 1 then
								menuName = "Framework_ItemSelectEditor.xml"
								MenuOpen(menuName)										
								local event = KEP_EventCreate( "EDIT_PARAM_EVENT" )
								KEP_EventEncodeString(event,  "OBJECT")
								KEP_EventEncodeString(event, typeFilter or "all")
								KEP_EventEncodeString(event, behaviorFilter or "all")
								KEP_EventEncodeString(event, attribute)
								KEP_EventEncodeString(event, propertyName)
								KEP_EventEncodeString(event, m_item.name)
								KEP_EventEncodeString(event, properties.defaultSelect or "None")
								local value = m_item.GLID or 0
								KEP_EventEncodeString(event, value)
								KEP_EventQueue( event )
							else
								newNPC( m_itemValues["meshType"].value, m_itemValues["meshGLID"].value)
							end
						else
							menuName = "Framework_ItemSelectEditor.xml"
							MenuOpen(menuName)										
							local event = KEP_EventCreate( "EDIT_PARAM_EVENT" )
							KEP_EventEncodeString(event, selectType)
							KEP_EventEncodeString(event, typeFilter or "all")
							KEP_EventEncodeString(event, behaviorFilter or "all")
							KEP_EventEncodeString(event, attribute)
							KEP_EventEncodeString(event, propertyName)
							KEP_EventEncodeString(event, m_item.name)
							KEP_EventEncodeString(event, properties.defaultSelect or "None")
							local value = m_item[attribute] or 0
							KEP_EventEncodeString(event, value)
							KEP_EventQueue( event )
						end

						-- Shift out of the way of the main editor
						local pos = MenuGetLocationThis()
						local mainMenu = MenuGetSizeThis()
						local editMenu = MenuGetSize(menuName)
						local screenWidth = Dialog_GetScreenWidth(gDialogHandle)

						if editMenu then
							if (pos.x + mainMenu.width + editMenu.width) <= screenWidth then
								pos.x = pos.x + mainMenu.width
							else
								pos.x = pos.x - editMenu.width
							end
							MenuSetLocation(menuName, pos.x, pos.y + EDITOR_LOCATION_OFFSET_Y)	
						end										
					end
				end
			end
		-- Preview the selected animation with the test button
		elseif propertyType == PROPERTY_TYPES.BUTTON_SELECT_TEST and string.find(newControlName, "btnTestAnim") then
			_G[newControlName .. "_OnButtonClicked"] = function(btnHandle)
				KEP_SetCurrentAnimationByGLID(m_itemValues[attribute].value)
			end
		-- Set color swatch
		elseif propertyType == PROPERTY_TYPES.COLOR_SELECT and string.find(newControlName, "imgColorBG") then
			local color = { a=255, r=m_item[attribute.."Red"], g=m_item[attribute.."Green"], b=m_item[attribute.."Blue"]}
			BlendColor_SetColor(Element_GetTextureColor(Static_GetDisplayElement(gHandles[newControlName])), 0, color)

			m_itemValues[attribute].control = newControlName
		--Set the name of the button select text
		elseif (propertyType == PROPERTY_TYPES.BUTTON_SELECT or propertyType == PROPERTY_TYPES.BUTTON_SELECT_TEST or propertyType == PROPERTY_TYPES.PLAYLIST_SELECT or propertyType ==PROPERTY_TYPES.IMAGE_SELECT or propertyType ==PROPERTY_TYPES.WORLD_SELECT) and string.find(newControlName, "stcName") then
			if m_item[attribute] then
				local selectType = properties.selectType
				local itemName
				if selectType == "DIALOG" then
					itemName = tostring(m_item[attribute])
				elseif selectType == "UNID" then
					itemName = m_gameItemNames[tostring(m_item[attribute])]
				elseif selectType == "CHARACTER_MESH" then
					if m_itemValues["meshType"].value == 1 then
						itemName = m_gameItemNames[tostring(m_item[attribute])]
					end
				else
					itemName = m_glidNames[tostring(m_item[attribute])]
				end	
				Static_SetText(newControl, itemName or "none")					
			end

			m_itemValues[attribute].control = newControlName	

		--Adds an item to the dynamic list
		elseif propertyType == PROPERTY_TYPES.ADD_ITEM and string.find(newControlName, "btnAddItem") then
			if type(m_item[attribute]) == "table" then

				Control_SetEnabled(newControl, (properties.maxCount == nil or #m_item[attribute] < properties.maxCount) )
				
				_G[newControlName .. "_OnButtonClicked"] = function(btnHandle)
					--Add new item to list
					table.insert(m_item[attribute], 0)

					refreshDynamicList(categoryIndex, attribute)
				end
			end
		--Adds an item to the dynamic list
		elseif propertyType == PROPERTY_TYPES.ADD_ITEM and string.find(newControlName, "stcAddItem") then
			Static_SetText(newControl, propertyName)

		--Set the values of the combo box
		elseif propertyType == PROPERTY_TYPES.DROP_DOWN and string.find(newControlName, "cmbDropDown") then
			local displayValues = properties.displayValues or m_dropDownParams[attribute]
			local trueItemValues = properties.trueValues or {}
			local valuePreset = false

			for k=1,#displayValues do
				ComboBox_AddItem( newControl, displayValues[k], k )

				if type(m_item[attribute]) == "number" then
					if trueItemValues[k] then
						if almostEquals(trueItemValues[k], m_item[attribute]) then -- Leave a little wiggle room for floating point comparisons
							ComboBox_SetSelectedByText(newControl, displayValues[k])
							valuePreset = true
						end
					end
				else	
					if trueItemValues[k] and trueItemValues[k] == m_item[attribute] then
						ComboBox_SetSelectedByText(newControl, displayValues[k])
						valuePreset = true
					end
				end
			end

			if m_item[attribute] and not valuePreset then
				ComboBox_SetSelectedByText(newControl, tostring(m_item[attribute]))
			end

			if width then
				local height = Control_GetHeight( newControl )
				Control_SetSize( newControl, width, height )
			end
			
			m_itemValues[attribute].control = newControlName
			m_itemValues[attribute].trueValues = trueItemValues

			--Handle input from combo box selection
			_G[newControlName .. "_OnComboBoxSelectionChanged"] = function(comboBoxHandle, selectedIndex, selectedText)						
				if m_itemValues[attribute] then
					local trueValue
					local currentValue = m_itemValues[attribute].value
					if #m_itemValues[attribute].trueValues > 0 then

						if m_itemValues[attribute].trueValues[selectedIndex + 1] then
							trueValue = m_itemValues[attribute].trueValues[selectedIndex + 1]
						end
					end

					if trueValue ~= nil then
						m_itemValues[attribute].value = trueValue
					else
						m_itemValues[attribute].value = selectedText
					end		

					if attribute == "meshType" and trueValue ~= currentValue then
						m_npcDropdown = gHandles[newControlName]
						Events.sendEvent("FRAMEWORK_VALIDATE_ATTRIBUTE", {UNID  = m_itemUNID, attribute = attribute, value = currentValue})
					end			
				end
			end

		--Set the values of the edit box
		elseif propertyType == PROPERTY_TYPES.TEXT and string.find(newControlName, "edText") then
			if m_item[attribute] then
				EditBox_SetText(newControl, m_item[attribute], false)
			end

			if width then
				local height = Control_GetHeight( newControl )
				Control_SetSize( newControl, width, height )
			end

			m_itemValues[attribute].control = newControlName
			
			_G[newControlName .. "_OnFocusIn"] = function(buttonHandle)
				if m_itemValues[attribute] then
					Dialog_SetModal(gDialogHandle, true)
				end
			end
			
			_G[newControlName .. "_OnEditBoxChange"] = function(editBoxHandle, editBoxText)
				if m_itemValues[attribute] then

					local newValue = editBoxText
					local resetValue = false

					if properties.number then
						if tonumber(newValue) ~= nil then
							newValue = tonumber(newValue)

							local min = properties.min

							-- if min and newValue < min then -- Input value is less than the minimum
							-- 	newValue = min
							-- 	resetValue = true
							-- end

							local max = properties.max

							if max and newValue > max then -- Input value is greater than the maximum 
								newValue = max
								resetValue = true
							end						
						elseif newValue ~= "" and newValue ~= "-" then
							--An invalid non-number character has been inputted
							newValue = m_itemValues[attribute].value
							resetValue = true				
						end

						if resetValue then
							EditBox_SetText(newControl, newValue, false)
						end	
					elseif properties.max and string.len(newValue) > properties.max then
						newValue = string.sub(newValue, 0, properties.max)
						EditBox_SetText(newControl, newValue, false)
					end	

					m_itemValues[attribute].value = newValue
				end
			end

			--Double check the value when clicking away.
			_G[newControlName .. "_OnFocusOut"] = function(editBoxHandle)
				if m_itemValues[attribute] then
					local editBoxText = EditBox_GetText(newControl)
					local newValue = editBoxText

					if properties.number then
						if newValue == "" or newValue == "-" then newValue = 0 end

						newValue = tonumber(newValue)

						local min = properties.min

						if min and newValue < min then -- Input value is less than the minimum
							newValue = min
						end

						local max = properties.max

						if max and newValue > max then -- Input value is greater than the maximum 
							newValue = max
						end

						EditBox_SetText(newControl, newValue, false)

						m_itemValues[attribute].value = newValue	
					elseif properties.max and string.len(newValue) > properties.max then
						newValue = string.sub(newValue, 0, properties.max)
						EditBox_SetText(newControl, newValue, false)		 										
					end					
					Dialog_SetModal(gDialogHandle, false)
				end
			end

		--Set the suffix of the property
		elseif propertyType == PROPERTY_TYPES.TEXT and string.find(newControlName, "stcSuffix") then
			local suffix = properties.suffix

			if suffix then
				local x = Control_GetLocationX(newControl) + width + 5 --5 pixel buffer
				local y = Control_GetLocationY(newControl)
				Control_SetLocation(newControl, x, y)
				Static_SetText(newControl, suffix)
			else
				Control_SetSize(newControl, 0, 0)
			end

		--Set the values of the check box
		elseif propertyType == PROPERTY_TYPES.CHECK_BOX and string.find(newControlName, "boxCheck") then
			if m_item[attribute] ~= nil then
				CheckBox_SetChecked( newControl, ((m_item[attribute] == "true") or (m_item[attribute] == true)) )
			else
				CheckBox_SetChecked(newControl, false)
			end

			m_itemValues[attribute].control = newControlName

			_G[newControlName .. "_OnCheckBoxChanged"] = function(checkboxHandle, checked)						
				if m_itemValues[attribute] then
					m_itemValues[attribute].value = checked == 1
					if attribute == "leaderboard" and checked == 1 then
						KEP_MessageBox("This feature requires your world to have Land Claim Flags and World Coins.")
					end
				end
			end
		--Alternate property background color
		elseif string.find(newControlName, "imgListItemBG") then
			if height then
				local width = Control_GetWidth( newControl )
				Control_SetSize( newControl, width, height )
			end

			local backgroundColor =  Element_GetTextureColor(Image_GetDisplayElement(gHandles[newControlName]))
			if m_propertyRows%2 == 0 then
				BlendColor_SetColor(backgroundColor, 0, PROPERTY_BACKGROUND_COLORS[1])
			else
				BlendColor_SetColor(backgroundColor, 0, PROPERTY_BACKGROUND_COLORS[2])
			end
			
			m_propertyRows = m_propertyRows + 1
		elseif string.find(newControlName, "imgListItemBase") then
			if height then
				local width = Control_GetWidth( newControl )
				Control_SetSize( newControl, width, height )
			end
		elseif propertyType == PROPERTY_TYPES.EDIT then
			_G[newControlName .. "_OnButtonClicked"] = function(btnHandle)
				if properties.selectType == "questGoal" then
					local menuName = "Framework_QuestGoalEditor.xml"
					MenuOpen(menuName)
					local pos = MenuGetLocationThis()
					local mainMenu = MenuGetSizeThis()
					local editMenu = MenuGetSize(menuName)
					local screenWidth = Dialog_GetScreenWidth(gDialogHandle)

					if editMenu then
						if (pos.x + mainMenu.width + editMenu.width) <= screenWidth then
							pos.x = pos.x + mainMenu.width
						else
							pos.x = pos.x - editMenu.width
						end
						MenuSetLocation(menuName, pos.x, pos.y + QUEST_EDITOR_OFFSET_Y)	
					end	


					m_item.name = EditBox_GetText(gHandles["edName"])
					local questGoalEvent = KEP_EventCreate("FRAMEWORK_EDIT_POPOUT_OPEN")
					KEP_EventEncodeString(questGoalEvent, Events.encode(m_item))
					
					KEP_EventQueue(questGoalEvent)
				end
			end
		end

		table.insert(listItem, newControlName)
	end

	movePropertiesToList(listItem, categoryIndex)
end



--Create dynamic property list category
function createDynamicList(properties, categoryIndex, list, attribute, propertyType)
	local categoryLocation = Control_GetLocationY( gHandles[m_categoryList[categoryIndex][1]] )
	m_insertLocation = categoryLocation + CATEGORY_ROW_HEIGHT
	m_propertyLists[categoryIndex] = {}

	--Duplicate controls for dynamic list
	for i=1,#list do		
		if propertyType == PROPERTY_TYPES.LOOT or propertyType == PROPERTY_TYPES.TRADES or propertyType == PROPERTY_TYPES.PARTICLE_EFFECTS or propertyType == PROPERTY_TYPES.DIALOG_PAGES or propertyType == PROPERTY_TYPES.STAGES or propertyType == PROPERTY_TYPES.NODES or propertyType == PROPERTY_TYPES.MARKER then
			if type(list[i]) ~= "table" then
						--Duplicate the data structure for this property
				list[i] = deepCopy(properties.defaultDataStuct)
			end
				if properties.propertyComponents.attribute then -- Account for a non-table with one entry (TO DO: find out why it was formatted this way)
				local propertyAttribute = properties.propertyComponents.attribute

				if list[i][propertyAttribute] == nil then
					list[i][propertyAttribute] = properties.defaultDataStuct[propertyAttribute]
				end

				local newAttributeKey = propertyAttribute .. tostring(i)
				local propertyComponent = properties.propertyComponents
				local propertyValue = list[i][propertyAttribute]
				
				m_itemValues[newAttributeKey] = {value = propertyValue, baseAttribute = propertyAttribute, propertyType = propertyComponent.propType}

				addDynamicProperty(propertyComponent, propertyComponent.propType, categoryIndex, list, i, newAttributeKey, attribute, true)		
			else -- Iterate through a table of propertyComponents
				for j=1,#properties.propertyComponents do
					local propertyAttribute = properties.propertyComponents[j].attribute

					if list[i][propertyAttribute] == nil then
						list[i][propertyAttribute] = properties.defaultDataStuct[propertyAttribute]
					end

					local newAttributeKey = propertyAttribute .. tostring(i)
					local propertyComponent = properties.propertyComponents[j]
					local propertyValue = list[i][propertyAttribute]
					
					m_itemValues[newAttributeKey] = {value = propertyValue, baseAttribute = propertyAttribute, propertyType = propertyComponent.propType}

					addDynamicProperty(propertyComponent, propertyComponent.propType, categoryIndex, list, i, newAttributeKey, attribute, true)		
				end

			end

			m_propertyRows = m_propertyRows + 1
		else
			local newAttributeKey = attribute .. tostring(i)
			m_itemValues[newAttributeKey] = {value = list[i], propertyType = propertyType}

			addDynamicProperty(properties, propertyType, categoryIndex, list, i, newAttributeKey, attribute)
			m_propertyRows = m_propertyRows + 1	
		end			
	end
end

--Add a dynamic property element to the list
function addDynamicProperty(properties, propertyType, categoryIndex, list, propertyIndex, newAttributeKey, parentAttribute, sameLineColor)
	local listItem = {}
	for j=1, #PROPERTY_TYPE_CONTROLS[propertyType] do
		local control = Dialog_GetControl( gDialogHandle, PROPERTY_TYPE_CONTROLS[propertyType][j])
		local newControl = copyControl(control)
		local newControlName = Control_GetName( newControl )

		--Set property Title
		if string.find(newControlName, "stcListItemName") then
			Static_SetText(newControl, properties.text)
			Control_SetPassthrough(newControl, 1)
		end


		--Set the value of the edit box
		if string.find(newControlName, "btnEdit") or string.find(newControlName, "btnSelect") or string.find(newControlName, "btnJumpToItem") then

			_G[newControlName .. "_OnButtonClicked"] = function(btnHandle)				
				local menuName = "Framework_ItemSelectEditor.xml"
				local event = KEP_EventCreate( "EDIT_PARAM_EVENT" )
				local selectType = properties.selectType
				if propertyType == PROPERTY_TYPES.DIALOG_ITEM then
					---- Open the dialog editor instead of the item select editor
					menuName = "Framework_DialogEditor.xml"
					---- Pass along core attributes
					KEP_EventEncodeString(event, parentAttribute) -- Source Table (e.g. "dialogPages")
					KEP_EventEncodeString(event, m_item.name) -- Name ("Edit the dialog for __")
					local value = list[propertyIndex].dialogPage or "Hello."
					KEP_EventEncodeString(event, value) -- Text value ("Greetings.")
					
					KEP_EventEncodeString(event, propertyIndex) -- Index in source table (e.g. "2" as in dialogPages[2])
					KEP_EventEncodeString(event, m_itemValues[newAttributeKey].baseAttribute) -- Relevant attribute within source table ("dialogPage")
				elseif propertyType == PROPERTY_TYPES.EDIT_DYNAMIC then
					if properties.selectType == "markerProperties" then
						 menuName = "Framework_LinkedZoneSelectEditor.xml"

						m_item.name = EditBox_GetText(gHandles["edName"])
						event = KEP_EventCreate("FRAMEWORK_EDIT_POPOUT_OPEN")
						KEP_EventEncodeString(event, Events.encode(m_item))
						KEP_EventEncodeString(event, tostring(propertyIndex))
						
						--KEP_EventQueue(questGoalEvent)
					end
                else
					KEP_EventEncodeString(event, properties.selectType)
					KEP_EventEncodeString(event, properties.typeFilter or "all") -- Filter
					KEP_EventEncodeString(event, properties.behaviorFilter or "all") -- 2nd Filter
					KEP_EventEncodeString(event, parentAttribute)
					KEP_EventEncodeString(event, trim1(properties.text))
					KEP_EventEncodeString(event, m_item.name)
					KEP_EventEncodeString(event, properties.defaultSelect or "None")
					if type(list[propertyIndex]) == "table" then
						KEP_EventEncodeString(event, list[propertyIndex][properties.attribute])
					else
						KEP_EventEncodeString(event, list[propertyIndex])
					end
					KEP_EventEncodeString(event, propertyIndex)
					KEP_EventEncodeString(event, m_itemValues[newAttributeKey].baseAttribute)
				end

				MenuOpen(menuName)

				KEP_EventQueue( event )

				-- Shift out of the way of the main editor
				local pos = MenuGetLocationThis()
				local mainMenu = MenuGetSizeThis()
				local editMenu = MenuGetSize(menuName)
				local screenWidth = Dialog_GetScreenWidth(gDialogHandle)

				if (pos.x + mainMenu.width + editMenu.width) <= screenWidth then
					pos.x = pos.x + mainMenu.width
				else
					pos.x = pos.x - editMenu.width
				end
				if propertyType == PROPERTY_TYPES.EDIT_DYNAMIC then
					MenuSetLocation(menuName, pos.x, pos.y + QUEST_EDITOR_OFFSET_Y)	
				else
					MenuSetLocation(menuName, pos.x, pos.y + EDITOR_LOCATION_OFFSET_Y)	
				end				
			end
		--Set the name of the item
		elseif string.find(newControlName, "stcName") then
			itemName = m_glidNames[tostring(m_itemValues[newAttributeKey].value)] or m_gameItemNames[tostring(m_itemValues[newAttributeKey].value)] or ""
			Static_SetText(newControl, itemName)

			m_itemValues[newAttributeKey].control = newControlName
		--Remove item from list
		elseif string.find(newControlName, "btnRemove") then
			_G[newControlName .. "_OnButtonClicked"] = function(btnHandle)
				m_itemValues[newAttributeKey] = nil				
				table.remove(list, propertyIndex)
				
				refreshDynamicList(categoryIndex, parentAttribute)
			end

		--Set the values of the combo box
		elseif propertyType == PROPERTY_TYPES.DROP_DOWN and string.find(newControlName, "cmbDropDown") then
			local displayValues = properties.displayValues
			local trueItemValues = properties.trueValues or {}
			local valuePreset = false

			for k=1,#displayValues do
				ComboBox_AddItem( newControl, displayValues[k], k )

				if trueItemValues[k] and trueItemValues[k] == m_itemValues[newAttributeKey].value then
					ComboBox_SetSelectedByText(newControl, displayValues[k])
					valuePreset = true
				end
			end

			if m_itemValues[newAttributeKey] and m_itemValues[newAttributeKey].value and not valuePreset then
				ComboBox_SetSelectedByText(newControl, tostring(m_itemValues[newAttributeKey].value))
			end

			if width then
				local height = Control_GetHeight( newControl )
				Control_SetSize( newControl, width, height )
			end
			
			m_itemValues[newAttributeKey].control = newControlName
			m_itemValues[newAttributeKey].trueValues = trueItemValues

			--Handle input from combo box selection
			_G[newControlName .. "_OnComboBoxSelectionChanged"] = function(comboBoxHandle, selectedIndex, selectedText)						
				if m_itemValues[newAttributeKey] then
					local trueValue
					if #m_itemValues[newAttributeKey].trueValues > 0 then

						if m_itemValues[newAttributeKey].trueValues[selectedIndex + 1] then
							trueValue = m_itemValues[newAttributeKey].trueValues[selectedIndex + 1]
						end
					end

					if trueValue ~= nil then
						m_itemValues[newAttributeKey].value = trueValue
					else
						m_itemValues[newAttributeKey].value = selectedText
					end

					if m_itemValues[newAttributeKey].baseAttribute then 
						m_item[parentAttribute][propertyIndex][m_itemValues[newAttributeKey].baseAttribute] = m_itemValues[newAttributeKey].value
					else
						m_item[parentAttribute][propertyIndex] = m_itemValues[newAttributeKey].value
					end
				end
			end

		--Set the values of the edit box
		elseif string.find(newControlName, "edText") then
			if m_itemValues[newAttributeKey] then
				EditBox_SetText(newControl, m_itemValues[newAttributeKey].value, false)
			end

			local width = properties.width


			if width then
				local height = Control_GetHeight( newControl )
				Control_SetSize( newControl, width, height )
			end

			_G[newControlName .. "_OnFocusIn"] = function(buttonHandle)
				if m_itemValues[newAttributeKey] then
					Dialog_SetModal(gDialogHandle, true)
				end
			end
			
			_G[newControlName .. "_OnEditBoxChange"] = function(editBoxHandle, editBoxText)
				if m_itemValues[newAttributeKey] then

					local newValue = editBoxText
					local resetValue = false

					if properties.number then
						if tonumber(newValue) ~= nil then
							newValue = tonumber(newValue)

							-- local min = properties.min

							-- if min and newValue < min then -- Input value is less than the minimum
							-- 	newValue = min
							-- 	resetValue = true
							-- end

							local max = properties.max

							if max and newValue > max then -- Input value is greater than the maximum 
								newValue = max
								resetValue = true
							end						
						elseif newValue ~= "" and newValue ~= "-" then
							--An invalid non-number character has been inputted
							newValue = m_itemValues[newAttributeKey].value
							resetValue = true				
						end

						if resetValue then
							EditBox_SetText(newControl, newValue, false)
						end
					elseif properties.max and string.len(newValue) > properties.max then
						newValue = string.sub(newValue, 0, properties.max)
						EditBox_SetText(newControl, newValue, false)
					end					

					m_itemValues[newAttributeKey].value = newValue
					
					if m_itemValues[newAttributeKey].baseAttribute then 
						m_item[parentAttribute][propertyIndex][m_itemValues[newAttributeKey].baseAttribute] = newValue
					else
						m_item[parentAttribute][propertyIndex] = newValue
					end
				end
			end

			--Double check the value when clicking away.
			_G[newControlName .. "_OnFocusOut"] = function(editBoxHandle)
				if m_itemValues[newAttributeKey] then
					local editBoxText = EditBox_GetText(newControl)
					local newValue = editBoxText

					if properties.number then
						if newValue == "" or newValue == "-" then newValue = 0 end

						newValue = tonumber(newValue)

						local min = properties.min

						if min and newValue < min then -- Input value is less than the minimum
							newValue = min
						end

						local max = properties.max

						if max and newValue > max then -- Input value is greater than the maximum 
							newValue = max
						end

						EditBox_SetText(newControl, newValue, false)

						m_itemValues[newAttributeKey].value = newValue
						
						m_item[parentAttribute][propertyIndex][m_itemValues[newAttributeKey].baseAttribute] = newValue					 										
						
						Dialog_SetModal(gDialogHandle, false)
					elseif properties.max and string.len(newValue) > properties.max then
						newValue = string.sub(newValue, 0, properties.max)
						EditBox_SetText(newControl, newValue, false)
					end					
				end
			end

		elseif string.find(newControlName, "stcSuffix") then
			local suffix = properties.suffix
			local width = properties.width

			if suffix then
				local x = Control_GetLocationX(newControl) + width + 5 --5 pixel buffer
				local y = Control_GetLocationY(newControl)
				Control_SetLocation(newControl, x, y)
				Static_SetText(newControl, suffix)
			else
				Control_SetSize(newControl, 0, 0)
			end

		--Property background color
		elseif string.find(newControlName, "imgListItemBG") then
			if properties.height then
				local width = Control_GetWidth( newControl )
				Control_SetSize( newControl, width, properties.height )
			end			

			local backgroundColor =  Element_GetTextureColor(Image_GetDisplayElement(gHandles[newControlName]))
			if m_propertyRows%2 == 0 then
				BlendColor_SetColor(backgroundColor, 0, PROPERTY_BACKGROUND_COLORS[1])
			else
				BlendColor_SetColor(backgroundColor, 0, PROPERTY_BACKGROUND_COLORS[2])
			end
			
		elseif string.find(newControlName, "imgListItemBase") then
			if properties.height then
				local width = Control_GetWidth( newControl )
				Control_SetSize( newControl, width, properties.height )
			end
		end

		-- Display default labels in select boxes for dynamic lists
		if string.find(newControlName, "stcNameBtnSelect") then
			if propertyType == PROPERTY_TYPES.DIALOG_ITEM and list[propertyIndex] and list[propertyIndex].dialogPage then
				Static_SetText(newControl, list[propertyIndex].dialogPage)
			end
		end

		table.insert(listItem, newControlName)
	end
	--Move accesories list to main list
	movePropertiesToList(listItem, categoryIndex)
end

function trim1(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end

--Refresh accessory list and remove accessory
function refreshDynamicList(categoryIndex, attribute)
	if type(m_item[attribute]) == "table" then
		--Clears out old list
		for i=1,#m_propertyLists[categoryIndex] do
			for j=1,#m_propertyLists[categoryIndex][i] do
				Dialog_RemoveControl( gDialogHandle, m_propertyLists[categoryIndex][i][j] )
			end
		end

		createPropertiesList(categoryIndex)
		updateList()
		checkPosition()
	end
end

--Move a set of properties to main list
function movePropertiesToList(listItem, categoryIndex)
	local baseControl = nil
	local baseLocation = {}
	local propertyControls = {}

	moveControlSet(listItem)

	table.insert(m_propertyLists[categoryIndex], listItem)
end

--Relocate control set based on list location
function moveControlSet(controlSet)
	local baseControl = nil
	local baseLocation = {}
	for _,controlName in pairs(controlSet) do
		if baseControl == nil then
			baseControl = controlName
			baseLocation.x = Control_GetLocationX( gHandles[controlName] )
			baseLocation.y = Control_GetLocationY( gHandles[controlName] )
			Control_SetLocation( gHandles[controlName], LIST_ANCHOR_POINT.x, m_insertLocation )
			
			m_insertLocation = m_insertLocation + Control_GetHeight( gHandles[controlName] )

		else
			local xDiff = baseLocation.x - Control_GetLocationX( gHandles[controlName] )
			local yDiff = baseLocation.y - Control_GetLocationY( gHandles[controlName] )
			local newX = Control_GetLocationX( gHandles[baseControl] ) - xDiff
			local newY = Control_GetLocationY( gHandles[baseControl] ) - yDiff

			Control_SetLocation( gHandles[controlName],newX, newY )
		end
	end
end

--Update list's control location
function updateList()
	local indexInsert = MENU_INSERT_INDEX
	m_insertLocation = m_listAnchor
	m_listHeight = 0
	for i=1,#m_categoryList do
		local baseCategoryControl = nil
		local baseCategoryLocation = {}
		local indexOffset = 0

		for j=1,#m_categoryList[i] do
			local controlName = m_categoryList[i][j]
			if baseCategoryControl == nil then
				baseCategoryControl = controlName
				baseCategoryLocation.x = Control_GetLocationX( gHandles[controlName] )
				baseCategoryLocation.y = Control_GetLocationY( gHandles[controlName] )

				Control_SetLocation( gHandles[controlName], LIST_ANCHOR_POINT.x, m_insertLocation )
				m_insertLocation = m_insertLocation + CATEGORY_ROW_HEIGHT
				m_listHeight = m_listHeight + CATEGORY_ROW_HEIGHT
			else
				local xDiff = baseCategoryLocation.x - Control_GetLocationX( gHandles[controlName] )
				local yDiff = baseCategoryLocation.y - Control_GetLocationY( gHandles[controlName] )
				local newX = Control_GetLocationX( gHandles[baseCategoryControl] ) - xDiff
				local newY = Control_GetLocationY( gHandles[baseCategoryControl] ) - yDiff
				Control_SetLocation( gHandles[controlName],newX, newY )
			end

			Dialog_MoveControlToIndex( gDialogHandle, gHandles[controlName], indexInsert )
			indexOffset = indexOffset + 1
		end

		indexInsert = indexInsert + indexOffset

		if m_propertyLists[i] then
			for j=1,#m_propertyLists[i] do
				local basePropertyControl = nil
				local baseCategoryLocation = {}
				indexOffset = 0

				for _,controlName in pairs(m_propertyLists[i][j]) do					
					if basePropertyControl == nil then
						basePropertyControl = controlName
						baseCategoryLocation.x = Control_GetLocationX( gHandles[controlName] )
						baseCategoryLocation.y = Control_GetLocationY( gHandles[controlName] )
						Control_SetLocation( gHandles[controlName], LIST_ANCHOR_POINT.x, m_insertLocation )
						m_insertLocation = m_insertLocation + Control_GetHeight( gHandles[controlName] )
						m_listHeight = m_listHeight + Control_GetHeight( gHandles[controlName] )
					else
						local xDiff = baseCategoryLocation.x - Control_GetLocationX( gHandles[controlName] )
						local yDiff = baseCategoryLocation.y - Control_GetLocationY( gHandles[controlName] )
						local newX = Control_GetLocationX( gHandles[basePropertyControl] ) - xDiff
						local newY = Control_GetLocationY( gHandles[basePropertyControl] ) - yDiff
						Control_SetLocation( gHandles[controlName],newX, newY )
					end
					Dialog_MoveControlToIndex( gDialogHandle, gHandles[controlName], indexInsert )
					indexOffset = indexOffset + 1
				end

				indexInsert = indexInsert + indexOffset
			end				
		end
	end
	updateScrollBar()
	checkVisibility()
end

--Update the size and location of the scroll bar
function updateScrollBar()
	local listPer = math.min(LIST_HEIGHT/m_listHeight, 1)
	local barHeight = listPer * SCROLL_BAR_MAX

	local buttonWidth = m_barScrolling and SCROLL_BAR_WIDTH + 200 or SCROLL_BAR_WIDTH
	local buttonHeight = m_barScrolling and barHeight + 50 or barHeight	

	Control_SetSize(gHandles["imgScrollBar"], SCROLL_BAR_WIDTH, barHeight)
	Control_SetSize(gHandles["btnScrollBar"], buttonWidth, buttonHeight)

	local listDiff = LIST_ANCHOR_POINT.y - m_listAnchor
	local barDiff = SCROLL_BAR_MAX - barHeight

	local barPosX = Control_GetLocationX(gHandles["imgScrollBar"])
	local barPosY = SCROLL_BAR_TOP + math.ceil((barHeight + barDiff) * (listDiff/m_listHeight))

	local buttonBarPosX = m_barScrolling and barPosX - 100 or barPosX
	local buttonBarPosY = m_barScrolling and barPosY - 25 or barPosY	

	Control_SetLocation( gHandles["imgScrollBar"], barPosX, barPosY )
	Control_SetLocation( gHandles["btnScrollBar"], buttonBarPosX, buttonBarPosY )
end

--Copy and paste controls
function copyControl(menuControl)
	if menuControl then
		Dialog_CopyControl( gDialogHandle, menuControl )
		Dialog_PasteControl( gDialogHandle )

		local numControls = Dialog_GetNumControls( gDialogHandle )

		local newControl = Dialog_GetControlByIndex( gDialogHandle, numControls - 1 )
		local controlName = Control_GetName( newControl )

		gHandles[controlName] = newControl	

		return newControl
	end
end

--Check the position of the list
function checkPosition()
	if m_listHeight < LIST_HEIGHT then
		jumpToTop()
	elseif m_listAnchor < LIST_ANCHOR_POINT.y - (m_listHeight - LIST_HEIGHT) then
		jumpToBottom()		
	end
end

--Check whether a property should be hidden
function checkVisibility()
	for i=1,#m_categoryList do
		local baseCategoryControl = nil
		local baseCategoryLocation = {}

		for j=1,#m_categoryList[i] do			
			local controlName = m_categoryList[i][j]
			if baseCategoryControl == nil then
				baseCategoryControl = controlName
				baseCategoryLocation.y = Control_GetLocationY( gHandles[controlName] )
			end

			Control_SetVisible( gHandles[controlName], checkEndzone(baseCategoryLocation.y) )

			if m_propertyLists[i] and #m_propertyLists[i] > 0 then
				if string.find(controlName, "imgListItemCategoryArrowClosed") then
					Control_SetVisible(gHandles[controlName], false)
				elseif string.find(controlName, "imgListItemCategoryArrowOpen") then
					Control_SetVisible( gHandles[controlName], checkEndzone(baseCategoryLocation.y))
				end					
			else
				if string.find(controlName, "imgListItemCategoryArrowClosed") then
					Control_SetVisible(gHandles[controlName], checkEndzone(baseCategoryLocation.y))
				elseif string.find(controlName, "imgListItemCategoryArrowOpen") then
					Control_SetVisible( gHandles[controlName], false)
				end					
			end
		end

		if m_propertyLists[i] then
			for j=1,#m_propertyLists[i] do
				local basePropertyControl = nil
				local baseCategoryLocation = {}
				for _,controlName in pairs(m_propertyLists[i][j]) do					
					if basePropertyControl == nil then
						basePropertyControl = controlName
						baseCategoryLocation.y = Control_GetLocationY( gHandles[controlName] )
					end
					Control_SetVisible( gHandles[controlName], checkEndzone(baseCategoryLocation.y) )			
				end
			end				
		end
	end	
end

--Check the location of a control against the top and bottom endzone masks
function checkEndzone(controlLocation, controlHeight)
	if controlLocation < LIST_ENDZONE_TOP then
		return false
	elseif controlLocation > LIST_ENDZONE_BOTTOM then
		return false
	else
		return true
	end
end

--Jump list to the top
function jumpToTop()
	m_listAnchor = LIST_ANCHOR_POINT.y
	updateList()
end

--Jump list to the bottom
function jumpToBottom()
	m_listAnchor = LIST_ANCHOR_POINT.y - (m_listHeight - LIST_HEIGHT)
	updateList()
end

--Jump list to a given point
function jumpToPoint(point)
	m_listAnchor = LIST_ANCHOR_POINT.y - (point - LIST_HEIGHT)
	updateList()
end

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end

function userCanViewProperty(permissionRequired)
	if permissionRequired == "Owner" and not KEP_IsOwner() then return false end
	if permissionRequired == "Moderator" and not KEP_IsOwnerOrModerator() then return false end

	return true
end

function almostEquals(value1, value2)
	local wiggleRoom = 0.00001
	if value1 - wiggleRoom < value2 and value1 + wiggleRoom > value2 then
		return true
	else
		return false
	end
end

function limitStackSize(attribute, UNID)


		if not m_gameItems[tostring(UNID)] then return 0 end
		local lootLimit = attribute
		local stackSize = m_gameItems[tostring(UNID)].stackSize or 100
		local itemType = m_gameItems[tostring(UNID)].itemType

		--armor, weapons and tools do not have a stack size, but are not stackable
		if itemType == "armor" or itemType == "weapon" or itemType == "tool" then
			stackSize = 1
		end
		
		if lootLimit > stackSize then
			KEP_MessageBox("Loot quanities have been adjusted automatically to conform to stack size limitations.")
		end
		return math.min(stackSize, lootLimit), stackSize
end

-- g_ccId = 0
function newNPC(meshType, npcData)

		MenuOpen("CharacterCreation3.xml")
		local ev = KEP_EventCreate("FRAMEWORK_INIT_CHAR_CREATOR")
		KEP_EventEncodeString(ev, "npc")
		local gender = 0
		if meshType == 2 then
			gender = 1
		end
		KEP_EventEncodeNumber(ev, gender)

		if npcData then
			KEP_EventEncodeString(ev, npcData)
		end
		KEP_EventQueue(ev)
	m_menuLocation = MenuGetLocationThis()
	MenuSetLocationThis(-1000,-1000)
	m_menuHidden = true
	MenuSetMinimized("HudBuild.xml", true)
	MenuClose("UnifiedInventory")
end

function updateNPCData(dispatcher, fromNetid, event, eventid, filter, objectid)
	MenuSetLocationThis(m_menuLocation.x,m_menuLocation.y)
	MenuSetMinimized("HudBuild.xml", false)
	m_menuHidden = nil
	MenuOpen("UnifiedNavigation")

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "gaming")
	KEP_EventQueue(ev)

	local gotoEvent = KEP_EventCreate("FRAMEWORK_NAV_GOTO_ITEM")
	KEP_EventEncodeString(gotoEvent, tostring(m_itemUNID))
	KEP_EventQueue(gotoEvent)

	if KEP_EventMoreToDecode(event) == 1 then
		m_itemValues["meshGLID"].value = KEP_EventDecodeString(event)
		m_item.meshGLID = m_itemValues["meshGLID"].value
	end

	MenuBringToFrontThis()
end

function onEditPopoutSaved(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_item = Events.decode(KEP_EventDecodeString(event))
end
---------------------------
-------Creating Child zones
---------------------------

function createNewChildZone()
	local url = GameGlobals.WEB_SITE_PREFIX .. CREATE_WORLD_SUFFIX .. "worldname=" .. m_item.name .. "&worlddesc=" .. m_item.description .. "&templateid=28&parentid="..m_item.parentID
	makeWebCall( url, WF.CREATE_WORLD)
end

function parseResponseCreateWorld(reponseXml)
	local code = nil  -- return code
	local desc = nil  -- result description
	local url  = nil  -- world url

	if (reponseXml == nil) then
		LogError("parseResponseCreateWorld null response.")
		code = -99
		desc = "Unexpected response"
		url  = ""
		return code, desc, url
	end

	code = string.match(reponseXml, "<ReturnCode>(.-)</ReturnCode>")
	desc = string.match(reponseXml, "<ReturnDescription>(.-)</ReturnDescription>")
	url  = string.match(reponseXml, "<STPURL>(.-)</STPURL>") 

	if (code == nil) then
		LogError("parseResponseCreateWorld: unexpected code in response ["..tostring(code).."] -- desc["..tostring(desc).."] -- url["..tostring(url).."]")
		code = -99
		desc = "Unexpected response"
		url  = ""
		return code, desc, url
	else
		code = tonumber(code)
	end

	return code, desc, url
end


function requestCreateWorldSuccess(code, desc, url)
	local childEvent = KEP_EventCreate("FRAMEWORK_NEW_CHILD_ZONE")
	KEP_EventEncodeString(childEvent, Events.encode(m_item))
	KEP_EventQueue(childEvent)
	MenuCloseThis()
end

function requestCreateWorldFailed(code, desc, url)
	if (code == -1) then
		desc = "The World could not be created.  Please try again later"
	elseif (code == -2) then
		desc = "You must first validate your email to create a World."
	elseif (code == -4) then
		desc = "You must be VIP to create more Worlds."
	elseif (code == -5) then
		local originalName = nil
		local recommendedName = nil
		local s, e = 0
		s, e, originalName    = string.find(desc, "<originalName>(.-)</originalName>")

		if (originalName ~= nil) then
			desc = "The World name ".. originalName.." already exists and must be changed. We recommend " .. recommendedName
		end

	elseif (code < -10) then
		desc = "The World could not be created.  Please try again later"
	end

	KEP_MessageBox(desc)
end