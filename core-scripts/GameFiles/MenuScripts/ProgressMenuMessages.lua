--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\CommonFunctions.lua")

ProgressMenuMessages = {}
	
	-- Sets the size of each message table so that accessing it in real-time saves us a half-cyle or two.
ProgressMenuMessages.init = function(self)
	for i, v in pairs(self) do
		if type(v) == "table" and type(v.init) == "function" then
			v:init()
		end
	end
end

ProgressMenuMessages.getRandomTip = function(self, messageType)
	local messageTable = self[messageType]
	local randomIndex = MathRandomInt(messageTable.n)
	
	return messageTable[randomIndex]
end

ProgressMenuMessages.getRandomPhrase = function(self)
	return self:getRandomTip("MESSAGE_TYPE_PHRASE")
end

-- ProgressMenuMessages.getRandomLootTip = function(self)
-- 	return self:getRandomTip("MESSAGE_TYPE_LOOT_TIP")
-- end

-- ProgressMenuMessages.getRandomGamingTip = function(self)
-- 	return self:getRandomTip("MESSAGE_TYPE_GAMING_TIP")
-- end


ProgressMenuMessages["MESSAGE_TYPE_PHRASE"] = {
    "Chasing the white rabbit"
    ,"Are we there yet?"
    ,"Hang on a sec, we almost got it"
    ,"Wait for it... wait for it..."
    ,"Shoo'ing away griefers"
    ,"Elves returning from break"
    ,"It's around here somewhere..."
    ,"Calculating the odds"
    ,"Busting a move!"
    ,"Deciphering intentions"
    ,"Contemplating the meaning of life"
    ,"Builders hard at work"
    ,"Painting the sky blue"
    ,"Calibrating the virtual coffee machine"
    ,"Grabbing a byte to eat"
    ,"Adding fun factor"
    ,"Setting VCR clock"
    ,"Positioning sun"
    ,"Eliminating pollution"
    ,"Tidying up"
    ,"Generating excitement"
    ,"Dropping the bass"
    ,"Dropping it like it's hot"
    ,"Solving world hunger"
    ,"Decreasing gas prices"
    ,"Adding belly button lint"
    ,"Warming up chairs"
    ,"Scaring off gremlins"
    ,"Filling water balloons"
    ,"Filling H2O canisters"
    ,"Letting dogs out"
    ,"Flexing legs"
    ,"Wiping off feet"
    ,"Injecting positive influences"
    ,"Teaching artificial intelligence"
    ,"Hydrating fauna"
    ,"Planning itinerary"
    ,"Generating happiness"
    ,"Charging teleporters"
    ,"Applying avatar attractiveness"
    ,"Lunch?"
    ,"Randomizing minglers"
    ,"Raising the roof"
    ,"Perfecting climate"
    ,"Constructing buildings"
    ,"Dressing avatar"
    ,"Building structures"
    ,"Transferring emotions"
    ,"Embedding cool factor"
    ,"Time for coffee?"
    ,"Deflecting haters"
    ,"It's nice to see you today!"
    ,"I'm glad you're here!"
    ,"You look nice today"
    --USER CREATED:
    ,"Locating hamsters"
    ,"Greasing wheels"
    ,"Filling foodbowls"
    ,"Spinning up the warp drive"
    ,"Waiter... there's a pixel in my soup"
    ,"Don't touch that cable!"
    ,"What made that noise?"
    ,"Tomorrow's lottery numbers are... "
    ,"Blasting a Blast"
    ,"Drumroll please"
    ,"Searching for marbles"
    ,"Tightening loose screws"
    ,"Brushing your hair"
    ,"Engaging warp drive"
    ,"Paying your parking tickets"
    ,"Putting the square peg in a round hole"
    ,"Pixelating the pixels"
    ,"Watering the plants"
    ,"Did someone order a pizza?"
    ,"Hey that tickles!"
    ,"Look Ma, no hands!"
    ,"Let's create something together"
    ,"Now, where did I put that..."
    ,"This is way more fun than algebra!"
    ,"And the monkey flips the switch"
    ,"Navigational holographics onboard"
    ,"Rocket boosters ignited"
    ,"Very funny... now beam up my clothes"
    ,"Curing a rainy day"
    ,"Lassoing unicorns"
    ,"No matter where you go, there you are"
    ,"You're Cute!"
    ,"Can you dig it?"
    ,"Scanning for schadenfreude"
    ,"Time to be creative"
    ,"Rave on!"
    ,"If you build it, they will come"
    ,"Why are you reading this?"
    ,"Polishing the servers"
    ,"Shooing away rain clouds"
    ,"Now that didn't hurt"
    ,"Nice hairdo"
    ,"It's just you, me, and the loading screen"
    ,"Can you lick your elbow?"
    ,"Tuning up air guitars"
    ,"Anti gravity zone; remain seated"
    ,"Waterproofing swimming pools"
    ,"Weighing pound cakes"
    ,"Watering sidewalks"
    ,"Planetary alignment complete"
    ,"Painting the lawn"
    ,"Animating animated objects"
    ,"Initializing fun"
    ,"Where is the elevator music?"
    ,"Loading humorous message...please wait"
    ,"Help, I'm being held hostage in a phrase-writing factory!"
    ,"You seem nice. Wanna be friends?"
    ,"Oh, its you again...."
    ,"So, I was thinking...."
    ,"Downloading a flea circus"
    ,"Watch for falling pixels"
    ,"Preparing snacks"
    ,"Deflecting solar flares"
    ,"Shuffling clouds"
    ,"Painting rainbows"
    ,"Setting the table"
    ,"Waxing stuff",
	
	["init"] = function(self)
		self.n = #self
	end
}

-- ProgressMenuMessages["MESSAGE_TYPE_LOOT_TIP"] = {
-- 	"Objects that glow green on mouseover can be clicked to interact",
-- 	"Open chests and other containers to find loot by clicking on them",
-- 	"Eat and drink by right-clicking on food items inside your Backpack",
-- 	"Use Weapons by selecting in toolbelt and clicking on targets",
-- 	"Wear Armor by dragging into the Armor slots in your Backpack",
-- 	"Right-click on your Pet to make it stay, wander, or follow",
-- 	"Opening chests can contain valuable gems",
-- 	"Top Worlds have bonus Rewards in chests",

-- 	["init"] = function(self)
-- 		self.n = #self
-- 	end
-- }

-- ProgressMenuMessages["MESSAGE_TYPE_GAMING_TIP"] = {
-- 	-- General Interaction
-- 	"Objects that glow green on mouseover can be clicked to interact",
-- 	"Hold down your Right-mouse button to steer your Avatar",
-- 	"Hold down your Left mouse button to orbit camera around yourself",
-- 	"Use the WASD keys on your keyboard to move your Avatar",
-- 	"Use the Spacebar to make your Avatar jump",
-- 	-- Player Toolbelt
-- 	"Your Toolbelt is used for Weapons, Tools, Food, and Placeables",
-- 	"Drag items to your Toolbelt to use them",
-- 	"Use the Hand Tool in the Toolbelt to move, rotate, and pickup objects",
-- 	"The numbers 1 thru 6 on your keyboard select items in your Toolbelt",
-- 	"Drag items to the Toolbelt then select and click into the world",
-- 	-- Player Backpack
-- 	"Your Backpack can hold 45 total slots of items",
-- 	"Drag items onto the ground to drop them or trade with other Players",
-- 	"The Crafting tab in your Backpack allows you to build custom objects",
-- 	"The Quests tab in your Backpack lets you manage your quests",
-- 	"Unique items cannot be stacked on each other in your Backpack",
-- 	"Non-unique items can be put in stacks of up to 100 items",
-- 	"World Coins can be stacked up to 100k coins per stack",
-- 	"If your backpack is full you can build chests or safes for extra storage",
-- 	-- Status Bars
-- 	"Eating fills your Energy Bar which in turn fills your Health Bar",
-- 	"If your Energy Bar reaches zero, you will start losing Health",
-- 	"If your Health Bar reaches zero, you will die",
-- 	"Mouse over your status bars to see the number values",
-- 	"Filling your Armor Bar gives you more Health!",
-- 	"The PvP icon shows areas where Players are allowed to fight each",
-- 	"The Player Building icon shows areas where building is allowed",
-- 	"The Player Destruction icon shows areas where destruction is allowed",
-- 	-- Loot System
-- 	"You can eat by right-clicking on food items inside your Backpack",
-- 	"Use Weapons by selecting them in your Toolbelt",
-- 	"Wear Armor by dragging them into the Armor slots in your Backpack",
-- 	"Use the best Weapons and Armor by comparing their ratings",
-- 	"P2P emote objects allow 2 players to engage together in an emote",
-- 	-- Placeables System
-- 	"You cannot build in an area claimed by another Player",
-- 	"You can move and rotate objects you've built in a world",
-- 	"Use the WASD keys on your keyboard to drive vehicles",
-- 	"You can play your very own music and videos by placing a TV",
-- 	"Build an indestructible Metal Safe to store and protect your valuables",
-- 	"Use building objects from your Toolbelt to place on the ground",
-- 	-- Character System
-- 	"Right-click on your Pet to make it stay, wander, or follow",
-- 	"You must feed your Pets at least once a week or it will run away",
-- 	"Vendors will only buyback items they are currently selling",
-- 	"Timed Vendors make you wait before they deliver the goods",
-- 	"Killing Monsters is a great way to earn fabulous Loot",
-- 	-- Quest System
-- 	"NPCs with exclamation marks over their heads are Quest Givers!",
-- 	"Quest Givers can tell a story and offer exciting Loot as a rewards",
-- 	"Your Quest Compass will point you to quest locations",
-- 	"When you complete a Quest, make sure to return to the Quest Giver",
-- 	"Some Quests are repeatable several times per day",
-- 	"Repeatable Quests are blue and one-time quests are yellow",
-- 	-- Harvestables System
-- 	"Trees, rocks, and metal require special tools to Harvest",
-- 	"Harvestables like trees, rocks, and metal provide crafting ingredients",
-- 	"Be a farmer by planting seeds and growing plants",
-- 	"Right-click to pick food from fully grown plant",
-- 	"Use a harvesting tool to completely cut down a plant",
-- 	"Plants can only grow to their next stage by feeding or watering them",
-- 	-- Crafting System
-- 	"Right-click on a Blueprint in your Backpack to learn it",
-- 	"Left-click on a Recipe in your Backpack to see its ingredients",
-- 	"Some Recipes require Crafting Helpers be in close proximity",
-- 	"Ingredients for Recipes are dropped by trees and rocks",
-- 	"Crafted objects can be placed into a world via the toolbelt",


-- 	["init"] = function(self)
-- 		self.n = #self
-- 	end
-- }










