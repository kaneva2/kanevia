--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------


-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_InventoryHelper.lua")
dofile("HUDHelper.lua")

local m_tLinkedZones = {}

local m_tPreviousDestination = nil
local m_tDestinationZone = {}
local m_tPlayerSpawners = {}

local m_tOverviewItem = nil --used for overview and markers
local m_iMarkerIndex = nil

local m_iZoneCount = 1
local m_iSelectedZone = 1

--selected zone instance and type
local m_iZoneInstanceID = ""
local m_iZoneType = ""


--parent zone and type
local m_iParentZoneInstanceID = ""
local m_iParentZoneType = ""

local m_icurrentZoneID = nil

local MAX_DROP_HEIGHT = 140
local DROP_ELEMENT_HEIGHT = 15

local MARKER_MENU_INCREASE = 180 --If editing a marker, increase menu size by this

local COLOR_MAX = 255
local POS_X_MAX = 945
local POS_Y_MAX = 540

local MARKER_CONTROLS = {
	"stcRed",
	"stcBlue",
	"stcGreen",
	"rSlider",
	"gSlider",
	"bSlider",
	"edRed",
	"edBlue",
	"edGreen",
	"stcTint",
	"imgMarker",
	"stcPosition",
	"stcPosX",
	"stcPosY",
	"edPosX",
	"edPosY"
}

-- When the menu is created
function onCreate()

	KEP_EventCreateAndQueue("FRAMEWORK_GET_PARENT_INFO")

	KEP_EventRegisterHandler( "editParamHandler", "EDIT_PARAM_EVENT", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler("parentInfoHandler", "FRAMEWORK_RETURN_PARENT_INFO", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("editMarkerHandler", "FRAMEWORK_EDIT_POPOUT_OPEN", KEP.HIGH_PRIO)

	Events.registerHandler("FRAMEWORK_RETURN_PLAYER_SPAWNERS", returnSpawnersHandler)
	m_icurrentZoneID = KEP_GetZoneInstanceId() 
end

------------------
---Event Handlers
------------------
function parentInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_iParentZoneType = KEP_EventDecodeNumber(event)
	m_iParentZoneInstanceID = KEP_EventDecodeNumber(event)

	requestWorlds()
end

function editParamHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local previousZone = KEP_EventDecodeString(tEvent)

	previousZone = string.gsub(previousZone, "\'", "\"")
	if previousZone then
		m_tPreviousDestination = Events.decode(previousZone)
	end
end

function BrowserPageReadyHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.LINKED_ZONES then
		local estr = KEP_EventDecodeString( event )
		ParseLocationData(estr)
	end
end

function returnSpawnersHandler(event)
	m_tPlayerSpawners = {}
	if event.playerSpawners then
		local spawnTable = Events.decode(event.playerSpawners)
		for PID, data in pairs(spawnTable) do
			data.PID = tonumber(PID)
			table.insert(m_tPlayerSpawners, data)
		end

	table.sort(m_tPlayerSpawners, customSort)		
	end

	updateDropDown()
end

------------------
---Web Functions
------------------
function  requestWorlds( )
	local url = GameGlobals.WEB_SITE_PREFIX..GET_LINKED_ZONES_SUFFIX.."&zoneInstanceID="..tostring(m_iParentZoneInstanceID).."&zoneType="..tostring(m_iParentZoneType)
	makeWebCall(url, WF.LINKED_ZONES)
end

function ParseLocationData(g_locationData)
    local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	s, strPos, result = string.find(g_locationData, "<ReturnCode>(.-)</ReturnCode>")

	m_tLinkedZones = {}
	m_iZoneCount = 0

	local lbxList = gHandles["lbxList"]

		-- Clear list
	ListBox_RemoveAllItems(lbxList)

	if result == "0" then
		for zoneData in string.gmatch(g_locationData, "<Table>(.-)</Table>") do
			m_tLinkedZones[m_iZoneCount] = {}
			s, strPos, zoneInstanceId = string.find(zoneData, "<zone_instance_id>(.-)</zone_instance_id>")
			m_tLinkedZones[m_iZoneCount].zoneID = zoneInstanceId


			s, strPos, name = string.find(zoneData, "<name>(.-)</name>")
			m_tLinkedZones[m_iZoneCount].name = name



			ListBox_AddItem(lbxList, name, m_iZoneCount)
			
			s, e, dbPath = string.find(zoneData, "<image_thumbnail>(.-)</image_thumbnail>")
			if not dbPath then
				dbPath = DEFAULT_KANEVA_PLACE_ICON
			end

			m_tLinkedZones[m_iZoneCount].thumbnail = dbPath		


			s, strPos, zoneType = string.find(zoneData, "<zone_type>(.-)</zone_type>")
			m_tLinkedZones[m_iZoneCount].zoneType = zoneType

			s, strPos, url = string.find(zoneData, "<STPURL>(.-)</STPURL>")
			m_tLinkedZones[m_iZoneCount].url = url

			if m_tPreviousDestination and m_tPreviousDestination.name == name then
				 ListBox_SelectItem(gHandles["lbxList"], m_iZoneCount)
			end
		    m_iZoneCount = m_iZoneCount + 1
		end
	end
end

-------------
--Control Handles
-------------
function btnCancel_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

function btnClose_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

function btnOK_OnButtonClicked(buttonHandle)

	if m_tOverviewItem then
		testColor()
		m_tOverviewItem.markers[m_iMarkerIndex].destination = deepCopy(m_tDestinationZone)
		--m_tOverviewItem.markers[m_iMarkerIndex].spawner = m_tDestinationZone.spawner

		local editEvent = KEP_EventCreate("FRAMEWORK_EDIT_POPOUT_SAVED")
		KEP_EventEncodeString(editEvent, Events.encode(m_tOverviewItem))
					
		KEP_EventQueue(editEvent)

		MenuCloseThis()
	else
		local event = KEP_EventCreate( "WORLD_SELECT_EVENT" )
		KEP_EventEncodeString(event, "zoneDestination")
		KEP_EventEncodeString(event, Events.encode(m_tDestinationZone))
	
		KEP_EventQueue( event )
		MenuCloseThis()
	end
end


function lbxList_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	m_tDestinationZone = m_tLinkedZones[sel_index]
	Events.sendEvent("FRAMEWORK_REQUEST_PLAYER_SPAWNERS", {zoneID = m_tDestinationZone.zoneID, zoneType = m_tDestinationZone.zoneType})
end


function updateDropDown()
	local cb = Dialog_GetComboBox(gDialogHandle, "cmbDropDown")
	ComboBox_RemoveAllItems(cb)

	ComboBox_AddItem(cb, "Default", 1)
	
	local count  = #m_tPlayerSpawners
	for i = 1, count do
		ComboBox_AddItem(cb, m_tPlayerSpawners[i].name, i -1)

	
		if m_tPreviousDestination and m_tPreviousDestination.spawner and m_tPreviousDestination.spawner.name  ==  m_tPlayerSpawners[i].name then 
			if m_tPreviousDestination.name == m_tDestinationZone.name then
				ComboBox_SetSelectedByText(cb, m_tPlayerSpawners[i].name)
				m_tDestinationZone.spawner = m_tPlayerSpawners[i]
			end
		end
	end

	local dropHeight = count * DROP_ELEMENT_HEIGHT

	if dropHeight > MAX_DROP_HEIGHT then
		dropHeight = MAX_DROP_HEIGHT
	end
	ComboBox_SetDropHeight(cb, dropHeight)

	_G["cmbDropDown_OnComboBoxSelectionChanged"] = function(comboBoxHandle, selectedIndex, selectedText)	
		if selectedIndex > 0 then	
			m_tDestinationZone.spawner = m_tPlayerSpawners[selectedIndex]
		end
	end

end

function customSort(a, b)
	return a.PID < b.PID
end

function setControlEnabledVisible(controlName, bool)
	Control_SetVisible(gHandles[controlName], bool)
	Control_SetEnabled(gHandles[controlName], bool)
end

-----------------------------------
------MARKER FUNCTIONS-------------
-----------------------------------

function editMarkerHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local menuSize = MenuGetSizeThis() 
	menuSize.height = menuSize.height + MARKER_MENU_INCREASE

	Static_SetText(gHandles["stcZones"], "Choose a destination zone for marker:")
	Dialog_SetSize(gDialogHandle, menuSize.width, menuSize.height)
	for index, control in pairs(MARKER_CONTROLS) do
		setControlEnabledVisible(control, true)
	end

	Control_SetLocationY(gHandles["btnOK"], Control_GetLocationY(gHandles["btnOK"]) + MARKER_MENU_INCREASE)
	Control_SetLocationY(gHandles["btnCancel"], Control_GetLocationY(gHandles["btnCancel"]) + MARKER_MENU_INCREASE)


	m_tOverviewItem = Events.decode(KEP_EventDecodeString(tEvent))
	m_iMarkerIndex = tonumber(KEP_EventDecodeString(tEvent))

	m_tPreviousDestination = {}
	m_tPreviousDestination = m_tOverviewItem.markers[m_iMarkerIndex].destination
	--m_tPreviousDestination.spawner = m_tOverviewItem.markers[m_iMarkerIndex].destination.spawner


	Slider_SetValue( gHandles["rSlider"], m_tOverviewItem.markers[m_iMarkerIndex].colorR or  52)
	Slider_SetValue( gHandles["gSlider"], m_tOverviewItem.markers[m_iMarkerIndex].colorG or 175)
	Slider_SetValue( gHandles["bSlider"], m_tOverviewItem.markers[m_iMarkerIndex].colorB or 212)

	EditBox_SetText(gHandles["edRed"],  m_tOverviewItem.markers[m_iMarkerIndex].colorR or  52, false)
	EditBox_SetText(gHandles["edGreen"],  m_tOverviewItem.markers[m_iMarkerIndex].colorG or 175, false)
	EditBox_SetText(gHandles["edBlue"], m_tOverviewItem.markers[m_iMarkerIndex].colorB or 212, false)

	EditBox_SetText(gHandles["edPosX"], tostring(m_tOverviewItem.markers[m_iMarkerIndex].positionX or 0), false)
	EditBox_SetText(gHandles["edPosY"], tostring(m_tOverviewItem.markers[m_iMarkerIndex].positionY or 0), false)

	testColor()
end

function rSlider_OnSliderValueChanged( slider, value )
	EditBox_SetText(gHandles["edRed"], value, false)
	testColor()
end

function gSlider_OnSliderValueChanged( slider, value )
	EditBox_SetText(gHandles["edGreen"], value, false)
	testColor()
end

function bSlider_OnSliderValueChanged( slider, value )
	EditBox_SetText(gHandles["edBlue"], value, false)
	testColor()
end

function edRed_OnEditBoxChange( editBox, value )

	value = validateEditBox(value, m_tOverviewItem.markers[m_iMarkerIndex].colorR, COLOR_MAX)
	EditBox_SetText(editBox, tostring(value), false)

	Slider_SetValue( gHandles["rSlider"], value )
	testColor()
end

function edGreen_OnEditBoxChange( editBox, value )
	value = validateEditBox(value, m_tOverviewItem.markers[m_iMarkerIndex].colorR, COLOR_MAX)

	EditBox_SetText(editBox, tostring(value), false)
	Slider_SetValue( gHandles["gSlider"], value )
	testColor()
end

function edBlue_OnEditBoxChange( editBox, value )
	value = validateEditBox(value, m_tOverviewItem.markers[m_iMarkerIndex].colorR, COLOR_MAX)

	EditBox_SetText(editBox, tostring(value), false)
	Slider_SetValue( gHandles["bSlider"], value )
	testColor()
end

function edPosX_OnEditBoxChange(editBox, value)
	value = validateEditBox(value, m_tOverviewItem.markers[m_iMarkerIndex].positionX, POS_X_MAX)

	m_tOverviewItem.markers[m_iMarkerIndex].positionX = value
	EditBox_SetText(editBox, tostring(value), false)
end

function edPosY_OnEditBoxChange(editBox, value)
	value = validateEditBox(value, m_tOverviewItem.markers[m_iMarkerIndex].positionY, POS_Y_MAX)

	m_tOverviewItem.markers[m_iMarkerIndex].positionY = value
	EditBox_SetText(editBox, tostring(value), false)
end


function validateEditBox(value, lastValue, maxValue)

	if value == "" then
		value = 0
	elseif not tonumber(value)   then	
		value =  lastValue
	elseif tonumber(value) < 0  then
		value = 0
	elseif tonumber(value) > maxValue  then
		value = maxValue
	else
		value = tonumber(value)
	end

	return value

end

function testColor()
	local r = tonumber(Slider_GetValue( gHandles["rSlider"] ))
	local g = tonumber(Slider_GetValue( gHandles["gSlider"] ))
	local b = tonumber(Slider_GetValue( gHandles["bSlider"] ))
	
	m_tOverviewItem.markers[m_iMarkerIndex].colorR = r
	m_tOverviewItem.markers[m_iMarkerIndex].colorG = g
	m_tOverviewItem.markers[m_iMarkerIndex].colorB = b

	local color = {  a=255, r=r, g = g, b=b }
	Image_SetColor( gHandles["imgMarker"], color )
end