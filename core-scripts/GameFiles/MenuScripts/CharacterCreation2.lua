--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\MenuTemplateHelper.lua")
dofile("..\\ClientScripts\\MenuGameIds.lua" )
dofile("..\\ClientScripts\\Utils.lua")

HUD_MINIMIZE_EVENT = "HudMinimizeEvent"
APT_SELECTION_EVENT = "ApartmentSelectionEvent"
KILL_BLACK_SCREEN = "KillBlackScreenEvent"

FORCED_STARTING_APARTMENT = 4649

HEIGHT_BASE_MALE	= 70
HEIGHT_BASE_FEMALE 	= 68
SCALE_TOLERANCE 	= 3

ANIMODE_STARTUP 	= 1
ANIMODE_IDLE 		= 2

gCurAnimMode		= ANIMODE_STARTUP
gAnimatedElements	= {}
gLabelMode			= nil

gViewportScroll		= nil

FEMALE 			= 1
MALE 			= 2
BODY_FEM_THIN 	= 3
BODY_FEM_MED 	= 4
BODY_FEM_BIG 	= 5
BODY_MALE_THIN 	= 6
BODY_MALE_MED 	= 7
BODY_MALE_BIG 	= 8

IDLE_ANIM_ID 	= 0

FLAGS 			= { ['FACE'] = 1, ['BODY'] = 2, ['EYECOLOR'] = 4, ['BROW'] = 8, ['FACECOVER'] = 16, ['HAIR'] = 32,
	['BODY_TYPE'] = 64, ['HEIGHT'] = 128, ['WEIGHT'] = 256 }
FLAGS_ALL_SET	= 1023

ACTOR_ID		= {  4, 3, 10, 11, 12, 13, 14, 15 }
CONFIG_HEAD		= {  6, 6, 6, 6, 6, 6, 6, 6 }
CONFIG_NECK		= { 99, 99, 1, 1, 1, 1, 1, 1 }
CONFIG_TORSO	= {  7, 7, 2, 2, 2, 2, 2, 2 }
CONFIG_CROTCH	= { 99, 99, 3, 3, 3, 3, 3, 3 }
CONFIG_ARMS		= { 99, 99, 4, 4, 4, 4, 4, 4 }
CONFIG_LEGS		= {  8, 8, 5, 5, 5, 5, 5, 5 }
CONFIG_HANDS	= { 99, 99, 6, 6, 6, 6, 6, 6 }
CONFIG_FEET		= { 14, 9, 7, 7, 7, 7, 7, 7 }
CONFIG_SHIRT	= {  9, 10, 8, 8, 8, 8, 8, 8 }
CONFIG_PANTS	= { 11, 12, 9, 9, 9, 9, 9, 9 }
CONFIG_BOOTS	= { 12, 13, 10, 10, 10, 10, 10, 10 }
CONFIG_HAIR		= {  0, 1, 12, 12, 12, 12, 12, 12 }
CONFIG_FACE		= {  4, 5, 13, 13, 13, 13, 13, 13 }
--CONFIG_LIPS		= { 99, 99, 14, 14, 14, 99, 99, 99 }

FACES_MALE 		= { 2988, 2989, 2990, 3720, 3746 }
FACES_FEMALE 	= { 2991, 2992, 2993, 3744, 3745 }

HAIR_FOR_EYES_MALE	= 18
HAIR_FOR_EYES_FEM 	= 23

FACE_EYE_RIGHT	= 0
FACE_EYE_LEFT 	= 2
FACE_EYEBROW	= 7
FACE_FACECOVER	= 8

-- The numbers in this section are the max index for an array starting at 0 (ie MAX_BEARD 0 - 15)
MAX_BEARD 		= 15
MAX_MAKEUP		= 15
MAX_BROW_FEM	= 6
MAX_BROW_MALE	= 5
-- BROW_COLOR for female only
MAX_BROW_COLOR	= 3
-- BEAR_COLOR for male only, affects eye brow color
MAX_BEARD_COLOR = 4
MAX_SKIN_COLOR	= 11
MAX_EYE_COLOR 	= 14

-- Same for the numbers within these hair color tables
HAIR_COLORS_MALE 	= { 8, 8, 13, 10, 8,
	8, 8, 8, 26, 26,
	26, 26, 26, 8, 12,
	8, 11, 8, 0, 8,
	8, 8, 8, 8, 8, 8}

HAIR_COLORS_FEM 	= { 26, 26, 26, 26, 26,
	26, 26, 26, 26, 26,
	26, 26, 26, 26, 26,
	26, 26, 26, 26, 8,
	8,	8,	8,	8,	8 }

VPMODE_SKINCOLOR	= 1
VPMODE_FACE			= 2
VPMODE_FACECOVER	= 3
VPMODE_BEARDCOLOR	= 4
VPMODE_HAIR			= 5
VPMODE_HAIRCOLOR	= 6
VPMODE_EYECOLOR		= 7
VPMODE_BROW			= 8
VPMODE_BODYTYPE		= 9

NUM_VIEWPORTS		= 9
NUM_COLUMNS			= 3

gDirtyFlag			= 0
gViewportDirtyFlag	= 0
gViewportFirstIndex = 1

gGenderIndex		= BODY_MALE_MED
gFaceIndex			= 1
gCurHairIndex		= 1
gCurEyebrow			= 0
gCurFaceCover		= 0
gCurSkinColor		= 0
gCurEyeColor		= 0
gCurBeardColor		= 0
gCurHairColor		= 0

gViewportsClean = false
gViewport			= {}
gCurViewportMode	= VPMODE_HAIR

gCurVPpos 			= { x = 0, 		y = 0, 		z = 0 }
gCurVPscale			= { x = 100, 	y = 100,	z = 100 }
gCurVProt			= { x = 0, 		y = 0, 		z = 0 }

gCurrentScaleY		= 100
gCurrentScaleXZ		= 100

gOrigGenderIndex		= BODY_MALE_MED
gOrigFaceIndex			= 1
gOrigCurHairIndex		= 1
gOrigCurEyebrow			= 0
gOrigCurFaceCover		= 0
gOrigCurSkinColor		= 0
gOrigCurEyeColor		= 0
gOrigCurBeardColor		= 0
gOrigCurHairColor		= 0

gOrigCurrentScaleXZ = 100
gOrigCurrentScaleY = 100

gPreviousNoSelDirty = 0 --Used if we select a menu but dont select an option
--and move to a new menu.  Update viewport with old values

gDoViewportUpdate = true  --used to turn the update function on and off.  the
--Dialog_OnRender is constantly called and when we close
--the menu the mini viewports are destroyed but it tries
--to update them causing a crash.

--Big ole hack for beefy not being the same height as other male actors
gBeefyMod = 0

CHAT_MINIMIZE_EVENT = "ChatMinimizeEvent"
GUIDE_COMPLETE_TASK_EVENT = "GuideCompleteTaskEvent"

--The base config sent to the server
gCharBaseConfig = nil

gCancelUpdate = false
gGracefulCleanup = false

g_ElapsedTime = 0
g_TotalTime = 0
g_LoadedFace = false
g_DontZoom = true

g_currentURL = ""

-----------------------------------------------------------------------------------
-- START CharCreate2DynamicMenu.lua Code
-----------------------------------------------------------------------------------

function UpdateMenuOnMoved()
	setupMenu()
	setupMenuViewports()
	showControlList( { "bntVPBodyType", "btnVPSkin", "btnVPFace", "btnVPEyes",
		"btnVPEyebrow", "btnVPHair", "btnVPHairColor",
		"btnVPFaceCover", "btnVPBeardColor", "btnRandomize",
		"btnSave", "btnCancel" }, true )
	if gCurViewportMode == VPMODE_BODYTYPE then
		showControl( "sldScaleXZ", true )
		showControl( "sldScaleY", true )
	end
end

function menuMovedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	UpdateMenuOnMoved()
end

function setupMenu()
	local HUD_HEIGHT 		= 83
	local HUD_X_FROM_MID 	= 407
	local screenW = Dialog_GetScreenWidth( gDialogHandle )
	local screenH = Dialog_GetScreenHeight( gDialogHandle )
	local menuH = Dialog_GetHeight( gDialogHandle )
	local x = ( screenW / 2 ) - HUD_X_FROM_MID
	Dialog_SetLocation( gDialogHandle, x, screenH - menuH - HUD_HEIGHT )

	local slider = nil
	slider = Dialog_GetSlider( gDialogHandle, "sldScaleY" )
	Slider_SetRange( slider, 90, 110 )
	Slider_SetValue( slider, 100 )
	slider = Dialog_GetSlider( gDialogHandle, "sldScaleXZ" )
	Slider_SetRange( slider, 90, 110 )
	Slider_SetValue( slider, 100 )
	gLabelMode = Dialog_GetStatic( gDialogHandle, "lblMode" )
	showControlList( {  "btnVPBack", "btnVPForward", "bntVPBodyType",
		"btnVPSkin", "btnVPFace", "btnVPEyes",
		"btnVPEyebrow", "btnVPHair", "btnVPHairColor",
		"btnVPFaceCover", "btnVPBeardColor", "btnRandomize",
		"btnSave", "sldScaleXZ", "sldScaleY", "lblHeightCurrent", "btnCancel" }, false )
	gViewportScroll = Dialog_GetListBox( gDialogHandle, "lstViewportScroll" )
	setupScrollBarTextures( gViewportScroll )
end

function setupViewportScroller( max, rem, rowHeight )
	local sbh = ListBox_GetScrollBar( gViewportScroll )
	local pageSize = 1
	local numLines = ( max / NUM_COLUMNS ) - ( NUM_VIEWPORTS / NUM_COLUMNS ) + 1

	if math.fmod( max, NUM_COLUMNS ) > 0 then
		numLines = numLines + 1
	end
	ListBox_RemoveAllItems( gViewportScroll )
	for i = 1, numLines do
		ListBox_AddItem( gViewportScroll, "", 0 )
	end
	ListBox_SetRowHeight( gViewportScroll, rowHeight )
	ScrollBar_SetPageSize( sbh, pageSize )
end

function updateViewportFirstIndex()
	local sbh = ListBox_GetScrollBar( gViewportScroll )
	local trackPos = ScrollBar_GetTrackPos( sbh )
	local oldFirstIndex = gViewportFirstIndex
	gViewportFirstIndex = trackPos * NUM_COLUMNS
	if gViewportFirstIndex ~= oldFirstIndex then
		setupMenuViewports()
	end
end

function sldScaleY_OnSliderValueChanged( sh )
	local sliderXZ = Dialog_GetSlider( gDialogHandle, "sldScaleXZ" )
	local scaleY = Slider_GetValue( sh )
	local scaleXZ = Slider_GetValue( sliderXZ )

	if not gCancelUpdate then
		local diff = math.abs( scaleY - scaleXZ )
		if diff > SCALE_TOLERANCE then
			if scaleY > scaleXZ then
				scaleXZ = scaleY - SCALE_TOLERANCE
			else
				scaleXZ = scaleY + SCALE_TOLERANCE
			end
			Slider_SetValue( sliderXZ, scaleXZ )
		end
		gCurrentScaleXZ = scaleXZ
		gCurrentScaleY = scaleY
	end
	local player = KEP_GetPlayer( )
	KEP_ScaleRuntimePlayer(player, (gCurrentScaleXZ / 100), (gCurrentScaleY / 100), (gCurrentScaleXZ / 100))
	updateHeightLabel()
end

function sldScaleXZ_OnSliderValueChanged( sh )
	local sliderY = Dialog_GetSlider( gDialogHandle, "sldScaleY" )
	local scaleY = Slider_GetValue( sliderY )
	local scaleXZ = Slider_GetValue( sh )
	if not gCancelUpdate then
		local diff = math.abs( scaleY - scaleXZ )
		if diff > SCALE_TOLERANCE then
			if scaleY < scaleXZ then
				scaleY = scaleXZ - SCALE_TOLERANCE
			else
				scaleY = scaleXZ + SCALE_TOLERANCE
			end
			Slider_SetValue( sliderY, scaleY )
		end
		gCurrentScaleXZ = scaleXZ
		gCurrentScaleY = scaleY
	end
	local player = KEP_GetPlayer( )
	KEP_ScaleRuntimePlayer(player, (gCurrentScaleXZ / 100), (gCurrentScaleY / 100), (gCurrentScaleXZ / 100))
	updateHeightLabel()
end

function updateHeightLabel()
	local startHeight = 0
	if isFemale() then
		startHeight = HEIGHT_BASE_FEMALE
	else
		startHeight = HEIGHT_BASE_MALE
	end
	local height = ( ( gCurrentScaleY - 100 ) / 100 ) * startHeight + startHeight
	local feet = math.floor( height / 12 )
	local inches = math.floor( height - ( feet * 12 ) + 0.5 )
	local sh = Dialog_GetStatic( gDialogHandle, "lblHeightCurrent" )
	if sh ~= nil then
		Static_SetText( sh, feet .. "\' " .. inches .. "\"" )
	end
end

function updateAnimation( elapsedMs )
	if gCurAnimMode == ANIMODE_STARTUP then
		updateMenuElements( elapsedMs )
	end
end

function updateMenuElements( elapsedMs )
	local allFinished = true
	for i, v in ipairs( gAnimatedElements ) do
		if ( v.time - v.startDelay ) < v.duration then
			allFinished = false
			v.time = v.time + elapsedMs
			local val = ( v.time - v.startDelay ) / v.duration
			if val < 0 then val = 0 end
			if val > 1 then val = 1 end
			local animPoint = MathFX.easeOut( 0, 1, val )
			v.size = v.sizeStart + ( ( v.sizeEnd - v.sizeStart ) * animPoint )
			Control_SetSize( v.control, v.size, Control_GetHeight( v.control ) )
		end
	end
	if allFinished then
		gCurAnimMode = ANIMODE_IDLE
		showControlList( { "bntVPBodyType", "btnVPSkin", "btnVPFace", "btnVPEyes",
			"btnVPEyebrow", "btnVPHair", "btnVPHairColor",
			"btnVPFaceCover", "btnVPBeardColor", "btnRandomize",
			"btnSave", "sldScaleXZ", "sldScaleY", "lblHeightCurrent", "btnCancel" }, true )
	end
end

function init() -- 1234

	readOriginalConfigs()
	setupMenu()
	initViewPorts()
	setupActor()
	bntVPBodyType_OnButtonClicked( nil )

	if isFemale() then
		local bh = Dialog_GetButton( gDialogHandle, "btnVPBeardColor" )
		local s_bh = Button_GetStatic(bh)
		Static_SetText(s_bh, "Eyebrow Color")
		bh = Dialog_GetButton( gDialogHandle, "btnVPFaceCover" )
		s_bh = Button_GetStatic(bh)
		Static_SetText(s_bh, "Makeup")
	end
end

function disableViewportUpdate()
	gDoViewportUpdate = false
end

function enableViewportUpdate()
	gDoViewportUpdate = true
end

function initViewPorts()
	if gDialogHandle ~= nil then
		local max, rem = getViewportOptionLimits()
		local rowHeight = 0
		for i = 0, NUM_VIEWPORTS - 1 do
			local button = Dialog_GetControl( gDialogHandle, "btnViewport" .. tostring( i ) )
			local dialogX = Dialog_GetLocationX( gDialogHandle )
			local dialogY = Dialog_GetLocationY( gDialogHandle )
			if button ~= nil then
				local GUTTER = 5
				local buttonBounds = { 	x = Control_GetLocationX( button ),
					y = Control_GetLocationY( button ),
					w = Control_GetWidth( button ),
					h = Control_GetHeight( button ) }
				local viewport = {}
				local genderIndex = BODY_MALE_MED

				--Initialize first 3 windows to different body types
				--since you cant see body in other views
				if i < 3 then
					genderIndex = BODY_MALE_THIN + i
					if isFemale() then
						genderIndex = BODY_FEM_THIN + i
					end
				else
					if isFemale() then
						genderIndex = BODY_FEM_MED
					end
				end

				viewport['actor'] = KEP_AddMenuGameObject( ACTOR_ID[ genderIndex ], IDLE_ANIM_ID )
				viewport['x'] = dialogX + buttonBounds.x + GUTTER
				viewport['y'] = dialogY + buttonBounds.y + GUTTER
				GetSet_SetVector( viewport.actor, MenuGameIds.POSITION, gCurVPpos.x, gCurVPpos.y, gCurVPpos.z )
				GetSet_SetVector( viewport.actor, MenuGameIds.SCALING, gCurVPscale.x, gCurVPscale.y, gCurVPscale.z )
				GetSet_SetVector( viewport.actor, MenuGameIds.ROTATION, gCurVProt.x, gCurVProt.y, gCurVProt.z )
				GetSet_SetVector( viewport.actor, MenuGameIds.VP_POSITION, viewport.x, viewport.y, 0 )
				GetSet_SetVector( viewport.actor, MenuGameIds.VP_DIMENSIONS, buttonBounds.w - ( GUTTER * 2 ), buttonBounds.h - ( GUTTER * 2 ), 0 )
				--init here so not updated with every time body is changed.
				KEP_SetCharacterAltConfig( viewport.actor, CONFIG_SHIRT[ genderIndex ], 1, 0 )
				KEP_SetCharacterAltConfig( viewport.actor, CONFIG_PANTS[ genderIndex ], 1, 0 )
				KEP_SetCharacterAltConfig( viewport.actor, CONFIG_BOOTS[ genderIndex ], 1, 0 )
				gViewport[ i + 1 ] = viewport
				rowHeight = buttonBounds.h
			end
		end
	end

	gViewportDirtyFlag = FLAGS_ALL_SET
end

function moveViewPorts()
	local max, rem = getViewportOptionLimits()
	local rowHeight = 0
	for i = 1, NUM_VIEWPORTS do
		local button = Dialog_GetControl( gDialogHandle, "btnViewport" .. tostring( i - 1 ) )
		local dialogX = Dialog_GetLocationX( gDialogHandle )
		local dialogY = Dialog_GetLocationY( gDialogHandle )
		if button ~= nil then
			if gViewport[i].actor then
				local GUTTER = 5
				local buttonBounds = { 	x = Control_GetLocationX( button ),
					y = Control_GetLocationY( button ),
					w = Control_GetWidth( button ),
					h = Control_GetHeight( button ) }

				gViewport[i]['x'] = dialogX + buttonBounds.x + GUTTER
				gViewport[i]['y'] = dialogY + buttonBounds.y + GUTTER
				GetSet_SetVector( gViewport[i].actor, MenuGameIds.VP_POSITION, gViewport[i]['x'], gViewport[i]['y'], 0 )
				rowHeight = buttonBounds.h
			end
		end
	end
end

-----------------------------------------------------------------------------------
-- END CharCreate2DynamicMenu.lua Code
-----------------------------------------------------------------------------------

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "menuMovedEventHandler", "MenuMovedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "zoneNavigationEventHandler",	"ZoneNavigationEvent",KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "MenuMovedEvent", KEP.MED_PRIO )
	KEP_EventRegister( CHAT_MINIMIZE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "ValidateURLEvent",			KEP.MED_PRIO )
	KEP_EventRegister( "ZoneNavigationEvent",KEP.MED_PRIO )

	local ev = KEP_EventCreate( HUD_MINIMIZE_EVENT)
	if(ev ~= 0) then
		KEP_EventEncodeNumber(ev, 1)
		KEP_EventEncodeNumber(ev, 1) -- override any show while hidden settings
		KEP_EventQueue( ev)
	end
end

function Dialog_OnCreate( dialogHandle )
	Helper_Dialog_OnCreate( dialogHandle )

	log("\n I shouldn't be opening anymore")

	-- DRF - Close All Non-Essential Menus Otherwise Weird Things Happen
	MenuCloseAllNonEssentialExceptThis()

	local ev = KEP_EventCreate( CHAT_MINIMIZE_EVENT)
	if ev ~= 0 then
		KEP_EventEncodeNumber(ev, 2) -- Code 2 does NOTHING
		KEP_EventQueue( ev)
	end

	local event = KEP_EventCreate( "ZoneNavigationEvent")
	KEP_EventSetFilter(event, ZONE_NAV.CURRENT_URL_REQUEST)
	KEP_EventQueue( event)
	
	math.randomseed( KEP_CurrentTime() )

	readOriginalConfigs()

	-- Turn off camera collision to avoid any weird camera issue during avatar customization
	-- Since player will respawn when this menu is closed, there is no need to restore this flag.
	KEP_SetCameraCollision(0)

	local temp = getCurFace()
	if (temp == 1 or temp == nil ) then
		return
	end
	g_LoadedFace = true
	init()
end

function Dialog_OnRender( dialogHandle, elapsedSec )
	if (g_LoadedFace) then
		updateAnimation( elapsedSec * 1000.0 )
		if gDoViewportUpdate then
			moveViewPorts()
		end
	end

	if (g_LoadedFace == true) then
		return
	end
	g_TotalTime = g_TotalTime + elapsedSec
	if (g_TotalTime > 1) then
		g_TotalTime = 0
		if (getCurFace() ~= 0) then
			g_LoadedFace = true
			init()
			local ev = KEP_EventCreate( KILL_BLACK_SCREEN)
			if(ev ~= 0) then
				KEP_EventQueue( ev)
			end
		end
	end
end

function Dialog_OnDestroy( dialogHandle )
	Helper_Dialog_OnDestroy( dialogHandle )
	if g_DontZoom then
		resetCamera()
	end

	if gGracefulCleanup==false then
		-- make sure menugameobjects are removed even under exterme conditions
		disableViewportUpdate()
		if (gViewportsClean == false) then
			cleanupMenuViewports()
		end
	end

	local ev = KEP_EventCreate( HUD_MINIMIZE_EVENT)
	if(ev ~= 0) then
		KEP_EventEncodeNumber(ev, 0)
		KEP_EventQueue( ev)
	end
end

function Dialog_OnMoved(dialogHandle, ptX, ptY)
	UpdateMenuOnMoved()
end

function updateAppearance()
	updateBodyParts()
	updateViewportBodyParts()
end

function zoneNavigationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ZONE_NAV.CURRENT_URL_INFO then
		local url = KEP_EventDecodeString( event )
		g_currentURL = url
	end
end 

--[[ DRF - Sends RefreshEvent To Hud2 Who Rezones Us ]]
function Rezone()
	if g_currentURL ~= "" then 
		local ev = KEP_EventCreate( "ValidateURLEvent" )
		KEP_EventEncodeString ( ev, g_currentURL)
		KEP_EventEncodeString (ev,"Rezone")
		KEP_EventQueue( ev )
	end
end

function btnCancel_OnButtonClicked( buttonHandle )
	
	-- Cancel Avatar Changes
	cancelUpdate()
end

function btnSave_OnButtonClicked( buttonHandle )

	-- Save Avatar Changes
	disableViewportUpdate()
	validateCharacter()

	-- Clean Things Up Ready For Rezone
	cleanupMenuViewports()
	gGracefulCleanup = true
	g_DontZoom = false

	-- Rezone To Refresh Avatar, Zone & Menus
	Rezone()
end

function btnRandomize_OnButtonClicked( buttonHandle )

	if isFemale() then
		gGenderIndex = math.random( BODY_FEM_THIN, BODY_FEM_BIG )
		gFaceIndex = math.random( #FACES_FEMALE )
		gCurEyebrow = math.random( 0, MAX_BROW_FEM )
		gCurFaceCover = math.random( 0, MAX_MAKEUP )
		gCurHairIndex = math.random( #HAIR_COLORS_FEM )
		gCurHairColor = math.random( 0, HAIR_COLORS_FEM[ gCurHairIndex ] )
		gCurBeardColor	= math.random( 0, MAX_BROW_COLOR )
	else
		gGenderIndex = math.random( BODY_MALE_THIN, BODY_MALE_BIG )
		gFaceIndex = math.random( #FACES_MALE )
		gCurEyebrow	= math.random( 0, MAX_BROW_MALE )
		gCurFaceCover = math.random( 0, MAX_BEARD )
		gCurHairIndex = math.random( #HAIR_COLORS_MALE )
		gCurHairColor = math.random( 0, HAIR_COLORS_MALE[ gCurHairIndex ] )
		gCurBeardColor	= math.random( 0, MAX_BEARD_COLOR )
	end
	gCurSkinColor	= math.random( 0, MAX_SKIN_COLOR )
	gCurEyeColor	= math.random( 0, MAX_EYE_COLOR )
	
	Log("RandomizeClicked: hairIndex="..gCurHairIndex.." faceIndex="..gFaceIndex)

	setupActor()
	resetViewportOptions()
	setupMenuViewports()
	gViewportDirtyFlag = FLAGS_ALL_SET
	updateAppearance()
end

function btnVPForward_OnButtonClicked( buttonHandle )
	local max, rem = getViewportOptionLimits()
	local showFwd = true
	if (gViewportFirstIndex + NUM_VIEWPORTS) - max <= 0 then
		gViewportFirstIndex = gViewportFirstIndex + NUM_VIEWPORTS
	end
	showControl( "btnVPBack", true )
	showControl( "btnVPForward", (gViewportFirstIndex + NUM_VIEWPORTS) - max <= 0 )
	setupMenuViewports()
	setViewportDirty( gCurViewportMode )
	updateAppearance()
end

function btnVPBack_OnButtonClicked( buttonHandle )
	local max, rem = getViewportOptionLimits()
	gViewportFirstIndex = gViewportFirstIndex - NUM_VIEWPORTS
	if gViewportFirstIndex < 1 then
		gViewportFirstIndex = 1
	end
	showControl( "btnVPBack", gViewportFirstIndex ~= 1 )
	showControl( "btnVPForward", (gViewportFirstIndex + NUM_VIEWPORTS) - max <= 0 )
	setupMenuViewports()
	setViewportDirty( gCurViewportMode )
	updateAppearance()
end

function btnVPSkin_OnButtonClicked( buttonHandle )
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty( gPreviousNoSelDirty )
	end
	gCurViewportMode = VPMODE_SKINCOLOR
	gBeefyMod = .35
	Static_SetText( gLabelMode, "Skin Tone" )
	if isFemale() then
		transformVPActor( 0, -26.5, 0, 10.5, 10.5, 10.5, 0, 15, 0 )
	else
		transformVPActor( 0, -22.0, 0, 8.5, 8.5, 8.5, 0, 10, 0 )
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty( gCurViewportMode )
	updateAppearance()
	zoomBody()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPHair_OnButtonClicked( buttonHandle )
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty( gPreviousNoSelDirty )
	end
	gCurViewportMode = VPMODE_HAIR
	gBeefyMod = .25
	Static_SetText( gLabelMode, "Hair Style" )
	if isFemale() then
		transformVPActor( -0.5, -16.5, 0, 6.5, 6.5, 6.5, 0, 10, 0 )
	else
		transformVPActor( 0, -13.5, 0, 5, 5, 5, 0, 10, 0 )
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty( gCurViewportMode )
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPHairColor_OnButtonClicked( buttonHandle )
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty( gPreviousNoSelDirty )
	end
	gCurViewportMode = VPMODE_HAIRCOLOR
	gBeefyMod = .25
	Static_SetText( gLabelMode, "Hair Color" )
	if isFemale() then
		transformVPActor( -0.5, -16.5, 0, 6.5, 6.5, 6.5, 0, 10, 0 )
	else
		transformVPActor( 0, -13.5, 0, 5, 5, 5, 0, 10, 0 )
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty( gCurViewportMode )
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPEyes_OnButtonClicked( buttonHandle )
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty( gPreviousNoSelDirty )
	end
	gCurViewportMode = VPMODE_EYECOLOR

	gBeefyMod = .75
	Static_SetText( gLabelMode, "Eye Color" )
	if isFemale() then
		transformVPActor( -3.75, -77, 0, 29, 29, 29, 0, 4, 0 )
	else
		transformVPActor( -1.5, -52, 0, 19, 19, 19, 0, 2, 0 )
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty( gCurViewportMode )
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPFace_OnButtonClicked( buttonHandle )
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty( gPreviousNoSelDirty )
	end
	gCurViewportMode = VPMODE_FACE
	gBeefyMod = .25
	Static_SetText( gLabelMode, "Face Style" )
	if isFemale() then
		transformVPActor( 0, -26.5, 0, 10.5, 10.5, 10.5, 0, 15, 0 )
	else
		transformVPActor( 0, -22.0, 0, 8.5, 8.5, 8.5, 0, 10, 0 )
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty( gCurViewportMode )
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPFaceCover_OnButtonClicked( buttonHandle )
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty( gPreviousNoSelDirty )
	end
	gCurViewportMode = VPMODE_FACECOVER
	gBeefyMod = .25
	if isFemale() then
		Static_SetText( gLabelMode, "Makeup Style" )
		transformVPActor( 0, -25.0, 0, 10, 10, 10, 0, 10, 0 )
	else
		Static_SetText( gLabelMode, "Beard Style" )
		transformVPActor( 0, -21.5, 0, 9, 9, 9, 0, 20, 0 )
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty( gCurViewportMode )
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPBeardColor_OnButtonClicked( buttonHandle )
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty( gPreviousNoSelDirty )
	end
	gCurViewportMode = VPMODE_BEARDCOLOR
	gBeefyMod = .35
	if isFemale() then
		Static_SetText( gLabelMode, "Eyebrow Color" )
		transformVPActor( -3.75, -77, 0, 29, 29, 29, 0, 4, 0 )
	else
		Static_SetText( gLabelMode, "Beard and Eyebrow Color" )
		transformVPActor( 0, -21, 0, 8, 8, 8, 0, 20, 0 )
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty( gCurViewportMode )
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function btnVPEyebrow_OnButtonClicked( buttonHandle )
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty( gPreviousNoSelDirty )
	end
	gCurViewportMode = VPMODE_EYEBROW
	gBeefyMod = .75
	Static_SetText( gLabelMode, "Eyebrow Style" )
	if isFemale() then
		transformVPActor( -3.75, -77, 0, 29, 29, 29, 0, 4, 0 )
	else
		transformVPActor( -1.5, -53, 0, 19, 19, 19, 0, 2, 0 )
	end
	resetViewportOptions()
	setupMenuViewports()
	setViewportDirty( gCurViewportMode )
	updateAppearance()
	zoomFace()
	gPreviousNoSelDirty = gCurViewportMode
end

function bntVPBodyType_OnButtonClicked( buttonHandle )
	if gPreviousNoSelDirty ~= 0 then
		setViewportDirty( gPreviousNoSelDirty )
	end
	gCurViewportMode = VPMODE_BODYTYPE
	gBeefyMod = .25
	Static_SetText( gLabelMode, "Body Size" )
	transformVPActor( 0, -5, 0, 3, 3, 3, 0, 20, 0 )
	resetViewportOptions()
	setupMenuViewports()
	showControl( "lblWeight", true )
	showControl( "lblHeight", true )
	showControl( "sldScaleXZ", true )
	showControl( "sldScaleY", true )
	showControl( "lblHeightCurrent", true )
	setViewportDirty( gCurViewportMode )
	updateAppearance()
	zoomBody()
	gPreviousNoSelDirty = gCurViewportMode
end

function setViewportDirty( category )

	if gCurViewportMode == VPMODE_BODYTYPE then
		--This does not need to be done since we never change menu viewport
		--actor body types
		--gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.BODY_TYPE )
		return
	end

	if gCurViewportMode == VPMODE_SKINCOLOR then
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.FACE )
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.BODY )
		return
	end

	if gCurViewportMode == VPMODE_FACE then
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.FACE )
	end

	if gCurViewportMode == VPMODE_FACECOVER	then
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.FACECOVER )
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.BROW )
		return
	end

	if gCurViewportMode == VPMODE_HAIR or gCurViewportMode == VPMODE_HAIRCOLOR then
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.HAIR )
		return
	end

	if gCurViewportMode == VPMODE_EYECOLOR	then
		--update hair to make sure it doesnt cover eye color
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.HAIR )
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.EYECOLOR )
		return
	end

	if gCurViewportMode == VPMODE_EYEBROW or gCurViewportMode == VPMODE_BEARDCOLOR then
		--update hair to make sure it doesnt cover makeup or eyebrow
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.HAIR )
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.BROW )
		gViewportDirtyFlag = BitFlag.set( gViewportDirtyFlag, FLAGS.FACECOVER )
		return
	end
end

function btnViewport0_OnButtonClicked( buttonHandle )
	viewportButtonHandler( gViewportFirstIndex  )
	updateAppearance()
	gPreviousNoSelDirty = 0
	setViewportDirty( gCurViewportMode )
end

function btnViewport1_OnButtonClicked( buttonHandle )
	viewportButtonHandler( gViewportFirstIndex + 1 )
	updateAppearance()
	gPreviousNoSelDirty = 0
	setViewportDirty( gCurViewportMode )
end

function btnViewport2_OnButtonClicked( buttonHandle )
	viewportButtonHandler( gViewportFirstIndex + 2 )
	updateAppearance()
	gPreviousNoSelDirty = 0
	setViewportDirty( gCurViewportMode )
end

function btnViewport3_OnButtonClicked( buttonHandle )
	viewportButtonHandler( gViewportFirstIndex + 3 )
	updateAppearance()
	gPreviousNoSelDirty = 0
	setViewportDirty( gCurViewportMode )
end

function btnViewport4_OnButtonClicked( buttonHandle )
	viewportButtonHandler( gViewportFirstIndex + 4 )
	updateAppearance()
	gPreviousNoSelDirty = 0
	setViewportDirty( gCurViewportMode )
end

function btnViewport5_OnButtonClicked( buttonHandle )
	viewportButtonHandler( gViewportFirstIndex + 5 )
	updateAppearance()
	gPreviousNoSelDirty = 0
	setViewportDirty( gCurViewportMode )
end

function btnViewport6_OnButtonClicked( buttonHandle )
	viewportButtonHandler( gViewportFirstIndex + 6 )
	updateAppearance()
	gPreviousNoSelDirty = 0
	setViewportDirty( gCurViewportMode )
end

function btnViewport7_OnButtonClicked( buttonHandle )
	viewportButtonHandler( gViewportFirstIndex + 7 )
	updateAppearance()
	gPreviousNoSelDirty = 0
	setViewportDirty( gCurViewportMode )
end

function btnViewport8_OnButtonClicked( buttonHandle )
	viewportButtonHandler( gViewportFirstIndex + 8 )
	updateAppearance()
	gPreviousNoSelDirty = 0
	setViewportDirty( gCurViewportMode )
end

function updateBodyParts()
	local player = KEP_GetPlayer( )
	if player ~= nil and gDirtyFlag ~= 0 then
		local newBody = false
		-- !!! NOTE: until Jonny's fix is in configBody MUST come before configFace !!!
		if BitFlag.test( gDirtyFlag, FLAGS.BODY ) then
			configBody( player )
			-- update player
			player = KEP_GetPlayer( )
			gDirtyFlag = BitFlag.clear( gDirtyFlag, FLAGS.BODY )
		end
		if BitFlag.test( gDirtyFlag, FLAGS.FACE ) then
			configFace( player )
			gDirtyFlag = BitFlag.clear( gDirtyFlag, FLAGS.FACE )
		end
		if BitFlag.test( gDirtyFlag, FLAGS.EYECOLOR ) then
			configEyeColor( player )
			gDirtyFlag = BitFlag.clear( gDirtyFlag, FLAGS.EYECOLOR )
		end
		if BitFlag.test( gDirtyFlag, FLAGS.BROW ) then
			configEyebrow( player )
			gDirtyFlag = BitFlag.clear( gDirtyFlag, FLAGS.BROW )
		end
		if BitFlag.test( gDirtyFlag, FLAGS.FACECOVER ) then
			configBeardAndMakeupColors( player )
			gDirtyFlag = BitFlag.clear( gDirtyFlag, FLAGS.FACECOVER )
		end
		if BitFlag.test( gDirtyFlag, FLAGS.HAIR ) then
			configHair( player )
			gDirtyFlag = BitFlag.clear( gDirtyFlag, FLAGS.HAIR )
		end
		if BitFlag.test( gDirtyFlag, FLAGS.BODY_TYPE ) then
			newBody = true
			gDirtyFlag = BitFlag.clear( gDirtyFlag, FLAGS.BODY_TYPE )
		end
		if BitFlag.test( gDirtyFlag, FLAGS.HEIGHT ) then
			newBody = true
			gDirtyFlag = BitFlag.clear( gDirtyFlag, FLAGS.HEIGHT )
		end
		if BitFlag.test( gDirtyFlag, FLAGS.WEIGHT ) then
			newBody = true
			gDirtyFlag = BitFlag.clear( gDirtyFlag, FLAGS.WEIGHT )
		end
	end
end

function updateViewportBodyParts()
	if gViewport ~= nil and gViewportDirtyFlag ~= 0 then
		for index, viewport in pairs(gViewport) do
			local vpa = viewport.actor
			if vpa ~= nil then
				if BitFlag.test( gViewportDirtyFlag, FLAGS.FACE ) then
					configViewportFace( vpa, index )
				end
				if BitFlag.test( gViewportDirtyFlag, FLAGS.BODY ) then
					configViewportBody( vpa, index )
				end
				if BitFlag.test( gViewportDirtyFlag, FLAGS.EYECOLOR ) then
					configViewportEyeColor( vpa, index )
				end
				if BitFlag.test( gViewportDirtyFlag, FLAGS.BROW ) then
					configViewportEyebrow( vpa, index )
				end
				if BitFlag.test( gViewportDirtyFlag, FLAGS.FACECOVER ) then
					configViewportBeardAndMakeupColors( vpa, index )
				end
				if BitFlag.test( gViewportDirtyFlag, FLAGS.HAIR ) then
					configViewportHair( vpa, index )
				end
			end
		end
		gViewportDirtyFlag = 0
	end
end

function getCurFace()
	local facemax = 0
	local oldface = gFaceIndex
	if isFemale() then
		facemax = #FACES_FEMALE
	else
		facemax = #FACES_MALE
	end
	local player = KEP_GetPlayer( )
	local t = { KEP_GetCharacterAttachmentTypes(player) }
	for i = 1, #t do
		for j = 1, facemax do
			gFaceIndex = j
			if tonumber(t[i]) == getFaceAssByIndex() then
				gFaceIndex = oldface
				return j
			end
		end
	end
	gFaceIndex = oldface
	return 1
end

function findIndexByValue( aTable, value )
	for i=1, #aTable do
		if aTable[i] == value then
			return i
		end
	end
end

function readOriginalConfigs()
	local player = KEP_GetPlayer( )
	if player ~= nil then
		bodyIndex = GetSet_GetNumber( player, MovementIds.IDXREFMODEL  )
		gGenderIndex = findIndexByValue( ACTOR_ID, bodyIndex )
		gOrigGenderIndex = gGenderIndex
		gFaceIndex	= getCurFace()
		gOrigFaceIndex = gFaceIndex

		gCurEyebrow = KEP_GetPlayerConfigIndex(player, getFaceAssByIndex(), FACE_EYEBROW)
		gOrigCurEyebrow = gCurEyebrow

		gCurSkinColor = KEP_GetPlayerConfigMaterial(player, 0, CONFIG_CROTCH[gGenderIndex])
		gOrigCurSkinColor = gCurSkinColor

		gCurEyeColor = KEP_GetPlayerConfigMaterial(player, getFaceAssByIndex(), FACE_EYE_LEFT)
		gOrigCurEyeColor = gCurEyeColor

		if not isFemale() then
			gCurHairColor = KEP_GetPlayerConfigMaterial(player, 0, 25)
			gCurHairIndex = KEP_GetPlayerConfigIndex(player, 0, 25)
			gCurFaceCover = KEP_GetPlayerConfigIndex(player, getFaceAssByIndex(), FACE_FACECOVER)
			gCurBeardColor = KEP_GetPlayerConfigMaterial(player, getFaceAssByIndex(), FACE_FACECOVER)
		else
			gCurHairColor = KEP_GetPlayerConfigMaterial(player, 0, 27)
			gCurHairIndex = KEP_GetPlayerConfigIndex(player, 0, 27)
			gCurFaceCover = KEP_GetPlayerConfigMaterial(player, getFaceAssByIndex(), FACE_FACECOVER)
		end

		gOrigCurHairIndex = gCurHairIndex
		gOrigCurHairColor = gCurHairColor
		gOrigCurBeardColor = gCurBeardColor
		gOrigCurFaceCover = gCurFaceCover

		local scale = KEP_GetRuntimePlayerScale(player)

		gCurrentScaleXZ = math.floor(scale.x*100)
		gCurrentScaleY = math.floor(scale.y*100)
		gOrigCurrentScaleXZ = gCurrentScaleXZ
		gOrigCurrentScaleY = gCurrentScaleY
	end
end

function resetCamera()
	zoomBody()
	--TODO: Get the players original camera configuration and resets
end

function zoomBody()
	KEP_SetCameraCollision(0)	-- call again to ensure no one overrides the flag after menu is opened
	local cam = KEP_GetPlayersActiveCamera(gGame)
	GetSet_SetNumber( cam, CameraIds.ORBITAZIMUTH, -3)
	GetSet_SetNumber( cam, CameraIds.ORBITELEVATION, .16)
	GetSet_SetNumber( cam, CameraIds.ORBITDIST, 12)
	GetSet_SetVector( cam, CameraIds.ORBITFOCUS, 0, 5, 0)
end

function zoomFace()
	KEP_SetCameraCollision(0)	-- call again to ensure no one overrides the flag after menu is opened
	local cam = KEP_GetPlayersActiveCamera(gGame)
	GetSet_SetNumber( cam, CameraIds.ORBITAZIMUTH, -3)
	GetSet_SetNumber( cam, CameraIds.ORBITELEVATION, .2)
	GetSet_SetNumber( cam, CameraIds.ORBITDIST, 1)
	AVATAR_HEIGHT_MAX = 110
	AVATAR_HEIGHT_MIN = 90
	CAMY_HEIGHT_MAX = 6.5 --this is the optimal camera height for max avatar height
	CAMY_HEIGHT_MIN = 5.5 --this is the optimal camera height for min avatar height
	--this sets the focus of the camera depending on teh characters height
	GetSet_SetVector( cam, CameraIds.ORBITFOCUS, 0, CAMY_HEIGHT_MAX - (AVATAR_HEIGHT_MAX - gCurrentScaleY)/(AVATAR_HEIGHT_MAX - AVATAR_HEIGHT_MIN) * (CAMY_HEIGHT_MAX - CAMY_HEIGHT_MIN), -3)
end

function setOriginalConfigs()
	gGenderIndex = gOrigGenderIndex
	gFaceIndex	= gOrigFaceIndex
	gCurHairIndex = gOrigCurHairIndex
	gCurEyebrow = gOrigCurEyebrow
	gCurFaceCover = gOrigCurFaceCover
	gCurSkinColor = gOrigCurSkinColor
	gCurEyeColor = gOrigCurEyeColor
	gCurBeardColor = gOrigCurBeardColor
	gCurHairColor = gOrigCurHairColor
	gCurrentScaleXZ = gOrigCurrentScaleXZ
	gCurrentScaleY = gOrigCurrentScaleY
end

function validateCharacter()
	local team = 2
    putOnWizardsRobeAndHat()
    if gCharBaseConfig ~= 0 and gCharBaseConfig ~= nil then
        KEP_CreateCharacter( gCharBaseConfig, GameGlobals.STARTER_HOME_ID, KEP_GetLoginName(), team, gCurrentScaleXZ, gCurrentScaleY, gCurrentScaleXZ )
    end
end

--This function configures a generic menu game object
--with the specifics chosen in character creation and
--sends you to the server nekkid.  This is done so that
--the players base config will not have any clothing
--that would prevent them from disrobing.
function putOnWizardsRobeAndHat()
	gCharBaseConfig = KEP_AddMenuGameObject( ACTOR_ID[ gGenderIndex ], IDLE_ANIM_ID )
	if gCharBaseConfig ~= 0 and gCharBaseConfig ~= nil then

		--Put move the render window offscreen, make 2 x 2 so you wont see
		--nekkid people if something messes up
		GetSet_SetVector( gCharBaseConfig, MenuGameIds.VP_POSITION, -10, -10, 0 )
		GetSet_SetVector( gCharBaseConfig, MenuGameIds.VP_DIMENSIONS, 2, 2, 0 )

		local faceGLID = getFaceByIndex()

		--Config the head --------------------------
		KEP_ArmMenuGameObject( gCharBaseConfig, faceGLID, 0 )
		KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, CONFIG_HEAD[ gGenderIndex ], 0, gCurSkinColor )
		KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, FACE_EYE_LEFT, 0, gCurEyeColor )
		KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, FACE_EYE_RIGHT, 0, gCurEyeColor )
		KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, FACE_EYEBROW, gCurEyebrow, gCurBeardColor )
		if isFemale() then
			KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, FACE_FACECOVER, 0, gCurFaceCover )
		else
			KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, FACE_FACECOVER, gCurFaceCover, gCurBeardColor )
		end

		--Config the eyes --------------------------
		KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, FACE_EYE_LEFT, 0, gCurEyeColor )
		KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, FACE_EYE_RIGHT, 0, gCurEyeColor )

		--Config the eyebrows ----------------------
		KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, FACE_EYEBROW, gCurEyebrow, gCurBeardColor )

		--Config the face cover --------------------
		if isFemale() then
			KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, FACE_FACECOVER, 0, gCurFaceCover )
		else
			KEP_MenuGameObjectSetEquippableConfig( gCharBaseConfig, faceGLID, FACE_FACECOVER, gCurFaceCover, gCurBeardColor )
		end

		--Config the hair --------------------------
		if not isFemale() then
			KEP_SetCharacterAltConfig( gCharBaseConfig, 25, gCurHairIndex, gCurHairColor )
			KEP_SetCharacterAltConfig( gCharBaseConfig, 26, gCurHairIndex, gCurHairColor )
		end
		KEP_SetCharacterAltConfig( gCharBaseConfig, 27, gCurHairIndex, gCurHairColor )
		KEP_SetCharacterAltConfig( gCharBaseConfig, 28, gCurHairIndex, gCurHairColor )
		KEP_SetCharacterAltConfig( gCharBaseConfig, 29, gCurHairIndex, gCurHairColor )
		KEP_SetCharacterAltConfig( gCharBaseConfig, 30, gCurHairIndex, gCurHairColor )
		KEP_SetCharacterAltConfig( gCharBaseConfig, 31, gCurHairIndex, gCurHairColor )

		--Config the skin tone ---------------------
		KEP_SetCharacterAltConfig( gCharBaseConfig, CONFIG_NECK[ gGenderIndex ],  0, gCurSkinColor )
		KEP_SetCharacterAltConfig( gCharBaseConfig, CONFIG_TORSO[ gGenderIndex ], 0, gCurSkinColor )
		KEP_SetCharacterAltConfig( gCharBaseConfig, CONFIG_HANDS[ gGenderIndex ], 0, gCurSkinColor )
		KEP_SetCharacterAltConfig( gCharBaseConfig, CONFIG_CROTCH[ gGenderIndex ], 0, gCurSkinColor )
		KEP_SetCharacterAltConfig( gCharBaseConfig, CONFIG_LEGS[ gGenderIndex ],  0, gCurSkinColor )
		KEP_SetCharacterAltConfig( gCharBaseConfig, CONFIG_FEET[ gGenderIndex ],  0, gCurSkinColor )
		if isFemale() then
			KEP_SetCharacterAltConfig( gCharBaseConfig, CONFIG_ARMS[ gGenderIndex ], 0, gCurSkinColor )
		else
			KEP_SetCharacterAltConfig( gCharBaseConfig, CONFIG_ARMS[ gGenderIndex ], 0, gCurSkinColor )
		end
	end
end

function getFaceByIndex( index )
	local faceIndex = gFaceIndex
	if index ~= nil and gCurViewportMode == VPMODE_FACE then
		faceIndex = index
	end
	if isFemale() then
		return FACES_FEMALE[ faceIndex ]
	else
		return FACES_MALE[ faceIndex ]
	end
end

function getFaceAssByIndex( index )
	local faceIndex = gFaceIndex
	if isFemale() then
		return FACES_FEMALE[ faceIndex ]
	else
		return FACES_MALE[ faceIndex ]
	end
end

function isFemale()
	if gGenderIndex == BODY_FEM_THIN or gGenderIndex == BODY_FEM_MED or gGenderIndex == BODY_FEM_BIG then
		return true
	end
	return false
end

function setupActor()
	gDirtyFlag = FLAGS_ALL_SET
end

function showControl( ctlName, enabled, visible )
	local c = Dialog_GetControl( gDialogHandle, ctlName )
	if visible == nil then
		Control_SetVisible( c, enabled )
	else
		Control_SetVisible( c, visible )
	end
	Control_SetEnabled( c, enabled )
end

function showControlList( list, enabled, visible )
	for i, v in ipairs( list ) do
		showControl( v, enabled, visible )
	end
end

function viewportButtonHandler( index )
	if gCurViewportMode	== VPMODE_HAIR then
		gCurHairIndex = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.HAIR )
	elseif gCurViewportMode	== VPMODE_HAIRCOLOR then
		gCurHairColor = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.HAIR )
	elseif gCurViewportMode	== VPMODE_EYECOLOR then
		gCurEyeColor = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.EYECOLOR )
	elseif gCurViewportMode	== VPMODE_SKINCOLOR then
		gCurSkinColor = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACE )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.BODY )
	elseif gCurViewportMode	== VPMODE_FACE then
		gFaceIndex = index
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACE )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACECOVER )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.BROW )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.EYECOLOR )
	elseif gCurViewportMode	== VPMODE_FACECOVER then
		gCurFaceCover = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACECOVER )
	elseif gCurViewportMode	== VPMODE_BEARDCOLOR then
		gCurBeardColor = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.FACECOVER )
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.BROW )
	elseif gCurViewportMode	== VPMODE_EYEBROW then
		gCurEyebrow = index - 1
		gDirtyFlag = BitFlag.set( gDirtyFlag, FLAGS.BROW )
	elseif gCurViewportMode	== VPMODE_BODYTYPE then
		if isFemale() then
			gGenderIndex = BODY_FEM_THIN + index - 1
		else
			gGenderIndex = BODY_MALE_THIN + index - 1
		end
		gDirtyFlag = FLAGS_ALL_SET
	end
end

function getViewportOptionLimits()
	local max, rem = NUM_VIEWPORTS, 0
	if gCurViewportMode	== VPMODE_HAIR then
		if isFemale() then
			max = #HAIR_COLORS_FEM
		else
			max = #HAIR_COLORS_MALE
		end
	elseif gCurViewportMode	== VPMODE_HAIRCOLOR then
		--Lua indexing from 1, so increment to get the right number of
		--hair colors from the table
		if isFemale() then
			max = HAIR_COLORS_FEM[ gCurHairIndex + 1] + 1
		else
			max = HAIR_COLORS_MALE[ gCurHairIndex + 1] + 1
		end
	elseif gCurViewportMode	== VPMODE_EYECOLOR then
		max = MAX_EYE_COLOR + 1
	elseif gCurViewportMode	== VPMODE_SKINCOLOR then
		max = MAX_SKIN_COLOR + 1
	elseif gCurViewportMode	== VPMODE_FACE then
		if isFemale() then
			max = #FACES_FEMALE
		else
			max = #FACES_MALE
		end
	elseif gCurViewportMode	== VPMODE_FACECOVER then
		if isFemale() then
			max = MAX_MAKEUP + 1
		else
			max = MAX_BEARD + 1
		end
	elseif gCurViewportMode	== VPMODE_BEARDCOLOR then
		if isFemale() then
			max = MAX_BROW_COLOR + 1
		else
			max = MAX_BEARD_COLOR + 1
		end
	elseif gCurViewportMode	== VPMODE_EYEBROW then
		if isFemale() then
			max = MAX_BROW_FEM + 1
		else
			max = MAX_BROW_MALE + 1
		end
	elseif gCurViewportMode	== VPMODE_BODYTYPE then
		max = 3
	else
		gViewportFirstIndex = 1
	end
	rem = math.fmod( max, NUM_VIEWPORTS )
	return max, rem
end

function resetViewportOptions()
	local max, rem = getViewportOptionLimits()
	showControl( "btnVPBack", false )
	showControl( "btnVPForward", max > NUM_VIEWPORTS )
	if gCurViewportMode	~= VPMODE_BODYTYPE then
		showControl( "lblWeight", false )
		showControl( "lblHeight", false )
		showControl( "sldScaleXZ", false )
		showControl( "sldScaleY", false )
		showControl( "lblHeightCurrent", false )
	end
	gViewportFirstIndex = 1
end

function transformVPActor( posX, posY, posZ, scaleX, scaleY, scaleZ, rotX, rotY, rotZ )
	if gViewport ~= nil then
		gCurVPpos 	= { x = posX, y = posY, z = posZ }
		gCurVPscale	= { x = scaleX, y = scaleY,	z = scaleZ }
		gCurVProt	= { x = rotX, y = rotY, z = rotZ }
		for index, viewport in pairs(gViewport) do
			local vpa = viewport.actor
			if vpa ~= nil and vpa ~= 0 then
				--This is a huge hack for meaty not being the same height as
				--the other male actors.  Lame.
				local mod = 0
				if (index == 3) and (not isFemale()) then
					mod = gBeefyMod
				end
				GetSet_SetVector( vpa, MenuGameIds.POSITION, posX, posY + mod, posZ )
				GetSet_SetVector( vpa, MenuGameIds.SCALING, scaleX, scaleY, scaleZ )
				GetSet_SetVector( vpa, MenuGameIds.ROTATION, rotX, rotY, rotZ )
			end
		end
	end
end

function setupMenuViewports()
	if gDialogHandle ~= nil then
		local max, rem = getViewportOptionLimits()
		local rowHeight = 0
		for i = 0, NUM_VIEWPORTS - 1 do
			local button = Dialog_GetControl( gDialogHandle, "btnViewport" .. tostring( i ) )
			local dialogX = Dialog_GetLocationX( gDialogHandle )
			local dialogY = Dialog_GetLocationY( gDialogHandle )
			if button ~= nil then
				if i + gViewportFirstIndex <= max then
					Control_SetEnabled( button, true )
				else
					Control_SetEnabled( button, false )
				end
			end
		end
	end
end

function cleanupMenuViewports()
	if gViewport ~= nil and gViewport ~= 0 then
		for index, viewport in pairs(gViewport) do
			local vpa = viewport.actor
			if vpa ~= nil and vpa ~= 0 then
				if (KEP_ObjectIsValid(vpa) == 1) then
					KEP_RemoveMenuGameObject( vpa )
				end
			end
		end
	end

	if gCharBaseConfig ~= nil and gCharBaseConfig ~= 0 then
		if (KEP_ObjectIsValid(gCharBaseConfig) == 1) then
			KEP_RemoveMenuGameObject( gCharBaseConfig )
		end
	end
	gViewportsClean = true
end

function configScale()
	local sliderXZ = Dialog_GetSlider( gDialogHandle, "sldScaleXZ" )
	local sliderY = Dialog_GetSlider( gDialogHandle, "sldScaleY" )
	Slider_SetValue(sliderY, gCurrentScaleY)
	Slider_SetValue(sliderXZ, gCurrentScaleXZ)
	sldScaleY_OnSliderValueChanged( sliderY )
	sldScaleXZ_OnSliderValueChanged( sliderXZ )
end

function configFace( player )
	local faceGLID = getFaceByIndex()
	KEP_ArmEquippableItem( player, faceGLID, gCurSkinColor )
	KEP_PlayerObjectSetEquippableConfig( player, faceGLID, CONFIG_HEAD[ gGenderIndex ], 0, gCurSkinColor )
end

function configEyeColor( player )
	local faceGLID = getFaceByIndex()
	KEP_PlayerObjectSetEquippableConfig( player, faceGLID, FACE_EYE_LEFT, 0, gCurEyeColor )
	KEP_PlayerObjectSetEquippableConfig( player, faceGLID, FACE_EYE_RIGHT, 0, gCurEyeColor )
end

function configEyebrow( player )
	local faceGLID = getFaceByIndex()
	KEP_PlayerObjectSetEquippableConfig( player, faceGLID, FACE_EYEBROW, gCurEyebrow, gCurBeardColor )
end

function configBeardAndMakeupColors( player )
	local faceGLID = getFaceByIndex()
	if isFemale() then
		KEP_PlayerObjectSetEquippableConfig( player, faceGLID, FACE_FACECOVER, 0, gCurFaceCover )
	else
		KEP_PlayerObjectSetEquippableConfig( player, faceGLID, FACE_FACECOVER, gCurFaceCover, gCurBeardColor )
	end
end

function configBody( oldPlayer )
	KEP_SetActorTypeOnPlayer( oldPlayer, ACTOR_ID[gGenderIndex])
	local player = KEP_GetPlayer( )
	configScale()
	KEP_SetCharacterAltConfigOnPlayer( player, CONFIG_TORSO[ gGenderIndex ], -1, gCurSkinColor )
	KEP_SetCharacterAltConfigOnPlayer( player, CONFIG_LEGS[ gGenderIndex ],  -1, gCurSkinColor )
	KEP_SetCharacterAltConfigOnPlayer( player, CONFIG_CROTCH[ gGenderIndex ], -1, gCurSkinColor )
	KEP_SetCharacterAltConfigOnPlayer( player, CONFIG_NECK[ gGenderIndex ],  -1, gCurSkinColor )
	KEP_SetCharacterAltConfigOnPlayer( player, CONFIG_HANDS[ gGenderIndex ], -1, gCurSkinColor )
	KEP_SetCharacterAltConfigOnPlayer( player, CONFIG_FEET[ gGenderIndex ],  -1, gCurSkinColor )
	if isFemale() then
		KEP_SetCharacterAltConfigOnPlayer( player, CONFIG_ARMS[ gGenderIndex ], -1, gCurSkinColor )
	else
		KEP_SetCharacterAltConfigOnPlayer( player, CONFIG_ARMS[ gGenderIndex ], -1, gCurSkinColor )
	end
end

function configHair( player )
	hairColor = gCurHairColor

	--Do this because not all hair styles (meshes) have the same amount of colors (materials)
	local colorIndex = 0
	if isFemale() then
		colorIndex = HAIR_COLORS_FEM[ gCurHairIndex + 1 ]
	else 
		colorIndex = HAIR_COLORS_MALE[ gCurHairIndex + 1 ]
	end

	if not colorIndex or hairColor > colorIndex then
		hairColor = 0
	end

	if not isFemale() then
		KEP_SetCharacterAltConfigOnPlayer( player, 25, gCurHairIndex, hairColor )
		KEP_SetCharacterAltConfigOnPlayer( player, 26, gCurHairIndex, hairColor )
	end
	KEP_SetCharacterAltConfigOnPlayer( player, 27, gCurHairIndex, hairColor )
	KEP_SetCharacterAltConfigOnPlayer( player, 28, gCurHairIndex, hairColor )
	KEP_SetCharacterAltConfigOnPlayer( player, 29, gCurHairIndex, hairColor )
	KEP_SetCharacterAltConfigOnPlayer( player, 30, gCurHairIndex, hairColor )
	KEP_SetCharacterAltConfigOnPlayer( player, 31, gCurHairIndex, hairColor )
end

--Because of lua indexing from 1 and c++ indexing from 0
--2 must be subracted from the addition of two 1 based indexes
function configViewportFace( vpa, index )

	local faceGLID = getFaceByIndex( index )
	if faceGLID~=nil then
		local skinColor = gCurSkinColor

		if gCurViewportMode	== VPMODE_SKINCOLOR then
			skinColor = index + gViewportFirstIndex - 2
		end
		KEP_ArmMenuGameObject( vpa, faceGLID, 0 )
		KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, CONFIG_HEAD[ gGenderIndex ], 0, skinColor )
		KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, FACE_EYE_LEFT, 0, gCurEyeColor )
		KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, FACE_EYE_RIGHT, 0, gCurEyeColor )
		KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, FACE_EYEBROW, gCurEyebrow, gCurBeardColor )
		if isFemale() then
			KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, FACE_FACECOVER, 0, gCurFaceCover )
		else
			KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, FACE_FACECOVER, gCurFaceCover, gCurBeardColor )
		end
	end
end

function configViewportEyeColor( vpa, index )
	local faceGLID = getFaceByIndex( index )
	local eyeColor = gCurEyeColor

	if gCurViewportMode	== VPMODE_EYECOLOR then
		eyeColor = index + gViewportFirstIndex - 2
	end
	KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, FACE_EYE_LEFT, 0, eyeColor )
	KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, FACE_EYE_RIGHT, 0, eyeColor )
end

function configViewportEyebrow( vpa, index )
	local faceGLID = getFaceByIndex( index )
	local eyebrow, beardColor = gCurEyebrow, gCurBeardColor

	if gCurViewportMode	== VPMODE_EYEBROW then
		eyebrow = index + gViewportFirstIndex - 2
	elseif gCurViewportMode == VPMODE_BEARDCOLOR then
		beardColor = index + gViewportFirstIndex - 2
	end
	KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, FACE_EYEBROW, eyebrow, beardColor )
end

function configViewportBeardAndMakeupColors( vpa, index )
	local faceGLID = getFaceByIndex( index )
	local faceCover, beardColor = gCurFaceCover , gCurBeardColor

	if gCurViewportMode	== VPMODE_FACECOVER then
		faceCover = index + gViewportFirstIndex - 2
	elseif gCurViewportMode == VPMODE_BEARDCOLOR then
		beardColor = index + gViewportFirstIndex - 2
	end
	if isFemale() then
		KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, FACE_FACECOVER, 0, faceCover )
	else
		KEP_MenuGameObjectSetEquippableConfig( vpa, faceGLID, FACE_FACECOVER, faceCover, beardColor )
	end
end

function configViewportBody( vpa, index )
	local skinColor = gCurSkinColor
	if gCurViewportMode	== VPMODE_SKINCOLOR then
		skinColor = index + gViewportFirstIndex - 2
	end

	KEP_SetCharacterAltConfig( vpa, CONFIG_NECK[ gGenderIndex ],  0, skinColor )

	if isFemale() then
		KEP_SetCharacterAltConfig( vpa, CONFIG_ARMS[ gGenderIndex ], 0, skinColor )
	else
		KEP_SetCharacterAltConfig( vpa, CONFIG_ARMS[ gGenderIndex ], 1, skinColor )
	end

	if gCurViewportMode	== VPMODE_SKINCOLOR then
		--you only see face and neck in window so dont bother changing the rest.
		return
	end

	KEP_SetCharacterAltConfig( vpa, CONFIG_TORSO[ gGenderIndex ], 1, skinColor )
	KEP_SetCharacterAltConfig( vpa, CONFIG_HANDS[ gGenderIndex ], 0, skinColor )
	KEP_SetCharacterAltConfig( vpa, CONFIG_CROTCH[ gGenderIndex ], 1, skinColor )
end

function configViewportHair( vpa, index )
	local hairIndex, hairColor = gCurHairIndex, gCurHairColor

	if gCurViewportMode	== VPMODE_EYECOLOR
	or gCurViewportMode == VPMODE_EYEBROW
	or ( gCurViewportMode == VPMODE_BEARDCOLOR and isFemale() ) then
		if isFemale() then
			hairIndex = HAIR_FOR_EYES_FEM
		else
			hairIndex = HAIR_FOR_EYES_MALE
		end
	elseif gCurViewportMode	== VPMODE_HAIR then
		hairIndex = index + gViewportFirstIndex - 2
	elseif gCurViewportMode	== VPMODE_HAIRCOLOR then
		hairColor = index + gViewportFirstIndex - 2
	end

	--Do this because not all hair styles (meshes) have the same amount of colors (materials)
	local colorIndex = HAIR_COLORS_MALE[ math.min(hairIndex + 1, #HAIR_COLORS_MALE ) ]
	if isFemale() then
		colorIndex = HAIR_COLORS_FEM[ math.min(hairIndex + 1, #HAIR_COLORS_FEM ) ]
	end

	if hairColor > colorIndex then
		hairColor = 0
	end

	if not isFemale() then
		KEP_SetCharacterAltConfig( vpa, 25, hairIndex, hairColor )
		KEP_SetCharacterAltConfig( vpa, 26, hairIndex, hairColor )
	end
	KEP_SetCharacterAltConfig( vpa, 27, hairIndex, hairColor )
	KEP_SetCharacterAltConfig( vpa, 28, hairIndex, hairColor )
	KEP_SetCharacterAltConfig( vpa, 29, hairIndex, hairColor )
	KEP_SetCharacterAltConfig( vpa, 30, hairIndex, hairColor )
	KEP_SetCharacterAltConfig( vpa, 31, hairIndex, hairColor )
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	if key == Keyboard.ESC then
		-- close this menu
		--CALL disableViewportUpdate FIRST or cleanupMenuViewports will destroy
		--the viewport objects Dialog_OnRender is trying to manipulate
		cancelUpdate()
	end
end

function cancelUpdate()

	--CALL disableViewportUpdate FIRST or cleanupMenuViewports will destroy
	--the viewport objects Dialog_OnRender is trying to manipulate
	gCancelUpdate = true
	disableViewportUpdate()
	cleanupMenuViewports()
	gGracefulCleanup = true
	setOriginalConfigs()
	gDirtyFlag = FLAGS_ALL_SET
	updateBodyParts()

	local player = KEP_GetPlayer( )
	if player~=nil then
		zone_index = KEP_GetCurrentZoneIndex()
		zone_index = InstanceId_GetId(zone_index)

		if zone_index == GameGlobals.CHARACTER_CREATION_ZI then
			local e = KEP_EventCreate( APT_SELECTION_EVENT )
			if e ~= 0 then
				KEP_EventSetFilter( e, FORCED_STARTING_APARTMENT )
				KEP_EventAddToServer( e )
				KEP_EventQueue( e )
				markGuideClosedInConfig()
			end

			-- hide menu to make sure no other buttons will be clicked while waiting for server response
			Dialog_SetMinimized( gDialogHandle, 1 )
			return
		end
	end

	MenuCloseThis()

	-- Rezone To Refresh Avatar, Zone & Menus
	Rezone()
end

function markGuideClosedInConfig()
	g_userName = KEP_GetLoginName()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetNumber(config, "GuideClosed"..g_userName, 0)
		KEP_ConfigSave( config )
	end
end

