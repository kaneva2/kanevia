--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\CAM.lua")
dofile("..\\Scripts\\GetAttributeType.lua")
dofile("BuildPlace.lua")
dofile("InventoryHelper.lua")
dofile("Framework_EventHelper.lua")

-- Enum menu tabs
local CAT_AVATAR = "avatar"
local CAT_BUILDING = "building"

-- Warning messages for Game Item Limit overflow -- TODO: Remove and move to Framework_AllGameItems.lua
local GI_LOAD_MESSAGE1 = "You have reached the limit of game objects in your world."
local GI_LOAD_MESSAGE2 = "Free some space by removing game objects."

-- Warning message for items you cannot use
local NON_OWNER_PLACE_MESSAGE		= "Objects can only be placed in worlds you own."
local PLAYER_MODE_PLACE_MESSAGE		= "You cannot place objects from your inventory while in Player Mode"
local WRONG_GENDER_WEAR_MESSAGE		= "This item cannot be worn by your gender."
local OWNER_JUMPSUIT_WEAR_MESSAGE	= "You cannot change your clothes when the jumpsuit is on."
local PLAYER_JUMPSUIT_WEAR_MESSAGE	= "You cannot change your clothes in this World."
local PLAYER_MODE_COPY_MESSAGE		= "You cannot copy objects from your inventory while in Player Mode"
local CLOTHING_ALLOWED_MESSAGE		= "Yellow outlines indicate what you are wearing now."

local CLICKED_CONTROLS = {"btnUse", "btnInfo", "btnChangeAnim", "btnDiscard", "btnCopy"}
local CATEGORIES = {avatar = "btnAvatar", building = "btnBuilding", gaming = "btnGaming"}

local SMALL_BUTTON_FONT = 9
local LARGE_BUTTON_FONT = 12
local BUTTON_NAME_LIMIT	= 8 -- #characters when to resize button name
local GREY_SEARCH_COLOR = {a = 255, r = 140, g = 140, b = 140}

local ITEMS_PER_PAGE = 20

local SUB_CATEGORIES = {
	avatar = {
		tabs = {
			btnTops 		= "Tops",
			btnBottoms 		= "Bottoms",
			btnShoes 		= "Shoes",
			btnCostumes 	= "Costumes",
			btnAccessories 	= "Accessories" 
		},

		list = {
			{name = "Emotes"},
			{name = "Underwear",		gender = "M"},
			{name = "Lingerie",			gender = "F"},
			{name = "Dresses &",		data = "Dresses & Skirts"},
			{name = "  Skirts",			data = "Dresses & Skirts"},
			{name = "Access Pass"},
			{name = " "},
			{name = "Storage"}
		}
	},

	building = {
		tabs = {
			btnMaterials    = "Building Materials",
			btnArchitecture = "Architecture",
			btnFurnishings  = "Furnishings",
			btnGarden       = "Home and Garden",
			btnElectronics  = "Electronics & Games" 
		},

		list = {
			{name = "Vehicles"},
			{name = "Statues"},
			{name = "Gifts"},
			{name = "Sound FX",		data = "Sound Effects"},
			{name = "Deeds"},
			{name = "Particles",	data = "Particle Effects"},
			{name = " "},
			{name = "Storage"}
		}
	}
}

NO_RESULTS_BUFFER 				= 30 -- Extra width for button around the length of the text
local m_shopCategoryLink		= "" -- Link to the subsection of the shop

local m_loading					= true --Loading the inventory?
local m_placedGameItem			= nil -- Game item that we're placing -- TODO: Remove and move to Framework_AllGameItems.lua

local m_YesNoQuestion 			= nil
local YES_NO_QUESTION_USE_DEED 	= 1
local m_pendingDeed 			= -1

g_t_items 						= {}
local m_pendingImages			= {}

local m_category 				= CAT_AVATAR
local m_subcategory 			= nil

local m_syncInventoryServer		= true

local m_timeStampOfLastPlacement= nil

gPlayerGender 					= ""
local m_advBuild				= false

g_iHasAccessPass 				= 0
g_iHasVIPPass 					= 0
g_iZoneHasAccessPass 			= 0
g_iZoneHasVIPPass 				= 0
g_frameworkEnabled				= false
g_playerModeActive				= false
g_anythingEquipped				= false
g_jumpsuitEnabled				= false

local m_clickedHeight			= 0

local FLASH_TIME 				= 0.15
local m_flashCount				= 0
local m_flashing				= false
local m_elapsedTime 			= 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "answerEventHandler", 					"YesNoAnswerEvent",					KEP.MED_PRIO )
	KEP_EventRegisterHandler( "attribEventHandler", 					"AttribEvent",						KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler",				"BrowserPageReadyEvent", 			KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ContentServiceCompletionHandler",		"ContentServiceCompletionEvent",	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "inventoryItemPlacedTimerEventHandler", 	"InventoryItemPlacedTimerEvent", 	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassInfoHandler", 			"ResponseAccessPassInfoEvent", 		KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassZoneInfoHandler", 		"ResponseAccessPassZoneInfoEvent", 	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "updateInventoryHandler",					"UpdateInventoryEvent", 			KEP.MED_PRIO )
	KEP_EventRegisterHandler( "returnFrameworkStateHandler",			"FrameworkState", 					KEP.MED_PRIO )
	KEP_EventRegisterHandler( "wokInventoryFilterEventHandler",			"WOK_INVENTORY_FILTER", 			KEP.HIGH_PRIO )
	-- Handle ScriptClientEvent events. NOTE: Only needed for Framework events. Would be nice to get other client menus 
	-- integrated with LibClient_Common. Consequences currently unknown
	KEP_EventRegisterHandler( "ScriptClientEventHandler", 		"ScriptClientEvent", 				KEP.MED_PRIO )
	
	-- DRF - Added
	KEP_EventRegisterHandler( "buildModeChangedEventHandler", "BuildModeChangedEvent", KEP.MED_PRIO )
	
	KEP_EventRegisterHandler( "updateWOKInventoryHandler", "InventoryUpdateEvent", KEP.HIGH_PRIO)

	InventoryHelper.registerInventoryHandlers()
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( DISPLAY_ACTION_ITEM_ERRORS_EVENT, 	KEP.HIGH_PRIO )
	KEP_EventRegister( KEP.WORLD_FAME_EVENT, 				KEP.MED_PRIO )
	KEP_EventRegister( "BrowserPageReadyEvent",				KEP.MED_PRIO )
	KEP_EventRegister( "ChangeInventoryTabEvent",			KEP.MED_PRIO )
	KEP_EventRegister( "ContentServiceCompletionEvent", 	KEP.MED_PRIO )
	KEP_EventRegister( "DynamicObjectChangeEvent", 			KEP.MED_PRIO )
	KEP_EventRegister( "InventoryItemPlacedTimerEvent", 	KEP.MED_PRIO )
	KEP_EventRegister( "SelectedItemInfoEvent", 			KEP.MED_PRIO )
	KEP_EventRegister( "SendGLIDEvent",			 			KEP.MED_PRIO )
	KEP_EventRegister( "UpdateInventoryEvent",				KEP.MED_PRIO )
	KEP_EventRegister( "YesNoAnswerEvent", 					KEP.MED_PRIO )
	KEP_EventRegister( "YesNoBoxEvent", 					KEP.MED_PRIO )
	KEP_EventRegister( "FRAMEWORK_GAME_ITEM_LOAD_MESSAGE",  KEP.MED_PRIO ) -- Informs Framework_GameItemLoadWarning of what to display on Game Item load overflow
	InventoryHelper.registerInventoryEvents()
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate(dialogHandle)

	gPlayerGender	= KEP_GetLoginGender()

	--get the player's passes
	KEP_EventCreateAndQueue( "RequestAccessPassInfoEvent" )

	--get passes for current zone
	KEP_EventCreateAndQueue( "RequestAccessPassZoneInfoEvent" )

	--Is this in advanced build? 
	local config = KEP_ConfigOpenWOK()
	if config ~= nil and config ~= 0 then
		local advBuildOK, advBuildON =  KEP_ConfigGetNumber( config, "AdvBuild", 0)
		m_advBuild = advBuildON == 1
	end 

	-- Is the Framework active in this world? Let's find out
	local event = KEP_EventCreate("GetFrameworkState")
	KEP_EventQueue(event)

	m_clickedHeight = Control_GetHeight(gHandles["imgClicked"])

	InventoryHelper.initializeInventory("g_t_items")
	InventoryHelper.initializeHover("it_use_type", USE_TYPE.ACTION_ITEM)
	InventoryHelper.initializeClicked(CLICKED_CONTROLS)
	InventoryHelper.initializePageNumbers()
	InventoryHelper.initializeCategories(CATEGORIES, CAT_AVATAR)

	EditBox_SetTextColor( gHandles["txtSearch"], GREY_SEARCH_COLOR )

	requestInventory(false)
end

function responseAccessPassInfoHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iHasAccessPass 	= Event_DecodeNumber(event)
	g_iHasVIPPass 		= Event_DecodeNumber(event)
end

function responseAccessPassZoneInfoHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iZoneHasAccessPass 	= Event_DecodeNumber(event)
	g_iZoneHasVIPPass 		= Event_DecodeNumber(event)
end

function Dialog_OnRender(dialogHandle, fElapsedTime)

	m_elapsedTime = m_elapsedTime + fElapsedTime
	if m_flashing and m_flashCount < 3 then
		if m_elapsedTime >  FLASH_TIME  * 2 then
			Control_SetVisible(gHandles["imgItemBroken"], true)
			m_elapsedTime = 0
			m_flashCount = m_flashCount + 1
		elseif m_elapsedTime > FLASH_TIME then
			Control_SetVisible(gHandles["imgItemBroken"], false)
		end
	else
		if m_elapsedTime > FLASH_TIME then
			m_flashing = false
			m_flashCount = 0
			Control_SetVisible(gHandles["imgItemBroken"], false)
		end
	end
end
----------------------------------------------------------------------
-- Page Updating
----------------------------------------------------------------------
function updateWOKInventoryHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	requestInventory(true)
end

function inventoryUpdated(page, search, category, updatedFeature)
	requestInventory(true)
end

function updateInventoryHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	InventoryHelper.clearClicked()
	requestInventory(true)
end

-- Handles the return for the current Framework state
function returnFrameworkStateHandler( dispatcher, fromNetid, event, eventid, filter, objectid )

	local frameworkState = cjson.decode(Event_DecodeString(event))
	if frameworkState.enabled ~= nil then
		g_frameworkEnabled = (frameworkState.enabled == true)
	end

	-- Player mode set
	if frameworkState.playerMode ~= nil then
		g_playerModeActive = frameworkState.playerMode == "Player"		
	end

	if frameworkState.anythingEquipped ~= nil then
		g_anythingEquipped = (frameworkState.anythingEquipped == true)
	end

	if frameworkState.jumpsuit ~= nil then
		g_jumpsuitEnabled = frameworkState.jumpsuit
	end

	handleBorderColor()
end

-- Handles the return for the unified inventory filter event
function wokInventoryFilterEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local filterString = Event_DecodeString(event)

	m_category = CAT_AVATAR
	m_subcategory = nil

	if string.match(filterString, ",") then
		m_category, m_subcategory = string.match(filterString, "([^,]+),([^,]+)")
	else
		m_category = filterString
	end

	if m_category == CAT_AVATAR then
		Control_SetEnabled(gHandles["btnCustomizeAvatar"], true)
		Control_SetVisible(gHandles["btnCustomizeAvatar"], true)
	else
		Control_SetEnabled(gHandles["btnCustomizeAvatar"], false)
		Control_SetVisible(gHandles["btnCustomizeAvatar"], false)
	end
	
	setShopCategoryLink(m_subcategory)
	clearPage(true)
	requestInventory(false)
end

function requestInventory(asynchronous)
	m_loading = true
	clearPage()

	if (m_syncInventoryServer == true) then
		m_syncInventoryServer = false
		KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
		if (asynchronous) then
			getInventoryFromWeb()
		end
	else
		getInventoryFromWeb()
	end
end

function getInventoryFromWeb()
	local category = m_category or CAT_AVATAR
	local webSubcategory, gameItemTypes = nil, nil	

	if m_subcategory and m_subcategory ~= ""  and m_subcategory ~= "Access Pass" then
		webSubcategory = getWebCategory(KEP_UrlEncode(m_subcategory))
		webSubcategory = string.gsub(webSubcategory, "&", "%26")
	elseif m_subcategory == "Access Pass" then
		webSubcategory = "&mature=true"
	end

	getPlayerInventory(WF.PLAYER_INVENTORY, InventoryHelper.page, InventoryHelper.itemsPerPage, category, InventoryHelper.searchText, webSubcategory, useType, gameItemTypes)

	handleBorderColor()
end

function buildModeChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	g_build_enabled = filter
	Log("buildModeChangedEventHandler: buildEnabled="..tostring(g_build_enabled))
end

function attribEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	-- Updated Storage/armed item - request sync	
	if (filter == AttribEvent.GENERIC_TYPE or filter == AttribEvent.ARMITEM_TYPE) then
		m_syncInventoryServer = true
		requestInventory(false)
	
	-- Just synced, retrieve from web to update the page
	elseif (filter == AttribEvent.PLAYER_INVEN) then
		getInventoryFromWeb()
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.PLAYER_INVENTORY then
		local xmlData = KEP_EventDecodeString( event )

		-- Test to make sure valid results and results are up to date (for multiple fast calls)
		local result = string.match(xmlData, "<ReturnCode>(.-)</ReturnCode>")
		local passthrough = tonumber( string.match(xmlData,"<Passthrough>(.-)</Passthrough>") or -1 )
		if result ~= "0" or passthrough ~= cf_inventoryPassthrough then
			return
		end

		-- Update the pages
		local totalItems = string.match(xmlData, "<TotalNumberRecords>(%d-)</TotalNumberRecords>")
		InventoryHelper.setMaxPages(totalItems)

		togglePageButtons(tonumber(totalItems) > ITEMS_PER_PAGE)

		g_t_items = {}

		decodeInventoryItemsFromWeb(g_t_items, xmlData, false)

		m_loading = false
		
		populateItems()

		--return KEP.EPR_CONSUMED
	end
end

function togglePageButtons(toggle)
	Control_SetVisible(gHandles["stcPage"], toggle)
	Control_SetVisible(gHandles["btnBack"], toggle)
	Control_SetVisible(gHandles["btnNext"], toggle)
end

function populateItems()
	m_pendingImages = InventoryHelper.setIcons({"bundle_global_id", "default_glid", "GLID"}, "SmartObj_ScriptIcon_82x82.tga")

	-- Finish loading any images in ContentServiceCompletionHandler
	InventoryHelper.setNameplates("name")
	InventoryHelper.setHightlights("is_armed")

	setNoResultsControls(not m_loading and #g_t_items == 0)
end

function ContentServiceCompletionHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter ~= 2) then return end
	local ignored = KEP_EventDecodeNumber(event)
	local glid    = KEP_EventDecodeNumber(event)
	if (glid ~= nil) then
		local iconImage = m_pendingImages[glid]
		if (iconImage ~= nil) then
			local texturePath = KEP_AddItemThumbnailToDatabase(glid)
			Element_AddExternalTexture(iconImage, gDialogHandle, texturePath, "loading.dds")
			Element_SetCoords(iconImage, 0, 0, -1, -1)
			m_pendingImages[glid] = nil
		end
	end
end

function clearPage(resetPages)
	if resetPages then
		InventoryHelper.page = 1
		InventoryHelper.pageMax = 1
		InventoryHelper.updatePageDisplay()
	end
	InventoryHelper.clearClicked()
	g_t_items = {}
	populateItems()
end

function UrlDecode(str)
    if (str) then
		str = string.gsub (str, "+", " ")
		str = string.gsub (str, "%%(%x%x)", function(h) return string.char(tonumber(h,16)) end)
		str = string.gsub (str, "\r\n", "\n")
	end
    return str
end

function setShopCategoryLink(encodedCategoryName)

	encodedCategoryName = encodedCategoryName or ""

	-- Handle special cases where extra data is needed
	if encodedCategoryName == "Tops" or encodedCategoryName == "Bottoms" or encodedCategoryName == "Shoes" or encodedCategoryName == "Costumes" or encodedCategoryName == "Accessories" then
		if gPlayerGender == "M" then
			encodedCategoryName = encodedCategoryName .. "&pcname=For Men"
		else
			encodedCategoryName = encodedCategoryName .. "&pcname=For Women"
		end
	elseif encodedCategoryName == "Emotes" then
		if gPlayerGender == "M" then
			encodedCategoryName = "Emotes For Men"
		else
			encodedCategoryName = "Emotes For Women"
		end
	elseif encodedCategoryName == "Deeds" then
		encodedCategoryName = "Zones"
	elseif encodedCategoryName == "Gifts" then
		encodedCategoryName = "Gifts %26 Collectables"
	elseif encodedCategoryName == "Electronics & Games" then
		encodedCategoryName = "Electronics %26 Games"
	elseif encodedCategoryName == "Dresses & Skirts" then
		encodedCategoryName = "Dresses %26 Skirts"
	elseif encodedCategoryName == "Particle Effects" then
		encodedCategoryName = "Special Effects"
	end

	-- Unless we've chosen AP, which isn't a category, 
	if encodedCategoryName == "" or encodedCategoryName == "Access Pass" or encodedCategoryName == "Zones" then
		m_shopCategoryLink = GameGlobals.WEB_SITE_PREFIX_SHOP
	else
		m_shopCategoryLink = GameGlobals.WEB_SITE_PREFIX_SHOP .. CATALOG_LANDING_SUFFIX .. "cName=" .. encodedCategoryName
	end
end

function setNoResultsControls(showNoResults)
	local stcCtrl = gHandles["stcNoSearch"]
	local btnCtrl = gHandles["btnNoSearch"]
	local btnText = "Browse Shop"
	if m_subcategory and InventoryHelper.searchText and InventoryHelper.searchText ~= "" then
		Static_SetText(stcCtrl, "No Results found in " .. UrlDecode(tostring(m_subcategory)) .. ".")
		btnText = "Search All"
	elseif m_subcategory then
		Static_SetText(stcCtrl, "Category is Empty.")

		Control_SetEnabled(gHandles["btnNoSearch"], true)
		Control_SetVisible(gHandles["btnNoSearch"], true)
		btnText = "Browse " .. UrlDecode(tostring(m_subcategory)) .. " in Shop"
	else
		Static_SetText(stcCtrl, "No Results Found.")
	end
	
	local width = Static_GetStringWidth(stcCtrl, btnText) + NO_RESULTS_BUFFER
	local xLoc = (Dialog_GetWidth(gDialogHandle) - width)/2
	Control_SetSize(btnCtrl, width, Control_GetHeight(btnCtrl))
	Control_SetLocationX(btnCtrl, xLoc)
	Static_SetText(Button_GetStatic(btnCtrl), btnText)
	Control_SetVisible(stcCtrl, showNoResults)
	Control_SetVisible(btnCtrl, showNoResults)
	if showNoResults then
		Dialog_MoveControlToFront(gDialogHandle, stcCtrl)
		Dialog_MoveControlToFront(gDialogHandle, btnCtrl)
	else
		Dialog_MoveControlToBack(gDialogHandle, stcCtrl)
		Dialog_MoveControlToBack(gDialogHandle, btnCtrl)
	end
end

function btnNoSearch_OnButtonClicked(btnHandle)
	if m_subcategory and InventoryHelper.searchText and InventoryHelper.searchText ~= "" then
	else
		launchWebBrowser(m_shopCategoryLink)
		MenuCloseThis()
		MenuClose("UnifiedInventory.xml")
	end
end

function btnStorage_OnButtonClicked(btnHandle)
	MenuOpen("Storage.xml")
end

function btnCustomizeAvatar_OnButtonClicked(btnHandle)
	MenuOpen("CharacterCreation3.xml")
	local ev = KEP_EventCreate("FRAMEWORK_INIT_CHAR_CREATOR")
	KEP_EventEncodeString(ev, "player")
	KEP_EventQueue(ev)
end

----------------------------------------------------------------------
-- Subcategories
----------------------------------------------------------------------

function getWebCategory(subcat)
	local webCat = subcat
	local webOverride = SUB_CATEGORIES[InventoryHelper.category]["web"]
	if webOverride and webOverride[subcat] then
		webCat = webOverride[subcat]
	end
	return webCat
end

----------------------------------------------------------------------
-- Clicked/Use
----------------------------------------------------------------------
function useItem(itemIndex)
	local updatePage = false
	
	local item = g_t_items[itemIndex]
	local useType = item["it_use_type"]

	local useTypePlacing = useType ~= USE_TYPE.NONE and useType ~= USE_TYPE.EQUIP and useType ~= USE_TYPE.ANIMATION	
	displayText = prohibitClothingChange(false)

	-- Not in world you own and trying to place.
	if useTypePlacing then
		local canPlace = not prohibitPlayerBuilding(true)
		if not canPlace then return end
	end

	if item.GLID and item.GLID >= 0 then
		if useType ~= USE_TYPE.NONE and useType ~= USE_TYPE.EQUIP then

			--Deed
			if useType==USE_TYPE.OLD_SCRIPT or useType==USE_TYPE.DEED or useType==USE_TYPE.CUSTOM_DEED then
				m_YesNoQuestion = YES_NO_QUESTION_USE_DEED
				m_pendingDeed = item

				local body = "By applying this deed, you will lose the current layout and all items here will be moved to your inventory.\n\nDo you want to continue?"
				KEP_YesNoBox( body, "Warning", WF.INVENTORY, 0, false, -1, -1, false, false)
			
			--Animation
			elseif useType==USE_TYPE.ANIMATION then
				ChangeAnimation( item.GLID )
			
			else

				--Particle
				if useType==USE_TYPE.PARTICLE then
					-- Attempting to apply an item to an existing object
					local selCount = KEP_GetNumSelected( )
					if selCount==0 then
						KEP_MessageBox("Please left click to select a target 3D object")
						return
					end
					
					local obj = ObjectSelected(1)
					Log("USE_TYPE_PARTICLE - glid="..item.GLID.." "..ObjectToStr(obj.id, obj.type).." subType="..item.inventory_sub_type)
					KEP_UseInventoryItemOnTarget(item.GLID, obj.type, obj.id, item.inventory_sub_type)

				-- Dynamic Object
				else
					enterBuildMode()
					markItemButtonPlaced()
					placeObjectInFrontOfPlayer(item.GLID, item.inventory_sub_type, useType)
				end
			end

			sendDynamicObjectChangeEvent() -- drf - tell DynamicObjectList to refresh
		
		-- Clothes - Remove/Wear
		elseif not (displayText ~= nil and displayText ~= CLOTHING_ALLOWED_MESSAGE) then
			if item["is_armed"] then
				KEP_DisarmInventoryItem( item.GLID)
				item["is_armed"] = false
			else
				if item["actor_group"]==0 or 
				(item["actor_group"]==1 and gPlayerGender=="M") or 
				(item["actor_group"]==2 and gPlayerGender=="F") then
					KEP_ArmInventoryItem( item.GLID)
					item["is_armed"] = true
					log("--- KEP_SendChatMessage 1 ".. tostring(item["actor_group"]))
				else
					--KEP_SendChatMessage( ChatType.System, WRONG_GENDER_WEAR_MESSAGE )
					log("--- KEP_SendChatMessage 2")
					local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
					KEP_EventEncodeNumber(statusEvent, 0)
					KEP_EventEncodeString(statusEvent, WRONG_GENDER_WEAR_MESSAGE)
					KEP_EventEncodeNumber(statusEvent, 3)
					KEP_EventEncodeNumber(statusEvent, 255)
					KEP_EventEncodeNumber(statusEvent, 0)
					KEP_EventEncodeNumber(statusEvent, 0)
					KEP_EventSetFilter(statusEvent, 4)
					KEP_EventQueue(statusEvent)
				end
			end
			if g_anythingEquipped then
				Framework.sendEvent("WOK_ARM_ITEM", {GLID = item.GLID})
			end
		end
	end
	
	if updatePage then
		m_syncInventoryServer = true
		requestInventory(false)
	end

	--Dialog_ClearFocus(gDialogHandle)
end

function handleBorderColor()
	local displayText = nil
	local showRed = false
	--local showBlue = false

	if m_category == CAT_AVATAR then
		displayText = prohibitClothingChange(false)
	elseif m_category == CAT_BUILDING then
		displayText = prohibitPlayerBuilding(false)
	end

	showRed = (displayText ~= nil and displayText ~= CLOTHING_ALLOWED_MESSAGE)

	if displayText then
		Static_SetText(gHandles["stcWarningText"], displayText)

		Control_SetVisible(gHandles["stcWarningText"], true)
	else
		--Control_SetVisible(gHandles["stcStorage"], true)
		Control_SetVisible(gHandles["btnStorage"], true)
		Control_SetEnabled(gHandles["btnStorage"], true)
		Control_SetVisible(gHandles["imgStorage"], true)
		Control_SetVisible(gHandles["stcWarningText"], false)
	end

	--Control_SetVisible(gHandles["stcWarningText"], showRed)
	Control_SetVisible(gHandles["imgTopBarRed"], showRed)
	--Control_SetVisible(gHandles["imgTopBarBlue"], showBlue)
end

-- Is the player allowed to change clothes? Create messages based on whether jumpsuit is enabled and owner
function prohibitClothingChange(displayMessage)
	local warningMessage = nil

	if g_frameworkEnabled and g_playerModeActive and g_jumpsuitEnabled then
		if KEP_IsOwnerOrModerator() then
			warningMessage = OWNER_JUMPSUIT_WEAR_MESSAGE
			else
			warningMessage = PLAYER_JUMPSUIT_WEAR_MESSAGE
			end

		if displayMessage then
			KEP_MessageBox(warningMessage)
		end
	else
		warningMessage = CLOTHING_ALLOWED_MESSAGE
	end

	return warningMessage
end

-- Is the player allowed to change clothes? Create messages based on whether jumpsuit is enabled and owner
function prohibitPlayerBuilding(displayMessage)
	local warningMessage = nil

	-- Not in world you own and trying to place.
	if not KEP_IsOwnerOrModerator() then
		
		warningMessage = NON_OWNER_PLACE_MESSAGE		
	-- Trying to place from Inventory while in player mode
	--elseif KEP_IsOwnerOrModerator() then --and g_playerModeActive then
		--warningMessage = PLAYER_MODE_PLACE_MESSAGE
	end
	
	if warningMessage and displayMessage then
		KEP_MessageBox(warningMessage)
	end

	return warningMessage
end

function markItemButtonPlaced()
	m_timeStampOfLastPlacement = KEP_CurrentTime()
	local event = KEP_EventCreate("InventoryItemPlacedTimerEvent")
	KEP_EventEncodeNumber(event, m_timeStampOfLastPlacement)
	KEP_EventQueueInFuture( event, 1)
	Control_SetEnabled(gHandles["btnUse"], false)
	Static_SetText(gHandles["btnUse"], "Placed!")
end

function clearItemButtonPlaced()
	m_timeStampOfLastPlacement = KEP_CurrentTime()
	Control_SetEnabled(gHandles["btnUse"], true)
	Static_SetText(gHandles["btnUse"], "Place")
end

function inventoryItemPlacedTimerEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local eventTimestamp = KEP_EventDecodeNumber(event)
	if eventTimestamp == m_timeStampOfLastPlacement then
		clearItemButtonPlaced()
	end
end

function enterBuildMode()
	local ev = KEP_EventCreate("EnterBuildModeEvent")
	KEP_EventSetFilter(ev, 1)
	KEP_EventEncodeNumber(ev, MODE_MOVE) -- Don't change mode
	KEP_EventQueue(ev)
	g_playerModeActive = false
	if MenuIsOpen("Framework_Toolbar.xml") then 
		KEP_ClearSelection() 
		local event = KEP_EventCreate("FRAMEWORK_SET_PLAYER_MODE")
		KEP_EventEncodeString(event, "God")
		KEP_EventQueue(event) -- NOTE: Handled in Framework_PlayerHandler
	end 
end

function sendDynamicObjectChangeEvent()
	Log("sendDynamicObjectChangeEvent")
	KEP_EventCreateAndQueue("DynamicObjectChangeEvent")
end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter ~= WF.INVENTORY) then return end

	local answer = KEP_EventDecodeNumber( event )
	Log("answerEventHandler: answer="..answer)
	if answer == 0 then return end

	local useItem = ( 
		(m_YesNoQuestion == YES_NO_QUESTION_USE_DEED) and 
		((m_pendingDeed["it_use_type"]==USE_TYPE.DEED) 
		or (m_pendingDeed["it_use_type"]==USE_TYPE.CUSTOM_DEED) 
		or (m_pendingDeed["it_use_type"]==USE_TYPE.OLD_SCRIPT))
	)
	if (useItem) then
		KEP_UseInventoryItem(m_pendingDeed.GLID, m_pendingDeed.inventory_sub_type)
	end
end

function itemClicked(itemIndex)
	local item = g_t_items[itemIndex]
		
	--Set Animation Button
	local showAnim = item["it_use_type"] == USE_TYPE.EQUIP and item["is_armed"] and item["GLID"] > 3000000 
	Control_SetVisible(gHandles["btnChangeAnim"], showAnim)
	
	--Set Use Button
	clearItemButtonPlaced()

	local useText = "Wear"

	resetCollar()

	if item["it_use_type"]~=USE_TYPE.NONE and item["it_use_type"]~=USE_TYPE.EQUIP then
		if 	item["it_use_type"] == USE_TYPE.ANIMATION or 
			item["it_use_type"] == USE_TYPE.OLD_SCRIPT or 
			item["it_use_type"] == USE_TYPE.DEED or 
			item["it_use_type"] == USE_TYPE.CUSTOM_DEED or 
			item["it_use_type"] == USE_TYPE.PARTICLE then
				useText = "Use"
		else
			useText = "Place"
		end	
		Control_SetEnabled(gHandles["btnDiscard"], item["is_default"] ~= 1)
	elseif item["is_armed"] then
		useText = "Remove"
		Control_SetEnabled(gHandles["btnDiscard"], false)
	else
		Control_SetEnabled(gHandles["btnDiscard"], item["is_default"] ~= 1)
	end

	Static_SetText(gHandles["btnUse"], useText)
	if string.len(useText) > BUTTON_NAME_LIMIT then
		changeButtonFont(gHandles["btnUse"], SMALL_BUTTON_FONT)
	else
		changeButtonFont(gHandles["btnUse"], LARGE_BUTTON_FONT)
	end

	if m_category == CAT_BUILDING then
		Control_SetEnabled(gHandles["btnUse"], KEP_IsOwnerOrModerator())
		Control_SetVisible(gHandles["btnUse"], KEP_IsOwnerOrModerator())
		Control_SetVisible(gHandles["imgClicked"], KEP_IsOwnerOrModerator())
		Control_SetEnabled(gHandles["btnInfo"], KEP_IsOwnerOrModerator())
		Control_SetVisible(gHandles["btnInfo"], KEP_IsOwnerOrModerator())
		Control_SetEnabled(gHandles["btnDiscard"], KEP_IsOwnerOrModerator())
		Control_SetVisible(gHandles["btnDiscard"], KEP_IsOwnerOrModerator())

		m_flashing = not KEP_IsOwnerOrModerator()
		Control_SetLocationY(gHandles["imgItemBroken"], Control_GetLocationY(gHandles["imgClicked"]) + 5)
		Control_SetLocationX(gHandles["imgItemBroken"], Control_GetLocationX(gHandles["imgClicked"]) + 6)
		Control_SetVisible(gHandles["imgItemBroken"], not KEP_IsOwnerOrModerator())
	end
	
end

--Reset from gaming tab
function resetCollar()
	Control_SetVisible(gHandles["btnCopy"], false)
	if Control_GetHeight(gHandles["imgClicked"]) > m_clickedHeight then
		resizeCollarHeight(-1 * Control_GetHeight(gHandles["btnCopy"]))
	end
end

function resizeCollarHeight(amount)
	local clickedCollar = gHandles["imgClicked"]
	Control_SetSize(clickedCollar, Control_GetWidth(clickedCollar), Control_GetHeight(clickedCollar) + amount)

	-- Ignore the first control (btnUse)
	for i = 2, #CLICKED_CONTROLS do
		local handle = gHandles[CLICKED_CONTROLS[i]]
		Control_SetLocationY(handle, Control_GetLocationY(handle) + amount)
	end
end

function btnUse_OnButtonClicked( buttonHandle )
	useItem(InventoryHelper.selectedIndex)
end

function btnCopy_OnButtonClicked( buttonHandle )
	if KEP_IsOwnerOrModerator() then --and not g_playerModeActive then
		-- Create event for the Framework to Copy to Game
		local event = {item = g_t_items[InventoryHelper.selectedIndex].metadata,
					   mode = "AddToGame"}

		-- Pass event down to the Framework
		Framework.sendEvent("WOK_ADD_TO_GAME", event)

		--Dialog_ClearFocus(gDialogHandle)
	else
		-- Warn users
		KEP_MessageBox(PLAYER_MODE_COPY_MESSAGE)
	end
end

function btnInfo_OnButtonClicked( buttonHandle )

	if MenuIsClosed("InventoryInfo.xml") then
		local handle = MenuOpenModal("InventoryInfo.xml")
		item = g_t_items[InventoryHelper.selectedIndex]

		local ev = KEP_EventCreate("SendGLIDEvent")
		KEP_EventEncodeNumber(ev, item.GIGLID or item.GLID)
		KEP_EventEncodeNumber(ev, item.inventory_sub_type)
		KEP_EventEncodeString(ev, item.display_name or item.name)
		KEP_EventEncodeString(ev, item.description or "")
		KEP_EventEncodeString(ev, item.creator or "")
		KEP_EventEncodeString(ev, "") -- Menu sent param

		if item.existsOnShop ~= nil then
			KEP_EventEncodeString(ev, tostring(item.existsOnShop))
		end

		KEP_EventQueue(ev)
	end

	--Dialog_ClearFocus(gDialogHandle)
end

function btnChangeAnim_OnButtonClicked( buttonHandle )

	local item = g_t_items[InventoryHelper.selectedIndex]
	if item["it_use_type"] == USE_TYPE.EQUIP and item["is_armed"] then
		MenuOpenModal("ChooseAnimation.xml")
		local ev = KEP_EventCreate("SelectedItemInfoEvent")
		KEP_EventEncodeNumber( ev, item.inventory_sub_type)
		KEP_EventEncodeNumber( ev, 1) -- num items
		KEP_EventEncodeNumber( ev, item["GLID"])
		KEP_EventEncodeNumber( ev, 0) -- placement ID not needed
		KEP_EventEncodeNumber( ev, 1) --quantity of item
		KEP_EventEncodeString( ev, item["name"] ) -- name
		KEP_EventEncodeNumber( ev, item["sell_price"] ) -- price
		KEP_EventQueue( ev )
	else
		--KEP_SendChatMessage( ChatType.System, "Can only change the animation of equipped accessory items" )
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, "Can only change the animation of equipped accessory items")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
	end

	--Dialog_ClearFocus(gDialogHandle)
end

function btnDiscard_OnButtonClicked( buttonHandle )

	if KEP_IsWorld() then
		KEP_MessageBox("Function currently unavailable in Developer Worlds.")
		return
	end

	if MenuIsClosed("StorageConfirm.xml") then
		local handle = MenuOpenModal("StorageConfirm.xml")
		item = g_t_items[InventoryHelper.selectedIndex]
		local ev = KEP_EventCreate("SendGLIDEvent")
		KEP_EventEncodeNumber(ev, item.GLID)
		KEP_EventEncodeNumber(ev, item.inventory_sub_type)
		KEP_EventQueue( ev)
	end

	--Dialog_ClearFocus(gDialogHandle)
end

function imgClicked_OnLButtonDblClk(btnHandle, x, y)
	local controlContainsPt = Control_ContainsPoint(gHandles[("btnItem" .. InventoryHelper.selectedIndex)], x, y)
	if InventoryHelper.selectedIndex and controlContainsPt then
		useItem(InventoryHelper.selectedIndex)
	end
end

----------------------------------------------------------------------
-- Misc. Buttons
----------------------------------------------------------------------

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	InventoryHelper.onButtonDown(x, y)
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
end

function btnShop_OnButtonClicked( buttonHandle )
	if Control_GetVisible(buttonHandle) then
		MenuClose("UnifiedInventory.xml")
		MenuCloseThis()
		launchWebBrowser(GameGlobals.WEB_SITE_PREFIX_SHOP)
	end
end

----------------------------------------------------------------------
-- Framework Events
----------------------------------------------------------------------
-- Handles events from the server NOTE: Currently only needed for Framework events
function ScriptClientEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- ScriptClientEvent
	if filter == 4 then
		-- All ScriptClientEvents with a filter of 4 will have the number of arguments encoded into the
		-- event as its first number
        local args = Event_DecodeNumber(event)
        if args == 2 then
            local eventType = Event_DecodeString(event)
			
			-- A Game Item has successfully been placed
			if eventType == "FRAMEWORK_PLACE_GAME_ITEM_CONFIRMATION" and m_placedGameItem then
				local eventTable = cjson.decode(Event_DecodeString(event))
				
				-- Placement failed
				if not eventTable.success then
					-- Open the GameItemLoadWarning menu and pass it a message
					if MenuIsClosed("Framework_GameItemLoadWarning.xml") then
						MenuOpen("Framework_GameItemLoadWarning.xml")
					end
					local ev = KEP_EventCreate("FRAMEWORK_GAME_ITEM_LOAD_MESSAGE")
					KEP_EventEncodeString(ev, GI_LOAD_MESSAGE1)
					KEP_EventEncodeString(ev, GI_LOAD_MESSAGE2)
					KEP_EventQueue(ev)
				-- Placement success
				end

				-- Reset placed game item
				m_placedGameItem = nil
			end
		end
	end
end

----------------------------------------------------------------------
-- Destroy Functions
----------------------------------------------------------------------
function Dialog_OnDestroy(dialogHandle)
	InventoryHelper.destroy()
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	InventoryHelper.OnKeyDown(key)
end