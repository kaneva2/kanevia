--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\ActionItemFunctions.lua")

g_assetType = nil
g_scriptPath = nil
g_errorString = nil

g_errors = {}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "errorsEventHandler", DISPLAY_ACTION_ITEM_ERRORS_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( DISPLAY_ACTION_ITEM_ERRORS_EVENT, KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnEdit_OnButtonClicked(buttonHandle)
	KEP_ShellOpen(g_scriptPath)
end

function btnTry_OnButtonClicked(buttonHandle)
	g_errorString = ActionItemFunctions.errorCheck(g_assetType, g_scriptPath)
	if not g_errorString then
		MenuCloseThis()
	else
		populateErrors()
	end
end

function errorsEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_errorString = KEP_EventDecodeString( event )
	g_scriptPath = KEP_EventDecodeString( event )
	g_assetType = KEP_EventDecodeNumber( event )
	populateErrors()
end

function populateErrors()
	ListBox_RemoveAllItems(gHandles["lstErrors"])
	g_errors = separateToTable(g_errorString)
	g_errors = separateBySize(g_errors)
	for i=1, #g_errors do
		ListBox_AddItem(gHandles["lstErrors"], g_errors[i], i)
	end
end

--Separate Error String into a table by errors
function separateToTable(errorStr)
	local errorList = {}
	for line in string.gmatch(errorStr, "(.-)\n") do
		table.insert(errorList, line)
	end
	return errorList
end

--Separate by size
function separateBySize(errorTable)
	local ctrlHandle = gHandles["stcSizing"]
	local ctrlHeight = Control_GetHeight(ctrlHandle)

	local i = 1
	while i <= #errorTable do
		local errorStr = errorTable[i]
		local s, e, firstWord = string.find(errorStr, "(.-) ")
		if not firstWord then
			firstWord = errorStr
		end

		if firstWord ~= "" then
			local line = ""
			local pos = 1

			--If the first word is longer than the string, divide up by character; otherwise by word
			if Static_GetStringHeight(ctrlHandle, firstWord) > ctrlHeight then
				while Static_GetStringHeight(ctrlHandle, line) < ctrlHeight do
					line = line .. string.sub(firstWord,pos,pos)
					pos = pos + 1
				end

			else
				local strHeight = Static_GetStringHeight(ctrlHandle, errorStr)
				if strHeight > ctrlHeight then
					local s, e, word = 0, 0, ""
					local strLen = string.len(errorStr)

					--Until the string height is too large, add new words to the line
					repeat
						line = line .. word
						pos = e + 1

						s, e, word = string.find(errorStr, "(.-) ", pos)
						if not word then
							word = string.sub(errorStr, pos)
						end
					until (Static_GetStringHeight(ctrlHandle, line .. word) > ctrlHeight) or (pos > strLen)
				end
			end

			--If position has changed, remove the old line, add the new one, and make the line after the remaining
			if pos > 1 then
				table.remove(errorTable, i)
				table.insert(errorTable, i, line)
				table.insert(errorTable, i+1, string.sub(errorStr,pos))
			end

			i = i+1

		else
			break
		end
	end

	return errorTable
end
