--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

-- Views Enumeration
g_viewUsers = 0
g_viewSystems = 1
g_viewBoth = 2
g_view = g_viewBoth -- current view

g_tabStr = "<t><t>"

-- Color Constants
g_colorRed = "ff7f0000"
g_colorYellow = "ff7f7f00"
g_colorGreen = "ff007f00"
g_colorBlue = "ff00007f"

g_userName = "<nil>"
g_userId = 0

g_altList = nil

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "MemberAltsEventHandler", "MemberAltsEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "YesNoBoxHandler", "YesNoAnswerEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- This Menu Only Allowed In DevMode As GM!
	local devMode = ToBool(KEP_DevMode())
	local isGM = ToBool(KEP_IsGM())
	if (not devMode or not isGM) then
		MenuCloseThis()
	end

	-- DRF - Only Enable SystemServiceEvent Link In Non-Dev Worlds
	local showSSE = ToBool(KEP_IsCommunity())
	setControlVisible("btnSSE", showSSE)

	-- Alt ListBox Uses HTML
	ListBox_SetHTMLEnabled(gHandles["lbAlts"], ToBOOL(true))
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function xmlStrToAltList(xml)

	-- Get Users List
	local xmlUsers = string.match(xml, "<Users>(.-)</Users>") or ""
	local userNameList = xmlStrToList(xmlUsers, g_tabStr)
	
	-- Get Systems List
	local xmlSystems = string.match(xml, "<Systems>(.-)</Systems>") or ""
	local systemIdList = xmlStrToList(xmlSystems, g_tabStr)
	
	-- Get Tree List
	local xmlTree = string.match(xml, "<Tree>(.-)</Tree>") or ""
	local treeList = xmlStrToList(xmlTree, g_tabStr)

	-- Alt List Is All Of These
	local altList = TableConcat(TableConcat(userNameList, systemIdList), treeList)
	return altList
end

function AltListLineFormat(alt)
	altFmt = alt or ""

	-- Remove System Id (GUID)
	altFmt = string.gsub(altFmt, " %x+[-]%x+[-]%x+[-]%x+[-]%x+ ", " ")

	-- Elaborate Windows Version (6.1.2345)
	altFmt = string.gsub(altFmt, " 5%.%d+.%d+ ", " XP ")
	altFmt = string.gsub(altFmt, " 6%.0%.%d+ ", " Vista ")
	altFmt = string.gsub(altFmt, " 6%.1%.%d+ ", " Win7 ")
	altFmt = string.gsub(altFmt, " 6%.2%.%d+ ", " Win8 ")
	altFmt = string.gsub(altFmt, " 6%.3%.%d+ ", " Win8.1 ")
	altFmt = string.gsub(altFmt, " 10%.%d+%.%d+ ", " Win10 ")

	-- Color Code Status
	if (string.find(altFmt, " Permanent ")) then
		altFmt = "<font color="..g_colorRed..">"..altFmt.."</font>"
	end
	if (string.find(alt, " Days ")) then
		altFmt = "<font color="..g_colorYellow..">"..altFmt.."</font>"
	end
	if (string.find(alt, " Online ")) then
		altFmt = "<font color="..g_colorGreen..">"..altFmt.."</font>"
	end

	return altFmt
end

function AltListUpdate(xml)

	-- Convert XML String To Alt List
	g_altList = xmlStrToAltList(xml)

	-- Update Alt ListBox With Alt List
	removeAllListBoxItems("lbAlts")
	for i, alt in ipairs(g_altList) do
		local altFmt = AltListLineFormat(alt)
		addListBoxItem("lbAlts", altFmt, 0)
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter ~= WF.MEMBER_ALTS then return end
	
	-- Alt List Update
	local xml = KEP_EventDecodeString(event)
	Log("BrowserPageReadyHandler: ...\n"..xml)
	AltListUpdate(xml)
end

function WebGetMemberAlts()
	makeWebCall(WEBCALL_GET_PLAYER_INFO.."alts&userName="..g_userName.."&view="..g_view, WF.MEMBER_ALTS)
end

function MemberAltsEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Decode Event (userId is optional, only for display)
	g_userName = KEP_EventDecodeString(event) or "<nil>"
	g_userId = KEP_EventDecodeNumber(event) or 0
	Log("MemberAltsEventHandler: userId="..g_userId.." userName="..g_userName)
	
	-- Display UserName (userId)
	setStaticText("stcUserName", "User [ "..g_userName.." ]")
	
	-- Get Member Alts
	WebGetMemberAlts()
end

function rbUsers_OnRadioButtonChanged(rbHandle)
	local checked = CheckBox_GetChecked(RadioButton_GetCheckBox(rbHandle))
	if checked then
		g_view = g_viewUsers
	end
	WebGetMemberAlts()
end

function rbSystems_OnRadioButtonChanged(rbHandle)
	local checked = CheckBox_GetChecked(RadioButton_GetCheckBox(rbHandle))
	if checked then
		g_view = g_viewSystems
	end
	WebGetMemberAlts()
end

function rbBoth_OnRadioButtonChanged(rbHandle)
	local checked = CheckBox_GetChecked(RadioButton_GetCheckBox(rbHandle))
	if checked then
		g_view = g_viewBoth
	end
	WebGetMemberAlts()
end

function btnSSE_OnButtonClicked( buttonHandle )
	KEP_YesNoBox("Send System Service Event?\n"..g_userName, "Confirm", WF.CONFIRM_SYSTEM_SERVICE)
end

function YesNoBoxHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter ~= WF.CONFIRM_SYSTEM_SERVICE then return end
	local answer = KEP_EventDecodeNumber( event )	-- 1: Yes, 0 = No
	if (g_userName and (answer == 1)) then
		LogWarn("Calling SendSystemServiceEvent("..g_userName..")")
		KEP_SendSystemServiceEvent(g_userName)
	end
end

function lbAlts_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	
	-- Mouse Selections Only
	if (ToBool(bFromKeyboard)) then return end
	
	-- Get Selected Alt List Text
	local altListText = g_altList[sel_index + 1] or ""
	
	-- If 'User [ <userName> ... ]' Show Alts
	local userName = string.match(altListText, "User %[ (%S+)")
	if (userName) then 
		SendMemberAltsEvent(userName)
	end

	-- If 'System [ sysName sysVer <systemId> ... Online ]' Send System Service To Force Crash Report
	--g_systemIdSSE = string.match(altListText, " (%x+[-]%x+[-]%x+[-]%x+[-]%x+) ")
	--if (g_systemIdSSE) then
	--	if (string.find(altListText, " Online ")) then
	--		KEP_YesNoBox("Send System Service Event?\n"..g_systemIdSSE, "Confirm", WF.CONFIRM_SYSTEM_SERVICE)
	--	end
	--end
end

function Dialog_OnKeyDown(dialogHandle, key)
	
	-- A = My MemberAlts
	if key == Keyboard.A then
		SendMemberAltsEvent(GetUserName(), GetUserId())
	end
end
