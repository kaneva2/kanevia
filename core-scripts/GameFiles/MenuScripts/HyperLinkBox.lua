--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

g_url = nil

HYPER_LINK_BOX_EVENT = "HyperLinkBoxEvent"

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "HyperLinkBoxEventHandler", HYPER_LINK_BOX_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( HYPER_LINK_BOX_EVENT, KEP.MED_PRIO )
end

function HyperLinkBoxEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	message = KEP_EventDecodeString( event ) or "No Message"
	buttontext = KEP_EventDecodeString( event ) or "OK"
	local url = KEP_EventDecodeString( event ) or "www.kaneva.com"
	sh = Dialog_GetStatic( gDialogHandle, "lblMessage" )
	Static_SetText( sh, message )
	bs = Button_GetStatic(Dialog_GetButton(gDialogHandle, "btnLink" ))
	Static_SetText(bs, buttontext)
	MenuSetMinimizedThis(false)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnLink_OnButtonClicked( buttonHandle)
	if g_url ~= nil then
		KEP_LaunchBrowser( g_url )
	end
	MenuCloseThis()
end
