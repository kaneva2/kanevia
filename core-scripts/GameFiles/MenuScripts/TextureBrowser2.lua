--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--TextureBrowser2_ab117: Updated Patterns menu (9/24/2013)
dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\Selection.lua")
dofile("..\\MenuScripts\\Lib_Vecmath.lua")

local Math = _G.Math

g_changeBuildModeEvent = true

WEB_ADDRESS = GameGlobals.WEB_SITE_PREFIX.."kgp/textureslist.aspx?"
WEB_ADDRESS_FEATURED = GameGlobals.WEB_SITE_PREFIX.."kgp/textureslist.aspx?type=f&channelId=" .. GameGlobals.FEATURED_CHANNEL
WEB_ADDRESS_SEARCH = GameGlobals.WEB_SITE_PREFIX.."kgp/textureslist.aspx?type=s"
WEB_ADDRESS_PL = GameGlobals.WEB_SITE_PREFIX.."kgp/playList.aspx?"
WEB_ADDRESS_CATEGORIES = GameGlobals.WEB_SITE_PREFIX.."kgp/textureslist.aspx?type=c"

TEXTURES_PER_PAGE = 16

COPIED_PATTERN_TIME = 2 -- How long the "Pattern Copied" shows on the menu

TAB_KANEVA = 1
TAB_MY = 2
TAB_PLAYLISTS = 3
TAB_PICTURES = 4

--Modes
MODE_MOVE 		= 1
MODE_ROTATE 	= 2
MODE_TEXTURES 	= 3

FILTER_NAMES = {"", "red", "blue", "yellow", "green", "brown"}

MAIN = 1
PLAYLISTS = 2
CATEGORIES = 3

CATEGORY_LIST = {}
CATEGORY_LIST[MAIN] = 
{
	"Featured",
	"All Patterns",
	"My Patterns",
	"My Pictures"
}

CATEGORY_LIST[CATEGORIES] = 
{
	"Abstract",
	"Architecture",
	"Collection",
	"Fabric",
	"Ground",
	"Industrial",
	"Landscape",
	"Metal",
	"Neon",
	"Object",
	"Other",
	"Plants",
	"Plastic",
	"Rock",
	"Sky",
	"Water",
	"Wood"
}

CATEGORY_LIST[PLAYLISTS]= {}

OCCUPIED_ANIMATION_GLID = 4612968
MIN_INTERACT_DISTANCE = 6
MIN_INTERACT_DISTANCE_SQ = MIN_INTERACT_DISTANCE * MIN_INTERACT_DISTANCE

g_objIds = {} -- Not sure what this is used for yet, may have to do with copy/paste so saving for now

g_t_textures = {}
g_selectedTexture = 0
g_curTextureList = 0

g_onTab = TAB_MY

g_categoryName = ""

g_filter = 1

g_clearSearch = true
g_searching = false
g_searchString = ""

g_curPage = 1
g_pages = 1

g_copied = false 
g_time = 0
g_endTime = true

g_playerMode = "God" -- Assume god mode unless told otherwise
g_playerLocOnOpen = {} -- Player position when the menu is opened

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "textureSelectionEventHandler", "TextureSelectionEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler( "buildModeChangedEventHandler", "BuildModeChangedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "changeBuildModeEventHandler", "ChangeBuildModeEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "enableApplyEventHandler", "EnableApplyEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "disableApplyEventHandler", "DisableApplyEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "enableResetEventHandler", "EnableResetEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "disableResetEventHandler", "DisableResetEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "frameworkStateHandler", "FRAMEWORK_RETURN_STATE", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "updateWeaponDurabilityHandler", "UPDATE_WEAPON_DURABILITY", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "applyTextureEventHandler", "ApplyTextureEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( "EnableApplyEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "DisableApplyEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "EnableResetEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "DisableResetEvent", KEP.HIGH_PRIO )
    KEP_EventRegister( "UPDATE_WEAPON_DURABILITY", KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- DRF - Do Not Open Texture Menu While In MenuEditor
	if (MenuIsOpen("MenuEditor.xml") or MenuIsOpen("BlackScreen.xml")) then
		LogWarn("MenuEditor Is Open - Closing...")
		g_changeBuildModeEvent = false
		MenuCloseThis()
		return
	end

	-- Create the thumbnail functions
	for i = 1, TEXTURES_PER_PAGE do
		_G["btnThumb" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			local n = string.match(Control_GetName(btnHandle), "(%d+)")
			selectTexture(tonumber(n))
		end
	end
	
	-- Disable Apply Button
	enableApplyButton(false)

	-- Update Reset Button
	updateResetButton()

	getMyPlaylists()

	populateDropdown()
	
	g_onTab = TAB_KANEVA
	
	changePage()
	
	Control_SetVisible(gHandles["btnBlank"],true)

	g_playerLocOnOpen = {KEP_GetPlayerPosition()}

	-- Get framework state
	local event = KEP_EventCreate("FRAMEWORK_REQUEST_STATE")
	KEP_EventSetFilter(event, 4)
	KEP_EventQueue(event)

end

function Dialog_OnDestroy(dialogHandle)

	-- If in player mode (using the paintbrush)
	if g_playerMode == "Player" then
		-- Deselect everything
		KEP_SetCurrentAnimationByGLID(0) -- stop painting animation
		KEP_ClearSelection()
	end

	-- DRF - Added
	if (g_changeBuildModeEvent == true) then
		local ev = KEP_EventCreate("ChangeBuildModeEvent")
		KEP_EventSetFilter(ev, MODE_MOVE)
		KEP_EventQueue( ev)
	end

	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- Controls how long Pattern copied & Paste appears 
	if g_copied == true and g_endTime == false then
		if g_time < COPIED_PATTERN_TIME then
			if g_time <= 0 then
				showPatternCopied(true)
			end
			g_time = g_time +fElapsedTime
		else 
			showPatternCopied(false)
		end
	end 

	-- If painting, play painting animation. Close menu when move too far away.
	if g_playerMode == "Player" then
		local playerLoc = {KEP_GetPlayerPosition()}

		if g_playerLocOnOpen == nil then
			g_playerLocOnOpen = playerLoc
		end

		if Math.getArrayDistanceSq(playerLoc, g_playerLocOnOpen) > MIN_INTERACT_DISTANCE_SQ then
			MenuCloseThis()
		else
			KEP_SetCurrentAnimationByGLID(OCCUPIED_ANIMATION_GLID)
		end
	end
end


function Dialog_OnMouseMove(dialogHandle, x, y)

	-- Update tooltips based on texture name/author
	for i = 1,#g_t_textures do 
		if Control_ContainsPoint(gHandles["btnThumb" .. i], x, y) ~= 0 and Control_GetVisible(gHandles["btnThumb" .. i]) == 1 then 
			Control_SetToolTip(gHandles["btnThumb"..i],g_t_textures[i]["name"].." by "..g_t_textures[i]["username"])
		end
		if Control_ContainsPoint(gHandles["imgThumbFrame" .. i], x, y) ~= 0 and Control_GetVisible(gHandles["imgThumbFrame" .. i]) == 1 then 
			Control_SetToolTip(gHandles["imgThumbFrame" .. i],g_t_textures[i]["name"].." by "..g_t_textures[i]["username"])
		end 
	end 

	-- Highlight/unhighlight the Pagination on hover
	local edPage = gHandles["edPage"]
	if Control_GetFocus(edPage) ~= 1 then
		if Control_ContainsPoint(edPage, x, y) == 1 then
			Control_SetEnabled(edPage, true)
			EditBox_SetText(edPage, "Enter #", false)
		elseif Control_GetEnabled(edPage) == 1 then
			Control_SetEnabled(edPage, false)
			setPageText()
		end
	end
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	
	-- If clicking outside the categories dropdown, hide it
	if Control_ContainsPoint(gHandles["lstCategories"], x, y) == 0 and Control_ContainsPoint(gHandles["btnCategories"], x, y) == 0 then
		Control_SetVisible(gHandles["lstCategories"], false)
	end

	-- If the edit box has focus, and you've clicked outside the search area, clear focus on the dialog so that the x shows properly
	if Control_GetFocus(gHandles["eBoxSearch"]) == 1 and Control_ContainsPoint(gHandles["eBoxSearch"], x, y) == 0 and Control_ContainsPoint(gHandles["btnSearch"], x, y) == 0 then
		MenuClearFocusThis()
	end		

	-- See Pagination functions - selecting pagination box. It is usually disabled to have to do here.
	if Control_ContainsPoint(gHandles["edPage"], x, y) == 1 then
		selectPageBox()
	end
end

-----------------------------------------------------------------------------------------
-- Web calls, Event Handlers, Parsing
-----------------------------------------------------------------------------------------

function getMyPlaylists()
	makeWebCall(WEB_ADDRESS_PL, WF.PLAYLISTS, 1, 25)
end

function getPlaylistPatterns(playlistId, g_curPage)
	clear()
	local url = WEB_ADDRESS_PL .. "type=tpl&pid=" .. playlistId .. "&assetTypeId=6&"
	makeWebCall(url, WF.PLAYLIST_PATTERNS, g_curPage, TEXTURES_PER_PAGE)
end

function changePage()
	clear()
	resetFrames()
	setPageText()

	if g_searchString ~= "" then
		if g_onTab == TAB_MY then
			makeWebCall(WEB_ADDRESS_SEARCH .. "&channelId=-1&ss=" .. g_searchString, WF.TEXTURE_LIST, g_curPage, TEXTURES_PER_PAGE)
		elseif g_onTab == TAB_PICTURES then
			makeWebCall(WEB_ADDRESS_SEARCH .. "&channelId=-1&ss=" .. g_searchString .. "&wantPics=true", WF.TEXTURE_LIST, g_curPage, TEXTURES_PER_PAGE)
		elseif g_onTab == TAB_PLAYLISTS then 
			makeWebCall(WEB_ADDRESS_SEARCH .. "&channelId=-1&ss=" .. g_searchString .. "&pId=" .. g_curTextureList, WF.TEXTURE_LIST, g_curPage, TEXTURES_PER_PAGE)
		else
			makeWebCall(WEB_ADDRESS_SEARCH .. "&channelId=" .. GameGlobals.FEATURED_CHANNEL .. "&ss=" .. g_searchString, WF.TEXTURE_LIST, g_curPage, TEXTURES_PER_PAGE)
		end

	-- If g_filter is enabled, and not set to 1
	elseif g_filter and g_filter ~= 1 then 
		makeWebCall(WEB_ADDRESS_SEARCH .. "&channelId=" .. GameGlobals.FEATURED_CHANNEL .. "&ss=".. FILTER_NAMES[g_filter], WF.TEXTURE_LIST, g_curPage, TEXTURES_PER_PAGE)
	elseif g_onTab == TAB_MY then
		makeWebCall(WEB_ADDRESS, WF.TEXTURE_LIST, g_curPage, TEXTURES_PER_PAGE)
	elseif g_onTab == TAB_PICTURES then
		makeWebCall(WEB_ADDRESS .. "wantPics=true", WF.TEXTURE_LIST, g_curPage, TEXTURES_PER_PAGE)
	elseif g_onTab == TAB_KANEVA then
		makeWebCall(WEB_ADDRESS_FEATURED, WF.TEXTURE_LIST, g_curPage, TEXTURES_PER_PAGE)
	elseif g_onTab == TAB_PLAYLISTS then 
		getPlaylistPatterns(g_curTextureList, g_curPage)
	else
		makeWebCall(WEB_ADDRESS_CATEGORIES .. "&category="..g_categoryName, WF.TEXTURE_LIST, g_curPage, TEXTURES_PER_PAGE)
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.TEXTURE_LIST  or filter == WF.PLAYLIST_PATTERNS then
		local xmlData = KEP_EventDecodeString( event )
		ParseTextureData(xmlData)
		--To textureDownloadHandler
		return KEP.EPR_CONSUMED
	elseif filter == WF.PLAYLISTS then
		g_message = KEP_EventDecodeString( event )
		ParsePlaylistData(g_message)
		return KEP.EPR_CONSUMED
	end
end

-- Handles the return for the current Framework state
function frameworkStateHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local frameworkState = cjson.decode(Event_DecodeString(event))		
	g_playerMode = frameworkState.playerMode or "God"
	if g_playerMode == "Player" then	
		updateResetButton()
	end
end

function ParseTextureData(xmlData)

	local texType = "" 
	local result = string.match(xmlData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then

		local numRec = calculateRanges(xmlData) -- number of records in this chunk

		g_t_textures = {}
		
		if string.find(xmlData, "<PlayList>(.-)</PlayList>") ~= nil then
			texType = "<PlayList>(.-)</PlayList>"
		else
			texType = "<Texture>.-</Texture>"
		end 
	
		for imagestr in string.gmatch(xmlData, texType) do

			local name = string.match(imagestr, "<name>(.-)</name>")												-- name of the asset
			local username = string.match(imagestr, "<username>(.-)</username>")									-- creator's username
			if texType == "<PlayList>(.-)</PlayList>" then 
				local file_size = 1
			else
				local file_size = string.match(imagestr, "<file_size>([0-9]+)</file_size>")				-- file size of the image
			end 
			local content_extension = string.match(imagestr, "<content_extension>(.-)</content_extension>")			-- the content extension of the image
			local asset_id = string.match(imagestr, "<asset_id>([0-9]+)</asset_id>")								-- what we are really looking for
			local created_date, created_time = string.match(imagestr, "<created_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)")-- Date this image was created
			local imageUrl = string.match(imagestr, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>")
			local imageFullURL = string.match(imagestr, "<image_full_path>(.-)</image_full_path>")
			
			local tex = {}
			tex["name"] = name
			tex["username"] = username
			tex["file_size"] = file_size
			tex["content_extension"] = content_extension
			tex["asset_id"] = tonumber(asset_id)
			tex["created_date"] = created_date
			tex["full_image_url"] = imageFullURL
			local full_path = KEP_DownloadTextureAsync( GameGlobals.CUSTOM_TEXTURE, asset_id, imageUrl, GameGlobals.THUMBNAIL )

			-- Strip off begnning of file path leaving only file name
			tex["thumbnailFile"] = string.gsub(full_path,".-\\", "" )

			table.insert(g_t_textures, tex)
		end
	end
end

function ParsePlaylistData(xmlData)
	
	local result = string.match(xmlData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		local numRec = calculateRanges(xmlData) -- number of records in this chunk
		local g_playlist_count = 1
		for playliststr in string.gmatch(xmlData, "<PlayList>(.-)</PlayList>") do
			local name = string.match(playliststr, "<name>(.-)</name>")
			local asset_group_id = string.match(playliststr, "<asset_group_id>([0-9]+)</asset_group_id>")
			local asset_count = string.match(playliststr, "<asset_count>([0-9]+)</asset_count>")
			
			local list = {}
			list["playlistString"] = playliststr
			list["name"] = name
			list["groupID"] = tonumber(asset_group_id)
			list["assetCount"] = tonumber(asset_count)
			
			table.insert(CATEGORY_LIST[PLAYLISTS],g_playlist_count,list)
			g_playlist_count = g_playlist_count+1
			populateDropdown()
		end
	end

	g_shift = SHIFT_DN
end

function textureDownloadHandler()
	showThumbnails()
end

function applyTextureEventHandler()
	-- If in player mode (using the paintbrush)
	if g_playerMode == "Player" then
		-- Decrement paintbrush durability
		local event = KEP_EventCreate("UPDATE_WEAPON_DURABILITY")
		KEP_EventSetFilter(event, 4)
		KEP_EventQueue(event)
	end
end

function textureSelectionEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local count = KEP_EventDecodeNumber( event ) or 0
	for i = 1, count do
		g_objIds[i] = KEP_EventDecodeNumber( event ) or 0
	end
end

function buildModeChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	g_build_enabled = filter
	if g_build_enabled ~= 1 then
		g_changeBuildModeEvent = false
		MenuCloseThis()
	end
	return KEP.EPR_OK
end

function changeBuildModeEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	g_currentMode = filter
	if g_currentMode ~= MODE_TEXTURES then
		g_changeBuildModeEvent = false
		MenuCloseThis()
	end
end

function enableApplyEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if g_selectedTexture > 0 or g_copied then
		enableApplyButton(true)
	end
end

function disableApplyEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	enableApplyButton(false)
end 

function enableResetEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	enableResetButton(true)
end

function disableResetEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	enableResetButton(false)
end 

function updateWeaponDurabilityHandler()
	MenuCloseThis()
end

-----------------------------------------------------------------------------------------
-- Thumbnails/Frames
-----------------------------------------------------------------------------------------
function showThumbnails()
	clear()
	
	if #g_t_textures > 0 then 
		showNoneSearched(false)

		for i = 1, #g_t_textures do
			ih = gHandles["imgThumb" .. i]
			ihBack = gHandles["btnThumb" .. i]
			fileName = "../../CustomTexture/".. g_t_textures[i]["thumbnailFile"]
			scaleImage(ih, ihBack, fileName)
			setTextureThumbnails(i, true)
		end
		
		if #g_t_textures < TEXTURES_PER_PAGE then
			for i=#g_t_textures+1, TEXTURES_PER_PAGE do 
				setTextureThumbnails(i, false)
			end 
		end
	else
		for i= 1,TEXTURES_PER_PAGE do
			setTextureThumbnails(i, false)
		end
		
		resetFrames()
		showNoneSearched(true)
	end 
end

function setTextureThumbnails(index,enabled)
	Control_SetVisible(gHandles["imgThumbBG" .. index],	enabled)
	Control_SetVisible(gHandles["btnThumb" .. index],	enabled)
	Control_SetVisible(gHandles["imgThumb" .. index],	enabled)
	Control_SetVisible(gHandles["imgOutline" .. index],	enabled)
end 

function clear()
	for i = 1, TEXTURES_PER_PAGE do
		setTextureThumbnails(i, false)

		-- Reset the size and location of the image
		local ih = gHandles["imgThumb"..i]
		local ihBack = gHandles["btnThumb"..i]
		local xCoord = Control_GetLocationX(ihBack)
		local yCoord = Control_GetLocationY(ihBack)
		local width = Control_GetWidth(ihBack)
		local height = Control_GetHeight(ihBack)
		Control_SetLocation(ih, xCoord, yCoord)
		Control_SetSize(ih, width, height)
		element = Image_GetDisplayElement(ih)
		Element_AddTexture(element, gDialogHandle, "patterns2_assets(5).tga")
		Element_SetCoords(element, 147, 265,226,321)
	end
end

function selectTexture(index)
	g_selectedTexture = index

	if index > 0 and index <= #g_t_textures then
		imgSelected = gHandles["imgSelectedTexture"]
		imgSelectedBack = gHandles["imgSelectedTextureBack"]
		fileName = "../../CustomTexture/".. g_t_textures[index]["thumbnailFile"]

		enableApplyButton(true)
		
		Control_SetVisible(gHandles["btnBlank"],false)
		
		scaleImage(imgSelected,imgSelectedBack,fileName)
		
		resetFrames()
		Control_SetVisible(gHandles["imgThumbFrame" .. index], true)

		showPatternCopied(false)
		g_copied = false
		Static_SetText(gHandles["btnApply"], "Apply")
		
		Control_SetVisible(gHandles["btnClipboardLabel"], false)
	end
end

function resetFrames()
	for frames= 1, TEXTURES_PER_PAGE do
		Control_SetVisible(gHandles["imgThumbFrame" .. frames], false)
	end
end

-----------------------------------------------------------------------------------------
-- Pagination
-----------------------------------------------------------------------------------------
function btnPrev_OnButtonClicked( buttonHandle )
	if g_curPage > 1 then
		g_curPage = g_curPage - 1	
	else
		g_curPage = 1 --Just in case...
	end
	changePage()
end

function btnNext_OnButtonClicked( buttonHandle )
	if g_curPage < g_pages then
		g_curPage = g_curPage + 1
	else
		g_curPage = g_pages --Just in case...
	end
	changePage()
end

function edPage_OnFocusOut( buttonHandle )
	Control_SetEnabled(buttonHandle, false)
	setPageText()
end

function edPage_OnEditBoxString(editBoxHandle, someString)
	g_curPage = tonumber(someString) or g_curPage
	if g_curPage < 1 then
		g_curPage = 1
	elseif g_curPage > g_pages then
		g_curPage = g_pages
	end
	changePage()
end

function setPageText()
	local pageText = "Page " .. g_curPage
	if g_curPage > 99 then
		pageText = "P. " .. g_curPage
	end
	EditBox_SetText(gHandles["edPage"], pageText, false)
end

function selectPageBox()
	local editBox = gHandles["edPage"]
	Control_SetEnabled(editBox, true)
	EditBox_ClearText(editBox)
	Control_SetFocus(editBox)
end

function calculateRanges(xmlData)
	local totalNumRec = string.match(xmlData, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
	local numRec = string.match(xmlData, "<NumberRecords>([0-9]+)</NumberRecords>")
	totalNumRec = tonumber(totalNumRec)
	g_pages = math.ceil(totalNumRec / TEXTURES_PER_PAGE)
	pagingButtonState()
	return numRec
end

function pagingButtonState()
	Control_SetEnabled(gHandles["btnNext"], g_curPage < g_pages)
	Control_SetEnabled(gHandles["btnPrev"], g_curPage > 1)
	setPageText()
end

-----------------------------------------------------------------------------------------
-- Categories
-----------------------------------------------------------------------------------------
function btnCategories_OnButtonClicked(btnHandle)
	local lstCategories = gHandles["lstCategories"]
	local visible = (Control_GetVisible(lstCategories) == 1)
	
	-- Set to opposite of what it is
	Control_SetVisible(lstCategories, not visible)
	
	-- Only set focus if we are setting to visible
	if not visible then
		Control_SetFocus(lstCategories)
	end
end

function lstCategories_OnListBoxSelection(lstHandle)
	local sel_text = ListBox_GetSelectedItemText(lstHandle)
	
	-- Reset page/search/filter
	g_curPage = 1
	clearSearch()
	updateFilter(1)

	selectCategory(sel_text)

	Control_SetVisible(lstHandle, false)

	changePage()
end

function selectCategory(categoryName)
	local playlistMatch = false 
	if categoryName == "Featured" or categoryName == nil then
		g_onTab = TAB_KANEVA
	elseif categoryName == "My Patterns" then
		g_onTab = TAB_MY
	elseif categoryName == "My Pictures" then
		g_onTab = TAB_PICTURES
	else
		for i,v in ipairs(CATEGORY_LIST[PLAYLISTS]) do 
			if categoryName == CATEGORY_LIST[PLAYLISTS][i]["name"] then 
				g_onTab = TAB_PLAYLISTS
				g_curTextureList = CATEGORY_LIST[PLAYLISTS][i]["groupID"]
				playlistMatch = true
			end 
		end 
		if playlistMatch == false  then 
			g_onTab = nil
			g_categoryName = categoryName
		end 
	end
	Static_SetText(gHandles["stcCategories"], categoryName or "Featured")
end

function populateDropdown()

	local lstCategories = gHandles["lstCategories"]
	ListBox_RemoveAllItems(lstCategories)
	local catEntries = 0
	for i, v in ipairs(CATEGORY_LIST) do
		for j, k in ipairs(v) do
			if k["playlistString"]~=nil then 
			ListBox_AddItem(lstCategories,k["name"],j)
			else 
			ListBox_AddItem(lstCategories, k,j)
			end 
			catEntries = catEntries +1
		end
	end

	Control_SetSize(lstCategories, Control_GetWidth(lstCategories), (ListBox_GetRowHeight(lstCategories)+.5) * catEntries)	
end 

-----------------------------------------------------------------------------------------
-- Color Filters 
-----------------------------------------------------------------------------------------
function changeFilter(filter)
	updateFilter(filter) -- Separate function for updating the filter and actually updating the page. Allows search and categories to clear filter

	-- Set page to 1 and clear the search
	g_curPage = 1

	if filter ~= 1 then
		clearSearch()

		if g_onTab ~= TAB_KANEVA then
			selectCategory()
		end
	end

	changePage()
end

function updateFilter(filter)
	g_filter = filter

	-- Set all controls to enabled if not selected or disabled if the current filter
	for i=1, #FILTER_NAMES do
		Control_SetEnabled(gHandles["btnFilter" .. i], i ~= filter)
	end
end

function btnFilter1_OnButtonClicked( buttonHandle )
	changeFilter(1)
end

function btnFilter2_OnButtonClicked( buttonHandle )
	changeFilter(2)
end

function btnFilter3_OnButtonClicked( buttonHandle )
	changeFilter(3)
end

function btnFilter4_OnButtonClicked( buttonHandle )
	changeFilter(4)
end

function btnFilter5_OnButtonClicked( buttonHandle )
	changeFilter(5)
end

function btnFilter6_OnButtonClicked( buttonHandle )
	changeFilter(6)
end

-----------------------------------------------------------------------------------------
-- Search
-----------------------------------------------------------------------------------------
function search()
	local searchString = EditBox_GetText(gHandles["eBoxSearch"])

	-- Do this check because if it is empty, OnFocusOut has already replaced the string with "Search"
	if g_clearSearch then
		clearSearch()
		changePage() -- Separate because it avoids double webcalls
		return
	end

	if searchString ~= g_searchString then
		g_searchString = searchString

		g_curPage = 1
		updateFilter(1)

		if (g_onTab ~= TAB_KANEVA) and (g_onTab ~= TAB_MY) and (g_onTab ~= TAB_PLAYLISTS) and (g_onTab ~= TAB_PICTURES) then
			selectCategory()
		end

		changePage()

		showSearchX(true)
	end 
	
	MenuClearFocusThis()
end

function clearSearch()
	EditBox_SetText( gHandles["eBoxSearch"], "Search", false)
	g_clearSearch = true
	showSearchX(false)	
	if g_searchString ~= "" then 
		g_searchString = ""
	end 
end

function showSearchX(show)
	Control_SetVisible(gHandles["btnSearchX"], show)
end

function btnSearch_OnButtonClicked( buttonHandle )
	search()
end

-- Hitting the "Enter" key in a search box
function eBoxSearch_OnEditBoxString(editBoxHandle, someString)
	search()
end

function btnSearchX_OnButtonClicked( buttonHandle )
	clearSearch()
	changePage() -- Separate because it avoids double webcalls
end 

function eBoxSearch_OnFocusIn(editBoxHandle)
	showSearchX(false)
	if g_clearSearch then
		EditBox_SetText(editBoxHandle, "", true)
	end
	g_clearSearch = false
end

-- Also see Dialog_OnLButtonDownInside - clears focus there when clicking anywhere else
function eBoxSearch_OnFocusOut(editBoxHandle)
	local search = EditBox_GetText(editBoxHandle)
	if search == "" or g_clearSearch then
		g_clearSearch = true
		EditBox_SetText(editBoxHandle, "Search", false)
	else
		g_clearSearch = false
		showSearchX(true)
	end
end

function showNoneSearched(enabled)
	Control_SetVisible(gHandles["stcNoneSearched1"], enabled)
	Control_SetVisible(gHandles["stcNoneSearched2"], enabled)
end 

-----------------------------------------------------------------------------------------
-- Apply/Copy/Reset
-----------------------------------------------------------------------------------------

function btnApply_OnButtonClicked( buttonHandle )

	if g_copied then 
		Log("ApplyClicked: COPIED PASTE")

		local ev = KEP_EventCreate( "CopyPasteTextureEvent")
		KEP_EventSetFilter(ev, 2) -- 2=paste
		KEP_EventQueue(ev)
		g_selectedTexture = 0
	
	elseif g_selectedTexture > 0 then
		local assetId = g_t_textures[g_selectedTexture]["asset_id"]
		if (assetId == nil) then return end 
		local texUrl = g_t_textures[g_selectedTexture]["full_image_url"]
		if (texUrl == nil) then texUrl = "" end
		Log("ApplyClicked: assetId="..assetId.." texUrl='"..texUrl.."'")

		local ev = KEP_EventCreate( "ApplyTextureEvent" )
		KEP_EventEncodeNumber(ev, GameGlobals.CUSTOM_TEXTURE)
		KEP_EventEncodeNumber(ev, assetId)
		KEP_EventEncodeString(ev, texUrl)
		KEP_EventQueue(ev)

		g_copied = false
	end

	showPatternCopied(false)
	
	Control_SetVisible(gHandles["btnResetPattern"], true)
	Control_SetEnabled(gHandles["btnResetPattern"], true)
	
	Control_SetVisible(gHandles["btnCopyPattern"], true)
	Control_SetEnabled(gHandles["btnCopyPattern"], true)
end

function btnCopyPattern_OnButtonClicked( buttonHandle )
	Log("CopyClicked: COPIED")

	Control_SetFocus(gHandles["btnCopyPattern"])
	MenuClearFocusThis()

	local ev = KEP_EventCreate( "CopyPasteTextureEvent")
	KEP_EventSetFilter(ev, 1) -- 1=copy
	KEP_EventQueue(ev)
		
	resetFrames()
	enableApplyButton(true)
		
	g_copied = true
	g_endTime = false 
	Static_SetText(gHandles["btnApply"], "Paste")
		
	Control_SetVisible(gHandles["btnClipboardLabel"], true)
end

function btnResetPattern_OnButtonClicked( buttonHandle )
	Log("ResetClicked")

	local ev = KEP_EventCreate( "ApplyTextureEvent" )
	KEP_EventEncodeNumber(ev, GameGlobals.CUSTOM_TEXTURE)
	KEP_EventEncodeNumber(ev, 0) -- 0=default texture
	KEP_EventEncodeString(ev, "") -- ""=default texture url
	KEP_EventQueue(ev)
	
	Control_SetVisible(gHandles["btnResetPattern"], true)
	Control_SetEnabled(gHandles["btnResetPattern"], false)
	
	Control_SetVisible(gHandles["btnCopyPattern"], true)
	Control_SetEnabled(gHandles["btnCopyPattern"], false)
end

function enableApplyButton(enable)
	Control_SetVisible(gHandles["btnApply"], true) -- Always keep on, we are just enabling/disabling. May be off from timer though
	Control_SetEnabled(gHandles["btnApply"], enable)
end

function enableResetButton(enable)
	Control_SetVisible(gHandles["btnResetPattern"], true) -- Always keep on, we are just enabling/disabling. May be off from timer though
	Control_SetEnabled(gHandles["btnResetPattern"], enable)
		
	Control_SetVisible(gHandles["btnCopyPattern"], true)
	Control_SetEnabled(gHandles["btnCopyPattern"], enable and g_playerMode == "God")

	showPatternCopied(false)
end

function updateResetButton()
	-- Enable Reset Button While Selected Object Has Custom Texture
	local objects = getSelectedObjects()
	if #objects < 1 then
		enableResetButton(false)
	else
		local hasCustomTexture = (string.match(objects[1].customTexture, ".*CustomTexture\\(.*)") ~= nil)
		enableResetButton(hasCustomTexture)
	end	
end

function showPatternCopied(show)
	Control_SetVisible(gHandles["btnCopyPattern"], not show)
	Control_SetVisible(gHandles["btnResetPattern"], not show)
	Control_SetVisible(gHandles["btnPatternCopied"], show)

	if not show then
		g_time = 0
		g_endTime = true
	end
end

-----------------------------------------------------------------------------------------
-- Misc Buttons
-----------------------------------------------------------------------------------------

function btnMorePatterns_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser(GameGlobals.WEB_SITE_PREFIX_KANEVA ..UPLOAD_PATTERNS_SUFFIX )
end
