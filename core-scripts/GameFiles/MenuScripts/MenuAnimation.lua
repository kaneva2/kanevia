--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------
-- MenuAnimation.lua
-- Documentation link: http://wiki.kaneva.com/mediawiki/index.php/Menu_Animation_Library
--
-- TODO (by importance):
-- 1. moveTo(), resizeTo() - moves to given location rather than a delta amount
-- 2. Allow input of element handles as well as dialog/controls for tweening textures
-- 3. Allow alignment (eg resize in center)
-- 4. Include additional arguments for easing to allow the advanced easing functions to be used
--------------------------------------------------------------------------------------------------------
dofile("Easing.lua")
dofile("..\\Scripts\\ControlTypes.lua")

-- All types of handles that are currently available to tween
HANDLE_TYPE = {}
HANDLE_TYPE.DIALOG = "Dialog"
HANDLE_TYPE.CONTROL = "Control"

-- All types of callbacks that are currently possible to add using Tween:on
CALLBACK = {}
CALLBACK.START 		= 1
CALLBACK.STOP 		= 2
CALLBACK.UPDATE 	= 3
CALLBACK.PAUSE		= 4

local m_activeTweens = {}	-- Tweens that are actively running
local m_currID = 0			-- Each tween has a unique ID used for faster storage/access

local m_pendingDelete = {}

--------------------------------------------------------------------------------------------------------
-- Calls that must be made by menu
--------------------------------------------------------------------------------------------------------
-- Call in Dialog_OnRender
function MenuAnimation_OnRender(fElapsedTime)
	-- For each active tween, get the current time and increment by time elapsed since last frame
	for id, tween in pairs(m_activeTweens) do
		local currTime = tween:getTimer()

		if tween:getReversed() then
			currTime = currTime - fElapsedTime
		else
			currTime = currTime + fElapsedTime
		end
		
		tween:update(currTime)
	end

	-- delete the tweens that were completed 
	for id, _ in pairs(m_pendingDelete) do
		m_activeTweens[id] = nil
	end

	-- clear the pending delete table 
	m_pendingDelete = {}
end

--------------------------------------------------------------------------------------------------------
-- Local Helpers
--------------------------------------------------------------------------------------------------------
local function formatHandleTable(handleType, handles)
	-- If not a table, make it one for consistency
	if type(handles) == "userdata" or type(handles) == "string" then
		handles = {handles}
	elseif type(handles) ~= "table" then -- TODO: check deeper to make sure table of userdata?
		LogError("MenuAnimation handles Must be type userdata or table of handles")
	end

	-- Convert any control names to handles
	for i, handle in pairs(handles) do
		if type(handle) == "string" then
			if handleType == HANDLE_TYPE.DIALOG then
				handles[i] = MenuHandle(handle)
			elseif handleType == HANDLE_TYPE.CONTROL then
				handles[i] = gHandles[handle]
			end
		end
	end
	
	return handles
end

local function getDisplayElementByHandleType(handleType, handle, elementTag)
	if handleType == HANDLE_TYPE.DIALOG then
		return Dialog_GetDisplayElement( handle, elementTag )
	elseif handleType == HANDLE_TYPE.CONTROL then
		return Control_GetDisplayElement( handle, elementTag )
	end
end

local function getFontTextureAlphas(handleType, handle, elementTag, controlState)
	local displayElement = getDisplayElementByHandleType(handleType, handle, elementTag)
				
	local fontColorElement = Element_GetFontColor(displayElement)
	local fontColor = BlendColor_GetColor(fontColorElement, controlState)

	local textureColorElement = Element_GetTextureColor(displayElement)
	local textureColor = BlendColor_GetColor(textureColorElement, controlState)

	return fontColor.a, textureColor.a
end

local function setFontAlpha(handleType, handle, elementTag, controlState, alpha)
	local displayElement = getDisplayElementByHandleType(handleType, handle, elementTag)

	-- Set color of text
	local fontColorElement = Element_GetFontColor(displayElement)
	local fontColor = BlendColor_GetColor(fontColorElement, controlState)
	fontColor.a = alpha
	BlendColor_SetColor(fontColorElement, controlState, fontColor)
end

local function setTextureAlpha(handleType, handle, elementTag, controlState, alpha)
	local displayElement = getDisplayElementByHandleType(handleType, handle, elementTag)

	-- Set color of texture
	local textureColorElement = Element_GetTextureColor(displayElement)
	local textureColor = BlendColor_GetColor(textureColorElement, controlState)
	textureColor.a = alpha
	BlendColor_SetColor(textureColorElement, controlState, textureColor)
end

local function constrainAlpha(alpha)
	if alpha < 1 then
		alpha = 1 -- Set to 1 because in some cases 0 makes the control turn white rather than disappear
	elseif alpha > 255 then
		alpha = 255
	end

	return alpha
end

--------------------------------------------------------------------------------------------------------
-- Tween Class
--------------------------------------------------------------------------------------------------------
--Lua OO setup, fun!
Tween = {}
Tween.__index = Tween

setmetatable(Tween, {
  __call = function (cls, ...)
    local self = setmetatable({}, cls)
    self:_init(...)
    return self
  end,
})

-- Setup the tween. Call using Tween(hT, h). 
-- dialogHandles: single dialog/table of dialogs by name or handle
-- controlHandles: single control/table of control by name or handle
function Tween:_init(dialogHandles, controlHandles)	
	self.dialogHandles = {}
	if dialogHandles then
		self.dialogHandles = formatHandleTable(HANDLE_TYPE.DIALOG, dialogHandles)
	end

	self.controlHandles = {}
	if controlHandles then
		self.controlHandles = formatHandleTable(HANDLE_TYPE.CONTROL, controlHandles)
	end

	-- ID used to easily access m_activeTweens
	m_currID = m_currID + 1
	self.id = m_currID

	-- list of all properties (move, rotate, etc) currently active on the tween
	self.props = {}

	-- Holds any enaabled callbacks
	self.callbacks = {}

	self.reversed			= false	-- is it reversing?
	self.paused 			= false -- Currently paused?

	self.timer = 0

	-- Store starting values here in case this is a manual tween
	self:storeStartingValues()
end

-- Used to setup any callbacks. If given nothing as a function, will remove the callback
-- callbackType: enum CALLBACK listed above, type you're setting up
-- callbackFunction The function it should call back to
function Tween:on(callbackType, callbackFunction)
	if type(callbackFunction) == "function" then
		self.callbacks[callbackType] = callbackFunction
	else
		self.callbacks[callbackType] = nil
	end
end

-- Start the tween if it is not started, paused, or stopped
function Tween:start(updateStartingValues)
	-- If already started then don't start again (must stop or pause first)
	if m_activeTweens[self.id] ~= nil then
		return
	end
	
	--table.insert(m_activeTweens, self.id, self)
	m_activeTweens[self.id] = self

	-- Only store if requested
	if updateStartingValues then
		self:storeStartingValues()
	end

	self.paused = false

	if self.callbacks[CALLBACK.START] then
		self.callbacks[CALLBACK.START](self, self.timer)
	end
end

-- Pause the tween if it is started
-- Stops the tween, but does not reset the timer to zero
function Tween:pause()
	-- Not currently started
	if not m_activeTweens[self.id] then
		return
	end

	m_pendingDelete[self.id] = true
	--m_activeTweens[self.id] = nil

	self.paused = true

	if self.callbacks[CALLBACK.PAUSE] then
		self.callbacks[CALLBACK.PAUSE](self, self.timer)
	end
end

-- Stops the tween if it is started
-- Resets the timer to 0
function Tween:stop()
	-- Not currently started
	if not m_activeTweens[self.id] then
		return
	end

	m_pendingDelete[self.id] = true
	--m_activeTweens[self.id] = nil

	self.paused = false

	-- Want to return time at which stopped, but also set to 0 before doing callback
	-- Initially added to allow reverse created inside stop callback to not be undone
	local tempTime = self.timer
	self.timer = 0

	if self.callbacks[CALLBACK.STOP] then
		self.callbacks[CALLBACK.STOP](self, tempTime)
	end
end

-- Translate all handles by the inputted amount on X/Y axis
-- deltaX/deltaY: the distance to move on the X/Y axis
-- length: how long in seconds it should take to complete
-- delay: how long it should wait before starting, defaults to 0
-- easeType: Any easing function found in Easing.lua, defaults to linear
function Tween:move(deltaX, deltaY, length, delay, easeType, preserveStartingValues)
	-- Clear out if entered with no info
	if deltaX == nil and deltaY == nil then
		self.props.move = nil
		return
	end

	self.props.move = preserveStartingValues and self.props.move or {}

	self.props.move.deltaX 		= deltaX or 0
	self.props.move.deltaY		= deltaY or 0
	self.props.move.len 		= length or 0
	self.props.move.delay 		= delay or 0
	self.props.move.easeType	= easeType or Easing.linear
	self.props.move.func 		= function(...) self:moveHandles(...) end -- the function we want to call so update doesn't have to call each individually

	-- Update for manual tweens, or tweens already started
	if not preserveStartingValues then
		self:storeStartingValues()
	end
end

-- Resizes all handles by the inputted amount
-- deltaW/deltaH: amount it should enlarge/shrink by width and height
-- length: how long in seconds it should take to complete
-- delay: how long it should wait before starting, defaults to 0
-- easeType: Any easing function found in Easing.lua, defaults to linear
function Tween:resize(deltaW, deltaH, length, delay, easeType)
	-- Clear out if entered with no info
	if deltaW == nil and deltaH == nil then
		self.props.resize = nil
		return
	end

	self.props.resize = {deltaW		= deltaW or 0,
						deltaH		= deltaH or 0,
						len 		= length or 0,
						delay 		= delay or 0,
						easeType 	= easeType or Easing.linear,
						func 		= function(...) self:resizeHandles(...) end} -- the function we want to call so update doesn't have to call each individually


	-- Update for manual tweens, or tweens already started
	self:storeStartingValues()
end

-- Fades the alpha and inputted handles, or every handle if a menu is given
-- deltaAlpha, amount to change the alpha by
-- length: how long in seconds it should take to complete
-- delay: how long it should wait before starting, defaults to 0
-- easeType: Any easing function found in Easing.lua, defaults to linear
function Tween:fade(deltaAlpha, length, delay, easeType)
	-- Clear out if entered with no info
	if deltaAlpha == nil then
		self.props.fade = nil
		return
	end

	self.props.fade = {	deltaAlpha	= deltaAlpha or 0,
						len 		= length or 0,
						delay 		= delay or 0,
						easeType 	= easeType or Easing.linear,
						func 		= function(...) self:fadeHandles(...) end} -- the function we want to call so update doesn't have to call each individually

	-- Update for manual tweens, or tweens already started
	self:storeStartingValues()
end

-- Adds a generic property to the props list
-- name: name for property
-- startingValue: what value to start from
-- endingValue: what value it will end at
-- length: how long in seconds it should take to complete
-- delay: how long it should wait before starting, defaults to 0
-- easeType: Any easing function found in Easing.lua, defaults to linear
function Tween:addProperty(name, startingValue, endingValue, length, delay, easeType)
	if name == "fade" or name == "move" or name == "resize" then
		LogWarn("Change the name of the property you are inputting to prevent conflict with standard properties")
		return
	end

	self.props[name] = { value 		= 0,
						startVal 	= startingValue or 0,
						endVal		= endingValue or 1,
						len 		= length or 0,
						delay 		= delay or 0,
						easeType 	= easeType or Easing.linear,
						func 		= function(...) self:updateProperty(name, ...) end } -- the function we want to call so update doesn't have to call each individually

	-- Set initial value
	local time = self.timer - delay
	if time < 0 then
		time = 0
	elseif time > length then
		time = length
	end
	self:updateProperty(name, self.timer)
end

-- Return the value if generic property, otherwise return percent way through (since others control multiple)
function Tween:getPropertyValue(name)
	if self.props[name] then
		if self.props[name]["value"] then
			return self.props[name]["value"]
		else
			local percent = (self.timer - self.props[name]["delay"]) / self.props[name]["len"]
			if percent < 0 then 
				percent = 0 
			elseif percent > 1 then
				percent = 1
			end
			return percent
		end
	end
end

-- Get the current time of the tween
function Tween:getTimer()
	return self.timer
end

function Tween:getHandles(handleType)
	if handleType == HANDLE_TYPE.DIALOG then
		return self.dialogHandles
	elseif handleType == HANDLE_TYPE.CONTROL then
		return self.controlHandles
	-- return all handles if not specified
	else
		return self:getAllHandles()
	end
end

function Tween:getAllHandles()
	local handles = {}
	
	for i, v in pairs(self.dialogHandles) do
		table.insert(handles, v)
	end

	for i, v in pairs(self.controlHandles) do
		table.insert(handles, v)
	end

	return handles
end

function Tween:getStarted()
	return m_activeTweens[self.id]
end

function Tween:getStopped()
	return (m_activeTweens[self.id] == nil or m_pendingDelete[self.id] == true) and not self.paused
end

function Tween:getPaused()
	return self.paused
end

-- Get whether the tween is reversed
function Tween:getReversed()
	return self.reversed
end

-- Set whether the tween is reversed
function Tween:reverse(reverse)
	if (reverse == self.reversed) then return end

	self.reversed = reverse

	-- If not started, set the timer to the max length so it starts from the end
	if self:getStopped() then
		m_pendingDelete[self.id] = nil
		if reverse then
			self.timer = self:maxLength()
		else
			self.timer = 0
		end
	end
end

-- Return the longest length+delay (the same time which stop will be called)
function Tween:maxLength()
	local maxLength = 0

	for index, property in pairs(self.props) do
		if (property.len + property.delay) > maxLength then
			maxLength = property.len + property.delay
		end
	end

	return maxLength
end

-- Update all handles based on a given time
-- Should be [0,length+delay]. Anything longer than length+delay will be ignored
function Tween:update(time)
	local finished = true
	self.timer = time

	-- For each property, check to see if it's active based on the timer.
	for index, prop in pairs(self.props) do
		-- Don't want to change the timer itself as it could be used for other properties
		local inputTime = self.timer - prop.delay

		-- Constrain to min/max time so that it always finishes at proper ends
		if self.timer >= (prop.len + prop.delay) then
			inputTime = prop.len
		elseif self.timer <= 0 then
			inputTime = 0
		else
			finished = false
		end

		-- If it's greater than the delay, we want to start moving it
		if self.timer > prop.delay then
			prop.func(inputTime)
		else
			prop.func(0)
		end
	end

	if self.callbacks[CALLBACK.UPDATE] then
		self.callbacks[CALLBACK.UPDATE](self, self.timer)
	end

	-- No properties were active, so all finished. Call stop.
	if finished then
		self:stop()
	end
end

-- Move all handles based on given time, mostly for internal use
function Tween:moveHandles(time)
	self:moveHandlesByType(HANDLE_TYPE.DIALOG, time)
	self:moveHandlesByType(HANDLE_TYPE.CONTROL, time)
end

-- Move all handles based on type and time, mostly for internal use
function Tween:moveHandlesByType(handleType, time)
	local moveInfo = self.props.move
	if (moveInfo == nil) then return end
	
	-- Specify which function and table we should use based on type
	local moveFunction, handleTable
	if handleType == HANDLE_TYPE.DIALOG then
		moveFunction = Dialog_SetLocation
		handleTable = self.dialogHandles
	else
		moveFunction = Control_SetLocation
		handleTable = self.controlHandles
	end

	-- Iterate through all handles and move each one based on move properties
	for index, handle in pairs(handleTable) do
		-- See Easing.lua for more info
		local newX = moveInfo.easeType( time, moveInfo.startLocs[handle].x, moveInfo.deltaX, moveInfo.len)
		local newY = moveInfo.easeType( time, moveInfo.startLocs[handle].y, moveInfo.deltaY, moveInfo.len)

		moveFunction(handle, newX, newY)
	end
end

-- Resize all handles based on given time, mostly for internal use
function Tween:resizeHandles(time)
	self:resizeHandlesByType(HANDLE_TYPE.DIALOG, time)
	self:resizeHandlesByType(HANDLE_TYPE.CONTROL, time)
end

-- Resize all handles based on type and time, mostly for internal use
function Tween:resizeHandlesByType(handleType, time)
	local resizeInfo = self.props.resize
	if (resizeInfo == nil) then return end

	-- Specify which function and table we should use based on type
	local resizeFunction, handleTable
	if handleType == HANDLE_TYPE.DIALOG then
		resizeFunction = Dialog_SetSize
		handleTable = self.dialogHandles
	else
		resizeFunction = Control_SetSize
		handleTable = self.controlHandles
	end

	-- Iterate through all handles and resize each one based on move properties
	for index, handle in pairs(handleTable) do
		-- See Easing.lua for more info
		local newW = resizeInfo.easeType( time, resizeInfo.startLocs[handle].w, resizeInfo.deltaW, resizeInfo.len)
		local newH = resizeInfo.easeType( time, resizeInfo.startLocs[handle].h, resizeInfo.deltaH, resizeInfo.len)

		resizeFunction(handle, newW, newH)
	end
end

-- Fade all handles based on given time, mostly for internal use
function Tween:fadeHandles(time)
	self:fadeHandlesByType(HANDLE_TYPE.DIALOG, time)
	self:fadeHandlesByType(HANDLE_TYPE.CONTROL, time)
end

-- Fade all handles based on type and time, mostly for internal use
function Tween:fadeHandlesByType(handleType, time)
	local fadeInfo = self.props.fade
	if (fadeInfo == nil) then return end

	-- Specify which table we should use based on type
	local handleTable
	if handleType == HANDLE_TYPE.DIALOG then
		handleTable = self.dialogHandles
	else
		handleTable = self.controlHandles
	end

	-- Iterate through all handles and resize each one based on move properties
	for index, handle in pairs(handleTable) do
		local fontAlpha = fadeInfo.easeType( time, fadeInfo.startLocs[handle].fontAlpha, fadeInfo.deltaAlpha, fadeInfo.len)
		local textureAlpha = fadeInfo.easeType( time, fadeInfo.startLocs[handle].textureAlpha, fadeInfo.deltaAlpha, fadeInfo.len)

		local controlType = "Dialog"
		if handleType == HANDLE_TYPE.CONTROL then
			controlType = Control_GetType(handle)
		end

		-- we want to do this for each state
		for i, tag in pairs(FADE_ELEMENTS[controlType]) do
			-- If it's part of a table, grab the state from it we need, otherwise default to normal
			-- Buttons specifically need to set their state + element at the same time otherwise the different states will remain visible at the wrong time
			local elementTag = tag
			local controlState = ControlStates.DXUT_STATE_NORMAL
			if type(tag) == "table" then
				elementTag = tag[1]
				controlState = tag[2] or ControlStates.DXUT_STATE_NORMAL
			end

			-- Copy these so any changes don't propagate to next iteration
			local newFontAlpha = fontAlpha
			local newTextureAlpha = textureAlpha

			-- For buttons, if were dealing with a disabled state, don't bring to full amt because we want it to show as disabled
			if controlState == ControlStates.DXUT_STATE_DISABLED and (elementTag == "normalDisplay" or elementTag == "disabledDisplay") then
				newFontAlpha = newFontAlpha/3
				newTextureAlpha = newTextureAlpha/3
			end

			-- Constrain to (0, 255]
			newFontAlpha = constrainAlpha(newFontAlpha)
			newTextureAlpha = constrainAlpha(newTextureAlpha)

			-- Set alphas on font and texture (font must fade as well as texture or text will still be visible)
			setFontAlpha(handleType, handle, elementTag, controlState, newFontAlpha)
			setTextureAlpha(handleType, handle, elementTag, controlState, newTextureAlpha)
		end
	end
end

function Tween:updateProperty(name, time)
	local propInfo = self.props[name]

	if propInfo == nil then return end

	-- If the startVal variable does not exist, perhaps they are calling another property via name
	if not propInfo.startVal then
		if name == "move" then
			self.moveHandles(time)
		elseif name == "resize" then
			self.resizeHandles(time)
		elseif name == "fade" then
			self.fadeHandles(time)
		else
			return
		end
	end

	local delta = propInfo.endVal - propInfo.startVal
	self.props[name]["value"] = propInfo.easeType( time, propInfo.startVal, delta, propInfo.len)
end

function Tween:setHandleLocations(x, y)
	self:setHandleLocationsByType(HANDLE_TYPE.DIALOG, x)
	self:setHandleLocationsByType(HANDLE_TYPE.CONTROL, y)
end

function Tween:setHandleLocationsByType(handleType, x, y)
	-- Specify which function and table we should use based on type
	local movePrefix, handleTable, getX, getY
	if handleType == HANDLE_TYPE.DIALOG then
		movePrefix = "Dialog"
		handleTable = self.dialogHandles
	else
		movePrefix = "Control"
		handleTable = self.controlHandles
	end

	for i, handle in pairs(handletable) do
		local newX = x
		local newY = y
		if (not x) then newX = _G[movePrefix .. "_GetLocationX"](handle) end
		if (not y) then newY = _G[movePrefix .. "_GetLocationY"](handle) end
		_G[movePrefix .. "SetLocation"](handle, newX, newY)
	end
end

function Tween:setHandleSizes(w, h)
	self:setHandleSizesByType(HANDLE_TYPE.DIALOG, w, h)
	self:setHandleSizesByType(HANDLE_TYPE.CONTROL, w, h)
end

function Tween:setHandleSizesByType(handleType, w, h)
	-- Specify which function and table we should use based on type
	local resizePrefix, handleTable
	if handleType == HANDLE_TYPE.DIALOG then
		resizePrefix = "Dialog"
		handleTable = self.dialogHandles
	else
		resizePrefix = "Control"
		handleTable = self.controlHandles
	end

	for i, handle in pairs(handletable) do
		local newW = w
		local newH = h
		if (not w) then newW = _G[resizePrefix .. "_GetWidth"](handle) end
		if (not h) then newH = _G[resizePrefix .. "_GetWidth"](handle) end
		_G[resizePrefix .. "SetLocation"](handle, newW, newH)
	end
end

function Tween:setHandleAlphas(alpha)
	self:setHandleAlphasByType(HANDLE_TYPE.DIALOG, alpha)
	self:setHandleAlphasByType(HANDLE_TYPE.CONTROL, alpha)
end

function Tween:setHandleAlphasByType(handleType, alpha)
	-- Specify which table we should use based on type
	local handleTable
	if handleType == HANDLE_TYPE.DIALOG then
		handleTable = self.dialogHandles
	else
		handleTable = self.controlHandles
	end

	for i, handle in pairs(handleTable) do
		local controlType = "Dialog"
		if handleType == HANDLE_TYPE.CONTROL then
			controlType = Control_GetType(handle)
		end

		-- we want to do this for each state
		for i, tag in pairs(FADE_ELEMENTS[controlType]) do
			-- If it's part of a table, grab the state from it we need, otherwise default to normal
			-- Buttons specifically need to set their state + element at the same time otherwise the different states will remain visible at the wrong time
			local elementTag = tag
			local controlState = ControlStates.DXUT_STATE_NORMAL
			if type(tag) == "table" then
				elementTag = tag[1]
				controlState = tag[2] or ControlStates.DXUT_STATE_NORMAL
			end

			local newFontAlpha = alpha
			local newTextureAlpha = alpha

			if not alpha then
				newFontAlpha, newTextureAlpha = getFontTextureAlphas(handleType, handle, elementTag, controlState)
			else
				-- For buttons, if were dealing with a disabled state, don't bring to full amt because we want it to show as disabled
				if controlState == ControlStates.DXUT_STATE_DISABLED and (elementTag == "normalDisplay" or elementTag == "disabledDisplay") then
					newFontAlpha = newFontAlpha/3
					newTextureAlpha = newTextureAlpha/3
				end

				-- Constrain to (0, 255]
				newFontAlpha = constrainAlpha(newFontAlpha)
				newTextureAlpha = constrainAlpha(newTextureAlpha)
			end

			-- Set alphas on font and texture (font must fade as well as texture or text will still be visible)
			setFontAlpha(handleType, handle, elementTag, controlState, newFontAlpha)
			setTextureAlpha(handleType, handle, elementTag, controlState, newTextureAlpha)
		end
	end
end

-- For each handle, store starting values based on active properties, mostly for internal use
function Tween:storeStartingValues()
	self:storeStartingValuesByType(HANDLE_TYPE.DIALOG)
	self:storeStartingValuesByType(HANDLE_TYPE.CONTROL)
end

-- For each handle by type, store starting values based on active properties, mostly for internal use
function Tween:storeStartingValuesByType(handleType)
	-- Specify which table/functions we should use based on type
	local handleTable, getLocX, getLocY, getWidth, getHeight
	if handleType == HANDLE_TYPE.DIALOG then
		handleTable = self.dialogHandles
		getLocX		= Dialog_GetLocationX
		getLocY		= Dialog_GetLocationY
		getWidth	= Dialog_GetWidth
		getHeight	= Dialog_GetHeight
	else
		handleTable = self.controlHandles
		getLocX		= Control_GetLocationX
		getLocY		= Control_GetLocationY
		getWidth	= Control_GetWidth
		getHeight	= Control_GetHeight
	end

	if (handleTable == {}) then return end

	-- If the move property exists, create a startLoc table if it does not exist, and store all start locations
	if self.props.move then
		if not self.props.move.startLocs then
			self.props.move.startLocs = {}
		end

		for index, handle in pairs(handleTable) do
			self.props.move.startLocs[handle] = {x = getLocX(handle), y = getLocY(handle)}
		end
	end

	-- If the resize property exists, create a startLoc table if it does not exist, and store all start sizes
	if self.props.resize then
		if not self.props.resize.startLocs then
			self.props.resize.startLocs = {}
		end

		for index, handle in pairs(handleTable) do
			self.props.resize.startLocs[handle] = {w = getWidth(handle), h = getHeight(handle)}
		end
	end

	-- If the fade property exists, create a startLoc table if it does not exist, and store all start alphas
	if self.props.fade then
		if not self.props.fade.startLocs then
			self.props.fade.startLocs = {}
		end

		for index, handle in pairs(handleTable) do
			-- We're only going to get info from the normal state. Possibly add in more complexity as needed
			local controlType = "Dialog"
			if handleType == HANDLE_TYPE.CONTROL then
				controlType = Control_GetType(handle)
			end

			local elementTag = FADE_ELEMENTS[controlType][1]
			local controlState = ControlStates.DXUT_STATE_NORMAL
			if type(elementTag) == "table" then
				elementTag = elementTag[1]
			end

			local fontAlpha, textureAlpha = getFontTextureAlphas(handleType, handle, elementTag, controlState)

			self.props.fade.startLocs[handle] = {fontAlpha = fontAlpha, textureAlpha = textureAlpha}
		end
	end
end

-- This is so gross I decided to hide it at the bottom. 
-- Ideally there would be a better way, but this is how MenuEditor does it as well. 
-- Additionally, not all element/state combinations should be touched (if we increase alpha of normal elmeent/disabled state it breaks the disabled state from being transparent)
FADE_ELEMENTS = {}-- a list of all the element tags for each ControlType
-- Leave off iconDisplay for now on Dialog because changing the alpha on it causes the title bar text to relocate
FADE_ELEMENTS["Dialog"]									= { "display", "backgroundDisplay"--[[, "iconDisplay"--]] }
FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_BUTTON]			= {	{"normalDisplay",		ControlStates.DXUT_STATE_NORMAL}, 
															{"normalDisplay",		ControlStates.DXUT_STATE_MOUSEOVER}, 
															{"normalDisplay",		ControlStates.DXUT_STATE_FOCUS}, 
															{"normalDisplay",		ControlStates.DXUT_STATE_PRESSED}, 
															{"normalDisplay",		ControlStates.DXUT_STATE_DISABLED}, 
															{"mouseOverDisplay",	ControlStates.DXUT_STATE_MOUSEOVER}, 
															{"focusedDisplay",		ControlStates.DXUT_STATE_FOCUS}, 
															{"pressedDisplay",		ControlStates.DXUT_STATE_PRESSED}, 
															{"disabledDisplay",		ControlStates.DXUT_STATE_DISABLED} }

FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_STATIC]			= {	{"display", ControlStates.DXUT_STATE_NORMAL}, 
															{"display", ControlStates.DXUT_STATE_DISABLED}}

FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_CHECKBOX]		= {	{"boxDisplay",		ControlStates.DXUT_STATE_NORMAL},
															{"boxDisplay",		ControlStates.DXUT_STATE_MOUSEOVER}, 
															{"boxDisplay",		ControlStates.DXUT_STATE_FOCUS}, 
															{"boxDisplay",		ControlStates.DXUT_STATE_PRESSED}, 
															{"boxDisplay",		ControlStates.DXUT_STATE_DISABLED}, 
															{"checkDisplay",	ControlStates.DXUT_STATE_NORMAL}, 
															{"checkDisplay",	ControlStates.DXUT_STATE_MOUSEOVER}, 
															{"checkDisplay",	ControlStates.DXUT_STATE_FOCUS}, 
															{"checkDisplay",	ControlStates.DXUT_STATE_PRESSED}, 
															{"checkDisplay",	ControlStates.DXUT_STATE_DISABLED} }

FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_RADIOBUTTON]	= {	{"boxDisplay",		ControlStates.DXUT_STATE_NORMAL},
															{"boxDisplay",		ControlStates.DXUT_STATE_MOUSEOVER}, 
															{"boxDisplay",		ControlStates.DXUT_STATE_FOCUS}, 
															{"boxDisplay",		ControlStates.DXUT_STATE_PRESSED}, 
															{"boxDisplay",		ControlStates.DXUT_STATE_DISABLED}, 
															{"checkDisplay",	ControlStates.DXUT_STATE_NORMAL}, 
															{"checkDisplay",	ControlStates.DXUT_STATE_MOUSEOVER}, 
															{"checkDisplay",	ControlStates.DXUT_STATE_FOCUS}, 
															{"checkDisplay",	ControlStates.DXUT_STATE_PRESSED}, 
															{"checkDisplay",	ControlStates.DXUT_STATE_DISABLED} }

FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_COMBOBOX]		= { "mainDisplay", "buttonDisplay", "dropdownDisplay", "selectionDisplay" }

FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_SLIDER]			= { "trackDisplay", "buttonDisplay" }

FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_EDITBOX]		= {	{"textAreaDisplay", ControlStates.DXUT_STATE_NORMAL}, 
															{"textAreaDisplay", ControlStates.DXUT_STATE_DISABLED}, 
															"Caret Color", "Selected Color", "Selected BG" }

FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_SIMPLEEDITBOX]	= {{"textAreaDisplay", ControlStates.DXUT_STATE_NORMAL}, 
															{"textAreaDisplay", ControlStates.DXUT_STATE_DISABLED}, 
															"Caret Color", "Selected Color", "Selected BG" }

FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_LISTBOX]		= { "mainDisplay", "selectionDisplay" }

FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_SCROLLBAR]		= { "trackDisplay", "upArrowDisplay", "downArrowDisplay", "buttonDisplay" }

FADE_ELEMENTS[ControlTypes.DXUT_CONTROL_IMAGE]			= {	{"display", ControlStates.DXUT_STATE_NORMAL}, 
															{"display", ControlStates.DXUT_STATE_DISABLED}}