--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

DEF_LOOP = 1.0

MIN_GAIN = 0.0
MAX_GAIN = 1.0
DEF_GAIN = MAX_GAIN

MIN_PITCH = 0.0
MAX_PITCH = 2.0
DEF_PITCH = 1.0

MIN_ROLLOFF = 0.0
MAX_ROLLOFF = 99999.0
DEF_ROLLOFF = 1.0

MIN_RANGE = 0.0
MAX_RANGE = 99999.0
DEF_RANGE = 25.0

MIN_LOOP_DELAY = 0.0
MAX_LOOP_DELAY = 99999.0
DEF_LOOP_DELAY = MIN_LOOP_DELAY

MIN_OUTER_GAIN = 0.0
MAX_OUTER_GAIN = 1.0
DEF_OUTER_GAIN = MIN_OUTER_GAIN

MIN_ANGLE = 0.0
MAX_ANGLE = 360.0
DEF_INNER_ANGLE = MAX_ANGLE
DEF_OUTER_ANGLE = MAX_ANGLE

-- KEPConstants.h ControlIds
SOUND_CONFIG = 16
SOUND_START = 17
SOUND_STOP = 18

RDO_OMNI = 1
RDO_DIRECTIONAL = 2
gRadioState = RDO_OMNI

gLoop = DEF_LOOP
gLoopDelay = DEF_LOOP_DELAY
gGain = DEF_GAIN
gPitch = DEF_PITCH
gRange = DEF_RANGE
gOuterGain = DEF_OUTER_GAIN
gInnerAngle = DEF_INNER_ANGLE
gOuterAngle = DEF_OUTER_ANGLE
gRolloff = DEF_ROLLOFF

g_revertOnClose = true

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	InitSettings()
end

function Dialog_OnDestroy(dialogHandle)
	if g_revertOnClose then 
		RevertSettings()
	end
	Helper_Dialog_OnDestroy( dialogHandle )
end

function ValidSelectedSound()
--	Log("ValidSelectedSound")
	
	-- Valid Current Sound Selected ?
	count = ObjectsSelected()
	for i = 1, count do
		local obj = ObjectSelected(i)
		if (obj.type == ObjectType.SOUND) and (obj.getset ~= nil) then
			return true
		end
	end
	return false
end

function SetSelectedSoundAttr(attr, val)
--	Log("SetSelectedSoundAttr: attr="..attr.." val="..val)
	
	-- Valid Current Sound Selected ?
	if not ValidSelectedSound() then return false end

	-- Set Sound Placement Attribute
	count = ObjectsSelected()
	for i = 1, count do
		local obj = ObjectSelected(i)
		if (obj.type == ObjectType.SOUND) and (obj.getset ~= nil) then
			GetSet_SetNumber(obj.getset, attr, val)
		end
	end
	return true
end

function GetSelectedSoundAttr(attr)
--	Log("GetSelectedSoundAttr: attr="..attr)
	
	-- Valid Current Sound Selected ?
	if not ValidSelectedSound() then return 0 end

	-- Get Sound Placement Attribute
	count = ObjectsSelected()
	for i = 1, count do
		local obj = ObjectSelected(i)
		if (obj.type == ObjectType.SOUND) and (obj.getset ~= nil) then
			return GetSet_GetNumber(obj.getset, attr)
		end
	end
	return 0
end

function TriggerSound()
	if ToBool(gLoop) == true then return end
	SetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_PLAYBACKCTRL, SOUND_START)
end

function LogSettings()
	local settings = Settings()
	Log("LogSettings:")
	Log(" ... loop="..settings.loop)
	Log(" ... loopDelay="..settings.loop_delay)
	Log(" ... gain="..settings.gain)
	Log(" ... pitch="..settings.pitch)
	Log(" ... range="..settings.range)
	Log(" ... outerGain="..settings.outer_gain)
	Log(" ... innerAngle="..settings.inner_angle)
	Log(" ... outerAngle="..settings.outer_angle)
	Log(" ... rolloff="..settings.rolloff)
end

function Settings()

	local settings = {}
	settings.loop = gLoop
	settings.loop_delay = gLoopDelay
	settings.gain = gGain
	settings.pitch = gPitch
	settings.range = gRange
	settings.outer_gain = gOuterGain
	settings.inner_angle = gInnerAngle
	settings.outer_angle = gOuterAngle
	settings.rolloff = gRolloff

	return settings
end

function ClampVal(v, vMin, vMax)
	if (v < vMin) then v = vMin end
	if (v > vMax) then v = vMax end
	return v
end

function RevertSettings()
	LogWarn("RevertSettings")
	
	-- Valid Current Sound Selected ?
	if not ValidSelectedSound() then return false end

	-- Revert Settings
	SetSelectedSoundAttr( SoundPlacementIds.IDS_SOUNDPLACEMENT_PITCH, gSaveSettings.pitch )
	SetSelectedSoundAttr( SoundPlacementIds.IDS_SOUNDPLACEMENT_GAIN, gSaveSettings.gain )
	SetSelectedSoundAttr( SoundPlacementIds.IDS_SOUNDPLACEMENT_MAX_DISTANCE, gSaveSettings.range )
	SetSelectedSoundAttr( SoundPlacementIds.IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR, gSaveSettings.rolloff )
	SetSelectedSoundAttr( SoundPlacementIds.IDS_SOUNDPLACEMENT_LOOP, gSaveSettings.loop )
	SetSelectedSoundAttr( SoundPlacementIds.IDS_SOUNDPLACEMENT_LOOP_DELAY, gSaveSettings.loop_delay )
	SetSelectedSoundAttr( SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN, gSaveSettings.outer_gain )
	SetSelectedSoundAttr( SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE, gSaveSettings.inner_angle )
	SetSelectedSoundAttr( SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE, gSaveSettings.outer_angle )

	return true
end

function InitSettings()
	Log("InitSettings")

	-- Get Initial Settings
	local inited = ToBool(GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_INITED))
	if inited then
		InitSettingsCurrent()
	else
		InitSettingsDefaults()
	end

	-- Set Omni/Directional
	if gInnerAngle == MAX_ANGLE and gOuterAngle == MAX_ANGLE then
		SetOmni()
	else
		SetDirectional()
	end

	-- Save Off Initial Settings For Possible Revert
	gSaveSettings = Settings()

	LogSettings()
end

function InitSettingsDefaults()
	Log("InitSettingsDefaults")

	-- Initialize With Default Settings
	SetPitch(DEF_PITCH)
	SetGain(DEF_GAIN)
	SetRange(DEF_RANGE)
	SetRolloff(DEF_ROLLOFF)
	SetLoop(DEF_LOOP)
	SetLoopDelay(DEF_LOOP_DELAY)
	SetOuterGain(DEF_OUTER_GAIN)
	SetInnerAngle(DEF_INNER_ANGLE, false)
	SetOuterAngle(DEF_OUTER_ANGLE, false)

	return true
end

function InitSettingsCurrent()
	Log("InitSettingsCurrent")

	-- Valid Current Sound Selected ?
	if not ValidSelectedSound() then return false end

	-- Initialize With Current Settings
	SetPitch(GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_PITCH))
	SetGain(GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_GAIN))
	SetRange(GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_MAX_DISTANCE))
	SetRolloff(GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR))
	SetLoop(GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_LOOP))
	SetLoopDelay(GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_LOOP_DELAY))
	SetOuterGain(GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN))
	SetInnerAngle(GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE), false)
	SetOuterAngle(GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE), false)

	return true
end

function sendUpdateToServer()
	
	-- Valid Current Sound Selected ?
	if not ValidSelectedSound() then return false end

	-- Send Update To Server For Selected Sounds
	count = ObjectsSelected()
	for i = 1, count do
		local obj = ObjectSelected(i)
		if (obj.type == ObjectType.SOUND) and (obj.id > 0) and (obj.glid > 0) then
			local e = KEP_EventCreate( "UpdateSoundCustomizationEvent" )
			instanceId = KEP_GetZoneInstanceId()
			KEP_EventAddToServer( e )
			KEP_EventSetObjectId( e, obj.id )
			KEP_EventEncodeNumber( e, obj.glid )
			KEP_EventEncodeNumber( e, gSaveSettings.pitch )
			KEP_EventEncodeNumber( e, gSaveSettings.gain )
			KEP_EventEncodeNumber( e, gSaveSettings.range )
			KEP_EventEncodeNumber( e, gSaveSettings.rolloff )
			KEP_EventEncodeNumber( e, gSaveSettings.loop )
			KEP_EventEncodeNumber( e, gSaveSettings.loop_delay )
			KEP_EventEncodeNumber( e, gSaveSettings.outer_gain )
			KEP_EventEncodeNumber( e, gSaveSettings.inner_angle )
			KEP_EventEncodeNumber( e, gSaveSettings.outer_angle )
			KEP_EventEncodeNumber( e, GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_X ) )
			KEP_EventEncodeNumber( e, GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_Y ) )
			KEP_EventEncodeNumber( e, GetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_Z ) )
			KEP_EventQueue( e )
		end
	end

	return true
end

function SetLoop(val)
	
	-- Set Loop
	gLoop = ToBOOL(ToBool(val))
	Log("SetLoop: val="..gLoop)

	-- Update Controls
	CheckBox_SetChecked(gHandles["chkLooping"], gLoop)

	-- Set Selected Sound Attribute
	SetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_LOOP, gLoop)
end

function chkLooping_OnCheckBoxChanged(handle, val)
	SetLoop(val)
	TriggerSound()
end

function SetLoopDelay(val)
	
	-- Set Loop Delay
	gLoopDelay = ClampVal(tonumber(val) or gLoopDelay, MIN_LOOP_DELAY, MAX_LOOP_DELAY)
	Log("SetLoopDelay: val="..gLoopDelay)

	-- Update Controls
	EditBox_SetText(gHandles["edtLoopDelay"], MathRound(gLoopDelay, 0), false)

	-- Set Selected Sound Attribute
	SetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_LOOP_DELAY, gLoopDelay)
end

function edtLoopDelay_OnEditBoxString(handle, val)
	MenuClearFocusThis()
end

function edtLoopDelay_OnFocusOut(handle)
	local val = EditBox_GetText(handle)
	SetLoopDelay(val)
	TriggerSound()
end

function SetRolloff(val)
	
	-- Set Pitch
	gRolloff = ClampVal(tonumber(val) or gRolloff, MIN_ROLLOFF, MAX_ROLLOFF)
	Log("SetRolloff: val="..gRolloff)

	-- Update Controls
	EditBox_SetText(gHandles["edtRollOff"], MathRound(gRolloff, 2), false)

	-- Set Selected Sound Attribute
	SetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR, gRolloff)
end

function edtRollOff_OnEditBoxString(handle, val)
	MenuClearFocusThis()
end

function edtRollOff_OnFocusOut(handle)
	local val = EditBox_GetText(handle)
	SetRolloff(val)
	TriggerSound()
end

function SetPitch(val)
	
	-- Set Pitch
	gPitch = ClampVal(tonumber(val) or gPitch, MIN_PITCH, MAX_PITCH)
	Log("SetPitch: val="..gPitch)

	-- Update Controls
	local sliderMax = Slider_GetRangeMax(gHandles["sldrPitch"])
	Slider_SetValue( gHandles["sldrPitch"], sliderMax * gPitch / MAX_PITCH )

	-- Set Selected Sound Attribute
	SetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_PITCH, gPitch)
end

function sldrPitch_OnSliderValueChanged(handle, val)
	local sliderMax = Slider_GetRangeMax(gHandles["sldrPitch"])
	SetPitch(MAX_PITCH * val / sliderMax)
	TriggerSound()
end

function SetGain(val)
	
	-- Set Gain
	gGain = ClampVal(tonumber(val) or gGain, MIN_GAIN, MAX_GAIN)
	Log("SetGain: val="..gGain)

	-- Update Controls
	local sliderMax = Slider_GetRangeMax(gHandles["sldrGain"])
	Slider_SetValue( gHandles["sldrGain"], sliderMax * gGain / MAX_GAIN )

	-- Set Selected Sound Attribute
	SetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_GAIN, gGain)
end

function sldrGain_OnSliderValueChanged(handle, val)
	local sliderMax = Slider_GetRangeMax(gHandles["sldrGain"])
	SetGain(MAX_GAIN * val / sliderMax)
	TriggerSound()
end

function SetRange(val)
	
	-- Set Range
	gRange = ClampVal(tonumber(val) or gRange, MIN_RANGE, MAX_RANGE)
	Log("SetRange: val="..gRange)

	-- Update Controls
	EditBox_SetText(gHandles["edtRange"], MathRound(gRange, 2), false)

	-- Set Selected Sound Attribute
	SetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_MAX_DISTANCE, gRange)
end

function edtRange_OnEditBoxString(handle, val)
	MenuClearFocusThis()
end

function edtRange_OnFocusOut(handle)
	local val = EditBox_GetText(handle)
	SetRange(val)
	TriggerSound()
end

function SetOuterGain(val)
	
	-- Set Outer Gain
	gOuterGain = ClampVal(tonumber(val) or gOuterGain, MIN_GAIN, MAX_GAIN)
	Log("SetOuterGain: val="..gOuterGain)

	-- Update Controls
	EditBox_SetText(gHandles["edtConeOuterGain"], MathRound(gOuterGain, 2), false)

	-- Set Selected Sound Attribute
	SetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN, gOuterGain)
end

function edtConeOuterGain_OnEditBoxString(handle, val)
	MenuClearFocusThis()
end

function edtConeOuterGain_OnFocusOut(handle)
	local val = EditBox_GetText(handle)
	SetOuterGain(val)
	TriggerSound()
end

function SetInnerAngle(val, autoAdj)
	
	-- Set Inner Angle
	gInnerAngle = ClampVal(tonumber(val) or gInnerAngle, MIN_ANGLE, MAX_ANGLE)
	Log("SetInnerAngle: val="..gInnerAngle)

	-- Auto Adjust ?
	if ToBool(autoAdj) and (gInnerAngle > gOuterAngle) then
		SetOuterAngle(gInnerAngle, false)
	end
	
	-- Update Controls
	Slider_SetValue(gHandles["sldInnerAngle"], gInnerAngle)
	EditBox_SetText(gHandles["edtConeInnerAngle"], MathRound(gInnerAngle, 2), false)

	-- Set Selected Sound Attribute
	SetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE, gInnerAngle)
end

function sldInnerAngle_OnSliderValueChanged(handle, val)
	SetInnerAngle(val, true)
	TriggerSound()
end

function edtConeInnerAngle_OnEditBoxString(handle, val)
	MenuClearFocusThis()
end

function edtConeInnerAngle_OnFocusOut(handle)
	local val = EditBox_GetText(handle)
	SetInnerAngle(val, true)
	TriggerSound()
end

function SetOuterAngle(val, autoAdj)
	
	-- Set Outer Angle
	gOuterAngle = ClampVal(tonumber(val) or gOuterAngle, MIN_ANGLE, MAX_ANGLE)
	Log("SetOuterAngle: val="..gOuterAngle)

	-- Auto Adjust ?
	if ToBool(autoAdj) and (gInnerAngle > gOuterAngle) then
		SetInnerAngle(gOuterAngle, false)
	end

	-- Update Controls
	Slider_SetValue(gHandles["sldOuterAngle"], gOuterAngle)
	EditBox_SetText(gHandles["edtConeOuterAngle"], MathRound(gOuterAngle, 2), false)

	-- Set Selected Sound Attribute
	SetSelectedSoundAttr(SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE, gOuterAngle)
end

function sldOuterAngle_OnSliderValueChanged(handle, val)
	SetOuterAngle(val, true)
	TriggerSound()
end

function edtConeOuterAngle_OnEditBoxString(handle, val)
	MenuClearFocusThis()
end

function edtConeOuterAngle_OnFocusOut(handle)
	local val = EditBox_GetText(handle)
	SetOuterAngle(val, true)
	TriggerSound()
end

function toggleDirectionalControls( active )
	Control_SetEnabled( gHandles["edtConeOuterGain"], active )
	Control_SetEnabled( gHandles["edtConeInnerAngle"], active )
	Control_SetEnabled( gHandles["edtConeOuterAngle"], active )
	Control_SetEnabled( gHandles["sldInnerAngle"], active )
	Control_SetEnabled( gHandles["sldOuterAngle"], active )
end

function SetOmni()

	-- Set Omni
	gRadioState = RDO_OMNI
	Log("SetOmni")

	-- Update Controls
	RadioButton_SetChecked( gHandles["rdoOmni"], true )
	toggleDirectionalControls( false )

	-- Set Selected Sound Attribute
	SetInnerAngle(MAX_ANGLE, false)
	SetOuterAngle(MAX_ANGLE, false)
end

function rdoOmni_OnRadioButtonChanged( radioButtonHandle )
	SetOmni()
	TriggerSound()
end

function SetDirectional()

	-- Set Directional
	gRadioState = RDO_DIRECTIONAL
	Log("SetDirectional")

	-- Update Controls
	RadioButton_SetChecked( gHandles["rdoDirectional"], true )
	toggleDirectionalControls( true )

	-- Set Selected Sound Attribute
	SetInnerAngle(gInnerAngle, false)
	SetOuterAngle(gOuterAngle, false)
end

function rdoDirectional_OnRadioButtonChanged( radioButtonHandle )
	SetDirectional()
	TriggerSound()
end

function saveSettings()
	Log("saveSettings")
	gSaveSettings = Settings()
	sendUpdateToServer()
end

function btnSave_OnButtonClicked(buttonHandle)
	saveSettings()
	g_revertOnClose = false
	MenuCloseThis()
end

function btnCancel_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end
