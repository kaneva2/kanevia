--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------------------------------------
-- InventoryHelper:
-- Contains API with features (searchable):
-- 1. Events - Overwrite in your inventory for custom functionality
-- 2. Inventory Initialization
-- 3. Icons
-- 4. Nameplates
-- 5. Hover
-- 6. Clicked
-- 7. Categories
-- 8. Pagination
-- 9. Search
-- 10. Highlight - Borders
-- 11. Quantity
-- 12. Tooltip
-- 13. Dialog Closing
-- 14. Helper Functions: Mainly for Internal Use
----------------------------------------------------------------------

----------------------------------------------------------------------
-- 1. Events - Overwrite in your inventory for custom functionality
----------------------------------------------------------------------
function inventoryUpdated(page, search, category, updatedFeature) 
	--Redefine this in your inventory to handle times the inventory is updated 
	--by changing categories, pages, etc

	--Updated feature lets you know what has been updated
end

function itemClicked(itemNumber)
	--Redefine this in your inventory to handle times an item is clicked
end

----------------------------------------------------------------------
-- 2. Inventory Initialization
-- Setup:
-- * Call InventoryHelper.initializeInventory to setup basic inventory
-- * Add registerInventoryHandlers and registerInventoryEvents to Handler and Event Registration
-- * Add function InventoryHelper.destroy() to OnDestroy
----------------------------------------------------------------------
DEFAULT_NUM_ICONS = 20

InventoryHelper 				= {}				-- Holds all of the InventoryHelper variables/functions
InventoryHelper.itemTable 		= nil 				-- table of items, used for hover and clicked
InventoryHelper.singleList		= false				-- Does this inventory pull from a single list for each tab/search/etc
InventoryHelper.itemsPerPage 	= DEFAULT_NUM_ICONS -- Number of items on the page

local g_tooltipIndex

-- Initialize an Inventory-style menu
-- itemTable: string of the table that holds the items to be displayed
-- OPTIONAL: singleListInventory: does the inventory pull from a single list for all pages (> max per page)
-- OPTIONAL: numItems: items per page (defaults to 20)
function InventoryHelper.initializeInventory(itemTable, singleListInventory, numItems) 

	InventoryHelper.itemTable = itemTable
	InventoryHelper.singleList = singleListInventory
	InventoryHelper.itemsPerPage = numItems or DEFAULT_NUM_ICONS
end

function InventoryHelper.registerInventoryHandlers()
	KEP_EventRegisterHandler( "showInventoryTooltipHandler", 	"ShowInventoryTooltipEvent", 	KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "showToolTipEventHandler", 		"ShowToolTipEvent", 			KEP.HIGH_PRIO )
end

function InventoryHelper.registerInventoryEvents()
	KEP_EventRegister( "UpdateFrameworkTooltipEvent", 	KEP.MED_PRIO )
	KEP_EventRegister( "ShowInventoryTooltipEvent", 	KEP.MED_PRIO )
	KEP_EventRegister( "CloseFrameworkTooltipEvent", KEP.MED_PRIO )
end

function InventoryHelper.destroy()
--	if InventoryHelper.currentTooltip ~= nil then
--		MenuClose("Framework_Tooltip.xml")
--	end
end

----------------------------------------------------------------------
-- 3. Icons
-- Required Controls:
-- * imgItem[1,max]: Image icon
-- * imgItemBG[1,max]: Background/border of image icon
-- * btnItem[1,max]: Invisible button in front of icon
-- Setup:
-- * Call InventoryHelper.setIcons to update all nameplates, returns unloaded images
----------------------------------------------------------------------
DEFAULT_LOADING_ICON = "loading.dds"

-- Set All Icons based on GLID or texture path
-- keyName: the key under which images or glids are stored
-- OPTIONAL: notFoundIcon: icon to display when value of key is nil in table
-- OPTIONAL: loadingIcon: icon to display if "loading" (value of key is empty or invalid)
-- OPTIONAL: hideKey: hide if this key is true
function InventoryHelper.setIcons(keyName, notFoundIcon, loadingIcon, hideKey)
	local pendingImages = {}

	for i = 1, InventoryHelper.itemsPerPage do
		local offsetIndex = calculateOffsetIndex(i)
		local item = _G[InventoryHelper.itemTable][offsetIndex]

		local iconHandle  = gHandles["imgItem" .. i]
		local iconVisible = item ~= nil

		-- If a hideKey is passed and is set to true, don't display
		if hideKey and iconVisible then
			local hideIcon = findKeyValueInTable(item, hideKey)
			iconVisible = not hideIcon
		end
		
		Control_SetVisible(iconHandle, iconVisible)
		Control_SetVisible(gHandles["imgItemBG" .. i],   iconVisible)
		Control_SetEnabled(gHandles["btnItem" .. i],     iconVisible)

		if iconVisible then
			local iconImage = Image_GetDisplayElement(iconHandle)
			local texturePath = findKeyValueInTable(item, keyName)
			local glid = texturePath

			if texturePath then
				-- It's a number, we know it's a GLID, get thumbnail based on that
				if tonumber(texturePath) then
					texturePath = KEP_AddItemThumbnailToDatabase(texturePath)
				end

				-- If the texture path is empty, add to pending and display loading
				if texturePath == "" then
					pendingImages[glid] = iconImage
					Element_AddExternalTexture(iconImage, gDialogHandle, loadingIcon or DEFAULT_LOADING_ICON, loadingIcon or DEFAULT_LOADING_ICON)
				else
					Element_AddExternalTexture(iconImage, gDialogHandle, texturePath, loadingIcon or DEFAULT_LOADING_ICON)
				end
			
			-- If the value found is nil, set to the default not found or loading
			else
				Element_AddTexture(iconImage, gDialogHandle, notFoundIcon or DEFAULT_LOADING_ICON)
			end

			-- Set to whole icon just in case
			Element_SetCoords(iconImage, 0, 0, -1, -1)
		end
	end

	return pendingImages
end


----------------------------------------------------------------------
-- 4. Nameplates
-- Required Controls:
-- * imgItemName[1,max]: Background of the item name
-- * stcItem[1,max]: Name of the item
-- Setup:
-- * Call InventoryHelper.setNameplates to update all nameplates
----------------------------------------------------------------------
-- Sets the nameplates of the menu
-- keyName: the key under which they are stored
-- OPTIONAL: hideKey: hide if this key is true
function InventoryHelper.setNameplates(keyName, hideKey)
	for i = 1, InventoryHelper.itemsPerPage do
		local offsetIndex = calculateOffsetIndex(i)
		local item = _G[InventoryHelper.itemTable][offsetIndex]
		local showItem = (item ~= nil)

		-- If a hideKey is passed and is set to true, don't display
		if hideKey then
			local hideIcon = findKeyValueInTable(item, hideKey)
			showItem = not hideIcon
		end

		local stcName = gHandles["stcItem" .. i]
		local nameBG = gHandles["imgItemName" .. i]

		-- Exit out if this component does not contain the expected elements
		if stcName == nil or nameBG == nil then return end

		if showItem then
			local name = stripTags(findKeyValueInTable(item, keyName), true)
			Static_SetText(gHandles["stcItem" .. i], name or "")
			Control_SetVisible(stcName, true)
			Control_SetVisible(nameBG, true)
		else
			Control_SetVisible(stcName, false)
			Control_SetVisible(nameBG, false)
		end
	end
end

----------------------------------------------------------------------
-- 5. Hover
-- Required Controls: 
-- * imgItem[1,max]: Image icon
-- * imgItemName[1,max]: Background of the item name
-- * stcItem[1,max]: Name of the item
-- * stcHoverLabel[1,max]: If set, displays a label on hover
-- Setup:
-- * Call InventoryHelper.initializeHover, with optional paramaters for label
----------------------------------------------------------------------
InventoryHelper.hoverLabel 		= nil 	-- If set, contains a table with key, value
InventoryHelper.itemNameHeight 	= 0 	-- Store this for resizing name plate purposes

-- If you want the hover state call this. 
-- Include labelText/Table/Key/Value to show the hover label
-- OPTIONAL: labelKey: the key to look for
-- OPTIONAL: labelValue: what value to compare to
-- OPTIONAL: labelText: use if you want to change the text
function InventoryHelper.initializeHover(labelKey, labelValue, labelText)


	InventoryHelper.itemNameHeight = Control_GetHeight(gHandles["imgItemName1"])

	if labelKey and labelValue then
		InventoryHelper.hoverLabel = {}
		InventoryHelper.hoverLabel.key = labelKey
		InventoryHelper.hoverLabel.value = labelValue
	end

	--Create functions for each icon for enter/leave
	for i = 1, InventoryHelper.itemsPerPage do
		_G["btnItem" .. tostring(i) .. "_OnMouseEnter"] = function(btnHandle)
			local name = string.match(Control_GetName(btnHandle), "(%d+)")
			InventoryHelper.setHover(tonumber(name), true)

			if not KEP_IsOwnerOrModerator() then
				InventoryHelper.clearClicked()
			end
		end
		
		_G["btnItem" .. tostring(i) .. "_OnMouseLeave"] = function(btnHandle)
			local name = string.match(Control_GetName(btnHandle), "(%d+)")
			InventoryHelper.setHover(tonumber(name), false)
		end

		if labelText then
			Static_SetText(gHandles["imgItemName" .. i], labelText or "")
		end
	end
end

function InventoryHelper.setHover(itemNumber, isHovering)
	local hoverLabel = gHandles["stcHoverLabel" .. itemNumber]

	--Don't reset if item is currently selected
	if isHovering or InventoryHelper.selectedIndex ~= itemNumber then
		local itemIcon = gHandles["imgItem" .. itemNumber]
		local itemNameBG = gHandles["imgItemName" .. itemNumber]
		local itemName = gHandles["stcItem" .. itemNumber]
		
		--Set the height/location of the overlay and item name
		local newY = Control_GetLocationY(itemIcon)
		local newH = Control_GetHeight(itemIcon)
		if not isHovering then
			newY = newY + newH - InventoryHelper.itemNameHeight
			newH = InventoryHelper.itemNameHeight
		end
		
		Control_SetLocationY(itemNameBG, newY)
		Control_SetLocationY(itemName, newY)
		Control_SetSize(itemNameBG, Control_GetWidth(itemNameBG), newH)
		Control_SetSize(itemName, Control_GetWidth(itemName), newH)

		--Set the Hover label
		if not InventoryHelper.hoverLabel then
			return
		end

		local offsetIndex = calculateOffsetIndex(itemNumber)
		local item = _G[InventoryHelper.itemTable][offsetIndex]
		if item and isHovering then
			local useType = findKeyValueInTable(item, InventoryHelper.hoverLabel.key)
			if useType == InventoryHelper.hoverLabel.value then
				Control_SetVisible(hoverLabel, true)
				return
			end
		end
	end

	-- Hide the hoverlabel because never caught
	if InventoryHelper.hoverLabel then
		Control_SetVisible(hoverLabel, false)
	end
end


----------------------------------------------------------------------
-- 6. Clicked
-- Required Controls: 
-- * stcItem[1,max]: statics that hole the name of the item
-- * imgItem[1,max]: image icon
-- * btnItem[1,max]: invisible button that covers the icon/name/etc
-- * imgClicked: frame that covers the icon
-- Setup:
-- * Create a frame imgClicked in the XML that has all controls on the frame setup to their proper locations
-- * Call initializeClicked with the controls that follow it
-- * Add InventoryHelper.onButtonDown to your Dialog_OnLButtonDownInside function
-- * Overwrite function itemClicked(itemNumber) to add additional functionality
----------------------------------------------------------------------
DEFAULT_FRAME_OFFSET_X = -6
DEFAULT_FRAME_OFFSET_Y = -5

InventoryHelper.selectedIndex 	= nil						-- Currently selected item
InventoryHelper.frameControls 	= nil						-- Controls the follow the frame, first one has default focus
InventoryHelper.frameOffsetX 	= DEFAULT_FRAME_OFFSET_X	-- How far the frame is offset on the x from the icon
InventoryHelper.frameOffsetY 	= DEFAULT_FRAME_OFFSET_Y	-- How far the frame is offset on the y from the icon

-- Initializes the clicked functionality
-- frameControls: Table of controls to move along with the frame - imgClicked (DO NOT include this)
--					The one you want to have focus on should be first
-- OPTIONAL: frameOffsetX: change the default offset from the icon in the horizontal
-- OPTIONAL: frameOffsetY: change the default offset from the icon in the vertical
function InventoryHelper.initializeClicked(frameControls, frameOffsetX, frameOffsetY)

	if frameOffsetX then
		InventoryHelper.frameOffsetX = frameOffsetX
	end

	if frameOffsetY then
		InventoryHelper.frameOffsetY = frameOffsetY
	end

	InventoryHelper.frameControls = frameControls

	--Create functions for each icon for clicking
	for i = 1, InventoryHelper.itemsPerPage do		
		_G["btnItem" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			local name = string.match(Control_GetName(btnHandle), "(%d+)")
			InventoryHelper.setClicked(tonumber(name))
		end
	end
end

-- Call this from Dialog_OnLButtonDownInside to complete clicked functionality
function InventoryHelper.onButtonDown(x, y)

	--Clear clicked if clicking anywhere in the dialog other than the clicked border
	if InventoryHelper.selectedIndex and Control_ContainsPoint(gHandles["imgClicked"], x, y) == 0 then
		InventoryHelper.clearClicked()
	end
end

-- Move the frame over the item and show all clicked buttons
-- User must overwrite the "itemClicked(itemNumber)" function for custom options
-- itemNumber: the item number that was clicked
function InventoryHelper.setClicked(itemNumber)

	InventoryHelper.selectedIndex = itemNumber
	local offsetIndex = calculateOffsetIndex(itemNumber)
	local item = _G[InventoryHelper.itemTable][offsetIndex]
	if item then
		--Set text height
		local itemName = gHandles["stcItem" .. itemNumber]
		local height = ( Control_GetHeight(gHandles["imgItem" .. itemNumber]) ) / 2
		Control_SetSize(itemName, Control_GetWidth(itemName), height)

		local frame = gHandles["imgClicked"]
		local offsetX = Control_GetLocationX(frame)
		local offsetY = Control_GetLocationY(frame)

		local btnItem = gHandles["btnItem" .. itemNumber]
		local frameX = Control_GetLocationX(btnItem) + InventoryHelper.frameOffsetX
		local frameY = Control_GetLocationY(btnItem) + InventoryHelper.frameOffsetY

		Control_SetLocation(frame, frameX, frameY)
		Dialog_MoveControlToFront(gDialogHandle, frame)
		Control_SetVisible(frame, true)

		offsetX = offsetX - frameX
		offsetY = offsetY - frameY
		for i, v in pairs(InventoryHelper.frameControls) do
			local ctrlHandle = gHandles[v]
			Control_SetLocation(ctrlHandle,
								Control_GetLocationX(ctrlHandle) - offsetX,
								Control_GetLocationY(ctrlHandle) - offsetY)
			Dialog_MoveControlToFront(gDialogHandle, ctrlHandle)
			Control_SetVisible(ctrlHandle, true)
		end

		itemClicked(itemNumber)

		local default = InventoryHelper.frameControls[1]
		Control_SetFocus(gHandles[default])
	end
end

-- Clear the clicked border
function InventoryHelper.clearClicked()

	for i, v in pairs(InventoryHelper.frameControls) do
		local ctrlHandle = gHandles[v]
		Dialog_MoveControlToBack(gDialogHandle, ctrlHandle)
		Control_SetVisible(ctrlHandle, false)
	end

	Dialog_MoveControlToBack(gDialogHandle, gHandles["imgClicked"])
	Control_SetVisible(gHandles["imgClicked"], false)
	
	--Set hover, use placeholder as hover only changes if selected item doesn't equal hover
	local tempSelected = InventoryHelper.selectedIndex or 1
	InventoryHelper.selectedIndex = nil
	InventoryHelper.setHover(tempSelected, false)
end


----------------------------------------------------------------------
-- 7. Categories
-- Required Controls:
-- * btn[CategoryNames]: Buttons named by each category. Enabled/disabled states corespond to inactive/current category
-- Setup:
-- * Call InventoryHelper.initializeCategories
----------------------------------------------------------------------
InventoryHelper.categories 	= {} 		-- List of categories, indexed by name with values of the button names
InventoryHelper.category 	= nil 		-- Current Category

-- If you want Categories call this
-- categoryArray: 	Array of the categories. 
-- 					Entries must match "btn[entry value]" to XML
-- defaultCategory: the default starting category. Only sets if category is unset
function InventoryHelper.initializeCategories(categoryArray, defaultCategory)
	InventoryHelper.categories = categoryArray
	InventoryHelper.category = InventoryHelper.category or defaultCategory -- Only set default category if not already set to avoid race conditions

	for category, button in pairs(categoryArray) do
		_G[button .. "_OnButtonClicked"] = function(btnHandle)
			InventoryHelper.setCategory(category)
		end
	end
end

-- Update the category icons and set the new category then notify the change
-- category: the name of the category to change to
function InventoryHelper.setCategory(category)

	-- If the category is hidden, don't want to change to it
	if Control_GetVisible(gHandles[InventoryHelper.categories[category]]) == 0 then
		return
	end

	local oldCategory = InventoryHelper.category

	if oldCategory == category then
		return
	end

	InventoryHelper.category = category

	Control_SetEnabled(gHandles[InventoryHelper.categories[oldCategory]], true)
	Control_SetEnabled(gHandles[InventoryHelper.categories[category]], false)

	InventoryHelper.page = 1
	inventoryUpdated(InventoryHelper.page, InventoryHelper.searchText, InventoryHelper.category, "category")
end

----------------------------------------------------------------------
-- 8. Pagination
-- Required Controls: 
-- * btnBack: Button that goes back a page
-- * btnNext: Button that goes forward a page
-- * stcPage: If page display, shows the page info
-- Setup:
-- * Call InventoryHelper.initializePageNumbers to display page numbers
-- * Must set max pages on updated with InventoryHelper.setMaxPages
-- * Can use gotoPage or updatePageDisplay to manually change pages or update arrows/text
----------------------------------------------------------------------
InventoryHelper.page 			= 1 	-- Current page
InventoryHelper.pageMax 		= 1 	-- Max Page - should be set by Inventory
InventoryHelper.pagePattern 	= nil 	-- If set, should be a string pattern that contains 2 digits to fill in page and page max

-- If you want page numbers to display do it here. Includes default display
-- OPTIONAL: pattern: how to display the page. Must include 2 %d's
function InventoryHelper.initializePageNumbers(pattern)
	InventoryHelper.pagePattern = pattern or "(%d/%d)"
end

-- Set the max pages based on the total number of items
-- totalItems: total number of items in the set (part of a category/search/etc)
function InventoryHelper.setMaxPages(totalItems)
	InventoryHelper.pageMax = maxPages(totalItems)
	InventoryHelper.updatePageDisplay()
end

-- Goto the page inputted. Must be within [1,max]
-- pageNum: Page to go to
function InventoryHelper.gotoPage(pageNum)
	if pageNum >= 1 and pageNum <= InventoryHelper.pageMax then
		InventoryHelper.page = pageNum
		InventoryHelper.updatePageDisplay()
	end

	inventoryUpdated(InventoryHelper.page, InventoryHelper.searchText, InventoryHelper.category, "page")
end

-- Updates the Page Icons/Numbers if applicable
function InventoryHelper.updatePageDisplay()
	Control_SetEnabled(gHandles["btnBack"], InventoryHelper.page > 1)

	Control_SetEnabled(gHandles["btnNext"], InventoryHelper.page < InventoryHelper.pageMax)

	if InventoryHelper.pagePattern then
		local text = string.format(InventoryHelper.pagePattern, InventoryHelper.page, InventoryHelper.pageMax)
		Static_SetText(gHandles["stcPage"], text)
	end
end

function btnBack_OnButtonClicked(btnHandle)
	InventoryHelper.gotoPage(InventoryHelper.page - 1)
end

function btnNext_OnButtonClicked(btnHandle)
	InventoryHelper.gotoPage(InventoryHelper.page + 1)
end


----------------------------------------------------------------------
-- 9. Search
-- Required Controls: 
-- * imgClearSearch: image when clicked clears the search
-- * txtSearch: text box that the player types in
-- Setup:
-- * None
----------------------------------------------------------------------
InventoryHelper.searchText 		= ""	-- Current text of the Search Box
InventoryHelper.clearingSearch 	= true 	-- Whether the search is queued to be cleared on next focus in

function InventoryHelper.clearSearch()
	Control_SetVisible(gHandles["imgClearSearch"], false)
	InventoryHelper.clearingSearch = true
	local searchBox = gHandles["txtSearch"]
	EditBox_ClearText(searchBox)
	txtSearch_OnFocusOut(searchBox)
	txtSearch_OnEditBoxChange( searchBox, "")
	Dialog_ClearFocus(gDialogHandle)
end

function txtSearch_OnEditBoxChange( editboxHandle, text)

	if (InventoryHelper.searchText ~= text) then
		InventoryHelper.searchText = text

		--When the edit box is typed in, if it's longer than 1 letter, request inventory
		if InventoryHelper.searchText then
			local searchLen = string.len(InventoryHelper.searchText)
			if searchLen > 0 then
				Control_SetVisible(gHandles["imgClearSearch"], true)
			end

			if searchLen ~= 1 then
				--Reset to page 1 on searching
				InventoryHelper.page = 1
				InventoryHelper.updatePageDisplay()
				inventoryUpdated(InventoryHelper.page, InventoryHelper.searchText, InventoryHelper.category, "search")
			end
		end
	end
end

function txtSearch_OnFocusIn(editBoxHandle)

	-- On enter, set the text to blank
	if InventoryHelper.clearingSearch then
		EditBox_SetText(editBoxHandle, "", true)
	end
	InventoryHelper.clearingSearch = false
end

function txtSearch_OnFocusOut(editBoxHandle)
	local search = EditBox_GetText(editBoxHandle)
	
	-- On exit, if blank, reset the text to "Search|"
	if search == "" or InventoryHelper.clearingSearch then
		InventoryHelper.clearingSearch = true
		EditBox_SetText(editBoxHandle, "Search", false)
	else
		InventoryHelper.clearingSearch = false
	end
end

function imgClearSearch_OnLButtonUp(ctrlHandle)
	if Control_GetVisible(ctrlHandle) then
		InventoryHelper.clearSearch()
	end
end

----------------------------------------------------------------------
-- 10. Highlight - Borders
-- Required Controls:
-- * imgItem[1,max]: image icon
-- * imgItemBG[1,max]: Background/border of image icon
-- Setup:
-- * Call setHighlights to update all
----------------------------------------------------------------------
DEFAULT_DEFAULT_COLOR = {a=255, r=0, g=0, b=0}
DEFAULT_HIGHLIGHT_COLOR = {a=255, r=255, g=255, b=255}
DEFAULT_BORDER_DIM_LEVEL = 127

-- Sets the highlight on the border of the background
-- keyName: the key under which they are stored
-- OPTIONAL: defaultColor: the default color of the background in {a, r, g, b}
-- OPTIONAL: highlightColor: the hightlight color of the background in {a, r, g, b}
-- OPTIONAL: dimIcon: how dark to dim the icon, 0-255 where 255 full brightness
function InventoryHelper.setHightlights(keyName, defaultColor, highlightColor, dimIcon)
	
	for i = 1, InventoryHelper.itemsPerPage do
		local offsetIndex = calculateOffsetIndex(i)
		local item = _G[InventoryHelper.itemTable][offsetIndex]

		defaultColor =  	defaultColor 	or DEFAULT_DEFAULT_COLOR
		highlightColor = 	highlightColor 	or DEFAULT_HIGHLIGHT_COLOR
		dimIcon = 	 		dimIcon			or DEFAULT_BORDER_DIM_LEVEL

		if item then
			Control_SetVisible(gHandles["imgItemBG" .. i], true)

			local highlight = findKeyValueInTable(item, keyName)
			local blendIcon =  Element_GetTextureColor(Image_GetDisplayElement(gHandles[("imgItem" .. i)]))
			local blendBG =  Element_GetTextureColor(Image_GetDisplayElement(gHandles[("imgItemBG" .. i)]))
			
			if highlight then
				BlendColor_SetColor(blendIcon, 0, {a=255, r=dimIcon, g=dimIcon, b=dimIcon})
				BlendColor_SetColor(blendBG, 0, highlightColor)
			else
				BlendColor_SetColor(blendIcon, 0, {a=255, r=255, g=255, b=255})
				BlendColor_SetColor(blendBG, 0, defaultColor)
			end
		end
	end
end


----------------------------------------------------------------------
-- 11. Quantity
-- Required Controls:
-- * stcItemCount[1,max]: Holds the quantities of items, visible if > 0
-- * imgItemCountBG[1,max]: Background for quantities of items, visible if > 0
-- Setup:
-- * Call setQuantities to update all
-- * Call setQuantity to set individual icons
----------------------------------------------------------------------
-- Set the quantity labels if there is an amount, otherwise hide it
-- keyName: the key under which they are stored
-- OPTIONAL: hideKey: manually hide
function InventoryHelper.setQuantities(keyName, hideKey)

	for i = 1, InventoryHelper.itemsPerPage do
		local offsetIndex = calculateOffsetIndex(i)
		local item = _G[InventoryHelper.itemTable][offsetIndex]
		local showItem = item ~= nil

		-- If a hideKey is passed and is set to true, don't display
		if hideKey then
			local hideIcon = findKeyValueInTable(item, hideKey)
			showItem = not hideIcon
		end

		-- Exit out if this component does not contain the expected elements
		if gHandles["stcItemCount" .. i] == nil or gHandles["imgItemCountBG" .. i] == nil then return end

		if showItem then
			local quantity = tonumber( findKeyValueInTable(item, keyName) or 0)
			InventoryHelper.setQuantity(i, quantity)
		else
			Control_SetVisible(gHandles["stcItemCount" .. i], false)
			Control_SetVisible(gHandles["imgItemCountBG" .. i], false)
		end

		-- set the position of the item count and the background
		local countWidth = Control_GetWidth(gHandles["imgItemCountBG".. i])
		local itemWidth = Control_GetWidth(gHandles["imgItemBG".. i])
		local itemPositionX = Control_GetLocationX(gHandles["imgItemBG".. i])

		--log("---------------------------- QUANITITY")
		--log("--- countWidth : ".. tostring(countWidth))
		--log("--- itemWidth : ".. tostring(itemWidth))
		--log("--- itemPositionX : ".. tostring(itemPositionX))
		--log("--- result : ".. tostring(itemPositionX + itemWidth - countWidth))

		Control_SetLocationX(gHandles["stcItemCount".. i], itemPositionX + itemWidth - countWidth)
		Control_SetLocationX(gHandles["imgItemCountBG".. i], itemPositionX + itemWidth - countWidth)
	end
end

-- Set the quantity of an item
-- itemNumber: which to set
-- amount: amount of quantity to set
function InventoryHelper.setQuantity(itemNumber, quantity)
	if gHandles["stcItemCount" .. itemNumber] and gHandles["imgItemCountBG" .. itemNumber] then
		local quantityLabel = gHandles["stcItemCount" .. itemNumber]
		Control_SetVisible(quantityLabel, quantity > 0)
		Static_SetText(quantityLabel, formatNumberSuffix(quantity))
		Control_SetVisible(gHandles["imgItemCountBG" .. itemNumber], quantity > 0)
	end
end

----------------------------------------------------------------------
-- 12. Tooltip
-- Required Controls:
-- * btnItem[1,max]: button for each item
-- Setup:
-- * Add registerInventoryHandlers() to the EventHandler Registration
-- * Add registerInventoryEvents() to the Event Registration
-- * Call InventoryHelper.initializeTooltips to show the framework tooltips
----------------------------------------------------------------------
--[[Example Tooltip table:
	{
		header 		= "This is a Title, it will be set based on keys of the itemTable defined in initializeInventory",
		subheader 	= { data 	= "you can set any field to have a color by creating a subtable",
						color 	= {a = 255, r = 255, g = 255, b = 255} },
		body 		= "Colors are based on ARGB",
		footer1 	= "if there is no value or data for a field it will be hidden",
		footer2		= {	label 	= "A label is a static value that is appended to the front of the data"
						data 	= "If there is a label ': ' will be added to the front of it"
						suffix	= "A suffix is also a static value added to the end of the data"},
		footer3		= {	data 	= "labels and suffixes can be combined in any pattern, one does not need the other",
						suffix 	= "The format is label .. ': ' .. data .. suffix. Only data is required"}
	}
--]]
InventoryHelper.tooltipTable 	= {} 	-- Default tooltip table to pull from
InventoryHelper.tooltipEvent 	= nil	-- Store the tooltip event
InventoryHelper.tooltipDelay 	= 0 	-- Delay before the tooltip shows
InventoryHelper.currentTooltip 	= nil 	-- Currently displayed tooltip
InventoryHelper.tooltipHideKey	= nil	-- A key if true the prevents a tooltip from showing

FIELDS = {"header", "subheader", "body", "durability", "footer1", "footer2", "footer3"}

-- Call to initialize Framework Tooltips to be shown
-- tooltipTable: a table as defined above
-- hideKey: If the key is true for an item, hide it
-- delay: override default time to display
function InventoryHelper.initializeTooltips(tooltipTable, hideKey, delay)

	for i = 1, InventoryHelper.itemsPerPage do
		Control_SetToolTip(gHandles["btnItem" .. i], Dialog_GetXmlName(gDialogHandle) .. "FRAMEWORKTOOLTIP" .. i)
	end

	InventoryHelper.tooltipTable = tooltipTable or {}
	InventoryHelper.tooltipHideKey = hideKey
	InventoryHelper.tooltipDelay = delay or InventoryHelper.tooltipDelay
end

-- Setup Tooltip on a single object
function InventoryHelper.addFrameworkTooltip(controlName, tooltipTable)
	--log("--- InventoryHelper.addFrameworkTooltip ")
	InventoryHelper.tooltipTable = tooltipTable
	--printTable(tooltipTable)
	Control_SetToolTip(gHandles[controlName], Dialog_GetXmlName(gDialogHandle) .. "FRAMEWORKTOOLTIP" .. controlName)
end

-- print the table 
function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end 

-- Remove framework tooltips from all buttons
function InventoryHelper.removeFrameworkTooltips()
	for i = 1, InventoryHelper.itemsPerPage do
		Control_SetToolTip(gHandles["btnItem" .. i], "")
	end
end

function showToolTipEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local title = KEP_EventDecodeString(event)
	local body = KEP_EventDecodeString(event)
	local mouseX = KEP_EventDecodeNumber(event)
	local mouseY = KEP_EventDecodeNumber(event)
	
	local menuName, tooltipNum = string.match(body, "^(.-)FRAMEWORKTOOLTIP(%d-)$")

	--log("--- InventoryHelper showToolTipEventHandler")
	
	-- If a shotToolTipEvent was fired for a menu other than me
	if menuName ~= Dialog_GetXmlName(gDialogHandle) then
		--log("--- showToolTipEventHandler 1")
		if g_tooltipIndex then
			if InventoryHelper.tooltipEvent then
				KEP_EventCancel(InventoryHelper.tooltipEvent)
			end
			InventoryHelper.currentTooltip = nil
			KEP_EventCreateAndQueue("CloseFrameworkTooltipEvent")
			g_tooltipIndex = nil
		end
	else
		--log("--- showToolTipEventHandler 2")
		-- Are we mousing over a valid item for tooltip display?
		if tooltipNum then
			tooltipNum = tonumber(tooltipNum)
			-- Are we mousing over a new item?
			if tooltipNum ~= g_tooltipIndex then
				g_tooltipIndex = tooltipNum
				
				local offsetIndex = calculateOffsetIndex(g_tooltipIndex)
				local item = _G[InventoryHelper.itemTable][offsetIndex]
				
				-- Does this item exist?
				if item and item.UNID ~= 0 then
					-- Queue up a ShowToolTipNowEvent in a second
					InventoryHelper.tooltipEvent = KEP_EventCreate("ShowInventoryTooltipEvent")
					KEP_EventEncodeString(InventoryHelper.tooltipEvent, menuName)
					KEP_EventEncodeNumber(InventoryHelper.tooltipEvent, tooltipNum)
					KEP_EventEncodeNumber(InventoryHelper.tooltipEvent, mouseX)
					KEP_EventEncodeNumber(InventoryHelper.tooltipEvent, mouseY)
					KEP_EventQueueInFutureMS(InventoryHelper.tooltipEvent, InventoryHelper.tooltipDelay)
				end
			end
		-- If we're leaving a valid tooltip item
		elseif g_tooltipIndex then
			if InventoryHelper.tooltipEvent then
				KEP_EventCancel(InventoryHelper.tooltipEvent)
			end
			InventoryHelper.currentTooltip = nil
			KEP_EventCreateAndQueue("CloseFrameworkTooltipEvent")
			g_tooltipIndex = nil
		end
	end
	--[[local hidingFWTooltip = title == "" and body == "" and InventoryHelper.tooltipEvent ~= nil
	if (menuName ~= Dialog_GetXmlName(gDialogHandle) or not tooltipNum) and not hidingFWTooltip then
		return KEP.EPR_OK
	end]]
	
	--return KEP.EPR_CONSUMED
end

-- Internal: catches the event and handles whether to show a FW tooltip
function showInventoryTooltipHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local menuName 		= KEP_EventDecodeString(event)
	local tooltipNum 	= KEP_EventDecodeNumber(event)
	local mouseX 		= KEP_EventDecodeNumber(event)
	local mouseY 		= KEP_EventDecodeNumber(event)
	
	if menuName ~= Dialog_GetXmlName(gDialogHandle) then
		return KEP.EPR_OK
	end

	if InventoryHelper.currentTooltip == tooltipNum then
	-- request to show same stuff, so ignore it
	else
		-- request to update
		InventoryHelper.currentTooltip = tooltipNum 
		updateTooltip(menuName, tooltipNum, mouseX, mouseY)
	end

	return KEP.EPR_CONSUMED
end

-- Internal: Open the framework tooltip menu if not open and send an event to update the fields based on the tooltipTable
function updateTooltip(menuName, index, x, y)
	local offsetIndex = calculateOffsetIndex(index)
	local item = _G[InventoryHelper.itemTable][offsetIndex]

	if item == nil or (InventoryHelper.tooltipHideKey and findKeyValueInTable(item, InventoryHelper.tooltipHideKey)) then
		return
	end
	
	local event = KEP_EventCreate("UpdateFrameworkTooltipEvent")

	-- if it contains backslash it's a full directory location, pull out just the xml
	if string.find(menuName, "\\") then
		menuName = string.match(menuName, "^.*\\([^\\]-%.xml)$")
	end

	KEP_EventEncodeString(event, menuName)
	KEP_EventEncodeNumber(event, x)
	KEP_EventEncodeNumber(event, y)
	
	KEP_EventEncodeString(event, cjson.encode(item.tooltip))

	MenuOpen("Framework_Tooltip.xml")
	KEP_EventQueue(event)
end

-- DEPRECATED

-- Returns the name and color values
-- "" and -1 are defaults respectively
--[[function findFieldInfo(item, field)
	local label, text, suffix = "", "", ""
	local color = {a = -1, r = -1, g = -1, b = -1}
	
	if (item ~= nil and type(item) == "table" and field ~= nil and (type(field) == "string" or type(field) == "table")) then
		-- if the item has a field for this, check it first
		local itemFieldValue = nil
		if item.tooltip then
			itemFieldValue = item.tooltip[field]
		end
		defaultFieldValue = InventoryHelper.tooltipTable[field]
		
		if itemFieldValue or defaultFieldValue then
			-- Find if there's text, check through item first then default
			if type(itemFieldValue) == "string" then
				text = tostring(findKeyValueInTable(item, itemFieldValue))
			elseif itemFieldValue and itemFieldValue.data then
				-- Custom snag for durability
				if field == "durability" then
					label = tostring(itemFieldValue.data)
				else
					text = tostring(findKeyValueInTable(item, itemFieldValue.data))
				end
			elseif type(defaultFieldValue) == "string" then
				text = tostring(findKeyValueInTable(item, defaultFieldValue))
			elseif defaultFieldValue and defaultFieldValue.data then
				text = tostring(findKeyValueInTable(item, defaultFieldValue.data))
			end

			-- The rest, only check if there's text
			local checkTable = text ~= "" and (type(itemFieldValue) == "table" or type(defaultFieldValue) == "table")
			if checkTable then
				color = checkField(itemFieldValue, defaultFieldValue, "color", "table") or color
				label = checkField(itemFieldValue, defaultFieldValue, "label", "string") or label
				suffix = checkField(itemFieldValue, defaultFieldValue, "suffix", "string") or suffix
			end
		end
	end

	text = text:gsub("^%l", string.upper)

	return label, text, suffix, color.a, color.r, color.g, color.b
end

-- DEPRECATED

function checkField(table1, table2, fieldName, valueType)
	if type(table1) == "table" and table1[fieldName] and type(table1[fieldName]) == valueType then
		return table1[fieldName]
	elseif type(table2) == "table" and table2[fieldName] and type(table2[fieldname]) == valueType then
		return table2[fieldName]
	end
	return nil
end]]

----------------------------------------------------------------------
-- 13. Dialog Closing
-- Required Controls:
-- * btnClose: Button that closes the menu
-- Setup:
-- * Add InventoryHelper.OnKeyDown to your Dialog_OnKeyDown function
----------------------------------------------------------------------
function InventoryHelper.OnKeyDown(key)
	
	-- < key
	if key == 188 then
		InventoryHelper.gotoPage(InventoryHelper.page - 1)
	-- > key
	elseif key == 190 then
		InventoryHelper.gotoPage(InventoryHelper.page + 1)
	end
end


----------------------------------------------------------------------
-- 14. Helper Functions: Mainly for Internal Use
----------------------------------------------------------------------
-- Find a key's value in a table or its sub-tables
-- Table: the table to look through
-- KeyName: the keyname to find. If a table: The order of keys to find
function findKeyValueInTable(table, keyName)
	if (table == nil or type(table) ~= "table" or keyName == nil) then
		return nil
	end

	-- If it's not a table, make it a table of size 1
	if type(keyName) ~= "table" then
		keyName = {keyName}
	end

	-- Go through all keys in the table
	for i, key in pairs(keyName) do
		-- If the table contains the key, return it
		if table[key] then
			return table[key]
		end

		-- If it doesn't check sub tables
		for i,v in pairs(table) do
			if type(v) == "table" then
				local value = findKeyValueInTable(v, key)

				if value ~= nil then return value end
			end
		end
	end

	return nil -- Don't return false because a value may purposefully be false
end

-- If it's a single list, where to pull from based on page
function calculateOffsetIndex(index)
	if InventoryHelper.singleList then
		index = ((InventoryHelper.page - 1) * InventoryHelper.itemsPerPage) + index
	end
	return index
end

-- Get the total number of items from the XML data and divide by items/page
function maxPages(totalItems)
	totalItems = tonumber(totalItems or 1)
	if totalItems == 0 then
		totalItems = 1
	end
	return math.ceil(totalItems/InventoryHelper.itemsPerPage)
end


function formatNumberSuffix(count)
	-- If no need for formatting, then return
	if count < 10000 then return count end

	local strCount = tostring(count)
	local newCount = ""

	local format = { 	max = 0,
						min = 0,
					 	suffix = ""
					}

	-- Thousands
	if #strCount <= 6 then
		format.max = 6
		format.min = 3
		format.suffix = "K"
	-- Millions
	elseif #strCount <= 9 then
		format.max = 9
		format.min = 6
		format.suffix = "M"
	-- Billions
	elseif #strCount <= 12 then
		format.max = 12
		format.min = 9
		format.suffix = "B"
	-- Trillions
	elseif #strCount <= 15 then
		format.max = 15
		format.min = 12
		format.suffix = "T"
	end

	for i = 1, #strCount do
		local diff = #strCount - i
		if #strCount <= format.max then
			if diff >= format.min then
				newCount = newCount..string.sub(strCount,i,i)
			else
				return newCount..format.suffix
			end
		end
	end
end
