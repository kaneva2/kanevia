--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("PageButtons.lua")
dofile("MenuStyles.lua")
dofile("MenuLocation.lua")

-- Catch the event from the friends and people menu telling us who the gift is for
PLAYER_USERID_EVENT = "PlayerUserIdEvent"

-- website addresses for queries
MESSAGE_CENTER_SUFFIX = "kgp/messagecenter.aspx?action="
GIFT_CATALOG_SUFFIX = "GetGiftCatalog&catId="
SEND_GIFT_SUFFIX = "GiveGift&toId="
SUBJECT_SUFFIX = "&subject="
MESSAGE_SUFFIX = "&message="
GIFT_ID_SUFFIX = "&giftId="

default_subject = " has sent you a gift!"

-- Current web address used for queries
g_web_address = nil
g_selectedIndex = -1

g_t_gifts = {}

-- User name and id to recieve gift
g_userID = 0
g_userName = nil

-- Default messages
default_msgs = {}
table.insert(default_msgs, "I hope you like this gift.")
table.insert(default_msgs, "I saw this gift and thought of you.")
table.insert(default_msgs, "I hope this gift makes your day a little better.")
table.insert(default_msgs, "Thanks for being my friend!")
table.insert(default_msgs, "Gaelin told me this was cool.")
table.insert(default_msgs, "I love buying things and thought I would share my love.")
table.insert(default_msgs, "This meets the afrech standard.")
table.insert(default_msgs, "You are worth it.")
table.insert(default_msgs, "Please enjoy this gift!")
table.insert(default_msgs, "jonnyboy loves this!")
table.insert(default_msgs, "Have a great day!")
table.insert(default_msgs, "Please accept this token of kindness.")
table.insert(default_msgs, "Enjoy!")
table.insert(default_msgs, "Hope this brightens your day!")
table.insert(default_msgs, "I couldn't resist getting this for you.")

g_message = nil

g_parseResults = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "PlayerUserIdEventHandler", PLAYER_USERID_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( PLAYER_USERID_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	--select a random canned message from table above
	g_message = default_msgs[math.random(1,#default_msgs)]

	EditBox_SetText( gHandles["editMessage"], g_message, false )
end

function PlayerUserIdEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_userID = KEP_EventDecodeNumber( event )
	g_userName = KEP_EventDecodeString( event )
	ebh = Dialog_GetEditBox( gDialogHandle, "editSendTo" )
	if ebh ~= nil then
		EditBox_SetText( gHandles["editSendTo"], g_userName, false )
	end

	local myName = KEP_GetLoginName()
	EditBox_SetText( gHandles["editSubject"], myName .. default_subject, false )

	catalogId = 1 -- TODO catalog is split up by catalog id (NPC)

	makeWebCall( GameGlobals.WEB_SITE_PREFIX..MESSAGE_CENTER_SUFFIX..GIFT_CATALOG_SUFFIX..tostring(catalogId), WF.GIFT_CATALOG, 1, 10)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.GIFT_CATALOG then
		if g_parseResults == true then
			results = KEP_EventDecodeString( event )
			ParseResults( results )
		else
			g_giftData = KEP_EventDecodeString( event )
			ParseGiftData()
		end
	end
end

function ParseResults( results )
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string
	s, e, result = string.find(results, "<ReturnCode>(.-)</ReturnCode>")
	if result ~= nil then
		if result == "0" then
			KEP_MessageBox("Your gift has been sent.")
			MenuCloseThis()
		elseif result == "-99" then
			KEP_MessageBox("Your gift cannot be sent.  That person is blocking you.")
		elseif result == "-98" then
			KEP_MessageBox("Insufficient funds to send that gift.")
		elseif result == "-97" then
			KEP_MessageBox("You cannot send yourself a gift.")
		else
			KEP_MessageBox("The gift was unable to be sent.")
		end
	end
end

-- Parse friend data from server
function ParseGiftData()
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local giftName = "" 		-- name of gift
	local giftDesc = ""     -- gift description
	local giftPrice = 0     -- gift price
	local giftID = 0        -- identification number of gift
	local userID = 0      -- Kaneva id of player
	s, strPos, result = string.find(g_giftData, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then

		s, e, numRec = string.find(g_giftData, "<NumberRecords>([0-9]+)</NumberRecords>")
		numRec = tonumber(numRec)

		local lbhDesc = Dialog_GetListBox(gDialogHandle, "lstDescription")
		local lbhPrice = Dialog_GetListBox(gDialogHandle, "lstPrice")
		local lbhThumb = Dialog_GetListBox(gDialogHandle, "lstThumbnail")

		g_t_gifts = {}

		for i=1,numRec do
			s, strPos, giftID = string.find(g_giftData, "<gift_id>(.-)</gift_id>", strPos)
			s, strPos, giftName = string.find(g_giftData, "<name>(.-)</name>", strPos)
			s, strPos, giftDesc= string.find(g_giftData, "<description>(.-)</description>", strPos)
			s, strPos, giftPrice = string.find(g_giftData, "<price>(%d+)</price>", strPos)

			local gift = {}
			local giftInfo = { KEP_GetItemInfo(tonumber(giftID)) }

			gift["ID"] = tonumber(giftID)
			gift["name"] = giftName
			gift["desc"] = giftDesc
			gift["price"] = tonumber(giftPrice)
			gift["icon"] = giftInfo
			table.insert(g_t_gifts, gift)
		end
		populateGifts()
	end
end

function populateGifts()
	for i,v in ipairs(g_t_gifts) do
		Static_SetText(gHandles["lblGiftName" .. i], v["name"])
		Static_SetText(gHandles["lblGiftDescription" .. i], v["desc"])
		Static_SetText(gHandles["lblGiftPrice" .. i], v["price"])
		KEP_LoadIconTextureByID(v["ID"], Image_GetDisplayElement(gHandles["imgGiftIcon" .. i]), gDialogHandle)
	end
end

function selectGift(selectedIndex)
	for i=1,10 do
		local element = Image_GetDisplayElement(gHandles["imgGiftWhiteBG" .. i])
		Element_AddTexture(element, gDialogHandle, "white.tga")
		Element_SetCoords(element, 1, 1, 3, 3)
	end

	g_selectedIndex = selectedIndex
	local element = Image_GetDisplayElement(gHandles["imgGiftWhiteBG" .. g_selectedIndex])
	Element_AddTexture(element, gDialogHandle, "25pGray.tga")
	Element_SetCoords(element, 0, 0, 4, 4)
end

function btnGift1_OnButtonClicked( buttonHandle )
	selectGift(1)
end

function btnGift2_OnButtonClicked( buttonHandle )
	selectGift(2)
end

function btnGift3_OnButtonClicked( buttonHandle )
	selectGift(3)
end

function btnGift4_OnButtonClicked( buttonHandle )
	selectGift(4)
end

function btnGift5_OnButtonClicked( buttonHandle )
	selectGift(5)
end

function btnGift6_OnButtonClicked( buttonHandle )
	selectGift(6)
end

function btnGift7_OnButtonClicked( buttonHandle )
	selectGift(7)
end

function btnGift8_OnButtonClicked( buttonHandle )
	selectGift(8)
end

function btnGift9_OnButtonClicked( buttonHandle )
	selectGift(9)
end

function btnGift10_OnButtonClicked( buttonHandle )
	selectGift(10)
end

function btnSend_OnButtonClicked( buttonHandle )
	sendGiftRequest()
end

function sendGiftRequest()
	local text = nil
	if (g_selectedIndex >= 0) then
		local giftID = g_t_gifts[g_selectedIndex]["ID"]
		local subject = default_subject
		g_message = g_t_gifts[g_selectedIndex]["name"]
		if g_userID ~= nil then

			local ebhM = Dialog_GetEditBox(gDialogHandle, "editMessage")
			if ebhM ~= "" then
				length = EditBox_GetTextLength( ebhM )
				text = EditBox_GetText(ebhM)
				if length > 258 then
					text = string.sub(text, 1 , 258)
				end
				if text ~= nil then
					g_message = g_message.." -- "..text
				end
			end

			local ebhS = Dialog_GetEditBox(gDialogHandle, "editSubject")
			if ebhS ~= nil then
				length = EditBox_GetTextLength( ebhS )
				text = EditBox_GetText(ebhS)
				if length > 258 then
					text = string.sub(text, 1 , 258)
				end
				if text ~= "" then
					subject = text
				end
			end

			-- create really long url
			local web_address = GameGlobals.WEB_SITE_PREFIX..MESSAGE_CENTER_SUFFIX..SEND_GIFT_SUFFIX..g_userID
			web_address = web_address..SUBJECT_SUFFIX..subject..MESSAGE_SUFFIX..g_message..GIFT_ID_SUFFIX..giftID

			g_parseResults = true
			makeWebCall( web_address, WF.GIFT_CATALOG)
		end
	else
		KEP_MessageBox("Please select a gift to send.")
	end
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
