--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("MenuTemplateHelper.lua")

QUEST_OPTION_SELECTED_EVENT = "QuestOptionSelectedEvent"

-- IDs specific to this quest (but same for each item)
g_itemAssignedID = nil
g_ID		 = nil

QuestID		= 33

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "attribEventHandler", "AttribEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( QUEST_OPTION_SELECTED_EVENT, KEP.MED_PRIO )
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	if filter == QuestID then
		local lbh = Dialog_GetListBox(gDialogHandle, "lstQuest")
		---KEP_Log( "attribEventHandler - listbox: " .. tostring(lbh) )
		if lbh ~= 0 then
			-- always remove all items
			ListBox_RemoveAllItems(lbh)
			g_itemAssignedID = nil
			g_ID		 = nil

			local num_items 	= KEP_EventDecodeNumber( event )
			g_itemAssignedID 	= KEP_EventDecodeNumber( event )
			g_ID 			= KEP_EventDecodeNumber( event )

			for i = 0, num_items-1 do
				local text = KEP_EventDecodeString( event )
				ListBox_AddItem(lbh, text, 0)
			end
		end
	end
end

function SetupListBoxScrollBars(listbox_name)
	local lbh = Dialog_GetListBox(gDialogHandle, listbox_name)
	if lbh ~= 0 then
		local sbh = ListBox_GetScrollBar(lbh)
		if sbh ~= 0 then
			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, gDialogHandle, "dxutcontrols.dds")
				Element_SetCoords(eh, 244, 142, 262, 155)
			end
			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, gDialogHandle, "dxutcontrols.dds")
				Element_SetCoords(eh, 269, 125, 282, 163)
			end
			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, gDialogHandle, "dxutcontrols.dds")
				Element_SetCoords(eh, 244, 125, 262, 142)
			end
			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				Element_AddTexture(eh, gDialogHandle, "dxutcontrols.dds")
				Element_SetCoords(eh, 244, 155, 262, 174)
			end
		end
	end
end

function questOptionSelection( questID, index )
	local questOptionEvent = KEP_EventCreate( QUEST_OPTION_SELECTED_EVENT )
	KEP_EventEncodeNumber( questOptionEvent, questID )
	KEP_EventEncodeNumber( questOptionEvent, index )
	KEP_EventAddToServer( questOptionEvent )
	KEP_EventQueue( questOptionEvent )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	setupDialogBase( gDialogHandle, 474, 353, true, true, false )

	-- setup look of listbox scrollbars
	SetupListBoxScrollBars("lstQuest")
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function lstQuest_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown)
	-- enable the select button
	local bh = Dialog_GetButton(gDialogHandle, "btnSelect")
	if bh ~= 0 then
		Control_SetEnabled(bh, true)
	end
end

function btnSelect_OnButtonClicked(buttonHandle)
	local lbh = Dialog_GetListBox(gDialogHandle, "lstQuest")
	local index = ListBox_GetSelectedItemIndex(lbh)
	if index ~= -1 then
		questOptionSelection( g_itemAssignedID, index )
		KEP_SelectQuest(index, g_itemAssignedID, g_ID)
	end
	MenuCloseThis()
end
