--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

-- dialog variables
g_dialogSec = 0
g_dialogDisplayDuration = 0

-- info message variables
g_infoMessageText = ""
g_ellipsesFrames = {"", ".", "..", "..."}
g_ellipsesFramesPerSec = 5.0

-- event filters
INFO_BOX_INIT_EVENT = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "TimedInfoBoxEventHandler", "TimedInfoBoxEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "TimedInfoBoxEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	
	hideControl("imgIcon")
	hideControl("txtMsg")

	MenuAlwaysBringToFrontThis()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnRender(dialogHandle, elapsedSec)
	
	-- Update Dialog Time
	g_dialogSec = g_dialogSec + elapsedSec

	-- Check to see if it's time to close
	if(g_dialogSec >= g_dialogDisplayDuration and g_dialogDisplayDuration > 0) then
		MenuCloseThis()
	else
		-- Update ellipses frame
		local div, frame = MathDivRem(math.floor(g_dialogSec * g_ellipsesFramesPerSec), #g_ellipsesFrames)

		-- Update message
		SetMessage(g_infoMessageText .. g_ellipsesFrames[frame + 1])
	end
end

--[[ Handle all TimedInfoBox events. ]]
function TimedInfoBoxEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter == INFO_BOX_INIT_EVENT) then
		g_dialogDisplayDuration = KEP_EventDecodeNumber( event )
		g_infoMessageText = KEP_EventDecodeString( event )
		local texture = KEP_EventDecodeString( event )
		local coordinates = {}
		coordinates.left = KEP_EventDecodeNumber( event )
		coordinates.right = KEP_EventDecodeNumber( event )
		coordinates.top = KEP_EventDecodeNumber( event )
		coordinates.bottom = KEP_EventDecodeNumber( event )
		SetMessage(g_infoMessageText)
		SetIcon(texture, coordinates)
	end
end

--[[ Set informational message. ]]
function SetMessage(msg)
	SetControlText("txtMsg", msg)
	showControl("txtMsg")
end

--[[ Set icon image. ]]
function SetIcon(texture, coordinates)
	local elem = Image_GetDisplayElement( gHandles["imgIcon"] )
	Element_AddTexture(elem, gDialogHandle, texture)
	Element_SetCoords(elem, coordinates.left, coordinates.top, coordinates.right, coordinates.bottom)
	showControl("imgIcon")
end
