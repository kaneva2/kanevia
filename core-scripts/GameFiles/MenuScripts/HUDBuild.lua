--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
--
-- HUDBuild
--
-- If you want to add more controls/elements, use the XML comments
-- Format the comment of the control as such: 
-- [element name]|[optional parent control]
-- There must be one parent control per element
-- Make parent control the button you want to be hotkeyed
--
-- Note: This menu is set to minimized until we receive notice that 
-- it has been updated via the UILayoutUpdateEvent after setting the icons.
-- This has been done to avoid flashing the menu while it is still setting up.
--
-------------------------------------------------------------------------------------------
dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\PopupMenuHelper.lua")
dofile("Framework_EventHelper.lua")
dofile("UGCFunctions.lua")
dofile("..\\MenuScripts\\UGCZoneLiteProxy.lua")

ELEMENT_PADDING = 7

COPY_OBJECTS_FILTER = 1
PASTE_OBJECTS_FILTER = 2
UNDO_ONCE	= 1
UNDO_ALL	= 2
REDO_ONCE	= 3

MENU_OBJECTLIST		= 10686
MENU_GROUPS			= 10699
MENU_BUILDSETTINGS	= 10685

BUILD_MODE_MAX_ORBIT_DISTANCE	= 100
g_playersCamera					= nil
g_origMaxDistance				= 30
g_origDistance					= 30

g_currentMode = MODE_MOVE

local m_playerID = 0

local m_showSounds	= 0
local m_showMediaTriggers = 0

local m_settingsMenu		= nil
local m_settingsControls	= {}

local m_actionsMenu		= nil
local m_actionsControls	= {}

local m_frameworkEnabled	= false
local m_playerMode			= false
local m_showXYZCoordinates	= false
local m_showActions			= false -- Undo/Redo/Copy/Paste
local m_advBuildSet			= false

local m_numberStartIndex	= 0
local m_numberEndIndex		= 0
local m_showingNames		= false
local m_hideNumeric 		= false

local m_worldGaming 		= false --used to say if gaming button should open to WOK or World game items

local m_activeElements		= {}
local m_elementControls		= {}

-- If it's a table, the first one is used for the label
ELEMENT_LABELS = {	playerMode	= "Player Mode",
					xyzCoords	= "coords",
					move		= "Move",
					rotate		= "Rotate",
					pickup		= "Pick Up",
					inspect		= "Inspect",
					inventory 	= "Inventory",
					gameSystem	= "Gaming",
					patterns	= "Patterns",
					settings	= {"Helpers", "settingsPopup"},
					jetPack		= "Fly",
					actions		= {"Actions", "actionsPopup"},
					buildExit = "Exit"
				}

-- Label replacements that vary depending on selection.
PICKUP_LABEL = ELEMENT_LABELS.pickup
PICKUP_TEXTURE = "CreatorToolbeltIcons.tga"
PICKUP_COORDINATES = {	left	= 153,
						right	= 203,
						top		= 0,
						bottom	= 50
					 }
REMOVE_LABEL = "Delete"
-- Awaiting actual icon from Dixon
REMOVE_TEXTURE = "CreatorToolbeltIcons.tga"
REMOVE_COORDINATES = {	left	= 459,
						right	= 509,
						top		= 0,
						bottom	= 50
					 }

local m_xyzBoxChanged = false

-- InitializeKEPEvents - Register all script events here. 
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "CloseBuildHudEvent",		KEP.HIGH_PRIO )
	KEP_EventRegister( "DynamicObjectChangeEvent",	KEP.MED_PRIO )
	KEP_EventRegister( "BuildModeOpenEvent",		KEP.MED_PRIO )
	KEP_EventRegister( "UpdateBuildToolbarEvent",		KEP.MED_PRIO )
	KEP_EventRegister( "BuildExitEvent",		KEP.MED_PRIO )
	KEP_EventRegister( "UNIFIED_GOTO_SYSTEM",		KEP.MED_PRIO )
	KEP_EventRegister( "CreateZoneMenuSetupEvent", KEP.MED_PRIO )

	-- Ankit ----- events to support SATFramework
	KEP_EventRegister( "RequestForSettingsMenuPoppedUpValueEvent", KEP.MED_PRIO )
	KEP_EventRegister( "RcvdMenuSettingsPoppedUpValueEvent", KEP.MED_PRIO )
end

-- InitializeKEPEventHandlers - Register all script event handlers here. 
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "closeBuildHudHandler",				"CloseBuildHudEvent",		KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "changeBuildModeEventHandler",		CHANGE_BUILD_MODE_EVENT,	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "UILayoutUpdatedEventHandler",		"UILayoutUpdatedEvent",		KEP.MED_PRIO )
	KEP_EventRegisterHandler( "buildSettingsEventHandler",			BUILD_SETTINGS_EVENT,		KEP.MED_PRIO )
	KEP_EventRegisterHandler( "updateBuildToolbarEventHandler",		"UpdateBuildToolbarEvent",	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "returnFrameworkStateHandler",		"FrameworkState",			KEP.MED_PRIO )
	KEP_EventRegisterHandler( "selectEventHandler",					"SelectEvent",				KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler",			"BrowserPageReadyEvent",	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "worldGameModeHandler", 				"FRAMEWORK_WORLD_GAME_MODE",KEP.HIGH_PRIO)

	-- Ankit ----- eventHandlers to support SATFramework
	KEP_EventRegisterHandler( "RequestForSettingsMenuPoppedUpValueEventHandler", "RequestForSettingsMenuPoppedUpValueEvent", KEP.MED_PRIO )
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) -- REQUIRED - DO NOT REMOVE

	m_settingsControls[1] = "imgPopupBG" -- Have to hand insert at 1 because helper treats 1 as BG element
	-- For rest, add everything with the comment settingsPopup
	for name, handle in pairs(gHandles) do
		local element = getControlElement(name)
		if element == "settingsPopup" then
			table.insert(m_settingsControls, name)
		end
	end
	m_settingsMenu = PopupMenuHelper_InitPopupMenu(gHandles["btnSettings"], m_settingsControls, nil, nil, nil, true, .15, .05)

	m_actionsControls[1] = "imgActionsBG" -- Have to hand insert at 1 because helper treats 1 as BG element
	-- For rest, add everything with the comment settingsPopup
	for name, handle in pairs(gHandles) do
		local element = getControlElement(name)
		if element == "actionsPopup" then
			table.insert(m_actionsControls, name)
		end

	end
	m_actionsMenu = PopupMenuHelper_InitPopupMenu(gHandles["btnActions"], m_actionsControls, nil, nil, nil, true, .15, .05)

	populateElementControlTable()
	-- Is the Framework active in this world? Let's find out
	local event = KEP_EventCreate("GetFrameworkState")
	KEP_EventQueue(event)
	updateIcons()

	KEP_EventCreateAndQueue("FRAMEWORK_REQUEST_WORLD_GAME_MODE")

	local editBoxes = {"edCoordsX", "edCoordsY", "edCoordsZ"}
	for i, name in pairs(editBoxes) do
		-- On enter/tab clear focus which also triggers OnFocusOut and handles updating
		_G[name .. "_OnEditBoxString"] = function(editBoxHandle)
			local val = tonumber(EditBox_GetText(editBoxHandle)) or 0
			EditBox_SetText(editBoxHandle, tostring(val), 0)
			MenuClearFocusThis()
		end

		_G[name .. "_OnEditBoxChange"] = function(editBoxHandle)
			m_xyzBoxChanged = true
		end

		_G[name .. "_OnFocusIn"] = function(editBoxHandle)
		end

		_G[name .. "_OnFocusOut"] = function(editBoxHandle)
			updatePositionRotation()
		end
	end

	local config = KEP_ConfigOpenWOK()
	if (config ~= nil) then
		local CreatorToolBoxOK, CreatorToolOPEN =  KEP_ConfigGetNumber( config, "CreatorToolbox", 0)
		if MenuIsClosed("CreatorToolbox.xml") and CreatorToolOPEN == 1  then
			MenuOpen("CreatorToolbox.xml") 
		end

		local okfilter, buildHelpOpen = KEP_ConfigGetString( config, "BuildHelpOpen", "false" )
		if buildHelpOpen == "true" then
			MenuOpen("BuildHelp.xml")
		end

		local _, showSounds = KEP_ConfigGetString( config, "BuildModeShowSoundTriggers", "false")
		m_showSounds = (showSounds == "true") and 1 or 0

		local _, showMediaTriggers = KEP_ConfigGetString( config, "BuildModeShowMediaTriggers", "false")
		m_showMediaTriggers = (showMediaTriggers == "true") and 1 or 0
	end

	showSounds()
	showMediaTriggers()

	-- Modify whichever camera they are currently using?
	g_playersCamera = KEP_GetPlayersActiveCamera()
	g_origMaxDistance = GetSet_GetNumber(g_playersCamera, CameraIds.ORBITMAXDIST)
	g_origDistance = GetSet_GetNumber(g_playersCamera, CameraIds.ORBITDIST)
	GetSet_SetNumber(g_playersCamera, CameraIds.ORBITMAXDIST, BUILD_MODE_MAX_ORBIT_DISTANCE)

	-- Get the user's IDs
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..KEP_GetLoginName(), WF.HUD_USER_ID)

	KEP_EventCreateAndQueue("BuildModeOpenEvent")
end


function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)	
	if filter == WF.HUD_USER_ID then
		local xmlData = KEP_EventDecodeString( event )
		m_playerID = tonumber(string.match(xmlData, "<player_id>(%d+)</player_id>") or "0")
	end
end

local NUM_KEY_BGS = 4
function setNumericHandler(event)
	if event.hideNumeric == nil then return end

	m_hideNumeric = event.hideNumeric
	-- ED-2630 - A/B test for hiding toolbelt numbers when the emote menu is enabled
	-- Take Unfied Build test into account, as the "B" group does not display numbers
	for i = 1, (m_numberEndIndex - m_numberStartIndex + 1) do
		if gHandles["stcKey"..tostring(i)] then
			Control_SetVisible(gHandles["stcKey"..tostring(i)], not m_hideNumeric)
			for j = 1, NUM_KEY_BGS do
				if gHandles["stcKey"..tostring(i).."BG"..tostring(j)] then
					Control_SetVisible(gHandles["stcKey"..tostring(i).."BG"..tostring(j)], not m_hideNumeric)
				end
			end
		end
	end
end

-- Event sent to refresh the build hud
function updateBuildToolbarEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	updateIcons()
end

-- Called when a player changes Framework mode or when the Framework changes states (enabled/disabled)
function returnFrameworkStateHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local frameworkTable = cjson.decode(Event_DecodeString(event))
	local frameworkEnabled =  frameworkTable.enabled == true
	local playerMode = frameworkTable.playerMode == "Player"
	
	if frameworkTable.enabled ~= nil then
		-- Only want to update if framework has changed state
		local frameworkChange = (frameworkEnabled ~= m_frameworkEnabled) or (playerMode ~= m_playerMode)
		m_frameworkEnabled = frameworkEnabled
		m_playerMode = playerMode

		if frameworkChange then
			updateIcons()
		end
	end
end

-- Event sent when various build settings have been updated
function buildSettingsEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if	filter == BUILD_SETTINGS_ADVANCED_BUILD
		or filter == BUILD_SETTINGS_SELECTION_SHOW_XYZ
		or filter == BUILD_SETTINGS_SELECTION_SHOW_UNDO_REDO then
		updateIcons()
	
	elseif filter == BUILD_SETTINGS_SELECTION_SHOW_SOUNDS then
		m_showSounds = KEP_EventDecodeNumber(event)
		showSounds()
	
	elseif filter == BUILD_SETTINGS_SELECTION_SHOW_MEDIA then
		m_showMediaTriggers = KEP_EventDecodeNumber(event)
		showMediaTriggers()
	end
end

-- Event sent when the UI layout has been updated, once it has make sure it's no longer minimized
function UILayoutUpdatedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if not MenuIsOpen("CharacterCreation3.xml") then
		MenuSetMinimizedThis(false)
	end
end

-- Look through the XML and add controls to the element (set of controls that stay together) they belong to
function populateElementControlTable()
	-- Populate the elements menu based on comment
	for element, comment in pairs(ELEMENT_LABELS) do
		m_elementControls[element] = {}

		-- If not a table, make it one for checking standard below
		local commentList = comment
		if type(comment) ~= "table" then
			commentList = {comment}
		end

		for i, singleComment in pairs(commentList) do
			for name, handle in pairs(gHandles) do
				local formattedComment = getControlElement(name)
				if formattedComment == singleComment then
					table.insert(m_elementControls[element], name)
				end
			end
		end
	end
end

-- Add elements that should be active to the active list
-- If adding/changing any do it in the order you want it to show
-- Also pay attention to m_numberStartIndex and m_numberEndIndex to show where starting/ending numbers at bottom
function updateIcons()
	m_activeElements = {}

	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		local xyzOk, showXYZ = KEP_ConfigGetString( config, "BuildModeShowXYZCoordinates", "false")
		m_showXYZCoordinates = (showXYZ == "true")
		local undoBool = "true"
		local undoRedoOk, showUndoRedo = KEP_ConfigGetString( config, "BuildModeShowUndoRedo", undoBool)
		m_showActions = (showUndoRedo == "true")

		local advBuildOK, advBuildON =  KEP_ConfigGetNumber( config, "AdvBuild", 0)
		m_advBuildSet = advBuildON == 1
	end

	if m_frameworkEnabled then
		
	end

	if m_showXYZCoordinates then
		table.insert(m_activeElements, "xyzCoords")
	end

	table.insert(m_activeElements, "move")
	m_numberStartIndex = #m_activeElements -- Always want to start numbers at the move icon

	table.insert(m_activeElements, "rotate")
	table.insert(m_activeElements, "pickup")
	table.insert(m_activeElements, "inspect")
	table.insert(m_activeElements, "inventory")
	
	table.insert(m_activeElements, "gameSystem")

if m_showActions then
		table.insert(m_activeElements, "actions")
	end

	if m_advBuildSet then
		table.insert(m_activeElements, "settings")
	end

	if m_frameworkEnabled then
		table.insert(m_activeElements, "jetPack")
	end
	
	table.insert(m_activeElements, "buildExit")
	m_numberEndIndex = (#m_activeElements) - 1 --exit is not numbered

	displayMenu()
	
end

-- Display the menu with control visibility/location based on the m_activeElements table
function displayMenu()
	hideAllControls()

	local xLocation = 0

	for i, element in pairs(m_activeElements) do
		local newXLocation = 0
		local distTraveled = 0

		for j, controlName in pairs(m_elementControls[element]) do
			local controlHandle = gHandles[controlName]
			local controlParent = getControlParent(controlName)

			-- Has a parent, move it with that offset considered
			if controlParent then
				local offset = Control_GetLocationX(controlHandle) - Control_GetLocationX(gHandles[controlParent]) - distTraveled
				Control_SetLocationX(gHandles[controlName], xLocation + offset)
			
			-- This is the parent
			else
				distTraveled = Control_GetLocationX(controlHandle) - xLocation
				Control_SetLocationX(controlHandle, xLocation)
				newXLocation = xLocation + Control_GetWidth(controlHandle) + ELEMENT_PADDING
			end

			-- Only show control if it isn't in the popup helper
			if not PopupMenuHelper_IsHandleInPopup(m_settingsMenu, controlHandle) and not PopupMenuHelper_IsHandleInPopup(m_actionsMenu, controlHandle) then
				Control_SetVisible(controlHandle, true)
			end
		end

		xLocation = newXLocation
	end

	updateNumberLocations()
	for i = 1, (m_numberEndIndex - m_numberStartIndex +1) do
		local text = ""

		text = ELEMENT_LABELS[m_activeElements[ (i + m_numberStartIndex -1) ]] -- Get the text of the current element
		if type(text) == "table" then
			text = text[1]
		end
		Log("TOOLBAR TEXT: "..tostring(text))
	
		OutlinedString_SetText("Key" .. i, text)
	end
	
	updateActiveButtons()

	MenuSetSizeThis(xLocation - ELEMENT_PADDING)

	KEP_EventCreateAndQueue("UpdateUILayoutEvent")
	
	if MenuIsOpen("Emotes") then
		setNumericHandler({hideNumeric = true})
	end
end

-- Update which buttons are active (currently - green square around the outside)
function updateActiveButtons()
	Control_SetVisible(gHandles["imgMoveActive"], g_currentMode == MODE_MOVE)
	Control_SetVisible(gHandles["imgRotateActive"], g_currentMode == MODE_ROTATE)
	Control_SetVisible(gHandles["imgInvActive"], MenuIsOpen("UnifiedInventory.xml") or MenuIsOpen("Inventory.xml") )
	Control_SetVisible(gHandles["imgJetPackActive"], MenuIsOpen("Framework_JetPack.xml"))
	Control_SetVisible(gHandles["imgInspectActive"], MenuIsOpen("DynamicObject.xml") or MenuIsOpen("PlaceableObj.xml"))
end

-- Update the locations of the numbers along the bottom based on m_numberStartIndex and m_numberEndIndex as indecies of the m_activeElements table
function updateNumberLocations()

	for i = 0, (m_numberEndIndex - m_numberStartIndex) do
		local numberBaseName = "Key" .. (i+1)
		local numberHandle = gHandles["stc" .. numberBaseName]
		local firstControl = m_elementControls[ m_activeElements[ (i + m_numberStartIndex) ] ][1] -- Get the first control of the current element
		local controlHandle = gHandles[ getControlParent(firstControl) or firstControl ] -- Use the parent if it has one

		local newLocation = Control_GetLocationX(controlHandle) + (Control_GetWidth(controlHandle) - Control_GetWidth(numberHandle))/2
		OutlinedString_SetLocationX(numberBaseName, newLocation)
		OutlinedString_SetVisible(numberBaseName, true)

	end
	
end

-- Get a control's parent control (for controls within an element set) if it has one, otherwise return nil
function getControlParent(controlName)
	controlHandle = gHandles[controlName]
	-- Get any text after a vertical bar to the end of the file
	return string.match(Control_GetComment(controlHandle), "^[^|]+|(.+)$")
end

-- Get the element (set) a control belongs to
function getControlElement(controlName)
	controlHandle = gHandles[controlName]
	-- Get any text from the beginning of a string before a vertical bar
	return string.match(Control_GetComment(controlHandle), "^[^|]+")
end

-- Hide every single control in the menu
function hideAllControls()
	for name, handle in pairs(gHandles) do
		Control_SetVisible(handle, false)
	end
end

-- Update the XYZ coords element
function updatePositionRotation()
	
	if m_xyzBoxChanged then
		m_xyzBoxChanged = false

		-- DRF - Crash Fix
		-- Entering text into coordinate edit boxes causes crash!
		-- KEP_EventEncode/DecodeNumber() really should stop this!
		local vec = {}
		vec.x = tonumber(EditBox_GetText(gHandles["edCoordsX"])) or 0
		vec.y = tonumber(EditBox_GetText(gHandles["edCoordsY"])) or 0
		vec.z = tonumber(EditBox_GetText(gHandles["edCoordsZ"])) or 0

		-- Send Event To Selection.lua To Position/Rotate Selected Objects
		ev = KEP_EventCreate("SetObjectsSelectedPosRotEvent")
		KEP_EventEncodeNumber(ev, vec.x)
		KEP_EventEncodeNumber(ev, vec.y)
		KEP_EventEncodeNumber(ev, vec.z)
		KEP_EventQueue(ev)
	end
end

-- Enter Framework Player Mode
function btnPlayerMode_OnLButtonDown(buttonHandle)
	local ev = KEP_EventCreate("FRAMEWORK_SET_PLAYER_MODE")
	KEP_EventEncodeString(ev, "Player")
	KEP_EventQueue(ev) -- NOTE: Handled in Framework_PlayerHandler

	clearFocusEditBoxes()
end

function btnMove_OnLButtonDown(buttonHandle)	
	changeMode(MODE_MOVE)
end
function imgMoveActive_OnLButtonDown(buttonHandle)
	btnMove_OnLButtonDown(buttonHandle)
end

function btnRotate_OnLButtonDown(buttonHandle)
	changeMode(MODE_ROTATE)
end

function imgInvActive_OnLButtonDown(buttonHandle)
	btnInventory_OnLButtonDown(buttonHandle)
end

function btnInventory_OnLButtonDown(buttonHandle)
	if not MenuIsOpen("UnifiedNavigation.xml") then
			MenuOpen("UnifiedNavigation.xml")
	end
		--MenuOpen("UnifiedNavigation.xml")

	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	KEP_EventEncodeString(ev, "building")
	KEP_EventQueue(ev)

	updateActiveButtons()
	clearFocusEditBoxes()
end

function imgRotateActive_OnLButtonDown(buttonHandle)
	btnRotate_OnLButtonDown(buttonHandle)
end

function btnPickUp_OnLButtonDown(buttonHandle)

	local objType, id = KEP_GetSelection(0)

	
	KEP_EventCreateAndQueue( "PickUpSelectedObjectsEvent" )

	-- Tell DynamicObjectList Menu To Refresh
	KEP_EventCreateAndQueue("DynamicObjectChangeEvent")

	clearFocusEditBoxes()
end

function btnTrash_OnLButtonDown(buttonHandle)
	btnPickUp_OnLButtonDown(buttonHandle)
end 

function btnInspect_OnLButtonDown(buttonHandle)
	if MenuIsOpen("DynamicObject.xml") or MenuIsOpen("PlaceableObj.xml") then
		MenuClose("DynamicObject.xml")
		MenuClose("PlaceableObj.xml")
	elseif KEP_GetNumSelected() == 1 then
		local objType, id = KEP_GetSelection(0)
		openDynamicObjectMenu(id)
	end

	updateActiveButtons()

	clearFocusEditBoxes()
end
function imgInspectActive_OnLButtonDown(buttonHandle)
	btnInspect_OnLButtonDown(buttonHandle)
end

function btnActions_OnLButtonDown(buttonHandle)
	PopupMenuHelper_ShowPopupMenu(m_actionsMenu, not m_actionsMenu.PoppedUp)

	clearFocusEditBoxes()
end

function btnGameSystems_OnLButtonDown(buttonHandle)
	if not MenuIsOpen("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")
	end
	
	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")

	if  m_worldGaming then
		KEP_EventEncodeString(ev, "gaming")
	else
		KEP_EventEncodeString(ev, "wokGaming")
	end
	KEP_EventQueue(ev)
	
	clearFocusEditBoxes()
end 

function btnPatterns_OnLButtonDown(buttonHandle)
	if g_currentMode == MODE_TEXTURES then
		changeMode(MODE_MOVE)
		MenuClose("TextureBrowser2")
	else
		changeMode(MODE_TEXTURES)
	end

	clearFocusEditBoxes()
end

function btnPatternsOrig_OnLButtonDown(buttonHandle)
	if g_currentMode == MODE_TEXTURES then
		changeMode(MODE_MOVE)
		MenuClose("TextureBrowser2")
	else
		changeMode(MODE_TEXTURES)
	end

	clearFocusEditBoxes()
end

function imgPatternsActive_OnLButtonDown(buttonHandle)
	btnPatternsOrig_OnLButtonDown(buttonHandle)
end

function btnSettings_OnLButtonDown(buttonHandle)
	PopupMenuHelper_ShowPopupMenu(m_settingsMenu, not m_settingsMenu.PoppedUp)

	clearFocusEditBoxes()
end

function btnJetPack_OnLButtonDown(buttonHandle)
	MenuToggleOpenClose("Framework_JetPack.xml")
	updateActiveButtons()

	clearFocusEditBoxes()
end 
function imgJetPackActive_OnLButtonDown(buttonHandle)
	btnJetPack_OnLButtonDown(buttonHandle)
end

function btnExit_OnLButtonDown(buttonHandle)
--Tell Selection.lua you are no longer in build mode, only exit build by this button 
	local ev = KEP_EventCreate("BuildExitEvent")
	KEP_EventQueue(ev)
	closeMenu(true)
	
	if m_frameworkEnabled then
		
		local ev = KEP_EventCreate("FRAMEWORK_SET_PLAYER_MODE")
		KEP_EventEncodeString(ev, "Player")
		KEP_EventQueue(ev) -- NOTE: Handled in Framework_PlayerHandler

		clearFocusEditBoxes()
	end 
end 
--------------------------------------------------------------------------------
-- Popup Menu
--------------------------------------------------------------------------------
function btnImportWorld_OnButtonClicked(buttonHandle)
	if (KEP_bulkZoneOpsEnabled()) then
		if MenuIsClosed("CreateZone.xml") then
			MenuOpenModal("CreateZone.xml")
			setupCreateZoneMenu()
		end 
	end
 
	PopupMenuHelper_ShowPopupMenu(m_settingsMenu, false)
end

function setupCreateZoneMenu()

	local currentZoneIndex = KEP_GetCurrentZoneIndex()
	local currentZoneName  = KEP_GetCurrentZoneName()
	 
	Log("setupCreateZoneMenu: currentZoneIndex="..currentZoneIndex) 

	if currentZoneIndex == nil then
		LogError("setupCreateZoneMenu: ERROR(unknown zoneIndex["..tostring(currentZoneIndex).."])")
		return 
	end

	local event = KEP_EventCreate("CreateZoneMenuSetupEvent")
	KEP_EventSetFilter(event, 2)                               -- (2 import)
	KEP_EventEncodeNumber(event, 1)                            -- showNewSection       -- (1 false -- 0 true)
	KEP_EventEncodeNumber(event, 0)                            -- trackStatus          -- (0 false -- 1 true)
	KEP_EventEncodeNumber(event, 1)                            -- (1 All -- 2 ScriptedOnly --3 NonScriptedOnly)
	KEP_EventEncodeNumber(event, UGCZLPZoneTypes.CURRENT_TYPE) -- importzonetype       - (3 home / 4 zone / 5 arena / 6 channel)
	KEP_EventEncodeNumber(event, currentZoneIndex)             -- zoneIndex            -- (imports will apply to this zone)
	KEP_EventEncodeNumber(event, 1)                            -- deleteDynamicObjects -- (0 delete all dynamic objects) (1 delete only dynamic objects with NO scripts)
	KEP_EventEncodeString(event, currentZoneName)              -- zoneName             -- (for display)
	KEP_EventQueue(event)

	Log("setupCreateZoneMenu: Dispatched Request[CreateZoneMenuSetupEvent] for zoneIndex["..tostring(currentZoneIndex).."] currentZoneName["..tostring(currentZoneName).."]")
end

function btnCreatorToolbox_OnButtonClicked(buttonHandle)
	MenuOpen("CreatorToolbox.xml")
	PopupMenuHelper_ShowPopupMenu(m_settingsMenu, false)
end

function btnObjectList_OnButtonClicked(buttonHandle)
	local ev = KEP_EventCreate("BuildOpenMenuEvent")
	KEP_EventSetFilter(ev, MENU_OBJECTLIST)
	KEP_EventQueue(ev)

	PopupMenuHelper_ShowPopupMenu(m_settingsMenu, false)
end

function btnGroups_OnButtonClicked(buttonHandle)
	local ev = KEP_EventCreate("BuildOpenMenuEvent")
	KEP_EventSetFilter(ev, MENU_GROUPS)
	KEP_EventQueue(ev)

	PopupMenuHelper_ShowPopupMenu(m_settingsMenu, false)
end

function chkSnapGrid_OnCheckBoxChanged(chkHandle)
	updateSnapping()
	PopupMenuHelper_ShowPopupMenu(m_settingsMenu, false)
end

function chkSnapObject_OnCheckBoxChanged(chkHandle)
	updateSnapping()
	PopupMenuHelper_ShowPopupMenu(m_settingsMenu, false)
end

function chkSnapRot_OnCheckBoxChanged(chkHandle)
	updateSnapping()
	PopupMenuHelper_ShowPopupMenu(m_settingsMenu, false)
end

function btnUndo_OnButtonClicked(btnHandle)
	local ev = KEP_EventCreate("UndoBuildEvent")
	KEP_EventSetFilter(ev, UNDO_ONCE)
	KEP_EventQueue( ev)
	
	PopupMenuHelper_ShowPopupMenu(m_actionsMenu, false)
end

function btnRedo_OnButtonClicked(btnHandle)
	local ev = KEP_EventCreate("UndoBuildEvent")
	KEP_EventSetFilter(ev, REDO_ONCE)
	KEP_EventQueue( ev)
	
	PopupMenuHelper_ShowPopupMenu(m_actionsMenu, false)
end

function btnCopy_OnButtonClicked(btnHandle)
	local ev = KEP_EventCreate("CopyPasteObjectsEvent")
	KEP_EventSetFilter(ev, COPY_OBJECTS_FILTER)
	KEP_EventQueue( ev)
	
	PopupMenuHelper_ShowPopupMenu(m_actionsMenu, false)
end

function btnPaste_OnButtonClicked(btnHandle)
	local ev = KEP_EventCreate("CopyPasteObjectsEvent")
	KEP_EventSetFilter(ev, PASTE_OBJECTS_FILTER)
	KEP_EventQueue( ev)
	
	PopupMenuHelper_ShowPopupMenu(m_actionsMenu, false)
end

-- Clear focus on the menu if any edit boxes are in focus (so we're no longer in edit mode for them)
function clearFocusEditBoxes()
	if ( Control_GetFocus(gHandles["edCoordsX"]) == 1 or
		 Control_GetFocus(gHandles["edCoordsY"]) == 1 or
		 Control_GetFocus(gHandles["edCoordsZ"]) == 1 ) then

		MenuClearFocusThis()
	end
end

function updateSnapping()
	local grid = CheckBox_GetChecked(gHandles["chkSnapGrid"])
	local object = CheckBox_GetChecked(gHandles["chkSnapObject"])
	local rotation = CheckBox_GetChecked(gHandles["chkSnapRot"])

	ev = KEP_EventCreate("EnableGridEvent")
	KEP_EventEncodeNumber(ev, grid)
	KEP_EventEncodeNumber(ev, rotation)
	KEP_EventEncodeNumber(ev, object)
	KEP_EventQueue(ev)
end

function btnBuildSettings_OnButtonClicked(buttonHandle)
	local ev = KEP_EventCreate("BuildOpenMenuEvent")
	KEP_EventSetFilter(ev, MENU_BUILDSETTINGS)
	KEP_EventQueue(ev)

	PopupMenuHelper_ShowPopupMenu(m_settingsMenu, false)
end

function changeMode(mode)
	-- Update the advanced mode boxes before changing modes
	-- May be handled first by the clearing focus, but do here as well just in case of race condition
	if g_currentMode == MODE_MOVE or g_currentMode == MODE_ROTATE then
		updatePositionRotation()
	end

	local ev = KEP_EventCreate( CHANGE_BUILD_MODE_EVENT )
	KEP_EventSetFilter(ev, mode)
	KEP_EventQueue( ev)
end

-- Have to catch as well to store info about what mode we're in, regardless of who changed it
function changeBuildModeEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	updateBuildMode(filter)

	clearFocusEditBoxes()
end

-- Change which mode were in
function updateBuildMode(mode)
	if mode == MODE_INVENTORY then 
		updateActiveButtons()
	else 
		g_currentMode = mode
		updateActiveButtons()

		if mode == MODE_SOUNDS then
			m_showSounds = 1
			showSounds()
			return
		end

		if g_currentMode == MODE_ROTATE then
			Static_SetText(gHandles["stcCoordsPosition"], "Rotation")
		elseif g_currentMode == MODE_MOVE then
			Static_SetText(gHandles["stcCoordsPosition"], "Position")
		end

		Control_SetEnabled(gHandles["stcCoordsPosition"], g_currentMode == MODE_MOVE or g_currentMode == MODE_ROTATE)
		Control_SetEnabled(gHandles["stcCoordsX"], g_currentMode == MODE_MOVE)
		Control_SetEnabled(gHandles["edCoordsX"], g_currentMode == MODE_MOVE)
		Control_SetEnabled(gHandles["stcCoordsY"], g_currentMode == MODE_MOVE or g_currentMode == MODE_ROTATE)
		Control_SetEnabled(gHandles["edCoordsY"], g_currentMode == MODE_MOVE or g_currentMode == MODE_ROTATE)
		Control_SetEnabled(gHandles["stcCoordsZ"], g_currentMode == MODE_MOVE)
		Control_SetEnabled(gHandles["edCoordsZ"], g_currentMode == MODE_MOVE)
	end 
end

function showSounds()
	if m_showSounds == 0 then
		local count = KEP_GetNumSelected()
		for i = 0,count-1 do
			local type, id = KEP_GetSelection(i)
			if type == ObjectType.SOUND then
				KEP_TurnOffWidget()
				KEP_ClearSelection()
			end
		end
	end

	KEP_EnableSoundPlacementRender(m_showSounds)
end

function showMediaTriggers()
	KEP_EnableMediaTriggerRender(m_showMediaTriggers)
end

function Dialog_OnRender(dialogHandle, fTimeElapsed)
	PopupMenuHelper_OnRender(fTimeElapsed)

	-- If the helpers menu is popped up and we click outside of it, close it
	-- NOTE: THIS IS A HACK! Sorry :(
	-- We currently have no event when a mouse is clicked anywhere - Dialog_OnLButtonDown does not fire over other menus
	-- If we gain said event move this to there
	if Dialog_IsLMouseDown(gDialogHandle) ~= 0 then
		-- Convert cursor location to local space
		local cursor = MenuGetLocationThis()
		cursor.x = Cursor_GetLocationX() - cursor.x
		cursor.y = Cursor_GetLocationY() - cursor.y

		if m_settingsMenu.PoppedUp and not PopupMenuHelper_ContainsPoint(m_settingsMenu, cursor.x, cursor.y) then
			PopupMenuHelper_ShowPopupMenu(m_settingsMenu, false)
		end

		if m_actionsMenu.PoppedUp and not PopupMenuHelper_ContainsPoint(m_actionsMenu, cursor.x, cursor.y) then
			PopupMenuHelper_ShowPopupMenu(m_actionsMenu, false)
		end
	end
	
	if MenuIsOpen("Emotes") then
		if m_hideNumeric == false then
			setNumericHandler({hideNumeric = true})
		end
	elseif m_hideNumeric == true then
		setNumericHandler({hideNumeric = false})
	end	
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	PopupMenuHelper_OnMouseMove(dialogHandle, x, y, true)
end

function selectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local showPickup = true

	-- If the user is not the world owner and/or did not place the item within the world
	-- it will not be returned to their inventory upon pickup. Adjust the text and icon to indicate.
	if m_playerID ~= 0 then
		if KEP_IsOwner() then
			local count = ObjectsSelected()

			if count > 0 then
				for i = 0, count-1 do
					local type, id = KEP_GetSelection(i)
					local objName, objDescription, objCanPlayMovie, objIsAttachable, objInteractive, objIsLocal, objDerivable, objAssetId, objFriendId, objCustomTexture, objGlobalId, objInvType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(id)

					if m_playerID ~= playerId then
						showPickup = false
						break
					end

					if MenuIsOpen("DynamicObject.xml") or MenuIsOpen("PlaceableObj.xml") then
						openDynamicObjectMenu(id)
					end
				end
			end
		else		
			showPickup = false
		end
	end

	if showPickup then
		Control_SetVisible(gHandles["btnTrash"], false)
		Control_SetEnabled(gHandles["btnPickUp"], true)
	else
		Control_SetLocation(gHandles["btnTrash"], Control_GetLocationX(gHandles["btnPickUp"]),Control_GetLocationY(gHandles["btnPickUp"]) )
		Control_SetVisible(gHandles["btnTrash"], true)
		Control_SetEnabled(gHandles["btnPickUp"], false)
	end

end

-- Replace the numbers along the bottoms with the names of the element
-- Uses the element name (comment before the vertical bar) to display
function showNames(show)
	if m_showingNames == show then
		return
	end
	m_showingNames = show

	for i = 1, (m_numberEndIndex - m_numberStartIndex + 1) do
		local text = ""

		if show then
			text = ELEMENT_LABELS[m_activeElements[ (i + m_numberStartIndex - 1) ]] -- Get the text of the current element
			if type(text) == "table" then
				text = text[1]
			end
		else
			text = i
		end


		OutlinedString_SetText("Key" .. i, text)
 
	end

		local playerModeText = show and getControlElement("stcPlayerMode") or ""
		OutlinedString_SetText("PlayerMode", playerModeText)

end

-- Set the location of an outlined string on the x with 2 strings left and 2 strings to the right by 1
function OutlinedString_SetLocationX(baseName, location)
	Control_SetLocationX(gHandles["stc" .. baseName], location)

	for i = 1, 4 do
		local offset = (i > 2) and 1 or -1
		Control_SetLocationX(gHandles["stc" .. baseName .. "BG" .. i], location + offset)
	end
end

function OutlinedString_SetVisible(baseName, visible)
	Control_SetVisible(gHandles["stc" .. baseName], visible)

	for i = 1, 4 do
		Control_SetVisible(gHandles["stc" .. baseName .. "BG" .. i], visible)
	end
end

function OutlinedString_SetText(baseName, text)
		SetControlText("stc" .. baseName, text)

		for i = 1, 4 do
			SetControlText("stc" .. baseName .. "BG" .. i, text)
		end
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	local keyNum = key - Keyboard.NUM0
	
	-- If the shift isn't down, and a (visible) number is being pressed, get the parent control and call the button
	if keyNum > 0 and keyNum <= (m_numberEndIndex - m_numberStartIndex + 1) then
		-- ED-2630 - A/B test for hiding toolbelt numbers when the emote menu is enabled
		if (MenuIsOpen("Emotes") and not bShiftDown) or
			(MenuIsClosed("Emotes") and bShiftDown) or
			(bShiftDown) then
			return
		end
		local firstControl = m_elementControls[ m_activeElements[ (keyNum + m_numberStartIndex - 1) ] ][1] -- Get the first control of the current element
		local parentControl = getControlParent(firstControl) or firstControl -- Use the parent if it has one
		_G[parentControl .. "_OnLButtonDown"](gHandles[parentControl])
	end
end

function closeBuildHudHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	closeMenu(true) -- Trust that the only ones sending this are truly wanting to close it
end

function worldGameModeHandler(dispatcher, fromNetId, event, filter, objectId)
	m_worldGaming = KEP_EventDecodeNumber(event) == 1
end

-- Use this closeMenu before actually closing to see if we're realy intending to
function closeMenu(override)

	-- In game worlds we only want to close the build hud in very specific instances
	-- In a game world, unless it has been called with a override, just clear the selection rather than actually closing
	if m_frameworkEnabled and not override then
		KEP_ClearSelection()
		return
	end

	MenuCloseThis()
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)

	KEP_EventCreateAndQueue("SaveBuildEvent")
	
	KEP_EnableSoundPlacementRender(0)
	KEP_EnableMediaTriggerRender(0)

	KEP_ClearSelection()

	MenuClose("BuildHelp.xml")

	-- re-set the camera's max distance	
	--GetSet_SetNumber(g_playersCamera, CameraIds.ORBITMAXDIST, g_origMaxDistance)
	--GetSet_SetNumber(g_playersCamera, CameraIds.ORBITDIST, g_origDistance)

	local ev = KEP_EventCreate("EnterBuildModeEvent")
	KEP_EventSetFilter(ev, 0)
	KEP_EventQueue(ev)

	Helper_Dialog_OnDestroy( dialogHandle ) -- REQUIRED - DO NOT REMOVE
end

-- Ankit ----- event handler to return the value of m_settingsMenu.PoppedUp back to SATFramework
function RequestForSettingsMenuPoppedUpValueEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local ev = KEP_EventCreate( "RcvdSettingsMenuPoppedUpValueEvent" )
	KEP_EventEncodeNumber( ev, m_settingsMenu.PoppedUp and 1 or 0 )
	KEP_EventQueue( ev )
end