--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Indicators.lua - Temporary location for framework indicators
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

g_objectIndicators = {}
g_healthIndicators = {}
local m_players = {}
g_statusInfo = {}

-- InitializeKEPEvents - Register all script events here. 
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "PlayerShowStatusInfoEvent", KEP.HIGH_PRIO )
end

-- InitializeKEPEventHandlers - Register all script event handlers here. 
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	--KEP_EventRegisterHandler( "textHandler", "RenderTextEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "addIndicatorHandler", "AddIndicatorEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "clearObjectIndicatorsHandler", "ClearObjectIndicatorsEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "addHealthIndicatorHandler", "AddHealthIndicatorEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "removeHealthIndicatorHandler", "RemoveHealthIndicatorEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "playerShowStatusInfoEventHandler", "PlayerShowStatusInfoEvent", KEP.HIGH_PRIO )

	Events.registerHandler("FRAMEWORK_ADD_INDICATOR", addIndicator)
	Events.registerHandler("FRAMEWORK_REMOVE_INDICATOR", clearIndicator)
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	MenuSetPassthroughThis(true)

	Control_SetVisible(gHandles["stcStatus1"], false)
	Control_SetVisible(gHandles["stcStatus2"], false)
	Control_SetVisible(gHandles["stcStatus3"], false)
	Control_SetEnabled(gHandles["stcStatus1"], false)
	Control_SetEnabled(gHandles["stcStatus2"], false)
	Control_SetEnabled(gHandles["stcStatus3"], false)
	local i
	for i = 1, 3 do
		local staticName = "stcStatus" .. tostring(i)
		Control_SetLocation( gHandles[staticName], 1000, 1000)
	end
end

function Dialog_OnMoved(dialogHandle)	
	local screenWidth = Dialog_GetScreenWidth(dialogHandle)
	local screenHeight = Dialog_GetScreenHeight(dialogHandle)
	local menuLoc = MenuGetLocationThis()
	local menuXPos = menuLoc.x
	local menuYPos = menuLoc.y
	
	-- Set status message positions
	local i
	for i = 1, 3 do
		local staticName = "stcStatus" .. tostring(i)
		if Control_GetVisible(gHandles[staticName]) == true then
			Control_SetLocation( gHandles[staticName], -menuXPos + (screenWidth/2) - (Control_GetWidth(gHandles[staticName])/2), -menuYPos + 100 + 30*(i-1))
		else
			Control_SetLocation( gHandles[staticName], 1000, 1000)
		end
	end
end

function Dialog_OnRender(dialogHandle, timeElapsed)
	for id, indicators in pairs(g_objectIndicators) do
		if indicators ~= nil then
			for gemPID, gem in pairs(indicators) do
				--Check to see if the object or player is still in the world, if not, remove all indicators
				if (gem.type == ObjectType.DYNAMIC and KEP_DynamicObjectGetGLID(id) ~= 0) or
				   (gem.type == ObjectType.PLAYER and m_players[id] and m_players[id].NetId and KEP_GetPlayerByNetworkDefinedID(m_players[id].NetId) ~= nil) then
					local rotation = gem.rotation + (360 * (gem.speed / 60) * timeElapsed)
					local targetPos = getTargetWorldPosition(gem.id, gem.type)
					local boundingTop = getBoundingTop(targetPos, gem.id, gem.type) or 0
					
					--Update the adjusted offsets if the object has been rotated
					if gem.type == ObjectType.DYNAMIC and gem.objRY ~= targetPos.ry then
						gem.objRY = targetPos.ry
						local offsets = {x = gem.offsetX, y = gem.offsetY, z = gem.offsetZ}
						gem.offsetX2, gem.offsetY2, gem.offsetZ2 = getRotatedOffsets(offsets, gem.objRY)
					end
					
					KEP_SetDynamicObjectRotation(gemPID, 0, rotation, 0)
					KEP_SetDynamicObjectPosition(gemPID, targetPos.x + gem.offsetX2, boundingTop + gem.offsetY2 + (gem.moveY * math.sin(rotation)), targetPos.z + gem.offsetZ2)
					g_objectIndicators[id][gemPID].rotation = rotation
				else --remove indicators and move on the the next object or player in the list
					for gemPID, gem in pairs(g_objectIndicators[id]) do
						KEP_DeleteDynamicObjectPlacement(gemPID)
					end
					g_objectIndicators[id] = {}
					break
				end
			end
		end
	end

	for id, indicator in pairs(g_healthIndicators) do
		if indicator ~= nil then
			local targetPos = getTargetWorldPosition(indicator.id, indicator.type)
			local headPos = getHeadPos(targetPos, indicator.id, indicator.type)

			-- Return if failed to get target's position
			if (headPos.x ~= nil) and (headPos.y ~= nil) and (headPos.z ~= nil) then
				if tonumber(headPos.z) < 1 then 
					local menuLoc = MenuGetLocationThis()
					local menuX = menuLoc.x
					local menuY = menuLoc.y
					--Resize the HP box based on distance to the camera
					local fontSize = (1000 / targetPos.z) - 1000
					if fontSize < 0 then
						fontSize = -fontSize
					end
					fontSize = math.min(fontSize,100)
					local HPBarWidth = fontSize*.7
					HPBarWidth = (fontSize*.7) * (indicator.health/100)
					if indicator.health <= 0 then
						HPBarWidth = 0
		
						Control_SetVisible(indicator.bg, false)
						Control_SetVisible(indicator.fg, false)
					else
						local HPBarHeight = fontSize*.1
						local HPFrameWidth = fontSize*.7
						local HPFrameHeight = fontSize*.1
	
						Control_SetSize(indicator.fg, HPBarWidth, HPBarHeight)
						Control_SetSize(indicator.bg, HPFrameWidth+4, HPFrameHeight+4)	
						Control_SetVisible(indicator.bg, true)
						Control_SetVisible(indicator.fg, true)
						-- Set the HP box offset by the menu's location (global coordinates, no menu coordinates)
						if indicator.type == ObjectType.PLAYER then
							Control_SetLocation(indicator.fg, (-menuX+headPos.x-(HPFrameWidth/2)), (-menuY+headPos.y)-(15/2)+4)
							Control_SetLocation(indicator.bg, (-menuX+headPos.x-(HPFrameWidth/2))-2, ((-menuY+headPos.y)-(15/2))+2)
						elseif indicator.type == ObjectType.DYNAMIC then
							Control_SetLocation(indicator.fg, (-menuX+headPos.x-(HPFrameWidth/2)), (-menuY+headPos.y)-(15/2)-10)
							Control_SetLocation(indicator.bg, (-menuX+headPos.x-(HPFrameWidth/2))-2, ((-menuY+headPos.y)-(15/2))-12)
						end
					end							
				else
					Control_SetVisible(indicator.bg, false)
					Control_SetVisible(indicator.fg, false)
				end
			else
				removeHealthIndicator(indicator.type, id)
			end
		end
	end

	if g_statusInfo[1] ~= nil and g_statusInfo[1].sRemaining < 0 then
		removeOldestStatusInfo() -- remove one at a time
	end
	local i
	for i = 1, 3 do
		if g_statusInfo[i] ~= nil then
			g_statusInfo[i].sRemaining = g_statusInfo[i].sRemaining - timeElapsed
		end
	end
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function playerShowStatusInfoEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local placementId = Event_DecodeNumber(event)
	local message = Event_DecodeString(event)
	local time = Event_DecodeNumber(event)
	local r = Event_DecodeNumber(event)
	local g = Event_DecodeNumber(event)
	local b = Event_DecodeNumber(event)

	local info = {}
	info.placementId = placementId
	info.message = message
	info.sRemaining = time
	info.r = r
	info.g = g
	info.b = b

	addStatusInfo(info)
end

function addStatusInfo(newInfo)
	local newSlot = 1
	local i
	for i=1, 3 do
		if g_statusInfo[i] ~= nil then
			newSlot = i + 1
		end
	end
	if newSlot > 3 then
		removeOldestStatusInfo()
		newSlot = 3
	end
	g_statusInfo[newSlot] = newInfo
	showStatusInfoText()
end

function removeOldestStatusInfo()
	g_statusInfo[1] = g_statusInfo[2]
	g_statusInfo[2] = g_statusInfo[3]
	g_statusInfo[3] = nil
	showStatusInfoText()
end

function showStatusInfoText()
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	local menuLoc = MenuGetLocationThis()
	local menuXPos = menuLoc.x
	local menuYPos = menuLoc.y
	
	local i
	for i = 1, 3 do
		local staticName = "stcStatus" .. tostring(i)
		if g_statusInfo[i] ~= nil then
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles[staticName])), 0, {a=255, r=g_statusInfo[i].r, g=g_statusInfo[i].g, b=g_statusInfo[i].b})
			Static_SetText(gHandles[staticName], g_statusInfo[i].message)
			Control_SetLocation( gHandles[staticName], -menuXPos + (screenWidth/2) - (Control_GetWidth(gHandles[staticName])/2), -menuYPos + 100 + 30*(i-1))
			Control_SetVisible(gHandles[staticName], true)
			Control_SetEnabled(gHandles[staticName], true)
		else
			Control_SetLocation(gHandles[staticName], 1000, 1000)
			Control_SetVisible(gHandles[staticName], false)
			Control_SetEnabled(gHandles[staticName], false)
		end
	end
end

function addHealthIndicatorHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local placementId = Event_DecodeNumber(event)
	local type = Event_DecodeNumber(event)
	local id = Event_DecodeString(event)
	local healthPercentage = Event_DecodeNumber(event)

	-- Object health bar
	if (type == ObjectType.DYNAMIC) and (KEP_DynamicObjectGetGLID(id) ~= 0) then
		-- Create new object health indicator
		if (g_healthIndicators[id] == nil) then
			local newIndicator = {}
			newIndicator.type = type
			newIndicator.id = id
			newIndicator.health = healthPercentage
			newIndicator.bg = Dialog_AddImage( gDialogHandle, "bg_" .. tostring(type) .. "_" .. tostring(id), 150, 200, "HealthBar.tga", 0, 0, 315, 30 )
			Control_SetSize(newIndicator.bg, 100, 20)
			Control_SetLocation(newIndicator.bg, 100, 200)
			Control_SetVisible(newIndicator.bg, false)
			newIndicator.fg = Dialog_AddImage( gDialogHandle, "fg_" .. tostring(type) .. "_" .. tostring(id), 150, 200, "HealthBar.tga", 0, 54, 325, 75 )
			Control_SetSize(newIndicator.fg, 100, 20)
			Control_SetLocation(newIndicator.fg, 100, -300)
			Control_SetVisible(newIndicator.fg, false)
			g_healthIndicators[id] = newIndicator
		
		-- Update existing health bar
		elseif g_healthIndicators[id] then
			g_healthIndicators[id].health = healthPercentage
		end
	
	-- Player health bar
	elseif type == ObjectType.PLAYER then
		updateNetIds()
		-- Create new health indicator
		if (m_players[id] ~= nil) and (g_healthIndicators[id] == nil) then
			local newIndicator = {}
			newIndicator.type = type
			newIndicator.id = id
			newIndicator.health = healthPercentage
			newIndicator.bg = Dialog_AddImage( gDialogHandle, "bg_" .. tostring(type) .. "_" .. tostring(id), 100, 200, "HealthBar.tga", 0, 0, 325, 30 )
			Control_SetSize(newIndicator.bg, 100, 20)
			Control_SetLocation(newIndicator.bg, 100, 200)
			Control_SetVisible(newIndicator.bg, false)
			newIndicator.fg = Dialog_AddImage( gDialogHandle, "fg_" .. tostring(type) .. "_" .. tostring(id), 100, 200, "HealthBar.tga", 0, 54, 325, 75 )
			Control_SetSize(newIndicator.fg, 100, 20)
			Control_SetLocation(newIndicator.fg, 100, -300)
			Control_SetVisible(newIndicator.fg, false)
			g_healthIndicators[id] = newIndicator
		
		-- Update existing health bar
		elseif g_healthIndicators[id] then
			g_healthIndicators[id].health = healthPercentage
		end
	end
end

function removeHealthIndicatorHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local placementId = Event_DecodeNumber(event)
	local type = Event_DecodeNumber(event)
	local id = Event_DecodeString(event)

	removeHealthIndicator(type, id)
end

-- function textHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

-- 	local msg = KEP_EventDecodeString( event )
-- 	local type = KEP_EventDecodeNumber( event )
-- 	local lmsg = string.lower(msg)
-- 	local msgS, msgE, player = string.find(msg, "Person (.+) not found") 

-- 	log("--- messageType : ".. tostring(type))
-- 	log("--- message : ".. tostring(msg))
-- 	log("--- msgS : ".. tostring(msgS))
-- 	log("--- msgE : ".. tostring(msgE))
-- 	log("--- player : ".. tostring(player))

-- 	-- Log To KanevaChatLog.log
-- 	-- KEP_LogChat(msg)

-- 	-- g_newMessage = true
-- 	-- if type == 3 or player ~= nil then

-- 	-- 	if player ~= nil then
-- 	-- 		if g_currentPrivates[string.lower(player)] and g_currentPrivates[string.lower(player)].replied == true then
-- 	-- 			msg = g_currentPrivates[string.lower(player)].realName.." has gone offline."
-- 	-- 		else
-- 	-- 			msg = player.." is offline or does not exist."
-- 	-- 			g_privateWith = string.lower(player)
-- 	-- 		end
-- 	-- 		addPrivateMessage(msg, player, false)
-- 	-- 	end
-- 	-- 	ret, ret2, replyTo, ret = string.find( msg, "(.+) tells you >" )
-- 	-- 	if replyTo ~= nil then
-- 	-- 		addPrivateMessage(msg, replyTo, true)
-- 	-- 		newPrivateMessage(replyTo)

				
-- 	-- 	else
-- 	-- 		ret, ret2, replyTo, ret = string.find( msg, "You tell (.+) >" )
-- 	-- 		if replyTo ~= nil then
-- 	-- 			addPrivateMessage(msg, replyTo, false)
-- 	-- 			g_privateWith = string.lower(replyTo)
-- 	-- 			OpenPrivateTab()
-- 	-- 		end
-- 	-- 	end
-- 	-- else
-- 	-- 	if tableCount(g_chatlines) >= g_chatbuffersize then
-- 	-- 		table.remove( g_chatlines, 1)
-- 	-- 		table.remove( g_chattypes, 1)
-- 	-- 	end
-- 	-- 	table.insert( g_chatlines, msg)
-- 	-- 	table.insert( g_chattypes, type)
-- 	-- end

-- 	-- LayoutAll()

-- 	-- local replyTo = nil
-- 	-- ret, ret2, replyTo, ret = string.find( msg, "(.+) tells you >" )
-- 	-- if replyTo ~= nil then
-- 	-- 	g_replyTo = replyTo
-- 	-- 	fireReplyUpdateEvent()
-- 	-- end
-- end

function removeHealthIndicator(type, id)
	for id, indicator in pairs(g_healthIndicators) do
		if indicator.type == type and indicator.id == id then
			Dialog_RemoveControl(gDialogHandle, "bg_" .. tostring(type) .. "_" .. tostring(id))
			Dialog_RemoveControl(gDialogHandle, "fg_" .. tostring(type) .. "_" .. tostring(id))
			g_healthIndicators[id] = nil
			return
		end
	end
end

function addIndicatorHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local type = Event_DecodeNumber(event)
	local id = Event_DecodeString(event)
	local offsetX = Event_DecodeNumber(event)
	local offsetY = Event_DecodeNumber(event)
	local offsetZ = Event_DecodeNumber(event)
	local glid = Event_DecodeNumber(event)
	local speed = Event_DecodeNumber(event)
	local moveY = Event_DecodeNumber(event)
	
	addIndicator({type = type, id = id, offsetX = offsetX, offsetY = offsetY, offsetZ = offsetZ, glid = glid, speed = speed, moveY = moveY})
end

function clearObjectIndicatorsHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local type = Event_DecodeNumber(event)
	local id = Event_DecodeString(event)


	clearIndicator({type = type, id = id})

end

function updateNetIds()
	-- Get all net Ids in the current zone
	local netIds = KEP_GetAllNetIds()
	local valueStart, value
	local valueEnd = 0
	
	-- extract all netIds
	valueStart, valueEnd, value = string.find(netIds, "[^%s]+", valueEnd)
	-- For each netId, check if it's currently registered
	while valueEnd <= string.len(netIds) do
		local netId = (tostring(string.sub(netIds, valueStart, valueEnd)))
		-- Get this player's name
		local player = KEP_GetPlayerByNetworkDefinedID(netId )
		-- Return if failed
		if player == nil then return end
		local playerName = GetSet_Safe_GetString( player, MovementIds.NAME )

		-- Ensure a playerName was returned
		if playerName and playerName ~= "" then
			-- Check if this player is currently registered
			if m_players[playerName] == nil then
				m_players[playerName] = {}
			end
			-- Register this player's netId
			m_players[playerName].NetId = tonumber(netId)
			-- Get the next netId if one exists
			valueStart, valueEnd, value = string.find(netIds, "[^%s]+", valueEnd+1)
			-- Fail check
			if valueEnd == nil then break end
		else
			LogWarn("updateNetIds: Unable to return playerName for netId["..tostring(netId).."]")
		end
	end
end

-- Gets the World Position of a target
function getTargetWorldPosition(target, targetType)
	local targetPos = {x = 0, y = -1000, z = 0}
	-- Different call for target type
	if targetType == ObjectType.PLAYER and m_players[target] ~= nil then
		local playerTarget = KEP_GetPlayerByNetworkDefinedID(m_players[target].NetId)
		if playerTarget ~= nil and KEP_ObjectIsValid(playerTarget) == 1 then
			targetPos.x, targetPos.y, targetPos.z = GetSet_Safe_GetVector(playerTarget, MovementIds.LASTPOSITION)
		end
	elseif targetType == ObjectType.DYNAMIC then
		targetPos.x, targetPos.y, targetPos.z = KEP_DynamicObjectGetPosition(tonumber(target))
		targetPos.rx, targetPos.ry, targetPos.rz = KEP_DynamicObjectGetRotation(tonumber(target))
	end
	return targetPos
end

-- Returns the head position of an object based on its type and location
function getHeadPos(targetPos, targetObject, targetType, yOffset)
	local headPos = {}
	yOffset = yOffset or 0
	-- Get object's position based on type filter
	if targetType == ObjectType.PLAYER then
		headPos.x, headPos.y, headPos.z = KEP_WorldToScreen(targetPos.x, targetPos.y+8.5+yOffset, targetPos.z)
	elseif targetType == ObjectType.DYNAMIC then
		-- Check if object exists, invalid objects have a GLID of 0
		local testGLID = KEP_DynamicObjectGetGLID(tonumber(targetObject))
		if testGLID == 0 then
			return {} -- Return an empty table to trigger cleanup
		end
	
		-- Bottom-right and top-left bound positions
		local BRbound = {}
		local TLbound = {}
		local tempX, tempY
		-- bottom-right and upper-left points of object's bounding box
		local x,y,z,x2,y2,z2 = KEP_DynamicObjectGetBoundingBox(tonumber(targetObject))
		BRbound.x, BRbound.y, BRbound.z = KEP_WorldToScreen(x, y+yOffset, z)
		TLbound.x, TLbound.y, TLbound.z = KEP_WorldToScreen(x2, y2+yOffset, z2)
		
		tempX, tempY, headPos.z = KEP_WorldToScreen(targetPos.x, targetPos.y+yOffset, targetPos.z)
		
		headPos.x = TLbound.x+((BRbound.x-TLbound.x)/2)
		headPos.y = TLbound.y
	end
	return headPos
end

-- Returns the y position of the top of an object's bounding box
function getBoundingTop(targetPos, targetObject, targetType, yOffset)
	local boundingTop = nil
	yOffset = yOffset or 0
	
	if targetType == ObjectType.PLAYER then
		boundingTop = targetPos.y + 8.5 + yOffset --TODO: Is there a method to get the player's bounding box?
	elseif targetType == ObjectType.DYNAMIC then
		local testGLID = KEP_DynamicObjectGetGLID(tonumber(targetObject))
		if testGLID == 0 then
			return nil
		end
		
		local x,y,z,x2,y2,z2 = KEP_DynamicObjectGetBoundingBox(tonumber(targetObject))
		local boundingHeight = y2 - y
		boundingTop = targetPos.y + boundingHeight + yOffset
	end
	
	return boundingTop
end

function getRotatedOffsets(offsets, rotY)
	local offsetX = offsets.x
	local offsetY = offsets.y
	local offsetZ = offsets.z
	
	if rotY ~= nil then
		local rotX = math.cos(rotY)
		local rotZ = math.sin(rotY)

		offsetX = ( offsets.z * rotX) + (-offsets.x * rotZ)
		offsetZ = (-offsets.x * rotX) - ( offsets.z * rotZ)
	end	
	return offsetX, offsetY, offsetZ
end


function addIndicator(event)

	local type = event.type
	local id = event.id
	local offsetX = event.offsetX
	local offsetY = event.offsetY
	local offsetZ = event.offsetZ
	local glid = event.glid
	local speed = event.speed
	local moveY = event.moveY

	if type == ObjectType.DYNAMIC and KEP_DynamicObjectGetGLID(tonumber(id)) ~= 0 then
		local newGem = {type = type, id = id, offsetX = offsetX, offsetY = offsetY, offsetZ = offsetZ, speed = speed, rotation = 0, moveY = moveY}
		
		local pos = getTargetWorldPosition(newGem.id, newGem.type)
		local offsets = {x = newGem.offsetX, y = newGem.offsetY, z = newGem.offsetZ}
		newGem.objRY = pos.ry
		newGem.offsetX2, newGem.offsetY2, newGem.offsetZ2 = getRotatedOffsets(offsets, newGem.objRY)		
		local gemLocalPID = KEP_PlaceLocalDynamicObj(glid, 1.0, 1, 0, pos.x+newGem.offsetX2, pos.y+newGem.offsetY2, pos.z+newGem.offsetZ2)		
		
		if g_objectIndicators[id] == nil then
			g_objectIndicators[id] = {}
		end
		g_objectIndicators[id][gemLocalPID] = newGem
	elseif type == ObjectType.PLAYER then
		updateNetIds()
		local targetPos = getTargetWorldPosition(id, type)
		local gemLocalPID = KEP_PlaceLocalDynamicObj(glid, 1.0, 1, 0, targetPos.x + offsetX, targetPos.y + offsetY, targetPos.z + offsetZ)
		local newGem = {type = type, id = id, offsetX = offsetX, offsetX2 = offsetX, offsetY = offsetY, offsetY2 = offsetY, offsetZ = offsetZ, offsetZ2 = offsetZ, speed = speed, rotation = 0, moveY = moveY}
		if g_objectIndicators[id] == nil then
			g_objectIndicators[id] = {}
		end
		g_objectIndicators[id][gemLocalPID] = newGem
	end
end

function  clearIndicator(event)
		id = event.id
		if g_objectIndicators[id] ~= nil then
		for gemLocalPID, gem in pairs(g_objectIndicators[id]) do
			KEP_DeleteDynamicObjectPlacement(gemLocalPID)
		end
		g_objectIndicators[id] = {}
	end
end