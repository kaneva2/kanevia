--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_DeathWarn.lua
--
-- Warning message that displays the first two times a player dies
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

function onCreate()
	KEP_EventRegisterHandler( "frameworkSendDeathCountHandler", "FRAMEWORK_SEND_DEATH_COUNT", KEP.MED_PRIO )
end

function frameworkSendDeathCountHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	 local deathCount = KEP_EventDecodeNumber(tEvent)

	local stc = Dialog_GetStatic(gDialogHandle, "stcHeader")
	if deathCount == 1 then
		Dialog_SetCaptionText(gDialogHandle, "First Death" )
	    Static_SetText(stc, "You have died.")
	    Control_SetVisible(stc, true)
	    stc = Dialog_GetStatic(gDialogHandle, "stcBody1")
	    Static_SetText(stc, "WARNING: The contents of your Backpack will drop on the ground upon your next death. These items can possibly be retrieved from your corpse.")
	    Control_SetVisible(stc, true)
	    stc = Dialog_GetStatic(gDialogHandle, "stcTip")
	    Control_SetVisible(stc, true)
	    stc = Dialog_GetControl(gDialogHandle, "btnOk")
	    Control_SetEnabled(stc, true)
	    Control_SetVisible(stc, true)
	else
		Dialog_SetCaptionText(gDialogHandle, "Second Death" )
		Static_SetText(stc, "You died again.")
		Control_SetVisible(stc, true)
	    stc = Dialog_GetStatic(gDialogHandle, "stcBody1")
	    Static_SetText(stc, "Your backpack items have dropped with your corpse, and can be retrieved within 10 minutes...unless someone else gets there first!")
	    Control_SetVisible(stc, true)
	    stc = Dialog_GetControl(gDialogHandle, "btnOk")
	    Control_SetEnabled(stc, true)
	    Control_SetVisible(stc, true)
	end
end