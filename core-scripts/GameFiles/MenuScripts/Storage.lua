--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("InventoryHelper.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

SEND_GLID_EVENT = "SendGLIDEvent"
UPDATE_INVENTORY_EVENT = "UpdateInventoryEvent"

CLICKED_CONTROLS = {"btnRestore", "stcQuantity", "btnInfo", "btnDiscard"}

-- Data is for Categories that don't match their display name
CATEGORIES = 
{
	{name = "All",				data = ""						},
	{name = "Tops"												},
	{name = "Bottoms" 											},
	{name = "Shoes" 											},
	{name = "Costumes" 											},
	{name = "Accessories"										},
	{name = "Emotes" 											},
	{name = "Underwear",		gender = "M"					},
	{name = "Lingerie",			gender = "F"					},
	{name = "Dresses & Skirts",	data = "Dresses %26 Skirts"		},
	{name = "Access Pass"										},
	{name = "Materials",		data = "Building Materials"		},
	{name = "Architecture"										},
	{name = "Furnishings"										},
	{name = "Home & Garden",	data = "Home and Garden"		},
	{name = "Electronics",		data = "Electronics %26 Games"	},
	{name = "Vehicles"											},
	{name = "Gameplay"											},
	{name = "Characters"			 							},
	{name = "Gifts"												},
	{name = "Sound FX",			data = "Sound Effects"			},
	{name = "Deeds"												},
	{name = "Particles",		data = "Special Effects"		}
}

g_t_items = {}

g_pendingImages = {}

g_syncInventoryServer = true

g_category = ""

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "attribEventHandler",					"AttribEvent",						KEP.MED_PRIO)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler",			"BrowserPageReadyEvent",			KEP.MED_PRIO)
	KEP_EventRegisterHandler( "ContentServiceCompletionHandler",	"ContentServiceCompletionEvent",	KEP.MED_PRIO)
	KEP_EventRegisterHandler( "updateInventoryHandler",				UPDATE_INVENTORY_EVENT,				KEP.MED_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( SEND_GLID_EVENT, KEP.MED_PRIO ) 
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ContentServiceCompletionEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate(dialogHandle)

	gPlayerGender = KEP_GetLoginGender()
	populateCategories()

	InventoryHelper.initializeInventory("g_t_items") 
	InventoryHelper.initializeHover()
	InventoryHelper.initializeClicked(CLICKED_CONTROLS)
	InventoryHelper.initializePageNumbers()

	requestInventory(true)
end

------------------------------------------------------------------------------------
-- Getting/Loading Items
------------------------------------------------------------------------------------
function inventoryUpdated()
	requestInventory(true)
end

function requestInventory(asynchronous)
	clearPage(false)

	if (g_syncInventoryServer == true) then
		g_syncInventoryServer = false
		KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_BANK)
		if (asynchronous) then
			getInventoryFromWeb()
		end
	else
		getInventoryFromWeb()
	end
end

function attribEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	
	--Deleting or transferring item, have to update the server again...
	if (filter == AttribEvent.GENERIC_TYPE) then
		g_syncInventoryServer = true
		requestInventory(true)
	elseif (filter == AttribEvent.PLAYER_INVEN) then
		getInventoryFromWeb()
	end
end

function getInventoryFromWeb()
	InventoryHelper.clearClicked()
	getBankInventory(WF.STORAGE_INVENTORY, InventoryHelper.page, InventoryHelper.itemsPerPage, g_category or "", InventoryHelper.searchText)
end

function ContentServiceCompletionHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if (filter == 2) then
		local ignored = KEP_EventDecodeNumber(event)
		local glid    = KEP_EventDecodeNumber(event)
		if (glid ~= nil) then
			local iconImage = g_pendingImages[glid]
			if (iconImage ~= nil) then
				local texturePath = KEP_AddItemThumbnailToDatabase(glid)
				Element_AddExternalTexture(iconImage, gDialogHandle, texturePath, "loading.dds")
				Element_SetCoords(iconImage, 0, 0, -1, -1)
				g_pendingImages[glid] = nil
			end
		end
	end

end

function updateInventoryHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter ~= DELETED_ITEM_FILTER then
		requestInventory(true)
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.STORAGE_INVENTORY then
		local xmlData = KEP_EventDecodeString( event )

		-- Test to make sure valid results and results are up to date (for multiple fast calls)
		local result = string.match(xmlData, "<ReturnCode>(.-)</ReturnCode>")
		local passthrough = tonumber( string.match(xmlData,"<Passthrough>(.-)</Passthrough>") or -1 )
		if result ~= "0" or passthrough ~= cf_bankPassthrough then
			return
		end
		
		-- Update the pages
		local totalItems = string.match(xmlData, "<TotalNumberRecords>(%d-)</TotalNumberRecords>")
		InventoryHelper.setMaxPages(totalItems)

		g_t_items = {}
		decodeInventoryItemsFromWeb(g_t_items, xmlData, false)
		
		populateItems(true)

		return KEP.EPR_CONSUMED
	end
end

function populateItems(showNoResults)
	g_pendingImages = InventoryHelper.setIcons({"bundle_global_id", "default_glid", "GLID"}, "SmartObj_ScriptIcon_82x82.tga")
	InventoryHelper.setNameplates({"display_name", "name"})
	setNoResults(showNoResults and #g_t_items == 0)
end

-- What to do if no results are returned
function setNoResults(showNoResults)
	Control_SetVisible(gHandles["imgStorageUpsell"], showNoResults)
end

function clearPage()
	g_t_items = {}
	populateItems(false)
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	InventoryHelper.onButtonDown(x, y)
	
	-- If clicking outside the categories dropdown, hide it
	if Control_ContainsPoint(gHandles["lstCategories"], x, y) == 0 then
		Control_SetVisible(gHandles["lstCategories"], false)
	end
end

----------------------------------------------------------------------
-- Category-Related Functions
----------------------------------------------------------------------
-- Keep up to date with categories in the lua instead of XML, adjust size of dropdown
function populateCategories()
	local lstCategories = gHandles["lstCategories"]
	ListBox_RemoveAllItems(lstCategories)
	local numItems = 0
	
	for i, v in ipairs(CATEGORIES) do
		if v.gender == nil or v.gender == gPlayerGender then
			ListBox_AddItem( lstCategories, v.name, i )
			numItems = numItems + 1
		end
	end
	
	Control_SetSize(lstCategories, Control_GetWidth(lstCategories), (ListBox_GetRowHeight(lstCategories)+.5) * numItems)
end

-- Not true combobox, show listbox
function btnCategories_OnButtonClicked(btnHandle)
	local lstCategories = gHandles["lstCategories"]
	Control_SetVisible(lstCategories, true)
	Control_SetFocus(lstCategories)
end

function lstCategories_OnListBoxSelection(lstHandle)
	changeCategory(ListBox_GetSelectedItemText(lstHandle))
	Control_SetVisible(lstHandle, false)
end

function changeCategory(category)
	category = category or "All" -- If no parameter, assume "All"

	-- Categories stored by index order for populating the dropdown, have to loop
	for i, v in ipairs(CATEGORIES) do
		if v.name == category then
			g_category = v.data or v.name -- Check for data first, sometimes display name does not match db
			Static_SetText(gHandles["stcCategories"], v.name)
			break
		end
	end

	-- Clear search and reset to page 1 on changing category
	InventoryHelper.clearSearch()
	InventoryHelper.page = 1

	requestInventory(true)
end

----------------------------------------------------------------------
-- Item Buttons
----------------------------------------------------------------------
function btnRestore_OnButtonClicked( buttonHandle )
	if KEP_IsWorld() then
		KEP_MessageBox("Function currently unavailable in Developer Worlds.")
		return
	end

	if InventoryHelper.selectedIndex then
		local item = g_t_items[InventoryHelper.selectedIndex]
		if item then
			KEP_WithdrawItemFromBank(item.GLID, item.quantity, item.inventory_sub_type)
		end
	end
end

function btnInfo_OnButtonClicked( buttonHandle )

	if MenuIsClosed("InventoryInfo.xml") and InventoryHelper.selectedIndex then
		MenuOpenModal("InventoryInfo.xml")
		item = g_t_items[InventoryHelper.selectedIndex]

		local ev = KEP_EventCreate( SEND_GLID_EVENT)
		KEP_EventEncodeNumber(ev, item.GLID or item.glid)
		KEP_EventEncodeNumber(ev, item.inventory_sub_type)
		KEP_EventEncodeString(ev, item.display_name or item.name)
		KEP_EventEncodeString(ev, item.description or "")
		KEP_EventEncodeString(ev, item.creator or "")
		KEP_EventEncodeString(ev, "Storage.xml")
		KEP_EventQueue(ev)
	end
end

function btnDiscard_OnButtonClicked( buttonHandle )

	if KEP_IsWorld() then
		KEP_MessageBox("Function currently unavailable in Developer Worlds.")
		return
	end

	if MenuIsClosed("InventoryDelete.xml") and InventoryHelper.selectedIndex then
		MenuOpenModal("InventoryDelete.xml")

		item = g_t_items[InventoryHelper.selectedIndex]
		local ev = KEP_EventCreate( SEND_GLID_EVENT)
		KEP_EventEncodeNumber(ev, item.GLID)
		KEP_EventEncodeNumber(ev, item.inventory_sub_type)
		KEP_EventQueue( ev)
	end
end

function btnInventory_OnButtonClicked(btnHandle)
	Log("Inventory Clicked")
	if MenuIsClosed("UnifiedNavigation.xml") then
		MenuOpen("UnifiedNavigation.xml")
		local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
		KEP_EventEncodeString(ev, "clothing")
		KEP_EventQueue(ev)
	end
	MenuCloseThis()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	InventoryHelper.OnKeyDown(key)
end
