--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

g_charDetail = 0
g_charDetailAuto = true
g_animQuality = 0
g_animQualityAuto = true
g_particleQuality = 5
g_particleQualityAuto = true
g_avatarNames = 5
g_avatarNamesAuto = true
g_dynObjQuality = 5
g_dynObjQualityAuto = true
g_shadows = true
g_invertY = false
g_mLookSwap = true
g_mediaEnabled = true

function ContainsPoint(ctl, x, y)
	return ToBool(Control_ContainsPoint(ctl, x, y))
end

function SetChecked(ctl, checked)
	CheckBox_SetChecked(ctl, ToBool(checked))
end

function GetChecked(ctl)
	return ToBool(CheckBox_GetChecked(ctl))
end


function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- Get Controls
	g_lblAnimQuality = Dialog_GetStatic(dialogHandle,"lblAnimQuality")
	g_sldAnimQuality = Dialog_GetSlider(dialogHandle,"sldAnimQuality")
	g_chkAnimQualityAuto = Dialog_GetCheckBox(dialogHandle, "chkAnimQualityAuto")
	g_lblParticleQuality = Dialog_GetStatic(dialogHandle,"lblParticleQuality")
	g_sldParticleQuality = Dialog_GetSlider(dialogHandle,"sldParticleQuality")
	g_chkParticleQualityAuto = Dialog_GetCheckBox(dialogHandle, "chkParticleQualityAuto")
	g_lblCharDetail = Dialog_GetStatic(dialogHandle,"lblCharDetail")
	g_sldCharDetail = Dialog_GetSlider(dialogHandle,"sldCharDetail")
	g_chkCharDetailAuto = Dialog_GetCheckBox(dialogHandle, "chkCharDetailAuto")
	g_lblAvatarNames = Dialog_GetStatic(dialogHandle,"lblAvatarNames")
	g_sldAvatarNames = Dialog_GetSlider(dialogHandle,"sldAvatarNames")
	g_chkAvatarNamesAuto = Dialog_GetCheckBox(dialogHandle, "chkAvatarNamesAuto")
	g_lblDynObjQuality = Dialog_GetStatic(dialogHandle,"lblDynObjQuality")
	g_sldDynObjQuality = Dialog_GetSlider(dialogHandle,"sldDynObjQuality")
	g_chkDynObjQualityAuto = Dialog_GetCheckBox(dialogHandle, "chkDynObjQualityAuto")
	g_chkShadows = Dialog_GetCheckBox(dialogHandle,"chkShadows")
	g_chkMediaEnabled = Dialog_GetCheckBox(dialogHandle,"chkMediaEnabled")
	g_chkInvertY = Dialog_GetCheckBox(dialogHandle,"chkInvertY")
	g_chkMLookSwap = Dialog_GetCheckBox(dialogHandle,"chkMLookSwap")

	Slider_SetRange(g_sldAnimQuality, 1, 10 )
	Slider_SetRange(g_sldParticleQuality, 1, 10 )
	Slider_SetRange(g_sldCharDetail, 1, 10 )
	Slider_SetRange(g_sldAvatarNames, 1, 10 )
	Slider_SetRange(g_sldDynObjQuality, 1, 10 )

	-- ED-7816 - Anti-Aliasing
	Static_SetText(g_chkShadows, "Anti-Alias")

	loadSettings()
end

function Dialog_OnDestroy(dialogHandle)
	KEP_WriteConfigSettings()
	Helper_Dialog_OnDestroy( dialogHandle )
end

function sldAnimQuality_OnSliderValueChanged( sh )
	g_animQuality = Slider_GetValue( sh )
	if not g_loadSettings then 
		SetChecked(g_chkAnimQualityAuto, false)
		g_animQualityAuto = false
		KEP_SetAnimationBias(g_animQuality, ToBOOL(g_animQualityAuto))
	end
end

function chkAnimQualityAuto_OnCheckBoxChanged(ch)
	g_animQualityAuto = GetChecked(ch) 
	KEP_SetAnimationBias(g_animQuality, ToBOOL(g_animQualityAuto))
end

function sldParticleQuality_OnSliderValueChanged( sh )
	g_particleQuality = Slider_GetValue( sh )
	if not g_loadSettings then 
		SetChecked(g_chkParticleQualityAuto, false)
		g_particleQualityAuto = false
		KEP_SetParticleBias(g_particleQuality, ToBOOL(g_particleQualityAuto))
	end
end

function chkParticleQualityAuto_OnCheckBoxChanged(ch) 
	g_particleQualityAuto = GetChecked(ch)
	KEP_SetParticleBias(g_particleQuality, ToBOOL(g_particleQualityAuto))
end

function sldCharDetail_OnSliderValueChanged( sh )
	g_charDetail = Slider_GetValue( sh )
	if not g_loadSettings then 
		SetChecked(g_chkCharDetailAuto, false)
		g_charDetailAuto = false
		KEP_SetLodBias(g_charDetail, ToBOOL(g_charDetailAuto))
	end
end

function chkCharDetailAuto_OnCheckBoxChanged(ch) 
	g_charDetailAuto = GetChecked(ch) 
	KEP_SetLodBias(g_charDetail, ToBOOL(g_charDetailAuto))
end

function sldAvatarNames_OnSliderValueChanged( sh )
	g_avatarNames = Slider_GetValue( sh )
	if not g_loadSettings then 
		SetChecked(g_chkAvatarNamesAuto, false)
		g_avatarNamesAuto = false
		KEP_SetAvatarNames(g_avatarNames, ToBOOL(g_avatarNamesAuto))
	end
end

function chkAvatarNamesAuto_OnCheckBoxChanged(ch) 
	g_avatarNamesAuto = GetChecked(ch)
	KEP_SetAvatarNames(g_avatarNames, ToBOOL(g_avatarNamesAuto))
end

function sldDynObjQuality_OnSliderValueChanged( sh )
	g_dynObjQuality = Slider_GetValue( sh )
	if not g_loadSettings then 
		SetChecked(g_chkDynObjQualityAuto, false)
		g_dynObjQualityAuto = false
		KEP_SetDOCullBias(g_dynObjQuality, ToBOOL(g_dynObjQualityAuto))
	end
end

function chkDynObjQualityAuto_OnCheckBoxChanged(ch) 
	g_dynObjQualityAuto = GetChecked(ch)
	KEP_SetDOCullBias(g_dynObjQuality, ToBOOL(g_dynObjQualityAuto))
end

function chkShadows_OnCheckBoxChanged(ch) 
	g_shadows = GetChecked(ch) 
--	KEP_SetShadows(ToBool(g_shadows))
	local antiAlias = 0
	local str = "Disable"
	if ToBool(g_shadows) then 
		antiAlias = 16 
		str = "Enable"
	end
	KEP_SetAntiAlias(antiAlias)
	KEP_MessageBox("Restart To "..str.." Anti-Alias")
end
function chkInvertY_OnCheckBoxChanged(ch) 
	g_invertY = GetChecked(ch) 
	KEP_SetInvertY(ToBOOL(g_invertY))
end
function chkMLookSwap_OnCheckBoxChanged(ch) 
	g_mLookSwap = GetChecked(ch) 
	KEP_SetMLookSwap(ToBOOL(g_mLookSwap))
end

function chkMediaEnabled_OnCheckBoxChanged(ch) 
	g_mediaEnabled = GetChecked(ch) 
	KEP_SetMediaEnabled(ToBOOL(g_mediaEnabled))
end

function loadSettings()
	g_loadSettings = true

	g_animQuality, g_animQualityAuto = KEP_GetAnimationBias()
	g_animQualityAuto = ToBool(g_animQualityAuto)
	g_particleQuality, g_particleQualityAuto = KEP_GetParticleBias()
	g_particleQualityAuto = ToBool(g_particleQualityAuto)
	g_charDetail, g_charDetailAuto = KEP_GetLodBias()
	g_charDetailAuto = ToBool(g_charDetailAuto)
	g_avatarNames, g_avatarNamesAuto = KEP_GetAvatarNames()
	g_avatarNamesAuto = ToBool(g_avatarNamesAuto)
	g_dynObjQuality, g_dynObjQualityAuto = KEP_GetDOCullBias()
	g_dynObjQualityAuto = ToBool(g_dynObjQualityAuto)
--	g_shadows = ToBool(KEP_GetShadows())
	g_shadows = ToBool(KEP_GetAntiAlias() > 0)
	g_mediaEnabled = ToBool(KEP_GetMediaEnabled())
	g_invertY = ToBool(KEP_GetInvertY())
	g_mLookSwap = ToBool(KEP_GetMLookSwap())

	Slider_SetValue(g_sldAnimQuality, g_animQuality )
	SetChecked(g_chkAnimQualityAuto, g_animQualityAuto)

	Slider_SetValue(g_sldParticleQuality, g_particleQuality )
	SetChecked(g_chkParticleQualityAuto, g_particleQualityAuto)

	Slider_SetValue(g_sldCharDetail, g_charDetail )
	SetChecked(g_chkCharDetailAuto, g_charDetailAuto)

	Slider_SetValue(g_sldAvatarNames, g_avatarNames )
	SetChecked(g_chkAvatarNamesAuto, g_avatarNamesAuto)

	Slider_SetValue(g_sldDynObjQuality, g_dynObjQuality )
	SetChecked(g_chkDynObjQualityAuto, g_dynObjQualityAuto)

	SetChecked(g_chkShadows, g_shadows)
	SetChecked(g_chkInvertY, g_invertY)
	SetChecked(g_chkMLookSwap, g_mLookSwap)
	SetChecked(g_chkMediaEnabled, g_mediaEnabled)

	g_loadSettings = false
end
