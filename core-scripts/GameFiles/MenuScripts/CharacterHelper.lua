--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- dofiled from LoginMenu Only!
dofile("..\\Scripts\\GetAttributeType.lua")

g_EnableCharModify = false
g_GetCharCountId = 0 -- avoid infinte recursion

function chLog(txt)
    KEP_Log("CharacterHelper::"..txt)
end

function CharacterHelperInitializeKEPEvents(dispatcher, handler, debugLevel)
    -- Nothing To Do Yet
end

function CharacterHelperInitializeKEPEventHandlers( dispatcher, handler, debugLevel, prio )
    KEP_EventRegisterHandler( "StringListEventHandler", "StringListEvent", prio )
	KEP_EventRegisterHandler( "ModifyCharEventHandler", "ModifyCharEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BadCharacterConfigEventHandler", "BadCharacterConfigEvent", prio )

	g_GetCharCountId = GetSet_AddNewNumericMember( KEP.gs, "GetCharCount", 0, KEP.amfTransient )
end

function chGetCharacters()
	getCharCount = GetSet_GetNumber( KEP.gs, g_GetCharCountId ) or 0
	chLog("chGetCharacters: charCount="..getCharCount)

	-- avoid infinite loop is server having issues
	if  getCharCount > 2 then
		Dialog_MsgBox( "Create Character Error - Please Try Again")
		KEP_Quit()
		return
	end

	-- Get Characters -> StringListEvent
	GetSet_SetNumber( KEP.gs, g_GetCharCountId, getCharCount + 1 );
	event = KEP_EventCreate( "GetAttributeEvent" )
	if event ~= nil then
		KEP_EventEncodeNumber( event, GetAttributeType.GET_CHARACTER_LIST ) -- attribute
		KEP_EventEncodeNumber( event, 50 ) -- uniInt
		KEP_EventEncodeNumber( event, 0 ) -- refrenceID
		KEP_EventEncodeNumber( event, 0 ) -- uniInt2
		KEP_EventEncodeNumber( event, 0 ) -- uniShort3
		KEP_EventAddToServer( event )
		KEP_EventQueue( event )
	end
end

function ModifyCharEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local doCharMod = KEP_EventDecodeNumber( event ) or 0
	chLog( "ModifyCharEventHandler: doCharMod="..doCharMod )
	if doCharMod == 1 then
		g_EnableCharModify = true
	end
	return KEP.EPR_CONSUMED
end

-- if there's a bad config, re-get chars
function BadCharacterConfigEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	chLog( "BadCharacterConfigEventHandler" )
	g_GetCharCount = 0
	chGetCharacters()
	return KEP.EPR_CONSUMED
end

function RedeemCreateCharacterFame()
	makeWebCall(GameGlobals.WEB_SITE_PREFIX..NEW_AVATAR_SUFFIX, WF.TARGETED)
end

function StringListEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local count = KEP_EventDecodeNumber( event ) or 0
	local userInt = KEP_EventDecodeNumber( event) or 0
	chLog( "StringListEventHandler: count="..count.." userInt="..userInt )

	if count == 0 or count == nil then
		local config = KEP_ConfigOpenWOK()
		if config ~= nil then
			KEP_ConfigSetNumber(config, "FirstTimeLogin", 1)
			KEP_ConfigSave( config )
		end
	else
		--FirstTimeLogin
		config = KEP_ConfigOpenWOK()
		if config ~= nil then
			KEP_ConfigSetNumber(config, "FirstTimeLogin", 0)
			KEP_ConfigSave( config )
		end
	end

	-- new version sends edb, too if objectid = 1
	local inc = 1
	if objectid ~= nil then
		if objectid == 1 then
			inc = 2
		end
	end

	-- 50 is the logon response/list of characters
	if (userInt ~= 50) then return KEP.EPR_CONSUMED end

	local charEdb = 3
	count = count - 1
	for i=0, count, inc do
		local character = KEP_EventDecodeString( event)
		if inc == 2 then
			local s = KEP_EventDecodeString( event)
			charEdb = tonumber(s)
		end
	end

	g_num_items = count + 1
	chLog( "StringListEventHandler: numItems="..g_num_items )

	-- Create Character
	local name = KEP_GetLoginName()
	local isFemale = KEP_IsFemale()
	if g_num_items > 0 then
		if (inc == 2 and charEdb < 5) or g_EnableCharModify then
			RedeemCreateCharacterFame()
			chCreateCharacter(name, isFemale)
		end
	else -- allow them to create one use
		RedeemCreateCharacterFame()
		chCreateCharacter(name, isFemale)
	end

	-- Play Character (re-zones home or to starting world)
	chLog( "StringListEventHandler: Calling KEP_PlayCharacter(0) ..." )

	KEP_PlayCharacter(0, GetStartingLink()) -- 0 is index
	MenuCloseThis()

	return KEP.EPR_CONSUMED
end

function chCreateCharacter(name, isFemale)
	chLog( "chCreateCharacter: name="..name.." isFemale="..tostring(isFemale))

	--face GLIDs
	FACES_MALE 		= { 2988, 2989, 2990, 3720, 3746 }
	FACES_FEMALE 	= { 2991, 2992, 2993, 3744, 3745 }

	FACE_EYE_RIGHT	= 0
	FACE_EYE_LEFT 	= 2
	FACE_EYEBROW	= 7
	FACE_FACECOVER	= 8

	CONFIG_HEAD		= 6 -- slot for face

	-- Create a default menuObject then create a character from that
	local genderIndex = 0
	if isFemale then
		genderIndex = 11
	else
		genderIndex = 14
	end

	local dummy = KEP_AddMenuGameObject(genderIndex, 0 )

	local face = nil;
	if isFemale then
		face = FACES_FEMALE[1]
	else
		face = FACES_MALE[1]
	end

	KEP_ArmMenuGameObject( dummy, face, 0 )
	KEP_MenuGameObjectSetEquippableConfig( dummy, face, CONFIG_HEAD, 0, 0 )
	KEP_MenuGameObjectSetEquippableConfig( dummy, face, FACE_FACECOVER, 0, 0 )
	KEP_MenuGameObjectSetEquippableConfig( dummy, face, FACE_EYE_RIGHT, 0, 0 )
	KEP_MenuGameObjectSetEquippableConfig( dummy, face, FACE_EYE_LEFT, 0, 0 )
	KEP_MenuGameObjectSetEquippableConfig( dummy, face, FACE_EYEBROW, 0, 0 )

	chLog("chCreateCharacter: MO<"..tostring( dummy )..">" )

	-- Create Character
	if KEP_CreateCharacter(dummy, GameGlobals.STARTER_HOME_ID, name, 2, 100, 100, 100 ) ~= 0 then
		KEP_RemoveMenuGameObject(dummy )
		chGetCharacters()
	end
end
