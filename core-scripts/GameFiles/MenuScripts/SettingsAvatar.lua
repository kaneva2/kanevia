--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")

PLAYER_TITLE_EVENT = "PlayerTitleEvent"
PLAYER_PLAYERID_EVENT = "PlayerPlayerIdEvent"
TITLE_LIST_EVENT = "TitleListEvent"
PLAYER_CHANGETITLE_EVENT = "PlayerChangeTitleEvent"
OPEN_CONTEXT_EVENT = "OpenContextEvent"
PLAYER_INFO_EVENT = "PlayerInfoEvent"

PLAYER_INFO_USERID_REQUEST = 1
PLAYER_INFO_USERID_RESPONSE = 2
PLAYER_INFO_PLAYERID_REQUEST = 3
PLAYER_INFO_PLAYERID_RESPONSE = 4

g_dialogHandle = 0
g_playerId = 0 --id of player looking at titles

local m_titles = {} --table of titles. Will be used so no duplicates show

function onCreate()

	KEP_EventRegister( PLAYER_TITLE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( TITLE_LIST_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( PLAYER_CHANGETITLE_EVENT, KEP.MED_PRIO )

	KEP_EventRegisterHandler( "titleListEventHandler", TITLE_LIST_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "PlayerInfoEventHandler", PLAYER_INFO_EVENT, KEP.MED_PRIO )

	requestPlayerID()

	-- setup look of listbox scrollbars
	SetupListBoxScrollBars("lstTitles")

	CheckBox_SetChecked(gHandles["chkAFK"], GetHideAFKSetting())
end


----------------------------
------Event handler---------
----------------------------

function PlayerInfoEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == PLAYER_INFO_PLAYERID_RESPONSE then
		g_playerId = KEP_EventDecodeNumber( event )
		local e = KEP_EventCreate( PLAYER_TITLE_EVENT )
		if e ~= 0 then
			KEP_EventEncodeNumber( e, g_playerId )
			KEP_EventAddToServer( e )
			KEP_EventQueue( e )
		end
	end
end

-- Handles a TitleListEvent received from the Server, populates the listbox with the player's earned titles
function titleListEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local numrows = KEP_EventDecodeNumber( event )
	lbh = Dialog_GetListBox( gDialogHandle, "lstTitles" )
	if not m_titles["<None>"] then
		ListBox_AddItem( lbh, "<None>", -1 )
		m_titles["<None>"] = true
	end
	if numrows ~= 0 then
		for i = 1, numrows, 1 do
			titleId = KEP_EventDecodeNumber( event )
			titleName = KEP_EventDecodeString( event )
			if not m_titles[titleName] then
				ListBox_AddItem( lbh, titleName, titleId )
				m_titles[titleName] = true
			end
		end
	end
end


--------------------
--Control Functions-
--------------------
function btnCharacterCreation_OnButtonClicked( buttonhandle )
	MenuOpen("CharacterCreation3.xml")
	local ev = KEP_EventCreate("FRAMEWORK_INIT_CHAR_CREATOR")
	KEP_EventEncodeString(ev, "player")
	KEP_EventQueue(ev)
	MenuCloseThis()
end


--AFK checkbox
function chkAFK_OnCheckBoxChanged( checkboxHandle, checked )
	SetHideAFKSetting(checked)
end 

function SetupListBoxScrollBars(listbox_name)
	local lbh = Dialog_GetListBox(gDialogHandle, listbox_name)
	if lbh ~= 0 then
		local sbh = ListBox_GetScrollBar(lbh)
		if sbh ~= 0 then

			local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
				--Blue body
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")

				Element_SetCoords(eh, 1, 37, 17, 69)
			end

			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
				--Grey lined
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end

			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Up arrow button
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end

			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
				--Down Arrow Button
				Element_AddTexture(eh, gDialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end
end

-----------------------
----Local Functions----
-----------------------
function requestPlayerID()
	local ev = KEP_EventCreate( PLAYER_INFO_EVENT)
	KEP_EventSetFilter(ev, PLAYER_INFO_PLAYERID_REQUEST)
	KEP_EventQueue( ev)
end


function lstTitles_OnListBoxSelection( listBoxHandle, bFromKeyboard, bMouseDown, sel_index )
	

	local titleId = ListBox_GetSelectedItemData(gHandles["lstTitles"])

	local titleText =  ListBox_GetItemText( listBoxHandle, sel_index ) or ""
	if titleText == "<None>" then
		titleText = ""
	end
	if(titleId ~= 0) then
		local e = KEP_EventCreate( PLAYER_CHANGETITLE_EVENT )
		KEP_EventEncodeNumber(e, g_playerId)
		KEP_EventEncodeNumber(e, titleId)
		KEP_EventAddToServer( e )
		KEP_EventQueue( e)

		Events.sendEvent("UPDATE_PLAYER_TITLE", {title = titleText})
	end
end