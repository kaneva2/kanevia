--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

---------------------------------------------------------------------
--
-- RecentlyPlaced.lua - Menu script for the RecentlyPlaced.xml menu. This enables
-- users to create their own custom deeds out of spaces they own.
-- @Author: Ben Whatley
--
---------------------------------------------------------------------

-- Script includes
dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\PlayerInformation.lua")
dofile("BuildPlace.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

-- Script globals
g_zoneObjectCount = 0
g_maxObjectCount = 0
g_fillPercentage = 0
g_maxHeight = 0
g_startingYPos = 0
g_itemToPlace = nil
g_retryItemBarPaste = false
g_requestingInventoryForBar = false

-- Recently Placed Items table (prefil with nil for ease)
g_recentlyPlacedItems = {nil, nil, nil, nil, nil}

-- Script constants
ADD_DYNAMIC_OBJECT_EVENT = "AddDynamicObjectEvent"
REMOVE_DYNAMIC_OBJECT_EVENT = "RemoveDynamicObjectEvent"
PURCHASE_GROUP_ITEMS_EVENT = "PurchaseGroupItemsEvent"
RETRY_PASTE_EVENT = "RetryPasteEvent"
NUM_RECENTLY_PLACED_SLOTS = 5
REQUEST_FOR_INVENTORY_ID 	= 51

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	PlayerInformation_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)

	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandlerV( "addDynamicObjectEventHandler", ADD_DYNAMIC_OBJECT_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandlerV( "removeDynamicObjectEventHandler", REMOVE_DYNAMIC_OBJECT_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFiltered( "attribEventHandler", "AttribEvent", REQUEST_FOR_INVENTORY_ID, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "retryPasteHandler", RETRY_PASTE_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "DynamicObjectChangeEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- Set globals and store zone info
	g_maxHeight = Control_GetHeight(gHandles['imgItemMeterBackground'])
	g_startingYPos = Control_GetLocationY(gHandles['imgItemMeter'])

	getDeedItemLimits()

	loadFromConfig()

	-- Minimize the interface until at least one item is placed.
	if(#g_recentlyPlacedItems <= 0) then
		MenuSetMinimizedThis(true)
	end
end

function Dialog_OnDestroy(dialogHandle)
	saveConfig()
	Helper_Dialog_OnDestroy( dialogHandle )
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	-- Loop through all non-nil items and update the visibility of their rollovers
	for i, v in ipairs(g_recentlyPlacedItems) do
		if v == nil then
			break
		end

		if Control_ContainsPoint(gHandles["imgItem" .. i], x, y) ~= 0 then
			if MenuIsClosed("Inventory.xml") then
				Control_SetVisible(gHandles["imgItemRollover" .. i], true)
				Control_SetEnabled(gHandles["imgItemRollover" .. i], true)
			end
		else
			Control_SetVisible(gHandles["imgItemRollover" .. i], false)
			Control_SetEnabled(gHandles["imgItemRollover" .. i], false)
		end
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Get the maximum object count from the deed info.
	if filter == WF.DEED_INFO then
		local returnData = KEP_EventDecodeString( event )
		local tableStart, tableEnd = 0
		local zoneObjectCount = 0
		local maxObjectCount = 0
		-- Just need the Max Object Count here.
		tableStart, tableEnd, g_maxObjectCount = parseDataByTag(returnData, "MaxObjectCount", "(.-)", 1)
		updateItemBar()
		setTooltips()
	elseif g_requestingInventoryForBar and filter == WF.RECENTLY_PLACED_QUANTITIES then
		g_requestingInventoryForBar = false
		g_retryItemBarPaste = false
		if g_itemToPlace ~= nil then

			local returnData = KEP_EventDecodeString( event )
			local s, e, glid = string.find(returnData, "(" .. tostring(g_itemToPlace.globalId) .. ")" )

			-- If glid is not nil, it found a matching one in the return data
			if glid or KEP_IsWorld() then
				placeObjectInFrontOfPlayer(g_itemToPlace.globalId, g_itemToPlace.invType, 0)
				g_itemToPlace = nil
			else
				MenuOpen("PurchaseGroupItems.xml")
				local ev = KEP_EventCreate( PURCHASE_GROUP_ITEMS_EVENT)
				KEP_EventEncodeNumber(ev, 1)
				KEP_EventEncodeNumber(ev, g_itemToPlace.globalId)
				KEP_EventEncodeNumber(ev, 1)
				KEP_EventQueue( ev)
				g_retryItemBarPaste = true
			end
		end
	end
end

-- Called when the KEP_GetPlayerInventory call returns
function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if g_requestingInventoryForBar then
		if filter == REQUEST_FOR_INVENTORY_ID then
			local glids = {g_itemToPlace.globalId}
			getItemQuantities(WF.RECENTLY_PLACED_QUANTITIES, glids)
		end
	end
end

function SendDynamicObjectChangeEvent()
	Log("SendDynamicObjectChangeEvent")
	KEP_EventCreateAndQueue("DynamicObjectChangeEvent")
end

-- Called to update the menu each time a dynamic object is added to the world
function addDynamicObjectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local playerID = filter
	local positionX = KEP_EventDecodeNumber(event)
	local positionY = KEP_EventDecodeNumber(event)
	local positionZ = KEP_EventDecodeNumber(event)
	local rotationX = KEP_EventDecodeNumber(event)
	local rotationY = KEP_EventDecodeNumber(event)
	local rotationZ = KEP_EventDecodeNumber(event)
	local slideX = KEP_EventDecodeNumber(event)
	local slideY = KEP_EventDecodeNumber(event)
	local slideZ = KEP_EventDecodeNumber(event)
	local placementId = KEP_EventDecodeNumber(event)
	local animationID = KEP_EventDecodeNumber(event)
	local assetID = KEP_EventDecodeNumber(event)
	local textureURL = KEP_EventDecodeString(event)
	local friendID = KEP_EventDecodeNumber(event)
	local playlistSWF = KEP_EventDecodeString(event)
	local playlistParams = KEP_EventDecodeString(event)
	local globalId = KEP_EventDecodeNumber(event)
	local attachable = KEP_EventDecodeNumber(event)
	local interactive = KEP_EventDecodeNumber(event)
	local canPlayFlash = KEP_EventDecodeNumber(event)
	local hasCollision = KEP_EventDecodeNumber(event)
	local boundMinX = KEP_EventDecodeNumber(event)
	local boundMinY = KEP_EventDecodeNumber(event)
	local boundMinZ = KEP_EventDecodeNumber(event)
	local boundMaxX = KEP_EventDecodeNumber(event)
	local boundMaxY = KEP_EventDecodeNumber(event)
	local boundMaxZ = KEP_EventDecodeNumber(event)
	local derivable = KEP_EventDecodeNumber(event)
	local invType   = KEP_EventDecodeNumber(event)

	insertRecentlyPlacedItem(globalId, invType, ObjectType.DYNAMIC)
	updateItemBar()
	setTooltips()

	-- Make sure the dialog is visible
	MenuSetMinimizedThis(false)

	SendDynamicObjectChangeEvent() -- drf - tell DynamicObjectList to refresh

	return KEP.EPR_OK
end

-- Called to update the menu each time a dynamic object is removed from the world
function removeDynamicObjectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	updateItemBar()
	setTooltips()

	SendDynamicObjectChangeEvent() -- drf - tell DynamicObjectList to refresh
end

-- Called to retry placement of an item bar selection
function retryPasteHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if g_retryItemBarPaste then
		g_retryItemBarPaste = false
		g_requestingInventoryForBar = true
		KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
	end
end

---------------------------------------------------------------------
-- Thumbnail Control Click Event Handlers
---------------------------------------------------------------------

function imgItem1_OnLButtonUp(controlHandle, x, y)
	selectRecentlyPlacedItem(1)
end

function imgItem2_OnLButtonUp(controlHandle, x, y)
	selectRecentlyPlacedItem(2)
end

function imgItem3_OnLButtonUp(controlHandle, x, y)
	selectRecentlyPlacedItem(3)
end

function imgItem4_OnLButtonUp(controlHandle, x, y)
	selectRecentlyPlacedItem(4)
end

function imgItem5_OnLButtonUp(controlHandle, x, y)
	selectRecentlyPlacedItem(5)
end

-- Called to insert a recently placed item into the listing
function insertRecentlyPlacedItem(globalId, invType, objectType)
	if isGLIDInRecentlyPlacedBar(globalId) == false then
		-- Shift over all existing recently placed items
		for i = #g_recentlyPlacedItems + 1, 1, -1 do
			if i <= NUM_RECENTLY_PLACED_SLOTS then
				if g_recentlyPlacedItems[i - 1] ~= nil then
					g_recentlyPlacedItems[i] = g_recentlyPlacedItems[i - 1]
				end
			end
		end

		-- Add the new item at slot #1
		g_recentlyPlacedItems[1] = {}
		g_recentlyPlacedItems[1].globalId = globalId
		g_recentlyPlacedItems[1].invType = invType
		g_recentlyPlacedItems[1].objectType = objectType

		-- Loop through all non-nil items and update their visibility and enabled status
		local eh = 0

		for i, v in ipairs(g_recentlyPlacedItems) do
			if v == nil then
				break
			end

			-- Update item texture and visibility
			eh = Image_GetDisplayElement(gHandles["imgItem" .. i])
			if eh ~= 0 then
				KEP_LoadIconTextureByID(v.globalId,eh,gDialogHandle);
			end

			Control_SetVisible(gHandles["imgItem" .. i], true)
			Control_SetVisible(gHandles["imgVertRule" .. i], true)

			Control_SetEnabled(gHandles["imgItem" .. i], true)
			Control_SetEnabled(gHandles["imgVertRule" .. i], true)
		end
	end
end

-- Called to copy a recently placed item when its thumbnail is clicked
function selectRecentlyPlacedItem(itemIndex)
	if MenuIsClosed("Inventory.xml") then
		g_itemToPlace = g_recentlyPlacedItems[itemIndex]
		g_requestingInventoryForBar = true
		KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
	end
end

-- Called to update the item bar after item insertion or deletion
function updateItemBar()

	-- Check fill boundaries
	g_zoneObjectCount = (KEP_GetDynamicObjectCount())
	g_fillPercentage = g_zoneObjectCount / g_maxObjectCount
	if g_fillPercentage > 1 then
		g_fillPercentage = 1
	elseif g_fillPercentage < 0 then
		g_fillPercentage = 0
	end

	-- Set RGB values and blend colors
	local red = 0
	local green = 0
	if g_fillPercentage == 0 then
		red = 0
		green = 255
	elseif g_fillPercentage < .5 then
		red = math.min(math.floor(255 * ((g_fillPercentage*2)/1)), 255)
		green = 255
	elseif g_fillPercentage > .5 then
		local fill = g_fillPercentage - .5
		red = 255
		green = math.max(math.floor(255 - (255 * ((fill*2)/1)), 0))
	end

	BlendColor_SetColor(Element_GetTextureColor(Image_GetDisplayElement(gHandles["imgItemMeter"])), 0, {a = 255, r = red, g = green, b = 0})

	-- Update bar stretch and positioning
	local width = Control_GetWidth(gHandles["imgItemMeter"])
	local height = math.ceil(g_maxHeight * (g_fillPercentage/1))
	local x = Control_GetLocationX(gHandles["imgItemMeter"])
	local y = g_startingYPos - height

	Control_SetLocation(gHandles["imgItemMeter"], x, y)
	Control_SetSize(gHandles["imgItemMeter"], width, height)
end

-- Called to set the tooltips of each recently placed item thumbnail
function setTooltips()
	Control_SetToolTip(gHandles["imgItemMeter"], g_zoneObjectCount .. " of " .. g_maxObjectCount .. " Items Placed")
	Control_SetToolTip(gHandles["imgItemMeterBackground"], g_zoneObjectCount .. " of " .. g_maxObjectCount .. " Items Placed")
end

-- Xml decoder, uses string searches to locate start and end tags
function parseDataByTag(strEventData, strTag, strPattern, startPos)
	if strEventData ~= nil then
		local strStartTag = "<" .. strTag .. ">"
		local strEndTag = "</" .. strTag .. ">"
		local strPattern = strStartTag..strPattern..strEndTag
		local iStart, iEnd, strData = string.find(strEventData, strPattern, startPos)
		return iStart, iEnd, strData
	end
end

-- Gets info on the item limits for deeds from the web
function getDeedItemLimits()
	local zoneIndex = tostring(KEP_GetCurrentZoneIndex())
	local zoneIndexPlain = tostring(InstanceId_GetId(zoneIndex))
	local instanceId = tostring(KEP_GetZoneInstanceId())
	local zoneIndexCleared = tostring(InstanceId_GetClearedInstanceId(zoneIndex))
	local web_address = GameGlobals.WEB_SITE_PREFIX.."kgp/deedInfo.aspx?action=DeedInfo&zoneIndex="..zoneIndexCleared.."&instanceId=" .. instanceId .. "&zone_index_plain=" .. zoneIndexPlain
	makeWebCall( web_address, WF.DEED_INFO)
end

-- Called to check and see if an item has been placed in the bar
function isGLIDInRecentlyPlacedBar(globalId)
	local isInBar = false
	for i, v in ipairs(g_recentlyPlacedItems) do
		if tostring(v.globalId) == tostring(globalId) then
			isInBar = true
			break
		end
	end
	return isInBar
end

-- Called to load the last 5 recently placed items
function loadFromConfig()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		local userName = KEP_GetLoginName()
		local loadedItemsOk, loadedItems = KEP_ConfigGetString( config, "RecentlyPlacedItems" .. userName, "")

		-- If we have any loaded items, split them up into GlobalId, InvType pairs
		if string.len(loadedItems) > 0 then
			local itemString = split(loadedItems, ":")
			local splitItemString = {}

			for i = #itemString, 0, -1 do
				if itemString[i] ~= nil and string.len(itemString[i]) > 0 then
					splitItemString = split(itemString[i], "-")
					-- Make sure we're only inserting valid dynamic objects with both GlobalIds and InvTypes
					if (splitItemString[1] ~= nil and string.len(splitItemString[1]) > 0) and (splitItemString[2] ~= nil and string.len(splitItemString[1]) > 0) then
						insertRecentlyPlacedItem(splitItemString[1], splitItemString[2], ObjectType.DYNAMIC)
					end
				end
			end
		end
	end
end

-- Called to save the last 5 recently placed items
function saveConfig()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		local itemString = ""
		-- Build config string
		for i, v in ipairs(g_recentlyPlacedItems) do
			if v == nil then
				break
			end
			itemString = itemString .. v.globalId .. "-" .. v.invType .. ":"
		end

		-- Store config string
		local userName = KEP_GetLoginName()
        KEP_ConfigSetString( config, "RecentlyPlacedItems" .. userName, itemString )
		KEP_ConfigSave( config )
	end
end
