--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("MenuHelper.lua")
dofile("languageFilter.lua")
dofile("MenuLocation.lua")
dofile("..\\Scripts\\ChatType.lua")
dofile("..\\ScriptData\\CustomColors.lua")
dofile("..\\MenuScripts\\ScrollPane.lua")

dofile("DevMode.lua")

g_drfStyle = false -- groovyStyle - out of focus passthrough

g_currentZoneInstanceId = 0
g_currentZoneIndex = 0
g_population = 0

g_chatlines = {}
g_chattypes = {}

g_displaylines = {}
g_displaylinecount = 0

-- Chat history globals
g_chatlinecnt = 10
g_chatbuffersize = 60
g_chatscrollpos = 0

RESIZE_X_OFF = 19
RESIZE_Y_OFF = 38
g_x_resizeoffset = 0
g_y_resizeoffset = 0

SIZE_X_MIN = 280
SIZE_X_DEF = 280
SIZE_Y_MIN = 125
SIZE_Y_DEF = 200
g_sizeX = SIZE_X_DEF
g_sizeY = SIZE_Y_DEF

MAIN_FRAME_X_MIN = 70
MAIN_FRAME_Y_MIN = 23

g_sizing = false -- menu currently resizing
g_minimized = nil -- menu currently minimized
g_ignoreEnter = false

-- Menu Focus In/Out
g_focus = nil
g_losingFocus = false
g_losingFocusTimeSec = 0
g_sendBarFocus = nil

--globals for things we hide, show, and move
g_transLevelMin = 1
g_transLevelMax = 4
function TransLevel(l)
	if (l == g_transLevelMin) then return 10 * tonumber("01000000", 16) end
	return 64 * (l - 1) * tonumber("01000000", 16)
end
g_transLevelFocusIn = 4
g_transLevelFocusOut = 2

-- who we got last tell from
g_replyTo = ""

-- chat type constants
ctTalk = 0
ctGroup = 1
ctClan = 2
g_chat_type = ctTalk

--toggle for naughty word filter
g_useFilter = true

-- Hold previous chat entries
g_cmdBuffer = {}

-- Position in our command history
g_bufferPos = 0

--Entered the world
g_entered = false

--Entered the client
g_firstEntered = false

--Entered press while chat is not in focus
g_SendBarIgnoreKey = false

-- Chat Text Saved Through Focus
g_saveText = ""

g_whoList = {} -- list of users in current world

--Is GM?
g_isGM = false

--Playername
local g_playerName = ""

local GROUP_TAB = 1
local PRIVATE_TAB = 2

local g_privateChats = {}

--name of last player you were talking to
local g_privateWith = ""

--this will contain  a list of members with their real name and if they have unread messages
local g_currentPrivates = {}

--current tab
local g_currentTab = GROUP_TAB

--Needed for private flashing
local FLASH_DURATION_TIME	= .5 -- seconds
--TODO: needs to be removed for inline
local g_chatFlashing		= false
local g_chatFlashingTime	= 0
local g_chatFlashCounter	= 0
local PRIVATE_FLASH_COLORS = {{a = 165, r = 107, g = 35, b = 117}, {a = 165, r = 171, g= 17, b =211}, {a = 200, r = 255, g = 255, b = 255}}

--max characters
local g_characterMax = 150

--passthrough enabled
local g_passthrough = false

-- PM source menu
local g_pmSource = ""

local DROP_MIN_HEIGHT = 72
local g_maxDropHeight = 140

local PERSON_BUTTON_OFFSET = 6
local PERSON_BUTTON_DEF_SIZE = 160

--if people are showing
local g_peopleShowing = true

local g_showScroll = false

local g_newLineHeight = 0
local g_newMessage = false
local g_newMessageHidden = false

--mosue position
local g_mousePositionX = 0
local g_mousePositionY =0
local g_mouseDownInside = false
local g_mouseDownOutside = false

--URL info
IN_ZONE_SUFFIX = "kgp/peopleInGame.aspx?&action=getZone&zid="
IN_ZONE_CIZ_SUFFIX = "&czi="

g_filterWordList = {
	"asshole",
	"bitch",
	"chink",
	"clit",
	"cocksucker",
	"cock",
	"cunt",
	"dickhead",
	"dick",
	"douche",
	"faggot",
	"motherfucker",
	"fucker",
	"fuck",
	"shit",
	"tits",
	"whore"
}

-- ED-4027 - Escape HTML Special Characters (&amp; &lt; &gt;)
function HTML_Escape(msg)
	msg = string.gsub(msg, "&", "&amp;")
	msg = string.gsub(msg, ">", "&gt;")
	msg = string.gsub(msg, "<", "&lt;")
	msg = string.gsub(msg, ";%)", "&#263A;")
	msg = string.gsub(msg, ":%)", "&#263A;")
	msg = string.gsub(msg, ";%(", "&#2639;")
	msg = string.gsub(msg, ":%(", "&#2639;")
	msg = string.gsub(msg, ";x", "&#2620;")
	msg = string.gsub(msg, ":x", "&#2620;")
	return msg
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "HudEvent", KEP.MED_PRIO )
	KEP_EventRegister( "StartTypingEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ChatKeyPressedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PrefEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ReplyUpdateEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ReplyRequestEvent", KEP.MED_PRIO )
	KEP_EventRegister( "SendPrivateMessageEvent", KEP.MED_PRIO )
  	KEP_EventRegister( "RESET_AFK", KEP.HIGH_PRIO )
end

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "textHandler", "RenderTextEvent", KEP.MED_PRIO )	 
	KEP_EventRegisterHandler( "attribEventHandler", "AttribEvent", KEP.MED_PRIO )	
	KEP_EventRegisterHandler( "chatKeyPressedEventHandler", "ChatKeyPressedEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "hudEventHandler", "HudEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "typeStartHandler", "StartTypingEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "prefEventHandler", "PrefEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ReplyRequestHandler", "ReplyRequestEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "SendPrivateMessageHandler","SendPrivateMessageEvent", KEP.MED_PRIO)

	-- DRF - Added
	DevMode_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
end

------------------------
--DIALOG FUNCTIONS--
------------------------
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- ChatMenu Replaces DevConsole Outside DevMode
	--if MenuIsOpen("DevConsole.xml") and not ToBool(DevMode()) then
		MenuClose("DevConsole.xml")
	--end

	--Ban list handler
	Events.registerHandler("RETURN_CHAT_BAN_LIST", returnBanList)
	Events.registerHandler("CHAT_ADD_PERSON", addPersonHandler)
	Events.registerHandler("CHAT_REMOVE_PERSON", removePersonHandler)

	g_chat_type = ctTalk
	g_firstEntered = false
	g_chatscrollpos = 0
	-- for i = 1, g_chatbuffersize do
	-- 	g_chatlines[i] = " "
	-- 	g_chattypes[i] = 0
	-- end

	--Get player name
	g_playerName = KEP_GetLoginName()

	-- Get Place Info
	g_currentZoneInstanceId = KEP_GetZoneInstanceId()
    g_currentZoneIndex = KEP_GetZoneIndex()
    g_currentZoneIndex = InstanceId_GetClearedInstanceId(g_currentZoneIndex)
 	makeWebCall(GameGlobals.WEB_SITE_PREFIX..GET_PLACE_INFORMATION_SUFFIX, WF.PLACE_INFO)

 	--Get user info
 	makeWebCall( GameGlobals.WEB_SITE_PREFIX.."kgp/userProfile.aspx?action=getOwnershipSummary", WF.GET_USER, 0, 8)

	-- Get Menu Controls
	g_cBtnMinimize = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnMinimize"))
	g_lblChat = Dialog_GetStatic(gDialogHandle, "lblChat")
	g_cLblChat = Static_GetControl(g_lblChat)
	g_imgMainFrame = Dialog_GetImage(gDialogHandle, "imgMainFrame")
	g_cImgMainFrame = Image_GetControl(g_imgMainFrame)
	g_imgBackDrop = Dialog_GetImage(gDialogHandle, "imgBackDrop") 
	g_cImgBackDrop = Image_GetControl(g_imgBackDrop)
	g_edtSendBar = Dialog_GetEditBox(gDialogHandle, "edtSendBar")
	g_cEdtSendBar = EditBox_GetControl(g_edtSendBar)
	g_cImgResize = Image_GetControl(Dialog_GetImage(gDialogHandle, "imgResize"))
	g_cBtnIncTrans = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnIncTrans"))
	g_cBtnDecTrans = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnDecTrans"))
	g_cTrimH1 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimH1"))
	g_cTrimH2 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimH2"))
	g_cTrimH3 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimH3"))
	g_cTrimH4 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimH4"))
	g_cTrimH5 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimH5"))
	g_cTrimH6 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimH6"))
	g_cTrimV1 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimV1"))
	g_cTrimV2 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimV2"))
	g_cTrimV3 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimV3"))
	g_cTrimV4 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimV4"))
	g_cTrimV5 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimV5"))
	g_cTrimV6 = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimV6"))
	g_cPrivateTrim = Image_GetControl(Dialog_GetImage(gDialogHandle, "trimPrivateH"))
	g_lstChatHist = Dialog_GetListBox(gDialogHandle, "lstChatHist")
	g_cLstChatHist = ListBox_GetControl(g_lstChatHist)
	g_lstChatHistSB = ListBox_GetScrollBar(g_lstChatHist)
	g_cLstChatHistSB = ScrollBar_GetControl(g_lstChatHistSB)
	g_clblTitle = Dialog_GetStatic(gDialogHandle, "lblTitleBarText")
	--tab
	g_cBtnGroup = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnGroup"))
	g_cBtnPrivate = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnPrivate"))
	g_cImgSelectedTab = Dialog_GetImage(gDialogHandle, "imgSelectedTab")
	g_cImgHighlight = Dialog_GetImage(gDialogHandle, "imgHighlight")
	g_clblWorldChat = Dialog_GetStatic(gDialogHandle, "lblWorldChat")
	g_clblPrivateChat = Dialog_GetStatic(gDialogHandle, "lblPrivateChat")
	--private chat
	g_cBtnEnd = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnEndChat"))
	g_cimgEndUnderline = Dialog_GetImage(gDialogHandle, "imgEndUnderline")
	g_cCmbDropDown = Dialog_GetComboBox(gDialogHandle, "cmbDropDown")
	g_cImgPrivateHighlight = Dialog_GetImage(gDialogHandle, "imgPrivateHighlight")
	g_clblPrivateWith = Dialog_GetStatic(gDialogHandle, "lblPrivateWith")
	g_clblNewPrivate = Dialog_GetStatic(gDialogHandle, "lblNewMessage")
	g_cImgNewPrivate = Dialog_GetImage(gDialogHandle, "imgNewMessage")
	--people list
	g_imgPeopleFrame = Dialog_GetImage(gDialogHandle, "imgPeopleFrame")
	g_imgInWorldFrame = Dialog_GetImage(gDialogHandle, "imgInWorldFrame")
	g_cimgPeopleFrame = Image_GetControl(g_imgPeopleFrame)
	g_cimgInWorldFrame = Image_GetControl(g_imgInWorldFrame)
	g_cBtnPerson = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnPerson"))
	g_cBtnExpand = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnExpandPeople"))
	g_cBtnCollapse = Button_GetControl(Dialog_GetButton(gDialogHandle, "btnCollapsePeople"))
	g_clblPeople = Dialog_GetStatic(gDialogHandle, "lblPeople")
	g_lstScrollbar = Dialog_GetListBox(gDialogHandle, "lbScrollbar")
	g_clstScrollbar = ListBox_GetControl(g_lstScrollbar)
	g_lstScrollbarSB = ListBox_GetScrollBar(g_lstScrollbar)
	g_clstScrollbarSB = ScrollBar_GetControl(g_lstScrollbarSB)


	g_peopleScrollPane = ScrollPane.new("ChatPeopleList", gDialogHandle, "imgPeopleFrame", "lbScrollbar", {})

	--Constant sizes
	g_worldFrameHeight = Control_GetHeight(g_cimgInWorldFrame)

	-- Used Only To Size Chat Text
	Static_SetText(g_lblChat, "")
	Control_SetVisible(g_cLblChat, true)

	ListBox_SetHTMLEnabled(g_lstChatHist, true)

	local eh = ScrollBar_GetTrackDisplayElement(g_lstChatHistSB)
	Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
	Element_SetCoords(eh, 185, 55, 186, 56)

	eh = ScrollBar_GetButtonDisplayElement(g_lstChatHistSB)
	Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
	Element_SetCoords(eh, 395, 572, 409, 589)

	eh = ScrollBar_GetUpArrowDisplayElement(g_lstChatHistSB)
	Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
	Element_SetCoords(eh, 395, 547, 409, 561)

	eh = ScrollBar_GetDownArrowDisplayElement(g_lstChatHistSB)
	Element_AddTexture(eh, gDialogHandle, "wok_hud_new.tga")
	Element_SetCoords(eh, 395, 642, 409, 656)

	
	-- Load Chat Config
	LoadChatConfig()

	-- Start Maximized & Out Of Focus
	Maximize(false)
	LoseFocus()

	setBold(g_clblWorldChat, true)

	local web_address = GameGlobals.WEB_SITE_PREFIX..IN_ZONE_SUFFIX..g_currentZoneInstanceId..IN_ZONE_CIZ_SUFFIX..g_currentZoneIndex
	makeWebCall( web_address, WF.FRIENDS, 1, 2000)

	ScrollBar_SetTrackPos( g_lstChatHistSB, 100 )

	g_pmSource = MenuNameThis()
end

function Dialog_OnDestroy(dialogHandle)

	-- Save Chat Config
	SaveChatConfig()

	Helper_Dialog_OnDestroy( dialogHandle )
end
function Dialog_OnRender(dialogHandle, timeSec)
	
	-- Ignore Arrow Keys Unless Send Bar In Focus
	local sendBarFocus = ToBool(Control_GetFocus(g_cEdtSendBar))
	if (g_sendBarFocus ~= sendBarFocus) then
		g_sendBarFocus = sendBarFocus
		--Dialog_SetIgnoreArrowKeys(gDialogHandle, not g_sendBarFocus)
	end

	-- Losing Focus Activates After 1 Second
	if g_losingFocus == true then
		g_losingFocusTimeSec = g_losingFocusTimeSec + timeSec
		if (g_losingFocusTimeSec > 1.0) then
			LoseFocus()
		end
	end

	if g_chatFlashing == true then
		g_chatFlashingTime = g_chatFlashingTime + timeSec
		if (g_chatFlashingTime > FLASH_DURATION_TIME) then
			local flashColor = nil
			if g_currentTab ~= PRIVATE_TAB and g_minimized == false then
				flashColor = Element_GetTextureColor(Image_GetDisplayElement(g_cImgPrivateHighlight))
			elseif g_minimized == true then
				flashColor = Element_GetFontColor(Static_GetDisplayElement(g_clblTitle))
				g_newMessageHidden = true
			elseif g_currentTab == PRIVATE_TAB then
				setMessageBubbleVisible(true)
			end
			g_chatFlashCounter = g_chatFlashCounter + 1
			local colorNumber  = g_chatFlashCounter % 2 + 1
			if g_minimized == true  and colorNumber == 1 then
				colorNumber= 3
			end
			local newColor = PRIVATE_FLASH_COLORS[colorNumber]

			--sanity check
			if flashColor then
				BlendColor_SetColor( flashColor, 0, newColor )
			end
			g_chatFlashingTime = 0
		end

		if g_chatFlashCounter > 12 then
			g_chatFlashing = false
			g_chatFlashCounter = 0
			setMessageBubbleVisible(false)
		end
	end

    if MenuGetMinimizedThis() then return end

	if (ToBool(Dialog_IsLMouseDown(gDialogHandle)) or ToBool(Dialog_IsRMouseDown(gDialogHandle)))  then
		if Control_ContainsPoint(g_cImgMainFrame, g_mousePositionX, g_mousePositionY) ~= 0 and Control_ContainsPoint(g_cBtnIncTrans, g_mousePositionX, g_mousePositionY) == 0 and Control_ContainsPoint(g_cImgResize, g_mousePositionX, g_mousePositionY) == 0 then
			if g_mouseDownOutside == false then
				GainFocus()
				g_mouseDownOutside = false
				g_mouseDownInside = true
			end
		elseif Control_ContainsPoint(g_cImgResize, g_mousePositionX, g_mousePositionY) ~= 0 then
			g_x_resizeoffset = g_mousePositionX - Control_GetLocationX(g_cImgResize)
			g_y_resizeoffset = g_mousePositionY - Control_GetLocationY(g_cImgResize)
			g_sizing = true
			SetPeopleListEnabledandVisible(false, false)
			Control_SetVisible(g_clstScrollbar, false)
		elseif (g_drfStyle == false) and (g_sizing == false) then
			if g_mouseDownInside == false then
				LoseFocus()
				g_mouseDownOutside = true
				g_mouseDownInside = false
			end
		end
	else
		g_mouseDownOutside = false
		g_mouseDownInside = false
		if g_sizing == true then
			g_sizing = false
			UpdatePeopleList()

		end
	end
end

function Dialog_OnMoved()
	LayoutAll()
	UpdatePeopleList()
end

-- Mouse moved hack to catch mouse down events.
function Dialog_OnMouseMove( dialogHandle, x, y )
	local pos = { x = x, y = y }

	g_mousePositionX = x
	g_mousePositionY =  y
	-- DRF Style - Menu Passthrough
	if g_drfStyle == true then

		-- Non-Passthrough Button Clicks
		if g_focus == false then
			local aMin = ToBool(Control_GetVisible(g_cBtnMinimize)) and ToBool(Control_ContainsPoint(g_cBtnMinimize, pos.x, pos.y))
			local aDec = ToBool(Control_GetVisible(g_cBtnDecTrans)) and ToBool(Control_ContainsPoint(g_cBtnDecTrans, pos.x, pos.y))
			local aInc = ToBool(Control_GetVisible(g_cBtnIncTrans)) and ToBool(Control_ContainsPoint(g_cBtnIncTrans, pos.x, pos.y))
			local passthrough = not aMin and not aDec and not aInc
			MenuSetPassthroughThis(passthrough)
		end

		-- Lose Focus Outside Menu
		if g_sizing == false and g_focus == true and not MenuContainsPointRelThis(pos.x , pos.y + 20) then
			g_losingFocus = true
		else
			LosingFocusReset()
		end
	end

	-- Menu Resize
	if g_sizing == true then
		
		if ToBool(Dialog_IsLMouseDown(gDialogHandle)) then
			g_sizeX = x - g_x_resizeoffset - RESIZE_X_OFF
			g_sizeY = y - g_y_resizeoffset - RESIZE_Y_OFF
		end
		g_chatscrollpos = 0

		LayoutAll()
	elseif (ToBool(Dialog_IsLMouseDown(gDialogHandle)) or ToBool(Dialog_IsRMouseDown(gDialogHandle))) and g_passthrough == false then
		MenuSetPassthroughThis(true)
		g_passthrough = true
	elseif not (ToBool(Dialog_IsLMouseDown(gDialogHandle)) or ToBool(Dialog_IsRMouseDown(gDialogHandle))) and g_passthrough == true then
		g_passthrough = false
		MenuSetPassthroughThis(false)
	end
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown, bCtrlDown, bAltDown)

	if key == Keyboard.ESC then
		LoseFocus()
	elseif key == Keyboard.PAGE_UP then
		if g_cmdBuffer[g_bufferPos + 1] ~= nil then
			g_bufferPos = g_bufferPos + 1
			EditBox_SetText(g_edtSendBar, g_cmdBuffer[g_bufferPos], false)
		end	
	elseif key == Keyboard.PAGE_DOWN then
		EditBox_SetText(g_edtSendBar, "", false)	
		if (g_bufferPos - 1) < 1 then
			g_bufferPos = 0		
		elseif g_cmdBuffer[g_bufferPos - 1] ~= nil then
			g_bufferPos = g_bufferPos - 1
			EditBox_SetText(g_edtSendBar, g_cmdBuffer[g_bufferPos], false)
		end
	elseif key == Keyboard.ENTER then
		g_SendBarIgnoreKey = true
		if g_focus == true then
			FocusControl()
		else
			Maximize(true)
		end
	elseif key == 192 then -- '~'
		ToggleMinimize()
	elseif g_focus == true then
		FocusControl()
	end

	-- DRF - Added
	DevMode_OnKeyDown(gDialogHandle, key, bShiftDown, bCtrlDown, bAltDown)
end

------------------------
--LOADING AND SAVING FUNCTIONS--
------------------------
function LoadChatConfig()
	
	-- Load Menu Location
	loadLocation(gDialogHandle, "Chat")

	-- Load Chat Config From WOKConfig.xml
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		ok, g_sizeX = KEP_ConfigGetNumber( config, "ChatHSize", g_sizeX )
		--ok, g_sizeY = KEP_ConfigGetNumber( config, "ChatVSize", g_sizeY )
		--ok, g_transLevelFocusIn = KEP_ConfigGetNumber(config, "ChatTLevelFocusIn", g_transLevelFocusIn)
       	ok, g_transLevelFocusOut = KEP_ConfigGetNumber(config, "ChatTLevelFocusOut", g_transLevelFocusOut)
		ok, g_useFilter = KEP_ConfigGetString( config, "ChatUseFilter", tostring(g_useFilter))
		g_useFilter = ToBool(g_useFilter)
		ok, g_drfStyle = KEP_ConfigGetString( config, "ChatDrfStyle", tostring(g_drfStyle))
		g_drfStyle = ToBool(g_drfStyle)
		ok, history = KEP_ConfigGetString(config, "ChatHistory" , "No save chat history.")
		ok, g_replyTo = KEP_ConfigGetString(config, "ChatReply" , g_replyTo)
		if g_sizeX < SIZE_X_MIN then g_sizeX = SIZE_X_MIN end
		if g_sizeY < SIZE_Y_MIN then g_sizeY = SIZE_Y_MIN end
		LoadChatConfigUnPackChatLines( history )
	end
end

function SaveChatConfig()

	-- Save Menu Location
	saveLocation(gDialogHandle, "Chat")	

	-- Save Chat Config To WOKConfig.xml
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetNumber( config, "ChatHSize", g_sizeX )
		KEP_ConfigSetNumber( config, "ChatVSize", g_sizeY )
--		KEP_ConfigSetNumber( config, "ChatTLevelFocusIn", g_transLevelFocusIn )
		KEP_ConfigSetNumber( config, "ChatTLevelFocusOut", g_transLevelFocusOut )
		KEP_ConfigSetString( config, "ChatUseFilter", tostring(g_useFilter) )
		KEP_ConfigSetString( config, "ChatDrfStyle", tostring(g_drfStyle) )
		KEP_ConfigSetString( config, "ChatHistory", SaveChatConfigPackChatLines())
		if g_replyTo ~= nil then
			KEP_ConfigSetString( config, "ChatReply", g_replyTo)
		end
		KEP_ConfigSave( config )
	end
end

-- Packs chat lines for SaveChatConfig()
function SaveChatConfigPackChatLines()
	local chatString = ""
	local typeString = ""
	for i = 1, g_chatbuffersize do
		if g_chatlines[ i ] ~= "" and g_chattypes[i] ~= nil then
		    chatString = chatString..g_chatlines[i].."<kbr "..g_chattypes[i]..">"
		end	
	end

	for i, v in pairs(g_privateChats) do
		for user, message in pairs(v) do
			chatString = chatString.."<pvt><user>"..g_currentPrivates[i].realName.."</user><message>"..message.."</message></pvt>"
		end
	end
	return chatString
end

-- Unpacks chat lines for LoadChatConfig()
function LoadChatConfigUnPackChatLines( strHist )
	
	if strHist == "" then
		local e = KEP_EventCreate( "RenderTextEvent" )
		KEP_EventEncodeString( e, "Welcome to Kaneva!" )
		KEP_EventEncodeNumber( e, ChatType.Group)		
		KEP_EventQueue( e )
		e = KEP_EventCreate( "RenderTextEvent" )
		KEP_EventEncodeString( e, "Type /help to see commands" )
		KEP_EventEncodeNumber( e, ChatType.System  )		
		KEP_EventQueue(e)
		g_firstEntered = true
	end
	local lastPos = 1 -- previous position of tag found
	local start = 1 -- beginning and end of found string
	local stop = 1
	local chatLine = ""
	local tag = ""
	local type = ""
	for privateMessages in string.gmatch(strHist, "<pvt>(.-)</pvt>") do
		privateUser = string.match(privateMessages, "<user>(.-)</user>")
		message = string.match(privateMessages, "<message>(.-)</message>")
		if privateUser and message then
			addPrivateMessage(message, privateUser, false)
		end
	end
	for i = 1, g_chatbuffersize do
		lastPos = stop  -- remember where we were in string
		
		--get location of tag that delimits new line of text and chat type
	    start, stop, type = string.find(strHist, "<kbr (%d+)>", lastPos)

		--check to make sure we found the next tag in the string
		if start == nil or stop == nil or lastPos == nil then
			return 0
  		else
  		    g_chattypes[i] = tonumber(type)
			--take text from between end of previous tag and just before start of next tag
        	chatLine = string.sub(strHist, lastPos, start-1)
        	g_chatlines[i] = chatLine
    		stop = stop + 1
		end
	end
	--displayPrivateElements()
end


------------------------
--CONTROL HANDLES--
------------------------
function edtSendBar_OnEditBoxKeyDown(dialogHandle, key, bShiftDown, bControlDown, bAltDown)

	local msg = EditBox_GetText(g_edtSendBar)
	if g_currentTab == GROUP_TAB then
		g_characterMax = 150
	else
		g_characterMax = 1000
	end

	if string.len(msg) > g_characterMax then
		msg = string.sub(msg, 0, g_characterMax)
        EditBox_SetText(g_edtSendBar, msg, false) 
	end

	if key == Keyboard.PAGE_UP then
		if g_cmdBuffer[g_bufferPos + 1] ~= nil then
			g_bufferPos = g_bufferPos + 1
			EditBox_SetText(g_edtSendBar, g_cmdBuffer[g_bufferPos], false)
		end
	elseif key == Keyboard.PAGE_DOWN then
		EditBox_SetText(g_edtSendBar, "", false)
		if (g_bufferPos - 1) < 1 then
			g_bufferPos = 0
		elseif g_cmdBuffer[g_bufferPos - 1] ~= nil then
			g_bufferPos = g_bufferPos - 1
			EditBox_SetText(g_edtSendBar, g_cmdBuffer[g_bufferPos], false)
		end
	elseif key == Keyboard.SPACEBAR then
		if msg == nil then return end
		local cmd, cmdS, cmdE = GetSendBarCommand()
		if cmd == nil then return end
		if cmd == "h" then
			EditBox_SetText(g_edtSendBar, "/help ", false)
		elseif cmd == "t" then
			EditBox_SetText(g_edtSendBar, "/tell ", false)
		elseif cmd == "w" then
			EditBox_SetText(g_edtSendBar, "/who ", false)
		elseif cmd == "k" then
			EditBox_SetText(g_edtSendBar, "/kick ", false)
		elseif cmd == "f" then
			EditBox_SetText(g_edtSendBar, "/filter ", false)
		elseif cmd == "reply" or cmd == "r" then
	        msg = "/tell "
			if g_replyTo ~= nil then 
				msg = msg..g_replyTo.." "
			end
			EditBox_SetText(g_edtSendBar, msg, false)
		end
	elseif key == Keyboard.TAB then
		local cmd, cmdS, cmdE = GetSendBarCommand()
		if cmd == nil then return end
		if cmd == "h" then
			EditBox_SetText(g_edtSendBar, "/help ", false)
		elseif cmd == "t" then
			EditBox_SetText(g_edtSendBar, "/tell ", false)
		elseif cmd == "w" then
			EditBox_SetText(g_edtSendBar, "/who ", false)
		elseif cmd == "k" then
			EditBox_SetText(g_edtSendBar, "/kick ", false)
		elseif cmd == "f" then
			EditBox_SetText(g_edtSendBar, "/filter ", false)
		elseif cmd == "reply" or cmd == "r" then
	        msg = "/tell "
			if g_replyTo ~= nil then 
				msg = msg..g_replyTo.." "
			end
			EditBox_SetText(g_edtSendBar, msg, false)
		end
		return KEP.EPR_CONSUMED
	elseif key == Keyboard.ESC then
		EditBox_ClearText( g_edtSendBar )
	end
end

-- 'ENTER' key was pressed within the bottom bar
function edtSendBar_OnEditBoxString(editBoxHandle, password)

	KEP_EventCreateAndQueue( "RESET_AFK" )

	-- Ignore Keys Used To Gain Focus
	if g_SendBarIgnoreKey == true then
		g_SendBarIgnoreKey = false
		FocusControl()
		return
	end

	-- Process Command
	g_bufferPos = 0
	local msg = EditBox_GetText(editBoxHandle)
	--move scroll bar to bottom	
	ScrollBar_SetTrackPos( g_lstChatHistSB, 100 )
	

	local cmd = GetSendBarCommand()
	if cmd == "help" or cmd == "h" then
		e = KEP_EventCreate("RenderTextEvent" )
				KEP_EventEncodeString( e, "Help Commands: " )
				KEP_EventEncodeNumber( e, ChatType.System  )		
				KEP_EventQueue( e )
		e = KEP_EventCreate( "RenderTextEvent" )
				KEP_EventEncodeString( e, "   /tell or /t [name] - send a private chat" )
				KEP_EventEncodeNumber( e, ChatType.System  )		
				KEP_EventQueue( e )
		e = KEP_EventCreate( "RenderTextEvent" )
				KEP_EventEncodeString( e, "   /kick or /k [name] - remove player from your world (Owner only)" )
				KEP_EventEncodeNumber( e, ChatType.System  )		
				KEP_EventQueue( e )
		e = KEP_EventCreate( "RenderTextEvent" )
				KEP_EventEncodeString( e, "   /clear or /c - clear chat history" )
				KEP_EventEncodeNumber( e, ChatType.System  )		
				KEP_EventQueue( e )
		e = KEP_EventCreate( "RenderTextEvent" )
				KEP_EventEncodeString( e, "Click a person's name below to see their info." )
				KEP_EventEncodeNumber( e, ChatType.System  )		
				KEP_EventQueue( e )
		EditBox_ClearText( g_edtSendBar )
		return
	elseif cmd == "filter" or cmd == "f" then
		g_useFilter = not g_useFilter
		e = KEP_EventCreate( "RenderTextEvent" )
				local str = "Off"
				if g_useFilter then str = "On" end
				KEP_EventEncodeString( e, "Language Filter "..str )
				KEP_EventEncodeNumber( e, ChatType.System  )		
				KEP_EventQueue( e )
		EditBox_ClearText( g_edtSendBar )
		return
	elseif cmd == "who" or cmd == "w" then
		g_peopleShowing = true
		ShowPeopleList(true)
		EditBox_ClearText( g_edtSendBar )
		return
	elseif cmd == "mute" then
		if g_isGM then
			local bannedUser = string.sub(string.lower(msg), 7, string.len(msg))
			Events.sendEvent("UPDATE_CHAT_BAN_LIST", {bannedUser = bannedUser, blocker = g_playerName})
		end
		EditBox_ClearText( g_edtSendBar )
		return
	elseif cmd == "unmute" then
		if g_isGM then
			local unbannedUser = string.sub(string.lower(msg), 9, string.len(msg))
			Events.sendEvent("UPDATE_CHAT_BAN_LIST", {unbannedUser = unbannedUser, blocker = g_playerName})
		end
		EditBox_ClearText( g_edtSendBar )
		return
	elseif cmd == "groovystyle" then
		g_drfStyle = not g_drfStyle
		e = KEP_EventCreate( "RenderTextEvent" )
				local str = "Off"
				if g_drfStyle then str = "On" end
				KEP_EventEncodeString( e, "Groovy Style "..str )
				KEP_EventEncodeNumber( e, ChatType.System  )		
				KEP_EventQueue( e )
		EditBox_ClearText( g_edtSendBar )
		return
	end

	-- Escape Html Special Characters
	msg = HTML_Escape(msg)

	-- Clear Chat History ?
	if msg ~= "" then
		
		table.insert(g_cmdBuffer, 1, msg)

		-- see if the user wants to clear chat history, by looking for /c or /clear
		local clearKey = "/clear"
		local clearKey2 = "/c"
		if (((string.find(string.lower(msg), clearKey) ~= nil) and (string.len(msg) == 6)) or ((string.find(string.lower(msg), clearKey2) ~= nil)and (string.len(msg) == 2))) then 
			--filters out any other text or letters so the /c and /clear can be clearly read
			-- Loop through all of the messages in our chat history
			g_chatlines = {}
			g_chattypes = {}
			local e = KEP_EventCreate( "RenderTextEvent" )
			KEP_EventEncodeString( e, "Chat cleared" )
			KEP_EventEncodeNumber( e, ChatType.System  )		
			KEP_EventQueue(e)
			LayoutAll()

			EditBox_ClearText( g_edtSendBar )
			msg = ""
			return
		end
	end

	-- send chat message
	if msg ~= "" then
		
		table.insert(g_cmdBuffer, 1, msg)
		
		-- see if replying look for /r or /reply
		local replyLen = 0
		local replyStart = nil         
		replyStart, replyLen = string.find( msg, "/reply ." )
		if replyStart == nil then 
			replyStart, replyLen = string.find( msg, "/r ." )
		end 
		
		if replyLen ~= nil and replyStart == 1 then
			if g_replyTo ~= nil then 

				msg = "/tell "..g_replyTo..string.sub( msg, replyLen - 1 )
				g_chat_type = ctPrivate
			else
				e = KEP_EventCreate( "RenderTextEvent" )
				KEP_EventEncodeString( e, "No one to reply to." )
				KEP_EventEncodeNumber( e, ctSystem  )		
				KEP_EventQueue( e )
				return 
			end 
		end

		if string.sub(string.lower(msg), 0, 1) == "/" then
			if string.sub(string.lower(msg), 0, 6) == "/tell "  then
				SendPrivateMessageEvent()
			end
			KEP_SendChatMessage(g_chat_type, msg )
		else
			if g_currentTab == GROUP_TAB then
				KEP_SendChatMessage(g_chat_type, "/shout "..msg )
				KEP_EventCreateAndQueue("Tray_PublicChatEvent")
			else
				KEP_SendChatMessage(ChatType.private, "/tell "..g_privateWith.." "..msg)
				SendPrivateMessageEvent()
			end
		end
	end
	
	if msg == "" then
		EditBox_ClearText( g_edtSendBar )
		g_ignoreEnter = true
		LoseFocus()
	end

	EditBox_ClearText( editBoxHandle ) 
end

function btnIncTrans_OnButtonClicked( buttonHandle )
   	if g_focus == false then
		if g_transLevelFocusOut < g_transLevelMax then
   			g_transLevelFocusOut = g_transLevelFocusOut + 1
   		else
   			g_transLevelFocusOut = g_transLevelMin
   		end
	end
	SetTransparency()
end

function btnGroup_OnButtonClicked(buttonHandle)
	if g_currentTab ~= GROUP_TAB then
		g_currentTab = GROUP_TAB
		updateTabDisplay(g_cBtnGroup)
		LayoutAll()
	end
end

function btnPrivate_OnButtonClicked(buttonHandle)
	if g_currentTab ~= PRIVATE_TAB then
		OpenPrivateTab()
	end
end

function btnEndChat_OnButtonClicked(buttonHandle)
	EndChat(g_privateWith)
end

function btnEndChat_OnMouseEnter(buttonHandle)
	local underline = Element_GetTextureColor(Image_GetDisplayElement(g_cimgEndUnderline))
	local textStatic = Button_GetStatic(g_cBtnEnd)
	Static_SetText(textStatic, "<font color=C8FFFFFF>end chat</font>")
  	BlendColor_SetColor( underline, 0, {a = 200, r = 255, g = 255, b = 255} )
end

function btnEndChat_OnMouseLeave(buttonHandle)
	local underline = Element_GetTextureColor(Image_GetDisplayElement(g_cimgEndUnderline))
	local textStatic = Button_GetStatic(g_cBtnEnd)
	Static_SetText(textStatic, "<font color=30FFFFFF>end chat</font>")
  	BlendColor_SetColor( underline, 0, {a = 150, r = 255, g = 255, b = 255} )
end


function btnCollapsePeople_OnButtonClicked(buttonHandle)
	g_peopleShowing = false
	ShowPeopleList(false)
end


function btnExpandPeople_OnButtonClicked(buttonHandle)
	g_peopleShowing = true
	ShowPeopleList(true)
end

function btnGroup_OnMouseEnter(buttonHandle)
	if g_currentTab ~= GROUP_TAB and  g_minimized == false and g_focus == true then
		Control_SetVisible(g_cImgHighlight, true)
		Control_SetLocation(g_cImgHighlight, Control_GetLocationX(g_cBtnGroup), Control_GetLocationY(g_cBtnGroup))
		Control_SetSize(g_cImgHighlight, Control_GetWidth(g_cBtnGroup), Control_GetHeight(g_cBtnGroup))
	end
end

function btnGroup_OnMouseLeave(buttonHandle)
	if g_currentTab ~= GROUP_TAB and g_minimized == false then
		Control_SetVisible(g_cImgHighlight, false)
	end
end

function btnPrivate_OnMouseEnter(buttonHandle)
	if g_currentTab ~= PRIVATE_TAB and  g_minimized == false and g_focus == true then
		Control_SetVisible(g_cImgHighlight, true)
		Control_SetLocation(g_cImgHighlight, Control_GetLocationX(g_cImgPrivateHighlight), Control_GetLocationY(g_cImgPrivateHighlight))
		Control_SetSize(g_cImgHighlight, Control_GetWidth(g_cBtnPrivate), Control_GetHeight(g_cBtnPrivate))
	end
end

function btnPrivate_OnMouseLeave(buttonHandle)
	if g_currentTab ~= PRIVATE_TAB and  g_minimized == false then
		Control_SetVisible(g_cImgHighlight, false)
	end
end

function btnMinimize_OnButtonClicked()
	ToggleMinimize()
end

function cmbDropDown_OnComboBoxSelectionChanged(comboBoxHandle, selectedIndex, selectedText)

	setMessageBubbleVisible(false)
	local userStrS, userStrE = string.find(selectedText, '*')
	local player = nil
	if userStrE then
		player = string.sub(selectedText, 22, userStrE-1)
		g_currentPrivates[string.lower(player)].newMessage = false
		g_privateWith = string.lower(player)
		updateDropDown(player, false)
	end
	if player == nil then
		player = selectedText
	end
	g_privateWith = string.lower(player)
	Static_SetText(g_clblPrivateWith, "Private chat with "..player)
	FlowChatLines()
	Control_SetFocus(g_edtSendBar, true)
end

--function btnDecTrans_OnButtonClicked( buttonHandle )
--	if g_focus == true then
--		if g_transLevelFocusIn > g_transLevelMin then
--			g_transLevelFocusIn = g_transLevelFocusIn - 1
--		end
--	else
--		if g_transLevelFocusOut > g_transLevelMin then
--			g_transLevelFocusOut = g_transLevelFocusOut - 1
--		end
--	end
--	SetTransparency()
--end

function edtSendBar_OnFocusIn(editBoxHandle)
	Maximize(false)
	EnterToChatClear()
end

function imgResize_OnLButtonDown(dialogHandle, x, y)
	g_x_resizeoffset = x - Control_GetLocationX(g_cImgResize)
	g_y_resizeoffset = y - Control_GetLocationY(g_cImgResize)
	g_sizing = true
	SetPeopleListEnabledandVisible(false, false)
	Control_SetVisible(g_clstScrollbar, false)
end

function GetSendBarCommand()
	local msg = EditBox_GetText(g_edtSendBar)
	if msg == nil then return end
	local cmd = ""
	local cmdS, cmdE
	cmdS, cmdE, cmd = string.find( msg, "/([^%s]+)", 1)
	if cmdS ~= 1 then return end
	if cmd == nil then return end
	return string.lower(cmd), cmdS, cmdE
end

function GetPMTarget()
	local target = nil
	local msg = EditBox_GetText(g_edtSendBar)
	if msg ~= nil then
		target = string.match(msg, '^/tell ([^%s]+)')
	end
	return target or g_privateWith
end

------------------------
--LAYOUT FUNCTIONS--
------------------------
function SetTransparency()
	if g_focus == true then
		level = TransLevel(g_transLevelFocusIn)
	else
		level = TransLevel(g_transLevelFocusOut)
	end
	local mytemptex = Element_GetTextureColor(Image_GetDisplayElement(g_imgMainFrame))
	local myPeopleImage = Element_GetTextureColor(Image_GetDisplayElement(g_imgPeopleFrame))
	local myInWorldImage = Element_GetTextureColor(Image_GetDisplayElement(g_imgInWorldFrame))
	local myTextEntryImage = Element_GetTextureColor(Image_GetDisplayElement(g_imgBackDrop))
   	local myTextHistoryImage = Element_GetTextureColor(EditBox_GetTextAreaDisplayElement(g_edtSendBar))
   	BlendColor_SetColorRaw( mytemptex, 0, level )
   	BlendColor_SetColorRaw( myPeopleImage, 0, level )
   	BlendColor_SetColorRaw( myInWorldImage, 0, level )
   	BlendColor_SetColorRaw( myTextEntryImage, 0, level )
   	BlendColor_SetColorRaw( myTextHistoryImage, 0, level )
	FocusControl()
end

function LosingFocusReset()
	g_losingFocus = false
	g_losingFocusTimeSec = 0
end

function FocusControl()
	if g_focus == true then
		Control_SetFocus(g_edtSendBar, true)
	else
		MenuClearFocusThis()
	end
end

function GainFocus()
	LosingFocusReset()
	if g_focus == true then return end
	g_focus = true
	LayoutGainFocus()
	if g_drfStyle == true then MenuSetPassthroughThis(false) end
	MenuSetCaptureKeysThis(true)
	EnterToChatClear()
	SetTransparency()
	FocusControl()
	Control_SetVisible( g_cBtnIncTrans, false )
end

function LoseFocus()
	LosingFocusReset()
	if g_focus == false then return end
	g_focus = false
	LayoutLoseFocus()
	if g_drfStyle == true then MenuSetPassthroughThis(true) end
	MenuSetCaptureKeysThis(false)
	EnterToChatSet()
	SetTransparency()
	FocusControl()
end

function LayoutGainFocus()
	if g_minimized == true then
		LayoutMinimize()
	else
		LayoutMaximize()
	end
end

function LayoutLoseFocus()
	if g_minimized == true then
	else
		Control_SetVisible( g_cLblChat, false )
		Control_SetVisible( g_cImgMainFrame, true )
		Control_SetVisible( g_cImgBackDrop, false )  
		Control_SetVisible( g_cEdtSendBar, true )
		Control_SetVisible( g_cImgResize, false )
		Control_SetVisible( g_cBtnIncTrans, true )
--		Control_SetVisible( g_cBtnDecTrans, true )
		Control_SetVisible( g_cBtnMinimize, true )
		Control_SetVisible( g_cLstChatHistSB, false )
		Control_SetVisible( g_cImgSelectedTab, false)
		Control_SetVisible( g_cBtnEnd, false)
		Control_SetVisible( g_cimgEndUnderline, false)
		Control_SetVisible (g_cCmbDropDown, false)
		Control_SetVisible (g_clstScrollbar, false)
		SetPeopleListEnabledandVisible(false, g_peopleShowing)
		Control_SetVisible(g_cTrimV5, true)
		Control_SetVisible(g_cTrimV6, true)
		Control_SetVisible(g_cTrimH5, true)
		Control_SetVisible(g_cTrimH6, true)
		SetVisibleTrim(false)
	end
end

function SetVisibleTrim(v)
	Control_SetVisible( g_cTrimH1, v)
	Control_SetVisible( g_cTrimH2, v)
	Control_SetVisible( g_cTrimH3, v)
	Control_SetVisible( g_cTrimH4, v)
	Control_SetVisible( g_cTrimV1, v)
	Control_SetVisible( g_cTrimV2, v)
	Control_SetVisible( g_cTrimV3, v)
	Control_SetVisible( g_cTrimV4, v)
end

function ToggleMinimize()
	if g_minimized == true then
		Maximize(true)
	else
		Minimize()
	end
	FocusControl()
end

function Minimize()
	g_minimized = true
	g_chatFlashing = false
	LayoutMinimize()
	LoseFocus()
end

function Maximize(gainFocus)
	g_minimized = false
	LayoutMaximize()
	if gainFocus then GainFocus() end
end

function LayoutMinimize()
	Control_SetVisible(g_cImgMainFrame, true)
	Control_SetVisible(g_cImgBackDrop, false)  
	Control_SetVisible(g_cEdtSendBar, false)
	Control_SetVisible(g_cImgResize, false)
	Control_SetVisible(g_cBtnIncTrans, false)
--	Control_SetVisible(g_cBtnDecTrans, false)
	Control_SetVisible(g_cLstChatHist, false)
	Control_SetVisible(g_cLstChatHistSB, false)
	Control_SetVisible(g_cBtnMinimize, true)
	Control_SetVisible(g_cImgPrivateHighlight, false)
	Control_SetVisible(g_cImgSelectedTab, false)
	Control_SetVisible(g_clblPrivateChat, false)
	Control_SetEnabled(g_cBtnGroup, false)
	Control_SetEnabled(g_cBtnPrivate, false)
	Control_SetVisible(g_clblWorldChat, false)
	Control_SetVisible(g_clblTitle, true)
	Control_SetVisible(g_clblPrivateWith, false)
	Control_SetVisible(g_cPrivateTrim, false)
	Control_SetVisible(g_cBtnEnd, false)
	Control_SetVisible(g_cimgEndUnderline, false)
	Control_SetVisible(g_cCmbDropDown, false)
	Control_SetVisible(g_clblPeople, false)
	SetPeopleListEnabledandVisible(false, false)
	Control_SetVisible(g_clstScrollbar, false)
	Control_SetVisible(g_cBtnCollapse, false)
	Control_SetVisible(g_cBtnExpand, false)
	Control_SetVisible(g_cTrimV5, false)
	Control_SetVisible(g_cTrimV6, false)
	Control_SetVisible(g_cTrimH5, false)
	Control_SetVisible(g_cTrimH6, false)
	SetVisibleTrim(false)
	LayoutAll()
end

function LayoutMaximize()
	Control_SetVisible(g_cImgMainFrame, true)
	Control_SetVisible(g_cImgBackDrop, true)  
	Control_SetVisible(g_cEdtSendBar, true)
	Control_SetVisible(g_cImgResize, true)
--	Control_SetVisible(g_cBtnIncTrans, true)
--	Control_SetVisible(g_cBtnDecTrans, true)
	Control_SetVisible(g_cLstChatHist, true)
	Control_SetVisible(g_cLstChatHistSB, true)
	Control_SetVisible(g_cBtnMinimize, true)
	Control_SetVisible( g_cImgSelectedTab, true)
	SetVisibleTrim(true)
	Control_SetVisible(g_cImgSelectedTab, true)
	Control_SetEnabled(g_cBtnGroup, true)
	Control_SetVisible(g_clblWorldChat, true)
	Control_SetVisible(g_clblTitle, false)
	Control_SetVisible(g_clblPeople, true)
	Control_SetVisible(g_clstScrollbar, g_showScroll and g_peopleShowing)
	Control_SetVisible(g_cBtnCollapse, g_peopleShowing)
	Control_SetVisible(g_cBtnExpand, not g_peopleShowing)
	Control_SetVisible(g_cTrimV5, not g_focus)
	Control_SetVisible(g_cTrimV6, not g_focus)
	Control_SetVisible(g_cTrimH5, not g_focus)
	Control_SetVisible(g_cTrimH6, not g_focus)
	SetPeopleListEnabledandVisible(g_focus, g_peopleShowing)
	displayPrivateElements()
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_clblTitle)), 0, PRIVATE_FLASH_COLORS[3])
	g_chatFlashing = false
	if g_newMessageHidden == true and g_currentTab == GROUP_TAB then
		Control_SetVisible(g_cImgPrivateHighlight, true)
		flashColor = Element_GetTextureColor(Image_GetDisplayElement(g_cImgPrivateHighlight))
		BlendColor_SetColor( flashColor, 0, PRIVATE_FLASH_COLORS[2])
		g_newMessageHidden = false
	end
	LayoutAll()
end

function LayoutValidate()

	-- Get Screen Size
	local screenH = Dialog_GetScreenHeight(gDialogHandle)
	local screenW = Dialog_GetScreenWidth(gDialogHandle)
	local peopleH = Control_GetHeight(g_cimgPeopleFrame) + g_worldFrameHeight

	-- Validate Menu Size 
	local maxY = screenH - 132
	local maxX = screenW - 41
	if g_sizeY > maxY then g_sizeY = maxY end
	if g_sizeY < SIZE_Y_MIN then g_sizeY = SIZE_Y_MIN end
	if g_sizeX > maxX then g_sizeX = maxX end
	if g_sizeX < SIZE_X_MIN then g_sizeX = SIZE_X_MIN end
	
	-- Validate Menu Location X
	local pos = MenuGetLocationThis()
	if pos.x > (screenW - (g_sizeX + 40)) then
		MenuSetLocationThis(screenW - (g_sizeX + 40), pos.y)
	end
	local pos = MenuGetLocationThis()
	if pos.x < 0 then
		MenuSetLocationThis(0, pos.y)
	end

	-- Validate Menu Location Y
	local pos = MenuGetLocationThis()
	if pos.y > (screenH - (g_sizeY + 70 )) then
		MenuSetLocationThis(pos.x, screenH - (g_sizeY + 70 ))
	end
	local pos = MenuGetLocationThis()
	if pos.y < 59 then -- avoid top hud
		MenuSetLocationThis(pos.x, 59)
	end

	-- Calculate New Chat Display Width
	if g_sizing then
		g_sizeY = g_sizeY - peopleH
		if g_sizeY < SIZE_Y_MIN then
			g_sizeY = SIZE_Y_MIN 
		end
	end
	g_sizeChatX = g_sizeX + 12
	g_sizeChatY = g_sizeY + 9
	

	--change drop max height 
	g_maxDropHeight = g_sizeChatY /2
	setDropDownHeight()
end

function SetMainFrameSize(x, y)
	if x < MAIN_FRAME_X_MIN then x = MAIN_FRAME_X_MIN end
	if y < MAIN_FRAME_Y_MIN then y = MAIN_FRAME_Y_MIN end
	newY = y + Control_GetHeight(g_cimgPeopleFrame) + g_worldFrameHeight
	MenuSetSizeThis(x, newY)
	if g_minimized then 
		newY = MAIN_FRAME_Y_MIN
	end
	Control_SetSize(g_cImgMainFrame, x, newY )
end

function SetPeopleFrameSize(x)
	
	--g_peopleScrollPane:removeAllChildren()
	--g_peopleScrollPane:recalcLayout()
	if x < SIZE_X_DEF then
		x = SIZE_X_DEF + 10
	end

	local columns = math.floor(x / (PERSON_BUTTON_DEF_SIZE + PERSON_BUTTON_OFFSET * 3)) 
	local population = tableCount(g_whoList)
	local rows = math.ceil(population / columns)
	if rows > 5  then
		rows = 5
	end
	local newHeight = rows * Control_GetHeight(g_cBtnPerson)
	if g_peopleShowing == false or g_minimized == true  then
		newHeight = 0
	end
	Control_SetSize(g_cimgPeopleFrame, x, newHeight)	
	Control_SetSize(g_cimgInWorldFrame, x, g_worldFrameHeight)
end

function SetPeopleFrameLocation(x,y)
	Control_SetLocation(g_cimgInWorldFrame, x, y- 18)
	local peopleY = y - 18 + g_worldFrameHeight
	Control_SetLocation(g_cimgPeopleFrame, x , peopleY)
	Control_SetLocation(g_clblPeople, x + 20, y - 20)
	if g_peopleShowing == true and not g_minimized then
		Control_SetLocation(g_cBtnCollapse, x + 6, y - 14)
		setControlEnabledVisible(g_cBtnCollapse, true)
	else
		Control_SetLocation(g_cBtnExpand, x + 6, y - 14)
	end

	local scrollX = Control_GetLocationX(g_imgPeopleFrame) + Control_GetWidth(g_imgPeopleFrame) - Control_GetWidth(g_lstScrollbar) - 4
	local scrollY = Control_GetLocationY(g_imgPeopleFrame)
	
	Control_SetLocation(g_clstScrollbar, scrollX , scrollY)
	
end

function GetMainFrameSize(x, y)
	return MenuGetSizeThis(x, y)
end

function LayoutAll()
	
	local trackPos = SetScrollPos()
	local trackPeoplePos = ScrollBar_GetTrackPos(g_clstScrollbarSB)
	--set the people frame size
	SetPeopleFrameSize(g_sizeX+40)
	
	-- Validate Layout 
	LayoutValidate()

	-- OuterFrame
	if g_minimized == true then
		SetMainFrameSize(MAIN_FRAME_X_MIN, MAIN_FRAME_Y_MIN)
	else
		SetMainFrameSize(g_sizeX + 40, g_sizeY + 72)
	end

	-- Chat Send Bar
	local edtSendBarX = 5
	local edtSendBarY = g_sizeY + 20
	local edtSendBarW = g_sizeChatX
	local edtSendBarH = 28
	Control_SetLocation(g_cEdtSendBar, edtSendBarX , edtSendBarY)
	Control_SetSize(g_cEdtSendBar, edtSendBarW, edtSendBarH)

	-- Chat History
	local imgBackDropX = 5
	local imgBackDropY = 5
	local imgBackDropW = g_sizeChatX
	local imgBackDropH = g_sizeChatY
	Control_SetLocation(g_cImgBackDrop, imgBackDropX , imgBackDropY)
	Control_SetSize(g_cImgBackDrop, g_sizeChatX, g_sizeChatY)
	Control_SetSize(g_cLblChat, g_sizeChatX, g_sizeChatY)
	--main frame
	local imgMainFrameX = Control_GetLocationX(g_cImgMainFrame)
	local imgMainFrameY = Control_GetLocationY(g_cImgMainFrame)
	local imgMainFrameW = Control_GetWidth(g_cImgMainFrame)
	local imgMainFrameH = Control_GetHeight(g_cImgMainFrame)
	if g_currentTab == GROUP_TAB then		
		Control_SetSize(g_cLstChatHist, g_sizeChatX + 21, g_sizeChatY)
		Control_SetLocation(g_cLstChatHist, Control_GetLocationX(g_cLstChatHist), Control_GetLocationY(g_cLblChat))
	else
		Control_SetSize(g_cLstChatHist, g_sizeChatX + 21, g_sizeChatY - 20)
		Control_SetLocation(g_cLstChatHist, Control_GetLocationX(g_cLstChatHist), Control_GetLocationY(g_cLblChat) + 20 )
	end
	-- H1 is the top of the editbox
	Control_SetSize(g_cTrimH1, edtSendBarW, 1)
	Control_SetLocation(g_cTrimH1, edtSendBarX, edtSendBarY - 1)
	
	-- H4 is at the bottom of the editbox
	Control_SetSize(g_cTrimH4, edtSendBarW, 1)
	Control_SetLocation(g_cTrimH4, edtSendBarX, edtSendBarY + edtSendBarH)
	
	-- V2 is the left of the editbox
	tempy = Control_GetLocationY(g_cEdtSendBar)
	Control_SetSize(g_cTrimV2, 1, edtSendBarH)
	Control_SetLocation(g_cTrimV2, edtSendBarX - 1, edtSendBarY)
	
	-- V3 is the right of the editbox
	Control_SetSize(g_cTrimV3, 1, edtSendBarH)
	Control_SetLocation(g_cTrimV3, edtSendBarX + edtSendBarW, edtSendBarY)

	-- H3 is top of history
	Control_SetSize(g_cTrimH3, imgBackDropW, 1)
	Control_SetLocation(g_cTrimH3, imgBackDropX, imgBackDropY - 1)
	
	-- H2 is the bottom of the history
	Control_SetSize(g_cTrimH2, imgBackDropW, 1)
	Control_SetLocation(g_cTrimH2, imgBackDropX, imgBackDropY + imgBackDropH)
	
	-- V4 is the left of the history
	Control_SetSize(g_cTrimV4, 1, imgBackDropH)
	Control_SetLocation(g_cTrimV4, imgBackDropX - 1, imgBackDropY)

	--V1 is the right of the history
	Control_SetSize(g_cTrimV1, 1, imgBackDropH)
	Control_SetLocation(g_cTrimV1, imgBackDropX + imgBackDropW, imgBackDropY)

	--v5, v6, h5, h6 are outline of main frame
	Control_SetSize(g_cTrimV5, 1, imgMainFrameH)
	Control_SetLocation(g_cTrimV5, imgMainFrameX, imgMainFrameY)

	Control_SetSize(g_cTrimV6, 1, imgMainFrameH)
	Control_SetLocation(g_cTrimV6, imgMainFrameX + imgMainFrameW, imgMainFrameY)

	Control_SetSize(g_cTrimH5, imgMainFrameW, 1)
	Control_SetLocation(g_cTrimH5, imgMainFrameX, imgMainFrameY)

	Control_SetSize(g_cTrimH6, imgMainFrameW, 1)
	Control_SetLocation(g_cTrimH6, imgMainFrameX, imgMainFrameY + imgMainFrameH)

	Control_SetSize(g_cPrivateTrim, edtSendBarW, 1)

	-- Min/Dec/Inc Buttons
	local frameSize = GetMainFrameSize()
	Control_SetLocation(g_cBtnMinimize, frameSize.width - 19, -15)
	Control_SetLocation(g_cBtnIncTrans, frameSize.width - 40, -14)


	Control_SetLocation(g_cBtnEnd, frameSize.width -  75, 10 )
	Control_SetLocation(g_cimgEndUnderline, frameSize.width - 75, 23)

	--Set location of people menu
	frameSize.height = frameSize.height - Control_GetHeight(g_cimgPeopleFrame) - g_worldFrameHeight
	SetPeopleFrameLocation(Control_GetLocationX(g_cImgMainFrame), frameSize.height )

	if g_focus or g_minimized then
		Control_SetVisible( g_cBtnIncTrans, false )
	else
		Control_SetVisible( g_cBtnIncTrans, true )
	end

	-- Resize
	Control_SetLocation(g_cImgResize, g_sizeX + RESIZE_X_OFF, frameSize.height + Control_GetHeight(g_cimgPeopleFrame) + g_worldFrameHeight- RESIZE_Y_OFF)

	-- Flow Chat Lines
	FlowChatLines()
	
	--show people
	local peopleVisible = (g_sizing == false and g_peopleShowing == true and g_minimized == false)
	local peopleEnabled = peopleVisible and g_focus == true
	SetPeopleListEnabledandVisible(peopleEnabled, peopleVisible)

	local messageCount = ListBox_GetSize(g_cLstChatHist)
	if trackPos ~= 100 and g_newMessage and messageCount == g_chatbuffersize then
		trackPos = trackPos - g_newLineHeight
		if trackPos < 0 then
			trackPos = 0
		end
	end
	g_newMessage = false
	ScrollBar_SetTrackPos( g_lstChatHistSB, trackPos )
	ScrollBar_SetTrackPos( g_lstScrollbarSB, trackPeoplePos )
end

function SetScrollPos()
	local trackPos = ScrollBar_GetTrackPos( g_cLstChatHistSB )


	--Check if scroll bar is currently on the bottom before any new message or resize
	local bottomPos = GetScrollBottomPosition()

	if trackPos >= bottomPos then
		trackPos = 100
	end
	return trackPos
end

function GetScrollBottomPosition()
	local page_size = ScrollBar_GetPageSize(g_lstChatHistSB)
	local messageCount = ListBox_GetSize(g_cLstChatHist)
	local bottomPos = messageCount - page_size
	return bottomPos
end

------------------------
--EVENT HANDLERS--
------------------------
function ReplyRequestHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local handle = KEP_EventDecodeNumber( event )
	fireReplyUpdateEvent()
end

-- Unused ?
function prefEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local lang_pref = KEP_EventDecodeString( event )
 	if lang_pref == "true" then
 	    g_useFilter = true
 	else
 	    g_useFilter = false
 	end
	return KEP.EPR_CONSUMED
end

-- Unused ?
function hudEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local msg = KEP_EventDecodeString( event )
	table.insert( g_chatlines, msg)
	table.remove( g_chatlines, 0)
	table.insert( g_chattypes, ChatType.System)
	table.remove( g_chattypes, 0)
	LayoutAll()
end

-- Unused ?
function typeStartHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local msg = KEP_EventDecodeString( event )
	Maximize(true)
	EditBox_ClearText(g_edtSendBar)
	EditBox_SetText(g_edtSendBar, msg, false)
end

-- Unused ?
function chatKeyPressedEventHandler()
	if g_ignoreEnter == false then
		Maximize(true)
	end
end


function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.PLACE_INFO then
		local estr = KEP_EventDecodeString( event )
		ParseLocationData(estr)
	   	return KEP.EPR_OK
	elseif filter == WF.FRIENDS  then
		local friendData = KEP_EventDecodeString( event )
		parseFriendData(friendData)
		return KEP.EPR_OK
	elseif filter == WF.GET_USER then
		local accessData = KEP_EventDecodeString( event )
		local s, strPos, result
		s, strPos, result = string.find(accessData, "<ReturnCode>(.-)</ReturnCode>")
		if result == "0" then
			s, strPos, result = string.find(accessData, "<GM>(.-)</GM>", strPos)
			g_isGM = strToBool(result)
		end
	end
end

-- Called From HUD Chat Button & Friend Private Message
function SendPrivateMessageHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local playerName = KEP_EventDecodeString(event)
	if KEP_EventMoreToDecode(event) == 1 then
		g_pmSource = KEP_EventDecodeString(event)
	end

	Maximize(true)
	if playerName and playerName ~= "No Player Name" then	
		g_privateWith = string.lower(playerName)	
		addPrivateMessage(false, playerName, false)
		OpenPrivateTab()
	end
end

-- Gets the 4096 event from the client that tells us to render
function attribEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter ~= 4096 then return end

	local msg   = KEP_EventDecodeString( event )
	local red   = KEP_EventDecodeNumber( event )
	local green = KEP_EventDecodeNumber( event )
	local blue  = KEP_EventDecodeNumber( event )
	local type  = KEP_EventDecodeNumber( event )

	if tableCount(g_chatlines) >= g_chatbuffersize then
		table.remove( g_chatlines, 0)
		table.remove( g_chattypes, 0)
	end
	table.insert( g_chatlines, msg)
	table.insert( g_chattypes, type)
	
		
	LayoutAll()

	return KEP.EPR_OK
end

function textHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	local msg = KEP_EventDecodeString( event )
	local type = KEP_EventDecodeNumber( event )
	local lmsg = string.lower(msg)
	local msgS, msgE, player = string.find(msg, "Person (.+) not found") 

	-- Log To KanevaChatLog.log
	KEP_LogChat(msg)

	g_newMessage = true
	if type == 3 or player ~= nil then

		if player ~= nil then
			if g_currentPrivates[string.lower(player)] and g_currentPrivates[string.lower(player)].replied == true then
				msg = g_currentPrivates[string.lower(player)].realName.." has gone offline."
			else
				msg = player.." is offline or does not exist."
				g_privateWith = string.lower(player)
			end
			addPrivateMessage(msg, player, false)
		end
		ret, ret2, replyTo, ret = string.find( msg, "(.+) tells you >" )
		if replyTo ~= nil then
			addPrivateMessage(msg, replyTo, true)
			newPrivateMessage(replyTo)

				
		else
			ret, ret2, replyTo, ret = string.find( msg, "You tell (.+) >" )
			if replyTo ~= nil then
				addPrivateMessage(msg, replyTo, false)
				g_privateWith = string.lower(replyTo)
				OpenPrivateTab()
			end
		end
	else
		if tableCount(g_chatlines) >= g_chatbuffersize then
			table.remove( g_chatlines, 1)
			table.remove( g_chattypes, 1)
		end
		table.insert( g_chatlines, msg)
		table.insert( g_chattypes, type)
	end

	LayoutAll()

	local replyTo = nil
	ret, ret2, replyTo, ret = string.find( msg, "(.+) tells you >" )
	if replyTo ~= nil then
		g_replyTo = replyTo
		fireReplyUpdateEvent()
	end
end

function returnBanList(event)
	if event.returnMessage and event.returnMessage ~= "" then
		if event.blocker and event.blocker == g_playerName then
			e = KEP_EventCreate("RenderTextEvent" )
				KEP_EventEncodeString( e, event.returnMessage )
				KEP_EventEncodeNumber( e, ChatType.System  )		
				KEP_EventQueue( e )
		end
	end
end

function addPersonHandler(event)
	if event.playerName and event.userID then
		local name = event.playerName
		local userID = event.userID
		if name ~= g_playerName then
			local userTable = {}
			userTable.name = name
			userTable.userID = userID
			table.insert(g_whoList, userTable)
			LayoutAll()
			UpdatePeopleList()
			--LayoutAll()
		end
	end
end

function removePersonHandler(event)
	if event.playerName then
		local name = event.playerName
		--if name ~= g_playerName then
			--local userTable = {}
			--userTable.name = name
			--userTable.userID = userID
			--table.insert(g_whoList, userTable)
			--UpdatePeopleList()
		--end
		for i, v in pairs(g_whoList) do
			if name == v.name then
				table.remove(g_whoList, i)	
				local buttonCheck =  Dialog_GetButton(gDialogHandle, "btnPerson_"..tostring(name))
				if buttonCheck then
					local buttonHandle = Button_GetControl(buttonCheck)
					Dialog_RemoveControl(gDialogHandle, "btnPerson_"..tostring(name))
					g_peopleScrollPane:removeChild(buttonHandle, false )
				end
				LayoutAll()
				UpdatePeopleList()
				--LayoutAll()
				return
			end
		end
	end
end
------------------------
--PARSING DATA--
------------------------
function ParseLocationData(g_locationData)
    local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	s, strPos, result = string.find(g_locationData, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then

		s, strPos, result = string.find(g_locationData, "<name>(.-)</name>")
		local name = result
	
		s, strPos, result = string.find(g_locationData, "<population_count>(.-)</population_count>")
		g_population = result
		
		local welcomeMessage = "Welcome to "..tostring(name).."!"
		if g_entered == false and g_firstEntered == false then
			local e = KEP_EventCreate("RenderTextEvent" )
			KEP_EventEncodeString( e, welcomeMessage )
			KEP_EventEncodeNumber( e, ChatType.Group  )		
			KEP_EventQueue(e)
			--For some reason, commenting this line causes people list to not show in beginning
		end	
		if tonumber(g_population) > 1 and g_entered == false then
			local e = KEP_EventCreate( "RenderTextEvent" )
			KEP_EventEncodeString( e, "Click a person's name below to see their info" )
			KEP_EventEncodeNumber( e, ChatType.Group  )		
			KEP_EventQueue(e)	
		end
		g_entered = true
 	end
end

function parseFriendData(g_friendData)
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	-- Reset Who List
	--g_whoList = {}
	
	-- Fill Who List With Users
	if not ( next(g_whoList) ) then
		s, strPos, result = string.find(g_friendData, "<ReturnCode>([0-9]+)</ReturnCode>")
		if result == "0" then
			s, e, numRec = string.find(g_friendData, "<NumberRecords>([0-9]+)</NumberRecords>")
			for i=1,numRec do
				
				s, strPos, user = string.find(g_friendData, "<name>(.-)</name>", strPos)
				s, strPos, userID = string.find(g_friendData, "<player_id>([0-9]+)</player_id>", strPos)
				
				local userTable = {}
				userTable.name = user
				userTable.userID = userID
				table.insert(g_whoList, userTable)
			end
			LayoutAll()
		end
	end
	UpdatePeopleList()
end

------------------------
--TEXT DISPLAY LAYOUT--
------------------------
function GetTextAndUserColors(type)
	cUser = "<font color=FF00C500>"
	cText = "<font color=FFD2D2D2>"
	if type == ChatType.Clan then
		cText = "<font color=" .. chatcolor.clan ..">"
	elseif type == ChatType.Group then
		cText = "<font color=FF5CE4E9>"
	elseif type == ChatType.System then
		cText = "<font color=FFFFBF0D>"
	elseif type == ChatType.Whisper then
		cText = "<font color=" .. chatcolor.whisper ..">"
		cUser = cText
	elseif type == ChatType.Shout then
		cText = "<font color=" .. chatcolor.shout ..">"
		cUser = cText
	elseif type == ChatType.Private then
		--cText = "<font color=FFBB0CE8>"
		cUser = "<font color=FFBB0CE8>"
	end
	return cText,cUser
end

function GetTextAndUserStrings(txt)
	local userStrS, userStrE, userStr = string.find(txt, "(.+>)", 1) 
	if userStr then
		return string.sub(txt, userStrE + 1, -1), userStr
	else
		return txt		
	end
end

function ResetDisplayLines()
	g_displaylines = {}
	g_displaylinecount = 0
	g_chatlinecnt = (g_sizeY / 15.5) - .5 
end

function RefreshDisplayLines()
	ListBox_RemoveAllItems(g_lstChatHist)
	for i = 1, g_chatbuffersize do
		local line = g_displaylines[g_chatbuffersize + 1 - i] 
		if line ~= nil and line ~= "" then
			if string.find(line, "</messagecenter>") ~= nil then		
				ListBox_AddItem(g_lstChatHist, line, 1)
			else
				ListBox_AddItem(g_lstChatHist, line, 0)
			end
		end
	end
	ListBox_SelectItem(g_lstChatHist, g_chatbuffersize)
end

function InsertDisplayLine(chatLineText, chatLineType)
	if chatLineText == nil or chatLineText == "" then return end

	-- User Name Formatted Only On First Line
	local strText, strUser = GetTextAndUserStrings(chatLineText)
	local cText, cUser = GetTextAndUserColors(chatLineType)
	local str = ""
	if g_displayLineUser == true and strUser and cUser then
		str = cUser .. strUser .. "</font>" .. cText .. " " .. strText .. "</font>"
	else
		str = cText .. strText .. "</font>"
	end
	g_displayLineUser = false
	table.insert( g_displaylines, 1, str)
	g_displaylinecount = g_displaylinecount + 1
end

function breaktoken(message)
	local spcpos = string.find(message, " ")
	local brackpos = nil --string.find(message, "<")
	local tokenpos
	local work
	local restmessage
--	local tag = false
	if message == nil or message == "" then
		return "", ""
	end
	if spcpos == nil and brackpos == nil then
		return message, ""
	elseif spcpos ~= nil and brackpos == nil then
		tokenpos = spcpos
--		tag = false
--	elseif spcpos == nil and brackpos ~= nil then
--		if brackpos == 1 then
--			tag = true
--		else
--			tag = false
--		end
--		tokenpos = brackpos
--	else
--		if brackpos == 1 then
--			tag = true
--		else
--			tag = false
--		end
--		tokenpos = math.min(spcpos, brackpos)
--	end
--	if tag == true then
--		local closepos = nil --string.find(message, ">")
--		if closepos ~= nil then
--			-- Hey this is an HTML tag... the entire tag is one token
--			word = string.sub(message, 1, closepos)
--			restmessage = string.sub(message, closepos + 1, -1)
--		else
--			-- Some loose bracket was in the string
--			word = string.sub(message, 1, tokenpos)
--			restmessage = string.sub(message, tokenpos + 1, -1)
--		end
--	else
		word = string.sub(message, 1, tokenpos)
		restmessage = string.sub(message, tokenpos + 1, -1)
	end
	return word, restmessage
end

function ChatLineFitsDisplayLine(chatLine)
	return (Static_GetStringWidth(g_lblChat, chatLine) <= (g_sizeChatX - 75))
end

function FlowChatLine(text, type)

	if text == nil or text == "" then return end

	g_newLineHeight = g_newLineHeight + 1
	if type == ChatType.Private then
		local msgS, msgE, player = string.find(text, "(.+) is offline or does not exist.") 
		if player then
			type = ChatType.System
		else
			msgS, msgE, player = string.find(text, "(.+) has gone offline.") 
			if player then
				type = ChatType.System
			end
		end
	end
	-- Does Line Fit Naturally ?
	if ChatLineFitsDisplayLine(text) then
		InsertDisplayLine(text, type)
		return
	end
	
	-- Find Where It Doesn't Fit
	local remLine = text
	local curLine = ""
	local lstLine = ""
	local word = ""
	while ChatLineFitsDisplayLine(curLine) do
		lstLine = curLine
		word, remLine = breaktoken(remLine)
		curLine = curLine..word
	end 

	-- Is It The First Word ?
	if lstLine == "" then

		-- Insert First Word & Flow Recursively
		InsertDisplayLine(word, type)
		FlowChatLine(remLine, type)
	else

		-- Insert What Fits & Flow Recursively
		InsertDisplayLine(lstLine, type)
		FlowChatLine(word..remLine, type)
	end
end

function FlowChatLines()

	-- Reset Display Lines
	ResetDisplayLines()

	local chatDisplayTable = {}

	if g_currentTab == GROUP_TAB then
		chatDisplayTable = deepCopy(g_chatlines)
	else
		chatDisplayTable = deepCopy(g_privateChats[g_privateWith])
	end

	-- Flow All Chat Lines Into Display Lines
	for i = 1, g_chatbuffersize do

		if chatDisplayTable[i] and chatDisplayTable[ i ] ~= "" and ((g_chattypes[ i ] ~= nil  and g_currentTab == GROUP_TAB) or g_currentTab == PRIVATE_TAB) then

			-- Filter Chat Line
			chatDisplayTable[i] = FilterChatLine(chatDisplayTable[i])

			-- Recursively Flow Chat Line
			g_displayLineUser = true
			if i == g_chatbuffersize then
				g_newLineHeight = 0
			end
			if  g_currentTab == GROUP_TAB then
				FlowChatLine(chatDisplayTable[i], g_chattypes[i])
			else
				FlowChatLine(chatDisplayTable[i], ChatType.Private)
			end
		end
	end

	-- Refresh Display Lines
	RefreshDisplayLines()
end

function FilterChatLine(chatLine)

	-- Always Abbreviate User Shouts
	chatLine = string.gsub(chatLine, " shouts > ", " >")
	chatLine = string.gsub(chatLine, " tells you > ", " >")
	chatLine = string.gsub(chatLine, "You tell (.+) >", g_playerName.. " >" )

	if g_useFilter == true then
		for i, filterWord in ipairs(g_filterWordList) do
			pat = string.gsub(filterWord, "(%a)", function (v) return "["..string.upper(v)..string.lower(v).."]" end)
			chatLine = string.gsub(chatLine, pat, "!@#$")
		end
	end

	return chatLine
end

------------------------
--PRIVATE MESSAGE FUNCTIONS--
------------------------
function addPrivateMessage(msg, player, recieved)
	
	local currentPrivates =  tableCount(g_privateChats)
	local playerCheck = string.lower(player)
	if g_privateChats[playerCheck] == nil then
		g_privateChats[playerCheck] = {}
		g_currentPrivates[playerCheck] = {}
		g_currentPrivates[playerCheck].realName = player
		g_currentPrivates[playerCheck].newMessage = false
		g_currentPrivates[playerCheck].replied = recieved	
		if g_privateWith == nil or g_privateWith == "" then
			g_privateWith = playerCheck
			Static_SetText(g_clblPrivateWith, "Private chat with "..tostring(player))
		end
		currentPrivates = tableCount(g_privateChats)
	end

	updateDropDown(player, recieved)
	if msg ~= false then
		if tableCount(g_privateChats[playerCheck]) >= g_chatbuffersize then
			table.remove(g_privateChats[playerCheck], 1)
		end
		table.insert(g_privateChats[playerCheck], msg)
	end
	
	displayPrivateElements()
end

function SendPrivateMessageEvent()
	local event = KEP_EventCreate("Tray_PrivateMessageEvent")
	KEP_EventEncodeString(event, g_pmSource)
	KEP_EventEncodeString(event, GetPMTarget())
	KEP_EventQueue(event)
	g_pmSource = MenuNameThis()
end

function updateDropDown(player, recieved)
	ComboBox_RemoveAllItems(g_cCmbDropDown)
	local playerCheck = string.lower(player)
	local index = 1
	local tempTable = {}
	for privateNames, value in pairs(g_currentPrivates) do
		table.insert(tempTable, value.realName)
	end
	table.sort(tempTable, function(a,b) return string.upper(a) < string.upper(b) end)
	for i, v in pairs(tempTable) do
		local indexName = string.lower(v)
		if g_currentPrivates[indexName] then
			if g_privateWith == "" then
				g_privateWith = indexName
			end
			if recieved == true and playerCheck == indexName then
				g_currentPrivates[indexName].realName = player
				g_currentPrivates[indexName].replied = true
				if playerCheck ~= g_privateWith or g_currentTab == GROUP_TAB then
					g_currentPrivates[indexName].newMessage = true
				end
			end

			local dropdownString = g_currentPrivates[indexName].realName
			if g_currentPrivates[indexName].newMessage then
				dropdownString = "<font color=FFBB0CE8>"..g_currentPrivates[indexName].realName.."* </font>"
			end
			ComboBox_AddItem(g_cCmbDropDown, dropdownString, index)
			index = index + 1
		end
	end

	if g_privateWith and g_privateWith ~= "" then
		ComboBox_SetSelectedByText(g_cCmbDropDown, g_currentPrivates[g_privateWith].realName)
	end
	setDropDownHeight()
	displayPrivateElements()
end

function displayPrivateElements()
	local currentPrivates =  tableCount(g_privateChats)

	if currentPrivates >= 1 then
		Static_SetText(g_clblPrivateWith, "Private chat with "..tostring(g_currentPrivates[g_privateWith].realName))
		setControlEnabledVisible(g_cBtnPrivate, true)
		Control_SetVisible(g_clblPrivateChat, not g_minimized)
		if currentPrivates > 1 then
			Static_SetText(g_clblPrivateChat, "Private Chat ("..tostring(currentPrivates)..")")
		else
			Static_SetText(g_clblPrivateChat, "Private Chat")
			setControlEnabledVisible(g_cCmbDropDown, false)
		end
	else
		setControlEnabledVisible(g_cBtnPrivate, false)
		Control_SetVisible(g_clblPrivateChat, false)
	end


	local showPrivateElements = g_currentTab == PRIVATE_TAB and not g_minimized	
	setControlEnabledVisible(g_cCmbDropDown,  showPrivateElements and currentPrivates > 1)
	Control_SetVisible(g_cPrivateTrim, showPrivateElements)
	Control_SetVisible(g_clblPrivateWith, showPrivateElements)
	Control_SetVisible(g_cBtnEnd, (showPrivateElements and g_focus== true ))
	Control_SetVisible(g_cimgEndUnderline, (showPrivateElements and g_focus== true ))
	for privateNames, v in pairs(g_currentPrivates) do
		if v.newMessage == true then
			Static_SetText(g_clblPrivateChat, Static_GetText(g_clblPrivateChat).."*")
			return
		end
		Static_SetText(g_clblPrivateChat, Static_GetText(g_clblPrivateChat))
	end
end

function EndChat(player)
	--sanity check
	if g_privateChats[player] then
		g_privateChats[player] = nil
		g_currentPrivates[player] = nil
		if g_privateWith == player then
			g_privateWith = ""
			g_currentTab = GROUP_TAB
			updateDropDown("", false)
			updateTabDisplay(g_cBtnGroup)
			LayoutAll()
		end
		if g_privateWith == nil then
			setControlEnabledVisible(g_cBtnPrivate, false)
		end
		e = KEP_EventCreate("RenderTextEvent" )
				KEP_EventEncodeString( e, "Your private conversation with "..tostring(player).." has been closed")
				KEP_EventEncodeNumber( e, ChatType.System  )		
				KEP_EventQueue( e )
	end
end

function fireReplyUpdateEvent()
	local updateReply = KEP_EventCreate( "ReplyUpdateEvent" )
	KEP_EventEncodeString( updateReply, g_replyTo )
	KEP_EventQueue( updateReply )
end

function setDropDownHeight()
	local dropHeight = tableCount(g_currentPrivates) * 15

	if g_maxDropHeight < DROP_MIN_HEIGHT then
		g_maxDropHeight = DROP_MIN_HEIGHT
	end

	if dropHeight < DROP_MIN_HEIGHT then
		dropHeight = DROP_MIN_HEIGHT
	elseif dropHeight > g_maxDropHeight  then
		dropHeight = g_maxDropHeight
	end

	ComboBox_SetDropHeight(g_cCmbDropDown, dropHeight)
end

function updateTabDisplay(buttonControl)
	FlowChatLines()
	displayPrivateElements()
	Control_SetLocation(g_cImgSelectedTab, Control_GetLocationX(buttonControl), Control_GetLocationY(buttonControl))
	Control_SetSize(g_cImgSelectedTab, Control_GetWidth(buttonControl), Control_GetHeight(buttonControl))
	Control_SetVisible(g_cImgHighlight, false)
	setBold(g_clblWorldChat, buttonControl == g_cBtnGroup)
	setBold(g_clblPrivateChat, buttonControl == g_cBtnPrivate)
end

function OpenPrivateTab()
	g_currentTab = PRIVATE_TAB
	updateTabDisplay(g_cBtnPrivate)
	Control_SetVisible(g_cImgPrivateHighlight, false)
	g_chatFlashing = false
	if g_currentPrivates[g_privateWith].newMessage == true then
		g_currentPrivates[g_privateWith].newMessage = false
		updateDropDown(g_privateWith, false)
	end
	ComboBox_SetSelectedByText(g_cCmbDropDown, g_currentPrivates[g_privateWith].realName)
	LayoutAll()
	Control_SetFocus(g_edtSendBar, true)
end

function setBold(controlName, bool)
	if bool == true then
		Element_AddFont(Static_GetDisplayElement(controlName), gDialogHandle, "Verdana", -12, 700, false)
	else
		Element_AddFont(Static_GetDisplayElement(controlName), gDialogHandle, "Verdana", -12, 400, false)
	end
end

function newPrivateMessage(replyTo)
	if not g_minimized and g_currentTab ~= PRIVATE_TAB then		
		Control_SetVisible(g_cImgPrivateHighlight, true)
	end
	g_chatFlashing = true
	if (string.lower(replyTo) == g_privateWith and g_currentTab == PRIVATE_TAB) then
		g_chatFlashing = false
	end
end

function setMessageBubbleVisible(visible)
	Control_SetVisible(g_cImgNewPrivate, visible)
	Control_SetVisible(g_clblNewPrivate, visible)
	g_chatFlashing = visible
end
------------------------
--PEOPLE LIST FUNCTIONS--
------------------------
function UpdatePeopleList()

	SetPeopleFrameSize(g_sizeX+40)
	local trackPeoplePos = ScrollBar_GetTrackPos(g_clstScrollbarSB)
	local columns = math.floor((g_sizeX+40) / (PERSON_BUTTON_DEF_SIZE + PERSON_BUTTON_OFFSET * 3) )
	local population = tableCount(g_whoList)
	local rows = math.ceil(population / columns)

	local scrollX = Control_GetLocationX(g_imgPeopleFrame) + Control_GetWidth(g_imgPeopleFrame) - Control_GetWidth(g_lstScrollbar) - 4
	local scrollY = Control_GetLocationY(g_imgPeopleFrame)
	
	Control_SetLocation(g_clstScrollbar, scrollX , scrollY)

	if rows > 5 then
		g_showScroll = true

		local showScroll = g_showScroll == true and g_peopleShowing == true and g_focus == true and g_sizing == false
		Control_SetVisible(g_clstScrollbar, showScroll)
		--Control_SetEnabled(g_clstScrollbar, true)
	else
		g_showScroll = false
		Control_SetVisible(g_clstScrollbar, false)
	end
	--g_peopleScrollPane:recalcLayout()
	if population > 0 and g_sizing == false then
		g_peopleScrollPane:removeAllChildren()
		g_population = population
		Static_SetText(g_clblPeople, "People in world ("..tostring(g_population)..")")

		local locX = Control_GetLocationX(g_cimgPeopleFrame)
		local locY = Control_GetLocationY(g_cimgPeopleFrame)
		local enabledButton =  g_focus == true and g_sizing == false and g_minimized == false
		for i, v in pairs(g_whoList) do
			local buttonCheck =  Dialog_GetButton(gDialogHandle, "btnPerson_"..tostring(v.name))
			local buttonHandle = ""
			if not buttonCheck then
				buttonHandle = copyControl(gDialogHandle, g_cBtnPerson, "btnPerson_"..tostring(v.name))

				Static_SetText( Button_GetStatic(buttonHandle), tostring(v.name))
				
				_G["btnPerson_" .. tostring(v.name) .. "_OnButtonClicked"] = function(btnHandle)
					OpenMemberMenu(v.name, v.userID)
				end

				_G["btnPerson_" .. tostring(v.name) .. "_OnRButtonUp"] = function(btnHandle)
					OpenMemberMenu(v.name, v.userID)
				end

			else
				buttonHandle = Button_GetControl(buttonCheck)
			end
			local positionX = math.ceil(i % columns)


			if positionX == 0 then
				positionX = columns
			end
			positionX = positionX - 1
			local positionY =  math.ceil(i/columns) - 1
			
			newX = locX + (PERSON_BUTTON_DEF_SIZE * positionX) + (positionX+1) * PERSON_BUTTON_OFFSET  + PERSON_BUTTON_OFFSET
			newY = locY + (Control_GetHeight(g_cBtnPerson) * positionY)

			Control_SetLocation(buttonHandle, newX, newY )
			local newButtonWidth = string.len(v.name) * 8
			if PERSON_BUTTON_DEF_SIZE < newButtonWidth then
				newButtonWidth = PERSON_BUTTON_DEF_SIZE
			end
			Control_SetSize(buttonHandle, newButtonWidth ,Control_GetHeight(g_cBtnPerson))
			Control_SetVisible(buttonHandle, not g_minimized)

			Control_SetEnabled(buttonHandle, enabledButton)
			g_peopleScrollPane:addChild(buttonHandle, false )
			--g_peopleScrollPane:recalcLayout()	
		end
	end
	ScrollBar_SetTrackPos( g_lstScrollbarSB, trackPeoplePos )
end

function OpenMemberMenu(name, userID)
	if name ~= g_playerName then
		MenuClose("RClickMember.xml")
		MenuOpen("RClickMember.xml")

		local menuOpenEvent = KEP_EventCreate( "PlayerTargetedMenuEvent" )
		local netId = 0
		if KEP_PlayerIsValid(name) then 
			local player = KEP_GetPlayerByName(name)
			netId = GetSet_Safe_GetNumber(player, MovementIds.NETWORKDEFINEDID)
		end

		KEP_EventEncodeNumber( menuOpenEvent, netId )
		KEP_EventEncodeString( menuOpenEvent, name )
		KEP_EventQueue( menuOpenEvent )
	else
		MenuToggleOpenClose("MyFame.xml")
	end
end

function ShowPeopleList(show)
	--Control_SetFocus(g_edtSendBar, false)
	if show == false then
		Control_SetSize(g_cimgPeopleFrame, 0, 0)	
	end

	setControlEnabledVisible(g_cBtnExpand, not g_peopleShowing)
	setControlEnabledVisible(g_cBtnCollapse, g_peopleShowing)
	UpdatePeopleList()
	LayoutAll()
	--Control_SetFocus(g_edtSendBar, true)
end

function SetPeopleListEnabledandVisible(enabled, visible)


	for i, v in pairs(g_whoList) do
		local buttonCheck =  Dialog_GetButton(gDialogHandle, "btnPerson_"..tostring(v.name))
		if buttonCheck then
			local buttonHandle = Button_GetControl(buttonCheck)
			Control_SetVisible(buttonHandle, visible)
			Control_SetEnabled(buttonHandle, enabled)
		end
	end
end

------------------------
--LOCAL FUNCTIONS--
------------------------
function EnterToChatSet()
	local currentText = EditBox_GetText(g_edtSendBar)
	if (currentText ~= " Press Enter To Chat") then
		g_saveText = currentText
	    EditBox_SetText( g_edtSendBar, " Press Enter To Chat", false )
	end
end

function EnterToChatClear()
	local currentText = EditBox_GetText(g_edtSendBar)
	if (currentText == " Press Enter To Chat") then
	    EditBox_SetText( g_edtSendBar, g_saveText, false )
		g_saveText = ""
	end
end

function tableCount(inputTable)
	local tempCount = 0

	for i,v in pairs(inputTable) do
		if v and v ~= "" then
			tempCount = tempCount + 1
		end
	end

	return tempCount
end


function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" or type(value) == "userdata" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end


function setControlEnabledVisible(controlName, bool)
	Control_SetVisible(controlName, bool)
	Control_SetEnabled(controlName, bool)
end

function copyControl(dialogHandle, controlHandle, newName)
	Dialog_CopyControl(dialogHandle, controlHandle)
	Dialog_PasteControl(dialogHandle)
	local handle = Dialog_GetControlByIndex(dialogHandle, Dialog_GetNumControls(dialogHandle) - 1)
	Control_SetName(handle, newName)
	return handle
end