--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_GenericPrompt.lua
-- 
-- Overwrite confirmation menu
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

----------------------
-- Local Functions
----------------------

local configureButtons--()

----------------------
-- Variables
----------------------

local CONFIG_TYPES = {OK = "ok", YES_NO = "yesNo", YES_NO_CANCEL = "yesNoCancel"}
local BUTTON_DEFAULT_POSITION = {x = 120, y = 83}
local TWO_BUTTON_OFFSET = 43
local THREE_BUTTON_OFFSET = 86

local m_configType

-- When the menu is created
function onCreate()
	-- Register client event handlers
	KEP_EventRegisterHandler("genericPromptInitHandler", "FRAMEWORK_GENERIC_PROMPT_INIT", KEP.HIGH_PRIO)
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Catches relevant data for this menu to display
function genericPromptInitHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_configType = KEP_EventDecodeString(tEvent) -- Button Configuration

	local text = KEP_EventDecodeString(tEvent) -- Description Text
	Static_SetText(gHandles["stcText"], text)

	if KEP_EventMoreToDecode(tEvent) == 1 then
		local title = KEP_EventDecodeString(tEvent) -- Title of Prompt
		Dialog_SetCaptionText(gDialogHandle, "     " .. title)
	end

	configureButtons()
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

function btnYes_OnButtonClicked(buttonHandle)
	local event = KEP_EventCreate( "FRAMEWORK_GENERIC_PROMPT_RETURN" )

	if m_configType == CONFIG_TYPES.OK then
		KEP_EventEncodeString(event, "ok")
	else
		KEP_EventEncodeString(event, "yes")
	end
	
	KEP_EventQueue( event )
	
	
	MenuCloseThis()
end

function btnNo_OnButtonClicked(buttonHandle)
	local event = KEP_EventCreate( "FRAMEWORK_GENERIC_PROMPT_RETURN" )
	KEP_EventEncodeString(event, "no")
	KEP_EventQueue( event )
	
	MenuCloseThis()
end

function btnCancel_OnButtonClicked(buttonHandle)
	local event = KEP_EventCreate( "FRAMEWORK_GENERIC_PROMPT_RETURN" )
	KEP_EventEncodeString(event, "cancel")
	KEP_EventQueue( event )
	
	MenuCloseThis()
end

-- -- -- -- -- -- --
-- Local Functions
-- -- -- -- -- -- --

configureButtons = function()

	if m_configType == CONFIG_TYPES.OK then
		Static_SetText(gHandles["btnYes"], "OK")
		Control_SetVisible(gHandles["btnYes"], true)
	elseif m_configType == CONFIG_TYPES.YES_NO then

		Control_SetLocation( gHandles["btnYes"],BUTTON_DEFAULT_POSITION.x - TWO_BUTTON_OFFSET, BUTTON_DEFAULT_POSITION.y)
		Control_SetLocation( gHandles["btnNo"],BUTTON_DEFAULT_POSITION.x + TWO_BUTTON_OFFSET, BUTTON_DEFAULT_POSITION.y )

		Control_SetVisible(gHandles["btnYes"], true)
		Control_SetVisible(gHandles["btnNo"], true)

	elseif m_configType == CONFIG_TYPES.YES_NO_CANCEL then

		Control_SetLocation( gHandles["btnYes"],BUTTON_DEFAULT_POSITION.x - THREE_BUTTON_OFFSET, BUTTON_DEFAULT_POSITION.y)
		Control_SetLocation( gHandles["btnCancel"],BUTTON_DEFAULT_POSITION.x + THREE_BUTTON_OFFSET, BUTTON_DEFAULT_POSITION.y )

		Control_SetVisible(gHandles["btnYes"], true)
		Control_SetVisible(gHandles["btnNo"], true)
		Control_SetVisible(gHandles["btnCancel"], true)
	end

end