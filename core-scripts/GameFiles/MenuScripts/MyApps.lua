--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

myApps = {}
g_selectedIndex = nil

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	ListBox_SetHTMLEnabled(gHandles["lbxApps"], true)
	requestApps()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.GET_MY_3DAPPS then
		local eventData = KEP_EventDecodeString(event)
		parseMy3DApps(eventData)
	end
end

function requestApps()
	web_address = GameGlobals.WEB_SITE_PREFIX .. GET_MY_3DAPPS_SUFFIX .. "&allWorldTypes=true"
	makeWebCall( web_address, WF.GET_MY_3DAPPS, 1, 10000000) --NOTE: PLEASE don't have more than 10 million apps!
end

function parseMy3DApps(ret)
	myApps = {}
	s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		local s, e, numRecs = string.find(ret, "<NumberRecords>(.-)</NumberRecords>")
		numRecs = tonumber(numRecs)
		if numRecs > 0 then
			local start = 0
			local last = 0
			for i=1,numRecs do
				s, e, appRecord = string.find(ret, "<3DApp (.-) />", e)
				local appData = {}
				start, last, appData.name = string.find(appRecord, 'Name="(.-)" Description')
				start, last, appData.description = string.find(appRecord, 'Description="(.-)" GameId')
				start, last, appData.gameId = string.find(appRecord, 'GameId="(.-)" CommunityId')
				start, last, appData.communityId = string.find(appRecord, 'CommunityId="(.-)" ThumbnailURL')
				start, last, appData.thumbnail = string.find(appRecord, 'ThumbnailURL="(.-)" OwnerUsername')
				start, last, appData.owner = string.find(appRecord, 'OwnerUsername="(.-)" IsPublic')
				start, last, appData.isPublic = string.find(appRecord, 'IsPublic="(.-)" ServerStatusId')
				start, last, appData.serverStatus = string.find(appRecord, 'ServerStatusId="(.-)" GameStatusId')
				start, last, appData.gameStatus = string.find(appRecord, 'GameStatusId="(.-)" GameAccessId')
				start, last, appData.gameAccess = string.find(appRecord, 'GameAccessId="(.-)" IncubatorHosted')
				start, last, appData.incubatorHosted = string.find(appRecord, 'IncubatorHosted="(.-)"')
				appData.gameId = tonumber(appData.gameId)
				appData.communityId = tonumber(appData.communityId)
				appData.isPublic = (appData.isPublic == "Y")
				appData.serverStatus = tonumber(appData.serverStatus)
				appData.gameStatus = tonumber(appData.gameStatus)
				appData.gameAccess = tonumber(appData.gameAccess)
				appData.incubatorHosted = tonumber(appData.incubatorHosted)
				table.insert(myApps, appData)
			end
		end
	end
	populateList()
end

function populateList()
	ListBox_RemoveAllItems(gHandles["lbxApps"])
	for i,v in ipairs(myApps) do
		if v.gameAccess == 5 then -- DELETED
			-- nothing
		else
			fontStart = "<font color=FF000000>"
			fontEnd = "</font>"
			extraStatus = ""
			if v.gameAccess == 2 then
				extraStatus = " (private)"
			elseif v.gameAccess == 4 then
				fontStart = "<font color=FFFF0000><i>"
				fontEnd = "</i></font>"
				extraStatus = " (inaccessible - connectivity issues)"
			end
			if v.serverStatus == 0 and v.incubatorHosted == 0 and v.gameId > 0 then
				fontStart = "<font color=FF999999><i>"
				fontEnd = "</i></font>"
				extraStatus = " (offline)"
			end
			ListBox_AddItem(gHandles["lbxApps"], fontStart .. v.name .. extraStatus .. fontEnd, v.communityId)
		end
	end
end

function btnGoTo3DApp_OnButtonClicked(buttonHandle)
	if g_selectedIndex ~= nil and g_selectedIndex > 0 then
		local appData = myApps[g_selectedIndex]
		if appData ~= nil and (appData.serverStatus > 0 or appData.incubatorHosted == 1) and appData.gameId > 0 then
			local url = "kaneva://" .. appData.gameId .. "/"
			gotoURL(url)
		elseif appData ~= nil and appData.gameId == 0 then
			local url = "kaneva://" .. KEP_GetStarId() .. "/" .. appData.name .. ".channel"
			gotoURL(url)
		else
			KEP_MessageBox("This app is not available.")
		end
	end
end

function btnManage3DApps_OnButtonClicked(buttonHandle)
	if g_selectedIndex ~= nil and g_selectedIndex > 0 then
		local appData = myApps[g_selectedIndex]
		if appData ~= nil then
			KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."community/3dapps/AppManagement.aspx?communityid=" .. appData.communityId )
		end
	end
end

function lbxApps_OnListBoxSelection(listBoxHandle, x, y, selectedIndex, selectedText)
	g_selectedIndex = selectedIndex + 1
	setControl(gHandles["btnGoTo3DApp"], true)
	setControl(gHandles["btnManage3DApps"], true)
end

function lbxApps_OnListBoxItemDblClick(listBoxHandle)
	btnGoTo3DApp_OnButtonClicked(gHandles["btnGoTo3DApp"])
end

function btnCreateNew_OnButtonClicked(buttonHandle)
	MenuOpenModal("CreateYourWorld.xml")
	MenuCloseThis()
end
