--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_BattleHandler.lua
--
-- Handles targeting, attacking, health bars and damage feedback
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\Lib_Vecmath.lua")
dofile("Framework_InventoryHelper.lua")
dofile("Framework_FlagHelper.lua")
dofile("Easing.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local FlagSystem	= _G.FlagSystem
local Math			= _G.Math
------------------------------------------------------------------------------------------------
-- Constant definitions
------------------------------------------------------------------------------------------------

-- CLICK IDS
local LEFT_CLICK  = 0
local RIGHT_CLICK = 1

-- These variables denote the trace mask types
local TRACE = {
	WORLD		= ObjectType.WORLD, 
	DYNAMIC		= ObjectType.DYNAMIC, 
	ENTITY		= ObjectType.ENTITY, 
	SOUND		= ObjectType.SOUND, 
	EQUIPPABLE	= ObjectType.EQUIPPABLE
}

local BLACK_RGB = {a = 255, r = 0, g = 0, b = 0}

local SELF_FLOATER_Y_OFFSET = 4.5
local TARGET_PLAYER_FLOATER_Y_OFFSET = 8.5
local DURABILITY_PER_ATTACK = 1
local TARGET_BREAK_TIMER = 2

local BAR_SIZE = 5 -- What scale to apply to the derived fontSize based on distance to target's head
local BAR_HEIGHT_RATIO = .125 -- What ratio of the bar height is the width

local OUT_OF_RANGE_COOLDOWN = 3
local MIN_INTERACTION_DISTANCE = 6

local DEFAULT_FONT = "Verdana"
local HEALTH_BAR_WIDTH_RATIO = .71 -- How much of the Frame is used for the width of the health bar
local HEALTH_BAR_HEIGHT_RATIO = .45 -- How much of the Frame is used for the height of the health bar
local HEALTH_BAR_X_OFFSET = .04 -- What percentage offset from the left of the frame does the bar draw from
local HEALTH_BAR_Y_OFFSET = .27 -- What percentage offset from the top of the frame does the bar draw from

local LEVEL_X_OFFSET = .72 -- What percentage of the frame from the left is used for the level
local LEVEL_Y_OFFSET = 0 -- What percentage of the frame from the top is used for the level
local LEVEL_FONT_RATIO = .7 -- Ratio of the frame's height used to determine font for level
-- FLOATER CONSTANTS
-- Floaters are generated text that emit from a specified target player for ammo/health/XP changes, etc.
local SELF_FLOATER_DURATION = .666
local TARGET_FLOATER_DAMAGE_DURATION = 1
local TARGET_FLOATER_MISS_DURATION = .7
local FLOATER_LENGTH = 2
local FLOATER_ITALICS = false
local MIN_FLOATER_FONT_SIZE = 10
local MAX_FLOATER_FONT_SIZE = 50
local FLOATER_CONTROL_WIDTH = 1000
local OBJECT_INDICATOR_BOUND_Y = 12 -- Used for healthbar + floaters if they would to appear offscreen

local HEALTH_BAR_SIZES = {
	MIN = {	width = 64 },
	MAX = {	width = 150 }
}
-- Essential fields to pass objectRegistered() check
local ESSENTIAL_OBJECT_FIELDS = {
	[4] = {
		"type", 
		"alive", 
		"health", 
		"maxHealth", 
		"targetable", 
		"mode", 
		"pvpEnabled",
		"playerDestruction", 
		"netId", 
		"playerObject", 
		"alive", 
		"armorRating"
	},
	[2] = {
		"type", 
		"alive", 
		"owner", 
		"level"
	}
}

local NET_ID_DELAY = .5 -- Time to wait after a player arrival/departure before updating netIds


------------------------------------------------------------------------------------------------
-- Local variable definitions
------------------------------------------------------------------------------------------------
local s_sCurrentTargetID			-- ID of current target
local s_tCurrentTargetObject		-- Object of current target
local s_tWorldSettings = {}			-- World settings
local s_tTargetableObjects = {}		-- Table of all (targetable) objects and players in the zone

local s_tEquippedItem = {}
local s_iEquippedItemSlot = 0
local s_tInventory = {}
local s_tAmmoCount = {}
local s_sPlayerName = ""
local s_sSelfObject = ""
local s_iNetID = 0
local s_bIsHealthBarHidden = false
local s_tHealthBar = {
	mouseover = {
		frame = nil,
		bar = nil,
		level = nil
	},
	target = {
		frame = nil,
		bar = nil,
		level = nil
	}
}

-- Handles of generated Damage notifications
local s_tFloaters = {}
local s_iFloaterCount = 0
local s_bIsWeaponEquipped = false

local s_bHasFired = false
local s_sFiringMethod = "AutoFire" -- (AutoFire, ClickToFire, TargetClickToFire, KeyToFire)
-- AutoFire: Shoots automatically if targeting
-- ClickToFire: Shoots at any object, including nontargetable, on click
-- TargetClickToFire: only shoots at targetables on click
-- KeyToFire: only shoots at targetables on key button

local s_dOutOfRangeCooldownTimer = 0
local s_dVisibleCooldownTimer -- If the target is not visible for 2 seconds, clear the target
local s_dNetIDTimer = 0

local s_bIsEmoteEnabled = false -- Track whether a special emote is happening
local s_tPlayerLocOnEmote = {} -- Player position when an emote begins

local s_tGameItems = {}

local s_bIsPlayerListUpdatePending = false

local s_tPendingPlayers = {}
local s_tOccupiedVehicles = {}	-- Occupied Vehicles


------------------------------------------------------------------------------------------------
-- Local function declarations
------------------------------------------------------------------------------------------------
local checkToolRequirements -- ()
local clearTarget -- ()
local decrementItem -- (slot)
local countAmmo -- ()
local getPlayerNameByNetID -- (netID)
local getVisible -- (targetObject, targetType)
local getPlayerObjectFromNetId -- (netId)
local validateObject -- (object)
local getWorldPosition -- (targetObject, targetType)
local targetInRange -- (targetObject, targetType, range, angle)
local attemptFire -- ()
local attemptTool -- ()
local objectRegistered -- (ID)
local checkFlagLocation --(targetObject)

---------------------------------------------------------
-- Generic Framework Functions
---------------------------------------------------------

-- Called when the menu is opened
function onCreate()
	-- Register events from the server
	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS", returnSettingsHandler)

	Events.registerHandler("FRAMEWORK_REGISTER_BATTLE_OBJECT_LIST", registerBattleObjectListHandler)
	Events.registerHandler("FRAMEWORK_REGISTER_BATTLE_OBJECT", registerBattleObjectHandler)
	Events.registerHandler("FRAMEWORK_UNREGISTER_BATTLE_OBJECT", unRegisterBattleObjectHandler)
	Events.registerHandler("FRAMEWORK_MODIFY_BATTLE_OBJECT", modifyBattleObjectHandler)
	Events.registerHandler("FRAMEWORK_PAINT_OBJECT", paintObjectHandler)
	Events.registerHandler("FRAMEWORK_EMOTE_OBJECT", emoteObjectHandler)
	Events.registerHandler("FRAMEWORK_GENERIC_ACCEPTED", genericAcceptedHandler)
	Events.registerHandler("DROP_PLAYER_INVENTORY", dropPlayerInventoryHandler)
	Events.registerHandler("FRAMEWORK_RETURN_VEHICLE_OCCUPIED", onOccupedVehicleHandler)
	Events.registerHandler("FRAMEWORK_PLAYER_LIST_UPDATE", playerListUpdateHandler) -- Get message whenever a player leaves or enters the world
	
	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler("attemptToolHandler", "FRAMEWORK_ATTEMPT_TOOL", KEP.HIGH_PRIO)

	KEP_EventCreateAndQueue( "FRAMEWORK_REQUEST_FLAGS_LOCALLY" )

	
	requestInventory(GAME_PID)

	--Events for flags
	--Regiester for flag updates
	KEP_EventRegisterHandler( "onFlagsReturnedFull", "UPDATE_FLAGS_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onFlagReturned", "UPDATE_FLAG_CLIENT", KEP.MED_PRIO)
	
	-- Register for client events
	KEP_EventRegisterHandler("buildHandModeHandler", "FRAMEWORK_BUILD_HAND_MODE", KEP.HIGH_PRIO)

	-- Inventory has been updated
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("weaponEquippedHandler", "FRAMEWORK_WEAPON_EQUIPPED", KEP.HIGH_PRIO)


	-- Register for Click events
	KEP_EventRegisterHandler("clickEventHandler", "ClickEvent", KEP.HIGH_PRIO)

	KEP_EventRegister( "UPDATE_WEAPON_DURABILITY", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler("decrementWeaponDurability", "UPDATE_WEAPON_DURABILITY", KEP.HIGH_PRIO)

	-- Request all battle objects in the zone
	Events.sendEvent("FRAMEWORK_REQUEST_BATTLE_OBJECTS")
	-- Request current zone settings
	Events.sendEvent("FRAMEWORK_REQUEST_SETTINGS")

	--Request all occupied vehicles
	Events.sendEvent("FRAMEWORK_REQUEST_OCCUPIED_VEHICLES")
	
	-- Handles floaters from the server
	Events.registerHandler("GENERATE_FLOATER", floaterBowl)
	
	-- Request inventory from the framework
	s_sPlayerName = KEP_GetLoginName()
	
	-- Get some info for this player
	local player = KEP_GetPlayer()
	if player ~= nil then
		s_iNetID = GetSet_Safe_GetNumber(player, MovementIds.NETWORKDEFINEDID)
	end
	s_sSelfObject = KEP_GetPlayerByNetworkDefinedID(s_iNetID)
	-- Instantiate yourself as an object
	s_tTargetableObjects[s_sPlayerName] = {
		type  = ObjectType.PLAYER,
		netId = s_iNetID
	}
	
	-- Disable mouse input to prevent mouse over visual gitches
	MenuSetPassthroughThis(true)

	-- Initialise the flag system
	FlagSystem:create()
	updateNetIds()
end


-- Called when the menu's closed
function onDestroy()
	-- Turn player name tags on
	KEP_SetZoneAllowsLabels(1)

	if s_bIsWeaponEquipped then
		-- Tell Framework_Inventory to un-equip this weapon
		Events.sendEvent("UNEQUIP_PLAYER_ITEM", {UNID = s_tEquippedItem.UNID, weapon = true})
		s_bIsWeaponEquipped = false
		disableActiveEmote()
	end
end

-- On dialog render
function Dialog_OnRender(dialogHandle, fElapsedTime)

	-- FLAG CHECK: Update pending netIds when ready
	if s_bIsPlayerListUpdatePending == true then
		if s_dNetIDTimer > 0 then
			s_dNetIDTimer = s_dNetIDTimer - fElapsedTime
		else -- Timer is done
			local tryAgain = false
			local updateDirty = false
			for i, name in pairs(s_tPendingPlayers) do
				if checkForPlayer(name) == true then
					updateDirty = true
					s_tPendingPlayers[i] = nil
				else
					tryAgain = true
				end
			end
			-- If at least one player is ready, update netIds			
			if updateDirty then
				updateNetIds()
			end
			-- If at least one player is NOT ready, keep trying
			if tryAgain then
				s_dNetIDTimer = NET_ID_DELAY
				s_bIsPlayerListUpdatePending = true
			else
				s_dNetIDTimer = 0
				s_bIsPlayerListUpdatePending = false
			end
		end
	end

	-- STATUS UPDATE: Check current target's validity if we have one
	if s_sCurrentTargetID then
		local tTargetObject = s_tTargetableObjects[s_sCurrentTargetID]
		if tTargetObject and (tTargetObject.type == ObjectType.PLAYER) then 
			if tTargetObject.netId then
				s_tCurrentTargetObject = getPlayerObjectFromNetId(tTargetObject.netId)
				if s_tCurrentTargetObject == nil then
					-- Log("Dialog_OnRender() Target["..tostring(s_sCurrentTargetID).."] has been invalidated. Clearing target")
					clearTarget()
					return	
				end
			else
				updateNetIds()
				return
			end
		end
	end
	
	-- ANIMATION: Handle floaters (if any), updating decay and deleting when finished
	handleFloaters(fElapsedTime)
	
	-- COUNTDOWN: Used by attemptFire during inRange check to avoid spamming "out of range" messages
	if s_dOutOfRangeCooldownTimer > 0 then
		s_dOutOfRangeCooldownTimer = s_dOutOfRangeCooldownTimer - fElapsedTime
	end

	-- STATUS UPDATE: Clear out the target if the durability of the weapon is 0
	if s_iEquippedItemSlot > 1 then
		local tItem = s_tInventory[s_iEquippedItemSlot - 1]
		if tItem and tItem.instanceData and tItem.instanceData.durability == 0 then
			clearTarget()
			return
		end
	end

	-- FLAG CHECK: Is there a pending "fire" event? (with a selected weapon/tool/generic and a depleted cooldown)
	if s_bHasFired then
		local tTargetObject = s_tTargetableObjects[s_sCurrentTargetID]
		--Target check to make sure if player, client has a netID. If not, will cause an 'Out of Range' error
		if  tTargetObject and tTargetObject.type == ObjectType.PLAYER then
			if s_tCurrentTargetObject == nil then
				updateNetIds()
				return
			end
		end
		-- Using a weapon, tool, or generic?
		local equippedItemType = nil
		local requiresTarget = true
		local tItemProperties = s_tEquippedItem and s_tEquippedItem.properties
		if tItemProperties then
			equippedItemType = tItemProperties.itemType
			-- Does the equipped item require a target?
			if tItemProperties.requiresTarget then
				if tItemProperties.requiresTarget == "false" then
					requiresTarget = false
				else
					requiresTarget = true
				end
			end
		end
		-- Is there a valid, active target?
		if 	s_sCurrentTargetID or
			(equippedItemType == "tool" and (s_sCurrentTargetID or not requiresTarget)) then -- Some tools do not require targets
			if equippedItemType == "weapon" then
				-- Attempt a shot
				attemptFire() -- with equippedItemType parameter
			elseif equippedItemType == "generic" then
				attemptGeneric()
			elseif equippedItemType == "tool" then
				attemptTool()
			end
		end
	end

	-- FLAG CHECK: Track player position and disable emote if the player moves
	if s_bIsEmoteEnabled then
		local playerLoc = {KEP_GetPlayerPosition()}
		if Math.getArrayDistanceSq(playerLoc, s_tPlayerLocOnEmote) > 0.01 then -- account for some rounding error
			disableActiveEmote()
		end
	end
	
	-- STATUS UPDATE: Handles the placement of health bars over the current target
	handleHealthBars(fElapsedTime)
end


-- To keep Esc from closing me. . . . . . . . . . . . .
function Dialog_OnKeyDown(dialogHandle, key, bShiftDown)
	-- Esc button
	if key == Keyboard.ESC then
		clearTarget()
	elseif key == Keyboard.F then
		local currentTime = KEP_CurrentTime()
		if s_sFiringMethod == "KeyToFire" and s_tEquippedItem and s_tEquippedItem.cooldownTime and currentTime >= s_tEquippedItem.cooldownTime and s_sCurrentTargetID then
			s_dVisibleCooldownTimer = TARGET_BREAK_TIMER
			s_bHasFired = true
		end
	end
end

---------------------------------------------------------
-- Event Handlers
---------------------------------------------------------
function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	s_tGameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	s_tGameItems = gameItems
end

function attemptToolHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	attemptTool()
end

-- Handles zone settings
function returnSettingsHandler(event)
	local destructibleObjects = event.destructibleObjects
	local allowPvP 			  = event.allowPvP
	local playerBuild 		  = event.playerBuild
	local nameTags 			  = event.playerNames
	
	-- Have name tags been set yet?
	if (s_tWorldSettings.nameTags == nil) or (nameTags ~= s_tWorldSettings.nameTags) then
		-- Allow name tags
		if nameTags then
			KEP_SetZoneAllowsLabels(1)
		-- Turn'em off
		else
			KEP_SetZoneAllowsLabels(0)
		end
	end
	
	-- Do you have a playerDestructible object targeted and did we just disable destruction?
	local tTargetObject = s_sCurrentTargetID and s_tTargetableObjects[s_sCurrentTargetID]
	if tTargetObject and (tTargetObject.type == ObjectType.DYNAMIC) and
	   (s_tWorldSettings.destructibleObjects ~= nil) and (s_tWorldSettings.destructibleObjects == true) and (destructibleObjects == false) then
		local owner = tTargetObject.owner
		-- If this object is owned
		if owner and (owner ~= "NoOwner") then
			clearTarget()
		end
	end
	
	-- Do you have a player targeted and PvP was just disabled
	if tTargetObject and (tTargetObject.type == ObjectType.PLAYER) and
	   s_tWorldSettings.allowPvP ~= nil and s_tWorldSettings.allowPvP ~= allowPvP and allowPvP == false then
		clearTarget()
	end
	
	-- Set world settings
	s_tWorldSettings = {
		destructibleObjects	= destructibleObjects,
		allowPvP			= allowPvP,
		playerBuild			= playerBuild,
		nameTags			= nameTags
	}
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if s_tInventory[updateIndex] then
		s_tInventory[updateIndex] = updateItem
	end
	
	countAmmo()
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local inventory = decompileInventory(tEvent)
	
	s_tInventory = inventory
	
	countAmmo()
end

-- Sent from Framework_Toolbar when a weapon has been equipped or de-equipped
function weaponEquippedHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- log("weaponEquippedHandler")
	-- Better safe than sorry
	local event
	if KEP_EventMoreToDecode(tEvent) ~= 0 then
		event = Events.decode(KEP_EventDecodeString(tEvent))
	end
	
	s_bIsWeaponEquipped = event.equipped and (event.equipped == true)
	
	-- Was a weapon passed in?
	if s_bIsWeaponEquipped and event.weapon then
		local currentTime = KEP_CurrentTime()
		s_tEquippedItem = event.weapon
		-- Enforce artificial weapon equip cooldown
		s_tEquippedItem.cooldownTime = currentTime + 1000
		clearTarget()
		if event.slot then
			s_iEquippedItemSlot = event.slot
		else
			s_iEquippedItemSlot = 0
		end

		--s_bHasFired = false
	-- No? Clean up
	else
		-- Clear target
		s_tEquippedItem = {}
		clearTarget()
	end

	local itemType = s_tEquippedItem.itemType or "weapon"

	local tTargetedObject = s_tTargetableObjects[s_sCurrentTargetID]
	if tTargetedObject and tTargetedObject.behavior == "pet" and itemType == "weapon" then
		clearTarget()
	end 

	if s_bIsEmoteEnabled then
		disableActiveEmote()
	end
end

-- Called when the mouse is clicked
function clickEventHandler(dispatcher, fromNetid, event, eventID, filter, objectID)
	-- log("clickEventHandler")
	if s_bIsWeaponEquipped then
		local button = KEP_EventDecodeNumber(event)
		-- Don't consume clicks if the toolbar's minimized or if a right click was fired
		if (button == RIGHT_CLICK) then return end

		-- Does the equipped item require a target?
		local requiresTarget = true
		if s_tEquippedItem and s_tEquippedItem.properties and s_tEquippedItem.properties.requiresTarget then
			if s_tEquippedItem.properties.requiresTarget == "false" then
				requiresTarget = false
			else
				requiresTarget = true
			end
		end

		-- Requires a target

		if requiresTarget then
			local sClickedObjectID = tostring(objectID)

			s_sCurrentTargetID = sClickedObjectID

			local bIsDestructionEnabled = false

			--is occupiedVehicle
			local bIsOccupiedVehicle = false
			if s_tOccupiedVehicles[sClickedObjectID] then
				bIsOccupiedVehicle = true
			end
			if filter == ObjectType.PLAYER then
				sClickedObjectID = tostring(getPlayerNameByNetID(objectID))
			elseif filter == ObjectType.DYNAMIC  and not bIsOccupiedVehicle then
				bIsDestructionEnabled = checkFlagLocation(sClickedObjectID)
			elseif bIsOccupiedVehicle then
				bIsDestructionEnabled = checkPVPFlagLocation(sClickedObjectID)
			end
			
			-- If this object isn't a known object, return
			if sClickedObjectID == nil then 
				clearTarget()
				return
			end
			
			
			local tTargetedObject = s_tTargetableObjects[sClickedObjectID]
			local tLocalPlayer = s_tTargetableObjects[s_sPlayerName]
			
			-- TODO: Wrap this into a setTarget() function
			if not tTargetedObject then
				if s_sFiringMethod == "ClickToFire" then
					s_dVisibleCooldownTimer = TARGET_BREAK_TIMER
					s_bHasFired = true
				end
				clearTarget()
				return 
			end

			
			local itemType = s_tEquippedItem.properties.itemType or "weapon"
			local paintWeapon = s_tEquippedItem.properties.paintWeapon == "true" or false
			local p2pEmoteItem = false
			local repairTool = tostring(s_tEquippedItem.properties.repairTool) == "true" or false
			if s_tEquippedItem and s_tEquippedItem.properties and s_tEquippedItem.properties.toolType == "p2pEmoteItem"then
				p2pEmoteItem = true
			end

			if  itemType == "weapon" and not checkToolRequirements()   then
				clearTarget()
				return
			end

			-- Don't target untargetable things. . . (unless using the paintbrush or p2pEmoteItem)
			if ((tTargetedObject.targetable == nil) or
				(tTargetedObject.targetable ~= "true" and tTargetedObject.targetable ~= true))
				and not (paintWeapon or p2pEmoteItem or repairTool or itemType == "generic") then
					if s_sFiringMethod == "ClickToFire" then
						s_dVisibleCooldownTimer = TARGET_BREAK_TIMER
						s_bHasFired = true
					end
					clearTarget()
					return
			end

			local itemBehavior = tTargetedObject.behavior or "none"
			local healingWeapon = s_tEquippedItem.properties.healingWeapon == "true" or false

			
			if ((itemBehavior == "pet" or itemBehavior == "actor" or itemBehavior == "vendor" or itemBehavior == "shop_vendor" or itemBehavior == "quest_giver") and itemType == "weapon" and (tLocalPlayer.pvpEnabled == false or checkPVPFlagLocation(sClickedObjectID) == false)) then
				--do not target pets/followers in pvp enabled zones
				clearTarget()
				return
			end


			--log("--- itemType = ".. tostring(itemType))
			if itemType == "tool" or itemType == "generic" then
				s_sFiringMethod = "TargetClickToFire" -- don't auto-attack with tools or generics
			else
				--log("--- DEFAULTING TO AutoFire")
				s_sFiringMethod = "AutoFire" -- default to auto-attack
			end

			-- Object clicked. Objects can't be healed. Objects can't emote.
			if (tTargetedObject.type == ObjectType.DYNAMIC) and not (healingWeapon and not repairTool) and not p2pEmoteItem then
				-- Are you allowed to attack owned items?
				--Are you in a zone you can target?
				if (((tLocalPlayer.playerDestruction == false and bIsOccupiedVehicle == false) or (bIsDestructionEnabled == false)) and repairTool == false) or itemType == "generic" then
					-- If this object isn't owned by anyone
					if tTargetedObject.spawned or (paintWeapon or itemType == "generic" or itemBehavior == "seed") then
						--do not target pets in pvp enabled zoned
						-- Set target
						-- Clear level image to create a new one
						if s_sCurrentTargetID and s_tHealthBar.target.level then
							Dialog_RemoveControl(gDialogHandle, Control_GetName(s_tHealthBar.target.level))
							s_tHealthBar.target.level = nil
						end
						Events.sendEvent("PLAYER_TARGET", {type = "Object", id = objectID})
						s_sCurrentTargetID = sClickedObjectID
					else
						-- Log("clickEventHandler() Cannot target player-placed objects right now (flag or zone setting applies)")
						clearTarget()
						s_bHasFired = false
						return
					end
				else
					-- Set target
					-- Clear level image to create a new one
					if s_sCurrentTargetID and s_tHealthBar.target.level then
						Dialog_RemoveControl(gDialogHandle, Control_GetName(s_tHealthBar.target.level))
						s_tHealthBar.target.level = nil
					end
					Events.sendEvent("PLAYER_TARGET", {type = "Object", id = objectID})
					s_sCurrentTargetID = sClickedObjectID
				end
				if s_sFiringMethod ~= "KeyToFire" then
					s_dVisibleCooldownTimer = TARGET_BREAK_TIMER
					s_bHasFired = true
				end

			-- Clicked player
			elseif (tTargetedObject.type == ObjectType.PLAYER) and
				((tTargetedObject.mode == "Player" and tTargetedObject.alive and s_sPlayerName ~= sClickedObjectID) and
				((tTargetedObject.pvpEnabled and tTargetedObject.pvpEnabled) or healingWeapon) and not paintWeapon and not ( repairTool and not healingWeapon))  or
				(p2pEmoteItem and s_sPlayerName ~= sClickedObjectID and tTargetedObject.mode == "Player")  then 


				-- Clear level image to create a new one
				if s_sCurrentTargetID and s_tHealthBar.target.level then
					Dialog_RemoveControl(gDialogHandle, Control_GetName(s_tHealthBar.target.level))
					s_tHealthBar.target.level = nil
				end
				Events.sendEvent("PLAYER_TARGET", {type = "Player", name = playerName})
				s_sCurrentTargetID = sClickedObjectID
				if s_sFiringMethod ~= "KeyToFire" then
					s_dVisibleCooldownTimer = TARGET_BREAK_TIMER
					s_bHasFired = true
				end
				
			-- Clear target if we have one
			else
				-- Fire
				if s_sFiringMethod == "ClickToFire" then
					s_dVisibleCooldownTimer = TARGET_BREAK_TIMER
					s_bHasFired = true
				end
				clearTarget()
			end

		-- Does NOT require a target

		else
			clearTarget()
			-- Get firing option
			local itemType = s_tEquippedItem.properties.itemType or "weapon"
			if itemType == "tool"  or itemType == "generic" then
				s_sFiringMethod = "TargetClickToFire" -- don't auto-attack with tools
			else
				--log("--- AUTOFIRE 2")
				s_sFiringMethod = "AutoFire" -- default to auto-attack
			end

			if s_sFiringMethod ~= "KeyToFire" then
				s_dVisibleCooldownTimer = TARGET_BREAK_TIMER
				s_bHasFired = true
			end
		end
	end
end

-- Handles the battle object list event from the server
function registerBattleObjectListHandler(event)
	-- Objects
	if event.objects then
		-- Extract all targetable objects
		for i, newObject in pairs(event.objects) do
			newObject.type = "Object"
			registerBattleObjectHandler(newObject)
		end
	end
	
	-- Players
	if event.players then
		-- Extract all targetable players
		for i, newObject in pairs(event.players) do
			newObject.type = "Player"
			registerBattleObjectHandler(newObject)
		end
	end
end

-- Handles a new battle object being registered
function registerBattleObjectHandler(event)
	local type  	  = event.type
	local ID 		  = tostring(event.ID)
	local UNID		  = event.UNID
	local alive 	  = event.alive
	local itemType	  = event.itemType
	local health 	  = event.health
	local maxHealth   = event.maxHealth
	local targetable  = event.targetable
	
	-- New object
	if type == "Object" then
		local owner				 = event.owner
		local tooltip			 = event.tooltip
		local level				 = event.level
		local isLoot			 = event.isLoot
		local master			 = event.master
		local behavior 			 = event.behavior
		local spawned			 = event.spawned
		--local interactionRange = event.interactionRange
		
		-- Update as many fields as were passed
		local tObject = s_tTargetableObjects[ID]
		if tObject == nil then
			tObject = {type = ObjectType.DYNAMIC}
			s_tTargetableObjects[ID] = tObject
		end
		if UNID		  ~= nil then tObject.UNID			= UNID end
		if alive 	  ~= nil then tObject.alive			= alive end
		if itemType	  ~= nil then tObject.itemType		= itemType end
		if owner 	  ~= nil then tObject.owner			= owner end
		if tooltip 	  ~= nil then tObject.tooltip		= tooltip end
		if health 	  ~= nil then tObject.health		= health end
		if maxHealth  ~= nil then tObject.maxHealth		= maxHealth end
		if targetable ~= nil then tObject.targetable	= targetable end
		if behavior   ~= nil then tObject.behavior		= behavior end
		if level 	  ~= nil then tObject.level			= level end
		if isLoot 	  ~= nil then tObject.isLoot		= isLoot end
		if master 	  ~= nil then tObject.master		= master end
		if spawned	  ~= nil then tObject.spawned		= spawned end
		
	-- New Player
	elseif type == "Player" then
		local mode  			= event.mode or "Player"
		local pvpEnabled 		= event.pvpEnabled
		local playerDestruction = event.playerDestruction
		local armorRating 		= event.armorRating
		
		-- Update as many fields as were passed
		local tObject = s_tTargetableObjects[ID]
		if tObject == nil then
			tObject = {type = ObjectType.PLAYER}
			s_tTargetableObjects[ID] = tObject
		end
		if alive			 ~= nil then tObject.alive				= alive end
		if mode				 ~= nil then tObject.mode				= mode end
		if pvpEnabled		 ~= nil then tObject.pvpEnabled			= pvpEnabled end
		if playerDestruction ~= nil then tObject.playerDestruction	= playerDestruction end
		if health			 ~= nil then tObject.health				= health end
		if maxHealth		 ~= nil then tObject.maxHealth			= maxHealth end
		if armorRating		 ~= nil then tObject.armorRating		= armorRating end
		if targetable		 ~= nil then tObject.targetable			= targetable end
	end
end

function unRegisterBattleObjectHandler(event)
	local ID = tostring(event.ID)
	
	if s_tTargetableObjects[ID] then
		if s_sCurrentTargetID and (s_sCurrentTargetID == ID) and s_tTargetableObjects[s_sCurrentTargetID] then
			clearTarget()
		end
		
		s_tTargetableObjects[ID] = nil
	else
		Log("unRegisterBattleObjectHandler() Passed ID["..tostring(ID).."] isn't a registered target.")
	end
end

-- Modify an existing Game Item
function modifyBattleObjectHandler(event)
	local ID = tostring(event.ID)
	local modifiedFields = event.modifiedFields
	-- Is this item a registered target?
	local tObject = s_tTargetableObjects[ID]
	if tObject then
		for index, value in pairs(modifiedFields) do
			-- We're talkin bout you son
			if (s_sPlayerName == ID) then
				-- Did you just go into God mode and have a weapon equipped?
				if (index == "mode" and value == "God") and s_bIsWeaponEquipped then
					-- Un-equip this weapon
					Events.sendEvent("UNEQUIP_PLAYER_ITEM", {GLID = s_tEquippedItem.properties.GLID, weapon = true})
					s_bIsWeaponEquipped = false
				-- Did you just die?
				elseif (index == "alive" and value == false) then
					-- Clear your target
					clearTarget()
					if s_bIsWeaponEquipped then
						-- Un-equip this weapon
						Events.sendEvent("UNEQUIP_PLAYER_ITEM", {GLID = s_tEquippedItem.properties.GLID, weapon = true})
						s_bIsWeaponEquipped = false
					end

					-- If your current target is player placed and playerDestruction is now false
				elseif (index == "playerDestruction") and
					(value == false) and
					s_sCurrentTargetID and (s_tTargetableObjects[s_sCurrentTargetID].type == ObjectType.DYNAMIC) then
					local owner = s_tTargetableObjects[s_sCurrentTargetID].owner
					-- If this object isn't owned by anyone
					if owner and (owner ~= "NoOwner") then
						clearTarget()
					end
				--Did you just go into a pvp disabled zone while targeting dog or another player
				elseif (index == "pvpEnabled" and value == false)  and s_tEquippedItem and s_sCurrentTargetID then
					local itemType = s_tEquippedItem.itemType or "weapon"
					local itemBehavior = s_tTargetableObjects[s_sCurrentTargetID].behavior
					if (s_sCurrentTargetID and s_tTargetableObjects[s_sCurrentTargetID] and (itemBehavior == "pet" or itemBehavior == "actor" or itemBehavior == "vendor" or itemBehavior == "shop_vendor" or itemBehavior == "quest_giver") and itemType == "weapon") or s_tTargetableObjects[s_sCurrentTargetID].type == ObjectType.PLAYER then
						clearTarget()
					end
				end

			-- If this a different object than you
			elseif s_sPlayerName ~= ID then
				-- Has your current target been modified?
				if s_sCurrentTargetID and (s_sCurrentTargetID == ID) and s_tTargetableObjects[s_sCurrentTargetID] then
					-- Did it just die, go into God mode or enter a pvpDisabled zone?
					if (index == "alive" 	  and value == false) or
					   (index == "health" 	  and value == 0) or 
					   (index == "mode" 	  and value == "God") or
					   (index == "pvpEnabled" and value == false) then
							clearTarget()
					end
				end
				-- Did this player's armorRating change?
				if (index == "armorRating" and value ~= s_tTargetableObjects[ID][index]) and
					s_tHealthBar.target.level then
					Dialog_RemoveControl(gDialogHandle, Control_GetName(s_tHealthBar.target.level))
					s_tHealthBar.target.level = nil
				end
			end
			s_tTargetableObjects[ID][index] = value
		end
	end
end

function buildHandModeHandler(event)
	if s_bIsEmoteEnabled then
		disableActiveEmote()
	end
end

-- Receives event from server updating flags
function emoteObjectHandler(event)

	local emoteState = event.emoteState
	if emoteState == nil then log("Framework_BattleHandler - emoteObjectHandler() - [emoteState] is nil."); return end
	
	-- -- Enable emote
	if emoteState == true and s_bIsEmoteEnabled == false then
		s_bIsEmoteEnabled = true
		s_tPlayerLocOnEmote = {KEP_GetPlayerPosition()}
	-- Disable emote
	elseif emoteState == false and s_bIsEmoteEnabled == true then
		s_bIsEmoteEnabled = false
	end

end

function genericAcceptedHandler(event)
	--log("--- genericAcceptedHandler")

	if event.accepted == true then
		-- Does the weapon match?
		if event.weapon == s_tEquippedItem.UNID then
			
			-- Turn on cooldown
			local currentTime = KEP_CurrentTime()
			s_tEquippedItem.cooldownTime = currentTime + (1000)

			-- Remove one of that generic item from inventory
			for i, tItem in ipairs(s_tInventory) do 
				-- Get first ammo slot in inventory for updating
				if tItem.UNID == s_tEquippedItem.UNID then
					decrementItem(i)
					-- Inform Framework_Toolbar that we're firing so it can queue the attack flash and cooldown
					-- Don't send attack flash if last of that item is about to be removed
					if tItem.count > 1 then
						local ev = KEP_EventCreate("FRAMEWORK_ATTACK_FLASH")
						KEP_EventEncodeString(ev, Events.encode(s_tEquippedItem))
						KEP_EventQueue(ev)
					end
					break
				end
			end
			
			updateDirty()
		end
	end
end

function dropPlayerInventoryHandler(event)
	-- When the player dies, unequip the current weapon
	if s_bIsWeaponEquipped then
		-- Tell Framework_Inventory to un-equip this weapon
		Events.sendEvent("UNEQUIP_PLAYER_ITEM", {UNID = s_tEquippedItem.UNID, weapon = true})
		s_bIsWeaponEquipped = false
		-- Inform Framework_Cursor_HighlightHandler of what's going on
		local ev = KEP_EventCreate("FRAMEWORK_WEAPON_EQUIPPED")
		local eventInfo = {equipped = false}
		KEP_EventEncodeString(ev, Events.encode(eventInfo))
		KEP_EventQueue(ev)	
		m_equippedUNID = nil
	end
	-- Tell the toolbar to select the hand
	ev = KEP_EventCreate("FRAMEWORK_SELECT_HAND")
	KEP_EventQueue(ev)
end

function onOccupedVehicleHandler(event)
	if event.PID then
		if event.occupied == 1 then
			s_tOccupiedVehicles[tostring(event.PID)] = true
		else
			s_tOccupiedVehicles[tostring(event.PID)] = nil
		end
	end
end

function playerListUpdateHandler(event) -- Register netIDs here
	-- log("\nPlayer "..tostring(event.playerName).." arriving = "..tostring(event.arriving == 1))
	local arriving = (event.arriving == 1)
	local playerRegistered = checkForPlayer(event.playerName)

	if arriving then
		if playerRegistered then
			updateNetIds()
		else
			table.insert(s_tPendingPlayers, event.playerName)
			s_bIsPlayerListUpdatePending = true
			s_dNetIDTimer = NET_ID_DELAY
		end
	else -- Player is leaving
		s_tTargetableObjects[event.playerName] = nil
		updateNetIds()
	end
end

function paintObjectHandler(event)
	local PID = tostring(event.PID)
	local owner = s_tTargetableObjects[PID].owner
	local targetItemType = s_tTargetableObjects[PID].itemType
	if(owner == s_sPlayerName) then -- TODO: player still loses durability when this happens. Move this check sooner?
		MenuOpen("TextureBrowser2.xml")
		textureSelEvent = KEP_EventCreate( "TextureSelectionEvent" )
		KEP_EventEncodeNumber(textureSelEvent, 1)
		KEP_EventEncodeNumber(textureSelEvent, event.PID)
		KEP_EventQueue( textureSelEvent )
		-- TODO: make sure that hard-coded object type below does not cause any breaks
		KEP_ReplaceSelection(ObjectType.DYNAMIC, event.PID) -- Select the object behind the scenes so the texture browser knows what to edit
		-- Metric webcall
		Events.sendEvent("UPDATE_PAINT_METRICS", {painter = owner})
	elseif targetItemType == "placeable" then
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, "You cannot paint an object you do not own.")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
	else
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, "You cannot paint that type of object.")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
	end
end


-- Handles floaters from Framework_Toolbar
function floaterBowl(event)
	generateFloater(event.text, event.color, event.target)
end

-- Regenerate the list of flag objects using info from the server
function onFlagsReturnedFull(dispatcher, fromNetid, event, eventid, filter, objectid)

	local flags = Events.decode(KEP_EventDecodeString(event))

	FlagSystem:resetFlags(flags)

end

function onFlagReturned(dispatcher, fromNetid, event, eventid, filter, objectid )
	local PID = KEP_EventDecodeNumber(event)
	if KEP_EventMoreToDecode(event) ~= 0 then
		flag = Events.decode(KEP_EventDecodeString(event))
		FlagSystem:addFlag(flag)
		return
	end
	FlagSystem:removeFlag(PID)
end

-------------------
-- Local Functions
-------------------

-- Counts up ammo from s_tInventory
countAmmo = function()
	s_tAmmoCount = {}
	-- Extract toolbar inventory info
	for i, item in ipairs(s_tInventory) do
		-- Create ammoCount fields for weapons with valid ammoTypes
		if item.properties.itemType == ITEM_TYPES.WEAPON and item.properties.ammoType then
			s_tAmmoCount[item.properties.ammoType] = 0
		end
	end
	
	-- Extract ammo counts for each weapon
	for ammoUNID, count in pairs(s_tAmmoCount) do
		for i, item in ipairs(s_tInventory) do
			if item.UNID and item.UNID == ammoUNID then
				s_tAmmoCount[ammoUNID] = s_tAmmoCount[ammoUNID] + item.count
			end
		end
	end
end

-- Clears current target
clearTarget = function()
	if s_sCurrentTargetID then
		s_sCurrentTargetID = nil
		for index, control in pairs(s_tHealthBar.target) do
			if control then
				-- Delete your target's HP Bar
				Dialog_RemoveControl(gDialogHandle, Control_GetName(control))
				s_tHealthBar.target[index] = nil
			end
		end
		Events.sendEvent("PLAYER_TARGET", {type = "Clear"})
		s_bHasFired = false
		--flushFloaters(s_sCurrentTargetID)
	end
end

-- Decrement an item from the designated slot
function decrementItem(slot)
	-- log("decrementItem")
	local item = s_tInventory[slot]
	if item then
		local iAmmoCount = s_tAmmoCount[item.UNID]
		if iAmmoCount then
			s_tAmmoCount[item.UNID] = iAmmoCount - 1
		end
		
		item.count = item.count - 1
		if item.count >= 1 then
			updateItem(slot, 0, item)
		else
			removeItem(slot, 0)
		end
	end
end

-- Gets a player by a netID
getPlayerNameByNetID = function(netID)
	if netID == nil then return nil end
	local playerObj = getPlayerObjectFromNetId(netID)
	
	-- Validate check this playerObj
	if KEP_ObjectIsValid(playerObj) == 0 then
		LogWarn("getPlayerNameByNetID() -  KEP_ObjectIsValid("..tostring(playerObj)..") returned 0 (invalid)")
		return nil
	end

	local playerName = GetSet_Safe_GetString(playerObj, MovementIds.NAME)
	if playerName == nil then
		LogWarn("getPlayerNameByNetID() - Failed to return playerName from GetSet_Safe_GetString("..tostring(playerObj)..", "..tostring(MovementIds.NAME)..")")
		return nil
	else
		return playerName
	end
end

-- Returns a player object from a netID
getPlayerObjectFromNetId = function(netId)
	if netId == nil then
		LogWarn("getPlayerObjectFromNetId() Received nil netId")
		return nil
	end
	local playerObj = KEP_GetPlayerByNetworkDefinedID(netId)

	if playerObj then
		if KEP_ObjectIsValid(playerObj) ~= 0 then
			return playerObj
		else
			LogWarn("getPlayerObjectFromNetId() KEP_ObjectIsValid("..tostring(playerObj)..") returned 0 (invalid)")
			return nil
		end
	else
		LogWarn("getPlayerObjectFromNetId() No playerObj returned from KEP_GetPlayerByNetworkDefinedID("..tostring(netId)..")")
		return nil
	end
end

local CENTER_OFFSET = 5
-- Detects if a target is visible or not
getVisible = function(targetObject, targetType)
	local targetPos = getWorldPosition(targetObject, targetType)
	targetPos.y = targetPos.y + CENTER_OFFSET
	
	-- Your World Position
	local x, y, z = KEP_GetPlayerPosition()
	y = y + CENTER_OFFSET
	
	local objTypeCenter, objIDCenter, rayDistanceCenter, targetDistanceCenter = 0, 0, 0, 0
	local vecDisplacement, vecTargetDirection, intersect = {}, {}, {}
	
	-- Trace to object's center
	vecDisplacement = Math.vecSub(targetPos, {x = x, y = y, z = z})
	vecTargetDirection, targetDistanceCenter = Math.vecNormalize(vecDisplacement)
	objTypeCenter, objIDCenter, intersect.x, intersect.y, intersect.z, rayDistanceCenter = KEP_RayTrace(x, y, z, vecTargetDirection.x, vecTargetDirection.y, vecTargetDirection.z, TRACE.WORLD + TRACE.DYNAMIC)
	
	-- Was the first object hit the target object?  If yes, then true.
	if (tostring(objIDCenter) == targetObject) or (rayDistanceCenter >= targetDistanceCenter) then
		return true, 0, 0
	else
		return false, objIDCenter, objTypeCenter
	end
end

-- Validates that an object is still . . . valid. . .
validateObject = function(object)
	local validObject = false
	if s_tTargetableObjects[object] then
		-- Check object
		if s_tTargetableObjects[object].type == ObjectType.DYNAMIC then
			local GLID = KEP_DynamicObjectGetGLID(tonumber(object))
			if GLID == 0 then
				LogWarn("validateObject("..tostring(object)..") failed. Failed to return GLID with KEP_DynamicObjectGetGLID()")
			else
				validObject = true
			end

		-- Check player
		elseif s_tTargetableObjects[object].type == ObjectType.PLAYER then
			if s_tTargetableObjects[object].playerObject == nil then
				LogWarn("validateObject("..tostring(object)..") failed. Player has no playerObject")
			else
				if KEP_ObjectIsValid(s_tTargetableObjects[object].playerObject) == 0 then
					LogWarn("validateObject("..tostring(object)..") failed. KEP_ObjectIsValid("..tostring(s_tTargetableObjects[object].playerObject)..") returned 0 (invalid)")
				else
					validObject = true
				end
			end
		end
	else
		LogWarn("validateObject("..tostring(object)..") failed. object["..tostring(object).."] has not been registered through FRAMEWORK_REGISTER_BATTLE_OBJECT")
	end
	local objectRegistered = objectRegistered(object)
	if not objectRegistered or not validObject then
		LogWarn("validateObject("..tostring(object)..") failed. Registered["..tostring(objectRegistered).."] Valid["..tostring(validObject).."]")
	end
	return validObject
end

-- Gets the World Position of a target
getWorldPosition = function(target, targetType)
	local targetPos = {x = 0, y = 0, z = 0}
	-- Different call for target type
	if targetType == ObjectType.PLAYER then
		targetPos.x, targetPos.y, targetPos.z = GetSet_Safe_GetVector(target, MovementIds.LASTPOSITION)
	elseif targetType == ObjectType.DYNAMIC then
		targetPos.x, targetPos.y, targetPos.z = KEP_DynamicObjectGetPositionAnim(target)
	end
	
	return targetPos
end

-- Returns if a target is in range or not and their range from the the attacker
targetInRange = function(targetObject, targetType, range, angle)
	-- Get your position
	local playerPos = {}
	playerPos.x, playerPos.y, playerPos.z, playerPos.r = KEP_GetPlayerPosition()
	
	local inAngle = false
	
	local success, hitX, hitY, hitZ, distance = KEP_SphereTestObject(targetType, targetObject, playerPos.x, playerPos.y, playerPos.z, range, playerPos.r, angle)
	local inRange = success ~= 0

	inAngle = inRange

	-- if inRange then
	-- 	local targetVector = {}
	-- 	targetVector.x = playerPos.x - hitX
	-- 	targetVector.y = playerPos.y - hitY
	-- 	targetVector.z = playerPos.z - hitZ
		
	-- 	local targetAngle = math.atan2(targetVector.x, targetVector.z)
	-- 	targetAngle = math.deg(targetAngle)
		
	-- 	if playerPos.r >= 0 and playerPos.r <= 90 then
	-- 		if math.abs(playerPos.r - targetAngle) < angle then
	-- 			inAngle = true
	-- 		end
	-- 	end
	-- 	if playerPos.r > 90 and playerPos.r <= 180 then
	-- 		if math.abs(math.abs(playerPos.r) - targetAngle) < angle then
	-- 			inAngle = true
	-- 		end
	-- 	end
	-- 	if playerPos.r > 180 and playerPos.r <= 240 then
	-- 		if math.abs(math.abs(playerPos.r - 360) + targetAngle) < angle then
	-- 			inAngle = true
	-- 		end
	-- 	end
	-- 	if playerPos.r > 240 and playerPos.r <= 360 then
	-- 		if math.abs((360 - playerPos.r ) + targetAngle) < angle then
	-- 			inAngle = true
	-- 		end
	-- 	end
	-- end
	
	return inRange, inAngle
end

-- Attempt to use a tool
attemptTool = function()
	-- log("attemptTool")
	-- Does the equipped item require a target?
	local requiresTarget = true
	if s_tEquippedItem and s_tEquippedItem.properties and s_tEquippedItem.properties.requiresTarget then
		if s_tEquippedItem.properties.requiresTarget == "false" then
			requiresTarget = false
		else
			requiresTarget = true
		end
	end

	local targetObject
	local targetAlive
	local currentHP
	local maxHP
	local targetItemType
	local tTargetedObject = s_sCurrentTargetID and s_tTargetableObjects[s_sCurrentTargetID]
	if requiresTarget then
		targetObject 		= s_sCurrentTargetID
		targetAlive 		= tTargetedObject.alive
		currentHP	  		= tTargetedObject.health
		maxHP		   		= tTargetedObject.maxHealth
		targetItemType		= tTargetedObject.itemType
	end

	local healingWeapon = false
	local repairTool = false
	local paintWeapon = false
	local emoteItem = false
	local p2pEmoteItem = false
	local tItemProperties = s_tEquippedItem and s_tEquippedItem.properties
	if tItemProperties then
		healingWeapon	= tItemProperties.healingWeapon == "true"
		repairTool		= tostring(tItemProperties.repairTool) == "true" or false
		paintWeapon		= tItemProperties.paintWeapon == "true"
		emoteItem		= tItemProperties.toolType == "emoteItem"
		p2pEmoteItem	= tItemProperties.toolType == "p2pEmoteItem"
	end
	local durabilityType = (tItemProperties and tItemProperties.durabilityType) or "repairable"

	local canFire = true

	local sTargetType = "Object"

	if requiresTarget then	
		-- targetObject for players is their playerObject
		if s_tTargetableObjects[s_sCurrentTargetID].type == ObjectType.PLAYER then
			targetObject = s_tCurrentTargetObject
			sTargetType = "Player"
		-- can't emote with anything but players
		elseif p2pEmoteItem then
			canFire = false
		end
	end
		
	local needsAmmo = (s_tEquippedItem.properties.ammoType ~= nil) and (s_tEquippedItem.properties.ammoType ~= 0)
	local ammoCount = 0

	if needsAmmo then
		ammoCount = s_tAmmoCount[s_tEquippedItem.properties.ammoType]
		canFire = ammoCount >= 1
	end
	
	if requiresTarget then
		if s_tTargetableObjects[s_sCurrentTargetID].master and s_tTargetableObjects[s_sCurrentTargetID].master == s_sPlayerName then
			canFire = false
		end
	end

	-- If this item's durability is 0
	if s_tEquippedItem.instanceData and s_tEquippedItem.instanceData.durability and s_tEquippedItem.instanceData.durability == 0 then
		-- Let the player know that their tool is broken
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, "Your tool is broken.")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
		clearTarget()
		canFire = false
	end

	local currentTime = KEP_CurrentTime()
	
	-- Requires a target

	-- If we're ready to fire (against a target)
	if canFire and (currentTime >= s_tEquippedItem.cooldownTime) and (targetAlive or not requiresTarget) then
		-- Check range and visibility against a target
		if requiresTarget then
			local target
			if s_tTargetableObjects[s_sCurrentTargetID].type == ObjectType.DYNAMIC then
				target = targetObject
			elseif s_tTargetableObjects[s_sCurrentTargetID].type == ObjectType.PLAYER then
				target = s_tTargetableObjects[s_sCurrentTargetID].netId
			end
			local inAngle, inRange = targetInRange(target, s_tTargetableObjects[s_sCurrentTargetID].type, s_tEquippedItem.properties.range, s_tEquippedItem.properties.coneAngle)
			-- Out of range
			if (not inRange) and (s_dOutOfRangeCooldownTimer <= 0) then
				displayOutOfRangeMessage()
				s_dOutOfRangeCooldownTimer = OUT_OF_RANGE_COOLDOWN

			-- If angle and range passed
			elseif (inAngle and inRange) then
				-- Check visibility
				local visible = getVisible(targetObject, s_tTargetableObjects[s_sCurrentTargetID].type)
				-- If we're ready to fire
				if visible then
					-- Turn on cooldown
					s_tEquippedItem.cooldownTime = currentTime + (s_tEquippedItem.properties.rateOfFire * 1000)
					
					if needsAmmo then
						local iAmmoType = s_tEquippedItem.properties.ammoType
						-- Remove ammo from inventory
						for i, tItem in ipairs(s_tInventory) do
							-- Get first ammo slot in inventory for updating
							if tItem.UNID == iAmmoType then
								decrementItem(i)
								break
							end
						end
					end
					
					updateDirty()
					
					-- Inform Framework_Toolbar that we're firing so it can queue the attack flash and cooldown
					local ev = KEP_EventCreate("FRAMEWORK_ATTACK_FLASH")
					KEP_EventEncodeString(ev, Events.encode(s_tEquippedItem))
					KEP_EventQueue(ev)

					-- Inform the server of the attack
					-- KEP_Log("Send ATTACK event (Tool, Targeted)")
					if paintWeapon then
						sTargetType = "Paint"
						Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sCurrentTargetID, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
					elseif emoteItem then
						sTargetType = "Emote"
						if s_bIsEmoteEnabled then
							disableActiveEmote()
						else
							Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sCurrentTargetID, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
						end
					elseif p2pEmoteItem then
						sTargetType = "P2PEmote"
						if s_bIsEmoteEnabled then
							disableActiveEmote()
						else
							Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sCurrentTargetID, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
						end
					else
						if not checkToolRequirements() then
							return
						end
						Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sCurrentTargetID, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
					end
					
					decrementWeaponDurability()
				end
			end
		
		-- Does NOT require a target
		else

			-- Turn on cooldown
			s_tEquippedItem.cooldownTime = currentTime + (s_tEquippedItem.properties.rateOfFire * 1000)
			
			decrementWeaponDurability()

			if needsAmmo then
				local iAmmoType = s_tEquippedItem.properties.ammoType
				-- Remove ammo from inventory
				for i, tItem in ipairs(s_tInventory) do
					-- Get first ammo slot in inventory for updating
					if tItem.UNID == iAmmoType then
						decrementItem(i)
						break
					end
				end
			end
			
			updateDirty()
			
			-- Inform Framework_Toolbar that we're firing so it can queue the attack flash and cooldown
			local ev = KEP_EventCreate("FRAMEWORK_ATTACK_FLASH")
			KEP_EventEncodeString(ev, Events.encode(s_tEquippedItem))
			KEP_EventQueue(ev)

			-- Inform the server of the attack
			-- KEP_Log("Send ATTACK event (tool, untargeted)")
			if paintWeapon then
				sTargetType = "Paint"
				Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sPlayerName, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
			elseif emoteItem then
				sTargetType = "Emote"
				if s_bIsEmoteEnabled then
					disableActiveEmote()
				else
					Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sPlayerName, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
				end
			elseif p2pEmoteItem then
				sTargetType = "P2PEmote"
				if s_bIsEmoteEnabled then
					disableActiveEmote()
				else
					Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sPlayerName, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
				end
			else
				Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sPlayerName, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
			end
		end
	end
	
	if s_sFiringMethod ~= "AutoFire" then
		s_bHasFired = false
	end

end

-- Attempt to use a tool
attemptGeneric = function()
	-- log("attemptGeneric")
	
	sTargetType = "Generic"

	local currentTime = KEP_CurrentTime()

	-- If we're ready to fire (against a target)
	if currentTime >= s_tEquippedItem.cooldownTime  then
		-- Check range and visibility against a target
		local target
		local tTargetedObject = s_tTargetableObjects[s_sCurrentTargetID]
		if tTargetedObject.type == ObjectType.DYNAMIC then
			target = s_sCurrentTargetID
		elseif tTargetedObject.type == ObjectType.PLAYER then
			target = tTargetedObject.netId
		end
		local inAngle, inRange = targetInRange(target, tTargetedObject.type, MIN_INTERACTION_DISTANCE, 90)
		
		-- Out of range
		if (not inRange) and (s_dOutOfRangeCooldownTimer <= 0) then
			displayOutOfRangeMessage()
			s_dOutOfRangeCooldownTimer = OUT_OF_RANGE_COOLDOWN

		-- If angle and range passed
		elseif (inAngle and inRange) then
			-- Check visibility
			local visible = getVisible(s_sCurrentTargetID, tTargetedObject.type)
			-- If we're ready to fire
			if visible then
				-- Inform the server of the attack
				-- KEP_Log("Send ATTACK event (Generic, Targeted) at "..tostring(s_sCurrentTargetID))
				Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sCurrentTargetID, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
			end
		end
	end
	
	if s_sFiringMethod ~= "AutoFire" then
		s_bHasFired = false
	end

end

-- Attempt to shoot
attemptFire = function()
	-- log("attemptFire")

	local tCurrentTargetObject = s_tTargetableObjects[s_sCurrentTargetID]
	if not tCurrentTargetObject then
		clearTarget()
		return
	end
	local targetObject 		= s_sCurrentTargetID
	local targetAlive 		= tCurrentTargetObject.alive
	local currentHP	  		= tCurrentTargetObject.health
	local maxHP		   		= tCurrentTargetObject.maxHealth
	local targetItemType	= tCurrentTargetObject.itemType

	--log("--- targetItemType : ".. tostring(targetItemType))

	if targetAlive == false or currentHP == 0 then
		clearTarget()
		return
	end
	
	local healingWeapon = false
	local repairTool = false
	local paintWeapon = false
	if s_tEquippedItem and s_tEquippedItem.properties then
		healingWeapon = s_tEquippedItem.properties.healingWeapon == "true"
		repairTool = tostring(s_tEquippedItem.properties.repairTool) == "true" or false
		paintWeapon = s_tEquippedItem.properties.paintWeapon == "true"
	end
	local durabilityType = s_tEquippedItem.properties.durabilityType or "repairable"

	local sTargetType = "Object"
	
	-- targetObject for players is their playerObject
	if tCurrentTargetObject.type == ObjectType.PLAYER then
		targetObject = s_tCurrentTargetObject
		sTargetType = "Player"
	end
	
	-- Don't attempt to heal a target that's at full health
	if (sTargetType == "Player") and healingWeapon and (currentHP >= maxHP) then return end
	if (sTargetType == "Object") and repairTool then
		if targetItemType ~= "placeable" then
			clearTarget()
			return
		elseif(currentHP >= maxHP) then
			local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeString(statusEvent, s_tGameItems[tostring(tCurrentTargetObject.UNID)].name .. " is already fully repaired")
			KEP_EventEncodeNumber(statusEvent, 3)
			KEP_EventEncodeNumber(statusEvent, 255)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventEncodeNumber(statusEvent, 0)
			KEP_EventSetFilter(statusEvent, 4)
			KEP_EventQueue(statusEvent)
			clearTarget()
			return 
		end
	end
	
	local canFire = true
	local needsAmmo = (s_tEquippedItem.properties.ammoType ~= nil) and (s_tEquippedItem.properties.ammoType ~= 0)
	local ammoCount = 0

	if needsAmmo then
		ammoCount = s_tAmmoCount[s_tEquippedItem.properties.ammoType]
		canFire = ammoCount >= 1
	end
	
	if tCurrentTargetObject.master and tCurrentTargetObject.master == s_sPlayerName then
		canFire = false
		clearTarget()
		return
	end

	-- If this item's durability is 0
	if s_tEquippedItem.instanceData and s_tEquippedItem.instanceData.durability and s_tEquippedItem.instanceData.durability == 0 then
		-- Let the player know that their weapon is broken
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, "Your weapon is broken.")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
		clearTarget()
		canFire = false
	end

	local currentTime = KEP_CurrentTime()
	-- If we're ready to fire
	if canFire and (currentTime >= s_tEquippedItem.cooldownTime) and targetAlive then
		-- Check range and visibility
		local target
		if tCurrentTargetObject.type == ObjectType.DYNAMIC then
			target = targetObject
		elseif tCurrentTargetObject.type == ObjectType.PLAYER then
			target = tCurrentTargetObject.netId
		end
		local inAngle, inRange = targetInRange(target, tCurrentTargetObject.type, s_tEquippedItem.properties.range, s_tEquippedItem.properties.coneAngle)
		-- Out of range
		if (not inRange) and (s_dOutOfRangeCooldownTimer <= 0) then
			displayOutOfRangeMessage()
			s_dOutOfRangeCooldownTimer = OUT_OF_RANGE_COOLDOWN

		-- If angle and range passed
		elseif inAngle and inRange then
			-- Check visibility
			local visible = getVisible(targetObject, tCurrentTargetObject.type)
			-- If we're ready to fire
			if visible then
				-- Turn on cooldown
				s_tEquippedItem.cooldownTime = currentTime + (s_tEquippedItem.properties.rateOfFire * 1000)

				if needsAmmo then
					local iAmmoType = s_tEquippedItem.properties.ammoType
					-- Remove ammo from inventory
					for i, slot in ipairs(s_tInventory) do
						-- Get first ammo slot in inventory for updating
						if slot.UNID == iAmmoType then
							decrementItem(i)
							break
						end
					end
				end
				
				updateDirty()
				
				-- Inform Framework_Toolbar that we're firing so it can queue the attack flash and cooldown
				local ev = KEP_EventCreate("FRAMEWORK_ATTACK_FLASH")
				KEP_EventEncodeString(ev, Events.encode(s_tEquippedItem))
				KEP_EventQueue(ev)

				-- Inform the server of the attack
				-- KEP_Log("Send ATTACK event (weapon)")
				if paintWeapon then
					sTargetType = "Paint"
					Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sCurrentTargetID, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
				elseif emoteItem then
					sTargetType = "Emote"
					if s_bIsEmoteEnabled then
						disableActiveEmote()
					else
						Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sCurrentTargetID, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
					end
				elseif p2pEmoteItem then
					sTargetType = "P2PEmote"
					if s_bIsEmoteEnabled then
						disableActiveEmote()
					else
						Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sCurrentTargetID, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
					end
				else
					if not checkToolRequirements() then
						return
					end
					Events.sendEvent("FRAMEWORK_ATTACK", {type = sTargetType, attacker = s_sPlayerName, attacked = s_sCurrentTargetID, weapon = s_tEquippedItem, weaponSlot = s_iEquippedItemSlot, itemType = targetItemType})
				end
				
				decrementWeaponDurability()
			end
		end
	end
	
	if s_sFiringMethod ~= "AutoFire" then
		s_bHasFired = false
	end

	-- throw a status message that there isn't any more ammo
	if needsAmmo == true and ammoCount == 0 and canFire == false and s_sCurrentTargetID then
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeString(statusEvent, "You are out of ammo.")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
		clearTarget()
	end 

end


checkToolRequirements = function()
	-- log("checkToolRequirements")
	--In case of player or invalid object
	if not s_tTargetableObjects[s_sCurrentTargetID] or not s_tTargetableObjects[s_sCurrentTargetID].UNID then
		return true 
	end
	local attackedItem = s_tGameItems[tostring(s_tTargetableObjects[s_sCurrentTargetID].UNID)]
	if attackedItem then
		local requiredUNID = tonumber(attackedItem.requiredTool) or (attackedItem.behaviorParams and tonumber(attackedItem.behaviorParams.requiredTool))
		if requiredUNID and requiredUNID > 0 then
			if tonumber(requiredUNID) ~= tonumber(s_tEquippedItem.UNID) then
				local requiredItem = s_tGameItems[tostring(requiredUNID)]
				local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventEncodeString(statusEvent, tostring(requiredItem.name).." required to harvest "..tostring(attackedItem.name)..".")
				KEP_EventEncodeNumber(statusEvent, 3)
				KEP_EventEncodeNumber(statusEvent, 255)
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventSetFilter(statusEvent, 4)
				KEP_EventQueue(statusEvent)
				clearTarget()
				return false
			end
		end
	end	
	
	return true
end

-- Checks an ID and returns if it's been fully registered or not
objectRegistered = function(ID)
	local objectRegistered = true
	local tObject = s_tTargetableObjects[ID]
	if tObject then
		-- Does this object have all its needed fields before it can be used?
		for _, field in pairs(ESSENTIAL_OBJECT_FIELDS[tObject.type]) do
			if tObject[field] == nil then
				LogWarn("objectRegistered("..tostring(ID)..") failed registration. It is missing field["..tostring(field).."]")
				objectRegistered = false
				break
			end
		end
	end
	return objectRegistered
end

checkFlagLocation = function(targetObject)
	local targetPos = {x = 0, y = 0, z = 0}
	targetPos.x, targetPos.y, targetPos.z = KEP_DynamicObjectGetPositionAnim(targetObject)

	return FlagSystem:getDominantFlagSetting("destruction", s_tWorldSettings.destructibleObjects, targetPos.x, targetPos.y, targetPos.z )
end

checkPVPFlagLocation = function(targetObject)
	local targetPos = {x = 0, y = 0, z = 0}
	targetPos.x, targetPos.y, targetPos.z = KEP_DynamicObjectGetPositionAnim(targetObject)

	return FlagSystem:getDominantFlagSetting("pvp", s_tWorldSettings.allowPvP, targetPos.x, targetPos.y, targetPos.z )
end

-- Decrement weapon durability
function decrementWeaponDurability()
	-- log("decrementWeaponDurability")
	local iSlot = s_iEquippedItemSlot - 1
	local tItem = s_tInventory[iSlot]
	if tItem and tItem.UNID == s_tEquippedItem.UNID then
		local weaponData = tItem
		if weaponData.instanceData then
			-- Don't reduce an indestructible item
			local durabilityType = weaponData.properties.durabilityType or "repairable"
			if durabilityType ~= "indestructible" then
				weaponData.instanceData.durability = weaponData.instanceData.durability - DURABILITY_PER_ATTACK
			end
			-- Don't go below zero
			if weaponData.instanceData.durability < 0 then
				weaponData.instanceData.durability = 0
			end
			
			-- s_tEquippedItem.instanceData.durability = weaponData.instanceData.durability
			updateItem(iSlot, 0, weaponData)

			-- Remove certain broken items
			if weaponData.instanceData.durability == 0 then
				-- Remove broken tools
				if weaponData.properties and weaponData.properties.itemType == "tool" then
					removeItem(iSlot, INVENTORY_PID)
					if s_bIsWeaponEquipped then
						-- Tell Framework_Inventory to un-equip this weapon
						Events.sendEvent("UNEQUIP_PLAYER_ITEM", {UNID = s_tEquippedItem.UNID, weapon = true})
						s_bIsWeaponEquipped = false
						-- Inform Framework_Cursor_HighlightHandler of what's going on
						local ev = KEP_EventCreate("FRAMEWORK_WEAPON_EQUIPPED")
						local eventInfo = {["equipped"] = false}
						KEP_EventEncodeString(ev, Events.encode(eventInfo))
						KEP_EventQueue(ev)	
						m_equippedUNID = nil
					end
					updateDirty()
				end
			end
		end
	end
end

-- Generates a floating notification text over a target
function generateFloater(floatText, RGB, targetID)
	local targetObjectID = targetID
	targetID = tostring(targetID)
	s_iFloaterCount = s_iFloaterCount + 1
	local floaterHandle = Dialog_AddStatic(gDialogHandle, "floater"..s_iFloaterCount, floatText, 500, -500, FLOATER_CONTROL_WIDTH, 500)
	
	-- Set the color for the floater
	local floaterElementHandle = Static_GetDisplayElement(floaterHandle)
	BlendColor_Init(Element_GetFontColor(floaterElementHandle), RGB, RGB, RGB)
	BlendColor_SetColor(Element_GetFontColor(floaterElementHandle), 0, RGB)
	
	-- Generate static stroke for this floater
	local strokes
	local textFormat = 1 -- top-centered
	local floatLife = TARGET_FLOATER_DAMAGE_DURATION
	
	local tTargetObject = s_tTargetableObjects[targetID]
	
	if tTargetObject.type == ObjectType.PLAYER then
		targetObjectID = tTargetObject.netId
		if targetObjectID == s_iNetID then
			floatLife = SELF_FLOATER_DURATION
		end
	elseif tTargetObject.type == ObjectType.PLAYER then
		if string.lower(floatText) == "miss" then
			floatLife = TARGET_FLOATER_MISS_DURATION
		end
	end
	
	Element_SetTextFormat(floaterElementHandle, textFormat)
	strokes = strokeTheString(floaterHandle, BLACK_RGB, textFormat)
	
	local floaterTable = {
		handle			= floaterHandle,
		strokeHandles	= strokes,
		decay			= 0,
		floatLife		= floatLife,
		color			= {
			a = RGB.a,
			r = RGB.r,
			g = RGB.g,
			b = RGB.b
		},
		text			= floatText,
		targetObjectID	= targetObjectID,
		id				= targetID,
		targetType		= s_tTargetableObjects[targetID].type
	}
	table.insert(s_tFloaters, floaterTable)
end

-- Places a floater according to target's location
function placeFloater(floater, menuX, menuY)
	local targetObject = floater.targetObjectID
	if floater.targetType == ObjectType.PLAYER then
		targetObject = getPlayerObjectFromNetId(targetObject)
	end
	-- Delete floaters if this playerObject is now gone
	if targetObject == nil then
		-- Log("placeFloater() - Flushing floaters for ID["..tostring(floater.id).."] since no targetObject could be acquired for it")
		flushFloaters(floater.id)
		return
	end
	
	-- Is this floater for the target or the current player?
	local targetPos = getWorldPosition(targetObject, floater.targetType)
	if targetPos == nil then return end
	
	local floaterStartPos = getHeadPos(targetPos, targetObject, floater.targetType)
	
	local distScale = ((10/floaterStartPos.z) - 10)
	if (floaterStartPos.x == nil) or (floaterStartPos.y == nil) or (floaterStartPos.z == nil) then return end
	floaterStartPos.x = floaterStartPos.x + distScale

	-- If the player is behind you, don't do anything
	if tonumber(floaterStartPos.z) >= 1 then return end
	
	-- Resize the HP box based on distance to the camera
	local fontSize = ((1000 / floaterStartPos.z) - 1000) * .6
	-- Enforce a min font size
	fontSize = math.max(fontSize, MIN_FLOATER_FONT_SIZE)
	-- Enforce a max font size
	fontSize = math.min(fontSize, MAX_FLOATER_FONT_SIZE)
	if fontSize > 0 then fontSize = -fontSize end
	
	local floaterWidth  = FLOATER_CONTROL_WIDTH
	local floaterHeight = Static_GetStringHeight(floater.handle, floater.text)
	
	local startingFont, endingFontSize
	
	-- Position the floater
	local floaterX = (-menuX+floaterStartPos.x)-(floaterWidth/2)
	local floaterY = (-menuY+floaterStartPos.y)-(floaterHeight*BAR_HEIGHT_RATIO)
	
	-- Floaters animate differently depending on who they're for
	if s_sSelfObject == targetObject then
		-- Scale up player's font size a bit
		startingFont = fontSize
		endingFontSize = (startingFont*1.53) - startingFont
		fontSize = Easing.linear(floater.decay, startingFont, endingFontSize, floater.floatLife)
		
		local floaterEndPos = getHeadPos(targetPos, targetObject, floater.targetType, -2)
		local startingPoint = (-menuY + floaterStartPos.y)
		local endingPoint = (-menuY + floaterEndPos.y) - startingPoint
		floaterY = Easing.linear(floater.decay, startingPoint, endingPoint, floater.floatLife)
	-- Floater for a target
	else
		-- Handle miss floaters
		if string.lower(floater.text) == "miss" then
			startingFont = fontSize
			endingFontSize = (fontSize*1.2) - startingFont
			fontSize = Easing.linear(floater.decay, startingFont, endingFontSize, floater.floatLife)
			
			local startingPoint = floaterY - (floaterHeight/2)
			local endingPoint = (floaterY - floaterHeight - floaterHeight) - startingPoint
			floaterY = Easing.linear(floater.decay, startingPoint, endingPoint, floater.floatLife)
		-- Handle target damage offsets
		else
			startingFont = fontSize * 3
			endingFontSize = fontSize - startingFont
			fontSize = Easing.linear(floater.decay, startingFont, endingFontSize, floater.floatLife)
			
			local startingPoint = floaterY - floaterHeight - (floaterHeight/2)
			local endingPoint = (floaterY - (floaterHeight/2)) - startingPoint
			floaterY = Easing.linear(floater.decay, startingPoint, endingPoint, floater.floatLife)
		end
	end
	
	Element_AddFont(Static_GetDisplayElement(floater.handle), gDialogHandle, DEFAULT_FONT, fontSize, FLOATER_CONTROL_WIDTH, FLOATER_ITALICS)
	
	Control_SetSize(floater.handle, floaterWidth, floaterHeight)

	Control_SetLocation(floater.handle, floaterX, floaterY)
	
	-- Floater's alpha progresses to 100% in 20% of the floater's animation lifecycle
	if floater.decay/floater.floatLife <= .5 then
		floater.color.a = Easing.linear(floater.decay, 0, 255, floater.floatLife * .5)
	-- It then animates back down to 0% over the next 80% of it's animation lifecycle
	else
		local floatBase = floater.decay - (floater.floatLife * .5)
		floater.color.a = Easing.linear(floatBase, 255, -255, floater.floatLife * .5)
	end
	BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(floater.handle)), 0, floater.color)
	
	-- Adjust strokes
	local strokeColor = BLACK_RGB
	strokeColor.a = math.max(0, floater.color.a/3)
	syncTheStrokes(floater.strokeHandles, floater.handle, DEFAULT_FONT, fontSize, FLOATER_CONTROL_WIDTH, FLOATER_ITALICS, strokeColor)
end

-- Returns the head position of an object based on its type and location
function getHeadPos(targetPos, targetObject, targetType, yOffset)
	local headPos = {}
	-- Get object's position based on type filter
	if targetType == ObjectType.PLAYER then
		if yOffset then
			targetPos.y = targetPos.y + yOffset
		end
		-- Offset notifications for yourself differently than for targets
		if targetObject == s_sSelfObject then
			headPos.x, headPos.y, headPos.z = KEP_WorldToScreen(targetPos.x, targetPos.y + SELF_FLOATER_Y_OFFSET, targetPos.z)
		else
			headPos.x, headPos.y, headPos.z = KEP_WorldToScreen(targetPos.x, targetPos.y + TARGET_PLAYER_FLOATER_Y_OFFSET, targetPos.z)
		end
		
	elseif targetType == ObjectType.DYNAMIC then
		-- bottom-right and upper-left points of object's bounding box
		local x,y,z,x2,y2,z2 = KEP_DynamicObjectGetBoundingBox(targetObject)

		if yOffset then
			y2 = y2 + yOffset
		end

		if (y2 - y) > OBJECT_INDICATOR_BOUND_Y then
			y2 =  y + OBJECT_INDICATOR_BOUND_Y
		end
	
		headPos.x, headPos.y, headPos.z = KEP_WorldToScreen(targetPos.x, y2, targetPos.z)
	end
	return headPos
end

-- Handles floaters on frame render
function handleFloaters(fElapsedTime)
	local iFloaterCount = #s_tFloaters
	if iFloaterCount > 0 then
		local locThis = MenuGetLocationThis()
		local menuX = locThis.x
		local menuY = locThis.y
		for i = iFloaterCount, 1, -1 do
			local tFloater = s_tFloaters[i]
			if tFloater then
				if tFloater.decay then
					tFloater.decay = tFloater.decay + fElapsedTime
				end
				
				-- If this floater's done floatin', delete it
				if (tFloater.floatLife > tFloater.decay) then
					placeFloater(tFloater, menuX, menuY)
				else
					-- Flush stroke statics
					for name, strokeHandle in pairs(tFloater.strokeHandles) do
						Dialog_RemoveControl(gDialogHandle, Control_GetName(strokeHandle))
					end
					Dialog_RemoveControl(gDialogHandle, Control_GetName(tFloater.handle))
					table.remove(s_tFloaters, i)
				end
			end
		end
	end
end

-- Handles the placement of health bars over the current target
function handleHealthBars(fElapsedTime)
	-- Do we have a weapon equipped and a target set?
	if s_bIsWeaponEquipped and s_sCurrentTargetID then
		-- If using a weapon (not a tool)
		if s_tEquippedItem and s_tEquippedItem.properties and s_tEquippedItem.properties.itemType and s_tEquippedItem.properties.itemType == "weapon" then
			local targetObject		= s_sCurrentTargetID
			local tTargetedObject	= s_tTargetableObjects[s_sCurrentTargetID]
			local objectType		= tTargetedObject.type
			
			-- Targeting an object
			if objectType == ObjectType.PLAYER then
				targetObject = s_tCurrentTargetObject
				if targetObject == nil then
					updateNetIds()
					return
				end
			end
			
			-- Detect if the player is visible
			local visible, rayObject, rayType = getVisible(targetObject, objectType)
			-- If a player is visible
			if visible then
				-- Reset target break cooldown
				s_dVisibleCooldownTimer = TARGET_BREAK_TIMER
				-- Generate health bar elements
				if s_tHealthBar.target.frame == nil then
					s_tHealthBar.target.frame = Dialog_AddImage(gDialogHandle, "TargetFrameHandle", -500,-500, "Framework_HudElements.tga", 0, 262, 258, 294)
				end
				if s_tHealthBar.target.bar == nil then
					s_tHealthBar.target.bar = Dialog_AddImage(gDialogHandle, "TargetBarHandle", -500, -500, "Framework_HudElements.tga", 260, 261, 261, 279)
				end
				if s_tHealthBar.target.level == nil and (tTargetedObject.level or tTargetedObject.armorRating) then
					local targetLevel = tTargetedObject.level
					if objectType == ObjectType.PLAYER then
						targetLevel = getLevelOfPlayer(tTargetedObject.armorRating)
					end
					local levelCoords = getLevelCoords(targetLevel)
					s_tHealthBar.target.level = Dialog_AddImage(gDialogHandle, "TargetLevelHandle", -500, -500, "Framework_HudElements.tga", unpack(levelCoords))
				end
				-- Position the generated bars
				placeHPBar("target", targetObject, objectType)
			-- If they're out of range
			else
				-- Hide the HP bar
				for index, control in pairs(s_tHealthBar.target) do
					if control then
						Control_SetVisible(control, false)
					end
				end
				s_bIsHealthBarHidden = true
				
				s_dVisibleCooldownTimer = s_dVisibleCooldownTimer - fElapsedTime
				
				if s_dVisibleCooldownTimer <= 0 then
					clearTarget()
				end
			end
		end
	end
end

-- Places the Health Bar for the current target or moused over target
function placeHPBar(barType, targetObject, targetType)
	local targetPos = getWorldPosition(targetObject, targetType)
	local headPos = getHeadPos(targetPos, targetObject, targetType)
	-- Return if failed to get target's position
	if (headPos.x == nil) or (headPos.y == nil) or (headPos.z == nil) then return end
	
	local tTargetedObject = s_tTargetableObjects[s_sCurrentTargetID]
	local currentHP = tTargetedObject.health
	local maxHP 	= tTargetedObject.maxHealth
	
	-- If using the paintbrush
	local paintWeapon = false
	if s_tEquippedItem.properties.paintWeapon == "true" then
		paintWeapon = true
	end

	-- If untargetable
	local untargetable = false
	local tObject = s_tTargetableObjects[targetObject]
	if tObject ~= nil and (tObject.targetable == nil or (tObject.targetable ~= "true" and tObject.targetable ~= true)) then
		untargetable = true
	end

	-- If the player is behind you, if using a paintbrush, or if untargetable, don't show health bar
	if tonumber(headPos.z) >= 1 or paintWeapon or untargetable then
		for index, control in pairs(s_tHealthBar[barType]) do
			if control then
				Control_SetVisible(control, false)
			end
		end
		s_bIsHealthBarHidden = true
		return
	end
	
	if s_bIsHealthBarHidden then
		for index, control in pairs(s_tHealthBar[barType]) do
			if control then
				Control_SetVisible(control, true)
			end
		end
		s_bIsHealthBarHidden = false
	end
	
	local locThis = MenuGetLocationThis()
	local menuX = locThis.x
	local menuY = locThis.y
	
	-- Resize the HP box based on distance to the camera
	local fontSize = (1000 / headPos.z) - 1000
	if fontSize < 0 then
		fontSize = -fontSize
	end
	fontSize = math.min(fontSize,100)
	
	-- Set Frame for bar
	local frameWidth = fontSize*BAR_SIZE
	local frameHeight
	-- Set bar width within min and max ranges
	frameWidth = math.max(HEALTH_BAR_SIZES.MIN.width, math.min(frameWidth, HEALTH_BAR_SIZES.MAX.width))
	frameHeight = frameWidth*BAR_HEIGHT_RATIO
	local frameX = (-menuX+headPos.x)-(frameWidth/2)
	local frameY = (-menuY+headPos.y)-frameHeight
	Control_SetSize(s_tHealthBar[barType].frame, frameWidth, frameHeight)
	Control_SetLocation(s_tHealthBar[barType].frame, frameX, frameY)
	
	-- Set Health bar
	local maxBarWidth = frameWidth * HEALTH_BAR_WIDTH_RATIO
	local barHeight = frameHeight * HEALTH_BAR_HEIGHT_RATIO
	local barOffsetX = frameWidth * HEALTH_BAR_X_OFFSET
	local barOffsetY = frameHeight * HEALTH_BAR_Y_OFFSET
	Control_SetSize(s_tHealthBar[barType].bar, (maxBarWidth * (currentHP/maxHP)), barHeight)
	Control_SetLocation(s_tHealthBar[barType].bar, frameX + barOffsetX, frameY + barOffsetY)
	
	if s_tHealthBar[barType].level then
		-- Position level
		local levelWidth = frameWidth - maxBarWidth
		local levelHeight = frameHeight
		local levelOffsetX = frameWidth * LEVEL_X_OFFSET
		local levelOffsetY = frameHeight * LEVEL_Y_OFFSET
		Control_SetSize(s_tHealthBar[barType].level, levelWidth, levelHeight)
		Control_SetLocation(s_tHealthBar[barType].level, frameX + levelOffsetX, frameY + levelOffsetY)
	end
	-- Move health bar frame to the back
	-- Push these statics to the back
	Dialog_MoveControlToBack(gDialogHandle, s_tHealthBar.target.frame)
end

-- Flush all visible floaters immediately
function flushFloaters(floaterID)
	for i = #s_tFloaters, 1, -1 do
		local tFloater = s_tFloaters[i]
		if floaterID then
			if tFloater.id == floaterID then
				-- Flush stroke statics
				for name, strokeHandle in pairs(tFloater.strokeHandles) do
					Dialog_RemoveControl(gDialogHandle, Control_GetName(strokeHandle))
				end
				Dialog_RemoveControl(gDialogHandle, Control_GetName(tFloater.handle))
				table.remove(s_tFloaters, i)
			end
		else
			-- Flush stroke statics
			for name, strokeHandle in pairs(tFloater.strokeHandles) do
				Dialog_RemoveControl(gDialogHandle, Control_GetName(strokeHandle))
			end
			Dialog_RemoveControl(gDialogHandle, Control_GetName(tFloater.handle))
			table.remove(s_tFloaters, i)
		end
	end
end

-- Find a player's netId based on their name
function updateNetIds()
	-- Get all net Ids in the current zone
	local netIds = KEP_GetAllNetIds()
	local checkedPlayers = {}
	
	local valueStart, value
	local valueEnd = 0
	
	-- Extract all netIds
	valueStart, valueEnd, value = string.find(netIds, "[^%s]+", valueEnd)
	if valueEnd == nil then return end
	-- For each netId, check if it's currently registered
	while valueEnd <= string.len(netIds) do
		local netId = (tostring(string.sub(netIds, valueStart, valueEnd)))
		local playerName = getPlayerNameByNetID(netId)
		-- Ensure a playerName was returned and the player is on the list s_tTargetableObjects
		if playerName and playerName ~= "" then
			-- Add playerName to checked players
			if s_tTargetableObjects[playerName] then
				checkedPlayers[playerName] = true
				s_tTargetableObjects[playerName].netId = tonumber(netId)
			end
		else
			LogError("updateNetIds() - getPlayerNameByNetID("..tostring(netId).." failed to return a valid playerName Return["..tostring(playerName).."]")
		end
		
		-- Get the next netId if one exists
		valueStart, valueEnd, value = string.find(netIds, "[^%s]+", valueEnd+1)
		-- Fail check
		if valueEnd == nil then break end
	end
	
	-- For every player that wasn't checked, clear out their now invalid netId
	for ID, object in pairs(s_tTargetableObjects) do
		if (object.type == PLAYER_OBJECT) and (checkedPlayers[ID] == nil) then
			-- Log("updateNetIds() Player["..tostring(ID).."] has exited your netID withdrawal distance. Clearing their NetID")
			-- This player's netId is no longer valid. Clear it out
			s_tTargetableObjects[name].netId 	  = nil
			-- Was this player your target?
			if s_sCurrentTargetID and s_sCurrentTargetID == ID then
				-- Log("updateNetIds() Current Target ["..tostring(ID).."] has exited your netID withdrawal distance. Clearing them as a target")
				clearTarget()
			end
		end
	end
end

-- Converts number to Roman numeral
function numToRomanNumeral(level)
	if level == 1 then
		return "I"
	elseif level == 2 then
		return "II"
	elseif level == 3 then
		return "III"
	elseif level == 4 then
		return "IV"
	elseif level == 5 then
		return "V"
	end
end

-- Extracts the coordinates for the level icon
function getLevelCoords(level)
	if level == 1 then
		return {0, 299, 62, 331}
	elseif level == 2 then
		return {62, 299, 124, 331}
	elseif level == 3 then
		return {124, 299, 186, 331}
	elseif level == 4 then
		return {186, 299, 248, 331}
	elseif level == 5 then
		return {0, 331, 62, 363}
	end
end

-- Gets the level of a player based on their armorRating
function getLevelOfPlayer(armorRating)
	if armorRating < 10 then
		return 1
	elseif armorRating < 100 then
		return 2
	elseif armorRating < 1000 then
		return 3
	elseif armorRating < 10000 then
		return 4
	elseif armorRating >= 10000 then
		return 5
	end
end

-- Remove any active emotes and emote particles
function disableActiveEmote()
	if s_bIsEmoteEnabled then
		KEP_SetCurrentAnimationByGLID(0)
		local player = KEP_GetPlayer()
		Events.sendEvent("UPDATE_EMOTE_PARTICLE", {attacker = s_sPlayerName, particle = 0, bone = "Bip01", enabled = false, offset = {0, 0, 0}, forwardDir = {0, 0, 0}, upDir = {0, 0, 0}})
		s_bIsEmoteEnabled = false
	end
end

-- Returns whether a player is registered in the object list
function checkForPlayer(playerName)
	for index, object in pairs(s_tTargetableObjects) do
		if object.type == ObjectType.PLAYER then
			if index == playerName then
				return true
			end
		end
	end
	return false
end

-- Returns whether a search item exists in a table
function checkForEntry(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return true
		end
	end
	return false
end

-- Returns the index of a search item within a table (if any)
function getIndex(inputTable, searchItem)
	for i, v in pairs(inputTable) do
		if v and v == searchItem then
			return i
		end
	end
	return -1
end

function displayOutOfRangeMessage()
	local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
	KEP_EventEncodeNumber(statusEvent, 0)
	KEP_EventEncodeString(statusEvent, "Out of Range")
	KEP_EventEncodeNumber(statusEvent, 3)
	KEP_EventEncodeNumber(statusEvent, 255)
	KEP_EventEncodeNumber(statusEvent, 0)
	KEP_EventEncodeNumber(statusEvent, 0)
	KEP_EventSetFilter(statusEvent, 4)
	KEP_EventQueue(statusEvent)
end