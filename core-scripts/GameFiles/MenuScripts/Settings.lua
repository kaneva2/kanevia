--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("Framework_MenuShadowHelper.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")

local TAB_COUNT = 6

local X_OFFSET = 85
local Y_OFFSET = 85

local MENU_TITLES = {"Basic", "Avatar", "Keyboard", "Graphics", "Interface", "Advanced"}
--SETTING_MENUS = {"SettingsBasic.xml", "SettingsAvatar.xml", "SettingsKeyboard.xml", "SettingsGraphics.xml", "SettingsInterface.xml", "SettingsAdvanced.xml"}

local m_selectedIndex = 1
local m_selectedMenu = nil

---------------------------
----Dialog Functions
---------------------------
-- When the menu is created
function onCreate()

	KEP_EventRegisterHandler( "openSettingsMenu", "SETTINGS_GOTO_SYSTEM", KEP.HIGH_PRIO )

	KEP_EventRegisterHandler( "clientResizeEventHandler", "ClientResizeEvent", KEP.LOW_PRIO )
	--Set the rollovers for each button
	STC_BOLD_CTRL = gHandles["stcSelectedBold"]
	IMG_SELECTED_CTRL = gHandles["imgTabSelected"]
	IMG_SELECTED_TOP_CTRL = gHandles["imgTabSelectedTop"]

	for i = 1, TAB_COUNT do
		_G["btnTab" .. tostring(i) .. "_OnMouseEnter"] = function(btnHandle)
			if i ~= m_selectedIndex then
				Control_SetVisible(gHandles["imgNavRollover"..i], true)
			end
		end

		_G["btnTab" .. tostring(i) .. "_OnMouseLeave"] = function(btnHandle)
			Control_SetVisible(gHandles["imgNavRollover"..i], false)
		end

		_G["btnTab" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			handleAction(i)
		end
	end

	resizeDropShadow()
	MenuCenterThis()

	m_lastPosition = MenuGetLocationThis()
end

function onDestroy()
	if m_selectedMenu then
		MenuClose(m_selectedMenu)
	end
end

function Dialog_OnRender(dialogHandle, fElapsedTime)
	curLoc = MenuGetLocationThis()
	if not m_dragging then
		if (curLoc.x ~= m_lastPosition.x or curLoc.y ~= m_lastPosition.y) then
			if Dialog_IsLMouseDown(dialogHandle) == 1 then
				m_dragging = true
			end
			MenuBringToFrontThis()

			if m_selectedMenu then
				MenuSetLocation(m_selectedMenu, curLoc.x + X_OFFSET, curLoc.y + Y_OFFSET)			
				MenuBringToFront(m_selectedMenu)
			end
		end
	else
		if m_selectedMenu then
			MenuSetLocation(m_selectedMenu, curLoc.x + X_OFFSET, curLoc.y + Y_OFFSET)	
		end

		if Dialog_IsLMouseDown(dialogHandle) == 0 then
			m_dragging = false
		end
	end
	m_lastPosition = curLoc
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	alignMenus()
end

function Dialog_OnRButtonDown(dialogHandle, x, y )
	alignMenus()
end

---------------------
--Event Handler------
---------------------

function openSettingsMenu(dispatcher, fromNetid, event, eventid, filter, objectid)
	local menuToOpen = KEP_EventDecodeString( event )
	if menuToOpen then
		for index, menus in pairs(MENU_TITLES) do
			if menuToOpen == menus then
				handleAction(index)
			end
		end
	end
end

function clientResizeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	MenuCenterThis()
end

---------------------------
-----Local Functions-------
---------------------------
function handleAction(index)
	local selectedMenu = MENU_TITLES[index]

	Control_SetVisible(gHandles["imgNavRollover"..index], false)
	Control_SetVisible(gHandles["stcTab"..m_selectedIndex], true)
	Control_SetVisible(gHandles["stcTab"..index], false)
	Control_SetLocation(STC_BOLD_CTRL, Control_GetLocationX(gHandles["stcTab"..index]), Control_GetLocationY(gHandles["stcTab"..index]))
	Static_SetText(STC_BOLD_CTRL, selectedMenu)

	if index == 1 then
		setControlEnabledVisible(IMG_SELECTED_CTRL, false)
		setControlEnabledVisible(IMG_SELECTED_TOP_CTRL, true)
	else
		setControlEnabledVisible(IMG_SELECTED_CTRL, true)
		setControlEnabledVisible(IMG_SELECTED_TOP_CTRL, false)
		Control_SetLocation(IMG_SELECTED_CTRL, Control_GetLocationX(gHandles["btnTab"..index]), Control_GetLocationY(gHandles["btnTab"..index]))
	end

	
	Static_SetText(gHandles["stcMenuTitle"], selectedMenu)
	m_selectedIndex = index


	openMenu("Settings"..selectedMenu..".xml")
end

function openMenu(menu)
	if m_selectedMenu then
		MenuClose(m_selectedMenu)
	end

	m_selectedMenu = menu
	MenuOpen(menu)

	MenuBringToFrontThis()
	MenuBringToFront(m_selectedMenu)
	local loc = MenuGetLocationThis()

	MenuSetLocation(menu, loc.x + X_OFFSET, loc.y + Y_OFFSET)

	alignMenus()
end

function alignMenus()
	MenuBringToFrontThis()

	if m_selectedMenu then
		MenuBringToFront(m_selectedMenu)
	end
end

function setControlEnabledVisible(controlName, bool)
	Control_SetVisible(controlName, bool)
	Control_SetEnabled(controlName, bool)
end