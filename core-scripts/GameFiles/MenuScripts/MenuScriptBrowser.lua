--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\DevToolsConstants.lua")

SOURCE_ASSET_DOWNLOAD_EVENT = "3DAppAssetDownloadCompleteEvent"
DELETE_COMPLETE_EVENT = "3DAppAssetPublishDeleteCompleteEvent"
FILE_BROWSER_CLOSED_EVENT = "FileBrowserClosedEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"

g_listhandles = {}
g_listscrollbars = {}

g_scripts = {}
gIsChildGame = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "yesNoAnswerHandler", YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "FileBrowserClosedHandler", FILE_BROWSER_CLOSED_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "AppAssetDownloadHandler", SOURCE_ASSET_DOWNLOAD_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "DeleteCompleteHandler", DELETE_COMPLETE_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( FILE_BROWSER_CLOSED_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	if KEP_IsWorld() then
		gIsChildGame = 1
	else
		gIsChildGame = 0
	end

	g_listhandles = { gHandles["menuScriptNameDummy"], gHandles["menuScriptName"] }
	g_listscrollbars = {}
	for i,h in ipairs(g_listhandles) do
		table.insert(g_listscrollbars, ListBox_GetScrollBar(h))
	end

	buildScriptList()

	selectScript(-1)

	--Fix the spacing on the title
	Dialog_SetCaptionText(gDialogHandle, "    Menu Script Browser")
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function AppAssetDownloadHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local path = KEP_EventDecodeString(event)
	local fileType = KEP_EventDecodeNumber(event)

	-- Make sure we got a menu script
	if fileType == DevToolsUpload.MENU_SCRIPT then
		KEP_ShellOpen(path)
	end
end

function buildScriptList()

	ListBox_RemoveAllItems(gHandles["menuScriptName"])
	g_scripts = {}

	local count = KEP_GetNumMenuScripts( gIsChildGame )
	for i = 0, count-1 do
		local menuScriptName = KEP_GetMenuScriptByIndex( gIsChildGame, i )

		local script = {}
		script.scriptname = menuScriptName
		script.published = true
		table.insert(g_scripts, script)
	end

	local count = KEP_Count3DAppUnpublishedAssets( DevToolsUpload.MENU_SCRIPT)
	for i = 1, count do
		local name, fullpath, modified = KEP_Get3DAppUnpublishedAsset(DevToolsUpload.MENU_SCRIPT, i)
		name = name .. ".lua"

		local found = false
		for i,v in ipairs(g_scripts) do
			if name == v.scriptname then
				found = true
				break
			end
		end

		if found == false then
			local script = {}
			script.scriptname = name
			script.published = false
			table.insert(g_scripts, script)
		end
	end

	for i,v in ipairs(g_scripts) do
		ListBox_AddItem(gHandles["menuScriptName"], v.scriptname, i)
	end
end

function FileBrowserClosedHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local dialogResult = KEP_EventDecodeNumber( event )
	local filePath = ""
	if dialogResult~=0 then
		filePath = KEP_EventDecodeString( event )
	else
		--File Browser was cancelled
		return
	end

	if filter==UGC_TYPE.SCRIPT_DO then
		KEP_Import3DAppAsset( DevToolsUpload.MENU_SCRIPT, filePath)
		buildScriptList()
	end
end

function btnImport_OnButtonClicked( buttonHandle )
	local event = KEP_EventCreate( FILE_BROWSER_CLOSED_EVENT )
	KEP_EventSetFilter( event, UGC_TYPE.SCRIPT_DO )
	UGC_BrowseFileByImportType( UGC_TYPE.SCRIPT_DO, event )
end

function DeleteCompleteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local name = KEP_EventDecodeString(event)
	local type = tonumber(KEP_EventDecodeNumber(event))
	local success = tonumber(KEP_EventDecodeNumber(event))
	local errstr = KEP_EventDecodeString(event)

	if success == 0 then
		KEP_MessageBox( errstr )
	end

	KEP_ReloadMenus()

	buildScriptList()
end

gFileToDelete = ""

function btnDelete_OnButtonClicked(buttonHandle)
	local index = ListBox_GetSelectedItemData(gHandles["menuScriptName"])
	local script = g_scripts[index]

	if script ~= nil then
		local scriptname = script.scriptname
		local baseName = string.sub(scriptname, 1, string.len(scriptname)-4)
		gFileToDelete = baseName

		KEP_YesNoBox( "Are you sure you want to delete: '"..scriptname.."\' ?", "Confirm Delete", WF.MENUSCRIPTBROWSER_DELETE_FILTER)
	end
end

function yesNoAnswerHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = tonumber(KEP_EventDecodeNumber(event))
	if filter == WF.MENUSCRIPTBROWSER_DELETE_FILTER then
		if answer == 1 then
			if gFileToDelete ~= "" then
				KEP_Delete3DAppAsset( gFileToDelete, DevToolsUpload.MENU_SCRIPT)
			end
		end

		gFileToDelete = ""
	end
end

function btnEdit_OnButtonClicked( buttonHandle )
	local index = ListBox_GetSelectedItemData(gHandles["menuScriptName"])
	local script = g_scripts[index]

	if script ~= nil then
		local scriptname = script.scriptname
		local baseName = string.sub(scriptname, 1, string.len(scriptname)-4)
		KEP_RequestSourceAssetFromAppServer( DevToolsUpload.MENU_SCRIPT, baseName)
	end
end

function selectScript(sel_index)
	if sel_index >= 0 then
		Control_SetEnabled(gHandles["btnEdit"], true)
		Control_SetEnabled(gHandles["btnDelete"], true)
	else
		Control_SetEnabled(gHandles["btnEdit"], false)
		Control_SetEnabled(gHandles["btnDelete"], false)
	end
end

function menuScriptName_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if (bMouseDown == 1 or bFromKeyboard == 1) or g_bSyncAllLists == 1 then
		selectScript( sel_index )
		if g_bSyncAllLists == 0 then
			syncAllLists( g_listhandles, listBoxHandle )
		end
	end
end

function menuSelected(listBoxHandle, bFromKeyboard, bMouseDown)
	if (bMouseDown == 1 or bFromKeyboard == 1) or g_bSyncAllLists == 1 then
		if g_bSyncAllLists == 0 then
			syncAllLists( g_listhandles, listBoxHandle )
		end
	end
end

function scrollBarChanged(listBoxHandle)
	if g_bSyncAllLists == 0 then
		syncAllScrollBars( g_listhandles, listBoxHandle )
	end
end

function menuScriptName_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	scrollBarChanged(listBoxHandle)
end
