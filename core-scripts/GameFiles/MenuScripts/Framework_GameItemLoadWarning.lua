--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_GameItemLoadWarning.lua
-- 
-- Warns the user of Game Item Load Issues
-- Author: Wes Anderson
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

----------------------
-- Statics
----------------------

-- When the menu is created
function onCreate()
	-- Register client - client event handlers
	KEP_EventRegisterHandler("gameItemLoadMessageHandler", "FRAMEWORK_GAME_ITEM_LOAD_MESSAGE", KEP.HIGH_PRIO)
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Handles Game Item limit warning messages
function gameItemLoadMessageHandler(dispatcher, fromNetid, event, eventID, filter, objectID)
	local message1, message2
	if KEP_EventMoreToDecode(event) ~= 0 then
		message1 = KEP_EventDecodeString(event)
	end
	if KEP_EventMoreToDecode(event) ~= 0 then
		message2 = KEP_EventDecodeString(event)
	end
	Static_SetText(gHandles["stcWarning1"], tostring(message1))
	Static_SetText(gHandles["stcWarning2"], tostring(message2))
	Control_SetVisible(gHandles["stcWarning1"], true)
	Control_SetVisible(gHandles["stcWarning2"], true)
	Control_SetVisible(gHandles["imgLoadingIcon"], true)
	Control_SetVisible(gHandles["btnOK"], true)
end
