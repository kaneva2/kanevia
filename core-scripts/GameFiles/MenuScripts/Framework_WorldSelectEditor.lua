--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Menu.lua - Kaneva Menu Script Template
-- 
-- Template script for creating a new Kaneva client menu.
--
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
--dofile("..\\MenuScripts\\InventoryHelper.lua")

local ITEMS_PER_PAGE = 5
local KEY_ESC = 27

--URL info
TRAVEL_ACTIION = "action=search3DApps"
TRAVEL_COMMUNITIES = "&myCommunitiesOnly=true"
TRAVEL_OWNED = "&onlyOwned=true"
TRAVEL_SEARCH = "&search="
TRAVEL_PASSTHROUGH = "&passthrough="

local m_displayItems = {}


local m_page = 1

local m_placesList = {}

local m_clearSearch = true
local m_searchText = ""

local m_webID = 0
local m_currentSelection = ""

-- When the menu is created
function onCreate()
	-- KEP_EventRegisterHandler( "editParamHandler", "EDIT_PARAM_EVENT", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	-- KEP_EventRegisterHandler( "updateWOKInventoryHandler", "InventoryUpdateEvent", KEP.HIGH_PRIO)

	-- Events.registerHandler("UPDATE_WORLD_INVENTORY_ITEMS", updateGameItems)
	-- Events.registerHandler("UPDATE_GAME_ITEMS_BY_TYPE", updateGameItems)

	-- Shift out of the way of the main editor
	MenuOffsetLocationThis(400, 0)

	requestWorlds()
end

--Webcalls 

function  requestWorlds( )
	m_webID = m_webID + 1
	local url = GameGlobals.WEB_SITE_PREFIX .. TRAVEL_LIST_SUFFIX .. TRAVEL_ACTIION
	if m_searchText ~= "" then
		url = url .. TRAVEL_SEARCH..m_searchText
	end
	

	url =   url.. TRAVEL_PASSTHROUGH ..m_webID .. TRAVEL_COMMUNITIES .. TRAVEL_OWNED
	makeWebCall(url, WF.BROWSE_PLACES_SEARCH, m_page, ITEMS_PER_PAGE)
end
function BrowserPageReadyHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == WF.BROWSE_PLACES_SEARCH then
 		local xmlData = KEP_EventDecodeString( event )
		
		parsePlaces(xmlData)
-- 		-- Get the page number and search returned, and make sure it's still correct. 
-- 		-- Otherwise, don't bother to load
-- 		local s, e, pageReturned = string.find(xmlData, "<Page>(%d-)</Page>")
-- 		pageReturned = tonumber(pageReturned) or 0
		
-- 		local s, e, searchReturned = string.find(xmlData, "<Search>(.-)</Search>")
-- 		searchReturned = searchReturned or ""
		
-- 		-- and check search on this if later if added.
-- 		if pageReturned == m_page then
-- 			--Get the total number of items from the XML data and divide by items/page, update page numbers
-- 			local s, e, totalItems = string.find(xmlData, "<TotalNumberRecords>(%d-)</TotalNumberRecords>")
-- 			totalItems = tonumber(totalItems or 1)
-- 			m_maxPage = math.ceil(totalItems/ITEMS_PER_PAGE)
-- 			if (m_maxPage <= 1) then
-- 			    setActivated(gHandles["btnPageNext"], false)
-- 			end
-- 			m_displayItems = {}
			
-- 			displayItems()
-- 		end

-- 		local eh = Image_GetDisplayElement(gHandles["imgCurrent"])
-- 		if eh ~= 0 then
-- 			KEP_LoadIconTextureByID(m_item.value, eh, gDialogHandle)
-- 		end

-- 		return KEP.EPR_CONSUMED
	end
end

 function parsePlaces(ret)

 	m_placesList = {}
	local s = 0        -- start and end index of substring
	local numRec = 0   -- number of records in this chunk
	local passThrough = 0 -- passthrough num
	local strPos = 0   -- current position in string to parse
	local result = ""  -- return string from find temp
	s, strPos, result = string.find(ret, "<ReturnCode>(.-)</ReturnCode>")
	s, strPos, passThrough = string.find(ret,"<Passthrough>(.-)</Passthrough>")
	s, strPos, searchLogID = string.find(ret,"<SearchLogId>(.-)</SearchLogId>")

	searchLogID = tonumber(searchLogID)
	passThrough = tonumber(passThrough)
	if result == "0"  then
		local s, e, numRecs = string.find(ret, "<TotalNumberRecords>(.-)</TotalNumberRecords>")
		local totalRecords = tonumber(numRecs)
		m_maxPage = math.ceil(totalRecords / ITEMS_PER_PAGE)

		local s, e, numRecs = string.find(ret, "<NumberRecords>(.-)</NumberRecords>")
		numRecs = tonumber(numRecs)
		
		if numRecs > 0 then
			local start = 0
			local last = 0
			for i=1,numRecs do
				record = nil
				
				temps, tempe, record = string.find(ret, "<_x0033_DApps>(.-)</_x0033_DApps>", e)
				
				if record ~= nil then
					s = temps
					e = tempe

					start, last, name           = string.find(record, "<name>(.-)</name>")
					start, last, commId         = string.find(record, "<community_id>(.-)</community_id>")
					start, last, isPublic       = string.find(record, "<is_public>(.-)</is_public>")
					start, last, kurl           = string.find(record, "<STPURL>(.-)</STPURL>")

					start, last, dbPath = string.find(record, "<thumbnail_small_path>(.-)</thumbnail_small_path>")
					if not dbPath then
						dbPath = DEFAULT_KANEVA_PLACE_ICON
					end
											
					-- Build Local Texture Filename
					local fileName = tostring(split(GetFilenameFromPath(dbPath), ".")[1])
					fileName, newnum = string.gsub(fileName,"_sm" ,"_me")
					dbPath, newnum = string.gsub(dbPath,"_sm" ,"_me")
					fileName = fileName..".\jpg" 
					KEP_DownloadTextureAsyncToGameId( fileName, dbPath )
					filePath = "../../CustomTexture/" .. fileName

					if isPublic == "Y" then
						local placeInfo =  "<name>".. name .. "</name><url>" .. kurl .. "</url><fileName>" .. fileName .. "</fileName><dbPath>".. dbPath .. "</dbPath><imagePath>" .. filePath .. "</imagePath>"
						table.insert(m_placesList, placeInfo)
					end
				end
			end
		end
	end
	displayPlaces()
	
end



function displayPlaces()
	ListBox_RemoveAllItems(gHandles["lstItems"])	
	local selections = 1
	local startIndex = 1
	local endIndex = ITEMS_PER_PAGE


	for index, placeInfo in pairs(m_placesList) do
		local start, last, displayName = string.find(placeInfo, "<name>(.-)</name>")

		ListBox_AddItem(gHandles["lstItems"], displayName, selections)

		selections = selections + 1
	end
end

function btnCancel_OnButtonClicked(buttonHandle)
	DestroyMenu(gDialogHandle)
end

function btnClose_OnButtonClicked(buttonHandle)
	DestroyMenu(gDialogHandle)
end

function btnOk_OnButtonClicked(buttonHandle)
	local event = KEP_EventCreate( "WORLD_SELECT_EVENT" )
	KEP_EventEncodeString(event, "worldDestination")
	KEP_EventEncodeString(event, m_currentSelection)
	
	KEP_EventQueue( event )
	MenuCloseThis()
end

function btnPageNext_OnButtonClicked(buttonHandle)
	ListBox_ClearSelection(gHandles["lstItems"])
	ListBox_RemoveAllItems(gHandles["lstItems"])
	m_page = m_page + 1
        if m_page <= 1 or m_maxPage == 1 then
			setActivated(gHandles["btnPagePrev"], false)
        else
            setActivated(gHandles["btnPagePrev"], true)
        end

	if m_page >= m_maxPage then
		m_page = m_maxPage
		setActivated(gHandles["btnPageNext"], false)
	end

	requestWorlds()
end

function btnPagePrev_OnButtonClicked(buttonHandle)
	ListBox_ClearSelection(gHandles["lstItems"])
	ListBox_RemoveAllItems(gHandles["lstItems"])
	m_page = m_page - 1
        if m_page < m_maxPage then 
           setActivated(gHandles["btnPageNext"], true)
        end

	if m_page <= 1 then
	   m_page = 1
	   setActivated(gHandles["btnPagePrev"], false)
	end

	requestWorlds()
end

function lstItems_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)

	local eh = Image_GetDisplayElement(gHandles["imgCurrent"])

	local placeInfo = m_placesList[sel_index + 1]
	local start, last, imagePath = string.find(placeInfo, "<imagePath>(.-)</imagePath>")
	if eh ~= 0 then
		Element_AddTexture(eh, gDialogHandle,imagePath)

		scaleImage(gHandles["imgCurrent"], gHandles["imgCurrentFrame"], imagePath)
	end
	
	local name = ListBox_GetItemText( listBoxHandle, sel_index )
	Static_SetText(gHandles["stcCurrentName"], name)

	m_currentSelection = placeInfo
	Control_SetEnabled(gHandles["btnOk"], true)
end

----------------------------------------------------------------------
-- Search-Related Functions
----------------------------------------------------------------------

function txtSearch_OnFocusIn(editBoxHandle)
	-- On enter, set the text to blank
	if m_clearSearch then
		EditBox_SetText(editBoxHandle, "", true)
	end
	m_clearSearch = false
end

function txtSearch_OnFocusOut(editBoxHandle)
	local search = EditBox_GetText(editBoxHandle)
	
	-- On exit, if blank, reset the text to "Search|"
	if search == "" or m_clearSearch then
		m_clearSearch = true
		EditBox_SetText(editBoxHandle, "Search", false)
	else
		m_clearSearch = false
	end
end

function txtSearch_OnEditBoxKeyDown(dialogHandle, key, bShiftDown, bControlDown, bAltDown)
	if key == Keyboard.ENTER then
		m_searchText = EditBox_GetText(dialogHandle)
		requestWorlds()
	end
end

function imgClearSearch_OnLButtonUp(ctrlHandle)
	if Control_GetVisible(ctrlHandle) then
		m_clearSearch = true

		local searchBox = gHandles["txtSearch"]
		EditBox_ClearText(searchBox)
		Dialog_ClearFocus(gDialogHandle)

		if m_page < m_maxPage then
			setActivated(gHandles["btnPageNext"], true)
		end

		m_searchText = ""
		requestWorlds()
	end
end

function setActivated(controlHandle, value)
	if controlHandle ~= nil then
		Control_SetVisible(controlHandle, value)
		Control_SetEnabled(controlHandle, value)
	end
end