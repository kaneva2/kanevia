--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "PlayKDPEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
end

function btnPlayKDP_OnButtonClicked( buttonHandle )
	KEP_EventCreateAndQueue("PlayKDPEvent")	
	MenuCloseThis()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
