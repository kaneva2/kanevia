--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

UGC_IMPORT_FAILED_MENU_EVENT = "UGCImportFailedMenuEvent"

function creationHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	validRangeMsg = KEP_EventDecodeString( event )
	propValueMsg = KEP_EventDecodeString( event )
	importTypeMsg = KEP_EventDecodeString( event )
	if validRangeMsg==nil or validRangeMsg=="" then
		LogWarn("UGCImportTextureCountWarning: missing creation parameters" )
		setStaticText( "lblBody1" , "" )
	else
		if importTypeMsg==nil or importTypeMsg=="" then
			importTypeMsg = "3D objects"		-- backward compatible with DO
		end
		local msg = getStaticText( "lblBody1" )
		msg = string.gsub( msg, "{ImportType}", importTypeMsg )
		msg = string.gsub( msg, "{MaxTexs}", validRangeMsg )
		msg = string.gsub( msg, "{NL}", "\n" )
		setStaticText( "lblBody1" , msg )
	end
	showControl( "lblBody1" )
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "creationHandler", UGC_IMPORT_FAILED_MENU_EVENT, KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnSuggestions_OnButtonClicked(buttonHandle)
	MenuOpenModal("UGCReduceCost.xml")
end
