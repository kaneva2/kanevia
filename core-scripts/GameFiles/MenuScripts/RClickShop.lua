--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua" )
dofile("..\\MenuScripts\\DevToolsConstants.lua")
dofile("..\\MenuScripts\\ActionItemFunctions.lua")
dofile("..\\MenuScripts\\Lib_Web.lua")



-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Web			= _G.Web

-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local FIRST_OFFSET_CONTROL		= "imgFrame"
local HORIZONTAL_RULE_PREFIX	= "imgHorDoubleBottom"

local CONTROL_PADDING			= 5
local DIALOG_PADDING			= 3

-- Each table is a row with CONTROL_PADDING pixels between. A table within a table are two controls on the same row.
local CONTROL_SETS = {
	NONOWNER	= { {"btnRave", "btnWebView", "btnBuy"} }
}

-----------------------------------------------------------------
-- Variables
-----------------------------------------------------------------
local m_numElements = 0
local m_obj = nil -- Initialized in newObject()


-----------------------------------------------------------------
-- Function definitions
-----------------------------------------------------------------

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "rightClickHandler", "DisplayRightClickEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "DynamicObjectInfoEventHandler", "DynamicObjectInfoEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ActionItemListResponseHandler", "ScriptClientEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "ScriptDownloadCompletedHandler", "3DAppAssetDownloadCompleteEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	UGCI.RegisterSubmissionEvents( )
	UGCI.RegisterUploadConfirmationMenuEvent( )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	
	m_obj = newObject()
	resetMenu()
	-- The menu is updated via DynamicObjectInfoEventHandler which is sent 
	-- automatically from Selection.lua when a dynamic object is right clicked.
end

function Dialog_OnDestroy(dialogHandle)
	KEP_ClearSelection()
	Helper_Dialog_OnDestroy( dialogHandle )
end

--------------------------------------------------------------------------------------
-- Event Handlers/Related
--------------------------------------------------------------------------------------
function rightClickHandler(dispatcher, fromNetid, event, eventid, filter, objectid)		
	-- This isn't being used for now, but leaving here just in case since it directly relates
end

-- Receives an object PID for display handling
function DynamicObjectInfoEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local PID = KEP_EventDecodeNumber(event)
	local type
	if KEP_EventMoreToDecode(event) ~= 0 then
		type = KEP_EventDecodeNumber(event)
	end
	
	-- New item selected
	if m_obj.PID then
		-- Save old item
		KEP_SaveDynamicObjectPlacementChanges(m_obj.PID)
		KEP_ReplaceSelection(type, PID)
	end
	
	updateProperties(PID, type)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.GET_DYNAMIC_OBJECT_INFO then
		local objectData = KEP_EventDecodeString( event )
		local returnCode = string.match(objectData, "<ReturnCode>(.-)</ReturnCode>")
		if tonumber(returnCode) ~= 0 then
			return
		end

		m_obj.name 				= Web.getDataByTag(objectData, "display_name", 		"(.-)")
		m_obj.creatorName 		= Web.getDataByTag(objectData, "creator", 			"(.-)")
		m_obj.raveCount 		= Web.getDataByTag(objectData, "rave_count", 		"(.-)")
		m_obj.webPrice 			= Web.getDataByTag(objectData, "web_price", 		"(.-)")

		local playerRaveCount 	= Web.getDataByTag(objectData, "player_rave_count",	"(.-)")
		m_obj.playerRaveCount 	= tonumber(playerRaveCount)

		local invType 			= Web.getDataByTag(objectData, "inventory_type", 	"(.-)")
		m_obj.invType 			= tonumber(invType)

		displayMenu() 

	elseif filter ==  WF.RAVE then
		local returnString = KEP_EventDecodeString( event )
		local result = string.match(returnString, "<ReturnCode>([0-9]+)</ReturnCode>")
		
		if result == "0" then	
			local numRaves = string.match(returnString, "<NumRaves>(.-)</NumRaves>", 1)
			m_obj.raveCount = tonumber(numRaves)
			setDescriptionRavePrice( KEP_IsOwnerOrModerator() )
		end

	elseif filter == WF.GET_SCRIPT_BUNDLE then
		local returnString = KEP_EventDecodeString( event )

		if string.find(returnString, "<ReturnCode>0</ReturnCode>") then

			local bundleGlid 		= string.match(returnString, "<global_id>(%d-)</global_id>")
			local bundleInvType 	= string.match(returnString, "<inventory_type>(%d-)</inventory_type>")
			local bundleName	 	= string.match(returnString, "<name>(.-)</name>")
			local bundleWebPrice	= string.match(returnString, "<market_cost>(%d-)</market_cost>")

			m_obj.bundleGlid 		= tonumber( bundleGlid )
			m_obj.bundleInvType 	= tonumber( bundleInvType )
			m_obj.bundleName 		= bundleName
			m_obj.bundleWebPrice 	= tonumber( bundleWebPrice )
		end
	end
end


function ActionItemListResponseHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ScriptClientEventTypes.GetAllScripts and m_obj.actionItem == nil then
		m_obj.actionItem = false
		
		local numRunningScripts = KEP_EventDecodeNumber(event)
		for i=1,numRunningScripts do
			local placementId, scriptname
			placementId = KEP_EventDecodeNumber(event)
			scriptname = KEP_EventDecodeString(event)
			if placementId == m_obj.PID then
				if string.find(scriptname, "ActionItems\\") ~= nil or string.find(scriptname, "ActionInstances\\") ~= nil or string.find(scriptname, "ActionItems/") ~= nil or string.find(scriptname, "ActionInstances/") ~= nil then
					if string.find(scriptname, "ActionItems/") or string.find(scriptname, "ActionInstances/") then
						scriptname = string.gsub(scriptname, "/", "\\")
					end
					m_obj.actionItem = true
					m_obj.scriptName = scriptname

					local scriptGlid = string.match(scriptname, "\\(%d-)%.lua")
					if scriptGlid then
						getBundleGlid( tonumber(scriptGlid) )
					end
				end
			end
		end
		
		displayMenu() 

		return KEP.EPR_CONSUMED
	end
end

--------------------------------------------------------------------------------------
-- Updating data/menu
--------------------------------------------------------------------------------------
function newObject()
	return { PID = -1,
				
			-- From KEP_DynamicObjectGetInfo
			name 			= nil, 
			description 	= nil,
			canPlayMovie 	= 0,
			isAttachable 	= 0,
			interactive 	= 0,
			isLocal 		= 0,
			derivable 		= 0,
			assetId 		= 0,
			friendId 		= -1,
			customTexture 	= "",
			GLID 			= 0,
			invType 		= 0,
			textureURL 		= "",
			gameItemId		= 0,
			playerId		= 0,

			-- From name of object??
			isFlashFrame = false,

			-- KEP_CanDynamicObjectSupportAnimation
			isAnimatiable = 0,

			-- KEP_GetDynamicObjectAnimation and BrowserPageReadyHandler WF.OBJECT_BROWSER_INFO
			animGlid = nil,
			animName = "",

			-- flashObjectInfoEventHandler
			mediaPlaying = "";

			-- From BrowserPageReadyHandler WF.GET_DYNAMIC_OBJECT_INFO
			raveCount 		= 0,
			playerRaveCount = 0,
			webPrice 		= 0,
			creatorName 	= nil,

			-- From ActionItemListResponseHandler
			actionItem 		= nil,
			scriptName 		= nil,
			bundleGlid 		= nil,
			bundleInvType 	= nil,
			bundleName 		= nil,
			bundleWebPrice 	= nil,

			-- From getFlashGame
			swfName 	= "",
			swfParams 	= ""
		}
end

-- Handles a new object
function updateProperties(objPID, objectType)
	m_obj = newObject()

	m_obj.PID  = objPID
	m_obj.scriptName = nil
	m_obj.objectType = objectType
	m_obj.name, m_obj.description, m_obj.canPlayMovie, m_obj.isAttachable, m_obj.interactive, m_obj.isLocal, m_obj.derivable, m_obj.assetId, m_obj.friendId, m_obj.customTexture, m_obj.GLID, m_obj.invType, m_obj.textureURL, m_obj.gameItemId, m_obj.playerId = KEP_DynamicObjectGetInfo(m_obj.PID)

	makeWebCall(GameGlobals.WEB_SITE_PREFIX..DYNAMIC_OBJECT_INFO_SUFFIX.."&globalId="..m_obj.GLID, WF.GET_DYNAMIC_OBJECT_INFO)

	-- ask server for list of objects with scripts
	ActionItemFunctions.requestServerActionItemList()

	-- next stop ActionItemListResponseHandler
	displayMenu() --also called in all the event handlers as they come in
end

function displayMenu()
	resetMenu()

	-- Load basic info at top
	setObjectName()
	setDescriptionRavePrice(true)
	showImage()
	Control_SetEnabled(gHandles["btnBuy"], m_obj.invType and m_obj.invType ~= 0)

	local startingControl = gHandles[FIRST_OFFSET_CONTROL]
	local offset = Control_GetLocationY(startingControl) + Control_GetHeight(startingControl) + CONTROL_PADDING
	-- Add User Buttons

	offset = addControlSet("NONOWNER", offset)
	Control_SetEnabled( gHandles["btnRave"], m_obj.playerRaveCount == 0)
	MenuBringToFrontThis()
end

function resetMenu()
	-- Hide horizonal rules
	for i = 1, m_numElements do
		Control_SetVisible(gHandles[HORIZONTAL_RULE_PREFIX .. i], false)
	end
	m_numElements = 0
end

function setObjectName()
	m_obj.name = truncateStaticToControl(m_obj.name, gHandles["lblObjectName"], true)
	Static_SetText(gHandles["lblObjectName"], m_obj.name)

	m_obj.creatorName = truncateStaticToControl(m_obj.creatorName, gHandles["lblUserName"], true)
	Static_SetText(gHandles["lblUserName"], m_obj.creatorName)
end

function showImage()
	local eh = Image_GetDisplayElement(gHandles["imgObjectPic"])	
	KEP_LoadIconTextureByID(m_obj.GLID, eh, gDialogHandle)
end

function setDescriptionRavePrice(owner)
	Static_SetText(gHandles["lblRaveCount"], m_obj.raveCount or "")
	Static_SetText(gHandles["lblOther"], m_obj.webPrice or "")

	Control_SetVisible(gHandles["imgRave"], 		 owner)
	Control_SetVisible(gHandles["lblRaveCount"], 	 owner)
	Control_SetVisible(gHandles["imgPrice"],	 	 owner)
	Control_SetVisible(gHandles["lblOther"], 		 owner)
end

function addControlSet(setName, offset)
	local controlSet = CONTROL_SETS[setName]
	local controlHeight = 0

	-- Set the next horizontal rule
	m_numElements = m_numElements + 1
	controlHeight = setControlLocation(HORIZONTAL_RULE_PREFIX .. m_numElements, offset)
	offset = offset + controlHeight + CONTROL_PADDING

	-- Set the controls in the set visble and to the correct location
	for i, controlBlock in ipairs(controlSet) do
		-- If it's a table, that means they should be on the same Y heigh so set without updating offset
		if type(controlBlock) == "table" then
			for j, control in ipairs(controlBlock) do
				controlHeight = setControlLocation(control, offset)
			end
		else
			controlHeight = setControlLocation(controlBlock, offset)
		end
		offset = offset + controlHeight + CONTROL_PADDING
	end

	-- Set the height of the menu
	local size = MenuGetSizeThis()
	MenuSetSizeThis(size.width, offset + DIALOG_PADDING + Dialog_GetCaptionHeight(gDialogHandle))

	--return the offset for future use
	return offset
end

function setControlLocation(controlName, location)
	local control = gHandles[controlName]
	Control_SetLocationY(control, location)
	Control_SetVisible(control, true)
	return Control_GetHeight(control)
end

function getBundleGlid( scriptGlid )
	local web_address = GameGlobals.WEB_SITE_PREFIX .. "kgp/instantBuy.aspx?gzip=true&operation=info".."&getScriptBundle=true&items=" .. scriptGlid
	makeWebCall(web_address, WF.GET_SCRIPT_BUNDLE)
end

--------------------------------------------------------------------------------------
-- Buttons
--------------------------------------------------------------------------------------
function btnRave_OnButtonClicked( buttonHandle )
	if KEP_IsWorld() then
		KEP_MessageBox("Cannot rave objects from a World.")
		return
	end
	
	if ( m_obj.playerRaveCount == 0 ) then
		local url = GameGlobals.WEB_SITE_PREFIX..SET_ITEM_RAVES_SUFFIX.."&globalId="..m_obj.GLID	
		makeWebCall( url, WF.RAVE, 1)
		Control_SetEnabled( gHandles["btnRave"], false)
	end
end

function btnWebView_OnButtonClicked( buttonHandle )
	viewItemOnWeb()
end

function btnBuy_OnButtonClicked( buttonHandle )
	buyObject()
end

function ScriptDownloadCompletedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local path = KEP_EventDecodeString(event)
	local fileType = KEP_EventDecodeNumber(event)
	local name = string.match(path, ".*\\(.+)%..*")
	if fileType == DevToolsUpload.ACTION_INSTANCE then
		ActionItemFunctions.OpenPreset(name, path, m_obj.PID)
	elseif fileType == DevToolsUpload.ACTION_ITEM then
		local newName = tostring(m_obj.PID)
		local resultPath = KEP_DuplicateActionItem(newName, path, DevToolsUpload.ACTION_INSTANCE)
		ActionItemFunctions.OpenPreset(name, resultPath, m_obj.PID)
	else
		return KEP.EPR_OK
	end
	
	MenuCloseThis()
end

function stcBuy_OnLButtonUp( stcHandle )
	buyObject()
end

function stcShop_OnLButtonUp( stcHandle )
	viewItemOnWeb()
end

function viewItemOnWeb()
	local shopGlid = m_obj.bundleGlid or m_obj.GLID
	KEP_LaunchBrowser(GameGlobals.WEB_SITE_PREFIX_SHOP .. IMAGE_DETAILS_SUFFIX .. shopGlid)
end

function buyObject()
	MenuOpen("Checkout.xml")
	
	local ev = KEP_EventCreate( "ItemInfoEvent" )
	KEP_EventEncodeNumber( ev, m_obj.bundleInvType or m_obj.invType )
	KEP_EventEncodeNumber( ev, 1 ) -- num items
	KEP_EventEncodeNumber( ev, m_obj.bundleGlid or m_obj.GLID )
	KEP_EventEncodeNumber( ev, 1 ) --quantity of item
	KEP_EventEncodeString( ev, m_obj.bundleName or m_obj.name ) -- name
	KEP_EventEncodeNumber( ev, m_obj.bundleWebPrice or m_obj.webPrice ) -- price
	KEP_EventQueue( ev )
end
