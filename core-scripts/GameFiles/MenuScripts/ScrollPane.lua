--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\ControlTypes.lua")

-- Vertical scroll pane controller class
ScrollPane = { defaultScrollAmount = 20 }

---------------------------------------------------------------------
-- Call ScrollPane.new() to allocate a new scroll pane controller
--
-- ScrollPane controller takes at least two DXUT controls: 
-- 1) An image control for the clipping and background
-- 2) An listbox used as scroll bar. Resize it so that only scroll bar is visible.
-- 3) In addition, caller can pass in a list of existing controls to
--    be added as children of the scroll pane.
---------------------------------------------------------------------
function ScrollPane.new(debugName, dialogHandle, containerName, scrollbarName, controlNames)
	local obj = setmetatable({}, {__index = ScrollPane})
	obj:create(debugName, dialogHandle, containerName, scrollbarName, controlNames)
	return obj
end

---------------------------------------------------------------------
-- Constructor
---------------------------------------------------------------------
function ScrollPane.create(self, debugName, dialogHandle, containerName, scrollbarName, controlNames)
	self.debugName = debugName

	-- Virtual container (an DXUT image). You should size it to indicate the boundary of the scroll pane.
	local container = Dialog_GetControl(dialogHandle, containerName)
	if container == nil then
		self:log("Scroll pane container control not found: `" .. tostring(containerName) .. "'")
		return nil
	end

	if Control_GetType(container) ~= ControlTypes.DXUT_CONTROL_IMAGE then
		self:log("Scroll pane container control must be an image: `" .. tostring(containerName) .. "'")
		return nil
	end

	-- An DXUTListBox used as scroll bar provider
	local scrollBarListBox = Dialog_GetListBox(dialogHandle, scrollbarName)
	local scrollBar = ListBox_GetScrollBar(scrollBarListBox)
	if scrollBar == nil then
		self:log("Cannot obtain scroll bar control from control: `" .. tostring(scrollbarName) .. "'")
		return nil
	end

	-- Init data members
	self.dialog = dialogHandle
	self.container = container
	self.children = {}
	self.scrollBar = scrollBar
	self.trackPos = 0
	self.scrollAmount = 0

	-- Load child controls if provided. Alternatively you can call addChild or addChildren after constructor.
	if controlNames then
		for _, name in ipairs(controlNames) do
			local childHandle = Dialog_GetControl(dialogHandle, name)
			if childHandle == nil then
				self:log("Scroll pane control not found: `" .. tostring(name) .. "'")
				return nil
			end

			self:addChild(childHandle, false, true)
		end
	end

	-- Hook up scroll bar event listener
	_G[scrollbarName .. "_OnScrollBarChanged"] = function(scrollBarHandle, listBoxHandle, pageSize, trackPos)
		return self:onScrollBarChanged(trackPos)
	end

	-- Recalculate scroll pane layout
	self:recalcLayout()

	return self
end

function ScrollPane.log(self, message) 
	self = self or { debugName = "nil" }
	KEP_Log("ScrollPane[" .. tostring(self.debugName) .. ": " .. tostring(message))
end

---------------------------------------------------------------------
-- Recalculate layout. Call this after addChild/removeChild calls or after
-- resizing the container.
---------------------------------------------------------------------
function ScrollPane.recalcLayout(self)
	self = self or { debugName = "nil" }
	if type(self.container) ~= "userdata" or type(self.children) ~= "table" then
		log("Invalid scroll pane object")
		return
	end

	local scrollHeight = 0

	local containerX, containerY = Control_GetLocationX(self.container), Control_GetLocationY(self.container)
	local containerW, containerH = Control_GetWidth(self.container), Control_GetHeight(self.container)

	for _, child in ipairs(self.children) do
		local ctrlH = Control_GetHeight(child.handle)
		if child.y + ctrlH > scrollHeight then
			scrollHeight = child.y + ctrlH
		end

		-- clip children
		Control_SetClipRect(child.handle, containerX, containerY, containerX+containerW, containerY+containerH)
	end

	self.scrollMax = math.floor(scrollHeight / ScrollPane.defaultScrollAmount)	-- scale scroll position from pixels into "lines"
	self.pageMax = math.floor(containerH / ScrollPane.defaultScrollAmount)
	self.scrollAmount = scrollHeight / self.scrollMax							-- actual scroll amount for this scroll pane
	
	-- Update scroll bar state
	ScrollBar_SetTrackRange(self.scrollBar, 0, self.scrollMax)
	ScrollBar_SetPageSize(self.scrollBar, self.pageMax)
	ScrollBar_SetTrackPos(self.scrollBar, self.trackPos)
end

function ScrollPane.getChildAbsPos(self, child)
	local containerX = Control_GetLocationX(self.container)
	local containerY = Control_GetLocationY(self.container)
	return containerX + child.x, containerY + child.y - self.trackPos * self.scrollAmount
end

function ScrollPane.updateChildRelPos(self, child)
	local containerX, containerY = Control_GetLocationX(self.container), Control_GetLocationY(self.container)
	local ctrlX, ctrlY = Control_GetLocationX(child.handle), Control_GetLocationY(child.handle)
	child.x = ctrlX - containerX

	-- child.y = ctrlY + self.trackPos * self.scrollAmount - containerY
	child.y = ctrlY - containerY
end

function ScrollPane.onScrollBarChanged(self, trackPos)
	self.trackPos = trackPos
	for _, child in ipairs(self.children) do
		local ctrlX, ctrlY = self:getChildAbsPos(child)
		Control_SetLocation(child.handle, ctrlX, ctrlY)
	end
end

function ScrollPane.addChild(self, controlHandle, relToContainer, noRecalc)
	if controlHandle == null then
		self:log("addChild: null argument")
		return
	end

	if relToContainer then
		local containerX, containerY = Control_GetLocationX(self.container), Control_GetLocationY(self.container)
		Control_SetLocation(controlHandle, containerX + Control_GetLocationX(controlHandle), containerY + Control_GetLocationY(controlHandle))
	end

	local child = { handle = controlHandle }
	table.insert(self.children, child)
	self:updateChildRelPos(child)

	if not noRecalc then
		self:recalcLayout()
	end
end

function ScrollPane.addChildren(self, controlHandles, relToContainer)
	if controlHandles == null then
		self:log("addChildren: null argument")
		return
	end

	for _, controlHandle in ipairs(controlHandles) do
		self:addChild(controlHandle, relToContainer, true)
	end

	self:recalcLayout()
end

function ScrollPane.removeChild(self, controlHandle)
	if controlHandle == null then
		self:log("removeChild: null argument")
		return
	end

	for index, child in ipairs(self.children) do
		if child.handle == controlHandle then
			table.remove(self.children, index)
			return
		end
	end

	self:recalcLayout()
end

function ScrollPane.removeAllChildren(self)
	self.children = {}
	self:recalcLayout()
end

function ScrollPane.enumerateChildren(self, func)
	for _, child in ipairs(self.children) do
		func(child)
	end
end
