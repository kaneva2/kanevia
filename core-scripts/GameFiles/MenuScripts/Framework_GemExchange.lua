--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Framework_GemExchange.lua
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- -- -- -- -- -- --
-- CONSTANTS
-- -- -- -- -- -- --
local NUM_GEM_TYPES = 5
local REWARDS_EACH_STRING = " Rewards each"
local REWARDS_REMAINING_STRING = " Rewards Remaining"

-- -- -- -- -- -- --
-- LOCAL FUNCTIONS
-- -- -- -- -- -- --
local createControlAssociations --()
local parseGemData --(gemString)
local populateGemData --()
local requestGemData --()
local exchangeGems --()
local handleEditBox --(index, value)
local handleEditBoxFocusOut --(index)
local handleCountDown --(index)
local handleCountUp --(index)
local calculateExchangeAmount --()
local formatCommaValue --(value)

-- -- -- -- -- -- --
-- LOCAL VARIABLES
-- -- -- -- -- -- --
local m_gemNames = {"diamond", "ruby", "emerald", "sapphire", "topaz"}
local m_currGems = {topaz = 0, sapphire = 0, emerald = 0, ruby = 0, diamond = 0}
local m_exchangeGems = {topaz = 0, sapphire = 0, emerald = 0, ruby = 0, diamond = 0}
local m_gemValues = {topaz = 0, sapphire = 0, emerald = 0, ruby = 0, diamond = 0}
local m_exchangeAmount = 0
local m_exchangeRemaining = 0

-- Called when the menu is created
function onCreate()
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	
	Events.registerHandler("FRAMEWORK_UPDATE_GEM_COUNTS", updateGemCounts)
	
	createControlAssociations()

	populateGemData()
	requestGemData()
end

-- When the ok button is clicked
function btn_Ok_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

function btnmenuCancel1_OnButtonClicked(buttonHandle)
	MenuCloseThis()
end

function btnmenuCancel2_OnButtonClicked(buttonHandle)
	m_exchangeGems = {topaz = 0, sapphire = 0, emerald = 0, ruby = 0, diamond = 0}
	m_exchangeAmount = 0
	populateGemData()
end

function btnmenuExchange_OnButtonClicked(buttonHandle)
	exchangeGems()
	m_exchangeGems = {topaz = 0, sapphire = 0, emerald = 0, ruby = 0, diamond = 0}
	m_exchangeAmount = 0
	populateGemData()
end

-- -- -- -- -- -- --
-- EVENT HANDLERS
-- -- -- -- -- -- --
function updateGemCounts(event)
	local success = parseGemData(event.text)
	if success then
		populateGemData()
	end
end

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- If browser return for Location Data web call
	if filter == WF.GEM_AMOUNTS or filter == WF.GEM_CONVERT or filter == WF.GEM_REWARD then
		local estr = KEP_EventDecodeString( tEvent )
		local success = parseGemData(estr)
		
		if success then
			populateGemData()
		end
		
	   	return
	end
end

-- Web call to receive the current gem data
function requestGemData()
	local address = GameGlobals.WEB_SITE_PREFIX.."/kgp/metaGameItems.aspx?action=getBalances&itemType=Gem"
	makeWebCall(address, WF.GEM_AMOUNTS)
end

function exchangeGems()
	local address = GameGlobals.WEB_SITE_PREFIX.."/kgp/metaGameItems.aspx?action=convertToRewards&itemType=Gem&itemAmounts="
	local validOperation = false
	
	for k, v in pairs(m_exchangeGems) do
		if v > 0 then
			if validOperation then
				address = address.."|"
			end
			validOperation = true
			local gemName = k:gsub("^%l", string.upper)
			address = address..gemName..","..tostring(v)
		end
	end
	
	if validOperation then
		makeWebCall(address, WF.GEM_CONVERT)
	end
end

-- -- -- -- -- -- --
-- LOCAL FUNCTIONS
-- -- -- -- -- -- --
function parseGemData(gemString)
	local returnCode = ParseXmlReturnCode(gemString)
	if (returnCode == nil or returnCode ~= 0) then
		return false --Failure!
	end
	
	m_exchangeRemaining = xmlGetInteger(gemString, "DailyConversionsRemaining")
	
	for item in string.gmatch(gemString, "<Item>(.-)</Item>") do
		local itemName = string.lower(xmlGetString(item, "Name"))
		local itemBalance = xmlGetInteger(item, "Balance")
		local itemValue = xmlGetInteger(item, "ConversionValueRewards")
		
		m_currGems[itemName] = itemBalance
		m_gemValues[itemName] = itemValue
	end
	
	return true
end

function populateGemData()
	for i = 1, NUM_GEM_TYPES do
		local gemName = m_gemNames[i]
		
		Static_SetText(gHandles["stcGemCount"..tostring(i)], tostring(m_currGems[gemName]))
		Static_SetText(gHandles["stcGemValue"..tostring(i)], formatCommaValue(m_gemValues[gemName])..REWARDS_EACH_STRING)
		
		Control_SetEnabled(gHandles["btnCountDown"..tostring(i)], (m_exchangeGems[gemName] > 0))
		EditBox_SetText(gHandles["editGemCount"..tostring(i)], tostring(m_exchangeGems[gemName]), false)
		Control_SetEnabled(gHandles["btnGemCountUp"..tostring(i)], (m_exchangeGems[gemName] < m_currGems[gemName] and m_exchangeAmount + m_gemValues[gemName] <= m_exchangeRemaining))
	end
	
	Static_SetText(gHandles["stcExchangeShadow"], formatCommaValue(m_exchangeAmount))
	Static_SetText(gHandles["stcExchangeCount"], formatCommaValue(m_exchangeAmount))
	Static_SetText(gHandles["stcRewardsRemaining"], formatCommaValue(m_exchangeRemaining)..REWARDS_REMAINING_STRING)
	
	Control_SetVisible(gHandles["btnmenuExchange"], (m_exchangeAmount > 0))
	Control_SetVisible(gHandles["btnmenuCancel1"], (m_exchangeAmount == 0))
	Control_SetVisible(gHandles["btnmenuCancel2"], (m_exchangeAmount > 0))
end

function handleEditBox(index, value)
	local gemName = m_gemNames[index]
	
	if value == "" then return end
	value = tonumber(value)
	
	if value ~= nil then
		m_exchangeGems[gemName] = 0
		local prevExchange = calculateExchangeAmount()
		local available = m_exchangeRemaining - prevExchange
		local cap = math.floor(available/m_gemValues[gemName])
		if value > cap then value = cap end
		if value > m_currGems[gemName] then value = m_currGems[gemName] end
		
		m_exchangeGems[gemName] = value
	end

	m_exchangeAmount = calculateExchangeAmount()
	populateGemData()
end

function handleEditBoxFocusOut(index)
	local gemName = m_gemNames[index]
	EditBox_SetText(gHandles["editGemCount"..tostring(index)], tostring(m_exchangeGems[gemName]), false)
end

function handleCountDown(index)
	local gemName = m_gemNames[index]
	m_exchangeGems[gemName] = m_exchangeGems[gemName] - 1
	m_exchangeAmount = calculateExchangeAmount()
	populateGemData()
end

function handleCountUp(index)
	local gemName = m_gemNames[index]
	m_exchangeGems[gemName] = m_exchangeGems[gemName] + 1
	m_exchangeAmount = calculateExchangeAmount()
	populateGemData()
end

function calculateExchangeAmount()
	local exchangeAmount = 0
	for k, v in pairs(m_exchangeGems) do
		exchangeAmount = exchangeAmount + (m_gemValues[k] * v)
	end
	
	return exchangeAmount
end

function createControlAssociations()
	for i = 1, NUM_GEM_TYPES do
		_G["btnCountDown".. tostring(i).. "_OnButtonClicked"] = function(btnHandle)
			local s, e, n = string.find(Control_GetName(btnHandle), "(%d+)")
			handleCountDown(tonumber(n))
		end
		
		_G["editGemCount".. tostring(i).. "_OnEditBoxChange"] = function(editBoxHandle, editBoxText)
			local s, e, n = string.find(Control_GetName(editBoxHandle), "(%d+)")
			handleEditBox(tonumber(n), editBoxText)
		end
		
		_G["editGemCount".. tostring(i).. "_OnFocusOut"] = function(editBoxHandle)
			local s, e, n = string.find(Control_GetName(editBoxHandle), "(%d+)")
			handleEditBoxFocusOut(tonumber(n))
		end	
		
		_G["btnGemCountUp".. tostring(i).. "_OnButtonClicked"] = function(btnHandle)
			local s, e, n = string.find(Control_GetName(btnHandle), "(%d+)")
			handleCountUp(tonumber(n))
		end
	end
end

-- Add comma to separate thousands
function formatCommaValue(value)
  	local formatted = value
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end