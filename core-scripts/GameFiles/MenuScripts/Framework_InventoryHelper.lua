--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

---------------------------
-- Inventory Constants
---------------------------
ITEM_TYPES = {	WEAPON = "weapon", 
				ARMOR = "armor",
				TOOL = "tool",
				CONSUMABLE = "consumable",
				AMMO = "ammo",
				GENERIC = "generic",
				PLACEABLE = "placeable",				
				CHARACTER = "character",
				SPAWNER = "spawner",
				HARVESTABLE = "harvestable",
				RECIPE = "recipe",
				BLUEPRINT = "blueprint",				
				QUEST = "quest",
				GEMBOX = "gembox",				
				FLAG = "flag",
				SHORTCUT = "shortcut",
				SETTINGS = "settings",
				MAIN_SETTINGS = "mainSettings",
				ENVIRONMENT_SETTINGS = "environmentSettings",
				PET = "pet",
				WELCOME_SETTINGS = "welcomeSettings",
				OVERVIEW_SETTINGS = "overviewSettings",
				PATH = "path",
				TUTORIAL = "tutorial",
				WAYPOINT = "waypoint"
			}
INVENTORY_ERRORS = { BACKPACK_FULL = "You do not have enough room in your backpack!",
					 MISSING_ITEM = "You do not have enough of a required item!"
				   }
EMPTY_ITEM = {UNID = 0, properties = {name = "EMPTY", GLID = 0, itemType = "generic"}, count = 0}
TOOLBAR_SLOTS = 5

INVENTORY_PID = 0
ARMORY_PID = -1
BLUEPRINT_PID = -2
QUEST_PID = -3
GAME_PID = -4

DEFAULT_STACK_SIZE = 100
MAX_INVENTORY_SIZE = 45

-- TODO: Find better default background icon. Also gameID still?
DEFAULT_IMAGE_TEXTURE = "Framework_HudElements.tga"
DEFAULT_IMAGE_COORDS = {L = 129,
						T = 0,
						R = 209,
						B = 81
					   }

local g_scaleCache = {}

-- Compiles an inventory table into an event
function compileInventory(inventoryTable, event)
	local inventoryString = Events.encode(inventoryTable)
	
	if inventoryString then
		KEP_EventEncodeString(event, inventoryString)
	end
	
	return event
end

function compileInventoryItem(item, event)
	local itemString = Events.encode(item)
	
	if itemString then
		KEP_EventEncodeString(event, itemString)
	end
	
	return event
end

function decompileInventory(event)
	local inventoryString = KEP_EventDecodeString(event)
	local inventory = Events.decode(inventoryString)
	return inventory
end

function decompileInventoryItem(event)
	local itemString = KEP_EventDecodeString(event)
	local item = Events.decode(itemString)
	
	return item
end

function processTransaction(removeTable, addTable, preprocess)
	preproccess = preproccess or false
	local transactionEvent = nil
	if not preprocess then 
		transactionEvent = KEP_EventCreate("INVENTORY_HANDLER_PROCESS_TRANSACTION")
	else
		transactionEvent = KEP_EventCreate("INVENTORY_HANDLER_PREPROCESS_TRANSACTION")
	end
	transactionEvent = compileInventory(removeTable or {}, transactionEvent)
	transactionEvent = compileInventory(addTable or {}, transactionEvent)
	KEP_EventEncodeString(transactionEvent, tostring(MenuNameThis()))
	
	KEP_EventSetFilter(transactionEvent, 4)
	KEP_EventQueue(transactionEvent)	
end

function removeItem(index, pid, lootInfo)
	local removeItemEvent = KEP_EventCreate("INVENTORY_HANDLER_REMOVE_ITEM")
	KEP_EventEncodeNumber(removeItemEvent, index)
	KEP_EventEncodeNumber(removeItemEvent, pid)
	removeItemEvent = compileInventory(lootInfo or {}, removeItemEvent)

	KEP_EventSetFilter(removeItemEvent, 4)
	KEP_EventQueue(removeItemEvent)
end

function updateItem(index, pid, item, isCancel)
	local addItemEvent = KEP_EventCreate("INVENTORY_HANDLER_ADD_ITEM")
	KEP_EventEncodeNumber(addItemEvent, index)
	KEP_EventEncodeNumber(addItemEvent, pid)
	
	addItemEvent = compileInventoryItem(item, addItemEvent)
	
	if isCancel ~= nil then
		KEP_EventEncodeString(addItemEvent, tostring(isCancel))
	end

	KEP_EventSetFilter(addItemEvent, 4)
	KEP_EventQueue(addItemEvent)
end

function updateItemLocal(index, pid, item, isCancel)
	local addItemEvent = KEP_EventCreate("INVENTORY_HANDLER_ADD_ITEM_LOCAL")
	KEP_EventEncodeNumber(addItemEvent, index)
	KEP_EventEncodeNumber(addItemEvent, pid)
	
	addItemEvent = compileInventoryItem(item, addItemEvent)

	if isCancel ~= nil then
		KEP_EventEncodeString(addItemEvent, tostring(isCancel))
	end
	
	KEP_EventSetFilter(addItemEvent, 4)
	KEP_EventQueue(addItemEvent)
end

function addQuest(quest)
	local addQuestEvent = KEP_EventCreate("INVENTORY_HANDLER_ADD_QUEST")
	
	addQuestEvent = compileInventoryItem(quest, addQuestEvent)
	
	KEP_EventSetFilter(addQuestEvent, 4)
	KEP_EventQueue(addQuestEvent)
end

function completeQuest(questUNID, questInfo)
	local completeQuestEvent = KEP_EventCreate("INVENTORY_HANDLER_COMPLETE_QUEST")
	KEP_EventEncodeNumber(completeQuestEvent, questUNID)
	if questInfo then
		completeQuestEvent = compileInventoryItem(questInfo, completeQuestEvent)
	end
	
	KEP_EventSetFilter(completeQuestEvent, 4)
	KEP_EventQueue(completeQuestEvent)	
end

function abandonQuest(questUNID)
	local abandonQuestEvent = KEP_EventCreate("INVENTORY_HANDLER_ABANDON_QUEST")
	KEP_EventEncodeNumber(abandonQuestEvent, questUNID)
	
	KEP_EventSetFilter(abandonQuestEvent, 4)
	KEP_EventQueue(abandonQuestEvent)	
end

function fulfillQuest(questUNID, questInfo)
	local fulfillQuestEvent = KEP_EventCreate("INVENTORY_HANDLER_FULFILL_QUEST")
	KEP_EventEncodeNumber(fulfillQuestEvent, questUNID)
	if questInfo then
		fulfillQuestEvent = compileInventoryItem(questInfo, fulfillQuestEvent)
	end
	
	KEP_EventSetFilter(fulfillQuestEvent, 4)
	KEP_EventQueue(fulfillQuestEvent)	
end

function updateFulfillCount(questUNID, updateAmt, questInfo)
	local fulfillQuestEvent = KEP_EventCreate("INVENTORY_HANDLER_UPDATE_FULFILL_COUNT")
	KEP_EventEncodeNumber(fulfillQuestEvent, questUNID)
	KEP_EventEncodeNumber(fulfillQuestEvent, updateAmt)
	if questInfo then
		fulfillQuestEvent = compileInventoryItem(questInfo, fulfillQuestEvent)
	end
	
	KEP_EventSetFilter(fulfillQuestEvent, 4)
	KEP_EventQueue(fulfillQuestEvent)	
end

function usesFulfillCount(questType)
	if questType == "craft" or questType == "kill" or questType == "place" then
		return true
	else
		return false
	end
end

function updateDirty()
	local updateDirtyEvent = KEP_EventCreate("INVENTORY_HANDLER_UPDATE_DIRTY")
	KEP_EventSetFilter(updateDirtyEvent, 4)
	KEP_EventQueue(updateDirtyEvent)
end

function requestInventory(pid)
    if MenuIsOpen("Framework_InventoryHandler") then
	    local requestInventoryEvent = KEP_EventCreate("INVENTORY_HANDLER_REQUEST_INVENTORY")
	    KEP_EventEncodeNumber(requestInventoryEvent, pid)
	    KEP_EventSetFilter(requestInventoryEvent, 4)
	    KEP_EventQueue(requestInventoryEvent)	
    end
end

function getItemNameByUNID(UNID)
	local requestItemNameEvent = KEP_EventCreate("INVENTORY_HANDLER_REQUEST_ITEM_NAME")
	KEP_EventEncodeNumber(requestItemNameEvent, tonumber(UNID) or 0)
	KEP_EventSetFilter(requestItemNameEvent, 4)
	KEP_EventQueue(requestItemNameEvent)	
end

function checkSlotPrerequisites(dragItem, hoverItem)
	if dragItem and dragItem.properties and dragItem.properties.itemType and 
	   hoverItem and hoverItem.properties and hoverItem.properties.dropType then
		if hoverItem.properties.dropType ~= dragItem.properties.itemType then
			return false
		end
		
		if hoverItem.properties.slotType ~= dragItem.properties.slotType then
			return false
		end
	elseif dragItem and dragItem.properties and dragItem.properties.dropType and
		   hoverItem and hoverItem.properties and hoverItem.UNID ~= 0 and hoverItem.properties.itemType then
		if dragItem.properties.dropType ~= hoverItem.properties.itemType then
		   return false
		end
		
		if hoverItem.properties.slotType ~= dragItem.properties.slotType then
			return false
		end		
	end
	
	return true
end

function tablePopulated(testTable)
	return testTable ~= nil and next(testTable) ~= nil
end

function cloneItem(item)
    if type(item) ~= "table" then return item end
    local meta = getmetatable(item)
    local itemClone = {}
    for k, v in pairs(item) do
        if type(v) == "table" then
            itemClone[k] = cloneItem(v)
        else
            itemClone[k] = v
        end
    end
    setmetatable(itemClone, meta)
    return itemClone
end

-- Applies an image on a control
function applyImage(handle, image)
	if handle == nil then KEP_Log("Framework_InventoryHelper.lua applyImage() handle is nil") return end
	
	-- Apply default image
	if image == nil or image == 0 then		
		Element_AddTexture(Image_GetDisplayElement(handle), gDialogHandle, DEFAULT_IMAGE_TEXTURE)
		Element_SetCoords(Image_GetDisplayElement(handle), DEFAULT_IMAGE_COORDS.L, DEFAULT_IMAGE_COORDS.T, DEFAULT_IMAGE_COORDS.R, DEFAULT_IMAGE_COORDS.B)
	-- Apply a GLID
	else	
		local imgHandle = Image_GetDisplayElement(handle)
		if imgHandle and gDialogHandle then
			KEP_LoadIconTextureByID(tonumber(image), imgHandle, gDialogHandle)
		end
	end
end

function displayStatusMessage(message, color)
	if color == nil then color = {} end
	if tonumber(color.r) == nil then color.r = 255 end
	if tonumber(color.b) == nil then color.g = 0 end
	if tonumber(color.g) == nil then color.b = 0 end
	local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
	KEP_EventEncodeNumber(statusEvent, 0)
	KEP_EventEncodeString(statusEvent, message or "")
	KEP_EventEncodeNumber(statusEvent, 3)
	KEP_EventEncodeNumber(statusEvent, color.r)
	KEP_EventEncodeNumber(statusEvent, color.g)
	KEP_EventEncodeNumber(statusEvent, color.b)
	KEP_EventSetFilter(statusEvent, 4)
	KEP_EventQueue(statusEvent)
end