--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------------------------------------
----Framework_MediaFlagHandler
--Handler for Event Flags, Media Players, Claim Flags and System Flags
----------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")


local m_flags = nil
local m_events = nil
local m_mediaObjects = nil  --This should only have PIDS and Locations

local m_playlists = {} --Table of playlist information based on media PID
local m_newMedia = {} --Table of new media players. Only new players (or ones edited in GameEditor) should be allowed to assign a playlist from this menu

local MEDIA_EVENT_FILTER = 5

function onCreate( )
	Events.sendEvent("FRAMEWORK_REQUEST_EVENT_FLAGS")

	Events.registerHandler("FRAMEWORK_RETURN_FLAGS_FULL", onFlagsFullReturned)
	Events.registerHandler("FRAMEWORK_RETURN_MEDIA_OBJECTS_FULL", onMediaFullReturned)
	Events.registerHandler("FRAMEWORK_RETURN_EVENT_FLAGS_FULL", onEventFlagsFullReturned)
	Events.registerHandler("FRAMEWORK_RETURN_FLAG", onFlagReturned)
	Events.registerHandler("FRAMEWORK_RETURN_MEDIA_OBJECT", onMediaReturned)
	Events.registerHandler("FRAMEWORK_RETURN_EVENT_FLAG", onEventFlagReturned)
	Events.registerHandler("FRAMEWORK_REMOVE_FLAG", onFlagRemoved)
	Events.registerHandler("FRAMEWORK_REMOVE_MEDIA_OBJECT", onMediaRemoved)
	Events.registerHandler("FRAMEWORK_REMOVE_EVENT_FLAG", onEventFlagRemoved)

	Events.registerHandler("FRAMEWORK_MEDIA_PLAYING", playlistHandler)

	KEP_EventRegisterHandler( "UpdateMediaEventHandler", "UpdateMediaEvent", KEP.MED_PRIO )

	KEP_EventRegisterHandler( "onFlagsRequestedLocally", "FRAMEWORK_REQUEST_FLAGS_LOCALLY", KEP.HIGH_PRIO)
	--KEP_EventRegisterHandler( "onMediaRequestedLocally", "FRAMEWORK_REQUEST_MEDIA_LOCALLY", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "onEventFlagsRequestedLocally", "FRAMEWORK_REQUEST_EVENT_FLAGS_LOCALLY", KEP.HIGH_PRIO)

end

-----------------------------
---SERVER EVENT HANDLERS-----
-----------------------------
----Flags-----
function onFlagsFullReturned(event)
	m_flags = event.flags
	shareFlagsFull()
end

function onFlagReturned(event)
	if event.flag and m_flags then
		local flag = event.flag
		m_flags[tostring(flag.PID)] = flag
		shareFlag(flag.PID, true)
	end
end

function onFlagRemoved(event)
	if event.PID then
		m_flags[tostring(event.PID)] = nil
		shareFlag(event.PID, false)
	end
end

---Meida Players----
function onMediaFullReturned(event)
	m_mediaObjects = event.media
	shareMediaFull()
end

function onMediaReturned(event)
	if event.mediaObject and m_mediaObjects then
		local mediaObject = event.mediaObject
		m_mediaObjects[tostring(mediaObject.PID)] = mediaObject
		shareMediaObject(mediaObject.PID, true)
	end
end

function onMediaRemoved(event)
	if event.PID then
		m_mediaObjects[tostring(event.PID)] = nil
		shareMediaObject(event.PID, false)
	end
end


-----Event Flags-------
function onEventFlagsFullReturned(event)
	m_events = event.eventFlags
	shareEventFlagsFull()
end

function onEventFlagReturned(event)
	if event.PID then
		shareEventFlag(event.PID)
	end
end

function onEventFlagRemoved(event)
	if event.PID then
		m_events[tostring(event.PID)] = nil
	end
end


-----------------------------
----CLIENT EVENT HANDLERS----
-----------------------------

function onEventFlagsRequestedLocally(dispatcher, fromNetId, event, eventId, filter, objectId)
	shareEventFlagsFull()
end

function onFlagsRequestedLocally( dispatcher, fromNetId, event, eventId, filter, objectId )
	shareFlagsFull()
end


------------------------
-----LOCAL FUNCTIONS----
------------------------

shareFlagsFull = function( )
	if m_flags then
		local event = KEP_EventCreate("UPDATE_FLAGS_CLIENT_FULL")
		KEP_EventEncodeString(event, Events.encode(m_flags))
		KEP_EventQueue(event)
	end
end

shareFlag = function(PID, addFlag )
	if PID then
		local event = KEP_EventCreate("UPDATE_FLAG_CLIENT")
		KEP_EventEncodeNumber(event, tonumber(PID))
		if addFlag == true then
			KEP_EventEncodeString(event, Events.encode(m_flags[tostring(PID)]))
		end
		KEP_EventQueue(event)
	end
end

shareMediaObject = function(PID, addMedia)
	if PID then
		local event = KEP_EventCreate("UPDATE_MEDIA_CLIENT")
		KEP_EventEncodeString(event, tostring(PID))
		if addMedia == true then
			KEP_EventEncodeString(event, Events.encode(m_mediaObjects[tostring(PID)]))
		end
		KEP_EventQueue(event)
	end
end

shareMediaFull = function()
	if m_mediaObjects then
		local event = KEP_EventCreate("UPDATE_MEDIA_CLIENT_FULL")
		KEP_EventEncodeString(event, Events.encode(m_mediaObjects))
		KEP_EventQueue(event)
	end
end

shareEventFlag = function(PID)
	if PID and m_events then
		m_events[tostring(PID)] = 1
		local event = KEP_EventCreate("UPDATE_EVENT_FLAGS_CLIENT")
		KEP_EventEncodeString(event, tostring(PID))
		KEP_EventQueue(event)
	end
end

shareEventFlagsFull = function()
	if m_events then
		local event = KEP_EventCreate("UPDATE_EVENT_FLAGS_CLIENT_FULL")
		KEP_EventEncodeString(event, Events.encode(m_events))
		KEP_EventQueue(event)
	end
end


---------------------
--Media Player Specific---
--------------------------

function playlistHandler(event)

	if event.PID and event.playlist and event.playlist ~= "" then
		local PID = tostring(event.PID)
		m_playlists[PID] = {}
		m_playlists[PID].playlist = event.playlist
		m_playlists[PID].originalPlaylist = event.originalPlaylist
		m_newMedia[PID] = true

		--Event to get what is currently player on a local media player
		local evt = KEP_EventCreate("UpdateMediaEvent" )
		KEP_EventSetFilter( evt, 1 )
		KEP_EventEncodeString( evt,  PID )
		KEP_EventQueue( evt )
	end
end

function UpdateMediaEventHandler(dispatcher, fromNetid, event, eventid, filter, objId)

	if filter ~= MEDIA_EVENT_FILTER then return end

	local mediaPlaying = ""

	objId = KEP_EventDecodeNumber(event)
	mediaPlaying = KEP_EventDecodeString(event)

	objId = tostring(objId)
		if m_playlists[objId] and m_newMedia[objId] then
			local mediaObject = m_playlists[objId]
			local currentId = -1
			local originalId = 0
			
			--Check if current playing media is the same as the orginial playlist. If so, it needs to change if world owner changes it in editor
			if mediaObject.originalPlaylist and mediaObject.originalPlaylist ~= "" then
				Ok, url, params, volPct, radiusAudio, radiusVideo = KEP_GetMediaParamsOnDynamicObject(objId)
				currentId = string.match(params, "playlistId=(%d+)$") or "-1"
				originalId = string.match(mediaObject.originalPlaylist, "<asset_id>(%d-)</asset_id>") or string.match(mediaObject.originalPlaylist, "<asset_group_id>(%d-)</asset_group_id>") or "0"
			end

			if ((mediaPlaying == "" or tostring(mediaPlaying) == "<None>") or (tostring(currentId) == tostring(originalId))) and mediaObject.playlist and mediaObject.playlist ~= "" then
				local playlist = mediaObject.playlist
				local playlistID = string.match(playlist, "<asset_id>(%d-)</asset_id>") or string.match(playlist, "<asset_group_id>(%d-)</asset_group_id>") or "0"
				local offsiteURL = ""

				local mediaOffsiteURL = string.match(playlist, "<asset_offsite_id>(.-)</asset_offsite_id>") or ""
				offsiteURL = UnescapeHTML(mediaOffsiteURL)
				local enc_offsiteURL = KEP_UrlEncode(offsiteURL)
				offsiteURL = enc_offsiteURL
				local event = KEP_EventCreate( BrowserIds.BROWSER_REQUEST_EVENT_NAME )
				local playlistString = "changePlaylist?objId=" .. objId

				if offsiteURL and offsiteURL ~= "" then
					playlistString = playlistString.. "&assetId="
				else
					playlistString = playlistString .. "&plId="
				end

				playlistString = playlistString .. tostring(playlistID)

				if offsiteURL  and offsiteURL ~= "" then
					local mature = string.match(playlist, "<mature>(.-)</mature>") or "N"
					local assetType = string.match(playlist, "<asset_type_id>(%d-)</asset_type_id>") or "0"
					playlistString = playlistString .. "&mature=" .. mature .. "&asset_type=" .. assetType .. "&stop=0&offsite_url=" .. offsiteURL
				end

				KEP_EventEncodeString( event, playlistString )
				KEP_EventAddToServer( event )
				KEP_EventQueue( event )
				m_newMedia[objId] = nil
				Events.sendEvent("FRAMEWORK_UPDATE_LOCAL_PLAYLIST", {newPlaylist = playlist, PID = objId} )
			end
		end
	--end
end