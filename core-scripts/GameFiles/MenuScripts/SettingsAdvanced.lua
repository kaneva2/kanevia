--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

dofile("..\\MenuScripts\\LibClient_Common.lua")

local m_currentURL = nil

function onCreate()
	KEP_EventRegisterHandler( "zoneNavigationEventHandler",	"ZoneNavigationEvent",KEP.MED_PRIO )

	
	CheckBox_SetChecked(gHandles["chkHideHome"], GetHideHomeSetting())

	local ev = KEP_EventCreate( "ZoneNavigationEvent")
	KEP_EventSetFilter(ev, ZONE_NAV.CURRENT_URL_REQUEST)
	KEP_EventQueue( ev)

	setHomeURLString(GetHomeLink())
	setStartURLString(GetWorldStartString())

	local startIndex = GetStartingOption()
   
	if tonumber(startIndex) == 1 then
		RadioButton_SetChecked(gHandles["radioStart"], true)
	else
		RadioButton_SetChecked(gHandles["radioContinue"], true)
	end
end

function zoneNavigationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ZONE_NAV.CURRENT_URL_INFO then
		local url = KEP_EventDecodeString( event )
		m_currentURL = url
	end
end

function chkHideHome_OnCheckBoxChanged( checkboxHandle, checked )
	SetHideHomeSetting(checked)
end 

function Dialog_OnDestroy(dialogHandle)

	Helper_Dialog_OnDestroy( dialogHandle )
end


function btnSetHome_OnButtonClicked(buttonHandle)
	if m_currentURL then
		SetHomeLink(m_currentURL)
		setHomeURLString(m_currentURL)
	end
end 

function btnSetStart_OnButtonClicked(buttonHandle)
	if m_currentURL then
		SetStartingLink(m_currentURL)
		setStartURLString(m_currentURL)
		SetWorldStartString(m_currentURL)
		RadioButton_SetChecked(gHandles["radioStart"], true)
		SetStartingOption(1)
	end
end 

function radioStart_OnRadioButtonChanged( buttonHandle )
	SetStartingLink(GetWorldStartString())
	SetStartingOption(1)
end

function radioContinue_OnRadioButtonChanged( buttonHandle)
	SetStartingLink(m_currentURL)
	SetStartingOption(2)
end

function setHomeURLString(url)
	url = parseFriendlyNameFromURL(url)
	EditBox_SetText(gHandles["edHome"], " "..url, false)
end

function setStartURLString(url)
	url = parseFriendlyNameFromURL(url)
	EditBox_SetText(gHandles["edStart"], " "..url, false)
end

