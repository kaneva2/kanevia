--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------


ARMOR_SLOTS = 4
ARMOR_SLOT_TYPES = {"head", "chest", "arms", "legs"}

-- Base damage for each armor by level
ARMOR_BASE = {1,11,101,1001,10001}
-- ARMOR ranges available for each armor by level
ARMOR_RANGE = {9,89,899,8999,89999}

ARMOR_NOTCH_WIDTH = 20
ARMOR_BAR_HEIGHT = 8
ARMOR_BAR_MAX_WIDTH = 100

DEFAULT_MAX_DURABILITY = 1000 -- 1,000 hits or attacks per weapon/armor
DURABILITY_BY_LEVEL = { [1]=10, [2]=100, [3]=250, [4]=500, [5]=1000 }


-- Left, top, right, bottom
DURABILITY_COLORS = {	GREEN  = {19, 179, 20, 185},
						YELLOW = {31, 179, 32, 185},
						RED 	  = {25, 179, 26, 185}
					}
						  
						  
COLORS_VALUES =	{	["Common"] = {a = 255, r = 255, g = 255, b = 255},
					["Rare"] = {a = 255, r = 0, g = 150, b = 255},
					["Very Rare"] = {a = 255, r = 174, g = 0, b = 255},
					["Extremely Rare"] = {a = 255, r = 255, g = 144, b = 0},
					["Never"] = {a = 255, r = 255, g = 144, b = 0}
				}
						
						
ITEM_LEVEL_COORDS = {
					{left = 0, right = 15, top = 146, bottom = 161},
					{left = 16, right = 31, top = 146, bottom = 161},
					{left = 32, right = 47, top = 146, bottom = 161},
					{left = 0, right = 15, top = 162, bottom = 177},
					{left = 16, right = 31, top = 162, bottom = 177}
}

ITEM_LEVEL_RARITY_OFFSET = {["Common"] = 0, ["Rare"] = 48, ["Very Rare"] = 96, ["Extremely Rare"] = 144, ["Never"] = 144}


ITEM_RARITY_BG = {	["Common"] = {left = 208, right = 292, top = 167, bottom = 251},
					["Rare"] = {left = 293, right = 377, top = 167, bottom = 251},
					["Very Rare"] = {left = 378, right = 462, top = 167, bottom = 251},
					["Extremely Rare"] = {left = 378, right = 462, top = 82, bottom = 166},
					["Never"] = {left = 378, right = 462, top = 82, bottom = 166}
				}
						

