--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Crafting.lua
--
-- Displays available recipes for crafting
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("..\\MenuScripts\\FrameworkTooltipHelper.lua")

----------------------
-- Statics
----------------------
local KEY_C = 67
local RECIPES_PER_PAGE = 20

local CRAFT_TIME = 5

local COLORS_VALUES =	{	["Common"] = {a = 255, r = 255, g = 255, b = 255},
							["Rare"] = {a = 255, r = 0, g = 150, b = 255},
							["Very Rare"] = {a = 255, r = 174, g = 0, b = 255},
							["Extremely Rare"] = {a = 255, r = 255, g = 144, b = 0}
						}
						
local OCCUPIED_ANIMATION_GLID = 4546763

----------------------
-- Local vars
----------------------
-- All available recipes
local s_recipes = {} -- [{inputs:{UNID,count},output:{GLID,UNID,name}]
local s_blueprints = {}
local s_displayRecipes = {}
-- Player's inventory
local s_inventory = {} --[{UNID, count, name, itemType}]
local s_playerBlueprints = {}
-- Table of unique UNIDs and their total counts
local s_inventoryCounts = {} -- {UNID:count}
local s_currPage = 1
local s_selectedRecipe = 0
local s_crafted = 1
local s_craftQuantity = 1
local s_crafting = false
local s_craftTime = 0
local s_proximityRequirements = {}

local displayRecipeCount = 0

local s_selectedTab = "All"

----------------------
-- Local functions
----------------------
local setHover -- (index, hovering)
local cancelCraft -- ()
local resetCraftVariables -- ()
local displayRecipes -- ()
local selectRecipe -- ()
local craftItem -- ()
local setCounts -- ()
local setCraftableOverlays -- ()
local getLevelMultiplier -- (levelMultiplier)

-- Called when the menu is created
function onCreate()
	-- Register event handlers
	-- Register handlers for client-to-client events
	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateBlueprintsClientFull", "UPDATE_BLUEPRINTS_CLIENT_FULL", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("onCheckLootResponse", "INVENTORY_HANDLER_CHECK_LOOT_RESPONSE", KEP.HIGH_PRIO)
	-- Register handler for server-to-client events
	Events.registerHandler("FRAMEWORK_RETURN_RECIPES", recipeReturnHandler)
	Events.registerHandler("FRAMEWORK_RECIPE_UPDATED", onRecipeUpdated)
	Events.registerHandler("PLAYER_IN_CRAFTING_PROXIMITY", checkPlayerCraftingProximity)	
	Events.registerHandler("DROP_PLAYER_INVENTORY", onPlayerKilled)

	FWTooltipHelper.registerInventoryEvents()
	FWTooltipHelper.registerInventoryHandlers()
	
	-- Request the player's inventory
	requestInventory(INVENTORY_PID)
	requestInventory(BLUEPRINT_PID)
	
	-- Retrieve recipes from the server
	Events.sendEvent("FRAMEWORK_GET_RECIPES")

	Events.sendEvent("CHECK_PLAYER_CRAFTING_PROXIMITY")
	
	-- Define item button click function handlers
	for i = 1, RECIPES_PER_PAGE do
		-- On recipe click
		_G["btnItem" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			local s, e, index = string.find(Control_GetName(btnHandle), "(%d+)")
			s_selectedRecipe = tonumber(index) + (s_currPage-1)*RECIPES_PER_PAGE
			s_crafted = 1
			s_craftQuantity = 1
			EditBox_SetText(gHandles["edCount"], tostring(s_craftQuantity), false)
			-- Cancel crafting if we currently are
			if s_crafting then
				cancelCraft()
			else
				setCounts()
			end
			-- Enable the count edit box
			Control_SetEnabled(gHandles["edCount"], true)
			selectRecipe()
		end
		
		-- On recipe mouseEnter
		_G["btnItem" .. tostring(i) .. "_OnMouseEnter"] = function(btnHandle)
			local s, e, index = string.find(Control_GetName(btnHandle), "(%d+)")
			setHover(tonumber(index), true)
		end
		
		-- On recipe mouseLeave
		_G["btnItem" .. tostring(i) .. "_OnMouseLeave"] = function(btnHandle)
			local s, e, index = string.find(Control_GetName(btnHandle), "(%d+)")
			setHover(tonumber(index), false)
		end
	end
end

function onDestroy()
	KEP_SetCurrentAnimationByGLID(0)
	FWTooltipHelper.destroy()
end

-- On frame update
function Dialog_OnRender(dialogHandle, fElapsedTime)
	-- If we're currently crafting something
	if s_crafting then
		-- Crafting complete?
		if s_craftTime >= CRAFT_TIME then
			craftItem()
		-- Keep crafting
		else
			KEP_SetCurrentAnimationByGLID(OCCUPIED_ANIMATION_GLID)
			s_craftTime = math.min(s_craftTime + fElapsedTime, CRAFT_TIME)
			
			local craftPercent = s_craftTime / CRAFT_TIME
			Control_SetSize(gHandles["imgCraftProgress"], 84*craftPercent, 84)
		end
	end
end

-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

-- All tab
function btnAllTab_OnButtonClicked(buttonHandle)
	cancelCraft()
	s_selectedRecipe = 0
	s_currPage = 1
	s_selectedTab = "All"
	formatDisplayTable()	
	displayRecipes()
	selectRecipe()
	
	-- Adjust tab buttons
	Control_SetEnabled(buttonHandle, false)
	Control_SetEnabled(gHandles["btnRecipesTab"], true)
	Control_SetEnabled(gHandles["btnBlueprintsTab"], true)
	
	Control_SetEnabled(gHandles["btnPrevPage"], false)
end

-- Recipes tab
function btnRecipesTab_OnButtonClicked(buttonHandle)
	cancelCraft()
	
	s_selectedRecipe = 0
	s_currPage = 1
	s_selectedTab = "Recipes"
	formatDisplayTable()
	displayRecipes()
	selectRecipe()
	
	-- Adjust tab buttons
	Control_SetEnabled(buttonHandle, false)
	Control_SetEnabled(gHandles["btnAllTab"], true)
	Control_SetEnabled(gHandles["btnBlueprintsTab"], true)
	
	Control_SetEnabled(gHandles["btnPrevPage"], false)
end

-- Blueprints tab
function btnBlueprintsTab_OnButtonClicked(buttonHandle)
	cancelCraft()
	
	s_selectedRecipe = 0
	s_currPage = 1
	s_selectedTab = "Blueprints"
	formatDisplayTable()
	displayRecipes()
	selectRecipe()
	
	-- Adjust tab buttons
	Control_SetEnabled(buttonHandle, false)
	Control_SetEnabled(gHandles["btnAllTab"], true)
	Control_SetEnabled(gHandles["btnRecipesTab"], true)
	
	Control_SetEnabled(gHandles["btnPrevPage"], false)
end

-- Previous page
function btnPrevPage_OnButtonClicked(buttonHandle)
	s_currPage = s_currPage - 1
	Control_SetEnabled(buttonHandle, (s_currPage > 1))
	
	s_selectedRecipe = 0
	s_crafted = 1
	s_craftQuantity = 1
	
	displayRecipes()
	selectRecipe()
end

-- Next page
function btnNextPage_OnButtonClicked(buttonHandle)
	s_currPage = s_currPage + 1
	Control_SetEnabled(gHandles["btnPrevPage"], true)
	
	s_selectedRecipe = 0
	s_crafted = 1
	s_craftQuantity = 1
	
	displayRecipes()
	selectRecipe()
end

-- Quantity Up
function btnCountUp_OnButtonClicked(buttonHandle)
	Dialog_ClearFocus(gDialogHandle)
	s_craftQuantity = s_craftQuantity + 1
	
	EditBox_SetText(gHandles["edCount"], tostring(s_craftQuantity), false)
	
	-- Disable count down if we're at 1
	if s_craftQuantity > 1 then
		Control_SetEnabled(gHandles["btnCountDown"], true)
	end
	
	selectRecipe()
end

-- Called when an edit box is changed
function edCount_OnEditBoxChange(editBoxHandle, editBoxText)
	-- Clear out if edit box is empty or 0
	if editBoxText == "" or editBoxText == "0" then
		Control_SetEnabled(gHandles["btnCraft"], false)
		
		s_craftQuantity = 0
		
		-- Disable count down button
		Control_SetEnabled(gHandles["btnCountDown"], false)
	-- Key was entered
	else
		-- Extract the entered key
		local newKey = string.sub(editBoxText, -1)
		
		-- If a number wasn't input
		if tonumber(newKey) == nil then
			-- Parse out this key
			local adjustedText
			if string.len(editBoxText) > 1 then
				adjustedText = string.sub(editBoxText, 0, string.len(editBoxText)-1)
			else
				adjustedText = "1"
				s_craftQuantity = 1
				-- Disable count down button
				Control_SetEnabled(gHandles["btnCountDown"], false)
				selectRecipe()
			end
			
			if string.len(adjustedText) > 0 then
				EditBox_SetText(gHandles["edCount"], adjustedText, false)
			end
		
		-- Extract new count
		else
			s_craftQuantity = tonumber(editBoxText)
			
			-- Disable count down if we're at 1
			if s_craftQuantity == 1 then
				Control_SetEnabled(gHandles["btnCountDown"], true)
			end
			
			selectRecipe()
		end
	end
end

-- Quantity Down
function btnCountDown_OnButtonClicked(buttonHandle)
	Dialog_ClearFocus(gDialogHandle)
	s_craftQuantity = s_craftQuantity - 1
	
	EditBox_SetText(gHandles["edCount"], tostring(s_craftQuantity), false)
	
	-- Disable count down if we're at 1
	if s_craftQuantity == 1 then
		Control_SetEnabled(gHandles["btnCountDown"], false)
	end
	
	selectRecipe()
end

-- On craft
function btnCraft_OnButtonClicked(buttonHandle)
	Dialog_ClearFocus(gDialogHandle)
	-- If we're currently not crafting, this cancels
	if not s_crafting then
		local checkEvent = KEP_EventCreate("INVENTORY_HANDLER_CHECK_LOOT")
		local checkItem = deepCopy(s_displayRecipes[s_selectedRecipe].output)
		checkItem.count = 1
		KEP_EventEncodeNumber(checkEvent, 0)
		checkEvent = compileInventoryItem(checkItem, checkEvent)
		KEP_EventSetFilter(checkEvent, 4)
		KEP_EventQueue(checkEvent)
	else
		cancelCraft()
	end
	Control_SetVisible(gHandles["imgCraftCountBG"], s_crafting)
	Control_SetVisible(gHandles["stcCraftCount"], s_crafting)
	Static_SetText(gHandles["stcCraftCount"], "1 of "..tostring(s_craftQuantity))
	
	Control_SetVisible(gHandles["btnCountDown"], not s_crafting)
	Control_SetVisible(gHandles["edCount"], not s_crafting)
	Control_SetVisible(gHandles["btnCountUp"], not s_crafting)
	Control_SetVisible(gHandles["stcQuantityLabel"], not s_crafting)
	Control_SetEnabled(gHandles["btnPrevPage"], ((not s_crafting) and (s_currPage > 1)))
	Control_SetEnabled(gHandles["btnNextPage"], ((not s_crafting) and (#s_displayRecipes > s_currPage*RECIPES_PER_PAGE)))
	Control_SetEnabled(gHandles["btnAllTab"], not s_crafting)
	Control_SetEnabled(gHandles["btnBlueprintsTab"], not s_crafting)
	Control_SetEnabled(gHandles["btnRecipesTab"], not s_crafting)
end

-- Called when a key is pressed
function Dialog_OnKeyDown(dialogHandle, key, shift, ctrl, alt)
	-- 'C' pressed
	if key == KEY_C then
		MenuClose("Framework_Crafting.xml")
	end
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Called when crafting to check if the player has space for the item
function onCheckLootResponse(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local lootValid = (tostring(KEP_EventDecodeString(tEvent)) == "true")
	
	if lootValid then --If the crafting is valid, begin crafting
		-- Queue up the crafting
		s_crafting = true
		
		-- Change crafting button to "Cancel"
		Static_SetText(gHandles["btnCraft"], "Cancel")
	else --Otherwise display a status message with the error
		local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
		KEP_EventEncodeNumber(statusEvent, m_containerPID)
		KEP_EventEncodeString(statusEvent, "Backpack is full!")
		KEP_EventEncodeNumber(statusEvent, 3)
		KEP_EventEncodeNumber(statusEvent, 255)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventEncodeNumber(statusEvent, 0)
		KEP_EventSetFilter(statusEvent, 4)
		KEP_EventQueue(statusEvent)
		cancelCraft()
	end
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if s_inventory[updateIndex] then
		s_inventory[updateIndex] = updateItem
		updateInventory()
	end
end

function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	s_inventory = decompileInventory(tEvent)
	updateInventory()
end

function updateBlueprintsClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	s_playerBlueprints = decompileInventory(tEvent)
	displayRecipeCount = #s_displayRecipes
	if tablePopulated(s_displayRecipes) then
		s_filteredDisplayRecipes = {}
		formatDisplayTable()
		
		displayRecipes()
		
		
	end
	--selectRecipe()
end

-- Returns all available recipes from the server
function recipeReturnHandler(event)
	if event.initial then
		s_recipes = {}
		s_blueprints = {}
	end
	if event.recipes and type(event.recipes) == "table" then
		for UNID, properties in pairs(event.recipes) do
			s_recipes[UNID] = properties
		end
	end
	
	if event.blueprints and type(event.blueprints) == "table" then
		for UNID, properties in pairs(event.blueprints) do
			s_blueprints[UNID] = properties
		end
	end

	formatDisplayTable()	
	displayRecipes()
end

function onRecipeUpdated(event)
	--Cancel crafting when ANY recipe is updated
	cancelCraft()
	displayRecipeCount = #s_displayRecipes
	
	if event and s_recipes ~= nil then
		local UNID = tostring(event.itemUNID)
		s_recipes[UNID] = event.itemInfo

		-- Populate s_displayRecipes with these recipes
		formatDisplayTable()		
	
		displayRecipes()

		if s_selectedRecipe then
			selectRecipe()
		end
	end
end

function checkPlayerCraftingProximity(event)
	table.insert(s_proximityRequirements, {UNID = tonumber(event.UNID), craftingResource = event.craftingResource})
end

-- If this player's been killed
function onPlayerKilled(event)
	MenuCloseThis()
end

function formatDisplayTable()
	s_displayRecipes = {}
	if s_selectedTab == "All" then
		for i,v in pairs(s_recipes) do
			if not v.invalid then
				table.insert(s_displayRecipes, v)
			end
		end
				
		for _, blueprint in pairs(s_playerBlueprints) do
			for i,v in pairs(s_blueprints) do
				if tostring(blueprint.UNID) == tostring(v.UNID) and not v.invalid then
					table.insert(s_displayRecipes, v)
					break				
				end
			end
		end
	elseif s_selectedTab == "Recipes" then
		for i,v in pairs(s_recipes) do
			if not v.invalid then
				table.insert(s_displayRecipes, v)
			end
		end
	elseif s_selectedTab == "Blueprints" then
	
		for _, blueprint in pairs(s_playerBlueprints) do
			for i,v in pairs(s_blueprints) do
				if tostring(blueprint.UNID) == tostring(v.UNID) and not v.invalid then
					table.insert(s_displayRecipes, v)
					break				
				end
			end
		end
	end
end

function formatTooltip(item)
	local tooltip = {}
	if item then
		tooltip["header"] = {label = item.output.properties.name}

		if item.output.properties.rarity then
			tooltip["header"].color = COLORS_VALUES[item.output.properties.rarity]
		end
		
		tooltip["body"] = {label = "\"" .. item.output.properties.description .. "\""}

		if item.output.properties.itemType == ITEM_TYPES.WEAPON then
			local level = tostring(item.output.properties.level)
			local range = tostring(item.output.properties.range)
			
			if item.itemType == ITEM_TYPES.BLUEPRINT then
				tooltip["footer1"] = {label = "Blueprint: "..tostring(item.output.properties.name)}
				tooltip["footer2"] = {label = "Level: "..tostring(level)}
				tooltip["footer3"] = {label = "Range: "..tostring(range)}
			else
				tooltip["footer1"] = {label = "Weapon Level: "..tostring(level)}
				tooltip["footer2"] = {label = "Range: "..tostring(range)}
			end
		elseif item.output.properties.itemType == ITEM_TYPES.ARMOR then
			local slotTypeDisplay = (item.output.properties.slotType:gsub("^%l", string.upper)) 
			local level = tostring(item.output.properties.level)
			
			if item.itemType == ITEM_TYPES.BLUEPRINT then 
				tooltip["footer1"] = {label = "Blueprint: "..tostring(item.output.properties.name)}
				tooltip["footer2"] = {label = "Armor Level: "..tostring(level)}
				tooltip["footer3"] = {label = "Slot: "..tostring(slotTypeDisplay)}
			else
				tooltip["footer1"] = {label = "Armor Level: "..tostring(level)}
				tooltip["footer2"] = {label = "Slot: "..tostring(slotTypeDisplay)}
			end
		elseif item.output.properties.itemType == ITEM_TYPES.TOOL then
			local level = tostring(item.output.properties.level)
			
			if item.itemType == ITEM_TYPES.BLUEPRINT then
				tooltip["footer1"] = {label = "Blueprint: "..tostring(item.output.properties.name)}
				tooltip["footer2"] = {label = "Level: "..tostring(level)}
			else
				tooltip["footer1"] = {label = "Tool Level: "..tostring(level)}
			end

		elseif item.output.properties.itemType == ITEM_TYPES.CONSUMABLE then
			local consumeValueDisplay = item.output.properties.consumeValue .. "%" 

			
			if item.output.properties.consumeType == "energy" then
				tooltip["footer1"] = {label = "Energy Bonus: "..tostring(consumeValueDisplay)}
			else
				tooltip["footer1"] = {label = "Health Bonus: "..tostring(consumeValueDisplay)}
			end
		elseif item.output.properties.itemType == ITEM_TYPES.PLACEABLE then

		elseif item.output.properties.itemType == ITEM_TYPES.GENERIC then

		elseif item.output.properties.itemType == ITEM_TYPES.AMMO then

		end
	end

	return tooltip
end

-- -- -- -- -- -- --
-- Local functions
-- -- -- -- -- -- --

-- Populates the menu
updateInventory = function()
	s_inventoryCounts = {}
	-- Compile UNID counts
	for i=1, #s_inventory do
		if s_inventory[i].UNID ~= 0 then
			-- New UNID entry
			if s_inventoryCounts[s_inventory[i].UNID] == nil then
				s_inventoryCounts[s_inventory[i].UNID] = s_inventory[i].count
			-- Duplicate entry. Increment UNID count
			else
				s_inventoryCounts[s_inventory[i].UNID] = s_inventoryCounts[s_inventory[i].UNID] + s_inventory[i].count
			end
		end
	end
	-- Set new counts if a recipe is currently selected
	if s_selectedRecipe then
		setCounts()
	end
	-- Update overlays
	setCraftableOverlays()
end

-- Sets the hover state for a recipe
setHover = function(index, hovering)
	local recipeStatic   = gHandles["stcItem"..index]
	local recipeStaticBG = gHandles["imgItemStaticBG"..index]
	
	local itemHandle = gHandles["imgItem"..index]
	
	local parentY = Control_GetLocationY(itemHandle)
	local parentHeight = Control_GetHeight(itemHandle)
	
	-- Positions and sizes image based on state
	if hovering then
		Control_SetSize(recipeStatic, 84, parentHeight)
		Control_SetLocationY(recipeStatic, parentY)
		
		Control_SetSize(recipeStaticBG, 84, parentHeight)
		Control_SetLocationY(recipeStaticBG, parentY)
		Control_SetVisible(gHandles["imgItemOverlay"..tostring(index)], false)
	else
		Control_SetSize(recipeStatic, 84, 14)
		Control_SetLocationY(recipeStatic, parentY + 70)
		
		Control_SetSize(recipeStaticBG, 84, 14)
		Control_SetLocationY(recipeStaticBG, parentY + 70)
		Control_SetVisible(gHandles["imgItemOverlay"..tostring(index)], true)
	end
end

-- Cancels crafting
cancelCraft = function()
	-- Cancel crafting
	KEP_SetCurrentAnimationByGLID(0)
	s_crafting = false
	s_craftQuantity = s_craftQuantity - (s_crafted-1)
	EditBox_SetText(gHandles["edCount"], tostring(s_craftQuantity), not s_crafting)
	s_crafted = 1
	
	-- Disable the crafting bar
	Control_SetSize(gHandles["imgCraftProgress"], 0, 110)
	
	s_craftTime = 0
	
	-- Change cancel button to "Craft"
	Static_SetText(Control_GetStatic(gHandles["btnCraft"]), "Craft")
	Control_SetEnabled(gHandles["btnAllTab"], (not s_crafting and s_selectedTab ~= "All"))
	Control_SetEnabled(gHandles["btnBlueprintsTab"],(not s_crafting and s_selectedTab ~= "Blueprints"))
	Control_SetEnabled(gHandles["btnRecipesTab"], (not s_crafting and s_selectedTab ~= "Recipes"))
end

-- Display current recipes
displayRecipes = function()
	local displayStartIndex = 1+((s_currPage-1) * RECIPES_PER_PAGE)
	-- Construct table of available recipes by tab
	local displayEndIndex = math.min(#s_displayRecipes, (displayStartIndex + RECIPES_PER_PAGE - 1))
	
	local displayIndex = 1
	
	-- Iterate of the recipes we'll be displaying
	for i = displayStartIndex, displayEndIndex do
		local recipeName = s_displayRecipes[i].output.properties.name
		local recipeGLID = s_displayRecipes[i].output.properties.GLID
		local recipeUNID = s_displayRecipes[i].output.UNID
		local recipeType = s_displayRecipes[i].itemType
		
		-- Set item name
		Static_SetText(gHandles["stcItem"..displayIndex], recipeName)
		Control_SetVisible(gHandles["stcItem"..displayIndex], true)
		
		-- Enable item button
		Control_SetEnabled(gHandles["btnItem"..displayIndex], true)
		Control_SetVisible(gHandles["btnItem"..displayIndex], true)
		
		-- Enable the static background
		Control_SetVisible(gHandles["imgItemStaticBG"..displayIndex], true)
		
		
		applyImage(gHandles["imgItem" .. displayIndex], recipeGLID)
		-- Set image icon
		local imgHandle = Image_GetDisplayElement(gHandles["imgItem"..displayIndex])
		KEP_LoadIconTextureByID(recipeGLID, imgHandle, gDialogHandle)

		local tooltip = formatTooltip(s_displayRecipes[i])
		FWTooltipHelper.addFrameworkTooltip("btnItem"..displayIndex, tooltip)

		-- Enable item image		
		Control_SetVisible(gHandles["imgItem"..displayIndex], true)
		
		if recipeType == ITEM_TYPES.BLUEPRINT then
			Control_SetVisible(gHandles["imgBlueprint"..displayIndex], true)
			for i2, blueprint in pairs(s_playerBlueprints) do
					if tostring(s_displayRecipes[i].UNID) == tostring(blueprint.UNID) then
						if s_displayRecipes[i].oneUse == "true" then 
							if blueprint.count >= 0 then
								local quantityLabel = gHandles["stcItemCount" ..displayIndex]
								local blueprintCount = tonumber(blueprint.count)
								
								Control_SetVisible(gHandles["stcItemCountBG"..displayIndex], true)
								Control_SetVisible(quantityLabel, blueprintCount > 0)
								Static_SetText(quantityLabel, blueprintCount)
							else
								Control_SetVisible(gHandles["stcItemCountBG"..displayIndex], false)
								Control_SetVisible(gHandles["stcItemCount"..displayIndex], false)	
							end
						else
							Control_SetVisible(gHandles["stcItemCountBG"..displayIndex], false)
							Control_SetVisible(gHandles["stcItemCount"..displayIndex], false)	
						end
						
					end
			end		
			
		else
			Control_SetVisible(gHandles["imgBlueprint"..displayIndex], false)
			Control_SetVisible(gHandles["stcItemCountBG"..displayIndex], false)
			Control_SetVisible(gHandles["stcItemCount"..displayIndex], false)	
		end
		
		displayIndex = displayIndex + 1
	end
	
	-- Display overlays for each item
	setCraftableOverlays()
	
	-- Hide the rest of the recipes
	for i = displayIndex, RECIPES_PER_PAGE do
		Control_SetVisible(gHandles["stcItem"..i], false)
		Control_SetVisible(gHandles["btnItem"..i], false)
		Control_SetVisible(gHandles["imgItemStaticBG"..i], false)
		Control_SetVisible(gHandles["imgItem"..i], false)
		Control_SetVisible(gHandles["imgBlueprint"..i], false)
		Control_SetVisible(gHandles["stcItemCountBG"..i], false)
		Control_SetVisible(gHandles["stcItemCount"..i], false)	
	end
	
	Control_SetVisible(gHandles["imgOutline"], false)
	
	-- Set pagination buttons
	-- Do we have more recipes to display?
	Control_SetEnabled(gHandles["btnNextPage"], (#s_displayRecipes > s_currPage*RECIPES_PER_PAGE))
	Control_SetVisible(gHandles["stcPageCounts"], true)
	Static_SetText(gHandles["stcPageCounts"], "("..tostring(s_currPage).."/"..tostring(math.max(math.ceil(#s_displayRecipes/RECIPES_PER_PAGE),1))..")")
end

-- Select a recipe
selectRecipe = function()
	-- Turn on the "Required Items", "Need", and "Have" label
	Control_SetVisible(gHandles["stcRequiredItem"], (s_selectedRecipe > 0))
	Control_SetVisible(gHandles["stcNeedLabel"], (s_selectedRecipe > 0))
	Control_SetVisible(gHandles["stcHaveLabel"], (s_selectedRecipe > 0))

	-- Turn on all quantity controls
	Control_SetVisible(gHandles["imgCraftCountBG"], false)
	Control_SetVisible(gHandles["stcCraftCount"], false)
	Control_SetVisible(gHandles["stcQuantityLabel"], (s_selectedRecipe > 0))
	Control_SetVisible(gHandles["btnCountDown"], (s_selectedRecipe > 0))
	Control_SetEnabled(gHandles["btnCountDown"], (s_craftQuantity > 1))
	Control_SetVisible(gHandles["edCount"], (s_selectedRecipe > 0))
	Control_SetVisible(gHandles["btnCountUp"], (s_selectedRecipe > 0))
	Control_SetEnabled(gHandles["btnCountUp"], (s_selectedRecipe > 0))
	Control_SetVisible(gHandles["btnCraft"], (s_selectedRecipe > 0))
	
	-- Set image icon
	if s_displayRecipes[s_selectedRecipe] then
		applyImage(gHandles["imgCurrRecipe"], s_displayRecipes[s_selectedRecipe].output.properties.GLID)
		--[[local eh = Image_GetDisplayElement(gHandles["imgCurrRecipe"])
		KEP_LoadIconTextureByID(s_displayRecipes[s_selectedRecipe].output.properties.GLID, eh, gDialogHandle)]]
	end
	-- Enable the currRecipe image
	Control_SetVisible(gHandles["imgCurrRecipe"], (s_selectedRecipe > 0))
		Control_SetVisible(gHandles["imgCurrRecipeStaticBG"], (s_selectedRecipe > 0))
	
	-- Set the currRecipe static
	if s_displayRecipes[s_selectedRecipe] then
		local pageIndex = ( math.fmod(s_selectedRecipe, (RECIPES_PER_PAGE)) == 0 ) and RECIPES_PER_PAGE or math.fmod(s_selectedRecipe, (RECIPES_PER_PAGE))
		Static_SetText(gHandles["stcCurrRecipe"], s_displayRecipes[s_selectedRecipe].output.properties.name)
		-- Highlight the selected recipe
		
		local recipeX, recipeY = Control_GetLocationX(gHandles["imgItem"..tostring(pageIndex)]), Control_GetLocationY(gHandles["imgItem"..tostring(pageIndex)])
		Control_SetLocation(gHandles["imgOutline"], recipeX-1, recipeY-1)
		Control_SetVisible(gHandles["imgOutline"], true)
	end
	Control_SetVisible(gHandles["stcCurrRecipe"], (s_selectedRecipe > 0))	
	
	local hasNuf = setCounts()
end

-- Crafts the selected item
craftItem = function()
	-- Remove the required inputs
	for i = 1, #s_displayRecipes[s_selectedRecipe].inputs do
		local input = s_displayRecipes[s_selectedRecipe].inputs[i]
		-- Find all references to this UNID and delete
		local decrementCount = s_displayRecipes[s_selectedRecipe].inputs[i].count
		for j=1, #s_inventory do
			if s_inventory[j].UNID == s_displayRecipes[s_selectedRecipe].inputs[i].UNID then
				-- Decrement as many of this UNID as you can
				local tempCount = s_inventory[j].count
				s_inventory[j].count = s_inventory[j].count - decrementCount
				if s_inventory[j].count <= 0 then
					removeItem(j, INVENTORY_PID)
				else
					updateItem(j, INVENTORY_PID, s_inventory[j])
				end
				decrementCount = decrementCount - tempCount
				if decrementCount <= 0 then break end
			end
		end
	end
	
	local newItem = s_displayRecipes[s_selectedRecipe].output
	newItem.count = 1
	if newItem.properties.itemType == ITEM_TYPES.WEAPON or newItem.properties.itemType == ITEM_TYPES.ARMOR or newItem.properties.itemType == ITEM_TYPES.TOOL then
		newItem.instanceData = {levelMultiplier = getLevelMultiplier()}
	end
	
	if s_displayRecipes[s_selectedRecipe].oneUse == "true" then
		removeItem(s_displayRecipes[s_selectedRecipe].tableIndex, BLUEPRINT_PID)
	end
	
	-- Assign a loot source for metric detection
	newItem.lootInfo = {sourcePID = KEP_GetLoginName(), lootSource = "Crafting", recipeGIID = s_selectedRecipe}
	updateItem(0, 0, newItem)
	updateDirty()
	
	-- All done crafting
	if s_crafted >= s_craftQuantity then
		cancelCraft()
		
		Control_SetVisible(gHandles["imgCraftCountBG"], false)
		Control_SetVisible(gHandles["stcCraftCount"], false)
		Control_SetVisible(gHandles["btnCountUp"], true)
		Control_SetVisible(gHandles["btnCountDown"], true)
		Control_SetVisible(gHandles["edCount"], true)
		Control_SetVisible(gHandles["stcQuantityLabel"], true)
		Control_SetEnabled(gHandles["btnPrevPage"], (s_currPage > 1))
		Control_SetEnabled(gHandles["btnNextPage"], (#s_displayRecipes > s_currPage*RECIPES_PER_PAGE))
	
	-- Do we have more to craft?
	else
		s_crafted = s_crafted + 1
		Static_SetText(gHandles["stcCraftCount"], tostring(s_crafted).." of "..tostring(s_craftQuantity))
	end
	s_craftTime = 0
	
	local hasNuf = setCounts()
	Control_SetSize(gHandles["imgCraftProgress"], 0, 110)
	
	EditBox_SetText(gHandles["edCount"], tostring(s_craftQuantity), not s_crafting)
end

-- Sets the counts for the currently selected item
setCounts = function()
	-- Set required item count statics
	local hasNuf = true
	if s_displayRecipes[s_selectedRecipe] then			
		local color
		if s_displayRecipes[s_selectedRecipe].proxReq then
			local nearProximityReq = false
			local proxReqName = tostring(s_displayRecipes[s_selectedRecipe].proxReq)
			for i=1,#s_proximityRequirements do
				if s_displayRecipes[s_selectedRecipe].proxReq == s_proximityRequirements[i].craftingResource then
					nearProximityReq = true
					break
				end
			end

			if nearProximityReq then
				color = {a=255,r=255,g=255,b=255}
			else
				color = {a=255,r=255,g=0,b=0}
				hasNuf = false
			end
			
			local stcProxReqHandle = Static_GetDisplayElement(gHandles["stcReqProximity"])
			BlendColor_SetColor(Element_GetFontColor(stcProxReqHandle), 0, color)
			
			Static_SetText(gHandles["stcReqProximity"], proxReqName)
			Control_SetVisible(gHandles["stcReqProximity"], true)			
		else
			Control_SetVisible(gHandles["stcReqProximity"], false)
		end

		-- set recipe item if we have one
		for i=1, 3 do
			if s_displayRecipes[s_selectedRecipe].inputs[i] then
				local reqItem = s_displayRecipes[s_selectedRecipe].inputs[i]
				local reqNeedCount = reqItem.count * (s_craftQuantity - (s_crafted-1))
				
				-- Set required item name
				Static_SetText(gHandles["stcReqItem"..i], reqItem.name)
				Control_SetVisible(gHandles["stcReqItem"..i], true)
				
				-- Set required item count
				Static_SetText(gHandles["stcReqItemCount"..i], reqNeedCount)
				Control_SetVisible(gHandles["stcReqItemCount"..i], true)
				
				-- Get how many of this item the player has
				local reqHasCount = s_inventoryCounts[reqItem.UNID] or 0
				
				-- Set the have color to red if you don't got nuf
				if reqNeedCount > reqHasCount then
					-- Set color to red
					hasNuf = false
					color = {a=255,r=255,g=0,b=0}
				else
					-- Set color to white
					color = {a=255,r=255,g=255,b=255}
				end	
				local stcElementHandle = Static_GetDisplayElement(gHandles["stcHaveItemCount"..i])
				BlendColor_SetColor(Element_GetFontColor(stcElementHandle), 0, color)
				
				-- Set how many of this item the player has
				Static_SetText(gHandles["stcHaveItemCount"..i], reqHasCount)
				Control_SetVisible(gHandles["stcHaveItemCount"..i], true)
			-- Clear out recipe input if none exists
			else
				Control_SetVisible(gHandles["stcReqItem"..i], false)
				Control_SetVisible(gHandles["stcReqItemCount"..i], false)
				Control_SetVisible(gHandles["stcHaveItemCount"..i], false)
			end
		end
	-- If no recipe exists for this index
	else
		for i=1, 3 do
			Control_SetVisible(gHandles["stcReqItem"..i], false)
			Control_SetVisible(gHandles["stcReqItemCount"..i], false)
			Control_SetVisible(gHandles["stcHaveItemCount"..i], false)
		end
		Control_SetVisible(gHandles["stcReqProximity"], false)
	end
	-- Enable the crafting button?
	Control_SetEnabled(gHandles["btnCraft"], hasNuf or s_crafting)
	return hasNuf
end

-- Set if the designated item is craftable
setCraftableOverlays = function()
	local displayStartIndex = 1+((s_currPage-1) * RECIPES_PER_PAGE)
	local displayEndIndex = displayStartIndex + 19
	
	local displayIndex = 1
	
	-- Iterate of the recipes we'll be displaying
	for i = displayStartIndex, displayEndIndex do
		local hasNuf = true
		if s_displayRecipes[i] then
			-- Check each item's input count against how much the player has
			for i,v in pairs(s_displayRecipes[i].inputs) do
				-- Doesn't have enough
				if v.count > (s_inventoryCounts[v.UNID] or 0) then
					hasNuf = false
					break
				end
			end
		end
		
		Control_SetEnabled(gHandles["imgItemOverlay"..tostring(displayIndex)], not hasNuf)
		Control_SetVisible(gHandles["imgItemOverlay"..tostring(displayIndex)], not hasNuf)
		
		displayIndex = displayIndex + 1
	end
end

getLevelMultiplier = function(levelMultiplier)
	levelMultiplier = levelMultiplier or math.random()
	levelMultiplier = math.ceil(levelMultiplier*10000)*0.0001
	return levelMultiplier
end