--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_InventoryHelper.lua")
dofile("MenuHelper.lua")



local m_vendors = {} 	-- THE HOLDER OF ALL THE THINGS!! (current trades from player should be stored in here)
						-- output, input, cost, and time :: [{output={UNID, properties={name, GLID, itemType}, count}, input={UNID, properties={name, GLID, itemType}, count}, cost, time}]


local m_vendorPID = {} --list of all timed vendor PIDs
local m_shortestTimes = {} -- list of all shortest times for every Vendor
local m_finishedVendors = {} --list of the vendors that have a finshed item

local m_currTime = 0
local m_elapsedTime = 0
local m_checkTimes = false --flag to determine when we should start checkign times (Must be after we get current time)

function onCreate()

	Events.sendEvent("FRAMEWORK_GET_ALL_TIMED_VENDS")
	Events.registerHandler("FRAMEWORK_RETURN_TIMED_VENDS", returnTimedVends)
	Events.registerHandler("FRAMEWORK_RETURN_ALL_TIMED_VENDS", returnAllTimedVends)
	Events.registerHandler("RETURN_CURRENT_TIME", onReturnCurrentTime)

	Events.sendEvent("FRAMEWORK_REQUEST_CURRENT_TIME", {playerName = KEP_GetLoginName(), PID = m_containerPID})

	KEP_EventRegisterHandler("onUpdateTimedVendor", "FRAMEWORK_UPDATE_TIMED_VENDOR", KEP.HIGH_PRIO)
end

function Dialog_OnRender(dialogHandle, fElapsedTime)

	m_elapsedTime = m_elapsedTime + fElapsedTime
	if m_elapsedTime >= 1 then
		m_currTime = m_currTime + 1
		m_elapsedTime = m_elapsedTime - 1

		-- Update vendor times
		if m_checkTimes then
			for PID, time in pairs(m_shortestTimes) do
				local finished = false
				if time < m_currTime then
					finished = true
				end
				updateFinishedVends(PID, finished)
			end
		end
	end

	
end



-------------------------
-- Event Handlers
---------------------------
--change for a single Timed Vendor
function returnTimedVends(event)
	if event.PID == nil then return end
	if event.timedVends and type(event.timedVends) == "table" then
		m_vendors[tostring(event.PID)] = event.timedVends
	end
	getShortestTimes()
end

--change for all Timed Vendors
function returnAllTimedVends(event )
	if event.timedVends and type(event.timedVends) == "table" then
		m_vendors = deepCopy(event.timedVends)
		getShortestTimes()
	end
end

function onReturnCurrentTime(event)
	m_currTime = event.timestamp or m_currTime -- modding the time down for floating point calculation issues
	m_checkTimes = true
end

function onUpdateTimedVendor(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	
end
-----------------------
----local functions----
-----------------------

function formatNumberSuffix(count)
	-- If no need for formatting, then return
	if count < 10000 then return count end

	local strCount = tostring(count)
	local newCount = ""

	local format = { 	max = 0,
						min = 0,
					 	suffix = ""
					}

	-- Thousands
	if #strCount <= 6 then
		format.max = 6
		format.min = 3
		format.suffix = "K"
	-- Millions
	elseif #strCount <= 9 then
		format.max = 9
		format.min = 6
		format.suffix = "M"
	-- Billions
	elseif #strCount <= 12 then
		format.max = 12
		format.min = 9
		format.suffix = "B"
	-- Trillions
	elseif #strCount <= 15 then
		format.max = 15
		format.min = 12
		format.suffix = "T"
	end

	for i = 1, #strCount do
		local diff = #strCount - i
		if #strCount <= format.max then
			if diff >= format.min then
				newCount = newCount..string.sub(strCount,i,i)
			else
				return newCount..format.suffix
			end
		end
	end
end

function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end

getShortestTimes = function()
	for vendorPID, data in pairs(m_vendors) do
		local shortestTime = nil
		for index, finishTime in pairs(data) do
			if shortestTime == nil or shortestTime > finishTime then 
				shortestTime = finishTime
			end
			m_shortestTimes[vendorPID] = shortestTime
		end
		if shortestTime == nil then
			if m_finishedVendors[vendorPID] then
				Events.sendEvent("FRAMEWORK_HIGHLIGHT_TIMED_VENDOR", {spawnerPID = vendorPID, enable = false})
				m_finishedVendors[vendorPID] = nil
				m_shortestTimes[vendorPID] = nil
			end
		end
	end
end

updateFinishedVends = function (PID, finished )
	if finished == true  then
		if m_finishedVendors[PID] == nil  then
			m_finishedVendors[PID] = true
			Events.sendEvent("FRAMEWORK_HIGHLIGHT_TIMED_VENDOR", {spawnerPID = PID, enable = finished})
		end
	else
		if m_finishedVendors[PID] then
			Events.sendEvent("FRAMEWORK_HIGHLIGHT_TIMED_VENDOR", {spawnerPID = PID, enable = false})
			m_finishedVendors[PID] = nil
		end
	end
end