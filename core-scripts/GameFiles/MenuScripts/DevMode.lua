--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- dofiled from DevConsole & ChatMenu
dofile("..\\MenuScripts\\DevModeFunctions.lua")

function DevMode_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "ClickEventHandler", "ClickEvent", KEP.MED_PRIO )
end

g_devMode = false
function DevMode_OnKeyDown(dialogHandle, key, bShiftDown, bCtrlDown, bAltDown)

	-- Toggle Developer Mode Hotkeys (Shft+Ctrl+D)
	local devMode = ToBool(DevMode())
	if (devMode
		and bShiftDown
		and bCtrlDown
		and key == string.byte("D") 
	) then
		g_devMode = not g_devMode
		Log("KeyDown: devMode="..tostring(g_devMode))
	elseif (devMode
		and bShiftDown
		and bCtrlDown
		and key == string.byte("F")
	) then
		MenuOpen("DevModeMenu.xml")
	elseif (g_devMode == true) then 
		DevMode_Hotkey(key) 
	end
	
end

g_npcCnt = 0
g_ccId = 0
function DevMode_Hotkey(key)
	--Log("DevMode: key="..key)
	
	if (DevMode() == 0) then return end
		
	-- '0' = Crash Test
	if (key == string.byte("0")) then
		DevMode_CrashClient()
	end

	-- 'A' = My MemberAlts
	if (key == string.byte("A")) then
		DevMode_MemberAlts()
	end

	-- 'D' = Log Dialogs
	if (key == string.byte("D")) then
		DevMode_LogDialogs()
	end

	-- 'G' = Log MsgGzip
	if (key == string.byte("G")) then
		DevMode_LogMsgGzip()
	end

	-- 'M' = Log Metrics
	if (key == string.byte("M")) then
		DevMode_LogMetrics()
	end

	-- 'P' = Log MsgProc
	if (key == string.byte("P")) then
		DevMode_LogMsgProc()
	end

	-- 'Q' = Open QA Menu
	if (key == string.byte("Q")) then
		DevMode_OpenQAMenu()
	end

	-- 'R' = Log Resources ()
	if (key == string.byte("R")) then
		DevMode_LogResources()
	end

	-- 'S' = Log Script Refs
	if (key == string.byte("S")) then
		DevMode_LogScriptRefs()
	end

	-- 'W' = Log WebCalls
	if (key == string.byte("W")) then
		DevMode_LogWebcalls()
	end

	-- 'F' = Toggle physics window
	if (key == string.byte("F")) then
		local oldShow = KEP_ShowPhysicsWindow(-1)
		local newShow = math.fmod(oldShow + 1, 2)
		DevMode_ShowPhysicsWindow(newShow)
	end

	-- 'N' = NPC Create Character
	if (key == string.byte("N")) then
		g_npcCnt = g_npcCnt + 1
		g_ccId = g_ccId + 1
		if (g_ccId > 1) then g_ccId = 0 end
		local netId = 1000 + g_npcCnt 
		local name = "npc"..netId
		local posX, posY, posZ, rotDeg = KEP_GetPlayerPosition()
		local dbIndex = 0
		local glidA = 0
		local glid1 = 0
		local glid2 = 0
		local glid3 = 0
		local glid4 = 0
		if (g_ccId == 1) then -- female
			dbIndex = 12 --12
			glidAnim = { 3438457, 4025624, 3505722, 3438457}
			glidA = glidAnim[MathRandomInt(4)]
			local glidArm = { 2226, 1639, 1884, 2220, 2736, 3302, 4540065, 3296, 2236, 3076 }
			glid1 = glidArm[MathRandomInt(10)]
		else -- male
			dbIndex = 14 --14
			local glidAnim = { 3448878, 3924152, 3492658, 3470908 }
			glidA = glidAnim[MathRandomInt(4)]
			local glidArm = { 2218, 2247, 2224, 2403, 3382, 3388, 2680, 2682, 2406, 2364 }
			glid1 = glidArm[MathRandomInt(10)]
		end
 		Log("DevMode - NPC Spawn - '"..name.."'")
		local mo = KEP_npcSpawn( {
			name=name, 
			charConfigId=g_ccId, 
			dbIndex=dbIndex, 
			netId=netId, 
			pos={x=posX, y=posY, z=posZ},
			rotDeg=rotDeg, 
			glidAnimSpecial=glidA
		} )
		local scale = KEP_GetRandomNumber() + 0.5
		KEP_ScaleRuntimePlayer(mo, scale, scale, scale)
		KEP_ArmInventoryItemOnPlayer(mo, glid1)
--		KEP_DisarmInventoryItemOnPlayer(mo, glid1)
--		KEP_npcDeSpawn( { netId=netId } )
		local armedGlids = KEP_GetPlayerArmedItems(mo)
		Log("armedItems="..#armedGlids)
		for i=1,#armedGlids do
			Log(" ... glid<"..armedGlids[i]..">")
		end
	end
end

-- DRF - ClickEvent: button (0=left 1=right), objType, objId, control, shift, alt, ...
function ClickEventHandler(dispatcher, fromNetId, event, eventId, filter, unused)
	if not ToBool(DevMode()) then return end
	local button = KEP_EventDecodeNumber(event)
	local objType = KEP_EventDecodeNumber(event) or ObjectType.NONE
	local objId = KEP_EventDecodeNumber(event) or 0
	local control = ToBool(KEP_EventDecodeNumber(event) or 0)
	local shift = ToBool(KEP_EventDecodeNumber(event) or 0)
	local alt = ToBool(KEP_EventDecodeNumber(event) or 0)
	if ((button == 0) and control and shift) then
		Log("DevMode - Attach Camera - "..ObjectToString(objId, objType))
		KEP_AttachActiveViewportCameraToObject(objType, objId)
	end
end