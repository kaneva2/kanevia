--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

SHOW_TOOLTIP_EVENT = "ShowToolTipEvent"
SHOW_TOOLTIP_NOW_EVENT = "ShowToolTipNowEvent"

LEADING_BORDER 		= 12
TAILING_BORDER 		= 12
POINTER_SIZE 		= 20
WINDOW_BORDER		= 5
DEFAULT_MAIN_HEIGHT = 15
DEFAULT_SUB_HEIGHT 	= 25
TEXT_BUFFER			= 3

TOOLTIP_DELAY 		= 750

g_title = ""
g_body = ""
g_mousePositionX = 0
g_mousePositionY = 0

g_preferredWidth = 10

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "showToolTipEventHandler", SHOW_TOOLTIP_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "showToolTipNowEventHandler", SHOW_TOOLTIP_NOW_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( SHOW_TOOLTIP_NOW_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	
	-- DRF - Passthrough Clicks 
	MenuSetPassthroughThis(true)

	g_preferredWidth = Control_GetWidth(gHandles["stcToolTip_main"])
	updateToolTip("", "", 0, 0) -- hide immediately
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

g_ShowInASecondEvent = nil

function showToolTipEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	local title = KEP_EventDecodeString(event)
	local body = KEP_EventDecodeString(event)
	local mouseX = KEP_EventDecodeNumber(event)
	local mouseY = KEP_EventDecodeNumber(event)

--	MenuClose("Framework_Tooltip.xml") -- Close here just in case others not hit (eg menu has been closed)

	if g_ShowInASecondEvent ~= nil then
		KEP_EventCancel( g_ShowInASecondEvent)
		g_ShowInASecondEvent = nil
	end

	-- Hopefully temporary - want to find solution that isn't hardcoding.
	-- Fixes bug where framework tooltip event sent out after menu is closed (or while closing) and ends up here unintentionally
	local frameworkTooltip = string.find(body, "^(.-)FRAMEWORKTOOLTIP(%d-)$") or string.find(body, "^(.-)FWTooltip(.-)$")

	if frameworkTooltip or (title == "" and body == "") then
		--hide immediately
		updateToolTip("", "", mouseX, mouseY)
	else
		--queue up a ShowToolTipNowEvent in a second
		g_ShowInASecondEvent = KEP_EventCreate( SHOW_TOOLTIP_NOW_EVENT)
		KEP_EventEncodeString(g_ShowInASecondEvent, title)
		KEP_EventEncodeString(g_ShowInASecondEvent, body)
		KEP_EventEncodeNumber(g_ShowInASecondEvent, mouseX)
		KEP_EventEncodeNumber(g_ShowInASecondEvent, mouseY)
		KEP_EventQueueInFutureMS(g_ShowInASecondEvent, TOOLTIP_DELAY)
	end
end

function showToolTipNowEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	if g_ShowInASecondEvent == event then
		g_ShowInASecondEvent = nil
	end

	local title = KEP_EventDecodeString(event)
	local body = KEP_EventDecodeString(event)
	local mouseX = KEP_EventDecodeNumber(event)
	local mouseY = KEP_EventDecodeNumber(event)

	if title == g_title and body == g_body then
	-- request to show same stuff, so ignore it
	else
		-- request to update
		updateToolTip(title, body, mouseX, mouseY)
	end
end

function updateToolTip(title, body, mouseX, mouseY)

	g_mousePositionX = mouseX
	g_mousePositionY = mouseY
	g_body = body
	g_title = title

	if g_body == "" and g_title == "" then
		-- request to hide
		MenuSetMinimizedThis(true)
		return
	end

	MenuSetMinimizedThis(false)

	MenuBringToFrontThis()

	local x = g_mousePositionX
	local y = g_mousePositionY

	Static_SetText(gHandles["stcToolTip_main"], g_title)
	Static_SetText(gHandles["stcToolTip_sub"], 	g_body)

	-- Reset the text boxes to their default size
	Control_SetSize(gHandles["stcToolTip_main"], 	g_preferredWidth, DEFAULT_MAIN_HEIGHT)
	Control_SetSize(gHandles["stcToolTip_sub"], 	g_preferredWidth, DEFAULT_SUB_HEIGHT)

	--Resize the box to hold the new text
	local headerWidth = Static_GetStringWidth(gHandles["stcToolTip_main"], g_title)
	local bodyWidth   = Static_GetStringWidth(gHandles["stcToolTip_sub"], g_body)
	local maxWidth    = math.max(headerWidth, bodyWidth)
	maxWidth = math.min(maxWidth, g_preferredWidth)
	local headerHeight = Static_GetStringHeight(gHandles["stcToolTip_main"], g_title)
	local bodyHeight = Static_GetStringHeight(gHandles["stcToolTip_sub"], g_body)
	local totalHeight = headerHeight + bodyHeight

	-- Decide location of subtext on the y axis; if there is a header add the buffer
	local subTextY = LEADING_BORDER + headerHeight
	if headerHeight > 0 then
		subTextY = subTextY + TEXT_BUFFER
		totalHeight = totalHeight + TEXT_BUFFER
	end

	-- shift the tooltip right and down about the size of the cursor
	x = x + POINTER_SIZE + TAILING_BORDER
	y = y + POINTER_SIZE + TAILING_BORDER

	--Check x,y against the edges of the main window
	local winW, winH = KEP_GetClientRectSize()
	local dialogWidth = maxWidth + TAILING_BORDER + LEADING_BORDER
	local dialogHeight = totalHeight + TAILING_BORDER + LEADING_BORDER

	if x + dialogWidth + WINDOW_BORDER > winW then
		x = winW - dialogWidth - WINDOW_BORDER
	elseif x - WINDOW_BORDER < 0 then
		x = WINDOW_BORDER
	end

	if y + dialogHeight + WINDOW_BORDER > winH then
		y = winH - dialogHeight - WINDOW_BORDER
	elseif y - WINDOW_BORDER < 0 then
		y = WINDOW_BORDER
	end

	-- Set Dialog + Strings into position and size
	MenuSetLocationThis(x, y)
	MenuSetSizeThis(dialogWidth, dialogHeight)

	Control_SetLocation(gHandles["stcToolTip_main"], LEADING_BORDER, LEADING_BORDER)
	Control_SetSize(gHandles["stcToolTip_main"], maxWidth, headerHeight)
	
	Control_SetLocation(gHandles["stcToolTip_sub"], LEADING_BORDER, subTextY)
	Control_SetSize(gHandles["stcToolTip_sub"], maxWidth, bodyHeight)
end
