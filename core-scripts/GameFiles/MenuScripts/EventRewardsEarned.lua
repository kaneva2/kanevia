--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Event Rewards Earned slide-in

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\GameZoneHelper.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")
dofile("..\\MenuScripts\\CAM.lua")
dofile("..\\ClientScripts\\CSZoneLiteProxy.lua")

g_time = 0
g_boxtime = 0
g_maxdist = 0
g_mindist = 0
g_totaldist = 0
g_boxout = false
g_slidestarted = false

function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler("EventRewardsEarnedEventHandler", "EventRewardsEarnedEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("ProgressEventHandler", "ProgressEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "clientResizeEventHandler",	"ClientResizeEvent",	KEP.MED_PRIO )
end

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister("EventRewardsEarnedEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	local screenWidth = Dialog_GetScreenWidth(dialogHandle)
	local screenHeight = Dialog_GetScreenHeight(dialogHandle)
	Dialog_SetLocation(dialogHandle, screenWidth, 68)

	--sets initial location of menu box
	g_maxdist = screenWidth - Dialog_GetWidth(dialogHandle)
	g_mindist = screenWidth + Dialog_GetWidth(dialogHandle)
	g_totaldist = Dialog_GetWidth(dialogHandle)

	if IsZoneCompletelyLoaded() then
		StartSlideIn()
	end
end

function Dialog_OnDestroy(dialogHandle)

	local player = KEP_GetPlayer()
	local netId = GetSet_GetNumber(player, MovementIds.NETWORKDEFINEDID)
	KEP_PlayParticleEffectOnPlayer(netId, "bip01", 1, 3697188 ) -- Coin Fountain

	Helper_Dialog_OnDestroy( dialogHandle )
end

-- Controls animation of slide in, if the animation has started
function Dialog_OnRender(dialogHandle, fElapsedTime)

	if g_slidestarted then
		local new_x = Dialog_GetLocationX(dialogHandle)
		local totalTime = 2

		if g_time > 0 and g_boxout == false then
			new_x = math.floor(new_x - g_totaldist*(fElapsedTime/totalTime))
			MenuSetLocationThis(new_x, Dialog_GetLocationY(dialogHandle))
			if new_x <= g_maxdist and g_boxout == false then
				g_boxtime = g_boxtime + (fElapsedTime/totalTime)
				MenuSetLocationThis(g_maxdist, Dialog_GetLocationY(dialogHandle))
				if g_boxtime >= 1 then -- time the number of seconds here box is still
					g_boxout = true
				end
			end
			MenuBringToFrontThis()
		end

		if g_time >0 and g_boxout == true then
			new_x = math.ceil(new_x + g_totaldist*(fElapsedTime/totalTime))
			MenuSetLocationThis(new_x, Dialog_GetLocationY(dialogHandle))
			MenuBringToFrontThis()
			if new_x >= g_mindist then
				MenuCloseThis()
			end
		end

		g_time = g_time + fElapsedTime
	end

end

function clientResizeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	g_maxdist = screenWidth - Dialog_GetWidth(gDialogHandle)
	g_mindist = screenWidth + Dialog_GetWidth(gDialogHandle)

	MenuSetLocationThis(g_maxdist, 68)
end

function EventRewardsEarnedEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid)
	-- Update rewards in slide-in
	local rewards = KEP_EventDecodeNumber(event)
	Static_SetText(gHandles["stcRewards"],"+ "..rewards)
	Log("EventRewardsEarnedEventHandler(): rewards count = " .. rewards)
end

function ProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	Log("ProgressEventHandler(): filter="..filter)
	if (filter == PROGRESS_CLOSED) then
		StartSlideIn()
	end
end

function IsZoneCompletelyLoaded()
	return MenuIsClosed("ProgressMenu.xml")
end

function StartSlideIn()
	g_slidestarted = true
end
