--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\ClientScripts\\CSZoneLiteProxy.lua")
dofile("..\\MenuScripts\\UGCZoneLiteProxy.lua")

-- List of Communities
-- where "community"
--  community.name
--  community.index
--  community.instance
--  community.listIndex // position in lbxlist
g_communityList = {}

-- List of zones (see UGCZoneLiteProxy.lua)
-- + zone.listIndex  // position in lbxlist
g_zoneDataList = {}

g_SelectedFreeZoneBtn = -1

g_confirm = false 

gCZ_gradCoords =  {}

-- internal enums
gCZ_MenuMode = {}
gCZ_MenuMode.NOMODE  = 0
gCZ_MenuMode.NEW     = 1
gCZ_MenuMode.IMPORT  = 2

gCZ_DisplayMode = {}
gCZ_DisplayMode.SHOW_NEW    = 0
gCZ_DisplayMode.SHOW_NEW_HEIGHT    = 524
gCZ_DisplayMode.SHOW_IMPORT = 1
gCZ_DisplayMode.SHOW_IMPORT_HEIGHT = 490
gCZ_DisplayMode.SHOW_STATUS = 2
gCZ_DisplayMode.SHOW_STATUS_HEIGHT = 305
gCZ_DisplayMode.SHOW_CONFIRM = 3
gCZ_DisplayMode.SHOW_CONFIRM_HEIGHT = 305

gCZ_MenuData = {}
gCZ_MenuData.Mode                 = gCZ_MenuMode.NOMODE
gCZ_MenuData.DisplayMode          = gCZ_DisplayMode.SHOW_IMPORT
gCZ_MenuData.TrackStatus          = 0                              -- (0 false -- 1 true)
gCZ_MenuData.CopyFlag             = 1                              -- (1 All -- 2 ScriptedOnly --3 NonScriptedOnly)
gCZ_MenuData.ImportZoneType       = UGCZLPZoneTypes.CURRENT_TYPE
gCZ_MenuData.ZoneIndex            = 0
gCZ_MenuData.DeleteDynamicObjects = 1                              -- (0 delete all dynamic objects) (1 delete only dynamic objects with NO scripts)
gCZ_MenuData.ZoneName             = ""

gCZ_WorkingData = {}
gCZ_WorkingData.Result    = 0 -- (0 false / 1 true)
gCZ_WorkingData.Key       = 0

gCZ_ShoppingSuffix = {}
gCZ_ShoppingSuffix[1]  = GameGlobals.WEB_SITE_PREFIX_SHOP..CATALOG_SUFFIX.."s=club&cId=1000"    -- "Clubs"
gCZ_ShoppingSuffix[2]  = GameGlobals.WEB_SITE_PREFIX_SHOP..CATALOG_SUFFIX.."s=castle&cId=1000"  -- "Castles"
gCZ_ShoppingSuffix[3]  = GameGlobals.WEB_SITE_PREFIX_SHOP..CATALOG_SUFFIX.."s=mansion&cId=1000" -- "Mansions"
gCZ_ShoppingSuffix[4]  = GameGlobals.WEB_SITE_PREFIX_SHOP..CATALOG_SUFFIX.."s=town&cId=1000"    -- "Towns"
gCZ_ShoppingSuffix[5]  = GameGlobals.WEB_SITE_PREFIX_SHOP..CATALOG_SUFFIX.."s=city&cId=1000"    -- "Cities"
gCZ_ShoppingSuffix[6]  = GameGlobals.WEB_SITE_PREFIX_SHOP..CATALOG_SUFFIX.."s=horror%20haunted%20zombie%20vampire&cId=1000" -- "Horror"
gCZ_ShoppingSuffix[7]  = GameGlobals.WEB_SITE_PREFIX_SHOP..CATALOG_SUFFIX.."s=scifi%20sci%20fi%20spaceship%20alien%20aliens%20future&cId=1000" -- "SciFi"
gCZ_ShoppingSuffix[8]  = GameGlobals.WEB_SITE_PREFIX_SHOP..CATALOG_SUFFIX.."s=west%20western&cId=1000"   -- "Western"
gCZ_ShoppingSuffix[9]  = GameGlobals.WEB_SITE_PREFIX_SHOP..CATALOG_SUFFIX.."s=egypt%20egyptian&cId=1000" -- "Egyptian"
gCZ_ShoppingSuffix[10] = GameGlobals.WEB_SITE_PREFIX_SHOP

-- 6 'panels' = [FreeZones / Browse / Zones / NewZone / Status / Controls]
gctrls_Panels = {}
gctrls_Panels["FreeZones"] =
{
	"imgFreeStartZones",
	"stcFreeStartingZones",
	"imgTopBarGradient",
	"imgGradiantFreeZone1",
	"imgFreeZone1",
	"stcFreeZone1",
	"btnFreeZone1",
	"imgGradiantFreeZone2",
	"imgFreeZone2",
	"stcFreeZone2",
	"btnFreeZone2",
	"imgGradiantFreeZone3",
	"imgFreeZone3",
	"stcFreeZone3",
	"btnFreeZone3",
	"imgGradiantFreeZone4",
	"imgFreeZone4",
	"stcFreeZone4",
	"btnFreeZone4",
	"imgWhiteVertRight",
	"imgWhiteVertMid",
	"imgWhiteVertLeft",
	"imgDarkVertRight",
	"imgDarkVertLeft",
	"imgDarkVertMiddle",
	"horizEndFreeZones",
}

gctrls_Panels["Browse"] =
{
	"stcBrowseMemberBold",
	"stcBrowseShop",
	"btnBrowse1",
	"btnBrowse2",
	"btnBrowse3",
	"btnBrowse4",
	"btnBrowse5",
	"btnBrowse6",
	"btnBrowse7",
	"btnBrowse8",
	"btnBrowse9",
	"btnBrowse10",
	"horizEndBrowse",
}

gctrls_Panels["Zones"] =
{
	"stcMyZonesBold",
	"lbxList",
	"btnRefreshList",
	"horizEndZones"
}

gctrls_Panels["NewZone"] =
{
	"stcNewZoneName",
	"edNewZoneName",
	"horizEndNewZone"
}

gctrls_Panels["Status"] =
{
	"UpperLeft_Slice",
	"UpperCenter_Slice",
	"UpperRight_Slice",
	"MiddleLeft_Slice",
	"MiddleCenter_Slice",
	"MiddleRight_Slice",
	"LowerLeft_Slice",
	"LowerCenter_Slice",
	"LowerRight_Slice",
	"stcStatus",
	"pbarStatus",
	"txtStatus",
	"stcResult",
	"btnSuccess",
	"btnFail",
}

gctrls_Panels["Controls"] =
{
	"btnOK",
	"btnCancel"
}

gctrls_Panels["Confirm"] =
{
		"stcConfirm1",
		"stcConfirm2",
		"stcConfirm3",
		"stcConfirm4",
		"stcConfirm5",
		"stcConfirm6",
		"stcConfirm7",
		"stcConfirm8"
}

gCZ_ProgressBar = {}
gCZ_ProgressBar.EMPTY     =   0
gCZ_ProgressBar.COMPLETE  = 240
gCZ_ProgressBar.INCREMENT =  40  -- (complete/6)

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	UGCZoneLiteProxy_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)

	KEP_EventRegisterHandler( "BrowserPageReadyHandler",          "BrowserPageReadyEvent",           KEP.MED_PRIO)
	KEP_EventRegisterHandler( "ImportZoneSuccessEventHandler",    "CSZLP_ImportZoneSuccessEvent",    KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "ImportChannelSuccessEventHandler", "CSZLP_ImportChannelSuccessEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "CreateZoneMenuSetupEventHandler",  "CreateZoneMenuSetupEvent",        KEP.MED_PRIO)
	KEP_EventRegisterHandler( "ImportZoneConfirmEventHandler",  "ImportZoneConfirmEvent",        KEP.MED_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	UGCZoneLiteProxy_InitializeKEPEvents(dispatcher, handler, debugLevel)

	KEP_EventRegister( "CreateZoneMenuSetupEvent",    KEP.MED_PRIO)
	KEP_EventRegister( "CreateZoneMenuFinishedEvent", KEP.MED_PRIO)
	KEP_EventRegister(  "ImportZoneConfirmEvent", KEP.MED_PRIO)
end

function ImportZoneConfirmEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_confirm = true 
	btnOK_OnButtonClicked(gHandles["btnOK"])
end 

function CreateZoneMenuSetupEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- only process this event if we have not been given data already
	if (gCZ_MenuData.Mode ~= gCZ_MenuMode.NOMODE) then
		return
	end

	if (filter == gCZ_MenuMode.NEW) then
		gCZ_MenuData.Mode                 = filter
		gCZ_MenuData.DisplayMode          = KEP_EventDecodeNumber(event)
		gCZ_MenuData.TrackStatus          = KEP_EventDecodeNumber(event)
		gCZ_MenuData.CopyFlag             = KEP_EventDecodeNumber(event)
		gCZ_MenuData.ImportZoneType       = KEP_EventDecodeNumber(event)
		gCZ_MenuData.ZoneIndex            = KEP_EventDecodeNumber(event)
		gCZ_MenuData.DeleteDynamicObjects = KEP_EventDecodeNumber(event)
		gCZ_MenuData.ZoneName             = KEP_EventDecodeString(event)
	elseif (filter == gCZ_MenuMode.IMPORT) then
		gCZ_MenuData.Mode                 = filter
		gCZ_MenuData.DisplayMode          = KEP_EventDecodeNumber(event)
		gCZ_MenuData.TrackStatus          = KEP_EventDecodeNumber(event)
		gCZ_MenuData.CopyFlag             = KEP_EventDecodeNumber(event)
		gCZ_MenuData.ImportZoneType       = KEP_EventDecodeNumber(event)
		gCZ_MenuData.ZoneIndex            = KEP_EventDecodeNumber(event)
		gCZ_MenuData.DeleteDynamicObjects = KEP_EventDecodeNumber(event)
		gCZ_MenuData.ZoneName             = KEP_EventDecodeString(event)
	else
		return
	end

	-- check importZoneType if it is set to current
	-- then use the current zone type
	if (gCZ_MenuData.ImportZoneType == UGCZLPZoneTypes.CURRENT_TYPE) then
		gCZ_MenuData.ImportZoneType = InstanceId_GetType(KEP_GetCurrentZoneIndex())
	end

	--  One time only set Dialog Controls Y position
	local YFactor = 0
	if (gCZ_MenuData.DisplayMode == gCZ_DisplayMode.SHOW_NEW) then
		YFactor = 34
	elseif (gCZ_MenuData.DisplayMode == gCZ_DisplayMode.SHOW_IMPORT) then
		YFactor = 0
	end

	Dialog_SetSize(gDialogHandle, Dialog_GetWidth(gDialogHandle), Dialog_GetHeight(gDialogHandle) + YFactor)
	for i,ctrlName in pairs(gctrls_Panels["Controls"]) do
		local hctrl = gHandles[ctrlName]
		Control_SetLocation(hctrl, Control_GetLocationX(hctrl), Control_GetLocationY(hctrl) + YFactor)
	end

	-- This is where we trigger the form loading logic
	toggleDialogPanels(gCZ_MenuData.DisplayMode)

	-- update visual part of UI
	if (gCZ_MenuData.Mode == gCZ_MenuMode.IMPORT) then
		Dialog_SetCaptionText(gDialogHandle, "Import World")
		EditBox_SetText(Dialog_GetControl(gDialogHandle, "edNewZoneName"), gCZ_MenuData.ZoneName, false)
	else
		Dialog_SetCaptionText(gDialogHandle, "Create World")
		EditBox_SetText(Dialog_GetControl(gDialogHandle, "edNewZoneName"), gCZ_MenuData.ZoneName, false)
	end

	-- Request Zones and Communities
	IZ_RequestCommunityList()
	seedFreeZones()
	if isIn3DApp() == true then
		UGCZLP_RequestZoneList()
	end
end

function CreateZoneMenuFinished()
	local eventName = "CreateZoneMenuFinishedEvent"
	local event = KEP_EventCreate( eventName)
	KEP_EventSetObjectId(event, gCZ_MenuData.ZoneIndex)
	KEP_EventEncodeNumber(event, gCZ_WorkingData.Result)   -- (0 false - 1 true)
	KEP_EventEncodeNumber(event, gCZ_WorkingData.Key)      -- newZoneIndex
	KEP_EventEncodeString(event, gCZ_MenuData.ZoneName)    -- zoneName
	KEP_EventQueue( event)
end

function IZ_RequestCommunityList()
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..TRAVEL_LIST_SUFFIX.."action=myCommunity".."&ownerOnly=true".."&importList=true", WF.BROWSE_PLACES_SEARCH, 1, 10000)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.BROWSE_PLACES_SEARCH then
		local responseXML = KEP_EventDecodeString(event)
		g_communityList = parseCommunities(responseXML)
		UIUpdateList()
	end
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate(dialogHandle)

	g_communityList = {}
	g_zoneDataList = {}

	toggleAllControls(false)

	local e = Image_GetDisplayElement(gHandles["imgGradiantFreeZone1"])
	gCZ_gradCoords = {}
	gCZ_gradCoords[1] = Element_GetCoordLeft(e)
	gCZ_gradCoords[2] = Element_GetCoordTop(e)
	gCZ_gradCoords[3] = Element_GetCoordRight(e)
	gCZ_gradCoords[4] = Element_GetCoordBottom(e)
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function toggleDialogPanels(displayMode)

	-- Dialog is designed to be in import mode by default.
	if (displayMode == gCZ_DisplayMode.SHOW_IMPORT) then
		local hide = {"NewZone", "Status", "Confirm"}
		local show = {"FreeZones", "Browse", "Zones", "Controls"}

		-- Dialog Height  --
		MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), gCZ_DisplayMode.SHOW_IMPORT_HEIGHT)

		-- Hide --
		for i,panelName in pairs(hide) do
			for i,ctrlName in pairs(gctrls_Panels[panelName]) do
				Control_SetVisible(gHandles[ctrlName], false)
				Control_SetEnabled(gHandles[ctrlName], false)
			end
		end

		-- Show --
		for i,panelName in pairs(show) do
			for i,ctrlName in pairs(gctrls_Panels[panelName]) do
				Control_SetVisible(gHandles[ctrlName], true)
				Control_SetEnabled(gHandles[ctrlName], true)
			end
		end

		Control_SetEnabled(gHandles["btnOK"], false)
	elseif (displayMode == gCZ_DisplayMode.SHOW_NEW) then
		local hide = {"Status","Confirm"}
		local show = {"FreeZones", "Browse", "Zones", "NewZone", "Controls"}

		-- Dialog Height  --
		MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), gCZ_DisplayMode.SHOW_NEW_HEIGHT)

		-- Hide --
		for i,panelName in pairs(hide) do
			for i,ctrlName in pairs(gctrls_Panels[panelName]) do
				Control_SetVisible(gHandles[ctrlName], false)
				Control_SetEnabled(gHandles[ctrlName], false)
			end
		end

		-- Show --
		for i,panelName in pairs(show) do
			for i,ctrlName in pairs(gctrls_Panels[panelName]) do
				Control_SetVisible(gHandles[ctrlName], true)
				Control_SetEnabled(gHandles[ctrlName], true)
			end
		end
		Control_SetEnabled(gHandles["btnOK"], false)
	elseif (displayMode == gCZ_DisplayMode.SHOW_STATUS) then
		local hide = {"FreeZones", "Browse", "Zones", "NewZone", "Controls", "Confirm"}
		local show = {"Status"}

		-- Dialog Height  --
		MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), gCZ_DisplayMode.SHOW_STATUS_HEIGHT)

		-- Hide --
		for i,panelName in pairs(hide) do
			for i,ctrlName in pairs(gctrls_Panels[panelName]) do
				Control_SetVisible(gHandles[ctrlName], false)
				Control_SetEnabled(gHandles[ctrlName], false)
			end
		end

		-- Show --
		for i,panelName in pairs(show) do
			for i,ctrlName in pairs(gctrls_Panels[panelName]) do
				Control_SetVisible(gHandles[ctrlName], true)
				Control_SetEnabled(gHandles[ctrlName], true)
			end
		end

		Control_SetEnabled(gHandles["btnSuccess"], false)
		Control_SetEnabled(gHandles["btnFail"], false)
	end	
end

function UIUpdateList()
	local lbxList = gHandles["lbxList"]
	local displayName
	local idx = 0

	-- we are rebuilding the guilist so figure out which record is currently selected
	local selListIdx = ListBox_GetSelectedItemData(gHandles["lbxList"])
	local selZone = nil
	for k,v in pairs(g_zoneDataList) do
		if (v.listIndex == selListIdx) then
			selZone = v
		end
	end

	-- Clear list
	ListBox_RemoveAllItems(lbxList)

	-- First put all [user] zones
	for k,v in pairs(g_zoneDataList) do
		if (v.isKanevaSupplied == false) then
			-- set listIndex
			v.listIndex = idx

			-- add to list
			displayName = v.zoneName
			if (v.lastOpResult ~= UGCZLPLastOperation.Result.NONE and v.lastOpResult ~= UGCZLPLastOperation.Result.SUCCESS) then
				displayName = displayName .."   ".. UGCZLPLastOpResultToString(v.lastOpResult) .." ".. v.lastOpMsg
			end
			ListBox_AddItem(lbxList, displayName, idx)
			idx = idx + 1
		end
	end

	-- Next add [communities]
	for k,v in ipairs(g_communityList) do
		-- set listIndex
		v.listIndex = idx

		-- add to list
		displayName = v.name
		ListBox_AddItem(lbxList, displayName, idx)
		idx = idx + 1
	end
end

function UIUpdateControls(strControlType, selIndex)
	Control_SetEnabled(gHandles["btnOK"], false)
	local okButtonEnabled = false
	if strControlType == "FreeZoneItem" then

		-- update freezones based on new index
		if (g_SelectedFreeZoneBtn ~= selIndex) then
			okButtonEnabled = true
		end
		toggleFreeZoneButtons(selIndex)

		-- unlselect anything from listbox
		ListBox_ClearSelection(gHandles["lbxList"])
	elseif strControlType == "ListBoxItem" then

		-- unlselect anything from freezones
		toggleFreeZoneButtons(g_SelectedFreeZoneBtn)

		-- check selIndex against zones
		-- check state of the zone to prevent importing zones that are busy
		for k,v in pairs(g_zoneDataList) do
			if (v.listIndex == selIndex) then
				if (v.lastOpResult ~= UGCZLPLastOperation.Result.PENDING and v.lastOpResult ~= UGCZLPLastOperation.Result.IMPORTING) then
					okButtonEnabled = true
					break
				end
			end
		end

		-- check selected item against communities -- if we didnt match it against a zone
		if (okButtonEnabled == false) then
			for k,v in pairs(g_communityList) do
				if (v.listIndex == selIndex) then
					okButtonEnabled = true
					break
				end
			end
		end
	end

	if (gCZ_MenuData.Mode == gCZ_MenuMode.IMPORT) then
		Control_SetEnabled(gHandles["btnOK"], okButtonEnabled)

	elseif (gCZ_MenuData.Mode == gCZ_MenuMode.NEW) then
		if (okButtonEnabled and (tostring(EditBox_GetText(gHandles["edNewZoneName"])) ~= "")) then
			Control_SetEnabled(gHandles["btnOK"], okButtonEnabled)
		end
	end
end

function UIUpdateStatusPanel(pbarXValue, statusMsg, resultMsg, statusCode)

	if (pbarXValue ~= nil) then
		Control_SetSize(gHandles["pbarStatus"], pbarXValue, Control_GetHeight(gHandles["pbarStatus"]))
	end

	if (statusMsg ~= nil) then
		Static_SetText(gHandles["txtStatus"], tostring(statusMsg))
	end

	if (resultMsg ~= nil) then
		Static_SetText(gHandles["stcResult"], tostring(resultMsg))
	end

	if (statusCode == nil) then
		Control_SetEnabled(gHandles["btnSuccess"], false)
		Control_SetEnabled(gHandles["btnFail"],   false)
	elseif (statusCode == 1) then
		Control_SetEnabled(gHandles["btnSuccess"], true)
		Control_SetEnabled(gHandles["btnFail"],   false)
	elseif (statusCode == 2) then
		Control_SetEnabled(gHandles["btnSuccess"], false)
		Control_SetEnabled(gHandles["btnFail"],   true)
	end
end

function toggleFreeZoneButtons(clickedBtn)

	-- if currently selected zone is clicked again turn it off
	if (g_SelectedFreeZoneBtn == clickedBtn) then
		g_SelectedFreeZoneBtn = -1
	else
		g_SelectedFreeZoneBtn = clickedBtn
	end

	for i=1,4 do
		local element = Image_GetDisplayElement(gHandles["imgGradiantFreeZone"..i])
		if (i ~= g_SelectedFreeZoneBtn) then
			-- turn off
			Element_SetCoords(element, gCZ_gradCoords[1], gCZ_gradCoords[2], gCZ_gradCoords[3], gCZ_gradCoords[4])
		else
			-- turn on
			Element_SetCoords(element, gCZ_gradCoords[1], gCZ_gradCoords[4], gCZ_gradCoords[3], gCZ_gradCoords[2])
		end
	end
end

function toggleAllControls(blnState)
	local panels = {"FreeZones", "Browse", "Zones", "NewZone", "Status", "Controls"}
	for i,panelName in pairs(panels) do
		for i,ctrlName in pairs(gctrls_Panels[panelName]) do
			Control_SetEnabled(gHandles[ctrlName], blnState)
		end
	end
end

----- Free Starting Zone buttons -----
function btnFreeZone1_OnButtonClicked(buttonHandle)
	UIUpdateControls("FreeZoneItem", 1)
end

function btnFreeZone2_OnButtonClicked(buttonHandle)
	UIUpdateControls("FreeZoneItem", 2)
end

function btnFreeZone3_OnButtonClicked(buttonHandle)
	UIUpdateControls("FreeZoneItem", 3)
end

function btnFreeZone4_OnButtonClicked(buttonHandle)
	UIUpdateControls("FreeZoneItem", 4)
end

----- Browse Shop buttons -----
function btnBrowse1_OnButtonClicked(buttonHandle)
	IZ_Shop(gCZ_ShoppingSuffix[1])
end

function btnBrowse2_OnButtonClicked(buttonHandle)
	IZ_Shop(gCZ_ShoppingSuffix[2])
end

function btnBrowse3_OnButtonClicked(buttonHandle)
	IZ_Shop(gCZ_ShoppingSuffix[3])
end

function btnBrowse4_OnButtonClicked(buttonHandle)
	IZ_Shop(gCZ_ShoppingSuffix[4])
end

function btnBrowse5_OnButtonClicked(buttonHandle)
	IZ_Shop(gCZ_ShoppingSuffix[5])
end

function btnBrowse6_OnButtonClicked(buttonHandle)
	IZ_Shop(gCZ_ShoppingSuffix[6])
end

function btnBrowse7_OnButtonClicked(buttonHandle)
	IZ_Shop(gCZ_ShoppingSuffix[7])
end

function btnBrowse8_OnButtonClicked(buttonHandle)
	IZ_Shop(gCZ_ShoppingSuffix[8])
end

function btnBrowse9_OnButtonClicked(buttonHandle)
	IZ_Shop(gCZ_ShoppingSuffix[9])
end

function btnBrowse10_OnButtonClicked(buttonHandle)
	IZ_Shop(gCZ_ShoppingSuffix[10])
end

function lbxList_OnListBoxSelection(listBoxHandle, x, y, selIndex, selText)
	UIUpdateControls("ListBoxItem", ListBox_GetSelectedItemData(gHandles["lbxList"]))
end

function btnRefreshList_OnButtonClicked(buttonHandle)

	-- Request Zones and Communities
	g_communityList = {}
	IZ_RequestCommunityList()
	if isIn3DApp() == true then
		UGCZLP_RequestZoneList()
	end
end

function edNewZoneName_OnEditBoxChange(editBoxHandle, text)
	local fmtText = string.gsub(text, "^%s*(.-)%s*$", "%1")
	local selItem = getSelectedItem()
	if (selItem ~= nil) then
		if (fmtText == "") then
			-- turn off on blank text
			if (Control_GetEnabled(gHandles["btnOK"]) == 1) then
				Control_SetEnabled(gHandles["btnOK"], false)
			end
		else
			-- turn back on
			if (Control_GetEnabled(gHandles["btnOK"]) == 0) then
				Control_SetEnabled(gHandles["btnOK"], true)
			end
		end
	end
end

function btnOK_OnButtonClicked(buttonHandle)

	-- Check our current operating mode
	local fmtZoneName = "empty"
	if (gCZ_MenuData.Mode ~= gCZ_MenuMode.IMPORT) then

		local newZoneName = EditBox_GetText(gHandles["edNewZoneName"])

		if string.len(newZoneName) > 64 then
			KEP_MessageBox( "New World Name too long. It must be 64 characters or less.", "Error")
			return
		end

		if string.find(newZoneName, ("[^%w%s]")) ~= nil then
			KEP_MessageBox( "New World Name must contain only numbers, letters, and spaces.", "Error")
			return
		end

		-- No blank zone names
		fmtZoneName = string.gsub(newZoneName, "^%s*(.-)%s*$", "%1")
		if (fmtZoneName == "") then
			KEP_MessageBox( "Missing World Name", "Error")
			return
		end
	end

	local selItem = getSelectedItem()

	if (selItem ~= nil) then

		if not g_confirm and (gCZ_MenuData.DisplayMode == gCZ_DisplayMode.SHOW_IMPORT) then 
		--	toggleDialogPanels(gCZ_DisplayMode.SHOW_CONFIRM)
			MenuOpenModal("ImportZoneConfirm.xml") 
			return;
		end
	
		local copyZoneIndex = 0
		local ugczlpimporttype = UGCZLPImportTypes.COMMUNITY

		if (selItem.type == "zone") then
			-- 3dApp importZone
			copyZoneIndex = InstanceId_MakeId(selItem.index, 0, InstanceId_GetType(KEP_GetCurrentZoneIndex()))
			copyZoneIndex = InstanceId_GetClearedInstanceId(copyZoneIndex)
			ugczlpimporttype = UGCZLPImportTypes.ZONE
		end

		if (gCZ_MenuData.TrackStatus == 1) then
			toggleDialogPanels(gCZ_DisplayMode.SHOW_STATUS)
		end

		-- Check our current operating mode
		if (gCZ_MenuData.Mode == gCZ_MenuMode.IMPORT) then

			-- Starting a ImportZone operation
			-- Reset monitored zone and progress bar
			gCZ_WorkingData.Key = InstanceId_GetId(gCZ_MenuData.ZoneIndex)
			UIUpdateStatusPanel(gCZ_ProgressBar.EMPTY, "", "", nil)
		
			-- Make the request
			if (isIn3DApp()) then
				UGCZLP_RequestImportZoneEvent(ugczlpimporttype, InstanceId_GetId(gCZ_MenuData.ZoneIndex), InstanceId_GetId(selItem.index), gCZ_MenuData.DeleteDynamicObjects, copyZoneIndex, gCZ_MenuData.CopyFlag, gCZ_MenuData.ImportZoneType, selItem.index, selItem.instanceId)
			else 

				local dstZoneDef = {}
					dstZoneDef.zoneIndex  = KEP_GetCurrentZoneIndex()
					dstZoneDef.instanceId = KEP_GetZoneInstanceId()

				local srcZoneDef = {}
					srcZoneDef.zoneIndex  = selItem.index
					srcZoneDef.instanceId = selItem.instanceId

				local baseZoneIndexPlain = InstanceId_GetId(selItem.index)

				UGCZLP_RequestImportChannelEvent(dstZoneDef, srcZoneDef, baseZoneIndexPlain, gCZ_MenuData.DeleteDynamicObjects)

			end
			 
		else
			-- Starting a NEWZone operation
			-- Reset monitored zone a progress bar
			gCZ_WorkingData.Key = fmtZoneName
			UIUpdateStatusPanel(gCZ_ProgressBar.EMPTY, "", "", nil)

			-- Make the request
			if (gCZ_MenuData.ZoneIndex > 0) then
				copyZoneIndex = gCZ_MenuData.ZoneIndex
			end
			UGCZLP_RequestCreateZoneEvent(ugczlpimporttype, InstanceId_GetId(selItem.index), fmtZoneName, UGCZLPZoneTypes.PERMANENT_ZONE, UGCZLPSpawnLoc.ZONE_DEFAULT, 100, copyZoneIndex, gCZ_MenuData.CopyFlag, gCZ_MenuData.ImportZoneType, selItem.index, selItem.instanceId)
		end
		
		if (gCZ_MenuData.TrackStatus == 0) then
			MenuCloseThis()
		end
	end
end

function btnCancel_OnButtonClicked(buttonHandle)
	if g_confirm and (gCZ_MenuData.DisplayMode == gCZ_DisplayMode.SHOW_IMPORT) then 
		g_confirm = false
		Control_SetEnabled(gHandles["btnOK"],true)
	else 
		CreateZoneMenuFinished()
		MenuCloseThis()
	end
end

function btnSuccess_OnButtonClicked(buttonHandle)
	CreateZoneMenuFinished()
	MenuCloseThis()
end

function btnFail_OnButtonClicked(buttonHandle)
	-- show Previous Panel
	toggleDialogPanels(gCZ_MenuData.DisplayMode)
end

function btnClose_OnButtonClicked(buttonHandle)
	CreateZoneMenuFinished()
	MenuCloseThis()
end

function Dialog_OnMouseMove(dialogHandle, x, y)
	for i=1,4 do
		local element = Image_GetDisplayElement(gHandles["imgGradiantFreeZone"..i])

		if (Control_ContainsPoint(gHandles["btnFreeZone"..i], x, y) == 0) then
			-- turn off (except selected one)
			if (i ~= g_SelectedFreeZoneBtn) then
				Element_SetCoords(element, gCZ_gradCoords[1], gCZ_gradCoords[2], gCZ_gradCoords[3], gCZ_gradCoords[4])
			end
		else
			-- turn on
			Element_SetCoords(element, gCZ_gradCoords[1], gCZ_gradCoords[4], gCZ_gradCoords[3], gCZ_gradCoords[2])
		end
	end
end

-- hooks for UGCZoneLiteProxy replies
function ImportZoneSuccessEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- unpack event from CSZoneLiteProxy
	local zoneIndex    = objectid
	local newZoneIndex = KEP_EventDecodeNumber(event)
	return KEP.EPR_CONSUMED
end

function ImportChannelSuccessEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- unpack event from CSZoneLiteProxy
	local zoneIndex    = objectid
	local newZoneIndex = KEP_EventDecodeNumber(event)
	return KEP.EPR_CONSUMED
end

function UGCZLP_ReplyZoneList(blnRetval, zoneList)
	g_zoneDataList = {}

	-- filter out the current zone -- prevent self importing
	for i,v in ipairs(zoneList) do
		table.insert(g_zoneDataList, v.zoneIndex, v)
	end

	-- update our UI
	UIUpdateList()
end

function UGCZLP_ReplyImportZoneEvent(blnRetval, zoneIndex, newZoneIndex, strMsg, lastOpResult, lastOpMsg)

	-- udpate status panel
	if (gCZ_WorkingData.Key == zoneIndex) then

		if (blnRetval == true) then
			gCZ_WorkingData.Key = newZoneIndex

			if (lastOpResult == UGCZLPLastOperation.Result.SUCCESS) then
				UIUpdateStatusPanel(gCZ_ProgressBar.COMPLETE, "", "Success", 1)
				gCZ_WorkingData.Result = 1 -- true
			elseif (lastOpResult == UGCZLPLastOperation.Result.FAILED) then
				local resultMsg = UGCZLPLastOpResultToString(lastOpResult) ..": "..tostring(strMsg)
				UIUpdateStatusPanel(nil, nil, resultMsg, 2)
				gCZ_WorkingData.Result = 0 -- false
			end
		else
			-- error
			UIUpdateStatusPanel(nil, nil, tostring(strMsg), 2)
			gCZ_WorkingData.Result = 0 -- false
		end
	else
		local zoneData = g_zoneDataList[zoneIndex]
		if (zoneData ~= nil) then
			if (blnRetval == true) then
				zoneData.zoneIndex    = newZoneIndex
				zoneData.lastOpResult = lastOpResult
				zoneData.lastOpMsg    = lastOpMsg

				g_zoneDataList[zoneIndex] = nil
				g_zoneDataList[newZoneIndex] = zoneData
			else
				zoneData.lastOpResult = lastOpResult
				zoneData.lastOpMsg    = lastOpMsg
			end

			-- refresh screen
			UIUpdateList()
		end
	end
end

function UGCZLP_ReplyCreateZoneEvent(blnRetval, zoneIndex, newZoneName, strMsg, lastOpResult, lastOpMsg)

	-- udpate status panel
	if (gCZ_WorkingData.Key == newZoneName) then
		if (blnRetval == true) then
			gCZ_WorkingData.Key = zoneIndex
			if (lastOpResult == UGCZLPLastOperation.Result.SUCCESS) then
				UIUpdateStatusPanel(gCZ_ProgressBar.COMPLETE, "", "Success", 1)
				gCZ_WorkingData.Result = 1 -- true
			elseif (lastOpResult == UGCZLPLastOperation.Result.FAILED) then
				local resultMsg = UGCZLPLastOpResultToString(lastOpResult) ..": "..tostring(strMsg)
				UIUpdateStatusPanel(nil, nil, resultMsg, 2)
				gCZ_WorkingData.Result = 0 -- false
			end
		else
			-- error
			UIUpdateStatusPanel(nil, nil, tostring(strMsg), 2)
			gCZ_WorkingData.Result = 0 -- false
		end
	end

	-- refresh list.
	if isIn3DApp() == true then
		UGCZLP_RequestZoneList()
	end
end

function UGCZLP_ReplyZoneStateChangeEvent(zoneIndex, lastOpResult, lastOpMsg)

	-- udpate status panel
	if (gCZ_WorkingData.Key == zoneIndex) then
		Control_SetSize(gHandles["pbarStatus"], Control_GetWidth(gHandles["pbarStatus"]) + gCZ_ProgressBar.INCREMENT, Control_GetHeight(gHandles["pbarStatus"]))
	else
		local zoneData = g_zoneDataList[zoneIndex]
		if (zoneData ~= nil) then
			zoneData.lastOpResult = lastOpResult
			zoneData.lastOpMsg    = lastOpMsg
			-- refresh screen
			UIUpdateList()
		end
	end
end

function IZ_Shop(url)
	KEP_LaunchBrowser(url)
end

function seedFreeZones()

	local FreeZone1 = {}
	FreeZone1.isKanevaSupplied = true
	FreeZone1.zoneIndex = 52
	FreeZone1.exgName   = "Beach_House_Deed.exg"
	FreeZone1.zoneName  = "Exclusive Beach House"
	FreeZone1.isDefault = false
	FreeZone1.lastOpMsg = ""
	FreeZone1.lastOpResult = 0
	table.insert(g_zoneDataList, FreeZone1.zoneIndex, FreeZone1)

	local FreeZone2 = {}
	FreeZone2.isKanevaSupplied = true
	FreeZone2.zoneIndex = 50
	FreeZone2.exgName   = "Nature_Preserve_Deed.exg"
	FreeZone2.zoneName  = "Outdoor_Retreat"
	FreeZone2.isDefault = false
	FreeZone2.lastOpMsg = ""
	FreeZone2.lastOpResult = 0
	table.insert(g_zoneDataList, FreeZone2.zoneIndex, FreeZone2)

	local FreeZone3 = {}
	FreeZone3.isKanevaSupplied = true
	FreeZone3.zoneIndex = 37
	FreeZone3.exgName   = "Island.exg"
	FreeZone3.zoneName  = "Island"
	FreeZone3.isDefault = false
	FreeZone3.lastOpMsg = ""
	FreeZone3.lastOpResult = 0
	table.insert(g_zoneDataList, FreeZone3.zoneIndex, FreeZone3)

	local FreeZone4 = {}
	FreeZone4.isKanevaSupplied = true
	FreeZone4.zoneIndex = 20
	FreeZone4.exgName   = "OpenSpace.exg"
	FreeZone4.zoneName  = "Green Acres"
	FreeZone4.isDefault = false
	FreeZone4.lastOpMsg = ""
	FreeZone4.lastOpResult = 0
	table.insert(g_zoneDataList, FreeZone4.zoneIndex, FreeZone4)
end

-- returns unified entry for zones and communities
--  entry.type       ("zone")			|| ("community")
--  entry.index      (zone.zoneIndex)	|| (community.index)
--  entry.instanceId (-1)				|| (community.instance)
function getSelectedItem()

	-- First check if user selected a Free Zone
	if (g_SelectedFreeZoneBtn ~= -1)  then

		local reqZoneName = ""
		if (g_SelectedFreeZoneBtn == 1) then
			reqZoneName = "Exclusive Beach House"
		elseif (g_SelectedFreeZoneBtn == 2) then
			reqZoneName = "Outdoor_Retreat"
		elseif (g_SelectedFreeZoneBtn == 3) then
			reqZoneName = "Island"
		elseif (g_SelectedFreeZoneBtn == 4) then
			reqZoneName = "Green Acres"
		end

		for k,v in pairs(g_zoneDataList) do
			if (v.zoneName == reqZoneName) then
				local retval = {}
				retval.type       = "zone"
				retval.index      = v.zoneIndex
				retval.instanceId = -1
				return retval
			end
		end
		return nil
	end

	-- No freeZone selected so look in list
	if (ListBox_GetSelectedItemIndex(gHandles["lbxList"]) == -1) then
		return nil
	end

	local selListIndex = ListBox_GetSelectedItemData(gHandles["lbxList"])

	-- check zone container
	for k,v in pairs(g_zoneDataList) do
		if (v.listIndex == selListIndex) then
			local retval = {}
			retval.type       = "zone"
			retval.index      = v.zoneIndex
			retval.instanceId = -1
			return retval
		end
	end

	-- check community container
	for k,v in pairs(g_communityList) do
		if (v.listIndex == selListIndex) then
			local retval = {}
			retval.type       = "community"
			retval.index      = v.index
			retval.instanceId = v.instance
			return retval
		end
	end
	return nil
end

function parseCommunities(responseXml)
	local resultList = {}
	local s = 0       -- start and end index of substring
	local numRec = 0  -- number of records in this chunk
	local strPos = 0  -- current position in string to parse
	local result = "" -- return string from find temp
	s, strPos, result = string.find(responseXml, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		local s, e, numRecs = string.find(responseXml, "<TotalNumberRecords>(.-)</TotalNumberRecords>")
		s, strPos, result = string.find(responseXml, "<ReturnCode>(.-)</ReturnCode>")
		if result == "0" then
			local s, e, numRecs = string.find(responseXml, "<NumberRecords>(.-)</NumberRecords>")
			numRecs = tonumber(numRecs)

			-- Parse Communities from XML
			if numRecs > 0 then
				local zoneStart, zoneEnd, nameStart, nameEnd, indexStart, indexEnd, instanceStart, instanceEnd = 0
				local zone, name, index, instance = ""
				for i=1,numRecs do
					zoneStart, zoneEnd, zone    = string.find(responseXml, "<Zone>(.-)</Zone>", zoneEnd)
					nameStart, nameEnd, name    = string.find(zone, "<name>(.-)</name>")
					indexStart, indexEnd, index = string.find(zone, "<zone_index>(.-)</zone_index>")
					instanceStart, instanceEnd, instance = string.find(zone, "<zone_instance_id>(.-)</zone_instance_id>")
					local newEntry = {}
					newEntry.name     = name
					newEntry.index    = index
					newEntry.instance = instance
					table.insert(resultList, newEntry)
				end
			end
		end
	end
	return resultList
end
