--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- TransactionComplete.lua
-- 
-- Transaction complete menu for donations, etc
-------------------------------------------------------------------------------
dofile("MenuHelper.lua")
dofile("HUDHelper.lua")

----------------------
-- Locals
----------------------
local price = "0"			-- price of item to be purchased
local worldName = ""		-- name of world or owner
local sourceMenu = ""		-- menu source of credit transaction

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "getTransactionComplete", "CREDIT_TRANSACTION_COMPLETE", KEP.HIGH_PRIO)
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

-- Dialog_OnDestroy = Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Handles transaction passed from previous menu choice (e.g. Donation menu choice)
function getTransactionComplete(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	price = KEP_EventDecodeString(tEvent)
	sourceMenu = KEP_EventDecodeString(tEvent)
	if sourceMenu == "Donation" or sourceMenu == "Key" or sourceMenu == "GiveCredits" then
		worldName = KEP_EventDecodeString(tEvent)
	end

	setTransactionComplete()
end

function setTransactionComplete()
	if sourceMenu == "Donation" then
		Static_SetText(gHandles["Menu_Title"], "Donation Complete")
		Static_SetText(gHandles["lblPurchaseText"], "Your Donation of "..price.." credits to "..worldName.." is complete.")
		Control_SetEnabled(gHandles["imgDonationTransaction"], true)
		Control_SetEnabled(gHandles["imgDefaultTransaction"], false)
		Control_SetEnabled(gHandles["imgKeyTransaction"], false)
	elseif sourceMenu == "Key" then
		Static_SetText(gHandles["Menu_Title"], "Transaction Complete")
		Static_SetText(gHandles["lblPurchaseText"], "Your Purchase of "..price.." keys for "..worldName.." is complete.")
		Control_SetEnabled(gHandles["imgKeyTransaction"], true)
		Control_SetEnabled(gHandles["imgDefaultTransaction"], false)
		Control_SetEnabled(gHandles["imgDonationTransaction"], false)
	elseif sourceMenu == "GiveCredits" then
		Static_SetText(gHandles["Menu_Title"], "Transaction Complete")
		Static_SetText(gHandles["lblPurchaseText"], "Your Gift of "..price.." credits to "..worldName.." is complete.")
		Control_SetEnabled(gHandles["imgDonationTransaction"], true)
		Control_SetEnabled(gHandles["imgDefaultTransaction"], false)
		Control_SetEnabled(gHandles["imgKeyTransaction"], false)
	else
		Static_SetText(gHandles["Menu_Title"], "Transaction Complete")
		Static_SetText(gHandles["lblPurchaseText"], "Your Purchase of "..price.." is complete.")
		Control_SetEnabled(gHandles["imgDefaultTransaction"], true)
		Control_SetEnabled(gHandles["imgKeyTransaction"], false)
		Control_SetEnabled(gHandles["imgDonationTransaction"], false)
	end
end
-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --
function btnOK(buttonHandle)
	MenuCloseThis()
end