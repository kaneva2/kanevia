--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

KDP_GAMEOVER_EVENT = "KDPGameOverEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"

ORIGINAL_PROGRESS_WIDTH = 285

g_filter = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "YesNoBoxEventHandler", KDP_GAMEOVER_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( KDP_GAMEOVER_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
end

function YesNoBoxEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local famePointsAwarded = KEP_EventDecodeNumber( event )
	local percentToNext = KEP_EventDecodeNumber( event )

	g_filter = filter

	sh = Dialog_GetStatic( gDialogHandle, "lblFamePoints" )
	Static_SetText( sh, famePointsAwarded )

	sh = Dialog_GetImage( gDialogHandle, "imgProgressBar" )
	progressWidth = ORIGINAL_PROGRESS_WIDTH * ( percentToNext / 100 )
	Control_SetSize(Image_GetControl(sh), progressWidth, 26)
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function btnYes_OnButtonClicked( buttonHandle)
	local answerEvent = KEP_EventCreate( YES_NO_ANSWER_EVENT )
	if g_filter ~= 0 then
		KEP_EventSetFilter( answerEvent, g_filter)
	end
	KEP_EventEncodeNumber( answerEvent, 1 )
	KEP_EventQueue( answerEvent )
	
	MenuCloseThis()
end

function btnNo_OnButtonClicked( buttonHandle)
	local answerEvent = KEP_EventCreate( YES_NO_ANSWER_EVENT )
	KEP_EventEncodeNumber( answerEvent, 0 )
	KEP_EventQueue( answerEvent )

	MenuCloseThis()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
