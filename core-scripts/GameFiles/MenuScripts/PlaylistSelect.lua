--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")

local TOOLTIP_SAVE = "Update Media"
local TOOLTIP_ADD = "Add to Playlist"
local CREATE_NEW_TEXT = "**Create New Playlist**"

local CONTROL_BUFFER = 10 -- Pixel separation between controls for resizing purposes

local VISIBLE_PLAYLISTS = 8
local MAX_ITEMS = 50 --Max items to display in the main display area
local MAX_PLAYLISTS = 50 -- Max number of playlists for the dropdown

local WEB_ADDRESS_FOR_PLAYLISTS = GameGlobals.WEB_SITE_PREFIX.."kgp/playList.aspx?type="
local WEB_ADDRESS_UPLOAD = GameGlobals.WEB_SITE_PREFIX_KANEVA .. "asset/publishedItemsNew.aspx?"

local NUM_TABS = 2
local TAB_ENTERTAINMENT		= "k"
local TAB_MY_PLAYLISTS		= "u"

--Extra Types
local SEARCH_ENTERTAINMENT	= "m"
local SEARCH_MY				= "y"
local GET_SUBLISTS			= "c"
local ADD_PLAYLIST			= "p"

local SEARCH_TEXT = {
	s = "Search Flash", 
	g = "Search Games",
	k = "Search All Videos",
	a = "Search TV Channels",
	u = "Search My Videos" 
}

local m_type	= nil
local m_objId	= nil
local m_edit    = 0  -- 0 for not in edit mode, 1 for in edit mode
local m_target  = " "

local m_tab = TAB_ENTERTAINMENT

local m_mediaList = {}

local m_searchString = ""
local m_clearSearch = true
local m_gameplayMode = ""

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "SelectMediaEventHandler", "SelectMediaEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "SavePlaylistNameEventHandler", "SavePlaylistNameEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "returnFrameworkStateHandler", "FrameworkState", KEP.MED_PRIO )

end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "SelectMediaEvent", KEP.MED_PRIO )
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( "FlashGameUpdatedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "SetupSaveEvent", KEP.MED_PRIO )
	KEP_EventRegister( "SavePlaylistNameEvent", KEP.MED_PRIO )
	KEP_EventRegister( KEP.WORLD_FAME_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "PLAYLIST_SELECT_EVENT", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )	

	-- Get list of my playlists for the dropdown menu
	makeWebCall(WEB_ADDRESS_FOR_PLAYLISTS .. TAB_MY_PLAYLISTS, WF.MY_PLAYLISTS,  1, MAX_PLAYLISTS)

	Events.registerHandler("FRAMEWORK_SELECT_MEDIA", selectMediaHandler)

	-- Is the Framework active in this world? Let's find out
	local event = KEP_EventCreate("GetFrameworkState")
	KEP_EventQueue(event)
end

function getMediaList()
	local searching =  m_searchString and m_searchString ~= ""
	local filter = WF.PLAYLIST

	clearLists()

	local requestType = m_type
	if m_type == PLAYLIST_TYPE_TV then
		if searching and m_tab == TAB_ENTERTAINMENT then
			requestType = SEARCH_ENTERTAINMENT
		elseif searching and m_tab == TAB_MY_PLAYLISTS then
			requestType = SEARCH_MY
		else
			requestType = m_tab
			if m_tab == TAB_ENTERTAINMENT then filter = WF.PLAYLIST_GROUPS end
		end
	end

	local webAddress = WEB_ADDRESS_FOR_PLAYLISTS .. requestType

	if searching then
		-- If on the media menu and entertainment tab we're searching sublists
		local enc_searchString = KEP_UrlEncode(m_searchString)
		webAddress = webAddress .. "&ss=" .. enc_searchString
	end

	makeWebCall(webAddress, filter,  1, MAX_ITEMS)
end

function SelectMediaEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if m_type == nil then
		m_type   = KEP_EventDecodeString( event )
		m_objId  = KEP_EventDecodeNumber( event )
		m_edit   = KEP_EventDecodeNumber( event )
		m_target = KEP_EventDecodeString( event )

		setMenuType()
		getMediaList()
	end

	return KEP.EPR_OK
end

function selectMediaHandler(event)
	if m_type == nil then
		m_type   = event.mediaType
		m_objId  = event.id
		m_edit   = false

		setMenuType()
		getMediaList()
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    if (filter == WF.PLAYLIST or filter == WF.PLAYLIST_GROUPS) then
		local xmlData = KEP_EventDecodeString( event )

		if not string.find(xmlData, "<ReturnCode>0</ReturnCode>") then
			return
		end

		local numRecords = string.match(xmlData, "<NumberRecords>(%d-)</NumberRecords>")
		numRecords = tonumber(numRecords or 0)

		if filter == WF.PLAYLIST_GROUPS then
			parsePlaylistGroups(xmlData)
		else
			m_mediaList = parseFlash(xmlData)
		end

		updateList()

	-- If the user added an item to a playlist, start playing that list on the object
	elseif filter == WF.ADD_TO_PLAYLIST then
		local xmlData = KEP_EventDecodeString( event )

		if not string.find(xmlData, "<ReturnCode>0</ReturnCode>") then
			return
		end

		local assetGroupId = string.match(xmlData, "<asset_group_id>([0-9]+)</asset_group_id>")
		local name = string.gsub(string.match(xmlData, "<name>(.-)</name>"), "&amp;", "&") or ""
		local assetCount = string.match(xmlData, "<asset_count>(.-)</asset_count>") or ""

		if assetGroupId then
			Log("ADD_TO_PLAYLIST - '"..name.."' assetId="..assetGroupId.." assetCount="..assetCount)
			local e = KEP_EventCreate( BrowserIds.BROWSER_REQUEST_EVENT_NAME )
			KEP_EventEncodeString( e, "changePlaylist?plId=" .. tostring(assetGroupId) .. "&objId=" .. m_objId)
			KEP_EventAddToServer( e )
			KEP_EventQueue( e )
		end

	elseif filter == WF.MY_PLAYLISTS then
		local xmlData = KEP_EventDecodeString( event )

		if not string.find(xmlData, "<ReturnCode>0</ReturnCode>") then
			return
		end

		local cmbHandle = gHandles["cmbPlaylists"]

		ComboBox_RemoveAllItems(cmbHandle)

		for playlistStr in string.gmatch(xmlData, "<PlayList>(.-)</PlayList>") do
			local name = string.gsub((string.match(playlistStr, "<name>(.-)</name>") or ""), "&amp;", "&")
			ComboBox_AddItem(cmbHandle, name, 0)
		end

		ComboBox_AddItem(cmbHandle, CREATE_NEW_TEXT, -1)
	end
end

function parseFlash(xmlData)
	local playlistURL = string.match(xmlData, "<PlayListURL>(.-)</PlayListURL>") or ""
	local playlists = {}

	for playlistStr in string.gmatch(xmlData, "<PlayList>(.-)</PlayList>") do
		local mediaItem = {}
		mediaItem.name			= string.gsub((string.match(playlistStr, "<name>(.-)</name>") or ""), "&amp;", "&")
		mediaItem.assetId		= string.match(playlistStr, "<asset_id>(%d-)</asset_id>") or string.match(playlistStr, "<asset_group_id>(%d-)</asset_group_id>") or "0"
		mediaItem.description	= string.match(playlistStr, "<description>(.-)</description>") or ""
		mediaItem.mature		= string.match(playlistStr, "<mature>(.-)</mature>") or "N"
		mediaItem.assetCount	= string.match(playlistStr, "<asset_count>(%d-)</asset_count>") or "1"
		mediaItem.offsiteURL	= string.match(playlistStr, "<asset_offsite_id>(.-)</asset_offsite_id>") or ""
		mediaItem.sortOrder		= string.match(playlistStr, "<sort_order>(%d-)</sort_order>") or "0"
		mediaItem.assetType		= string.match(playlistStr, "<asset_type_id>(%d-)</asset_type_id>") or "0"
		mediaItem.groupCategory	= string.match(playlistStr, "<group_category_id>(%d-)</group_category_id>") or "0"
		
		mediaItem.URL			= string.gsub(playlistURL, "ASSET_ID", mediaItem.assetId)

		mediaItem.playlistString = playlistStr

		table.insert(playlists, mediaItem)
	end

	return playlists
end

function parsePlaylistGroups(xmlData)
	local categoryList = gHandles["lstCategories"]

	for playlistStr in string.gmatch(xmlData, "<PlayList>(.-)</PlayList") do
		local group_category_id = string.match(playlistStr, "<group_category_id>([-]?[0-9]+)</group_category_id>")
		local name = string.gsub(string.match(playlistStr, "<name>(.-)</name>"), "&amp;", "&")

		ListBox_AddItem( categoryList, name, group_category_id )
	end

	local itemSelected = ListBox_GetSelectedItemIndex(categoryList)
	local listSize = ListBox_GetSize(categoryList)

	--No item selected, select the first one
	if itemSelected < 0 and listSize > 0 then
		ListBox_SelectItem(categoryList, 0)
	end
end

--------------------------------------------------------------------------
-- Menu Setup
--------------------------------------------------------------------------
function updateList()
	local lstMedia = gHandles["lstMedia"]

	ListBox_RemoveAllItems(lstMedia)

	ListBox_AddItem(lstMedia, "<None>", 0)

	-- Add count if My Playlists and not searching
	local addCount = m_type == PLAYLIST_TYPE_TV and m_tab == TAB_MY_PLAYLISTS and (not m_searchString or m_searchString == "")

	if addCount then
		ListBox_AddItem(lstMedia, "ALL", -1)
	end

	for i, v in ipairs(m_mediaList) do
		local name = v.name
		if addCount then
			name = name .. " (" .. v.assetCount .. " items)"
		end

		ListBox_AddItem(lstMedia, name, v.assetId)
	end

	showNoResults(m_mediaList and #m_mediaList > 0)
end

function setMenuType()
	local name = "Media"
	if m_type == PLAYLIST_TYPE_FRAME then
		name = "Flash Widget"
	elseif m_type == PLAYLIST_TYPE_GAME then
		name = "Game"
	end

	if m_type ~= PLAYLIST_TYPE_TV then
		hideTabs()
		Static_SetText(gHandles["stcListHeader"], "Available " .. name .. "s")
	else
		showSubCatList(m_tab == TAB_ENTERTAINMENT)
	end
	
	-- If it's flash, set the title to just widget for the add new button
	local newName = (m_type == PLAYLIST_TYPE_FRAME) and "Widget" or name

	Static_SetText(gHandles["btnAddNew"], "Add New " .. newName)
	Dialog_SetCaptionText(gDialogHandle, " Choose " .. name)

	setDefaultSearchText()
end

function hideTabs()
	for i = 1, NUM_TABS do
		Control_SetVisible(gHandles["btnTab" .. i], false)
	end

	-- Move all controls up (except the close button) by the height of the tabs
	local height = Control_GetHeight(gHandles["btnTab1"])
	local closeButton = gHandles["btnClose"]
	for i, v in pairs(gHandles) do
		if v ~= closeButton then
			Control_SetLocationY(v, Control_GetLocationY(v) - height)
		end
	end

	local size = MenuGetSizeThis()
	MenuSetSizeThis(size.width, size.height - height)
end

function lstMedia_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	updateDivLines(gHandles["lstMedia"], "imgLst")
end

function lstCategories_OnScrollBarChanged(scrollBarHandle, listBoxHandle, page_size, track_pos)
	updateDivLines(gHandles["lstCategories"], "imgLstLeft")
end

-- Starting at the top, for each line if there is a param, add a div line
-- If it's an odd line, also add a background
function updateDivLines(listHandle, prefix)
	local startPos = ScrollBar_GetTrackPos(ListBox_GetScrollBar(listHandle))
	local displayedItems = ListBox_GetSize(listHandle) - startPos
	for i = 1, VISIBLE_PLAYLISTS do
		local lstDiv = prefix .. "Div" .. tostring(i)
		local lstBG = prefix .. "BG" .. tostring(i)
		
		local itemDisplayed = i <= displayedItems
		local oddItem = ((startPos + i) % 2) == 0
		
		Control_SetVisible(gHandles[lstDiv], itemDisplayed)
		Control_SetVisible(gHandles[lstBG], itemDisplayed and oddItem)
	end

	-- Show the last alternating line
	local showBottomBG = (displayedItems >= VISIBLE_PLAYLISTS) and ((startPos + VISIBLE_PLAYLISTS + 1) % 2 == 0)
	Control_SetVisible(gHandles[prefix .. "BG9"], showBottomBG)	
end

function showNoResults(enabled)
	--Do something here if no results
end 

--------------------------------------------------------------------------
-- Tabs
--------------------------------------------------------------------------
function btnTab1_OnButtonClicked(buttonHandle)
	setTab(TAB_ENTERTAINMENT)
end

function btnTab2_OnButtonClicked(buttonHandle)
	setTab(TAB_MY_PLAYLISTS)
end

function setTab(tab)
	if tab ~= m_tab then
		m_tab = tab

		clearSearch()
		
		Control_SetEnabled(gHandles["btnTab1"], tab ~= TAB_ENTERTAINMENT)
		Control_SetEnabled(gHandles["btnTab2"], tab ~= TAB_MY_PLAYLISTS)

		showSubCatList(tab == TAB_ENTERTAINMENT)

		getMediaList()
	end
end

-- On the entertainment tab, we have 2 lists - categories and items
-- Have to move the right one to the correct spot
function showSubCatList(show)
	local catList = gHandles["lstCategories"]
	local mediaList = gHandles["lstMedia"]

	local leftListLoc = Control_GetLocationX(catList)
	local listWidth = Control_GetWidth(catList)

	-- if cateogry list shows, we want media list to be half size (the same width as cat list) and placed to the right of the category list
	if show then
		Control_SetLocationX(mediaList, leftListLoc + listWidth)
		Control_SetSize(mediaList, listWidth, Control_GetHeight(mediaList))
	else
		Control_SetLocationX(mediaList, leftListLoc)
		Control_SetSize(mediaList, listWidth*2, Control_GetHeight(mediaList))
	end

	Control_SetVisible(catList, show)
	Control_SetVisible(gHandles["imgLstLeftBG"], show)
end

--------------------------------------------------------------------------
-- Search
--------------------------------------------------------------------------
function search()
	local searchString = EditBox_GetText(gHandles["eBoxSearch"])

	-- Do this check because if it is empty, OnFocusOut has already replaced the string with "Search"
	if m_clearSearch then
		clearSearch()
		getMediaList() -- Separate because it avoids double webcalls
		return
	end

	if searchString ~= m_searchString then
		m_searchString = searchString
		getMediaList()

		showSubCatList(false)
		showSearchX(true)

		-- Show add to playlists for places that add to playlist instead of setting the video
		if m_type == PLAYLIST_TYPE_TV then
			showPlaylistSection(true)
		end
	end 
end

function clearSearch()
	-- Clear box regardless of of if it's already empty
	setDefaultSearchText()
	m_clearSearch = true
	showSearchX(false)
	showSubCatList(m_type == PLAYLIST_TYPE_TV and m_tab == TAB_ENTERTAINMENT)
	showPlaylistSection(false)
	
	if m_searchString ~= "" then 
		m_searchString = ""
	end 
end

function showSearchX(show)
	Control_SetVisible(gHandles["btnSearchX"], show)
end

function btnSearch_OnButtonClicked( buttonHandle )
	search()
end

-- Hitting the "Enter" key in a search box
function eBoxSearch_OnEditBoxString(editBoxHandle, someString)
	search()
end

function btnSearchX_OnButtonClicked( buttonHandle )
	clearSearch()
	getMediaList() -- Separate because it avoids double webcalls
end 

function eBoxSearch_OnFocusIn(editBoxHandle)
	showSearchX(false)
	if m_clearSearch then
		EditBox_SetText(editBoxHandle, "", true)
	end
	m_clearSearch = false
end

-- Also see Dialog_OnLButtonDownInside - clears focus there when clicking anywhere else
function eBoxSearch_OnFocusOut(editBoxHandle)
	local search = EditBox_GetText(editBoxHandle)
	if search == "" or m_clearSearch then
		m_clearSearch = true
		setDefaultSearchText()
	else
		m_clearSearch = false
		showSearchX(true)
	end
end

function setDefaultSearchText()
	local searchType = m_type
	if m_type == PLAYLIST_TYPE_TV then
		searchType = m_tab
	end

	EditBox_SetText(gHandles["eBoxSearch"], SEARCH_TEXT[searchType], false)
end

function showPlaylistSection(show)
	local cmbHandle = gHandles["cmbPlaylists"]
	local saveHandle = gHandles["btnSave"]

	-- Show or hide playlists
	Control_SetVisible(gHandles["stcAddTo"], show)
	Control_SetVisible(cmbHandle, show)

	--Move Controls up/down
	local locationY = CONTROL_BUFFER
	if show then
		locationY = locationY + Control_GetLocationY(cmbHandle) + Control_GetHeight(cmbHandle)
	else
		locationY = locationY + Control_GetLocationY(gHandles["imgHorizLine"])
	end
	Control_SetLocationY(saveHandle, locationY)
	Control_SetLocationY(gHandles["btnCancel"], locationY)

	--Resize Menu
	local dialogHeight = Control_GetLocationY(saveHandle) + Control_GetHeight(saveHandle) + CONTROL_BUFFER + Dialog_GetCaptionHeight(gDialogHandle)
	MenuSetSizeThis(Dialog_GetWidth(gDialogHandle), dialogHeight)

	--Set Save button text/tooltip
	local btnSaveText = show and "Add" or "Save"
	Static_SetText(saveHandle, btnSaveText)
	local btnSaveTooltip = show and TOOLTIP_ADD or TOOLTIP_SAVE
	Control_SetToolTip(saveHandle, btnSaveTooltip)
end

--------------------------------------------------------------------------
-- Saving Functions
--------------------------------------------------------------------------
function saveMedia()
	local index = ListBox_GetSelectedItemIndex(gHandles["lstMedia"])
	local playlistId = ListBox_GetSelectedItemData(gHandles["lstMedia"]) -- causes stop
	local offsiteURL = ""
	local searching = m_searchString and m_searchString ~= ""

	if searching == true and m_type == PLAYLIST_TYPE_TV  then
		if index > 0 then
			saveToPlaylist()
		end
		return
	end

	-- Nothing Selected
	if index < 0 then
		return

	-- Selected <None>
	elseif index == 0 then
		Log("saveMedia: NONE SELECTED")

	-- Selected ALL from the my playlists tab
	elseif index == 1 and (m_type == PLAYLIST_TYPE_TV and m_tab == TAB_MY_PLAYLISTS and not searching) then
		Log("saveMedia: ALL SELECTED")

	-- Selected User Defined Playlist
	elseif index > 0 then
		-- Since the tab "my playlists" has the 'ALL' option we need to subtract by 1
		if m_type == PLAYLIST_TYPE_TV and m_tab == TAB_MY_PLAYLISTS then
			index = index - 1
		end

		local mediaItem = m_mediaList[index] -- Listbox index starts at 0, and m_mediaList starts at 1. Since we add a "<None>" to the top of the list the index corresponds correctly

		-- the source of this playlist was from Kaneva\WOK. A 3dapp DB does not have
		-- these playlist assets and can't look up additional information about the
		-- asset in their local DB. Thus, we provide additional information about the asset that
		-- the 3dapp DB does not have. Brett 04/15/10.

		if  m_edit ~= 1 then
			offsiteURL = UnescapeHTML(mediaItem.offsiteURL)
			local enc_offsiteURL = KEP_UrlEncode(offsiteURL)
			offsiteURL = enc_offsiteURL
			if offsiteURL == nil then
				offsiteURL = ""
			end
		end

		Log("saveMedia: SELECTED["..index.."] - '"..mediaItem.name.."' playlistId="..tostring(playlistId))

		if m_edit == 1 then
			local event = KEP_EventCreate( "PLAYLIST_SELECT_EVENT" )
			KEP_EventEncodeString(event, m_target)
			KEP_EventEncodeString( event, mediaItem.name)
			KEP_EventEncodeString( event, mediaItem.playlistString)
			KEP_EventQueue( event )
			MenuCloseThis()
			return
		end
	end

	local event = KEP_EventCreate( BrowserIds.BROWSER_REQUEST_EVENT_NAME )
	local playlistString = "changePlaylist?objId=" .. m_objId

	if m_type ~= PLAYLIST_TYPE_TV then
		playlistString = playlistString.. "&assetId="
	else
		playlistString = playlistString .. "&plId="
	end

	playlistString = playlistString .. tostring(playlistId)

	--Additional info for flash widgets/games
	if index > 0 and (m_type ~= PLAYLIST_TYPE_TV ) then
		playlistString = playlistString .. "&mature=" .. m_mediaList[index].mature .. "&asset_type=" .. m_mediaList[index].assetType .. "&stop=0&offsite_url=" .. offsiteURL
	end

	KEP_EventEncodeString( event, playlistString )
	KEP_EventAddToServer( event )
	KEP_EventQueue( event )
	if m_type == PLAYLIST_TYPE_GAME then
		KEP_EventCreateAndQueue( "FlashGameUpdatedEvent" )
	end

	MenuCloseThis()
end

function cmbPlaylists_OnComboBoxSelectionChanged( cmbHandle, sel_index, sel_text)

	-- If it's the last item were creating a new playlist
	local cmbSize = ComboBox_GetNumItems(cmbHandle)
	if sel_index == cmbSize - 1 then
		ComboBox_SetSelectedByIndex(cmbHandle, 0)

		MenuOpenModal("SaveSmartObject.xml")

		local event = KEP_EventCreate("SetupSaveEvent")
		KEP_EventEncodeString(event, "Playlist")
		KEP_EventQueue(event)
	end
end

function SavePlaylistNameEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local name = KEP_EventDecodeString(event)
	local cmbHandle = gHandles["cmbPlaylists"]
	local cmbSize = ComboBox_GetNumItems(cmbHandle) - 1

	--Remove the last item (add new), add the name, then add it back again
	ComboBox_RemoveItem(cmbHandle, cmbSize)
	ComboBox_AddItem(cmbHandle, name, 0)
	ComboBox_AddItem(cmbHandle, CREATE_NEW_TEXT, -1)

	ComboBox_SetSelectedByIndex(cmbHandle, cmbSize)
end

function saveToPlaylist()
	local assetId = ListBox_GetSelectedItemData( gHandles["lstMedia"] )
	local playlistSelected = ComboBox_GetSelectedText(gHandles["cmbPlaylists"])
	local enc_playlistSelected = KEP_UrlEncode(playlistSelected)
	makeWebCall(WEB_ADDRESS_FOR_PLAYLISTS .. ADD_PLAYLIST .. "&aId=" .. assetId .. "&p=" .. enc_playlistSelected ..
			"&FromGameplayMode=" .. m_gameplayMode ..
            "&FromObjPlacementId=" .. m_objId ..
            "&FromZoneInstanceId=" .. KEP_GetZoneInstanceId() ..
            "&FromZoneType=" .. InstanceId_GetType(KEP_GetCurrentZoneIndex())	
			, WF.ADD_TO_PLAYLIST,  1, MAX_ITEMS)

	ListBox_ClearSelection(gHandles["lstMedia"])
end

function clearLists()
	m_mediaList = {}
	ListBox_RemoveAllItems(gHandles["lstMedia"])
	ListBox_ClearSelection(gHandles["lstMedia"])
	
	-- If nothing is selected, Save should be disabled
	Control_SetEnabled(gHandles["btnSave"], false)

	-- Don't want to clear the categories list if searching
	if m_type == PLAYLIST_TYPE_TV and m_tab == TAB_ENTERTAINMENT and m_searchString and m_searchString ~= "" then
		return
	end

	ListBox_ClearSelection(gHandles["lstCategories"])
	ListBox_RemoveAllItems(gHandles["lstCategories"])
end

function lstCategories_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	selectCategory(sel_index)
end

function lstMedia_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	Control_SetEnabled(gHandles["btnSave"], true)
end

function selectCategory(index)
	local group_category_id = ListBox_GetItemData(gHandles["lstCategories"], index)
	local webAddress = WEB_ADDRESS_FOR_PLAYLISTS .. GET_SUBLISTS .. "&gcId=" .. group_category_id
	makeWebCall(webAddress, WF.PLAYLIST,  1, MAX_ITEMS)
	ListBox_ClearSelection(gHandles["lstMedia"])
end

function btnAddNew_OnButtonClicked( buttonHandle )
	KEP_LaunchBrowser( WEB_ADDRESS_UPLOAD .. "&FromGameplayMode=" .. m_gameplayMode ..
                "&FromObjPlacementId=" .. m_objId ..
                "&FromZoneInstanceId=" .. KEP_GetZoneInstanceId() ..
                "&FromZoneType=" .. InstanceId_GetType(KEP_GetCurrentZoneIndex()))
end

-- Called when a player changes Framework mode or when the Framework changes states (enabled/disabled).
function returnFrameworkStateHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local eventTable = Events.decode(Event_DecodeString(event) or "")
	-- Player mode set
	if eventTable.playerMode ~= nil then
		m_gameplayMode = eventTable.playerMode == "Player" and "Player" or "Creator"
	end
end

--------------------------------------------------------------------------
-- Closing Functions
--------------------------------------------------------------------------
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function btnSave_OnButtonClicked(buttonHandle)
	saveMedia()
end
	