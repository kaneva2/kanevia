--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

g_filter = 0
g_objectid = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "YesNoBoxEventHandler", "YesNoBoxEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "YesNoBoxEvent", KEP.MED_PRIO )
	KEP_EventRegister( "YesNoAnswerEvent", KEP.MED_PRIO )
end

function YesNoBoxEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local message = KEP_EventDecodeString( event )
	local title = KEP_EventDecodeString( event )

	g_filter = filter
	g_objectid = objectid

	sh = Dialog_GetStatic( gDialogHandle, "lblMessage" )
	Static_SetText( sh, message )

	sh = Dialog_GetStatic( gDialogHandle, "Menu_Title" )
	Static_SetText( sh, title )

	-- exception might be thrown when querying width and height parameters as some scripts might not have been updated
	-- DMA: I hate exceptions, adding a check here to see if there are more parameters left to decode.
	if KEP_EventMoreToDecode( event ) == 0 then
		return
	end

	local width, height
	width = -1
	height = -1
	width = KEP_EventDecodeNumber( event )
	height = KEP_EventDecodeNumber( event )
	if width~=-1 or height~=-1 then
		local dlgW, dlgH, ctl, ctlW, ctlH, ctlX, ctlY
		dlgW = Dialog_GetWidth( gDialogHandle )
		dlgH = Dialog_GetHeight( gDialogHandle )
		if width==-1 then
			width = dlgW
		end
		if height==-1 then
			height = dlgH
		end

		-- 1. set dialog size
		MenuSetSizeThis( width, height )

		-- 2. resize dialog top ("Top")
		ctl = Dialog_GetControl( gDialogHandle, "Top" )
		ctlW = Control_GetWidth( ctl )
		ctlH = Control_GetHeight( ctl )
		Control_SetSize( ctl, ctlW + width - dlgW, ctlH )

		-- 3. resize and relocate dialog bottom ("Bottom")
		ctl = Dialog_GetControl( gDialogHandle, "Bottom" )
		ctlW = Control_GetWidth( ctl )
		ctlH = Control_GetHeight( ctl )
		Control_SetSize( ctl, ctlW + width - dlgW, ctlH )
		ctlX = Control_GetLocationX( ctl )
		ctlY = Control_GetLocationY( ctl )
		Control_SetLocation( ctl, ctlX, ctlY + height - dlgH )

		-- 4. resize dialog background image ("WhiteBKG")
		ctl = Dialog_GetControl( gDialogHandle, "WhiteBKG" )
		ctlW = Control_GetWidth( ctl )
		ctlH = Control_GetHeight( ctl )
		Control_SetSize( ctl, ctlW + width - dlgW, ctlH + height - dlgH )

		-- 5. resize titlebar static ("Menu_Title")
		ctl = Dialog_GetControl( gDialogHandle, "Menu_Title" )
		ctlW = Control_GetWidth( ctl )
		ctlH = Control_GetHeight( ctl )
		Control_SetSize( ctl, ctlW + width - dlgW, ctlH )

		-- 6. relocate Yes/No button ("btnYes"/"btnNo")
		ctl = Dialog_GetControl( gDialogHandle, "btnYes" )
		ctlX = Control_GetLocationX( ctl )
		ctlY = Control_GetLocationY( ctl )
		Control_SetLocation( ctl, ctlX + width/2 - dlgW/2, ctlY + height - dlgH )

		ctl = Dialog_GetControl( gDialogHandle, "btnNo" )
		ctlX = Control_GetLocationX( ctl )
		ctlY = Control_GetLocationY( ctl )
		Control_SetLocation( ctl, ctlX + width/2 - dlgW/2, ctlY + height - dlgH )

		-- 7. resize message ("lblMessage")
		ctl = Dialog_GetControl( gDialogHandle, "lblMessage" )
		ctlW = Control_GetWidth( ctl )
		ctlH = Control_GetHeight( ctl )
		Control_SetSize( ctl, ctlW + width - dlgW, ctlH + height - dlgH )
	end

	if KEP_EventMoreToDecode( event ) == 0 then
		return
	end

	local flushLeft = false
	flushLeft = KEP_EventDecodeNumber( event ) ~= 0
	if flushLeft then
		local sh, ih, elem, ctl, btnX1, btnY1, btnX2, btnY2, btnW
		local leftMargin = 0

		-- change title style
		sh = Dialog_GetStatic( gDialogHandle, "Menu_Title" )
		elem = Static_GetDisplayElement( sh )
		Element_SetTextFormat( elem, TEXT_VALIGN_CENTER )	-- remove HALIGN_CENTER

		-- change message style
		sh = Dialog_GetStatic( gDialogHandle, "lblMessage" )
		leftMargin = Control_GetLocationX( Static_GetControl( sh ) )
		elem = Static_GetDisplayElement( sh )
		Element_SetTextFormat( elem, TEXT_VALIGN_CENTER + TEXT_WORDWRAP )	-- remove HALIGN_CENTER

		-- move yes/no button
		ctl = Dialog_GetControl( gDialogHandle, "btnYes" )
		btnX1 = Control_GetLocationX( ctl )
		btnY1 = Control_GetLocationY( ctl )
		btnW = Control_GetWidth( ctl )
		Control_SetLocation( ctl, leftMargin, btnY1 )

		ctl = Dialog_GetControl( gDialogHandle, "btnNo" )
		btnX2 = Control_GetLocationX( ctl )
		btnY2 = Control_GetLocationY( ctl )
		Control_SetLocation( ctl, leftMargin + btnX2 - btnX1, btnY2 )
	end

	-- Bring To Front
	MenuBringToFrontThis()
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
end

function btnYes_OnButtonClicked( buttonHandle)
	local answerEvent = KEP_EventCreate("YesNoAnswerEvent")
	if g_filter ~= 0 then
		KEP_EventSetFilter( answerEvent, g_filter)
	end
	KEP_EventSetObjectId( answerEvent, g_objectid )
	KEP_EventEncodeNumber( answerEvent, 1 )
	KEP_EventQueue( answerEvent )
	MenuCloseThis()
end

function btnNo_OnButtonClicked( buttonHandle)
	local answerEvent = KEP_EventCreate("YesNoAnswerEvent")
	-- pass fitler and object id to caller even if user says NO: this will be useful under certain situation
	if g_filter ~= 0 then
		KEP_EventSetFilter( answerEvent, g_filter)
	end
	KEP_EventSetObjectId( answerEvent, g_objectid )
	KEP_EventEncodeNumber( answerEvent, 0 )
	KEP_EventQueue( answerEvent )
	MenuCloseThis()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end
