--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\UGCFunctions.lua")

newImg = ""

gTarget = {}
gTarget.valid = 0
gTarget.pos = {}
gTarget.pos.x = 0
gTarget.pos.y = 0
gTarget.pos.z = 0
gTarget.normal = {}
gTarget.normal.x = 0
gTarget.normal.y = 0
gTarget.normal.z = 0
gTarget.type = ObjectType.DYNAMIC
gTarget.id = -1
gTarget.name = "Media Library"

g_t_LocationSettings = {}
g_locationType = nil

gNextDropId = 0

g_clearSearch = true
editClicked = false 

-- InitializeKEPEvents - Register all script events here. 
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "ShotEvent", KEP.MED_PRIO )
	KEP_EventRegister( "UGCDeployCompletionEvent", KEP.MED_PRIO )
end

-- InitializeKEPEventHandlers - Register all script event handlers here. 
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "ShotEventHandler", "ShotEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "UGCDeployCompletionHandler", "UGCDeployCompletionEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) -- REQUIRED - DO NOT REMOVE
       
	if KEP_IsOwner() then 
		Control_SetEnabled(gHandles["chkWorldThumb"], true)
		Control_SetVisible(gHandles["chkWorldThumb"], true)
		Control_SetEnabled(gHandles["stcworldThumb"], true)
		Control_SetVisible(gHandles["stcworldThumb"], true)
	else 
		Control_SetEnabled(gHandles["chkWorldThumb"], false)
		Control_SetVisible(gHandles["chkWorldThumb"], false)
		Control_SetEnabled(gHandles["stcworldThumb"], false)
		Control_SetVisible(gHandles["stcworldThumb"], false)
	end 
		
	-- Request Location Data
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..GET_PLACE_INFORMATION_SUFFIX, WF.PLACE_INFO)
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle ) -- REQUIRED - DO NOT REMOVE
end

function ShotEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local shot= KEP_EventDecodeString(event)
	local s, e, jpg = string.find(shot, "\\([^\\]-.jpg)")
	Log("ShotEventHandler: file="..jpg)
       
	local img = gHandles["selectedImg"]    
	local eh= Image_GetDisplayElement(img)
	local imgFront = Dialog_GetControl(gDialogHandle,"selectedImg")
	local imgBG = Dialog_GetControl(gDialogHandle,"imgTempBack")
	scaleImage(imgFront,imgBG,"..\\..\\Screenshots\\"..jpg)
	newImg = shot
end

function btnShare_OnButtonClicked( buttonHandle )
	local gImportInfo = {}
	gImportInfo.sessionId = -1
	gImportInfo.fullPath = newImg
	gImportInfo.category = UGC_TYPE.TEXTURE
	gImportInfo.type = UGC_TYPE.TEXTURE

	local dropId = gNextDropId
	gNextDropId = gNextDropId + 1

	UGCI.CallUGCHandler("TextureImport.xml", 0, dropId, gImportInfo, gTarget, 0, 0 )
	Control_SetEnabled(gHandles["btnShare"], false)
end

function UGCDeployCompletionHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local ugcType = Event_DecodeNumber( event )
	local ugcId = Event_DecodeNumber( event )
	local resultText = Event_DecodeString( event )         -- padded by WebCallTask
	local httpStatusCode = Event_DecodeNumber( event )     -- padded by WebCallTask

	if httpStatusCode==200 then
		local s, e = 0      -- start and end of captured string
		local sResult = nil
		local sURL = nil
		s, e, sResult = string.find( resultText, "<ReturnCode>(-*%d+)</ReturnCode>")
		s, e, sAssetId = string.find( resultText, "<AssetId>(%d+)</AssetId>")
		s, e, sURL = string.find(resultText, "<TextureUrl>(.+)</TextureUrl>")

		if sURL ~= nil and sAssetId ~= nil then
			
			-- Get Screenshot Caption
			local captionText = ""
			caption = Dialog_GetEditBox( gDialogHandle, "editCaption" )
			if editClicked == false  then
				captionText = ""
			else 
				captionText = EditBox_GetText( caption )
			end
			
			-- Send Screenshot Caption With Asset Id
			local commId = g_t_LocationSettings["CommunityId"] or ""
			local zoneIndex = g_t_LocationSettings["ZoneIndex"] or ""
			local enc_captionText = KEP_UrlEncode(captionText)
			local url = GameGlobals.WEB_SITE_PREFIX .. SEND_CHAT_BLAST_SUFFIX .. "diarytext=" .. enc_captionText .."&blasttype=CAMERA_SCREENSHOT&assetId="..sAssetId.."&commID="..commId.."&zone_index="..zoneIndex
			local checkedThumb  = getControlChecked("chkWorldThumb")
			if checkedThumb == true then 
				url = url .. "&setasthumbnail=true"	
			else 
				url = url .. "&setasthumbnail=false"	
			end 
			makeWebCall(url, 0)
		end
	else
		LogError("UGCDeployCompletionHandler: Screenshot Upload FAILED") 
	end
	
	MenuClose("TextureImport.xml")

	MenuCloseThis()
	
	return KEP.EPR_NOT_PROCESSED
end

function editCaption_OnFocusIn(editBoxHandle)
	if g_clearSearch then
		EditBox_SetText(editBoxHandle, "", true)
		editClicked = true 
	end
	g_clearSearch = false
end 

function editCaption_OnFocusOut(editBoxHandle)

	-- On exit, if blank, reset the text to "Search|"
	local search = EditBox_GetText(editBoxHandle)
	if search == "" or g_clearSearch then
		g_clearSearch = true
		EditBox_SetText(editBoxHandle, "Add caption...", false)
		editClicked = false
	else
		g_clearSearch = false
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Parse Location Data
	if filter ~= WF.PLACE_INFO then return end
	local estr = KEP_EventDecodeString( event )
	ParseLocationData(estr)
	return KEP.EPR_OK
end

function ParseLocationData(locationData)
	local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	s, strPos, result = string.find(locationData, "<ReturnCode>(.-)</ReturnCode>")
	if result == "0" then
		s, strPos, result = string.find(locationData, "<name>(.-)</name>")
		g_t_LocationSettings["Name"] = result
				
		s, strPos, result = string.find(locationData, "<current_zone_type>(.-)</current_zone_type>")
		g_t_LocationSettings["ZoneType"] = tonumber(result)
        
		s, strPos, result = string.find(locationData, "<community_id>(.-)</community_id>")
		g_t_LocationSettings["CommunityId"] = tonumber(result)
		
		s, strPos, result = string.find(locationData, "<current_zone_index>(.-)</current_zone_index>")
		g_t_LocationSettings["ZoneIndex"] = tonumber(result)
	end

	if (g_locationType == nil) then
		if (g_t_LocationSettings["ZoneType"] == 3) then
			g_locationType = HOME
		elseif(g_t_LocationSettings["ZoneType"] == 6) then
			g_locationType = COMMUNITY_HANGOUT
		elseif(g_t_LocationSettings["ZoneType"] == 4) then
			g_locationType = PUBLIC
		end	
	end
end

function getControlChecked(ctrlName)
	local ctrl = gHandles[ctrlName]
	local ctrlType = Control_GetType(ctrl)
	if (ctrlType == ControlTypes.DXUT_CONTROL_RADIOBUTTON) then
		return (CheckBox_GetChecked(RadioButton_GetCheckBox(ctrl)) == 1)
	elseif (ctrlType == ControlTypes.DXUT_CONTROL_CHECKBOX) then
		return (CheckBox_GetChecked(ctrl) == 1)
	end
	return false
end
