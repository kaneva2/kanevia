--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_NPCActionMenu.lua
--
-- Displays available interaction options with Actors
-------------------------------------------------------------------------------
--Ankit check add a onDestroy function for the menu and onDestroy send an event back to server so if it is a rider, decrement the no of players interacting
dofile("..\\ClientScripts\\MathHelper.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\CAM.lua")
dofile("..\\MenuScripts\\Lib_NpcDefinitions.lua")
dofile("MenuHelper.lua")
dofile("Framework_ActorHelper.lua")
dofile("Framework_InventoryHelper.lua")
dofile("CharacterCreationHelper.lua")
local NpcDefs = _G.NpcDefinitions -- Assign the global NpcDefinitions to a local variable for faster access

---------------
-- CONSTANTS --
---------------

-- Visuals
FONTS = {
	DEFAULT = "Verdana",
	HEAD = "Verdana",
	REP_LBL = "Verdana",
	ACT_LBL = "Verdana",
}

FONT_SIZES = {
	HEAD = 14,
	REP_LBL = 12,
	REP_CNT = 10,
	ACT_LBL = 16,
}

local FONT_BUFFER = 4

COLORS = { -- frequently used colors
    WHITE = {a=255, r=255, g=255, b=255},
    BLACK = {a=255, r=0, g=0, b=0},
    AQUA = {a = 255, r=0, g=246, b=255},
    GREY = {a = 255, r=90, g=90, b=90},
    GOLD = {a = 255, r = 230, g = 220, b = 15},
    HEAD = {a = 255, r=220, g=220, b=220},
    REP_LBL = {a = 255, r=0, g=246, b=255},
    ACT_LBL = {a = 255, r=135, g=152, b=255},
}

-- Sizing constants
local BASE_MENU_SIZE = {X = 200, Y = 300}
local MENU_PAD = {TOP = 12, BOTTOM = 10, LEFT = 10, RIGHT = 10}

local HEAD_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT, Y = FONT_SIZES.HEAD + FONT_BUFFER}
local HEAD_PAD = {BOTTOM = 4}

local REP_LBL_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT, Y = FONT_SIZES.REP_LBL + FONT_BUFFER}
local REP_LBL_PAD = {BOTTOM = 6}
local REP_BAR_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT, Y = 14}
local REP_TICK_SIZE = {X = 3, Y = 14}
local REP_CNT_PAD = {LEFT = 4}

local DIV_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT, Y = 1}
local DIV_PAD = {TOP = 12, BOTTOM = 12}

local ACT_ICON_SIZE = {X = 24, Y = 24}
local ACT_ICON_PAD = {RIGHT = 10}
local ACT_LBL_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - ACT_ICON_SIZE.X - ACT_ICON_PAD.RIGHT - MENU_PAD.RIGHT, Y = FONT_SIZES.ACT_LBL + FONT_BUFFER}
local ACT_BTN_SIZE = {X = BASE_MENU_SIZE.X - MENU_PAD.LEFT - MENU_PAD.RIGHT, Y = math.max(ACT_ICON_SIZE.Y, ACT_LBL_SIZE.Y)}
local ACT_BTN_SPACING = 16

local LOCK_SIZE = {X = 12,  Y =3 , W = 15, H = 19}

-- Menu modes
local MENU_MODE = {VENDOR = "vendor", QUEST_GIVER = "quest_giver", ACTOR = "actor", PET = "pet", PLAYER = "player", ACTOR_PET = "actor_pet", VENDOR_PET = "vendor_pet", QUEST_GIVER_PET = "quest_giver_pet"}
local ACT_LIST = {
	[MENU_MODE.VENDOR] = {"Trade", "Gift"},
	[MENU_MODE.QUEST_GIVER] = {"Quests", "Gift"},
	[MENU_MODE.ACTOR] = {"Talk", "Gift"},
	[MENU_MODE.PET] = {"Stay", "Follow", "Wander"},
	[MENU_MODE.ACTOR_PET] = {"Talk", "Stay", "Follow", "Wander", "Clothing", "Gift"},
	[MENU_MODE.VENDOR_PET] = {"Trade", "Stay", "Follow", "Wander", "Clothing", "Gift"},
	[MENU_MODE.QUEST_GIVER_PET] = {"Quests", "Stay", "Follow", "Wander", "Clothing", "Gift"},
	[MENU_MODE.PLAYER] = {"Inspect", "Chat", "Rave", "Interact"}, -- Functionality coming in P2
}

-- Class-specific
local PET_STATE = { IDLE      = 0,
					WANDER    = 1,
					STAY      = 2,
					FOLLOW    = 3,
			   		CHASE     = 4,
			   		RETURNING = 5,	   	
			   		DEAD   	  = 6
			  	  }


local TALK_ANIMATION = 4709570

---LOCATION PADDING
local CAM_PADDING = .95
local MOVE_PADDING = 8


---------------------
-- LOCAL VARIABLES --
---------------------

local m_cacheReady = false
local m_npcDataReady = false
local m_initDataProcessed = false
local m_initialized = false

-- Sizing
local m_menu_size = {x = BASE_MENU_SIZE.X, y = BASE_MENU_SIZE.Y}

-- Mode-based
local m_curr_mode = nil
local m_menu_actions = nil
local m_menu_name = "Amelia"

local m_currActorPID = nil
local m_currActorUNID = nil
local m_isFollower = false
local m_isSpouse = false
local m_isRider = false

local m_mousePos = {}
local m_mouseOff = false

local m_repValue = 0
local m_repScale = "Normal"
local m_repUnlock = 0
local m_repUnlockString = "Acquaintance"

local m_gameItems = {}

local m_equippedGLIDs = {}

local m_netID = 1000
local m_gender = MALE

local m_startCamX = nil
local m_startCamY = nil
local m_startCamZ = nil

local m_startPosX = nil
local m_startPosY = nil
local m_startPosZ = nil
--------------------
-- CORE FUNCTIONS --
--------------------

function onCreate()
	Events.registerHandler("FRAMEWORK_DEFINE_NPC_ACTIONS", defineNpcActionsHandler)
	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)
	Events.registerHandler("FRAMEWORK_CLOSE_ACTOR_MENU", onCloseMenu)
	
	KEP_EventRegister( "FRAMEWORK_NPC_LOCKED", KEP.MED_PRIO )

	KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "answerEventHandler", "YesNoAnswerEvent", KEP.MED_PRIO )

	requestInventory(GAME_PID)
	
	m_mousePos.x = Cursor_GetLocationX()
	m_mousePos.y = Cursor_GetLocationY()

	KEP_SetCurrentAnimationByGLID(TALK_ANIMATION)

	m_startCamX, m_startCamY, m_startCamZ = KEP_GetPlayerCameraOrientation()

	m_startPosX, m_startPosY, m_startPosZ = KEP_GetPlayerPosition()
end

function onDestroy()
	if m_isRider then
		Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = m_currActorPID, interacting = false})
	end
end

function Dialog_OnMoved()
	if m_initialized then
	    recenterEverything() 
    end
end

function Dialog_OnRender( dialogHandle, fElapsedTime)
	
	if (ToBool(Dialog_IsLMouseDown(dialogHandle))) then
		if m_mouseOff then
			MenuCloseThis()
		end
	end
	if m_cacheReady and m_npcDataReady and not m_initDataProcessed then
		finalizeInit()
		m_initDataProcessed = true
	end

	local camX, camY, camZ = KEP_GetPlayerCameraOrientation()
	local dotP = math.abs(dotProduct(camX, camY, camZ, m_startCamX, m_startCamY, m_startCamZ))
	if dotP < CAM_PADDING then
		MenuCloseThis()
	end

	local posX, posY, posZ = KEP_GetPlayerPosition()

	local xDiff = posX - m_startPosX
	local yDiff = posY - m_startPosY
	local zDiff = posZ - m_startPosZ
	local xyzDistanceSqrd = xDiff * xDiff + zDiff * zDiff + yDiff * yDiff
	local paddingSquared  = MOVE_PADDING * MOVE_PADDING
	if (paddingSquared <= xyzDistanceSqrd) then
		MenuCloseThis()
	end

end

function Dialog_OnMouseMove( dialogHandle, x, y )
	if Control_ContainsPoint(gHandles["imgMenuBG"], x, y) ~= 0 then
		m_mouseOff = false
	else
		m_mouseOff = true
	end
end
-------------------
-- GUI FUNCTIONS --
-------------------

function generateMenuElements()
	local insertPoint_y = MENU_PAD.TOP -- Tracks the Y-value insert point for new menu elements
	-- Add the background
	-- addImage("imgMenuBG", rect(0, 0, m_menu_size.x, m_menu_size.y), COLORS.GREY, "Framework_HudElements.tga", rect(377, 0, 431 - 377, 54))
	Control_SetVisible(gHandles["imgMenuBG"], true)
	Control_SetSize(gHandles["imgMenuBG"], m_menu_size.x, m_menu_size.y)
	Control_SetLocationX(gHandles["imgMenuBG"], 0)
    Control_SetLocationY(gHandles["imgMenuBG"], 0)
    Control_SetVisible(gHandles["imgMenuBG"], true)
	
	-- Add the header (npc/pet/player name)
	addLabel("stcHead", m_menu_name, rect(MENU_PAD.LEFT, insertPoint_y, HEAD_SIZE.X, HEAD_SIZE.Y), FONTS.HEAD, FONT_SIZES.HEAD, COLORS.HEAD, true)
	-- Element_AddFont(Static_GetDisplayElement("stcHead"), gDialogHandle, FONTS.HEAD, -1 * FONT_SIZES.HEAD, 800, false)
	insertPoint_y = insertPoint_y + HEAD_SIZE.Y

	-- If a vendor, NPC, or quest giver, add the reputation bar
	if m_curr_mode == MENU_MODE.ACTOR or
		m_curr_mode == MENU_MODE.VENDOR or
		m_curr_mode == MENU_MODE.QUEST_GIVER or
		m_curr_mode == MENU_MODE.ACTOR_PET or
		m_curr_mode == MENU_MODE.VENDOR_PET or
		m_curr_mode == MENU_MODE.QUEST_GIVER_PET then

		addLabel("stcRepLbl", "Reputation: Close Friend", rect(MENU_PAD.LEFT, HEAD_PAD.BOTTOM + insertPoint_y, REP_LBL_SIZE.X, REP_LBL_SIZE.Y), FONTS.REP_LBL, FONT_SIZES.REP_LBL, COLORS.REP_LBL, false)
		insertPoint_y = insertPoint_y + HEAD_PAD.BOTTOM + REP_LBL_SIZE.Y
		
		addImage("imgRepBarBG", rect(MENU_PAD.LEFT - 1, REP_LBL_PAD.BOTTOM + insertPoint_y - 1, REP_BAR_SIZE.X + 2, REP_BAR_SIZE.Y + 2), COLORS.BLACK, "white.tga", rect(0, 0, -1, -1))	
		--insertPoint_y = insertPoint_y + REP_LBL_PAD.BOTTOM + REP_BAR_SIZE.Y

		addImage("imgRepBarEmpty", rect(MENU_PAD.LEFT, REP_LBL_PAD.BOTTOM + insertPoint_y, REP_BAR_SIZE.X, REP_BAR_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.tga", rect(244, 1, 1, 15))	
		--insertPoint_y = insertPoint_y + REP_LBL_PAD.BOTTOM + REP_BAR_SIZE.Y

		local maxScale = NpcDefs.REP_SCALES[m_repScale][NpcDefs.REP_LEVELS[NpcDefs.MAX_REP_LEVEL]]
		local repBarWidth = math.min(REP_BAR_SIZE.X * (m_repValue / maxScale), REP_BAR_SIZE.X)
		addImage("imgRepBar", rect(MENU_PAD.LEFT, REP_LBL_PAD.BOTTOM + insertPoint_y, repBarWidth, REP_BAR_SIZE.Y), COLORS.AQUA, "InteractionMenuIcons.tga", rect(251, 2, 1, 13))

		local friendValue = NpcDefs.REP_SCALES[m_repScale][NpcDefs.REP_LEVELS[3]]
		local friendTickX = REP_BAR_SIZE.X * (friendValue / maxScale)
		addImage("imgTickClose", rect(MENU_PAD.LEFT + friendTickX, REP_LBL_PAD.BOTTOM + insertPoint_y, REP_TICK_SIZE.X, REP_TICK_SIZE.Y), COLORS.BLACK, "InteractionMenuIcons.tga", rect(243, 1, 1, 15))	

		local closeValue = NpcDefs.REP_SCALES[m_repScale][NpcDefs.REP_LEVELS[4]]
		local closeTickX = REP_BAR_SIZE.X * (closeValue / maxScale)
		addImage("imgTickInner", rect(MENU_PAD.LEFT + closeTickX, REP_LBL_PAD.BOTTOM + insertPoint_y, REP_TICK_SIZE.X, REP_TICK_SIZE.Y), COLORS.BLACK, "InteractionMenuIcons.tga", rect(243, 1, 1, 15))	

		local goalTick = true
		local goalTickX = REP_BAR_SIZE.X
		local repText = "Reputation : "
		if m_isSpouse then
			repText = repText .. NpcDefs.SPECIAL_LEVELS[2]
			goalTick = false
		elseif m_isFollower then
			repText = repText .. NpcDefs.SPECIAL_LEVELS[1]
			goalTick = false
		elseif m_repValue < friendValue then
			goalTickX = friendTickX
			repText = repText .. NpcDefs.REP_LEVELS[2]
		elseif m_repValue < closeValue then
			goalTickX = closeTickX
			repText = repText .. NpcDefs.REP_LEVELS[3]
		elseif m_repValue < maxScale then
			repText = repText .. NpcDefs.REP_LEVELS[4]
		else
			repText = repText .. NpcDefs.REP_LEVELS[NpcDefs.MAX_REP_LEVEL]
			goalTick = false
		end

		-- Mouseover number values of reputation, to display above the reputation bar
		local repBarCntString = formatCommaValue(tostring(m_repValue)).." \/ "..formatCommaValue(tostring(maxScale))
		addLabel("stcRepBarCnt", repBarCntString, rect(MENU_PAD.LEFT + REP_CNT_PAD.LEFT, REP_LBL_PAD.BOTTOM + insertPoint_y, REP_BAR_SIZE.X - REP_CNT_PAD.LEFT, REP_BAR_SIZE.Y), FONTS.DEFAULT, FONT_SIZES.REP_CNT, COLORS.WHITE, false)
		addButton("btnRepBarCnt", " ", rect(MENU_PAD.LEFT, REP_LBL_PAD.BOTTOM + insertPoint_y, REP_BAR_SIZE.X, REP_BAR_SIZE.Y), "invisible.tga", rect(0, 0, 1, 1)) -- Invisible button
		if m_repValue > 0 then
			Control_SetVisible(Dialog_GetStatic(gDialogHandle,"stcRepBarCnt"), false)
		end
		_G["btnRepBarCnt_OnMouseEnter"] = function(buttonHandle)
			Control_SetVisible(Dialog_GetStatic(gDialogHandle,"stcRepBarCnt"), true)
		end
		_G["btnRepBarCnt_OnMouseLeave"] = function(buttonHandle)
			if m_repValue > 0 then
				Control_SetVisible(Dialog_GetStatic(gDialogHandle,"stcRepBarCnt"), false)
			end
		end

		-- TO DO: reflect follower, spouse status properly
		local repLabel = Dialog_GetStatic(gDialogHandle, "stcRepLbl")
		Static_SetText(repLabel, repText)

		if goalTick then
			addImage("imgTickGoal", rect(MENU_PAD.LEFT + goalTickX, REP_LBL_PAD.BOTTOM + insertPoint_y, REP_TICK_SIZE.X, REP_TICK_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.tga", rect(244, 18, 1, 13))
		end
		insertPoint_y = insertPoint_y + REP_LBL_PAD.BOTTOM + REP_BAR_SIZE.Y
	end

	-- Add a divider line
	addImage("imgDiv", rect(MENU_PAD.LEFT, DIV_PAD.TOP + insertPoint_y, DIV_SIZE.X, DIV_SIZE.Y), COLORS.GREY, "white.tga", rect(0, 0, -1, -1))
	insertPoint_y = insertPoint_y + DIV_PAD.TOP + DIV_SIZE.Y + DIV_PAD.BOTTOM

	-- Add the relevant action buttons
	for i,v in pairs(m_menu_actions) do

		local showClothing = false
		local NPCProperties = nil
		if v == "Clothing" and m_gameItems[tostring(m_currActorUNID)] then
			NPCProperties = m_gameItems[tostring(m_currActorUNID)]
			if NPCProperties.meshType and NPCProperties.meshType ~= 1 and NPCProperties.behaviorParams.customization == true then
				showClothing = true
			end
		end
		-- Set icon
		if v == "Quests" then
			addImage("imgActIcon"..tostring(i).."a", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(0, 0, 24, 24))
			addImage("imgActIcon"..tostring(i).."b", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(0 + 120, 0, 24, 24))
		elseif v == "Talk" then
			addImage("imgActIcon"..tostring(i).."a", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(96, 0, 24, 24))
			addImage("imgActIcon"..tostring(i).."b", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(96 + 120, 0, 24, 24))
		elseif v == "Trade" then
			addImage("imgActIcon"..tostring(i).."a", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(96, 24, 24, 24))
			addImage("imgActIcon"..tostring(i).."b", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(96 + 120, 24, 24, 24))
		elseif v == "Gift" then
			addImage("imgActIcon"..tostring(i).."a", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(48, 0, 24, 24))
			addImage("imgActIcon"..tostring(i).."b", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(48 + 120, 0, 24, 24))
		elseif v == "Stay" then
			addImage("imgActIcon"..tostring(i).."a", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(0, 24, 24, 24))
			addImage("imgActIcon"..tostring(i).."b", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(0 + 120, 24, 24, 24))
		elseif v == "Follow" then
			addImage("imgActIcon"..tostring(i).."a", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(24, 24, 24, 24))
			addImage("imgActIcon"..tostring(i).."b", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(24 + 120, 24, 24, 24))
		elseif v == "Wander" then
			addImage("imgActIcon"..tostring(i).."a", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(48, 24, 24, 24))
			addImage("imgActIcon"..tostring(i).."b", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(48 + 120, 24, 24, 24))
		elseif showClothing then
			addImage("imgActIcon"..tostring(i).."a", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.tga", rect(96, 48, 24, 24))
			addImage("imgActIcon"..tostring(i).."b", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.tga", rect(96 + 120, 48, 24, 24))
		end

		if not (v == "Clothing" and showClothing == false) then
			
			Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgActIcon"..tostring(i).."b"), false)

			addLabel("stcActLbl"..tostring(i), v, rect(MENU_PAD.LEFT + ACT_ICON_SIZE.X + ACT_ICON_PAD.RIGHT, insertPoint_y + (ACT_ICON_SIZE.Y - ACT_LBL_SIZE.Y)/2, ACT_LBL_SIZE.X, ACT_LBL_SIZE.Y), FONTS.ACT_LBL, FONT_SIZES.ACT_LBL, COLORS.ACT_LBL, true)
			--Lock up the first action if not at the right level
			if i == 1 and m_curr_mode ~= MENU_MODE.PET then
				if 	(m_repValue < m_repUnlock and m_repUnlockString ~= NpcDefs.SPECIAL_LEVELS[1] and m_repUnlockString ~= NpcDefs.SPECIAL_LEVELS[2]) or
					(not m_isFollower and m_repUnlockString == NpcDefs.SPECIAL_LEVELS[1]) or
					(not m_isSpouse and m_repUnlockString == NpcDefs.SPECIAL_LEVELS[2]) then
						addImage("imgLocked", rect(MENU_PAD.LEFT + LOCK_SIZE.X, insertPoint_y + LOCK_SIZE.Y, LOCK_SIZE.W, LOCK_SIZE.H), COLORS.WHITE, "InteractionMenuIcons.tga", rect(240, 33, 15, 19))
				end
			end
			addButton("btnAct"..tostring(i), " ", rect(MENU_PAD.LEFT, insertPoint_y, ACT_BTN_SIZE.X, ACT_BTN_SIZE.Y), "invisible.tga", rect(0, 0, 1, 1)) -- Invisible button
						
			insertPoint_y = insertPoint_y + ACT_BTN_SIZE.Y + ACT_BTN_SPACING
			
			_G["btnAct" .. tostring(i) .. "_OnMouseEnter"] = function(buttonHandle)
				-- Add the glow and bold when hovering over Quest
				BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(Dialog_GetStatic(gDialogHandle, "stcActLbl"..tostring(i)))), 0, COLORS.WHITE)
				Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgActIcon"..tostring(i).."a"), false)
				Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgActIcon"..tostring(i).."b"), true)
			end
			_G["btnAct" .. tostring(i) .. "_OnMouseLeave"] = function(buttonHandle)
				-- Removes the glow and bold when the mouse leaves
				BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(Dialog_GetStatic(gDialogHandle, "stcActLbl"..tostring(i)))), 0, COLORS.ACT_LBL)
				Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgActIcon"..tostring(i).."a"), true)
				Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgActIcon"..tostring(i).."b"), false)
			end

			-- Set button functions
			if i == 1 and m_curr_mode ~= MENU_MODE.PET then -- First action triggers basic activate (gifting currently does nothing)
				_G["btnAct"..tostring(i).."_OnButtonClicked"] = function(btnHandle)
					if 	(m_repValue < m_repUnlock and m_repUnlockString ~= NpcDefs.SPECIAL_LEVELS[1] and m_repUnlockString ~= NpcDefs.SPECIAL_LEVELS[2]) or
						(not m_isFollower and m_repUnlockString == NpcDefs.SPECIAL_LEVELS[1]) or
						(not m_isSpouse and m_repUnlockString == NpcDefs.SPECIAL_LEVELS[2]) then
							MenuOpen("Framework_NPCLocked.xml")
						
							local event = KEP_EventCreate("FRAMEWORK_NPC_LOCKED")
							KEP_EventEncodeString(event, "You do not have enough reputation with "..tostring(m_menu_name))
							if m_repUnlockString == "Acquaintance" then
								KEP_EventEncodeString(event, "You need to be an <b> "..tostring(m_repUnlockString).."</b>")
							else
								KEP_EventEncodeString(event, "You need to be a <b> "..tostring(m_repUnlockString).."</b>")
							end
							KEP_EventQueue(event)
					else
						Events.sendEvent("ACTIVATE_ACTOR", {PID = m_currActorPID})
					end
					MenuCloseThis()
				end
			elseif v == "Gift" then
				_G["btnAct"..tostring(i).."_OnButtonClicked"] = function(btnHandle)
					MenuOpen("Framework_NPCGifting.xml")
					Events.sendEvent("REQUEST_INIT_NPC_GIFTING", {PID = m_currActorPID})
					MenuCloseThis()
				end
			elseif v == "Clothing" then
				_G["btnAct"..tostring(i).."_OnButtonClicked"] = function(btnHandle)
					spawnModelNPC()
					Events.sendEvent("FRAMEWORK_HIDE_FOLLOWER", {hide = true, PID = m_currActorPID})
					--DO CLOTHING STUFF
					if not MenuIsOpen("UnifiedNavigation.xml") then
						MenuOpen("UnifiedNavigation.xml")
					end
					MenuOpen("Framework_NPCInventory")
					local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
					KEP_EventEncodeString(ev, "npc")
					KEP_EventQueue(ev)
	
					local npcEvent = KEP_EventCreate("FRAMEWORK_OPEN_NPC_INVENTORY")
					KEP_EventEncodeString(npcEvent, "")-- Equipped Glids
					KEP_EventEncodeNumber(npcEvent, m_netID)	--Net ID
					local gender  = 0
					if m_gender == FEMALE then
						gender = 1
					end
					KEP_EventEncodeNumber(npcEvent, gender)	--Gender: 1 = female, 0 = male
					KEP_EventEncodeNumber(npcEvent, m_currActorPID)
					KEP_EventQueue(npcEvent)
					MenuCloseThis()
				end
			elseif v == "Stay" then
				_G["btnAct"..tostring(i).."_OnButtonClicked"] = function(btnHandle)
					Events.sendEvent("UPDATE_PET_STATE", {PID = m_currActorPID, stateChange = PET_STATE.STAY})
					MenuCloseThis()
				end
			elseif v == "Follow" then
				_G["btnAct"..tostring(i).."_OnButtonClicked"] = function(btnHandle)
					Events.sendEvent("UPDATE_PET_STATE", {PID = m_currActorPID, stateChange = PET_STATE.FOLLOW})
					MenuCloseThis()
				end
			elseif v == "Wander" then
				_G["btnAct"..tostring(i).."_OnButtonClicked"] = function(btnHandle)
					Events.sendEvent("UPDATE_PET_STATE", {PID = m_currActorPID, stateChange = PET_STATE.WANDER})
					MenuCloseThis()
				end
			end
		end
	end

	-- Add the dismiss button
	if m_isFollower then
		addImage("imgActIconDismissA", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(72, 24, 24, 24))
		addImage("imgActIconDismissB", rect(MENU_PAD.LEFT, insertPoint_y, ACT_ICON_SIZE.X, ACT_ICON_SIZE.Y), COLORS.WHITE, "InteractionMenuIcons.dds", rect(72 + 120, 24, 24, 24))

		Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgActIconDismissB"), false)

		addLabel("stcActLblDismiss", "Dismiss", rect(MENU_PAD.LEFT + ACT_ICON_SIZE.X + ACT_ICON_PAD.RIGHT, insertPoint_y + (ACT_ICON_SIZE.Y - ACT_LBL_SIZE.Y)/2, ACT_LBL_SIZE.X, ACT_LBL_SIZE.Y), FONTS.ACT_LBL, FONT_SIZES.ACT_LBL, COLORS.ACT_LBL, true)
		addButton("btnActDismiss", " ", rect(MENU_PAD.LEFT, insertPoint_y, ACT_BTN_SIZE.X, ACT_BTN_SIZE.Y), "invisible.tga", rect(0, 0, 1, 1)) -- Invisible button

		insertPoint_y = insertPoint_y + ACT_BTN_SIZE.Y + ACT_BTN_SPACING
		
		_G["btnActDismiss_OnMouseEnter"] = function(buttonHandle)
			-- Add the glow and bold when hovering over Quest
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(Dialog_GetStatic(gDialogHandle, "stcActLblDismiss"))), 0, COLORS.WHITE)
			Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgActIconDismissA"), false)
			Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgActIconDismissB"), true)
		end
		_G["btnActDismiss_OnMouseLeave"] = function(buttonHandle)
			-- Removes the glow and bold when the mouse leaves
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(Dialog_GetStatic(gDialogHandle, "stcActLblDismiss"))), 0, COLORS.ACT_LBL)
			Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgActIconDismissA"), true)
			Control_SetVisible(Dialog_GetImage(gDialogHandle,"imgActIconDismissB"), false)
		end

		_G["btnActDismiss_OnButtonClicked"] = function(btnHandle)
			MenuOpenModal("YesNoBox.xml")
			local yesnoEvent = KEP_EventCreate( "YesNoBoxEvent" )
			KEP_EventEncodeString( yesnoEvent, "Are you sure you want to remove this Follower from the world?" )
			KEP_EventEncodeString( yesnoEvent, "Confirm" )
			KEP_EventQueue( yesnoEvent )
		end
	end

	insertPoint_y = insertPoint_y - ACT_BTN_SPACING -- Eliminate padding after the very last action in the list

	m_menu_size.y = insertPoint_y + MENU_PAD.BOTTOM -- Update menu size based on generated elements
	m_initialized = true
	recenterEverything() -- Resize and reposition the menu
end

function recenterEverything()
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
    local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
    Dialog_SetSize(gDialogHandle, m_menu_size.x, m_menu_size.y)
    Control_SetSize(gHandles["imgMenuBG"], m_menu_size.x, m_menu_size.y)
    SetMenuPositon()
end

function SetMenuPositon()
	local bufferSize = 25
	local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	local firstActionPosY = Control_GetLocationY(Dialog_GetButton(gDialogHandle, "btnAct1"))

	local posX = m_mousePos.x + bufferSize
	local posY = m_mousePos.y - firstActionPosY

	-- Prevent going off the right side of the screen
	if posX + m_menu_size.x > screenWidth then
		posX = screenWidth - m_menu_size.x - bufferSize
	end
	-- Prevent going off the bottom of the screen
	if posY + m_menu_size.y > screenHeight then
		posY = screenHeight - m_menu_size.y - bufferSize
	-- Prevent going off the top of the screen
	elseif posY < 0 then
		posY = bufferSize
	end

	Dialog_SetLocation(gDialogHandle, posX, posY)
end

-- TO DO: Remove this unused code
function removeHandles()
	Control_SetVisible(gHandles["imgMenuBG"], false)
	Dialog_RemoveControl(gDialogHandle, "stcHead")
	if 	m_curr_mode == MENU_MODE.ACTOR or
		m_curr_mode == MENU_MODE.VENDOR or
		m_curr_mode == MENU_MODE.QUEST_GIVER or
		m_curr_mode == MENU_MODE.ACTOR_PET or
		m_curr_mode == MENU_MODE.VENDOR_PET or
		m_curr_mode == MENU_MODE.QUEST_GIVER_PET then
			Dialog_RemoveControl(gDialogHandle, "stcRepLbl")
			Dialog_RemoveControl(gDialogHandle, "imgRepBar")
			Dialog_RemoveControl(gDialogHandle, "stcRepBarCnt")
			Dialog_RemoveControl(gDialogHandle, "btnRepBarCnt")
	end
	Dialog_RemoveControl(gDialogHandle, "imgDiv")
	for i,v in pairs(m_menu_actions) do
		Dialog_RemoveControl(gDialogHandle, "imgActIcon"..tostring(i).."a")
		Dialog_RemoveControl(gDialogHandle, "imgActIcon"..tostring(i).."b")
		Dialog_RemoveControl(gDialogHandle, "stcActLbl"..tostring(i))
		Dialog_RemoveControl(gDialogHandle, "btnAct"..tostring(i))
	end
	if m_isFollower then
		Dialog_RemoveControl(gDialogHandle, "imgActIconDismissA")
		Dialog_RemoveControl(gDialogHandle, "imgActIconDismissB")
		Dialog_RemoveControl(gDialogHandle, "stcActLblDismiss")
		Dialog_RemoveControl(gDialogHandle, "btnActDismiss")
	end
end

-- TO DO: Remove this unused code
function reloadMenuInMode(mode)
	m_initialized = false
	removeHandles()
	m_curr_mode = mode
	m_menu_actions = ACT_LIST[mode]
	generateMenuElements()
end

--------------------
-- EVENT HANDLERS --
--------------------

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)

	m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	m_gameItems = gameItems
	m_cacheReady = true
end

function defineNpcActionsHandler(event)
	m_isRider = event.isRider or false
	if m_isRider then
		Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = event.actorPID, interacting = true})
	end
	if event.actorPID == nil then return end
	if event.UNID == nil then return end

	m_currActorPID = event.actorPID
	m_currActorUNID = event.UNID
	m_isFollower = event.isFollower or false
	m_isSpouse = event.isSpouse or false
	m_repValue = event.value or 0
	m_equippedGLIDs = event.equippedGLIDs or {}

	m_npcDataReady = true
end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = KEP_EventDecodeNumber( event )
	if answer ~= 0 then -- Confirm dismiss follower
		-- MenuOpen("Framework_NPCGifting.xml")
		Events.sendEvent("DISMISS_FOLLOWER", {PID = m_currActorPID})
		MenuCloseThis()
	end
end

function finalizeInit()
	local actorItem = m_gameItems[tostring(m_currActorUNID)]
	if actorItem then
		local mode = actorItem.behavior
		if mode == "shop_vendor" then
			mode = "vendor"
		end
		if m_isFollower then mode = mode.."_pet" end
		m_curr_mode = mode
		if mode == "pet" then
			m_menu_name = actorItem.name
		else
			m_menu_name = actorItem.behaviorParams.actorName
		end
		m_repScale = actorItem.behaviorParams.repScale or "Normal"
		m_repUnlockString = actorItem.behaviorParams.repUnlock or NpcDefs.REP_LEVELS[2]
		m_repUnlock = NpcDefs.REP_SCALES[m_repScale][m_repUnlockString] or 0
	end
	if m_curr_mode then
		m_menu_actions = ACT_LIST[m_curr_mode]
		generateMenuElements()
	end
end

---------------------
-- LOCAL FUNCTIONS --
---------------------

rect = function(x, y, w, h)
    return { x = x, y = y, w = w, h = h }
end

color = function(a, r, g, b)
    return { a = a, r = r, g = g, b = b }
end

addImage = function(name, rect, color, texName, texRect)
    local img = Dialog_GetImage(gDialogHandle, name)
    if img == nil then
        img = Dialog_AddImage(gDialogHandle, name, rect.x, rect.y, texName, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
        Image_SetColor(img, color)
        Control_SetSize(Image_GetControl(img), rect.w, rect.h)
    end
    return img
end

addButton = function(name, text, rect, texName, texRect)
    local btn = Dialog_GetButton(gDialogHandle, name)
    if btn == nil then
        btn = Dialog_AddButton(gDialogHandle, name, text, rect.x, rect.y, rect.w, rect.h)
    end

    local elem
    elem = Button_GetDisabledDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetFocusedDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetMouseOverDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetNormalDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetPressedDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)

    return btn
end

addLabel = function(name, text, rect, fontFace, fontSize, fontColor, bold)
    local stc = Dialog_GetStatic(gDialogHandle, name)
    if stc == nil then
        local stcFontSize = fontSize + 2
        local stcW = stcFontSize*#text*0.8
        stc = Dialog_AddStatic(gDialogHandle, name, text, rect.x, rect.y, stcW, rect.h)
        local elem = Static_GetDisplayElement(stc)
        local bc = Element_GetFontColor(elem)
        local weight = 400
        if bold then
        	weight = 800
        end
        Element_AddFont(elem, gDialogHandle, fontFace, stcFontSize, weight, false)
        Element_SetTextFormat(elem, TEXT_HALIGN_LEFT + TEXT_VALIGN_CENTER)
        BlendColor_SetColor(bc, 0, fontColor)
    end
end

-- Copy and paste controls
copyControl = function(menuControl)
    if menuControl then
        Dialog_CopyControl( gDialogHandle, menuControl )
        Dialog_PasteControl( gDialogHandle )

        local numControls = Dialog_GetNumControls( gDialogHandle )

        local newControl = Dialog_GetControlByIndex( gDialogHandle, numControls - 1 )
        local controlName = Control_GetName( newControl )

        gHandles[controlName] = newControl  

        return newControl
    end
end


---------------------------------
---------NPC Dress Up--------
---------------------------------
spawnModelNPC = function()
	local actorItem = m_gameItems[tostring(m_currActorUNID)]

	local npcData = string.gsub(actorItem.meshGLID, "\'", "\"")
	local paperDollCustoms = Events.decode(npcData)
	if not ( next(m_equippedGLIDs) ) then
		m_equippedGLIDs = paperDollCustoms["GLIDS"]
	end

	local posX, posY, posZ = KEP_DynamicObjectGetPosition(m_currActorPID)
	rX, rY, rZ = KEP_DynamicObjectGetRotation(m_currActorPID)
	
	rotDeg = Rad2Deg(rY) - 270 
	--Male = 0, Female = 1
	m_gender = MALE
	if actorItem.meshType == 2 then
		m_gender =  FEMALE
	end

	dbIndex = paperDollCustoms["dbIndex"]

	local newNPC = KEP_npcSpawn( {
		name= actorItem.behaviorParams.actorName, 
		netId=m_netID,
		charConfigId= m_gender, -- Gender index
		dbIndex=dbIndex, -- Actor ID
		pos={x=posX, y=posY, z=posZ},
		rotDeg=rotDeg, 
		glidAnimSpecial= 0, 
		glidsArmed= m_equippedGLIDs
	} )

	Debug.printTableError("customs", paperDollCustoms)
	local faceGLID
	local faceIndex = paperDollCustoms["faceIndex"]
	if m_gender == FEMALE then
		faceGLID = FACES_FEMALE[faceIndex]
	else
		faceGLID = FACES_MALE[faceIndex]
	end
	-- Prepare NPC for further customization
	KEP_SetActorTypeOnPlayer(newNPC, ACTOR_ID[m_gender])
	-- Face
	local skinColor = paperDollCustoms["skinColor"]
	KEP_ArmEquippableItem(newNPC, faceGLID, skinColor)
	KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, CONFIG_HEAD[3], 0, skinColor)
	-- Eyes
	local eyeColor = paperDollCustoms["eyeColor"]
	KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, FACE_EYE_LEFT, 0, eyeColor)
	KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, FACE_EYE_RIGHT, 0, eyeColor)
	-- Eyebrows
	local beardColor = paperDollCustoms["beardColor"] 
	local eyebrowIndex = paperDollCustoms["eyebrowIndex"] 
	KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, FACE_EYEBROW, eyebrowIndex, beardColor)
	-- Beard and makeup

	local faceCover = paperDollCustoms["faceCover"]
	if m_gender == FEMALE then
		KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, FACE_FACECOVER, 0, faceCover)
	else
		KEP_PlayerObjectSetEquippableConfig(newNPC, faceGLID, FACE_FACECOVER, faceCover, beardColor)
	end
	-- Hair

	local hairIndex = paperDollCustoms["hairIndex"]
	local hairColor = paperDollCustoms["hairColor"]	
	if m_gender == MALE then
		KEP_SetCharacterAltConfigOnPlayer(newNPC, 25, hairIndex, hairColor)
		KEP_SetCharacterAltConfigOnPlayer(newNPC, 26, hairIndex, hairColor)
	end
	KEP_SetCharacterAltConfigOnPlayer(newNPC, 27, hairIndex, hairColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, 28, hairIndex, hairColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, 29, hairIndex, hairColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, 30, hairIndex, hairColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, 31, hairIndex, hairColor)

	-- Body
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_FEET[3], -1, skinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_TORSO[3], -1, skinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_LEGS[3], -1, skinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_NECK[3], -1, skinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_CROTCH[3], -1, skinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_HANDS[3], -1, skinColor)
	KEP_SetCharacterAltConfigOnPlayer(newNPC, CONFIG_ARMS[3], -1, skinColor)

	local scaleXZ = paperDollCustoms["scaleXZ"]
	local scaleY = paperDollCustoms["scaleY"]
	KEP_ScaleRuntimePlayer(newNPC, (scaleXZ/ 100), (scaleY/ 100), (scaleXZ/ 100))
	

end

function vectorToDegrees(x, z)
	--Use the provided math functions and basic conversion to get the radians, then the degrees.
	local aTan = math.atan2(z, x)
	local angle = (aTan / math.pi) * 180
	
	--The angle is between -180 and 180, and because of this the angle needs to be shifted if it's less than 0
	if angle < 0 then
		angle = 360 + angle
	end

	return angle
end

formatCommaValue = function(value)
  	local formatted = value
  	while true do  
    	formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
   		if (k==0) then
      		break
   		end
 	end
 	return formatted
end