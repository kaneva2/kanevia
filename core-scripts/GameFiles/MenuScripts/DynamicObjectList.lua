--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")

g_objectCount = 0
g_objects = {}
g_listbox = nil
g_ignoreSelection = false
g_selectCount = 0

g_updateList = false
g_updateSec = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "selectEventHandler", "SelectEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "DynamicObjectChangeEventHandler", "DynamicObjectChangeEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "DynamicObjectInfoEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ItemInfoEvent", KEP.MED_PRIO )
	KEP_EventRegister( "DynamicObjectChangeEvent", KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	
	ClearGlobalItemInfo()
	
	g_listbox = Dialog_GetControl( gDialogHandle, "lstObjects" )
	ListBox_SetHTMLEnabled(g_listbox, true)

	updateList()
end

function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

function ClearGlobalItemInfo()
	g_invType = nil
	g_glid = nil
	g_name = nil
	g_price = nil
end

function selectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if g_selectCount <= 0 then
		updateSelection()
	end
	g_selectCount = g_selectCount - 1
end

function updateList()

	g_objects = {}

	local doCount = KEP_GetDynamicObjectCount()
	for i = 1, doCount do
		local objId = KEP_GetDynamicObjectPlacementIdByPos(i - 1)
		local obj = ObjectGetInfo(objId, ObjectType.DYNAMIC)
		table.insert(g_objects, obj)
	end

	local sounds = { KEP_GetSoundPlacementIdList() }
	for i = 1, #sounds do
		local objId = sounds[i]
		local obj = ObjectGetInfo(objId, ObjectType.SOUND)
		table.insert(g_objects, obj)
	end

	Log("updateList: objects="..#g_objects)

	populateList(nil)

	updateSelection()
end

function updateSelection()
	ListBox_ClearSelection(g_listbox)
	count = ObjectsSelected()
	for i = 1,count do
		local obj = ObjectSelected(i)
		result = ListBox_FindItem(g_listbox, obj.id)
		if result ~= -1 then
			g_ignoreSelection = true
			ListBox_SelectItem(g_listbox, result)
		end
	end
end

function populateList(queryString)
	ListBox_RemoveAllItems(g_listbox)
	if queryString == nil then
		for i = 1, #g_objects do
			local obj = g_objects[i]
			ListBox_AddItem(g_listbox, obj.name, obj.id)
		end
	else
		for i = 1, #g_objects do
			local obj = g_objects[i]
			local start = string.find(string.lower(obj.name), string.lower(queryString))
			if start ~= nil then
				local displayName = string.sub(obj.name, 0, start - 1) 
					.. "<font color=FFFF0000>" 
					.. string.sub(obj.name, start, start + string.len(queryString) - 1) 
					.. "</font>" 
					.. string.sub(obj.name, start + string.len(queryString), -1)
				ListBox_AddItem(g_listbox, displayName, obj.id)
			end
		end
	end
end

function getType(objId)
	for i = 1, #g_objects do
		if g_objects[i].id == objId then
			return g_objects[i].type
		end
	end
	return ObjectType.NONE
end

function lstObjects_OnListBoxSelection(listBoxHandle, bFromKeyboard, bMouseDown, sel_index)
	if g_ignoreSelection then
		g_ignoreSelection = false
		return
	end

	g_selectCount = 1
	KEP_ClearSelection()
	local index = ListBox_GetMultiSelectedItemIndex(Dialog_GetControl(gDialogHandle, "lstObjects"), -1)
	while index ~= -1 do
		local objId = ListBox_GetItemData(listBoxHandle, index)
		local objType = getType(objId)
		if objType ~= ObjectType.NONE then
			KEP_SelectObject(objType, objId)
			g_selectCount = g_selectCount + 1
		end
		index = ListBox_GetMultiSelectedItemIndex(Dialog_GetControl(gDialogHandle, "lstObjects") , index)
	end
end

function OpenPropertiesMenu()
	
	-- BUG FIX - Only If One Object Is Selected!
	if (ObjectsSelected() ~= 1) then return end

	-- Clear Properties
	ClearGlobalItemInfo()
	
	-- Get List Selected Object
	local lh = Dialog_GetListBox(gDialogHandle, "lstObjects")
	local objId = ListBox_GetItemData(lh, ListBox_GetSelectedItemIndex(lh))
	local objType = getType(objId)
	if objId == nil or objId == 0 or (objType ~= ObjectType.DYNAMIC) then return end

	-- Open Object Properties Menu
	if KEP_IsOwnerOrModerator() then
		MenuOpen("PlaceableObj.xml")
	else
		MenuOpen("RClickShop.xml")
	end

	-- Trigger DynamicObjectInfo Event (updates properties menu)
	local idEvent = KEP_EventCreate( "DynamicObjectInfoEvent" )
	KEP_EventEncodeNumber(idEvent, objId)
	KEP_EventEncodeNumber(idEvent, objType)
	KEP_EventQueue( idEvent )
end

function lstObjects_OnListBoxItemDblClick(listboxHandle, selectedIndex, selectedText)
	OpenPropertiesMenu()
end

function btnProperties_OnButtonClicked( buttonHandle )
	OpenPropertiesMenu()
end

function edtSearch_OnEditBoxChange( editBoxHandle )
	local query = EditBox_GetText( editBoxHandle )
	if (query ~= nil) and (string.len(query)>= 0) then
		populateList(query)
	end
end

function doBuy( )

	-- BUG FIX - Only If One Object Is Selected!
	if (ObjectsSelected() ~= 1) then return end

	-- Clear Properties
	ClearGlobalItemInfo()

	-- Get List Selected Object
	local lh = Dialog_GetListBox(gDialogHandle, "lstObjects")
	local objId = ListBox_GetItemData(lh, ListBox_GetSelectedItemIndex(lh))
	if objId == nil or objId == 0 then return end

	-- Present Checkout To Buy Object
	for i,v in ipairs(g_objects) do
		if v.id == objId then
			if KEP_IsWorld() then
				KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_SHOP .. IMAGE_DETAILS_SUFFIX .. v.glid )
				return
			end
			
			MenuOpen("Checkout.xml")

			g_invType = v.invType
			g_glid = v.glid
			g_name = v.name
			g_price = nil -- let webcall get real price

			local ev = KEP_EventCreate( "ItemInfoEvent" )
			KEP_EventEncodeNumber( ev, g_invType )
			KEP_EventEncodeNumber( ev, 1) -- num items
			KEP_EventEncodeNumber( ev, g_glid)
			KEP_EventEncodeNumber( ev, 1) --quantity of item
			KEP_EventEncodeString( ev, g_name )
			KEP_EventEncodeNumber( ev, -1) -- let webcall get real price
			KEP_EventQueue( ev )
			Log("doBuy(): ItemInfoEvent -> invType="..g_invType.." glid="..g_glid.." name="..g_name)

			-- Fire off a webcall to figure out the correct price and update the Checkout dialog when it returns.
			local web_address = GameGlobals.WEB_SITE_PREFIX..DYNAMIC_OBJECT_INFO_SUFFIX.."&objId="..v.id
			makeWebCall( web_address, WF.GET_DYNAMIC_OBJECT_INFO)

			break
		end
	end
end

function btnBuy_OnButtonClicked( buttonHandle )
	doBuy()
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	if filter == WF.GET_DYNAMIC_OBJECT_INFO then
		local result = KEP_EventDecodeString( event )		
		local Start, End, invType = string.find(result, "<inventory_type>(.-)</inventory_type>")
		if (g_invType == nil) then
			g_invType = tonumber(invType) or 0
		end

		if (g_glid == nil) then
			g_glid = 0
		end

		local Start, End, name = string.find(result, "<display_name>(.-)</display_name>")
		if (g_name == nil) then
			g_name = name or "nil"
		end

		local Start, End, price = string.find(result, "<web_price>(.-)</web_price>")
		if (g_price == nil) then
			g_price = tonumber(price) or 0
		end

		Log("BrowserPageReadyHandler(): ItemInfoEvent -> invType="..g_invType.." glid="..g_glid.." name="..g_name.." price="..g_price)
	
		local ev = KEP_EventCreate( "ItemInfoEvent" )
		KEP_EventEncodeNumber( ev, g_invType )
		KEP_EventEncodeNumber( ev, 1) -- num items
		KEP_EventEncodeNumber( ev, g_glid)
		KEP_EventEncodeNumber( ev, 1) --quantity of item
		KEP_EventEncodeString( ev, g_name )
		KEP_EventEncodeNumber( ev, g_price ) -- web_price
		KEP_EventQueue( ev )
	end
end

function Dialog_OnRender(dialogHandle, elapsedSec)
	if not g_updateList then return end

	-- Update List After 1 Second (give object time to get situated first)
	g_updateSec = g_updateSec + elapsedSec
	if (g_updateSec > 1.0) then
		g_updateList = false
		g_updateSec = 0
		updateList()
	end
end

function DynamicObjectChangeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	Log("DynamicObjectChangeEventHandler: Calling updateList()")
	g_updateList = true
	g_updateSec = 0
end
