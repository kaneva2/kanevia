--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Map.lua
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\Lib_Map.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")
-- dofile("Framework_GameItemEditor.lua")
dofile("Framework_InventoryHelper.lua")

local MapDefinitions = _G.MapDefinitions -- Assign the global MapDefinitions to a local variable for faster access

local MAX_SPAWN_RADIUS = 75

WorldRenderStyles = {   -- world map visual style
    FULL_TEXTURE = 0x00,
    OBJECT_COLOR = 0x01,
    EDGE_TRACING = 0x02,
    EDGE_WHITE_ON_BLACK = 0x03,
    EDGE_BLACK_ON_WHITE = 0x04,
}

WorldRenderOptions = { -- world map mapping options
    EXG_MESHES = 1,
    STATIONARY_OBJECTS = 2,
    MOVING_OBJECTS = 4,
}

COLORS = { -- frequently used colors
    WHITE = {a=255, r=255, g=255, b=255},
    BLACK = {a=255, r=0, g=0, b=0},
    GREY = {a = 255, r=90, g=90, b=90},
    OUTLINE = "#FF000000"
}

local DEFAULT_SCROLL_AMT = 10
local SQRT_TWO = math.sqrt(2)

local worldCenter = { x = 0, z = 0 }
local m_prevWorldCenter = {x = worldCenter.x, z = worldCenter.z}
local defaultWorldSize = { x = 1000, z = 1000 } -- Map snapshot size
local worldSize = { x = defaultWorldSize.x, z = defaultWorldSize.z } -- Map snapshot size (dynamic)
local mapSize = {x = 670, z = 670} -- Map display size
local mapBorderSize = { x = 150, z = 20}
local MAX_ZOOM_IN = 10
local MAX_ZOOM_OUT = 10000 -- 9794

local iconSize = {x = 40, z = 40}
local btnSize = {x = 32, z = 32}
local btnPadding = 10
local btnSpacing = 3
local compassPadding = 20
local headerPadding = 20
local legendFirstEntryPadding = 12
local legendPadding = 20
local legendWidth = 338
local legendScaling = 1 -- Change to adjust map icon size

local mapFont = "Verdana"
local headerFontSize = 24
local legendFontSize = 16
local compassFontSize = 14
local labelFontSize = 12

local dynTexIdWorldMap = 0
local mapStyle = WorldRenderStyles.FULL_TEXTURE
local strImgWorldMapBG = "imgWorldMapBG"
local strImgWorldMap = "imgWorldMap"
local strEditBoxWorldCenter = "edWorldCenter" -- "X", "Z"
local strEditBoxWorldSize = "edWorldSize" -- "X", "Z"
local strEditBoxMapW = "edMapW"
local strEditBoxMapH = "edMapH"
local strEditBoxMapStyle = "edMapStyle"

local m_worldName = ""
local m_mapControl = nil
local m_eventTitle = ""

local m_mapItemData = {}
local m_pan = {x=0, z=0}
local m_clickAndDrag = false
local m_clickDragPrevPos = nil

local m_gameItems = {} -- added for the game item cache, keys are UNID as a string
local m_players = {} -- Table of all (targetable) players in the zone

-- Local Function Declarations
local requestMapData -- ()

------------------
-- CORE METHODS --
------------------

function onCreate()

    -- Register events
    KEP_EventRegister( "FRAMEWORK_RETURN_COMPASS_DATA", KEP.HIGH_PRIO )
    -- Register Client event handlers
    KEP_EventRegisterHandler("zoneNavigationEventHandler", "ZoneNavigationEvent", KEP.HIGH_PRIO)
    KEP_EventRegisterHandler("updateGameClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
    KEP_EventRegisterHandler("updateGameClientFull", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)
    KEP_EventRegisterHandler("returnTrackedQuest", "FRAMEWORK_RETURN_COMPASS_DATA", KEP.HIGH_PRIO)
    -- Register Server Handlers
    Events.registerHandler("FLAG_GROUP_INFO_RETURNED", flagGroupInfoReturnedHandler)

    -- Request world name
    local ev = KEP_EventCreate("ZoneNavigationEvent")
    KEP_EventSetFilter(ev, ZONE_NAV.CURRENT_URL_REQUEST)
    KEP_EventQueue(ev)

    -- Request game item cache (for getting object properties)
    requestInventory(GAME_PID)
    -- Update Net Ids (for getting other players in the world)
    updateNetIds()
    -- Generate the map
    if MenuIsOpen("Framework_MapDataHandler.xml") then
        -- Get Map Data
        KEP_EventRegisterHandler("returnWorldMapHandler", "FRAMEWORK_WORLD_MAP_RETURNED_TO_CLIENT", KEP.HIGH_PRIO)
        requestMapData()
        -- genFakeData()
    else -- Not in a game world, so don't need map or compass data; skip straight to making the map!
        initializeMapData()
        DevMode_WorldMapGenerator()
    end
    
end

function onDestroy()
    DevMode_WorldMapCleanup()
end

function Dialog_OnMoved()
    recenterEverything()
end

recenterEverything = function()
    local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
    local screenHeight = Dialog_GetScreenHeight(gDialogHandle)

    -- Resize and reposition dialog
    Dialog_SetSize(gDialogHandle, screenWidth, screenHeight)
    Control_SetSize(Dialog_GetControl(gDialogHandle, strImgWorldMapBG), screenWidth, screenHeight)
    Control_SetSize(m_mapControl, mapSize.x, mapSize.z)
    Dialog_SetLocation(gDialogHandle, 0, 0)

    local imgWorldMap = Dialog_GetImage(gDialogHandle, strImgWorldMap)

    if dynTexIdWorldMap == 0 then
        dynTexIdWorldMap = KEP_CreateDynamicTexture(mapSize.x, mapSize.z)
    end

    if dynTexIdWorldMap == 0 then
        LogError("DevMode - World Map Generator: ERROR creating dynamic texture")
    else
        local elem = Image_GetDisplayElement(imgWorldMap)
        Element_AddDynamicTexture(elem, gDialogHandle, dynTexIdWorldMap)
        KEP_GenerateWorldMap(dynTexIdWorldMap, mapStyle, WorldRenderOptions.STATIONARY_OBJECTS+WorldRenderOptions.MOVING_OBJECTS, 0, worldCenter.x - worldSize.x / 2, -300, worldCenter.z - worldSize.z / 2, worldCenter.x + worldSize.x / 2, 300, worldCenter.z + worldSize.z / 2)
    end

    repositionMapElements()

    updateIcons()
end

--------------------
-- EVENT HANDLERS --
--------------------

function updateGameClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
    local updateItem = decompileInventoryItem(tEvent)
    m_gameItems[updateIndex] = updateItem
end

function updateGameClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    local gameItems = decompileInventory(tEvent)
    m_gameItems = gameItems
end

function returnWorldMapHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)

    initializeMapData()
    
    local strData = KEP_EventDecodeString(tEvent)
    local mapData = Events.decode(strData)

    -- Update map data from event
    if mapData then
        for i, mapObject in pairs(mapData) do
            -- Get item data from event
            local tempType = mapObject.type
            local tempPos = {x = mapObject.position.x, y = mapObject.position.y, z = mapObject.position.z} 
            local tempPID = mapObject.PID
            local tempUNID = mapObject.UNID
            local tempName = ""
            local tempRadius = 0
            local tempHeight = 0
            local tempWidth = 0
            local tempDepth = 0

            -- Update corpse name
            if tempType == MapDefinitions.LABEL_INDEX.CORPSE then
                tempName = KEP_GetLoginName().."\'s Remains"
            end

            -- Update pet location
            if tempType == MapDefinitions.LABEL_INDEX.PET then
                tempPos.x, tempPos.y, tempPos.z = KEP_DynamicObjectGetPositionAnim(tempPID)
            end

            if tempType == MapDefinitions.LABEL_INDEX.EVENT then
                --tempPos.x, tempPos.y, tempPos.z = KEP_DynamicObjectGetPositionAnim(tempPID)
                tempName = mapObject.name

            end

            -- Get item data from game item cache
            local tempItem = m_gameItems[tostring(tempUNID)] or nil

            -- If exists in the game item system
            if tempItem then
                -- if tempType == MapDefinitions.LABEL_INDEX.EVENT then
                --     -- Get name
                -- end

                -- Get radius
                if tempItem.radius then
                    tempRadius = tempItem.radius
                elseif tempItem.spawnRadius then
                    if tempItem.behaviorParams.maxSpawns ~= 1 then
                        tempRadius = math.min(tempItem.spawnRadius, MAX_SPAWN_RADIUS)
                    end
                elseif tempItem.behaviorParams then
                    if tempItem.behaviorParams.spawnRadius then
                        tempRadius = math.min(tempItem.behaviorParams.spawnRadius, MAX_SPAWN_RADIUS)
                    end
                end

                -- Get individual flag names
                if tempType == MapDefinitions.LABEL_INDEX.LANDCLAIM then

                    if tempItem.behaviorParams then
                        if tempItem.behaviorParams.height then
                            tempHeight = tempItem.behaviorParams.height
                        end

                        if tempItem.behaviorParams.width then
                            tempWidth = tempItem.behaviorParams.width
                        else
                            tempWidth = tempRadius * SQRT_TWO
                        end

                        if tempItem.behaviorParams.depth then
                            tempDepth = tempItem.behaviorParams.depth
                        else
                            tempDepth = tempRadius * SQRT_TWO
                        end
                    end
                    Events.sendEvent("REQUEST_FLAG_GROUP_INFO", {playerName = KEP_GetLoginName(), flagPID = tempPID})
                end
                -- Get actor names
               if tempItem.behaviorParams and tempItem.behaviorParams.actorName then
                    tempName = tempItem.behaviorParams.actorName
                -- Get name of spawned objects (instead of the spawner)
                elseif tempItem.behaviorParams and tempItem.behaviorParams.spawnUNID then
                    local tempItemSpawned = m_gameItems[tostring(tempItem.behaviorParams.spawnUNID)] or nil
                    if tempItemSpawned and tempItemSpawned.name then
                        tempName = tempItemSpawned.name
                    end
                -- Get name
                elseif tempItem.name then
                    tempName = tempItem.name
                end

                
            end
            -- TO DO: Remove this conditional
            if tempType ~= MapDefinitions.LABEL_INDEX.PLAYERS then
                local tempData = {iconType = tempType, name = tempName, pos = tempPos, radius = tempRadius, height = tempHeight, width = tempWidth, depth = tempDepth, PID = tempPID, UNID = tempUNID}
                table.insert(m_mapItemData, tempData)
            end
        end
    end
    
    if MenuIsOpen("Framework_QuestCompass.xml") then
        -- Get Tracked quest ID and position from compass
        KEP_EventCreateAndQueue("FRAMEWORK_GET_COMPASS_DATA")
    else -- Wait for compass data to display map, unless compass not available
        DevMode_WorldMapGenerator()
    end
end

function flagGroupInfoReturnedHandler(event)
    -- Loop through m_mapItemData
    for i=1,#m_mapItemData do
        if m_mapItemData[i].iconType == MapDefinitions.LABEL_INDEX.LANDCLAIM then
            if m_mapItemData[i].PID == event.flagInfo.PID then
                if event.flagInfo then
                    local tempName = event.flagInfo.groupName
                    if tempName and tempName ~= "" then
                        m_mapItemData[i].name = tempName
                    end
                end
            end
        end
    end
end

function zoneNavigationEventHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    if filter == ZONE_NAV.CURRENT_URL_INFO then
        local url = KEP_EventDecodeString(tEvent)
        m_worldName = parseFriendlyNameFromURL(url)
        local stc = Dialog_GetStatic(gDialogHandle, "stcLegend")
        local ctrl = Dialog_GetControl(gDialogHandle, "stcLegend")
        if stc ~= nil then
            Static_SetText(stc, m_worldName)
            local scaledWidth = Static_GetStringWidth(ctrl, m_worldName) + 2*legendFontSize
            Control_SetSize(ctrl, scaledWidth, headerFontSize)
            Control_SetVisible(stc, true)
        end
    end
end


function returnTrackedQuest(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
    local questUNID = tonumber(KEP_EventDecodeString(tEvent))
    local questName = tostring(KEP_EventDecodeString(tEvent)) or ""
    local readyForTurnIn = tostring(KEP_EventDecodeString(tEvent))
    -- Add the quest goal
    local tempX = tonumber(KEP_EventDecodeString(tEvent))
    local tempY = tonumber(KEP_EventDecodeString(tEvent))
    local tempZ = tonumber(KEP_EventDecodeString(tEvent))
    if questUNID ~= nil and tempX ~= nil and tempY ~= nil and tempZ ~= nil and readyForTurnIn == "inProgress" then
        local tempData = {iconType = MapDefinitions.LABEL_INDEX.GOAL, name = questName, pos = {x = tempX, y=tempY, z = tempZ}, radius = 0, PID = 0, UNID = questUNID}
        table.insert(m_mapItemData, tempData)
    end
    -- If quest is ready for turn-in
    if readyForTurnIn == "returning" then
        -- Update the current quest
        for i=1,#m_mapItemData do
            -- Find all quest givers
            if m_mapItemData[i].iconType == MapDefinitions.LABEL_INDEX.QUEST_GIVER then
                -- If exists in the game item cache
                if m_gameItems[tostring(m_mapItemData[i].UNID)] then
                    local tempQuestGiver = m_gameItems[tostring(m_mapItemData[i].UNID)]
                    -- If has any quests
                    if tempQuestGiver.behaviorParams and tempQuestGiver.behaviorParams.quests then
                        -- Iterate through quests
                        for questIndex,tempQuestUNID in pairs(tempQuestGiver.behaviorParams.quests) do
                            -- Quest giver has the active quest
                            if tempQuestUNID == questUNID then
                                -- Update quest giver type
                                m_mapItemData[i].iconType = MapDefinitions.LABEL_INDEX.CURRENT_QUEST_GIVER
                            end
                        end

                    end
                end
            end
        end
    end
    -- Display Map
    DevMode_WorldMapGenerator()
end

------------------
-- GUI HANDLERS --
------------------

function btnClickAndDrag_OnLButtonDown(buttonHandle, x, y)
    -- TO DO: Figure out why click and drag loses focus when the mouse moves over another overlapping element (e.g. map icons)
    -- Dialog_MoveControlToFront(gDialogHandle, gHandles["btnClickAndDrag"])
    -- Control_SetFocus( gHandles["btnClickAndDrag"] )
    clickAndDragBegin(x, y)
end

function btnClickAndDrag_OnLButtonUp(controlHandle, x, y)
    clickAndDragEnd()
    -- Dialog_MoveControlToBack(gDialogHandle, gHandles["btnClickAndDrag"])
end

function imgWorldMapBG_OnMouseEnter(controlHandle)
    if m_clickAndDrag then
        clickAndDragEnd()
        -- Dialog_MoveControlToBack(gDialogHandle, gHandles["btnClickAndDrag"])
    end
end

function imgWorldMapBG_OnMouseLeave(controlHandle)
    clickAndDragEnd()
    -- Dialog_MoveControlToBack(gDialogHandle, gHandles["btnClickAndDrag"])
end

function Dialog_OnMouseMove(dialogHandle, x, y)
    -- If scrolling, then update list
    if m_clickAndDrag then
        local scrollMod = {x=0, y=0}
        
        scrollMod.x = (m_clickDragPrevPos.x - x) * 1
        scrollMod.x = round(scrollMod.x)
        
        scrollMod.y = (m_clickDragPrevPos.y - y) * 1
        scrollMod.y = round(scrollMod.y)

        -- Swap x and y for panning the image because of 90 degree rotation
        updatePan(scrollMod.y, scrollMod.x)

        m_clickDragPrevPos = {x=x, y=y}
    end
end

----------------------
-- GLOBAL FUNCTIONS --
----------------------

function updatePan(x, z)
    m_pan.x = m_pan.x - getPanDistance(x,z).x
    m_pan.z = m_pan.z - getPanDistance(x,z).z -- Swap the sign because of 90 degree rotation
    worldCenter = { x = m_pan.x, z = m_pan.z }
    shiftMap()
    updateIcons()
end

function DevMode_WorldMapGenerator()
    Log("DevMode - World Map Generator")
    createAndPositionMapElements()
    worldCenter = { x = 0, z = 0 }
    worldSize.x = defaultWorldSize.x
    worldSize.z = defaultWorldSize.z
    -- btnUpdateMap_OnButtonClicked()
    btnCenterMap_OnButtonClicked()
end

function DevMode_WorldMapCleanup()
    Log("DevMode - World Map Cleanup")
    if dynTexIdWorldMap ~= 0 then
        KEP_DestroyDynamicTexture(dynTexIdWorldMap)
        dynTexIdWorldMap = 0
    end
end

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

---------------------
-- LOCAL FUNCTIONS --
---------------------

createAndPositionMapElements = function()
    local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
    local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
    -- KEP_GetClientRectSize()

    addImage(strImgWorldMapBG, rect(0, 0, screenWidth, screenHeight), color(200, 255, 255, 255), "ZoneLoading.tga", rect(0, 0, 1, 1))

    -- Map image elements
    mapBorderSize.z = screenHeight/2 - mapSize.z/2
    mapBorderSize.x = screenWidth/2 - mapSize.x/2 - legendWidth/2
    -- Grey outline
    addImage("imgWorldMapOutline", rect(mapBorderSize.x - 1, mapBorderSize.z - 1, mapSize.x + 2, mapSize.z + 2), COLORS.GREY, "white.tga", rect(0, 0, 1, 1))
    -- Black map BG
    addImage("imgWorldMapBlack", rect(mapBorderSize.x, mapBorderSize.z, mapSize.x, mapSize.z), COLORS.BLACK, "white.tga", rect(0, 0, 1, 1))
    -- The map itself
    addImage(strImgWorldMap, rect(mapBorderSize.x, mapBorderSize.z, mapSize.x, mapSize.z), COLORS.WHITE, "white.tga", rect(0, 0, -1, -1))
    -- Invisible click and drag button over the map
    addButton("btnClickAndDrag", " ", rect(mapBorderSize.x, mapBorderSize.z, mapSize.x, mapSize.z), "invisible.tga", rect(0, 0, 1, 1))
    -- Set up a clipping rectangle for the map
    m_mapControl = Dialog_GetControl(gDialogHandle, strImgWorldMap)
    local containerX, containerY = Control_GetLocationX(m_mapControl), Control_GetLocationY(m_mapControl)
    local containerW, containerH = Control_GetWidth(m_mapControl), Control_GetHeight(m_mapControl)
    Control_SetClipRect(m_mapControl, containerX, containerY, containerX+containerW, containerY+containerH)

    -- local tempBtn = Dialog_GetControl(gDialogHandle, "btnClickAndDrag")
    -- Control_SetClipRect(tempBtn, containerX, containerY, containerX+containerW, containerY+containerH)

    -- Legend elements
    local iconCount = tableCount(MapDefinitions.LEGEND_LABELS) - 1
    local skipOffset = 0
    for i=iconCount,0,-1 do
        if iconTypeExists(i) == true then
            -- Special positioning rules for the first entry in the legend
            local tempPadding = 0
            if i==0 then
                tempPadding = legendFirstEntryPadding
            end
            -- Add the legend icon and label
            addImage("imgLegendIcon"..tostring(i), rect(mapBorderSize.x + mapSize.x + legendPadding, mapBorderSize.z + mapSize.z - ((iconCount-i+1-skipOffset)*iconSize.z)*legendScaling - tempPadding, iconSize.x*legendScaling, iconSize.z*legendScaling), COLORS.WHITE, "map_menuElements.tga", getLegendTexture(i, iconSize))
            addLabel("stcLegendLabel"..tostring(i), tostring(MapDefinitions.LEGEND_LABELS[i].label), rect(mapBorderSize.x + mapSize.x + iconSize.x*legendScaling + legendPadding + 10, mapBorderSize.z + mapSize.z - ((iconCount-i+1-skipOffset)*iconSize.z)*legendScaling, iconSize.x*legendScaling, iconSize.z*legendScaling), mapFont, legendFontSize, COLORS.WHITE)
        else
            skipOffset = skipOffset + 1
        end
    end

    -- Legend label
    local legendLabel = "Map Legend"
    if m_worldName ~= "" then
        legendLabel = m_worldName
    end
    addLabel("stcLegend", legendLabel, rect(mapBorderSize.x + mapSize.x + legendPadding, mapBorderSize.z + mapSize.z - ((iconCount+1-skipOffset)*iconSize.z)*legendScaling - headerPadding - headerFontSize, headerFontSize*string.len(m_worldName), headerFontSize), mapFont, headerFontSize, COLORS.WHITE)
    local tempControl = Dialog_GetControl(gDialogHandle, "stcLegend")
    local scaledWidth = Static_GetStringWidth(tempControl, legendLabel) + 2*legendFontSize
    Control_SetSize(tempControl, scaledWidth, headerFontSize)
    -- Hide until name is ready
    if m_worldName == "" then
        Control_SetVisible(tempControl, false)
    end

    -- Map icons
    iconCount = tableCount(m_mapItemData)

    -- First pass for the icon radius circles
    for i=iconCount,1,-1 do -- reverse order so most important is on top
        if m_mapItemData[i].pos.y >= -0.01 then -- Don't display icons beneath the ground
            local tempPos = convertCoord(m_mapItemData[i].pos, worldSize, mapSize)
            local tempType = m_mapItemData[i].iconType
            local tempX = tempPos.x - ((iconSize.x*legendScaling)/2) + mapBorderSize.x
            local tempZ = tempPos.z - ((iconSize.z*legendScaling)/2) + mapBorderSize.z
            
            -- Draw the area circle
            if tempType == MapDefinitions.LABEL_INDEX.MONSTER or tempType == MapDefinitions.LABEL_INDEX.HARVESTABLE then
                local tempRad = m_mapItemData[i].radius
                if tempRad > 0 then
                    tempX = tempPos.x - tempRad + mapBorderSize.x
                    tempZ = tempPos.z - tempRad + mapBorderSize.z
                    local tempColor = MapDefinitions.LEGEND_COLORS[tempType].color
                    tempColor.a = 107 -- Set transparency
                    addImage("imgMapIconRadius"..tostring(i), rect(tempX, tempZ, tempRad*2, tempRad*2), tempColor, "Framework_HudElements.tga", rect(467, 213, 40, 40))
                    -- Set up clipping rectangle at the map display bounds
                    local tempControl = Dialog_GetControl(gDialogHandle, "imgMapIconRadius"..tostring(i))
                    local containerX = Control_GetLocationX(m_mapControl)
                    local containerY = (Control_GetLocationY(m_mapControl))
                    local containerW = Control_GetWidth(m_mapControl)
                    local containerH = (Control_GetHeight(m_mapControl))
                    Control_SetClipRect(tempControl, containerX, containerY, containerX+containerW, containerY+containerH)
                end
            -- Draw the area rectangle
            elseif tempType == MapDefinitions.LABEL_INDEX.LANDCLAIM then
                local tempDepth = m_mapItemData[i].depth
                local tempWidth = m_mapItemData[i].width
                local tempHeight = m_mapItemData[i].height
                if tempWidth > 0 and tempDepth > 0 and tempHeight > 0 then
                    tempX = tempPos.x - tempDepth/2 + mapBorderSize.x
                    tempZ = tempPos.z - tempWidth/2 + mapBorderSize.z

                    local tempColor = MapDefinitions.LEGEND_COLORS[tempType].color
                    tempColor.a = 107 -- Set transparency
                    addImage("imgMapIconRadius"..tostring(i), rect(tempX, tempZ, tempDepth, tempWidth), tempColor, "map_menuElements.tga", rect(160, 120, 40, 40))
                    -- Set up clipping rectangle at the map display bounds
                    local tempControl = Dialog_GetControl(gDialogHandle, "imgMapIconRadius"..tostring(i))
                    local containerX = Control_GetLocationX(m_mapControl)
                    local containerY = (Control_GetLocationY(m_mapControl))
                    local containerW = Control_GetWidth(m_mapControl)
                    local containerH = (Control_GetHeight(m_mapControl))
                    Control_SetClipRect(tempControl, containerX, containerY, containerX+containerW, containerY+containerH)
                end
            end
        end
    end

    -- Second pass for the icons (draw the image ABOVE the radius circle)
    for i=iconCount,1,-1 do -- reverse order so most important is on top
        if m_mapItemData[i].pos.y >= -0.01 or m_mapItemData[i].iconType == MapDefinitions.LABEL_INDEX.YOU or m_mapItemData[i].iconType == MapDefinitions.LABEL_INDEX.PLAYERS then -- Don't display icons beneath the ground, but always show players
            local tempPos = convertCoord(m_mapItemData[i].pos, worldSize, mapSize)
            local tempType = m_mapItemData[i].iconType
            local tempRad = m_mapItemData[i].radius
            local tempX = tempPos.x - ((iconSize.x*legendScaling)/2) + mapBorderSize.x
            local tempZ = tempPos.z - ((iconSize.z*legendScaling)/2) + mapBorderSize.z

            addImage("imgMapIcon"..tostring(i), rect(tempX, tempZ, iconSize.x*legendScaling, iconSize.z*legendScaling), COLORS.WHITE, "map_menuElements.tga", getLegendTexture(tempType, iconSize))
            -- Set up clipping rectangle at the map display bounds
            local tempControl = Dialog_GetControl(gDialogHandle, "imgMapIcon"..tostring(i))
            local containerX = Control_GetLocationX(m_mapControl)
            local containerY = (Control_GetLocationY(m_mapControl))
            local containerW = Control_GetWidth(m_mapControl)
            local containerH = (Control_GetHeight(m_mapControl))
            Control_SetClipRect(tempControl, containerX, containerY, containerX+containerW, containerY+containerH)


            -- Mouseover tooltip
            -- On icon mouseEnter
            _G["imgMapIcon"..tostring(i).."_OnMouseEnter"] = function(btnHandle)
                setHover(tonumber(i), true)
            end
            -- On icon mouseLeave
            _G["imgMapIcon"..tostring(i).."_OnMouseLeave"] = function(btnHandle)
                setHover(tonumber(i), false)
            end
            -- clickAndDrag handlers (because icon displays over clickAndDrag element)
            _G["imgMapIcon"..tostring(i).."_OnLButtonDown"] = function(buttonHandle, x, y)
                clickAndDragBegin(x, y)
            end
            _G["imgMapIcon"..tostring(i).."_OnLButtonUp"] = function(buttonHandle, x, y)
                clickAndDragEnd()
            end

            -- clickAndDrag handlers (because radius displays over clickAndDrag element)
            _G["imgMapIconRadius"..tostring(i).."_OnLButtonDown"] = function(buttonHandle, x, y)
                clickAndDragBegin(x, y)
            end
            _G["imgMapIconRadius"..tostring(i).."_OnLButtonUp"] = function(buttonHandle, x, y)
                clickAndDragEnd()
            end
        end
    end

    -- Map compass
    addImage("imgCompass", rect(mapBorderSize.x + mapSize.x - iconSize.x - compassPadding, mapBorderSize.z + mapSize.z - iconSize.z - compassPadding, iconSize.x, iconSize.z), COLORS.WHITE, "map_menuElements.tga", rect(0, iconSize.z*2, iconSize.x, iconSize.z))
    addLabel("stcNorth", "N", rect(mapBorderSize.x + mapSize.x - iconSize.x/2 - compassFontSize/2+2 - compassPadding, mapBorderSize.z + mapSize.z - iconSize.z - compassFontSize - compassPadding, compassFontSize, compassFontSize), mapFont, compassFontSize, COLORS.WHITE)
    generateOutlinedString("North", 1, COLORS.OUTLINE)
    addLabel("stcSouth", "S", rect(mapBorderSize.x + mapSize.x - iconSize.x/2 - compassFontSize/2+2 - compassPadding, mapBorderSize.z + mapSize.z - compassPadding, compassFontSize, compassFontSize), mapFont, compassFontSize, COLORS.WHITE)
    generateOutlinedString("South", 1, COLORS.OUTLINE)
    addLabel("stcEast", "E", rect(mapBorderSize.x + mapSize.x - compassPadding, mapBorderSize.z + mapSize.z - iconSize.z/2 - compassFontSize/2 - compassPadding, compassFontSize, compassFontSize), mapFont, compassFontSize, COLORS.WHITE)
    generateOutlinedString("East", 1, COLORS.OUTLINE)
    addLabel("stcWest", "W", rect(mapBorderSize.x + mapSize.x - iconSize.x - compassFontSize - compassPadding, mapBorderSize.z + mapSize.z - iconSize.z/2 - compassFontSize/2 - compassPadding, compassFontSize, compassFontSize), mapFont, compassFontSize, COLORS.WHITE)
    generateOutlinedString("West", 1, COLORS.OUTLINE)

    -- clickAndDrag handlers (because compass displays over clickAndDrag element)
    local tempCompassElements = {"imgCompass", "stcNorth", "stcNorthBG1", "stcNorthBG2", "stcNorthBG3", "stcNorthBG4", "stcSouth", "stcSouthBG1", "stcSouthBG2", "stcSouthBG3", "stcSouthBG4", "stcEast", "stcEastBG1", "stcEastBG2", "stcEastBG3", "stcEastBG4", "stcWest", "stcWestBG1", "stcWestBG2", "stcWestBG3", "stcWestBG4"}
    for i=1,#tempCompassElements do
        local baseName = tempCompassElements[i]
        _G[baseName.."_OnLButtonDown"] = function(buttonHandle, x, y)
            clickAndDragBegin(x, y)
        end
        _G[baseName.."_OnLButtonUp"] = function(buttonHandle, x, y)
            clickAndDragEnd()
        end
    end

    -- Navigation buttons (from top to bottom)
    addButton("btnCenterMap", " ", rect(mapBorderSize.x + btnPadding, mapBorderSize.z + mapSize.z - 2*btnSpacing - btnPadding - 3*btnSize.z, btnSize.x, btnSize.z), "map_menuElements.tga", rect(3*iconSize.x, 2*iconSize.z, btnSize.x, btnSize.z)) -- Center
    addButton("btnZoomIn", " ", rect(mapBorderSize.x + btnPadding, mapBorderSize.z + mapSize.z - btnSpacing - btnPadding - 2*btnSize.z, btnSize.x, btnSize.z), "map_menuElements.tga", rect(5*iconSize.x, 2*iconSize.z, btnSize.x, btnSize.z)) -- Zoom in
    addButton("btnZoomOut", " ", rect(mapBorderSize.x + btnPadding, mapBorderSize.z + mapSize.z - btnPadding - btnSize.z, btnSize.x, btnSize.z), "map_menuElements.tga", rect(iconSize.x, 2*iconSize.z, btnSize.x, btnSize.z)) -- Zoom out

    -- Close button
    local tempX = screenWidth - iconSize.x - 62
    local tempZ = labelFontSize/2 + 2
    addLabel("stcCloseMap", "Close Map", rect(tempX, tempZ, iconSize.x, iconSize.z), mapFont, labelFontSize, COLORS.WHITE)
    addButton("btnCloseMapText", " ", rect(tempX, tempZ, iconSize.x*2, iconSize.z), "invisible.tga", rect(0, 0, 1, 1)) -- Invisible close button over text
    _G["btnCloseMapText_OnButtonClicked"] = function(buttonHandle)
       btnClose_OnButtonClicked()
    end
    addButton("btnClose", " ", rect(screenWidth - iconSize.x - 6, 10, iconSize.x, iconSize.z), "map_menuElements.tga", rect(2*iconSize.x, 3*iconSize.z, iconSize.x, iconSize.z)) -- Close button (x)
    -- Add mouseover texture to close button
    local btn = Dialog_GetButton(gDialogHandle, "btnClose")
    local texRect = rect(3*iconSize.x, 3*iconSize.z, iconSize.x, iconSize.z)
    local elem = Button_GetMouseOverDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, "map_menuElements.tga")
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)

    -- Tooltip box
    Dialog_MoveControlToFront(gDialogHandle, gHandles["imgTooltipBG"])
    Dialog_MoveControlToFront(gDialogHandle, gHandles["stcTooltipName"])
    Dialog_MoveControlToFront(gDialogHandle, gHandles["stcTooltipType"])
    -- Tooltip icon ?
    Control_SetVisible(gHandles["imgTooltipBG"], false)
    Control_SetVisible(gHandles["stcTooltipName"], false)
    Control_SetVisible(gHandles["stcTooltipType"], false)
    -- Make sure the clickable area is on top
    -- local tempControl = Dialog_GetControl(gDialogHandle, "btnClickAndDrag")
    -- Dialog_MoveControlToFront(gDialogHandle, tempControl)
end

-- Recenters all map elements on the screen
repositionMapElements = function()
    local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
    local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
    local ctrl = Dialog_GetControl(gDialogHandle, "imgWorldMapOutline")
    mapBorderSize.z = screenHeight/2 - mapSize.z/2
    mapBorderSize.x = screenWidth/2 - mapSize.x/2 - legendWidth/2
    Control_SetLocation(ctrl, mapBorderSize.x - 1, mapBorderSize.z - 1)
    ctrl = Dialog_GetControl(gDialogHandle, "imgWorldMapBlack")
    Control_SetLocation(ctrl, mapBorderSize.x, mapBorderSize.z)
    ctrl = Dialog_GetControl(gDialogHandle, "btnClickAndDrag")
    Control_SetLocation(ctrl, mapBorderSize.x, mapBorderSize.z)
    
    -- Update map image and clipping rectangle
    Control_SetLocation(m_mapControl, mapBorderSize.x, mapBorderSize.z)
    local containerX, containerY = Control_GetLocationX(m_mapControl), Control_GetLocationY(m_mapControl)
    local containerW, containerH = Control_GetWidth(m_mapControl), Control_GetHeight(m_mapControl)
    Control_SetClipRect(m_mapControl, containerX, containerY, containerX+containerW, containerY+containerH)

     -- Legend elements
    local iconCount = tableCount(MapDefinitions.LEGEND_LABELS) - 1
    local skipOffset = 0
    for i=iconCount,0,-1 do
        if iconTypeExists(i) == true then
            -- Special positioning rules for the first entry in the legend
            local tempPadding = 0
            if i==0 then
                tempPadding = legendFirstEntryPadding
            end
            ctrl = Dialog_GetControl(gDialogHandle, "imgLegendIcon"..tostring(i))
            Control_SetLocation(ctrl, mapBorderSize.x + mapSize.x + legendPadding, mapBorderSize.z + mapSize.z - ((iconCount-i+1-skipOffset)*iconSize.z)*legendScaling - tempPadding)
            ctrl = Dialog_GetControl(gDialogHandle, "stcLegendLabel"..tostring(i))
            Control_SetLocation(ctrl, mapBorderSize.x + mapSize.x + iconSize.x*legendScaling + legendPadding + 10, mapBorderSize.z + mapSize.z - ((iconCount-i+1-skipOffset)*iconSize.z)*legendScaling)
        else
            skipOffset = skipOffset + 1
        end
    end
    ctrl = Dialog_GetControl(gDialogHandle, "stcLegend")
    Control_SetLocation(ctrl, mapBorderSize.x + mapSize.x + legendPadding, mapBorderSize.z + mapSize.z - ((iconCount+1-skipOffset)*iconSize.z)*legendScaling - headerPadding - headerFontSize)

    -- Map icons
    iconCount = tableCount(m_mapItemData)

    -- First pass for the icon radius circles
    for i=iconCount,1,-1 do -- reverse order so most important is on top
        if m_mapItemData[i].pos.y >= -0.01 or m_mapItemData[i].iconType == MapDefinitions.LABEL_INDEX.YOU or m_mapItemData[i].iconType == MapDefinitions.LABEL_INDEX.YOU then -- Don't display icons beneath the ground, but always show players
            local tempPos = convertCoord(m_mapItemData[i].pos, worldSize, mapSize)
            local tempType = m_mapItemData[i].iconType
            local tempX = tempPos.x - ((iconSize.x*legendScaling)/2) + mapBorderSize.x
            local tempZ = tempPos.z - ((iconSize.z*legendScaling)/2) + mapBorderSize.z
            
            -- Map icon
            ctrl = Dialog_GetControl(gDialogHandle, "imgMapIcon"..tostring(i))
            Control_SetLocation(ctrl, tempX, tempZ)
            Control_SetClipRect(ctrl, containerX, containerY, containerX+containerW, containerY+containerH)

            -- Draw the area circle
            if tempType == MapDefinitions.LABEL_INDEX.MONSTER or tempType == MapDefinitions.LABEL_INDEX.HARVESTABLE then
                local tempRad = m_mapItemData[i].radius
                if tempRad > 0 then
                    tempX = tempPos.x - tempRad + mapBorderSize.x
                    tempZ = tempPos.z - tempRad + mapBorderSize.z
                    ctrl = Dialog_GetControl(gDialogHandle, "imgMapIconRadius"..tostring(i))

                    Control_SetLocation(ctrl, tempX, tempZ)
                    Control_SetClipRect(ctrl, containerX, containerY, containerX+containerW, containerY+containerH)
                end
            -- Draw the area rectangle
            elseif tempType == MapDefinitions.LABEL_INDEX.LANDCLAIM then
                local tempDepth = m_mapItemData[i].depth
                local tempWidth = m_mapItemData[i].width
                local tempHeight = m_mapItemData[i].height
                if tempWidth > 0 and tempDepth > 0 and tempHeight > 0 then
                    tempX = tempPos.x - tempDepth/2 + mapBorderSize.x
                    tempZ = tempPos.z - tempWidth/2 + mapBorderSize.z
                    ctrl = Dialog_GetControl(gDialogHandle, "imgMapIconRadius"..tostring(i))

                    Control_SetLocation(ctrl, tempX, tempZ)
                    Control_SetClipRect(ctrl, containerX, containerY, containerX+containerW, containerY+containerH)
                end
            end
        end
    end

    -- Compass
    ctrl = Dialog_GetControl(gDialogHandle, "imgCompass")
    Control_SetLocation(ctrl, mapBorderSize.x + mapSize.x - iconSize.x - compassPadding, mapBorderSize.z + mapSize.z - iconSize.z - compassPadding)
    -- North
    ctrl = Dialog_GetControl(gDialogHandle, "stcNorth")
    local tempX = mapBorderSize.x + mapSize.x - iconSize.x/2 - compassFontSize/2+2 - compassPadding
    local tempZ = mapBorderSize.z + mapSize.z - iconSize.z - compassFontSize - compassPadding
    Control_SetLocation(ctrl, tempX, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcNorthBG1")
    Control_SetLocation(ctrl, tempX + 1, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcNorthBG2")
    Control_SetLocation(ctrl, tempX - 1, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcNorthBG3")
    Control_SetLocation(ctrl, tempX, tempZ + 1)
    ctrl = Dialog_GetControl(gDialogHandle, "stcNorthBG4")
    Control_SetLocation(ctrl, tempX, tempZ - 1)
    -- South
    ctrl = Dialog_GetControl(gDialogHandle, "stcSouth")
    tempX = mapBorderSize.x + mapSize.x - iconSize.x/2 - compassFontSize/2+2 - compassPadding
    tempZ = mapBorderSize.z + mapSize.z - compassPadding
    Control_SetLocation(ctrl, tempX, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcSouthBG1")
    Control_SetLocation(ctrl, tempX + 1, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcSouthBG2")
    Control_SetLocation(ctrl, tempX - 1, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcSouthBG3")
    Control_SetLocation(ctrl, tempX, tempZ + 1)
    ctrl = Dialog_GetControl(gDialogHandle, "stcSouthBG4")
    Control_SetLocation(ctrl, tempX, tempZ - 1)
    -- East
    ctrl = Dialog_GetControl(gDialogHandle, "stcEast")
    tempX = mapBorderSize.x + mapSize.x - compassPadding
    tempZ = mapBorderSize.z + mapSize.z - iconSize.z/2 - compassFontSize/2 - compassPadding
    Control_SetLocation(ctrl, tempX, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcEastBG1")
    Control_SetLocation(ctrl, tempX + 1, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcEastBG2")
    Control_SetLocation(ctrl, tempX - 1, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcEastBG3")
    Control_SetLocation(ctrl, tempX, tempZ + 1)
    ctrl = Dialog_GetControl(gDialogHandle, "stcEastBG4")
    Control_SetLocation(ctrl, tempX, tempZ - 1)
    -- West
    ctrl = Dialog_GetControl(gDialogHandle, "stcWest")
    tempX = mapBorderSize.x + mapSize.x - iconSize.x - compassFontSize - compassPadding
    tempZ = mapBorderSize.z + mapSize.z - iconSize.z/2 - compassFontSize/2 - compassPadding
    Control_SetLocation(ctrl, tempX, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcWestBG1")
    Control_SetLocation(ctrl, tempX + 1, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcWestBG2")
    Control_SetLocation(ctrl, tempX - 1, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "stcWestBG3")
    Control_SetLocation(ctrl, tempX, tempZ + 1)
    ctrl = Dialog_GetControl(gDialogHandle, "stcWestBG4")
    Control_SetLocation(ctrl, tempX, tempZ - 1)

    -- Navigation buttons
    tempX = mapBorderSize.x + btnPadding
    ctrl = Dialog_GetControl(gDialogHandle, "btnCenterMap")
    tempZ = mapBorderSize.z + mapSize.z - 2*btnSpacing - btnPadding - 3*btnSize.z
    Control_SetLocation(ctrl, tempX, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "btnZoomIn")
    tempZ = mapBorderSize.z + mapSize.z - btnSpacing - btnPadding - 2*btnSize.z
    Control_SetLocation(ctrl, tempX, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "btnZoomOut")
    tempZ = mapBorderSize.z + mapSize.z - btnPadding - btnSize.z
    Control_SetLocation(ctrl, tempX, tempZ)

    -- Close button
    ctrl = Dialog_GetControl(gDialogHandle, "stcCloseMap")
    tempX = screenWidth - iconSize.x - 62
    tempZ = labelFontSize/2 + 2
    Control_SetLocation(ctrl, tempX, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "btnCloseMapText")
    Control_SetLocation(ctrl, tempX, tempZ)
    ctrl = Dialog_GetControl(gDialogHandle, "btnClose")
    tempX = screenWidth - iconSize.x - 6
    tempZ = 10
    Control_SetLocation(ctrl, tempX, tempZ)
end

-- Sets the hover state for an icon
setHover = function(index, hovering)
    local tempName = m_mapItemData[index].name
    local tempType = tonumber(m_mapItemData[index].iconType)
    local tempTypeName = MapDefinitions.LEGEND_LABELS[tempType].label
    if hovering then
        -- Update tooltip text
        Static_SetText(gHandles["stcTooltipName"], tempName)
        local tempColor = MapDefinitions.LEGEND_COLORS[tempType].color
        BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(gHandles["stcTooltipName"])), 0, tempColor)
        Static_SetText(gHandles["stcTooltipType"], tempTypeName)
        -- Resize the tooltip box to fit the longest string
        local tempImg = Dialog_GetControl(gDialogHandle, "imgMapIcon"..tostring(index))
        local tempNameW = Static_GetStringWidth(gHandles["stcTooltipName"], tempName)
        local tempTypeW = Static_GetStringWidth(gHandles["stcTooltipType"], tempTypeName)
        local tempW = math.max(tempNameW, tempTypeW) + 16
        local tempH = Control_GetHeight(gHandles["imgTooltipBG"])
        Control_SetSize(gHandles["imgTooltipBG"], tempW, tempH)
        -- Anchor the tooltip box to the right of the icon
        local tempX = Control_GetLocationX(tempImg) + (iconSize.x*legendScaling)
        local tempY = Control_GetLocationY(tempImg) + (iconSize.z*legendScaling)/2 - tempH/2
        Control_SetLocation(gHandles["imgTooltipBG"], tempX, tempY)
        Control_SetLocation(gHandles["stcTooltipName"], tempX + 8, tempY + 8)
        Control_SetLocation(gHandles["stcTooltipType"], tempX + 8, tempY + 32)   
        -- Show tooltip box
        Control_SetVisible(gHandles["imgTooltipBG"], true)
        Control_SetVisible(gHandles["stcTooltipName"], true)
        Control_SetVisible(gHandles["stcTooltipType"], true)
    else
        -- Hide tooltip box
        Control_SetVisible(gHandles["imgTooltipBG"], false)
        Control_SetVisible(gHandles["stcTooltipName"], false)
        Control_SetVisible(gHandles["stcTooltipType"], false)    
    end
end

updateIcons = function()
     -- Get # of icons to iterate (improve this to use "for x in pairs")
    local iconCount = tableCount(m_mapItemData)
    for i=iconCount,1,-1 do
        if m_mapItemData[i].pos.y >= -0.01 or m_mapItemData[i].iconType == MapDefinitions.LABEL_INDEX.YOU or m_mapItemData[i].iconType == MapDefinitions.LABEL_INDEX.PLAYERS then -- Don't display icons beneath the ground, but always show players
            -- Get the control
            local tempImg = Dialog_GetControl(gDialogHandle, "imgMapIcon"..tostring(i))
            -- Get the base position (scaled to fit)
            local tempPos = convertCoord(m_mapItemData[i].pos, worldSize, mapSize)
            -- Set the position (account for icon size and map position)
            local tempX = tempPos.x - ((iconSize.x*legendScaling)/2) + mapBorderSize.x-- + worldCenter.x-- + m_pan.x
            local tempZ = tempPos.z - ((iconSize.z*legendScaling)/2) + mapBorderSize.z-- - worldCenter.z-- + m_pan.z
            Control_SetLocation(tempImg, tempX, tempZ)
            -- Update the radius (if relevant)
            local tempType = m_mapItemData[i].iconType
            -- Draw the radius circle (if relevant)
            if tempType == MapDefinitions.LABEL_INDEX.MONSTER or tempType == MapDefinitions.LABEL_INDEX.LANDCLAIM or tempType == MapDefinitions.LABEL_INDEX.HARVESTABLE then
                tempImg = Dialog_GetControl(gDialogHandle, "imgMapIconRadius"..tostring(i))
                if tempImg ~= nil then -- Some harvestables don't have a radius circle
                    -- Radius CIRCLE
                    if tempType == MapDefinitions.LABEL_INDEX.MONSTER or tempType == MapDefinitions.LABEL_INDEX.HARVESTABLE then
                        local tempRad = m_mapItemData[i].radius * (defaultWorldSize.x / worldSize.x)
                        if tempRad >= 0 then
                            tempX = tempPos.x - tempRad + mapBorderSize.x
                            tempZ = tempPos.z - tempRad + mapBorderSize.z
                            Control_SetLocation(tempImg, tempX, tempZ)
                            Control_SetSize(tempImg, tempRad*2, tempRad*2)
                        end
                    -- Radius RECTANGLE
                    elseif tempType == MapDefinitions.LABEL_INDEX.LANDCLAIM then
                        -- m_mapItemData[i].depth * (defaultWorldSize.x / worldSize.x)
                        local tempDepth = m_mapItemData[i].depth * (defaultWorldSize.x / worldSize.x) * (1/SQRT_TWO)
                        local tempWidth = m_mapItemData[i].width * (defaultWorldSize.z / worldSize.z) * (1/SQRT_TWO)
                        local tempHeight = m_mapItemData[i].height
                        if tempWidth > 0 and tempDepth > 0 and tempHeight > 0 then
                            tempX = tempPos.x - tempDepth/2 + mapBorderSize.x
                            tempZ = tempPos.z - tempWidth/2 + mapBorderSize.z
                            Control_SetLocation(tempImg, tempX, tempZ)
                            Control_SetSize(tempImg, tempDepth, tempWidth)
                        end
                    end
                end
            end
        end
    end
end

getLegendTexture = function(inputType, iconSize)
    local tempRect
    local rowCount = 6 -- Texture sheet rows of 6 (zero-indexed)
    if inputType < rowCount then
        tempRect = rect(inputType*iconSize.x, 0, iconSize.x, iconSize.z)
    elseif inputType >= rowCount and inputType < 2*rowCount then
        tempRect = rect((inputType-rowCount)*iconSize.x, iconSize.z, iconSize.x, iconSize.z)
    else
        tempRect = rect((inputType-rowCount-5)*iconSize.x, 3*iconSize.z, iconSize.x, iconSize.z)
    end

    return tempRect
end

updateMap = function()
    Log("DevMode - Update World Map")

    local screenWidth = Dialog_GetScreenWidth(gDialogHandle)
    local screenHeight = Dialog_GetScreenHeight(gDialogHandle)

    -- Resize and reposition dialog
    Dialog_SetSize(gDialogHandle, screenWidth, screenHeight)
    Control_SetSize(Dialog_GetControl(gDialogHandle, strImgWorldMapBG), screenWidth, screenHeight)
    Control_SetSize(m_mapControl, mapSize.x, mapSize.z)
    Dialog_SetLocation(gDialogHandle, 0, 0)

    local imgWorldMap = Dialog_GetImage(gDialogHandle, strImgWorldMap)

    if dynTexIdWorldMap == 0 then
        dynTexIdWorldMap = KEP_CreateDynamicTexture(mapSize.x, mapSize.z)
    end

    if dynTexIdWorldMap == 0 then
        LogError("DevMode - World Map Generator: ERROR creating dynamic texture")
    else
        local elem = Image_GetDisplayElement(imgWorldMap)
        Element_AddDynamicTexture(elem, gDialogHandle, dynTexIdWorldMap)
        KEP_GenerateWorldMap(dynTexIdWorldMap, mapStyle, WorldRenderOptions.STATIONARY_OBJECTS+WorldRenderOptions.MOVING_OBJECTS, 0, worldCenter.x - worldSize.x / 2, -300, worldCenter.z - worldSize.z / 2, worldCenter.x + worldSize.x / 2, 300, worldCenter.z + worldSize.z / 2)
        Control_SetLocation(m_mapControl, mapBorderSize.x, mapBorderSize.z) -- Reset dragged image position
        Control_SetVisible(m_mapControl, true)
    end

    updateIcons()
end

--------------------------
-- INTERACTION HANDLERS --
--------------------------

-- Called when a key is pressed
function Dialog_OnKeyDown(dialogHandle, key, shift, ctrl, alt)

    -- Close the map with the 'M' key
    if (key == string.byte("M")) then
        btnClose_OnButtonClicked()
    end

end

function btnCenterMap_OnButtonClicked()
    -- Center map at player
    local px, py, pz, rot = KEP_GetPlayerPosition()
    px = math.floor(px * 10 + 0.5) / 10
    pz = math.floor(pz * 10 + 0.5) / 10
    worldCenter.x = px
    worldCenter.z = pz
    -- Update pan
    m_pan.x = px
    m_pan.z = pz
    -- Reset zoom
    -- worldSize.x = defaultWorldSize.x
    -- worldSize.z = defaultWorldSize.z

    btnUpdateMap_OnButtonClicked()
end

function btnZoomIn_OnButtonClicked()
    local tempXSize = math.floor(worldSize.x / 1.5 * 10 + 0.5) / 10
    local tempYSize = math.floor(worldSize.z / 1.5 * 10 + 0.5) / 10
    if tempXSize > MAX_ZOOM_IN and tempYSize > MAX_ZOOM_IN then
        worldSize.x = tempXSize
        worldSize.z = tempYSize
        btnUpdateMap_OnButtonClicked()
    end
end

function btnZoomOut_OnButtonClicked()
    local tempXSize = math.floor(worldSize.x * 1.5 * 10 + 0.5) / 10
    local tempYSize = math.floor(worldSize.z * 1.5 * 10 + 0.5) / 10
    if tempXSize < MAX_ZOOM_OUT and tempYSize < MAX_ZOOM_OUT then
        worldSize.x = tempXSize
        worldSize.z = tempYSize
        btnUpdateMap_OnButtonClicked()
    end
end

function btnUpdateMap_OnButtonClicked()
    local prevMapW = mapSize.x
    local prevMapH = mapSize.z

    if dynTexIdWorldMap~=0 and ((prevMapW ~= mapSize.x or prevMapH ~= mapSize.z) or m_clickAndDrag == true) then
        -- Texture dimension changed: discard old texture
        KEP_DestroyDynamicTexture(dynTexIdWorldMap)
        dynTexIdWorldMap = 0
    end

    updateMap()
end

----------------------
-- HELPER FUNCTIONS --
----------------------

rect = function(x, y, w, h)
    return { x = x, y = y, w = w, h = h }
end

color = function(a, r, g, b)
    return { a = a, r = r, g = g, b = b }
end

addImage = function(name, rect, color, texName, texRect)
    local img = Dialog_GetImage(gDialogHandle, name)
    if img == nil then
        img = Dialog_AddImage(gDialogHandle, name, rect.x, rect.y, texName, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
        Image_SetColor(img, color)
        Control_SetSize(Image_GetControl(img), rect.w, rect.h)
    end
    return img
end

addButton = function(name, text, rect, texName, texRect)
    local btn = Dialog_GetButton(gDialogHandle, name)
    if btn == nil then
        btn = Dialog_AddButton(gDialogHandle, name, text, rect.x, rect.y, rect.w, rect.h)
    end

    local elem
    elem = Button_GetDisabledDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetFocusedDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetMouseOverDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetNormalDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)
    elem = Button_GetPressedDisplayElement(btn)
    Element_AddTexture(elem, gDialogHandle, texName)
    Element_SetCoords(elem, texRect.x, texRect.y, texRect.x + texRect.w, texRect.y + texRect.h)

    return btn
end

addLabel = function(name, text, rect, fontFace, fontSize, fontColor)
    local stc = Dialog_GetStatic(gDialogHandle, name)
    if stc == nil then
        local stcFontSize = fontSize + 2
        local stcW = stcFontSize*#text*0.8
        stc = Dialog_AddStatic(gDialogHandle, name, text, rect.x, rect.y, stcW, rect.h)
        local elem = Static_GetDisplayElement(stc)
        local bc = Element_GetFontColor(elem)
        Element_AddFont(elem, gDialogHandle, fontFace, stcFontSize, 0, false)
        Element_SetTextFormat(elem, TEXT_HALIGN_LEFT + TEXT_VALIGN_CENTER)
        BlendColor_SetColor(bc, 0, fontColor)
    end
end

-- Returns the number of non-nil items in a table
tableCount = function(inputTable)
    local tempCount = 0
    for i,v in pairs(inputTable) do
        if v ~= nil then
            tempCount = tempCount + 1
        end
    end
    return tempCount
end

convertCoord = function(inputCoord, inputSize, outputSize)
    local tempCoord = {}
    -- Determine origin point (middle of the map)
    local xOffset = mapSize.x/2
    local zOffset = mapSize.z/2
    -- Determine scaling
    local xScale = ((outputSize.x) / inputSize.x) or 1
    local zScale = ((outputSize.z) / inputSize.z) or 1
    local xPanScale = (mapSize.x / worldSize.x) or 1
    local zPanScale = (mapSize.z / worldSize.z) or 1
    -- Adjust for zoom
    tempCoord.z = inputCoord.x * xScale -- Swap z and x for the initial coordinate because of 90 degree rotation
    tempCoord.x = inputCoord.z * zScale -- Swap z and x for the initial coordinate because of 90 degree rotation
    -- Adjust for pan
    tempCoord.x = tempCoord.x - (worldCenter.z * xPanScale * 1) -- Swap z and x for the relative world center because of 90 degree rotation
    tempCoord.z = tempCoord.z - (worldCenter.x * zPanScale * 1) -- Swap z and x for the relative world center because of 90 degree rotation
    -- Adjust for quadrant
    tempCoord.x = -tempCoord.x + xOffset
    tempCoord.z = -tempCoord.z + zOffset -- Swap the sign because of 90 degree rotation
    -- Round to one decimal place
    tempCoord.x = math.floor(tempCoord.x * 10 + 0.5) / 10
    tempCoord.z = math.floor(tempCoord.z * 10 + 0.5) / 10

    return tempCoord
end

-- Returns whether a search item exists in a table
iconTypeExists = function(searchItem)
    for i,v in pairs(m_mapItemData) do
        if v and v.iconType and v.iconType == searchItem then
            if v.pos.y >= -0.01 or v.iconType == MapDefinitions.LABEL_INDEX.YOU or v.iconType == MapDefinitions.LABEL_INDEX.PLAYERS then -- Icons beneath the ground don't count, unless they are a player
                return true
            end
        end
    end
    return false
end

getPanDistance = function(x,z)
    -- Base shift value
    local tempDist = {x=x, z=z}
    -- Ratio scaled shift value
    local xPanUnits = (worldSize.x / defaultWorldSize.x) or 1
    local zPanUnits = (worldSize.z / defaultWorldSize.z) or 1

    tempDist.x = tempDist.x * xPanUnits
    tempDist.z = tempDist.z * zPanUnits

    return tempDist
end

function generateOutlinedString(baseName, outlineSize, outlineColor)
    local tempControl = Dialog_GetControl(gDialogHandle, "stc"..baseName)
    local controlText = "<font color="..tostring(outlineColor)..">"..Static_GetText(tempControl).."</font>"
    local XLocation = Control_GetLocationX(tempControl)
    local YLocation = Control_GetLocationY(tempControl)
    local XOffset = {0, 0, outlineSize, -1*outlineSize}
    local YOffset = {outlineSize, -1*outlineSize, 0, 0}
    for i = 1, 4 do 
        local newControl = copyControl(tempControl)
        local newControlName = "stc" .. baseName .. "BG" .. i
        Control_SetName(newControl, newControlName)
        Static_SetText(newControl, controlText)
        Control_SetLocationX(newControl, XLocation + XOffset[i])
        Control_SetLocationY(newControl, YLocation + YOffset[i])
        Static_SetUseHTML(newControl, true)
        gHandles[newControlName] = newControl
    end
    Dialog_MoveControlToFront(gDialogHandle, tempControl)
end

-- Copy and paste controls
function copyControl(menuControl)
    if menuControl then
        Dialog_CopyControl( gDialogHandle, menuControl )
        Dialog_PasteControl( gDialogHandle )

        local numControls = Dialog_GetNumControls( gDialogHandle )

        local newControl = Dialog_GetControlByIndex( gDialogHandle, numControls - 1 )
        local controlName = Control_GetName( newControl )

        gHandles[controlName] = newControl  

        return newControl
    end
end

-- Player data functions

-- Find a player's netId based on their name
function updateNetIds()
    -- Get all net Ids in the current zone
    local netIds = KEP_GetAllNetIds()
    local checkedPlayers = {}
    
    local valueStart, value
    local valueEnd = 0
    
    -- Extract all netIds
    valueStart, valueEnd, value = string.find(netIds, "[^%s]+", valueEnd)
    if valueEnd == nil then return end
    -- For each netId, check if it's currently registered
    while valueEnd <= string.len(netIds) do
        local netId = (tostring(string.sub(netIds, valueStart, valueEnd)))
        local playerName = getPlayerNameByNetID(netId)
        -- Ensure a playerName was returned
        if playerName and playerName ~= "" then
            -- Add playerName to checked players
            checkedPlayers[playerName] = true
            -- Does this player need a default registration?
            if m_players[playerName] == nil then
                m_players[playerName] = {type      = ObjectType.PLAYER,
                                         netId     = tonumber(netId)
                                        }
            -- If this player exists but is simply missing their netId
            elseif m_players[playerName].netId ~= tonumber(netId) then
                m_players[playerName].netId = tonumber(netId)
            
            elseif m_players[playerName].netId == nil then
                m_players[playerName].netId = tonumber(netId)
            end
        -- else
        --     LogError("updateNetIds() - getPlayerNameByNetID("..tostring(netId).." failed to return a valid playerName Return["..tostring(playerName).."]")
        end
        
        -- Get the next netId if one exists
        valueStart, valueEnd, value = string.find(netIds, "[^%s]+", valueEnd+1)
        -- Fail check
        if valueEnd == nil then break end
    end
    
    -- For every player that wasn't checked, clear out their now invalid netId
    for ID, object in pairs(m_players) do
        if (object.type == PLAYER_OBJECT) and (checkedPlayers[ID] == nil) then
            Log("updateNetIds() Player["..tostring(ID).."] has exited your netID withdrawal distance. Clearing their NetID")
            -- This player's netId is no longer valid. Clear it out
            m_players[name].netId     = nil
        end
    end
end

-- Gets a player by a netID
getPlayerNameByNetID = function(netID)
    if netID == nil then return nil end
    local playerObj = getPlayerObjectFromNetId(netID)
    
    -- Validate check this playerObj
    if KEP_ObjectIsValid(playerObj) == 0 then
        LogWarn("getPlayerNameByNetID() -  KEP_ObjectIsValid("..tostring(playerObj)..") returned 0 (invalid)")
        return nil
    end

    local playerName = GetSet_Safe_GetString(playerObj, MovementIds.NAME)
    local controlType = GetSet_Safe_GetNumber(playerObj, MovementIds.CONTROLTYPE)
    -- LogInfo("<" .. tostring(netID) .. "> control type is " .. tostring(controlType))
    -- Is not an NPC?
    if controlType~=-1 and controlType~=0 then
        -- LogInfo("<" .. tostring(netID) .. "> NOT a player")
        return nil
    -- Player has a name?
    elseif playerName == nil then
        LogWarn("getPlayerNameByNetID() - Failed to return playerName from GetSet_Safe_GetString("..tostring(playerObj)..", "..tostring(MovementIds.NAME)..")")
        return nil
    else
        return playerName
    end
end

-- Gets the World Position of a target
getWorldPosition = function(target, targetType)
    local targetPos = {x = 0, y = 0, z = 0}
    -- Different call for target type
    if targetType == ObjectType.PLAYER then
        targetPos.x, targetPos.y, targetPos.z = GetSet_Safe_GetVector(target, MovementIds.LASTPOSITION)
    end
    
    return targetPos
end

-- Returns a player object from a netID
getPlayerObjectFromNetId = function(netId)
    if netId == nil then
        LogWarn("getPlayerObjectFromNetId() Received nil netId")
        return nil
    end
    local playerObj = KEP_GetPlayerByNetworkDefinedID(netId)

    if playerObj then
        if KEP_ObjectIsValid(playerObj) ~= 0 then
            return playerObj
        else
            LogWarn("getPlayerObjectFromNetId() KEP_ObjectIsValid("..tostring(playerObj)..") returned 0 (invalid)")
            return nil
        end
    else
        LogWarn("getPlayerObjectFromNetId() No playerObj returned from KEP_GetPlayerByNetworkDefinedID("..tostring(netId)..")")
        return nil
    end
end

-- Click and Drag Methods

clickAndDragEnd = function()
    Control_SetVisible(m_mapControl, false)
    -- Control_SetLocation(m_mapControl, mapBorderSize.x, mapBorderSize.z) -- Reset dragged image position
    -- Control_SetVisible(m_mapControl, true)
    btnUpdateMap_OnButtonClicked() -- Re-render map
    m_clickAndDrag = nil
    m_clickDragPrevPos = nil
end 

clickAndDragBegin = function(x, y)
    m_clickAndDrag = true
    m_clickDragPrevPos = {x=x, y=y}
    m_prevWorldCenter = {x=worldCenter.x, z=worldCenter.z}
end

shiftMap = function()
    -- Control_SetLocation(m_mapControl, m_pan.z - worldSize.z, m_pan.x - worldSize.x)
    local tempCoord = {x = worldCenter.z - m_prevWorldCenter.z, z = worldCenter.x - m_prevWorldCenter.x}

    -- Determine scaling
    local xScale = (mapSize.x / worldSize.x) or 1
    local zScale = (mapSize.z / worldSize.z) or 1
    
    -- Adjust for zoom
    tempCoord.z = tempCoord.z * zScale -- Swap z and x for the initial coordinate because of 90 degree rotation
    tempCoord.x = tempCoord.x * xScale -- Swap z and x for the initial coordinate because of 90 degree rotation
    
    -- Round to one decimal place
    tempCoord.x = math.floor(tempCoord.x * 10 + 0.5) / 10
    tempCoord.z = math.floor(tempCoord.z * 10 + 0.5) / 10

    -- Adjust for map origin
    tempCoord.x = tempCoord.x + mapBorderSize.x
    tempCoord.z = tempCoord.z + mapBorderSize.z
    
    Control_SetLocation(m_mapControl, tempCoord.x, tempCoord.z)
end

-----------------------
-- Generate map data --
-----------------------

updatePlayerPosition = function()
    -- TO DO: Get player rotation and adjust icon accordingly (requires ability to rotate images)
    m_mapItemData[1].pos = {x = tempX, y = tempY, z = tempZ}
end

initializeMapData = function()
    m_mapItemData = {}
    -- Add player position
    local tempX, tempY, tempZ, tempR = KEP_GetPlayerPosition()
    local tempData = {iconType = MapDefinitions.LABEL_INDEX.YOU, name = KEP_GetLoginName(), pos = {x = tempX, y=tempY, z = tempZ}, radius = 0, PID = 0, UNID = 0}
    table.insert(m_mapItemData, tempData)
    addPlayersByNetID()
end

addPlayersByNetID = function()
    for i,v in pairs(m_players) do
        local tempNetId = v.netId
        if tempNetId then
            local tempName = tostring(i)
            local tempPlayerObj = getPlayerObjectFromNetId(tempNetId)
            if tempPlayerObj then
                local tempPos = getWorldPosition(tempPlayerObj, ObjectType.PLAYER)
                if tempName and tempName ~= KEP_GetLoginName() and tempPos then
                    -- Add player to table of map data
                    local tempData = {iconType = MapDefinitions.LABEL_INDEX.PLAYERS, name = tempName, pos = {x = tempPos.x, y=tempPos.y, z = tempPos.z}, radius = 0, PID = 0, UNID = 0}
                    table.insert(m_mapItemData, tempData)
                end
            end
        end
    end
end

--------------------------
-- FAKE DATA GENERATION --
--------------------------

genFakeData = function()
    initializeMapData()

    local tempData = {iconType = MapDefinitions.LABEL_INDEX.CORPSE, name = KEP_GetLoginName().."'s Corpse", pos = {x = 212, y=0, z = 93}, radius = 0, PID = 0, UNID = 0}
    table.insert(m_mapItemData, tempData)
    tempData = {iconType = MapDefinitions.LABEL_INDEX.CURRENT_QUEST_GIVER, name = genRandName(), pos = genRandCoord(), radius = math.random(20, 80), PID = 0, UNID = 0}
    table.insert(m_mapItemData, tempData)
    tempData = {iconType = MapDefinitions.LABEL_INDEX.GOAL, name = genRandName(), pos = genRandCoord(), radius = math.random(20, 80), PID = 0, UNID = 0}
    table.insert(m_mapItemData, tempData)

    for i=1,30 do
        local tempData = {iconType = genRandType(), name = genRandName(), pos = genRandCoord(), radius = math.random(20, 80), PID = 0, UNID = 0}
        table.insert(m_mapItemData, tempData)
    end
end

genRandType = function()
    local tempType = math.random(0,8)
    local typeConversion = {
        [0] = 2,
        [1] = 3,
        [2] = 6,
        [3] = 7,
        [4] = 8,
        [5] = 9,
        [6] = 10,
        [7] = 11,
        [8] = 12
    }
    tempType = typeConversion[tempType]
    
    return tempType
end

genRandCoord = function()
    local tempPos = {}
    tempPos.x = math.random(worldSize.x/2)
    if math.random(2) == 1 then
        tempPos.x = -tempPos.x
    end
    tempPos.z = math.random(worldSize.z/2)
    if math.random(2) == 1 then
        tempPos.z = -tempPos.z
    end
    tempPos.y = 0
    return tempPos
end

genRandName = function()
    local firstLetterConsonants = {"B","C","Ch","D","F","G","H","J","K","L","M","N","P","Qu","R","S","Sh","T","Th","Tch","V","W","X","Y","Z"} -- 24
    local firstLetterVowels = {"A","Ae","Ai","Au","E","Ee","Ei","I","Ie","O","Oo","Ou","U","Ya","Ye","Yi","Yo","Yu"} -- 18
    local consonants = {"b","c","ch","d","f","g","h","j","k","l","m","n","p","qu","r","s","sh","t","th","tch","v","w","x","y","z"} -- 24
    local vowels = {"a","ae","ai","au","e","ee","ei","i","ie","o","oo","ou","u","ya","ye","yi","yo","yu"} -- 18
    local nameLength = math.max(math.floor(math.random(6)), 3)
    local tempName = ""
    for i=0, nameLength do
        local tempChar = ""
        if i == 0 then
            -- if math.random(2) == 1 then
                tempIndex = math.random(24)
                tempChar = firstLetterConsonants[tempIndex]
            -- else
            --     tempIndex = math.random(18)
            --     tempChar = firstLetterVowels[tempIndex]
            -- end
        elseif i%2 == 0 then
            tempIndex = math.random(24)
            tempChar = consonants[tempIndex]
        else
            tempIndex = math.random(18)
            tempChar = vowels[tempIndex]
        end
        tempName = tempName..tempChar
    end
    return tempName
end

requestMapData = function()
    local ev = KEP_EventCreate("FRAMEWORK_WORLD_MAP_REQUESTED_BY_CLIENT")
    KEP_EventQueue(ev)
end