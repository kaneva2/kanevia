--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------------------------
--Menu to handle sizing the drop shadow
--This will also handle centering menu on lower resolution
----------------------------------------------------------
TITLEBAR_HEIGHT = 30
X_OFFSET = -29
Y_OFFSET = -55
WIDTH_OFFSET = 65
HEIGHT_OFFSET = 59


function resizeDropShadow( )
	if gHandles["imgShadow"] then
		local imgShadowCtrl = gHandles["imgShadow"]
		local menuSize = MenuGetSizeThis()
		Control_SetLocation(imgShadowCtrl, X_OFFSET, Y_OFFSET)
		Control_SetSize(imgShadowCtrl, (menuSize.width + WIDTH_OFFSET), (menuSize.height +  HEIGHT_OFFSET))
	end

	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	if screenHeight <= 760 then
		MenuCenterThis(true)
	end
end

function clientResizeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local screenHeight = Dialog_GetScreenHeight(gDialogHandle)
	if screenHeight <= 760 then
		MenuCenterThis(true)
	end
end