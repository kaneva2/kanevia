--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_ImageHandler.lua
-- Author: Wes
-- Downloads images for Game Items for easy reference by menus that would
-- display Game Items by UNID
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

------------------------
-- Local vars
------------------------
--local m_images = {}
local m_gameId
local m_downloadImageHandle

------------------------
-- Local functions
------------------------
local downloadImage -- (UNID, image)
local safeDecode -- (event, type)
local extractImageName -- (imageURL)

function onCreate()
	-- Register events we'll be sending from this menu
	--[[KEP_EventRegister("IMAGES_GET_ALL", KEP.HIGH_PRIO)
	KEP_EventRegister("IMAGES_RETURN_ALL", KEP.HIGH_PRIO)
	KEP_EventRegister("IMAGES_GET_BY_UNID", KEP.HIGH_PRIO)
	KEP_EventRegister("IMAGES_RETURN_BY_UNID", KEP.HIGH_PRIO)]]
	
	-- Register handlers for client-to-client events
	--[[KEP_EventRegisterHandler("getAllImages", "IMAGES_GET_ALL", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("getImageByUNID", "IMAGES_GET_BY_UNID", KEP.HIGH_PRIO)]]
	
	-- Register handler for server-to-client events
	Events.registerHandler("DOWNLOAD_GLID_ICON", downloadGLIDIcon)
	
	m_downloadImageHandle = Image_GetDisplayElement(gHandles["imgGLIDDownload"])
	m_gameId = KEP_GetGameId()
end

------------------------
-- Event handlers
------------------------
-- Request to return all images downloaded by this client
--[[function getAllImages(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local event = KEP_EventCreate("IMAGES_RETURN_ALL")
	
	local imageTable = Events.encode(m_images)
	
	-- Ready to fire return event
	if imageTable and event then
		KEP_EventEncodeString(event, imageTable)
		KEP_EventSetFilter(event, 4)
		KEP_EventQueue(event)
	else
		LogWarn("getAllImages() Failed to encode return event for all Game Item Images")
	end
end]]

-- Request to return all images downloaded by this client
--[[function getImageByUNID(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local UNID = tostring(safeDecode(tEvent, "number"))
	local event = KEP_EventCreate("IMAGES_RETURN_BY_UNID")
	
	--Ready to fire the return
	if m_images[UNID] and event and UNID then
		local returnTable = {UNID = UNID,
							 image = m_images[UNID]
							}
		-- local imageTable = Events.encode(returnTable)
		if imageTable then
			KEP_EventEncodeString(event, imageTable)
			KEP_EventSetFilter(event, 4)
			KEP_EventQueue(event)
		else
			LogWarn("Framework_ImageHandler.lua - getImageByUNID() Failed to encode return event {UNID["..tostring(UNID)..",image["..tostring(m_images[UNID]).."]}")
		end
	else
		LogWarn("Framework_ImageHandler.lua - getImageByUNID() No valid image registered for UNID["..tostring(UNID).."]")
	end
end]]

-- Download a GLID Icon
function downloadGLIDIcon(event)
	if event.images then
		-- Download all images in the imageTable
		for i=1, #event.images do
			local UNID = event.images[i].UNID
			local image = event.images[i].image
			-- Map result to this UNID
			local returnVal = downloadImage(UNID, image)
			--m_images[UNID] = returnVal
		end
	end
end

------------------------
-- Local functions
------------------------

-- Downloads an image based on the type of item passed in
downloadImage = function(UNID, image)
	-- Are we dealing with a filestore image path?
	if string.find(image, "filestore") and KEP_DownloadTextureAsyncToGameId then
		-- The name the image will saved as
		local filename = extractImageName(image) or UNID..".jpg"
		local path = KEP_DownloadTextureAsyncToGameId(filename, image)
		
		-- Strip off beginning of file path leaving only file name
		path = string.gsub(path,".-\\", "" )
		path = "../../CustomTexture/".. m_gameId .. "/" .. path
		return path
	-- Are we dealing with a UNID?
	elseif type(image) == "number" then
		if ValidGlid(image) and m_downloadImageHandle then -- drf - validate glid & image handle
			KEP_LoadIconTextureByID(image, m_downloadImageHandle, gDialogHandle)
		end
		return image
	end
	return
end

-- Takes in an event and if there's an item for decoding, returns it
--[[safeDecode = function(event, type)
	-- Return nil if there's nothing left to decode
	if KEP_EventMoreToDecode(event) == 0 then return end
	if type == "number" then
		return KEP_EventDecodeNumber(event)
	elseif type == "string" then
		return KEP_EventDecodeString(event)
	end
end]]

-- Extracts a helpful image name from a URL
extractImageName = function(imageURL)
	local imageName
	-- Check for valid URL
	local sStart, sEnd = string.find(imageURL, "filestore")
	if sEnd then
		imageName = string.sub(imageURL, sEnd+4, string.len(imageURL))
		if string.find(imageName, ".") then
			imageName = string.sub(imageName, 0, string.len(imageName)-4)
		end
		
		-- Continue to parse out forward slashes until they're all gone
		local imageCopy = imageName
		while string.find(imageName, "/") do
			-- Parse out up to the "/"
			local sStart, sEnd, _ = string.find(imageName, "/")
			if sEnd then
				imageName = string.sub(imageName, sEnd+1, string.len(imageName))
			end
		end
		return imageName
	else
		return
	end
end