--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_PlayerBuild.lua
--
-- Helps players manage their placed items in the zone
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\Framework_InventoryHelper.lua")
dofile("..\\Scripts\\BuildMode.lua")
dofile("..\\MenuScripts\\Lib_Vecmath.lua")
dofile("..\\MenuScripts\\Framework_FlagHelper.lua")
dofile("..\\MenuScripts\\Framework_MediaHelper.lua")
dofile("..\\MenuScripts\\Framework_BuildActionHelper.lua")
dofile("..\\MenuScripts\\CAM.lua")

-- Constants
local PLACEMENT_OFFSET = {x = 0, y = 0, z = -10} -- Placement offset for all player placeables (TODO: Including Prefabs? TBD)

local MIN_INTERACT_DISTANCE = 140 -- How far away can the player interact with objects?
local MIN_INTERACT_DISTANCE_SQRD = MIN_INTERACT_DISTANCE * MIN_INTERACT_DISTANCE -- (squared for cheaper comparison point)

local SELECT_HIGHLIGHT			= {0, 0, 1}
local MOUSEOVER_HIGHLIGHT_RED	= {1, 0, 0}


local CAMERA_BUFFER = .3
local PLAYER_BUILD_CAMERA = 40*(math.pi/180)

-- Local vars
local s_playerName
local s_isInPlayerMode
local s_placementMode -- "Prefab" or "Placement" Passed from Framework_Toolbar with the itemData in "FRAMEWORK_BUILD_ITEM_INFO"
local s_isInHandMode = false -- default to false so that non-game world is not affected
local s_isBuildingAllowed = true --set true to start, only turn it off if we get a no build reason
local s_isBuildingAllowedInZone	-- World build setting
local s_wasBuildingAllowed 
local s_maxFlags = 0
local s_maxFlagObjects = 0
local s_flagBuildOnly = false


local s_shouldForceHighlightUpdate = false
local s_selectedObjectPID
local s_ghostList -- Table of placement info of current locally generated ghosts {GLID, PID, offset}
local s_noBuildReason
local DEFAULT_NO_BUILD_REASON = "You cannot build here."

local s_buildLocation = {}
local s_placedObjects = {} -- table of placed objects and their owner
local s_gameItems = {} -- added for the game item cache, keys are UNID as a string 

local s_gameItemLoadExceeded = false -- Has the Game Item Load been exceeded?

local s_lastPlayerLocation = {0, 0, 0}

local s_cameraAutoAdjust = 0

-- last item moused over behavior
local s_lastItemBehavior 

local FlagSystem			= _G.FlagSystem			-- Assign the global FlagSystem to a local variable for faster access
local MediaSystem			= _G.MediaSystem		-- Assign the global MediaSystem to a local variable for faster access
local BuildActionFactory	= _G.BuildActionFactory	-- Assign the global BuildActionFactory to a local variable for faster access
local Math					= _G.Math				-- Assign the global Math to a local variable for faster access

-- Local functions
local updateHighlightForObject -- (objectPID)
local placeGhost -- ()
local clearGhost -- ()
local selectObject -- (PID)
local deselectObject -- (PID)

local saveDynamicObject -- (PID)
local showBuildControls -- (show)
local changeBuildMode -- (mode)
local isObjectWithinInteractionRange -- (PID)
local isPlacedObjectMetalSafe -- (objectPID)
local isPlacedObjectSpawned -- (placedObject)
local isPlacedObjectOwned -- (placedObject)
local canSelectObject -- (objectPID)
local canSelect -- (filter, PID)
local requestAction -- (action)
local hasPlayerPositionChanged --()
local isLastClickedObjectVehicle -- ()
local onSelectionDenied -- ()
local getWorldInfoForBuildAction -- ()
local adjustCamera --()

-- Called when the menu is created
function onCreate()
	-- Get player's name
	s_playerName = KEP_GetLoginName()
	
	-- Register an event used to communicate when BuildHelp is opened and closed
	KEP_EventRegister( "FRAMEWORK_ON_BUILD_HELPER_CHANGE", KEP.MED_PRIO )

	KEP_EventRegister( "UPDATE_FLAG_REGION_COLOR", KEP.MED_PRIO )

	-- Event handlers
	KEP_EventRegisterHandler("onBuildModeChanged", "FRAMEWORK_PLAYER_BUILD_CHANGE_MODE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onFrameworkModeChanged", "FrameworkState", KEP.HIGH_PRIO)
	
	-- Event handlers for using a selected item 
	KEP_EventRegisterHandler("onUseSelectedObject", "FRAMEWORK_USE_SELECTED_ITEM", KEP.HIGH_PRIO)
	
	-- Register for Click events
	KEP_EventRegisterHandler("onClicked", "ClickEvent", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onSelectionChanged", "SelectEvent", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler("onNewItemBeingBuilt", "FRAMEWORK_BUILD_ITEM_INFO", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onNewItemBeingPlaced", "FRAMEWORK_BUILD_PLACE_ITEM", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onHandModeChanged", "FRAMEWORK_BUILD_HAND_MODE", KEP.HIGH_PRIO)
	
	KEP_EventRegisterHandler( "onObjectMovedByWidget", "WidgetTranslationEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "onObjectPositionChanged", "DynamicObjectMovedEvent", KEP.HIGH_PRIO )

	-- event to see the behavior of the last item moused over
	KEP_EventRegisterHandler( "onItemMousedOver", "ITEM_MOUSE_OVER_BEHAVIOR", KEP.HIGH_PRIO )
	
	-- Register for game item updates
	KEP_EventRegisterHandler("onGameItemUpdatedOnClient", "UPDATE_GAME_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("onAllGameItemsUpdatedOnClient", "UPDATE_GAME_CLIENT_FULL", KEP.HIGH_PRIO)


	--Regiester for flag updates
	KEP_EventRegisterHandler( "onFlagsReturnedFull", "UPDATE_FLAGS_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onFlagReturned", "UPDATE_FLAG_CLIENT", KEP.MED_PRIO)

	--Regiester for media updates
	KEP_EventRegisterHandler( "onMediaReturnedFull", "UPDATE_MEDIA_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onMediaReturned", "UPDATE_MEDIA_CLIENT", KEP.MED_PRIO)
	
	-- NOTE: Framework_BattleHandler will request battle objects and we'll piggy-back the return here
	-- Register for targetable entitty event

	Events.registerHandler("FRAMEWORK_REGISTER_BATTLE_OBJECT_LIST", onAllBattleObjectsUpdated)
	Events.registerHandler("FRAMEWORK_REGISTER_BATTLE_OBJECT", onBattleObjectUpdated)
	Events.registerHandler("FRAMEWORK_UNREGISTER_BATTLE_OBJECT", onBattleObjectRemoved)
	
	
	-- Set original build setting
	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS", onWorldSettingsChanged)

	-- Register flag events
	Events.registerHandler("FRAMEWORK_PLACE_CLAIM_FLAG_CONFIRMATION", onClaimFlagPlacementRejected)
	
	--Register media player events
	--Events.registerHandler("FRAMEWORK_RETURN_MEDIA_OBJECTS", onMediaPlayersUpdated)
	Events.registerHandler("FRAMEWORK_INVALID_MEDIA_PLACMENT", onMediaPlayerPlacementRejected)
	
	
	-- Register for Game Item Load limit exceeded events
	Events.registerHandler("FRAMEWORK_GAME_ITEM_LOAD", onGameItemLimitReached)
	-- Request the current load (NOTE: Framework_Toolbar piggybacks this events. Take care before removing this event)
	Events.sendEvent("FRAMEWORK_REQUEST_GAME_ITEM_LOAD")
	
	-- Initialise the flag system
	FlagSystem:create()
	MediaSystem:create()
	
end

function onDestroy()
	if s_selectedObjectPID then
		deselectObject(s_selectedObjectPID)
	end

	if s_cameraAutoAdjust then
		SetCameraLevel(1)
	end
end


-- On dialog render
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if s_ghostList then
		placeGhost()
	end

	-- Clear selection when distance to player has moved beyond max range
	-- Only check if proper mode, object is selected
	if s_isInHandMode and s_selectedObjectPID then
		
		local playerPosChanged = hasPlayerPositionChanged()
		-- Only deselect if player has moved - otherwise will cause camera issues
		if playerPosChanged then 
			if not isObjectWithinInteractionRange(s_selectedObjectPID) then
				KEP_ClearSelection()
			end
		end
	end
end

function Dialog_OnLButtonUp(dialogHandle, x, y)
	
	handleDeselect()
	
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --


function onBuildModeChanged(dispatcher, fromNetid, event, eventid, filter, objectid)
	local mode = KEP_EventDecodeNumber(event)

	changeBuildMode(mode)
end

-- Use the selected item
function onUseSelectedObject(dispatcher, fromNetid, event, eventid, filter, objectid)
	Events.sendEvent("ON_USE_OBJECT", {PID = s_selectedObjectPID})
end

-- Called when a player changes Framework mode or when the Framework changes states (enabled/disabled)
function onFrameworkModeChanged(dispatcher, fromNetid, event, eventid, filter, objectid)

	local frameworkState = cjson.decode(Event_DecodeString(event))
	if frameworkState.playerMode ~= nil then
		if s_selectedObjectPID then
			deselectObject(s_selectedObjectPID)
		end
		s_isInPlayerMode = (frameworkState.playerMode == "Player")
	
		-- Are we leaving player mode?
		if not s_isInPlayerMode then
			-- Clean up
			if s_ghostList then
				clearGhost()
			end
			
			s_isInHandMode = false
		end
	end
end

-- Retrieves new world settings from the server
function onWorldSettingsChanged(event)
	-- battle_handleSettings(event)
	s_isBuildingAllowedInZone	= event.playerBuild -- Only changes when a new world setting is passed from the server
	s_wasBuildingAllowed		= event.playerBuild

	s_maxFlags					= event.maxFlags
	s_maxFlagObjects			= event.maxFlagObjects
	s_maxFlagObjects			= event.maxFlagObjects
	s_flagBuildOnly				= event.flagBuildOnly
	
	-- Sending event from playerbuild to toolbar for build mode change
	local ev = KEP_EventCreate("FRAMEWORK_BUILD_SETTING")
	KEP_EventEncodeString(ev, tostring(s_isBuildingAllowedInZone))
	KEP_EventEncodeString(ev, DEFAULT_NO_BUILD_REASON)
	KEP_EventQueue(ev)
end



-- Called when the player clicks
function onClicked(dispatcher, fromNetid, event, eventid, filter, objectid)
	local button = KEP_EventDecodeNumber(event) or 0
	local objType = KEP_EventDecodeNumber(event) or 0
	local objId = KEP_EventDecodeNumber(event) or 0

	if not s_isInPlayerMode or not s_isInHandMode then 
		return 
	end

	-- 0 = left click, 1 = right click 
	-- the player build should show if you left click and own the item 
	local playerCanBuild = canSelect(objType, objId)
	-- if button == 0 and not playerCanBuild then
	-- 	KEP_ClearSelection()
	-- 	return
	-- elseif button == 1 and playerCanBuild then 
	-- 	KEP_ClearSelection()
	-- 	return
	-- end 
	if button == 1 and playerCanBuild then 
		KEP_ClearSelection()
		return
	end 
	
	-- If the object selected is further away than the max, consider it clicking on nothing and clear selection
	if not isObjectWithinInteractionRange(objId) then
		KEP_ClearSelection()
		return
	end
		
	-- If we've clicked something new
	if s_selectedObjectPID ~= objId then
		-- Do we own it?
		local canBeSelected = canSelect(objType, objId) and s_lastItemBehavior ~= "pet"
		if canBeSelected then
			KEP_ReplaceSelection(ObjectType.DYNAMIC, objId)
		else
			onSelectionDenied()
			
			-- Is something currently build selected?
			if s_selectedObjectPID then
				KEP_ClearSelection()
			end
		end
	end
end

-- Called when the player clicks
function onSelectionChanged(dispatcher, fromNetId, event, eventId, filter, objectid)
	local count = KEP_EventDecodeNumber(event)
	for i = 1, count do
		local selectionType		= KEP_EventDecodeNumber(event)
		local PID				= KEP_EventDecodeNumber(event)
		local isSelected		= KEP_EventDecodeNumber(event)
		
		if selectionType == ObjectType.DYNAMIC then
			if isSelected == 0 then
				deselectObject(PID)
			elseif isSelected == 1 then
				local shouldSelect = false
				if not s_isInPlayerMode then
					shouldSelect = true
				else
					if(s_isInHandMode) then
						if canSelectObject(PID) then
							shouldSelect = true
						else
							onSelectionDenied()
						end
					end
				end
				
				if shouldSelect then
					selectObject(PID)
				end
			end
		end
	end
end


-- Request to place an item from the toolbar
function onItemMousedOver(dispatcher, fromNetid, event, eventid, filter, objectid)
	s_lastItemBehavior = KEP_EventDecodeString(event)
end


-- New item ready for displaying
function onNewItemBeingBuilt(dispatcher, fromNetid, event, eventid, filter, objectid)
	local placementInfo = cjson.decode(KEP_EventDecodeString(event))
	s_ghostList = placementInfo.items
	
	s_placementMode = placementInfo.mode
	
	s_shouldForceHighlightUpdate = true
	-- Place a ghost object in front of the player

	adjustCamera()

	placeGhost()
end

-- Request to place an item from the toolbar
function onNewItemBeingPlaced(dispatcher, fromNetid, event, eventid, filter, objectid)
	clearGhost()
end



-- Enter or exit hand mode
function onHandModeChanged(dispatcher, fromNetid, event, eventid, filter, objectid)
	s_isInHandMode = KEP_EventDecodeString(event) == "true"
	
	-- Entering hand mode
	if s_isInHandMode then
		clearGhost()
	
	-- Leaving hand mode
	else
		if s_ghostList then
			clearGhost()
		end
		
		-- Clear build selected?
		if s_selectedObjectPID then
			deselectObject(s_selectedObjectPID)
		end
	end
end




-- Retrieves battle objects from the server
function onAllBattleObjectsUpdated(event)
	if event.objects then
		-- Extract all targetable objects
		for i, object in ipairs(event.objects) do
		
			local PID 	= tonumber(object.ID)
			local x, y, z = KEP_DynamicObjectGetPosition(PID)
			
			s_placedObjects[PID] =	{
				owner		= object.owner,
				position	= {x = x, y = y, z = z},
				spawned		= object.spawned
			}
			
		end
	end
end

-- Retrieves new battle object from the server
function onBattleObjectUpdated(event)
	local type  = event.type
	local PID 	= tonumber(event.ID)

	if type == "Object" then
		local x, y, z = KEP_DynamicObjectGetPosition(PID)
		-- PlayerBuild handles owned item selection for movement and thus only needs alive and owner
		s_placedObjects[PID] =	{	
			owner		= event.owner,
			position	= {x = x, y = y, z = z},
			spawned		= event.spawned
		}
	end
end

function onBattleObjectRemoved(event)
	local PID 	= tonumber(event.ID)

	if PID then
		s_placedObjects[PID] = nil
	else
		log("WARNING: Framework_PlayerBuild - onBattleObjectRemoved received an invalid PID: " .. tostring(event.ID))
	end
end




-- Regenerate the list of flag objects using info from the server
function onFlagsReturnedFull(dispatcher, fromNetid, event, eventid, filter, objectid)
	local flags = Events.decode(KEP_EventDecodeString(event))
	FlagSystem:resetFlags(flags)

end

function onFlagReturned(dispatcher, fromNetid, event, eventid, filter, objectid )
	local PID = KEP_EventDecodeNumber(event)
	if KEP_EventMoreToDecode(event) ~= 0 then
		flag = Events.decode(KEP_EventDecodeString(event))
		FlagSystem:addFlag(flag)
		return
	end
	FlagSystem:removeFlag(PID)
end

-- Regenerate the list of media objects
function onMediaReturnedFull(dispatcher, fromNetId, event, eventId, filter, objectId)
	local media = Events.decode(KEP_EventDecodeString(event))
	MediaSystem:resetMedia(media)
	
end

function onMediaReturned(dispatcher, fromNetId, event, eventId, filter, objectId)
	local PID = KEP_EventDecodeString(event)
	if KEP_EventMoreToDecode(event) ~= 0 then
		media = Events.decode(KEP_EventDecodeString(event))
		MediaSystem:addMedia(media, tonumber(PID))
		return
	end
	MediaSystem:removeMedia(PID)
end

-- Answers to event generated by the server to inform the request for placing a claim flag was denied
function onClaimFlagPlacementRejected(event)
	s_isBuildingAllowed = false
	
	s_noBuildReason = "Cannot Place Flag. Items not owned in area."

	-- Display reason for not building
	displayStatusMessage(s_noBuildReason)
	
	handleDeselect()
	
	s_isBuildingAllowed = true
end

-- Answers to event generated by the server to inform the request for placing a media player was denied
function onMediaPlayerPlacementRejected(event)
	s_isBuildingAllowed = false

	s_noBuildReason = "Too close to media player owned by another player."
	
	-- Display reason for not building
	displayStatusMessage(s_noBuildReason)
	
	handleDeselect()
	
	s_isBuildingAllowed = true
end



-- Called when the Game Item limit has been exceeded
function onGameItemLimitReached(event)
	-- If the Game Item load is such that players can't build anymore (98% full or something. Server determines this)
	if event.playerBuild == false then
		s_gameItemLoadExceeded = true

		-- If this player is currently attempting to place something, red it out so they know the load JUST overflowed
		if s_ghostList then
			for i, ghostInfo in pairs(s_ghostList) do
				updateHighlightForObject(ghostInfo.PID)
			end
		end
	end
end

-- Checks the flag location each time a player built object is moved
function onObjectMovedByWidget(dispather, fromNetId, event, eventId, filter, objectId)
	if not s_isInPlayerMode or not s_isInHandMode then 
		return 
	end

    local x = KEP_EventDecodeNumber(event)
    local y = KEP_EventDecodeNumber(event)
    local z = KEP_EventDecodeNumber(event)
	
	local position = {x = x, y = y, z = z}
	local worldInfo = getWorldInfoForBuildAction()

	local movementAction = BuildActionFactory.createMovementAction(s_selectedObjectPID, position, worldInfo)
	requestAction(movementAction)
end

function onObjectPositionChanged(dispather, fromNetId, event, eventId, filter, objectId)
	local PID = KEP_EventDecodeNumber(event)
	local object = s_placedObjects[PID]
	if( object ) then
		local x = KEP_EventDecodeNumber(event)
		local y = KEP_EventDecodeNumber(event)
		local z = KEP_EventDecodeNumber(event)
		object.position = {x = x, y = y, z = z}
	end
end



-- NOTE: onGameItemUpdatedOnClient(...) and onAllGameItemsUpdatedOnClient(...) functions are added to get a hold of Metal Safe objects in the world.
-- Should there appear a more efficient method for retrieving this info in the future, the following two hefty functions may become redundant...

-- Update a single game object entry
function onGameItemUpdatedOnClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local updateIndex = tostring(KEP_EventDecodeNumber(tEvent))
	local updateItem = decompileInventoryItem(tEvent)
	
	s_gameItems[updateIndex] = updateItem
end

-- Update the full list of game items
function onAllGameItemsUpdatedOnClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	local gameItems = decompileInventory(tEvent)
	s_gameItems = gameItems
end



-- -- -- -- -- -- --
-- Local Functions
-- -- -- -- -- -- --


getWorldInfoForBuildAction = function()
	local worldInfo = {
		playerName					= s_playerName,
		placedObjectList			= s_placedObjects,
		isBuildingAllowedInWorld	= s_isBuildingAllowedInZone,
		maxFlags					= s_maxFlags,
		maxFlagObjects				= s_maxFlagObjects,
		flagBuildOnly 				= s_flagBuildOnly,
		flagSystem					= FlagSystem,
		mediaSystem					= MediaSystem,
		fInteractionRangeTester		= isObjectWithinInteractionRange
	}
	
	return worldInfo
end

hasPlayerPositionChanged = function()
	local playerLocation = {KEP_GetPlayerPosition()}
	
	local hasPositionChanged =	s_lastPlayerLocation[1] ~= playerLocation[1] or 
								s_lastPlayerLocation[2] ~= playerLocation[2] or 
								s_lastPlayerLocation[3] ~= playerLocation[3]
								
	s_lastPlayerLocation = playerLocation
	
	return hasPositionChanged
end


handleDeselect = function()

	if s_selectedObjectPID and s_isInPlayerMode then
		-- Can the user build where they are?
		if s_isBuildingAllowed then
			
			s_buildLocation[1], s_buildLocation[2], s_buildLocation[3] = KEP_DynamicObjectGetPosition(s_selectedObjectPID)
			s_buildLocation[4], s_buildLocation[5], s_buildLocation[6] = KEP_DynamicObjectGetRotation(s_selectedObjectPID)
			
		else
			KEP_SetDynamicObjectPosition(s_selectedObjectPID, s_buildLocation[1], s_buildLocation[2], s_buildLocation[3])
			-- KEP_SetDynamicObjectRotation(s_selectedObjectPID, s_buildLocation[4], s_buildLocation[5], s_buildLocation[6]) -- no need to set rotation since only moved out of area

			KEP_ClearSelection()

			-- Close build controls if they were open
			local showEvent = KEP_EventCreate("FRAMEWORK_SHOW_BUILD_TOOLS")
			KEP_EventEncodeNumber(showEvent, 0)
			KEP_EventEncodeString(showEvent, "false")
			KEP_EventEncodeNumber(showEvent, -1)
			KEP_EventEncodeString(showEvent, "")
			KEP_EventQueue(showEvent)
			
			-- Display message
			displayStatusMessage(s_noBuildReason)
			
			s_selectedObjectPID = nil
			s_isBuildingAllowed = true
		end
	end
end

-- Sets the outline state for an object. Checks flags, player mode and Game Item load
updateHighlightForObject = function(objectPID)
	-- If a player is capable of building here (flags) and is in player mode
	if s_isBuildingAllowed and s_isInPlayerMode then
		-- Only check Game Item Load if we're placing a ghost
		if s_ghostList then
			-- Load not exceeded? Default outline
			if (not s_gameItemLoadExceeded) then
				KEP_SetOutlineState(ObjectType.DYNAMIC, objectPID, 1)
				KEP_SetOutlineColor(ObjectType.DYNAMIC, objectPID, unpack(SELECT_HIGHLIGHT))
				KEP_SetOutlineZSorted(ObjectType.DYNAMIC, objectPID, 0)
				local event = KEP_EventCreate( "UPDATE_FLAG_REGION_COLOR" )
				KEP_EventSetFilter(event, 4)
				KEP_EventEncodeNumber(event, 0)
				KEP_EventEncodeNumber(event, 255)
				KEP_EventEncodeNumber(event, 0)
				KEP_EventQueue(event)
			-- Load exceeded, red
			else
				KEP_SetOutlineState(ObjectType.DYNAMIC, objectPID, 1)
				KEP_SetOutlineColor(ObjectType.DYNAMIC, objectPID, unpack(MOUSEOVER_HIGHLIGHT_RED))
				KEP_SetOutlineZSorted(ObjectType.DYNAMIC, objectPID, 0)
				local event = KEP_EventCreate( "UPDATE_FLAG_REGION_COLOR" )
				KEP_EventSetFilter(event, 4)
				KEP_EventEncodeNumber(event, 255)
				KEP_EventEncodeNumber(event, 0)
				KEP_EventEncodeNumber(event, 0)
				KEP_EventQueue(event)
			end
		-- Regular item, default outline
		else
			KEP_SetOutlineState(ObjectType.DYNAMIC, objectPID, 1)
			KEP_SetOutlineColor(ObjectType.DYNAMIC, objectPID, unpack(SELECT_HIGHLIGHT))
			KEP_SetOutlineZSorted(ObjectType.DYNAMIC, objectPID, 0)
			local event = KEP_EventCreate( "UPDATE_FLAG_REGION_COLOR" )
			KEP_EventSetFilter(event, 4)
			KEP_EventEncodeNumber(event, 0)
			KEP_EventEncodeNumber(event, 255)
			KEP_EventEncodeNumber(event, 0)
			KEP_EventQueue(event)
		end
	-- Unable of building here
	elseif s_isInPlayerMode then
		KEP_SetOutlineState(ObjectType.DYNAMIC, objectPID, 1)
		KEP_SetOutlineColor(ObjectType.DYNAMIC, objectPID, unpack(MOUSEOVER_HIGHLIGHT_RED))
		KEP_SetOutlineZSorted(ObjectType.DYNAMIC, objectPID, 0)
		local event = KEP_EventCreate( "UPDATE_FLAG_REGION_COLOR" )
		KEP_EventSetFilter(event, 4)
		KEP_EventEncodeNumber(event, 255)
		KEP_EventEncodeNumber(event, 0)
		KEP_EventEncodeNumber(event, 0)
		KEP_EventQueue(event)
	end
end

-- Places a ghost at the player's feet
placeGhost = function()

	-- 1234 TO DO: Display volume here

	-- Handle each ghost
	if s_ghostList then
		-- Get the player's position and orientation
		local x, y, z, r = KEP_GetPlayerPosition()
		local radian = math.rad(r)
		local rotX = math.sin(radian)
		local rotZ = math.cos(radian)
		
		local zVector = {x = rotX, y = 0, z = rotZ} 	-- look vector
		local yVector = {x = 0, y = 1, z = 0}			-- up vector
		local xVector = {x = -rotZ, y = 0.0, z = rotX}	-- X = Z x Y
		
		-- Find the lowest z object. Offset by that.
		local lowestItem = PLACEMENT_OFFSET
		if s_placementMode == "Prefab" then
			for i, ghostInfo in pairs(s_ghostList) do
				if ghostInfo.offset.z < lowestItem.z then
					lowestItem = {x = 0, y = 0, z = ghostInfo.offset.z}
				end
			end
			lowestItem.z = lowestItem.z + PLACEMENT_OFFSET.z
		end

		local finalOffset
		local placeX, placeY, placeZ
		for i, ghostInfo in pairs(s_ghostList) do
			-- Calculate the final offset on the ghost
			if ghostInfo.offset then 
				finalOffset = Math.vecAdd(lowestItem, ghostInfo.offset)
			else
				finalOffset = lowestItem
			end
			
			-- Apply the offset
			placeX = x + Math.vecDot(xVector, finalOffset)
			placeY = y + Math.vecDot(yVector, finalOffset)
			placeZ = z + Math.vecDot(zVector, finalOffset)
			
			-- If this ghost has a PID associated with it, position it
			if ghostInfo.PID then
				KEP_SetDynamicObjectPosition(ghostInfo.PID, placeX, placeY, placeZ)
				KEP_SetDynamicObjectRotation(ghostInfo.PID, 0, radian, 0)
				
				local position = {x = placeX, y = y, z = placeZ}
				local worldInfo = getWorldInfoForBuildAction()
	
				local placementAction = BuildActionFactory.createPlacementAction(ghostInfo, position, worldInfo)
				requestAction(placementAction)
			-- Create a PID for this ghost
			else
				ghostInfo.PID = KEP_PlaceLocalDynamicObj(ghostInfo.GLID, 1.0, 1, 0, placeX, placeY, placeZ)
				KEP_DynamicObjectMakeTranslucent(ghostInfo.PID)
				KEP_DynamicObjectEnableCollision(ghostInfo.PID, false)

				--updateHighlightForObject(ghostInfo.PID)
				s_shouldForceHighlightUpdate = true
			end
			
			-- Display the volume mesh
			if ghostInfo.PID then -- ObjectType
				local event = KEP_EventCreate( "FLAG_REGION_DISPLAY_UPDATE" )
				KEP_EventSetFilter(event, 4)
				KEP_EventEncodeNumber(event, ghostInfo.PID) -- PID of object in question
				KEP_EventEncodeNumber(event, 1) -- 1 = selected, 0 = deselected
				KEP_EventEncodeNumber(event, ghostInfo.UNID) -- UNID of object in question (optional parameter, used because PID is negative)
				KEP_EventQueue(event)
			end
		end
	end
end

-- Clears a ghost
clearGhost = function()
	-- Clean up old ghost?
	if s_ghostList then
		for i, ghostInfo in pairs(s_ghostList) do
			-- Select object
			KEP_SelectObject(ObjectType.DYNAMIC, ghostInfo.PID)
			-- Pick up selected item
			--local ev = KEP_EventCreate("PickUpSelectedObjectsEvent")
			--KEP_EventQueue(ev)
			ObjectDel(ghostInfo.PID, ObjectType.DYNAMIC)
			KEP_ClearSelection()
			s_placementMode = nil
			
			-- Hide the volume mesh
			if ghostInfo.PID then -- ObjectType
				local event = KEP_EventCreate( "FLAG_REGION_DISPLAY_UPDATE" )
				KEP_EventSetFilter(event, 4)
				KEP_EventEncodeNumber(event, ghostInfo.PID) -- PID of object in question
				KEP_EventEncodeNumber(event, 0) -- 1 = selected, 0 = deselected
				KEP_EventEncodeNumber(event, ghostInfo.UNID) -- UNID of object in question (optional parameter, used because PID is negative)
				KEP_EventQueue(event)
			end
		end
		s_ghostList = nil
	end

	if s_cameraAutoAdjust == true then
		SetCameraLevel(1)
	end
end

-- Build select the designated PID
selectObject = function(PID)

	--Use this call to get the UNID of the object
	local obj = {}
	obj.name, obj.description, obj.canPlayMovie, obj.isAttachable, obj.interactive, obj.isLocal, obj.derivable, obj.assetId, obj.friendId, obj.customTexture, obj.GLID, obj.invType, obj.textureURL, obj.gameItemId, obj.playerId = KEP_DynamicObjectGetInfo(PID)
	local ev = KEP_EventCreate("FRAMEWORK_SET_SELECTED_OBJECT")
	KEP_EventEncodeNumber(ev, PID)
	KEP_EventEncodeNumber(ev,  obj.gameItemId)
	
	KEP_EventQueue(ev)

	s_selectedObjectPID = PID

	--new PID was selected, make sure to update hightlight we tried to move it
	s_shouldForceHighlightUpdate = true
	s_isBuildingAllowed = true --Mark building allowed on selecting on object. Request action will handle if it can move or not

	-- Safes, Vehicles, Lockable Doors, Local Media Players
	local isSafe = isPlacedObjectMetalSafe(PID)
	local isVehicle = isPlacedObjectVehicle(PID)
	local isDoor = isPlacedObjectLockableDoor(PID)
	local isMedia = isPlacedObjectLocalMediaPlayer(PID)
	local allowsPIN = isSafe or isVehicle or isDoor or isMedia

	s_wasBuildingAllowed = s_isBuildingAllowedInZone

	s_buildLocation[1], s_buildLocation[2], s_buildLocation[3] = KEP_DynamicObjectGetPosition(PID)
	s_buildLocation[4], s_buildLocation[5], s_buildLocation[6] = KEP_DynamicObjectGetRotation(PID)
	
	if s_isInPlayerMode then
		showBuildControls(true, allowsPIN, PID)
	end
end

deselectObject = function(PID)
	handleDeselect()
	local ev = KEP_EventCreate("FRAMEWORK_SET_SELECTED_OBJECT")
	KEP_EventEncodeNumber(ev, 0)
	KEP_EventQueue(ev)
	s_selectedObjectPID = nil

	if PID then
		saveDynamicObject(PID)
	end
	
	if s_isInPlayerMode then
		showBuildControls(false, false, nil)
	end
end


canSelect = function(selectionType, PID)
	local isObject = selectionType == ObjectType.DYNAMIC
	if( isObject ) then
		return canSelectObject(PID)
	end
	return false
end

-- Is the placeable object with the provided ID selectable by the current player?
canSelectObject = function(objectPID)
	local placedObject = s_placedObjects[objectPID]
	if( placedObject ) then
		local isOwned = isPlacedObjectOwned(placedObject)

		--log("--- isOwned ".. tostring(isOwned))

		-- check to make sure this is not a character, then chack if it is owned, then selectable 
		--Also do not allow user to select and move the tutorial teleporters!
		if isPlacedObjectCharacter(objectPID)or isPlacedObjectTutorialTeleporter(objectPID) then
			return false
		elseif isOwned then
			return true
		else
			local isSelectable =	not FlagSystem:isPlaceableObjectLandClaimFlag(objectPID) and 
										FlagSystem:checkLandClaimFlagMembershipForPlayer(s_playerName, objectPID) and 
									not isPlacedObjectMetalSafe(objectPID) and
									not isPlacedObjectSpawned(placedObject)
			return isSelectable
		end
	end
	
	return false
end

onSelectionDenied = function()
	-- if isLastClickedObjectVehicle() then
	-- 	displayStatusMessage("You do not own this vehicle.")
	-- end
end


isLastClickedObjectVehicle = function()
	return s_lastItemBehavior == "wheeled_vehicle"
end


-- Tells you if a given object is owned
isPlacedObjectOwned = function(placedObject)
	return placedObject and placedObject.owner == s_playerName
end

isPlacedObjectSpawned = function(placedObject)
	return placedObject and placedObject.spawned
end


-- Is the placeable object with the given placement ID a Metal Safe?
isPlacedObjectMetalSafe = function(objectPID)
	local	name, 
			description, 
			canPlayMovie, 
			isAttachableObject, 
			isInteractive, 
			isLocal, 
			isDerivable, 
			assetID, 
			friendID, 
			customTexture, 
			globalID, 
			invType, 
			textureURL, 
			gameItemID, 
			playerId = KEP_DynamicObjectGetInfo(objectPID)

	local gameItemEntry = s_gameItems[tostring(gameItemID)]

	return gameItemEntry and gameItemEntry.behavior == "loot" and gameItemEntry.behaviorParams and gameItemEntry.behaviorParams["locked"]
end

-- Is the placeable object with the given placement ID a Metal Safe?
isPlacedObjectTutorialTeleporter = function(objectPID)
	local	name, 
			description, 
			canPlayMovie, 
			isAttachableObject, 
			isInteractive, 
			isLocal, 
			isDerivable, 
			assetID, 
			friendID, 
			customTexture, 
			globalID, 
			invType, 
			textureURL, 
			gameItemID, 
			playerId = KEP_DynamicObjectGetInfo(objectPID)

	local gameItemEntry = s_gameItems[tostring(gameItemID)]

	return gameItemEntry and gameItemEntry.behavior == "teleporter" and gameItemEntry.behaviorParams and gameItemEntry.behaviorParams["specialDestination"]
end

-- Is the placeable object with the given placement ID a vehicle?
isPlacedObjectVehicle = function(objectPID)
	local	name, 
			description, 
			canPlayMovie, 
			isAttachableObject, 
			isInteractive, 
			isLocal, 
			isDerivable, 
			assetID, 
			friendID, 
			customTexture, 
			globalID, 
			invType, 
			textureURL, 
			gameItemID, 
			playerId = KEP_DynamicObjectGetInfo(objectPID)

	local gameItemEntry = s_gameItems[tostring(gameItemID)]

	return gameItemEntry and gameItemEntry.behavior == "wheeled_vehicle"
end

isPlacedObjectCharacter = function (objectPID)	
	local	name, 
			description, 
			canPlayMovie, 
			isAttachableObject, 
			isInteractive, 
			isLocal, 
			isDerivable, 
			assetID, 
			friendID, 
			customTexture, 
			globalID, 
			invType, 
			textureURL, 
			gameItemID, 
			playerId = KEP_DynamicObjectGetInfo(objectPID)

	local gameItemEntry = s_gameItems[tostring(gameItemID)]

	--log("--- gameItemEntry.behavior ".. tostring(gameItemEntry.behavior))

	return gameItemEntry and (gameItemEntry.behavior == "actor" or 
		gameItemEntry.behavior == "vendor" or 
		gameItemEntry.behavior == "clothing" or 
		gameItemEntry.behavior == "creditsVendor" or 
		gameItemEntry.behavior == "gemVendor" or 
		gameItemEntry.behavior == "randomVendor" or 
		gameItemEntry.behavior == "timedVendor" or
		gameItemEntry.behavior == "quest_giver" or
		gameItemEntry.behavior == "shop_vendor")
end

-- Is the placeable object with the given placement ID a local media player?
isPlacedObjectLocalMediaPlayer = function(objectPID)
	local	name, 
			description, 
			canPlayMovie, 
			isAttachableObject, 
			isInteractive, 
			isLocal, 
			isDerivable, 
			assetID, 
			friendID, 
			customTexture, 
			globalID, 
			invType, 
			textureURL, 
			gameItemID, 
			playerId = KEP_DynamicObjectGetInfo(objectPID)

	local gameItemEntry = s_gameItems[tostring(gameItemID)]

	return gameItemEntry and gameItemEntry.behavior == "media"
end

-- Is the placeable object with the given placement ID a lockable door?
isPlacedObjectLockableDoor = function(objectPID)
	local	name, 
			description, 
			canPlayMovie, 
			isAttachableObject, 
			isInteractive, 
			isLocal, 
			isDerivable, 
			assetID, 
			friendID, 
			customTexture, 
			globalID, 
			invType, 
			textureURL, 
			gameItemID, 
			playerId = KEP_DynamicObjectGetInfo(objectPID)

	local gameItemEntry = s_gameItems[tostring(gameItemID)]

	return gameItemEntry and gameItemEntry.behavior == "door" and gameItemEntry.behaviorParams["locked"]
end

-- Is the placeable object with the given placement ID a lockable door?
getPlacedObjectName = function(objectPID)
	local	name, 
			description, 
			canPlayMovie, 
			isAttachableObject, 
			isInteractive, 
			isLocal, 
			isDerivable, 
			assetID, 
			friendID, 
			customTexture, 
			globalID, 
			invType, 
			textureURL, 
			gameItemID, 
			playerId = KEP_DynamicObjectGetInfo(objectPID)

	local gameItemEntry = s_gameItems[tostring(gameItemID)]
	if gameItemEntry and gameItemEntry.name then
		return gameItemEntry.name
	else
		return ""
	end
end
	
requestAction = function(action)
	
	-- Check if the action requested is permitted
	s_noBuildReason = action:checkActionPermissibility()
	
	s_isBuildingAllowed = s_noBuildReason == nil
	-- Check for change in build setting
	if s_wasBuildingAllowed ~= s_isBuildingAllowed or s_shouldForceHighlightUpdate then
		-- If a ghost is offending, PlayerBuild will handle it
		if s_ghostList then
			updateHighlightForObject(action.objectPID)
		-- All other highlights are handled by Framework_Cursor_HighlightHandler
		else
			local ev = KEP_EventCreate("FRAMEWORK_OBJECT_OFFENDING_FLAG")
			KEP_EventEncodeNumber(ev, action.objectPID)
			KEP_EventEncodeString(ev, tostring(s_isBuildingAllowed))
			KEP_EventQueue(ev)
		end

		-- Sending event from playerbuild to toolbar for build mode change
		local ev = KEP_EventCreate("FRAMEWORK_BUILD_SETTING")
		KEP_EventEncodeString(ev, tostring(s_isBuildingAllowed))
		KEP_EventEncodeString(ev, s_noBuildReason or "")
		KEP_EventQueue(ev)

		-- Set old build setting to current
		s_wasBuildingAllowed = s_isBuildingAllowed
		s_shouldForceHighlightUpdate = false
	end
end


showBuildControls = function(show, allowsPIN, selectedPID)
	if not show then
		changeBuildMode(false) -- Hide widget

		-- Only close the PlaceableObj menu, we open it through the button now
		if MenuIsOpen("PlaceableObj.xml") then
			MenuClose("PlaceableObj.xml")
		end
	end
	
		local selectedObjectCount = KEP_GetNumSelected()
		local BUILD_HELP_MENU = "BuildHelp.xml"
		
		if( show and selectedObjectCount > 0 ) then
			MenuOpen(BUILD_HELP_MENU)
		else
			if MenuIsOpen(BUILD_HELP_MENU) then 
				MenuClose(BUILD_HELP_MENU)
			end
		end

	local showNum = show and 1 or 0
	local showEvent = KEP_EventCreate("FRAMEWORK_SHOW_BUILD_TOOLS")

	KEP_EventEncodeNumber(showEvent, showNum) -- Whether or not to show build tools
	KEP_EventEncodeString(showEvent, tostring(allowsPIN)) -- Whether or not to show PIN
	KEP_EventEncodeNumber(showEvent, tonumber(selectedPID)) -- PID of selected object
	KEP_EventEncodeString(showEvent, getPlacedObjectName(tonumber(selectedPID))) -- Name of selected object
	KEP_EventQueue(showEvent)
	
end

saveDynamicObject = function(PID)
	local obj = {}
	obj.name, obj.description, obj.canPlayMovie, obj.isAttachable, obj.interactive, obj.isLocal, obj.derivable, obj.assetId, obj.friendId, obj.customTexture, obj.GLID, obj.invType, obj.textureURL, obj.gameItemId, obj.playerId = KEP_DynamicObjectGetInfo(PID)

	-- PJD - KEP_DynamicObjectGetInited check inserted to resolve a potential race condition when placing objects.  Don't attempt to save if this object is not initialized on the client yet.
	if not obj.GLID or KEP_DynamicObjectGetInited(PID) == 0 then
		return
	end

	-- Get the object position and rotation on client to send to the framework
	local objPos = {}
	local radian = 0
	objPos.x, objPos.y, objPos.z = KEP_DynamicObjectGetPosition(PID)
	_, radian, _ = KEP_DynamicObjectGetRotation(PID)
	
	objPos.rx = math.cos(radian)
	objPos.ry = 0
	objPos.rz = -math.sin(radian)

	Events.sendEvent("FRAMEWORK_DYNAMIC_OBJECT_SAVE_SERVER", {PID = PID, UNID = obj.gameItemId, objPos = objPos})
end

changeBuildMode = function(mode)

--	if mode == MODE_MOVE then
--		KEP_DisplayTranslationWidget(ObjectType.DYNAMIC)
--	elseif mode == MODE_ROTATE then
--		KEP_DisplayRotationWidget(ObjectType.DYNAMIC)
--	else
--		-- Turn Off Build Widget
--		KEP_TurnOffWidget()
--	end

	-- DRF - Added - Must Request Selection.lua To Change Build Modes!
	local e = KEP_EventCreate(CHANGE_BUILD_MODE_EVENT)
	KEP_EventSetFilter(e, mode)
	KEP_EventQueue(e)
end

-- Get the squared distance of the object
isObjectWithinInteractionRange = function(PID)
	-- CJW, NOTE: Multiplication is more efficient than taking the power or squarerooting
	local objLoc = {KEP_DynamicObjectGetPositionAnim(PID)}
	local playerLoc = {KEP_GetPlayerPosition()}
	
	local xDiff = objLoc[1] - playerLoc[1]
	local yDiff = objLoc[2] - playerLoc[2]
	local zDiff = objLoc[3] - playerLoc[3]
	local distanceSqrd = xDiff * xDiff + yDiff * yDiff + zDiff * zDiff
	
	return distanceSqrd <= MIN_INTERACT_DISTANCE_SQRD
end


adjustCamera = function()
    local cam = KEP_GetPlayersActiveCamera()
    local rotXZ = GetSet_GetNumber( cam, CameraIds.ORBITAZIMUTH)
    local rotY = GetSet_GetNumber(cam, CameraIds.ORBITELEVATION )
    if  math.abs(rotXZ) < CAMERA_BUFFER then
        if rotY < PLAYER_BUILD_CAMERA then
            --Temporarily  disable auto adjust
            local cameraLevel = KEP_GetPlayerCameraAutoElevation()
            SetCameraLevel(0)
            s_cameraAutoAdjust = cameraLevel < CAM_DISABLED_ADJUST_ANGLE
            --Adjust camera
            local cam = KEP_GetPlayersActiveCamera()
            GetSet_SetNumber( cam, CameraIds.ORBITAZIMUTH, 0 )
            GetSet_SetNumber( cam, CameraIds.ORBITELEVATION, PLAYER_BUILD_CAMERA )
        end
    end
end