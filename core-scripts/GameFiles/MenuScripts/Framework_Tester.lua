--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_Tester.lua
--
-- Interfaces with Framework_Test.lua
--
-- Is open by default in all Framework-enabled worlds.
--
-- Commands
-- Activate / Deactivate: 			Ctrl+Shift+F
-- Run Automated test:	  			Ctrl+Shift+S
-- Run Manual test:		  			Ctrl+Shift+M (Not implemented)
-- List classes to manually report: Ctrl+Shift+L
-- Summary report:					Ctrl+Shift+R
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

g_activated = false
g_testComplete = false

local KEY_TEST = 84 -- 'T'
local KEY_ESC = 27

-- Local functions
local log
local showMenu
local printReport

g_errorReport = {}
g_passCount = 0
g_failCount = 0
g_totalTests = 0

-- Handles generated for each class
g_genHandles = {}
g_classes = {}
g_lastTest = nil

local g_testPID

local g_handlesGenerated = false

-- When the menu is created
function onCreate()
	Events.registerHandler("FRAMEWORK_TEST_OBJECT_GENERATED", testObjectGenerated)
	Events.registerHandler("FRAMEWORK_DEBUGGER", debugEvent)
	
	showMenu(false)
end

function activate()
	g_activated = true
	Log("Activated")
	Log("Ctrl+Shift+F or ESC - Deactive debugger")
			
	showMenu(true)
			
	-- Generate test object
	if g_testPID == nil then
		Events.sendEvent("FRAMEWORK_TEST_INIT", {})
	end
end

function deactivate()
	g_activated = false
	Log("De-activated")
	Log("Ctrl+Shift+F - Re-activate debugger")
	
	showMenu(false)
			
	-- Destroy test object
	if g_testPID then
		KEP_SelectObject(2, g_testPID)
		-- Pick up selected item
		KEP_EventCreateAndQueue("PickUpSelectedObjectsEvent")
		g_testPID = nil
	end

	Events.sendEvent("FRAMEWORK_TEST_CLEANUP", {})
end

-- When the menu is moved
function Dialog_OnMoved(dialogHandle, x, y)
	if g_activated == false then
		showMenu(false)
	end
end

-- Called when a key is pressed
function Dialog_OnKeyDown(dialogHandle, key, shift, ctrl, alt)
	-- Activate or de-active framework debugger
	if key == KEY_TEST and shift and ctrl then
		if g_activated then
			deactivate()
		else
			activate()
		end
	
	-- ESC key
	elseif key == KEY_ESC and g_activated then
		deactivate()
	end
end

---------------------
-- Button Handlers
---------------------

-- Start
function btnStart_OnButtonClicked(buttonHandle)
	-- Need to reset before testing all?
	if g_testComplete then
		Control_SetEnabled(gHandles["btnReport"], false)
		-- Reset class displays
		for i=1, #g_classes do
			local className = g_classes[i].name
			Static_SetText(g_genHandles[className].result, "0/"..tostring(g_classes[i].totalTests))
			Static_SetText(g_genHandles[className].success, "- - -")
			Control_SetEnabled(g_genHandles[className].logButton, false)
			Static_SetText(Button_GetStatic(g_genHandles[className].logButton), "Test")
			
			g_errorReport[className].Fail = {}
			g_errorReport[className].Pass = {}
			g_errorReport[className].tested = 0
			g_errorReport[className].passed = 0
			g_errorReport[className].failed = 0
		end
	end
	g_testComplete = false
	Events.sendEvent("BEGIN_TEST", {test = "All"})
end

-- Report
function btnReport_OnButtonClicked(buttonHandle)
	for class, data in pairs(g_errorReport) do
		printReport(class)
	end
end

-- Cancel
function btnCancel_OnButtonClicked(buttonHandle)
	deactivate()
end

-- Reset
function btnReset_OnButtonClicked(buttonHandle)
	-- Reset all tests
	Control_SetEnabled(gHandles["btnReport"], false)
	Control_SetEnabled(buttonHandle, false)
	-- Reset class displays
	for i=1, #g_classes do
		local className = g_classes[i].name
		Static_SetText(g_genHandles[className].result, "0/"..tostring(g_classes[i].totalTests))
		Static_SetText(g_genHandles[className].success, "- - -")
		Control_SetEnabled(g_genHandles[className].logButton, true)
		Static_SetText(Button_GetStatic(g_genHandles[className].logButton), "Test")
		
		g_errorReport[className].Fail = {}
		g_errorReport[className].Pass = {}
		g_errorReport[className].tested = 0
		g_errorReport[className].passed = 0
		g_errorReport[className].failed = 0
		g_errorReport[className].mode = "Test"
	end
end

---------------------
-- Event Handlers
---------------------

-- Test object generated - save PID
function testObjectGenerated(event)
	g_testPID = event.PID
	Log("Test PID = " .. tostring(g_testPID))
end

function createStaticControl(name, caption, rect)
    local handle = Dialog_AddStatic(gDialogHandle, name, caption, rect.x, rect.y, rect.w, rect.h)
    local elem = Static_GetDisplayElement(handle)
    Element_AddFont(elem, gDialogHandle, "Verdana", -12, 100, false)
    Element_SetTextFormat(elem, TEXT_HALIGN_LEFT + TEXT_VALIGN_CENTER)
    return handle
end

function createButtonControl(name, caption, rect)
    local handle = Dialog_AddButton(gDialogHandle, name, caption, rect.x, rect.y, rect.w, rect.h)
    local elem
    elem = Button_GetNormalDisplayElement(handle)
    Element_AddFont(elem, gDialogHandle, "Verdana", -12, 100, false)
    elem = Button_GetDisabledDisplayElement(handle)
    Element_AddFont(elem, gDialogHandle, "Verdana", -12, 100, false)
    elem = Button_GetFocusedDisplayElement(handle)
    Element_AddFont(elem, gDialogHandle, "Verdana", -12, 100, false)
    elem = Button_GetPressedDisplayElement(handle)
    Element_AddFont(elem, gDialogHandle, "Verdana", -12, 100, false)
    elem = Button_GetMouseOverDisplayElement(handle)
    Element_AddFont(elem, gDialogHandle, "Verdana", -12, 100, false)
    return handle
end

-- Receives events from the server
function debugEvent(event)
	-- List of classes to be tested
	if event.type == "classList" and g_handlesGenerated == false then
		Log(">>> classList with " .. tostring(#event.classes) .. " items")
		g_classes = event.classes
		
		-- Generate classes for each class
		for i=1, #g_classes do
            -- 16 items each column
            local columnTag = tostring(math.floor((i + 15) / 16))
            local yofs = 25 * ((i - 1) % 16 + 1)
            local ctlHeading, rect
            
            ctlHeading = Dialog_GetControl(gDialogHandle, "stcTestNameH"..columnTag)
			rect = { x = Control_GetLocationX(ctlHeading), y = Control_GetLocationY(ctlHeading) + yofs, w = Control_GetWidth(ctlHeading), h = Control_GetHeight(ctlHeading) }
			local stcTestName = createStaticControl("stcTestName"..tostring(i), tostring(g_classes[i].name), rect)

            ctlHeading = Dialog_GetControl(gDialogHandle, "stcResultH"..columnTag)
			rect = { x = Control_GetLocationX(ctlHeading), y = Control_GetLocationY(ctlHeading) + yofs, w = Control_GetWidth(ctlHeading), h = Control_GetHeight(ctlHeading) }
			local stcResult = createStaticControl("stcResult"..tostring(i), "0/"..tostring(g_classes[i].totalTests), rect)

            ctlHeading = Dialog_GetControl(gDialogHandle, "stcSuccessH"..columnTag)
			rect = { x = Control_GetLocationX(ctlHeading), y = Control_GetLocationY(ctlHeading) + yofs, w = Control_GetWidth(ctlHeading), h = Control_GetHeight(ctlHeading) }
			local stcSuccess = createStaticControl("stcSuccess"..tostring(i), "- - -", rect)

            rect = { x = rect.x + rect.w, y = rect.y + 5, w = rect.w, h = 25 }
			local btnReport = createButtonControl("btnReport"..tostring(i), "Test", rect)

			-- Generate button handler for this button
			_G["btnReport"..tostring(i).."_OnButtonClicked"] = function(btnHandle)
																	local s, e, n = string.find(Control_GetName(btnHandle), "(%d+)")
																	-- Test individual class
																	if g_errorReport[g_classes[tonumber(n)].name].mode == "Test" then
																		g_lastTest = nil
																		Events.sendEvent("BEGIN_TEST", {test = g_classes[tonumber(n)].name})
																	-- Report this class
																	elseif g_errorReport[g_classes[tonumber(n)].name].mode == "Report" then
																		printReport(g_classes[tonumber(n)].name)
																	end
															   end

			g_genHandles[g_classes[i].name] = {class = stcTestName, result = stcResult, success = stcSuccess, logButton = btnReport}
			g_errorReport[g_classes[i].name] = {Fail = {}, Pass = {}, totalTests = g_classes[i].totalTests, tested = 0, passed = 0, failed = 0, mode = "Test"}
		end
		
		g_handlesGenerated = true
	
	-- Test has begun
	elseif event.type == "testBegin" then
		Log(">>> testBegin " .. tostring(event.class))
		-- Do we have a previous class to cleanup before moving on?
		if g_lastTest then
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[g_lastTest].class)), 0, {a=255, r=255, g=255, b=255})
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[g_lastTest].result)), 0, {a=255, r=255, g=255, b=255})
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[g_lastTest].success)), 0, {a=255, r=255, g=255, b=255})
			
			-- Prepare previously tested log button to be reported with once test is complete
			Static_SetText(Button_GetStatic(g_genHandles[g_lastTest].logButton), "Report")
			g_errorReport[g_lastTest].mode = "Report"
			
			-- Report success percent
			if g_errorReport[g_lastTest].totalTests > 0 then
				local successPercent = (g_errorReport[g_lastTest].passed / g_errorReport[g_lastTest].totalTests) * 100
				Static_SetText(g_genHandles[g_lastTest].success, tostring(successPercent).."%")
			end
		end
		g_lastTest = event.class
		BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[g_lastTest].class)), 0, {a=255, r=255, g=0, b=0})
		BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[g_lastTest].result)), 0, {a=255, r=255, g=0, b=0})
		BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[g_lastTest].success)), 0, {a=255, r=255, g=0, b=0})
		
		Static_SetText(Button_GetStatic(g_genHandles[g_lastTest].logButton), "Testing")
		Control_SetEnabled(g_genHandles[g_lastTest].logButton, false)
	
	-- Report test result
	elseif event.type == "reportResult" then
		local class    = event.class
		local funcName = event.funcName
		local result   = event.result
		local details  = event.details

		g_errorReport[class].tested = g_errorReport[class].tested + 1
		Static_SetText(g_genHandles[class].result, tostring(g_errorReport[class].tested).."/"..tostring(g_errorReport[class].totalTests))
		if result then
			result = "Pass"
			g_errorReport[class].passed = g_errorReport[class].passed + 1
		else
			g_errorReport[class].failed = g_errorReport[class].failed + 1
			result = "Fail"
		end
		table.insert(g_errorReport[class][result], {funcName = funcName, details = details})

	-- Test complete
	elseif event.type == "testComplete" then
		local test = event.test
		-- Complete test concluded
		if test == "All" then
			if g_lastTest then
				-- Set last tested class back to white
				BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[g_lastTest].class)), 0, {a=255, r=255, g=255, b=255})
				BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[g_lastTest].result)), 0, {a=255, r=255, g=255, b=255})
				BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[g_lastTest].success)), 0, {a=255, r=255, g=255, b=255})
				
				Static_SetText(Button_GetStatic(g_genHandles[g_lastTest].logButton), "Report")
				g_errorReport[g_lastTest].mode = "Report"
				
				-- Report success percent
				if g_errorReport[g_lastTest].totalTests > 0 then
					local successPercent = (g_errorReport[g_lastTest].passed / g_errorReport[g_lastTest].totalTests) * 100
					Static_SetText(g_genHandles[g_lastTest].success, tostring(successPercent).."%")
				end
				g_lastTest = nil
			end
			
			-- Enable all report buttons
			for class, data in pairs(g_genHandles) do
				Control_SetEnabled(data.logButton, true)
			end
		
			-- Ready to test again
			Control_SetEnabled(gHandles["btnReport"], true)
			-- Reset
			Control_SetEnabled(gHandles["btnReset"], true)
			
			-- Destroy test object
			if g_testPID then
				KEP_SelectObject(2, g_testPID)
				-- Pick up selected item
				KEP_EventCreateAndQueue("PickUpSelectedObjectsEvent")
				g_testPID = nil
			end
			
		-- Custom test concluded
		elseif g_genHandles[test] and g_errorReport[test] then
			-- Set last tested class back to white
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[test].class)), 0, {a=255, r=255, g=255, b=255})
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[test].result)), 0, {a=255, r=255, g=255, b=255})
			BlendColor_SetColor(Element_GetFontColor(Static_GetDisplayElement(g_genHandles[test].success)), 0, {a=255, r=255, g=255, b=255})
			
			-- Report success percent
			if g_errorReport[test].totalTests > 0 then
				local successPercent = (g_errorReport[test].passed / g_errorReport[test].totalTests) * 100
				Static_SetText(g_genHandles[test].success, tostring(successPercent).."%")
			end
			g_lastTest = nil
			
			Static_SetText(Button_GetStatic(g_genHandles[test].logButton), "Report")
			g_errorReport[test].mode = "Report"
			Control_SetEnabled(g_genHandles[test].logButton, true)
			
		end
		-- Reset
		Control_SetEnabled(gHandles["btnReset"], true)
		
		g_testComplete = true
	
	-- Log message
	elseif event.type == "log" then
		local message = event.message
		Log(tostring(event.message))
	end
end

---------------------
-- Helper functions
---------------------

log = function(message)
	Log(tostring(message))
end

-- Reveal the menu or not
showMenu = function(state)
	for name, handle in pairs(gHandles) do
		Control_SetVisible(handle, state)
	end
	for class, data in pairs(g_genHandles) do
		for name, handle in pairs(data) do
			Control_SetVisible(handle, state)
		end
	end
	if state then
		MenuSetSizeThis(800, 500)
		MenuSetLocationThis((Dialog_GetScreenWidth(gDialogHandle)/2 - Dialog_GetWidth(gDialogHandle)/2),
							(Dialog_GetScreenHeight(gDialogHandle)/2 - Dialog_GetHeight(gDialogHandle)/2))
	else
		MenuSetSizeThis(1, 1)
		MenuSetLocationThis(-5000, -5000)
	end
end

-- Prints error report
printReport = function(class)
	if g_errorReport[class] then
		Log("UNIT TEST REPORT - "..tostring(class))
		if #g_errorReport[class].Fail > 0 then
			for i=1, #g_errorReport[class].Fail do
				Log("	"..tostring(g_errorReport[class].Fail[i].funcName).."() failed - "..tostring(g_errorReport[class].Fail[i].details))
			end
		else
			Log("	No errors exist.")
			local tested
			for i=1, #g_errorReport[class].Pass do
				if tested == nil then
					tested = tostring(g_errorReport[class].Pass[i].funcName).."()"
				else
					tested = tested..", "..tostring(g_errorReport[class].Pass[i].funcName).."()"
				end
			end
			Log("	Tested functions: "..tostring(tested))
		end
	else
		Log(tostring(class).." doesn't have an errorReport table")
	end
end
