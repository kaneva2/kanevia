--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_LinkedZones.lua
--UI for created and editing Linked Zones
-- Copyright 2013 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("Framework_InventoryHelper.lua")
dofile("..\\MenuScripts\\Lib_GameItemBootstrap.lua")
dofile("HUDHelper.lua")

local ITEMS_PER_PAGE = 10

local NEW_PLUS_OFFSET_X = 24
local NEW_PLUS_OFFSET_Y = 8
local NEW_STATIC_OFFSET_X = 7
local NEW_STATIC_OFFSET_Y = 40

local MAIN_SETTINGS_GLID = 4628803
local ENVIRONMENT_GLID = 4628826
local WELCOME_GLID = 4676146

local VIP_ZONES = 5
local VALID_ZONE_TYPE = 6

local BUTTON_TELEPORT_LOCATION_X = { PARENT = 160, CHILD = 240}

local DEFAULT_SETTINGS = {destructibleObjects = true,
					allowPvP 			= true,
					privacy				= "Public",
					playerBuild			= true,
					playerNames			= false,
					frameworkEnabled	= false,
					jumpsuit			= false,
					starvation			= false,
					starveTime			= 20,
					leaderboard			= false,
					dropItems			= false,
					lobbySpawnOnly		= false,
					respawnTimerOff		= false,
					itemType   			= "Child Zone",
					GLID 				= MAIN_SETTINGS_GLID
				   }

local PRIVACY_SETTINGS = {"Open", "Private", "Owner Only"}
local ALLOW_ALL			= 1
local ALLOW_PRIVATE		= 2
local ALLOW_OWNER_ONLY	= 3

local m_tZoneDisplay = {}

local m_worldCheckBox

local m_iZoneCount = 1
local m_iSelectedZone = 1
local m_iNewZoneIndex = nil
local m_tNewChildSettings = nil

local m_iZoneInstanceID = ""
local m_iZoneType = ""
local m_iParentZoneInstanceID = ""
local m_iParentZoneType = ""

local m_iAvailableZones = 2
local m_bVIPPass = false

local m_editorName = {[ITEM_TYPES.MAIN_SETTINGS ] = "Main Settings",
					  [ITEM_TYPES.ENVIRONMENT_SETTINGS ] = "Environment Settings",
					  [ITEM_TYPES.WELCOME_SETTINGS] = "Welcome Menu",
					  Zone = "New Child Zone"

					}

-- When the menu is created
function onCreate()


	KEP_EventCreateAndQueue("FRAMEWORK_GET_PARENT_INFO")
	--KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( "FRAMEWORK_NEW_CHILD_ZONE", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_BUY_NEW_CHILD", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_DELETE_CHILD", KEP.HIGH_PRIO)
	KEP_EventRegister( "CONFIRM_CREDIT_ZONE_TRANSACTION", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler("textureDownloadHandler", "LooseTextureDownloadEvent", KEP.LOW_PRIO)
	KEP_EventRegisterHandler("newZoneHandler", "FRAMEWORK_NEW_CHILD_ZONE", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler("parentInfoHandler", "FRAMEWORK_RETURN_PARENT_INFO", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "responseAccessPassInfoHandler", 	"ResponseAccessPassInfoEvent", 	KEP.MED_PRIO )
	KEP_EventRegisterHandler( "creditTransactionHandler", 	"CONFIRM_CREDIT_ZONE_TRANSACTION", 	KEP.MED_PRIO )


	-- Register events from the server
	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS", returnSettingsHandler)
	Events.registerHandler("FRAMEWORK_RETURN_ENVIRONMENT", returnEnvironmentHandler)
	Events.registerHandler("FRAMEWORK_RETURN_WELCOME", returnWelcomeHandler)
	Events.registerHandler("FRAMEWORK_RETURN_CHILD_SETTINGS", returnSettingsHandler)
	Events.registerHandler("FRAMEWORK_RETURN_CHILD_ENVIRONMENT", returnEnvironmentHandler)
	Events.registerHandler("FRAMEWORK_RETURN_CHILD_WELCOME", returnWelcomeHandler)

	Events.registerHandler("FRAMEWORK_RETURN_OVERVIEW", returnOverviewHandler)

	Events.sendEvent("FRAMEWORK_REQUEST_OVERVIEW")

	m_worldCheckBox = Dialog_GetControl(gDialogHandle, "chkViewGameSystem")

	m_iCurrentZoneID = KEP_GetZoneInstanceId() 
	m_iZoneInstanceID = m_iCurrentZoneID
	m_iCurrentZoneType = KEP_GetZoneIndexType()
	m_iZoneType = m_iCurrentZoneType

	for i = 1, ITEMS_PER_PAGE do
		_G["btnZone" .. tostring(i) .. "_OnMouseEnter"] = function(btnHandle)
			if i == m_iZoneCount then
				Control_SetVisible(gHandles["imgPlusIconGlow"], true)
				Control_SetLocation(gHandles["imgPlusIconGlow"], Control_GetLocationX(gHandles["imgPlusIcon"]),  Control_GetLocationY(gHandles["imgPlusIcon"]))
			end
		end

		_G["btnZone" .. tostring(i) .. "_OnMouseLeave"] = function(btnHandle)
			if i == m_iZoneCount then
				Control_SetVisible(gHandles["imgPlusIconGlow"], false)
			end
		end

		_G["btnZone" .. tostring(i) .. "_OnButtonClicked"] = function(btnHandle)
			handleAction(i)
		end

		if i ~= 1 then
			if KEP_IsOwner() then
				Control_SetToolTip(gHandles["imgZoneLock"..i], "Additional Zones can be purchased for 1000 credits each")
			else
				Control_SetToolTip(gHandles["imgZoneLock"..i], "Only Owners can add zones")
			end
		end
	end
	

	Control_SetPassthrough(gHandles["imgSelected"], 1)
end

function onDestroy()
	InventoryHelper.destroy()
end

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	KEP_EventCreateAndQueue("FRAMEWORK_ALIGN_INVENTORY")
	InventoryHelper.onButtonDown(x, y)
end

-------------------------------------------------
-------------------------------------------------
-- Control Functions
-------------------------------------------------
-------------------------------------------------

function chkViewGameSystem_OnCheckBoxChanged( checkboxHandle, checked )
	local ev = KEP_EventCreate("UNIFIED_GOTO_SYSTEM")
	
	KEP_EventEncodeString(ev, "wokGaming")
	KEP_EventQueue(ev)

	local event = KEP_EventCreate("FRAMEWORK_WORLD_GAME_MODE")
	KEP_EventEncodeNumber(event, checked)
	KEP_EventQueue(event)
end

function btnSettings_OnButtonClicked(buttonHandle)
	requestPrivacySettings()
	
end

function btnEnv_OnButtonClicked(buttonHandle)
	editWorldItem(s_tEnvironmentSettings, "environmentSettings")
end

function btnWelcome_OnButtonClicked(buttonHandle)
	editWorldItem(s_tWelcomeSettings, "welcomeSettings")
end

function btnOverview_OnButtonClicked(buttonHandle)
	
	editWorldItem(s_tOverviewSettings, "overviewSettings")
end

function btnTeleport_OnButtonClicked(buttonHandle)
	if m_tZoneDisplay[m_iSelectedZone].url then
		gotoURL(m_tZoneDisplay[m_iSelectedZone].url)
	end
end

function btnDelete_OnButtonClicked(buttonHandle)
	MenuOpen("Framework_LinkedZoneConfirm.xml")
	local event = KEP_EventCreate("FRAMEWORK_DELETE_CHILD")
	local zone = m_tZoneDisplay[m_iSelectedZone]
	KEP_EventEncodeString(event,  zone.name)
	KEP_EventEncodeString(event, zone.thumbnail)
	KEP_EventEncodeNumber(event, zone.zoneID)
	KEP_EventEncodeNumber(event, zone.zoneType)
	KEP_EventQueue(event)
	--local url = GameGlobals.WEB_SITE_PREFIX..DELETE_LINKED_ZONE.."&zoneInstanceID="..tostring(m_tZoneDisplay[m_iSelectedZone].zoneID).."&zoneType="..tostring(m_tZoneDisplay[m_iSelectedZone].zoneType)
	--makeWebCall(url, WF.LINKED_ZONES_DELETE)
end

function btnWebProfile_OnMouseEnter( buttonHandle )
	setControlEnabledVisible("imgWebGlow", true)
end

function btnWebProfile_OnMouseLeave( buttonHandle )
	setControlEnabledVisible("imgWebGlow", false)
end

function btnWebProfile_OnButtonClicked(buttonHandle)
	KEP_LaunchBrowser( GameGlobals.WEB_SITE_PREFIX_KANEVA .."channel/"..m_tZoneDisplay[m_iSelectedZone].name..".channel" )
end

function handleAction(index)
	if index == m_iZoneCount and index ~= m_iSelectedZone  then
		if m_iAvailableZones >= m_iZoneCount then
			local newWorldSettings = DEFAULT_SETTINGS
			newWorldSettings.name = m_tZoneDisplay[1].name.." Zone "..tostring(m_iZoneCount - 1)
			newWorldSettings.description = "Child zone for ".. m_tZoneDisplay[1].name
			newWorldSettings.itemType = "Zone"
			newWorldSettings.parentID = m_tZoneDisplay[1].zoneID
			editWorldItem(newWorldSettings, "childZone")
		else
			local config = KEP_ConfigOpenWOK()
			if config ~= nil and config ~= 0 then
				local playerName = KEP_GetLoginName()
				local ok, hideChildMenu =  KEP_ConfigGetNumber(config, "HideAddChildMenu"..tostring(playerName), 0)
				if hideChildMenu == 1 then 
					local event = KEP_EventCreate("UPDATE_CREDIT_TRANSACTION")
					KEP_EventEncodeString(event, 1000)
					KEP_EventEncodeString(event, playerName)
					KEP_EventEncodeString(event, m_tZoneDisplay[1].name)
					KEP_EventEncodeString(event, "Zone")
					KEP_EventQueue(event)
					toggleMenu("CreditTransaction")
				else
					MenuOpen("Framework_LinkedZoneConfirm.xml")
					local event = KEP_EventCreate("FRAMEWORK_BUY_NEW_CHILD")
					KEP_EventEncodeString(event,  m_tZoneDisplay[1].name)
					KEP_EventQueue(event)
				end
			end
		end
	else
		setSelectedWorld(index)
	end
end

function setSelectedWorld(index)
	Control_SetLocation(gHandles["imgSelected"], Control_GetLocationX(gHandles["btnZone"..tostring(index)]),  Control_GetLocationY(gHandles["btnZone"..tostring(index)]))
	Static_SetText(gHandles["stcWorldName"], m_tZoneDisplay[index].name)
	if index == 1 or (tostring(m_tZoneDisplay[index].zoneID) == tostring(m_iCurrentZoneID)) or not KEP_IsOwner() then
		setControlEnabledVisible("btnDelete", false)
		Control_SetLocationX(gHandles["btnTeleport"], BUTTON_TELEPORT_LOCATION_X.PARENT)
	else
		Control_SetLocationX(gHandles["btnTeleport"], BUTTON_TELEPORT_LOCATION_X.CHILD)
		setControlEnabledVisible("btnDelete", true)
	end
	m_iSelectedZone = index
	m_iZoneType =  m_tZoneDisplay[index].zoneType
	m_iZoneInstanceID =  m_tZoneDisplay[index].zoneID

	enableSettingsControls("Settings", false)	
	enableSettingsControls("Env", false)	
	enableSettingsControls("Welcome", false)	

	Events.sendEvent("FRAMEWORK_REQUEST_ENVIRONMENT",  {zoneID = tonumber(m_iZoneInstanceID), zoneType = tonumber(m_iZoneType)})
	Events.sendEvent("FRAMEWORK_REQUEST_WELCOME",  {zoneID = tonumber(m_iZoneInstanceID), zoneType = tonumber(m_iZoneType)})
	Events.sendEvent("FRAMEWORK_REQUEST_SETTINGS", {zoneID = tonumber(m_iZoneInstanceID), zoneType = tonumber(m_iZoneType)})

	setControlEnabledVisible("btnWebProfile", true)
	setControlEnabledVisible("imgWeb", true)
end

-------------------------------------------------
-------------------------------------------------
-- Web Functions
-------------------------------------------------
-------------------------------------------------

--For this instance, we are just going to count the current world at parent. This will change with api....i hope
function requestParentZoneData()
	if m_iParentZoneType == 0 or m_iParentZoneInstanceID == 0 then
		m_iParentZoneInstanceID = m_iCurrentZoneID
		m_iParentZoneType = m_iCurrentZoneType
	end
	local url = GameGlobals.WEB_SITE_PREFIX..GET_LINKED_ZONES_SUFFIX.."&zoneInstanceID="..tostring(m_iParentZoneInstanceID).."&zoneType="..tostring(m_iParentZoneType)
	makeWebCall(url, WF.LINKED_ZONES)
end

function requestPrivacySettings( )
	local url = GameGlobals.WEB_SITE_PREFIX.. GET_ACCESS_SUFFIX.."&gameId="..tostring(KEP_GetGameId()).."&zoneInstanceID="..tostring(m_iZoneInstanceID).."&zonetype="..tostring(m_iZoneType)
	makeWebCall(url, WF.GET_ZONE_ACCESS)
end

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	if filter == WF.LINKED_ZONES then
		local estr = KEP_EventDecodeString( tEvent )
		ParseLocationData(estr)
	elseif filter == WF.LINKED_ZONES_ADD then
		local estr = KEP_EventDecodeString( tEvent )
		local s = 0 		-- start and end index of substring
		local strPos = 0    	-- current position in string to parse
		local result = ""
		s, strPos, result = string.find(estr, "<ReturnDescription>(.-)</ReturnDescription>")
		if result == "success" then
			Static_SetText(gHandles["stcNew"], "Add Zone")
			m_iAvailableZones = m_iAvailableZones + 1
			Events.sendEvent("FRAMEWORK_SET_AVAILABLE_ZONES", {availableZones = m_iAvailableZones})
			handleAction(m_iZoneCount)
		else
			KEP_MessageBox("Unable to purchase zone. " .. result .. ".")
		end
	elseif filter == WF.LINKED_ZONES_DELETE then
		if m_iZoneCount == 3 then
			Events.sendEvent("FRAMEWORK_LINKED_ZONE_UPDATED", {isLinked = false})
		end
		requestParentZoneData()
		handleAction(1)
	elseif filter == WF.GET_ZONE_ACCESS then
		local estr = KEP_EventDecodeString( tEvent )
		ParsePrivacyData(estr)
		
	end
end

function ParseLocationData(g_locationData)
    local s = 0 		-- start and end index of substring
	local numRec = 0   	 	-- number of records in this chunk
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	s, strPos, result = string.find(g_locationData, "<ReturnCode>(.-)</ReturnCode>")

	m_tZoneDisplay = {}
	m_iZoneCount = 1
	if result == "0" then
		for zoneData in string.gmatch(g_locationData, "<Table>(.-)</Table>") do
			m_tZoneDisplay[m_iZoneCount] = {}

			s, strPos, name = string.find(zoneData, "<name>(.-)</name>")
			m_tZoneDisplay[m_iZoneCount].name = name
			
			s, e, dbPath = string.find(zoneData, "<image_thumbnail>(.-)</image_thumbnail>")
			if not dbPath then
				dbPath = DEFAULT_KANEVA_PLACE_ICON
			end


			s, strPos, community_id = string.find(zoneData, "<community_id>(.-)</community_id>")
			m_tZoneDisplay[m_iZoneCount].community_id = community_id

			s, strPos, zoneInstanceId = string.find(zoneData, "<zone_instance_id>(.-)</zone_instance_id>")
			m_tZoneDisplay[m_iZoneCount].zoneID = zoneInstanceId

			s, strPos, zoneType = string.find(zoneData, "<zone_type>(.-)</zone_type>")
			m_tZoneDisplay[m_iZoneCount].zoneType = zoneType

			s, strPos, url = string.find(zoneData, "<STPURL>(.-)</STPURL>")
			m_tZoneDisplay[m_iZoneCount].url = url

			if tonumber(zoneInstanceId) == m_iZoneInstanceID or tonumber(community_id) == m_iZoneInstanceID  then
				m_iSelectedZone = m_iZoneCount
				handleAction(m_iZoneCount)
			end

			g_fileName = tostring(split(GetFilenameFromPath(dbPath), ".")[1])
			g_fileName = community_id .. "_" .. g_fileName .. CUSTOM_TEXTURE_THUMB
						
			KEP_DownloadTextureAsyncToGameId( g_fileName, dbPath )
			
			local gameId = KEP_GetGameId()
			g_fileName = "../../CustomTexture/".. gameId .. "/" .. g_fileName

			-- Calls the scaleImage function from CommonFunctions in order to scale the thumbnail				
			scaleImage(gHandles["imgZone" .. m_iZoneCount], gHandles["imgZoneBG" .. m_iZoneCount], g_fileName)      		

		    m_tZoneDisplay[m_iZoneCount].thumbnail = g_fileName		

		    ---Handle new world
		    if m_iNewZoneIndex and m_iNewZoneIndex == m_iZoneCount then
		    	m_tNewChildSettings.zoneID = zoneInstanceId
		    	m_tNewChildSettings.zoneType = zoneType
		    	m_tNewChildSettings.availableZones = m_iAvailableZones

		    	local privacy = ALLOW_ALL

				if m_tNewChildSettings.privacy == "Private" then 
					privacy = ALLOW_PRIVATE
				elseif m_tNewChildSettings.privacy == "Owner Only" then
					privacy = ALLOW_OWNER_ONLY
				end

				m_tNewChildSettings.privacy = nil

				local web_address = GameGlobals.WEB_SITE_PREFIX..SET_ACCESS_SUFFIX..privacy.."&cover=0&gameId="..tostring(KEP_GetGameId()).."&zoneInstanceId="..tostring(m_tNewChildSettings.zoneID).."&zoneType="..tostring(m_tNewChildSettings.zoneType)
				makeWebCall( web_address, WF.SET_ZONE_ACCESS)

		    	Events.sendEvent("FRAMEWORK_UPDATE_SETTINGS", {settings = m_tNewChildSettings})
		    	m_tNewChildSettings.privacy = nil

				if m_iZoneCount == 2 then 
					Events.sendEvent("FRAMEWORK_LINKED_ZONE_UPDATED", {isLinked = true})
				end
		    end

		    m_iZoneCount = m_iZoneCount + 1
		end
		formatDisplayTable()
	end
end

function ParsePrivacyData(privacyData)
	 local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp

	s, strPos, result = string.find(privacyData, "<ReturnCode>(.-)</ReturnCode>")

	if result == "0" then
		local privacy = 0
		s, strPos, privacy = string.find(privacyData, "<isPrivate>(.-)</isPrivate>")

		if privacy then
			privacy = tonumber(privacy) + 1
		end

		s_tMainSettings.privacy = PRIVACY_SETTINGS[privacy]

		editWorldItem(s_tMainSettings, "mainSettings")
	end
end

-------------------------------------------------
-------------------------------------------------
-- Setting Event Handlers
-------------------------------------------------
-------------------------------------------------

-- Handles zone settings
function returnSettingsHandler(event)
	if event.starvation == nil then event.starvation = false end

	s_tMainSettings = {destructibleObjects = event.destructibleObjects,
					allowPvP 			= event.allowPvP,
					playerBuild			= event.playerBuild,
					playerNames			= event.playerNames,
					frameworkEnabled	= event.frameworkEnabled,
					jumpsuit			= event.jumpsuit,
					starvation			= event.starvation,
					starveTime			= event.starveTime,
					leaderboard			= event.leaderboard,
					dropItems			= event.dropItems,
					lobbySpawnOnly		= event.lobbySpawnOnly,
					respawnTimerOff		= event.respawnTimerOff,
					flagBuildOnly		= event.flagBuildOnly,
					maxFlags			= event.maxFlags,
					maxFlagObjects		= event.maxFlagObjects,
					name  				= m_tZoneDisplay[m_iSelectedZone].name.." World Settings",
					itemType   			= ITEM_TYPES.MAIN_SETTINGS,
					GLID 				= MAIN_SETTINGS_GLID,
					zoneID 				= m_iZoneInstanceID,
					zoneType            = m_iZoneType
				   }
	enableSettingsControls("Settings", true)	
	Static_SetText(gHandles["stcSettings"], "Main Settings")
	--m_iAvailableZones = 6
	applyImage(gHandles["imgSettings"], MAIN_SETTINGS_GLID)
end

-- Handles zone settings
function returnEnvironmentHandler(event)
	s_tEnvironmentSettings = {	--[[envEnabled		= event.envEnabled,]]
							fogEnabled		= event.fogEnabled,
							fogStartRange 	= event.fogStartRange,
							fogEndRange 	= event.fogEndRange,
							fogColor		= event.fogColor,
							fogColorRed		= event.fogColorRed,
							fogColorGreen	= event.fogColorGreen,
							fogColorBlue	= event.fogColorBlue,
							--[[ambientEnabled	= event.ambientEnabled,]]
							ambientColor	= event.ambientColor,
							ambientColorRed	= event.ambientColorRed,
							ambientColorGreen = event.ambientColorGreen,
							ambientColorBlue = event.ambientColorBlue,
							sunColor		= event.sunColor,
							sunColorRed		= event.sunColorRed,
							sunColorGreen 	= event.sunColorGreen,
							sunColorBlue 	= event.sunColorBlue,
							name  			= m_tZoneDisplay[m_iSelectedZone].name.. " Environment Settings",
							itemType   		= ITEM_TYPES.ENVIRONMENT_SETTINGS,
							GLID 			= ENVIRONMENT_GLID,
							zoneID 			= m_iZoneInstanceID,
							zoneType        = m_iZoneType
						}

	enableSettingsControls("Env", true)
	Static_SetText(gHandles["stcEnv"], "Environment")
	applyImage(gHandles["imgEnv"], ENVIRONMENT_GLID)

end

function returnWelcomeHandler(event)
	s_tWelcomeSettings = {	--[[envEnabled		= event.envEnabled,]]
							showMenu		= event.showMenu,
							showWorldName 	= event.showWorldName,
							welcomeImage 	= event.welcomeImage,
							welcomeText		= event.welcomeText,
							name  			= m_tZoneDisplay[m_iSelectedZone].name .. " Welcome Menu",
							itemType   		= ITEM_TYPES.WELCOME_SETTINGS,
							GLID 			= WELCOME_GLID,
							zoneID 			= m_iZoneInstanceID,
							zoneType        = m_iZoneType
						}

	enableSettingsControls("Welcome", true)
	Static_SetText(gHandles["stcWelcome"], "Welcome")
	applyImage(gHandles["imgWelcome"], WELCOME_GLID)
end

function returnOverviewHandler(event )

	s_tOverviewSettings = {
					overviewEnabled 	= event.overviewEnabled or false,
					overviewImage 		= event.overviewImage,
					markers 			= event.markers,
					name  				= "Overview Settings",
					itemType   			= ITEM_TYPES.OVERVIEW_SETTINGS,
					GLID 				= MAIN_SETTINGS_GLID
				   }
end
---------------------------
----Event handlers---------
--------------------------
function newZoneHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_iNewZoneIndex = m_iZoneCount
	m_tNewChildSettings = Events.decode(KEP_EventDecodeString(event))
	requestParentZoneData()
end

function parentInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_iParentZoneType = KEP_EventDecodeNumber(event)
	m_iParentZoneInstanceID = KEP_EventDecodeNumber(event)
	m_iAvailableZones = KEP_EventDecodeNumber(event)
	KEP_EventCreateAndQueue( "RequestAccessPassInfoEvent" )
	requestParentZoneData()
end

function textureDownloadHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	formatDisplayTable()
end

function responseAccessPassInfoHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local apPass 	= Event_DecodeNumber(event)
	m_bVIPPass 	= (Event_DecodeNumber(event) == 1)

	if m_bVIPPass and (m_iAvailableZones < VIP_ZONES) then
		m_iAvailableZones = VIP_ZONES
	end
	--Now that i know my VIP settings, request world settings
	Events.sendEvent("FRAMEWORK_REQUEST_SETTINGS")
	Events.sendEvent("FRAMEWORK_REQUEST_ENVIRONMENT")
	Events.sendEvent("FRAMEWORK_REQUEST_WELCOME")
end

function creditTransactionHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local url = GameGlobals.WEB_SITE_PREFIX..BUY_LINKED_ZONE_SLOT.."&zoneInstanceID="..tostring(m_iParentZoneInstanceID).."&zoneType="..tostring(m_iParentZoneType)
	makeWebCall(url, WF.LINKED_ZONES_ADD)
end

-------------------------------------------------
-------------------------------------------------
-- Local Functions
-------------------------------------------------
-------------------------------------------------

function formatDisplayTable()
	setControlEnabledVisible("imgPlusIcon", false)
	setControlEnabledVisible("stcNew", false)

	if not m_tZoneDisplay[1] then return end --if no parent, then nothing to display
	for i=1, ITEMS_PER_PAGE do
		
		if m_tZoneDisplay[i] and m_tZoneDisplay[i].name then
			enableControls(i, true)
			Static_SetText(gHandles["stcZone"..i], m_tZoneDisplay[i].name)

			--local ihFore = Dialog_GetImage(gDialogHandle, "imgZone"..m_iZoneCount)
			--local ihBack = Dialog_GetImage(gDialogHandle, "imgZoneBG"..m_iZoneCount)

			local zoneImage = Image_GetDisplayElement(gHandles["imgZone" .. i])
			Element_AddTexture(zoneImage, gDialogHandle, m_imagePath)
			scaleImage(gHandles["imgZone" .. i], gHandles["imgZoneBG" .. i], m_tZoneDisplay[i].thumbnail)
			if i ~= 1 then
				setControlEnabledVisible("imgZoneLock"..i, false)
			end
		else
			enableControls(i, false)

			local validZoneType = tonumber(m_tZoneDisplay[1].zoneType) == VALID_ZONE_TYPE
			if not validZoneType then 
				m_iAvailableZones = 1
				Control_SetToolTip(gHandles["imgZoneLock"..i], "This world does not support linked zones.")
			end
			setControlEnabledVisible("imgZoneLock"..i, (i > m_iAvailableZones))
			if i == m_iZoneCount and KEP_IsOwner() and validZoneType then
				setControlEnabledVisible("btnZone"..i, true)
				setControlEnabledVisible("imgPlusIcon", true)
				setControlEnabledVisible("stcNew", true)
				Control_SetLocation(gHandles["imgPlusIcon"], Control_GetLocationX(gHandles["btnZone"..i]) + NEW_PLUS_OFFSET_X, Control_GetLocationY(gHandles["btnZone"..i]) + NEW_PLUS_OFFSET_Y)
				Control_SetLocation(gHandles["stcNew"], Control_GetLocationX(gHandles["btnZone"..i]) + NEW_STATIC_OFFSET_X, Control_GetLocationY(gHandles["btnZone"..i]) + NEW_STATIC_OFFSET_Y)
				if i == m_iAvailableZones + 1 then
					Static_SetText(gHandles["stcNew"], "Buy Zone")
					setControlEnabledVisible("imgZoneLock"..i, false)
				else
					Static_SetText(gHandles["stcNew"], "Add Zone")
				end
			end	
		end 

		if i == m_iNewZoneIndex then
			handleAction(i)
			m_iNewZoneIndex = nil
		end
	end
end

function editWorldItem(settings, prefab)
	MenuOpen("Framework_GameItemEditor.xml")
	local item = settings

	local flatItem = {}

	flatenItemData(item, item, flatItem)

	local itemInfo = Events.encode(flatItem)
	
	local event = KEP_EventCreate("EDIT_GAME_ITEM_INFO")
	KEP_EventEncodeString(event, -1) -- UNID
	KEP_EventEncodeString(event, itemInfo) -- item info
	KEP_EventEncodeString(event, m_editorName[item.itemType]) -- Editor title
	KEP_EventEncodeString(event, prefab) -- prefabType
	KEP_EventEncodeString(event, "editItem")	-- menu mode
	KEP_EventQueue(event)
end

function flatenItemData(baseData, itemData, flatItem)
	for attribute,value in pairs(itemData) do
		if flatItem[attribute] == nil then
			flatItem[attribute] = value
		end
	end
end

function enableControls(index, enabled)
	setControlEnabledVisible("btnZone"..index, enabled)
	setControlEnabledVisible("imgZone"..index, enabled)
	setControlEnabledVisible("stcZone"..index, enabled)
	setControlEnabledVisible("imgZoneName"..index, enabled)
end

function setControlEnabledVisible(controlName, bool)
	Control_SetVisible(gHandles[controlName], bool)
	Control_SetEnabled(gHandles[controlName], bool)
end

function enableSettingsControls(setting, enabled)
	setControlEnabledVisible("img"..setting.."BG", enabled)
	setControlEnabledVisible("img"..setting, enabled)
	setControlEnabledVisible("img"..setting.."Name", enabled)
	setControlEnabledVisible("stc"..setting, enabled)
	setControlEnabledVisible("btn"..setting, enabled)
end