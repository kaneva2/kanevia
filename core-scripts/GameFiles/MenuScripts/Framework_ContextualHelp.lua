--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_ContextualHelp.lua - Menu for the Contextual Help system
--
-- Created by Edie "Danger" Woelfle 
--
-- Copyright 2016 Kaneva
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("MenuHelper.lua")
dofile("ContextualHelper.lua")

TEXT_MARGIN = 20
BUFFER = 2
SPACING = 20

currentMenuSize = {}
currentMenuSize.height = 0
currentMenuSize.width = 0

menuLocationX = 0
menuLocationY = 0

contextualHelpID = 0

local m_closeTime 

-- InitializeKEPEvents - Register all script events here. 
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	
end

-- InitializeKEPEventHandlers - Register all script event handlers here. 
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "openContextualMenuEventHandler", "openContextualMenuEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "updateContextualHelpMenuEventHandler", "updateContextualMenuEvent", KEP.HIGH_PRIO)
end

---------------------------------------------------------
-- Event Handlers
---------------------------------------------------------

function openContextualMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	showElements(true)

	local eventParams = KEP_EventDecodeString(event)
	eventParams = Events.decode(eventParams)

	contextualHelpID = eventParams.type

	if not m_closeTime then m_closeTime = eventParams.closeTime end

	-- set the help text for the menu
	setHelpText(eventParams.help, eventParams.bubbleWidth)

	-- set the arrow and menu positions
	setHelpArrow(eventParams.arrow, eventParams.x, eventParams.y, eventParams.width, eventParams.height)

	-- bring the menu to front when updated 
	MenuBringToFrontThis()
end

function updateContextualHelpMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	local eventParams = KEP_EventDecodeString(event)
	eventParams = Events.decode(eventParams)

	if not m_closeTime then m_closeTime = eventParams.closeTime end

	-- set the help text for the menu
	setHelpText(eventParams.help, eventParams.bubbleWidth)

	-- set the arrow and menu positions
	setHelpArrow(eventParams.arrow, eventParams.x, eventParams.y, eventParams.width, eventParams.height)

	-- bring the menu to front when updated 
	MenuBringToFrontThis()
end

---------------------------------------------------------
-- Helper Functions
---------------------------------------------------------

function showElements(show)
	for name, handle in pairs(gHandles) do
		Control_SetEnabled(handle, show)
		Control_SetVisible(handle, show)
	end
	MenuSetMinimizedThis(not show)
end

function btnClose_OnButtonClicked(buttonHandle)
	closeCurrentHelp()
	MenuCloseThis()
end 

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle ) -- REQUIRED - DO NOT REMOVE
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	ContextualHelper.hideHelp(contextualHelpID)
	m_closeTime = nil
	Helper_Dialog_OnDestroy( dialogHandle ) -- REQUIRED - DO NOT REMOVE
end

-- Dialog_OnRender - Called upon screen render. Animate your script here.
-- Called every frame
function Dialog_OnRender(dialogHandle, fElapsedTime)
	if m_closeTime then
		if m_closeTime > 0 then
			m_closeTime = m_closeTime - fElapsedTime
		else
			-- tell the controller this event is complete 
			closeCurrentHelp()
		end
	end

	MenuBringToFrontThis()
end

function closeCurrentHelp()
	ContextualHelper.completeHelp(contextualHelpID)
	m_closeTime = nil
	showElements(false)
end

-- When the screen is moved, make sure we're still in the right spot 
function Dialog_OnMoved(dialogHandle, x, y)
	MenuSetLocationThis(menuLocationX, menuLocationY)
	MenuBringToFrontThis()
end

-- sets all the dialog text for the menu, and resize it to hug the text 
function setHelpText(help, bubbleWidth)
	-- set the title text 
	Static_SetText(gHandles["stcHeader"], UnescapeHTML(help.title))

	-- set the description text
	Static_SetText(gHandles["stcBody"], UnescapeHTML(help.description))

	if bubbleWidth then bubbleWidth = bubbleWidth - (TEXT_MARGIN * 2) end

	-- get the height of the body text and set the height 
	local bodyHandle = gHandles["stcBody"]
	local bodyWidth = bubbleWidth or Control_GetWidth(bodyHandle)
	local bodyHeight = Static_GetStringHeight(bodyHandle, Static_GetText(bodyHandle))
	Control_SetSize(bodyHandle, bodyWidth, bodyHeight)

	-- set the height of the menu
	local menuHeight = bodyHeight + Control_GetLocationY(bodyHandle) + TEXT_MARGIN
	local menuWidth = bodyWidth + (TEXT_MARGIN * 2)
	MenuSetSizeThis(menuWidth, menuHeight)

	currentMenuSize.width = menuWidth
	currentMenuSize.height = menuHeight

	-- set the location of the close X
	local closeY = Control_GetLocationY(gHandles["btnClose"])
	local closeWidth = Control_GetWidth(gHandles["btnClose"])
	Control_SetLocationX(gHandles["btnClose"], menuWidth - (closeWidth + closeY))
end 

-- sets the position of the arrow, and the menu
function setHelpArrow(arrowPosition, x, y, width, height)
	-- make all arrows invisible 
	Control_SetVisible(gHandles["imgLeftArrow"], false)
	Control_SetVisible(gHandles["imgRightArrow"], false)
	Control_SetVisible(gHandles["imgTopArrow"], false)
	Control_SetVisible(gHandles["imgDownArrow"], false)

	local menuSize = currentMenuSize

	if arrowPosition == ContextualHelpFormat.LEFT_ARROW then
		Control_SetVisible(gHandles["imgLeftArrow"], true)

		-- resize the menu for the height of the arrow 
		local minimumMenuSize = Control_GetHeight(gHandles["imgLeftArrow"]) + (TEXT_MARGIN * 2)
		currentMenuSize.height = math.max(currentMenuSize.height, minimumMenuSize)
		MenuSetSizeThis(currentMenuSize.width, currentMenuSize.height)
		menuSize = currentMenuSize

		-- set the location of the arrow 
		Control_SetLocationY(gHandles["imgLeftArrow"], (menuSize.height - Control_GetHeight(gHandles["imgLeftArrow"])) / 2)

		-- set the location of the menu
		menuLocationX = x + width + Control_GetWidth(gHandles["imgLeftArrow"])
		menuLocationY = y + (height / 2) - (menuSize.height / 2)
	elseif arrowPosition == ContextualHelpFormat.RIGHT_ARROW then
		Control_SetVisible(gHandles["imgRightArrow"], true)

		-- resize the menu for the height of the arrow 
		local minimumMenuSize = Control_GetHeight(gHandles["imgRightArrow"]) + (TEXT_MARGIN * 2)
		currentMenuSize.height = math.max(currentMenuSize.height, minimumMenuSize)
		MenuSetSizeThis(currentMenuSize.width, currentMenuSize.height)
		menuSize = currentMenuSize

		-- set the location of the arrow 
		Control_SetLocationY(gHandles["imgRightArrow"], (menuSize.height - Control_GetHeight(gHandles["imgRightArrow"])) / 2)
		Control_SetLocationX(gHandles["imgRightArrow"], menuSize.width - 2)

		-- set the location of the menu
		menuLocationX = x - menuSize.width - math.abs(Control_GetWidth(gHandles["imgRightArrow"]))
		menuLocationY = y + (height / 2) - (menuSize.height / 2)
	elseif arrowPosition == ContextualHelpFormat.TOP_ARROW then
		Control_SetVisible(gHandles["imgTopArrow"], true)

		-- set the location of the arrow 
		Control_SetLocationX(gHandles["imgTopArrow"], (menuSize.width - Control_GetWidth(gHandles["imgTopArrow"])) / 2)

		-- set the location of the menu
		menuLocationX = x + ((width - menuSize.width) / 2)
		menuLocationY = y - menuSize.height + height + Control_GetHeight(gHandles["imgTopArrow"])
	elseif arrowPosition == ContextualHelpFormat.BOTTOM_ARROW then
		Control_SetVisible(gHandles["imgDownArrow"], true)

		-- set the location of the arrow 
		Control_SetLocationY(gHandles["imgDownArrow"], menuSize.height - 2)
		Control_SetLocationX(gHandles["imgDownArrow"], (menuSize.width - Control_GetWidth(gHandles["imgDownArrow"])) / 2)

		-- set the location of the menu
		menuLocationX = x + ((width - menuSize.width) / 2)
		menuLocationY = y - menuSize.height - Control_GetHeight(gHandles["imgDownArrow"])
	elseif arrowPosition == ContextualHelpFormat.LEFT_NO_ARROW then
		-- set the location of the menu
		menuLocationX = x + width + SPACING
		menuLocationY = y + (height / 2) - (menuSize.height / 2)
	elseif arrowPosition == ContextualHelpFormat.RIGHT_NO_ARROW then
		-- set the location of the menu
		menuLocationX = x - menuSize.width - SPACING
		menuLocationY = y + (height / 2) - (menuSize.height / 2)
	elseif arrowPosition == ContextualHelpFormat.TOP_NO_ARROW then
		-- set the location of the menu
		menuLocationX = x + ((width - menuSize.width) / 2)
		menuLocationY = y - menuSize.height - SPACING
	elseif arrowPosition == ContextualHelpFormat.BOTTOM_NO_ARROW then
		-- set the location of the menu
		menuLocationX = x + ((width - menuSize.width) / 2) 
		menuLocationY = y + height + SPACING
	else
		menuLocationX = x
		menuLocationY = y
	end

	-- set the menu location where calculated 
	MenuSetLocationThis(menuLocationX, menuLocationY)
end

-- print the table 
function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end