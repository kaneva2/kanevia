--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_LastGameItemWarning.lua
-- 
-- Warns the user of removing the last game item in the zone
-- Author: Wes Anderson
--This menu will also handle removing of an event flag that has an event scheduled with it
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_EventHelper.lua")

----------------------
-- Globals
----------------------

local CLAIM_GLID = 4612626
local EVENT_GLID = 4647017
local m_bEventFlag = false
local m_iFlagPID = 0
-- When the menu is created
function onCreate()
	-- Register client - client event handlers
	KEP_EventRegisterHandler("deleteFlagHandler", "FRAMEWORK_DELETE_FLAG", KEP.HIGH_PRIO)
end

-- -- -- -- -- -- --
-- Event Handlers
-- -- -- -- -- -- --

-- Handles Game Item limit warning messages
function deleteFlagHandler(dispatcher, fromNetid, event, eventID, filter, objectID)

	m_iFlagPID = KEP_EventDecodeNumber(event)

	m_sFlagType = KEP_EventDecodeString(event)

	local flagImage = Image_GetDisplayElement(gHandles["imgItem"])
	--changes the menu to reflect deleting an event flag
	if m_sFlagType == "event" then
		Static_SetText(gHandles["stcMessage"], "Removing this flag will delete its event. <br>All credits or rewards spent for a Premium event will be lost.")
		Dialog_SetCaptionText(gDialogHandle, " Remove Event Flag")

		KEP_LoadIconTextureByID(EVENT_GLID, flagImage, gDialogHandle)
	elseif m_sFlagType == "claimOnly" then
		Static_SetText(gHandles["stcMessage"], "Deleting a land claim flag will remove all placed items within its area.")
		Dialog_SetCaptionText(gDialogHandle, " Remove Land Claim Flag")

		KEP_LoadIconTextureByID(CLAIM_GLID, flagImage, gDialogHandle)
	else
		Static_SetText(gHandles["stcMessage"], "Picking up a land claim flag will permanently delete it.")
		Dialog_SetCaptionText(gDialogHandle, " Remove Land Claim Flag")

		KEP_LoadIconTextureByID(CLAIM_GLID, flagImage, gDialogHandle)
	end
end

-- Disable Gaming
function btnDelete_OnButtonClicked(buttonHandle)
		
	if m_sFlagType == "event" then
		Events.sendEvent("FRAMEWORK_DESTROY_EVENT_FLAG", {PID = m_iFlagPID})
	elseif m_sFlagType == "claimOnly" then
		Events.sendEvent("FLAG_DELETE_ITEMS", {PID = m_iFlagPID})
	end

	KEP_EventCreateAndQueue("FRAMEWORK_SELECT_HAND")
	MenuCloseThis()

	Events.sendEvent("FRAMEWORK_PICKUP_GAME_ITEM", {PID = m_iFlagPID})
end