--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_WorldCoinExchange.lua
-- Author: ARA 2015
-- Client menu script for world coin exchange to rewards feature
-------------------------------------------------------------------------------
dofile("HUDHelper.lua")
dofile("..\\MenuScripts\\InventoryHelper.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("Framework_DragDropHelper.lua")

local SLIDER_NAME = "sldr_menuAmount"
local STEP_VALUE = 1

local m_playerName = ""
local m_ownerName = ""
local m_communityId = 0
local m_maxExchange = nil
local m_exchangeSuccess = nil
local m_myCoins = 0
local m_currCoins = 0
local m_currRewards = 0
local m_bPressed = false
local m_inventory = {}
local m_rangeMax = nil
local m_sliderHandle = nil
local m_isPendingExchange = false

--TODO: PJD - This menu needs to be converted to Framework-style

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )

	KEP_EventRegisterHandler("updateInventoryClient", "UPDATE_INVENTORY_CLIENT", KEP.HIGH_PRIO)	
	KEP_EventRegisterHandler("updateInventoryClientFull", "UPDATE_INVENTORY_CLIENT_FULL", KEP.HIGH_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	
	Events.registerHandler("FRAMEWORK_UPDATE_COIN_EXCHANGE", updateCoinExchange)

	requestLocationData() -- get zone information
	requestExchangeData() -- get exchange limits

	requestInventory(INVENTORY_PID)

	m_sliderHandle = gHandles[SLIDER_NAME]
	m_rangeMax = Slider_GetRangeMax(m_sliderHandle)

	Slider_SetValue(m_sliderHandle, 0)
	Static_SetText(gHandles["stc_menuRewardCount"], tostring(0))
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy( dialogHandle )
end

-- Catches web calls
function BrowserPageReadyHandler(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	-- If browser return for Location Data web call
	if filter == WF.PLACE_INFO then
		local estr = KEP_EventDecodeString( tEvent )
		ParseLocationData(estr)
	   	return KEP.EPR_CONSUMED
	-- If browser return for Exchange Data web call
	elseif filter == WF.EXCHANGE_INFO then
		-- TODO: add EXCHANGE_INFO to WF
		local returnData = KEP_EventDecodeString( tEvent )
		ParseExchangeData( returnData )
	   	return KEP.EPR_CONSUMED
	end
end

-- Web call to receive current information for available exchange options
function requestExchangeData()
	local address = GameGlobals.WEB_SITE_PREFIX .. WORLD_ITEM_CONVERSION_SUFFIX
	makeWebCall( address, WF.EXCHANGE_INFO )
end

-- Web call to receive the current zone's location data
function requestLocationData()
	local address = GameGlobals.WEB_SITE_PREFIX..GET_PLACE_INFORMATION_SUFFIX
	makeWebCall( address , WF.PLACE_INFO)
end

function updateCoinExchange(event)
	ParseExchangeData(event.text)
	m_currRewards = 0
	m_isPendingExchange = false
end

-- Read exchange data web call return
function ParseExchangeData(xmlData)
	local result = string.match(xmlData, "<ReturnCode>([0-9]+)</ReturnCode>")
	-- if the web call is a success
	if result == "0" then
		s, strPos, m_maxExchange = string.find(xmlData, "<DailyConversionsRemaining>(.-)</DailyConversionsRemaining>")
		s, strPos, m_exchangeSuccess = string.find(xmlData, "<ReturnDescription>(.-)</ReturnDescription>")
		if m_exchangeSuccess == nil then
			s, strPos, m_exchangeSuccess = string.find(xmlData, "<ResultDescription>(.-)</ResultDescription>")
		end
	end
	if m_maxExchange == nil then
		LogWarn("Maximum exchange data was not returned from the web call Return xml["..tostring(xmlData).."]")
		MenuCloseThis()
	end
	if m_exchangeSuccess == nil then
		LogWarn("Success exchange data was not returned from the web call Return xml["..tostring(xmlData).."]")
		MenuCloseThis()
	end

	Static_SetText(gHandles["stc_menuRewardSubtext"], m_maxExchange.." remaining for today")
	if Control_GetEnabled(gHandles["stc_menuSubHeader_noCoins"]) ~= 1 and Control_GetEnabled(gHandles["stc_menuSubHeader_ownExchange"]) ~= 1 then
		if tonumber(m_maxExchange) <= 0 then
			Control_SetEnabled(gHandles["stc_menuSubHeader_maxCoins"], true)

			Control_SetEnabled(m_sliderHandle, false)
			Control_SetEnabled(gHandles["stc_menuSliderSubtext"], false)
			Control_SetEnabled(gHandles["stc_menuRewardSubtext"], false)
			Control_SetEnabled(gHandles["btn_menuCancel1"], true)
			Control_SetEnabled(gHandles["btn_menuCancel2"], false)
			Control_SetEnabled(gHandles["btn_menuExchange"], false)
			Control_SetVisible(gHandles["btn_menuCancel2"], false)
			Control_SetVisible(gHandles["btn_menuExchange"], false)
		else
			Control_SetEnabled(gHandles["stc_menuSubHeader_maxCoins"], false)

			Control_SetEnabled(m_sliderHandle, true)
			Control_SetEnabled(gHandles["stc_menuSliderSubtext"], true)
			Control_SetEnabled(gHandles["stc_menuRewardSubtext"], true)
			Control_SetEnabled(gHandles["btn_menuCancel1"], false)
			Control_SetEnabled(gHandles["btn_menuCancel2"], true)
			Control_SetEnabled(gHandles["btn_menuExchange"], true)
			Control_SetVisible(gHandles["btn_menuCancel2"], true)
			Control_SetVisible(gHandles["btn_menuExchange"], true)
		end
	end
end

-- Read location data web call return
function ParseLocationData(locationData)
    local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	s, strPos, m_ownerName = string.find(locationData, "<creator_username>(.-)</creator_username>")
	s, strPos, m_communityId = string.find(locationData, "<community_id>(.-)</community_id>")
	-- Nil check returns
	if m_ownerName == nil then
		LogWarn("No player name returned from GetPlaceInformation web call Return xml["..tostring(locationData).."]")
		MenuCloseThis()
	elseif m_communityId == nil then
		LogWarn("No community ID returned from GetPlaceInformation web call Return xml["..tostring(locationData).."]")
		MenuCloseThis()
	end
	if Control_GetEnabled(gHandles["stc_menuSubHeader_noCoins"]) ~= 1 and Control_GetEnabled(gHandles["stc_menuSubHeader_maxCoins"]) ~= 1 then
		if m_ownerName ~= KEP_GetLoginName() then
			Control_SetEnabled(gHandles["stc_menuSubHeader_ownExchange"], true)

			Control_SetEnabled(m_sliderHandle, false)
			Control_SetEnabled(gHandles["stc_menuSliderSubtext"], false)
			Control_SetEnabled(gHandles["stc_menuRewardSubtext"], false)
			Control_SetEnabled(gHandles["btn_menuCancel1"], true)
			Control_SetEnabled(gHandles["btn_menuCancel2"], false)
			Control_SetEnabled(gHandles["btn_menuExchange"], false)
			Control_SetVisible(gHandles["btn_menuCancel2"], false)
			Control_SetVisible(gHandles["btn_menuExchange"], false)
		else
			Control_SetEnabled(gHandles["stc_menuSubHeader_ownExchange"], false)

			Control_SetEnabled(m_sliderHandle, true)
			Control_SetEnabled(gHandles["stc_menuSliderSubtext"], true)
			Control_SetEnabled(gHandles["stc_menuRewardSubtext"], true)
			Control_SetEnabled(gHandles["btn_menuCancel1"], false)
			Control_SetEnabled(gHandles["btn_menuCancel2"], true)
			Control_SetEnabled(gHandles["btn_menuExchange"], true)
			Control_SetVisible(gHandles["btn_menuCancel2"], true)
			Control_SetVisible(gHandles["btn_menuExchange"], true)
		end
	end
end


function updateInventoryClientFull(dispatcher, fromNetid, tEvent, eventid, filter, objectid)
	m_inventory = decompileInventory(tEvent)
	updateInventoryCounts()
end

function updateInventoryClient(dispatcher, fromNetid, tEvent, eventid, filter, objectid)	
	local updateIndex = KEP_EventDecodeNumber(tEvent)
	local updateItem = decompileInventoryItem(tEvent)
	
	if m_inventory[updateIndex] then
		m_inventory[updateIndex] = updateItem
		updateInventoryCounts()
	end
end

function updateInventoryCounts()
	local myCoins = 0
	
	for index, item in pairs(m_inventory) do
		if tostring(item.properties.name) == "World Coin" then
			myCoins = myCoins + item.count
		end
	end

	m_myCoins = myCoins
	m_currCoins = 0

	-- Zero out the coin and reward counts upon inventory update
	Static_SetText(gHandles["stc_menuCoinCount"], tostring(myCoins))
	Static_SetText(gHandles["stc_menuRewardCount"], tostring(0))
	Slider_SetValue(m_sliderHandle, m_currCoins)

	if Control_GetEnabled(gHandles["stc_menuSubHeader_ownExchange"]) ~= 1 and Control_GetEnabled(gHandles["stc_menuSubHeader_maxCoins"]) ~= 1 then
		if m_myCoins <= 0 then
			Control_SetEnabled(gHandles["stc_menuSubHeader_noCoins"], true)

			Control_SetEnabled(m_sliderHandle, false)
			Control_SetEnabled(gHandles["stc_menuSliderSubtext"], false)
			Control_SetEnabled(gHandles["stc_menuRewardSubtext"], false)
			Control_SetEnabled(gHandles["btn_menuCancel1"], true)
			Control_SetEnabled(gHandles["btn_menuCancel2"], false)
			Control_SetEnabled(gHandles["btn_menuExchange"], false)
			Control_SetVisible(gHandles["btn_menuCancel2"], false)
			Control_SetVisible(gHandles["btn_menuExchange"], false)
		else
			Control_SetEnabled(gHandles["stc_menuSubHeader_noCoins"], false)

			Control_SetEnabled(m_sliderHandle, true)
			Control_SetEnabled(gHandles["stc_menuSliderSubtext"], true)
			Control_SetEnabled(gHandles["stc_menuRewardSubtext"], true)
			Control_SetEnabled(gHandles["btn_menuCancel1"], false)
			Control_SetEnabled(gHandles["btn_menuCancel2"], true)
			Control_SetEnabled(gHandles["btn_menuExchange"], true)
			Control_SetVisible(gHandles["btn_menuCancel2"], true)
			Control_SetVisible(gHandles["btn_menuExchange"], true)
		end
	end
end

function calculateCost(curValue)
	local maxCoins = m_myCoins
	if m_myCoins > tonumber(m_maxExchange) then
		maxCoins = tonumber(m_maxExchange)
	end

	m_currRewards = math.ceil((curValue * maxCoins) / m_rangeMax)
	m_currCoins = math.ceil(m_myCoins - m_currRewards)

	Static_SetText(gHandles["stc_menuRewardCount"], tostring(m_currRewards))
	Static_SetText(gHandles["stc_menuCoinCount"], tostring(m_currCoins))
end
-- -- -- -- -- -- --
-- Button Handlers
-- -- -- -- -- -- --

function Dialog_OnLButtonDownInside(dialogHandle, x, y)
	m_bPressed = true

	if ((Control_ContainsPoint(gHandles["btn_menuCancel1"], x, y) == 1) and Control_GetEnabled(gHandles["btn_menuCancel1"]) == 1) or ((Control_ContainsPoint(gHandles["btn_menuCancel2"], x, y) == 1) and Control_GetEnabled(gHandles["btn_menuCancel2"]) == 1) then
		MenuCloseThis()
	end

	if not m_isPendingExchange and (Control_ContainsPoint(gHandles["btn_menuExchange"], x, y) == 1) and Control_GetEnabled(gHandles["btn_menuExchange"]) == 1 and m_currRewards > 0 then
		-- request rewards
		Events.sendEvent("FRAMEWORK_CONVERT_WORLD_COINS", {count = tonumber(m_currRewards)})
		m_currRewards = 0
		m_isPendingExchange = true
	end
end

function sldr_menuAmount_OnSliderValueChanged(sliderHandle, sliderValue)
	calculateCost(sliderValue)
end