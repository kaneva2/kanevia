--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")
dofile("../Scripts/ParticleSysIds.lua")
dofile("../Scripts/CoreItems.lua")
dofile("../Scripts/UGCImportState.lua")
dofile("../Scripts/TextureImportStateIds.lua")

PARTICLE_EDITOR_TEXTURE_BROWSER_CLOSED_EVENT = "ParticleEditorTextureBrowserClosedEvent"
TEXTURE_IMPORT_PROGRESS_EVENT = "TextureImportProgressEvent"

gNextDropId = 2000000	-- a separate drop ID segment (2000000-...) DragDropEvents uses (0~999999), ImportMenu uses (1m~1999999)
gTarget = { valid = 0, pos = { x = 0, y = 0, z = 0 }, normal = { x = 0, y = 0, z = 0 }, type = ObjectType.UNKNOWN, id = -1, name = "" }

gDummyDynObjGlid = CoreItems.NullObject

gTabShortNames = { "P", "F", "E" }
gTabNames = { "Particle", "Forces", "Emitter" }
gCurrTabIdx = 0

DAMPING_MODEL = { LINEAR = 0, QUADRATIC = 1 }
EMITTER_SHAPE = { BOX = 0, SPHERE = 1 }

gParticleDef = {
	textures = {
		fileNames = {},
		convertedImagePaths = {},
		handles = {},
		customFrameRate = 0,
		additiveAlpha = 0,
		randomInitFrame = 0,
		randomXMirroring = 0,
		randomYMirroring = 0,
	},
	colorFunc = {
		r = { [0] = 255, [1000] = 255 },
		g = { [0] = 255, [1000] = 255 },
		b = { [0] = 255, [1000] = 255 },
		a = { [0] = 255, [1000] = 255 },
	},
	sizeFunc = { [0] = 0.02, [1000] = 0.02 },
	rotation = {
		randomDirection = 0,
		initAngle = 0, initAngleVar = 0,
		angleFunc = { [0] = 0, [1000] = 0 },
	},
	gravityCoeff = 0,
	dampingCoeff = 0, dampingByVel = DAMPING_MODEL.LINEAR, dampingBySize = DAMPING_MODEL.LINEAR,
}

gEmitterDef = {
	shape = EMITTER_SHAPE.BOX,
	center = { x = 0, y = 0, z = 0 },
	size = { x = 1, y = 1, z = 1 },
	birthRateFunc = { [0] = 100, [1000] = 100 },
	birthRateVarFunc = {},
	particleLifeFunc = { [0] = 1, [1000] = 1 },
	particleLifeVarFunc = {},
	initVelFunc = { x = { [0] = 0, [1000] = 0 }, y = { [0]=5, [1000]=5 }, z = { [0] = 0, [1000] = 0 } },
	initVelVarFunc = { x = {}, y = {}, z = {} },
	ptcSizeCoeffFunc = { [0] = 1, [1000] = 1 },
	ptcSizeCoeffVarFunc = {},
	ptcRotCoeffFunc = { [0] = 1, [1000] = 1 },
	ptcRotCoeffVarFunc = {},
}

gEffectDef = {
	offset = { x = 0, y = 0, z = 0 },
	emitterLife = 10, emitterLifeVar = 0, loopEmitter = 1,
	emitIntoObjectSpace = 0,
}

gGraphMap = {
	{
		-- Particle Tab Functions
		P_tabRedFunc		= { base=gParticleDef.colorFunc.r,
								zoomY=1,	fmtY="%d",	resY=1,		scaleY=255,	offsetY=0 },		-- graph [0, 1] -> R [0, 255], R res = 1, initial zoom = full range
		P_tabGreenFunc		= { base=gParticleDef.colorFunc.g,
								zoomY=1,	fmtY="%d",	resY=1,		scaleY=255,	offsetY=0 },		-- graph [0, 1] -> G [0, 255], G res = 1, initial zoom = full range
		P_tabBlueFunc		= { base=gParticleDef.colorFunc.b,
								zoomY=1,	fmtY="%d",	resY=1,		scaleY=255,	offsetY=0 },		-- graph [0, 1] -> B [0, 255], B res = 1, initial zoom = full range
		P_tabAlphaFunc		= { base=gParticleDef.colorFunc.a,
								zoomY=1,	fmtY="%d",	resY=1,		scaleY=255,	offsetY=0 },		-- graph [0, 1] -> A [0, 255], A res = 1, initial zoom = full range
		P_tabSizeFunc		= { base=gParticleDef.sizeFunc,
								zoomY=0.1,	fmtY="%.2f",resY=0.01,	scaleY=10,	offsetY=0 },		-- graph [0, 1] -> Size [0, 10], size res = 0.01, initial zoom = 1/10 (size [0, 1])
		P_tabRotationFunc	= { base=gParticleDef.rotation.angleFunc,
								zoomY=1,	fmtY="%d",	resY=1,		scaleY=720,	offsetY=-360 },		-- graph [0, 1] -> Rotation [-360, 360], rotation res = 1, initial zoom = full range
		curr = "P_tabRedFunc",
	},

	nil,-- Force Tab Functions

	{
		-- Emitter Tab Functions
		E_tabBRFunc			= { base=gEmitterDef.birthRateFunc,		var=gEmitterDef.birthRateVarFunc,
								zoomY=0.1,	fmtY="%d",	resY=1,		scaleY=1000,offsetY=0 },		-- graph [0, 1] -> BR [0, 1000], BR res = 1, initial zoom = 1/10 (BR [0, 100])
		E_tabXFunc			= { base=gEmitterDef.initVelFunc.x,		var=gEmitterDef.initVelVarFunc.x,
								zoomY=0.1,	fmtY="%.2f",resY=0.01,	scaleY=200,	offsetY=-100 },		-- graph [0, 1] -> VX [-100, 100], VX res = 0.01, initial zoom = 1/10 (VX [-10, 10])
		E_tabYFunc			= { base=gEmitterDef.initVelFunc.y,		var=gEmitterDef.initVelVarFunc.y,
								zoomY=0.1,	fmtY="%.2f",resY=0.01,	scaleY=200,	offsetY=-100 },		-- graph [0, 1] -> VY [-100, 100], VY res = 0.01, initial zoom = 1/10 (VY [-10, 10])
		E_tabZFunc			= { base=gEmitterDef.initVelFunc.z,		var=gEmitterDef.initVelVarFunc.z,
								zoomY=0.1,	fmtY="%.2f",resY=0.01,	scaleY=200,	offsetY=-100 },		-- graph [0, 1] -> VZ [-100, 100], VZ res = 0.01, initial zoom = 1/10 (VZ [-10, 10])
		E_tabLifeFunc		= { base=gEmitterDef.particleLifeFunc,	var=gEmitterDef.particleLifeVarFunc,
								zoomY=0.1,	fmtY="%.2f",resY=0.01,	scaleY=40,	offsetY=0 },		-- graph [0, 1] -> PL [0, 40], PL res = 0.1, initial zoom = 1/4 (PL [0, 10])
		E_tabSizeFunc		= { base=gEmitterDef.ptcSizeCoeffFunc,	var=gEmitterDef.ptcSizeCoeffVarFunc,
								zoomY=1,	fmtY="%.2f",resY=0.01,	scaleY=10,	offsetY=0 },		-- graph [0, 1] -> SC [0, 10], SC res = 0.1, initial zoom = 1 (SC [0, 10])
		E_tabRotationFunc	= { base=gEmitterDef.ptcRotCoeffFunc,	var=gEmitterDef.ptcRotCoeffVarFunc,
								zoomY=1,	fmtY="%.2f",resY=0.01,	scaleY=20,	offsetY=-10 },		-- graph [0, 1] -> RC [-10, 10], RC res = 0.1, initial zoom = 1 (RC [-10, 10])
		curr = "E_tabBRFunc",
	},
}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "TextureBrowserClosedHandler", PARTICLE_EDITOR_TEXTURE_BROWSER_CLOSED_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "TextureImportProgressEventHandler", TEXTURE_IMPORT_PROGRESS_EVENT, KEP.HIGH_PRIO+1 )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( PARTICLE_EDITOR_TEXTURE_BROWSER_CLOSED_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( TEXTURE_IMPORT_PROGRESS_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	local ctl = Dialog_GetCheckBox( gDialogHandle, "P_cbRandomStartingImage" )
	CheckBox_SetChecked( ctl, gParticleDef.textures.randomInitFrame~=0 )
	ctl = Dialog_GetCheckBox( gDialogHandle, "P_cbRandomXImageMirroring" )
	CheckBox_SetChecked( ctl, gParticleDef.textures.randomXMirroring~=0 )
	ctl = Dialog_GetCheckBox( gDialogHandle, "P_cbRandomYImageMirroring" )
	CheckBox_SetChecked( ctl, gParticleDef.textures.randomYMirroring~=0 )
	ctl = Dialog_GetCheckBox( gDialogHandle, "P_cbRandomRotationDirection" )
	CheckBox_SetChecked( ctl, gParticleDef.rotation.randomDirection~=0 )
	ctl = Dialog_GetCheckBox( gDialogHandle, "F_cbGravity" )
	CheckBox_SetChecked( ctl, gParticleDef.gravityCoeff~=0 )
	ctl = Dialog_GetCheckBox( gDialogHandle, "F_cbDamping" )
	CheckBox_SetChecked( ctl, gParticleDef.dampingCoeff~=0 )
	ctl = Dialog_GetCheckBox( gDialogHandle, "E_cbLoop" )
	CheckBox_SetChecked( ctl, gEffectDef.loopEmitter~=0 )

	ctl = Dialog_GetRadioButton( gDialogHandle, "P_rbSpreadImages" )
	RadioButton_SetChecked( ctl, gParticleDef.textures.customFrameRate==0 )
	ctl = Dialog_GetRadioButton( gDialogHandle, "P_rbLoopImages" )
	RadioButton_SetChecked( ctl, gParticleDef.textures.customFrameRate~=0 )
	ctl = Dialog_GetCheckBox( gDialogHandle, "P_cbAdditiveAlpha" )
	CheckBox_SetChecked( ctl, gParticleDef.textures.additiveAlpha~=0 )
	UpdateEmitterShapeControls()

	setEditBoxText( "P_txtLoopFPS", "15" )
	enableControl( "P_txtLoopFPS", false )

	setEditBoxText( "P_txtRandomRotationFrom", tostring(gParticleDef.rotation.initAngle-gParticleDef.rotation.initAngleVar) )
	setEditBoxText( "P_txtRandomRotationTo", tostring(gParticleDef.rotation.initAngle+gParticleDef.rotation.initAngleVar) )
	enableControl( "P_txtRandomRotationFrom", gParticleDef.rotation.randomDirection )
	enableControl( "P_txtRandomRotationTo", gParticleDef.rotation.randomDirection )

	setEditBoxText( "F_txtGravityCoeff", tostring(gParticleDef.gravityCoeff) )
	enableControl( "F_txtGravityCoeff", gParticleDef.gravityCoeff~=0 )

	setEditBoxText( "F_txtDampingCoeff", tostring(gParticleDef.dampingCoeff) )
	enableControl( "F_txtDampingCoeff", gParticleDef.dampingCoeff~=0 )
	enableControl( "F_cbVelocityFunc", gParticleDef.dampingCoeff~=0 )
	enableControl( "F_cbSizeFunc", gParticleDef.dampingCoeff~=0 )

	addComboBoxItem( "F_cbVelocityFunc", "Linear", DAMPING_MODEL.LINEAR )
	addComboBoxItem( "F_cbVelocityFunc", "Quadratic", DAMPING_MODEL.QUADRATIC )
	selectComboBoxItemByData( "F_cbVelocityFunc", gParticleDef.dampingByVel )

	addComboBoxItem( "F_cbSizeFunc", "Linear", DAMPING_MODEL.LINEAR )
	addComboBoxItem( "F_cbSizeFunc", "Quadratic", DAMPING_MODEL.QUADRATIC )
	selectComboBoxItemByData( "F_cbSizeFunc", gParticleDef.dampingBySize )

	setEditBoxText( "E_txtLife", tostring(gEffectDef.emitterLife) )
	setEditBoxText( "E_txtLifeVariation", tostring(gEffectDef.emitterLifeVar) )
	setEditBoxText( "E_txtCenterX", tostring(gEmitterDef.center.x) )
	setEditBoxText( "E_txtCenterY", tostring(gEmitterDef.center.y) )
	setEditBoxText( "E_txtCenterZ", tostring(gEmitterDef.center.z) )
	setEditBoxText( "E_txtSizeX", tostring(gEmitterDef.size.x) )
	setEditBoxText( "E_txtSizeY", tostring(gEmitterDef.size.y) )
	setEditBoxText( "E_txtSizeZ", tostring(gEmitterDef.size.z) )
	
	ctl = Dialog_GetCheckBox( gDialogHandle, "E_cbEmitIntoObjectSpace" )
	CheckBox_SetChecked( ctl, gEffectDef.emitIntoObjectSpace~=0 )

	local refX, refY = GetMarkerLocation( 1 )
	if refX~=nil and refY~=nil then
		AlignTabControls( 2, refX, refY )
		AlignTabControls( 3, refX, refY )
	end
	SetTabVisible( 2, false )
	SetTabVisible( 3, false )
	SetTabVisible( 1, true )
	gCurrTabIdx = 1

	btnApply_OnButtonClicked()
	SwitchGraph( Dialog_GetButton( gDialogHandle, "P_tabRedFunc" ) )
end

function Dialog_OnDestroy(dialogHandle)
	if gDummyPlacementId~=nil then
		KEP_RemoveParticleSystemFromDynamicObject( gDummyPlacementId )
		KEP_DeleteDynamicObjectPlacement( gDummyPlacementId )
	end
	Helper_Dialog_OnDestroy( dialogHandle )
end

function tabButton_OnButtonClicked( buttonHandle )

	if gCurrTabIdx~=0 then
		SetTabVisible( gCurrTabIdx, false )
		ClearGraph()
	end

	local ctlName = Control_GetName( Button_GetControl(buttonHandle) )
	gCurrTabIdx = 0
	for idx, tabName in pairs(gTabNames) do
		tabFullName = "tab" .. tabName
		if tabFullName==ctlName then
			gCurrTabIdx = idx
			break
		end
	end

	if gCurrTabIdx~=0 then
		SetTabVisible( gCurrTabIdx, true )
		if gGraphMap[gCurrTabIdx]~=nil and gGraphMap[gCurrTabIdx].curr~=nil then
			SwitchGraph( Dialog_GetButton( gDialogHandle, gGraphMap[gCurrTabIdx].curr ) )
		end
	end
end

tabParticle_OnButtonClicked	= tabButton_OnButtonClicked
tabForces_OnButtonClicked	= tabButton_OnButtonClicked
tabEmitter_OnButtonClicked	= tabButton_OnButtonClicked

function SetTabVisible( tabIdx, visible )
	local tabName = gTabNames[tabIdx]
	local tabShortName = gTabShortNames[tabIdx]
	if (tabName == nil or tabShortName == nil) then return end
	
	Log("SetTabVisible: '"..tabName.."' visible="..tostring(visible))
	if visible then
		enableControl( "tab" .. tabName, false )
		showControl( "imgBB" .. tabIdx )
		showControl( "imgWB" .. tabIdx )
	else
		enableControl( "tab" .. tabName, true )
		hideControl( "imgBB" .. tabIdx )
		hideControl( "imgWB" .. tabIdx )
	end

	ShowControlsByPrefix( tabShortName .. "_", visible )

	-- DRF - Added
	if (tabName == "Emitter" and visible) then
		UpdateEmitterShapeControls()
	end
end

function GetMarkerLocation( tabIdx )
	if gTabNames[tabIdx]~=nil and gTabShortNames[tabIdx]~=nil then
		local markerControlName = gTabShortNames[tabIdx]
		local marker = Image_GetControl( Dialog_GetImage( gDialogHandle, markerControlName ) )
		if marker~=nil then
			local markerX = Control_GetLocationX( marker )
			local markerY = Control_GetLocationY( marker )
			return markerX, markerY
		end
	else
		LogError( "GetMarkerLocation: Invalid tab index: " .. tabIdx )
	end
end

function AlignTabControls( tabIdx, refX, refY )
    local markerX, markerY = GetMarkerLocation( tabIdx )
	if markerX~=nil and markerY~=nil then
		local diffX = markerX - refX
		local diffY = markerY - refY
		local tabPrefix = gTabShortNames[tabIdx]
		for i=1,Dialog_GetNumControls( gDialogHandle ) do
			local ctl = Dialog_GetControlByIndex( gDialogHandle, i )
			if (ctl ~= nil) then
				local ctlName = Control_GetName( ctl )
				if (ctlName ~= nil) then
					local matched = string.find(ctlName, tabPrefix .. "_" )==1
					if matched then
						Control_SetLocation( ctl, Control_GetLocationX(ctl)-diffX, Control_GetLocationY(ctl)-diffY )
					end
				end
			end
		end
	end
end

function P_lbImages_OnListBoxSelection( listboxHandle, bFromKeyboard, bMouseDown, sel_index )
	enableControl( "P_btnDelImage", sel_index~=-1 )
	enableControl( "P_btnMoveImageUp", sel_index>0 )
	enableControl( "P_btnMoveImageDown", sel_index~=-1 and sel_index<#gParticleDef.textures.fileNames-1 )
end

function P_rbSpreadImages_OnRadioButtonChanged( radioButtonHandle )
	local checked = CheckBox_GetChecked( RadioButton_GetCheckBox( radioButtonHandle ) )
	if checked~=0 then
		gParticleDef.textures.customFrameRate = 0
	end
	enableControl( "P_txtLoopFPS", checked==0 )
end

function P_rbLoopImages_OnRadioButtonChanged( radioButtonHandle )
	local checked = CheckBox_GetChecked( RadioButton_GetCheckBox( radioButtonHandle ) )
	if checked~=0 then
		local fps = getEditBoxText( "P_txtLoopFPS" )
		gParticleDef.textures.customFrameRate = tonumber( fps )
	end
	enableControl( "P_txtLoopFPS", checked~=0 )
end

function P_txtLoopFPS_OnEditBoxChange( editBoxHandle )
	local val = getEditBoxText( "P_txtLoopFPS" )
	gParticleDef.textures.customFrameRate = tonumber( val )
end

function P_cbAdditiveAlpha_OnCheckBoxChanged( checkBoxHandle, check )
	gParticleDef.textures.additiveAlpha = check
end

function P_cbRandomStartingImage_OnCheckBoxChanged( checkBoxHandle, check )
	gParticleDef.textures.randomInitFrame = check
end

function P_cbRandomXImageMirroring_OnCheckBoxChanged( checkBoxHandle, check )
	gParticleDef.textures.randomXMirroring = check
end

function P_cbRandomYImageMirroring_OnCheckBoxChanged( checkBoxHandle, check )
	gParticleDef.textures.randomYMirroring = check
end

function P_cbRandomRotationDirection_OnCheckBoxChanged( checkBoxHandle, check )
	gParticleDef.rotation.randomDirection = check
	enableControl( "P_txtRandomRotationFrom", check )
	enableControl( "P_txtRandomRotationTo", check )
end

function P_txtRandomRotationFrom_OnEditBoxChange( editBoxHandle )
	local from = getEditBoxText( "P_txtRandomRotationFrom" )
	local to = getEditBoxText( "P_txtRandomRotationTo" )
	gParticleDef.rotation.initAngle = (tonumber( from ) + tonumber( to )) / 2
	gParticleDef.rotation.initAngleVar = (tonumber( to ) - tonumber( from )) / 2
end

function P_txtRandomRotationTo_OnEditBoxChange( editBoxHandle )
	local from = getEditBoxText( "P_txtRandomRotationFrom" )
	local to = getEditBoxText( "P_txtRandomRotationTo" )
	gParticleDef.rotation.initAngle = (tonumber( from ) + tonumber( to )) / 2
	gParticleDef.rotation.initAngleVar = (tonumber( to ) - tonumber( from )) / 2
end

function F_cbGravity_OnCheckBoxChanged( checkBoxHandle, check )
	if check~=0 then
		local coeff = getEditBoxText("F_txtGravityCoeff")
		gParticleDef.gravityCoeff = tonumber( coeff )
	else
		gParticleDef.gravityCoeff = 0
	end

	enableControl( "F_txtGravityCoeff", check )
end

function F_cbDamping_OnCheckBoxChanged( checkBoxHandle, check )
	if check~=0 then
		local coeff = getEditBoxText("F_txtDampingCoeff")
		gParticleDef.dampingCoeff = tonumber( coeff )
	else
		gParticleDef.dampingCoeff = 0
	end

	enableControl( "F_txtDampingCoeff", check )
	enableControl( "F_cbVelocityFunc", check )
	enableControl( "F_cbSizeFunc", check )
end

function F_txtGravityCoeff_OnEditBoxChange( editBoxHandle )
	local val = getEditBoxText( "F_txtGravityCoeff" )
	gParticleDef.gravityCoeff = tonumber( val )
end

function F_txtDampingCoeff_OnEditBoxChange( editBoxHandle )
	local val = getEditBoxText( "F_txtDampingCoeff" )
	gParticleDef.dampingCoeff = tonumber( val )
end

function F_cbVelocityFunc_OnComboBoxSelectionChanged( comboBoxHandle, index, text )
	local sel, data = getComboBoxSelectedItem( F_cbVelocityFunc )
	gParticleDef.dampingByVel = data
end

function F_cbSizeFunc_OnComboBoxSelectionChanged( comboBoxHandle, index, text )
	local sel, data = getComboBoxSelectedItem( F_cbSizeFunc )
	gParticleDef.dampingBySize = data
end

function E_txtLife_OnEditBoxChange( editBoxHandle )
	local val = getEditBoxText( "E_txtLife" )
	gEffectDef.emitterLife = tonumber( val )
end

function E_txtLifeVariation_OnEditBoxChange( editBoxHandle )
	local val = getEditBoxText( "E_txtLifeVariation" )
	gEffectDef.emitterLifeVar = tonumber( val )
end

function E_cbLoop_OnCheckBoxChanged( checkBoxHandle, check )
	gEffectDef.loopEmitter = check
end

function SetEmitterShape(emitterShape)
	gEmitterDef.shape = emitterShape
	UpdateEmitterShapeControls()
end

function UpdateEmitterShapeControls()
	ctl = Dialog_GetRadioButton( gDialogHandle, "E_rbBoxEmitter" )
	RadioButton_SetChecked( ctl, gEmitterDef.shape == EMITTER_SHAPE.BOX )
	ctl = Dialog_GetRadioButton( gDialogHandle, "E_rbSphereEmitter" )
	RadioButton_SetChecked( ctl, gEmitterDef.shape == EMITTER_SHAPE.SPHERE )
	if (gEmitterDef.shape == EMITTER_SHAPE.SPHERE) then
		setControlEnabled("E_txtSizeX", true)
		setControlEnabled("E_txtSizeY", false)
		setControlEnabled("E_txtSizeZ", false)
		setControlText("E_Static8", "Size:")
	else
		setControlEnabled("E_txtSizeX", true)
		setControlEnabled("E_txtSizeY", true)
		setControlEnabled("E_txtSizeZ", true)
		setControlText("E_Static8", "Size:      W                  H                   D")
	end
end

function E_rbBoxEmitter_OnRadioButtonChanged( radioButtonHandle )
	local checked = CheckBox_GetChecked( RadioButton_GetCheckBox( radioButtonHandle ) )
	if checked~=0 then
		SetEmitterShape(EMITTER_SHAPE.BOX)
	end
end

function E_rbSphereEmitter_OnRadioButtonChanged( radioButtonHandle )
	local checked = CheckBox_GetChecked( RadioButton_GetCheckBox( radioButtonHandle ) )
	if checked~=0 then
		SetEmitterShape(EMITTER_SHAPE.SPHERE)
	end
end

function GetEditBoxValue(edText)
	local val = tonumber(getEditBoxText( edText )) or 0
	if (math.abs(val) < .001) then val = 0 end -- drf - crash fix
	return val
end

function E_txtCenterX_OnEditBoxChange( editBoxHandle )
	gEmitterDef.center.x = GetEditBoxValue("E_txtCenterX")
end

function E_txtCenterY_OnEditBoxChange( editBoxHandle )
	gEmitterDef.center.y = GetEditBoxValue("E_txtCenterY")
end

function E_txtCenterZ_OnEditBoxChange( editBoxHandle )
	gEmitterDef.center.z = GetEditBoxValue("E_txtCenterZ")
end

function E_txtSizeX_OnEditBoxChange( editBoxHandle )
	gEmitterDef.size.x = GetEditBoxValue("E_txtSizeX")
end

function E_txtSizeY_OnEditBoxChange( editBoxHandle )
	gEmitterDef.size.y = GetEditBoxValue("E_txtSizeY")
end

function E_txtSizeZ_OnEditBoxChange( editBoxHandle )
	gEmitterDef.size.z = GetEditBoxValue("E_txtSizeZ")
end

function E_cbEmitIntoObjectSpace_OnCheckBoxChanged( checkBoxHandle, check )
	gEffectDef.emitIntoObjectSpace = check
end

function SwitchGraph( tabButtonHandle )
	ClearGraph()
	local graphName = Control_GetName( Button_GetControl( tabButtonHandle ) )
	DrawGraph( gGraphMap[gCurrTabIdx][graphName] )
	if gGraphMap[gCurrTabIdx].curr~=nil then
		enableControl( gGraphMap[gCurrTabIdx].curr, 1 )
	end
	enableControl( graphName, 0 )
	gGraphMap[gCurrTabIdx].curr = graphName
end

P_tabRedFunc_OnButtonClicked = SwitchGraph
P_tabGreenFunc_OnButtonClicked = SwitchGraph
P_tabBlueFunc_OnButtonClicked = SwitchGraph
P_tabAlphaFunc_OnButtonClicked = SwitchGraph
P_tabSizeFunc_OnButtonClicked = SwitchGraph
P_tabRotationFunc_OnButtonClicked = SwitchGraph
E_tabBRFunc_OnButtonClicked = SwitchGraph
E_tabXFunc_OnButtonClicked = SwitchGraph
E_tabYFunc_OnButtonClicked = SwitchGraph
E_tabZFunc_OnButtonClicked = SwitchGraph
E_tabLifeFunc_OnButtonClicked = SwitchGraph
E_tabSizeFunc_OnButtonClicked = SwitchGraph
E_tabRotationFunc_OnButtonClicked = SwitchGraph

function btnApply_OnButtonClicked( buttonHandle )
    Log("PreviewClicked")
	if gParticleGLID==nil then
		-- first time
		local glid = KEP_CreateNewUGCParticleSystem( )
		if glid~=0 then
			Log( "... UGC Particle GLID = " .. glid )
			gParticleGLID = glid
		else
			LogError( "UGC Particle Creation Failed" )
			return
		end
	end

	local ps = KEP_GetParticleSystemByGLID( gParticleGLID )
	if ps~=nil then
		local psName = GetSet_GetString( ps, ParticleSysIds.SYSTEMNAME )
		Log( "... UGC Particle name = " .. psName )
		if psName~=nil then
			if gDummyPlacementId==nil then
				gDummyPlacementId = KEP_PlaceLocalDynamicObj(gDummyDynObjGlid, 1.0, 0, 0 )
			end
			KEP_AddParticleSystemToDynamicObject( gDummyPlacementId, psName )
		end

		GetSetEx_SetSTLStringVector( ps, ParticleSysIds.PARTICLE_TEXTURES_FILENAMES, gParticleDef.textures.fileNames )
		GetSetEx_SetSTLStringVector( ps, ParticleSysIds.PARTICLE_TEXTURES_HANDLES, gParticleDef.textures.handles )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_TEXTURES_CUSTOMFRAMERATE, gParticleDef.textures.customFrameRate )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_TEXTURES_ADDITIVEALPHA, gParticleDef.textures.additiveAlpha )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_TEXTURES_RANDOMINITFRAME, gParticleDef.textures.randomInitFrame )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_TEXTURES_RANDOMXMIRRORING, gParticleDef.textures.randomXMirroring )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_TEXTURES_RANDOMYMIRRORING, gParticleDef.textures.randomYMirroring )
		GetSetEx_SetFunction( ps, ParticleSysIds.PARTICLE_COLORFUNC_R, gParticleDef.colorFunc.r )
		GetSetEx_SetFunction( ps, ParticleSysIds.PARTICLE_COLORFUNC_G, gParticleDef.colorFunc.g )
		GetSetEx_SetFunction( ps, ParticleSysIds.PARTICLE_COLORFUNC_B, gParticleDef.colorFunc.b )
		GetSetEx_SetFunction( ps, ParticleSysIds.PARTICLE_COLORFUNC_A, gParticleDef.colorFunc.a )
		GetSetEx_SetFunction( ps, ParticleSysIds.PARTICLE_SIZEFUNC, gParticleDef.sizeFunc )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_ROTATION_RANDOMDIRECTION, gParticleDef.rotation.randomDirection )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_ROTATION_INITANGLE, gParticleDef.rotation.initAngle )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_ROTATION_INITANGLEVAR, gParticleDef.rotation.initAngleVar )
		GetSetEx_SetFunction( ps, ParticleSysIds.PARTICLE_ROTATION_ANGLEFUNC, gParticleDef.rotation.angleFunc )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_GRAVITYCOEFF, gParticleDef.gravityCoeff )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_DAMPINGCOEFF, gParticleDef.dampingCoeff )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_DAMPINGBYVELOCITY, gParticleDef.dampingByVel )
		GetSet_SetNumber( ps, ParticleSysIds.PARTICLE_DAMPINGBYSIZE, gParticleDef.dampingBySize )
        GetSet_SetNumber( ps, ParticleSysIds.EMITTER_SHAPE, gEmitterDef.shape )
		GetSet_SetVector( ps, ParticleSysIds.EMITTER_CENTER, gEmitterDef.center.x, gEmitterDef.center.y, gEmitterDef.center.z )
		GetSet_SetVector( ps, ParticleSysIds.EMITTER_SIZE, gEmitterDef.size.x, gEmitterDef.size.y, gEmitterDef.size.z )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_BIRTHRATEFUNC, gEmitterDef.birthRateFunc )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_BIRTHRATEVARFUNC, gEmitterDef.birthRateVarFunc )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_PARTICLELIFEFUNC, gEmitterDef.particleLifeFunc )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_PARTICLELIFEVARFUNC, gEmitterDef.particleLifeVarFunc )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_INITVELFUNC_X, gEmitterDef.initVelFunc.x )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_INITVELFUNC_Y, gEmitterDef.initVelFunc.y )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_INITVELFUNC_Z, gEmitterDef.initVelFunc.z )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_INITVELVARFUNC_X, gEmitterDef.initVelVarFunc.x )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_INITVELVARFUNC_Y, gEmitterDef.initVelVarFunc.y )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_INITVELVARFUNC_Z, gEmitterDef.initVelVarFunc.z )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_PTCSIZECOEFFFUNC, gEmitterDef.ptcSizeCoeffFunc )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_PTCSIZECOEFFVARFUNC, gEmitterDef.ptcSizeCoeffVarFunc )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_PTCROTCOEFFFUNC, gEmitterDef.ptcRotCoeffFunc )
		GetSetEx_SetFunction( ps, ParticleSysIds.EMITTER_PTCROTCOEFFVARFUNC, gEmitterDef.ptcRotCoeffVarFunc )
		GetSet_SetNumber( ps, ParticleSysIds.EMITTERLIFE, gEffectDef.emitterLife )
		GetSet_SetNumber( ps, ParticleSysIds.EMITTERLIFEVAR, gEffectDef.emitterLifeVar )
		GetSet_SetNumber( ps, ParticleSysIds.LOOPEMITTER, gEffectDef.loopEmitter )
		GetSet_SetNumber( ps, ParticleSysIds.EMITINTOOBJECTSPACE, gEffectDef.emitIntoObjectSpace )
		GetSet_SetNumber( ps, ParticleSysIds.APPLYCHANGES, 1 )
	end
end

function btnSubmit_OnButtonClicked( buttonHandle )
    Log("SubmitClicked")

	-- Clear and readd all textures
	UGC_ClearUGCObjImagesByType( UGC_TYPE.PARTICLE_EFFECT, gParticleGLID, UGC_TYPE.TEX_UGCDO )
	for i=1,#gParticleDef.textures.handles do
		UGC_AddImageToUGCObj( UGC_TYPE.PARTICLE_EFFECT, gParticleGLID, UGC_TYPE.TEX_UGCDO, gParticleDef.textures.convertedImagePaths[i], gParticleDef.textures.handles[i] )
	end

	-- Notify UGCManager.lua
	UGCI.SendImportCompletionEvent( UGC_TYPE.PARTICLE_EFFECT, gParticleGLID, 0, "ParticleEffect", gDummyPlacementId, 0, 1 )

	-- Reset the submission parameters so that further alterations to the particle can be submitted
	KEP_RemoveParticleSystemFromDynamicObject( gDummyPlacementId )
	ps = nil
	psName = nil
	gDummyPlacementID = nil
	gParticleGLID = nil
	btnApply_OnButtonClicked( buttonHandle )
end

function P_btnAddImage_OnButtonClicked( buttonHandle )
	local event = KEP_EventCreate( PARTICLE_EDITOR_TEXTURE_BROWSER_CLOSED_EVENT )
	UGC_BrowseFileByImportType( UGC_TYPE.TEXTURE, event ) -- second param (initial folder) TBD
end

function P_btnDelImage_OnButtonClicked( buttonHandle )
	local idx, _, data = getListBoxSelectedItem( "P_lbImages" )
	if idx~=nil then
		table.remove( gParticleDef.textures.fileNames, idx + 1 )
		table.remove( gParticleDef.textures.convertedImagePaths, idx + 1 )
		table.remove( gParticleDef.textures.handles, idx + 1 )
		removeListBoxItem( "P_lbImages", idx )
	end
end

function P_btnMoveImageUp_OnButtonClicked( buttonHandle )
	local idx, text, data = getListBoxSelectedItem( "P_lbImages" )
	if idx~=nil and idx>0 then
		local tmp = gParticleDef.textures.fileNames[idx]
		gParticleDef.textures.fileNames[idx] = gParticleDef.textures.fileNames[idx+1]
		gParticleDef.textures.fileNames[idx+1] = tmp
		tmp = gParticleDef.textures.convertedImagePaths[idx]
		gParticleDef.textures.convertedImagePaths[idx] = gParticleDef.textures.convertedImagePaths[idx+1]
		gParticleDef.textures.convertedImagePaths[idx+1] = tmp
		tmp = gParticleDef.textures.handles[idx]
		gParticleDef.textures.handles[idx] = gParticleDef.textures.handles[idx+1]
		gParticleDef.textures.handles[idx+1] = tmp
		
		removeListBoxItem( "P_lbImages", idx )
		insertListBoxItem( "P_lbImages", idx-1, text, data )
		selectListBoxItemByIndex( "P_lbImages", idx-1 )
	end
end

function P_btnMoveImageDown_OnButtonClicked( buttonHandle )
	local idx, text, data = getListBoxSelectedItem( "P_lbImages" )
	if idx~=nil and idx<#gParticleDef.textures.fileNames-1 then
		local tmp = gParticleDef.textures.fileNames[idx+1]
		gParticleDef.textures.fileNames[idx+1] = gParticleDef.textures.fileNames[idx+2]
		gParticleDef.textures.fileNames[idx+2] = tmp
		tmp = gParticleDef.textures.convertedImagePaths[idx+1]
		gParticleDef.textures.convertedImagePaths[idx+1] = gParticleDef.textures.convertedImagePaths[idx+2]
		gParticleDef.textures.convertedImagePaths[idx+2] = tmp
		tmp = gParticleDef.textures.handles[idx+1]
		gParticleDef.textures.handles[idx+1] = gParticleDef.textures.handles[idx+2]
		gParticleDef.textures.handles[idx+2] = tmp
		
		removeListBoxItem( "P_lbImages", idx )
		insertListBoxItem( "P_lbImages", idx+1, text, data )
		selectListBoxItemByIndex( "P_lbImages", idx+1 )
	end
end

function TextureBrowserClosedHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	local dialogResult = KEP_EventDecodeNumber( event )
	local filePath = ""
	if dialogResult~=0 then
		filePath = KEP_EventDecodeString( event )
	else
		return
	end

	local dropId = gNextDropId
	gNextDropId = gNextDropId + 1

	local importInfo = {}
	importInfo.fullPath = filePath
	importInfo.sessionId = -1
	importInfo.category = UGC_TYPE.TEXTURE
	importInfo.type = UGC_TYPE.TEXTURE

	Log( "TextureBrowserClosedHandler: Import particle texture From " .. filePath )
	UGCI.CallUGCHandler( "TextureImport.xml", 0, dropId, importInfo, gTarget, 0, 0 )
end

function TextureImportProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	if filter==UGCIS.PROCESSED then
		if objectid~=nil then
			ImpSessionId = objectid
			ImpSession = UGC_GetImportSession( ImpSessionId )
			if ImpSession~=nil then
				local targetType = GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETTYPE )
				local targetObjId= GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETID )
				local targetX = GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETPOS_X )
				local targetY = GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETPOS_Y )
				local targetZ = GetSet_GetNumber( ImpSession, TextureImportStateIds.TARGETPOS_Z )
				local convertedImagePath = GetSet_GetString( ImpSession, TextureImportStateIds.CONVERTEDIMAGEPATH )
		 		local fullPath = GetSet_GetString( ImpSession, TextureImportStateIds.SOURCEIMAGEPATH )
		 		local textureKey = GetSet_GetString( ImpSession, TextureImportStateIds.TEXTUREKEY )
		 		
		 		if targetType==ObjectType.UNKNOWN and fullPath~=nil and convertedImagePath~=nil then
					Log( "Particle texture [" .. fullPath .. "] imported as [" .. convertedImagePath .. "]" )
		 			
		 			table.insert( gParticleDef.textures.fileNames, fullPath )
		 			table.insert( gParticleDef.textures.convertedImagePaths, convertedImagePath )
		 			table.insert( gParticleDef.textures.handles, textureKey )
		 			
		 			fullPath = string.lower(fullPath)
		 			if string.len(fullPath)>25 then
		 				fullPath = "... " .. string.sub( fullPath, -20 )
		 			end
		 			addListBoxItem( "P_lbImages", fullPath, textureKey )		
				end
			end
		end
	end
end

function GetSetEx_SetSTLStringVector( gsVar, gsId, dataTable )
	local gsVector = GetSet_GetObject( gsVar, gsId )
	if gsVector~=nil then
		-- clear array
		GetSet_SetNumber( gsVector, -1, 0 )
		-- append string
		local idx, val
		for idx, val in ipairs(dataTable) do
			GetSet_SetString( gsVector, idx-1, val )
		end
	end
end

function GetSetEx_SetFunction( gsVar, gsId, functionTable )
	local gsFunc = GetSet_GetObject( gsVar, gsId )
	if gsFunc~=nil then
		-- clear array
		GetSet_SetNumber( gsFunc, -1, 0 )
		-- append data
		local key, val
		for key, val in pairs(functionTable) do
			if key>=0 then
				GetSet_SetNumber( gsFunc, key, val )
			else
				--TODO: log error
			end
		end
	end
end

function ShowControlsByPrefix( prefix, visible )
	for i=1,Dialog_GetNumControls( gDialogHandle ) do
		local ctl = Dialog_GetControlByIndex( gDialogHandle, i )
		if (ctl ~= nil) then 
			local ctlName = Control_GetName( ctl )
			if (ctlName ~= nil) then
				local matched = string.find(ctlName, prefix )==1
				if matched then
					Control_SetVisible( ctl, visible )
				end
			end
		end
	end
end

--  Graph Editor

SLOPE = { LEVEL = 1, UP = 2, DOWN = 3 }
HANDLE_STATES = { NORMAL = 1, SELECTED = 2 }
gGraphHandleColors = { 0xff202020, 0xffc02020 }

gGraphLineSegs = {}			-- all images (IMenuControl type) used for rendering line segments in graph
gGraphHandles = {}			-- all images (IMenuControl type) used for rendering drag handles in graph
gGraphVarHandles = {}
gGraphVarIndicators = {}
gHandleW = 10				-- dimensions used while rendering drag handles
gHandleH = 10				-- dimensions used while rendering drag handles
gGraphXScale = 1000			-- X [0,1] to data key scaling (key [0, 1000], each unit represents 1/1000 of total time) ---- Note: Unlike X scaling, Y scaling is per graph.
gVarIndicatorW = 4
gMinDiffX = gHandleW

gGraphX, gGraphY, gGraphW, gGraphH = nil, nil, nil, nil		-- graph boundaries
gCurrGraphSettings = nil									-- settings for current graph (incl data, zoom, scaling, etc)
gHandleDragInfo = nil										-- data related to handle dragging
gGraphHandleTemplateCtl = nil								-- template menu control for rendering drag handles
gGraphLineTemplateCtl = nil									-- template menu control for rendering line segments
gGraphVarHandleTemplateCtl = nil
gGraphVarIndicatorTemplateCtl = nil
gTexCoords = { level={}, vert={}, gentleDn={}, slopeDn={}, steepDn={}, gentleUp={}, slopeUp={}, steepUp={} }	-- texture coordinates used for rendering line segments of different slope types
gSelectedHandleIndex = nil
gVarHandleSelected = false
gHoveredHandleIndex = nil

function imgGraphSurface_OnLButtonDown( controlHandle, x, y )

	local keyCount = #gCurrGraphSettings.sortedKeys
	if gCurrGraphSettings==nil or keyCount==0 then
		return
	end

	-- clicked inside graph
	ClearHandleSelection()

	-- check handle-click
	for i=1,keyCount do
		if Control_ContainsPoint( gGraphHandles[i], x, y )~=0 then
			-- handle clicked
			SelectHandle( i, false )
			StartDraggingHandle( i, x, y )
			return
		elseif gCurrGraphSettings.var~=nil then
			local key = gCurrGraphSettings.sortedKeys[i]
			if gCurrGraphSettings.var[key]~=nil and gCurrGraphSettings.var[key]~=0 and Control_ContainsPoint( gGraphVarHandles[i], x, y )~=0 then
				-- variation handle clicked
				SelectHandle( i, true )
				StartDraggingHandle( i, x, y )
				return
			end
		end
	end

	-- check line-click
	local prevKey, prevVal
	local idx, key
	for idx, key in ipairs(gCurrGraphSettings.sortedKeys) do
		local val = gCurrGraphSettings.base[key]
		if prevKey~=nil then
			local x1, y1 = CalcCoordsByKeyValuePair( prevKey, prevVal )
			local x2, y2 = CalcCoordsByKeyValuePair( key, val )
	
			if x1+gMinDiffX<=x and x<=x2-gMinDiffX then
				local yy = y1 + (y2 - y1) * (x - x1) / (x2 - x1)
				if math.abs( y - yy ) <= gHandleH then
					InsertHandle( x, y )
					SelectHandle( idx, false )
					StartDraggingHandle( idx, x, y )
					return
				end
			end
		end

		prevKey = key
		prevVal = val
	end
end

function imgGraphSurface_OnRButtonDown( controlHandle, x, y )
	if gHandleDragInfo~=nil then		-- dragging
		EndDraggingHandle( true )
	end
end

function Dialog_OnMouseMove( dialogHandle, x, y )
	if gHandleDragInfo~=nil then		-- dragging
		-- update coords
		DraggingHandle( x, y )
		if Dialog_IsLMouseDown(gDialogHandle)==0 then
			-- release if necessary
			EndDraggingHandle( false )
		end
	end
end

function imgGraphSurface_OnLButtonUp( controlHandle, x, y )
	if gHandleDragInfo~=nil then		-- dragging
		-- update coords
		DraggingHandle( x, y )
		-- release
		EndDraggingHandle( false )
	end
end

function G_btnSetItem_OnButtonClicked( buttonHandle )
	if gSelectedHandleIndex~=nil then
	
		local newKey = tonumber( getEditBoxText( "G_txtTimeEdit" ) )
		local newVal = tonumber( getEditBoxText( "G_txtValueEdit" ) )
		if newKey==nil or newVal==nil then
			KEP_MessageBox( "Please enter only numbers in time and value box" )
			return
		end

		if math.floor(newKey)==newKey and newKey<0 or newKey>1000 then
			KEP_MessageBox( "Time must be integer between 0 and 1000" )
			return
		end
		
		newKey = math.floor(newKey)

		local oldKey = gCurrGraphSettings.sortedKeys[gSelectedHandleIndex]
		local minKey, maxKey = oldKey, oldKey
		local minVal, maxVal = gCurrGraphSettings.offsetY, gCurrGraphSettings.scaleY + gCurrGraphSettings.offsetY
		
		if gVarHandleSelected then
			if newKey~=oldKey then
				-- should be guranteed by UI but check anyway
				KEP_MessageBox( "Time should not be modified while setting new variation" )
			end
		else
			if gSelectedHandleIndex>1 and gSelectedHandleIndex<#gCurrGraphSettings.sortedKeys then
				minKey = gCurrGraphSettings.sortedKeys[gSelectedHandleIndex-1] + 1
				maxKey = gCurrGraphSettings.sortedKeys[gSelectedHandleIndex+1] - 1
			end
		end
		
		if newKey<minKey or newKey>maxKey then
			KEP_MessageBox( "Time must be between " .. minKey .. " and " .. maxKey )
			return
		end
		
		if newVal<minVal or newVal>maxVal then
			KEP_MessageBox( "Value must be between " .. minVal .. " and " .. maxVal )
			return
		end

		if newKey~=oldKey and gCurrGraphSettings.base[newKey]~=nil then
			KEP_MessageBox( "Value already exists at time: " .. newKey )
			return
		end
		
		UpdateHandle( gSelectedHandleIndex, newKey, newVal, gVarHandleSelected )
	end
end

function G_btnDeleteItem_OnButtonClicked( buttonHandle )
	if gSelectedHandleIndex~=nil then
		if gVarHandleSelected or gSelectedHandleIndex~=1 and gSelectedHandleIndex~=#gCurrGraphSettings.sortedKeys then
			RemoveHandle( gSelectedHandleIndex, gVarHandleSelected )
		end
	end
end

function G_btnAddVariation_OnButtonClicked( buttonHandle )
	if gSelectedHandleIndex~=nil and gCurrGraphSettings.var~=nil and not gVarHandleSelected then
		local key = gCurrGraphSettings.sortedKeys[gSelectedHandleIndex]
		if key~=nil then
		
			if gCurrGraphSettings.randomized~=true then
				-- scan existing keys and add dummy var data if missing
				local tmpKey
				for _, tmpKey in ipairs(gCurrGraphSettings.sortedKeys) do
					if gCurrGraphSettings.var[tmpKey]==nil then
						gCurrGraphSettings.var[tmpKey] = 0
					end
				end
				
				gCurrGraphSettings.randomized = true
			end

			gCurrGraphSettings.var[key] = gCurrGraphSettings.scaleY * gCurrGraphSettings.zoomY / 10
			
			enableControl( "G_btnAddVariation", false )
			RedrawCurrentGraph()
		end
	end
end

function G_btnZoomIn_OnButtonClicked( buttonHandle )
	gCurrGraphSettings.zoomY = gCurrGraphSettings.zoomY / 2
	RedrawCurrentGraph()
end

function G_btnZoomOut_OnButtonClicked( buttonHandle )
	gCurrGraphSettings.zoomY = math.min(gCurrGraphSettings.zoomY * 2, 1)
	RedrawCurrentGraph()
end

-- Drawing graph

function RedrawCurrentGraph()
	-- redraw graph
	local graphToDraw = gCurrGraphSettings
	ClearGraph()
	DrawGraph( graphToDraw )
end

function ClearGraph()

	-- hide all graph controls
	ShowControlsByPrefix( "G_", 0 )

	ClearHandleSelection()
	for _, ctl in ipairs(gGraphLineSegs) do
		Control_SetVisible( ctl, 0 )
	end
	for _, ctl in ipairs(gGraphHandles) do
		Control_SetVisible( ctl, 0 )
	end
	for _, ctl in ipairs(gGraphVarHandles) do
		Control_SetVisible( ctl, 0 )
	end
	for _, ctl in ipairs(gGraphVarIndicators) do
		Control_SetVisible( ctl, 0 )
	end

	gCurrGraphSettings = nil
end

function DrawGraph( graphSettings )

	-- show all graph controls
	ShowControlsByPrefix( "G_", 1 )

	-- save current graph
	gCurrGraphSettings = graphSettings

	if gCurrGraphSettings.base==nil or gCurrGraphSettings.base[0]==nil then	-- key '0' is required
		-- empty table
		return
	end

	-- One-time intializations for global data
	if gGraphX==nil then
		local ctl = Dialog_GetControl( gDialogHandle, "G_imgBackground" )
		gGraphX = Control_GetLocationX( ctl )
		gGraphY = Control_GetLocationY( ctl )
		gGraphW = Control_GetWidth( ctl )
		gGraphH = Control_GetHeight( ctl )
	end

	if gGraphHandleTemplateCtl==nil then
		gGraphHandleTemplateCtl = Dialog_GetControl( gDialogHandle, "imgGraphHandle" )
		gGraphLineTemplateCtl = Dialog_GetControl( gDialogHandle, "imgGraphLineLevel" )
		gGraphVarHandleTemplateCtl = Dialog_GetControl( gDialogHandle, "imgGraphVarHandle" )		-- to be changed
		gGraphVarIndicatorTemplateCtl = Dialog_GetControl( gDialogHandle, "imgGraphLineVert" )	-- to be changed
	end

	if gTexCoords.level.left==nil then
		local img = Dialog_GetImage( gDialogHandle, "imgGraphLineLevel" )
		local elem = Image_GetDisplayElement( img )
		gTexCoords.level.left = Element_GetCoordLeft( elem )
		gTexCoords.level.top = Element_GetCoordTop( elem )
		gTexCoords.level.right = Element_GetCoordRight( elem )
		gTexCoords.level.bottom = Element_GetCoordBottom( elem )

		local img = Dialog_GetImage( gDialogHandle, "imgGraphLineVert" )
		local elem = Image_GetDisplayElement( img )
		gTexCoords.vert.left = Element_GetCoordLeft( elem )
		gTexCoords.vert.top = Element_GetCoordTop( elem )
		gTexCoords.vert.right = Element_GetCoordRight( elem )
		gTexCoords.vert.bottom = Element_GetCoordBottom( elem )

		img = Dialog_GetImage( gDialogHandle, "imgGraphLineSlopeUp" )
		elem = Image_GetDisplayElement( img )
		gTexCoords.slopeUp.left = Element_GetCoordLeft( elem )
		gTexCoords.slopeUp.top = Element_GetCoordTop( elem )
		gTexCoords.slopeUp.right = Element_GetCoordRight( elem )
		gTexCoords.slopeUp.bottom = Element_GetCoordBottom( elem )

		img = Dialog_GetImage( gDialogHandle, "imgGraphLineSlopeDown" )
		elem = Image_GetDisplayElement( img )
		gTexCoords.slopeDn.left = Element_GetCoordLeft( elem )
		gTexCoords.slopeDn.top = Element_GetCoordTop( elem )
		gTexCoords.slopeDn.right = Element_GetCoordRight( elem )
		gTexCoords.slopeDn.bottom = Element_GetCoordBottom( elem )

		img = Dialog_GetImage( gDialogHandle, "imgGraphLineGentleSlopeUp" )
		elem = Image_GetDisplayElement( img )
		gTexCoords.gentleUp.left = Element_GetCoordLeft( elem )
		gTexCoords.gentleUp.top = Element_GetCoordTop( elem )
		gTexCoords.gentleUp.right = Element_GetCoordRight( elem )
		gTexCoords.gentleUp.bottom = Element_GetCoordBottom( elem )

		img = Dialog_GetImage( gDialogHandle, "imgGraphLineGentleSlopeDown" )
		elem = Image_GetDisplayElement( img )
		gTexCoords.gentleDn.left = Element_GetCoordLeft( elem )
		gTexCoords.gentleDn.top = Element_GetCoordTop( elem )
		gTexCoords.gentleDn.right = Element_GetCoordRight( elem )
		gTexCoords.gentleDn.bottom = Element_GetCoordBottom( elem )

		img = Dialog_GetImage( gDialogHandle, "imgGraphLineSteepUp" )
		elem = Image_GetDisplayElement( img )
		gTexCoords.steepUp.left = Element_GetCoordLeft( elem )
		gTexCoords.steepUp.top = Element_GetCoordTop( elem )
		gTexCoords.steepUp.right = Element_GetCoordRight( elem )
		gTexCoords.steepUp.bottom = Element_GetCoordBottom( elem )

		img = Dialog_GetImage( gDialogHandle, "imgGraphLineSteepDown" )
		elem = Image_GetDisplayElement( img )
		gTexCoords.steepDn.left = Element_GetCoordLeft( elem )
		gTexCoords.steepDn.top = Element_GetCoordTop( elem )
		gTexCoords.steepDn.right = Element_GetCoordRight( elem )
		gTexCoords.steepDn.bottom = Element_GetCoordBottom( elem )
	end

	-- Mark Y-axis min/max values
	local xx, yMin, yMax
	xx, yMin = CalcKeyValuePairByCoords( gGraphX, gGraphY + gGraphH )
	setStaticText( "G_lblYMin", string.format( gCurrGraphSettings.fmtY, yMin ) )
	xx, yMax = CalcKeyValuePairByCoords( gGraphX, gGraphY )
	setStaticText( "G_lblYMax", string.format( gCurrGraphSettings.fmtY, yMax ) )

	-- Update zooming buttons
	enableControl( "G_btnZoomOut", gCurrGraphSettings.zoomY<1 )

	-- Pre-sort table keys
	gCurrGraphSettings.sortedKeys = {}
	local key, val
    for key, val in pairs(gCurrGraphSettings.base) do
        table.insert( gCurrGraphSettings.sortedKeys, key )
    end
    table.sort( gCurrGraphSettings.sortedKeys )

	-- Prepare dialog controls for rendering
	local idx
	local prevKey, prevVal
	for idx, key in ipairs( gCurrGraphSettings.sortedKeys ) do

		val = gCurrGraphSettings.base[key]

		if #gGraphVarIndicators<idx then
			AddNewControlToArray( gGraphVarIndicators, gGraphVarIndicatorTemplateCtl )
		end

		if #gGraphHandles<idx then
			AddNewControlToArray( gGraphHandles, gGraphHandleTemplateCtl )
		end

		if #gGraphLineSegs<idx then
			AddNewControlToArray( gGraphLineSegs, gGraphLineTemplateCtl )
		end

		if #gGraphVarHandles<idx then
			AddNewControlToArray( gGraphVarHandles, gGraphVarHandleTemplateCtl )
		end

		if idx>1 then
			PlaceLineSeg( idx-1, prevKey, prevVal, key, val, false )
		end

		PlaceHandle( idx, key, val, false )

		if gCurrGraphSettings.var~=nil and gCurrGraphSettings.var[key]~=nil and gCurrGraphSettings.var[key]~=0 then
			local variation = gCurrGraphSettings.var[key]
			local upperBound = val + variation
			local lowerBound = val - variation
			PlaceHandle( idx, key, upperBound, true )
			PlaceLineSeg( idx, key, upperBound, key, lowerBound, true )
		end

		prevKey = key
		prevVal = val
	end
end

-- Menu controls used for graph rendering

function AddNewControlToArray( array, template )
	-- Note: I have to assume new control is always pasted to the end
	-- Otherwise there is no way to obtain the handle of the new control.
	local newCtlIndex = Dialog_GetNumControls( gDialogHandle )
	Dialog_CopyControl( gDialogHandle, template )
	Dialog_PasteControl( gDialogHandle )
	local newCtl = Dialog_GetControlByIndex( gDialogHandle, newCtlIndex )
	table.insert( array, newCtl )

	-- shift it backward to allow graph surface on the top
	Dialog_MoveControlBackward( gDialogHandle, newCtl )
end

function PlaceHandleAt( handleIndex, x, y, bIsVarInd )
	local ctl
	if not bIsVarInd then
		ctl = gGraphHandles[handleIndex]
	else
		ctl = gGraphVarHandles[handleIndex]
	end
	if y<gGraphY or y>gGraphY+gGraphH then
		Control_SetVisible( ctl, 0 )
	else
		Control_SetLocation( ctl, x - gHandleW/2, y - gHandleH/2 )
		Control_SetVisible( ctl, 1 )
		Control_SetSize( ctl, gHandleW, gHandleH )
	end
end

function PlaceLineSegAt( lineIndex, x1, y1, x2, y2, bIsVarInd )

	if gGraphLineSegs==nil then
		return
	end

	local ctlLine
	if not bIsVarInd then
		ctlLine = gGraphLineSegs[lineIndex]
	else
		ctlLine = gGraphVarIndicators[lineIndex]
	end

	local len = math.sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) )
	local slope = (y2-y1) / math.max(x2-x1,1)	-- x2 always >= x1

	-- boundary check
	local left, top, right, bottom = x1, math.min(y1, y2), x2, math.max(y1, y2)
	if bottom<gGraphY or top>gGraphY+gGraphH then
		-- completely outside
		Control_SetVisible( ctlLine, 0 )

	else
		if top<gGraphY then
			if slope>0 then
				left = left + math.floor((right - left) * (gGraphY - top) / (bottom - top))
			elseif slope<0 then
				right = left + math.floor((right - left) * (bottom - gGraphY) / (bottom - top))
			end
			top = gGraphY
		end

		if bottom>gGraphY+gGraphH then
			if slope>0 then
				right = left + math.floor((right - left) * (gGraphY + gGraphH - top) / (bottom - top))
			elseif slope<0 then
				left = left + math.floor((right - left) * (bottom - gGraphY - gGraphH) / (bottom - top))
			end
			bottom = gGraphY+gGraphH
		end

		Control_SetVisible( ctlLine, 1 )
		if not bIsVarInd then
			Control_SetLocation( ctlLine, left, top )
			Control_SetSize( ctlLine, right-left+1, bottom-top+1 )
		else
			Control_SetLocation( ctlLine, left-gVarIndicatorW/2, top )
			Control_SetSize( ctlLine, right-left+gVarIndicatorW, bottom-top )
		end
	end

	local img = Control_GetImage(ctlLine)
	local elem = Image_GetDisplayElement( img )
	if math.abs(y1-y2)<3 then			-- LEVEL
		Element_SetCoords( elem, gTexCoords.level.left, gTexCoords.level.top, gTexCoords.level.right, gTexCoords.level.bottom )
	elseif math.abs(x1-x2)<2 then		-- VERTICAL
		Element_SetCoords( elem, gTexCoords.vert.left, gTexCoords.vert.top, gTexCoords.vert.right, gTexCoords.vert.bottom )
	elseif slope>7 then					-- STEEP DOWNWARD
		Element_SetCoords( elem, gTexCoords.steepDn.left, gTexCoords.steepDn.top, math.min(gTexCoords.steepDn.right, gTexCoords.steepDn.left+len), math.min(gTexCoords.steepDn.bottom, gTexCoords.steepDn.top+len) )
	elseif slope>0.1 then					-- SLOPE DOWNWARD
		Element_SetCoords( elem, gTexCoords.slopeDn.left, gTexCoords.slopeDn.top, math.min(gTexCoords.slopeDn.right, gTexCoords.slopeDn.left+len), math.min(gTexCoords.slopeDn.bottom, gTexCoords.slopeDn.top+len) )
	elseif slope>0 then					-- GENTLE SLOPE DOWNWARD
		Element_SetCoords( elem, gTexCoords.gentleDn.left, gTexCoords.gentleDn.top, math.min(gTexCoords.gentleDn.right, gTexCoords.gentleDn.left+len), math.min(gTexCoords.gentleDn.bottom, gTexCoords.gentleDn.top+len) )
	elseif slope>=-0.1 then				-- GENTLE SLOPE UPWARD
		Element_SetCoords( elem, gTexCoords.gentleUp.left, math.max(gTexCoords.gentleUp.top, gTexCoords.gentleUp.bottom-len), math.min(gTexCoords.gentleUp.right, gTexCoords.gentleUp.left+len), gTexCoords.gentleUp.bottom )
	elseif slope>=-7 then				-- SLOPE UPWARD
		Element_SetCoords( elem, gTexCoords.slopeUp.left, math.max(gTexCoords.slopeUp.top, gTexCoords.slopeUp.bottom-len), math.min(gTexCoords.slopeUp.right, gTexCoords.slopeUp.left+len), gTexCoords.slopeUp.bottom )
	else								-- STEEP UPWARD
		Element_SetCoords( elem, gTexCoords.steepUp.left, math.max(gTexCoords.steepUp.top, gTexCoords.steepUp.bottom-len), math.min(gTexCoords.steepUp.right, gTexCoords.steepUp.left+len), gTexCoords.steepUp.bottom )
	end
end

function PlaceHandle( handleIndex, key, val, bIsVarInd )
	local x, y = CalcCoordsByKeyValuePair( key, val )
	PlaceHandleAt( handleIndex, x, y, bIsVarInd )
end

function PlaceLineSeg( lineIndex, key1, val1, key2, val2, bIsVarInd )
	local ctl = gGraphLineSegs[lineIndex]
	local x1, y1 = CalcCoordsByKeyValuePair( key1, val1 )
	local x2, y2 = CalcCoordsByKeyValuePair( key2, val2 )
	PlaceLineSegAt( lineIndex, x1, y1, x2, y2, bIsVarInd )
end

function ClearHandleSelection()
	if gSelectedHandleIndex~=nil then
		SetHandleState( gSelectedHandleIndex, HANDLE_STATES.NORMAL, gVarHandleSelected )
		gSelectedHandleIndex = nil
		gVarHandleSelected = false
	end
	DisableKeyValueEditBoxes()
	enableControl( "G_btnSetItem", false )
	enableControl( "G_btnDeleteItem", false )
	enableControl( "G_btnAddVariation", false )
end

function SelectHandle( handleIndex, bIsVarInd )
	gSelectedHandleIndex = handleIndex
	gVarHandleSelected = bIsVarInd
	SetHandleState( gSelectedHandleIndex, HANDLE_STATES.SELECTED, gVarHandleSelected )
	
	enableControl( "G_btnSetItem", gSelectedHandleIndex~=nil )
	enableControl( "G_btnDeleteItem", bIsVarInd or gSelectedHandleIndex~=nil and gSelectedHandleIndex~=1 and gSelectedHandleIndex~=#gCurrGraphSettings.sortedKeys )
	
	if not bIsVarInd then
		local enableVarBtn = false
		if gSelectedHandleIndex~=nil and gCurrGraphSettings.var~=nil then
			local key = gCurrGraphSettings.sortedKeys[gSelectedHandleIndex]
			if key~=nil and (gCurrGraphSettings.var[key]==nil or gCurrGraphSettings.var[key]==0) then
				enableVarBtn = true
			end
		end
		enableControl( "G_btnAddVariation", enableVarBtn )
	end
end

function InsertHandle( x, y )

	-- Update data.
	-- NOTE: sortedKeys table will be updated in DrawGraph call
	local key, val = CalcKeyValuePairByCoords( x, y )
	gCurrGraphSettings.base[key] = val
	if gCurrGraphSettings.var~=nil and gCurrGraphSettings.randomized==true then
		gCurrGraphSettings.var[key] = 0
	end

	RedrawCurrentGraph()
end

function RemoveHandle( handleIndex, bIsVarInd )
	local key = gCurrGraphSettings.sortedKeys[handleIndex]
	if key~=nil then
		if not bIsVarInd then
			gCurrGraphSettings.base[key] = nil
			if gCurrGraphSettings.var~=nil then
				gCurrGraphSettings.var[key] = nil
			end
		else
			gCurrGraphSettings.var[key] = 0
		end

		RedrawCurrentGraph()
	end
end

function SetHandleState( handleIndex, state, bIsVarInd )
	local ctl
	if not bIsVarInd then
		ctl = gGraphHandles[handleIndex]
	else
		ctl = gGraphVarHandles[handleIndex]
	end
	local img = Control_GetImage( ctl )
	local elem = Image_GetDisplayElement( img )
	local bc = Element_GetTextureColor( elem )
	local color = gGraphHandleColors[state] or 0xff000000
	BlendColor_SetColorRaw( bc, 0, color )
end

function UpdateHandle( handleIndex, newKey, newVal, bIsVarInd )

	local oldKey = gCurrGraphSettings.sortedKeys[handleIndex]

	if not bIsVarInd then
		-- Move drag handle
		PlaceHandle( handleIndex, newKey, newVal, false )

		-- Move line segments
		local prevKey = gCurrGraphSettings.sortedKeys[handleIndex-1]
		if prevKey~=nil then
			local prevVal = gCurrGraphSettings.base[prevKey]
			PlaceLineSeg( handleIndex-1, prevKey, prevVal, newKey, newVal, false )
		end

		local nextKey = gCurrGraphSettings.sortedKeys[handleIndex+1]
		if nextKey~=nil then
			local nextVal = gCurrGraphSettings.base[nextKey]
			PlaceLineSeg( handleIndex, newKey, newVal, nextKey, nextVal, false )
		end
		
		-- Move drag handle for variation indicator
		if gCurrGraphSettings.var~=nil and gCurrGraphSettings.var[oldKey]~=nil and gCurrGraphSettings.var[oldKey]~=0 then
			PlaceHandle( handleIndex, newKey, newVal+gCurrGraphSettings.var[oldKey], true )
			PlaceLineSeg( handleIndex, newKey, newVal+gCurrGraphSettings.var[oldKey], newKey, newVal-gCurrGraphSettings.var[oldKey], true )
		end
		
		local savedVariation = nil
		if gCurrGraphSettings.var~=nil then
			savedVariation = gCurrGraphSettings.var[oldKey]
		end

		if oldKey~=newKey then
			gCurrGraphSettings.base[oldKey] = nil
			if savedVariation~=nil then
				gCurrGraphSettings.var[oldKey] = nil
			end
			gCurrGraphSettings.sortedKeys[handleIndex] = newKey
		end

		gCurrGraphSettings.base[newKey] = newVal
		if savedVariation~=nil then
			gCurrGraphSettings.var[newKey] = savedVariation
		end
		
	else
		-- Move drag handle
		if newVal==0 then
			-- hide handle if no value
			Control_SetVisible( gGraphVarHandles[handleIndex], false )
		else
			-- Move drag handle for variation indicator
			PlaceHandle( handleIndex, newKey, gCurrGraphSettings.base[newKey]+newVal, true )

			-- Move line segments
			PlaceLineSeg( handleIndex, newKey, gCurrGraphSettings.base[newKey]+newVal, newKey, gCurrGraphSettings.base[newKey]-newVal, true )
		end
	
		gCurrGraphSettings.var[newKey] = newVal
	end
end

-----------------------------------------
-- Graph to screen coordinates conversion
-----------------------------------------

function CalcKeyValuePairByCoords( x, y )
	local deltaX = x - gGraphX
	local deltaY = gGraphY + gGraphH - y
	deltaX = math.max( math.min( deltaX, gGraphW ), 0 )
	deltaY = math.max( math.min( deltaY, gGraphH ), 0 )
	local key = math.floor( deltaX * gGraphXScale / gGraphW )
	local val = math.floor( ((deltaY * gCurrGraphSettings.scaleY / gGraphH + gCurrGraphSettings.offsetY) * gCurrGraphSettings.zoomY) / gCurrGraphSettings.resY ) * gCurrGraphSettings.resY
	return key, val
end

function CalcCoordsByKeyValuePair( key, val )
	local x = math.floor(gGraphX + gGraphW * key / gGraphXScale)
	local y = math.floor(gGraphY + gGraphH - gGraphH * (val / gCurrGraphSettings.zoomY - gCurrGraphSettings.offsetY) / gCurrGraphSettings.scaleY)
	return x, y
end

-----------------------------------------
-- Drag handle manipulation
-----------------------------------------

function StartDraggingHandle( handleIndex, x, y )
	gHandleDragInfo = {}
	gHandleDragInfo.handleIndex = handleIndex
	
	local key = gCurrGraphSettings.sortedKeys[handleIndex]
	local baseVal = gCurrGraphSettings.base[key]
	local maxVal = gCurrGraphSettings.base[key]
	if gCurrGraphSettings.var~=nil and gCurrGraphSettings.var[key]~=nil then
		maxVal = maxVal + gCurrGraphSettings.var[key]
	end
	
	if gVarHandleSelected then
		gHandleDragInfo.startX, gHandleDragInfo.startY = CalcCoordsByKeyValuePair( key, maxVal )
		_, gHandleDragInfo.baseY = CalcCoordsByKeyValuePair( key, baseVal )
	else
		gHandleDragInfo.startX, gHandleDragInfo.startY = CalcCoordsByKeyValuePair( key, baseVal )
		
		if handleIndex>1 then
			local prevKey = gCurrGraphSettings.sortedKeys[handleIndex-1]
			gHandleDragInfo.prevHandleX, gHandleDragInfo.prevHandleY = CalcCoordsByKeyValuePair( prevKey, gCurrGraphSettings.base[prevKey] )
		end
		if handleIndex < # gCurrGraphSettings.sortedKeys  then
			local nextKey = gCurrGraphSettings.sortedKeys[handleIndex+1]
			gHandleDragInfo.nextHandleX, gHandleDragInfo.nextHandleY = CalcCoordsByKeyValuePair( nextKey, gCurrGraphSettings.base[nextKey] )
		end
		
		if maxVal~=baseVal then
			local _, tmpY = CalcCoordsByKeyValuePair( key, maxVal )
			gHandleDragInfo.varIndHeight = math.abs(tmpY - gHandleDragInfo.startY)
		end
	end

	-- Determine dragging boundary
	gHandleDragInfo.minY, gHandleDragInfo.maxY = gGraphY, gGraphY + gGraphH
	gHandleDragInfo.minX, gHandleDragInfo.maxX = gHandleDragInfo.startX, gHandleDragInfo.startX
	if gHandleDragInfo.prevHandleX~=nil and gHandleDragInfo.nextHandleX~=nil then
		gHandleDragInfo.minX = gHandleDragInfo.prevHandleX+gMinDiffX
		gHandleDragInfo.maxX = gHandleDragInfo.nextHandleX-gMinDiffX
	end

	-- Offset between cursor and the center of the drag handle
	gHandleDragInfo.mouseOffsetX = x - gHandleDragInfo.startX
	gHandleDragInfo.mouseOffsetY = y - gHandleDragInfo.startY

	-- UI update
	EnableKeyValueEditBoxes()

	-- Update handle position
	DraggingHandle( x, y )
end

function DraggingHandle( x, y )
	gHandleDragInfo.currX = math.min( math.max( x - gHandleDragInfo.mouseOffsetX, gHandleDragInfo.minX ), gHandleDragInfo.maxX )
	gHandleDragInfo.currY = math.min( math.max( y - gHandleDragInfo.mouseOffsetY, gHandleDragInfo.minY ), gHandleDragInfo.maxY )
	DragHandleTo( gHandleDragInfo.handleIndex, gHandleDragInfo.currX, gHandleDragInfo.currY, gHandleDragInfo.varIndHeight, gHandleDragInfo.baseY, false, gVarHandleSelected )
end

function EndDraggingHandle( cancel )
	local destX, destY, bRecalc = gHandleDragInfo.startX, gHandleDragInfo.startY, false
	if not cancel and (gHandleDragInfo.currX~=gHandleDragInfo.startX or gHandleDragInfo.currY~=gHandleDragInfo.startY) then
		destX, destY, bRecalc = gHandleDragInfo.currX, gHandleDragInfo.currY, true
	end
	DragHandleTo( gHandleDragInfo.handleIndex, destX, destY, gHandleDragInfo.varIndHeight, gHandleDragInfo.baseY, bRecalc, gVarHandleSelected )
	gHandleDragInfo = nil
end

function DragHandleTo( handleIndex, x, y, varIndHeight, baseY, bRecalc, bIsVarInd )
	local newKey, newVal
	if not bIsVarInd then
		newKey, newVal = CalcKeyValuePairByCoords( x, y )
	else
		newKey = gCurrGraphSettings.sortedKeys[handleIndex]
		local _, anyBound = CalcKeyValuePairByCoords( x, y )
		newVal = math.abs(anyBound-gCurrGraphSettings.base[newKey])
	end
	
	UpdateKeyValueEditBoxes( newKey, newVal )

	if bRecalc then
		UpdateHandle( handleIndex, newKey, newVal, bIsVarInd )
	else
		-- move handle to cursor position
		PlaceHandleAt( handleIndex, x, y, bIsVarInd )

		-- also move handle for variation indicator
		if varIndHeight~=nil then
			PlaceHandleAt( handleIndex, x, y-varIndHeight, true )
			PlaceLineSegAt( handleIndex, x, y-varIndHeight, x, y+varIndHeight, true )
		end
		
		if baseY~=nil then
			local curVarIndH = math.abs(y-baseY)
			PlaceLineSegAt( handleIndex, x, y, x, y+curVarIndH*2, true )
		end

		if gHandleDragInfo.prevHandleX~=nil then
			PlaceLineSegAt( handleIndex-1, gHandleDragInfo.prevHandleX, gHandleDragInfo.prevHandleY, x, y, false )
		end

		if gHandleDragInfo.nextHandleX~=nil then
			PlaceLineSegAt( handleIndex, x, y, gHandleDragInfo.nextHandleX, gHandleDragInfo.nextHandleY, false )
		end
	end
end

-- Graph Editor Utils

function UpdateKeyValueEditBoxes( newKey, newVal )
	setEditBoxText( "G_txtTimeEdit", newKey )
	setEditBoxText( "G_txtValueEdit", string.format(gCurrGraphSettings.fmtY, newVal) )
end

function EnableKeyValueEditBoxes()
	enableControl( "G_txtTimeEdit", true )
	enableControl( "G_txtValueEdit", true )
end

function DisableKeyValueEditBoxes()
	enableControl( "G_txtTimeEdit", false )
	enableControl( "G_txtValueEdit", false )
	setEditBoxText( "G_txtTimeEdit", "" )
	setEditBoxText( "G_txtValueEdit", "" )
end
