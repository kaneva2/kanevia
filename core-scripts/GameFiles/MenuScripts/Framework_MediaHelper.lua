--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\Lib_VecMath.lua")

--------------------------------------
-- MEDIA FUNCTIONALITY
-- This class works in conjunction with the FRAMEWORK_RETURN_MEDIA_OBJECTS event to maintain a list of media player objects in the current zone
-- Some expediency functions are provided to cater for commonly used functionality.
--------------------------------------


Media = {}


-- Add a convenience function to the flag, get some abstraction in there..
function Media.isMediaInRangeOf(self, other)
	local radiusSum = (self.radius + other.radius)
	local diff = Math.vecSub(self.position, other.position)
	local xzDistanceSqrd = Math.vecLenSq(diff)
	return (radiusSum * radiusSum >= xzDistanceSqrd)
end



MediaSystem = {}	-- Pseudo-class system for retrieving flag info from a central point (implemented so as to keep related game logic in one place...)


--------------------------------------
-- MEDIA FUNCTIONALITY
--------------------------------------

function MediaSystem.create(self)

	self.m_mediaList = {}
	
end

-- Resets the flag information in the world
function MediaSystem.resetMedia(self, mediaList)
	local newMediaList = {}
	newMediaList.mediaMap = {}
	local MediaTemplate = _G.Media -- Assign to local variable to reduce performance hit for referring to a global variable.
	
	if mediaList then
		for i, media in pairs(mediaList) do
			table.insert(newMediaList, media)
			-- Map the PID of this media object to the object itself so as to be able to efficiently check whether or not a PID belongs to a media object.
			newMediaList.mediaMap[media.PID] = media
			
			-- Make sure media functionality is accessible to this media object
			setmetatable(media, {__index = MediaTemplate})
		end
	end
	
	self.m_mediaList = newMediaList
end

function MediaSystem.addMedia(self, media, PID)
	if self.m_mediaList.mediaMap[PID] then
		
		for index, mediaObject in pairs(self.m_mediaList) do 
			if mediaObject.PID == PID then
				self.m_mediaList[index] = media
				self.m_mediaList.mediaMap[PID] = media
				setmetatable(media, {__index = _G.Media})
				return
			end
		end
	else
		table.insert(self.m_mediaList, media)
		self.m_mediaList.mediaMap[PID] = media

		-- Make sure media functionality is accessible to this media object
		setmetatable(media, {__index = _G.Media})
	end
end

function MediaSystem.removeMedia(self, PID)
	for index, media in pairs(self.m_mediaList) do
		if media.PID == tonumber(PID) then
			table.remove(self.m_mediaList, index)
			break
		end
	end

	self.m_mediaList.mediaMap[tonumber(PID)] = nil
end

function MediaSystem.getMediaWithPID(self, objectPID)
	return self.m_mediaList.mediaMap[objectPID]
end

function MediaSystem.getMediaList(self)
	return self.m_mediaList
end

function MediaSystem.getMediaInRangeOfMedia(self, mediaObject, checkFilter)
	for j, media in ipairs(self.m_mediaList) do
		if( mediaObject ~= media ) then
			local passesFilter = not checkFilter or checkFilter(media)
			if passesFilter then
				local isMediaTooClose = media:isMediaInRangeOf(mediaObject)
				if isMediaTooClose then
					return media
				end
			end
		end
	end
	
	return nil
end