--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("PageButtons.lua")
dofile("MenuLocation.lua")

WEB_ADDRESS = GameGlobals.WEB_SITE_PREFIX.."kgp/friendsList.aspx?game=true"
SEND_GIFT_EVENT_NAME = "SendDailyGiftEvent"
GIFT_OPEN_MENU_EVENT = "GiftOpenGiftMenu"

-- Hold current friend data returned from server
g_message = ""

g_friends_count   = 0
g_t_friends       = {}
g_curPage         = 1
g_friendsPerPage  = 200
g_friends_count   = 0
g_selected_friend = -1
g_totalNumRec     = 0

g_gift_id         = 0
recipient         = {}
numFriends        = 0

function setGiftIcon( icon_name, id )
	local ich = Dialog_GetImage(gDialogHandle, icon_name)
	if ich ~= 0 then
		Control_SetVisible(ich, true)
		local eh = Image_GetDisplayElement(ich)
		if eh ~= 0 then
			KEP_LoadIconTextureByID(id, eh, gDialogHandle)
		end
	end
end

function DisplayAvailableFriends()
	fl = Dialog_GetListBox(gDialogHandle, "lstAvailableFriends")
	if fl == nil then return end
    if g_t_friends == nil then return end
    
    for i = 0, g_friends_count - 1 do
        skip = 0
        friend_id = tonumber(g_t_friends[i + 1]["userID"])
        for i=1, numFriends do
            if recipient[i] == friend_id then
                skip = 1
                break
            end
        end
        if skip == 0 then
            ListBox_AddItem(fl, g_t_friends[i + 1]["name"], friend_id )
        end
    end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	ch = Dialog_GetStatic(gDialogHandle, "gift_id")
	g_gift_id = tonumber(Static_GetText(ch))
	setGiftIcon("imgGift", g_gift_id)
	g_message = KEP_EventDecodeString( event )  --contains the XML returned
	ParseFriendData(gDialogHandle)
	DisplayAvailableFriends()
	return KEP.EPR_OK
end

function ParseFriendData(dialogHandle)
	local s = 0     			-- start of substring
	local numRec = 0   		-- number of records in this chunk
	local strPos = 0    		-- current position in string to parse
	local result = ""  		-- return string from find temp
	local name       = "" 	-- username
	local playerID   = 0    -- numerical id of player
	local glueDate   = "" -- date user became friend
	local glueTime   = "" -- time user became friend
	local online     = ""   -- online status of user
	local parseStr   = ""  -- all parsed data (probably ditch later when using list box)
	local lastOnline = "" -- last time user was online
	local parseStr = ""
	local sh = Dialog_GetStatic( gDialogHandle, "lblQuery" )
	s, e, result = string.find(g_message, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		numRec = calculateRanges()
		g_t_friends = {}
		g_friends_count = 0

		for i=1, numRec do
			local friendstr = ""
			local trash = 0
			local s = 0
			s, strPos, friendstr = string.find(g_message, "<Friend>(.-)</Friend>", strPos)
			s, trash, online = string.find(friendstr, "<online>(%w+)</online>", 1)
			s, trash, name = string.find(friendstr, "<username>(.-)</username>", 1)
			s, trash, userID = string.find(friendstr, "<user_id>([0-9]+)</user_id>", 1)
			s, trash, playerID = string.find(friendstr, "<player_id>([0-9]+)</player_id>", 1)
			s, trash, glueDate, glueTime = string.find(friendstr, "<glued_date>(%w+%-%w+%-%w+)T(%w+:%w+:%w+)", 1)
			s, trash, imageUrl = string.find(friendstr, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>", 1 )

			local pal = {}
			pal["name"] = name
			pal["playerID"] = playerID
			pal["userID"] = userID
			pal["thumbnailFile"] = nil
			table.insert(g_t_friends, pal)
			g_friends_count = g_friends_count + 1
		end
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
    KEP_EventRegisterHandler( "RecieveGiftingInfoHandler", GIFT_OPEN_MENU_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( SEND_GIFT_EVENT_NAME, KEP.MED_PRIO )
	KEP_EventRegister( GIFT_OPEN_MENU_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
    Helper_Dialog_OnCreate( dialogHandle )

	makeWebCall( WEB_ADDRESS, WF.FRIENDS, g_curPage, g_friendsPerPage)

	-- setup look of listbox scrollbars
	local lbh = Dialog_GetListBox(dialogHandle, "lstAvailableFriends")
	if lbh ~= 0 then
        local sbh = ListBox_GetScrollBar(lbh)
		if sbh ~= 0 then
            local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
                Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end
			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
                Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end
			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
                Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end
			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
                Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end

	-- setup look of listbox scrollbars
	local lbh = Dialog_GetListBox(dialogHandle, "lstSelectedFriends")
	if lbh ~= 0 then
        local sbh = ListBox_GetScrollBar(lbh)
		if sbh ~= 0 then
            local eh = ScrollBar_GetTrackDisplayElement(sbh)
			if eh ~= 0 then
                Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 37, 17, 69)
			end
			eh = ScrollBar_GetButtonDisplayElement(sbh)
			if eh ~= 0 then
                Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 19, 17, 35)
			end
			eh = ScrollBar_GetUpArrowDisplayElement(sbh)
			if eh ~= 0 then
                Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 1, 17, 17)
			end
			eh = ScrollBar_GetDownArrowDisplayElement(sbh)
			if eh ~= 0 then
                Element_AddTexture(eh, dialogHandle, "scroll_bar.tga")
				Element_SetCoords(eh, 1, 71, 17, 87)
			end
		end
	end
end

function Dialog_OnDestroy(dialogHandle)
    Helper_Dialog_OnDestroy( dialogHandle )
end

function calculateRanges()
    local numRec = 0  --number of records returned
	local s, e  -- start and end indices of found string
	local results_h = Dialog_GetStatic( gDialogHandle, "lblResults")
	local text = "Showing "
	local high = g_curPage * g_friendsPerPage
	local low = ((g_curPage - 1) * g_friendsPerPage) + 1

	s, e, g_totalNumRec = string.find(g_message, "<TotalNumberRecords>([0-9]+)</TotalNumberRecords>")
	s, e, numRec = string.find(g_message, "<NumberRecords>([0-9]+)</NumberRecords>")

	g_totalNumRec = tonumber(g_totalNumRec)

	g_pageCap = math.ceil(g_totalNumRec / g_friendsPerPage)
	g_setCap = math.ceil(g_pageCap / g_window)

	if high > g_totalNumRec then
        high = g_totalNumRec
	end

	if numRec == "0" then
        low = numRec
	end

	if totalRec_h ~= nil then
        Static_SetText(totalRec_h, g_totalNumRec)
	end

	if results_h ~= nil then
        text = text..low.." - "..high.." of "..g_totalNumRec
		Static_SetText(results_h, text)
	end

	if g_InitBtnText then
        setButtonText()
		g_InitBtnText = false
	end

	return numRec
end

function lstAvailableFriends_OnListBoxItemDblClick( listboxHandle )
    fl = Dialog_GetListBox(gDialogHandle, "lstSelectedFriends")
	if fl ~= nil then
        ListBox_InsertItem(fl, 0, ListBox_GetSelectedItemText(listboxHandle), ListBox_GetSelectedItemData(listboxHandle))
		ListBox_RemoveItem(listboxHandle, ListBox_GetSelectedItemIndex(listboxHandle))
	end
end

function lstSelectedFriends_OnListBoxItemDblClick( listboxHandle )
    fl = Dialog_GetListBox(gDialogHandle, "lstAvailableFriends")
	if fl ~= nil then
        ListBox_InsertItem(fl, 0, ListBox_GetSelectedItemText(listboxHandle), ListBox_GetSelectedItemData(listboxHandle))
		ListBox_RemoveItem(listboxHandle, ListBox_GetSelectedItemIndex(listboxHandle))
	end
end

function btnProceed_OnButtonClicked( buttonHandle )
    --anyone selected?
	lbh = Dialog_GetListBox(gDialogHandle, "lstSelectedFriends")
	numFriends = ListBox_GetSize( lbh )

	if numFriends > 0 then
        --lets do it!
		ev = KEP_EventCreate( SEND_GIFT_EVENT_NAME )
		KEP_EventAddToServer( ev )
		KEP_EventEncodeNumber( ev, numFriends )
		KEP_EventEncodeNumber( ev, g_gift_id )
		for i=1, numFriends do
            KEP_EventEncodeNumber(ev, ListBox_GetItemData(lbh, i-1) )
		end
		KEP_EventQueue( ev)
	else
        Dialog_MsgBox("Please select friends by double clicking.")
	end

	MenuCloseThis()
end

function RecieveGiftingInfoHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
    if filter == 3 then
		maxFriends = KEP_EventDecodeNumber(event)
		numFriends = KEP_EventDecodeNumber(event)
		for i=1,numFriends do
            recipient[i] = KEP_EventDecodeNumber( event )
		end
		setRadioEnabled("btnProceed", true)
	end
end
