--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("UGCFunctions.lua")
dofile("..\\Scripts\\UGCImportState.lua")
dofile("..\\Scripts\\CharAnimImportStateIds.lua")

gActorTypes = actorTypes	-- see UGCGlobals.lua

gDropId = -1
gImportInfo = nil
gTargetInfo = nil
gBtnState = 0
gKeyState = 0
gAnimGLID = 0
gAnimDuration = 0
gSpeedFactor = 1
gCropStart = 0
gCropEnd = -1
gPlayerGender = "M"
gChangeStuntAnimSettingCounter = 0
gActorGroup = 0

function UGCImportMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	gDropId = UGCI.GetDialogInstanceId( gDialogHandle )
	local dlgId = Dialog_GetId( gDialogHandle )
	if filter~=dlgId or objectid~=gDropId then
		-- Ignore event with mismatched IDs
		return
	end

	local eventData = UGCI.ParseUGCImportMenuEvent( event )
	gImportInfo = eventData.importInfo
	gTargetInfo = eventData.targetInfo
	gBtnState = eventData.btnState
	gKeyState = eventData.keyState

	Log( "UGCImportMenuEventHandler: dlgId=" .. dlgId .. " dropId=" .. gDropId .. " type=" .. gImportInfo.type .. " path=" .. gImportInfo.fullPath )

	if gImportInfo.type == UGC_TYPE.CHARANIM and gTargetInfo.id == 0 then
		local player = KEP_GetPlayer( )
		gTargetInfo.id  = GetSet_GetNumber( player, MovementIds.NETWORKDEFINEDID)
	end

	if gImportInfo.type==UGC_TYPE.CHARANIM then

		if gImportInfo.sessionId~=-1 then

			-- previewing animation
			local impSession = UGC_GetImportSession( gImportInfo.sessionId )
			if impSession~=nil then
				gAnimGLID = GetSet_GetNumber( impSession, CharAnimImportStateIds.LOCALGLID )
				gAnimDuration = GetSet_GetNumber( impSession, CharAnimImportStateIds.ANIMDURATION )
				setStaticText("lblDuration", string.format("%0.2f (sec)", gAnimDuration/1000))
				gCropStart = GetSet_GetNumber( impSession, CharAnimImportStateIds.CROPSTART )
				gCropEnd = GetSet_GetNumber( impSession, CharAnimImportStateIds.CROPEND )
				if gCropEnd==-1 then
					gCropEnd = gAnimDuration
				end
				setEditBoxText("edCropStart", tostring(gCropStart), false)
				setEditBoxText("edCropEnd", tostring(gCropEnd), false)
				OnCroppingEditBoxChange( nil )

				gSpeedFactor = GetSet_GetNumber( impSession, CharAnimImportStateIds.SPEEDSCALE )
				SetSpeedFactor( gSpeedFactor )

				gActorGroup = GetSet_GetNumber( impSession, CharAnimImportStateIds.TARGETACTORTYPE )
				SelectActorType( gActorGroup )

				if gTargetInfo.type == ObjectType.DYNAMIC or gTargetInfo.type == ObjectType.EQUIPPABLE then
					-- Hide loop checkbox if ADO animation according to JP
					-- NOTE: looping is set to true by default in UGCCharAnimImport.cpp
					hideControl( "chkLoop" )
				end

				local looping = GetSet_GetNumber( impSession, CharAnimImportStateIds.LOOPING )
				local chkLoop = Dialog_GetCheckBox( gDialogHandle, "chkLoop" )
				CheckBox_SetChecked( chkLoop, looping )
				chkLoop_OnCheckBoxChanged( chkLoop, looping )

				local chkOffset = Dialog_GetCheckBox( gDialogHandle, "chkOffset" )
				CheckBox_SetChecked( chkOffset, 0 )

				local defaultOfs = {}
				local targetOfs = {}
				defaultOfs.x, defaultOfs.y, defaultOfs.z = GetSet_GetVector( impSession, CharAnimImportStateIds.DEFAULTOFFSET )
				targetOfs.x, targetOfs.y, targetOfs.z = GetSet_GetVector( impSession, CharAnimImportStateIds.TARGETOFFSET )
				if defaultOfs.x~=0 or defaultOfs.y~=0 or defaultOfs.z~=0 then
					enableControl( "chkOffset", 1 )
					if math.abs(defaultOfs.x-targetOfs.x)<1e-6 and math.abs(defaultOfs.y-targetOfs.y)<1e-6 and math.abs(defaultOfs.z-targetOfs.z)<1e-6 then
						CheckBox_SetChecked( chkOffset, 1 )
					end
				else
					enableControl( "chkOffset", 0 )
				end
				ChangeAnimation( gAnimGLID )
				return
			end
		end
	else
		LogError( "UGCImportMenuEventHandler: don't know how to handle import type " .. gImportInfo.type )
	end

	-- Cancel Import And Close Menu
	MenuCloseThis()
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "UGCImportMenuEventHandler", UGC_IMPORT_MENU_EVENT, KEP.HIGH_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )

	-- get player gender
	gPlayerGender = KEP_GetLoginGender()

	-- populate gender list
	addComboBoxItems( "cbActorType", gActorTypes )
	
	MenuClose("ImportAccessoryAnimation.xml")
end

function Dialog_OnDestroy(dialogHandle)
	
	-- DRF - Added
	if (gImportInfo and gImportInfo.sessionId) then
		UGC_CancelImportAsync(gImportInfo.sessionId )
	end
	
	Helper_Dialog_OnDestroy( dialogHandle )
end

function ChangeAnimation( animGLID )
	if gTargetInfo.type == ObjectType.DYNAMIC then
		KEP_SetAnimationOnDynamicObject(gTargetInfo.id, animGLID)
		hideControl("cbActorType")
		hideControl("Static1")
	elseif gTargetInfo.type == ObjectType.EQUIPPABLE then
		local player = KEP_GetPlayer( )
		if player~=nil then
			KEP_PlayerObjectSetEquippableAnimation(player, gTargetInfo.id, animGLID )
		end
	else
		KEP_SetCurrentAnimationByGLID(animGLID)
	end
end

function btnNext_OnButtonClicked( buttonHandle )
	if gTargetInfo.type == ObjectType.EQUIPPABLE then
		-- set actor type to equippable
		local impSession = UGC_GetImportSession( gImportInfo.sessionId )
		GetSet_SetNumber( impSession, CharAnimImportStateIds.TARGETACTORTYPE, UGC_EQUIPPABLE_GROUP )
		gActorGroup = UGC_EQUIPPABLE_GROUP
	end
	UGC_ContinueImportAsync( gImportInfo.sessionId )
	
	-- Prevent Cancel On Close
	gImportInfo = nil
	
	MenuCloseThis()
end

function cbActorType_OnComboBoxSelectionChanged(comboBoxHandle, sel_index, sel_text)
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession~=nil then
		local text, data = getComboBoxSelectedItem( "cbActorType" )
		if gActorTypes[data].actorGroup~=gActorGroup then
			-- set actor type
			GetSet_SetNumber( impSession, CharAnimImportStateIds.TARGETACTORTYPE, gActorTypes[data].actorGroup )

			-- mount STUNT entity if necessary
			local stuntMounted = GetSet_GetNumber( impSession, CharAnimImportStateIds.AIMOUNTED )
			if stuntMounted==nil then
				stuntMounted = 0
			end
			if gActorTypes[data].gender~=gPlayerGender then
				if stuntMounted==0 then
					KEP_Mount(gActorTypes[data].stuntId, MT_STUNT, 1 )
					stuntMounted = 1
					gChangeStuntAnimSettingCounter = 5	-- Hack: wait to make sure new animation is loaded into CSkeletonObject before setting animation attributes
				end
			elseif stuntMounted~=0 then
				KEP_Unmount( )
				stuntMounted = 0
			end
			GetSet_SetNumber( impSession, CharAnimImportStateIds.AIMOUNTED, stuntMounted )

			-- and reconvert
			UGC_ClearErrors( gImportInfo.sessionId )
			UGC_ReconvertAsync(gImportInfo.sessionId )

			-- Prevent Cancel On Close
			gImportInfo = nil
		
			MenuCloseThis()
		end
	end
end

function slSpeed_OnSliderValueChanged( slider )
	gSpeedFactor = GetSpeedFactor()
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession then
		GetSet_SetNumber( impSession, CharAnimImportStateIds.SPEEDSCALE, gSpeedFactor )
		KEP_ChangeAnimationSpeed(gTargetInfo.type, gTargetInfo.id, gAnimGLID, gSpeedFactor );
		local effDuration = (gCropEnd - gCropStart) / gSpeedFactor
		setStaticText("lblDuration", string.format("%0.2f (sec)", effDuration/1000))
	end

	-- restart animation
	ChangeAnimation(gAnimGLID)
end

function OnCroppingEditBoxChange( editBox )
	local cropStart = 0
	local cropEnd = gAnimDuration
	local edStart = Dialog_GetEditBox( gDialogHandle, "edCropStart" )
	local edEnd = Dialog_GetEditBox( gDialogHandle, "edCropEnd" )
	if edStart~=nil and edEnd~=nil then
		local strCropStart = EditBox_GetText( edStart )
		local strCropEnd = EditBox_GetText( edEnd )
		if strCropStart~=nil and strCropEnd~=nil then
			cropStart = tonumber( strCropStart )
			cropEnd = tonumber( strCropEnd )
			if cropStart==nil or cropStart<0 then
				cropStart = 0
			end
			if cropStart > gAnimDuration then
				cropStart = gAnimDuration
			end
			if cropEnd==nil or cropEnd<0 or cropEnd>gAnimDuration then
				cropEnd = gAnimDuration
			end
			if cropEnd < cropStart then
				cropEnd = cropStart
			end
		end
	end
	gCropStart = cropStart
	gCropEnd = cropEnd

	-- call UGC manager to pass over the parameter
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession then
		GetSet_SetNumber( impSession, CharAnimImportStateIds.CROPSTART, gCropStart )
		GetSet_SetNumber( impSession, CharAnimImportStateIds.CROPEND, gCropEnd )
		KEP_ChangeAnimationCropping(gTargetInfo.type, gTargetInfo.id, gAnimGLID, gCropStart, gCropEnd );
		local effDuration = (gCropEnd - gCropStart) / gSpeedFactor
		setStaticText("lblDuration", string.format("%0.2f (sec)", effDuration/1000))
	end

	-- restart animation
	ChangeAnimation(gAnimGLID)
end

edCropStart_OnEditBoxChange = OnCroppingEditBoxChange
edCropEnd_OnEditBoxChange = OnCroppingEditBoxChange

function chkLoop_OnCheckBoxChanged( checkBox, checked )
	-- call UGC manager to pass over the parameter
	local looping = checked
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	if impSession then
		GetSet_SetNumber( impSession, CharAnimImportStateIds.LOOPING, looping )
		KEP_ChangeAnimationLooping(gTargetInfo.type, gTargetInfo.id, gAnimGLID, looping)
	end

	-- restart animation
	ChangeAnimation(gAnimGLID)
end

function chkOffset_OnCheckBoxChanged( checkBox, checked )
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )

	-- set offset
	local x, y, z = 0, 0, 0
	if checked~=0 then
		x, y, z = GetSet_GetVector( impSession, CharAnimImportStateIds.DEFAULTOFFSET )
	end
	GetSet_SetVector( impSession, CharAnimImportStateIds.TARGETOFFSET, x, y, z)

	-- and reconvert
	UGC_ClearErrors( gImportInfo.sessionId )
	UGC_ReconvertAsync(gImportInfo.sessionId )

	-- Prevent Cancel On Close
	gImportInfo = nil

	MenuCloseThis()
end

function Dialog_OnRender(dialogHandle, elapsedTime)
	if gChangeStuntAnimSettingCounter > 0 then
		gChangeStuntAnimSettingCounter = gChangeStuntAnimSettingCounter - 1
		if gChangeStuntAnimSettingCounter==0 then
			KEP_ChangeAnimationSpeed(gTargetInfo.type, gTargetInfo.id, gAnimGLID, gSpeedFactor )
			KEP_ChangeAnimationCropping(gTargetInfo.type, gTargetInfo.id, gAnimGLID, gCropStart, gCropEnd )
		end
	end
end

function SetSpeedFactor( speedFactor )
	local slider = Dialog_GetSlider( gDialogHandle, "slSpeed" )
	local sliderValue = 50
	if speedFactor > 1 then
		sliderValue = (speedFactor - 1) * 50 + 50
	else
		sliderValue = 50 - (1 - speedFactor) * 100
	end
	Slider_SetValue( slider, sliderValue )
	slSpeed_OnSliderValueChanged( slider )
end

function GetSpeedFactor()
	local slider = Dialog_GetSlider( gDialogHandle, "slSpeed" )
	local sliderValue = Slider_GetValue( slider )
	local speedFactor = 1
	if sliderValue > 50 then
		speedFactor = (sliderValue - 50) / 50 + 1
	else
		speedFactor = 1 - ((50 - sliderValue) / 100)
	end
	return speedFactor
end

function SelectActorType( actorGroup )
	for idx, actorType in ipairs(gActorTypes) do
		if actorType.actorGroup == actorGroup then
			selectComboBoxItemByData( "cbActorType", actorType.data )
			break
		end
	end
end
