--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Functions for obtaining and parsing credit
-- balances (regular and gift) from the WOK
-- web query

--This event come from menus that impact credits.
--Sends a new credit total to the HUD
UPDATE_HUD_EVENT = "UpdateHUDBalanceEvent"

--Send credit total to HUD
--credits:  number of credits
--getNewBalance: number representing bool for whether or not to call credit
--				 balance query again
function updateHUDBalance( credits, giftCredits, getNewBalance )
    local ev = KEP_EventCreate( UPDATE_HUD_EVENT )
    KEP_EventEncodeNumber( ev, credits )
    KEP_EventEncodeNumber( ev, giftCredits )
    KEP_EventEncodeNumber( ev, getNewBalance )
    KEP_EventQueue( ev )
end

function getUserID( playerName, menu_filter )
	if playerName ~= nil and playerName ~= "" then
		makeWebCall( GameGlobals.WEB_SITE_PREFIX..USERID_SUFFIX..playerName, menu_filter)
	end
end

function getBalances(userID, menu_filter)
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..BALANCE_SUFFIX..userID, menu_filter)
end

function getBalancesSinceLastLogin(userID, menu_filter)
	makeWebCall( GameGlobals.WEB_SITE_PREFIX..BALANCE_SINCE_LAST_LOGIN_SUFFIX..userID, menu_filter)
end

function getAvailableCreditRedemptions(menu_filter)
	makeWebCall (WEBCALL_GET_AVAILABLE_CREDIT_REDEMPTIONS, menu_filter)
end

function ParseBalances( browserString )
	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local regCredits = 0	-- regular credits
	local giftCredits = 0	-- gift credits
	s, strPos, result = string.find(browserString, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		s, strPos, regCredits = string.find(browserString, "<Balance>(.-)</Balance>", 1)
		if regCredits ~= nil then
			regCredits = tonumber(regCredits)
		end
		s, strPos, giftCredits = string.find(browserString, "<GiftBalance>(.-)</GiftBalance>", 1)
		if giftCredits ~= nil then
			giftCredits = tonumber(giftCredits)
		end
		return regCredits, giftCredits
	end
end

function ParseBalancesSinceLastLogin( browserString )
	local s = 0 		-- start and end index of substring
	local strPos = 0    	-- current position in string to parse
	local result = ""  		-- return string from find temp
	local lastLoginRewards = 0	-- rewards since last login
	local lastLoginCredits = 0	-- credits since last login
	local lastLogon = ""	-- credits since last login
	s, strPos, result = string.find(browserString, "<ReturnCode>([0-9]+)</ReturnCode>")
	if result == "0" then
		s, strPos, lastLoginRewards = string.find(browserString, "<RewardsSinceLastLogin>(.-)</RewardsSinceLastLogin>", 1)
		if lastLoginRewards ~= nil then
			lastLoginRewards = tonumber(lastLoginRewards)
		end
		s, strPos, lastLoginCredits = string.find(browserString, "<CreditsSinceLastLogin>(.-)</CreditsSinceLastLogin>", 1)
		if lastLoginCredits ~= nil then
			lastLoginCredits = tonumber(lastLoginCredits)
		end
		s, strPos, lastLogon = string.find(browserString, "<LastLogon>(.-)</LastLogon>", 1)
		if lastLogon ~= nil then
			lastLogon = lastLogon
		end
		return lastLoginRewards, lastLoginCredits, lastLogon
	end
end

function ParseAvailableCreditRedemptions( browserString )
	local result = string.match(browserString, "<ReturnCode>(.-)</ReturnCode>")
	local availableRedemptions = 0

	if result == "0" then
		availableRedemptions = tonumber(string.match(browserString, "<AvailableCreditRedemptions>(.-)</AvailableCreditRedemptions>") or "0")
	end

	return availableRedemptions
end