--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("UGCImportProgCommon.lua")
dofile("..\\Scripts\\CharAnimImportStateIds.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")

gModuleName = "AnimImport"	-- used in UGCImportProgCommon.lua
gUgcTypeDesc = "Animation"		-- used in UGCImportProgCommon.lua
gImportStateGetSetIds = CharAnimImportStateIds
ADD_ANIM_IMPORT_EVENT = "AddAnimImportEvent"

function UGCI.RegisterEventHandlers(dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "UGCImportConfirmationHandler", UGC_IMPORT_CONFIRMATION_RESULT_EVENT, KEP.HIGH_PRIO )
end

function UGCI.RegisterEvents(dispatcher, handler, debugLevel )
	KEP_EventRegister( UGC_IMPORT_CONFIRMATION_RESULT_EVENT, KEP.MED_PRIO )
end

function UGCI.OnCreateDialog()
-- do nothing
end

function UGCI.OnDestroyDialog()
	-- unmount STUNT entity
	local impSession = UGC_GetImportSession( gImportInfo.sessionId )
	local stuntMounted = GetSet_GetNumber( impSession, gImportStateGetSetIds.AIMOUNTED )
	if stuntMounted~=nil and stuntMounted~=0 then
		KEP_Unmount( )
		GetSet_SetNumber( impSession, gImportStateGetSetIds.AIMOUNTED, 0 )
	end

	-- return players to standing pose
	-- If we want to do something for DO then this is the place to do it.
	local targetType = GetSet_GetNumber( impSession, gImportStateGetSetIds.TARGETTYPE )
	if targetType ~= ObjectType.UNKNOWN then
		if targetType == ObjectType.DYNAMIC  then
		-- ADO animation
		-- Do nothing
		else
			-- Character animation
			KEP_SetCurrentAnimation(1)
		end
	end
end

function UGCI.OnPreviewing( impSession, impSessionId )
	changeDialogState( STATE_ANALYZING )
	Dialog_SetMinimized( gDialogHandle, 0 )
end

function UGCI.OnPreviewed( impSession, impSessionId )
	-- set animation GLID to use
	GetSet_SetNumber( impSession, gImportStateGetSetIds.LOCALGLID, -1 )
	-- tell state machine to continue import process
	UGC_ContinueImportAsync( impSessionId )
end

function UGCI.OnImporting( impSession, impSessionId )
	changeDialogState( STATE_IMPORTING )
	Dialog_SetMinimized( gDialogHandle, 0 )
end

function UGCI.OnImported( impSession, impSessionId )
	-- tell state machine to continue import process
	UGC_ContinueImportAsync( impSessionId )
end

function UGCI.OnConverting( impSession, impSessionId )
	changeDialogState( STATE_IMPORTING )
	Dialog_SetMinimized( gDialogHandle, 0 )
end

function UGCI.OnConverted( impSession, impSessionId )
	-- bring up import options dialog
	Dialog_SetMinimized( gDialogHandle, 1 )
	UGCI.CallUGCHandler("AnimImportOptions.xml", 0, gDropId, gImportInfo, gTargetInfo, gBtnState, gKeyState )
end

function UGCI.OnProcessing( impSession, impSessionId )
	changeDialogState( STATE_IMPORTING )
	Dialog_SetMinimized( gDialogHandle, 0 )
end

function UGCI.OnProcessed( impSession, impSessionId )

	local internalError = true

	if impSession~=nil then

		internalError = false

		local localGLID = GetSet_GetNumber( impSession, gImportStateGetSetIds.LOCALGLID )
		local rootURI = GetSet_GetString( impSession, gImportStateGetSetIds.ROOTURI )
		local targetActorType = GetSet_GetNumber( impSession, gImportStateGetSetIds.TARGETACTORTYPE )
		local targetType = GetSet_GetNumber( impSession, gImportStateGetSetIds.TARGETTYPE )
		local targetId = GetSet_GetNumber( impSession, gImportStateGetSetIds.TARGETID )

		if targetType ~= ObjectType.UNKNOWN then
			if targetType == ObjectType.DYNAMIC or targetType == ObjectType.EQUIPPABLE then
				KEP_BakeAnimation( targetType, targetId, localGLID )
			else
				-- Character animation
				KEP_BakeCharacterAnimation( localGLID )
			end
		end

		-- Reset settings on player avatar (also pass thru to stunt actor if used)
		KEP_ChangeAnimationSpeed( targetType, targetId, localGLID, 1 )
		KEP_ChangeAnimationCropping( targetType, targetId, localGLID, 0, -1 )

		local s, e, animName
		s, e, animName = string.find( rootURI, ".*\\(.+)" )
		if animName==nil then
			animName = rootURI
		end

		UpdateAnimImportsMenu( localGLID, animName, targetActorType, targetType, targetId )

		-- Close menu (remove temporary actor and restore actor pose)
		-- Must be called before UGC_CleanupImportSessionAsync but after BakeCharacterAnimation
		MenuCloseThis()

		-- Cleanup import session
		UGC_CleanupImportSessionAsync( impSessionId )
	end

	if internalError then
		LogError("Import Failed - Internal Error")
		KEP_MessageBox( "Import failed due to an internal error.\n\n[code:5]", "Import failed" )
	end
end

function UGCI.OnPreviewFailed( impSession, impSessionId )
	local msg = nil
	if GetSet_GetNumber( impSession, gImportStateGetSetIds.SKELETONMISMATCHED )~=0 then
		-- skeleton mismatched
		msg = "Incompatible skeleton."
	elseif GetSet_GetString( impSession, gImportStateGetSetIds.ANIMURI )=="" then
		-- No animation
		msg = "No animation found."
	end
	return msg
end

function UGCI.OnImportFailed( impSession, impSessionId )
-- do nothing
end

function UGCI.OnProcessingFailed( impSession, impSessionId )
-- do nothing
end

function UGCI.OnCancelled( impSession, impSessionId )
-- do nothing
end

function UpdateAnimImportsMenu( animGLID, animName, animActorType, targetType, targetId )

	MenuOpen("AnimImports.xml")

	local event = KEP_EventCreate( ADD_ANIM_IMPORT_EVENT )
	KEP_EventEncodeNumber( event, animGLID )
	KEP_EventEncodeString( event, animName )
	KEP_EventEncodeNumber( event, animActorType )
	KEP_EventEncodeNumber( event, targetId )
	KEP_EventQueue( event )
end
