--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

---------------------------------------------------------------------
-- DRF - Unified Progress Functions (moved from CommonFunctions)
---------------------------------------------------------------------

g_progressDebug = false -- set true for ProgressMenu::ProgressUpdate debug

-- progress event filter enumeration
PROGRESS_NO = 0
PROGRESS_OPEN = 1
PROGRESS_OPENED = 2
PROGRESS_CLOSE = 3
PROGRESS_CLOSED = 4
PROGRESS_UPDATE = 5
PROGRESS_ERROR = 6
PROGRESS_CANCEL = 7
PROGRESS_TAG = 8
PROGRESS_OPT_CANCEL = 9
PROGRESS_OPT_FADE = 10
PROGRESS_OPT_MSG = 11
PROGRESS_LAST_GOOD_URL_SET = 12
PROGRESS_LAST_GOOD_URL_GET = 13

-- progress option cancel enumeration
PROGRESS_OPT_CANCEL_NO = 0 -- progress cannot be cancelled by user, can be changed
PROGRESS_OPT_CANCEL_YES = 1 -- progress can be cancelled by user, can be changed
PROGRESS_OPT_CANCEL_NEVER = 2 -- progress cannot be cancelled by user, cannot be changed
PROGRESS_OPT_CANCEL_ALWAYS = 3 -- progress can be cancelled by user, cannot be changed
PROGRESS_OPT_CANCEL_DEFAULT = PROGRESS_OPT_CANCEL_NEVER -- default cancel option

-- progress option fade enumeration
PROGRESS_OPT_FADE_NO = 0 -- do not fade out background
PROGRESS_OPT_FADE_MIN = 1 -- fade to min slowly
PROGRESS_OPT_FADE_MAX = 2 -- fade to max slowly
PROGRESS_OPT_FADE_MIN_NOW = 3 -- fade to min immediately
PROGRESS_OPT_FADE_MAX_NOW = 4 -- fade to max immediately
PROGRESS_OPT_FADE_DEFAULT = PROGRESS_OPT_FADE_MAX -- default fade option

-- progress option message source enumeration
PROGRESS_OPT_MSG_NO = 0 -- do not display messages, can be changed
PROGRESS_OPT_MSG_RANDOM = 1 -- display periodic random messages, can be changed
PROGRESS_OPT_MSG_UPDATE = 2 --  display update messages only, can be changed
PROGRESS_OPT_MSG_DEBUG = 3 -- display update messages only, cannot be changed
PROGRESS_OPT_MSG_DEFAULT = PROGRESS_OPT_MSG_RANDOM -- default message option

-- Progress Functions Loggers
function pfLog(txt) Log("ProgressFunctions::"..txt) end
function pfLogWarn(txt) LogWarn("ProgressFunctions::"..txt) end
function pfLogError(txt) LogError("ProgressFunctions::"..txt) end

function ProgressOpen(id)
    --pfLog("ProgressOpen: '"..id.."'")
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_OPEN)
	KEP_EventEncodeString( ev, id )
	KEP_EventQueue( ev )
end

function ProgressOpened()
	--pfLog("ProgressOpened")
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_OPENED)
	KEP_EventQueue( ev )
end

function ProgressClose(id)
    --pfLog("ProgressClose: '"..id.."'")
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_CLOSE)
	KEP_EventEncodeString( ev, id )
	KEP_EventQueue( ev )
end

function ProgressClosed()
	--pfLog("ProgressClosed")
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_CLOSED)
	KEP_EventQueue( ev )
end

function ProgressUpdate(id, msg)
    --pfLog("ProgressUpdate: '"..id.."' msg='"..msg.."'")
    if (not g_progressDebug) then return end
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_UPDATE)
	KEP_EventEncodeString( ev, id )
	KEP_EventEncodeString( ev, msg )
	KEP_EventQueue( ev )
end

function ProgressError(id, msg)
	pfLog("ProgressError: '"..id.."' msg='"..msg.."'")
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_ERROR)
	KEP_EventEncodeString( ev, id )
	KEP_EventEncodeString( ev, msg )
	KEP_EventQueue( ev )
end

function ProgressCancel(id, msg)
	--pfLog("ProgressCancel: '"..id.."' msg='"..msg.."'")
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_CANCEL)
	KEP_EventEncodeString( ev, id )
	KEP_EventEncodeString( ev, msg )
	KEP_EventQueue( ev )
end

function ProgressTag(id, tag)
	--pfLog("ProgressTag: '"..id.."' tag='"..tag.."'")
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_TAG)
	KEP_EventEncodeString( ev, id )
	KEP_EventEncodeString( ev, tag )
	KEP_EventQueue( ev )
end

function ProgressOptCancel(id, optCancel)
    --pfLog("ProgressOptCancel: '"..id.."' optCancel="..optCancel)
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_OPT_CANCEL)
	KEP_EventEncodeString( ev, id )
	KEP_EventEncodeNumber( ev, optCancel )
	KEP_EventQueue( ev )
end

function ProgressOptFade(id, optFade)
    --pfLog("ProgressOptFade: '"..id.."' optFade="..optFade)
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_OPT_FADE)
	KEP_EventEncodeString( ev, id )
	KEP_EventEncodeNumber( ev, optFade )
	KEP_EventQueue( ev )
end

function ProgressOptMsg(id, optMsg)
    --pfLog("ProgressOptMsg: '"..id.."' optMsg='"..optMsg.."'")
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_OPT_MSG)
	KEP_EventEncodeString( ev, id )
	KEP_EventEncodeNumber( ev, optMsg )
	KEP_EventQueue( ev )
end

function ProgressLastGoodUrlSet(id, url)
    pfLog("ProgressLastGoodUrlSet: '"..id.."' url='"..url.."'")
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_LAST_GOOD_URL_SET)
	KEP_EventEncodeString( ev, id )
	KEP_EventEncodeString( ev, url )
	KEP_EventQueue( ev )
end

function ProgressLastGoodUrlGet(id)
    pfLog("ProgressLastGoodUrlGet: '"..id.."'")
	local ev = KEP_EventCreate( "ProgressEvent" )
	KEP_EventSetFilter(ev, PROGRESS_LAST_GOOD_URL_GET)
	KEP_EventEncodeString( ev, id )
	KEP_EventQueue( ev )
end

function CloseProgressMenu(loadStatus, errorMessage)
	local ev = KEP_EventCreate("CloseProgressMenu")
	KEP_EventEncodeString(ev, loadStatus)
	if loadStatus == "Errored" and errorMessage ~= nil then
		KEP_EventEncodeString(ev, errorMessage)
	end
    KEP_EventQueue(ev)
end