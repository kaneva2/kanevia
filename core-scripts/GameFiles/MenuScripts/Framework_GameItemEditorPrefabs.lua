--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

PROPERTY_TYPES = {	BUTTON_SELECT = "buttonSelect",
					BUTTON_SELECT_TEST = "buttonSelectTest",
					DROP_DOWN = "dropDown", 
					TEXT = "text",
					TITLE = "title",
					CHECK_BOX = "checkBox", 
					ACCESSORIES = "accessories", 
					ADD_ITEM = "addAccesories", 
					LOOT = "loot", 
					PARTICLE_EFFECTS = "particleEffects",
					PARTICLE_ITEM = "particleItem",
					PARTICLE_DROP = "particleDrop",
					DIALOG_PAGES = "dialogPages",
					DIALOG_ITEM = "dialogItem",
					LOOT_ITEM = "lootItem", 
					LOOT_SUB_ITEM = "lootSubItem", 
					LOOT_DROP = "lootDrop",
					DELETABLE_ITEM = "deletableItem",
					CLOTHING = "clothing",
					TRADES = "trades",
					QUESTS = "quests",
					STAGES = "stages",
					SINGLE_DYNAMIC_ITEMS = "singleDynamicItems",
					COLOR_SELECT = "colorSelect",
					PLAYLIST_SELECT = "playlistSelect",
					IMAGE_SELECT = "imageSelect",
					WORLD_SELECT = "worldSelect",
					WAYPOINT = "waypoint",
					NODES = "nodes",
					PREVIEW = "preview",
					EDIT = "edit",
					EDIT_DYNAMIC = "editDynamic",
					MARKER = "marker"}

--Controls for the collapsable categories
LIST_ITEM_CATEGORY_CONTROLS = {"imgListItemCategoryBase", "btnListItemCategory", "imgListItemCategoryArrowOpen", "imgListItemCategoryArrowClosed", "stcListItemCategory", "imgListItemCategoryBG"}

--Controls for the property prefab types
PROPERTY_TYPE_CONTROLS = {}	
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.BUTTON_SELECT] = {"imgListItemBase", "stcListItemName", "btnJumpToItem", "stcNameBtnSelect", "imgBtnSelectBG", "btnSelect", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.BUTTON_SELECT_TEST] = {"imgListItemBase", "stcListItemNameShort", "btnJumpToItem", "btnTestAnim", "stcNameBtnSelect", "imgBtnSelectBG", "btnSelect", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.DROP_DOWN] = {"imgListItemBase", "stcListItemName", "cmbDropDown", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.TEXT] = {"imgListItemBase", "stcListItemName", "stcSuffix", "edText", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.TITLE] = {"imgListItemBase", "stcListItemTitle", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.CHECK_BOX] = {"imgListItemBase", "stcListItemName", "boxCheck", "stcCheckBoxOn", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.ACCESSORIES] = {"imgListItemBase", "btnRemoveRight", "stcListItemName", "btnJumpToItemShort", "stcNameAccessory", "imgNameBGAccessory", "btnEditAccessory", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.ADD_ITEM] = {"imgListItemBase", "btnAddItem", "stcAddItem", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.LOOT] = {}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.TRADES] = {}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.PARTICLE_EFFECTS] = {}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.PARTICLE_ITEM] = {"imgListItemBase", "btnRemove", "stcListItemName", "btnJumpToItem", "stcNameBtnSelect", "imgBtnSelectBG", "btnSelect", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.PARTICLE_DROP] = {"imgListItemBase", "stcListItemName", "cmbDropDown", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.LOOT_ITEM] = {"imgListItemBase", "btnRemove", "stcListItemName", "btnJumpToItem", "stcNameBtnSelect", "imgBtnSelectBG", "btnSelect", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.DELETABLE_ITEM] = {"imgListItemBase", "btnRemove", "stcListItemName", "btnJumpToItem", "stcNameBtnSelect", "imgBtnSelectBG", "btnSelect", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.DIALOG_ITEM] = {"imgListItemBase", "btnRemove", "stcListItemName", "btnJumpToItem", "stcNameBtnSelect", "imgBtnSelectBG", "btnSelect", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.LOOT_SUB_ITEM] = {"imgListItemBase", "stcListItemName", "btnJumpToItem", "stcNameBtnSelect", "imgBtnSelectBG", "btnSelect", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.LOOT_DROP] = {"imgListItemBase", "stcListItemName", "stcSuffix", "edText", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.COLOR_SELECT] = {"imgListItemBase", "stcListItemName", "imgListItemDivider", "btnEditColor", "imgColorBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.QUESTS] = {"imgListItemBase", "btnRemoveRight", "stcListItemName", "btnJumpToItemShort", "stcNameAccessory", "imgNameBGAccessory", "btnEditAccessory", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.DIALOG_PAGES] = {"imgListItemBase", "btnRemoveRight", "stcListItemName", "btnJumpToItemShort", "stcNameAccessory", "imgNameBGAccessory", "btnEditAccessory", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.WAYPOINT] = {"imgListItemBase", "stcListItemName", "imgListItemDivider", "btnSetWaypoint", "btnGoToWaypoint", "btnRemoveWaypoint"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.SINGLE_DYNAMIC_ITEMS] = {"imgListItemBase", "btnRemoveRight", "stcListItemName", "btnJumpToItemShort", "stcNameAccessory", "imgNameBGAccessory", "btnEditAccessory", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.PLAYLIST_SELECT] = {"imgListItemBase", "stcListItemName", "btnJumpToItem", "stcNameBtnSelect", "imgBtnSelectBG", "btnSelect", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.IMAGE_SELECT] = {"imgListItemBase", "stcListItemName", "btnJumpToItem", "stcNameBtnSelect", "imgBtnSelectBG", "btnSelect", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.WORLD_SELECT] = {"imgListItemBase", "stcListItemName", "btnJumpToItem", "stcNameBtnSelect", "imgBtnSelectBG", "btnSelect", "imgListItemDivider", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.PREVIEW] = {"imgListItemBase", "btnPreview", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.EDIT] = {"imgListItemBase", "stcListItemName", "imgListItemDivider","btnEdit", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.EDIT_DYNAMIC] = {"imgListItemBase", "btnRemove", "stcListItemName", "imgListItemDivider","btnEdit", "imgListItemBG"}
	PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.MARKER] = {}
	--PROPERTY_TYPE_CONTROLS[PROPERTY_TYPES.CLOTHING] = {}

PROPERTIES_PREFABS = {}
	--These properties determines each attribute's prefab displays, behavior, and values

	-- == General == --
		-- text (string) - The title text on the left side of the editor
		-- propType (CONST) - The type of prefab display that will be used for this attribute
		-- attribute (string) - The attribute being edited for the game item
		-- parent (string) - This will add the attribute and it's value to a table of this name
		-- addByKey (bool) - This will add the attribute to the given parent by the attribute's name
		-- thumbnail (bool) - This value will be used to display the thumbnail image (Only GLID values right now)
		-- width (number) - Width of the input box
		-- number (bool) - Does the input have to be a number?
		-- default (any) - Is there a fall back default value to use?

	-- == Edit Box == --
		-- min (number) - minimum value for the input
		-- max (number) - maximum value for the input

	-- == Button Select == --
		-- selectType - The type of item being displayed
			-- COLOR - Colors
			-- PLAYLIST - Playlists
			-- ACCESSORY - Accessories
			-- SOUND - Sounds
			-- PARTICLE - Particles
			-- OBJECT - Inventory Objects
			-- OBJECT_ANIMATION - World Object Animations
			-- DIALOG - Brings up the Dialog Editor Menu
			-- UNID - Game System Objects
				-- typeFilter - Filters out game items by itemType
				-- behaviorFilter - Filters out game items by behavior
			-- MEDIAPLAYER - Objects that can play media
			--OBJECT_ACCESSORY  -Will include both the object and accessory lists

	-- == Drop Down == -- 
		-- displayValues (table) -- Table of display values for the drop down box
		-- trueValues (table) -- The actual values for a drop down (This maps by index to the display values)

	-- == Dynamic Lists == --
		-- defaultDataStuct (table) -- If the data for an item in the list is a table, what are the table values needed?
		-- maxCount (number) -- How many items can be added to this list?

	-- == LOOT == --
	PROPERTIES_PREFABS["weapon"] =	{	{text = "Basic Properties", properties ={	{text = "Accessory Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "ACCESSORY",  thumbnail = true, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare", "Never"}},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Range", propType = PROPERTY_TYPES.TEXT, attribute = "range", width = 70, number = true, min = 0, suffix = "feet"},
																					{text = "Damage Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "bonusDamage", width = 70, number = true, min = 0},
																					{text = "Rate of Fire", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rateOfFire", displayValues = {"Slow", "Normal", "Fast", "Very Fast"}, trueValues = {2.5, 1, .75, .5}, width = 100},
																					{text = "Ammo Type", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "ammoType", selectType = "UNID", typeFilter = "ammo", number = true, default = 0},
																					{text = "Is Healing Weapon", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "healingWeapon"},
																					{text = "Is Repair Tool", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repairTool"},
																				}
										},
										{text = "Animations", 		properties ={	{text = "Animation Set", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "animationSet", displayValues = {"Handgun", "Rifle", "Knife", "Sword"}, trueValues = {"One-Handed Ranged", "Two-Handed Ranged", "One-Handed Melee", "Two-Handed Melee"}},
																				}
										},
										
										{text = "Sound", 			properties ={	{text = "Sound Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "soundGLID", selectType = "SOUND", number = true, default = 0},
																					{text = "Sound Range", propType = PROPERTY_TYPES.TEXT, attribute = "soundRange", width = 70, number = true, min = 0, suffix = "feet", default = 0},
																				}
										}
										--[[
										{text = "Effects", 			properties ={	{text = "Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "particle", selectType = "PARTICLE", number = true},
																				}
										}
										]]
									}
	-- Name, description, mesh, level 1 - 5
	PROPERTIES_PREFABS["paintbrush"] =	{	{text = "Basic Properties", properties ={	{text = "Accessory Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "ACCESSORY",  thumbnail = true, number = true},
																						{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																				}
										}
									}

	PROPERTIES_PREFABS["emoteItem"] =	{	{text = "Basic Properties", properties ={	{text = "Thumbnail", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "ACCESSORY", thumbnail = true, number = true},
																							{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare", "Never"}},
																							{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																							{text = "Rate of Fire", propType = PROPERTY_TYPES.TEXT, attribute = "rateOfFire", width = 70, number = true, min = 5, max = 3600, suffix = "seconds", default = 10}
																				}	
										},
										{text = "Male Accessories", properties ={	{text = "Accessory Mesh", propType = PROPERTY_TYPES.ACCESSORIES, attribute = "maleGLIDS", parent = "addGLIDS", selectType = "ACCESSORY", number = true},
																					{text = "Add Accessory", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "maleGLIDS", maxCount = 10},
																				}
										},
										{text = "Female Accessories", properties ={	{text = "Accessory Mesh", propType = PROPERTY_TYPES.ACCESSORIES, attribute = "femaleGLIDS", parent = "addGLIDS", selectType = "ACCESSORY", number = true},
																					{text = "Add Accessory", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "femaleGLIDS", maxCount = 10},
																				}
										},
										{text = "Emote", 		properties ={	{text = "Male Animation", propType = PROPERTY_TYPES.BUTTON_SELECT_TEST, attribute = "emoteM", selectType = "ALL_ANIMATIONS", number = true, default = 0},
																				{text = "Female Animation", propType = PROPERTY_TYPES.BUTTON_SELECT_TEST, attribute = "emoteF", selectType = "ALL_ANIMATIONS", number = true, default = 0},
																			}
										},
										{text = "Sound", 			properties ={	{text = "Sound Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "soundGLID", selectType = "SOUND", number = true, default = 0},
																					{text = "Sound Range", propType = PROPERTY_TYPES.TEXT, attribute = "soundRange", width = 70, number = true, min = 0, suffix = "feet", default = 0},
																				}
										},
										{text = "Effects", 			properties ={	{propType = PROPERTY_TYPES.PARTICLE_EFFECTS, attribute = "particleEffectsMine", parent = "particleEffects", defaultDataStuct = {effect=0, bone="Bip01 Head"}, 
																												propertyComponents = 	{	{text = "     Particle", propType = PROPERTY_TYPES.PARTICLE_ITEM, attribute = "effect", selectType = "PARTICLE", parent = "particleEffects", number = true},
																																			{text = "     Attaches To", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "bone", parent = "particleEffects", displayValues = {"Head", "Waist", "Left Hand", "Right Hand", "Left Foot", "Right Foot"}, trueValues = {"Bip01 Head", "Bip01 Pelvis", "Bip01 L Hand", "Bip01 R Hand", "Bip01 L Foot", "Bip01 R Foot"}},
																																		}
																				},
																				{text = "Add Input Requirement", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "particleEffectsMine", maxCount = 6}
																				}
										}
									}

	PROPERTIES_PREFABS["p2pEmoteItem"] =	{	{text = "Basic Properties", properties ={	{text = "Thumbnail", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "ACCESSORY", thumbnail = true, number = true},
																							{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare", "Never"}},
																							{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																							{text = "Range", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "range", displayValues = {"5 feet", "10 feet", "20 feet", "30 feet"}, trueValues = {5, 10, 20, 30}, width = 100},
																							{text = "Rate of Fire", propType = PROPERTY_TYPES.TEXT, attribute = "rateOfFire", width = 70, number = true, min = 5, max = 3600, suffix = "seconds", default = 10}
																				}	
										},
										{text = "Male Accessories", properties ={	{text = "Accessory Mesh", propType = PROPERTY_TYPES.ACCESSORIES, attribute = "maleGLIDS", parent = "addGLIDS", selectType = "ACCESSORY", number = true},
																					{text = "Add Accessory", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "maleGLIDS", maxCount = 10},
																				}
										},
										{text = "Female Accessories", properties ={	{text = "Accessory Mesh", propType = PROPERTY_TYPES.ACCESSORIES, attribute = "femaleGLIDS", parent = "addGLIDS", selectType = "ACCESSORY", number = true},
																					{text = "Add Accessory", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "femaleGLIDS", maxCount = 10},
																				}
										},
										{text = "Sound", 			properties ={	{text = "Sound Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "soundGLID", selectType = "SOUND", number = true, default = 0},
																					{text = "Sound Range", propType = PROPERTY_TYPES.TEXT, attribute = "soundRange", width = 70, number = true, min = 0, suffix = "feet", default = 0},
																				}
										},
										{text = "Your Emote", 		properties ={	{text = "Male Animation", propType = PROPERTY_TYPES.BUTTON_SELECT_TEST, attribute = "emoteM", selectType = "ALL_ANIMATIONS", number = true, default = 0},
																				{text = "Female Animation", propType = PROPERTY_TYPES.BUTTON_SELECT_TEST, attribute = "emoteF", selectType = "ALL_ANIMATIONS", number = true, default = 0},
																			}
										},
										{text = "Your Effects", 			properties ={	{propType = PROPERTY_TYPES.PARTICLE_EFFECTS, attribute = "particleEffectsMine", parent = "particleEffects", defaultDataStuct = {effect=0, bone="Bip01 Head"}, 
																												propertyComponents = 	{	{text = "     Particle", propType = PROPERTY_TYPES.PARTICLE_ITEM, attribute = "effect", selectType = "PARTICLE", parent = "particleEffects", number = true},
																																			{text = "     Attaches To", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "bone", parent = "particleEffects", displayValues = {"Head", "Waist", "Left Hand", "Right Hand", "Left Foot", "Right Foot"}, trueValues = {"Bip01 Head", "Bip01 Pelvis", "Bip01 L Hand", "Bip01 R Hand", "Bip01 L Foot", "Bip01 R Foot"}},
																																		}
																				},
																				{text = "Add Input Requirement", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "particleEffectsMine", maxCount = 6}
																				},
										},
										{text = "Your Target's Emote", 		properties ={	{text = "Male Animation", propType = PROPERTY_TYPES.BUTTON_SELECT_TEST, attribute = "emoteMTheirs", selectType = "ALL_ANIMATIONS", number = true, default = 0},
																				{text = "Female Animation", propType = PROPERTY_TYPES.BUTTON_SELECT_TEST, attribute = "emoteFTheirs", selectType = "ALL_ANIMATIONS", number = true, default = 0},
																			}
										},
										{text = "Your Target's Effects", 			properties ={	{propType = PROPERTY_TYPES.PARTICLE_EFFECTS, attribute = "particleEffectsTheirs", parent = "particleEffectsTheirs", defaultDataStuct = {effect=0, bone="Bip01 Head"}, 
																												propertyComponents = 	{	{text = "     Particle", propType = PROPERTY_TYPES.PARTICLE_ITEM, attribute = "effect", selectType = "PARTICLE", parent = "particleEffectsTheirs", number = true},
																																			{text = "     Attaches To", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "bone", parent = "particleEffectsTheirs", displayValues = {"Head", "Waist", "Left Hand", "Right Hand", "Left Foot", "Right Foot"}, trueValues = {"Bip01 Head", "Bip01 Pelvis", "Bip01 L Hand", "Bip01 R Hand", "Bip01 L Foot", "Bip01 R Foot"}},
																																		}
																				},
																				{text = "Add Input Requirement", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "particleEffectsTheirs", maxCount = 6}
																				},
										}
									}

	PROPERTIES_PREFABS["ammo"] =	{	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare", "Never"}}
																				}
										}
									}
	PROPERTIES_PREFABS["armor"] =	{	{text = "Basic Properties", properties ={	{text = "Thumbnail", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "ACCESSORY", thumbnail = true, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare", "Never"}},
																					{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "bonusHealth", width = 70, number = true, min = 0, default = 3},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, trueValues = {1, 2, 3, 4, 5}, width = 70, number = true},
																					{text = "Type", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "slotType", displayValues = {"Head", "Chest", "Legs", "Arms"}, trueValues = {"head", "chest", "legs", "arms"}, width = 100},
																				}
										},
										{text = "Male Accessories", properties ={	{text = "Accessory Mesh", propType = PROPERTY_TYPES.ACCESSORIES, attribute = "maleGLIDS", parent = "addGLIDS", selectType = "ACCESSORY", number = true},
																					{text = "Add Accessory", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "maleGLIDS", maxCount = 10},
																				}
										},
										{text = "Female Accessories", properties ={	{text = "Accessory Mesh", propType = PROPERTY_TYPES.ACCESSORIES, attribute = "femaleGLIDS", parent = "addGLIDS", selectType = "ACCESSORY", number = true},
																					{text = "Add Accessory", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "femaleGLIDS", maxCount = 10},
																				}
										}
									}
	PROPERTIES_PREFABS["consumable"] = {{text = "Basic Properties", properties ={	{text = "Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT_ACCESSORY",  thumbnail = true, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare", "Never"}},
																					{text = "Property Buff on use", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "consumeType", displayValues = {"Energy", "Health"}, trueValues = {"energy", "health"} },
																					{text = "Amount of Buff", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "consumeValue", displayValues = {"10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%"}, trueValues = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100}, width = 90, number = true},
																					{text = "Male Animation", propType = PROPERTY_TYPES.BUTTON_SELECT_TEST, attribute = "emoteM", selectType = "ALL_ANIMATIONS", number = true, default = 0},
																					{text = "Female Animation", propType = PROPERTY_TYPES.BUTTON_SELECT_TEST, attribute = "emoteF", selectType = "ALL_ANIMATIONS", number = true, default = 0},
																					{text = "Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "soundGLID", selectType = "SOUND", number = true, default = 0},
																					{text = "Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "effect", selectType = "PARTICLE", number = true},
																				}
										},
										
									}

	PROPERTIES_PREFABS["generic"] = {	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},	
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare", "Never"}}
																				}
										}
									}

	-- == RECIPE == --
	PROPERTIES_PREFABS["recipe"] = 	{	{text = "Basic Properties", properties ={	{text = "Output Item", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "output", selectType = "UNID", number = true}
																				}
										},
										{text = "Input", 			properties ={	{propType = PROPERTY_TYPES.LOOT, attribute = "inputs", defaultDataStuct = {UNID=0, count=1}, 
																												propertyComponents = 	{	{text = "     Loot Item", propType = PROPERTY_TYPES.LOOT_ITEM, attribute = "UNID", parent = "input", selectType = "UNID", number = true},
																																			{text = "        Item Count", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "count", parent = "input", width = 50, number = true, min = 1, max = 9999},
																																		}
																				},
																				{text = "Add Input Requirement", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "inputs", maxCount = 3},
																			}
										},
										{text = "Crafting Helpers", properties ={	{text = "Crafting Helper", propType = PROPERTY_TYPES.SINGLE_DYNAMIC_ITEMS, attribute = "proxReq", selectType = "UNID", typeFilter = "placeable", behaviorFilter = "campfire", number = true},
																					{text = "Add Crafting Helper", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "proxReq", maxCount = 10},
																				}
										}
									}

	PROPERTIES_PREFABS["blueprint"] = {	{text = "Basic Properties", properties ={	{text = "Output Item", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "output", selectType = "UNID", number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare", "Never"}},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "One Use Only", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "oneUse"}
																				}
										},
										{text = "Input", 			properties ={	{propType = PROPERTY_TYPES.LOOT, attribute = "inputs", defaultDataStuct = {UNID=0, count=1}, 
																												propertyComponents = 	{	{text = "     Loot Item", propType = PROPERTY_TYPES.LOOT_ITEM, attribute = "UNID", parent = "input", selectType = "UNID", number = true},
																																			{text = "        Item Count", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "count", parent = "input", width = 50, number = true, min = 1, max = 9999},
																																		}
																				},
																				{text = "Add Input Requirement", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "inputs", maxCount = 3},
																			}
										},
										{text = "Crafting Helpers", properties ={	{text = "Crafing Helper", propType = PROPERTY_TYPES.SINGLE_DYNAMIC_ITEMS, attribute = "proxReq", selectType = "UNID", typeFilter = "placeable", behaviorFilter = "campfire", number = true},
																					{text = "Add Crafing Helper", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "proxReq", maxCount = 10},
																				}
										}
									}

	-- == CHARACTERS == --
	PROPERTIES_PREFABS["character"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																					{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true}
																				}
										}
									}
	PROPERTIES_PREFABS["monster"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																					{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																					{text = "Rotation", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "eulerRotY", parent = "behaviorParams", addByKey = true, displayValues = {"0?? (No rotation)", "45??", "90?? (1/4 Turn)", "135??", "180?? (1/2 Turn)", "225??", "270?? (3/4 Turn)", "305??"}, trueValues = {0, 45, 90, 135, 180, 225, 270, 305}, number = true},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Health Multiplier", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "healthMultiplier", displayValues = {"1", "10", "100", "1,000", "10,000", "100,000", "1,000,000"}, trueValues = {1, 10, 100, 1000, 10000, 100000, 1000000}, parent = "behaviorParams", width = 120, addByKey = true, number = true}
																				}
										},
										{text = "Behavior", 		properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet per second"},
																					{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet per second"},
																					{text = "Aggro Range", propType = PROPERTY_TYPES.TEXT, attribute = "aggroRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet"},
																					{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																					{text = "Attack Range", propType = PROPERTY_TYPES.TEXT, attribute = "attackRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet"},
																					{text = "Attack Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackSound", parent = "behaviorParams", addByKey = true, selectType = "SOUND", number = true, default = 4522644}
																				}
										},
										{text = "Animations", 		properties ={	{text = "Move Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "moving", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true},
																					{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idle", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true},
																					{text = "Attacking Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attacking", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true},
																					{text = "Death Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "death", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true}
																				}
										},
										{text = "General Loot", 	properties ={	{text = "Loot Container Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "spawnGLID", selectType = "OBJECT", parent = "behaviorParams", addByKey = true, number = true, default = 4468596},
																					{text = "Armor (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "armor", parent = "loot", addByKey = true},
																					{text = "Weapon (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "weapon", parent = "loot", addByKey = true},
																					{text = "Tool (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "tool", parent = "loot", addByKey = true},
																					{text = "Consumables (2 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "consumable", parent = "loot", addByKey = true},
																					{text = "Ammunition (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "ammo", parent = "loot", addByKey = true},
																					{text = "Blueprint (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "blueprint", parent = "loot", addByKey = true},
																					{text = "Seeds (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "seed", parent = "loot", addByKey = true},
																					{text = "Generic Item (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "generic", parent = "loot", addByKey = true},																								
																				}
										},
										{text = "Custom Loot", 	properties ={	{propType = PROPERTY_TYPES.LOOT, attribute = "singles", parent = "loot", defaultDataStuct = {UNID=0, drop=0}, 
																												propertyComponents = 	{	{text = "     Loot Item", propType = PROPERTY_TYPES.LOOT_ITEM, attribute = "UNID", parent = "lootTable", selectType = "UNID", number = true},
																																			{text = "        Drop Chance", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "drop", parent = "lootTable", width = 30, suffix = "%", number = true, min = 0, max = 100},
																																		}
																				},
																				{text = "Add Loot", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "singles", maxCount = 10},
																			}
										}
									}
	PROPERTIES_PREFABS["pet"] = {		{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																					{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																					{text = "Rotation", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "eulerRotY", parent = "behaviorParams", addByKey = true, displayValues = {"0?? (No rotation)", "45??", "90?? (1/4 Turn)", "135??", "180?? (1/2 Turn)", "225??", "270?? (3/4 Turn)", "305??"}, trueValues = {0, 45, 90, 135, 180, 225, 270, 305}, number = true},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare"}},
																					{text = "Attack Range", propType = PROPERTY_TYPES.TEXT, attribute = "attackRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet"},
																					{text = "Enable Combat", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "combatEnabled", parent = "behaviorParams", addByKey = true, default = false},
																					{text = "Requires Food", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "requiresFood", parent = "behaviorParams", addByKey = true, default = false},
																					{text = "Food", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "food", selectType = "UNID", typeFilter = "generic", number = true, default = 0}
																				}
										},
										{text = "Movement", 		properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second"},
																					{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second"},
																					{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																				}
										},
										{text = "Animations", 		properties ={	{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 0},
																					{text = "Walk Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "walkAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true},
																					{text = "Run Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "runAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true},
																					{text = "Death Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true},
																					{text = "Attack Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true}
																				}
										},
										{text = "Sounds", 			properties ={	{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																					{text = "Follow Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true},
																					{text = "Chase Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chaseSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true},
																					{text = "Death Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true},
																					{text = "Attack Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true}
																				}
										},
										{text = "Particle Effects", properties ={	{text = "Idle Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idlePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																					{text = "Follow Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true},
																					{text = "Chase Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chasePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true},
																					{text = "Death Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true},																					
																					{text = "Attack Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true}
																					
																				}
										},
									}
	PROPERTIES_PREFABS["animal"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																					{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																					{text = "Rotation", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "eulerRotY", parent = "behaviorParams", addByKey = true, displayValues = {"0?? (No rotation)", "45??", "90?? (1/4 Turn)", "135??", "180?? (1/2 Turn)", "225??", "270?? (3/4 Turn)", "305??"}, trueValues = {0, 45, 90, 135, 180, 225, 270, 305}, number = true},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true}
																				}
										},
										{text = "Movement", 		properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second"},
																					{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second"},
																					{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																				}
										},
										{text = "Animations", 		properties ={	{text = "Move Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "moving", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true},
																					{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idle", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 0}
																				}
										},
										{text = "Loot", 			properties ={	{text = "Loot Container Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "spawnGLID", selectType = "OBJECT", parent = "behaviorParams", addByKey = true, number = true, default = 4468596},
																					{text = "Item Dropped", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "UNID", selectType = "UNID", parent = "limitItems", addByKey = true, number = true},
																					{text = "Drop Limit", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "limit", displayValues = {"1", "2", "3", "4", "5"}, parent = "limitItems", addByKey = true, width = 70, number = true}
																				}
										}
									}
	PROPERTIES_PREFABS["actor"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																				{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																				{text = "Display Name", propType = PROPERTY_TYPES.TEXT, attribute = "actorName", parent = "behaviorParams", addByKey = true, min = 0, max = 21},
																				{text = "Title", propType = PROPERTY_TYPES.TEXT, attribute = "actorTitle", parent = "behaviorParams", addByKey = true, min = 0, max = 21},
																				{text = "Show Display Name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showLabels", parent = "behaviorParams", addByKey = true, default = true},
																				{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 4246971},
																			},
									},
									{text = "Advanced Mesh Properties", properties ={	{text = "Rotation", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "eulerRotY", parent = "behaviorParams", addByKey = true, displayValues = {"0?? (No rotation)", "45??", "90?? (1/4 Turn)", "135??", "180?? (1/2 Turn)", "225??", "270?? (3/4 Turn)", "305??"}, trueValues = {0, (math.pi/4), (math.pi/2), (3*math.pi/4), (math.pi), (5*math.pi/4), (3*math.pi/2), (7*math.pi/4)}, number = true},
																					}
									},
									{text = "Dialog Pages", properties ={	{text = "Dialog", propType = PROPERTY_TYPES.DIALOG_PAGES, attribute = "dialogPages", parent = "behaviorParams", defaultDataStuct = {dialogPage="Greetings."},
																											propertyComponents = 	{text = "     Dialog", propType = PROPERTY_TYPES.DIALOG_ITEM, attribute = "dialogPage", selectType = "DIALOG", addByKey = true, default = "Default text."
																																	}
																			},
																			{text = "     Add Dialog", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "dialogPages", maxCount = 9},
																		}
									},
									{text = "Reputation", properties ={		{text = "Enable Reputation", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repEnabled", parent = "behaviorParams", addByKey = true, default = false},
																			{text = "Reputation Scale", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repScale", parent = "behaviorParams"	, addByKey = true, displayValues = {"Easy", "Normal", "Hard"}, default = "Normal"},
																			{text = "Reputation Unlock Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repUnlock", parent = "behaviorParams"	, addByKey = true, displayValues = {"Acquaintance", "Friend", "Close Friend", "Best Friend", "Follower", "Spouse"}, default = "Friendly"},
																			{text = "Basic Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftBasic", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Standard Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftStandard", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Special Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpecial", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Follower Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftFollower", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Spouse Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpouse", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 404},
																			{text = "Gifting Interval", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "giftInterval", parent = "behaviorParams", addByKey = true, displayValues = {"None", "1/2 hour", "1 hour", "2 hours", "6 hours", "12 hours", "1 day", "1 week"}, trueValues = {0, 30, 60, 120, 360, 720, 1440, 10080}, number = true, default = 1440}, -- Time in minutes
																			{text = "Standard Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgGift", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Follower Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgFollow", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Spouse Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgSpouse", parent = "behaviorParams", selectType = "DIALOG", addByKey = true, default = ""},
																		}
									},
									{text = "Follower Properties", properties ={	{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Enable Combat", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "combatEnabled", parent = "behaviorParams", addByKey = true, default = false},
																					{text = "Allow Player Customization", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "customization", parent = "behaviorParams", addByKey = true, default = true}
																				}
									},
									{text = "Idle", properties ={	-- {text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 3931355}
																	{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246971},
																	{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																	{text = "Idle Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idlePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																}
									},
									{text = "Walk/Wander", properties ={	{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 10},
																			{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																			{text = "Walk Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "walkAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246986},
																			{text = "Walk Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Walk Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																		}
									},
									{text = "Run", properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 25},
																	{text = "Run Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "runAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246983},
																	{text = "Run Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chaseSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																	{text = "Run Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chasePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																}
									},
									{text = "Attack", properties ={		{text = "Attack Range", propType = PROPERTY_TYPES.TEXT, attribute = "attackRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet"},
																		{text = "Attack Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 3317748},
																		{text = "Attack Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Attack Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																}
									},
									{text = "Death", properties ={		
																		{text = "Death Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4154955},
																		{text = "Death Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Death Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																}
									},
								}
	PROPERTIES_PREFABS["vendor"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																					{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																					{text = "Display Name", propType = PROPERTY_TYPES.TEXT, attribute = "actorName", parent = "behaviorParams", addByKey = true},
																					{text = "Title", propType = PROPERTY_TYPES.TEXT, attribute = "actorTitle", parent = "behaviorParams", addByKey = true},
																					{text = "Show Display Name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showLabels", parent = "behaviorParams", addByKey = true, default = true},
																					{text = "Header", propType = PROPERTY_TYPES.TEXT, attribute = "header", parent = "behaviorParams", addByKey = true, max = 34},
																					{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 4246971},
																				}
										},
										{text = "Trades", 			properties ={	{propType = PROPERTY_TYPES.TRADES, attribute = "trades", parent = "trades", defaultDataStuct = {input=0, output=0, outputCount=1, cost=1}, 
																											propertyComponents = 	{	{text = "     Item For Sale", propType = PROPERTY_TYPES.LOOT_ITEM, attribute = "output", selectType = "UNID", number = true},
																																		{text = "     Item For Sale Count", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "outputCount", width = 70, number = true, min = 1, default = 1},
																																		{text = "        Required Item", propType = PROPERTY_TYPES.LOOT_SUB_ITEM, attribute = "input", selectType = "UNID", number = true},
																																		{text = "        Required Item Count", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "cost", width = 70, number = true, min = 1}																																		
																																	}
																					},


																					{text = "Add Trade", propType = PROPERTY_TYPES.ADD_ITEM, maxCount = 27, attribute = "trades"},
																				}
										},
										{text = "Reputation", properties ={		{text = "Enable Reputation", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repEnabled", parent = "behaviorParams", addByKey = true, default = false},
																			{text = "Reputation Scale", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repScale", parent = "behaviorParams"	, addByKey = true, displayValues = {"Easy", "Normal", "Hard"}, default = "Normal"},
																			{text = "Reputation Unlock Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repUnlock", parent = "behaviorParams"	, addByKey = true, displayValues = {"Acquaintance", "Friend", "Close Friend", "Best Friend", "Follower", "Spouse"}, default = "Friendly"},
																			{text = "Basic Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftBasic", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Standard Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftStandard", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Special Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpecial", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Follower Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftFollower", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Spouse Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpouse", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 404}, 
																			{text = "Gifting Interval", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "giftInterval", parent = "behaviorParams", addByKey = true, displayValues = {"None", "1/2 hour", "1 hour", "2 hours", "6 hours", "12 hours", "1 day", "1 week"}, trueValues = {0, 30, 60, 120, 360, 720, 1440, 10080}, number = true, default = 1440}, -- Time in minutes
																			{text = "Standard Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgGift", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Follower Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgFollow", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Spouse Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgSpouse", parent = "behaviorParams", selectType = "DIALOG", addByKey = true, default = ""},
																		}
										},
										{text = "Follower Properties", properties ={	{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																						{text = "Enable Combat", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "combatEnabled", parent = "behaviorParams", addByKey = true, default = false},
																						{text = "Allow Player Customization", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "customization", parent = "behaviorParams", addByKey = true, default = true}
																					}
										},
										{text = "Idle", properties ={	-- {text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 3931355}
																		{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246971},
																		{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Idle Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idlePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Walk/Wander", properties ={	{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 10},
																				{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																				{text = "Walk Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "walkAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246986},
																				{text = "Walk Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																				{text = "Walk Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																			}
										},
										{text = "Run", properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 25},
																		{text = "Run Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "runAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246983},
																		{text = "Run Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chaseSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Run Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chasePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Attack", properties ={		{text = "Attack Range", propType = PROPERTY_TYPES.TEXT, attribute = "attackRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet"},
																			{text = "Attack Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 3317748},
																			{text = "Attack Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Attack Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Death", properties ={		
																			{text = "Death Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4154955},
																			{text = "Death Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Death Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
									}
	PROPERTIES_PREFABS["clothing"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																					{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																					{text = "Display Name", propType = PROPERTY_TYPES.TEXT, attribute = "actorName", parent = "behaviorParams", addByKey = true},
																					{text = "Title", propType = PROPERTY_TYPES.TEXT, attribute = "actorTitle", parent = "behaviorParams", addByKey = true},
																					{text = "Show Display Name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showLabels", parent = "behaviorParams", addByKey = true, default = true},
																					{text = "Header", propType = PROPERTY_TYPES.TEXT, attribute = "header", parent = "behaviorParams", addByKey = true, max = 34},
																					{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 4246971},
																				}
										},
										{text = "Clothing", 		properties ={	{propType = PROPERTY_TYPES.TRADES, attribute = "trades", parent = "trades", defaultDataStuct = {output=0}, 
																											propertyComponents = 	{	{text = "     Item For Sale", propType = PROPERTY_TYPES.LOOT_ITEM, attribute = "output", selectType = "CLOTHING", number = true} --,
																																		--{text = "     Item For Sale Count", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "outputCount", width = 70, number = true, min = 1, default = 1},
																																		--{text = "        Required Item", propType = PROPERTY_TYPES.LOOT_SUB_ITEM, attribute = "input", selectType = "UNID", number = true},
																																		--{text = "        Required Item Count", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "cost", width = 70, number = true, min = 1}																																		
																																	}
																					},


																						{text = "Add Clothing", propType = PROPERTY_TYPES.ADD_ITEM, maxCount = 27, attribute = "trades"},
																					}
										},
										{text = "Reputation", properties ={		{text = "Enable Reputation", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repEnabled", parent = "behaviorParams", addByKey = true, default = false},
																			{text = "Reputation Scale", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repScale", parent = "behaviorParams"	, addByKey = true, displayValues = {"Easy", "Normal", "Hard"}, default = "Normal"},
																			{text = "Reputation Unlock Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repUnlock", parent = "behaviorParams"	, addByKey = true, displayValues = {"Acquaintance", "Friend", "Close Friend", "Best Friend", "Follower", "Spouse"}, default = "Friendly"},
																			{text = "Basic Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftBasic", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Standard Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftStandard", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Special Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpecial", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Follower Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftFollower", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Spouse Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpouse", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 404},
																			{text = "Gifting Interval", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "giftInterval", parent = "behaviorParams", addByKey = true, displayValues = {"None", "1/2 hour", "1 hour", "2 hours", "6 hours", "12 hours", "1 day", "1 week"}, trueValues = {0, 30, 60, 120, 360, 720, 1440, 10080}, number = true, default = 1440}, -- Time in minutes
																			{text = "Standard Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgGift", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Follower Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgFollow", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Spouse Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgSpouse", parent = "behaviorParams", selectType = "DIALOG", addByKey = true, default = ""},
																		}
										},
										{text = "Follower Properties", properties ={	{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																						{text = "Enable Combat", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "combatEnabled", parent = "behaviorParams", addByKey = true, default = false},
																						{text = "Allow Player Customization", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "customization", parent = "behaviorParams", addByKey = true, default = true}
																					}
										},
										{text = "Idle", properties ={	-- {text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 3931355}
																		{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246971},
																		{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Idle Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idlePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Walk/Wander", properties ={	{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 10},
																				{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																				{text = "Walk Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "walkAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246986},
																				{text = "Walk Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																				{text = "Walk Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																			}
										},
										{text = "Run", properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 25},
																		{text = "Run Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "runAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246983},
																		{text = "Run Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chaseSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Run Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chasePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Attack", properties ={		{text = "Attack Range", propType = PROPERTY_TYPES.TEXT, attribute = "attackRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet"},
																			{text = "Attack Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 3317748},
																			{text = "Attack Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Attack Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Death", properties ={		
																			{text = "Death Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4154955},
																			{text = "Death Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Death Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
									}
	PROPERTIES_PREFABS["creditsVendor"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																						{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																						{text = "Display Name", propType = PROPERTY_TYPES.TEXT, attribute = "actorName", parent = "behaviorParams", addByKey = true},
																						{text = "Title", propType = PROPERTY_TYPES.TEXT, attribute = "actorTitle", parent = "behaviorParams", addByKey = true},
																						{text = "Show Display Name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showLabels", parent = "behaviorParams", addByKey = true, default = true},
																						{text = "Header", propType = PROPERTY_TYPES.TEXT, attribute = "header", parent = "behaviorParams", addByKey = true, max = 34},
																						{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 4246971},
																					}
										},
										{text = "Trades", 				properties ={	{propType = PROPERTY_TYPES.TRADES, attribute = "trades", parent = "trades", defaultDataStuct = {output = 0, outputCount = 1, cost = 10, input = "Rewards"}, 
																											propertyComponents = 	{	{text = "     Item For Sale", propType = PROPERTY_TYPES.LOOT_ITEM, attribute = "output", selectType = "UNID", number = true},
																																		{text = "     Item For Sale Count", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "outputCount", width = 70, number = true, min = 1, default = 1},
																																		{text = "        Currency Type", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "input", displayValues = {"Rewards", "Credits"}},
																																		{text = "        Sale Price", propType = PROPERTY_TYPES.TEXT, attribute = "cost", number = true, min = 10}
																																	}
																					},
																						{text = "Add Trade", propType = PROPERTY_TYPES.ADD_ITEM, maxCount = 27, attribute = "trades"},
																				}
										},
										{text = "Reputation", properties ={		{text = "Enable Reputation", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repEnabled", parent = "behaviorParams", addByKey = true, default = false},
																			{text = "Reputation Scale", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repScale", parent = "behaviorParams"	, addByKey = true, displayValues = {"Easy", "Normal", "Hard"}, default = "Normal"},
																			{text = "Reputation Unlock Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repUnlock", parent = "behaviorParams"	, addByKey = true, displayValues = {"Acquaintance", "Friend", "Close Friend", "Best Friend", "Follower", "Spouse"}, default = "Friendly"},
																			{text = "Basic Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftBasic", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Standard Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftStandard", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Special Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpecial", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Follower Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftFollower", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Spouse Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpouse", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 404},
																			{text = "Gifting Interval", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "giftInterval", parent = "behaviorParams", addByKey = true, displayValues = {"None", "1/2 hour", "1 hour", "2 hours", "6 hours", "12 hours", "1 day", "1 week"}, trueValues = {0, 30, 60, 120, 360, 720, 1440, 10080}, number = true, default = 1440}, -- Time in minutes
																			{text = "Standard Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgGift", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Follower Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgFollow", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Spouse Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgSpouse", parent = "behaviorParams", selectType = "DIALOG", addByKey = true, default = ""},
																		}
										},
										{text = "Follower Properties", properties ={	{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																						{text = "Enable Combat", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "combatEnabled", parent = "behaviorParams", addByKey = true, default = false},
																						{text = "Allow Player Customization", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "customization", parent = "behaviorParams", addByKey = true, default = true}
																					}
										},
										{text = "Idle", properties ={	-- {text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 3931355}
																		{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246971},
																		{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Idle Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idlePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Walk/Wander", properties ={	{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 10},
																				{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																				{text = "Walk Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "walkAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246986},
																				{text = "Walk Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																				{text = "Walk Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																			}
										},
										{text = "Run", properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 25},
																		{text = "Run Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "runAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246983},
																		{text = "Run Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chaseSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Run Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chasePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Attack", properties ={		{text = "Attack Range", propType = PROPERTY_TYPES.TEXT, attribute = "attackRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet"},
																			{text = "Attack Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 3317748},
																			{text = "Attack Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Attack Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Death", properties ={		
																			{text = "Death Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4154955},
																			{text = "Death Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Death Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
									}
	PROPERTIES_PREFABS["gemVendor"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																					{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																					{text = "Display Name", propType = PROPERTY_TYPES.TEXT, attribute = "actorName", parent = "behaviorParams", addByKey = true},
																					{text = "Title", propType = PROPERTY_TYPES.TEXT, attribute = "actorTitle", parent = "behaviorParams", addByKey = true},
																					{text = "Show Display Name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showLabels", parent = "behaviorParams", addByKey = true, default = true},
																					{text = "Header", propType = PROPERTY_TYPES.TEXT, attribute = "header", parent = "behaviorParams", addByKey = true, max = 34},
																					{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 4246971},
																					}
										},
										{text = "Trades", 				properties ={	{propType = PROPERTY_TYPES.TRADES, attribute = "trades", parent = "trades", defaultDataStuct = {output = 0, outputCount = 1, cost = 1, input = "Topaz"}, 
																											propertyComponents = 	{	{text = "     Item For Sale", propType = PROPERTY_TYPES.LOOT_ITEM, attribute = "output", selectType = "UNID", number = true},
																																		{text = "     Item For Sale Count", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "outputCount", width = 70, number = true, min = 1, default = 1},
																																		{text = "        Gem Type", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "input", displayValues = {"Diamond", "Ruby", "Emerald", "Sapphire", "Topaz"}},
																																		{text = "        Sale Price", propType = PROPERTY_TYPES.TEXT, attribute = "cost", number = true, min = 1}
																																	}
																					},
																						{text = "Add Trade", propType = PROPERTY_TYPES.ADD_ITEM, maxCount = 27, attribute = "trades"},
																				}
										},
										{text = "Reputation", properties ={		{text = "Enable Reputation", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repEnabled", parent = "behaviorParams", addByKey = true, default = false},
																			{text = "Reputation Scale", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repScale", parent = "behaviorParams"	, addByKey = true, displayValues = {"Easy", "Normal", "Hard"}, default = "Normal"},
																			{text = "Reputation Unlock Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repUnlock", parent = "behaviorParams"	, addByKey = true, displayValues = {"Acquaintance", "Friend", "Close Friend", "Best Friend", "Follower", "Spouse"}, default = "Friendly"},
																			{text = "Basic Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftBasic", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Standard Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftStandard", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Special Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpecial", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Follower Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftFollower", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Spouse Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpouse", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 404},
																			{text = "Gifting Interval", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "giftInterval", parent = "behaviorParams", addByKey = true, displayValues = {"None", "1/2 hour", "1 hour", "2 hours", "6 hours", "12 hours", "1 day", "1 week"}, trueValues = {0, 30, 60, 120, 360, 720, 1440, 10080}, number = true, default = 1440}, -- Time in minutes
																			{text = "Standard Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgGift", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Follower Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgFollow", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Spouse Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgSpouse", parent = "behaviorParams", selectType = "DIALOG", addByKey = true, default = ""},
																		}
										},
										{text = "Follower Properties", properties ={	{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																						{text = "Enable Combat", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "combatEnabled", parent = "behaviorParams", addByKey = true, default = false},
																						{text = "Allow Player Customization", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "customization", parent = "behaviorParams", addByKey = true, default = true}
																					}
										},
										{text = "Idle", properties ={	-- {text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 3931355}
																		{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246971},
																		{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Idle Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idlePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Walk/Wander", properties ={	{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 10},
																				{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																				{text = "Walk Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "walkAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246986},
																				{text = "Walk Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																				{text = "Walk Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																			}
										},
										{text = "Run", properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 25},
																		{text = "Run Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "runAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246983},
																		{text = "Run Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chaseSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Run Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chasePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Attack", properties ={		{text = "Attack Range", propType = PROPERTY_TYPES.TEXT, attribute = "attackRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet"},
																			{text = "Attack Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 3317748},
																			{text = "Attack Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Attack Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Death", properties ={		
																			{text = "Death Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4154955},
																			{text = "Death Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Death Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
									}
	PROPERTIES_PREFABS["randomVendor"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																					{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																					{text = "Display Name", propType = PROPERTY_TYPES.TEXT, attribute = "actorName", parent = "behaviorParams", addByKey = true},
																					{text = "Title", propType = PROPERTY_TYPES.TEXT, attribute = "actorTitle", parent = "behaviorParams", addByKey = true},
																					{text = "Show Display Name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showLabels", parent = "behaviorParams", addByKey = true, default = true},
																					{text = "Required Item", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "input", parent = "trade", addByKey = true, selectType = "UNID", number = true, default = 140}, -- defaults to world coin
																					{text = "Required Item Quantity", propType = PROPERTY_TYPES.TEXT, attribute = "cost", parent = "trade", addByKey = true, width = 70, number = true, min = 1},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 0}
																				}
										},
										{text = "General Loot", 		properties ={	{text = "Armor", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "armor", parent = "loot", addByKey = true, default = false},
																					{text = "Weapon", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "weapon", parent = "loot", addByKey = true, default = false},
																					{text = "Tool", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "tool", parent = "loot", addByKey = true, default = false},
																					{text = "Consumables", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "consumable", parent = "loot", addByKey = true, default = true}, -- defaults to checked
																					{text = "Ammunition", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "ammo", parent = "loot", addByKey = true, default = false},
																					{text = "Blueprint", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "blueprint", parent = "loot", addByKey = true},
																					{text = "Seeds", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "seed", parent = "loot", addByKey = true},
																					{text = "Generic Item", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "generic", parent = "loot", addByKey = true, default = false},																							
																				}
										},
										{text = "Custom Loot", 			properties ={	{propType = PROPERTY_TYPES.LOOT, attribute = "singles", parent = "loot", defaultDataStuct = {UNID=0, drop=0, quantity = 1},
																												propertyComponents = 	{	{text = "     Loot Item", propType = PROPERTY_TYPES.LOOT_ITEM, attribute = "UNID", parent = "lootTable", selectType = "UNID", number = true},
																																			{text = "        Drop Chance", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "drop", parent = "lootTable", width = 30, suffix = "%", number = true, min = 0, max = 100},
																																			{text = "        Quantity", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "quantity", parent = "lootTable", width = 70, number = true, min = 1, max = 100000}
																																		}
																				},
																					{text = "Add Loot", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "singles", maxCount = 10},
																			}
										},
										{text = "Reputation", properties ={		{text = "Enable Reputation", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repEnabled", parent = "behaviorParams", addByKey = true, default = false},
																			{text = "Reputation Scale", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repScale", parent = "behaviorParams"	, addByKey = true, displayValues = {"Easy", "Normal", "Hard"}, default = "Normal"},
																			{text = "Reputation Unlock Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repUnlock", parent = "behaviorParams"	, addByKey = true, displayValues = {"Acquaintance", "Friend", "Close Friend", "Best Friend", "Follower", "Spouse"}, default = "Friendly"},
																			{text = "Basic Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftBasic", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Standard Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftStandard", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Special Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpecial", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Follower Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftFollower", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Spouse Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpouse", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 404},
																			{text = "Gifting Interval", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "giftInterval", parent = "behaviorParams", addByKey = true, displayValues = {"None", "1/2 hour", "1 hour", "2 hours", "6 hours", "12 hours", "1 day", "1 week"}, trueValues = {0, 30, 60, 120, 360, 720, 1440, 10080}, number = true, default = 1440}, -- Time in minutes
																			{text = "Standard Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgGift", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Follower Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgFollow", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Spouse Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgSpouse", parent = "behaviorParams", selectType = "DIALOG", addByKey = true, default = ""},
																		}
										},
										{text = "Follower Properties", properties ={	{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																						{text = "Enable Combat", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "combatEnabled", parent = "behaviorParams", addByKey = true, default = false},
																						{text = "Allow Player Customization", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "customization", parent = "behaviorParams", addByKey = true, default = true}
																					}
										},
										{text = "Idle", properties ={	-- {text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 3931355}
																		{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246971},
																		{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Idle Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idlePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Walk/Wander", properties ={	{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 10},
																				{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																				{text = "Walk Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "walkAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246986},
																				{text = "Walk Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																				{text = "Walk Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																			}
										},
										{text = "Run", properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 25},
																		{text = "Run Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "runAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246983},
																		{text = "Run Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chaseSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Run Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chasePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Attack", properties ={		{text = "Attack Range", propType = PROPERTY_TYPES.TEXT, attribute = "attackRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet"},
																			{text = "Attack Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 3317748},
																			{text = "Attack Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Attack Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
										{text = "Death", properties ={		
																			{text = "Death Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4154955},
																			{text = "Death Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Death Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																	}
										},
									}
	PROPERTIES_PREFABS["timedVendor"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																				{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																				{text = "Display Name", propType = PROPERTY_TYPES.TEXT, attribute = "actorName", parent = "behaviorParams", addByKey = true, max = 21},
																				{text = "Title", propType = PROPERTY_TYPES.TEXT, attribute = "actorTitle", parent = "behaviorParams", addByKey = true, max = 21},
																				{text = "Show Display Name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showLabels", parent = "behaviorParams", addByKey = true, default = true},
																				{text = "Header", propType = PROPERTY_TYPES.TEXT, attribute = "header", parent = "behaviorParams", addByKey = true, max = 34},
																				{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 4246971},
																			}
									},
									{text = "Timed Trades", 			properties ={	{propType = PROPERTY_TYPES.TRADES, attribute = "trades", parent = "trades", defaultDataStuct = {input=0, output=0, outputCount=1, cost=1, time=15}, 
																										propertyComponents = 	{	{text = "     Item For Sale", propType = PROPERTY_TYPES.LOOT_ITEM, attribute = "output", selectType = "UNID", number = true},
																																	{text = "     Item For Sale Count", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "outputCount", width = 70, number = true, min = 1, default = 1},
																																	{text = "        Required Item", propType = PROPERTY_TYPES.LOOT_SUB_ITEM, attribute = "input", selectType = "UNID", number = true},
																																	{text = "        Required Item Count", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "cost", width = 70, number = true, min = 1},	
																																	{text = "        Processing Time", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "time", displayValues = {"0 minutes", "1 minute", "5 minutes","10 minutes","15 minutes","1/2 hour", "1 hour", "2 hours", "6 hours", "12 hours", "1 day", "1 week"}, trueValues = {0,1, 5, 10, 15, 30, 60, 120, 360, 720, 1440, 10080}, number = true}																																
																																}
																				},


																				{text = "Add Timed Trade", propType = PROPERTY_TYPES.ADD_ITEM, maxCount = 27, attribute = "trades"},
																}
									},
									{text = "Reputation", properties ={		{text = "Enable Reputation", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repEnabled", parent = "behaviorParams", addByKey = true, default = false},
																			{text = "Reputation Scale", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repScale", parent = "behaviorParams"	, addByKey = true, displayValues = {"Easy", "Normal", "Hard"}, default = "Normal"},
																			{text = "Reputation Unlock Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repUnlock", parent = "behaviorParams"	, addByKey = true, displayValues = {"Acquaintance", "Friend", "Close Friend", "Best Friend", "Follower", "Spouse"}, default = "Friendly"},
																			{text = "Basic Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftBasic", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Standard Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftStandard", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Special Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpecial", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Follower Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftFollower", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Spouse Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpouse", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 404},
																			{text = "Gifting Interval", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "giftInterval", parent = "behaviorParams", addByKey = true, displayValues = {"None", "1/2 hour", "1 hour", "2 hours", "6 hours", "12 hours", "1 day", "1 week"}, trueValues = {0, 30, 60, 120, 360, 720, 1440, 10080}, number = true, default = 1440}, -- Time in minutes
																			{text = "Standard Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgGift", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Follower Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgFollow", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Spouse Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgSpouse", parent = "behaviorParams", selectType = "DIALOG", addByKey = true, default = ""},
																		}
									},
									{text = "Follower Properties", properties ={	{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Enable Combat", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "combatEnabled", parent = "behaviorParams", addByKey = true, default = false},
																					{text = "Allow Player Customization", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "customization", parent = "behaviorParams", addByKey = true, default = true}
																				}
									},
									{text = "Idle", properties ={	-- {text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 3931355}
																	{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246971},
																	{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																	{text = "Idle Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idlePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																}
									},
									{text = "Walk/Wander", properties ={	{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 10},
																			{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																			{text = "Walk Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "walkAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246986},
																			{text = "Walk Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Walk Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																		}
									},
									{text = "Run", properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 25},
																	{text = "Run Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "runAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246983},
																	{text = "Run Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chaseSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																	{text = "Run Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chasePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																}
									},
									{text = "Attack", properties ={		{text = "Attack Range", propType = PROPERTY_TYPES.TEXT, attribute = "attackRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet"},
																		{text = "Attack Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 3317748},
																		{text = "Attack Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Attack Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																}
									},
									{text = "Death", properties ={		
																		{text = "Death Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4154955},
																		{text = "Death Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																		{text = "Death Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																}
									},
								}
	---HARVESTABLES---
	PROPERTIES_PREFABS["harvestable"] ={{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Required Tool", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "requiredTool", selectType = "UNID", typeFilter = "weapon", defaultSelect = "Any", parent = "behaviorParams", addByKey = true, number = true},
																				}
										},
										{text = "Loot", 			properties ={	{text = "Loot Container Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "spawnGLID", selectType = "OBJECT", parent = "behaviorParams", addByKey = true, number = true},
																					{text = "Item Dropped", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "UNID", selectType = "UNID", parent = "limitItems", addByKey = true, number = true},
																					{text = "Drop Limit", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "limit", displayValues = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, parent = "limitItems", addByKey = true, width = 70, number = true}
																				}
										}
									}

									
	PROPERTIES_PREFABS["seed"] = {	{	text = "Basic Properties", 
										properties = { 
											{	text		= "First Stage Mesh", 
												propType	= PROPERTY_TYPES.BUTTON_SELECT, 
												attribute	= "GLID", 
												selectType	= "OBJECT", 
												thumbnail	= true, 
												number		= true,
											},
											{	text		= "Required Item", 
												propType	= PROPERTY_TYPES.BUTTON_SELECT, 
												attribute	= "requiredItemUNID", 
												selectType	= "UNID", 
												typeFilter	= "generic", 
												number		= true, 
											},
											{	text		= "Required Quantity", 
												propType	= PROPERTY_TYPES.TEXT, 
												attribute	= "requiredQuantity", 
												width		= 70, 
												number		= true, 
												min			= 1,
											},
											{	text			= "Time to Next Stage", 
												propType		= PROPERTY_TYPES.TEXT, 
												attribute		= "requiredTime", 
												width			= 70,
												number			= true,
												min 			= 0,
												suffix 			= "minutes"
											},
											{	text			= "Level", 
												propType		= PROPERTY_TYPES.DROP_DOWN, 
												attribute		= "level", 
												displayValues	= {"1", "2", "3", "4", "5"}, 
												width			= 70, 
												number			= true,
												default 		= 30
											},
											{	text			= "Rarity", 
												propType		= PROPERTY_TYPES.DROP_DOWN, 
												attribute		= "rarity", 
												displayValues	= {"Common", "Rare", "Very Rare", "Extremely Rare"}
											}
										}
									},
									{	text = "Stages",
										properties = {	
											{	propType			= PROPERTY_TYPES.STAGES, 
												attribute			= "middleStages", 
												parent				= "middleStages", 
												defaultDataStuct	= {GLID = 4638294, requiredItemUNID = 267, requiredQuantity = 1, requiredTime = 30}, 
												propertyComponents	= {	
													{	text		= "     Stage Mesh", 
														propType	= PROPERTY_TYPES.DELETABLE_ITEM, 
														attribute	= "GLID", 
														parent		= "middleStages", 
														selectType	= "OBJECT",  
														thumbnail	= true, 
														number		= true
													},
													{	text		= "     Required Item", 
														propType	= PROPERTY_TYPES.BUTTON_SELECT, 
														attribute	= "requiredItemUNID", 
														parent		= "middleStages", 
														selectType	= "UNID", 
														typeFilter	= "generic", 
														number		= true, 
													},
													{	text		= "     Required Quantity", 
														propType	= PROPERTY_TYPES.TEXT, 
														attribute	= "requiredQuantity", 
														parent		= "middleStages", 
														width		= 70, 
														number		= true, 
														min			= 1,
													},
													{	text			= "     Time to Next Stage", 
														propType		= PROPERTY_TYPES.TEXT, 
														attribute		= "requiredTime",
														parent			= "middleStages", 
														width			= 70, 
														number			= true,
														min 			= 0,
														suffix 			= "minutes"
													}
												}
											},
											{	text		= "Add Stage", 
												propType	= PROPERTY_TYPES.ADD_ITEM, 
												attribute	= "middleStages",
												maxCount	= 5
											}
										}
									},
									{	text = "Final Stage", 		
										properties = {
											{	text		= "Final Stage Mesh", 
												propType	= PROPERTY_TYPES.BUTTON_SELECT, 
												attribute	= "finalGLID", 
												selectType	= "OBJECT",  
												thumbnail	= true,
												number		= true,
												default		= 4638395
											},
											{	text		= "Harvest Tool", 
												propType	= PROPERTY_TYPES.BUTTON_SELECT, 
												attribute	= "requiredTool", 
												selectType	= "UNID", 
												typeFilter	= "weapon", 
												number		= true, 
												default		= 0
											},
											{	text		= "Harvest Mesh", 
												propType	= PROPERTY_TYPES.BUTTON_SELECT, 
												attribute	= "harvestGLID", 
												selectType	= "OBJECT", 
												addByKey	= true, 
												number		= true
											}
										}
									},
									{	text = "Pick Loot",
										properties = {
											
											{	text		= "Override w/ Harvest Loot", 
												propType	= PROPERTY_TYPES.CHECK_BOX, 
												attribute	= "overridePickLoot",
												addByKey	= true,
												default		= false
											},
											{	text		= "Pick Loot Item", 
												propType	= PROPERTY_TYPES.BUTTON_SELECT, 
												attribute	= "pickLoot", 
												selectType	= "UNID", 
												addByKey	= true, 
												number		= true
											},
											{	text			= "Random Quantity", 
												propType		= PROPERTY_TYPES.DROP_DOWN, 
												attribute		= "pickLimit", 
												displayValues	= {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, 
												addByKey		= true, 
												width			= 70, 
												number			= true
											},
											{	text			= "Pick Cooldown", 
												propType		= PROPERTY_TYPES.TEXT, 
												attribute		= "pickCooldown", 
												width			= 70, 
												number			= true,
												min 			= 1,
												suffix          = "minutes",
												default 		= 30
											},
										}
									},
									{	text = "Harvest Random Loot",
										properties = {	
											{	text		= "Armor (1 slot)", 
												propType	= PROPERTY_TYPES.CHECK_BOX, 
												attribute	= "armor",
												parent		= "loot",
												addByKey	= true,
												default		= false
											},
											{	text		= "Weapon (1 slot)", 
												propType	= PROPERTY_TYPES.CHECK_BOX, 
												attribute	= "weapon",
												parent		= "loot",
												addByKey	= true,
												default		= false
											},
											{	text		= "Tool (1 slot)", 
												propType	= PROPERTY_TYPES.CHECK_BOX, 
												attribute	= "tool",
												parent		= "loot",
												addByKey	= true,
												default		= false
											},
											{	text		= "Consumables (2 slots)", 
												propType	= PROPERTY_TYPES.CHECK_BOX, 
												attribute	= "consumable",
												parent		= "loot",
												addByKey	= true,
												default		= false
											},
											{	text		= "Ammunition (1 slot)", 
												propType	= PROPERTY_TYPES.CHECK_BOX, 
												attribute	= "ammo",
												parent		= "loot",
												addByKey	= true,
												default		= false
											},
											{	text		= "Blueprint (1 slot)", 
												propType	= PROPERTY_TYPES.CHECK_BOX, 
												attribute	= "blueprint",
												parent		= "loot",
												addByKey	= true,
												default		= false
											},
											{	text		= "Generic Item (1 slot)", 
												propType	= PROPERTY_TYPES.CHECK_BOX, 
												attribute	= "generic",
												parent		= "loot",
												addByKey	= true,
												default		= false
											},
										}
									},
									{	text = "Harvest Custom Loot",
										properties = {	
											{	propType			= PROPERTY_TYPES.LOOT, 
												attribute			= "singles",
												parent				= "loot",
												defaultDataStuct	= {UNID = 0, limit = 1, drop = 10},
												propertyComponents	= {	
													{	text		= "        Loot Item", 
														propType	= PROPERTY_TYPES.LOOT_ITEM, 
														attribute	= "UNID", 
														parent		= "lootTable",
														selectType	= "UNID",
														number 		= true
													},
													{	text			= "        Loot Quantity", 
														propType		= PROPERTY_TYPES.DROP_DOWN, 
														attribute		= "limit", 
														parent			= "lootTable",
														displayValues	= {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, 
														trueValues		= {1, 2, 3, 4, 5, 6, 7, 8, 9, 10},  
														width			= 70, 
														number			= true
													},
													{	text		= "        Drop Chance", 
														propType	= PROPERTY_TYPES.LOOT_DROP, 
														attribute	= "drop",
														parent		= "lootTable",
														width		= 30,
														number		= true,
														min			= 0,
														max			= 100,
													}
												}
											},
											{	text		= "Add Loot (up to 10)", 
												propType	= PROPERTY_TYPES.ADD_ITEM, 
												attribute	= "singles", 
												maxCount	= 10
											},
										}
									}
								}
	---PLACEABLES---								
	PROPERTIES_PREFABS["placeable"] = {	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare"}},
																					{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																					{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1}
																				}
										}
									}
	
	---PLACEABLE LOOTS---								
	PROPERTIES_PREFABS["placeable_loot"] = { 	{	text		= "Basic Properties",
													properties	=	{	{	text		= "Visible Mesh", 
																			propType	= PROPERTY_TYPES.BUTTON_SELECT, 
																			attribute	= "GLID", 
																			selectType	= "OBJECT",  
																			thumbnail	= true, 
																			number		= true
																		},
																		{	text			= "Level", 
																			propType		= PROPERTY_TYPES.DROP_DOWN, 
																			attribute		= "level", 
																			displayValues	= {"1", "2", "3", "4", "5"}, 
																			width			= 70, 
																			number			= true
																		},
																		{	text			= "Rarity",
																			propType		= PROPERTY_TYPES.DROP_DOWN, 
																			attribute		= "rarity", 
																			displayValues	= 	{	"Common", 
																									"Rare", 
																									"Very Rare", 
																									"Extremely Rare"
																								}
																		},
																		{	text		= "Destructible", 
																			propType	= PROPERTY_TYPES.CHECK_BOX, 
																			attribute	= "targetable"
																		},
																		{	text		= "Health Multiplier", 
																			propType	= PROPERTY_TYPES.TEXT, 
																			attribute	= "healthMultiplier", 
																			width		= 70, 
																			number		= true, 
																			default		= 1, 
																			min			= 0.1
																		}
																	}
												},
												{	text		= "General Loot Properties", 	
													properties	=	{	{	text		= "Generates Loot", 
																			propType	= PROPERTY_TYPES.CHECK_BOX, 
																			attribute	= "generatesLoot",
																			default		= false,
																		},
																		
																		{	text		= "Interaction Radius", 
																			propType	= PROPERTY_TYPES.TEXT, 
																			attribute	= "range", 
																			width		= 70, 
																			number		= true, 
																			min			= 0, 
																			suffix		= "feet",
																			default		= 10,
																		},
																	}
												},
												{	text		= "Custom Loot Properties", 	
													properties	=	{	{	propType			= PROPERTY_TYPES.LOOT, 
																			attribute			= "singles",
																			defaultDataStuct	=	{	UNID = 366, 
																										maxDropCount = 100,
																										dropRate = 10,
																									},
																			propertyComponents	= 	{	{	text		= "     Loot Generated", 
																											propType	= PROPERTY_TYPES.LOOT_ITEM, 
																											attribute	= "UNID", 
																											selectType	= "UNID", 
																											number		= true
																										},
																										
																										{	text		= "        Loot Storage Cap", 
																											propType	= PROPERTY_TYPES.TEXT, 
																											attribute	= "maxDropCount", 
																											width		= 50, 
																											number		= true, 
																											min			= 1,
																											max			= 2000000,
																										},
																										
																										{	text		= "        Loot per Hour",
																											propType	= PROPERTY_TYPES.TEXT, 
																											attribute	= "dropRate",
																											width		= 50, 
																											number		= true, 
																											min			= 1, 
																											max			= 100000,
																										},
																									}
																		},
																		
																		{	text		= "Add Loot", 
																			propType	= PROPERTY_TYPES.ADD_ITEM, 
																			attribute	= "singles", 
																			maxCount	= 1
																		},
																	}
												}
											}	
									
	PROPERTIES_PREFABS["wheeled_vehicle"] = {	{text = "Basic Properties", 	properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																							{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																							{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare"}},
																							{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																							{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1},
																							{text = "Owner Locked", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "ownerLocked", parent = "behaviorParams", addByKey = true, default = true},
																							{text = "Show Driver", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showDriver", parent = "behaviorParams", addByKey = true, default = false},
																							{text = "Driver Animation", propType = PROPERTY_TYPES.BUTTON_SELECT_TEST, attribute = "driverAnim", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 0},
																							{text = "Interaction Range", propType = PROPERTY_TYPES.TEXT, attribute = "interactionRange", parent = "behaviorParams", addByKey = true, number = true, width = 70, min = 10, default = 25},
																							{text = "Enable Sound", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "enableSound", parent = "behaviorParams", addByKey = true, default = false},
																							{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "behaviorParams", addByKey = true, number = true, default = 4672657}
																						}
												},

												{text = "Vehicle Properties", 	properties =	{	{text = "X Offset", propType = PROPERTY_TYPES.TEXT, attribute = "offX", parent = "behaviorParams", addByKey = true, number = true, width = 70, suffix = "meters", default = 0}, -- Left/Right
																								{text = "Y Offset", propType = PROPERTY_TYPES.TEXT, attribute = "offY", parent = "behaviorParams", addByKey = true, number = true, width = 70, suffix = "meters", default = 0}, -- Up/Down
																								{text = "Z Offset", propType = PROPERTY_TYPES.TEXT, attribute = "offZ", parent = "behaviorParams", addByKey = true, number = true, width = 70, suffix = "meters", default = 0}, -- Forward/Back
																								{text = "Rotation", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "eulerRotY", parent = "behaviorParams", addByKey = true, displayValues = {"0?? (No rotation)", "45??", "90?? (1/4 Turn)", "135??", "180?? (1/2 Turn)", "225??", "270?? (3/4 Turn)", "305??"}, trueValues = {0, (math.pi/4), (math.pi/2), (3*math.pi/4), (math.pi), (5*math.pi/4), (3*math.pi/2), (7*math.pi/4)}, number = true},
																								{text = "Length", propType = PROPERTY_TYPES.TEXT, attribute = "dimLength", parent = "behaviorParams", addByKey = true, number = true, width = 70, suffix = "meters", default = 5},
																								{text = "Width", propType = PROPERTY_TYPES.TEXT, attribute = "dimWidth", parent = "behaviorParams", addByKey = true, number = true, width = 70, suffix = "meters", default = 2},
																								{text = "Height", propType = PROPERTY_TYPES.TEXT, attribute = "dimHeight", parent = "behaviorParams", addByKey = true, number = true, width = 70, suffix = "meters", default = 1.5},
																								
																								{text = "Top Speed", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "topSpeed", parent = "behaviorParams", addByKey = true, displayValues = {"45 MPH (72 KPH)", "60 MPH (97 KPH)", "90 MPH (145 KPH)", "120 MPH (193 KPH)", "150 MPH (241 KPH)", "180 MPH (290 KPH)", "200 MPH (322 KPH)", "220 MPH (354 KPH)", "250 MPH (402 KPH)", "270 MPH (435 KPH)"}, trueValues = {72, 97, 145, 193, 241, 290, 322, 354, 402, 435}, number = true},
																								
																								{text = "Horsepower", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "horsepower", parent = "behaviorParams", addByKey = true, displayValues = {"40", "80", "100", "120", "150", "200", "250", "300", "400", "500", "750", "1000", "1200"}, number = true},

																								{text = "Enable Superjump ('T')", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "superjumpEnabled", parent = "behaviorParams", addByKey = true, default = false},

																								{text = "Loaded Suspension Height", propType = PROPERTY_TYPES.TEXT, attribute = "loadedSusp", parent = "behaviorParams", addByKey = true, number = true, width = 70, max = 4, min = 0, suffix = "meters", default = .5}, -- How low it rides
																								{text = "Unloaded Suspension Height", propType = PROPERTY_TYPES.TEXT, attribute = "unloadedSusp", parent = "behaviorParams", addByKey = true, number = true, width = 70, max = 2, min = 0, suffix = "meters", default = .75}, -- How poorly the front sticks to the ground
																								{text = "Mass", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "mass", parent = "behaviorParams", addByKey = true, displayValues = {"1100 Lbs (500 Kg)", "1500 Lbs (680 Kg)", "2000 Lbs (907 Kg)", "2200 Lbs (1000 Kg)", "2500 Lbs (1134 Kg)", "2800 Lbs (1270 Kg)", "3000 Lbs (1360 Kg)", "3500 Lbs (1587 Kg)", "4000 Lbs (1814 Kg)", "5000 Lbs (2268 Kg)", "6000 Lbs (2722 Kg)", "7000 Lbs (3175 Kg)", "8000 Lbs (3628 Kg)", "10000 Lbs (4535 Kg)", "14000 Lbs (6530 Kg)", "18000 Lbs (8165 Kg)"}, trueValues = {500, 680, 907, 1000, 1134, 1270, 1360, 1587, 1814, 2268, 2722, 3175, 3628, 4535, 6530, 8165}, number = true},
																								{text = "Wheel Friction", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "wheelFriction", parent = "behaviorParams", addByKey = true, displayValues = {"0.5", "1.0", "1.5", "2.0", "2.5", "3.0", "3.5", "4.0", "4.5", "5.0", "5.5", "6.0", "6.5", "7.0", "7.5", "8.0", "8.5", "9.0", "9.5", "10.0", "10.5", "11.0", "11.5", "12.0", "12.5", "13.0", "13.5", "14.0", "14.5", "15.0"}, trueValues = {0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0, 10.5, 11.0, 11.5, 12.0, 12.5, 13.0, 13.5, 14.0, 14.5, 15.0}, number = true},
																								{text = "Unlimited Fuel", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "unlimitedFuel", parent = "behaviorParams", addByKey = true, default = false},
																								{text = "Fuel Capacity", propType = PROPERTY_TYPES.TEXT, attribute = "fuelTime", parent = "behaviorParams", addByKey = true, number = true, width = 70, suffix = "minutes", default = 20},
																								{text = "Fuel Type", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "fuelType", parent = "behaviorParams", addByKey = true, selectType = "UNID", typeFilter = "generic", number = true, default = 403},
																								{text = "Enable Wear and Tear", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "wearAndTear", parent = "behaviorParams", addByKey = true, default = false},
																								{text = "Wear Rate", propType = PROPERTY_TYPES.TEXT, attribute = "wearAndTearTime", parent = "behaviorParams", addByKey = true, number = true, width = 70, suffix = "minutes", default = 10}
																							}
												}
											}
	PROPERTIES_PREFABS["seat"] = 	{	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare"}},
																					{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																					{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1},
																					{text = "Energy Regeneration", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "regen", parent = "behaviorParams", addByKey = true, default = true},
																					{text = "Energy Regen. Time", propType = PROPERTY_TYPES.TEXT, attribute = "regenTime", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 1, max = 60, default = 2, suffix = "minutes"}
																				}
										},
										
										{text = "Player Positions", properties ={	{propType = PROPERTY_TYPES.NODES, attribute = "playerPositions", parent = "playerPositions", defaultDataStuct = {maleX=0, maleY=0, maleZ=0, maleRot=0, maleAnim=3720521, femaleX=0, femaleY=0, femaleZ=0, femaleRot=0, femaleAnim=3392791}, 
																						propertyComponents = 	{	{text = "     Male Animation", propType = PROPERTY_TYPES.DELETABLE_ITEM, attribute = "maleAnim", selectType = "ALL_ANIMATIONS", parent = "playerPositions", addByKey = true, number = true, default = 3720521},
																													{text = "         Male Offset X", propType = PROPERTY_TYPES.TEXT, attribute = "maleX", parent = "playerPositions", addByKey = true, number = true, width = 70, default = 0},
																													{text = "         Male Offset Y", propType = PROPERTY_TYPES.TEXT, attribute = "maleY", parent = "playerPositions", addByKey = true, number = true, width = 70, default = 0},
																													{text = "         Male Offset Z", propType = PROPERTY_TYPES.TEXT, attribute = "maleZ", parent = "playerPositions", addByKey = true, number = true, width = 70, default = 0},
																													{text = "         Male Rotation", propType = PROPERTY_TYPES.TEXT, attribute = "maleRot", parent = "playerPositions", addByKey = true, number = true, width = 70, min = 0, max = 360, default = 0},
																													{text = "     Female Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "femaleAnim", selectType = "ALL_ANIMATIONS", parent = "playerPositions", addByKey = true, number = true, default = 3392791},
																													{text = "         Female Offset X", propType = PROPERTY_TYPES.TEXT, attribute = "femaleX", parent = "playerPositions", addByKey = true, number = true, width = 70, default = 0},
																													{text = "         Female Offset Y", propType = PROPERTY_TYPES.TEXT, attribute = "femaleY", parent = "playerPositions", addByKey = true, number = true, width = 70, default = 0},
																													{text = "         Female Offset Z", propType = PROPERTY_TYPES.TEXT, attribute = "femaleZ", parent = "playerPositions", addByKey = true, number = true, width = 70, default = 0},
																													{text = "         Female Rotation", propType = PROPERTY_TYPES.TEXT, attribute = "femaleRot", parent = "playerPositions", addByKey = true, number = true, width = 70, min = 0, max = 360, default = 0},
																												}
																					},

																					{text = "Add Position", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "playerPositions", maxCount = 10}
																				}
										}
									}
	PROPERTIES_PREFABS["door"] = 	{	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare"}},
																					{text = "Locked", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "locked", parent = "behaviorParams", addByKey = true},
																					{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																					{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1}
																				}
										},
										{text = "Door Properties", 	properties ={	{text = "Door Open Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "openAnim", selectType = "OBJECT_ANIMATION", parent = "behaviorParams", addByKey = true, number = true},
																					{text = "Door Close Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "closeAnim", selectType = "OBJECT_ANIMATION", parent = "behaviorParams", addByKey = true, number = true},
																					{text = "Stay Open Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "openedAnim", selectType = "OBJECT_ANIMATION", parent = "behaviorParams", addByKey = true, number = true},
																					{text = "Door Animation Time", propType = PROPERTY_TYPES.TEXT, attribute = "animTime", parent = "behaviorParams", addByKey = true, width = 35, number = true}
																				}
										}
									}
	PROPERTIES_PREFABS["campfire"] = {	{text = "Basic Properties", 	properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																					{text = "Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "particle", selectType = "PARTICLE", parent = "behaviorParams", addByKey = true, number = true, default = 4465625},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare"}},
																					{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																					{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1}
																				}
										},
										{text = "Crafting Properties", 	properties ={	{text = "Crafting Resource Name", propType = PROPERTY_TYPES.TEXT, attribute = "craftingResource", parent = "behaviorParams", addByKey = true},
																						{text = "Proximity Distance", propType = PROPERTY_TYPES.TEXT, attribute = "craftingProximity", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet"}
																					}
										}
									}
	PROPERTIES_PREFABS["repair"] = {	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																					{text = "Display Name", propType = PROPERTY_TYPES.TEXT, attribute = "actorName", parent = "behaviorParams", addByKey = true, max = 21},
																					{text = "Title", propType = PROPERTY_TYPES.TEXT, attribute = "actorTitle", parent = "behaviorParams", addByKey = true, max = 21},
																					{text = "Show Display Name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showLabels", parent = "behaviorParams", addByKey = true, default = true},
																					{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 0},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare"}},
																					{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																					{text = "Repair Resource", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "repairUNID", selectType = "UNID", typeFilter = "ammo,consumable,generic", defaultSelect = "None", parent = "behaviorParams", addByKey = true, number = true},
																					{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1}
																				}
										}
									}
	PROPERTIES_PREFABS["media"] = {	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "MEDIAPLAYER",  thumbnail = true, number = true},
																					{text = "Display Name", propType = PROPERTY_TYPES.TEXT, attribute = "actorName", parent = "behaviorParams", addByKey = true, max = 21},
																					{text = "Show Display Name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showLabels", parent = "behaviorParams", addByKey = true, default = true},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare"}},
																					{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																					{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1},
																					{text = "Playlist", propType = PROPERTY_TYPES.PLAYLIST_SELECT, attribute = "playlist", selectType = "PLAYLIST", name = ""},
																					{text = "Radius", propType = PROPERTY_TYPES.TEXT, attribute = "radius", width = 70, number = true, default = 1, min = 1}

																				}
										}
									}
	PROPERTIES_PREFABS["loot"] = 	{	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																					{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																					{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare"}},
																					{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																					{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1},
																					{text = "Locked", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "locked", parent = "behaviorParams", addByKey = true}
																				}
										}
									}
	PROPERTIES_PREFABS["mover"] = 	{	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																					{text = "Speed", propType = PROPERTY_TYPES.TEXT, attribute = "speed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, default = 8},
																					{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																					{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1},
																					{text = "Pause Time", propType = PROPERTY_TYPES.TEXT, attribute = "stopDelay",parent = "behaviorParams", width = 70, number = true, default = 3, min = 1, max = 500, suffix = "seconds", addByKey = true}
																				}
										}
									}
	PROPERTIES_PREFABS["teleporter"] = 	{	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																						{text = "Idle Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleParticle", selectType = "PARTICLE", parent = "behaviorParams", addByKey = true, number = true},
																						{text = "Charge Up Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chargeParticle", selectType = "PARTICLE", parent = "behaviorParams", addByKey = true, number = true},
																						{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																						{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1},
																						{text = "Interaction Range", propType = PROPERTY_TYPES.TEXT, attribute = "triggerRadius",  parent = "behaviorParams", addByKey = true, width = 70, number = true, default = 2, min = 1, suffix = "feet"},
																						{text = "Charge Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chargeSoundID", selectType = "SOUND", parent = "behaviorParams", addByKey = true, number = true, default = 4379853},
																						{text = "Arrival Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "arriveSoundID", selectType = "SOUND", parent = "behaviorParams", addByKey = true, number = true, default = 4379854},
																						{text = "Instant Teleport", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "instantTeleport", parent = "behaviorParams", addByKey = true},
																					}
											}
										}
	PROPERTIES_PREFABS["worldTeleporter"] = 	{	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																						{text = "World Destination", propType = PROPERTY_TYPES.WORLD_SELECT, attribute = "worldDestination", parent = "behaviorParams", addByKey = true, selectType = "WORLD"},
																						{text = "Idle Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleParticle", selectType = "PARTICLE", parent = "behaviorParams", addByKey = true, number = true},
																						{text = "Charge Up Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chargeParticle", selectType = "PARTICLE", parent = "behaviorParams", addByKey = true, number = true},
																						{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																						{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1},
																						{text = "Interaction Range", propType = PROPERTY_TYPES.TEXT, attribute = "triggerRadius", parent = "behaviorParams", addByKey = true, width = 70, number = true, default = 2, min = 1, suffix = "feet"},
																						{text = "Charge Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chargeSoundID", parent = "behaviorParams", addByKey = true, selectType = "SOUND", number = true, default = 4379853}
																					}
											}
										}
	PROPERTIES_PREFABS["zoneTeleporter"] = 	{	{text = "Basic Properties", properties ={	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																						{text = "Destination Zone", propType = PROPERTY_TYPES.WORLD_SELECT, attribute = "zoneDestination", selectType = "ZONE"},
																						{text = "Instant Teleport", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "instantTeleport", parent = "behaviorParams", addByKey = true},
																						{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																						{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1},
																						{text = "Interaction Range", propType = PROPERTY_TYPES.TEXT, attribute = "triggerRadius", parent = "behaviorParams", addByKey = true, width = 70, number = true, default = 2, min = 1, suffix = "feet"},
																						{text = "Idle Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleParticle", selectType = "PARTICLE", parent = "behaviorParams", addByKey = true, number = true},
																						{text = "Charge Up Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chargeParticle", selectType = "PARTICLE", parent = "behaviorParams", addByKey = true, number = true},
																						{text = "Charge Up Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chargeSoundID", parent = "behaviorParams", addByKey = true, selectType = "SOUND", number = true, default = 4379853}
																					}
											}
										}
	PROPERTIES_PREFABS["trap"] = 	{	{text = "Basic Properties", properties =	{	{text = "Visible Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																						{text = "Rarity", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "rarity", displayValues = {"Common", "Rare", "Very Rare", "Extremely Rare", "Never"}},
																						{text = "Destructible", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "targetable"},
																						{text = "Health Multiplier", propType = PROPERTY_TYPES.TEXT, attribute = "healthMultiplier", width = 70, number = true, default = 1, min = 0.1},
																						{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																						{text = "% Damage per Second", propType = PROPERTY_TYPES.TEXT, attribute = "percentDamage", parent = "behaviorParams", addByKey = true, width = 30, number = true, min = 0, max = 100, default = 10}
																					}
										},
										{text = "Behavior", properties =			{	{text = "Idle Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleParticle", selectType = "PARTICLE", parent = "behaviorParams", addByKey = true, number = true},
																						{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "behaviorParams", addByKey = true, number = true},
																						{text = "Damage Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "damageSound", selectType = "SOUND", parent = "behaviorParams", addByKey = true, number = true}
																					}
										}
									}

	-- == SPAWNERS == --
	PROPERTIES_PREFABS["spawner_player"] = {}

	PROPERTIES_PREFABS["spawner_player_lobby"] = {}

	PROPERTIES_PREFABS["spawner_character"] = {	{text = "Basic Properties", 	properties ={	{text = "Spawn Radius", propType = PROPERTY_TYPES.TEXT, attribute = "spawnRadius", number = true, width = 50, min = 0, suffix = "feet"},
																								{text = "Respawn Time", propType = PROPERTY_TYPES.TEXT, attribute = "respawnTime", number = true, width = 50, min = 0, suffix = "seconds"},
																								{text = "Save Spawn Time", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "persistentSpawn", parent = "behaviorParams", addByKey = true, default = false}
																				}
												},
												{text = "Character Properties", properties ={	{text = "Character", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "spawnUNID", selectType = "UNID", typeFilter = "character", parent = "behaviorParams", addByKey = true, number = true},
																								{text = "Max Spawn", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "maxSpawns", parent = "behaviorParams", addByKey = true, displayValues = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, width = 70, number = true}
																				}
												}
											}
	PROPERTIES_PREFABS["spawner_loot"] = 	{	{text = "Basic Properties", 	properties ={	{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},				
																								{text = "Respawn Time", propType = PROPERTY_TYPES.TEXT, attribute = "respawnTime", width = 50, number = true, min = 0, suffix = "seconds"},
																								{text = "Save Spawn Time", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "persistentSpawn", parent = "behaviorParams", addByKey = true, default = false}
																				}
												},											
												{text = "General Loot", 		properties ={	{text = "Loot Container Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "spawnGLID", selectType = "OBJECT", number = true, default = 4468596},
																								{text = "Armor (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "armor", parent = "loot", addByKey = true},
																								{text = "Tool (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "tool", parent = "loot", addByKey = true},
																								{text = "Weapon (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "weapon", parent = "loot", addByKey = true},
																								{text = "Consumables (2 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "consumable", parent = "loot", addByKey = true},
																								{text = "Ammunition (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "ammo", parent = "loot", addByKey = true},
																								{text = "Blueprint (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "blueprint", parent = "loot", addByKey = true},
																								{text = "Seeds (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "seed", parent = "loot", addByKey = true},
																								{text = "Generic Item (1 slot)", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "generic", parent = "loot", addByKey = true},																								
																				}
												},
												{text = "Custom Loot", 	properties ={	{propType = PROPERTY_TYPES.LOOT, attribute = "singles", parent = "loot", defaultDataStuct = {UNID=0, drop=0}, 
																														propertyComponents = 	{	{text = "     Loot Item", propType = PROPERTY_TYPES.LOOT_ITEM, attribute = "UNID", parent = "lootTable", selectType = "UNID", number = true},
																																					{text = "        Drop Chance", propType = PROPERTY_TYPES.LOOT_DROP, attribute = "drop", parent = "lootTable", width = 30, suffix = "%", number = true, min = 0, max = 100},
																																				}
																								},


																								{text = "Add Loot", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "singles", maxCount = 10},
																				}
												}
												
											}
	PROPERTIES_PREFABS["spawner_loot_limit"] = {{text = "Basic Properties", 	properties ={	{text = "Spawn Radius", propType = PROPERTY_TYPES.TEXT, attribute = "spawnRadius", width = 50, number = true, min = 0, suffix = "feet"},
																								{text = "Respawn Time", propType = PROPERTY_TYPES.TEXT, attribute = "respawnTime", width = 50, number = true, min = 0, suffix = "seconds"},
																								{text = "Save Spawn Time", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "persistentSpawn", parent = "behaviorParams", addByKey = true, default = false},
																								{text = "Auto-Pickup Enabled", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "autoLoot", parent = "behaviorParams", addByKey = true, default = false}
																				}
												},
												{text = "Loot", 				properties ={	{text = "Loot Container Mesh", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "spawnGLID", selectType = "OBJECT", number = true, default = 4468596},
																								{text = "Item Dropped", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "UNID", selectType = "UNID", parent = "limitItems", addByKey = true, number = true},
																								{text = "Drop Limit", propType = PROPERTY_TYPES.TEXT, attribute = "limit", parent = "limitItems", addByKey = true, width = 70, number = true}
																				}
												}
											}

	PROPERTIES_PREFABS["spawner_harvestable"] ={{text = "Basic Properties", 	properties ={	{text = "Spawn Radius", propType = PROPERTY_TYPES.TEXT, attribute = "spawnRadius", width = 50, number = true, min = 0, suffix = "feet"},
																								{text = "Respawn Time", propType = PROPERTY_TYPES.TEXT, attribute = "respawnTime", width = 50, number = true, min = 0, suffix = "seconds"},
																								{text = "Save Spawn Time", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "persistentSpawn", parent = "behaviorParams", addByKey = true, default = false},
																								{text = "Spawn Object", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "spawnUNID", selectType = "UNID", typeFilter = "harvestable", parent = "behaviorParams", addByKey = true, number = true},
																								{text = "Max Spawn", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "maxSpawns", parent = "behaviorParams", addByKey = true, displayValues = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, width = 70, number = true}
																				}
												}
											}

	-- == GAME SETTINGS == --
	PROPERTIES_PREFABS["mainSettings"] = {	{text = "Basic Properties", properties ={	{text = "Privacy", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "privacy", displayValues = {"Closed (Private)", "Open", "Only Owners"}, trueValues = {"Private", "Public", "Owner Only"}, permissionRequired = "Owner"},
																						{text = "PvP", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "allowPvP"},
																						{text = "Destructible Objects", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "destructibleObjects"},
																						{text = "Player Build Mode", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "playerBuild"},
																						{text = "Player Names", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "playerNames"},
																						{text = "Player Jumpsuits", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "jumpsuit"},
																						{text = "Leaderboard", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "leaderboard"},
																						{text = "Drop Items on Death", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "dropItems"},
																					}
											},
											{text = "Energy Settings", properties ={	{text = "Death on Starvation", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "starvation"},
																						{text = "Energy Depletion Time", propType = PROPERTY_TYPES.TEXT, attribute = "starveTime", number = true, width = 70, min = 1, max = 999, suffix = "minutes"}
																					}
											},
											{text = "Spawn Settings", properties ={		{text = "Always Spawn at Lobby", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "lobbySpawnOnly"},
																						{text = "No Player Respawn Countdown", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "respawnTimerOff"}
																					}
											},
											{text = "Land Claim Flags", properties ={	{text = "Restrict Player Building to Flags", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "flagBuildOnly"},
																								{text = "Max Flags per Player", propType = PROPERTY_TYPES.TEXT, attribute = "maxFlags", number = true, width = 70, min = 1, max = 50, suffix = "flags", default = 3},
																								{text = "Max Building Objects per Flag", propType = PROPERTY_TYPES.TEXT, attribute = "maxFlagObjects", number = true, width = 70, min = 10, max = 5000, suffix = "objects", default = 500}
																							}
											}
										}

	-- == ENVIRONMENT SETTINGS == --       
    PROPERTIES_PREFABS["environmentSettings"] = {  	--[[{text = "Basic Properties", properties ={	{text = "Environment System", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "envEnabled"}
                                                                                            }
                                                    },]]
                                                    {text = "Fog Properties", properties = 	{	{text = "Fog", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "fogEnabled"},
                                                                                                {text = "Start Radius", propType = PROPERTY_TYPES.TEXT, attribute = "fogStartRange", number = true, width = 70, suffix = "feet"},
                                                                                                {text = "End Radius", propType = PROPERTY_TYPES.TEXT, attribute = "fogEndRange", number = true, width = 70, suffix = "feet"},
                                                                                                {text = "Color (RGB):", propType = PROPERTY_TYPES.COLOR_SELECT, attribute = "fogColor", selectType = "COLOR"}
                                                                                            }
                                                    },
                                                    {text = "Ambient Light Properties", properties ={	{text = "Color (RGB):", propType = PROPERTY_TYPES.COLOR_SELECT, attribute = "ambientColor", selectType = "COLOR"}
		                                                                                            }
                                                    },
                                                    {text = "Sun Properties", properties =	{	{text = "Color (RGB):", propType = PROPERTY_TYPES.COLOR_SELECT, attribute = "sunColor", selectType = "COLOR"}
                                                                                        	}
                                                    }
                                                }

    -- == WELCOME SETTINGS == --   
    PROPERTIES_PREFABS["welcomeSettings"] = {	{text = "Basic Properties", properties ={	{text = "Show Welcome Menu", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showMenu"},
																						{text = "Show world name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showWorldName"},
																						{text = "Edit Image", propType = PROPERTY_TYPES.IMAGE_SELECT, attribute = "welcomeImage", selectType = "IMAGE"},
																						{text = "Edit Text",propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "welcomeText", selectType = "DIALOG", addByKey = true, default = "Default text."},
																						{text = "", propType = PROPERTY_TYPES.PREVIEW, attribute = "preview"}
																					}
											}
										}

	-- == OVERVIEW SETTINGS == --
	PROPERTIES_PREFABS["overviewSettings"] = {	{text = "Basic Properties", properties ={	{text = "Use World Overview Image", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "overviewEnabled"},
																						{text = "Image", propType = PROPERTY_TYPES.IMAGE_SELECT, attribute = "overviewImage", selectType = "IMAGE"},
																						{text = "", propType = PROPERTY_TYPES.PREVIEW, attribute = "preview"}
																					}
											},
											{text = "Markers", 			properties ={	{propType = PROPERTY_TYPES.MARKER, attribute = "markers", parent = "markers", defaultDataStuct = {}, 
																										propertyComponents = 	{	{text = "      Marker", propType = PROPERTY_TYPES.EDIT_DYNAMIC, attribute = "markerProperties", selectType = "markerProperties"}
																																																															
																																}

																				},


																				{text = "Add New Marker", propType = PROPERTY_TYPES.ADD_ITEM, maxCount = 10, attribute = "markers"},
																}
											}
										}


	PROPERTIES_PREFABS["childZone"] = {	{text = "Basic Properties", properties ={	{text = "Privacy", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "privacy", displayValues = {"Closed (Private)", "Open", "Only Owners"}, trueValues = {"Private", "Public", "Owner Only"}, permissionRequired = "Owner"},
																						{text = "PvP", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "allowPvP"},
																						{text = "Destructible Objects", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "destructibleObjects"},
																						{text = "Player Build Mode", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "playerBuild"},
																						{text = "Player Names", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "playerNames"},
																						{text = "Player Jumpsuits", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "jumpsuit"},
																						{text = "Leaderboard", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "leaderboard"},
																						{text = "Drop Items on Death", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "dropItems"},
																					}
											},
											{text = "Energy Settings", properties ={	{text = "Death on Starvation", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "starvation"},
																						{text = "Energy Depletion Time", propType = PROPERTY_TYPES.TEXT, attribute = "starveTime", number = true, width = 70, min = 1, max = 999, suffix = "minutes"}
																					}
											},
											{text = "Spawn Settings", properties ={		{text = "Always Spawn at Lobby", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "lobbySpawnOnly"},
																						{text = "No Player Respawn Countdown", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "respawnTimerOff"}
																					}
											}
										}

	-- == FLAG == --
	PROPERTIES_PREFABS["flag"] = {	{text = "Basic Properties", properties ={	{text = "Height", propType = PROPERTY_TYPES.TEXT, attribute = "height", width = 35, number = true, parent = "behaviorParams", addByKey = true, suffix = "feet", min = 1},
																				{text = "Width", propType = PROPERTY_TYPES.TEXT, attribute = "width", width = 35, number = true, parent = "behaviorParams", addByKey = true, suffix = "feet", min = 1},
																				{text = "Depth", propType = PROPERTY_TYPES.TEXT, attribute = "depth", width = 35, number = true, parent = "behaviorParams", addByKey = true, suffix = "feet", min = 1},
																				-- {text = "Radius", propType = PROPERTY_TYPES.TEXT, attribute = "radius", width = 35, number = true, parent = "behaviorParams", addByKey = true,  suffix = "feet", min = 1},
																			}
									}
								}

	PROPERTIES_PREFABS["claim"] = {	{text = "Basic Properties", properties ={	{text = "No Destruction", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "noDestruct"}
																			}
									},
									{text = "Flag Area", properties ={	-- {text = "Shape", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "regionShape", parent = "behaviorParams", addByKey = true, displayValues = {"Rectangular", "Spherical", "Cylindrical"}, default = "Rectangular"},
																				{text = "Height", propType = PROPERTY_TYPES.TEXT, attribute = "height", width = 35, number = true, parent = "behaviorParams", addByKey = true, suffix = "feet", min = 1, max = 100, default = 50},
																				{text = "Width", propType = PROPERTY_TYPES.TEXT, attribute = "width", width = 35, number = true, parent = "behaviorParams", addByKey = true, suffix = "feet", min = 1, max = 100, default = 50},
																				{text = "Depth", propType = PROPERTY_TYPES.TEXT, attribute = "depth", width = 35, number = true, parent = "behaviorParams", addByKey = true, suffix = "feet", min = 1, max = 100, default = 50},
																				-- {text = "Radius", propType = PROPERTY_TYPES.TEXT, attribute = "radius", width = 35, number = true, parent = "behaviorParams", addByKey = true,  suffix = "feet", min = 1, max = 100},
																				-- {text = "Color", propType = PROPERTY_TYPES.COLOR_SELECT, attribute = "regionColor", selectType = "COLOR"},
																			}
									},
									{text = "Land Claim Flag Settings", properties ={
										{text = "Enable Maintenance Fees", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "feesEnabled", parent = "behaviorParams", addByKey = true, default = false},
										{text = "Maintenance Fee Interval", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "feeInterval", parent = "behaviorParams", addByKey = true, displayValues = {"1 Week", "2 Weeks", "3 Weeks", "1 Month", "2 Months"}, trueValues = {1,2,3,4,8}, width = 150, default = 2},
										{text = "Maintenance Fee Object", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "feeObject", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 140},
										{text = "Maintenance Fee Quantity", propType = PROPERTY_TYPES.TEXT, attribute = "feeCount", width = 35, number = true, parent = "behaviorParams", addByKey = true, min = 1, max = 1000000, default = 5000},
																					}
									}
								}

	-- == QUEST SYSTEM == --
	PROPERTIES_PREFABS["quest_giver"] = {	{text = "Basic Properties", properties ={	{text = "Character Mesh", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "meshType", displayValues = {"Custom Mesh", "Female Avatar", "Male Avatar"}, trueValues = {1,2,3}, width = 150, default = 1},
																				{text = "", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "meshGLID",selectType = "CHARACTER_MESH", addByKey = true, number = false, thumbnail = true},
																				{text = "Display Name", propType = PROPERTY_TYPES.TEXT, attribute = "actorName", parent = "behaviorParams", addByKey = true, max = 21},
																				{text = "Title", propType = PROPERTY_TYPES.TEXT, attribute = "actorTitle", parent = "behaviorParams", addByKey = true, max = 21},
																				{text = "Show Display Name", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "showLabels", parent = "behaviorParams", addByKey = true, default = true},
																				{text = "Greeting Text", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "dialog", selectType = "DIALOG", parent = "behaviorParams", addByKey = true},
																				{text = "Greeting Image", propType = PROPERTY_TYPES.IMAGE_SELECT, attribute = "greetingImage", parent = "behaviorParams", addByKey = true, selectType = "IMAGE"},
																				{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 4246971},
																			}
											},
											{text = "Quests", properties ={	{text = "Quest", propType = PROPERTY_TYPES.QUESTS, attribute = "quests", parent = "behaviorParams", selectType = "UNID", typeFilter = "quest", number = true},
																						{text = "Add Quest", propType = PROPERTY_TYPES.ADD_ITEM, attribute = "quests", maxCount = 10},
																					}
											},
											{text = "Reputation", properties ={		{text = "Enable Reputation", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repEnabled", parent = "behaviorParams", addByKey = true, default = false},
																			{text = "Reputation Scale", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repScale", parent = "behaviorParams"	, addByKey = true, displayValues = {"Easy", "Normal", "Hard"}, default = "Normal"},
																			{text = "Reputation Unlock Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "repUnlock", parent = "behaviorParams"	, addByKey = true, displayValues = {"Acquaintance", "Friend", "Close Friend", "Best Friend", "Follower", "Spouse"}, default = "Friendly"},
																			{text = "Basic Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftBasic", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Standard Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftStandard", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Special Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpecial", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Follower Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftFollower", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 0},
																			{text = "Spouse Gift", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "giftSpouse", parent = "behaviorParams", addByKey = true, selectType = "UNID", thumbnail = true, number = true, default = 404},
																			{text = "Gifting Interval", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "giftInterval", parent = "behaviorParams", addByKey = true, displayValues = {"None", "1/2 hour", "1 hour", "2 hours", "6 hours", "12 hours", "1 day", "1 week"}, trueValues = {0, 30, 60, 120, 360, 720, 1440, 10080}, number = true, default = 1440}, -- Time in minutes
																			{text = "Standard Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgGift", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Follower Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgFollow", selectType = "DIALOG", parent = "behaviorParams", addByKey = true, default = ""},
																			{text = "Spouse Message", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "msgSpouse", parent = "behaviorParams", selectType = "DIALOG", addByKey = true, default = ""},
																		}
											},
											{text = "Follower Properties", properties ={	{text = "Level", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "level", displayValues = {"1", "2", "3", "4", "5"}, width = 70, number = true},
																							{text = "Enable Combat", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "combatEnabled", parent = "behaviorParams", addByKey = true, default = false},
																							{text = "Allow Player Customization", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "customization", parent = "behaviorParams", addByKey = true, default = true}
																						}
											},
											{text = "Idle", properties ={	-- {text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "animation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true, default = 3931355}
																			{text = "Idle Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246971},
																			{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Idle Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idlePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																		}
											},
											{text = "Walk/Wander", properties ={	{text = "Walk Speed", propType = PROPERTY_TYPES.TEXT, attribute = "walkSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 10},
																					{text = "Wander Range", propType = PROPERTY_TYPES.TEXT, attribute = "wanderRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, suffix = "feet", default = 25},
																					{text = "Walk Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "walkAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246986},
																					{text = "Walk Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																					{text = "Walk Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "followPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																				}
											},
											{text = "Run", properties ={	{text = "Run Speed", propType = PROPERTY_TYPES.TEXT, attribute = "runSpeed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet per second", default = 25},
																			{text = "Run Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "runAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4246983},
																			{text = "Run Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chaseSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																			{text = "Run Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "chasePart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																		}
											},
											{text = "Attack", properties ={		{text = "Attack Range", propType = PROPERTY_TYPES.TEXT, attribute = "attackRange", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0, suffix = "feet"},
																				{text = "Attack Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 3317748},
																				{text = "Attack Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																				{text = "Attack Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "attackPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																		}
											},
											{text = "Death", properties ={		
																				{text = "Death Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathAnim", selectType = "ALL_ANIMATIONS", parent = "animations", addByKey = true, number = true, default = 4154955},
																				{text = "Death Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathSound", selectType = "SOUND", parent = "sounds", addByKey = true, number = true, default = 0},
																				{text = "Death Particle Effect", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "deathPart", selectType = "PARTICLE", parent = "particles", addByKey = true, number = true, default = 0},
																		}
											},
										}
	PROPERTIES_PREFABS["quest"] = 	{	{text = "Basic Properties", properties ={	{text = "Image", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
																					{text = "Quest Description", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "questPreText", selectType = "DIALOG", addByKey = true},
																					{text = "Goal", propType = PROPERTY_TYPES.EDIT, attribute = "questGoal", selectType = "questGoal"},
																					{text = "Completion Text", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "questPostText", selectType = "DIALOG", addByKey = true},
																					{text = "Repeatable", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repeatable"},
																					{text = "Repeat Timer", propType = PROPERTY_TYPES.TEXT, attribute = "repeatTime", addByKey = true, number = true, width = 70, suffix = "minutes", min = 0, max = 99999}, -- Time in minutes
																					{text = "Prerequisite Quest", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "prerequisiteQuest", defaultSelect = "None", selectType = "UNID", typeFilter = "quest", number = true},
																					{text = "Auto-Complete", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "autoComplete"},
																					{text = "Auto-Accept", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "autoAccept"},
																					{text = "Presentation Range", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "barkRange", number = true, displayValues = {"Face-to-Face", "Radius", "Infinite"}, trueValues = {0, 1, (-1)}, default = 0}
																					-- {text = "Auto-Present Range", propType = PROPERTY_TYPES.TEXT, attribute = "barkRange", number = true, width = 70, suffix = "feet", min = 0, max = 99999}
																				}
										},
										{text = "Reward Item", properties ={	{text = "Item", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "rewardUNID", selectType = "UNID", defaultSelect = "None", parent = "rewardItem", addByKey = true, number = true},
																				{text = "Quantity", propType = PROPERTY_TYPES.TEXT, attribute = "rewardCount", parent = "rewardItem", addByKey = true, number = true, min = 0, max = 99999}
																			}
										},
										-- {text = "Waypoint", properties ={	{text = "Waypoint", propType = PROPERTY_TYPES.WAYPOINT, attribute = "waypointPID"},
										-- 									{text = "Radius", propType = PROPERTY_TYPES.TEXT, attribute = "radius", parent = "waypointInfo", addByKey = true, number = true, width = 70, suffix = "feet"},
										-- 									{text = "Height", propType = PROPERTY_TYPES.TEXT, attribute = "height", parent = "waypointInfo", addByKey = true, number = true, width = 70, suffix = "feet"}
										-- 								}
										-- }
									}
	-- -- "quest_new" used for the first time creating a quest, before waypoints can be placed									
	-- PROPERTIES_PREFABS["quest_new"] = 	{	{text = "Basic Properties", properties ={	{text = "Image", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "GLID", selectType = "OBJECT",  thumbnail = true, number = true},
	-- 																				{text = "Quest Description", propType = PROPERTY_TYPES.TEXT, attribute = "questPreText"},
	-- 																				{text = "Completion Text", propType = PROPERTY_TYPES.TEXT, attribute = "questPostText"},
	-- 																				{text = "Repeatable", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "repeatable"},
	-- 																				{text = "Repeat Timer", propType = PROPERTY_TYPES.TEXT, attribute = "repeatTime", addByKey = true, number = true, width = 70, suffix = "minutes", min = 0, max = 99999}, -- Time in minutes
	-- 																				{text = "Prerequisite Quest", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "prerequisiteQuest", defaultSelect = "None", selectType = "UNID", typeFilter = "quest", number = true},
	-- 																				{text = "Auto-Complete", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "autoComplete"},
	-- 																				{text = "Auto-Accept", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "autoAccept"},
	-- 																				{text = "Presentation Range", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "barkRange", number = true, displayValues = {"Face-to-Face", "Radius", "Infinite"}, trueValues = {0, 1, (-1)}, default = 0}
	-- 																				-- {text = "Auto-Present Range", propType = PROPERTY_TYPES.TEXT, attribute = "barkRange", number = true, width = 70, suffix = "feet"}
	-- 																			}
	-- 									},
	-- 									{text = "Required Item", properties ={	{text = "Item", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "requiredUNID", selectType = "UNID", defaultSelect = "None", parent = "requiredItem", addByKey = true, number = true},
	-- 																			{text = "Quantity", propType = PROPERTY_TYPES.TEXT, attribute = "requiredCount", parent = "requiredItem", addByKey = true, number = true, min = 0, max = 99999},
	-- 																			{text = "Destroy On Completion", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "destroyOnCompletion"}
	-- 																		}
	-- 									},
	-- 									{text = "Reward Item", properties ={	{text = "Item", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "rewardUNID", selectType = "UNID", defaultSelect = "None", parent = "rewardItem", addByKey = true, number = true},
	-- 																			{text = "Quantity", propType = PROPERTY_TYPES.TEXT, attribute = "rewardCount", parent = "rewardItem", addByKey = true, number = true, min = 0, max = 99999}
	-- 																		}
	-- 									},
	-- 									{text = "Waypoint", properties ={	{text = "You must save before setting a quest waypoint.", propType = PROPERTY_TYPES.TITLE, attribute = "waypoint"}
	-- 																	}
	-- 									}
	-- 								}
									
	-- == PATHING SYSTEM == --
	PROPERTIES_PREFABS["path"] = 	{	{text = "Basic Properties", properties ={	{text = "Game Object", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "pathRider_parentUNID", selectType = "UNID", typeFilter = "character,placeable", behaviorFilter= "vendor,shop_vendor,actor,quest_giver,placeable_loot", defaultSelect = "None", parent = "behaviorParams", addByKey = true, number = true, thumbnail = true},
																					{text = "Speed", propType = PROPERTY_TYPES.TEXT, attribute = "speed", parent = "behaviorParams", addByKey = true, width = 35, number = true, min = 0.1, suffix = "feet per second", default = 8, number = true},
																					{text = "Path Type", propType = PROPERTY_TYPES.DROP_DOWN, attribute = "pathType", parent = "behaviorParams", addByKey = true, displayValues = {"Closed", "Open", "Reverse"}, trueValues = {1, 2, 3}, number = true},
																					{text = "Friction", propType = PROPERTY_TYPES.CHECK_BOX, attribute = "friction", parent = "behaviorParams", addByKey = true}
																				}
										},
										{text = "Move Properties", properties =	{	{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "moveAnimation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true},
																					{text = "Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "moveParticle", selectType = "PARTICLE", parent = "behaviorParams", addByKey = true, number = true},
																					{text = "Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "moveSound", selectType = "SOUND", parent = "behaviorParams", addByKey = true, number = true}
																			}
										},
										{text = "Idle Properties", properties =	{	{text = "Animation", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleAnimation", selectType = "ALL_ANIMATIONS", parent = "behaviorParams", addByKey = true, number = true},
																					{text = "Particle", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleParticle", selectType = "PARTICLE", parent = "behaviorParams", addByKey = true, number = true},
																					{text = "Idle Sound", propType = PROPERTY_TYPES.BUTTON_SELECT, attribute = "idleSound", selectType = "SOUND", parent = "behaviorParams", addByKey = true, number = true},
																				
																			}
										}
									}

-- Default item value for creating new items with the editor
NEW_ITEM_DEFAULTS = {}
--Loot
NEW_ITEM_DEFAULTS["weapon"] = {name = "New Weapon Item", itemType = "weapon", GLID = 4468633, rarity = "Rare", level = 1, GIGLID = 0, animationSet = "One-Handed Ranged", coneAngle = 30, bonusDamage = 1, rateOfFire = 1, range = 900, ammoType = 0, soundGLID = 4505666, soundRange = 80, description = "Give this new weapon a description!", healingWeapon = false, repairTool = false, durabilityType = "repairable"}
NEW_ITEM_DEFAULTS["tool"] = {name = "New Tool Item", itemType = "tool", GLID = 4612780, rarity = "Rare", level = 1, GIGLID = 0, animationSet = "Tool", coneAngle = 30, bonusDamage = 1, rateOfFire = 1, range = 900, ammoType = 0, soundGLID = 4505666, soundRange = 80, description = "Give this new tool a description!", healingWeapon = false, durabilityType = "limited", requiresTarget = "true"}
NEW_ITEM_DEFAULTS["paintbrush"] = {name = "New Paintbrush", itemType = "tool", GLID = 4612780, rarity = "Rare", level = 1, GIGLID = 0, animationSet = "Tool", coneAngle = 50, bonusDamage = 0, rateOfFire = 1, range = 10, description = "A paintbrush used to add textures.", healingWeapon = false, repairTool = false, paintWeapon = true, durabilityType = "limited", toolType = "paintbrush", requiresTarget = "true"}

NEW_ITEM_DEFAULTS["emoteItem"] = {name = "New Emote Item", itemType = "tool", GLID = 4619813, femaleGLIDS = {}, maleGLIDS = {}, rarity = "Rare", level = 1, GIGLID = 0, animationSet = "Tool", coneAngle = 30, bonusDamage = 1, rateOfFire = 5, range = 10, ammoType = 0, emoteM = 0, emoteF = 0, particleEffectsMine = {}, soundGLID = nil, soundRange = 0, description = "Give this new emote item a description!", durabilityType = "indestructible", toolType = "emoteItem", requiresTarget = "false"}
NEW_ITEM_DEFAULTS["p2pEmoteItem"] = {name = "New Two-Person Emote Item", itemType = "tool", GLID = 4619813, femaleGLIDS = {}, maleGLIDS = {}, rarity = "Rare", level = 1, range = 10, GIGLID = 0, animationSet = "Tool", coneAngle = 90, bonusDamage = 1, rateOfFire = 5, range = 10, ammoType = 0, emoteM = 0, emoteF = 0, emoteMTheirs = 0, emoteFTheirs = 0, particleEffectsMine = {}, particleEffectsTheirs = {}, soundGLID = nil, soundRange = 0, description = "Perform a 2-person emote with a friend!", durabilityType = "indestructible", toolType = "p2pEmoteItem", requiresTarget = "true"}

NEW_ITEM_DEFAULTS["armor"] = {name = "New Armor Item", itemType = "armor", GLID = 4471003, femaleGLIDS = {}, maleGLIDS = {}, rarity = "Rare", level = 1, GIGLID = 0, slotType = "head", bonusHealth = 3, description = "Give this new armor piece a description!", durabilityType = "repairable"}
NEW_ITEM_DEFAULTS["ammo"] = {name = "New Ammo Item", itemType = "ammo", GLID = 4465706, rarity = "Common", level = 1, GIGLID = 0, description = "Give this new ammo type a description!"}
NEW_ITEM_DEFAULTS["consumable"] = {name = "New Consumable Item", itemType = "consumable", GLID = 4465626, rarity = "Common", level = 1, GIGLID = 0, addGLIDS = {4470683,4470684}, consumeType = "energy", consumeValue = 25, description = "Give this new consumable type a description!", emoteM = 4695026, emoteF = 4695026, soundGLID = nil, effect = nil}
NEW_ITEM_DEFAULTS["generic"] = {name = "New Generic Item", itemType = "generic", GLID = 4470121, rarity = "Common", level = 1, GIGLID = 0, description = "Give this new generic type a description!"}

--Recipe
NEW_ITEM_DEFAULTS["recipe"] = {name = "New Recipe", itemType = "recipe", GLID = 4465706, GIGLID = 0, input1 = 124, count1 = 5, input2 = 0, count2 = 0, input3 = 0, count3 = 0, output = 81, description = "Give this new recipe a description!"}
NEW_ITEM_DEFAULTS["blueprint"] = {name = "New Blueprint", itemType = "blueprint", rarity = "Extremely Rare", oneUse = true, level = 1, GLID = 4495343, GIGLID = 0, input1 = 124, count1 = 5, input2 = 0, count2 = 0, input3 = 0, count3 = 0, output = 81, description = "Give this new blueprint a description!"}

--Placeables
NEW_ITEM_DEFAULTS["placeable"] = {name = "New Placeable", itemType = "placeable", GLID = 4464120, GIGLID = 0, rarity = "Common", targetable = "true", level = 3, description = "Give this new placeable item a description!"}
NEW_ITEM_DEFAULTS["placeable_loot"] = {name = "New Placeable Loot", itemType = "placeable", GLID = 4464120, GIGLID = 0, rarity = "Common", targetable = "true", level = 3, behavior = "placeable_loot", description = "Give this new placeable loot item a description!"}
NEW_ITEM_DEFAULTS["seat"] = {name = "New Seating Item", itemType = "placeable", GLID = 4254343, GIGLID = 0, rarity = "Common", targetable = "true", level = 3, behavior = "seat", playerPositions = {{maleX=0, maleY=2.93, maleZ=0, maleRot=90, maleAnim=3720521, femaleX=0, femaleY=2.93, femaleZ=0, femaleRot=90, femaleAnim=3392791}}, description = "Give this new seating item a description!"}
NEW_ITEM_DEFAULTS["wheeled_vehicle"] = {name = "New Car", itemType = "placeable", GLID = 4628745, GIGLID = 0, rarity = "Common", targetable = "true", level = 3, behavior = "wheeled_vehicle", eulerRotY = math.pi, wheelFriction = 2.5, description = "Give this new vehicle item a description!", fuelType = 403, fuelTime = 5, unlimitedFuel = false}
NEW_ITEM_DEFAULTS["campfire"] = {name = "New Crafting Helper", itemType = "placeable", GLID = 4465604, GIGLID = 0, rarity = "Common", targetable = "true", level = 3, behavior = "campfire", particle = 4465625, craftingResource = "Crafting Helper", craftingProximity = 25, description = "Give this new crafting helper a description!"}
NEW_ITEM_DEFAULTS["door"] = {name = "New Door", itemType = "placeable", GLID = 4464142, GIGLID = 0, rarity = "Common", targetable = "true", level = 3, behavior = "door", openAnim = 4468409, closeAnim = 4468410, openedAnim = 4468417, animTime = 1, description = "Give this new door a description!"}
NEW_ITEM_DEFAULTS["loot"] = {name = "New Loot Container", itemType = "placeable", GLID = 4468596, GIGLID = 0, rarity = "Common", targetable = "false", level = 3, behavior = "loot", description = "Give this new container a description!"}
NEW_ITEM_DEFAULTS["trap"] = {name = "New Trap", itemType = "placeable", GLID = 4548287, GIGLID = 0, targetable = "true", rarity = "Never", level = 3, behavior = "trap", description = "Give this new trap a description!"}
NEW_ITEM_DEFAULTS["repair"] = {name = "New Repair Station", itemType = "placeable", GLID = 4551263, GIGLID = 0, rarity = "Common", targetable = "false", level = 3, behavior = "repair", repairUNID = 124, actorName = "Repair Bench", description = "Give this new repair station a description!"}
NEW_ITEM_DEFAULTS["claim"] = {name = "New Land Claim Flag", itemType = "placeable", GLID = 4612626, GIGLID = 0, rarity = "Common", targetable = "false", level = 3, behavior = "claim", enabled = "true", description = "Allows Players to stake their own land claims upon which only they can build!", height = 50, depth = 50, width = 50, noDestruct = "false"}
NEW_ITEM_DEFAULTS["media"] = {name = "New Media Player", itemType = "placeable", GLID = 753, GIGLID = 0, rarity = "Common", targetable = "false", level = 3, behavior = "media", description = "Allows Players to play their own media!", radius = 50, playlist = ""}
NEW_ITEM_DEFAULTS["mover"] = {name = "New Platform System", itemType = "placeable", GLID = 4497863, GIGLID = 0, rarity = "Rare", targetable = "false", level = 3, behavior = "mover", description = "Create your own elevators and trams with this moving platform system." }
NEW_ITEM_DEFAULTS["teleporter"] = {name = "New Teleporter System", itemType = "placeable", GLID = 4496120, GIGLID = 0, rarity = "Rare", targetable = "false", level = 3, behavior = "teleporter", description = "Rip your atoms apart and assemble them in another place with this linked teleporter system.", idleParticle = 4527517, chargeParticle = 4496129 }
NEW_ITEM_DEFAULTS["worldTeleporter"] = {name = "New World to World Teleporter", itemType = "placeable", GLID = 4496120, GIGLID = 0, rarity = "Rare", targetable = "false", level = 3, behavior = "teleporter", description = "This Teleporter will take you to a whole new World!", idleParticle = 4527517, chargeParticle = 4496129, worldDestination = "" }
NEW_ITEM_DEFAULTS["zoneTeleporter"] = {name = "New Linked Zone Teleporter", itemType = "placeable", GLID = 4496120, GIGLID = 0, rarity = "Rare", targetable = "false", level = 3, behavior = "teleporter", description = "This Teleporter will take you to a Linked Zone!", idleParticle = 4527517, chargeParticle = 4496129, zoneDestination = ""}

--Spawners
NEW_ITEM_DEFAULTS["spawner_loot"] = {name = "New Loot Spawner", itemType = "spawner", GLID = 4465631, GIGLID = 0, rarity = "Common", level = 1, behavior = "spawner_loot", respawnTime = 120, spawnGLID = 4468596, spawnRadius = 1800, generic = false, armor = false, ammo = false, consumable = false, seed = false, weapon = false, description = "Give this new loot spawner a description!"}
NEW_ITEM_DEFAULTS["spawner_loot_limit"] = {name = "New Single Item Spawner", itemType = "spawner", GLID = 4465631, GIGLID = 0, rarity = "Common", level = 1, behavior = "spawner_loot", respawnTime = 120, spawnGLID = 4468596, spawnRadius = 1800, maxSpawns = 1, UNID = 81, limit = 1, description = "Give this new item spawner a description!"}
NEW_ITEM_DEFAULTS["spawner_character"] = {name = "New Character Spawner", itemType = "spawner", GLID = 4465632, GIGLID = 0, rarity = "Common", level = 1, behavior = "spawner_character", respawnTime = 300, spawnRadius = 1800, maxSpawns = 1, spawnUNID = 201, description = "Give this new loot character a description!"}
NEW_ITEM_DEFAULTS["spawner_harvestable"] = {name = "New Harvestable Spawner", itemType = "spawner", GLID = 4478234, GIGLID = 0, rarity = "Common", level = 1, behavior = "spawner_harvestable", respawnTime = 300, spawnRadius = 1800, maxSpawns = 1, spawnUNID = 261, description = "Give this new harvestable spawner a description!"}
NEW_ITEM_DEFAULTS["spawner_player"] = {name = "Player Spawner", itemType = "spawner", GLID = 4465630, GIGLID = 0, rarity = "Common", level = 1, behavior = "spawner_player", description = "Give this new player spawner a description!"}
NEW_ITEM_DEFAULTS["spawner_player_lobby"] = {name = "New Lobby Spawner", itemType = "spawner", GLID = 4465630, GIGLID = 0, newPlayer = true, rarity = "Common", level = 1, behavior = "spawner_player", description = "Give this new default player spawner a description!"}

--Characters
NEW_ITEM_DEFAULTS["monster"] = {name = "New Monster", itemType = "character", meshGLID = 4195001, GLID = 4195001, GIGLID = 0, rarity = "Common", level = 1, behavior = "monster", healthMultiplier = 1, spawnGLID = 4468596, attackSound = 4522644, walkSpeed = 2, runSpeed = 5, attackRange = 3, wanderRange = 25, aggroRange = 40, idle = 4162553, moving = 4195012, attacking = 4350206, death = 4154955, generic = false, armor = false, ammo = false, consumable = false, weapon = false, seed = false, description = "Give this new monster item a description!"}
NEW_ITEM_DEFAULTS["animal"] = {name = "New Animal", itemType = "character", meshGLID = 4478309, GLID = 4478309, GIGLID = 0, rarity = "Common", level = 1, behavior = "animal", limit = 5, idle = 0, moving = 4478310, walkSpeed = 10, runSpeed = 25, spawnGLID = 4468596, description = "Give this new animal a description!"}

NEW_ITEM_DEFAULTS["actor"] = {name = "New NPC", itemType = "character", meshGLID = 4286277, GLID = 4286277, GIGLID = 0, rarity = "Common", level = 1, behavior = "actor", actorName = "New NPC", actorTitle = "Default", animation = 4246971, idleAnim = 4246971, walkAnim = 4246986, runAnim = 4246983, attackAnim = 3317748, deathAnim = 4154955, dialogPages = {}, idle = 0, description = "Give this new NPC a description!", walkSpeed = 10, runSpeed = 25, attackRange = 5, wanderRange = 25}
NEW_ITEM_DEFAULTS["vendor"] = {name = "New Vendor", itemType = "character", meshGLID = 4534611, GLID = 4534611, rarity = "Common", level = 1, GIGLID = 0, timed = false, behavior = "vendor", actorName = "New Vendor", actorTitle = "New Vendor", animation = 4246971, idleAnim = 4246971, walkAnim = 4246986, runAnim = 4246983, attackAnim = 3317748, deathAnim = 4154955, trades = {}, description = "A friendly loot vendor for selling items.", walkSpeed = 10, runSpeed = 25, attackRange = 5, wanderRange = 25}
NEW_ITEM_DEFAULTS["creditsVendor"] = {name = "New Credits/Rewards Vendor", itemType = "character", meshGLID = 4534611, GLID = 4534611, rarity = "Common", level = 1, GIGLID = 0, timed = false, behavior = "vendor", actorName = "New Credits/Rewards Vendor", actorTitle = "New Credits/Rewards Vendor", credits = true, animation = 4246971, idleAnim = 4246971, walkAnim = 4246986, runAnim = 4246983, attackAnim = 3317748, deathAnim = 4154955, trades = {}, description = "A friendly loot vendor for selling items for Credits or Rewards. You receive 70% commission for Credits, 10% commission for Rewards", walkSpeed = 10, runSpeed = 25, attackRange = 5, wanderRange = 25}
NEW_ITEM_DEFAULTS["randomVendor"] = {name = "New Random Loot Vendor", itemType = "character", meshGLID = 4600898, GLID = 4600898, rarity = "Common", level = 1, GIGLID = 0, behavior = "vendor", actorName = "Random Loot Vendor", actorTitle = "", random = true, idleAnim = 208, walkAnim = 4246986, runAnim = 4246983, attackAnim = 3317748, deathAnim = 4154955, description = "A loot vendor for selling a random item.", generic = false, armor = false, consumable = true, weapon = false, ammo = false, seed = false, input = 140, cost = 1, singles = {{UNID=140, drop=50, quantity = 1}, {UNID=140, drop=10, quantity = 10}, {UNID=140, drop=0.1, quantity = 100}}, walkSpeed = 10, runSpeed = 25, attackRange = 5, wanderRange = 25}
NEW_ITEM_DEFAULTS["gemVendor"] = {name = "New Gem Vendor", itemType = "character", meshGLID = 44534611, GLID = 4534611, rarity = "Common", level = 1, GIGLID = 0, timed = false, behavior = "vendor", actorName = "New Gem Vendor", actorTitle = "New Gem Vendor", gem = true, animation = 4246971, idleAnim = 4246971, walkAnim = 4246986, runAnim = 4246983, attackAnim = 3317748, deathAnim = 4154955, trades = {}, description = "A friendly loot vendor for selling items for Gems (10% Rewards commission for the world owner).", walkSpeed = 10, runSpeed = 25, attackRange = 5, wanderRange = 25}
NEW_ITEM_DEFAULTS["timedVendor"] = {name = "New Timed Vendor", itemType = "character", meshGLID = 4534611, GLID = 4534611, rarity = "Common", level = 1, GIGLID = 0, timed = true, lastVID = 0, behavior = "vendor", actorName = "New Timed Vendor", actorTitle = "New Timed Vendor", animation = 4246971, idleAnim = 4246971, walkAnim = 4246986, runAnim = 4246983, attackAnim = 3317748, deathAnim = 4154955, trades = {}, description = "A friendly timed loot vendor for selling items.", walkSpeed = 10, runSpeed = 25, attackRange = 5, wanderRange = 25}
NEW_ITEM_DEFAULTS["clothing"] = {name = "New Clothing Vendor", itemType = "character", meshGLID = 4534611, GLID = 4534611, rarity = "Common", level = 1, GIGLID = 0, timed = false, behavior = "shop_vendor", actorName = "New Clothing Vendor", actorTitle = "New Clothing Vendor", clothing = true, animation = 4246971, idleAnim = 4246971, walkAnim = 4246986, runAnim = 4246983, attackAnim = 3317748, deathAnim = 4154955, trades = {}, description = "A friendly loot vendor for selling clothing.", walkSpeed = 10, runSpeed = 25, attackRange = 5, wanderRange = 25}

NEW_ITEM_DEFAULTS["pet"] = {name = "New Pet", itemType = "character", meshGLID = 4375990, GLID = 4375990, GIGLID = 0, rarity = "Common", stackSize = 1, level = 1, behavior = "pet", walkSpeed = 10, runSpeed = 25, attackRange = 5, wanderRange = 25, respawnCooldown = 30, spawnGLID = 4468596, runAnim = 4376013, attackAnim = 4552787, walkAnim = 4378086, idleAnim = 4375993, attackAnim = 4552787, deathAnim = 4552785, description = "Give this new pet a description!"}

--Harvestable
NEW_ITEM_DEFAULTS["harvestable"] = {name = "New Harvestable", itemType = "harvestable", GLID = 4476914, GIGLID = 0, rarity = "Common", level = 1, behavior = "harvestable", spawnGLID = 4470121, limit = 5, description = "Give this new harvestable a description!"}
NEW_ITEM_DEFAULTS["seed"] = {name = "New Seed", itemType = "harvestable", GLID = 4638292, GIGLID = 0, rarity = "Common", level = 1, behavior = "seed", middleStages = {{GLID = 4638294, requiredTime = 30, requiredQuantity = 1, requiredItemUNID = 267}}, requiredItemUNID = 267, requiredTime = 30, requiredQuantity = 1, finalGLID = 4638305, pickLoot = 269, pickLimit = 5, pickCooldown = 15, pickGLID = 4636816, requiredTool = 125, harvestGLID = 4636816, singles = {{UNID=268, limit=5, drop = 100},{UNID=269, limit=5, drop = 100},{UNID=264, limit=5, drop = 100}}, description = "Give this new seed a description!"}

--Quests
NEW_ITEM_DEFAULTS["quest_giver"] = {name = "New Quest Giver", itemType = "character", meshGLID = 4310445, GLID = 4310445, rarity = "Common", level = 1, behavior = "quest_giver", actorName = "The New Quest Giver", actorTitle = "Quest Giver Supreme", dialog = "Check out these quests!", animation = 4246971, idleAnim = 4246971, walkAnim = 4246986, runAnim = 4246983, attackAnim = 3317748, deathAnim = 4154955, quests = {}, description = "The bringer of adventure!"}
NEW_ITEM_DEFAULTS["quest"] = {name = "New Quest", itemType = "quest", level = 1, GLID = 4503530, description = "Example quest.", questPreText = "This is an example quest", questPostText = "You completed the quest. Now go away.", prerequisiteQuest = "0", requiredUNID = "0", requiredCount = 0, rewardUNID = "0", rewardCount = 0, repeatable = false, destroyOnCompletion = true, repeatTime = 30, waypointPID = 0, height = 10, radius = 10, autoComplete = false, autoAccept = false, barkRange = 0}

--Pathing System
NEW_ITEM_DEFAULTS["path"] = {name = "New Path", itemType = "path", GLID = 4276230, GIGLID = 0, behavior = "path", level = 1, rarity = "Common", description = "Give this new path a description!", speed = 8, pathRider_parentUNID = 53}


--The game item is passed through this function to make sure that everything is in the correct format to be saved to the DB
function formatGameItem(item)
	--log("--- item.itemType: ".. tostring(item.itemType))
	local compiledItem = {}

	if item.itemType == "mainSettings" or item.itemType == "environmentSettings" or item.itemType == "flag" or item.itemType == "welcomeSettings" or item.itemType == "overviewSettings" or item.itemType == "Zone" then
		return item
	end

	if item.regionColor then
		compiledItem.regionColor = item.regionColor
	end

	if item.itemType == "recipe" or item.itemType == "blueprint" then
	
		compiledItem.output = item.output

		compiledItem.inputs = item.inputs

		if item.proxReq then
			compiledItem.proxReq = item.proxReq
		end

		compiledItem.name = item.name
		compiledItem.description = item.description
		compiledItem.itemType = item.itemType
		compiledItem.invalid = false

		if item.itemType == "blueprint" then
			compiledItem.GLID = item.GLID
			compiledItem.level = item.level
			compiledItem.rarity = item.rarity
			compiledItem.oneUse = tostring(item.oneUse)
		end

	elseif item.itemType == "character" or item.itemType == "harvestable" then

		compiledItem.name = item.name
		compiledItem.description = item.description
		compiledItem.itemType = item.itemType
		compiledItem.rarity = item.rarity
		compiledItem.level = item.level
		compiledItem.GLID = item.GLID
		compiledItem.GIGLID = item.GIGLID
		compiledItem.behavior = item.behavior
		compiledItem.meshType = item.meshType
		compiledItem.lastGLID = item.lastGLID
		compiledItem.meshGLID = item.meshGLID

		if compiledItem.meshType == 1 then
			compiledItem.GLID = tonumber(item.meshGLID)
		end

		
		if item.combatEnabled ~= nil then
			compiledItem.combatEnabled = item.combatEnabled
		end
		
		if item.requiresFood then
			compiledItem.requiresFood = item.requiresFood
		end

		if item.food then 
			compiledItem.food = item.food
		end

		if item.stackSize then
			compiledItem.stackSize = item.stackSize
		end

		-- Recompile Class Parameters
		if item.behaviorParams then
			compiledItem.behaviorParams = {}
			for attribute,value in pairs(item.behaviorParams) do
				compiledItem.behaviorParams[attribute] = value
			end
			-- Recompile Class animations
			if item.animations then
				compiledItem.behaviorParams.animations = item.animations
			end
			
			if item.sounds then
				compiledItem.behaviorParams.sounds = item.sounds
			end

			if item.particles then
				compiledItem.behaviorParams.particles = item.particles
			end

			if item.quests then
				compiledItem.behaviorParams.quests = {}
				for i,v in pairs(item.quests) do
					compiledItem.behaviorParams.quests[i] = v
				end
			end

			if item.dialogPages then
				compiledItem.behaviorParams.dialogPages = {}
				for i,v in pairs(item.dialogPages) do
					compiledItem.behaviorParams.dialogPages[i] = v
				end
			end
		end

		if item.timed then
			compiledItem.behaviorParams.timed = true
			local tempVID = 0
			for i=#item.trades, 1, -1 do
				if item.trades[i].VID and item.trades[i].VID > tempVID then
					tempVID = item.trades[i].VID
				end
			end
			compiledItem.behaviorParams.lastVID = tempVID
		end

		if item.random then
			compiledItem.behaviorParams.random = true
		end

		if item.credits then
			compiledItem.behaviorParams.credits = true
		end

		if item.gem then
			compiledItem.behaviorParams.gem = true
		end

		if item.clothing then
			compiledItem.behaviorParams.clothing = true
		end

		-- Recompile Loot Tables
		if item.loot or item.singles or item.limitItems then
			item.loot = item.loot or {}
			compiledItem.loot = {}
			for attribute,value in pairs(item.loot) do
				compiledItem.loot[attribute] = value
			end

			if item.singles ~= nil then
				compiledItem.loot.singles = item.singles
			end

			if item.limitItems ~= nil then
				compiledItem.loot.limitItems = item.limitItems
			end
		end

		if item.trades then
			for i=#item.trades, 1, -1 do
				if item.clothing then
					if item.trades[i].output == nil or item.trades[i].output == 0 then
						table.remove(item.trades, i)
					end
				else
					if item.trades[i].output == nil or item.trades[i].output == 0 or item.trades[i].input == nil or item.trades[i].input == 0 or not tonumber(item.trades[i].cost) or tonumber(item.trades[i].cost) == 0 or i > 27 then
						table.remove(item.trades, i)
					elseif not tonumber(item.trades[i].outputCount) or tonumber(item.trades[i].outputCount) == 0 then
						item.trades[i].outputCount = 1
					elseif item.timed then
						item.trades[i].VID = compiledItem.behaviorParams.lastVID
						compiledItem.behaviorParams.lastVID = compiledItem.behaviorParams.lastVID + 1
					end
				end
			end
			compiledItem.trades = item.trades
		end

		if item.trade then
			if not tonumber(item.trade.cost) or tonumber(item.trade.cost) == 0 then
				item.trade.cost = 1
			end
			compiledItem.trade = item.trade
		end
		
		if item.behavior == "seed" then
	
			compiledItem.requiredItemUNID	= item.requiredItemUNID
			compiledItem.requiredTime		= item.requiredTime
			compiledItem.requiredQuantity	= item.requiredQuantity
			compiledItem.finalGLID			= item.finalGLID
			compiledItem.pickLoot			= item.pickLoot
			compiledItem.pickLimit			= item.pickLimit
			compiledItem.pickCooldown		= item.pickCooldown
			compiledItem.pickGLID			= item.pickGLID
			compiledItem.requiredTool		= item.requiredTool
			compiledItem.harvestGLID		= item.harvestGLID
			compiledItem.version			= 2		--> This is to differentiate from older versions of seed items
			compiledItem.overridePickLoot	= tostring(item.overridePickLoot)
			
			if item.middleStages then
				item.middleStages = item.middleStages or {}
				compiledItem.middleStages = {}
				for i, stage in pairs(item.middleStages) do
					if type(stage) == "table" then
						local newStage = {}
						for attribute, value in pairs(stage) do
							newStage[attribute] = value
						end
						compiledItem.middleStages[i] = newStage
					end
				end
			end
			
			compiledItem.loot = item.loot
			compiledItem.singles = item.singles
			compiledItem.loot.singles = item.singles  --> CJW, 04.26.2016: This is asinine, whoever designed the "singles" workflow needs a lecture on data duplication. I tried changing it but the related code is entrenched too deeply.

		end


	elseif item.itemType == "spawner" then

		compiledItem.name = item.name
		compiledItem.itemType = item.itemType
		compiledItem.rarity = item.rarity
		compiledItem.level = item.level
		compiledItem.GLID = item.GLID
		compiledItem.GIGLID = item.GIGLID
		compiledItem.spawnGLID = item.spawnGLID
		compiledItem.spawnRadius = item.spawnRadius
		compiledItem.respawnTime = item.respawnTime
		compiledItem.description = item.description
		compiledItem.behavior = item.behavior
		
		-- Recompile Class Parameters
		if item.behaviorParams then
			compiledItem.behaviorParams = {}
			for attribute,value in pairs(item.behaviorParams) do
				compiledItem.behaviorParams[attribute] = value
			end
		end

		-- Recompile Loot Tables for loot spawners
		if item.loot or item.singles or item.limitItems then
			if compiledItem.behaviorParams == nil then 
				compiledItem.behaviorParams = {}
			end
			compiledItem.behaviorParams.loot = {}

			if item.loot ~= nil then
				for attribute,value in pairs(item.loot) do
					compiledItem.behaviorParams.loot[attribute] = item.loot[attribute]
				end
			end

			if item.singles ~= nil then
				compiledItem.behaviorParams.loot.singles = item.singles
			end

			if item.limitItems ~= nil then				
				compiledItem.behaviorParams.loot.limitItems = item.limitItems
			end
		end
	elseif item.itemType == "placeable" then

		compiledItem.name = item.name
		compiledItem.itemType = item.itemType
		compiledItem.rarity = item.rarity
		compiledItem.level = item.level
		compiledItem.targetable = tostring(item.targetable)
		compiledItem.GLID = item.GLID
		compiledItem.GIGLID = item.GIGLID
		compiledItem.behavior = item.behavior
		compiledItem.description = item.description	
		compiledItem.height = item.height
		compiledItem.radius = item.radius
		compiledItem.enabled = item.enabled
		compiledItem.healthMultiplier = item.healthMultiplier
		compiledItem.playlist = item.playlist
		compiledItem.noDestruct = tostring(item.noDestruct)
		compiledItem.generatesLoot = tostring(item.generatesLoot)
		compiledItem.range = item.range
		compiledItem.zoneDestination = item.zoneDestination

		-- Recompile Class Parameters
		if item.behaviorParams then
			compiledItem.behaviorParams = {}
			for attribute,value in pairs(item.behaviorParams) do
				compiledItem.behaviorParams[attribute] = value
			end
		end
		
		if( item.singles ) then
			compiledItem.singles = item.singles
		end

		if item.behavior == "seat" then
			compiledItem.playerPositions = item.playerPositions
			if item.playerPositions then
				item.playerPositions = item.playerPositions or {}
				compiledItem.playerPositions = {}
				for attribute,value in pairs(item.playerPositions) do
					compiledItem.playerPositions[attribute] = value
				end
			end
		end

		if item.behavior == "claim" then
			compiledItem.regionColorRed = item.regionColorRed
			compiledItem.regionColorGreen = item.regionColorGreen
			compiledItem.regionColorBlue = item.regionColorBlue
		end

	elseif item.itemType == "quest" then

		compiledItem.name = item.name
		compiledItem.itemType = item.itemType
		compiledItem.level = item.level
		compiledItem.GLID = item.GLID
		compiledItem.GIGLID = item.GIGLID
		compiledItem.description = item.description
		compiledItem.questPreText = item.questPreText
		compiledItem.questPostText = item.questPostText
		compiledItem.prerequisiteQuest = tostring(item.prerequisiteQuest)
		compiledItem.repeatable = tostring(item.repeatable)
		compiledItem.destroyOnCompletion = tostring(item.destroyOnCompletion)
		compiledItem.repeatTime = item.repeatTime
		compiledItem.waypointPID = item.waypointPID
		compiledItem.pendingWaypoint = item.waypoint
		compiledItem.waypointInfo = {}

		compiledItem.questType = item.questType
		compiledItem.goalImage = item.goalImage

		--talk to/take to
		compiledItem.targetUNID = item.targetUNID
		--go to
		compiledItem.goToDescription = item.goToDescription

		compiledItem.barkRange = item.barkRange
		compiledItem.autoComplete = tostring(item.autoComplete)
		compiledItem.autoAccept = tostring(item.autoAccept)

		compiledItem.requiredItem = {}
		compiledItem.requiredItem.requiredUNID = tostring(item.requiredUNID or 0)
		if item.requiredUNID and tostring(item.requiredUNID) ~= "0" then
				if tonumber(item.requiredCount) > 0 then
					compiledItem.requiredItem.requiredCount = tonumber(item.requiredCount) or 1
				else
					compiledItem.requiredItem.requiredCount = 1
				end
		else
			compiledItem.requiredItem.requiredCount = 0
		end

		compiledItem.rewardItem = {}
		if item.rewardItem then
			compiledItem.rewardItem.rewardUNID = tostring(item.rewardItem.rewardUNID or 0)
			if item.rewardItem.rewardUNID and tostring(item.rewardItem.rewardUNID) ~= "0" then
				if item.rewardItem.rewardCount > 0 then
					compiledItem.rewardItem.rewardCount = tonumber(item.rewardItem.rewardCount) or 1
				else
					compiledItem.rewardItem.rewardCount = 1
				end
			else
				compiledItem.rewardItem.rewardCount = 0
			end
		else
			compiledItem.rewardItem.rewardUNID = "0"
			compiledItem.rewardItem.rewardCount = 0
		end

		-- Recompile waypoint parameters
		compiledItem.waypointInfo.height = item.height or 0
		compiledItem.waypointInfo.radius = item.radius or 0

	elseif item.itemType == "path" then
		compiledItem.name = item.name
		compiledItem.description = item.description
		compiledItem.itemType = item.itemType
		compiledItem.rarity = item.rarity
		compiledItem.level = item.level
		compiledItem.GLID = item.GLID
		compiledItem.GIGLID = item.GIGLID
		compiledItem.behavior = item.behavior
		if item.behaviorParams then
			compiledItem.behaviorParams = {}
			for attribute,value in pairs(item.behaviorParams) do
				compiledItem.behaviorParams[attribute] = value
			end
		end
	else -- else loot
		for attribute,value in pairs(item) do
			if NEW_ITEM_DEFAULTS[item.itemType][attribute] ~= nil then -- Nil values won't get initialized. If intentional, Handle as special cases below.
				compiledItem[attribute] = value
			end
		end
		if item.itemType == "weapon" then
			compiledItem.healingWeapon = tostring(item.healingWeapon)
			compiledItem.repairTool = tostring(item.repairTool)
			compiledItem.paintWeapon = tostring(item.paintWeapon)
			compiledItem.durabilityType = tostring(item.durabilityType)
			compiledItem.requiresTarget = tostring(item.requiresTarget)
			compiledItem.toolType = tostring(item.toolType)
		elseif item.itemType == "consumable" then
			compiledItem.emoteM = item.emoteM
			compiledItem.emoteF = item.emoteF
			compiledItem.soundGLID = item.soundGLID
			compiledItem.effect = item.effect
		elseif item.itemType == "tool" then
			compiledItem.paintWeapon = tostring(item.paintWeapon)
			compiledItem.toolType = tostring(item.toolType)
			compiledItem.durabilityType = tostring(item.durabilityType)
			compiledItem.requiresTarget = tostring(item.requiresTarget)
			compiledItem.emoteM = tostring(item.emoteM)
			compiledItem.emoteF = tostring(item.emoteF)
			compiledItem.emoteMTheirs = tostring(item.emoteMTheirs)
			compiledItem.emoteFTheirs = tostring(item.emoteFTheirs)
			if item.particleEffects then
				for i=#item.particleEffects, 1, -1 do
					if item.particleEffects[i].effect == nil or item.particleEffects[i].effect == 0 then
						table.remove(item.particleEffects, i)
					end
				end
				compiledItem.particleEffects = item.particleEffects
			end
			if item.particleEffectsMine then
				for i=#item.particleEffectsMine, 1, -1 do
					if item.particleEffectsMine[i].effect == nil or item.particleEffectsMine[i].effect == 0 then
						table.remove(item.particleEffectsMine, i)
					end
				end
				compiledItem.particleEffectsMine = item.particleEffectsMine
			end
			if item.particleEffectsTheirs then
				for i=#item.particleEffectsTheirs, 1, -1 do
					if item.particleEffectsTheirs[i].effect == nil or item.particleEffectsTheirs[i].effect == 0 then
						table.remove(item.particleEffectsTheirs, i)
					end
				end
				compiledItem.particleEffectsTheirs = item.particleEffectsTheirs
			end
			if item.maleGLIDS ~= nil then
				compiledItem.maleGLIDS = {}
				for index,value in pairs(item.maleGLIDS) do
					compiledItem.maleGLIDS[index] = item.maleGLIDS[index]
				end
			end
			if item.femaleGLIDS ~= nil then
				compiledItem.femaleGLIDS = {}
				for index,value in pairs(item.femaleGLIDS) do
					compiledItem.femaleGLIDS[index] = item.femaleGLIDS[index]
				end
			end
		elseif item.Type == "armor" then
			compiledItem.durabilityType = tostring(item.durabilityType)
		end
	end

	return compiledItem
end

function validateGameItem(item, UNID)
	if item.itemType == "recipe" or item.itemType == "blueprint" then
		local promptText
		if item.output <= 0 then -- If a recipe has an output
			promptText = "Please assign an output object."
		elseif item.inputs == nil or #item.inputs <= 0 then -- If it has any inputs
			promptText = "Please assign an input object"
		elseif item.inputs then -- If the inputs are valid
			for i=1,#item.inputs do
				if item.inputs[i].UNID > 0 then
					return true
				end
			end

			promptText = "Please assign an input object"
		end

		MenuOpenModal("Framework_GenericPrompt.xml")
		local promptEvent = KEP_EventCreate( "FRAMEWORK_GENERIC_PROMPT_INIT" )
		KEP_EventEncodeString(promptEvent, "ok")
		KEP_EventEncodeString(promptEvent, promptText)
		KEP_EventEncodeString(promptEvent, "Can't Save")
		KEP_EventQueue( promptEvent )

		return false
	end

	if item.itemType == "placeable" then
		local valuesToCheck = {}
		if item.radius then
			valuesToCheck.radius = item.radius
		end
		if item.behaviorParams and item.behaviorParams.height then
			valuesToCheck.height = item.behaviorParams.height
		end
		if item.behaviorParams and item.behaviorParams.width then
			valuesToCheck.width = item.behaviorParams.width
		end
		if item.behaviorParams and item.behaviorParams.depth then
			valuesToCheck.depth = item.behaviorParams.depth
		end
		if next(valuesToCheck) then
			validateAttribute(UNID, "flag area", valuesToCheck)
			return false
		end
	end

	return true
end

-- Function to be used if a certain attribute can't be changed once the object is place (example: radius)
function validateAttribute(UNID, attribute, attributeTable)
	Events.sendEvent("FRAMEWORK_VALIDATE_ATTRIBUTE", {UNID  = UNID, attribute = attribute, values = attributeTable})
end


-- CJW: This is called just before the GameItemEditor starts populating property widgets with the item content.
-- So, if you want to make adjustments to that item for backwards compatibility, do it here.
function enforceOldItemCompatibility(itemType, item)
	if itemType == "seed" then
		if not item.version then
			item.version = 1
		end
		
		if item.version < 2 then
			item.requiredItemUNID = item.food
			item.requiredTime = item.stage1Timer
			item.requiredQuantity = 1
			if( item.middleStages ) then
				for i, v in pairs(item.middleStages) do
					if type(v) == "table" then
						v.GLID = v.stageGLID
						v.stageGLID = nil
						v.requiredTime = v.stageTimer
						v.stageTimer = nil
						v.requiredQuantity = 1
						v.requiredItemUNID = item.food
					end
				end
			end
			
			item.food = nil
			item.stage1Timer = nil
			
			if( item.singles ) then
				for i, v in pairs(item.singles) do
					v.drop = 100
				end
			end
			
			if item.loot then
				item.loot.armor = false
				item.loot.weapon = false
				item.loot.tool = false
				item.loot.consumable = false
				item.loot.seed = false
				item.loot.ammo = false
				item.loot.blueprint = false
				item.loot.generic = false
			end
		end
	elseif itemType == "pet" then
		if item.combatEnabled == nil then
			item.combatEnabled = true
		end
	end
end