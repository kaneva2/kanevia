--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("MenuHelper.lua")
dofile("..\\MenuScripts\\DevToolsConstants.lua")

MENU_LOAD_EVENT = "MenuLoadEvent"
MENU_CREATE_EVENT = "MenuCreateEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"
SOURCE_ASSET_DOWNLOAD_EVENT = "3DAppAssetDownloadCompleteEvent"
DELETE_COMPLETE_EVENT = "3DAppAssetPublishDeleteCompleteEvent"

gMenuList = nil
gIsChildGame = 0
gLoadWOKMenus = false
gAllMenus = {}

gCurrentMenu = nil
gCurrentMenuName = nil

gChatMenuOpen = false

gEditDownloadedMenu = false

MENU_TITLE = "    Menu Browser"

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "yesNoAnswerHandler", YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AppAssetDownloadHandler", SOURCE_ASSET_DOWNLOAD_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "DeleteCompleteHandler", DELETE_COMPLETE_EVENT, KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( MENU_LOAD_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( MENU_CREATE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
end

function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate( dialogHandle )
	
	-- Close Chat Menu If Open
	g_chatMenuOpen = MenuIsOpen("ChatMenu.xml")
	if g_chatMenuOpen then
		MenuClose("ChatMenu.xml")
	end

	-- Load Menu Config
	local config = KEP_ConfigOpenWOK()
	if (config ~= nil) then
		local okfilter, tutPref = KEP_ConfigGetString( config, "WOKMenuEditor", "false" )
		if tutPref == "true" then
			gLoadWOKMenus = true
		else
			gLoadWOKMenus = false
		end
	end

	-- DRF - Allow WOK Menu Editing In Dev Mode
	if (ToBool(DevMode())) then
		gLoadWOKMenus = true
	end

	if KEP_IsWorld() then
		gIsChildGame = 1
	else
		gIsChildGame = 0
	end
	gMenuList = gHandles["menuList"]
	ListBox_SetHTMLEnabled(gMenuList, true)

	populateMenuTable()

	populateList(nil)

	local scrollBarHandle = ListBox_GetScrollBar(gMenuList)
	setupScrollBarDefaultTextures(scrollBarHandle)

	Dialog_SetCaptionText( gDialogHandle, MENU_TITLE )
end

-- DRF - Closes Currently Selected Menu
function MenuCloseCurrent()
	if gCurrentMenuName then
		MenuClose(gCurrentMenuName)
	end
end

-- DRF - Opens Currently Selected Menu
function MenuOpenCurrent(menuName)
	Log("MenuOpenCurrent: menuName="..menuName)

	-- Close Existing Current Menu
	MenuCloseCurrent()

	-- Open New Current Menu
	gCurrentMenu = Dialog_CreateAndEdit(menuName, true)
	gCurrentMenuName = menuName
	Dialog_SetModal(gCurrentMenu, false)
	Dialog_SetKeyboardInputEnabled(gCurrentMenu, false)
	Dialog_SetEdit(gCurrentMenu, false)

	-- Put the menu centered in the area to the left of the menu browser
	local sizeCur = MenuGetSize(gCurrentMenuName)
	local x = Dialog_GetLocationX(gDialogHandle) / 2
	x = x - (sizeCur.width / 2)
	local y = Dialog_GetScreenHeight(gDialogHandle) / 2
	y = y - (sizeCur.height / 2)
	MenuSetLocation(gCurrentMenuName, x, y)

	-- Finally Bring This To Front
	MenuBringToFrontThis()
end

function Dialog_OnDestroy(dialogHandle)

	-- Close Currently Selected Menu
	MenuCloseCurrent()	

	-- Re-Open Chat Menu If Not Editing Menu
	if g_chatMenuOpen and not MenuIsOpen("MenuEditor.xml") then
		MenuOpen("ChatMenu.xml")
	end

	Helper_Dialog_OnDestroy( dialogHandle )
end

function editSearch_OnEditBoxChange( editBoxHandle )
	local query = EditBox_GetText( editBoxHandle )
	populateList(query)
end

function menuList_OnListBoxSelection(listBoxHandle, x, y)
	if not KEP_IsWorld() then
		
		-- Update Current Selected Menu
		local index = ListBox_GetSelectedItemData(listBoxHandle)
		local name = getNameFromId(index)
		MenuOpenCurrent(name)
	else
		local index = ListBox_GetSelectedItemData( listBoxHandle )
		local menuName = getNameFromId(index)
		local baseName = string.sub(menuName, 1, string.len(menuName)-4)

		gEditDownloadedMenu = false
		KEP_RequestSourceAssetFromAppServer( DevToolsUpload.MENU_XML, baseName)
	end
end

function btnNext_OnButtonClicked(buttonHandle)
	local index = ListBox_GetSelectedItemIndex(gMenuList)
	local size = ListBox_GetSize(gMenuList)
	if index ~= -1 and index < size - 1 then
		index = index + 1
		ListBox_SelectItem(gMenuList, index)
	end
end

function btnPrevious_OnButtonClicked(buttonHandle)
	local index = ListBox_GetSelectedItemIndex(gMenuList)
	if index > 0 then
		index = index - 1
		ListBox_SelectItem(gMenuList, index)
	end
end

function btnEdit_OnButtonClicked(buttonHandle)
	local index = ListBox_GetSelectedItemData( gMenuList )
	local menuName = getNameFromId(index)
	local baseName = string.sub(menuName, 1, string.len(menuName)-4)
	if not KEP_IsWorld() then
		LoadMenu(menuName)
	else
		gEditDownloadedMenu = true
		KEP_RequestSourceAssetFromAppServer( DevToolsUpload.MENU_XML, baseName)
	end
end

function menuList_OnListBoxItemDblClick(listBoxHandle)
	local index = ListBox_GetSelectedItemData( listBoxHandle )
	local menuName = getNameFromId(index)
	local baseName = string.sub(menuName, 1, string.len(menuName)-4)
	gEditDownloadedMenu = true
	KEP_RequestSourceAssetFromAppServer( DevToolsUpload.MENU_XML, baseName)
end

function LoadMenu(menuName)
	if MenuIsClosed("MenuEditor.xml") or (gCurrentMenuName == "MenuEditor.xml") then
		MenuOpen("MenuEditor.xml")
	end

	local ev = KEP_EventCreate( MENU_LOAD_EVENT)
	KEP_EventEncodeString(ev, menuName)
	KEP_EventQueue( ev)

	MenuCloseThis()
end

function btnNew_OnButtonClicked(buttonHandle)
	if MenuIsClosed("MenuEditor.xml") or (gCurrentMenuName == "MenuEditor.xml") then
		MenuOpen("MenuEditor.xml")
	end

	KEP_EventCreateAndQueue(MENU_CREATE_EVENT)

	MenuCloseThis()
end

gFolderToDelete = ""
gFileToDelete = ""
gFolderToDelete2 = ""
gFileToDelete2 = ""

function btnDelete_OnButtonClicked(buttonHandle)
	if index == 0 then
		return
	end
	local index = ListBox_GetSelectedItemData(gMenuList)
	local s, e, path
	local menuName = getNameFromId(index)
	local filePath = KEP_GetAppMenuDir()
	s, e, path, gFolderToDelete = string.find(filePath, "(.-)([^\\/]-)[\\/]*$") -- captures "C:\Blah\" "Menus"
	gFileToDelete = menuName

	local xmlName = Dialog_GetScriptName(gCurrentMenu)
	local scriptPath = KEP_GetAppScriptDir()
	s, e, path, gFolderToDelete2 = string.find(scriptPath, "(.-)([^\\/]-)[\\/]*$") -- captures "C:\Blah\" "Menus"
	gFileToDelete2 = xmlName

	KEP_YesNoBox( "Are you sure you want to delete:\n\'"..menuName.."\'\nand\n\'"..xmlName.."\'?", "Confirm Delete", WF.MENUBROWSER_DELETE_FILTER)
end

function yesNoAnswerHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = tonumber(KEP_EventDecodeNumber(event))
	if filter == WF.MENUBROWSER_DELETE_FILTER then
		if answer == 1 then
			if gFolderToDelete ~= "" and gFileToDelete ~= "" and gFileToDelete2 ~= "" and gFolderToDelete2 ~= "" then
				-- Try to delete the .xml file
				local dotPos = string.find(gFileToDelete, "%.")
				local baseName = string.sub(gFileToDelete, 1, dotPos - 1)

				KEP_Delete3DAppAsset( baseName, DevToolsUpload.MENU_XML)
				KEP_Delete3DAppAsset( baseName, DevToolsUpload.MENU_SCRIPT)
			end
		end

		gFolderToDelete = ""
		gFileToDelete = ""
		gFolderToDelete2 = ""
		gFileToDelete2 = ""
	end
end

function populateMenuTable()
	gAllMenus = {}
	local numMenus = KEP_GetNumMenus(gIsChildGame)
	for i=0,numMenus-1 do
		local entry = {}
		entry["name"] = KEP_GetMenuByIndex( gIsChildGame, i)
		entry["path"] = "default"
		entry["type"] = DevToolsUpload.MENU_XML
		entry["modified"] = 0
		entry["id"] = i
		if (entry["name"] ~= nil and entry["name"] ~= "") then
			gAllMenus[entry["name"]] = entry
		end
	end
	local uniqueIds = numMenus
	local localDevCount = KEP_Count3DAppUnpublishedAssets(DevToolsUpload.MENU_XML)
	local unPubIndex
	for unPubIndex=1, localDevCount do
		entry = {}
		local name, fullpath, modified = KEP_Get3DAppUnpublishedAsset( DevToolsUpload.MENU_XML, unPubIndex)
		entry["name"] = name .. ".xml"
		entry["path"] = fullpath
		entry["type"] = DevToolsUpload.MENU_XML
		entry["modified"] = modified
		if gAllMenus[entry["name"]] ~= nil then
			entry["id"] = gAllMenus[entry["name"]]["id"]
		else
			entry["id"] = uniqueIds
			uniqueIds = uniqueIds + 1
		end
		gAllMenus[entry["name"]] = entry
	end
end

function getNameFromId(id)
	for name,entry in pairs(gAllMenus) do
		if entry["id"] == id then
			return entry["name"]
		end
	end
	LogError("getNameFromId: FAILED")
	return nil
end

function AppAssetDownloadHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local path = KEP_EventDecodeString(event)
	local fileType = KEP_EventDecodeNumber(event)

	if fileType==DevToolsUpload.MENU_XML then
		local menuName = string.gsub(path, ".*\\", "")
		if gEditDownloadedMenu then
			LoadMenu(menuName)
		else
			-- Update Current Selected Menu
			local index = ListBox_GetSelectedItemData(gMenuList)
			local name = getNameFromId(index)
			MenuOpenCurrent(name)
		end

		return KEP.EPR_CONSUMED
	end
end

function btnPublish_OnButtonClicked(buttonHandle)
	MenuOpen("DevToolsPublish.xml")
end

function btnScripts_OnButtonClicked(buttonHandle)
	MenuOpen("MenuScriptBrowser.xml")
end

function populateList(queryString)
	local t_displayArray = {}

	for name, entry in pairs(gAllMenus) do
		table.insert(t_displayArray, entry)
	end

	table.sort(t_displayArray, function(a, b) return string.lower(a["name"]) < string.lower(b["name"]) end)

	ListBox_RemoveAllItems(gMenuList)
	if queryString == nil or queryString == "" then
		for i,entry in ipairs(t_displayArray) do
			ListBox_AddItem(gMenuList, entry["name"], entry["id"])
		end
	else
		for i,entry in ipairs(t_displayArray) do
			local start = string.find(string.lower(entry["name"]), string.lower(queryString))
			if start ~= nil then
				local displayName = string.sub(entry["name"], 0, start - 1) .. "<font color=FFFF0000>" .. string.sub(entry["name"], start, start + string.len(queryString) - 1) .. "</font>" .. string.sub(entry["name"], start + string.len(queryString),-1)
				ListBox_AddItem(gMenuList, displayName, entry["id"])
			end
		end
	end
end

function DeleteCompleteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local name = KEP_EventDecodeString(event)
	local type = tonumber(KEP_EventDecodeNumber(event))
	local success = tonumber(KEP_EventDecodeNumber(event))
	local errstr = KEP_EventDecodeString(event)
	if success == 0 then
		KEP_MessageBox( errstr )
		MenuCloseThis()
	end

	KEP_ReloadMenus()

	populateMenuTable()

	local query = EditBox_GetText( gHandles["editSearch"] )
	populateList(query)
end
