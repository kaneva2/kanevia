--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-----------------------------------
-- Popup Menus
-----------------------------------
dofile("..\\Scripts\\ControlTypes.lua")
dofile("MenuAnimation.lua")

g_SubMenus = {}

DEFAULT_FADE_LENGTH = .25
MAX_ALPHA 		= 255

function PopupMenuEvent_PopupUpdated(state)
	-- Override in own script to use event for when popup is shown/hidden
	-- Helps in fading popups where you only want to do things that are fully visible
end

function PopupMenuEvent_FadeUpdated(alphaLevel, alphaLevelFast)
	-- Override in own script to use event for when fade is changing
end

-- Add to Dialog_OnRender
function PopupMenuHelper_OnRender(fTimeElapsed)
	MenuAnimation_OnRender(fTimeElapsed)
end

function PopupMenuHelper_InitPopupMenu(buttonHandle, arrayOfMenuContentsNames, imgButtonHighlightHandle, imgItemHighlightHandle, arrayOfMenuItemNames, fade, fadeLength, fadeLengthFast)
	local menuStruct = {}
	menuStruct.Name = arrayOfMenuContentsNames[1]
	menuStruct.Button = buttonHandle
	menuStruct.Handles = {}
	for i,v in ipairs(arrayOfMenuContentsNames) do
		table.insert(menuStruct.Handles, gHandles[v])
	end
	menuStruct.ButtonHighlight = imgButtonHighlightHandle
	menuStruct.ItemHighlight = imgItemHighlightHandle
	menuStruct.Items = {}
	if arrayOfMenuItemNames ~= nil then
		for i,v in ipairs(arrayOfMenuItemNames) do
			table.insert(menuStruct.Items, gHandles[v])
		end
	end
	menuStruct.PoppedUp = false

	menuStruct.Tweens = {}
	if fade then
		menuStruct.Tweens = PopupMenuHelper_SetupTweens(menuStruct.Name, menuStruct.Handles, fadeLength, fadeLengthFast)
	end

	table.insert(g_SubMenus, menuStruct)

	return menuStruct
end

function PopupMenuHelper_SetupTweens(name, handles, fadeLength, fadeLengthFast)
	tweens = {}

	-- If we have a fast fade, create 2 tweens
	if fadeLengthFast then
		-- First handle is background, give that the longer tween
		local fade1 = Tween(nil, handles[1])
		fade1:setHandleAlphas(0)
		fade1:fade(MAX_ALPHA, fadeLength or DEFAULT_FADE_LENGTH, 0, Easing.outQuad)
		fade1:addProperty("alpha", 1, 255, fadeLength or DEFAULT_FADE_LENGTH, 0, Easing.outQuad)
		fade1:addProperty("alphaFast", 1, 255, fadeLengthFast, fadeLength-fadeLengthFast, Easing.outQuad)
		table.insert(tweens, fade1)

		-- Add rest of the handles to the shorter one
		local tempHandles = {}
		for i = 2,#handles do
			table.insert(tempHandles, handles[i])
		end

		local fade2 = Tween(nil, tempHandles)
		fade2:setHandleAlphas(0)
		fade2:fade(MAX_ALPHA, fadeLengthFast, fadeLength-fadeLengthFast, Easing.outQuad)
		fade2:on(CALLBACK.UPDATE, PopupMenuHelper_FastUpdate)
		table.insert(tweens, fade2)
	else
		local fade = Tween(nil, handles)
		fade:setHandleAlphas(0)
		fade:fade(MAX_ALPHA, fadeLength or DEFAULT_FADE_LENGTH, 0, Easing.outQuad)
		fade:addProperty("alpha", 1, 255, fadeLength or DEFAULT_FADE_LENGTH, 0, Easing.outQuad)
		table.insert(tweens, fade)
	end

	-- Create on tween 1 since we know it exists and it's the longest
	tweens[1]:on(CALLBACK.STOP, PopupMenuHelper_FadeStopped)
	tweens[1]:on(CALLBACK.UPDATE, PopupMenuHelper_FadeUpdate)

	return tweens
end

-- Called when a fade stops
function PopupMenuHelper_FadeStopped(tween, time)
	-- If time is 0, the popup should be hidden
	if time <= 0 then
		-- the first handle is the name of the submenu
		local name = Control_GetName(tween:getHandles(HANDLE_TYPE.CONTROL)[1])
		for index, subMenu in pairs(g_SubMenus) do
			if name == subMenu.Name then 
				PopupMenuHelper_DoShowPopupMenu(g_SubMenus[index], false, false)
				break
			end
		end
	end

	-- If ending at end time, we know that the fade has finished fully opaque
	-- If time is 0, then we know it's hidden
	-- So send the PopupUpdated call based on whether it's visible or not
	PopupMenuEvent_PopupUpdated(time > 0)
end

function PopupMenuHelper_FadeUpdate(tween, time)
	local alphaLevel = tween:getPropertyValue("alpha")
	local alphaLevelFast = tween:getPropertyValue("alphaFast")

	-- Send and event so that implementing menus can use the alphaLevels to do extra functionality
	PopupMenuEvent_FadeUpdated(alphaLevel, alphaLevelFast or alphaLevel)
end

function PopupMenuHelper_FastUpdate(tween, time)
	if tween:getReversed() and time > 0 and tween:getPropertyValue("fade") <= 0 then
		for i, handle in pairs(tween:getHandles(HANDLE_TYPE.CONTROL)) do
			Control_SetVisible(handle, false)
		end
	end
end

function PopupMenuHelper_ShowPopupMenu(menuStruct, bShow, bAutoEnable)

	-- If fading, start that, otherwise go straight to show/hide
	if #menuStruct.Tweens > 0 then
		if bShow then
			PopupMenuHelper_DoShowPopupMenu(menuStruct, true, false)
		end

		-- Set to forward if trying to show, reverse if trying to hide
		for i, tween in pairs(menuStruct.Tweens) do
			tween:reverse(not bShow)
			tween:start()
		end
	else
		PopupMenuHelper_DoShowPopupMenu(menuStruct, bShow, bAutoEnable)
	end
end

function PopupMenuHelper_DoShowPopupMenu(menuStruct, bShow, bAutoEnable)

	for i,v in ipairs(menuStruct.Handles) do
		Control_SetVisible(v, bShow)
		if bAutoEnable ~= nil and bAutoEnable then
			Control_SetEnabled(v, bShow)
		end
	end

	-- TODO: Consider hiding all other menus when a new one is shown.
	if menuStruct.ButtonHighlight ~= nil then
		-- Move the highlight image to menuStruct.Button's position and width
		Control_SetVisible(menuStruct.ButtonHighlight, bShow)
		Control_SetEnabled(menuStruct.ButtonHighlight, bShow)
		if bShow then
			Control_SetLocation(menuStruct.ButtonHighlight, Control_GetLocationX(menuStruct.Button), Control_GetLocationY(menuStruct.Button) )
			Control_SetSize(menuStruct.ButtonHighlight, Control_GetWidth(menuStruct.Button), Control_GetHeight(menuStruct.ButtonHighlight) )
		end
	end

	if menuStruct.ItemHighlight ~= nil then
		if not bShow then
			Control_SetVisible(menuStruct.ItemHighlight, false)
			Control_SetEnabled(menuStruct.ItemHighlight, false)
		end
	end
	menuStruct.PoppedUp = bShow
end

function PopupMenuHelper_OnMouseMove(dialogHandle, x, y, bKeepOpenOnMove)
	for i,v in ipairs(g_SubMenus) do
		if Control_GetVisible(v.Handles[1]) == 1 then
			if (not bKeepOpenOnMove and Control_ContainsPoint(v.Button, x, y) == 0) and (Control_ContainsPoint(v.Handles[1], x, y) == 0) then
				-- neither the button nor menu background contains the point, so close it.
				PopupMenuHelper_ShowPopupMenu(v, false)
			elseif v.ItemHighlight ~= nil then
				Control_SetVisible(v.ItemHighlight, false)
				Control_SetEnabled(v.ItemHighlight, false)

				-- move the ItemHighlight to the item the cursor is over
				for j,k in ipairs(v.Items) do
					if Control_ContainsPoint(k, x, y) ~= 0 then
						Control_SetVisible(v.ItemHighlight, true)
						Control_SetEnabled(v.ItemHighlight, true)
						Control_SetSize(v.ItemHighlight, Control_GetWidth(k), Control_GetHeight(k))
						Control_SetLocation(v.ItemHighlight, Control_GetLocationX(k), Control_GetLocationY(k))
						break
					end
				end
			end
		end
	end
end

function PopupMenuHelper_AdditionalHighlightsOff(ihTable)
	for i,v in ipairs(ihTable) do
		Control_SetVisible(gHandles[v], false)
		Control_SetEnabled(gHandles[v], false)
	end
end

function PopupMenuHelper_AdditionalHighlightsMouseMove(ihTable, x, y)
	for i,v in ipairs(ihTable) do
		if (Control_ContainsPoint(gHandles[v], x, y) == 0) then
			Control_SetVisible(gHandles[v], false)
			Control_SetEnabled(gHandles[v], false)
		else
			Control_SetVisible(gHandles[v], true)
			Control_SetEnabled(gHandles[v], true)
		end
	end
end

function PopupMenuHelper_OnMouseClick(x, y)
	local popupsClosed = {}
	for i,v in ipairs(g_SubMenus) do
		if Control_GetVisible(v.Handles[1]) == 1 then
			if (Control_ContainsPoint(v.Button, x, y) == 0) and (Control_ContainsPoint(v.Handles[1], x, y) == 0) then
				-- neither the button nor menu background contains the point, so close it.
				PopupMenuHelper_ShowPopupMenu(v, false)
				table.insert(popupsClosed, v.Name)
			end
		end
	end
	return popupsClosed
end

function PopupMenuHelper_RemoveFromPopup(menuStruct, buttonHandle)
	local newHandleArray = {}
	for i,v in ipairs(menuStruct.Handles) do
		if v ~= buttonHandle then
			table.insert(newHandleArray, v)
		end
	end
	menuStruct.Handles = newHandleArray
end

function PopupMenuHelper_AddToPopup(menuStruct, buttonHandle)
	if not PopupMenuHelper_IsHandleInPopup(menuStruct, buttonHandle) then
		table.insert(menuStruct.Handles, buttonHandle)
	end
end

function PopupMenuHelper_IsHandleInPopup(menuStruct, buttonHandle)
	local handleFound = false
	for i,v in ipairs(menuStruct.Handles) do
		if v == buttonHandle then
			handleFound = true
		end
	end
	return handleFound
end

-- If the given x y location is within the popup menu specified, assuming first handle is the background
-- Use LOCAL SPACE (x,y start at top left of menu)
-- Returns false if clicked within popupmenu but it's not showing
function PopupMenuHelper_ContainsPoint(menuStruct, x, y)
	if ( (menuStruct.Button and Control_ContainsPoint(menuStruct.Button, x, y) == 1)) 
			or ((Control_GetVisible(menuStruct.Handles[1]) == 1 and Control_ContainsPoint(menuStruct.Handles[1], x, y) == 1)) then
		return true
	end
	return false
end