--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\Scripts\\GameGlobals.lua")

-- web call for getting the list, pass the user id
CONTEXTUAL_WEB_CALL = GameGlobals.WEB_SITE_PREFIX.. "kgp/fameInfo.aspx?action=GetContextualHelp&helpListId=7&userId="

-- web call for marking a tutorial step as complete, insert the user id and the packet id 
CONTEXTUAL_WEB_CALL_COMPLETE = GameGlobals.WEB_SITE_PREFIX.. "kgp/RedeemPacket.aspx?action=redeemPacket&fameTypeId=7&userId="
CONTEXTUAL_WEB_CALL_COMPLETE_2 = "&packetId="

-- holds the contextual help tips 
g_contextualHelp = {}
g_contextualHelpHistory = {}

-- holds the current contextual help shown
currentHelpID = 0

-- the user id
g_userID = -1 
g_username = ""

--------------------------------------------------------------------
-- Initializers
--------------------------------------------------------------------

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	-- get the user id to kickoff web call for api
	KEP_EventRegisterHandler( "newZoneLoadedHandler", "ZoneLoadedEvent", KEP.HIGH_PRIO )
	--KEP_EventRegisterHandler( "ClientSpawnEventHandler", "ClientSpawnEvent", KEP.HIGH_PRIO )

	-- read the contextual help 
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )

	-- show the contextual help menu
	KEP_EventRegisterHandler( "showContextualHelpHandler", "showContextualHelpEvent", KEP.HIGH_PRIO )

	-- hide the contextual help menu
	KEP_EventRegisterHandler( "hideContextualHelpHandler", "hideContextualHelpEvent", KEP.HIGH_PRIO )

	-- mark a help tip as complete 
	KEP_EventRegisterHandler( "contextualHelpCompleteHandler", "contextualHelpCompleteEvent", KEP.HIGH_PRIO )

	-- update the help tip positioning 
	KEP_EventRegisterHandler("updateContextualHelpMenuHandler", "updateContextualHelpEvent", KEP.HIGH_PRIO)

	KEP_EventRegisterHandler("forceContextualHelpCompleteHandler", "forceContextualHelpCompleteEvent", KEP.HIGH_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister("showContextualHelpEvent", KEP.MED_PRIO)
	KEP_EventRegister("hideContextualHelpEvent", KEP.MED_PRIO)
	KEP_EventRegister("contextualHelpCompleteEvent", KEP.MED_PRIO)
	KEP_EventRegister("openContextualMenuEvent", KEP.MED_PRIO)
	KEP_EventRegister("updateContextualMenuEvent", KEP.MED_PRIO)
	KEP_EventRegister("closeContextualMenuEvent", KEP.MED_PRIO)
	KEP_EventRegister("updateContextualHelpEvent", KEP.MED_PRIO)
	KEP_EventRegister("forceContextualHelpCompleteEvent", KEP.MED_PRIO)
end

--------------------------------------------------------------------
-- Event Handlers 
--------------------------------------------------------------------

-- make a webcall to the login name for the api
function newZoneLoadedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- make webcall to get the userID 
	g_username = KEP_GetLoginName()
	if not (g_username == "") and not (g_username == nil) then
		local web_address = GameGlobals.WEB_SITE_PREFIX.. LOGIN_INFO_SUFFIX.. g_username
		makeWebCall(web_address, WF.FAME_GETUSERID, 0, 0)
	end 
end

-- decode the api on return 
function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter == WF.FAME_GETUSERID) then
		local returnData = KEP_EventDecodeString(event)
		
		-- parse the user id
		ParseUserID(returnData)

		-- make a webcall to get the contextual helps
		if tablePopulated(g_contextualHelp) then return end
		makeWebCall(CONTEXTUAL_WEB_CALL.. tostring(g_userID), WF.CONTEXTUAL_HELP, 0, 0)
	elseif (filter == WF.CONTEXTUAL_HELP) then
		local returnData = KEP_EventDecodeString(event)

		-- parse the contextual help for the player 
		ParseContextualHelp(returnData)
	elseif (filter == WF.CONTEXTUAL_HELP_RESULT) then
		--local returnData = KEP_EventDecodeString(event)
	end
end

-- open a contextual help menu
function showContextualHelpHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- decode the event to get the help ID
	local eventParams = KEP_EventDecodeString(event)
	eventParams = Events.decode(eventParams)
	local helpID = tonumber(eventParams.type)

	-- might need something in here to make sure the event needs to pop up 
	--log("--- g_contextualHelp[".. tostring(helpID).. "]\t".. tostring(g_contextualHelp[helpID]))
	if (g_contextualHelp[helpID] and not tipComplete(helpID) and currentHelpID == 0) then
		log("--- showing: ".. g_contextualHelp[helpID].name)
		currentHelpID = helpID

		-- open the contextual help 
		MenuOpen("Framework_ContextualHelp.xml")

		-- send an event to the menu with all the information 
		local menuEvent = KEP_EventCreate("openContextualMenuEvent")
		eventParams.help = deepCopy(g_contextualHelp[helpID])
		KEP_EventEncodeString(menuEvent, Events.encode(eventParams))
		KEP_EventQueue(menuEvent)
	end 
end

-- updates the contextual help that is currently open 
function updateContextualHelpMenuHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- decode the event to get the help ID
	local eventParams = KEP_EventDecodeString(event)
	eventParams = Events.decode(eventParams)
	local helpID = tonumber(eventParams.type)

	-- only update the help that is currently open 
	if currentHelpID == helpID then
		-- send an event to the menu with all the information 
		local menuEvent = KEP_EventCreate("updateContextualMenuEvent")
		eventParams.help = deepCopy(g_contextualHelp[helpID])
		KEP_EventEncodeString(menuEvent, Events.encode(eventParams))
		KEP_EventQueue(menuEvent)
	end 
end

-- hide a contextual help menu, not completed 
function hideContextualHelpHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- decode the event to get the help ID
	local eventParams = KEP_EventDecodeString(event)
	eventParams = Events.decode(eventParams)
	local helpID = tonumber(eventParams.type)

	-- might need something in here to make sure the event needs to close
	if currentHelpID == helpID then
		currentHelpID = 0

		-- close the contextual help menu
		MenuClose("Framework_ContextualHelp.xml")

		local menuEvent = KEP_EventCreate("closeContextualMenuEvent")
		KEP_EventEncodeString(menuEvent, Events.encode(eventParams))
		KEP_EventQueue(menuEvent)
	end 
end

-- set the event as complete 
function contextualHelpCompleteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local eventParams = KEP_EventDecodeString(event)
	eventParams = Events.decode(eventParams)
	local helpID = tonumber(eventParams.type)

	if currentHelpID == helpID then
		g_contextualHelp[helpID].isComplete = true
		currentHelpID = 0

		-- close the contextual help menu
		MenuClose("Framework_ContextualHelp.xml")

		local menuEvent = KEP_EventCreate("closeContextualMenuEvent")
		KEP_EventEncodeString(menuEvent, Events.encode(eventParams))
		KEP_EventQueue(menuEvent)

		-- make the webcall to complete the event
		local packetId = g_contextualHelp[helpID].packetId
		local web_address = CONTEXTUAL_WEB_CALL_COMPLETE.. g_userID.. CONTEXTUAL_WEB_CALL_COMPLETE_2.. tostring(packetId)
		makeWebCall(web_address, WF.CONTEXTUAL_HELP_RESULT, 0, 0)
	end
end

-- set the event as complete 
function forceContextualHelpCompleteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local eventParams = KEP_EventDecodeString(event)
	eventParams = Events.decode(eventParams)
	local helpID = tonumber(eventParams.type)

	if g_contextualHelp[helpID].isComplete == false then
		g_contextualHelp[helpID].isComplete = true

		-- close the contextual help menu
		if currentHelpID == helpID then
			currentHelpID = 0

			MenuClose("Framework_ContextualHelp.xml")

			local menuEvent = KEP_EventCreate("closeContextualMenuEvent")
			KEP_EventEncodeString(menuEvent, Events.encode(eventParams))
			KEP_EventQueue(menuEvent)
		end

		-- make the webcall to complete the event
		local packetId = g_contextualHelp[helpID].packetId
		local web_address = CONTEXTUAL_WEB_CALL_COMPLETE.. g_userID.. CONTEXTUAL_WEB_CALL_COMPLETE_2.. tostring(packetId)
		makeWebCall(web_address, WF.CONTEXTUAL_HELP_RESULT, 0, 0)
	end
end

--------------------------------------------------------------------
-- Helper functions
--------------------------------------------------------------------

-- parse the user information to get the user id, taken from WFTracking 
function ParseUserID(returnData)
	s, e, result = string.find(returnData, "<ReturnCode>(%d+)</ReturnCode>")
	if result == "0" then
		s, e, kanevaUserId = string.find(returnData, "<kaneva_user_id>(.-)</kaneva_user_id>", e)
		g_userID = tonumber(kanevaUserId)
	end
end

-- parse the information from the contextual help webpage
function ParseContextualHelp(returnData)
	--log("--- ParseContextualHelp")

	local nudgeID = ""
	local name = ""
	local title = ""
	local description = ""
	local packetId = ""
	local rewards = ""
	local isComplete = ""
	local impressions = ""
	local status = ""

	-- parse through the list of contextual helps and get the properties of those incomplete 
	for itemData in string.gmatch(returnData, "<Nudge>(.-)</Nudge>") do
		if itemData then
			-- log("--- itemData ".. tostring(itemData))
			
			-- get the properties from the xml
			s, strPos, nudgeID = string.find(itemData, "<NudgeId>(.-)</NudgeId>")
			s, strPos, name = string.find(itemData, "<Name>(.-)</Name>")
			s, strPos, title = string.find(itemData, "<Title>(.-)</Title>")
			s, strPos, description = string.find(itemData, "<Desc>(.-)</Desc>")
			s, strPos, packetId = string.find(itemData, "<PacketId>(.-)</PacketId>")
			s, strPos, rewards = string.find(itemData, "<Rewards>(.-)</Rewards>")
			s, strPos, isComplete = string.find(itemData, "<IsComplete>(.-)</IsComplete>")
			s, strPos, impressions = string.find(itemData, "<Impressions>(.-)</Impressions>")
			s, strPos, status = string.find(itemData, "<Status>(.-)</Status>")

			-- set the properties in the table
			nudgeID = tonumber(nudgeID)
			g_contextualHelp[nudgeID] = {} 
			g_contextualHelp[nudgeID].name = name
			g_contextualHelp[nudgeID].title = title
			g_contextualHelp[nudgeID].description = description
			g_contextualHelp[nudgeID].packetId = packetId
			g_contextualHelp[nudgeID].rewards = rewards
			g_contextualHelp[nudgeID].isComplete = isComplete
			g_contextualHelp[nudgeID].impressions = impressions
			g_contextualHelp[nudgeID].status = status

			-- log("------------------- NUDGE ID ".. tostring(nudgeID))
			-- printTable(g_contextualHelp[nudgeID])
		end 
	end
end

-- returns if the help tip has been completed
function tipComplete(helpID)
	return (g_contextualHelp[helpID].isComplete == "true" or g_contextualHelp[helpID].isComplete == true)
end

-- print the table 
function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end 

function tablePopulated(testTable)
    return testTable ~= nil and next(testTable) ~= nil
end