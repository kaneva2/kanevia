--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\Scripts\\UIGlobals.lua")

-- Update this version number if you want to change defaults below
VERSION_NUMBER = 14

-- Sets of HUD Menus, default is what opens if does not belong to any other groups. Index other HUD menu sets by AB group id
-- Defaults stored by XML name
-- autoOpen = opens on client spawn
-- showHidden true when show despite hidden hud
HUD_MENUS = {}

HUD_MENUS.default = {	
	HUDKaneva		= {	x			= ALIGN.CENTER,
						y			= ALIGN.TOP,
						offsetY		= 0,
						autoOpen	= true,
						showHidden	= true},

	HUDBuild		= {	x 			= ALIGN.CENTER,
						y 			= ALIGN.BOTTOM,
						offsetY		= 0,
						autoOpen	= false}
}

-- Menus handled by the HUD but not part of the main bar
-- Set to true if modal

g_newChatMenu = true -- new in game chat -or- old KIM chat
if g_newChatMenu then
	EXTRA_MENUS = {ChatMenu = false, DevConsole = false, Tooltip = false, Framework_Indicators = false, MouseOverToolTip = false}
else
	EXTRA_MENUS = {DevConsole = false, Tooltip = false, Framework_Indicators = false, MouseOverToolTip = false}
end

local m_defaultsChecked = false 	-- Have the defaults been checked already this session?
local m_hudMinimized 	= 0 		-- 1 for hidden, 0 for show (params for Dialog_SetMinimized call)
local m_newVersion = false 			-- Verison changed so override

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "ClientSpawnHandler",			"ClientSpawnEvent",		KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "OpenHUDEventtHandler",		"OpenHUDEvent",			KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "CloseHUDEventHandler",		"CloseHUDEvent",		KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "hudMinimizeEventHandler",	"HudMinimizeEvent",		KEP.MED_PRIO  )
	KEP_EventRegisterHandler( "keyChangedEventHandler",		"KeyChangedEvent",		KEP.MED_PRIO )
	KEP_EventRegisterHandler( "hideHUDSettingEventHandler", "HideHUDSettingEvent",	KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "HudMinimizeEvent", 			KEP.MED_PRIO )
	KEP_EventRegister( "UpdateUILayoutEvent", 		KEP.MED_PRIO )
	KEP_EventRegister( "OpenHUDEvent",				KEP.MED_PRIO )
	KEP_EventRegister( "CloseHUDEvent",				KEP.MED_PRIO )
	KEP_EventRegister( "HUDClosedEvent",			KEP.MED_PRIO )
	KEP_EventRegister( "HUDHiddenEvent",			KEP.MED_PRIO )
	KEP_EventRegister( "HideHUDSettingEvent",		KEP.MED_PRIO )
	KEP_EventRegister( "HideToolbeltSettingEvent",	KEP.MED_PRIO )
	KEP_EventRegister( "HideMapIconSettingEvent",	KEP.MED_PRIO )
	KEP_EventRegister( "HideObjectGlowSettingEvent",KEP.MED_PRIO )
	KEP_EventRegister( "HideAFKStatusSettingEvent",	KEP.MED_PRIO )
end

-- On zone in
function ClientSpawnHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- Only check once per login to make sure all the defaults have been set for the menus
	if not m_defaultsChecked then
		setDefaults()
		m_defaultsChecked = true
	end

	openHUD()
end

function hideHUDSettingEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local checked = KEP_EventDecodeNumber(event)	
	local ev = KEP_EventCreate("HudMinimizeEvent")
	KEP_EventEncodeNumber(ev, checked)
	KEP_EventQueue(ev)
end

function hudMinimizeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	m_hudMinimized = KEP_EventDecodeNumber(event)
	if m_hudMinimized > 1 then m_hudMinimized = 1
	elseif m_hudMinimized < 0 then m_hudMinimized = 0 end

	-- Override the showHidden menus?
	local override = false
	if KEP_EventMoreToDecode(event) ~= 0 then
		override = KEP_EventDecodeNumber(event) == 1
	end

	setHUDVisible(override)
end

-- Handles keyboard events
function keyChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	isKeyDown = KEP_EventDecodeNumber(event)
	-- Minimize HUD keyboard shortcut
	if isKeyDown == 1 and filter == Keyboard.F11 then
		m_hudMinimized = (m_hudMinimized == 1) and 0 or 1 -- Reverse
		SetHideHUDSetting(m_hudMinimized)
	end
end

function OpenHUDEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	openHUD()
end

function CloseHUDEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	closeHUD()
end

-- Open the HUD menus and any extra related menus
function openHUD()
	local hudMenus = getHUDMenus()

	for menu, info in pairs(hudMenus) do
		if info.autoOpen then -- Only open autopen menus
			MenuOpen(menu)
		end
	end

	-- Let the UILayout know we want to update the layout now that all open
	-- Possibly to be removed if going to be handled by layout automatically when opening and closing menus
	local ev = KEP_EventCreate("UpdateUILayoutEvent")
	KEP_EventQueue(ev)

	-- Open all extra menus listed above, any menus that are not directly related to HUD but opened at the same time
	for menu, modal in pairs(EXTRA_MENUS) do
		if modal then
			MenuOpenModal(menu)
		else
			MenuOpen(menu)
		end
	end
end

-- Close all HUD menus that are auto opened
function closeHUD()
	local hudMenus = getHUDMenus()

	for menu, info in pairs(hudMenus) do
		if info.autoOpen then -- Only affects auto opened menus because the others are independent
			MenuClose(menu)
		end
	end

	local HUDClose = KEP_EventCreate( "HUDClosedEvent" )
	KEP_EventQueue( HUDClose )
end

-- Hide or show HUD through the minimize feature, unless it has a showHidden attribute
-- override: whether to ignore the showHidden settings (for times we want to hide EVERYTHING)
function setHUDVisible(override)
	local hudMenus = getHUDMenus()

	for menu, info in pairs(hudMenus) do
		if info.autoOpen then -- Only affects autoOpen main HUD menus
			if info.showHidden and not override and MenuHandle(menu) ~= nil then -- Always show depite hidden
				Dialog_SetMinimized(MenuHandle(menu), 0)
			elseif menu ~= "HUDKaneva" then
				-- Hud minimizes itself.
				Dialog_SetMinimized(MenuHandle(menu), m_hudMinimized)
			end
		end
	end

	local ev = KEP_EventCreate("HUDHiddenEvent")
	KEP_EventEncodeNumber(ev, m_hudMinimized)
	KEP_EventEncodeString(ev, tostring(override))
	KEP_EventQueue(ev)
end

-- Store the defaults in the UIConfig file. Should be moved along with related functions to its own file at some point for non-HUD menus to use
function setDefaults()
	-- TODO: Move to top when in own file
	-- UIConfig has XML tags that contain the name of the menu + location information
	-- Valid tags are X, Y, OffsetX, OffsetY, Parent, ParentX, ParentY
	-- Fields such as X/Y/ParentX/ParentY contain numbers or align constants from UIGlobals
	-- eg <MenuNameX>ALIGN.LEFT</MenuNameX>
	-- Also contains a list of all menus that utilize the UILayout feature seperated by spaces under <MenuList> tag
	local config = KEP_ConfigOpen("UIConfig.xml","UIConfig")
	if config == nil then
		return
	end

	local versionOK, version = KEP_ConfigGetNumber(config, "VersionNumber", 0)
	m_newVersion = version ~= VERSION_NUMBER

	-- Reset list of menus if new version
	if m_newVersion then
		Log("Resetting to UI locations to defaults. Updating version number " .. tostring(version) .. " to " .. tostring(VERSION_NUMBER))
		KEP_ConfigSetString(config, "MenuList", "")
	end

	local hudMenus = getHUDMenus()

	for menu, info in pairs(hudMenus) do
		addMenuToLayout(config, menu)

		setConfigStringIfEmpty(config, menu .. "X", info.x or "0")
		setConfigStringIfEmpty(config, menu .. "Y", info.y or "0")

		if info.offsetX then
			setConfigStringIfEmpty(config, menu .. "OffsetX", info.offsetX)
		end

		if info.offsetY then
			setConfigStringIfEmpty(config, menu .. "OffsetY", info.offsetY)
		end
		
		if info.parent then
			setConfigStringIfEmpty(config, menu .. "Parent", info.parent)
			setConfigStringIfEmpty(config, menu .. "ParentX", info.parentX or "0")
			setConfigStringIfEmpty(config, menu .. "ParentY", info.parentY or "0")
		end
	end

	KEP_ConfigSetNumber(config, "VersionNumber", VERSION_NUMBER)

	KEP_ConfigSave(config)
end

-- Set a config string unless the value has already been set. Do this so we don't override changes made by user
function setConfigStringIfEmpty(config, key, value)
	if m_newVersion or not KEP_ConfigValueExists(config, key) then
		KEP_ConfigSetString(config, key, value)
	end
end

-- Add the menu to a list containing all menus controlled by the UILayout feature
function addMenuToLayout(config, menuName)
	local ok, currentMenus = KEP_ConfigGetString(config, "MenuList", "")

	-- Already exists, don't add it again
	if string.find(currentMenus, menuName) then
		return
	end

	currentMenus = currentMenus .. " " .. menuName

	KEP_ConfigSetString(config, "MenuList", currentMenus)
end

function getHUDMenus()
	return HUD_MENUS.default
end

function printTable(table)
	for key,value in pairs(table) do
		if type(value) == "table" then
			log("--------------START TABLE " .. key)
			printTable(value)
			log("--------------END TABLE " .. key)
		else
			log(tostring(key) .. ", " .. tostring(value))
		end
	end
end