--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Native event specifications

REPLEvents = {
	["REPLInputEvent"] = {
		{"input", "string"},			-- String: `input'
	},

	["REPLResultEvent"] = {
		{"incomplete", "number", 0 },	-- Number: `incomplete' OR 0
		{"error",      "string", ""},	-- String: `error' OR ""
	},
}
