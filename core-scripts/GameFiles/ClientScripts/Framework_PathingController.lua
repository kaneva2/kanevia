--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\MenuFunctions.lua")
dofile("..\\MenuScripts\\ObjectFunctions.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\LibClient_Common.lua")
dofile("..\\MenuScripts\\Lib_GameDefinitions.lua")
dofile("..\\Scripts\\LWG.lua")

--Update it to use the LWG version
GT_LINESTRIP = 258

local m_gearOffsetYFromWaypoint = 7.5
local m_waypointInteractionRangeSqrd = 625

local m_waypointObjectInit = false

local m_currentBuildMode = nil

local m_pathRiders = {}
local m_selectedPathWaypointPID = nil
local m_selectedPathWaypointPIDCoords = {}
local m_selectedPathWaypointGearPID = nil
local m_selectedPathParentPID = nil
local m_selectedPath = {}
local m_selectedPathType = nil
local m_pathWaypointsCoords = {}
local m_drawnPathGeomID = nil

local m_lastGameItem = nil

function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP_EventRegister( "PathWaypointSelectedEvent", KEP.HIGH_PRIO )
end

-- InitializeKEPEventHandlers - Register all script event handlers here. 
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )
	KEP_EventRegisterHandler( "translateEventHandler", "WidgetTranslationEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "rotateEventHandler", "WidgetRotationEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "selectEventHandler", "SelectEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BuildExitEventHandler", "BuildExitEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "DynamicObjectInitializedEventHandler", "DynamicObjectInitializedEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "changeBuildModeEventHandler", CHANGE_BUILD_MODE_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "setObjectsSelectedPosRotEventHandler", "SetObjectsSelectedPosRotEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ScriptClientEventHandler", "ScriptClientEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "GoToPlaceEventHandler", "GoToPlaceEvent", KEP.MED_PRIO )
	
	Events.registerHandler("ChildPathWaypointPidsEvent", ChildPathWaypointPidsEventHandler)
	Events.registerHandler("PathRiderPlacedEvent", PathRiderPlacedEventHandler)
	Events.registerHandler("PathRiderPickedUpEvent", PathRiderPickedUpEventHandler)
end

function resetPathing()
	if m_drawnPathGeomID then
		KEP_DestroyGeometry(m_drawnPathGeomID)
		m_drawnPathGeomID = nil
	end
	if m_selectedPathWaypointPID then
		m_selectedPathWaypointPID = nil
		m_selectedPathWaypointPIDCoords = {}
	end
	if m_selectedPathWaypointGearPID then
		KEP_DeleteDynamicObjectPlacement(m_selectedPathWaypointGearPID)
		m_selectedPathWaypointGearPID = nil
	end
	m_selectedPathParentPID = nil
	m_selectedPath = {}
	m_selectedPathType = nil
	m_pathWaypointsCoords = {}
end

function handlePathWaypointSelection(selectedObjPID)
	m_selectedPathWaypointPID = selectedObjPID
	m_selectedPathParentPID = nil
	m_selectedPath = {}
	m_selectedPathType = nil
	m_pathWaypointsCoords = {}
	if m_drawnPathGeomID then
		KEP_DestroyGeometry(m_drawnPathGeomID)
		m_drawnPathGeomID = nil
	end
	waypointX, waypointY, waypointZ = KEP_DynamicObjectGetPositionAnim(m_selectedPathWaypointPID)
	m_selectedPathWaypointPIDCoords.x = waypointX
	m_selectedPathWaypointPIDCoords.y = waypointY
	m_selectedPathWaypointPIDCoords.z = waypointZ
	if m_selectedPathWaypointGearPID then
		KEP_SetDynamicObjectPosition(m_selectedPathWaypointGearPID, waypointX, waypointY + m_gearOffsetYFromWaypoint, waypointZ)
	else
		m_selectedPathWaypointGearPID = KEP_PlaceLocalDynamicObj(GameDefinitions.PATHWAYPOINT_GEAR_GLID, 1.0, 1, 0, waypointX, waypointY + m_gearOffsetYFromWaypoint, waypointZ)
	end
	if m_selectedPathWaypointGearPID then
		KEP_AddMouseOverSetting(ObjectType.DYNAMIC, m_selectedPathWaypointGearPID, MOUSEOVER_OUTLINE, "Click to Edit", CURSOR_TYPE_DEFAULT)
	end
	waypointRot = ObjectGetRotation(m_selectedPathWaypointPID, ObjectType.DYNAMIC)
	ObjectSetRotation(m_selectedPathWaypointGearPID, ObjectType.DYNAMIC, waypointRot)
	Events.sendEvent("RequestForAllSiblingWaypointPIDs", {pathWaypointPID = m_selectedPathWaypointPID})
end

function RetrieveWaypointLocation()
	local noOfWaypoints = 0
	for i, v in pairs(m_selectedPath) do
		noOfWaypoints = noOfWaypoints + 1
		m_pathWaypointsCoords[tostring(v)] = {}
		if v == m_selectedPathWaypointPID then
			m_pathWaypointsCoords[tostring(v)] = m_selectedPathWaypointPIDCoords
		else
			m_pathWaypointsCoords[tostring(v)].x, m_pathWaypointsCoords[tostring(v)].y, m_pathWaypointsCoords[tostring(v)].z = KEP_DynamicObjectGetPositionAnim(v)
		end
	end
	return noOfWaypoints
end

function DrawCompletePath()
	m_pathWaypointsCoords = {}
	local noOfWaypoints = RetrieveWaypointLocation()
	if noOfWaypoints < 2 then
		if m_drawnPathGeomID ~= nil then
			KEP_DestroyGeometry(m_drawnPathGeomID)
		end
		return
	end
	local linePointsArray = {}
	local lineNormalArray = {}
	local count = 1
	for i, v in pairs(m_selectedPath) do
		linePointsArray[count] =  m_pathWaypointsCoords[tostring(v)].x
		linePointsArray[count + 1] =  m_pathWaypointsCoords[tostring(v)].y
		linePointsArray[count + 2] =  m_pathWaypointsCoords[tostring(v)].z
		count = count + 3
	end
	if (m_selectedPathType == 1 or m_selectedPathType == 3) and (noOfWaypoints > 2) then
		--closed or reverse path, add 1st waypoint at the end again to complete a loop
		linePointsArray[count] = m_pathWaypointsCoords[tostring(m_selectedPath[1])].x
		linePointsArray[count + 1] = m_pathWaypointsCoords[tostring(m_selectedPath[1])].y
		linePointsArray[count + 2] = m_pathWaypointsCoords[tostring(m_selectedPath[1])].z
		count = count + 3
	end
	if m_drawnPathGeomID ~= nil then
		KEP_DestroyGeometry(m_drawnPathGeomID)
	end
	m_drawnPathGeomID = KEP_CreateCustomGeometry(GT_LINESTRIP, linePointsArray, lineNormalArray)
	KEP_SetGeometryMaterial(m_drawnPathGeomID, {diffuse = {r = 0.0, g = 0.0, b = 1.0, a = 1.0}, 
												ambient = {}, 
												specular = {}, 
												emissive = {r = 0.0, g = 1.0, b = 1.0, a = 1.0}, 
												power = {}})
	KEP_SetGeometryVisible(m_drawnPathGeomID, true)
end

function ChildPathWaypointPidsEventHandler ( event )
	if m_selectedPathWaypointPID == event.selectedPathWaypointPID then
		m_selectedPathParentPID = event.selectedPathParentPID
		m_selectedPath = event.childPathWaypointPIDs
		m_selectedPathType = event.pathType
		DrawCompletePath()
	else
		resetPathing()
	end
end

function PathRiderPlacedEventHandler ( event )
	m_pathRiders[tostring(event.riderPID)] = 1
end

function PathRiderPickedUpEventHandler ( event )
	m_pathRiders[tostring(event.riderPID)] = nil
end

-- Handles events from the server NOTE: Currently only needed for Framework events
function ScriptClientEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- ScriptClientEvent
	if filter == 4 then
		-- All ScriptClientEvents with a filter of 4 will have the number of arguments encoded into the
		-- event as its first number
        local args = Event_DecodeNumber(event)
        if args == 2 then
            local eventType = Event_DecodeString(event)
			
			-- One Game Item remains
			if eventType == "FRAMEWORK_ONE_GAME_ITEM_DO_LEFT" then
				local eventTable = cjson.decode(Event_DecodeString(event))
				local lastGameItem = eventTable.lastGameItem
				g_playerPerm = eventTable.playerPerm
				
				if lastGameItem then
					m_lastGameItem = lastGameItem
				else
					m_lastGameItem = nil
				end
			end
		end
	end
end

function selectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	selectedObjPID = nil
	needToReset = true
	local count = ObjectsSelected()
	if count == 1 then
		local selectedObj = ObjectSelected(1)
		selectedObjPID = selectedObj.id
		_, _, _, _, _, _, _, _, _, _, glid, invSubType, texUrl, objUNID, playerId = KEP_DynamicObjectGetInfo(selectedObjPID)
		if glid == GameDefinitions.PATHWAYPOINT_GEAR_GLID then
			local posX, posY, posZ = KEP_GetPlayerPosition()
			local wpX, wpY, wpZ = KEP_DynamicObjectGetPositionAnim(m_selectedPathWaypointPID)
			m_selectedPathWaypointPIDCoords.x = wpX
			m_selectedPathWaypointPIDCoords.y = wpY
			m_selectedPathWaypointPIDCoords.z = wpZ
			local xDiff = posX - wpX
			local yDiff = posY - wpY
			local zDiff = posZ - wpZ
			local xyzDistanceSqrd = xDiff * xDiff + zDiff * zDiff + yDiff * yDiff
			if (m_waypointInteractionRangeSqrd <= xyzDistanceSqrd) then
				local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventEncodeString(statusEvent, "You must be closer to interact with that object.")
				KEP_EventEncodeNumber(statusEvent, 3)
				KEP_EventEncodeNumber(statusEvent, 255)
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventSetFilter(statusEvent, 4)
				KEP_EventQueue(statusEvent)
			else
				needToReset = false
				MenuOpen("Framework_PathWaypointOptions.xml")
				local e = KEP_EventCreate("PathWaypointSelectedEvent")
				KEP_EventEncodeNumber(e, m_selectedPathWaypointPID)
				if m_lastGameItem and m_lastGameItem[tostring(m_selectedPathWaypointPID)] == m_selectedPathWaypointPID then
					KEP_EventEncodeNumber(e, 1)
				else
					KEP_EventEncodeNumber(e, 0)
				end
				KEP_EventQueue(e)
			end
		elseif objUNID == GameDefinitions.PATHWAYPOINT_UNID and m_waypointObjectInit == true then
			needToReset = false
			handlePathWaypointSelection(selectedObjPID)
		end
	end
	
	local count = KEP_EventDecodeNumber(event)
	for i = 1,count do
		local type     = KEP_EventDecodeNumber(event) or ObjectType.NONE
		local id	   = KEP_EventDecodeNumber(event) or 0
		local selected = KEP_EventDecodeNumber(event) or 0
		if m_pathRiders[tostring(id)] == 1 then
			KEP_DeselectObject(type, id)
		end
	end
	
	if needToReset then
		resetPathing()
	end
end

function translateEventHandler( dispatcher, fromNetId, event, eventId, filter, objectId )
	if m_selectedPathWaypointPID ~= nil then
		x = KEP_EventDecodeNumber(event) or 0
		y = KEP_EventDecodeNumber(event) or 0
		z = KEP_EventDecodeNumber(event) or 0
		m_selectedPathWaypointPIDCoords.x = x
		m_selectedPathWaypointPIDCoords.y = y
		m_selectedPathWaypointPIDCoords.z = z
		if m_drawnPathGeomID ~= nil then
			DrawCompletePath()
		end
		if m_selectedPathWaypointGearPID then
			KEP_SetDynamicObjectPosition(m_selectedPathWaypointGearPID, x, y + m_gearOffsetYFromWaypoint, z)
		end
	end
end

function rotateEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if m_selectedPathWaypointGearPID and m_selectedPathWaypointPID then
		waypointRot = ObjectGetRotation(m_selectedPathWaypointPID, ObjectType.DYNAMIC)
		ObjectSetRotation(m_selectedPathWaypointGearPID, ObjectType.DYNAMIC, waypointRot)
	end
end

function setObjectsSelectedPosRotEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if m_selectedPathWaypointPID ~= nil and m_currentBuildMode == MODE_MOVE then
		x = KEP_EventDecodeNumber(event) or 0
		y = KEP_EventDecodeNumber(event) or 0
		z = KEP_EventDecodeNumber(event) or 0
		m_selectedPathWaypointPIDCoords.x = x
		m_selectedPathWaypointPIDCoords.y = y
		m_selectedPathWaypointPIDCoords.z = z
		if m_drawnPathGeomID ~= nil then
			DrawCompletePath()
		end
		if m_selectedPathWaypointGearPID then
			KEP_SetDynamicObjectPosition(m_selectedPathWaypointGearPID, x, y + m_gearOffsetYFromWaypoint, z)
		end
	elseif m_selectedPathWaypointGearPID and m_selectedPathWaypointPID and m_currentBuildMode == MODE_ROTATE then
		waypointRot = ObjectGetRotation(m_selectedPathWaypointPID, ObjectType.DYNAMIC)
		ObjectSetRotation(m_selectedPathWaypointGearPID, ObjectType.DYNAMIC, waypointRot)
	end
end

function changeBuildModeEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	m_currentBuildMode = filter
end

function BuildExitEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	MenuClose("PathWaypointOptions.xml")
	MenuClose("PathWaypointEditor.xml")
	if m_drawnPathGeomID ~= nil then
		KEP_DestroyGeometry(m_drawnPathGeomID)
	end
end

function DynamicObjectInitializedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if m_waypointObjectInit == false then
		_, _, _, _, _, _, _, _, _, _, glid, invSubType, texUrl, objUNID, playerId = KEP_DynamicObjectGetInfo(objectid)
		if objUNID == GameDefinitions.PATHWAYPOINT_UNID then
			m_waypointObjectInit = true
		end
	end
end

function GoToPlaceEventHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	if( filter == KEP.GOTO_URL ) then
		m_waypointObjectInit = false
	end
end