--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\GameGlobals.lua")

--Catch player spawned event before web call for dyn obj and send request to server
--filter is the zoneIndex
--decode instanceId
function ClientSpawnEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local instanceID =  KEP_EventDecodeNumber( event )
	local zoneIndex = filter

	-- requeue event to server
	local evt = KEP_EventCreate( KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME )
	if evt ~= 0 then
		KEP_EventAddToServer( evt )
		KEP_EventSetFilter( evt, zoneIndex )
		KEP_EventEncodeNumber( evt, instanceID )
		KEP_EventQueue( evt )
	end
	return KEP.EPR_CONSUMED
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "ClientSpawnEventHandler", "ClientSpawnEvent", KEP.LOW_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( KEP.SERVERDYNOBJ_REQUEST_EVENT_NAME, KEP.MED_PRIO )
end
