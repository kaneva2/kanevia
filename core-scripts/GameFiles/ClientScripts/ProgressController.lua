--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\WebSuffix.lua")

-- progress dialog variables
g_progressOpening = false -- pending progress dialog to open   
g_progressClosing = false -- pending progress dialog to close
g_progressCanceling = false -- pending progress to cancel
g_progressErroring = false -- pending progress to error
g_progressMsg = nil -- progress message text (attached to metrics)
g_progressTag = nil -- progress tag text (attached to metrics)
g_progressGuid = nil -- progress guid text (attached to metrics)

-- progress options
g_optCancel = PROGRESS_OPT_CANCEL_DEFAULT
g_optFade = PROGRESS_OPT_FADE_DEFAULT
g_optMsg = PROGRESS_OPT_MSG_DEFAULT

-- progress references
g_numRefs = 0
g_refs = {}

-- progress cancel url (do not reset)
g_lastGoodUrl = "home"

-- render text exceptions (forces close menu if these occur)
-- Most of these are defined in KEPLang_ENU.xml
g_excRenderText = {
	"Login Failed", -- standardized in ED-4559
    "Error", 
	"Server is not available", -- id=91, ClientEngine::LoginToServerFailed()
    "Your account is locked", -- id=107
    "Server is not authorizing players", -- id=108
    "You cannot go to that place", -- id=111,112,114
    "You currently do not have access to that place", -- id=113
    "The STAR has reached its population limit. Please try later.", -- id=127
    "You are not authorized to travel to that STAR.", -- id=128
    "go to that place", -- id=94,
    "access to that place",
    "No patch urls for game"
}

function ProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

    -- Handle All Progress Events
    if (filter == PROGRESS_OPEN) then
        local id = KEP_EventDecodeString( event )
        ProgressOpenEventHandler(id)
    elseif (filter == PROGRESS_OPENED) then
        ProgressOpenedEventHandler()
    elseif (filter == PROGRESS_CLOSE) then 
        local id = KEP_EventDecodeString( event )
        ProgressCloseEventHandler(id)
    elseif (filter == PROGRESS_CLOSED) then 
        ProgressClosedEventHandler()
    elseif (filter == PROGRESS_UPDATE) then 
        local id = KEP_EventDecodeString( event )
        local msg = KEP_EventDecodeString( event )
        ProgressUpdateEventHandler(id, msg)
    elseif (filter == PROGRESS_ERROR) then 
        local id = KEP_EventDecodeString( event )
        local msg = KEP_EventDecodeString( event )
        ProgressErrorEventHandler(id, msg)
    elseif (filter == PROGRESS_CANCEL) then 
        local id = KEP_EventDecodeString( event )
        local msg = KEP_EventDecodeString( event )
        ProgressCancelEventHandler(id, msg)
    elseif (filter == PROGRESS_TAG) then 
        local id = KEP_EventDecodeString( event )
        local tag = KEP_EventDecodeString( event )
        ProgressTagEventHandler(id, tag)
    elseif (filter == PROGRESS_OPT_CANCEL) then 
        if (id ~= "ProgressController") then  
            local id = KEP_EventDecodeString( event )
            local optCancel = KEP_EventDecodeNumber( event )
            local optChange = ((g_optCancel ~= PROGRESS_OPT_CANCEL_NEVER) and (g_optCancel ~= PROGRESS_OPT_CANCEL_ALWAYS))
            if (optChange) then
                g_optCancel = optCancel 
            end
        end
    elseif (filter == PROGRESS_OPT_FADE) then 
        if (id ~= "ProgressController") then  
            local id = KEP_EventDecodeString( event )
            local optFade = KEP_EventDecodeNumber( event )
            if (g_optFade ~= PROGRESS_OPT_FADE_MIN_NOW and g_optFade ~= PROGRESS_OPT_FADE_MAX_NOW) then
                g_optFade = optFade 
            end
        end
    elseif (filter == PROGRESS_OPT_MSG) then 
        if (id ~= "ProgressController") then  
            local id = KEP_EventDecodeString( event )
            local optMsg = KEP_EventDecodeNumber( event )
            g_optMsg = optMsg 
        end
    elseif (filter == PROGRESS_LAST_GOOD_URL_SET) then 
        if (id ~= "ProgressController") then  
            g_lastGoodUrl = KEP_EventDecodeString( event )
        end
    elseif (filter == PROGRESS_LAST_GOOD_URL_GET) then 
        if (id ~= "ProgressController") then  
            ProgressLastGoodUrlSet(g_lastGoodUrl)
        end  
    end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
    KEP_EventRegisterHandler( "ProgressEventHandler", "ProgressEvent", KEP.MED_PRIO )
    KEP_EventRegisterHandler( "RenderTextEventHandler", "RenderTextEvent", KEP.MED_PRIO )
    KEP_EventRegisterHandler( "BadGotoPlaceURLEventHandler", "BadGotoPlaceURLEvent", KEP.MED_PRIO )
    KEP_EventRegisterHandler( "DisconnectedEventHandler", "DisconnectedEvent", KEP.MED_PRIO )
    KEP_EventRegisterHandler( "CloseProgressMenuHandler", "CloseProgressMenu", KEP.HIGH_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
    KEP_EventRegister( "ProgressEvent", KEP.MED_PRIO )
    KEP_EventRegister( "CompleteLoad", KEP.HIGH_PRIO )
    KEP_EventRegister( "CloseProgressMenu", KEP.MED_PRIO )
    KEP_EventRegister( "BadGotoPlaceURLEvent", KEP.MED_PRIO )
end

function ProgressOptRefresh()
    ProgressOptCancel("ProgressController", g_optCancel)
    ProgressOptFade("ProgressController", g_optFade)
    ProgressOptMsg("ProgressController", g_optMsg)
end

function ProgressRefsLog()
    Log("ProgressRefsLog: numRefs="..g_numRefs)
    for id,_ in pairs(g_refs) do
        local timeMs = GetTimeElapsedMs(g_refs[id])
        Log("ProgressRefsLog: ... '"..id.."' timeMs="..timeMs)
    end
end

function ProgressMenuIsOpen()
    return MenuIsOpen("ProgressMenu.xml")
end

function ProgressMenuIsOpenOrOpening()
    return (ProgressMenuIsOpen() or g_progressOpening)
end

function ProgressMenuIsClosed()
    return (not ProgressMenuIsOpen())
end

function ProgressMenuIsClosedOrClosing()
    return (ProgressMenuIsClosed() or g_progressClosing)
end

function ProgressMenuOpen(now)
    
    -- Progress Menu Already Open or Opening?
    if ((now == false) and ProgressMenuIsOpenOrOpening()) then
        return
    end
        
    -- Open Progress Menu
    Log("ProgressMenuOpen: Opening Progress Menu")
    g_progressOpening = true
    MenuOpen("ProgressMenu.xml")
 
    -- Now ?
    if (now) then
        if (g_optFade == PROGRESS_OPT_FADE_MIN) then
            g_optFade = PROGRESS_OPT_FADE_MIN_NOW -- fade min now following reopen
        end
        if (g_optFade == PROGRESS_OPT_FADE_MAX) then
            g_optFade = PROGRESS_OPT_FADE_MAX_NOW -- fade max now following reopen
        end
    end

    -- Refresh Progress Options
    ProgressOptRefresh()
end

function CloseProgressMenuHandler(event)
   ProgressMenuClose()
end

function ProgressMenuClose()

    -- Progress Menu Already Closed or Closing?
    if (ProgressMenuIsClosedOrClosing()) then
        return
    end
    
    -- Close Progress Menu
    Log("ProgressMenuClose: Closing Progress Menu")
    g_progressClosing = true
    MenuClose("ProgressMenu.xml")
end

function ProgressOpenEventHandler(id)
    Log("ProgressOpenEventHandler: '"..id.."'")
    
    -- Identifier Already Open?
    if (g_refs[id] ~= nil) then
        Log("ProgressOpenEventHandler: '"..id.."' Already Open!")
        return
    end

    -- Wrap 4 Stages Of Rezone (Loading->PatchProgress->ZoneDownloader->LoadingSpinner)
    if (id == "Loading" or id == "PatchProgress" or id == "ZoneDownloader" or id == "LoadingSpinner") then
        if g_refs["Rezone"] == nil then
            g_idWrap = id
            Log("ProgressOpenEventHandler: Automatically Opening Rezone - START - idWrap="..g_idWrap)
            ProgressOpenEventHandler("Rezone")
            Log("ProgressOpenEventHandler: Automatically Opening Rezone - END")
        end
    end
        
    -- Assign Guid [timeMs:userId:rand] & Send ProgressMenu Metrics On Open
    if (g_numRefs == 0) then
        g_progressGuid = GetTimeMs().."_"..GetUserId().."_"..MathRandomInt(999999)
        g_refs["ProgressMenu"] = GetTimeMs() -- add id reference as time now
    end
    
    -- Add Identifier Reference
    g_refs[id] = GetTimeMs() -- add id reference as time now
    g_numRefs = g_numRefs + 1 -- increment number of references
    
    -- Open Progress Menu If References And Not Open Or Opening
    if (g_numRefs > 0 and not ProgressMenuIsOpenOrOpening()) then
        
        -- Close All Pre-Rezone Menus Unless Wrapping ZoneDownloader
        if (id == "Rezone") then
            if (g_idWrap ~= "ZoneDownloader") then
                MenuCloseAllPreRezone()
            end
            g_idWrap = ""
        end
        
        -- Open Progress Menu (not now)
        ProgressMenuOpen(false)
    end
end

function ProgressOpenedEventHandler()
    Log("ProgressOpenedEventHandler: Progress Menu Opened")
    
    -- Unexpectedly Opened?
    if (g_progressOpening == false) then
        LogError("ProgressOpenedEventHandler: Progress Opened Unexpected - Closing...")
        ProgressMenuClose()
        return
    end
    
    -- Progress Is Done Opening
    g_progressOpening = false
end

function ProgressCloseEventHandler(id)
    Log("ProgressCloseEventHandler: '"..id.."'")
    
    -- Identifier Already Closed?
    if (g_refs[id] == nil) then
        Log("ProgressCloseEventHandler: '"..id.."' Already Closed!")
        return
    end
    
    -- Remove Identifier Reference
    g_refs[id] = nil -- remove id reference
    g_numRefs = g_numRefs - 1 -- decrement number of references

    -- Close Progress Menu If No References Or Rezone Complete
    local idRezone = (id == "Rezone")
    if ((g_numRefs <= 0) or idRezone) then
        
        -- Rezone Complete - Remember Last Good Url
        if (idRezone and g_progressTag and g_progressTag~="") then
            g_lastGoodUrl = g_progressTag
            Log("ProgressCloseEventHandler: lastGoodUrl='"..g_lastGoodUrl.."'")
        end
        KEP_EventCreateAndQueue("CompleteLoad")
    end
end

function ProgressClosedEventHandler()
    Log("ProgressClosedEventHandler: Progress Menu Closed")
    
    -- Unexpectedly Closed?
    if (g_progressClosing == false) then
        LogError("ProgressClosedEventHandler: Progress Closed Unexpected - Opening Now...")
        ProgressMenuOpen(true)
        return
    end

    -- Cancel If Opening
    if (g_progressOpening == true) then
        Log("ProgressClosedEventHandler: Progress Opening - Cancelling Close...")
        return
    end
            
    -- Flag Window Refresh (just incase)
-- DRF - Doesn't seem to fix customize avatar resize issue, might be causing crash during rezone. Disable for now.
--	KEP_WindowRefresh()

    -- Finish Closing Progress (reset everything)
    g_progressGuid = nil
    g_progressMsg = nil
    g_progressTag = nil
    g_numRefs = 0
    g_refs = {}
    g_optCancel = PROGRESS_OPT_CANCEL_DEFAULT
    g_optFade = PROGRESS_OPT_FADE_DEFAULT
    g_optMsg = PROGRESS_OPT_MSG_DEFAULT

    -- Progress Is Done Closing & Erroring
    g_progressClosing = false
    g_progressErroring = false

    -- Progress Is Done Canceling
    if (g_progressCanceling) then
        g_progressCanceling = false
        
        -- Goto Last Good Url
        -- Only try this if LoginMenu is not open otherwise user must login
        if MenuIsClosed("LoginMenu.xml") then
--			Log("ProgressClosedEventHandler: gotoURL(lastGoodUrl) url='"..g_lastGoodUrl.."'")
--			gotoURL(g_lastGoodUrl) -- apparently url is not universal, go home instead
            goHome()
        end
    end
end

function ProgressUpdateEventHandler(id, msg)
--	Log("ProgressUpdateEventHandler: '"..id.."' msg='"..msg.."'")
end

function ProgressErrorEventHandler(id, msg)
    Log("ProgressErrorEventHandler: '"..id.."' msg='"..msg.."'")
    g_progressErroring = true
    g_progressMsg = msg
end

function ProgressCancelEventHandler(id, msg)
    Log("ProgressCancelEventHandler: '"..id.."' msg='"..msg.."'")
    g_progressCanceling = true
    ProgressMenuClose()
end

function ProgressTagEventHandler(id, tag)
    Log("ProgressTagEventHandler: '"..id.."' tag='"..tag.."'")
    g_progressTag = tag
end

function ProgressCreateError(msg)
    Log("ProgressCreateError: msg='"..msg.."'")

    -- Reopen If Error While Closing
    if (g_progressClosing == true) then
        Log("ProgressCreateError: ProgressError While Closing - Opening Now...")
        ProgressMenuOpen(true)
    end

    -- Progress Error (as 'ProgressMenu' to report metrics)
    ProgressError("ProgressMenu", msg)
end

function RenderTextEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    
	-- ED-8441 - Send Chats To Chat Log (KanevaChatLog.log)
	local msg = KEP_EventDecodeString( event )
    local st, en = string.find(msg, " says > ") 
    if (st ~= nil) then
		KEP_LogChat(msg)
	else
		Log("RenderTextEventHandler: '"..msg.."'")
	end

    -- Progress Menu Already Closed?
    if (ProgressMenuIsClosed()) then
        return
    end

    -- Create Progress Error On RenderText Exceptions
    for i,_ in pairs(g_excRenderText) do 
        local st, en = string.find(msg, g_excRenderText[i]) 
        if (st ~= nil) then
            Log("RenderTextEventHandler: Caught Exception!")
            ProgressCreateError(msg)
        end
    end
end

function BadGotoPlaceURLEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

    -- Progress Menu Already Closed?
    if (ProgressMenuIsClosed()) then
        return
    end

    -- Create Progress Error
    Log("BadGotoPlaceURLEventHandler: Caught Exception!")
    ProgressCreateError("URL Invalid")
end

function DisconnectedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

    -- Progress Menu Already Closed?
    if (ProgressMenuIsClosed()) then
        return
    end

    -- Create Progress Error
    Log("DisconnectedEventHandler: Caught Exception!")
    ProgressCreateError("Network Disconnected")
end
