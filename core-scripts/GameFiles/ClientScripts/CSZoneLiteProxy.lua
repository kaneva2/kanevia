--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")

CSZoneLiteProxyEvents = {}
CSZoneLiteProxyEvents.ImportZoneSuccess    = "CSZLP_ImportZoneSuccessEvent"
CSZoneLiteProxyEvents.ImportChannelSuccess = "CSZLP_ImportChannelSuccessEvent"

PROXYEvents = {}
PROXYEvents.Request = {}
PROXYEvents.Request.ZoneList         = "CSZLP_ZoneListRequestEvent"
PROXYEvents.Request.SpawnList        = "CSZLP_SpawnListRequestEvent"
PROXYEvents.Request.CreateZone       = "CSZLP_CreateZoneRequestEvent"
PROXYEvents.Request.DeleteZone       = "CSZLP_DeleteZoneRequestEvent"
PROXYEvents.Request.RenameZone       = "CSZLP_RenameZoneRequestEvent"
PROXYEvents.Request.SetDefaultZone   = "CSZLP_SetDefaultZoneRequestEvent"
PROXYEvents.Request.AddSpawn         = "CSZLP_AddSpawnRequestEvent"
PROXYEvents.Request.DeleteSpawn      = "CSZLP_DeleteSpawnRequestEvent"
PROXYEvents.Request.SetInitialArena  = "CSZLP_SetInitialArenaRequestEvent"
PROXYEvents.Request.UpdateSpawn      = "CSZLP_UpdateSpawnRequestEvent"
PROXYEvents.Request.ImportZone       = "CSZLP_ImportZoneRequestEvent"
PROXYEvents.Request.ImportChannel    = "CSZLP_ImportChannelRequestEvent"
PROXYEvents.Handlers = {}
PROXYEvents.Handlers.ZoneList        = "CSZLP_ZoneListRequestEventHandler"
PROXYEvents.Handlers.SpawnList       = "CSZLP_SpawnListRequestEventHandler"
PROXYEvents.Handlers.CreateZone      = "CSZLP_CreateZoneRequestEventHandler"
PROXYEvents.Handlers.DeleteZone      = "CSZLP_DeleteZoneRequestEventHandler"
PROXYEvents.Handlers.RenameZone      = "CSZLP_RenameZoneRequestEventHandler"
PROXYEvents.Handlers.SetDefaultZone  = "CSZLP_SetDefaultZoneRequestEventHandler"
PROXYEvents.Handlers.AddSpawn        = "CSZLP_AddSpawnRequestEventHandler"
PROXYEvents.Handlers.DeleteSpawn     = "CSZLP_DeleteSpawnRequestEventHandler"
PROXYEvents.Handlers.SetInitialArena = "CSZLP_SetInitialArenaRequestEventHandler" ---- _SCR_ TODO
PROXYEvents.Handlers.UpdateSpawn     = "CSZLP_UpdateSpawnRequestEventHandler"
PROXYEvents.Handlers.ImportZone      = "CSZLP_ImportZoneRequestEventHandler"
PROXYEvents.Handlers.ImportChannel   = "CSZLP_ImportChannelRequestEventHandler"

PROXYEvents.Reply = {}
PROXYEvents.Reply.ZoneList         = "CSZLP_ZoneListReplyEvent"
PROXYEvents.Reply.SpawnList        = "CSZLP_SpawnListReplyEvent"
PROXYEvents.Reply.CreateZone       = "CSZLP_CreateZoneReplyEvent"
PROXYEvents.Reply.DeleteZone       = "CSZLP_DeleteZoneReplyEvent"
PROXYEvents.Reply.RenameZone       = "CSZLP_RenameZoneReplyEvent"
PROXYEvents.Reply.SetDefaultZone   = "CSZLP_SetDefaultZoneReplyEvent"
PROXYEvents.Reply.AddSpawn         = "CSZLP_AddSpawnReplyEvent"
PROXYEvents.Reply.DeleteSpawn      = "CSZLP_DeleteSpawnReplyEvent"
PROXYEvents.Reply.SetInitialArena  = "CSZLP_SetInitialArenaReplyEvent"
PROXYEvents.Reply.UpdateSpawn      = "CSZLP_UpdateSpawnReplyEvent"
PROXYEvents.Reply.ImportZone       = "CSZLP_ImportZoneReplyEvent"
PROXYEvents.Reply.ImportChannel    = "CSZLP_ImportChannelReplyEvent"
PROXYEvents.Reply.ZoneStateChange  = "CSZLP_ZoneStateChangeReplyEvent"

-- Events/Handler Names
PROXYServerEvents = {}
PROXYServerEvents.UgcZoneLiteEvent                  = "UgcZoneLiteEvent"
PROXYServerEvents.UgcZoneLiteEventHandler           = "CSZLP_UgcZoneLiteEventHandler"

PROXYServerEvents.SpaceImportEvent                  = "SpaceImportEvent"
PROXYServerEvents.SpaceImportResponseEvent          = "SpaceImportResponseEvent"
PROXYServerEvents.SpaceImportResponseEventHandler   = "CSZLP_SpaceImportResponseEventHandler"

PROXYServerEvents.ChannelImportEvent                = "ChannelImportEvent"
PROXYServerEvents.ChannelImportResponseEvent        = "ChannelImportResponseEvent"
PROXYServerEvents.ChannelImportResponseEventHandler = "CSZLP_ChannelImportResponseEventHandler"

-- from UGCZoneLiteEvent.h
-- filter values for requests
local PROXYUGCFilterIds = {}
PROXYUGCFilterIds.REQ_CREATE_ZONE       = 1
PROXYUGCFilterIds.REQ_LIST_ZONES        = 2
PROXYUGCFilterIds.REQ_DELETE_ZONE       = 3
PROXYUGCFilterIds.REQ_RENAME_ZONE       = 4
PROXYUGCFilterIds.REQ_SET_DEFAULT_ZONE  = 5
PROXYUGCFilterIds.REQ_LIST_SPAWNS       = 6
PROXYUGCFilterIds.REQ_ADD_SPAWN         = 7
PROXYUGCFilterIds.REQ_DELETE_SPAWN      = 8
PROXYUGCFilterIds.REQ_SET_INITIAL_ARENA = 9
PROXYUGCFilterIds.REQ_UPDATE_SPAWN      = 10
PROXYUGCFilterIds.REQ_RE_CREATE_ZONE    = 11

-- filter values for replies
PROXYUGCFilterIds.REPLY_ZONES_LIST        = 101
PROXYUGCFilterIds.REPLY_SPAWNS_LIST       = 102
PROXYUGCFilterIds.REPLY_CREATE_ZONE       = 103
PROXYUGCFilterIds.REPLY_DELETE_ZONE       = 104
PROXYUGCFilterIds.REPLY_RENAME_ZONE       = 105
PROXYUGCFilterIds.REPLY_SET_DEFAULT_ZONE  = 106
PROXYUGCFilterIds.REPLY_ADD_SPAWN         = 107
PROXYUGCFilterIds.REPLY_DELETE_SPAWN      = 108
PROXYUGCFilterIds.REPLY_SET_INITIAL_ARENA = 109
PROXYUGCFilterIds.REPLY_UPDATE_SPAWN      = 110
PROXYUGCFilterIds.REPLY_RE_CREATE_ZONE    = 111

-- HACK
-- We need to filter out bad data that comes from server.supported_worlds
-- Only the following zones will considered valid
PROXYValidKanevaZones = {
	"Theater",
	"Big 1 BR Apartment",
	"Studio Apartment",
	"Med 1 BR Apartment",
	"L Shaped Apartment",
	"Club",
	"Night Club",
	"Small Theater",
	"Small Coffee House",
	"City Home",
	"Manor Home",
	"Public Dance Club",
	"Easter Maze",
	"Premium_Outdoor_Retreat",
	"Exclusive Beach House",
	"Outdoor_Retreat",
	"Island",
	"Green Acres",
	"Sandbox"
}

CSZLPLastOperation = {}
CSZLPLastOperation.Result = {}
CSZLPLastOperation.Result.NONE      = 0
CSZLPLastOperation.Result.SUCCESS   = 1
CSZLPLastOperation.Result.PENDING   = 2
CSZLPLastOperation.Result.FAILED    = 3
CSZLPLastOperation.Result.IMPORTING = 4
CSZLPLastOperation.Msg = {}
CSZLPLastOperation.Msg.NONE       = ""
CSZLPLastOperation.Msg.CREATE     = "Create"
CSZLPLastOperation.Msg.DELETE     = "Delete"
CSZLPLastOperation.Msg.RENAME     = "Rename"
CSZLPLastOperation.Msg.IMPORT     = "Import"
CSZLPLastOperation.Msg.SETDEFAULT = "SetDefault"

--  monitoredZone = {}
--    monitoredZone.zoneIndex
--    monitoredZone.lastOpResult = CSZLPLastOperation.Result
--    monitoredZone.lastOpMsg    = CSZLPLastOperation.Msg
g_monitoredZones = {}

CSLPImportTypes = {}
CSLPImportTypes.ZONE      = 0
CSLPImportTypes.COMMUNITY = 1
CSLPImportTypes.CHANNEL   = 2

-- queuedImport = {}
--    queuedImport.id                       [ZoneIndex]
--    queuedImport.newInstanceIndex         [Zone to apply imported zone to]
--    queuedImport.importZoneType           [4 zone -- 5 arena]
--    queuedImport.communityIndex           [Zone to import]
--    queuedImport.communityInstanceId      [Instance id of imported zone]
--    queuedImport.preserveScriptedObjects
g_queuedImportRequests = {}



function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)

	-- server
	KEP_EventRegisterHandler(PROXYServerEvents.UgcZoneLiteEventHandler,           PROXYServerEvents.UgcZoneLiteEvent,           KEP.HIGH_PRIO)
	KEP_EventRegisterHandler(PROXYServerEvents.SpaceImportResponseEventHandler,   PROXYServerEvents.SpaceImportResponseEvent,   KEP.HIGH_PRIO)
	KEP_EventRegisterHandler(PROXYServerEvents.ChannelImportResponseEventHandler, PROXYServerEvents.ChannelImportResponseEvent, KEP.HIGH_PRIO)

	-- Communicates with Proxy MenuScript
	KEP_EventRegisterHandler(PROXYEvents.Handlers.ZoneList,        PROXYEvents.Request.ZoneList,        KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.SpawnList,       PROXYEvents.Request.SpawnList,       KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.CreateZone,      PROXYEvents.Request.CreateZone,      KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.DeleteZone,      PROXYEvents.Request.DeleteZone,      KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.RenameZone,      PROXYEvents.Request.RenameZone,      KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.SetDefaultZone,  PROXYEvents.Request.SetDefaultZone,  KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.AddSpawn,        PROXYEvents.Request.AddSpawn,        KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.DeleteSpawn,     PROXYEvents.Request.DeleteSpawn,     KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.SetInitialArena, PROXYEvents.Request.SetInitialArena, KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.UpdateSpawn,     PROXYEvents.Request.UpdateSpawn,     KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.ImportZone,      PROXYEvents.Request.ImportZone,      KEP.MED_PRIO)
	KEP_EventRegisterHandler(PROXYEvents.Handlers.ImportChannel,   PROXYEvents.Request.ImportChannel,   KEP.MED_PRIO)
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	-- server events
	KEP_EventRegister(PROXYServerEvents.UgcZoneLiteEvent,           KEP.HIGH_PRIO)
	KEP_EventRegister(PROXYServerEvents.SpaceImportEvent,           KEP.HIGH_PRIO)
	KEP_EventRegister(PROXYServerEvents.SpaceImportResponseEvent,   KEP.HIGH_PRIO)
	KEP_EventRegister(PROXYServerEvents.ChannelImportEvent,         KEP.HIGH_PRIO)
	KEP_EventRegister(PROXYServerEvents.ChannelImportResponseEvent, KEP.HIGH_PRIO)


	-- register events we listen for from menuscript
	KEP_EventRegister(PROXYEvents.Request.ZoneList,        KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.SpawnList,       KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.CreateZone,      KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.DeleteZone,      KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.RenameZone,      KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.SetDefaultZone,  KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.AddSpawn,        KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.DeleteSpawn,     KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.SetInitialArena, KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.UpdateSpawn,     KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.ImportZone,      KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Request.ImportChannel,   KEP.MED_PRIO)

	-- register events we fire back to menuscript
	KEP_EventRegister(PROXYEvents.Reply.ZoneList,        KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.SpawnList,       KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.CreateZone,      KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.DeleteZone,      KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.RenameZone,      KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.SetDefaultZone,  KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.AddSpawn,        KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.DeleteSpawn,     KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.SetInitialArena, KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.UpdateSpawn,     KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.ImportZone,      KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.ImportChannel,   KEP.MED_PRIO)
	KEP_EventRegister(PROXYEvents.Reply.ZoneStateChange, KEP.MED_PRIO)

	KEP_EventRegister(CSZoneLiteProxyEvents.ImportZoneSuccess,    KEP.HIGH_PRIO)
	KEP_EventRegister(CSZoneLiteProxyEvents.ImportChannelSuccess, KEP.HIGH_PRIO)
end

--------------------------------
-- List Actions
--------------------------------

function CSZLP_ZoneListRequestEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	KEP_Log("CSZLP_ZoneListRequestEventHandler Received eventid["..eventid.."] filter["..filter.."] objectid["..objectid.."]")

	local evtName = PROXYServerEvents.UgcZoneLiteEvent
	local evtFilterId = PROXYUGCFilterIds.REQ_LIST_ZONES

	-- craft/fire server event
	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventSetFilter(serverEvent, evtFilterId)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)

	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] filter["..evtFilterId.."]")
end

function CSZLP_SpawnListRequestEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local evtName = PROXYServerEvents.UgcZoneLiteEvent
	local evtFilterId = PROXYUGCFilterIds.REQ_LIST_SPAWNS

	-- unpack client event
	local eventData = {}
	eventData.id = objectid

	-- craft/fire server event
	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventSetFilter(serverEvent, evtFilterId)
	KEP_EventSetObjectId(serverEvent, eventData.id)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)

	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] filter["..evtFilterId.."] for zoneIndex["..tostring(eventData.id).."]")
end

--------------------------------
-- Zone Actions
--------------------------------

function CSZLP_CreateZoneRequestEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)

	local evtName = PROXYServerEvents.UgcZoneLiteEvent
	local evtFilterId = PROXYUGCFilterIds.REQ_CREATE_ZONE

	-- unpack client event
	local eventData = {}
	eventData.id = objectid
	eventData.importType          = KEP_EventDecodeNumber(event)
	eventData.newZoneName         = KEP_EventDecodeString(event)
	eventData.ugczlpZoneType      = KEP_EventDecodeNumber(event)
	eventData.ugczlpSpawnLoc      = KEP_EventDecodeNumber(event)
	eventData.occupancyLimit      = KEP_EventDecodeNumber(event)
	eventData.copyZoneIndex       = KEP_EventDecodeNumber(event)
	eventData.copyFlag            = KEP_EventDecodeNumber(event)
	eventData.importZoneType      = KEP_EventDecodeNumber(event)
	eventData.communityIndex      = KEP_EventDecodeNumber(event)
	eventData.communityInstanceId = KEP_EventDecodeNumber(event)

	-- Need to handle multi event for Communities
	-- so we queue the request
	if (eventData.importType == CSLPImportTypes.COMMUNITY) then
		local queuedImport = {}
		queuedImport.id = eventData.newZoneName
		queuedImport.newInstanceIndex           = -99 -- PlaceHolder since we need to wait until create succeeds
		queuedImport.importZoneType             = eventData.importZoneType
		queuedImport.communityIndex             = eventData.communityIndex
		queuedImport.communityInstanceId        = eventData.communityInstanceId
		queuedImport.preserveScriptedObjects    = 1 -- true
		g_queuedImportRequests[queuedImport.id] = queuedImport
		KEP_Log("CSZoneLiteProxy added queuedImport for zoneName["..tostring(queuedImport.id).."]")
	end

	-- monitor this request
	PROXY_UpdateMonitoredZones(eventData.newZoneName, CSZLPLastOperation.Result.PENDING, CSZLPLastOperation.Msg.CREATE)

	-- craft/fire server event
	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventSetFilter(serverEvent, evtFilterId)
	KEP_EventSetObjectId(serverEvent, eventData.id)               -- baseZoneIndex
	KEP_EventEncodeString(serverEvent, eventData.newZoneName)      -- new name alphanum+space, must be unique
	KEP_EventEncodeNumber(serverEvent, eventData.ugczlpZoneType)   -- 4 (perm or arena) or 5 (arena only)
	KEP_EventEncodeNumber(serverEvent, eventData.ugczlpSpawnLoc)   -- 0 (use zone default) 1 (use player location)
	KEP_EventEncodeNumber(serverEvent, eventData.occupancyLimit)   -- occupancy limit, 0 to copy from baseZone
	KEP_EventEncodeNumber(serverEvent, eventData.copyZoneIndex)    -- zone to copy from (0 if no copy needed)
	KEP_EventEncodeNumber(serverEvent, eventData.copyFlag)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)

	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] filter["..evtFilterId.."] for baseZoneIndex["..tostring(eventData.id).."] newZonename["..tostring(eventData.newZoneName).."] copyZoneIndex["..tostring(eventData.copyZoneIndex).."]")
end

function CSZLP_DeleteZoneRequestEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local evtName = PROXYServerEvents.UgcZoneLiteEvent
	local evtFilterId = PROXYUGCFilterIds.REQ_DELETE_ZONE

	-- unpack client event
	local eventData = {}
	eventData.id = objectid
	eventData.ugczlpDynamicObjects = KEP_EventDecodeNumber(event)

	-- monitor this request
	PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.PENDING, CSZLPLastOperation.Msg.DELETE)

	-- craft/fire server event
	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventSetObjectId(serverEvent, eventData.id)
	KEP_EventSetFilter(serverEvent, evtFilterId)
	KEP_EventEncodeNumber(serverEvent, eventData.ugczlpDynamicObjects)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)

	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] filter["..evtFilterId.."] for zoneIndex["..tostring(eventData.id).."] ugczlpDynamicObjects["..tostring(eventData.ugczlpDynamicObjects).."]")
end

function CSZLP_RenameZoneRequestEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local evtName = PROXYServerEvents.UgcZoneLiteEvent
	local evtFilterId = PROXYUGCFilterIds.REQ_RENAME_ZONE

	-- unpack client event
	local eventData = {}
	eventData.id = objectid
	eventData.newZoneName = KEP_EventDecodeString(event)

	-- monitor this request
	PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.PENDING, CSZLPLastOperation.Msg.RENAME)

	-- craft/fire server event
	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventSetFilter(serverEvent, evtFilterId)
	KEP_EventSetObjectId(serverEvent, eventData.id)
	KEP_EventEncodeString(serverEvent, eventData.newZoneName)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)

	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] filter["..evtFilterId.."] for zoneIndex["..tostring(eventData.id).."] newZoneName["..tostring(eventData.newZoneName).."]")
end

function CSZLP_SetDefaultZoneRequestEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local evtName = PROXYServerEvents.UgcZoneLiteEvent
	local evtFilterId = PROXYUGCFilterIds.REQ_SET_DEFAULT_ZONE

	-- unpack client event
	local eventData = {}
	eventData.id = objectid

	-- monitor this request
	PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.PENDING, CSZLPLastOperation.Msg.SETDEFAULT)

	-- craft/fire server event
	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventSetFilter(serverEvent, evtFilterId)
	KEP_EventSetObjectId(serverEvent, eventData.id)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)

	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] filter["..tostring(evtFilterId).."] for zoneIndex["..tostring(eventData.id).."]")
end

function CSZLP_AddSpawnRequestEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local evtName = PROXYServerEvents.UgcZoneLiteEvent
	local evtFilterId = PROXYUGCFilterIds.REQ_ADD_SPAWN

	-- unpack client event
	local eventData = {}
	eventData.id = objectid
	eventData.name          = KEP_EventDecodeString(event)
	eventData.usePlayerPos  = KEP_EventDecodeNumber(event)
	eventData.x             = KEP_EventDecodeNumber(event)
	eventData.y             = KEP_EventDecodeNumber(event)
	eventData.z             = KEP_EventDecodeNumber(event)
	eventData.r             = KEP_EventDecodeNumber(event)

	-- craft/fire server event
	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventSetFilter(serverEvent, evtFilterId)
	KEP_EventSetObjectId(serverEvent, eventData.id)
	KEP_EventEncodeString(serverEvent, eventData.name)
	KEP_EventEncodeNumber(serverEvent, eventData.usePlayerPos)
	KEP_EventEncodeNumber(serverEvent, eventData.x)
	KEP_EventEncodeNumber(serverEvent, eventData.y)
	KEP_EventEncodeNumber(serverEvent, eventData.z)
	KEP_EventEncodeNumber(serverEvent, eventData.r)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)

	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] filter["..evtFilterId.."] for zoneIndex["..tostring(eventData.id).."]")
end

function CSZLP_DeleteSpawnRequestEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local evtName = PROXYServerEvents.UgcZoneLiteEvent
	local evtFilterId = PROXYUGCFilterIds.REQ_DELETE_SPAWN

	-- unpack client event
	local eventData = {}
	eventData.id = objectid
	eventData.spawnId = KEP_EventDecodeNumber(event)

	-- craft/fire server event
	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventSetFilter(serverEvent, evtFilterId)
	KEP_EventSetObjectId(serverEvent, eventData.id)
	KEP_EventEncodeNumber(serverEvent, eventData.spawnId)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)

	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] filter["..evtFilterId.."] for zoneIndex["..tostring(eventData.id).."] spawnId["..tostring(eventData.spawnId).."]")
end

function CSZLP_UpdateSpawnRequestEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local evtName = PROXYServerEvents.UgcZoneLiteEvent
	local evtFilterId = PROXYUGCFilterIds.REQ_UPDATE_SPAWN

	-- unpack client event
	local eventData = {}
	eventData.id = objectid
	eventData.zoneIndex     = KEP_EventDecodeNumber(event)
	eventData.zoneName      = KEP_EventDecodeString(event)
	eventData.x             = KEP_EventDecodeNumber(event)
	eventData.y             = KEP_EventDecodeNumber(event)
	eventData.z             = KEP_EventDecodeNumber(event)
	eventData.r             = KEP_EventDecodeNumber(event)

	-- craft/fire server event
	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventSetFilter(serverEvent, evtFilterId)
	KEP_EventSetObjectId(serverEvent, eventData.id)
	KEP_EventEncodeNumber(serverEvent, eventData.zoneIndex)
	KEP_EventEncodeString(serverEvent, eventData.zoneName)
	KEP_EventEncodeNumber(serverEvent, eventData.x)
	KEP_EventEncodeNumber(serverEvent, eventData.y)
	KEP_EventEncodeNumber(serverEvent, eventData.z)
	KEP_EventEncodeNumber(serverEvent, eventData.r)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)

	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] filter["..evtFilterId.."] for spawnPointId["..eventData.id.."] zoneIndex["..tostring(eventData.zoneIndex).."]" )
end



function CSZLP_ImportZoneRequestEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local evtName = PROXYServerEvents.UgcZoneLiteEvent
	local evtFilterId = PROXYUGCFilterIds.REQ_RE_CREATE_ZONE

	-- unpack client event
	local eventData = {}
	eventData.id = objectid
	eventData.importType          = KEP_EventDecodeNumber(event)
	eventData.baseZoneIndex       = KEP_EventDecodeNumber(event)
	eventData.dynamicObjects      = KEP_EventDecodeNumber(event)
	eventData.copyZoneIndex       = KEP_EventDecodeNumber(event)
	eventData.copyFlag            = KEP_EventDecodeNumber(event)
	eventData.importZoneType      = KEP_EventDecodeNumber(event)
	eventData.communityIndex      = KEP_EventDecodeNumber(event)
	eventData.communityInstanceId = KEP_EventDecodeNumber(event)

	-- Need to handle multi event for Communities
	-- so we queue the request
	if (eventData.importType == CSLPImportTypes.COMMUNITY) then
		local queuedImport = {}
		queuedImport.id = eventData.id
		queuedImport.newInstanceIndex        = -99 -- PlaceHolder since we need to wait until recreate succeeds
		queuedImport.importZoneType          = eventData.importZoneType
		queuedImport.communityIndex          = eventData.communityIndex
		queuedImport.communityInstanceId     = eventData.communityInstanceId
		queuedImport.preserveScriptedObjects = eventData.dynamicObjects
		g_queuedImportRequests[queuedImport.id] = queuedImport
		KEP_Log("CSZoneLiteProxy added queuedImport for zoneIndex["..tostring(eventData.id).."]")
	end

	-- monitor this request
	PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.PENDING, CSZLPLastOperation.Msg.IMPORT)

	-- craft/fire server event
	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventSetFilter(serverEvent, evtFilterId)
	KEP_EventSetObjectId(serverEvent, eventData.id)
	KEP_EventEncodeNumber(serverEvent, eventData.baseZoneIndex)
	KEP_EventEncodeNumber(serverEvent, eventData.dynamicObjects)
	KEP_EventEncodeNumber(serverEvent, eventData.copyZoneIndex)
	KEP_EventEncodeNumber(serverEvent, eventData.copyFlag)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)

	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] filter["..evtFilterId.."] for zoneIndex["..eventData.id.."]")
end

function PROXY_ImportCommunityRequestEvent(oldZoneIndex, newZoneIndex)
	local evtName = PROXYServerEvents.SpaceImportEvent

	if (g_queuedImportRequests[oldZoneIndex] ~= nil) then
		local record = g_queuedImportRequests[oldZoneIndex]
		record.newInstanceIndex = InstanceId_MakeId(newZoneIndex, 0, record.importZoneType)

		-- craft/fire server event
		local serverEvent = KEP_EventCreate(evtName)
		KEP_EventEncodeNumber(serverEvent, record.newInstanceIndex)    -- Zone to apply imported zone to
		KEP_EventEncodeNumber(serverEvent, record.communityIndex)      -- Zone to import
		KEP_EventEncodeNumber(serverEvent, record.communityInstanceId) -- Instance id of imported zone
		KEP_EventEncodeNumber(serverEvent, record.preserveScriptedObjects)
		KEP_EventAddToServer(serverEvent)
		KEP_EventQueue(serverEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] for zoneIndex["..oldZoneIndex.."]")

		g_queuedImportRequests[oldZoneIndex] = nil
		g_queuedImportRequests[newZoneIndex] = record
	end
end

function CSZLP_ImportChannelRequestEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local evtName = PROXYServerEvents.ChannelImportEvent

	-- unpack client event
	local eventData = {}
	eventData.id = objectid      -- dstZoneDef.zoneIndex
	eventData.dstZoneIndex       = objectid
	eventData.dstInstanceId      = KEP_EventDecodeNumber(event)
	eventData.baseZoneIndexPlain = KEP_EventDecodeNumber(event)
	eventData.srcZoneIndex       = KEP_EventDecodeNumber(event)
	eventData.srcInstanceId      = KEP_EventDecodeNumber(event)
	eventData.maintainScripts    = KEP_EventDecodeNumber(event)

	-- monitor this request
	PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.PENDING, CSZLPLastOperation.Msg.IMPORT)

	local serverEvent = KEP_EventCreate(evtName)
	KEP_EventEncodeNumber(serverEvent, eventData.dstZoneIndex)
	KEP_EventEncodeNumber(serverEvent, eventData.dstInstanceId)
	KEP_EventEncodeNumber(serverEvent, eventData.baseZoneIndexPlain)
	KEP_EventEncodeNumber(serverEvent, eventData.srcZoneIndex)
	KEP_EventEncodeNumber(serverEvent, eventData.srcInstanceId)
	KEP_EventEncodeNumber(serverEvent, eventData.maintainScripts)
	KEP_EventAddToServer(serverEvent)
	KEP_EventQueue(serverEvent)
	KEP_Log("CSZoneLiteProxy Dispatched["..evtName.."] for objectid["..objectid.."]")

end



function CSZLP_UgcZoneLiteEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	KEP_Log("CSZoneLiteProxy Received["..PROXYServerEvents.UgcZoneLiteEvent.."] filter["..filter.."] objectid["..objectid.."]")

	-- List Actions
	if filter == PROXYUGCFilterIds.REPLY_ZONES_LIST then
		local zoneCount = 0

		-- Unpack server event
		local retval, zoneList = Proxy_UnpackZoneList(event)

		if (retval == 1) then
			-- Filter bad KanevaZones + Add Monitoring info to list
			zoneList = PROXY_ProcessZones(zoneList)
			-- craft/fire client event
			zoneCount = #zoneList
		end

		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.ZoneList)
		KEP_EventEncodeNumber(clientEvent, retval)
		KEP_EventEncodeNumber(clientEvent, zoneCount)
		for i=1,zoneCount do
			KEP_EventEncodeNumber(clientEvent, zoneList[i].zoneIndex)
			KEP_EventEncodeString(clientEvent, zoneList[i].zoneName)
			KEP_EventEncodeString(clientEvent, zoneList[i].exgName)
			KEP_EventEncodeNumber(clientEvent, zoneList[i].isDefault)
			KEP_EventEncodeNumber(clientEvent, zoneList[i].isKanevaSupplied)
			KEP_EventEncodeNumber(clientEvent, zoneList[i].lastOpResult)
			KEP_EventEncodeString(clientEvent, zoneList[i].lastOpMsg)
		end

		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.ZoneList.."]")
	elseif filter == PROXYUGCFilterIds.REPLY_SPAWNS_LIST then

		-- Unpack server event
		local retval, spawnPointList = Proxy_UnpackSpawnList(event)

		-- craft/fire client event
		local spawnCount = #spawnPointList
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.SpawnList)
		KEP_EventSetObjectId(clientEvent, objectid)
		KEP_EventEncodeNumber(clientEvent, retval)
		KEP_EventEncodeNumber(clientEvent, spawnCount)
		for i=1,spawnCount do
			KEP_EventEncodeNumber(clientEvent, spawnPointList[i].id)
			KEP_EventEncodeString(clientEvent, spawnPointList[i].name)
			KEP_EventEncodeNumber(clientEvent, spawnPointList[i].x)
			KEP_EventEncodeNumber(clientEvent, spawnPointList[i].y)
			KEP_EventEncodeNumber(clientEvent, spawnPointList[i].z)
			KEP_EventEncodeNumber(clientEvent, spawnPointList[i].r)
			KEP_EventEncodeNumber(clientEvent, spawnPointList[i].lastOpResult)
			KEP_EventEncodeString(clientEvent, spawnPointList[i].lastOpMsg)
		end
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.SpawnList.."]")

		-- Zone Actions
	elseif filter == PROXYUGCFilterIds.REPLY_CREATE_ZONE then
		-- Unpack server event
		local eventData = {}
		eventData.id = objectid
		eventData.retval   = KEP_EventDecodeNumber(event)
		eventData.zoneName = KEP_EventDecodeString(event)
		eventData.msg      = KEP_EventDecodeString(event)

		-- check for multi part request and upate monitored zones
		if (eventData.retval == 0) then

			-- operation failed remove any lingering multi part requests from queue
			g_queuedImportRequests[eventData.zoneName] = nil
			PROXY_UpdateMonitoredZones(eventData.zoneName, CSZLPLastOperation.Result.FAILED, CSZLPLastOperation.Msg.CREATE)

			-- append lastOp fields to event record
			Proxy_AddLastOpFieldsToRecord(eventData.zoneName, eventData)

			-- craft/fire client event
			local clientEvent = KEP_EventCreate(PROXYEvents.Reply.CreateZone)
			KEP_EventSetObjectId(clientEvent, eventData.id)
			KEP_EventEncodeNumber(clientEvent, eventData.retval)
			KEP_EventEncodeString(clientEvent, eventData.zoneName)
			KEP_EventEncodeString(clientEvent, eventData.msg)
			KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
			KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
			KEP_EventQueue(clientEvent)
			KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.CreateZone.."]")
		else
			-- Is it a multi part request
			if (g_queuedImportRequests[eventData.zoneName] ~= nil) then

				-- rekey monitored zones and add data result
				g_monitoredZones[eventData.zoneName] = nil
				PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.IMPORTING, CSZLPLastOperation.Msg.CREATE)

				-- append lastOp fields to event record
				Proxy_AddLastOpFieldsToRecord(eventData.id, eventData)

				-- craft/fire client event
				local clientEvent = KEP_EventCreate(PROXYEvents.Reply.CreateZone)
				KEP_EventSetObjectId(clientEvent, eventData.id)
				KEP_EventEncodeNumber(clientEvent, eventData.retval)
				KEP_EventEncodeString(clientEvent, eventData.zoneName)
				KEP_EventEncodeString(clientEvent, eventData.msg)
				KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
				KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
				KEP_EventQueue(clientEvent)
				KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.CreateZone.."]")

				-- rekey queuedImport id (does not get updated by importrequest
				g_queuedImportRequests[eventData.zoneName].id = eventData.id
				PROXY_ImportCommunityRequestEvent(eventData.zoneName, eventData.id)
			else
				PROXY_UpdateMonitoredZones(eventData.zoneName, CSZLPLastOperation.Result.SUCCESS, CSZLPLastOperation.Msg.CREATE)

				-- append lastOp fields to event record
				Proxy_AddLastOpFieldsToRecord(eventData.zoneName, eventData)

				-- craft/fire client event
				local clientEvent = KEP_EventCreate(PROXYEvents.Reply.CreateZone)
				KEP_EventSetObjectId(clientEvent, eventData.id)
				KEP_EventEncodeNumber(clientEvent, eventData.retval)
				KEP_EventEncodeString(clientEvent, eventData.zoneName)
				KEP_EventEncodeString(clientEvent, eventData.msg)
				KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
				KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
				KEP_EventQueue(clientEvent)
				KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.CreateZone.."]")
			end
		end
	elseif filter == PROXYUGCFilterIds.REPLY_DELETE_ZONE then

		-- Unpack server event
		local eventData = {}
		eventData.id = objectid
		eventData.retval = KEP_EventDecodeNumber(event)
		eventData.msg    = KEP_EventDecodeString(event)
		if (eventData.retval == 1) then
			-- operation succeeded remove entry from monitored queue
			g_monitoredZones[eventData.id] = nil
		else
			-- update monitored zones
			PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.FAILED, CSZLPLastOperation.Msg.DELETE)
		end

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(eventData.id, eventData)

		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.DeleteZone)
		KEP_EventSetObjectId(clientEvent, eventData.id)
		KEP_EventEncodeNumber(clientEvent, eventData.retval)
		KEP_EventEncodeString(clientEvent, eventData.msg)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.DeleteZone.."]")
	elseif filter == PROXYUGCFilterIds.REPLY_RENAME_ZONE then

		-- Unpack server event
		local eventData = {}
		eventData.id = objectid
		eventData.retval      = KEP_EventDecodeNumber(event)
		eventData.newZoneName = KEP_EventDecodeString(event)
		eventData.msg         = KEP_EventDecodeString(event)

		-- update monitored zones
		if (eventData.retval == 1) then
			PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.SUCCESS, CSZLPLastOperation.Msg.RENAME)
		else
			PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.FAILED, CSZLPLastOperation.Msg.RENAME)
		end

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(eventData.id, eventData)

		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.RenameZone)
		KEP_EventSetObjectId(clientEvent, eventData.id)
		KEP_EventEncodeNumber(clientEvent, eventData.retval)
		KEP_EventEncodeString(clientEvent, eventData.newZoneName)
		KEP_EventEncodeString(clientEvent, eventData.msg)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.RenameZone.."]")
	elseif filter == PROXYUGCFilterIds.REPLY_SET_DEFAULT_ZONE then

		-- Unpack server event
		local eventData = {}
		eventData.id = objectid
		eventData.retval   = KEP_EventDecodeNumber(event)
		eventData.msg      = KEP_EventDecodeString(event)

		-- upate monitored zones
		if (eventData.retval == 1) then
			PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.SUCCESS, CSZLPLastOperation.Msg.SETDEFAULT)
		else
			PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.FAILED, CSZLPLastOperation.Msg.SETDEFAULT)
		end

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(eventData.id, eventData)

		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.SetDefaultZone)
		KEP_EventSetObjectId(clientEvent, eventData.id)
		KEP_EventEncodeNumber(clientEvent, eventData.retval)
		KEP_EventEncodeString(clientEvent, eventData.msg)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.SetDefaultZone.."]")
	elseif filter == PROXYUGCFilterIds.REPLY_RE_CREATE_ZONE then

		-- Unpack server event
		local eventData = {}
		eventData.id = objectid
		eventData.retval       = KEP_EventDecodeNumber(event)
		eventData.newZoneIndex = KEP_EventDecodeNumber(event)
		eventData.msg          = KEP_EventDecodeString(event)

		-- check for multi part request and upate monitored zones
		if (eventData.retval == 0) then

			-- operation failed remove any lingering multi part requests from queue
			g_queuedImportRequests[eventData.id] = nil
			PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.FAILED, CSZLPLastOperation.Msg.IMPORT)

			-- append lastOp fields to event record
			Proxy_AddLastOpFieldsToRecord(eventData.id, eventData)

			-- craft/fire client event
			local clientEvent = KEP_EventCreate(PROXYEvents.Reply.ImportZone)
			KEP_EventSetObjectId(clientEvent, eventData.id)
			KEP_EventEncodeNumber(clientEvent, eventData.retval)
			KEP_EventEncodeNumber(clientEvent, eventData.newZoneIndex)
			KEP_EventEncodeString(clientEvent, eventData.msg)
			KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
			KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
			KEP_EventQueue(clientEvent)
			KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.ImportZone.."]")
		else

			-- Is it a multi part request
			if (g_queuedImportRequests[eventData.id] ~= nil) then

				-- update monitored data result
				PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.IMPORTING, CSZLPLastOperation.Msg.IMPORT)

				-- append lastOp fields to event record
				Proxy_AddLastOpFieldsToRecord(eventData.id, eventData)

				-- craft/fire client event
				local clientEvent = KEP_EventCreate(PROXYEvents.Reply.ImportZone)
				KEP_EventSetObjectId(clientEvent, eventData.id)
				KEP_EventEncodeNumber(clientEvent, eventData.retval)
				KEP_EventEncodeNumber(clientEvent, eventData.newZoneIndex)
				KEP_EventEncodeString(clientEvent, eventData.msg)
				KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
				KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
				KEP_EventQueue(clientEvent)
				KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.ImportZone.."]")

				-- clear old zoneinfo and add for newZoneIndex
				g_monitoredZones[eventData.id] = nil
				PROXY_UpdateMonitoredZones(eventData.newZoneIndex, CSZLPLastOperation.Result.IMPORTING, CSZLPLastOperation.Msg.IMPORT)
				PROXY_ImportCommunityRequestEvent(eventData.id, eventData.newZoneIndex)
			else
				PROXY_UpdateMonitoredZones(eventData.id, CSZLPLastOperation.Result.SUCCESS, CSZLPLastOperation.Msg.IMPORT)

				-- append lastOp fields to event record
				Proxy_AddLastOpFieldsToRecord(eventData.id, eventData)

				-- craft/fire client event
				local clientEvent = KEP_EventCreate(PROXYEvents.Reply.ImportZone)
				KEP_EventSetObjectId(clientEvent, eventData.id)
				KEP_EventEncodeNumber(clientEvent, eventData.retval)
				KEP_EventEncodeNumber(clientEvent, eventData.newZoneIndex)
				KEP_EventEncodeString(clientEvent, eventData.msg)
				KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
				KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
				KEP_EventQueue(clientEvent)
				KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.ImportZone.."]")

				-- craft fire import success message
				local clientImportEvent = KEP_EventCreate(CSZoneLiteProxyEvents.ImportZoneSuccess)
				KEP_EventSetObjectId(clientImportEvent,  eventData.id)           -- zoneIndex
				KEP_EventEncodeNumber(clientImportEvent, eventData.newZoneIndex) -- newZoneIndex
				KEP_EventQueue(clientImportEvent)
				KEP_Log("CSZoneLiteProxy Dispatched ["..CSZoneLiteProxyEvents.ImportZoneSuccess.."] for zoneIndex["..tostring(originalZoneIndex).."] newZoneIndex["..tostring(zoneIndex).."]")
			end
		end

		-- Spawn Actions
	elseif filter == PROXYUGCFilterIds.REPLY_ADD_SPAWN then

		-- Unpack server event
		local eventData = {}
		eventData.id = objectid
		eventData.retval    = KEP_EventDecodeNumber(event)
		eventData.spawnId   = KEP_EventDecodeNumber(event)
		eventData.spawnName = KEP_EventDecodeString(event)
		eventData.msg       = KEP_EventDecodeString(event)

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(eventData.id, eventData)

		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.AddSpawn)
		KEP_EventSetObjectId(clientEvent, eventData.id)
		KEP_EventEncodeNumber(clientEvent, eventData.retval)
		KEP_EventEncodeNumber(clientEvent, eventData.spawnId)
		KEP_EventEncodeString(clientEvent, eventData.spawnName)
		KEP_EventEncodeString(clientEvent, eventData.msg)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.AddSpawn.."]")
	elseif filter == PROXYUGCFilterIds.REPLY_DELETE_SPAWN then

		-- Unpack server event
		local eventData = {}
		eventData.id = objectid
		eventData.retval   = KEP_EventDecodeNumber(event)
		eventData.spawnId  = KEP_EventDecodeNumber(event)
		eventData.msg      = KEP_EventDecodeString(event)

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(eventData.id, eventData)

		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.DeleteSpawn)
		KEP_EventSetObjectId(clientEvent, eventData.id)
		KEP_EventEncodeNumber(clientEvent, eventData.retval)
		KEP_EventEncodeNumber(clientEvent, eventData.spawnId)
		KEP_EventEncodeString(clientEvent, eventData.msg)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.DeleteSpawn.."]")
	elseif filter == PROXYUGCFilterIds.REPLY_UPDATE_SPAWN then

		-- Unpack server event
		local eventData = {}
		eventData.id = objectid
		eventData.retval   = KEP_EventDecodeNumber(event)
		eventData.spawnId  = KEP_EventDecodeNumber(event)
		eventData.msg      = KEP_EventDecodeString(event)

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(eventData.id, eventData)

		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.UpdateSpawn)
		KEP_EventSetObjectId(clientEvent, eventData.id)
		KEP_EventEncodeNumber(clientEvent, eventData.retval)
		KEP_EventEncodeNumber(clientEvent, eventData.spawnId)
		KEP_EventEncodeString(clientEvent, eventData.msg)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.UpdateSpawn.."]")

	else
		KEP_Log("CSZoneLiteProxy WARNING Unknown filter["..filter.."] Received -- params: fromNetid["..fromNetid.."] eventid["..eventid.."]")
	end
end

function CSZLP_SpaceImportResponseEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- Unpack server event
	local eventData = {}
	eventData.id = objectid
	eventData.retval = KEP_EventDecodeNumber(event)
	eventData.status = KEP_EventDecodeString(event)

	-- return values from server
	-- #define OP_SUCCESS    0
	-- #define OP_INPROGRESS 1
	-- #define OP_FAILED     2

	local zoneIndex = -1
	for k, entry in pairs(g_queuedImportRequests) do
		if (entry.newInstanceIndex == eventData.id) then
			zoneIndex = k
			break
		end
	end
	if (zoneIndex == -1) then
		KEP_Log("CSZoneLiteProxy CSZLP_SpaceImportResponseEventHandler ERROR no matching zoneIndex for eventData.id["..eventData.id.."]")
		return
	end

	-- Import Completed
	if (eventData.retval == 0) then

		-- update monitored zones
		PROXY_UpdateMonitoredZones(zoneIndex, CSZLPLastOperation.Result.SUCCESS, CSZLPLastOperation.Msg.IMPORT)

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(zoneIndex, eventData)

		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.ImportZone)
		KEP_EventSetObjectId(clientEvent, zoneIndex)
		KEP_EventEncodeNumber(clientEvent, 1) -- true
		KEP_EventEncodeNumber(clientEvent, zoneIndex)
		KEP_EventEncodeString(clientEvent, eventData.status)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.ImportZone.."]")

		-- if the request was queued
		local originalZoneIndex = zoneIndex
		if (g_queuedImportRequests[zoneIndex] ~= nil) then
			originalZoneIndex = g_queuedImportRequests[zoneIndex].id
		end

		-- craft fire import success message
		local clientImportEvent = KEP_EventCreate(CSZoneLiteProxyEvents.ImportZoneSuccess)
		KEP_EventSetObjectId(clientImportEvent,  originalZoneIndex)  -- zoneIndex
		KEP_EventEncodeNumber(clientImportEvent, zoneIndex)          -- newZoneIndex
		KEP_EventQueue(clientImportEvent)
		KEP_Log("CSZoneLiteProxy Dispatched ["..CSZoneLiteProxyEvents.ImportZoneSuccess.."] for zoneIndex["..tostring(originalZoneIndex).."] newZoneIndex["..tostring(zoneIndex).."]")

		-- clear request from queue
		g_queuedImportRequests[zoneIndex] = nil

		-- Import in progress
	elseif (eventData.retval == 1) then

		-- update monitored zones
		PROXY_UpdateMonitoredZones(zoneIndex, CSZLPLastOperation.Result.IMPORTING, "("..eventData.status..")")

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(zoneIndex, eventData)

		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.ZoneStateChange)
		KEP_EventSetObjectId(clientEvent, zoneIndex)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.ZoneStateChange.."]")

	-- Import failed
	elseif (eventData.retval == 2) then

		-- update monitored zones
		PROXY_UpdateMonitoredZones(zoneIndex, CSZLPLastOperation.Result.FAILED, CSZLPLastOperation.Msg.IMPORT)

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(zoneIndex, eventData)

		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.ImportZone)
		KEP_EventSetObjectId(clientEvent, zoneIndex)
		KEP_EventEncodeNumber(clientEvent, 0) -- false
		KEP_EventEncodeNumber(clientEvent, zoneIndex)
		KEP_EventEncodeString(clientEvent, eventData.status)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.ImportZone.."]")
	else
		KEP_Log("CSZoneLiteProxy["..PROXYServerEvents.SpaceImportResponseEvent.."] WARNING unkown return value["..eventData.retval.."] objectid["..objectid.."]")
		return
	end
end

function CSZLP_ChannelImportResponseEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Unpack server event
	local eventData = {}
	eventData.id = objectid
	eventData.retval       = KEP_EventDecodeNumber(event)
	eventData.status       = KEP_EventDecodeString(event)
	eventData.newZoneIndex = KEP_EventDecodeNumber(event)

	-- return values from server
	-- #define OP_SUCCESS    0
	-- #define OP_INPROGRESS 1
	-- #define OP_FAILED     2
	local zoneIndex = objectid

	-- Import Completed
	if (eventData.retval == 0) then

		-- update monitored zones
		PROXY_UpdateMonitoredZones(zoneIndex, CSZLPLastOperation.Result.SUCCESS, CSZLPLastOperation.Msg.IMPORT)

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(zoneIndex, eventData)

		-- craft fire import success message
		local clientImportEvent = KEP_EventCreate(CSZoneLiteProxyEvents.ImportChannelSuccess)
		KEP_EventSetObjectId(clientImportEvent,  zoneIndex)
		KEP_EventEncodeNumber(clientImportEvent, eventData.newZoneIndex)
		KEP_EventQueue(clientImportEvent)
		KEP_Log("CSZoneLiteProxy Dispatched ["..CSZoneLiteProxyEvents.ImportChannelSuccess.."] for channel zoneIndex["..tostring(zoneIndex).."] newZoneIndex["..tostring(eventData.newZoneIndex).."]")


	-- Import in progress
	elseif (eventData.retval == 1) then

		-- update monitored zones
		PROXY_UpdateMonitoredZones(zoneIndex, CSZLPLastOperation.Result.IMPORTING, "("..eventData.status..")")

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(zoneIndex, eventData)

		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.ZoneStateChange)
		KEP_EventSetObjectId(clientEvent, zoneIndex)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.ZoneStateChange.."]")

	-- Import failed
	elseif (eventData.retval == 2) then

		-- update monitored zones
		PROXY_UpdateMonitoredZones(zoneIndex, CSZLPLastOperation.Result.FAILED, CSZLPLastOperation.Msg.IMPORT)

		-- append lastOp fields to event record
		Proxy_AddLastOpFieldsToRecord(zoneIndex, eventData)


		-- craft/fire client event
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.ImportChannel)
		KEP_EventSetObjectId(clientEvent, eventData.id)
		KEP_EventEncodeNumber(clientEvent, 0) -- false
		KEP_EventEncodeNumber(clientEvent, eventData.newZoneIndex)
		KEP_EventEncodeString(clientEvent, eventData.status)
		KEP_EventEncodeNumber(clientEvent, eventData.lastOpResult)
		KEP_EventEncodeString(clientEvent, eventData.lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched ["..PROXYEvents.Reply.ImportChannel.."]")
	else
		KEP_Log("CSZoneLiteProxy["..PROXYServerEvents.ChannelImportResponseEvent.."] WARNING unkown return value["..eventData.retval.."] objectid["..objectid.."]")
		return
	end
end



function Proxy_UnpackZoneList(event)
	local zoneList = {}
	if (event == nil) then
		return 0 -- false
	end
	local listCount = KEP_EventDecodeNumber(event)
	for i=1,listCount do
		local newEntry = {}
		newEntry.zoneIndex        = KEP_EventDecodeNumber(event)
		newEntry.zoneName         = KEP_EventDecodeString(event)
		newEntry.exgName          = KEP_EventDecodeString(event)
		newEntry.isDefault        = KEP_EventDecodeNumber(event)

		-- validate zone before caching
		-- if any of these trigger clear out the entire list
		if (type(newEntry.zoneIndex) ~= "number") then
			KEP_Log("CSZoneListProxy::UnpackZoneList ERROR unpacking event newEntry.zoneIndex["..tostring(newEntry.zoneIndex).."]")
			Proxy_TraceDump(newEntry)
			return 0 -- false
		elseif (type(newEntry.zoneName) ~= "string" or newEntry.zoneName == "") then
			KEP_Log("CSZoneListProxy::UnpackZoneList ERROR unpacking event newEntry.zoneName["..tostring(newEntry.zoneName).."]")
			Proxy_TraceDump(newEntry)
			return 0 -- false
		elseif (type(newEntry.exgName) ~= "string") then
			KEP_Log("CSZoneListProxy::UnpackZoneList ERROR unpacking event newEntry.exgName["..tostring(newEntry.exgName).."]")
			Proxy_TraceDump(newEntry)
			return 0 -- false
		elseif (type(newEntry.isDefault) ~= "number") then
			KEP_Log("CSZoneListProxy::UnpackZoneList ERROR unpacking event newEntry.isDefault["..tostring(newEntry.isDefault).."]")
			Proxy_TraceDump(newEntry)
			return 0 -- false
		else

			newEntry.isKanevaSupplied = 1 -- true
			if (newEntry.zoneIndex >= 1000 or newEntry.zoneIndex == 55) then
				newEntry.isKanevaSupplied = 0 -- false
			end

			table.insert(zoneList, newEntry)
		end
	end
	return 1, zoneList
end

function Proxy_UnpackSpawnList(event, zoneIndex)
	local spawnPointList = {}
	if (event == nil) then
		return 0 -- false
	end

	-- list of spawn points, with count and the zoneIndex (plain) first
	local listCount = KEP_EventDecodeNumber(event)

	for i=1,listCount do
		local newEntry = {}
		newEntry.id   = KEP_EventDecodeNumber(event)
		newEntry.name = KEP_EventDecodeString(event)
		newEntry.x    = KEP_EventDecodeNumber(event)
		newEntry.y    = KEP_EventDecodeNumber(event)
		newEntry.z    = KEP_EventDecodeNumber(event)
		newEntry.r    = KEP_EventDecodeNumber(event)

		-- validate spawnPoint before inserting
		if (type(newEntry.id) ~= "number" or newEntry.id <= 0) then
			KEP_Log("CSZoneListProxy::UnpackSpawnList ERROR unpacking event newEntry.id["..tostring(newEntry.id).."]")
			Proxy_TraceDump(newEntry)
			return 0 -- false
		elseif (type(newEntry.name) ~= "string" or newEntry.name == "") then
			KEP_Log("CSZoneListProxy::UnpackSpawnList ERROR unpacking event newEntry.name["..tostring(newEntry.name).."]")
			Proxy_TraceDump(newEntry)
			return 0 -- false
		elseif (type(newEntry.x) ~= "number") then
			KEP_Log("CSZoneListProxy::UnpackSpawnList ERROR unpacking event newEntry.x["..tostring(newEntry.x).."]")
			Proxy_TraceDump(newEntry)
			return 0 -- false
		elseif (type(newEntry.y) ~= "number") then
			KEP_Log("CSZoneListProxy::UnpackSpawnList ERROR unpacking event newEntry.y["..tostring(newEntry.y).."]")
			Proxy_TraceDump(newEntry)
			return 0 -- false
		elseif (type(newEntry.z) ~= "number") then
			KEP_Log("CSZoneListProxy::UnpackSpawnList ERROR unpacking event newEntry.z["..tostring(newEntry.z).."]")
			Proxy_TraceDump(newEntry)
			return 0 -- false
		elseif (type(newEntry.r) ~= "number") then
			KEP_Log("CSZoneListProxy::UnpackSpawnList ERROR unpacking event newEntry.r["..tostring(newEntry.r).."]")
			Proxy_TraceDump(newEntry)
			return 0 -- false
		else

			-- spawns are not monitored atm
			newEntry.lastOpResult = CSZLPLastOperation.Result.NONE
			newEntry.lastOpMsg    = CSZLPLastOperation.Msg.NONE
			-- spawns are not monitored atm

			table.insert(spawnPointList, newEntry)
		end
	end
	return 1, spawnPointList
end

-- filter out invalid zones comming back from server
-- add monitoring info to zones
function PROXY_ProcessZones(tblZoneList)
	local filterdZones = {}
	for _,zoneData in pairs(tblZoneList) do
		if (zoneData.isKanevaSupplied == 0) then
			Proxy_AddLastOpFieldsToRecord(zoneData.zoneIndex, zoneData)
			table.insert(filterdZones, zoneData)
		else
			for _,v in pairs(PROXYValidKanevaZones) do
				if zoneData.zoneName == v then
					Proxy_AddLastOpFieldsToRecord(zoneData.zoneIndex, zoneData)
					table.insert(filterdZones, zoneData)
				end
			end
		end
	end
	return filterdZones
end

-- add entry if needed or simply update entry if it exists
function PROXY_UpdateMonitoredZones(key, lastOpResult, lastOpMsg)

	if (g_monitoredZones[key] == nil) then
		local mzEntry = {}
		mzEntry.zoneIndex    = key
		mzEntry.lastOpResult = lastOpResult
		mzEntry.lastOpMsg    = lastOpMsg
		g_monitoredZones[key] = mzEntry
		KEP_Log("CSZoneLiteProxy added monitored zone for zone["..tostring(key).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
	else
		g_monitoredZones[key].lastOpResult = lastOpResult
		g_monitoredZones[key].lastOpMsg    = lastOpMsg
		KEP_Log("CSZoneLiteProxy updated monitored zone for zone["..tostring(key).."] lastOpResult["..tostring(lastOpResult).."] lastOpMsg["..tostring(lastOpMsg).."]")
	end

	-- craft/fire client event
	if (lastOpResult == CSZLPLastOperation.Result.PENDING) then
		local clientEvent = KEP_EventCreate(PROXYEvents.Reply.ZoneStateChange)
		KEP_EventSetObjectId(clientEvent, key)
		KEP_EventEncodeNumber(clientEvent, lastOpResult)
		KEP_EventEncodeString(clientEvent, lastOpMsg)
		KEP_EventQueue(clientEvent)
		KEP_Log("CSZoneLiteProxy Dispatched["..PROXYEvents.Reply.ZoneStateChange.."]")
	end
end

function Proxy_AddLastOpFieldsToRecord(key, record)
	if (g_monitoredZones[key] == nil) then
		record.lastOpResult = CSZLPLastOperation.Result.NONE
		record.lastOpMsg    = CSZLPLastOperation.Msg.NONE
	else
		record.lastOpResult = g_monitoredZones[key].lastOpResult
		record.lastOpMsg    = g_monitoredZones[key].lastOpMsg
	end
end

-- recursivly dump table
function Proxy_TraceDump(aVar, indent)
	indent = indent or 0
	if (type(aVar) ~= "table") then
		KEP_Log(string.rep(" ", indent).." Value["..tostring(aVar).."]")
	else
		for k, v in pairs(aVar) do
			if type(v) == "table" then
				KEP_Log(string.rep(" ", indent).." Key["..tostring(k).."]")
				Proxy_TraceDump(v, indent + 2)
			else
				KEP_Log(string.rep(" ", indent).." Key["..tostring(k).."], Value["..tostring(v).."]")
			end
		end
	end
end
