--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\MenuScripts\\WebSuffix.lua")
dofile("..\\MenuScripts\\TryOnInventory_Expired.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")

g_player = 0
g_playerId = 0

function TryOnInventoryHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	glid = KEP_EventDecodeNumber( event );
	usetype = KEP_EventDecodeNumber( event );
	if glid>=3000000 and (usetype==200) then	-- add other types as needed
		local url = GameGlobals.WEB_SITE_PREFIX .. TRY_ON_ITEM_SUFFIX .. "&glid=" .. glid
		makeWebCall(url, WF.TRY_ON_ITEM, 0, 0)
		return KEP.EPR_CONSUMED
	end
	MenuOpen("TryOnInventory.xml")
	return KEP.EPR_OK
end

function TryOnInventoryHandler_Expired(dispatcher, fromNetid, event, eventid, filter, objectid)
	glid = KEP_EventDecodeNumber( event );
	usetype = KEP_EventDecodeNumber( event );
	placementId = KEP_EventDecodeNumber( event );

	g_player = KEP_GetPlayer()
	g_playerId = GetSet_GetNumber( g_player, MovementIds.NETWORKDEFINEDID )

	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		ok, name = KEP_ConfigGetString( config, "showTryOnExpire", "true" )
		if name == "true" then
			MenuOpen( "TryOnInventory_Expired.xml")

			local expiredInventory = KEP_EventCreate( KEP.TRYON_INVENTORY_EXPIRE_EVENT )

			KEP_EventEncodeNumber( expiredInventory, glid )
			KEP_EventEncodeNumber( expiredInventory, usetype )
			KEP_EventEncodeNumber( expiredInventory, placementId )

			KEP_EventSetFilter( expiredInventory, KEP.FILTER_TRYON_EXPIRE )

			KEP_EventQueue( expiredInventory )
		else
			RemoveTryOnItem( g_playerId, usetype, placementId)
		end
	end
	return KEP.EPR_OK
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandlerFiltered( "TryOnInventoryHandler", "TryOnInventoryEvent", KEP.FILTER_TRYONINVENTORY, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFiltered( "TryOnInventoryHandler_Expired", "TryOnInventoryEvent", KEP.FILTER_TRYONINVENTORY_EXPIRED, 0, 0, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( KEP.TRYON_INVENTORY_EXPIRE_EVENT, KEP.MED_PRIO )
end
