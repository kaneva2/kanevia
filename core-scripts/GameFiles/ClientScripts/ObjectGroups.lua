--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\AttribEventIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\ClientScripts\\PlayerInformation.lua")
dofile("..\\Scripts\\GetAttributeType.lua")

COPIED_GROUP = nil

SelectionSets = {}

g_copiedInventory = {}
g_copyingGroup = false
g_selectingSet = false
g_selectingSetCount = 0
g_copyingLastObject = false
g_requestingInventory = false
g_retryPaste = false
g_copyingIndex = 0
g_copyingGLID = 0

--------------KEY STATES--------------------------------------------
altDown = false
ctrlDown = false
shiftDown = false

-- Object Groups Loggers
function ogLog(txt) Log("ObjectGroups::"..txt) end
function ogLogWarn(txt) LogWarn("ObjectGroups::"..txt) end
function ogLogError(txt) LogError("ObjectGroups::"..txt) end

function ObjectGroups_InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "PurchaseGroupItemsEvent", KEP.MED_PRIO )
	KEP_EventRegister( "RetryPasteEvent", KEP.MED_PRIO )
	KEP_EventRegister( "SelectionSetUpdateEvent", KEP.MED_PRIO )
	KEP_EventRegister( "DynamicObjectChangeEvent", KEP.MED_PRIO )
end

function ObjectGroups_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	PlayerInformation_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	
	KEP_EventRegisterHandlerFiltered( "attribEventHandler", "AttribEvent", AttribEvent.PLAYER_INVEN, 0, 0, KEP.MED_PRIO )
	KEP_EventRegisterHandlerV( "addDynamicObjectEventHandlerForObjectGroups", "AddDynamicObjectEvent", KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "ObjectGroups_BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "retryPasteHandler", "RetryPasteEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "selectionSetUpdateHandler", "SelectionSetUpdateEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ObjectGroups_keyChangedEventHandler", "KeyChangedEvent", KEP.MED_PRIO )
end

-----DO GROUP CLASS-------------------------------------------
--This defines an action that will be tracked in a action history and can be undone
------------------------------------------------------------------

ObjectGroup = {}
ObjectGroup.__index = ObjectGroup

function ObjectGroup.create(objects, name, pos)
	local objectGroup = {}
	setmetatable(objectGroup, ObjectGroup)
	objectGroup.name = name
	objectGroup.pos = pos
	objectGroup.objects = objects
	return objectGroup
end

function getObjectInfo(id, type)
	return ObjectGetInfo(id, type)
end

function clearCopy()
	COPIED_GROUP = {}
	g_copyingGroup = false
end

function ObjectsGetLowestHeight(objects)
	local lowestHeight = 9999
	for i,v in ipairs(objects) do
		if (v.type == ObjectType.DYNAMIC) then
			local minX, minY, minZ, maxX, maxY, maxZ = KEP_DynamicObjectGetBoundingBox(v.id)
			local tempMin = math.min(minY, maxY)
			lowestHeight = math.min(lowestHeight, tempMin)
		end
	end
	return lowestHeight
end

function copySelectedObjects()
	g_copyingGroup = false
	local objects = ObjectsSelectedGetInfoExcept(ObjectType.WORLD)
	COPIED_GROUP = createObjectGroup(objects)
end

function createObjectGroup(objs, name)
	
	-- Get Objects Group Position (centroid, above ground)
	local pos = ObjectsGetPosition(objs)
	pos.y = ObjectsGetLowestHeight(objs)

	-- Update Each Object Position, Rotation & Offset To Group Position
	for i,v in ipairs(objs) do
		v.pos, v.rot = ObjectGetPositionAndRotation(v.id, v.type)
		v.posOff = {
			x = v.pos.x - pos.x,
			y = v.pos.y - pos.y,
			z = v.pos.z - pos.z
		}
	end
	
	-- Return New Object Group
	return ObjectGroup.create(objs, name, pos)
end

function pasteCopiedGroup()
	if COPIED_GROUP == nil then return end

	local objects = COPIED_GROUP.objects
	if objects == nil or #objects < 1 then 
		ogLogError("pasteCopiedGroup: Nothing Copied")
		return 
	end

	KEP_ClearSelection()

	if not gameItemCheck(objects) then
		ogLogError("pasteCopiedGroup: gameItemCheck() FAILED")
		InfoBoxMsg("Copy/Paste is not compatible with Gaming objects.")
		return
	end

	g_copyingGroup = true
	g_copyingIndex = 0
	
	-- Paste Objects At Player Last Position
	local movementObj = KEP_GetPlayer()
	local pos = {}
	pos.x, pos.y, pos.z = GetSet_GetVector(movementObj, MovementIds.LASTPOSITION)
	COPIED_GROUP.pos = pos

	ogLog("pasteCopiedGroup: pos="..VecToString(pos))

	-- Paste Will Occur In addDynamicObjectEventHandlerForObjectGroups()
	g_requestingInventory = true
	KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
end

-- Called once for each pasted item. Eventually triggers 
-- addDynamicObjectEventHandlerForObjectGroups() which actually 
-- sets the placed object position, rotation, and texture.
function pasteNextObject()
	local objects = COPIED_GROUP.objects
	if g_copyingIndex >= #objects then
		g_copyingLastObject = true
		g_copyingGroup = false
		return
	end
	g_copyingIndex = g_copyingIndex + 1
	local object = objects[g_copyingIndex]
	g_copyingGLID = object.glid
	KEP_UseInventoryItem(g_copyingGLID, 0)
end

function playerInventoryCheck(objects)

	NEEDED_OBJECTS = {}
	g_ownsAllNeededObjects = true
	g_copiedInventory = g_playerInventory

	-- go over list of required objects (we want to copy)
	-- and make sure they exist in inventory
	for i,v in ipairs(objects) do
		local copyGlid = v.glid
		local copyGlidExists = false

		for j=1, #g_copiedInventory do
			local invItem = g_copiedInventory[j]
			if (copyGlid == invItem["GLID"]) then
				-- match in inventory
				copyGlidExists = true
				break
			end 
		end

		-- After looking at the inventory if the glid is not  there
		-- add it to the needs with a stable qty of 1
		if (copyGlidExists ~= true) then
			local item = {}
			item["GLID"] = copyGlid
			item["quantity"] = 1
			table.insert(NEEDED_OBJECTS, item)
			g_ownsAllNeededObjects = false
		end
	end

	return g_ownsAllNeededObjects
end

function gameItemCheck(objects)
	for i,v in ipairs(objects) do
		if v.gameItemId and v.gameItemId ~= 0 then
			return false
		end
	end
	return true
end

function saveSelectionSet(objectGroup, saveToFile)
	if objectGroup.name == nil or objectGroup.name == "" or #objectGroup.objects == 0 then
		return
	end
	
	--check to see if one already exists
	for i,v in ipairs(SelectionSets) do
		if v.name == objectGroup.name then
			v.objects = objectGroup.objects
			v.pos = objectGroup.pos
			if saveToFile == true then
				saveSelectionSetToFile(v)
			end
			return
		end
	end
	
	--save new
	table.insert(SelectionSets, objectGroup)
	if saveToFile == true then
		saveSelectionSetToFile(objectGroup)
	end
	sendSelectionSetUpdate()
end

function deleteSelectionSet(name)
	for i,v in ipairs(SelectionSets) do
		if v.name == name then
			table.remove(SelectionSets, i)
			sendSelectionSetUpdate()
			deleteSelectionSetFromFile(name)
			return
		end
	end
end

function selectSelectionSet(name)
	for i,v in ipairs(SelectionSets) do
		if v.name == name then
			if ctrlDown == false then
				KEP_ClearSelection()
			end
			local objects = v.objects
			g_selectingSet = true
			g_selectingSetCount = 0
			for j,obj in ipairs(objects) do
				KEP_SelectObject(obj.type, obj.id)
				g_selectingSetCount = g_selectingSetCount + 1
			end
			return
		end
	end
end

function sendSelectionSetUpdate()
	local ev = KEP_EventCreate("SelectionSetUpdateEvent")
	KEP_EventSetFilter(ev, 1)
	KEP_EventEncodeNumber(ev, #SelectionSets)
	for i,v in ipairs(SelectionSets) do
		KEP_EventEncodeString(ev, v.name)
	end
	KEP_EventQueue(ev)
end

function ObjectGroups_keyChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local isKeyDown = KEP_EventDecodeNumber(event)
	if(isKeyDown == 1) then
		if filter == 17 then --CTRL
			ctrlDown = true
		elseif filter == 16 then --SHIFT
			shiftDown = true
		elseif filter == 18 then --ALT
			altDown = true
		end
	else
		if filter == 17 then --CTRL
			ctrlDown = false
		elseif filter == 16 then --SHIFT
			shiftDown = false
		elseif filter == 18 then --ALT
			altDown = false
		end
	end
end

function selectionSetUpdateHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == 2 then --requesting update
		sendSelectionSetUpdate()
	elseif filter == 3 then -- select Set
		selectionName = KEP_EventDecodeString(event)
		selectSelectionSet(selectionName)
	elseif filter == 4 then -- save set
		local selectionName = KEP_EventDecodeString(event)
		local selectedObjs = ObjectsSelectedGetInfo()
		local selectedGroup = createObjectGroup(selectedObjs, selectionName)	
		saveSelectionSet(selectedGroup, true)
	elseif filter == 5 then
		local selectionName = KEP_EventDecodeString(event)
		deleteSelectionSet(selectionName)
	end
end

function retryPasteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if g_retryPaste then
		g_retryPaste = false
		pasteCopiedGroup()
	end
end

function ObjectGroups_BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if g_requestingInventory == true then
		if filter == WF.COPIED_QUANTITIES then
			g_requestingInventory = false
			g_retryPaste = false

			xml = KEP_EventDecodeString( event )
			g_playerInventory = {}
			decodeInventoryItemsFromWeb( g_playerInventory, xml, false )

			if (playerInventoryCheck(COPIED_GROUP.objects) == true) or KEP_IsWorld() then
				pasteNextObject()
			else
				local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventEncodeString(statusEvent, "Cannot place items you do not own.")
				KEP_EventEncodeNumber(statusEvent, 3)
				KEP_EventEncodeNumber(statusEvent, 255)
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventEncodeNumber(statusEvent, 0)
				KEP_EventSetFilter(statusEvent, 4)
				KEP_EventQueue(statusEvent)
			end

--[[ DRF - ED-1913 - Copy/Paste Requires Purchase Bug
			if (playerInventoryCheck(COPIED_GROUP.objects) == true) or KEP_IsWorld() then
				pasteNextObject()
			else
				g_itemsMenu = MenuOpen( "PurchaseGroupItems.xml")
				local ev = KEP_EventCreate("PurchaseGroupItemsEvent")

				local numObjects = #NEEDED_OBJECTS
				KEP_EventEncodeNumber(ev, numObjects)
				for i,v in pairs(NEEDED_OBJECTS) do
					KEP_EventEncodeNumber(ev, v["GLID"])
					KEP_EventEncodeNumber(ev, v["quantity"])
				end
				KEP_EventQueue(ev)
				g_retryPaste = true
			end
]]
			-- TODO - This should be moved to attribEventHandler() and not as a webcall callback
		end
	end
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if g_requestingInventory == true then
		if filter == AttribEvent.PLAYER_INVEN then
			local copiedGLIDs = {}
			for i,v in pairs(COPIED_GROUP.objects) do
				local glid = v.glid
				if not copiedGLIDs[glid] then
					table.insert(copiedGLIDs, glid, glid)
				end
			end

			-- TODO - Just Call pasteNextObject() Here!
			getItemQuantities(WF.COPIED_QUANTITIES, copiedGLIDs)
		end
	end
end

function addDynamicObjectEventHandlerForObjectGroups(dispatcher, fromNetId, event, eventId, filter, objectId)
	playerID = filter
	positionX = KEP_EventDecodeNumber(event)
	positionY = KEP_EventDecodeNumber(event)
	positionZ = KEP_EventDecodeNumber(event)
	rotationX = KEP_EventDecodeNumber(event)
	rotationY = KEP_EventDecodeNumber(event)
	rotationZ = KEP_EventDecodeNumber(event)
	slideX = KEP_EventDecodeNumber(event)
	slideY = KEP_EventDecodeNumber(event)
	slideZ = KEP_EventDecodeNumber(event)
	placementID = KEP_EventDecodeNumber(event)
	animationID = KEP_EventDecodeNumber(event)
	assetID = KEP_EventDecodeNumber(event)
	texUrl = KEP_EventDecodeString(event)
	friendID = KEP_EventDecodeNumber(event)
	playlistSWF = KEP_EventDecodeString(event)
	playlistParams = KEP_EventDecodeString(event)
	globalID = KEP_EventDecodeNumber(event)
	attachable = KEP_EventDecodeNumber(event)
	interactive = KEP_EventDecodeNumber(event)
	canPlayFlash = KEP_EventDecodeNumber(event)
	hasCollision = KEP_EventDecodeNumber(event)
	boundMinX = KEP_EventDecodeNumber(event)
	boundMinY = KEP_EventDecodeNumber(event)
	boundMinZ = KEP_EventDecodeNumber(event)
	boundMaxX = KEP_EventDecodeNumber(event)
	boundMaxY = KEP_EventDecodeNumber(event)
	boundMaxZ = KEP_EventDecodeNumber(event)
	derivable = KEP_EventDecodeNumber(event)
	invType   = KEP_EventDecodeNumber(event)
	
	-- This is where paste actually places objects
	if g_copyingGroup == true and g_copyingGLID == globalID then

		-- Get Copied Object Position As Offset From Group Position
		local objects = COPIED_GROUP.objects
		local object = objects[g_copyingIndex]
		local pos = {
			x = COPIED_GROUP.pos.x + object.posOff.x, 
			y = COPIED_GROUP.pos.y + object.posOff.y, 
			z = COPIED_GROUP.pos.z + object.posOff.z
		}

		-- Set New Object Postion, Rotation & Texture
		ObjectSetPosition(placementID, object.type, pos)
		ObjectSetRotation(placementID, object.type, object.rot)
		if (object.type ~= ObjectType.SOUND) then
			ObjectSetTexture(placementID, object.type, object.assetId, object.texUrl or "")
			KEP_SaveDynamicObjectPlacementChanges(placementID)
		end

		KEP_SelectObject(object.type, placementID)

		pasteNextObject()
	end
end

function loadSelectionSets()
	local zoneID = KEP_GetZoneInstanceId()
	ogLog("loadSelectionSets: zoneId="..zoneID)

	local readLines = readFileIntoTable()
	SelectionSets = {}
	for i,v in ipairs(readLines) do
		local line = tostring(v)
		if string.find(line, "ZONE:"..tostring(zoneID)) ~= nil then
			local objectGroup = parseStringAsSelectionSet(line)
			saveSelectionSet(objectGroup, false)
		end
	end
end

function parseStringAsSelectionSet(line)
	local selectionName = ""
	local objects = {}
	
	local commaFound = string.find(line, ",")
	
	if commaFound == nil then
		return
	end

	--delete zoneID, we don't need it
	line = string.sub(line, commaFound + 1)
	commaFound = string.find(line, ",")
	if commaFound == nil then
		return
	end
	selectionName = string.sub(line, 1, commaFound - 1)
	line = string.sub(line, commaFound)
	commaFound = string.find(line, ",")
	
	while commaFound ~= nil do
		line = string.sub(line, 2)
		local semiColonFoundLine = string.find(line, ":")
		local objectInfo = string.sub(line, 1, semiColonFoundLine + 1)
		local semiColonFound = string.find(objectInfo, ":")
		local objectId = tonumber(string.sub(objectInfo, 1, semiColonFound - 1))
		local objectType = tonumber(string.sub(objectInfo, semiColonFound + 1))
		if objectId ~= nil and objectType ~= nil then
			local object = getObjectInfo(objectId, objectType)
			if not object.exists then 
				ogLogError("parseStringAsSelectionSet: Object Does Not Exist - objId="..objectId.." objType="..objectType)
			else
				table.insert(objects, object)
			end
		end
		commaFound = string.find(line, ",")
		if commaFound ~= nil then
			line = string.sub(line, commaFound)
		end
	end
	
	local selectedGroup = createObjectGroup(objects, selectionName)	
	return selectedGroup
end

function readFileIntoTable()
	local lines = {}
	
	--Read whole file in
	local readfile, errmsg = KEP_FileOpenConfig("selectionSets.cfg","r")
	if readfile ~= 0 then
		--Read first line
		local ok, line, errMsg = KEP_FileReadLine( readfile )
		while ok == 1 do
			table.insert(lines, line)
			ok, line, errMsg = KEP_FileReadLine( readfile )
		end
		KEP_FileClose( readfile )
	else
		local writefile, errmsg = KEP_FileOpenConfig("selectionSets.cfg","w")
		KEP_FileWriteLine(writefile, "")
		KEP_FileClose( writefile )
	end
	return lines
end

function deleteSelectionSetFromFile(name)

	local zoneID = KEP_GetZoneInstanceId()
	local readLines = readFileIntoTable()
	local existingFound = false
	ogLog("deleteSelectionSetFromFile: zoneId="..zoneID.." objGroup='"..name)
	
	local writefile, errmsg = KEP_FileOpenConfig("selectionSets.cfg","w")
	if writefile ~= nil then
		for i,v in ipairs(readLines) do
			local line = tostring(v)
			local group = parseStringAsSelectionSet(line)
			
			if string.find(line, "ZONE:"..tostring(zoneID)) ~= nil and group.name == name then
				existingFound = true
				--skip this line of writing
			else
				KEP_FileWriteLine(writefile, line .. "\n")
			end
		end
		if existingFound == false then
			KEP_FileWriteLine(writefile, selectionSetString .. "\n")
		end
		KEP_FileClose( writefile )
	end
end

function saveSelectionSetToFile(objectGroup)

	local zoneID = KEP_GetZoneInstanceId()
	local name = objectGroup.name
	local selectionSetString = "ZONE:"..tostring(zoneID)..","..tostring(name)
	local objects = objectGroup.objects
	ogLog("saveSelectionSetToFile: zoneId="..zoneID.." objGroup='"..objectGroup.name.."' objects="..#objectGroup.objects)

	for i,v in ipairs(objects) do
		selectionSetString = selectionSetString ..","..v.id..":"..v.type
	end
	
	local readLines = readFileIntoTable()
	local existingFound = false
	
	local writefile, errmsg = KEP_FileOpenConfig("selectionSets.cfg","w")
	if writefile ~= nil then
		for i,v in ipairs(readLines) do
			local skip = false
			local line = tostring(v)
			local group = parseStringAsSelectionSet(line)
			
			if group.name == nil or group.name == "" then -- bad selection set
				skip = true
			end
			if skip == false then
				if string.find(line, "ZONE:"..tostring(zoneID)) ~= nil and group.name == name then
					existingFound = true
					KEP_FileWriteLine(writefile, selectionSetString .. "\n")
				else
					KEP_FileWriteLine(writefile, line .. "\n")
				end
			end
		end
		if existingFound == false then
			KEP_FileWriteLine(writefile, selectionSetString .. "\n")
		end
		KEP_FileClose( writefile )
	end
end
