--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")

function scriptServerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	KEP_EventAddToServer( event )
	KEP_EventQueue( event )
	return KEP.EPR_CONSUMED
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "scriptServerEventHandler", "ScriptServerEvent", KEP.MED_PRIO )
end

