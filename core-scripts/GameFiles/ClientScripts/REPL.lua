--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Simple Lua REPL

dofile("../MenuScripts/LibClient_Common.lua")
dofile("REPLEvents.lua")

local partialInput = ""
local EOF_TAG = "'<eof>'"

local function myPrint(...)
	local n = select("#", ...)
	for i=1,n do
		KEP_ConsolePrint(tostring(select(i, ...)))
		if i<n then
			KEP_ConsolePrint("\t")
		else
			KEP_ConsolePrint("\n")
		end
	end
end

function onCreate()
	Log("onCreate")
	-- Declare native events before registering handlers against them
	-- Framework events do not need to be declared
	Events.declareNativeEvents(REPLEvents)
	Events.registerNativeHandler("REPLInputEvent", onREPLInput)
end

function onREPLInput(event)
	Log("onREPLInput " .. tostring(event.input))

	-- override print function
	_G.print = myPrint

    local input = partialInput .. event.input
	partialInput = ""

	local code = input
	local toPrint = false
	if string.sub(code, 1, 1)=="=" then
		-- Quick evaluation similar to Lua command line
		code = "return " .. string.sub(input, 2)
    elseif string.sub(code, 1, 1)=="/" then
        -- Slash command support
        KEP_SendChatMessage(0, code)
        parseResults(code, true, "[Slash Command]")
        return
	end

    local fun, err = loadstring(code)
	if fun then
		parseResults(input, pcall(fun))
	else
		parseResults(input, false, err)
	end
end

function parseResults(input, ok, ...)
	if ok then
		-- Succeeded
		print(...)
		Events.sendNativeEvent("REPLResultEvent", {})
	else
		-- Failed
		local err = select(1, ...)
		if err and string.sub(err, -#EOF_TAG)==EOF_TAG then
			-- Incomplete
			partialInput = input .. string.char(13)
			Events.sendNativeEvent("REPLResultEvent", {incomplete=1})
		elseif err then
			-- Lua Error
			if string.sub(err, 1, 1)=="[" then
				local s, e = string.find(err, "]:")
				if e then
					err = "stdin" .. string.sub(err, e)
				end
			end
			Events.sendNativeEvent("REPLResultEvent", {error=err})
		else
			-- Unknown
			Events.sendNativeEvent("REPLResultEvent", {error="Unknown error"})
		end
	end
end