--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\UnlockableIds.lua")

REQUEST_UNLOCKABLE_EVENT = "RequestUnlockableEvent"

unlockables = {}

Unlockable = {}
Unlockable.__index = Unlockable
--type is Unlockable Type above
--Id is either Emote id or FeatureId above depending on type
--locked 0 or 1
--fameType and Level Number, if fametype == 0 then fame is not required
function Unlockable.create(type, id, gender, locked, fameType, levelNumber)
	local unlockable = {}             -- our new object
	setmetatable(unlockable,Unlockable)
	unlockable.type = tonumber(type) or 0
	unlockable.id = tonumber(id) or 0
	unlockable.gender = tostring(gender) or "F"
	unlockable.locked = tonumber(locked) or 1
	unlockable.fameType = tonumber(fameType) or 0
	unlockable.levelNumber = tonumber(levelNumber) or 0
	return unlockable
end

function addUnlockable(type, id, gender, locked, fameType, levelNumber)
	local unlock = getUnlockable(type, id, gender, false)
	if unlock == nil then
		local unlockable = Unlockable.create(type, id, gender, locked, fameType, levelNumber)
		table.insert(unlockables, unlockable)
	end
end

function getUnlockable(type, id, gender, expectToFind)
	for i,v in ipairs(unlockables) do
		if v.type == type and v.id == id and v.gender == gender then
			return v
		end
	end
	if expectToFind then
--		Log("getUnlockable - Failed to find "..type.." "..id.." "..gender.." in "..#unlockables.." unlockables.")
	end
end

function requestAllEmotes(gender)
	local emotes = {}
	for i,v in ipairs(unlockables) do
		if v.type == UnlockableType.Emote and v.gender == gender then
			table.insert(emotes, v)
		end
	end
	local ev = KEP_EventCreate( REQUEST_UNLOCKABLE_EVENT)
	KEP_EventSetFilter(ev, UnlockableRequestFilter.EmotesResponse)
	KEP_EventEncodeNumber(ev, #emotes)
	for i,v in ipairs(emotes) do
		KEP_EventEncodeNumber(ev, v.type)
		KEP_EventEncodeNumber(ev, v.id)
		KEP_EventEncodeString(ev, v.gender)
		KEP_EventEncodeNumber(ev, v.locked)
		KEP_EventEncodeNumber(ev, v.fameType)
		KEP_EventEncodeNumber(ev, v.levelNumber)
	end
	KEP_EventQueue( ev)
end

function requestUnlockableInfo(type, id, gender)
	local unlockable = getUnlockable(type, id, gender, true)
--	Log("SENDING REQUESTED UNLOCKABLE: " .. tostring(type) .. " " .. tostring(id) .. " " .. tostring(unlockable))
	if unlockable ~= nil then
		local ev = KEP_EventCreate( REQUEST_UNLOCKABLE_EVENT)
		KEP_EventSetFilter(ev, UnlockableRequestFilter.FeatureResponse)
		KEP_EventEncodeNumber(ev, unlockable.type)
		KEP_EventEncodeNumber(ev, unlockable.id)
		KEP_EventEncodeString(ev, unlockable.gender)
		KEP_EventEncodeNumber(ev, unlockable.locked)
		KEP_EventEncodeNumber(ev, unlockable.fameType)
		KEP_EventEncodeNumber(ev, unlockable.levelNumber)
		KEP_EventQueue( ev)
	end
end
--if unlockable has unlocked, send an event
--Unlockables must be included within WFTRacking to work, does not work alone
function updateUnlockables()
	for i,v in ipairs(unlockables) do
		if v.fameType > 0 and v.gender == g_gender then
			local fameLevel = FameLevels[v.fameType]
			if fameLevel ~= nil then
				oldLocked = v.locked
				newLocked = 1
				if fameLevel.level >= v.levelNumber then
					newLocked = 0
				end
				v.locked = newLocked
				if oldLocked ~= newLocked then
					requestUnlockableInfo(v.type, v.id, v.gender)
				end
			end
		end
	end
end
