--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile( "..\\Scripts\\KEP.lua" )
dofile( "..\\MenuScripts\\CommonFunctions.lua" )

INFO_BOX_EVENT = "InfoBoxEvent"

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandlerFiltered( "genericInfoBoxHandler", KEP.GENERIC_INFO_BOX_EVENT, KEP.FILTER_GENERIC_INFO_BOX_MSG1, 0, 0, KEP.MED_PRIO )
end

-- Called from game client to display a potential error while not
-- interrupting the render loop, and thus, disconnecting the client.
function genericInfoBoxHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	-- Display Message Box
	local msg = KEP_EventDecodeString( event )
	KEP_MessageBox( msg, nil, nil, nil, nil, nil, nil, true )
	return KEP.EPR_OK
end

