--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile( "..\\Scripts\\KEP.lua" )
dofile( "..\\Scripts\\SoundGlobals.lua" )
dofile( "..\\Scripts\\MovementIds.lua" )

-- Fired from server python to inform client which npc was hit
function  collisionHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	npcId = objectid
	sound = SG.ItemSounds[npcId]
	if sound ~= nil then
		-- 0,0,0 is xyz of sound, it means play sound at players location
		-- TODO: Check directsound to confirm and rethink this function a little
		KEP_PlaySoundById(sound[SG.INDEX], sound[SG.LOOPING], 0, 0, 0, sound[SG.SOUND_FACTOR], sound[SG.CLIP_RADIUS])
	end
	return KEP.EPR_OK
end

function broadcastSoundEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local playerMovObj = KEP_GetPlayer( )
	playerX, playerY, playerZ = GetSet_GetVector( playerMovObj, MovementIds.LASTPOSITION )
	local id = KEP_EventDecodeNumber( event )
	local soundX = KEP_EventDecodeNumber( event )
	local soundY = KEP_EventDecodeNumber( event )
	local soundZ = KEP_EventDecodeNumber( event )
	local fallOff = KEP_EventDecodeNumber( event )
	local clipDist = KEP_EventDecodeNumber( event )
	KEP_PlaySoundById(id, 0, soundX - playerX, soundY - playerY, soundZ - playerZ, fallOff, clipDist)
	return KEP.EPR_OK
end

function broadcastMusicEventHandler(dispatcher, fromNetId, event, eventid, filter, objectid)
	local id = KEP_EventDecodeNumber( event )
	KEP_LogDebug( "--> broadcast music id: " .. tostring(id))
	KEP_PlaySoundTrackById(id, 0, -1)
	return KEP.EPR_OK
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "collisionHandler",   "NPCCollisionEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "broadcastSoundEventHandler", "BroadcastSoundEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "broadcastMusicEventHandler", "BroadcastMusicEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "NPCCollisionEvent", KEP.MED_PRIO )
	KEP_EventRegister( "BroadcastSoundEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "BroadcastMusicEvent", KEP.HIGH_PRIO )
end
