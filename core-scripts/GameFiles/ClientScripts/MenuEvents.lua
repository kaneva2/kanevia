--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile( "..\\Scripts\\KEP.lua" )
dofile("..\\MenuScripts\\CommonFunctions.lua")

gPersistantMenus = {}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "destroyMenuHandler", "DestroyMenuEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "launchMenuHandler", "LaunchMenuEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "launchBrowserEventHandler", "LaunchBrowserEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "persistMenuEventHandler", "PersistMenuEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "launchPersistantMenusEventHandler", "LaunchPersistantMenusEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "DestroyMenuEvent", KEP.MED_PRIO )
	KEP_EventRegister( "LaunchMenuEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "LaunchBrowserEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PersistMenuEvent", KEP.MED_PRIO )
end

function destroyMenuHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local dialogHandle = KEP_EventDecodeNumber( event )
	DestroyMenu(dialogHandle)
end

function launchMenuHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	menuName = KEP_EventDecodeString( event )
	dh = DoMenuOpen(menuName)
	return KEP.EPR_CONSUMED
end

function launchBrowserEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local url = KEP_EventDecodeString( event )
	KEP_LaunchBrowser( url )
	return KEP.EPR_CONSUMED
end

function persistMenuEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local menuName = KEP_EventDecodeString( event )
	local size = 0
	for i, menu in ipairs( gPersistantMenus ) do
		size = size + 1
	end
	gPersistantMenus[ size + 1 ] = menuName
	return KEP.EPR_OK
end

function launchPersistantMenusEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	for i, menu in ipairs( gPersistantMenus ) do
		DoMenuOpen(menu)
	end
	gPersistantMenus = {}
	return KEP.EPR_OK
end

