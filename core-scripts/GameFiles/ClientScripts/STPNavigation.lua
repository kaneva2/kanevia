--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\Framework_EventHelper.lua")

ZoneURL = {}
ZoneURL.__index = ZoneURL

function ZoneURL.create(url, name, category)
	local zoneURL = {}             -- our new object
	setmetatable(zoneURL,ZoneURL)
	zoneURL.url = url
	zoneURL.name = name
	zoneURL.category = category
	zoneURL.timeStamp = 0
	return zoneURL
end

FAVORITES_FILENAME = "Favorites.cfg"
HISTORY_FILENAME = "History.cfg"

MAX_HISTORY = 50
MAX_FAVORITES = 50

History = {}
Favorites = {}
SearchableURLS = {} --a list of unique URLs for stp typing
ZoneLoadedHistory = {}  --a list of newZoneLoadedHandler events that came through before history and favorites were loaded
g_PlacesVisitedThisSession = {}

g_currentUrl = "" -- always contains the current zone url

g_nextUrl = ""

g_currentHistoryIndex = 0
g_firstLoad = false
g_firstHomeLoad = false
g_loadDefaultURLs = false
g_AddToHistory = true
g_loginName = nil
g_historyFavoritesLoaded = false --lets newZoneLoadedHandler know if it can access the history and favorites yet

TEN_MINUTES_LATER_EVENT = "TenMinutesLaterEvent"
g_tenMinutesLaterEvent = nil
g_tenMinutesLaterEventAlreadyHandled = false

EVERY_15_MINUTES_EVENT = "EveryFifteenMinutesEvent"
g_every15MinutesTimerStarted = false

NUDGE_TIMER_EVENT = "NudgeTimerEvent"
g_nudgeThirtySecondsLaterEvent = nil
g_nudgeFiveMinutesLaterEvent = nil

g_frameworkEnabled = false

IN_WORLD_INVITE_EVENT = "InWorldInviteEvent"
ZONE_NAVIGATION_EVENT = "ZoneNavigationEvent"

NEW_PLACE_VISITED_THIS_SESSION_EVENT = "NewPlaceVisitedThisSessionEvent"
VALIDATE_URL_EVENT = "ValidateURLEvent"
TRAVEL_NOTICE_EVENT = "TravelNoticeEvent"

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "newZoneLoadedHandler", "ZoneLoadedEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "zoneNavigationEventHandler", ZONE_NAVIGATION_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "attribEventHandler", "AttribEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "inWorldInviteEventHandler", IN_WORLD_INVITE_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "EntityCfgCompleteEventHandler", "EntityCfgCompleteEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "TenMinutesLaterEventHandler", TEN_MINUTES_LATER_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "Every15MinutesEventHandler", EVERY_15_MINUTES_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "validateURLEventHandler", VALIDATE_URL_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "returnFrameworkStateHandler", "FrameworkState", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( ZONE_NAVIGATION_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( IN_WORLD_INVITE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( TEN_MINUTES_LATER_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( EVERY_15_MINUTES_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( NEW_PLACE_VISITED_THIS_SESSION_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( VALIDATE_URL_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( TRAVEL_NOTICE_EVENT, KEP.MED_PRIO )
end

function newZoneLoadedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local zoneName = KEP_EventDecodeString( event )
	local currentUrl = KEP_EventDecodeString( event )
	
	-- Reset framework variables since we don't receive the events outside of a Framework World
	g_frameworkEnabled = false
	
	Log("newZoneLoadedHandler: name='"..zoneName.."' url='"..currentUrl.."'")

	-- Grab zone info
	local parsedName = string.lower(parseNameFromURL(currentUrl) or "")
	local gameName = string.lower(parseGameNameFromURL(currentUrl) or "")
	local zoneIndex = InstanceId_GetId(KEP_GetZoneIndex())
	local username = KEP_GetLoginName()
	local zoneIsUserHome = isUrlUserHome(currentUrl, username) and zoneIndex ~= GameGlobals.CHARACTER_CREATION_ZI
	local urlType = string.lower(parseZoneTypeFromURL(currentUrl) or "")

	-- If user is visiting their home zone for the first time this session, log funnel progression
	if zoneIsUserHome and g_firstHomeLoad == false then
		makeWebCall(WEBCALL_NEW_USER_FUNNEL_PROGRESSION .. "entered_home", 0)
		g_firstHomeLoad = true
	end
	
	-- Add Zone Name And Url To Favorites -> History
	if (g_historyFavoritesLoaded == false) then
		table.insert(ZoneLoadedHistory, {zoneName, currentUrl})
		return
	end
	
	handleZoneLoaded( zoneName, currentUrl )
	
	-- Clear the "10 minutes later" event, since we didn't stay long enough to fire the handler
	if g_tenMinutesLaterEvent ~= nil then
		KEP_EventCancel( g_tenMinutesLaterEvent)
		g_tenMinutesLaterEvent = nil
	end

	if g_nudgeThirtySecondsLaterEvent ~= nil then
		KEP_EventCancel( g_nudgeThirtySecondsLaterEvent)
		g_nudgeThirtySecondsLaterEvent = nil
	end
	g_nudgeThirtySecondsLaterEvent = KEP_EventCreate( NUDGE_TIMER_EVENT)
	KEP_EventEncodeNumber(g_nudgeThirtySecondsLaterEvent, FameIds.WORLD_FAME)
	KEP_EventQueueInFuture( g_nudgeThirtySecondsLaterEvent, 30)
	
	if g_nudgeFiveMinutesLaterEvent ~= nil then
		KEP_EventCancel( g_nudgeFiveMinutesLaterEvent)
		g_nudgeFiveMinutesLaterEvent = nil
	end
	g_nudgeFiveMinutesLaterEvent = KEP_EventCreate( NUDGE_TIMER_EVENT)
	KEP_EventEncodeNumber(g_nudgeFiveMinutesLaterEvent, FameIds.WORLD_FAME)
	KEP_EventQueueInFuture( g_nudgeFiveMinutesLaterEvent, 300)

	if (urlType == "home") and (parsedName == string.lower(username)) then
		-- Don't give them travel credit for going to their own home

		-- Send an event announcing the place visited this session, in case someone wants to know
		-- when we cross certain thresholds. I'm looking at you, WFTracking!
		local event = KEP_EventCreate( NEW_PLACE_VISITED_THIS_SESSION_EVENT)
		KEP_EventEncodeNumber(event, #g_PlacesVisitedThisSession)
		KEP_EventEncodeString(event, currentUrl)
		KEP_EventQueue( event)
	else

		-- check if this new place is already in the list of places visited this session
		local bAlreadyVisited = false
		for i,v in ipairs(g_PlacesVisitedThisSession) do
			if v == parsedName then
				bAlreadyVisited = true
				break
			end
		end
		if not bAlreadyVisited then
			table.insert(g_PlacesVisitedThisSession, parsedName)

			-- Send an event announcing the unique place visited this session, in case someone wants to know
			-- when we cross certain thresholds. I'm looking at you, WFTracking!
			local event = KEP_EventCreate( NEW_PLACE_VISITED_THIS_SESSION_EVENT)
			KEP_EventEncodeNumber(event, #g_PlacesVisitedThisSession)
			KEP_EventEncodeString(event, currentUrl)
			KEP_EventQueue( event)
		end

		-- Create a future event for "10 minutes later"
		if not g_tenMinutesLaterEventAlreadyHandled then
			g_tenMinutesLaterEvent = KEP_EventCreate( TEN_MINUTES_LATER_EVENT)
			KEP_EventEncodeString(g_tenMinutesLaterEvent, zoneName)
			KEP_EventQueueInFuture( g_tenMinutesLaterEvent, 600) -- 10 minutes * 60 seconds
		end
	end
end

function EntityCfgCompleteEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

function TenMinutesLaterEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local zoneName = KEP_EventDecodeString(event)
	KEP_EventRemoveHandler( "TenMinutesLaterEventHandler", TEN_MINUTES_LATER_EVENT)
	g_tenMinutesLaterEventAlreadyHandled = true
	g_tenMinutesLaterEvent = nil
end

function Every15MinutesEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local ev = KEP_EventCreate( EVERY_15_MINUTES_EVENT)
	KEP_EventQueueInFuture( ev, 900)
	g_every15MinutesTimerStarted = true
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.IN_WORLD_INVITE then
		local returnData = KEP_EventDecodeString( event )
		local result = string.match(returnData, "<ResultDescription>(.-)</ResultDescription>")
		local code = string.match(returnData, "<ReturnCode>(.-)</ReturnCode>")

		if tonumber(code) == 0 then
			KEP_MessageBox("Invite was sent successfully.")
		end
	elseif filter == WF.VALIDATE_STPURL then
		
		-- Got CommonFunctions::gotoURL(URL) URL Validation Result
		local returnData = KEP_EventDecodeString( event )
		local accessReason = string.match(returnData, "<ReturnCode>(.-)</ReturnCode>")
		local accessMessage = string.match(returnData, "<ReturnDescription>(.-)</ReturnDescription>") or ""
		local url = string.match(returnData, "<STPURL>(.-)</STPURL>") or ""
		local communityId = string.match(returnData, "<CommunityId>(.-)</CommunityId>") or 0
		local thumbnailPath = string.match(returnData, "<ThumbnailMediumPath>(.-)</ThumbnailMediumPath>") or ""
	
		-- If the webcall returned no results, as in the case of an error in webcall processing,
		-- or if the results were successful, start the zoning process and defer to the server to further determine access
		if accessReason == nil or tonumber(accessReason) == 0 then
			-- Queue 'GotoPlaceEvent' With Url
			local ev = KEP_EventCreate( KEP.GOTOPLACE_EVENT_NAME )
			KEP_EventSetFilter( ev, KEP.GOTO_URL )
			KEP_EventEncodeNumber( ev, 0 ) -- 0 = char
			KEP_EventEncodeString ( ev, g_nextUrl)
			KEP_EventQueue( ev )

			-- Open Progress Menu For 'Rezone'
			if (g_nextUrl == GetHomeUrl()) then
				Log("BrowserPageReadyHandler: ... Going Home - Cannot Cancel ...")
				ProgressOptCancel("Rezone", PROGRESS_OPT_CANCEL_NEVER)
			end
			ProgressTag("Rezone", g_nextUrl) -- tag progress with the url for metrics reporting
			ProgressOpen("Rezone")  -- open progress for Rezone (Loading->PatchProgress->ZoneDownloader->LoadingSpinner)
		else
			MenuOpen("TravelNotice.xml")

			-- Queue 'TravelNoticeEvent'
			local ev = KEP_EventCreate( TRAVEL_NOTICE_EVENT )
			KEP_EventEncodeNumber( ev, tonumber(accessReason) ) 
			KEP_EventEncodeString ( ev, accessMessage)
			KEP_EventEncodeNumber( ev, tonumber(communityId) ) 
			KEP_EventEncodeString ( ev, thumbnailPath)
			KEP_EventQueue( ev )
		end
	end
end

function zoneNavigationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == ZONE_NAV.HISTORY_REQUEST then
		updateHistory()
		
	elseif filter == ZONE_NAV.FAVORITES_REQUEST then
		updateFavorites()
		
	elseif filter == ZONE_NAV.ADD_FAVORITE then
		local name = KEP_EventDecodeString( event )
		local url = KEP_EventDecodeString( event )
		local category = KEP_EventDecodeString( event )
		if name == nil or name == "" or url == nil or url == "" then
			KEP_MessageBox("Name or URL was not specified")
			return
		end
		addToFavorites(url, name, category)
		saveFavoritesToFile()
		
	elseif filter == ZONE_NAV.REMOVE_FAVORITE then
		local name = KEP_EventDecodeString( event )
		deleteZoneURLByName(Favorites, name)
		saveFavoritesToFile()
		
	elseif filter == ZONE_NAV.CURRENT_URL_REQUEST then
		SendZoneNavigationEvent()
		
	elseif filter == ZONE_NAV.GO_BACK then
		
		navigateBack()
	elseif filter == ZONE_NAV.GO_FORWARD then
		
		navigateForward()
	elseif filter == ZONE_NAV.SEARCH_REQUEST then
		local searchString = KEP_EventDecodeString(event)
		local numURLs = KEP_EventDecodeNumber(event)
		local validURLs = searchURLs(searchString, numURLs)
		
		local ev = KEP_EventCreate( ZONE_NAVIGATION_EVENT)
		KEP_EventSetFilter(ev, ZONE_NAV.SEARCH_RESPONSE)
		KEP_EventEncodeString(ev, searchString)
		KEP_EventEncodeNumber(ev, #validURLs)
		for i,v in ipairs(validURLs) do
			KEP_EventEncodeString(ev, v.name)
			KEP_EventEncodeString(ev, v.url)
			KEP_EventEncodeString(ev, v.category)
		end
		
		KEP_EventQueue( ev)
	end
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	
	-- character name
	if filter == 21 then
		-- set the player name and show it
		local name = KEP_EventDecodeString( event )
		g_loginName = name
		
		if g_firstLoad == false then
			loadFavorites()
			loadHistory()			
			g_firstLoad = true
			g_historyFavoritesLoaded = true
			
			if g_loadDefaultURLs == true then
				loadDefaultURLs()
			end
			
			if g_every15MinutesTimerStarted == false then
				local ev = KEP_EventCreate( EVERY_15_MINUTES_EVENT)
				KEP_EventQueueInFuture( ev, 900)
				g_every15MinutesTimerStarted = true
			end
			
			if (#ZoneLoadedHistory > 0) then
				for i,v in ipairs(ZoneLoadedHistory) do
--					Log( "attribEventHandler: zoneHistory["..i.."]: name=" .. v[1] .. " url='" .. v[2].."'")
					handleZoneLoaded( v[1], v[2] )
				end
				SendZoneNavigationEvent()
				ZoneLoadedHistory = {}
			end
		end
	end
end

function inWorldInviteEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local toUsername = KEP_EventDecodeString(event)
	local message = KEP_EventDecodeString(event)
	local url = KEP_EventDecodeString(event)
	local trackingRequestId = KEP_EventDecodeString(event)
	local currentGameId = KEP_EventDecodeNumber(event)
	
	if message == "" then 
		message = IVTM.DEFAULT_MESSAGE
	end
	
	if url == "" then
		url = g_currentUrl
	end
	local zoneName = parseFriendlyNameFromURL(g_currentUrl) or "their location"
	local webURL = GameGlobals.WEB_SITE_PREFIX .. "kgp/messagecenter.aspx?action=SendInWorldInvite&username=" .. toUsername .. "&message=" .. message .. "&kanevaURL=" .. url .. "&trackingMetric=" .. trackingRequestId .. "&route=true" .. "&gameId=" .. currentGameId .. "&zoneName=" .. zoneName
	makeWebCall(webURL, WF.IN_WORLD_INVITE, 0, 0)
	Log("inWorldInviteEventHandler: INVITE SENT - url="..webURL)
end

function validateURLEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_nextUrl = KEP_EventDecodeString(event)
	local rezone = false 
	local import = false
	if KEP_EventMoreToDecode (event) ~= 0 then 
		local zoningType = KEP_EventDecodeString(event)
		if zoningType == "Rezone" then 
			rezone = true
			local username = KEP_GetLoginName()
			Events.sendEvent("FRAMEWORK_SAVE_LAST_POSITION", {player = username})
		elseif zoningType == "ImportZone" then
			import = true
		end 
	end 
	
	-- Prevent Rezone To Person In Same Framework World
	if g_frameworkEnabled and rezone == false and import == false then
		local nextName = ParseNameFromUrl(g_nextUrl)
		local userName = string.match(nextName, "(.-)%.people")
		if ((userName ~= nil) and ToBool(KEP_PlayerIsValid(userName))) then
			LogWarn("validateURLEventHandler: Framework Rezone To Person '"..userName.."' - Aborting")
			return 
		end
		
		if splitURL(g_nextUrl)==splitURL(g_currentUrl) then 
			Framework.sendEvent("FRAMEWORK_RESPAWN_REQUEST", {})
			return
		end 
	end

	g_currentUrl = ""
	-- Encode Url Into Url Validation Webcall
	local enc_nextUrl = KEP_UrlEncode(g_nextUrl)
	makeWebCall(GameGlobals.WEB_SITE_PREFIX.."kgp/validatePlayerZoning.aspx?action=ValidateURLPermissions&stpurl="..enc_nextUrl, WF.VALIDATE_STPURL)
	
end

function splitURL(URL) 
		-- only get latter part of the URL
		local substring = split(URL, "/")
		local suffix = substring[#substring]
		return suffix
end 
-- Handles the return for the current Framework state
function returnFrameworkStateHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	local frameworkState = cjson.decode(Event_DecodeString(event))
	if frameworkState.enabled ~= nil then
		g_frameworkEnabled = (frameworkState.enabled == true)
	end
end

function handleZoneLoaded( zoneName, currentUrl )
	Log("handleZoneLoaded: name='"..zoneName.."' url='"..currentUrl.."'")

	local prevURL = ""
	if #History > 0 then
		local prevZoneURL = History[#History]
		prevURL = prevZoneURL.url
	end
	
	if currentUrl ~= nil and currentUrl ~= "" then
		Log("handleZoneLoaded: Saving currentUrl='"..currentUrl.."'")
		g_currentUrl = currentUrl		
	end
	
	if currentUrl == prevURL then
		g_AddToHistory = false
	end
	
	if g_AddToHistory == false then
		g_AddToHistory = true
		return KEP.EPR_OK
	end
	
	
	local name = parseFriendlyNameFromURL(currentUrl)
	local gameName = parseGameNameFromURL(currentUrl)
	local zoneType = parseZoneTypeFromURL(currentUrl)
	if name == "" or gameName == "" then
		return
	end
	name = name .. " (" .. gameName .. ")"
	addToHistory(currentUrl, name)
	g_currentHistoryIndex = #History
	saveHistoryToFile()
end

function navigateBack()
	if g_currentHistoryIndex > 1 then
		zoneURL = History[g_currentHistoryIndex - 1]
		if zoneURL ~= nil then
            gotoURL(zoneURL.url)
			g_currentHistoryIndex = g_currentHistoryIndex - 1
			g_AddToHistory = false
		end
	end
end

function navigateForward()
	if g_currentHistoryIndex < #History then
		zoneURL = History[g_currentHistoryIndex + 1]
		if zoneURL ~= nil then
            gotoURL(zoneURL.url)
			g_currentHistoryIndex = g_currentHistoryIndex + 1
			g_AddToHistory = false
		end
	end
end

function addToFavorites(url, name, category)
	if category == nil then
		category = ""
	end
	
	if #Favorites >= MAX_FAVORITES then
		KEP_MessageBox("Maximum number of favorites exceeded. Cannot add to Favorites.")
		return
	end
	
	local success = addZoneURL(Favorites, url, name, category)
	if success == false then
		LogError("addToFavorites: FAILED url="..url)
		return false
	end
	Log("addToFavorites(): OK url=" .. url)
end

function addToHistory(url, name)
	if not addZoneURL(History, url, name, "HISTORY") then
		LogError("addToHistory: addZoneURL() FAILED - name='"..name.."' url='"..url.."'")
		return false
	end
	Log("addToHistory: OK - name='"..name.."' url='"..url.."'")
	
	while #History > MAX_HISTORY do
		table.remove(History, 1)
	end
end

function loadZoneURLListFromFile(filename)
	local zoneURLs = {}
	--Read whole file in
	local readfile, errmsg = KEP_FileOpenConfig(filename, "r")
	if readfile ~= 0 then
		--Read first line
		local ok, line, errMsg = KEP_FileReadLine( readfile )
		while ok == 1 do
			local zoneURL = parseStringAsZoneURL(line)
			if zoneURL ~= nil then
				table.insert(zoneURLs, zoneURL)
			end
			ok, line, errMsg = KEP_FileReadLine( readfile )
		end
		KEP_FileClose( readfile )
	else
		g_loadDefaultURLs = true
		local writefile, errmsg = KEP_FileOpenConfig(filename, "w")
		KEP_FileWriteLine(writefile, "")
		KEP_FileClose( writefile )
	end

	-- clear out any deprecated defaults
	purgeDeprecatedURLs(zoneURLs)

	return zoneURLs
end

function loadDefaultURLs()
	addToFavorites(getHomeURL(), "Home", "")
	addToFavorites(getParentGameBaseURL() .. "Kaneva City.zone", "Kaneva City", "")
	addToFavorites(getParentGameBaseURL() .. "Employment Agency.zone", "Employment Agency", "")
	saveFavoritesToFile()
	g_loadDefaultURLs = false
end

function purgeDeprecatedURLs(list)
	deleteZoneURLByName(list, "Ka-Ching!")
	deleteZoneURLByName(list, "Texas Hold'em")
end

function getParentGameBaseURL()
	local config = KEP_ConfigOpenWOK()
	local url = ""
	if config ~= nil then
		ok, url = KEP_ConfigGetString( config, "CurrentZoneURL", "" )
	end
	return url
end

function getHomeURL()
	local config = KEP_ConfigOpenWOK()
	local url = ""
	if config ~= nil then
		ok, url = KEP_ConfigGetString( config, "CurrentZoneURL", "" )
	end
	url = url .. KEP_GetLoginName() .. ".home"
	return url
end

function saveZoneURLListToFile(zoneUrls, filename)
	local writefile, errmsg = KEP_FileOpenConfig(filename, "w")
	if writefile ~= nil then
		for i,v in ipairs(zoneUrls) do
			local line = zoneURLToString(v)
			KEP_FileWriteLine(writefile, line .. "\n")
		end
		KEP_FileClose( writefile )
	end
end

function loadFavorites()	
	Favorites = loadZoneURLListFromFile(g_loginName..FAVORITES_FILENAME)
	saveFavoritesToFile()
	redoSearchableURLs()
end

function loadHistory()
	History = loadZoneURLListFromFile(g_loginName..HISTORY_FILENAME)
	saveHistoryToFile()
	redoSearchableURLs()
end

function saveFavoritesToFile()
	saveZoneURLListToFile(Favorites, g_loginName..FAVORITES_FILENAME)
	redoSearchableURLs()
end

function saveHistoryToFile()
	saveZoneURLListToFile(History, g_loginName..HISTORY_FILENAME)
	redoSearchableURLs()
end

function parseStringAsZoneURL(str)
	local commaFound = string.find(str, ",")
	if commaFound == nil then
		return nil
	end
	
	local url = string.sub(str, 1, commaFound - 1)	
	
	str = string.sub(str, commaFound + 1)
	commaFound = string.find(str, ",")
	if commaFound == nil then
		return nil
	end
	
	local name = string.sub(str, 1, commaFound - 1)	
	local category = string.sub(str, commaFound + 1)
	
	if category == nil then
		category = ""
	end	
	
	url = string.gsub(url, "stp", PREFIX)	
	
	local zoneURL = ZoneURL.create(url, name, category)
	return zoneURL
end

function zoneURLToString(zoneURL)
	local string = zoneURL.url .. "," .. zoneURL.name .. "," .. zoneURL.category
	return string
end

function updateHistory()
	local ev = KEP_EventCreate( ZONE_NAVIGATION_EVENT)
	KEP_EventSetFilter(ev, ZONE_NAV.HISTORY_INFO)
	KEP_EventEncodeNumber(ev, #History)
	for i,v in ipairs(History) do
		KEP_EventEncodeString(ev, v.name)
		KEP_EventEncodeString(ev, v.url)
	end
	KEP_EventQueue( ev)
end

function updateFavorites()
	local ev = KEP_EventCreate( ZONE_NAVIGATION_EVENT)
	KEP_EventSetFilter(ev, ZONE_NAV.FAVORITES_INFO)
	KEP_EventEncodeNumber(ev, #Favorites)
	for i,v in ipairs(Favorites) do
		KEP_EventEncodeString(ev, v.name)
		KEP_EventEncodeString(ev, v.url)
		KEP_EventEncodeString(ev, v.category)
	end
	KEP_EventQueue( ev)
end

-- DRF - Send ZoneNavigationEvent To Everyone With Current Url
function SendZoneNavigationEvent()
	Log("SendZoneNavigationEvent: currentUrl='"..g_currentUrl.."'")
	local ev = KEP_EventCreate( ZONE_NAVIGATION_EVENT)
	KEP_EventSetFilter(ev, ZONE_NAV.CURRENT_URL_INFO)
	KEP_EventEncodeString(ev, g_currentUrl)
	KEP_EventQueue( ev)

--Ankit ----- storing the current world url as a getset string
	currentWorldURL_GetSetID, currentWorldURL_GetSetType = GetSet_FindIdByName(KEP.gs, "CurrentWorldURL")
	if currentWorldURL_GetSetID == KEP.amrNotFound then
		currentWorldURL_GetSetID = GetSet_AddNewString(KEP.gs, "CurrentWorldURL", "", KEP.amfTransient)
	end
	if currentWorldURL_GetSetID == KEP.amrNotFound then
		Log("SendZoneNavigationEvent: Failed to save current world url")
	else
		GetSet_SetString(KEP.gs, currentWorldURL_GetSetID, g_currentUrl)
		Log("SendZoneNavigationEvent: current world url saved as a get set string with name 'CurrentWorldURL'")
	end
end

function redoSearchableURLs()
	SearchableURLs = {}
	for i,v in ipairs(Favorites) do
		local valid = true
		for j,w in ipairs(SearchableURLs) do
			if v.url == w.url then
				valid = false
				break
			end
		end
		if valid then
			table.insert(SearchableURLs, v)
		end
	end
	
	for i,v in ipairs(History) do
		local valid = true
		for j,w in ipairs(SearchableURLs) do
			if v.url == w.url then
				valid = false
				break
			end
		end
		if valid then
			table.insert(SearchableURLs, v)
		end
	end
end

function searchURLs(searchString, numResponses)
	searchString = string.lower(searchString)
	local validURLs = {}
	for i,v in ipairs(SearchableURLs) do
		if string.find(string.lower(v.url), searchString, 1, true) ~= nil or string.find(string.lower(v.name), searchString, 1, true) ~= nil then
			table.insert(validURLs, v)
		end
		
		if #validURLs >= numResponses then
			return validURLs
		end
	end
	return validURLs
end

function addZoneURL(list, url, name, category)
	if url == "" or name == "" or url == nil or name == nil then
		return false
	end
	
	if string.find(name, ",") ~= nil or string.find(url, ",") ~= nil or string.find(category, ",") ~= nil then
		KEP_MessageBox("Url, name, and category cannot contain ','")
		return
	end
	
	local zoneURL = ZoneURL.create(url, name, category)
	table.insert(list, zoneURL)
	return true
end

function deleteZoneURLByName(list, name)
	for i,v in ipairs(list) do
		if v.name == name then
			Log("deleteZoneURLByName: Removing " .. name .. " from Favorites")
			table.remove(list, i)
			return
		end
	end
end