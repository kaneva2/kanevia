--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..//Scripts//KEP.lua")
dofile("..//Scripts//GameGlobals.lua")
dofile("..//Scripts//NudgeIds.lua")
dofile("..//Scripts//CommandActions.lua")
dofile("..//Scripts//FameIds.lua")

FAME_CHECKLIST_INFO_EVENT = "GetFameChecklistInfoEvent"
GUIDE_COMPLETE_TASK_EVENT = "GuideCompleteTaskEvent"
REDEEM_FAME_PACKET_EVENT = "RedeemFamePacketEvent"
GUIDE_TASK_STATUS_EVENT = "GuideTaskStatusEvent"
guide_tasks = {}
updated = false
justCompletedTask = 0
sortOrder = {NudgeIds.CUSTOMIZEYOURAVATAR, NudgeIds.WALKAROUND, NudgeIds.CUSTOMIZEYOURHOME,
	NudgeIds.GOTOWORK, NudgeIds.INVITEFRIENDS, NudgeIds.CHATWITHSOMEONE, NudgeIds.RAVESOMEONE}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
end

function actionEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if(filter == CommandActions.Forward or filter == CommandActions.Backward) then
		completeTask(NudgeIds.WALKAROUND)
	end
end

function checklistInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local info = KEP_EventDecodeString(event)
	updated = true
	ParseChecklistInfo(info)
end

function guideTaskCompletedHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local nudgeId = KEP_EventDecodeNumber(event)
	completeTask(nudgeId)
end

function completeTask(nudgeId)
	if(updated == true) then
		for k,v in pairs(guide_tasks) do
			if tonumber(k) == nudgeId then
				if(guide_tasks[k].completed == false) then
					guide_tasks[k].completed = true
					local completeTaskEvent = KEP_EventCreate( REDEEM_FAME_PACKET_EVENT)
					if(completeTaskEvent ~= 0) then
						KEP_EventEncodeNumber(completeTaskEvent, FameIds.WORLD_FAME)
						KEP_EventEncodeNumber(completeTaskEvent, getPacketIDfromNudgeId(nudgeId))
						KEP_EventAddToServer(completeTaskEvent)
						KEP_EventQueue( completeTaskEvent)
					end
					sendTaskStatusEvent(nudgeId)
					justCompletedTask = nudgeId

					-- show a status event 
					--KEP_SendChatMessage(ChatType.System, "Congratulations! You just completed the Guide task '" .. guide_tasks[k].name .. "'")
					local statusEvent = KEP_EventCreate("PlayerShowStatusInfoEvent")
					KEP_EventEncodeNumber(statusEvent, 0)
					KEP_EventEncodeString(statusEvent, "Congratulations! You just completed the Guide task '" .. guide_tasks[k].name .. "'")
					KEP_EventEncodeNumber(statusEvent, 3)
					KEP_EventEncodeNumber(statusEvent, 0)
					KEP_EventEncodeNumber(statusEvent, 255)
					KEP_EventEncodeNumber(statusEvent, 0)
					KEP_EventSetFilter(statusEvent, 4)
					KEP_EventQueue(statusEvent)
				end
			end
		end
	end
end

function sendTaskStatusEvent(nudgeId)
	for k,v in pairs(guide_tasks) do
		if tonumber(k) == nudgeId then
			local ev = KEP_EventCreate( GUIDE_TASK_STATUS_EVENT)
			KEP_EventSetFilter(ev, nudgeId)
			KEP_EventEncodeString(ev, guide_tasks[k].name)
			KEP_EventEncodeString(ev, guide_tasks[k].description )
			KEP_EventEncodeNumber(ev, guide_tasks[k].rewardsEarned)
			KEP_EventEncodeNumber(ev, (guide_tasks[k].completed and 1 or 0)) -- converts to number
			KEP_EventQueue( ev)
		end
	end
end

function sendJustCompleted()
	local ev = KEP_EventCreate( GUIDE_TASK_STATUS_EVENT)
	if(justCompletedTask ~= 0) then
		for k,v in pairs(guide_tasks) do
			if tonumber(k) == justCompletedTask then
				local ev = KEP_EventCreate( GUIDE_TASK_STATUS_EVENT)
				KEP_EventSetFilter(ev, 4)
				KEP_EventEncodeNumber(ev, justCompletedTask)
				KEP_EventEncodeString(ev, guide_tasks[k].name)
				KEP_EventEncodeString(ev, guide_tasks[k].description )
				KEP_EventEncodeNumber(ev, guide_tasks[k].rewardsEarned)
				KEP_EventEncodeNumber(ev, (guide_tasks[k].completed and 1 or 0)) -- converts to number
				KEP_EventQueue( ev)
			end
		end
		justCompletedTask = 0 --set to 0 since it is no longer just completed, reward message is expired
	end
end

function guideTaskStatusHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if(filter == 1) then --request one nudge update
		local nudgeId = KEP_EventDecodeNumber(event)
		sendTaskStatusEvent(nudgeId)
	elseif filter == 2 then --request all nudges
		sendAllTasks()
	elseif filter == 3 then --get Just Completed Task, used by Guide_TaskList to display context sensitive reward message
		sendJustCompleted()
	end
	return KEP.EPR_OK
end

function sendAllTasks()
	local index = 1
	for k,v in pairs(guide_tasks) do
		for k,v in pairs(guide_tasks) do
			if(tonumber(k) == sortOrder[index]) then
				sendTaskStatusEvent(tonumber(k))
			end
		end
		index = index + 1
	end
end

function ParseChecklistInfo(info)
	numRec = 7
	guide_tasks = {}
	local strPos = 0    	-- current position in string to parse
	for i=1,numRec do
		s, strPos, nudgeId = string.find(info, "<NudgeId>(.-)</NudgeId>", strPos)
		s, strPos, name = string.find(info, "<Name>(.-)</Name>", strPos)
		s, strPos, description = string.find(info, "<Desc>(.-)</Desc>", strPos)
		s, strPos, rewards = string.find(info, "<Rewards>(.-)</Rewards>", strPos)
		s, strPos, completed = string.find(info, "<IsComplete>(.-)</IsComplete>", strPos)

		if(completed == "true") then
			completed = true
		else
			completed = false
		end
		AddNewTask(nudgeId, name, description, rewards, completed)
	end
	sendAllTasks()
end

function AddNewTask(nudgeId, name, description, rewardsEarned, completed)
	local task = {}
	task.nudgeId = nudgeId
	task.name = name
	task.description = description
	task.rewardsEarned = rewardsEarned
	task.completed = completed
	guide_tasks[nudgeId] = task
end
