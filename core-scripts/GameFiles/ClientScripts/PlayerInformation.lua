--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\MenuScripts\\WebSuffix.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\CreditBalances.lua")

g_userID = 0
g_kanevaUserId = 0
g_playerID = 0
g_playerCredits = 0
g_playerRewards = 0

g_playerInventory = {}

PLAYER_INFO_EVENT = "PlayerInfoEvent"
PLAYER_INFO_USERID_REQUEST = 1
PLAYER_INFO_USERID_RESPONSE = 2
PLAYER_INFO_PLAYERID_REQUEST = 3
PLAYER_INFO_PLAYERID_RESPONSE = 4
PLAYER_INFO_CREDITS_REWARDS_REQUEST = 5
PLAYER_INFO_CREDITS_REWARDS_RESPONSE = 6

function PlayerInformation_BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.USER_ID then
		xml = KEP_EventDecodeString( event )
		ParseUserID(xml)
	elseif filter ==  WF.HUD_USER_ID then
		xml = KEP_EventDecodeString( event )
		ParseKanevaUserID(xml)
	elseif filter == WF.PLAYER_INVENTORY then
		xml = KEP_EventDecodeString( event )
		g_playerInventory = {}
		decodeInventoryItemsFromWeb( g_playerInventory, xml, false )
	elseif filter == WF.HUD_BALANCES then
		local estr = KEP_EventDecodeString( event )
		g_playerCredits, g_playerRewards = ParseBalances(estr)
	end
end

--Add this to the InitializeKEPEventHandlers of whatever script includes this file
function PlayerInformation_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "PlayerInformation_BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "PlayerInformation_BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "PlayerInfoEventHandler", PLAYER_INFO_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( PLAYER_INFO_EVENT, KEP.MED_PRIO )
end

function PlayerInfoEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local ev = KEP_EventCreate( PLAYER_INFO_EVENT)

	if filter == PLAYER_INFO_USERID_REQUEST then
		KEP_EventEncodeNumber(ev, g_kanevaUserId)
		KEP_EventSetFilter(ev, PLAYER_INFO_USERID_RESPONSE)
		KEP_EventQueue( ev)
	elseif filter == PLAYER_INFO_PLAYERID_REQUEST then
		KEP_EventEncodeNumber(ev, g_playerID)
		KEP_EventSetFilter(ev, PLAYER_INFO_PLAYERID_RESPONSE)
		KEP_EventQueue( ev)
	elseif filter == PLAYER_INFO_CREDITS_REWARDS_REQUEST then
		KEP_EventEncodeNumber(ev, g_playerCredits)
		KEP_EventEncodeNumber(ev, g_playerRewards)
		KEP_EventSetFilter(ev, PLAYER_INFO_CREDITS_REWARDS_RESPONSE)
		KEP_EventQueue( ev)
	end
end

function ParseKanevaUserID( browserString )
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string

	s, e, result = string.find(browserString, "<ReturnCode>(%d+)</ReturnCode>")

	if result == "0" then
		-- Parse needed info from XML
		s, e, result = string.find(browserString, "<user_id>(%d+)</user_id>", e)
		g_kanevaUserId = tonumber(result)
		s, e, result = string.find(browserString, "<player_id>(%d+)</player_id>", e)
		g_playerID = tonumber(result)
	end
end

function ParseUserID( browserString )
	local result = nil  -- result description
	local s, e = 0      -- start and end of captured string

	s, e, result = string.find(browserString, "<ReturnCode>(%d+)</ReturnCode>")

	if result == "0" then
		-- Parse needed info from XML
		s, e, result = string.find(browserString, "<user_id>(%d+)</user_id>", e)
		g_userID = tonumber(result)
	end
end
