--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\Scripts\\UIGlobals.lua") 

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "UpdateUILayoutEventHandler",	"UpdateUILayoutEvent",	KEP.MED_PRIO )
	-- MED_PRIO: Make sure it stays medium priority so other menus can handle any resizing issues first (eg HUDHelper.lua)
	KEP_EventRegisterHandler( "clientResizeEventHandler",	"ClientResizeEvent",	KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "UpdateUILayoutEvent",	KEP.MED_PRIO )
	KEP_EventRegister( "UILayoutUpdatedEvent",	KEP.MED_PRIO )
	KEP_EventRegister( "ClientResizeEvent",		KEP.HIGH_PRIO )
end

function clientResizeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	updateUILayout()
end

function UpdateUILayoutEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	updateUILayout()
end

-- Update the locations of all menus contained within the UIConfig config file
function updateUILayout()
	local config = KEP_ConfigOpen("UIConfig.xml","UIConfig")

	if config == nil then
		return
	end

	local menusUpdated = {}

	local ok, menuList = KEP_ConfigGetString(config, "MenuList", "")

	for menu in string.gmatch(menuList, "[^%s]+") do
		if menu ~= "HUDKaneva" then
			menusUpdated = updateMenuLocation(menu, config, menusUpdated)
		end
	end

	KEP_EventQueue(KEP_EventCreate("UILayoutUpdatedEvent"))
end

-- Decides how to update the location of a specific menu based on the info in UIConfig
-- Recursively updates any parents menus before updated itself to avoid any race conditions
function updateMenuLocation(menuName, configInfo, tblUpdated)
	if MenuIsOpen(menuName) and not tblUpdated[menuName] then
		local hasParent, parent = KEP_ConfigGetString(configInfo, menuName .. "Parent", "")
		
		if hasParent == 1 then
			tblUpdated = updateMenuLocation(parent, configInfo, tblUpdated)

			if MenuIsOpen(parent) then
				moveMenuParented(menuName, parent, configInfo)
			else
				moveMenu(menuName, configInfo)
			end
		else
			moveMenu(menuName, configInfo)
		end
	end

	tblUpdated[menuName] = true

	return tblUpdated
end

-- Move menu that has a parent, gets info about parent before passing in to general move menu file
function moveMenuParented(menuName, parentMenu, configInfo)
	-- Setup Variables
	local pinX, pinY = 0, 0

	local xOk, pAlignX = KEP_ConfigGetString(configInfo, menuName .. "ParentX", 0)
	local yOk, pAlignY = KEP_ConfigGetString(configInfo, menuName .. "ParentY", 0)

	-- Set pin locations
	-- Horizontal
	local parentX = Dialog_GetLocationX(MenuHandle(parentMenu))
	local parentW = Dialog_GetWidth(MenuHandle(parentMenu))
	if pAlignX == ALIGN.LEFT then
		pinX = parentX
	elseif pAlignX == ALIGN.CENTER then
		pinX = parentX + (parentW/2)
	elseif pAlignX == ALIGN.RIGHT then
		pinX = parentX + parentW
	else
		pinX = parentX + (tonumber(pAlignX) or 0)
	end

	-- Vertical
	local parentY = Dialog_GetLocationY(MenuHandle(parentMenu))
	local parentH = Dialog_GetHeight(MenuHandle(parentMenu))
	if pAlignY == ALIGN.TOP then
		pinY = parentY
	elseif pAlignY == ALIGN.CENTER then
		pinY = parentY + (parentH/2)
	elseif pAlignY == ALIGN.BOTTOM then
		pinY = parentY + parentH
	else
		pinY = parentY + (tonumber(pAlignY) or 0)
	end

	moveMenu(menuName, configInfo, pinX, pinY)
end

-- Move a menu based on values from config file.
-- Options pinX and pinY values for where to pin it to if not just the edges of the screen
function moveMenu(menuName, configInfo, pinX, pinY)
	-- Setup Variables
	local x, y = pinX or 0, pinY or 0
	local winW, winH = pinX, pinY

	if not pinX or not pinY then
		winW, winH = KEP_GetClientRectSize()
	end

	--Horizontal
	local xOk, alignX	= KEP_ConfigGetString(configInfo, menuName .. "X", 0)
	local xOk, offsetX	= KEP_ConfigGetString(configInfo, menuName .. "OffsetX", 0)
	if alignX == ALIGN.LEFT then
		x = x
	elseif alignX == ALIGN.CENTER then
		x = (winW - Dialog_GetWidth(MenuHandle(menuName)))/2
	elseif alignX == ALIGN.RIGHT then
		x = winW -  Dialog_GetWidth(MenuHandle(menuName))
	else
		x = x + tonumber(alignX) or 0
	end
	x = x + offsetX

	-- Vertical
	local yOk, alignY = KEP_ConfigGetString(configInfo, menuName .. "Y", 0)
	local yOk, offsetY	= KEP_ConfigGetString(configInfo, menuName .. "OffsetY", 0)
	if alignY == ALIGN.TOP then
		y = y
	elseif alignY == ALIGN.CENTER then
		y = (winH - Dialog_GetHeight(MenuHandle(menuName)))/2
	elseif alignY == ALIGN.BOTTOM then
		y = winH - Dialog_GetHeight(MenuHandle(menuName))
	else
		y = y + tonumber(alignY) or 0
	end
	y = y + offsetY

	Dialog_SetLocation(MenuHandle(menuName), x, y)
end
