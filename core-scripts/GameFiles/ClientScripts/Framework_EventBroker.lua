--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Framework_EventBroker.lua
--
-- Catches events intended for the Framework from menus that don't
-- use the LibClient_Common event registration system
-------------------------------------------------------------------------------
dofile("..\\MenuScripts\\LibClient_Common.lua")

-- Client event registration
function InitializeKEPEvents(dispatcher, handler, debugLevel)    
	-- Register the broker event so others can register handlers
	KEP_EventRegister("FrameworkIntendedEvent", KEP.HIGH_PRIO)
end

-- Client event handler registration
function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler("frameworkIntendedEventHandler", "FrameworkIntendedEvent", KEP.HIGH_PRIO)
end

-- Handles events when the player changes modes
function frameworkIntendedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local eventName
	local eventData = {}
	-- Extract the name of the event we'll be sending
	if KEP_EventMoreToDecode(event) ~= 0 then
		eventName = KEP_EventDecodeString(event)
	end
	if KEP_EventMoreToDecode(event) ~= 0 then
		eventData = KEP_EventDecodeString(event)
	end
	eventData = JSON.decode(eventData)
	
	Events.sendEvent(eventName, eventData)
end