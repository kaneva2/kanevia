--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\SoundGlobals.lua" )
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\MenuScripts\\WebSuffix.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\Scripts\\CommandActions.lua")
dofile("..\\Scripts\\NotificationTypes.lua")
dofile("..\\Scripts\\FameIds.lua")
dofile("..\\Scripts\\ClientTickler.lua")
dofile("..\\Scripts\\TransactionTypes.lua")
dofile("..\\ClientScripts\\MathHelper.lua")
dofile("..\\Scripts\\ScriptClientEventTypes.lua")
dofile("..\\Scripts\\ScriptServerEventTypes.lua")
dofile("..\\Scripts\\EnvironmentIds.lua")
dofile("..\\MenuScripts\\Framework_EventHelper.lua")
dofile("..\\MenuScripts\\UGCZoneLiteProxy.lua")

-- DRF - Allow ClientTickler Event Handlers
g_allowClientTickler = true
g_allowClientTicklerFriendRequest = true
g_allowClientTicklerPlayerRaved = true
g_allowClientTicklerNewMessage = true
g_allowClientTicklerInWorldInvite = true
g_allowClientTicklerBalanceChange = true
g_allowClientTicklerEventReminder = true
g_allowClientTicklerInWorldGiftInvite = true
g_allowClientTicklerWebWorldInvite = true


PARSE_FRIEND_USERID = 4
PARSE_FRIEND_REQUEST = 5
PARSE_RAVE_COUNTS = 6
PARSE_RAVE_RESULTS = 7

g_parseType = -1

g_username = ""
g_userID = 0
g_friendUsername = ""
g_sourceMenu = ""
g_friendUserId = 0

g_oneTime = false

g_volPercentDefault = 50.0

-- Media Object Params
g_mUrl = ""
g_mParams= ""
g_mVolPct = 100.0
g_mRadiusAudio = 0.0 -- assume global
g_mRadiusVideo = 0.0 -- assume global
g_mediaObjects = {}

GET_USERID_SUFFIX = "kgp/userProfile.aspx?action=getUserIdFromAvatarName&avatar="
GET_USER_BLASTS_SUFFIX = "kgp/userProfile.aspx?action=getTotalBlastsFromNow"
UPDATE_SUFFIX = "kgp/updateFriend.aspx?userId="
ACTION_SUFFIX = "&action="

UPDATE_HUD_INTERVAL = 60

g_appAssetFilePath = ""
g_appAssetObjectId = 0

g_tempMuteApplied = false

----Framework----
g_playerModeActive = false
g_frameworkEnabled = nil
g_firstEnabledFramework = false
g_defaultSpawnPointFetched = false

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "scriptClientEventHandler", "ScriptClientEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "playerAnimationHandler",  "PlayerAnimationEvent", KEP.MED_PRIO )
--	KEP_EventRegisterHandler( "latencyUpdateEventHandler",  "LatencyUpdateEvent", KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "actionEventHandler", "ActionEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "cameraRotateActionEventHandler", "ActionEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "updateHudEventHandler",  "UpdateHUDEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "keyChangedEventHandler", "KeyChangedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "addFriendEventHandler", "AddFriendEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "premiumItemEventHandler", "PremiumItemEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "textEventHandler", "TextEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "answerEventHandler", "YesNoAnswerEvent", KEP.MED_PRIO )
	
	-- DEPRECATED
	KEP_EventRegisterHandlerV( "updateDynamicObjectUrlEventHandler", "UpdateDynamicObjectUrlEvent", KEP.HIGH_PRIO )

	-- DRF - Added
	KEP_EventRegisterHandlerV( "updateDynamicObjectMediaEventHandler", "UpdateDynamicObjectMediaEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "UpdateMediaEventHandler", "UpdateMediaEvent", KEP.MED_PRIO )

	KEP_EventRegisterHandler( "sendRaveEventHandler",  "SendRaveEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "acceptFriendEventHandler", "AcceptFriendEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "dynamicObjectSpawnMonitorStartedHandler", "DynamicObjectSpawnMonitorStartedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "dynamicObjectSpawnMonitorCompletedHandler", "DynamicObjectSpawnMonitorCompletedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "chatMinimizeEventHandler", "ChatMinimizeEvent", KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "trayPublicChatEventHandler", "Tray_PublicChatEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "trayPrivateMessageEventHandler", "Tray_PrivateMessageEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "appAssetNameConflictEventHandler", "3DAppAssetUploadNameConflictEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "temporarySoundMuteEventHandler", "TemporarySoundMuteEvent", KEP.MED_PRIO )

	-- Needed for default spawn point acquisition
	UGCZoneLiteProxy_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	
    -- DRF Added
	KEP_EventRegisterHandler( "clientTicklerHandler", "ClientTickler", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AllowClientTicklerEventHandler", "AllowClientTicklerEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AllowClientTicklerFriendRequestEventHandler", "AllowClientTicklerFriendRequestEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AllowClientTicklerPlayerRavedEventHandler", "AllowClientTicklerPlayerRavedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AllowClientTicklerNewMessageEventHandler", "AllowClientTicklerNewMessageEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AllowClientTicklerInWorldInviteEventHandler", "AllowClientTicklerInWorldInviteEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AllowClientTicklerBalanceChangeEventHandler", "AllowClientTicklerBalanceChangeEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AllowClientTicklerEventReminderEventHandler", "AllowClientTicklerEventReminderEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AllowClientTicklerInWorldGiftInviteEventHandler", "AllowClientTicklerInWorldGiftInviteEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AllowClientTicklerWebWorldInviteEventHandler", "AllowClientTicklerWebWorldInviteEvent", KEP.MED_PRIO )
	
	KEP_EventRegisterHandler( "returnFrameworkStateHandler", "FrameworkState", KEP.HIGH_PRIO )

	KEP_EventRegisterHandler( "newZoneLoadedHandler", "ZoneLoadedEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "volumeChangedEventHandler", "VolumeChangedEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	gScriptDir = "GameFiles\\ClientScripts\\"
	KEP_EventRegister( "BlastsUpdateEvent", KEP.MED_PRIO )
	KEP_EventRegister(  "PlayerAnimationEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PurchaseRaveEvent", KEP.MED_PRIO )
	KEP_EventRegister(  "UpdateHUDEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PremiumItemEvent", KEP.MED_PRIO )
	KEP_EventRegister( "AddFriendEvent", KEP.MED_PRIO )

	-- DEPRECATED
	KEP_EventRegisterSecureV( "UpdateDynamicObjectUrlEvent", KEP.MED_PRIO, KEP.ESEC_SERVER, KEP.ESEC_CLIENT )
	
	-- DRF - Added
	KEP_EventRegisterSecureV( "UpdateDynamicObjectMediaEvent", KEP.MED_PRIO, KEP.ESEC_SERVER, KEP.ESEC_CLIENT )
	KEP_EventRegister( "UpdateMediaEvent", KEP.MED_PRIO )

	KEP_EventRegister(  "SendRaveEvent", KEP.MED_PRIO )
	KEP_EventRegister(  "PlayerUserIdEvent", KEP.MED_PRIO )
	KEP_EventRegister(  "BalanceChangedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "AcceptFriendEvent", KEP.MED_PRIO )
	KEP_EventRegister( "DynamicObjectSpawnMonitorStartedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "DynamicObjectSpawnMonitorCompletedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ChatMinimizeEvent", KEP.MED_PRIO )
	KEP_EventRegister( "Tray_PublicChatEvent", KEP.MED_PRIO )
	KEP_EventRegister( "Tray_PrivateMessageEvent", KEP.MED_PRIO )
	KEP_EventRegister( "3DAppAssetUploadNameConflictEvent", KEP.MED_PRIO )
	KEP_EventRegister( "YesNoBoxEvent", KEP.MED_PRIO )
	KEP_EventRegister( "YesNoAnswerEvent", KEP.MED_PRIO )
	KEP_EventRegister( "EventRewardsEarnedEvent", KEP.MED_PRIO )
	--KEP_EventRegister( "ReturnFrameworkState", KEP.MED_PRIO )
	KEP_EventRegister( "GetFrameworkState", KEP.MED_PRIO )
	KEP_EventRegister( "FRAMEWORK_INCREMENT_LOOT_COUNT", KEP.MED_PRIO )
	KEP_EventRegister( "TemporarySoundMuteEvent", KEP.MED_PRIO )

	-- Needed for default spawn point acquisition
	UGCZoneLiteProxy_InitializeKEPEvents(dispatcher, handler, debugLevel)
	
	-- DRF Added
	KEP_EventRegister( "ClientTickler", KEP.MED_PRIO )
	KEP_EventRegister( "AllowClientTicklerEvent", KEP.MED_PRIO )
	KEP_EventRegister( "AllowClientTicklerFriendRequestEvent", KEP.MED_PRIO )
	KEP_EventRegister( "AllowClientTicklerPlayerRavedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "AllowClientTicklerNewMessageEvent", KEP.MED_PRIO )
	KEP_EventRegister( "AllowClientTicklerInWorldInviteEvent", KEP.MED_PRIO )
	KEP_EventRegister( "AllowClientTicklerBalanceChangeEvent", KEP.MED_PRIO )
	KEP_EventRegister( "AllowClientTicklerEventReminderEvent", KEP.MED_PRIO )
	KEP_EventRegister( "AllowClientTicklerInWorldGiftInviteEvent", KEP.MED_PRIO )
	KEP_EventRegister( "AllowClientTicklerWebWorldInviteEvent", KEP.MED_PRIO )
	
	KEP_EventRegister("InventoryUpdateEvent", KEP.MED_PRIO)
end

function MediaParamsStr()
	return "'"..g_mUrl.." + "..g_mParams.."' volPct="..g_mVolPct.." radius=["..g_mRadiusAudio.." "..g_mRadiusVideo.."]"
end

function MediaObjectAdd(objId, media)
	objId = tonumber(objId)
--	Log("MediaObjectAdd: "..ObjectToString(objId).." '"..media.."'")
	g_mediaObjects[objId] = media			
	SendUpdateMediaEvent(objId)
end

function SendUpdateMediaEvent(objId)
	objId = tonumber(objId)
	local media = g_mediaObjects[objId] or "<None>"
--	Log("SendUpdateMediaEvent: "..ObjectToString(objId).." '"..media.."'")

	-- Legacy Event (filter=0)
	local evt = KEP_EventCreate( "UpdateMediaEvent" )
	KEP_EventSetFilter( evt, 0 )
	KEP_EventEncodeString(evt, media)
	KEP_EventQueue( evt )
	
	-- New Event (filter=5)
	local evt = KEP_EventCreate( "UpdateMediaEvent" )
	KEP_EventSetFilter( evt, 5 )
	KEP_EventEncodeNumber(evt, objId)
	KEP_EventEncodeString(evt, media)
	KEP_EventQueue( evt )
end

function UpdateMediaEventHandler(dispatcher, fromNetid, event, eventid, filter, objId)

	if (filter == 1) then

		-- Send UpdateMediaEvent For Requested Object
		objId = tonumber(KEP_EventDecodeString(event))
		SendUpdateMediaEvent(objId)

	elseif (filter == 3) then
		
		--placementId request
		local placementId = 0
		for k,v in pairs(g_mediaObjects) do			
			placementId = k
			break
		end				
		
		local evt = KEP_EventCreate( "UpdateMediaEvent" )
		KEP_EventSetFilter( evt, 4 )
		KEP_EventEncodeNumber( evt, placementId )
		KEP_EventEncodeNumber( evt, g_mVolPct )
		KEP_EventEncodeNumber( evt, g_mRadiusAudio )
		KEP_EventEncodeNumber( evt, g_mRadiusVideo )
		KEP_EventQueue( evt )
	end
end

-- DEPRECATED - Use updateDynamicObjectMediaEventHandler() Instead!
function updateDynamicObjectUrlEventHandler(dispatcher, fromNetid, event, eventid, filter, objId)
	
	-- Decode Event Media Params
	g_mUrl = KEP_EventDecodeString( event ) or ""
	g_mParams = KEP_EventDecodeString( event ) or ""
	g_mVolPct = 100.0
	g_mRadiusAudio = 0.0 -- global presence
	g_mRadiusVideo = 0.0 -- global presence
	
--	Log("updateDynamicObjectUrlEventHandler: "..MediaParamsStr())

	-- Get Object Info
	local g_objName, g_objDescription, g_objCanPlayMovie, g_objIsAttachable, g_objInteractive, g_objIsLocal, g_objDerivable, g_objAssetId, g_objFriendId, g_objCustomTexture, g_objGlobalId, g_objInvType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(objId)
	
	--this is a flash game
	if ( g_objInteractive == 1 ) then
--		Log("updateDynamicObjectUrlEventHandler: ->UpdateMediaEvent(filter=2 interactive)")
		local evt = KEP_EventCreate( "UpdateMediaEvent" )
		KEP_EventSetFilter( evt, 2 )
		KEP_EventQueue( evt )
	else
		-- Update Cached Flash Object Info
		local s, trash, media = string.find(g_mParams, "&.*/(.-)&")
		if (media == nil) then media = g_mUrl end
		MediaObjectAdd(objId, media)
	end 
end

-- DRF - Added
function updateDynamicObjectMediaEventHandler(dispatcher, fromNetid, event, eventid, filter, objId)
	
	-- Decode Event Media Params
	g_mUrl = KEP_EventDecodeString(event) or ""
	g_mParams = KEP_EventDecodeString(event) or ""
	g_mVolPct = KEP_EventDecodeNumber(event) or 100.0
	g_mRadiusAudio = KEP_EventDecodeNumber(event) or 0.0 -- global presence
	g_mRadiusVideo = KEP_EventDecodeNumber(event) or 0.0 -- global presence
	
--	Log("updateDynamicObjectMediaEventHandler: "..MediaParamsStr())

	-- Get Object Info
	local g_objName, g_objDescription, g_objCanPlayMovie, g_objIsAttachable, g_objInteractive, g_objIsLocal, g_objDerivable, g_objAssetId, g_objFriendId, g_objCustomTexture, g_objGlobalId, g_objInvType, textureURL, gameItemId, playerId = KEP_DynamicObjectGetInfo(objId)

	--this is a flash game
	if ( g_objInteractive == 1 ) then
--		Log("updateDynamicObjectMediaEventHandler: ->UpdateMediaEvent(filter=2 interactive)")
		local evt = KEP_EventCreate( "UpdateMediaEvent" )
		KEP_EventSetFilter( evt, 2 )
		KEP_EventQueue( evt )
	else
		-- Update Cached Flash Object Info
		local s, trash, media = string.find(g_mParams, "&.*/(.-)&")
		if (media == nil) then media = g_mUrl end
		MediaObjectAdd(objId, media)
	end
end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local answer = KEP_EventDecodeNumber( event )	-- 1: Yes, 0 = No
	if filter == WF.CONFIRM_FRIEND_REQUEST then
		if answer==1 then
			SendFriendRequest(g_friendUserId)
		end
		return KEP.EPR_CONSUMED	
	elseif filter == WF.APP_ASSET_NAMECONFLICT then
		fileName = ""
		filename_start, filename_end, fullname, extension = string.find(g_appAssetFilePath, "(.-)%.(.+)")
		filename_start, filename_end, fileNameNoExtension = string.find(fullname, ".+\\(.+)")
		if answer == 1 then
			fileName = fileNameNoExtension .. "." .. extension
			KEP_UploadAssetTo3DApp( fileNameNoExtension, g_appAssetFilePath)
		elseif answer == 0 then		
			fileNameNoExtension = fileNameNoExtension .. "_copy"
			fileName = fileNameNoExtension .. "." .. extension
			KEP_UploadAssetTo3DApp(fileNameNoExtension, g_appAssetFilePath)
		end

		-- attach the script to the object
		if ( g_appAssetObjectId ~= 0 ) then
			local ev = KEP_EventCreate( "ScriptServerEvent")
			KEP_EventSetFilter(ev, ScriptServerEventTypes.AttachScript)
			KEP_EventAddToServer(ev)
			KEP_EventEncodeNumber(ev, g_appAssetObjectId)
			KEP_EventEncodeNumber(ev, 0)
			KEP_EventEncodeNumber(ev, 0)
			KEP_EventEncodeString(ev, fileName)
			KEP_EventQueue( ev)
		end
		g_appAssetFilePath = ""
		g_appAssetObjectId = 0
		return KEP.EPR_CONSUMED
	end
end

function sendRaveEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_friendUserId = KEP_EventDecodeNumber( event )
	g_friendUsername = KEP_EventDecodeString(event)
	g_sourceMenu = KEP_EventDecodeString(event)
	g_parseType = PARSE_RAVE_COUNTS
	makeWebCall(GameGlobals.WEB_SITE_PREFIX .. PROFILE_COUNTS_SUFFIX.."&userId=" .. g_friendUserId, WF.DEFAULT_HANDLER)
end

function addFriendEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_friendUsername = KEP_EventDecodeString(event)
	g_parseType = PARSE_FRIEND_USERID
	g_sourceMenu = KEP_EventDecodeString(event)
	makeWebCall(GameGlobals.WEB_SITE_PREFIX..GET_USERID_SUFFIX..g_friendUsername, WF.DEFAULT_HANDLER)
end

function acceptFriendEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local friendId = KEP_EventDecodeNumber(event)
	makeWebCall( GameGlobals.WEB_SITE_PREFIX.."kgp/messagecenter.aspx?action=AcceptFriend&friendId="..friendId, WF.MESSAGES)
	KEP_MessageBox("Friend Request Accepted")
end

function premiumItemEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	if filter == 1 then
		globalId = KEP_EventDecodeNumber(event)
		quantity = KEP_EventDecodeNumber(event)
		if MenuIsClosed("PremiumItemPurchase.xml") then
			MenuOpen("PremiumItemPurchase.xml")
			local premiumItemEvent = KEP_EventCreate( "PremiumItemEvent")
			KEP_EventSetFilter( premiumItemEvent, 3024 )
			KEP_EventEncodeNumber( premiumItemEvent, globalId )
			KEP_EventEncodeNumber( premiumItemEvent, quantity )
			KEP_EventQueue( premiumItemEvent )
		end
	end
end

function textEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	if KEP_IsCommunity() then --we are in WoK, so ignore this event
		return KEP.EPR_NOT_PROCESSED
	end
	
	--This is a hack for 3dapps to be able to recognize NPC touches in the 3d app lobby
	--We currently don't have any APIs for NPCs, so in the mean time, we will send a menu event to
	--the scripts with a paramater indicating CMD_LOOT has been triggered on the lobby NPC
	CMD_LOOT = 2009
	if filter == CMD_LOOT then --capture all loot commands in 3dapps as NPC touches in 3d app lobby
		KEP_SendMenuEvent( 1, CMD_LOOT)
		return KEP.EPR_CONSUMED
	end
end

function actionEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

    if filter == CommandActions.Chat then
		-- TODO
	elseif filter == CommandActions.OpenBuildMode and g_playerModeActive == false and KEP_IsOwnerOrModerator() then
		local state = KEP_EventDecodeNumber(event)
		if state == 1 then
			local ev = KEP_EventCreate(  "EnterBuildModeEvent")

			-- if were in build mode already close it, otherwise open
			if MenuIsOpen("HUDBuild.xml") then
				KEP_EventSetFilter(ev, 0)
			else
				KEP_EventSetFilter(ev, 1)
			end

			KEP_EventEncodeNumber(ev, -1) -- Don't change mode
			KEP_EventQueue( ev)
		end
	end
end

g_cameraRotateLastTickCount = nil
g_cameraRotateEventStart_ZoneIndex = nil
g_cameraRotateEventStart_X = nil
g_cameraRotateEventStart_Y = nil
g_cameraRotateEventStart_Z = nil

function cameraRotateActionEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	
	if (filter == CommandActions.CameraRotate or filter == CommandActions.CameraCharRotate) then
		-- Since we can't detect when the user STOPS left-dragging, we'll have to check the elapsed time since the last
		-- mouse drag event and if it's low enough, pretend that the user is still dragging. Yes, this fails if the user
		-- holds down the left click while rotating with the keyboard.
		local tickCount = KEP_CurrentTime()
		if g_cameraRotateLastTickCount ~= nil then
			if tickCount < g_cameraRotateLastTickCount then -- Be careful of wrapping around
				tickCount = tickCount + 4294967295 -- 0xFFFFFFFF
			end
			if (tickCount - g_cameraRotateLastTickCount) > 100 then
				g_cameraRotateEventStart_ZoneIndex = nil -- It's been too long since the last mouse event, so cancel the drag.
			end
		end
		g_cameraRotateLastTickCount = tickCount

		local camX
		local camY
		local camZ
		camX, camY, camZ = KEP_GetPlayerCameraOrientation()
		zoneIndex = KEP_GetCurrentZoneIndex()
		if g_cameraRotateEventStart_ZoneIndex == nil or g_cameraRotateEventStart_ZoneIndex ~= zoneIndex then
			g_cameraRotateEventStart_ZoneIndex = zoneIndex
			g_cameraRotateEventStart_X = camX
			g_cameraRotateEventStart_Y = camY
			g_cameraRotateEventStart_Z = camZ
		end
		
		local dotP = math.abs(dotProduct(camX, camY, camZ, g_cameraRotateEventStart_X, g_cameraRotateEventStart_Y, g_cameraRotateEventStart_Z))

		-- Make sure they look around "enough" (about 18 degrees) before awarding it. 
		-- Also, this hides a bug where closing a dialog box causes a rotate event
		if dotP < 0.95 then
			KEP_EventRemoveHandler("cameraRotateActionEventHandler", "ActionEvent")
		end
    end
end

-- Handles events from the server
function scriptClientEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == ScriptClientEventTypes.PlayerLoadMenu then -- MENU OPEN
		menuName = KEP_EventDecodeString(event)
		MenuOpen(menuName)
	end
	return KEP.EPR_OK
end

--------------------------------------------------------------------------------------------------------------------
-- DRF - ClientTickler Event Handlers
--------------------------------------------------------------------------------------------------------------------

function AllowClientTicklerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local allow = KEP_EventDecodeNumber( event )
    g_allowClientTickler = (allow ~= 0)
end

function AllowClientTicklerFriendRequestEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local allow = KEP_EventDecodeNumber( event )
    g_allowClientTicklerFriendRequest = (allow ~= 0)
end

function AllowClientTicklerPlayerRavedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local allow = KEP_EventDecodeNumber( event )
    g_allowClientTicklerPlayerRaved = (allow ~= 0)
end

function AllowClientTicklerNewMessageEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local allow = KEP_EventDecodeNumber( event )
    g_allowClientTicklerNewMessage = (allow ~= 0)
end

function AllowClientTicklerInWorldInviteEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local allow = KEP_EventDecodeNumber( event )
    g_allowClientTicklerInWorldInvite = (allow ~= 0)
end

function AllowClientTicklerBalanceChangeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local allow = KEP_EventDecodeNumber( event )
    g_allowClientTicklerBalanceChange = (allow ~= 0)
end

function AllowClientTicklerEventReminderEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    local allow = KEP_EventDecodeNumber( event )
    g_allowClientTicklerEventReminder = (allow ~= 0)
end

function AllowClientTicklerInWorldGiftInviteEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    local allow = KEP_EventDecodeNumber( event )
    g_allowClientTicklerInWorldGiftInvite = (allow ~= 0)
end

function AllowClientTicklerWebWorldInviteEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local allow = KEP_EventDecodeNumber( event )
    g_allowClientTicklerWebWorldInvite = (allow ~= 0)
end


function clientTicklerHandler( dispatcher, fromNetid, event, eventid, filter, objectid )
	
    -- Allow ClientTickler Event Handling?
    if (not g_allowClientTickler) then
        Log("clientTicklerHandler: ClientTickler events not allowed")
        return
    end
    
    -- Handle Client Tickler Event
	local eventFilter = KEP_EventDecodeNumber( event )
	if ((eventFilter == ClientTickler.FriendRequest) and g_allowClientTicklerFriendRequest) then
		local senderUsername = KEP_EventDecodeString( event )
		local senderUserId = KEP_EventDecodeNumber( event )
		local senderPicture =  KEP_EventDecodeString( event )
		Log("clientTicklerHandler: FriendRequest userId="..senderUserId)
		showFriendRequest(senderUsername, senderUserId, senderPicture)
		return
	elseif ((eventFilter == ClientTickler.PlayerRaved) and g_allowClientTicklerPlayerRaved) then
		raveType = KEP_EventDecodeString( event )
		raverUserName = KEP_EventDecodeString( event )
		raverUserId = KEP_EventDecodeNumber( event )
		raverUserPicture = KEP_EventDecodeString( event )
		ravedUserName = KEP_EventDecodeString( event )
		ravedUserId = KEP_EventDecodeNumber( event )
		ravedUserPicture = KEP_EventDecodeString( event )
		web_address = GameGlobals.WEB_SITE_PREFIX..RAVE_SUFFIX..raverUserId.."&raveType=single&currency=FREE"
	    local message = "You have been raved by "..raverUserName
		local rollOverText = "Raved by " .. raverUserName
		local rollOverSubtext = "(Click to rave back!)"
		if raveType == "mega" then
			rollOverText = "MegaRaved by " .. raverUserName
			message = "You have been MegaRaved by "..raverUserName..". \nCheck your Gifts in your Inventory to view your MegaRave!"
			SendChatMessage( "Congratulations! " .. ravedUserName .. " was just MegaRaved by ".. raverUserName .. ". ", ChatType.Shout )
		end
		Log("clientTicklerHandler: PlayerRaved userId="..raverUserId)
		sendNotificationEvent(message, rollOverText, rollOverSubtext, NotificationTextureTypes.Rave, raverUserPicture, raverUserName .. "-profile", web_address, "Rave Back!", 15000, NotificationTypes.RavePlayer, tostring(raverUserId), raverUserName, "", "", "")
	elseif ((eventFilter == ClientTickler.NewMessage) and g_allowClientTicklerNewMessage) then
		local senderUsername = KEP_EventDecodeString( event )
		local senderUserId = KEP_EventDecodeNumber( event )
		local senderPicture =  KEP_EventDecodeString( event )
		local message = "You have a new message from " .. senderUsername
		local rollOverText = "New Message from " .. senderUsername
		local rollOverSubtext = "(Click to view your messages)"
		local web_address = GameGlobals.WEB_SITE_PREFIX_KANEVA.."mykaneva/mailbox.aspx"
		Log("clientTicklerHandler: NewMessage userId="..senderUserId)
		sendNotificationEvent(message, rollOverText, rollOverSubtext, NotificationTextureTypes.Message, senderPicture, senderUsername .. "-profile", web_address, "View Messages", 15000, NotificationTypes.ViewInbox, tostring(senderUserId), senderUsername, "", "", "")
	elseif ((eventFilter == ClientTickler.InWorldInvite) and g_allowClientTicklerInWorldInvite) then
		local senderUsername = KEP_EventDecodeString( event )
		local senderUserId = KEP_EventDecodeNumber( event )
		local senderPicture =  KEP_EventDecodeString( event )
		local message = KEP_EventDecodeString( event )
		local kanevaURL = KEP_EventDecodeString( event )
		local trackingRequestId = KEP_EventDecodeString( event )
		local rollOverText = "You have an invite from " .. senderUsername
		local rollOverSubtext = "(Click to join him/her)"
		Log("clientTicklerHandler: InWorldInvite userId="..senderUserId.." url="..kanevaURL.." trackingRequestId="..trackingRequestId)
		sendNotificationEvent(message, rollOverText, rollOverSubtext, NotificationTextureTypes.Message, senderPicture, senderUsername .. "-profile", "", "Accept Invite", 20000, NotificationTypes.GoToURL, tostring(senderUserId), senderUsername, kanevaURL, trackingRequestId, message)
	elseif ((eventFilter == ClientTickler.BalanceChange) and g_allowClientTicklerBalanceChange) then
		local username = KEP_EventDecodeString( event )
		local userid = KEP_EventDecodeNumber( event )
		local credits = KEP_EventDecodeNumber( event )
		local rewards = KEP_EventDecodeNumber( event )
		local keiPointId = KEP_EventDecodeString( event )
		local amount = KEP_EventDecodeNumber( event )
		local transType = KEP_EventDecodeNumber( event )
		local balance_event = KEP_EventCreate("BalanceChangedEvent")
		KEP_EventEncodeNumber( balance_event, credits )
		KEP_EventEncodeNumber( balance_event, rewards )

		-- If we're awarding Rewards for event attendance, show the sliding menu
		if transType == TransactionTypes.CASH_TT_EVENT and string.upper(keiPointId) == "GPOINT" then
			showEventRewardsEarned(amount)
		end

		Log("clientTicklerHandler: BalanceChange userId="..userid)
		KEP_EventQueue(balance_event)	
	elseif ((eventFilter == ClientTickler.EventReminder) and g_allowClientTicklerEventReminder) then
		local senderUsername = KEP_EventDecodeString( event )
		local senderUserId = KEP_EventDecodeNumber( event )
		local senderPicture =  KEP_EventDecodeString( event )
		local message = (KEP_EventDecodeString( event ) or IVTM.EVENT_REMINDER_DEFAULT_MESSAGE)
		local eventName = KEP_EventDecodeString( event )
		local kanevaURL = KEP_EventDecodeString( event )
		local trackingRequestId = KEP_EventDecodeString( event )
		local rollOverText = "Your event is starting now!"
		local rollOverSubtext = "(Click to attend)"
		Log("clientTicklerHandler: EventReminder userId="..senderUserId.." url="..kanevaURL.." trackingRequestId="..trackingRequestId)
		sendNotificationEvent(message, rollOverText, rollOverSubtext, NotificationTextureTypes.Message, senderPicture, senderUsername .. "-profile", "", "Accept Invite", 20000, NotificationTypes.EventReminder, eventName, senderUsername, kanevaURL, trackingRequestId, message)
	elseif ((eventFilter == ClientTickler.InWorldGiftInvite) and g_allowClientTicklerInWorldGiftInvite) then
		local senderUsername = KEP_EventDecodeString( event )
		local senderUserId = KEP_EventDecodeNumber( event )
		local senderPicture =  KEP_EventDecodeString( event )
		local message = KEP_EventDecodeString( event )
		local kanevaURL = KEP_EventDecodeString( event )
		local trackingRequestId = KEP_EventDecodeString( event )
		local rollOverText = "You have a gift from " .. senderUsername
		local rollOverSubtext = "(Click to accept and join him/her)"
		Log("clientTicklerHandler: InWorldGiftInvite userId="..senderUserId.." url="..kanevaURL.." trackingRequestId="..trackingRequestId)
		sendNotificationEvent(message, rollOverText, rollOverSubtext, NotificationTextureTypes.Message, senderPicture, senderUsername .. "-profile", "", "Accept Invite", 20000, NotificationTypes.GiftInvite, tostring(senderUserId), senderUsername, kanevaURL, trackingRequestId, message)
	elseif eventFilter == ClientTickler.ReceivedCreditGift then
		local senderUsername = KEP_EventDecodeString( event )
		local senderUserId = KEP_EventDecodeNumber( event )
		local senderPicture =  KEP_EventDecodeString( event )
		local message = KEP_EventDecodeString( event )
		Log("clientTicklerHandler: ReceivedCreditGift userId="..senderUserId)
		sendNotificationEvent(message, "", "", NotificationTextureTypes.Local_RewardsEarned, senderPicture, senderUsername .. "-profile", "", "", 20000, NotificationTypes.ReceivedCreditGift, tostring(senderUserId), senderUsername, "", "", message)
	elseif eventFilter == ClientTickler.EventCreated then
		-- Handler code here for EventCreated event
        local eventId = KEP_EventDecodeNumber( event )
		local title = KEP_EventDecodeString( event )
        local details = KEP_EventDecodeString( event )
        local startTime = KEP_EventDecodeString( event )
        local endTime = KEP_EventDecodeString( event )
        local communityId = KEP_EventDecodeNumber( event )
        local objPlacementId = KEP_EventDecodeNumber( event )
        local statusId = 0
        local isNewEvent = "empty"
       	if KEP_EventMoreToDecode (event) ~= 0 then
       		statusId = KEP_EventDecodeNumber (event)
       		isNewEvent = KEP_EventDecodeString (event)
       	end
        Events.sendEvent("FRAMEWORK_EVENT_UPDATED", {eventID = eventId, title = title, description = details, startTime = startTime, endTime = endTime, zoneID = communityId, PID = objPlacementId, statusId = statusId, newEvent = isNewEvent})
	elseif ((eventFilter == ClientTickler.WebWorldInvite) and g_allowClientTicklerWebWorldInvite) then
		local senderUsername = KEP_EventDecodeString( event )
		local senderUserId = KEP_EventDecodeNumber( event )
		local senderPicture =  KEP_EventDecodeString( event )
		local message = "You have a new world invite from " .. senderUsername
		local rollOverText = ""
		local rollOverSubtext = ""
		local web_address = GameGlobals.WEB_SITE_PREFIX_KANEVA.."mykaneva/messagecenter.aspx?mc=ci"
		Log("clientTicklerHandler: WebWorldInvite userId="..senderUserId)
		sendNotificationEvent(message, rollOverText, rollOverSubtext, NotificationTextureTypes.Message, senderPicture, senderUsername .. "-profile", web_address, "View Invites", 15000, NotificationTypes.ViewWorldInvite, tostring(senderUserId), senderUsername, "", "", "")
	end
end

function showFriendRequest(username, userId, picture)
	local rollOverText = "You have a friend request from " .. username
	local rollOverSubtext = "(Click to accept the friend request)"
	sendNotificationEvent(rollOverText, rollOverText, rollOverSubtext, NotificationTextureTypes.Message, picture, username .. "-profile", "", "Accept Request", 20000, NotificationTypes.FriendRequest, tostring(userId), username, "", "", "")
end

function showEventRewardsEarned( rewards )
	if MenuIsClosed("EventRewardsEarned.xml")  and tonumber(rewards) > 0 then
		MenuOpen("EventRewardsEarned.xml")
		local ev = KEP_EventCreate( "EventRewardsEarnedEvent" )
		KEP_EventEncodeNumber(ev, rewards)
		KEP_EventQueue(ev)
	end
end

function updateHudEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	--start the timer
	g_TimerEvent = KEP_EventCreate(  "UpdateHUDEvent" )
	KEP_EventQueueInFuture( g_TimerEvent, UPDATE_HUD_INTERVAL )
end

function playerAnimationHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local animIndex = KEP_EventDecodeNumber( event )
	local version = KEP_EventDecodeNumber( event )
	KEP_SetCurrentAnimation(animIndex, version)
end

--function latencyUpdateEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
--	--Handler so that log won't get spammed, won't reach here if Hud consumes event
--	return KEP.EPR_OK
--end

function keyChangedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	isKeyDown = KEP_EventDecodeNumber(event)
	if isKeyDown == 1 then
		if filter == Keyboard.F12 and ToBool(KEP_IsKeyDown(Keyboard.CTRL)) then
			-- Toggle enable status
			local enable = 1 - KEP_IsDownloadPriorityVisualizationEnabled()
			KEP_EnableDownloadPriorityVisualization(enable)						-- Glue argument type `b' takes number parameter from Lua.

			-- Display a confirmation message in dev console or chat menu
			local textEvent = KEP_EventCreate( "RenderTextEvent" )
			if textEvent ~= nil then 
				if enable ~= 0 then
					KEP_EventEncodeString( textEvent, "Download Visualization: ENABLED" )
				else
					KEP_EventEncodeString( textEvent, "Download Visualization: DISABLED" )
				end
				KEP_EventEncodeNumber( textEvent, ChatType.System )
				KEP_EventQueue( textEvent )
			end
		end
	end
	return KEP.EPR_OK
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if g_oneTime == false then
		g_oneTime = true
		onLogin()
	end

	if filter == WF.DEFAULT_HANDLER then
		g_returnData = KEP_EventDecodeString( event )
		if g_parseType == PARSE_FRIEND_USERID then
			ParseFriendUserID()
			KEP_YesNoBox( "Are you sure you want to send a friend request to "..g_friendUsername.."?", "Confirm", WF.CONFIRM_FRIEND_REQUEST, 0 )
		elseif g_parseType == PARSE_FRIEND_REQUEST then
			ParseFriendRequestResult()
		elseif g_parseType == PARSE_RAVE_COUNTS then
	   		s, e, g_alreadyRaved = string.find(g_returnData, "<AlreadyRaved>(%d+)</AlreadyRaved>")
	    	g_alreadyRaved = tonumber(g_alreadyRaved)
	    	if g_friendUserId > 0 then
		    	if g_alreadyRaved == 0 then
					makeWebCall(GameGlobals.WEB_SITE_PREFIX..RAVE_SUFFIX..g_friendUserId.."&raveType=single&currency=FREE", WF.DEFAULT_HANDLER)
					g_parseType = PARSE_RAVE_RESULTS
				else
					MenuOpen("Checkout.xml")
					local ev = KEP_EventCreate( "PurchaseRaveEvent" )
					KEP_EventEncodeNumber(ev, g_friendUserId)
					KEP_EventEncodeNumber(ev, 0) --ravePlus or megarave?
					KEP_EventEncodeNumber(ev, 0) --ravePerson is 0, ravePlace is 1
					KEP_EventEncodeString(ev, g_friendUsername)
					KEP_EventEncodeNumber(ev, 1) --not rave back is 0, rave back is 1
					KEP_EventEncodeString(ev, g_sourceMenu)
					KEP_EventQueue( ev)
				end
			else
				KEP_MessageBox("The rave request could not be completed.")
			end
		elseif g_parseType == PARSE_RAVE_RESULTS then
			ParseRaveResults()
		end
	elseif filter ==  WF.TRACKING_REQUEST_GAME_ACCEPT then

		local trackingData = KEP_EventDecodeString( event )

		s, strPos, result = string.find(trackingData, "<Result><Result>(.-)</Result></Result>")

		if s~= nil and result ~= "0" then
			LogError( "Unable to obtain tracking request for invite accept. Result:"..result)
			return
		end
		return KEP.EPR_CONSUMED
	end
end

function appAssetNameConflictEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_appAssetFilePath = KEP_EventDecodeString(event)
	
	if KEP_EventMoreToDecode( event ) ~= 0 then
		g_appAssetObjectId = KEP_EventDecodeNumber(event)
	end
	dh = MenuOpenModal("YesNoBox.xml")
	local yesnoEvent = KEP_EventCreate( "YesNoBoxEvent" )
	if yesnoEvent==nil then
		return false
	end

	KEP_EventSetFilter( yesnoEvent, WF.APP_ASSET_NAMECONFLICT )
	KEP_EventSetObjectId( yesnoEvent, objectid )
	KEP_EventEncodeString( yesnoEvent, "The file you are attempting to upload already exists in your world. Do you want to overwrite that file? Clicking No will upload a copy of the file with _copy appended to the name." )
	KEP_EventEncodeString( yesnoEvent, "World Asset Name Conflict" )
	KEP_EventQueue( yesnoEvent )
end

function ParseRaveResults()
	local result = nil  -- result description
	local code = nil 	-- return code
	local s, e = 0      -- start and end of captured string
	s, e, code = string.find(g_returnData, "<ReturnCode>(.-)</ReturnCode>")
	s, e, result = string.find(g_returnData, "<ResultDescription>(.-)</ResultDescription>")
	if result ~= nil then
		if result == "already raved" then
			KEP_MessageBox(RM.ALREADY_RAVED.. "this person.")
	    end
	    if result == "success" then
	        KEP_MessageBox(RM.RAVE.."this person.")
		end
	end
end

function ParseFriendUserID()
	local result = nil  	-- result description
	local s, e = 0      	-- start and end of captured string
	local sh = nil			-- static handle
	local dbPath = ""			-- path of picture on website, returned from database	
	s, e, result = string.find(g_returnData, "<ReturnCode>(%d+)</ReturnCode>")
	if result == "0" then
	
		-- Parse needed info from XML
		s, e, result = string.find(g_returnData, "<user_id>(%d+)</user_id>", e)
		g_friendUserId = tonumber(result)
		s, e, result = string.find(g_returnData, "<player_id>(%d+)</player_id>", e)
		s, e, dbPath = string.find(g_returnData, "<thumbnail_medium_path>(.-)</thumbnail_medium_path>", e)
		if not dbPath then
			dbPath = DEFAULT_AVATAR_PIC_PATH
		end
	end
end

function ParseFriendRequestResult()
	local result = nil  -- result description
	local code = nil 	-- return code
	local s, e = 0      -- start and end of captured string
	s, e, code = string.find(g_returnData, "<ReturnCode>(.-)</ReturnCode>")
	s, e, result = string.find(g_returnData, "<ResultDescription>(.-)</ResultDescription>")
	if result ~= nil then
		if code == "1" then
			if result == "already friends" then
				KEP_MessageBox(RM.ALREADY_FRIENDS..g_friendUsername..".")
			elseif result == "already requested to be friends" then
				KEP_MessageBox(RM.FRIEND_REQUEST_SENT..g_friendUsername..".")
			end
		elseif code == "0" then
			KEP_MessageBox("Friend Request sent successfully!")
		else
			KEP_MessageBox("Friend request was unsuccessful.")
		end
	end
end

function SendFriendRequest(userId)
	 if userId ~= 0 and userId ~= nil then
		g_parseType = PARSE_FRIEND_REQUEST
		makeWebCall(GameGlobals.WEB_SITE_PREFIX..UPDATE_SUFFIX..userId..ACTION_SUFFIX.."Add", WF.DEFAULT_HANDLER)
	end
end

function onLogin()
	applySavedVolume()
end

function applySavedVolume()
	local volPercent = g_volPercentDefault --default volume on startup
	local isMuted = false
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		local okvolume, volume = KEP_ConfigGetNumber( config, "GlobalVolume",  volPercent)
		if okvolume == 1 then
			if (volume < 0.0) then volume = 0.0 end
			if (volume > 100.0) then volume = 100.0 end
			volPercent = volume
		else
			volPercent = g_volPercentDefault
		end

		local okmute, mute = KEP_ConfigGetString( config, "VolumeMuted", "false" )
		if mute == "true" then
		    isMuted = true
		else
		    isMuted = false
		end
	end
	
	if isMuted then
		KEP_SetMute(1)
	else
		KEP_SetMute(0)
	end
	KEP_SetMasterVolume(volPercent);
end

function volumeChangedEventHandler()
	local config = KEP_ConfigOpenWOK()
	if config ~= nil then
		KEP_ConfigSetNumber( config, "GlobalVolume", KEP_GetMasterVolume() )
		KEP_ConfigSetString( config, "VolumeMuted", tostring(KEP_GetMute()~=0) )
		KEP_ConfigSave( config )
	end
end

gLoadingSpinnerMenu = nil

function dynamicObjectSpawnMonitorStartedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if gLoadingSpinnerMenu==nil then
		-- DRF - New Unified Progress Menu
        --ProgressOpen("Rezone") -- DRF - Open Rezone Just Incase We Got Here Not Through gotoURL()
        gLoadingSpinnerMenu = MenuOpen("LoadingSpinner.xml") -- Rezone - Stage 4 of 4
	end
end

function dynamicObjectSpawnMonitorCompletedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if gLoadingSpinnerMenu~=nil then
		DestroyMenu( gLoadingSpinnerMenu )
		gLoadingSpinnerMenu = nil
	end

    -- DRF - New Unified Progress Menu
    ProgressClose("Rezone") -- DRF - Close Rezone On Complete
end

function chatMinimizeEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	num = KEP_EventDecodeNumber( event )
	if( num == 1 ) then
	    -- disable chat menu, ignore for now
	elseif num == 2 then
	    -- minimize chat menu, ignore for now
	elseif num == 3 then
	    -- restore chat menu
	else
	    -- enable and focus chat menu, ignore for now
	end
	
	return KEP.EPR_OK
end

function trayPublicChatEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

function trayPrivateMessageEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

function temporarySoundMuteEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
    local muteSound = (KEP_EventDecodeNumber( event ) == 1)

	if muteSound then
		KEP_SetMasterVolume(0.0)
		g_tempMuteApplied = true
	else
		applySavedVolume()
		g_tempMuteApplied = false
	end
end

------------------------- Framework events -------------------------------

-- Called when a new zone is loaded
function newZoneLoadedHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- Reset framework state booleans
	g_playerModeActive = false
	g_frameworkEnabled = nil
	g_defaultSpawnPointFetched = false
	-- Override temporary mute
	if g_tempMuteApplied then
		g_tempMuteApplied = false
		applySavedVolume()
	end
end

-- Called when a player changes Framework mode or when the Framework changes states (enabled/disabled)
function returnFrameworkStateHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local eventTable = cjson.decode(Event_DecodeString(event))
	
	-- Enabled flag set
	if eventTable.enabled ~= nil then
		g_frameworkEnabled = eventTable.enabled == true
	end
	
	-- Player mode set
	if eventTable.playerMode ~= nil then
		g_playerModeActive = eventTable.playerMode == "Player"
	end
	
	-- The first time the Framework state is returned, if we haven't fetched the spawn point for this world, do so now
	if not g_defaultSpawnPointFetched then
		-- Snag the default spawn point for this zone now
		local zoneInstanceId = InstanceId_GetId(KEP_GetCurrentZoneIndex())
		UGCZLP_RequestSpawnList(zoneInstanceId) -- Returned with UGCZLP_ReplySpawnList()
		g_defaultSpawnPointFetched = true
	end

	-- If this is the first time the user has encountered the framework this session, record funnel progression
	if g_frameworkEnabled and g_firstEnabledFramework == false then
		makeWebCall(WEBCALL_NEW_USER_FUNNEL_PROGRESSION .. "entered_game_world", 0)
		g_firstEnabledFramework = true
	end
end

-- Handles the return for spawn points
function UGCZLP_ReplySpawnList(blnRetval, zoneIndex, spawnPointList)
	if (blnRetval == false) then
		return
	end
	
	if (spawnPointList ~= nil and #spawnPointList >= 1) then
		local spawnPoint = spawnPointList[1]
		
		-- Send the default spawn point down to the Framework
		Framework.sendEvent("FRAMEWORK_RETURN_DEFAULT_SPAWNPOINT", {spawnPoint = spawnPoint})
	else 
		return
	end
end
