--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\MenuScripts\\WebSuffix.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\Scripts\\GameGlobals.lua")

spawnLocals = {
	[1] = "Mall.zone#frontdisplay_mall1",
	[2] = "Kaneva City.zone#frontdisplay_city1",
	[3] = "The Plaza.zone#frontdisplay_plaza1",
	[4] = "Ka-Ching! Lobby.zone#frontdisplay_kaching1",
	[5] = "The Third Dimension.zone#frontdisplay_dance1"
}

numZones = 5
spawnIndex = 1
timeInterval = 30 -- seconds

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "frontDisplayMoveHandler", "FrontDisplayMoveEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
    
	KEP_EventRegister( "FrontDisplayMoveEvent", KEP.MED_PRIO )
end

function frontDisplayMoveHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	data = spawnLocals[spawnIndex]
	spawnIndex = spawnIndex + 1
	if(spawnIndex > numZones) then
		spawnIndex = 1
	end
	playerSpawnSubZone( data )
	local ev = KEP_EventCreate( "FrontDisplayMoveEvent")
	KEP_EventQueueInFuture( ev, timeInterval)
end

function startFrontDisplay()
	local ev = KEP_EventCreate( "FrontDisplayMoveEvent")
	KEP_EventQueue( ev)
end
