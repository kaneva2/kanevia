--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua") 
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\BuildMode.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\WebSuffix.lua")
dofile("..\\ClientScripts\\PlayerInformation.lua")
dofile("..\\ClientScripts\\MathHelper.lua")
dofile("..\\ClientScripts\\ObjectGroups.lua")
dofile("..\\Scripts\\NudgeIds.lua")
dofile("..\\Scripts\\FameIds.lua")
dofile("..\\Scripts\\UnlockableIds.lua")
dofile("..\\Scripts\\GetAttributeType.lua")
dofile("..\\MenuScripts\\Framework_EventHelper.lua")
dofile("..\\MenuScripts\\Lib_GameDefinitions.lua")
dofile("..\\MenuScripts\\Framework_FlagHelper.lua")

-- Setting Limits
local MIN_SETTING = 0.01
local DEC_SETTING = 2 -- 2 decimals precision

g_selectingSet = false
g_selectingSetCount = 0

g_playFlashGamesOnRightClick = true -- drf - allow playing flash games on right click
g_playFlashGameStarting = false

local RDOE_FILTER_SCRIPTSERVER = 1

local QUICK_MOVE_MODE = 99
local MAX_DISTANCE = 50

----------BUILDER FAME-------------------------------
g_BuildModeActiveTimerEvent = nil
g_timeStampOfLastAction = nil
g_retryOnce = false

----------BUILD MODE ENABLED-----------------------------------------
g_build_enabled = 0
g_build_mode_locked = 0
g_req_level = 3

---------------MODES------------------------------------------------
g_currentMode = MODE_MOVE
g_userName = nil

------------BUILD SETTINGS-------------------------------------
g_cameraCollision = 1
g_selectionGlow = 1
local SHOW_WIDGET = 1
local ADVANCED_MOVEMENT = 1
local DROP_DISTANCE = 0
g_showWidget = SHOW_WIDGET
g_advancedMovement = ADVANCED_MOVEMENT
g_dropDistance = DROP_DISTANCE
g_widgetUsesGridAxis = true -- drf - added - widget aligns to grid axis vs rotated object axis

-------------MOVEMENT HELPER VALUES-----------------------------------

-- Snap Grid Settings
local DEFAULT_SNAP_GRID = 0.25
local DEFAULT_SNAP_GRID_CTRL = 0.1
local DEFAULT_SNAP_GRID_SHIFT = 1.0

-- Snap Rotation Settings
local DEFAULT_SNAP_ROTATION = 5.0
local DEFAULT_SNAP_ROTATION_CTRL = 1.0
local DEFAULT_SNAP_ROTATION_SHIFT = 90.0

-- Snap Object Settings
local DEFAULT_SNAP_OBJECT = 0.5

-- Builder Mode Snap Settings (BuildCfg.xml)
g_snap = {grid = true, rotation = true, object = true}
g_snapSpacing = {grid = DEFAULT_SNAP_GRID, rotation = DEFAULT_SNAP_ROTATION, object = DEFAULT_SNAP_OBJECT}
g_snapSpacingCtrl = {grid = DEFAULT_SNAP_GRID_CTRL, rotation = DEFAULT_SNAP_ROTATION_CTRL}
g_snapSpacingShift = {grid = DEFAULT_SNAP_GRID_SHIFT, rotation = DEFAULT_SNAP_ROTATION_SHIFT}

-- Player Mode Snap Settings (BuildCfg.xml)
g_playerSnap = {grid = false, rotation = true, object = true}
g_playerSnapSpacing = {grid = DEFAULT_SNAP_GRID, rotation = DEFAULT_SNAP_ROTATION, object = DEFAULT_SNAP_OBJECT}
g_playerSnapSpacingCtrl = {grid = DEFAULT_SNAP_GRID_CTRL, rotation = DEFAULT_SNAP_ROTATION_CTRL}
g_playerSnapSpacingShift = {grid = DEFAULT_SNAP_GRID_SHIFT, rotation = DEFAULT_SNAP_ROTATION_SHIFT}

-- Modifier Key States
g_altDown = false -- allows key movements
g_ctrlDown = false -- forces ctrl settings
g_shiftDown = false -- forces shift settings
g_toggleSnap = false -- toggles snapping

-------------AXIS ALIGNMENT----------------------------------------
g_axis = 0

local BUILD_MENU_ID         = 10670
local ADVANCED_MENU_ID      = 10685
local OBJECTS_LIST_MENU_ID  = 10686
local OBJECT_GROUPS_MENU_ID = 10699

----------COPY/PASTE------------------------------------------------
g_copiedAssetId = 0
g_copiedAssetType = GameGlobals.CUSTOM_TEXTURE
g_copiedTexUrl = ""
local COPY_OBJECTS_FILTER = 1
local PASTE_OBJECTS_FILTER = 2

----UNDO FILTERS----------------------------------------------------
local UNDO_ONCE = 1
local UNDO_ALL = 2
local REDO_ONCE = 3
g_lastUndoID = 0

----CLICK IDS----------------------------------------------------
local LEFT_CLICK  = 0
local RIGHT_CLICK = 1
g_lastPlacementId = 0

----BUILD ACTION TYPES-------------------------------------------
local ACTION_NONE = 0
local ACTION_TRANSLATION = 1
local ACTION_ROTATION = 2
local ACTION_PATTERN = 3
local ACTION_PLACE_OBJECT = 4
local ACTION_REMOVE_OBJECT = 5
g_actionTypeStr = {
	"NONE",
	"TRANSLATION",
	"ROTATION",
	"PATTERN",
	"PLACE",
	"REMOVE"
}
function ActionTypeToString(a) return g_actionTypeStr[a+1] end

----Framework-------------------------------------------
local LAST_GAME_ITEM_PLAYER_WARNING1 = "Can't Pick It Up"
local LAST_GAME_ITEM_PLAYER_WARNING2 = "You can't pick up the last game object in this world."
g_playerModeActive = false
g_frameworkEnabled = false
g_lastGameItem = nil
g_playerPerm = nil


local m_placedObjects = {} -- table of placed objects and their owner
local m_eventFlags = {}

local s_isInHandMode = true
local s_flagBuildOnly = false

local m_bBuildDisabled = false

local FlagSystem			= _G.FlagSystem			-- Assign the global FlagSystem to a local variable for faster access

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	
	KEP_EventRegister("PlayerTargetedMenuEvent", KEP.HIGH_PRIO)
	KEP_EventRegister("SendScreenCoordsEvent", KEP.HIGH_PRIO)
	KEP_EventRegister( "FLAG_REGION_DISPLAY_UPDATE", KEP.HIGH_PRIO ) -- Event to display/hide flag region volumes

	ObjectGroups_InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	PlayerInformation_InitializeKEPEventHandlers(dispatcher, handler, debugLevel) --for PlayerInformation.lua

	KEP_EventRegisterHandler( "ClickEventHandler", "ClickEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "selectEventHandler", "SelectEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "translateEventHandler", "WidgetTranslationEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "rotateEventHandler", "WidgetRotationEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "undoRedoBuildEventHandler", "UndoBuildEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "saveBuildEventHandler", "SaveBuildEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerV( "addDynamicObjectEventHandler", "AddDynamicObjectEvent", KEP.LOW_PRIO )
	KEP_EventRegisterHandlerV( "removeDynamicObjectEventHandler", "RemoveDynamicObjectEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "enterBuildModeEventHandler", "EnterBuildModeEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "changeBuildModeEventHandler", CHANGE_BUILD_MODE_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "setObjectsSelectedPosRotEventHandler", "SetObjectsSelectedPosRotEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "applyTextureEventHandler", "ApplyTextureEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "keyChangedEventHandler", "KeyChangedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "pickUpObjectsHandler", "PickUpSelectedObjectsEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "enableGridEventHandler", "EnableGridEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "playerBuildSettingsEventHandler", "PlayerBuildSettingsEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "copyPasteTextureHandler", "CopyPasteTextureEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "copyPasteObjectsHandler", "CopyPasteObjectsEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "buildKeyPressedHandler", "BuildKeyPressedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "widgetAlignHandler", "WidgetAlignEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "buildOpenMenuEventHandler", "BuildOpenMenuEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "buildSettingsEventHandler", BUILD_SETTINGS_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "requestUnlockableEventHandler", "RequestUnlockableEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "buildModeActiveTimerEventHandler", "BuildModeActiveTimerEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	-- Handle ScriptClientEvent events. NOTE: Only needed for Framework events. Would be nice to get other client menus 
	-- integrated with LibClient_Common. Consequences currently unknown
	KEP_EventRegisterHandler( "ScriptClientEventHandler", "ScriptClientEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "returnFrameworkStateHandler", "FrameworkState", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BuildExitEventHandler", "BuildExitEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "onEventFlagsReturnedFull", "UPDATE_EVENT_FLAGS_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onEventFlagReturned", "UPDATE_EVENT_FLAGS_CLIENT", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onHandModeChanged", "FRAMEWORK_BUILD_HAND_MODE", KEP.HIGH_PRIO)
	
	--Regiester for flag updates
	KEP_EventRegisterHandler( "onFlagsReturnedFull", "UPDATE_FLAGS_CLIENT_FULL", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "onFlagReturned", "UPDATE_FLAG_CLIENT", KEP.MED_PRIO)

	-- DRF - Added
	KEP_EventRegisterHandler( "newZoneLoadedHandler", "ZoneLoadedEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "validateURLEventHandler", "ValidateURLEvent", KEP.MED_PRIO )
	
	-- Placeable Item Updates Handler
	Events.registerHandler("FRAMEWORK_REGISTER_BATTLE_OBJECT_LIST", registerBattleObjectListHandler)
	Events.registerHandler("FRAMEWORK_REGISTER_BATTLE_OBJECT", registerBattleObjectHandler)
	Events.registerHandler("FRAMEWORK_UNREGISTER_BATTLE_OBJECT", unregisterBattleObjectHandler)
	Events.registerHandler("FRAMEWORK_RETURN_SETTINGS", onWorldSettingsChanged)

end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	ObjectGroups_InitializeKEPEvents(dispatcher, handler, debugLevel)
	
	KEP_EventRegister( DYNAMIC_OBJ_MENU_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "UndoBuildEvent", KEP.MED_PRIO )
	KEP_EventRegister( "EnterBuildModeEvent", KEP.MED_PRIO )
	KEP_EventRegister( "SaveBuildEvent", KEP.MED_PRIO )
	KEP_EventRegisterSecureV( "AddDynamicObjectEvent", KEP.HIGH_PRIO, KEP.ESEC_SERVER, KEP.ESEC_CLIENT )
	KEP_EventRegisterSecureV( "RemoveDynamicObjectEvent", KEP.HIGH_PRIO, KEP.ESEC_SERVER + KEP.ESEC_CLIENT, KEP.ESEC_SERVER + KEP.ESEC_CLIENT )
	KEP_EventRegister( CHANGE_BUILD_MODE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "SetObjectsSelectedPosRotEvent", KEP.MED_PRIO )
	KEP_EventRegister( "TextureSelectionEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ApplyTextureEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PickUpSelectedObjectsEvent", KEP.MED_PRIO )
	KEP_EventRegister( "EnableGridEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PlayerBuildSettingsEvent", KEP.MED_PRIO )
	KEP_EventRegister( "CopyPasteTextureEvent", KEP.MED_PRIO )
	KEP_EventRegister( "CopyPasteObjectsEvent", KEP.MED_PRIO )
	KEP_EventRegister( "BuildKeyPressedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "WidgetAlignEvent", KEP.MED_PRIO )
	KEP_EventRegister( WORLD_OBJ_MENU_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( DYNAMIC_OBJECT_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "BuildOpenMenuEvent", KEP.MED_PRIO )
	KEP_EventRegister( BUILD_SETTINGS_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( EQUIPPABLE_ITEM_TRANSLATE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "CloseBuildHudEvent", KEP.MED_PRIO )
	KEP_EventRegister( "DisplayRightClickEvent", KEP.MED_PRIO )
	KEP_EventRegister( "RequestUnlockableEvent", KEP.MED_PRIO )
	KEP_EventRegister( "BuildModeActiveTimerEvent", KEP.MED_PRIO )
	KEP_EventRegister( "EnableApplyEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "DisableApplyEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "EnableResetEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "DisableResetEvent", KEP.HIGH_PRIO )
	KEP_EventRegister( "FrameworkState", KEP.HIGH_PRIO )
	KEP_EventRegister( "FrameworkLastGameItemPID", KEP.MED_PRIO )
	KEP_EventRegister( "FrameworkNPCDialog", KEP.MED_PRIO )
	KEP_EventRegister( "UpdatedBuildSettingsEvent", KEP.MED_PRIO )
	KEP_EventRegister( "BuildModeChangedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "HideHUDSettingEvent", KEP.MED_PRIO)
	KEP_EventRegister( "BuildExitEvent", KEP.MED_PRIO)
	KEP_EventRegister( "FRAMEWORK_BUILD_HAND_MODE", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_REQUEST_EVENT_FLAGS_LOCALLY", KEP.HIGH_PRIO)
	KEP_EventRegister( "UPDATE_EVENT_FLAGS_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegister( "UPDATE_EVENT_FLAGS_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegister( "UPDATE_FLAGS_CLIENT_FULL", KEP.HIGH_PRIO)
	KEP_EventRegister( "UPDATE_FLAG_CLIENT", KEP.HIGH_PRIO)
	KEP_EventRegister( "FRAMEWORK_DELETE_FLAG", KEP.HIGH_PRIO)
	-- DRF - Added
	KEP_EventRegister( "StartFlashGameEvent", KEP.MED_PRIO ) 
	KEP_EventRegister( "DynamicObjectChangeEvent", KEP.MED_PRIO )
    
   	g_userName = KEP_GetLoginName()
end

function openTextureBrowser(objects)

	MenuOpen("TextureBrowser2.xml")

	textureSelEvent = KEP_EventCreate( "TextureSelectionEvent" )
	local count = 0
	if objects ~= nil then
		for i,v in ipairs(objects) do
			if v.type == ObjectType.DYNAMIC or v.type == ObjectType.WORLD then
				count = count + 1
			end
		end
	end
	KEP_EventEncodeNumber(textureSelEvent, count)
	for i,v in ipairs(objects) do
		if v.type == ObjectType.DYNAMIC or v.type == ObjectType.WORLD then
			KEP_EventEncodeNumber(textureSelEvent, v.id)
		end
	end
	KEP_EventQueue( textureSelEvent )
end

function updateSoundPlacementEditMenu()
	
	-- Close Menu If Already Open
	MenuClose("SoundPlacementEdit.xml") 
	
	-- Only Open Menu If One Sound Object Is Selected
	local objects = ObjectsSelected()
	if (objects == 1) then
		if (ObjectSelected(1).type == ObjectType.SOUND) then 
			MenuOpen("SoundPlacementEdit.xml") 
		end
	end
end

function openBuildHudMenu()	MenuOpen("HUDBuild.xml") end

function openDynamicObjectListMenu() MenuOpen("DynamicObjectList.xml") end

function openObjectGroupsMenu()	MenuOpen("ObjectGroups.xml") end

function openBuildAdvancedMenu()

	if g_playerModeActive then
		MenuOpen("PlayerBuildSettings.xml")
	else
		MenuOpen("BuildAdvanced.xml")
	end

	-- Update Menus
	updateAllMenus()
end

function openBuildHelpMenu()
	if (g_currentMode == QUICK_MOVE_MODE) then
		MenuOpen("QuickMove.xml")
		g_currentMode = MODE_MOVE
	end
end

function openContextMenu( x, y, kanevaUserId, clickedName )
	-- Re-Open RClickMenu
	MenuClose("RClickMember.xml")
	MenuOpen("RClickMember.xml")
	
	--fire event to pass player id to the menu script.
	Log("openContextMenu: PlayerTargetedMenuEvent -> targetedUserId="..kanevaUserId.." targetedUserName="..clickedName)
	local menuOpenEvent = KEP_EventCreate( "PlayerTargetedMenuEvent" )
	KEP_EventEncodeNumber( menuOpenEvent, kanevaUserId )
	KEP_EventEncodeString( menuOpenEvent, clickedName )
	KEP_EventQueue( menuOpenEvent )
	
	local sendScreenCoordsEvent = KEP_EventCreate( "SendScreenCoordsEvent" )
	KEP_EventEncodeNumber( sendScreenCoordsEvent, x )
	KEP_EventEncodeNumber( sendScreenCoordsEvent, y )
	KEP_EventQueue( sendScreenCoordsEvent )
	
	return dialogHandle
end

function BuildHudMenu_SetEnabled(controlName, enable)
	local dialog = MenuHandle("HUDBuild")
	if (dialog == nil) then 
		return 
	end
	ctrl = Dialog_GetControl(dialog, controlName)
	if ctrl ~= nil then
		Control_SetEnabled(ctrl, enable)
	end
end

function UpDownMenu_SetEnabled(enable)
	local dialog = MenuHandle("UpDownHelper.xml")
	if (dialog == nil) then 
		return 
	end

	local upCtrl = Dialog_GetControl(dialog, "btnUp")
	local downCtrl = Dialog_GetControl(dialog, "btnDown")
	if upCtrl ~= nil and downCtrl ~= nil then
		Control_SetEnabled(upCtrl, enable)
		Control_SetEnabled(downCtrl, enable)
	end
end

function objectSelectedEvent(event) -- 1234
	log("objectSelectedEvent")

    -- Update flag so script will recalc group pivot point	
	g_selectionChanged = true

	-- Parse Event and Add/Remove Object Outline
	local count = KEP_EventDecodeNumber(event)
	for i = 1,count do
		local type     = KEP_EventDecodeNumber(event) or ObjectType.NONE -- Number defined in ObjectType table
		local id	   = KEP_EventDecodeNumber(event) or 0 -- PID of object in question
		local selected = KEP_EventDecodeNumber(event) or 0 -- 1 = selected, 0 = deselected
		setObjectOutline(type, id, selected, 0)
		
		local tempObj = getObject(type, id)
		
		if type == 4 then -- ObjectType.
			local event = KEP_EventCreate( "FLAG_REGION_DISPLAY_UPDATE" )
			KEP_EventSetFilter(event, 4)
			KEP_EventEncodeNumber(event, id) -- PID of object in question
			KEP_EventEncodeNumber(event, selected) -- 1 = selected, 0 = deselected
			KEP_EventQueue(event)
		end

	end

	-- Log Selected Objects
	SelectedObjectsLog()
end

function setObjectOutline(type, id, selected, color)
	if g_selectionGlow == 0 then
		KEP_SetOutlineState(type, id, 0)
	else
		KEP_SetOutlineState(type, id, selected)
	end
	if (type == ObjectType.DYNAMIC) then
		KEP_SetOutlineColor(type, id, 0.05, 0.25, 1.0)
	elseif (type == ObjectType.ENTITY) then
		KEP_SetOutlineColor(type, id, 1.0, 0.25, 0.05)
	elseif (type == ObjectType.WORLD) then
		KEP_SetOutlineColor(type, id, 0.05, 0.25, 1.0)
	elseif (type == ObjectType.SOUND) then
		KEP_SetOutlineColor(type, id, 0.25, 1.0, 0.25)
	end
	KEP_SetOutlineZSorted(type, id, 0)
end

function updateUpDownMenu()
	if g_showWidget == 1 then
		MenuClose("UpDownHelper.xml")
	elseif MenuIsOpen("HUDBuild") then
		MenuOpen("UpDownHelper.xml")
	end
end

-----BUILD ACTION CLASS-------------------------------------------
--This defines an action that will be tracked in a action history and can be undone
------------------------------------------------------------------
BuildAction = {}
BuildAction.__index = BuildAction

g_currentBuildAction = nil 
g_lastUndoAction = nil
g_undoActions = {}
g_redoActions = {}
g_AdvBuild = 0

function BuildAction.create(objects, actionType)
	local buildAction = {}             -- our new object
	setmetatable(buildAction,BuildAction)
	buildAction.objects = objects
	buildAction.actionType = actionType
	buildAction.rotPivot = { x=0, y=0, z=0 }
	return buildAction
end

-- Action Type Functions
function ActionTypeSame(a1, a2) return (a1.actionType == a2.actionType) end
function ActionTypePos(action) return (action.actionType == ACTION_TRANSLATION) end
function ActionTypeRot(action) return (action.actionType == ACTION_ROTATION) end
function ActionTypePosRot(action) return (ActionTypePos(action) or ActionTypeRot(action)) end
function ActionTypePattern(action) return (action.actionType == ACTION_PATTERN) end
function ActionTypePlace(action) return (action.actionType == ACTION_PLACE_OBJECT) end
function ActionTypeRemove(action) return (action.actionType == ACTION_REMOVE_OBJECT) end
function ActionTypePlaceRemove(action) return (ActionTypePlace(action) or ActionTypeRemove(action)) end

-- Action Objects Functions
function ActionObjectsSame(a1, a2)
	local isTheSame = true
	local objects1 = a1.objects
	local objects2 = a2.objects
	if #objects1 ~= #objects2 then
		return false
	else
		for i,v in ipairs(objects1) do
			local object1 = objects1[i]
			local object2 = objects2[i]
			if object1.id ~= object2.id then
				isTheSame = false
			end
		end
	end
	return isTheSame
end


-----BUILD OBJECT CLASS-------------------------------------------
--This is a wrapper for all objects
------------------------------------------------------------------
BuildObject = {}
BuildObject.__index = BuildObject

function BuildObject.create(objectType, objectID)
	local buildObject = {}             -- our new object

	setmetatable(buildObject,BuildObject)
	buildObject.id = objectID
	buildObject.type = objectType
	buildObject.name = ""
	buildObject.glid = 0
	buildObject.assetId = 0
	buildObject.assetIdStart = 0
	buildObject.friendId = 0
	buildObject.friendIdStart = 0
	buildObject.invType = 0
	buildObject.customTexture = ""
	buildObject.texUrl = ""
	buildObject.texUrlStart = ""
	buildObject.pos = { x=0, y=0, z=0 }
	buildObject.posStart = { x=0, y=0, z=0 }
	buildObject.rot = { x=0, y=0, z=0 }
	buildObject.rotStart = { x=0, y=0, z=0 }

	return buildObject
end

function getObject(type, id)

	-- Create New Object
	local object = BuildObject.create(type, id)
	
	-- Get Object Info (NEW API)
	local obj = ObjectGetInfo(id, type)
	if (obj == nil) then return object end
	object.id = obj.id
	object.type = obj.type
	object.glid = obj.glid
	object.name = obj.name
	object.assetId = obj.assetId
	object.assetIdStart = obj.assetId
	object.friendId = obj.friendId
	object.friendIdStart = obj.friendId
	object.invType = obj.invType	
	object.customTexture = obj.customTexture
	object.texUrl = obj.texUrl
	object.texUrlStart = obj.texUrl	
	object.pos = obj.pos
	object.posStart = obj.pos
	object.rot = obj.rot
	object.rotStart = obj.rot

	return object
end

function selectObjects(objects)
	KEP_ClearSelection()
	g_selectingSet = true
	g_selectingSetCount = #objects
	for i,v in ipairs(objects) do
		KEP_SelectObject(v.type, v.id)
	end
end

function getSelectedObjects()
	local selectedObjects = {} --our currently selected objects

	-- Objects Selected ?
	count = ObjectsSelected()
	if (count < 1) then return selectedObjects end

	-- Accumulate All Selected Objects Info Into Table
	for i = 0,count-1 do
		local type, id = KEP_GetSelection(i)
		local object = getObject(type, id)
		table.insert(selectedObjects, object)
	end

	return selectedObjects
end

function AreSelectedObjectsSameType(objectType)
	local count = ObjectsSelected()
	for i = 0,count-1 do
		local type, id = KEP_GetSelection(i)
		if (objectType == (ObjectType.DYNAMIC + ObjectType.SOUND)) then
			if (type ~= ObjectType.DYNAMIC) and (type ~= ObjectType.SOUND) then
				return false
			end
		elseif type ~= objectType then
			return false
		end
	end
	return true
end

function createDynamicObjectPlacementAction(actionType, objID, glid)
	if actionType == ACTION_PLACE_OBJECT or actionType == ACTION_REMOVE_OBJECT then
		local buildObject = getObject(ObjectType.DYNAMIC, objID)
		local buildObjects = { }
		table.insert(buildObjects, buildObject)
		local buildAction = BuildAction.create(buildObjects, actionType)
		return buildAction
	end
	return nil
end

-- Updates all given action objects positions and rotations
function updateActionObjectsPosRot(action)
	if (action == nil) or (action.objects == nil) then return end
	--Log("updateActionObjectsPosRot: objects="..#action.objects)
	for i,v in ipairs(action.objects) do
		v.pos, v.rot = ObjectGetPositionAndRotation(v.id, v.type)
		--Log("updateActionObjectsPosRot: ... "..ObjectToString(v.id, v.type))
	end
end

-- DRF - Saves current build action and starts new 'NONE' action with selected objects.
function resetCurrentBuildAction()
	local buildEnabled = (g_build_enabled == 1)
	if not buildEnabled then return end

	-- Get Selected Objects
	local objects = getSelectedObjects()
	LogInfo("resetCurrentBuildAction: objects="..#objects)		
		
	-- Accumulate Action For Undo/Redo
	local newAction = BuildAction.create(objects, ACTION_NONE)
	updateCurrentAction(newAction)
		
	-- Update Widget & Menus
	updateWidgetAndAllMenus()

	-- Update Apply/Reset Buttons
	local objects = getSelectedObjects()
	if (#objects < 1) then 
		KEP_EventCreateAndQueue( "DisableApplyEvent")			
		KEP_EventCreateAndQueue( "DisableResetEvent")
	else
		KEP_EventCreateAndQueue( "EnableApplyEvent")
		if (#objects == 1) then
			local object = objects[1]
			local texture = object.customTexture
			local s,b,filename = string.find(texture, ".*CustomTexture\\(.*)")  
			if filename~=nil then 
				KEP_EventCreateAndQueue( "EnableResetEvent")
			else
				KEP_EventCreateAndQueue( "DisableResetEvent")
			end 
		end		
	end 	
end

function updateCurrentAction(newAction)
	if (newAction == nil) then return g_currentBuildAction end

	-- Same Type & Objects As Current Action?
	local sameType = false
	local sameObjects = false
	local sameAction = false
	if (g_currentBuildAction ~= nil) then
		sameType = ActionTypeSame(g_currentBuildAction, newAction)
		sameObjects = ActionObjectsSame(g_currentBuildAction, newAction)
		sameAction = sameType and sameObjects
	end

	-- New Action Or Just An Extension Of Current Action ?
	-- If it is not a new action then simply update the current action 
	-- otherwise put current action into undo history before overwriting.
	-- DRF - BUG FIX - Patterning Is Always A New Action!
	local isTypePattern = ActionTypePattern(newAction)
	local sameTypePattern = sameType and isTypePattern 
	local isActionNew = (not sameAction) or isTypePattern
	
	-- If Build Action Is New
	if isActionNew then
		
		-- Save Current Build Action ?
		local saveCurrentAction = ((g_currentBuildAction ~= nil) and (g_currentBuildAction.actionType ~= ACTION_NONE))
		if saveCurrentAction then
				
			-- Prevent Frequent Pos/Rot Saves
			local isTypePosRot = ActionTypePosRot(g_currentBuildAction)
			local doSave = (not sameObjects) or ((not isTypePosRot) and (not sameTypePattern)) 
			if doSave then 
				saveCurrentBuildAction()
			end

			-- DRF - ED-1938 - Undo/Redo Broken By Accessory Import
			-- Valid Undo/Redo Action ?
			if validUndoRedoAction(g_currentBuildAction) then
	
				-- Current Action Becomes Undo Action
				addUndoAction(g_currentBuildAction)

				-- Redo Actions Start Fresh
				clearRedoActions()

				logUndoRedoActions()
			end
		end
		
		-- Set New Current Build Action
		g_currentBuildAction = newAction

		-- Set Start Position & Rotations For All Objects
		if ActionTypePosRot(g_currentBuildAction) then
			for i,v in ipairs(g_currentBuildAction.objects) do
				v.posStart, v.rotStart = ObjectGetPositionAndRotation(v.id, v.type)
			end
		end
	else
		if ActionTypeRot(g_currentBuildAction) then
			g_currentBuildAction.rotPivot = newAction.rotPivot
		end
	end

	updateAllMenus()

	-- store the timestamp of the last build action, to help us track "active" build time.
	g_timeStampOfLastAction = KEP_CurrentTime()
end

function pickUpSelectedObjects()
	local objects = getSelectedObjects()
	for i,v in ipairs(objects) do
		if v.type == ObjectType.DYNAMIC then

			--If this is an event flag with an event
			local isEventFlag = m_eventFlags[tostring(v.id)]
			
			local isClaimFlag = FlagSystem:isPlaceableObjectLandClaimFlag(v.id)
			if isEventFlag then
				MenuOpen("Framework_DeleteFlag.xml")
				local event = KEP_EventCreate("FRAMEWORK_DELETE_FLAG")
				KEP_EventEncodeNumber(event,v.id)
				KEP_EventEncodeString(event, "event")
				KEP_EventQueue(event)
			elseif isClaimFlag and s_flagBuildOnly then
				MenuOpen("Framework_DeleteFlag.xml")
				local event = KEP_EventCreate("FRAMEWORK_DELETE_FLAG")
				KEP_EventEncodeNumber(event,v.id)
				KEP_EventEncodeString(event, "claimOnly")
				KEP_EventQueue(event)
			-- If this Game Item is the last Game Item
			elseif g_lastGameItem and g_lastGameItem[tostring(v.id)] == v.id then
				-- Owners are prompted
				 if g_playerPerm ~= 3 then
					MenuOpen("Framework_LastGameItemWarning.xml")
					
					local event = KEP_EventCreate("FrameworkLastGameItemPID")
					KEP_EventEncodeNumber(event, v.id)
					KEP_EventQueue(event)
				-- Players get a warning
				else
					MenuOpen("Framework_NPCDialog.xml")
					
					local event = KEP_EventCreate("FrameworkNPCDialog")
					KEP_EventEncodeString(event, LAST_GAME_ITEM_PLAYER_WARNING1)
					KEP_EventEncodeString(event, LAST_GAME_ITEM_PLAYER_WARNING2)
					KEP_EventQueue(event)
				end
			else
				-- Inform the Framework of the removed object
				local _, _, _, _, _, _, _, _, _, _, _, _, _, gameItemId = KEP_DynamicObjectGetInfo(v.id)
				Events.sendEvent("FRAMEWORK_PICKUP_GAME_ITEM", {PID = v.id})
				local isGameItem = (gameItemId ~= nil and gameItemId > 0)
				ObjectDel(v.id, ObjectType.DYNAMIC, isGameItem)
			end
			if KEP_IsWorld() then
				ObjectAdd(v.glid, ObjectType.DYNAMIC, v.invType)
			end
		elseif v.type == ObjectType.SOUND then
            ObjectDel(v.id, v.type)
        end
	end
    KEP_ClearSelection()  
end

function applyTextureToSelectedObjects(pictureType, assetId, texUrl)
	
	-- DRF - BUG FIX - 0 Is Default Texture Not -1
	if (assetId <= 0) then 
		assetId = 0 
		texUrl = ""
	end

	-- Get Selected Objects
	local objects = getSelectedObjects()
	if (#objects < 1) then return end

	Log("applyTextureToSelectedObjects: numObj="..#objects
		.." assetId="..assetId
		.." texUrl='"..texUrl
	)

	-- Accumulate Action For Undo/Redo
	local textureAction = BuildAction.create(objects, ACTION_PATTERN)
	updateCurrentAction(textureAction)

	-- Texture All Selected Objects
	for i,v in ipairs(g_currentBuildAction.objects) do
			
		if v.type == ObjectType.DYNAMIC then
			if pictureType == GameGlobals.FRIEND_PICTURE then							
				v.friendId = assetId					
				KEP_ApplyFriendPicture(v.id, assetId, GameGlobals.FULLSIZE)
			else				
				if (not KEP_IsWorld() ) then
					texUrl = ""
				end
				ObjectSetTexture(v.id, v.type, assetId, texUrl)
			end				
		elseif v.type == ObjectType.WORLD and pictureType == GameGlobals.CUSTOM_TEXTURE then
			ObjectSetTexture(v.id, v.type, assetId)
		end

		-- DRF - Save AssetId & Texture URL For Undo
		v.assetId = assetId
		v.texUrl = texUrl
	end

	-- Log Current Action Objects
	for i,v in ipairs(g_currentBuildAction.objects) do
		Log("applyTextureToSelectedObjects: ... "
			..ObjectToString(v.id, v.type)
			.." assetIdStart="..v.assetIdStart
			.." assetIdNow="..v.assetId
		)
	end

	-- DRF - ED-1383 - Pattern Apply Does Not Update Undo/Redo Stack
	-- NOTE: Pattern apply should immediately 'commit' the pattern change
	-- saving the action to the undo stack and resetting the current build 
	-- action with the selected objects ready for a new action.
	resetCurrentBuildAction()
end

function saveObject(type, objID, texUrl)
	if (type == ObjectType.DYNAMIC) then
		dynamicObjectSavePlacement(objID)
	elseif (type == ObjectType.WORLD) then
		KEP_SaveWldObjectChanges(objID, texUrl or "")
    elseif (type == ObjectType.SOUND) then
        -- Sound Position & Rotation Changes Are Saved Automatically
	end
end

function saveAction(buildAction)
	if (buildAction == nil) then return end

	-- Only Save Actions Translation, Rotation, & Pattern
	local doSave = ActionTypePosRot(buildAction) or ActionTypePattern(buildAction)
	if doSave then

		-- Get All Objects
		local objects = {}
		for i,obj in ipairs(buildAction.objects) do
			objects[obj.id] = obj
		end
	
		-- Save All Objects
		for _,v in pairs(objects) do
			saveObject(v.type, v.id, v.texUrl)
		end
	end
end

function saveCurrentBuildAction()
	if g_currentBuildAction == nil then return end
	Log("saveCurrentBuildAction: OK")
	saveAction(g_currentBuildAction)
end

function saveBuildEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	saveCurrentBuildAction()
end

function isWorldObjectCustomizable(id)
	local buildEnabled = (g_build_enabled == 1)

	-- no world objects are customizable outside build
	if not buildEnabled then
		return false
	end

	-- world objects are customizable if you can hang pictures or change texture
	local assetId, customTexture, canHangPictures, canChangeTexture = KEP_GetWorldObjectInfo(id)
	return (canHangPictures == 1) or (canChangeTexture == 1)
end

function isLeftClickInteractable(objectType, objectID)

	if (objectType == ObjectType.WORLD) then
		return isWorldObjectCustomizable(objectID)
	elseif (objectType == ObjectType.DYNAMIC) then
		-- dynamic objects are interactable inside of build mode
		return (g_build_enabled ~= 0)
	elseif (objectType == ObjectType.ENTITY) then
		return false
	elseif (objectType == ObjectType.SOUND) then
		return true
	else
		return false
	end
end

function BuildExitEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	exitBuild()
end 

function exitBuild()
		-- DRF - Bug Fix 
		-- Must Save Current Build Action Before Closing Build Mode!
		saveCurrentBuildAction()
		KEP_ClearSelection()
		KEP_TurnOffWidget()

		-- Don't close in MODE_NONE, should default to MOVE and only be explicitly called as NONE
		if g_currentMode == MODE_NONE then g_currentMode = MODE_MOVE end

		KEP_SetControlConfig(0) -- Unlock the camera which was locked when pressing ctrl/alt/shift
		KEP_SetCameraCollision(1)
		KEP_SetRolloverMode(ObjectType.DYNAMIC, 0)
		KEP_SetRolloverMode(ObjectType.WORLD, 0)
		KEP_SetRolloverMode(ObjectType.ENTITY, 1)
		KEP_SetRolloverMode(ObjectType.SOUND, 0)
		
		MenuClose("BuildHelp.xml")
		MenuClose("BuildAdvanced.xml")
		MenuClose("DynamicObjectList.xml")
		MenuClose("ObjectGroups.xml")
		MenuClose("PlaceableObj.xml")
		MenuClose("UpDownHelper.xml")
		
		local ev = KEP_EventCreate( "DisplayRightClickEvent")
		KEP_EventQueue( ev)
		
		local idEvent = KEP_EventCreate( DYNAMIC_OBJECT_INFO_EVENT )
		KEP_EventEncodeNumber(idEvent, g_lastPlacementId)
		KEP_EventQueue( idEvent )
		
		local evt = KEP_EventCreate( "CloseBuildHudEvent")
		KEP_EventEncodeNumber(evt, g_build_enabled)
		KEP_EventQueue( evt)

		-- Save Build Settings
		saveSettings()
		
		local ev = KEP_EventCreate("HideHUDSettingEvent")
		KEP_EventEncodeNumber(ev, 0)
		KEP_EventQueue(ev)
end 

-- DRF - ClickEvent: button (0=left 1=right), objType, objId, control, shift, alt, ...
function ClickEventHandler(dispatcher, fromNetId, event, eventId, filter, unused)
	local button = KEP_EventDecodeNumber(event)
	local objType = KEP_EventDecodeNumber(event) or ObjectType.NONE
	local objId = KEP_EventDecodeNumber(event) or 0
	local control = ToBool(KEP_EventDecodeNumber(event) or 0)
	local shift = ToBool(KEP_EventDecodeNumber(event) or 0)
	local alt = ToBool(KEP_EventDecodeNumber(event) or 0)
	
	-- Get Mouse Clicks Since Client OnActivate()
	local clicks = KEP_MouseClicks()

	if (button == nil) then return end
	if (button == LEFT_CLICK) then 
		click = "Left"
	else
		click = "Right"
	end

	g_lastPlacementId = objId	
	
	-- Left click, only for owner's and moderators, and only if player mode is not active
	if (button == LEFT_CLICK) then
		
		-- Are we left-clicking on players?
		if objType == ObjectType.ENTITY then
			-- We can have the left mouse click activate the context menu in one of two situations:
			-- 1) When we're in a non-game world, or
			-- 2) When we are in a game world but also in hand mode.
			local canOpenContextMenu = (not g_frameworkEnabled or s_isInHandMode)
			
			if( canOpenContextMenu ) then
				MenuClose("RClickShop.xml")
			
				AttemptToOpenContextMenuForObject(objId)
			end
		end
		
		-- QUICK REFERENCE:
		-- g_frameworkEnabled --> game mode on
		-- g_build_enabled --> build mode on
		-- g_playerModeActive --> is game mode on but in non-build mode
		
		--log("g_playerModeActive: " .. tostring(g_playerModeActive))
		--log("g_frameworkEnabled: " .. tostring(g_frameworkEnabled))
		--log("g_build_enabled: " .. tostring(g_build_enabled))
	
		if (not g_playerModeActive) and (KEP_IsOwnerOrModerator()) then
			-- DRF - Bug Fix - Ignore First Click Following OnActivate()
			-- Users click inside and outside of client frequently while chatting since KIM is 
			-- a separate window.  This causes world owners and moderators to accidentally select 
			-- objects on their first click return to the client following a chat.  This disturbing 
			-- side-effect of accidental object selection also throws them uncomfortably in build mode
			-- instead of simply re-activating the client window in order to do other things like 
			-- change emotes or move their avatar during a party or event.  Most builders rarely 
			-- mixup building and chat at the same time.
			
			local config = KEP_ConfigOpenWOK()
			if config ~= nil and config ~= 0 then
				local advBuildOK, advBuildON =  KEP_ConfigGetNumber( config, "AdvBuild", g_AdvBuild)
				g_AdvBuild = advBuildON
			end 
			

			if (objType == ObjectType.DYNAMIC) or (objType == ObjectType.SOUND) then
			
				-- Close the right-click menu
				MenuClose("RClickShop.xml")

				local isLeftClickSelectionAllowed = g_frameworkEnabled or g_build_enabled ~= 0
				-- If control key pressed, add to selection (multiselect), otherwise replace whole selection
				if isLeftClickSelectionAllowed then
					if control then 	
						KEP_ToggleSelected(objType, objId)
					else
						KEP_ReplaceSelection(objType, objId)
					end
				else
					ApplyDeselection()
				end

				-- Try Opening Sound Edit Menu (fails if not a single selected sound)
				updateSoundPlacementEditMenu()
			
			else
				local interactable = isLeftClickInteractable(objType, objId)
				if (not interactable) or (g_currentMode ~= MODE_TEXTURES) then
					-- clear selection when they click on nothing or a non-interactable object
					Log("NOT INTERACTABLE OBJECT")
					
					ApplyDeselection()
				else
					-- Left Click + Control ?
					if control then 	
						--multiselect things like tiles/skyboxes
						KEP_ToggleSelected(objType, objId)
					else
						KEP_ReplaceSelection(objType, objId)
						g_currentMode = MODE_TEXTURES 
						local objects = getSelectedObjects()
						openTextureBrowser(objects)
					end

				end
			end
		end
	
		-- Update DynamicObjectList Menu (selection changed)
		KEP_EventCreateAndQueue("DynamicObjectChangeEvent")
	
	elseif (button == RIGHT_CLICK) then
		
		ApplyDeselection()
		
		if shift and (objType == ObjectType.DYNAMIC) then
			OpenShopMenuForObject(objId, objType)
		else
			-- Close the right-click menu
			MenuClose("RClickShop.xml")
			
			if (objType == ObjectType.DYNAMIC) or (objType == ObjectType.SOUND) then
				-- ABC test for "More Difficult Build Mode" - Variant B
				if (not g_playerModeActive) and (KEP_IsOwnerOrModerator()) then
					AttemptToOpenObjectMenuForObject(objId, objType)
				end
			elseif (objType == ObjectType.ENTITY) then 
				local success = AttemptToOpenContextMenuForObject(objId) 
				if not success then
					KEP_SendInteractCommand()
				end
			else
				-- clear selection when they click on nothing or a non-interactable object
				ApplyDeselection()
			end
		end

		-- Allow Playing Flash Games On Right Click ?
		if (g_playFlashGamesOnRightClick and (not shift) and (objType == ObjectType.DYNAMIC)) then

			-- Is Object 'Interactive' (Flash Game) ?
			local obj = ObjectGetInfo(objId, objType)
			if (not ToBool(obj.isInteractive)) then return end

			-- Get Flash Game Parameters (-> BrowserPageReadyHandler -> StartFlashGame)
			g_playFlashGameStarting = true
			makeWebCall(GameGlobals.WEB_SITE_PREFIX.."kgp/dynamicObjectInfo.aspx?type=g&placementId="..objId, WF.FLASH_GAME)
		end
	end
end

function AttemptToOpenContextMenuForObject(objId)
	log("Selection.lua - AttemptToOpenContextMenuForObject")
	local playerMovementObj = KEP_GetPlayerByNetworkDefinedID( objId )
	local controlType = GetSet_GetNumber(playerMovementObj, MovementIds.CONTROLTYPE)
	-- Is player?
	if (controlType == 0) or (controlType == -1) then
		local playerName = GetSet_GetString(playerMovementObj, MovementIds.NAME )
		local loginName = KEP_GetLoginName()
		if loginName ~= playerName then
			Log("ClickEventHandler: RIGHT_CLICK - targetUserId="..objId.." targetUserName="..playerName)
			openContextMenu( 600, 300, objId, playerName)
			KEP_SendMenuEvent( 2, 1000, playerName)
		end
		
		return true
	end
	
	-- Anything else is assumed to be an NPC
	return false
end


function AttemptToOpenObjectMenuForObject(objId, objType)
	-- Works only in non-gaming worlds
	if g_build_enabled == 0 then 
		if g_frameworkEnabled == false then
			MenuOpen("PlaceableObj.xml")
			local e = KEP_EventCreate("DynamicObjectInfoEvent")
			KEP_EventEncodeNumber(e, objId)
			KEP_EventEncodeNumber(e, objType)
			KEP_EventQueue(e)
			g_build_enabled = 0
		end
	end
end

function ApplyDeselection()
	KEP_ClearSelection()
	KEP_TurnOffWidget()
	MenuClose("PlaceableObj.xml")
end

function OpenShopMenuForObject(objId, objType)
	MenuOpen("RClickShop.xml")
	local ev = KEP_EventCreate("DynamicObjectInfoEvent")
	KEP_EventEncodeNumber(ev, objId)
	KEP_EventEncodeNumber(ev, objType)
	KEP_EventQueue(ev)
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	if (filter == WF.FLASH_GAME) and g_playFlashGameStarting then
	
		-- Parse Flash Game Parameters
		local parseTxt = KEP_EventDecodeString( event )
		if (not ParseTagReturnCodeOk(parseTxt)) then return end
		local swfName = ParseTagStr(parseTxt, "swf_name", "", true)
		local swfParam = ParseTagStr(parseTxt, "swf_parameter", "", true)
		if swfName == "" then 
			swfName = ParseTagStr(parseTxt, "media_path", "", true) 
		end

		-- Start Flash Game
		StartFlashGame(swfName, swfParam)
	elseif filter == WF.GET_ZONE_TEMPLATE then
		local returnData = KEP_EventDecodeString(event)
		s, strPos, result = string.find(returnData, "<TemplateId>(.-)</TemplateId>")

		if result and result == "31" then
			m_bBuildDisabled = true
		else
			m_bBuildDisabled = false
		end
	end

	g_playFlashGameStarting = false
end

function StartFlashGame(swfName, swfParam)
	
	-- Valid Flash Parameters ?
	if (swfName == nil) or (swfName == "") then 
		LogWarn("StartFlashGame: Invalid Parameters - swfName='"..swfName.."' swfParam='"..swfParam.."'")
		return false
	end

	-- Open Flash Game And Start
	Log("StartFlashGame: OK - '"..swfName.." "..swfParam.."'")
	MenuOpen("FlashGame.xml")
	local ev = KEP_EventCreate( "StartFlashGameEvent" )
	KEP_EventEncodeString( ev, swfName )
	KEP_EventEncodeString( ev, swfParam )
	KEP_EventQueue( ev )
	return true
end

function selectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	objectSelectedEvent(event)
	
	--selection set logic so that widget recalc won't happen until all object highlighted
	if g_selectingSet then
		g_selectingSetCount = g_selectingSetCount - 1
		if g_selectingSetCount <= 0 then
			g_selectingSetCount = 0
			g_selectingSet = false
		end
	end
	
	-- On Deselect Reset Current Build Action
	if not g_selectingSet then
		resetCurrentBuildAction()
	end
end

function applyTextureEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local pictureType = KEP_EventDecodeNumber(event) or 0
	local assetId = KEP_EventDecodeNumber(event) or 0
	local texUrl = KEP_EventDecodeString(event) or ""
	Log("applyTextureEventHandler: assetId="..assetId.." texUrl='"..texUrl.."'")
	applyTextureToSelectedObjects(pictureType, assetId, texUrl)
end

function addDynamicObjectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- Update DynamicObjectList Menu
	KEP_EventCreateAndQueue("DynamicObjectChangeEvent")

	-- DRF - BUG FIX - Don't Do If Not My Action!
	local playerID = filter
	local isMe = ((g_playerID ~= 0) and (g_playerID == playerID))
	if not isMe then 
--		LogWarn("addDynamicObjectEventHandler: Not Me - Ignoring - playerId="..playerID.." objId="..objectId)
		return KEP.EPR_OK
	end

	-- Decode Event Data
	positionX = KEP_EventDecodeNumber(event)
	positionY = KEP_EventDecodeNumber(event)
	positionZ = KEP_EventDecodeNumber(event)
	rotationX = KEP_EventDecodeNumber(event)
	rotationY = KEP_EventDecodeNumber(event)
	rotationZ = KEP_EventDecodeNumber(event)
	slideX = KEP_EventDecodeNumber(event)
	slideY = KEP_EventDecodeNumber(event)
	slideZ = KEP_EventDecodeNumber(event)
	placementID = KEP_EventDecodeNumber(event)
	animationID = KEP_EventDecodeNumber(event)
	assetID = KEP_EventDecodeNumber(event)
	texUrl = KEP_EventDecodeString(event)
	friendID = KEP_EventDecodeNumber(event)
	playlistSWF = KEP_EventDecodeString(event)
	playlistParams = KEP_EventDecodeString(event)
	globalID = KEP_EventDecodeNumber(event)
	attachable = KEP_EventDecodeNumber(event)
	interactive = KEP_EventDecodeNumber(event)
	canPlayFlash = KEP_EventDecodeNumber(event)
	hasCollision = KEP_EventDecodeNumber(event)
	boundMinX = KEP_EventDecodeNumber(event)
	boundMinY = KEP_EventDecodeNumber(event)
	boundMinZ = KEP_EventDecodeNumber(event)
	boundMaxX = KEP_EventDecodeNumber(event)
	boundMaxY = KEP_EventDecodeNumber(event)
	boundMaxZ = KEP_EventDecodeNumber(event)
	derivable = KEP_EventDecodeNumber(event)
	invType   = KEP_EventDecodeNumber(event)

	--Log("addDynamicObjectEventHandler: "..ObjectToString(placementID))

	local buildEnabled = (g_build_enabled == 1)
	if buildEnabled then
		
		-- Finally Got Undo/Redo Placement Id ?
		-- If so we must finish setting position, rotation, and texture of the object.
		if g_lastUndoID == globalID and g_lastUndoAction ~= nil then
			if ActionTypePlaceRemove(g_lastUndoAction) then
				
				LogWarn("UndoRemove/RedoPlace - glid<"..globalID.."> objId="..placementID)
				
				-- Set Object Position, Rotation, & Texture
				-- NOTE: This should only be 1 object!
				local oldPlacementID = 0
				for i,v in ipairs(g_lastUndoAction.objects) do
					oldPlacementID = v.id
					
					local pos = v.posStart
					local rot = v.rotStart
					local texUrl = v.texUrlStart or ""
					LogWarn(" ... pos="..VecToString(pos).." rot="..VecRotToString(rot).." texUrl="..texUrl)

					-- Set Object Position, Rotation, & Texture
					ObjectSetPosition(placementID, v.type, pos)
					ObjectSetRotation(placementID, v.type, rot)
                    ObjectSetTexture(placementID, v.type, v.assetIdStart, texUrl)
					
					saveObject(v.type, placementID, texUrl)
				end

				--now we go back and associate all old actions with new placementID
				for i,buildAction in ipairs(g_undoActions) do
					for j,buildObject in ipairs(buildAction.objects) do
						if buildObject.id == oldPlacementID then
							buildObject.id = placementID
						end
					end
				end
				
				for i,buildAction in ipairs(g_redoActions) do
					for j,buildObject in ipairs(buildAction.objects) do
						if buildObject.id == oldPlacementID then
							buildObject.id = placementID
						end
					end
				end
			end
			g_lastUndoID = 0
			
			return KEP.EPR_OK

		elseif g_copyingGroup ~= true and g_copyingLastObject ~= true then --apply grid to all objects on drop
			
			-- Set Object Position
			local pos = {
				x = positionX,
				y = positionY,
				z = positionZ
			}
			ObjectSetPosition(placementID, ObjectType.DYNAMIC, pos)
			
			saveObject(ObjectType.DYNAMIC, placementID)

			-- Select objects after placement
			KEP_ReplaceSelection(ObjectType.DYNAMIC, placementID)
		end

		g_copyingLastObject = false
		local buildAction = createDynamicObjectPlacementAction(ACTION_PLACE_OBJECT, placementID, globalID)
		local objects = buildAction.objects
		local object = objects[1]
		updateCurrentAction(buildAction)

	else
		MenuClose("RClickShop.xml")

		-- Send the Placeables menu this object for movement
		local ev = KEP_EventCreate("DynamicObjectInfoEvent")
		KEP_EventEncodeNumber(ev, placementID)
		KEP_EventEncodeNumber(ev, ObjectType.DYNAMIC)
		KEP_EventQueue(ev)
	end

	return KEP.EPR_OK
end

function removeDynamicObjectEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)

	-- Update DynamicObjectList Menu
	KEP_EventCreateAndQueue("DynamicObjectChangeEvent")

	-- DRF - BUG FIX
	-- Previously this called KEP_TurnOffWidget() as other players entered and exited the zone
	-- causing the player building to abort the current build selection and leave build mode.
	-- Now this only occurs if the object being removed from the zone is the same as the object
	-- currently selected in build mode.
	TurnOffWidgetIfSelectedObjectsIncludes(objectId)

	-- DRF - BUG FIX - Don't Do If Not My Action!
	local playerID = filter
	local isMe = ((g_playerID ~= 0) and (g_playerID == playerID))
	if not isMe then 
--		LogWarn("removeDynamicObjectEventHandler: Not Me - Ignoring - playerId=".. playerID.." objId="..objectId)
		return KEP.EPR_OK
	end

	-- DRF - BUG FIX - Don't Do If Not Building!
	local buildEnabled = (g_build_enabled == 1)
	if not buildEnabled then 
		LogWarn("removeDynamicObjectEventHandler: Not Building - Ignoring - objId="..objectId)
		return KEP.EPR_OK
	end
	
	-- DRF - BUG FIX - Don't Do If Object Does Not Exist!
	if not ObjectExists(objectId) then 
		LogWarn("removeDynamicObjectEventHandler: Object Does Not Exist - Ignoring - objId="..objectId)
		return KEP.EPR_OK
	end

	--Log("removeDynamicObjectEventHandler: "..ObjectToString(objectId))

	-- DRF - TODO - Probably Shouldn't Be Done On Switch From Creator To Player Modes In Game Worlds!!!
	-- Add Undo Action(REMOVE_OBJECT)
	local buildAction = createDynamicObjectPlacementAction(ACTION_REMOVE_OBJECT, objectId, 0)
	local buildObject = buildAction.objects[1]
	if g_lastUndoID == buildObject.glid and g_lastUndoID ~= 0 then
		g_lastUndoID = 0
		if filter~=RDOE_FILTER_SCRIPTSERVER then
			KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
		end
		return KEP.EPR_OK
	end
	updateCurrentAction(buildAction)
	if filter~=RDOE_FILTER_SCRIPTSERVER then
		KEP_RequestCharacterInventory(GetAttributeType.GET_INVENTORY_PLAYER)
	end

	return KEP.EPR_OK
end

-- DRF - Only clears the build mode selected objects if none of them are the given objectId.
function TurnOffWidgetIfSelectedObjectsIncludes(objectId)
	if (ObjectsSelected() == 0) then return end
	if (SelectedObjectsIncludes(objectId)) then
		KEP_TurnOffWidget()
	end
end

g_buildLock = false -- lock build mode changes 
function enterBuildModeEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	Log("enterBuildModeEventHandler: filter="..filter.." buildEnabled="..g_build_enabled)
	-- filter -> (0=ExitBuild, 1=EnterBuild, 2=EnterDesign, 3=EnterDevelop, 4=Lock, 5=Unlock)
	
	-- DRF - ED-1647 - During Accessory Import Build Mode Is Not Disabled
	-- Lock Build Mode ?
	if (filter == 5) then
		LogWarn("BUILD_UNLOCK")
		g_buildLock = false
		return
	elseif (filter == 4) then
		LogWarn("BUILD_LOCK")
		g_buildLock = true
		return
	end

	local newMode = nil
	if KEP_EventMoreToDecode(event) ~= 0 then
		tempMode = KEP_EventDecodeNumber(event) 
		if tempMode >= 0 then -- a negative value to make this not change the mode
			newMode = tempMode
		end
	end
	
	openBuildMode(filter, newMode)

	return KEP.EPR_OK
end

function validateURLEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	
	-- Must Save Current Build Action Before Closing Build Mode!
	if (g_build_enabled == 1) then
		LogWarn("validateURLEventHandler: SAVING CURRENT BUILD ACTION")
		saveCurrentBuildAction()
		KEP_ClearSelection()
		KEP_TurnOffWidget()
	end
end

function openBuildMode(open, mode)
	-- DRF - ED-1647 - During Accessory Import Build Mode Is Not Disabled
	-- DRF - Build locked ?
	if (g_buildLock) then
		LogError("BUILD_LOCKED")
		return
	end
		
	if m_bBuildDisabled then return end
	-- Don't close the menu if in a game world and in creator mode
	-- Only do it while the build hud is open, because if it's been closed then we want to clean up (eg leaving zone)
	if open == 0 and g_frameworkEnabled == true and g_playerModeActive == false and MenuIsOpen("HUDBuild") then
		Log("openBuildMode: CLOSE - KEP_ClearSelection()")
		KEP_ClearSelection()
		return
	end

	-- Entering Build Mode?
	if open == 1 then
		Log("openBuildMode: OPEN")

		-- Can User Build Here?
		if KEP_IsOwnerOrModerator() and KEP_IsWorld() then
			--OK to build
		elseif g_build_mode_locked == 1 then
			KEP_MessageBox("You must reach level " .. g_req_level .. " to use Build Mode")
			return KEP.EPR_OK
		end

		--Update the dynamic object menu if already open
		if MenuIsOpen("DynamicObject.xml") or MenuIsOpen("PlaceableObj.xml") and ObjectsSelected() == 1 then
			local objType, id = KEP_GetSelection(0)
			openDynamicObjectMenu(id)
		end
	end
	
	-- Same Mode?
	if g_build_enabled == open then
		LogWarn("openBuildMode: SAME MODE")
		if mode and mode ~= g_currentMode then
			g_currentMode = mode -- don't want to turn off widget but we want event
			changeMode(g_currentMode)
		end
		return
	end
	
	-- Set New Mode
	g_build_enabled = open
	
	if g_build_enabled ~= 1 then --EXITING BUILD MODE		
		
		Log("openBuildMode: CLOSING...")
		exitBuild()

	else --ENTERING BUILD MODE
		
		Log("openBuildMode: OPENING...")

		openBuildHudMenu()
		
		--hide HUD 
		local ev = KEP_EventCreate("HideHUDSettingEvent")
		KEP_EventEncodeNumber(ev, open)
		KEP_EventQueue(ev)

		-- Load Build Settings
		loadSettings()
		
		g_currentMode = mode or g_currentMode
		
		--Hack to enable QuickMove.xml and BuildHelp.xml to exist concurrently for now
		if g_openBuildHelp == true then
			openBuildHelpMenu()
		end

		updateUpDownMenu()

		changeMode(g_currentMode)

		g_copiedAssetId = 0
		KEP_WidgetObjectSpace()

		-- Re lock down the camera if alt, ctrl, or shift is held
		if (g_altDown or g_shiftDown or g_ctrlDown) then KEP_SetControlConfig(2) end

		KEP_SetCameraCollision(g_cameraCollision) -- don't camera collide while in build mode
		KEP_SetRolloverMode(ObjectType.DYNAMIC, 1)
		KEP_SetRolloverMode(ObjectType.WORLD, 1)
		KEP_SetRolloverMode(ObjectType.ENTITY, 1)
		KEP_SetRolloverMode(ObjectType.SOUND, 1)
		KEP_WidgetShowWidget(g_showWidget)

		--TODO: AB Setup - add AB test to disable here when ready; requested placeholder by PM
		-- Ignore user settings in player mode, only basic movements
		if g_playerModeActive then
			KEP_WidgetAdvancedMovement(0)
		else
			KEP_WidgetAdvancedMovement(g_advancedMovement)
		end

		loadSelectionSets()
		
		if COPIED_GROUP ~= nil then
			if #COPIED_GROUP.objects > 0 then
				BuildHudMenu_SetEnabled( "btnPaste", true)
			end
		end
		
		-- Start a timer to count active time in build mode.
		if g_BuildModeActiveTimerEvent == nil then
			g_BuildModeActiveTimerEvent = KEP_EventCreate( "BuildModeActiveTimerEvent")
			KEP_EventQueueInFuture( g_BuildModeActiveTimerEvent, 120)
		end
		requestNudges()
	end

	-- Broadcast Build Mode Changed Event
	local ev = KEP_EventCreate("BuildModeChangedEvent")
	KEP_EventSetFilter(ev, g_build_enabled)
	KEP_EventQueue(ev)
end

function changeMode(mode, sendEvent)
--Not a real mode, only for toggling highlights in build hud so ignore this as a mode change
	if mode == MODE_INVENTORY then 
		return
	end 
	-- Are We Changing Modes ?
	g_changingModes = (g_currentMode ~= mode)
	if (g_changingModes) then
		Log("changeMode: Changing Modes - mode="..mode)
		g_currentMode = mode
	end

	-- ground/sky only accessible in patterns, deselect it if changed
	if g_currentMode ~= MODE_TEXTURES and ObjectsSelected() == 1 then
		local objType, id = KEP_GetSelection(0)
		if objType == ObjectType.WORLD then
			KEP_ClearSelection()			
		end
	end
	-- Mode Changes Require Updated Widget & Menus
	updateWidgetAndAllMenus()

	-- Broadcast Change Build Mode Event ?
	if (sendEvent ~= false) then
		local ev = KEP_EventCreate(CHANGE_BUILD_MODE_EVENT)
		KEP_EventSetFilter(ev, mode)
		KEP_EventQueue(ev)
	end
end

-- Allows Mode Changes From Other Scripts
function changeBuildModeEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local mode = filter
	Log("changeBuildModeEventHandler: mode="..mode)
	changeMode(mode, false)
end

function copyPasteTextureHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	
	-- Get Selected Objects
	local objects = getSelectedObjects()
	if (#objects < 1) then return end

	-- Copy Or Paste ?
	if filter == 1 then --COPY

		-- Cannot Copy Multi-Object Textures
		if (#objects > 1) then
			LogError("copyPasteTextureHandler: Cannot Copy Multi-Select Object Patterns")
			KEP_MessageBox("Cannot Copy Multi-Select Object Patterns")
		else
			local obj = objects[1]			

			-- Friend Picture Or Texture?
			local txtType = ""
			if obj.friendId > 0 then					
				txtType = "FRIEND_PICTURE"
				g_copiedAssetType = GameGlobals.FRIEND_PICTURE
				g_copiedAssetId = obj.friendId
				g_copiedTexUrl = obj.texUrl
			else
				txtType = "TEXTURE"
				g_copiedAssetType = GameGlobals.CUSTOM_TEXTURE
				g_copiedAssetId = obj.assetId
				g_copiedTexUrl = obj.texUrl
			end
			Log("copyPasteTextureHandler: COPY "..txtType.." - "
				..ObjectToString(obj.id, obj.type)
				.." assetId="..g_copiedAssetId
				.." texUrl='"..g_copiedTexUrl.."'"
			)
								
			if MenuIsOpen("TextureBrowser2.xml") then
				local imageHandle = nil 
				local ihBack = nil
				local textureMenu = MenuHandle("TextureBrowser2.xml")
				if (textureMenu ~= nil) then					
					imageHandle = Dialog_GetControl( textureMenu,  "imgSelectedTexture" )
					ihBack = Dialog_GetControl( textureMenu,  "imgSelectedTextureBack" )	
					local displayElement = Image_GetDisplayElement( imageHandle )
					local s,b,filename = string.find(obj.customTexture, ".*CustomTexture\\(.*)")   
					if (filename ~= nil) then
						Control_SetVisible(Dialog_GetControl( textureMenu,  "btnBlank" ),false)
						fileName = "../../CustomTexture/" .. filename
						local temp = gDialogHandle
						gDialogHandle = textureMenu
						scaleImage(imageHandle, ihBack, fileName)
						gDialogHandle = temp
					end
				end
			end
		end
	elseif (filter == 2) and (g_copiedAssetId ~= 0) then --PASTE

		-- Friend Picture Or Texture?
		local txtType = ""
		if (g_copiedAssetType == GameGlobals.FRIEND_PICTURE) then
			txtType = "FRIEND_PICTURE"
		else
			txtType = "TEXTURE"
		end
		Log("copyPasteTextureHandler: PASTE "..txtType.." - "
			.." assetId="..g_copiedAssetId
			.." texUrl='"..g_copiedTexUrl.."'"
		)

		-- Apply Texture To Selected Objects
		applyTextureToSelectedObjects(g_copiedAssetType, g_copiedAssetId, g_copiedTexUrl)		
	end
end

function buildKeyPressedHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local key = filter
	-- Get Selected Objects
	local objects = getSelectedObjects()
	if (#objects < 1) then return end

	-- Alt Must Be Down For Arrow Key Movement
	tempAlt = g_altDown
	if (key == Keyboard.LEFT) or (key == Keyboard.RIGHT) or (key == Keyboard.UP) or (key == Keyboard.DOWN) then
		g_altDown = true
	end

	-- Move Selected Objects According To Key Pressed
	moveObjectsFromKey(objects, key)
	
	-- Restore Alt Key State
	g_altDown = tempAlt
end

function enableGridEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	
	-- Update Snap Settings (sometimes only snap type is being set and not other snap settings)
	g_snap.grid = (KEP_EventDecodeNumber(event) == 1)
	g_snap.rotation = (KEP_EventDecodeNumber(event) == 1)
	g_snap.object = (KEP_EventDecodeNumber(event) == 1)
		
	if KEP_EventMoreToDecode(event) ~= 0 then
		g_snapSpacing.grid = validateSetting(KEP_EventDecodeNumber(event) or 0)
		g_snapSpacing.rotation = validateSetting(KEP_EventDecodeNumber(event) or 0)
		g_snapSpacing.object = validateSetting(KEP_EventDecodeNumber(event) or 0)
	end
	
	-- DRF - Update Platform Snap Settings
	updatePlatformSnapSettings()

	-- Save Build Settings
	saveSettings()

	-- Update Menus
	updateAllMenus()
end

function playerBuildSettingsEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- Update Snap Settings (sometimes only snap type is being set and not other snap settings)
	g_playerSnap.grid = (KEP_EventDecodeNumber(event) == 1)
	g_playerSnap.rotation = (KEP_EventDecodeNumber(event) == 1)
	g_playerSnap.object = (KEP_EventDecodeNumber(event) == 1)
		
	if KEP_EventMoreToDecode(event) ~= 0 then
		g_playerSnapSpacing.grid = validateSetting(KEP_EventDecodeNumber(event) or 0)
		g_playerSnapSpacing.rotation = validateSetting(KEP_EventDecodeNumber(event) or 0)
		g_playerSnapSpacing.object = validateSetting(KEP_EventDecodeNumber(event) or 0)
	end
	
	-- DRF - Update Platform Snap Settings
	updatePlatformSnapSettings()

	-- Save Build Settings
	saveSettings()

	-- Update Menus
	updateAllMenus()
end

function copyPasteObjectsHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if filter == COPY_OBJECTS_FILTER then
		copySelectedObjects()
		BuildHudMenu_SetEnabled( "btnPaste", true)
	elseif filter == PASTE_OBJECTS_FILTER then
		pasteCopiedGroup()
	end
end

function buildSettingsEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	
	-- Update Build Settings
	if filter == BUILD_SETTINGS_CAMERA_COLLISION then
		g_cameraCollision = KEP_EventDecodeNumber(event)
		KEP_SetCameraCollision(g_cameraCollision)
	elseif filter == BUILD_SETTINGS_SELECTION_GLOW then
		g_selectionGlow = KEP_EventDecodeNumber(event)
		local objects = getSelectedObjects()
		selectObjects(objects)
	elseif filter == BUILD_SETTINGS_SELECTION_SHOW_WIDGET then
		g_showWidget = KEP_EventDecodeNumber(event)
		updateUpDownMenu()
		KEP_WidgetShowWidget(g_showWidget)
	elseif filter == BUILD_SETTINGS_SELECTION_ADVANCED_MOVEMENT then
		g_advancedMovement = KEP_EventDecodeNumber(event)
		
		--TODO: AB Setup - add AB test to disable here when ready; requested placeholder by PM
		-- Ignore user settings in player mode, only basic movements
		if g_playerModeActive then
			KEP_WidgetAdvancedMovement(0)
		else
			KEP_WidgetAdvancedMovement(g_advancedMovement)
		end
	elseif filter == BUILD_SETTINGS_DROP_DISTANCE then
		g_dropDistance = KEP_EventDecodeNumber(event)
	end

	-- Save Build Settings
	saveSettings()
end

function widgetAlignHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	g_axis = KEP_EventDecodeNumber(event)
	if g_axis == 0 then
		KEP_WidgetObjectSpace()
	else
		KEP_WidgetWorldSpace()
	end
end

function pickUpObjectsHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	pickUpSelectedObjects()
end

function buildOpenMenuEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if filter == ADVANCED_MENU_ID then
		openBuildAdvancedMenu()
	elseif filter == BUILD_MENU_ID then
		openBuildHudMenu()
	elseif filter == OBJECTS_LIST_MENU_ID then
		openDynamicObjectListMenu()
	elseif filter == OBJECT_GROUPS_MENU_ID then
		openObjectGroupsMenu()
	end
end

function requestUnlockableEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == UnlockableRequestFilter.FeatureResponse then
		local type = KEP_EventDecodeNumber(event)
		local id = KEP_EventDecodeNumber(event)
		local gender = KEP_EventDecodeString(event)
		local locked = KEP_EventDecodeNumber(event)
		local fameType = KEP_EventDecodeNumber(event)
		local levelNumber = KEP_EventDecodeNumber(event)
		if type == UnlockableType.Feature and id == FeatureIds.BuildMode then
			-- g_build_mode_locked = locked
			-- g_req_level = levelNumber
		end
	end
end

--DEFAULT SETTINGS
function resetToDefaultSettings()

	-- General Settings
	g_cameraCollision = 1
	g_selectionGlow = 1
	g_showWidget = SHOW_WIDGET
	g_advancedMovement = ADVANCED_MOVEMENT
	g_dropDistance = DROP_DISTANCE
	g_widgetUsesGridAxis = true

	-- Builder Snap Settings
	g_snap.grid = true
	g_snap.rotation = true
	g_snap.object = true
	g_snapSpacing.grid = DEFAULT_SNAP_GRID
	g_snapSpacing.rotation = DEFAULT_SNAP_ROTATION
	g_snapSpacing.object = DEFAULT_SNAP_OBJECT
	g_snapSpacingCtrl.grid = DEFAULT_SNAP_GRID_CTRL
	g_snapSpacingCtrl.rotation = DEFAULT_SNAP_ROTATION_CTRL
	g_snapSpacingShift.grid = DEFAULT_SNAP_GRID_SHIFT
	g_snapSpacingShift.rotation = DEFAULT_SNAP_ROTATION_SHIFT

	-- Player Snap Settings
	g_playerSnap.grid = false
	g_playerSnap.rotation = true
	g_playerSnap.object = true
	g_playerSnapSpacing.grid = DEFAULT_SNAP_GRID
	g_playerSnapSpacing.rotation = DEFAULT_SNAP_ROTATION
	g_playerSnapSpacing.object = DEFAULT_SNAP_OBJECT
	g_playerSnapSpacingCtrl.grid = DEFAULT_SNAP_GRID_CTRL
	g_playerSnapSpacingCtrl.rotation = DEFAULT_SNAP_ROTATION_CTRL
	g_playerSnapSpacingShift.grid = DEFAULT_SNAP_GRID_SHIFT
	g_playerSnapSpacingShift.rotation = DEFAULT_SNAP_ROTATION_SHIFT

	-------------AXIS ALIGNMENT----------------------------------------
	g_axis = 0

	updatePlatformSnapSettings()
end

-- Enforce decimal precision (2 digits) and positive value greater then MIN_SETTING (0.01)
function validateSetting(a)
	a = tonumber(a) or 0
	if (a < MIN_SETTING) then a = MIN_SETTING end
	a = MathRound(a, DEC_SETTING) -- enforce decimal setting
	return a
end

function saveSettings()
	
	-- Open Config File (BuildConfig.xml)
	local config = KEP_ConfigOpenBuild()
	if config == nil or config == 0 then return end

	-- Save Global Build Settings
	KEP_ConfigSetNumber( config, "CameraCollision", g_cameraCollision )
	KEP_ConfigSetNumber( config, "SelectionGlow", g_selectionGlow )
	KEP_ConfigSetNumber( config, "ShowWidget", g_showWidget)
	KEP_ConfigSetNumber( config, "AdvancedMovement", g_advancedMovement)
	KEP_ConfigSetNumber( config, "AxisAlignment", g_axis )
	KEP_ConfigSetNumber( config, "DropDistance2",g_dropDistance)
	KEP_ConfigSetString( config, "WidgetUsesGridAxis", tostring(g_widgetUsesGridAxis))

	-- Save Builder Build Settings
	KEP_ConfigSetString( config, "SnapGrid", tostring(g_snap.grid) )
	KEP_ConfigSetString( config, "SnapRotation", tostring(g_snap.rotation) )
	KEP_ConfigSetString( config, "SnapObject", tostring(g_snap.object) )
	KEP_ConfigSetNumber( config, "SnapGridSpacing", validateSetting(g_snapSpacing.grid) )
	KEP_ConfigSetNumber( config, "SnapRotationSpacing", validateSetting(g_snapSpacing.rotation) )
	KEP_ConfigSetNumber( config, "SnapObjectSpacing", validateSetting(g_snapSpacing.object) )
	KEP_ConfigSetNumber( config, "SnapGridCtrlSpacing", validateSetting(g_snapSpacingCtrl.grid) )
	KEP_ConfigSetNumber( config, "SnapRotationCtrlSpacing", validateSetting(g_snapSpacingCtrl.rotation) )
	KEP_ConfigSetNumber( config, "SnapGridShiftSpacing", validateSetting(g_snapSpacingShift.grid) )
	KEP_ConfigSetNumber( config, "SnapRotationShiftSpacing", validateSetting(g_snapSpacingShift.rotation) )

	-- Save Player Build Settings
	KEP_ConfigSetString( config, "PlayerSnapGrid", tostring(g_playerSnap.grid) )
	KEP_ConfigSetString( config, "PlayerSnapRotation", tostring(g_playerSnap.rotation) )
	KEP_ConfigSetString( config, "PlayerSnapObject", tostring(g_playerSnap.object) )
	KEP_ConfigSetNumber( config, "PlayerSnapGridSpacing", validateSetting(g_playerSnapSpacing.grid) )
	KEP_ConfigSetNumber( config, "PlayerSnapRotationSpacing", validateSetting(g_playerSnapSpacing.rotation) )
	KEP_ConfigSetNumber( config, "PlayerSnapObjectSpacing", validateSetting(g_playerSnapSpacing.object) )
	KEP_ConfigSetNumber( config, "PlayerSnapGridCtrlSpacing", validateSetting(g_playerSnapSpacingCtrl.grid) )
	KEP_ConfigSetNumber( config, "PlayerSnapRotationCtrlSpacing", validateSetting(g_playerSnapSpacingCtrl.rotation) )
	KEP_ConfigSetNumber( config, "PlayerSnapGridShiftSpacing", validateSetting(g_playerSnapSpacingShift.grid) )
	KEP_ConfigSetNumber( config, "PlayerSnapRotationShiftSpacing", validateSetting(g_playerSnapSpacingShift.rotation) )

	-- Save Config File
	KEP_ConfigSave( config )
end

function loadSettings()

	--reset to Default first in case error occurs and settings are not loaded properly
	resetToDefaultSettings()
	
	local config = KEP_ConfigOpenBuild()
	if config ~= nil and config ~= 0 then
		local ok

		-- Load Global Build Settings
		ok, g_cameraCollision = KEP_ConfigGetNumber( config, "CameraCollision", 1 )
		ok, g_selectionGlow = KEP_ConfigGetNumber( config, "SelectionGlow", 1 )
		ok, g_showWidget = KEP_ConfigGetNumber( config, "ShowWidget", SHOW_WIDGET )
		ok, g_advancedMovement = KEP_ConfigGetNumber( config, "AdvancedMovement", ADVANCED_MOVEMENT )
		ok, g_axis = KEP_ConfigGetNumber( config, "AxisAlignment", 0 )
		ok, g_dropDistance = KEP_ConfigGetNumber( config, "DropDistance2", DROP_DISTANCE )
		ok, val = KEP_ConfigGetString( config, "WidgetUsesGridAxis", "true" )
		g_widgetUsesGridAxis = ToBool(val)

		-- Load Builder Build Settings
		ok, val = KEP_ConfigGetString( config, "SnapGrid", "true" )
		g_snap.grid = ToBool(val)
		ok, val = KEP_ConfigGetString( config, "SnapRotation", "true" )
		g_snap.rotation = ToBool(val)
		ok, val = KEP_ConfigGetString( config, "SnapObject", "true" )
		g_snap.object = ToBool(val)
		ok, val = KEP_ConfigGetNumber( config, "SnapGridSpacing", DEFAULT_SNAP_GRID )
		g_snapSpacing.grid = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "SnapRotationSpacing", DEFAULT_SNAP_ROTATION )
		g_snapSpacing.rotation = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "SnapObjectSpacing", DEFAULT_SNAP_OBJECT )
		g_snapSpacing.object = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "SnapGridCtrlSpacing", DEFAULT_SNAP_GRID_CTRL )
		g_snapSpacingCtrl.grid = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "SnapRotationCtrlSpacing", DEFAULT_SNAP_ROTATION_CTRL )
		g_snapSpacingCtrl.rotation = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "SnapGridShiftSpacing", DEFAULT_SNAP_GRID_SHIFT )
		g_snapSpacingShift.grid = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "SnapRotationShiftSpacing", DEFAULT_SNAP_ROTATION_SHIFT )
		g_snapSpacingShift.rotation = validateSetting(val)

		-- Load Player Build Settings
		ok, val = KEP_ConfigGetString( config, "PlayerSnapGrid", "false" )
		g_playerSnap.grid = ToBool(val)
		ok, val = KEP_ConfigGetString( config, "PlayerSnapRotation", "true" )
		g_playerSnap.rotation = ToBool(val)
		ok, val = KEP_ConfigGetString( config, "PlayerSnapObject", "true" )
		g_playerSnap.object = ToBool(val)
		ok, val = KEP_ConfigGetNumber( config, "PlayerSnapGridSpacing", DEFAULT_SNAP_GRID )
		g_playerSnapSpacing.grid = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "PlayerSnapRotationSpacing", DEFAULT_SNAP_ROTATION )
		g_playerSnapSpacing.rotation = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "PlayerSnapObjectSpacing", DEFAULT_SNAP_OBJECT )
		g_playerSnapSpacing.object = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "PlayerSnapGridCtrlSpacing", DEFAULT_SNAP_GRID_CTRL )
		g_playerSnapSpacingCtrl.grid = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "PlayerSnapRotationCtrlSpacing", DEFAULT_SNAP_ROTATION_CTRL )
		g_playerSnapSpacingCtrl.rotation = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "PlayerSnapGridShiftSpacing", DEFAULT_SNAP_GRID_SHIFT )
		g_playerSnapSpacingShift.grid = validateSetting(val)
		ok, val = KEP_ConfigGetNumber( config, "PlayerSnapRotationShiftSpacing", DEFAULT_SNAP_ROTATION_SHIFT )
		g_playerSnapSpacingShift.rotation = validateSetting(val)
	end

	-- DRF - Update Platform Snap Settings
	updatePlatformSnapSettings()

	-- Update Menus
	updateAllMenus()
end

------------------------- Framework events -------------------------------

-- Handles events from the server NOTE: Currently only needed for Framework events
function ScriptClientEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- ScriptClientEvent
	if filter == 4 then
		-- All ScriptClientEvents with a filter of 4 will have the number of arguments encoded into the
		-- event as its first number
        local args = Event_DecodeNumber(event)
        if args == 2 then
            local eventType = Event_DecodeString(event)
			
			-- One Game Item remains
			if eventType == "FRAMEWORK_ONE_GAME_ITEM_DO_LEFT" then
				local eventTable = cjson.decode(Event_DecodeString(event))
				local lastGameItem = eventTable.lastGameItem
				g_playerPerm = eventTable.playerPerm
				
				if lastGameItem then
					g_lastGameItem = lastGameItem
				else
					g_lastGameItem = nil
				end
			end
		end
	end
end

-- Called when a player changes Framework mode or when the Framework changes states (enabled/disabled)
function returnFrameworkStateHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local eventTable = cjson.decode(Event_DecodeString(event))
	if eventTable.enabled ~= nil then
		g_frameworkEnabled = eventTable.enabled
	end
	
	g_playerModeActive = false
	
	if eventTable.playerMode ~= nil then
		g_playerModeActive = (eventTable.playerMode == "Player")
	
		if g_playerModeActive then
			openBuildMode(0)
			--TODO: AB Setup - add AB test to disable here when ready; requested placeholder by PM
			KEP_WidgetAdvancedMovement(0) -- Use simple movements always when in player mode
		else
			openBuildMode(1, MODE_NONE)
			KEP_WidgetAdvancedMovement(g_advancedMovement) -- Use user settings when not in player mode
		end

		-- DRF - Update Platform Snap Settings On Mode Change
		updatePlatformSnapSettings()
	end
end

-- Called when a new zone is loaded
function newZoneLoadedHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- Reset the placed objects list when we enter a new zone
	m_placedObjects = {}
	
	-- Reset framework variables since we don't receive the events outside of a FW
	g_playerModeActive = false
	g_frameworkEnabled = false
	m_bBuildDisabled = false

	-- Clear Undo/Redo History
	clearUndoRedoActions()

	local zoneInstanceID = KEP_GetZoneInstanceId() 
	local zoneType = KEP_GetZoneIndexType()
	local url = GameGlobals.WEB_SITE_PREFIX .. GET_WORLD_TEMPLATE_ID .. "zoneInstanceId="..tostring(zoneInstanceID) .. "&zoneType="..tostring(zoneType)
	makeWebCall(url, WF.GET_ZONE_TEMPLATE)
end

function onEventFlagsReturnedFull(dispatcher, fromNetId, event, eventId, filter, objectId)
	m_eventFlags = Events.decode(Event_DecodeString(event))
end

function onEventFlagReturned(dispatcher, fromNetId, event, eventId, filter, objectId)
	local PID =  KEP_EventDecodeString(event)
	m_eventFlags[PID] = 1
end

function onFlagsReturnedFull(dispatcher, fromNetid, event, eventid, filter, objectid)

	FlagSystem:create()
	local flags = Events.decode(KEP_EventDecodeString(event))
	FlagSystem:resetFlags(flags)

end

function onFlagReturned(dispatcher, fromNetid, event, eventid, filter, objectid )
	local PID = KEP_EventDecodeNumber(event)
	if KEP_EventMoreToDecode(event) ~= 0 then
		flag = Events.decode(KEP_EventDecodeString(event))
		FlagSystem:addFlag(flag)
		return
	end
	FlagSystem:removeFlag(PID)
end


-- Enter or exit hand mode
function onHandModeChanged(dispatcher, fromNetid, event, eventid, filter, objectid)
	s_isInHandMode = KEP_EventDecodeString(event) == "true"
end


-----------------------------------------------------------------------------
-- Dynamic Object Save Helper
-- PJD - Game Object saving is handled in PlaceableObj.lua
-----------------------------------------------------------------------------
function dynamicObjectSavePlacement(objId)
	if (objId == nil) or (objId == 0) then return end

	-- If this object is not a game item, then save it off as normal
	local obj = ObjectGetInfo(objId, ObjectType.DYNAMIC)
	if (obj.gameItemId == nil) or (obj.gameItemId == 0) then
		KEP_SaveDynamicObjectPlacementChanges(obj.id)
	end
end

-----------------------------------------------------------------------------
-- DRF - Undo/Redo Functions
-----------------------------------------------------------------------------

function clearUndoRedoActions()
	Log("clearUndoRedoActions: OK")
	clearUndoActions()
	clearRedoActions()
end

function clearUndoActions()
	g_currentBuildAction = nil 
	g_lastUndoAction = nil
	g_undoActions = {}
	g_timeStampOfLastAction = KEP_CurrentTime()
	updateUndoRedoButtons()
end

function clearRedoActions()
	g_redoActions = {}
	updateUndoRedoButtons()
end

function logUndoRedoActions()
	logUndoActions()
	logRedoActions()
end

function logUndoActions(logMax)
	Log("logUndoActions: actions="..#g_undoActions)
	logMax = logMax or 5
	for i, v in ipairs(g_undoActions) do
		if (#v.objects >= 1) then
			logMax = logMax - 1
			if (logMax < 0) then 
				Log(" ...")
				return 
			end
			local obj = v.objects[1]
			Log(" ... "..ActionTypeToString(v.actionType).." objects="..#v.objects.." - "
				..ObjectToString(obj)
				.." assetIdStart="..obj.assetIdStart
				.." assetIdEnd="..obj.assetId
			)
		end
	end
end

function logRedoActions(logMax)
	Log("logRedoActions: actions="..#g_redoActions)
	logMax = logMax or 5
	for i, v in ipairs(g_redoActions) do
		if (#v.objects >= 1) then
			logMax = logMax - 1
			if (logMax < 0) then 
				Log(" ...")
				return 
			end
			local obj = v.objects[1]
			Log(" ... "..ActionTypeToString(v.actionType).." objects="..#v.objects.." - "
				..ObjectToString(obj)
				.." assetIdStart="..obj.assetIdStart
				.." assetIdEnd="..obj.assetId
			)
		end
	end
end

function updateUndoRedoButtons()
	local enableRedo = (#g_redoActions > 0)
	local enableUndo = (#g_undoActions > 0)
	BuildHudMenu_SetEnabled( "btnRedo", enableRedo)
	BuildHudMenu_SetEnabled( "btnUndo", enableUndo)
	BuildHudMenu_SetEnabled( "btnResetAll", enableUndo)
end

-- DRF - ED-1938 - Undo/Redo Broken By Accessory Import
-- Undo/Redo Can't Act On Local Objects (glid < 0)
function validUndoRedoAction(action)
	if (action == nil) then return false end
	if (#action.objects < 1) then return false end
	if (action.objects[1].id <= 0) then return false end
	return true
end

function addUndoAction(buildAction)
	if not validUndoRedoAction(buildAction) then return end
	table.insert(g_undoActions, buildAction)
	g_timeStampOfLastAction = KEP_CurrentTime()
	updateUndoRedoButtons()
end

function delUndoAction(index)
	table.remove(g_undoActions, index)
	g_timeStampOfLastAction = KEP_CurrentTime()
	updateUndoRedoButtons()
end

function addRedoAction(buildAction)
	if not validUndoRedoAction(buildAction) then return end
	table.insert(g_redoActions, buildAction)
	updateUndoRedoButtons()
end

function delRedoAction(index)
	table.remove(g_redoActions, index)
	updateUndoRedoButtons()
end

function undoRedoBuildEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	if filter == UNDO_ONCE then
		doUndo()
	elseif filter == UNDO_ALL then
		doUndoAll()
	elseif filter == REDO_ONCE then
		doRedo()
	end
end

function doUndoAll()
	for i = 0, #g_undoActions do
		doUndo()
	end
end

function doUndo()
	Log("doUndo")

	-- Push Current Action To Undo Stack
	if g_currentBuildAction ~= nil then
		if g_currentBuildAction.actionType ~= ACTION_NONE then
			addUndoAction(g_currentBuildAction)
			g_currentBuildAction = nil
		end
	end
	
	local buildAction = nil
	local historyLength = #g_undoActions
	if(historyLength > 0) then
		buildAction = g_undoActions[historyLength]
	else
		LogError("doUndo: Nothing To Undo")
		return
	end

	-- Perform Undo Action
	local objects = buildAction.objects
	if ActionTypePos(buildAction) then
		for i,v in ipairs(objects) do
			ObjectSetPosition(v.id, v.type, v.posStart)
			ObjectSetRotation(v.id, v.type, v.rotStart)
		end
	elseif ActionTypeRot(buildAction) then
		for i,v in ipairs(objects) do
			ObjectSetPosition(v.id, v.type, v.posStart)
			ObjectSetRotation(v.id, v.type, v.rotStart)
		end
	elseif ActionTypePlace(buildAction) then
		for i,v in ipairs(objects) do
			ObjectDel(v.id, v.type)
			g_lastUndoID = v.glid
			-- Pos/Rot/Tex Set In addDynamicObjectEventHandler()
		end
	elseif ActionTypeRemove(buildAction) then
		for i,v in ipairs(objects) do
			ObjectUseInventory(v.glid)
			g_lastUndoID = v.glid
			-- Pos/Rot/Tex Set In addDynamicObjectEventHandler()
		end
	elseif ActionTypePattern(buildAction) then
		for i,v in ipairs(objects) do
			ObjectSetTexture(v.id, v.type, v.assetIdStart or 0, v.texUrlStart or "")
		end
	end

	-- DRF - Bug Fix - Place Undo Clears Selection
	if ActionTypePlace(buildAction) then
		KEP_ClearSelection()
	elseif ActionTypeRemove(buildAction) then
		-- Can't Select Until addDynamicObjectEventHandler()
	else
		selectObjects(buildAction.objects)
	end

	g_lastUndoAction = buildAction
	saveAction(g_lastUndoAction)

	addRedoAction(buildAction)
	delUndoAction(historyLength)

	updateWidgetAndAllMenus()

	logUndoRedoActions()
end

function doRedo()
	Log("doRedo")

	local buildAction = nil
	local historyLength = #g_redoActions
	if(historyLength > 0) then
		buildAction = g_redoActions[historyLength]
	else
		LogError("doRedo: Nothing To Redo")
		return
	end
	
	-- Perform Redo Action
	local objects = buildAction.objects
	if ActionTypePos(buildAction) then
		for i,v in ipairs(objects) do
			ObjectSetPosition(v.id, v.type, v.pos)
			ObjectSetRotation(v.id, v.type, v.rot)
		end
	elseif ActionTypeRot(buildAction) then
		for i,v in ipairs(objects) do
			ObjectSetPosition(v.id, v.type, v.pos)
			ObjectSetRotation(v.id, v.type, v.rot)
		end
	elseif ActionTypePlace(buildAction) then
		for i,v in ipairs(objects) do
			ObjectUseInventory(v.glid)
			g_lastUndoID = v.glid
			-- Pos/Rot/Tex Set In addDynamicObjectEventHandler()
		end
	elseif ActionTypeRemove(buildAction) then
		for i,v in ipairs(objects) do
			ObjectDel(v.id, v.type)
			if KEP_IsWorld() then
				ObjectAdd(v.glid, ObjectType.DYNAMIC, v.invType)
			end
			g_lastUndoID = v.glid
			-- Pos/Rot/Tex Set In addDynamicObjectEventHandler()
		end
	elseif ActionTypePattern(buildAction) then
		for i,v in ipairs(objects) do
			ObjectSetTexture(v.id, v.type, v.assetId, v.texUrl or "")
		end
	end
	
	-- DRF - Bug Fix - Remove Redo Clears Selection
	if ActionTypeRemove(buildAction) then
		KEP_ClearSelection()
	elseif ActionTypePlace(buildAction) then
		-- Can't Select Until addDynamicObjectEventHandler()
	else
		selectObjects(buildAction.objects)
	end
	
	g_lastUndoAction = buildAction
	saveAction(g_lastUndoAction)

	addUndoAction(buildAction)
	delRedoAction(historyLength)

	updateWidgetAndAllMenus()

	logUndoRedoActions()
end

-----------------------------------------------------------------------------
-- DRF - Snap Grid Settings Get/Set
-----------------------------------------------------------------------------

function getGridSnap()
	local snap
	if g_playerModeActive then 
		snap = g_playerSnap.grid 
	else
		snap = g_snap.grid
	end
	return (snap and (getGridSnapSpacingKeyMod() >= MIN_SETTING))
end

function setGridSnap(snap)
	if g_playerModeActive then 
		g_playerSnap.grid = snap
	else
		g_snap.grid = snap
	end
	updatePlatformSnapSettings()
end

function getGridSnapSpacing()
	local spacing
	if g_playerModeActive then 
		spacing = g_playerSnapSpacing.grid 
	else
		spacing = g_snapSpacing.grid
	end
	return spacing
end

function getGridSnapSpacingKeyMod()
	local spacing = getGridSnapSpacing()
	if g_ctrlDown == true then
		spacing = g_snapSpacingCtrl.grid
	elseif g_shiftDown == true then
		spacing = g_snapSpacingShift.grid
	end
	return spacing
end

function setGridSnapSpacing(spacing)
	if (spacing ~= nil) then
		spacing = validateSetting(spacing)
		if g_playerModeActive then 
			g_playerSnapSpacing.grid = spacing
		else
			g_snapSpacing.grid = spacing
		end
	end
	updatePlatformSnapSettings()
end

-----------------------------------------------------------------------------
-- DRF - Snap Rotation Settings Get/Set
-----------------------------------------------------------------------------

function getRotationSnap()
	local snap
	if g_playerModeActive then 
		snap = g_playerSnap.rotation 
	else
		snap = g_snap.rotation
	end
	return (snap and (getRotationSnapSpacingKeyMod() >= MIN_SETTING))
end

function setRotationSnap(snap)
	if g_playerModeActive then 
		g_playerSnap.rotation = snap
	else
		g_snap.rotation = snap
	end
	updatePlatformSnapSettings()
end

-- returns rotation snap spacing in normalized degrees (0-360)
function getRotationSnapSpacing()
	local spacing
	if g_playerModeActive then 
		spacing = g_playerSnapSpacing.rotation 
	else
		spacing = g_snapSpacing.rotation
	end
	return spacing
end

-- returns key modified rotation snap spacing in normalized degrees (0-360)
function getRotationSnapSpacingKeyMod()
	local spacing = getRotationSnapSpacing()
	if g_ctrlDown == true then
		spacing = g_snapSpacingCtrl.rotation
	elseif g_shiftDown == true then
		spacing = g_snapSpacingShift.rotation
	end
	return spacing
end

-- sets rotation snap spacing in normalized degrees (0-360)
function setRotationSnapSpacing(spacing)
	if (spacing ~= nil) then
		spacing = DegNorm(validateSetting(spacing))
		if g_playerModeActive then 
			g_playerSnapSpacing.rotation = spacing
		else
			g_snapSpacing.rotation = spacing
		end
	end
	updatePlatformSnapSettings()
end

-----------------------------------------------------------------------------
-- DRF - Snap Object Settings Get/Set
-----------------------------------------------------------------------------

function getObjectSnap()
	local snap
	if g_playerModeActive then 
		snap = g_playerSnap.object 
	else
		snap = g_snap.object
	end
	return (snap and (getObjectSnapSpacing() >= MIN_SETTING))
end

function setObjectSnap(snap)
	if g_playerModeActive then 
		g_playerSnap.object = snap
	else
		g_snap.object = snap
	end
	updatePlatformSnapSettings()
end

function getObjectSnapSpacing()
	local spacing
	if g_playerModeActive then 
		spacing = g_playerSnapSpacing.object 
	else
		spacing = g_snapSpacing.object
	end
	return spacing
end

function setObjectSnapSpacing(spacing)
	if (spacing ~= nil) then
		spacing = validateSetting(spacing)
		if g_playerModeActive then 
			g_playerSnapSpacing.object = spacing
		else
			g_snapSpacing.object = spacing
		end
	end
	updatePlatformSnapSettings()
end

-----------------------------------------------------------------------------
-- DRF - Updates Platform With All Snap Settings
-----------------------------------------------------------------------------

function updatePlatformSnapSettings()
	KEP_SetGridSpacing(getGridSnapSpacing())
-- TODO - KEP_SetRotationSpacing(getRotationSnapSpacing())
-- TODO - KEP_SetObjectSpacing(getObjectSnapSpacing())
end

-----------------------------------------------------------------------------
-- DRF - Object Translation/Rotation
-----------------------------------------------------------------------------

-- Returns position and rotation of selected objects pivot (widget)
function ObjectsSelectedGetPivot()
	local pos = { x=0, y=0, z=0 }
	local rot = { x=0, y=0, z=0 }

	-- No Objects Selected ?
	local objects = ObjectsSelected()
	if (objects < 1) then return pos, rot end
		
	-- Assume Pivot As Selected Objects Position & Rotation
	pos, rot = ObjectsSelectedGetPositionAndRotation()

	-- Multi-Objects Use Build Action Rotation Pivot (starts at 0)
	if (objects > 1) and (g_currentBuildAction ~= nil) then
		rot = g_currentBuildAction.rotPivot
	end

	return pos, rot
end

-- DRF - Performs Event Commanded Selected Object Position/Rotation.
-- NOTE - Only called for HUDBuild coordinate edit box changes, not mouse drag, keyboard, or Movement Helper clicks
function setObjectsSelectedPosRotEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- Position Or Rotation (degrees) Vector
	local vec = {}
	vec.x = KEP_EventDecodeNumber(event) or 0
	vec.y = KEP_EventDecodeNumber(event) or 0
	vec.z = KEP_EventDecodeNumber(event) or 0

	Log("setObjectsSelectedPosRotEventHandler: vec="..VecToString(vec).." mode="..g_currentMode)

	-- Reset Widget & Update Widget & Menus
    KEP_TurnOffWidget()
	updateWidgetAndAllMenus()

	-- Get Selected Objects
	local objects = getSelectedObjects()
	if (#objects < 1) then return end

	-- Perform Mode Action
	if g_currentMode == MODE_ROTATE then

   		-- Convert Given Vector In Degrees To Rotation In Radians
		local rot = {
			x = Deg2Rad(vec.x),
			y = Deg2Rad(vec.y),
			z = Deg2Rad(vec.z)
		}

		-- Rotate Selected Objects (no snap, logs)
		rotateSelectedObjects(rot, false, true)
	else
		
		-- Convert Given Vector In Absolute Position To Translation Direction
		local posSel = ObjectsSelectedGetPosition()
		local dir = {
			x = vec.x - posSel.x,
			y = vec.y - posSel.y,
			z = vec.z - posSel.z
		}
				
		-- Translate Selected Objects (no snap, logs)
		translateSelectedObjects(dir, 1, false, false, true)
	end
	
	-- Update Widget & Menus
	updateWidgetAndAllMenus()
end

-- DRF - Performs Event Commanded Selected Object Rotation.
-- NOTE - Only called from mouse drags, not keyboard or Movement Helper clicks
function translateEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- Decode Event Data (position)
	local pos = {}
	pos.x = KEP_EventDecodeNumber(event) or 0
	pos.y = KEP_EventDecodeNumber(event) or 0
	pos.z = KEP_EventDecodeNumber(event) or 0
	
	local doSnap = getObjectSnap()
	
	if( KEP_EventMoreToDecode(event) ~= 0 ) then
		doSnap = KEP_EventDecodeString(event) == "true"
	end
	--log("translateEventHandler: pos="..VecToString(pos))

	-- Update Widget Position
	KEP_SetWidgetPosition(pos.x, pos.y, pos.z)
	
	-- Get Selected Objects
	local objects = getSelectedObjects()
	if (#objects < 1) then return end

	-- Convert Given Vector In Absolute Position To Translation Direction
	local posSel = ObjectsSelectedGetPosition()
	local dir = {
		x = pos.x - posSel.x,
		y = pos.y - posSel.y,
		z = pos.z - posSel.z
	}

	-- Translate Selected Objects (snap, no log)
	translateSelectedObjects(dir, 1, true, doSnap, false)
end

-- DRF - Performs Event Commanded Selected Object Rotation.
-- NOTE - Only called from mouse drags, not keyboard or Movement Helper clicks.
function rotateEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)

	-- Decode Event Data (position & rotation)
	local pos = {}
	pos.x = KEP_EventDecodeNumber(event) or 0
	pos.y = KEP_EventDecodeNumber(event) or 0 
	pos.z = KEP_EventDecodeNumber(event) or 0
	local rot = {}
	rot.x = KEP_EventDecodeNumber(event) or 0
	rot.y = KEP_EventDecodeNumber(event) or 0
	rot.z = KEP_EventDecodeNumber(event) or 0
	--Log("rotateEventHandler: pos="..VecToString(pos).." rot="..VecRotToString(rot))

	-- DRF - BUG FIX - Caller Is Off By (g_PI / 2)!
	rot.y = rot.y + (g_PI / 2)

	-- Rotate Selected Objects (snap, no log)
	rotateSelectedObjects(rot, true, false)
end

-- DRF - Performs Key Commanded Selected Object Rotation & Translation.
-- NOTE - Only called from keyboard or Movement Helper rotates.
function keyChangedEventHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local key = filter
	local isKeyDown = (KEP_EventDecodeNumber(event) == 1)

	--Log("keyChangedEventHandler: key="..key.." isKeyDown="..tostring(isKeyDown))

	-- Key Up/Down?
	-- NOTE: Keep track of ctrl/shift/alt regardless of build mode, but only lock camera when in build
	local buildEnabled = (g_build_enabled == 1)
	if isKeyDown then		
		if (key == Keyboard.ESC) then
			KEP_ClearSelection()
		elseif (key == Keyboard.CTRL) then
			g_ctrlDown = true
			if buildEnabled then KEP_SetControlConfig( 2 ) end
		elseif (key == Keyboard.SHIFT) then
			g_shiftDown = true			
			if buildEnabled then KEP_SetControlConfig( 2 ) end
		elseif (key == Keyboard.ALT) then
			g_altDown = true			
			if buildEnabled then KEP_SetControlConfig( 2 ) end
		elseif buildEnabled and (not g_toggleSnap) and (key == Keyboard.INSERT) then -- DRF - Added
			g_toggleSnap = true
			setGridSnap(not getGridSnap())
			setRotationSnap(not getRotationSnap())
			LogWarn("+INSERT - Toggle Snapping ON - snapR="..tostring(getRotationSnap()).." snapG="..tostring(getGridSnap()))
		end

		-- Perform Key Actions Only In Build Mode
		if buildEnabled then
			if (key == Keyboard.Z) and (g_ctrlDown == true) then --UNDO/REDO depending on shift key
				if g_shiftDown then
					doRedo()
				else
					doUndo()
				end
			elseif (key == Keyboard.Y) and (g_ctrlDown == true) then --REDO
				doRedo()
			elseif (key == Keyboard.C) and (g_ctrlDown == true) then --COPY
				copySelectedObjects()
				BuildHudMenu_SetEnabled( "btnPaste", true)
			elseif (key == Keyboard.V) and (g_ctrlDown == true) then --PASTE
				pasteCopiedGroup()
			elseif (key == Keyboard.X) and (g_ctrlDown == true) then --CUT
				copySelectedObjects()
				pickUpSelectedObjects()
				BuildHudMenu_SetEnabled( "btnPaste", true)
			elseif (key == Keyboard.DEL) then
				pickUpSelectedObjects()
			end

			-- Perform Key Action
			local objects = getSelectedObjects()
			moveObjectsFromKey(objects, key)
		end

	else -- Key Up

		if (key == Keyboard.CTRL) then
			g_ctrlDown = false			
			KEP_SetControlConfig( 0 )
		elseif (key == Keyboard.SHIFT) then
			g_shiftDown = false			
			KEP_SetControlConfig( 0 )
		elseif (key == Keyboard.ALT) then
			g_altDown = false			
			KEP_SetControlConfig( 0 )
		elseif g_toggleSnap and (key == Keyboard.INSERT) then -- DRF - Added
			g_toggleSnap = false
			setGridSnap(not getGridSnap())
			setRotationSnap(not getRotationSnap())
			LogWarn("-INSERT - Toggle Snapping OFF - snapR="..tostring(getRotationSnap()).." snapG="..tostring(getGridSnap()))
		end
	end
end

-- DRF - Performs Key Commanded Selected Object Translation & Rotation.
-- NOTE - Only called from keyboard or Movement Helper rotates.
function moveObjectsFromKey(objects, keyDown)
	--Log("key="..keyDown)
		
	--Delete now picks up an object so ignore it
	if keyDown == Keyboard.DEL then return end

	-- Get Translation Distance
	local translateObjects = false
	local translateDist = getGridSnapSpacingKeyMod()
	
	-- Get Rotation Degrees
	local rotateObjects = false
	local rotateRad = Deg2Rad(getRotationSnapSpacingKeyMod())
	
	-- Key Rotations ?
	local keyPageUpDown = false
	if keyDown == Keyboard.PAGE_UP then
		-- Counter-Clockwise
		keyPageUpDown = true
		rotateRad = -rotateRad
		rotateObjects = true
	elseif keyDown == Keyboard.PAGE_DOWN then
		-- Clockwise
		keyPageUpDown = true
		rotateObjects = true
	end
		
	-- Movements Are Relative To Camera View
	camX, camY, camZ = KEP_GetPlayerCameraOrientation()	
	local dotProduct = math.abs(dotProduct(camX, camY, camZ, 0, 1, 0))
	if(dotProduct > 0.95) then
		camX, camY, camZ = KEP_GetPlayerCameraUpVector()
	end
	camY = 0		
	camX, camY, camZ = normalizeVector(camX, camY, camZ)
	camX, camZ = roundVectorToNearestAngle(camX, camZ, math.rad(45)) 
		
	-- Apply Key Modifiers
	local dir = { x=0, y=0 , z=0 }
	if g_altDown or g_ctrlDown or g_shiftDown then
		if keyDown == Keyboard.LEFT then
			dir.x = camZ * -1.0
			dir.y = camY
			dir.z = camX
			translateObjects = true
		elseif keyDown == Keyboard.RIGHT then
			dir.x = camZ
			dir.y = camY
			dir.z = camX  * -1.0
			translateObjects = true
		elseif keyDown == Keyboard.UP then
			dir.x = camX
			dir.y = camY
			dir.z = camZ
			translateObjects = true
		elseif keyDown == Keyboard.DOWN then
			dir.x = camX * -1.0
			dir.y = camY
			dir.z = camZ * -1.0
			translateObjects = true
		end
	end
	
	if g_altDown then
		if keyDown == Keyboard.HOME then
			
			-- Bring Object To Me
			local objPos = ObjectsSelectedGetPosition()
			local player = KEP_GetPlayer()
			if (player == nil) then return end
			local px, py, pz = KEP_GetPlayerPosition(player)
			if (px == nil or py == nil or pz == nil) then return end
			dir.x = px - objPos.x
			dir.y = py - objPos.y
			dir.z = pz - objPos.z
			translateDist = 1
			translateObjects = true
			LogWarn("moveObjectsFromKey: Bring Object To Me - pos="..VecToString(objPos))
		
		elseif keyDown == Keyboard.END then
			
			-- Bring Me To Object 
			-- Land on it by adding a foot and offset slightly to avoid triangle edge collision bug
			local objPos = ObjectsSelectedGetPosition()
			KEP_SetPlayerPosition(objPos.x + .01, objPos.y + 1, objPos.z)
			LogWarn("moveObjectsFromKey: Bring Me To Object - pos="..VecToString(objPos))
			return
		end		
	else
		if keyDown == Keyboard.HOME then
			dir.x = 0
			dir.y = 1
			dir.z = 0
			translateObjects = true
		elseif keyDown == Keyboard.END then
			dir.x = 0
			dir.y = -1
			dir.z = 0
			translateObjects = true
		end	
	end

	-- Perform Object Rotations
	if rotateObjects and AreSelectedObjectsSameType(ObjectType.DYNAMIC + ObjectType.SOUND) then

		-- Assure We Are In Rotate Mode
		changeMode(MODE_ROTATE)

		-- Rotation Is Relative To Selected Objects Pivot Rotation
		local pos, rot = ObjectsSelectedGetPivot()
		rot.y = rot.y + rotateRad

		-- Rotate Selected Objects (snap, log)
		rotateSelectedObjects(rot, true, true)
	end
	

	-- Perform Object Translations
	if translateObjects and AreSelectedObjectsSameType(ObjectType.DYNAMIC + ObjectType.SOUND) then
		-- Assure We Are In Move Mode
		changeMode(MODE_MOVE)

		-- Translate Selected Objects (snap, no obj snap, log)
		--local doSnapObj = false -- sheeno says no -- getObjectSnap()
		--translateSelectedObjects(dir, translateDist, true, doSnapObj, true)
		
		requestTranslation(dir, translateDist, false)
	end
end


-- CJW, NOTE: Instead of directly calling translateSelectedObjects on an object being moved by the player
-- call requestTranslation, which will let Framework_PlayerBuild keep track of the legality of the motion. 
-- If the movement is not deemed permissable then Framework_PlayerBuild will restore the previous position.
function requestTranslation(dir, distance, doSnap)
	-- 1) Convert from displacement to absolute position
	-- 2) Construct WidgetTranslationEvent with the absolute position
	-- 3) Send event

	
	local currentPos = ObjectsSelectedGetPosition()
	local desiredPos = {
		x = dir.x * distance + currentPos.x,
		y = dir.y * distance + currentPos.y,
		z = dir.z * distance + currentPos.z
	}
	
	-- This will emulate a widget translation action which is monitored by Framework_PlayerBuild.lua
	local event = KEP_EventCreate("WidgetTranslationEvent")
	KEP_EventEncodeNumber(event, desiredPos.x)
	KEP_EventEncodeNumber(event, desiredPos.y)
	KEP_EventEncodeNumber(event, desiredPos.z)
	
	if( doSnap ~= nil ) then
		KEP_EventEncodeString(event, tostring(doSnap))
	end
	
	KEP_EventQueue(event)
end



-- DRF - Returns if all selected objects are snappable (DYNOBJ + SOUND)
function SelectedObjectsAreSnappable()
	objects = getSelectedObjects()
	for i,v in ipairs(objects) do
		if ((v.type ~= ObjectType.DYNAMIC) and (v.type ~= ObjectType.SOUND)) then
			return false
		end
	end
	return true
end

-- DRF - Actually Translates Selected Objects Given Direction Vector & Distance
-- NOTE - Called on key and movement helper clicks and on mouse drag.
function translateSelectedObjects(dir, dist, doSnap, doSnapObj, doLog)
	--Normalize Direction Vector
	dir = MathVecNorm(dir)
	dist = dist * dir.mag
	if (dist < .0001) then 
		if doLog then
			LogWarn("translateSelectedObjects: INSIGNIFICANT TRANSLATION - dist="..dist)
		end
		return 
	end

	-- Get Selected Objects
	objects = getSelectedObjects()
	if (#objects < 1) then return end
	local multiObject = (#objects > 1)

	-- Selected Objects Snappable?
	local snappable = SelectedObjectsAreSnappable()
	doSnap = doSnap and snappable
	doSnapObj = doSnapObj and snappable

	-- Historically we never grid snapped during multi-object translation.
	-- Not really sure what the user expects to happen here but I expect
	-- we might need to add a modifier key or build setting for them.
	local doGridSnapSingle = doSnap and getGridSnap() and not multiObject
	local doGridSnapMulti = doSnap and getGridSnap() and multiObject

	-- Get Selected Objects Position
	local posSel = ObjectsSelectedGetPosition()

	if doLog then	
		Log("translateSelectedObjects: numObj="..#objects
			.." pos="..VecToString(posSel)
			.." dir="..VecToString(dir)
			.." dist="..MathRoundStr(dist, 3)
			.." doSnapSingle="..tostring(doGridSnapSingle)
			.." doSnapMulti="..tostring(doGridSnapMulti)
			.." doSnapObj="..tostring(doSnapObj)
		)
	end

	-- Accumulate Action For Undo/Redo
	local translateAction = BuildAction.create(objects, ACTION_TRANSLATION)
	updateCurrentAction(translateAction)

	-- Calculate Delta Translation
	local delta = { 
		x = dist * dir.x, 
		y = dist * dir.y,
		z = dist * dir.z
	}

	-- Apply Multi-Object Grid Snapping ?
	if doGridSnapMulti then
		local spacing = getGridSnapSpacingKeyMod()
		delta.x, delta.y, delta.z = snap3D(delta.x, delta.y, delta.z, spacing)
	end

	-- Update Current Action Objects Position & Rotations (incase they moved)
	updateActionObjectsPosRot(g_currentBuildAction)

	-- For All Current Action Objects
	for i,v in ipairs(g_currentBuildAction.objects) do
		
		-- Calculate New Object Position
		local posNew = {
			x = v.pos.x + delta.x,
			y = v.pos.y + delta.y,
			z = v.pos.z + delta.z		
		}

		-- Apply Single-Object Grid Snapping ?
		if doGridSnapSingle then
			local spacing = getGridSnapSpacingKeyMod()
			posNew.x, posNew.y, posNew.z = snap3D(posNew.x, posNew.y, posNew.z, spacing)
		end

		-- Set New Object Position
		ObjectSetPosition(v.id, v.type, posNew)
	end

	-- Apply Object Snapping ?
	if doSnapObj then
		KEP_SnapSelection(validateSetting(getObjectSnapSpacing()))
	end

	-- Update Current Action Objects Positions & Rotations For Undo
	updateActionObjectsPosRot(g_currentBuildAction)
	
	-- Log Current Action Objects
	if doLog then
		for i,v in ipairs(g_currentBuildAction.objects) do
			Log("translateSelectedObjects: ... "..ObjectToString(v.id, v.type))
		end
	end

	-- Update Widget & All Menus
	updateWidgetAndAllMenus()
end

-- DRF - Actually Rotates Selected Objects Given Absolute Angle In Radians
-- NOTE - Called on key and movement helper clicks and on mouse drag.
function rotateSelectedObjects(rot, doSnap, doLog)

	-- Get Selected Objects
	objects = getSelectedObjects()
	if (#objects < 1) then return end
	local multiObject = (#objects > 1)

	-- Selected Objects Snappable?
	local snappable = SelectedObjectsAreSnappable()
	doSnap = doSnap and snappable

	-- Historically we grid snapped during multi-object rotation.
	-- Not really sure what the user expects to happen here but I expect
	-- we might need to add a modifier key or build setting for them.
	local doRotationSnapSingle = doSnap and getRotationSnap() and not multiObject
	local doRotationSnapMulti = doSnap and getRotationSnap() and multiObject

	-- Apply Rotation Snapping ?
	if doRotationSnapSingle or doRotationSnapMulti then
		local rotateRad = Deg2Rad(getRotationSnapSpacingKeyMod())
		rot.x, rot.y, rot.z = snap3D(rot.x, rot.y, rot.z, rotateRad)
	end
	
	-- Get Selected Objects Position
	-- Use fixed position on mode or selection change to prevent
	-- centroid rotation errors from accumulating into object runaway.
	if (g_changingModes or g_selectionChanged) then 
		g_changingModes = false
		g_selectionChanged = false
		g_posSel = ObjectsSelectedGetPosition()
		Log("PIVOT SAVED - posSel="..VecToString(g_posSel))
	end
	local posSel = g_posSel

	-- Log Anticipated Rotation
	if doLog then
		Log("rotateSelectedObjects: numObj="..#objects
			.." pos="..VecToString(posSel)
			.." rot="..VecRotToString(rot)
			.." doSnapSingle="..tostring(doRotationSnapSingle)
			.." doSnapMulti="..tostring(doRotationSnapMulti)
		)
	end

	-- Accumulate Action For Undo/Redo
	local rotateAction = BuildAction.create(objects, ACTION_ROTATION)
	rotateAction.rotPivot = rot
	updateCurrentAction(rotateAction)

	-- Update Current Action Objects Position & Rotations (incase they moved)
	updateActionObjectsPosRot(g_currentBuildAction)

	-- For All Current Action Objects
	for i,v in ipairs(g_currentBuildAction.objects) do

		-- Only Dynamic Object & Sounds
		if (v.type == ObjectType.DYNAMIC) or (v.type == ObjectType.SOUND) then

			-- Set New Object Position & Rotation 
			if multiObject then
			
				-- Set Multi Object New Position As Rotated Around Centroid
				local posNew = {}
				posNew.x, posNew.y, posNew.z = KEP_RotatePointAroundPoint(v.posStart.x, v.posStart.y, v.posStart.z, posSel.x, posSel.y, posSel.z, rot.x, rot.y, rot.z)
				ObjectSetPosition(v.id, v.type, posNew)

				-- Set Multi Object New Rotation Relative To Start Rotation
				local rotNew = {
					x = rot.x + v.rotStart.x,
					y = rot.y + v.rotStart.y,
					z = rot.z + v.rotStart.z
				}
				ObjectSetRotation(v.id, v.type, rotNew)
			else

				-- Set Single Object New Rotation Directly
				ObjectSetRotation(v.id, v.type, rot)
			end			
		end
	end

	-- Update Current Action Objects Positions & Rotations For Undo
	updateActionObjectsPosRot(g_currentBuildAction)

	-- Log Current Action Objects
	if doLog then
		for i,v in ipairs(g_currentBuildAction.objects) do
			Log("rotateSelectedObjects: ... "..ObjectToString(v.id, v.type))
		end
	end

	-- Update Widget & All Menus
	updateWidgetAndAllMenus()
end

-- DRF - Updates Widget For MODE_MOVE
function updateWidgetModeMove()

	-- Nothing Selected ?
	local objects = ObjectsSelected()
	if (objects < 1) then 
		Log("updateWidgetModeMove: NOTHING SELECTED - KEP_TurnOffWidget()")
		KEP_TurnOffWidget()
		return
	end

	-- Update Widget Mode Translate
	if AreSelectedObjectsSameType(ObjectType.DYNAMIC + ObjectType.SOUND) then

		-- Get Selected Objects Pivot Position & Rotation
		local pos, rot = ObjectsSelectedGetPivot()

		-- Widget Uses Grid Axis Instead Of Object Rotation Axis ?
		if g_widgetUsesGridAxis and getGridSnap() then
			rot = { x = 0, y = 0, z = 0 }
		end

		-- Put Widget In Translation Mode
		-- NOTE: Before SetWidgetPos/Rot() As This Resets Widget Position & Rotation!
		KEP_DisplayTranslationWidget(ObjectType.DYNAMIC)

		-- Set Widget Position & Rotation Same As Selected Objects
		KEP_SetWidgetPosition(pos.x, pos.y, pos.z)
		KEP_SetWidgetRotation(rot.x, rot.y, rot.z)

	elseif AreSelectedObjectsSameType(ObjectType.EQUIPPABLE) then
		KEP_DisplayTranslationWidget(ObjectType.EQUIPPABLE)
	end
end

-- DRF - Updates Widget For MODE_ROTATE
function updateWidgetModeRotate()

	-- Nothing Selected ?
	local objects = ObjectsSelected()
	if (objects < 1) then 
		Log("updateWidgetModeRotate: NOTHING SELECTED - KEP_TurnOffWidget()")
		KEP_TurnOffWidget()
		return
	end

	-- Update Widget Mode Rotate
	if AreSelectedObjectsSameType(ObjectType.DYNAMIC + ObjectType.SOUND) then

		-- Get Selected Objects Pivot Position & Rotation
		local pos, rot = ObjectsSelectedGetPivot()

		-- Put Widget In Rotation Mode
		-- NOTE: Before SetWidgetPos/Rot() As This Resets Widget Position & Rotation!
		KEP_DisplayRotationWidget(ObjectType.DYNAMIC)

		-- Set Widget Position & Rotation Same As Selected Objects
		KEP_SetWidgetPosition(pos.x, pos.y, pos.z)
		KEP_SetWidgetRotation(rot.x, rot.y, rot.z)

	elseif AreSelectedObjectsSameType(ObjectType.EQUIPPABLE) then
		KEP_DisplayRotationWidget(ObjectType.EQUIPPABLE)
	end
end

-- DRF - Updates Widget For MODE_TEXTURES
function updateWidgetModeTextures()
	
	-- Texture Browser Instead Of Widget
	KEP_TurnOffWidget()
	local objects = getSelectedObjects()
	openTextureBrowser(objects)
end

-- Updates Movement Widget To Reflect Build Mode & Updates All Menus With Selected Object Position & Rotation
function updateWidgetAndAllMenus()
	
	-- Update Widget For Mode
	if (g_currentMode == MODE_MOVE) then
		updateWidgetModeMove()		
	elseif (g_currentMode == MODE_ROTATE) then
		updateWidgetModeRotate()
	elseif (g_currentMode == MODE_TEXTURES) then
		updateWidgetModeTextures()
	elseif (g_currentMode == MODE_NONE) then
		KEP_TurnOffWidget()
    end
    
	-- Update Menus
	updateAllMenus()
end

-- Updates All Menus With Selected Object Position & Rotation
function updateAllMenus()

	-- Update All Menus With Selected Objects Pivot Position & Rotation
	local pos, rot = ObjectsSelectedGetPivot()
	updateAllMenusPosRot(pos, rot)
end

-- Updates All Menus With Given Position & Rotation & Current Snap Settings
function updateAllMenusPosRot(pos, rot)
--	Log("updateAllMenusPosRot: pos="..VecToString(pos).." rot="..VecToString(rot))
	
	-- Update Build Hud
	local objects = ObjectsSelected()
	if (objects > 0) and AreSelectedObjectsSameType(ObjectType.DYNAMIC + ObjectType.SOUND) then
		BuildHudMenu_SetEnabled( "btnCopy", true)
		BuildHudMenu_SetEnabled( "btnPickUp", true)
		UpDownMenu_SetEnabled(true)
	else
		BuildHudMenu_SetEnabled( "btnCopy", false)
		UpDownMenu_SetEnabled(false)
	end

	-- Update Undo/Redo Buttons
	updateUndoRedoButtons()

	-- Update Build Hud Menu
	local buildMenu = MenuHandle("HUDBuild")
	if buildMenu ~= nil then

		-- Update 'XYZ' Coordinates Edit Boxes
		local objects = getSelectedObjects()
		editPosX = Dialog_GetControl(buildMenu, "edCoordsX")
		editPosY = Dialog_GetControl(buildMenu, "edCoordsY")
		editPosZ = Dialog_GetControl(buildMenu, "edCoordsZ")
		if (#objects > 0) and AreSelectedObjectsSameType(ObjectType.DYNAMIC + ObjectType.SOUND) then
			
			if g_currentMode == MODE_MOVE then
				
				-- Set Position Edit Box Text (enforcing decimal setting)
				EditBox_SetText(editPosX, MathRound(pos.x, DEC_SETTING), false)
				EditBox_SetText(editPosY, MathRound(pos.y, DEC_SETTING), false)
				EditBox_SetText(editPosZ, MathRound(pos.z, DEC_SETTING), false)
			
			elseif g_currentMode == MODE_ROTATE then
			
				-- Set Rotation Edit Box Text (enforcing decimal setting, normalized degrees)
				EditBox_SetText(editPosX, "0", false)
				EditBox_SetText(editPosY, DegNorm(MathRound(Rad2Deg(rot.y), DEC_SETTING)), false)
				EditBox_SetText(editPosZ, "0", false)
			else

				-- No Display While Not In Move/Rotate Mode
				EditBox_SetText(editPosX, "", false)
				EditBox_SetText(editPosY, "", false)
				EditBox_SetText(editPosZ, "", false)
			end
		else	
					
			-- No Selection
			EditBox_SetText(editPosX, "", false)
			EditBox_SetText(editPosY, "", false)
			EditBox_SetText(editPosZ, "", false)		
		end

		-- Update Helpers Sub-Menu Check Boxes
		local chkGrid = Dialog_GetControl(buildMenu, "chkSnapGrid")
		local chkRot = Dialog_GetControl(buildMenu, "chkSnapRot")
		local chkObj = Dialog_GetControl(buildMenu, "chkSnapObject")
		CheckBox_SetChecked(chkGrid, getGridSnap())
		CheckBox_SetChecked(chkRot, getRotationSnap())
		CheckBox_SetChecked(chkObj, getObjectSnap())
	end
	
	-- Update Build Advanced Menu
	local advancedMenu = MenuHandle("BuildAdvanced.xml")
	if advancedMenu ~= nil then

		--UPDATE GRID SETTINGS
		local checkGridCorners = Dialog_GetControl(advancedMenu, "cbObjectSnapping")
		local checkRotSnapping = Dialog_GetControl(advancedMenu, "cbRotSnapping")
		local checkGridOn = Dialog_GetControl(advancedMenu, "cbGridSnapping")

		local editGridSpacing = Dialog_GetControl(advancedMenu, "edGridSpacing")
		local editGridRotation = Dialog_GetControl(advancedMenu, "edGridRotation")
		local editGridSnapDistance = Dialog_GetControl(advancedMenu, "edGridSnapDistance")

		local snapG = g_snap.grid
		CheckBox_SetChecked(checkGridOn, snapG)
		Control_SetVisible(editGridSpacing, snapG)

		local snapR = g_snap.rotation
		CheckBox_SetChecked(checkRotSnapping, snapR)
		Control_SetVisible(editGridRotation, snapR)

		local snapO = g_snap.object
		CheckBox_SetChecked(checkGridCorners, snapO)
		Control_SetVisible(editGridSnapDistance, snapO)
		
		EditBox_SetText(editGridSpacing, validateSetting(g_snapSpacing.grid), false )
		EditBox_SetText(editGridRotation, validateSetting(g_snapSpacing.rotation), false )
		EditBox_SetText(editGridSnapDistance, validateSetting(g_snapSpacing.object), false )
		
		--UPDATE CAMERA COLLISION
		local cbCameraCollision = Dialog_GetControl(advancedMenu, "cbCameraCollision")
		CheckBox_SetChecked(cbCameraCollision, g_cameraCollision)

		-- UPDATE OBJECT GLOW
		local cbSelectionGlow = Dialog_GetControl(advancedMenu, "cbSelectionGlow")
		CheckBox_SetChecked(cbSelectionGlow, g_selectionGlow)

		-- UPDATE SHOW WIDGET
		local cbShowWidget = Dialog_GetControl(advancedMenu, "cbShowWidget")
		CheckBox_SetChecked(cbShowWidget, g_showWidget)

		-- UPDATE ADVANCED MOVEMENT
		local cbAdvancedMovement = Dialog_GetControl(advancedMenu, "cbAdvancedMovement")
		CheckBox_SetChecked(cbAdvancedMovement, g_advancedMovement)

		-- UPDATE DROP DISTANCE		
		local cbPlaceAtAvatar = Dialog_GetControl(advancedMenu, "cbPlaceAtAvatar")
		CheckBox_SetChecked(cbPlaceAtAvatar, g_dropDistance == 0)

-- TODO	-- UPDATE WIDGET AXIS
--		local cbWidgetGridAxis = Dialog_GetControl(advancedMenu, "cbWidgetGridAxis")
--		CheckBox_SetChecked(cbWidgetGridAxis, g_widgetUsesGridAxis)
	end

	-- Update Player Settings menu
	local playerSettings = MenuHandle("PlayerBuildSettings.xml")
	if playerSettings ~= nil then

		--UPDATE SNAP SETTINGS
		local checkGridCorners = Dialog_GetControl(playerSettings, "cbObjectSnapping")
		local checkRotSnapping = Dialog_GetControl(playerSettings, "cbRotSnapping")
		local checkGridOn = Dialog_GetControl(playerSettings, "cbGridSnapping")

		local editGridSpacing = Dialog_GetControl(playerSettings, "edGridSpacing")
		local editGridRotation = Dialog_GetControl(playerSettings, "edRotSpacing")
		local editGridSnapDistance = Dialog_GetControl(playerSettings, "edSnapDistance")

		local snapG = g_playerSnap.grid
		CheckBox_SetChecked(checkGridOn, snapG)
		Control_SetVisible(editGridSpacing, snapG)

		local snapR = g_playerSnap.rotation
		CheckBox_SetChecked(checkRotSnapping, snapR)
		Control_SetVisible(editGridRotation, snapR)

		local snapO = g_playerSnap.object
		CheckBox_SetChecked(checkGridCorners, snapO)
		Control_SetVisible(editGridSnapDistance, snapO)
		
		EditBox_SetText(editGridSpacing, validateSetting(g_playerSnapSpacing.grid), false )
		EditBox_SetText(editGridRotation, validateSetting(g_playerSnapSpacing.rotation), false )
		EditBox_SetText(editGridSnapDistance, validateSetting(g_playerSnapSpacing.object), false )
	end
end


-- Retrieves battle objects from the server
function registerBattleObjectListHandler(event)
	
	if event.objects then
		-- Extract all targetable objects
		for i, object in ipairs(event.objects) do
			local ID 	= tonumber(object.ID)
			m_placedObjects[ID] = {	owner = object.owner  }
		end
	end
end

-- Retrieves new battle object from the server
function registerBattleObjectHandler(event)
	local type  = event.type
	local ID 	= tonumber(event.ID)
	if type == "Object" then
		-- PlayerBuild handles owned item selection for movement and thus only needs alive and owner
		m_placedObjects[ID] = {	owner = event.owner }
	end
end

-- Removes battle objects that are no longer in game.
function unregisterBattleObjectHandler(event)
	local ID 	= tonumber(event.ID)
	if ID then
		m_placedObjects[ID] = nil
	end
end

-- Retrieves new world settings from the server
function onWorldSettingsChanged(event)

	s_flagBuildOnly				= event.flagBuildOnly
end

