--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\MovementCapsIds.lua")
dofile("..\\Scripts\\MovementIds.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\Progressive.lua")
dofile("..\\Scripts\\ChatType.lua")
dofile("..\\MenuScripts\\WebSuffix.lua")
dofile("..\\ClientScripts\\PatchHelper.lua")
dofile("..\\ClientScripts\\FrontDisplay.lua")
dofile("..\\Scripts\\CameraIds.lua")
dofile("..\\Scripts\\FameIds.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\EventProcTimeExperiment.lua")

first_zone_loaded = false
g_loadTopPlaces = false

FORCED_STARTING_APARTMENT = 4649

g_current_zoneinstanceid = -1
parse_results = true

ARENA_LOBBY_ZONE_INDEX = 38
prev_zone_was_arena = false
prev_zone_was_arena_lobby = false

CHAT_MINIMIZE_CODE = 2 --see chatmenu.lua

gNewbieCharCreated = false

-- 0 = No Access Pass
-- 1 = Has Access Pass
g_iHasAccessPass = 0
g_iHasVIPPass = 0

-- 0 = Non Access Pass Zone
-- 1 = Access Pass Zone
g_isAccessPassZone = 0
g_isVIPZone = 0

g_userName = ""

g_currentUrl = ""
g_my3DApp = 0

EVENT_NOTIFICATION_HOURS = 1 --Number of hours the notification display checks for

-- Target zone info for metrics.
g_zoningGuid = nil
g_targetZoneIndex = 0
g_targetZoneInstanceId = 0
g_targetZoneType = 0
g_zoningStartTime = nil

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "logonResponseHandler", "LogonResponseEvent", KEP.HIGH_PRIO+1 )
	KEP_EventRegisterHandler( "newZoneLoadedHandler", "ZoneLoadedEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "pendingSpawnEventHandler", KEP.PENDINGSPAWN_EVENT_NAME, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "PatchProgressEventHandler", "PatchProgressEvent", KEP.LOW_PRIO )
	KEP_EventRegisterHandler( "testPassListHandler", "PassListEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	-- register LOW+1 to be just above C++ last chance handler
	KEP_EventRegisterHandler( "LastChanceRenderTextEventHandler", "RenderTextEvent", KEP.LOW_PRIO+1 )
	KEP_EventRegisterHandler( "ClientSpawnHandler", "ClientSpawnEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "requestAccessPassInfoHandler", "RequestAccessPassInfoEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "requestAccessPassZoneInfoHandler", "RequestAccessPassZoneInfoEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "killBlackScreenHandler", "KillBlackScreenEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "answerEventHandler", "YesNoAnswerEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "appReturnEventHandler", "3DAppReturnEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "zoneLoadingStartHandler", "ZoneLoadingStartEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "zoneLoadingCompleteHandler", "ZoneLoadingCompleteEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "zoneLoadingFailedHandler", "ZoneLoadingFailedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "chatOpenHandler", "RoomChatOpenEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "chatMinimizedHandler", "RoomChatMinimizedEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "chatMissingHandler", "RoomChatMissingEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "chatTimeoutHandler", "ChatStatusTimeoutEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "CloseProgressMenuHandler", "CloseProgressMenu", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "ZoningPingEventHandler", "ZoningPingEvent", KEP.LOW_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "SpawnAfterLoginEvent", KEP.MED_PRIO )
	KEP_EventRegister( "HudMinimizeEvent", KEP.MED_PRIO )
	KEP_EventRegister( "HideLocationMenuEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ChatMinimizeEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PatchProgressEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PassListEvent", KEP.MED_PRIO )
	KEP_EventRegister( KEP.WORLD_FAME_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PlayerLeftEvent", KEP.MED_PRIO )
	KEP_EventRegister( "PlayerTargetingEnabledEvent", KEP.MED_PRIO )
	KEP_EventRegister( "LaunchPersistantMenusEvent", KEP.MED_PRIO )
	KEP_EventRegister( "RequestAccessPassInfoEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ResponseAccessPassInfoEvent", KEP.MED_PRIO )
	KEP_EventRegister( "RequestAccessPassZoneInfoEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ResponseAccessPassZoneInfoEvent", KEP.MED_PRIO )
	KEP_EventRegister( "KillBlackScreenEvent", KEP.MED_PRIO )
	KEP_EventRegister( "3DAppReturnEvent", KEP.MED_PRIO )
	KEP_EventRegister( "RoomChatOpenEvent", KEP.MED_PRIO )
	KEP_EventRegister( "RoomChatMinimizedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "RoomChatMissingEvent", KEP.MED_PRIO )
	KEP_EventRegister( "ChatStatusTimeoutEvent", KEP.MED_PRIO )
	KEP_EventRegister( "BuildSettingsEvent", KEP.MED_PRIO )
	KEP_EventRegister( "CloseProgressMenu", KEP.MED_PRIO )
	KEP_EventRegister( "ZoningPingEvent", KEP.LOW_PRIO )
end

function testPassListHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- filter = 1 if zone PassList, sent on spawn
	-- filter = 2 if player PassList, then object id is player's networkassigned id, send on trade
	if filter == 1 then
		g_iHasAccessPass = 0
		g_iHasVIPPass = 0
		g_isAccessPassZone = 0
	
		local playerPassCount =	KEP_EventDecodeNumber( event )
		Log( "testPassListHandler: PASS CHECK: " .. playerPassCount)
		for i = 1, playerPassCount do
			local passId = KEP_EventDecodeNumber( event )
			
			if(passId == 1 ) then
				g_iHasAccessPass = 1
			elseif(passId == 2) then
				g_iHasVIPPass = 1
			end
			Log( "testPassListHandler: Player PassId: ("..tostring(i)..") "..tostring(passId) )
		end
		local passCount =	KEP_EventDecodeNumber( event )
		for i = 1, passCount do
			local passId = KEP_EventDecodeNumber( event )
			if(passId == 1 ) then
				g_isAccessPassZone = 1
			elseif(passId == 2) then
				g_isVIPZone = 1
			end
			Log( "testPassListHandler: Zone PassId: ("..tostring(i)..") "..tostring(passId) )
		end
	end	

	-- Stuff changed, so send an update to anyone who cares	
	local ev = KEP_EventCreate( "ResponseAccessPassInfoEvent" )
	KEP_EventEncodeNumber( ev, g_iHasAccessPass  )
	KEP_EventEncodeNumber( ev, g_iHasVIPPass)
	KEP_EventQueue( ev )

	local ev = KEP_EventCreate( "ResponseAccessPassZoneInfoEvent" )
	KEP_EventEncodeNumber( ev, g_isAccessPassZone  )
	KEP_EventEncodeNumber( ev, g_isVIPZone )
	KEP_EventQueue( ev )
end 

function pendingSpawnEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	KEP_ConfigEntitySubstitute( 10, ObjectType.DYNAMIC, 8 )
	KEP_ConfigEntitySubstitute( 11, ObjectType.DYNAMIC, 8 )
	KEP_ConfigEntitySubstitute( 12, ObjectType.DYNAMIC, 8 )
	KEP_ConfigEntitySubstitute( 13, ObjectType.DYNAMIC, 7 )
	KEP_ConfigEntitySubstitute( 14, ObjectType.DYNAMIC, 7 )
	KEP_ConfigEntitySubstitute( 15, ObjectType.DYNAMIC, 7 )
	
	-- we'll get this if the menu is not loaded, we load it a re-fire the same event for the menu 
	-- to handle
	if fromNetid == 0 then -- 0 if from server, 1 if local so we skip to avoid infinite loop
		local initialSpawn = KEP_EventDecodeNumber( event )
		local coverCharge =  KEP_EventDecodeNumber( event )
		local targetZoneInstanceId = KEP_EventDecodeNumber( event )
		zoneIndex = InstanceId_GetId(filter)
		if(prev_zone_was_arena_lobby == true and zoneIndex == 38) then
			sendPlayerLeftArenaEvent()
		end

		-- Record that zoning has started.
		local targetZoneIndex = InstanceId_GetClearedInstanceId(filter)
		local targetZoneType = InstanceId_GetType(filter)
		ResetZoningStatus(targetZoneIndex, targetZoneInstanceId, targetZoneType)	
		
        if PROGRESSIVE then
            -- DRF - New Unified Progress Menu
            Log("pendingSpawnEventHandler: Loading ZoneDownloader.xml (Rezone 3 of 4)")
            --ProgressOpen("Rezone") -- DRF - Open Rezone Just Incase We Got Here Not Through gotoURL()
            MenuOpen("ZoneDownloader.xml") -- Rezone - Stage 3 of 4
        else
            return KEP.EPR_OK -- let default handle it by spawning there
        end

        -- requeue event to menu
		local evt = KEP_EventCreate( KEP.PENDINGSPAWN_EVENT_NAME )
		KEP_EventSetFilter( evt, filter )
		KEP_EventSetObjectId( evt, objectid )
		KEP_EventEncodeNumber( evt, initialSpawn )
		KEP_EventEncodeNumber( evt, coverCharge )
		KEP_EventEncodeNumber( evt, instanceId )
		KEP_EventQueue( evt )
	end 
	return KEP.EPR_CONSUMED
end 

function sendPlayerLeftArenaEvent()
	if KEP_GetParentGameId() == 0 then
		local e = KEP_EventCreate( "PlayerLeftEvent")
		if e ~= 0 then
			KEP_EventAddToServer(e)
			KEP_EventQueue( e)
		end
	end
end

function newZoneLoadedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local zoneName = string.lower(KEP_EventDecodeString( event ))
	local currentUrl = string.lower( KEP_EventDecodeString( event ) )
	g_currentZoneIndex = KEP_GetZoneIndex()
	g_currentZoneType = KEP_GetZoneIndexType()
	g_currentUrl = currentUrl

	Log("newZoneLoadedHandler: zoneName="..zoneName.." zoneIndex="..g_currentZoneIndex.." zoneType="..g_currentZoneType.." url='" .. currentUrl.."'")
	
	local cfg = KEP_ConfigOpenWOK()
	if cfg ~= nil then
		KEP_ConfigSetString( cfg, "CurrentZoneURL", string.sub( currentUrl, 1, string.find( currentUrl, '/', 10 ) ) )
		KEP_ConfigSave( cfg )
	end
	
	if zoneName == string.lower("loginmenu.exg") then 				
        Log("newZoneLoadedHandler: Loading LoginMenu.xml")

		MenuOpen("LoginMenu.xml") -- drf - crash fix - using MenuOpen
		
		-- Reset our message counts.  We only receive new item counts in game.
		local config = KEP_ConfigOpenWOK()
		if config ~= nil then
			KEP_ConfigSetNumber( config, "NewMessages", 0 )
			KEP_ConfigSetNumber( config, "FriendReqs", 0 )
			KEP_ConfigSetNumber( config, "ChannelReqs", 0 )
			KEP_ConfigSetNumber( config, "NewGifts", 0 )
			KEP_ConfigSave( config )
		end
	elseif g_currentZoneType == KEP.CI_ARENA then		
		prev_zone_was_arena = true
		prev_zone_was_arena_lobby = false
		DisablePlayerTargeting()
	else		
		if prev_zone_was_arena then
			KEP_SetAvatarNames( 5 )
			KEP_SetControlConfig( 0 )
			local pte = KEP_EventCreate( "PlayerTargetingEnabledEvent")
			if pte ~= 0 then
				KEP_EventEncodeNumber(pte, 1)
				KEP_EventQueue( pte)
			end
		end
		
		if prev_zone_was_arena_lobby then
			sendPlayerLeftArenaEvent()
		end
		
		if zoneName == string.lower("arena_lobby.exg") then
			prev_zone_was_arena_lobby = true
		else
			prev_zone_was_arena_lobby = false
		end
		
		prev_zone_was_arena = false		
		if not first_zone_loaded then
		     first_zone_loaded = true		
			 g_loadTopPlaces = true
			local spalEvent = KEP_EventCreate( "SpawnAfterLoginEvent")
			KEP_EventQueue( spalEvent)
		else
			g_current_zoneinstanceid = KEP_GetZoneInstanceId()
		    parse_results = true
   			web_address = GameGlobals.WEB_SITE_PREFIX..GET_PLACE_INFORMATION_SUFFIX
			makeWebCall(web_address, WF.ZONE_LOADER, 0, 0)			
		end
	end

	return KEP.EPR_OK
end

function checkCreateFrom3DApp()
	if g_my3DApp ~= 0 then
        gotoURL("kaneva://"..tostring(g_my3DApp))
		g_my3DApp = 0;		
	end
end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

function appReturnEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	g_my3DApp = KEP_EventDecodeNumber( event )
end

function loadTopPlaces()
	
    -- DRF - Unsupported in KANEVIA
    if KEP.KANEVIA then
        LogWarn("loadTopPlaces: KANEVIA UNSUPPORTED")
        return
    end
    
    local gameId = KEP_GetParentGameId()
	if gameId == 0 then --we are WoK so try and open this menu
		if gNewbieCharCreated == false then

            MenuOpen("Dashboard.xml")

			--Get Events happening in the next hour, to be launched when Dashboard is
			local webAddress = GameGlobals.WEB_SITE_PREFIX .. "kgp/eventList.aspx?type=100&nextHours=" .. EVENT_NOTIFICATION_HOURS
			makeWebCall(webAddress, WF.LAUNCH_EVENT_COUNT, 1, 1)

			--Get Premium Events happening in the next hour, to be launched when Dashboard is
			local webAddress = GameGlobals.WEB_SITE_PREFIX .. "kgp/eventList.aspx?type=100&onlyPremium=Y&nextHours=" .. EVENT_NOTIFICATION_HOURS
			makeWebCall(webAddress, WF.LAUNCH_EVENT_COUNT, 1, 1)
		else
			checkCreateFrom3DApp()
		end
	end
end

function PatchProgressEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- if we get here, no one else consumed it
	-- we handle error cases, or create the progress menu
	-- TODO: are these the only cases we need to handle?
	local patchEnded = false
	
	if filter == MSG_RECONNECT then
		local url = KEP_EventDecodeString( event )
		textEvent = KEP_EventCreate( "RenderTextEvent" )
		if textEvent ~= nil then 
			KEP_EventEncodeString( textEvent, "Lost connection during patch" )
			KEP_EventEncodeNumber( textEvent, ChatType.System )
			KEP_EventQueue( textEvent )
		end
		patchEnded = true
	elseif filter == MSG_VERBOSE_ERROR then
		local url = KEP_EventDecodeString( event )
		local errNum = KEP_EventDecodeNumber( event )
		local errMsg = KEP_EventDecodeString( event )
		textEvent = KEP_EventCreate( "RenderTextEvent" )
		if textEvent ~= nil then 
			KEP_EventEncodeString( textEvent, "Error patching "..url..". -- "..errMsg.." ("..tostring(errNum)..")"  )
			KEP_EventEncodeNumber( textEvent, ChatType.System )
			KEP_EventQueue( textEvent )
		end
		patchEnded = true
	elseif filter == MSG_ERROR then 
		local url = KEP_EventDecodeString( event )
		local errNum = KEP_EventDecodeNumber( event )
		local errMsg = "Unknown"
		if errNum == ERR_NOTCONNECTED then 
			errMsg = "Not connected"
		elseif errNum == ERR_NOVERSION then
			errMsg = "No version"
		elseif errNum == ERR_TIMEOUT then
			errMsg = "Timeout"
		end 
		
		textEvent = KEP_EventCreate( "RenderTextEvent" )
		if textEvent ~= nil then 
			KEP_EventEncodeString( textEvent, "Error: "..errMsg )
			KEP_EventEncodeNumber( textEvent, ChatType.System )
			KEP_EventQueue( textEvent )
		end
		patchEnded = true
	elseif filter == MSG_ABORT then
		patchEnded = true;
	end 

	if not patchEnded then
		if MenuIsClosed("PatchProgress.xml") then 
			-- DRF - New Unified Progress Menu
			Log("PatchProgressEventHandler: Loading PatchProgress.xml (Rezone 2 of 4)")
			--ProgressOpen("Rezone") -- DRF - Open Rezone Just Incase We Got Here Not Through gotoURL()
			MenuOpen("PatchProgress.xml") -- Rezone - Stage 2 of 4
		end
	end

	return KEP.EPR_OK
end

function ClientSpawnHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local evt = KEP_EventCreate( "LaunchPersistantMenusEvent" )
	KEP_EventQueue( evt )
    player_zi = filter
    player_zi = InstanceId_GetId(player_zi)
    if player_zi == GameGlobals.CHARACTER_CREATION_ZI then
		local e = KEP_EventCreate( "ChatMinimizeEvent" )
        if e ~= 0 then
			KEP_EventEncodeNumber( e, CHAT_MINIMIZE_CODE )
			KEP_EventQueue( e )
        end
    
        if not gNewbieCharCreated then 

			-- Launch HomePresetSelect Menu (launches AvatarPresetSelect menu)
			MenuOpen("HomePresetSelect.xml")
            gNewbieCharCreated = true

			-- DRF - Black Screen Covers Everything During Zone Loading
			MenuOpen("BlackScreen.xml")

			return
        else
            if FORCED_STARTING_APARTMENT > 0 then
				local e = KEP_EventCreate( "ApartmentSelectionEvent" )
                if e ~= 0 then
					KEP_EventSetFilter( e, FORCED_STARTING_APARTMENT )
					KEP_EventAddToServer( e )
					KEP_EventQueue( e )
                end
            else
                MenuOpenModal("ApartmentSelector.xml")
            end    
        end
    end
	
	local gameId = KEP_GetParentGameId()
	
	-- Reset World Requests (NOT COMMUNITIES)
	local gid = KEP_GetGameId()
	if ((gid ~= KEP.GID_PRO) 
     and (gid ~= KEP.GID_PRE)
     and (gid ~= KEP.GID_DEV)
	 and (gid ~= KEP.GID_RC)) then
		WorldReqsReset(gid)
	end

	notifyTravelCompleted(gid, g_current_zoneinstanceid, g_currentZoneType)

	g_userName = KEP_GetLoginName()
	
	if (g_loadTopPlaces == true) then
		loadTopPlaces()
		g_loadTopPlaces = false
	end

	if not hasBuildBeenSet() then
		setBuildSettings()
	end

    return KEP.EPR_OK
end

function hasBuildBeenSet()
	local wokConfig = KEP_ConfigOpenWOK()
	if wokConfig and wokConfig ~= 0 then
		-- Reset appended because we're resetting EVERYONE!
		local buildSetOk, buildSet = KEP_ConfigGetString( wokConfig, "BuildSettingsReset" .. g_userName, "true" )
		if buildSetOk == 1 and buildSet == "true" then
			return true
		end
	end

	return false
end

--oldBuildMode, 1 = old build settings (legacy movements and widget on), 0 = new build settings
function setBuildSettings()
	-- Set Build Mode Settings for A/B test
	local groupList = KEP_GetABGroupList()
	local config = KEP_ConfigOpenBuild()
	if config ~= nil and config ~= 0  and groupList ~= nil then
		
		-- Set everyone to old build mode
		local oldBuildMode = 1

		-- Putting here because this will only be here for the duration of the test
		local SHOW_WIDGET = 6
		local ADVANCED_MOVEMENT = 7

		-- If either is not equal to what it should be, set
		local showWidgetOk, showWidget = KEP_ConfigGetNumber( config, "ShowWidget", 1 )
		if showWidgetOk ~= 1 or showWidget ~= oldBuildMode then
			local ev = KEP_EventCreate("BuildSettingsEvent")	
			KEP_EventSetFilter(ev, SHOW_WIDGET)
			KEP_EventEncodeNumber(ev, oldBuildMode)
			KEP_EventQueue( ev)
		end

		local advancedMovementOk, advancedMovement = KEP_ConfigGetNumber( config, "AdvancedMovement", 1 )
		if advancedMovementOk ~= 1 or advancedMovement ~= oldBuildMode then
			local ev = KEP_EventCreate("BuildSettingsEvent")
			KEP_EventSetFilter(ev, ADVANCED_MOVEMENT)
			KEP_EventEncodeNumber(ev, oldBuildMode)
			KEP_EventQueue( ev)
		end

		-- Recognize that it's been set
		local wokConfig = KEP_ConfigOpenWOK()
		if wokConfig and wokConfig ~= 0 then
			local username = KEP_GetLoginName()
			KEP_ConfigSetString( wokConfig, "BuildSettingsReset"..g_userName, "true" )
			KEP_ConfigSave( wokConfig )
		end
	end

    return KEP.EPR_OK
end

function notifyTravelCompleted(gameId, zoneInstanceId, zoneType)
	Log("notifyTravelCompleted: gameId="..tostring(gameId).." zoneInstanceId="..tostring(zoneInstanceId).." zoneType="..tostring(zoneType))
	local web_address = GameGlobals.WEB_SITE_PREFIX.."kgp/notify.aspx?action=TravelCompleted&gameId="..gameId.."&zoneInstanceId="..zoneInstanceId.."&zoneType="..zoneType
	makeWebCall(web_address, WF.NOTIFY_SPAWNING_IN_WORLD, 0, 0)
end

function DisablePlayerTargeting()
	local pte = KEP_EventCreate( "PlayerTargetingEnabledEvent")
	if pte ~= 0 then
		KEP_EventEncodeNumber(pte, 0)
		KEP_EventQueue( pte)
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter == WF.ZONE_LOADER and parse_results == true) then
		local s = 0 		-- start and end index of substring
		local numRec = 0   	 	-- number of records in this chunk
		local strPos = 0    	-- current position in string to parse
		local result = ""  		-- return string from find temp
		local lZoneType = 0     -- zone_type returned from the call
		accessData = KEP_EventDecodeString( event )
	    s, strPos, result = string.find(accessData, "<ReturnCode>(.-)</ReturnCode>")
		if result == "0" then
			s, strPos, lZoneType = string.find(accessData, "<current_zone_type>(.-)</current_zone_type>", strPos)
			if lZoneType == "3" then
			    parse_results = false
                web_address = GameGlobals.WEB_SITE_PREFIX .. VISIT_PLACE_SUFFIX .. "&zoneInstanceId=" .. g_current_zoneinstanceid .. "&zoneType=" .. 3
				makeWebCall(web_address, WF.ZONE_LOADER, 0, 0)
			else
			    parse_results = false
			    web_address = GameGlobals.WEB_SITE_PREFIX .. VISIT_PLACE_SUFFIX .. "&zoneInstanceId=" .. g_current_zoneinstanceid .. "&zoneType=" .. 6
				makeWebCall(web_address, WF.ZONE_LOADER, 0, 0)
			end
		end
	elseif filter == WF.LAUNCH_EVENT_COUNT then

		-- Launch Events Menu If Events Are Happening (but don't hide DailyBonus animation)
		local xmlData = KEP_EventDecodeString( event )
		local s, e, records = string.find( xmlData, "<NumberRecords>(%d-)</NumberRecords>")
		records = tonumber(records) or 0
		if records > 0 then
			MenuOpen("Events.xml")
			if MenuIsOpen("Framework_Welcome") then
				MenuBringToFront("Framework_Welcome.xml")
			end
		end
	end
end

function LastChanceRenderTextEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	return KEP.EPR_NOT_PROCESSED
end

function requestAccessPassInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local ev = KEP_EventCreate( "ResponseAccessPassInfoEvent" )
	KEP_EventEncodeNumber( ev, g_iHasAccessPass  )
	KEP_EventEncodeNumber( ev, g_iHasVIPPass)
	KEP_EventQueue( ev )
	return KEP.EPR_CONSUMED
end

function requestAccessPassZoneInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local ev = KEP_EventCreate( "ResponseAccessPassZoneInfoEvent" )
	KEP_EventEncodeNumber( ev, g_isAccessPassZone  )
	KEP_EventEncodeNumber( ev, g_isVIPZone )
	KEP_EventQueue( ev )
	return KEP.EPR_CONSUMED
end

function killBlackScreenHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	MenuClose("BlackScreen.xml")
end

function logonResponseHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	local connectionErrorCode = KEP_EventDecodeString( event )
	local hResultCode = KEP_EventDecodeNumber( event )
end

function zoneLoadingStartHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	MenuClose("Loading.xml")

    -- DRF - New Unified Progress Menu
	Log("zoneLoadingStartHandler(): Loading Loading.xml (Rezone 1 of 4)")
	MenuOpen("Loading.xml") -- Rezone - Stage 1 of 4
end

function zoneLoadingCompleteHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	MenuClose("Loading.xml")
end

function zoneLoadingFailedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	MenuClose("Loading.xml")
end

function chatOpenHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

function chatMinimizedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

function chatMissingHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

function chatTimeoutHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

-- Generates a new GUID indicating a distinct zoning instance.
function ResetZoningStatus(targetZoneIndex, targetZoneInstanceId, targetZoneType)
	g_zoningGuid = KEP_GetCurrentEpochTime().."_"..GetUserId().."_"..MathRandomInt(999999)
	g_targetZoneIndex = targetZoneIndex
	g_targetZoneInstanceId = targetZoneInstanceId
	g_targetZoneType = targetZoneType
	g_zoningStartTime = GetTimeMs()
end

-- Clears zoning metrics state.
function ClearZoningStatus()
	g_zoningGuid = nil
	g_targetZoneIndex = 0
	g_targetZoneInstanceId = 0
	g_targetZoneType = 0
	g_zoningStartTime = nil
end

-- Handles the ProgressEvent for zoning metrics
function CloseProgressMenuHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if g_zoningGuid ~= nil then 
		ClearZoningStatus()
	end
end
