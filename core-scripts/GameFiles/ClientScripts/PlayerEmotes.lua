--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\MenuScripts\\WebSuffix.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\CAM.lua")

PLAYER_EMOTE_EVENT = "PlayerEmoteEvent"

g_loadonce = 0
g_emoteidlist = {}

g_t_items = nil
gPlayerGender = "M"

RESPONSE_ACCESS_PASS_ZONE_INFO_EVENT = "ResponseAccessPassZoneInfoEvent"
RESPONSE_ACCESS_PASS_INFO_EVENT = "ResponseAccessPassInfoEvent"
g_iHasAccessPass = 0
g_iHasVIPPass = 0
g_iZoneHasAccessPass = 0
g_iZoneHasVIPPass = 0

function responseAccessPassInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iHasAccessPass = KEP_EventDecodeNumber(event)
	g_iHasVIPPass = KEP_EventDecodeNumber(event)
	return KEP.EPR_OK
end

function responseAccessPassZoneInfoHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	g_iZoneHasAccessPass = KEP_EventDecodeNumber(event)
	g_iZoneHasVIPPass = KEP_EventDecodeNumber(event)
	return KEP.EPR_OK
end

function playerEmoteEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local key = KEP_EventDecodeNumber(event)
	PlayAnimationIndex(key)
	return KEP.EPR_OK
end

function attribEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	if filter == 21 then
		if g_loadonce == 0 then
			for i=1,10 do g_emoteidlist[i]=-1 end
			LoadKeyBoardEmotes()
			getPlayerInventory(WF.PLAYER_INVENTORY_EMOTES, 1, 100000, "avatar", "", "emotes")
			g_loadonce = 1
		end
	end
end

-- Load emotes from '%APPDATA%\Kaneva\<gameId>\<userName>Emotes.cfg'
function LoadKeyBoardEmotes()

	-- Load Emotes
	local filename = GetUserName().."Emotes.cfg"
	local f, errmsg = KEP_FileOpenConfig(filename,"r")
	if (f == 0) then
		LogWarn("LoadKeyBoardEmotes: FileOpenConfig("..filename..") FAILED - Calling SaveDefaultKeyBoardEmotes()...")
		if (not SaveDefaultKeyBoardEmotes()) then
			LogError("LoadKeyBoardEmotes: SaveDefaultKeyBoardEmotes() FAILED")
			return false
		end
		return LoadKeyBoardEmotes()
	end

	--Read the number of pages that we have stored.
	g_emotes = 0
	ok=1
	msg=nil
	errMsg=nil
	ok,msg,errMsg = KEP_FileReadLine( f )
	g_maxpages = tonumber(msg) or 1
	ok,msg,errMsg = KEP_FileReadLine( f )
	page = tonumber(msg) or 1
	cpage=1
	for i=1,g_maxpages do
		for j =1,10 do
			ok,msg,errMsg = KEP_FileReadLine( f )
			ok,msg2,errMsg = KEP_FileReadLine( f )
			ok,msg3,errMsg = KEP_FileReadLine( f )
			if ok and (cpage==page) then
				g_emoteidlist[j]=tonumber(msg3)
			end
			g_emotes = g_emotes + 1
		end
		cpage=cpage+1
	end

	KEP_FileClose(f)

	Log("LoadKeyBoardEmotes: '"..filename.."' pages="..g_maxpages.." emotes="..g_emotes)

	return true
end

--Loads an emote when we press an key on the keyboard
function LoadEmotesOnKeyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	LoadKeyBoardEmotes()
end

--Play an Specific animation index
function PlayAnimationIndex(index)
	if g_emoteidlist[index]~=-1 then
		ChangeAnimation(g_emoteidlist[index])
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "attribEventHandler", "AttribEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "playerEmoteEventHandler", PLAYER_EMOTE_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "LoadEmotesOnKeyHandler", "LoadCurrentPageEmotes", KEP.HIGH_PRIO)
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassInfoHandler", RESPONSE_ACCESS_PASS_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "responseAccessPassZoneInfoHandler", RESPONSE_ACCESS_PASS_ZONE_INFO_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	gScriptDir = "GameFiles\\ClientScripts\\"
	KEP_EventRegister( PLAYER_EMOTE_EVENT,KEP.MED_PRIO )
	KEP_EventRegister( "MyCustomEvents",KEP.MED_PRIO )
	KEP_EventRegister( "LoadCurrentPageEmotes",KEP.MED_PRIO )
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( RESPONSE_ACCESS_PASS_INFO_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( RESPONSE_ACCESS_PASS_ZONE_INFO_EVENT, KEP.MED_PRIO )
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == WF.PLAYER_INVENTORY_EMOTES then
		-- temp: get animation from inventory for now -- eventually we will have a separate wok service
		local xmlData = KEP_EventDecodeString( event )
		gPlayerGender = KEP_GetLoginGender()
		g_t_items   = {}
		decodeInventoryItemsFromWeb( g_t_items, xmlData, true )
		return KEP.EPR_OK   -- don't consume this, leave it for inventory menu
	end
end
