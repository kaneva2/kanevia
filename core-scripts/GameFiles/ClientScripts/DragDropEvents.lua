--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Main event dispatcher for Drag-n-Drop
dofile("..\\Scripts\\KEP.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\UGCFunctions.lua")
dofile("..\\Scripts\\ChatType.lua")

gTarget = {}
gTarget.valid = 0
gTarget.pos = {}
gTarget.pos.x = 0
gTarget.pos.y = 0
gTarget.pos.z = 0
gTarget.normal = {}
gTarget.normal.x = 0
gTarget.normal.y = 0
gTarget.normal.z = 0
gTarget.type = ObjectType.UNKNOWN
gTarget.id = -1
gTarget.name = ""
gBtnState = 0
gKeyState = 0
gImportInfo = {}
gImportInfo.category = UGC_TYPE.UNKNOWN
gImportInfo.type = UGC_TYPE.UNKNOWN
gImportInfo.fullPath = ""
gImportInfo.sessionId = -1

gNextDropId = 0

----- Framework -----
g_playerModeActive = false

-- Fired when a file is dragged over or dropped into Client Window
function DragDropEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	local isInitial = KEP_EventDecodeNumber( event )	-- drag enter
	local isDropped = KEP_EventDecodeNumber( event )	-- drop on target
	local dropType = KEP_EventDecodeNumber( event )	-- DROP_FILE or DROP_TEXT
	gImportInfo.fullPath = KEP_EventDecodeString( event )
	local mouseX = KEP_EventDecodeNumber( event )
	local mouseY = KEP_EventDecodeNumber( event )
	gKeyState = KEP_EventDecodeNumber( event )

	if isDropped==0 then
		return KEP.ERR_OK
	end

	-- File dropped: take action based on import type
	if gImportInfo.type==UGC_TYPE.UNKNOWN then
		-- Determine import type
		local impCat, impType
		impCat, impType = UGC_ClassifyImportType( dropType, gImportInfo.fullPath, gTarget.type, gTarget.id, gTarget.pos.x, gTarget.pos.y, gTarget.pos.z, gTarget.normal.x, gTarget.normal.y, gTarget.normal.z )
		gImportInfo.category = impCat
		gImportInfo.type = impType
		UGCI.ReportPossibleImportAttempt( dropType, gImportInfo, gTarget, gBtnState, gKeyState )
	end

	-- File dropped: take action based on import type
	if isDropped~=0 and gImportInfo.type~=UGC_TYPE.UNKNOWN then
		if not KEP_IsOwnerOrModerator() then
			KEP_MessageBox( "You don't have permission to customize this place. " )
			return
		end

		local dropId = gNextDropId
		gNextDropId = gNextDropId + 1
		ProcessDroppedItem( dropId )
	end
	return KEP.EPR_OK
end

-- Fired when a file is dragged out of Client Window
function DragLeaveEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )

	-- Reset import type
	gImportInfo.category = UGC_TYPE.UNKNOWN
	gImportInfo.type = UGC_TYPE.UNKNOWN
	gImportInfo.fullPath = ""
	return KEP.EPR_OK
end

-- Fired when target changes
function TargetingEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid )
	gTarget.valid = 1
	gTarget.pos.x = KEP_EventDecodeNumber( event )
	gTarget.pos.y = KEP_EventDecodeNumber( event )
	gTarget.pos.z = KEP_EventDecodeNumber( event )
	gTarget.normal.x = KEP_EventDecodeNumber( event )
	gTarget.normal.y = KEP_EventDecodeNumber( event )
	gTarget.normal.z = KEP_EventDecodeNumber( event )
	gBtnState = KEP_EventDecodeNumber( event )
	gKeyState = KEP_EventDecodeNumber( event )
	gTarget.type = KEP_EventDecodeNumber( event )
	gTarget.id = KEP_EventDecodeNumber( event )
	gTarget.name = KEP_EventDecodeString( event )
	return KEP.EPR_OK
end

-- Called from C to initialize all the handlers in this script
function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "DragDropEventHandler", "DragDropEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "DragLeaveEventHandler", "DragLeaveEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "TargetingEventHandler", "TargetedPositionEventEx", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "returnFrameworkStateHandler", "FrameworkState", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "newZoneLoadedHandler", "ZoneLoadedEvent", KEP.HIGH_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( UGC_IMPORT_MENU_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( UGC_SUBMISSION_FEEDBACK_EVENT, KEP.MED_PRIO )
end

function ProcessDroppedItem( dropId )
	if g_playerModeActive then return end
	if gImportInfo.type==UGC_TYPE.INVALID or gImportInfo.type==UGC_TYPE.UNKNOWN then	-- UGC_TYPE.INVALID==-1, which make gImportInfo.category 0xF0 = URL so this must be checked before everything else
		UGCI.ReportInvalidImport( gImportInfo, gTarget, gBtnState, gKeyState )
	elseif gImportInfo.type==UGC_TYPE.COLLADA or gImportInfo.category==UGC_TYPE.DYNOBJ or gImportInfo.category==UGC_TYPE.CHARANIM then
		UGCI.CallUGCHandler("DaeImport.xml", 1, dropId, gImportInfo, gTarget, gBtnState, gKeyState )
	elseif gImportInfo.category==UGC_TYPE.TEXTURE then
		UGCI.CallUGCHandler("TextureImport.xml", 0, dropId, gImportInfo, gTarget, gBtnState, gKeyState )
	elseif gImportInfo.category==UGC_TYPE.URL then
		UGCI.CallUGCHandler("URLImport.xml", 0, dropId, gImportInfo, gTarget, gBtnState, gKeyState )
	elseif gImportInfo.category==UGC_TYPE.SOUND then
		if gImportInfo.type==UGC_TYPE.WAV_SOUND then
			UGCI.ImportAndSubmitWavFile( gImportInfo, gTarget, gBtnState, gKeyState )
		elseif gImportInfo.type==UGC_TYPE.OGG_SOUND then
			UGCI.ImportAndSubmitOggFile( gImportInfo, gTarget, gBtnState, gKeyState )
		elseif gImportInfo.type==UGC_TYPE.MP3_SOUND then
			UGCI.ImportAndSubmitMp3File( gImportInfo, gTarget, gBtnState, gKeyState )
		end
	end

	-- Reset import type
	gImportInfo.category = UGC_TYPE.UNKNOWN
	gImportInfo.type = UGC_TYPE.UNKNOWN
	gImportInfo.fullPath = ""
end

------------------------- Framework events -------------------------------

-- Called when a player changes Framework mode or when the Framework changes states (enabled/disabled)
function returnFrameworkStateHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	local eventTable = cjson.decode(Event_DecodeString(event))
	if eventTable.playerMode ~= nil then
		g_playerModeActive = (eventTable.playerMode == "Player")
	end
end

-- Called when a new zone is loaded
function newZoneLoadedHandler(dispatcher, fromNetId, event, eventId, filter, objectId)
	-- If a player was in framework player mode, exit it
	if g_playerModeActive then
		g_playerModeActive = false
	end
end