--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\Scripts\\FameIds.lua")

g_requesterNetID = 0
g_requesterName = ""
g_animationName = ""

YES_NO_BOX_EVENT = "YesNoBoxEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"

P2P_ANIM_PERMISSION_REPLY_EVENT = "P2pAnimPermissionReplyEvent"

P2P_TIME_OUT_EVENT = "P2pTimeOutEvent"

g_pendingRequest = false

--Timeout for request in seconds
P2P_TIMEOUT = 10

--Remeber timeout event handle to cancel if answered b4 timeout period
g_timeOutEvent = 0

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "p2pAnimRequestNotifyHandler", "P2pAnimRequestNotifyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "p2pAnimEventHandler", "P2pAnimEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "answerEventHandler", YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "requestTimeOutHandler", P2P_TIME_OUT_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( YES_NO_BOX_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( P2P_TIME_OUT_EVENT, KEP.MED_PRIO )
end


function requestTimeOutHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if g_pendingRequest then
		
		MenuClose( "YesNoBox.xml" )

		--deny request
		answer = 0
		g_timeOutEvent = 0

		local e = KEP_EventCreate( P2P_ANIM_PERMISSION_REPLY_EVENT )
		if e ~= 0 then
			KEP_EventSetFilter( e, g_requesterID )
			KEP_EventSetObjectId( e, answer )
			KEP_EventAddToServer( e )
			KEP_EventQueue( e )
		end
	end
end

function p2pAnimEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if filter ~= WF.P2P_ANIM then
		return
	end

	--We have an answer.  Cancel timeout event if its still around.
	if g_timeOutEvent ~= 0 then
		KEP_EventCancel( g_timeOutEvent)
		g_timeOutEvent = 0
	end

	--if answer is 0 then they do not want to do the animation otherwise we
	--get the player netid returned from yesnobox
	local answer = KEP_EventDecodeNumber( event )

	g_pendingRequest = false

	local e = KEP_EventCreate( P2P_ANIM_PERMISSION_REPLY_EVENT )
	if e ~= 0 then
		KEP_EventSetFilter( e, g_requesterID )
		KEP_EventSetObjectId( e, answer )
		KEP_EventAddToServer( e )
		KEP_EventQueue( e )
	end
end

function p2pAnimRequestNotifyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	g_requesterName = KEP_EventDecodeString(event)
	g_animationName = KEP_EventDecodeString(event)

	g_requesterID = filter

	MenuOpen( "YesNoBox.xml" )

	local yesNoEvent = KEP_EventCreate( YES_NO_BOX_EVENT )
	if yesNoEvent ~= 0 then
		g_pendingRequest = true
		KEP_EventSetFilter( yesNoEvent, WF.P2P_ANIM  )
		KEP_EventEncodeString( yesNoEvent, g_requesterName.." wants to "..g_animationName..".\n\nDo you wish to accept this request?" )
		KEP_EventEncodeString( yesNoEvent, "Confirm" )
		KEP_EventQueue( yesNoEvent )
	end

	g_timeOutEvent = KEP_EventCreate( P2P_TIME_OUT_EVENT )
	if g_timeOutEvent ~= 0 then

		KEP_EventQueueInFuture( g_timeOutEvent, P2P_TIMEOUT )

	end
	return KEP.EPR_OK
end
