--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\UGCFunctions.lua")
dofile("..\\Scripts\\ChatType.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\UGCGlobals.lua")
dofile("..\\Scripts\\UGCImportState.lua")

gUGCObjects = {}
gGlobalOptions = nil
gGlobalOptionsRequested = false

function UGCSubmissionFeedbackEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter==UGC_TYPE.OGG_SOUND or filter==UGC_TYPE.EQUIPPABLE or filter==UGC_TYPE.PARTICLE_EFFECT then
		UGCI.SubmissionFeedbackEventHandler(event, filter, objectid, filter, objectid, false, true, false )
		return KEP.EPR_CONSUMED
	end
	return KEP.EPR_OK
end

function UGCImportCompletionHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local ice = UGCI.DecodeImportCompletionEvent( event, filter, objectid )
	if gUGCObjects[ice.ugcType]==nil then
		gUGCObjects[ice.ugcType] = {}
	end
	Log("UGCImportCompletionHandler: ugcType="..ice.ugcType .. " ugcObjId=" .. ice.ugcObjId .. " ugcObjName=" .. ice.ugcObjName )

	gUGCObjects[ice.ugcType][ice.ugcObjId] = {}
	gUGCObjects[ice.ugcType][ice.ugcObjId].submitted = 0
	gUGCObjects[ice.ugcType][ice.ugcObjId].name = ice.ugcObjName
	gUGCObjects[ice.ugcType][ice.ugcObjId].param1 = ice.param1
	gUGCObjects[ice.ugcType][ice.ugcObjId].param2 = ice.param2

	if ice.submitNow==1 then
		UGCI.SubmitUGCObj( ice.ugcType, ice.ugcObjId, ice.isDerivation, ice.ugcObjName, "", ice.param1, ice.param2 )
	end
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "UGCSubmissionFeedbackEventHandler", UGC_SUBMISSION_FEEDBACK_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "UGCImportCompletionHandler", UGC_IMPORT_COMPLETION_EVENT, KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ClientSpawnHandler", "ClientSpawnEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "BrowserPageReadyHandler", "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "UGCGlobalOptionsQueryHandler", UGC_GLOBAL_OPTIONS_QUERY_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	UGCI.RegisterSubmissionEvents( )
	UGCI.RegisterUploadConfirmationMenuEvent( )

	KEP_EventRegister( UGC_IMPORT_COMPLETION_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( UGC_IMPORT_FAILED_MENU_EVENT	, KEP.MED_PRIO )
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO )
	KEP_EventRegister( UGC_GLOBAL_OPTIONS_QUERY_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( UGC_GLOBAL_OPTIONS_RESULT_EVENT, KEP.MED_PRIO )
end

function ClientSpawnHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	-- Load global UGC options if not already done: user must have logged in
	if gGlobalOptionsRequested==false then
		gGlobalOptionsRequested = true
		UGCI.LoadGlobalOptionsFromServer( WF.UGC_GLOBAL_OPTIONS )
	end
end

function BrowserPageReadyHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter==WF.UGC_GLOBAL_OPTIONS then
		local xml = KEP_EventDecodeString( event )
		local globalOptions = UGCI.ParseGlobalOptionsXml( xml )
		if globalOptions~=nil then
			gGlobalOptions = globalOptions
		end
		return KEP.EPR_CONSUMED
	end
	return KEP.EPR_NOT_PROCESSED
end

function UGCGlobalOptionsQueryHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if gGlobalOptions~=nil then
		local optNames = UGCI.ParseGlobalOptionsQueryEvent( event )
		local results = {}
		for idx, name in pairs(optNames) do
			results[name] = gGlobalOptions[name]
		end
		local resultEvent = UGCI.ComposeGlobalOptionsResultEvent( filter, results )
		KEP_EventQueue( resultEvent )
	end
	return KEP.EPR_CONSUMED
end
