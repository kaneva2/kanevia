--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\Scripts\\SoundPlacementIds.lua")
dofile("..\\Scripts\\BuildMode.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\Scripts\\PlacementSource.lua")

SOUND_CONFIG_ID_MIN = SoundPlacementIds.IDS_SOUNDPLACEMENT_PITCH
SOUND_CONFIG_ID_MAX = SoundPlacementIds.IDS_SOUNDPLACEMENT_RELATIVE

SoundPlaybackMode = {}
SoundPlaybackMode.SCRIPT = 0
SoundPlaybackMode.ALWAYS_ON = 1
SoundPlaybackMode.TRIGGERED_ON = 2
SoundPlaybackMode.TRIGGERED_ONOFF = 3

MODE_SOUNDS = 4 --See selection.lua for other modes

MAX_ANGLE = 360

g_customizationsReceived = false -- Have we received our customizations for this zone?

g_soundPlacements = {}
g_soundCustomizations = {}

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "ClientSpawnEventHandler", "ClientSpawnEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandler( "AddSoundEventHandler", "AddSoundEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerV( "RemoveDynamicObjectEventHandler", "RemoveDynamicObjectEvent" , KEP.MED_PRIO )
	KEP_EventRegisterHandlerV( "MoveAndRotateDynamicObjectEventHandler", "MoveAndRotateDynamicObjectEvent" , KEP.MED_PRIO )
	KEP_EventRegisterHandler( "TryOnExpireEventHandler", "TryOnExpireEvent" , KEP.MED_PRIO )
	KEP_EventRegisterHandler( "ServerSoundCustomizationEventHandler", "SoundCustomizationReplyEvent" , KEP.MED_PRIO )
	KEP_EventRegisterHandler( "SelectionEventHandler", "SelectEvent" , KEP.MED_PRIO )
	KEP_EventRegisterHandler( "UpdateSoundEventHandler",  "UpdateSoundCustomizationEvent", KEP.MED_PRIO )
	KEP_EventRegisterHandlerFilteredV( "SoundPlaybackControlHandler", "ControlDynamicObjectEvent", KEP.SOUND_START, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFilteredV( "SoundPlaybackControlHandler", "ControlDynamicObjectEvent", KEP.SOUND_STOP, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO )
	KEP_EventRegisterHandlerFilteredV( "SoundConfigHandler", "ControlDynamicObjectEvent", KEP.SOUND_CONFIG, KEP.FILTER_MATCH, KEP.NO_OBJID_FILTER, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "SoundCustomizationRequestEvent", KEP.MED_PRIO )
	KEP_EventRegister( "SoundCustomizationReplyEvent", KEP.MED_PRIO )
	KEP_EventRegister( "UpdateSoundCustomizationEvent", KEP.MED_PRIO )
	KEP_EventRegisterSecureV( "MoveAndRotateDynamicObjectEvent", KEP.MED_PRIO, KEP.ESEC_CLIENT+KEP.ESEC_SERVER, KEP.ESEC_CLIENT+KEP.ESEC_SERVER )
	KEP_EventRegisterSecureV( "ControlDynamicObjectEvent", KEP.HIGH_PRIO, KEP.ESEC_CLIENT+KEP.ESEC_SERVER, KEP.ESEC_CLIENT+KEP.ESEC_SERVER )
end

-- This handler should be called before any AddSoundEvent's from zone customizations
function ClientSpawnEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	g_customizationsReceived = false
	g_soundPlacements = {}
	g_soundCustomizations = {}

	local instanceID =  KEP_EventDecodeNumber( event )
	local zoneIndex = filter

	-- Request sound customizations from server (python script)
	-- requeue event to server
	local evt = KEP_EventCreate( "SoundCustomizationRequestEvent" )
	if evt ~= 0 then
		KEP_EventAddToServer( evt )
		KEP_EventSetFilter( evt, zoneIndex )
		KEP_EventEncodeNumber( evt, instanceID )
		KEP_EventQueue( evt )
	end

	return KEP.EPR_OK
end

function AddSoundEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

    local placementSource = filter
	local DOType = KEP_EventDecodeNumber(event)
	local pid = KEP_EventDecodeNumber(event)
	local slot = KEP_EventDecodeNumber(event)
	local x = KEP_EventDecodeNumber(event)
	local y = KEP_EventDecodeNumber(event)
	local z = KEP_EventDecodeNumber(event)

	if g_soundPlacements[pid]~=nil then
		LogWarn("AddSoundEventHandler: ALREADY ADDED - SP<"..pid..">")
		return
	end

	g_soundPlacements[pid] = { globalId = objectid, x = x, y = y, z = z }

	Log("AddSoundEventHandler: SP<"..pid..">")

	ok = ToBool(KEP_SoundPlacementAdd(pid))
	if not ok then
		LogError("KEP_SoundPlacementAdd() FAILED - soundPlacementId="..pid)
		return 
	end

	KEP_SoundPlacementSetPosition(pid, x, y, z)

	-- Wait for customization to get back from server
	if g_customizationsReceived then
		KEP_SoundPlacementPlaySound(pid, objectid) --change last number to glid when time comes
		ApplyCustomization( pid )
	end

	-- Force Sound Placement Mode To Script For Server Placed Sounds
    if placementSource == PLACEMENT_GENERATE then
		local sp = KEP_SoundPlacementGet( pid )
		GetSet_SetNumber( sp, SoundPlacementIds.PLAYBACKMODE, SoundPlaybackMode.SCRIPT )
	end

	--return KEP.EPR_CONSUMED -- Dont let client engine get sound placements
end

function ApplyCustomization( pid )
	local s = g_soundCustomizations[pid]
	if s == nil then return end
	updateSound( pid, s.pitch, s.gain, s.max_distance, s.roll_off, s.loop, s.delay, s.dx, s.dy, s.dz, s.cone_outer_gain, s.cone_inner_angle, s.cone_outer_angle )
end

function ServerSoundCustomizationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local first = 0       --start of substring
	local last = 0        --end of substring
	local strPos = 1      --posintion in string
	local count = 0

	local xml = KEP_EventDecodeString( event )

	first, strPos, sound = string.find( xml, "<S>(.-)</S>", strPos )
	while sound ~= nil do
		count = count + 1

		local pid, pitch, gain, max_distance, roll_off
		local loop, delay, dx, dy, dz
		local cone_outer_gain, cone_inner_angle, cone_outer_angle

		first, last, pid = string.find( sound, "<o>(.-)</o>", 1 )
		first, last, pitch = string.find( sound, "<p>(.-)</p>", 1 )
		first, last, gain = string.find( sound, "<g>(.-)</g>", 1 )
		first, last, max_distance = string.find( sound, "<m>(.-)</m>", 1)
		first, last, roll_off = string.find( sound, "<r>(.-)</r>", 1 )
		first, last, loop = string.find( sound, "<l>(.-)</l>", 1 )
		first, last, delay = string.find( sound, "<d>(.-)</d>", 1 )
		first, last, dx = string.find( sound, "<x>(.-)</x>", 1 )
		first, last, dy = string.find( sound, "<y>(.-)</y>", 1 )
		first, last, dz = string.find( sound, "<z>(.-)</z>", 1 )
		first, last, cone_outer_gain = string.find( sound, "<og>(.-)</og>", 1 )
		first, last, cone_inner_angle = string.find( sound, "<ia>(.-)</ia>", 1 )
		first, last, cone_outer_angle = string.find( sound, "<oa>(.-)</oa>", 1 )

		pid = tonumber(pid)
		pitch = tonumber(pitch)
		gain = tonumber(gain)
		max_distance = tonumber(max_distance)
		roll_off = tonumber(roll_off)
		loop = tonumber(loop)
		delay = tonumber(delay)
		dx = tonumber(dx)
		dy = tonumber(dy)
		dz = tonumber(dz)
		cone_outer_gain = tonumber(cone_outer_gain)
		cone_inner_angle = tonumber(cone_inner_angle)
		cone_outer_angle = tonumber(cone_outer_angle)

		Log("ServerSoundCustomizationEventHandler: SP<"..pid..">")

		saveSoundCustomization( pid, pitch, gain, max_distance, roll_off, loop, delay, dx, dy, dz, cone_outer_gain, cone_inner_angle, cone_outer_angle )

		first, strPos, sound = string.find( xml, "<S>(.-)</S>", strPos + 1)
	end

	--Got our customizations now we dont have to cache objects anymore
	g_customizationsReceived = true

	for pid, soundPlacement in pairs(g_soundPlacements) do
		KEP_SoundPlacementPlaySound(pid, soundPlacement.globalId)
		ApplyCustomization( pid )
	end

	return KEP.EPR_CONSUMED
end

function saveSoundCustomization( pid, pitch, gain, max_distance, roll_off, loop, delay, dx, dy, dz, cone_outer_gain, cone_inner_angle, cone_outer_angle )
	g_soundCustomizations[pid] = {
		pitch = pitch,
		gain = gain,
		max_distance = max_distance,
		roll_off = roll_off,
		loop = loop,
		delay = delay,
		dx = dx,
		dy = dy,
		dz = dz,
		cone_outer_gain = cone_outer_gain,
		cone_inner_angle = cone_inner_angle,
		cone_outer_angle = cone_outer_angle
	}
end

function updateSound( pid, pitch, gain, max_distance, roll_off, loop, delay, dx, dy, dz, cone_outer_gain, cone_inner_angle, cone_outer_angle )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_PITCH, pitch )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_GAIN, gain )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_MAX_DISTANCE, max_distance )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR, roll_off )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_LOOP, loop )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_LOOP_DELAY, delay )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_X, dx )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_Y, 0 )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_Z, dz )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN, cone_outer_gain )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE, cone_inner_angle )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE, cone_outer_angle )

	-- set visual rotation
	if dx ~= 0 or dy ~= 0 or dz ~= 0 then
		KEP_SoundPlacementSetRotationVector(pid, dx, 0, dz)
	end
end

-- pid is placement id
-- aid is attribute id
function setSoundAttribute( pid, aid, value )

	-- Valid Sound Placement ?
	if pid <= 0 then return false end
	if not ToBool(KEP_SoundPlacementIsValid(pid)) then return false end

	-- Set Sound Placement Attribute
	local sp = KEP_SoundPlacementGet( pid )
	if sp == nil then return false end
	GetSet_SetNumber( sp, aid, value )
	return true
end

-- pid is placement id
-- aid is attribute id
function getSoundAttribute( pid, aid )
	
	-- Valid Sound Placement ?
	if pid <= 0 then return 0 end
	if not ToBool(KEP_SoundPlacementIsValid(pid)) then return 0 end

	-- Get Sound Placement Attribute
	local sp = KEP_SoundPlacementGet( pid )
	if sp == nil then return 0 end
	return GetSet_GetNumber( sp, aid )
end

function getSoundDirection( pid )
	local dx = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_X )
	local dy = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_Y )
	local dz = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_Z )
	return dx, dy, dz
end

function setSoundDirection( pid, dx, dy, dz )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_X, dx )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_Y, dy )
	setSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_Z, dz )
end

function SelectionEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local count = KEP_EventDecodeNumber( event )
	for i = 1, count do
		local objectType = KEP_EventDecodeNumber( event )
		local pid = KEP_EventDecodeNumber( event )
		local state = KEP_EventDecodeNumber( event )

		if pid > 0 and objectType == ObjectType.SOUND then

			if state == 0 then

				local soundPlacement = g_soundPlacements[pid]

				-- Valid Sound Placement ?
				local valid = ToBool(KEP_SoundPlacementIsValid(pid))

				if soundPlacement~=nil and valid then

					sx, sy, sz = KEP_SoundPlacementGetPosition(pid)
					dx, dy, dz = getSoundDirection(pid)
					dy = 0 --set to 0 to avoid y rotation
					innerConeAngle = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE )
					outerConeAngle = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE )

					local posChanged = false
					local dirChanged = false

					if sx ~= nil and (soundPlacement.x ~= sx or soundPlacement.y ~= sy or soundPlacement.z ~= sz) then

						soundPlacement.x = sx
						soundPlacement.y = sy
						soundPlacement.z = sz

						posChanged = true
					end

					--check to see if sound is omni directional
					if innerConeAngle ~= MAX_ANGLE and outerConeAngle ~= MAX_ANGLE and (dx ~= 0 or dz ~= 0) then

						--its not 0 so the edit menu must have changed it.  see if it has changed since last deselect

						if g_soundCustomizations[pid] == nil or (g_soundCustomizations[pid].dx ~= dx or g_soundCustomizations[pid].dy ~= dy or g_soundCustomizations[pid].dz ~= dz) then

							if g_soundCustomizations[pid] == nil then
								g_soundCustomizations[pid] = {}
							end

							g_soundCustomizations[pid].dx = dx
							g_soundCustomizations[pid].dy = dy
							g_soundCustomizations[pid].dz = dz

							--set here so user doesnt have to hit save in edit menu
							setSoundDirection( pid, dx, dy, dz )

							sendUpdateToServer( pid )

							dirChanged = true
						end
					end

					--object was deselected and has position or direction change, alert others
					if posChanged or dirChanged then
						local e = KEP_EventCreate( "MoveAndRotateDynamicObjectEvent" )
						if e ~= 0 then
							KEP_EventAddToServer( e )
							KEP_EventSetObjectId(e, pid)
							KEP_EventEncodeNumber( e, soundPlacement.x )
							KEP_EventEncodeNumber( e, soundPlacement.y )
							KEP_EventEncodeNumber( e, soundPlacement.z )
							KEP_EventEncodeNumber( e, KEP_CurrentTime() )
							KEP_EventEncodeNumber( e, dx ) -- x direction
							KEP_EventEncodeNumber( e, dy ) -- y direction (dont set y or distortion will happen)
							KEP_EventEncodeNumber( e, dz ) -- z direction
							KEP_EventEncodeNumber( e, dz ) -- slide.x
							KEP_EventEncodeNumber( e, 0 )  -- slide.y
							KEP_EventEncodeNumber( e,-dx ) -- slide.z

							KEP_EventQueue( e )
						end
					end
				end

				-- Close SoundPlacementEdit Menu ?
				MenuClose("SoundPlacementEdit.xml")

			end -- state == 0
		end -- pid > 0 and objectType == ObjectType.SOUND
	end -- for count

	return KEP.EPR_OK
end

function RemoveDynamicObjectEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local pid = objectid
		
	-- DRF - Valid Sound Placement?
	if not ToBool(KEP_SoundPlacementIsValid(pid)) then return end

	Log("RemoveDynamicObjectEventHandler: SP<"..pid..">")
	
	KEP_SoundPlacementDel( pid )
	
	return KEP.EPR_OK
end

function UpdateSoundEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local pid = objectid
		
	-- DRF - Valid Sound Placement?
	if not ToBool(KEP_SoundPlacementIsValid(pid)) then return end

	local glid = KEP_EventDecodeNumber( event )
	local pitch = KEP_EventDecodeNumber( event )
	local gain = KEP_EventDecodeNumber( event )
	local max_distance = KEP_EventDecodeNumber( event )
	local roll_off = KEP_EventDecodeNumber( event )
	local loop = KEP_EventDecodeNumber( event )
	local loop_delay = KEP_EventDecodeNumber( event )
	local cone_outer_gain = KEP_EventDecodeNumber( event )
	local cone_inner_angle = KEP_EventDecodeNumber( event )
	local cone_outer_angle = KEP_EventDecodeNumber( event )
	local dir_x = KEP_EventDecodeNumber( event )
	local dir_y = KEP_EventDecodeNumber( event )
	local dir_z = KEP_EventDecodeNumber( event )

	updateSound(pid, pitch, gain, max_distance, roll_off, loop, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle )
end

function MoveAndRotateDynamicObjectEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local pid = objectid
		
	-- DRF - Valid Sound Placement?
	if not ToBool(KEP_SoundPlacementIsValid(pid)) then return end

	-- Decode Sound Position & Rotation
	local x = KEP_EventDecodeNumber( event )
	local y = KEP_EventDecodeNumber( event )
	local z = KEP_EventDecodeNumber( event )
	KEP_EventDecodeNumber( event ) -- skip timeMs
	local rx = KEP_EventDecodeNumber( event )
	local ry = KEP_EventDecodeNumber( event )
	local rz = KEP_EventDecodeNumber( event )
	-- Ignore remaining data in the event: sx, sy, sz

	-- Set Sound Position & Rotation
	KEP_SoundPlacementSetPosition(pid, x, y, z)
	setSoundDirection(pid, rx, ry, rz)
end

function TryOnExpireEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local playerId    = KEP_EventDecodeNumber( event )
	local glid        = KEP_EventDecodeNumber( event )
	local baseGlid    = KEP_EventDecodeNumber( event )
	local useType     = KEP_EventDecodeNumber( event )
	local expire      = KEP_EventDecodeNumber( event )
	local placeId     = KEP_EventDecodeNumber( event )
	if useType == KEP.USE_TYPE_SOUND then
		local ev = KEP_EventCreate( "EnterBuildModeEvent")
		KEP_EventSetFilter(ev, 1)
		KEP_EventEncodeNumber(ev, MODE_SOUNDS)
		KEP_EventQueue( ev)
	end
end

function sendUpdateToServer( pid )
	local glid
	if g_soundPlacements[pid]~=nil then
		glid = g_soundPlacements[pid].globalId
	end

	if glid==nil or glid<=0 then
		return
	end

	local e = KEP_EventCreate( "UpdateSoundCustomizationEvent" )
	if e ~= 0 then
		instanceId = KEP_GetZoneInstanceId()
		KEP_EventAddToServer( e )
		KEP_EventSetObjectId( e, pid )
		KEP_EventEncodeNumber( e, glid )
		local value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_PITCH )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_GAIN )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_MAX_DISTANCE )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_LOOP )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_LOOP_DELAY )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_X )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_Y )
		KEP_EventEncodeNumber( e, value )
		value = getSoundAttribute( pid, SoundPlacementIds.IDS_SOUNDPLACEMENT_DIR_Z )
		KEP_EventEncodeNumber( e, value )
		KEP_EventQueue( e )
	end
end

function SoundPlaybackControlHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local pid = objectid
	local controlId = filter
	if controlId~=KEP.SOUND_START and controlId~=KEP.SOUND_STOP then
		return KEP.EPR_NOT_PROCESSED
	end

	local argCount = KEP_EventDecodeNumber( event ) or 0
	if argCount < 1 then return KEP.EPR_CONSUMED end

	local delayInMS = KEP_EventDecodeNumber( event ) or 0
	if delayInMS == 0 then

		-- DRF - Valid Sound Placement?
		if not ToBool(KEP_SoundPlacementIsValid(pid)) then
			LogError("SoundPlaybackControlHandler: SoundPlacementIsValid() FAILED - SP<"..pid..">")
			return KEP.EPR_CONSUMED
		end

		local sp = KEP_SoundPlacementGet(pid)
		if sp == nil then
			LogError("SoundPlaybackControlHandler: SoundPlacementGet() FAILED - SP<"..pid..">")
			return KEP.EPR_CONSUMED
		end

		-- Play Or Stop Sound If In Range Of Player
		GetSet_SetNumber( sp, SoundPlacementIds.IDS_SOUNDPLACEMENT_PLAYBACKCTRL, controlId )
	else
		local e = KEP_EventCreate( "ControlDynamicObjectEvent" )
		KEP_EventSetObjectId( e, pid )
		KEP_EventSetFilter( e, controlId )
		KEP_EventEncodeNumber( e, 1 )		-- argument count
		KEP_EventEncodeNumber( e, 0 )		-- no delay
		KEP_EventQueueInFutureMS( e, delayInMS )
	end

	return KEP.EPR_CONSUMED
end

function SoundConfigHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local pid = objectid
	local controlId = filter
	if controlId~=KEP.SOUND_CONFIG then
		return KEP.EPR_NOT_PROCESSED
	end

	local argCount = KEP_EventDecodeNumber( event )
	if argCount < 2 or math.fmod(argCount,2)~=0 then
		LogError( "SoundConfigHandler: Event Missing Arguments" )
	else

		-- DRF - Valid Sound Placement?
		if not ToBool(KEP_SoundPlacementIsValid(pid)) then
			LogError("SoundConfigHandler: SoundPlacementIsValid() FAILED - SP<"..pid..">")
			return KEP.EPR_CONSUMED
		end

		local sp = KEP_SoundPlacementGet(pid)
		if sp == nil then
			LogError("SoundConfigHandler: SoundPlacementGet() FAILED - SP<"..pid..">")
			return KEP.EPR_CONSUMED
		end

		for argIdx=1,argCount,2 do
			local id = KEP_EventDecodeNumber( event )
			local val = KEP_EventDecodeNumber( event )
			if id >= SOUND_CONFIG_ID_MIN and id <= SOUND_CONFIG_ID_MAX then		-- validate config ID range
				if id==SoundPlacementIds.IDS_SOUNDPLACEMENT_PITCH or
				id==SoundPlacementIds.IDS_SOUNDPLACEMENT_GAIN or
				id==SoundPlacementIds.IDS_SOUNDPLACEMENT_MAX_DISTANCE or
				id==SoundPlacementIds.IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR or
				id==SoundPlacementIds.IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN then
					val = val / 1000												-- data helper for float type config values
				end
				GetSet_SetNumber( sp, id, val )
			end
		end
	end

	return KEP.EPR_CONSUMED
end
