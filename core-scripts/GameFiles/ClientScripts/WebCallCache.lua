--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile( "..\\Scripts\\KEP.lua" )
dofile("..\\MenuScripts\\CommonFunctions.lua")

Log = LogOff -- disable logging

g_cachedURLs = {}
g_refreshTimeSec = 600
MIN_REFRESH_TIME_SEC = 5

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "BrowserPageReadyEventHandler", "BrowserPageReadyEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "MakeWebCallCachedEventHandler", "MakeWebCallCachedEvent", KEP.MED_PRIO)
	KEP_EventRegisterHandler( "WebCallCacheRefreshEventHandler", "WebCallCacheRefreshEvent", KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( "BrowserPageReadyEvent", KEP.MED_PRIO)
	KEP_EventRegister( "MakeWebCallCachedEvent", KEP.MED_PRIO )
	KEP_EventRegister( "WebCallCacheRefreshEvent", KEP.MED_PRIO )
end

function WebCall(url, filter, startIndex, maxItems)
--	Log("WebCall: "..url.." ["..filter..","..startIndex..","..maxItems.."]")
	makeWebCall(url, filter, startIndex, maxItems)
end

function MakeWebCallCachedEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	-- Get Event Parameters
	local url = KEP_EventDecodeString(event) or ""
	local filter = KEP_EventDecodeNumber(event) or 0
	local startIndex = KEP_EventDecodeNumber(event) or 0
	local maxItems = KEP_EventDecodeNumber(event) or 0
	local staleTimeSec = KEP_EventDecodeNumber(event) or 0 -- time until results are stale (sec)
	local staleDoRefresh = KEP_EventDecodeString(event) or "" -- automatically refresh stale results (""=off "menu.xml"=while menu open)
	if (url == "") then
		return
	end

	-- Set Garbage Collect Interval
	if ((staleDoRefresh ~= "") and (staleTimeSec > MIN_REFRESH_TIME_SEC)) then
		if (g_refreshTimeSec > staleTimeSec) then
			g_refreshTimeSec = staleTimeSec
			WebCallCacheRefreshEventQueue()
		end
	end

	-- Get Cache Result
	local cachedURL = g_cachedURLs[url]
	if cachedURL == nil then
		g_cachedURLs[url] = {}
		cachedURL = g_cachedURLs[url]
	end
	local cachedResult = cachedURL[filter..","..startIndex..","..maxItems]

	-- Check Cache Hit/Miss
	local cacheHit = (cachedResult ~= nil)
	if (cacheHit) then
		local timeSec = GetTimeElapsedMs(cachedResult.cachedTimeMs) / 1000
		local resultOk = (cachedResult.httpStatusCode ~= nil) and (cachedResult.result ~= nil)

		-- Cache Hit (Check Result Freshness)
		--if (resultOk and (timeSec < staleTimeSec)) then
		if (resultOk) then
			-- Fresh Results (Fire Page Ready Event & Do Web Call)
			Log("MakeWebCallCachedEventHandler: HIT FRESH timeSec="..timeSec)
			local ev = KEP_EventCreate("BrowserPageReadyEvent")
			KEP_EventSetFilter(ev, filter)
			KEP_EventEncodeString(ev, cachedResult.result)
			KEP_EventEncodeNumber(ev, cachedResult.httpStatusCode)
			KEP_EventEncodeString(ev, url)
			KEP_EventQueue(ev)
		else
			-- Stale Results (Do Web Call)
			Log("MakeWebCallCachedEventHandler: HIT STALE timeSec="..timeSec)
		end

		-- Update Modifyable Parameters
		cachedResult.staleTimeSec = staleTimeSec
	else

		-- Cache Miss (Create New Result)
		Log("MakeWebCallCachedEventHandler: MISS")
		local newCachedResult = {}
		newCachedResult.filter = filter
		newCachedResult.startIndex = startIndex
		newCachedResult.maxItems = maxItems
		newCachedResult.httpStatusCode = nil
		newCachedResult.result = nil
		newCachedResult.cachedTimeMs = 0
		newCachedResult.staleTimeSec = staleTimeSec
		newCachedResult.staleDoRefresh = staleDoRefresh
		g_cachedURLs[url][filter..","..startIndex..","..maxItems] = newCachedResult
	end

	-- Do Web Call
	WebCall(url, filter, startIndex, maxItems)
end

function BrowserPageReadyEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	
	-- Extract All Webcall Return Parameters (all seem optional but we need them all)
	if KEP_EventMoreToDecode(event) == 0 then return end
	local result = KEP_EventDecodeString(event)
	if KEP_EventMoreToDecode(event) == 0 then return end
	local httpStatusCode = KEP_EventDecodeNumber(event)
	if KEP_EventMoreToDecode(event) == 0 then return end
	local url = KEP_EventDecodeString(event)
	if KEP_EventMoreToDecode(event) == 0 then return end
	local startIndex = KEP_EventDecodeNumber(event)
	if KEP_EventMoreToDecode(event) == 0 then return end
	local maxItems = KEP_EventDecodeNumber(event)

	local dstStr = ""
	local cachedURL = g_cachedURLs[url]
	if cachedURL ~= nil then
		local cachedResult = cachedURL[filter..","..startIndex..","..maxItems]
		if cachedResult ~= nil then
			dstStr = "-> "..cachedResult.staleDoRefresh
			cachedResult.result = result
			cachedResult.httpStatusCode = httpStatusCode
			cachedResult.cachedTimeMs = GetTimeMs()
		end
--		Log("BrowserPageReadyEventHandler: "..url.." ["..filter..","..startIndex..","..maxItems.."] "..dstStr) --.."...\n"..result)
	end
end

function WebCallCacheRefreshEventQueue()
	local ev = KEP_EventCreate( "WebCallCacheRefreshEvent", KEP.MED_PRIO )
	KEP_EventQueueInFuture( ev, g_refreshTimeSec)
end

function WebCallCacheRefreshEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	Log("WebCallCacheRefreshEventHandler: ...")

	-- Refresh Expired Urls (only for named dialogs that are currently open)
	for url,_ in pairs(g_cachedURLs) do
		local curl = g_cachedURLs[url]
		for scurl,_ in pairs(curl) do
			local cr = curl[scurl]
			local timeSec = GetTimeElapsedMs(cr.cachedTimeMs) / 1000
			local doRefresh = ((cr.staleDoRefresh ~= "") and MenuIsOpen(cr.staleDoRefresh))
			local needsRefresh = doRefresh and ((timeSec + g_refreshTimeSec) >= cr.staleTimeSec)

			-- Do Web Call If Needed
			if (needsRefresh) then
				Log("cacheResult: '"..url.."' ["..scurl.."] -> "..cr.staleDoRefresh.." timeSec="..timeSec)
				WebCall(url, cr.filter, cr.startIndex, cr.maxItems)
			end
		end
	end

	-- Queue Next Refresh
	WebCallCacheRefreshEventQueue()
end
