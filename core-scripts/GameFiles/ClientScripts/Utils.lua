--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------


-----------------------------------------------------------
----- Double Queues
-----------------------------------------------------------

Queue = {}
function Queue.new()
	return {first = 0, last = -1}
end

function Queue.pushLeft( queue, value )
	local first = queue.first - 1
	queue.first = first
	queue[first] = value
end

function Queue.pushRight( queue, value )
	local last = queue.last + 1
	queue.last = last
	queue[last] = value
end

function Queue.popLeft( queue )
	local first = queue.first
	if first > queue.last then return nil end  -- queue is empty
	local value = queue[first]
	queue[first] = nil        -- to allow garbage collection
	queue.first = first + 1
	return value
end

function Queue.popRight( queue )
	local last = queue.last
	if queue.first > last then return nil end  -- queue is empty
	local value = queue[last]
	queue[last] = nil         -- to allow garbage collection
	queue.last = last - 1
	return value
end

function Queue.getSize( queue )
	local size = ( queue.last - queue.first ) + 1
	if size < 0 then size = 0 end
	return size
end

-----------------------------------------------------------


-----------------------------------------------------------
----- String Split
-----------------------------------------------------------

function stringSplit( str, delim )

	local l = {n=0}
	local f = function( str )
		l.n = l.n + 1
		l[l.n] = str
	end
	local p = "%s*(.-)%s*"..delim.."%s*"
	str = string.gsub( str, "^%s+", "" )
	str = string.gsub( str, "%s+$", "" )
	str = string.gsub( str, p, f )
	l.n = l.n + 1
	l[l.n] = string.gsub( str, "(%s%s*)$", "" )
	l.n = nil
	return l
end

-----------------------------------------------------------


-----------------------------------------------------------
----- Math FX
-----------------------------------------------------------

MathFX = {}
function MathFX.lerp( start, stop, val )
	return ( ( 1.0 - val ) * start ) + ( val * stop )
end

function MathFX.easeOut( start, stop, val )
	return MathFX.lerp( start, stop, math.sin( val * math.pi * 0.5 ) )
end

function MathFX.easeInOut( start, stop, val )
	return MathFX.lerp( start, stop, val * val * ( 3.0 - 2.0 * val ) )
end

function MathFX.boing( start, stop, val )
	if val < 0 then val = 0 end
	if val > 1 then val = 1 end
	val = ( math.sin( val * math.pi * ( 0.2 + 2.5 * val * val * val ) ) * math.pow( 1.0 - val, 2.2 ) + val ) * ( 1.0 + ( 1.2 * ( 1.0 - val ) ) )
	return start + ( stop - start ) * val
end

-----------------------------------------------------------


-----------------------------------------------------------
----- Bit Flags
-----------------------------------------------------------

BitFlag = {}
function BitFlag.test( comp, flag )
	return math.fmod( comp, 2 * flag ) >= flag
end

function BitFlag.set( comp, flag )
	if math.fmod( comp, 2 * flag ) >= flag then
		return comp
	end
	return comp + flag
end

function BitFlag.clear( comp, flag )
	if math.fmod( comp, 2 * flag ) >= flag then
		return comp - flag
	end
	return comp
end
