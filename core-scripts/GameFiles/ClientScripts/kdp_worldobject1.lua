--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\Scripts\\GameGlobals.lua")
dofile("..\\MenuScripts\\WebSuffix.lua")
dofile("..\\MenuScripts\\MenuHelper.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")

g_gameId = 0
g_objectId = 0
g_networkAssignedId = 0
g_tempRadius = 0
g_miscInt = 0
g_miscStr = nil
g_x = 0
g_y = 0
g_z = 0
g_xCenter = 0
g_yCenter = 0
g_zCenter = 0

-- Are dance events locked
g_kdpLock = false

-- Object id of the dance floor that acquired lock
g_kdpLockOwner = 0

YES_NO_BOX_EVENT = "YesNoBoxEvent"
YES_NO_ANSWER_EVENT = "YesNoAnswerEvent"
KDP_MENU_CLOSE_EVENT = "KDPCloseEvent"
PLAY_KDP_EVENT = "PlayKDPEvent"

function loadMenu( menuXml )
	if MenuIsClosed(menuXml) then
		local handle = MenuOpen(menuXml)

		local sh = Dialog_GetStatic(handle, "GamePlay")
		dialogx = Dialog_GetLocationX(handle)
		dialogy = Dialog_GetLocationY(handle)
		gamePlayCtrlWidth = Dialog_GetWidth(handle)
		gamePlayCtrlHeight = Dialog_GetHeight(handle)

		local e = KEP_EventCreate( KEP.KDP1_EVENT )
		KEP_EventEncodeNumber( e, g_gameId )
		KEP_EventEncodeNumber( e, g_objectId )
		KEP_EventEncodeNumber( e, g_networkAssignedId )
		KEP_EventEncodeNumber( e, g_tempRadius )
		KEP_EventEncodeNumber( e, g_miscInt )
		KEP_EventEncodeString( e, g_miscStr )
		KEP_EventEncodeNumber( e, g_x )
		KEP_EventEncodeNumber( e, g_y )
		KEP_EventEncodeNumber( e, g_z )
		KEP_EventEncodeNumber( e, g_xCenter )
		KEP_EventEncodeNumber( e, g_yCenter )
		KEP_EventEncodeNumber( e, g_zCenter )
		KEP_EventEncodeNumber( e, dialogx )
		KEP_EventEncodeNumber( e, dialogy )
		KEP_EventEncodeNumber( e, gamePlayCtrlWidth )
		KEP_EventEncodeNumber( e, gamePlayCtrlHeight )
		KEP_EventSetFilter( e, KEP.FILTER_START_GAME)
		KEP_EventQueue( e )

		makeWebCall(GameGlobals.WEB_SITE_PREFIX .. VISIT_DANCEFLOOR_SUFFIX .. "&objectId=" .. g_objectId, WF.KDP, 0, 0)
	end
end

function kdpMenuCloseHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
--	local kdpHandle = KEP_EventDecodeNumber( event )
end

function answerEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	loadMenu( "kdp1.xml" )
	g_kdpLockOwner = 0
	g_kdpLock = false
end

function worldObjectTriggerHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if (filter == 0) then return end -- filter is 0 when event is sent from scripted client triggers
	--filter is global id of dance floor (type)
	action  = KEP_EventDecodeNumber( event );

	if action == 1 then
		if not g_kdpLock then
			g_kdpLock = true
			g_kdpLockOwner = objectid
			MenuOpen( "kdp_wanttoplay.xml")
			saveEventData( event, filter, objectid )
		else
			return
		end
	elseif action == 2 then

		-- Close Game Menu
		local kdpOpen = MenuIsOpen("kdp1.xml")
		MenuClose("kdp1.xml")

		if objectid == g_kdpLockOwner then

			-- Send End Game Event
			if (kdpOpen ~= 0) then
				local e = KEP_EventCreate( KEP.KDP1_EVENT )
				KEP_EventEncodeNumber( e, filter )
				KEP_EventSetFilter( e, KEP.FILTER_END_GAME)
				KEP_EventQueue( e )
			end

			MenuClose("kdp_wanttoplay.xml")

			g_kdpLockOwner = 0
			g_kdpLock = false
		else
			return
		end
	end
	return KEP.EPR_OK
end

function saveEventData( event, filter, objectid )
	g_networkAssignedId = KEP_EventDecodeNumber( event )
	g_tempRadius = KEP_EventDecodeNumber( event )
	g_miscInt = KEP_EventDecodeNumber( event )
	g_miscStr = KEP_EventDecodeString( event )
	g_x = KEP_EventDecodeNumber( event )
	g_y = KEP_EventDecodeNumber( event )
	g_z = KEP_EventDecodeNumber( event )
	g_xCenter = KEP_EventDecodeNumber( event )
	g_yCenter = KEP_EventDecodeNumber( event )
	g_zCenter = KEP_EventDecodeNumber( event )
	g_objectId = objectid
	g_gameId = filter
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandlerFiltered( "answerEventHandler", YES_NO_ANSWER_EVENT, KEP.FILTER_KDP_ANSWER_PLAY, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "worldObjectTriggerHandler", "TriggerEvent", KEP.KDP1ID, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "worldObjectTriggerHandler", "TriggerEvent", KEP.KDP2ID, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "worldObjectTriggerHandler", "TriggerEvent", KEP.KDP3ID, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "worldObjectTriggerHandler", "TriggerEvent", KEP.LARGE_DISCO_FLOOR, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "worldObjectTriggerHandler", "TriggerEvent", KEP.MEDIUM_DISCO_FLOOR, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "worldObjectTriggerHandler", "TriggerEvent", KEP.SMALL_DISCO_FLOOR, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "worldObjectTriggerHandler", "TriggerEvent", KEP.SNOWGLOBE_DISCO_FLOOR, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "worldObjectTriggerHandler", "TriggerEvent", KEP.ANIMATED_FLOOR_32, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "worldObjectTriggerHandler", "TriggerEvent", KEP.ANIMATED_FLOOR_48, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandlerFiltered( "worldObjectTriggerHandler", "TriggerEvent", KEP.ANIMATED_FLOOR_64, 0, 0, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "answerEventHandler", PLAY_KDP_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "kdpMenuCloseHandler", KDP_MENU_CLOSE_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( YES_NO_BOX_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( YES_NO_ANSWER_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( KDP_MENU_CLOSE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( PLAY_KDP_EVENT, KEP.MED_PRIO )
end
