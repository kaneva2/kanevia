--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("..\\Scripts\\KEP.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")

V_2_0 = true

menuXml = nil

DYNAMIC_OBJ_MENU_EVENT = "DynamicObjectMenuEvent"
PICTURE_FRAME_MENU_EVENT = "PictureFrameMenuEvent"
FLASHGAME_MENU_OPEN_EVENT = "OpenFlashGameMenuEvent"
CLOSE_DYNMENU_EVENT = "CloseDynMenuEvent"
FLASHGAME_MENU_CLOSE_EVENT = "CloseFlashGameMenuEvent"
INTERACTIVE_HUD_TEXT_EVENT = "InteractiveHudTextEvent"

-- Position to open menu at instead of right on top of object
X_COORD = 400
Y_COORD = 0

menuOpenEvent = 0
g_isInteractive = 0

g_ObjectPurchasingHandle = nil

function loadMenu(owner, menuXml, placementId, friendId, x, y,  isAttachableObject, name, canPlayMovie, description, isInteractive, isLocal, isDerivable )

	if isAttachableObject == 0 or (isAttachableObject ~= 0 and canPlayMovie ~= 0) then
		-- show regular dynamic obj menu
		if menuXml == nil then
			if owner ~= 0 then
				menuXml = "DynamicObjectMenu.xml"
			else
				menuXml = "DynamicObjectMenuVisitor.xml"
			end
			Dialog_CreateAtCoordinates( menuXml, X_COORD, Y_COORD)
  		end
	else
		-- show picture frame menu, which is the only attachable obj
		if menuXml == nil then
			menuXml = "PictureFrameMenu.xml"
			Dialog_CreateAtCoordinates( menuXml, X_COORD, Y_COORD )
		end

		--fire event to pass placement id to the menu script
		menuOpenEvent = KEP_EventCreate( PICTURE_FRAME_MENU_EVENT )
		KEP_EventEncodeNumber( menuOpenEvent, placementId )
		KEP_EventEncodeString( menuOpenEvent, name )
		KEP_EventEncodeNumber( menuOpenEvent, friendId )
		KEP_EventQueue( menuOpenEvent )
	end

    --fire event to pass placement id to the menu script
	menuOpenEvent = KEP_EventCreate( DYNAMIC_OBJ_MENU_EVENT )
	KEP_EventEncodeNumber( menuOpenEvent, placementId )
	KEP_EventEncodeString( menuOpenEvent, name )
	KEP_EventEncodeNumber( menuOpenEvent, canPlayMovie )
	KEP_EventEncodeNumber( menuOpenEvent, isAttachableObject )
	KEP_EventEncodeString( menuOpenEvent, description )
	KEP_EventEncodeNumber( menuOpenEvent, isInteractive )
	KEP_EventEncodeNumber( menuOpenEvent, isLocal )
	KEP_EventEncodeNumber( menuOpenEvent, isDerivable )
	KEP_EventQueue( menuOpenEvent )
	
	return menuXml
end

function dynamicObjectSelectedHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local menuXmlWas = menuXml
	menuXml = nil

	local placementId  = KEP_EventDecodeNumber( event );
	local friendId = KEP_EventDecodeNumber( event );
	local x  = KEP_EventDecodeNumber( event );
	local y  = KEP_EventDecodeNumber( event );
	local isAttachableObject = KEP_EventDecodeNumber( event );
	local name = KEP_EventDecodeString( event );
	local canPlayMovie = KEP_EventDecodeNumber( event );
    if V_2_0 then
		g_isInteractive = KEP_EventDecodeNumber( event );
    end
	local description = KEP_EventDecodeString( event );
	local rightClick = KEP_EventDecodeNumber( event );
	local leftClick = KEP_EventDecodeNumber( event )
	local isLocal = KEP_EventDecodeNumber( event )
	local isDerivable = KEP_EventDecodeNumber( event )

	KEP_Log("DynamicObjects::dynamicObjectSelectedHandler(): placementId="..placementId )

    if rightClick ~= 0 then
        if g_isInteractive ~= 0 then
        	if MenuIsClosed("FlashGameOverview.xml") then
				MenuOpen("FlashGameOverview.xml")
				menuOpenEvent = KEP_EventCreate( FLASHGAME_MENU_OPEN_EVENT )
				KEP_EventEncodeNumber( menuOpenEvent, placementId )
				if KEP_IsOwnerOrModerator() then
					KEP_EventEncodeNumber( menuOpenEvent, 1 )
    		  	else
					KEP_EventEncodeNumber( menuOpenEvent, 0 )
    		  	end
				KEP_EventEncodeNumber( menuOpenEvent, friendId )
				KEP_EventEncodeNumber( menuOpenEvent, x )
				KEP_EventEncodeNumber( menuOpenEvent, y )
				KEP_EventEncodeNumber( menuOpenEvent, isAttachableObject )
				KEP_EventEncodeString( menuOpenEvent, name )
				KEP_EventEncodeNumber( menuOpenEvent, canPlayMovie )
				KEP_EventEncodeNumber( menuOpenEvent, g_isInteractive )
				KEP_EventEncodeString( menuOpenEvent, description )
				KEP_EventEncodeNumber( menuOpenEvent, rightClick )
				KEP_EventEncodeNumber( menuOpenEvent, leftClick )
				KEP_EventQueue( menuOpenEvent )
			end
		else
			if KEP_IsOwnerOrModerator() then
       			openDynamicObjectMenu(placementId, true)
	        else
       			openDynamicObjectMenu(placementId, false)
	        end
			KEP_ReplaceSelection( type, id)
	        if menuXml ~= menuXmlWas then
	            sendCloseEvent( menuXmlWas )
	        end
		end
    end
	return KEP.EPR_OK
end

function sendCloseEvent( menuXml )
	local ev = KEP_EventCreate( CLOSE_DYNMENU_EVENT )
	KEP_EventEncodeNumber( ev, MenuHandle(menuXml) or 0 )
	KEP_EventQueue( ev )
end

function closeFlashGameHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	showDynObjMenu = KEP_EventDecodeNumber( event )
	if showDynObjMenu ~= 0 then
		local placementId  = KEP_EventDecodeNumber( event )
		local friendId = KEP_EventDecodeNumber( event )
		local x  = KEP_EventDecodeNumber( event )
		local y  = KEP_EventDecodeNumber( event )
		local isAttachableObject = KEP_EventDecodeNumber( event )
		local name = KEP_EventDecodeString( event )
		local canPlayMovie = KEP_EventDecodeNumber( event )
		g_isInteractive = KEP_EventDecodeNumber( event )
		local description = KEP_EventDecodeString( event )
		local rightClick = KEP_EventDecodeNumber( event )
		local leftClick = KEP_EventDecodeNumber( event )

		menuXmlWas = menuXml
		menuXml = nil

		if KEP_IsOwnerOrModerator() then
	        menuXml = loadMenu( 1, menuXml, placementId, friendId, x, y, isAttachableObject, name, canPlayMovie, description, g_isInteractive, 0)	-- do we need isLocal flag here?
	    else
	        menuXml = loadMenu( 0, menuXml, placementId, friendId, x, y, isAttachableObject, name, canPlayMovie, description, g_isInteractive, 0)
	    end

	    if menuXml ~= menuXmlWas then
	        sendCloseEvent( menuXmlWas )
	    end
	end
	return KEP.EPR_OK
end

function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "dynamicObjectSelectedHandler", "DynamicObjectSelectedEvent", KEP.HIGH_PRIO )
	KEP_EventRegisterHandler( "closeFlashGameHandler", FLASHGAME_MENU_CLOSE_EVENT, KEP.MED_PRIO )
end

function InitializeKEPEvents(dispatcher, handler, debugLevel)
	KEP_EventRegister( DYNAMIC_OBJ_MENU_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( PICTURE_FRAME_MENU_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( CLOSE_DYNMENU_EVENT, KEP.HIGH_PRIO )
	KEP_EventRegister( FLASHGAME_MENU_CLOSE_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( FLASHGAME_MENU_OPEN_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( INTERACTIVE_HUD_TEXT_EVENT, KEP.MED_PRIO )
	KEP_EventRegister( DYNAMIC_OBJECT_INFO_EVENT, KEP.MED_PRIO )
    KEP_EventRegister( "DynamicObjectSelectedEvent", KEP.MED_PRIO )
end
