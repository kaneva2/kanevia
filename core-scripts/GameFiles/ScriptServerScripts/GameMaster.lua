--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

function kgp_arrive(user)
	g_id = kgp.objectgetid()
	kgp.objectclearlabels(g_id)
	g_labels = {{"Press TAB key to interact.", 0.015}}
	kgp.objectaddlabels(g_id, g_labels)
end