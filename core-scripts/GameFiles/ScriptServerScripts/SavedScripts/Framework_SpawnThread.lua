--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Framework_SpawnThread.lua
-- Creates a VM thread for use by the Spawn Controller
Class.importClass("Class_SpawnThread")

function kgp_start(user)
	s_instance = Class_SpawnThread.new()
end

function kgp_stop(user)	
	if(s_instance  and type(s_instance.stopThread) == "function" ) then
		s_instance:stopThread()
	end
end
