--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Copied over from ...\Core\Deploy\GameContent\templates\Shared\GameFiles\Scripts\EnvironmentIds.lua
EnvironmentIds                     = {}
EnvironmentIds.AMBIENTLIGHTREDDAY                        =      0  -- RGB color
EnvironmentIds.AMBIENTLIGHTON                            =      1  -- gsBOOL
EnvironmentIds.SUNRED                                    =      2  -- RGB color
EnvironmentIds.SUNLIGHTON                                =      3  -- gsBOOL
EnvironmentIds.SUNPOSITION                               =      4  -- x,y,x coords
EnvironmentIds.FOGRED                                    =      5  -- RGB color
EnvironmentIds.FOGON                                     =      6  -- gsBOOL
EnvironmentIds.BACKRED                                   =      7  -- RGB color
EnvironmentIds.LOCKBACKCOLOR                             =      8  -- gsBool
EnvironmentIds.FOGSTART                                  =      9  -- gsFloat
EnvironmentIds.FOGRANGE                                  =     10  -- gsFloat
EnvironmentIds.ENABLETIMESYSTEM                          =     11  -- gsBOOL
EnvironmentIds.ENVIRONMENTTIMESYSTEM                     =     12  -- object
EnvironmentIds.GRAVITY                                   =     13  -- gsFloat
EnvironmentIds.SEALEVEL                                  =     14  -- gsFloat
EnvironmentIds.SEAVISCUSDRAG                             =     15  -- gsFloat
