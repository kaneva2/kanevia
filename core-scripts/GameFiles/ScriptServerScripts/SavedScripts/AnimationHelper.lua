--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

Subject = {}
Subject.__index = Subject

function Subject.create(subjectType, id)
	local subject = {}             -- our new object
	setmetatable(subject,Subject)
	
	subject.type = subjectType
	subject.id = id
	subject.queue = {}
	
	return subject
end

Animation = {}
Animation.__index = Animation

function Animation.create(glid, mSecs)
	local animation = {}             -- our new object
	setmetatable(animation,Animation)
	
	animation.glid = glid
	animation.duration = mSecs
	animation.timeLeft = mSecs

	return animation
end

--CONSTANTS
PLAYER_TYPE = 0
OBJECT_TYPE = 1

--GLOBALS
gSubjects = {}

--APIS 
local gSecond = 0
local gTotal = 0
local prevTick = 0
function updateAnimations( user,tick )
	--tellPlayer(user, "user " .. tick)
	if (prevTick == 0) then
		prevTick = tick
	end	
	local diff = tick - prevTick	
	prevTick = tick
	gSecond = gSecond + diff
	
	--if (gSecond > 1000) then --1 second has passed'		
		--gTotal = gTotal + 1
		--tellPlayer(user, gTotal .. " seconds have passed")
		gSecond = 0
		--decrement all animations on all subjects
		for i,v in ipairs(gSubjects) do
			--decrement the first animation on each subject, if it has reached 0 then remove it
			--and start the next one
			local animation = v.queue[1]
			if (animation ~= nil) then
				--animation.timeLeft = animation.timeLeft - 1
				animation.timeLeft = animation.timeLeft - diff
				--tellPlayer(user, "timeLeft " .. animation.timeLeft .. " diff " .. diff)
				if (animation.timeLeft <= 0) then
					--tellPlayer(user, "time for next animation")
					--remove this animation and play the next one
					table.remove(v.queue, 1)
					playAnimation(v)
				end
			else
				--this subject has no animations left, we could remove them here, but there is no real need to				
			end
			
		end
	--end

end

function playerQueueAnimation( userId, glid, mSecs )
	--check to see if there is a subject already defined for this userId
	local present, subject = isSubjectCreated( PLAYER_TYPE, userId )
	if (present == false) then		
		--create a new subject and start playing the animation immediately		
		subject = Subject.create(PLAYER_TYPE, userId)
		table.insert(gSubjects, subject)		
		--tellPlayer(userId, "creating anew subject")
	end
	--insert animation into subjects queue
	insertAnimation(subject, glid, mSecs)
	
end

function playerClearAnimation( userId )
	local present, subject = isSubjectCreated( PLAYER_TYPE, userId )
	if (subject ~= nil) then
		subject.queue = nil
		subject.queue = {}
	end
end

function objectQueueAnimation( pId, glid, mSecs)
	--tellPlayer(gUser, "pid " .. tostring(pId) .. " glid " .. glid .. " num " .. mSecs)
	--check to see if there is a subject already defined for this userId
	local present, subject = isSubjectCreated( OBJECT_TYPE, pId )
	--tellPlayer(gUser, "present " .. tostring(present) .. " subject " .. tostring(subject))	
	if (present == false) then		
		--create a new subject and start playing the animation immediately
		subject = Subject.create(OBJECT_TYPE, pId)
		table.insert(gSubjects, subject)
	end
	insertAnimation(subject, glid, mSecs)
end

function objectClearAnimation( pId )
	local present, subject = isSubjectCreated( OBJECT_TYPE, pId )
	if (subject ~= nil) then
		subject.queue = nil
		subject.queue = {}
	end
end

--HELPER FUNCTIONS

function tellPlayer(user, msg)
	kgp.playertell(user, msg, "", 1)
end

function insertAnimation( subject, glid, mSecs )
	local playNow = false
	--kgp.gamelogmessage("subject " .. tostring(subject))
	--tellPlayer(gUser, "subject " .. tostring(subject))
	--check for an empty queue	
	if (#subject.queue == 0) then
		playNow = true
	end
	
	local animation = Animation.create( glid, mSecs )
	table.insert(subject.queue, animation)
	
	if (playNow) then
		--tellPlayer(subject.id, "Playing now")
		playAnimation(subject)
	end

end

function playAnimation( subject )
	--play the top level animation
	local animation = subject.queue[1]
	if (animation ~= nil) then
		if (subject.type == PLAYER_TYPE) then
			--tellPlayer(subject.id, "Playing animation " .. animation.glid)
			kgp.playersetanimation( subject.id, animation.glid )
		elseif (subject.type == OBJECT_TYPE) then
			kgp.objectsetanimation( subject.id, animation.glid )
		end
	end

end

function isSubjectCreated( subjectType, id )
	for i,v in ipairs(gSubjects) do
		if (v.type == subjectType and v.id == id) then
			return true, v
		end
	end
	return false, 0
end

-- This function sets the animation of the user
function setAnimation(user, animation)
	if user == nil then -- Make sure a user is passed in
		--Don't do anything! But this prevents crashing.
		return
	end
	
	if animation == nil then -- Make sure an animation is passed in
		kgp.playertell(user, "invalid parameters specified", "AnimationHelper.lua", 1)
		return
	end
	
	local gender = kgp.playergetgender(user) -- Get the player's gender
	
	if animation[gender] == nil then -- Animation does not exist!
		kgp.playertell(user, "Animation does not exist for this gender!", "AnimationHelper.lua", 1)
		return
	end
	
	local gender = kgp.playergetgender(user) -- Get the player's gender
	kgp.playersetanimation(user, animation[gender]) -- Set the animation
end

---------------------------------------------------------------------------
--Animation Tables: Reference for all of Kaneva's Animations
---------------------------------------------------------------------------
ANIM_STAND = {F=4980, M=5170}
ANIM_RUN = {F=4983, M=5400}
ANIM_WALK = {F=5345, M=5470}
ANIM_DANCE4 = {F=5360, M=5416}
ANIM_JUMP = {F=5326, M=5471}
ANIM_DANCE5 = {F=5361, M=5417}
ANIM_DANCE6 = {F=5344, M=5441}
ANIM_WAVE = {F=5456, M=5436}
ANIM_LAUGH = {F=5501, M=5468}
ANIM_SIT = {F=5346, M=5642}
ANIM_CROUCH = {F=5055, M=5473}
ANIM_STAND2 = {F=nil, M=5060}
ANIM_LAY_DOWN = {F=5350, M=5472}
ANIM_YES = {F=5302, M=5434}
ANIM_NO = {F=5455, M=5435}
ANIM_POINT = {F=5457, M=5645}
ANIM_CLAP = {F=5650, M=5646}
ANIM_CHEER = {F=5459, M=5437}
ANIM_CRY = {F=5500, M=5438}
ANIM_FLIRT = {F=5461, M=5440}
ANIM_FLIRT2 = {F=5462, M=nil}
ANIM_INSULT = {F=5463, M=5341}
ANIM_INSULT2 = {F=5467, M=5342}
ANIM_INSULT3 = {F=5464, M=5343}
ANIM_BLOW_KISS = {F=5465, M=5415}
ANIM_ZOMBIE_IDLE = {F=5529, M=5530}
ANIM_SPRINKLER = {F=5618, M=5626}
ANIM_LAWNMOWER = {F=5617, M=5625}
ANIM_ROBOT = {F=5616, M=5624}
ANIM_FLEX_MUSCLE = {F=5619, M=5648}
ANIM_CHEST_POUND = {F=5614, M=5647}
ANIM_SCREAM = {F=5613, M=5649}
ANIM_GIVE_UP = {F=5612, M=5644}
ANIM_COUNTRY = {F=5362, M=5433}
ANIM_DISCO = {F=5363, M=5420}
ANIM_TECHNO = {F=5364, M=5421}
ANIM_SEXY = {F=5365, M=5499}
ANIM_SEXY2 = {F=5366, M=5117}
ANIM_LATIN = {F=5483, M=5498}
ANIM_LATIN2 = {F=5482, M=5497}
ANIM_BREAK = {F=5369, M=5496}
ANIM_BREAK2 = {F=5370, M=5495}
ANIM_COUNTRY2 = {F=5389, M=5442}
ANIM_POP = {F=5481, M=nil}
ANIM_POP2 = {F=5480, M=5494}
ANIM_POP3 = {F=5479, M=nil}
ANIM_POP4 = {F=5478, M=5493}
ANIM_DISCO2 = {F=5477, M=5443}
ANIM_HIP_HOP = {F=5476, M=5444}
ANIM_DANCE34 = {F=5475, M=nil}
ANIM_HIP_HOP2 = {F=5378, M=5445}
ANIM_DANCE35 = {F=5390, M=nil}
ANIM_DANCE36 = {F=5391, M=nil}
ANIM_DANCE37 = {F=5392, M=nil}
ANIM_POSE = {F=5393, M=nil}
ANIM_POSE2 = {F=5058, M=nil}
ANIM_INJURED = {F=5529, M=5530}
ANIM_HANDSHAKE = {F=5607, M=5596}
ANIM_HIGH_FIVE = {F=5611, M=5597}
ANIM_HUG = {F=5610, M=5714}
ANIM_KISS = {F=5609, M=5599}
ANIM_MAKE_OUT = {F=5595, M=5600}
ANIM_CHAT = {F=5629, M=5637}
ANIM_CHAT2 = {F=5635, M=5643}
ANIM_CHAT3 = {F=5630, M=5638}
ANIM_SLOW_DANCE = {F=5631, M=5639}
ANIM_SLOW_DANCE2 = {F=5632, M=5640}
ANIM_SIT2 = {F=5633, M=5641}
ANIM_SIT3 = {F=5634, M=5642}
ANIM_BACKFLIP = {F=5715, M=5716}
ANIM_ARM_AROUND_WAIST = {F=5718, M=5720}
ANIM_ARM_AROUND_SHOULDER = {F=5723, M=5722}
ANIM_KACHING_STAND = {F=5509, M=5168}
ANIM_KACHING_RUN = {F=5512, M=5163}
ANIM_KACHING_WALK = {F=5513, M=5162}
ANIM_KACHING_JUMP = {F=5511, M=5402}
ANIM_KACHING_STRAFE_LEFT = {F=5406, M=5164}
ANIM_KACHING_STRAFE_RIGHT = {F=5407, M=5165}
ANIM_KACHING_DIE = {F=5408, M=5166}
ANIM_KACHING_FIRE = {F=5510, M=5159}
ANIM_KACHING_CROUCH = {F=5508, M=5401}
ANIM_KACHING_FIRE_LOW = {F=5196, M=5507}
ANIM_KACHING_RELOAD = {F=5282, M=5161}
ANIM_KACHING_CROUCH_FIRE = {F=5196, M=5507}
ANIM_KACHING_CROUCH_WALK = {F=5196, M=5507}
ANIM_KACHING_CROUCH_LEFT = {F=5196, M=5507}
ANIM_KACHING_CROUCH_RIGHT = {F=5196, M=5507}
ANIM_KACHING_CROUCH_DEATH = {F=5588, M=5587}
ANIM_WALL_LEAN = {F=4981, M=5061}
ANIM_WALL_LEAN2 = {F=5006, M=5085}
ANIM_DANCE = {F=5008, M=5087}
ANIM_DANCE2 = {F=5009, M=5088}
ANIM_DANCE3 = {F=5010, M=5089}
ANIM_DANCE7 = {F=5011, M=5090}
ANIM_DANCE8 = {F=5012, M=5091}
ANIM_DANCE9 = {F=5013, M=5092}
ANIM_DANCE10 = {F=5014, M=5093}
ANIM_DANCE11 = {F=5015, M=5094}
ANIM_DANCE12 = {F=5016, M=5095}
ANIM_DANCE13 = {F=5017, M=5096}
ANIM_DANCE14 = {F=5018, M=5097}
ANIM_DANCE15 = {F=5019, M=5098}
ANIM_DANCE16 = {F=5020, M=5099}
ANIM_DANCE17 = {F=5021, M=5100}
ANIM_DANCE18 = {F=5022, M=5101}
ANIM_DANCE19 = {F=5023, M=5102}
ANIM_DANCE20 = {F=5024, M=5103}
ANIM_DANCE21 = {F=5025, M=5104}
ANIM_DANCE22 = {F=5026, M=5105}
ANIM_DANCE23 = {F=5027, M=5106}
ANIM_DANCE24 = {F=5028, M=5107}
ANIM_DANCE25 = {F=5029, M=5108}
ANIM_DANCE26 = {F=5030, M=5109}
ANIM_STUMBLE = {F=5031, M=5110}
ANIM_STUMBLE2 = {F=5032, M=5111}
ANIM_VICTORY = {F=5033, M=5112}
ANIM_PERFECT = {F=5034, M=5113}
ANIM_DANCE27 = {F=5035, M=5114}
ANIM_DANCE28 = {F=5036, M=5115}
ANIM_DANCE29 = {F=5037, M=5116}
ANIM_DANCE30 = {F=nil, M=5128}
ANIM_DANCE31 = {F=nil, M=5129}
ANIM_DANCE32 = {F=nil, M=5130}
ANIM_DANCE33 = {F=nil, M=5131}
ANIM_BACKFLIP2 = {F=nil, M=5134}