--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Effect Codes using in kgp.objectSetEffectTrack and kgp.playerSetEffectTrack
EFFECT_CODES = {}
EFFECT_CODES.ANIMATION  = 0
EFFECT_CODES.SOUND      = 1
EFFECT_CODES.PARTICLE   = 2
EFFECT_CODES.SOUND_OBJ  = 3
EFFECT_CODES.MOVE		= 4
EFFECT_CODES.ROTATE		= 5

--SPECIAL MENU EVENTS
MENU_EVENTS = {}
MENU_EVENTS.LOBBY_NPC = 2009
MENU_EVENTS.PLAYER_RCLICK = 1000

--LUA TYPE DEFINES
LUA_TYPES = {}
LUA_TYPES.NIL           = 0
LUA_TYPES.BOOLEAN       = 1
LUA_TYPES.LIGHTUSERDATA	= 2
LUA_TYPES.NUMBER		   = 3
LUA_TYPES.STRING		   = 4
LUA_TYPES.TABLE         = 5
LUA_TYPES.FUNCTION      = 6
LUA_TYPES.USERDATA      = 7
LUA_TYPES.THREAD        = 8

-- MLB - Replaced by ObjectType definition, 
-- 		 one stop shopping for all. Intent
--		 is for Game script and Platform C++ code
--		 to use the same base definition.
--
-- Object types returned from playerGetObjectInfront()
--OBJTYPE = {}
--OBJTYPE.NONE	= 0
--OBJTYPE.WORLD	= 1
--OBJTYPE.DYNAMIC	= 2
--OBJTYPE.PLAYER	= 3
--OBJTYPE.NPC	    = 4
--OBJTYPE.SOUND	= 5
--OBJTYPE.EQUIPPABLE = 6

-- Types of account permissions a user has returned from kgp.getPlayerPermissions()
PERMISSIONS = {}
PERMISSIONS.GM = 0
PERMISSIONS.OWNER = 1
PERMISSIONS.MODERATOR = 2
PERMISSIONS.MEMBER = 3