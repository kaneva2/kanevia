--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Script Test Container
-- Created: YFC 03/2015

kgp.scriptDoFile("Lib_Test.lua")

function kgp_start( user, arg, caller )
	if TestContainer.checkTestCaseMessage(arg) then
		-- Parse and run test case
		TestContainer.processTestCase(caller, arg)
	end
end

kgp_message = TestContainer.processMessage
