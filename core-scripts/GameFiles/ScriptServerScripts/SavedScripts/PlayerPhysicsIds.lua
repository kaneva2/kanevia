--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Created based on the generated file pPhysics.lua
pPhysics                    = {}
pPhysics.CURSPEED                                 =      0  -- gsFloat
pPhysics.AUTODECEL                                =      1  -- gsBOOL
pPhysics.GRAVITY                                  =      2  -- gsFloat
pPhysics.MAXSPEED                                 =      3  -- gsFloat
pPhysics.MINSPEED                                 =      4  -- gsFloat
pPhysics.WALKMAXSPEED                             =      5  -- gsFloat
pPhysics.WALKMINSPEED                             =      6  -- gsFloat
pPhysics.ACCELLERATE                              =      7  -- gsFloat
pPhysics.DECELLERATE                              =      8  -- gsFloat
pPhysics.PITCHACCEL                               =      9  -- gsFloat
pPhysics.PITCHDECEL                               =     10  -- gsFloat
pPhysics.YAWACCEL                                 =     11  -- gsFloat
pPhysics.YAWDECEL                                 =     12  -- gsFloat
pPhysics.CURROTX                                  =     13  -- gsFloat
pPhysics.CURROTY                                  =     14  -- gsFloat
pPhysics.CURROTZ                                  =     15  -- gsFloat
pPhysics.CURTRANSX                                =     16  -- gsFloat
pPhysics.CURTRANSY                                =     17  -- gsFloat
pPhysics.CURTRANSZ                                =     18  -- gsFloat
pPhysics.ROLLFACTOR                               =     19  -- gsFloat
pPhysics.MINYAW                                   =     20  -- gsFloat
pPhysics.MAXYAW                                   =     21  -- gsFloat
pPhysics.MINPITCH                                 =     22  -- gsFloat
pPhysics.MAXPITCH                                 =     23  -- gsFloat
pPhysics.MAXROLL                                  =     24  -- gsFloat
pPhysics.ROLLACCEL                                =     25  -- gsFloat
pPhysics.ROLLDECEL                                =     26  -- gsFloat
pPhysics.STANDVERTICALTOSURFACE                   =     27  -- gsBOOL
pPhysics.BURSTFORCEY                              =     28  -- gsFloat
pPhysics.ROTATIONDRAGCONSTANT                     =     29  -- gsFloat
pPhysics.ROTATIONALGROUNDDRAG                     =     30  -- gsFloat
pPhysics.ANTIGRAVITY                              =     31  -- gsFloat
pPhysics.DOGRAVITY                                =     32  -- gsBOOL
pPhysics.JUMPPOWER                                =     33  -- gsFloat
pPhysics.FREELOOKSPEED                            =     34  -- gsFloat
pPhysics.FREELOOKCURMAXPITCH                      =     35  -- gsFloat
pPhysics.FREELOOKCURMAXYAW                        =     36  -- gsFloat
pPhysics.INVERTMOUSE                              =     37  -- gsBOOL
pPhysics.MASS                                     =     38  -- gsFloat
pPhysics.CURROTATIONVECT                          =     39  -- x,y,x coords
pPhysics.CURVELOCITYVECT                          =     40  -- x,y,x coords
pPhysics.LASTPOSITION                             =     41  -- x,y,x coords
pPhysics.GROUNDDRAG                               =     42  -- gsFloat
pPhysics.AUTODECELY                               =     43  -- gsBOOL
pPhysics.AUTODECELZ                               =     44  -- gsBOOL
pPhysics.CAPABILITYFOVMAX                         =     45  -- gsFloat
pPhysics.CAPABILITYFOVMIN                         =     46  -- gsFloat
pPhysics.CAPFOVCHANGESENSITIVITY                  =     47  -- gsFloat
pPhysics.FREELOOKYAWFORCE                         =     48  -- gsFloat
pPhysics.STRAFEREDUCTION                          =     49  -- gsFloat
pPhysics.CROSSSECTIONALAREA                       =     50  -- gsFloat
pPhysics.AIRCONTROL                               =     51  -- gsFloat
pPhysics.JUMPRECOVERTIME                          =     52  -- gsDouble
pPhysics.BISWALKING                               =     53  -- gsBool
pPhysics.COLLISIONENABLED                         =     54  -- gsBool
