--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
--  GoogleWebHelper.lua - provides helper functions for retrieving Google Website Optimizer
--						  experiments
-------------------------------------------------------------------------------------------

--------------------------------
-- Globals
--------------------------------
gGoogleLoginTemplateURL = "https://www.google.com/accounts/ClientLogin?accountType=HOSTED_OR_GOOGLE&Email=#EMAIL#&Passwd=#PASSWORD#&service=analytics&source=GoogleWebHelper"
gGoogleExperimentsURL = "https://www.google.com/analytics/feeds/websiteoptimizer/experiments"
gGoogleExperimentDetailTemplateURL = "https://www.google.com/analytics/feeds/websiteoptimizer/experiments/#EXPERIMENTNUMBER#"

gGoogleAuthHeaderIdentifier = "GoogleLogin auth="

gAuthHeader = nil
gExperiments = {}

--------------------------------
-- Class Definitions
--------------------------------
Experiment = {}
Experiment.__index = Experiment

function Experiment.create()
	local experiment = {}
	setmetatable(experiment,Experiment)
	return experiment
end

function Experiment:setValue(name, value)
	if (value == nil) then
		self[name] = ""
	else
		self[name] = value
	end
end


--------------------------------
-- API Functions
--------------------------------

--  setupGoogleWebHelper()
function setupGoogleWebHelper( emailAddress, password )
	-- set up login URL with the parameters
	local loginURL = gGoogleLoginTemplateURL
	loginURL = string.gsub(loginURL, "#EMAIL#", emailAddress)
	loginURL = string.gsub(loginURL, "#PASSWORD#", password)
	
	local returnCode, returnDescription, resultXML = kgp.scriptmakewebcall( loginURL )
	if ( returnCode == 200 ) then
		return setAuthHeader( resultXML )
	else
		kgp.gamelogmessage("GoogleWebHelper.lua - setupGoogleWebHelper() failed: loginURL call returned " .. returnCode .. " " .. returnDescription) 
		return false
	end
end

--  getGoogleExperiments() 
function getGoogleExperiments()
	if (gAuthHeader ~= nil) then
		local returnCode, returnDescription, resultXML = kgp.scriptmakewebcall( gGoogleExperimentsURL, false, gAuthHeader )
		if ( returnCode == 200 ) then
			return setExperiments( resultXML )
		else
			kgp.gamelogmessage("GoogleWebHelper.lua - getGoogleExperiments() failed: gGoogleExperimentsURL call returned " .. returnCode .. " " .. returnDescription) 
			return false
		end
	else
		kgp.gamelogmessage("GoogleWebHelper.lua - getGoogleExperiments() failed: Auth Header not setup") 
		return false
	end
end

--  getGoogleExperimentDetail() 
function getGoogleExperimentDetail( experimentNumber )
	-- set up login URL with the parameters
	local experimentURL = gGoogleExperimentDetailTemplateURL
	experimentURL = string.gsub(experimentURL, "#EXPERIMENTNUMBER#", experimentNumber)
	
	if (gAuthHeader ~= nil) then
		local returnCode, returnDescription, resultXML = kgp.scriptmakewebcall( experimentURL, false, gAuthHeader )
		if ( returnCode == 200 ) then
			kgp.gamelogmessage("GoogleWebHelper.lua - getGoogleExperimentDetail(): " .. resultXML )
		else
			kgp.gamelogmessage("GoogleWebHelper.lua - getGoogleExperimentDetail() failed: gGoogleExperimentDetailTemplateURL call returned " .. returnCode .. " " .. returnDescription) 
			return false
		end
	else
		kgp.gamelogmessage("GoogleWebHelper.lua - getGoogleExperimentDetail() failed: Auth Header not setup") 
		return false
	end
end

function buildTrackingURL( experimentKey, accountId, userId, experimentId, variantNumber, pageViewType, gwoInternalCode )

	local url = "http://www.google-analytics.com/__utm.gif?"
	url = url .. "utmwv=4.8.6"
	url = url .. "&utmn=140855261"
	url = url .. "&utmhn=3d-wok.kaneva.com"
	url = url .. "&utmcs=ISO-8859-1"
	url = url .. "&utmsr=1440x900"
	url = url .. "&utmsc=32-bit"
	url = url .. "&utmul=en-us"
	url = url .. "&utmje=1"
	url = url .. "&utmfl=10.1%20r103"
	url = url .. "&utmhid=1539122367"
	url = url .. "&utmr=-"
	url = url .. "&utmp=%2F" .. experimentKey .. "%2F" .. pageViewType
	url = url .. "&utmac=" .. accountId
	url = url .. "&utmcc="
	url = url .. "__utma%3D47393701." .. userId .. ".1294777279.1294777279.1295021491.1%3B%2B"
	url = url .. "__utmz%3D47393701.1294777279.1.1."
	url = url .. "utmcsr%3D(direct)%7C"
	url = url .. "utmccn%3D(direct)%7C"
	url = url .. "utmcmd%3D(none)%3B%2B"										
	url = url .. "__utmx%3D47393701." .. experimentId .. experimentKey .. "%3A" .. gwoInternalCode .. "%3A" .. variantNumber .. "%3B"
	url = url .. "&utmu=qAAg"

	return url
end

function trackConversionView( experimentKey, accountId, userId, experimentId, variantNumber, gwoInternalCode )
	
	local url = buildTrackingURL( experimentKey, accountId, userId, experimentId, variantNumber, "goal", gwoInternalCode )
	
	local returnCode, returnDescription, resultXML = kgp.scriptmakewebcall( url, false )
	if ( returnCode == 200 ) then
		kgp.gamelogmessage("GoogleWebHelper.lua - trackConversionView(): " .. resultXML )
		return true
	else
		kgp.gamelogmessage("GoogleWebHelper.lua - trackConversionView() failed: url call returned " .. returnCode .. " " .. returnDescription) 
		return false
	end
	
end

function trackVariantView( experimentKey, accountId, userId, experimentId, variantNumber, gwoInternalCode )
	
	local url = buildTrackingURL( experimentKey, accountId, userId, experimentId, variantNumber, "test", gwoInternalCode )
	
	local returnCode, returnDescription, resultXML = kgp.scriptmakewebcall( url, false )
	if ( returnCode == 200 ) then
		kgp.gamelogmessage("GoogleWebHelper.lua - trackVariantView(): " .. resultXML )
		return true
	else
		kgp.gamelogmessage("GoogleWebHelper.lua - trackVariantView() failed: url call returned " .. returnCode .. " " .. returnDescription) 
		return false
	end
	
end

--------------------------------
-- Helper Functions
--------------------------------
function setAuthHeader( loginReturnXML )
	local startPos, endPos, authToken = string.find(loginReturnXML, ".+Auth=(.+)\n")
	if ( authToken ~= nil ) then
		local authHeader = gGoogleAuthHeaderIdentifier .. authToken
		gAuthHeader = {"Authorization", authHeader}
		return true
	else
		kgp.gamelogmessage("GoogleWebHelper.lua - setAuthHeader() failed: no Auth token found in ClientLogin response") 
		return false
	end
end

function setExperiments( expReturnXML )
	local workingXML = expReturnXML
	local startPos, endPos, entry = string.find( workingXML, "<entry>(.-)</entry>" )
	while ( entry ~= nil ) do
		local s, e, experimentId = string.find( workingXML, "<gwo:experimentId>(.-)</gwo:experimentId>" )	
		s, e, status = string.find( workingXML, "<gwo:status>(.-)</gwo:status>" )	
		s, e, numCombinations = string.find( workingXML, "<gwo:numCombinations>(.-)</gwo:numCombinations>" )	
		s, e, id = string.find( workingXML, "<id>(.-)</id>" )	
		s, e, updated = string.find( workingXML, "<updated>(.-)</updated>" )	
		s, e, title = string.find( workingXML, "<title .->(.-)</title>" )	
		s, e, analyticsAccountId = string.find( workingXML, "<gwo:analyticsAccountId>(.-)</gwo:analyticsAccountId>" )	
		s, e, experimentType = string.find( workingXML, "<gwo:experimentType>(.-)</gwo:experimentType>" )	
		
		-- conditional attributes
		local numAbPageVariations = ""
		local numSections = ""
		if ( experimentType == "AB" ) then
			s, e, numAbPageVariations = string.find( workingXML, "<gwo:numAbPageVariations>(.-)</gwo:numAbPageVariations>" )	
		elseif ( experimentType == "Multivariate" ) then
			s, e, numSections = string.find( workingXML, "<gwo:numSections>(.-)</gwo:numSections>" )	
		end
		
		local experiment = Experiment.create()
		experiment:setValue("experimentId", experimentId)
		experiment:setValue("status", status)
		experiment:setValue("numCombinations", numCombinations)
		experiment:setValue("id", id)
		experiment:setValue("updated", updated)
		experiment:setValue("title", title)
		experiment:setValue("analyticsAccountId", analyticsAccountId)
		experiment:setValue("experimentType", experimentType)
		experiment:setValue("numAbPageVariations", numAbPageVariations)
		experiment:setValue("numSections", numSections)
		table.insert( gExperiments, experiment )
		
		workingXML = string.sub( workingXML, endPos )
		startPos, endPos, entry = string.find( workingXML, "<entry>(.-)</entry>" )
	end
	 
	return true
end

function logExperiments()
	for i,v in ipairs(gExperiments) do
		kgp.gamelogmessage( "----------------------------------------" ) 
		kgp.gamelogmessage( "v[experimentId] \t" .. v["experimentId"] ) 
		kgp.gamelogmessage( "v[status] \t\t" .. v["status"] ) 
		kgp.gamelogmessage( "v[numCombinations] \t" .. v["numCombinations"] ) 
		kgp.gamelogmessage( "v[id] \t\t\t" .. v["id"] ) 
		kgp.gamelogmessage( "v[updated] \t\t" .. v["updated"] ) 
		kgp.gamelogmessage( "v[title] \t\t" .. v["title"] ) 
		kgp.gamelogmessage( "v[analyticsAccountId] \t" .. v["analyticsAccountId"] ) 
		kgp.gamelogmessage( "v[experimentType] \t" .. v["experimentType"] ) 
		kgp.gamelogmessage( "v[numAbPageVariations] " .. v["numAbPageVariations"] ) 
		kgp.gamelogmessage( "v[numSections] \t" .. v["numSections"] ) 
	end
end






