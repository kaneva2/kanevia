--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------


function roundVectorToNearestAngle(x, z, angle) 
	local numAngles = (2 * math.pi) / angle
	local finalX = 0
	local finalZ = 0
	local finalAngleBetween = 0
	for i = 0, numAngles do
		--test angle
		local tempX = math.cos(i * angle)
		local tempZ = math.sin(i * angle)
		local angleBetween = dotProduct(x, 0, z, tempX, 0, tempZ)
		if angleBetween > finalAngleBetween then
			finalAngleBetween = angleBetween
			finalX = tempX
			finalZ = tempZ
		end
	end
	return finalX, finalZ
end

function getCentroid(objects)
	local x = 0
	local y = 0
	local z = 0
	local numObjects = 0
	for i,v in ipairs(objects) do
		if v.x ~= nil and v.y ~= nil and v.z ~= nil then
			x = x + v.x
			y = y + v.y
			z = z + v.z
			numObjects = numObjects + 1
		end
	end
	if numObjects > 0 then
		x = x / numObjects
		y = y / numObjects
		z = z / numObjects
	end
	return x,y,z
end

function degreesToRadians(degrees)
	local rads = math.rad(degrees)
	return rads
end

function radiansToDegrees(radians)
	local deg = math.deg(radians)
	return deg
end

function round(num, idp)
	return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

function normalizeVector(x, y, z)
	local magnitude = math.sqrt(x^2 + y^2 + z^2)
	local epsilon = 0.00001
	if (magnitude > epsilon) then
		return x / magnitude, y / magnitude, z / magnitude
	else
		return 0, 0, 0
	end
end

function dotProduct(x1, y1, z1, x2, y2, z2)
	return x1 * x2 + y1 * y2 + z1 * z2
end