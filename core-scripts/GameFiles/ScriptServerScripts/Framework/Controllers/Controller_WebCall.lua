--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Controller_WebCall.lua
---------------------------------------------------------------------
-- Controller Web Call - Handles web calls.
---------------------------------------------------------------------
Class.createClass("Controller_WebCall", "Controller")

-- Controller Exported Functions
Class.export({
	-- inherited from Controller
	{ "config", "key", "value" }
})

---------------------------------------------------------------------
-- References to Libraries
---------------------------------------------------------------------
local Toolbox		= _G.Toolbox
local Environment	= _G.Environment
local Events		= _G.Events

---------------------------------------------------------------------
-- Local Function Definitions
---------------------------------------------------------------------
local validateWebCall --(CID, input, callType)
local formatWebCall --(input)
local formatTableParams --(tableParams)
local determineEnvironmentPrefix --()

---------------------------------------------------------------------
-- Local Constant Definitions
---------------------------------------------------------------------
local ENVIRONMENT_PREFIX_DEV = "http://dev-wok.kaneva.com/"
local ENVIRONMENT_PREFIX_PREV = "http://pv-wok.kaneva.com/"
local ENVIRONMENT_PREFIX_PROD = "http://wok.kaneva.com/"
local ENVIRONMENT_PREFIX_RC = "http://rc-wok.kaneva.com/"
local ENVIRONMENT_TOKEN_DEV = "9802b499K8edaK407eKab73Kd2e9fceb5178"
local ENVIRONMENT_TOKEN_PREV = "f2bed117K3df6K4d9eKb361K62bd04bdb98b"
local ENVIRONMENT_TOKEN_PROD = "76321c71K3f69K4d58K999bKbd7254153b96"
local ENVIRONMENT_TOKEN_RC = "06d46735Kd633K42d9K9deeKc7e5ffe84a5f" --same as production
local WEBCALL_FOLDER = "kgp/"
local WEBCALL_SUFFIX = ".aspx"
local SERVER_FOLDER = "server/"
local TOKEN_ARG = "sToken"

local m_websitePrefix = nil
local m_securityToken = nil

local m_callIndex = 1
local m_callQueue = {}

---------------------------------------------------------------------
-- Known Web Call Validation Function Definitions
-- These must be defined above the web call types, where they are
-- linked.
---------------------------------------------------------------------
local validateAwardGemCall = function(input)
	if tostring(input.username or "") == "" or Toolbox.isContainerEmpty(input.itemAmounts) then
		return false
	end
	
	input.itemAmounts = formatTableParams(input.itemAmounts)	
	return true
end

local validateConvertItemCall = function(input)
	if tostring(input.username or "") == "" or Toolbox.isContainerEmpty(input.itemAmounts) or input.itemAmounts["World Coin"] == nil then
		return false
	end
	
	input.itemAmounts = formatTableParams(input.itemAmounts)	
	return true
end

local validateMetricBuildCall = function(input)
	if tostring(input.username or "") == "" or tonumber(input.zoneInstanceId) == nil or tonumber(input.zoneType) == nil or tonumber(input.gameItemId) == nil or
	   tostring(input.itemType or "") == "" or tostring(input.gameplayMode or "") == "" then
		return false
	end
	input.gameItemGlid = tonumber(input.gameItemGlid) or 0
	input.behavior = tostring(input.behavior) or ""
	return true
end

local validateMetricLootCall = function(input)
	if tostring(input.username or "") == "" or tonumber(input.zoneInstanceId) == nil or tonumber(input.zoneType) == nil or tonumber(input.gameItemId) == nil or
	   tostring(input.itemType or "") == "" or tostring(input.lootSource or "") == "" then
		return false
	end
	input.gameItemGlid = tonumber(input.gameItemGlid) or 0
	input.behavior = tostring(input.behavior) or ""
	return true
end

local validateMetricActivationCall = function(input)
	if tostring(input.username or "") == "" or tonumber(input.zoneInstanceId) == nil or tonumber(input.zoneType) == nil or tostring(input.activationAction or "") == "" then
		return false
	end
	return true
end

local validatePaintCall = function(input)
	if tostring(input.username or "") == "" or tonumber(input.zoneInstanceId) == nil or tonumber(input.zoneType) == nil then
		return false
	end
	return true
end

local validateMetricCall = function(input)
	if tostring(input.reportName or "") == "" or Toolbox.isContainerEmpty(input.reportParams) then
		return false
	end
	
	input.reportParams = formatTableParams(input.reportParams)	
	return true
end

---------------------------------------------------------------------
-- Web Call Types
-- All specific web calls need to be defined here
---------------------------------------------------------------------
local WEB_CALL_TYPES = { GENERIC = {input = {}},
						 AWARD_GEM = {input = {webCall = SERVER_FOLDER .. "metaGameItemsServer", action = "creditItemBalances", itemType = "Gem"}, validationFunc = validateAwardGemCall},
						 CONVERT_ITEM = {input = {webCall = SERVER_FOLDER .. "metaGameItemsServer", action = "convertToRewards", itemType = "Coin"}, validationFunc = validateConvertItemCall},
						 METRIC_BUILD = {input = {webCall = "scriptGameItemMetrics", action = "logItemPlacement"}, validationFunc = validateMetricBuildCall},
						 METRIC_LOOT = {input = {webCall = "scriptGameItemMetrics", action = "logLootAcquisition"}, validationFunc = validateMetricLootCall},
						 METRIC_PAINT = {input = {webCall = "scriptGameItemMetrics", action = "logPaintAction"}, validationFunc = validatePaintCall},
						 METRIC_ACTIVATION = {input = {webCall = "scriptGameItemMetrics", action = "logFrameworkActivation"}, validationFunc = validateMetricActivationCall},
						 GET_TOP_WORLDS = {input = {webCall = SERVER_FOLDER .. "TopWorldsTour", action = "GetTourWorld", start = "1", max = "6"}},
						 REWARD_TOP_WORLD_LOOT = {input = {webCall = SERVER_FOLDER .. "TopWorldsTour", action = "RedeemTourWorldRewards"}},
						 METRIC_GENERIC = {input = {webCall = "scriptGameItemMetrics"}, validationFunc = validateMetricCall},
						 REQUEST_EVENT = {input = {webCall = SERVER_FOLDER .. "worldEvents"}},
						 CAN_WORLD_DROP_GEMS = {input = {webCall = SERVER_FOLDER .. "worldGemChests", action = "CanWorldDropGems"}}
					   }

---------------------------------------------------------------------
-- Create - Called on controller start-up
---------------------------------------------------------------------
function create(self)
	_super.create(self)
	Events.registerHandler("FRAMEWORK_MAKE_WEB_CALL", self.onMakeWebCall, self)
	determineEnvironmentPrefix()
end

-- Event wrapper for FRAMEWORK_MAKE_WEB_CALL
function onMakeWebCall(self, event)
	makeWebCall(self, event.CID or event.PID, event.input, event.callType)
end

---------------------------------------------------------------------
-- Controller Exported Functions
---------------------------------------------------------------------
-- makeWebCall - Request the controller make a web call
---------------------------------------------------------------------
function makeWebCall(self, CID, input, callType)
	if tonumber(CID) == nil then CID = 0 end
	if input == nil or type(input) ~= "table" then input = {} end
	if WEB_CALL_TYPES[string.upper(callType or "")] == nil then callType = "GENERIC" end
	
	if validateWebCall(input, callType) then
		local address = formatWebCall(input)
		log("("..tostring(CID or 0).."): Web Call = "..tostring(address))
		m_callQueue[m_callIndex] = {CID = CID, callType = callType}
		kgp.scriptCallAsync(m_callIndex, "scriptMakeWebcall", address)
		m_callIndex = m_callIndex + 1
	elseif CID > 0 then
		Events.sendEvent("FRAMEWORK_WEB_CALL_RESPONSE", {CID = CID, callType = callType, success = false})
	end
end

---------------------------------------------------------------------
-- Controller Private Functions
---------------------------------------------------------------------
-- validateWebCall
---------------------------------------------------------------------
validateWebCall = function(input, callType)
	if m_websitePrefix == nil then --Cannot make the call without a valid prefix
		log("ERROR - Controller_WebCall - Call attempt made without valid prefix; aborting!")
		return false
	end
	for k, v in pairs(WEB_CALL_TYPES[string.upper(callType)].input) do
		input[k] = v
	end
	
	if tostring(input.webCall or "") == "" then
		return false
	end
	
	if string.match(input.webCall, SERVER_FOLDER) then
		if m_securityToken == nil then
			--Invalid environmental security token!
			return false
		end
		input[TOKEN_ARG] = m_securityToken
	end
	
	if WEB_CALL_TYPES[string.upper(callType)].validationFunc then
		return WEB_CALL_TYPES[string.upper(callType)].validationFunc(input)
	end
	
	return true
end

---------------------------------------------------------------------
-- formatWebCall
---------------------------------------------------------------------
formatWebCall = function(input)
	local address = m_websitePrefix .. input.webCall .. WEBCALL_SUFFIX
	local firstElement = true
	for k, v in pairs(input) do
		if k ~= "webCall" and (type(v) == "string" or type(v) == "number") then
			if not firstElement then
				address = address .. "&"
			else
				address = address .. "?"
			end
			address = address .. tostring(k) .. "=" .. tostring(v)
			firstElement = false
		end
	end
	
	return address
end

---------------------------------------------------------------------
-- formatTableParams
---------------------------------------------------------------------
formatTableParams = function(tableParams)
	local paramString = ""
	for k, v in pairs(tableParams or {}) do
		if paramString ~= "" then
			paramString = paramString .. "|"
		end
		paramString = paramString .. tostring(k) .. "," .. tostring(v)
	end
	
	return paramString
end


---------------------------------------------------------------------
-- determineEnvironmentPrefix
---------------------------------------------------------------------
determineEnvironmentPrefix = function()
	local gameID = kgp.gameGetCurrentGameId()
	if gameID == Environment.GAME_ID_PROD then --Production
		m_websitePrefix = ENVIRONMENT_PREFIX_PROD .. WEBCALL_FOLDER
		m_securityToken = ENVIRONMENT_TOKEN_PROD
	elseif gameID == Environment.GAME_ID_RC then -- Release Candidate (RC)
		m_websitePrefix = ENVIRONMENT_PREFIX_RC .. WEBCALL_FOLDER
		m_securityToken = ENVIRONMENT_TOKEN_RC
	elseif gameID == Environment.GAME_ID_PREV then --Preview
		m_websitePrefix = ENVIRONMENT_PREFIX_PREV .. WEBCALL_FOLDER
		m_securityToken = ENVIRONMENT_TOKEN_PREV
	elseif gameID == Environment.GAME_ID_DEV then -- Dev
		m_websitePrefix = ENVIRONMENT_PREFIX_DEV .. WEBCALL_FOLDER
		m_securityToken = ENVIRONMENT_TOKEN_DEV
	else
		log("ERROR - Controller_WebCall - Unable to determine valid game ID! gameID = "..tostring(gameID))
	end
end

---------------------------------------------------------------------
-- Global Event Response Handlers
---------------------------------------------------------------------
-- kgp_result
---------------------------------------------------------------------
function _G.kgp_result(result)
	if type(result) ~= "table" then
		log("kgp_result called with bad argument type: " .. type(result))
		return
	end

	if type(result.http) ~= "table" then
		log("kgp_result: received non HTTP result - other asynchronous API?")
		return
	end

	if type(result.arg) ~= "number" then
		log("kgp_result: received unknown result. callIndex not set.")
		return
	end

	-- HTTP results (kgp.scriptMakeWebcall)
	local returnCode = result.http.code
	local returnDescription = result.http.desc
	local text = result.data
	local callIndex = result.arg
	
	if not m_callQueue[callIndex] then
		log("kgp_result: call with index "..tostring(callIndex).." not found in queue.")
		return
	end
	
	local CID = m_callQueue[callIndex].CID
	local callType = m_callQueue[callIndex].callType
	
	m_callQueue[callIndex] = nil

	log("returnCode = "..tostring(returnCode)..", returnDescription = "..tostring(returnDescription)..", text = "..tostring(text))	
	if CID > 0 then
		Events.sendEvent("FRAMEWORK_WEB_CALL_RESPONSE", {CID = CID, callType = callType, success = true, returnCode = returnCode, returnDescription = returnDescription, text = text})
	end
end