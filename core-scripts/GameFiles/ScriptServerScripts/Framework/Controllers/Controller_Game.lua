--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Controller_Game.lua
include("Lib_Vecmath.lua")
include("Lib_Common.lua")
include("PlayerPhysicsIds.lua")
include("Lib_SaveLoad.lua")
--include("Lib_GameItemBootstrap.lua")
include("DayStateIds.lua")
include("EnvironmentIds.lua")
include("Lib_GameDefinitions.lua")
include("Lib_Web.lua")
include("Lib_ZoneInfo.lua")
include("Lib_SpinUpMetrics.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local GameDefinitions	= _G.GameDefinitions
local Toolbox			= _G.Toolbox
local Web				= _G.Web
local SaveLoad			= _G.SaveLoad
local Events			= _G.Events
local Players			= _G.Players
local Class				= _G.Class
local Math				= _G.Math
local ZoneInfo 			= _G.ZoneInfo
local SpinUpMetrics     = _G.SpinUpMetrics

-----------------------------------------------------------------
-- Class Specification
-----------------------------------------------------------------
Class.createClass("Controller_Game", "Controller")

Class.useBehaviors({
	"Behavior_Sound"
})

Class.export({
	{ "writeToTicker", "event"},
	-- Player APIs
	{ "playerSetPhysics", "event" },
	{ "playerTeleport", "event" },
	{ "playerPlaySound", "event" },
	{ "playerStopSound", "event" },
	{ "playerSetAnimation", "event" },
	{ "playerSetParticle", "event" },
	{ "playerRemoveParticle", "event" },
	{ "playerPlaceItem", "event" },
	{ "playerSetMode", "event" },
	{ "spawnPlayer", "event" },
	-- HUD element management
	{ "enableHealthBar", "event" },
	{ "enableEnergyBar", "event" },
	{ "enableArmorBar", "event" },
	-- Stat management
	{ "playerAddEnergy", "event" },
	{ "playerRemoveEnergy", "event" },
	{ "playerSetEnergy", "event" },
	{ "logProgress", "event" },
	-- inherited from Controller
	{ "config", "key", "value" },
	{ "getWorldSettings" }
})




----------------------------------------------------------------------------------------------------------------------
-- TEMP CONTROLLER CREATION
-- NOTE: This will be removed once Controller_Player is properly integrated into the system.
----------------------------------------------------------------------------------------------------------------------
--local threadPID = kgp.objectGenerate(GLID_SINGULARITY, -1000, -1000, -1000, 0, 0, 1)
--if threadPID then
--	kgp.objectSetScript(threadPID, "Controller_Player.lua")
--end
----------------------------------------------------------------------------------------------------------------------


--------------------------------------------------------
-- Constant definitions
--------------------------------------------------------


-- Table of events to be registered and accompanying handlers
-- NOTE: Only events needed post-Framework spinup should be here. If you need an event to respond while the Framework is still
-- offline, register for it in in create()
local CONTROLLER_EVENTS = {
	REQUEST_MAP_BY_TYPE 				 	= "onMapTypeRequest", -- Returns all objects of a specified type
	FRAMEWORK_REQUEST_BR_HUD_INFO 		 	= "onPlayerChangeModeOpened", -- Register for player mode change menu opened
	FRAMEWORK_PLAYER_CHANGE_MODE 		 	= "onPlayerChangeMode", -- Register for player mode change events
	FRAMEWORK_CONSUME_ENERGY_ITEM 		 	= "onEnergyItemConsumed", -- Register for energy item consumed events
	FRAMEWORK_REQUEST_SETTINGS 			 	= "onSettingsRequested", -- Register for requests for the current zone settings
	FRAMEWORK_UPDATE_SETTINGS 			 	= "onSettingsUpdated",
	FRAMEWORK_REQUEST_ENVIRONMENT 		 	= "onEnvironmentRequested", -- Register for requests for the current environment settings
	FRAMEWORK_UPDATE_ENVIRONMENT 		 	= "onEnvironmentUpdated",
	FRAMEWORK_REQUEST_WELCOME	 		 	= "onWelcomeRequested", -- Register for requests for the current environment settings
	FRAMEWORK_UPDATE_WELCOME			 	= "onWelcomeUpdated",
	FRAMEWORK_REQUEST_OVERVIEW	 		 	= "onOverviewRequested", -- Register for requests for the current environment settings
	FRAMEWORK_UPDATE_OVERVIEW			 	= "onOverviewUpdated",
	REGISTER_SYSTEM_DYNAMIC_OBJECT 			= "onRegisterSystemObject", -- Register for flag settings 
	UNREGISTER_SYSTEM_DYNAMIC_OBJECT 		= "onUnregisterSystemObject",
	FRAMEWORK_REQUEST_FLAGS 			 	= "onRequestFlags",
	FRAMEWORK_REQUEST_MEDIA_OBJECTS 		= "onRequestMediaObjects",
	FRAMEWORK_REQUEST_EVENT_FLAGS			= "onRequestEventFlags",
	EVENT_FLAG_OCCURRING					= "onEventFlagOccurring",
	SYSTEM_FLAG_TRIGGER 				 	= "onFlagTrigger", -- Flag Trigger Events
	REQ_FLAG_FEE_NOTIF_REQ_LIST 			= "onFlagFeeNotifReqList",
	FLAG_DELETE_ITEMS						= "onFlagDeleteItems",
	FRAMEWORK_SET_CREATOR_BOOST 		 	= "setCreatorBoost", -- Register for creator boost events
	--FRAMEWORK_SYSTEM_OBJECT_READY 		= "onSystemObjectReady", -- System objects send an event when ready and need to be displayed to world creators
	FRAMEWORK_DYNAMIC_OBJECT_SAVE_SERVER 	= "onDynamicObjectSave", -- Dynamic objects send an event when manipulated, we may need to save these objects or update their positions in the s_uItemInstances
    FRAMEWORK_DYNAMIC_OBJECT_MOVE 	        = "onDynamicObjectMove",
	FRAMEWORK_REQUEST_PERM_AND_CREATOR_MODE = "requestPermissionsAndCreatorMode",
	FRAMEWORK_REQUEST_CURRENT_TIME		 	= "returnCurrentTime",
	FRAMEWORK_HIDE_OBJECT					= "onHideObject",
	FRAMEWORK_SET_AVAILABLE_ZONES			= "onSetAvailableZones",
	FRAMEWORK_REQUEST_PLAYER_SPAWNERS		= "getPlayerSpawners",
	FRAMEWORK_LINKED_ZONE_TELEPORT			= "onLinkedZoneTeleport"
	--SETUP_DB 							 	= "setupDB", -- TODO: Temp Rickman setup call for homes and communities. We should setup Rickman some other way in the future (on world creation)
	--RICKMAN_BOOTSTRAP 					 = "bootstrap"
}

--This probably doesn't belong here
local DEF_MAX_ENERGY = 100
local DEF_MAX_HEALTH = 10
local DEF_DEATH_COUNT = 0
local DEF_NPC_SPOUSE = -1
local DEF_FOLLOW_COUNT = 0

local PLATFORM_GLID = 4382757

-- World lower-respawn boundary
local WORLD_LOWER_BOUND = -5000

-- Respawn Y buffer to prevent falling through stuff
local RESPAWN_Y_BUFFER = 1

local DEFAULT_STARVE_TIME = 1200
local m_starveTime = DEFAULT_STARVE_TIME -- How many seconds will it take for a full energy bar to reach 0
local SECONDS_PER_MINUTE = 60


local STARVE_DURATION = 120 -- How many seconds will it take for a full health player to starve?
local ENERGY_FOR_HEALTH_REGEN = 50 -- What percent must your energy be before health regen can take place
local HEALTH_REGEN_PER_SEC = 0.5 -- What percent of your health regens every second?
local SAVE_TIME = 300 -- How frequently the server writes out player data to the database

local TELEPORT_TIME = 2 --How long to wait until player can teleport

local BOUND_CHK_BUFFER = 0.001 -- Amount of leeway for "is X within bounds of Y" checks
local BOUND_CHK_BUFFER_Y = 1.5 -- Amount of leeway for checks BELOW the Y value (accounts for slight slopes and the fact that the player avatar is technically slightly beneath the ground)

local PIN_SAVE_KEY = "DNC_pinCode"
local PIN_ACCESS_MEM_KEY = "DNC_pinCodeMemory"

-- Controller activation order
local ACTIVATION_ORDER = {
	"Spawn",
	"Battle",
	"Inventory",
	"Player"
}

-- Controller deactivation order
local DEACTIVATION_ORDER = {
	"Player",
	"Inventory",
	"Spawn",
	"Battle"
}

local DEF_PHYSICS = {
	MAXSPEED = 0.11, 
	MINSPEED = -0.11, 
	ACCELLERATE = 1, 
	DECELLERATE = 0.2,
	CURTRANSY = 0, 
	ROTATIONDRAGCONSTANT = 0.3, 
	ROTATIONALGROUNDDRAG = 1, 
	JUMPPOWER = 2, 
	GROUNDDRAG = 0.75,
	STRAFEREDUCTION = 0.5,
	CROSSSECTIONALAREA = 0.9, 
	AIRCONTROL = 0.3, 
	JUMPRECOVERTIME = 0,
	COLLISIONENABLED = 1
}

--Magic numbers for event flags
local EVENT_SCHEDULED = 1 
local EVENT_OCCURRING = 2

--Magic number for permission for normal players. Less than this value are GM's, Owners and mods
local PLAYER_PERMISSION  = 2


-----------------------------------------------------------------
-- Local variable definitions
-----------------------------------------------------------------
local s_uItemInstances		-- SharedData table of all known objects in the world {PID:{itemData}}
local s_uPlayers			-- SharedData table of all players {playerName:{playerData}}
local s_uPlayerSpawners 	-- SharedData table player Spawners {PID:{properties}}
local s_tPendingPlayers = {} -- Table of players that haven't been instantiated in the world
local s_uItemProperties		-- This is now maintained as a SharedData as opposed to a simple Lua table. The contained data is structured as so: {UNID: {propertyList}}

local s_tGameConfigs = {} --SharedData table for Game Configs


local s_iControllerActivationIndex = 1
local s_bAreControllersReady = false
local s_bHasFrameworkStarted = false
local s_bIsInstantiatingFramework = false

local s_tControllerStates = {
	Battle = {
		online = false,
		active = false,
	},
	Spawn = {
		online = false,
		active = false,
	},
	Inventory = {
		online = false,
		active = false,
	},
	Player = {
		online = false,
		active = false
	}
}

local s_tObjectsEnablingHUD = {health = {}, energy = {}, armor = {}} -- Table of objects that have opened the various HUD elements

local s_nTick
local s_nAlertTimer = 0
local s_nAlertInterval = 30 -- in seconds

local s_nSaveTimer = SAVE_TIME

local s_tSettings = {} -- Table of all worlds settings
local s_tEnvironment = {} -- Table of all environment settings
local s_tWelcome = {} --Table of all welcome screen settings
local s_tOverview = {} --Table of all overview screen settings
local s_tFlags = {}
local s_tMedia = {}
local s_tEvents = {}
local s_tEventsReturned = false

--Only send the media and flags once the server is ready
local s_bIsPendingMedia = true
local s_bPendingFlags = true
local s_tGameConfigs = {} -- Game Config fields

local s_uInstantiatedGameItem = nil -- The Game Item that instantiated the framework (needed to log the first placed item)
local s_uInstantiatingPlayer = nil -- Player instantiating the Framework
local s_uDeinstantiatingPlayer = nil -- Player de-instantiating the Framework

local s_tDefaultEnvironment
local s_tDefaultSpawnPoint
local s_tRespawnQueue = {} -- Players queued for a respawn

local s_bIsCreatorModeEnabled -- Is Creator Mode Enabled in this world?

local s_tTopWorldsReward = {}

local s_iZoneInstanceID, s_iZoneType

local s_tInitialPlayerPositions = {} --Initial position when a player spawns

--local s_banList = {} --List of players banned from chat in world

local s_tPlayerTitles = {} -- List of player titles to be updated to include or exclude "AFK"

-----------------------------------------------------------------
-- Local function declarations
-----------------------------------------------------------------

local getPlayers -- ()
local deprecateEnergy -- (player, deltaTick)
local regenEnergy -- (player, deltaTick)
local regenHealth -- (player, deltaTick)
local starvePlayer -- (player, deltaTick)
local getEnvironmentSettings -- ()
local setEnvironmentSettings -- ()
local getWelcomeSettings -- ()
local getOverviewSettings -- {}
local checkRGBRange -- ()
local checkFlagZoneSettings -- (playerSetting, inFlag, playerName, flagType, PID)
local systemObjectShowForPlayer -- (playerID, PID, name, particleGLID)
local systemObjectHideForPlayer -- (playerID, PID)
local getPlayerTimedVends -- (playerName)
local constructLabelTable -- (labelTable)
local getCreatorModeEnabled -- ()
local registerSytemObjects --(itemTable, event)
local unRegisterSystemObjects --(itemTable,event)
local isPlayerClaimFlagMember -- (playerName, flagPID)

function create(self)
	_super.create(self)
	
	-- Register for timer events
	Events.registerHandler("TIMER", self.onTimer, self)
	 -- Handles PlayerHandler menu idle state opened, requesting basic player info
	Events.registerHandler("FRAMEWORK_REQUEST_IDLE_PLAYER_INFO", self.onIdlePlayerInfoRequested, self)
	-- Handles HUDKaneva requesting parent info for KIM
	Events.registerHandler("FRAMEWORK_REQUEST_PARENT_DATA", self.onParentDataRequested, self)

	--Handles Chatmenu ban list
	--Events.registerHandler("UPDATE_CHAT_BAN_LIST", self.updateChatBanList, self)

	--Loading complete
	Events.registerHandler("FRAMEWORK_LOADING_COMPLETE", self.loadingComplete, self)
	
	s_uItemInstances	= SharedData.new("items", false)
	s_uPlayers			= SharedData.new("players", false)
	s_uPlayerSpawners	= SharedData.new("spawners", false)
	s_uItemProperties	= SharedData.new("gameItemProperties", false)

	s_tGameConfigs		= SharedData.new("config", false)
	
	local tempConfig = kgp.gameLoadGlobalConfig({"GameItemLoad", "WOK_FrameworkEnabled", "HomeChildWorldTemplateId", "DynamicZoneLinkParentId", "KTown80InstanceId"})

	for index, value in pairs(tempConfig) do
		s_tGameConfigs[index] = value
	end

	getCreatorModeEnabled()
	self:getWorldSettings()

	math.randomseed(kgp.gameGetCurrentTime()) -- Generate truely random numbers on math.random()

	-- Is the Framework on at all?
	if s_tGameConfigs.WOK_FrameworkEnabled then
		-- Get what controllers are active on their VMs at this point
		-- (Accounting for race conditions. If Controllers could boot up in a guaranteed order, we would not need this)
		Events.sendEvent("FRAMEWORK_QUERY_CONTROLLER_STATE")
		Events.registerHandler("FRAMEWORK_CONTROLLER_STATE", self.controllerState, self)
		Events.registerHandler("FRAMEWORK_INSTANTIATE", self.instantiateFramework, self)
		Events.registerHandler("FRAMEWORK_DE_INSTANTIATE", self.deInstantiateFramework, self)
		Events.registerHandler("FRAMEWORK_RETURN_ENVIRONMENT_DEFAULTS", self.returnEnvironmentDefaults, self)
		Events.registerHandler("FRAMEWORK_RETURN_ZONE_DATA", self.returnZoneData, self)
		Events.registerHandler("FRAMEWORK_RETURN_DEFAULT_SPAWNPOINT", self.returnDefaultSpawnPoint, self)
		Events.registerHandler("FRAMEWORK_WEB_CALL_RESPONSE", self.webCallResponse, self)
		Events.registerHandler("FRAMEWORK_CHECK_TOP_WORLD_REWARD", self.checkTopWorldReward, self)
		Events.registerHandler("FRAMEWORK_REQUEST_FLAG_ZONE_SETTINGS", self.returnFlagZoneSettings, self)
		Events.registerHandler("FRAMEWORK_DECLINE_EVENT", self.declineEventHandler, self)
		Events.registerHandler("FRAMEWORK_ACCEPT_EVENT", self.acceptEventHandler, self)
		Events.registerHandler("FRAMEWORK_UPDATE_AFK_STATUS", self.updateAFKStatus, self)
		Events.registerHandler("FRAMEWORK_REQUEST_DEATH_COUNT", self.returnDeathCount, self)
		Events.registerHandler("FRAMEWORK_INCREMENT_DEATH_COUNT", self.incrementDeathCount, self)
		Events.registerHandler("FRAMEWORK_REQ_FOLLOWER_COUNT", self.onReqFollowerCount, self)
		Events.registerHandler("FRAMEWORK_UPDATE_FOLLOWER_COUNT", self.updateFollowerCount, self)
		Events.registerHandler("FRAMEWORK_REQ_NPC_SPOUSE", self.onReqNpcSpouse, self)
		Events.registerHandler("FRAMEWORK_UPDATE_NPC_SPOUSE", self.updateNpcSpouse, self)
		Events.registerHandler("RETURN_NPC_SPOUSE_EXISTS", self.onReturnNpcSpouseExists, self)
		Events.registerHandler("RETURN_NPC_FOLLOWER_EXISTS", self.onReturnNpcFollowerExists, self)
		Events.registerHandler("FRAMEWORK_MOVE_PLAYER", self.playerTeleport, self)
		Events.registerHandler("FRAMEWORK_SAVE_LAST_POSITION", self.savePlayerPostion, self)

		Events.registerHandler("TUTORIAL_SEND_TO_KTOWN", self.sendPlayerToKTown, self)
		
		
		Events.registerHandler("REQUEST_PIN_CODE", self.requestPinCode, self)
		Events.registerHandler("SAVE_PIN_CODE", self.savePinCode, self)
		Events.registerHandler("RESET_NPC_SPOUSE_FOR_PLAYER", self.resetNpcSpouse, self)
		-- decrement follower count
		

		-- Register for arrive events
		Events.registerHandler("PLAYER-ARRIVE", self.onPlayerArrive, self)
		-- Register for depart events
		Events.registerHandler("PLAYER-DEPART", self.onPlayerDepart, self)
		
		
		if s_tSettings.frameworkEnabled then
			-- Bootup the Framework
			bootupFramework(self, true)
		end
	end
end

function stop(self, user)
	_super.stop(self, user)
end

-- Called when a Controller's state has been modified
function controllerState(self, event)
	if event.controller == nil then log("Controller_Game - controllerState() Passed controller is nil") return end
	local stateController = s_tControllerStates[event.controller]
	local controller = event.controller
	-- Is this a registered controller
	if stateController then
		local controllerState = event.state
		-- Was this controller just brought online and is in line for activation?
		if (stateController.online == false) and (controllerState.online == true) then
			stateController.online = true
			
			-- Is this controller in queue for activation?
			if ACTIVATION_ORDER[s_iControllerActivationIndex] == controller and s_tSettings.frameworkEnabled then
				self:activateQueuedController()
			end
		end
		
		-- Was this controller just activated?
		if (stateController.active == false) and (controllerState.active == true) then
			stateController.active = true
			
			if ACTIVATION_ORDER[s_iControllerActivationIndex] == event.controller then
				SpinUpMetrics.recordControllerSpinupComplete(ACTIVATION_ORDER[s_iControllerActivationIndex])
				s_iControllerActivationIndex = s_iControllerActivationIndex + 1
				self:activateQueuedController()
			end
		
		-- Controller was just shut down
		elseif (stateController.active == true) and (controllerState.active == false) then
			stateController.active = false
			
			if DEACTIVATION_ORDER[s_iControllerActivationIndex] == event.controller then
				-- Log progress
				self:logProgress{log = "Controller shut down ["..tostring(controller).."]"}
				
				-- Do we have another controller to deactivate?
				s_iControllerActivationIndex = s_iControllerActivationIndex + 1
				self:deActivateQueuedController()
			end
		end
	else
		log("Controller_Game - controllerState() Controller["..tostring(event.controller).."] isn't a registered controller in s_tControllerStates")
	end
end

-- Called when the Framework needs to be instantiated
function instantiateFramework(self, event)
	if s_bHasFrameworkStarted then log("Controller_Game - instantiateFramework() Framework already online") return end
	if event.player == nil then log("Controller_Game - instantiateFramework() event.player is nil") return end
	if event.gameItem == nil then log("Controller_Game - instantiateFramework() event.gameItem is nil") return end
	
	s_uInstantiatingPlayer = event.player
	s_uInstantiatedGameItem = event.gameItem
	
	s_bIsInstantiatingFramework = true
	s_iControllerActivationIndex = 1
	
	local tPlayers = getPlayers()
	for sName, uPlayer in pairs(tPlayers) do
		-- Lock all players
		self:playerSetPhysics({user = uPlayer, lock = true})
		
		-- Open the Progress menu
		kgp.playerLoadMenu(uPlayer.ID, "Framework_LoadProgress.xml")
	end
	
	-- Update frameworkEnabled flag
	s_tSettings.frameworkEnabled = true
	kgp.gameSetFrameworkEnabled(true)
	
	getCreatorModeEnabled()

	-- Bootup the Framework
	bootupFramework(self, true)
end

-- De-instantiate the Framework
function deInstantiateFramework(self, event)
	if s_bHasFrameworkStarted == false then log("Controller_Game - deInstantiateFramework() Framework already offline") return end
	
	s_iControllerActivationIndex = 1
	-- Open Framework_LoadProgress for all players
	local tPlayers = getPlayers()
	for sName, uPlayer in pairs(tPlayers) do
		kgp.playerLoadMenu(uPlayer.ID, "Framework_LoadProgress.xml")
	end
	
	s_uDeinstantiatingPlayer = event.player
	
	self:logProgress{log = "Shutting down Framework", display = "Deactivating Gaming"}
	
	-- Queue up the first Controller for deactivation
	local activationEvent = {["controller"] = DEACTIVATION_ORDER[s_iControllerActivationIndex]}
	Events.sendEvent("FRAMEWORK_DEACTIVATE_CONTROLLER", activationEvent)
end

-- Handles the environment settings when returned from DefaultHandlers
function returnEnvironmentDefaults(self, event)
	if event.sun == nil then log("Controller_Game - returnEnvironmentDefaults() event.sun is nil") return end
	if event.ambient == nil then log("Controller_Game - returnEnvironmentDefaults() event.ambient is nil") return end
	
	s_tDefaultEnvironment = {
		sunColorRed 	  = math.floor(event.sun.r*255),
		sunColorGreen 	  = math.floor(event.sun.g*255),
		sunColorBlue 	  = math.floor(event.sun.b*255),
		ambientColorRed   = math.floor(event.ambient.r*255),
		ambientColorGreen = math.floor(event.ambient.g*255),
		ambientColorBlue  = math.floor(event.ambient.b*255)
	}
end

-- Handles the zoneInstanceId and zoneType from Framework_PlayerHandler
function returnZoneData(self, event)
	s_iZoneInstanceID = event.zoneInstanceId
	s_iZoneType = event.zoneType
end

-- Handles the default spawn point returned from DefaultHandlers
function returnDefaultSpawnPoint(self, event)
	if event.spawnPoint == nil then log("Controller_Game - returnDefaultSpawnPoint() event.spawnPoint is nil") return end
	
	s_tDefaultSpawnPoint = event.spawnPoint
	
	-- Are there any players in the respawn queue?
	local rot = {}
	rot.rx, rot.rz = Math.degreesToVector(s_tDefaultSpawnPoint.r)
	
	for user,_ in pairs(s_tRespawnQueue) do
		if user then
			local tTeleportSettings = {
				user = user,
				x = s_tDefaultSpawnPoint.x, 
				y = s_tDefaultSpawnPoint.y + RESPAWN_Y_BUFFER, 
				z = s_tDefaultSpawnPoint.z,
				rx = rot.rz,
				ry = 0,
				rz = rot.rx
			}
			playerTeleport(self, tTeleportSettings)
		end
	end
	s_tRespawnQueue = {}
end

-- Sent from Controller_WebCall which returns webcall results
function webCallResponse(self, event)
	if event.CID == 100 and event.callType == "GET_TOP_WORLDS" then
		local username = Web.getDataByTag(event.text, "Username", "(.-)")
		for object in string.gmatch(event.text, "<_x0033_DApps>(.-)</_x0033_DApps") do
			local zoneID = Web.getDataByTag(object, "zone_instance_id", "(.-)")			
			if tonumber(zoneID) == tonumber(s_iZoneInstanceID) then
				log("webCallResponse() -- In Top World " .. zoneID .. " " .. username)
				--In top World!
				userRewarded = Web.getDataByTag(object, "user_rewarded", "(.-)")
				if userRewarded == "N" then
					--Reward player
					s_tTopWorldsReward[username] = true
					log("webCallResponse() -- " .. username .. " has not been rewarded")
				end
			end
		end
	elseif event.CID == 200 and event.callType == "REQUEST_EVENT" then
		s_tEventsReturned = true
		local scheduledEvents = false
		local pid = nil


		for eventStr in string.gmatch(event.text, "<Event>(.-)</Event>") do
			pid = string.match(eventStr, "<ObjPlacementId>(.-)</ObjPlacementId>")
			if pid then
				s_tEvents[tostring(pid)] = EVENT_SCHEDULED
				scheduledEvents = true
			end
		end

		if scheduledEvents then
			Events.sendClientEvent("FRAMEWORK_RETURN_EVENT_FLAGS_FULL", {eventFlags =  s_tEvents})
			Events.sendEvent("REGISTER_EVENT_FLAGS", {eventsInfo = event.text})
		end
	end
end

-- Sent from Controller_WebCall which returns webcall results
function checkTopWorldReward(self, event)
	if event.player then
		log("checkTopWorldReward() -- check if player can be rewarded")
		if s_tTopWorldsReward[event.player.name] then
			--Player gets reward
			s_tTopWorldsReward[event.player.name] = false
			log("checkTopWorldReward() -- REWARD PLAYER!")
			local input = {username = event.player.name, zoneInstanceId = s_iZoneInstanceID, zoneType = s_iZoneType}
			Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {input = input, callType = "REWARD_TOP_WORLD_LOOT"})
		end
	end
end

--function setupDB(self, event)
	--SaveLoad.saveGameData("CurrentZoneType", "Survival")
	--SaveLoad.saveGameData("NextUNID", 2000)
	--local settings = {allowPvP = true, privacy = "Public", destructibleObjects = true, playerBuild = true, playerNames = false}
	--local envSettings = {--[[envEnabled = true, ]]fogEnabled = true, fogStartRange = 1, fogEndRange = 7000, fogColorRed = 255, fogColorGreen = 229.5, fogColorBlue = 178.5, --[[ambientEnabled = false,]] ambientColorRed = 255, ambientColorGreen = 255, ambientColorBlue = 255, sunColorRed = 255, sunColorGreen = 255, sunColorBlue = 255}
	--SaveLoad.saveGameData("EnvironmentSettings", Events.encode(envSettings))
	--SaveLoad.saveGameData("WorldSettings", Events.encode(settings))
	--bootstrap()
--end

--[[function bootstrap()
	bootstrapGameItems()
end]]

---------------------------------
-- Controller events handlers
---------------------------------

-- When a a player enters the zone
function onPlayerArrive(self, event)
	local player = event.player
	local playerName = player and player.name
	if playerName == nil then
		log("ERROR DETECTED - ED-6813 : onPlayerArrive player name was nil!")
		Debug.printTableError("ERROR DETECTED - ED-6813", player)
		return
	end
	--Send an event to clear out the last game item stored by the player
	Events.sendClientEvent("FRAMEWORK_ONE_GAME_ITEM_DO_LEFT", {lastGameItem = nil, playerPerm = player.perm}, player.ID)
	SpinUpMetrics.recordPlayerArrive(playerName)
	self:instantiatePlayer(player)
	-- If the Framework's online, activate this player
	if s_tSettings.frameworkEnabled and player then
		-- Lock this player
		self:playerSetPhysics({user = player, lock = true})
		if s_bAreControllersReady then
			self:activatePlayer(player)
			Events.sendClientEvent("FRAMEWORK_PLAYER_LIST_UPDATE", {playerName = playerName, arriving = 1})
		else
			s_tPendingPlayers[playerName] = {player = player}
		end
	-- Otherwise, hold onto them in case the Framework gets booted up this session
	else
		s_tPendingPlayers[playerName] = {player = player}
	end

	-- if event.player.name and event.player.ID then
	-- 	Events.sendClientEvent("CHAT_ADD_PERSON", {playerName = event.player.name, userID = event.player.ID})
	-- end

	-- Add the player to the AFK player list
	local newPlayer = {}
	newPlayer.user = player
	newPlayer.AFK = false
	newPlayer.name = playerName
end





-- When a a player leaves the zone
function onPlayerDepart(self, event)
	
	if self == nil then log("Controller_Game - onPlayerDepart() self is nil."); return end
	if event == nil then log("Controller_Game - onPlayerDepart() event is nil."); return end
	if event.player == nil then log("Controller_Game - onPlayerDepart() event.player is nil."); return end
	if event.pos == nil then log("Controller_Game - onPlayerDepart() event.pos is nil."); return end
	
	local uPlayer = event.player
	
	if uPlayer == nil then log("Controller_Game - onPlayerDepart() event.player is nil."); return end
	
	-- Remove the player from the player list
	
	Events.sendClientEvent("FRAMEWORK_PLAYER_LIST_UPDATE", {playerName = uPlayer.name, arriving = 0})


	if uPlayer.inZone then
		-- Save off player stats if the framework's online
		if uPlayer and uPlayer.name and s_bHasFrameworkStarted then
			log("****** Saving "..tostring(uPlayer.name).." ("..tostring(uPlayer.ID)..") spawn point: x = "..tostring(event.pos.x)..", y = "..tostring(event.pos.y)..", z = "..tostring(event.pos.z)..", rx = "..tostring(event.pos.rx)..", ry = "..tostring(event.pos.ry)..", rz = "..tostring(event.pos.rz))
			kgp.playerSaveZoneSpawnPoint(event.player.ID, event.pos.x,  event.pos.y + RESPAWN_Y_BUFFER, event.pos.z,
														  event.pos.rx, event.pos.ry, event.pos.rz)
			-- Save off stats for this player
			local zonesVisited = nil
			if ZoneInfo.isLinkedZone and uPlayer.zonesVisited then
				zonesVisited = Toolbox.deepCopy(uPlayer.zonesVisited)
			end

			local playerStats = {energy = uPlayer.energy or DEF_MAX_ENERGY,
			maxEnergy = uPlayer.maxEnergy or DEF_MAX_ENERGY,
								 health = uPlayer.health or DEF_MAX_HEALTH,
								 maxHealth = uPlayer.maxHealth or DEF_MAX_HEALTH,
								 deathCount = uPlayer.deathCount or DEF_DEATH_COUNT,
								 npcSpouse = uPlayer.npcSpouse or DEF_NPC_SPOUSE,
								 followerCount = uPlayer.followerCount or DEF_FOLLOW_COUNT,
								 zonesVisited  = zonesVisited
								}
			SaveLoad.savePlayerData(uPlayer.name, "playerStats", Events.encode(playerStats))
		end
		
		if uPlayer.damageSound then
			Behavior_Sound.delete({soundID = uPlayer.damageSound})
			uPlayer.damageSound = nil
		end
		
		if uPlayer.playerInventorySound then
			Behavior_Sound.delete({soundID = uPlayer.playerInventorySound})
			uPlayer.playerInventorySound = nil
		end

		-- Mark player as exited
		uPlayer.inZone = false
		-- if event.player.name  then
		-- 	Events.sendClientEvent("CHAT_REMOVE_PERSON", {playerName = event.player.name})
		-- end
	end
end

function playerLoadInventory(self, event)
	Events.sendEvent("PLAYER_LOAD_INVENTORY", event)
end

function downloadGameItemIcon(self, event)
	Events.sendEvent("DOWNLOAD_GAME_ITEM_ICON", event)
end

function updateAFKStatus(self, event)
	local userInfo = event.player
	local AFKStatus = event.AFK
	-- Update AFK status
	if AFKStatus == true then
		-- Save of their previous title (Return this to them when they are no longer AFK ??? or leave the world?)
		s_tPlayerTitles[userInfo.ID] = kgp.playerGetTitle(userInfo.ID)
		-- Assign the AFK title
		kgp.playerSetTitle(userInfo.ID, "<AFK> Away from Keyboard")
	elseif AFKStatus == false then
		if userInfo and userInfo.ID then
			-- Restore the old title
			local tempTitle = s_tPlayerTitles[userInfo.ID] or ""
			kgp.playerSetTitle(userInfo.ID, tempTitle)
			-- Reset animation
			-- self:playerSetAnimation{user = userInfo, animation = 0} -- Stand
		end
	end
end

-- Called on server tick
function onTimer(self, event)
	-- Initialize s_nTick
	if not s_nTick then
		s_nTick = event.tick
		return
	end
	
	local deltaTick = event.tick - s_nTick
	
	if self == nil then log("Controller_Game - onTimer() self is nil."); return end
	if event == nil then log("Controller_Game - onTimer() event is nil."); return end
	
	-- Update all known player positions
	for sName, uPlayer in pairs(s_uPlayers) do
		if uPlayer and uPlayer.type and uPlayer.ID and uPlayer.position and uPlayer.inZone then
			-- Update player's location
			local pX, pY, pZ, pRX, pRY, pRZ = kgp.playerGetLocation(uPlayer.ID)
			uPlayer.position.x  = pX  or 0
			uPlayer.position.y  = pY  or 0
			uPlayer.position.z  = pZ  or 0
			uPlayer.position.rx = pRX or 1
			uPlayer.position.ry = pRY or 0
			uPlayer.position.rz = pRZ or 0
			
			-- Handle health and energy only if the Framework's online
			if s_tGameConfigs.WOK_FrameworkEnabled then
				if uPlayer.mode ~= "God" then
					-- Detect if the player has fallen "too far"
					if uPlayer.position.y < WORLD_LOWER_BOUND then
						-- Respawn this player
						self:spawnPlayer({user = uPlayer})
					end
				end
			
				local energyActive = false
				local healthActive = false
				-- If someone in the zone is using energy
				for PID, _ in pairs(s_tObjectsEnablingHUD.energy) do
					energyActive = true
					break
				end
				-- If someone in the zone is using health
				for PID, _ in pairs(s_tObjectsEnablingHUD.health) do
					healthActive = true
					break
				end
				
				if uPlayer.mode == "Player" and uPlayer.alive then
					-- If energy is active in the world
					if energyActive then
						-- If this player has energy to lose
						if uPlayer.regenTime then
							regenEnergy(uPlayer, deltaTick/1000)
							if healthActive then
								regenHealth(uPlayer, deltaTick/1000)
							end
						elseif uPlayer.energy > 0 then
							-- Deprecate energy from every player
							deprecateEnergy(uPlayer, deltaTick/1000)
							if healthActive then
								regenHealth(uPlayer, deltaTick/1000)
							end
						-- No energy, deduct health
						elseif healthActive and s_tSettings.starvation then
							local bAlert = false
							-- Count down for player's starvation alert
							if s_nAlertTimer >= 0 then
								s_nAlertTimer = s_nAlertTimer - deltaTick
							else
								bAlert = true
								s_nAlertTimer = s_nAlertInterval * 1000
							end

							starvePlayer(uPlayer, deltaTick/1000, bAlert)
						end
					end
				end
			end
		end
	end
	
	s_nSaveTimer = s_nSaveTimer - deltaTick/1000
	if s_nSaveTimer <= 0 then
		Events.sendEvent("FRAMEWORK_SAVE_PLAYER_DATA")
		s_nSaveTimer = SAVE_TIME
	end
	
	s_nTick = event.tick
end

-- Called when an object requests all map objects by type
function onMapTypeRequest(self, event)
	if self == nil then log("Controller_Game - onMapTypeRequest() self is nil."); return end
	if event == nil then log("Controller_Game - onMapTypeRequest() event is nil."); return end
	if event.type == nil then log("Controller_Game - onMapTypeRequest() event.type is nil. Sender["..tostring(event._sender).."]"); return end

	local matchedObjects = {}
	for PID, uItem in pairs(s_uItemInstances) do
		-- Is this of the requested type?
		if uItem and uItem.type == event.type then
			matchedObjects[PID] = uItem
		end
	end
	
	local player
	if event.user then
		player = Players.determineUser(event.user, s_uPlayers)
	end

	local tEventData = {
		map		= matchedObjects,
		user	= player or 0,
		PID		= event.PID
	}
	Events.sendEvent("RETURN_MAP_BY_TYPE", tEventData, event._sender)
end

-- Called when a player opens the mode change menu
function onPlayerChangeModeOpened(self, event)
	-- Error check inputs
	if self == nil then log("Controller_Game - onPlayerChangeModeOpened() self is nil."); return end
	if event == nil then log("Controller_Game - onPlayerChangeModeOpened() event is nil."); return end
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Game - onPlayerChangeModeOpened() event.player is nil."); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Game - onPlayerChangeModeOpened() event.player.ID is nil."); return end
	
	-- Turn on health bar?
	for PID, _ in pairs(s_tObjectsEnablingHUD.health) do
		Events.sendClientEvent("FRAMEWORK_ENABLE_HEALTH", {health = event.player.health, maxHealth = event.player.maxHealth}, playerID)
		break
	end
	-- Turn on energy bar?
	for PID, _ in pairs(s_tObjectsEnablingHUD.energy) do
		Events.sendClientEvent("FRAMEWORK_ENABLE_ENERGY", {energy = event.player.energy, maxEnergy = event.player.maxEnergy}, playerID)
		break
	end
	-- Turn on armor bar?
	for PID, _ in pairs(s_tObjectsEnablingHUD.armor) do
		Events.sendClientEvent("FRAMEWORK_ENABLE_ARMOR", {}, playerID)
		break
	end
end

-- Requests a basic idle player's 
function onIdlePlayerInfoRequested(self, event)
	-- Error check inputs
	if self == nil then log("Controller_Game - onIdlePlayerInfoRequested() self is nil."); return end
	if event == nil then log("Controller_Game - onIdlePlayerInfoRequested() event is nil."); return end
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Game - onIdlePlayerInfoRequested() event.player is nil."); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Game - onIdlePlayerInfoRequested() event.player.ID is nil."); return end
	
	-- Every player needs to know:
	-- Permission, framework configs and enabled/disabled
	
	local tEventData = {
		perm				= event.player.perm,
		frameworkEnabled	= s_tSettings.frameworkEnabled,
		configs				= Toolbox.deepCopy(s_tGameConfigs),
		jumpsuitOn			= s_tSettings.jumpsuit
	}
	Events.sendClientEvent("FRAMEWORK_RETURN_IDLE_PLAYER_INFO", tEventData, playerID)
end

---Request info about the parent
function onParentDataRequested(self, event)
	-- Error check inputs
	if self == nil then log("Controller_Game - onParentDataRequested() self is nil."); return end
	if event == nil then log("Controller_Game - onParentDataRequested() event is nil."); return end
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Game - onParentDataRequested() event.player is nil."); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Game - onParentDataRequested() event.player.ID is nil."); return end
	
	-- Every player needs to know:
	-- Permission, framework configs and enabled/disabled
	local parentID, parentType, parentCommunity = ZoneInfo.getParentZoneInstanceAndType()
	log("parentID = "..tostring(parentID)..", parentType = "..tostring(parentType)..", parentCommunity = "..tostring(parentCommunity))
	if parentType == 3 and parentCommunity and parentCommunity > 0 then
		parentID = parentCommunity
	end

	local bIsGameHome = kgp.gameHasDynamicLink()

	local availableZones = SaveLoad.loadGameData("AvailableZones", parentID, parentType)
	Events.sendClientEvent("FRAMEWORK_RETURN_PARENT_DATA",{zoneID = parentID, zoneType = parentType, availableZones = availableZones, gameHome = bIsGameHome} , playerID)
end


-- function updateChatBanList(self, event)
-- 	if self == nil then log("Controller_Game - onIdlePlayerInfoRequested() self is nil."); return end
-- 	if event == nil then log("Controller_Game - onIdlePlayerInfoRequested() event is nil."); return end

-- 	local blocker = event.blocker or ""
-- 	local returnMessage = ""
-- 	if blocker ~= "" then
-- 		returnMessage = "Player not found."
-- 	end
-- 	for key, value in pairs(s_uItemInstances) do
-- 		if string.lower(key) == event.bannedUser then
-- 			perm = s_uItemInstances[key].perm
-- 			if perm > 2 then 
-- 				table.insert(s_banList, key)	
-- 				returnMessage = event.bannedUser.." has been muted."
-- 			else
-- 				returnMessage = "Cannot mute GM or Owner."
-- 			end
-- 		end
-- 	end

-- 	if event.unbannedUser then
-- 		for i = 1, #s_banList do
-- 			--sanity check
-- 			if s_banList[i] and string.lower(s_banList[i]) == event.unbannedUser then
-- 				table.remove(s_banList, i)
-- 				returnMessage = event.unbannedUser.." is no longer muted."
-- 			end
-- 		end
-- 		if returnMessage == "Player not found." then
-- 			returnMessage = "Player is not currently muted."
-- 		end
-- 	end

-- 	Events.sendClientEvent("RETURN_CHAT_BAN_LIST", {banList = s_banList, returnMessage = returnMessage, blocker = blocker })
-- end

function loadingComplete(self, event)
	if self == nil then log("Controller_Game - loadingComplete() self is nil."); return end
	if event == nil then log("Controller_Game - loadingComplete() event is nil."); return end
	if event.player == nil then log("Controller_Game - loadingComplete() event.player is nil."); return end
	
	--If the tutorial is running, we do not need to do anything else
	if event.tutorial then return end
	local user = Players.determineUser(event.player, s_uPlayers)
	if user == nil then
		log("ERROR DETECTED - ED-2184 : loadingComplete unable to determine user!")
		Debug.printTableError("ERROR DETECTED - ED-2184 : event.player ", event.player)
		return
	end
	user.loading = nil
	user.canTeleport = false
	Events.setTimer(TELEPORT_TIME, function() user.canTeleport = true end)

	--Check if in land claim flag for leaderboard/top right hud
	checkFlagZoneSettings(true, false, event.player, "playerBuild", 0)
end

-- Called when a player changes modes
function onPlayerChangeMode(self, event)
	-- Error check inputs
	if self == nil then log("Controller_Game - onPlayerChangeMode() self is nil."); return end
	if event == nil then log("Controller_Game - onPlayerChangeMode() event is nil."); return end
	-- Was a valid player passed in?
	local uPlayer = event.player
	if uPlayer == nil then log("Controller_Game - onPlayerChangeMode() event.player is nil."); return end
	local playerName = uPlayer.name
	if playerName == nil then log("Controller_Game - onPlayerChangeMode() event.player.name is nil."); return end
		
	-- Go from player to god mode
	if uPlayer.mode == "Player" and event.mode == "God" then
		--kgp.playerRenderLabelsOnTop(event.player.ID, true)
		uPlayer.mode = "God"
		playerSetMode(self, {player = uPlayer})
	-- Go from god to player mode
	elseif uPlayer.mode == "God" and event.mode == "Player" then
		kgp.playerRenderLabelsOnTop(uPlayer.ID, false)
		uPlayer.mode = "Player"
		playerSetMode(self, {player = uPlayer})
	end
end

-- Called when a system object (shortcut or spawner) is created
--[[function onSystemObjectReady(self, event)
	local PID = tonumber(event.PID)
	local name = event.name
	local particleGLID = event.particleGLID

	if PID == nil or name == nil then return end
	
	local players = getPlayers()
	for _, player in pairs(players) do
		if player and player.inZone and player.ID and player.mode and player.mode == "God" then
			systemObjectShowForPlayer(player.ID, PID, name, particleGLID)
		elseif player and player.inZone and player.ID then
			systemObjectHideForPlayer(player.ID, PID)
		end
	end
end]]

-- Called when a dynamic object is manipulated outside of the framework
function onDynamicObjectSave(self, event)
	log("Controller_Game - onDynamicObjectSave")
	if not s_uItemInstances or not event or not event.PID then
		log("ERROR DETECTED - ED-5314: s_uItemInstances = "..tostring(s_uItemInstances)..", event = "..tostring(event)..", event.PID = "..tostring(event and event.PID).." (Ready : "..tostring(s_bAreControllersReady)..", Started : "..tostring(s_bHasFrameworkStarted)..")")
		return
	end
	local uItem = s_uItemInstances[event.PID]
	if uItem and uItem.type == "mover" then return end
	-- If this object is a game item, then save the new position off to the DB here
	if event.UNID ~= 0 then
		--Console.Write(Console.INFO, "Saving off dynamic object with PID = "..tostring(event.PID)..", UNID = "..tostring(event.UNID))
		if not validateRotationVector(event.objPos.rx, event.objPos.ry, event.objPos.rz) then
			log("ERROR DETECTED - ED-6320 : onDynamicObjectSave : event.PID = "..tostring(event.PID)..", event.objPos.rx = "..tostring(event.objPos.rx)..", event.objPos.ry = "..tostring(event.objPos.ry)..", event.objPos.rz = "..tostring(event.objPos.rz))
			return
		end

		local gameItem =  s_uItemProperties[tostring(event.UNID)]
		if gameItem and (gameItem.flagType or gameItem.behavior == "claim") then
			event.objPos.rx = 0
			event.objPos.rz = 1
		end
		kgp.gameItemSetPosRot(event.PID, event.objPos.x, event.objPos.y, event.objPos.z, event.objPos.rx, event.objPos.ry, event.objPos.rz)
	end
	
	if uItem then
		uItem.position = event.objPos
	else
		log("ERROR DETECTED - ED-6810: onDynamicObjectSave : uItem was nil for event.PID = "..tostring(event and event.PID))
	end
	
	Events.sendEvent("OBJECT_RESPAWN", {PID = event.PID})
end

function onDynamicObjectMove(self, event)
	local uItem = s_uItemInstances[event.PID]
    if uItem and uItem.type == "mover" then return end
	-- If this object is a game item, then save the new position off to the DB here
	if event.UNID ~= 0 then
		if not validateRotationVector(event.objPos.rx, event.objPos.ry, event.objPos.rz) then
			log("ERROR DETECTED - ED-6320 : onDynamicObjectMove : event.PID = "..tostring(event.PID)..", event.objPos.rx = "..tostring(event.objPos.rx)..", event.objPos.ry = "..tostring(event.objPos.ry)..", event.objPos.rz = "..tostring(event.objPos.rz))
			return
		end
		kgp.gameItemSetPosRot(event.PID, event.objPos.x, event.objPos.y, event.objPos.z, event.objPos.rx, event.objPos.ry, event.objPos.rz)
	end
	
	if uItem then
		uItem.position = event.objPos
	else
		log("ERROR DETECTED - ED-6810: onDynamicObjectMove : uItem was nil for event.PID = "..tostring(event and event.PID))
	end  
end

function validateRotationVector(rx, ry, rz)
	rx = tonumber(rx)
	ry = tonumber(ry)
	rz = tonumber(rz)
	
	if rx == nil or ry == nil or rz == nil then
		return false
	end
	
	local length = (rx * rx) + (ry * ry) + (rz * rz)
	
	return (length ~= 0)
end

-- When a client requests permissions of a given player
function requestPermissionsAndCreatorMode(self, event)
	if self == nil then log("Controller_Game - requestPermissionsAndCreatorMode() - self is nil."); return end
	if event == nil then log("Controller_Game - requestPermissionsAndCreatorMode() - event is nil."); return end
	local player = event.player
	if player == nil then log("Controller_Game - requestPermissionsAndCreatorMode() - event.player is nil."); return end
	if player.perm == nil then log("Controller_Game - requestPermissionsAndCreatorMode() - player.perm is nil."); return end
	if player.ID == nil then log("Controller_Game - requestPermissionsAndCreatorMode() - player.ID is nil."); return end
	
	-- Return the player's permission
	Events.sendClientEvent("FRAMEWORK_RETURN_PERM_AND_CREATOR_MODE", {perm = player.perm, creatorModeEnabled = (s_bIsCreatorModeEnabled == "true")}, player.ID)
end

-- Called when an energy item is consumed (i.e. a drink)
function onEnergyItemConsumed(self, event)
	if self == nil then log("Controller_Game - onEnergyItemConsumed() - self is nil."); return end
	if event == nil then log("Controller_Game - onEnergyItemConsumed() - event is nil."); return end
	local player = event.player
	if player == nil then log("Controller_Game - onEnergyItemConsumed() - event.player is nil."); return end
	if event.consumeValue == nil then log("Controller_Battle - onHealthItemConsumed() - event.consumeValue is nil."); return end
	if event.itemEffects == nil then log("Controller_Battle - onHealthItemConsumed() - event.itemEffects is nil."); return end
	
	-- Treat consumeValue as a percentage of player's total energy
	local energyDelta = player.maxEnergy * (event.consumeValue/100)
	-- Add energy
	if event.consumeValue > 0 then
		self:playerAddEnergy({user = player, energy = energyDelta})
	-- Remove energy
	else
		self:playerRemoveEnergy({user = player, energy = energyDelta})
	end
	-- Play animation, sound, and particle
	if event.itemEffects then
		self:energyConsumableEffects(player, event.itemEffects)
	end
end

-- Animation, emote, and sound for consuming an energy item
function energyConsumableEffects(self, player, itemEffects)
	-- 1: Animation
	local gender = player.gender  or "M"
	local emoteAnim = GameDefinitions.DEFAULT_PLAYER_ANIMS.EAT
	-- Get gendered emote
	if gender == "M" and itemEffects.emoteM and itemEffects.emoteM ~= 0 then
		emoteAnim = itemEffects.emoteM
	elseif gender == "F" and itemEffects.emoteF and itemEffects.emoteF ~= 0 then
		emoteAnim = itemEffects.emoteF
	end
	self:playerSetAnimation{user = player, animation = emoteAnim}

	-- 2: Emote -- Where does the particle attach?
	if itemEffects.effect and itemEffects.effect ~= 0 then
		local particle = itemEffects.effect -- take as input
		local bone = "Bip01 Head" -- take as input?
		local offset 		= {0, 0, 0}
		local forwardDir 	= {0, 0, 0}
		local upDir 		= {0, 0, 0}
		if bone == "Bip01 Head" then
			local offset 		= {2, 0, 0}
		end
		local attachKeyIndex = 0
		local attachKey = bone.." slotID "..tostring(attachKeyIndex)
		if player.particles then
			 -- Get number of particles
			local particleCount = 0
			for key,value in pairs(player.particles) do
				particleCount = particleCount + 1
			end
			-- If there are already particles using the intended index, update the index	
			local keyMatch = true
			for numericalIndex = attachKeyIndex, particleCount, 1 do
				if keyMatch == true then -- Do not check other numerical values unless a match is found
					attachKey = bone.." slotID "..tostring(numericalIndex) -- Test key using current numericalIndex
					keyMatch = false -- Keep this value for attachKey UNLESS a match is found among the particles
					for key,value in pairs(player.particles) do -- Loop through all particles
						if key == attachKey then -- If the key we want to use matches an existing key
							keyMatch = true -- Flag this as a key match
						end
					end
				end
			end
			if keyMatch == true then -- If, after checking against ALL existing keys, there are still no valid options
				attachKey = bone.." slotID "..tostring(particleCount + attachKeyIndex) -- Create a new incremented index
			end
		end
		self:playerSetParticle{
			user 			= player,
			particleKey 	= attachKey,
			boneName 		= bone,
			particleGLID 	= particle,
			offset 			= {x = offset[1], y = offset[2], z = offset[3]},
			forwardDir 		= {x = forwardDir[1], y = forwardDir[2], z = forwardDir[3]},
			upDir 			= {x = upDir[1], y = upDir[2], z = upDir[3]}
		}
	end

	-- Events.sendClientEvent("FRAMEWORK_EMOTE_OBJECT", {emoteState = true}, emotePlayer.ID)

	-- 3: Sound
	
	if itemEffects.soundGLID then
		local tSoundParams = GameDefinitions.generateGenericSoundParams(player.ID, itemEffects.soundGLID, 20, true)
		player.drinkSound = Behavior_Sound.generate(tSoundParams)
		Behavior_Sound.play({soundID = player.drinkSound})
	end
end

-- Returns the settings for the zone
function onSettingsRequested(self, event)
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Game - onSettingsRequested() event.player is nil."); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Game - onSettingsRequested() event.player.ID is nil."); return end
	
	local settings = {}

	if event.zoneID and event.zoneType then
		settings = SaveLoad.loadGameData("WorldSettings", event.zoneID, event.zoneType)
		if settings then
			settings = Events.decode(settings)
			Events.sendClientEvent("FRAMEWORK_RETURN_CHILD_SETTINGS", settings, playerID)
		end
	else
		for i, v in pairs(s_tSettings) do
			settings[i] = v
		end

		if settings.starveTime then
			m_starveTime = settings.starveTime * SECONDS_PER_MINUTE -- Convert from minutes to seconds
		end
		
		if settings.allowPvP == false or settings.destructibleObjects == false then
			local tPlayers = getPlayers()
			for sName, uPlayer in pairs(tPlayers) do
				uPlayer.pvpEnabled = checkFlagZoneSettings(true, false, sName, "pvp", 0)
				uPlayer.playerDestruction = checkFlagZoneSettings(true, false, sName, "playerDestruction", 0)
			end	
		end
		Events.sendClientEvent("FRAMEWORK_RETURN_SETTINGS", settings, playerID)
	end
end

-- Called when the settings are updated

function onSettingsUpdated(self, event)
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Game - onSettingsUpdated() event.player is nil."); return end
	local playerID = event.player.ID
	--if playerID == nil then log("Controller_Game - onSettingsUpdated() event.player.ID is nil."); return end
	if event.settings == nil then log("Controller_Game - onSettingsUpdated() event.settings is nil."); return end	

	local zoneID = tonumber(event.settings.zoneID)
	local zoneType = tonumber(event.settings.zoneType)
	local zID, zIndex, zType = kgp.gameGetCurrentInstance() --child ZoneInstanceID, ZoneIndex, ZoneType 
	if zoneID == nil then zoneID = zID end
	if zoneType == nil then zoneType = zType end
	
	if zID == zoneID then
		-- New battle settings or build settings
		if self:compareSettings(event.settings) then
			-- Inform Toolbar of players of the new settings
			Events.sendClientEvent("FRAMEWORK_RETURN_SETTINGS", event.settings)
		end
		-- New Jumpsuit settings. Notify Controller_Inventory
		if event.settings.jumpsuit ~= s_tSettings.jumpsuit then
			Events.sendEvent("FRAMEWORK_JUMPSUIT_MODIFIED", {jumpsuit = event.settings.jumpsuit})
		end

		if event.settings.respawnTimerOff ~= s_tSettings.respawnTimerOff then
			Events.sendEvent("FRAMEWORK_RESPAWN_TIMER_MODIFIED", {respawnTimerOff = event.settings.respawnTimerOff})
		end

		if event.settings.destructibleObjects ~= nil then s_tSettings.destructibleObjects = event.settings.destructibleObjects end
		if event.settings.allowPvP ~= nil 			 then s_tSettings.allowPvP = event.settings.allowPvP end
		if event.settings.playerBuild ~= nil 		 then s_tSettings.playerBuild = event.settings.playerBuild end
		if event.settings.playerNames ~= nil 		 then s_tSettings.playerNames = event.settings.playerNames end
		if event.settings.jumpsuit ~= nil 	 		 then s_tSettings.jumpsuit = event.settings.jumpsuit end
		if event.settings.starvation ~= nil		 	 then s_tSettings.starvation = event.settings.starvation end
		if event.settings.starveTime ~= nil		 	 then s_tSettings.starveTime = event.settings.starveTime
														  m_starveTime = s_tSettings.starveTime * SECONDS_PER_MINUTE -- convert from minutes to seconds
		end
		if event.settings.leaderboard ~= nil		 then s_tSettings.leaderboard = event.settings.leaderboard end
		if event.settings.dropItems ~= nil			 then s_tSettings.dropItems = event.settings.dropItems end
		if event.settings.lobbySpawnOnly ~= nil	 	 then s_tSettings.lobbySpawnOnly = event.settings.lobbySpawnOnly end
		if event.settings.respawnTimerOff ~= nil     then s_tSettings.respawnTimerOff = event.settings.respawnTimerOff end
		if event.settings.flagBuildOnly ~= nil       then s_tSettings.flagBuildOnly = event.settings.flagBuildOnly end
		if event.settings.maxFlags ~= nil     		 then s_tSettings.maxFlags = event.settings.maxFlags end
		if event.settings.maxFlagObjects ~= nil      then s_tSettings.maxFlagObjects = event.settings.maxFlagObjects end

		local tPlayers = getPlayers()
	
	--No need to check flag zone settings if updating settings for different world
		for sName, uPlayer in pairs(tPlayers) do
			uPlayer.playerDestruction = checkFlagZoneSettings(true, false, sName, "playerDestruction", 0)
			uPlayer.pvpEnabled = checkFlagZoneSettings(true, false, sName, "pvp", 0)
		end
		SaveLoad.saveGameData("WorldSettings", Events.encode(s_tSettings))
	elseif event.saved == nil then
		SaveLoad.saveGameData("WorldSettings", Events.encode(event.settings), zoneID, zoneType)
		Events.sendClientEvent("FRAMEWORK_RETURN_CHILD_SETTINGS", event.settings, playerID)
		event.saved = true
		Events.sendBroadcastEvent("FRAMEWORK_UPDATE_SETTINGS", event)
	end	
end

-- Returns the environment settings for the zone
function onEnvironmentRequested(self, event)
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Game - onEnvironmentRequested() event.player is nil."); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Game - onEnvironmentRequested() event.player.ID is nil."); return end
	
	-- Return world settings
	if event.zoneID and event.zoneType then
		local environment = SaveLoad.loadGameData("EnvironmentSettings", event.zoneID, event.zoneType)
		if environment then
			local envSettings = Events.decode(environment)
			Events.sendClientEvent("FRAMEWORK_RETURN_CHILD_ENVIRONMENT", envSettings, playerID)
		end
	else
		Events.sendClientEvent("FRAMEWORK_RETURN_ENVIRONMENT", s_tEnvironment, playerID)
	end
end

-- Called when the environment is updated
function onEnvironmentUpdated(self, event)
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Game - onEnvironmentUpdated() event.player is nil."); return end
	local playerID = event.player.ID
	--if playerID == nil then log("Controller_Game - onEnvironmentUpdated() event.player.ID is nil."); return end
	if event.environment == nil then log("Controller_Game - onEnvironmentUpdated() event.environment is nil."); return end		

	-- New environment settings
	local zID, zIndex, zType = kgp.gameGetCurrentInstance() --child ZoneInstanceID, ZoneIndex, ZoneType 
	if tonumber(zID) == tonumber(event.environment.zoneID) then
		for j, k in pairs(event.environment) do
			if k ~= s_tEnvironment[j] then
				for i, v in pairs(event.environment) do
					s_tEnvironment[i] = v
				end

				setEnvironmentSettings()

				-- Inform Toolbar of players' new environment
				Events.sendClientEvent("FRAMEWORK_RETURN_ENVIRONMENT", s_tEnvironment)
				break
			end
		end

		-- Cleanup Environment Settings for saving game data
		local environmentSettings = {}
		--environmentSettings.envEnabled			= s_tEnvironment.envEnabled
		environmentSettings.fogEnabled			= s_tEnvironment.fogEnabled
		environmentSettings.fogStartRange 		= s_tEnvironment.fogStartRange
		environmentSettings.fogEndRange 		= s_tEnvironment.fogEndRange
		environmentSettings.fogColor			= s_tEnvironment.fogColor
		environmentSettings.fogColorRed			= s_tEnvironment.fogColorRed
		environmentSettings.fogColorGreen		= s_tEnvironment.fogColorGreen
		environmentSettings.fogColorBlue		= s_tEnvironment.fogColorBlue
		--environmentSettings.ambientEnabled		= s_tEnvironment.ambientEnabled
		environmentSettings.ambientColor		= s_tEnvironment.ambientColor
		environmentSettings.ambientColorRed		= s_tEnvironment.ambientColorRed
		environmentSettings.ambientColorGreen 	= s_tEnvironment.ambientColorGreen
		environmentSettings.ambientColorBlue 	= s_tEnvironment.ambientColorBlue
		environmentSettings.sunColor			= s_tEnvironment.sunColor
		environmentSettings.sunColorRed			= s_tEnvironment.sunColorRed
		environmentSettings.sunColorGreen 		= s_tEnvironment.sunColorGreen
		environmentSettings.sunColorBlue 		= s_tEnvironment.sunColorBlue
		
		SaveLoad.saveGameData("EnvironmentSettings", Events.encode(environmentSettings))
	elseif event.saved == nil then
		SaveLoad.saveGameData("EnvironmentSettings", Events.encode(event.environment),  tonumber(event.environment.zoneID), tonumber(event.environment.zoneType))
		Events.sendClientEvent("FRAMEWORK_RETURN_CHILD_ENVIRONMENT", event.environment, playerID)

		event.saved = true
		Events.sendBroadcastEvent("FRAMEWORK_UPDATE_ENVIRONMENT", event)
	end
end

-- Returns the welcome settings for the zone
function onWelcomeRequested(self, event)
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Game - onEnvironmentRequested() event.player is nil."); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Game - onEnvironmentRequested() event.player.ID is nil."); return end
	
	-- Return world settings
	if event.zoneID and event.zoneType then
		local welcome = SaveLoad.loadGameData("WelcomeSettings", event.zoneID, event.zoneType)
		local welSettings = {}
		if welcome then
			welSettings = Events.decode(welcome)
		else
			welSettings.showMenu = false
			welSettings.showWorldName = true
			welSettings.welcomeImage = "None"
			welSettings.welcomeText = ""
		end
		Events.sendClientEvent("FRAMEWORK_RETURN_CHILD_WELCOME", welSettings, playerID)
	else
		Events.sendClientEvent("FRAMEWORK_RETURN_WELCOME", s_tWelcome, playerID)
	end
end

-- Called when the welcome setting is updated
function onWelcomeUpdated(self, event)
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Game - onWelcomeUpdate() event.player is nil."); return end
	local playerID = event.player.ID
	--if playerID == nil then log("Controller_Game - onWelcomeUpdated() event.player.ID is nil."); return end
	if event.welcome == nil then log("Controller_Game - onWelcomeUpdated() event.welcome is nil."); return end		

	local zID, zIndex, zType = kgp.gameGetCurrentInstance() --child ZoneInstanceID, ZoneIndex, ZoneType 
	local zoneID = tonumber(event.welcome.zoneID)
	local zoneType = tonumber(event.welcome.zoneType)
	if zID == zoneID then
		for j, k in pairs(event.welcome) do
			if k ~= s_tWelcome[j] then
				for i, v in pairs(event.welcome) do
					s_tWelcome[i] = v
				end


				for sName, uPlayer in pairs(s_uPlayers) do
					if uPlayer.perm < PLAYER_PERMISSION then
						Events.sendClientEvent("FRAMEWORK_RETURN_WELCOME", s_tWelcome, uPlayer.ID)
					end
				end
				break
			end
		end

		-- Cleanup Welcome Settings for saving game data
		local welcomeSettings = {}
		--environmentSettings.envEnabled			= s_tEnvironment.envEnabled
		welcomeSettings.showMenu				= s_tWelcome.showMenu
		welcomeSettings.showWorldName 			= s_tWelcome.showWorldName
		welcomeSettings.welcomeImage			= s_tWelcome.welcomeImage
		welcomeSettings.welcomeText				= s_tWelcome.welcomeText
		
		SaveLoad.saveGameData("WelcomeSettings", Events.encode(welcomeSettings))
	elseif event.saved == nil then
		SaveLoad.saveGameData("WelcomeSettings", Events.encode(event.welcome), zoneID, zoneType)
		Events.sendClientEvent("FRAMEWORK_RETURN_CHILD_WELCOME", event.welcome, playerID)

		event.saved = true
		Events.sendBroadcastEvent("FRAMEWORK_UPDATE_WELCOME", event)
	end
end

-- Returns the welcome settings for the zone
function onOverviewRequested(self, event)
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Game - onOverviewRequested() event.player is nil."); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Game - onOverviewRequested() event.player.ID is nil."); return end
	
	Events.sendClientEvent("FRAMEWORK_RETURN_OVERVIEW", s_tOverview, playerID)
end

-- Called when the overview setting is updated
function onOverviewUpdated(self, event)
	if event.overview == nil then log("Controller_Game - onOverviewUpdated() event.overview is nil."); return end		

	local valuesUpdated = false
	for index, value in pairs(event.overview) do
		if value ~= s_tOverview[index] then
			s_tOverview[index] = value
			valuesUpdated = true
		end
	end

	--Only need to send to client and broadcast if something was updated
	if valuesUpdated then
		Events.sendClientEvent("FRAMEWORK_RETURN_OVERVIEW", s_tOverview)

		if event.saved == nil then

			local overviewSettings = {}

			overviewSettings.overviewEnabled			= s_tOverview.overviewEnabled
			overviewSettings.overviewImage				= s_tOverview.overviewImage
			overviewSettings.markers					= s_tOverview.markers

			local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
			SaveLoad.saveGameData("OverviewSettings", Events.encode(overviewSettings), parentID, parentType)
			event.saved = true
			Events.sendBroadcastEvent("FRAMEWORK_UPDATE_OVERVIEW", event)
		end
	end
end

--Return System Flags
function onRequestFlags(self, event)
	Events.sendClientEvent("FRAMEWORK_RETURN_FLAGS_FULL", {flags = s_tFlags}, event.player.ID)
	if s_bPendingFlags then
		Events.sendEvent("RETURN_FLAGS_FULL", {flags = s_tFlags})
		s_bPendingFlags = false
	end
end

function onRequestMediaObjects(self, event)
	Events.sendClientEvent("FRAMEWORK_RETURN_MEDIA_OBJECTS_FULL", {media = s_tMedia}, event.player.ID)
	s_bIsPendingMedia = false
end

function onRequestEventFlags(self, event)
	Events.sendClientEvent("FRAMEWORK_RETURN_EVENT_FLAGS_FULL", {eventFlags =  Toolbox.deepCopy(s_tEvents)}, event.player.ID)
end

--set the event flag so controller know its occuring
function onEventFlagOccurring(self, event)
	if event == nil or event.PID == nil or s_uItemInstances[event.PID] == nil then
		log("ERROR DETECTED - ED-6360 : onEventFlagOccurring called : event = "..tostring(event)..", event.PID = "..tostring(event and event.PID)..", s_uItemInstances[event.PID] = nil")
		return
	end
	--s_uItemInstances[event.PID].status = "Occurring"
	s_tEvents[tostring(event.PID)] =  EVENT_OCCURRING
	Events.sendEvent("EVENT_FLAG_STATUS_UPDATED", {PID = event.PID})

	if event.teleport == "true" then
		for sName, uPlayer in pairs(s_uPlayers) do
			if uPlayer and uPlayer.inZone then
				Events.sendEvent("PLAYER_EVENT_INVITE", {player = uPlayer})
				self:spawnPlayerAtEventFlag(uPlayer)
			end
		end
	else
		for sName, uPlayer in pairs(s_uPlayers) do
			if uPlayer and uPlayer.inZone then
				Events.sendEvent("PLAYER_EVENT_INVITE", {player = uPlayer})
			end
		end
	end
end

--decline event
function declineEventHandler(self, event)
	if event.player == nil then log("Controller_Game - declineEventHandler() event.player is nil.") return end
	if event.player.name == nil then log("Controller_Game - declineEventHandler() event.player.name is nil.") return end
	if s_tInitialPlayerPositions[event.player.name] then
		self:spawnPlayer({user = event.player})
	end
end

function acceptEventHandler(self, event)
	if event.player == nil then log("Controller_Game - acceptEventHandler() event.player is nil.") return end

	self:spawnPlayerAtEventFlag(event.player)
	s_tInitialPlayerPositions[event.player.name] = nil
end

function returnDeathCount(self, event)
	local uPlayer = event.player
	if uPlayer == nil then log("Controller_Game - returnDeathCount() event.player is nil.") return end

	local tempDeathCount = DEF_DEATH_COUNT
	if uPlayer and uPlayer.deathCount then
		tempDeathCount = uPlayer.deathCount or 1
	end
	Events.sendClientEvent("RETURN_DEATH_COUNT", {deathCount = tempDeathCount}, uPlayer.ID)
end

function incrementDeathCount(self, event)
	local uPlayer = event.player
	if uPlayer == nil then log("Controller_Game - incrementDeathCount() event.player is nil.") return end
	
	if uPlayer and uPlayer.deathCount then
		uPlayer.deathCount = uPlayer.deathCount + 1 -- Update value for next time it is requested
	end
end

function onReqFollowerCount(self, event)
	local uPlayer = event.player
	if uPlayer == nil then log("Controller_Game - onReqFollowerCount() event.player is nil.") return end

	local tempCount = 0
	if uPlayer.followerCount then
		tempCount = uPlayer.followerCount or DEF_FOLLOW_COUNT
	end

	Events.sendClientEvent("FRAMEWORK_RETURN_FOLLOWER_COUNT", {followerCount = tempCount}, uPlayer.ID)
end

function updateFollowerCount(self, event)
	local uPlayer = event.player
	local sPlayerName = event.playerName
	local ichangeAmt = event.changeAmt
	if uPlayer == nil and sPlayerName == nil then log("Controller_Game - updateFollowerCount() event.player or event.playerName is nil.") return end
	if ichangeAmt == nil then log("Controller_Game - updateFollowerCount() event.changeAmt is nil.") return end
	
	if sPlayerName then
		uPlayer = s_uPlayers[tostring(sPlayerName)]
	end

	if uPlayer then
		if uPlayer.followerCount then
			uPlayer.followerCount = uPlayer.followerCount + ichangeAmt
		else
			uPlayer.followerCount = ichangeAmt
		end
	end
end

function onReturnNpcFollowerExists(self, event)
	local iFollower = event.PID
	local sPlayerName = event.playerName
	if iFollower == nil then log("Controller_Game - onReturnNpcFollowerExists() event.iFollower is nil.") return end
	if sPlayerName == nil then log("Controller_Game - onReturnNpcFollowerExists() event.playerName is nil.") return end

	local uPlayer = s_uPlayers[tostring(sPlayerName)]
	if uPlayer and uPlayer.followerCount then
		uPlayer.followerCount = uPlayer.followerCount + 1
	else
		uPlayer.followerCount = 1
	end
end

function onReqNpcSpouse(self, event)
	local uPlayer = event.player
	if uPlayer == nil then log("Controller_Game - onReqNpcSpouse() event.player is nil.") return end

	local tempSpouse = DEF_NPC_SPOUSE
	if uPlayer.npcSpouse and uPlayer.npcSpouse ~= DEF_NPC_SPOUSE then
		tempSpouse = uPlayer.npcSpouse
	end

	Events.sendClientEvent("FRAMEWORK_RETURN_NPC_SPOUSE", {npcSpouse = tempSpouse}, uPlayer.ID)
end

function updateNpcSpouse(self, event)
	local uPlayer = event.player
	local iSpouse = event.PID
	if uPlayer == nil then log("Controller_Game - updateNpcSpouse() event.player is nil.") return end
	if iSpouse == nil then log("Controller_Game - updateNpcSpouse() event.PID is nil.") return end
	
	uPlayer.npcSpouse = iSpouse
end

function onReturnNpcSpouseExists(self, event)
	local iSpouse = event.PID
	local sPlayerName = event.playerName
	if iSpouse == nil then log("Controller_Game - onReturnNpcSpouseExists() event.iSpouse is nil.") return end
	if sPlayerName == nil then log("Controller_Game - onReturnNpcSpouseExists() event.playerName is nil.") return end
	
	local uPlayer = s_uPlayers[tostring(sPlayerName)]
	if uPlayer then
		uPlayer.npcSpouse = iSpouse
	end
end

function resetNpcSpouse(self, event)
	local sPlayerName = event.playerName
	if sPlayerName == nil then log("Controller_Game - resetNpcSpouse() event.playerName is nil.") return end
	
	local uPlayer = s_uPlayers[tostring(sPlayerName)]
	if uPlayer then
		uPlayer.npcSpouse = DEF_NPC_SPOUSE
	end
end

-- Register New System  Object
function onRegisterSystemObject(self, event)
	if event.objectType and event.info then
		local PID = tostring(event.info.PID)
		if event.objectType == "flag" then
			s_tFlags[PID] = event.info
			if s_bPendingFlags then return end
			Events.sendEvent("RETURN_FLAG", {flag = event.info})
			Events.sendClientEvent("FRAMEWORK_RETURN_FLAG", {flag = event.info})
		elseif event.objectType == "media" then
			s_tMedia[PID] = event.info
			if s_bIsPendingMedia then return end
			Events.sendClientEvent("FRAMEWORK_RETURN_MEDIA_OBJECT", {mediaObject =  s_tMedia[PID]})
		elseif event.objectType == "event" and s_tEventsReturned then
			
			if not s_tEvents[PID] then
				s_tEvents[PID] = EVENT_SCHEDULED
				Events.sendClientEvent("FRAMEWORK_RETURN_EVENT_FLAG", {PID = event.info.PID})
			end
		end
	end
end

registerSytemObjects = function(itemTable, event)
	local oldItem = false
	for i, v in ipairs(itemTable) do
		if(v.PID == event.info.PID) then
			oldItem = true
			itemTable[i] = event.info
			break
		end
	end

	if not oldItem then
		table.insert(itemTable, event.info)
	end
	return itemTable
end
	

-- Unregister System Flag
function onUnregisterSystemObject(self, event)

	if event.PID and event.objectType then
		local PID = tostring(event.PID)
		if event.objectType == "flag" then
			if s_tFlags[PID] then 
				s_tFlags[PID] = nil
			end
			if s_bPendingFlags then return end
				Events.sendEvent("REMOVE_FLAG", {PID = PID})
				Events.sendClientEvent("FRAMEWORK_REMOVE_FLAG", {PID = PID})
		elseif event.objectType == "media" then
			if s_tMedia[PID] then 
				s_tMedia[PID] = nil
			end
			if s_bIsPendingMedia then return end
			Events.sendClientEvent("FRAMEWORK_REMOVE_MEDIA_OBJECT", {PID = PID})
		elseif event.objectType == "event" then
			if s_tEvents[tostring(event.PID)] then
				s_tEvents[tostring(event.PID)] = nil
				Events.sendClientEvent("FRAMEWORK_REMOVE_EVENT_FLAG", {PID = PID})
			end
		end
	end
end

unRegisterSystemObjects = function(itemTable, event)
	for i, v in ipairs(itemTable) do
		if event.PID then
			if(v.PID == event.PID) then
				table.remove(itemTable, i)
				break
			end
		end
	end
	
	return itemTable
end

-- Check whether a player is a member of a flag
-- CJW, NOTE: This could be made more efficient by converting s_tFlags and flag.members into bag structures...
isPlayerClaimFlagMember = function(playerName, flagPID)
	for i, flag in pairs(s_tFlags) do
		if(flag.PID == flagPID) then
			for i, memberName in pairs(flag.members) do
				if memberName == playerName then
					return true
				end
			end
		end
	end
	
	return false
end

-- Called when a player enters a flag's trigger
function onFlagTrigger(self, event)
	-- Was a valid player passed in?
	local uPlayer = event.player
	if uPlayer == nil then log("Controller_Game - onFlagTrigger() event.player is nil."); return end
	if event.triggerType == nil then log("Controller_Game - onFlagTrigger() event.triggerType is nil."); return end
	if event.flagInfo.PID == nil then log("Controller_Game - onFlagTrigger() event.flagType is nil."); return end
	if event.flagInfo.flagType == nil then log("Controller_Game - onFlagTrigger() event.flagType is nil."); return end
	if event.flagInfo.enabled == nil then log("Controller_Game - onFlagTrigger() event.enabled is nil."); return end

	local playerName = uPlayer.name
	
	if playerName == nil then
		log("ERROR DETECTED - ED-6931 : onFlagTrigger player name was nil for player with ID = "..tostring(uPlayer and uPlayer.ID))
		return
	end
	
	local inFlag = event.triggerType == 1
	local playerSetting
	if event.flagInfo.enabled and inFlag then -- If entering a true flag, then always true
		playerSetting = true
	else
		if inFlag then playerSetting = event.flagInfo.enabled else playerSetting = not event.flagInfo.enabled end
		--playerSetting = checkFlagZoneSettings(playerSetting, inFlag, playerName, event.flagInfo.flagType, event.flagInfo.PID)
	end

	if event.flagInfo.flagType == "pvp" then
		if playerSetting then
			if uPlayer.pvpEnabled == false then
				newSetting = checkFlagZoneSettings(true, inFlag, playerName, "pvp", event.flagInfo.PID)
				if newSetting == true then
					uPlayer.pvpEnabled = true
					Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = playerName, modifiedFields = {pvpEnabled = true}})
					kgp.playerShowStatusInfo(uPlayer.ID, "Player Combat Area.", 3, 0, 255, 0)
					Events.sendClientEvent("FRAMEWORK_CHANGE_STATUS_ICON", {player = playerName, status = "PVP", enabled = true}, uPlayer.ID)
				end
			end
		else
			if uPlayer.pvpEnabled == true then
				newSetting = checkFlagZoneSettings(true, inFlag, playerName, "pvp", event.flagInfo.PID)
				if newSetting == false then
					uPlayer.pvpEnabled = false
					Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = playerName, modifiedFields = {pvpEnabled = false}})
					kgp.playerShowStatusInfo(uPlayer.ID, "No Player Combat Area.", 3, 255, 0, 0)
					Events.sendClientEvent("FRAMEWORK_CHANGE_STATUS_ICON", {player = playerName, status = "PVP", enabled = false}, uPlayer.ID)
				end
			end
		end
	elseif event.flagInfo.flagType == "playerBuild" then
		if playerSetting then
			if uPlayer.playerBuild == false then
				newSetting = checkFlagZoneSettings(true, inFlag, playerName, "playerBuild", event.flagInfo.PID)
				if newSetting == true then
					uPlayer.playerBuild = true
					kgp.playerShowStatusInfo(uPlayer.ID, "Player Building Area.", 3, 0, 255, 0)
					Events.sendClientEvent("FRAMEWORK_CHANGE_STATUS_ICON", {player = playerName, status = "Build", enabled = true}, uPlayer.ID)
					Events.sendClientEvent("FRAMEWORK_BUILD_STATUS_UPDATE", {buildFlag = true}, uPlayer.ID)
				end
			end
		else
			if uPlayer.playerBuild == true then
				newSetting = checkFlagZoneSettings(true, inFlag, playerName, "playerBuild", event.flagInfo.PID)
				if newSetting == false then
					uPlayer.playerBuild = false
					kgp.playerShowStatusInfo(uPlayer.ID, "No Player Building Area.", 3, 255, 0, 0)
					Events.sendClientEvent("FRAMEWORK_CHANGE_STATUS_ICON", {player = playerName, status = "Build", enabled = false}, uPlayer.ID)
					Events.sendClientEvent("FRAMEWORK_BUILD_STATUS_UPDATE", {buildFlag = false}, uPlayer.ID)
				end
			end
		end
	elseif event.flagInfo.flagType == "playerDestruction" then
		if playerSetting then
			if uPlayer.playerDestruction == false then
				newSetting = checkFlagZoneSettings(true, inFlag, playerName, "playerDestruction", event.flagInfo.PID)
				if newSetting == true then
					uPlayer.playerDestruction = true
					Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = playerName, modifiedFields = {playerDestruction = true}})
					kgp.playerShowStatusInfo(uPlayer.ID, "Player Destruction Area.", 3, 0, 255, 0)
					Events.sendClientEvent("FRAMEWORK_CHANGE_STATUS_ICON", {player = playerName, status = "Destruction", enabled = true}, uPlayer.ID)
				end
			end
		else
			if uPlayer.playerDestruction == true then
				newSetting = checkFlagZoneSettings(true, inFlag, playerName, "playerDestruction", event.flagInfo.PID)
				if newSetting == false or inFlag == false then
					uPlayer.playerDestruction = false
					Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = playerName, modifiedFields = {playerDestruction = false}})
					kgp.playerShowStatusInfo(uPlayer.ID, "No Player Destruction Area.", 3, 255, 0, 0)
					Events.sendClientEvent("FRAMEWORK_CHANGE_STATUS_ICON", {player = playerName, status = "Destruction", enabled = false}, uPlayer.ID)
				end
			end
		end
	elseif event.flagInfo.flagType == "claimFlag" then

		if playerSetting then
			Events.sendClientEvent("FRAMEWORK_CLAIM_FLAG_STATUS_UPDATE", {buildFlag = true, flagPID = event.flagInfo.PID}, uPlayer.ID)
			local isFlagOwner = event.flagInfo.owner
			local isFlagMember = isFlagOwner or isPlayerClaimFlagMember(uPlayer.name, event.flagInfo.PID)
			if isFlagMember then
				kgp.playerShowStatusInfo(uPlayer.ID, "Entered claimed Land!", 3, 0, 255, 0)
			else
				
				uPlayer.playerBuild = false
				kgp.playerShowStatusInfo(uPlayer.ID, "Player Build Disabled. In claimed land", 3, 255, 0, 0) 
				Events.sendClientEvent("FRAMEWORK_CHANGE_STATUS_ICON", {player = playerName, status = "Build", enabled = false}, uPlayer.ID)
			end
			if event.flagInfo.noDestruct and event.flagInfo.noDestruct ~= false and event.flagInfo.noDestruct ~= "false" then
				if uPlayer.playerDestruction == true then
					uPlayer.playerDestruction = false
					Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = playerName, modifiedFields = {playerDestruction = false}})
					kgp.playerShowStatusInfo(uPlayer.ID, "Player Destruction Disabled.", 3, 255, 0, 0)
					Events.sendClientEvent("FRAMEWORK_CHANGE_STATUS_ICON", {player = playerName, status = "Destruction", enabled = false}, uPlayer.ID)
				end
			end
		else
			Events.sendClientEvent("FRAMEWORK_CLAIM_FLAG_STATUS_UPDATE", {buildFlag = false, flagPID = event.flagInfo.PID}, uPlayer.ID)
			if event.flagInfo.owner then
				kgp.playerShowStatusInfo(uPlayer.ID, "Left claimed Land", 3, 255, 0, 0)
			else
					uPlayer.playerBuild = true
					kgp.playerShowStatusInfo(uPlayer.ID, "Player Build Enabled. Left claimed land", 3, 0, 255, 0)
					Events.sendClientEvent("FRAMEWORK_CHANGE_STATUS_ICON", {player = playerName, status = "Build", enabled = true}, uPlayer.ID)
			end

			if event.flagInfo.noDestruct and event.flagInfo.noDestruct ~= false and event.flagInfo.noDestruct ~= "false"  and uPlayer.playerDestruction == false then
				if uPlayer.playerDestruction == false then
					newSetting = checkFlagZoneSettings(true, inFlag, playerName, "playerDestruction", event.flagInfo.PID)
					if newSetting == true then
						uPlayer.playerDestruction = true
						Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = playerName, modifiedFields = {playerDestruction = true}})
						kgp.playerShowStatusInfo(uPlayer.ID, "Player Destruction Area.", 3, 0, 255, 0)
						Events.sendClientEvent("FRAMEWORK_CHANGE_STATUS_ICON", {player = playerName, status = "Destruction", enabled = true}, uPlayer.ID)
					end
				end
			end
		end
	end
end

function returnFlagZoneSettings(self, event)
	local playerName = event.player.name
	
	if playerName == nil then
		log("ERROR DETECTED - ED-6931 : onFlagTrigger player name was nil for player with ID = "..tostring(uPlayer and uPlayer.ID))
		return
	end
	
	local tEventData = {
		PVP 			= checkFlagZoneSettings(true, false, playerName, "pvp", 0), 
		build 			= checkFlagZoneSettings(true, false, playerName, "playerBuild", 0),
		destruction 	= checkFlagZoneSettings(true, false, playerName, "playerDestruction", 0)
	}
	
	Events.sendClientEvent("FRAMEWORK_RETURN_FLAG_ZONE_SETTINGS", tEventData, event.player.ID)
end

function onFlagFeeNotifReqList(self, event)
	if self == nil then log("Controller_Game - onFlagFeeNotifReq() self is nil."); return end
	if event == nil then log("Controller_Game - onFlagFeeNotifReq() event is nil."); return end
	if event.players == nil then log("Controller_Game - onFlagFeeNotifReq() event.players is nil."); return end
	if event.message == nil then log("Controller_Game - onFlagFeeNotifReq() event.message is nil."); return end

	local playersToMessage = {}
	for index, playerToMessage in pairs(event.players) do
		local msg = event.message
		local tempPlayer = Players.determineUser(playerToMessage, s_uPlayers)
		if tempPlayer and tempPlayer.ID then
			Events.sendClientEvent("FRAMEWORK_REQ_FLAG_FEE_NOTIF", {message = event.message}, tempPlayer.ID)
		end
	end
end

function onFlagDeleteItems(self, event)
	if self == nil then log("Controller_Game - onFlagDeleteItems() self is nil."); return end
	if event == nil then log("Controller_Game - onFlagDeleteItems() event is nil."); return end
	if event.PID == nil then log("Controller_Game - onFlagDeleteItems() event.PID is nil."); return end
	local flagPID = event.PID
	local tempFlag = nil
	for index, flag in pairs(s_tFlags) do
		if flag.flagType == "claimFlag" and flag.PID == flagPID then
			tempFlag = flag
			if tempFlag then
				for PID, uItem in pairs(s_uItemInstances) do
					if uItem and uItem.UNID then
						local gameItem =  s_uItemProperties[tostring(uItem.UNID)]
						if gameItem and gameItem.itemType and gameItem.behavior and ((gameItem.itemType == GameDefinitions.ITEM_TYPES.PLACEABLE and gameItem.behavior ~= "wheeled_vehicle") or (gameItem.behavior == "seed")) then
							local pos = uItem.position
							local itemWithinFlag = isPointInBoundary(tempFlag, pos.x, pos.y, pos.z)
							if itemWithinFlag and uItem.PID ~= flagPID then
								-- delete it
								kgp.gameItemDeletePlacement(uItem.PID)
								Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = uItem.PID})
							end
						end
					end
				end
			end
			break
		end
	end
end

-- Set Creator boost mode
function setCreatorBoost(self, event)
	-- Ensure that this user is in Creator Mode
	if event.player == nil then log("Controller_Game - setCreatorBoost() event.player is nil."); return end
	
	-- Attaching / un-boosting Jet Pack
	if event.boost == "Active" then
		self:playerSetPhysics({user = event.player, curTransY = 0,   maxSpeed = 0.6, minSpeed = -0.6, antiGravity = 2, collisionEnabled = 0})
	-- Boosting with Jet Pack on
	elseif event.boost == "Boost" then
		self:playerSetPhysics({user = event.player, curTransY = 0.6, maxSpeed = 0.6, minSpeed = -0.6, collisionEnabled = 0})
	-- Holding Shift with Jet Pack on
	elseif event.boost == "Fall" then
		self:playerSetPhysics({user = event.player, antiGravity = 0, curTransY = 0,  maxSpeed = 0.6, minSpeed = -0.6, collisionEnabled = 0})
	-- Removing the Jet Pack
	elseif event.boost == "Off" then
		self:playerSetPhysics({user = event.player, reset = true, collisionEnabled = 1})
	end
end

---------------------------
-- Player APIs
---------------------------

-- Request for player physics access
function playerSetPhysics(self, event)
	if self == nil then log("Controller_Game - playerSetPhysics() self is nil."); return end
	if event == nil then log("Controller_Game - playerSetPhysics() event is nil."); return end
	if event.user == nil then log("Controller_Game - playerSetPhysics() event.user is nil."); return end
	
	local user = Players.determineUser(event.user, s_uPlayers)
	-- Valid player passed?
	if user then
		local physics = {}
		-- Set default physics values
		if event.reset then
			physics[pPhysics.MAXSPEED] 			   = DEF_PHYSICS.MAXSPEED
			physics[pPhysics.MINSPEED] 			   = DEF_PHYSICS.MINSPEED
			physics[pPhysics.ACCELLERATE] 		   = DEF_PHYSICS.ACCELLERATE
			physics[pPhysics.DECELLERATE] 		   = DEF_PHYSICS.DECELLERATE
			physics[pPhysics.CURTRANSY] 		   = DEF_PHYSICS.CURTRANSY
			physics[pPhysics.ROTATIONDRAGCONSTANT] = DEF_PHYSICS.ROTATIONDRAGCONSTANT
			physics[pPhysics.ROTATIONALGROUNDDRAG] = DEF_PHYSICS.ROTATIONALGROUNDDRAG
			physics[pPhysics.JUMPPOWER] 		   = DEF_PHYSICS.JUMPPOWER
			physics[pPhysics.GROUNDDRAG] 		   = DEF_PHYSICS.GROUNDDRAG
			physics[pPhysics.STRAFEREDUCTION] 	   = DEF_PHYSICS.STRAFEREDUCTION
			physics[pPhysics.CROSSSECTIONALAREA]   = DEF_PHYSICS.CROSSSECTIONALAREA
			physics[pPhysics.AIRCONTROL] 		   = DEF_PHYSICS.AIRCONTROL
			physics[pPhysics.JUMPRECOVERTIME] 	   = DEF_PHYSICS.JUMPRECOVERTIME
			physics[pPhysics.ANTIGRAVITY] 	   	   = 0
			physics[pPhysics.COLLISIONENABLED] 	   = 1
		
		elseif event.lock then
			physics[pPhysics.MAXSPEED] 			   = 0
			physics[pPhysics.MINSPEED] 			   = 0
			physics[pPhysics.CURTRANSY] 		   = 0
			physics[pPhysics.ROTATIONDRAGCONSTANT] = 0
			physics[pPhysics.JUMPPOWER] 		   = 0
		-- Set custom physics values
		else
			physics[pPhysics.MAXSPEED] 			   = event.maxSpeed				or user.physics.maxSpeed
			physics[pPhysics.MINSPEED] 			   = event.minSpeed				or user.physics.minSpeed
			physics[pPhysics.ACCELLERATE] 		   = event.accellerate			or user.physics.accellerate
			physics[pPhysics.DECELLERATE] 		   = event.decellerate			or user.physics.decellerate
			physics[pPhysics.CURTRANSY] 		   = event.curTransY 			or user.physics.curTransY
			physics[pPhysics.ROTATIONDRAGCONSTANT] = event.rotationDragConstant or user.physics.rotationDragConstant
			physics[pPhysics.ROTATIONALGROUNDDRAG] = event.rotationalGroundDrag or user.physics.rotationalGroundDrag
			physics[pPhysics.JUMPPOWER] 		   = event.jumpPower 		    or user.physics.jumpPower
			physics[pPhysics.GROUNDDRAG] 		   = event.groundDrag 		    or user.physics.groundDrag
			physics[pPhysics.STRAFEREDUCTION] 	   = event.strafeReduction 	    or user.physics.strafeReduction
			physics[pPhysics.CROSSSECTIONALAREA]   = event.crossSectionalArea   or user.physics.crossSectionalArea
			physics[pPhysics.AIRCONTROL] 		   = event.airControl 		    or user.physics.airControl
			physics[pPhysics.JUMPRECOVERTIME] 	   = event.jumpRecoverTime 	    or user.physics.jumpRecoverTime
			physics[pPhysics.COLLISIONENABLED] 	   = event.collisionEnabled 	or user.physics.collisionEnabled
			if event.curRotY 	 then physics[pPhysics.CURROTY] 	= event.curRotY 	end
			if event.curRotZ 	 then physics[pPhysics.CURROTZ] 	= event.curRotZ 	end
			if event.curTransX 	 then physics[pPhysics.CURTRANSX] 	= event.curTransX 	end
			if event.curTransZ 	 then physics[pPhysics.CURTRANSZ] 	= event.curTransZ 	end
			if event.burstForceY then physics[pPhysics.BURSTFORCEY] = event.burstForceY end
			if event.antiGravity then physics[pPhysics.ANTIGRAVITY] = event.antiGravity end
		end
		-- Set player's values to assigned physics values
		user.physics.maxSpeed 			 	= physics[pPhysics.MAXSPEED]
		user.physics.minSpeed 			  	= physics[pPhysics.MINSPEED]
		user.physics.accellerate 			= physics[pPhysics.ACCELLERATE]
		user.physics.decellerate 			= physics[pPhysics.DECELLERATE]
		user.physics.curTransY			  	= physics[pPhysics.CURTRANSY]
		user.physics.rotationDragConstant	= physics[pPhysics.ROTATIONDRAGCONSTANT]
		user.physics.rotationalDragConstant = physics[pPhysics.ROTATIONALDRAGCONSTANT]
		user.physics.jumpPower 			 	= physics[pPhysics.JUMPPOWER]
		user.physics.groundDrag 			= physics[pPhysics.GROUNDDRAG]
		user.physics.strafeReduction 		= physics[pPhysics.STRAFEREDUCTION]
		user.physics.crossSectionalArea 	= physics[pPhysics.CROSSSECTIONALAREA]
		user.physics.airControl 			= physics[pPhysics.AIRCONTROL]
		user.physics.jumpRecoverTime 		= physics[pPhysics.JUMPRECOVERTIME]
		user.physics.antiGravity 			= physics[pPhysics.ANTIGRAVITY]
		user.physics.collisionEnabled 		= physics[pPhysics.collisionEnabled]

		kgp.playerSetPhysics(user.ID, physics)
	end
end

-- Randomly spawns player
function spawnPlayer(self, event)
	-- Spawn's returning players who have declined Event invite to initial position they should have spanwed
	local user = event.user
	local playerName = user.name

	if s_tSettings.lobbySpawnOnly then
		s_tInitialPlayerPositions[playerName] = nil
	end
	
	local tPlayerInitialPos = s_tInitialPlayerPositions[playerName]

	if tPlayerInitialPos  then
		playerTeleport(self, {user = user, x = tPlayerInitialPos.x, y = tPlayerInitialPos.y, z = tPlayerInitialPos.z,
							  rx =  tPlayerInitialPos.rx, ry =  tPlayerInitialPos.ry, rz = tPlayerInitialPos.rz})
		s_tInitialPlayerPositions[playerName] = nil
		return
	end

	local spawnPoints = {}
	local spawnPointsNew = {}
	for PID, uItem in pairs(s_uItemInstances) do
		if uItem and uItem.type == "Spawn Point" then
			if uItem.newPlayer == false then
				table.insert(spawnPoints, uItem)
			else
				table.insert(spawnPointsNew, uItem)
			end
		end
	end

	-- Spawn at a spawn point if they exist and you're either not a new player and always spawn at lobby setting is false or a new player and there's no lobby spawners
	if self:canSpawnToPlayerSpawner(spawnPoints, spawnPointsNew, user) then
		local spawnPos = spawnPoints[math.random(1, #(spawnPoints))].position
		playerTeleport(self, {user = event.user, x = spawnPos.x, y = (spawnPos.y + RESPAWN_Y_BUFFER), z = spawnPos.z,
							  rx = spawnPos.rx, ry = spawnPos.ry, rz = spawnPos.rz})
	-- Otherwise use lobby spawn points if they exist
	elseif #spawnPointsNew ~= 0 then
		local spawnPos = spawnPointsNew[math.random(1, #(spawnPointsNew))].position
		playerTeleport(self, {user = user, x = spawnPos.x, y = (spawnPos.y + RESPAWN_Y_BUFFER), z = spawnPos.z,
							  rx = spawnPos.rx, ry = spawnPos.ry, rz = spawnPos.rz})
	-- Otherwise use default spawn point
	elseif s_tDefaultSpawnPoint and s_tDefaultSpawnPoint.x and s_tDefaultSpawnPoint.y and s_tDefaultSpawnPoint.z and s_tDefaultSpawnPoint.r then
		local rot = {}
		rot.rx, rot.rz = Math.degreesToVector(s_tDefaultSpawnPoint.r)
		playerTeleport(self, {user = user, x = s_tDefaultSpawnPoint.x, y = (s_tDefaultSpawnPoint.y + RESPAWN_Y_BUFFER), z = s_tDefaultSpawnPoint.z,
							  rx = rot.rz, ry = 0, rz = rot.rx})
	-- OTHERWISE queue this player for a spawn once the server has acquired the default zone spawn point
	else
		s_tRespawnQueue[event.user] = true
	end

	
    if user.newPlayer then

    	local zonesVisited = nil
		if ZoneInfo.isLinkedZone and event.user.zonesVisited then
			zonesVisited = Toolbox.deepCopy(event.user.zonesVisited)
		end
		-- Save off stats for this player
		local playerStats = {
			energy		= event.user.energy or DEF_MAX_ENERGY, 
			maxEnergy	= event.user.maxEnergy or DEF_MAX_ENERGY,
			health		= event.user.health or DEF_MAX_HEALTH, 
			maxHealth	= event.user.maxHealth or DEF_MAX_HEALTH,
			deathCount	= event.user.deathCount or DEF_DEATH_COUNT,
			npcSpouse 	= DEF_NPC_SPOUSE, -- Wait to update this until the spouse is confirmed
			followerCount = DEF_FOLLOW_COUNT, -- Wait to update this until the # of followers is confirmed
			zonesVisited = zonesVisited

		}
		SaveLoad.savePlayerData(user.name, "playerStats", Events.encode(playerStats))
		user.newPlayer = false
    end
end

function spawnPlayerAtEventFlag( self, player )
	if not player.ID then return end
	for i, v in pairs(s_tEvents) do
		if s_uItemInstances[i] and v == EVENT_OCCURRING then
			local eventFlag = s_uItemInstances[i]
			local Px, Py, Pz, Prx, Pry, Prz = kgp.playerGetLocation(player.ID)
			local playerPosition = {x = Px, y = Py, z = Pz, rx = Prx, ry = Pry, rz =  Prz}
			s_tInitialPlayerPositions[player.name] = playerPosition
			playerTeleport(self, {user = player, x = eventFlag.position.x + 3, y = ( eventFlag.position.y + RESPAWN_Y_BUFFER), z =  eventFlag.position.z + 3,
						  rx =  eventFlag.position.rx, ry =  eventFlag.position.ry, rz =  eventFlag.position.rz})
		end
	end
end

--Determines if player can spawn to a player spawner
function canSpawnToPlayerSpawner(self, playerSpawners, lobbySpawners, user)

	if playerSpawners[1] == nil then return false end
	if lobbySpawners[1] == nil then return true end

	if s_tSettings.lobbySpawnOnly == true or user.newPlayer then
		return false
	else
		return true
	end

end

-- Teleport player
function playerTeleport(self, event)
	if self == nil then log("Controller_Game - playerTeleport() self is nil."); return end
	if event == nil then log("Controller_Game - playerTeleport() event is nil."); return end
	
	local user = Players.determineUser(event.user, s_uPlayers)
	if user == nil then log("Controller_Game - playerTeleport() user is nil."); return end
	if user.ID == nil then log("Controller_Game - playerTeleport() user.ID is nil."); return end
	if user.position == nil then log("Controller_Game - playerTeleport() user.position is nil."); return end
	
	user.position.x  = event.x  or 0
	user.position.y  = event.y  or 0
	user.position.z  = event.z  or 0
	user.position.rx = event.rx or 1
	user.position.ry = event.ry or 0
	user.position.rz = event.rz or 1
	if user.position.rx == 0 and user.position.rz == 0 then
		log("ERROR DETECTED - ED-7041 : playerTeleport attempted to set invalid user rotation!")
		Debug.printTableError("ERROR DETECTED - ED-7041 : user", user)
		user.position.rx = 1
	end
	kgp.playerTeleport(user.ID, user.position.x, user.position.y, user.position.z, user.position.rx, user.position.ry, user.position.rz)
end

function savePlayerPostion(self, event)
	if event.player == nil then log("Controller_Game - savePlayerPostion() event.player is nil.") return end
	local tempPlayer = s_uItemInstances[event.player]
	if tempPlayer == nil then log("Controller_Game - savePlayerPostion() tempPlayer is nil.") return end
	local pX, pY, pZ, pRX, pRY, pRZ = kgp.playerGetLocation(tempPlayer.ID)
	kgp.playerSaveZoneSpawnPoint(tempPlayer.ID, pX,  pY, pZ, pRX, pRY, pRZ)
end

-- Load the player's stats
function playerLoadStats(self, player)
	-- Default stats table
	local defStats = {
		energy		= DEF_MAX_ENERGY, 
		maxEnergy	= DEF_MAX_ENERGY,
		health		= DEF_MAX_HEALTH, 
		maxHealth	= DEF_MAX_HEALTH,
		deathCount	= DEF_DEATH_COUNT,
		npcSpouse   = DEF_NPC_SPOUSE,
		followerCount = DEF_FOLLOW_COUNT
	}
	local isNewPlayer = true
	if player and player.name then
		-- Load stats data
		local playerStatsString = SaveLoad.loadPlayerData(player.name, "playerStats")

		-- Was data available for decoding?
		if playerStatsString and playerStatsString ~= "[]" then
			playerStats = Events.decode(playerStatsString)
			
			if playerStats == nil then
				log("ERROR DETECTED - ED-5617 : player.name = "..tostring(player and player.name)..", playerStatsString = "..tostring(playerStatsString))
				return defStats, false
			end
			
			-- Default nil values
			if playerStats.energy 	  == nil 	  then playerStats.energy    		= defStats.energy end
			if playerStats.maxEnergy  == nil 	  then playerStats.maxEnergy 		= defStats.maxEnergy end
			if playerStats.health 	  == nil 	  then playerStats.health    		= defStats.health end
			if playerStats.maxHealth  == nil 	  then playerStats.maxHealth 		= defStats.maxHealth end
			if playerStats.deathCount == nil 	  then playerStats.deathCount		= defStats.deathCount end
			if playerStats.npcSpouse  == nil 	  then playerStats.npcSpouse		= defStats.npcSpouse end
			if playerStats.followerCount == nil   then playerStats.followerCount	= defStats.followerCount end
			
			isNewPlayer = false

			if ZoneInfo.isLinkedZone() then
				local zoneInstanceID, zoneIndex, zoneType = kgp.gameGetCurrentInstance()
				local zoneVisited = false
				if playerStats.zonesVisited then
					local zonesVisited = Toolbox.deepCopy(playerStats.zonesVisited)
					if zonesVisited[tostring(zoneInstanceID)] then 
						zoneVisited = true
					end
				end
				if zoneVisited == false then
					if not playerStats.zonesVisited then
						playerStats.zonesVisited = {}
					else
						playerStats.zonesVisited = Toolbox.deepCopy(playerStats.zonesVisited)
					end
					playerStats.zonesVisited[tostring(zoneInstanceID)] = true
					isNewPlayer = true
				end
			end



			
			return playerStats, isNewPlayer
		-- No? Then return the default table
		else
			return defStats, isNewPlayer
		end
	else
		log("ERROR DETECTED - ED-2183 : playerLoadStats : player = "..tostring(player)..", player.name = nil")
		if player then
			Debug.printTableError("ERROR DETECTED - ED-2183 : playerLoadStats : player ", player)
		end
		return defStats, false
	end
end

-- Play sound on a player
function playerPlaySound(self, event)
	if self == nil then log("Controller_Game - playerPlaySound() self is nil."); return end
	if event == nil then log("Controller_Game - playerPlaySound() event is nil."); return end
	if event.soundGLID == nil then log("Controller_Game - playerPlaySound() event.soundGLID is nil."); return end

	local user = Players.determineUser(event.user, s_uPlayers)
	if user == nil then log("Controller_Game - playerPlaySound() Failed to extract user."); return end
	if user.ID == nil then log("Controller_Game - playerPlaySound() user.ID is nil."); return end
	
	kgp.soundPlayOnClient(event.soundGLID, user.ID)
	-- Remove after x seconds?
	if event.playTime then
		Events.setTimer(event.playTime, playerStopSound, self, {user = user, soundGLID = event.soundGLID})
	end
end

function playerStopSound(self, event)
	if self == nil then log("Controller_Game - playerStopSound() self is nil."); return end
	if event == nil then log("Controller_Game - playerStopSound() event is nil."); return end
	if event.soundGLID == nil then log("Controller_Game - playerStopSound() event.soundGLID is nil."); return end
	
	local user = Players.determineUser(event.user, s_uPlayers)
	if user == nil then log("Controller_Game - playerStopSound() Failed to extract user."); return end
	if user.ID == nil then log("Controller_Game - playerStopSound() Failed to extract.ID user."); return end
	
	kgp.soundStopOnClient(event.soundGLID, user.ID)
end

function playerSetAnimation(self, event)
	if self == nil then log("Controller_Game - playerSetAnimation() self is nil."); return end
	if event == nil then log("Controller_Game - playerSetAnimation() event is nil."); return end
	if event.animation == nil then log("Controller_Game - playerSetAnimation() event.animation is nil."); return end
	
	local user = Players.determineUser(event.user, s_uPlayers)
	if user == nil then log("Controller_Game - playerSetAnimation() Failed to extract user."); return end
	if user.ID == nil then log("Controller_Game - playerSetAnimation() Failed to extract.ID user."); return end
	
	-- Is the player alive
	if user.alive then
        log("Setting animation "..tostring(event.animation).." for user "..tostring(user.ID))
		kgp.playerSetAnimation(user.ID, event.animation)
	end
end

-- Sets a particle on a player
function playerSetParticle(self, event)
	-- Error check inputs
	local user = event.user
	if user == nil then log("Controller_Game - playerSetParticle() - [user] is nil."); return false end

	if tonumber(event.particleGLID) == nil then log("Controller_Game - playerSetParticle() - [particleGLID] must be a number."); return false end
	local bone = event.boneName or "default"
	if not user.particleKeys[bone] then log("Controller_Game - playerSetParticle() - bone["..tostring(bone).."] not a valid avatar bone."); return false end
	if event.offset then
		if event.offset.x then
			if tonumber(event.offset.x) == nil then log("Controller_Game - playerSetParticle() - offset.x["..tostring(event.offset.x).."] must be a number."); return false end
		end
		if event.offset.y then
			if tonumber(event.offset.y) == nil then log("Controller_Game - playerSetParticle() - offset.y["..tostring(event.offset.y).."] must be a number."); return false end
		end
		if event.offset.z then
			if tonumber(event.offset.z) == nil then log("Controller_Game - playerSetParticle() - offset.z["..tostring(event.offset.z).."] must be a number."); return false end
		end
	end
	if event.forwardDir then
		if event.forwardDir.x then -- X is avatar up/down
			if tonumber(event.forwardDir.x) == nil then log("Controller_Game - playerSetParticle() - forwardDir.x["..tostring(event.forwardDir.x).."] must be a number."); return false end
		end
		if event.forwardDir.y then -- Y is avatar left/right
			if tonumber(event.forwardDir.y) == nil then log("Controller_Game - playerSetParticle() - forwardDir.y["..tostring(event.forwardDir.y).."] must be a number."); return false end
		end
		if event.forwardDir.z then -- Z is avatar forward/back
			if tonumber(event.forwardDir.z) == nil then log("Controller_Game - playerSetParticle() - forwardDir.z["..tostring(event.forwardDir.z).."] must be a number."); return false end
		end
	end
	if event.upDir then
		if event.upDir.x then
			if tonumber(event.upDir.x) == nil then log("Controller_Game - playerSetParticle() - upDir.x["..tostring(event.upDir.x).."] must be a number."); return false end
		end
		if event.upDir.y then
			if tonumber(event.upDir.y) == nil then log("Controller_Game - playerSetParticle() - upDir.y["..tostring(event.upDir.y).."] must be a number."); return false end
		end
		if event.upDir.z then
			if tonumber(event.upDir.z) == nil then log("Controller_Game - playerSetParticle() - upDir.z["..tostring(event.upDir.z).."] must be a number."); return false end
		end
	end
	if event.playTime ~= nil and type(event.playTime) ~= "number" then log("Controller_Game - playerSetParticle() - playTime["..tostring(event.playTime).."] must be a number."); return false end

	local offset = {0, 0, 0}
	if event.offset then
		offset = {event.offset.x or 0, event.offset.y or 0, event.offset.z or 0}
	end
	local forwardDir = {0, 0, 1}
	if event.forwardDir then
		forwardDir = {event.forwardDir.x or 0, event.forwardDir.y or 0, event.forwardDir.z or 1}
	end
	local upDir = {0, 1, 0}
	if event.upDir then
		upDir = {event.upDir.x or 0, event.upDir.y or 1, event.upDir.z or 0}
	end

	local boneSlot
	if bone == "default" then boneSlot = "" end
	kgp.playerSetParticle(user.ID, event.particleGLID, boneSlot or bone, user.particleKeys[bone], offset[1], offset[2], offset[3], forwardDir[1], forwardDir[2], forwardDir[3], upDir[1], upDir[2], upDir[3])

	-- Assign this bone and slot to the provided key
	user.particles[event.particleKey] = {
		bone   = bone,
		slotID = user.particleKeys[bone]
	}

	-- Increment particleKey for this bone for next particle set
	user.particleKeys[bone] = user.particleKeys[bone] + 1

	-- Remove after x seconds?
	if event.playTime then
		Events.setTimer(event.playTime, playerRemoveParticle, self, {user = user, particleKey = event.particleKey})
	end

	Events.sendClientEvent("FRAMEWORK_EMOTE_OBJECT", {emoteState = true}, user.ID)

	return event.particleKey
end

-- Removes a particle from a player
function playerRemoveParticle(self, event)

	-- Error check events
	local user = event.user
	if user == nil then log("Controller_Game - playerRemoveParticle() - [user] is nil."); return false end

	if event.particleKey then
		local userParticleKey = user.particles[event.particleKey]
		if userParticleKey then
			local bone   = userParticleKey.bone
			local slotID = userParticleKey.slotID
			-- Remove this particle
			local boneSlot
			if bone == "default" then boneSlot = "" end
			kgp.playerRemoveParticle(user.ID, boneSlot or bone, slotID)

			-- Decrement this bone's slotID key
			user.particleKeys[bone] = math.max(user.particleKeys[bone] - 1, 0)

			-- Clear out particle slot
			user.particles[event.particleKey] = nil

			
			Events.sendClientEvent("FRAMEWORK_EMOTE_OBJECT", {emoteState = false}, user.ID)

			return true
		else
			log("Controller_Game - playerRemoveParticle() - particleKey["..tostring(event.particleKey).."] is not a valid particle key for this player."); return false
		end
	else
		log("Controller_Game - playerRemoveParticle() - [particleKey] is nil."); return false
	end
end

-- Place a player item
function playerPlaceItem(self, event)
	local user = event.user
	if s_uPlayers[user.name].mode == "Player" then
		-- Register this player for a build mode place queue
		s_buildQueues[user.ID] = event.GLID
		
		-- Open the framework player build menu
		kgp.playerLoadMenu(user.ID, "Framework_PlayerBuild.xml")
	end
end

-- Set player's mode
function playerSetMode(self, event)
	local player = event.player
	local playerMode = player and player.mode
	
	if playerMode == nil then
		log("ERROR DETECTED - ED-6812 : playerSetMode : player mode was nil!")
		Debug.printTableError("ERROR DETECTED - ED-6812", player)
		return
	end
	
	-- Unlock players
	self:playerSetPhysics({user = player, reset = true})
	-- Hide or display objects marked for creatorOnly
	for PID, uItem in pairs(s_uItemInstances) do
		-- Is this object creatorOnly?
		if uItem then
			-- Is this item for creators only?
			if uItem.creatorOnly then
				if player.mode == "Player" then
					kgp.objectHide(tonumber(PID), player.ID)
					kgp.objectRemoveParticleForPlayer(player.ID, tonumber(PID))
				else
					kgp.objectShow(tonumber(PID), player.ID)
					if uItem.particleGLID then
						kgp.objectSetParticleForPlayer(player.ID, tonumber(PID), uItem.particleGLID)
					end
				end
			elseif uItem.ownerOnly and uItem.ownerName then
				if player.mode == "Player" then
					if uItem.ownerName ~= player.name then
						kgp.objectHide(tonumber(PID), player.ID)
						kgp.objectRemoveParticleForPlayer(player.ID, tonumber(PID))
					end
				else
					kgp.objectShow(tonumber(PID), player.ID)
					if uItem.particleGLID then
						kgp.objectSetParticleForPlayer(player.ID, tonumber(PID), uItem.particleGLID)
					end
				end
			end
			-- Does this item manually request labels or not?
			if uItem.showLabels == nil or (uItem.showLabels ~= nil and uItem.showLabels == true) then
				-- Does this item have labels that match this player's mode?
				if uItem.modeLabels then 
					kgp.objectClearLabelsForPlayer(player.ID, tonumber(PID))
					if uItem.modeLabels[player.mode] then
						local label = constructLabelTable(uItem.modeLabels[player.mode])
						kgp.objectAddLabelsForPlayer(player.ID, tonumber(PID), label)
					end
				end
			end
			-- Set quest icon?
			if uItem.quests then
				-- Players get quest icons
				if player.mode == "Player" then
					Events.sendEvent("FRAMEWORK_SET_PLAYER_QUEST_ICONS", {player = player})
				-- Gods don't
				else
					Events.sendEvent("FRAMEWORK_CLEAR_PLAYER_QUEST_ICONS", {player = player})
				end
			end
		end
	end
	-- Inform users of this player's new status
	Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = player.name, modifiedFields = {mode = player.mode}})
	
	-- Enter God mode
	if player.mode == "God" then
		-- If this player changed modes while dying
		if not player.alive then
			-- Tell Controller_Battle to cancel a queued respawn timer if one exists
			Events.sendEvent("FRAMEWORK_CANCEL_RESPAWN_TIMER", {player = player})
			
			-- Clear animations
			kgp.playerSetAnimation(player.ID, 0)
		end
		
	-- Enter Player mode
	elseif player.mode == "Player" then
		-- Open Toolbar
		if s_tSettings.frameworkEnabled then kgp.playerLoadMenu(player.ID, "Framework_Toolbar.xml") end
		if player.alive == false then
			Events.sendEvent("FRAMEWORK_KILL_PLAYER", {user = player})
		end
	end
end

-- HUD Management Functions

-- Registers a PID in the zone for the health bar
function enableHealthBar(self, event)
	if self == nil then log("Controller_Game - enableHealthBar() self is nil."); return end
	if self.PID == nil then log("Controller_Game - enableHealthBar() self.PID is nil."); return end
	
	-- Did a controller register for this system?
	if event then
		if event.controller then
			self.PID = event.controller
		end
	end
	s_tObjectsEnablingHUD.health[tostring(self.PID)] = true
	-- If this was the first health bar-enabled item, add health bars to everyone in the zone
	local count = 0
	for PID, _ in pairs(s_tObjectsEnablingHUD.health) do
		count = count + 1
	end
	-- This was the first item to register
	if count == 1 then
		-- Get every player in the zone
		local tPlayers = getPlayers()
		for sName, uPlayer in pairs(tPlayers) do
			Events.sendClientEvent("FRAMEWORK_ENABLE_HEALTH", {health = uPlayer.health, maxHealth = uPlayer.maxHealth}, uPlayer.ID)
		end
	end
end

-- Registers a PID in the zone for the health bar
function enableEnergyBar(self, event)
	if self == nil then log("Controller_Game - enableEnergyBar() self is nil."); return end
	if self.PID == nil then log("Controller_Game - enableEnergyBar() self.PID is nil."); return end
	
	-- Did a controller register for this system?
	if event then
		if event.controller then
			self.PID = event.controller
		end
	end
	
	s_tObjectsEnablingHUD.energy[tostring(self.PID)] = true
	-- If this was the first energy bar-enabled item, add energy bars to everyone in the zone
	local count = 0
	for PID, _ in pairs(s_tObjectsEnablingHUD.energy) do
		count = count + 1
	end
	-- This was the first item to register
	if count == 1 then
		-- Get every player in the zone
		local tPlayers = getPlayers()
		for sName, uPlayer in pairs(tPlayers) do
			Events.sendClientEvent("FRAMEWORK_ENABLE_ENERGY", {energy = uPlayer.energy, maxEnergy = uPlayer.maxEnergy}, uPlayer.ID)
		end
	end
end

-- Registers a PID in the zone for the armor bar
function enableArmorBar(self, event)
	if self == nil then log("Controller_Game - enableArmorBar() self is nil."); return end
	if self.PID == nil then log("Controller_Game - enableArmorBar() self.PID is nil."); return end
	
	-- Did a controller register for this system?
	if event then
		if event.controller then
			self.PID = event.controller
		end
	end
	
	s_tObjectsEnablingHUD.armor[tostring(self.PID)] = true
	-- If this was the first armor bar-enabled item, add armor bars to everyone in the zone
	local count = 0
	for PID, _ in pairs(s_tObjectsEnablingHUD.armor) do
		count = count + 1
	end
	-- This was the first item to register
	if count == 1 then
		Events.sendClientEvent("FRAMEWORK_ENABLE_ARMOR", {})
	end
end

-- Add energy for a player
function playerAddEnergy(self, event)
	if self == nil then log("Controller_Game - playerAddEnergy() self is nil."); return end
	if event == nil then log("Controller_Game - playerAddEnergy() event is nil."); return end
	if event.energy == nil then log("Controller_Game - playerAddEnergy() event.energy is nil."); return end

	local user 		  = Players.determineUser(event.user, s_uPlayers)
	if user 		  == nil then log("Controller_Game - playerAddEnergy() Failed to extract user."); return end
	if user.ID 		  == nil then log("Controller_Game - playerAddEnergy() user.ID is nil."); return end
	if user.energy 	  == nil then log("Controller_Game - playerAddEnergy() user.energy is nil."); return end
	if user.maxEnergy == nil then log("Controller_Game - playerAddEnergy() user.maxEnergy is nil."); return end
	
	-- Don't add energy if player is currently maxed out
	if user.energy < user.maxEnergy then
		local energyGain = event.energy
		
		user.energy = math.min(user.energy + energyGain, user.maxEnergy)
		
		-- Update HUD
		Events.sendClientEvent("FRAMEWORK_SET_ENERGY", {energy = user.energy, maxEnergy = user.maxEnergy}, user.ID)
	end
end

-- Remove energy from a player
function playerRemoveEnergy(self, event)
	if self == nil then log("Controller_Game - playerRemoveEnergy() self is nil."); return end
	if event == nil then log("Controller_Game - playerRemoveEnergy() event is nil."); return end
	if event.energy == nil then log("Controller_Game - playerRemoveEnergy() event.energy is nil."); return end
	
	local user 		  = Players.determineUser(event.user, s_uPlayers)
	if user 		  == nil then log("Controller_Game - playerRemoveEnergy() Failed to extract user " .. tostring(event.user)); return end
	if user.ID 		  == nil then log("Controller_Game - playerRemoveEnergy() user.ID is nil."); return end
	if user.energy 	  == nil then log("Controller_Game - playerRemoveEnergy() user.energy is nil."); return end
	if user.maxEnergy == nil then log("Controller_Game - playerRemoveEnergy() user.maxEnergy is nil."); return end
	
	-- Don't remove health if player is already dead
	if user.energy > 0 then
		local energyLoss = event.energy
		
		user.energy = math.max(user.energy - energyLoss, 0)
		
		-- Update HUD
		Events.sendClientEvent("FRAMEWORK_SET_ENERGY", {energy = user.energy, maxEnergy = user.maxEnergy}, user.ID)
	end
end

-- Set energy for a player
function playerSetEnergy(self, event)
	if self == nil then log("Controller_Game - playerSetEnergy() self is nil."); return end
	if event == nil then log("Controller_Game - playerSetEnergy() event is nil."); return end
	
	local user = Players.determineUser(event.user, s_uPlayers)
	if user == nil then log("Controller_Game - playerSetEnergy() Failed to extract user."); return end
	if user.ID == nil then log("Controller_Game - playerSetEnergy() user.ID is nil."); return end
	if user.energy == nil then log("Controller_Game - playerSetEnergy() user.energy is nil."); return end
	if user.maxEnergy == nil then log("Controller_Game - playerSetEnergy() user.maxEnergy is nil."); return end
	
	local percentage = event.percentage
	local customEnergy = event.energy
	
	-- If a custom energy value was passed in
	if customEnergy then
		-- Range check energy
		if customEnergy < 0 then customEnergy = 0 end
		if customEnergy > user.maxEnergy then customEnergy = user.maxEnergy end
		user.energy = customEnergy
	elseif percentage then
		-- Range check percentage
		if percentage < 0 then percentage = 0 end
		if percentage > 100 then percentage = 100 end
		
		user.energy = user.maxEnergy * (percentage/100)
	end
	
	-- Update HUD
	Events.sendClientEvent("FRAMEWORK_SET_ENERGY", {energy = user.energy, maxEnergy = user.maxEnergy}, user.ID)
end

-- Logs progress of Framework Instantiation
function logProgress(self, event)
	local tEventData = {
		log				= tostring(event.log),
		loadPercentage	= event.percentage,
		display			= event.display
	}
	Events.sendClientEvent("FRAMEWORK_LOG_PROGRESS", tEventData)
end

-- Gets WorldSettings
function getWorldSettings(self)
	local settings = SaveLoad.loadGameData("WorldSettings")
	
	if settings then
		s_tSettings = Events.decode(settings)
		s_tSettings.frameworkEnabled = (kgp.gameGetFrameworkEnabled() == 1)
		log("s_tSettings.frameworkEnabled = "..tostring(s_tSettings.frameworkEnabled))
		-- Override custom game item load?
		if s_tSettings.customGameItemLoad then
			s_tGameConfigs.GameItemLoad = tonumber(s_tSettings.customGameItemLoad)
		end
	end
end

-- Instantiate a player
-- TODO: Can this all be contained in a class? A new Controller? Brainstorm
function instantiatePlayer(self, player)
	local playerName = player.name
	
	if playerName == nil then
		log("ERROR DETECTED - ED-2183 : instantiatePlayer : player.name was nil!")
		Debug.printTableError("ERROR DETECTED - ED-2183 : instantiatePlayer : player ", player)
		return
	end

	-- New player?
	local pX, pY, pZ, pRX, pRY, pRZ = kgp.playerGetLocation(player.ID)

	-- Populate new s_uPlayers entry with new player
	player.alive = true
	player.loading = true
	player.position = {
		x  = pX  or 0, y  = pY  or 0, z  = pZ  or 0,
		rx = pRX or 0, ry = pRY or 0, rz = pRZ or 0
	}

	player.physics = {
		maxSpeed				= DEF_PHYSICS.MAXSPEED, 			   	minSpeed				= DEF_PHYSICS.MINSPEED,
		accellerate				= DEF_PHYSICS.ACCELLERATE, 		   		decellerate		 		= DEF_PHYSICS.DECELLERATE,
		curTransY				= DEF_PHYSICS.CURTRANSY, 		   		rotationDragConstant	= DEF_PHYSICS.ROTATIONDRAGCONSTANT,
		rotationalGroundDrag	= DEF_PHYSICS.ROTATIONALGROUNDDRAG, 	jumpPower 				= DEF_PHYSICS.JUMPPOWER,
		groundDrag				= DEF_PHYSICS.GROUNDDRAG, 		   		strafeReduction 		= DEF_PHYSICS.STRAFEREDUCTION,
		crossSectionalArea		= DEF_PHYSICS.CROSSSECTIONALAREA,   	airControl 				= DEF_PHYSICS.AIRCONTROL,
		jumpRecoverTime			= DEF_PHYSICS.JUMPRECOVERTIME,      	collisionEnabled		= DEF_PHYSICS.COLLISIONENABLED
	}
	player.particles = {} -- Keyed table of attached particles and their slots
	-- Slot IDs of occupied bones
	player.particleKeys = {
		["default"]				= 0, ["Bip01"]				= 0, ["Bip01 Pelvis"]		= 0, ["Bip01 Spine"]		= 0, 
		["Bip01 Spine1"]		= 0, ["Bip01 Spine2"]		= 0, ["Bip01 Neck"] 		= 0, ["Bip01 L Clavicle"]	= 0,
		["Bip01 L UpperArm"]	= 0, ["Bip01 L Forearm"]	= 0, ["Bip01 L Hand"]		= 0, ["Bip01 L Finger0"]	= 0,
		["Bip01 L Finger01"]	= 0, ["Bip01 L Finger1"]	= 0, ["Bip01 L Finger11"]	= 0, ["Bip01 L Finger2"]	= 0,
		["Bip01 L Finger21"]	= 0, ["Bip01 L Finger3"]	= 0, ["Bip01 L Finger31"]	= 0, ["Bip01 L ForeTwist"]	= 0,
		["Bip01 R Clavicle"]	= 0, ["Bip01 R UpperArm"]	= 0, ["Bip01 R Forearm"]	= 0, ["Bip01 R Hand"]		= 0,
		["Bip01 R Finger0"]		= 0, ["Bip01 R Finger01"]	= 0, ["Bip01 R Finger1"]	= 0, ["Bip01 R Finger11"]	= 0,
		["Bip01 R Finger2"]		= 0, ["Bip01 R Finger21"]	= 0, ["Bip01 R Finger3"]	= 0, ["Bip01 R Finger31"]	= 0,
		["Bip01 R ForeTwist"]	= 0, ["Bip01 L Thigh"]		= 0, ["Bip01 L Calf"]		= 0, ["Bip01 L Foot"]		= 0,
		["Bip01 L Toe"]			= 0, ["Bip01 R Thigh"]		= 0, ["Bip01 R Calf"]		= 0, ["Bip01 R Foot"]		= 0,
		["Bip01 R Toe"]			= 0, ["Bip01 Head"]			= 0
	}
	
	-- Anything has been equipped this session (used for the jumpsuit logic in Controler_Inventory)
	player.equippedAnything = false
	
	local playerStats = {}
	local isNewPlayer = false
	-- Load player stats

	if (player.energy == nil) or
	   (player.maxEnergy == nil) or
	   (player.health == nil) or
	   (player.maxHealth == nil) or
	   (player.deathCount == nil) or
	   (player.npcSpouse == nil) or
	   (player.followerCount == nil) or
	   ZoneInfo.isLinkedZone() then

		-- If the player has no stats, it's their first visit to this world
		--ALways get stats in linked zones
		playerStats, isNewPlayer = self:playerLoadStats(player)
	end
	
	player.newPlayer = isNewPlayer

	player.energy     = playerStats.energy 	   or player.energy 	or DEF_MAX_ENERGY
	player.maxEnergy  = playerStats.maxEnergy  or player.maxEnergy  or DEF_MAX_ENERGY

	player.maxHealth  = playerStats.maxHealth  or player.maxHealth  or DEF_MAX_HEALTH
	player.health     = playerStats.health 	   or player.health     or DEF_MAX_HEALTH
	player.deathCount = playerStats.deathCount or playerStats.deathCount or DEF_DEATH_COUNT
	player.npcSpouse  = DEF_NPC_SPOUSE -- Wait to update this until the spouse is confirmed
	player.followerCount = DEF_FOLLOW_COUNT -- Wait to update this until the # of followers is confirmed
	player.zonesVisited = playerStats.zonesVisited
	
	if player.health <= 0 then
		player.health = DEF_MAX_HEALTH
		player.energy = DEF_MAX_ENERGY
	end
	
	player.attemptingManualRespawn = false -- Is this player attempting to respawn manually
	
	-- Set armorRating to 0. Will be updated in Controller_Inventory when their Inventory and armor is loaded
	player.armorRating = 0
	player.baseArmorRating = 0
	
	-- Declare an empty inventory, armor and blueprints table. It will be populated shortly (Controller_Inventory)
	player.inventory = {}
	player.armor = {}
	player.blueprints = {}
	-- What UNID is equipped
	player.equipped = ""

	player.targetable = true
	player.target	  = {} -- Pointer to target object
	player.inZone     = true -- Mark player as in the zone
	
	player.mode = "Player"
	player.type = "Player"
	
	player.lootRate = 0

	s_uPlayers[playerName] = player
	
	-- Load PlayerHandler menu
	kgp.playerLoadMenu(player.ID, "Framework_PlayerHandler.xml")

	kgp.playerDeleteZoneSpawnPoint(player.ID)
end

-- Activates a player
function activatePlayer(self, player)
	player.pvpEnabled = checkFlagZoneSettings(true, false, player.name, "pvp", 0)
	player.playerDestruction = checkFlagZoneSettings(true, false, player.name, "playerDestruction", 0)
	player.playerBuild = checkFlagZoneSettings(true, false, player.name, "playerBuild", 0)
	
	kgp.playerRenderLabelsOnTop(player.ID, false)
	
	-- Load this player's timed vends
	if player.name then
		local timedVends = getPlayerTimedVends(player.name)
		player.timedVends = {} -- Instantiate sharedData table before populating it
		for PID, vendInfo in pairs(timedVends) do
			if type(vendInfo) == "table" then
				player.timedVends[PID] = {}
				for i,v in pairs(vendInfo) do
					player.timedVends[PID][i] = v
				end
			end
		end
	end

	-- Generate sound
	if player.gender == "M" then
		local tSoundParams = GameDefinitions.generateGenericSoundParams(player.ID, 4522074, 20, true)
		player.damageSound = Behavior_Sound.generate(tSoundParams)
	
	elseif player.gender == "F" then
		local tSoundParams = GameDefinitions.generateGenericSoundParams(player.ID, 4522071, 20, true)
		player.damageSound = Behavior_Sound.generate(tSoundParams)
	end
	
	--Send welcome screen settings to any users that are loading.
	if s_tWelcome.showMenu == true then
		Events.sendClientEvent("FRAMEWORK_RETURN_WELCOME", s_tWelcome, player.ID)
	end

	-- Load player inventory (Handled by Controller_Inventory.lua)
	Events.sendEvent("PLAYER_LOAD_INVENTORY", {user = player})
	
	-- Inform Framework_PlayerHandler that this player's in a Framework-enabled world
	Events.sendClientEvent("FRAMEWORK_ACTIVATE_PLAYER", {mode = player.mode}, player.ID)
	Events.sendEvent("ACTIVATE_PLAYER", {player = player})
	SpinUpMetrics.recordPlayerActivate(player.name)

	-- Set this player's mode
	player.inZone = true
	playerSetMode(self, {player = player})

	local eventOccurring = false
	for i, v in pairs(s_tEvents) do
		if s_uItemInstances[i] and v == EVENT_OCCURRING then
			eventOccurring = true
			Events.sendEvent("PLAYER_EVENT_INVITE", {player = player})
		end
	end

	--check if a set postion
	local zoneInstanceID, zoneIndex, zoneType = kgp.gameGetCurrentInstance()
	local spawnPostion = SaveLoad.loadPlayerData(player.name, "playerSpawnLocation", zoneInstanceID, zoneType)

	if spawnPostion then
		local tSpawnPoint = Events.decode(spawnPostion)
		playerTeleport(self, {user = player, x = tSpawnPoint.x, y = tSpawnPoint.y, z = tSpawnPoint.z,
							  rx =  tSpawnPoint.rx, ry =  tSpawnPoint.ry, rz = tSpawnPoint.rz})
		SaveLoad.deletePlayerData(player.name, "playerSpawnLocation", zoneInstanceID, zoneType)
	-- If the framework's being instantiated manually
	elseif s_uInstantiatingPlayer then
		-- New visitors to the world should be spawned.
		if (player.newPlayer and player.perm == 3) then
			self:spawnPlayer({user = player, respawnAttempt = true})
		end
	-- If not manually instantiated, we can assume the Framework's coming up automatically (zone in)
	else
		-- All new players get spawned

		if eventOccurring then
			self:spawnPlayerAtEventFlag(player)
		elseif (player.newPlayer or s_tSettings.lobbySpawnOnly) then
			self:spawnPlayer({user = player, respawnAttempt = true})		
		end
	end

	local input = {username = player.name}
	Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {CID = 100, input = input, callType = "GET_TOP_WORLDS"})

	s_tTopWorldsReward[player.name] = false	

	Events.sendEvent("CHECK_IF_NPC_SPOUSE_EXISTS", {playerName = player.name})
	Events.sendEvent("CHECK_IF_NPC_FOLLOWERS_EXIST", {playerName = player.name})
	
	Events.sendEvent("COUNT_ACTIVE_DOS_ON_REQUEST", {})
end


---------------------------------
-- Controller-managed functions
---------------------------------

-- Write message to ticker
function writeToTicker(self, event)
	-- Send event to a specific user
	if event.user then
		Events.sendClientEvent("FRAMEWORK_CONSOLE_WRITE", {color = {r=event.color.r, g=event.color.g, b=event.color.b}, text = tostring(event.message), textureName = event.textureName}, event.user.ID)
	-- Send event to every client in the zone
	else
		Events.sendClientEvent("FRAMEWORK_CONSOLE_WRITE", {color = {r=event.color.r, g=event.color.g, b=event.color.b}, text = tostring(event.message), textureName = event.textureName})
	end
end


-- Compares entries of 2 tables. Returns true if there is a difference. False if they are identical
function diffTables(tab1, tab2)
	-- First check that the two passed in tables match
	for index, value in pairs(tab1) do
		-- Does the second table have a corresponding entry?
		if tab2[index] ~= nil then
			-- If both indices types don't match, return an error
			if type(value) ~= type(tab2[index]) then
				return nil
			end
		else
			return nil
		end
	end

	-- If types match up, compare
	for index, value in pairs(tab1) do
		-- Is this entry a table itself?
		if type(value) == "table" or type(value) == "userdata" then
			-- Check deeper
			local diff = diffTables(value, tab2[index])
			if diff then
				return true
			elseif diff == nil then
				return nil
			end
		-- Direct entry compare
		elseif value ~= tab2[index] then
			-- Difference exists
			return true
		end
	end
	-- Could find no difference or error. Tables match
	return false
end

-- -- -- -- -- -- -- -- -- -- --
-- Local functions
-- -- -- -- -- -- -- -- -- -- --

-- Get all players in the s_uPlayers
getPlayers = function()
	local tPlayers = {}
	for sName, uPlayer in pairs(s_uPlayers) do
		if uPlayer and uPlayer.type and uPlayer.inZone then
			tPlayers[sName] = uPlayer
		end
	end
	return tPlayers
end

-- Deprecate energy from all players in the zone
deprecateEnergy = function(player, deltaTick)
	if player.loading then return end
	local energyLoss = (player.maxEnergy/m_starveTime) * deltaTick
	playerRemoveEnergy("", {energy = energyLoss, user = player})
end

regenEnergy = function(player, deltaTick)
	local regenTime = tonumber(player.regenTime)
	local maxEnergy = tonumber(player.maxEnergy)
	if regenTime == nil or maxEnergy == nil then return end
	local energyGain = (maxEnergy/regenTime) * deltaTick
	playerAddEnergy("", {energy = energyGain, user = player})
end

-- Regen health
regenHealth = function(player, deltaTick)
	-- Is this player's energy over ENERGY_FOR_HEALTH_REGEN%? i.e. CAN they regen health?
	if ((player.energy/player.maxEnergy)*100 >= ENERGY_FOR_HEALTH_REGEN) and (player.health < player.maxHealth) and player.alive then
		-- Regen health
		local healthGain = player.maxHealth * (deltaTick*(HEALTH_REGEN_PER_SEC/100))

		Events.sendEvent("PLAYER_ADD_HEALTH", {health = healthGain, user = player})
	end
end

-- Starve the player
starvePlayer = function(player, deltaTick, activeAlert)
	local healthLoss = (player.maxHealth/STARVE_DURATION) * deltaTick

	Events.sendEvent("PLAYER_REMOVE_HEALTH", {health = healthLoss, source = "starvation", alert = activeAlert, user = player})
end

-- Attempt to start up the Framework
function bootupFramework(self, enabled)
	s_iControllerActivationIndex = 1
	-- Boot up the Framework
	if enabled then
		SpinUpMetrics.recordFrameworkSpinupStart()
		-- Log startup progress
		self:logProgress{log = "Registering Game's event handlers", display = "Activating Gaming"}
		-- Register all event handlers
		for event, handler in pairs(CONTROLLER_EVENTS) do
			Events.registerHandler(event, self[handler], self)
		end
		-- Local event
		Events.addLocalEvents{ "DEBUG" } -- Make debug event handled locally without going through event system.
		-- Log event handler registration complete
		self:logProgress{log = "Game - Event handlers registered"}
		
		-- Log DB pull
		self:logProgress{log = "Game - DB Pulling EnvironmentSettings"}
		getEnvironmentSettings()
		self:logProgress{log = "Game - DB EnvironmentSettings pulled"}

		self:logProgress{log = "Game - DB Pulling WelcomeSettings"}
		getWelcomeSettings()
		self:logProgress{log = "Game - DB WelcomeSettings pulled"}

		self:logProgress{log = "Game - DB Pulling OverviewSettings"}
		getOverviewSettings()
		self:logProgress{log = "Game - DB OverviewSettings pulled"}

		--if ZoneInfo.isLinkedZone() then
			self:logProgress{log = "Game - DB Pulling Player Spawners"}
			self:getPlayerSpawners()
			self:logProgress{log = "Game - DB Player Spawners pulled"}
		--end

		-- Log controller activation begin
		self:logProgress{log = "Activating Controller["..tostring(ACTIVATION_ORDER[s_iControllerActivationIndex]).."]"}
		-- Activate the first Controller (activation returns come back through "FRAMEWORK_CONTROLLER_STATE")

		local activationEvent = {["controller"] = ACTIVATION_ORDER[s_iControllerActivationIndex]}
		-- Pass over the GameItemLoad for Controller_Spawn
		if ACTIVATION_ORDER[s_iControllerActivationIndex] == "Spawn" then
			activationEvent.GameItemLoad = s_tGameConfigs.GameItemLoad
		-- Inventory gets the jumpsuit setting
		elseif ACTIVATION_ORDER[s_iControllerActivationIndex] == "Player" then
			activationEvent.jumpsuit = s_tSettings.jumpsuit
		elseif ACTIVATION_ORDER[s_iControllerActivationIndex] == "Battle" then
			activationEvent.respawnTimerOff = s_tSettings.respawnTimerOff
		end
		Events.sendEvent("FRAMEWORK_ACTIVATE_CONTROLLER", activationEvent)

	-- Last step in Framework shut-down. Shutting down Controller_Game
	else
		-- Log startup progress
		self:logProgress{log = "Unregistering Game's event handlers"}
		-- Unregister all un-needed event handlers
		for event, handler in pairs(CONTROLLER_EVENTS) do
			Events.unregisterHandler(event, self[handler], self)
		end
		-- Log event handler unregistration complete
		self:logProgress{log = "Game - Event handlers un-registered"}
		
		-- Disable fog
		s_tEnvironment.fogEnabled = false
		if s_tDefaultEnvironment then
			s_tEnvironment.sunColorRed 		= s_tDefaultEnvironment.sunColorRed
			s_tEnvironment.sunColorGreen 	= s_tDefaultEnvironment.sunColorGreen
			s_tEnvironment.sunColorBlue 		= s_tDefaultEnvironment.sunColorBlue
			s_tEnvironment.ambientColorRed 	= s_tDefaultEnvironment.ambientColorRed
			s_tEnvironment.ambientColorGreen = s_tDefaultEnvironment.ambientColorGreen
			s_tEnvironment.ambientColorBlue 	= s_tDefaultEnvironment.ambientColorBlue
		end
		setEnvironmentSettings()
		
		-- Log controller de-activation complete
		self:logProgress{log = "Controller shut down [Game]", percentage = 100}
	end
end

getEnvironmentSettings = function()
	local envSettings = SaveLoad.loadGameData("EnvironmentSettings")

	if envSettings then
		s_tEnvironment = Events.decode(envSettings)
		
		-- If the environmentSettings are pulled during a Framework activation, set the environment to "off"
		if s_uInstantiatedGameItem and s_uInstantiatingPlayer and s_tDefaultEnvironment then
			s_tEnvironment.fogEnabled = false
			s_tEnvironment.sunColorRed 		= s_tDefaultEnvironment.sunColorRed
			s_tEnvironment.sunColorGreen 	= s_tDefaultEnvironment.sunColorGreen
			s_tEnvironment.sunColorBlue 		= s_tDefaultEnvironment.sunColorBlue
			s_tEnvironment.ambientColorRed 	= s_tDefaultEnvironment.ambientColorRed
			s_tEnvironment.ambientColorGreen = s_tDefaultEnvironment.ambientColorGreen
			s_tEnvironment.ambientColorBlue 	= s_tDefaultEnvironment.ambientColorBlue
			
			-- Save off these modified settings so we don't pull them next time
			local environmentSettings = {}
			--environmentSettings.envEnabled			= s_tEnvironment.envEnabled
			environmentSettings.fogEnabled			= s_tEnvironment.fogEnabled
			environmentSettings.fogStartRange 		= s_tEnvironment.fogStartRange
			environmentSettings.fogEndRange 		= s_tEnvironment.fogEndRange
			environmentSettings.fogColor			= s_tEnvironment.fogColor
			environmentSettings.fogColorRed			= s_tEnvironment.fogColorRed
			environmentSettings.fogColorGreen		= s_tEnvironment.fogColorGreen
			environmentSettings.fogColorBlue		= s_tEnvironment.fogColorBlue
			--environmentSettings.ambientEnabled		= s_tEnvironment.ambientEnabled
			environmentSettings.ambientColor		= s_tEnvironment.ambientColor
			environmentSettings.ambientColorRed		= s_tEnvironment.ambientColorRed
			environmentSettings.ambientColorGreen 	= s_tEnvironment.ambientColorGreen
			environmentSettings.ambientColorBlue 	= s_tEnvironment.ambientColorBlue
			environmentSettings.sunColor			= s_tEnvironment.sunColor
			environmentSettings.sunColorRed			= s_tEnvironment.sunColorRed
			environmentSettings.sunColorGreen 		= s_tEnvironment.sunColorGreen
			environmentSettings.sunColorBlue 		= s_tEnvironment.sunColorBlue
			
			SaveLoad.saveGameData("EnvironmentSettings", Events.encode(environmentSettings))
		end
		
		setEnvironmentSettings()
	else
		log("Controller_Game - getEnvironmentSettings() Failed to retrieve 'EnvironmentSettings' from script_game_date. No environment settings loaded.")
	end
end

setEnvironmentSettings = function()
	local envVar = {}
	local dsVar = {}
	local fog = {}

	local USE_DEBUGGING_ENVIRONMENT_SETTINGS = false
	-- The following are the settings you get on a homeworld. Leaving it here for testing purposes.
	if( USE_DEBUGGING_ENVIRONMENT_SETTINGS ) then
		envVar[EnvironmentIds.AMBIENTLIGHTREDDAY] = {0.2, 0.2, 0.2}
		envVar[EnvironmentIds.AMBIENTLIGHTON] = 1
		envVar[EnvironmentIds.SUNRED] = {0.899, 0.899, 0.81}
		envVar[EnvironmentIds.SUNLIGHTON] = 1
		envVar[EnvironmentIds.SUNPOSITION] = {300, 500, -700}
		envVar[EnvironmentIds.FOGRED] = {0.449, 0.629, 0.8}
		envVar[EnvironmentIds.FOGON] = 1
		envVar[EnvironmentIds.BACKRED] = {0.5, 0.5, 0.5}
		envVar[EnvironmentIds.LOCKBACKCOLOR] = 0
		envVar[EnvironmentIds.FOGSTART] = 7000
		envVar[EnvironmentIds.FOGRANGE] = 10000
		envVar[EnvironmentIds.ENABLETIMESYSTEM] = 1
-- drf - deprecated environment wind
--		envVar[EnvironmentIds.ENABLEWINDRANDOMIZER] = 0
--		envVar[EnvironmentIds.WINDDIRECTIONX] = 0
--		envVar[EnvironmentIds.WINDDIRECTIONY] = 0
--		envVar[EnvironmentIds.WINDDIRECTIONZ] = 0
--		envVar[EnvironmentIds.MAXWINDSTRENGTH] = 0
		envVar[EnvironmentIds.GRAVITY] = -0.098
		envVar[EnvironmentIds.SEALEVEL] = 0
		envVar[EnvironmentIds.SEAVISCUSDRAG] = 0
	else
		envVar[EnvironmentIds.FOGON] = 1
		envVar[EnvironmentIds.ENABLETIMESYSTEM] = 1
	end

 

	-- -- If environment system is enabled
	-- if s_tEnvironment.envEnabled then
	-- 	if not (s_tEnvironment.fogEnabled and s_tEnvironment.ambientEnabled) then
	-- 		s_tEnvironment.fogEnabled = true
	-- 		s_tEnvironment.ambientEnabled = true
	-- 	end
	-- -- Environment system is disabled
	-- else

	-- 	s_tEnvironment.fogEnabled = false
	-- 	s_tEnvironment.ambientEnabled = false
	-- end

	-- Set Fog Defaults
	if s_tEnvironment.fogEnabled then
		dsVar[DayStateIds.FOGSTARTRANGE] = s_tEnvironment.fogStartRange or 5000
		dsVar[DayStateIds.FOGENDRANGE] = s_tEnvironment.fogEndRange or 8000

		s_tEnvironment.fogColorRed = checkRGBRange(s_tEnvironment.fogColorRed)
		s_tEnvironment.fogColorGreen = checkRGBRange(s_tEnvironment.fogColorGreen)
		s_tEnvironment.fogColorBlue = checkRGBRange(s_tEnvironment.fogColorBlue)

		local fogColorRed = s_tEnvironment.fogColorRed / 255
		local fogColorGreen = s_tEnvironment.fogColorGreen / 255
		local fogColorBlue = s_tEnvironment.fogColorBlue / 255
		fog = {fogColorRed,fogColorGreen,fogColorBlue}
		dsVar[DayStateIds.FOGCOLORRED] = fog
	else
		-- dsVar[DayStateIds.FOGSTARTRANGE] = 5000
		-- dsVar[DayStateIds.FOGENDRANGE] = 8000

		-- fog = {1,1,1}
		-- dsVar[DayStateIds.FOGCOLORRED] = fog
		envVar[EnvironmentIds.FOGON] = 0
		envVar[EnvironmentIds.BACKRED] = {0, 0, 0}
	end

	-- Set Ambient Light Defaults
	--if s_tEnvironment.ambientEnabled then
		s_tEnvironment.ambientColorRed = checkRGBRange(s_tEnvironment.ambientColorRed)
		s_tEnvironment.ambientColorGreen = checkRGBRange(s_tEnvironment.ambientColorGreen)
		s_tEnvironment.ambientColorBlue = checkRGBRange(s_tEnvironment.ambientColorBlue)

		local ambientColorRed = checkRGBRange(s_tEnvironment.ambientColorRed) / 255
		local ambientColorGreen = checkRGBRange(s_tEnvironment.ambientColorGreen) / 255
		local ambientColorBlue = checkRGBRange(s_tEnvironment.ambientColorBlue) / 255
		local ambient = {ambientColorRed,ambientColorGreen,ambientColorBlue}
		dsVar[DayStateIds.AMBIENTCOLORRED] = ambient
	-- else
	-- 	dsVar[DayStateIds.AMBIENTCOLORRED] = {1,1,1}
	-- end

	-- Set Sun Defaults
	--if s_tEnvironment.sunEnabled then
		s_tEnvironment.sunColorRed = checkRGBRange(s_tEnvironment.sunColorRed)
		s_tEnvironment.sunColorGreen = checkRGBRange(s_tEnvironment.sunColorGreen)
		s_tEnvironment.sunColorBlue = checkRGBRange(s_tEnvironment.sunColorBlue)

		local sunColorRed = checkRGBRange(s_tEnvironment.sunColorRed) / 255
		local sunColorGreen = checkRGBRange(s_tEnvironment.sunColorGreen) / 255
		local sunColorBlue = checkRGBRange(s_tEnvironment.sunColorBlue) / 255
		local sun = {sunColorRed,sunColorGreen,sunColorBlue}
		dsVar[DayStateIds.SUNCOLORRED] = sun
	-- else
	-- 	dsVar[DayStateIds.SUNCOLORRED] = {1,1,1}
	-- end

	kgp.scriptSetDayState(dsVar)
	kgp.scriptSetEnvironment(envVar)
end

getWelcomeSettings = function()
	local welSettings = SaveLoad.loadGameData("WelcomeSettings")

	if welSettings then
		s_tWelcome = Events.decode(welSettings)
		
	else
		s_tWelcome.showMenu = false
		s_tWelcome.showWorldName = true
		s_tWelcome.welcomeImage = "None"
		s_tWelcome.welcomeText = ""
	end
end

getOverviewSettings = function()
	--Always save and load from parent for this setting.
	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	local overviewSettings = SaveLoad.loadGameData("OverviewSettings", parentID, parentType)

	if overviewSettings then
		s_tOverview = Events.decode(overviewSettings)
	else
		s_tOverview.overviewEnabled = false
		s_tOverview.overviewImage = "none"
		s_tOverview.markers = {}
	end
end


function getPlayerSpawners(self, event)
	if event and event.player then
		local  playerSpawners = SaveLoad.loadGameData("playerSpawners", tonumber(event.zoneID), tonumber(event.zoneType))
		Events.sendClientEvent("FRAMEWORK_RETURN_PLAYER_SPAWNERS", {playerSpawners = playerSpawners}, event.player.ID)
	else
		local playerSpawners = SaveLoad.loadGameData("playerSpawners")
		if playerSpawners then
			local tempTable = Events.decode(playerSpawners)

			for k, v in pairs(tempTable) do
				s_uPlayerSpawners[tostring(k)] = v
			end
		end
	end
end

--teleport myself to a new zone or location
function onLinkedZoneTeleport(self, event)
	if event.player == nil then return end
	if event.player.ID == nil then return end
	if event.destination == nil then return end

	local uPlayer = event.player
	local destination = event.destination
	if destination.spawner then
		local zoneID = tonumber(destination.zoneID)
		local zoneType = tonumber(destination.zoneType)
		local zID, zIndex, zType = kgp.gameGetCurrentInstance()

		if zoneID and zoneID == zID  and s_uPlayerSpawners then
			local spawnPostion = s_uPlayerSpawners[tostring(destination.spawner.PID)].position
				playerTeleport(self, {user = uPlayer, x = spawnPostion.x, y = spawnPostion.y + RESPAWN_Y_BUFFER, z = spawnPostion.z,
								rx = spawnPostion.rx, ry = spawnPostion.ry, rz = spawnPostion.rz})
				return
		else
			local sPlayerSpawners = SaveLoad.loadGameData("playerSpawners", zoneID, zoneType)
			if sPlayerSpawners then
				local tSpawnPoints = Events.decode(sPlayerSpawners)
				for PID, uItem in pairs(tSpawnPoints) do
					if tonumber(destination.spawner.PID) == tonumber(PID) then
						local spawnPostion = uItem.position
						if spawnPostion then
							SaveLoad.savePlayerData(uPlayer.name, "playerSpawnLocation", Events.encode(spawnPostion), zoneID, zoneType)
						end
					end
				end
			end
		end
	end
	if destination.zoneID and destination.zoneType  then
		Events.sendClientEvent("FRAMEWORK_GOTO_ZONE", {zoneID = destination.zoneID, zoneType = destination.zoneType}, uPlayer.ID)
	end
end



function sendPlayerToKTown(self, event)
	kTownID = s_tGameConfigs.DynamicZoneLinkParentId

	Events.sendClientEvent("FRAMEWORK_GOTO_ZONE", {zoneID = kTownID, zoneType = 6}, event.player.ID)
end

function requestPinCode(self, event)
	local PID = event.PID
	local saveLoc = PIN_SAVE_KEY.."_"..tostring(PID)
	local pinDataString = SaveLoad.loadGameData(saveLoc)

	local pinData = {}
	if pinDataString then
		pinData = Events.decode(pinDataString)
	end
	local tempPinCode = pinData.pinCode or nil

	Events.sendClientEvent("RETURN_PIN_CODE", {pinCode = tempPinCode}, event.player.ID)
end

function savePinCode(self, event)
	-- Save the new PIN
	local PID = event.PID
	local pinCode = event.pinCode
	local pinData = {}
	pinData.pinCode = pinCode
	local pinDataString = Events.encode(pinData)
	local saveLoc = PIN_SAVE_KEY.."_"..tostring(PID)
	SaveLoad.saveGameData(saveLoc, pinDataString)
	-- Clear any users who the object remembers from using the last version of the PIN
	local pinMembers = {}
	pinDataString = Events.encode(pinMembers)
	local saveLoc = PIN_ACCESS_MEM_KEY.."_"..tostring(PID)
	SaveLoad.saveGameData(saveLoc, pinDataString)
end

checkRGBRange = function(color)
	if color and color >= 0 and color <= 255 then
		return color
	else
		return 255
	end
end


checkFlagZoneSettings = function(playerSetting, inFlag, playerName, flagType, PID)
	if playerName == nil then return false end -- Corrupted player data, no permissions for you!
	local flags = {}
	
	for i, v in pairs(s_tFlags) do
		if v.flagType == flagType  then
			if not (inFlag == false and v.PID == PID) then
				table.insert(flags, v)
			end
		end
	end
	local pX, pY, pZ, pRX, pRY, pRZ
	--Handle Land Claim flags
	local uPlayer = s_uPlayers[playerName]
	if uPlayer and uPlayer.ID  then
	 	pX, pY, pZ, pRX, pRY, pRZ = kgp.playerGetLocation(uPlayer.ID)
		if pX == nil or pY == nil or pZ == nil then
			log("ERROR DETECTED - ED-6932 : player with ID = "..tostring(uPlayer.ID).." and name = "..tostring(playerName).." returned an invalid location in checkFlagZoneSettings!")
			return false
		end		
	else
		return false
	end

	local landClaims = {}
	if flagType == "playerBuild" then
		for i, v in pairs(s_tFlags) do
			if v.flagType == "claimFlag" and v.PID ~= PID then
				if isPointInBoundary(v, pX, pY, pZ) then
					if uPlayer and uPlayer.ID then
						Events.sendClientEvent("FRAMEWORK_CLAIM_FLAG_STATUS_UPDATE", {buildFlag = true, flagPID = v.PID}, uPlayer.ID)
					end
					if not isPlayerClaimFlagMember(playerName, v.PID) then
						return false
					end
				end
			end
		end
	elseif flagType == "playerDestruction" then
		for i, v in pairs(s_tFlags) do
			if v.flagType == "claimFlag" and v.noDestruct ~= false and v.noDestruct ~= "false" and v.PID ~= PID then
				if isPointInBoundary(v, pX, pY, pZ) then
					return false
				end
			end
		end
	end

	local dominantFlag = false
	for i, v in pairs(flags) do
		if isPointInBoundary(v, pX, pY, pZ) or (inFlag == true and v.PID == PID) then
			if( dominantFlag ) then
			-- Dominant flag is the flag with the smaller radius
				local dominantArea = dominantFlag.width * dominantFlag.height * dominantFlag.depth
				local flagArea = v.width * v.depth * v.height
				if( dominantArea > flagArea ) then
						dominantFlag = v
				elseif( dominantArea == flagArea ) then
					if flagType == "pvp" then
						if v.enabled == s_tSettings.allowPvP then
							dominantFlag = v
						end
					elseif flagType == "playerBuild" then
						if v.enabled == s_tSettings.playerBuild then
							dominantFlag = v
						end
					elseif flagType == "playerDestruction" then
						if v.enabled == s_tSettings.destructibleObjects then
							dominantFlag = v
						end
					end
				end
			else
				dominantFlag = v
			end
		end
	end

	if dominantFlag then
		playerSetting = dominantFlag.enabled
	else
		if flagType == "pvp" then
			playerSetting = s_tSettings.allowPvP
		elseif flagType == "playerBuild" then
			playerSetting = s_tSettings.playerBuild
		-- elseif flagType == "claimFlag" then
		-- 	playerSetting = s_tSettings.playerBuild
		elseif flagType == "playerDestruction" then
			playerSetting = s_tSettings.destructibleObjects
		end
	end
	return playerSetting
end

isPointInBoundary = function(flag, x, y, z)
	-- OLD SPHERE CHECK
	-- CJW, NOTE: Multiplication is more efficient than taking the power or squarerooting
	-- local xDiff = x - flag.position.x
	-- local yDiff = y - flag.position.y
	-- local zDiff = z - flag.position.z
	-- local xyzDistanceSqrd = xDiff * xDiff + zDiff * zDiff + yDiff * yDiff
	-- local radiusSqrd = flag.radius * flag.radius
	-- return (radiusSqrd >= xyzDistanceSqrd)

	-- NEW CUBE CHECK
	local flagDepth 		= (flag.depth /2) or 0
	local flagHeight 		= flag.height or 0
	local flagWidth 		= (flag.width / 2) or 0

	local xMin = flag.position.x - flagDepth - BOUND_CHK_BUFFER
	local xMax = flag.position.x + flagDepth + BOUND_CHK_BUFFER
	local yMin = flag.position.y - BOUND_CHK_BUFFER_Y
	local yMax = flag.position.y + flagHeight + BOUND_CHK_BUFFER
	local zMin = flag.position.z - flagWidth - BOUND_CHK_BUFFER
	local zMax = flag.position.z + flagWidth + BOUND_CHK_BUFFER

	-- log("\n x < xMin: "..tostring(x < xMin))
	-- log("\n y < yMin: "..tostring(y < yMin))
	-- log("\n x < zMin: "..tostring(x < zMin))
	-- log("\n x < xMax: "..tostring(x < xMin))
	-- log("\n x < xMax: "..tostring(x < xMax))
	-- log("\n x < zMax: "..tostring(x < zMax))


	if  x < xMin or x > xMax or
		y < yMin or y > yMax or
		z < zMin or z > zMax then
		return false
	end
	return true
end

isFlagInBoundary = function(self, otherFlag)
	-- OLD SPHERE CHECK
	-- CJW, NOTE: Multiplication is more efficient than taking the power or squarerooting
	-- local xDiff = otherFlag.position.x - self.position.x
	-- local yDiff = otherFlag.position.y - self.position.y
	-- local zDiff = otherFlag.position.z - self.position.z
	-- local xyzDistanceSqrd = xDiff * xDiff + zDiff * zDiff + yDiff * yDiff
	-- local radiusSum = self.radius + otherFlag.radius
	-- local radiusSqrd = radiusSum * radiusSum
	-- return (radiusSqrd >= xyzDistanceSqrd)

	-- NEW CUBE CHECK
	local flagDepth 		= (flag.depth /2) or 0
	local otherFlagDepth 	= (otherFlag.depth /2) or 0
	local flagHeight 		= flag.height or 0
	local otherFlagHeight 	= otherFlag.height or 0
	local flagWidth 		= (flag.width / 2) or 0
	local otherFlagWidth 	= (otherFlag.width /2) or 0
	-- flag max/min values
	local xMin = flag.position.x - flagDepth - BOUND_CHK_BUFFER
	local xMax = flag.position.x + flagDepth + BOUND_CHK_BUFFER
	local yMin = flag.position.y - BOUND_CHK_BUFFER_Y
	local yMax = flag.position.y + flagHeight + BOUND_CHK_BUFFER
	local zMin = flag.position.z - flagWidth - BOUND_CHK_BUFFER
	local zMax = flag.position.z + flagWidth + BOUND_CHK_BUFFER
	-- otherFlag max/min values
	local xMin2 = otherFlag.position.x - otherFlagDepth - BOUND_CHK_BUFFER
	local xMax2 = otherFlag.position.x + otherFlagDepth + BOUND_CHK_BUFFER
	local yMin2 = otherFlag.position.y - BOUND_CHK_BUFFER_Y
	local yMax2 = otherFlag.position.y + otherFlagHeight + BOUND_CHK_BUFFER
	local zMin2 = otherFlag.position.z - otherFlagWidth - BOUND_CHK_BUFFER
	local zMax2 = otherFlag.position.z + otherFlagWidth + BOUND_CHK_BUFFER

	if  (xMin < xMax2 and xMax > xMin2) and
		(yMin < yMax2 and yMax > yMin2) and
		(zMin < zMax2 and zMax > zMin2) then
		return true
	end
	return false
end

constructLabelTable = function(labels)
	local returnTable = {}
	for labelIndex, labelTable in pairs(labels) do
		local labelColor = labelTable.color or {}
		returnTable[labelIndex] = {labelTable.label, labelTable.size, labelColor.r or 1, labelColor.g or 1, labelColor.b or 1}
	end
	return returnTable
end

-- Detect if Creator mode is enabled in this world
getCreatorModeEnabled = function()
	s_bIsCreatorModeEnabled = SaveLoad.loadGameData("CreatorModeEnabled")
end

--Compare settings to see if one has changed
compareSettings = function(self, eventSettings)
	if 	(eventSettings.allowPvP ~= s_tSettings.allowPvP) or
		(eventSettings.destructibleObjects ~= s_tSettings.destructibleObjects) or 
		(eventSettings.playerBuild ~= s_tSettings.playerBuild) or 
		(eventSettings.playerNames ~= s_tSettings.playerNames) or 
		(eventSettings.jumpsuit ~= s_tSettings.jumpsuit) or 
		(eventSettings.starvation ~= s_tSettings.starvation) or
		(eventSettings.starveTime ~= s_tSettings.starveTime) or
		(eventSettings.leaderboard ~= s_tSettings.leaderboard) or
		(eventSettings.dropItems ~= s_tSettings.dropItems) or
		(eventSettings.lobbySpawnOnly ~= s_tSettings.lobbySpawnOnly) or
		(eventSettings.respawnTimerOff ~= s_tSettings.respawnTimerOff) or
		(eventSettings.flagBuildOnly ~= s_tSettings.flagBuildOnly) or
		(eventSettings.maxFlags ~= s_tSettings.maxFlags) or
		(eventSettings.maxFlagObjects ~= s_tSettings.maxFlagObjects) then

			return true
	else
		return false
	end
end

-- Activates next queued Controller indexed by s_iControllerActivationIndex
function activateQueuedController(self)
	if s_tSettings.frameworkEnabled then
		-- Do we have another controller to activate?
		if ACTIVATION_ORDER[s_iControllerActivationIndex] and s_tControllerStates[ACTIVATION_ORDER[s_iControllerActivationIndex]].online then
			-- Log progress % complete = s_iControllerActivationIndex*5 (10,15,20%)
			self:logProgress{log = "Activating Controller["..tostring(ACTIVATION_ORDER[s_iControllerActivationIndex]).."]", percentage = s_iControllerActivationIndex*5}
			local activationEvent = {["controller"] = ACTIVATION_ORDER[s_iControllerActivationIndex]}
			-- Pass over the GameItemLoad for Controller_Spawn
			if ACTIVATION_ORDER[s_iControllerActivationIndex] == "Spawn" then
				activationEvent.GameItemLoad = s_tGameConfigs.GameItemLoad
			-- Inventory gets the jumpsuit setting
			elseif ACTIVATION_ORDER[s_iControllerActivationIndex] == "Player" then
				activationEvent.jumpsuit = s_tSettings.jumpsuit
			elseif ACTIVATION_ORDER[s_iControllerActivationIndex] == "Battle" then
				activationEvent.respawnTimerOff = s_tSettings.respawnTimerOff
			end
			SpinUpMetrics.recordControllerSpinupStart(ACTIVATION_ORDER[s_iControllerActivationIndex])
			Events.sendEvent("FRAMEWORK_ACTIVATE_CONTROLLER", activationEvent)
		-- Out of controllers to activate
		else
			-- Let's confirm they all got booted
			for i, v in pairs(s_tControllerStates) do
				if v.active == false then
					log("Controller_Game - onControllerActivated() Controller["..tostring(ACTIVATION_ORDER[i]).."] didn't register.")
					return
				end
			end
			SpinUpMetrics.recordFrameworkSpinupComplete()
			s_bAreControllersReady = true
		end
	end
	
	-- Are we ready to finalize the activation?
	if s_bAreControllersReady and not s_bHasFrameworkStarted then
		self:finalizeFrameworkActivation()
	end
end

-- Deactivates the next queues Controller indexed by s_iControllerActivationIndex
function deActivateQueuedController(self)
	if DEACTIVATION_ORDER[s_iControllerActivationIndex] and s_tControllerStates[DEACTIVATION_ORDER[s_iControllerActivationIndex]].active then
		-- Log progress, deactivate next controller
		self:logProgress{log = "Deactivating Controller["..tostring(DEACTIVATION_ORDER[s_iControllerActivationIndex]).."]", percentage = s_iControllerActivationIndex*5}
		local activationEvent = {["controller"] = DEACTIVATION_ORDER[s_iControllerActivationIndex]}
		Events.sendEvent("FRAMEWORK_DEACTIVATE_CONTROLLER", activationEvent)
	-- Out of controllers to activate
	else
		-- Let's confirm they all got deactivated
		for i, v in pairs(s_tControllerStates) do
			if v.active then
				log("Controller_Game - onControllerActivated() Controller["..tostring(DEACTIVATION_ORDER[i]).."] didn't deactivate.")
				return
			end
		end
		s_bAreControllersReady = false
	end
	
	-- Are we ready to finalize the deactivation?
	if not s_bAreControllersReady and s_bHasFrameworkStarted then
		self:finalizeFrameworkDeActivation()
	end
end

-- Finalizes Framework activation
function finalizeFrameworkActivation(self)
	-- Log progress
	self:logProgress{log = "All controllers active"}
	self:logProgress{log = "Activating players"}
	s_bHasFrameworkStarted = true
	
	local zoneInstanceID, zoneIndex, zoneType = kgp.gameGetCurrentInstance()
	Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {CID = 100, input = {zoneInstanceId = zoneInstanceID, zoneType = zoneType}, callType = "CAN_WORLD_DROP_GEMS"})
	Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {CID = 200, input = {zoneInstanceId = zoneInstanceID, zoneType = 6}, callType = "REQUEST_EVENT"})
	
	-- Instantiate all players
	for name, data in pairs (s_tPendingPlayers) do
		self:activatePlayer(data.player)				
	end
	
	if s_uInstantiatedGameItem and s_uInstantiatingPlayer then
		if s_uInstantiatedGameItem.itemType == "placeable" then
			Events.sendEvent("WOK_PLACE_GAME_ITEM", {item   	   = s_uInstantiatedGameItem,
													 player 	   = s_uInstantiatingPlayer,
													 mode   	   = "Place",
													 instantiating = true
													 })
		else
			Events.sendEvent("WOK_CREATE_SPAWNER", {item   		  = s_uInstantiatedGameItem,
													player 		  = s_uInstantiatingPlayer,
													mode   		  = "CreateSpawner",
													instantiating = true
													})
		end
	end
	
	-- Log progress
	self:logProgress{log = "All players activated", percentage = 100}
	-- Close the progress menu for each player
	for name, data in pairs (s_tPendingPlayers) do
		if s_uInstantiatingPlayer then
			if name == s_uInstantiatingPlayer.name then
				-- Close Progress menu
				Events.sendClientEvent("FRAMEWORK_CLOSE_PROGRESS", {activationAction = "Activate"}, data.player.ID)
				if s_uInstantiatingPlayer and s_iZoneType then
					local tEventData = {
						CID = 0, 
						input = {
							username			= name,
							zoneInstanceId		= s_iZoneInstanceID,
							zoneType			= s_iZoneType,
							activationAction	= "Activate"
						},
						callType = "METRIC_ACTIVATION"
					}
					Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", tEventData)						
				end
			else
				Events.sendClientEvent("FRAMEWORK_CLOSE_PROGRESS", {}, data.player.ID)
			end
		else
			Events.sendClientEvent("FRAMEWORK_CLOSE_PROGRESS", {}, data.player.ID)
		end
	end
	s_tPendingPlayers = {}
	-- Clear out the instantiating player
	s_uInstantiatingPlayer = nil
end

function finalizeFrameworkDeActivation(self)
	-- De-instantiate all players
	self:logProgress{log = "De-instantiating players"}
	
	s_tPendingPlayers = {}
	local tPlayers = getPlayers()
	-- Handle each player
	for sName, uPlayer in pairs(tPlayers) do
		if uPlayer then
			-- Put all non-players into God mode for next bootup
			if uPlayer.perm ~= 3 then
				uPlayer.mode = "God"
			end
			
			-- Insert each player into the s_tPendingPlayers table in case the Framework is booted back up
			s_tPendingPlayers[sName] = {player = uPlayer}

			if uPlayer.inZone then
				-- Close all other menus
				Events.sendClientEvent("FRAMEWORK_CLOSE_ALL_FRAMEWORK_MENUS", {}, uPlayer.ID)
				-- Reset all players' physics to be safe. NOTE: LoadProgress.xml is modal, so they won't be able to move during the shut down
				self:playerSetPhysics({user = uPlayer, reset = true})
				
				-- Inform Framework_PlayerHandler that this player's in a Framework-enabled world
				Events.sendClientEvent("FRAMEWORK_DEACTIVATE_PLAYER", {}, uPlayer.ID)

				kgp.playerDeleteZoneSpawnPoint(uPlayer.ID)

				local zonesVisited = nil
				if ZoneInfo.isLinkedZone and uPlayer.zonesVisited then
					zonesVisited = Toolbox.deepCopy(uPlayer.zonesVisited)
				end
				-- Save off stats for this player
				local playerStats = {energy = uPlayer.energy or DEF_MAX_ENERGY,
				maxEnergy = uPlayer.maxEnergy or DEF_MAX_ENERGY,
									 health = uPlayer.health or DEF_MAX_HEALTH,
									 maxHealth = uPlayer.maxHealth or DEF_MAX_HEALTH,
									 deathCount = uPlayer.deathCount or DEF_DEATH_COUNT,
									 npcSpouse = uPlayer.npcSpouse or DEF_NPC_SPOUSE,
									 followerCount = uPlayer.followerCount or DEF_FOLLOW_COUNT,
									 zonesVisited  = zonesVisited
									}
				SaveLoad.savePlayerData(uPlayer.name, "playerStats", Events.encode(playerStats))
			end
		else
			log("ERROR DETECTED - ED-7091 : finalizeFrameworkDeActivation encountered a nil player 1! sName = "..tostring(sName))
			Debug.printTableError("ERROR DETECTED - ED-7091 : tPlayers", tPlayers)
		end
	end
	
	-- De-instantiate all players
	self:logProgress{log = "Players de-instantiated.", percentage = 90}
	
	-- Update frameworkEnabled flag
	s_tSettings.frameworkEnabled = false
	kgp.gameSetFrameworkEnabled(false)
	
	-- Shut down this Controller last (Controller_Game)
	bootupFramework(self, false)
	
	-- Close the progress menu for each player
	for sName, uPlayer in pairs(tPlayers) do
		if uPlayer then
			if sName == s_uDeinstantiatingPlayer.name then
				-- Close Progress menu
				Events.sendClientEvent("FRAMEWORK_CLOSE_PROGRESS", {activationAction = "Deactivate"}, uPlayer.ID)
			else
				Events.sendClientEvent("FRAMEWORK_CLOSE_PROGRESS", {}, uPlayer.ID)
			end
		else
			log("ERROR DETECTED - ED-7109 : finalizeFrameworkDeActivation encountered a nil player 1! sName = "..tostring(sName))
			Debug.printTableError("ERROR DETECTED - ED-7091 : tPlayers", tPlayers)
		end
	end
	
	s_bHasFrameworkStarted = false
	-- Clear out the de-instantiating player
	s_uDeinstantiatingPlayer = nil
end

-------------------------------
-- Object Show / Hide Helpers
-------------------------------
function systemObjectShowForPlayer(playerID, PID, name, particleGLID)
	kgp.objectClearLabelsForPlayer(playerID, PID)
	kgp.objectAddLabelsForPlayer(playerID, PID, {{name, 0.013}})
	kgp.objectShow(PID, playerID)
	if particleGLID then
		kgp.objectSetParticleForPlayer(playerID, PID, particleGLID)
	end
end

function systemObjectHideForPlayer(playerID, PID)
	kgp.objectClearLabelsForPlayer(playerID, PID)
	kgp.objectHide(PID, playerID)
	kgp.objectRemoveParticleForPlayer(playerID, PID)
end

-- Loads the player's timed vends
function getPlayerTimedVends(playerName)
	local zoneInstanceID, zoneIndex, zoneType = kgp.gameGetCurrentInstance()
	local pulledVends = SaveLoad.loadPlayerData(playerName, "timedVends", zoneInstanceID, zoneType)
	local timedVends = {}
	if pulledVends then
		pulledVends = Events.decode(pulledVends)
	
		for PID, vendInfo in pairs(pulledVends) do
			local numPID = tonumber(PID)
			if numPID then
				timedVends[numPID] = {}
				for vendID, finishTime in pairs(vendInfo) do
					if tonumber(vendID) then  --TODO: There is some data corruption happening causing the vendID to be saved as NIL!
						timedVends[numPID][tonumber(vendID)] = finishTime
					else
						log("getPlayerTimedVends for "..tostring(playerName).." encountered a nil vendID!")
					end
				end
			else
				log("getPlayerTimedVends for "..tostring(playerName).." encountered a nil numPID!")
			end
		end
	end

	return timedVends
end

function returnCurrentTime(self, event)
	local time = kgp.gameGetCurrentTime() or 0
	--time = math.fmod(time, 2629743) --modded down to days for client use
	
	Events.sendClientEvent("RETURN_CURRENT_TIME", {timestamp = time, PID = self.PID}, event.player.ID)
end

function onHideObject(self, event)
	local tPlayers = getPlayers()
	local iPID = tonumber(event.PID)
	
	for sName, uPlayer in pairs(tPlayers) do
	-- Is this item creatorOnly?
		if uPlayer.mode == "Player" and event.hide and event.owner ~= sName then
			kgp.objectHide(iPID, uPlayer.ID)
			kgp.objectRemoveParticleForPlayer(uPlayer.ID, iPID)
		else
			kgp.objectShow(iPID, uPlayer.ID)
			local uItem = s_uItemInstances[event.PID]
			if uItem and uItem.particleGLID then
				kgp.objectSetParticleForPlayer(uPlayer.ID, iPID, uItem.particleGLID)
			end
		end
	end
end

function onSetAvailableZones(self, event)
	if event.availableZones == nil then return end

	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	SaveLoad.saveGameData("AvailableZones", event.availableZones, parentID, parentType)

	local bIsGameHome = kgp.gameHasDynamicLink()

	local availableZones = SaveLoad.loadGameData("AvailableZones", parentID, parentType)

	Events.sendClientEvent("FRAMEWORK_RETURN_PARENT_DATA",{zoneID = parentID, zoneType = parentType, availableZones = event.availableZones,  gameHome = bIsGameHome} , event.player.ID)
end