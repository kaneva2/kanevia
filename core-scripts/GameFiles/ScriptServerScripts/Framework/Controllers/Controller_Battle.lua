--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Controller_Battle.lua
include("KanevaConstants.lua")		-- Needed for effect track
include("Lib_GameDefinitions.lua")

Class.createClass( "Controller_Battle", "Controller" )

Class.import("Game")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local Debug				= _G.Debug
local GameDefinitions	= _G.GameDefinitions
local Events			= _G.Events
local Players			= _G.Players
local Class				= _G.Class

-----------------------------------------------------------------
-- Debugging Helpers
-----------------------------------------------------------------
local LOG_FUNCTION_CALL, LOG_FUNCTION_CALL_FILTER_ID = Debug.generateCustomLog("Controller_Battle - ")
Debug.enableLogForFilter(LOG_FUNCTION_CALL_FILTER_ID, false) -- Set this to true if you want to get a complete log of Controller_Inventory function calls

-- Console.ModuleOutputLevels[Class.getClassName(_thisClass)] = Console.DEBUG

-- Events
-- WEAPON-EQUIPPED, WEAPON-REMOVED, ARMOR-EQUIPPED, ARMOR-REMOVED
-- WEAPON-REGISTER, WEAPON-UNREGISTER, ARMOR-REGISTER, ARMOR-UNREGISTER, AMMO-REGISTER, AMMO-UNREGISTER
-- PLAYER-ATTACK (from client), PLAYER-ATTACK-RESP (to client)

-- Weapon Sockets
-- Left Hand, Right Hand

-- Armor sockets
-- Left Arm, Right Arm, Left Thigh/Leg, Right Thigh/Leg, Left Foot, Right Foot, Head, Chest, Back

-- Weapon registration
-- name (human friendly weapon name)
-- class (script name )
-- type (weapon type: Firearm, Melee, Projectile)
-- sockets

-- Armor registration
-- name (human friendly item name)
-- class (script name)
-- sockets

-- Ammo registration
-- name (human friendly item name)
-- class (script name)
-- weapons (compatible weapons)

-- Equip/remove
-- player (ID, name)
-- class (script name)

Class.useBehaviors({
	"Behavior_Sound"
})

Class.export({
	--{ "registerWeapon",   "pid", "className", "type", "name", "sockets", "attackInterval", "equipOnStart" , "icon", "maxAmmo"}, -- Wes suspects this isn't needed anymore
	--{ "registerSpawn",    "spawn"}, -- Un-needed?
	--{ "registerArmor",    "pid", "className", "name", "sockets" }, -- Un-needed?
	--{ "registerAmmo",     "pid", "className", "name", "weapons" }, -- Un-needed?
	--{ "unregisterWeapon", "pid", "name" }, -- Un-needed?
	--{ "unregisterArmor",  "pid", "className" }, -- Un-needed?
	--{ "unregisterAmmo",   "pid", "className" }, -- Un-needed?
	--{ "equipWeapon",      "player", "name", "pid" }, -- Un-needed?
	--{ "equipArmor",       "player", "className" }, -- Un-needed?
	--{ "removeWeapon",     "player", "name" }, -- Un-needed?
	--{ "removeArmor",      "player", "className" }, -- Un-needed?
	--{ "playerAttack",     "player" }, -- Un-needed?
	--{ "playerNextWeapon", "player" }, -- Un-needed?
	-- Player management
	{ "playerAddHealth", "event" },
	{ "playerRemoveHealth", "event" },
	{ "playerSetHealth", "event" },
	
	-- Object management
	{ "objectAttack",		"event" },
	{ "objectAddHealth",	"event" },
	{ "objectRemoveHealth",	"event" },
	{ "objectSetHealth",	"event" },

	-- inherited from Controller
	{ "config", "key", "value" }
})

--------------------------------------------------------
-- Constant definitions
--------------------------------------------------------

-- Table of events to be registered and accompanying handlers
-- NOTE: Only events needed post-Framework spinup should be here. If you need an event to respond while the Framework is still
-- offline, register for it in in create()
local CONTROLLER_EVENTS = {
	PLAYER_TARGET 					   = "onPlayerAcquiredTarget",
	PLAYER_ADD_HEALTH 				   = "playerAddHealth",
	PLAYER_REMOVE_HEALTH 			   = "playerRemoveHealth",
	FRAMEWORK_CONSUME_HEALTH_ITEM 	   = "onHealthItemConsumed",
	FRAMEWORK_ATTACK 				   = "onPlayerAttackedTarget", -- Register for attack events
	FRAMEWORK_RESPAWN_REQUEST 		   = "WOKrespawnRequest", -- Sent from HUDKaneva on respawn request
	FRAMEWORK_RESPAWN_PLAYER 		   = "playerRespawn", -- Sent from Framework_RespawnCountdown when the respawn timer's complete
	FRAMEWORK_MANUAL_RESPAWN_CANCELLED = "playerRespawnCancelled", -- Sent from Framework_RespawnCountdown when the respawn timer's complete
	FRAMEWORK_KILL_PLAYER 			   = "killPlayer", -- Sent from Controller_Game when a dead God enters Player mode
	FRAMEWORK_CANCEL_RESPAWN_TIMER 	   = "cancelRespawnTimer", -- Sent from Controller_Game when a player enters God mode dead (rage-mose swap)
	FRAMEWORK_RESPAWN_TIMER_MODIFIED   = "onRespawnTimerModified" --Sent from Controller_Game when the respawn timer setting is modified
}

local DEFAULT_GAME_RESPAWN_INTERVAL = 10
local MANUAL_RESPAWN_TIME = 15 -- How long it takes to respawn when requested manually (HUDKaneva respawn button)

-- Miss chanced by percentage of range to target (The closer you are to the weapon's max range, the greater the miss chance)
local MISS_CHANCES = {
	[25]	= .5,
	[50]	= 1,
	[75]	= 25,
	[100]	= 50
}
local MELEE_MISS_CHANCE = 15

local DAMAGED_PLAYER_PARTICLE = 4524826
local DAMAGED_PARTICLE_TIME = 3000 -- How long in milliseconds this particle will play on hit

local DURABILITY_PER_HIT = 1 -- How much durability damage is inflicted per attack?

--------------------------------------------------------
-- Local variable definitions
 --------------------------------------------------------
local s_uItemInstances				-- Table of all known objects in the world {PID: {itemData}}
local s_uPlayers					-- Table of players maintained in a sharedData {playerName: {playerData}}
local s_uItemProperties						--> SharedData holding properties for all items that can be created in the current world.

local s_enableHUDObjects = {weapon = {}} -- Table of objects that have opened the various HUD elements -- Un-needed?

local s_zoneInstanceId, s_zoneType

local s_controllerReady = false

local s_respawnTimer = {} -- Table of timed events for respawning players

local s_respawnTimerOff = false -- Should the player get a respawn timer?

local s_eventFlagOpen = {}  --Table of players that have the event flag open

--------------------------------------------------------
-- Local function declarations
--------------------------------------------------------
local calculateWeaponDamage -- (level, multiplier, healingWeapon(optional))
local mitigateDamage -- (attacked, damage)
local checkForMiss -- (attacker, attacked, range)
local decrementArmorDurability -- (player)




-----------------------------------------------------------------
-- Main entry to and exit from this controller
-----------------------------------------------------------------
function create(self)
	LOG_FUNCTION_CALL("create")
	
	_super.create(self)
	
	-- Register Controller_Battle with Controller_Game
	Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Battle", state = {["online"] = true}})
	
	-- Request for current controller state
	Events.registerHandler( "FRAMEWORK_QUERY_CONTROLLER_STATE", self.onControllerStateQuery, self )
	-- Register an activate event for this controller
	Events.registerHandler( "FRAMEWORK_ACTIVATE_CONTROLLER", self.onActivate, self )
	Events.registerHandler( "FRAMEWORK_DEACTIVATE_CONTROLLER", self.onDeactivate, self )
	Events.registerHandler( "FRAMEWORK_RETURN_ZONE_DATA", self.returnZoneData, self)

	Events.registerHandler("UPDATE_PAINT_METRICS", self.updatePaintMetricsHandler, self)
	Events.registerHandler("UPDATE_EMOTE_PARTICLE", self.updateEmoteParticleHandler, self)
	Events.registerHandler("FRAMEWORK_EVENT_FLAG_MENU", self.eventFlagMenuHandler, self)
	
	s_uItemInstances	= SharedData.new("items", false)
	s_uPlayers			= SharedData.new("players", false)
	s_uItemProperties	= SharedData.new("gameItemProperties", false)
end

-----------------------------------------------------------------
-- Controller event handlers
-----------------------------------------------------------------


-- Request for current controller state
function onControllerStateQuery(self, event)
	LOG_FUNCTION_CALL("onControllerStateQuery")
	
	local state = {online = true}
	if s_controllerReady then
		state.active = s_controllerReady
	end
	Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Battle", state = state})
end

-- Handles controller activation events
function onActivate(self, event)
	LOG_FUNCTION_CALL("onActivate")
	
	-- Time to activate?
	if event.controller and event.controller == "Battle" then
		-- Log beginning of handler registration
		Game.logProgress{log = "Battle - Registering event handlers"}

		s_respawnTimerOff = event.respawnTimerOff
		-- Register all event handlers
		for event, handler in pairs(CONTROLLER_EVENTS) do
			Events.registerHandler(event, self[handler], self)
		end
		
		-- Log event handler registration completion
		Game.logProgress{log = "Battle - Event handlers registered"}
		
		s_controllerReady = true
		-- Inform Controller_Game that Battle is finished activating
		Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Battle", state = {["active"] = true}})
	end
end

-- Shut down this Controller
function onDeactivate(self, event)
	LOG_FUNCTION_CALL("onDeactivate")
	
	-- Time to activate?
	if event.controller and event.controller == "Battle" then
		-- Log beginning of handler un-registration
		Game.logProgress{log = "Battle - Un-registering event handlers"}
		-- Unregister all un-needed event handlers
		for event, handler in pairs(CONTROLLER_EVENTS) do
			Events.unregisterHandler(event, self[handler], self)
		end
		-- Log event un-registration completion
		Game.logProgress{log = "Battle - Event handlers un-registered"}
		
		s_controllerReady = false
		-- Inform Controller_Game that Battle is finished de-activating
		Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Battle", state = {active = false}})
	end
end

-- Called when a player targets something
function onPlayerAcquiredTarget(self, event)
	LOG_FUNCTION_CALL("onPlayerAcquiredTarget")
	
	-- Error check inputs
	if event == nil then log("Controller_Battle - onPlayerAcquiredTarget() event is nil"); return end

	local sTargetType = event.type
	if sTargetType == nil then log("Controller_Battle - onPlayerAcquiredTarget() sTargetType is nil"); return end
	local uTargetingPlayer = event.player
	if uTargetingPlayer == nil then log("Controller_Battle - onPlayerAcquiredTarget() targeter is nil"); return end
	
	-- Clear current target?
	if sTargetType == "Clear" then
		uTargetingPlayer.target = {}
		return
	end
	
	local tNewTarget = {}
	
	-- Extract SD from the target
	-- If a player was targeted
	if sTargetType == "Player" then
		local targetName = event.name
		if targetName == nil then return end
		local uPlayer = s_uPlayers[targetName]
		for index, value in pairs(uPlayer) do
			tNewTarget[index] = value
		end
		tNewTarget.ID = targetName
	
	-- If an object was targeted
	elseif sTargetType == "Object" then
		local targetPID = event.id
		if targetPID == nil then return end
		local target = s_uItemInstances[targetPID]
		if target == nil then log("Controller_Battle - onPlayerAcquiredTarget() - Target["..tostring(targetPID).."] isn't in s_uItemInstances") return end
		for index, value in pairs(target) do
			tNewTarget[index] = value
		end
		tNewTarget.ID = targetPID
	
	-- Invalid target passed in
	else 
		log("Controller_Battle - onPlayerAcquiredTarget() sTargetType is invalid ["..tostring(sTargetType).."]"); 
		return
	end
	
	-- New target clicked
	local uCurrentTarget = uTargetingPlayer.target
	if uCurrentTarget and ((uCurrentTarget.ID == nil) or (uCurrentTarget.ID ~= tNewTarget.ID)) then
		--self:clearTarget{user = uTargetingPlayer}
		if tNewTarget.maxHealth == nil or tNewTarget.maxHealth == 0 then
			return
		end
		
		-- Assign new target
		if sTargetType == "Object" then
			--kgp.playerAddHealthIndicator(uTargetingPlayer.ID, 3, tostring(event.id), (tNewTarget.health/tNewTarget.maxHealth)*100)
			uCurrentTarget.ID = event.id
		elseif sTargetType == "Player" then
			--local newTargetID = tNewTarget.name
			--kgp.playerAddHealthIndicator(uTargetingPlayer.ID, 4, newTargetID, (tNewTarget.health/tNewTarget.maxHealth)*100)
			uCurrentTarget.ID = tNewTarget.name
		end

		-- Set new target fields minus ID
		for index, value in pairs(tNewTarget) do
			if index ~= "ID" then
				uCurrentTarget[index] = value
			end
		end
	end
end

-- Kills a player
function killPlayer(self, event)
	LOG_FUNCTION_CALL("killPlayer")
	
	if (event == nil) or (event.user == nil) then return end

	local dyingPlayer = Players.determineUser(event.user, s_uPlayers)
	if dyingPlayer == nil then log("Controller_Battle - killPlayer() Failed to extract dyingPlayer from user["..tostring(event.user).."]."); return end

	-- Don't kill Gods
	if dyingPlayer.mode == "God" then return end
	if dyingPlayer == nil then log("Controller_Battle - killPlayer() - Failed to extract user SD from user["..tostring(event.user).."]") return end
	
	Game.playerSetPhysics({user = event.user, lock = true})
	
	dyingPlayer.alive = false
	
	Events.sendEvent("PLAYER_DIED", {player = dyingPlayer})
	Events.sendClientEvent("DROP_PLAYER_INVENTORY", {dropLocation = Toolbox.deepCopy(dyingPlayer.position)}, dyingPlayer.ID)
	Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, dyingPlayer.ID)

	Events.setTimer(.5, function()
							kgp.playerSetAnimation(dyingPlayer.ID, GameDefinitions.DEFAULT_PLAYER_ANIMS.DEATH)
						 end)

	s_respawnTimer[dyingPlayer.name] = Events.setTimer(DEFAULT_GAME_RESPAWN_INTERVAL, function()
																						self:playerRespawn{player = dyingPlayer}
																					  end)
	
	-- Remove particles of dying player, if any
	if dyingPlayer.particles then
		for key,particlesData in pairs(dyingPlayer.particles) do
			if particlesData then
				Game.playerRemoveParticle{	
					user 			= dyingPlayer,
					particleKey 	= key,
					boneName 		= particlesData.bone,
					particleGLID 	= particle
				}
			end
		end
	end

	-- Inform the client that this player is dead
	Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = dyingPlayer.name, modifiedFields = {alive = false,
																									   health = 0}})

	if event.attacker and event.attacker.type == "Player" and event.attacker.name then
		self:recordPVPKill(dyingPlayer, event.attacker)
	elseif event.attacker then
		self:recordMonsterKill(dyingPlayer, event.attacker)
	else
		Events.sendEvent("PLAYER_DIED", {player = dyingPlayer})
		self:recordOtherKill(dyingPlayer)
	end
end

function recordPVPKill(self, dyingPlayer, attacker)
	LOG_FUNCTION_CALL("recordPVPKill")
	
	if dyingPlayer == nil then log("Controller_Battle - recordPVPKill() dyingPlayer is nil."); return false end
	if attacker == nil then log("Controller_Battle - recordPVPKill() attacker is nil."); return false end

	local zoneInstanceId, zoneIndex, zoneType = kgp.gameGetCurrentInstance()
	local metricsData = {
		username 	   = attacker.name,
		victim		   = dyingPlayer.name,
		zoneInstanceId = zoneInstanceId,
		zoneType 	   = zoneType,
		timestamp	   = kgp.gameGetCurrentTime()
	}

	kgp.scriptRecordMetric("pvp_kill_log", metricsData)
end

function recordMonsterKill(self, dyingPlayer, attacker)
	LOG_FUNCTION_CALL("recordMonsterKill")
	
	if dyingPlayer == nil then log("Controller_Battle - recordMonsterKill() dyingPlayer is nil."); return false end
	if attacker == nil then log("Controller_Battle - recordMonsterKill() attacker is nil."); return false end

	local zoneInstanceId, zoneIndex, zoneType = kgp.gameGetCurrentInstance()
	local metricsData = {
		monsterName 	= attacker.name,
		monsterType 	= attacker.type,
		victim		   	= dyingPlayer.name,
		zoneInstanceId 	= zoneInstanceId,
		zoneType 	   	= zoneType,
		timestamp	   	= kgp.gameGetCurrentTime()
	}

	kgp.scriptRecordMetric("monster_kill_log", metricsData)
end

function recordOtherKill(self, dyingPlayer)
	LOG_FUNCTION_CALL("recordOtherKill")
	
	if dyingPlayer == nil then log("Controller_Battle - recordMonsterKill() dyingPlayer is nil."); return false end
	
	local zoneInstanceId, zoneIndex, zoneType = kgp.gameGetCurrentInstance()
	local metricsData = {
		victim		   = dyingPlayer.name,
		zoneInstanceId = zoneInstanceId,
		zoneType 	   = zoneType,
		timestamp	   = kgp.gameGetCurrentTime()
	}

	kgp.scriptRecordMetric("other_kill_log", metricsData)
end

-- Caught from Controller_Game when a player enters God mode dead
function cancelRespawnTimer(self, event)
	LOG_FUNCTION_CALL("cancelRespawnTimer")
	
	if self == nil then log("Controller_Battle - cancelRespawnTimer() self is nil."); return end
	if event == nil then log("Controller_Battle - cancelRespawnTimer() event is nil."); return end
	local player = event.player
	if player == nil then log("Controller_Battle - cancelRespawnTimer() event.player is nil."); return end
	
	-- Does this player have a queued event timer for respawning?
	if s_respawnTimer[player.name] then
		Events.cancelTimer(s_respawnTimer[player.name])
		s_respawnTimer[player.name] = nil
	end
end

-- Respawn Timer setting modified
function onRespawnTimerModified(self, event)
	LOG_FUNCTION_CALL("onRespawnTimerModified")
	
	s_respawnTimerOff = event.respawnTimerOff == true
end


-- Adds health to an object
function objectAddHealth(self, event)
	LOG_FUNCTION_CALL("objectAddHealth")
	
	if self == nil then log("Controller_Battle - objectAddHealth() self is nil."); return end
	if event == nil then log("Controller_Battle - objectAddHealth() event is nil."); return end
	if event.PID == nil then log("Controller_Battle - objectAddHealth() event.PID is nil."); return end
	if event.health == nil then log("Controller_Battle - objectAddHealth() event.health is nil."); return end
	local object    = s_uItemInstances[event.PID]
	if object == nil then log("Controller_Battle - objectAddHealth() No object in SharedData is registered with PID["..tostring(event.PID).."].") return end
	local healer = event.healer
	object.maxHealth = event.maxHealth or object.maxHealth
	
	-- If the healer's a player, send them a floater
	if healer and healer.type and healer.type == "Player" then
		if event.health < 0 then
			event.health = event.health * -1
		end
		Events.sendClientEvent("GENERATE_FLOATER", {text = "+"..tostring(math.ceil(event.health)), target = event.PID, color = GameDefinitions.COLORS.GREEN}, healer.ID)
	end
	
	-- Don't add health if the object's at max health
	if object.health < object.maxHealth then
		--log("--- object.maxHealth : ".. tostring(object.maxHealth).. "\n\tobject.health : ".. tostring(object.health).. "\n\tevent.health : ".. tostring(event.health))

		object.health = math.min(object.health + event.health, object.maxHealth)
		
		-- Only SD objects care about healing events. No one cares about placed objects. They're stinky losers
		if object and healer then
			Events.sendEvent("OBJECT_HEALED", {healed = object, healer = healer, regenAmount = event.health})
		end
		
		Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = event.PID, modifiedFields = {health = object.health}})
	end
end

-- Removes health from an object
function objectRemoveHealth(self, event)
	LOG_FUNCTION_CALL("objectRemoveHealth")
	
	if self == nil then log("Controller_Battle - objectRemoveHealth() self is nil."); return end
	if event == nil then log("Controller_Battle - objectRemoveHealth() event is nil."); return end
	if event.PID == nil then log("Controller_Battle - objectRemoveHealth() event.PID is nil."); return end
	if event.health == nil then log("Controller_Battle - objectRemoveHealth() event.health is nil."); return end
	local object    = s_uItemInstances[event.PID]
	if object == nil then log("Controller_Battle - objectAddHealth() No object in SharedData is registered with PID["..tostring(event.PID).."].") return end
	local attacker = event.attacker
	
	-- If the attacker's a player, send them a floater
	if attacker and attacker.type and attacker.type == "Player" then
		Events.sendClientEvent("GENERATE_FLOATER", {text = "-"..tostring(math.ceil(event.health)), target = event.PID, color = GameDefinitions.COLORS.RED}, attacker.ID)
	elseif attacker and attacker.master and attacker.master.ID then
		Events.sendClientEvent("GENERATE_FLOATER", {text = "-"..tostring(math.ceil(event.health)), target = event.PID, color = GameDefinitions.COLORS.RED}, attacker.master.ID)
	end
	
	-- Don't add health if the object's at max health
	if object.health > 0 then
		object.health = math.max(object.health - event.health, 0)

		-- Only mapObjects get object damaged events. Nobody cares about placed objects
		if attacker then 
			if object then
				Events.sendEvent("OBJECT_DAMAGED", {attacked = object, attacker = attacker, damage = event.health})
			end
		end 
		
		-- If the object died
		if object.health <= 0 then
			self:killObject({PID = event.PID, attacker = attacker})
		else
			-- Update this object's health bar on the client
			Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = event.PID, modifiedFields = {health = object.health}})
		end
	end
end

-- Called when an object's health value is modified
function objectSetHealth(self, event)
	LOG_FUNCTION_CALL("objectSetHealth")
	
	if self == nil then log("Controller_Battle - objectSetHealth() self is nil."); return end
	if event == nil then log("Controller_Battle - objectSetHealth() event is nil."); return end
	if event.PID == nil then log("Controller_Battle - objectSetHealth() event.PID is nil."); return end
	local object = s_uItemInstances[event.PID]
	if object == nil then log("Controller_Battle - objectSetHealth() No object in the mapis registered to PID["..tostring(event.PID).."]."); return end
	
	local percentage = event.percentage
	local customHealth = event.health
	
	-- If a custom health value was passed in
	if customHealth then
		-- Range check health
		if customHealth < 0 then customHealth = 0 end
		if customHealth > object.maxHealth then customHealth = object.maxHealth end
		object.health = customHealth
	elseif percentage then
		-- Range check percentage
		if percentage < 0 then percentage = 0 end
		if percentage > 100 then percentage = 100 end
		
		object.health = object.maxHealth * (percentage/100)
	end
	
	-- If the object died
	if object.health <= 0 then
		self:killObject({PID = event.PID, attacker = attacker})
	else
		Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = event.PID, modifiedFields = {health = object.health}})
	end
end

-- Called when an object has been killed through combat
function killObject(self, event)
	LOG_FUNCTION_CALL("killObject")
	
	if self == nil then log("Controller_Battle - killObject() self is nil."); return end
	if event == nil then log("Controller_Battle - killObject() event is nil."); return end
	if event.PID == nil then log("Controller_Battle - killObject() event.PID is nil."); return end
	local object = s_uItemInstances[event.PID]
	if object == nil then log("Controller_Battle - killObject() No object in the map is registered to PID["..tostring(event.PID).."]."); return end
	
	object.alive = false
	
	-- Detect if this item had loot to drop
	if object.loot then
		local canDropGemBox = (object.type == "Monster")
		if event.attacker and event.attacker.ID then 
			object.attackerID = event.attacker.ID 
		end
		Events.sendEvent("GENERATE_LOOT_CHEST", {object = object, despawnTime = 300, canDropGemBox = canDropGemBox})
	end

	-- Broadcast the dead for whoever cares
	Events.sendEvent("OBJECT_DIED", {object = object})
	if event.attacker and event.attacker.name then
		if object.type == "Monster" then
			self:recordKillMonster(object, event.attacker)
			if event.attacker.ID and object.UNID then
				Events.sendClientEvent("FRAMEWORK_TRACK_MONSTER_KILL", {}, event.attacker.ID)
				Events.sendClientEvent("FRAMEWORK_QUEST_HANDLER_RECORD_KILL", {UNID = object.UNID}, event.attacker.ID)
			end
		elseif object.type == "Animal" then
			if event.attacker.ID and object.UNID then
				Events.sendClientEvent("FRAMEWORK_QUEST_HANDLER_RECORD_KILL", {UNID = object.UNID}, event.attacker.ID)
			end
		elseif object.type == "Harvestable" then
			if event.attacker.ID then
				Events.sendClientEvent("FRAMEWORK_TRACK_HARVEST_COUNT", {}, event.attacker.ID)
			end
		end
	end
	
	-- If this item was spawned, let it clean itself up on death 
	if not object.spawnerPID and object.type ~= "Pet" and object.type ~= "Actor" then
		kgp.gameItemDeletePlacement(event.PID)
		Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = event.PID})
	end

	Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = event.PID, modifiedFields = {alive = false}})
end

function recordKillMonster(self, object, attacker)
	LOG_FUNCTION_CALL("recordKillMonster")
	
	if object == nil then log("Controller_Battle - recordKillMonster() object is nil."); return false end
	if attacker == nil then log("Controller_Battle - recordKillMonster() attacker is nil."); return false end

	local zoneInstanceId, zoneIndex, zoneType = kgp.gameGetCurrentInstance()
	local metricsData = {
		username 	   = attacker.name,
		interaction	   = "killedObject",
		zoneInstanceId = zoneInstanceId,
		zoneType 	   = zoneType,
		gameItemName   = object.name,
		gameItemId 	   = object.UNID,
		timestamp      = kgp.gameGetCurrentTime()
	}

	kgp.scriptRecordMetric("monster_interaction_log", metricsData)
end

-- Add health to a player
function playerAddHealth(self, event)
	LOG_FUNCTION_CALL("playerAddHealth")
	
	if self == nil then log("Controller_Battle - playerAddHealth() self is nil."); return end
	if event == nil then log("Controller_Battle - playerAddHealth() event is nil."); return end
	if event.health == nil then log("Controller_Battle - playerAddHealth() event.health is nil."); return end

	local user 		  = Players.determineUser(event.user, s_uPlayers)
	if user 		  == nil then log("Controller_Battle - playerAddHealth() Failed to extract user."); return end
	if user.ID 		  == nil then log("Controller_Battle - playerAddHealth() user.ID is nil."); return end
	if user.health 	  == nil then log("Controller_Battle - playerAddHealth() user.health is nil."); return end
	if user.maxHealth == nil then log("Controller_Battle - playerAddHealth() user.maxHealth is nil."); return end
	if user.name 	  == nil then log("Controller_Battle - playerAddHealth() user.name is nil."); return end
	
	local healer = event.healer -- Optional param if we wanna do something for the person who healed this person
	
	-- Generate a floater for the target on him/herself
	--Events.sendClientEvent("GENERATE_FLOATER", {text = "+", target = user.name, color = GameDefinitions.COLORS.GREEN}, user.ID)
	if healer and healer.ID then
		-- Generate a floater for the attacker
		Events.sendClientEvent("GENERATE_FLOATER", {text = "+"..tostring(math.ceil(event.health)), target = user.name, color = GameDefinitions.COLORS.GREEN}, healer.ID)
		Events.sendClientEvent("GENERATE_FLOATER", {text = "+"..tostring(math.ceil(event.health)), target = user.name, color = GameDefinitions.COLORS.GREEN}, user.ID)
	end
	
	-- Don't add health if player is currently maxed out
	if user.health < user.maxHealth then
		local healthGain = event.health
		
		user.health = math.min(user.health + healthGain, user.maxHealth)

		user.health = user.health > user.maxHealth and user.maxHealth or user.health
		
		-- Update HUD
		Events.sendClientEvent("FRAMEWORK_SET_HEALTH", {health = user.health, maxHealth = user.maxHealth}, user.ID)
		
		Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = user.name, modifiedFields = {health = user.health}})
	end
end

-- Should this be moved back to battle?
-- Remove health from a player
function playerRemoveHealth(self, event)
	LOG_FUNCTION_CALL("playerRemoveHealth")
	
	if self == nil then log("Controller_Battle - playerRemoveHealth() self is nil."); return end
	if event == nil then log("Controller_Battle - playerRemoveHealth() event is nil."); return end
	if event.health == nil then log("Controller_Battle - playerRemoveHealth() event.health is nil."); return end
	
	local user 		  = Players.determineUser(event.user, s_uPlayers)
	if user 		  == nil then log("Controller_Battle - playerRemoveHealth() Failed to extract user " .. tostring(event.user)); return end
	if user.ID 		  == nil then log("Controller_Battle - playerRemoveHealth() user.ID is nil."); return end
	if user.health 	  == nil then log("Controller_Battle - playerRemoveHealth() user.health is nil."); return end
	if user.maxHealth == nil then log("Controller_Battle - playerRemoveHealth() user.maxHealth is nil."); return end
	if user.name 	  == nil then log("Controller_Battle - playerRemoveHealth() user.name is nil."); return end
	if user.mode 	  == nil then log("Controller_Battle - playerRemoveHealth() user.mode is nil."); return end

	if user.loading then log("Controller_Battle - playerRemoveHealth() user.loading is true.") return end
	if s_eventFlagOpen[user.name] then  return end

	-- Can't damage Gods
	if user.mode == "God" then return end
	
	local attacker = event.attacker -- Optional param if we wanna do something for the person who attacked (auto-target, etc)
	local generateFloater = true

	--Target vehicle if in a car
	if user.inVehicle and user.occupiedVehicle then
		local vehicle = user.occupiedVehicle
		if vehicle.targetable == "true" then
			self:objectRemoveHealth({PID = tonumber(vehicle.PID), health = event.health, attacker = attacker})
			Events.sendClientEvent("FRAMEWORK_SET_VEHICLE_HEALTH", {health = vehicle.health, maxHealth = vehicle.maxHealth}, user.ID)
			--If less then 5 percent, leave vehicle
			if math.ceil((vehicle.health / vehicle.maxHealth) * 100) < 5 then 
				Events.sendEvent("FRAMEWORK_PLAYER_LEAVE_VEHICLE", {player = user})
				kgp.playerShowStatusInfo(user.ID, (vehicle.name or "This vehicle").. " is broken and needs to be repaired before driving.", 3, 255, 0, 0)
			end
		else
			generateFloater = false
		end
	-- Don't remove health if player is already dead
	elseif user.health > 0 then
		local healthLoss = event.health
		
		user.health = math.max(user.health - healthLoss, 0)

		user.health = user.health > user.maxHealth and user.maxHealth or user.health

		-- Update HUD
		local starveAlert = ((event.source and event.source == "starvation") and event.alert)
		Events.sendClientEvent("FRAMEWORK_SET_HEALTH", {health = user.health, maxHealth = user.maxHealth, flash = event.flash, alert = starveAlert}, user.ID)
		
		-- Did the player die?
		if user.health <= 0 then
			-- Log a kill if we had an attacker
			if attacker then
				Game.writeToTicker({color = {r = 255, g = 0, b = 0}, message = tostring(attacker.name).." killed "..tostring(user.name).."."})
			end
			self:killPlayer({user = user.ID, attacker = attacker})
		else
			Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = user.name, modifiedFields = {health = user.health}})
		end
		
		-- Special cases for attacked reaction
		if attacker then
			-- Play blood splatter particle
			Game.playerSetParticle{
				user 			= user,
				particleKey 	= "playerDamage",
				boneName 		= "Bip01 Spine1",
				particleGLID	= DAMAGED_PLAYER_PARTICLE,
				offset			= {x = -4, y = 0, z = 0},
				forwardDir		= {x = -1, y = 0, z = 0}
			}

			-- Play damage sound?
			if user.damageSound then
				local lossPercentage = healthLoss/user.maxHealth
				if (lossPercentage) > .2 then
					Events.setTimer( .5, function () Behavior_Sound.play({soundID = user.damageSound}) end )
				end
			end

			Events.sendEvent("PLAYER_DAMAGED", {attacked = user, attacker = attacker, damage = healthLoss})
			
			-- Cancel manual respawn if one is pending
			if user.attemptingManualRespawn then
				Events.sendClientEvent("FRAMEWORK_CANCEL_RESPAWN", {}, user.ID)
			end
			
			-- Decrement armor durability
			decrementArmorDurability(user)
		end
	end

	-- Generate a floater for the target on him/herself
	if generateFloater then
		Events.setTimer( .1, function () Events.sendClientEvent("GENERATE_FLOATER", {text = "-"..tostring(math.ceil(event.health)), target = user.name, color = GameDefinitions.COLORS.RED}, user.ID) end )
		if attacker and attacker.ID then
			-- Generate a floater for the attacker
			Events.sendClientEvent("GENERATE_FLOATER", {text = "-"..tostring(math.ceil(event.health)), target = user.name, color = GameDefinitions.COLORS.RED}, attacker.ID)
		elseif attacker and attacker.master and attacker.master.ID then
			Events.sendClientEvent("GENERATE_FLOATER", {text = "-"..tostring(math.ceil(event.health)), target = user.name, color = GameDefinitions.COLORS.RED}, attacker.master.ID)
		end
	end
end

-- Set health for a player
function playerSetHealth(self, event)
	LOG_FUNCTION_CALL("playerSetHealth")
	
	if self == nil then log("Controller_Battle - playerSetHealth() self is nil."); return end
	if event == nil then log("Controller_Battle - playerSetHealth() event is nil."); return end
	
	local user = Players.determineUser(event.user, s_uPlayers)
	if user == nil then log("Controller_Battle - playerSetHealth() Failed to extract user."); return end
	if user.ID == nil then log("Controller_Battle - playerSetHealth() user.ID is nil."); return end
	if user.health == nil then log("Controller_Battle - playerSetHealth() user.health is nil."); return end
	if user.maxHealth == nil then log("Controller_Battle - playerSetHealth() user.maxHealth is nil."); return end
	
	local percentage = event.percentage
	local customHealth = event.health
	
	-- If a custom health value was passed in
	if customHealth then
		-- Range check health
		if customHealth < 0 then customHealth = 0 end
		if customHealth > user.maxHealth then customHealth = user.maxHealth end
		user.health = customHealth
	elseif percentage then
		-- Range check percentage
		if percentage < 0 then percentage = 0 end
		if percentage > 100 then percentage = 100 end
		
		user.health = user.maxHealth * (percentage/100)
	end
	
	user.health = user.health > user.maxHealth and user.maxHealth or user.health

	-- If the player's dead
	if user.health <= 0 then
		self:killPlayer({user = user.ID})
	else
		Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = user.name, modifiedFields = {health = user.health}})
	end
	
	-- Update HUD
	Events.sendClientEvent("FRAMEWORK_SET_HEALTH", {health = user.health, maxHealth = user.maxHealth}, user.ID)
end

-- Request from HUDKanevafor a player to respawn
function WOKrespawnRequest(self, event)
	LOG_FUNCTION_CALL("WOKrespawnRequest")
	
	if self == nil then log("Controller_Battle - WOKrespawnRequest() self is nil."); return end
	if event == nil then log("Controller_Battle - WOKrespawnRequest() event is nil."); return end
	local player = event.player
	if player == nil then log("Controller_Battle - WOKrespawnRequest() event.player is nil."); return end
	
	-- Alive players get the timer

	if player.alive and player.mode ~= "God" and not s_respawnTimerOff then
		player.attemptingManualRespawn = true
		-- Open the respawn countdown 
		kgp.playerLoadMenu(player.ID, "Framework_RespawnCountdown.xml")
		Events.sendClientEvent("FRAMEWORK_RESPAWN_TIME", {respawnTime = MANUAL_RESPAWN_TIME}, player.ID)
	-- Gods respawn at a player spawn point
	elseif player.mode == "God" or s_respawnTimerOff  then
		self:playerRespawn{player = player}
	end
end

-- Respawn the player TODO: Make into user-accessible API?
--TODO: Move to player controller
function playerRespawn(self, event)
	LOG_FUNCTION_CALL("playerRespawn")
	
	if self == nil then log("Controller_Battle - playerRespawn() self is nil."); return end
	if event == nil then log("Controller_Battle - playerRespawn() event is nil."); return end
	local player = event.player
	if player == nil then log("Controller_Battle - playerRespawn() event.player is nil."); return end
	if player.ID == nil then log("Controller_Battle - playerRespawn() player.ID is nil."); return end
	if player.name == nil then log("Controller_Battle - playerRespawn() player.name is nil."); return end
	
	-- Clear respawn timer event pointer
	s_respawnTimer[player.name] = nil
	
	Game.spawnPlayer({user = player})

	if player.mode ~= "God" then
		-- Send event that the player is alive
		player.alive = true
		-- Stop any active animations
		kgp.playerSetAnimation(player.ID, 0)
		-- Set default animations
		local gender = player.gender
		if gender == "F" then
			kgp.playerDefineAnimation(player.ID, GameDefinitions.DEFAULT_ANIMS.F.STAND, GameDefinitions.DEFAULT_ANIMS.F.RUN, GameDefinitions.DEFAULT_ANIMS.F.WALK, GameDefinitions.DEFAULT_ANIMS.F.JUMP)
		elseif gender == "M" then
			kgp.playerDefineAnimation(player.ID, GameDefinitions.DEFAULT_ANIMS.M.STAND, GameDefinitions.DEFAULT_ANIMS.M.RUN, GameDefinitions.DEFAULT_ANIMS.M.WALK, GameDefinitions.DEFAULT_ANIMS.M.JUMP)
		else
			log("ERROR DETECTED - ED-7420 : playerRespawn unable to set animations due to invalid gender!  gender = "..tostring(gender)..", player.name = "..tostring(player and player.name))
		end
		-- Unlock player
		Game.playerSetPhysics({user = player.ID, reset = true})
		
		-- Was this respawn queued manually?
		if player.attemptingManualRespawn or event.forcedRespawn then
			player.attemptingManualRespawn = false
		else
			self:playerSetHealth({user = player, percentage = 100})
			Game.playerSetEnergy({user = player, percentage = 100})
		end
		
		-- Inform the client that this player is alive
		Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = player.name, modifiedFields = {alive = true}})
	end
end

-- Called when a manual player respawn has been cancelled
function playerRespawnCancelled(self, event)
	LOG_FUNCTION_CALL("playerRespawnCancelled")
	
	if self == nil then log("Controller_Battle - playerRespawnCancelled() self is nil."); return end
	if event == nil then log("Controller_Battle - playerRespawnCancelled() event is nil."); return end
	local player = event.player
	if player == nil then log("Controller_Battle - playerRespawnCancelled() event.player is nil."); return end
	-- Un-queue this player's manualRespawn flag
	if player.attemptingManualRespawn then
		player.attemptingManualRespawn = false
	end
end

-- Called when a health item is consumed
function onHealthItemConsumed(self, event)
	LOG_FUNCTION_CALL("onHealthItemConsumed")
	
	if self == nil then log("Controller_Battle - onHealthItemConsumed() - self is nil."); return end
	if event == nil then log("Controller_Battle - onHealthItemConsumed() - event is nil."); return end
	local player = event.player
	if player == nil then log("Controller_Battle - onHealthItemConsumed() - event.player is nil."); return end
	if event.consumeValue == nil then log("Controller_Battle - onHealthItemConsumed() - event.consumeValue is nil."); return end
	if player.maxHealth == nil then 
		log("ERROR DETECTED - ED-6318 : player.maxHealth is nil. player = "..tostring(player)..", player.name = " ..tostring(player.name) .. ", player.health = " .. tostring(player.health))
		return
	end
	if event.itemEffects == nil then log("Controller_Battle - onHealthItemConsumed() - event.itemEffects is nil."); return end
	
	-- Treat consumeValue as a percentage of player's total health
	local healthDelta = player.maxHealth * (event.consumeValue/100)
	-- Add health
	if event.consumeValue > 0 then
		self:playerAddHealth({user = player, health = healthDelta})
	-- Remove health
	else
		self:playerRemoveHealth({user = player, health = healthDelta})
	end
	-- Play animation, sound, and particle
	if event.itemEffects then
		self:healthConsumableEffects(player, event.itemEffects)
	end
end

-- Animation, emote, and sound for consuming a health item
function healthConsumableEffects(self, player, itemEffects)
	LOG_FUNCTION_CALL("healthConsumableEffects")
	
	-- 1: Animation
	local gender = player.gender or "M"
	local emoteAnim = GameDefinitions.DEFAULT_PLAYER_ANIMS.EAT
	-- Get gendered emote
	if gender == "M" and itemEffects.emoteM and itemEffects.emoteM ~= 0 then
		emoteAnim = itemEffects.emoteM
	elseif gender == "F" and itemEffects.emoteF and itemEffects.emoteF ~= 0 then
		emoteAnim = itemEffects.emoteF
	end
	Game.playerSetAnimation{user = player, animation = emoteAnim} 

	-- 2: Emote -- Where does the particle attach?
	if itemEffects.effect and itemEffects.effect ~= 0 then
		local particle = itemEffects.effect -- take as input
		local bone = "Bip01 Head" -- take as input?
		local offset 		= {0, 0, 0}
		local forwardDir 	= {0, 0, 0}
		local upDir 		= {0, 0, 0}
		if bone == "Bip01 Head" then
			local offset 		= {2, 0, 0}
		end
		local attachKeyIndex = 0
		local attachKey = bone.." slotID "..tostring(attachKeyIndex)
		if player.particles then
			 -- Get number of particles
			local particleCount = 0
			for key,value in pairs(player.particles) do
				particleCount = particleCount + 1
			end
			-- If there are already particles using the intended index, update the index	
			local keyMatch = true
			for numericalIndex = attachKeyIndex, particleCount, 1 do
				if keyMatch == true then -- Do not check other numerical values unless a match is found
					attachKey = bone.." slotID "..tostring(numericalIndex) -- Test key using current numericalIndex
					keyMatch = false -- Keep this value for attachKey UNLESS a match is found among the particles
					for key,value in pairs(player.particles) do -- Loop through all particles
						if key == attachKey then -- If the key we want to use matches an existing key
							keyMatch = true -- Flag this as a key match
						end
					end
				end
			end
			if keyMatch == true then -- If, after checking against ALL existing keys, there are still no valid options
				attachKey = bone.." slotID "..tostring(particleCount + attachKeyIndex) -- Create a new incremented index
			end
		end
		Game.playerSetParticle{		
			user 			= player,
			particleKey 	= attachKey,
			boneName 		= bone,
			particleGLID 	= particle,
			offset 			= {x = offset[1], y = offset[2], z = offset[3]},
			forwardDir 		= {x = forwardDir[1], y = forwardDir[2], z = forwardDir[3]},
			upDir 			= {x = upDir[1], y = upDir[2], z = upDir[3]}
		}
	end
	
	-- Events.sendClientEvent("FRAMEWORK_EMOTE_OBJECT", {emoteState = true}, emotePlayer.ID)

	-- 3: Sound
	if itemEffects.soundGLID then
		local tSoundParams = GameDefinitions.generateGenericSoundParams(player.ID, itemEffects.soundGLID, 20, true)
		player.eatSound = Behavior_Sound.generate(tSoundParams)
		
		Behavior_Sound.play({soundID = player.eatSound})
	end
end



-- Called when a player has attacked something
function onPlayerAttackedTarget(self, event)
	LOG_FUNCTION_CALL("onPlayerAttackedTarget")
	
	if self == nil then log("Controller_Battle - onPlayerAttackedTarget() - self is nil."); return end
	if event == nil then log("Controller_Battle - onPlayerAttackedTarget() - event is nil."); return end
	if event.type == nil then log("Controller_Battle - onPlayerAttackedTarget() - event.type is nil."); return end
	if event.attacker == nil then log("Controller_Battle - onPlayerAttackedTarget() - event.attacker is nil."); return end
	local attacker = Players.determineUser(event.attacker, s_uPlayers)
	if attacker == nil then log("Controller_Battle - onPlayerAttackedTarget() - Failed to extract sharedData from attacker["..tostring(event.attacker).."."); return end
	if event.attacked == nil then log("Controller_Battle - onPlayerAttackedTarget() - event.attacked is nil."); return end
	if event.weapon == nil then log("Controller_Battle - onPlayerAttackedTarget() - event.weapon is nil."); return end
	if event.weaponSlot == nil then log("Controller_Battle - onPlayerAttackedTarget() - event.weaponSlot is nil."); return end

	-- Play sound effect, if any
	local attackSound = event.player.playerInventorySound	
	if (attackSound) then
		Behavior_Sound.play({soundID = attackSound})
		Events.sendEvent("CHECK_AGAINST_SOUND", {attacker = attacker, weapon = event.weapon})
	end

	local battleWeapon = event.weapon
	if s_uItemProperties[tostring(battleWeapon.UNID)] then
		battleWeapon.properties = Toolbox.deepCopy(s_uItemProperties[tostring(battleWeapon.UNID)])
	end
	-- CJW, May the Fourth 2016 - Disabling the following as the code attempts to access a section of s_uItemInstances at the same time
	-- as Controller_Inventory is updating it, causing weirdness and a disturbance in the force.
--[[	if attacker.inventory and attacker.inventory[tostring(event.weaponSlot-1)] and attacker.inventory[tostring(event.weaponSlot-1)].properties then
		battleWeapon.properties = {}
		battleWeapon.properties = Toolbox.deepCopy(attacker.inventory[tostring(event.weaponSlot-1)].properties)
	end
--]]
	-- Special case properties
	local healingWeapon = false
	if battleWeapon and battleWeapon.properties and (battleWeapon.properties.healingWeapon == "true" or tostring(battleWeapon.properties.repairTool) == "true") then
		healingWeapon = true
	end
	local paintWeapon = false
	if battleWeapon and battleWeapon.properties and battleWeapon.properties.paintWeapon == "true" then
		paintWeapon = true
	end
	local weaponType = battleWeapon.properties.itemType or "weapon"
	local weaponLevel = 1
	if battleWeapon.instanceData and battleWeapon.instanceData.levelMultiplier then
		weaponLevel = battleWeapon.instanceData.levelMultiplier
	end

	if battleWeapon.properties.level == nil then
		log("ERROR LOG: Error detected in onPlayerAttackedTarget: weapon at slot "..tostring(event.weaponSlot).." has a nil level! Attacker name "..tostring(attacker.name))
	end
	
	local damage = calculateWeaponDamage(battleWeapon.properties.level or event.weapon.properties.level, weaponLevel * (battleWeapon.properties.bonusDamage or 1), healingWeapon)

	-- Set attack animation set
	if battleWeapon.properties.animationSet then
		local idleAnimations = GameDefinitions.ANIMATION_SETS[battleWeapon.properties.animationSet].idle
		local attackAnimations = GameDefinitions.ANIMATION_SETS[battleWeapon.properties.animationSet].attacking
		-- Don't animate tools
		if weaponType ~= "tool" and attackAnimations then
			kgp.playerDefineAnimation(event.player.ID, attackAnimations.STAND, attackAnimations.RUN, attackAnimations.WALK, attackAnimations.JUMP)
		end
		
		-- Set small buffer to let idle animations apply before attacking again
		local animTimer = battleWeapon.properties.rateOfFire - .15
		animTimer = animTimer > 0 and animTimer or 0
		Events.setTimer(animTimer, function()
										if(event.player.equipped ~= "" and weaponType ~= "tool" and idleAnimations) then
											kgp.playerDefineAnimation(event.player.ID, idleAnimations.STAND, idleAnimations.RUN, idleAnimations.WALK, idleAnimations.JUMP)
										end
									end)
	end

	-- Are we targeting a player? Paint weapon does not affect players
	if event.type == "Player" and not paintWeapon then
		local uAttackedPlayer = Players.determineUser(event.attacked, s_uPlayers)
		if uAttackedPlayer == nil then log("Controller_Battle - onPlayerAttackedTarget() Failed to extract attacked player from user["..tostring(event.attacked).."]."); return end

		-- Calculate miss chance based on range from target
		local miss = checkForMiss(attacker, uAttackedPlayer, battleWeapon.properties.range)
		if miss == nil then log("Controller_Battle - onPlayerAttackedTarget() checkForMiss(attacker["..tostring(attacker).."], attacked["..tostring(uAttackedPlayer).."], weaponRange["..tostring(battleWeapon.properties.range).."]) Failed"); return end
		-- Healing weapons can't miss
		if miss and not healingWeapon then
			-- Send miss floater
			Events.sendClientEvent("GENERATE_FLOATER", {text = "MISS", target = uAttackedPlayer.name, color = GameDefinitions.COLORS.WHITE}, attacker.ID)
		else
			if not healingWeapon then
				damage = mitigateDamage(uAttackedPlayer, damage)
			end
			if uAttackedPlayer == nil then log("Controller_Battle - onPlayerAttackedTarget() - Failed to extract sharedData from attacked["..tostring(event.attacked).."."); return end
			-- Heal the target?
			if damage < 0 then
				self:playerAddHealth({user = uAttackedPlayer, health = -damage, healer = attacker})
			elseif damage >= 0 then
				self:playerRemoveHealth({user = uAttackedPlayer, health = damage, attacker = attacker, flash = true})
			end
		end
	-- Are we targeting an object?
	elseif event.type == "Object" then
		-- Shoot the object unless it is a paintWeapon
		if  not paintWeapon then
			local uAttackedItem = s_uItemInstances[tonumber(event.attacked)] 
			if uAttackedItem == nil then
				log("ERROR DETECTED - ED-6319 : uAttackedItem is nil. event.attacked = "..tostring(event.attacked)..", attacker.name = " ..tostring(attacker.name))
				return
			end
			local miss = checkForMiss(attacker, uAttackedItem, battleWeapon.properties.range)
			if miss == nil then log("Controller_Battle - onPlayerAttackedTarget() checkForMiss(attacker["..tostring(attacker).."], attacked["..tostring(uAttackedItem).."], weaponRange["..tostring(battleWeapon.properties.range).."]) Failed"); return end
			if miss and not healingWeapon and not uAttackedItem.type == "Harvestable" then
				-- Send miss floater
				Events.sendClientEvent("GENERATE_FLOATER", {text = "MISS", target = tonumber(event.attacked), color = GameDefinitions.COLORS.WHITE}, attacker.ID)
			else
				damage = mitigateDamage(uAttackedItem, damage)
				-- Heal the target?
				if damage < 0 then
					self:objectAddHealth({PID = tonumber(event.attacked), health = damage, healer = attacker})
				elseif damage >= 0 then
					if uAttackedItem and type(uAttackedItem) ~= "number" and uAttackedItem.requiredTool and tonumber(uAttackedItem.requiredTool) > 0 then -- If a tool is required to attack this object
						if tostring(uAttackedItem.requiredTool) == tostring(battleWeapon.UNID) then -- If required tool matches the battleWeapon

							if uAttackedItem.isStaged == nil then -- If NOT a staged object
								self:objectRemoveHealth({PID = tonumber(event.attacked), health = damage, attacker = attacker})
							else -- If a staged object
								Events.sendEvent("OBJECT_HARVEST", {target = tonumber(event.attacked), attacker = attacker})
							end
						else
							kgp.playerShowStatusInfo(attacker.ID, "You lack the required tool to harvest this object.", 3, 255, 0, 0)
						end
					else -- Perform regular combat damage
						self:objectRemoveHealth({PID = tonumber(event.attacked), health = damage, attacker = attacker})
					end					
				end
			end
		end
	
	-- Handle special cases for non-weapons

	-- Texture the object if it is a paintWeapon
	elseif event.type == "Paint" then
		Events.sendClientEvent("FRAMEWORK_PAINT_OBJECT", {PID = tonumber(event.attacked)}, attacker.ID)
	elseif event.type == "Generic" then
		Events.sendEvent("GENERIC_USE", {target = tonumber(event.attacked), attacker = attacker, weapon = tonumber(battleWeapon.UNID)})
	-- Play an emote
	elseif event.type == "Emote" or event.type == "P2PEmote" then

		local emoteAnim = nil
		local particleList = nil
		local emotePlayer = attacker
		
		-- Handle the emote for the main player (attacker)
		if emotePlayer then
			local gender = emotePlayer.gender 
			if battleWeapon.properties then
				local emoteItem = false

				-- Get gendered emote
				if (battleWeapon.properties.emoteM and gender == "M") then
					emoteAnim = battleWeapon.properties.emoteM
				elseif (battleWeapon.properties.emoteF and gender == "F") then
					emoteAnim = battleWeapon.properties.emoteF
				end

				-- Get particle effects

				if battleWeapon.properties.particleEffectsMine then
				 	particleList = battleWeapon.properties.particleEffectsMine
				end
			end

			if emoteAnim then
				Game.playerSetAnimation{user = emotePlayer, animation = tonumber(emoteAnim)}
			end

			local particleIndex = 0
			if particleList ~= nil then
				for index,value in pairs(particleList) do
					local compiledParticle = {}
					local offsetInput 		= {0, 0, 0}
					local forwardDirInput 	= {0, 0, 0}
					local upDirInput 		= {0, 0, 0}
					compiledParticle.effect = tonumber(particleList[index].effect)
					compiledParticle.bone = particleList[index].bone
					-- Handle special case offsets
					if compiledParticle.bone == "Bip01 Head" then
						local offsetInput 		= {2, 0, 0}
					end
					if index then
						particleIndex = index
					end
					updateEmoteParticle(emotePlayer, compiledParticle.effect, compiledParticle.bone, true, offsetInput, forwardDirInput, upDirInput, particleIndex)
				end
			-- If no need to wait for adding particles, update emote state
			else
				Events.sendClientEvent("FRAMEWORK_EMOTE_OBJECT", {emoteState = true}, emotePlayer.ID)
			end
		end

		-- Handle emote for the targeted player (attacked) in a P2P emote
		if event.type == "P2PEmote" then

			local emoteAnimTheirs = nil
			local particleListTheirs = nil
			local emotePlayerTarget = Players.determineUser(event.attacked, s_uPlayers)

			if emotePlayerTarget then
				local gender = emotePlayerTarget.gender -- Get it again for p2p
				if battleWeapon.properties then
					local emoteItem = false

					-- Get gendered emote
					if (battleWeapon.properties.emoteMTheirs and gender == "M") then
						emoteAnimTheirs = battleWeapon.properties.emoteMTheirs
					elseif (battleWeapon.properties.emoteFTheirs and gender == "F") then
						emoteAnimTheirs = battleWeapon.properties.emoteFTheirs
					end

					-- Get particle effects
					if battleWeapon.properties.particleEffectsTheirs then
					 	particleListTheirs = battleWeapon.properties.particleEffectsTheirs
					end
				end

				if emoteAnimTheirs then
					Game.playerSetAnimation{user = emotePlayerTarget, animation = tonumber(emoteAnimTheirs)}
				end

				local particleIndex = 0
				if particleListTheirs ~= nil then
					for index,value in pairs(particleListTheirs) do
						local compiledParticle = {}
						local offsetInput 		= {0, 0, 0}
						local forwardDirInput 	= {0, 0, 0}
						local upDirInput 		= {0, 0, 0}
						compiledParticle.effect = tonumber(particleListTheirs[index].effect)
						compiledParticle.bone = particleListTheirs[index].bone
						-- Handle special case offsets
						if compiledParticle.bone == "Bip01 Head" then
							local offsetInput 		= {2, 0, 0}
						end
						if index then
							particleIndex = index
						end
						updateEmoteParticle(emotePlayerTarget, compiledParticle.effect, compiledParticle.bone, true, offsetInput, forwardDirInput, upDirInput, particleIndex)
					end
				-- If no need to wait for adding particles, update emote state
				else
					Events.sendClientEvent("FRAMEWORK_EMOTE_OBJECT", {emoteState = true}, emotePlayer.ID)
					Events.sendClientEvent("FRAMEWORK_EMOTE_OBJECT", {emoteState = true}, emotePlayerTarget.ID)
				end
			end
		end
	end
end

-- Called when an object has attacked something
function objectAttack(self, event)
	LOG_FUNCTION_CALL("objectAttack")
	
	if event.attacked == nil then log("Controller_Battle - objectAttack() - Failed to extract sharedData from user["..tostring(event.user).."."); return end
	if event.attacker and event.attacker.level then
		if event.attacked.type == "Player" then
			self:attackPlayer(event)
		else
			self:attackObject(event)
		end
	end
end

-- Called when a player is being attacked (by an object)
function attackPlayer(self, event)
	LOG_FUNCTION_CALL("attackPlayer")
	
	if event.attacked == nil then log("Controller_Battle - attackPlayer() - Failed to extract sharedData from user["..tostring(event.user).."."); return end
	-- Calculate damage based on attacker's level
	if event.attacker and event.attacker.level then

		local damage = 0
		-- Attacker does damage by percent total health?
		if event.attacker.percentDamage then
			damage = event.attacked.maxHealth* (event.attacker.percentDamage / 100 )
		elseif event.attacker.armorRating then
			-- Damage and maxHealth are the same thing
			damage = event.attacker.armorRating
			damage = mitigateDamage(attacked, damage)
		end

		-- Heal the target?
		if damage < 0 then
			self:playerAddHealth({user = event.attacked, health = damage, healer = event.attacker})
		elseif damage > 0 then
			self:playerRemoveHealth({user = event.attacked, health = damage, attacker = event.attacker, flash = true})
		end
	end
end

-- Called when a player is being attacked (by an object)
function attackObject(self, event)
	LOG_FUNCTION_CALL("attackObject")
	
	if event.attacked == nil then log("Controller_Battle - attackObject() - Failed to extract sharedData from user["..tostring(event.user).."."); return end
	-- Calculate damage based on attacker's level
	if event.attacker and event.attacker.level then
		-- Damage and maxHealth are the same thing
		local damage = event.attacker.armorRating or 0
		
		-- Apply this damage to the player passed in
		local attacked = event.attacked
		
		damage = mitigateDamage(attacked, damage)
		-- Heal the target?
		if damage < 0 then
			self:objectAddHealth({PID = tonumber(attacked.PID), health = damage, healer = event.attacker})
		elseif damage >= 0 then
			self:objectRemoveHealth({PID = tonumber(attacked.PID), health = damage, attacker = event.attacker})	
		end
	end
end


-- Handles the zoneInstanceId and zoneType from Framework_PlayerHandler
function returnZoneData(self, event)
	s_zoneInstanceId = event.zoneInstanceId
	s_zoneType = event.zoneType
end

function updateEmoteParticleHandler(self, event)
	LOG_FUNCTION_CALL("updateEmoteParticleHandler")
	
	local attacker = Players.determineUser(event.attacker, s_uPlayers)
	local particle = event.particle
	local bone = event.bone
	local enabled = event.enabled
	local offset = event.offset
	local forwardDir = event.forwardDir
	local upDir = event.upDir
	local attachKeyIndex = 0
	local attachKey = bone.." slotID "..tostring(attachKeyIndex)

	updateEmoteParticle(attacker, particle, bone, enabled, offset, forwardDir, upDir, attachKeyIndex)
end

--Handles when a player opens and closes the event flag menu
function eventFlagMenuHandler(self, event)
	LOG_FUNCTION_CALL("eventFlagMenuHandler")
	
	if event.player == nil then log("Controller_Battle - eventFlagMenuHandler() - event.player is nil."); return end
	if event.player.name == nil then log("Controller_Battle - eventFlagMenuHandler() - event.player.name is nil."); return end

	if event.open then
		s_eventFlagOpen[event.player.name] = true
	else
		s_eventFlagOpen[event.player.name] = nil
	end
end

updateEmoteParticle = function(emotePlayer, particle, bone, enabled, offset, forwardDir, upDir, attachKeyIndex)
	LOG_FUNCTION_CALL("updateEmoteParticle")
	
	if emotePlayer == nil then log("Framework_BattleHandler - updateEmoteParticle() - [emotePlayer] is nil."); return end

	-- Enable particle
	if enabled then

		local attachKey = bone.." slotID "..tostring(attachKeyIndex)
		
		if emotePlayer and emotePlayer.particles then
			 -- Get number of particles
			local particleCount = 0
			for key,value in pairs(emotePlayer.particles) do
				particleCount = particleCount + 1
			end
			-- If there are already particles using the intended index, update the index	
			local keyMatch = true
			for numericalIndex = attachKeyIndex, particleCount, 1 do
				if keyMatch == true then -- Do not check other numerical values unless a match is found
					attachKey = bone.." slotID "..tostring(numericalIndex) -- Test key using current numericalIndex
					keyMatch = false -- Keep this value for attachKey UNLESS a match is found among the particles
					for key,value in pairs(emotePlayer.particles) do -- Loop through all particles
						if key == attachKey then -- If the key we want to use matches an existing key
							keyMatch = true -- Flag this as a key match
						end
					end
				end
			end
			if keyMatch == true then -- If, after checking against ALL existing keys, there are still no valid options
				attachKey = bone.." slotID "..tostring(particleCount + attachKeyIndex) -- Create a new incremented index
			end
		end

		Game.playerSetParticle{
			user			= emotePlayer,
			particleKey		= attachKey,
			boneName		= bone,
			particleGLID	= particle,
			offset			= {x = offset[1], y = offset[2], z = offset[3]},
			forwardDir		= {x = forwardDir[1], y = forwardDir[2], z = forwardDir[3]},
			upDir			= {x = upDir[1], y = upDir[2], z = upDir[3]}
		}
		
		-- Send usage metrics
		local metricInput = {
			reportName = "OnUseEmoteItem",
			reportParams = {username = emotePlayer.name}
		} -- TODO: Double check this
		Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {input = metricInput, callType = "METRIC_GENERIC"})		
	
	-- Disable particle
	else

		if emotePlayer.particles then
			for key,particlesData in pairs(emotePlayer.particles) do
				if particlesData then
					Game.playerRemoveParticle{	
						user 			= emotePlayer,
						particleKey 	= key,
						boneName 		= particlesData.bone,
						particleGLID 	= particle
					}
				end
			end
		end
	end
end

function updatePaintMetricsHandler(self, event)
	LOG_FUNCTION_CALL("updatePaintMetricsHandler")
	
	local input = {
		username		= event.painter,
		zoneInstanceId	= s_zoneInstanceId,
		zoneType		= s_zoneType
	}
	Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {input = input, callType = "METRIC_PAINT"})
end

-- Calculate the damage of a given weapon level with its multiplier applied
calculateWeaponDamage = function(level, multiplier, healingWeapon)
	LOG_FUNCTION_CALL("calculateWeaponDamage")
	
    if level == nil or multiplier == nil or level > 5 or level < 1 then
        log("ERROR LOG: Error detected in calculateWeaponDamage: level = "..tostring(level)..", multiplier = "..tostring(multiplier))
		if level == nil then
			level = 1
		else
			return 0
		end
    end
	local damage = GameDefinitions.calculateDamageRating(level, multiplier)
	if healingWeapon then
		damage = - damage
	end
	return damage
end

-- Mitigates damage based on the attacked's armorRating
mitigateDamage = function(attacked, damage)
	LOG_FUNCTION_CALL("mitigateDamage")
	
	if attacked then
		if attacked.armorRating then
			return math.max((damage - (attacked.armorRating * GameDefinitions.ARMOR_MITIGATION)),0)
		else
			return damage
		end
	else
		return damage
	end
end

-- Calculate if this weapon missed
checkForMiss = function(attacker, attacked, range)
	LOG_FUNCTION_CALL("checkForMiss")
	
	local miss
	local roll = math.random() * 100
	if attacker == nil then log("Controller_Battle - checkForMiss() attacker is nil"); return end
	if attacker.position == nil then log("Controller_Battle - checkForMiss() attacker.position is nil"); return end
	if attacker.position.x == nil then log("Controller_Battle - checkForMiss() attacker.position.x is nil"); return end
	if attacker.position.y == nil then log("Controller_Battle - checkForMiss() attacker.position.y is nil"); return end
	if attacker.position.z == nil then log("Controller_Battle - checkForMiss() attacker.position.z is nil"); return end
	if attacked == nil then log("Controller_Battle - checkForMiss() attacked is nil"); return end
	if range == nil then log("Controller_Battle - checkForMiss() range is nil"); return end
	-- Calculate for Melee attacks at 15%
	if range <= 6 then
		miss = roll <= MELEE_MISS_CHANCE
	-- Calculate for Range attacks based on range
	else
		local missChance = 100
		local attackedPos = {}
		if type(attacked) == "table" or type(attacked) == "userdata" then
			if attacked.position == nil then log("Controller_Battle - checkForMiss() attacked.position is nil"); return end
			if attacked.position.x == nil then log("Controller_Battle - checkForMiss() attacked.position.x is nil"); return end
			if attacked.position.y == nil then log("Controller_Battle - checkForMiss() attacked.position.y is nil"); return end
			if attacked.position.z == nil then log("Controller_Battle - checkForMiss() attacked.position.z is nil"); return end
			attackedPos = {
				x = attacked.position.x,
				y = attacked.position.y,
				z = attacked.position.z
			}
		else
			local x,y,z = kgp.objectGetStartLocation(attacked)
			if x == nil then log("Controller_Battle - checkForMiss() x returned from kgp.objectGetStartLocation("..tostring(attacked)..") is nil"); return end
			if y == nil then log("Controller_Battle - checkForMiss() y returned from kgp.objectGetStartLocation("..tostring(attacked)..") is nil"); return end
			if z == nil then log("Controller_Battle - checkForMiss() z returned from kgp.objectGetStartLocation("..tostring(attacked)..") is nil"); return end
			attackedPos = {
				x = x,
				y = y,
				z = z
			}
		end
		-- Get the distance from the attacker to the attacked
		local dist = math.sqrt(math.pow((attacker.position.x - attackedPos.x),2) +
							   math.pow((attacker.position.y - attackedPos.y),2) +
							   math.pow((attacker.position.z - attackedPos.z),2))
		-- Calculate percentage of range
		local percOfRange = (dist / range) * 100
		-- Find missChance
		for perc, chance in pairs(MISS_CHANCES) do
			if percOfRange <= perc then
				missChance = math.min(chance, missChance)
			end
		end
		miss = roll <= missChance
	end
	return miss
end

-- Attempts to decrement armor durability. Detects what slot to damage and informs Controller_Inventory
decrementArmorDurability = function(player)
	LOG_FUNCTION_CALL("decrementArmorDurability")
	
	local playerArmor = s_uPlayers[player.name].armor
	if playerArmor then
		local armorIndexes = {}
		-- Count up all equipped pieces of armor with durability remaining
		for i, v in pairs(playerArmor) do
			if v.UNID ~= 0 then
				if v.instanceData and v.instanceData.durability and v.instanceData.durability > 0 then
					table.insert(armorIndexes, i)
				end
			end
		end
		if #armorIndexes > 0 then
			local decrementIndex = math.random(#armorIndexes)
			local armorIndex = armorIndexes[decrementIndex]
			-- Don't decrement indestructible armor
			local durabilityType = "repairable"
			if playerArmor.properties and playerArmor.properties.durabilityType then
				durabilityType = playerArmor.properties.durabilityType
			end
			if durabilityType ~= "indestructible" then
				-- Ask Controller_Inventory to decrement the durability and perform all required updates
				Events.sendEvent("UPDATE_PLAYER_ARMOR_DURABILITY", {player = player, slot = armorIndex, durabilityLoss = DURABILITY_PER_HIT})
			end
		end
	end
end
