--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-----------------------------------------------------------------
-- Controller_Inventory.lua
-----------------------------------------------------------------
include("Lib_GameItemBootstrap.lua")
include("Lib_SaveLoad.lua")
include("Lib_GameItemInterface.lua")
include("Lib_GameDefinitions.lua")
include("ControllerHelper_Map.lua")
include("Lib_Web.lua")
include("Lib_ZoneInfo.lua")

-----------------------------------------------------------------
-- Define Controller_Inventory 
-----------------------------------------------------------------
Class.createClass("Controller_Inventory", "Controller")


-----------------------------------------------------------------
-- Setup the virtual interface
-----------------------------------------------------------------
Class.import("Game")
Class.export({
	{ "generateLootChest", "event"},
	-- inherited from Controller
	{ "config", "key", "value" }
})


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local Debug				= _G.Debug
local GameDefinitions	= _G.GameDefinitions
local Web				= _G.Web
local Console			= _G.Console
local SaveLoad			= _G.SaveLoad
local Events			= _G.Events
local GII 				= _G.GII
local ZoneInfo 			= _G.ZoneInfo

-----------------------------------------------------------------
-- Debugging Helpers
-----------------------------------------------------------------
local LOG_FUNCTION_CALL, LOG_FUNCTION_CALL_FILTER_ID = Debug.generateCustomLog("Controller_Inventory - ")
Debug.enableLogForFilter(LOG_FUNCTION_CALL_FILTER_ID, false) -- Set this to true if you want to get a complete log of Controller_Inventory function calls


-----------------------------------------------------------------
-- Constant definitions
-----------------------------------------------------------------


-- Table of events to be registered and accompanying handlers
-- NOTE: Only events needed post-Framework spinup should be here. If you need an event to respond while the Framework is still
-- offline, register for it in in create()
local tCONTROLLER_EVENTS = {
	CONTROLLER_REGISTER 							= "onRegister",						-- Register to catch objects registering themselves
	CREATE_NEW_GAME_ITEM 							= "createGameItem",
	EDIT_GAME_ITEM 									= "editGameItem",
	EDIT_GAME_ITEM_INTERNAL							= "editGameItemInternal",
	DELETE_GAME_ITEM 								= "onGameItemDeleted",
	DELETE_GAME_ITEM_INTERNAL 						= "onGameItemDeletedInternal",
	UNIQUE_NAME_CHECK 								= "uniqueNameCheck",
	FRAMEWORK_GOTO_ITEM 							= "onGameItemGoto",
	GET_ALL_GAME_ITEMS 								= "getAllGameItems",
	FRAMEWORK_GET_RECIPES 							= "getRecipes",
	FRAMEWORK_EXTRACT_GAME_ITEM_GLIDS 				= "extractGameItemGLIDs",
	FRAMEWORK_EXTRACT_ALL_GAME_ITEM_GLIDS 			= "extractAllGameItems",
	GENERATE_LOOT_CHEST 							= "generateLootChest",				-- Generate a loot chest
	GENERATE_RANDOM_LOOT 							= "generateRandomLoot",				-- Generate random loot for random loot vendor
	FRAMEWORK_ROLL_FOR_GEM_CHEST 					= "rollForGemChest",				-- Rolls for a gem chest by player
	OBJECT_INSTANCE_PENDING 						= "onPlacePendingResponse",			-- Register for player build mode placed items
	OBJECT_UPDATE_RESPONSE							= "onUpdateGameItem",
	FRAMEWORK_REQUEST_BATTLE_OBJECTS				= "onBattleObjectsRequested",		-- Called whenever Framework_BattleHelper needs targets
	FRAMEWORK_CREATE_PLACE_SPAWN 					= "onPlayerCreatePlaceSpawn",		-- Register for player build mode placed items
	FRAMEWORK_OBJECT_REMOVED 						= "onItemRemoved",					-- Item has been removed
	FRAMEWORK_REQUEST_OBJECT_ASSOCIATIONS 			= "onRequestObjectAssociations",
	FRAMEWORK_GET_TIMED_VENDS						= "getTimedVends",
	FRAMEWORK_GET_ALL_TIMED_VENDS					= "getAllTimedVends",
	FRAMEWORK_SAVE_TIMED_VENDS						= "saveTimedVend",
	FRAMEWORK_PASS_AVAILABLE_QUESTS			 		= "passAvailableQuests",
	FRAMEWORK_PLACE_QUEST_WAYPOINT					= "createQuestWaypoint",
	FRAMEWORK_REMOVE_QUEST_WAYPOINT					= "removeQuestWaypoint",
	FRAMEWORK_PICKUP_GAME_ITEM				 		= "pickupGameItem",
	FRAMEWORK_PICKUP_LAST_GAME_ITEM					= "pickupLastGameItem",
	FRAMEWORK_GET_QUEST_GIVER_SPAWNERS				= "getQuestGiverSpawners",
	FRAMEWORK_VALIDATE_ATTRIBUTE					= "validateAttribute",
	FRAMEWORK_UPDATE_OBJECT_PLACEMENT				= "updateObjectPlacement",
	REGISTER_SYSTEM_DYNAMIC_OBJECT 					= "onRegisterSettingObject",		-- Register for flag settings 
	UNREGISTER_SYSTEM_DYNAMIC_OBJECT 				= "onUnregisterSettingObject",
	FRAMEWORK_REQUEST_WORLD_MAP						= "onWorldMapRequested",
	EVENT_FLAG_STATUS_UPDATED						= "onEventFlagStatusUpdated",
	EVENT_FLAG_STATUS_UPDATED						= "onEventFlagStatusUpdated",
	REQUEST_LOOT									= "onLootRequested",
	FRAMEWORK_WEB_CALL_RESPONSE						= "onWebCallResponded",
	FRAMEWORK_LINKED_ZONE_UPDATED					= "onLinkedZoneUpdated"
}

local PAPER_DOLL_MAGIC_GLID = 6 -- Do you believe in magic? GLID defined on platform side as meaningful as a placed paper doll object
local BOUND_CHK_BUFFER = 0.001 -- Amount of leeway for "is X within bounds of Y" checks
local BOUND_CHK_BUFFER_Y = 1.5 -- Amount of leeway for checks BELOW the Y value (accounts for slight slopes and the fact that the player avatar is technically slightly beneath the ground)

local SQUARE_OF_TWO = 1.414

-- Fields used for Game Item GLID extraction
local GLID_EXTRACTION_FIELDS = {	
	["BehaviorParams"] = {	
		"animations", 
		"spawnGLID", 
		"closeAnim", 
		"openAnim", 
		"openedAnim", 
		"idleParticle", 
		"chargeParticle", 
		"attackSound"
	},
	["Standard"] = {	
		"GLID", 
		"addGLIDS", 
		"femaleGLIDS", 
		"maleGLIDS", 
		"soundGLID"
	}
}


-- TODO: Expect this to change when the default spawner flow is finalized
local DEFAULT_SPAWNER = {
	["character"] = {	name = "New Spawner", itemType = "spawner", GLID = 4465632, rarity = "Common", level = 1, GIGLID = 0,
						behavior = "spawner_character", respawnTime = 300, spawnRadius = 1800, spawnGLID  = 4478234,
						behaviorParams = {defaultSpawner = true, maxSpawns = 1, spawnUNID = 0},
						description = "Spawner for bla bla bla"
	},
	["harvestable"] = {	name = "New Spawner", itemType = "spawner", GLID = 4478234, rarity = "Common", level = 1, GIGLID = 0,
						behavior = "spawner_harvestable", respawnTime = 300, spawnRadius = 1800, spawnGLID  = 4478234,
						behaviorParams = {defaultSpawner = true, maxSpawns = 1, spawnUNID = 261},
						description = "Spawner for bla bla bla"
	},
	["placeable"] = {	name = "New Spawner", itemType = "spawner", GLID = 4465631, rarity = "Common", level = 1, GIGLID = 0,
						behavior = "spawner_loot", respawnTime = 300, spawnRadius = 1800, spawnGLID  = 4468596,
						behaviorParams = {defaultSpawner = true, loot = {limitItems = {UNID = 0, limit = 1} } },
						description = "Spawner for bla bla bla"
	},
	["loot"] = {		name = "New Spawner", itemType = "spawner", GLID = 4465631, rarity = "Common", level = 1, GIGLID = 0,
						behavior = "spawner_loot", respawnTime = 120, spawnRadius = 1800, spawnGLID  = 4468596,
						behaviorParams = {defaultSpawner = true, loot = {limitItems = {UNID = 0, limit = 1} } },
						description = "Spawner for bla bla bla"
	}
}


-----------------------------------------------------------------
-- Local variable definitions
-----------------------------------------------------------------

local s_uItemInstances 						-- Table of all known objects in the world {PID: {sharedData}}
local s_uPlayers							-- List of all players held in SharedData {playerName: {playerData}}
local s_uItemProperties						-- This is now maintained as a SharedData as opposed to a simple Lua table. The contained data is structured as so: {UNID: {propertyList}}


local s_iNextUNID
local s_tActiveDOs					= {} 	-- Dynamic Objects that reference Game Items and know who placed them
local s_tGameItemTypes				= {} 	-- Game Items by Type; {Type:{UNID list}}
local s_tObjectAssociations			= {} 	-- Table of object association tables (used for teleporters, moving platforms, etc)
local s_iObjectAssociationCount		= 0
local s_tInstanceQueue				= {}
local s_iInstanceCount				= 0
local s_iInstancedCount				= 0
local s_bIsControllerReady			= false
local s_iPendingID					= 0
local s_tPendingQueue				= {}
local s_tDeinstantiationQueue		= {} 	-- PIDs pending a de-instantiation
local s_iStartingInventorySize		= 0 	-- How large is the starting inventory size (for reference when creating player inventory
local s_iLastGameItem 						-- PID of last Game Item if applicable
local s_iZoneInstanceID
local s_iZoneType
local s_bInTopWorld					= false
local s_tLandClaimFlags				= {} 	-- Table of land claim flags -> mapping PID to claim flags
local s_tMediaPlayers				= {} 	-- Table of media players -> mapping PID to media player
local s_bCanDropGems				= false
local s_deinstantiateFramework 		= false -- Flag to indicate if need to deinstantiateFramework on game item pickup
local s_deinstantiatingPlayer 		= nil	-- Player who is causing deinstantiating of the framework

-----------------------------------------------------------------
-- Local function declarations
-----------------------------------------------------------------
local loadGameItemProperties				-- ()
local detectRequiredSystems					-- ()
local compileActiveDOs						-- ()
local saveGameItem							-- (UNID, gameItem)
local validateItemContainer					-- (UNID)
local validateRecipes						-- ()
local validateRecipe						-- (UNID)
local validateQuestPrereqs					-- ()
local validateQuestItems					-- ()
local validateQuestGiverQuests				-- ()
local validateQuestWaypoint					-- (PID)
local checkRecipe							-- (recipe)
local placeItemSpawn						-- (self, playerName, spawnUNID, position)
local checkForDefaultSpawner				-- (default)
local rarityCheck							-- (UNID)
local getItemInfoFromPID					-- (PID)
local getBehaviorParams						-- (UNID)
local extractGLIDsByUNID					-- (UNID)
local checkForUniqueName					-- {name, itemType}
local loadObjectAssociations				-- ()
local portWOKFieldsToGameItem				-- (item)
local isLootItem							-- (item)
local initFrameworkDB						-- ()
local countActiveDOs						-- ()
local constructLabelTable					-- (labelTable)
local validateVendors						-- ()
local checkVendor							-- (unid, removedUNID)
local checkObjectPlaceability				-- (event)
local collectMetricsForPlacedItem			-- (placedItemUNID, playerName, playerMode)
local instantiateActiveDOs					-- (event)
local getQuestFromWaypoint					-- (waypointPID)
local itemsInQueue							-- ()
local checkForClaimFlags					-- ()
local getDynamicObjectPosition				-- (objectPID)
local getClaimFlagsAtPoint					-- (x, y, z)
local isPointInFlagBoundary					-- (flag, x, y, z)
local doFlagsIntersect						-- (flag, otherFlag)
local doMediaPlayersIntersect				-- (media, otherMedia)
local isClaimFlagCreationAllowed			-- (item)
local isMediaPlayerCreationAllowed			-- (item)
local isPlaceableItem						-- function(UNID)
local createTeleporterAssociations			-- (PID, placementInfo)
local createMoverAssociations				-- (PID, placementInfo)
local normalizeDropChance					-- (objectLoot)
local generateLootItem						-- (objectLoot, level)
local getRandomLoot							-- function(lootTable, level)
local validateFlagArea						-- function(item, UNID)

-----------------------------------------------------------------
-- Main entry to and exit from this controller
-----------------------------------------------------------------
function create(self)
	LOG_FUNCTION_CALL("create")
	
	_super.create(self)

	-- Handles events from WOK Inventory for adding/placing Game Items
	Events.registerHandler("WOK_CREATE_SPAWNER", self.WOKcreateSpawner, self)
	Events.registerHandler("WOK_PLACE_GAME_ITEM", self.WOKplaceGameItem, self)
	Events.registerHandler("WOK_ADD_TO_GAME", self.WOKaddToGame, self)
	Events.registerHandler("FRAMEWORK_PLAYER_PLACE_ITEM", self.onPlayerBuildPlaceItem, self)
	Events.registerHandler("FRAMEWORK_RETURN_ZONE_DATA", self.onZoneDataReturned, self)	
	-- Register to catch objects Un-registering themselves
	Events.registerHandler("CONTROLLER_UNREGISTER", self.onUnregister, self)
	-- Request for current controller state
	Events.registerHandler( "FRAMEWORK_QUERY_CONTROLLER_STATE", self.onControllerStateQuery, self )
	-- Register an activate event for this controller
	Events.registerHandler( "FRAMEWORK_ACTIVATE_CONTROLLER", self.onActivate, self )
	Events.registerHandler( "FRAMEWORK_DEACTIVATE_CONTROLLER", self.onDeactivate, self )
	-- Register an event to place path waypoint and rider
	Events.registerHandler("PLACE_PATH_WAYPOINT", self.placePathWaypoint, self)
	Events.registerHandler("PLACE_PATH_RIDER", self.placePathRider, self)
	-- Register an event to update path waypoint linkages and rider
	Events.registerHandler( "UPDATE_PATH_WAYPOINT_LINKAGES_ON_ADD", self.updatePathWaypointLinkagesOnAdd, self )
	Events.registerHandler( "UPDATE_PATH_WAYPOINT_LINKAGES_ON_DEL", self.updatePathWaypointLinkagesOnDel, self )
	
	--Register an handler to count active dos on request from game controller
	Events.registerHandler( "COUNT_ACTIVE_DOS_ON_REQUEST", self.countActiveDOsOnRequest, self )
	Events.registerHandler( "ACTIVE_DO_DESTROYED", self.activeDODestroyed, self )
	
	-- Initialize item instance shared data
	s_uItemInstances	= SharedData.new("items", false)
	s_uPlayers			= SharedData.new("players", false)
	
	-- Load game item properties into s_uItemProperties
	loadGameItemProperties()

	-- Improve randomness of math.random()
	math.randomseed(kgp.gameGetCurrentTime())
	
	-- Register Controller_Inventory with Controller_Game
	Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Inventory", state = {online = true}})
end

function stop(self, user)
	LOG_FUNCTION_CALL("stop")
	_super.stop(self, user)
end


---------------------------------
-- Controller event handlers
---------------------------------


-- Handles controller activation events
function onActivate(self, event)
	LOG_FUNCTION_CALL("onActivate - " .. tostring(event.controller))
	-- Time to activate?
	if event.controller and event.controller == "Inventory" then
		-- Log beginning of handler registration
		Game.logProgress{log = "Inventory - Registering event handlers"}
		-- Register all event handlers
		for event, handler in pairs(tCONTROLLER_EVENTS) do
			Events.registerHandler(event, self[handler], self)
		end
		-- Log completion of event registration
		Game.logProgress{log = "Inventory - Event handlers registered"}
		
		-- Local event
		Events.addLocalEvents{ "DEBUG" } -- Make debug event handled locally without going through event system.
		
		-- Set Health/Energy/Armor based on existing Game Items
		detectRequiredSystems()
		
		-- Pull what DOs in the zone map to UNIDs and who owns them
		Game.logProgress{log = "Inventory - DB Pulling placed Game Items"}
		compileActiveDOs()
		Game.logProgress{log = "Inventory - DB placed Game Items pulled", percentage = 20}
		
		-- Load associated object tables
		Game.logProgress{log = "Inventory - DB Object Associations"}
		loadObjectAssociations()
		Game.logProgress{log = "Inventory - DB Object Associations pulled", percentage = 25}
		
		-- Inventory registers with Controller_Game after this
		Game.logProgress{log = "Inventory - Instantiating placed Game Items"}
		
		instantiateActiveDOs()
	end
end

-- Called to deactivate this controller
function onDeactivate(self, event)

	LOG_FUNCTION_CALL("onDeactivate")
	-- Time to activate?
	if event.controller and event.controller == "Inventory" then
		-- Log beginning of handler un-registration
		Game.logProgress{log = "Inventory - Un-registering event handlers"}
		-- Unregister all un-needed event handlers
		for event, handler in pairs(tCONTROLLER_EVENTS) do
			Events.unregisterHandler(event, self[handler], self)
		end
		-- Log completion of event registration
		Game.logProgress{log = "Inventory - Event handlers un-registered"}
		
		s_bIsControllerReady = false
		
		-- Inventory registers with Controller_Game after this
		Game.logProgress{log = "Inventory - De-Instantiating placed Game Items"}
		local objectQueued = false
		-- Queue up all remaining DOs
		for PID, props in pairs(s_tActiveDOs) do
			local itemInfo = getItemInfoFromPID(PID)
		
			if itemInfo.behavior then
				Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = PID})
				s_tDeinstantiationQueue[PID] = true
				objectQueued = true
				
				-- NOTE: If an object has been queued for a de-instantiation, we'll finish shutting down through onUnregister()
			end
		end
		if objectQueued == false then
			-- Inform Controller_Game that Inventory is finished de-activating
			Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Inventory", state = {["active"] = false}})
		end
	end
end


-- Request for current controller state
function onControllerStateQuery(self, event)
	LOG_FUNCTION_CALL("onControllerStateQuery")
	local state = {online = true}
	if s_bIsControllerReady then
		state.active = s_bIsControllerReady
	end
	-- Inform Controller_Game that Inventory is finished activating
	Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Inventory", state = state})
end



-- Handles the zoneInstanceId and zoneType from Framework_PlayerHandler
function onZoneDataReturned(self, event)
	LOG_FUNCTION_CALL("onZoneDataReturned")
	
	s_iZoneInstanceID = event.zoneInstanceId
	s_iZoneType = event.zoneType
end


-- Called when an object registers itself
function onRegister(self, event)
	LOG_FUNCTION_CALL("onRegister")
	if self == nil then log("Controller_Inventory - onRegister() self is nil."); return end
	if event == nil then log("Controller_Inventory - onRegister() event is nil."); return end
	
	local uItem = event.sharedData
	if uItem == nil then log("Controller_Inventory - onRegister() event.sharedData is nil. Sender["..tostring(event._sender).."]"); return end
	
	local PID = uItem.PID
	if PID == nil then log("Controller_Inventory - onRegister() event.sharedData.PID is nil. Sender["..tostring(event._sender).."]"); return end
	
	local activeDO = s_tActiveDOs[PID]
	if activeDO then
		uItem.owner = activeDO.owner
	end
	
	uItem.spawned = uItem.spawnerPID ~= nil
	
	s_uItemInstances[PID] = uItem
	
	local itemInfo = getItemInfoFromPID(PID)
    itemInfo.owner = uItem.owner
	-- Inform all players that a new targetable item has been added
	if uItem.targetable then
		-- TODO: Smarter generation for health? If targetable, give health. If "hasArmor" flag, assign health special?
		-- If this item is placeable, generate it's health differently
		if itemInfo.itemType == GameDefinitions.ITEM_TYPES.PLACEABLE and itemInfo.behavior ~= "trap" then
			if itemInfo.healthMultiplier then
				uItem.maxHealth = GameDefinitions.PLACEABLE_HEALTH[itemInfo.level or 1] * itemInfo.healthMultiplier
			else
				uItem.maxHealth = GameDefinitions.PLACEABLE_HEALTH[itemInfo.level or 1]
			end
			uItem.health 	 = uItem.maxHealth
		else
			if uItem == nil then log("Controller_Inventory.lua - onRegister() s_uItemInstances["..tostring(PID).."] is nil") end
			if uItem then
				-- Assign this object a multiplier for health and damage
				uItem.multiplier = GameDefinitions.getItemLevelMultiplier()
				-- Assign this object a random armor rating based on its level
				uItem.armorRating = GameDefinitions.calculateRandomArmorRating(uItem.level, uItem.multiplier)
				uItem.health 	   = uItem.armorRating * GameDefinitions.ARMOR_MULTIPLIER * (uItem.healthMultiplier or 1)
				uItem.maxHealth   = uItem.health
			end
		end
	end
	
	
	-- Register this object if it's not makred as creatorOnly
	if s_bIsControllerReady and not uItem.creatorOnly then
		-- Mover objects don't register with the client as they can't be moved properly by players in build mode
		-- TODO: Find a solution for this ^
		if itemInfo.behavior ~= "mover" then

			local master
			if itemInfo.behavior == "pet" then
				master = itemInfo.owner
			end
			
			local eventData = {	
				type 	    	 = "Object",
				ID 	        	 = uItem.PID,
				UNID			 = uItem.UNID,
				alive 	    	 = uItem.alive,
				itemType 	     = itemInfo.itemType,
				owner 	    	 = itemInfo.owner,
				tooltip	    	 = uItem.tooltip,
				health	    	 = uItem.health,
				maxHealth   	 = uItem.maxHealth,
				targetable  	 = tostring(uItem.targetable == "true" and uItem.combatEnabled ~= false),
				behavior 		 = itemInfo.behavior,
				level	    	 = uItem.level or 1,
				isLoot	    	 = uItem.type == "Item Container",
				master			 = master,
				interactionRange = uItem.interactionRange,
				spawned			 = uItem.spawned or false
			}

			-- Register this object as a new targetable
			Events.sendClientEvent("FRAMEWORK_REGISTER_BATTLE_OBJECT", eventData)
		end
	end
	
	-- TODO: Does the following block of code need to be carried into Controller_Player??
	
	-- If this object is marked as creatorOnly, hide it from all users in Player mode
	if uItem.creatorOnly or uItem.modeLabels or uItem.quests then
		for name, uPlayer in pairs(s_uPlayers) do
			if uPlayer and uPlayer.inZone then
				-- Is this item creatorOnly?
				if uItem.creatorOnly then
					if uPlayer.mode == "Player" then
						kgp.objectHide(tonumber(PID), uPlayer.ID)
						kgp.objectRemoveParticleForPlayer(uPlayer.ID, tonumber(PID))
					else
						kgp.objectShow(tonumber(PID), uPlayer.ID)
						if uItem.particleGLID then
							kgp.objectSetParticleForPlayer(uPlayer.ID, tonumber(PID), uItem.particleGLID)
						end
					end
				end
				-- Does this item manually request labels or not?
				if uItem.showLabels == nil or (uItem.showLabels ~= nil and uItem.showLabels == true) then
					-- Does this item have labels that match this player's mode?
					if uItem.modeLabels then
						kgp.objectClearLabelsForPlayer(uPlayer.ID, tonumber(PID))
						if uItem.modeLabels[uPlayer.mode] then
							local label = constructLabelTable(uItem.modeLabels[uPlayer.mode])
							kgp.objectAddLabelsForPlayer(uPlayer.ID, tonumber(PID), label)
						end
					end
				end
				
				-- Set appropriate quest icon for players if they've received their quest state data from InventoryHandler
				if uItem.quests and uPlayer.mode == "Player" then
					Events.sendEvent("REQUEST_QUEST_ICON_ASSIGNMENT", {player = uPlayer, object = uItem} )
				end
			end
		end
	end
	
	-- TODO-LOADING: Update Object instantiation load
	if s_tInstanceQueue[tonumber(PID)] then
		s_iInstancedCount = s_iInstancedCount + 1
		s_tInstanceQueue[tonumber(PID)] = nil
		Game.logProgress{log = "Inventory - Game Item Registered ["..tostring(s_iInstancedCount).." / "..tostring(s_iInstanceCount).."]"}
	end
	if not s_bIsControllerReady and not itemsInQueue() then
		Game.logProgress{log = "Inventory - All placed Game Items successfully instantiated", percentage = 80}
		s_bIsControllerReady = true
		Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Inventory", state = {["active"] = true}})

		Events.sendEvent("FRAMEWORK_CHECK_FLAG_STATE")
	end
	
	if s_bIsControllerReady and not itemsInQueue() then
		countActiveDOs()
	end
	
	self:onObjectAddedToWorld(uItem)
	Events.sendEvent("FRAMEWORK_OBJECT_REGISTERED", {PID = tonumber(PID)})
end


-- Called when an object unregisters itself onDestroy or such
function onUnregister(self, event)
	LOG_FUNCTION_CALL("onUnregister")
	
	if self == nil then log("Controller_Inventory - onUnregister() self is nil."); return end
	if event == nil then log("Controller_Inventory - onUnregister() event is nil."); return end
	
	local uItem = event.sharedData
	if uItem == nil then log("Controller_Inventory - onUnregister() event.sharedData is nil. Sender["..tostring(event._sender).."]"); return end
	
	self:onObjectRemovedFromWorld(uItem)
	
	-- Un-register object
	local PID = uItem.PID
	if PID then
		Events.sendClientEvent("FRAMEWORK_UNREGISTER_BATTLE_OBJECT", {ID = PID})

		-- Was this Game Item part of the un-instantiation queue
		if s_tDeinstantiationQueue[PID] then
			s_tDeinstantiationQueue[PID] = nil
			
			local queuedObjectsRemain = false
			-- Was the last Game Item unregistered?
			for i, v in pairs(s_tDeinstantiationQueue) do
				queuedObjectsRemain = true
				break
			end
			if queuedObjectsRemain == false then
				-- Inform Controller_Game that Inventory is finished de-activating
				Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Inventory", state = {active = false}})
			end
		end
		
		if s_uItemInstances[PID] then
			s_uItemInstances[PID] = nil
		else
			log("Controller_Inventory - onUnregister() Unable to unregister "..tostring(PID).." from map. Sender["..tostring(event._sender).."]")
		end
	else
		log("Controller_Inventory - onUnregister() Unable to unregister "..tostring(PID).." from map. PID["..tostring(PID).."] wasn't included in the event's sharedData")
	end
end

function stop(self, user)
    _super.stop(self, user)
end

-- Sent from Controller_WebCall which returns webcall results
function onWebCallResponded(self, event)
	LOG_FUNCTION_CALL("onWebCallResponded")
	
	if event.CID == 100 and event.callType == "GET_TOP_WORLDS" then
		local username = Web.getDataByTag(event.text, "Username", "(.-)")
		for object in string.gmatch(event.text, "<_x0033_DApps>(.-)</_x0033_DApps") do
			local zoneID = Web.getDataByTag(object, "zone_instance_id", "(.-)")
			if tonumber(zoneID) == tonumber(s_iZoneInstanceID) then
				s_bInTopWorld = true
			end
		end
	end

	if event.CID == 100 and event.callType == "CAN_WORLD_DROP_GEMS" then
		local canDropGems = Web.getDataByTag(event.text, "CanDropGems", "(.-)")
		if canDropGems == "true" then
			s_bCanDropGems = true
		end
	end
end

function onLinkedZoneUpdated(self, event)
	countActiveDOs()
end


function getAllGameItems(self, event)
	LOG_FUNCTION_CALL("getAllGameItems")
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getAllGameItems() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getAllGameItems() event is nil."); return end

	local itemCache = Toolbox.breakTableIntoChunks(s_uItemProperties, -1, true)
	if itemCache and type(itemCache) == "table" and #itemCache > 0 then
		Events.sendClientEvent("UPDATE_ALL_GAME_ITEMS", {items = itemCache[1]}, event.player.ID)
	end
end

function updateObjectPlacement(self, event)
	LOG_FUNCTION_CALL("updateObjectPlacement")
	local PID, prevPID = tonumber(event.PID), tonumber(event.prevPID)
	if PID and prevPID then
		s_tActiveDOs[PID] = s_tActiveDOs[prevPID]
		s_tActiveDOs[prevPID] = nil
	end
end


-- Creates a new Game Item
function createGameItem(self, event)
	LOG_FUNCTION_CALL("createGameItem")
	if self == nil then log("Controller_Inventory - createGameItem() self is nil."); return false end
	if event == nil then log("Controller_Inventory - createGameItem() event is nil."); return false end
	if event.properties == nil then log("Controller_Inventory - createGameItem() event.properties is nil."); return false end
	if event.player == nil then log("Controller_Inventory - createGameItem() event.player is nil."); return false end	
	local item = event.properties
	
	local isUnique, uniqueName, conflictingUNID = checkForUniqueName{name = item.name, itemType = item.itemType}

	-- If a name conflict arose, use the uniqueName returned (in the case of duplicate)
	if not isUnique then
		item.name = uniqueName
	end
	
	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	if ZoneInfo.isLinkedZone() then
		local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
		s_iNextUNID = event.UNID or tonumber(SaveLoad.loadGameData("NextUNID", parentID, parentType))
	end

	local strNextUNID = tostring(s_iNextUNID)
	-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
	--Toolbox.setUserdataAtIndex(s_uItemProperties, strNextUNID, item)

	s_uItemProperties[strNextUNID] = item

	if item.itemType then
		local itemType = tostring(item.itemType)
		if not s_tGameItemTypes[itemType] then
			s_tGameItemTypes[itemType] = {}
		end
		table.insert(s_tGameItemTypes[itemType], strNextUNID)
	end
	
	-- Download this new Game Item's image
	--self:downloadGameItemIcon{player = event.player, UNID = tostring(s_iNextUNID)}

	saveGameItem(tonumber(s_iNextUNID), item)

	local newUNID = s_iNextUNID
	
	s_iNextUNID = s_iNextUNID + 1	

	
	SaveLoad.saveGameData("NextUNID", s_iNextUNID, parentID, parentType)

	if ZoneInfo.isLinkedZone() and not event.UNID then
		event.UNID = strNextUNID
		Events.sendBroadcastEvent("CREATE_NEW_GAME_ITEM", event)
	end

	return true, newUNID
end

-- Deleted an item from Game Items

function onGameItemDeleted(self, event)
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemDeleted() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemDeleted() event is nil."); return end
	if event.UNID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemDeleted() event.UNID is nil."); return end
	if event.player == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemDeleted() event.player is nil."); return end

	if ZoneInfo.isLinkedZone() then
		Events.sendBroadcastEvent("DELETE_GAME_ITEM_INTERNAL", event)
	end

	self:onGameItemDeletedInternal(event)

end

function onGameItemDeletedInternal(self, event)
	LOG_FUNCTION_CALL("onGameItemDeletedInternal")
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemDeletedInternal() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemDeletedInternal() event is nil."); return end
	if event.UNID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemDeletedInternal() event.UNID is nil."); return end
	if event.player == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemDeletedInternal() event.player is nil."); return end

	local strUNID = tostring(event.UNID)
	local iUNID = tonumber(event.UNID)
	self:cleanupGameItemReferences(event.player, strUNID)
	
	local gameItem = s_uItemProperties[strUNID]
	if gameItem == nil then
		log("ERROR DETECTED - ED-7951 - onGameItemDeletedInternal : game item with UNID "..tostring(strUNID).." was not found!")
		return
	end
	local gameItemType = gameItem.itemType
	local gameItemWaypointPID = tonumber(gameItem.waypointPID)
	
	--Clean up quest waypoints, if needed
	if gameItemType == "quest" and gameItemWaypointPID and gameItemWaypointPID > 0 then
		kgp.gameItemDeletePlacement(gameItemWaypointPID)
		Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = gameItemWaypointPID})
	end
	
	s_uItemProperties[strUNID] = nil

	for index, UNID in pairs(s_tGameItemTypes[gameItemType]) do
		if UNID == strUNID then
			table.remove(s_tGameItemTypes[gameItemType], index)
			if Toolbox.isContainerEmpty(s_tGameItemTypes[gameItemType]) then
				s_tGameItemTypes[gameItemType] = nil
			end
		end
	end

	
	-- Do we need to check up on the quests?
	if gameItemType == "quest" then
		-- Update quest prereqs
		validateQuestPrereqs()
		
		-- Validate quest giver quests
		validateQuestGiverQuests()

		Events.sendEvent("QUESTS_INVALIDATED", {questUNIDs = {iUNID}})
	end
	
	-- If this was a loot item
	local isLoot = isLootItem(gameItem)
	if isLoot then
		-- Remove this item if it's part of a quest prereq or reward
		local questUpdated, questsToBeRemoved = validateQuestItems()
		if questUpdated then
			Events.sendEvent("QUESTS_INVALIDATED", {questUNIDs = questsToBeRemoved})
		end
	end

	validatePlayerInventories()

	validateRecipes()

	validateVendors()

	-- Validate item containers and safes
	validateItemContainer(event.UNID)
	
	Events.sendEvent("OBJECT_REMOVE_SAFE", {UNID = tonumber(event.UNID)})
	
	
	
	-- Validate spawners
	Events.sendEvent("OBJECT_DELETE", {UNID = tonumber(event.UNID)}) 

	GII.gameItemDelete(tonumber(event.UNID))
	
	Events.sendClientEvent("FRAMEWORK_GAME_ITEM_UPDATED", {itemUNID = event.UNID})
end

-- WOK call to create a spawner for an object
function WOKcreateSpawner(self, event)
	LOG_FUNCTION_CALL("WOKcreateSpawner")
	if self == nil then log("Controller_Inventory - WOKcreateSpawner() self is nil."); return end
	if event == nil then log("Controller_Inventory - WOKcreateSpawner() event is nil."); return end
	if event.player == nil then log("Controller_Inventory - WOKcreateSpawner() event.player is nil."); return end
	if event.player.ID == nil then log("Controller_Inventory - WOKcreateSpawner() event.player.ID is nil."); return end
	if event.item == nil then log("Controller_Inventory - WOKcreateSpawner() event.item is nil."); return end
	local newItem = portWOKFieldsToGameItem(event.item)
	
	-- Initialize this world's DB if it has never been setup before we add this new Game Item
	if not s_bIsControllerReady then
		if not s_iNextUNID then
			initFrameworkDB()
		end
		
		local eventData = {
			gameItem	= newItem,
			player		= event.player
		}
		
		Events.sendEvent("FRAMEWORK_INSTANTIATE", eventData)
		return
	end
	
	local matchingItem = {}
	-- Does this Game Item exist?
	for UNID, props in pairs(s_uItemProperties) do
		-- Found a matching Game Item for this GIGLID
		if props and props.GIGLID and (props.GIGLID == newItem.GIGLID) then
			matchingItem = {UNID = UNID}
			for i, v in pairs(props) do
				matchingItem[i] = v
			end
			break
		end
	end
	-- Do we need to make a Game Item out of this new item?
	if matchingItem.UNID == nil then
		local fauxEvent = {
			mode			= event.mode, 
			item			= newItem, 
			player			= event.player, 
			instantiating	= event.instantiating
		}
		self:WOKaddToGame(fauxEvent)
		-- NOTE: Once the new Game Item is created, we'll return to this call again to hit the else below
	else
		local playerPos = event.player.position
		if playerPos == nil then log("Controller_Inventory - WOKcreateSpawner() event.player.position is nil."); return end
		-- TODO: Offset placement by look direction like regular build placement
		local position = {
			x = playerPos.x,
			y = playerPos.y,
			z = playerPos.z, 
			rx = playerPos.rx,
			ry = playerPos.ry,
			rz = playerPos.rz, 
			player = event.player
		}
		local success, spawner, newSpawner, spawnerUNID = placeItemSpawn(self, event.player, matchingItem.UNID, position)
		
		if success and not event.instantiating then
			kgp.playerLoadMenu(event.player.ID, "Framework_SaveVerify.xml")
			local eventData = {
				UNID = matchingItem.UNID,
				item = newItem,
				mode = "CreateSpawner",
				itemCreated = event.gameItemCreated,
				spawner = spawner,
				spawnerUNID  = spawnerUNID
			}
			Events.sendClientEvent("FRAMEWORK_SAVE_INFO", eventData, event.player.ID)
		end
	end
end

-- Call from the WOK Inventory to place a Game Item
function WOKplaceGameItem(self, event)
	LOG_FUNCTION_CALL("WOKplaceGameItem")
	if self == nil then log("Controller_Inventory - WOKplaceGameItem() self is nil."); return end
	if event == nil then log("Controller_Inventory - WOKplaceGameItem() event is nil."); return end
	if event.player == nil then log("Controller_Inventory - WOKplaceGameItem() event.player is nil."); return end
	if event.player.ID == nil then log("Controller_Inventory - WOKplaceGameItem() event.player.ID is nil."); return end
	if event.item == nil then log("Controller_Inventory - WOKplaceGameItem() event.item is nil."); return end
	
	local newItem = portWOKFieldsToGameItem(event.item)

	-- 06.06.2016 - TEMP HACK TO ENSURE ALL WORLD OBJECTS HAVE BEHAVIOR
	-- THIS SHOULD GET REMOVED ONCE WE ARE SURE ALL WORLD OBJECTS ARE UPDATED ON THE DB.
	if( newItem.behavior == nil ) then
		newItem.behavior = "placeable_loot"
	end
	
	-- Initialize this world's DB if it has never been setup before we add this new Game Item
	if not s_bIsControllerReady then
		if not s_iNextUNID then
			initFrameworkDB()
		end
		
		local eventData = {
			gameItem = newItem, 
			--[[{UNID 	= tonumber(matchingItem.UNID),
			GIGLID 	= tonumber(newItem.GIGLID),
			itemType = newItem.itemType,
			behavior = newItem.behavior,]]
			player 	= event.player
		}
		
		Events.sendEvent("FRAMEWORK_INSTANTIATE", eventData)
		return
	end
	
	local matchingItem = {}
	-- Does this Game Item exist?
	for UNID, props in pairs(s_uItemProperties) do
		-- Found a matching Game Item for this GIGLID
		if props and props.GIGLID and (props.GIGLID == newItem.GIGLID) then
			matchingItem = {UNID = UNID}
			for i, v in pairs(props) do
				matchingItem[i] = v
			end
			break
		end
	end
	-- Do we need to make a Game Item out of this new item?
	if matchingItem.UNID == nil then
		local fauxEvent = {mode = event.mode, item = newItem, player = event.player, instantiating = event.instantiating}
		self:WOKaddToGame(fauxEvent)
		-- NOTE: Once the new Game Item is created, we'll return to this call again to hit the else below
	else
		local playerPos = event.player.position
		if playerPos == nil then log("Controller_Inventory - WOKcreateSpawner() event.player.position is nil."); return end
		
		-- TODO: Offset placement by look direction like regular build placement
		local fauxEvent = {
			GLID		= tonumber(newItem.GLID),
			UNID		= tonumber(matchingItem.UNID),
			behavior	= newItem.behavior,
			player 		= event.player,
			x  = playerPos.x,   y = playerPos.y,   z = playerPos.z,
			rx = playerPos.rx, ry = playerPos.ry, rz = playerPos.rz
		}
		local newPID = self:onPlayerBuildPlaceItem(fauxEvent)
		
		if newPID and not event.instantiating then
			kgp.playerLoadMenu(event.player.ID, "Framework_SaveVerify.xml")
			local eventData = {
				UNID		= matchingItem.UNID, 
				item		= newItem, 
				mode		= "Place",
				itemCreated	= event.gameItemCreated
			}
			Events.sendClientEvent("FRAMEWORK_SAVE_INFO", eventData, event.player.ID)
		end
	end
end

-- WOK call to add an object to the Game System
function WOKaddToGame(self, event)
	LOG_FUNCTION_CALL("WOKaddToGame")
	-- Check inputs
	if self == nil then log("Controller_Inventory - WOKaddToGame() self is nil."); return end
	if event == nil then log("Controller_Inventory - WOKaddToGame() event is nil."); return end
	if event.player == nil then log("Controller_Inventory - WOKaddToGame() event.player is nil."); return end
	if event.player.ID == nil then log("Controller_Inventory - WOKaddToGame() event.player.ID is nil."); return end
	if event.item == nil then log("Controller_Inventory - WOKaddToGame() event.item is nil."); return end
	local newItem = portWOKFieldsToGameItem(event.item)
	
	-- Convert any nested table that wasn't accounted for through the auto-xml parser into a table
	for i,v in pairs(event.item) do
		if isNestedKey(i) and type(v) == "string" then
			local tbl = string.gsub(v, "\'", "\"")
			event.item[i] = Events.decode(tbl)
		end
	end

	-- If we're overwriting
	if event.overwrite then
		local UNIDToOverwrite = event.UNIDToOverwrite
		if UNIDToOverwrite == nil then log("Controller_Inventory - WOKaddToGame() event.UNIDToOverwrite is nil."); return end
		local editedGameItem = s_uItemProperties[tostring(UNIDToOverwrite)]
		if editedGameItem == nil then log("Controller_Inventory - WOKaddToGame() No registered Game Item with UNIDToOverwrite["..tostring(UNIDToOverwrite).."] is nil."); return end

		newItem.name = editedGameItem.name
		
		self:editGameItem{	
			UNID 			= UNIDToOverwrite,
			properties 		= newItem,
			preserveGIGLID	= true,
			mode 			= event.mode,
			instantiating  	= event.instantiating,
			player 		 	= event.player
		}
	else
		-- Check for unique name if we're not overwriting
		-- NOTE: If the name is not unique, the NameResolver will open from uniqueNameCheck() and pass the results back down again
		local isUnique = self:uniqueNameCheck{	
			name 			= newItem.name,
			item 		  	= newItem,
			mode 		  	= event.mode,
			player			= event.player,
			instantiating	= event.instantiating
		}
		if isUnique then
			local success, UNID = self:createGameItem{properties = newItem, player = event.player}
			
			if success then
				-- If all we're doing is adding a game item
				if event.mode == "AddToGame" then
					kgp.playerLoadMenu(event.player.ID, "Framework_SaveVerify.xml")
					Events.sendClientEvent("FRAMEWORK_SAVE_INFO", {UNID = UNID, item = newItem, mode = "AddToGame"}, event.player.ID)
					
				-- If we were making a spawner, head on back up
				elseif event.mode == "CreateSpawner" then
					local fauxEvent = {mode = event.mode, item = newItem, player = event.player, gameItemCreated = true, instantiating = event.instantiating}
					self:WOKcreateSpawner(fauxEvent)
				
				-- If we were placing a Game Item
				elseif event.mode == "Place" then
					local fauxEvent = {mode = event.mode, item = newItem, player = event.player, gameItemCreated = true, instantiating = event.instantiating}
					self:WOKplaceGameItem(fauxEvent)
				end
			else
				log("Controller_Inventory - WOKaddToGame() - Failed to create new Game Item with createGameItem(). metaData["..tostring(Events.encode(newItem)).."]")
			end
		end
	end
end

-- Check for unique name
function uniqueNameCheck(self, event)
	LOG_FUNCTION_CALL("uniqueNameCheck")
	if self == nil then log("Controller_Inventory - uniqueNameCheck() self is nil."); return end
	if event == nil then log("Controller_Inventory - uniqueNameCheck() event is nil."); return end
	if event.name == nil then log("Controller_Inventory - uniqueNameCheck() event.name is nil."); return end
	if event.item == nil then log("Controller_Inventory - uniqueNameCheck() event.item is nil."); return end
	if event.mode == nil then log("Controller_Inventory - uniqueNameCheck() event.mode is nil."); return end
	if event.player == nil then log("Controller_Inventory - uniqueNameCheck() event.player is nil."); return end
	local itemData = event.item
	local itemName = event.name
	local itemType = itemData.itemType
	local GLID = itemData.GLID
	local mode = event.mode
	local player = event.player
	local playerID = event.player.ID

	if playerID == nil then log("Controller_Inventory - uniqueNameCheck() playerID is nil."); return end
	
	local isUnique, uniqueName, conflictingUNID = checkForUniqueName{name = itemName, itemType = itemType}
	
	-- If the name is not unique and the Framework isn't online
	if not isUnique and event.instantiating then
		-- Overwrite this game item
		local fauxEvent = {mode = mode, item = itemData, player = event.player, overwrite = true, UNIDToOverwrite = conflictingUNID, instantiating = event.instantiating}
		self:WOKaddToGame(fauxEvent)
		
	-- If this name is not unique and we got here from the Inventory
	elseif not isUnique and ((mode == "AddToGame") or (mode == "CreateSpawner") or (mode == "Place")) then
		-- Resolve the non-unique name
		kgp.playerLoadMenu(playerID, "Framework_NameResolver.xml")
		
		local data = {	
			mode			= mode,
			name 			= itemName,
			suggestedName   = uniqueName,
			itemType 		= itemType,
			GLID 			= GLID,
			conflictingUNID	= conflictingUNID,
			itemData		= itemData
		}
		Events.sendClientEvent("FRAMEWORK_RESOLVE_THIS", data, playerID)
	else
		-- Return the results for whoever sent this
		local data = {	
			original   = itemName,
			isUnique   = isUnique,
			uniqueName = uniqueName,
			mode 	   = mode
		}
		Events.sendClientEvent("FRAMEWORK_UNIQUE_NAME_RETURN", data, playerID)
	end
	
	-- Return the results if this call came from within the server
	return isUnique, uniqueName
end

-- Goto Item from Framework
function onGameItemGoto(self, event)
	LOG_FUNCTION_CALL("onGameItemGoto")
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemGoto() self is nil."); return end
	if event.player == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemGoto() event.player is nil."); return end
	if event.UNID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemGoto() event is nil."); return end
	local UNID = tostring(event.UNID)
	if UNID == tostring(GameDefinitions.PATHWAYPOINT_UNID) then
		local pathWaypoint = s_uItemInstances[event.PID]
		if pathWaypoint == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemGoto() pathWaypoint is nil."); return end
		local pathParent = s_uItemInstances[pathWaypoint.parentPathPID]
		if pathParent == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onGameItemGoto() pathParent is nil."); return end
		UNID = tostring(pathParent.UNID)
	end

	if UNID and UNID ~= "nil" then
		gotoGameItemInSystem(event.player.name, UNID)
	end
end

-- Crafts an editGameItem event for Controller_Spawn to weigh against current server load
function editGameItem(self, event )
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - editGameItem() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Inventory - editGameItem() event is nil."); return end
	if event.UNID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - editGameItem() event.UNID is nil."); return end
	if event.properties == nil then Console.Write(Console.DEBUG, "Controller_Inventory - editGameItem() event.properties is nil."); return end

	if ZoneInfo.isLinkedZone() then
		Events.sendBroadcastEvent("EDIT_GAME_ITEM_INTERNAL", event)
	end

	self:editGameItemInternal(event)
end

function editGameItemInternal(self, event)

	LOG_FUNCTION_CALL("editGameItem")
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - editGameItem() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Inventory - editGameItem() event is nil."); return end
	if event.UNID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - editGameItem() event.UNID is nil."); return end
	if event.properties == nil then Console.Write(Console.DEBUG, "Controller_Inventory - editGameItem() event.properties is nil."); return end
	
	local UNID = tostring(event.UNID)
	local gameItem = s_uItemProperties[UNID]

	if gameItem then	
		local itemInfo = {UNID = tonumber(UNID)}
		if gameItem then
			for i, v in pairs(gameItem) do
				itemInfo[i] = v
			end
		end
		
		for attribute, value in pairs(event.properties) do
			itemInfo[attribute] = value
		end
		
		local classInfo = getBehaviorParams(itemInfo.UNID, itemInfo.behaviorParams)
		if classInfo then
			for i, v in pairs(classInfo) do
				itemInfo[i] = v
			end
		end
		
		itemInfo.pendingID = s_iPendingID
		s_tPendingQueue[s_iPendingID] = event
		s_iPendingID = s_iPendingID + 1
		
		Events.sendEvent("OBJECT_UPDATE", itemInfo) -- Handled by Controller_Spawn. Returned through onUpdateGameItem()
	end	
end

function cleanupGameItemReferences(self, player, itemUNID)
	LOG_FUNCTION_CALL("cleanupGameItemReferences")
	local item = s_uItemProperties[itemUNID]
	
	if item == nil then
		log("ERROR DETECTED - ED-7837 - item was nil in cleanupGameItemReferences!  itemUNID = "..tostring(itemUNID))
		return
	end

	-- Check if this item is in a vendor
	for UNID, properties in pairs(s_uItemProperties) do
		if properties and properties.itemType == GameDefinitions.ITEM_TYPES.CHARACTER and properties.behavior == GameDefinitions.ITEM_TYPES.VENDOR then
			local modified = checkVendor(UNID, itemUNID)

			if modified then
				self:editGameItem({player = player, UNID = UNID, properties = Toolbox.deepCopy(properties)})
			end
		end
	end

	if item.itemType == GameDefinitions.ITEM_TYPES.AMMO then
		for UNID, properties in pairs(s_uItemProperties) do
			if properties and properties.itemType == GameDefinitions.ITEM_TYPES.WEAPON then
				if properties.ammoType and tonumber(properties.ammoType) == tonumber(itemUNID) then
					properties.ammoType = 0
					self:editGameItem({player = player, UNID = UNID, properties = Toolbox.deepCopy(properties)})
				end
			end
		end
	elseif item.itemType == GameDefinitions.ITEM_TYPES.WEAPON or item.itemType == GameDefinitions.ITEM_TYPES.TOOL then
		for UNID, properties in pairs(s_uItemProperties) do
			if properties and properties.itemType == GameDefinitions.ITEM_TYPES.HARVESTABLE then
				if properties.behaviorParams and properties.behaviorParams.requiredTool and tonumber(properties.behaviorParams.requiredTool) == tonumber(itemUNID) then
					properties.behaviorParams.requiredTool = 0
					self:editGameItem({player = player, UNID = UNID, properties = Toolbox.deepCopy(properties)})
				end
			end
		end
	elseif item.itemType == GameDefinitions.ITEM_TYPES.PLACEABLE and item.behavior == "campfire" then
		for UNID, properties in pairs(s_uItemProperties) do
			if properties and properties.itemType == GameDefinitions.ITEM_TYPES.RECIPE or properties.itemType == GameDefinitions.ITEM_TYPES.BLUEPRINT then
				if properties.proxReq and tonumber(properties.proxReq) == tonumber(itemUNID) then
					properties.proxReq = 0
					self:editGameItem({player = player, UNID = UNID, properties = Toolbox.deepCopy(properties)})
				end
			end
		end		
	end
end


function validateAttribute(self, event)
	LOG_FUNCTION_CALL("validateAttribute")
	
	if event.player == nil then return end
	if event.player.ID == nil then return end
	local attribute = event.attribute
	local itemUNID = tostring(event.UNID)
	local values = event.values

	local invalidChange = false

	if attribute == "flag area" then
		local gameItem = s_uItemProperties[itemUNID]
		if gameItem then
			if  (values.radius and gameItem.radius ~= values.radius) or
				(values.height and gameItem.height ~= values.height) or
				(values.width  and gameItem.width ~= values.width) or
				(values.depth  and gameItem.depth ~= values.depth) then
					if ZoneInfo.isLinkedZone() then
						invalidChange = kgp.gameItemIsPlaced(itemUNID)
					else
						for ID, uItem in pairs(s_uItemInstances) do
							if uItem and tostring(uItem.UNID) == itemUNID then
								invalidChange = true
								break
							end
						end
					end
			end
		end
	end

	if attribute == "meshType" and itemUNID then
		if ZoneInfo.isLinkedZone() then
				invalidChange = kgp.gameItemIsPlaced(itemUNID)
		else
			for ID, uItem in pairs(s_uItemInstances) do
				if uItem and tostring(uItem.UNID) == itemUNID then
					invalidChange = true
					break
				end
			end
		end
	end
	Events.sendClientEvent("FRAMEWORK_ATTRIBUTE_RETURN", {valid = invalidChange, attribute = attribute, value = value}, event.player.ID)
end




-- Rolls for a gem chest
function rollForGemChest(self, event)
	LOG_FUNCTION_CALL("rollForGemChest")
	
	if self == nil then log("Controller_Inventory - rollForGemChest() self is nil."); return end
	if event == nil then log("Controller_Inventory - rollForGemChest() event is nil."); return end
	if event.player == nil then log("Controller_Inventory - rollForGemChest() event.player is nil."); return end

	local gemInfo = nil
	local giveGem = false
	local gemChance = 0
	local roll = math.random() * 100
	if s_bCanDropGems then
		gemChance = 7.5
		giveGem = gemChance >= roll
		local gemData = s_uItemProperties[tostring(GameDefinitions.GEM_CHEST_UNID)]
		if gemData and giveGem then
			gemInfo = {	
				UNID		= GameDefinitions.GEM_CHEST_UNID,
				properties	= gemData,
				count		= 1
			}
		end	
	end
	
	log("TEMP-LOG. !REMOVE! Controller_Inventory - rolling for gem. gemChance["..tostring(gemChance).."%] roll["..tostring(roll).."] giveGem["..tostring(giveGem).."]")

	-- TODO: PJD - Remove this block if gems end up dropping only in Top Worlds
	-- if event.player.lootRate and event.player.lootRate > 0 then
		-- local rewardsPerHour = GEMS_REWARDS_PER_HOUR / GameDefinitions.GEM_AVERAGE_VALUE
		-- local gemChance = math.min((event.player.lootRate / 60 / 60 * rewardsPerHour * 100), 100)
		
		-- local roll = math.random()*100
		-- if s_bInTopWorld then
			-- gemChance = math.min(gemChance * 2, 100)
		-- end
		-- giveGem = gemChance >= roll
		
		-- log("TEMP-LOG. !REMOVE! Controller_Inventory - rolling for gem. rewardsPerHour["..tostring(rewardsPerHour).."] lootRate["..tostring(event.player.lootRate).."] gemChance["..tostring(gemChance).."%] roll["..tostring(roll).."] giveGem["..tostring(giveGem).."]")
		
		-- local gemData = s_uItemProperties[tostring(GameDefinitions.GEM_CHEST_UNID)]
		-- if gemData then
			-- gemInfo = {UNID = GameDefinitions.GEM_CHEST_UNID,
					   -- properties = gemData,
					   -- count = 1
					  -- }
		-- end

	-- end
	
	-- Pass result to Class_ItemContainer for loot table population
	local eventData = {
		giveGem	= giveGem,
		gemInfo	= gemInfo,
		player	= event.player,
		PID		= event.PID
	}
	Events.sendEvent("FRAMEWORK_GEM_CHEST_ROLL_RESULT", eventData, event._sender)
end


-- Gets recipes for the the Crafting menu
function getRecipes(self, event)
	LOG_FUNCTION_CALL("getRecipes")
	
	
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getRecipes() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getRecipes() event is nil."); return end
	if event.player == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getRecipes() event.player is nil."); return end
	if event.player.ID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getRecipes() event.player is nil."); return end
	
	local worldRecipes = {}
	local worldBlueprints = {}
	
	local gameItemBatch = {	
		[1] = {	
			worldBlueprints = {}, 
			worldRecipes = {}
		}
	}
	local batchIndex = 1
	local batchSize = 0

	for UNID, item in pairs(s_uItemProperties) do
		if item and item.itemType == GameDefinitions.ITEM_TYPES.RECIPE or item.itemType == GameDefinitions.ITEM_TYPES.BLUEPRINT then
			if batchSize >= GameDefinitions.MAX_GAME_ITEM_QUEUE_SIZE then
				batchIndex = batchIndex + 1
				batchSize = 0
				gameItemBatch[batchIndex] = {worldBlueprints = {}, worldRecipes = {}}
			end		
		
			local recipe = packageRecipeToClient(UNID, item)
			recipe = Toolbox.deepCopy(recipe, true)
			
			if recipe.itemType == GameDefinitions.ITEM_TYPES.BLUEPRINT then
				gameItemBatch[batchIndex].worldBlueprints[UNID] = recipe
			else
				gameItemBatch[batchIndex].worldRecipes[UNID] = recipe
			end
			
			batchSize = batchSize + 1
		end
	end

	local batchCount = #gameItemBatch
	for i, gameItem in ipairs(gameItemBatch) do
		local batchEvent = {
			recipes = gameItem.worldRecipes, 
			blueprints = gameItem.worldBlueprints
		}
		if i == 1 then
			batchEvent.initial = true
		end
		if i == batchCount then
			batchEvent.finished = true
		end

		Events.sendClientEvent("FRAMEWORK_RETURN_RECIPES", batchEvent, event.player.ID)
	end
end


-- Extracts the GLIDs from a Game Item for Shop/Inventory upload
function extractGameItemGLIDs(self, event)
	LOG_FUNCTION_CALL("extractGameItemGLIDs")
	
	-- Error check inputs
	if self == nil then log("Controller_Game - extractGameItemGLIDs() self is nil."); return end
	if event == nil then log("Controller_Game - extractGameItemGLIDs() event is nil."); return end
	if event.player == nil then log("Controller_Game - extractGameItemGLIDs() event.player is nil."); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Game - extractGameItemGLIDs() playerID is nil."); return end
	local returnType = event.returnType
	if returnType == nil then log("Controller_Game - extractGameItemGLIDs() event.returnType is nil."); return end
	local UNID = event.UNID
	if UNID == nil then log("Controller_Game - extractGameItemGLIDs() No Game Item UNID passed in."); return end
	
	-- Extract required GLIDs for a Game Item by type
	local GLIDs = extractGLIDsByUNID(UNID)
	-- Convert this hash table into an indexed table
	local newGLIDs = {}
	for GLID,_ in pairs(GLIDs) do
		table.insert(newGLIDs, GLID)
	end
	-- What shall I do with this data?
	if returnType == "call" then
		return newGLIDs
		
	-- Send it to Framework_PublishGameItems for publishing
	elseif returnType == "web" then
		Events.sendClientEvent("FRAMEWORK_RETURN_GAME_ITEM_GLIDS", {GLIDs = newGLIDs}, playerID)
	end
end

-- Extract all Game Item GLIDs
function extractAllGameItems(self, event)
	LOG_FUNCTION_CALL("extractAllGameItems")
	
	-- Error check inputs
	if self == nil then log("Controller_Game - extractAllGameItems() self is nil."); return end
	
	local totalGLIDs = {}
	for UNID, props in pairs(s_uItemProperties) do
		local GLIDs = self:extractGameItemGLIDs{returnType = "call", UNID = UNID}
		for GLID,_ in pairs(GLIDs) do
			totalGLIDs[GLID] = true
		end
	end
end


-- When an item is picked up manually
function onItemRemoved(self, event)
	LOG_FUNCTION_CALL("onItemRemoved")
	
	local PID = event.PID
	if PID == nil then log("Controller_Inventory - onItemRemoved() event.PID is nil."); return end
	
	-- If the removed PID is active, clean it up
	if s_tActiveDOs[PID] then
		local itemInfo = getItemInfoFromPID(PID)
		
		if itemInfo.behavior then
			Events.sendEvent("OBJECT_PICKUP", {PID = PID})
			Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = PID})
		else
			Events.sendClientEvent("FRAMEWORK_UNREGISTER_BATTLE_OBJECT", {ID = PID})
		end

		if itemInfo.behavior and itemInfo.behavior == "waypoint" then
			validateQuestWaypoint(PID)
		end
		
		s_tActiveDOs[PID] = nil
		
		-- Check if there's only one activeDO left
		countActiveDOs()
	end
end

function activeDODestroyed(self, event)
	if s_tActiveDOs[event.activeDODestroyedPID] then
		s_tActiveDOs[event.activeDODestroyedPID] = nil
	end
	-- Check if there's only one activeDO left
	countActiveDOs()
end

-- Request handler for object associations
function onRequestObjectAssociations(self, event)
	LOG_FUNCTION_CALL("onRequestObjectAssociations")
	
	local PID = event.PID
	Events.sendEvent("FRAMEWORK_RETURN_OBJECT_ASSOCIATIONS", {objectAssociations = getObjectAssociations(PID), PID = PID})
end

-- Request handler for object associations
function onClientRequestObjectAssociations(self, event)
	LOG_FUNCTION_CALL("onClientRequestObjectAssociations")
	
	local playerID = event.player.ID
	Events.sendClientEvent("FRAMEWORK_CLIENT_UPDATE_OBJECT_ASSOCIATIONS", {objectAssociations = s_tObjectAssociations}, playerID)
end



-- Gets the timed vends table for a given player
function getTimedVends(self, event)
	LOG_FUNCTION_CALL("getTimedVends")
	
	-- Extract player info
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() event is nil."); return end
	if event.PID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() event.PID is nil."); return end
	if event.player == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() event.player is nil."); return end
	if event.player.ID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() event.player.ID is nil."); return end
	if event.player.timedVends == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() event.player.timedVends is nil."); return end
	if event.player.timedVends[event.PID] == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() event.player.timedVends["..tostring(event.PID).."] is nil."); return end
	
	local tempVends = Toolbox.deepCopy(event.player.timedVends[event.PID])
	
	Events.sendClientEvent("FRAMEWORK_RETURN_TIMED_VENDS", {timedVends = tempVends, PID = event.PID}, event.player.ID)
end

-- Gets the timed vends table for a given player
function getAllTimedVends(self, event)
	LOG_FUNCTION_CALL("getAllTimedVends")
	
	-- Extract player info
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() event is nil."); return end
	if event.player == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() event.player is nil."); return end
	if event.player.ID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() event.player.ID is nil."); return end
	if event.player.timedVends == nil then Console.Write(Console.DEBUG, "Controller_Inventory - getTimedVends() event.player.timedVends is nil."); return end
	
	local tempVends = Toolbox.deepCopy(event.player.timedVends)
	Events.sendClientEvent("FRAMEWORK_RETURN_ALL_TIMED_VENDS", {timedVends = tempVends}, event.player.ID)
end



-- Called when a set of timed vends needs to be saved for a player
function saveTimedVend(self, event)
	LOG_FUNCTION_CALL("saveTimedVend")
	
	local vendPID = event.PID
	local player = event.player
	-- Extract player info
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - saveTimedVend() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Inventory - saveTimedVend() event is nil."); return end
	if vendPID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - saveTimedVend() event.PID is nil."); return end
	if event.timedVends == nil then Console.Write(Console.DEBUG, "Controller_Inventory - saveTimedVend() event.timedVends is nil."); return end
	if player == nil then Console.Write(Console.DEBUG, "Controller_Inventory - saveTimedVend() event.player is nil."); return end
	if player.name == nil then Console.Write(Console.DEBUG, "Controller_Inventory - saveTimedVend() player.name is nil."); return end
	if player.timedVends == nil then Console.Write(Console.DEBUG, "Controller_Inventory - saveTimedVend() player.timedVends is nil."); return end
	
	-- Assign new timedVends for this player at the specified PID
	-- player.timedVends[vendPID] = Toolbox.deepCopy(event.timedVends)

	local iPID = tonumber(vendPID)
	local timedVends = {}
	
	for vendID, finishTime in pairs(event.timedVends) do
		if finishTime ~= 0 then
			timedVends[vendID] = finishTime
		end
	end
	
	player.timedVends[iPID] = timedVends

	local vendCopies = {}
	-- Loop through all player's vendor PIDs
	for PID, vendInfo in pairs(player.timedVends) do
		-- Clear each PID's vends
		local vendCopy = {}
		-- Loop through the player's timed vends
		for vendID, finishTime in pairs(vendInfo) do
			-- Check for faulty finish times
			if finishTime ~= 0 then
				vendCopy[tostring(vendID)] = finishTime
			end
		end
		
		vendCopies[tostring(PID)] = vendCopy
	end
	
	--Send the new saved info the the TimedVendorHandler
	local tempVends = Toolbox.deepCopy(player.timedVends[vendPID])
	Events.sendClientEvent("FRAMEWORK_RETURN_TIMED_VENDS", {timedVends = tempVends, PID = vendPID}, event.player.ID)
	-- Save the new timedVends table off to the DB for the future

	local zoneInstanceID, zoneIndex, zoneType = kgp.gameGetCurrentInstance()
	SaveLoad.savePlayerData(player.name, "timedVends", Events.encode(vendCopies), zoneInstanceID, zoneType)
end

function getObjectAssociations(objectPID) -- Only returns the FIRST association. TO DO: Make it return ALL associations
	LOG_FUNCTION_CALL("getObjectAssociations")
	
	if objectPID then
		for _, objectAssociations in pairs(s_tObjectAssociations) do
			for _, associatedPID in pairs(objectAssociations) do
				if associatedPID == objectPID then
					return objectAssociations
				end
			end
		end
	end
	
	return {}
end


-- Called when a player places an item in the zone
function onPlayerBuildPlaceItem(self, event)
	LOG_FUNCTION_CALL("onPlayerBuildPlaceItem")
	
	-- Error check inputs
	if self == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() self is nil."); return end
	if event == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event is nil."); return end
	-- Was a valid player passed in?
	local player = event.player
	local playerID = player and player.ID
	if player == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event.player is nil."); return end
	if playerID == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event.player.ID is nil."); return end
	if event.GLID == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event.GLID is nil."); return end
	if event.UNID == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event.UNID is nil."); return end
	if event.x == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event.x is nil."); return end
	if event.y == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event.y is nil."); return end
	if event.z == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event.z is nil."); return end
	if event.rx == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event.rx is nil."); return end
	if event.ry == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event.ry is nil."); return end
	if event.rz == nil then log("Controller_Inventory - onPlayerBuildPlacedItem() event.rz is nil."); return end
	
	if event.rx == 0 and event.rz == 0 then
		log("ERROR DETECTED - ED-6840 : onPlayerBuildPlaceItem: event.rx == 0 and event.rz == 0")
		Debug.printTableError("ERROR DETECTED - ED-6840 : event", event)
		Debug.printStack()
		return
	end
	
	-- Instantiate this object if the Framework is online.
	if s_bIsControllerReady then

		local permission = checkObjectPlaceability(event)
		
		if( permission == GameDefinitions.PLACEABILITY.ALLOWED ) then
			
			-- Place the item in the game
			local PID = placeGameItem(event)

			-- Notify clients
			Events.sendClientEvent("FRAMEWORK_PLACE_GAME_ITEM_CONFIRMATION", {success = true, PID = PID}, playerID)
			
			-- Collect me some metrics
			collectMetricsForPlacedItem(event.UNID, player.name, player.mode)
			
			-- Return results
			return PID
		end
	end
end


function onPlacePendingResponse(self, event)
	LOG_FUNCTION_CALL("onPlacePendingResponse")
	
	local pendingID = event.pendingID
	local pendingSuccess = event.pendingSuccess
	local maxLoad = event.maxLoad
	local peakLoad = event.peakLoad
	local currentLoad = event.currentLoad
	local PID = nil

	if pendingSuccess then
		local placementInfo = s_tPendingQueue[pendingID]
		
		if placementInfo then
			if placementInfo.behavior and placementInfo.behavior == "teleporter"  and not placementInfo.worldTeleporter then
				placementInfo.x = placementInfo.x + (5 * placementInfo.rz)
				placementInfo.z = placementInfo.z - (5 * placementInfo.rx)
			end

			local placementGLID = tonumber(placementInfo.GLID)
			local placementProperties = s_uItemProperties[placementInfo.UNID]
			
			if placementProperties and placementProperties.meshType and placementProperties.meshType ~= 1 then
				placementGLID = PAPER_DOLL_MAGIC_GLID
			end
			
			if placementGLID then
				PID = kgp.gameItemPlace(	placementInfo.player.ID, 
											placementGLID, 
											placementInfo.UNID, 
											placementInfo.x, placementInfo.y, placementInfo.z, 
											placementInfo.rx, placementInfo.ry, placementInfo.rz)
			end
			
			if PID then
				s_tActiveDOs[PID] = {	
					UNID	= placementInfo.UNID, 
					owner	= placementInfo.player.name
				}
				
				-- Check if there's only one activeDO left
				countActiveDOs()
				
				Events.sendEvent("OBJECT_INSTANCE_CLASS_FINALIZE", {pendingID = pendingID, PID = PID})
				
				if placementInfo.behavior and placementInfo.behavior == "teleporter" and not placementInfo.worldTeleporter then
					createTeleporterAssociations(PID, placementInfo)
				elseif placementInfo.behavior and placementInfo.behavior == "mover" then
					createMoverAssociations(PID, placementInfo)
				elseif placementInfo.behavior and placementInfo.jango then
					createFollowerAssociations(PID, placementInfo)
				end
			end
		end
	end
	
	local pendingOperation = s_tPendingQueue[pendingID]
	if pendingOperation and pendingOperation.UNID then
		
		local player = pendingOperation.player
		if player and player.ID then
			if pendingOperation.behavior == "pathWaypoint" then
				Events.sendClientEvent("PLACE_PATH_WAYPOINT_CONFIRMATION", {success = pendingSuccess, PID = PID}, player.ID)
			else
				Events.sendClientEvent("FRAMEWORK_PLACE_GAME_ITEM_CONFIRMATION", {success = pendingSuccess, PID = PID}, player.ID)
			end
		
			collectMetricsForPlacedItem(pendingOperation.UNID, player.name, player.mode)
		end
	end
	
	s_tPendingQueue[pendingID] = nil
end





-- Sent from Controller_Spawn once a Game Item edit request has been processed
function onUpdateGameItem(self, event)
	LOG_FUNCTION_CALL("onUpdateGameItem")
	
	local pendingID = event.pendingID
	if pendingID == nil or s_tPendingQueue[pendingID] == nil then return end
	local success = event.success
	if success then
		event = s_tPendingQueue[pendingID]
		local strUNID = tostring(event.UNID)
		local preserveGIGLID = event.preserveGIGLID --false if not passed
		local gameItem = s_uItemProperties[strUNID]

		for attribute, value in pairs(event.properties) do
			-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
			--Toolbox.setUserdataAtIndex(gameItem, attribute, value, true)
			gameItem[attribute] = value
		end
		
		-- Clear out GIGLID of edited items if necessary
		if not preserveGIGLID then
			gameItem.GIGLID = nil
		end

		if gameItem.itemType == GameDefinitions.ITEM_TYPES.BLUEPRINT then
			local validRecipe = validateRecipe(strUNID)

			validatePlayerInventories()
			validateItemContainer(strUNID)
			--If no valid, then return
			if not validRecipe then return end
		
		elseif gameItem.itemType == GameDefinitions.ITEM_TYPES.RECIPE then
			local validRecipe = validateRecipe(strUNID)
			--If no valid, then return
			if not validRecipe then return end
		elseif isLootItem(gameItem) then
			validatePlayerInventories()
			validateItemContainer(strUNID)
			
		-- Clean up deleted quests
		elseif gameItem.itemType == GameDefinitions.ITEM_TYPES.QUEST then
			-- Update all player quest icons and quest info
			Events.sendEvent("REQUEST_COMPLETE_PLAYER_QUEST_UPDATES")
		end

		if event.mode then
			-- If all we're doing is adding a game item
			if event.mode == "AddToGame" then
				kgp.playerLoadMenu(event.player.ID, "Framework_SaveVerify.xml")
				Events.sendClientEvent("FRAMEWORK_SAVE_INFO", {UNID = strUNID, item = event.properties, mode = "Overwrite"}, event.player.ID)
			-- If we were making a spawner, head on back up
			elseif event.mode == "CreateSpawner" then
				local fauxEvent = {mode = event.mode, item = event.properties, player = event.player, instantiating = event.instantiating}
				self:WOKcreateSpawner(fauxEvent)
			-- Placing Game Item
			elseif event.mode == "Place" then
				local fauxEvent = {mode = event.mode, item = event.properties, player = event.player, instantiating = event.instantiating}
				self:WOKplaceGameItem(fauxEvent)
			end
		end
		
		saveGameItem(tonumber(strUNID), gameItem)
	end
	if not event.instantiating then
		Events.sendClientEvent("FRAMEWORK_EDIT_GAME_ITEM_CONFIRMATION", {success = success}, s_tPendingQueue[pendingID].player.ID)
	end
	
	s_tPendingQueue[pendingID] = nil
end

-- Places a game item outside of the load limit enforcement
-- Used for unweighted objects and associated objects when the primary object has been load-validated
function placeGameItem(placementInfo)
	LOG_FUNCTION_CALL("placeGameItem")
	
	local PID = kgp.gameItemPlace(	placementInfo.player.ID, 
									placementInfo.GLID, 
									placementInfo.UNID, 
									placementInfo.x, placementInfo.y, placementInfo.z, 
									placementInfo.rx, placementInfo.ry, placementInfo.rz)
	
	-- Add this PID to the placedItems table
	s_tActiveDOs[PID] = {	
		UNID	= placementInfo.UNID,
		owner	= placementInfo.player.name
	}
	-- Check if there's only one activeDO left
	countActiveDOs()
	
	local itemInfo = getItemInfoFromPID(PID)
	if placementInfo.behavior then
		-- Load behavior details
		local classInfo = getBehaviorParams(placementInfo.UNID)
		for i, v in pairs(classInfo) do
			itemInfo[i] = v
		end
		itemInfo.owner = placementInfo.player.name
		itemInfo.UNID = placementInfo.UNID
		
		if placementInfo.additionalPathInfo then
			itemInfo.additionalPathInfo = placementInfo.additionalPathInfo
		end
		
		local eventData = {	
			PID			= PID, 
			behavior	= placementInfo.behavior, 
			input		= itemInfo
		}
		Events.sendEvent("OBJECT_INSTANCE_CLASS", eventData)
	else
		log("ERROR: Attempt to place object without behavior. PID = "..tostring(PID))
	end
	
	return PID
end

function createQuestWaypoint(self, event)
	LOG_FUNCTION_CALL("createQuestWaypoint")
--
	--local questInfo = event.questUNID and s_uItemProperties[tostring(event.questUNID)]
	--if questInfo == nil then return end
	local waypointInfo = s_uItemProperties[tostring(GameDefinitions.WAYPOINT_UNID)]
	if waypointInfo == nil then Console.Write(Console.DEBUG, "Waypoint game item data not found: Aborting quest waypoint creation!") return end
	if event.player == nil then return end
	if event.player.ID == nil then return end

	local questName = event.questName or "New Quest"
	
	waypointInfo.player = event.player
	waypointInfo.GLID = GameDefinitions.WAYPOINT_GLID --TODO: Should it be passed from the item editor?
	waypointInfo.UNID = GameDefinitions.WAYPOINT_UNID
	waypointInfo.x = event.player.position.x
	waypointInfo.y = event.player.position.y
	waypointInfo.z = event.player.position.z
	waypointInfo.rx = event.player.position.rx
	waypointInfo.ry = event.player.position.ry
	waypointInfo.rz = event.player.position.rz
	
	local PID = kgp.gameItemPlace(	waypointInfo.player.ID, 
									waypointInfo.GLID, 
									waypointInfo.UNID, 
									waypointInfo.x, waypointInfo.y, waypointInfo.z, 
									waypointInfo.rx, waypointInfo.ry, waypointInfo.rz)
	
	s_tActiveDOs[PID] = {
		UNID	= waypointInfo.UNID,
		owner	= waypointInfo.player.name
	}
					   
	local itemInfo = getItemInfoFromPID(PID)
	local classInfo = getBehaviorParams(waypointInfo.UNID)
	for i, v in pairs(classInfo) do
		itemInfo[i] = v
	end
	
	itemInfo.owner = waypointInfo.player.name
	itemInfo.UNID = waypointInfo.UNID
	
	itemInfo.name = questName .. " " .. itemInfo.name
	
	-- questInfo.waypointPID = tonumber(PID)
	-- local itemToSave = {}
	-- for key, value in pairs(questInfo) do
	-- 	if type(value) == "table" then
	-- 		itemToSave[key] = Events.encode(value)
	-- 	elseif type(value) == "userdata" then
	-- 		local tValue = Toolbox.deepCopy(value, true)
	-- 		itemToSave[key] = Events.encode(tValue)
	-- 	else
	-- 		itemToSave[key] = value
	-- 	end
	-- end
	-- itemToSave.GIID = tonumber(event.questUNID)
	-- itemToSave.GIGLID = nil
	-- GII.gameItemUpdate(itemToSave.GIID, itemToSave)
	
	itemInfo.animation = GameDefinitions.WAYPOINT_ANIM

	Events.sendEvent("OBJECT_INSTANCE_CLASS", {PID = PID, behavior = itemInfo.behavior, input = itemInfo})
	Events.sendClientEvent("FRAMEWORK_UPDATE_QUEST_WAYPOINT", {waypointPID = PID}, event.player.ID)
end

function removeQuestWaypoint(self, event)
	LOG_FUNCTION_CALL("removeQuestWaypoint")
	
	if event.player == nil then return end
	if event.player.ID == nil then return end
	if event.waypointPID == nil then return end

	--if not questInfo then Console.Write(Console.INFO, "Quest game item data not found: Aborting quest waypoint deletion!") return end
	
	local iWaypointPID = tonumber(event.waypointPID)
	if iWaypointPID and iWaypointPID > 0 then
		kgp.gameItemDeletePlacement(iWaypointPID)
		Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = iWaypointPID})

		Events.sendClientEvent("FRAMEWORK_UPDATE_QUEST_WAYPOINT", {waypointPID = 0}, event.player.ID)
		validateQuestWaypoint(iWaypointPID) --Validate in case we remove then do not save. This will make sure it saves properly
	end
end



-- Request to pickup a selected Game Item from Player Build Mode
function pickupGameItem(self, event)
	LOG_FUNCTION_CALL("pickupGameItem")
	-- Error check inputs
	if self == nil then log("Controller_Inventory - pickupGameItem() self is nil."); return false end
	if event == nil then log("Controller_Inventory - pickupGameItem() event is nil."); return false end
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Inventory - pickupGameItem() event.player is nil."); return false end
	if event.PID == nil then log("Controller_Inventory - pickupGameItem() event.PID is nil."); return false end
	
	
	-- Check if we, as the player, are allowed to remove the placeable object.
	local objectToPickup = s_tActiveDOs[event.PID]
	if( objectToPickup ) then
		local isPlayerOwnerOfItem = ((objectToPickup.owner == event.player.name) or (event.player.mode == "God")) 
		local doFlagsAllowRemoval = false
		
		-- If we are not the owner, then check on land claim flags to see if the player has special permissions to manipulate the object in question
		if not isPlayerOwnerOfItem  then
			local x, y, z = getDynamicObjectPosition(event.PID)
			local flags = getClaimFlagsAtPoint(x, y, z)
			
			for i, flag in pairs(flags) do
				if( flag.membersBag[event.player.name] == true ) then
					doFlagsAllowRemoval = true
				else
					doFlagsAllowRemoval = false
					break
				end
			end
		end
		
		
		if( isPlayerOwnerOfItem or doFlagsAllowRemoval ) then 
			--last game item, set flag to deinstantiate framework once picked up
			if event.lastGameItem == true then
				s_deinstantiateFramework = true
				s_deinstantiatingPlayer = event.player
			end
			local associatedObjects = getObjectAssociations(event.PID)
			if (next(associatedObjects)) then
				for _, associatedPID in pairs(associatedObjects) do
					self:onItemRemoved({PID = associatedPID})
					kgp.gameItemDeletePlacement(associatedPID)
				end
			else
				self:onItemRemoved({PID = event.PID})
				kgp.gameItemDeletePlacement(event.PID)
			end
		end
	end
end

function pickupLastGameItem(self, event)
	LOG_FUNCTION_CALL("pickupLastGameItem")
	-- Error check inputs
	if self == nil then log("Controller_Inventory - pickupLastGameItem() self is nil."); return false end
	if event == nil then log("Controller_Inventory - pickupLastGameItem() event is nil."); return false end
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Inventory - pickupLastGameItem() event.player is nil."); return false end
	if event.PID == nil then log("Controller_Inventory - pickupLastGameItem() event.PID is nil."); return false end
	event.lastGameItem = true
	self:pickupGameItem(event)
end

-- Request all spawners and instances of quest givers
function getQuestGiverSpawners(self, event)
	LOG_FUNCTION_CALL("getQuestGiverSpawners")
	
	
	-- Error check inputs
	if self == nil then log("Controller_Inventory - getQuestGiverSpawners() self is nil."); return false end
	if event == nil then log("Controller_Inventory - getQuestGiverSpawners() event is nil."); return false end
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Inventory - getQuestGiverSpawners() event.player is nil."); return false end

	local questGivers = {}

	for ID, uItem in pairs(s_uItemInstances) do
		-- Is this a spawner?	
		if uItem then
			local strUNID = tostring(uItem.UNID)
			local gameItem = s_uItemProperties[strUNID]
			if gameItem then
				local character = nil
				if gameItem.itemType == GameDefinitions.ITEM_TYPES.SPAWNER then
					-- Does this spawn a character
					local spawner = gameItem
					local spawnUNID = spawner.behaviorParams and spawner.behaviorParams.spawnUNID
					if spawnUNID then
						local spawnItem = s_uItemProperties[tostring(spawnUNID)]
						if spawnItem and spawnItem.itemType == GameDefinitions.ITEM_TYPES.CHARACTER then
							character = spawnItem
						end
					end
				elseif gameItem.itemType == GameDefinitions.ITEM_TYPES.CHARACTER then
					character = gameItem
				end
				
				if character and character.behavior == "quest_giver" and character.behaviorParams and character.behaviorParams.quests then
					for i, questUNID in pairs(character.behaviorParams.quests) do
						local strQuestUNID = tostring(questUNID)
						local questGiver = questGivers[strQuestUNID]
						if questGiver then
							table.insert(questGiver, tonumber(ID))
						else
							questGivers[strQuestUNID] = {tonumber(ID)}
						end
					end
				end
			end
		end
	end

	Events.sendClientEvent("FRAMEWORK_RETURN_QUEST_GIVER_SPAWNERS", {questLocations = questGivers}, event.player.ID)
end



-- Called when a player opens the toolbar
function onBattleObjectsRequested(self, event)
	LOG_FUNCTION_CALL("onBattleObjectsRequested")
	
	-- Error check inputs
	if self == nil then log("Controller_Player - onBattleObjectsRequested() self is nil."); return end
	if event == nil then log("Controller_Player - onBattleObjectsRequested() event is nil."); return end
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Player - onBattleObjectsRequested() event.player is nil."); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Player - onBattleObjectsRequested() event.player.ID is nil."); return end

	-- Send table of targetable items to the toolbar
	local objectQueue = {{}}
	local objectIndex = 1
	-- Insert objects
	for sID, uItem in pairs(s_uItemInstances) do
		if uItem and not uItem.creatorOnly then
			local itemInfo 		= getItemInfoFromPID(sID)
			itemInfo.owner		= uItem.owner
			-- Movers don't get registered with the client until we can solve how to move movers - TODO: This
			if itemInfo.behavior ~= "mover" then

				local newObject = {
					ID 	 	  			= sID,
					alive 	  			= uItem.alive,
					health				= uItem.health,
					maxHealth			= uItem.maxHealth,
					targetable			= tostring(uItem.targetable == "true" and uItem.combatEnabled ~= false),
					UNID				= itemInfo.UNID,
					owner				= itemInfo.owner,
					tooltip				= uItem.tooltip,
					interactionRange	= uItem.interactionRange,
					level				= uItem.level or 1,
					behavior			= itemInfo.behavior,
					itemType			= itemInfo.itemType,
					spawned				= (uItem.spawnerPID ~= nil) or false,
					isLoot				= uItem.type == "Item Container",
				}
				if itemInfo.itemType == "character" then
					newObject.master = itemInfo.owner
				end

				table.insert(objectQueue[objectIndex], newObject)
				if #objectQueue[objectIndex] >= GameDefinitions.MAX_EVENT_QUEUE_SIZE then
					objectIndex = objectIndex + 1
					objectQueue[objectIndex] = {}
				end
			end
		end
	end

	local playerQueue = {{}}
	local playerIndex = 1
	-- Insert players
	for sName, uPlayer in pairs(s_uPlayers) do
		if uPlayer and uPlayer.inZone then
			local newObject = {	
				ID 	 	  			= sName,
				alive 	  			= uPlayer.alive,
				health				= uPlayer.health,
				maxHealth			= uPlayer.maxHealth,
				targetable			= uPlayer.targetable,
				mode    	 		= uPlayer.mode,
				pvpEnabled 			= uPlayer.pvpEnabled,
				playerDestruction 	= uPlayer.playerDestruction,
				armorRating			= uPlayer.baseArmorRating,
			}
			table.insert(playerQueue[playerIndex], newObject)
			if #playerQueue[playerIndex] >= GameDefinitions.MAX_EVENT_QUEUE_SIZE then
				playerIndex = playerIndex + 1
				playerQueue[playerIndex] = {}
			end
		end
	end

	-- Send players up
	for i, players in ipairs(playerQueue) do
		Events.sendClientEvent("FRAMEWORK_REGISTER_BATTLE_OBJECT_LIST", {players = players}, playerID)
	end
	
	-- Send objects up
	for i, objects in ipairs(objectQueue) do
		Events.sendClientEvent("FRAMEWORK_REGISTER_BATTLE_OBJECT_LIST", {objects = objects}, playerID)
	end
end


-- Called when a player places an item in the zone
function onPlayerCreatePlaceSpawn(self, event)
	LOG_FUNCTION_CALL("onPlayerCreatePlaceSpawn")
	-- Error check inputs
	if self == nil then log("Controller_Inventory - onPlayerCreatePlaceSpawn() self is nil."); return false end
	if event == nil then log("Controller_Inventory - onPlayerCreatePlaceSpawn() event is nil."); return false end
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Inventory - onPlayerCreatePlaceSpawn() event.player is nil."); return false end
	if event.spawnUNID == nil then log("Controller_Inventory - onPlayerCreatePlaceSpawn() event.spawnUNID is nil."); return false end
	if event.x == nil then log("Controller_Inventory - onPlayerCreatePlaceSpawn() event.x is nil."); return false end
	if event.y == nil then log("Controller_Inventory - onPlayerCreatePlaceSpawn() event.y is nil."); return false end
	if event.z == nil then log("Controller_Inventory - onPlayerCreatePlaceSpawn() event.z is nil."); return false end
	if event.rx == nil then log("Controller_Inventory - onPlayerCreatePlaceSpawn() event.rx is nil."); return false end
	if event.ry == nil then log("Controller_Inventory - onPlayerCreatePlaceSpawn() event.ry is nil."); return false end
	if event.rz == nil then log("Controller_Inventory - onPlayerCreatePlaceSpawn() event.rz is nil."); return false end

	local position = {x = event.x, y = event.y, z = event.z, rx = event.rx, ry = event.ry, rz = event.rz}
	placeItemSpawn(self, event.player, event.spawnUNID, position)
end

-- Tell a player to pre-download a list of GLID icons for the game item icons
-- TODO: This shouldn't be necessary. Look into why KEP_LoadIconTextureByID fails on first call
--[[function downloadGameItemIcon(self, event)
	if self == nil then log("Controller_Inventory - downloadGameItemIcon() self is nil."); return end
	if event == nil then log("Controller_Inventory - downloadGameItemIcon() event is nil."); return end
	if event.player == nil then log("Controller_Inventory - downloadGameItemIcon() event.player is nil."); return end
	if type(event.player) ~= "userdata" then log("Controller_Inventory - downloadGameItemIcon() event.player isn't a sharedData entry."); return end
	local imageTable = {}
	-- If a specific UNID wasn't passed in, pass in every UNID in s_uItemProperties
	if not event.UNID then
		for UNID,item in pairs(s_uItemProperties) do
			-- Only download icons for valid Game Items
			if item.GLID then
				local image = item.GLID or GameDefinitions.DEFAULT_GI_ICON
				-- Do we have a valid thumbnail for this item?
				if item.thumbnail and (item.thumbnail ~= "") and string.find(item.thumbnail, "filestore") then
					image = item.thumbnail
				end
				table.insert(imageTable, {UNID = UNID,
										  image = image})
			end
		end
	-- Unique UNID was passed in
	elseif s_uItemProperties[event.UNID] then
		local item = s_uItemProperties[event.UNID]
		local image = item.GLID or GameDefinitions.DEFAULT_GI_ICON
		-- Do we have a valid thumbnail for this item?
		if item and item.thumbnail and (item.thumbnail ~= "") and string.find(item.thumbnail, "filestore") then
			image = item.thumbnail
		end
		
		table.insert(imageTable, {UNID = tostring(event.UNID),
								  image = image})
	end
	-- Everyone, download this new image
	Events.sendClientEvent("DOWNLOAD_GLID_ICON", {images = imageTable})
end]]



-- Register New System  Object
function onRegisterSettingObject(self, event)
	LOG_FUNCTION_CALL("onRegisterSettingObject")
	
	if event.objectType == "flag" then
		if event.info.flagType == "claimFlag" then
			local flag = event.info
			s_tLandClaimFlags[flag.PID] = flag
			flag.membersBag = Toolbox.convertContainerToSet(flag.members)
			if checkForClaimFlags() == 1 then
				Events.sendClientEvent("FIRST_CLAIM_FLAG_PLACED", {})
			end
		end
	elseif event.objectType == "media" then
		local media = event.info
		s_tMediaPlayers[media.PID] = media
	end
end

-- Unregister System Flag
function onUnregisterSettingObject(self, event)
	LOG_FUNCTION_CALL("onUnregisterSettingObject")

	if event.objectType == "flag" then
		s_tLandClaimFlags[event.PID] = nil
		if checkForClaimFlags() == 0 then
			Events.sendClientEvent("LAST_CLAIM_FLAG_DELETED", {})
		end
	elseif event.objectType == "media" then
		s_tMediaPlayers[event.PID] = nil
	end
end

-- TODO: This will need to be carried into its own Controller
function onWorldMapRequested(self, event)
	LOG_FUNCTION_CALL("onWorldMapRequested")
	local player = event.player
	if player == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onWorldMapRequested() event.player is nil."); return end
	local playerName = player.name
	if playerName == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onWorldMapRequested() Player with ID of "..tostring(player.ID).." has a NIL name!"); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Inventory - onWorldMapRequested() event.player.ID is nil."); return end
	
	-- Get map data
	local mapData = MapHelper.generateMapDataForPlayer(playerName, s_uItemInstances, s_uItemProperties)
	
	local iCHUNK_SIZE = 200
	local mapDataInChunks, iChunkCount = Toolbox.breakTableIntoChunks(mapData, iCHUNK_SIZE)
	
	-- Send off the map data in manageable chunks to whoever requested it
	for i, v in ipairs(mapDataInChunks) do
		local data = {
			mapData = v,
			batchIndex = i,
			totalBatchCount = iChunkCount
		}
		Events.sendClientEvent("FRAMEWORK_WORLD_MAP_RETURNED_FROM_SERVER", data, playerID)
	end
end



-- Generate a loot chest
function generateLootChest(self, event)
	LOG_FUNCTION_CALL("generateLootChest")
	
	local object = event.object

	local lootTable = {}
	local lootPossible = false
	if event.lootTableOverride then
		lootTable = event.lootTableOverride
	else
		local uObjectLoot = object.loot
		if not uObjectLoot then
			log("ERROR DETECTED - ED-5378: uObjectLoot is NIL!  object.type = "..tostring(object.type)..", object.UNID = "..tostring(object.UNID))
			return
		end
		-- Iterate over this object's loot table to construct entries for the loot container
		lootTable, lootPossible = self:generateLootTable(uObjectLoot, object.level)
		-- Reset the timer for loot spawners if necessary (only if loot was even possible in the first place)
		if lootPossible and object.type and object.spawnType then
			-- Only affects loot spawners that failed to spawn something
			if tostring(object.type) == "spawner" and tostring(object.spawnType) == "loot" then
				local tempSpawnerPID = object.PID
				local isEmpty = true
				for index, props in pairs(lootTable) do
					isEmpty = false
					break
				end
				-- If no loot table
				if tempSpawnerPID and lootTable[1] == nil then
					-- Tell spawner to reset itself
					Events.sendEvent("RESET_SPAWNER", {spawnerPID = tempSpawnerPID})
					return
				end
			end
		end
	end

	spawnGLID = object.spawnGLID or 4468596
	if event.spawnGLIDOverride then
		spawnGLID = event.spawnGLIDOverride
	end

	if object.position == nil then
		log("ERROR DETECTED - ED-6364 : object.position = nil, object.PID = "..tostring(object and object.PID)..", object.name = "..tostring(object and object.name))
		Debug.printTableError("ERROR DETECTED - ED-6364 : object ", object)
		return
	end
	local location = Toolbox.deepCopy(object.position)
	if event.positionOverride then
		local x = event.positionOverride.x
		local y = event.positionOverride.y
		local z = event.positionOverride.z
		location.x = x -- + (10*rotX) -- PLUS: Left, MINUS: Right
		location.y = y
		location.z = z -- + (10*rotZ) -- PLUS: Right, MINUS: Left
	end

	--Making sure for Autoloot, you only get max of one item slot 
	if object.autoLoot then
		local lootItem = lootTable[1]
		if lootItem.count == nil then
			log("ERROR DETECTED - ED-6930 : generateLootChest encountered a nil item count! UNID = "..tostring(lootItem and lootItem.UNID)..", stackSize = "..tostring(lootItem and lootItem.stackSize))
		end
		if lootItem then
			local lootUNID = lootItem.UNID
			local lootItem = s_uItemProperties[tostring(lootUNID)]
			local stackSize = lootItem.stackSize
			local lootItemType =  lootItem.itemType
			if	lootItemType == GameDefinitions.ITEM_TYPES.ARMOR or 
				lootItemType == GameDefinitions.ITEM_TYPES.WEAPON or 
				lootItemType == GameDefinitions.ITEM_TYPES.TOOL then
				
				lootItem.count = 1
			elseif stackSize then
				lootItem.count = math.min(stackSize, lootItem.count or 1)
			else
				lootItem.count = math.min(GameDefinitions.DEFAULT_STACK_SIZE, lootItem.count or 1)
			end
		else
			log("ERROR DETECTED - ED-6811 : object with UNID "..tostring(object and object.UNID).." and PID "..tostring(object and object.PID).." attempted autoloot with no loot!")
		end
	end

	-- If we have loot, make a chest
	for index, props in pairs(lootTable) do
		local eventData = {
			spawnerPID	= object.spawnerPID or 0,
			spawnType	= "loot",
			GLID		= spawnGLID,
			location	= location,
			spawnInput	= {
				items			= lootTable, 
				despawnTime		= event.despawnTime, 
				canDropGemBox	= event.canDropGemBox, 
				autoLoot		= object.autoLoot or false,
				noCollision		= object.noCollision or false,
				attackerID		= object.attackerID
			}
		}
		Events.sendEvent("OBJECT_SPAWN", eventData)
		break
	end
end


function onLootRequested(self, event)
	LOG_FUNCTION_CALL("onLootRequested")
	
	if self == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onLootRequested() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onLootRequested() event is nil."); return end
	if event.returnPID == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onLootRequested() event.returnPID is nil."); return end
	if event.object == nil then Console.Write(Console.DEBUG, "Controller_Inventory - onLootRequested() event.object is nil."); return end

	local object = event.object
	
	if object then
		local uObjectLoot = object.loot
		local lootTable = {}
		if uObjectLoot then
			lootTable = self:generateLootTable(uObjectLoot, object.level)
		else
			log("ERROR DETECTED - ED-7120 : onLootRequested encountered a nil object loot table!")
			Debug.printTableError("ERROR DETECTED - ED-7120 : object", object)
		end
		
		local lootTableWithProperties = {}
		-- Get the properties for each loot item
		for i, v in pairs(lootTable) do
			local strUNID = tostring(v.UNID)
			local itemProperties = s_uItemProperties[strUNID]
			if itemProperties then
				v.properties = itemProperties
				lootTableWithProperties[i] = v
			end
		end
	
		Events.sendEvent("RETURN_LOOT", {lootTable = lootTableWithProperties, returnPID = event.returnPID})
	end
end

-- Iterates over a loot table and generates a 
function generateLootTable(self, objectLoot, level)
	LOG_FUNCTION_CALL("generateLootTable")
	
	local lootTable = {}
	local lootPossible = false

	if not objectLoot then
		log("FATAL ERROR DETECTED - ED-7120 : generateLootTable encountered a nil object loot table!")
		Debug.printStack()
		return lootTable, lootPossible
	end
	-- local highestRoll = 0
	-- local highestRollItem = -1 -- Initialize to -1 to check the possibility that NOTHING can drop

	-- Does this object have limitItems (Harvestables for instance)
	local limitItems = objectLoot and objectLoot.limitItems
	if limitItems then
		local UNID = limitItems["UNID"]
		if UNID then
			local gameItem = s_uItemProperties[tostring(UNID)]
			if gameItem then 
				local limit = limitItems["limit"]
				if limit then
					-- First index is UNID, second is limit
					local stackSize = gameItem.stackSize or limit
					local minLimit = math.min(stackSize, limit)
					if minLimit < 1 then
						log("ERROR DETECTED - ED-8269 : generateLootTable encountered an invalid min limit! gameItem.stackSize = "..tostring(gameItem.stackSize)..", limit = "..tostring(limit))
						Debug.printTableError("ERROR DETECTED - ED-8269 : gameItem", gameItem)
						minLimit = 1
					end
					local loot = {	
						UNID	= tonumber(UNID), 
						count	= math.random(1, minLimit)
					}
					table.insert(lootTable, loot)
					lootPossible = true
					return lootTable, lootPossible
				end
			end
		end
	end

	local rollPass = {weapon = {}, armor = {}, tool = {}, consumable = {}, ammo = {}, generic = {}, blueprint = {}, seed = {}, singles = {}}
	
	-- Collect all Game Items according to the types exposed
	for UNID, item in pairs(s_uItemProperties) do
		if item then
			local itemType = item.itemType
			-- Don't handle items that aren't valid loot types
			if (itemType == GameDefinitions.ITEM_TYPES.ARMOR) or
			   (itemType == GameDefinitions.ITEM_TYPES.TOOL) or 
			   (itemType == GameDefinitions.ITEM_TYPES.WEAPON) or 
			   (itemType == GameDefinitions.ITEM_TYPES.CONSUMABLE) or 
			   (itemType == GameDefinitions.ITEM_TYPES.AMMO) or 
			   (itemType == GameDefinitions.ITEM_TYPES.GENERIC) or
			   (itemType == GameDefinitions.ITEM_TYPES.BLUEPRINT) or
			   (itemType == GameDefinitions.ITEM_TYPES.HARVESTABLE and item.behavior == "seed") then

				-- Weapon, Armor, and Tool are pulled by level
				if item.level == level then
					-- Armor
					if (itemType == GameDefinitions.ITEM_TYPES.ARMOR) and objectLoot.armor then
						local passedDrop, rollValue, rollChance = rarityRoll(UNID) -- Roll value not being used, but can be used to track the highest roll
						if rollChance > 0 then
							lootPossible = true
							-- if rollValue > highestRoll then
							-- 	highestRoll = rollValue
							-- 	highestRollItem = UNID
							-- end
						end
						if passedDrop then table.insert(rollPass.armor, UNID) end

					-- Tool
					elseif (itemType == GameDefinitions.ITEM_TYPES.TOOL) and objectLoot.tool then
						local passedDrop, rollValue, rollChance = rarityRoll(UNID)
						if rollChance > 0 then
							lootPossible = true
							-- if rollValue > highestRoll then
							-- 	highestRoll = rollValue
							-- 	highestRollItem = UNID
							-- end
						end
						if passedDrop then table.insert(rollPass.tool, UNID) end
					
					-- Weapon
					elseif (itemType == GameDefinitions.ITEM_TYPES.WEAPON) and objectLoot.weapon then
						local passedDrop, rollValue, rollChance = rarityRoll(UNID)
						if rollChance > 0 then
							lootPossible = true
							-- if rollValue > highestRoll then
							-- 	highestRoll = rollValue
							-- 	highestRollItem = UNID
							-- end
						end
						if passedDrop then table.insert(rollPass.weapon, UNID) end
						
					--Blueprint
					elseif (itemType == GameDefinitions.ITEM_TYPES.BLUEPRINT) and objectLoot.blueprint then
						local passedDrop, rollValue, rollChance = rarityRoll(UNID)
						if rollChance > 0 then
							lootPossible = true
							-- if rollValue > highestRoll then
							-- 	highestRoll = rollValue
							-- 	highestRollItem = UNID
							-- end
						end
						if passedDrop then
							table.insert(rollPass.blueprint, UNID)
						end
					end
				end
				-- Consumable
				if (itemType == GameDefinitions.ITEM_TYPES.CONSUMABLE) and objectLoot.consumable then
					local passedDrop, rollValue, rollChance = rarityRoll(UNID)
					if rollChance > 0 then
						lootPossible = true
						-- if rollValue > highestRoll then
						-- 	highestRoll = rollValue
						-- 	highestRollItem = UNID
						-- end
					end
					if passedDrop then table.insert(rollPass.consumable, UNID) end
				
				-- Ammo
				elseif (itemType == GameDefinitions.ITEM_TYPES.AMMO) and objectLoot.ammo then
					local passedDrop, rollValue, rollChance = rarityRoll(UNID)
					if rollChance > 0 then
						lootPossible = true
							--if rollValue > highestRoll then
							--	highestRoll = rollValue
							--	highestRollItem = UNID
							--end
					end
					if passedDrop then table.insert(rollPass.ammo, UNID) end
				-- Seed
				elseif (itemType == GameDefinitions.ITEM_TYPES.HARVESTABLE) and item.behavior == "seed" and objectLoot.seed then
					local passedDrop, rollValue, rollChance = rarityRoll(UNID)
					if rollChance > 0 then
						lootPossible = true
							--if rollValue > highestRoll then
							--	highestRoll = rollValue
							--	highestRollItem = UNID
							--end
					end
					if passedDrop then table.insert(rollPass.seed, UNID) end
				-- Generic
				elseif (itemType == GameDefinitions.ITEM_TYPES.GENERIC) and objectLoot.generic then
					local passedDrop, rollValue, rollChance = rarityRoll(UNID)
					if rollChance > 0 then
						lootPossible = true
						-- if rollValue > highestRoll then
						-- 	highestRoll = rollValue
						-- 	highestRollItem = UNID
						-- end
					end
					if passedDrop then table.insert(rollPass.generic, UNID) end
				end
			end
		end
	end
	-- Roll on one-off items
	if objectLoot.singles then
		for index, itemInfo in pairs(objectLoot.singles) do
			if Toolbox.isContainer(itemInfo) then
				if tonumber(itemInfo.UNID) == nil then
					log("ERROR - **** generateLootTable **** - itemInfo.UNID was NIL!")
				end
				if tonumber(itemInfo.drop) == nil then
					log("ERROR - **** generateLootTable **** - itemInfo.drop was NIL!")
				end			
				if (tonumber(itemInfo.UNID) or 0) > 0 and (tonumber(itemInfo.drop) or 0) > 0 then
					--log("--- itemInfo.drop: ".. tostring(itemInfo.drop))

					local passedDrop, rollValue, rollChance  = rarityRoll(itemInfo.UNID, tonumber(itemInfo.drop))
					if rollChance > 0 then
						lootPossible = true
						-- if rollValue > highestRoll then
						-- 	highestRoll = rollValue
						-- 	highestRollItem = itemInfo.UNID
						-- end
					end

					if passedDrop then
						local gameItem = s_uItemProperties[tostring(itemInfo.UNID)]
						if gameItem.itemType == GameDefinitions.ITEM_TYPES.AMMO then
							local stackSize = gameItem.stackSize or 1
							table.insert(lootTable, {UNID = tonumber(itemInfo.UNID), count = math.min(stackSize, 10)})
						else
							table.insert(lootTable, {UNID = tonumber(itemInfo.UNID), count = 1})
						end
					end
				end
			else
				log("ERROR DETECTED - ED-7039 : itemInfo was not a table! itemInfo = "..tostring(itemInfo)..", type = "..type(itemInfo))
				Debug.printTableError("ERROR DETECTED - ED-7039 : objectLoot", objectLoot)
			end
		end
	end
	
	-- Populate the lootTable off of the successful rolls
	
	-- Randomly extract one piece of the passed armor rolls
	local ARMOR_COUNT = #rollPass.armor
	if (ARMOR_COUNT >= 1) then
		table.insert(lootTable, {UNID = tonumber(rollPass.armor[math.random(ARMOR_COUNT)]), count = 1})
	end
	-- Randomly extract one piece of the passed tool rolls
	local TOOL_COUNT = #rollPass.tool
	if (TOOL_COUNT >= 1) then
		table.insert(lootTable, {UNID = tonumber(rollPass.tool[math.random(TOOL_COUNT)]), count = 1})
	end
	-- Randomly extract one piece of the passed weapon rolls
	local WEAPON_COUNT = #rollPass.weapon
	if (WEAPON_COUNT >= 1) then
		table.insert(lootTable, {UNID = tonumber(rollPass.weapon[math.random(WEAPON_COUNT)]), count = 1})
	end
	-- Randomly extract two piece of the passed consumable rolls
	local CONSUMABLE_COUNT = #rollPass.consumable
	if (CONSUMABLE_COUNT >= 1) then
		local randomIndex = math.random(CONSUMABLE_COUNT)
		table.insert(lootTable, {UNID = tonumber(rollPass.consumable[randomIndex]), count = 1})
		table.remove(rollPass.consumable, randomIndex)
		-- Randomly insert second value if we have one
		CONSUMABLE_COUNT = #rollPass.consumable
		if (CONSUMABLE_COUNT >= 1) then
			table.insert(lootTable, {UNID = tonumber(rollPass.consumable[math.random(CONSUMABLE_COUNT)]), count = 1})
		end
	end
	-- Randomly extract one piece of the passed ammo rolls
	local AMMO_COUNT = #rollPass.ammo
	if (AMMO_COUNT >= 1) then
		-- CJW: THIS NEEDS MORE ATTENTION!!
		local randomAmmo = rollPass.ammo[math.random(AMMO_COUNT)]
		local stackSize = s_uItemProperties[randomAmmo].stackSize or 10
		table.insert(lootTable, {UNID = tonumber(randomAmmo), count = math.min(stackSize, 10)})
	end
	-- Randomly extract one piece of the passed generic rolls
	local GENERIC_COUNT = #rollPass.generic
	if (GENERIC_COUNT >= 1) then
		table.insert(lootTable, {UNID = tonumber(rollPass.generic[math.random(GENERIC_COUNT)]), count = 1})
	end
	
	local BLUEPRINT_COUNT = #rollPass.blueprint
	if (BLUEPRINT_COUNT >= 1) then
		table.insert(lootTable, {UNID = tonumber(rollPass.blueprint[math.random(BLUEPRINT_COUNT)]), count = 1})
	end

	-- Randomly extract one piece of the passed seed rolls
	local SEED_COUNT = #rollPass.seed
	if (SEED_COUNT >= 1) then
		table.insert(lootTable, {UNID = tonumber(rollPass.seed[math.random(SEED_COUNT)]), count = 1})
	end
		
	-- If empty lootTable, snag highest rolled piece of total set (at least one piece of something included)
	-- if lootTable[1] == nil and highestRollItem ~= -1 then
	-- 	table.insert(lootTable, {UNID = tonumber(highestRollItem), count = 1})
	-- end

	return lootTable, lootPossible
end


-- Generate random loot for random loot vendor
function generateRandomLoot(self, event)
	LOG_FUNCTION_CALL("generateRandomLoot")
	
	local loot = event.loot
	
	-- Iterate over this loot table to construct the possible outputs for the random loot vendor
	local lootItem = {}
	lootItem = generateLootItem(loot, event.level)

	-- If we have loot, make a chest
	if lootItem then
		Events.sendEvent("RETURN_RANDOM_LOOT", {lootTable = lootItem, playerID = event.playerID})
	end
end

-- Get a quest giver's details
function passAvailableQuests(self, event)
	LOG_FUNCTION_CALL("passAvailableQuests")
	if self == nil then log("Controller_Inventory - passAvailableQuests() self is nil."); return end
	if event == nil then log("Controller_Inventory - passAvailableQuests() event is nil."); return end
	if event.questGiver == nil then log("Controller_Inventory - passAvailableQuests() event.questGiver is nil."); return end
	if event.questGiver.quests == nil then log("Controller_Inventory - passAvailableQuests() event.questGiver.quests is nil."); return end
	if event.player == nil then log("Controller_Inventory - passAvailableQuests() event.player is nil."); return end
	-- if event.questIndex == nil then log("Controller_Inventory - passSpecificQuest() event.questIndex is nil."); return end
	
	-- Get details for each quest the quest giver has
	local quests = {}
	local rewardItems = {}
	for index, questUNID in pairs(event.questGiver.quests) do
		-- Ensure this quest is still valid
		local gameItem = s_uItemProperties[tostring(questUNID)]
		if gameItem then
			-- Construct the quest in a format that InventoryHandler likes
			local quest = {
				properties	= Toolbox.deepCopy(gameItem),
				UNID		= questUNID
			}
			
			-- Append quest requiredItem metadata into the requiredItem table
			local requiredItem = s_uItemProperties[tostring(quest.properties.requiredItem.requiredUNID)]
			if requiredItem then
				quest.properties.requiredItem.GLID = requiredItem.GLID
				quest.properties.requiredItem.name = requiredItem.name
			end
			-- Append quest rewardItem metadata into the rewardItem table
			local rewardItem = {}
			local rewardUNID = quest.properties.rewardItem.rewardUNID
			local rewardPropeties = s_uItemProperties[tostring(rewardUNID)]
			
			if tonumber(rewardUNID) > 0 then
				rewardItem = {
					UNID		= rewardUNID,
					count		= quest.properties.rewardItem.rewardCount
				}
				-- Add instanceData?
				local itemType = rewardPropeties.itemType
				if	itemType == GameDefinitions.ITEM_TYPES.WEAPON or 
					itemType == GameDefinitions.ITEM_TYPES.ARMOR or
					itemType == GameDefinitions.ITEM_TYPES.TOOL then
					
					local instanceDurability = GameDefinitions.DEFAULT_MAX_DURABILITY
					local durabilityType = rewardPropeties.durabilityType or "repairable"
					if durabilityType == "limited" then
						instanceDurability = GameDefinitions.DURABILITY_BY_LEVEL[rewardPropeties.itemLevel]
					end
					rewardItem.instanceData = {
						levelMultiplier	= GameDefinitions.getItemLevelMultiplier(),
						durability		= instanceDurability
					}
				end
			end
			if rewardPropeties then
				quest.properties.rewardItem.GLID = rewardPropeties.GLID
				quest.properties.rewardItem.name = rewardPropeties.name
			end
			
			table.insert(quests, quest)
			table.insert(rewardItems, rewardItem)
		end
	end
	
	-- Open the Quest Giver menu for this player
	kgp.playerLoadMenu(event.player.ID, "Framework_QuestGiver.xml")
	
	local eventData = {	
		name 	 		= event.questGiver.name,
		PID		 		= tonumber(event.questGiver.PID),
		GLID	 		= s_uItemProperties[tostring(s_uItemInstances[event.questGiver.PID].UNID)].GLID,
		quests 	 		= quests,
		rewardItems		= rewardItems,
		dialog 	 		= event.questGiver.dialog,
		greetingImage	= event.questGiver.greetingImage,
		isRider			= event.isRider,
		riderPID		= event.riderPID
	}

	-- quest index override
	if event.questIndex then
		eventData.questIndex = event.questIndex
	end

	Events.sendClientEvent("FRAMEWORK_PASS_QUEST_GIVER_DETAILS", eventData, event.player.ID)
end

-- Get a quest giver's details
-- function passSpecificQuest(self, event)
-- 	Events.sendClientEvent("FRAMEWORK_PASS_SPECIFIC_QUEST_DETAILS", eventData, event.player.ID)
-- end

function placePathWaypoint(self, event)
	LOG_FUNCTION_CALL("placePathWaypoint")
	-- Error check inputs
	if self == nil then log("Controller_Inventory - placePathWaypoint() self is nil."); return end
	if event == nil then log("Controller_Inventory - placePathWaypoint() event is nil."); return end
	-- Was a valid player passed in?
	local player
	if event.playerName == nil then
		player = event.player
	else
		player = s_uPlayers[event.playerName]
		event.player = player
	end
	local playerID = player and player.ID
	if player == nil then log("Controller_Inventory - placePathWaypoint() event.player is nil."); return end
	if playerID == nil then log("Controller_Inventory - placePathWaypoint() event.player.ID is nil."); return end
	if event.GLID == nil then log("Controller_Inventory - placePathWaypoint() event.GLID is nil."); return end
	if event.UNID == nil then log("Controller_Inventory - placePathWaypoint() event.UNID is nil."); return end
	if event.x == nil then log("Controller_Inventory - placePathWaypoint() event.x is nil."); return end
	if event.y == nil then log("Controller_Inventory - placePathWaypoint() event.y is nil."); return end
	if event.z == nil then log("Controller_Inventory - placePathWaypoint() event.z is nil."); return end
	if event.rx == nil then log("Controller_Inventory - placePathWaypoint() event.rx is nil."); return end
	if event.ry == nil then log("Controller_Inventory - placePathWaypoint() event.ry is nil."); return end
	if event.rz == nil then log("Controller_Inventory - placePathWaypoint() event.rz is nil."); return end
	if event.additionalPathInfo == nil then log("Controller_Inventory - placePathWaypoint() event.additionalPathInfo is nil."); return end
	if event.additionalPathInfo.prevPathWaypointPID ~= nil then
		prevPathWaypoint = s_uItemInstances[event.additionalPathInfo.prevPathWaypointPID]
		if prevPathWaypoint == nil then log("Controller_Inventory - placePathWaypoint() prevPathWaypoint not available."); return end
		parentPath = s_uItemInstances[prevPathWaypoint.parentPathPID]
	elseif event.additionalPathInfo.parentPathPID ~= nil then
		parentPath = s_uItemInstances[event.additionalPathInfo.parentPathPID]
	else
		log("Controller_Inventory - placePathWaypoint() prevPathWaypoint and parentPath, both were not found.")
		return
	end
	if parentPath == nil then log("Controller_Inventory - placePathWaypoint() parent Path not available."); return end
	if parentPath.noOfPathWaypoints >= 50 then
		log("Controller_Inventory - placePathWaypoint() waypoint limit of 50 reached.")
		kgp.playerShowStatusInfo(playerID, "Maximum Waypoint limit of 50 has been reached!", 3, 255, 0, 0)
		return
	end
	
	if event.rx == 0 and event.rz == 0 then
		log("ERROR DETECTED - ED-6840 : onPlayerBuildPlaceItem: event.rx == 0 and event.rz == 0")
		Debug.printTableError("ERROR DETECTED - ED-6840 : event", event)
		Debug.printStack()
		return
	end
	
	-- Instantiate this object if the Framework is online.
	if s_bIsControllerReady then
		
		local permission = checkObjectPlaceability(event)
		
		if( permission == GameDefinitions.PLACEABILITY.ALLOWED ) then
			
			-- Place the item in the game
			local PID = placeGameItem(event)

			-- Notify clients
			Events.sendClientEvent("PLACE_PATH_WAYPOINT_CONFIRMATION", {success = true, PID = PID}, playerID)
			
			-- Collect me some metrics
			collectMetricsForPlacedItem(event.UNID, player.name, player.mode)
			
			-- Return results
			return PID
		end
	end
end

function placePathRider(self, event)
	LOG_FUNCTION_CALL("placePathRider")
	-- Error check inputs
	if self == nil then log("Controller_Inventory - placePathRider() self is nil."); return end
	if event == nil then log("Controller_Inventory - placePathRider() event is nil."); return end
	-- Was a valid player passed in?
	local player = s_uPlayers[event.playerName]
	event.player = player
	local playerID = player and player.ID
	if player == nil then log("Controller_Inventory - placePathRider() event.player is nil."); return end
	if playerID == nil then log("Controller_Inventory - placePathRider() event.player.ID is nil."); return end
	if event.UNID == nil then log("Controller_Inventory - placePathRider() event.UNID is nil."); return end
	local spawnLoc = event.location
	if spawnLoc == nil then log("Controller_Inventory - placePathRider() event.location is nil."); return end
	if spawnLoc.x == nil then log("Controller_Inventory - placePathRider() event.x is nil."); return end
	if spawnLoc.y == nil then log("Controller_Inventory - placePathRider() event.y is nil."); return end
	if spawnLoc.z == nil then log("Controller_Inventory - placePathRider() event.z is nil."); return end
	if spawnLoc.rx == nil then log("Controller_Inventory - placePathRider() event.rx is nil."); return end
	if spawnLoc.ry == nil then log("Controller_Inventory - placePathRider() event.ry is nil."); return end
	if spawnLoc.rz == nil then log("Controller_Inventory - placePathRider() event.rz is nil."); return end
	if event.additionalPathInfo == nil then log("Controller_Inventory - placePathRider() event.additionalPathInfo is nil."); return end
	
	if spawnLoc.rx == 0 and spawnLoc.rz == 0 then
		log("ERROR DETECTED - ED-6840 : onPlayerBuildPlaceItem: event.rx == 0 and event.rz == 0")
		Debug.printTableError("ERROR DETECTED - ED-6840 : event", event)
		Debug.printStack()
		return
	end
	
	local input = {}
	input.spawnerPID = event.spawnerPID
	input.spawnType = s_uItemProperties[event.UNID].behavior .. "_rider"
	if input.spawnType == nil then log("Controller_Inventory - placePathRider() event.itemType is nil."); return end
	input.GLID = s_uItemProperties[event.UNID].GLID
	if input.GLID == nil then log("Controller_Inventory - placePathRider() event.GLID is nil."); return end
	input.location = spawnLoc
	local spawnInput = {}
	spawnInput.UNID = event.UNID
	spawnInput.additionalPathInfo = event.additionalPathInfo
	spawnInput.behavior = input.spawnType
	if spawnInput.behavior == nil then log("Controller_Inventory - placePathRider() event.behavior is nil."); return end
	for i, v in pairs(s_uItemProperties[event.UNID]) do
		if i ~= "itemType" and i ~= "GLID" and i ~= "behavior" and i ~= "behaviorParams" then
			spawnInput[i] = v
		end
	end
	if s_uItemProperties[event.UNID].behaviorParams then
		for i, v in pairs(s_uItemProperties[event.UNID].behaviorParams) do
			if i ~= "itemType" and i ~= "GLID" and i ~= "behavior" and i ~= "behaviorParams" then
				spawnInput[i] = v
			end
		end
	end
	input.spawnInput = spawnInput
	Events.sendEvent("OBJECT_SPAWN", {spawnerPID = input.spawnerPID, spawnType = input.spawnType, GLID = input.GLID, location = input.location, spawnInput = input.spawnInput})
end

function updatePathWaypointLinkagesOnAdd(self, event)
	currentPathWaypoint = s_uItemInstances[event.crntPathWaypointPID]
	prevPathWaypoint = s_uItemInstances[event.prevPathWaypointPID]
	if ((currentPathWaypoint == nil) or (prevPathWaypoint == nil)) then
		log("ERROR DETECTED - updatePathWaypointLinkagesOnAdd - current or prev path waypoint is nil and hence not available in inventory.") 
		return
	end
	nextPID = prevPathWaypoint.nextPathWaypointPID
	currentPathWaypoint.parentPathPID = prevPathWaypoint.parentPathPID
	currentPathWaypoint.prevPathWaypointPID = prevPathWaypoint.PID
	currentPathWaypoint.nextPathWaypointPID = nextPID
	prevPathWaypoint.nextPathWaypointPID = currentPathWaypoint.PID
	if nextPID then
		nextPathWaypoint = s_uItemInstances[prevPathWaypoint.nextPathWaypointPID]
		if nextPathWaypoint == nil then
			log("ERROR DETECTED - updatePathWaypointLinkagesOnAdd - nextPID is there but next path waypoint is nil.") 
			return
		end
		nextPathWaypoint.prevPathWaypointPID = currentPathWaypoint.PID
	end
	Events.sendEvent("SAVE_PATHWAYPOINT_DATA", {PID1 = currentPathWaypoint.prevPathWaypointPID, PID2 = currentPathWaypoint.PID, PID3 = currentPathWaypoint.nextPathWaypointPID})
	Events.sendEvent("ADD_PATH_WAYPOINT", {newPathWaypointPID = event.crntPathWaypointPID, parentPathPID = prevPathWaypoint.parentPathPID, prevPathWaypointPID = prevPathWaypoint.PID})
end

function updatePathWaypointLinkagesOnDel(self, event)
	prevPID = event.prevPathWaypointPID
	nextPID = event.nextPathWaypointPID
	parentPath = s_uItemInstances[event.parentPathPID]
	if nextPathWaypoint == nil then
		log("ERROR DETECTED - updatePathWaypointLinkagesOnDel - parent path is nil.") 
		return 
	end
	if prevPID then
		prevPathWaypoint = s_uItemInstances[prevPID]
		prevPathWaypoint.nextPathWaypointPID = nextPID
	end
	if nextPID then
		nextPathWaypoint = s_uItemInstances[nextPID]
		nextPathWaypoint.prevPathWaypointPID = prevPID
	end
	Events.sendEvent("DEL_PATH_WAYPOINT", {delPathWaypointPID = event.delPathWaypointPID, parentPathPID = parentPath.PID})
	Events.sendEvent("SAVE_PATHWAYPOINT_DATA", {PID1 = prevPID or nil, PID2 = nextPID or nil})
end


----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
-- Local function definitions
----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------


-- Load GameItems and NextUNID field from DB
loadGameItemProperties = function()
	LOG_FUNCTION_CALL("loadGameItemProperties")
	
	s_uItemProperties = SharedData.new("gameItemProperties", false)
	
	-- Download all game item information from the DB
	local gameData = GII.gameItemLoadAllByGame()
	if gameData then
		for UNID, gameItem in pairs(gameData) do
			local itemInfo = {}
			for key, value in pairs(gameItem) do
				if isNestedKey(key) then
					value = string.gsub(value, "\'", "\"")
					value = Events.decode(value)
				end
				itemInfo[key] = tonumber(value) or value
			end

			-- 02.04.2016 - TEMP HACK TO ENSURE ALL WORLD OBJECTS HAVE BEHAVIOR
			-- THIS SHOULD GET REMOVED ONCE WE ARE SURE ALL WORLD OBJECTS ARE UPDATED ON THE DB.
			if( itemInfo["behavior"] == nil ) then
				itemInfo["behavior"] = "placeable_loot"
			end

			itemInfo = validateFlagArea(itemInfo, UNID)

			if itemInfo.name then
				itemInfo.name = tostring(itemInfo.name)
			end
			
			local strUNID = tostring(UNID)
			-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
			--Toolbox.setUserdataAtIndex(s_uItemProperties, strUNID, itemInfo)
			s_uItemProperties[strUNID] = itemInfo
			if itemInfo["itemType"] then
				local itemType = tostring(itemInfo["itemType"])
				if not s_tGameItemTypes[itemType] then
					s_tGameItemTypes[itemType] = {}
				end
				table.insert(s_tGameItemTypes[itemType], strUNID)
			end
		end
	end
	
	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	s_iNextUNID = tonumber(SaveLoad.loadGameData("NextUNID", parentID, parentType))

	-- Ensure quest prereqs are still intact
	validateQuestPrereqs()
	
	-- Remove this item if it's part of a quest prereq or reward
	validateQuestItems()
	
	validateQuestGiverQuests()

	validateRecipes()

	validateVendors()
end


-- TODO: It might be better to carry this into Controller_Game
-- Compiles all game items into SharedData
validateFlagArea = function (item, UNID)
	if( item["behavior"] == "flag" or item["behavior"] == "claim") then
		if item.behaviorParams == nil and item.radius then
			item.behaviorParams = {}
			item.behaviorParams.height = item.height or item.radius * SQUARE_OF_TWO
			item.behaviorParams.width = item.radius * SQUARE_OF_TWO
			item.behaviorParams.depth = item.radius * SQUARE_OF_TWO
		end
	end
	return item
end

detectRequiredSystems = function()
	LOG_FUNCTION_CALL("detectRequiredSystems")
	
	local enableHealth, enableEnergy, enableArmor = false, false, false
	for UNID, item in pairs(s_uItemProperties) do
		if item then
			-- Check for health enable
			if not enableHealth then
				if item.itemType == GameDefinitions.ITEM_TYPES.CONSUMABLE then
					if item.consumeType == "health" then enableHealth = true end
				elseif (item.itemType == GameDefinitions.ITEM_TYPES.WEAPON) or (item.itemType == GameDefinitions.ITEM_TYPES.ARMOR) then enableHealth = true end
			end
			-- Check for energy enable
			if not enableEnergy then
				if item.itemType == GameDefinitions.ITEM_TYPES.CONSUMABLE then
					if item.consumeType == "energy" then enableEnergy = true end
				end
			end
			-- Check for armor enable
			if not enableArmor then
				if item.itemType == GameDefinitions.ITEM_TYPES.ARMOR then enableArmor = true end
			end
		end
	end
	
	-- Register for health
	if enableHealth then
		Game.enableHealthBar({controller = "Inventory"})
	end
	-- Register for energy
	if enableEnergy then
		Game.enableEnergyBar({controller = "Inventory"})
	end
	-- Register for armor
	if enableArmor then
		Game.enableArmorBar({controller = "Inventory"})
	end
end

-- Loads associated object tables from the database
loadObjectAssociations = function()
	LOG_FUNCTION_CALL("loadObjectAssociations")
	
	s_tObjectAssociations = {}
	s_iObjectAssociationCount = SaveLoad.loadGameData("numObjectAssociations") or 0
	
	for i = 1, s_iObjectAssociationCount do
		local objectAssociations = SaveLoad.loadGameData("objectAssociations_"..tostring(i))
		if objectAssociations then
			objectAssociations = Events.decode(objectAssociations)
			local associationValid = true
			for _, PID in pairs(objectAssociations) do
				if s_tActiveDOs[PID] == nil then
					associationValid = false
					break
				end
			end
			
			if associationValid then
				table.insert(s_tObjectAssociations, objectAssociations)
			else
				SaveLoad.deleteGameData("objectAssociations_"..tostring(i))
			end
		end
	end
	
	Events.sendClientEvent("FRAMEWORK_CLIENT_UPDATE_OBJECT_ASSOCIATIONS", {objectAssociations = s_tObjectAssociations})
end

-- Port WOK Game Item fields to Framework Game Item-valid fields
portWOKFieldsToGameItem = function(item)
	LOG_FUNCTION_CALL("portWOKFieldsToGameItem")
	
	local WOK_TAG_CONVERSIONS = {
		["glid"]			= "GLID", 
		["item_type"]		= "itemType", 
		["game_item_glid"]	= "GIGLID"
	}

	if Toolbox.isContainer(item) then
		for i, v in pairs(item) do
			-- Port WOK fields over to Game Item fields
			local conversion = WOK_TAG_CONVERSIONS[i]
			if conversion then
				item[conversion] = v
				item[i] = nil
			end
		end
	else
		log("Controller_Inventory - portWOKFieldsToGameItem() Passed item is not a table - item["..tostring(item).."] type["..tostring(item).."]")
	end
	return item
end

-- Gets DOs that map to Game Items in the zone and who owns them
compileActiveDOs = function()
	LOG_FUNCTION_CALL("compileActiveDOs")
	
	local owned = kgp.gameItemGetOwned()
	if owned then
		for PID, itemInfo in pairs(owned) do
			s_tActiveDOs[PID] = {	
				UNID	= itemInfo.UNID,
				owner	= itemInfo.ownerName,
				spawned = false
			}
		end
	end
	
	-- TODO: Verify associated UNIDs are still active	
end

saveGameItem = function(UNID, gameItem, internal)
	LOG_FUNCTION_CALL("saveGameItem")
	
	gameItem = Toolbox.deepCopy(gameItem, true)
	
	local itemToSave = {}
	for key, value in pairs(gameItem) do
		if Toolbox.isContainer(value) then
			itemToSave[key] = Events.encode(value)
		else
			itemToSave[key] = value
		end
	end

	
	itemToSave.GIID = UNID
	GII.gameItemUpdate(UNID, itemToSave)
	
	local gameItemType = gameItem.itemType

	Events.sendClientEvent("FRAMEWORK_GAME_ITEM_UPDATED", {itemUNID = UNID, itemInfo = gameItem})

	if gameItemType == GameDefinitions.ITEM_TYPES.RECIPE or gameItemType == GameDefinitions.ITEM_TYPES.BLUEPRINT then
		--Compile recipe info up
		local recipe = packageRecipeToClient(UNID, gameItem)
		recipe = Toolbox.deepCopy(recipe, true)
		Events.sendClientEvent("FRAMEWORK_RECIPE_UPDATED", {itemUNID = UNID, itemInfo = recipe})
	end
end

gotoGameItemInSystem = function(playerName, UNID)
	LOG_FUNCTION_CALL("gotoGameItemInSystem")
	
	local item = s_uItemProperties[UNID]
	if item == nil then log("gotoGameItemInSystem - game item with UNID "..tostring(UNID).." was not found.") return end
	
	--kgp.playerLoadMenu(s_uPlayers[playerName].ID, "UnifiedNavigation.xml")
	Events.sendClientEvent("FRAMEWORK_GOTO_GAME_ITEM", {itemUNID = UNID, itemType = item.itemType, behavior = item.behavior}, s_uPlayers[playerName].ID)
end


validatePlayerInventories = function()
	LOG_FUNCTION_CALL("validatePlayerInventories")
	
	Events.sendEvent("REQUEST_PLAYER_INVENTORIES_VALIDATION")
end

validateItemContainer = function(UNID)
	LOG_FUNCTION_CALL("validateItemContainer")
	
	local gameItem = s_uItemProperties[tostring(UNID)]

	Events.sendEvent("ITEM_PROPERTIES_UPDATE")
	
	Events.sendClientEvent("UPDATE_ITEM_PROPERTIES", {UNID = UNID, properties = gameItem and Toolbox.deepCopy(gameItem, true)})
end

validateRecipes = function()
	LOG_FUNCTION_CALL("validateRecipes")
	
	for UNID, item in pairs(s_uItemProperties) do
		if item and item.itemType == GameDefinitions.ITEM_TYPES.RECIPE or item.itemType == GameDefinitions.ITEM_TYPES.BLUEPRINT then
			local validated = checkRecipe(item)

			if not validated then
				item.invalid = true				
				saveGameItem(tonumber(UNID), item)
			end
		end
	end
end

validateVendors = function()
	LOG_FUNCTION_CALL("validateVendors")
	
	for UNID, item in pairs(s_uItemProperties) do
		if item and item.itemType == GameDefinitions.ITEM_TYPES.CHARACTER and item.behavior == GameDefinitions.ITEM_TYPES.VENDOR then
			local modified = checkVendor(UNID)
			if modified then
				saveGameItem(tonumber(UNID), item)
			end
		end
	end
end

validateRecipe = function(UNID)
	LOG_FUNCTION_CALL("validateRecipe")
	
	local item = s_uItemProperties[UNID]
	local validated = checkRecipe(item)

	if not validated then
		item.invalid = true		
		saveGameItem(tonumber(UNID), item)
	end

	return validated
end

-- Detects if a quest's pre-requisite has been deleted
validateQuestPrereqs = function()
	LOG_FUNCTION_CALL("validateQuestPrereqs")

	for UNID, item in pairs(s_uItemProperties) do
		if item and item.itemType == GameDefinitions.ITEM_TYPES.QUEST then
			-- Does this quest have a prerequisite?
			if tonumber(item.prerequisiteQuest) ~= 0 then
				-- Does this prerequisite no longer exist?
				if not s_uItemProperties[tostring(item.prerequisiteQuest)] then
					item.prerequisiteQuest = 0
					saveGameItem(tonumber(UNID), item)
				end
			end
		end
	end
end

-- Validate each prerequisite and reward of a quest is still valid
validateQuestItems = function()
	LOG_FUNCTION_CALL("validateQuestItems")
	
	local questUpdated = false
	local questsToBeRemoved = {}
	for UNID, item in pairs(s_uItemProperties) do
		if item and item.itemType == GameDefinitions.ITEM_TYPES.QUEST then
			local questModified = false
			-- Does this quest's required item match the deletedUNID or is it an invalid item
			if item.requiredItem and tonumber(item.requiredItem.requiredUNID) and (tonumber(item.requiredItem.requiredUNID) > 0) and s_uItemProperties[tostring(item.requiredItem.requiredUNID)] == nil then
				item.requiredItem = {
					requiredUNID  = "0",
					requiredCount = 0
				}
				questModified = true
				questUpdated = true
				table.insert(questsToBeRemoved, tonumber(UNID))
			elseif item.rewardItem and (tonumber(item.rewardItem.rewardUNID) > 0) and s_uItemProperties[tostring(item.rewardItem.rewardUNID)] == nil then
				item.rewardItem = {
					rewardUNID  = "0",
					rewardCount = 0
				}
				questModified = true
				questUpdated = true
				table.insert(questsToBeRemoved, tonumber(UNID))
			end
			
			if questModified then
				saveGameItem(tonumber(UNID), item)
			end
		end
	end
	return questUpdated, questsToBeRemoved
end

-- Validates all quest givers' quests still exist
validateQuestGiverQuests = function()
	LOG_FUNCTION_CALL("validateQuestGiverQuests")
	
	for UNID, item in pairs(s_uItemProperties) do
		-- Check quest givers
		if item and item.itemType == GameDefinitions.ITEM_TYPES.CHARACTER and item.behavior == "quest_giver" then
			local questsModified = false
			-- Ensure each quest giver's quest is still valid
			local questGiverBehaviorParams = item.behaviorParams
			if questGiverBehaviorParams and questGiverBehaviorParams.quests then
				for index, questUNID in pairs(questGiverBehaviorParams.quests) do
					if not s_uItemProperties[tostring(questUNID)] then
						questGiverBehaviorParams.quests[index] = nil
						questsModified = true
					end
				end
			end
			
			if questsModified then
				saveGameItem(tonumber(UNID), item)
			end
		end
	end
end

-- Validate if the quest waypoint was deleted
validateQuestWaypoint = function(PID)
	for UNID, item in pairs(s_uItemProperties) do
		if item and item.itemType == GameDefinitions.ITEM_TYPES.QUEST then
			if item.waypointPID and item.waypointPID == PID then
				item.waypointPID = 0
				item.waypointInfo = {}
				saveGameItem(tonumber(UNID), item)
			end
		end
	end
end

-- Validate trade inputs and outputs for a specific vendor
checkVendor = function(UNID, removedUNID)
	LOG_FUNCTION_CALL("checkVendor")
	
	local bModified = false
	
	local tRemoveList = {}

	local gameItem = s_uItemProperties[UNID]
	if gameItem and gameItem.trades then
		for i, trade in pairs(gameItem.trades) do
			local bLocalModified = false
			
			local strTradeInput = tostring(trade.input)
			local strTradeOutput = tostring(trade.output)
			
			-- Check if trades contain the item that is going to be removed
			if removedUNID and strTradeInput == tostring(removedUNID) or strTradeOutput == tostring(removedUNID) then
				bLocalModified = true
			-- Check if trades contain valid items (inputs and outputs) 
			else
				local inputGameItem = s_uItemProperties[strTradeInput]
				local outputGameItem = s_uItemProperties[strTradeOutput]
				if (not inputGameItem and not strTradeInput == "Rewards" and not strTradeInput == "Credits" ) or not outputGameItem then
					bLocalModified = true
				end
			end

			-- If this trade was modified, mark it for removal from the vendor
			if bLocalModified == true then
				bModified = true
				table.insert(tRemoveList, i)
			end
		end
		
		-- Remove entries that were marked for removal.
		for i, v in pairs(tRemoveList) do
			gameItem.trades[v] = nil
			gameItem.trades = Toolbox.convertTableToIndexed(gameItem.trades)
		end
	end

	return bModified
end

-- Validate recipe inputs for a specific recipe
checkRecipe = function(recipe)
	LOG_FUNCTION_CALL("checkRecipe")
	
	local inputCount = 0
	local inputList = recipe.inputs
	if inputList and Toolbox.isContainer(inputList) then
		local tRemoveList = {}
		for i, input in pairs(inputList) do
			local input = inputList[i]
			local strUNID = tostring(input.UNID)
			if input.count > 0 and tonumber(strUNID) > 0 and s_uItemProperties[strUNID] then
				inputCount = inputCount + 1
			else
				-- Mark entry for removal
				table.insert(tRemoveList, i)
			end
		end
		
		-- Remove entries that were marked for removal.
		for i, v in pairs(tRemoveList) do
			inputList[v] = nil
		end
	end

	--If no valid inputs
	if inputCount == 0 then
		return false
	end

	if recipe.output and type(recipe.output) == "number" then
		local strUNID = tostring(recipe.output)
		-- If the output is a valid UNID
		if tonumber(strUNID) > 0 and s_uItemProperties[strUNID] then
			--TODO: Fix logic
		else
			recipe.output = 0
			return false
		end
	else
		return false
	end

	return true
end

packageRecipeToClient = function(UNID, item)
	LOG_FUNCTION_CALL("packageRecipeToClient")

	local recipe = {}
	recipe.inputs = {}
	
	-- Copy over all inputs
	for i, recipeInput in pairs(item.inputs) do
	--for i=1, #item.inputs do
		if recipeInput then
			
			local recipeItem = s_uItemProperties[tostring(recipeInput.UNID)]
			
			if recipeItem == nil then
				log("ERROR DETECTED - ED-6936 : Item with UNID "..tostring(recipeInput.UNID).." not found for recipe!")
				Debug.printTableError("ERROR DETECTED - ED-6936 : item", item)
				recipeItem = {name = "Unknown Item"}
			end
			
			recipe.inputs[i] = {	
				UNID  = recipeInput.UNID,
				name  = recipeItem.name,
				count = recipeInput.count
			}
		end
	end

	recipe.output = {	
		UNID		= item.output, 
		properties	= s_uItemProperties[tostring(item.output)]
	}

	recipe.proxReq = item.proxReq
	recipe.UNID = UNID
	recipe.itemType = item.itemType
	recipe.name = item.name
	recipe.description = item.description
	recipe.invalid = item.invalid
	
	if item.itemType==GameDefinitions.ITEM_TYPES.BLUEPRINT then
		recipe.rarity = item.rarity
		recipe.oneUse = item.oneUse
		recipe.count = item.count
		recipe.level = item.level
		recipe.GLID = item.GLID
	end

	return recipe
end

placeItemSpawn = function(self, player, UNID, position)
	
	LOG_FUNCTION_CALL("placeItemSpawn")
	
	local spawnerUNID = checkForDefaultSpawner(UNID)
	local newSpawner = false

	local spawner
	if spawnerUNID ~= nil then
		spawner = s_uItemProperties[tostring(spawnerUNID)]
	else
		--MAKE DEFAULT
		local spawnItem = s_uItemProperties[tostring(UNID)]
		newSpawner = true

		local itemType = spawnItem.itemType
		--isLootItem?
		if (	itemType == GameDefinitions.ITEM_TYPES.CONSUMABLE or
				itemType == GameDefinitions.ITEM_TYPES.AMMO or
				itemType == GameDefinitions.ITEM_TYPES.WEAPON or
				itemType == GameDefinitions.ITEM_TYPES.ARMOR or
				itemType == GameDefinitions.ITEM_TYPES.TOOL or
				itemType == GameDefinitions.ITEM_TYPES.GENERIC) or
				(itemType == GameDefinitions.ITEM_TYPES.CHARACTER and spawnItem and spawnItem.behavior == "pet") or
				(itemType == GameDefinitions.ITEM_TYPES.HARVESTABLE and spawnItem and spawnItem.behavior == "seed") then

			itemType = "loot"
		end

		-- Create a default spawner for this item by type
		spawner = Toolbox.deepCopy(DEFAULT_SPAWNER[itemType])

		spawner.name = tostring(spawnItem.name).." Spawner"
		spawner.description = "Spawner for "..tostring(spawnItem.name)
		
		-- Assign the new UNID		
		if itemType == GameDefinitions.ITEM_TYPES.HARVESTABLE or itemType == GameDefinitions.ITEM_TYPES.CHARACTER then
			spawner.behaviorParams.spawnUNID = UNID
		else
			spawner.behaviorParams.loot.limitItems.UNID = UNID
		end

		local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()

		if ZoneInfo.isLinkedZone() then
		s_iNextUNID = tonumber(SaveLoad.loadGameData("NextUNID", parentID, parentType))
		end

		spawnerUNID = s_iNextUNID
		spawner.GIID = spawnerUNID
		
		local strNextUNID = tostring(s_iNextUNID)

		s_uItemProperties[strNextUNID] = spawner

		saveGameItem(s_iNextUNID, spawner)

		-- Download this new Game Item's image
		--downloadGameItemIcon({}, {player = event.player, UNID = tostring(s_iNextUNID)})

		s_iNextUNID = s_iNextUNID + 1
		
		SaveLoad.saveGameData("NextUNID", s_iNextUNID, parentID, parentType)
	end

	local placementInfo = {	
		player = player, 
		GLID = spawner.GLID,
		UNID = tonumber(spawnerUNID), 
		behavior = spawner.behavior, 
		x = position.x, y = position.y, z = position.z, 
		rx = position.rx, ry = position.ry, rz = position.rz
	}
	
	self:onPlayerBuildPlaceItem(placementInfo)

	-- Return success
	return true, spawner, newSpawner, spawnerUNID
end

checkForDefaultSpawner = function(spawnUNID)
	
	LOG_FUNCTION_CALL("checkForDefaultSpawner")
	
	for UNID, spawner in pairs(s_uItemProperties) do
		if spawner and spawner.itemType == GameDefinitions.ITEM_TYPES.SPAWNER then
			if spawner.behaviorParams and spawner.behaviorParams.defaultSpawner then
				--We need to standardize this...
				if spawner.behaviorParams.spawnUNID then
					if spawner.behaviorParams.spawnUNID == spawnUNID then
						return UNID
					end
				elseif spawner.behaviorParams.loot then
					if spawner.behaviorParams.loot.limitItems.UNID == spawnUNID then
						return UNID
					end
				end				
			end
		end
	end
	return nil
end



-- Roll on an object's rarity type to see if it's passed
rarityRoll = function(UNID, dropChance)
	LOG_FUNCTION_CALL("rarityRoll")
	
	local gameItem = s_uItemProperties[tostring(UNID)]
	
	if gameItem == nil then 
		return false, 0, 0
	end
	local rarity = gameItem.rarity

	local rollChance = dropChance or GameDefinitions.ROLL_CHANCE[rarity] or 100
	--log("--- rollChance for ".. rarity.. ": ".. tostring(rollChance).. "\n\tdropChance: ".. tostring(dropChance).. "\n\tGameDefinitions.ROLL_CHANCE[rarity]: ".. tostring(GameDefinitions.ROLL_CHANCE[rarity]).. "\n\t100: 100")
	local roll = math.random() * 100
	--log("--- roll ".. tostring(roll).. " <= rollChance ".. tostring(rollChance))

	-- added a bailout for "never" items that shouldn't drop 
	if rollChance > 0 and roll <= rollChance then
		return true, roll, rollChance
	else
		return false, roll, rollChance
	end
end


-- Gets an item's info from a PID
getItemInfoFromPID = function(PID)
	LOG_FUNCTION_CALL("getItemInfoFromPID")
	
	local itemInfo = {	
		owner    = "NoOwner",
		itemType = "unknown"
	}
	
	local activeDO = s_tActiveDOs[tonumber(PID)]
	if activeDO then
		local UNID = activeDO.UNID
		itemInfo.owner = activeDO.owner or "NoOwner"
		if UNID then
			itemInfo.UNID = UNID
			local gameItem = s_uItemProperties[tostring(UNID)]
			if gameItem then
				for i, v in pairs(gameItem) do
					itemInfo[i] = v
				end
				itemInfo.itemType = itemInfo.itemType or "unknown"
			end
		end
	end
	
	return itemInfo
end

-- Gets the behaviorParams for an object by UNID
getBehaviorParams = function(UNID, behaviorDetails)
	LOG_FUNCTION_CALL("getBehaviorParams")
	local gameItem = s_uItemProperties[tostring(UNID)]
	
	if gameItem.behavior then
		
		if behaviorDetails == nil then
			behaviorDetails = {}
			if gameItem.behaviorParams then
				behaviorDetails = Toolbox.deepCopy(gameItem.behaviorParams)
			end
		else
			behaviorDetails = Toolbox.deepCopy(behaviorDetails)
		end
		
		if behaviorDetails.spawnUNID then
			-- Do we have the spawnUNID needed?
			local spawnItem = s_uItemProperties[tostring(behaviorDetails.spawnUNID)]
			if spawnItem then
				behaviorDetails.spawnInput = Toolbox.deepCopy(spawnItem)
				if behaviorDetails.spawnInput == nil then behaviorDetails.spawnInput = {} end
				
				-- Extract the spawnGLID for this spawner
				behaviorDetails.spawnGLID = spawnItem.GLID
				
				-- If this object has a behavior, load its parameters as well
				if spawnItem.behavior then
					behaviorDetails.spawnType = spawnItem.behavior
					behaviorDetails.spawnInput.UNID = tonumber(behaviorDetails.spawnUNID)
					-- Load behavior details
					if spawnItem.behaviorParams then
						for i, v in pairs(spawnItem.behaviorParams) do
							behaviorDetails.spawnInput[i] = v
						end
					end
				end
			end
		end
	end
	
	return behaviorDetails
end

-- Extracts Game Items needed from a Game Item by type
extractGLIDsByUNID = function(UNID)
	LOG_FUNCTION_CALL("extractGLIDsByUNID")

	local gameItem = s_uItemProperties[tostring(UNID)]
	if gameItem == nil then log("Controller_Game - extractGLIDsByUNID() "..tostring(UNID).." is not a registered Game Item."); return end
	local itemType = gameItem.itemType
	if itemType == nil then log("Controller_Game - extractGLIDsByUNID() Game Item "..tostring(UNID).." has no itemType field."); return end
	
	local GLIDs = {}
	-- Ignore invalid Game Item types
	if (itemType == GameDefinitions.ITEM_TYPES.RECIPE) or (itemType == GameDefinitions.ITEM_TYPES.SPAWNER) or (itemType == GameDefinitions.ITEM_TYPES.SHORTCUT) then
		log("Controller_Game - extractGLIDsByUNID() Item Type["..tostring(itemType).."] is not a valid ")
		return GLIDs
	end
	
	-- Extract standard Game Item fields
	for i, field in pairs(GLID_EXTRACTION_FIELDS.Standard) do
		local gameItemField = gameItem[field]
		if gameItemField then
			if Toolbox.isContainer(gameItemField) then
				for i, v in pairs(gameItemField) do
					GLIDs[tostring(v)] = true
				end
			else
				GLIDs[tostring(gameItemField)] = true
			end
		end
	end
	
	-- Extract unique items from behavior fields
	if gameItem.behavior then
		local classInfo = getBehaviorParams(event.UNID)
		if classInfo then
			-- Extract BehaviorParam Game Item fields
			for i, field in pairs(GLID_EXTRACTION_FIELDS.BehaviorParams) do
				local classInfoField = classInfo[field]
				if classInfoField then
					if Toolbox.isContainer(classInfoField) then
						for i, v in pairs(classInfoField) do
							GLIDs[tostring(v)] = true
						end
					else
						GLIDs[tostring(classInfoField)] = true
					end
				end
			end
		end
	end
	
	-- Scrub 0's from the GLIDs table before returning
	for GLID, _ in pairs(GLIDs) do
		if tonumber(GLID) == 0 then
			GLIDs[GLID] = nil
		end
	end
	
	return GLIDs
end



-- Check a Game Item for a unique by type
checkForUniqueName = function(item)
	LOG_FUNCTION_CALL("checkForUniqueName")
	
	local unique = true
	local uniqueName = item.name
	local conflictingUNID = 0
	
	local ALIS_RUNE = "(.+)%((%d+)%)$"
	
	local uniqueFound = false
	while not uniqueFound do
		local nameChanged = false
		for UNID, props in pairs(s_uItemProperties) do
			-- Only compare against items of the same type
			if props and props.itemType == item.itemType then
				-- Name conflict found
				if props.name == uniqueName then
					conflictingUNID = UNID
					local str, digit = string.match(uniqueName, ALIS_RUNE)
					if digit then
						uniqueName = str .. "(" .. (digit + 1) .. ")"
					else
						uniqueName = uniqueName .. "(1)"
					end
					unique = false
					nameChanged = true
					break
				end
			end
		end
		-- Loop again if we changed the name
		if nameChanged then
			uniqueFound = false
		else
			uniqueFound = true
		end
	end
	-- Item was not unique
	if not unique then
		return unique, uniqueName, conflictingUNID
	else
		return unique, item.name, conflictingUNID
	end
end


-- TODO: This can be moved into helper file
isLootItem = function(item)
	if item.itemType == GameDefinitions.ITEM_TYPES.CHARACTER and item.behavior == "pet" then -- Pets are loot items
		return true
	elseif item.itemType == GameDefinitions.ITEM_TYPES.HARVESTABLE and item.behavior == "seed" then -- Seeds are loot items
		return true
	elseif	item.itemType == GameDefinitions.ITEM_TYPES.WEAPON or 
			item.itemType == GameDefinitions.ITEM_TYPES.AMMO or 
			item.itemType == GameDefinitions.ITEM_TYPES.ARMOR or 
			item.itemType == GameDefinitions.ITEM_TYPES.TOOL or 
			item.itemType == GameDefinitions.ITEM_TYPES.BLUEPRINT or 
			item.itemType == GameDefinitions.ITEM_TYPES.CONSUMABLE or 
			item.itemType == GameDefinitions.ITEM_TYPES.GENERIC or 
			item.itemType == GameDefinitions.ITEM_TYPES.PLACEABLE then
		return true
	end

	return false
end

-- Initializes the Framework's DB (NextUNID, default Game Items, etc)
initFrameworkDB = function()
	LOG_FUNCTION_CALL("initFrameworkDB")
	
	-- Initialize this world's DB
	kgp.gameSetFrameworkEnabled(true)
	kgp.gameInitFrameworkDB()
	
	Game.getWorldSettings()
	
	loadGameItemProperties()
end

-- Counts up all activeDOs and inform clients when there's one item left
function countActiveDOsOnRequest(self, event)
	countActiveDOs()
end
countActiveDOs = function()
	LOG_FUNCTION_CALL("countActiveDOs")
	
	-- If there is one activeDO left, inform Inventory
	local DOCount = 0
	local pidsToIgnore = {}
	local lastDO
	local lastUNID
	for PID, data in pairs(s_tActiveDOs) do
		if pidsToIgnore[tostring(PID)] == nil then
			uItemProps = s_uItemProperties[data.UNID]
			if uItemProps then
				if uItemProps.behavior == "pathWaypoint" then
					pidsToIgnore[tostring(PID)] = PID
				elseif uItemProps.behavior == "mover" or uItemProps.behavior == "teleporter" then
					local associatedObjects = getObjectAssociations(PID)
					if (next(associatedObjects)) then
						for _, associatedPID in pairs(associatedObjects) do
							if associatedPID ~= PID then
								pidsToIgnore[tostring(associatedPID)] = associatedPID
							end
						end
					end
				end
			end
		end
	end
	for PID, data in pairs(s_tActiveDOs) do
		if pidsToIgnore[tostring(PID)] == nil then
			DOCount = DOCount + 1
			lastDO = PID
			lastUNID = data.UNID
			if DOCount > 1 then break end
		end
	end
	
	-- If this is the one and only Game Item
	if DOCount == 1 and not ZoneInfo.isLinkedZone() then
		s_iLastGameItem = lastDO
		lastGameItems = {}
		lastGameItems[tostring(lastDO)] = lastDO
		lastItemProps = s_uItemProperties[lastUNID]
		if lastItemProps then
			if lastItemProps.behavior == "path" then
				gameItem = s_uItemInstances[lastDO]
				if gameItem and gameItem.noOfPathWaypoints == 1 then
					for PID, data in pairs(s_tActiveDOs) do
						if PID ~= lastDO then
							lastGameItems[tostring(PID)] = PID
						end
					end
				end
			elseif lastItemProps.behavior == "mover" or lastItemProps.behavior == "teleporter" then
				local associatedObjects = getObjectAssociations(lastDO)
				if (next(associatedObjects)) then
					for _, associatedPID in pairs(associatedObjects) do
						lastGameItems[tostring(associatedPID)] = associatedPID
					end
				end
			end
		else
			log("ERROR DETECTED - CountActiveDOs: the last active DO's uItemProperties is nil")
		end
		for sName, uPlayer in pairs(s_uPlayers) do
			if uPlayer and uPlayer.inZone then
				Events.sendClientEvent("FRAMEWORK_ONE_GAME_ITEM_DO_LEFT", {lastGameItem = lastGameItems, playerPerm = uPlayer.perm}, uPlayer.ID)
			end
		end
	-- If we have more than 1 Game Item and we've sent a lastGameItem before
	elseif s_iLastGameItem or ZoneInfo.isLinkedZone() then
		s_iLastGameItem = nil
		
		for sName, uPlayer in pairs(s_uPlayers) do
			-- Nil out client-stored last Game Item PID
			if uPlayer and uPlayer.inZone then
				Events.sendClientEvent("FRAMEWORK_ONE_GAME_ITEM_DO_LEFT", {lastGameItem = nil, playerPerm = uPlayer.perm}, uPlayer.ID)
			end
		end
	end
	
	--If all the active DO's are picked up and destroyed and need to deinstantiate the framework
	if ((not next(s_tActiveDOs)) and (s_deinstantiateFramework == true)) then
		Events.sendEvent("FRAMEWORK_DE_INSTANTIATE", {player = s_deinstantiatingPlayer})
		s_deinstantiateFramework = false
		s_deinstantiatingPlayer = nil
	end
end


-- TODO: This may need to be moved to the Controller_Player after Controller_Player is finished with splitting off
constructLabelTable = function(labels)
	LOG_FUNCTION_CALL("constructLabelTable")
	
	local returnTable = {}
	for labelIndex, labelTable in pairs(labels) do
		local labelColor = labelTable.color or {}
		returnTable[labelIndex] = {
			labelTable.label, 
			labelTable.size, 
			labelColor.r or 1, 
			labelColor.g or 1, 
			labelColor.b or 1
		}
	end
	return returnTable
end



function onEventFlagStatusUpdated(self, event)
	if event.PID then
		local eventFlag = s_uItemInstances[event.PID]
		self:onObjectAddedToWorld(eventFlag)
	end
end

function onObjectAddedToWorld(self, uItem)
	LOG_FUNCTION_CALL("onObjectAddedToWorld")

	local mapObject, playerName = MapHelper.generateSingleMapObject(uItem, s_uItemProperties)
	
	if mapObject then
		-- Do we have a player specified to send this info to?
		if( playerName ) then
			local playerObject = s_uPlayers[playerName]
			-- Is the player online?
			if playerObject then
				-- Send off the map data to whoever requested it
				Events.sendClientEvent("FRAMEWORK_WORLD_MAP_OBJECT_ADDED", {mapObject = mapObject}, playerObject.ID)
			end
		else
			-- Broadcast the event
			Events.sendClientEvent("FRAMEWORK_WORLD_MAP_OBJECT_ADDED", {mapObject = mapObject})
		end
	end
end

function onObjectRemovedFromWorld(self, uItem)
	LOG_FUNCTION_CALL("onObjectRemovedFromWorld")
		
	local mapObject, playerName = MapHelper.generateSingleMapObject(uItem, s_uItemProperties)
	if mapObject then
		-- Do we have a player specified to send this info to?
		if( playerName ) then
			local playerObject = s_uPlayers[playerName]
			-- Is the player online?
			if playerObject then
				-- Send off the map data to whoever requested it
				Events.sendClientEvent("FRAMEWORK_WORLD_MAP_OBJECT_REMOVED", {PID = mapObject.PID}, playerObject.ID)
			end
		else
			-- Broadcast the event
			Events.sendClientEvent("FRAMEWORK_WORLD_MAP_OBJECT_REMOVED", {PID = mapObject.PID})
		end
	end
end


checkObjectPlaceability = function(event)
	LOG_FUNCTION_CALL("checkObjectPlaceability")

	-- If the item whose placement is requested has no behavior, then go ahead with the placement
	if( not event.behavior ) then return GameDefinitions.PLACEABILITY.ALLOWED end
	
	-- Gather information on the item that the client requested be placed.
	local player = event.player
	local playerID = player and player.ID
	
	local itemInfo = {}		
	local gameItem = s_uItemProperties[tostring(event.UNID)]
	if gameItem then
		for i, v in pairs(gameItem) do
			itemInfo[i] = v
		end
	end
	
	local classInfo = getBehaviorParams(event.UNID)
	if classInfo then
		for i, v in pairs(classInfo) do
			itemInfo[i] = v
		end
	end

	itemInfo.owner = player.name
	itemInfo.itemType = itemInfo.itemType or "unknown"
	itemInfo.UNID = event.UNID
	
	if itemInfo.behaviorParams and itemInfo.behaviorParams.worldDestination or itemInfo.zoneDestination or itemInfo.specialDestination then
		event.worldTeleporter = true
	end
	if event.behavior == "teleporter" and not event.worldTeleporter then
		itemInfo.associated = {["teleporter"] = 1}
	elseif event.behavior == "mover" then
		itemInfo.associated = {["waypoint"] = 2}
	end
	
	itemInfo.position = {x = event.x, y = event.y, z = event.z}
	
	-- Check whether if the item being placed is a claim flag and if the conditions for that are OK
	if( not isClaimFlagCreationAllowed(itemInfo) ) then
		Events.sendClientEvent("FRAMEWORK_PLACE_CLAIM_FLAG_CONFIRMATION",{}, playerID)
		return GameDefinitions.PLACEABILITY.REJECTED
	end
	
	-- Check whether if the item being placed is a media player and if the conditions for that are OK
	if( not isMediaPlayerCreationAllowed(itemInfo) ) then
		Events.sendClientEvent("FRAMEWORK_INVALID_MEDIA_PLACMENT",{}, playerID)
		return GameDefinitions.PLACEABILITY.REJECTED
	end
	
	-- if pathWaypoint, populate itemInfo also with parentPathPID and prev and next pathWaypoint's PID
	if event.behavior == "pathWaypoint" then
		itemInfo.additionalPathInfo = event.additionalPathInfo
	elseif event.behavior == "path" then
		if itemInfo.pathRider_parentUNID == nil then
			log("Controller_Inventory - checkObjectPlaceability() path does not have a valid rider object")
			return GameDefinitions.PLACEABILITY.REJECTED
		end
	end

	-- Calling event here with a PID of 0, indicating a pending placement
	-- Response success fail and finalization of object placement happens in onPlacePendingResponse, triggered by this event from the Spawn Controller
	local eventData = {
		PID			= 0, 
		behavior	= event.behavior, 
		input		= itemInfo, 
		pendingID	= s_iPendingID
	}

	Events.sendEvent("OBJECT_INSTANCE_CLASS", eventData)
	
	s_tPendingQueue[s_iPendingID] = event
	s_iPendingID = s_iPendingID + 1
	
	return GameDefinitions.PLACEABILITY.PENDING
end




-- Instantiate all active DOs in the zone based on s_tActiveDOs
instantiateActiveDOs = function(event)
	LOG_FUNCTION_CALL("instantiateActiveDOs")

	-- Object instantiation is a two-pass process, instancing all waypoints first
	local NUM_PASSES = 2
	s_tInstanceQueue[0] = true
	s_iInstancedCount = 0
	s_iInstanceCount = 0
	for instantiatePass = 1, NUM_PASSES do
		for PID, props in pairs(s_tActiveDOs) do
			local info = getItemInfoFromPID(PID)
			if (instantiatePass == 1 and info.itemType == "waypoint") or (instantiatePass ~= 1 and info.itemType ~= "waypoint") then
				info.PID = tonumber(PID)
				info.alive = true
				info.type = "Object"
				if props.UNID then
					local gameItem = s_uItemProperties[tostring(props.UNID)]
					-- Does this DO have a class that needs instantiation (spawner, placeable w/ behavior, etc)
					if gameItem then
						if gameItem.behavior then
							
							-- Check for hybrid classes
							local tempBehavior = gameItem.behavior
							local associatedObjects = getObjectAssociations(info.PID)
							for i, associatedPID in pairs(associatedObjects) do
								-- Cloned hybrid will be the first index, check if PIDs match
								if i == 1 and associatedPID == info.PID then
									if tempBehavior == "actor" then -- TO DO: Sync this up with Controller_Spawn SPAWN_OBJECT_TYPES table and eliminate magic strings
										tempBehavior = "actor_pet"
									elseif tempBehavior == "vendor" then
										tempBehavior = "vendor_pet"
									elseif tempBehavior == "shop_vendor" then
										tempBehavior = "shop_vendor_pet"
									elseif tempBehavior == "quest_giver" then
										tempBehavior = "quest_giver_pet"
									end
								end
							end

							-- Load behavior details
							local classInfo = getBehaviorParams(props.UNID)
							for i, v in pairs(classInfo) do
								info[i] = v
							end
							info.UNID = props.UNID
							
							-- Introspect quest name for waypoints associated with quests
							if info.itemType == "waypoint" then
								info.animation = GameDefinitions.WAYPOINT_ANIM
								local quest = getQuestFromWaypoint(PID)
								if quest then
									info.name = quest.name .. " " .. info.name
								end
							end
							
							s_iInstanceCount = s_iInstanceCount + 1
							s_tInstanceQueue[tonumber(PID)] = true
							Game.logProgress{
								log =	"Inventory - Instantiating Game Item UNID[" ..
										tostring(props.UNID) .. 
										"] PID["..tostring(PID) .. 
										"] name[" ..
										tostring(gameItem.name) ..
										"]",
								perentage = 80 * (s_iInstancedCount / s_iInstanceCount)
							}

							Events.sendEvent("OBJECT_INSTANCE_CLASS", {PID = PID, behavior = tempBehavior, input = info})
						else
                            log("ERROR: Attempt to instance object without behavior. PID = "..tostring(PID))
						end
					end
				end
			end
		end
	end
	
	s_tInstanceQueue[0] = nil
	if not itemsInQueue() then
		s_bIsControllerReady = true
		-- Inform Controller_Game that Inventory is finished activating
		Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Inventory", state = {["active"] = true}})

		Events.sendEvent("FRAMEWORK_CHECK_FLAG_STATE")
	end
end


getQuestFromWaypoint = function(waypointPID)
	LOG_FUNCTION_CALL("getQuestFromWaypoint")
	
	if s_uItemProperties == nil then return end
	for UNID, item in pairs(s_uItemProperties) do
		if item and item.waypointPID and item.waypointPID == waypointPID then
			return item
		end
	end
end

itemsInQueue = function()
	LOG_FUNCTION_CALL("itemsInQueue")
	
	return not Toolbox.isContainerEmpty(s_tInstanceQueue)
end



-- Get # of claim flags in the world
checkForClaimFlags = function()
	local flagCount = 0
	for PID, flag in pairs(s_tLandClaimFlags) do
		if flag then
			flagCount = flagCount + 1
		end
	end
	return flagCount
end



getDynamicObjectPosition = function(objectPID)
	LOG_FUNCTION_CALL("getDynamicObjectPosition")
	
	local mapEntry = s_uItemInstances[tostring(objectPID)]
	if( mapEntry ) then
		local pos = mapEntry.position
		if( pos ) then 
			return pos.x, pos.y, pos.z
		end
	end
	
	return kgp.objectGetStartLocation(tonumber(objectPID))
end



getClaimFlagsAtPoint = function(x, y, z)
	LOG_FUNCTION_CALL("getClaimFlagsAtPoint")
	
	local flags = {}
	
	for PID, flag in pairs(s_tLandClaimFlags) do
		if isPointInFlagBoundary(flag, x, y, z) then
			table.insert(flags, flag)
		end
	end
	
	return flags
end

-- Checks whether a point falls within the boundaries of a flag
isPointInFlagBoundary = function(flag, x, y, z)
	LOG_FUNCTION_CALL("isPointInFlagBoundary")
	
	if tonumber(x) == nil or tonumber(y) == nil or tonumber(z) == nil then
		log("ERROR DETECTED - ED-7040 : isPointInFlagBoundry failed! x = "..tostring(x)..", y = "..tostring(y)..", z = "..tostring(z))
		Debug.printTableError("ERROR DETECTED - ED-7040 : flag", flag)
		Debug.printStack()
		return false
	end
	
	-- OLD SPHERE CHECK
	-- local xDiff = x - flag.position.x
	-- local zDiff = z - flag.position.z
	-- local xzDistanceSqrd = xDiff * xDiff + zDiff * zDiff
	-- local radiusSqrd = flag.radius * flag.radius
	-- return (radiusSqrd > xzDistanceSqrd) and flag.height >= math.abs(y - flag.position.y)

	-- NEW CUBE CHECK
	local flagDepth 		= (flag.depth / 2) or 0
	local flagHeight 		= flag.height  or 0
	local flagWidth 		= (flag.width / 2) or 0

	local xMin = flag.position.x - flagDepth - BOUND_CHK_BUFFER
	local xMax = flag.position.x + flagDepth + BOUND_CHK_BUFFER
	local yMin = flag.position.y - BOUND_CHK_BUFFER_Y
	local yMax = flag.position.y + flagHeight + BOUND_CHK_BUFFER
	local zMin = flag.position.z - flagWidth - BOUND_CHK_BUFFER
	local zMax = flag.position.z + flagWidth + BOUND_CHK_BUFFER

	if  x < xMin or x > xMax or
		y < yMin or y > yMax or
		z < zMin or z > zMax then
		return false
	end
	return true
end

-- Checks whether two flags intersect
doFlagsIntersect = function(flag, otherFlag)
	LOG_FUNCTION_CALL("doFlagsIntersect")

	-- OLD SPHERE CHECK
	-- local xDiff = otherFlag.position.x - flag.position.x
	-- local zDiff = otherFlag.position.z - flag.position.z
	-- local xzDistanceSqrd = xDiff * xDiff + zDiff * zDiff
	-- local radiusSum = flag.radius + otherFlag.radius
	-- local radiusSqrd = radiusSum * radiusSum
	-- return (radiusSqrd >= xzDistanceSqrd) and (flag.height + otherFlag.height >= math.abs(otherFlag.position.y - flag.position.y))

	-- NEW CUBE CHECK
	local flagDepth 		= (flag.depth / 2) or 0
	local otherFlagDepth 	= (otherFlag.depth /2) or 0
	local flagHeight 		= flag.height or 0
	local otherFlagHeight 	= otherFlag.height or 0
	local flagWidth 		= (flag.width /2) or 0
	local otherFlagWidth 	= (otherFlag.width /2) or 0
	-- flag max/min values
	local xMin = flag.position.x - flagDepth - BOUND_CHK_BUFFER
	local xMax = flag.position.x + flagDepth + BOUND_CHK_BUFFER
	local yMin = flag.position.y - BOUND_CHK_BUFFER_Y
	local yMax = flag.position.y + flagHeight + BOUND_CHK_BUFFER
	local zMin = flag.position.z - flagWidth - BOUND_CHK_BUFFER
	local zMax = flag.position.z + flagWidth + BOUND_CHK_BUFFER
	-- otherFlag max/min values
	local xMin2 = otherFlag.position.x - otherFlagDepth - BOUND_CHK_BUFFER
	local xMax2 = otherFlag.position.x + otherFlagDepth + BOUND_CHK_BUFFER
	local yMin2 = otherFlag.position.y + BOUND_CHK_BUFFER - BOUND_CHK_BUFFER_Y
	local yMax2 = otherFlag.position.y + otherFlagHeight + BOUND_CHK_BUFFER
	local zMin2 = otherFlag.position.z - otherFlagWidth - BOUND_CHK_BUFFER
	local zMax2 = otherFlag.position.z + otherFlagWidth + BOUND_CHK_BUFFER

	if  (xMin < xMax2 and xMax > xMin2) and
		(yMin < yMax2 and yMax > yMin2) and
		(zMin < zMax2 and zMax > zMin2) then
		return true
	end
	return false
end

-- Checks whether two media players intersect
doMediaPlayersIntersect = function(media, otherMedia)
	LOG_FUNCTION_CALL("doMediaPlayersIntersect")
	local xDiff = otherMedia.position.x - media.position.x
	local yDiff = otherMedia.position.y - media.position.y
	local zDiff = otherMedia.position.z - media.position.z
	local distanceSqrd = xDiff * xDiff + yDiff * yDiff + zDiff * zDiff
	local radiusSum = (media.radius + otherMedia.radius)
	return (radiusSum * radiusSum >= distanceSqrd)
end



-- CJW, TODO: Claim flag functionality needs to be encapsulated in a class and the radius checks need to be carried there.
isClaimFlagCreationAllowed = function(item)
	LOG_FUNCTION_CALL("isClaimFlagCreationAllowed")
	if item.behavior == "claim" then
		for PID, info in pairs(s_tActiveDOs) do
			if info.owner ~= item.owner then
				local x, y, z = kgp.objectGetStartLocation(PID)
				if x and y and z then
					local flag = s_tLandClaimFlags[PID]
					if flag then
						if doFlagsIntersect(item, flag) then
							return false
						end
					else
						if(isPlaceableItem(info.UNID) and  isPointInFlagBoundary(item, x, y, z))  then
							return false
						end
					end
				end
			end
		end
	end
	
	return true
end

-- CJW, TODO: Media Player functionality needs to be encapsulated in a class and the radius checks need to be carried there.
 isMediaPlayerCreationAllowed = function(item)
	LOG_FUNCTION_CALL("isMediaPlayerCreationAllowed")
	-- If passed item is a behavio
	if item.behavior == "media" then
		for PID, info in pairs(s_tActiveDOs) do
			if info.owner ~= item.owner then
				local media = s_tMediaPlayers[PID]
				if media then
					local x, y, z = kgp.objectGetStartLocation(tonumber(PID))
					if x and y and z then
						if( doMediaPlayersIntersect(item, media) ) then
							return false
						end
					end
				end
			end
		end
	end
	
	return true
end


isPlaceableItem = function(UNID)
	local gameItem = s_uItemProperties[tostring(UNID)]
	return gameItem ~= nil and gameItem.itemType ~= nil and gameItem.itemType == GameDefinitions.ITEM_TYPES.PLACEABLE
end


createTeleporterAssociations = function(PID, placementInfo)
	LOG_FUNCTION_CALL("createTeleporterAssociations")
	placementInfo.x = placementInfo.x - (10 * placementInfo.rz)
	placementInfo.z = placementInfo.z + (10 * placementInfo.rx)
	local PID2 = placeGameItem(placementInfo)
	
	s_iObjectAssociationCount = s_iObjectAssociationCount + 1
	local objectAssociations = {}
	table.insert(objectAssociations, PID)
	table.insert(objectAssociations, PID2)
	SaveLoad.saveGameData("numObjectAssociations", s_iObjectAssociationCount)
	SaveLoad.saveGameData("objectAssociations_"..tostring(s_iObjectAssociationCount), Events.encode(objectAssociations))
	
	table.insert(s_tObjectAssociations, objectAssociations)
	
	Events.sendClientEvent("FRAMEWORK_CLIENT_UPDATE_OBJECT_ASSOCIATIONS", {objectAssociations = s_tObjectAssociations})
	
	Events.sendEvent("FRAMEWORK_REQUEST_OBJECT_ASSOCIATIONS", {PID = PID}) -- Handles requests for object associations
	Events.sendEvent("FRAMEWORK_REQUEST_OBJECT_ASSOCIATIONS", {PID = PID2})
end



createMoverAssociations = function(PID, placementInfo)
	LOG_FUNCTION_CALL("createMoverAssociations")
	
	local waypointInfo = s_uItemProperties[tostring(GameDefinitions.WAYPOINT_UNID)]
	if waypointInfo == nil then Console.Write(Console.DEBUG, "Waypoint game item data not found: Aborting creation!") return end
	
	waypointInfo.player	= placementInfo.player
	waypointInfo.GLID	= placementInfo.GLID
	waypointInfo.UNID	= GameDefinitions.WAYPOINT_UNID
	waypointInfo.x		= placementInfo.x
	waypointInfo.y		= placementInfo.y
	waypointInfo.z		= placementInfo.z
	waypointInfo.rx		= placementInfo.rx
	waypointInfo.ry		= placementInfo.ry
	waypointInfo.rz		= placementInfo.rz
	local PID2 = placeGameItem(waypointInfo)
	
	waypointInfo.y = waypointInfo.y + 8
	local PID3 = placeGameItem(waypointInfo)
	
	s_iObjectAssociationCount = s_iObjectAssociationCount + 1
	local objectAssociations = {}
	table.insert(objectAssociations, PID)
	table.insert(objectAssociations, PID2)
	table.insert(objectAssociations, PID3)
	SaveLoad.saveGameData("numObjectAssociations", s_iObjectAssociationCount)
	SaveLoad.saveGameData("objectAssociations_"..tostring(s_iObjectAssociationCount), Events.encode(objectAssociations))
	
	table.insert(s_tObjectAssociations, objectAssociations)
	
	Events.sendClientEvent("FRAMEWORK_CLIENT_UPDATE_OBJECT_ASSOCIATIONS", {objectAssociations = s_tObjectAssociations})
	
	Events.sendEvent("FRAMEWORK_REQUEST_OBJECT_ASSOCIATIONS", {PID = PID})
end


createFollowerAssociations = function(PID, placementInfo)
	LOG_FUNCTION_CALL("createFollowerAssociations")
	placementInfo.x = placementInfo.x - (10 * placementInfo.rz)
	placementInfo.z = placementInfo.z + (10 * placementInfo.rx)
	local PID2 = placementInfo.jango
	s_iObjectAssociationCount = s_iObjectAssociationCount + 1
	local objectAssociations = {}
	table.insert(objectAssociations, PID)
	table.insert(objectAssociations, PID2)
	SaveLoad.saveGameData("numObjectAssociations", s_iObjectAssociationCount)
	SaveLoad.saveGameData("objectAssociations_"..tostring(s_iObjectAssociationCount), Events.encode(objectAssociations))
	
	table.insert(s_tObjectAssociations, objectAssociations)
	
	Events.sendClientEvent("FRAMEWORK_CLIENT_UPDATE_OBJECT_ASSOCIATIONS", {objectAssociations = s_tObjectAssociations})
	
	Events.sendEvent("FRAMEWORK_REQUEST_OBJECT_ASSOCIATIONS", {PID = PID}) -- Handles requests for object associations
	Events.sendEvent("FRAMEWORK_REQUEST_OBJECT_ASSOCIATIONS", {PID = PID2})
end


collectMetricsForPlacedItem = function(placedItemUNID, playerName, playerMode)

	local placedItem = s_uItemProperties[tostring(placedItemUNID)]
			
	if placedItem and s_iZoneInstanceID and s_iZoneType then
		local mode = (playerMode == "God") and "Creator" or "Player"
		
		local input = {	
			username 	  	= playerName,
			zoneInstanceId	= s_iZoneInstanceID,
			zoneType 	  	= s_iZoneType,
			gameItemId 	  	= placedItemUNID,
			gameItemGlid   	= placedItem.GIGLID,
			itemType 	  	= placedItem.itemType,
			behavior 	  	= placedItem.behavior,
			gameplayMode   	= mode
		}
		Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {input = input, callType = "METRIC_BUILD"})
	end
end



-- Normalize the sum drop chance of custom loot up to 100% 
normalizeDropChance = function(objectLoot)
	
	LOG_FUNCTION_CALL("normalizeDropChance")
	
	local sumChance = 0
	local sumGenerics = 0

	for index, value in pairs(objectLoot) do
		if s_tGameItemTypes[tostring(index)] and (tostring(value) == "true" or tostring(value) == "empty") then
			sumGenerics = sumGenerics + 1
			objectLoot[index] = true
		elseif tostring(value) == "true" then
			--if itemType is checked, but not in game system
			objectLoot[index] = "empty"
		elseif tostring(index) ~= "singles" and not tostring(value) == "empty"  then
			objectLoot[index] = false
		end
	end

	objectLoot.sumGenerics = sumGenerics

	-- Loop through singles to calculate the sum drop chance
	if objectLoot.singles then
		for index, itemInfo in pairs(objectLoot.singles) do

			if tonumber(itemInfo.UNID) == nil then
				log("ERROR - **** normalizeDropChance **** - itemInfo.UNID was NIL!")
			end
			if tonumber(itemInfo.drop) == nil then
				log("ERROR - **** normalizeDropChance **** - itemInfo.drop was NIL!")
			end

			if tonumber(itemInfo.UNID) > 0 and tonumber(itemInfo.drop) > 0 then
				sumChance = sumChance + tonumber(itemInfo.drop)
			end
		end
	end

	-- Add the sum drop chance to loot table for later use
	objectLoot.sumChance = sumChance

	if sumChance > 100 then
		if objectLoot.singles then
			for index, itemInfo in pairs(objectLoot.singles) do
				if tonumber(itemInfo.UNID) > 0 and tonumber(itemInfo.drop) > 0 then
					itemInfo.drop = (itemInfo.drop / sumChance) * 100
				end
			end
		end
	end

	return objectLoot
end



-- Iterates over a loot table and generates a random loot item
generateLootItem = function(objectLoot, level)
	LOG_FUNCTION_CALL("generateLootItem")
	
	
	local lootItem = {}
	local rand = math.random() * 100

	-- TODO: on save of Random Loot Vendor, normalize the sum drop chance of custom loot up to 100% (new function)
	local lootTable = {}
	lootTable = normalizeDropChance(objectLoot) -- brings all custom loot to a sum chance of up to 100%

	-- if there are custom loot
	if lootTable.singles then
		-- the sum drop chance is 100%, then only choose the loot item from the custom drops
		if lootTable.sumChance >= 100 then
			local chanceChecked = 0

			for index, variable in pairs(lootTable.singles) do
				if variable.drop then
					chanceChecked = chanceChecked + variable.drop
					local item = s_uItemProperties[tostring(variable.UNID)]
					if rand <= chanceChecked then
						if tonumber(variable.UNID) > 0 and item then
							if item.itemType == GameDefinitions.ITEM_TYPES.WEAPON or item.itemType == GameDefinitions.ITEM_TYPES.ARMOR or item.itemType == GameDefinitions.ITEM_TYPES.TOOL then
								lootItem = {UNID = tonumber(variable.UNID), count = 1}
							elseif item.stackSize then
								lootItem = {UNID = tonumber(variable.UNID), count = math.min(item.stackSize, variable.quantity)}
							else
								lootItem = {UNID = tonumber(variable.UNID), count = math.min(GameDefinitions.DEFAULT_STACK_SIZE, variable.quantity)}
							end
						else
							lootItem = {UNID = 0, count = 0}
						end
						break
					end
				end
			end
		-- Drop chance is below 100, so give out the remainder to general loot accordingly
		else
			local remChance = 100 - lootTable.sumChance -- chance % left after singles
			local sumChance = 0

			local lootType = ""

			-- loop through item types till chance lands on the final chosen type
			for index, variable in pairs(lootTable) do
				if tostring(index) == "singles" then
					for i, v in pairs(variable) do
						if v.drop then
							local item = s_uItemProperties[tostring(v.UNID)]
							sumChance = sumChance + v.drop

							if rand <= sumChance then
								if tonumber(v.UNID) > 0 and item then
									if item.itemType == GameDefinitions.ITEM_TYPES.WEAPON or item.itemType == GameDefinitions.ITEM_TYPES.ARMOR or item.itemType == GameDefinitions.ITEM_TYPES.TOOL then
										lootItem = {UNID = tonumber(v.UNID), count = 1}
									elseif item.stackSize then
										lootItem = {UNID = tonumber(v.UNID), count = math.min(item.stackSize, v.quantity)}
									else
										lootItem = {UNID = tonumber(v.UNID), count = math.min(GameDefinitions.DEFAULT_STACK_SIZE, v.quantity)}
									end
									break
								end
							end
						else
							--Is this needed?
							lootItem = {UNID = 0, count = 0}
						end
					end

					-- If singles is the chosen loot item, then break out of the greater loop over general and custom loot
					if not Toolbox.isContainerEmpty(lootItem) then
						break
					end
				end
			end

			-- If there's still nothing in the loot item, then go through s_uItemProperties to find a general loot item (no singles item was found)
			if Toolbox.isContainerEmpty(lootItem) then
				if lootTable.sumGenerics > 0 then
					lootItem = getRandomLoot(lootTable, level)
				else -- Custom loot item was not selected and there are no generics available to roll
					lootItem = {UNID = 0, count = 0}
				end
			end
		end
	else -- No custom loot, so roll through the general loot
		if lootTable.sumGenerics > 0 then

			lootItem = getRandomLoot(lootTable, level)
		else
			lootItem = {UNID = 0, count = 0}
		end
	end

	return lootItem
end



getRandomLoot = function(lootTable, level)
	LOG_FUNCTION_CALL("getRandomLoot")
	
	
	local lootItem = {UNID = 0, count = 0}
	local rollPassLoot = {}

	for index, value in pairs(lootTable) do
		if tostring(value) == "true" and s_tGameItemTypes[tostring(index)] then
			for i, UNID in pairs(s_tGameItemTypes[index]) do
				if level and  (	index == GameDefinitions.ITEM_TYPES.WEAPON or 
									index == GameDefinitions.ITEM_TYPES.ARMOR or 
		 							index == GameDefinitions.ITEM_TYPES.TOOL or 
		 							index == GameDefinitions.ITEM_TYPES.BLUEPRINT) then


					local gameItem = s_uItemProperties[tostring(UNID)]
					if gameItem and gameItem.level and gameItem.level == level then
						table.insert(rollPassLoot, UNID)
					end
				else
					table.insert(rollPassLoot, UNID)
				end
			end
		end
	end

	if not Toolbox.isContainerEmpty(rollPassLoot) then
	
		--shuffle table of items
		local LOOT_COUNT = #rollPassLoot
		for i = 1, LOOT_COUNT do
			local temp = rollPassLoot[i]
			local randPassLoot = math.ceil(math.random() * LOOT_COUNT)
			rollPassLoot[i] = rollPassLoot[randPassLoot]
			rollPassLoot[randPassLoot] = temp
		end

		--Go through the now shuffled table, and rarityRoll each one until until once gains success
		for i, loot in ipairs(rollPassLoot) do
			
			local iLoot = tonumber(loot)
			if rarityRoll(iLoot) then
				
				local gameItem = s_uItemProperties[tostring(loot)]
				
				if gameItem.itemType == GameDefinitions.ITEM_TYPES.AMMO then
					local stackSize = gameItem.stackSize or 10
					lootItem = {	
						UNID	= iLoot, 
						count	= math.min(stackSize, 10)
					}
					break
				else
					lootItem = {	
						UNID	= iLoot, 
						count	= 1
					}
					break
				end
			end
		end
	end
	
	return lootItem
end
