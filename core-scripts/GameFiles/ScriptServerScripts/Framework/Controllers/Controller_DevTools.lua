--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Controller_DevTools.lua
Class.createClass("Controller_DevTools", "Controller")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox 
local Events			= _G.Events

-----------------------------------------------------------------
-- Local variables
-----------------------------------------------------------------
local MRU = {}
local devs = {}

function create( self )
	_super.create( self )
	Events.registerHandler( "PLAYER-ARRIVE",	self.playerInit )
	Events.registerHandler( "PLAYER-DEPART",	self.playerCleanup )

	Events.registerHandler( "SET-MRU",	function(event) if event.player~=nil then setMRU(event.player, event) end end )
	Events.registerHandler( "GET-MRU",	function(event) if event.player~=nil then Events.sendClientEvent("GET-MRU-RESP", {list=getMRU(event.player)}, event.player.ID) end end )
	Events.registerHandler( "RESET-MRU",function(event) if event.player~=nil then resetMRU(event.player) end end )
end

function playerInit( event )
	local player = event.player
	--Console.Write( Console.INFO, "Welcome " .. player.name )

	if player.perm==0 or player.perm==1 or player.perm==2 then
		loadMRU( player )
		loadDevUI( player )
		devs[player.ID] = true
	end
end

function playerCleanup( event )
	local player = event.player

	if devs[player.ID]~=nil then
		devs[player.ID] = nil
		saveMRU( player )
	end
end

function loadDevUI( player )
	kgp.playerLoadMenu( player.ID, "Client_DevTools.xml" )
end

function setMRU( player, item )
	--Console.Write( Console.DEBUG, "setMRU: player=" .. tostring(player.name) )
	if player==nil or player.ID==nil or player.name==nil then
		return
	end

	if MRU[player.name]==nil then
		MRU[player.name] = {}
	end

	if MRU[player.name][item.type]==nil then
		MRU[player.name][item.type] = {}
	end

	MRU[player.name][item.type][item.name] = item.time
	if devs[player.ID]~=nil then
		saveMRU( player )
	end
end

function getMRU( player )
	--Console.Write( Console.DEBUG, "getMRU: player=" .. tostring(player.name) )
	if player==nil or player.ID==nil or player.name==nil then
		return {}
	end

	-- Events.tellAllPlayers( "Development MRU.get(" .. user .. ")" )
	return MRU[player.name] or {}
end

function resetMRU( player )
	--Console.Write( Console.DEBUG, "resetMRU: player=" .. tostring(player.name) )
	if player==nil or player.ID==nil or player.name==nil then
		return
	end

	MRU[player.name] = nil
end

function loadMRU( player )
	--Console.Write( Console.DEBUG, "loadMRU: player=" .. tostring(player.name) )
	if player==nil or player.ID==nil then
		return
	end

	local MRUJSON = kgp.playerLoadData( player.name, kgp.gameGetCurrentGameId(), "MRU" )
	--Console.Write( Console.DEBUG, "loadMRU: " .. tostring(MRUJSON) )
	if MRUJSON~=nil and MRUJSON~="" and MRUJSON~="[]" and MRUJSON~="{\"list\":[]}" then
		local decoded = Events.decode(MRUJSON)
		if decoded~=nil then
			MRU[player.name] = decoded.list
		end
	end
end

function saveMRU( player )
	--Console.Write( Console.DEBUG, "saveMRU: player=" .. tostring(player.name) )
	local list = getMRU(player)
	if not Toolbox.isContainerEmpty(list) then
		kgp.playerSaveData( player.name, kgp.gameGetCurrentGameId(), "MRU", Events.encode{ list = list } )
	else
		kgp.playerSaveData( player.name, kgp.gameGetCurrentGameId(), "MRU", "" )
	end
end
