--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Controller_Spawn.lua
---------------------------------------------------------------------
-- Controller Spawn - Handles event APIs for adding and removing
-- Framework class functionality to spawned and instanced PIDs.
-- Implementation is "threaded" and load balanced.
---------------------------------------------------------------------
Class.createClass("Controller_Spawn", "Controller")

Class.import("Game")

-- Controller Exported Functions
Class.export({
	{ "registerThread", "thread" },
	{ "reportThreadUpdateDelta", "pendingID", "loadDelta" },
	-- inherited from Controller
	{ "config", "key", "value" }
})

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Console			= _G.Console
local Events			= _G.Events

---------------------------------------------------------------------
-- Local Function Definitions
---------------------------------------------------------------------
local createClassMap --()
local createThread --(count)
local getThreadPID --(spawnerPID, forDestroy)
local checkThreadLoads --(self)
local getSpawnerPeakLoad --(self, spawnData)
local getAssociatedLoad --(self, spawnData)
local getCurrentThreadLoad --()
local processSpawnQueue --()

---------------------------------------------------------------------
-- Local Constant Definitions
---------------------------------------------------------------------
local THREAD_MAX_COUNT = 20 -- Cap total spawn threads at 20
local THREAD_BATCH_COUNT = 5 -- The number of threads to create per batch
local THREAD_LOAD_SOFT_CAP = 100 -- Load soft cap, if all threads have reached this cap then spawn another batch
local TOTAL_LOAD_CAP = 2000

local GLID_SINGULARITY = 4276230 -- Tiny, collision-free object to attach script VMs for threads

local THREAD_WAIT = 0 -- Flag to indicate that we are generating more threads and should wait until they are ready

-- Table of events to be registered and accompanying handlers
-- NOTE: Only events needed post-Framework spinup should be here. If you need an event to respond while the Framework is still
-- offline, register for it in in create()
local CONTROLLER_EVENTS = {
	OBJECT_SPAWN 				   = "spawnObject", -- Event handlers - All spawn and instance events pass through this controller
	OBJECT_DESPAWN 				   = "despawnObject",
	OBJECT_DESPAWN_ALL 			   = "despawnObjectAll",
	OBJECT_INSTANCE_CLASS 		   = "instanceClass",
	OBJECT_INSTANCE_CLASS_FINALIZE = "instanceClassFinalize",
	OBJECT_REMOVE_CLASS 		   = "removeClass",
	OBJECT_UPDATE 				   = "updateObject",
	OBJECT_DELETE 				   = "deleteObject",
	OBJECT_RESPAWN 				   = "respawnObject",
	OBJECT_UPDATE_PLACEMENT_GLID   = "updatePlacementGLID",
	FRAMEWORK_LOG_LOAD 			   = "logLoad",
}

---------------------------------------------------------------------
-- Spawn Object Types
-- All types that need to wrap to Framework classes must be
-- defined here!
---------------------------------------------------------------------
local SPAWN_OBJECT_TYPES = { MONSTER = "monster",
							 ANIMAL = "animal",
							 PET = "pet",
							 SEED = "seed",
							 ACTOR = "actor",
							 ACTOR_PET = "actor_pet",
							 VENDOR_PET = "vendor_pet",
							 SHOP_VENDOR_PET = "shop_vendor_pet",
							 QUEST_GIVER_PET = "quest_giver_pet",
							 LOOT = "loot",
							 DOOR = "door",
							 QUEST_GIVER = "quest_giver",
							 SEAT = "seat",
							 SPAWNER_CHARACTER = "spawner_character",
							 SPAWNER_HARVESTABLE = "spawner_harvestable",
							 SPAWNER_LOOT = "spawner_loot",
							 SPAWNER_PLAYER = "spawner_player",
							 SHORTCUT = "shortcut",
							 CRAFTING_OBJECT = "campfire",
							 HARVESTABLE = "harvestable",
							 FLAG = "flag",
							 CLAIMFLAG = "claim",
							 EVENTFLAG = "eventflag",
							 TELEPORTER = "teleporter",
							 MOVER = "mover",
							 WAYPOINT = "waypoint",
							 VENDOR = "vendor",
							 REPAIR = "repair",
							 TRAP = "trap",
							 MEDIA = "media",
							 SHOP_VENDOR = "shop_vendor",
							 WHEELED_VEHICLE = "wheeled_vehicle",
							 PLACEABLE_LOOT = "placeable_loot",
							 PLACEABLE_LOOT_RIDER = "placeable_loot_rider",
							 PATH = "path",
							 PATH_WAYPOINT = "pathwaypoint",
							 ACTOR_RIDER = "actor_rider",
							 VENDOR_RIDER = "vendor_rider",
							 SHOP_VENDOR_RIDER = "shop_vendor_rider",
							 QUEST_GIVER_RIDER = "quest_giver_rider"
							}

---------------------------------------------------------------------
-- Local Variable Definitions
---------------------------------------------------------------------
local s_classMap = {} -- Wraps object types to class name and load weight
local s_threads = {} -- Table of all created threads
local s_threadCount = 0 -- Total number of threads created
local s_initialized = false -- First time initialization, set to true after first batch of threads created
local s_createCount = 0 -- Count of threads being created currently, decrements on thread registration
local s_spawnQueue = {} -- Queue of all items waiting for threads to be created, clears out when all ready
local s_pendingQueue = {}
local s_pendingUpdates = {}
local s_pendingID = 1
local s_controllerReady = false

---------------------------------------------------------------------
-- Create - Called on controller start-up
---------------------------------------------------------------------
function create(self)
	_super.create(self)
	
	-- Register Controller_Battle with Controller_Game
	Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Spawn", state = {["online"] = true}})
	
	-- Request for current controller state
	Events.registerHandler( "FRAMEWORK_QUERY_CONTROLLER_STATE", self.onControllerStateQuery, self )
	-- Register an activate event for this controller
	Events.registerHandler( "FRAMEWORK_ACTIVATE_CONTROLLER", self.onActivate, self )
	Events.registerHandler( "FRAMEWORK_DEACTIVATE_CONTROLLER", self.onDeActivate, self )
end

-- Request for current controller state
function onControllerStateQuery(self, event)
	local state = {online = true}
	if s_controllerReady then
		state.active = s_controllerReady
	end
	-- Inform Controller_Game that Spawn is finished activating
	Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Spawn", state = state})
end

-- Activate this controller
function onActivate(self, event)
	-- Time to activate?
	if event.controller and event.controller == "Spawn" then
		createClassMap() -- Associate object types with class data

		-- Assign custom GameItemLoad from Controller_Game
		if event.GameItemLoad then
			TOTAL_LOAD_CAP = tonumber(event.GameItemLoad) or TOTAL_LOAD_CAP
		end

		-- Log beginning of handler registration
		Game.logProgress{log = "Spawn - Registering event handlers"}
		
			-- Register all event handlers
		for event, handler in pairs(CONTROLLER_EVENTS) do
			Events.registerHandler(event, self[handler], self)
		end

		Game.logProgress{log = "Spawn - Event handlers registered"}

		-- Inform Controller_Game that Spawn is finished activating
		s_controllerReady = true
		Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Spawn", state = {["active"] = true}})
	end
end

-- De-activate this controller
function onDeActivate(self, event)
	-- Time to activate?
	if event.controller and event.controller == "Spawn" then
		Game.logProgress{log = "Spawn - Un-registering event handlers"}
		-- Unregister all un-needed event handlers
		for event, handler in pairs(CONTROLLER_EVENTS) do
			Events.unregisterHandler(event, self[handler], self)
		end
		Game.logProgress{log = "Spawn - Event handlers un-registered"}
		
		-- Inform Controller_Game that Spawn is finished activating
		s_controllerReady = false
		
		-- Inform Controller_Game that Spawn is finished de-activating
		Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Spawn", state = {active = false}})
	end
end

---------------------------------------------------------------------
-- Controller Exported Functions
---------------------------------------------------------------------
-- registerThread - Callback when a generated thread completes setup
---------------------------------------------------------------------
function registerThread(self, thread)
	if thread and thread.PID and s_threads[thread.PID] then
		s_threads[thread.PID].thread = thread
		s_createCount = s_createCount - 1
		if s_createCount == 0 then
			processSpawnQueue(self)
		end
	end
end
-- reportThreadUpdateDelta - Callback for update load check
---------------------------------------------------------------------
function reportThreadUpdateDelta(self, pendingID, loadDelta)
	local tPendingUpdate = s_pendingUpdates[pendingID]
	if tPendingUpdate == nil then return end
	tPendingUpdate.loadDelta = tPendingUpdate.loadDelta + loadDelta
	tPendingUpdate.updateCount = tPendingUpdate.updateCount - 1
	
	if tPendingUpdate.updateCount > 0 then
		return
	end
	
	local load, peakLoad = getCurrentThreadLoad()
	local updatePeak = peakLoad + tPendingUpdate.loadDelta
	local success = true -- updatePeak <= TOTAL_LOAD_CAP or tPendingUpdate.loadDelta <= 0
	if success then
		Events.sendEvent("THREAD_OBJECT_UPDATE", tPendingUpdate.event)
	end
	
	Events.sendEvent("OBJECT_UPDATE_RESPONSE", {success = success, pendingID = tPendingUpdate.pendingID})
	s_pendingUpdates[pendingID] = nil
end

---------------------------------------------------------------------
-- Controller Private Functions
---------------------------------------------------------------------
-- createClassMap - Associates object types with class data
-- Class associations MUST be configured here.
---------------------------------------------------------------------
function createClassMap()
	s_classMap = {}
	s_classMap[SPAWN_OBJECT_TYPES.MONSTER]  	  	   = {className = "Class_SpawnMonster", 		classWeight = 5}
	s_classMap[SPAWN_OBJECT_TYPES.ANIMAL]  	   	  	   = {className = "Class_Animal",   	    	classWeight = 4}
	s_classMap[SPAWN_OBJECT_TYPES.PET]  	   	  	   = {className = "Class_Pet",   	    		classWeight = 5}
	s_classMap[SPAWN_OBJECT_TYPES.SEED]  	   	  	   = {className = "Class_Seed",   	    		classWeight = 5}
	s_classMap[SPAWN_OBJECT_TYPES.ACTOR]  	   	  	   = {className = "Class_Actor",   	    		classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.ACTOR_PET]   	  	   = {className = "Class_ActorPet",	    		classWeight = 5}
	s_classMap[SPAWN_OBJECT_TYPES.VENDOR_PET]		   = {className = "Class_VendorPet",			classWeight = 5}
	s_classMap[SPAWN_OBJECT_TYPES.SHOP_VENDOR_PET]     = {className = "Class_ShopVendorPet",		classWeight = 5}
	s_classMap[SPAWN_OBJECT_TYPES.QUEST_GIVER_PET]     = {className = "Class_QuestGiverPet",		classWeight = 5}
	s_classMap[SPAWN_OBJECT_TYPES.LOOT]     	  	   = {className = "Class_ItemContainer",    	classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.DOOR]     	  	   = {className = "Class_Door", 		    	classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.SEAT]			   	   = {className = "Class_Seat", 		    	classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.SPAWNER_CHARACTER]   = {className = "Class_CharacterSpawner", 	classWeight = 5}
	s_classMap[SPAWN_OBJECT_TYPES.SPAWNER_HARVESTABLE] = {className = "Class_CharacterSpawner", 	classWeight = 5}
	s_classMap[SPAWN_OBJECT_TYPES.SPAWNER_LOOT]   	   = {className = "Class_LootSpawner", 	    	classWeight = 3}
	s_classMap[SPAWN_OBJECT_TYPES.SPAWNER_PLAYER]      = {className = "Class_SpawnPoint", 	    	classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.SHORTCUT]    	  	   = {className = "Class_Shortcut",				classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.CRAFTING_OBJECT]	   = {className = "Class_CraftingObject",		classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.HARVESTABLE]	  	   = {className = "Class_Harvestable",	    	classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.FLAG]	  	      	   = {className = "Class_Flag",	   				classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.CLAIMFLAG]	  	   = {className = "Class_ClaimFlag",	   		classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.EVENTFLAG]	  	   = {className = "Class_EventFlag",	   		classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.TELEPORTER]      	   = {className = "Class_Teleporter",			classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.MOVER]      		   = {className = "Class_Mover",				classWeight = 3}
	s_classMap[SPAWN_OBJECT_TYPES.WAYPOINT]      	   = {className = "Class_Waypoint",				classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.VENDOR]      	   	   = {className = "Class_Vendor",				classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.REPAIR]      	   	   = {className = "Class_RepairStation",		classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.TRAP]      	   	   = {className = "Class_Trap",					classWeight = 2}
	s_classMap[SPAWN_OBJECT_TYPES.QUEST_GIVER]         = {className = "Class_QuestGiver",			classWeight = 2}
	s_classMap[SPAWN_OBJECT_TYPES.MEDIA]         	   = {className = "Class_MediaPlayer",			classWeight = 3}
	s_classMap[SPAWN_OBJECT_TYPES.SHOP_VENDOR]         = {className = "Class_ShopVendor",			classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.WHEELED_VEHICLE]     = {className = "Class_WheeledVehicle",		classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.PLACEABLE_LOOT]	   = {className = "Class_PlaceableLoot",		classWeight = 0}
	s_classMap[SPAWN_OBJECT_TYPES.PLACEABLE_LOOT_RIDER]= {className = "Class_PlaceableLootRider",	classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.PATH]				   = {className = "Class_Path",					classWeight = 3}
	s_classMap[SPAWN_OBJECT_TYPES.PATH_WAYPOINT]	   = {className = "Class_PathWaypoint",			classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.ACTOR_RIDER]		   = {className = "Class_ActorRider",			classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.VENDOR_RIDER]		   = {className = "Class_VendorRider",			classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.SHOP_VENDOR_RIDER]   = {className = "Class_ShopVendorRider",		classWeight = 1}
	s_classMap[SPAWN_OBJECT_TYPES.QUEST_GIVER_RIDER]   = {className = "Class_QuestGiverRider",		classWeight = 1}
end

---------------------------------------------------------------------
-- createThread - Creates a number of threads based on the count
---------------------------------------------------------------------
function createThread(count)
	count = count or 1 -- count defaults to 1
	s_createCount = s_createCount + count
	for i = 1, count do
		-- generate a thread object and attach the wrapper script
		log("createThread :: i = "..tostring(i)..", s_threadCount = "..tostring(s_threadCount).." : calling kgp.objectGenerate")
		local threadPID = kgp.objectGenerate(GLID_SINGULARITY, -1000, -1000, -1000, 0, 0, 1)
		log("createThread :: i = "..tostring(i)..", s_threadCount = "..tostring(s_threadCount).." : returned from kgp.objectGenerate with PID = "..tostring(threadPID))
		if threadPID then
			s_threadCount = s_threadCount + 1
			s_threads[threadPID] = {thread = nil}
			log("createThread :: i = "..tostring(i)..", s_threadCount = "..tostring(s_threadCount)..", threadPID = "..tostring(threadPID).." : calling kgp.objectSetScript")
			kgp.objectSetScript(threadPID, "Framework_SpawnThread.lua")
			log("createThread :: i = "..tostring(i)..", s_threadCount = "..tostring(s_threadCount)..", threadPID = "..tostring(threadPID).." : returned from kgp.objectSetScript")
		end
	end
end

---------------------------------------------------------------------
-- getThreadPID - Returns a thread to be used to instantiate a class
-- If a thread is already associated with a PID, then it is returned.
-- If no threads are associated, then the one with the lowest load
-- is selected.  For removal, only the associated thread is returned.
---------------------------------------------------------------------
function getThreadPID(spawnerPID, forDestroy)
	-- Check if this PID is already associated with a thread, unless this is for a generally managed object or for destruction
	if spawnerPID ~= 0 or forDestroy then
		for threadPID, threadData in pairs(s_threads) do
			if threadData.thread then
				for key, value in pairs(threadData.thread.managedPIDs) do
					if tonumber(key) == spawnerPID then
						return threadPID -- return the thread, if associated with this operation
					end
				end
			end
		end
	end
	
	-- If this is for destruction and no threads were found, return nil
	if forDestroy then return nil end
	
	-- If we are creating threads, add to queue and run again when ready
	if s_createCount > 0 then return THREAD_WAIT end
	
	-- There are no threads in existance!  Queue up a batch and wait
	if s_threadCount == 0 then
		createThread(THREAD_BATCH_COUNT)
		return THREAD_WAIT
	end
	
	-- Unassociated PID, determine the thread with the lowest current load
	local nextThread = {PID = nil, load = 0, peakLoad = 0}
	for threadPID, threadData in pairs(s_threads) do
		if threadData.thread and (nextThread.PID == nil or threadData.thread.peakLoad < nextThread.peakLoad) then
			nextThread = {PID = threadPID, load = threadData.thread.threadLoad, peakLoad = threadData.thread.peakLoad}
		end
	end
	
	return nextThread.PID -- New thread to use
end

---------------------------------------------------------------------
-- checkThreadLoads - Evaluates the current load on all threads and
-- creates new threads if needed.
---------------------------------------------------------------------
function checkThreadLoads(self)
	if s_threadCount >= THREAD_MAX_COUNT then return end --No need to spin up more threads if we're already at max
	
	for threadPID, threadData in pairs(s_threads) do
		if threadData.thread == nil or threadData.thread.peakLoad < THREAD_LOAD_SOFT_CAP then
			return -- At least one thread is below max load, nothing to do here
		end
	end
	
	createThread(THREAD_BATCH_COUNT) -- All threads were past cap, create a new batch
end

---------------------------------------------------------------------
-- getSpawnerPeakLoad - Evaluates the total peak load of a class
---------------------------------------------------------------------
function getSpawnerPeakLoad(self, spawnData)
	if spawnData.behavior and string.find(spawnData.behavior, "spawner_") then
		if spawnData.behavior == "spawner_loot" then
			return s_classMap[SPAWN_OBJECT_TYPES.LOOT].classWeight
		else
			local spawnInput = spawnData.input and spawnData.input.spawnInput
			if spawnInput and spawnInput.behavior then
				local tClassInfo = s_classMap[spawnInput.behavior]
				if tClassInfo then
					return tClassInfo.classWeight * (spawnData.input.maxSpawns or 1)
				end
			end
		end
	end

	return 0
end

---------------------------------------------------------------------
-- getAssociatedLoad - Evaluates the load of objects created with the
-- placement of the current object
---------------------------------------------------------------------
function getAssociatedLoad(self, spawnData)
	local associatedLoad = 0
	if spawnData.input and spawnData.input.associated then
		for behavior, count in pairs(spawnData.input.associated) do
			if s_classMap[behavior] then
				associatedLoad = associatedLoad + (s_classMap[behavior].classWeight * count)
			end
		end
	end

	return associatedLoad
end

---------------------------------------------------------------------
-- getCurrentThreadLoad - Returns the current and peak load of all
-- spawn threads.
---------------------------------------------------------------------
function getCurrentThreadLoad()
	local load = 0
	local peakLoad = 0
	for threadPID, threadData in pairs(s_threads) do
		if threadData and threadData.thread then
			load = load + (threadData.thread.threadLoad or 0)
			peakLoad = peakLoad + (threadData.thread.peakLoad or 0)
		end
	end
	
	return load, peakLoad
end

---------------------------------------------------------------------
-- Event Handlers - These APIs validate required parameters and then
-- are forwarded to the proper thread.
---------------------------------------------------------------------
-- spawnObject - Spawns an object and attaches class functionality.
-- Input: spawnType - The type of this object (defined as a constant)
--		  spawnerPID - The PID of the spawning object.
--        GLID - The GLID of the object to spawn.
--		  location{x, y, z} - The location to spawn this object.
--		  spawnInput - Optional table to pass to class create.
---------------------------------------------------------------------
function spawnObject(self, event)
	event.spawnType = string.lower(tostring(event.spawnType))
	if s_classMap[event.spawnType] == nil then Console.Write(Console.ERROR, "Spawn type validation failed!") return end
	if event.spawnerPID == nil then Console.Write(Console.ERROR, "Spawner PID validation failed!") return end
	if event.GLID == nil then Console.Write(Console.ERROR, "GLID validation failed!") return end
	if event.location == nil or event.location.x == nil or event.location.y == nil or event.location.z == nil then Console.Write(Console.ERROR, "Location validation failed!") return end
	if event.spawnInput == nil then event.spawnInput = {} end
	if event.spawnType == "loot" and event.player and event.player.ID then
		event.spawnInput.attackerID = event.player.ID
	end
	local threadPID = getThreadPID(event.spawnerPID)
	if threadPID then
		if threadPID ~= THREAD_WAIT then
			local tClassInfo = s_classMap[event.spawnType]
			event.spawnClass = tClassInfo.className
			event.classWeight = tClassInfo.classWeight
			-- This will update automatically, but need to apply the weight here to have it instantly reflected
			s_threads[threadPID].thread.threadLoad = s_threads[threadPID].thread.threadLoad + tClassInfo.classWeight
			Events.sendEvent("THREAD_OBJECT_SPAWN", event, threadPID)
			checkThreadLoads(self)
		else
			table.insert(s_spawnQueue, event)
		end
	end
end

---------------------------------------------------------------------
-- despawnObject - Despawns a previously generated object
-- Input: spawnerPID - The PID of the spawning object.
--        despawnPID (optional) - The PID of the generated object to despawn.
---------------------------------------------------------------------
function despawnObject(self, event)
	if event.spawnerPID == nil then Console.Write(Console.ERROR, "Spawner PID validation failed!") return end
	if event.spawnerPID == 0 and event.despawnPID == nil then Console.Write(Console.ERROR, "Spawner despawn PID was invalid!") end
	
	local threadKey = event.spawnerPID
	if threadKey == 0 then threadKey = event.despawnPID end
	local threadPID = getThreadPID(threadKey, true)
	if threadPID and threadPID ~= THREAD_WAIT then
		Events.sendEvent("THREAD_OBJECT_DESPAWN", event, threadPID)
	end
end

---------------------------------------------------------------------
-- despawnObjectAll - Despawns all objects from a specific spawner
-- Input: spawnerPID - The PID of the spawning object.
---------------------------------------------------------------------
function despawnObjectAll(self, event)
	event.despawnAll = true
	despawnObject(self, event)
end

---------------------------------------------------------------------
-- instanceClass - Attaches class functionality to a given PID
---------------------------------------------------------------------
function instanceClass(self, event)
	event.behavior = string.lower(tostring(event.behavior))

	local tClassInfo = s_classMap[event.behavior]
	if tClassInfo == nil then Console.Write(Console.ERROR, "Behavior type validation failed! ("..tostring(behavior)..")") return end
	if event.PID == nil then Console.Write(Console.ERROR, "Object PID validation failed!") return end
	if event.input == nil then event.input = {} end
	
	if event.PID == 0 then
		self:instanceClassPending(event)
		return
	end
	
	local threadPID = getThreadPID(event.PID)
	if threadPID then
		if threadPID ~= THREAD_WAIT then
			event.instanceClass = tClassInfo.className
			event.classWeight = tClassInfo.classWeight
			event.peakWeight = event.classWeight + getSpawnerPeakLoad(self, event)
			-- This will update automatically, but need to apply the weight here to have it instantly reflected
			s_threads[threadPID].thread.threadLoad = s_threads[threadPID].thread.threadLoad + event.classWeight
			s_threads[threadPID].thread.peakLoad = s_threads[threadPID].thread.peakLoad + event.peakWeight
			Events.sendEvent("THREAD_OBJECT_INSTANCE_CLASS", event, threadPID)
			checkThreadLoads(self)
		else
			table.insert(s_spawnQueue, event)			
		end
	end
end

---------------------------------------------------------------------
-- instanceClassPending - Analyses load and determines if an object
-- may be placed.  Returns load to inventory controller.
---------------------------------------------------------------------
function instanceClassPending(self, event)
	if event.pendingID == nil then Console.Write(Console.ERROR, "instanceClassPending called, but pending ID was nil!") return end
	
	event.instanceClass = s_classMap[event.behavior].className
	event.classWeight = s_classMap[event.behavior].classWeight
	event.peakWeight = event.classWeight + getSpawnerPeakLoad(self, event) + getAssociatedLoad(self, event)
	
	local load, peakLoad = getCurrentThreadLoad()
	local pendingSuccess = false
	
	if peakLoad + event.peakWeight <= TOTAL_LOAD_CAP then
		s_pendingQueue[event.pendingID] = event
		pendingSuccess = true
	end
	
	Events.sendEvent("OBJECT_INSTANCE_PENDING", {pendingID = event.pendingID, currentLoad = load, peakLoad = peakLoad, maxLoad = TOTAL_LOAD_CAP, pendingSuccess = pendingSuccess})
end

---------------------------------------------------------------------
-- instanceClassFinalize - Attaches class functionality to a given PID
-- that is in the pending queue
---------------------------------------------------------------------
function instanceClassFinalize(self, event)
	Debug.printTableError("event", event)
	if event.pendingID == nil then Console.Write(Console.ERROR, "instanceClassFinalize called, but pending ID was nil!") return end
	if event.PID == nil or event.PID == 0 then Console.Write(Console.ERROR, "instanceClassFinalize called, but the PID provided was invalid!") return end
	local tPending = s_pendingQueue[event.pendingID]
	if tPending == nil then Console.Write(Console.ERROR, "instanceClassFinalized called, but given ID was not pending ("..tostring(event.pendingID)..")!") return end
	
	tPending.PID = event.PID
	self:instanceClass(tPending)
	s_pendingQueue[event.pendingID] = nil
end


---------------------------------------------------------------------
-- removeClass - Remove class functionality from a given PID
---------------------------------------------------------------------
function removeClass(self, event)
	if event.PID == nil then Console.Write(Console.ERROR, "Object PID validation failed!") return end
	
	local threadPID = getThreadPID(event.PID, true)
	if threadPID and threadPID ~= THREAD_WAIT then
		Events.sendEvent("THREAD_OBJECT_REMOVE_CLASS", event, threadPID)
	end
end

---------------------------------------------------------------------
-- updateObject - Update managed objects on threads by UNID
---------------------------------------------------------------------
function updateObject(self, event)
	--TODO: Event parameter validation
	if s_classMap[event.behavior] then
		local itemInfo = {behavior = event.behavior, input = event}
		event.classWeight = s_classMap[event.behavior].classWeight
		event.peakWeight = event.classWeight + getSpawnerPeakLoad(self, itemInfo)

		if string.find(event.behavior, "spawner_") and s_threadCount > 0 then
			s_pendingUpdates[s_pendingID] = {
				updateCount		= s_threadCount,
				loadDelta		= 0,
				pendingID		= event.pendingID,
				event			= event,
			}
			event.pendingID = s_pendingID
			s_pendingID = s_pendingID + 1
			Events.sendEvent("THREAD_OBJECT_UPDATE_CHECK", event)
			return
		end
	end
	
	Events.sendEvent("THREAD_OBJECT_UPDATE", event)
	Events.sendEvent("OBJECT_UPDATE_RESPONSE", {success = true, pendingID = event.pendingID})
end

---------------------------------------------------------------------
-- deleteObject - Delete managed objects on threads by UNID
---------------------------------------------------------------------
function deleteObject(self, event)
	--TODO: Event parameter validation
	event.delete = true
	Events.sendEvent("THREAD_OBJECT_UPDATE", event, threadPID)
end

---------------------------------------------------------------------
-- respawnObject - Despawns objects on a spawner for immediate respawning
---------------------------------------------------------------------
function respawnObject(self, event)
	--TODO: Event parameter validation
	Events.sendEvent("THREAD_OBJECT_RESPAWN", event, threadPID)
end

---------------------------------------------------------------------
-- updatePlacementGLID - Updates the placed GLID of an object by PID
---------------------------------------------------------------------
function updatePlacementGLID(self, event)
	if tonumber(event.PID) == nil then Console.Write(Console.ERROR, "Object PID validation failed!") return end
	if tonumber(event.GLID) == nil then Console.Write(Console.ERROR, "New GLID validation failed!") return end
	
	local threadPID = getThreadPID(event.PID, true)
	if threadPID and threadPID ~= THREAD_WAIT then
		Events.sendEvent("THREAD_OBJECT_UPDATE_PLACEMENT_GLID", event, threadPID)
	end
end

---------------------------------------------------------------------
-- processSpawnQueue - Process items in the spawn queue
---------------------------------------------------------------------
function processSpawnQueue(self)
	for index, spawnData in pairs(s_spawnQueue) do
		if s_createCount > 0 then break end
		if spawnData._type == "OBJECT_SPAWN" then
			self:spawnObject(spawnData)
		elseif spawnData._type == "OBJECT_INSTANCE_CLASS" then
			self:instanceClass(spawnData)
		end
		
		s_spawnQueue[index] = nil
	end
end

---------------------------------------------------------------------
-- logLoad - Prints out load information to the script log
---------------------------------------------------------------------
function logLoad(self)
	local loadLog = "\n********************************\n"
	local threadCount = 0
	local currentLoad = 0
	local peakLoad = 0
	
	for threadPID, threadData in pairs(s_threads) do
		if threadData.thread then
			threadCount = threadCount + 1
			loadLog = loadLog.."Spawn Thread "..tostring(threadCount)..": currentLoad = "..tostring(threadData.thread.threadLoad or 0)..", peakLoad = "..tostring(threadData.thread.peakLoad or 0).."\n"
			currentLoad = currentLoad + (threadData.thread.threadLoad or 0)
			peakLoad = peakLoad + (threadData.thread.peakLoad or 0)
		end
	end
	loadLog = loadLog.."********************************\n"
	loadLog = loadLog.."Total: currentLoad = "..tostring(currentLoad)..", peakLoad = "..tostring(peakLoad).." Max Load = "..tostring(TOTAL_LOAD_CAP).."\n"
	loadLog = loadLog.."********************************"
	
	log(loadLog)
end