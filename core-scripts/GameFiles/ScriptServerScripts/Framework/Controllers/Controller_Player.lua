--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Controller_Player.lua
include("Lib_SaveLoad.lua")
include("Lib_GameDefinitions.lua")
include("Lib_ZoneInfo.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local Debug				= _G.Debug
local GameDefinitions	= _G.GameDefinitions
local Console			= _G.Console
local SaveLoad			= _G.SaveLoad
local Events			= _G.Events
local Players			= _G.Players
local Class				= _G.Class
local ZoneInfo 			= _G.ZoneInfo

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
Class.createClass("Controller_Player", "Controller")

Class.useBehaviors({
	"Behavior_Sound"
})

Class.import("Game")


-----------------------------------------------------------------
-- Debugging Helpers
-----------------------------------------------------------------
local LOG_FUNCTION_CALL, LOG_FUNCTION_CALL_FILTER_ID = Debug.generateCustomLog("Controller_Player - ")
Debug.enableLogForFilter(LOG_FUNCTION_CALL_FILTER_ID, false) -- Set this to true if you want to get a complete log of Controller_Player function calls


-----------------------------------------------------------------
-- Constant definitions
-----------------------------------------------------------------

-- Table of events to be registered and accompanying handlers
-- NOTE: Only events needed post-Framework spinup should be here. If you need an event to respond while the Framework is still
-- offline, register for it in create()
local tCONTROLLER_EVENTS = {
	["PLAYER-DEPART"]						= "onPlayerDeparted",
	FRAMEWORK_SAVE_PLAYER_DATA				= "onPlayerDataSaveRequested",
	
	FRAMEWORK_LOOT_ACQUIRED					= "onLootAcquired",
	PLAYER_LOAD_INVENTORY					= "onPlayerInventoryLoadingRequested",
	REQUEST_PLAYER_INVENTORY				= "onPlayerInventoryRequested",
	
	QUESTS_INVALIDATED						= "onQuestsInvalidated",
	REQUEST_COMPLETE_PLAYER_QUEST_UPDATES	= "onCompleteQuestUpdateRequested",
	REQUEST_PLAYER_QUESTS					= "onPlayerQuestsRequested",
	REQUEST_PLAYER_TRACKED_QUEST			= "onPlayerTrackedQuestRequested",
	UPDATE_PLAYER_TRACKED_QUEST				= "onPlayerTrackedQuestUpdated",
	UPDATE_PLAYER_QUEST_DATA				= "onPlayerQuestDataUpdated",
	UPDATE_PLAYER_QUEST_PROGRESS			= "onPlayerQuestProgressUpdated",
	FRAMEWORK_SET_PLAYER_QUEST_ICONS		= "onPlayerQuestIconsUpdateRequested",		--> Note this may actually be redundant since onPlayerQuestProgressUpdated does the same thing.
	FRAMEWORK_CLEAR_PLAYER_QUEST_ICONS		= "onPlayerQuestIconsClearingRequested",
	REQUEST_QUEST_ICON_ASSIGNMENT			= "onPlayerQuestIconAssignmentRequested",
	FRAMEWORK_QUEST_PLAY_SOUND				= "playCompletionSound",
	
	UPDATE_PLAYER_INVENTORY_SLOTS			= "onPlayerInventorySlotsUpdated",
	UPDATE_PLAYER_ARMORY_SLOTS				= "onPlayerArmorySlotsUpdated",
	UPDATE_PLAYER_BLUEPRINT_SLOTS			= "onPlayerBlueprintSlotsUpdated",
	UPDATE_PLAYER_ARMOR_DURABILITY			= "onPlayerArmorDurabilityUpdated",
	
	FRAMEWORK_REQUEST_BR_HUD_INFO			= "onBottomRightHUDInfoRequested",			-- Register for player mode change menu opened
	
	PLAYER_EQUIP_ITEM						= "onItemEquipped",
	UNEQUIP_PLAYER_ITEM						= "onItemUnequipped",
	UPDATE_PLAYER_HIDE_ARMOR				= "onArmorHidingUpdated",
	FRAMEWORK_JUMPSUIT_MODIFIED				= "onJumpsuitSettingModified",
	
	WOK_ARM_ITEM							= "WOKArmItem",
	
	FRAMEWORK_REDEEM_GEM_CHEST				= "onGemChestCashInRequested",
	FRAMEWORK_CONVERT_WORLD_COINS			= "onWorldCoinExchangeRequested",
	FRAMEWORK_WEB_CALL_RESPONSE				= "onWebCallResponded",
	
	REQUEST_PLAYER_INVENTORIES_VALIDATION	= "onPlayerInventoriesValidationRequested",
	HIDE_PLAYER								= "hidePlayer",
	SHOW_PLAYER								= "showPlayer",
	UPDATE_PLAYER_TITLE						= "onPlayerTitleUpdated"
}

local COMPLETION_SOUND = 4646088
local COMPLETION_EFFECT = 4642096

-----------------------------------------------------------------
-- Local variable definitions
-----------------------------------------------------------------
local s_uItemProperties						--> SharedData holding properties for all items that can be created in the current world.
local s_uItemInstances						--> SharedData holding a list of all item instances.
local s_uPlayers							--> SharedData holding a list of all players.

local s_iZoneInstanceID
local s_iZoneType

local s_bIsControllerReady		= false

local s_bIsJumpSuitOn			= false		-- Should the jumpsuit be auto-equipped in this world?

local s_tDirtyInventories		= {}
local s_tArmorHiddenStates		= {}		-- Should armor be hiding?
local s_tActiveQuests			= {}		-- Table of players, their quests and completion state for said quest
local s_tPlayerIcons			= {}		-- Table of players' quest icons by PID
local s_tPlayerIconMessage		= {}		-- Table if message has shown for quest completion state
local s_nextCallCID				= 1			-- The caller ID of the next web call

local s_tDefaultInventory		= {
	inventory = {},
	armor = {},
	blueprints = {}
} -- Default player Inventory. Populated with loadDefaultInventory()


local s_tPendingWebCalls = {}




-----------------------------------------------------------------
-- Local function declarations
-----------------------------------------------------------------
local sendInventoryToClient			-- function(playerName)

local equipInventory				-- function(playerName)
local validatePlayerTimedVends		-- function(player)
local validatePlayerInventory		-- function(playerInventory)
local validatePlayerInventories		-- function()
local populatePlayerInventory		-- function(player, playerInventory)
local populatePlayerArmor			-- function(player, playerArmor)
local populatePlayerBlueprints		-- function(player, playerBlueprints)

local loadPlayerQuests				-- function(event)

local savePlayerInventory			-- function(playerName)
local savePlayerQuests				-- function(playerName)

local recordQuestCompletion			-- function(playerName, completeQuest)

local removeQuests					-- function(tQuestUNIDs)
local sendQuestsToClient			-- function(playerName)

local updatePlayerQuestIcons		-- function(player, resetIcons)

local setQuestIcon					-- function(player, object)
local getQuestIcon					-- function(quests, player)

local calculateArmorRating			-- function(playerName)
local updateDirtyInventories		-- function()
local deductItemAtSlot				-- function(player, slot)
local deductItemByUNID				-- function(player, UNID, count)

local logInventorySizeError			-- function(user, functionName)




-----------------------------------------------------------------
-- Main entry to and exit from this controller
-----------------------------------------------------------------
function create(self)
	LOG_FUNCTION_CALL("create")
	_super.create(self)
	
	s_uItemProperties	= SharedData.new("gameItemProperties", false)
	s_uItemInstances	= SharedData.new("items", false)
	s_uPlayers			= SharedData.new("players", false)
	
	
	-- Register an activate event for this controller
	Events.registerHandler( "FRAMEWORK_ACTIVATE_CONTROLLER", self.onActivate, self )
	Events.registerHandler( "FRAMEWORK_DEACTIVATE_CONTROLLER", self.onDeactivate, self )
	Events.registerHandler( "FRAMEWORK_QUERY_CONTROLLER_STATE", self.onControllerStateQueried, self )
	Events.registerHandler( "FRAMEWORK_RETURN_ZONE_DATA", self.onZoneDataReturned, self)
	
	-- Populates s_tDefaultInventory
	loadDefaultInventory()
	
	
	-- Register Controller_Player with Controller_Game
	Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Player", state = {online = true}})
end

function stop(self, user)
	LOG_FUNCTION_CALL("stop")
	updateDirtyInventories(self)
	
	_super.stop(self, user)
end


-----------------------------------------------------------------
-- Events
-----------------------------------------------------------------

function onActivate(self, event)
	LOG_FUNCTION_CALL("onActivate")
	
	if event.controller and event.controller == "Player" then
		s_bIsJumpSuitOn = event.jumpsuit
		
		for event, handler in pairs(tCONTROLLER_EVENTS) do
			Events.registerHandler(event, self[handler], self)
		end
		
		s_bIsControllerReady = true
		Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Player", state = {active = true}})
	end
end

function onDeactivate(self, event)
	LOG_FUNCTION_CALL("onDeactivate")
	
	-- Time to activate?
	if event.controller and event.controller == "Player" then
		
		s_bIsControllerReady = false
		
		for event, handler in pairs(tCONTROLLER_EVENTS) do
			Events.unregisterHandler(event, self[handler], self)
		end
		
		-- Un-equip any equipped Framework item and TODO: default clothing
		for name, sharedData in pairs(s_uPlayers) do
			if sharedData then
				kgp.playerSetEquipped(sharedData.ID, "", "KANEVA")
			end
		end
		
		-- Inform Controller_Game that Controller_Player is finished de-activating
		Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Player", state = {active = false}})
	end
end


-- Request for current controller state
function onControllerStateQueried(self, event)
	local state = {online = true}
	if s_bIsControllerReady then
		state.active = s_bIsControllerReady
	end
	
	-- Inform Controller_Game that Spawn is finished activating
	Events.sendEvent("FRAMEWORK_CONTROLLER_STATE", {controller = "Player", state = state})
end


function onZoneDataReturned(self, event)
	LOG_FUNCTION_CALL("onZoneDataReturned")
	
	s_iZoneInstanceID = event.zoneInstanceId
	s_iZoneType = event.zoneType
end



-- When a a player leaves the zone
function onPlayerDeparted(self, event)
	LOG_FUNCTION_CALL("onPlayerDeparted")
	if self == nil then log("Controller_Player - onPlayerDeparted() self is nil."); return end
	if event == nil then log("Controller_Player - onPlayerDeparted() event is nil."); return end
	
	-- TODO: What cleanup needs to happen here?
	local sharedData = event.player
	
	if sharedData == nil then log("Controller_Player - onPlayerDeparted() event.player is nil."); return end
	
	s_tPlayerIcons[event.player.name] = {}
	
	-- Save inventory if dirty
	if sharedData and sharedData.name and s_tDirtyInventories[sharedData.name] then
		SaveLoad.savePlayerData(sharedData.name, "playerInventory", Events.encode(s_tDirtyInventories[sharedData.name]))
		s_tDirtyInventories[sharedData.name] = nil
	end
end


function onPlayerDataSaveRequested(self, event)
	LOG_FUNCTION_CALL("onSavePlayerData")
	updateDirtyInventories(self)
end


-- Sent from Framework_InventoryHandler when a piece of loot has been acquired
function onLootAcquired(self, event)
	LOG_FUNCTION_CALL("onLootAcquired")
	
	if event.player and s_iZoneInstanceID and s_iZoneType then
		local loot = s_uItemProperties[tostring(event.itemUNID)]
		
		-- TODO: PJD - Temp block to record old metrics to compare with new, remove along with lootSource parameter when done
		local lootSource = event.lootSource
		if loot and lootSource and (event.itemCount or 0) > 0 and (lootSource == "Vendor" or lootSource == "Crafting" or lootSource == "Chest") then
			local input = {	
				username 	  	= event.player.name,
				zoneInstanceId	= s_iZoneInstanceID,
				zoneType 	 	= s_iZoneType,
				gameItemId 	 	= event.itemUNID,
				gameItemGlid 	= loot.GIGLID,
				itemType 	  	= loot.itemType,
				behavior 	  	= loot.behavior,
				lootSource 	  	= event.lootSource
			}

			Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", {input = input, callType = "METRIC_LOOT"})
		end
		-- End temp block
		
		if loot then			
			-- Pull the server time, the player's last loot time, cumulative loot rate diffs and total loot entries measured
			local currTime = kgp.gameGetCurrentTime() or 0
			local sourcePID = event.sourcePID
			local sourceObject = s_uItemInstances[tostring(sourcePID)]
			local sourceUNID = sourceObject and sourceObject.UNID
			local spawnerPID = nil
			
			if sourceObject ~= nil and sourceUNID == nil then
				local spawnerObject = s_uItemInstances[tostring(sourceObject.spawnerPID)]
				if spawnerObject then
					sourceUNID = spawnerObject.UNID
					spawnerPID = sourceObject.spawnerPID
				end
			end		

			local sourceItem = s_uItemProperties[tostring(sourceUNID)]			
			
			if (event.itemCount or 0) > 0 then
			   	if event.origCount then
					log("ERROR DETECTED - ED-6965 : Attempt to add item with invalid stack size of "..tostring(event.origCount))
					Debug.printTableError("ERROR DETECTED - ED-6965 : event", event, 2)
				end		

				local input = {
					username			= event.player.name,
					lootItemName		= loot.name,
					lootGIID			= event.itemUNID,
					lootItemType		= loot.itemType,
					lootItemBehavior	= loot.behavior,
					sourceItemName		= (sourceItem and sourceItem.name),
					sourceGIID			= sourceUNID,
					sourceItemType		= (sourceItem and sourceItem.itemType),
					sourceItemBehavior	= (sourceItem and sourceItem.behavior),
					sourcePID			= event.sourcePID,
					spawnerPID			= spawnerPID,
					zoneInstanceId		= s_iZoneInstanceID,
					zoneType			= s_iZoneType,
					quantityReceived	= event.itemCount,
					timestamp			= currTime,
					lootSource			= event.lootSource,
					recipeGIID			= event.recipeGIID,
					transactionID		= event.transactionID
				}
			   
			    log("METRIC - loot_received - "..Events.encode(input))
				kgp.scriptRecordMetric("loot_received", input)

				-- TODO-GEMS: Temp until we get the above APIs. Then use below
				event.player.lootRate = 30
				--event.player.lootRate = totalLootTimeDiffs / totalLootCounts
			elseif (event.itemCount or 0) < 0 then
				local input = {	
					username 	  	  	= event.player.name,
					lootItemName   	  	= loot.name,
					lootGIID		  	= event.itemUNID,
					lootItemType		= loot.itemType,
					lootItemBehavior	= loot.behavior,
					zoneInstanceId 	  	= s_iZoneInstanceID,
					zoneType 	  	  	= s_iZoneType,
					quantityRemoved		= -event.itemCount,
					timestamp			= currTime,
					lootSource			= event.lootSource,
					transactionID		= event.transactionID
				}
			   	if event.origCount then
					log("ERROR DETECTED - ED-6965 : Attempt to remove item with invalid stack size of "..tostring(event.origCount))
					Debug.printTableError("ERROR DETECTED - ED-6965 : input", input)
				end
				
				if event.questUNID and sourceItem then
					local questItem = s_uItemProperties[tostring(event.questUNID)]
					if questItem and questItem.rewardItem then
						local rewardUNID = tonumber(questItem.rewardItem.rewardUNID)
						if rewardUNID and rewardUNID > 0 then
							rewardItem = s_uItemProperties[tostring(rewardUNID)]
							if rewardItem then
								input.targetItemName = rewardItem.name
								input.targetItemGIID = rewardUNID
								input.targetItemType = rewardItem.itemType
								input.targetItemBehavior = rewardItem.behavior
								input.targetItemCount = questItem.rewardCount or 1
							end
						end
					end
				end
				
				if event.lootSlot and sourceItem then
					local purchaseUNID = sourceItem.trades and sourceItem.trades[tostring(event.lootSlot)] and sourceItem.trades[tostring(event.lootSlot)].output
					if purchaseUNID then
						local purchaseItem = s_uItemProperties[tostring(purchaseUNID)]
						if purchaseItem then
							input.targetItemName = purchaseItem.name
							input.targetItemGIID = purchaseUNID
							input.targetItemType = purchaseItem.itemType
							input.targetItemBehavior = purchaseItem.behavior
							input.targetItemCount = sourceItem.trades[tostring(event.lootSlot)].outputCount or 1
						end
					end
				end
				
				log("METRIC - loot_removed - "..Events.encode(input))
				kgp.scriptRecordMetric("loot_removed", input)
			end
		end
	end
end



-- Loads the player's inventory for this world
function onPlayerInventoryLoadingRequested(self, event)
	LOG_FUNCTION_CALL("onPlayerInventoryLoadingRequested")
	
	if self == nil then log("Controller_Player - onPlayerInventoryLoadingRequested() self is nil."); return end
	if event == nil then log("Controller_Player - onPlayerInventoryLoadingRequested() event is nil."); return end
	if event.user == nil then log("Controller_Player - onPlayerInventoryLoadingRequested() event.user is nil."); return end
	local user = Players.determineUser(event.user, s_uPlayers)
	if user == nil then log("Controller_Player - onPlayerInventoryLoadingRequested() user could not be extracted from determineUser()."); return end
	local userName = user.name
	if userName == nil then log("Controller_Player - onPlayerInventoryLoadingRequested() user.name is nil"); return end


	-- Validate this player's timed vends
	validatePlayerTimedVends(user)

	-- Load inventory from DB
	local savedInventory = SaveLoad.loadPlayerData(user.name, "playerInventory")
	
	local inventoryModified = false
	-- Does this user have an inventory?
	if savedInventory and savedInventory ~= "[]" then
		savedInventory = Events.decode(savedInventory)
		
		inventoryModified, savedInventory = validatePlayerInventory(savedInventory)
	else
		savedInventory = s_tDefaultInventory
	end
	
	local uPlayer = s_uPlayers[userName]
	
	populatePlayerInventory(uPlayer, savedInventory.inventory)
	populatePlayerArmor(uPlayer, savedInventory.armor)
	populatePlayerBlueprints(uPlayer, savedInventory.blueprints)
	
	if inventoryModified then
		-- TEMP-FUNCTION - Checks if the player's inventory is no longer of size 45
		logInventorySizeError(user, "loadPlayerInventory()")
		savePlayerInventory(user.name)
	end

	uPlayer.armorRating, uPlayer.baseArmorRating = calculateArmorRating(user.name)
	Events.sendClientEvent("FRAMEWORK_SET_ARMOR", {armorRating = uPlayer.baseArmorRating}, uPlayer.ID)

	equipInventory(user.name)
	
	local eventData = {
		type 			  = "Player",
		ID 			   	  = user.name,
		alive 			  = uPlayer.alive,
		mode 			  = uPlayer.mode,
		pvpEnabled 	   	  = uPlayer.pvpEnabled,
		playerDestruction = uPlayer.playerDestruction,
		health 		   	  = uPlayer.health,
		maxHealth 		  = uPlayer.maxHealth,
		armorRating 	  = uPlayer.baseArmorRating,
		targetable		  = "true"
	}
	
	-- Register this player as a new targetable player
	Events.sendClientEvent("FRAMEWORK_REGISTER_BATTLE_OBJECT", eventData)
																
	loadPlayerQuests(event) -- Load quests
end



function onPlayerInventoryRequested(self, event)
	LOG_FUNCTION_CALL("onPlayerInventoryRequested")
	
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerInventoryRequested() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerInventoryRequested() event is nil."); return end
	local player = event.player
	if player == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerInventoryRequested() event.player is nil."); return end
	local sPlayerName = player.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerInventoryRequested() Player with ID of "..tostring(event.player.ID).." has a NIL name!"); return end
	
	if player.inventory then
		sendInventoryToClient(sPlayerName)
	end
end


function onQuestsInvalidated(self, event)
	LOG_FUNCTION_CALL("onQuestsInvalidated")
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onQuestsInvalidated() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onQuestsInvalidated() event is nil."); return end
	if event.questUNIDs == nil then Console.Write(Console.DEBUG, "Controller_Player - onQuestsInvalidated() event.questUNIDs is nil."); return end
	
	removeQuests(event.questUNIDs)
end


-- Update the player's quests
-- Copied from updatePlayerInventory, above
-- I don't understand the table validation with the break, but duplicated it here to keep it consistent
function onPlayerQuestsRequested(self, event)
	LOG_FUNCTION_CALL("onPlayerQuestsRequested")
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestsRequested() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestsRequested() event is nil."); return end
	local player = event.player
	if player == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestsRequested() event.player is nil."); return end
	local sPlayerName = player.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestsRequested() Player with ID of "..tostring(event.player.ID).." has a NIL name!"); return end
	
	-- Ensure we have a valid quest log
	if player.quests then
		sendQuestsToClient(sPlayerName)
	end
end





function onPlayerTrackedQuestRequested(self,event)
	LOG_FUNCTION_CALL("onPlayerTrackedQuestRequested")
	
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerTrackedQuestRequested() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerTrackedQuestRequested() event is nil."); return end
	local player = event.player
	if player == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerTrackedQuestRequested() event.player is nil."); return end
	local sPlayerName = player.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerTrackedQuestRequested() Player with ID of "..tostring(event.player.ID).." has a NIL name!"); return end
	local playerID = player.ID
	
	local questTracked = SaveLoad.loadPlayerData(sPlayerName, "playerQuestTracking")

	if questTracked and questTracked ~= "[]" then
		questTracked = Events.decode(questTracked)
		kgp.playerLoadMenu(playerID, "Framework_QuestCompass.xml")
		Events.sendClientEvent("FRAMEWORK_RETURN_TRACKED_QUEST", {quest = questTracked}, playerID)
	end
end


function onPlayerTrackedQuestUpdated(self,event)
	LOG_FUNCTION_CALL("onPlayerTrackedQuestUpdated")
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerTrackedQuestUpdated() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerTrackedQuestUpdated() event is nil."); return end
	local player = event.player
	if player == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerTrackedQuestUpdated() event.player is nil."); return end
	local sPlayerName = player.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerTrackedQuestUpdated() Player with ID of "..tostring(player.ID).." has a NIL name!"); return end
	local quest = event.quest
	if quest == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerTrackedQuestUpdated() event.quest is nil."); return end

	if quest == false  then
		SaveLoad.deletePlayerData(sPlayerName, "playerQuestTracking")
	else
		SaveLoad.savePlayerData(sPlayerName, "playerQuestTracking", Events.encode(quest))
	end
end



-- Called when a player's quest completion status has been modified
function onPlayerQuestDataUpdated(self, event)
	LOG_FUNCTION_CALL("onPlayerQuestDataUpdated")
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestDataUpdated() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestDataUpdated() event is nil."); return end
	local player = event.player
	local sPlayerName = player and player.name
	if player == nil or sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestDataUpdated() event.player is nil."); return end
	local questsComplete, questsIncomplete = event.questsComplete, event.questsIncomplete
	if questsComplete == nil or questsIncomplete == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestDataUpdated() event quest data is nil."); return end
	
	local completes = {}
	-- Count up completed quests so we have the next insert slot if needed
	local completeQuestSlot = 1
	if not Toolbox.isContainerEmpty(questsComplete) then
	--if #event.questsComplete > 0 then
		for j, k in pairs(player.quests.complete) do
			completeQuestSlot = completeQuestSlot + 1
		end
	end
	-- Update or add to complete quests table
	for questIndex, completeQuest in pairs(questsComplete) do
		completes[completeQuest.UNID] = true
		local playerHasQuest = false
		for i, quest in pairs(player.quests.complete) do
			-- If player has this quest already, just update it's completeTime
			if completeQuest.UNID == quest.UNID then
				playerHasQuest = true
				quest.completeTime = completeQuest.completeTime
				break
			end
		end
		-- If the player doesn't have this quest, add it to the next available slot
		if not playerHasQuest then
			player.quests.complete[completeQuestSlot] = {	
				UNID			= completeQuest.UNID,
				completeTime	= completeQuest.completeTime
			}
			completeQuestSlot = completeQuestSlot + 1
		end
		-- Record the quest completion metric
		recordQuestCompletion(sPlayerName, completeQuest)
	end
	
	local incompletes = {}
	-- Find quests that are in the player's incomplete table that are no longer there
	for i, playerQuest in pairs(event.player.quests.incomplete) do
		local abandonQuest = true
		for j, quest in pairs(event.questsIncomplete) do
			if tonumber(playerQuest.UNID) == tonumber(quest.UNID) then
				abandonQuest = false
				break
			end
		end
		if abandonQuest then
			event.player.quests.incomplete[i] = nil
			local activeQuest = s_tActiveQuests[sPlayerName]
			if activeQuest then
				activeQuest[playerQuest] = nil
			end
			if playerQuest.UNID and s_tActiveQuests[sPlayerName] and s_tActiveQuests[sPlayerName][playerQuest.UNID] then
				s_tActiveQuests[sPlayerName][playerQuest.UNID] = nil -- Clear out the entry for the purposes of calculating quest icons
			end
		end
	end
	
	-- Update player's incomplete quests
	-- Find an active incomplete quest
	for i, quest in pairs(event.questsIncomplete) do
		event.player.quests.incomplete[i] = {
				UNID			= quest.UNID,
				fulfilled		= quest.fulfilled,
				fulfillCount	= quest.fulfillCount
		}
		incompletes[quest.UNID] = true
	end
	
	-- Detect if a quest was just completed to remove it from the s_tActiveQuests table
	for player, activeQuests in pairs(s_tActiveQuests) do
		for questUNID, _ in pairs(activeQuests) do
			-- If this tracked quest is only in the complete table, it's not active anymore
			if completes[questUNID] and not incompletes[questUNID] then
				activeQuests[questUNID] = nil
			end
		end
	end
	
	savePlayerQuests(sPlayerName)
	sendQuestsToClient(sPlayerName)
	updatePlayerQuestIcons(player)
end


-- Update a player's quest progress
function onPlayerQuestProgressUpdated(self, event)
	LOG_FUNCTION_CALL("onPlayerQuestProgressUpdated")
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestProgressUpdated() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestProgressUpdated() event is nil."); return end

	local player = event.player
	local sPlayerName = player and player.name
	if player == nil or sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerQuestProgressUpdated() event.player is nil."); return end

	if event.questUNID then
		local activeQuest = s_tActiveQuests[sPlayerName]
		if activeQuest == nil then
			activeQuest = {}
		end
		
		local quest = s_uItemProperties[tostring(event.questUNID)]
		
		-- Does this player have enough items to complete the quest?

		local fulfilled = false
		if quest.questType then -- Has a quest type
			if quest.questType == "collect" then -- Collection quest
				if quest.requiredItem and quest.requiredItem.requiredCount and
				event.progressCount and (event.progressCount >= quest.requiredItem.requiredCount) then -- Required count is met
					fulfilled = true
				end
			elseif event.fulfilled then -- All other quest types
				fulfilled = event.fulfilled or false
			end
		end

		activeQuest[event.questUNID] = fulfilled
		s_tActiveQuests[sPlayerName] = activeQuest
	end
	
	-- Update all quest icons for this player if requested
	if event.updateIcons then
		updatePlayerQuestIcons(player)
	end
end




function onPlayerQuestIconsUpdateRequested(self, event)
	LOG_FUNCTION_CALL("onPlayerQuestIconsUpdateRequested")
	
	if self == nil then log("Controller_Player - onPlayerQuestIconsUpdateRequested() self is nil."); return end
	if event == nil then log("Controller_Player - onPlayerQuestIconsUpdateRequested() event is nil."); return end
	if event.player == nil then log("Controller_Player - onPlayerQuestIconsUpdateRequested() event.player is nil."); return end
	
	updatePlayerQuestIcons(event.player, event.resetIcons)
end

function onPlayerQuestIconsClearingRequested(self, event)
	LOG_FUNCTION_CALL("onPlayerQuestIconsClearingRequested")
	
	if self == nil then log("Controller_Player - onPlayerQuestIconsClearingRequested() self is nil."); return end
	if event == nil then log("Controller_Player - onPlayerQuestIconsClearingRequested() event is nil."); return end
	if event.player == nil then log("Controller_Player - onPlayerQuestIconsClearingRequested() event.player is nil."); return end
	
	if s_tPlayerIcons[event.player.name] == nil then
		return
	end
	
	for objectPID, iconState in pairs(s_tPlayerIcons[event.player.name]) do
		kgp.objectUnEquipItemForPlayer(event.player.ID, tonumber(objectPID), tonumber(iconState))
	end
	
	s_tPlayerIcons[event.player.name] = {}
end

function onPlayerQuestIconAssignmentRequested(self, event)
	LOG_FUNCTION_CALL("onPlayerQuestIconAssignmentRequested")
	
	if self == nil then log("Controller_Player - onPlayerQuestIconAssignmentRequested() self is nil."); return end
	if event == nil then log("Controller_Player - onPlayerQuestIconAssignmentRequested() event is nil."); return end
	
	setQuestIcon(event.player, event.object)
end


function onCompleteQuestUpdateRequested(self, event)
	LOG_FUNCTION_CALL("onCompleteQuestUpdateRequested")
	
	-- Update all player quest icons and quest info
	for sName, uPlayer in pairs(s_uPlayers) do
		if uPlayer and uPlayer.inZone then
			sendQuestsToClient(sName)
			updatePlayerQuestIcons(uPlayer)
		end
	end
end


function playCompletionSound(self, event)
	if event.player then

		Game.playerPlaySound({user = event.player, soundGLID = COMPLETION_SOUND})
		Game.playerSetParticle{user 			= event.player,
								   particleKey 	= "questCompletion",
								   boneName 	= "Bip01 L Foot",
								   particleGLID = COMPLETION_EFFECT,
								   offset 		= {x = 0, y = 0, z = 0},
								   upDir 		= {x =  -1, y = 0, z = -1}
								  }
	end
end

function onPlayerInventorySlotsUpdated(self, event)
	LOG_FUNCTION_CALL("onPlayerInventorySlotsUpdated")
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerInventorySlotsUpdated() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerInventorySlotsUpdated() event is nil."); return end
	local uPlayer = event.player
	if uPlayer == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerInventorySlotsUpdated() event.player is nil."); return end
	if event.updatedSlots == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerInventorySlotsUpdated() event.updatedSlots is nil."); return end
	
	for i, slot in pairs(event.updatedSlots) do
		local index = slot.index
		local item = slot.item
		local gameItem = s_uItemProperties[tostring(item.UNID)]
		
		if gameItem then
			local itemData = uPlayer.inventory[index]
			itemData.UNID  = item.UNID
			itemData.count = item.count
			--TODO:Look into this line. Must have overwrite true for ED-5978, check into other solutions and what it could break
			
		
			-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
			--Toolbox.setUserdataAtIndex(itemData, "properties", gameItem)
			itemData["properties"] = gameItem
			
			
			
			for invIndex, invValue in pairs(gameItem) do
				if invIndex == "particleEffects" or invIndex == "particleEffectsMine" or invIndex == "particleEffectsTheirs" then
					local properties = {}
					for k, v in pairs(invValue) do
						properties[k] = Toolbox.deepCopy(v)
					end
					
					-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
					--Toolbox.setUserdataAtIndex(itemData.properties, invIndex, properties)
					itemData.properties[invIndex] = properties
				end
			end
			itemData.instanceData = {}
			if item.instanceData then
				for i2, v in pairs(item.instanceData) do
					-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
					--Toolbox.setUserdataAtIndex(itemData.instanceData, i2, v)
					itemData.instanceData[i2] = v
				end
			end
		else
			-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
			--Toolbox.setUserdataAtIndex(uPlayer.inventory, index, itemData, true)
			if uPlayer.inventory then
				uPlayer.inventory[index] = GameDefinitions.EMPTY_ITEM
			else
				log("ERROR DETECTED - ED-7836 - uPlayer.inventory was nil for player "..tostring(player.name))
				Debug.printTableError("ERROR DETECTED - ED-7836 - player", player)
			end
		end
	end
	-- TEMP-FUNCTION - Checks if the player's inventory is no longer of size 45
	logInventorySizeError(event.player, "onPlayerInventorySlotsUpdated()")
	savePlayerInventory(event.player.name)
end


function onPlayerArmorySlotsUpdated(self, event)
	LOG_FUNCTION_CALL("onPlayerArmorySlotsUpdated")
	
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerArmorySlotsUpdated() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerArmorySlotsUpdated() event is nil."); return end
	local uPlayer = event.player
	local sPlayerName = uPlayer and uPlayer.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerArmorySlotsUpdated() event.player is nil."); return end
	if event.updatedSlots == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerArmorySlotsUpdated() event.updatedSlots is nil."); return end
	
	for i, slot in pairs(event.updatedSlots) do
		local index = slot.index
		local item = slot.item
		
		local oldArmor = uPlayer.armor[index]
		local oldArmorUNID = tostring(oldArmor.UNID)
		local newArmorUNID = tostring(item.UNID)
		local gameItem = s_uItemProperties[newArmorUNID]
		if gameItem then
			--[[-- If a piece of armor was swapped
			if tonumber(oldArmorUNID) ~= 0 and tonumber(newArmorUNID) ~= 0 and s_uItemProperties[oldArmorUNID] and s_uItemProperties[newArmorUNID] then
				-- Un-equip all GLIDs associated with the old UNID
				unEquipArmorGLIDs(uPlayer, oldArmorUNID)
			end]]
			oldArmor.UNID  = item.UNID
			oldArmor.count = item.count
			-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
			--Toolbox.setUserdataAtIndex(oldArmor, "properties", gameItem, true)
			oldArmor["properties"] = gameItem
			
			local instanceData = {}
			if item.instanceData then
				for i, v in pairs(item.instanceData) do
					instanceData[i] = v
				end
			end
			-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
			--Toolbox.setUserdataAtIndex(oldArmor, "instanceData", instanceData, true)
			oldArmor["instanceData"] = instanceData
		else
			--[[-- If a piece of armor was just removed
			if tonumber(oldArmorUNID) ~= 0 and s_uItemProperties[oldArmorUNID] then
				-- Un-equip all GLIDs associated with the old UNID
				unEquipArmorGLIDs(uPlayer, oldArmorUNID)
			end]]
			
			-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
			--Toolbox.setUserdataAtIndex(uPlayer.armor, index, emptyArmor, true)
			uPlayer.armor[index] = GameDefinitions.EMPTY_ITEM
		end
	end
	
	-- Equip player's inventory
	equipInventory(sPlayerName)
	
	-- Calculate armor rating and adjust maxHealth by it
	uPlayer.armorRating, uPlayer.baseArmorRating = calculateArmorRating(sPlayerName)
	uPlayer.maxHealth = 10 + (uPlayer.armorRating)
	-- Health can't be greater than maxHealth
	uPlayer.health = math.min(uPlayer.health, uPlayer.maxHealth)
	
	local eventData =  {
		ID = sPlayerName, 
		modifiedFields = {	
			armorRating = uPlayer.baseArmorRating,
			health = uPlayer.health,
			maxHealth = uPlayer.maxHealth
		}
	}
	
	Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", eventData)
	
	-- Update HUD
	Events.sendClientEvent("FRAMEWORK_SET_ARMOR", {armorRating = uPlayer.baseArmorRating}, uPlayer.ID)
	Events.sendClientEvent("FRAMEWORK_SET_HEALTH", {health = uPlayer.health, maxHealth = uPlayer.maxHealth}, uPlayer.ID)
	-- TEMP-FUNCTION - Checks if the player's inventory is no longer of size 45
	logInventorySizeError(uPlayer, "onPlayerArmorySlotsUpdated()")
	-- Save off updated inventory
	savePlayerInventory(sPlayerName)
end




function onPlayerBlueprintSlotsUpdated(self, event)
	
	LOG_FUNCTION_CALL("onPlayerBlueprintSlotsUpdated")
	
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerBlueprintSlotsUpdated() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerBlueprintSlotsUpdated() event is nil."); return end
	local uPlayer = event.player
	local sPlayerName = uPlayer and uPlayer.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerBlueprintSlotsUpdated() event.player is nil."); return end
	if event.updatedSlots == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerBlueprintSlotsUpdated() event.updatedSlots is nil."); return end
	
	for i, slot in pairs(event.updatedSlots) do
		local index = slot.index
		if index > 0 then
			local item = slot.item
			local gameItem = s_uItemProperties[tostring(item.UNID)]
			if gameItem then
				local blueprint = uPlayer.blueprints[index]
				if blueprint == nil then
					blueprint = {}
				end
				blueprint.UNID = item.UNID
				blueprint.count = item.count
				blueprint.properties = gameItem
				
				-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
				--Toolbox.setUserdataAtIndex(uPlayer.blueprints, index, blueprint)
				uPlayer.blueprints[index] = blueprint
			end
		else
			--Remove item
			index = -index
			local blueprints = uPlayer.blueprints
			for bpIndex, blueprint in pairs(blueprints) do
				if tonumber(bpIndex) == index then
					blueprints[bpIndex] = nil
				elseif tonumber(bpIndex) > index then
					blueprints[bpIndex] = blueprints[bpIndex + 1]
				end
			end
		end
	end
	
	-- TEMP-FUNCTION - Checks if the player's inventory is no longer of size 45
	logInventorySizeError(uPlayer, "updatePlayerBlueprintSlots()")
	savePlayerInventory(sPlayerName)
end


-- Called from Controller_Battle when a player's armor's durability gets modified
function onPlayerArmorDurabilityUpdated(self, event)
	
	LOG_FUNCTION_CALL("onPlayerArmorDurabilityUpdated")

	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerArmorDurabilityUpdated() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerArmorDurabilityUpdated() event is nil."); return end
	local uPlayer = event.player
	local sPlayerName = uPlayer and uPlayer.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerArmorDurabilityUpdated() event.player is nil."); return end
	local index = event.slot
	local durabilityLoss = event.durabilityLoss
	if index == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerArmorDurabilityUpdated() event.slot is nil."); return end
	if durabilityLoss == nil then Console.Write(Console.DEBUG, "Controller_Player - onPlayerArmorDurabilityUpdated() event.durabilityLoss is nil."); return end
	
	local armorBroke = false -- Did a piece of armor JUST become broken?
	
	local armor = uPlayer.armor[index]
	
	local oldDurability = armor.instanceData.durability
	local durabilityType = armor.properties.durabilityType or "repairable"
	if durabilityType ~= "indestructible" then
		armor.instanceData.durability = oldDurability - durabilityLoss
	
		if oldDurability > 0 and armor.instanceData.durability == 0 then
			armorBroke = true
		end
	end
	
	local itemCopy = Toolbox.deepCopy(armor)
			
	-- Inform Framework_InventoryHandler of the new armor durability
	local eventData = {
		pid = -1,
		index = tonumber(index),
		instanceData = itemCopy.instanceData
	}
	Events.sendClientEvent("UPDATE_ITEM_INSTANCE_DATA", eventData, uPlayer.ID)
	
	-- If a piece of armor just broke, we need to clean up some stuff
	if armorBroke then
		-- equipInventory will remove the broken armor properly
		equipInventory(sPlayerName)
		
		-- Calculate armor rating and adjust maxHealth by it
		uPlayer.armorRating, uPlayer.baseArmorRating = calculateArmorRating(sPlayerName)
		uPlayer.maxHealth = 10 + (uPlayer.armorRating)
		
		-- Health can't be greater than maxHealth
		uPlayer.health = math.min(uPlayer.health, uPlayer.maxHealth)
		local eventData = {
			ID = sPlayerName, 
			modifiedFields = {	
				armorRating = uPlayer.baseArmorRating,
				health = uPlayer.health,
				maxHealth = uPlayer.maxHealth
			}
		}
		Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", eventData)
		
		-- Update HUD
		Events.sendClientEvent("FRAMEWORK_SET_ARMOR", {armorRating = uPlayer.baseArmorRating}, uPlayer.ID)
		Events.sendClientEvent("FRAMEWORK_SET_HEALTH", {health = uPlayer.health, maxHealth = uPlayer.maxHealth}, uPlayer.ID)
	end
	
	-- TEMP-FUNCTION - Checks if the player's inventory is no longer of size 45
	logInventorySizeError(uPlayer, "onPlayerArmorDurabilityUpdated()")
	-- Save off updated inventory
	savePlayerInventory(sPlayerName)
end


-- Called when the BottomRightHUD opens
function onBottomRightHUDInfoRequested(self, event)
	LOG_FUNCTION_CALL("onBottomRightHUDInfoRequested")
	
	-- Error check inputs
	if self == nil then log("Controller_Player - onBottomRightHUDInfoRequested() self is nil."); return end
	if event == nil then log("Controller_Player - onBottomRightHUDInfoRequested() event is nil."); return end
	-- Was a valid player passed in?
	if event.player == nil then log("Controller_Player - onBottomRightHUDInfoRequested() event.player is nil."); return end
	local sPlayerName = event.player.name
	if sPlayerName == nil then log("Controller_Player - onBottomRightHUDInfoRequested() event.player.name is nil."); return end
	local playerID = event.player.ID
	if playerID == nil then log("Controller_Player - onBottomRightHUDInfoRequested() event.player.ID is nil."); return end
	
	local _, baseArmorRating = calculateArmorRating(event.player.name)
	
	Events.sendClientEvent("FRAMEWORK_SET_ARMOR", {armorRating = baseArmorRating}, playerID)
end


-- Called when a weapon is equipped from the toolbar menu
function onItemEquipped(self, event)
	LOG_FUNCTION_CALL("onItemEquipped")

	if self == nil then log("Controller_Player - onItemEquipped() self is nil."); return end
	if event == nil then log("Controller_Player - onItemEquipped() event is nil."); return end
	local uPlayer = event.player
	if uPlayer == nil then log("Controller_Player - onItemEquipped() event.player is nil."); return end
	local sPlayerName = uPlayer.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onItemEquipped() Player with ID of "..tostring(event.player.ID).." has a NIL name!"); return end
	if event.UNID == nil then log("Controller_Player - equipWeapon() event.slot is nil."); return end
	
	local strUNID = tostring(event.UNID)
	local gameItem = s_uItemProperties[strUNID]
	if gameItem then
		-- Equip weapon
		if gameItem.itemType == GameDefinitions.ITEM_TYPES.WEAPON or gameItem.itemType == GameDefinitions.ITEM_TYPES.TOOL then
			uPlayer.equipped = strUNID
			
			if gameItem.soundGLID then
				local tSoundData = GameDefinitions.generateGenericSoundParams(uPlayer.ID, gameItem.soundGLID, gameItem.soundRange, true)
				event.player.playerInventorySound = Behavior_Sound.generate(tSoundData)
			end
		end
		
		if uPlayer.equipped ~= "" then
			equipInventory(sPlayerName)
		end
	end

	event.player.handEquipped = false
end

-- Un-equip an item
function onItemUnequipped(self, event)
	LOG_FUNCTION_CALL("onItemUnequipped")
	
	-- Extract player info
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onItemUnequipped() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onItemUnequipped() event is nil."); return end
	local uPlayer = event.player
	if uPlayer == nil then Console.Write(Console.DEBUG, "Controller_Player - onItemUnequipped() event.player is nil."); return end
	local sPlayerName = uPlayer.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onItemUnequipped() event.player.name is nil."); return end
	if event.UNID == nil then Console.Write(Console.DEBUG, "Controller_Player - onItemUnequipped() event.UNID is nil."); return end

	local gameItem = s_uItemProperties[tostring(event.UNID)]
	if gameItem then
		-- Un-equip weapon
		if gameItem.itemType == GameDefinitions.ITEM_TYPES.WEAPON or gameItem.itemType == GameDefinitions.ITEM_TYPES.TOOL then
			kgp.playerUnEquipItem(uPlayer.ID, gameItem.GLID)

			if(gameItem.soundGLID) then
				Behavior_Sound.delete({soundID = uPlayer.playerInventorySound})
				uPlayer.playerInventorySound = nil
			end
		end
		uPlayer.equipped = ""
	end
	
	equipInventory(sPlayerName)

	uPlayer.handEquipped = true
end


function onArmorHidingUpdated(self, event)
	LOG_FUNCTION_CALL("onArmorHidingUpdated")
	
	if self == nil then Console.Write(Console.DEBUG, "Controller_Player - onArmorHidingUpdated() self is nil."); return end
	if event == nil then Console.Write(Console.DEBUG, "Controller_Player - onArmorHidingUpdated() event is nil."); return end
	if event.player == nil then Console.Write(Console.DEBUG, "Controller_Player - onArmorHidingUpdated() event.player is nil."); return end
	local sPlayerName = event.player.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onArmorHidingUpdated() event.player.name is nil."); return end

	s_tArmorHiddenStates[sPlayerName] = event.hideArmor
	equipInventory(sPlayerName)
end


-- Jumpsuit setting modified
function onJumpsuitSettingModified(self, event)
	LOG_FUNCTION_CALL("onJumpsuitSettingModified")
	
	s_bIsJumpSuitOn = event.jumpsuit == true
	-- Call equipInventory for each player
	for name, sharedData in pairs(s_uPlayers) do
		if sharedData and sharedData.inZone then
			equipInventory(name)
		end
	end
end


function WOKArmItem(self, event)
	LOG_FUNCTION_CALL("WOKArmItem")
	equipInventory(event.player.name)
end


function onGemChestCashInRequested(self, event)
	LOG_FUNCTION_CALL("onGemChestCashInRequested")
	
	if self == nil then log("Controller_Player - onGemChestCashInRequested() self is nil."); return end
	if event == nil then log("Controller_Player - onGemChestCashInRequested() event is nil."); return end
	local uPlayer = event.player
	if uPlayer == nil then log("Controller_Player - onGemChestCashInRequested() event.player is nil."); return end
	local sPlayerName = uPlayer.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onGemChestCashInRequested() Player with ID of "..tostring(event.player.ID).." has a NIL name!"); return end
	local slot = event.slot
	if slot == nil then Console.Write(Console.DEBUG, "Controller_Player - onGemChestCashInRequested() called without a valid inventory slot!"); return end

	if uPlayer.inventory then
		local gemSlot = 0
		local item = uPlayer.inventory[slot]
		if item and item.UNID == GameDefinitions.GEM_CHEST_UNID and item.count > 0 then
			gemSlot = slot
		end
		
		if gemSlot > 0 then -- valid slot found, attempt to reward gem
			local rand = math.random() * 100
			local currChance = 0
			local reward = 0

			for i = #GameDefinitions.GEM_ROLL_CHANCES, 1, -1 do
				currChance = currChance + GameDefinitions.GEM_ROLL_CHANCES[i]
				if rand <= currChance then
					reward = i
					break
				end
			end

			if reward > 0 and deductItemAtSlot(uPlayer, slot) then
				s_tPendingWebCalls[s_nextCallCID] = {
					player = uPlayer, 
					slot = slot, 
					gemType = reward
				}
				local webCallEventData = {
					CID			= s_nextCallCID, 
					input		= {
						username	= sPlayerName, 
						itemAmounts	= { [GameDefinitions.GEM_ROLL_NAMES[reward]] = 1 }
					}, 
					callType	= "AWARD_GEM"
				}
				Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", webCallEventData)
				s_nextCallCID = s_nextCallCID + 1
			end
		end
	end
end


function onWorldCoinExchangeRequested(self, event)
	LOG_FUNCTION_CALL("onWorldCoinExchangeRequested")

	if self == nil then log("Controller_Player - onWorldCoinExchangeRequested() self is nil."); return end
	if event == nil then log("Controller_Player - onWorldCoinExchangeRequested() event is nil."); return end
	local uPlayer = event.player
	if uPlayer == nil then log("Controller_Player - onWorldCoinExchangeRequested() event.player is nil."); return end
	local sPlayerName = uPlayer.name
	if sPlayerName == nil then Console.Write(Console.DEBUG, "Controller_Player - onWorldCoinExchangeRequested() Player with ID of "..tostring(event.player.ID).." has a NIL name!"); return end
	if tonumber(event.count) == nil then Console.Write(Console.DEBUG, "Controller_Player - onWorldCoinExchangeRequested() called without a valid item count!"); return end
	
	if uPlayer and uPlayer.inventory then
		if tonumber(event.count) > 0 and deductItemByUNID(uPlayer, GameDefinitions.WORLD_COIN_UNID, tonumber(event.count)) then
			s_tPendingWebCalls[s_nextCallCID] = {player = uPlayer}
			local data = {	
				CID			= s_nextCallCID, 
				input		= {	
					username	= sPlayerName, 
					itemAmounts = {	["World Coin"] = event.count}
				}, 
				callType 	= "CONVERT_ITEM"
			}
			Events.sendEvent("FRAMEWORK_MAKE_WEB_CALL", data)
			s_nextCallCID = s_nextCallCID + 1
		end
	end
end

-- Sent from Controller_WebCall which returns webcall results
function onWebCallResponded(self, event)
	LOG_FUNCTION_CALL("onWebCallResponded")
	
	local CID = event.CID
	local webcall = s_tPendingWebCalls[CID]
	
	if event.callType == "AWARD_GEM" and webcall then
		if event.success and webcall.player then
			Events.sendClientEvent("FRAMEWORK_UPDATE_GEM_COUNTS", {text = event.text, gemType = webcall.gemType}, webcall.player.ID)
			s_tPendingWebCalls[CID] = nil
		end
	elseif event.callType == "CONVERT_ITEM" and webcall then
		if event.success and webcall.player then
			Events.sendClientEvent("FRAMEWORK_UPDATE_COIN_EXCHANGE", {text = event.text}, webcall.player.ID)
			s_tPendingWebCalls[CID] = nil
		end
	end
end



function onPlayerInventoriesValidationRequested(self, event)
	LOG_FUNCTION_CALL("onPlayerInventoriesValidationRequested")
	
	validatePlayerInventories()
end

function onPlayerTitleUpdated(self, event )
	if event.player == nil then return end
	if event.player.ID == nil then return end
	if event.title == nil then return end

	kgp.playerSetTitle( event.player.ID, event.title)
end


function hidePlayer(self, event)
	if event.playerName == nil then return end
	if s_uPlayers[event.playerName] == nil then return end
	kgp.playerHide(s_uPlayers[event.playerName].ID)
end

function showPlayer(self, event)
	if event.playerName == nil then return end
	if s_uPlayers[event.playerName] == nil then return end
	kgp.playerShow(s_uPlayers[event.playerName].ID)
end

-----------------------------------------------------------------
-- Local function definitions
-----------------------------------------------------------------


-- Loads the default Inventory
loadDefaultInventory = function()
	LOG_FUNCTION_CALL("loadDefaultInventory")
	
	-- Default s_tDefaultInventory appropriately
	for i = 1, GameDefinitions.PLAYER_INVENTORY_CAPACITY do
		-- EMPTY ITEM
		s_tDefaultInventory.inventory[i] = GameDefinitions.EMPTY_ITEM
	end

	for i = 1, GameDefinitions.PLAYER_INVENTORY_ARMOR_SLOTS do
		-- EMPTY ITEM
		s_tDefaultInventory.armor[i] = GameDefinitions.EMPTY_ITEM
	end
	
	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	local defaultInventory = SaveLoad.loadGameData("DefaultInventory", parentID, parentType)
	
	if defaultInventory and defaultInventory ~= "[]" then
		local defaultModified = false
		defaultInventory = Events.decode(defaultInventory)
		
		if defaultInventory.inventory then
			for slot, itemInfo in pairs(defaultInventory.inventory) do
				if itemInfo.UNID ~= 0 and s_uItemProperties[tostring(itemInfo.UNID)] == nil then
					defaultModified = true
					s_tDefaultInventory.inventory[slot] = GameDefinitions.EMPTY_ITEM
				else
					s_tDefaultInventory.inventory[slot] = itemInfo
				end
			end
		end
		-- Default armor
		if defaultInventory.armor then
			for slot, itemInfo in pairs(defaultInventory.armor) do
				if itemInfo.UNID ~= 0 and s_uItemProperties[tostring(itemInfo.UNID)] == nil then
					defaultModified = true
					s_tDefaultInventory.armor[slot] = GameDefinitions.EMPTY_ITEM
				else
					s_tDefaultInventory.armor[slot] = itemInfo
				end
			end
		end
		-- Default blueprints
		if defaultInventory.blueprints then
			for slot,itemInfo in pairs(defaultInventory.blueprints) do
				if itemInfo.UNID ~= 0 and s_uItemProperties[tostring(itemInfo.UNID)] == nil then
					defaultModified = true
					s_tDefaultInventory.blueprints[slot] = {UNID = 0, count = 0}
				else
					s_tDefaultInventory.blueprints[slot] = itemInfo
				end
			end
		end
		if defaultModified then
			local defToSave = {}
			-- Strip properties and instanceData from defaults
			for inventory, inventoryTable in pairs(s_tDefaultInventory) do
				local saveInfo = {}
				for slot, itemInfo in pairs(inventoryTable) do
					saveInfo[slot] = {
						UNID = itemInfo.UNID, 
						count = itemInfo.count
					}
				end
				
				defToSave[inventory] = saveInfo
			end
			SaveLoad.saveGameData("DefaultInventory", Events.encode(defToSave), parentID, parentType)
		end
	end
end


-- Send a player's inventory to the client
sendInventoryToClient = function(sPlayerName)
	LOG_FUNCTION_CALL("sendInventoryToClient")

	local uPlayer = s_uPlayers[sPlayerName]

	local clientInventory = {}	
	for index, item in pairs(uPlayer.inventory) do
		
		inventoryContent = {}
		inventoryContent.count = tonumber(item.count)
		inventoryContent.UNID = tonumber(item.UNID)
		inventoryContent.properties = {}
		
		
		for i, v in pairs(item.properties) do
			if not Toolbox.isContainer(v) then
				inventoryContent.properties[i] = v
			end			
		end
		
		inventoryContent.instanceData = {}
		for i, v in pairs(item.instanceData) do
			inventoryContent.instanceData[i] = v
		end
		
		--Send cooldown and food name with pets
		local gameItem = s_uItemProperties[tostring(item.UNID)]
		if gameItem and gameItem.behavior == "pet" then
			if tonumber(gameItem.food) then
				local foodItem = s_uItemProperties[tostring(gameItem.food)]
				if foodItem then
					inventoryContent.properties.foodName = foodItem.name
				end
			end
		end
		
		clientInventory[tonumber(index)] = inventoryContent
		
	end

	local clientArmor = {}
	for index, item in pairs(uPlayer.armor) do
		local armorContent = {}
		armorContent.count = tonumber(item.count)
		armorContent.UNID = tonumber(item.UNID)
		armorContent.properties = {}

		for i,v in pairs(item.properties) do
			if not Toolbox.isContainer(v) then
				armorContent.properties[i] = v
			end				
		end
		armorContent.instanceData = {}
		for i, v in pairs(item.instanceData) do
			armorContent.instanceData[i] = v
		end
		
		clientArmor[tonumber(index)] = armorContent
	end
	
	local clientBlueprints = {}
	for i, blueprint in pairs(uPlayer.blueprints) do
		clientBlueprints[i] = {
			UNID = blueprint.UNID,
			count = blueprint.count,
			oneUse = blueprint.oneUse
		}
	end
	
	local eventData = {
		updatedInventory	= clientInventory, 
		updatedArmor		= clientArmor,
		updatedBlueprints	= clientBlueprints
	}
	
	Events.sendClientEvent("UPDATE_INVENTORY", eventData, uPlayer.ID)
end



-- Equip all associated items on the player
equipInventory = function(sPlayerName)
	LOG_FUNCTION_CALL("equipInventory")

	local uPlayer = s_uPlayers[sPlayerName]
	
	local playerInventory = uPlayer.inventory
	local playerArmor = uPlayer.armor
	local equipped = uPlayer.equipped
	local equippedItem
	if equipped ~= "" then
		equippedItem = s_uItemProperties[tostring(equipped)]
	end
	
	local equipString -- Used if jumpsuit is on
	
	-- Equip default equipment set
	if s_bIsJumpSuitOn then
		if uPlayer.gender == "M" then
			equipString = GameDefinitions.DEFAULT_MALE_GLIDS
		elseif uPlayer.gender == "F" then
			equipString = GameDefinitions.DEFAULT_FEMALE_GLIDS
		end
	end

	-- Equip armor
	--Wait for player handler to send if server needs to hide or equip armor
	if s_tArmorHiddenStates[sPlayerName] then
		for index, item in pairs(playerArmor) do
			if item.properties and item.properties.GLID ~= 0 and item.instanceData.durability > 0 and s_tArmorHiddenStates[sPlayerName] ~= 1 then
				--Look up additional GLIDS
				local gameItem = s_uItemProperties[tostring(item.UNID)]

				if gameItem then
					if uPlayer.gender ~= nil and uPlayer.gender == "F" then
						if gameItem.femaleGLIDS and Toolbox.isContainer(gameItem.femaleGLIDS) then
							for _, GLID in pairs(gameItem.femaleGLIDS) do
								if equipString then equipString = equipString..", "..GLID
								else equipString = tostring(GLID) end
							end
						end
					else
						if gameItem.maleGLIDS and Toolbox.isContainer(gameItem.maleGLIDS) then
							for _, GLID in pairs(gameItem.maleGLIDS) do
								if equipString then equipString = equipString..", "..GLID
								else equipString = tostring(GLID) end
							end
						end
					end
				end
			end
		end
	end
	
	local equipAnimations = false
	if equippedItem and equippedItem.GLID then
		-- Equip weapon
		if equippedItem.itemType == GameDefinitions.ITEM_TYPES.WEAPON then
			if equipString then equipString = equipString .. ", " .. equippedItem.GLID
			else equipString = tostring(equippedItem.GLID) end
		-- Equip tool
		elseif equippedItem.itemType == GameDefinitions.ITEM_TYPES.TOOL then
			local genderedGLID = false
			if uPlayer.gender ~= nil and uPlayer.gender == "F" then
				if equippedItem.femaleGLIDS and Toolbox.isContainer(equippedItem.femaleGLIDS) then
					for _, GLID in pairs(equippedItem.femaleGLIDS) do
						genderedGLID = true
						if equipString then equipString = equipString .. ", " .. GLID
						else equipString = tostring(GLID) end
					end
				end
			else
				if equippedItem.maleGLIDS and Toolbox.isContainer(equippedItem.maleGLIDS) then
					for _, GLID in pairs(equippedItem.maleGLIDS) do
						genderedGLID = true
						if equipString then equipString = equipString .. ", " .. GLID
						else equipString = tostring(GLID) end
					end
				end
			end
			if not genderedGLID then
				if equipString then equipString = equipString .. ", " .. equippedItem.GLID
				else equipString = tostring(equippedItem.GLID) end
			end
		end
		
		-- Activate custom animation set
		if equippedItem.animationSet then
			equipAnimations = true
			local animationSet = GameDefinitions.ANIMATION_SETS[equippedItem.animationSet] or GameDefinitions.ANIMATION_SETS["One-Handed Melee"]
			local animations = animationSet.idle
			-- if a tool, use default male/female animations
			if equippedItem.animationSet and equippedItem.animationSet == "Tool" then
			-- if a regular weapon, use special animations
				local gender = uPlayer.gender
				if gender == "F" then
					kgp.playerDefineAnimation( uPlayer.ID, GameDefinitions.DEFAULT_ANIMS.F.STAND, GameDefinitions.DEFAULT_ANIMS.F.RUN, GameDefinitions.DEFAULT_ANIMS.F.WALK, GameDefinitions.DEFAULT_ANIMS.F.JUMP )
				elseif gender == "M" then
					kgp.playerDefineAnimation( uPlayer.ID, GameDefinitions.DEFAULT_ANIMS.M.STAND, GameDefinitions.DEFAULT_ANIMS.M.RUN, GameDefinitions.DEFAULT_ANIMS.M.WALK, GameDefinitions.DEFAULT_ANIMS.M.JUMP )
				else
					log("ERROR DETECTED - ED-7420 : equipInventory 1 unable to set animations due to invalid gender!  gender = "..tostring(gender)..", player.name = "..tostring(uPlayer and uPlayer.name))
				end	
			else
				kgp.playerDefineAnimation( uPlayer.ID, animations.STAND, animations.RUN, animations.WALK, animations.JUMP )
			end
		end
	end
	
	-- Is the jumpsuit on?
	if s_bIsJumpSuitOn then
		-- Don't equip Kaneva set atop this armor set
		kgp.playerSetEquipped(uPlayer.ID, equipString or "")
	else
		kgp.playerSetEquipped(uPlayer.ID, equipString or "", "KANEVA")
	end
	
	-- Has the player removed their last piece of anything?
	if uPlayer.equippedAnything and not equipString then
		uPlayer.equippedAnything = false
	-- Equipped their first piece of anything
	elseif equipString and uPlayer.equippedAnything == false then
		uPlayer.equippedAnything = true
	end
	Events.sendClientEvent("FRAMEWORK_ANYTHING_EQUIPPED", {anythingEquipped = uPlayer.equippedAnything}, uPlayer.ID)
	
	-- No custom animations for something equipped? Return to default animations
	if not equipAnimations and not uPlayer.inVehicle then
		local gender = uPlayer.gender
		if gender == "F" then
			kgp.playerDefineAnimation( uPlayer.ID, GameDefinitions.DEFAULT_ANIMS.F.STAND, GameDefinitions.DEFAULT_ANIMS.F.RUN, GameDefinitions.DEFAULT_ANIMS.F.WALK, GameDefinitions.DEFAULT_ANIMS.F.JUMP )
		elseif gender == "M" then
			kgp.playerDefineAnimation( uPlayer.ID, GameDefinitions.DEFAULT_ANIMS.M.STAND, GameDefinitions.DEFAULT_ANIMS.M.RUN, GameDefinitions.DEFAULT_ANIMS.M.WALK, GameDefinitions.DEFAULT_ANIMS.M.JUMP )
		else
			log("ERROR DETECTED - ED-7420 : equipInventory 2 unable to set animations due to invalid gender!  gender = "..tostring(gender)..", player.name = "..tostring(uPlayer and uPlayer.name))
		end		
	end
end





-- Ensures a player's timed vends table still has valid PIDs live in the zone to fulfill their orders
validatePlayerTimedVends = function(player)
	
	LOG_FUNCTION_CALL("validatePlayerTimedVends")
	

	local vendsChanged = false
	-- Does this player have a valid timedVends table
	if player.timedVends then
		-- Iterate over each player vend
		for PID, vendInfo in pairs(player.timedVends) do
			-- First ensure this vendPID is active in the zone and is valid
			local uItem = s_uItemInstances[PID]
			if uItem then
				-- Ensure each pending vend for this PID is still valid
				for vendID, finishTime in pairs(vendInfo) do
					-- If this player's vendID doesn't exist anymore (it's been deleted)
					local vendMatch = false
					local gameItem = s_uItemProperties[tostring(uItem.spawnUNID)]
					if not gameItem then
						gameItem = s_uItemProperties[tostring(uItem.UNID)]
					end
					for i, v in pairs(gameItem.trades) do
						if tonumber(v.VID) == tonumber(vendID) then
							vendMatch = true
						end
					end

					if not vendMatch then
						-- Remove this pending vend
						vendInfo[vendID] = nil
						vendsChanged = true
					end
				end
			-- PID has been removed
			else
				player.timedVends[PID] = nil
				vendsChanged = true
			end
		end
	end
	if vendsChanged then
		local vendCopy = {}
		for PID, vendInfo in pairs(player.timedVends) do
			local stringPID = tostring(PID)
			vendCopy[stringPID] = {}
			for vendID, finishTime in pairs(vendInfo) do
				vendCopy[stringPID][tostring(vendID)] = finishTime
			end
		end
		
		-- Save the new timedVends table off to the DB for the future
		local zoneInstanceID, zoneIndex, zoneType = kgp.gameGetCurrentInstance()
		SaveLoad.savePlayerData(player.name, "timedVends", Events.encode(vendCopy), zoneInstanceID, zoneType)
	end
end



-- Validates an inventory is valid. If not, cleans up
validatePlayerInventory = function(playerInventory)
	LOG_FUNCTION_CALL("validatePlayerInventory")
	
	local unidDereferenced = false
	-- Verify inventory UNID is still valid
	for i, inventory in pairs(playerInventory.inventory) do
		-- Check that this UNID is still a valid Game Item
		local isUNIDValid = inventory.UNID ~= 0 and s_uItemProperties[tostring(inventory.UNID)] == nil
		if isUNIDValid then
			unidDereferenced = true
			-- Remove this item from the user's inventory
			playerInventory.inventory[i] = GameDefinitions.EMPTY_ITEM
		end
	end
	-- Verify armor UNIDs
	for i, armor in pairs(playerInventory.armor) do
		-- Check that this UNID is still a valid Game Item
		local isUNIDValid = armor.UNID ~= 0 and s_uItemProperties[tostring(armor.UNID)] == nil
		if isUNIDValid then
			unidDereferenced = true
			playerInventory.armor[i] = GameDefinitions.EMPTY_ITEM
		end
	end
	
	return unidDereferenced, playerInventory
end


validatePlayerInventories = function()
	LOG_FUNCTION_CALL("validatePlayerInventories")

	for key, uPlayer in pairs(s_uPlayers) do
		if uPlayer then
			if uPlayer.inventory and uPlayer.inZone then
				--Update currently equipped weapon
				if uPlayer.equipped then
					local gameItem = s_uItemProperties[tostring(uPlayer.equipped)]

					if gameItem and gameItem.soundGLID then
						Behavior_Sound.delete({soundID = uPlayer.playerInventorySound})
						
						local tSoundParams = GameDefinitions.generateGenericSoundParams(uPlayer.ID, gameItem.soundGLID, 20, true)
						uPlayer.playerInventorySound = Behavior_Sound.generate(tSoundParams)
					end	
				end
				
				for i = 1, GameDefinitions.PLAYER_INVENTORY_CAPACITY do
					
					local inventoryItem = uPlayer.inventory[i]
					
					local gameItem = inventoryItem and s_uItemProperties[tostring(inventoryItem.UNID)]
					
					if not gameItem then
						uPlayer.inventory[i] = GameDefinitions.EMPTY_ITEM
					else
						-- Get data from game item SD

						-- Extract game item data
						for index, value in pairs(gameItem) do
							inventoryItem.properties[index] = value
						end
						-- Extract instance data if it exists
						if inventoryItem.instanceData then
							for index, value in pairs(inventoryItem.instanceData) do
								if index == "levelMultiplier" then value = GameDefinitions.getItemLevelMultiplier(value) end
								inventoryItem.instanceData[index] = value
							end
						-- Construct default instanceData
						elseif inventoryItem.properties.itemType == GameDefinitions.ITEM_TYPES.WEAPON or inventoryItem.properties.itemType == GameDefinitions.ITEM_TYPES.ARMOR or inventoryItem.properties.itemType == GameDefinitions.ITEM_TYPES.TOOL then
							local instanceDurability = GameDefinitions.DEFAULT_MAX_DURABILITY
							local durabilityType = inventoryItem.properties.durabilityType or "repairable"
							if durabilityType == "limited" then
								instanceDurability = GameDefinitions.DURABILITY_BY_LEVEL[inventoryItem.properties.itemLevel]
							end

							inventoryItem.instanceData = {	
								levelMultiplier = GameDefinitions.getItemLevelMultiplier(),
								durability = instanceDurability
							}
						end
					end
				end

				for i = 1, GameDefinitions.PLAYER_INVENTORY_ARMOR_SLOTS do
					local armorItem = uPlayer.armor[i]
					local gameItem = armorItem and s_uItemProperties[tostring(armorItem.UNID)]
					
					if not gameItem then
						uPlayer.armor[i] = GameDefinitions.EMPTY_ITEM
					else
						-- Extract game item data
						for index, value in pairs(gameItem) do
							armorItem.properties[index] = value
						end
						
						-- Extract instance data if it exists
						if armorItem.instanceData then
							for index, value in pairs(armorItem.instanceData) do
								if index == "levelMultiplier" then value = GameDefinitions.getItemLevelMultiplier(value) end
								armorItem.instanceData[index] = value
							end
						-- Construct default instanceData
						elseif armorItem.properties.itemType == GameDefinitions.ITEM_TYPES.WEAPON or armorItem.properties.itemType == GameDefinitions.ITEM_TYPES.ARMOR or armorItem.properties.itemType == GameDefinitions.ITEM_TYPES.TOOL then
							local instanceDurability = GameDefinitions.DEFAULT_MAX_DURABILITY
							local durabilityType =  armorItem.properties.durabilityType or "repairable"
							if durabilityType == "limited" then
								instanceDurability = GameDefinitions.DURABILITY_BY_LEVEL[armorItem.properties.itemLevel]
							end
							armorItem.instanceData = {	
								levelMultiplier	= GameDefinitions.getItemLevelMultiplier(),
								durability		= instanceDurability
							}
						end
					end
				end

				-- Calculate armor rating and adjust maxHealth by it
				uPlayer.armorRating, uPlayer.baseArmorRating = calculateArmorRating(uPlayer.name)
				uPlayer.maxHealth = 10 + (uPlayer.armorRating)
				-- Health can't be greater than maxHealth
				uPlayer.health = math.min(uPlayer.health, uPlayer.maxHealth)
				
				local eventData = {	
					ID = uPlayer.name, 
					modifiedFields = {
						armorRating = uPlayer.baseArmorRating,
						health = uPlayer.health,
						maxHealth = uPlayer.maxHealth
					}
				}

				Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", eventData)
				-- Update HUD
				Events.sendClientEvent("FRAMEWORK_SET_ARMOR", {armorRating = uPlayer.baseArmorRating}, uPlayer.ID)
				Events.sendClientEvent("FRAMEWORK_SET_HEALTH", {health = uPlayer.health, maxHealth = uPlayer.maxHealth}, uPlayer.ID)
				-- TEMP-FUNCTION - Checks if the player's inventory is no longer of size 45
				logInventorySizeError(uPlayer, "validatePlayerInventories()")
				-- Save off updated inventory
				savePlayerInventory(uPlayer.name)

				sendInventoryToClient(key)
				equipInventory(key)
			end
		end
	end
end



-- Populates a player's inventory slot accordingly
populatePlayerInventory = function(uPlayer, playerInventory)
	LOG_FUNCTION_CALL("populatePlayerInventory")
	
	-- Fill player's inventory table
	for i, inventory in pairs(playerInventory) do
		local newInventory = {	
			UNID = inventory.UNID, 
			count = inventory.count,
			properties = {	
				GLID = 0, 
				name = "EMPTY", 
				itemType = GameDefinitions.ITEM_TYPES.GENERIC
			},
			instanceData = {}
		}
		
		-- Get data from game item SD
		if inventory.UNID ~= 0 then
			local gameItem = s_uItemProperties[tostring(inventory.UNID)]
			
			if gameItem == nil then
				log("ERROR DETECTED - ED-5618: inventory.UNID = "..tostring(inventory.UNID)..", player.name = "..tostring(uPlayer and uPlayer.name)..", slot = "..tostring(i))
				newInventory.UNID = 0
				newInventory.count = 0
			else
				-- Extract game item data
				for index, value in pairs(gameItem) do
					-- Only insert non-nested entries ...except for special cases
					if not Toolbox.isContainer(value) then
						newInventory.properties[index] = value
					elseif index == "particleEffects" or index == "particleEffectsMine" or index == "particleEffectsTheirs" then
						newInventory.properties[index] = {}
						for k, v in pairs(value) do
							newInventory.properties[index][k] = Toolbox.deepCopy(v)
						end
					end
				end
			end
			
			-- Extract instance data if it exists
			if inventory.instanceData then
				for index, value in pairs(inventory.instanceData) do
					if index == "levelMultiplier" then value = GameDefinitions.getItemLevelMultiplier(value) end
					newInventory.instanceData[index] = value
				end
				-- If this weapon doesn't have a durability field, assign one now
				if (newInventory.properties.itemType == GameDefinitions.ITEM_TYPES.WEAPON or 
					newInventory.properties.itemType == GameDefinitions.ITEM_TYPES.ARMOR or 
					newInventory.properties.itemType == GameDefinitions.ITEM_TYPES.TOOL) and
					not newInventory.instanceData.durability then
					
						local instanceDurability = GameDefinitions.DEFAULT_MAX_DURABILITY
						local durabilityType =  newInventory.properties.durabilityType or "repairable"
						if durabilityType == "limited" then
							instanceDurability = GameDefinitions.DURABILITY_BY_LEVEL[newInventory.properties.itemLevel]
						end
						newInventory.instanceData.durability = instanceDurability
				end
			-- Construct default instanceData fields
			elseif 	newInventory.properties.itemType == GameDefinitions.ITEM_TYPES.WEAPON or 
					newInventory.properties.itemType == GameDefinitions.ITEM_TYPES.ARMOR or 
					newInventory.properties.itemType == GameDefinitions.ITEM_TYPES.TOOL then
					
				local instanceDurability = GameDefinitions.DEFAULT_MAX_DURABILITY
				local durabilityType =  newInventory.properties.durabilityType or "repairable"
				if durabilityType == "limited" then
					instanceDurability = GameDefinitions.DURABILITY_BY_LEVEL[newInventory.properties.itemLevel]
				end
				newInventory.instanceData = {	
					levelMultiplier = GameDefinitions.getItemLevelMultiplier(),
					durability = instanceDurability
				}
			end
		end
		-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
		--Toolbox.setUserdataAtIndex(player.inventory, i, newInventory)
		uPlayer.inventory[i] = newInventory
	end
end



-- Populates a player's armor
populatePlayerArmor = function(uPlayer, playerArmor)

	LOG_FUNCTION_CALL("populatePlayerArmor")
	
	-- Fill player's armor slots
	for i, armor in pairs(playerArmor) do
		-- TODO: Somehow, some items are being saved off as a completely empty table
		local newArmor = {	
			UNID = armor.UNID or 0, 
			count = armor.count or 0,
			properties = {	
				GLID = 0, 
				name = "EMPTY", 
				itemType = GameDefinitions.ITEM_TYPES.GENERIC
			},
			instanceData = {}
		}
		
		-- Ensure this armor item has a UNID
		if armor and armor.UNID and armor.UNID ~= 0 then
			-- Get data from game item SD
			local gameItem = s_uItemProperties[tostring(armor.UNID)]
			
			if gameItem == nil then
				log("ERROR DETECTED - ED-5615 : armor.UNID = "..tostring(armor.UNID)..", player.name = "..tostring(uPlayer and uPlayer.name)..", slot = "..tostring(i))
				newArmor.UNID = 0
				newArmor.count = 0
			else
				-- Extract game item data
				for index, value in pairs(gameItem) do
					-- Only insert non-nested entries
					if not Toolbox.isContainer(value) then
						newArmor.properties[index] = value
					end
				end
				
				-- Extract instance data if it exists
				if armor.instanceData then
					for index, value in pairs(armor.instanceData) do
						if index == "levelMultiplier" then value = GameDefinitions.getItemLevelMultiplier(value) end
						newArmor.instanceData[index] = value
					end
					-- If this armor piece does not have a durability, assign one now
					if not newArmor.instanceData.durability then
						local instanceDurability = GameDefinitions.DEFAULT_MAX_DURABILITY
						local durabilityType =  newArmor.instanceData.durabilityType or "repairable"
						if durabilityType == "limited" then
							instanceDurability = GameDefinitions.DURABILITY_BY_LEVEL[newArmor.instanceData.itemLevel]
						end
						newArmor.instanceData.durability = instanceDurability
					end
				-- Construct default instanceData
				else
					local instanceDurability = GameDefinitions.DEFAULT_MAX_DURABILITY
					local durabilityType =  newArmor.instanceData.durabilityType or "repairable"
					if durabilityType == "limited" then
						instanceDurability = GameDefinitions.DURABILITY_BY_LEVEL[newArmor.instanceData.itemLevel]
					end
					newArmor.instanceData = {	
						levelMultiplier	= GameDefinitions.getItemLevelMultiplier(),
						durability		= instanceDurability
					}
				end
			end
		end
		-- CJW, 05.12.2016 - Removing reference to Toolbox.setUserDataAtIndex as nested tables can now be copied in full. Keeping old code just in case things go awry.
		--Toolbox.setUserdataAtIndex(uPlayer.armor, i, newArmor)
		uPlayer.armor[i] = newArmor
	end
end



-- Populates a player's blueprints
populatePlayerBlueprints = function(uPlayer, playerBlueprints)
	LOG_FUNCTION_CALL("populatePlayerBlueprints")
	
	if playerBlueprints then
		for i, v in pairs(playerBlueprints) do
			uPlayer.blueprints[i] = {	
				UNID	= v.UNID,
				count	= v.count
			}
		end
	end
end





-- Loads the player's quests for this world
loadPlayerQuests = function(event)
	LOG_FUNCTION_CALL("loadPlayerQuests")
	if event == nil then log("Controller_Player - loadPlayerQuests() event is nil."); return end
	if event.user == nil then log("Controller_Player - loadPlayerQuests() event.user is nil."); return end
	local uPlayer = Players.determineUser(event.user, s_uPlayers)
	if uPlayer == nil then log("Controller_Player - loadPlayerQuests() user could not be extracted from determineUser()."); return end
	local userName = uPlayer.name
	if userName == nil then log("Controller_Player - loadPlayerQuests() user.name is nil"); return end
	
	uPlayer.quests = {
		complete	= {},
		incomplete	= {}
	}
	
	-- Load completed quests from DB
	local questsComplete = SaveLoad.loadPlayerData(uPlayer.name, "playerQuestsComplete")
	
	-- Does this user have completed quests?
	if questsComplete and questsComplete ~= "[]" then
		local newQuests = {}
		questsComplete = Events.decode(questsComplete) -- Not a fan of this, should probably allocate another variable

		-- Convert to hash table for s_uItemInstances
		for i, q in ipairs(questsComplete) do
			-- Ensure this UNID is still a valid quest
			if type(q) == "table" and s_uItemProperties[tostring(q.UNID)] then
				uPlayer.quests.complete[tostring(i)] = { 
					UNID = q.UNID,
					completeTime = q.completeTime
				}
				table.insert(newQuests, {	UNID = q.UNID,
											completeTime = q.completeTime})
			end
		end
		
		-- If a quest has been modified, save off the updated date
		if #newQuests ~= #questsComplete then
			SaveLoad.savePlayerData(uPlayer.name, "playerQuestsComplete", Events.encode(newQuests))
		end
	end
	
	-- Load incomplete quests from DB
	local questsIncomplete = SaveLoad.loadPlayerData(uPlayer.name, "playerQuestsIncomplete")
		
	-- Does this user have incomplete quests?
	if questsIncomplete and questsIncomplete ~= "[]" then
		local newQuests = {}
		questsIncomplete = Events.decode(questsIncomplete) -- Not a fan of this, should probably allocate another variable
		
		-- Convert to hash table for s_uItemInstances
		for i, q in ipairs(questsIncomplete) do
			-- Ensure this UNID is still a valid quest
			if type(q) == "table" and s_uItemProperties[tostring(q.UNID)] then
				uPlayer.quests.incomplete[tostring(i)] = { 
					UNID = q.UNID,
					fulfilled = q.fulfilled,
					fulfillCount = q.fulfillCount
				}
				table.insert(newQuests, {	UNID = q.UNID,
											fulfilled = q.fulfilled,
											fulfillCount = q.fulfillCount,})
			end
			-- TO DO: Figure out a way to preserve the OLD list of incomplete quests
			-- Ensure this UNID is still a valid quest
			-- local gameItem = s_uItemProperties[tostring(q)]
			-- if gameItem then
			-- 	uPlayer.quests.incomplete[tostring(i)] = q
			-- 	table.insert(newQuests, q)
			-- end
		end
		
		-- If a quest has been modified, save off the updated quest
		if #newQuests ~= #questsIncomplete then
			SaveLoad.savePlayerData(uPlayer.name, "playerQuestsIncomplete", Events.encode(newQuests))
		end
	end
end


-- Saved the player's inventory to the DB
savePlayerInventory = function(sPlayerName)
	LOG_FUNCTION_CALL("savePlayerInventory")
	
	local uPlayer = s_uPlayers[sPlayerName]
	
	local playerInventory	= uPlayer.inventory
	local playerArmor		= uPlayer.armor
	local playerBlueprints	= uPlayer.blueprints

	-- Compile a table capable of json encoding for saving
	local saveTable = {}
	local saveInventory = {}
	local saveArmor = {}
	local saveBlueprints = {}
	
	for index, item in pairs(playerInventory) do
		local saveItem = {}
		
		saveItem.UNID = item.UNID
		saveItem.count = item.count
		
		if item.instanceData then
			for i,v in pairs(item.instanceData) do
				-- Save off instanceData if it exists
				if saveItem.instanceData == nil then
					saveItem.instanceData = {}
				end
				saveItem.instanceData[i] = v
			end
		end
		
		saveInventory[tonumber(index)] = saveItem
	end

	for index,item in pairs(playerArmor) do
		local saveItem = {}
		
		saveItem.UNID = item.UNID
		saveItem.count = item.count

		for i, v in pairs(item.instanceData) do
			-- Save off instanceData if it exists
			if saveItem.instanceData == nil then
				saveItem.instanceData = {}
			end
			saveItem.instanceData[i] = v
		end
		
		saveArmor[tonumber(index)] = saveItem
	end

	if playerBlueprints then
		for index,item in pairs(playerBlueprints) do
			local saveItem = {}
			saveItem.UNID = item.UNID
			saveItem.count = item.count
			saveItem.oneUse = item.oneUse
			
			saveBlueprints[tonumber(index)] = saveItem
		end
	end

	saveTable.inventory = saveInventory
	saveTable.armor = saveArmor
	saveTable.blueprints = saveBlueprints
	
	s_tDirtyInventories[sPlayerName] = saveTable
end


savePlayerQuests = function(sPlayerName)
	LOG_FUNCTION_CALL("savePlayerQuests")
	
	local uPlayer = s_uPlayers[sPlayerName]
	if uPlayer and uPlayer.quests then
		-- TODO: Mark dirty and save on close?
		if uPlayer.quests.complete then
			local completeQuests = {}
			-- Convert questsComplete to a indexed table
			for i, questInfo in pairs(uPlayer.quests.complete) do
				local completeQuest = {
					UNID			= questInfo.UNID,
					completeTime	= questInfo.completeTime
				}
				table.insert(completeQuests, completeQuest)
			end
			SaveLoad.savePlayerData(sPlayerName, "playerQuestsComplete", Events.encode(completeQuests))
		end
		
		if uPlayer.quests.incomplete then
			local incompleteQuests = {}
			-- Convert questsComplete to a indexed table
			-- Convert questsComplete to a indexed table
			for i, questInfo in pairs(uPlayer.quests.incomplete) do
				local incompleteQuest = {
					UNID			= questInfo.UNID,
					fulfilled		= questInfo.fulfilled,
					fulfillCount	= questInfo.fulfillCount
				}
				table.insert(incompleteQuests, incompleteQuest)
			end
			SaveLoad.savePlayerData(sPlayerName, "playerQuestsIncomplete", Events.encode(incompleteQuests))
		end
	end
end



-- Records user quest completion metric
recordQuestCompletion = function(sPlayerName, completeQuest)
	LOG_FUNCTION_CALL("recordQuestCompletion")
	if sPlayerName == nil then log("Controller_Player - recordQuestCompletion() sPlayerName is nil."); return end
	if completeQuest == nil then log("Controller_Player - recordQuestCompletion() completeQuest is nil."); return end
	if s_iZoneInstanceID == nil then log("Controller_Player - recordQuestCompletion() s_iZoneInstanceID is nil."); return end
	if s_iZoneType == nil then log("Controller_Player - recordQuestCompletion() s_iZoneType is nil."); return end
	local fullQuestInfo = s_uItemProperties[tostring(completeQuest.UNID)]
	if fullQuestInfo == nil then log("Controller_Player - recordQuestCompletion() completeQuest not found in s_uItemProperties."); return end

	local metricsData = {	
		username		= sPlayerName,
		zoneInstanceId	= s_iZoneInstanceID,
		zoneType		= s_iZoneType,
		questUNID		= completeQuest.UNID,
		questGIGLID		= fullQuestInfo.GIGLID,
		questName		= fullQuestInfo.name,
		completeTime	= fullQuestInfo.completeTime
	}
	
	local rewardUNID = fullQuestInfo.rewardItem and tostring(fullQuestInfo.rewardItem.rewardUNID)
	local rewardItem = rewardUNID and s_uItemProperties[rewardUNID]
	if rewardItem then
		metricsData.rewardItemUNID = rewardUNID
		metricsData.rewardItemGIGLID = rewardItem.GIGLID 
		metricsData.rewardItemName = rewardItem.name
	end					   	
	kgp.scriptRecordMetric("quest_completion", metricsData)
end


removeQuests = function(tQuestUNIDs)
	for name, uPlayer in pairs(s_uPlayers) do
		if uPlayer then
			local questModified =  false
			local activeQuest = s_tActiveQuests[name]
			
			for _, removeUNID in pairs(tQuestUNIDs) do
				for index, questInfo in pairs(uPlayer.quests.incomplete) do
					if questInfo.UNID == removeUNID then
						questModified = true
						uPlayer.quests.incomplete[index] = nil
						if activeQuest and activeQuest[questInfo.UNID] ~= nil then
							activeQuest[questInfo.UNID] = nil
						end
						break
					end
				end
				for index, questInfo in pairs(uPlayer.quests.complete) do
					if questInfo.UNID == removeUNID then
						questModified = true
						uPlayer.quests.complete[index] = nil
						break
					end
				end
			end
			
			if questModified then
				savePlayerQuests(name)
				sendQuestsToClient(name)
			end
			-- Update the quest icon for each quest
			updatePlayerQuestIcons(uPlayer)
		end
	end
end


-- Send a player's quests to the client
sendQuestsToClient = function(sPlayerName)
	LOG_FUNCTION_CALL("sendQuestsToClient")
	
	local uPlayer = s_uPlayers[sPlayerName]
	local clientQuestsIncomplete = {}
	
	if uPlayer.quests and uPlayer.quests.incomplete then
		for index, questInfo in pairs(uPlayer.quests.incomplete) do
			local quest = s_uItemProperties[tostring(questInfo.UNID)]
			
			if quest == nil then -- Invalid quest, need to clean up and remove
				log("Controller_Player - sendQuestsToClient() - Player has invalid incomplete quest UNID["..tostring(questInfo.UNID).."]. This should not have happened.")
				break
			end
			
			index = tonumber(index)
			clientQuestsIncomplete[index] = {UNID = tonumber(questInfo.UNID), fulfilled = questInfo.fulfilled, fulfillCount = questInfo.fulfillCount}
		end
	end
	
	local clientQuestsComplete = {}
	
	if uPlayer.quests and uPlayer.quests.complete then
		for index, questInfo in pairs(uPlayer.quests.complete) do
			local quest = s_uItemProperties[tostring(questInfo.UNID)]
			--local quest = s_uItemProperties[tostring(questInfo)]
			
			if quest == nil then -- Invalid quest, need to clean up and remove
				log("Controller_Player - sendQuestsToClient() - Player has invalid complete quest UNID["..tostring(questInfo.UNID).."]. This should not have happened.")
				break
			end
			
			index = tonumber(index)
			clientQuestsComplete[index] = {UNID = tonumber(questInfo.UNID),	completeTime = questInfo.completeTime}
		end
	end
	
	Events.sendClientEvent("UPDATE_QUESTS", {questsIncomplete = clientQuestsIncomplete, questsComplete = clientQuestsComplete}, uPlayer.ID)
end


-- Sets all quest icons for a given player
updatePlayerQuestIcons = function(uPlayer, resetIcons)
	LOG_FUNCTION_CALL("updatePlayerQuestIcons")
	
	-- Set the quest icon for each quest giver in the zone
	for ID, sharedData in pairs(s_uItemInstances) do
		-- If character
		if sharedData then
			if sharedData.type == "QuestGiver" then
				setQuestIcon(uPlayer, sharedData)
			end
		end
	end
end



-- Sets a quest icon over an object
setQuestIcon = function(uPlayer, object)
	LOG_FUNCTION_CALL("setQuestIcon")
	if uPlayer == nil then log("Controller_Player - setQuestIcon() player is nil."); return end
	if object == nil then log("Controller_Player - setQuestIcon() object is nil."); return end
	if object.quests == nil then log("Controller_Player - setQuestIcon() object.quests is nil."); return end
	
	local questState = getQuestIcon(object.quests, uPlayer)
	local questIcon = GameDefinitions.QUEST_ICONS[questState]
	local sPlayerName = uPlayer.name
	local playerID = uPlayer.ID
	local objectPID = object.PID
	
	if sPlayerName == nil then
		log("ERROR DETECTED - ED-6935 : setQuestIcon player name was nil for player with ID = "..tostring(playerID))
		Debug.printTableError("ERROR DETECTED - ED-6935", player)
		return
	end
	
	if s_tPlayerIcons[sPlayerName] == nil then
		s_tPlayerIcons[sPlayerName] = {}
	end
	local currentIconState = tonumber(s_tPlayerIcons[sPlayerName][objectPID])
	local INDICATOR = GameDefinitions.QUEST_INDICATOR_INFO
	
	if currentIconState then
		kgp.objectUnEquipItemForPlayer(playerID, tonumber(objectPID), currentIconState)
	end
	
	
	if questIcon then
		
		-- Do not show quest icons for followers or spouses that do not belong to the player
		-- Because they are unable to interact with those items.
		shouldShow = true
		
		if(object["followerOf"] and (object["followerOf"] ~= sPlayerName) ) then
			shouldShow = false
		end
		
		if(object["spouseOf"] and (object["spouseOf"] ~= sPlayerName) ) then
			shouldShow = false
		end
		
		if(shouldShow) then
		
			kgp.objectEquipItemForPlayer(playerID, tonumber(objectPID), questIcon, "")
			kgp.objectSetEquippedItemAnimationForPlayer(playerID, tonumber(objectPID), questIcon, INDICATOR.ANIM)
			
			local labelPlacement = object.showLabels and INDICATOR.LABEL_ON or INDICATOR.LABEL_OFF		
			
			kgp.objectRepositionEquippedItemForPlayer(playerID, tonumber(objectPID), questIcon, INDICATOR.ALIGNMENT, labelPlacement, INDICATOR.ROTATION, INDICATOR.SCALE)
			-- Icon state has been changed. Save it 
		
			s_tPlayerIcons[sPlayerName][objectPID] = tostring(questIcon)
		end
	end
	
end



-- Get a quest icon for a player and a set of quests based on the player's completion status of each
getQuestIcon = function(quests, uPlayer)
	LOG_FUNCTION_CALL("getQuestIcon")
	
	local sPlayerName = uPlayer and uPlayer.name
	local questIcon
	local currentTime = kgp.gameGetCurrentTime() or 0
	--currentTime = math.fmod(currentTime, 2629743) --modded down to match client's version
	-- Iterate over each quest
	for _, questUNID in pairs(quests) do
		-- Get the quest
		local quest = s_uItemProperties[tostring(questUNID)]
		if quest then
			-- Get the player's state for this quest
			local questCompletable
			if s_tActiveQuests[sPlayerName] then
				questCompletable = s_tActiveQuests[sPlayerName][questUNID]
			end
			local questCompleted = false
			local prerequisiteCompleted = false
			local cooldownUp = false
			if uPlayer.quests and uPlayer.quests.complete then
				-- Has the player completed this quest and if this quest has a prequisite, has this player completed THAT
				for i, questInfo in pairs(uPlayer.quests.complete) do
					if questInfo.UNID == questUNID then
						questCompleted = true
					
						if quest.repeatable == "true" then
							cooldownUp = ((currentTime - questInfo.completeTime) >= (quest.repeatTime * 60)) or (quest.repeatTime == 0)
						end
					end
					if tonumber(quest.prerequisiteQuest) ~= 0 and tonumber(quest.prerequisiteQuest) == questInfo.UNID then
						prerequisiteCompleted = true
					end
				end
			end
			local questState

			-- Completable
			if questCompletable == true  then
				questState = "COMPLETE"
				local iconIndex = tostring(sPlayerName)..tostring(questUNID)
				if not s_tPlayerIconMessage[iconIndex] and not quest.complete then
					s_tPlayerIconMessage[iconIndex] = true
					--kgp.playerShowStatusInfo(uPlayer.ID, "Quest ready for turn in!", 3, 0, 255, 0)
				end
			-- Not completable (either active or never seen before)
			
			-- Repeatable quests
			elseif quest.repeatable == "true" then
				-- Active
				if questCompletable == false then
					questState = "REP_IN_PROGRESS"
				-- Knows nuthin about it
				else
					-- Are they ready to accept this quest again?
					if questCompleted and cooldownUp then
						questState = "REP_AVAILABLE"
						
					-- If this player's never completed this repeatable, it's available
					elseif not questCompleted then
						questState = "REP_AVAILABLE"
					end
				end
				s_tPlayerIconMessage[tostring(sPlayerName)..tostring(questUNID)] = nil
			-- Normal quest
			else
				-- Active
				if questCompletable == false then
					questState = "IN_PROGRESS"
				-- Knows nuthin about it except I haven't done it
				elseif not questCompleted then
					-- Quest has a prerequisite and you've completed it
					if tonumber(quest.prerequisiteQuest) ~= 0 and prerequisiteCompleted then
						questState = "AVAILABLE"
					-- Quest has no prerequisite
					elseif tonumber(quest.prerequisiteQuest) == 0 then
						questState = "AVAILABLE"
					end
				end
				s_tPlayerIconMessage[tostring(sPlayerName)..tostring(questUNID)] = nil
			end
			-- Prioritize the results
			if questIcon == nil or (questIcon and questState and GameDefinitions.QUEST_ICON_PRIORITY[questState] < GameDefinitions.QUEST_ICON_PRIORITY[questIcon]) then
				questIcon = questState
			end
		end
	end
	return questIcon
end






-- Calculate the damage of a given weapon level with its multiplier applied
calculateArmorRating = function(sPlayerName)
	
	LOG_FUNCTION_CALL("calculateArmorRating")
	
	local armorSet = s_uPlayers[sPlayerName].armor
	
	local totalArmorBonus = 0
	local baseArmorBonus = 0

	for _, item in pairs(armorSet) do
		local itemProperties = item.properties
		local instanceData = item.instanceData
		if itemProperties and itemProperties.level and
			instanceData and instanceData.levelMultiplier and instanceData.durability > 0 then
			
			local armorRating = GameDefinitions.calculateRandomArmorRating(itemProperties.level, instanceData.levelMultiplier / GameDefinitions.PLAYER_INVENTORY_ARMOR_SLOTS)
			armorRating = math.floor(armorRating)
			
			baseArmorBonus = baseArmorBonus + armorRating

			--Is there a bonus to health?
			if itemProperties.bonusHealth then
				armorRating = armorRating * itemProperties.bonusHealth
			else
				armorRating = armorRating * GameDefinitions.ARMOR_MULTIPLIER
			end

			totalArmorBonus = totalArmorBonus + armorRating
		end
	end

	return totalArmorBonus, baseArmorBonus
end




updateDirtyInventories = function()
	LOG_FUNCTION_CALL("updateDirtyInventories")
	for playerName, saveTable in pairs(s_tDirtyInventories) do
		SaveLoad.savePlayerData(playerName, "playerInventory", Events.encode(saveTable))
		s_tDirtyInventories[playerName] = nil
	end
end



deductItemAtSlot = function(uPlayer, slot)
	LOG_FUNCTION_CALL("deductItemAtSlot")

	local sPlayerName = uPlayer and uPlayer.name
	local uInventory = sPlayerName and s_uPlayers[sPlayerName].inventory
	
	if uInventory then
		local item = uInventory[slot]
		if item.count > 0 then
			if item.count == 1 then

				item.UNID = 0
				item.count = 0
				item.properties = {GLID = 0, name = "EMPTY", itemType = GameDefinitions.ITEM_TYPES.GENERIC} 
				item.instanceData = {}

				--This does not work for some reason. properties and instancedata will be nil
				-- inventory[slot] = GameDefinitions.EMPTY_ITEM
			else
				item.count = item.count - 1
			end

			-- TEMP-FUNCTION - Checks if the player's inventory is no longer of size 45
			logInventorySizeError(uPlayer, "deductItemAtSlot()")
			savePlayerInventory(sPlayerName)
			sendInventoryToClient(sPlayerName)
			return true
		end
	end
	
	return false
end



deductItemByUNID = function(uPlayer, UNID, count)
	LOG_FUNCTION_CALL("deductItemByUNID")
	
	local sPlayerName = uPlayer and uPlayer.name
	local uInventory = uPlayer and uPlayer.inventory
	
	if uInventory then
		--First pass determines if we have enough of the given item
		local inventoryCount = count
		for slot, item in pairs(uInventory) do
			if tonumber(item.UNID) == UNID and item.count > 0 then
				inventoryCount = inventoryCount - item.count
				if inventoryCount <= 0 then break end
			end
		end

		if inventoryCount > 0 then return false end -- Not enough of the given item found

		--Second pass removes items from inventory
		for slot, item in pairs(uInventory) do
			if tonumber(item.UNID) == UNID and item.count > 0 then
				local itemCount = item.count
				if itemCount > count then
					item.count = item.count - count
					count = 0
				else
					uInventory[slot] = GameDefinitions.EMPTY_ITEM
					count = count - itemCount
				end
				
				if count <= 0 then
					-- TEMP-FUNCTION - Checks if the player's inventory is no longer of size 45
					logInventorySizeError(player, "deductItemByUNID()")
					savePlayerInventory(sPlayerName)
					sendInventoryToClient(sPlayerName)
					return true
				end
			end
		end
	end
	
	return false
end



-- TEMP-FUNCTION to catch and error log out when a player's inventory is of an invalid size
logInventorySizeError = function(user, functionName)
	local count = 0
	for i, v in pairs(user.inventory) do
		count = count + 1
	end
	if count < 45 then
		log("\n\nTEMP-LOG: INVENTORY COUNT ERROR = Saving Inventory for player["..tostring(user.name).."] in "..tostring(functionName)..". Inventory size["..tostring(count).."]\n\n")
	end
end

