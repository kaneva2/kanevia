--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
-- Kaneva 3D App Development Framework
-------------------------------------------------------------------------------------
-- File name   : Controller_Events.lua
-- Module name : Event System
-- Functions   : (1) Distribute events across VMs
--               (2) Maintain player list and player properties
--               (3) Handle some system-level events
--                      * EVENT-SUB, EVENT-UNSUB  (subscription)
--                      * EVENT-PING              (client ping)
--                      * EVENT-DEBUG             (debugging)
--                      * TELL-ALL, TELL-DEV      (dev console communications)
-- Author      : Yanfeng Chen
-- Created     : 09/2013
-------------------------------------------------------------------------------------

Class.registerAsService("Controller_Events")

local g_players = {}				-- map player NetIDs to players
local g_globalRegistry = {}			-- global event handler registry

function kgp_start(user)
	kgp.scriptSetMessagePolicy(false, true) -- Game world event handling accepts broadcast messages from external worlds
end

-- player tracking upon arrival and departure
function kgp_arrive( user )
	playerInit(user)
end

function kgp_depart( user )
	playerCleanup( user )
end

-- Listen to object-to-object events
function kgp_message( message, sourceId )
	local event = Events.decode( message )
	if event~=nil then
		--Console.Write(Console.DEBUG, "Received kgp_message " .. event._type)
		handleEvent(event, message)
	end
end

-- Listen to client-to-server events
function kgp_menuevent(user, params)
	if params[1] == "GET_TEMPLATE_ID" then
		kgp.playerSendEvent(user, "RETURN_TEMPLATE_ID", "COMMUNITY FRAMEWORK")
		return
	end
	
	if #params<2 then
		return
	end

	--Console.Write( Console.DEBUG, "Received kgp_menuevent " .. params[1] )

	local eventDataEncoded = params[2]

	local header = {_type=params[1], _sender=user, _senderType="PLAYER"}
	local headerEncoded = Events.encode(header)

	-- Treat empty event data table as nil
	if eventDataEncoded=="[]" or eventDataEncoded=="{}" then
		eventDataEncoded = nil
	end

	-- DEBUG ONLY - TO BE REMOVED
	if headerEncoded:sub(1,1)~="{" or headerEncoded:sub(-1,-1)~="}" then
		-- improperly encoded header
		error("JSON encoding algorithm failed: \"" .. headerEncoded .. "\"")
		return
	end
	-- END DEBUG ONLY

	local encoded = headerEncoded

	if eventDataEncoded~=nil then
		if eventDataEncoded:sub(1,1)~="{" or eventDataEncoded:sub(-1,-1)~="}" then
			-- improperly encoded event data
			-- Ignore silently for backward compatibility
			return
		end

		-- concatenate two JSON strings without reencoding
		encoded = eventDataEncoded:sub(1, -2) .. "," .. headerEncoded:sub(2)
	end
	
	handleEvent(params[1], encoded)
end

-- event handling and dispatching
function handleEvent( event, encoded )

	if event==nil or (type(event)~="table" and type(event)~="string") then
		--Console.Write( Console.WARN, "Received invalid message \"" .. encoded .. "\"" )
		return
	end

	local eventType
	if type(event)=="string" then
		eventType = event
		event = nil

		-- Decode some events for local processing
		if 	eventType=="TELL-ALL" or eventType=="TELL-DEVS" or
			eventType=="EVENT-SUB" or eventType=="EVENT-UNSUB" or eventType=="EVENT-DEBUG" or eventType=="EVENT-PING" then

			event = Events.decode(encoded)
			if event==nil then
				-- something is wrong
				error("EventSystem.handleEvent: critical error occurred")
			end
		end
	else
		eventType = event._type
	end

	if eventType=="TELL-ALL" and type(event.text)=="string" then
		tellAllPlayers( event.text, event.who )
		return
	end

	if eventType=="TELL-DEVS" and type(event.text)=="string" then
		tellAllDevelopers( event.text, event.who )
		return
	end

	if eventType=="EVENT-SUB" and type(event.event)=="string" then
		--Console.Write( Console.DEBUG, "Subscription[" .. event.event .. "] " .. tostring(event._sender) )
		addEventListener( event.event, event._sender )
		return
	end

	if eventType=="EVENT-UNSUB" and type(event.event)=="string" then
		removeEventListener( event.event, event._sender )
		return
	end

	if eventType=="EVENT-PING" and type(event.player)=="table" and type(event.player.ID)=="number" then
		--Console.Write(Console.DEBUG, "Received PING from " .. event.player.name .. ", timestamp: " .. tostring(event.timestamp))
		Events.sendClientEvent( "EVENT-PING-RESP", {timestamp=event.timestamp}, event.player.ID )
		return
	end

	if eventType=="EVENT-DEBUG" and type(event.player)=="table" and type(event.player.ID)=="number" then
		debug(event.player)
		return
	end

	if dispatchEvent(eventType, encoded) then
		return
	end

	--Console.Write(Console.WARN, "Received unknown message \"" .. encoded .. "\"")
end

function dispatchEvent(eventType, encoded)
	--Console.Write( Console.DEBUG, "dispatchEvent: eventType=\"" .. eventType )
	if g_globalRegistry[eventType]~=nil then
		for objectId, _ in pairs(g_globalRegistry[eventType]) do
			--Console.Write( Console.DEBUG, "dispatchEvent \"" .. eventType .. "\" to " .. tostring(objectId) )
			kgp.objectSendMessage( objectId, encoded )
		end
	end

	return true
end

-- Add a newly joined player
function playerInit( user )
	local playerName = kgp.playerGetName(user)

	-- Report an error here if playerGetName fails
	if playerName ~= nil then
		-- Create a SharedData object and store it in the player data list
		g_players[user] = Events.getPlayer(user)
	else
		log("ERROR DETECTED - ED-6929 : playerInit : kgp.playerGetName failed on user " .. tostring(user))
	end
end

-- Remove a departed player
function playerCleanup( user )
	g_players[user] = nil
end

-- Register a script as new listener for an event
function addEventListener( eventType, objectId )
	if g_globalRegistry[eventType]==nil then
		g_globalRegistry[eventType] = {}
	end

	g_globalRegistry[eventType][objectId] = kgp.gameGetCurrentTime()
end

-- Remove a script from the listener list of an event
function removeEventListener( eventType, objectId )
	if g_globalRegistry[eventType] then
		g_globalRegistry[eventType][objectId] = nil
	end
end

-- Output messages in dev console for all players in zone
function tellAllPlayers( text, who )
	if who==nil then
		who = "Dispatcher"
	end

	for playerID, _ in pairs(g_players) do
		kgp.playerTell( playerID, text, who, 1 )
	end
end

-- Output messages in dev console for all developers in zone
function tellAllDevelopers( text, who )
	if who==nil then
		who = "Dispatcher"
	end

	for playerID, player in pairs(g_players) do
		if player~=nil then
			local perm = player.perm
			if perm==0 or perm==1 or perm==2 then
				kgp.playerTell( playerID, text, who, 1 )
			end
		end
	end
end

-- Dump debug messages upon developer request
function debug(player)

	local msg = ""
	local perm = player.perm
	if perm==0 or perm==1 or perm==2 then
		-- print debug info
		for et, _ in pairs(g_globalRegistry) do
			msg = msg .. "[" .. et .. "], "
		end
	else
		tellAllDevelopers( "User [" .. tostring(player.name) .. "] attempt to access EventSystem/debug()" )
		msg = "EventSystem greets you"
	end

	kgp.playerTell( player.ID, msg, "Dispatcher", 1 )
end

-- Override Console.write function to use local calls
function Console.Write( level, msg )
	if level <= Console.OutputLevel then
		if level>=Console.DEBUG then
			tellAllDevelopers( msg )
		else
			tellAllPlayers( msg )
		end
	end
end
