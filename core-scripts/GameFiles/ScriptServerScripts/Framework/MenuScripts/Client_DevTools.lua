--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

dofile("LibClient_Common.lua")
dofile("..\\MenuScripts\\CommonFunctions.lua")
dofile("..\\MenuScripts\\DevToolsConstants.lua")

gShowMenu = true
MRULists = {}
hMRUList = nil
hDevConBG = nil
hHints = nil
hScriptNameEdit = nil
hScriptCreateButton = nil
listOwner = nil
gMRUID = 0
gDevConTimer = 0
gDevConOnTop = false
gHintsTimer = 0
gHintsID = 0
gScriptNameEntryVisible = false

gHints = {
	"[`] Toogle Dev Console",
	"[SHIFT-M] Menus",
	"[SHIFT-A] Menu Scripts",
	"[SHIFT-S] Server Scripts",
	"[SHIFT-UP] Hide",
	"[SHIFT-DOWN] Show",
}

function onCreate()
	hMRUList = Dialog_GetControl(gDialogHandle, "lstMRU")
	hDevConBG = Dialog_GetControl(gDialogHandle, "imgDevConBG")
	hHints = Dialog_GetControl(gDialogHandle, "stcHints")
	hScriptNameEdit = Dialog_GetControl(gDialogHandle, "edScriptName")
	hScriptCreateButton = Dialog_GetControl(gDialogHandle, "btnScriptCreate")

	loadMRUData()

	Events.registerHandler( "GET-MRU-RESP", onGetMRUResp )

	KEP_EventRegisterHandler( "AppAssetDownloadHandler", "3DAppAssetDownloadCompleteEvent", KEP.HIGH_PRIO + 1 )

	Dialog_BringToFront(gDialogHandle)

	gShowMenu = false
	updateUI()
end

function Dialog_OnRender(dialogHandle, elapsedSec)
	gDevConTimer = math.max(gDevConTimer - elapsedSec, 0)
	if gDevConTimer==0 and not gDevConOnTop then
		gDevConTimer = 0.5
		sendDevConsoleToBack()
	end

	gHintsTimer = math.max(gHintsTimer - elapsedSec, 0)
	if gHintsTimer==0 then
		gHintsTimer = 5
		rotateHints()
	end
end

function btnPublish_OnButtonClicked( buttonHandle )
	sendDevConsoleToBack()
	if IsDialogOrVariantOpenByName("DevToolsPublish.xml") == 0 then
		local publishMenu = gotoMenu("DevToolsPublish.xml", 0, 0)
		Dialog_BringToFront(publishMenu)
	end
end

function btnMenus_OnButtonClicked( buttonHandle )
	sendDevConsoleToBack()
	showDialog("MenuBrowser.xml")
end

function btnMenuScripts_OnButtonClicked( buttonHandle )
	sendDevConsoleToBack()
	showDialog("MenuScriptBrowser.xml")
end

function btnScripts_OnButtonClicked( buttonHandle )
	sendDevConsoleToBack()
	showDialog("ServerScriptBrowser.xml")
end

function btnShowHide_OnButtonClicked( buttonHandle )
	sendDevConsoleToBack()
	gShowMenu = not gShowMenu
	updateUI()
end

function Dialog_OnKeyDown(dialogHandle, key, bShiftDown, bCtrlDown, bAltDown)
	-- KEP_Log( "keydown: " .. tostring(key) .. ", shift=" .. tostring(bShiftDown) .. ", ctrl=" .. tostring(bCtrlDown) .. ", alt=" .. tostring(bAltDown) )
	if key==192 then	-- apostrophe
		if gDevConOnTop then
			sendDevConsoleToBack()
		else
			bringDevConsoleToFront()
		end
	elseif bShiftDown and not bCtrlDown and not bAltDown then
		if key==string.byte("A") then
			showDialog("MenuScriptBrowser.xml")
		elseif key==string.byte("S") then
			showDialog("ServerScriptBrowser.xml")
		elseif key==string.byte("M") then
			showDialog("MenuBrowser.xml")
		elseif key==38 then		-- SHIFT-UP
			gShowMenu = false
			updateUI()
		elseif key==40 then		-- SHIFT-DOWN
			gShowMenu = true
			updateUI()
		end
	end
end

function AppAssetDownloadHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	local path = KEP_EventDecodeString(event)
	local fileType = KEP_EventDecodeNumber(event)

	if fileType==DevToolsUpload.MENU_SCRIPT or fileType==DevToolsUpload.SAVED_SCRIPT then
		local name = getShortName( fileType, path )
		addToMRU(fileType, name, KEP_GetCurrentEpochTime())
		KEP_ShellOpen( path )
		return KEP.EPR_CONSUMED
	end
end

function addToMRU( type, name, timestamp )
	if MRULists[type]==nil then
		MRULists[type] = {}
	end

	local key = string.upper(name)

	gMRUID = gMRUID + 1

	MRULists[type][key] = {}
	MRULists[type][key].name = name
	MRULists[type][key].type = type
	MRULists[type][key].time = timestamp
	MRULists[type][key].trace = gMRUID
	KEP_Log( "Add to MRULists[" .. type .. "] : " .. MRULists[type][key].name )

	KEP_Log( "Encoded: " .. Events.encode{ type = type, name = name, time = timestamp } )
	KEP_SendMenuEvent( 2, "SET-MRU", Events.encode{ type = type, name = name, time = timestamp } )
end

function editMRUItem( item )
	local baseName = string.gsub(item.name, "[.][^.]+$", "")
	KEP_RequestSourceAssetFromAppServer( item.type, baseName )
end

function getShortName( type, path )
	return string.gsub(path, ".*\\", "")
end

function btnMenuRecent_OnButtonClicked( buttonHandle )
	sendDevConsoleToBack()
	if toggleMRU( buttonHandle ) then
		gCurrentMRUType = DevToolsUpload.MENU_XML
		populateMRUItems( gCurrentMRUType )
	end
end
function btnMenuScriptRecent_OnButtonClicked( buttonHandle )
	sendDevConsoleToBack()
	if toggleMRU( buttonHandle ) then
		gCurrentMRUType = DevToolsUpload.MENU_SCRIPT
		populateMRUItems( gCurrentMRUType )
	end
end
function btnScriptRecent_OnButtonClicked( buttonHandle )
	sendDevConsoleToBack()
	if toggleMRU( buttonHandle ) then
		gCurrentMRUType = DevToolsUpload.SAVED_SCRIPT
		populateMRUItems( gCurrentMRUType )
	end
end

function toggleMRU( buttonHandle )
	if Control_GetVisible( hMRUList )~=0 and (listOwner==buttonHandle or buttonHandle==nil) then
		Control_SetVisible( hMRUList, false )
		ListBox_RemoveAllItems( hMRUList )
		listOwner = nil
		gCurrentMRUType = nil
		return false
	else
		Control_SetVisible( hMRUList, true )
		listOwner = buttonHandle
		return true
	end
end

function populateMRUItems( type )
	ListBox_RemoveAllItems( hMRUList )
	if MRULists[type]~=nil then
		for k, v in pairs(MRULists[type]) do
			ListBox_AddItem( hMRUList, v.name, v.trace )
		end
	end
end

function lstMRU_OnListBoxItemDblClick( listHandle )

	if gCurrentMRUType==nil then
		-- something is wrong
		return
	end

	local item = nil
	local trace = ListBox_GetSelectedItemData( listHandle )
	for k, v in pairs(MRULists[gCurrentMRUType]) do
		if v.trace==trace then
			item = v
		end
	end

	if item==nil then
		-- TODO: report
		return
	end

	if gCurrentMRUType==DevToolsUpload.MENU_XML then

	else
		editMRUItem( item )
		toggleMRU( nil )
	end
end

function loadMRUData()
	KEP_SendMenuEvent( 2, "GET-MRU", Events.encode{} )
end

function onGetMRUResp( event )
	-- reset local MRU lists
	MRULists = {}
	gMRUID = 0

	-- load received data
	for fileType, list in pairs(event.list) do
		for name, timestamp in pairs(list) do
			addToMRU( fileType, name, timestamp )
		end
	end
end

function clearMRUData()
	KEP_SendMenuEvent( 2, "RESET-MRU", Events.encode{} )
	MRULists = {}
	gMRUID = 0
end

function sendDevConsoleToBack()
	gDevConOnTop = false
	local dlg = GetDialogOrVariantByName( "DevConsole.xml" )
	if dlg~=nil then
		Dialog_SendToBack( dlg )
		Control_SetVisible( hDevConBG, false )
	end
end

function bringDevConsoleToFront()
	gDevConOnTop = true
	local dlg = GetDialogOrVariantByName( "DevConsole.xml" )
	if dlg~=nil then
		local ofsX = Dialog_GetLocationX( gDialogHandle )
		local ofsY = Dialog_GetLocationY( gDialogHandle )
		local x = Dialog_GetLocationX( dlg )
		local y = Dialog_GetLocationY( dlg )
		local w = Dialog_GetWidth( dlg )
		local h = Dialog_GetHeight( dlg )
		local sizer = Dialog_GetControl(dlg, "imgResize")
		if sizer~=nil then
			w = Control_GetLocationX( sizer ) + Control_GetWidth( sizer )
			h = Control_GetLocationY( sizer ) + Control_GetHeight( sizer )
		end
		Control_SetLocation( hDevConBG, x-ofsX, y-ofsY-18 )
		Control_SetSize( hDevConBG, w, h+18 )
		Control_SetVisible( hDevConBG, true )
		Dialog_BringToFront( gDialogHandle )
		Dialog_BringToFront( dlg )
	end
end

function showDialog( dialogName )
	local dlg = GetDialogOrVariantByName(dialogName)
	if dlg == nil then
		dlg = gotoMenu(dialogName,0,0)
	end

	Dialog_BringToFront(dlg)
end

function rotateHints()
	gHintsID = gHintsID + 1
	if gHintsID > #gHints then
		gHintsID = 1
	end

	Static_SetText(Control_GetStatic( hHints ), gHints[gHintsID] )
end

function updateUI()
	local buttonHandle = gHandles["btnShowHide"]
	if gShowMenu then
		local x = Dialog_GetLocationX( gDialogHandle )
		local y = Dialog_GetLocationY( gDialogHandle )
		Dialog_SetLocationFormat( gDialogHandle, 5 )	-- center + top
		Dialog_SetLocation( gDialogHandle, x, 0 )
		Static_SetText(Button_GetStatic(buttonHandle), "hide")
	else
		local x = Dialog_GetLocationX( gDialogHandle )
		local y = Dialog_GetLocationY( gDialogHandle )
		Dialog_SetLocationFormat( gDialogHandle, 4 )	-- center only
		Dialog_SetLocation( gDialogHandle, x, - Dialog_GetHeight( gDialogHandle ) )
		Static_SetText(Button_GetStatic(buttonHandle), "devtools")
	end
end

function btnScriptAdd_OnButtonClicked( buttonHandle )
	sendDevConsoleToBack()
	gScriptNameEntryVisible = not gScriptNameEntryVisible
	Control_SetVisible( hScriptCreateButton, gScriptNameEntryVisible )
	Control_SetVisible( hScriptNameEdit, gScriptNameEntryVisible )
end

function btnScriptCreate_OnButtonClicked( buttonHandle )
	sendDevConsoleToBack()
	local name = EditBox_GetText(Control_GetEditBox( hScriptNameEdit ))
	if name==nil then
		KEP_MessageBox( "Error reading input" )
		return
	end

	if name=="" then
		KEP_MessageBox( "Please enter a name for the new script" )
		return
	end

	if string.gsub(name, "[A-Za-z0-9_-]", "")~="" then
		KEP_MessageBox( "Invalid script name. Please use only letters, numbers, underscore or dash" )
		return
	end

	if string.upper(string.sub(name, -4))~=".LUA" then
		name = name .. ".lua"
	end

	KEP_Log( "Creating temporary file: " .. name )
	local file = KEP_FileOpen( name, "w", "ScriptData" )
	if file==nil then
		KEP_MessageBox( "Error creating temporary file" )
		return
	end

	KEPFile_WriteLine( file, "-- " .. name )
	KEP_FileClose( file )

	gScriptNameEntryVisible = false
	Control_SetVisible( hScriptCreateButton, false )
	Control_SetVisible( hScriptNameEdit, false )

	local menuDir = KEP_GetMenuDir()
	local fullPath = string.gsub(menuDir, "\\Menus$", "\\ScriptData\\" .. name)

	KEP_Log( "Import script from " .. fullPath )
	KEP_Import3DAppAsset(DevToolsUpload.SAVED_SCRIPT, fullPath)

	showDialog("ServerScriptBrowser.xml")
end
