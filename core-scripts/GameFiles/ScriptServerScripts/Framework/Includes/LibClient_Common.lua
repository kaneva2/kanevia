--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- LibClient_Common.lua

-- Includes everything essential to framework development
dofile("..\\Scripts\\KEP.lua")

local s_iZoneInstanceID = nil


-- include/require/dofile
function include(fileName)
	if Types and Types.validateArguments ~= nil then		-- Allow "Lib_Types.lua" to be included without validation. Chicken and egg problem.
		Types.validateArguments{ {fileName, "fileName", "string"} }
	end
	dofile(fileName)
end

-- low level log function: call corresponding logging API depending on the environment
function log(message)
	if s_iZoneInstanceID == nil then s_iZoneInstanceID = KEP_GetZoneInstanceId() end
	
	if s_iZoneInstanceID then
		message = "["..tostring(s_iZoneInstanceID).."] "..message
	end
	
	KEP_Log(message)
end

-- TODO: This function needs to move into a Utilities library.
-- Copies all entried of table 2 into table 1
function deepCopy(copyTable)
	local tempTable = {}
	for index, value in pairs(copyTable) do
		if type(value) == "table" or type(value) == "userdata" then
			tempTable[index] = deepCopy(value)
		else
			tempTable[index] = value
		end
	end
	return tempTable
end

include "..\\MenuScripts\\LibClient_Environment.lua"
include "..\\MenuScripts\\Lib_Toolbox.lua"
include "..\\MenuScripts\\Lib_Types.lua"
include "..\\MenuScripts\\Lib_Debug.lua"
include "..\\MenuScripts\\LibClient_Events.lua"
