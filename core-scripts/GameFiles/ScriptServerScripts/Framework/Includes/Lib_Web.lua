--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_Web.lua


local function getDataByTag(strXmlData, strTag, strPattern)
	local strStartTag	= "<" .. strTag .. ">"
	local strEndTag		= "</" .. strTag .. ">"
	
	return string.match(strXmlData, strStartTag .. strPattern .. strEndTag)
end


Web = {
	getDataByTag		= getDataByTag,
}


