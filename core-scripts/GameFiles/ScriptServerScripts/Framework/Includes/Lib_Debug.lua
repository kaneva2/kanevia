--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_Debug.lua

Debug = {}

-----------------------------------------
-- LOCAL CONSTANTS
-----------------------------------------
local ALLOW_USERDATA		= Environment and Environment.isRunningOnServer and Environment.isRunningOnServer()	-- Set this to true if working on the server side and to false if working on the client	
local DEFAULT_INDENT_SIZE	= 4
local LOG_MODE				= Environment and Environment.isRunningInDebugMode and Environment.isRunningInDebugMode()

-----------------------------------------
-- LOCAL FUNCTION FORWARD DECLARATIONS
-----------------------------------------
local printFunctionInfoAtDepth -- (iDepth)
local printTableInternal -- function(name, t, maxDepth, iFilterIndex)
local printTableRecursive -- function(t, indent, depth)

-----------------------------------------
-- LIBRARY FUNCTIONS
-----------------------------------------

Debug.printStack = function(bSuppressLog)
	local strOutput = 			"\n\t-------------------------------"
	strOutput = strOutput ..	"\n\t------- CALLSTACK START -------"
	local strFuncOutput = ""
	local bSuccess = false
	local iDepth = 3 --> Start with the depth of the function that is making the call
	
	repeat
		bSuccess, strFuncOutput = printFunctionInfoAtDepth(iDepth)
		strOutput = strOutput .. strFuncOutput

		iDepth = iDepth + 1
		
	until bSuccess == false
	
	strOutput = strOutput ..	"\n\t-------- CALLSTACK END --------"
	strOutput = strOutput ..	"\n\t-------------------------------"
	
	if not bSuppressLog then
		log(strOutput)
	end
	
	return strOutput
end

-- Print the contents of the given table "t", but only if we are in debug mode
Debug.printTable = function(name, t, maxDepth, iFilterIndex)
	if LOG_MODE then
		printTableInternal(name, t, maxDepth, iFilterIndex)
	end
end

-- Print the contents of the given table "t", regardless of context
Debug.printTableError = function(name, t, maxDepth, iFilterIndex)
	printTableInternal(name, t, maxDepth, iFilterIndex)
end

-----------------------------------------
-- LOCAL FUNCTION DEFINITIONS
-----------------------------------------

-- Print the contents of the given table "t".
-- Hierarchical structure is reflected by indentation
-- The table is traversed up to the "maxDepth" depth
printTableInternal = function (name, t, maxDepth, iFilterIndex)
	if not name then name = "" end -- use an empty string, if no name provided
	-- Return if the passed filter index is disabled
	if( isFilterIndexEnabled and not isFilterIndexEnabled(iFilterIndex) ) then
		return
	end
	
	maxDepth = maxDepth or 0
	log("PRINT TABLE START - " .. name .. ":"  .. tostring(t))
	if type(t) == "table" or (ALLOW_USERDATA and type(t) == "userdata") then
		log("{")
			printTableRecursive(t, DEFAULT_INDENT_SIZE, maxDepth)
		log("}")
	end
	log("PRINT TABLE FINISH - " .. name .. ":"  .. tostring(t))
end

printFunctionInfoAtDepth = function(iDepth)
	local strOutput = ""
	
	local tFuncInfo = debug.getinfo(iDepth)
	
	if type(tFuncInfo) == "table" then
		local sourceFile = tFuncInfo.short_src
		if sourceFile then
			strOutput = strOutput .. "\n-----------------------------------------------------------------------------------"
			strOutput = strOutput .. "\nSTACK ENTRY: -" .. tostring(iDepth - 2)
			strOutput = strOutput .. "\n\t - Function   \t: " .. tostring(tFuncInfo.name)
			strOutput = strOutput .. "\n\t - Source File\t: " .. tostring(sourceFile)
			strOutput = strOutput .. "\n\t - Line       \t: " .. tostring(tFuncInfo.currentline)
			strOutput = strOutput .. "\n\t - Variables  \t: "
			
			local paramIndex = 1
			local name, value
			repeat
				name, value = debug.getlocal(iDepth, paramIndex)
				if name then
					local formattedName = string.format('%-20s', tostring(name))
					strOutput = strOutput .. "\n\t\t" .. formattedName .. " -\t" .. tostring(value)
				end
				
				paramIndex = paramIndex + 1
			until name == nil
		end
	end
	
	return tFuncInfo ~= nil, strOutput
end

-- Helper function for Debug.printTable
printTableRecursive = function(t, indent, depth)
	indent = indent or DEFAULT_INDENT_SIZE
	local leadSpace = ""
	for i = 1, indent do
		leadSpace = " " .. leadSpace
	end
		
	local canRecurse = depth ~= 1
		
	--log("Recursive depth: "..tostring(depth) .. ", Can recurse: " .. tostring(canRecurse))

	for key,value in pairs(t) do
		if( type(key) == "string" ) then
			log(leadSpace .. "'" .. tostring(key) .. "' : " .. tostring(value))
		else
			log(leadSpace .. tostring(key) .. " : " .. tostring(value))
		end
		if canRecurse and (type(value) == "table" or (ALLOW_USERDATA and type(value) == "userdata")) then
			log(leadSpace .. "{")
				printTableRecursive(value, indent + DEFAULT_INDENT_SIZE, depth - 1)
			log(leadSpace .. "}")
		end
	end
end

-----------------------------------------
-- CONDITIONAL DEFINITIONS
-----------------------------------------

if not LOG_MODE then
	Debug.log = function() end
	Debug.generateCustomLog = function()
		return function() end
	end
	Debug.enableLogForFilter = function() end
	Debug.resetAllFilters = function() end
	
else
	-----------------------------------------
	-- GLOBAL CONSTANTS
	-----------------------------------------
	-- NADA!
	
	-----------------------------------------
	-- LOCAL CONSTANTS
	-----------------------------------------
	-- NADA!
	
	-----------------------------------------
	-- LOCAL VARIABLES
	-----------------------------------------
	local s_filterTable = {}
	local s_iNextFilterIndex = 1

	-----------------------------------------
	-- LOCAL FUNCTION DEFINITIONS
	-----------------------------------------
	local isFilterIndexEnabled = nil -- function(filterIndex)

	-----------------------------------------
	-- LIBRARY FUNCTIONS
	-----------------------------------------
	
	-- Only log calls with enabled filters get displayed.
	-- All filters are enabled by default
	Debug.log = function(text, iFilterIndex) 
		if( isFilterIndexEnabled(iFilterIndex) ) then
			log(text)
		end
	end
	
	Debug.generateCustomLog = function(headerText)
		local iFilterIndex = s_iNextFilterIndex
		s_iNextFilterIndex = s_iNextFilterIndex + 1
		local retFunc = function(text)
			if( isFilterIndexEnabled(iFilterIndex) ) then
				log(tostring(headerText) .. tostring(text)) 
			end
		end
		
		return retFunc, iFilterIndex
	end
	
	Debug.enableLogForFilter = function(iFilterIndex, enabled)
		s_filterTable[tonumber(iFilterIndex)] = enabled ~= false
	end
	
	Debug.resetAllFilters = function()
		s_filterTable = {}
	end	

	isFilterIndexEnabled = function(iFilterIndex)
		local iIndex = tonumber(iFilterIndex)
		if iIndex == nil then
			return true
		end
		
		local filterEnabled = s_filterTable[iIndex]
		return filterEnabled == nil or filterEnabled == true
	end
end








