--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

include("Lib_ZoneInfo.lua")

-- Lib_SaveLoad.lua
-- Helper library to ensure the correct save / load API is called, based on current environment
SaveLoad = {}

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local ZoneInfo 			= _G.ZoneInfo
-------------------------------------------------------------------------------------
-- API WRAPPER FUNCTIONS
-------------------------------------------------------------------------------------

-- Player APIs
function SaveLoad.savePlayerData(playerName, attribute, value, zoneInstanceId, zoneType)
	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	return kgp.scriptSavePlayerData(playerName, attribute, tostring(value), zoneInstanceId or parentID or 0, zoneType or parentType or 0)
end

function SaveLoad.loadAllPlayerData(playerName, zoneInstanceId, zoneType)
	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	return kgp.scriptLoadAllPlayerData(playerName, zoneInstanceId or parentID or 0, zoneType or parentType or 0)
end

function SaveLoad.loadPlayerData(playerName, attribute, zoneInstanceId, zoneType)
	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	return kgp.scriptLoadPlayerData(playerName, attribute, zoneInstanceId or parentID or 0, zoneType or parentType or 0)
end

function SaveLoad.deletePlayerData(playerName, attribute, zoneInstanceId, zoneType)
	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	return kgp.scriptDeletePlayerData(playerName, attribute, zoneInstanceId or parentID or 0, zoneType or parentType or 0)
end

function SaveLoad.deleteAllPlayerData(playerName, zoneInstanceId, zoneType)
	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	return kgp.scriptDeleteAllPlayerData(playerName, zoneInstanceId or parentID or 0, zoneType or parentType or 0)
end

-- Game APIs
function SaveLoad.saveGameData(attribute, value, zoneInstanceId, zoneType)
	return kgp.scriptSaveGameData(attribute, tostring(value), zoneInstanceId or 0, zoneType or 0)
end

--[[ -- PJD - Deprecated 07/29/16
function SaveLoad.loadAllGameData()
	return kgp.scriptLoadAllGameData()
end
]]--

function SaveLoad.loadGameData(attribute, zoneInstanceId, zoneType)
	return kgp.scriptLoadGameData(attribute, zoneInstanceId or 0, zoneType or 0)
end

function SaveLoad.deleteGameData(attribute, zoneInstanceId, zoneType)
	return kgp.scriptDeleteGameData(attribute, zoneInstanceId or 0, zoneType or 0)
end

--[[ -- PJD - Deprecated 07/29/16
function SaveLoad.deleteAllGameData()
	return kgp.scriptDeleteAllGameData()
end
]]--