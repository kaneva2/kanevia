--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_Colors.lua



-- check if argument is a proper color value and return normalized value if valid
local function validateColor(val, paramName, index, default)
	val = val or default

	if type(val) == "table" then
		if val.r == nil and #val == 3 then
			return {r = val[1], g = val[2], b = val[3]}
		end
		if val.r ~= nil and val.g ~= nil and val.b ~= nil then
			return val
		end
	end

	-- failed
	error("Argument #" .. tostring(index) .. " \"" .. tostring(paramName) .. "\" is invalid, expected: { r, g, b }", 3)
end


-- compose a color value
local function createColor(r, g, b)
	return {r = r, g = g, b = b}
end




Colors = {
	validateColor	= validateColor,
	createColor		= createColor,
}
