--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

include("Lib_ZoneInfo.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local ZoneInfo 			= _G.ZoneInfo

local DEFAULT_NEXT_UNID = 2000
local s_nestedKeys = {
	behaviorParams = true,
	dialogPages = true,
	loot = true,
	trade = true,
	trades = true,
	inputs = true,
	addGLIDS = true,
	femaleGLIDS = true,
	maleGLIDS = true,
	waypointInfo = true,
	rewardItem = true,
	requiredItem = true,
	proxReq = true,
	particleEffects = true,
	particleEffectsMine = true,
	particleEffectsTheirs = true,
	middleStages = true,
	quests = true,
	quest = true,
	playerPositions = true,
	singles = true,
}

local s_defaultItems = {}
-- -- System Shortcuts (except for the Gem Box cause reasons)
s_defaultItems[1] = Events.decode('{"name":"Master Game System","itemType":"settings","GLID":4490497,"level":1,"GIGLID":0,"behavior":"shortcut","behaviorParams":{"shortcutType":"setting","menu":"Framework_WorldSettings","creatorOnly":true},"description":"Master game system settings."}')
-- TODO: 2 - Placeables System Shortcut (Jones)
s_defaultItems[2] = Events.decode('{"name":"Top Worlds","itemType":"settings","GLID":4509353,"level":1,"GIGLID":0,"behavior":"shortcut","behaviorParams":{"menu":"BrowsePlaces","creatorOnly":false,"shortcutType":"topWorlds"},"description":"Behold all the marvelous Worlds that are the Top."}')
s_defaultItems[4] = Events.decode('{"name":"Gem Exchange","itemType":"settings","GLID":4509353,"level":1,"GIGLID":0,"behavior":"shortcut","behaviorParams":{"menu":"Framework_GemExchange","creatorOnly":false,"modeLabels":{"Player":["name"]}},"description":"Exchange gems collected from all over Kaneva for rewards."}')
s_defaultItems[5]  = Events.decode('{"name":"Gem Chest","itemType":"gembox","GLID":4575150,"rarity":"Common","level":1,"GIGLID":0,"description":"It is a chest with a gem in it! Put it in your toolbelt and use it to open it!"}')
s_defaultItems[6] = Events.decode('{"name": "World Coin Converter","GIGLID": 1000179,"GIID": 6,"itemType": "shortcut","GLID": 2811,"rarity": "Common","level": 1,"BundleGlid": 0,"behavior": "shortcut","behaviorParams": {"menu":"CurrencyConversion","creatorOnly":false,"modeLabels":{"Player":["name"],"Creator":["name"]}},"description": "Convert your World Coins to something slightly more valuable!"}')
s_defaultItems[7] = Events.decode('{"name":"Donation Box","itemType":"settings","GLID":4509353,"level":1,"GIGLID":0,"behavior":"shortcut","behaviorParams":{"shortcutType":"donation","menu":"Donation","creatorOnly":false,"modeLabels":{"Player":["name"]}},"description":"A donation box used to show appreciation for your hard work."}')
s_defaultItems[8] = Events.decode('{"name":"World Creation Station","itemType":"settings","GLID":4509353,"level":1,"GIGLID":0,"behavior":"shortcut","behaviorParams":{"menu":"CreateYourWorld","creatorOnly":false,"modeLabels":{"Player":["name"]}},"description":"Create a World!"}')
s_defaultItems[9] = Events.decode('{"name":"Travel Center","itemType":"settings","GLID":4509353,"level":1,"GIGLID":0,"behavior":"shortcut","behaviorParams":{"shortcutType":"travel","menu":"BrowsePlaces","creatorOnly":false,"modeLabels":{"Player":["name"]}},"description":"I can show you the world. Shining, shimmering, splendid."}')
s_defaultItems[10] = Events.decode('{"name":"Waypoint","itemType":"waypoint","GLID":4276230,"level":1,"GIGLID":0,"behavior":"waypoint","description":"Waypoint used by system objects."}')

-- Placeables
s_defaultItems[11] = Events.decode('{"name":"Wood Wall 8x16","itemType":"placeable","targetable":true,"GLID":4464120,"rarity":"Common","level":3,"GIGLID":0,"description":"Keep out unwanted visitors with a sturdy wooden wall."}')
s_defaultItems[12] = Events.decode('{"name":"Wood Wall 4x16","itemType":"placeable","targetable":true,"GLID":4464125,"rarity":"Common","level":3,"GIGLID":0,"description":"Keep out unwanted visitors with a sturdy wooden wall."}')
s_defaultItems[13] = Events.decode('{"name":"Wood Wall 8x8","itemType":"placeable","targetable":true,"GLID":4468234,"rarity":"Common","level":3,"GIGLID":0,"description":"Keep out unwanted visitors with a sturdy wooden wall."}')
s_defaultItems[14] = Events.decode('{"name":"Wood Wall 4x8","itemType":"placeable","targetable":true,"GLID":4464127,"rarity":"Common","level":3,"GIGLID":0,"description":"Keep out unwanted visitors with a sturdy wooden wall."}')
s_defaultItems[15] = Events.decode('{"name":"Wood Floor 16x16","itemType":"placeable","targetable":true,"GLID":4464130,"rarity":"Common","level":3,"GIGLID":0,"description":"Make for sure footing with a sturdy wooden floor."}')
s_defaultItems[16] = Events.decode('{"name":"Wood Floor 8x8","itemType":"placeable","targetable":true,"GLID":4468403,"rarity":"Common","level":3,"GIGLID":0,"description":"Make for sure footing with a sturdy wooden floor."}')
s_defaultItems[17] = Events.decode('{"name":"Wood Block 4x4x4","itemType":"placeable","targetable":true,"GLID":4468402,"rarity":"Common","level":3,"GIGLID":0,"description":"Get creative with an all-purpose sturdy wooden block."}')
s_defaultItems[18] = Events.decode('{"name":"Wood Door 8x8","itemType":"placeable","targetable":true,"GLID":4464142,"rarity":"Common","level":3,"GIGLID":0,"behavior":"door","behaviorParams":{"openAnim":4468409,"closeAnim":4468410,"openedAnim":4468417,"animTime":1},"description":"Keep out unwanted visitors with a sturdy wooden door."}')
s_defaultItems[19] = Events.decode('{"name":"Wood Stairs 8x8","itemType":"placeable","targetable":true,"GLID":4464357,"rarity":"Common","level":3,"GIGLID":0,"description":"Rise to new heights with these sturdy wooden stairs."}')

s_defaultItems[20] = Events.decode('{"name":"Fire Pit","itemType":"placeable","targetable":true,"GLID":4465604,"rarity":"Common","level":3,"GIGLID":0,"behavior":"campfire","behaviorParams":{"name":"Fire Pit","craftingResource":"Fire Pit","craftingProximity":25},"description":"Cook food and craft items with this comforting campfire."}')
s_defaultItems[21] = Events.decode('{"name":"Chest","itemType":"placeable","targetable":true,"GLID":4468596,"rarity":"Common","level":3,"GIGLID":0,"behavior":"loot","description":"Store up to 20 slots of loot with a sturdy wooden chest."}')

s_defaultItems[22] = Events.decode('{"name":"Concrete Wall 8x16","itemType":"placeable","targetable":true,"GLID":4482606,"rarity":"Rare","level":4,"GIGLID":0,"description":"Ensure your security with a hardened concrete wall."}')
s_defaultItems[23] = Events.decode('{"name":"Concrete Wall 4x16","itemType":"placeable","targetable":true,"GLID":4482585,"rarity":"Rare","level":4,"GIGLID":0,"description":"Ensure your security with a hardened concrete wall."}')
s_defaultItems[24] = Events.decode('{"name":"Concrete Wall 8x8","itemType":"placeable","targetable":true,"GLID":4482599,"rarity":"Rare","level":4,"GIGLID":0,"description":"Ensure your security with a hardened concrete wall."}')
s_defaultItems[25] = Events.decode('{"name":"Concrete Wall 4x8","itemType":"placeable","targetable":true,"GLID":4482557,"rarity":"Rare","level":4,"GIGLID":0,"description":"Ensure your security with a hardened concrete wall."}')
s_defaultItems[26] = Events.decode('{"name":"Concrete Floor 16x16","itemType":"placeable","targetable":true,"GLID":4482543,"rarity":"Rare","level":4,"GIGLID":0,"description":"Stand with confidence on a hardened concrete floor."}')
s_defaultItems[27] = Events.decode('{"name":"Concrete Floor 8x8","itemType":"placeable","targetable":true,"GLID":4482144,"rarity":"Rare","level":4,"GIGLID":0,"description":"Stand with confidence on a hardened concrete floor."}')
s_defaultItems[28] = Events.decode('{"name":"Concrete Block 4x4x4","itemType":"placeable","targetable":true,"GLID":4482620,"rarity":"Rare","level":4,"GIGLID":0,"description":"Build boldly with an all-purpose hardened concrete block."}')
s_defaultItems[29] = Events.decode('{"name":"Concrete Stairs 8x8","itemType":"placeable","targetable":true,"GLID":4487523,"rarity":"Rare","level":4,"GIGLID":0,"description":"Step it up with these hardened concrete stairs."}')
s_defaultItems[30] = Events.decode('{"name":"Concrete Door 8x8","itemType":"placeable","targetable":true,"GLID":4481077,"rarity":"Rare","level":4,"GIGLID":0,"behavior":"door","behaviorParams":{"openAnim":4468409,"closeAnim":4468410,"openedAnim":4468417,"animTime":1},"description":"Ensure your security with a hardened concrete door."}')

s_defaultItems[31] = Events.decode('{"name":"Metal Wall 8x16","itemType":"placeable","targetable":true,"GLID":4484809,"rarity":"Rare","level":5,"GIGLID":0,"description":"Provide powerful protection with an ironclad metal wall."}')
s_defaultItems[32] = Events.decode('{"name":"Metal Wall 4x16","itemType":"placeable","targetable":true,"GLID":4484783,"rarity":"Rare","level":5,"GIGLID":0,"description":"Provide powerful protection with an ironclad metal wall."}')
s_defaultItems[33] = Events.decode('{"name":"Metal Wall 8x8","itemType":"placeable","targetable":true,"GLID":4484778,"rarity":"Rare","level":5,"GIGLID":0,"description":"Provide powerful protection with an ironclad metal wall."}')
s_defaultItems[34] = Events.decode('{"name":"Metal Wall 4x8","itemType":"placeable","targetable":true,"GLID":4484782,"rarity":"Rare","level":5,"GIGLID":0,"description":"Provide powerful protection with an ironclad metal wall."}')
s_defaultItems[35] = Events.decode('{"name":"Metal Floor 16x16","itemType":"placeable","targetable":true,"GLID":4485129,"rarity":"Rare","level":5,"GIGLID":0,"description":"Walk with conviction on an ironclad metal floor."}')
s_defaultItems[36] = Events.decode('{"name":"Metal Floor 8x8","itemType":"placeable","targetable":true,"GLID":4485128,"rarity":"Rare","level":5,"GIGLID":0,"description":"Walk with conviction on an ironclad metal floor."}')
s_defaultItems[37] = Events.decode('{"name":"Metal Block 4x4x4","itemType":"placeable","targetable":true,"GLID":4487615,"rarity":"Rare","level":5,"GIGLID":0,"description":"Realize your dreams with an all-purpose ironclad metal block."}')
s_defaultItems[38] = Events.decode('{"name":"Metal Stairs 8x8","itemType":"placeable","targetable":true,"GLID":4487521,"rarity":"Rare","level":5,"GIGLID":0,"description":"Reach for the stars with these ironclad metal stairs."}')
s_defaultItems[39] = Events.decode('{"name":"Metal Door 8x8","itemType":"placeable","targetable":true,"GLID":4485243,"rarity":"Rare","level":5,"GIGLID":0,"behavior":"door","behaviorParams":{"openAnim":4468409,"closeAnim":4468410,"openedAnim":4468417,"animTime":1},"description":"Provide powerful protection with an ironclad metal door."}')

s_defaultItems[40] = Events.decode('{"name":"Lockable Metal Door 8x8","itemType":"placeable","targetable":true,"GLID":4495488,"rarity":"Rare","level":5,"GIGLID":0,"behavior":"door","behaviorParams":{"locked":true,"openAnim":4468409,"closeAnim":4468410,"openedAnim":4468417,"animTime":1},"description":"Keep your valuables safe with a locked metal door."}')
s_defaultItems[41] = Events.decode('{"name":"Metal Safe","itemType":"placeable","targetable":false,"GLID":4498129,"rarity":"Very Rare","level":5,"GIGLID":0,"behavior":"loot","behaviorParams":{"locked":true},"description":"Store up to 20 slots of loot with a locked metal safe."}')

s_defaultItems[42] = Events.decode('{"name":"Teleporter System","itemType":"placeable","targetable":false,"GLID":4496120,"rarity":"Rare","level":3,"GIGLID":0,"behavior":"teleporter","behaviorParams":{"idleParticle":4363676,"chargeParticle":4496129},"description":"Rip your atoms apart and assemble them in another place with this linked teleporter system."}')
s_defaultItems[43] = Events.decode('{"name":"Platform System","itemType":"placeable","targetable":false,"GLID":4497863,"rarity":"Rare","level":3,"GIGLID":0,"behavior":"mover","behaviorParams":{},"description":"Create your own elevators and trams with this moving platform system."}')

s_defaultItems[44] = Events.decode('{"name":"Guard Tower","itemType":"placeable","targetable":true,"GLID":4523335,"rarity":"Never","level":5,"GIGLID":0,"description":"A formidable tower of defense that can keep its occupants high and dry above the waves of endless zombies."}')

s_defaultItems[45] = Events.decode('{"name":"Lava Trap","itemType":"placeable","targetable":true,"GLID":4548287,"rarity":"Never","level":3,"GIGLID":0,"behavior":"trap","behaviorParams":{"idleParticle":3498042,"idleSound":4561114,"damageSound":4561115,"percentDamage":10},"description":"Its a trap!"}')
s_defaultItems[46] = Events.decode('{"name":"Electric Fence Trap","itemType":"placeable","targetable":true,"GLID":4548198,"rarity":"Never","level":3,"GIGLID":0,"behavior":"trap","behaviorParams":{"idleParticle":4422160,"idleSound":4556522,"damageSound":4561113,"percentDamage":10},"description":"Its a trap!"}')-- TODO: 46-50 - Reserved for future default placeables

s_defaultItems[47] = Events.decode('{"name":"Repair Bench","itemType":"placeable","targetable":true,"GLID":4551263,"rarity":"Common","level":3,"GIGLID":0,"behavior":"repair","behaviorParams":{"repairUNID":124, "actorName":"Repair Bench"}, "description":"A sturdy repair bench used for repairing Armor, Weapons, and Tools."}')

-- Spawners
s_defaultItems[61] = Events.decode('{"name":"All Purpose Loot Spawner","itemType":"spawner","GLID":4465631,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_loot","respawnTime":120,"spawnRadius":1800,"spawnGLID":4468596,"behaviorParams":{"loot":{"generic":true,"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true}},"description":"Place on the ground to spawn a variety of loot for Players to collect."}')
s_defaultItems[62] = Events.decode('{"name":"Player Spawner","itemType":"spawner","GLID":4465630,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_player","description":"Place on the ground to provide a location for Players to respawn after death."}')
s_defaultItems[63] = Events.decode('{"name":"Zombie Spawner","itemType":"spawner","GLID":4465632,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_character","respawnTime":300,"spawnRadius":1800,"behaviorParams":{"maxSpawns":1,"spawnUNID":201},"description":"Place on the ground to spawn AI from the Character System."}')
s_defaultItems[64] = Events.decode('{"name":"Chicken Spawner","itemType":"spawner","GLID":4465632,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_character","respawnTime":300,"spawnRadius":1800,"behaviorParams":{"maxSpawns":1,"spawnUNID":211},"description":"Place on the ground to spawn AI from the Character System."}')
s_defaultItems[65] = Events.decode('{"name":"Tree Spawner","itemType":"spawner","GLID":4478234,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_harvestable","respawnTime":300,"spawnRadius":1800,"behaviorParams":{"maxSpawns":1,"spawnUNID":261},"description":"Place on the ground to spawn a tree for harvesting."}')
s_defaultItems[66] = Events.decode('{"name":"Boulder Spawner","itemType":"spawner","GLID":4478234,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_harvestable","respawnTime":300,"spawnRadius":1800,"behaviorParams":{"maxSpawns":1,"spawnUNID":262},"description":"Place on the ground to spawn a boulder for harvesting."}')
s_defaultItems[67] = Events.decode('{"name":"Scrap Metal Spawner","itemType":"spawner","GLID":4478234,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_harvestable","respawnTime":300,"spawnRadius":1800,"behaviorParams":{"maxSpawns":1,"spawnUNID":263},"description":"Place on the ground to spawn some scrap metal for harvesting."}')
s_defaultItems[68] = Events.decode('{"name":"NPC Female Spawner","itemType":"spawner","GLID":4465632,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_character","respawnTime":300,"spawnRadius":1800,"behaviorParams":{"maxSpawns":1,"spawnUNID":212},"description":"Place on the ground to spawn an NPC from the Character System."}')
s_defaultItems[69] = Events.decode('{"name":"NPC Male Spawner","itemType":"spawner","GLID":4465632,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_character","respawnTime":300,"spawnRadius":1800,"behaviorParams":{"maxSpawns":1,"spawnUNID":213},"description":"Place on the ground to spawn an NPC from the Character System."}')
s_defaultItems[70] = Events.decode('{"name":"Robot Spawner","itemType":"spawner","GLID":4465632,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_character","respawnTime":300,"spawnRadius":1800,"behaviorParams":{"maxSpawns":1,"spawnUNID":214},"description":"Place on the ground to spawn AI from the Character System."}')
s_defaultItems[71] = Events.decode('{"name":"Lobby Spawner","itemType":"spawner","GLID":4529421,"rarity":"Common","level":1,"GIGLID":0,"behavior":"spawner_player","behaviorParams":{"newPlayer":true},"description":"Place on the ground to provide a location for new Players to spawn into the world."}')
-- TODO: 72-80 - Reserved for future default spawners

-- Loot
s_defaultItems[80]  = Events.decode('{"name": "Health Pill","itemType": "ammo","GLID": 4588774,"rarity": "Common","level": 1,"GIGLID":0,"description": "Contains the feel good."}')
s_defaultItems[81]  = Events.decode('{"name":"Mac-10 Magazine","itemType":"ammo","GLID":4465706,"rarity":"Common","level":1,"GIGLID":0,"description":"A magazine of 9mm ammunition used in the Mac-10 sub-machinegun."}')
s_defaultItems[82]  = Events.decode('{"name":"Box of Spas-12 Shells","itemType":"ammo","GLID":4464407,"rarity":"Common","level":1,"GIGLID":0,"description":"A box of .00 buck shells used in the Spas-12 shotgun."}')
s_defaultItems[83]  = Events.decode('{"name":"Wood","itemType":"generic","GLID":4470121,"rarity":"Common","level":1,"GIGLID":0,"description":"A split piece of wood used for crafting items."}')
s_defaultItems[84]  = Events.decode('{"name":"Uncooked Chicken","itemType":"consumable","GLID":4465627,"addGLIDS":[4470670,4470682],"rarity":"Common","level":1,"consumeType":"energy","consumeValue":25,"description":"Use a fire pit to cook this large chicken breast."}')

s_defaultItems[85]  = Events.decode('{"name":"Cheap Head Armor","itemType":"armor","GLID":4471003,"femaleGLIDS":[4472098],"maleGLIDS":[4471003],"rarity":"Rare","level":1,"slotType":"head","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[86]  = Events.decode('{"name":"Cheap Chest Armor","itemType":"armor","GLID":4471002,"femaleGLIDS":[4472097],"maleGLIDS":[4471002],"rarity":"Rare","level":1,"slotType":"chest","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[87]  = Events.decode('{"name":"Cheap Arms Armor","itemType":"armor","GLID":4471000,"femaleGLIDS":[4472095,4472096,4472157],"maleGLIDS":[4471000,4471001,4471042],"rarity":"Rare","level":1,"slotType":"arms","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[88]  = Events.decode('{"name":"Cheap Legs Armor","itemType":"armor","GLID":4471008,"femaleGLIDS":[4472099,4472100,4472101,4472102,4472154],"maleGLIDS":[4471008,4471005,4471006,4471007,4471046],"rarity":"Rare","level":1,"slotType":"legs","description":"Armor provides Players both health and some protection from damage."}')

s_defaultItems[89]  = Events.decode('{"name":"Tarnished Head Armor","itemType":"armor","GLID":4470643,"femaleGLIDS":[4472082],"maleGLIDS":[4470643],"rarity":"Rare","level":2,"slotType":"head","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[90]  = Events.decode('{"name":"Tarnished Chest Armor","itemType":"armor","GLID":4470642,"femaleGLIDS":[4472081],"maleGLIDS":[4470642],"rarity":"Rare","level":2,"slotType":"chest","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[91]  = Events.decode('{"name":"Tarnished Arms Armor","itemType":"armor","GLID":4470640,"femaleGLIDS":[4472079,4472080,4472158],"maleGLIDS":[4470641,4471041,4470640],"rarity":"Rare","level":2,"slotType":"arms","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[92]  = Events.decode('{"name":"Tarnished Legs Armor","itemType":"armor","GLID":4470644,"femaleGLIDS":[4472083,4472084,4472085,4472086,4472154],"maleGLIDS":[4470645,4470647,4470648,4471046,4470644],"rarity":"Rare","level":2,"slotType":"legs","description":"Armor provides Players both health and some protection from damage."}')

s_defaultItems[93]  = Events.decode('{"name":"Standard Head Armor","itemType":"armor","GLID":4471016,"femaleGLIDS":[4472116],"maleGLIDS":[4471016],"rarity":"Rare","level":3,"slotType":"head","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[94]  = Events.decode('{"name":"Standard Chest Armor","itemType":"armor","GLID":4471015,"femaleGLIDS":[4472115],"maleGLIDS":[4471015],"rarity":"Rare","level":3,"slotType":"chest","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[95]  = Events.decode('{"name":"Standard Arms Armor","itemType":"armor","GLID":4471013,"femaleGLIDS":[4472113,4472114,4472156],"maleGLIDS":[4471014,4471043,4471013],"rarity":"Rare","level":3,"slotType":"arms","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[96]  = Events.decode('{"name":"Standard Legs Armor","itemType":"armor","GLID":4471017,"femaleGLIDS":[4472118,4472119,4472120,4472121,4472154],"maleGLIDS":[4471018,4471019,4471021,4471046,4471017],"rarity":"Rare","level":3,"slotType":"legs","description":"Armor provides Players both health and some protection from damage."}')

s_defaultItems[97]  = Events.decode('{"name":"Excellent Head Armor","itemType":"armor","GLID":4471034,"femaleGLIDS":[4472135],"maleGLIDS":[4471034],"rarity":"Rare","level":4,"slotType":"head","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[98]  = Events.decode('{"name":"Excellent Chest Armor","itemType":"armor","GLID":4471033,"femaleGLIDS":[4472134],"maleGLIDS":[4471033],"rarity":"Rare","level":4,"slotType":"chest","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[99]  = Events.decode('{"name":"Excellent Arms Armor","itemType":"armor","GLID":4471031,"femaleGLIDS":[4472132,4472133,4472155],"maleGLIDS":[4471032,4471044,4471031],"rarity":"Rare","level":4,"slotType":"arms","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[100] = Events.decode('{"name":"Excellent Legs Armor","itemType":"armor","GLID":4471035,"femaleGLIDS":[4472136,4472137,4472138,4472139,4472154],"maleGLIDS":[4471036,4471037,4471038,4471046,4471035],"rarity":"Rare","level":4,"slotType":"legs","description":"Armor provides Players both health and some protection from damage."}')

s_defaultItems[101] = Events.decode('{"name":"Peerless Head Armor","itemType":"armor","GLID":4470637,"femaleGLIDS":[4472073],"maleGLIDS":[4470637],"rarity":"Rare","level":5,"slotType":"head","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[102] = Events.decode('{"name":"Peerless Chest Armor","itemType":"armor","GLID":4470631,"femaleGLIDS":[4472072],"maleGLIDS":[4470631],"rarity":"Rare","level":5,"slotType":"chest","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[103] = Events.decode('{"name":"Peerless Arms Armor","itemType":"armor","GLID":4470630,"femaleGLIDS":[4472070,4472071,4472159],"maleGLIDS":[4470629,4471040,4470630],"rarity":"Rare","level":5,"slotType":"arms","description":"Armor provides Players both health and some protection from damage."}')
s_defaultItems[104] = Events.decode('{"name":"Peerless Legs Armor","itemType":"armor","GLID":4470633,"femaleGLIDS":[4472074,4472075,4472076,4472077,4472154],"maleGLIDS":[4470634,4470635,4470636,4471046,4470633],"rarity":"Rare","level":5,"slotType":"legs","description":"Armor provides Players both health and some protection from damage."}')

s_defaultItems[105] = Events.decode('{"name":"Cheap Mac-10","itemType":"weapon","GLID":4468633,"soundGLID":4505666,"rarity":"Rare","level":1,"animationSet":"One-Handed Ranged","coneAngle":30,"bonusDamage":0.5,"rateOfFire":0.5,"range":300,"ammoType":81,"description":"Create your own doorways with this extreme-RPM bullet hose."}')
s_defaultItems[106] = Events.decode('{"name":"Cheap Spas-12","itemType":"weapon","GLID":4468424,"soundGLID":4505664,"rarity":"Rare","level":1,"animationSet":"Two-Handed Ranged","coneAngle":50,"bonusDamage":1.5,"rateOfFire":1,"range":300,"ammoType":82,"description":"No door, wall, or sea of zombies is a barrier to someone with a Spas-12 shotgun in their hands."}')
s_defaultItems[107] = Events.decode('{"name":"Cheap Katana","itemType":"weapon","GLID":4470948,"rarity":"Rare","level":1,"animationSet":"Two-Handed Melee","coneAngle":50,"bonusDamage":1.5,"rateOfFire":1,"range":6,"description":"The design of the Katana has remained unchanged for over 600 years...a testament to the fact that you cannot improve upon perfection."}')

s_defaultItems[108] = Events.decode('{"name":"Tarnished Mac-10","itemType":"weapon","GLID":4468633,"soundGLID":4505666,"rarity":"Rare","level":2,"animationSet":"One-Handed Ranged","coneAngle":30,"bonusDamage":0.5,"rateOfFire":0.5,"range":300,"ammoType":81,"description":"Create your own doorways with this extreme-RPM bullet hose."}')
s_defaultItems[109] = Events.decode('{"name":"Tarnished Spas-12","itemType":"weapon","GLID":4468424,"soundGLID":4505664,"rarity":"Rare","level":2,"animationSet":"Two-Handed Ranged","coneAngle":50,"bonusDamage":1.5,"rateOfFire":1,"range":300,"ammoType":82,"description":"No door, wall, or sea of zombies is a barrier to someone with a Spas-12 shotgun in their hands."}')
s_defaultItems[110] = Events.decode('{"name":"Tarnished Katana","itemType":"weapon","GLID":4470948,"rarity":"Rare","level":2,"animationSet":"Two-Handed Melee","coneAngle":50,"bonusDamage":1.5,"rateOfFire":1,"range":6,"description":"The design of the Katana has remained unchanged for over 600 years...a testament to the fact that you cannot improve upon perfection."}')

s_defaultItems[111] = Events.decode('{"name":"Standard Mac-10","itemType":"weapon","GLID":4468633,"soundGLID":4505666,"rarity":"Rare","level":3,"animationSet":"One-Handed Ranged","coneAngle":30,"bonusDamage":0.5,"rateOfFire":0.5,"range":300,"ammoType":81,"description":"Create your own doorways with this extreme-RPM bullet hose."}')
s_defaultItems[112] = Events.decode('{"name":"Standard Spas-12","itemType":"weapon","GLID":4468424,"soundGLID":4505664,"rarity":"Rare","level":3,"animationSet":"Two-Handed Ranged","coneAngle":50,"bonusDamage":1.5,"rateOfFire":1,"range":300,"ammoType":82,"description":"No door, wall, or sea of zombies is a barrier to someone with a Spas-12 shotgun in their hands."}')
s_defaultItems[113] = Events.decode('{"name":"Standard Katana","itemType":"weapon","GLID":4470948,"rarity":"Rare","level":3,"animationSet":"Two-Handed Melee","coneAngle":50,"bonusDamage":1.5,"rateOfFire":1,"range":6,"description":"The design of the Katana has remained unchanged for over 600 years...a testament to the fact that you cannot improve upon perfection."}')

s_defaultItems[114] = Events.decode('{"name":"Excellent Mac-10","itemType":"weapon","GLID":4468633,"soundGLID":4505666,"rarity":"Rare","level":4,"animationSet":"One-Handed Ranged","coneAngle":30,"bonusDamage":0.5,"rateOfFire":0.5,"range":300,"ammoType":81,"description":"Create your own doorways with this extreme-RPM bullet hose."}')
s_defaultItems[115] = Events.decode('{"name":"Excellent Spas-12","itemType":"weapon","GLID":4468424,"soundGLID":4505664,"rarity":"Rare","level":4,"animationSet":"Two-Handed Ranged","coneAngle":50,"bonusDamage":1.5,"rateOfFire":1,"range":300,"ammoType":82,"description":"No door, wall, or sea of zombies is a barrier to someone with a Spas-12 shotgun in their hands."}')
s_defaultItems[116] = Events.decode('{"name":"Excellent Katana","itemType":"weapon","GLID":4470948,"rarity":"Rare","level":4,"animationSet":"Two-Handed Melee","coneAngle":50,"bonusDamage":1.5,"rateOfFire":1,"range":6,"description":"The design of the Katana has remained unchanged for over 600 years...a testament to the fact that you cannot improve upon perfection."}')

s_defaultItems[117] = Events.decode('{"name":"Peerless Mac-10","itemType":"weapon","GLID":4468633,"soundGLID":4505666,"rarity":"Rare","level":5,"animationSet":"One-Handed Ranged","coneAngle":30,"bonusDamage":0.5,"rateOfFire":0.5,"range":300,"ammoType":81,"description":"Create your own doorways with this extreme-RPM bullet hose."}')
s_defaultItems[118] = Events.decode('{"name":"Peerless Spas-12","itemType":"weapon","GLID":4468424,"soundGLID":4505664,"rarity":"Rare","level":5,"animationSet":"Two-Handed Ranged","coneAngle":50,"bonusDamage":1.5,"rateOfFire":1,"range":300,"ammoType":82,"description":"No door, wall, or sea of zombies is a barrier to someone with a Spas-12 shotgun in their hands."}')
s_defaultItems[119] = Events.decode('{"name":"Peerless Katana","itemType":"weapon","GLID":4470948,"rarity":"Rare","level":5,"animationSet":"Two-Handed Melee","coneAngle":50,"bonusDamage":1.5,"rateOfFire":1,"range":6,"description":"The design of the Katana has remained unchanged for over 600 years...a testament to the fact that you cannot improve upon perfection."}')

s_defaultItems[120] = Events.decode('{"name":"Cooked Chicken","itemType":"consumable","GLID":4465626,"addGLIDS":[4470683,4470684],"rarity":"Common","level":1,"consumeType":"energy","consumeValue":50,"description":"Fire roasted chicken."}')
s_defaultItems[121] = Events.decode('{"name":"Can of Soda","itemType":"consumable","GLID":4465628,"addGLIDS":[4470685,4470686],"rarity":"Common","level":1,"consumeType":"energy","consumeValue":25,"description":"Refreshing carbonated drink."}')
s_defaultItems[122] = Events.decode('{"name":"Can of Tuna","itemType":"consumable","GLID":4465629,"addGLIDS":[4470687,4470689],"rarity":"Common","level":1,"consumeType":"energy","consumeValue":50,"description":"Dolphin-safe albacore tuna."}')

s_defaultItems[123]  = Events.decode('{"name":"Stone","itemType":"generic","GLID":4490122,"rarity":"Rare","level":1,"GIGLID":0,"description":"A large chunk of stone used for crafting items."}')
s_defaultItems[124]  = Events.decode('{"name":"Scrap Metal","itemType":"generic","GLID":4491585,"rarity":"Rare","level":1,"GIGLID":0,"description":"A twisted plate of metal used for crafting items."}')

s_defaultItems[125] = Events.decode('{"name":"Axe","itemType":"weapon","GLID":4495449,"rarity":"Common","level":1,"animationSet":"Two-Handed Melee","coneAngle":50,"bonusDamage":1.0,"rateOfFire":1,"range":6,"description":"A sharpened axe that seems custom made for splitting wood... or other things."}')

s_defaultItems[126] = Events.decode('{"name":"Combination Lock","itemType":"generic","GLID":4495501,"rarity":"Rare","level":1,"GIGLID":0,"stackSize":1,"description":"A lock used for crafting locked metal doors that are only opened by their owners."}')

s_defaultItems[127] = Events.decode('{"name":"Cheap M4 Carbine","itemType":"weapon","GLID":4503707,"soundGLID":4505666,"rarity":"Rare","level":1,"animationSet":"Two-Handed Ranged","coneAngle":30,"bonusDamage":1.5,"rateOfFire":0.5,"range":1500,"ammoType":137,"description":"An assault rifle."}')
s_defaultItems[128] = Events.decode('{"name":"Cheap Sniper Rifle","itemType":"weapon","GLID":4503532,"soundGLID":4505664,"rarity":"Rare","level":1,"animationSet":"Two-Handed Ranged","coneAngle":20,"bonusDamage":2.0,"rateOfFire":2.5,"range":2400,"ammoType":138,"description":"The MSG90 sniper rifle.  Huntin time!"}')
s_defaultItems[129] = Events.decode('{"name":"Tarnished M4 Carbine","itemType":"weapon","GLID":4503707,"soundGLID":4505666,"rarity":"Rare","level":2,"animationSet":"Two-Handed Ranged","coneAngle":30,"bonusDamage":1.5,"rateOfFire":0.5,"range":1500,"ammoType":137,"description":"An assault rifle."}')
s_defaultItems[130] = Events.decode('{"name":"Tarnished Sniper Rifle","itemType":"weapon","GLID":4503532,"soundGLID":4505664,"rarity":"Rare","level":2,"animationSet":"Two-Handed Ranged","coneAngle":20,"bonusDamage":2.0,"rateOfFire":2.5,"range":2400,"ammoType":138,"description":"The MSG90 sniper rifle.  Huntin time!"}')
s_defaultItems[131] = Events.decode('{"name":"Standard M4 Carbine","itemType":"weapon","GLID":4503707,"soundGLID":4505666,"rarity":"Rare","level":3,"animationSet":"Two-Handed Ranged","coneAngle":30,"bonusDamage":1.5,"rateOfFire":0.5,"range":1500,"ammoType":137,"description":"An assault rifle."}')
s_defaultItems[132] = Events.decode('{"name":"Standard Sniper Rifle","itemType":"weapon","GLID":4503532,"soundGLID":4505664,"rarity":"Rare","level":3,"animationSet":"Two-Handed Ranged","coneAngle":20,"bonusDamage":2.0,"rateOfFire":2.5,"range":2400,"ammoType":138,"description":"The MSG90 sniper rifle.  Huntin time!"}')
s_defaultItems[133] = Events.decode('{"name":"Excellent M4 Carbine","itemType":"weapon","GLID":4503707,"soundGLID":4505666,"rarity":"Rare","level":4,"animationSet":"Two-Handed Ranged","coneAngle":30,"bonusDamage":1.5,"rateOfFire":0.5,"range":1500,"ammoType":137,"description":"An assault rifle."}')
s_defaultItems[134] = Events.decode('{"name":"Excellent Sniper Rifle","itemType":"weapon","GLID":4503532,"soundGLID":4505664,"rarity":"Rare","level":4,"animationSet":"Two-Handed Ranged","coneAngle":20,"bonusDamage":2.0,"rateOfFire":2.5,"range":2400,"ammoType":138,"description":"The MSG90 sniper rifle.  Huntin time!"}')
s_defaultItems[135] = Events.decode('{"name":"Peerless M4 Carbine","itemType":"weapon","GLID":4503707,"soundGLID":4505666,"rarity":"Rare","level":5,"animationSet":"Two-Handed Ranged","coneAngle":30,"bonusDamage":1.5,"rateOfFire":0.5,"range":1500,"ammoType":137,"description":"An assault rifle."}')
s_defaultItems[136] = Events.decode('{"name":"Peerless Sniper Rifle","itemType":"weapon","GLID":4503532,"soundGLID":4505664,"rarity":"Rare","level":5,"animationSet":"Two-Handed Ranged","coneAngle":20,"bonusDamage":2.0,"rateOfFire":2.5,"range":2400,"ammoType":138,"description":"The MSG90 sniper rifle.  Huntin time!"}')

s_defaultItems[137]  = Events.decode('{"name":"5.56 Magazine","itemType":"ammo","GLID":4505161,"rarity":"Common","level":1,"GIGLID":0,"description":"A magazine of 5.56mm ammo for the M4 Carbine."}')
s_defaultItems[138]  = Events.decode('{"name":"7.62 Magazine","itemType":"ammo","GLID":4505164,"rarity":"Common","level":1,"GIGLID":0,"description":"A magazine of 7.62mm ammo for the Sniper Rifle."}')

s_defaultItems[139]  = Events.decode('{"name":"Gas Can","itemType":"generic","GLID":4507263,"rarity":"Rare","level":1,"GIGLID":0,"description":"A can of fuel.  No worries, it is not volatile."}')
s_defaultItems[140]  = Events.decode('{"name":"World Coin","itemType":"generic","GLID":4524061,"rarity":"Rare","level":1,"GIGLID":0,"stackSize":999,"description":"A coin that can be used to buy loot from vendors."}')

s_defaultItems[141] = Events.decode('{"name":"Sledgehammer","itemType":"weapon","GLID":4541896,"rarity":"Common","level":2,"animationSet":"Two-Handed Melee","coneAngle":50,"bonusDamage":1.0,"rateOfFire":1,"range":6,"description":"A heavy hammer for smashing stones and skulls."}')
s_defaultItems[142] = Events.decode('{"name":"Crowbar","itemType":"weapon","GLID":4539879,"rarity":"Common","level":3,"animationSet":"Two-Handed Melee","coneAngle":50,"bonusDamage":1.0,"rateOfFire":1,"range":6,"description":"A metal crowbar used to harvest scrap metal from hunks of junk."}')

s_defaultItems[143] = Events.decode('{"name": "Damaged Health Injector","itemType": "weapon","GLID": 4588770,"rarity": "Rare","level": 1,"ammoType": "80","animationSet": "One-Handed Ranged","bonusDamage": "0.5","coneAngle": "30","description": "Provides the feel good.","range": "300","rateOfFire": "0.5","soundGLID": "4505666","soundRange": "300"}')
s_defaultItems[144] = Events.decode('{"name": "Tarnished Health Injector","itemType": "weapon","GLID": 4588770,"rarity": "Rare","level": 2,"ammoType": "80","animationSet": "One-Handed Ranged","bonusDamage": "0.5","coneAngle": "30","description": "Provides the feel good.","range": "300","rateOfFire": "0.5","soundGLID": "4505666","soundRange": "300"}')
s_defaultItems[145] = Events.decode('{"name": "Standard Health Injector","itemType": "weapon","GLID": 4588770,"rarity": "Rare","level": 3,"ammoType": "80","animationSet": "One-Handed Ranged","bonusDamage": "0.5","coneAngle": "30","description": "Provides the feel good.","range": "300","rateOfFire": "0.5","soundGLID": "4505666","soundRange": "300"}')
s_defaultItems[146] = Events.decode('{"name": "Excellent Health Injector","itemType": "weapon","GLID": 4588770,"rarity": "Rare","level": 4,"ammoType": "80","animationSet": "One-Handed Ranged","bonusDamage": "0.5","coneAngle": "30","description": "Provides the feel good.","range": "300","rateOfFire": "0.5","soundGLID": "4505666","soundRange": "300"}')
s_defaultItems[147] = Events.decode('{"name": "Peerless Health Injector","itemType": "weapon","GLID": 4588770,"rarity": "Rare","level": 5,"ammoType": "80","animationSet": "One-Handed Ranged","bonusDamage": "0.5","coneAngle": "30","description": "Provides the feel good.","range": "300","rateOfFire": "0.5","soundGLID": "4505666","soundRange": "300"}')
s_defaultItems[148] = Events.decode('{"name": "Cheap Paintbrush","GIGLID": 1000192,"GIID": 148,"itemType": "weapon","GLID": 4612780,"rarity": "Rare","level": 1,"BundleGlid": 1500130,"animationSet": "Tool","bonusDamage": "0","coneAngle": "50","description": "A paintbrush used to add textures.","paintWeapon": "true","range": "10","rateOfFire": "1"}')
s_defaultItems[149] = Events.decode('{"name": "Tarnished Paintbrush","GIGLID": 1000194,"GIID": 149,"itemType": "weapon","GLID": 4612780,"rarity": "Rare","level": 2,"BundleGlid": 1500132,"animationSet": "Tool","bonusDamage": "0","coneAngle": "50","description": "A paintbrush used to add textures.","paintWeapon": "true","range": "10","rateOfFire": "1"}')
s_defaultItems[150] = Events.decode('{"name": "Standard Paintbrush","GIGLID": 1000196,"GIID": 150,"itemType": "weapon","GLID": 4612780,"rarity": "Rare","level": 3,"BundleGlid": 1500134,"animationSet": "Tool","bonusDamage": "0","coneAngle": "50","description": "A paintbrush used to add textures.","paintWeapon": "true","range": "10","rateOfFire": "1"}')
s_defaultItems[151] = Events.decode('{"name": "Excellent Paintbrush","GIGLID": 1000197,"GIID": 151,"itemType": "weapon","GLID": 4612780,"rarity": "Rare","level": 4,"BundleGlid": 1500135,"animationSet": "Tool","bonusDamage": "0","coneAngle": "50","description": "A paintbrush used to add textures.","paintWeapon": "true","range": "10","rateOfFire": "1"}')
s_defaultItems[152] = Events.decode('{"name": "Peerless Paintbrush","GIGLID": 1000198,"GIID": 152,"itemType": "weapon","GLID": 4612780,"rarity": "Rare","level": 5,"BundleGlid": 1500136,"animationSet": "Tool","bonusDamage": "0","coneAngle": "50","description": "A paintbrush used to add textures.","paintWeapon": "true","range": "10","rateOfFire": "1"}')

-- TODO: 141-200 reserved for future default loot

-- Characters
s_defaultItems[201] = Events.decode('{"name":"Zombie 1","itemType":"character","GLID":4195001,"rarity":"Common","level":1,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":1,"runSpeed":5,"attackRange":3,"attackSound":4522644,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death":4154955}},"description":"Not the smartest monsters in the world, maybe that\'s why they keep asking for brains?"}')
s_defaultItems[202] = Events.decode('{"name":"Boss Zombie 1","itemType":"character","GLID":4470911,"rarity":"Common","level":1,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":10,"runSpeed":5,"attackRange":3,"attackSound":4522644,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death":4154955}},"description":"Not the smartest monsters in the world, maybe that\'s why they keep asking for brains?"}')
s_defaultItems[203] = Events.decode('{"name":"Zombie 2","itemType":"character","GLID":4195001,"rarity":"Common","level":2,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":1,"runSpeed":5,"attackRange":3,"attackSound":4522644,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death": 4154955}},"description":"Not the smartest monsters in the world, maybe that\'s why they keep asking for brains?"}')
s_defaultItems[204] = Events.decode('{"name":"Boss Zombie 2","itemType":"character","GLID":4470911,"rarity":"Common","level":2,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":10,"runSpeed":5,"attackRange":3,"attackSound":4522644,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death":4154955}},"description":"Not the smartest monsters in the world, maybe that\'s why they keep asking for brains?"}')
s_defaultItems[205] = Events.decode('{"name":"Zombie 3","itemType":"character","GLID":4195001,"rarity":"Common","level":3,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":1,"runSpeed":5,"attackRange":3,"attackSound":4522644,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death": 4154955}},"description":"Not the smartest monsters in the world, maybe that\'s why they keep asking for brains?"}')
s_defaultItems[206] = Events.decode('{"name":"Boss Zombie 3","itemType":"character","GLID":4470911,"rarity":"Common","level":3,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":10,"runSpeed":5,"attackRange":3,"attackSound":4522644,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death":4154955}},"description":"Not the smartest monsters in the world, maybe that\'s why they keep asking for brains?"}')
s_defaultItems[207] = Events.decode('{"name":"Zombie 4","itemType":"character","GLID":4195001,"rarity":"Common","level":4,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":1,"runSpeed":5,"attackRange":3,"attackSound":4522644,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death": 4154955}},"description":"Not the smartest monsters in the world, maybe that\'s why they keep asking for brains?"}')
s_defaultItems[208] = Events.decode('{"name":"Boss Zombie 4","itemType":"character","GLID":4470911,"rarity":"Common","level":4,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":10,"runSpeed":5,"attackRange":3,"attackSound":4522644,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death":4154955}},"description":"Not the smartest monsters in the world, maybe that\'s why they keep asking for brains?"}')
s_defaultItems[209] = Events.decode('{"name":"Zombie 5","itemType":"character","GLID":4195001,"rarity":"Common","level":5,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":1,"runSpeed":5,"attackRange":3,"attackSound":4522644,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death": 4154955}},"description":"Not the smartest monsters in the world, maybe that\'s why they keep asking for brains?"}')
s_defaultItems[210] = Events.decode('{"name":"Boss Zombie 5","itemType":"character","GLID":4470911,"rarity":"Common","level":5,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":10,"runSpeed":5,"attackRange":3,"attackSound":4522644,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death":4154955}},"description":"Not the smartest monsters in the world, maybe that\'s why they keep asking for brains?"}')
s_defaultItems[211] = Events.decode('{"name":"Chicken","itemType":"character","GLID":4478309,"rarity":"Common","level":1,"GIGLID":0,"behavior":"animal","behaviorParams":{"spawnGLID":4465627},"loot":{"limitItems":{"UNID":84,"limit":5},"ammo":false,"armor":false,"weapon":false,"consumable":false,"generic":false,"singles":{"UNID1":5,"UNID2":3,"UNID3":5}},"behaviorParams":{"animations":{"idle":0,"moving":4478310,"death":0},"walkSpeed":3,"runSpeed":12},"description":"Chickens are tasty when cooked, but are not as satisfying when eaten raw."}')
s_defaultItems[212] = Events.decode('{"name":"NPC Female","itemType":"character","GLID":4286277,"rarity":"Common","level":1,"GIGLID":0,"behavior":"actor","behaviorParams":{"actorName":"NPC Female","actorTitle":"Information","dialog":"Welcome to Kaneva!", "animation":3931355},"description":"A friendly face to impart information."}')
s_defaultItems[213] = Events.decode('{"name":"NPC Male","itemType":"character","GLID":4310445,"rarity":"Common","level":1,"GIGLID":0,"behavior":"actor","behaviorParams":{"actorName":"NPC Male","actorTitle":"Information","dialog":"Welcome to Kaneva!","animation":3563579},"description":"A friendly face to impart information."}')
s_defaultItems[214] = Events.decode('{"name":"Robot 1","itemType":"character","GLID":4515797,"rarity":"Common","level":1,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":1,"runSpeed":5,"attackRange":3,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death":4154955}},"description":"I wonder if its name is ERROR?"}')
s_defaultItems[215] = Events.decode('{"name":"Boss Robot 1","itemType":"character","GLID":4515797,"rarity":"Common","level":1,"GIGLID":0,"behavior":"monster","loot":{"generic":true,"singles":{},"armor":true,"ammo":true,"consumable":true,"weapon":true,"blueprint":true},"behaviorParams":{"wanderRange":25,"healthMultiplier":10,"runSpeed":5,"attackRange":3,"walkSpeed":2,"aggroRange":40,"animations":{"idle":4162553,"moving":4195012,"attacking":4350206,"death":4154955}},"description":"I wonder if its name is ERROR?"}')
s_defaultItems[216] = Events.decode('{"name":"Vendor","itemType":"character","GLID":4514556,"rarity":"Common","level":1,"GIGLID":0,"behavior":"vendor","behaviorParams":{"timed":false, "actorName":"Vendor"},"trades":[{"output":122, "input":140, "cost":1},{"output":121, "input":140, "cost":1},{"output":105, "input":124, "cost":10},{"output":81, "input":124, "cost":1}], "description":"A friendly loot vendor for selling items."}')
s_defaultItems[217] = Events.decode('{"name":"Timed Vendor","itemType":"character","GLID":4514556,"rarity":"Common","level":1,"GIGLID":0,"behavior":"vendor","behaviorParams":{"timed":true, "actorName":"Timed Vendor"},"trades":[{"output":122, "input":140, "cost":1, "time":5},{"output":121, "input":140, "cost":1, "time":10},{"output":105, "input":124, "cost":10, "time":5},{"output":81, "input":124, "cost":1, "time":5}], "description":"A friendly timed loot vendor for selling items."}')

s_defaultItems[218] = Events.decode('{"name":"Bob the Quest Giver","itemType":"character","GLID":4310445,"rarity":"Common","level":1,"GIGLID":0,"behavior":"quest_giver","behaviorParams":{"actorName":"Bob the Quest Giver","actorTitle":"Quest Giver Supreme","dialog":"Check out these sweet quests","animation":3563579,"quests":[15,16,17]},"description":"He is the bringer of ADVENTURE!"}')

s_defaultItems[219] = Events.decode('{"name":"Random Loot Vendor","itemType":"character","GLID":4600898,"rarity":"Common","level":1,"GIGLID":0,"behavior":"vendor","behaviorParams":{"actorName":"Random Loot Vendor","actorTitle":"Random Loot Vendor","random":true},"description":"A loot vendor for selling a random item.","loot":{"singles":[{"UNID":140,"drop":50,"quantity":1},{"UNID":140,"drop":10,"quantity":10},{"UNID":140,"drop":0.1,"quantity":100}],"generic":false,"armor":false,"ammo":false,"consumable":true,"weapon":false},"trade":[{"input":140,"cost":1}]}')

s_defaultItems[220] = Events.decode('{"name":"Lucy The Dog","itemType":"character","GLID":4375990,"rarity":"Common","level":1,"GIGLID":0,"behavior":"pet","stackSize":1,"behaviorParams":{"walkSpeed":10,"runSpeed":25,"animations":{"idleAnim":4375993,"walkAnim":4378086,"runAnim":4376013,"attackAnim":4376008,"deathAnim":4375993},"particles":{"idlePart":0,"followPart":0,"chasePart":0,"attackPart":0,"deathPart":0},"sounds":{"idleSound":0,"followSound":0,"chaseSound":0,"attackSound":0,"deathSound":0}},"description":"Lucy the loyal dog"}')

-- TODO: 220-260 reserved for future default characters

-- Harvestables
s_defaultItems[261] = Events.decode('{"name":"Tree","itemType":"harvestable","GLID":4476914,"rarity":"Common","level":1,"GIGLID":0,"behavior":"harvestable","behaviorParams":{"spawnGLID":4470121,"requiredTool":125},"loot":{"limitItems":{"UNID":83,"limit":5}},"description":"A tree spawner. Chop the trees down to get wood"}')
s_defaultItems[262] = Events.decode('{"name":"Boulder","itemType":"harvestable","GLID":4489136,"rarity":"Common","level":1,"GIGLID":0,"behavior":"harvestable","behaviorParams":{"spawnGLID":4490122,"requiredTool":141},"loot":{"limitItems":{"UNID":123,"limit":5}},"description":"A boulder spawner. Destory the boulder to get stone"}')
s_defaultItems[263] = Events.decode('{"name":"Car","itemType":"harvestable","GLID":4491719,"rarity":"Common","level":1,"GIGLID":0,"behavior":"harvestable","behaviorParams":{"spawnGLID":4491585,"requiredTool":142},"loot":{"limitItems":{"UNID":124,"limit":5}},"description":"A broken down car spawner. Destroy the car to get scrap metal"}')
-- TODO: 264-300 reserved for future default harvestables

-- Recipes
s_defaultItems[301] = Events.decode('{"name":"Wood Wall 8x16 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":13,"count":2}],"output":11,"description":"Keep out unwanted visitors with a sturdy wooden wall."}')
s_defaultItems[302] = Events.decode('{"name":"Wood Wall 4x16 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":14,"count":2}],"output":12,"description":"Keep out unwanted visitors with a sturdy wooden wall."}')
s_defaultItems[303] = Events.decode('{"name":"Wood Wall 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":83,"count":8}],"output":13,"description":"Keep out unwanted visitors with a sturdy wooden wall."}')
s_defaultItems[304] = Events.decode('{"name":"Wood Wall 4x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":83,"count":4}],"output":14,"description":"Keep out unwanted visitors with a sturdy wooden wall."}')
s_defaultItems[305] = Events.decode('{"name":"Wood Floor 16x16 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":16,"count":4}],"output":15,"description":"Make for sure footing with a sturdy wooden floor."}')
s_defaultItems[306] = Events.decode('{"name":"Wood Floor 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":83,"count":8}],"output":16,"description":"Make for sure footing with a sturdy wooden floor."}')
s_defaultItems[307] = Events.decode('{"name":"Wood Block 4x4x4 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":83,"count":4}],"output":17,"description":"Get creative with an all-purpose sturdy wooden block."}')
s_defaultItems[308] = Events.decode('{"name":"Wood Door 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":83,"count":8}],"output":18,"description":"Keep out unwanted visitors with a sturdy wooden door."}')
s_defaultItems[309] = Events.decode('{"name":"Wood Stairs 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":83,"count":8}],"output":19,"description":"Rise to new heights with these sturdy wooden staircase."}')

s_defaultItems[310] = Events.decode('{"name":"Fire Pit Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":83,"count":3}],"output":20,"description":"Cook food and craft items with this comforting campfire."}')
s_defaultItems[311] = Events.decode('{"name":"Chest Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":83,"count":16}],"output":21,"description":"Store up to 20 slots of loot with a sturdy wooden chest."}')
s_defaultItems[312] = Events.decode('{"name":"Cooked Chicken Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":84,"count":1}],"proxReq":20,"output":120,"description":"Fire roasted chicken."}')

s_defaultItems[313] = Events.decode('{"name":"Concrete Wall 8x16 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":24,"count":2}],"output":22,"description":"Ensure your security with a hardened concrete wall."}')
s_defaultItems[314] = Events.decode('{"name":"Concrete Wall 4x16 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":25,"count":2}],"output":23,"description":"Ensure your security with a hardened concrete wall."}')
s_defaultItems[315] = Events.decode('{"name":"Concrete Wall 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":123,"count":8}],"output":24,"description":"Ensure your security with a hardened concrete wall."}')
s_defaultItems[316] = Events.decode('{"name":"Concrete Wall 4x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":123,"count":4}],"output":25,"description":"Ensure your security with a hardened concrete wall."}')
s_defaultItems[317] = Events.decode('{"name":"Concrete Floor 16x16 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":27,"count":4}],"output":26,"description":"Stand with confidence on a hardened concrete floor."}')
s_defaultItems[318] = Events.decode('{"name":"Concrete Floor 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":123,"count":8}],"output":27,"description":"Stand with confidence on a hardened concrete floor."}')
s_defaultItems[319] = Events.decode('{"name":"Concrete Block 4x4x4 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":123,"count":4}],"output":28,"description":"Build boldly with an all-purpose hardened concrete block."}')
s_defaultItems[320] = Events.decode('{"name":"Concrete Stairs 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":123,"count":8}],"output":29,"description":"Step it up with these hardened concrete stairs."}')
s_defaultItems[321] = Events.decode('{"name":"Concrete Door 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":123,"count":8}],"output":30,"description":"Ensure your security with a hardened concrete door."}')

s_defaultItems[322] = Events.decode('{"name":"Metal Wall 8x16 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":33,"count":2}],"proxReq":20,"output":31,"description":"Provide powerful protection with an ironclad metal wall."}')
s_defaultItems[323] = Events.decode('{"name":"Metal Wall 4x16 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":34,"count":2}],"proxReq":20,"output":32,"description":"Provide powerful protection with an ironclad metal wall."}')
s_defaultItems[324] = Events.decode('{"name":"Metal Wall 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":124,"count":8}],"proxReq":20,"output":33,"description":"Provide powerful protection with an ironclad metal wall."}')
s_defaultItems[325] = Events.decode('{"name":"Metal Wall 4x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":124,"count":4}],"proxReq":20,"output":34,"description":"Provide powerful protection with an ironclad metal wall."}')
s_defaultItems[326] = Events.decode('{"name":"Metal Floor 16x16 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":36,"count":4}],"proxReq":20,"output":35,"description":"Walk with conviction on an ironclad metal floor."}')
s_defaultItems[327] = Events.decode('{"name":"Metal Floor 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":124,"count":8}],"proxReq":20,"output":36,"description":"Walk with conviction on an ironclad metal floor."}')
s_defaultItems[328] = Events.decode('{"name":"Metal Block 4x4x4 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":124,"count":4}],"proxReq":20,"output":37,"description":"Realize your dreams with an all-purpose ironclad metal block."}')
s_defaultItems[329] = Events.decode('{"name":"Metal Stairs 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":124,"count":8}],"proxReq":20,"output":38,"description":"Reach for the stars with these ironclad metal stairs."}')
s_defaultItems[330] = Events.decode('{"name":"Metal Door 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":124,"count":8}],"proxReq":20,"output":39,"description":"Provide powerful protection with an ironclad metal door."}')
s_defaultItems[331] = Events.decode('{"name":"Lockable Metal Door 8x8 Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":39,"count":1},{"UNID":126,"count":1}],"proxReq":20,"output":40,"description":"Keep your valuables safe with a locked metal door."}')
s_defaultItems[332] = Events.decode('{"name":"Lockable Metal Safe Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":124,"count":100},{"UNID":126,"count":1}],"proxReq":20,"output":41,"description":"Store up to 20 slots of loot with a locked metal safe."}')
s_defaultItems[333] = Events.decode('{"name":"Repair Bench Recipe","itemType":"recipe","GLID":0,"inputs":[{"UNID":83,"count":25},{"UNID":124,"count":10}],"output":47,"description":"A sturdy Repair Bench used for repairing Armor, Weapons, and Tools."}')

s_defaultItems[335] = Events.decode('{"name":"Guard Tower Blueprint","itemType":"blueprint","GLID":4495343,"rarity":"Never","level":1,"inputs":[{"UNID":123,"count":300},{"UNID":124,"count":200},{"UNID":83,"count":100}],"proxReq":20,"output":44,"oneUse":true,"description":"A formidable tower of defense that can keep its occupants high and dry above the waves of endless zombies."}')
-- TODO: 336-359 reserved for future default recipes

-- TODO: 360-380 Game System Flags
s_defaultItems[360] = Events.decode('{"name":"No PVP","itemType":"flag","level":1,"GLID":4503526,"behavior":"flag","enabled":false,"flagType":"pvp","radius":250,"height":50,"description":"No PVP in this zone."}')
s_defaultItems[361] = Events.decode('{"name":"Allow PVP","itemType":"flag","level":1,"GLID":4503525,"behavior":"flag","enabled":true,"flagType":"pvp","radius":250,"height":50,"description":"Allow PVP in this zone."}')
s_defaultItems[362] = Events.decode('{"name":"No Build Flag","itemType":"flag","level":1,"GLID":4503527,"behavior":"flag","enabled":false,"flagType":"playerBuild","radius":250,"height":50,"description":"Players cannot build in this zone."}')
s_defaultItems[363] = Events.decode('{"name":"Allow Build Flag","itemType":"flag","level":1,"GLID":4503528,"behavior":"flag","enabled":true,"flagType":"playerBuild","radius":250,"height":50,"description":"Players can build in this zone."}')
s_defaultItems[364] = Events.decode('{"name":"No Player Destruction","itemType":"flag","level":1,"GLID":4503529,"behavior":"flag","enabled":false,"flagType":"playerDestruction","radius":250,"height":50,"description":"Players cannot destroy placed objects in this area."}')
s_defaultItems[365] = Events.decode('{"name":"Allow Player Destruction","itemType":"flag","level":1,"GLID":4503530,"behavior":"flag","enabled":true,"flagType":"playerDestruction","radius":250,"height":50,"description":"Players can destroy place objects in this area."}')
s_defaultItems[366] = Events.decode('{"name":"Small Land Claim Flag","itemType":"placeable","level":1,"GLID":4612626,"behavior":"claimFlag","enabled":true,"flagType":"flaimFlag","radius":25,"height":50,"description":"Allows Players to stake their own land claims upon which only they can build!"}')
s_defaultItems[367] = Events.decode('{"name":"Land Claim Flag","itemType":"placeable","level":1,"GLID":4612626,"behavior":"claimFlag","enabled":true,"flagType":"flaimFlag","radius":50,"height":50,"description":"Allows Players to stake their own land claims upon which only they can build!"}')
s_defaultItems[368] = Events.decode('{"name":"Large Land Claim Flag","itemType":"placeable","level":1,"GLID":4612626,"behavior":"claimFlag","enabled":true,"flagType":"flaimFlag","radius":100,"height":50,"description":"Allows Players to stake their own land claims upon which only they can build!"}')

-- 400-500 reserved for default Quests
-- TODO: Quest icon GLIDs for image purposes
s_defaultItems[400] = Events.decode('{"name":"Quest Example Stub","itemType":"quest","level":1,"GLID":4490497,"description":"Example quest.","questPreText":"This is an example quest","questPostText":"You completed the quest. Now go away.","prerequisiteQuest":"0","requiredItem":{"requiredUNID":123,"requiredCount":5},"rewardItem":{"rewardUNID":331,"rewardCount":1},"repeatable":true,"destroyOnCompletion":true,"repeatTime":1,"waypointPID":0,"waypointInfo":{"height":10,"radius":10}}')
s_defaultItems[401] = Events.decode('{"name": "Credits/Rewards Vendor","itemType":"character","level":1,"GLID":4534611,"rarity":"Common","behavior":"vendor","behaviorParams":{"actorName":"Credits/Rewards Vendor","actorTitle":"Credits/Rewards Vendor","credits":true},"description":"A friendly loot vendor for selling items for Credits or Rewards. You receive 70% commission for Credits, 10% commission for Rewards","trades":[]}')

function bootstrapGameItems()
	for UNID, gameItem in pairs(s_defaultItems) do
		local itemToSave = {}
		for key, value in pairs(gameItem) do
			if type(value) == "table" then
				itemToSave[key] = Events.encode(value)
			else
				itemToSave[key] = value
			end
		end
		itemToSave.GIID = UNID
		kgp.gameItemUpdate(UNID, itemToSave)
	end
	checkNextUNID()
end

function checkNextUNID()
	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	local nextUNID = tonumber(SaveLoad.loadGameData("NextUNID", parentID, parentType))
	if nextUNID == nil or nextUNID < DEFAULT_NEXT_UNID then
		SaveLoad.saveGameData("NextUNID", DEFAULT_NEXT_UNID)
	end
end

function isNestedKey(key)
	return s_nestedKeys[key] or false
end