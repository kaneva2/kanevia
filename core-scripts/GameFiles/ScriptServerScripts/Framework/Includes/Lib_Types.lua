--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_Types.lua
-------------------------------------------------------------------
-- Type related helper functions
-------------------------------------------------------------------
-- * validateArguments
-------------------------------------------------------------------


-- Validate a list of arguments: params = { {arg1, paramName1, expectedType1[, optional1]}, {arg2, paramName2, expectedType2[, optional2]}, ... }
local function validateArguments(params)
	for index, param in ipairs(params) do
		local actualArg		= param[1]
		local paramName		= param[2]
		local expectedType	= param[3]
		local optional		= param[4]

		if not(optional and actualArg == nil)								-- check if optional arg
		   and expectedType ~= type(actualArg)								-- check if type matches
		   and (expectedType ~= "class" or (Class and Class.isClassOrInstance and Class.isClassOrInstance(actualArg)))	-- check if "class" pseudo type
		   and expectedType ~= "variant" then								-- check if "variant" pseudo type
			-- error(debug.getinfo(2, "n").name .. "(): argument #" .. tostring(index) .. " \"" .. tostring(paramName) .. "\" is invalid, expected: " .. tostring(expectedType) .. ", got " .. type(actualArg), 3)
			error("Argument #" .. tostring(index) .. " \"" .. tostring(paramName) .. "\" is invalid, expected: " .. tostring(expectedType) .. ", got " .. type(actualArg), 3)
		end
	end
end



Types = {
	validateArguments	= validateArguments,
}
