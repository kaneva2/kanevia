--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_Environment.lua

Environment = {
	GAME_ID_PROD	= 3296,
	GAME_ID_PREV	= 3298,
	GAME_ID_DEV		= 5316,
	GAME_ID_RC		= 5310,
}

local __SERVER__	= false
local __DEBUG__		= __DEV_MODE__ == 1

function Environment.isRunningOnServer()
	return __SERVER__ == true
end

function Environment.isRunningOnClient()
	return __SERVER__ ~= true
end

function Environment.isRunningInDebugMode()
	return __DEBUG__
end

