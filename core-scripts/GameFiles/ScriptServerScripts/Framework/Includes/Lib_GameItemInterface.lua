--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

include("Lib_ZoneInfo.lua")

-- Lib_GameItemInterface.lua
-- Helper library to wrap calls to server-side zone Game Item APIs
GII = {}

----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local ZoneInfo 			= _G.ZoneInfo

-------------------------------------------------------------------------------------
-- API WRAPPER FUNCTIONS
-------------------------------------------------------------------------------------
function GII.gameItemLoadAllByGame(zoneInstanceId, zoneType)

	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	if tonumber(zoneInstanceId) == nil then zoneInstanceId = parentID end
	if tonumber(zoneType) == nil then zoneType = parentType end
	return kgp.gameItemLoadAllByGame(zoneInstanceId or 0, zoneType or 0)
end

function GII.gameItemUpdate(UNID, itemToSave, zoneInstanceId, zoneType)

	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	if tonumber(zoneInstanceId) == nil then zoneInstanceId = parentID end
	if tonumber(zoneType) == nil then zoneType = parentType end
	kgp.gameItemUpdate(UNID, zoneInstanceId or 0, zoneType or 0, itemToSave)
end

function GII.gameItemDelete(UNID, zoneInstanceId, zoneType)

	local parentID, parentType = ZoneInfo.getParentZoneInstanceAndType()
	if tonumber(zoneInstanceId) == nil then zoneInstanceId = parentID end
	if tonumber(zoneType) == nil then zoneType = parentType end
	kgp.gameItemDelete(UNID, zoneInstanceId or 0, zoneType or 0)
end

