--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_Compat.lua
-- Addressing Lua version compatibility issues

if __LUA_VERSION__ >= 502 then
	-- define getfenv() if not already
	if _G.getfenv==nil then
		_G.getfenv = function(f)
			if type(f)~="function" then
				if type(f)~="number" then
					error("bad argument #1 to 'getfenv' (number expected, got " .. type(f) .. ")")
				end

				if f<0 then
					error("bad argument #1 to 'getfenv' (level must be non-negative)", 2)
				end

				if f==0 then
					return _G
				end

				local info = debug.getinfo(f+1)
				if type(info)~="table" or type(info.func)~="function" then
					error("bad argument #1 to 'getfenv' (invalid level)", 2)
				end

				f = info.func
			end

			local name, env = debug.getupvalue(f, 1)
			if name~="_ENV" then
				error("Internal error: upvalue at index 1 is not _ENV", 1)
			end

			return env
		end
	end

	-- define setfenv() if not already
	if _G.setfenv==nil then
		_G.setfenv = function(f, env)
			if type(f)~="function" then
				if type(f)~="number" then
					error("bad argument #1 to 'setfenv' (number expected, got " .. type(f) .. ")")
				end

				if f<0 then
					error("bad argument #1 to 'setfenv' (level must be non-negative)", 2)
				end

				if f==0 then
					error("bad argument #1 to 'setfenv' (level 0 is not supported)", 2)	-- can't handle level 0 as Lua 5.1
				end

				local info = debug.getinfo(f+1)
				if type(info)~="table" or type(info.func)~="function" then
					error("bad argument #1 to 'setfenv' (invalid level)", 2)
				end

				f = info.func
			end

			local name = debug.setupvalue(f, 1, env)
			if name~="_ENV" then
				error("Internal error: upvalue at index 1 is not _ENV", 1)
			end
			return f
		end
	end
end
