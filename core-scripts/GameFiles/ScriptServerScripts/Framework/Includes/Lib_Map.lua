--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

MapDefinitions = {}

local LABEL_INDEX = { -- Order the legend labels will display (also needs to match the order of the icon texture file)
    YOU					= 0, -- Always exists
    CORPSE				= 1, -- Is death possible?
    EVENT				= 2, -- Is there an active event?
    PET					= 3, -- Are there pets?
    LANDCLAIM			= 4, -- Are there any landclaim flags?
    GOAL				= 5, -- Is there a current goal?
    CURRENT_QUEST_GIVER	= 6, -- Is there a current quest giver?
    QUEST_GIVER			= 7, -- Are there quest givers?
    VENDOR				= 8, -- Are there vendors?
    PLAYERS				= 9, -- Are there other players?
    MONSTER				= 10, -- Are there monsters?
    LOOT_PLACEABLE		= 11, -- Are there loot-generating placeables?
    HARVESTABLE			= 12 -- Are there harvestables?
}

MapDefinitions.LABEL_INDEX = LABEL_INDEX

local isClient = Environment == nil or Environment.isRunningOnClient == nil or Environment.isRunningOnClient()
if isClient then
	-- Name and color for displaying info about associated types
	MapDefinitions.LEGEND_COLORS = { 
		[LABEL_INDEX.YOU]					= {color = {a=255, r=223, g=223, b=223}},
		[LABEL_INDEX.CORPSE]				= {color = {a=255, r=118, g=118, b=118}},
		[LABEL_INDEX.EVENT]					= {color = {a=255, r=223, g=223, b=223}},
		[LABEL_INDEX.LANDCLAIM]				= {color = {a=255, r=222, g=116, b=53}},
		[LABEL_INDEX.GOAL]					= {color = {a=255, r=246, g=240, b=101}},
		[LABEL_INDEX.CURRENT_QUEST_GIVER]	= {color = {a=255, r=255, g=224, b=46}},
		[LABEL_INDEX.QUEST_GIVER]			= {color = {a=255, r=242, g=222, b=127}},
		[LABEL_INDEX.VENDOR]				= {color = {a=255, r=157, g=175, b=6}},
		[LABEL_INDEX.PLAYERS]				= {color = {a=255, r=0, g=174, b=240}},
		[LABEL_INDEX.MONSTER]				= {color = {a=255, r=241, g=51, b=51}},
		[LABEL_INDEX.LOOT_PLACEABLE]		= {color = {a=255, r=255, g=210, b=0}},
		[LABEL_INDEX.HARVESTABLE]			= {color = {a=255, r=2, g=255, b=0}},
		[LABEL_INDEX.PET]					= {color = {a=255, r=223, g=223, b=223}}
	}

	-- Get whether or not these are relevant to the world (update map as data received)
	MapDefinitions.LEGEND_LABELS = {
		[LABEL_INDEX.YOU]					= {label="You"},
		[LABEL_INDEX.CORPSE]				= {label="Your Corpse"},
		[LABEL_INDEX.EVENT]					= {label="Player Event"},
		[LABEL_INDEX.LANDCLAIM]				= {label="Land Claim Flag"},
		[LABEL_INDEX.GOAL] 					= {label="Current Quest Goal"},
		[LABEL_INDEX.CURRENT_QUEST_GIVER]	= {label="Current Quest Goal"},
		[LABEL_INDEX.QUEST_GIVER]			= {label="Quest Giver"},
		[LABEL_INDEX.VENDOR]				= {label="Vendor"},
		[LABEL_INDEX.PLAYERS]				= {label="Other Player"},
		[LABEL_INDEX.MONSTER]				= {label="Dangerous Threat"},
		[LABEL_INDEX.LOOT_PLACEABLE]		= {label="Your Loot Generating Placeable"},
		[LABEL_INDEX.HARVESTABLE]			= {label="Harvestable"},
		[LABEL_INDEX.PET]					= {label="Your Pet"}
	}
end