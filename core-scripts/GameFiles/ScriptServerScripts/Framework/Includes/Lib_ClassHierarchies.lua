--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--------------------------------------------------------------
-- Lib_ClassHierarchies.lua
--------------------------------------------------------------

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Class				= _G.Class


ClassHierarchies = {}


local tClassDefinitions = {
	Class_Actor = {
		super		= "Class_Base",
		location	= "Class_ActorDefinition.lua",
	},
	Class_ActorRider = {
		super		= "Class_PathRider",
		location	= "Class_ActorDefinition.lua",
	},
	Class_ActorPet = {
		super		= "Class_Pet",
		location	= "Class_ActorDefinition.lua",
	},
	Class_Vendor = {
		super		= "Class_Actor",
		location	= "Class_VendorDefinition.lua",
	},
	Class_VendorPet = {
		super		= "Class_ActorPet",
		location	= "Class_VendorDefinition.lua",
	},
	Class_VendorRider = {
		super		= "Class_ActorRider",
		location	= "Class_VendorDefinition.lua",
	},
	Class_ShopVendor = {
		super		= "Class_Actor",
		location	= "Class_ShopVendorDefinition.lua",
	},
	Class_ShopVendorPet = {
		super		= "Class_ActorPet",
		location	= "Class_ShopVendorDefinition.lua",
	},
	Class_ShopVendorRider = {
		super		= "Class_ActorRider",
		location	= "Class_ShopVendorDefinition.lua",
	},
	Class_QuestGiver = {
		super		= "Class_Actor",
		location	= "Class_QuestGiverDefinition.lua",
	},
	Class_QuestGiverPet = {
		super		= "Class_ActorPet",
		location	= "Class_QuestGiverDefinition.lua",
	},
	Class_QuestGiverRider = {
		super		= "Class_ActorRider",
		location	= "Class_QuestGiverDefinition.lua",
	},
	Class_BaseItemContainer = {
		super		= "Class_Base",
		location	= "Class_BaseItemContainerDefinition.lua",
	},
	Class_BaseItemContainerFromHarvestable = {
		super		= "Class_Harvestable",
		location	= "Class_BaseItemContainerDefinition.lua",
	},
	Class_BaseItemContainerRider = {
		super		= "Class_PathRider",
		location	= "Class_BaseItemContainerDefinition.lua",
	}
}


function ClassHierarchies.loadClass(sClassName)
	local tClassInfo = tClassDefinitions[sClassName]
	if tClassInfo then
		Class.createClass(sClassName, tClassInfo.super, tClassInfo.location)
		return
	end

	include(sClassName .. ".lua")
end


