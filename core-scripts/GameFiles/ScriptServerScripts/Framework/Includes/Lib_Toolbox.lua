--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_Toolbox.lua


local function isContainer(t)
	return type(t) == "table" or type(t) == "userdata"
end


-- Checks whether a table of string entries can be converted to a numeric table.
local function canConvertToNumericTable(t)
	if not t["1"] then
		return false
	end
	
	for i, v in pairs(t) do
		if tonumber(i) == nil then
			return false
		end
	end
	
	return true
end

-- Makes a nested copy of the passed in data. 
-- Containers of "userdata" type are converted to tables. 
-- Optionally, containers can be converted to numeric tables where possible.
local function deepCopy(copyData, bConvertToNumericTables)
	if copyData == nil then
		log("ERROR DETECTED - ED-6321 : deepCopy called where copyData is nil!")
		Debug.printStack()
	else
		local dataType = type(copyData)
		if dataType == "table" or dataType == "userdata" then
			local bConvert = bConvertToNumericTables == true and canConvertToNumericTable(copyData)
			local tempTable = {}
			for index, value in pairs(copyData) do
				local i = bConvert and tonumber(index) or index
				tempTable[i] = deepCopy(value, bConvertToNumericTables)
			end
			return tempTable
		end
	end
	
	return copyData
end

local function shallowCopy(t)
	local dataType = type(t)
	if dataType == "table" or dataType == "userdata" then
		local newTable = {}

		for k, v in pairs(t) do
			newTable[k] = v
		end

		return newTable
	end
	return nil
end



-- This breaks tables into smaller chunks mainly so we can send large data across the network
-- Arguments: 
--		t 					: table to be broken up,
--		iChunkEntryCount	: max number of entries that a chunk can contain
--		bDuplicateEntries	: allow entries to be deep-copied?
local function breakTableIntoChunks(t, iChunkEntryCount, bDuplicateEntries)
	local tChunkList = {}
	local iChunkCount = 0
	local iEntryCount = 0
	local tCurrentChunk
	
	for i, v in pairs(t) do
		if iEntryCount == iChunkEntryCount or not tCurrentChunk then
			tCurrentChunk = {}
			table.insert(tChunkList, tCurrentChunk)
			iEntryCount = 0
			iChunkCount = iChunkCount + 1
		end
		
		tCurrentChunk[i] = bDuplicateEntries == true and deepCopy(v, true) or v
		iEntryCount = iEntryCount + 1
	end
	
	return tChunkList, iChunkCount
end


-- 03/11/2016, CJW, NOTE: Temp fix to let tables of depth two or more get copied into SharedData such as s_uItemInstances
-- As things stand, assigning a table to a member of a SharedData structure, lets the table get copied up to the first depth. Anything beyond that is ignored.
-- 06/01/2016, CJW, Tables can now be deep-copied into SharedData, so this function is pretty much deprecated.
local function setUserdataAtIndex(userdata, index, data)
	if index then
		if type(userdata) == "table" or type(userdata) == "userdata" then
			if type(data) == "table" or type(data) == "userdata" then
				
				userdata[index] = {}
				
				for i, v in pairs(data) do
					setUserdataAtIndex(userdata[index], i, v)
				end
			else
				userdata[index] = data
			end
		end
	end
end

-- 03/22/2016, CJW, NOTE: Since, the "next" function crashes when a userdata is passed to it 
-- we needed a more generic function to check for emptiness of container objects.
local function isContainerEmpty(t)
	local dataType = type(t)
	if dataType == "table" then
		return next(t) == nil
	elseif dataType == "userdata" then
		return SharedData.isEmpty(t)
	end
	
	log("ERROR: Invalid type being sent to function isTableEmpty: " .. type(t))
	return true
end


-- Debugging function to detect whether a variable contains userdata
local function containsUserdata(t)
	local argType = type(t)
	
	if argType == "userdata" then
		return true
	end
	
	if argType == "table" then
		for _, v in pairs(t) do
			if containsUserdata(v) then
				return true
			end
		end
	end
	
	return false
end

local function getContainerKeys(t)
	if type(t) == "table" or type(t) == "userdata" then
		local keys = {}
		for k, _ in pairs(t) do
			table.insert(keys, k)
		end
		return keys
	end
end


local function getContainerValues(t)
	if type(t) == "table" or type(t) == "userdata" then
		local values = {}
		for _, v in pairs(t) do
			table.insert(values, v)
		end
		return values
	end
end


local function convertContainerToSet(t)
	if type(t) == "table" or type(t) == "userdata" then
		local set = {}
		for _, v in pairs(t) do
			set[v] = true
		end
		return set
	end
end


-- create a table by associating keys and values
local function associateKeysToValues(keys, values)
	if type(keys) ~= "table" or type(values) ~= "table" then
		return nil
	end

	local retTable = {}
	for index, key in pairs(keys) do
		retTable[key] = values[index]
	end

	return retTable
end




------------------------------------------------------------
-- Quicksort helpers START
------------------------------------------------------------
-- Quicksort helper function (returns a table of all items beyond the pivot)
local function rest(tableToSort)
	local tableRest = {}
	for i = 2, #tableToSort do
		table.insert(tableRest, tableToSort[i])
	end
	return tableRest
end

-- Quicksort helper function (concatenates two tables)
local function concat(tableA, tableB)
	local tableC = {}
	for _, data in pairs(tableA) do
		table.insert(tableC, data)
	end
	for _, data in pairs(tableB) do
		table.insert(tableC, data)
	end
	return tableC
end


-- Converts a hash table to an index table
local function convertTableToIndexed(tableToConvert)
	if tableToConvert then
		local indexedTable = {}
		for k, v in pairs(tableToConvert) do
			table.insert(indexedTable, v)
		end
		return indexedTable
	end
end

-- Perform a quicksort on an indexed table
local function sortIndexedTableByParameter(tableToSort, parameter)
	-- Return if it is of size 1 or less
	if tableToSort == nil or #tableToSort <= 1 or parameter == nil then
		return tableToSort or {}
	end

	local pivotData, tableRest = tableToSort[1], rest(tableToSort)
	local lesser, greater = {}, {}
	for _, restData in pairs(tableRest) do
		if restData[parameter] and pivotData[parameter] and restData[parameter] < pivotData[parameter] then
		  table.insert(lesser, restData)
		else
		  table.insert(greater, restData)
		end
	end
	
	return concat(sortIndexedTableByParameter(lesser, parameter), concat({pivotData}, sortIndexedTableByParameter(greater, parameter)))
end

-- Perform a quicksort on a hashed or indexed table (converts to indexed)
local function sortTableByParameter(tableToSort, parameter)
	return sortIndexedTableByParameter(convertTableToIndexed(tableToSort), parameter)
end
------------------------------------------------------------
-- Quicksort helpers FINISH
------------------------------------------------------------








Toolbox = {
	breakTableIntoChunks		= breakTableIntoChunks,
	setUserdataAtIndex			= setUserdataAtIndex,
	isContainerEmpty			= isContainerEmpty,
	deepCopy					= deepCopy,
	shallowCopy					= shallowCopy,
	isContainer					= isContainer,
	canConvertToNumericTable	= canConvertToNumericTable,
	containsUserdata			= containsUserdata,
	getContainerKeys			= getContainerKeys,
	getContainerValues			= getContainerValues,
	convertContainerToSet		= convertContainerToSet,
	associateKeysToValues		= associateKeysToValues,
	sortTableByParameter		= sortTableByParameter,
	convertTableToIndexed		= convertTableToIndexed
}


