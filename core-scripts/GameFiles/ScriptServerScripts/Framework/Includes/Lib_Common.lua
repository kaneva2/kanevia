--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_Common.lua

-- Includes everything essential to server-side development
local s_iZoneInstanceID = nil

-- include/require/dofile
function include(fileName)
	if Types and Types.validateArguments ~= nil then		-- Allow "Lib_Types.lua" to be included without validation. Chicken and egg problem.
		Types.validateArguments{ {fileName, "fileName", "string"} }
	end
	kgp.scriptDoFile(fileName)
end

-- low level log function: call corresponding logging API depending on the environment
function log(message)
	-- server script version
	if s_iZoneInstanceID == nil then s_iZoneInstanceID = kgp.gameGetCurrentInstance() end

	if s_iZoneInstanceID then
		message = "["..tostring(s_iZoneInstanceID).."] "..message
	end

	kgp.gameLogMessage(message)
end


include "Lib_Environment.lua"
include "Lib_Toolbox.lua"
include "Lib_Compat.lua"
include "Lib_Types.lua"
include "Lib_Debug.lua"
include "Lib_SharedData.lua"
include "Lib_Class.lua"
include "Lib_ClassHierarchies.lua"
include "Lib_Events.lua"
include "Lib_Players.lua"


