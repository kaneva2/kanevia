--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_Vecmath.lua
-- To be replace by C API

local tolerance = 1E-4


-- compose a vector value
local function createVector(x, y, z)
	return {x = x, y = y, z = z}
end


-- check if argument is a proper vector value and return normalized value if valid
local function validateVector(val, paramName, index, default)
	val = val or default

	if type(val) == "table" then
		if val.r == nil and #val == 3 then
			return {x = val[1], y = val[2], z = val[3]}
		end
		if val.x ~= nil and val.y ~= nil and val.z ~= nil then
			return val
		end
	end

	-- failed
	error("Argument #" .. tostring(index) .. " \"" .. tostring(paramName) .. "\" is invalid, expected: { x, y, z }", 3)
end

------------------------------------------------
-- Primitive vector operations
local function vecAdd( v1, v2 )
	if v1.z == nil then
		return { x = v1.x + v2.x, y = v1.y + v2.y }
	else
		return { x = v1.x + v2.x, y = v1.y + v2.y, z = v1.z + v2.z }
	end
end

local function vecSub( v1, v2 )
	if v1.z == nil then
		return { x = v1.x - v2.x, y = v1.y - v2.y }
	else
		return { x = v1.x - v2.x, y = v1.y - v2.y, z = v1.z - v2.z }
	end
end

local function vecNeg( v1 )
	if v1.z == nil then
		return { x = -v1.x, y = -v1.y }
	else
		return { x = -v1.x, y = -v1.y, z = -v1.z }
	end
end

local function vecDot( v1, v2 )
	if v1.z == nil then
		return v1.x * v2.x + v1.y * v2.y
	else
		return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z
	end
end

local function vecCross( v1, v2 )
	if v1.z == nil then
		error( "vecCross: vectors must be three dimensional", 2 )
	else
		return { x = v1.y * v2.z - v1.z * v2.y, y = v1.z * v2.x - v1.x * v2.z, z = v1.x * v2.y - v1.y * v2.x }
	end
end

local function vecLenSq( v )
	return vecDot( v, v )
end

local function vecLen( v )
	return math.sqrt(vecLenSq(v))
end

local function vecNormalize( v )
	local len = vecLen(v)
	if len > 0.00001 then
		return {x = v.x / len, y = v.y / len, z = v.z and v.z / len}, len
	else
		return {0, 0, 0}, len
	end
end

local function vecTransform( v, m )
	if v.z == nil then
		return {
			x = m[1][1] * v.x + m[2][1] * v.y + m[3][1], 
			y = m[1][2] * v.x + m[2][2] * v.y + m[3][2], 
		}
	else
		return { 
			x = m[1][1] * v.x + m[2][1] * v.y + m[3][1] * v.z + m[4][1], 
			y = m[1][2] * v.x + m[2][2] * v.y + m[3][2] * v.z + m[4][2], 
			z = m[1][3] * v.x + m[2][3] * v.y + m[3][3] * v.z + m[4][3], 
		}
	end
end

local function vecFromArray( arr )
	return { x = arr[1], y = arr[2], z = arr[3] }
end

local function vecToArray( v )
	return { v.x, v.y, v.z }
end

local function matIdentity( m )
	if m == nil then
		m = { {}, {}, {}, {} }
	end

	m[1][1] = 1  m[1][2] = 0  m[1][3] = 0  m[1][4] = 0
	m[2][1] = 0  m[2][2] = 1  m[2][3] = 0  m[2][4] = 0
	m[3][1] = 0  m[3][2] = 0  m[3][3] = 1  m[3][4] = 0
	m[4][1] = 0  m[4][2] = 0  m[4][3] = 0  m[4][4] = 1
	return m
end

local function matInvert( m1 )
	-- Quick inversion with following assumptions
	-- * 3x3 rotation sub-matrix is orthogonal
	-- * no scaling
	-- * no shear or perspective transformation
	local m2 = matIdentity()

	-- first transpose the rotation matrix since it's orthogonal
	m2[1][1] = m1[1][1];
	m2[1][2] = m1[2][1];
	m2[1][3] = m1[3][1];
	m2[2][1] = m1[1][2];
	m2[2][2] = m1[2][2];
	m2[2][3] = m1[3][2];
	m2[3][1] = m1[1][3];
	m2[3][2] = m1[2][3];
	m2[3][3] = m1[3][3];

	-- now get the new translation vector
	local temp_x = m1[4][1];
	local temp_y = m1[4][2];
	local temp_z = m1[4][3];

	m2[4][1] = -(temp_x * m1[1][1] + temp_y * m1[1][2] + temp_z * m1[1][3]);
	m2[4][2] = -(temp_x * m1[2][1] + temp_y * m1[2][2] + temp_z * m1[2][3]);
	m2[4][3] = -(temp_x * m1[3][1] + temp_y * m1[3][2] + temp_z * m1[3][3]);

	return m2
end

local function matLookAt( eye, at )
	local forward = vecSub( at, eye )
	local matrix = {
			{ 0, 0, 0, 0 },
			{ 0, 1, 0, 0 },
			{ forward.x, forward.y, forward.z, 0 },
			{ eye.x, eye.y, eye.z, 1 }
		}
	matrix[1] = vecToArray( vecCross( vecFromArray(matrix[2]), vecFromArray(matrix[3]) ) )
	matrix[1][4] = 0
	if math.abs(forward.y)>tolerance then
		-- recalculate up vector since forward is not horizontal
		matrix[2] = vecToArray( vecCross( vecFromArray(matrix[3]), vecFromArray(matrix[1]) ) )
		matrix[2][4] = 0
	end

	return matrix
end

local function matValidate( m )
	if type(m) ~= "table" or 
	   type(m[1]) ~= "table" or #m[1] ~= 4 or
	   type(m[2]) ~= "table" or #m[2] ~= 4 or
	   type(m[3]) ~= "table" or #m[3] ~= 4 or
	   type(m[4]) ~= "table" or #m[4] ~= 4 then
			error("Invalid transform", 3)
	end
end


------------------------------------------------
-- Analytical geometry


-- 2D or 3D distance
local function getVectorDistance( vect1, vect2 )
	local vector = vecSub(vect1, vect2)
	return vecLen(vector)
end

-- 2D or 3D distance squared
local function getVectorDistanceSq( vect1, vect2 )
	local vector = vecSub(vect1, vect2)
	return vecLenSq(vector)
end

-- 2D or 3D distance
local function getArrayDistance( arr1, arr2 )
	return getVectorDistance(vecFromArray(arr1), vecFromArray(arr2))
end

-- 2D or 3D distance
local function getArrayDistanceSq( arr1, arr2 )
	return getVectorDistanceSq(vecFromArray(arr1), vecFromArray(arr2))
end

-- Vertex above plane (result undefined if plane is vertical)
-- vert(x, y, z), plane(x, y, z, c)
local function isVertAbovePlane( vert, plane )
	return vecDot(vert, plane) + plane.c > 0
end

-- Vertex falls on plane (effective tolerate does not adjust automatically to incline, will converge to zero if plane is vertical)
-- vert(x, y, z), plane(x, y, z, c)
local function isVertOnPlane( vert, plane )
	return math.abs( vecDot(vert, plane) + plane.c ) <= tolerance
end

-- Vertex above or to the right of the line
-- vert2D(x, y), line(x, y, c)
local function isVertAboveLine( vert2D, line )
	return vecDot(vert2D, line) + plane.c > 0
end

-- Vertex falls on line
-- vert2D(x, y), line(x, y, c)
local function isVertOnLine( vert2D, line )
	return math.abs( vecDot(vert2D, line) + plane.c ) <= tolerance
end

-- Transform vertex from world space to object space
-- transform: object transform = matrix or { x, y, z, rx, ry, rz } - position and rotation vector (forward)
local function vertWorldToObject( vert, transform )
	local matrix
	if type(transform) == "table" and #transform == 4 then
		matValidate(transform)
		matrix = transform
	else
		if type(transform) ~= "table" and type(transform)~="userdata"  or 
		   transform.x==nil  or  transform.y==nil  or transform.z==nil or 
		   transform.rx==nil or  transform.ry==nil or transform.rz==nil then
		   error("Invalid transform", 2)
		end

		matrix = matLookAt( {x=transform.x, y=transform.y, z=transform.z}, {x=transform.x+transform.rx, y=transform.y+transform.ry, z=transform.z+transform.rz} ) 
	end

	return vecTransform( vert, matInvert(matrix) )
end

-- Transform vertex from object space to world space
-- transform: object transform = matrix or { x, y, z, rx, ry, rz[, tx, ty, tz] } - position, rotation vector (forward) and tangent vector (slide)
local function vertObjectToWorld( vert, transform )
	local matrix
	if type(transform) == "table" and #transform == 4 then
		matValidate(transform)
		matrix = transform
	else
		if type(transform) ~= "table" and type(transform) ~= "userdata"  or 
		   transform.x == nil  or  transform.y == nil  or transform.z == nil or 
		   transform.rx == nil or  transform.ry == nil or transform.rz == nil then
		   error("Invalid transform", 2)
		end

		matrix = matLookAt( {x = transform.x, y = transform.y, z = transform.z}, {x = transform.x + transform.rx, y = transform.y + transform.ry, z = transform.z + transform.rz} ) 
	end

	return vecTransform( vert, matrix )
end


-- This function takes in the x and z vector components and returns the angle in degrees
local function vectorToDegrees(x, z)
	--Use the provided math functions and basic conversion to get the radians, then the degrees.
	local aTan = math.atan2(z, x)
	local angle = (aTan / math.pi) * 180
	
	--The angle is between -180 and 180, and because of this the angle needs to be shifted if it's less than 0
	if angle < 0 then
		angle = 360 + angle
	end

	return angle
end

-- This function converts a given angle (in degrees) to a 3 part vector, very handy
local function degreesToVector(angle)
	if angle == nil then return 0, 0 end

	local radian = (angle * math.pi) / 180
	return math.cos(radian), math.sin(radian)
end



Math = {
	createVector			= createVector,
	validateVector			= validateVector,
	vecAdd					= vecAdd,
	vecSub					= vecSub,
	vecNeg					= vecNeg,
	vecDot					= vecDot,
	vecCross				= vecCross,
	vecLenSq				= vecLenSq,
	vecLen					= vecLen,
	vecNormalize			= vecNormalize,
	vecTransform			= vecTransform,
	vecFromArray			= vecFromArray,
	vecToArray				= vecToArray,
	matIdentity				= matIdentity,
	matInvert				= matInvert,
	matLookAt				= matLookAt,
	matValidate				= matValidate,
	getVectorDistance		= getVectorDistance,
	getVectorDistanceSq		= getVectorDistanceSq,
	getArrayDistance		= getArrayDistance,
	getArrayDistanceSq		= getArrayDistanceSq,
	isVertAbovePlane		= isVertAbovePlane,
	isVertOnPlane			= isVertOnPlane,
	isVertAboveLine			= isVertAboveLine,
	isVertOnLine			= isVertOnLine,
	vertWorldToObject		= vertWorldToObject,
	vertObjectToWorld		= vertObjectToWorld,
	vectorToDegrees			= vectorToDegrees,
	degreesToVector			= degreesToVector,
}

-----------------------------------------------
-----------------------------------------------
-- THE FOLLOWING SEEM TO BE REDUNDANT STUFF!!!!
-----------------------------------------------
-----------------------------------------------

ContainingTests = {}
-- sphere.r
-- Criteria: v inside sphere centered at origin
function ContainingTests.sphere( sphere, vert )
	return vecLenSq(vert) <= sphere.r * sphere.r
end

-- box.w/h/d
-- Criteria: v inside box centered at origin
function ContainingTests.box( box, vert )
	return math.abs(vert.x) <= box.w / 2 and math.abs(vert.y) <= box.h / 2 and math.abs(vert.z) <= box.d / 2
end

-- cylinder.r/h
-- Criteria: v inside cylinder centered at origin
function ContainingTests.cylinder( cylinder, vert )
	return vecLenSq{x = vert.x, y = 0, z = vert.z} <= cylinder.r * cylinder.r and math.abs(vert.y) <= cylinder.h / 2
end

-- frustum.h/r1/r2 (circular) or frustum.h/w1/d1/w2/d2 (rectanglar)
-- Criteria: v.y between 0 and frustum.h, vert.x/z inside cross section
function ContainingTests.frustum( frustum, vert )
	if vert.y < 0 or vert.y > frustum.h then
		return false
	end
	if frustum.r1 ~= nil and frustum.r2 ~= nil then
		local r = frustum.r1 + (frustum.r2 - frustum.r2) * vert.y / frustum.h
		return vecLenSq{x = vert.x, y = 0, z = vert.z} <= r * r
	elseif frustum.w1~=nil and frustum.d1 ~= nil and frustum.w2 ~= nil and frustum.d2 ~= nil then
		local w = frustum.w1 + (frustum.w2 - frustum.w1) * vert.y / frustum.h
		local d = frustum.d1 + (frustum.d2 - frustum.d1) * vert.y / frustum.h
		return math.abs(vert.x)<=w and math.abs(vert.z)<=d
	end

	error("Invalid frustum", 2)
end

UpAxisTransforms = {}
function UpAxisTransforms.x( vert )
	return { x = vert.z, y = vert.x, z = vert.y }
end

function UpAxisTransforms.y( vert )
	return vert
end

function UpAxisTransforms.z( vert )
	return { x = vert.y, y = vert.z, z = vert.x }
end
