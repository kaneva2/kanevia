--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_Players.lua

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Console			= _G.Console

-- check if input is a player
local function isPlayer(obj)
	return SharedData and SharedData.isDictionary(obj) and obj.ID ~= nil
end



-- Decipher player input and extract the correct player
local function determineUser(object, uPlayerList)
	
	if object == nil then log("determineUser() userData is nil."); return end
	
	-- Shared Data
	if type(object) == "userdata" then
		-- Pointer was passed
		if object.type == "Player" then
			return object
		end

	-- ID was passed
	elseif uPlayerList then
		if type(object) == "number" then
			-- Find the player with this ID
			for ID, uPlayer in pairs(uPlayerList) do
				if uPlayer then
					-- Only check against player map objects
					if uPlayer.ID == object then
						return uPlayer
					end
				end
			end

		-- Name was passed
		elseif type(object) == "string" then
			-- Is this a registered player name?
			return uPlayerList[object]
		end
	end
	
	return nil
end


local function extractUser(user)
	-- If user passed is a SD pointer
	if type(user) == "userdata" then
		-- Ensure SD pointer has a type
		if user.type then
			-- Ensure SD type is a player
			if user.type ~= "Player" then Console.Write(Console.DEBUG, "extractUser() - user["..tostring(user).."] is not a player object."); return nil end
		else
			Console.Write(Console.DEBUG, "extractUser() - user does not have a type field."); return nil
		end
		return user
	-- name check
	elseif type(user) == "string" then
		-- Generate a sharedData pointer
		local SD = SharedData.new( {"player", user}, false )
		if SD.type then
			if SD.type ~= "Player" then Console.Write(Console.DEBUG, "extractUser() - user["..tostring(user).."] is not a player object."); return nil end
		else
			Console.Write(Console.DEBUG, "extractUser() - user does not have a type field."); return nil
		end
		return SD
	-- ID check
	elseif type(user) == "number" then
		-- Get userName
		local playerName = kgp.playerGetName(user)
		if playerName == nil or playerName == "" then Console.Write(Console.DEBUG, "extractUser() - Failed to extract a playerName from user["..tostring(user).."]"); return nil end

		-- Generate a sharedData pointer
		local SD = SharedData.new( {"player", playerName}, false )
		if SD.type then
			if SD.type ~= "Player" then Console.Write(Console.DEBUG, "extractUser() - user["..tostring(user).."] is not a player object."); return nil end
		else
			Console.Write(Console.DEBUG, "extractUser() - user does not have a type field."); return nil
		end
		return SD
	end
end



-- Player Validation
local function validatePlayerID(number)
	return type(number) == "number"
end

local function validatePlayerGender(gender)
	if type(gender) == "string" and (gender == "M" or gender == "F") then
		return true
	end
	
	return false
end

local function validatePlayerName(vstring)
	return type(vstring) == "string"
end

local function validatePlayerPosition(position)
	return	type(position) == "userdata" and 
			type(position.x) == "number" and 
			type(position.y) == "number" and 
			type(position.z) == "number" and
			type(position.rx) == "number" and 
			type(position.ry) == "number" and 
			type(position.rz) == "number"
end


local function validatePlayer(player)
	return player and validatePlayerPosition(player.position) and validatePlayerID(player.ID)
end



Players = {
	isPlayer				= isPlayer,
	determineUser			= determineUser,
	extractUser				= extractUser,
	validatePlayerID		= validatePlayerID,
	validatePlayerGender	= validatePlayerGender,
	validatePlayerName		= validatePlayerName,
	validatePlayerPosition	= validatePlayerPosition,
	validatePlayer			= validatePlayer,
}
