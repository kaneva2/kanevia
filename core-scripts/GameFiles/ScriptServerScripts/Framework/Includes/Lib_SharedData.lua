--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_SharedData.lua		[Yanfeng 09/2013]
SharedData = {}

function SharedData.new(nameOrInstance, readonly)

	if type(nameOrInstance)=="userdata" then
		return SharedData.new(kgp.dictGetName(nameOrInstance), readonly)
	end

	local name = nameOrInstance
	local dictName
	if type(name)=="string" or type(name)=="nil" then
		-- simple name or nil
		dictName = name
	elseif type(name)=="table" and #name>0 then
		-- token array: concatenated with delimiter "."
		dictName = name[1]
		for k, v in ipairs(name) do
			if k>1 then 
				dictName = dictName .. "." .. v
			end
		end
	else
		error("SharedData: parameter #1 \"name\" for new function must be a string or an array of strings", 2)
	end

	-- allocate new or reuse existing dictionary. returned dictionary object is a userdata that calls destructor automatically upon gc.
	return kgp.dictGenerate(dictName, readonly or false)
end

function SharedData.isEmpty(instance)
	local allKeys = kgp.dictGetAllKeys(instance)
	return next(allKeys) == nil
end

-- Check if input is a shared data dictionary
function SharedData.isDictionary(obj)
	if type(obj) ~= "userdata" then
		return false
	end

	if type(kgp) ~= "table" or kgp.dictGetInfo == nil then
		return false
	end

	local name = kgp.dictGetInfo(obj)
	return name ~= nil
end

local luaPairs

function SharedData.pairs(instance)

	if type(instance)=="table" and luaPairs~=nil then
		return luaPairs(instance)
	end

	if type(instance)~="userdata" then
		error("SharedData.pairs: invalid parameter #1 \"instance\": expected table or shared data object, got " .. type(instance), 2)
	end

	local allKeys = kgp.dictGetAllKeys(instance)
	if allKeys==nil then
		error("SharedData.pairs: unable to iterate. Parameter is not a valid shared data object?", 2)
	end

	-- Must use a C function as iterator due to the notorious "attempt to yield across metamethod/C-call boundary" issue
	-- 
	-- Basically the for statement is considered as C function call. If the iterator is implemented in LUA, when a for loop calls 
	-- the iterator it's literally calling lua from C. Thus you cannot tell LUA to yield in an iterator function, unless your LUA
	-- environment supports "yield anywhere" feature (such as LuaJIT, and probably LUA 5.2--pending verification).
	return kgp.dictIterate, {instance, allKeys, 0}, nil
end

-- hook over the standard Lua pairs function
if _G.pairs~=SharedData.pairs then
	luaPairs = _G.pairs
	_G.pairs = SharedData.pairs
end
