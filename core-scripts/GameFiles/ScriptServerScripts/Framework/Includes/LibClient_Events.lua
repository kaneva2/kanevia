--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- LibClient_Events.lua
-- Events library for menu scripts
-- 05/2015 YC - Added native event handling support

dofile("..\\MenuScripts\\MenuHelper.lua")

-- Dev Console I/O
Console = { ERROR = 10, WARN = 20, INFO = 30, DEBUG = 40, TRACE = 50, ModuleOutputLevels = {} }
Console.OutputLevel = Console.INFO

function Console.Write( level, msg )
	local info = { KEP_GetPlayerInfo() }
	local who = info[11]
	local outputLevel = Console.OutputLevel
	if _thisClass ~= nil then
		who = Class and Class.getClassName(_thisClass) or who
		outputLevel = Console.ModuleOutputLevels[who] or outputLevel
	end

	if level <= outputLevel then
		if level >= Console.INFO then
			Events.tellAllDevelopers( msg, who )
		else
			Events.tellAllPlayers( msg, who )
		end
	end

	-- log all ERROR/WARN/INFO messages + anything else dumped in console
	if level<=outputLevel or level<=Console.INFO then
		-- log it automatically
		local strLvl = ""
		for k, v in pairs(Console) do
			if type(k)=="string" and type(v)=="number" and v==level then
				strLvl = k		-- map log level to name
				break
			end
		end
		log( string.format("[%-5s] %s", strLvl, msg) )
	end
end


Events = {}

local LocalRegistry = {}
local NativeEventSpecs = {}
local EventHandlersTracker = {}
local eventHandlerRegistered = false
local FRAMEWORK_EVENT_PREFIX = "ScriptingFramework."
local BAD_EVENT_ID = 0xffff
local SCRIPT_EVENT_MAGIC_FILTER = 0x4652414D		-- "FRAMEWRK"
local SCRIPT_EVENT_MAGIC_OBJECTID = 0x4557524B

-- post process json-decoded table
local function postProcEventTable( luaTable, depth )
	local removal = {}
	for k, v in pairs(luaTable) do
		if type(v)=="userdata" then
			-- cjson compatibility fix: remove any userdata values coming out of json decode function. cjson uses light userdata values for json "null"s.
			removal[k] = true
		elseif type(v)=="table" and depth>0 then
			-- recurse down
			postProcEventTable( v, depth-1 )
		end
	end
	for k, v in pairs(removal) do
		luaTable[k] = nil
	end
end

function Events.encode( luaTable )
	-- validate input
	if type(luaTable)~="table" then
		error("Events.encode: parameter must be a table")
	end

	return cjson.encode(luaTable)
end

function Events.decode( encoded )
	-- quickly validate JSON syntax
	local bracketLeft = string.sub(encoded, 1, 1)
	local bracketRight = string.sub(encoded, -1, -1)
	if	(bracketLeft~="{" or bracketRight~="}") and
		(bracketLeft~="[" or bracketRight~="]") then
		-- ignore this error for now. logging only.
		-- Console.Write( Console.ERROR, "Events.decode: input is not a valid JSON string" )
		log("JSON sanity check failed: \"" .. tostring(encoded) .. "\"" )
		return nil
	end

	-- decode
	local event = cjson.decode(encoded)
	if event==nil then
		Console.Write( Console.WARN, "Events.decode: result is nil for \"" .. tostring(encoded) .. "\"" )
		log("JSON decode failed: \"" .. tostring(encoded) .. "\"" )
		return nil
	end

	-- remove json "null"s from table (encoded as lightuserdata by cjson)
	postProcEventTable( event, 5 )

	return event
end

local function fetchField(luaTable, fieldSpec)
	local fname, ftype, default = fieldSpec[1], fieldSpec[2], fieldSpec[3]
	local value = luaTable[fname]
	if value==nil then
		value = default
	end
	if value~=nil and ftype~=type(value) then
		error("fetchField(\"" .. tostring(fname) .. "\"): type / value mismatch: expecting " .. tostring(ftype) .. " type, got " .. tostring(value) .. " (" .. type(value) .. ")")
	end
	return ftype, value
end

local function storeField(eventData, fieldSpec, value)
	if fieldSpec~=nil then
		local fname, ftype = fieldSpec[1], fieldSpec[2]
		if value~=nil then
			if type(value)~=ftype then
				error("storeField(\"" .. tostring(fname) .. "\"): type / value mismatch: expecting " .. tostring(ftype) .. " type, got " .. tostring(value) .. " (" .. type(value) .. ")")
			end
			eventData[fname] = value
		end
	end
end

local function encodeNativeEvent(event, eventData, eventSpec)
	if eventData==nil then return end

	-- set filter
	if eventSpec.filter then
		-- override filter type
		eventSpec.filter[2] = "number"
		local _, filter = fetchField(eventData, eventSpec.filter)
		if filter~=nil then
			Event_SetFilter(event, filter)
		end
	end
	-- set objectId
	if eventSpec.objectId then
		-- override objectId type
		eventSpec.objectId[2] = "number"
		local _, objectId = fetchField(eventData, eventSpec.objectId)
		if objectId~=nil then
			Event_SetObjectId(event, objectId)
		end
	end
	-- encode data
	for _, fieldSpec in ipairs(eventSpec) do
		local ftype, fval = fetchField(eventData, fieldSpec)
		if fval~=nil then
			if ftype=="string" then
				Event_EncodeString(event, fval)
			elseif ftype=="number" then
				Event_EncodeNumber(event, fval)
			else
				error("encodeNativeEvent: unsupported field type: " .. tostring(ftype))
			end
		end
	end
end

local function decodeNativeEvent(event, filter, objectId, eventSpec)
	local eventData = {}
	if eventSpec==nil then
		return eventData
	end

	-- get filter
	storeField(eventData, eventSpec.filter, filter)
	-- get objectId
	storeField(eventData, eventSpec.objectId, objectId)
	-- get all fields
	for _, fieldSpec in ipairs(eventSpec) do

		local ftype = fieldSpec[2]
		local value
		if ftype=="string" then
			value = Event_DecodeString(event)
		elseif ftype=="number" then
			value = Event_DecodeNumber(event)
		else
			error("decodeNativeEvent: unsupported field type: " .. tostring(ftype))
		end
		storeField(eventData, fieldSpec, value)
	end

	return eventData
end

function Events.tellAllPlayers( text, who )
	Events.sendEvent( "TELL-ALL", { text=text, who=who } )
end

function Events.tellAllDevelopers( text, who )
	Events.sendEvent( "TELL-DEVS", { text=text, who=who } )
end

-- Send event to server-side scripts
function Events.sendEvent( eventName, eventData, pid )
	local eventDataJSON = Events.encode(eventData or {})
	KEP_SendMenuEvent( 2, eventName, eventDataJSON )
end

-- Send event to client-side scripts
function Events.sendClientEvent(eventName, eventData, playerID)
	eventData = eventData or {}
	eventData._type = eventName
	eventData._sender = __SCRIPT__ or "(null)"
	if __MENU__~=nil then
		eventData._senderType = "MENU"
	else
		eventData._senderType = "CLIENT"
	end

	eventName = FRAMEWORK_EVENT_PREFIX .. eventName
	if KEP_EventGetId(eventName)==BAD_EVENT_ID then
		-- Event not registered anywhere. Do not send this event because no handler is not defined for it
	else
		local event = KEP_EventCreate(eventName)
		-- Use some magic numbe to distingish framework events from native events when received
		Event_SetFilter(event, SCRIPT_EVENT_MAGIC_FILTER)
		Event_SetObjectId(event, SCRIPT_EVENT_MAGIC_OBJECTID)
		Event_EncodeString(event, Events.encode(eventData))
		KEP_EventQueue(event)
	end
end

function Events.sendNativeEvent(eventName, eventData)
	if eventData~=nil and type(eventData)~="table" then
		error("Events.sendNativeEvent: event data must be a table")
	end

	local eventSpec = NativeEventSpecs[eventName]
	if eventSpec==nil then
		error("Events.sendNativeEvent: event `" .. tostring(eventName) .. "' not declared")
	end

	if KEP_EventGetId(eventName)==BAD_EVENT_ID then
		-- Event not registered anywhere. Do not send this event because no handler is not defined for it
	else
		local event = KEP_EventCreate(eventName)
		encodeNativeEvent(event, eventData, eventSpec)
		KEP_EventQueue(event)
	end
end

function Events.declareNativeEvents(eventSpecs)
	for eventName, eventSpec in pairs(eventSpecs) do
		NativeEventSpecs[eventName] = eventSpec
		KEP_EventRegister(eventName, KEP.MED_PRIO)
	end
end

function Events.registerHandler( eventName, handler, obj )
	-- Failsafe in case the script is not following the rule
	if not eventHandlerRegistered then
		KEP_Log( "InitializeKEPEventHandlers is overriden by other. Now re-process handler registration for " .. tostring(__SCRIPT__) )
		KEP_EventRegisterHandler( "Events_ScriptClientEventHandler", "ScriptClientEvent", KEP.HIGH_PRIO + 100 )
		KEP_EventRegisterHandler( "Events_ClickEventHandler", "ClickEvent", KEP.HIGH_PRIO + 100 )
		eventHandlerRegistered = true
	end

	-- Validate parameters
	if obj~=nil and type(obj)~="table" and obj~="global" then
		error("Invalid parameter: obj")
	end

	if type(handler)~="function" then
		error("Invalid parameter: handler")
	end

	-- Use LocalRegistry for framework events wrapped in a ScriptClientEvent
	if LocalRegistry[eventName]==nil then
		-- build registry with weak tables
		LocalRegistry[eventName] = setmetatable({}, {__mode="k"})
	end

	if obj==nil then
		-- global handlers (not associated with an instance variable)
		obj = "global"
	end

	if LocalRegistry[eventName][obj]==nil then
		LocalRegistry[eventName][obj] = {}
	end

	LocalRegistry[eventName][obj][handler] = true

	-- Also register it natively for direct client-side dispatching
	local nativeEventName = FRAMEWORK_EVENT_PREFIX .. eventName
	Events.registerNativeHandler(nativeEventName, handler, obj)
end

function Events.registerNativeHandler( eventName, handler, obj )
	if obj~=nil and type(obj)~="table" and obj~="global" then
		error("Invalid parameter: obj")
	end

	if type(handler)~="function" then
		error("Invalid parameter: handler")
	end

	if obj==nil then
		-- global handlers (not associated with an instance variable)
		obj = "global"
	end

	-- Register event name natively
	KEP_EventRegister(eventName, KEP.MED_PRIO)

	-- Generate a global name for the handler
	local autoIndex = (EventHandlersTracker[eventName] or 0) + 1
	EventHandlersTracker[eventName] = autoIndex
	local autoName = eventName .. "#" .. tostring(autoIndex)

	-- Register event handlers natively
	local eventSpec = NativeEventSpecs[eventName]
	local function wrapper(dispatcher, from, event, id, filter, objectId)
		local luaTable
		if eventSpec==nil then
			luaTable = Events.decode(Event_DecodeString(event))
		else
			luaTable = decodeNativeEvent(event, filter, objectId, eventSpec)
		end
		if obj=="global" then
			-- global handlers (not associated with an instance variable)
			handler(luaTable)
		else
			-- class  member handlers
			handler(obj, luaTable)
		end
	end
	_G[autoName] = wrapper
	KEP_EventRegisterHandler(autoName, eventName, KEP.MED_PRIO)
end

function Events_ScriptClientEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	if filter == 4 then
		-- All ScriptClientEvents with a filter of 4 will have the number of arguments encoded into the
		-- event as its first number
        local args = Event_DecodeNumber(event)
        if args == 2 then
            local eventName = Event_DecodeString(event)

			local eventData = nil

			if LocalRegistry[eventName]~=nil then
				for obj, handlers in pairs(LocalRegistry[eventName]) do
					for handler, _ in pairs(handlers) do

						-- Lazy decode of eventData. Only decode if there's a handler for this event.
						if eventData == nil then
							local eventDataJSON = Event_DecodeString(event)
							eventData = Events.decode(eventDataJSON)
							if eventData==nil then
								-- Unrecognized format: legacy event?
								return
							end
						end

						if obj=="global" then
							handler( eventData )
						else
							handler( obj, eventData )
						end
					end
				end
			end
		end
	end
end

function Events_ClickEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)
	Events.dispatchEventToLocalListeners({_type = "MOUSE-LEFT-CLICK", targetID = objectid, targetType = filter})
end

function Events.dispatchEventToLocalListeners( event )
	-- Console.Write( Console.INFO, "dispatchEventToLocalListeners: event._type=" .. event._type )
	local eventHandlerLists = LocalRegistry[event._type]

	if eventHandlerLists~=nil then
		for obj, handlers in pairs(eventHandlerLists) do
			for handler, _ in pairs(handlers) do
				if obj=="global" then
					-- call a global function
					handler( event )
				elseif event._class == nil or event._class == Class.getClassName(obj) then
					-- call a class instance function
					handler( obj, event )
				end
			end
		end
	end
end

-- InitializeKEPEvents - Register all script events here.
function InitializeKEPEvents(dispatcher, handler, debugLevel)
end

-- InitializeKEPEventHandlers - Register all script event handlers here.
function InitializeKEPEventHandlers(dispatcher, handler, debugLevel)
	KEP_EventRegisterHandler( "Events_ScriptClientEventHandler", "ScriptClientEvent", KEP.HIGH_PRIO + 100 )
	KEP_EventRegisterHandler( "Events_ClickEventHandler", "ClickEvent", KEP.HIGH_PRIO + 100 )
	eventHandlerRegistered = true

	if __MENU__==nil and type(onCreate)=="function" then
		-- Not a menu dialog - call onCreate now
		onCreate()
	end
end

-- Dialog_OnCreate = Called upon menu creation.
function Dialog_OnCreate(dialogHandle)
	Helper_Dialog_OnCreate(dialogHandle) -- REQUIRED - DO NOT REMOVE

	-- sanity check
	if __MENU__==nil then
		error("Internal variable `__MENU__' undefined for a menu script")
	end

	if type(onCreate)=="function" then
		onCreate()
	end
end

-- Dialog_OnDestroy - Called upon menu destruction.
function Dialog_OnDestroy(dialogHandle)
	Helper_Dialog_OnDestroy(dialogHandle) -- REQUIRED - DO NOT REMOVE
	if type(onDestroy)=="function" then
		onDestroy()
	end
end
