--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_SpinUpMetrics

SpinUpMetrics = { SPINUP_START_FRAMEWORK = "spinup_start_framework",
				  SPINUP_COMPLETE_FRAMEWORK = "spinup_complete_framework",
				  SPINUP_START_CONTROLLER = "spinup_start_controller",
				  SPINUP_COMPLETE_CONTROLLER = "spinup_complete_controller",
				  SPINUP_PLAYER_ARRIVE = "spinup_player_arrive",
				  SPINUP_PLAYER_ACTIVATE = "spinup_player_activate"
}

local recordControllerMetric --(metric, controllerName)
local recordPlayerMetric --(metric, playerName)
local getZoneInfo --()

local s_iZoneInstanceID
local s_iZoneIndex
local s_iZoneType

function SpinUpMetrics.recordFrameworkSpinupStart()
	recordControllerMetric(SpinUpMetrics.SPINUP_START_FRAMEWORK)
end

function SpinUpMetrics.recordFrameworkSpinupComplete()
	recordControllerMetric(SpinUpMetrics.SPINUP_COMPLETE_FRAMEWORK)
end

function SpinUpMetrics.recordControllerSpinupStart(controllerName)
	recordControllerMetric(SpinUpMetrics.SPINUP_START_CONTROLLER, controllerName)
end

function SpinUpMetrics.recordControllerSpinupComplete(controllerName)
	recordControllerMetric(SpinUpMetrics.SPINUP_COMPLETE_CONTROLLER, controllerName)
end

function SpinUpMetrics.recordPlayerArrive(playerName)
	recordPlayerMetric(SpinUpMetrics.SPINUP_PLAYER_ARRIVE, playerName)
end

function SpinUpMetrics.recordPlayerActivate(playerName)
	recordPlayerMetric(SpinUpMetrics.SPINUP_PLAYER_ACTIVATE, playerName)
end

recordControllerMetric = function(metric, controllerName)
	getZoneInfo()
	local currTime = kgp.gameGetCurrentTime() or 0
	local input = {timestamp = currTime, zoneInstanceId = s_iZoneInstanceID, zoneType = s_iZoneType, controller = controllerName, metric = metric}
	log("SPINUP METRIC - framework_spinup - metric = "..tostring(metric)..", controller = "..tostring(controllerName))
	kgp.scriptRecordMetric("framework_spinup", input)
end

recordPlayerMetric = function(metric, playerName)
	getZoneInfo()
	local currTime = kgp.gameGetCurrentTime() or 0
	local input = {timestamp = currTime, zoneInstanceId = s_iZoneInstanceID, zoneType = s_iZoneType, player = playerName, metric = metric}
	log("SPINUP METRIC - framework_spinup - metric = "..tostring(metric)..", player = "..tostring(playerName))
	kgp.scriptRecordMetric("framework_spinup", input)
end

getZoneInfo = function()
	if s_iZoneInstanceID == nil or s_iZoneIndex == nil or s_iZoneType == nil then
		s_iZoneInstanceID, s_iZoneIndex, s_iZoneType = kgp.gameGetCurrentInstance()
	end
end