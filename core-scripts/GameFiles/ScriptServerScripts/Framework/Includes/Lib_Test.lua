--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Script Testing Framework
--
-- YFC 03/2015 based on Google Test Framework and WA's Framework Test Scripts

-----------------------------------------------------------------------------
-- All assertion predicates
local predicates = {
	{"T",   {"== true"}},
	{"F",   {"== false"}},
	{"NIL", {"== nil"}},
	{"EQ",  {"==", ""}},
	{"NE",  {"~=", ""}},
	{"GT",  {">" , ""}},
	{"LT",  {"<" , ""}},
	{"GE",  {">=" , ""}},
	{"LE",  {"<=", ""}},
	{"IN",  {"be any of", ""}},		-- be any of the items in a list
	{"NIN", {"be none of", ""}},	-- be none of the items in a list
	{"NR",  {"near"     , ", tolerance: +/-", ""}},	-- near, within tolerance
	{"AWY", {"away from", ", tolerance: +/-", ""}},	-- away from, outside tolerance
	{"XEQ", {"===", ""}},			-- reference equal for tables/userdata/functions
	{"XNE", {"~==", ""}},			-- reference not equal
	{"TYP", {"be type of", ""}},	-- type check
	{"NFL", {"has no field", ""}},	-- key does not exist
	{"FLD", {"has field", ""}},		-- key exists
	{"FLT", {"has field", "of type", ""}},		-- key exists and value type matches
	{"FLV", {"has field", "with value", ""}},	-- key value pair exists
	{"MTD", {"has method by name", ""}},
}

-- Generate enumerates
local PRED = {}
for i, predicate in ipairs(predicates) do
	PRED[predicate[1]] = i
end

-----------------------------------------------------------------------------
-- Some magic numbers

local MAX_TABLE_EQ_DEPTH = 5	-- maximum depth of table allowed for tableEqual()
local MAX_LISTSTR_ITEMS  = 20	-- maximum array index for tableToStr()
local MAX_LISTSTR_LENGTH = 80	-- maximum length of the string allowed for tableToStr()
local STD_FP_TOLERANCE   = 1E-6	-- current floating point tolerance
local MAX_FP_TOLERANCE   = 1E-3	-- maximuim floating point tolerance

-----------------------------------------------------------------------------
-- Local helper functions
local tableToStr

local function toStr( v )
	if type(v)=="string" then return "\"" .. v .. "\"" end
	if type(v)=="table" then return tableToStr(v) end
	return tostring(v)
end

function tableToStr( t )
	if type(t)~="table" then return "*" .. type(t) .. "*" end

	for k, v in pairs(t) do
		if type(k)~="number" or k>#t then
			return tostring(t)	-- if non-numeric key or stray key found, return a generic text
		end
	end

	-- return comma separated text
	local str = "{"
	for i=1, math.min(#t, MAX_LISTSTR_ITEMS) do
		local token
		if type(t[i])=="table" then
			token = tostring(t[i])	-- nested tables - don't display contents
		else
			token = toStr(t[i])
		end
		if #token + 3 + #str >= MAX_LISTSTR_LENGTH then
			str = str .. ", ..."
			break
		end

		if i>1 then
			str = str .. ", "
		end
		str = str .. token
	end
	return str .. "}"
end

local function predToStr( op, ... )
	local arg = {...}
	local str = ""
	if predicates[op] ~= nil then
		for i, token in ipairs(predicates[op][2]) do
			str = str .. toStr(arg[i]) .. " " .. tostring(token) .. " "
		end
	end
	return str
end

local function tableEqual( v1, v2, tolerance, depth, keyPath )
	if type(v1)~="table" or type(v2)~="table" then return nil, "Internal: tableEqual against non-table values" end
	if v1==v2 then return nil, "Internal: tableEqual against tables of equal references" end	-- same table

	depth = depth or MAX_TABLE_EQ_DEPTH
	if depth==0 then return nil, "attempt to compare tables with depth > " .. tostring(MAX_TABLE_EQ_DEPTH) end	-- max depth exceeded

	keyPath = keyPath or ""

	for key, val1 in pairs(v1) do
		local val2 = v1[key]
		local keyPath1 = keyPath .. "." .. key
		if type(key)~="number" and type(key)~="string" then
			return nil, "attempt to compare tables with unsupported keys: expecting number or string, got " .. type(key)
		elseif val2==nil then
			return false, "left." .. keyPath1 .. " (" .. toStr(val1) .. ") does not exist on the right side"
		elseif type(val1)~=type(val2) then
			return false, "left." .. keyPath1 .. " is a " .. type(val1) .. " while right." .. keyPath1 .. " is a " .. type(val2)
		elseif type(val1)=="table" and val1~=val2 then
			local res, msg = tableEqual(val1, val2, tolerance, depth-1, keyPath1)
			if not res then
				return res, msg
			end
		elseif val1~=val2 then
			return false, "left." .. keyPath1 .. " (" .. toStr(val1) .. ") does not equals to right." .. keyPath1 .. " (" .. toStr(val2) .. ")"
		end
	end

	for key, _ in pairs(v2) do
		local keyPath1 = keyPath .. "." .. key
		if v1[key]==nil then
			return false, "left lacks key " .. keyPath1
		end
	end

	return true
end

local function equal( v1, v2, tolerance )
	if type(v1)~=type(v2) then
		if v1~=nil and v2~=nil then
			return nil, "attempt to compare two values of different types"
		else
			return false	-- compare not nil with nil
		end
	elseif type(v1)=="table" and v1==v2 then
		return nil, "attempt to compare two tables of equal references"
	elseif type(v1)=="userdata" or type(v1)=="function" then
		return nil, "attempt to compare two " .. type(v1) .. " values"
	elseif type(v1)=="table" and v1~=v2 then
		return tableEqual(v1, v2, tolerance)
	elseif v1~=v2 and (type(v1)~="number" or math.abs(v1-v2)>(tolerance or STD_FP_TOLERANCE)) then
		return false
	else
		return true
	end
end

local function less( v1, v2 )
	if type(v1) ~= type(v2) then
		return nil, "attempt to compare two values of different types"
	elseif type(v1)~="number" and type(v1)~="string" then
		return nil, "attempt to get order on unsupported types: expecting number of string, got " .. tostring(v1)
	elseif v1==v2 then
		return false
	elseif v1 > v2 then
		return false
	else
		return true
	end
end

local function inList( item, list, tolerance )
	if type(list)~="table" then
		return nil, "right hand side is not a table"
	else
		for i, val in ipairs(list) do
			if equal(item, val, tolerance) then
				return true, "match found in the list"
			end
		end
		return false, "match not found in the list"
	end
end

local function near( val, target, tolerance )
	if type(val)~="number" then
		return nil, "the value being tested is not a number"
	elseif type(target)~="number" then
		return nil, "the target to be tested against is not a number"
	elseif type(tolerance)~="number" then
		return nil, "tolerance must be a number"
	elseif math.abs(val - target)>tolerance then
		return false, "value is beyond tolerance"
	else
		return true, "value is within tolerance"
	end
end

-- Send a message to client test menu
local function tellOwner(what, owner)
	--TODO: avoid using Events library
	Events.sendClientEvent("FRAMEWORK_DEBUGGER", what, owner)
end

-----------------------------------------------------------------------------
-- The Test fixture class
Test = {}

local TEST_SCRIPT_PREFIX = "TEST_"
local TEST_SCRIPT_EXTENSION = ".lua"
local function makeScriptPath( groupName, testName )
	-- return groupName .. "/" .. TEST_SCRIPT_PREFIX .. testName .. TEST_SCRIPT_EXTENSION
	return TEST_SCRIPT_PREFIX .. testName .. TEST_SCRIPT_EXTENSION
end

function Test.new( initializers )
	local newTest = {}
	setmetatable(newTest, {__index = initializers.class or Test})
	newTest:create(initializers)
	return newTest
end

function Test.create( self, initializers )
	-- Make test class, name, group and owner read-only
	local class = initializers.class
	local name  = initializers.name
	local group = initializers.group
	local owner = initializers.owner
	local controllerPID = initializers.controllerPID

	self.getClass = function() return class end
	self.getName  = function() return name end
	self.getGroup = function() return group end
	self.getOwner = function() return owner end
	self.getControllerPID = function() return controllerPID end

	self.tasks = {}
    self.data = {}
end

function Test.addTask( self, name, func )
	if type(name)~="string" then
		error("Invalid test name, expecting string, got " .. type(name))
	elseif type(func)~="function" then
		error("Invalid test body, expecting function, got " .. type(func))
	end

	table.insert(self.tasks, {name = name, func = func})
end

function Test.setUpTestCase( self )
	-- Reset last test result
	self.lastResult = true
	self.lastError = nil
	self.currTaskIndex = nil

	-- TEST helpers for the test scripts
	_G.TEST = function(name, func) self:addTask(name, func) end

	-- Load test script with pcall
	self.scriptPath = makeScriptPath( self:getGroup(), self:getName() )
	local ok, res = pcall(function() self:loadScript(self.scriptPath) end)

	-- Remove helpers
	_G.TEST = nil

	if not ok then
		self.lastResult = false
		self.lastError = res
	end
end

function Test.tearDownTestCase( self )
end

function Test.setUp( self )
end

function Test.tearDown( self )
end

function Test.loadScript( self, scriptPath )
	self:log("Loading test script: " .. scriptPath)
	local res, err = kgp.scriptDoFile(scriptPath)
	if res then
		self:log("Loaded test script: " .. scriptPath)
	else
		error("[" .. scriptPath .. "]" .. tostring(err))
	end
end

-- Execute next test item
function Test.nextTask( self )
	-- sanity check
	if #self.tasks==0 then
		self:log("No tasks defined in test script: " .. self.scriptPath)
		return false
	end

	-- Prepare
	self.currTaskIndex = (self.currTaskIndex or 0) + 1
	local task = self.tasks[self.currTaskIndex]
	task.lastResult = true
	self:setUp()

	self:log("TESTING [" .. tostring(task.name) .. "]")

	-- Increase Indentation
	local lastIndent = self.indent
	self.indent = (self.indent or "") .. "  "

	-- Execute and record result
	local ok, res = pcall(function() return task.func(self) end)

	-- Check for unexpected exception
	if task.lastResult and not ok then
		self:miss("Unexpected error", res)
	end

	-- Restore Indentation
	self.indent = lastIndent

	-- Analyze test result
	if task.lastResult==true then
		self:log("PASSED  [" .. tostring(task.name) .. "]")
		if not self.resultReportedByScript then				-- For certain fixtures, results are sent to owner by the scripts
			tellOwner({type = "reportResult", class = self:getName(), funcName = task.name, result = true, details = "Test passed"}, self:getOwner())
		end
	elseif task.lastResult==false then
		-- error occurred / assertion failed
		self:log("FAILED  [" .. tostring(task.name) .. "]")
		if not self.resultReportedByScript or not ok then	-- For certain fixtures, results are sent to owner by the scripts
			tellOwner({type = "reportResult", class = self:getName(), funcName = task.name, result = false, details = "Test failed"}, self:getOwner())
		end
	else
		-- something is wrong
		self:log("ERROR   [" .. tostring(task.name) .. "]: last result is " .. tostring(task.lastResult))
		if not self.resultReportedByScript then				-- For certain fixtures, results are sent to owner by the scripts
			tellOwner({type = "reportResult", class = self:getName(), funcName = task.name, result = false, details = "Unknown result: " .. tostring(task.lastResult)}, self:getOwner())
		end
	end

	-- Cleanup
	self:tearDown()

	if self.currTaskIndex<#self.tasks then
		return true, res		-- the task function can optionally return a time delay (in ms) before next task is launched
	else
		self.currTaskIndex = nil
		return false
	end
end

function Test.log( self, message )
	if type(self.currTaskIndex)=="number" then
		kgp.gameLogMessage("[" .. tostring(self.scriptPath) .. "] Test " .. string.format("%02d", self.currTaskIndex) .. ": " .. (self.indent or "") .. tostring(message))
	else
		kgp.gameLogMessage("[" .. tostring(self.scriptPath) .. "]: " .. (self.indent or "") .. tostring(message))
	end

	tellOwner({type = "log", message = tostring(message)}, self:getOwner())
end

-- Expectation missed
function Test.miss( self, predStr, msg )
	self.tasks[self.currTaskIndex].lastResult = false	-- Current task failed
	self.lastResult = false								-- Entire test case failed

	-- Log the error message
	if msg then	
		self:log("FAILED " .. predStr .. ": " .. msg)
	else
		self:log("FAILED " .. predStr)
	end
end

-- Assertion failed
function Test.fail( self, predStr, msg )
	rawset(self, "miss", nil)		-- remove temporary override if any

	-- Call miss()
	self:miss(predStr, msg)

	-- raise an error to terminate current task
	local errorMsg = predStr
	if msg then	errorMsg = predStr .. ": " .. msg end

	error(errorMsg, 2)
end

function Test.setFPTolerance( self, tol )
	if type(tol)==number and tol>=0 and tol<MAX_FP_TOLERANCE then
		self.tolerance = tol
	else
		error("Invalid input for setFPTolerance", 2)
	end
end

function Test.getFPTolerance( self )
	return self.tolerance or STD_FP_TOLERANCE
end

function Test.expectTrue( self, v1 )
	local predStr = predToStr(PRED.T, v1)
	if v1~=true then
		self:miss(predStr)
	end
end

function Test.expectFalse( self, v1 )
	local predStr = predToStr(PRED.F, v1)
	if v1~=false then
		self:miss(predStr)
	end
end

function Test.expectNil( self, v1 )
	local predStr = predToStr(PRED.NIL, v1)
	if v1~=nil then
		self:miss(predStr)
	end
end

function Test.expectEQ( self, v1, v2 )
	local predStr = predToStr(PRED.EQ, v1, v2)
	if v1==nil and v2==nil then
		self:miss(predStr, "NIL values found at both side - using expectNil to avoid false positives caused by faulty rules")
	else
		local res, msg = equal(v1, v2, self:getFPTolerance())
		if res~=true then		-- res is a three-way value. make sure to use explicit true/false test to catch any exception conditions
			self:miss(predStr, msg)
		end
	end
end

function Test.expectNE( self, v1, v2 )
	local predStr = predToStr(PRED.NE, v1, v2)
	local res, msg = equal(v1, v2, self:getFPTolerance())
	if res~=false then		-- res is a three-way value. make sure to use explicit true/false test to catch any exception conditions
		self:miss(predStr, msg)
	end
end

function Test.expectGT( self, v1, v2 )
	local predStr = predToStr(PRED.GT, v1, v2)
	local equ, msgEqu, les, msgLes

	equ, msgEqu = equal(v1, v2, 0)		-- zero tolerance on ordering
	if equ~=false then		-- res is a three-way value. make sure to use explicit true/false test to catch any exception conditions
		self:miss(predStr, msgEqu)
	end

	les, msgLes = less(v1, v2)
	if les~=false then		-- res is a three-way value. make sure to use explicit true/false test to catch any exception conditions
		self:miss(predStr, msgLes)
	end
end

function Test.expectLT( self, v1, v2 )
	local predStr = predToStr(PRED.LT, v1, v2)
	local res, msg

	res, msg = less(v1, v2)
	if res~=true then		-- res is a three-way value. make sure to use explicit true/false test to catch any exception conditions
		self:miss(predStr, msg)
	end
end

function Test.expectGE( self, v1, v2 )
	local predStr = predToStr(PRED.GE, v1, v2)
	local res, msg

	res, msg = less(v1, v2)
	if res~=false then		-- res is a three-way value. make sure to use explicit true/false test to catch any exception conditions
		self:miss(predStr, msg)
	end
end

function Test.expectLE( self, v1, v2 )
	local predStr = predToStr(PRED.LE, v1, v2)
	local equ, msgEqu, les, msgLes

	local equ, msgEqu = equal(v1, v2, 0)	-- zero tolerance on ordering
	local les, msgLes = less(v1, v2)
	if equ~=true and les~=true then		-- res is a three-way value. make sure to use explicit true/false test to catch any exception conditions
		self:miss(predStr, msgEqu or msgLes)
	end
end

function Test.expectInList( self, v1, v2 )
	local predStr = predToStr(PRED.IN, v1, v2)
	local res, msg = inList(v1, v2, self:getFPTolerance())
	if res~=true then
		self:miss(predStr, msg)
	end
end

function Test.expectNotInList( self, v1, v2 )
	local predStr = predToStr(PRED.IN, v1, v2)
	local res, msg = inList(v1, v2, self:getFPTolerance())
	if res~=false then
		self:miss(predStr, msg)
	end
end

function Test.expectNear( self, v1, v2, v3 )
	local predStr = predToStr(PRED.NR, v1, v2, v3)
	local res, msg = near(v1, v2, v3)
	if res~=true then
		self:miss(predStr, msg)
	end
end

function Test.expectAwayFrom( self, v1, v2, v3 )
	local predStr = predToStr(PRED.AWY, v1, v2, v3)
	local res, msg = near(v1, v2, v3)
	if res~=false then
		self:miss(predStr, msg)
	end
end

function Test.expectRefEQ( self, v1, v2 )
	local predStr = predToStr(PRED.XEQ, v1, v2)
	if v1==nil and v2==nil then
		self:miss(predStr, "NIL values found at both side - using expectNil to avoid false positives caused by faulty rules")
	elseif v1~=v2 then
		self:miss(predStr)
	end
end

function Test.expectRefNE( self, v1, v2 )
	local predStr = predToStr(PRED.XNE, v1, v2)
	if v1==v2 then
		self:miss(predStr)
	end
end

function Test.expectType( self, v1, v2 )
	local predStr = predToStr(PRED.TYP, v1, v2)
	if type(v1)~=v2 then
		self:miss(predStr, "Expecting type " .. tostring(v2) .. ", got " .. type(v1))
	end
end

function Test.expectUndefinedField( self, v1, v2 )
	local predStr
	predStr = predToStr(PRED.NFL, v1, v2)

	if type(v1)~="table" and type(v1)~="userdata" then
		self:miss(predStr, "Left value should be a table or a dictionary, got " .. type(v1))
	elseif type(v2)=="nil" then
		self:miss(predStr, "Key should not be nil")
	elseif v1[v2]~=nil then
		self:miss(predStr, "Key exists")
	end
end

function Test.expectField( self, v1, v2, v3 )
	local predStr
	if v3==nil then
		predStr = predToStr(PRED.FLD, v1, v2)
	else
		predStr = predToStr(PRED.FLV, v1, v3, v2)
	end

	if type(v1)~="table" and type(v1)~="userdata" then
		self:miss(predStr, "Left value should be a table or a dictionary, got " .. type(v1))
	elseif type(v2)=="nil" then
		self:miss(predStr, "Key should not be nil")
	elseif v1[v2]==nil then
		self:miss(predStr, "Key does not exist")
	elseif v3~=nil and not equal(v1[v2], v3) then
		self:miss(predStr, "Key exists but value is " .. toStr(v1[v2]))
	end
end

function Test.expectFieldType( self, v1, v2, v3 )
	local predStr = predToStr(PRED.FLT, v1, v2, v3)

	if type(v1)~="table" and type(v1)~="userdata" then
		self:miss(predStr, "Left value should be a table or a dictionary, got " .. type(v1))
	elseif type(v2)=="nil" then
		self:miss(predStr, "Key should not be nil")
	elseif type(v3)~="string" then
		self:miss(predStr, "Value type should be a string, got " .. type(v3))
	elseif v1[v2]==nil then
		self:miss(predStr, "Key does not exist")
	elseif type(v1[v2])~=v3 then
		self:miss(predStr, "Key exists but value is a " .. type(v1[v2]))
	end
end

function Test.expectMethod( self, v1, v2 )
	local predStr = predToStr(PRED.MTD, v1, v2)
	if type(v1)~="table" and type(v1)~="userdata" then
		self:miss(predStr, "Left value should be a table or a dictionary, got " .. type(v1))
	elseif type(v2)=="nil" then
		self:miss(predStr, "Right value should not be nil")
	elseif v1[v2]==nil then
		self:miss(predStr, "Key does not exist")
	elseif type(v1[v2])~="function" then
		self:miss(predStr, "Key exists but value is " .. toStr(v1[v2]))
	end
end

-- Generate assert fuctions based on all expects
local allExpects = {}
for k, v in pairs(Test) do
	if type(v)=="function" and #k>6 and string.sub(k, 1, 6)=="expect" then
		table.insert(allExpects, k)
	end
end
for _, expectName in pairs(allExpects) do
	local expectFunc = Test[expectName]
	local assertName = "assert" .. string.sub(expectName, 7)
	Test[assertName] = function( self, ... )
		rawset(self, "miss", Test.fail)	-- temporarily override miss function
		expectFunc( self, ... )
		rawset(self, "miss", nil)		-- remove temporary override
	end
end

-----------------------------------------------------------------------------
-- Test class for legacy test scripts
TestLegacy = { _super = Test, resultReportedByScript = true }
setmetatable(TestLegacy, {__index = TestLegacy._super})	-- inherits Test class

-- Overrides
function TestLegacy.setUpTestCase( self )
	-- Initializations
	self._G_backup = {}
	self._G_backup.g_tester     = _G.g_tester
	self._G_backup.g_testerSD   = _G.g_testerSD
	self._G_backup.testNext     = _G.testNext
	self._G_backup.reportResult = _G.reportResult

	-- Make legacy scripts happy. TODO: get rid of them
	_G.g_tester = self:getOwner()
	_G.g_testerSD = Events.getPlayer(self:getOwner())
	_G.testNext = function() end
	_G.reportResult =
		function(result, class, funcName, details)
			tellOwner({type = "reportResult", class = class, funcName = funcName, result = result, details = details or ""}, self:getOwner())
			if result==false then
				self.lastResult = false
				self.lastError = details
			end
		end

	-- Call super class to load the test script
	TestLegacy._super.setUpTestCase(self)

	-- Get the wrapper class
	local legacyTestWrapperClass = _G[self:getName()]
	if type(legacyTestWrapperClass)~="table" or type(legacyTestWrapperClass.create)~="function" then
		error("TestLegacy class is incompatible with test script: " .. self.scriptPath)
	end

	-- Create wrapper class instance
	local legacyTestWrapper = legacyTestWrapperClass.create()
	if not legacyTestWrapper or type(legacyTestWrapper.test)~="function" then
		error("TestLegacy class is incompatible with test script: " .. self.scriptPath)
	end

	-- Add a task for the entire script
	self:addTask(self:getName(), function() legacyTestWrapper:test() end)
end

function TestLegacy.tearDownTestCase( self )
	-- Call super class
	TestLegacy._super.tearDownTestCase(self)

	-- Cleanup
	_G.g_tester     = self._G_backup.g_tester
	_G.g_testerSD   = self._G_backup.g_testerSD
	_G.testNext     = self._G_backup.testNext
	_G.reportResult = self._G_backup.reportResult
	self._G_backup = nil
end


-----------------------------------------------------------------------------
-- TestProgress class
TestProgress = {}

-- Allocator
function TestProgress.new( initializers )
	local newProgress = {}
	setmetatable(newProgress, {__index = TestProgress})
	newProgress:create(initializers)	-- call constructor
	return newProgress
end

-- Constructor
function TestProgress.create( self, initializers )
	-- Set owner as read-only
	local owner = initializers.owner
	self.getOwner = function() return owner end

	-- Reset
	self:rewind()
end

-- Send entire manifest to client
function TestProgress.manifest( self )
	kgp.gameLogMessage("TestProgress.manifest")
	--TODO: fix this - send tests from all groups in a flat list for now
	local testDefinitions = {}
	for gi = 1, TestManifest.getGroupCount() do
		for i = 1, TestManifest.getTestCountInGroup(gi) do
			local groupName, testName, _, totalTests = TestManifest.getTestDefinition(gi, i)
			if groupName and testName and totalTests then
				table.insert(testDefinitions, {name=testName, group=groupName, totalTests=totalTests})
			end
		end
	end

	kgp.gameLogMessage("TestProgress.manifest: send test list to " .. tostring(self:getOwner()))
	tellOwner({type = "classList", classes = testDefinitions}, self:getOwner())
end

function TestProgress.rewind( self )
	self.currGroupIndex = 1
	self.currTestIndex = 0
end

-- Return false if seek beyond the end. Otherwise true.
function TestProgress.next( self )
	self.currTestIndex = self.currTestIndex + 1
	if self.currTestIndex > TestManifest.getTestCountInGroup(self.currGroupIndex) then
		-- Group complete: try next group
		self.currGroupIndex = self.currGroupIndex + 1
		self.currTestIndex = 0

		if self.currGroupIndex > TestManifest.getGroupCount() then
			-- no more
			self.currGroupIndex = 0
			self.currTestIndex = 0
			return false
		else
			-- first test in next group
			return self:next()
		end
	end

	return true
end

-- Return false if test name not found. Otherwise true.
function TestProgress.seek( self, groupName, testName )
	local groupIndex, testIndex = TestManifest.findTest( groupName, testName )
	if groupIndex~=nil and testIndex~=nil then
		self.currGroupIndex = groupIndex
		self.currTestIndex = testIndex
		return true
	end

	return false
end

function TestProgress.testNext( self )
	if self:next() then
		self:testOne()
		return true
	end

	return false
end

-- Execute current test
function TestProgress.testOne( self )
	local groupName, testName, testClass, _ = TestManifest.getTestDefinition(self.currGroupIndex, self.currTestIndex)
	if not groupName or not testName or not testClass then
		-- Something is wrong
		error("TestProgress.testOne: getTestDefinition(group: " .. tostring(self.currGroupIndex) .. ", test: " .. tostring(self.currTestIndex) .. ") failed")
		return
	end

	-- destroy existing container
	if self.currTestContainer then
		self.currTestContainer:destroy()
		self.currTestContainer = nil
	end

	kgp.gameLogMessage("TestProgress.testOne: send testBegin [" .. tostring(testName) .. "] to " .. tostring(self:getOwner()))
	tellOwner({type = "testBegin", class = testName}, self:getOwner())

	-- create test container (and test launches automatically)
	self.currTestContainer = TestContainer.new{groupName = groupName, testName = testName, testClass=testClass, owner = self:getOwner()}
end

function TestProgress.complete( self )
	--TODO: testComplete is inconsisent with testBegin
	kgp.gameLogMessage("TestProgress.testOne: send testComplete to " .. tostring(self:getOwner()))
	tellOwner({type = "testComplete", test = "All"}, self:getOwner())
end

function TestProgress.destroy( self )
	--TODO
end


-----------------------------------------------------------------------------
-- TestManifest singleton
TestManifest = {}

local testQueue = {}
local testerQueue = {}
local testsInProgress = {}

local function isKnownTester( requester )
	for i, tester in ipairs(testerQueue) do
		if tester == requester then
			return true
		end
	end
	return false
end

function TestManifest.addGroup( groupName, testDefinitions, groupTestClass )
	groupTestClass = groupTestClass or "Test"

	local group = {}
	for i, testDef in ipairs(testDefinitions) do
		if type(testDef)=="string" then
			testDef = {testDef, groupTestClass}
		end
		if type(testDef)~="table" then
			error("Invalid test definition at " .. tostring(groupName) .. "[" .. tostring(i) .. "]", 2)
		end

		local testName = testDef[1]
		local testClass = testDef[2]
		if type(testName)~="string" then
			error(tostring(groupName) .. "[" .. tostring(i) .. "]: [" .. tostring(testName) .. "] is not a valid test name", 2)
		elseif type(_G[testClass])~="table" then
			error(tostring(groupName) .. "[" .. tostring(i) .. "]: [" .. tostring(testClass) .. "] is not a valid test class", 2)
		end

		table.insert(group, {name = testName, class = testClass, totalTests = 0})
	end

	table.insert(testQueue, {groupName, group})
end

function TestManifest.getGroupCount()
	return #testQueue
end

function TestManifest.getTestCountInGroup( groupIndex )
	if not testQueue[groupIndex] then
		return nil
	end
	return #testQueue[groupIndex][2]
end

function TestManifest.findTest( groupName, testName )
	for gi, group in ipairs(testQueue) do
		if groupName==nil or groupName==group[1] then
			for ti, test in ipairs(group[2]) do
				if test.name==testName then
					return gi, ti
				end
			end
		end
	end

	return nil, nil
end

function TestManifest.getTestDefinition( groupIndex, testIndex )
	local groupName, testName, testClass, totalTests
	local group = testQueue[groupIndex]
	if group~=nil then
		local test = group[2][testIndex]
		if test~=nil then
			groupName = group[1]
			testName = test.name
			testClass = test.class
			totalTests = test.totalTests
		end
	end
	return groupName, testName, testClass, totalTests
end

function TestManifest.addTester( clientId )
	kgp.gameLogMessage("TestManifest.addTester: " .. tostring(clientId))
	local perm = kgp.playerGetPermissions(clientId)
	if perm == 1 then
		table.insert(testerQueue, clientId)
		kgp.playerLoadMenu(clientId, "Framework_Tester.xml")	-- menu is loaded dormant until
	end
end

function TestManifest.removeTester( clientId )
	kgp.gameLogMessage("TestManifest.removeTester: " .. tostring(clientId))
	if type(clientId)~="number" then return	end

	for i=#testerQueue, 1, -1 do
		if testerQueue[i]==clientId then
			table.remove(testerQueue, i)
		end
	end
end

function TestManifest.acquire( requester )
	kgp.gameLogMessage("TestManifest.acquire: " .. tostring(requester))
	if not isKnownTester(requester) then
		return nil, "Unknown tester " .. tostring(requester)
	end

	if testsInProgress[requester]~=nil then
		return nil, "Test already in progress for tester " .. tostring(requester)
	end

	local progress = TestProgress.new({owner = requester})
	testsInProgress[requester] = progress

	-- Send manifest to owner
	progress:manifest()
	return progress
end

function TestManifest.release( requester )
	if not isKnownTester(requester) then
		return nil, "Unknown tester " .. tostring(requester)
	end

	local progress = testsInProgress[requester]
	if progress==nil then
		return nil, "No test progress found for tester " .. tostring(requester)
	end

	progress:destroy()
	testsInProgress[requester] = nil
	return true
end

function TestManifest.getProgress( requester )
	if not isKnownTester(requester) then
		return nil, "Unknown tester " .. tostring(requester)
	end

	local progress = testsInProgress[requester]
	if progress==nil then
		return nil, "No test progress found for tester " .. tostring(requester)
	end

	return progress
end

function TestManifest.debug()
	for i, group in ipairs(testQueue) do
		kgp.gameLogMessage("testQueue[" .. tostring(i) .. "] = {\"" .. tostring(group[1]) .. "\", " .. #group[2] .. " tests }")
	end

	for i, tester in ipairs(testerQueue) do
		kgp.gameLogMessage("testerQueue[" .. tostring(i) .. "] = " .. tostring(tester))
	end

	for i, prog in pairs(testsInProgress) do
		kgp.gameLogMessage("testsInProgress: " .. tostring(i))
	end
end

-----------------------------------------------------------------------------
-- TestContainer class
TestContainer = {}

-- Container message format: stf://<TestClass>:<OwnerId>/<Group>/<Test>
-- E.g. stf://Test:10354684/Libraries/Lib_Class

local TEST_CASE_MSG_PREFIX = "stf://"
local TEST_SCRIPT_PATH_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-:/"
local TEST_CASE_COMPLETE_MSG_PREFIX = "TEST_CASE_COMPLETE:"
local TEST_NEXT_MSG = "TEST_NEXT"

-- TestContainer private functions
local function makeTestCaseMessage(testClass, owner, groupName, testName)
	return TEST_CASE_MSG_PREFIX .. testClass .. ":" .. tostring(owner) .. "/" .. groupName .. "/" .. testName
end

local function makeTestCaseCompletionMessage(owner)
	return TEST_CASE_COMPLETE_MSG_PREFIX .. tostring(owner)
end

local function checkTestCaseMessage(message)
	if string.sub(message, 1, #TEST_CASE_MSG_PREFIX) == TEST_CASE_MSG_PREFIX then
		return string.sub(message, #TEST_CASE_MSG_PREFIX+1)
	else
		return nil
	end
end

local function checkTestCaseCompletionMessage(message)
	if string.sub(message, 1, #TEST_CASE_COMPLETE_MSG_PREFIX) == TEST_CASE_COMPLETE_MSG_PREFIX then
		return tonumber(string.sub(message, #TEST_CASE_COMPLETE_MSG_PREFIX+1))
	else
		return nil
	end
end

local function parseContainerMessage(message)
	local path = checkTestCaseMessage(message)
	if not path then
		return nil
	end

	for i = 1, #path do
		local c = string.sub(path, i, i)
		if not string.find(TEST_SCRIPT_PATH_CHARACTERS, c) then
			return nil
		end
	end

	-- Two slashes exactly
	local ofsSlash1, ofsSlash2, ofsSlash3, ofsColon1, ofsColon2
	ofsSlash1 = string.find(path, "/")
	if ofsSlash1 then
		ofsSlash2 = string.find(path, "/", ofsSlash1+1)
		if ofsSlash2 then
			ofsSlash3 = string.find(path, "/", ofsSlash2+1)
		end
	end

	if ofsSlash1==nil or ofsSlash2==nil or ofsSlash3~=nil then
		return nil
	end

	-- One colon exactly before first slash
	ofsColon1 = string.find(path, ":")
	if ofsColon1 then
		ofsColon2 = string.find(path, ":", ofsColon1+1)
	end

	if ofsColon1==nil or ofsColon2~=nil or ofsColon1>=ofsSlash1 then
		return nil
	end

	-- Break down
	local testClass = string.sub(path, 1, ofsColon1-1)
	local testOwner = string.sub(path, ofsColon1+1, ofsSlash1-1)
	local groupName = string.sub(path, ofsSlash1+1, ofsSlash2-1)
	local testName = string.sub(path, ofsSlash2+1)
	if testClass=="" or tonumber(testOwner)==nil or groupName=="" or testName=="" then
		return nil
	end

	return testClass, tonumber(testOwner), groupName, testName
end

-- TestContainer class instantiation
function TestContainer.new( initializers, self )
	local newContainer = self or {}
	setmetatable(newContainer, {__index = TestContainer})
	newContainer:create(initializers)
	return newContainer
end

-- TestContainer class constructor
function TestContainer.create( self, initializers )
	kgp.gameLogMessage("TestContainer.create")
	self.PID = kgp.objectGenerate(GLID_SINGULARITY, 0, -5000, -5000, 1, 0, 0)
	self.owner = initializers.owner
	self.groupName = initializers.groupName
	self.testName = initializers.testName
	self.testClass = initializers.testClass
	kgp.gameLogMessage("TestContainer.create: PID=" .. tostring(self.PID) .. ", owner=" .. tostring(self.owner) .. ", group=" .. tostring(self.groupName) .. ", test=" .. tostring(self.testName) .. ", fixture=" .. tostring(self.testClass))

	kgp.objectSetScript(self.PID, "Framework_TestContainer.lua", makeTestCaseMessage(self.testClass, self.owner, self.groupName, self.testName))
end

-- TestContainer class destructor
function TestContainer.destroy( self )
	kgp.gameLogMessage("TestContainer.destroy")
	if self.PID then
		kgp.gameLogMessage("TestContainer.destroy: remove PID " .. tostring(self.PID))
		kgp.objectDelete( self.PID )
	end
end

------------------------------
-- TestContainer static members

-- this test case
local testCase

-- Called by a test container to parse a test case message
function TestContainer.checkTestCaseMessage(message)
	return checkTestCaseMessage(message)
end

-- Called by test controller to parse a case completion message
function TestContainer.checkTestCaseCompletionMessage(message)
	return checkTestCaseCompletionMessage(message)
end

-- Called by a test container to parse the test case command and process the test case
function TestContainer.processTestCase(controllerPID, message)
	kgp.gameLogMessage("TestContainer: PID=" .. tostring(kgp.objectGetId()) .. " RECEIVED " .. tostring(message))
	local testClassName, owner, groupName, scriptName = parseContainerMessage(message)
    if testClassName==nil or owner==nil or groupName==nil or scriptName==nil then
        error("Invalid test item: " .. tostring(message));
    end

	kgp.gameLogMessage("TestContainer: PID=" .. tostring(kgp.objectGetId()) .. ", owner=" .. tostring(owner) .. ", group=" .. tostring(groupName) .. ", test=" .. tostring(scriptName) .. ", fixture=" .. tostring(testClassName))
	if type(_G[testClassName])~="table" then
		error( "invalid fixture: " .. tostring(testClassName) )
	end

	-- Create an instance of the fixture class
	testCase = Test.new{ class = _G[testClassName], name = scriptName, group = groupName, owner = owner, controllerPID = controllerPID }

	-- Load test
	testCase:setUpTestCase()

	if testCase.lastResult then
		-- Loaded successfully. Schedule for the first test.
		kgp.objectSendMessage(kgp.objectGetId(), TEST_NEXT_MSG)

	else
		-- Loading failed. Check for errors
		kgp.gameLogMessage("TestContainer: PID=" .. tostring(kgp.objectGetId()) .. " ERROR: " .. tostring(testCase.lastError))

		-- Report global test error to client
		tellOwner({type = "reportResult", class = scriptName, funcName = "<Global>", result = false, details = tostring(err)}, owner)

		-- Clean up for the entire test case
		testCase:tearDownTestCase()

		-- Notify test controller of completion
		kgp.objectSendMessage(testCase:getControllerPID(), makeTestCaseCompletionMessage(testCase:getOwner()))
	end
end

-- Called by a test container to parse the test control command
function TestContainer.processMessage(message, sender)
	if message==TEST_NEXT_MSG then

		local more, delay = testCase:nextTask()
		if more then
			-- Schedule for next
			kgp.objectSendMessage(kgp.objectGetId(), TEST_NEXT_MSG, delay or 0)

		else
			-- All done
			-- Clean up for the entire test case
			testCase:tearDownTestCase()

			kgp.gameLogMessage("TestContainer: PID=" .. tostring(kgp.objectGetId()) .. " COMPLETED")

			-- Notify test controller of completion
			kgp.objectSendMessage(testCase:getControllerPID(), makeTestCaseCompletionMessage(testCase:getOwner()))
		end
	end
end
