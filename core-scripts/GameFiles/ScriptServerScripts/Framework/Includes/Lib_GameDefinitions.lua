--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

	
local GameDefs = {}
GameDefinitions = GameDefs

----------------------------------------------------------------
-- Library Constants
----------------------------------------------------------------


GameDefs.DEFAULT_STACK_SIZE				= 100 --Default stacksize of items that don't have stacksize
GameDefs.PLACEABLE_HEALTH				= {1000, 10000, 100000, 1000000, 10000000} -- In English - {1,000   10,000   100,000   1,000,000   10,000,000}

GameDefs.DURABILITY_BY_LEVEL			= { [1] = 10, [2] = 100, [3] = 250, [4] = 500, [5] = 1000 }
GameDefs.DEFAULT_MAX_DURABILITY			= 1000	-- 1,000 hits or attacks per weapon/armor
GameDefs.DEFAULT_MIN_DURABILITY			= 100	-- Minimum random durability

GameDefs.PLAYER_INVENTORY_ARMOR_SLOTS	= 4

-- ARMOR multiplier for max HP
GameDefs.ARMOR_MULTIPLIER				= 3
-- Base damage for each armor by level
GameDefs.ARMOR_BASE						= {1, 11, 101, 1001, 10001}
-- ARMOR ranges available for each armor by level
GameDefs.ARMOR_RANGE					= {9, 89, 899, 8999, 89999}
-- Damage mitigation for armor
GameDefs.ARMOR_MITIGATION				= 0			-- PJD - armor mitigation disabled
-- Base damage for each weapon by level
GameDefs.DAMAGE_BASE					= {1, 11, 101, 1001, 10001}
-- Damage ranges available for each weapon by level
GameDefs.DAMAGE_RANGE					= {9, 89, 899, 8999, 89999}


-- Max event queue size for battle object registration
-- NOTE: Same constant exists in Controller_Battle. If you update this, update it there too
GameDefs.MAX_EVENT_QUEUE_SIZE			= 100

-- Max number of game items a single client event can contain
GameDefs.MAX_GAME_ITEM_QUEUE_SIZE		= 10

GameDefs.WORLD_COIN_UNID				= 140
GameDefs.GEM_CHEST_UNID					= 5
GameDefs.WAYPOINT_UNID					= 10 -- Unique numerical identifier for the waypoint game object

GameDefs.WAYPOINT_GLID					= 4563344
GameDefs.WAYPOINT_ANIM					= 4563345

-- NOTE: The code that uses the DEFAULT_GI_ICON is currently commented out. May need to remove this...
GameDefs.DEFAULT_GI_ICON					= "filestore10/5509627/7200348/2upbeqc.jpg" -- TODO: Define default filestore for Game Items cause this one's silly

GameDefs.PLAYER_INVENTORY_CAPACITY		= 45

GameDefs.DEFAULT_SOUND_RANGE			= 1200


GameDefs.PLACEABILITY = {
	ALLOWED		= 1,
	REJECTED	= 2,
	PENDING		= 3,
}


GameDefs.ITEM_TYPES = {
	WEAPON		= "weapon", 
	ARMOR		= "armor",
	TOOL		= "tool",
	CONSUMABLE	= "consumable", 
	PLACEABLE	= "placeable", 
	GENERIC		= "generic",
	QUEST		= "quest",
	RECIPE		= "recipe",
	SPAWNER		= "spawner",
	SHORTCUT	= "shortcut",
	CHARACTER	= "character",
	AMMO		= "ammo",
	HARVESTABLE	= "harvestable",
	FLAG		= "flag",
	BLUEPRINT	= "blueprint",
	VENDOR		= "vendor",
	GEMBOX		= "gembox",
	PATH		= "path"
}



-- Default equipment GLIDs
GameDefs.DEFAULT_MALE_GLIDS		= "4471040, 4471046, 1909"
GameDefs.DEFAULT_FEMALE_GLIDS	= "2008, 4472159, 4472154"

GameDefs.ANIMATION_SETS = {
	["Two-Handed Ranged"] = {	
		idle		= {WALK = 4305048, RUN = 4305046, STAND = 4305045, JUMP = 4305049},
		attacking	= {WALK = 4305057, RUN = 4305056, STAND = 4305055, JUMP = 4305058}
	},
	["One-Handed Ranged"] = {	
		idle		= {WALK = 4305021, RUN = 4305020, STAND = 4305019, JUMP = 4305022},
		attacking	= {WALK = 4305025, RUN = 4305024, STAND = 4305023, JUMP = 4305026}
	},
	["Two-Handed Melee"] = {	
		idle		= {WALK = 4111266, RUN = 4111265, STAND = 4111264, JUMP = 4111267},
		attacking	= {WALK = 4111270, RUN = 4111269, STAND = 4111273, JUMP = 4111271}
	},
	["One-Handed Melee"] = {	
		idle		= {WALK = 4305063, RUN = 4305062, STAND = 4305061, JUMP = 4305064},
		attacking	= {WALK = 4305067, RUN = 4305066, STAND = 4305065, JUMP = 4305068}
	},
	 -- Tool anims will be overridden by default player anims
	["Tool"] = {	
		idle		= {WALK = 4305063, RUN = 4305062, STAND = 4305061, JUMP = 4305064},
		attacking	= {WALK = 4305067, RUN = 4305066, STAND = 4305065, JUMP = 4305068}
	},
}


GameDefs.DEFAULT_ANIMS = {
	M = {WALK = 508, RUN = 438, STAND = 208, JUMP = 509},
	F = {WALK = 383, RUN = 21,  STAND = 18,  JUMP = 339}
}

GameDefs.DEFAULT_PLAYER_ANIMS = {
	DEATH	= 4154955,
	EAT		= 4695026
}


-- Various quest icon GLID states
GameDefs.QUEST_ICONS = {
	AVAILABLE 		= 4700121, -- Gold !
	IN_PROGRESS 	= 4700246, -- Silver ?
	COMPLETE 		= 4700245, -- Gold ?
	REP_AVAILABLE	= 4700244, -- Blue !
	REP_IN_PROGRESS	= 4700247  -- Blue ?
}

-- Quest icon display priority
GameDefs.QUEST_ICON_PRIORITY = {
	COMPLETE 		= 1,
	AVAILABLE 		= 3,
	REP_AVAILABLE	= 4,
	IN_PROGRESS 	= 5,
	REP_IN_PROGRESS	= 6
} -- Repeatable and regular quests have the same completion icon


GameDefs.QUEST_INDICATOR_INFO = {
	LABEL_ON	= {x = 0, y = 2.2, z = 0},
	LABEL_OFF	= {x = 0, y = 0.6, z = 0},
	ALIGNMENT	= {x = 0, y = 1, z = 0},
	ROTATION	= {x = 0, y = 0, z = 0},
	SCALE		= {x = 1, y = 1, z = 1},
	ANIM		= 4697700
}


GameDefs.ROLL_CHANCE = {
	["Common"]			= 20,
	["Rare"]			= 3,
	["Very Rare"]		= .1,
	["Extremely Rare"]	= .01,
	["Never"]			= 0
}

GameDefs.COLORS = {
	RED		= {a = 255, r = 255 ,g = 0, b = 0},
	GREEN	= {a = 255, r = 0, g = 255, b = 0},
	BLUE	= {a = 255, r = 0, g = 0, b = 255},
	WHITE	= {a = 255, r = 255, g = 255, b = 255},
}


GameDefs.EMPTY_ITEM = {	
	UNID = 0,
	properties = {
		name		= "EMPTY",
		GLID		= 0,
		itemType	= GameDefs.ITEM_TYPES.GENERIC
	},
	count = 0,
	instanceData = {}
}


GameDefs.GEM_ROLL_NAMES				= {"Topaz", "Sapphire", "Emerald", "Ruby", "Diamond"}
-- NOTE: Code that uses GEMS_REWARDS_PER_HOUR is currently commented out.
--GameDefs.GEMS_REWARDS_PER_HOUR		= 100 -- The high level desire for average rewards granted per hour. May need to become KWAS-accessible in the future?
local GEM_ROLL_CHANCES				= {70, 25, 4.4, .5, .1} -- indexed by gem type: 1=topaz, 2=sapphire, 3=emerald, 4=ruby, 5=diamond
GameDefs.GEM_ROLL_CHANCES			= GEM_ROLL_CHANCES
--local GEM_VALUES					= {1, 10, 100, 500, 1000} -- TODO-GEM: Pull these values from the web instead of hard-coding
-- NOTE: Code that uses GEMS_REWARDS_PER_HOUR is currently commented out.
--GameDefs.GEM_AVERAGE_VALUE			= (	(GEM_ROLL_CHANCES[1] * GEM_VALUES[1]) + (GEM_ROLL_CHANCES[2] * GEM_VALUES[2]) +
--												(GEM_ROLL_CHANCES[3] * GEM_VALUES[3]) + (GEM_ROLL_CHANCES[4] * GEM_VALUES[4]) +
--												(GEM_ROLL_CHANCES[5] * GEM_VALUES[5])) * 0.01

												
							

--Constants for the path and pathWaypoint object
GameDefs.PATH_GLID = 4276230
GameDefs.PATHWAYPOINT_GLID = 4700301
GameDefs.PATHWAYPOINT_UNID = 405
GameDefs.PATHWAYPOINT_GEAR_GLID = 4722055
GameDefs.RIDER_STATES = {idle = 0,
						 moving = 1,
						 interacting = 2,
						 onHold = 3}
GameDefs.PATH_RIDER_ACC_TIME = 3
GameDefs.PATH_RIDER_DEC_TIME = 5

----------------------------------------------------------------
-- Library Utilities
----------------------------------------------------------------						
GameDefs.getItemLevelMultiplier = function(iLevelMultiplier)
	
	iLevelMultiplier = iLevelMultiplier or math.random()
	iLevelMultiplier = math.ceil(iLevelMultiplier * 10000) * 0.0001
	
	return iLevelMultiplier
end

GameDefs.calculateRandomArmorRating = function(iLevel, iMultiplier)
	return GameDefs.ARMOR_BASE[iLevel] + (GameDefs.ARMOR_RANGE[iLevel] * iMultiplier)
end

GameDefs.calculateDamageRating = function(iLevel, iMultiplier)
	return GameDefs.DAMAGE_BASE[iLevel] + (GameDefs.DAMAGE_RANGE[iLevel] * iMultiplier)
end

-- Generates a random durability
GameDefs.getRandomItemDurability = function()
	return math.max( math.floor( math.random(GameDefs.DEFAULT_MAX_DURABILITY) ), GameDefs.DEFAULT_MIN_DURABILITY )
end
				

GameDefs.generateGenericSoundParams = function(iAttachID, iSoundGLID, iRange, isPlayer)
	iRange = iRange or GameDefs.DEFAULT_SOUND_RANGE
	
	local tSoundParams = {
		soundGLID		= iSoundGLID,
		pitch			= 1,
		volume			= 0.5,
		distance		= iRange,
		attenuation		= 1,
		looping			= 0,
		loopdelay 		= 0,
		innerconeangle	= 360,
		outerconeangle	= 360,
		outerconegain	= 0.5;
		{x = 0, y = 0, z = 0, rx = 1, ry = 0, rz = 1},
	}

	if isPlayer then
		tSoundParams.ID = iAttachID
	else
		tSoundParams.PID = iAttachID
	end
	
	return tSoundParams
end
