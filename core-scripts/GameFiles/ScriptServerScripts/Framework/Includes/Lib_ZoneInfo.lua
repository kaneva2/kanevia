--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_ZoneInfo.lua
-- Helper library to get zone data between linked worlds

local s_tGameConfigs --SharedData table for Game Configs

--Returns the parent's zoneId and type. If parent world or non-linked world, defaults to 0
local function getParentZoneInstanceAndType()

	s_tGameConfigs		= SharedData.new("config", false)
	local childZoneInstanceID, childZoneIndex, childZoneType = kgp.gameGetCurrentInstance() --child ZoneInstanceID, ZoneIndex, ZoneType 
	local parentZoneInstaceID, parentZoneIndex, parentZoneType = kgp.getParentZoneInfo(childZoneInstanceID, childZoneType) --parent ZoneInstanceID, ZoneIndex, ZoneType. If none, defaults to 0
	local isTemplate = kgp.gameHasDynamicLink()
	if isTemplate then
		parentZoneInstaceID = tonumber(s_tGameConfigs.DynamicZoneLinkParentId)
		parentZoneType = 6
	end
	return parentZoneInstaceID, parentZoneType
end

--Returns table of child zones. If no children, table is nil
local function getChildZones(zoneInstanceId, zoneType)
	local currentZoneID, currentZoneIndex, currentZoneType = kgp.gameGetCurrentInstance()
	local childZones = kgp.getChildZoneIds(zoneInstanceId or currentZoneID, zoneType or currentZoneType)
	return childZones
end

--Check to determine world is a Linked world or not depending if it has a parent or children
local function isLinkedZone()
	local parentID, parentZone = getParentZoneInstanceAndType()
	local childZones = getChildZones()

	if childZones or parentID ~= 0 then
		return true
	else
		return false
	end
end

ZoneInfo = {
	getParentZoneInstanceAndType = getParentZoneInstanceAndType,
	getChildZones = getChildZones,
	isLinkedZone = isLinkedZone
}