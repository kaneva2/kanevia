--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--------------------------------------------------------------
-- Lib_Class.lua
--------------------------------------------------------------
-- Create following 'keywords' in the global namespace
--   * createClass			: declare a new class
--   * createBehavior		: declare a new behavior
--   * importClass  		: import a class into global namespace
--   * useBehaviors			: use behaviors
--   * export				: export public methods
--   * import				: use exported functions from a controller
--   * service				: declare current script as a system service (controller)
--   * getServicePID		: obtain PID based on service name
--------------------------------------------------------------
-- Revisions
-- * First version																	09/2013 YC
-- * Added import/export keywords													10/2013 YC
-- * Added properties keyword														10/2013 YC
-- * Added delete and kgp_stop														10/2013 YC
-- * Added service and getServicePID												11/2013 YC
-- * Added functionality for retreiving class content from third party files		03/2016 CJW
--------------------------------------------------------------

local CLASS_SIGNATURE		= "KGP_CLASS"
local BEHAVIOR_SIGNATURE	= "KGP_BEHAVIOR"
local CLASS_PREFIX			= "Class_"
local CONTROLLER_PREFIX		= "Controller_"

--------------------------------------------------------------
-- Declare private functions
--------------------------------------------------------------
local getExportTable, exportMethod
local checkStopHook						--, delete
local getScriptPath						-- function()
local getClassPath						-- function()
local loadFileContentIntoEnvironment	-- function(sFilename, dEnvironment)
local registerBehavior					-- function(sBehaviorName, tBehavior)
local getRegisteredBehavior				-- function(sBehaviorName, bLoadIfNotFound)

--------------------------------------------------------------
-- Private variables
--------------------------------------------------------------
local s_tClassInstances = {}
local s_bIsKGPStopHooked = false
local s_iZoneIndex, s_uZoneServices



-- import a class
local function importClass(className)
	ClassHierarchies.loadClass(className)

	-- return imported class
	return _G[className]
end

-- Check if input is a class or a class instance
local function isClassOrInstance(obj)
	return type(obj) == "table" and obj._sig == CLASS_SIGNATURE
end


-- class function declare a new class. call this function at the beginning of a class script.
local function createClass(name, super, classContentFile)
	local isClassDefined = _G[name] ~= nil
	if isClassDefined then
		return
	end
	
	local thisClass
	local superClass

	Types.validateArguments{ {name, "name", "string"}, {super, "super", "string", true} }

	if super ~= nil then
		-- Use a cached version if this super class already has been loaded in this VM
		superClass = _G[super]
		if superClass == nil then
			-- Otherwise load the class into VM memory
			superClass = importClass(super)
		end
		
		if type(superClass) ~= "table" then
			error("class(): error loading super class [" .. super .. "]", 2)
		end

		if superClass._sig ~= CLASS_SIGNATURE then
			error("class(): You can only inherit from another KGP class", 2)
		end
	end

	thisClass = { _sig = CLASS_SIGNATURE, _className = name, _super = superClass }
	_G[name] = thisClass
	_G._thisClass = thisClass		-- this will be overriden by sub classes through inheritance

	thisClass.new = function( ... )
		-- Create instance table and make it inherit from the class
		local instance = { _class = thisClass }
		if thisClass._properties ~= nil then
			-- class with properties need per-instance shared storage
			rawset(instance, "_properties", thisClass._properties)
			instance._dictionary = SharedData.new()					-- TODO: double check to make sure dictionary will be disposed properly
			-- use a special lookup function to cross reference property dictionary and the instance table.
			-- Note that we have to use C functions for metamethods due to the yield-across-C-metamethod-boundary issue
			setmetatable( instance, { __index = kgp.classGetProperty, __newindex = kgp.classSetProperty } )
		else
			-- plain old class
			setmetatable( instance, { __index = _G[name] } )
		end

		-- Call constructor if defined
		if type(instance.create) == "function" then
			instance:create( ... )
		end

		-- Insert into global instances list
		s_tClassInstances[instance] = kgp.gameGetCurrentTick()

		-- Hook kgp_stop, uncomment this to have all objects get a destroy call before the server spins down.
		--checkStopHook()

		--log("new(): created instance " .. tostring(instance) .. ". Class: " .. tostring(getClassName(instance)))
		return instance
	end

	-- inherits super class or global environment
	setmetatable( thisClass, { __index = (superClass or _G) } )

	-- If a content location is specified, then load the contents of that file into the class
	if classContentFile then
		loadFileContentIntoEnvironment(classContentFile, thisClass)
	else
		-- update global environment so that all other functions /variables in the calling script use it
		setfenv( 2, thisClass )
	end
end


-- behavior function declare a new behavior. call this function at the beginning of a behavior script.
local function createBehavior(name)

	-- construct behavior
	local thisBehavior = { _sig = BEHAVIOR_SIGNATURE, _behaviorName = name }
	
	-- inherits global environment
	setmetatable(thisBehavior, { __index = _G } )

	-- update global environment so that all other functions /variables in the calling script use it
	setfenv( 2, thisBehavior )
	
	registerBehavior(name, thisBehavior)
end




-- use a list of behaviors in a class. call this function after class().
local function useBehaviors(behaviors)

	-- grab current environment (thisClass)
	local thisClass = getfenv(2)

	if not isClassOrInstance(thisClass) then
		error("useBehaviors(): must call declare class with class() function first", 2)
	end

	for _, behaviorName in ipairs(behaviors) do
		thisClass[behaviorName] = getRegisteredBehavior(behaviorName, true)
	end
end



-- return class name
local function getClassName(self, stripPrefix, prefix)
	if type(self) ~= "table" then
		return nil
	end

	if stripPrefix then
		local s, e
		if prefix ~= nil then
			s, e = string.find(self._className, prefix)					-- custom prefix
		else
			s, e = string.find(self._className, CLASS_PREFIX)			-- CLASS_
			if s ~= 1 then
				s, e = string.find(self._className, CONTROLLER_PREFIX)	-- CONTROLLER_
			end
		end
		if s == 1 and e~=nil then
			return string.sub(self._className, e+1)
		end
	end

	return self._className
end

-- declare properties
local function createProperties( propList )
	local thisClass = getfenv(2)

	-- validate context
	if not isClassOrInstance(thisClass) then
		error("createProperties(): must call declare class with class() function first", 2)
	end

	-- validate parameters
	if type(propList)~="table" then
		error("createProperties(): invalid argument 1 `propList', expected: table, got: " .. type(propList), 2)
	end

	-- empty list?
	if #propList==0 then
		return
	end

	local propertyTable = rawget(thisClass, "_properties")
	if propertyTable==nil then
		-- first make a copy of the inherited property table, if any
		propertyTable = Toolbox.shallowCopy(thisClass._properties) or {}
		rawset(thisClass, "_properties", propertyTable)
	end
	
	for index, prop in ipairs(propList) do
		if type(prop) == "string" and prop ~= "" then
			-- record property name
			propertyTable[prop] = true
		else
			error("properties(): invalid property at item #" .. index, 2)
		end
	end
end

-- export methods
local function export( specs )
	local thisClass = getfenv(2)

	-- validate context
	if not isClassOrInstance(thisClass) then
		error("export(): must call declare class with class() function first", 2)
	end

	-- validate parameters
	if type(specs) ~= "table" then
		error("export(): invalid argument 1 `specs', expected: table, got: " .. type(specs), 2)
	end

	local shortName = getClassName(thisClass, true)
	if shortName == nil or shortName=="" then
		error("export(): Unable to determine current class name", 2)
	end

	-- get export tables
	local exportTable = getExportTable( shortName )
	if exportTable._PID ~= nil and exportTable._PID ~= kgp.objectGetId() then
		error("export(): Library `" .. shortName .. "' already exported in another script: " .. tostring(exportTable._PID).." exportTable._PID["..tostring(exportTable._PID).."], kgp.objectGetId()["..tostring(kgp.objectGetId()).."]", 2)
	end
	exportTable._PID = kgp.objectGetId()

	-- process export spec
	for index, item in ipairs(specs) do
		if type(item) == "table" and item[1] ~= nil then
			-- expose exported method
			Console.Write( Console.DEBUG, "export method " .. shortName .. "." .. item[1] )
			exportMethod(thisClass, exportTable, item)
		else
			error("export(): invalid export spec at item #" .. index, 2)
		end
	end

	-- Reference shared table in this class to prevent it from getting orphaned
	rawset(thisClass, "_exports", exportTable)
end

-- use an export library
local function import( libName, namespace )
	namespace = namespace or libName

	-- Obtain controller export table
	local exportTable = getExportTable( libName )
	if exportTable._PID == nil then
		error( "import(): Export library `" .. libName .. "' not found", 2 )		-- report error if no such export library
	end

	-- generate adapters for current class
	Events.generateAdaptersByExportTable( namespace, exportTable )
end

-- register current script as a system service (controller)
local function registerAsService( serviceName )
	if s_uZoneServices == nil then
		if s_iZoneIndex == nil then
			s_iZoneIndex = kgp.gameGetCurrentZone()
		end
		s_uZoneServices = kgp.dictGenerate( "kgp.services." .. tostring(s_iZoneIndex) )
	end
	if not s_uZoneServices then
		error("Unable to obtain service scripts map for current zone", 2)
	end
	s_uZoneServices[serviceName] = kgp.objectGetId()
end

-- obtain PID for a specific system service
local function getServicePID( serviceName )
	if s_uZoneServices == nil then
		if s_iZoneIndex == nil then
			s_iZoneIndex = kgp.gameGetCurrentZone()
		end
		s_uZoneServices = kgp.dictGenerate( "kgp.services." .. tostring(s_iZoneIndex) )
	end
	if not s_uZoneServices then
		error("Unable to obtain service scripts map for current zone", 2)
	end
	return s_uZoneServices[serviceName]
end


-- Check if input is a class (not an instance)
local function isClass(obj)
	return isClassOrInstance(obj) and obj._class == nil
end

-- Check if input is an instance
local function isInstance(obj)
	return isClassOrInstance(obj) and obj._class ~= nil
end


-- Check if a class or an instance has public properties
local function hasProperties(obj)
	return type(obj._properties) == "table" and #obj._properties > 0
end


-- check if input is an instance of a specific class
local function isInstanceOf(obj, className, depth)

	depth = depth or 0

	if not isClassOrInstance(obj) then
		return false
	end

	if getClassName(obj) == className then
		return true
	end

	local MAX_CLASS_TRAVERSAL_DEPTH = 20
	if obj._super ~= nil and depth < MAX_CLASS_TRAVERSAL_DEPTH then
		return isInstanceOf(obj._super, className, depth + 1)
	end
end


-------------------------------------------------------
-- The Class Library interface
-------------------------------------------------------

Class = {
	isClassOrInstance	= isClassOrInstance,
	importClass			= importClass,
	createClass			= createClass,
	createBehavior		= createBehavior,
	useBehaviors		= useBehaviors,
	getClassName		= getClassName,
	createProperties	= createProperties,
	export				= export,
	import				= import,
	registerAsService	= registerAsService,
	getServicePID		= getServicePID,
	isClass				= isClass,
	isInstance			= isInstance,
	hasProperties		= hasProperties,
	isInstanceOf		= isInstanceOf,
}

-------------------------------------------------------
-- private functions
-------------------------------------------------------


function getRegisteredBehavior(sBehaviorName, bLoadIfNotFound)
	local tBehavior = _G._behaviors and _G._behaviors[sBehaviorName]
	
	if not tBehavior and bLoadIfNotFound == true then
		include(sBehaviorName .. ".lua")
		tBehavior = _G._behaviors and _G._behaviors[sBehaviorName]
		if not tBehavior then
			error("Could not load behavior: " .. tostring(sBehaviorName))
		end
	end
	
	return tBehavior
end

function registerBehavior(sBehaviorName, tBehavior)
	if _G._behaviors == nil then
		_G._behaviors = {}
	end
	
	_G._behaviors[sBehaviorName] = tBehavior
end



function getExportTable( libName )
	if s_iZoneIndex == nil then
		s_iZoneIndex = kgp.gameGetCurrentZone()
	end
	return SharedData.new({"KGP", "ExportTable", tostring(s_iZoneIndex), libName})
end

-- Export a methods into "export library". See import() function.
-- E.g. if Controller_Battle export a registerWeapon function, any script can call import("Battle") to gain access to it. 
-- NOTE: exported methods is called asynchonously and does not return any values.
function exportMethod( thisClass, exportTable, methodDef )
	local methodName = methodDef[1]

	-- generate event name
	local eventName = string.upper(string.gsub(methodName, "[A-Z]", "-%1"))

	-- register library function
	local method = SharedData.new()									-- debug mode: always recreate method table to allow frequent code updates
	-- local method = exportTable[methodName] or SharedData.new()	-- release mode: reuse if possible to save time and memory

	method.eventName = eventName
	if #methodDef > 1 then
		method.params = SharedData.new()
		for k, v in ipairs(methodDef) do
			if k > 1 then
				method.params[tostring(k - 1)] = v				-- should we change dictionary API to support numeric keys???
			end
		end
	end

	exportTable[methodName] = method
end

-- delete function - dispose an instance created by Class.new()
function delete( instance )
	local msg

	-- validations
	if not isInstance(instance) then
		-- not a valid instance
		msg = "delete(): ".. tostring(instance) .. " is not a valid class instance"
		log(msg)
		-- report as an error
		error(msg, 2)
		return
	end

	if s_tClassInstances[instance] == nil then
		-- not a recorded instance
		msg = "delete(): " .. tostring(instance) .. " is an unknown instance of class: " .. tostring(getClassName(instance))
		log(msg)
		-- ignore silently
		return
	end

	-- remove from global instance list
	s_tClassInstances[instance] = nil

	-- check if deleted
	if instance._deleted ~= nil then
		-- already deleted
		msg = "delete(): " .. tostring(instance) .. " is already deleted. Class: " .. tostring(getClassName(instance))
		log(msg)
		-- ignore silently
		return
	end

	-- call destroy() if available
	if type(instance.destroy) == "function" then
		msg = "delete(): " .. tostring(instance) .. " - calling destroy(). Class: " .. tostring(getClassName(instance))
		instance:destroy()
	else
		msg = "delete(): " .. tostring(instance) .. " - no destructor defined. Class: " .. tostring(getClassName(instance))
	end

	instance._deleted = true
end

-- hook kgp_stop function
function checkStopHook()

	-- Save existing handler
	local prevFunction = _G.kgp_stop

	-- Create hook function for kgp_stop
	_G.kgp_stop = function()
		Console.Write( Console.DEBUG, "kgp_stop" )
		log("kgp_stop called on script " .. tostring(kgp.objectGetId()))
				
		-- delete all instances
		local allInstances = Toolbox.getContainerKeys(s_tClassInstances)
		if allInstances then
			--[[ another casualty of "yield across C/metamethod boundary" issue. Disable sorting for now until we have a better option.
			-- sort instance list by creation timestamp in decending order
			table.sort(allInstances, function( a, b )
										 local timeA = s_tClassInstances[a] or 0
										 local timeB = s_tClassInstances[b] or 0
										 return timeA > timeB
									 end)
			--]]

			-- delete instances from newest to oldest
			for _, instance in ipairs(allInstances) do
				delete(instance)
			end
		end

		-- call existing kgp_stop handler
		if prevFunction ~= nil then
			prevFunction()
		end
	end

	s_bIsKGPStopHooked = true
end



function loadFileContentIntoEnvironment(sFilename, dEnvironment)
	--log("loadFileContentIntoEnvironment")
	local sClassDir = getClassPath()
	local sFullPath = sClassDir .. sFilename
	
	-- Load the chunk
	local fLoadedChunk, sError = loadfile(sFullPath)

	if fLoadedChunk then
		-- Set the environment of the chunk
		setfenv(fLoadedChunk, dEnvironment)
		-- Execute the chunk into the environment
		fLoadedChunk()
	else
		error(tostring(sError))
	end
end


function getScriptPath()
   local rawPath = debug.getinfo(2, "S").source
   -- Remove the initial @ character
   local refinedPath = rawPath:sub(2)
   -- Return the string up to the last / or \ characters
   return refinedPath:match(".*[\\/]")
end

function getClassPath()
	local strPath = getScriptPath()
	return strPath .. "..\\Classes\\"
end