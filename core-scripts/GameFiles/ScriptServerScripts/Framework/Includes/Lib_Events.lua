--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox 
local Types				= _G.Types
-----------------------------------------------------
-- Console definition
-----------------------------------------------------

-- Dev Console I/O
Console = { ERROR = 10, WARN = 20, INFO = 30, DEBUG = 40, TRACE = 50, ModuleOutputLevels = {} }
Console.OutputLevel = Console.INFO

-- Generate a reference to the Shared Data map
local s_uPlayers		= SharedData.new("players", false)

-- Copied From TriggerEnums.h
local s_triggerAction_scripting = 9
local s_triggerAction_scriptServer = 10

local s_triggerId_dynamic = 0 -- old 'dynamic' trigger (uses s_triggerAction_scriptServer)
local s_triggerId_client = 1 -- old 'client' trigger (uses s_triggerAction_scripting)

function Console.Write( level, msg )
	local who = "Dispatcher"
	local outputLevel = Console.OutputLevel
	if _thisClass ~= nil then
		who = Class.getClassName(_thisClass) or who
		outputLevel = Console.ModuleOutputLevels[who] or outputLevel
	end

	msg = msg or ""
	if level <= outputLevel then
		if level>=Console.INFO then
			Events.tellAllDevelopers( msg, who )	-- developers receive all outputs
		else
			Events.tellAllPlayers( msg, who )		-- visitors see errors and warnings only
		end
	end

	-- log all ERROR/WARN/INFO messages + anything else dumped in console
	if level<=outputLevel or level<=Console.INFO then
		-- log it automatically
		local strLvl = ""
		for k, v in pairs(Console) do
			if type(k)=="string" and type(v)=="number" and v==level then
				strLvl = k		-- map log level to name
				break
			end
		end
		log( string.format("[%-5s] %s", strLvl, msg) )
	end
end

-- Wellknown controllers
-- UPDATE: 11/2013: controller PIDs are no longer hard-coded. Use getServicePID() instead.
--[[
PID_EVENTSYSTEM = 1			-- Drivers: 1 - 9
PID_INPUT       = 2
PID_HDTIMER     = 6
PID_GAME        = 10		-- Core: 10-29
PID_INVENTORY   = 15
PID_BATTLE      = 30		-- Add-on modules: 30-89
PID_COMMERCE    = 40
PID_DEVTOOLS    = 90		-- UI helper (90-99)
--]]

-- Wellknown events
-- TIMER, PLAYER-ARRIVE, PLAYER-DEPART, COLLIDE, MOUSE-LEFT-CLICK, MOUSE-RIGHT-CLICk

-- Global GLIDs
GLID_SINGULARITY = 4276230

-- Limits
MAX_PENDING_TIMERS_PER_SCRIPT = 200
MAX_DYNAMIC_TRIGGERS_PER_SCRIPT = 200

-- System Timer Expiry
EVENT_DICTIONARY_REF_EXPIRY = 10		-- 10 seconds before event-initiated dictionary references expire (see Events.encode)



-----------------------------------------------------
-- Events definition
-----------------------------------------------------

Events = {}

local LocalRegistry = {}
local LocalHooks = {}
local MessageHook = nil
local NextTimerID = 1
local PendingTimerCount = 0
local DynamicTriggerCount = 0
local LocalEventList = {}
local TimerHandlers = {}
local TriggerHandlers = {}
local InitialPlayers = nil

-- Use kgp_timer for clean up of event references of SharedData-in-transit
local SharedDataEventRefCleanupHandler = nil
local SharedDataEventRefArrays = {}         -- Workaround to prevent dictionaries from being garbage collected before event delivery.
local SharedDataEventRefArraysExpTime = 0   -- Timestamp when SharedDataEventRefArrays[1] expires

-- following events are generated directly in this script
-- no need to forward to event controller.
local KGPHandlerMap = {
	-- [ eventName ]       = { KGPHandlerName,         { paramList }, bReloadPlayer, bPIDMatching }     -- in general bReloadPlayer = true only if PLAYER-ARRIVE
	["PLAYER-ARRIVE"]      = { "kgp_arrive",           { "user" }, true },
	["PLAYER-DEPART"]      = { "kgp_depart",           { "user", "name", "pos"} },
	["MOUSE-LEFT-CLICK"]   = { "kgp_objectLeftClick",  { "user", "modifiers", "clickTable", "count", "objects" }, false, true },
	["MOUSE-RIGHT-CLICK"]  = { "kgp_objectRightClick", { "user", "modifiers", "clickTable", "count", "objects" }, false, true },
	["COLLIDE"]            = { "kgp_collide",          { "user", "placementId", "objectPosition", "type"}, false, true },
	["TRIGGER"]            = { "kgp_trigger",          { "user", "triggerType", "playerPosition", "triggerPosition", "radius", "param", "triggerId" }, false, true },
	["TIMER"]              = { "kgp_timer",            { "tick" } },
	["PLAYER-LEFT-CLICK"]  = { "kgp_playerLeftClick",  { "user", "clickTable", "target" } },
	["PLAYER-RIGHT-CLICK"] = { "kgp_playerRightClick", { "user", "clickTable", "target" } },
	["MENU-BUTTON"]        = { "kgp_menuButton",       { "user", "menuName", "result", "caption", "identifier" } },
	["MENU-KEY-DOWN"]      = { "kgp_menuKeyDown",      { "user", "menuName", "key", "identifier" } },
	["MENU-KEY-UP"]        = { "kgp_menuKeyUp",        { "user", "menuName", "key", "identifier" } },
	["MENU-LIST-SELECTION"]= { "kgp_menuListSelection",{ "user", "menuName", "listSelection", "identifier" } },
	["MENU-SHOP-BUY"]      = { "kgp_menuShopBuy",      { "user", "menuName", "itemId", "itemName", "identifier" } },
	["MENU-CLOSED"]        = { "kgp_menuClosed",       { "user", "menuName", "identifier" } },
}

local function getEventType(eventName)
	if eventName:sub(1,6)=="TIMER:" then
		return "TIMER", string.sub(eventName, 7)
	elseif eventName:sub(1,8)=="TRIGGER:" then
		return "TRIGGER", string.sub(eventName, 9)
	else
		return eventName
	end
end

local function installSharedDataEventRefCleanupHandler()
    for i = 1, EVENT_DICTIONARY_REF_EXPIRY do
        SharedDataEventRefArrays[i] = {}        -- One table for each second for performance
    end
    SharedDataEventRefArraysExpTime = math.floor(kgp.scriptGetTick() / 1000) + 1  -- SharedDataEventRefArrays[1] expires next second

    SharedDataEventRefCleanupHandler = function(event)
        local time = math.floor(event.tick / 1000)
        -- Check for expiry second by second
        for i = 1, EVENT_DICTIONARY_REF_EXPIRY do
            if time >= SharedDataEventRefArraysExpTime then
                -- remove head and rollover
                table.remove(SharedDataEventRefArrays, 1)
                SharedDataEventRefArrays[EVENT_DICTIONARY_REF_EXPIRY] = {}
                SharedDataEventRefArraysExpTime = SharedDataEventRefArraysExpTime + 1
            else
                -- leave the rest
                break
            end
        end

        if time >= SharedDataEventRefArraysExpTime then
            -- Resync if we missed a lot of timer events (EVENT_DICTIONARY_REF_EXPIRY has elapsed since last timer call)
            SharedDataEventRefArraysExpTime = time + 1
        end
    end

    -- Register a kgp_timer callback
    Events.registerHandler("TIMER", SharedDataEventRefCleanupHandler)
end

function Events.getPlayer( playerID, playerName, reload )
	if playerID==nil then
		error("Events.getPlayer: playerID must not be nil", 2)
	end
	if playerName==nil then
		playerName = kgp.playerGetName(playerID)
	end
	local player = SharedData.new{ "player", playerName }
	if player.ID==nil or reload then
		player.name = playerName
		player.gender = kgp.playerGetGender(playerID)
		player.perm = kgp.playerGetPermissions(playerID)
		player.ID = playerID
		if player.gender ~= "M" and player.gender ~= "F" then
			log("ERROR DETECTED - ED-7420 : Events.getPlayer unable to aquire valid gender for player!  gender = "..tostring(player.gender)..", player.name = "..tostring(playerName)..", playerID = "..tostring(playerID))
		end
	end
	return player
end

-- scan and pre-convert SharedData objects
local function preprocTableForEvent( luaTable, depth, dictsFound )
	local temp = {}
	for k, v in pairs(luaTable) do
		if type(v)=="userdata" then
			-- special handling for SharedData object
			local name = kgp.dictGetInfo(v)
			if name~=nil then
				if dictsFound then
					table.insert(dictsFound, v)
				end
				temp[k] = { _sharedData = name }
			else
				-- other userdata is not supported in event
			end
		elseif type(v)=="table" and depth>0 then
			-- recurse down
			preprocTableForEvent( v, depth-1, dictsFound )
		end
	end
	for k, v in pairs(temp) do
		luaTable[k] = v
	end
end

-- scan and convert special table back to SharedData objects
local function postProcEventTable( luaTable, depth )
	local temp = {}
	local removal = {}
	for k, v in pairs(luaTable) do
		if type(v)=="table" and type(v._sharedData)=="string" then
			-- special handling for SharedData object
			local obj = kgp.dictGenerate(v._sharedData)
			if obj~=nil then
				temp[k] = obj
			else
				-- error
			end
		elseif type(v)=="userdata" then
			-- cjson compatibility fix: remove any userdata values coming out of json decode function. cjson uses light userdata values for json "null"s.
			removal[k] = true
		elseif type(v)=="table" and depth>0 then
			-- recurse down
			postProcEventTable( v, depth-1 )
		end
	end
	for k, v in pairs(temp) do
		luaTable[k] = v
	end
	for k, v in pairs(removal) do
		luaTable[k] = nil
	end
end

local function internalSetTimer( slimEvent, seconds, handler, obj, eventData )
	local timerID = NextTimerID
	NextTimerID = NextTimerID + 1
	local timerEvent =  "TIMER:" .. tostring(timerID)
	Events.registerHandler( timerEvent, handler, obj )

	local encoded
	if slimEvent then
		encoded = "{\"_type\":\"" .. timerEvent .. "\"}"
	else
		if eventData==nil then
			eventData = {}
		end
		eventData._type = timerEvent
		eventData._sender = kgp.objectGetId()
		eventData._senderType = "OBJECT"

		encoded = Events.encode(eventData)
	end

	kgp.objectSendMessage( kgp.objectGetId(), encoded, seconds*1000 )
	TimerHandlers[timerID] = { func = handler, obj = obj }
	return timerID
end

function Events.encode( luaTable )
	-- validate input
	Types.validateArguments{ {luaTable, "luaTable", "table"} }

    -- install shared data event reference expiry handler if not already
    if SharedDataEventRefCleanupHandler == nil then
        installSharedDataEventRefCleanupHandler()
    end

	-- scan and pre-convert SharedData objects (up to two levels down from top)
	preprocTableForEvent( luaTable, 2, SharedDataEventRefArrays[EVENT_DICTIONARY_REF_EXPIRY] )

	local encoded, err = cjson.encode(luaTable)
	if encoded==nil then
		log("JSON encode failed (" .. tostring(err) .. ")" )
	end
	return encoded
end

function Events.decode( encoded )
	-- quickly validate JSON syntax
	local bracketLeft = string.sub(encoded, 1, 1)
	local bracketRight = string.sub(encoded, -1, -1)
	if	(bracketLeft~="{" or bracketRight~="}") and
		(bracketLeft~="[" or bracketRight~="]") then
		-- ignore this error for now. logging only.
		Console.Write( Console.ERROR, "Events.decode: input is not a valid JSON string. See log for more detail" )
		log("JSON sanity check failed: \"" .. tostring(encoded) .. "\"" )
		return nil
	end

	-- decode
	local event, err = cjson.decode(encoded)
	if event==nil then
		Console.Write( Console.ERROR, "Events.decode: result is nil. See log for more detail" )
		log("JSON decode failed (" .. tostring(err) .. "): \"" .. tostring(encoded) .. "\"" )
		return nil
	end

	-- scan and convert back SharedData objects
	postProcEventTable( event, 5 )

	if event.player==nil and event._senderType=="PLAYER" and event._sender~=nil then
		event.player = Events.getPlayer(event._sender)
	end

	-- DEBUG ONLY - TO BE REMOVED
	if type(event.player)=="table" and event.player.gender==nil then
		Console.Write( Console.ERROR, "Invalid player table" )
	end
	-- END DEBUG ONLY

	return event
end

-- unpack event table into a list of values based on key list
function Events.unpack( event, keyList )
	event = event or {}
	if type(event)~="table" then
		error("Events.unpack: invalid parameter #1 `event', expected: table, got: " .. type(event), 2)
	end

	keyList = keyList or {}
	if type(keyList)~="table" and type(keyList)~="userdata" then
		error("Events.unpack: invalid parameter #2 `keyList', expected: table or userdata, got: " .. type(keyList), 2)
	end

	local getKey
	if type(keyList)=="userdata" then
		getKey = function(i) return keyList[tostring(i)] end
	else
		getKey = function(i) return keyList[i] end
	end

	local result = {}
	local index = 1

	while getKey(index) do
		result[index] = event[getKey(index)]		-- use square bracket to allow nil values in the result table
		index = index + 1
	end

	return unpack(result)
end

function Events.tellAllPlayers( text, who )
	Events.sendEvent( "TELL-ALL", { text=text, who=who } )
end

function Events.tellAllDevelopers( text, who )
	Events.sendEvent( "TELL-DEVS", { text=text, who=who } )
end

function Events.addLocalEvents( eventList )
	for _, eventType in ipairs(eventList) do
		LocalEventList[eventType] = true
	end
end

function Events.isLocalEvent( eventName )
	local eventType = getEventType(eventName)
	return LocalEventList[eventType]~=nil;
end

function Events.registerHandler( eventName, handler, obj )

	-- install message hook if not already
	Events.checkMessageHook()

	-- validate arguments
	Types.validateArguments{ {eventName, "eventName", "string"}, {handler, "handler", "function"}, {obj, "obj", "table", true} }

	-- initialize handler table for the specific event if not already
	if LocalRegistry[eventName]==nil then
		-- build registry with weak tables
		LocalRegistry[eventName] = setmetatable({}, {__mode="k"})
	end

	if obj==nil then
		-- global handlers (not associated with an instance variable)
		obj = "global"
	end

	-- initialize handler list for the specific object if not already
	if LocalRegistry[eventName][obj]==nil then
		LocalRegistry[eventName][obj] = {}
	end

	-- record event handler
	LocalRegistry[eventName][obj][handler] = true

	if Events.isLocalEvent( eventName ) then
		-- check and install local event hooks if necessary
		Events.checkEventHooks(eventName)
	else
		-- otherwise register handler with central event system
		Console.Write( Console.DEBUG, "registerHandler: " .. eventName .. " with central dispatcher" )
		Events.sendEvent( "EVENT-SUB", { event=eventName } )
	end
end

function Events.unregisterHandler( eventName, handler, obj )
	-- validate arguments
	Types.validateArguments{ {eventName, "eventName", "string"}, {handler, "handler", "function"}, {obj, "obj", "table", true} }

	if obj==nil then
		-- global handlers (not associated with an instance variable)
		obj = "global"
	end

	-- remove handler and cleanup
	if LocalRegistry[eventName]~=nil and LocalRegistry[eventName][obj]~=nil then
		LocalRegistry[eventName][obj][handler] = nil

		if Toolbox.isContainerEmpty(LocalRegistry[eventName][obj]) then

			LocalRegistry[eventName][obj] = nil
			if Toolbox.isContainerEmpty(LocalRegistry[eventName]) then
				LocalRegistry[eventName] = nil

				-- unsubscribe from global dispatcher since we don't have any listeners locally
				Events.sendEvent( "EVENT-UNSUB", { event=eventName } )
			end
		end
	end
end

function Events.setTimer( seconds, handler, obj, eventData )
	-- validate arguments
	Types.validateArguments{ {seconds, "seconds", "number"}, {handler, "handler", "function"}, {obj, "obj", "table", true} }

	-- Enforce system limit on outstanding timers
	if PendingTimerCount >= MAX_PENDING_TIMERS_PER_SCRIPT then
		error("setTimer: too many pending timers (" .. tostring(PendingTimerCount) .. ")", 2)
	end

	Console.Write( Console.DEBUG, "Event.setTimer " .. tostring(seconds) .. " seconds" )
	local timerID = internalSetTimer( false, seconds, handler, obj, eventData )

	PendingTimerCount = PendingTimerCount + 1
	return timerID
end

function Events.cancelTimer( timerID )
	-- validate arguments
	Types.validateArguments{ {timerID, "timerID", "number"} }

	Console.Write( Console.DEBUG, "Event.cancelTimer " .. tostring(timerID) )
	if TimerHandlers[timerID]~=nil then
		local timerEvent = "TIMER:" .. tostring(timerID)
		Events.unregisterHandler( timerEvent, TimerHandlers[timerID].func, TimerHandlers[timerID].obj )
		TimerHandlers[timerID] = nil
		PendingTimerCount = PendingTimerCount - 1
	end
end

-- Constants for trigger APIs
kgp.PrimitiveType = { Sphere = 0, Cuboid = 1,  Cylinder = 2, RectangularFrustum = 3, CircularFrustum = 4 }
kgp.Axis = { X = 0, Y = 1, Z = 2 }

local function makeTriggerVolume(volumeOrRadius)
    if type(volumeOrRadius) == "number" then
        -- Trigger V1 with radius
        return { type = kgp.PrimitiveType.Sphere, radius = volumeOrRadius }
    else
        -- Trigger V2 with volume
        return volumeOrRadius
    end
end

function Events.setTrigger( volumeOrRadius, handler, obj )
    local triggerVolume = makeTriggerVolume(volumeOrRadius)

	-- validate arguments
	Types.validateArguments{ {triggerVolume, "volume", "table"}, {handler, "handler", "function"}, {obj, "obj", "table", true} }

	local pid = obj.PID or kgp.objectGetId()
	Console.Write( Console.DEBUG, "Event.setTrigger for " .. tostring(pid) )

	-- remove existing if already set
	Events.removeTrigger()

	kgp.objectSetTriggerV2( pid, s_triggerId_dynamic, s_triggerAction_scriptServer, triggerVolume, pid )
	Events.registerHandler( "TRIGGER", handler, obj )
	TriggerHandlers[pid] = { func = handler, obj = obj }
	return pid
end

function Events.setTriggerOnPosition( x, y, z, volumeOrRadius, handler, obj )
    local triggerVolume = makeTriggerVolume(volumeOrRadius)

	-- validate arguments
	Types.validateArguments{ {x, "x", "number"}, {y, "y", "number"}, {z, "z", "number"}, {triggerVolume, "volume", "table"}, {handler, "handler", "function"}, {obj, "obj", "table", true} }

	-- Enforce system limit on dynamic triggers
	if DynamicTriggerCount >= MAX_DYNAMIC_TRIGGERS_PER_SCRIPT then
		error("setTriggerOnPosition: too many dynamic triggers (" .. tostring(DynamicTriggerCount) .. ")", 2)
	end

	local pid = kgp.objectGenerate( GLID_SINGULARITY, x, y, z, 0, 0, 1 )
	if pid==nil then
		error("Events.setTriggerOnPosition: error creating place holder for the new trigger", 2)
	end
	Console.Write( Console.DEBUG, "Event.setTriggerOnPosition for " .. tostring(pid) )

	kgp.objectSetTriggerV2( pid, s_triggerId_dynamic, s_triggerAction_scriptServer, triggerVolume, pid )
	kgp.objectMapEvents( pid )
	local triggerName = "TRIGGER:" .. tostring(pid)
	Events.registerHandler( triggerName, handler, obj )
	TriggerHandlers[pid] = { name = triggerName, func = handler, obj = obj }
	DynamicTriggerCount = DynamicTriggerCount + 1
	return pid
end

function Events.removeTrigger( triggerID )
	-- validate arguments
	Types.validateArguments{ {triggerID, "triggerID", "number", true} }

	local pid = triggerID or kgp.objectGetId()
	if TriggerHandlers[pid]~=nil then

		Console.Write( Console.DEBUG, "Event.removeTrigger for " .. tostring(pid) )
		Events.unregisterHandler( TriggerHandlers[pid].name or "TRIGGER", TriggerHandlers[pid].func, TriggerHandlers[pid].obj )
		kgp.objectRemoveTriggerV2( pid, s_triggerId_dynamic )

		if TriggerHandlers[pid] and TriggerHandlers[pid].name then
			-- dynamically generated: remove now
			kgp.objectDelete(pid)
			DynamicTriggerCount = DynamicTriggerCount - 1
		end

		TriggerHandlers[pid] = nil
	end
end

function Events.setClientTrigger( volumeOrRadius, obj )
    local triggerVolume = makeTriggerVolume(volumeOrRadius)

	-- validate arguments
   	Types.validateArguments{ {triggerVolume, "volume", "table"}, {obj, "obj", "table", true} }

	local pid = obj.PID or kgp.objectGetId()
	Console.Write( Console.DEBUG, "Event.setClientTrigger for " .. tostring(pid) )

	-- remove existing if already set
	Events.removeClientTrigger()

	kgp.objectSetTriggerV2( pid, s_triggerId_client, s_triggerAction_scripting, triggerVolume, pid )
	return pid
end

function Events.removeClientTrigger( triggerID )
	-- validate arguments
	Types.validateArguments{ {triggerID, "triggerID", "number", true} }

	local pid = triggerID or kgp.objectGetId()
	kgp.objectRemoveTriggerV2( pid, s_triggerId_client )
end

function Events.sendEvent( eventName, eventData, pid )
	if eventData==nil then
		eventData = {}
	end
	eventData._type = eventName
	eventData._sender = kgp.objectGetId()
	eventData._senderType = "OBJECT"
	if PID_EVENTSYSTEM==nil then
		PID_EVENTSYSTEM = Class.getServicePID("Controller_Events")
	end
	kgp.objectSendMessage( pid or PID_EVENTSYSTEM or 0, Events.encode(eventData) )
end

function Events.sendBroadcastEvent( eventName, eventData, inCurrentZone, toLinkedZones )
	if inCurrentZone == nil then inCurrentZone = false end
	if toLinkedZones == nil then toLinkedZones = true end
	if eventData==nil then eventData = {} end
	
	eventData._type = eventName
	eventData._sender = kgp.objectGetId()
	eventData._senderType = "OBJECT"
	if PID_EVENTSYSTEM==nil then
		PID_EVENTSYSTEM = Class.getServicePID("Controller_Events")
	end
	kgp.scriptBroadcastMessage( Events.encode(eventData), inCurrentZone, toLinkedZones )
end

function Events.sendClientEvent( eventName, eventData, playerID )
	if eventData==nil then
		eventData = {}
	end
	
	eventData._type = eventName
	eventData._sender = kgp.objectGetId()
	eventData._senderType = "OBJECT"
	
	if Environment.isRunningInDebugMode() then
		local hasUserdata = Toolbox.containsUserdata(eventData)
		if hasUserdata then
			local strStack = Debug.printStack(true)
			error("Trying to send userdata to client! This will most definitely cause issues!!!!!" .. strStack)
		end
	end
	
	-- If a playerID was passed in, use it
	if playerID then		
		kgp.playerSendEvent(playerID, eventName, Events.encode(eventData))
		
	-- Otherwise, send this event to every player in the zone
	else
		-- Find every player in the zone
		for sName, uPlayer in pairs(s_uPlayers) do
			if uPlayer and uPlayer.inZone then
				kgp.playerSendEvent(uPlayer.ID, eventName, Events.encode(eventData))
			end
		end
	end
end

function Events.dispatchEventToLocalListeners( event, PIDMatching )
	-- Console.Write( Console.INFO, "dispatchEventToLocalListeners: event._type=" .. event._type )
	local eventHandlerLists = LocalRegistry[event._type]

	if eventHandlerLists~=nil then

		-- BEGIN WORKAROUND for missed kgp_arrive calls
		-- Fix player table for any missing data that is critical to all scripts
		if event.player~=nil then
			local playerID = event.player.ID
			if playerID~=nil then
				if event.player.position==nil then
					local x, y, z, rx, ry, rz = kgp.playerGetLocation( playerID )
					event.player.position = { x = x or 0, y = y or 0, z = z or 0, rx = rx or 0, ry = ry or 0, rz = rz or 1 }
				end
			end
		end

		-- Filter faux PLAYER-ARRIVE events to prevent duplicates
		if event._type=="PLAYER-ARRIVE" and InitialPlayers~=nil then
			local playerID = event.player and event.player.ID
			if playerID then
				if InitialPlayers[playerID]~=nil then					-- One of the "initial" players we might have missed
					if InitialPlayers[playerID]==true then
						InitialPlayers[playerID] = false				-- This is to ensure one PLAYER-ARRIVE event per player
					else
						-- PLAYER-ARRIVE already received, ignore duplicate
						-- log("[" .. tostring(kgp.objectGetId()) .. "] Drop duplicated PLAYER-ARRIVE (faux=" .. tostring(event.faux or false) .. ") for " .. tostring(kgp.playerGetName(playerID)) .. " (" .. tostring(playerID) .. ")")
						return
					end
				end
			else
				log("[" .. tostring(kgp.objectGetId()) .. "] Received bad PLAYER-ARRIVE, event.player=" .. tostring(event.player))
			end
		end
		-- END WORKAROUND

		for obj, handlers in pairs(eventHandlerLists) do
			for handler, _ in pairs(handlers) do
				if obj == "global" then
					-- call a global function
					handler( event )
				elseif event._class == nil or event._class == Class.getClassName(obj) then
					-- call a class instance function
					if PIDMatching and obj.PID ~= nil then
						-- matching PID from event and obj.PID
						local eventPID
						-- Hard-coded work-arounds, to be reworked
						if event._type == "MOUSE-LEFT-CLICK" or event._type == "MOUSE-RIGHT-CLICK" then
							eventPID = event.objects and event.objects[1]
						elseif event._type == "COLLIDE" then
							eventPID = event.placementId
						elseif event._type == "TRIGGER" then
							eventPID = event.param
						end
						
						if eventPID == nil or eventPID == obj.PID then
							handler( obj, event )
						end
					else
						handler( obj, event )
					end
				end
			end
		end
	end

	-- Dispose memory for timer events and handlers
	local eventType, suffix = getEventType(event._type)
	if eventType=="TIMER" and suffix~=nil and suffix~="" then
		local timerID = tonumber(suffix)
		local classInfo
		if _thisClass~=nil then
			classInfo = "Class: " .. Class.getClassName(_thisClass)
		end
		if timerID==nil then
			Console.Write( Console.ERROR, "Unable to remove expired timer `" .. event._type .. "', timerID is nil. " .. (classInfo or "") )
		else
			-- log("Remove expired timer `" .. event._type .. "', timerID=" .. timerID .. " " .. (classInfo or ""))
			Events.cancelTimer(timerID)
		end
	end
end

-- Hooks!!!
function Events.checkEventHooks(eventName)

	local eventType = getEventType(eventName)
	if LocalHooks[eventType]~=nil then
		-- already hooked
		return
	end

	-- TODO: unhook upon removal

	-- Lookup kgp_xxx handler function name by event type
	local kgpHandlerInfo = KGPHandlerMap[eventType]
	if kgpHandlerInfo and kgpHandlerInfo[1] then

		local kgpHandlerName = kgpHandlerInfo[1]
		local params = kgpHandlerInfo[2]
		local reloadPlayer = kgpHandlerInfo[3]
		local PIDMatching = kgpHandlerInfo[4]

		-- Save existing handler
		local prevFunction = _G[kgpHandlerName]
		local hookFunction

		-- Create hook function
		if kgpHandlerName=="kgp_trigger" then
			-- special handling for kgp_trigger
			hookFunction = function( user, triggertype, playerposition, triggerposition, radius, param, triggerId )
				Console.Write( Console.DEBUG, "Event: " .. eventType )
				local triggerEvent = "TRIGGER"
				if TriggerHandlers[param] and TriggerHandlers[param].name then
					triggerEvent = TriggerHandlers[param].name
				end

				Events.dispatchEventToLocalListeners({ _type=triggerEvent, player=Events.getPlayer(user), triggerType=triggertype, playerPosition=playerposition, triggerPosition=triggerposition, radius=radius, param=param, triggerId=triggerId }, true)
				if prevFunction~=nil then
					prevFunction( user, triggertype, playerposition, triggerposition, radius, param, triggerId )
				end
			end

		elseif kgpHandlerName=="kgp_depart" then
			-- special handling for kgp_depart
			hookFunction = function( user, name, pos )
				Console.Write( Console.DEBUG, "Event: " .. eventType )
				-- Remove from InitialPlayers list to allow future PLAYER-ARRIVE events to fire for this player
				if InitialPlayers~=nil then
					InitialPlayers[user] = nil
				end
				-- for kgp_depart, playerGetName might fail. use the new player name parameter instead
				Events.dispatchEventToLocalListeners{ _type=eventType, player=Events.getPlayer(user, name), pos = pos }
				if prevFunction~=nil then
					prevFunction( user, name )
				end
			end

		else
			-- generic hook
			hookFunction = function( ... )
				Console.Write( Console.DEBUG, "Event: " .. eventType )
				local event = Toolbox.associateKeysToValues( params, {...} )
				event._type = eventType
				if event.user~=nil then
					event.player = Events.getPlayer(event.user, nil, reloadPlayer)
					event.user = nil
				end
				Events.dispatchEventToLocalListeners( event, PIDMatching )
				if prevFunction~=nil then
					prevFunction( ... )
				end
			end

			-- BEGIN WORKAROUND for missed kgp_arrive calls
			if kgpHandlerName=="kgp_arrive" and InitialPlayers==nil then
				-- get a list of existing players and send faux PLAYER-ARRIVE events for them
				local players = kgp.gameGetPlayersInZone()
				if type(players)=="table" then
					InitialPlayers = Toolbox.convertContainerToSet(players)
					for _, playerID in ipairs(players) do
						log("[" .. kgp.objectGetId() .. "] Send faux PLAYER-ARRIVE event for " .. tostring(kgp.playerGetName(playerID)) .. " (" .. tostring(playerID) .. ")")
						Events.sendEvent("PLAYER-ARRIVE", {player=Events.getPlayer(playerID, nil, true), faux=true}, kgp.objectGetId())
					end
				end
			end
			-- END WORKAROUND
		end

		-- Hook kgp event handler
		LocalHooks[eventType] = hookFunction
		Console.Write( Console.DEBUG, kgpHandlerName .. " hook installed on " .. tostring(kgp.objectGetId()) )
		_G[kgpHandlerName] = LocalHooks[eventType]
	end
end

function Events.checkMessageHook()
	if MessageHook~=nil then
		-- already hooked
		return
	end

	-- install hook for kgp_message
	local prevFunction = _G.kgp_message
	MessageHook = function( message, sourceId )
		Console.Write( Console.DEBUG, "kgp_message called on " .. tostring(kgp.objectGetId) )

		event = Events.decode( message )
		if event~=nil then
			Events.dispatchEventToLocalListeners( event )
		end

		if prevFunction~=nil then
			prevFunction( message, sourceId )
		end
	end

	Console.Write( Console.DEBUG, "kgp_message hook installed on " .. tostring(kgp.objectGetId()) )
	_G.kgp_message = MessageHook
end

-- Event adapters
function Events.generateAdapters( lib, pid, events )
	_G[lib] = {}
	for funcName, def in pairs(events) do
		local eventDef = def
		_G[lib][funcName] = function( ... )
			local arg = {...}
			local eventName = eventDef[1]
			local eventData = {}
			for i=2,#eventDef do
				eventData[eventDef[i]] = arg[i-1]
			end
			Console.Write( Console.DEBUG, lib .. "." .. funcName .. " -> " .. eventName )
			Events.sendEvent( eventName, eventData, pid )
		end
	end
end

-- Event adapters by export table
function Events.generateAdaptersByExportTable( lib, exportTable )
	_G[lib] = _G[lib] or {}
	for methodName, exportDef in pairs(exportTable) do
		if type(exportDef)=="userdata" then
			Console.Write( Console.DEBUG, "Import function " .. lib .. "." .. methodName )
			_G[lib][methodName] = function( ... )
				local arg = {...}
				local eventName = exportDef.eventName
				local eventData = {}
				local i = 1
				while exportDef.params and exportDef.params[tostring(i)]~=nil do					-- dictionary objects take string keys only
					eventData[exportDef.params[tostring(i)]] = arg[i]
					i = i + 1
				end
				Console.Write( Console.DEBUG, lib .. "." .. methodName .. " -> " .. eventName )
				Events.sendEvent( eventName, eventData, exportTable._PID )
			end
		end
	end
end

-- Register event handlers by export table
function Events.registerHandlersByExportTable( instance, exportTable )
	-- validate parameters
	if type(exportTable)~="userdata" then
		error("exportMethod: invalid argument 1 `exportTable', expected: userdata, got: " .. type(methodName))
	end

	-- loop through export table
	for methodName, exportDef in pairs(exportTable) do
		if type(exportDef)=="userdata" then
			Console.Write( Console.DEBUG, "Register handler `" .. methodName .. "' for " .. tostring(exportDef.eventName) )

			-- validate
			if type(instance[methodName])=="function" then
				local eventName = exportDef.eventName
				local params = exportDef.params

				-- register handler
				Events.registerHandler(eventName, function(event)
													  instance[methodName](instance, Events.unpack(event, params))
												  end)
			else
				-- otherwise report a warning
				Console.Write( Console.WARN, "Method `" .. methodName .. "' not found in `" .. Class.getClassName(instance) )
			end
		end
	end
end

-- debug
function Events.dumpHandlers()
	log("***************************************************************")
	for eventName, eventHandlerLists in pairs(LocalRegistry) do
		if eventHandlerLists~=nil then
			for obj, handlers in pairs(eventHandlerLists) do
				for handler, _ in pairs(handlers) do
					if obj=="global" then
						-- call a global function
						log("EventHandler[" .. eventName .. "][global] = " .. tostring(handler))
					else
						-- call a class instance function
						log("EventHandler[" .. eventName .. "][" .. tostring(Class.getClassName(obj)) .. ":" .. tostring(obj) .. "] = " .. tostring(handler))
					end
				end
			end
		end
	end
	log("===============================================================")
end

---[[
-- TODO: figure out why closure doesn't work without these functions predefined
dummy = function() end
kgp_message=dummy
kgp_arrive=dummy
kgp_depart=dummy
kgp_objectLeftClick=dummy
kgp_objectRightClick=dummy
kgp_collide=dummy
kgp_trigger=dummy
kgp_timer=dummy
kgp_playerLeftClick = dummy
kgp_playerRightClick = dummy
kgp_menuButton = dummy
kgp_menuKeyDown = dummy
kgp_menuKeyUp = dummy
kgp_menuListSelection = dummy
kgp_menuShopBuy = dummy
kgp_menuClosed = dummy
kgp_result = dummy
kgp_elapsed = dummy
--]]

Events.addLocalEvents( Toolbox.getContainerKeys(KGPHandlerMap) or {} )