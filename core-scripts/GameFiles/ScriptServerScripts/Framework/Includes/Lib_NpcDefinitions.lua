--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lib_NpcDefinitions.lua

local NpcDefs = {}
NpcDefinitions = NpcDefs

NpcDefs.REP_LEVELS = {
	"Foe",
	"Acquaintance",
	"Friend",
	"Close Friend",
	"Best Friend"
	-- "Follower",
	-- "Spouse"
}

NpcDefs.SPECIAL_LEVELS = {
	"Follower",
	"Spouse",
}

NpcDefs.MAX_REP_LEVEL = 5

NpcDefs.REP_ABANDON_LEVEL = NpcDefs.REP_LEVELS[2]

NpcDefs.REP_SCALES = {

	["Easy"] = {
		[NpcDefs.REP_LEVELS[1]] = -1,
		[NpcDefs.REP_LEVELS[2]] = 0,
		[NpcDefs.REP_LEVELS[3]] = 500,
		[NpcDefs.REP_LEVELS[4]] = 1000,
		[NpcDefs.REP_LEVELS[5]] = 2500,
		["Shun"] = 100
	},
	["Normal"] = {
		[NpcDefs.REP_LEVELS[1]] = -1,
		[NpcDefs.REP_LEVELS[2]] = 0,
		[NpcDefs.REP_LEVELS[3]] = 1000,
		[NpcDefs.REP_LEVELS[4]] = 2500,
		[NpcDefs.REP_LEVELS[5]] = 5000,
		["Shun"] = 250
	},
	["Hard"] = {
		[NpcDefs.REP_LEVELS[1]] = -1,
		[NpcDefs.REP_LEVELS[2]] = 0,
		[NpcDefs.REP_LEVELS[3]] = 2500,
		[NpcDefs.REP_LEVELS[4]] = 5000,
		[NpcDefs.REP_LEVELS[5]] = 10000,
		["Shun"] = 500
	},
}

NpcDefs.MAX_FOLLOWERS = 10