--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Includes
include("Lib_SaveLoad.lua")

-- Class_Collectible.lua
Class.createClass("Class_ClaimFlag", "Class_Base")


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local SaveLoad			= _G.SaveLoad
local Events			= _G.Events

-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local DEFAULT_FLAG_OBJECT = "Land Claim Flag"
local DEFAULT_FLAG_RADIUS = 250
local DEFAULT_FLAG_HEIGHT = 50
local DEFAULT_FLAG_WIDTH = 50
local DEFAULT_FLAG_DEPTH = 50

local DAYS_PER_WEEK = 7
local DAYS_PER_MONTH = 30

local SEC_PER_MIN = 60
local SEC_PER_HOUR = SEC_PER_MIN * 60
local SEC_PER_DAY = SEC_PER_HOUR * 24
local SEC_PER_WEEK = DAYS_PER_WEEK * SEC_PER_DAY
local SEC_PER_MONTH = 2629743
local SEC_PER_YEAR = 31556926

local MILLISEC_PER_SEC = 1000

local FEE_CHECK_INTERVAL = SEC_PER_HOUR * MILLISEC_PER_SEC -- 3 * 1000 -- How often to check if the fee is almost due/past due (in milliseconds)
local DEFAULT_FEE_INTERVAL = 2 * DAYS_PER_WEEK * SEC_PER_DAY -- Default payment period for the maintenance fee
local FEE_ALERT_DAYS = 3 -- Days before fee due date during which the fee is considered "almost due"
local FEE_GRACE_PERIOD_DEL_OBJ = 1 * SEC_PER_DAY -- How long PAST the due date until objects are deleted
local FEE_GRACE_PERIOD_DEL_SELF = 2 * SEC_PER_DAY -- How long PAST the due date until the flag is deleted

local BOUND_CHK_BUFFER_Y = 1.5 -- Amount of leeway for checks BELOW the Y value (accounts for slight slopes and the fact that the player avatar is technically slightly beneath the ground)

local BUILD_FLAG_KEY = "DNC_claimFlag"

s_type = "claimFlag"

--------------------
-- CORE FUNCTIONS --
--------------------

function create(self, input)
	input = input or {}
	_super.create(self, input)

	self.feeInterval = DEFAULT_FEE_INTERVAL
	if input.feeInterval then
		self.feeInterval = (input.feeInterval * DAYS_PER_WEEK * SEC_PER_DAY) -- Convert from weeks to seconds
	end
	self.feesEnabled = input.feesEnabled or false

	-- Load current state
	self.flagInit = false
	self:loadFlagState("default")

	-- Intitialize variables
	self.name = input.name or DEFAULT_FLAG_OBJECT
	self.flagType = "claimFlag"
	self.enabled = "true"
	self.height = input.height or DEFAULT_FLAG_HEIGHT
	self.width = input.width or DEFAULT_FLAG_WIDTH
	self.depth = input.depth or DEFAULT_FLAG_DEPTH
	self.radius = input.radius or DEFAULT_FLAG_RADIUS
	self.noDestruct = input.noDestruct or "false"
	self.feeCheckTimer = FEE_CHECK_INTERVAL
	self.tick = 0

	-- self.feeAlert = false
	-- self.feeWarn = false
	self.objectsDeleted = false
	self.deathPending = false
	
	self:setCollision(false)

	-- Register event handlers
	Events.registerHandler("TIMER", self.onTimer, self)
	Events.registerHandler("FRAMEWORK_DYNAMIC_OBJECT_SAVE_SERVER", self.onDynamicObjectSave, self)
	Events.registerHandler("REQUEST_FLAG_GROUP_INFO", self.requestFlagGroupInfo, self)
	Events.registerHandler("REQUEST_ALL_FLAGS", self.requestAllFlags, self)
	Events.registerHandler("SAVE_FLAG_GROUP_INFO", self.saveFlagGroupInfo, self)
	Events.registerHandler("FLAG_FEE_PAYMENT", self.onFeePayment, self)
	Events.registerHandler("PLAYER-ARRIVE", self.onPlayerArrive, self) -- Register for arrive events
	Events.registerHandler("FRAMEWORK_CHECK_FLAG_STATE", self.checkFeePaymentStateHandler, self)

	-- self.flagTrigger = Events.setTrigger(self.radius, self.onTrigger, self) -- Needs new math

	kgp.gameItemSetPosRot(self.PID, self.position.x, self.position.y, self.position.z, 0, self.position.ry, 1)
	-- A box-shaped trigger with (0, 0, 0) at origin of the parent object
	local triggerMin = {x = -self.depth/2, y = -BOUND_CHK_BUFFER_Y, z = -self.width/2}
	local triggerMax = {x = self.depth/2, y = self.height, z = self.width/2}
	self.flagTrigger = Events.setTrigger({type = kgp.PrimitiveType.Cuboid, min = triggerMin, max = triggerMax }, self.onTrigger, self)
	
	self:registerSystemFlag({})
end

function destroy(self)

	self:saveFlagState("default") -- Save new values to DB
	
	_super.destroy(self)

	Events.sendEvent("UNREGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "flag", PID = self.PID})
	
	Events.unregisterHandler("TIMER", self.onTimer, self)
	Events.unregisterHandler("FRAMEWORK_DYNAMIC_OBJECT_SAVE_SERVER", self.onDynamicObjectSave, self)
	Events.unregisterHandler("REQUEST_FLAG_GROUP_INFO", self.requestFlagGroupInfo, self)
	Events.unregisterHandler("SAVE_FLAG_GROUP_INFO", self.saveFlagGroupInfo, self)
	Events.unregisterHandler("FLAG_FEE_PAYMENT", self.onFeePayment, self)
	Events.unregisterHandler("PLAYER-ARRIVE", self.onPlayerArrive, self)
	Events.unregisterHandler("FRAMEWORK_CHECK_FLAG_STATE", self.checkFeePaymentStateHandler, self)

	
	if self.flagTrigger then
		Events.removeTrigger(self.flagTrigger)
	end
	
	-- Tell HUD to hide flag icon when destroyed
	Events.sendClientEvent("FRAMEWORK_CLAIM_FLAG_STATUS_UPDATE", {buildFlag = false, flagPID = self.PID})
end

--------------------
-- EVENT HANDLERS --
--------------------

-- Timer handler
function onTimer(self, event)
	self.onServerTick(self, event)
end

-- Wrapper for onTimer
function onServerTick(self, event)
	if self.tick == 0 then self.tick = event.tick end
	local deltaTick = event.tick - self.tick
	self:updateFeePaymentTimer(deltaTick)
	self.tick = event.tick
end

function onTrigger(self, event)
	local owner = false
	if event.player and (event.player.mode ~= "God") then
		if event.player.name==self.owner then
			owner = true
		end
	end
	local flag = {}
	flag.PID = self.PID
	flag.flagType = self.flagType
	flag.enabled = self.enabled
	flag.owner = owner
	flag.playername = self.owner
	flag.noDestruct = self.noDestruct
	Events.sendEvent("SYSTEM_FLAG_TRIGGER", {player = event.player, triggerType = event.triggerType, flagInfo = flag})
end 

function registerSystemFlag(self, event)
	local flagInfo = {}
	flagInfo.PID = self.PID
	flagInfo.position = {x = self.position.x, y = self.position.y, z = self.position.z}
	flagInfo.flagType = self.flagType
	flagInfo.enabled = self.enabled
	flagInfo.height = self.height
	flagInfo.width = self.width
	flagInfo.depth = self.depth
	flagInfo.radius = self.radius
	flagInfo.playername = self.owner
	flagInfo.noDestruct = self.noDestruct
	if not ( next(self.members) ) then
		table.insert(self.members, self.owner)
	end
	flagInfo.members = self.members
	Events.sendEvent("REGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "flag", info = flagInfo})
end

function onDynamicObjectSave(self, event)
	if event.PID and event.PID == self.PID then
		local flagInfo = {}
		flagInfo.PID = self.PID
		flagInfo.position = {x = event.objPos.x, y = event.objPos.y, z = event.objPos.z}
		flagInfo.flagType = self.flagType
		flagInfo.enabled = self.enabled
		flagInfo.height = self.height
		flagInfo.width = self.width
		flagInfo.depth = self.depth
		flagInfo.radius = self.radius
		flagInfo.playername = self.owner
		flagInfo.noDestruct = self.noDestruct
		flagInfo.members = self.members
		Events.sendEvent("REGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "flag", info = flagInfo})
	end
end

function requestFlagGroupInfo(self, event)
	-- If same flag as the one requested
	if event.flagPID == self.PID then
		self:compileAndSendFlagGroupInfo()
	end
end

function requestAllFlags(self, event)
	self:compileAndSendFlagGroupInfo()
end

function saveFlagGroupInfo(self, event)
	if event.flagInfo.PID == self.PID then
		self.groupName = event.flagInfo.groupName
		self.ravers = event.flagInfo.ravers
		self.members = event.flagInfo.members
		self:saveFlagState("default") -- Save new values to DB
		self:compileAndSendFlagGroupInfo() -- Send updated values to Framework_TopRightHUD
		self:registerSystemFlag({})
	end
end

function onFeePayment(self)
	if event.PID == nil then return end
	if event.PID == self.PID then
		self.currentTime = kgp.gameGetCurrentTime() or 0 -- absolute time in seconds
		
		self.lastPaidDate = self.currentTime -- Tracks rep deprecation for followers
		self.feeDueDate  = self.feeDueDate + self.feeInterval
		
		self:saveFlagState("default")

		self:compileAndSendFlagGroupInfo()
	end
end

-- When a a player enters the zone
function onPlayerArrive(self, event)
	if event.player == nil then return end
	if event.player.name == nil then return end
	local playerName = event.player.name
	
	-- Send the new player a fee due notification, if necessary
	if checkForEntry(self.members, playerName) and self.flagInit == false then
		self:checkFeePaymentState(playerName)
	end
end

---------------------
-- LOCAL FUNCTIONS --
---------------------

function compileAndSendFlagGroupInfo(self)
	self.currentTime = kgp.gameGetCurrentTime() or 0 -- absolute time in seconds

	local flagInfo = {}
	flagInfo.PID = self.PID
	flagInfo.UNID = self.UNID
	flagInfo.groupName = self.groupName
	flagInfo.ravers = self.ravers
	flagInfo.members = self.members
	flagInfo.owner = self.ownerName

	flagInfo.feeDueDate = self.feeDueDate
	flagInfo.isFeeDue = ((self.currentTime >= self.feeDueDate) and self.feesEnabled)
	flagInfo.isFeeDueSoon = ((self.currentTime + FEE_ALERT_DAYS * SEC_PER_DAY >= self.feeDueDate) and self.feesEnabled)

	Events.sendClientEvent("FLAG_GROUP_INFO_RETURNED", {flagInfo = flagInfo, origin = event.player.name})
end

function saveFlagState(self, saveString)
	local flagData = {}
	flagData.groupName = self.groupName
	flagData.ravers = self.ravers
	flagData.members = self.members
	flagData.lastPaidDate = self.lastPaidDate

	-- Name
	local ownerInfo = kgp.objectGetOwner(self.PID)
	if ownerInfo then
		self.ownerName = ownerInfo.ownerName
	end
	flagData.ownerName = self.ownerName

	local flagDataString = Events.encode(flagData)
	local saveLoc = ""
	if saveString == "default" then
		saveLoc = BUILD_FLAG_KEY.."_"..tostring(self.PID), flagDataString
	else
		saveLoc = saveString
	end
	SaveLoad.saveGameData(saveLoc, flagDataString)
end

function loadFlagState(self, saveString)
	self.currentTime = kgp.gameGetCurrentTime() or 0 -- absolute time in seconds

	local saveLoc = ""
	if saveString == "default" then
		saveLoc = BUILD_FLAG_KEY.."_"..tostring(self.PID)
	else
		saveLoc = saveString
	end
	local flagDataString = SaveLoad.loadGameData(saveLoc)
	local flagData = {}
	if flagDataString then
		flagData = Events.decode(flagDataString)
	end
	self.ravers = flagData.ravers or {}
	self.members = flagData.members or {}
	self.ownerName = flagData.ownerName or ""
	-- Name
	if self.ownerName == "" then
		local ownerInfo = kgp.objectGetOwner(self.PID)
		if ownerInfo then
			self.ownerName = ownerInfo.ownerName
		end
		flagData.ownerName = self.ownerName
	end
	self.groupName = flagData.groupName or ""
	if self.groupName == "" or self.groupName == " " then
	 	self.groupName = self.ownerName.." Place"
	end

	self.lastPaidDate = flagData.lastPaidDate or self.currentTime
	self.feeDueDate = self.lastPaidDate + self.feeInterval -- log out

	self.flagInit = true
end

function updateFeePaymentTimer(self, deltaTick)
	self.feeCheckTimer = self.feeCheckTimer - deltaTick
	if self.feeCheckTimer <= 0 then
		self.currentTime = kgp.gameGetCurrentTime() or 0 -- absolute time in seconds

		self:checkFeePaymentState("")

		self.feeCheckTimer = FEE_CHECK_INTERVAL
		self:saveFlagState("default")
	end
end

function checkFeePaymentStateHandler(self, event)
	playerToMessage = ""
	if event.playerToMessage then
		playerToMessage = event.playerToMessage
	end
	self:checkFeePaymentState(playerToMessage)
end

function checkFeePaymentState(self, playerToMessage)
	self.currentTime = kgp.gameGetCurrentTime() or 0 -- absolute time in seconds

	-- CLAIM FLAG TEST CODE
	-- self.feeDueDate = self.currentTime - FEE_GRACE_PERIOD_DEL_SELF - 1 		-- TEST DELETE SELF
	-- self.feeDueDate = self.currentTime - FEE_GRACE_PERIOD_DEL_OBJ - 1 		-- TEST DELETE OBJECTS
	-- self.feeDueDate = self.currentTime + FEE_ALERT_DAYS * SEC_PER_DAY - 1 	-- TEST FEE IMMINENT
	-- self.feeDueDate = self.currentTime - 1 									-- TEST FEE PAST DUE

	if self.feesEnabled and self.feesEnabled == true and
	   self.flagInit and not self.deathPending and self.currentTime ~= 0 then

		-- Time to delete self
		if self.currentTime >= self.feeDueDate + FEE_GRACE_PERIOD_DEL_SELF then
			Events.sendEvent("FLAG_DELETE_ITEMS", {PID = self.PID})
			self.deathPending = true
			self:kill()
		-- Time to delete objects within flag
		elseif self.currentTime >= self.feeDueDate + FEE_GRACE_PERIOD_DEL_OBJ and not self.objectsDeleted then
			self.objectsDeleted = true
			Events.sendEvent("FLAG_DELETE_ITEMS", {PID = self.PID})
		-- Fee imminent alert
		elseif self.currentTime >= self.feeDueDate - FEE_ALERT_DAYS * SEC_PER_DAY and self.currentTime < self.feeDueDate then
			-- send message to top right hud, which sends message to flag info
			-- self.feeAlert = true
			local daysRemaining = getDayString(self.feeDueDate - self.currentTime)
			local tempMessage = "Your "..tostring(self.groupName).." Land Claim Flag requires payment in "..tostring(daysRemaining).." or you will lose your placed items!"
			local playerList = {}
			if playerToMessage and playerToMessage ~= "" then
				table.insert(playerList, playerToMessage)
			else
				for index, playerName in pairs(self.members) do
					table.insert(playerList, playerName)
				end
			end
			Events.sendEvent("REQ_FLAG_FEE_NOTIF_REQ_LIST", {players = playerList, message = tempMessage})
		elseif self.currentTime >= self.feeDueDate then
			-- self.feeWarn = true
			local hoursOrMinRemaining = getHourMinString(self.feeDueDate + FEE_GRACE_PERIOD_DEL_OBJ - self.currentTime)
			self.deleteSelfWarn = false
			local tempMessage = "The Maintenance Fee on "..tostring(self.groupName).." is past due! Pay within "..tostring(hoursOrMinRemaining).." or your items will be removed!"
			local playerList = {}
			if playerToMessage and playerToMessage ~= "" then
				table.insert(playerList, playerToMessage)
			else
				for index, playerName in pairs(self.members) do
					table.insert(playerList, playerName)
				end
			end
			Events.sendEvent("REQ_FLAG_FEE_NOTIF_REQ_LIST", {players = playerList, message = tempMessage})
		end
	end
end

----------------------
-- HELPER FUNCTIONS --
----------------------

convertTime = function(timestamp)
    local time = {}
    time["timestamp"] = timestamp
    -- time["month"]  = math.floor((math.fmod(timestamp, SEC_PER_YEAR) / SEC_PER_MON) + 1)  -- Add 1 (0 based)
    -- time["week"]   = math.floor( math.fmod(timestamp, SEC_PER_MONTH) / SEC_PER_WEEK)
    time["day"]    = math.floor( math.fmod(timestamp, SEC_PER_MONTH) / SEC_PER_DAY)
    time["hour"]   = math.floor( math.fmod(timestamp, SEC_PER_DAY) / SEC_PER_HOUR)
    time["minute"] = math.floor( math.fmod(timestamp, SEC_PER_HOUR) / SEC_PER_MIN)
    time["second"] = math.floor( math.fmod(timestamp, SEC_PER_MIN))
    
    return time
end

getDayString = function(timestamp)
	local tempTimestamp = math.floor(timestamp) -- /1000 -- Convert from miliseconds to seconds
	local time = convertTime(tempTimestamp)
	local timeString = ""
	if time.day > 0 then
		timeString = timeString..tostring(time.day)
		if time.day == 1 then
			timeString = timeString.." day"
		else
			timeString = timeString.." days"
		end
	elseif time.hour > 0 then
		timeString = " <1 day"
	elseif time.minute > 0 then
		timeString = " <1 day"
	elseif time.second > 0 then
		timeString = " <1 day"
	end
	return timeString
end

getHourMinString = function(timestamp)
	local tempTimestamp = math.floor(timestamp) -- /1000 -- Convert from miliseconds to seconds
	local time = convertTime(tempTimestamp)
	local timeString = ""
	if time.day > 0 then
		local tempHours = time.day * 24
		if time.hour > 0 then
			tempHours = tempHours + time.hour
		end
		timeString = timeString..tostring(tempHours).." hours"
	elseif time.hour > 0 then
		timeString = timeString..tostring(time.hour)
		if time.hour == 1 then
			timeString = timeString.." hour"
		else
			timeString = timeString.." hours"
		end
	elseif time.minute > 0 then
		timeString = timeString..tostring(time.minute)
		if time.minute == 1 then
			timeString = timeString.." minute"
		else
			timeString = timeString.." minutes"
		end
	elseif time.second > 0 then
		timeString = " <1 minute"
	end
	return timeString
end

getFeeDateString = function(self, inputTime)
	local tempTime 	 = inputTime -- Convert to seconds
	local tempYear   = math.floor((tempTime / 31556926) + 1970) -- Date serials start at 1/1/1970
	local tempMonth  = math.floor((math.fmod(tempTime, 31556926) / 2629743) + 1)  -- Add 1 (0 based)
	if tempMonth < 10 then tempMonth = "0"..tostring(tempMonth) end
	local tempDay	 = math.floor( math.fmod(tempTime, 2629743) / SEC_PER_DAY)
	if tempDay < 10 then tempDay = "0"..tostring(tempDay) end
	local tempHour   = math.floor( math.fmod(tempTime, SEC_PER_DAY) / SEC_PER_HOUR)
	if tempHour < 10 then tempHour = "0"..tostring(tempHour) end
	local tempMin 	 = math.floor( math.fmod(tempTime, SEC_PER_HOUR) / SEC_PER_MIN)
	if tempMin < 10 then tempMin = "0"..tostring(tempMin) end
	local tempSec 	 = math.floor( math.fmod(tempTime, SEC_PER_MIN))
	if tempSec < 10 then tempSec = "0"..tostring(tempSec) end

	return tempMonth.."\/"..tempDay.."\/"..tempYear..", "..tempHour..":"..tempMin..":"..tempSec
end

-- Returns whether a search item exists in a table
checkForEntry = function(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return true
		end
	end
	return false
end