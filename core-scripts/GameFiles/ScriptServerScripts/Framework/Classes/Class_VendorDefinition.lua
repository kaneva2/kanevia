--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- Class_Vendor.lua
-- Super class for all NPC vendors
----------------------------------------
include("Lib_GameDefinitions.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local GameDefinitions	= _G.GameDefinitions
local Events			= _G.Events
local Class				= _G.Class
local Players			= _G.Players
local NpcDefs 			= _G.NpcDefinitions

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
Class.createProperties({
	"timed",
	"header"
})

----------------------------------------
-- Constants
----------------------------------------
local INTERACTION_RANGE		= 25
local DEFAULT_NAME			= "Vendor Items"

local EMPTY_ITEM = {
	output = GameDefinitions.EMPTY_ITEM,
	input = GameDefinitions.EMPTY_ITEM,
	outputCount = 0, 
	cost = 0
}

local INVENTORY_KEY			= "vendorInventory"
local ITEMS_PER_PAGE		= 13
local PLACE_HOLDERS			= 4

local DEFAULT_HEADER		= "Choose an item to "
local RANDOM_HEADER			= "Pay for a chance to get a piece of random loot."

----------------------------------------
-- Global variables
----------------------------------------
s_type = "Vendor"

---------------------------------------
-- Private Methods
----------------------------------------

-- Instantiate an instance of Vendor
function create(self, input)
	local tempItems = {}
	if input == nil then input = {} end
	input.interactionRange = INTERACTION_RANGE
	
	_super.create(self, input)

	self.players = {}
	--self.tradeItems = {} --containing multiple trades
	--self.tradeItem = {}	-- containing a single trade

	self.spawnerPID = input.spawnerPID
	self.PID = input.PID
	self.timed = input.timed -- is a timed vendor?
	self.random = input.random -- is a random vendor?
	self.credits = input.credits -- is a credits vendor?
	self.gem = input.gem -- is a gem vendor?
	self.clothing = input.clothing -- is a clothing vendor???? --- Edie 
	self.lastVID = input.lastVID
	self.GLID = input.GLID

	self.name = input.name or DEFAULT_NAME
	self.actorName = input.actorName

	self.level = input.level or 1
	
	if input.header == "" then
		input.header = nil
	end
	-- no need to loop through all trades if its a random vendor, only takes 1 item type
	if self.random then
		if self.actorName then
			self.header = RANDOM_HEADER
		else
			self.header = RANDOM_HEADER
		end

		if input.trade.input ~= 0 then
			self.tradeItem = Toolbox.deepCopy(input.trade)
		end

		if not Toolbox.isContainerEmpty(input.loot.singles) then
			self.loot = {
				singles = {}
			}
			for i, v in pairs(input.loot.singles) do
				if type(v) == "table" or type(v) == "userdata" then
					self.loot.singles[i] = {
						UNID		= v.UNID,
						drop		= v.drop,
						quantity	= v.quantity
					}
					
					if v.UNID > 0 then
						self.tradeItems = true
					end
				else
					log("CLASS ERROR DETECTED - ED-7338 : Actor name: "..tostring(self.actorName).."; PID: "..tostring(self.PID).."; Type: "..tostring(type(v)))
				end
			end
		end
		
		for i, v in pairs(input.loot) do
			if tostring(i) == "singles" then
			else
				if v == true then
					self.tradeItems = true
				end
				self.loot[i] = v
			end
		end
	else -- NOT a random loot vendor
		if self.timed then
			self.header = input.header or DEFAULT_HEADER.."order"
		else
			self.header = input.header or DEFAULT_HEADER.."buy"
		end

		if input.trades and not Toolbox.isContainerEmpty(input.trades) then
			self.tradeItems = Toolbox.deepCopy(input.trades, true)
		end
	end


	if Toolbox.isContainer(self.tradeItems) then
		self:validateVendorValues()
	end
	if self.tradeItems then
		local tooltipText = "Click to purchase from Vendor."
		if self.timed then tooltipText = "Click to purchase from Timed Vendor." end
		if self.random then tooltipText = "Click to purchase from Random Lot Vendor." end
		if self.credits then tooltipText = "Click to purchase from Credits/Rewards Vendor." end
		if self.gem then tooltipText = "Click to purchase from Gem Vendor." end
		if self.clothing then tooltipText = "Click to purchase from the Clothing Vendor." end 
		if self.actorName and self.actorName ~= "" then tooltipText = "Click to purchase from "..self.actorName.."." end
		self:updateTooltip(tooltipText) -- Enable tooltips
	else	-- else the vendor has nothing for sale
		local tooltipText = "Vendor currently has nothing for sale."
		if self.timed then tooltipText = "Timed Vendor currently has nothing for sale." end
		if self.actorName and self.actorName ~= "" then tooltipText = self.actorName.." currently has nothing for sale." end
		self:updateTooltip(tooltipText) -- Enable tooltips
	end

	--self.trigger = Events.setTrigger(self.interactionRange, self.onTrigger, self)
    self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)


	Events.registerHandler("RETURN_RANDOM_LOOT", self.returnRandomLoot, self)
	Events.registerHandler("VENDOR_RESET", self.onReset, self)
	Events.registerHandler("FRAMEWORK_CLOSED_VENDOR", self.onVendorClosed, self)
	Events.registerHandler("FRAMEWORK_HIGHLIGHT_TIMED_VENDOR", self.highlightTimedVendor, self)
	Events.registerHandler("ACTIVATE_ACTOR", self.activateActorHandler, self)
end

function destroy(self, input)
	_super.destroy(self)

	Events.unregisterHandler("VENDOR_RESET", self.onReset, self)
	Events.unregisterHandler("FRAMEWORK_CLOSED_VENDOR", self.onVendorClosed, self)
	Events.unregisterHandler("ACTIVATE_ACTOR", self.activateActorHandler, self)

	if self.trigger then
		Events.removeTrigger(self.trigger)
	end

    if self.proxTrigger then
        Events.removeClientTrigger(self.proxTrigger)
    end
end
----------------------------------------
-- Event handlers
----------------------------------------

-- Right click handler
function onRightClick(self, event)
	--log("Class_Vendor - onRightClick")
	local uPlayer = event.player
	if uPlayer == nil then return end
	-- Activate actor
	self:preActivate(event)
end

-- Left click handler
function onLeftClick(self, event)
	--log("Class_Vendor - onLeftClick")
	local uPlayer = event.player
	if uPlayer == nil then return end
	-- Activate actor
	self:preActivate(event) 

end

function onReset(self, event)
	if self.PID == event.PID then
		if self.random then
			local uPlayer = event.player
			-- kgp.playerLoadMenu(g_currPlayerID, "Framework_RandomVendor.xml")
			Events.sendClientEvent("UPDATE_VENDOR_ITEMS", {updatedItems = self.tradeItem, PID = self.PID}, uPlayer.ID)
			Events.sendEvent("GENERATE_RANDOM_LOOT", {loot = self.loot, level = self.level, playerID = uPlayer.ID})
		end
	end
end

function onVendorClosed(self, event )
	if event.PID == self.PID then
		if event.player and event.player.activeMenuPID and event.player.activeMenuPID == self.PID then
			event.player.activeMenuPID = nil
		end
		if self.isRider then
			Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = self.PID, interacting = false})
		end
	end
end

-- Validate all conditions necessary before onActivate
function preActivate(self, event)
	log("Class_Vendor - preActivate")
	local uPlayer = event.player
	if not Players.validatePlayer(uPlayer) then
		return false
	end	

	-- Player's can't activate vendors
	if uPlayer and uPlayer.mode ~= "God" then
		if uPlayer.inVehicle then
			kgp.playerShowStatusInfo(uPlayer.ID, "You cannot do that while in a vehicle.", 3, 255, 0, 0)
		elseif uPlayer.alive == nil or uPlayer.alive == false then
			kgp.playerShowStatusInfo(uPlayer.ID, "You cannot do that while dead.", 3, 255, 0, 0)
		elseif  getDistanceToPoint(self, uPlayer.position) > self.interactionRange then
			kgp.playerShowStatusInfo(uPlayer.ID, "You must be closer to interact with that object.", 3, 255, 0, 0)
		else
			if self.repEnabled then
				if (self.followerOf == uPlayer.name or self.followerOf == nil) then
					Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)
					uPlayer.activeMenuPID = self.PID
					kgp.playerLoadMenu(uPlayer.ID, "Framework_NPCActionMenu.xml")
					local eventProperties = {
						actorPID = self.PID,
						UNID = self.UNID,
						value = self:getRepByPlayer(uPlayer.name),
						isFollower = self.followerOf == uPlayer.name,
						isSpouse = self.spouseOf == uPlayer.name,
						isRider = self.isRider,
						equippedGLIDs = self.equippedGLIDs
					}
					if self.behavior then
						if string.sub(self.behavior, -6, -1) == "_rider" then
							eventProperties.isRider = true
						end
					end
					Events.sendClientEvent("FRAMEWORK_DEFINE_NPC_ACTIONS", eventProperties, uPlayer.ID)
					Events.sendClientEvent("FULFILL_ACTOR_TALK_TO", {UNID = self.UNID}, uPlayer.ID)
				-- Cannot activate another player's spouse that unlocks at the spouse level (but CAN activate another player's follower that unlocks at the folloer level)
				elseif self.repUnlock == NpcDefs.SPECIAL_LEVELS[2] and self.spouseOf ~= uPlayer.name then
					kgp.playerShowStatusInfo(uPlayer.ID, "Only their spouse can do that.", 3, 255, 0, 0)		
				else
					-- Do not close and reopen the same Vendor
				    local activeMenu = uPlayer.activeMenuPID
				    if activeMenu and activeMenu == self.PID then
				    	return
				    else
				    	 Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)
				    end
				   	uPlayer.activeMenuPID = self.PID
					self:onActivate(event)
				end
			else
				-- Do not close and reopen the same Vendor
			    local activeMenu = uPlayer.activeMenuPID
			    if activeMenu and activeMenu == self.PID then
			    	return
			    else
			    	 Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)
			    end
			   	uPlayer.activeMenuPID = self.PID
				self:onActivate(event)
			end
		end
	end
	return false
end

-- Called on activation
function onActivate(self, event)
	log("Class_Vendor - onActivate")
	local uPlayer = event.player
	if uPlayer == nil then
		return
	end

	if (self.random and (self.tradeItem == nil or self.loot == nil)) or (self.random == nil and self.tradeItems == nil) then
		kgp.playerShowStatusInfo(uPlayer.ID, "This Vendor has nothing for sale at the moment!", 3, 255, 0, 0)
		return
	end

	Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)

	if self.timed then
		kgp.playerLoadMenu(uPlayer.ID, "Framework_TimedResource.xml")
		Events.sendClientEvent("UPDATE_TIMED_SPAWNER_PID", {spawnPID = self.spawnerPID, PID = self.PID}, uPlayer.ID)
	elseif self.random then
		kgp.playerLoadMenu(uPlayer.ID, "Framework_RandomVendor.xml")
	elseif self.credits then
		kgp.playerLoadMenu(uPlayer.ID, "Framework_CreditsVendor.xml")
	elseif self.gem then
		kgp.playerLoadMenu(uPlayer.ID, "Framework_GemVendor.xml")
	elseif self.clothing then
		kgp.playerLoadMenu(uPlayer.ID, "Framework_ClothingVendor.xml")
	elseif self.singleItem then
		kgp.playerLoadMenu(uPlayer.ID, "Framework_SingleItemVendor.xml")
	else
		kgp.playerLoadMenu(uPlayer.ID, "Framework_Vendor.xml")
	end

	-- Sends update container event to InventoryHandler > updateItemContainer
	if self.random then
		Events.sendClientEvent("UPDATE_VENDOR_ITEMS", {updatedItems = self.tradeItem, PID = self.PID}, uPlayer.ID)
		Events.sendEvent("GENERATE_RANDOM_LOOT", {loot = self.loot, level = self.level, playerID = uPlayer.ID})
	else
		Events.sendClientEvent("UPDATE_VENDOR_ITEMS", {updatedItems = Toolbox.convertTableToIndexed(self.tradeItems), PID = self.PID}, uPlayer.ID)
	end
	Events.sendClientEvent("UPDATE_VENDOR_HEADER", {header = self.header, name = self.actorName, PID = self.PID, GLID = self.GLID}, uPlayer.ID)
	if self.isRider then
		Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = self.PID, interacting = true})
	end

	Events.sendClientEvent("FULFILL_ACTOR_TALK_TO", {UNID = self.UNID}, uPlayer.ID)
end


----------------------------------------
-- Class functions
----------------------------------------
function activateActorHandler(self, event)
	local uPlayer = event.player
	local iPID = event.PID
	if uPlayer == nil then return end
	if iPID == nil then return end

	if iPID == self.PID then
		self:onActivate(event)
	end
end

function returnRandomLoot(self, event)
	local lootTable = Toolbox.deepCopy(event.lootTable)
	local tempLoot = {}
	tempLoot.UNID = lootTable.UNID
	tempLoot.count = lootTable.count

	-- If an empty item, don't get properties. This allow you to win "nothing."
	--if lootTable.UNID == 0 and lootTable.count == 0 then
		Events.sendClientEvent("UPDATE_RANDOM_VENDOR_LOOT", {lootTable = tempLoot}, event.playerID)
	--end
end

function highlightTimedVendor(self, event)
	if event.player and self.timed then
		if event.spawnerPID and (tostring(event.spawnerPID) == tostring(self.spawnerPID)) or (tostring(event.spawnerPID) == tostring(self.PID)) then
			Events.sendClientEvent("FRAMEWORK_UPDATE_TIMED_STATE", {PID = self.PID, enable = event.enable}, event.player.ID)
		end
	end
end

function validateVendorValues( self)
	local validVendor = false
	for index, data in pairs(self.tradeItems) do
		if data.input then
			validVendor = true
			data.cost = data.cost or 1
		end
		if data.output then
			data.outputCount = data.outputCount or 1
			if self.credits then
				validVendor = true
			end
		end

		if (data.output and data.input) and not (self.credits or self.random or self.clothing) then
			validVendor = true
		end
	end

	if #self.tradeItems == 1 then
		self.singleItem = true
	end

	if not validVendor then
		self.tradeItems = nil
	end
end