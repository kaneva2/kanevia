--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

include("Lib_SaveLoad.lua")


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local SaveLoad			= _G.SaveLoad
local Events			= _G.Events
local Class				= _G.Class

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------

Class.import("Inventory")

Class.createClass("Class_Spawner", "Class_Base")

-- public properties
Class.createProperties({
	"spawnGLID",
	"particleGLID",
	"spawnCoords",
	"spawnUNID"
})

-----------------------------------------------------------------
-- Constant Definitions
-----------------------------------------------------------------
local DEFAULT_MAX_SPAWNS = 1
local DEFAULT_SPAWN_RADIUS = 300
local DEFAULT_RESPAWN_TIME = 300

local MAX_SPAWN_RADIUS = 75

local SPAWNER_PARTICLE_GLID = nil
local CREATOR_ONLY = true -- This object won't display for players
local DEFAULT_MODE_LABELS = {God = {"name"}}

s_type = "spawner"



function create(self, input)
	input = input or {}
	input.creatorOnly = CREATOR_ONLY
	input.modeLabels = DEFAULT_MODE_LABELS
	_super.create(self, input)
	
	self.persistentSpawn = input.persistentSpawn or false
	self.autoLoot = input.autoLoot or false

	self:setCollision(false)
	
	self.particleGLID = SPAWNER_PARTICLE_GLID
	self.maxSpawns = input.maxSpawns or DEFAULT_MAX_SPAWNS
	self.spawnCoords = input.spawnCoords
	
	if self.spawnCoords == nil then
		self.spawnCoords = {}
		self.spawnCoords.x, self.spawnCoords.y, self.spawnCoords.z, self.spawnCoords.rx, self.spawnCoords.ry, self.spawnCoords.rz = kgp.objectGetStartLocation(self.PID)
	end
	
	self.spawnRadius = input.spawnRadius or DEFAULT_SPAWN_RADIUS
	self.respawnTime = input.respawnTime or DEFAULT_RESPAWN_TIME
	
	self.spawnType = input.spawnType
	self.spawnGLID = input.spawnGLID
	self.spawnInput = input.spawnInput or {}

	if self.spawnInput then
		self.spawnUNID = self.spawnInput.UNID
	end
	
	self.spawnInfo = {}
	for i = 1, self.maxSpawns do
		self.spawnInfo[i] = {spawnPID = 0, spawnTimer = 0}
	end
	
	self:loadSpawnTimers()
	
	self.spawnActive = false
	self.waitForSpawn = false
	self.usersInProximity = {}
	
	self.tick = nil
	
	Events.registerHandler("OBJECT_SPAWNED", self.onObjectSpawned, self)
	Events.registerHandler("OBJECT_DESPAWNED", self.onObjectDespawned, self)
	Events.registerHandler("RESET_SPAWNER", self.resetSpawnerHandler, self)
	
	Events.registerHandler("TIMER", self.onTimer, self)
	-- Register players that leave the zone
	Events.registerHandler("PLAYER-DEPART", self.onDepart, self)
	
	Events.registerHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)
	
	self.proxTrigger = Events.setTrigger(self.spawnRadius, self.onTrigger, self)
end

function destroy(self)
	Events.sendEvent("OBJECT_DESPAWN_ALL", {spawnerPID = self.PID})

	Events.unregisterHandler("OBJECT_SPAWNED", self.onObjectSpawned, self)
	Events.unregisterHandler("OBJECT_DESPAWNED", self.onObjectDespawned, self)
	Events.unregisterHandler("TIMER", self.onTimer, self)
	Events.unregisterHandler("PLAYER-DEPART", self.onDepart, self)
	Events.unregisterHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)

	if self.proxTrigger then
		Events.removeTrigger(self.proxTrigger)
	end
	
	_super.destroy(self)
end

function onDepart(self, event)
	self.usersInProximity[event.player.ID] = nil
	
	if areUsersInProximity(self.usersInProximity) == false then
		self.spawnActive = false
		for i, spawn in ipairs(self.spawnInfo) do
			spawn.spawnPID = 0
		end
		Events.sendEvent("OBJECT_DESPAWN_ALL", {spawnerPID = self.PID, spawnType = self.spawnType})
	end
end

function onTimer(self, event)
	if self.tick == nil then self.tick = event.tick return end

	local tickDiff = event.tick - self.tick
	self.tick = event.tick
	
	for i, spawn in ipairs(self.spawnInfo) do
		if spawn.spawnPID == 0 then
			if spawn.spawnTimer == 0 then
				if self.spawnActive and self.waitForSpawn == false then
					self.waitForSpawn = true
					local spawnPoint = {x = self.spawnCoords.x, y = self.spawnCoords.y, z = self.spawnCoords.z, rx = self.spawnCoords.rx, ry = self.spawnCoords.ry, rz = self.spawnCoords.rz}
					if self.maxSpawns > 1 then
						local spawnRadius = math.min(self.spawnRadius, MAX_SPAWN_RADIUS)
						spawnPoint.x = math.random((self.spawnCoords.x - spawnRadius), (self.spawnCoords.x + spawnRadius))
						spawnPoint.z = math.random((self.spawnCoords.z - spawnRadius), (self.spawnCoords.z + spawnRadius))
						spawnPoint.rx = math.random() * 2 - 1 --randomize rotation between -1 and 1
						spawnPoint.rz = math.random() * 2 - 1 --randomize rotation between -1 and 1
					else
						if not (self.spawnCoords.x and self.spawnCoords.y and self.spawnCoords.z and self.spawnCoords.rx and self.spawnCoords.ry and self.spawnCoords.rz) then
							self.spawnCoords.x, self.spawnCoords.y, self.spawnCoords.z, self.spawnCoords.rx, self.spawnCoords.ry, self.spawnCoords.rz = kgp.objectGetStartLocation(self.PID)
						end
						spawnPoint = self.spawnCoords
					end
					if self.loot then
						local object = self._dictionary
						object.spawnerPID = self.PID
						object.spawnType = self.spawnType
					
						if self.autoLoot then
							object.autoLoot = true
						end
						
						Inventory.generateLootChest({object = object, canDropGemBox = true}) -- All spawned loot containers can drop a gem box
					else
						Events.sendEvent("OBJECT_SPAWN", {spawnerPID = self.PID, spawnType = self.spawnType, GLID = self.spawnGLID, location = spawnPoint, spawnInput = self.spawnInput})
					end
				end
			else
				spawn.spawnTimer = spawn.spawnTimer - tickDiff
				if spawn.spawnTimer < 0 then spawn.spawnTimer = 0 end
			end
		end
	end
end

function onTrigger(self, event)
	-- Only players trigger spawners
	if event.player then
		if self.usersInProximity == nil then self.usersInProximity = {} end
		if event.triggerType == 1 then
			local eventMode = event.mode or event.player.mode
			self.usersInProximity[event.player.ID] = eventMode
			if eventMode ~= "God" and self.spawnActive == false then
				self.spawnActive = true
				self:onTimer({tick = kgp.scriptGetTick()})
			end
		else
			self.usersInProximity[event.player.ID] = nil
		end
		
		if areUsersInProximity(self.usersInProximity) == false then
			self.spawnActive = false
			for i, spawn in ipairs(self.spawnInfo) do
				spawn.spawnPID = 0
			end
			Events.sendEvent("OBJECT_DESPAWN_ALL", {spawnerPID = self.PID, spawnType = self.spawnType})
		end
	end
end

function onObjectSpawned(self, event)
	if event.spawnerPID == self.PID and event.spawnType == self.spawnType then
		for i, spawn in ipairs(self.spawnInfo) do
			if spawn.spawnPID == 0 and spawn.spawnTimer == 0 then
				spawn.spawnPID = event.spawnPID
				self.waitForSpawn = false
				self:onTimer({tick = kgp.scriptGetTick()})
				return
			end
		end
	end
end

function onObjectDespawned(self, event)
	if self.spawnActive and event.spawnerPID == self.PID and event.spawnType == self.spawnType then
		for i, spawn in ipairs(self.spawnInfo) do
			if spawn.spawnPID == event.spawnPID then
				spawn.spawnPID = 0
				if event.refresh ~= true then
					spawn.spawnTimer = self.respawnTime * 1000
					saveSpawnTimers(self)
				end
				return
			end
		end
	end
end

function resetSpawner(self, event)
	if self.spawnActive then
		for i, spawn in ipairs(self.spawnInfo) do
			if spawn.spawnPID == 0 and spawn.spawnTimer == 0 then
				spawn.spawnPID = 0
				spawn.spawnTimer = self.respawnTime * 1000
				self.waitForSpawn = false
				saveSpawnTimers(self)
				self:onTimer({tick = kgp.scriptGetTick()})
			end
			return
		end
	end
end

function resetSpawnerHandler(self, event)
	if event.spawnerPID == self.PID and self.waitForSpawn == true then
		self:resetSpawner()
	end
end

-- Called when a player changes modes
function onPlayerModeChanged(self, event)
	if event.player and event.mode and self.usersInProximity[event.player.ID] then
		event.triggerType = 1
		self:onTrigger(event)
	end
end

function areUsersInProximity(usersInProximity)
	for playerID, state in pairs(usersInProximity) do
		if state and state ~= "God" then return true end
	end
	return false
end

function loadSpawnTimers(self)
	local savedValues = SaveLoad.loadGameData("DNC_SpawnTimers_"..tostring(self.PID))
	
	if savedValues and not self.persistentSpawn then
		SaveLoad.deleteGameData("DNC_SpawnTimers_"..tostring(self.PID))
		return
	end
	
	if savedValues and savedValues ~= "[]" then
		savedValues = Events.decode(savedValues)
		local curTime = kgp.gameGetCurrentTime() or 0
		for i, savedValue in ipairs(savedValues) do
			if savedValue and self.spawnInfo[i] and savedValue > 0 then
				local spawnTimer = savedValue - curTime
				if spawnTimer > 0 then
					self.spawnInfo[i].spawnTimer = spawnTimer * 1000
				end
			end
		end
	end
end

function saveSpawnTimers(self)
	if not self.persistentSpawn then return end
	
	local curTime = kgp.gameGetCurrentTime() or 0
	local savedValues = nil
	
	for i, spawn in ipairs(self.spawnInfo) do
		if savedValues == nil then savedValues = {} end
		local spawnTimer = curTime + math.ceil(spawn.spawnTimer / 1000)
		table.insert(savedValues, spawnTimer)
	end
	
	if savedValues then
		SaveLoad.saveGameData("DNC_SpawnTimers_"..tostring(self.PID), Events.encode(savedValues))
	end
end