--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_PlaceableLoot.lua
-- Dumb placeable objects without any specific behavior are represented by this class on the server.
include("Lib_GameDefinitions.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Debug				= _G.Debug
local GameDefinitions	= _G.GameDefinitions
local Events			= _G.Events
local Class				= _G.Class

local LOG_FUNCTION_CALL, LOG_PLACEABLE_LOOT_FILTER = Debug.generateCustomLog("Class_PlaceableLoot - ")
Debug.enableLogForFilter(LOG_PLACEABLE_LOOT_FILTER, false) -- Set this to true if you want to get a complete log of Class_PlaceableLoot function calls


-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
Class.createClass("Class_PlaceableLoot", "Class_BaseItemContainer")

Class.createProperties({
	"doesGenerateLoot",
})

----------------------------------------
-- Class-specific constants
----------------------------------------

local DEFAULT_NAME = "Dumb-as-a-doorknob-yet-lootable object"
local MAX_DROP_RATE = 100000
local MILISECONDS_IN_AN_HOUR = 1000 * 60 * 60
local DATABASE_KEY = "DNC_PlaceableLoot"
s_type = "placeable_loot"


----------------------------------------
-- Local functions
----------------------------------------
local getStackSizeFromItemProperty -- (itemProperties)


----------------------------------------
-- Constructor - Destructors
----------------------------------------

function create(self, input)
	LOG_FUNCTION_CALL("create")
	
	input = input or {}
	
	_super.create(self, input)
	
	self:initFromInput(input)
	
	if( self.doesGenerateLoot ) then

		self:loadVariablesFromDB()
		
		-- Register for timer event
		Events.registerHandler("TIMER", self.onTimer, self)
		Events.registerHandler("ACTIVATE_PLAYER", self.onPlayerEnteredWorld, self)
		
		-- Update client side highlights
		self:updateClientHighlight()
	end
end

function destroy(self)
	LOG_FUNCTION_CALL("destroy")
	
	if( self.isBeingRemoved ) then
		-- Item is being removed from the world
		self:deleteVariablesFromDB()
	else
		-- World is shutting down
		self:saveVariablesToDB()
	end
	
	if self.generateLoot then
		-- Unregister from events signed up to
		Events.unregisterHandler("TIMER", self.onTimer, self)
		Events.unregisterHandler("PLAYER-ARRIVE", self.onPlayerEnteredWorld, self)
	end
	
	_super.destroy(self)
end



----------------------------------------
-- Event handlers
----------------------------------------


-- Right click event handler
function onRightClick(self, event)
	LOG_FUNCTION_CALL("onRightClick")

	-- Don't allow anyone but the owner to interact
	local player = event.player
	if(self.owner ~= player.name) then
		return
	end
	
	-- Don't allow if it doesn't generate loot
	if( not self.doesGenerateLoot ) then 
		return
	end
	
	-- Don't allow interaction if the container is empty
	if( self:isContainerEmpty() ) then
		return
	end
	
	self:activateContainerForPlayer(event.player, false)
end



-- Timer event, activated close to every second
function onTimer(self, event)
	LOG_FUNCTION_CALL("onTimer")
	
	if( not self.doesGenerateLoot ) then 
		return
	end
	
	local timeStamp = event.tick
	local timeDiff = 0
	
	-- If we have just woken the world up then apply the time that passed while we were away
	if( self.serverSleepPeriod ) then
		--log("self.serverSleepPeriod: " .. tostring(self.serverSleepPeriod))
		timeDiff = self.serverSleepPeriod
		self.lastTimeStamp = timeStamp
		self.serverSleepPeriod = nil
	else
		if self.lastTimeStamp ~= nil then 
			timeDiff = timeStamp - self.lastTimeStamp
		end
		-- Update the last time stamp 
		self.lastTimeStamp = timeStamp
	end
	
	-- HACK TO LET THE LOOTABLE OBJECT PRODUCE LOOT EVERY 10 SECONDS
	--timeDiff = timeDiff * 360
	
	if( timeDiff > 0 ) then
		self:updateCountdowns(timeDiff)
	end
end

-- Activated whenever a player enters the world
function onPlayerEnteredWorld(self, event)
	LOG_FUNCTION_CALL("onPlayerEnteredWorld")
	
	local player = event.player
	
	if player and player.ID and player.name == self.owner  then
		-- Find out if there is loot to be collected. If the container is empty 
		-- check whether the server just woke up and whether the time that passed 
		-- while asleep granted this object any loot.
		local hasLoot = not self:isContainerEmpty() or self:willReceiveLootAfterSleepPeriod()
		if hasLoot then
			Events.sendClientEvent("FRAMEWORK_PLAYER_HAS_LOOT", {}, player.ID)
		end
		
		-- Update client side highlights
		self:updateClientHighlight()
	end	
end


-- Activated by self, whenever a loot is generated
function onLootGenerated(self, lootInfo)
	LOG_FUNCTION_CALL("onLootGenerated")
	
	self:setClientHighlight(true)
	
	-- We are currently disabling this as per Chris' instructions
	do
		return 
	end
	
	if( lootInfo and lootInfo.UNID  ) then
		local lootProperties = self:getItemPropertiesByUNID(lootInfo.UNID)
		
		if( lootProperties ) then
			local message = "Your " .. tostring(lootProperties.name) .. " has generated earnings"
			self:sendNotificationToOwner(message)
		end
	end
end


-- OVERRIDE Class_BaseItemContainer.onItemContainerClosed
function onItemContainerClosed(self, event)
	
	_super.onItemContainerClosed(self, self.event)
	
	self:updateClientHighlight()
end



----------------------------------------
-- Class methods
----------------------------------------


-- Set up this class from the given input
function initFromInput(self, input)
	LOG_FUNCTION_CALL("initFromInput")
	
	self.name = input.name or DEFAULT_NAME
	
	self.doesGenerateLoot = input.generatesLoot == "true"
	
	if( self.doesGenerateLoot ) then
		self.interactionRange = input.range
		
		self.countdownList = {}
		self.lootInfoList = {}
		if( input.singles ) then
			for i, lootInput in pairs(input.singles) do
				local loot = {}
				
				if( not lootInput.dropRate or lootInput.dropRate <= 0 ) then
					loot.dropRate = 1
				else
					loot.dropRate = lootInput.dropRate
					if (loot.dropRate > MAX_DROP_RATE) then
						loot.dropRate = MAX_DROP_RATE
					end
				end
				
				if( not lootInput.maxDropCount or lootInput.maxDropCount <= 0 ) then
					loot.maxDropCount = 1
				else
					loot.maxDropCount = lootInput.maxDropCount
				end
				
				loot.UNID = lootInput.UNID
				
				--local countdownLength = MILISECONDS_IN_AN_HOUR / loot.dropRate
				local countdownLength = MILISECONDS_IN_AN_HOUR
				loot.countdownLength = countdownLength
				
				self.countdownList[tonumber(i)] = countdownLength
				
				self.lootInfoList[tonumber(i)] = loot
			end
		end
	else
		self.interactionRange = nil
	end
end



-- Check if there will be loot generated after waking up from a sleep period
function willReceiveLootAfterSleepPeriod(self)
	if self.serverSleepPeriod then
		for i, countdown in ipairs(self.countdownList) do
			if countdown <= self.serverSleepPeriod then
				return true
			end
		end
	end
	
	return false
end

-- Update time counters and generate loot if necessary
function updateCountdowns(self, timeDiff)
	LOG_FUNCTION_CALL("updateCountdowns - timeDiff: " .. tostring(timeDiff))
	
	for i, countdown in ipairs(self.countdownList) do
		--log("Countdown(" .. tostring(i) .. ") = " .. tostring(countdown))
		local timeLeft = countdown - timeDiff
		
		-- Have we triggered the counter?
		while( timeLeft <= 0 ) do
			local lootInfo = self.lootInfoList[i]
			
			timeLeft = timeLeft + lootInfo.countdownLength

			-- Generate loot
			local newLoot = self:generateLoot(lootInfo, lootInfo.dropRate)
			if( newLoot ) then
				self:resetItemsTo(newLoot)
				self:onLootGenerated(lootInfo)
			end
		end
		
		self.countdownList[i] = timeLeft
	end
end

-- Returns the updated items list if there is a change
-- If no change was made, returns nada
function generateLoot(self, loot, lootCount)
	LOG_FUNCTION_CALL("generateLoot")
	
	if( loot ) then
		lootCount = lootCount or 1
		
		local currentItemCount = self:getExistingItemCountByUNID(loot.UNID)
		
		lootCount = lootCount + currentItemCount
		-- If we are up-to-capacity, then return early
		if lootCount > loot.maxDropCount then
			lootCount = loot.maxDropCount
		end
		
		if( currentItemCount == lootCount ) then
			return
		end
		
		local itemProperties = self:getItemPropertiesByUNID(loot.UNID)
		
		if( not itemProperties ) then
			log("WARNING: Item properties for " .. tostring(loot.UNID) .. " could not be found!")
		else
			-- Generate a new list of items from scratch
			local updatedItemsList = {}
			
			-- Add already existing items to the list but exclude ones that are of currently added loot's type
			for i = 1, self.containerSize do
				if not self:isItemAtIndexEmpty(i) then
					local item = self.itemsList[i]
					if(item.UNID ~= loot.UNID) then
						table.insert(updatedItemsList, item)
					end
				end
			end
			
			local itemStackSize = getStackSizeFromItemProperty(itemProperties)
			-- Add the new loot type in stack-size-permitted chunks
			while( lootCount > 0 ) do
			
				local newItem = {	
					UNID			= loot.UNID, 
					count			= lootCount > itemStackSize and itemStackSize or lootCount,
					properties		= itemProperties,
					instanceData	= self.getInstanceDataForItemProperty(itemProperties, false)
				}
				
				table.insert(updatedItemsList, newItem)
				
				lootCount = lootCount - itemStackSize
			end
			
			return updatedItemsList
		end
	end
end


-- Get the total count of a particular object type that we have accumulated
function getExistingItemCountByUNID(self, UNID)
	LOG_FUNCTION_CALL("getExistingItemCountByUNID")

	local count = 0
	
	for i, item in pairs(self.itemsList) do
		if (item.UNID == UNID) then
			count = count + item.count
		end
	end

	return count
end

-- The item properties is set inside the onItemPropertiesReturned function
function getItemPropertiesByUNID(self, UNID)
	return self.m_uItemProperties and self.m_uItemProperties[UNID]
end



-- Send a visual notification to the client owner of this object
function sendNotificationToOwner(self, message)
	LOG_FUNCTION_CALL("sendNotificationToOwner")
	
	local input = 	{	message = message,
						user = self.owner,
						textureName = "Loot"	--> Check NotificationTypes.lua to get the full list of texture types
					}
	self.writeToTicker(input)
end


function updateClientHighlight(self)
	
	local isEmpty = self:isContainerEmpty()
	
	self:setClientHighlight(not isEmpty)

end

-- Highlight the object on the client
function setClientHighlight(self, enable)
	LOG_FUNCTION_CALL("setClientHighlight - " .. tostring(enable))
	
	-- Get the owner and send it the lootable object PID to highlight
	local player = self:getOwnerData()
	if( player ) then 
		Events.sendClientEvent("FRAMEWORK_UPDATE_TIMED_STATE", {PID = self.PID, enable = enable}, player.ID)
	end
	
end



----------------------------------------
-- Class Methods (Database Interactions)
----------------------------------------

PLACEABLE_LOOT_ARCHIVE_VERSION_1_0		= 1
PLACEABLE_LOOT_ARCHIVE_VERSION_CURRENT	= PLACEABLE_LOOT_ARCHIVE_VERSION_1_0


-- Save contents to DB
function saveVariablesToDB(self)
	LOG_FUNCTION_CALL("saveVariablesToDB")
	
	if not self.doesGenerateLoot then
		return
	end

	local saveContent = {}
	
	-- Save archiving version
	saveContent.archiveVersion = PLACEABLE_LOOT_ARCHIVE_VERSION_CURRENT
	-- Save current time
	saveContent.saveTime = kgp.gameGetCurrentTime()
	-- Save countdowns for each loot
	saveContent.countdownList = {}
	for i, lootInfo in ipairs(self.lootInfoList) do
		local newEntry = {}
		newEntry.UNID = lootInfo.UNID
		newEntry.countdown = self.countdownList[i]
		
		--local properties = self:getItemPropertiesByUNID(lootInfo.UNID)
		--newEntry.instanceData = properties and properties.instanceData
		saveContent.countdownList[tonumber(i)] = newEntry
	end

	SaveLoad.saveGameData(DATABASE_KEY .. "_" .. tostring(self.PID), Events.encode(saveContent))
end

-- Load contents from DB
function loadVariablesFromDB(self)
	LOG_FUNCTION_CALL("loadVariablesFromDB")
	
	if not self.doesGenerateLoot then
		return
	end
	
	local savedContent = SaveLoad.loadGameData(DATABASE_KEY .. "_" .. tostring(self.PID))
	
	if savedContent ~= nil and savedContent ~= "[]" then
		savedContent = Events.decode(savedContent)
		-- Check the archive version.
		if savedContent.archiveVersion == PLACEABLE_LOOT_ARCHIVE_VERSION_1_0 then
			-- Get the time that has passed
			self.serverSleepPeriod = (kgp.gameGetCurrentTime() - savedContent.saveTime) * 1000 --> Converted to miliseconds, which is the unit the counters operate on.
			
			for i, lootInfo in ipairs(self.lootInfoList) do
				local savedLootInfo = savedContent.countdownList[i]

				if savedLootInfo and lootInfo.UNID == savedLootInfo.UNID then
					self.countdownList[i] = savedLootInfo.countdown
				else
					log("WARNING: Data loaded from DB does not match the template loot items")
				end
			end
		end
	end
end

-- Remove any saved data from DB
function deleteVariablesFromDB(self)
	LOG_FUNCTION_CALL("deleteVariablesFromDB")
	
	SaveLoad.deleteGameData(DATABASE_KEY .. "_" .. tostring(self.PID))
end





----------------------------------------
-- Local helper functions
----------------------------------------

-- 
function getStackSizeFromItemProperty(itemProperties)
	LOG_FUNCTION_CALL("getStackSizeFromItemProperty")
	
	local itemStackSize = (itemProperties and itemProperties.stackSize)
	
	if not itemStackSize then
		local itemType = itemProperties.itemType
		if itemType == 'weapon' or itemType == 'armor' or itemType == 'tool' then
			itemStackSize = 1
		else
			itemStackSize = GameDefinitions.DEFAULT_STACK_SIZE
		end
	end
	
	return itemStackSize
end
