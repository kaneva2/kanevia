--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_Collectible.lua

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Events			= _G.Events
local Class				= _G.Class
local Players			= _G.Players

-----------------------------------------
-- Class specification
-----------------------------------------
Class.createClass("Class_CraftingObject", "Class_Object")

Class.createProperties({
	"interactionRange"
})

-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local DEFAULT_CRAFING_OBJECT = "CRAFTING OBJECT"
local DEFAULT_CRAFTING_PROXIMITY = 25
local DEFAULT_PARTICLE = 4465625

s_type = "CraftingObject"

function create(self, input)

	if input == nil then 
		input = {}
	end

	self.interactionRange = input.interactionRange or DEFAULT_CRAFTING_PROXIMITY

	_super.create(self, input)

	-- Console.Write(Console.INFO, "CRAFTING OBJECT")
	self.particle = input.particle or DEFAULT_PARTICLE
	self.name = input.name or DEFAULT_CRAFING_OBJECT
	self.craftingResource = input.craftingResource
	self.craftingProximity = input.craftingProximity or DEFAULT_CRAFTING_PROXIMITY

	self.playersInProximity = {}
	self.playerClicksPending = {}

	self.targetable = input.targetable or "true"

	-- BURN!
	self:setParticle(self.particle)

	self:updateTooltip("Click to craft with "..self.name..".")
	
	Events.registerHandler("CHECK_PLAYER_CRAFTING_PROXIMITY", self.checkPlayerProximity, self)
	Events.registerHandler("CRAFTING_MENU_READY", self.craftingMenuReadyHandler, self)	
	Events.registerHandler("CRAFTING_MENU_SORTED", self.craftingMenuSortedHandler, self)	

	self.craftTrigger = Events.setTrigger(self.craftingProximity, self.onCraftingProximity, self)
    self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
end

function destroy(self)
	_super.destroy(self)
	Events.unregisterHandler("CHECK_PLAYER_CRAFTING_PROXIMITY", self.checkPlayerProximity, self)
	Events.unregisterHandler("CRAFTING_MENU_READY", self.craftingMenuReadyHandler, self)	
	Events.unregisterHandler("CRAFTING_MENU_SORTED", self.craftingMenuSortedHandler, self)	

	if self.craftTrigger then
        Events.removeTrigger(self.craftTrigger)
    end

    if self.proxTrigger then
        Events.removeClientTrigger(self.proxTrigger)
    end
end

function onCraftingProximity(self, event)
	if event.triggerType == 1 and event.player ~= nil and Players.validatePlayerName(event.player.name) and Players.validatePlayerID(event.player.ID) then
		self.playersInProximity[event.player.name] = event.player.ID
		Events.sendClientEvent("PLAYER_IN_CRAFTING_PROXIMITY", {UNID = self.UNID, craftingResource = self.craftingResource}, event.player.ID)
	elseif event.triggerType == 2 and event.player ~= nil and Players.validatePlayerName(event.player.name) then
		self.playersInProximity[event.player.name] = nil
		Events.sendClientEvent("PLAYER_OUT_CRAFTING_PROXIMITY", {UNID = self.UNID, craftingResource = self.craftingResource}, event.player.ID)
	end
end

function checkPlayerProximity( self, event )
	if event.player ~= nil and Players.validatePlayerName(event.player.name)  and Players.validatePlayerID(event.player.ID) and self.playersInProximity[event.player.name] ~= nil then
		Events.sendClientEvent("PLAYER_IN_CRAFTING_PROXIMITY", {UNID = self.UNID, craftingResource = self.craftingResource}, event.player.ID)
	end
end

-- On right click handler
function onLeftClick(self, event)
	local uPlayer = event.player
	local handEquipped = (uPlayer.handEquipped == true or uPlayer.handEquipped == nil)
	local doesPlayerOwn = self.owner == uPlayer.name

	-- if the hand tool is selected and the player owns the object, then we shouldn't interact with it 
	if (handEquipped and doesPlayerOwn) or not handEquipped then
		return
	end 


	if self:isPointWithinRange(uPlayer.position, DEFAULT_CRAFTING_PROXIMITY) then
		-- MenuOpen("TextureBrowser2.xml")
		-- kgp.playerLoadMenu(event.player.ID, "Framework_PlayerCrafting.xml") -- Unified inventory
		-- kgp.playerLoadMenu(event.player.ID, "Framework_CraftingObjectHandler.xml") -- Unified inventory
		-- kgp.playerLoadMenu(event.player.ID, "Framework_Crafting.xml") -- Unified inventory
		-- self:openCraftingMenu(event)

		Events.sendClientEvent("FRAMEWORK_OPEN_CRAFTING_MENU", {}, uPlayer.ID)
		--Events.sendClientEvent("FRAMEWORK_PLAYER_CRAFTING_HELPER_CLICK", {UNID = self.UNID}, event.player.ID) -- NOTE: this event won't make it through if the PlayerCrafting menu is not open yet, but it exists here in case the crafting menu IS already open
		
		-- Save off player id to use when crafting menu is confirmed to be open (what if crafting menu is already open?)
		self.playerClicksPending[uPlayer.ID] = true

	else
		kgp.playerShowStatusInfo(uPlayer.ID, "Out of range", 3, 255, 0, 0)
	end
end

-- On left click handler
function onRightClick(self, event)
	self:activate(event)
end

function onUse(self, event)
	if(event.PID and self.PID == event.PID) then
		self:activate(event)
	end
end

function activate(self, event)
	local uPlayer = event.player
	
	if self:isPointWithinRange(uPlayer.position, DEFAULT_CRAFTING_PROXIMITY) then
		-- MenuOpen("TextureBrowser2.xml")
		-- kgp.playerLoadMenu(event.player.ID, "Framework_PlayerCrafting.xml") -- Unified inventory
		-- kgp.playerLoadMenu(event.player.ID, "Framework_CraftingObjectHandler.xml") -- Unified inventory
		-- kgp.playerLoadMenu(event.player.ID, "Framework_Crafting.xml") -- Unified inventory
		-- self:openCraftingMenu(event)

		Events.sendClientEvent("FRAMEWORK_OPEN_CRAFTING_MENU", {}, uPlayer.ID)
		--Events.sendClientEvent("FRAMEWORK_PLAYER_CRAFTING_HELPER_CLICK", {UNID = self.UNID}, event.player.ID) -- NOTE: this event won't make it through if the PlayerCrafting menu is not open yet, but it exists here in case the crafting menu IS already open
		
		-- Save off player id to use when crafting menu is confirmed to be open (what if crafting menu is already open?)
		self.playerClicksPending[uPlayer.ID] = true

	else
		kgp.playerShowStatusInfo(uPlayer.ID, "Out of range", 3, 255, 0, 0)
	end
end

-- Finishes what began in onRightClick once the PlayerCrafting menu is confirmed to be open
function craftingMenuReadyHandler(self, event)
	if self.playerClicksPending[event.player.ID] and self.playerClicksPending[event.player.ID] == true then
		self.playerClicksPending[event.player.ID] = false
		Events.sendClientEvent("FRAMEWORK_PLAYER_CRAFTING_HELPER_CLICK", {UNID = self.UNID}, event.player.ID)
	end
end

function craftingMenuSortedHandler(self, event)
	if self.playerClicksPending[event.player.ID] and self.playerClicksPending[event.player.ID] == true then
		self.playerClicksPending[event.player.ID] = false
	end
end
