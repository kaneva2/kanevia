--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- use battle lib
include("Lib_SaveLoad.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local SaveLoad			= _G.SaveLoad
local Events			= _G.Events
local Class				= _G.Class

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------

Class.import("Inventory")

Class.createClass("Class_ItemContainer", "Class_BaseItemContainer")

Class.createProperties({
	"locked",
	"isCorpse",
	"corpseOwner"
})

-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local DEFAULT_NAME = "ITEM CONTAINER"
local PIN_SAVE_KEY = "DNC_pinCode"
local PIN_ACCESS_MEM_KEY = "DNC_pinCodeMemory"

local CHEST_DROP_SPEED = 40

s_type = "Item Container"

-----------------------------------------------------------------
-- Static variables
-----------------------------------------------------------------
local s_uPlayers


function create(self, input)
	
	_super.create(self, input)
	
	input.name = input.name or DEFAULT_NAME
	
	self.locked = input.locked == nil and DEFAULT_LOCKED or input.locked

	
	self.isCorpse = input.isCorpse == true
	
	if self.isCorpse then
		self.corpseOwner = input.corpseOwner
		self:setCollision(false)
	end

	if input.noCollision then
		self:setCollision(false)
	end 
	
	-- Add trigger for proximity highlighting
	self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
	
	self.despawnTime = input.despawnTime
	if self.despawnTime then
		self.despawnTime = (self.despawnTime * 1000) + kgp.scriptGetTick()
		if input.attackerID then
			Events.sendClientEvent("FRAMEWORK_REPOSITION_CHEST_CHECK", {PID = self.PID, position = Toolbox.deepCopy(self.position)}, input.attackerID)
		elseif self.spawnerPID then
			--No player found on the spawn, go to first player
			s_uPlayers = SharedData.new("players", false)
			for name, sharedData in pairs(s_uPlayers) do
				if sharedData and sharedData.inZone then
					Events.sendClientEvent("FRAMEWORK_REPOSITION_CHEST_CHECK", {PID = self.PID, position = Toolbox.deepCopy(self.position)}, sharedData.ID)
					break
				end
			end
		end
	end
	
	Events.registerHandler("OBJECT_REMOVE_SAFE", self.removeFromSafe, self)
	Events.registerHandler("FRAMEWORK_DESELECT_SINGLE_LOOT", self.deselectSingleLootChest, self)
	Events.registerHandler("FRAMEWORK_ATTEMPT_PICKUP_SAFE", self.pickUpSafe, self)
	Events.registerHandler("PIN_CODE_ACCEPTED", self.onPinAccepted, self)
	if self.despawnTime then
		Events.registerHandler("TIMER", self.onTimer, self)
		Events.registerHandler("FRAMEWORK_REPOSITION_CHEST_RETURN", self.repositionChest, self)
	end

	self.activated = false

end

function destroy(self)
	
	-- Remove trigger for proximity highlighting 
	if self.proxTrigger then
        Events.removeClientTrigger(self.proxTrigger)
    end

	if self.despawnTime then
		Events.unregisterHandler("TIMER", self.onTimer, self)
	end
	
	Events.unregisterHandler("OBJECT_REMOVE_SAFE", self.removeFromSafe, self)
	Events.unregisterHandler("FRAMEWORK_DESELECT_SINGLE_LOOT", self.deselectSingleLootChest, self)
	Events.unregisterHandler("FRAMEWORK_ATTEMPT_PICKUP_SAFE", self.pickUpSafe, self)
	Events.unregisterHandler("PIN_CODE_ACCEPTED", self.onPinAccepted, self)
	_super.destroy(self)
end

--------------------
-- Event handlers --
--------------------

function onTimer(self, event)
	
	-- tick, tock goes the clock
	if self.despawnTime and event.tick >= self.despawnTime then
		self.despawnTime = nil
		Events.unregisterHandler("TIMER", self.onTimer, self)
		Events.sendEvent("OBJECT_DESPAWN", {spawnerPID = self.spawnerPID or 0, despawnPID = self.PID, spawnType = "loot"})
	end
end


-- On player use event handler
function onUse(self, event)
	if(event.PID and self.PID == event.PID) then
		self.ctrlClick = nil
		self:activateContainerForPlayer(event.player, false)
	end
end

-- Right click event handler
function onRightClick(self, event)
	if event.modifiers and event.modifiers.ctrl and self.spawnerPID then
		self.ctrlClick = true
	end
	self:activateContainerForPlayer(event.player, false)
	self.ctrlClick = nil
end

-- Left click event handler
function onLeftClick(self, event)
	local handEquipped = (event.player.handEquipped == true or event.player.handEquipped == nil)
	local doesPlayerOwn = not self.owner or event.player.name == self.owner
	-- if the hand tool is selected and the player does not own
	if (handEquipped and not doesPlayerOwn) or not self.owner then
		self:activateContainerForPlayer(event.player, false)
	end 
end

-- Perform when the object is successfully activated by click or by pin code
validActivation = function(self, inputPlayer)
	self:activateContainerForPlayer(inputPlayer, true)
end

-- OVERRIDE Class_BaseItemContainer.onItemContainerClosed
function onItemContainerClosed(self, event)
	
	_super.onItemContainerClosed(self, self.event)
	
	self:checkForDespawn()
end

function repositionChest(self, event )
	if event.PID == self.PID then
		if event.pos then
			pos = event.pos
			local distanceToNode = self:getDistanceToPoint({x = pos.x, y = pos.y, z = pos.z})
			local dropTime = distanceToNode/CHEST_DROP_SPEED * 1000
			kgp.objectMove(self.PID, dropTime, 0, dropTime, 0, 1, pos.x, pos.y, pos.z)
			self.position.x = pos.x
			self.position.y = pos.y
			self.position.z = pos.z
		end
	end
end

---------------------
-- Class functions --
---------------------

function checkForDespawn(self)
	
	if self.spawnerPID then
		local despawn = self:isContainerEmpty()
		if despawn then
			Events.sendEvent("OBJECT_DESPAWN", {spawnerPID = self.spawnerPID, despawnPID = self.PID, spawnType = "loot"})
		end
	end
end

function removeFromSafe(self, event)
	
	for i, item in ipairs(self.itemsList) do
		if item.UNID == event.UNID then
			self:resetItemAtIndex(i)
			self:setupTooltips()
		end
	end
end

function deselectSingleLootChest(self, event)
	
	if event.PID == self.PID then
		if event.success then
			Events.registerHandler("TIMER", self.onTimer, self)
			self.despawnTime = 600 + kgp.scriptGetTick()
			kgp.playerSetAnimation(event.player.ID, 4672800)
		else
			self.activated = false
		end
	end
end

function pickUpSafe(self, event )
	if event.player and event.PID == self.PID then
		local player = event.player
		Events.sendClientEvent("FRAMEWORK_CLOSE_ITEM_CONTAINER", {}, player.ID)
		local success = true
		if self.owner ~= player.name and self.locked then
			kgp.playerShowStatusInfo(player.ID, "You do not own this safe.", 3, 255, 0, 0)
			success = false
		end

		for i, v in pairs(self.itemsList) do
			if v.UNID > 0 then 
				kgp.playerShowStatusInfo(player.ID, "Your container has items in it still and must be empty to pick up!", 3, 255, 0, 0)
				success = false
				break
			end	
		end

		Events.sendClientEvent("FRAMEWORK_PICKUP_RETURN", {PID = self.PID, success = success }, player.ID)
	end
end

-- OVERRIDE Class_BaseItemContainer.canPlayerOpenContainer
function canPlayerOpenContainer(self, player, preValidated)
	return self:canPlayerOpenSafe(player, preValidated)
end

-- Check whether a player can open the safe
function canPlayerOpenSafe(self, player, preValidated)
	-- If the item owner is not set (i.e. the item has no owner) then the player has access to the container.
	local isPlayerOwnerOfItem = not self.owner or player.name == self.owner
	local pinCode = self:getPinCode()
	local pinMembers = self:getPinAccessMemory()
	local isPreValidated = checkForEntry(pinMembers, player.name) or preValidated
	if isPlayerOwnerOfItem or isPreValidated then
		return true
	elseif self.locked and pinCode ~= nil then
		if preValidated ~= true then
			kgp.playerLoadMenu(player.ID, "Framework_PINCode.xml")
			Events.sendClientEvent("OBJECT_ACTIVATE_PIN", {PID = self.PID, name = self.name, owner = self.owner}, player.ID)
		end
	elseif not self.locked then
		return true
	else
		return false
	end
end

function getPinCode(self)
	local PID = self.PID
	local saveLoc = PIN_SAVE_KEY.."_"..tostring(PID)
	local pinDataString = SaveLoad.loadGameData(saveLoc)

	local pinData = {}
	if pinDataString then
		pinData = Events.decode(pinDataString)
	end
	local tempPinCode = pinData.pinCode or nil

	return tempPinCode
end

function onPinAccepted(self, event)
	if event.PID and event.PID == self.PID then
		self:validActivation(event.player)
		self:savePinAccessMemory(event.player.name)
	end
end

function getPinAccessMemory(self)
	local PID = self.PID
	local saveLoc = PIN_ACCESS_MEM_KEY.."_"..tostring(PID)
	local pinDataString = SaveLoad.loadGameData(saveLoc)

	local pinMembers = {}
	if pinDataString then
		pinMembers = Events.decode(pinDataString)
	end

	return pinMembers
end

function savePinAccessMemory(self, playerName)
	local PID = event.PID
	local pinCode = event.pinCode
	local pinMembers = self:getPinAccessMemory()
	if pinMembers == nil then -- if never saved before
		pinMembers = {}
	elseif #pinMembers == 0 then
		pinMembers = {}
	end
	if not checkForEntry(pinMembers, playerName) then
		table.insert(pinMembers, playerName)
		local pinDataString = Events.encode(pinMembers)
		local saveLoc = PIN_ACCESS_MEM_KEY.."_"..tostring(PID)
		SaveLoad.saveGameData(saveLoc, pinDataString)
	end
end

-- Returns whether a search item exists in a table
checkForEntry = function(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return true
		end
	end
	return false
end

function onAutoLootActivated(self, player)
	if self.activated == false then
		if self.canDropGemBox and not self.gemRolledFor then
			Events.sendEvent("FRAMEWORK_ROLL_FOR_GEM_CHEST", {player = player, PID = self.PID}) -- We'll open the item container after this returns through FRAMEWORK_GEM_CHEST_ROLL_RESULT
			self.autoLoot = true
		else
			local compactItemList = self:getCompactItemsList()
			Events.sendClientEvent("FRAMEWORK_ADD_SINGLE_LOOT", {spawnerPID = self.spawnerPID, PID = self.PID, itemList = compactItemList}, player.ID)
		end
		self.activated = true
	end
end