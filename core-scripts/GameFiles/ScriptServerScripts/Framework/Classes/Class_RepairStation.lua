--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- Class_RepairStation.lua
-- Super class for all NPC repair stations
----------------------------------------
-- RepairStation inherits Actor
Class.createClass("Class_RepairStation", "Class_Actor")


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Events			= _G.Events
local Players			= _G.Players

----------------------------------------
-- Constant Definitions
----------------------------------------
local INTERACTION_RANGE		= 25
local DEFAULT_REPAIR_UNID	= 124

s_type = "RepairStation"

----------------------------------------
-- Private Methods
----------------------------------------
-- Instantiate an instance of RepairStation
function create(self, input)
	if input == nil then input = {} end
	input.name = input.name or DEFAULT_NAME

	input.interactionRange = INTERACTION_RANGE
	
	_super.create(self, input)

	self:updateTooltip("Click to repair with "..self.name..".")
	
	self.targetable = input.targetable or "true"

	self.spawnerPID = input.spawnerPID

	self.repairUNID = input.repairUNID or DEFAULT_REPAIR_UNID

	--self.trigger = Events.setTrigger(self.interactionRange, self.onTrigger, self)
end

function destroy(self, input)
	_super.destroy(self)

	if self.trigger then
		Events.removeTrigger(self.trigger)
	end
end
----------------------------------------
-- Event handlers
----------------------------------------

-- Right click handler
function onRightClick(self, event)
	self:onActivate(event) 
end

function onLeftClick(self, event)
	local handEquipped = (event.player.handEquipped == true or event.player.handEquipped == nil)
	local doesPlayerOwn = self.owner == event.player.name
	-- if the hand tool is selected and the player does not own 
	if handEquipped and not doesPlayerOwn then
		self:onActivate(event)
	end 
end


function onUse(self, event)
	if(event.PID and self.PID == event.PID) then
		self:onActivate(event)
	end
end


-- function onTrigger(self, event)
-- 	local player = event.player

-- 	if player == nil then
-- 		return
-- 	end

-- 	-- If player exits trigger radius and actor is active, close menu
-- 	if event.triggerType == 2 and player.mode ~= "God" then
-- 		Events.sendClientEvent("FRAMEWORK_CLOSE_REPAIR", {}, player.ID)
-- 	end
-- end

-- Called on activation
function onActivate(self, event)
	local uPlayer = event.player
	if not Players.validatePlayer(uPlayer) then
		return
	end	
		
	-- Creators can't activate vendors
	if uPlayer and uPlayer.mode ~= "God" then
		if uPlayer.alive == nil or uPlayer.alive == false then
			kgp.playerShowStatusInfo(uPlayer.ID, "You cannot do that while dead.", 3, 255, 0, 0)
		elseif  getDistanceToPoint(self, uPlayer.position) > self.interactionRange then
			kgp.playerShowStatusInfo(uPlayer.ID, "You must be closer to interact with that object.", 3, 255, 0, 0)
		else
			uPlayer.activeMenuPID = self.PID
			kgp.playerLoadMenu(uPlayer.ID, "Framework_Repair.xml")
			Events.sendClientEvent("UPDATE_REPAIR_UNID", {repairUNID = self.repairUNID, PID = self.PID}, uPlayer.ID)
		end
	end
end