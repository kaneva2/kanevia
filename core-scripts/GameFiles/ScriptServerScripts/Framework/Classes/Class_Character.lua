--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_Character.lua
--
-- Character super class for:
-- Class_Player TODO
-- Class_NPC
--
-- User-definable variables
-- STRING
-- team
-- NUMBER
-- visionAngle, visionRange, score
-- TABLE
-- home({x,y,z,rx,ry,rz})
--
-- User-accessible class methods
-- addXP(xpGain)
--
-- Class Event Handlers
-- onRespawn()
-- onVision()
--
----------------------------------------

-----------------------------------------
-- Class specification
-----------------------------------------
Class.createClass("Class_Character", "Class_BaseMobile")

Class.createProperties({
	"maxHealth"
})

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Events			= _G.Events

----------------------------------------
-- "Private" class variables/methods
----------------------------------------
-- Variables
-- Constants
local DEFAULT_APPROVAL    = {}  -- Table of approval ratings
local DEFAULT_TARGET 	  = nil -- Target
--local DEFAULT_TEAM 		  = ""  -- TODO: wut team?
local DEFAULT_CHARACTER_RESPAWN_INTERVAL = 0
--local DEFAULT_CHARACTER_ARMOR_FACTOR = 1
--local DEFAULT_CHARACTER_MAX_HEALTH = 100
----------------------------------------
-- Public class variables
----------------------------------------
s_type = "Character"

----------------------------------------
-- Private Methods
----------------------------------------

-- Instantiate an instance of character
function create(self, input)
	-- Init super class
	input = input or {}

	--input.health = input.health or DEFAULT_CHARACTER_MAX_HEALTH
	_super.create(self, input)
	
	self:setCollision(false)

	-- Init this class
	-- User-definable variables (Public, document for users)
	--self.team 		 = input.team 		 or DEFAULT_TEAM
	--self.score 		 = input.score 		 or 0
	self.respawnInterval = input.respawnInterval or DEFAULT_CHARACTER_RESPAWN_INTERVAL

	-- Battle related properties (move to behavior?)
	--[[self.initArmorFactor = input.initArmorFactor or DEFAULT_CHARACTER_ARMOR_FACTOR
	self.currArmorFactor = self.initArmorFactor
	self.maxHealth       = self.health]]

	-- Self-defined variables (private, DO NOT DOCUMENT)
	self.target      = DEFAULT_TARGET
	
	-- Register for respawn events
	Events.registerHandler("CHARACTER-RESPAWN:" .. tostring(self.ID), self.onRespawn, self)
end

function destroy(self)
	_super.destroy(self)
	Events.unregisterHandler("CHARACTER-RESPAWN:" .. tostring(self.ID), self.onRespawn, self)
end

----------------------------------------
-- Event handlers
----------------------------------------

function onRespawn(self, event)
	--Console.Write( Console.DEBUG, "onRespawn " .. tostring(self.ID) )

	-- call respawn method
	self:respawn()
end

----------------------------------------
-- Public Class Methods
----------------------------------------

function die(self, reason)
	--Console.Write( Console.INFO, self.name .. " died: " .. reason )
	self.health = 0
	self:hide()

	-- Notify game controller
	Game.characterDeath({ ID = self.ID })
end

function respawn(self)
	--Console.Write( Console.INFO, self.name .. " is now back in the game" )
	self:show()
	self.health = self.maxHealth

	-- Notify game controller
	Game.characterRespawn({ ID = self.ID })
end