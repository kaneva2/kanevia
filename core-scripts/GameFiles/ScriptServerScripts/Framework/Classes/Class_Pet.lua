--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_Pet.lua
--
-- Pet class inherited from NPC
--
-- User-definable variables
-- STRING
-- NUMBER
-- TABLE
--
-- User-accessible methods
--
----------------------------------------
include("Lib_SaveLoad.lua")
include("Lib_GameDefinitions.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------

local Toolbox			= _G.Toolbox
local GameDefinitions	= _G.GameDefinitions
local SaveLoad			= _G.SaveLoad
local Events			= _G.Events
local Class				= _G.Class


-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
-- Create a Pet class inherited from NPC
Class.createClass("Class_Pet", "Class_BattleEntity")

Class.createProperties({
	"master",
	"showLabels",
	"combatEnabled"
})


-----------------------------------------------------------------
-- Constant Definitions
-----------------------------------------------------------------
local DEFAULT_NAME = "Pet"

local MILISECONDS_PER_SECOND = 1000
local SECONDS_PER_MINUTE = 60
local SECONDS_PER_HOUR = SECONDS_PER_MINUTE * 60
local SECONDS_PER_DAY = SECONDS_PER_HOUR * 24
local MILISECONDS_PER_MINUTE = MILISECONDS_PER_SECOND * SECONDS_PER_MINUTE
local MILISECONDS_PER_DAY = SECONDS_PER_DAY * MILISECONDS_PER_SECOND

local SIT_THRESHOLD = 3.5 --Old sit threshold of .5  plus the SLOT_OFFEST 
local WALK_THRESHOLD = 15 --adjusted so pet walks when player is walking, runs when player is running
local ATTACK_TIME = 2000
local DEFAULT_ATTACK_RANGE = 5
local DEFAULT_SPEED = 3
local HEALTH_REGEN_TICK = 30 * MILISECONDS_PER_SECOND -- 30k miliseconds
local HEALTH_STARVE_TICK = 30 * MILISECONDS_PER_SECOND -- 30000
local HEALTH_STARVE_DAYS = 7
local DEATH_ANIM_TIME = 1.75

local DEFAULT_WANDER_RANGE = 25
local WANDER_TIMER = 5000

local DEFAULT_DRAW_DISTANCE = 1800

local SLOT_OFFSETS = 	{	
	{x = 3, y = 0, z = 0},
	{x = 3, y = 0, z = 3},
	{x = 0, y = 0, z = 3},
	{x = -3, y = 0, z = 3},
	{x = -3, y = 0, z = 0}
}

local DEFAULT_ROTATION = {	
	x = 0.0,
	y = 0.0,
	z = 0.0,
	w = 1.0
}

-- States for NPC
local STATE = { 
	IDLE      = 0,
	WANDER    = 1,
	STAY      = 2,
	FOLLOW    = 3,
	CHASE     = 4,
	RETURNING = 5,	   	
	DEAD   	  = 6
}

local PET_KEY = "pet"

s_type = "Pet"


-- Instantiate an instance of NPC
function create(self, input)
	-- TODO: Error check input values
	if input == nil then input = {} end
	input.name = input.name  or DEFAULT_NAME
	
	self.registered = false
	Events.registerHandler("FRAMEWORK_OBJECT_REGISTERED", self.onRegistered, self)
	
	_super.create(self, input)
	
	self.animations = {}
	self.particles = {}
	self.sounds = {}

	-- User-definable variables (Public, document for users)

	-- Handle Euler rotation -> Quaternian conversion here
	self.eulerRotX = 0
	self.eulerRotY = input.eulerRotY or 0
	self.eulerRotZ = 0

	self.walkSpeed = input.walkSpeed or DEFAULT_SPEED
	self.runSpeed  = input.runSpeed or DEFAULT_SPEED
	self.speed     = self.walkSpeed or DEFAULT_SPEED

	self.tick = 0
	self.attackTimer = 0
	self.attackRange = input.attackRange or DEFAULT_ATTACK_RANGE
	self.wanderRange = input.wanderRange or DEFAULT_WANDER_RANGE
	self.regenTimer = 0
	self.starveTimer = HEALTH_STARVE_TICK
	
	self.combatEnabled = input.combatEnabled == nil or input.combatEnabled == true

	if input.food then
		self.food = input.food
		
		local uProperties = self.m_uItemProperties[input.food]
		if uProperties then
			self.foodName = uProperties.name
			self.tooltip = "Likes to Eat: " .. tostring(uProperties.name)
		end
	end

	if input.requiresFood then
		self.requiresFood = input.requiresFood
	end 

	if input.showLabels == nil then
		input.showLabels = true
	end

	self.showLabels = input.showLabels or false
	if not self.showLabels then
		self:clearLabels()
	end

	if input.animations then
		if tonumber(input.animations.idleAnim) then
			self.animations.idleAnim = input.animations.idleAnim
		end
		if tonumber(input.animations.walkAnim) then
			self.animations.walkAnim = input.animations.walkAnim
		end
		if tonumber(input.animations.runAnim) then
			self.animations.runAnim = input.animations.runAnim
		end
		if tonumber(input.animations.attackAnim) then
			self.animations.attackAnim = input.animations.attackAnim
		end
		if tonumber(input.animations.deathAnim) then
			self.animations.deathAnim = input.animations.deathAnim
		end
	end

	if input.particles then
		if tonumber(input.particles.idlePart) then
			self.particles.idlePart = input.particles.idlePart
		end
		if tonumber(input.particles.followPart) then
			self.particles.followPart = input.particles.followPart
		end
		if tonumber(input.particles.chasePart) then
			self.particles.chasePart = input.particles.chasePart
		end
		if tonumber(input.particles.attackPart) then
			self.particles.attackPart = input.particles.attackPart
		end
		if tonumber(input.particles.deathPart) then
			self.particles.deathPart = input.particles.deathPart
		end
	end

	if input.sounds then
		if tonumber(input.sounds.idleSound) then
			self.sounds.idleSound = input.sounds.idleSound

			if self.sounds.idleSound and self.sounds.idleSound > 0 then
				local tSoundParams = GameDefinitions.generateGenericSoundParams(self.PID, self.sounds.idleSound, 20)
				self.idleSoundID = generateSound(tSoundParams)
			end
		end
		if tonumber(input.sounds.followSound) then
			self.sounds.followSound = input.sounds.followSound

			if self.sounds.followSound and self.sounds.followSound > 0 then
				local tSoundParams = GameDefinitions.generateGenericSoundParams(self.PID, self.sounds.followSound, 20)
				self.followSoundID = generateSound(tSoundParams)
			end
		end
		if tonumber(input.sounds.chaseSound) then
			self.sounds.chaseSound = input.sounds.chaseSound

			if self.sounds.chaseSound and self.sounds.chaseSound > 0 then
				local tSoundParams = GameDefinitions.generateGenericSoundParams(self.PID, self.sounds.chaseSound, 20)
				self.chaseSoundID = generateSound(tSoundParams)
			end
		end
		if tonumber(input.sounds.attackSound) then
			self.sounds.attackSound = input.sounds.attackSound

			if self.sounds.attackSound and self.sounds.attackSound > 0 then
				local tSoundParams = GameDefinitions.generateGenericSoundParams(self.PID, self.sounds.attackSound, 20)
				self.attackSoundID = generateSound(tSoundParams)
			end
		end
		if tonumber(input.sounds.deathSound) then
			self.sounds.deathSound = input.sounds.deathSound

			if self.sounds.deathSound and self.sounds.deathSound > 0 then
				local tSoundParams = GameDefinitions.generateGenericSoundParams(self.PID, self.sounds.deathSound, 20)
				self.deathSoundID = generateSound(tSoundParams)
			end
		end
	end	

	self.master = nil
	self.moving = false

	self:setCollision(false)
	self:setDrawDistance(DEFAULT_DRAW_DISTANCE)

	local ownerInfo = kgp.objectGetOwner(self.PID)
	self.ownerName = ""
	if ownerInfo then
   		self.ownerName = ownerInfo.ownerName
	end

	self.modeLabels = nil
	
	
	Events.registerHandler("OBJECT_REMOVED_FROM_BATTLE", self.objectRemovedFromBattle, self)
	Events.registerHandler("OBJECT_DAMAGED", self.onObjectDamaged, self)
	Events.registerHandler("OBJECT_DIED", self.onObjectDied, self)
	Events.registerHandler("OBJECT_PICKUP", self.onObjectPickup, self)
	Events.registerHandler("PLAYER_DAMAGED", self.onPlayerDamaged, self)
	Events.registerHandler("PLAYER_DIED", self.onPlayerDied, self)	
	Events.registerHandler("PLAYER-DEPART", self.onDepart, self)
	Events.registerHandler("PET_FOLLOWING", self.onPetFollowing, self)
	Events.registerHandler("UPDATE_PET_STATE", self.onUpdatePetState, self)
	Events.registerHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)
	Events.registerHandler("PLAYER-ARRIVE", self.onPlayerArrivePet, self)


	-- handler for feeding the pet 
	Events.registerHandler("GENERIC_USE", self.onGenericUse, self)
	
end

function onServerSpindown(self)
	destroy(self)
end


function destroy(self)
	_super.destroy(self)
	
	if self.state and self.state ~= STATE.DEAD then
		if self.state == STATE.FOLLOW then
			kgp.gameItemSetPosRot(self.PID, self.position.x, self.position.y, self.position.z, self.position.rx, self.position.ry, self.position.rz)
		else
			kgp.gameItemSetPosRot(self.PID, self.home.x, self.home.y, self.home.z, self.position.rx, self.position.ry, self.position.rz)
		end
		self:savePet()
	end
	
	Events.unregisterHandler("PLAYER-ARRIVE", self.onPlayerArrivePet, self)
	Events.unregisterHandler("OBJECT_DAMAGED", self.onObjectDamaged, self)
	Events.unregisterHandler("OBJECT_DIED", self.onObjectDied, self)
	Events.unregisterHandler("OBJECT_PICKUP", self.onObjectPickup, self)
	Events.unregisterHandler("PLAYER_DAMAGED", self.onPlayerDamaged, self)
	Events.unregisterHandler("PLAYER_DIED", self.onPlayerDied, self)	
	Events.unregisterHandler("PLAYER-DEPART", self.onDepart, self)
	Events.unregisterHandler("PET_FOLLOWING", self.onPetFollowing, self)
	Events.unregisterHandler("UPDATE_PET_STATE", self.onUpdatePetState, self)
	Events.unregisterHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)
	Events.unregisterHandler("GENERIC_USE", self.onGenericUse, self)
	Events.unregisterHandler("OBJECT_REMOVED_FROM_BATTLE", self.objectRemovedFromBattle, self)

	
	
	if self.idleSoundID then deleteSound({soundID = self.idleSoundID}) end
	if self.followSoundID then deleteSound({soundID = self.followSoundID}) end
	if self.chaseSoundID then deleteSound({soundID = self.chaseSoundID}) end
	if self.attackSoundID then deleteSound({soundID = self.attackSoundID}) end
	if self.deathSoundID then deleteSound({soundID = self.deathSoundID}) end
end

function onRegistered(self, event)
	if event.PID == self.PID and self.registered == false then
		self.registered = true
		self:loadPet()
	end
	if event.PID == self.PID then
		self:updateLabels()
	end
end 

function savePet(self)
	local petData = {}
	petData.state = self.state

	if self.requiresFood and self.requiresFood == true then
		petData.lastFedDate = self.lastFedDate
	else -- Don't starve if disabled
		petData.lastFedDate = self.currentTime
	end
		
	petData.maxHealth = self.maxHealth
	petData.levelMultiplier = self.levelMultiplier
	
	-- encode the data and save it
	SaveLoad.saveGameData(PET_KEY.. "_".. tostring(self.PID), Events.encode(petData))
end

function loadPet(self)
	-- set the current time 
	self.currentTime = (kgp.gameGetCurrentTime()) or 0 -- absolute time in seconds

	-- get the data from the 
	local petDataString = SaveLoad.loadGameData(PET_KEY.. "_".. tostring(self.PID))

	local petData = {}

	-- decode the string 
	if tonumber(petDataString) == nil then
		if petDataString then
			petData = Events.decode(petDataString)
		end 
	else
		petData.state = tonumber(petDataString)
	end 
	
	if petData then 
		-- updating the state information 
		
		local prevState = petData.state or STATE.FOLLOW
		if prevState and (prevState == STATE.STAY or prevState == STATE.WANDER or prevState == STATE.IDLE) then
			self:setState(prevState)
			self.toggleState = prevState
		else
			if self.ownerName and self.ownerName ~= "NoOwner" then
				self:setMaster(SharedData.new( {"player", self.ownerName} ))
			else
				self:setState(STATE.IDLE)			
			end
		
			self.toggleState = STATE.IDLE
		end

		-- get the last saved maxhealth and level multiplier 
		self.maxHealth = petData.maxHealth or self.maxHealth
		self.levelMultiplier = petData.levelMultiplier or self.levelMultiplier

		local petHealth = self.maxHealth
		if self.requiresFood and self.requiresFood == true then
			-- last fed information 
			self.lastFedDate = petData.lastFedDate or self.currentTime
			-- dealth information 
			self.deathDate = self.lastFedDate + (MILISECONDS_PER_DAY * HEALTH_STARVE_DAYS)
			-- calculate the current health
			petHealth = (self.maxHealth) * (1 - (self.currentTime - self.lastFedDate) / (SECONDS_PER_DAY * HEALTH_STARVE_DAYS))
		else -- Don't starve if disabled
			self.lastFedDate = self.currentTime
			self.deathDate = self.lastFedDate + (MILISECONDS_PER_DAY * HEALTH_STARVE_DAYS)
		end

		-- override the health
		self.health = petHealth

		-- order battle to set the health 
		Battle.objectSetHealth({PID = self.PID, health = petHealth})
	else
		-- default state is stay
		self:setState(STATE.STAY)
		self.toggleState = STATE.STAY

		-- dog doesn't have a death date, yet
		self.deathDate = self.currentTime + MILISECONDS_PER_DAY * HEALTH_STARVE_DAYS

		-- we're going to assume your pet has a full belly
		self.lastFedDate = self.currentTime

		-- set the health to max
		Battle.objectSetHealth({PID = self.PID, health = self.maxHealth})

		self.health = self.maxHealth
	end

	
	-- save out a proper JSON (here because the pets were implemented before this data was saved off a JSON)
	self:savePet()

	self:updateLabels()
end

----------------------------------------
-- Class Overrides
----------------------------------------

function newVictim(self, victim)
	if self.combatEnabled then
		self.victim = victim
		self:setState(STATE.CHASE)
	end
end

function setAnimation(self, animation)
	if animation then
		_super.setAnimation(self, animation)
	end
end

function setParticle(self, particle)
	if particle then
		_super.setParticle(self, particle)
	end
end

function playStateSound(self, soundID)
	if soundID then
		playSound({soundID = soundID})
	end
end

----------------------------------------
-- Public Class Methods
----------------------------------------

-- Sets the state for the Pet
function setState(self, state)

	--Lock down if dead (no resurrection)
	if self.state and self.state == STATE.DEAD then return end
	
	if(self.state == nil or state ~= self.state) then
		-- only enter new state if it is different than the current state.
		if state == STATE.IDLE then
			self:enterIdleState()
		elseif state == STATE.WANDER then
			self:enterWanderState()
		elseif state == STATE.STAY then
			self:enterStayState()
		elseif state == STATE.FOLLOW then
			self:enterFollowState()
		elseif state == STATE.RETURNING  then
			self:enterReturningState()	
		elseif state == STATE.CHASE then
			self:enterChaseState()	
		elseif state == STATE.DEAD then
			self:enterDeadState()
		end
	end
end

function updateLabels(self)
	if self == nil then
		return
	end
		
	if self.showLabels then
		self:clearLabels()

		local labels = {}
		
		-- shows the actor's spouse information (if relevant)
		if self.spouseOf ~= nil then
			table.insert(labels, {self.actorName, 0.016})
			table.insert(labels, {"Spouse of "..self.spouseOf, 0.015, 0.9, 0.5, 0.2})
			self.actorTitle = "Spouse of "..self.spouseOf
		elseif self.followerOf ~= nil then
			table.insert(labels, {self.actorName, 0.016})
			table.insert(labels, {"Follower of "..self.followerOf, 0.015, 0.9, 0.5, 0.2})
			self.actorTitle = "Follower of "..self.followerOf
		else
			table.insert(labels, {self.name, 0.013})
			-- shows the pet's owner information
			if self.ownerName ~= "NoOwner" then
				table.insert(labels, {self.ownerName .. "'s Pet", 0.013, 0.9, 0.5, 0.2})
			end
			
			-- show the pet's health 
			if self.health and self.maxHealth and self.requiresFood then
				if self.health < 0 then
					table.insert(labels, {"Health: 0/".. tostring(math.ceil(self.maxHealth)), 0.013, 1.0, 0, 0})
				elseif (self.health / self.maxHealth) <= 0.15 then
					table.insert(labels, {"Health: ".. tostring(math.ceil(self.health)) .. "/".. tostring(math.ceil(self.maxHealth)), 0.013, 1.0, 0, 0})
					if self.foodName then self.tooltip = "Urgently Needs: ".. tostring(self.foodName) end 
				elseif (self.health / self.maxHealth) <= 0.70 then
					table.insert(labels, {"Health: ".. tostring(math.ceil(self.health)) .. "/".. tostring(math.ceil(self.maxHealth)), 0.013, 1.0, 1.0, 0})
					if self.foodName then self.tooltip = "Needs to Eat: ".. tostring(self.foodName) end 
				else
					table.insert(labels, {"Health: ".. tostring(math.ceil(self.health)) .. "/".. tostring(math.ceil(self.maxHealth)), 0.013, 0, 1.0, 0})
					if self.foodName then self.tooltip = "Likes to Eat: ".. tostring(self.foodName) end 
				end
			end

			-- show's the pet's state 
			if self.state == STATE.IDLE or self.state == STATE.WANDER then 
				table.insert(labels, {"Wander", 0.013, 0.2, 0.5, 0.9})
			elseif self.state == STATE.STAY then
				table.insert(labels, {"Stay", 0.013, 0.2, 0.5, 0.9})
			elseif self.state == STATE.FOLLOW then
				table.insert(labels, {"Follow", 0.013, 0.2, 0.5, 0.9})
			end
		end

		if #labels > 0 then
			self:addLabels(labels)
		end
	end
end

-- Enter idle state function
function enterIdleState(self)	
	if self.inMotion then self:stopMovement() end

	self.master = nil

	self.speed = self.walkSpeed	
	self.stateChangeCooldown = WANDER_TIMER

	self:setAnimation(self.animations.idleAnim)
	self:playStateSound(self.idleSoundID)
	self:removeParticle()
	self:setParticle(self.particles.idlePart)

	self.state = STATE.IDLE
	self:handleIdleState(0)

	self:updateLabels()
end

-- Enter wander state function
function enterWanderState(self)	
	if self.inMotion then self:stopMovement() end

	self.master = nil

	self.speed = self.walkSpeed
	self:setWanderLocation()
	self.moving = true

	self:setAnimation(self.animations.walkAnim)

	self.state = STATE.WANDER	
	self:updateLabels()
end




-- Enter stay state function
function enterStayState(self)
	if self.inMotion then self:stopMovement() end

	self.master = nil
	self:setHomeLocation(self.position.x, self.position.y, self.position.z)

	self:setAnimation(self.animations.idleAnim)
	self:playStateSound(self.idleSoundID)
	self:removeParticle()
	self:setParticle(self.particles.idlePart)	

	self.state = STATE.STAY

	self:updateLabels()
end

-- Enter follow state function
function enterFollowState(self)
	-- Can't follow if no master is designated
	if self.master == nil then return end

	if self.inMotion then self:stopMovement() end
	
	self.victim = nil
	self.speed = self.runSpeed

	local input = {}
	input.target = self.master
	input.buffer = 1
	input.offset = SLOT_OFFSETS[1]	
	self:chase(input)

	self.moving = true

	self:playStateSound(self.followSoundID)
	self:removeParticle()
	self:setParticle(self.particles.followPart)

	self.state = STATE.FOLLOW
	self:handleFollowState()

	self:updateLabels()
end

-- Enter returning state function
function enterReturningState(self)	
	if self.inMotion then self:stopMovement() end

	self.master = nil
	self.victim = nil

	self.speed = self.runSpeed
	local input = {x = self.home.x, y = self.home.y, z = self.home.z, lookAt = true, rx = self.position.rx, ry = self.position.ry, rz = self.position.rz}
	self:moveTo(input)
	self.moving = true

	self:setAnimation(self.animations.runAnim)

	self.state = STATE.RETURNING
end

-- Enter chase state function
function enterChaseState(self)
	-- Can't chase if no master is designated
	if self.victim == nil then return end
	
	if self.inMotion then self:stopMovement() end		

	self.speed = self.runSpeed

	local input = {}
	input.target = self.victim
	input.buffer = 3	
	self:chase(input)

	self.moving = true

	self:playStateSound(self.chaseSoundID)
	self:removeParticle()
	self:setParticle(self.particles.chasePart)

	self.state = STATE.CHASE
	self:handleChaseState(0)
end

-- Enter dead state function
function enterDeadState(self)
	if self.inMotion then self:stopMovement() end

	self.victim = nil
	self.master = nil
	self.agroTable = {}	

	self:setAnimation(self.animations.deathAnim)
	self:playStateSound(self.deathSoundID)
	self:removeParticle()
	self:setParticle(self.particles.deathPart)

	self.state = STATE.DEAD
	Events.setTimer( DEATH_ANIM_TIME, 	function ()
											SaveLoad.deleteGameData(PET_KEY.."_"..tostring(self.PID))
											self:kill()
										end )
end

-- Handles Pet's state machine Override's parents (NPC)
function handleState(self, deltaTick)
	if self.state == STATE.IDLE then
		self:handleIdleState(deltaTick)
	elseif self.state == STATE.STAY then
		-- self:enterStayState()
	elseif self.state == STATE.FOLLOW then
		self:handleFollowState()
	elseif self.state == STATE.CHASE then
		self:handleChaseState(deltaTick)
	end
end

function handleIdleState(self, deltaTick)
	self.stateChangeCooldown = self.stateChangeCooldown - deltaTick

	if self.stateChangeCooldown <= 0 then
		self:setState(STATE.WANDER)
	end
end

-- Overrides parent Chase state
function handleFollowState(self)
	-- Check the distance from us to our destination
	-- local distToDest = self:getDistanceToPoint(self.destination)
	local distToDest = 0
	if self.chaseTarget ~= 0 and (type(self.chaseTarget) == "table" or type(self.chaseTarget) == "userdata") then
		distToDest = self:getDistanceToPoint(self.chaseTarget.position)
	end

	if distToDest <= SIT_THRESHOLD then
		self:setAnimation(self.animations.idleAnim)
	elseif distToDest <= WALK_THRESHOLD then
		-- Walk animation
		self:setAnimation(self.animations.walkAnim)
		self.speed = self.walkSpeed
	else
		-- Running animation
		self:setAnimation(self.animations.runAnim)
		self.speed = self.runSpeed
	end
end

-- Overrides parent Chase state
function handleChaseState(self, deltaTick)

	if self.victim == nil then return end
	-- Check the distance from us to our target
	local distToTarget = self:getDistanceToPoint(self.victim.position)

	if self.victim == nil and Toolbox.isContainerEmpty(self.agroTable) then
		if self.master then
			self:setState(STATE.FOLLOW)
		else
			self:setState(STATE.RETURNING)
		end
		return
	end

	if self.victim and not self.victim.pvpEnabled and not self.victim.PID then
		self.agroTable[tostring(self.victim.name)] = nil
		if self.master then
			self:setState(STATE.FOLLOW)
		else
			self:setState(STATE.RETURNING)
		end
		return
	end

	-- If we're close enough to the target
	if distToTarget <= self.attackRange then
		self:setAnimation(self.animations.attackAnim)
		self:setParticle(self.particles.attackPart)
		self:playStateSound(self.attackSoundID)
	else
		self:setAnimation(self.animations.runAnim)
	end

	self.attackTimer = self.attackTimer - deltaTick

	if distToTarget <= self.attackRange and self.attackTimer <= 0 and self.victim and self.victim.alive then
		self.attackTimer = ATTACK_TIME
		self:attack({attacked = self.victim})
		-- Attacked victim gets a threat floor
		self:setThreatFloor(self.victim, 20)
	elseif self.victim and not self.victim.alive then
		if self.victim.PID then
			self.agroTable[tostring(self.victim.PID)] = nil
		else
			self.agroTable[tostring(self.victim.name)] = nil
		end
		if Toolbox.isContainerEmpty(self.agroTable) then
			if self.master then
				self:setState(STATE.FOLLOW)
			else
				self:setState(STATE.RETURNING)
			end
		else
			self:evaluateAgroTable()
		end
	end
end

-- Wrapper for onGenericUse
function onGenericUse(self, event)

	-- is this the correct target?
	if event.target == self.PID  then
		--log("--- onGenericUse event.weapon = ".. tostring(event.weapon).. " self.food = ".. tostring(self.food))
		-- if event.weapon == self.food 
		if event.weapon == self.food then
			--log("--- onGenericUse accepted")
			if event.attacker and event.attacker.ID then
				Events.sendClientEvent("FRAMEWORK_GENERIC_ACCEPTED", {weapon = event.weapon, accepted = true, errorType = "none", food = self.food}, event.attacker.ID)
			end

			if self.health and self.maxHealth then
				-- update the health 
				Battle.objectAddHealth({PID = self.PID, health = self.maxHealth, maxHealth = self.maxHealth})
				self.lastFedDate = (kgp.gameGetCurrentTime()) or 0
				self.deathDate = self.lastFedDate + HEALTH_STARVE_DAYS * MILISECONDS_PER_DAY
				self:savePet()

				self.health = self.maxHealth

				self:updateLabels()
			end 
		end
	end
end

----------------------------------------
-- Event handlers
----------------------------------------

-- Player depart handler
function onDepart(self, event)
	self.onPlayerDepart(self, event)
end

-- Right click handler
function onRightClick(self, event)
	local uPlayer = event.player
	if uPlayer == nil then return end
	-- Activate actor
	self:preActivate(event)
end

-- Left click handler
function onLeftClick(self, event)
	local uPlayer = event.player
	if uPlayer == nil then return end
	local handEquipped = (event.player.handEquipped == true or event.player.handEquipped == nil)
	-- If the player is not trying to feed the pet
	if handEquipped then
		-- Activate pet
		--if self:preActivate(event) then
		self:preActivate(event)
		--end
	end
end

-- Timer handler
function onTimer(self, event)
	self.onServerPetTick(self, event)
end

----------------------------------------
-- Event handler Wrappers
----------------------------------------

-- When a a player enters the zone
function onPlayerArrivePet(self, event)
	self:updateLabels()
end

-- Wrapper for onDepart
function onPlayerDepart(self, event)
	
	if self.master and event.player and self.master.name == event.player.name then
		--self:setState(STATE.STAY)
	elseif self.victim and event.player and self.victim.name == event.player.name then
		if self.victim.name == event.player.name then
			self.victim = nil
		end

		self.agroTable[tostring(event.player.name)] = nil

		if Toolbox.isContainerEmpty(self.agroTable) then
			self:setState(STATE.FOLLOW)
		else
			self:evaluateAgroTable()
		end
	end
end

-- Validate all conditions necessary before onPlayerActivate
function preActivate(self, event)
	local uPlayer = event.player
	if uPlayer == nil then
		return false
	end

	if uPlayer.name == self.ownerName then
		Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)
		uPlayer.activeMenuPID = self.PID
		kgp.playerLoadMenu(uPlayer.ID, "Framework_NPCActionMenu.xml")
		local eventProperties = {
					actorPID = self.PID,
					UNID = self.UNID
				}
		Events.sendClientEvent("FRAMEWORK_DEFINE_NPC_ACTIONS", eventProperties, uPlayer.ID)
		return true
	end
	return false
end

-- Wrapper for onRightClick
--This doesn't seem to be used anymore with new NPCAction menu
function onPlayerActivate(self, event)
	local uPlayer = event.player
	if uPlayer == nil then return end

	-- Cycle state
	if self.state == STATE.IDLE then
		self:setMaster(uPlayer)
		self:setState(STATE.FOLLOW)
		self.toggleState = STATE.FOLLOW
	elseif self.state == STATE.WANDER then
		self:setMaster(uPlayer)
		self:setState(STATE.FOLLOW)
		self.toggleState = STATE.FOLLOW
	elseif self.state == STATE.STAY then
	--	no setMaster
		self:setState(STATE.WANDER)
		self.toggleState = STATE.WANDER
	elseif self.state == STATE.FOLLOW then
	--  no setMaster
		self:setState(STATE.STAY)
		self.toggleState = STATE.STAY
	elseif self.state == STATE.CHASE then
		self:clearAgroTable()
		self:setMaster(uPlayer)
		self:setState(STATE.FOLLOW)
		self.toggleState = STATE.FOLLOW
	end
end

function onUpdatePetState(self, event)
	local uPlayer = event.player
	local iNewState = event.stateChange
	local iPID = event.PID
	if uPlayer == nil then return end
	if iNewState == nil then return end
	if iPID == nil then return end
	
	
	if iPID == self.PID then

		-- Update info based on previous state
		if self.state == STATE.CHASE then
			self:clearAgroTable()
		end

		-- Set the new state
		if iNewState == STATE.STAY and self.state ~= STATE.STAY then
			self:setState(STATE.STAY)
			self.toggleState = STATE.STAY
			-- no setMaster
		elseif iNewState == STATE.FOLLOW and self.state ~= STATE.FOLLOW then
			self:setState(STATE.FOLLOW)
			self.toggleState = STATE.FOLLOW
			self:setMaster(uPlayer)
		elseif iNewState == STATE.WANDER and self.state ~= STATE.WANDER then
			self:setState(STATE.WANDER)
			self.toggleState = STATE.WANDER
			-- no setMaster
		end

	end	
end

-- Wrapper for onTimer
function onServerPetTick(self, event)
	if self.tick == 0 and self.followerOf == nil then
		self.tick = event.tick -- Only update local tick if NOT a follower pet (Class_ActorDefinition will do it first)
	end 
	local deltaTick = event.tick - self.tick
	self:handleState(deltaTick)
	self:regenerateHealth(deltaTick)
	self:starve(deltaTick)
	if self.followerOf == nil then
		self.tick = event.tick -- Only update local tick if NOT a follower pet (Class_ActorDefinition will do it first)
	end
end

----------------------------------------
-- Behavior Events
----------------------------------------

-- Called when the pet arrives at its destination
function onMovementArrived(self, event)
	self.moving = false
	if self.state == STATE.WANDER then
		self:setState(STATE.IDLE)
	elseif self.state == STATE.RETURNING then
		local newState = self.toggleState or STATE.STAY
		self:setState(newState)
	end
end

-- Called when the monster is damaged
function onObjectDamaged(self, event)
	if event.attacker and event.attacked and canAttackType(event.attacked.type) then
		--Is my master attacking?
		if self.master and event.attacker.type == "Player" and event.attacker.name == self.master.name and event.attacked.PID ~= self.PID then
			self:setThreatFloor(event.attacked, 20)		
			self:addThreat(event.attacked, 50)
			self:evaluateAgroTable()
		--Am I being attacked?
		elseif event.attacked.PID == self.PID then
			self:setThreatFloor(event.attacker, 20)		
			self:addThreat(event.attacker, 25)
			self:evaluateAgroTable()			
		end

		self:updateLabels()
	end
end

-- Object has been removed from battle (A monster could have been destroyed without dying, objects leaving, etc)
-- 
function objectRemovedFromBattle(self, event)

	if(event.object.PID == self.PID) then
		return
	end
	
	if not Toolbox.isContainerEmpty(self.agroTable) then
		if self.victim and event.object.PID == self.victim.PID then
			self.victim = nil
		end

		if self.agroTable[tostring(event.object.PID)] then
			self.agroTable[tostring(event.object.PID)] = nil
		end
		
		-- If the agroTable was not empty and now is after removing the object,
		-- then we must update state (can only have things in agroTable on FOLLOW)
		if Toolbox.isContainerEmpty(self.agroTable) then
			if self.master then
				self:setState(STATE.FOLLOW)
			else
				self:setState(STATE.RETURNING)
			end			
		else
			self:evaluateAgroTable()
		end
	end
end





-- Called when an object dies
function onObjectDied(self, event)
	-- Who just died?
	if (event.object.PID == self.PID) and self.state ~= STATE.DEAD then
		self:setState(STATE.DEAD)
		return
	end

	if not Toolbox.isContainerEmpty(self.agroTable) then
		if self.victim and event.object.PID == self.victim.PID then
			self.victim = nil
		end

		if self.agroTable[tostring(event.object.PID)] then
			self.agroTable[tostring(event.object.PID)] = nil
		end

		if Toolbox.isContainerEmpty(self.agroTable) then
			if self.master then
				self:setState(STATE.FOLLOW)
			else
				self:setState(STATE.RETURNING)
			end			
		else
			self:evaluateAgroTable()
		end
	end
end

-- Called when an object gets picked up
function onObjectPickup(self, event)
	-- Did we get picked up?
	if (event.PID == self.PID) then
		self.state = STATE.DEAD
		SaveLoad.deleteGameData(PET_KEY.."_"..tostring(self.PID))
	end
end

-- Called when a player is damaged
function onPlayerDamaged(self, event)
	-- Do I have a master?
	if self.master and event.attacked and event.attacker and canAttackType(event.attacker.type) and event.damage and event.damage > 0 then
		-- Is my master getting attacked?
		if event.attacked.name == self.master.name then
			self:setThreatFloor(event.attacker, 20)		
			self:addThreat(event.attacker, 50)
			self:evaluateAgroTable()
		-- Is my master attacking?
		elseif event.attacker.name == self.master.name then
			self:setThreatFloor(event.attacked, 20)		
			self:addThreat(event.attacked, 75)
			self:evaluateAgroTable()
		end
	end
end

function onPlayerDied( self, event )
	-- Who just died?
	if event.player ~= nil and event.player.name then	
		if self.victim and self.victim.name == event.player.name then
			self.victim = nil
		end

		if self.master and self.master.name == event.player.name then
			self.master = nil
		end

		if not Toolbox.isContainerEmpty(self.agroTable) then
			if self.agroTable[event.player.name] then
				self.agroTable[tostring(event.player.name)] = nil
			end

			if Toolbox.isContainerEmpty(self.agroTable) then
				if self.master then
					self:setState(STATE.FOLLOW)
				else
					if self.state == STATE.CHASE then
						self:setState(STATE.RETURNING)
					elseif self.state == STATE.FOLLOW then
						self:setHomeLocation(self.position.x, self.position.y, self.position.z)
						self:setState(STATE.IDLE)
					end
				end			
			else
				self:evaluateAgroTable()
			end
		else
			if self.master then
				self:setState(STATE.FOLLOW)
			else
				if self.state == STATE.CHASE then
					self:setState(STATE.RETURNING)
				elseif self.state == STATE.FOLLOW then
					self:setState(STATE.IDLE)
				end
			end	
		end
	end
end

function onPetFollowing(self, event)
	if self.master and self.ownerName and self.ownerName == event.owner and self.master.name == event.owner then
		if event.PID ~= self.PID then
			-- A pet with a different pid is now following this pets master, switch to STAY
			self:setState(STATE.STAY)
		else
			-- This pet is now following this pets master, switch to FOLLOW
			self:setState(STATE.FOLLOW)
		end
	end
end
	
-- Called when a player changes modesi
function onPlayerModeChanged(self, event)

	-- Ensure this player wasn't our target
	if self.state ~= STATE.DYING and event.mode == "God" and event.player ~= nil and event.player.name then
		if self.agroTable[event.player.name] then
			self.agroTable[event.player.ID] = nil
		end

		if Toolbox.isContainerEmpty(self.agroTable) then
			if(self.state == STATE.FOLLOW and not self.master) then
				self:setState(STATE.STAY)
			end
		else
			self:evaluateAgroTable()
		end
	end
end

----------------------------------------
-- local functions
----------------------------------------

function setMaster(self, player)
	if player and ( self.master == nil or self.master.name ~= player.name ) then
		self.master = player		
		Events.sendEvent("PET_FOLLOWING", {owner = self.ownerName, PID = self.PID})
	end
end

function regenerateHealth(self, deltaTick)
	-- don't regenerate if it requires food. 
	--log("--- self.requiresFood = "..tostring(self.requiresFood))
	if self.requiresFood and self.requiresFood == true then 
		return
	end 

	--log("--- regenerateHealth")

	if self.health and self.maxHealth then
		if self.state ~= STATE.DEAD and self.health < self.maxHealth then
			self.regenTimer = self.regenTimer - deltaTick
			if self.regenTimer <= 0 then
				local healthGained = self.maxHealth/10 --10% of health returned
				local input = {}
				input.PID = self.PID
				input.health = healthGained
				input.healer = self.master or self._dictionary
				Battle.objectAddHealth(input)
				self.regenTimer = HEALTH_REGEN_TICK
			end
		end
	end
end

-- removes health from the pet 
function starve( self, deltaTick )
	-- log("--- starve me")

	-- if the pet is dead or doesn't require food, ignore this 
	if self.state ~= STATE.DEAD and (self.requiresFood and self.requiresFood == true) then 

		-- if the pet has a health and a max health 
		if self.health and self.maxHealth then
			self.starveTimer = self.starveTimer - deltaTick
			
			-- time to stave the pet a little 
			if self.starveTimer <= 0 then
				-- set the current time 
				self.currentTime = (kgp.gameGetCurrentTime()) or 0 -- absolute time in seconds

				local newHealth = (self.maxHealth) * ((HEALTH_STARVE_TICK / MILISECONDS_PER_SECOND) / (SECONDS_PER_DAY * HEALTH_STARVE_DAYS))
				-- log("--- PET DATA FOR ".. tostring(self.PID).. "--- \n\tlastFedDate : ".. tostring(self.lastFedDate).. "\n\tcurrentTime : ".. tostring(self.currentTime))
				-- log("--- PET DATA FOR ".. tostring(self.PID).. "--- \n\tnewHealth : ".. tostring(newHealth))

				-- update the health through battle handler 
				Battle.objectRemoveHealth({PID = self.PID, health = newHealth})

				-- reset the starve timer 
				self.starveTimer = HEALTH_STARVE_TICK

				-- reduce the lag on the health updating 
				--self.health = newHealth

				-- save the pet in its current state 
				self:savePet()

				-- update the labels on the pet 
				self:updateLabels()
			end
		end
	end
end

function setHomeLocation(self, x, y, z)
	self.home.x = x
	self.home.y = y
	self.home.z = z
end

function setWanderLocation(self)
	local wanderPoint = {}
	wanderPoint.x = math.random((self.home.x - self.wanderRange), (self.home.x + self.wanderRange))
	wanderPoint.y = self.home.y
	wanderPoint.z = math.random((self.home.z - self.wanderRange), (self.home.z + self.wanderRange))

	local input = {x = wanderPoint.x, y = wanderPoint.y, z = wanderPoint.z, lookAt = true, rx = self.position.rx, ry = self.position.ry, rz = self.position.rz}
	self:moveTo(input)
end

function canAttackType(classType)
	if classType and (classType == "Pet" or classType == "Animal" or classType == "Monster" or classType == "Player" or classType == "Actor" or classType == "Vendor" or classType == "QuestGiver") then
		return true
	end

	return false
end