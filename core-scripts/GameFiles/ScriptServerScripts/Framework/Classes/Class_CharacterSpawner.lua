--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

Class.createClass("Class_CharacterSpawner", "Class_Spawner")

local MAX_CHARACTER_COUNT = 10
local DEFAULT_CHARACTER_GLID = 4195001
local DEFAULT_ANIMATION = 4465636
local DEFAULT_CHARACTER_TYPE = "monster"

function create(self, input)
	input.spawnType = input.spawnType or "monster"
	input.spawnGLID = input.spawnGLID or DEFAULT_CHARACTER_GLID
	if input.maxSpawns and input.maxSpawns > MAX_CHARACTER_COUNT then input.maxSpawns = MAX_CHARACTER_COUNT end
	_super.create(self, input)
	
	self:setAnimation(DEFAULT_ANIMATION)
end	