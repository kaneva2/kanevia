--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_Monster.lua
--
-- Monster class inherited from NPC
--
-- User-definable variables
-- STRING
-- NUMBER
-- TABLE
--
-- User-accessible methods
--
---------------------------------------


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Events			= _G.Events
local Class				= _G.Class
local Players			= _G.Players

-----------------------------------------
-- Class specification
-----------------------------------------
Class.createClass("Class_Animal", "Class_Character")

Class.createProperties({
	"spawnerPID",
	"spawnGLID"
})


-----------------------------------------------------------------
-- Constant Definitions
-----------------------------------------------------------------
local DEFAULT_IDLE_ANIMATION  = 0 --TODO: Need chicken animations
local DEFAULT_MOVE_ANIMATION  = 4478310
local DEFAULT_DEATH_ANIMATION = 0 --TODO: Need chicken animations
local DEFAULT_SPAWN_GLID = 4465627

local DEFAULT_NAME = "Chicken"

local DEFAULT_WALK_SPEED = 1
local DEFAULT_RUN_SPEED  = 12

local LEASH_DISTANCE = 75

local DEATH_ANIMATION_TIME = 0 --TODO: Adjust for final asset

local DEFAULT_WANDER_RANGE = 25
local DEFAULT_WANDER_TIME = 5000

local FLEE_DISTANCE = 30

-- States for NPC
local STATE = {
	IDLE    = 0,
	WANDER = 1,
	FLEEING = 2,
	RETURNING = 4,
	DYING = 5,
}

s_type = "Animal"



-- Instantiate an instance of NPC
function create(self, input)
	-- TODO: Error check input values
	if input == nil then input = {} end
	input.targetable = "true"
	_super.create(self, input)
	
	-- Old static members
	self.tick = 0
	self.stateChangeCooldown = 0

	self.eulerRotX = 0
	self.eulerRotY = input.eulerRotY or 0
	self.eulerRotZ = 0
	
	-- User-definable variables (Public, document for users)
	self.name = input.name  or DEFAULT_NAME
	self.walkSpeed = input.walkSpeed or DEFAULT_WALK_SPEED
	self.runSpeed = input.runSpeed or DEFAULT_RUN_SPEED

	self.speed = self.walkSpeed
	
	self.spawnerPID = input.spawnerPID
	self.spawnGLID = input.spawnGLID or DEFAULT_SPAWN_GLID

	self.wanderRange = input.wanderRange or DEFAULT_WANDER_RANGE

	self.animations = {}
	self.animations.idle = DEFAULT_IDLE_ANIMATION
	self.animations.moving = DEFAULT_MOVE_ANIMATION
	self.animations.death = DEFAULT_DEATH_ANIMATION

	if input.animations then
		if input.animations.idle then self.animations.idle = input.animations.idle or DEFAULT_IDLE_ANIMATION end
		if input.animations.moving then self.animations.moving = input.animations.moving or DEFAULT_MOVE_ANIMATION end
		if input.animations.death then self.animations.death = input.animations.death or DEFAULT_DEATH_ANIMATION end
	end

	self.target = nil
	self:setState(STATE.WANDER)

	--Locks Y position of animal
	self.lockYPosition = true

	self:setCollision(false)

	Events.registerHandler("OBJECT_HEALED", self.onHealed, self)
	Events.registerHandler("OBJECT_DAMAGED", self.onDamage, self)
	Events.registerHandler("PLAYER_DIED", self.onPlayerDied, self)
	Events.registerHandler("OBJECT_DIED", self.onObjectDied, self)
	
	Events.registerHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)
	--self:setMouseOver(5)
	
	-- Register for health bars
	self:enableHealthBar({})
	
	self.fleeTrigger = Events.setTrigger(FLEE_DISTANCE, self.onTrigger, self)
end

function destroy(self)
	_super.destroy(self)
	
	Events.unregisterHandler("OBJECT_HEALED", self.onHealed, self)
	Events.unregisterHandler("OBJECT_DAMAGED", self.onDamage, self)
	Events.unregisterHandler("PLAYER_DIED", self.onPlayerDied, self)
	Events.unregisterHandler("OBJECT_DIED", self.onObjectDied, self)
	Events.unregisterHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)
	if self.fleeTrigger then
		Events.removeTrigger(self.fleeTrigger)
	end
end

-- Sets the state for the Monster
function setState(self, state)
	if state == self.state then return end
	
	if self.inMotion == true then
		self:stopMovement()
	end	
	
	if state == STATE.IDLE then
		self:enterIdleState(self)
	elseif state == STATE.WANDER then
		self:enterWanderState(self)
	elseif state == STATE.FLEE then
		self:enterFleeState(self)
	elseif state == STATE.RETURNING then
		self:enterReturningState(self)
	elseif state == STATE.DYING then
		self:enterDyingState(self)
	end
end

function enterIdleState(self)
	self.state = STATE.IDLE
	self:setAnimation(self.animations.idle)
	self.stateChangeCooldown = DEFAULT_WANDER_TIME
	self:show()
end

function enterWanderState(self)
	self.state = STATE.WANDER
	self.speed = self.walkSpeed
	self:setAnimation(self.animations.moving)
	self:setWanderLocation()
	self:show()
end

function enterFleeState(self)
	if self.target == nil then return end
	self.state = STATE.FLEE
	self.speed = self.runSpeed
	self:setAnimation(self.animations.moving)

	self:flee({target = self.target})
	self:show()
end

function enterReturningState(self)
	self.state = STATE.RETURNING
	self:setAnimation(self.animations.moving)

	local input = {x = self.home.x, y = self.home.y, z = self.home.z, lookAt = true, rx = self.position.rx, ry = self.position.ry, rz = self.position.rz}
	self:moveTo(input)
	self:show()
end

function enterDyingState(self)
	self.state = STATE.DYING
	self:setAnimation(self.animations.death)

	Events.setTimer( DEATH_ANIMATION_TIME, function () self:despawn() end )
end

function handleState(self, deltaTick)

	if self.state == STATE.IDLE then
		self:handleIdleState(deltaTick)
	elseif self.state == STATE.WANDER then
	elseif self.state == STATE.FLEE then
		self:handleFleeingState(deltaTick)
	elseif self.state == STATE.RETURNING then
	end
end

function handleIdleState(self, deltaTick)
	if self.stateChangeCooldown <= 0 then
		self:setState(STATE.WANDER)
	end
end

function handleFleeingState(self, deltaTick)
	if self.target == nil then return end
	-- Check the distance from us to our target
	local distToTarget = self:getDistanceToPoint(self.target.position)
	local distFromHome = self:getDistanceToPoint(self.home)

	if distFromHome >= LEASH_DISTANCE then
		self:setState(STATE.RETURNING)
		return
	end
end

function setWanderLocation(self)
	local wanderPoint = {}
	wanderPoint.x = math.random((self.home.x - self.wanderRange), (self.home.x + self.wanderRange))
	wanderPoint.y = self.home.y
	wanderPoint.z = math.random((self.home.z - self.wanderRange), (self.home.z + self.wanderRange))

	local input = {x = wanderPoint.x, y = wanderPoint.y, z = wanderPoint.z, lookAt = true, rx = self.position.rx, ry = self.position.ry, rz = self.position.rz}
	self:moveTo(input)
end

function despawn(self)
	if self.spawnerPID then
		Events.sendEvent("OBJECT_DESPAWN", {spawnerPID = self.spawnerPID, despawnPID = self.PID, spawnType = "animal"})	
	end
end

----------------------------------------
-- Event handlers
----------------------------------------

-- Timer handler
function onTimer(self, event)
	self.onServerTick(self, event)
end

-- Wrapper for onTimer
function onServerTick(self, event)
	if self.tick == 0 then self.tick = event.tick end
	local deltaTick = event.tick - self.tick
	self.stateChangeCooldown = self.stateChangeCooldown - deltaTick
	self:handleState(deltaTick)
	self.tick = event.tick
end

-- Called when the monster arrives at its destination
function onMovementArrived(self, event)
	if self.state == STATE.WANDER then
		self:setState(STATE.IDLE)
	elseif self.state == STATE.RETURNING then
		self:setState(STATE.IDLE)
	end
end

function onTrigger(self, event)
	if (self.state == STATE.IDLE or self.state == STATE.WANDER) and
	   (event.triggerType == 1) and
	   (event.player ~= nil) and
	   event.player.alive and 
	   (event.player.mode ~= "God") then
		self.target = event.player
		self:setState(STATE.FLEE)
	end
end

-- Called when the monster is healed
function onHealed(self, event)
	-- TODO: Un-needed I suppose. I don't reward getting healed.
end

-- Called when the monster is damaged
function onDamage(self, event)
	-- Was I attacked?
	if event.attacked and event.attacked.PID and event.attacked.PID == self.PID then
		-- WHO ATTACKED ME!?
		if (self.state ~= STATE.CHASING) and (self.state ~= STATE.RETURNING) and (self.state ~= STATE.DYING) and event.attacker then
			-- RUN AWAY!
			self.target = event.attacker
			self:setState(STATE.FLEE)
		end
	end
end

function onPlayerDied( self, event )
	if event.player and Players.validatePlayerName(event.player.name) and self.target and (event.player.name == self.target.name) then
		self:setState(STATE.RETURNING)
	end
end

-- Called when an object dies
function onObjectDied(self, event)
	-- Did I just die?
	if (event.object.PID == self.PID) then
		self:setState(STATE.DYING)
	end
end

-- Called when the player changes mode
function onPlayerModeChanged(self, event)
	-- If this player was our target
	if self.target and event.player and (event.player.ID == self.target.ID) and (event.mode == "God") then
		-- Clear target
		self:setState(STATE.RETURNING)
	end
end