--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- Class_QuestGiver.lua
-- Displays quests for users
----------------------------------------

include("Lib_NpcDefinitions.lua")
local NpcDefs = _G.NpcDefinitions

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Events			= _G.Events
local Players			= _G.Players

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
Class.createProperties({
	"quests"
})

----------------------------------------
-- Constant Definitions
----------------------------------------

local DEFAULT_QUESTS = {}
s_type = "QuestGiver"

local DEFAULT_INTERACTION_RANGE = 25
local BARK_MODE_RADIUS 		=  1
local BARK_MODE_NORMAL 		=  0
local BARK_MODE_INFINITE 	= -1

-----------------------------------------------------------------
-- Static variable definitions
-----------------------------------------------------------------

local s_uItemProperties = nil

----------------------------------------
-- Private Methods
----------------------------------------

-- Instantiate an instance of the Quest Giver class
function create(self, input)
	if input == nil then input = {} end
	
	_super.create(self, input)

	-- Create a read-only global reference to the "item properties" shared data.
	-- Note that each "Class" instance created within this thread has access to this data.
	s_uItemProperties = SharedData.new("gameItemProperties", true)
	
	-- Quests assigned to this Quest Giver
	self.quests = DEFAULT_QUESTS

	-- Quests passed in?
	self.greetingImage = input.greetingImage or nil
	if Toolbox.isContainer(input.quests) then
		for index, questUNID in pairs(input.quests) do
			self.quests[index] = questUNID
			-- Get the quest item and its bark range
			local questItem = s_uItemProperties[tostring(questUNID)]
		end
	end

	Events.registerHandler("ACTIVATE_ACTOR", self.activateActorHandler, self)
	Events.registerHandler("QUEST_HANDLER_READY", self.onQuestHandlerReady, self) -- Register players that enter the zone
	Events.registerHandler("FRAMEWORK_QUEST_GAME_ITEM_EDITED", self.onQuestItemEdit, self) -- Update triggers if quest item is changed
	Events.registerHandler("QUEST_COMPLETE_CHECK_NEXT", self.onQuestCompleteCheckNext, self)
end

-- Called on destroy
function destroy(self)
	Events.unregisterHandler("ACTIVATE_ACTOR", self.activateActorHandler, self)
	Events.unregisterHandler("QUEST_HANDLER_READY", self.onQuestHandlerReady, self)
	Events.unregisterHandler("FRAMEWORK_QUEST_GAME_ITEM_EDITED", self.onQuestItemEdit, self)
	Events.unregisterHandler("QUEST_COMPLETE_CHECK_NEXT", self.onQuestCompleteCheckNext, self)
	_super.destroy(self)
	
	-- TODO: Un-register event handlers
end

----------------------------------------
-- Event handlers
----------------------------------------

-- Right click handler
function onRightClick(self, event)
	log("Class_QuestGiver - onRightClick")
	local uPlayer = event.player
	if uPlayer == nil then return end
	-- Activate actor
	self:preActivate(event)
end

-- Left click handler
function onLeftClick(self, event)
	log("Class_QuestGiver - onLeftClick")
	local uPlayer = event.player
	if uPlayer == nil then return end
	-- Activate actor
	self:preActivate(event)
		
end

function activateActorHandler(self, event)
	local uPlayer = event.player
	local iPID = event.PID
	if uPlayer == nil then return end
	if iPID == nil then return end

	if iPID == self.PID then
		self:onActivate(event)
	end
end

function onTrigger(self, event)
	if event.player == nil then return end
	if event.triggerType == 1 and event.player.mode ~= "God" then
		for index, questUNID in pairs(self.quests) do
			-- Get the quest item and its bark range
			local questItem = s_uItemProperties[tostring(questUNID)]
			local barkRange = BARK_MODE_NORMAL
			if questItem then
				barkRange = tonumber(questItem.barkRange)
			end
			if barkRange == BARK_MODE_RADIUS then
				self:questBark(event, index)
			end
		end
	end
	_super.onTrigger(self, event)
end

-- onQuestHandlerReady handler
function onQuestHandlerReady(self, event)
	if event.player == nil then return end
	for index, questUNID in pairs(self.quests) do
		-- Get the quest item and its bark range
		local questItem = s_uItemProperties[tostring(questUNID)]
		local barkRange = BARK_MODE_NORMAL
		if questItem then
			barkRange = tonumber(questItem.barkRange)
		end
		-- Check if the newly-arrived player is in bark range
		local inRange = (getDistanceToPoint(self, event.player.position) <= DEFAULT_INTERACTION_RANGE)
		if (barkRange == BARK_MODE_RADIUS and inRange) or barkRange == BARK_MODE_INFINITE then
			self:questBark(event, index)
		end
	end
end

function onQuestItemEdit(self, event)
	if event.UNID == nil then return end
	-- for index, questUNID in pairs(self.quests) do -- Check quests
	-- 	if event.UNID == questUNID then -- Match found
	-- 		Events.removeTrigger(self.trigger) -- Remove old trigger
	-- 		self:resetTriggers()
	-- 		break
	-- 	end
	-- end
end

function onQuestCompleteCheckNext(self, event)
	if event.player == nil then return end
	if event.questUNID == nil then return end
	for index, myQuestUNID in pairs(self.quests) do -- Check quests
		local questItem = s_uItemProperties[tostring(myQuestUNID)]
		if questItem then
			if questItem.prerequisiteQuest and tonumber(questItem.prerequisiteQuest) == tonumber(event.questUNID) then
				local barkRange = tonumber(questItem.barkRange) or BARK_MODE_NORMAL
				local inRange = (getDistanceToPoint(self, event.player.position) <= DEFAULT_INTERACTION_RANGE)
				if (barkRange == BARK_MODE_RADIUS and inRange) or barkRange == BARK_MODE_INFINITE then
					Events.setTimer(.5, function()
							self:questBark(event, index)
						 end)
				end
			end
		end
	end
end

----------------------------------------
-- Class functions
----------------------------------------

-- Validate all conditions necessary before onActivate
function preActivate(self, event)
	log("Class_QuestGiver - preActivate")
	local uPlayer = event.player
	if not Players.validatePlayer(uPlayer) then
		return false
	end

	if uPlayer.mode ~= "Player" then
		return false
	end

	if uPlayer.alive == nil or uPlayer.alive == false then
		kgp.playerShowStatusInfo(uPlayer.ID, "You cannot do that while dead.", 3, 255, 0, 0)
		return false
	end

	if self:isPointWithinRange(uPlayer.position, self.interactionRange) then
		if self.repEnabled then
			if (self.followerOf == uPlayer.name or self.followerOf == nil) then
				Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)
				uPlayer.activeMenuPID = self.PID
				kgp.playerLoadMenu(uPlayer.ID, "Framework_NPCActionMenu.xml")
				local eventProperties = {
					actorPID = self.PID,
					UNID = self.UNID,
					value = self:getRepByPlayer(uPlayer.name),
					isFollower = self.followerOf == uPlayer.name,
					isSpouse = self.spouseOf == uPlayer.name,
					isRider = self.isRider,
					equippedGLIDs = self.equippedGLIDs
				}
				if self.behavior then
					if string.sub(self.behavior, -6, -1) == "_rider" then
						eventProperties.isRider = true
					end
				end
				Events.sendClientEvent("FRAMEWORK_DEFINE_NPC_ACTIONS", eventProperties, uPlayer.ID)
				Events.sendClientEvent("FULFILL_ACTOR_TALK_TO", {UNID = self.UNID}, uPlayer.ID)
			-- Cannot activate another player's spouse that unlocks at the spouse level (but CAN activate another player's follower that unlocks at the folloer level)
			elseif self.repUnlock == NpcDefs.SPECIAL_LEVELS[2] and self.spouseOf ~= uPlayer.name then
				kgp.playerShowStatusInfo(uPlayer.ID, "Only their spouse can do that.", 3, 255, 0, 0)		
			else
				self:onActivate(event)
			end
		else
			self:onActivate(event)
		end
	else
		-- Status menu
		kgp.playerShowStatusInfo(uPlayer.ID, "You must be closer to interact with that object.", 3, 255, 0, 0)
	end
	return false
end

-- Called on activation, overrides Actor's onActivate
function onActivate(self, event)
	log("Class_QuestGiver - onActivate")
	local uPlayer = event.player
	if uPlayer == nil then return end

	Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)
	
	--if self.dialog and self.dialog ~= "" then
	-- Pass along my quests to Controller_Game for filtering and passing to the Quest Giver dialog
	local tQuestGiverData = {
		quests 		  = self.quests,
		name  		  = self.actorName,
		dialog 		  = self.dialog,
		PID    		  = self.PID,
		greetingImage = self.greetingImage
	}
	Events.sendEvent("FRAMEWORK_PASS_AVAILABLE_QUESTS", {player = uPlayer, questGiver = tQuestGiverData, isRider = self.isRider, riderPID = self.PID})
	uPlayer.activeMenuPID = self.PID
	--end

	Events.sendClientEvent("FULFILL_ACTOR_TALK_TO", {UNID = self.UNID}, uPlayer.ID)
end

----------------------------------------
-- Local functions
----------------------------------------

-- Check to see if it is ok to bark a quest
function preValidateQuestBark(self, event)
	log("Class_QuestGiver - preActivate")
	local uPlayer = event.player
	-- Invalid player
	if not Players.validatePlayer(uPlayer) then
		return false
	end
	-- Not in player mode
	if uPlayer.mode ~= "Player" then
		return false
	end
	-- Player is dead
	if uPlayer.alive == nil or uPlayer.alive == false then
		return false
	end
	-- Has reputation enabled with a valid unlock level and rep scale
	if self.repEnabled and self.repUnlock and self.repScale then
		-- Unlocks at the follower level
		if self.repUnlock == NpcDefs.SPECIAL_LEVELS[1] then
			-- Is the player's follower
			if self.followerOf and self.followerOf == uPlayer.name then
				return true
			else -- Is another player's/no one's follower
				return false
			end
		-- Unlocks at the spouse level
		elseif self.repUnlock == NpcDefs.SPECIAL_LEVELS[2] then
			-- Is the player's spouse
			if self.spouseOf and self.spouseOf == uPlayer.name then
				return true
			else -- Is another player's/no one's spouse
				return false
			end
		-- Unlocks at a reputation value
		else
			local playerRep = self:getRepByPlayer(uPlayer.name) or 0
			local requiredRep = NpcDefs.REP_SCALES[self.repScale][self.repUnlock] or 0
			-- Has reputation
			if playerRep then
				-- Has enough reputation
				if playerRep >= requiredRep then
					return true
				else -- Does not have enough reputation
					return false
				end
			else -- Does not have any reputation
				return false
			end
		end
	else -- Does not have an unlock level or does not have a valid unlock level or rep scale
		return true
	end
	-- Catch-all
	return false
end

function questBark(self, event, questIndex)
	local uPlayer = event.player
	if uPlayer == nil then return end
	if questIndex == nil then return end
	if self:preValidateQuestBark(event) == true then
		Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)
		Events.sendClientEvent("FRAMEWORK_BARK_TRIGGERED", {questIndex = self.quests[questIndex]}, uPlayer.ID)
	end
	-- uPlayer.activeMenuPID = self.PID
end

-- function resetTriggers(self)
-- 	for index, questUNID in pairs(self.quests) do
-- 		-- Get the quest item and its bark range
-- 		local questItem = s_uItemProperties[tostring(questUNID)]
-- 		local barkRange = tonumber(questItem.barkRange) or 0
-- 		-- Add a bark trigger for that quest, if relevant
-- 		if barkRange > 0 then
-- 			-- Get quest barkRange
-- 			local functionName = "onTrigger"..tostring(index)
-- 			_G[functionName] = function(self, event)
-- 				if event.player == nil then return end
-- 				if event.triggerType == 1 and event.player.mode ~= "God" then
-- 					self:triggerHandler(event, index)
-- 				end
-- 			end
-- 			self.trigger = Events.setTrigger(barkRange, _G[functionName], self)
-- 		end
-- 	end
-- end