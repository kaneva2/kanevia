--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

include("Lib_ZoneInfo.lua")
include("Lib_SaveLoad.lua")
-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Class		= _G.Class
local ZoneInfo  = _G.ZoneInfo
local SaveLoad	= _G.SaveLoad
local Toolbox 	= _G.Toolbox
-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------

Class.createClass("Class_SpawnPoint", "Class_Object")

Class.createProperties({
	"newPlayer",
	"particleGLID"
})

-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local DEFAULT_NAME = "SPAWN"
local DEFAULT_ANIMATION = 4465635
local DEFAULT_NEW_ANIMATION = 4529427
local CREATOR_ONLY = true -- This object won't display for players
local DEFAULT_MODE_LABELS = {God = {"name"}}
local s_uPlayerSpawners

s_type = "Spawn Point"



function create(self, input)

	s_uPlayerSpawners = SharedData.new("spawners", false)

	input = input or {}

	if not input.newPlayer then
		input.name = createLinkedZoneSpawner(input)
	end
	input.name = input.name or DEFAULT_NAME
	input.creatorOnly = CREATOR_ONLY
	input.modeLabels = DEFAULT_MODE_LABELS


	_super.create(self, input)
	
	self.newPlayer = input.newPlayer or false
	
	self:setCollision(false)
	if self.newPlayer then
		self:setAnimation(DEFAULT_NEW_ANIMATION)
	else
		self:setAnimation(DEFAULT_ANIMATION)
	end

	Events.registerHandler("FRAMEWORK_DYNAMIC_OBJECT_SAVE_SERVER", self.onDynamicObjectSave, self)
end

function destroy(self)

	if self.isBeingRemoved and s_uPlayerSpawners then
		s_uPlayerSpawners[tostring(self.PID)] = nil

		local tempPlayerSpawners = Toolbox.deepCopy(s_uPlayerSpawners)
		SaveLoad.saveGameData("playerSpawners", Events.encode(tempPlayerSpawners))
	end
	
	Events.unregisterHandler("FRAMEWORK_DYNAMIC_OBJECT_SAVE_SERVER", self.onDynamicObjectSave, self)
	_super.destroy(self)

end

function createLinkedZoneSpawner(input)
	local newSpawner  = false
	local spawnerName = "Player Spawner 1"
	local PID = tostring(input.PID)

	if not s_uPlayerSpawners then
		newSpawner = true
		s_uPlayerSpawners = {}
	elseif not s_uPlayerSpawners[PID] then
		newSpawner = true
	end

	if newSpawner then
		local nextIndex =  SaveLoad.loadGameData("NextSpawnerIndex") or 1
		local tempTable = {}
		local name = "Player Spawner "..tostring(nextIndex)
		tempTable.name = name
		local baseX, baseY, baseZ, baseRX, baseRY, baseRZ = kgp.objectGetStartLocation(input.PID) -- Get object's location
		tempTable.position = {
			x  = baseX or 0,
			y  = baseY or 0,
			z  = baseZ or 0,
			rx = baseRX or 0,
			ry = baseRY or 0,
			rz = baseRZ or 1
		}

		s_uPlayerSpawners[PID] = tempTable

		nextIndex = nextIndex + 1

		SaveLoad.saveGameData("NextSpawnerIndex", nextIndex)

		local tempPlayerSpawners = Toolbox.deepCopy(s_uPlayerSpawners)
		log("\ntempPlayerSpawners: "..tostring(Events.encode(tempPlayerSpawners)))
		SaveLoad.saveGameData("playerSpawners", Events.encode(tempPlayerSpawners))
		return name
	else
		return s_uPlayerSpawners[PID].name
	end
end

function onDynamicObjectSave(self, event)
	if event.PID and event.PID == self.PID then
		if s_uPlayerSpawners and s_uPlayerSpawners[tostring(self.PID)] then
			self.position = event.objPos
			s_uPlayerSpawners[tostring(self.PID)].position = event.objPos
			local tempPlayerSpawners = Toolbox.deepCopy(s_uPlayerSpawners)
			SaveLoad.saveGameData("playerSpawners", Events.encode(tempPlayerSpawners))
		end
	end
end