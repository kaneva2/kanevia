--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_Path.lua
-- Path consisting of waypoints used by riders

--include any libs that are not part of lib_common.lua
include("Lib_SaveLoad.lua")
include("Lib_GameDefinitions.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local Debug				= _G.Debug
local SaveLoad			= _G.SaveLoad
local GameDefinitions	= _G.GameDefinitions
local Events			= _G.Events
local Class				= _G.Class

Class.createClass("Class_Path", "Class_Base")

Class.useBehaviors({
	"Behavior_Sound"
})

Class.createProperties({
	"speed",
	"pathType",
	"friction",
	"idleAnimGLID",
	"moveAnimGLID",
	"idleParticleGLID",
	"moveParticleGLID",
	"idleSoundGLID",
	"moveSoundGLID",
	"pathRiderParentUNID",
	"noOfPathWaypoints"
})

local CREATOR_ONLY = true -- This object won't display for players
local DEFAULT_MODE_LABELS = {God = {"Path"}}
local DEFAULT_MOVE_SPEED = 8

local pathTypeEnum = {Closed = 1, Open = 2, Reverse = 3}

s_type = "path"

-----------------------------------------------------------------
-- Class-specific functions
-----------------------------------------------------------------
function create(self, input)
	--assign input to be passed to base class
	input = input or {}
	input.creatorOnly = CREATOR_ONLY
	input.modeLabels = DEFAULT_MODE_LABELS
	--create base class with the input needed
	self.registered = false
	Events.registerHandler("FRAMEWORK_OBJECT_REGISTERED", self.onRegistered, self)
	_super.create(self, input)
	
	--assign shared data
	self.speed = input.speed or DEFAULT_MOVE_SPEED
	self.pathType = input.pathType or pathTypeEnum.Closed
	self.friction = input.friction
	self.idleAnimGLID = input.idleAnimation
	self.moveAnimGLID = input.moveAnimation
	self.idleParticleGLID = input.idleParticle
	self.moveParticleGLID = input.moveParticle
	self.idleSoundGLID = input.idleSound
	self.moveSoundGLID = input.moveSound
	self.pathRiderParentUNID = input.pathRider_parentUNID
	
	self.completeDestruction = false --use it with extreme precaution
	
	if self.pathType == pathTypeEnum.Open then
		self.crntDirection = "fwd"
	end
	
	--create and assign locals to this specific class
	self.noOfCollisionEnabledWaypoints = 0
	self.pathWaypointsPIDs = {}
	self.noOfPathWaypoints = 0
	self.pathRidersPID = nil
	self.noOfPlayersInsideTriggers = 0
	self.idleSoundID = nil
	self.moveSoundID = nil
	
	self:hide() -- Hide by default
	self:setCollision(false) -- Waypoints have no collision
	
	--register events and handlers
	Events.registerHandler("ADD_PATH_WAYPOINT", self.addPathWaypoint, self)
	Events.registerHandler("DEL_PATH_WAYPOINT", self.delPathWaypoint, self)
	Events.registerHandler("WAYPOINT_TRIGGER_EVENT", self.onWaypointTrigger, self)
	Events.registerHandler("ADD_PATH_RIDER", self.addPathRider, self)
	Events.registerHandler("PATH_RIDER_PICKED_UP", self.onPathRiderPickedUp, self)
	Events.registerHandler("OBJECT_PICKUP", self.onObjectPickup, self)
	Events.registerHandler("UPDATE_COLLISION_STATE_TO_PARENT", self.onUpdateCollisionStateToParent, self)
	Events.registerHandler("REQUEST_PATH_INFO_FOR_RIDER", self.onRequestPathInfoForRider, self)
	Events.registerHandler("REQUEST_RIDER_MOVEMENT_OPTIONS", self.onRequestRiderMovementOptions, self)
	Events.registerHandler("AddInfoToPathWaypointPropertiesEvent", self.onAddInfoToPathWaypointProperties, self)
	Events.registerHandler("ClientRequestForAllChildPathPIDs", self.onClientRequestForAllChildPathPIDs, self)
end

function destroy(self)
	if self.completeDestruction == true then
		Events.sendEvent("PATH_DESTROYED_CLEANUP", {parentPathPID = self.PID})
		SaveLoad.deleteGameData("path_"..tostring(self.PID))
	end
	_super.destroy(self)
	Events.unregisterHandler("FRAMEWORK_OBJECT_REGISTERED", self.onRegistered, self)
	Events.unregisterHandler("ADD_PATH_WAYPOINT", self.addPathWaypoint, self)
	Events.unregisterHandler("DEL_PATH_WAYPOINT", self.delPathWaypoint, self)
	Events.unregisterHandler("WAYPOINT_TRIGGER_EVENT", self.onWaypointTrigger, self)
	Events.unregisterHandler("ADD_PATH_RIDER", self.addPathRider, self)
	Events.unregisterHandler("PATH_RIDER_PICKED_UP", self.onPathRiderPickedUp, self)
	Events.unregisterHandler("OBJECT_PICKUP", self.onObjectPickup, self)
	Events.unregisterHandler("UPDATE_COLLISION_STATE_TO_PARENT", self.onUpdateCollisionStateToParent, self)
	Events.unregisterHandler("REQUEST_PATH_INFO_FOR_RIDER", self.onRequestPathInfoForRider, self)
	Events.unregisterHandler("REQUEST_RIDER_MOVEMENT_OPTIONS", self.onRequestRiderMovementOptions, self)
	Events.unregisterHandler("AddInfoToPathWaypointPropertiesEvent", self.onAddInfoToPathWaypointProperties, self)
	Events.unregisterHandler("ClientRequestForAllChildPathPIDs", self.onClientRequestForAllChildPathPIDs, self)
	if self.idleSoundID then deleteSound({soundID = self.idleSoundID}) end
	if self.moveSoundID then deleteSound({soundID = self.moveSoundID}) end
	if self.pathRidersPID then
		Events.sendEvent("OBJECT_DESPAWN", {spawnerPID = self.PID, despawnPID = self.pathRidersPID})
	end
end

function onRegistered(self, event)
	if event.PID == self.PID and self.registered == false then
		self.registered = true
		self:loadPathData()
	end
end

function addPathWaypoint(self, event)
	if self.PID == event.parentPathPID then
		self.noOfPathWaypoints = self.noOfPathWaypoints + 1
		if self.pathWaypointsPIDs[self.noOfPathWaypoints - 1] == event.prevPathWaypointPID then
			self.pathWaypointsPIDs[self.noOfPathWaypoints] = event.newPathWaypointPID
			Events.sendEvent("WAYPOINT_UPDATE_NAME", {waypointPID = event.newPathWaypointPID, newName = self.name .. " " .. tostring(self.noOfPathWaypoints)})
		else
			local startShifting = false
			local pidToShift = event.newPathWaypointPID
			for i, v in pairs(self.pathWaypointsPIDs) do
				if startShifting == true then
					self.pathWaypointsPIDs[i] = pidToShift
					Events.sendEvent("WAYPOINT_UPDATE_NAME", {waypointPID = pidToShift, newName = self.name .. " " .. tostring(i)})
					pidToShift = v
				elseif v == event.prevPathWaypointPID then
					startShifting = true
				end
			end
			self.pathWaypointsPIDs[self.noOfPathWaypoints] = pidToShift
			Events.sendEvent("WAYPOINT_UPDATE_NAME", {waypointPID = pidToShift, newName = self.name .. " " .. tostring(self.noOfPathWaypoints)})
		end
		Events.sendEvent("PATHWAYPOINT_ADDED_CONFIRMATION", {PID = event.newPathWaypointPID})
		self:savePathData()
		if self.noOfPathWaypoints == 2 then
			if self.noOfCollisionEnabledWaypoints < 1 or self.noOfPlayersInsideTriggers > 0 then
				Events.sendEvent("TRIGGER_RIDER_PATHING", {riderPID = self.pathRidersPID})
			end
		elseif self.noOfPathWaypoints == 1 then
			--First Waypoint, add a path rider
			startingWaypointGS = kgp.objectGetGameState(self.pathWaypointsPIDs[1])
			riderLocation = {x = startingWaypointGS.position.x, y = startingWaypointGS.position.y, z = startingWaypointGS.position.z,
							 rx = startingWaypointGS.position.rx, ry = startingWaypointGS.position.ry, rz = startingWaypointGS.position.rz}
			local placePathRiderEventInfo = {playerName = self.owner, UNID = self.pathRiderParentUNID, additionalPathInfo = {parentPathPID = self.PID, speed = self.speed, friction = self.friction},
											 location = riderLocation, spawnerPID = self.PID}
			Events.sendEvent("PLACE_PATH_RIDER", placePathRiderEventInfo)
		end
		Events.sendEvent("COUNT_ACTIVE_DOS_ON_REQUEST", {})
	end
end

function delPathWaypoint(self, event)
	if self.PID == event.parentPathPID then
		local prevPID = nil
		local nextPID = nil
		local startShifting = false
		for i, v in pairs(self.pathWaypointsPIDs) do
			if startShifting == true then
				self.pathWaypointsPIDs[i - 1] = v
				Events.sendEvent("WAYPOINT_UPDATE_NAME", {waypointPID = v, newName = self.name .. " " .. tostring(i - 1)})
			elseif v == event.delPathWaypointPID then
				startShifting = true
				prevPID = self.pathWaypointsPIDs[i - 1] or nil
				nextPID = self.pathWaypointsPIDs[i + 1] or nil
			end
		end
		self.pathWaypointsPIDs[self.noOfPathWaypoints] = nil
		self.noOfPathWaypoints = self.noOfPathWaypoints - 1
		if self.noOfPathWaypoints == 0 then
			Events.sendEvent("PATH_DESTROYED_CLEANUP", {parentPathPID = self.PID})
			Events.sendEvent("ACTIVE_DO_DESTROYED", {activeDODestroyedPID = self.PID})
			SaveLoad.deleteGameData("path_"..tostring(self.PID))
			Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = self.PID})
			kgp.gameItemDeletePlacement(self.PID)
		else
			Events.sendEvent("UPDATE_WAYPOINT_LINKAGES", {prevWaypointPID = prevPID, nextWaypointPID = nextPID})
			self:savePathData()
			Events.sendEvent("COUNT_ACTIVE_DOS_ON_REQUEST", {})
		end
	end
end

function onWaypointTrigger(self, event)
	if self.PID == event.parentPathPID then
		if event.triggerType == 1 then
			-- Player enters trigger
			self.noOfPlayersInsideTriggers = self.noOfPlayersInsideTriggers + 1
			if self.noOfPlayersInsideTriggers == 1 and self.noOfCollisionEnabledWaypoints > 0 then
				Events.sendEvent("TRIGGER_RIDER_PATHING", {riderPID = self.pathRidersPID})
			end
		elseif event.triggerType == 2 then
			-- Player exits trigger
			self.noOfPlayersInsideTriggers = self.noOfPlayersInsideTriggers - 1
			if self.noOfPlayersInsideTriggers < 0 then
				self.noOfPlayersInsideTriggers = 0
			end
		end
	end
end

function addPathRider(self, event)
	if self.PID == event.parentPathPID then
		self.pathRidersPID = event.newPathRiderPID
		Events.sendEvent("PATHRIDER_ADDED_CONFIRMATION", {PID = event.newPathRiderPID, noOfPathWaypoints = self.noOfPathWaypoints, crntWaypoint = self.pathWaypointsPIDs[1]})
		self:savePathData()
		if self.idleSoundGLID then
			local idleSoundParams = GameDefinitions.generateGenericSoundParams(self.pathRidersPID, self.idleSoundGLID, 20, false)
			idleSoundParams.looping = 1
			self.idleSoundID = generateSound(idleSoundParams)
		end
		if self.moveSoundGLID then
			local moveSoundParams = GameDefinitions.generateGenericSoundParams(self.pathRidersPID, self.moveSoundGLID, 20, false)
			moveSoundParams.looping = 1
			self.moveSoundID = generateSound(moveSoundParams)
		end
	end
end

function onPathRiderPickedUp(self, event)
	if self.PID == event.parentPathPID then
		self.pathRidersPID = nil
		self:savePathData()
		local placePathRiderEventInfo = {playerName = self.owner, UNID = event.pickedUpRiderParentUNID, additionalPathInfo = {parentPathPID = self.PID},
										 x = self.position.x, y = self.position.y, z = self.position.z, rx = self.position.rx, ry = self.position.ry, rz = self.position.rz}
		Events.sendEvent("PLACE_PATH_RIDER", placePathRiderEventInfo)
	end
end

function onObjectPickup(self, event)
	if (event.PID == self.PID) then
		Events.sendEvent("PATH_DESTROYED_CLEANUP", {parentPathPID = self.PID})
		SaveLoad.deleteGameData("path_"..tostring(self.PID))
		Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = self.PID})
		kgp.gameItemDeletePlacement(self.PID)
	end
end

function onUpdateCollisionStateToParent(self, event)
	if self.PID == event.parentPathPID then
		if event.collisionEnabled == true then
			self.noOfCollisionEnabledWaypoints = self.noOfCollisionEnabledWaypoints + 1
		else
			self.noOfCollisionEnabledWaypoints = self.noOfCollisionEnabledWaypoints - 1
			if self.noOfCollisionEnabledWaypoints == 0 then
				Events.sendEvent("TRIGGER_RIDER_PATHING", {riderPID = self.pathRidersPID})
			end
		end
		self:savePathData()
	end
end

function onRequestPathInfoForRider(self, event)
	if self.PID == event.parentPathPID then
		if event.currentState == GameDefinitions.RIDER_STATES.idle then
			if event.currentWaypointPID == nil then
				--Invalid State. Return to 1st waypoint and restart
				event.currentState = GameDefinitions.RIDER_STATES.moving
				crntPID = nil
				destPID = self.pathWaypointsPIDs[1]
				anim = self.moveAnimGLID
				stopSound({soundID = self.moveSoundID})
				stopSound({soundID = self.idleSoundID})
				sound = self.moveSoundID
				particle = self.moveParticleGLID
			else
				if self.noOfPathWaypoints < 2 then
					event.currentState = GameDefinitions.RIDER_STATES.onHold
				end
				crntPID = event.currentWaypointPID
				destPID = nil
				anim = self.idleAnimGLID
				stopSound({soundID = self.moveSoundID})
				stopSound({soundID = self.idleSoundID})
				sound = self.idleSoundID
				particle = self.idleParticleGLID
			end
		elseif event.currentState == GameDefinitions.RIDER_STATES.moving then
			if event.currentWaypointPID == nil or self.noOfPathWaypoints < 2 then
				--Invalid State or no of waypoints fell below 2. Return to 1st waypoint and restart
				crntPID = nil
				destPID = self.pathWaypointsPIDs[1]
				anim = self.moveAnimGLID
				stopSound({soundID = self.moveSoundID})
				stopSound({soundID = self.idleSoundID})
				sound = self.moveSoundID
				particle = self.moveParticleGLID
			else
				crntPID = event.currentWaypointPID
				if self.noOfCollisionEnabledWaypoints > 0 and self.noOfPlayersInsideTriggers <= 0 and self.pathWaypointsPIDs[1] == crntPID then
					event.currentState = GameDefinitions.RIDER_STATES.onHold
					anim = self.idleAnimGLID
					stopSound({soundID = self.moveSoundID})
					stopSound({soundID = self.idleSoundID})
					sound = self.idleSoundID
					particle = self.idleParticleGLID
					destPID = nil
				else
					destIndex = nil
					for i, v in pairs(self.pathWaypointsPIDs) do
						if v == crntPID then
							if self.pathType == pathTypeEnum.Closed then
								destIndex = (i >= self.noOfPathWaypoints) and 1 or (i + 1)
							elseif self.pathType == pathTypeEnum.Reverse then
								destIndex = (i <= 1) and self.noOfPathWaypoints or (i - 1)
							elseif self.pathType == pathTypeEnum.Open then
								if self.crntDirection == "fwd" then
									if i >= self.noOfPathWaypoints then
										destIndex = self.noOfPathWaypoints - 1
										self.crntDirection = "rev"
									else
										destIndex = i + 1
									end
								elseif self.crntDirection == "rev" then
									if i <= 1 then
										destIndex = 2
										self.crntDirection = "fwd"
									else
										destIndex = i - 1
									end
								end
							end
							break
						end
					end
					if destIndex ~= nil then
						destPID = self.pathWaypointsPIDs[destIndex]
					else
						--Destination not found. Reset by returning to first waypoint
						crntPID = nil
						destPID = self.pathWaypointsPIDs[1]
					end
					anim = self.moveAnimGLID
					stopSound({soundID = self.moveSoundID})
					stopSound({soundID = self.idleSoundID})
					sound = self.moveSoundID
					particle = self.moveParticleGLID
				end
			end
		end
		Events.sendEvent("PATH_INFO_FOR_RIDER", {riderPID = event.riderPID, currentState = event.currentState,
												 currentWaypointPID = crntPID, destinationWaypointPID = destPID,
												 crntAnim = anim, crntSound = sound, crntParticle = particle})
	end
end

function onRequestRiderMovementOptions(self, event)
	if event.parentPathPID == self.PID then
		if event.movementState == "idle" then
			stopSound({soundID = self.moveSoundID})
			stopSound({soundID = self.idleSoundID})
			Events.sendEvent("MOVEMENT_INFO_FOR_RIDER", {riderPID = event.riderPID, crntAnim = self.idleAnimGLID, crntSound = self.idleSoundID, crntParticle = self.idleParticleGLID})
		elseif event.movementState == "move" then
			stopSound({soundID = self.idleSoundID})
			stopSound({soundID = self.idleSoundID})
			Events.sendEvent("MOVEMENT_INFO_FOR_RIDER", {riderPID = event.riderPID, crntAnim = self.moveAnimGLID, crntSound = self.moveSoundID, crntParticle = self.moveParticleGLID})
		end
	end
end

function onAddInfoToPathWaypointProperties(self, event)
	if event.parentPID == self.PID then
		for i, v in pairs(self.pathWaypointsPIDs) do
			if v == event.pathWaypointPID then
				Events.sendClientEvent("ResponseToPathWaypointPropertiesEvent", {pauseTime = event.pauseTime, rotationType = event.rotationType, easeIn = event.easeIn, easeOut = event.easeOut,
																				 collisionTrigger = event.collisionTrigger, triggerSize = event.triggerSize, index = i, parentUNID = self.UNID}, 
																				 event.playerID)
				break
			end
		end
	end
end

function onClientRequestForAllChildPathPIDs(self, event)
	if event.parentPathPID == self.PID then
		Events.sendClientEvent("ChildPathWaypointPidsEvent", {selectedPathWaypointPID = event.pathWaypointPID, selectedPathParentPID = self.PID,
															  childPathWaypointPIDs = self.pathWaypointsPIDs, pathType = self.pathType}, event.playerID)
	end
end

-----------------------------------------------------------------
-- Local functions
-----------------------------------------------------------------
function savePathData(self)
	local pathData = {}
	pathData.pathWaypointsPIDs = self.pathWaypointsPIDs
	pathData.pathRidersPID = self.pathRidersPID
	pathData.noOfPathWaypoints = self.noOfPathWaypoints
	pathData.noOfCollisionEnabledWaypoints = self.noOfCollisionEnabledWaypoints
	-- encode the data and save it
	SaveLoad.saveGameData("path_".. tostring(self.PID), Events.encode(pathData))
end

function loadPathData(self)
	-- set the current time 
	self.currentTime = (kgp.gameGetCurrentTime()) or 0 -- absolute time in seconds

	-- get the data from the 
	local pathDataString = SaveLoad.loadGameData("path_".. tostring(self.PID))

	local pathData = {}

	-- decode the string 
	if pathDataString then
		pathData = Events.decode(pathDataString)
	else
		pathData = nil
	end

	local riderLocation = nil
	if pathData then 
		-- updating the state information
		self.pathWaypointsPIDs = pathData.pathWaypointsPIDs
		self.noOfPathWaypoints = pathData.noOfPathWaypoints
		self.noOfCollisionEnabledWaypoints = pathData.noOfCollisionEnabledWaypoints
		
		--reset the rider to starting location and rotation
		startingWaypointGS = nil
		if self.pathWaypointsPIDs[1] == nil then
			event = {}
			event.PID = self.PID
			self:onObjectPickup(event)
			return
		else
			startingWaypointGS = kgp.objectGetGameState(self.pathWaypointsPIDs[1])
		end
		if startingWaypointGS == nil or startingWaypointGS.ID == nil then
			riderLoc = self.position
		else
			riderLoc = startingWaypointGS.position
		end
		riderLocation = {x = riderLoc.x, y = riderLoc.y, z = riderLoc.z,
						 rx = riderLoc.rx, ry = riderLoc.ry, rz = riderLoc.rz}
		local placePathRiderEventInfo = {playerName = self.owner, UNID = self.pathRiderParentUNID, additionalPathInfo = {parentPathPID = self.PID, speed = self.speed, friction = self.friction},
										 location = riderLocation, spawnerPID = self.PID}
		Events.sendEvent("PLACE_PATH_RIDER", placePathRiderEventInfo)
	else
		if self.noOfPathWaypoints == 0 then
			local placePathWaypointEventInfo = {playerName = self.owner, GLID = GameDefinitions.PATHWAYPOINT_GLID, UNID = GameDefinitions.PATHWAYPOINT_UNID
										, x = self.position.x, y = self.position.y, z = self.position.z, rx = self.position.rx, ry = self.position.ry, rz = self.position.rz
										, behavior = "pathWaypoint", additionalPathInfo = {parentPathPID = self.PID, prevPathWaypointPID = nil}}
			Events.sendEvent("PLACE_PATH_WAYPOINT", placePathWaypointEventInfo)
		end
	end
end