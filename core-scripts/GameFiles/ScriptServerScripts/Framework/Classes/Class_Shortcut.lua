--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_Shortcut.lua
--
----------------------------------------

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Events			= _G.Events
local Class				= _G.Class

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
Class.createClass("Class_Shortcut", "Class_Base")

Class.createProperties({
	"interactionRange"
})

-----------------------------------------------------------------
-- Constant Definitions
-----------------------------------------------------------------
local CREATOR_ONLY = true -- This object will hide from players
local DEFAULT_MENU = "Framework_NPCDialog"
local DEFAULT_SHORTCUT_TYPE = "Shortcut" -- This object will hide from players
local DEFAULT_ANIMATION = 4465646
local DEFAULT_MODE_LABELS = {God = {"name"}}
local INTERACTION_RANGE = 20
s_type = "Shortcut"


-- Instantiate an instance of character
function create(self, input)
	-- Init super class
	input = input or {}
	input.modeLabels = DEFAULT_MODE_LABELS
	if input.behaviorParams and input.behaviorParams.modeLabels then
		for label, labelInfo in pairs(input.behaviorParams.modeLabels) do
			input.modeLabels[label] = labelInfo
		end
	end
	self.interactionRange = INTERACTION_RANGE
	_super.create(self, input)
	
	-- Init this class
	self.menu = DEFAULT_MENU
	self.shortcutType = DEFAULT_SHORTCUT_TYPE
	self.creatorOnly = CREATOR_ONLY
	-- All shortcuts show their name to players in Creator mode ("God mode" -_-)
	
	if input.behaviorParams then
		self.menu = input.behaviorParams.menu
		self.shortcutType = input.behaviorParams.shortcutType or self.shortcutType
		if input.behaviorParams.creatorOnly ~= nil then
			self.creatorOnly = input.behaviorParams.creatorOnly
		end
	end
	
	-- Setting shortcuts
	if self.shortcutType == "setting" then
		self:setCollision(false)
		self:setAnimation(DEFAULT_ANIMATION)
	-- Donation shortcuts
	elseif self.shortcutType == "donation" then
		local angle = 90
		local X_Movement = math.cos((angle*math.pi)/180)
		local Y_Movement = math.sin((angle*math.pi)/180)
		kgp.objectSetTexturePanning(self.PID, 0, 1, X_Movement*(0.04/100), Y_Movement*(0.04/100))
	end
	
	if not self.creatorOnly then
		self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
	end
end

-- Called when this class is destroyed
function destroy(self)
	_super.destroy(self)
	
	if self.proxTrigger then
		Events.removeClientTrigger(self.proxTrigger)
	end
end

----------------------------------------
-- Event handlers
----------------------------------------

-- On right click handler
function onRightClick(self, event)
	self:onActivate(event) 
end

function onLeftClick(self, event)	
	local handEquipped = event.player.handEquipped == true or event.player.handEquipped == nil
	local doesPlayerOwn = self.owner == event.player.name

	if handEquipped and not doesPlayerOwn then
		self:onActivate(event)
	end
end

-- Called on activation
function onActivate(self, event)
	local player = event.player
	if player == nil then log("Class_Shortcut - onActivate() Failed to extract player") end
	
	if getDistanceToPoint(self, player.position) > self.interactionRange then
		kgp.playerShowStatusInfo(player.ID, "You must be closer to interact with that object.", 3, 255, 0, 0)
	else
		-- Settings open a magical way
		if self.shortcutType == "setting" then
			Events.sendEvent("FRAMEWORK_GOTO_ITEM", {UNID = self.UNID, player = event.player})
		-- Travel is also magical
		elseif self.shortcutType == "travel" then
			Events.sendClientEvent("FRAMEWORK_OPEN_TRAVEL_MENU", {mode = "travel"}, player.ID)
		elseif self.shortcutType == "topWorlds" then
			Events.sendClientEvent("FRAMEWORK_OPEN_TRAVEL_MENU", {mode = "topWorlds"}, player.ID)
		-- If menu is World Coin Converter
		elseif self.menu == "Framework_WorldCoinExchange" then
			-- TODO: filter player only menus from playerOnly variable (needs to be added to KWAS properly through Ben)
			Events.sendClientEvent("FRAMEWORK_OPEN_PLAYER_MENU", {menu = self.menu..".xml"}, player.ID)
		-- Everyone else just opens a boring menu. Laaaaame
		else
			kgp.playerLoadMenu(player.ID, self.menu..".xml")
		end
	end
end