--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_Door.lua
-- Parent class for animated doors
include("Lib_SaveLoad.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local SaveLoad			= _G.SaveLoad
local Toolbox			= _G.Toolbox
local Events			= _G.Events
local Class				= _G.Class
local Players			= _G.Players

-----------------------------------------------------------------
-- Class specifications
-----------------------------------------------------------------

Class.createClass("Class_Door", "Class_Base")

Class.createProperties({
	"locked",
	"interactionRange"
})

-----------------------------------------------------------------
-- Class-specific constants
-----------------------------------------------------------------
local DEFAULT_NAME = "Wooden Door"
local DEFAULT_LABEL_OPEN = "Click to open"
local DEFAULT_LABEL_CLOSE = "Click to close"
local DEFAULT_LEVEL = 4 --basic wooden doors are level 4
local PIN_SAVE_KEY = "DNC_pinCode"
local PIN_ACCESS_MEM_KEY = "DNC_pinCodeMemory"

local DEFAULT_OPEN_ANIM = 4468409
local DEFAULT_CLOSE_ANIM = 4468410
local DEFAULT_OPENED_ANIM = 4468417
local DEFAULT_CLOSED_ANIM = 0

local DEFAULT_ANIM_TIME = 1

local INTERACTION_RANGE = 20

local DEFAULT_LOCKED = false

s_type = "Door"

-----------------------------------------------------------------
-- Class-specific functions
-----------------------------------------------------------------
local moveObject --(self, playerID)
local isPointInFlagBoundary -- (flag, x, y, z)


function create(self, input)
	if input == nil then input = {} end
	self.tooltip = DEFAULT_LABEL_OPEN
	
	self.targetable = input.targetable or "true"
	self.level = input.level or DEFAULT_LEVEL
	self.interactionRange = INTERACTION_RANGE
	_super.create(self, input)
	
	self.name = input.name or DEFAULT_NAME
	
	self.openAnim = input.openAnim or DEFAULT_OPEN_ANIM
	self.closeAnim = input.closeAnim or DEFAULT_CLOSE_ANIM
	
	self.openedAnim = input.openedAnim or DEFAULT_OPENED_ANIM
	self.closedAnim = input.closedAnim or DEFAULT_CLOSED_ANIM

	self.animTime = input.animTime or DEFAULT_ANIM_TIME
	self.locked = input.locked or DEFAULT_LOCKED
	
	self.open = false
	self.moving = false

    self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
	
	self.landClaimFlags = {}
	
	Events.registerHandler("RETURN_FLAGS_FULL", self.onRegisterFlagsFull, self)
	Events.registerHandler("RETURN_FLAG", self.onRegisterFlag, self)
	Events.registerHandler("REMOVE_FLAG", self.onRemoveFlag, self)
	Events.registerHandler("PIN_CODE_ACCEPTED", self.onPinAccepted, self)
end 

function destroy(self)
	_super.destroy(self)

    if self.proxTrigger then
        Events.removeClientTrigger(self.proxTrigger)
    end
	
	Events.unregisterHandler("RETURN_FLAGS_FULL", self.onRegisterFlagsFull, self)
	Events.unregisterHandler("RETURN_FLAG", self.onRegisterFlag, self)
	Events.unregisterHandler("REMOVE_FLAG", self.onRemoveFlag, self)
	Events.unregisterHandler("PIN_CODE_ACCEPTED", self.onPinAccepted, self)
end

-- On object right click
function onRightClick(self, event)
	self:onActivate(event)
end

function onLeftClick(self, event)
	local handEquipped = (event.player.handEquipped == true or event.player.handEquipped == nil)
	local doesPlayerOwn = canPlayerOpenSafe(self, event.player.name)
	-- if the hand tool is selected and the player does not own 
	if handEquipped and not doesPlayerOwn then
		self:onActivate(event)
	end 
end

function onUse(self, event)
	if(event.PID and self.PID == event.PID) then
		self:onActivate(event)
	end
end

-- On activate
function onActivate(self, event)
	-- Only valid players can activate the door
	if event.player and (event.player.mode ~= "God") then
		local pinCode = self:getPinCode()
		if self.locked == true then
			local canOpen = self:canPlayerOpenSafe(event.player.name)
			if canOpen then
				self:validActivation(event.player)
			elseif pinCode ~= nil then
				-- if MenuIsOpen("Framework_PINCode.xml") then
				-- 	MenuClose("Framework_PINCode.xml")
				-- end
				kgp.playerLoadMenu(event.player.ID, "Framework_PINCode.xml")
				Events.sendClientEvent("OBJECT_ACTIVATE_PIN", {PID = self.PID, name = self.name, owner = self.owner}, event.player.ID)
			else
				kgp.playerShowStatusInfo(event.player.ID, "You do not have access to this door.", 3, 255, 0, 0)
			end
		else
			self:validActivation(event.player)
		end
	end
end

function onRotateComplete(self, event)
	self.doorTimer = nil
	self.moving = false
	
	if self.open then
		self:setAnimation(self.openedAnim)
	else
		self:setAnimation(self.closedAnim)
	end
end

-- Perform when the object is successfully activated by click or by pin code
validActivation = function(self, inputPlayer)
	moveObject(self, inputPlayer)
end

-- Move the object based on its current location
moveObject = function(self, player)
	if not Players.validatePlayer(player) then
		return
	end
	
	if  getDistanceToPoint(self, player.position) > self.interactionRange then
		kgp.playerShowStatusInfo(player.ID, "You must be closer to interact with that object.", 3, 255, 0, 0)
	elseif self.moving == false then
		if self.open then -- close the damn door!
			self:setAnimation(self.closeAnim)
			self.open = false
			Events.setTimer(0.5, function() setCollision(self, true) end)
			self:updateTooltip(DEFAULT_LABEL_OPEN)
		else -- open that thing
			self:setAnimation(self.openAnim)
			self.open = true
			Events.setTimer(0.5, function() setCollision(self, false) end)
			self:updateTooltip(DEFAULT_LABEL_CLOSE)
		end
		self.moving = true
		self.doorTimer = Events.setTimer(self.animTime, onRotateComplete, self)
	else
		kgp.playerShowStatusInfo(player.ID, "The door is already in motion.", 3, 255, 0, 0)
	end
end

function canPlayerOpenSafe(self, playerName)
	local isPlayerOwnerOfItem = playerName == self.owner
	local doClaimFlagsGivePermission = false
	local pinMembers = self:getPinAccessMemory()
	local preValidated = checkForEntry(pinMembers, playerName)

	if isPlayerOwnerOfItem or preValidated then 
		return true
	else
		-- If we are not the owner, then check on land claim flags to see if the player has special permissions to manipulate the object in question
		local pos = self.position
		local flags = self:getClaimFlagsAtPoint(pos.x, pos.y, pos.z)
		for i, flag in ipairs(flags) do
			if( flag.membersBag[playerName] == true ) then
				doClaimFlagsGivePermission = true
			else
				doClaimFlagsGivePermission = false
				break
			end
		end
	end
	
	return doClaimFlagsGivePermission
end

function getClaimFlagsAtPoint(self, x, y, z)
	local flags = {}
	
	for i, flag in pairs(self.landClaimFlags) do
		if isPointInFlagBoundary(flag, x, y, z) then
			table.insert(flags, flag)
		end
	end
	
	return flags
end

function getPinCode(self)
	local PID = self.PID
	local saveLoc = PIN_SAVE_KEY.."_"..tostring(PID)
	local pinDataString = SaveLoad.loadGameData(saveLoc)

	local pinData = {}
	if pinDataString then
		pinData = Events.decode(pinDataString)
	end
	local tempPinCode = pinData.pinCode or nil

	return tempPinCode
end

function getPinAccessMemory(self)
	local PID = self.PID
	local saveLoc = PIN_ACCESS_MEM_KEY.."_"..tostring(PID)
	local pinDataString = SaveLoad.loadGameData(saveLoc)

	local pinMembers = {}
	if pinDataString then
		pinMembers = Events.decode(pinDataString)
	end

	return pinMembers
end

function savePinAccessMemory(self, playerName)
	local PID = event.PID
	local pinCode = event.pinCode
	local pinMembers = self:getPinAccessMemory()
	if pinMembers == nil then -- if never saved before
		pinMembers = {}
	elseif #pinMembers == 0 then
		pinMembers = {}
	end
	if not checkForEntry(pinMembers, playerName) then
		table.insert(pinMembers, playerName)
		local pinDataString = Events.encode(pinMembers)
		local saveLoc = PIN_ACCESS_MEM_KEY.."_"..tostring(PID)
		SaveLoad.saveGameData(saveLoc, pinDataString)
	end
end

-- Returns whether a search item exists in a table
checkForEntry = function(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return true
		end
	end
	return false
end

isPointInFlagBoundary = function(flag, x, y, z)
	local xDiff = x - flag.position.x
	local zDiff = z - flag.position.z
	local xzDistanceSqrd = xDiff * xDiff + zDiff * zDiff
	local radiusSqrd = flag.radius * flag.radius
	return (radiusSqrd > xzDistanceSqrd) and flag.height >= math.abs(y - flag.position.y)
end

--------------------
-- Event handlers --
--------------------

function onRegisterFlagsFull(self, event)
	self.landClaimFlags = {}
	
	for i, flag in pairs(event.flags) do
		if(flag.flagType == "claimFlag") then
			flag.membersBag = Toolbox.convertContainerToSet(flag.members)
			self.landClaimFlags[tostring(flag.PID)] = flag
			--table.insert(self.landClaimFlags, flag)
		end
	end
end

function onRegisterFlag(self, event )
	if event.flag and event.flag.flagType == "claimFlag" then
		local flag = event.flag
		flag.membersBag = Toolbox.convertContainerToSet(flag.members)
		self.landClaimFlags[tostring(flag.PID)] = flag
	end
end

function onRemoveFlag(self, event )
	if event.PID then
		self.landClaimFlags[tostring(event.PID)] = nil
	end
end

function onPinAccepted(self, event)
	if event.PID and event.PID == self.PID then
		self:validActivation(event.player)
		self:savePinAccessMemory(event.player.name)
	end
end