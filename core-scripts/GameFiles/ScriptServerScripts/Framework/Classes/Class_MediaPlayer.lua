--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_MediaPlayer.lua
-- Parent class for local media players
include("Lib_SaveLoad.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local SaveLoad			= _G.SaveLoad
local Toolbox			= _G.Toolbox
local Events			= _G.Events
local Class				= _G.Class

Class.createClass("Class_MediaPlayer", "Class_Base")

Class.createProperties({
	"interactionRange",
	"playlist"
})

--------------------------------------------------------------
-- Class-specific constants
--------------------------------------------------------------
local DEFAULT_NAME = "Local Media Player"
local DEFAULT_LEVEL = 4 
local PIN_SAVE_KEY = "DNC_pinCode"
local PIN_ACCESS_MEM_KEY = "DNC_pinCodeMemory"

local INTERACTION_RANGE = 20

local PLAYLIST_TYPE_MEDIA	= "m"

s_type = "media"

--------------------------------------------------------------
-- Class-specific functions
--------------------------------------------------------------
local moveObject --(self, playerID)
local isPointInFlagBoundary -- (flag, x, y, z)

--------------------------------------------------------------
-- Static variables
--------------------------------------------------------------
local s_tOriginalPlaylist = {}


function create(self, input)
	if input == nil then input = {} end
	self.tooltip = "Right-click to select playlist"
	
	self.targetable = input.targetable or "true"
	self.level = input.level or DEFAULT_LEVEL
	self.interactionRange = INTERACTION_RANGE
	self.radius = input.radius or 50
	self.landClaimFlags = {}

	self.playlist = input.playlist or nil
	_super.create(self, input)

	if not s_tOriginalPlaylist[self.PID] then
		s_tOriginalPlaylist[self.PID] = self.playlist
	end
	
	self.name = input.name or DEFAULT_NAME
	self.actorName = input.actorName or ""
	self.showLabels = input.showLabels

	self:clearLabels()
	if self.showLabels and self.actorName ~= "" then
		local labels = {}
	-- size (typically 0.013, r, g, b (rgb out of 1)
		table.insert(labels, {self.actorName, 0.013, 1, 1, 1})
	
		if #labels > 0 then
			self:addLabels(labels)
		end
	end
	self:registerSystemMedia({})

	--Needs to be evaluated
	Events.sendClientEvent("FRAMEWORK_MEDIA_PLAYING", {PID = self.PID, playlist = self.playlist, originalPlaylist = s_tOriginalPlaylist[self.PID]})


	Events.registerHandler("FRAMEWORK_DYNAMIC_OBJECT_SAVE_SERVER", self.onDynamicObjectSave, self)
	Events.registerHandler("FRAMEWORK_UPDATE_LOCAL_PLAYLIST", self.updatePlaylist, self)
	Events.registerHandler("PIN_CODE_ACCEPTED", self.onPinAccepted, self)
	
	Events.registerHandler("RETURN_FLAGS_FULL", self.onRegisterFlagsFull, self)
	Events.registerHandler("RETURN_FLAG", self.onRegisterFlag, self)
	Events.registerHandler("REMOVE_FLAG", self.onRemoveFlag, self)
	
	self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
	kgp.objectSetMediaVolumeAndRadius (self.PID, -1, self.radius,  self.radius)
	self:setRadius()
end 

function updatePlaylist(self,event)
	if tostring(event.PID) == tostring(self.PID) then
		s_tOriginalPlaylist[self.PID] = event.newPlaylist
	end
end

function setRadius(self,event)
	kgp.objectSetMediaVolumeAndRadius (self.PID, -1, self.radius,  self.radius)
end

-- On object right click
function onRightClick(self, event)
	self:onActivate(event)
end

function onUse(self, event)
	if(event.PID and self.PID == event.PID) then
		self:onActivate(event)
	end
end

function onLeftClick(self, event)
	local handEquipped = (event.player.handEquipped == true or event.player.handEquipped == nil)
	local doesPlayerOwn = canPlayerActivate(self, event.player.name)
	-- if the hand tool is selected and the player does not own 
	if handEquipped and not doesPlayerOwn then
		self:onActivate(event)
	end 
end

-- On activate
function onActivate(self, event)
	if event.player and (event.player.mode ~= "God") then
		local canActivate = self:canPlayerActivate(event.player.name)
		local pinCode = self:getPinCode()
		local pinMembers = self:getPinAccessMemory()
		local preValidated = checkForEntry(pinMembers, event.player.name)
		if canActivate or preValidated then
			self:validActivation(event.player)
		elseif pinCode ~= nil then
			kgp.playerLoadMenu(event.player.ID, "Framework_PINCode.xml")
			Events.sendClientEvent("OBJECT_ACTIVATE_PIN", {PID = self.PID, name = self.name, owner = self.owner}, event.player.ID)
		else
			-- Playlist cannot be edited by this player. Make him/her aware with a polite message.
			kgp.playerShowStatusInfo(event.player.ID, "You do not have access to this media object.", 3, 255, 0, 0)
		end
	end
end

validActivation = function(self, inputPlayer)
	-- Allow the playlist to be edited
	kgp.playerLoadMenu(inputPlayer.ID, "PlaylistSelect.xml")
	Events.sendClientEvent("FRAMEWORK_SELECT_MEDIA", {mediaType = PLAYLIST_TYPE_MEDIA, id = self.PID}, inputPlayer.ID)
end

function registerSystemMedia(self, event)
	local mediaInfo = {}
	mediaInfo.PID = self.PID
	mediaInfo.position = {x = self.position.x, y = self.position.y, z = self.position.z}
	mediaInfo.radius = self.radius
	Events.sendEvent("REGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "media", info = mediaInfo})
end

function onDynamicObjectSave(self, event)
	if event.PID and event.PID == self.PID  and event.objPos then
		local mediaInfo = {}
		mediaInfo.PID = self.PID
		mediaInfo.position = event.objPos
		mediaInfo.radius = self.radius
		Events.sendEvent("REGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "media", info = mediaInfo})
	end
end

function destroy(self)
	Events.unregisterHandler("RETURN_FLAGS_FULL", self.onRegisterFlagsFull, self)
	Events.unregisterHandler("RETURN_FLAG", self.onRegisterFlag, self)
	Events.unregisterHandler("REMOVE_FLAG", self.onRemoveFlag, self)
	Events.unregisterHandler("FRAMEWORK_DYNAMIC_OBJECT_SAVE_SERVER", self.onDynamicObjectSave, self)
	Events.unregisterHandler("FRAMEWORK_UPDATE_LOCAL_PLAYLIST", self.updatePlaylist, self)
	Events.unregisterHandler("PIN_CODE_ACCEPTED", self.onPinAccepted, self)

	Events.sendEvent("UNREGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "media", PID = self.PID})
	_super.destroy(self)
end

function canPlayerActivate(self, playerName)
	local isPlayerOwnerOfItem = playerName == self.owner
	local doClaimFlagsGivePermission = false
	
	if( isPlayerOwnerOfItem ) then 
		return true
	else
		-- If we are not the owner, then check on land claim flags to see if the player has special permissions to manipulate the object in question
		local pos = self.position
		local flags = self:getClaimFlagsAtPoint(pos.x, pos.y, pos.z)
		for i, flag in ipairs(flags) do
			if( flag.membersBag[playerName] == true ) then
				doClaimFlagsGivePermission = true
			else
				doClaimFlagsGivePermission = false
				break
			end
		end
	end
	
	return doClaimFlagsGivePermission
end

function getClaimFlagsAtPoint(self, x, y, z)
	local flags = {}
	
	for i, flag in ipairs(self.landClaimFlags) do
		if isPointInFlagBoundary(flag, x, y, z) then
			table.insert(flags, flag)
		end
	end
	
	return flags
end

function getPinCode(self)
	local PID = self.PID
	local saveLoc = PIN_SAVE_KEY.."_"..tostring(PID)
	local pinDataString = SaveLoad.loadGameData(saveLoc)

	local pinData = {}
	if pinDataString then
		pinData = Events.decode(pinDataString)
	end
	local tempPinCode = pinData.pinCode or nil

	return tempPinCode
end

function getPinAccessMemory(self)
	local PID = self.PID
	local saveLoc = PIN_ACCESS_MEM_KEY.."_"..tostring(PID)
	local pinDataString = SaveLoad.loadGameData(saveLoc)

	local pinMembers = {}
	if pinDataString then
		pinMembers = Events.decode(pinDataString)
	end

	return pinMembers
end

function savePinAccessMemory(self, playerName)
	local PID = event.PID
	local pinCode = event.pinCode
	local pinMembers = self:getPinAccessMemory()
	if pinMembers == nil then -- if never saved before
		pinMembers = {}
	elseif #pinMembers == 0 then
		pinMembers = {}
	end
	if not checkForEntry(pinMembers, playerName) then
		table.insert(pinMembers, playerName)
		local pinDataString = Events.encode(pinMembers)
		local saveLoc = PIN_ACCESS_MEM_KEY.."_"..tostring(PID)
		SaveLoad.saveGameData(saveLoc, pinDataString)
	end
end

-- Returns whether a search item exists in a table
checkForEntry = function(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return true
		end
	end
	return false
end

function onRegisterFlagsFull(self, event)
	self.landClaimFlags = {}
	
	for i, flag in pairs(event.flags) do
		if(flag.flagType == "claimFlag") then
			flag.membersBag = Toolbox.convertContainerToSet(flag.members)
			self.landClaimFlags[tostring(flag.PID)] = flag
			--table.insert(self.landClaimFlags, flag)
		end
	end
end

function onRegisterFlag(self, event )
	if event.flag then
		local flag = event.flag
		flag.membersBag = Toolbox.convertContainerToSet(flag.members)
		self.landClaimFlags[tostring(flag.PID)] = flag
	end
end

function onRemoveFlag(self, event )
	if event.PID then
		self.landClaimFlags[tostring(event.PID)] = nil
	end
end

function onPinAccepted(self, event)
	if event.PID and event.PID == self.PID then
		self:validActivation(event.player)
		self:savePinAccessMemory(event.player.name)
	end
end

isPointInFlagBoundary = function(flag, x, y, z)
	local xDiff = x - flag.position.x
	local zDiff = z - flag.position.z
	local xzDistanceSqrd = xDiff * xDiff + zDiff * zDiff
	local radiusSqrd = flag.radius * flag.radius
	return (radiusSqrd > xzDistanceSqrd) and flag.height >= math.abs(y - flag.position.y)
end