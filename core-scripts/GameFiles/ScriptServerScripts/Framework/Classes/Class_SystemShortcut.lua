--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_SystemShortcut.lua
--
----------------------------------------

-- Inherit from Base Class
Class.createClass("Class_SystemShortcut", "Class_Base")

----------------------------------------
-- "Private" class variables/methods
----------------------------------------
-- Variables
-- Constants
local DEFAULT_SETTINGS = "Master"
local DEFAULT_ANIMATION = 4465646

----------------------------------------
-- Public class variables
----------------------------------------
s_type = "SystemShortcut"

----------------------------------------
-- Private Methods
----------------------------------------

-- Instantiate an instance of character
function create(self, input)
	-- Init super class
	input = input or {}

	_super.create(self, input)

	-- Init this class
	self.settings = input.settings or DEFAULT_SETTINGS
	if self.settings == "Donation" then
		angle = 90
        X_Movement = math.cos((angle*math.pi)/180)
        Y_Movement = math.sin((angle*math.pi)/180)
        kgp.objectSetTexturePanning(self.PID,0,1,X_Movement*(0.04/100),Y_Movement*(0.04/100))
		self:setCollision(true)
	else
		self:setCollision(false)
	end

	self:setAnimation(DEFAULT_ANIMATION)
end

-- Called when this class is destroyed
function destroy(self)
	_super.destroy(self)
end

----------------------------------------
-- Event handlers
----------------------------------------

-- On right click handler
function onRightClick(self, event)
	self:onActivate(event)
end

-- Called on activation
function onActivate(self, event)
	local player = event.player
	if player == nil then log("Class_SystemShortcut - onActivate() Failed to extract player") end
	-- Open System menu
	if self.settings == "Donation" then
		kgp.playerLoadMenu(player.ID, "Donation.xml")
	elseif self.settings == "Master" then
		kgp.playerLoadMenu(player.ID, "Framework_GameSystems.xml")
	end
end