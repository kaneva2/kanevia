--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_Seat.lua

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Players			= _G.Players

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
Class.createClass("Class_Seat", "Class_InteractiveNode")

----------------------------------------
-- Constant Definitions
----------------------------------------
local TICK_DELAY = 2
local DEFAULT_REGEN_TIME = 2 --minutes
local SECONDS_PER_MINUTE = 60


function create(self, input)
	_super.create(self, input)
	
	if input.regen or input.regen == nil then
		self.regen = true
		self.regenTime = (input.regenTime or DEFAULT_REGEN_TIME) * SECONDS_PER_MINUTE
	end
	
	self.usersInProximity = {}
	self.delayTick = nil
end 

function destroy(self)
	_super.destroy(self)
end

local ERROR_MARGIN = 0.05
function onServerTick(self, event)
	_super.onServerTick(self, event)
	for node, data in pairs(self.nodes) do
		if data.player ~= nil then
			if self.delayTick and self.delayTick > 0 then --delay the first tick in case we are still moving the player into position
				-- Console.Write(Console.INFO, "Decrementing tick delay for "..tostring(data.player.name))
				self.delayTick = self.delayTick - 1
				if self.delayTick <= 0 then self.delayTick = nil end
			elseif data.player and Players.validatePlayerPosition(data.player.position) then
				local playerRotationX, playerPositionX, playerPositionY, playerPositionZ = 0
				playerPositionX = data.player.position.x
				playerPositionZ = data.player.position.z
				playerRotationX = data.player.position.rx
				if math.abs(data.position.rx - playerRotationX) > ERROR_MARGIN then
					if math.abs(data.position.x - playerPositionX) < ERROR_MARGIN and 
					   math.abs(data.position.z - playerPositionZ) < ERROR_MARGIN then
						playerSetAnimation({user = self.nodes[node].player.ID, animation = 0})
					end
					self.removePlayerFromNode(self, data.player, self.getPlayerNode(self, data.player))
				end
			end
		end
	end
end

function addPlayerToNode(self, player, node)
	local gender = player.gender
	if gender == "M" then gender = "male" else gender = "female" end
	
	_super.addPlayerToNode(self, player, node)
	if self.regen then
		player.regenPID = self.PID
		player.regenTime = self.regenTime
	end
	self.delayTick = TICK_DELAY
	
	for k, v in pairs(self.usersInProximity) do
		local event = {player = v, triggerType = 1}
		self.onProximity(self, event)
	end
end

function removePlayerFromNode(self, player, node)	
	if self.delayTick then
		self.delayTick = nil
	end
	
	_super.removePlayerFromNode(self, player, node)
	if player.regenPID == self.PID then
		player.regenPID = nil
		player.regenTime = nil
	end
	
	for k, v in pairs(self.usersInProximity) do
		local event = {player = v, triggerType = 1}
		self.onProximity(self, event)
	end
end