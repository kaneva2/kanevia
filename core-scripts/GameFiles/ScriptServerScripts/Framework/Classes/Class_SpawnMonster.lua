--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_Monster.lua
--
-- Monster class inherited from NPC
--
-- User-definable variables
-- STRING
-- NUMBER
-- TABLE
--
-- User-accessible methods
--
---------------------------------------
include("Lib_GameDefinitions.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local GameDefinitions	= _G.GameDefinitions
local Events			= _G.Events
local Class				= _G.Class

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------

Class.createClass("Class_SpawnMonster", "Class_BattleEntity")

Class.createProperties({
	"spawnerPID",
	"healthMultiplier",
	"spawnGLID"
})


-----------------------------------------------------------------
-- Constant Definitions
-----------------------------------------------------------------
local DEFAULT_IDLE_ANIMATION  = 4162553
local DEFAULT_MOVE_ANIMATION  = 4195012
local DEFAULT_ATTACK_ANIMATION  = 4350206
local DEFAULT_DEATH_ANIMATION = GameDefinitions.DEFAULT_PLAYER_ANIMS.DEATH

local DEFAULT_NAME = "Brainfeaster"

local DEFAULT_WALK_SPEED = 5
local DEFAULT_RUN_SPEED  = 15

local DEFAULT_ATTACK_RANGE = 5
local DEFAULT_WANDER_RANGE = 25
local DEFAULT_AGGRO_RANGE = 30

local DEFAULT_ATTACK_MOB_RANGE = 50
local DEFAULT_SOUND_MOB_RANGE = 0

local DEFAULT_DRAW_DISTANCE = 1800

local BUFFER_DISTANCE = 3
local CHASE_DISTANCE_BUFFER = 10
local LEASH_DISTANCE = 200

local ATTACK_TIME = 2000
local DEATH_ANIMATION_TIME = 10

local WANDER_TIMER = 5000

local DEFAULT_XP = 1

local INITIAL_THREAT = 20
local ATTACK_THREAT = 8

-- States for NPC
local STATE = {
	IDLE		= 0,
	WANDER		= 1,
	CHASE		= 2,
	RETURNING	= 4,
	DEAD		= 5,
}

local SOUND_AGGRO_SCALE = .1 -- The scale we apply to weapon ranges before the monster aggroes

local ATTACK_SOUND_REPEAT_COUNT = 2

s_type = "Monster"



-- Instantiate an instance of NPC
function create(self, input)
	-- TODO: Error check input values
	if input == nil then input = {} end
	input.targetable = "true"
	self.healthMultiplier = input.healthMultiplier or 1
	_super.create(self, input)
	self:setDrawDistance(DEFAULT_DRAW_DISTANCE)
	
	self.eulerRotX = 0
	self.eulerRotY = input.eulerRotY or 0
	self.eulerRotZ = 0

	--log("--- self.eulerRotY ".. tostring(self.eulerRotY))
	
	-- Old static members
	self.tick = 0
	self.attackTimer = 0
	self.stateChangeCooldown = 0	
	
	-- User-definable variables (Public, document for users)
	self.name = input.name  or DEFAULT_NAME
	self.walkSpeed = input.walkSpeed or DEFAULT_WALK_SPEED
	self.runSpeed = input.runSpeed or DEFAULT_RUN_SPEED

	self.speed = self.walkSpeed
	
	self.spawnerPID = input.spawnerPID
	self.spawnGLID = input.spawnGLID

	self.animations = {}
	self.animations.idle = DEFAULT_IDLE_ANIMATION
	self.animations.moving = DEFAULT_MOVE_ANIMATION
	self.animations.attacking = DEFAULT_ATTACK_ANIMATION
	self.animations.death = DEFAULT_DEATH_ANIMATION
	self.monsterKillXP = input.monsterKillXP or DEFAULT_XP

	self.aggroRange = input.aggroRange or DEFAULT_AGGRO_RANGE
	self.wanderRange = input.wanderRange or DEFAULT_WANDER_RANGE
	self.attackRange = input.attackRange or DEFAULT_ATTACK_RANGE

	if input.animations then
		if input.animations.idle then self.animations.idle = input.animations.idle end
		if input.animations.moving then self.animations.moving = input.animations.moving end
		if input.animations.attacking then self.animations.attacking = input.animations.attacking end
		if input.animations.death then self.animations.death = input.animations.death end
	end

	self.attackSoundGLID = input.attackSound
	
	self:setState(STATE.WANDER)

	Events.registerHandler("OBJECT_HEALED", self.onHealed, self)
	Events.registerHandler("OBJECT_DAMAGED", self.onDamage, self)
	Events.registerHandler("PLAYER_DIED", self.onPlayerDied, self)
	Events.registerHandler("OBJECT_DIED", self.onObjectDied, self)
	
	Events.registerHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)

	Events.registerHandler("FRAMEWORK_CALL_FOR_HELP", self.callForHelpHandler, self)
	Events.registerHandler("CHECK_AGAINST_SOUND", self.mobSound, self)

	--self:setMouseOver(5)
	
	-- Register for health bars
	self:enableHealthBar({})

	self:setCollision(false)
	
	self.agroTrigger = Events.setTrigger(self.aggroRange, self.onTrigger, self)
	
	local tSoundParams = GameDefinitions.generateGenericSoundParams(self.PID, self.attackSoundGLID, 20)
	self.attackSound = generateSound( tSoundParams );

	self.attackSoundRepeat = ATTACK_SOUND_REPEAT_COUNT
end

function destroy(self)
	_super.destroy(self)

	Events.unregisterHandler("OBJECT_HEALED", self.onHealed, self)
	Events.unregisterHandler("OBJECT_DAMAGED", self.onDamage, self)
	Events.unregisterHandler("PLAYER_DIED", self.onPlayerDied, self)
	Events.unregisterHandler("OBJECT_DIED", self.onObjectDied, self)
	Events.unregisterHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)

	Events.unregisterHandler("FRAMEWORK_CALL_FOR_HELP", self.callForHelpHandler, self)
	Events.unregisterHandler("CHECK_AGAINST_SOUND", self.mobSound, self)

	if self.agroTrigger then
		Events.removeTrigger(self.agroTrigger)
	end

	if self.attackSound then deleteSound({soundID = self.attackSound}) end
end

-- Sets the state for the Monster
function setState(self, state)

	if state == self.state then return end
	
	if self.inMotion == true then
		self:stopMovement()
	end	
	
	if state == STATE.IDLE then
		self:enterIdleState(self)
	elseif state == STATE.WANDER then
		self:enterWanderState(self)
	elseif state == STATE.CHASE then
		self:enterChaseState(self)
	elseif state == STATE.RETURNING then
		self:enterReturningState(self)
	elseif state == STATE.DEAD then
		self:enterDeadState(self)
	end
end

function enterIdleState(self)
	self.state = STATE.IDLE
	self:setAnimation(self.animations.idle)
	self.stateChangeCooldown = WANDER_TIMER
	self:show()
end

function enterWanderState(self)
	self.state = STATE.WANDER
	self.speed = self.walkSpeed
	self:setAnimation(self.animations.moving)
	self:setWanderLocation()
	self:show()
end

function enterChaseState(self)
	if self.victim == nil then return end
	self.state = STATE.CHASE
	self.speed = self.runSpeed
	self:setAnimation(self.animations.moving)

	self:chase({target = self.victim, buffer = BUFFER_DISTANCE})
	self:show()
end

function enterReturningState(self)
	self.victim = nil
	self.state = STATE.RETURNING
	self:setAnimation(self.animations.moving)

	local input = {x = self.home.x, y = self.home.y, z = self.home.z, lookAt = true, rx = self.position.rx, ry = self.position.ry, rz = self.position.rz}
	self:moveTo(input)
	self:show()

	self.attackSoundRepeat = ATTACK_SOUND_REPEAT_COUNT
end

function enterDeadState(self)
	self.agroTable = {}
	self.state = STATE.DEAD
	self:setAnimation(self.animations.death)

	Events.setTimer( DEATH_ANIMATION_TIME, function () self:despawn() end )
end

function handleState(self, deltaTick)
	if self.state == STATE.IDLE then
		self:handleIdleState(deltaTick)
	elseif self.state == STATE.WANDER then
	elseif self.state == STATE.CHASE then
		self:handleChaseState(deltaTick)
	elseif self.state == STATE.RETURNING then
	end
end

function handleIdleState(self, deltaTick)
	if self.stateChangeCooldown <= 0 then
		self:setState(STATE.WANDER)
	end
end

function handleChaseState(self, deltaTick)
	if self.victim == nil then return end
	-- Check the distance from us to our target
	local distToTarget = self:getDistanceToPoint(self.victim.position)
	local distFromHome = self:getDistanceToPoint(self.home)
	
	self:erodeAgro(deltaTick)

	if Toolbox.isContainerEmpty(self.agroTable) then
		self:setState(STATE.RETURNING)
		return
	end

	-- If we're close enough to the target
	if distToTarget <= CHASE_DISTANCE_BUFFER then
		self:setAnimation(self.animations.attacking)
	elseif distToTarget > CHASE_DISTANCE_BUFFER then
		self:setAnimation(self.animations.moving)
	end

	-- Check distance to target within a small buffer to overcome floating point error
	local inRange = distToTarget < self.attackRange + 0.1

	self.attackTimer = self.attackTimer - deltaTick
	if inRange and self.attackTimer <= 0 and self.victim and self.victim.alive then
		self.attackTimer = ATTACK_TIME

		self:attack({attacked = self.victim})

		-- Attacked victim gets a threat floor
		self:setThreatFloor(self.victim, ATTACK_THREAT)
		
		if self.attackSoundRepeat > 0 then
			playSound({soundID = self.attackSound})
			self.attackSoundRepeat = self.attackSoundRepeat - 1
		end
	elseif self.victim and not self.victim.alive then
		self.agroTable[tostring(self.victim.PID)] = nil
		if Toolbox.isContainerEmpty(self.agroTable) then
			self:setState(STATE.RETURNING)
		else
			self:evaluateAgroTable()
		end
	end
end

function setWanderLocation(self)
	local wanderPoint = {}
	wanderPoint.x = math.random((self.home.x - self.wanderRange), (self.home.x + self.wanderRange))
	wanderPoint.y = self.home.y
	wanderPoint.z = math.random((self.home.z - self.wanderRange), (self.home.z + self.wanderRange))

	local input = {x = wanderPoint.x, y = wanderPoint.y, z = wanderPoint.z, lookAt = true, rx = self.position.rx, ry = self.position.ry, rz = self.position.rz}
	self:moveTo(input)
end

function despawn(self)
	if self.spawnerPID then
		Events.sendEvent("OBJECT_DESPAWN", {spawnerPID = self.spawnerPID, despawnPID = self.PID, spawnType = "monster"})	
	end
end

function newVictim(self, victim)
	self.victim = victim
	self:setState(STATE.IDLE)
	self:setState(STATE.CHASE)
end

----------------------------------------
-- Event handlers
----------------------------------------

-- Timer handler
function onTimer(self, event)
	self.onServerTick(self, event)
end

-- Wrapper for onTimer
function onServerTick(self, event)
	if self.tick == 0 then self.tick = event.tick end
	local deltaTick = event.tick - self.tick
	self.stateChangeCooldown = self.stateChangeCooldown - deltaTick
	self:handleState(deltaTick)
	self.tick = event.tick
end

-- Called when the monster arrives at its destination
function onMovementArrived(self, event)
	if self.state == STATE.WANDER then
		self:setState(STATE.IDLE)
	elseif self.state == STATE.RETURNING then
		self:setState(STATE.IDLE)
	end
end

function onTrigger(self, event)
	if (self.state ~= STATE.DEAD and self.state ~= STATE.RETURNING) and (self.state ~= STATE.CHASE) and
	   (event.triggerType == 1) and
	   (event.player) and
	   (event.player.alive) and
	   (event.player.mode ~= "God") then
		self:setThreatFloor(event.player, INITIAL_THREAT)
		self:evaluateAgroTable()
	end
end

-- Called when the monster is healed
function onHealed(self, event)
	-- TODO: Un-needed I suppose. I don't reward getting healed.
end

-- Called when the monster is damaged
function onDamage(self, event)
	-- Was I attacked?
	if event.attacked and event.attacked.PID and event.attacked.PID == self.PID then
		-- WHO ATTACKED ME!?
		if self.state ~= STATE.DEAD and event.attacker then
			self:setThreatFloor(event.attacker, INITIAL_THREAT)		
			self:addThreat(event.attacker,  math.ceil(((event.damage or 0)/(self.maxHealth or 100))*100))
			self:evaluateAgroTable()
			Events.sendEvent("FRAMEWORK_CALL_FOR_HELP", {attacked = event.attacked, attacker = event.attacker})
		end
	end
end

function onPlayerDied( self, event )
	if self.state ~= STATE.DEAD and event.player ~= nil and event.player.ID and self.agroTable[event.player.name] then
		self.agroTable[event.player.name] = nil
		if Toolbox.isContainerEmpty(self.agroTable) then
			self:setState(STATE.RETURNING)
		else
			self:evaluateAgroTable()
		end
	end
end

-- Called when an object dies
function onObjectDied(self, event)
	-- Did I just die?
	if (event.object.PID == self.PID) then
		self:setState(STATE.DEAD)
	elseif self.state ~= STATE.DEAD and event.object ~= nil and event.object.PID and self.agroTable[tostring(event.object.PID)] then
		self.agroTable[tostring(event.object.PID)] = nil
		if Toolbox.isContainerEmpty(self.agroTable) then
			self:setState(STATE.RETURNING)
		else
			self:evaluateAgroTable()
		end
	end
end

-- Called when a player changes modes
function onPlayerModeChanged(self, event)
	
	-- Notify anyone that you are about to disapear from battle without dying
	local object = {}
	object.PID = self.PID
	Events.sendEvent("OBJECT_REMOVED_FROM_BATTLE", {object = object})

	-- Ensure this player wasn't our target
	if self.state ~= STATE.DEAD and event.mode == "God" and event.player ~= nil and event.player.ID and self.agroTable[event.player.name] then
		self.agroTable[event.player.name] = nil
		if Toolbox.isContainerEmpty(self.agroTable) then
			self:setState(STATE.RETURNING)
		else
			self:evaluateAgroTable()
		end
	end
end

function callForHelpHandler(self, event)
	if self.state ~= STATE.DEAD then
		if event.attacked == nil or event.attacked.position == nil then
			log("ERROR DETECTED - ED-5707 : event.attacked = "..tostring(event.attacked)..
			                             ", event.attacked.name = "..tostring(event.attacked and event.attacked.name)..
										 ", event.attacked.position = "..tostring(event.attacked and event.attacked.position)..
										 ", self.PID = "..tostring(self.PID)..
										 ", self.UNID = "..tostring(self.UNID))
			return
		end
		local y = event.attacked.position.y
		if math.abs(y - self.position.y) < 10 then	
			local distToAttacked = self:getDistanceToPoint(event.attacked.position)
			if distToAttacked < DEFAULT_ATTACK_MOB_RANGE then
				self:setThreatFloor(event.attacker, INITIAL_THREAT)
				self:evaluateAgroTable()
			end
		end
	end
end

function mobSound(self, event)
	if self.state ~= STATE.DEAD then
		local y = event.attacker.position.y
		local sound_mob_range = DEFAULT_SOUND_MOB_RANGE
		if event.weapon.properties.soundRange then
			sound_mob_range = event.weapon.properties.soundRange * SOUND_AGGRO_SCALE
		end
		if math.abs(y - self.position.y) < 10 then
			local distToAttacked = self:getDistanceToPoint(event.attacker.position)
			if distToAttacked < sound_mob_range then
				self:setThreatFloor(event.attacker, INITIAL_THREAT)
				self:evaluateAgroTable()
			end
		end
	end
end