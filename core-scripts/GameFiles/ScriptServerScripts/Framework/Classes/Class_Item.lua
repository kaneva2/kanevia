--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_Item.lua
--
-- Super class for all items
--
----------------------------------------
Class.createClass("Class_Item", "Class_Base")

-- TODO: Is this class layer needed?
function create(self, input)
	_super.create(self, input)
end