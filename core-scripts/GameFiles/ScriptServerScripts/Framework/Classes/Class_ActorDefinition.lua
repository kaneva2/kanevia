--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- Class_Actor.lua
-- Super class for all NPC vendors, Quest Givers and dialog
----------------------------------------

include("Lib_SaveLoad.lua")
include("Lib_GameDefinitions.lua")
include("Lib_NpcDefinitions.lua")
-- Bind libraries to a local variable for efficiency's sake
local NpcDefs = _G.NpcDefinitions

-----------------------------------------
-- Class specification
-----------------------------------------
Class.createProperties({
	"gender",
	"interactionRange",
	"showLabels",
	"followerOf",
	"spouseOf"
})

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local Events			= _G.Events
local Players			= _G.Players

-------------------------------------------
-- Constants
-------------------------------------------
local DEFAULT_IDLE_ANIMATION_M  = 3563579
local DEFAULT_IDLE_ANIMATION_F  = 3931355
local DEFAULT_INTERACTION_RANGE = 25

local SOUND_GIFT  = 4724389
local SOUND_FOLLOWER = 4724387

local ACTOR_KEY = "DNC_Actor"
local DEFAULT_MODE_LABELS		= {
	God		= {"actorName", "actorTitle"},
	Player	= {"actorName", "actorTitle"}
}

local MILLISECONDS_PER_SECOND = 1000
local SECONDS_PER_MINUTE = 60
local SECONDS_PER_HOUR = SECONDS_PER_MINUTE * 60
local SECONDS_PER_DAY = SECONDS_PER_HOUR * 24
local MILLISECONDS_PER_MINUTE = MILLISECONDS_PER_SECOND * SECONDS_PER_MINUTE
local MILLISECONDS_PER_DAY = SECONDS_PER_DAY * MILLISECONDS_PER_SECOND

local REP_SHUN_CHECK_INTERVAL = MILLISECONDS_PER_SECOND * SECONDS_PER_HOUR -- PLAYEST VALUE: 1000 = once per second
local MILLISECONDS_BETWEEN_SHUNS = 1 * MILLISECONDS_PER_DAY -- BUG TEST VALUE: 5000 = every 5 seconds
local MILLISECONDS_UNTIL_SHUN = 3 * MILLISECONDS_PER_DAY -- BUG TEST VALUE: 1000 = one second

s_type = "Actor"

-- Instantiate an instance of NPC
function create(self, input)
	if input == nil then input = {} end

	self.actorName = input.actorName or ""
	self.targetable = input.targetable or "false"
	
	-- Multiple dialog elements
	self.dialogPages = {}
	self.dialogExists = false
	if input.dialogPages then
		for i, v in pairs(input.dialogPages) do
			self.dialogPages[tonumber(i)] = v
			if v ~= "" then
				self.dialogExists = true
			end
		end
	end
	-- Single dialog element
	self.dialog = input.dialog
	if self.dialog ~= nil and self.dialog ~= "" then
		if self.dialogExists == false then
			-- There is no table of dialogue pages, so populate it with this one element instead
			-- NOTE: this means that an actor CANNOT use both dialog AND dialogPages properties. This is to ensure backwards compatability only.
			table.insert(self.dialogPages, {dialogPage = self.dialog})
			self.dialogExists = true
		end
	end
	if self.dialogExists then
		local tooltipText = "Click to talk to Actor"
		if self.actorName ~= "" then tooltipText = "Click to talk to "..self.actorName.."." end
		self.tooltip = tooltipText
	end
	
	self.actorTitle = input.actorTitle or ""
	input.modeLabels = DEFAULT_MODE_LABELS
	
	if input.showLabels == nil then
		input.showLabels = true
	end
	self.showLabels = input.showLabels or false

	self.repList = {}
	self.repScale = input.repScale or "Normal"
	self.repEnabled = input.repEnabled or false
	self.repUnlock = input.repUnlock or "Acquaintance"

	self.giftedList = {}
	
	self.shunTimer = REP_SHUN_CHECK_INTERVAL
	self.tick = 0

	self.giftBasic = input.giftBasic
	self.giftStandard = input.giftStandard
	self.giftSpecial = input.giftSpecial
	self.giftInterval = input.giftInterval or 0
	self.giftInterval = self.giftInterval * SECONDS_PER_MINUTE * MILLISECONDS_PER_SECOND -- Convert from minutes (prefab) to MILLISECONDS (local use)

	-- For Follower version
	self.followerOf = nil
	self.spouseOf = nil
	--Follower clothing
	self.equippedGLIDs = {}
	self.meshGLID = input.meshGLID
	self.customization = input.customization
	-- For Jango version
	self.followList = {}

	self.jango = nil -- PID of the master all follower clones are derived from

	self.behavior = input.behavior
	self.isRider = false
	if self.behavior then
		if string.sub(self.behavior, -6, -1) == "_rider" then
			self.isRider = true
		end
	end
	
	_super.create(self, input)

	-- Load current state
	self:loadActorState("default")
	
	if not self.showLabels then
		self:clearLabels()
	end
	
	-- User-definable variables (Public, document for users)
	self.gender = "female"
	if input.gender and input.gender == "male" then
		self.gender = input.gender
	end
	
	if self.gender == "female" then
		self.animation = tonumber(input.animation) or DEFAULT_IDLE_ANIMATION_F
	else
		self.animation = tonumber(input.animation) or DEFAULT_IDLE_ANIMATION_M
	end
	
	if self.animation ~= nil and self.animation > 0 then
		self:setAnimation(self.animation)
	end
	
	self.interactionRange = input.interactionRange or DEFAULT_INTERACTION_RANGE

	self.trigger = Events.setTrigger(self.interactionRange, self.onTrigger, self)
	
	-- Register players that enter the zone
	Events.registerHandler("PLAYER-ARRIVE", self.onArrive, self)
	Events.registerHandler("ACTIVATE_ACTOR", self.activateActorHandler, self)	

	Events.registerHandler("REQUEST_INIT_NPC_GIFTING", self.requestInitNpcGiftingHandler, self)
	Events.registerHandler("UPDATE_NPC_REP", self.updateNpcRepHandler, self)
	Events.registerHandler("RESET_NPC_REP_FOR_PLAYER", self.resetNpcRepForPlayer, self)
	Events.registerHandler("REQUEST_JANGO_STATE", self.onRequestJangoState, self)
	Events.registerHandler("RETURN_JANGO_STATE", self.onReturnJangoState, self)
	Events.registerHandler("REMOVE_CLONE_FROM_JANGO", self.removeCloneFromJango, self)
	Events.registerHandler("CHECK_IF_NPC_SPOUSE_EXISTS", self.onCheckIfNpcSpouseExists, self)
	Events.registerHandler("CHECK_IF_NPC_FOLLOWERS_EXIST", self.onCheckIfNpcFollowersExist, self)
	Events.registerHandler("FRAMEWORK_REQ_GIFT_READY", self.onRequestGiftReady, self)
	Events.registerHandler("FRAMEWORK_UPDATE_NPC_SPOUSE", self.onUpdateNpcSpouse, self)
	Events.registerHandler("FRAMEWORK_UPDATE_FOLLOWER_COUNT", self.onUpdateFollower, self)
	Events.registerHandler("DISMISS_FOLLOWER", self.onDismissFollower, self)
	Events.registerHandler("FRAMEWORK_HIDE_FOLLOWER", self.onHideFollower, self)
	Events.registerHandler("FRAMEWORK_UPDATE_NPC_CLOTHING", self.updateNPCClothing, self)
	Events.registerHandler("REMOVE_JANGE_CLONES", self.onRemoveJangoClones, self)

    self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)

    -- Object association events, used for querying the controller and processing the response
	Events.registerHandler("FRAMEWORK_RETURN_OBJECT_ASSOCIATIONS", self.onReturnObjectAssociations, self)
	Events.sendEvent("FRAMEWORK_REQUEST_OBJECT_ASSOCIATIONS", {PID = self.PID})
end

function destroy(self)
	-- TO DO: Send a cleanup event to the Jango
	_super.destroy(self)
	
	Events.unregisterHandler("PLAYER-ARRIVE", self.onArrive, self)
	Events.unregisterHandler("ACTIVATE_ACTOR-ARRIVE", self.onArrive, self)

	Events.unregisterHandler("REQUEST_INIT_NPC_GIFTING", self.requestInitNpcGiftingHandler, self)
	Events.unregisterHandler("UPDATE_NPC_REP", self.updateNpcRepHandler, self)
	Events.unregisterHandler("RESET_NPC_REP_FOR_PLAYER", self.resetNpcRepForPlayer, self)
	Events.unregisterHandler("REQUEST_JANGO_STATE", self.onRequestJangoState, self)
	Events.unregisterHandler("RETURN_JANGO_STATE", self.onReturnJangoState, self)
	Events.unregisterHandler("REMOVE_CLONE_FROM_JANGO", self.removeCloneFromJango, self)
	Events.unregisterHandler("CHECK_IF_NPC_SPOUSE_EXISTS", self.onCheckIfNpcSpouseExists, self)
	Events.unregisterHandler("CHECK_IF_NPC_FOLLOWERS_EXIST", self.onCheckIfNpcFollowersExist, self)
	Events.unregisterHandler("FRAMEWORK_REQ_GIFT_READY", self.onRequestGiftReady, self)
	Events.unregisterHandler("FRAMEWORK_UPDATE_NPC_SPOUSE", self.onUpdateNpcSpouse, self)
	Events.unregisterHandler("FRAMEWORK_UPDATE_FOLLOWER_COUNT", self.onUpdateFollower, self)
	Events.unregisterHandler("DISMISS_FOLLOWER", self.onDismissFollower, self)
	Events.unregisterHandler("FRAMEWORK_HIDE_FOLLOWER", self.onHideFollower, self)
	Events.unregisterHandler("FRAMEWORK_UPDATE_NPC_CLOTHING", self.updateNPCClothing, self)
	Events.unregisterHandler("REMOVE_JANGE_CLONES", self.onRemoveJangoClones, self)

	Events.unregisterHandler("FRAMEWORK_RETURN_OBJECT_ASSOCIATIONS", self.onReturnObjectAssociations, self)
	Events.unregisterHandler("ACTIVATE_ACTOR", self.activateActorHandler, self)
	
	if self.trigger then
		Events.removeTrigger(self.trigger)
	end

	if self.proxTrigger then
		Events.removeClientTrigger(self.proxTrigger)
	end

	if self.followerOf ~= nil then
		Events.sendEvent("REMOVE_CLONE_FROM_JANGO", {jangoPID = self.jango, playerName = self.followerOf})
		Events.sendEvent("FRAMEWORK_UPDATE_FOLLOWER_COUNT", {changeAmt = (-1), playerName = self.followerOf})
	end
	if self.spouseOf ~= nil then
		Events.sendEvent("RESET_NPC_SPOUSE_FOR_PLAYER", {playerName = self.spouseOf})
	end

	-- Erase database content for this item if being removed from the world
	if(self.isBeingRemoved) then
		if self.jango == nil then -- If not a follower
			Events.sendEvent("REMOVE_JANGE_CLONES", {jangoPID = self.PID}) -- Remove any related followers
		end
		-- Item is being removed from the world as opposed to the world getting shut down
		self:deleteActorState("default")
	end
end

----------------------------------------
-- Event handlers
----------------------------------------
-- Right click handler
function onRightClick(self, event)
	log("Class_Actor - onRightClick")
	local uPlayer = event.player
	if uPlayer == nil then return end
	-- Activate actor
	self:preActivate(event)
end

-- Left click handler
function onLeftClick(self, event)
	log("Class_Actor - onLeftClick")
	local uPlayer = event.player
	if uPlayer == nil then return end
	-- Activate actor
	self:preActivate(event)
end

-- onArrive handler
function onArrive(self, event)
	self.onPlayerArrive(self, event)
end

-- activateActorHandler handler
function activateActorHandler(self, event)
	local uPlayer = event.player
	local iPID = event.PID
	if uPlayer == nil then return end
	if iPID == nil then return end

	if iPID == self.PID then
		self:onActivate(event)
	end
end

-- requestInitNpcGiftingHandler handler
function requestInitNpcGiftingHandler(self, event)
	local uPlayer = event.player
	local iPID = event.PID
	if uPlayer == nil then return end
	if iPID ~= self.PID then return end

	local iRep = self:getRepByPlayer(uPlayer.name)
	local npcProperties = {
		actorPID = self.PID,
		actorUNID = self.UNID,
		actorBehavior = self.behavior,
		repScore = iRep,
		isJango = self:checkForFollowerByPlayer(uPlayer.name),
		isFollower = uPlayer.name == self.followerOf,
		isSpouse = uPlayer.name == self.spouseOf,
		isRider = self.isRider
	}
	Events.sendClientEvent("FRAMEWORK_INIT_NPC_GIFTING", npcProperties, uPlayer.ID) -- Send event to gifting menu
end

function updateNpcRepHandler(self, event)
	local uPlayer = event.player
	local sPlayerName = event.playerName
	local iRepChange = event.repChange
	local iPID = event.PID

	if uPlayer == nil and sPlayerName == nil then return end -- Accepts either a player object or a playerName string
	if iRepChange == nil then return end
	if iPID ~= self.PID then return end -- Not for me

	if uPlayer and uPlayer.name then
		sPlayerName = uPlayer.name
	end

	local prevRep = self.repList[sPlayerName] or 0
	if self.repList[sPlayerName] == nil then
		self.repList[sPlayerName] = iRepChange -- Create an entry if it does not exist yet
	else
		self.repList[sPlayerName] = self.repList[sPlayerName] + iRepChange
	end
	-- Enforce a maximum reputation score
	if self.repScale and self.repList[sPlayerName] > NpcDefs.REP_SCALES[self.repScale][NpcDefs.REP_LEVELS[NpcDefs.MAX_REP_LEVEL]] then
		self.repList[sPlayerName] = NpcDefs.REP_SCALES[self.repScale][NpcDefs.REP_LEVELS[NpcDefs.MAX_REP_LEVEL]]
	end

	-- If receiving a gift, then update the appropriate tracking values
	if iRepChange > 0 then
		self.currentTime = (kgp.gameGetCurrentTime() * 1000) or 0 -- absolute time in milliseconds

		self.giftedList[sPlayerName] = self.currentTime -- Tracks 1 day gifting limit
		self.lastGiftedDate = self.currentTime -- Tracks rep deprecation for followers
		self.nextShunDate = self.lastGiftedDate + MILLISECONDS_UNTIL_SHUN
		if uPlayer then
			playerPlaySound({user = event.player, soundGLID = SOUND_GIFT})
			self:setParticle(4711248) -- TO DO: Make this only visible to the player who triggered it
			-- Don't generate floater if already at max reputation
			if self.repScale and prevRep < NpcDefs.REP_SCALES[self.repScale][NpcDefs.REP_LEVELS[NpcDefs.MAX_REP_LEVEL]] then
				Events.sendClientEvent("GENERATE_FLOATER", {text = "+"..tostring(iRepChange).." Reputation", target = self.PID, color = {a=255, r=0, g=255, b=255}}, uPlayer.ID)
			end
		end
	end

	self:saveActorState("default")
end

function resetNpcRepForPlayer(self, event)
	local sPlayerName = event.playerName
	local iPID = event.PID

	if sPlayerName == nil then return end
	if iPID ~= self.PID then return end -- Not for me

	if uPlayer then
		sPlayerName = uPlayer.name
	end

	if self.repList[sPlayerName] ~= nil then
		self.repList[sPlayerName] = 0 -- TO DO: get what the default value SHOULD be
	end

	self:saveActorState("default")
end

-- Table of objects associated with this PID received from the game controller
function onReturnObjectAssociations(self, event)
	if event.PID ~= self.PID then return end -- Ignore if this event is not for us
	if event.objectAssociations == nil then return end
	if event.objectAssociations[1] == self.PID and event.objectAssociations[2] ~= self.PID then -- First index is own PID, second index is PID of the master copy that this follower is cloned from
		-- If jango and other data have not already been loaded in by the save state (i.e. this follower made just now for the first time)
		if self.jango == nil then
			self.jango = tonumber(event.objectAssociations[2])
			self.followerOf = self.owner -- Not just an NPC anymore
			self:saveActorState("default")
			self:updateStateLabels()
			Events.sendEvent("REQUEST_JANGO_STATE", {jangoPID = self.jango, clonePID = self.PID, playerName = self.followerOf})
		else
			self:checkForMissedShuns()
			self:saveActorState("default")
			if self.pendingDeath == true then
				self:kill()
			end
		end
	end
end

function onRequestJangoState(self, event)
	local sPlayerName = event.playerName
	local iJangoPID = event.jangoPID
	local iClonePID = event.clonePID
	
	if iJangoPID ~= self.PID then return end
	if iClonePID == nil then return end
	if sPlayerName == nil then return end

	local jangoState = {}
	jangoState.repList = {}
	local tempRep = self:getRepByPlayer(sPlayerName)
	jangoState.repList[sPlayerName] = tempRep

	-- Track association between player and follower clones
	self.followList[sPlayerName] = iJangoPID

	self:saveActorState("default")

	Events.sendEvent("RETURN_JANGO_STATE", {clonePID = iClonePID, jangoState = jangoState})
end

function onReturnJangoState(self, event)
	local iClonePID = event.clonePID
	local tJangoState = event.jangoState
	
	if iClonePID ~= self.PID then return end
	if tJangoState == nil then return end
	
	if tJangoState.repList then
		self.repList = event.jangoState.repList
		self.currentTime = (kgp.gameGetCurrentTime() * 1000) or 0 -- absolute time in milliseconds
		self:checkForMissedShuns()
		-- self.lastGiftedDate = self.currentTime
		-- self.nextShunDate = self.lastGiftedDate + MILLISECONDS_UNTIL_SHUN
		self:saveActorState("default")
		if self.pendingDeath == true then
			self:kill()
		end
	end

	-- Done initializing all values. Now, update title.
	if self.spouseOf ~= nil and self.actorTitle ~= self.spouseOf then
		self.actorTitle = self.spouseOf
		self:updateStateLabels()
	end

	self:setParticle(4700999)
end

function removeCloneFromJango(self, event)
	local sPlayerName = event.playerName
	local iJangoPID = event.jangoPID
	
	if iJangoPID ~= self.PID then return end
	if sPlayerName == nil then return end

	-- Reset rep for owner of the clone
	if self.repList[sPlayerName] ~= nil then
		self.repList[sPlayerName] = 0
	end

	-- Remove clone from follow list
	if self.followList[sPlayerName] ~= nil then
		self.followList[sPlayerName] = -1
	end

	-- Reset ability to give gifts today
	if self.giftedList[sPlayerName] ~= nil then
		self.giftedList[sPlayerName] = nil
	end

	self:saveActorState("default")
end

function onCheckIfNpcSpouseExists(self, event)
	local sPlayerName = event.playerName
	if sPlayerName == nil then return end
	if self.spouseOf == sPlayerName then
		Events.sendEvent("RETURN_NPC_SPOUSE_EXISTS", {PID = self.PID, playerName = sPlayerName})
	end
end

function onCheckIfNpcFollowersExist(self, event)
	local sPlayerName = event.playerName
	if sPlayerName == nil then return end
	if self.followerOf == sPlayerName then
		Events.sendEvent("RETURN_NPC_FOLLOWER_EXISTS", {PID = self.PID, playerName = sPlayerName})
	end
end

function onRequestGiftReady(self, event)
	local iPID = event.PID
	local uPlayer = event.player

	if iPID ~= self.PID then return end -- Not for me
	if uPlayer == nil or uPlayer.name == nil then return end

	self.currentTime = (kgp.gameGetCurrentTime() * 1000) or 0 -- absolute time in milliseconds
	local tempGiftReady = false
	local timeRemaining = 0

	if self.giftedList[uPlayer.name] == nil then
		tempGiftReady = true
	elseif self.giftedList[uPlayer.name] then
		if self.giftedList[uPlayer.name] + self.giftInterval <= self.currentTime then
			tempGiftReady = true
		else
			local targetTime = self.giftedList[uPlayer.name] + self.giftInterval
			timeRemaining = math.max(targetTime - self.currentTime, 0)
		end
	end

	Events.sendClientEvent("FRAMEWORK_RETURN_GIFT_READY", {giftReady = tempGiftReady, timeRemaining = timeRemaining}, uPlayer.ID)
end

function onUpdateNpcSpouse(self, event)
	local iPID = event.PID
	local uPlayer = event.player

	if iPID ~= self.PID then return end -- Not for me
	if uPlayer == nil or uPlayer.name == nil then return end

	self.spouseOf = uPlayer.name
	playerPlaySound({user = event.player, soundGLID = 4716684})
	self:setParticle(4716743)
	Events.setTimer(5, function() self:removeParticle() end)

	self:updateStateLabels()

	self:saveActorState("default")
end

function onUpdateFollower(self, event)
	if not event.player then return end
	if self.PID == event.PID then
		playerPlaySound({user = event.player, soundGLID = SOUND_FOLLOWER})
	end
end

function updateStateLabels(self)
	if self.showLabels then
		self:clearLabels()

		-- Shows the actor's spouse information
		local labels = {}
		table.insert(labels, {self.actorName, 0.016})
		if self.spouseOf ~= nil then
			table.insert(labels, {"Spouse of "..self.spouseOf, 0.015, 0.9, 0.5, 0.2})
		elseif self.followerOf ~= nil then
			table.insert(labels, {"Follower of "..self.followerOf, 0.015, 0.9, 0.5, 0.2})
		else
			table.insert(labels, {self.actorTitle, 0.015, 0.9, 0.5, 0.2})
		end

		if #labels > 0 then
			self:addLabels(labels)
		end
	end
end

function getRepByPlayer(self, playerName)
	if playerName == nil then return end
	if self.repList[playerName] == nil then
		self.repList[playerName] = 0
	end

	return self.repList[playerName]
end

function checkForFollowerByPlayer(self, playerName)
	if playerName == nil then return end
	if self.followList[playerName] == nil then
		self.followList[playerName] = -1
		return false
	elseif self.followList[playerName] == -1 then
		return false
	end

	return true
end

-- Wrapper for player arrive
function onPlayerArrive(self, event)
	local player = event.player
	if player == nil or Players.validatePlayerID(player.ID) == false then
		return
	end
end

function onTrigger(self, event)
	local player = event.player
	if player == nil then return end

	-- If player exits trigger radius and actor is active, close dialog
	if event.triggerType == 2 then
		if player.activeMenuPID and player.activeMenuPID == self.PID  then
			Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, player.ID)
			player.activeMenuPID = nil
		end
	elseif event.triggerType == 1 then
		Events.sendClientEvent("ACTOR_DELIVERY_TRIGGERED", {UNID = self.UNID}, player.ID)
	end
end

function onDismissFollower(self, event)
	if self.PID ~= event.PID then return end
	self:kill()
end

function onHideFollower(self, event)
	if self.PID == event.PID then
		if event.player and event.hide then
			if self.proxTrigger then
				Events.removeClientTrigger(self.proxTrigger)
				self.proxTrigger = nil
			end
			kgp.objectHide(self.PID, event.player.ID)
		else
			if self.proxTrigger == nil then
				self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
			end
			kgp.objectShow(self.PID, event.player.ID)
		end
	end
end

function onRemoveJangoClones(self, event)
	if event.jangoPID == nil then return end
	if event.jangoPID == self.jango then
		self:kill()
	end
end

function updateNPCClothing(self, event)
	if self.PID == event.PID then
		self.equippedGLIDs = event.GLIDS
		self:dressUpFollower()
		self:saveActorState("default")

		if self.proxTrigger == nil then
			self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
		end
		kgp.objectShow(self.PID, event.player.ID)
	end
end
----------------------------------------
-- Class functions
----------------------------------------

-- Validate all conditions necessary before onActivate
function preActivate(self, event)
	log("Class_Actor - preActivate")
	local uPlayer = event.player
	if not Players.validatePlayer(uPlayer) then
		log("Class_Actor - onActivate: player validation failed")
		return false
	end
	
	if uPlayer.mode ~= "Player" then
		return false
	end

	if uPlayer.alive == nil or uPlayer.alive == false then
		kgp.playerShowStatusInfo(uPlayer.ID, "You cannot do that while dead.", 3, 255, 0, 0)
		return false
	end
	
	if uPlayer.inVehicle then
		kgp.playerShowStatusInfo(uPlayer.ID, "You cannot do that while in a vehicle.", 3, 255, 0, 0)
		return false
	end
	if self:isPointWithinRange(uPlayer.position, self.interactionRange) then
		--if self.dialogExists then
			if self.repEnabled then
				if (self.followerOf == uPlayer.name or self.followerOf == nil) then
					Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)
					uPlayer.activeMenuPID = self.PID
					kgp.playerLoadMenu(uPlayer.ID, "Framework_NPCActionMenu.xml")
					local eventProperties = {
						actorPID = self.PID,
						UNID = self.UNID,
						value = self:getRepByPlayer(uPlayer.name),
						isFollower = self.followerOf == uPlayer.name,
						isSpouse = self.spouseOf == uPlayer.name,
						isRider = self.isRider,
						equippedGLIDs = self.equippedGLIDs
					}
					Events.sendClientEvent("FRAMEWORK_DEFINE_NPC_ACTIONS", eventProperties, uPlayer.ID)
					Events.sendClientEvent("FULFILL_ACTOR_TALK_TO", {UNID = self.UNID}, uPlayer.ID)
				-- Cannot activate another player's spouse that unlocks at the spouse level (but CAN activate another player's follower that unlocks at the folloer level)
				elseif self.repUnlock == NpcDefs.SPECIAL_LEVELS[2] and self.spouseOf ~= uPlayer.name then
					kgp.playerShowStatusInfo(uPlayer.ID, "Only their spouse can do that.", 3, 255, 0, 0)		
				else
					self:onActivate(event)
				end
			else
				self:onActivate(event)
			end
		--end
	else
		-- Status menu
		kgp.playerShowStatusInfo(uPlayer.ID, "You must be closer to interact with that object.", 3, 255, 0, 0)
	end
	return false
end


-- Called on activationuPlayer
function onActivate(self, event)
	log("Class_Actor - onActivate")
	local uPlayer = event.player
	if uPlayer == nil then return end

	if not self.dialogExists then return end

	Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)

	uPlayer.activeMenuPID = self.PID
	kgp.playerLoadMenu(uPlayer.ID, "Framework_NPCDialog.xml")
	Events.sendClientEvent("SET_DIALOG_TEXT", {name = self.actorName, text = Toolbox.deepCopy(self.dialogPages), isRider = self.isRider, riderPID = self.PID}, uPlayer.ID)
	Events.sendClientEvent("FULFILL_ACTOR_TALK_TO", {UNID = self.UNID}, uPlayer.ID)
end

----------------------------------------
-- Class Functions (Database Interactions)
----------------------------------------

function saveActorState(self, saveString)
	local ActorData = {}
	ActorData.repList = self.repList
	ActorData.giftedList = self.giftedList
	ActorData.followList = self.followList
	ActorData.followerOf = self.followerOf
	ActorData.spouseOf = self.spouseOf
	ActorData.jango = self.jango
	ActorData.lastGiftedDate = self.lastGiftedDate
	ActorData.lastShunDate = self.lastShunDate
	ActorData.equippedGLIDs = self.equippedGLIDs
		
	local ActorDataString = Events.encode(ActorData)
	
	local saveLoc = ""
	if saveString == "default" then
		saveLoc = ACTOR_KEY .. "_" .. tostring(self.PID), ActorDataString
	else
		saveLoc = saveString
	end
	SaveLoad.saveGameData(saveLoc, ActorDataString)
end


function loadActorState(self, saveString)
	self.currentTime = (kgp.gameGetCurrentTime() * 1000) or 0 -- absolute time in milliseconds
	
	local saveLoc = ""
	if saveString == "default" then
		saveLoc = ACTOR_KEY .. "_" .. tostring(self.PID)
	else
		saveLoc = saveString
	end
	local ActorDataString = SaveLoad.loadGameData(saveLoc)

	local ActorData = {}
	if ActorDataString then
		ActorData = Events.decode(ActorDataString)
	end

	self.repList = ActorData.repList or {}
	self.giftedList = ActorData.giftedList or {}
	self.followList = ActorData.followList or {}
	self.followerOf = ActorData.followerOf or nil
	self.spouseOf = ActorData.spouseOf or nil
	self.jango = ActorData.jango or nil
	self.lastGiftedDate = ActorData.lastGiftedDate or (self.currentTime - self.giftInterval)
	self.equippedGLIDs = ActorData.equippedGLIDs or {}
	if self.customization and ( next(self.equippedGLIDs) ) then
		self:dressUpFollower()
	end

	-- Do we know when the last shun occurred and is it a valid value (i.e. equal to or past the deadline to start shunning)
	if ActorData.lastShunDate and ActorData.lastShunDate >= self.lastGiftedDate + MILLISECONDS_UNTIL_SHUN then
		-- Use the save data for the last shun
		self.lastShunDate = ActorData.lastShunDate
	end

	self:saveActorState("default")
end

function deleteActorState(self, saveString)
	if saveString == "default" then
		saveLoc = ACTOR_KEY .. "_" .. tostring(self.PID)
	else
		saveLoc = saveString
	end
	
	SaveLoad.deleteGameData(saveLoc)
end

-- Timer handler
function onTimer(self, event)
	self.onServerTick(self, event)
end

-- Wrapper for onTimer
function onServerTick(self, event)
	if self.tick == 0 then self.tick = event.tick end
	local deltaTick = event.tick - self.tick
	self:updateShunTimer(deltaTick)
	-- If you are a hybrid actor_pet, then call the PET version of onServerTick
	if self.onServerPetTick then
		self.onServerPetTick(self, event)
	end
	-- If you are a hybrid actor_rider, then call the PATH_RIDER version of onServerTick
	if self.OnPathRiderTick then
		self.OnPathRiderTick(self, event)
	end
	self.tick = event.tick
end

function updateShun(self)
	-- Check if the actor is a follower, has a repList, and has a reputation for the player they are following
	if self.followerOf and self.repList and self.repList[self.followerOf] then
		-- Check that a repScale exists before indexing it
		if self.repScale then
			local oldRep = self.repList[self.followerOf]
			-- Determine decrmented rep value based on rep scale
			local newRep = oldRep - NpcDefs.REP_SCALES[self.repScale]["Shun"]
			-- If rep reduced to friendly, then no longer a valid follower
			if newRep <= NpcDefs.REP_SCALES[self.repScale][NpcDefs.REP_ABANDON_LEVEL] then
				self.pendingDeath = true
				if self.jango ~= nil then
					self:kill()
				end
			else -- Otherwise, decrement rep normally
				self.repList[self.followerOf] = newRep
				-- TO DO: Send event to update relevant open menus with new reputation values
			end
		end
	end
end

-- Reduces reputation for the actor if it is a follower
function updateShunTimer(self, deltaTick)
	self.shunTimer = self.shunTimer - deltaTick
	if self.shunTimer <= 0 then
		self.currentTime = (kgp.gameGetCurrentTime() * 1000) or 0 -- absolute time in milliseconds
		if self.nextShunDate and self.currentTime >= self.nextShunDate then
			-- log("\n shun tick")
			self:updateShun()
			self.lastShunDate = self.currentTime
			self.nextShunDate = self.currentTime + MILLISECONDS_BETWEEN_SHUNS
			self:saveActorState("default")
		end
		self.shunTimer = REP_SHUN_CHECK_INTERVAL
	end
end

 function dressUpFollower(self)
	local tempCharConfig = self.meshGLID
	tempCharConfig = string.gsub(tempCharConfig, "\'", "\"")
	tempCharConfig = Events.decode(tempCharConfig)

	if not tempCharConfig then return end


	tempCharConfig["GLIDS"] = self.equippedGLIDs
	tempCharConfig["faceIndex"] = tempCharConfig["faceIndex"] - 1
	tempCharConfig = Events.encode(tempCharConfig)
	--tempCharConfig = '{"scaleY":101,"skinColor":1,"faceIndex":4,"GLIDS":[1360,1249,3180392],"beardColor":3,"faceCover":10,"scaleXZ":101,"dbIndex":11,"hairColor":7,"hairIndex":20,"eyebrowIndex":5,"eyeColor":7} '

	 kgp.npcSpawn{
            PID = self.PID,
            charConfig = tempCharConfig,
            pos = { x = self.position.x or 0, y = self.position.y or 0, z = self.position.z or 0 }, 
            rotDeg = Math.vectorToDegrees(self.position.rx or 0, self.position.rz or 1),
            glidAnimSpecial = 0,
            glidsArmed = {}
        }
 end

 function checkForMissedShuns(self)
 	self.currentTime = (kgp.gameGetCurrentTime() * 1000) or 0 -- absolute time in milliseconds
	-- Should a shun have happened since the last gift was given
	local pastDeadline = (self.currentTime >= self.lastGiftedDate + MILLISECONDS_UNTIL_SHUN)
	-- log("\n self.currentTime: "..tostring(self.currentTime))
	-- log("\n self.lastGiftedDate: "..tostring(self.lastGiftedDate))
	-- log("\n pastDeadline: "..tostring(pastDeadline))
	if pastDeadline then
		-- How much are we past the deadline to start shunning
		local amtPastDeadline = (self.currentTime - self.lastGiftedDate) - MILLISECONDS_UNTIL_SHUN
		-- log("\n amtPastDeadline: "..tostring(amtPastDeadline))
		-- Do we know when the last shun occurred and is it a valid value (i.e. equal to or past the deadline to start shunning)
		if self.lastShunDate and self.lastShunDate >= self.lastGiftedDate + MILLISECONDS_UNTIL_SHUN then
			-- Update how much we are past the deadline accordingly (accounts for shuns already given)
			amtPastDeadline = (self.currentTime - self.lastShunDate) - MILLISECONDS_BETWEEN_SHUNS
		end
		-- log("\n adjusted amtPastDeadline: "..tostring(amtPastDeadline).."\n (accounts for shuns already given)")
		-- How many shuns should have happened since the last shun was last saved
		local deficitShuns = math.floor(amtPastDeadline / MILLISECONDS_BETWEEN_SHUNS)
		-- log("\n deficitShuns: "..tostring(deficitShuns))
		-- Catch up on missed shuns
		for i=1,deficitShuns do
			if self.pendingDeath then break end
			self:updateShun()
		end
		-- What is the time remaining until the next shun
		local amtTillNextDeadline = amtPastDeadline - MILLISECONDS_UNTIL_SHUN - (deficitShuns * MILLISECONDS_BETWEEN_SHUNS)
		-- MILLISECONDS_BETWEEN_SHUNS - (amtPastDeadline % (MILLISECONDS_BETWEEN_SHUNS))
		-- Start the countdown until the next shun
		self.nextShunDate = self.currentTime + amtTillNextDeadline
		-- Now that retroactive shunning has happened, update the last shun date to BEFORE the current time
		self.lastShunDate = self.nextShunDate - MILLISECONDS_BETWEEN_SHUNS
	else
		self.lastShunDate = self.lastGiftedDate
		self.nextShunDate = self.lastGiftedDate + MILLISECONDS_UNTIL_SHUN
	end

	self:saveActorState("default")
 end

