--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_Trap.lua
--
-- Trap class inherited from Placeables
--
-- User-definable variables
-- STRING
-- NUMBER
-- TABLE
--
-- User-accessible methods
--
---------------------------------------
include("Lib_GameDefinitions.lua")


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local GameDefinitions	= _G.GameDefinitions
local Events			= _G.Events
local Class				= _G.Class

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------

Class.createClass("Class_Trap", "Class_BattleEntity")

Class.createProperties({
	"percentDamage"
})


-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local DEFAULT_NAME = "Trap"
local DEFAULT_DRAW_DISTANCE = 1800

local ATTACK_TIME = 1000
-- local ATTACK_SOUND_REPEAT_COUNT = 2

-- States for Placeable
local STATE = {
	IDLE		= 0,
	ATTACKING	= 1,
	DYING		= 2,
}

s_type = "trap"


-- Instantiate an instance of Placeables
function create(self, input)
	-- TODO: Error check input values
	if input == nil then input = {} end
	_super.create(self, input)
	self:setDrawDistance(DEFAULT_DRAW_DISTANCE)
	
	self.targetable = input.targetable or "true"
	self.healthMultiplier = input.healthMultiplier or 1
	
	-- Old static members
	self.tick = 0
	self.attackTable = {}
	
	-- User-definable variables (Public, document for users)
	self.name = input.name  or DEFAULT_NAME

	self.idleParticleGLID = input.idleParticle
	self.idleSoundGLID = input.idleSound
	self.damageSoundGLID = input.damageSound

	self.percentDamage = input.percentDamage

	self:setState(STATE.IDLE)

	-- Register for timer event
	Events.registerHandler("TIMER", self.onTimer, self)
	Events.registerHandler("COLLIDE", self.onCollide, self)
	Events.registerHandler("OBJECT_HEALED", self.onHealed, self)
	Events.registerHandler("OBJECT_DAMAGED", self.onDamage, self)
	Events.registerHandler("PLAYER_DIED", self.onPlayerDied, self)
	Events.registerHandler("OBJECT_DIED", self.onObjectDied, self)
	Events.registerHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)

	--self:setMouseOver(5)
	
	-- Register for health bars
	self:enableHealthBar({})
	
	if self.idleSoundGLID then
		-- Generate sounds
		local tSoundParams = GameDefinitions.generateGenericSoundParams(self.PID, self.idleSoundGLID, 20)
		tSoundParams.looping = 1
		self.idleSound = generateSound(tSoundParams)
		
		-- Set idle sound
		playSound({soundID = self.idleSound})
	end

	if self.damageSoundGLID then
		local tSoundParams = GameDefinitions.generateGenericSoundParams(self.PID, self.damageSoundGLID, 20)
		self.damageSound = generateSound(tSoundParams)
	end
	
	if self.idleParticleGLID then
		-- Set idle particle
		self:setParticle(self.idleParticleGLID)
	end
end

function destroy(self)
	_super.destroy(self)
	
	Events.unregisterHandler("TIMER", self.onTimer, self)
	Events.unregisterHandler("COLLIDE", self.onCollide, self)
	Events.unregisterHandler("OBJECT_HEALED", self.onHealed, self)
	Events.unregisterHandler("OBJECT_DAMAGED", self.onDamage, self)
	Events.unregisterHandler("PLAYER_DIED", self.onPlayerDied, self)
	Events.unregisterHandler("OBJECT_DIED", self.onObjectDied, self)
	Events.unregisterHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)

	if self.idleSound then deleteSound({soundID = self.idleSound}) end
	if self.damageSound then deleteSound({soundID = self.damageSound}) end
end

-- Sets the state for the Trap
function setState(self, state)
	if state == self.state then return end
	
	if state == STATE.IDLE then
		self:enterIdleState(self)
	elseif state == STATE.ATTACKING then
		self:enterAttackingState(self)
	elseif state == STATE.DYING then
		self:enterDyingState(self)
	end
end

function enterIdleState(self)
	self.state = STATE.IDLE
	self:show()
end

function enterAttackingState(self)
	self.state = STATE.ATTACKING
	self:show()
end

function enterDyingState(self)
	self.attackTable = {}
	self.state = STATE.DYING

	self:despawn()
end

function handleState(self, deltaTick)
	if self.state == STATE.ATTACKING then
		self:handleAttackingState(deltaTick)
	end
end

function handleAttackingState(self, deltaTick)
	if Toolbox.isContainerEmpty(self.attackTable) then
		self:setState(STATE.IDLE)
		return
	end

	local attacked = false

	for k, v in pairs(self.attackTable) do
		v.timer = v.timer - deltaTick
		-- If player is ready for an attack and are colliding with the trap then attack
		if v.timer <= 0 then
			if v.colliding then
				v.timer = ATTACK_TIME
				if v.player.mode ~= "God" then
					self:attack({attacked = v.player})
					attacked = true
				end
			-- Remove player for attack table if timer is up and they're not colliding
			else
				self.attackTable[k] = nil
			end
		end
	end

	if attacked and self.damageSound then
		playSound({soundID = self.damageSound})
	end
end

function despawn(self)
	if self.spawnerPID then
		Events.sendEvent("OBJECT_DESPAWN", {spawnerPID = self.spawnerPID, despawnPID = self.PID, spawnType = "trap"})	
	end
end

function incrementAttackTable(self, player)
	-- If a player has just triggered the trap, attack and add them to the attackTable
	if self.attackTable[player.ID] == nil then
		self.attackTable[player.ID] = {}
		self.attackTable[player.ID].player = player
		self.attackTable[player.ID].timer = ATTACK_TIME
		self.attackTable[player.ID].colliding = true
		self:damagePlayer(player)
	-- If player is currently in attack table and their timer is ready for an attack then attack them and reset their timer
	elseif self.attackTable[player.ID].timer <= 0 then
		self.attackTable[player.ID].timer = ATTACK_TIME
		self.attackTable[player.ID].colliding = true
		self:damagePlayer(player)
	end
end

function decrementAttackTable(self, player)
	if self.attackTable[player.ID] ~= nil then
		self.attackTable[player.ID].colliding = false
	end
end

function damagePlayer(self, player)
	if player.mode ~= "God" then
		self:attack({attacked = player})
		if self.damageSound then playSound({soundID = self.damageSound}) end
	end
end
----------------------------------------
-- Event handlers
----------------------------------------

-- Timer handler
function onTimer(self, event)
	self.onServerTick(self, event)
end

-- Wrapper for onTimer
function onServerTick(self, event)
	if self.tick == 0 then self.tick = event.tick end
	local deltaTick = event.tick - self.tick
	self:handleState(deltaTick)
	self.tick = event.tick
end

function onCollide(self, event)
	if (self.state ~= STATE.DYING) and
	   (event.player) and
	   (event.player.alive) then

	   	if event.type == true then -- collide type is entering[true]
			self:incrementAttackTable(event.player)
			self:setState(STATE.ATTACKING)
		elseif event.type == false then
			self:decrementAttackTable(event.player)
		end 
	end
end

-- Called when the trap is healed
function onHealed(self, event)
	-- TODO: Un-needed I suppose. I don't reward getting healed.
end

-- Called when the trap is damaged
function onDamage(self, event)
	-- TODO: Un-needed I suppose. I don't punish getting damaged.
end

function onPlayerDied( self, event )
	if self.state ~= STATE.DYING and event.player ~= nil and event.player.ID and self.attackTable[event.player.ID] then
		self.attackTable[event.player.ID] = nil
		if Toolbox.isContainerEmpty(self.attackTable) then
			self:setState(STATE.IDLE)
		else
			self:setState(STATE.ATTACKING)
		end
	end
end

-- Called when an object dies
function onObjectDied(self, event)
	-- Did I just die?
	if (event.object.PID == self.PID) then
		self:setState(STATE.DYING)
	end
end

-- Called when a player changes modes
function onPlayerModeChanged(self, event)
	-- Ensure this player wasn't our target
	if self.state ~= STATE.DYING and event.mode == "God" and event.player ~= nil and event.player.ID and self.attackTable[event.player.ID] then
		self:decrementAttackTable(event.player)
		if Toolbox.isContainerEmpty(self.attackTable) then
			self:setState(STATE.IDLE)
		else
			self:setState(STATE.ATTACKING)
		end
	end
end