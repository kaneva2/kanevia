--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

Class.createClass("Class_LootSpawner", "Class_Spawner")

local DEFAULT_ANIMATION = 4465634

function create(self, input)
	input.spawnType = "loot"
	input.spawnGLID = input.spawnGLID or 4468596
	input.maxSpawns = 1
	
	_super.create(self, input)
	
	self:setAnimation(DEFAULT_ANIMATION)
end	