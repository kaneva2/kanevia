--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_Teleporter.lua
-- Parent class for creating Teleporters

include("Lib_GameDefinitions.lua")
include("Lib_SaveLoad.lua")

Class.createClass("Class_Teleporter", "Class_Object")


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local GameDefinitions	= _G.GameDefinitions
local Events			= _G.Events
local SaveLoad 			= _G.SaveLoad

-----------------------------------------------------------------
-- Class-specific constants
-----------------------------------------------------------------
local DEFAULT_TRIGGER_RADIUS = 2
local DEFAULT_CHARGE_TIME = 0.7
local POSITION_Y_OFFSET = 0.5
local ACTIVATE_TIMER = 2
local s_uPlayerSpawners
local s_tGameConfigs
s_type = "teleporter"



function create(self, input)

	s_tGameConfigs	  = SharedData.new("config", false)
	s_uPlayerSpawners = SharedData.new("spawners", false)
	self.position = nil
	_super.create(self, input)
	self.active = false
	
	if input == nil then 
		input = {}
	end

	-- Default properties
	self.triggerRadius = input.triggerRadius or DEFAULT_TRIGGER_RADIUS
	self.chargeTime = input.chargeTime or DEFAULT_CHARGE_TIME
	self.chargeSoundID = input.chargeSoundID
	self.arriveSoundID = input.arriveSoundID
	self.chargeParticle = input.chargeParticle
	self.idleParticle = input.idleParticle
	self.instantTeleport = input.instantTeleport
	if self.instantTeleport then
		self.chargeTime = 0
	end
	--multiWorldTeleporter
	self.worldDestination = input.worldDestination
	--linked zone Teleporter
	local zoneDestination = input.zoneDestination
	if zoneDestination then
		zoneDestination = string.gsub(zoneDestination, "\'", "\"")
		self.zoneDestination = Events.decode(zoneDestination)
	end

	self.specialDestination = input.specialDestination

	self.linkedTeleporters = {}
	self.playerTimers = {}
	
	-- Register for events
	self.trigger = Events.setTrigger(self.triggerRadius, self.onTrigger, self)
	-- Register players that enter the zone
	Events.registerHandler("RETURN_MAP_BY_TYPE",  self.onMapReturn, self)
	
	-- Particles and sounds
	if self.idleParticle then
		self:setParticle(self.idleParticle)
	end

	if self.chargeSoundID then
		local tSoundParams = GameDefinitions.generateGenericSoundParams(self.PID, self.chargeSoundID, 20)
		self.chargeSound = generateSound(tSoundParams)
	end
	if self.arriveSoundID then
		local tSoundParams = GameDefinitions.generateGenericSoundParams(self.PID, self.arriveSoundID, 20)
		self.arriveSound = generateSound(tSoundParams)
	end
	
	self.activateTimer = Events.setTimer(ACTIVATE_TIMER, function() self.active = true end)

	if not self.worldDestination and not self.zoneDestination and not self.specialDestination then
		Events.registerHandler("FRAMEWORK_RETURN_OBJECT_ASSOCIATIONS", self.onReturnObjectAssociations, self)
		Events.sendEvent("FRAMEWORK_REQUEST_OBJECT_ASSOCIATIONS", {PID = self.PID})
	elseif self.zoneDestination then
		Events.registerHandler("FRAMEWORK_GOTO_LINKED_ZONED", self.linkedZoneTeleport, self)
	end
end

-- Clean up sounds on destroy
function destroy(self)
	_super.destroy(self)
	
	Events.unregisterHandler("RETURN_MAP_BY_TYPE",  self.onMapReturn, self)
	Events.unregisterHandler("FRAMEWORK_RETURN_OBJECT_ASSOCIATIONS", self.onReturnObjectAssociations, self)
	Events.unregisterHandler("FRAMEWORK_GOTO_LINKED_ZONED", self.linkedZoneTeleport, self)
	
	if self.trigger then
		Events.removeTrigger(self.trigger)
	end
	
	if self.activateTimer then
		Events.cancelTimer(self.activateTimer)
	end
	
	if self.cycleTimer then
		Events.cancelTimer(self.cycleTimer)
	end
	
	for key, _ in pairs(self.playerTimers) do
		Events.cancelTimer(self.playerTimers[key])
	end
	
	if self.chargeSound then deleteSound({soundID = self.chargeSound}) end
	if self.arriveSound then deleteSound({soundID = self.arriveSound}) end
end

function onTrigger(self, event)
	local player = event.player

	if player == nil then
		return
	end	

	if player.loading then
		return
	end
	
	local idleParticle = self.idleParticle
	local chargeParticle = self.chargeParticle
	if event.triggerType == 1 and self.active then
		-- Player enters teleporter themselves
		if player.canTeleport == nil or player.canTeleport == true then
			player.canTeleport = false
			player.teleporting = true
			self.playerTimers[player.ID] = 0
			if self.specialDestination then
				if self.specialDestination == "K-Town" then
					Events.sendClientEvent("FRAMEWORK_GOTO_ZONE", {zoneID = s_tGameConfigs.DynamicZoneLinkParentId, zoneType = 6}, event.player.ID)
					return
				elseif self.specialDestination == "Home" then
					Events.sendClientEvent("FRAMEWORK_GOTO_HOME", {}, event.player.ID)
					return
				end
			elseif self.worldDestination then
				if self.worldDestination ~= "" then
					if self.chargeSound then playSound({soundID = self.chargeSound}) end
					if chargeParticle then
						setParticle(self, chargeParticle)
					end
					self.playerTimers[player.ID] = Events.setTimer(self.chargeTime, 
																	function()	   
																	kgp.playerLoadMenu(player.ID, "Framework_WorldTeleporter.xml")
																	Events.sendClientEvent("UPDATE_WORLD_TELEPORTER", {placeInfo = self.worldDestination}, player.ID)
																	setParticle(self, idleParticle or 0)
																	self.playerTimers[player.ID] = nil 
																	end)
				end
			elseif self.zoneDestination then
				local zoneID = tonumber(self.zoneDestination.zoneID)
				local zID, zIndex, zType = kgp.gameGetCurrentInstance() 
				if chargeParticle then
					setParticle(self, chargeParticle)
				end
				if self.chargeSound then playSound({soundID = self.chargeSound}) end

				if self.instantTeleport then
					event.PID = self.PID
					self:linkedZoneTeleport(event)
					setParticle(self, idleParticle or 0)
				elseif zoneID == zID then
					event.PID = self.PID
					self.playerTimers[player.ID] = Events.setTimer(self.chargeTime, 
																	function()	   
																	self:linkedZoneTeleport(event)
																	self.playerTimers[player.ID] = nil 
																	setParticle(self, idleParticle or 0)
																	player.canTeleport = true
																	player.teleporting = false
																	end)
				else
					self.playerTimers[player.ID] = Events.setTimer(self.chargeTime, 
																	function()	   
																	kgp.playerLoadMenu(player.ID, "Framework_WorldTeleporter.xml")
																	setParticle(self, idleParticle or 0)
																	Events.sendClientEvent("UPDATE_WORLD_TELEPORTER", {zoneInfo = self.zoneDestination, PID = self.PID}, player.ID)
																	self.playerTimers[player.ID] = nil 
																	end)
				end
			else
				self:requestMapByType({type = "teleporter", user = player, PID = self.PID})
			end
		-- Player enters teleporter by another teleporter
		elseif player.canTeleport == false then
			player.teleporting = false
			if self.arriveSound then playSound({soundID = self.arriveSound}) end
		end
	-- Let players teleporter again only after they leave the trigger
	elseif event.triggerType == 2 and player.teleporting == false then
		if idleParticle then
			setParticle(self, idleParticle)
		else
			setParticle(self, 0)
		end
		player.canTeleport = true
	elseif event.triggerType == 2 and self.playerTimers[player.ID] then
		player.canTeleport = true
		player.teleporting = false
		if self.playerTimers[player.ID] ~= 0 then
			Events.cancelTimer(self.playerTimers[player.ID])
		end
		self.playerTimers[player.ID] = nil
		if idleParticle then
			setParticle(self, idleParticle)
		else
			setParticle(self, 0)
		end
	else
		if idleParticle then
			setParticle(self, idleParticle)
		else
			setParticle(self, 0)
		end
	end
end

function onReturnObjectAssociations(self, event)
	if event.PID ~= self.PID then return end
	self.linkedTeleporters = event.objectAssociations	
end

function onMapReturn(self, event)
	if event.PID == nil or event.PID ~= self.PID then return end
	local mapType = ""
	for i, v in pairs(event.map) do
		mapType = v.type
		break
	end

	if mapType == "teleporter" then
		local linkedTeleporters = {}

		for key, value in pairs(event.map) do
			if tonumber(key) ~= self.PID then
				for _, linkedPID in pairs(self.linkedTeleporters) do
					if tonumber(key) == linkedPID then
						table.insert(linkedTeleporters, value)
					end
				end
			end
		end
		
		-- If this teleporter has any links
		if #linkedTeleporters > 0 then			
			local randomIndex = math.random(1, #linkedTeleporters)
			local destTeleporter = linkedTeleporters[randomIndex]
		
			if self.chargeSound then playSound({soundID = self.chargeSound}) end
			
			if self.chargeParticle then
				setParticle(self, self.chargeParticle)
				setParticle(destTeleporter, self.chargeParticle)
			end
			
			if self.idleParticle then
				if self.cycleTimer then
					Events.cancelTimer(self.cycleTimer)
				end
				self.cycleTimer = Events.setTimer(self.chargeTime*2, function() setParticle(self, self.idleParticle) setParticle(destTeleporter, self.idleParticle) end)
			end

			if event.user.teleporting then
				self.playerTimers[event.user.ID] = Events.setTimer(self.chargeTime, function() playerTeleport({user = event.user.ID, x = destTeleporter.position.x, y = destTeleporter.position.y + POSITION_Y_OFFSET, z = destTeleporter.position.z,
																											   rx = destTeleporter.position.rx, ry = destTeleporter.position.ry, rz = destTeleporter.position.rz})
																							   self.playerTimers[event.user.ID] = nil end)
			end
		else
			kgp.playerShowStatusInfo(event.user.ID, "This teleporter has no destination!", 3, 255, 0, 0)
			event.user.teleporting = false
		end
	end
end

function requestMapByType(self, input)
	Events.sendEvent("REQUEST_MAP_BY_TYPE", input)
end

function linkedZoneTeleport(self, event)
	log("\nhello?\n")
	if event.player and event.player.ID and event.PID and event.PID == self.PID then
		event.destination = self.zoneDestination
		Events.sendEvent("FRAMEWORK_LINKED_ZONE_TELEPORT", event)

		setParticle(self, idleParticle or 0)
	end
end