--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_Harvestable.lua


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Events			= _G.Events
local Class				= _G.Class

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------

Class.createClass("Class_Harvestable", "Class_Base")

Class.createProperties({
	"spawnerPID",
	"spawnGLID",
	"requiredTool"
})

-----------------------------------------------------------------
-- Class-specific constants
-----------------------------------------------------------------

local DEFAULT_NAME = "Wood Node"
local DEFAULT_ANIMATION = 4465636
local DEFAULT_SPAWN_GLID = 4470121

s_type = "Harvestable"


function create(self, input)
	if input == nil then input = {} end
	input.targetable = "true"
	_super.create(self, input)
	
	-- User-definable variables (Public, document for users)
	self.name = input.name  or DEFAULT_NAME
	
	self.spawnerPID = input.spawnerPID
	self.spawnGLID = input.spawnGLID or DEFAULT_SPAWN_GLID

	self.requiredTool = input.requiredTool
	
	Events.registerHandler("OBJECT_HEALED", self.onHealed, self)
	Events.registerHandler("OBJECT_DAMAGED", self.onDamage, self)
	Events.registerHandler("OBJECT_DIED", self.onObjectDied, self)
	--self:setMouseOver(5)
	
	-- Register for health bars
	self:enableHealthBar({})
	
	self:setAnimation(DEFAULT_ANIMATION)
end

function destroy(self)
	_super.destroy(self)

	Events.unregisterHandler("OBJECT_HEALED", self.onHealed, self)
	Events.unregisterHandler("OBJECT_DAMAGED", self.onDamage, self)
	Events.unregisterHandler("OBJECT_DIED", self.onObjectDied, self)
end

function despawn(self)
	if self.spawnerPID then
		Events.sendEvent("OBJECT_DESPAWN", {spawnerPID = self.spawnerPID, despawnPID = self.PID, spawnType = "harvestable"})	
	end
end

----------------------------------------
-- Event handlers
----------------------------------------
function onHealed(self, event)
	-- TODO: Un-needed I suppose. I don't reward getting healed.
end

-- Called when the monster is damaged
function onDamage(self, event)
	-- TODO: Anything to do here when damaged?
end

-- Called when an object dies
function onObjectDied(self, event)
	-- Did I just die?
	if (event.object.PID == self.PID) then
		self:despawn()
	end
end