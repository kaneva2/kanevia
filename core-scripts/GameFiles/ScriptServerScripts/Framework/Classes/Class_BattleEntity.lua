--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_Monster.lua
--
-- Monster class inherited from NPC
--
-- User-definable variables
-- STRING
-- NUMBER
-- TABLE
--
-- User-accessible methods
--
---------------------------------------
Class.createClass("Class_BattleEntity", "Class_BaseMobile")

local DEFAULT_NAME = "Battle Entity"

s_type = "Monster"

-- Instantiate an instance of NPC
function create(self, input)
	-- TODO: Error check input values
	if input == nil then input = {} end
	_super.create(self, input)

	self.targetable = input.targetable or "true"
	self.agroTable = {}
	self.victim = nil
end

function destroy(self)
	_super.destroy(self)
end

function evaluateAgroTable(self)
	local target = nil
	
	for k, v in pairs(self.agroTable) do
		if target == nil or v.threat > target.threat then
			target = v
		end
	end
	
	if target and ( self.victim == nil or ( self.victim.type == "Player" and self.victim.name ~= target.victim.name ) or ( tonumber(self.victim.PID) ~= tonumber(target.victim.PID) ) ) then
		self:newVictim(target.victim)
	end
end

function newVictim(self, victim)
	self.victim = victim
end

function addThreat(self, victim, threat)
	local victimKey
	if victim.type == "Player" then
		victimKey = victim.name
	else
		victimKey = tostring(victim.PID)
	end

	if self.agroTable[victimKey] == nil then
		self.agroTable[victimKey] = {}
		self.agroTable[victimKey].victim = victim
		self.agroTable[victimKey].threat = 0
	end
	
	self.agroTable[victimKey].threat = self.agroTable[victimKey].threat + threat
end

function setThreatFloor(self, victim, threat)
	local victimKey
	if victim.type == "Player" then
		victimKey = victim.name
	else
		victimKey = tostring(victim.PID)
	end

	if self.agroTable[victimKey] == nil then
		self.agroTable[victimKey] = {}
		self.agroTable[victimKey].victim = victim
		self.agroTable[victimKey].threat = 0
	end

	if self.agroTable[victimKey].threat < threat then
		self.agroTable[victimKey].threat = threat
	end
end

function erodeAgro(self, deltaTick)
	for k, v in pairs(self.agroTable) do
		v.threat = v.threat - deltaTick/1000
		if v.threat <= 0 then
			self.agroTable[k] = nil
		end
	end
end

function clearAgroTable(self)
	self.agroTable = {}
	self.victim = nil
end

function attack(self, input)
	-- Error check inputs
	if input.attacked == nil then Console.Write(Console.DEBUG, "attackPlayer() - [attacked] is nil."); return end
	input.attacker = self._dictionary
	Battle.objectAttack(input)
end
