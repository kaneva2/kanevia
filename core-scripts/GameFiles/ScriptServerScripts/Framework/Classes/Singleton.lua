--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Singleton.lua

Class.createClass("Singleton")

local s_instance

function create(self)
	s_instance = self
end

function getInstance()
	return s_instance
end

function _G.kgp_start( user )
	local thisClass = _G._thisClass
	
	if not Class.isClassOrInstance(thisClass) then
		error("Singleton: unable to identify current class")
	end

	thisClass.new()

	if s_instance and type(s_instance.start) == "function" then
		s_instance:start(user)
	end
end

-- for future use
function _G.kgp_stop( user )
	if s_instance and type(s_instance.stop) == "function" then
		s_instance:stop(user)
	end
end
