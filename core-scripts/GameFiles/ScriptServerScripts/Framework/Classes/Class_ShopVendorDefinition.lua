--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- Class_ShopVendor.lua
-- Super class for NPC vendors selling items from Kaneva's shop 
----------------------------------------

include("Lib_NpcDefinitions.lua")
local NpcDefs = _G.NpcDefinitions

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local Events			= _G.Events
local Players			= _G.Players

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
Class.createProperties({
	"header"
})

----------------------------------------
-- Constants
----------------------------------------
local INTERACTION_RANGE	= 25
local DEFAULT_NAME = "Vendor Items"

local INVENTORY_KEY = "vendorInventory"
local ITEMS_PER_PAGE = 13
local PLACE_HOLDERS = 4

local DEFAULT_HEADER = "Choose an item to "

----------------------------------------
-- Global Variables
----------------------------------------
s_type = "Vendor"

----------------------------------------
-- Private Methods
----------------------------------------
-- Instantiate an instance of Vendor
function create(self, input)
	local tempItems = {}
	if input == nil then input = {} end
	input.interactionRange = INTERACTION_RANGE
	
	_super.create(self, input)

	self.players = {}
	self.tradeItems = {} --containing multiple trades
	self.tradeItem = {}	-- containing a single trade

	self.spawnerPID = input.spawnerPID
	self.PID = input.PID
	self.clothing = input.clothing -- is a clothing vendor???? --- Edie 
	self.lastVID = input.lastVID
	self.GLID = input.GLID

	self.name = input.name or DEFAULT_NAME
	self.actorName = input.actorName

	if input.header == "" then
		input.header = nil
	end
	
	-- sets the header??? 
	self.header = input.header or DEFAULT_HEADER.."buy"
	-- saves the trade items, which holds the GLIDs for the items we're trying to sell 
	self.trades = Toolbox.deepCopy(input.trades, true)

	Events.registerHandler("ACTIVATE_ACTOR", self.activateActorHandler, self)
	Events.registerHandler("FRAMEWORK_CLOSED_VENDOR", self.onVendorClosed, self)

	--self.trigger = Events.setTrigger(self.interactionRange, self.onTrigger, self)
end

function destroy(self, input)
	Events.unregisterHandler("ACTIVATE_ACTOR", self.activateActorHandler, self)
	Events.unregisterHandler("FRAMEWORK_CLOSED_VENDOR", self.onVendorClosed, self)
	_super.destroy(self)

	if self.trigger then
		Events.removeTrigger(self.trigger)
	end
end
----------------------------------------
-- Event handlers
----------------------------------------

function activateActorHandler(self, event)
	local uPlayer = event.player
	local iPID = event.PID
	if uPlayer == nil then return end
	if iPID == nil then return end

	if iPID == self.PID then
		self:onActivate(event)
	end
end

-- Right click handler
function onRightClick(self, event)
	log("Class_ShopVendor - onRightClick")
	local uPlayer = event.player
	if uPlayer == nil then return end
	-- Activate actor
	self:preActivate(event)
end

-- Left click handler
function onLeftClick(self, event)
	log("Class_ShopVendor - onLeftClick")
	local uPlayer = event.player
	if uPlayer == nil then return end
	-- Activate actor
	self:preActivate(event)
end

-- Validate all conditions necessary before onActivate
function preActivate(self, event)
	log("Class_ShopVendor - preActivate")
	local uPlayer = event.player
	if not Players.validatePlayer(uPlayer) then
		log("Class_ShopVendor wasn't able to activate. Something has gone terribly wrong.")
		return false
	end	
		
	-- Players can't activate vendors
	if uPlayer and uPlayer.mode ~= "God" then
		if uPlayer.alive == nil or uPlayer.alive == false then
			kgp.playerShowStatusInfo(uPlayer.ID, "You cannot do that while dead.", 3, 255, 0, 0)
		elseif  getDistanceToPoint(self, uPlayer.position) > self.interactionRange then
			kgp.playerShowStatusInfo(uPlayer.ID, "You must be closer to interact with that object.", 3, 255, 0, 0)
		else
			if self.repEnabled then
				if (self.followerOf == uPlayer.name or self.followerOf == nil) then
					Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)
					uPlayer.activeMenuPID = self.PID
					kgp.playerLoadMenu(uPlayer.ID, "Framework_NPCActionMenu.xml")
					local eventProperties = {
						actorPID = self.PID,
						UNID = self.UNID,
						value = self:getRepByPlayer(uPlayer.name),
						isFollower = self.followerOf == uPlayer.name,
						isSpouse = self.spouseOf == uPlayer.name,
						isRider = self.isRider,
						equippedGLIDs = self.equippedGLIDs
					}
					if self.behavior then
						if string.sub(self.behavior, -6, -1) == "_rider" then
							eventProperties.isRider = true
						end
					end
					Events.sendClientEvent("FRAMEWORK_DEFINE_NPC_ACTIONS", eventProperties, uPlayer.ID)
					Events.sendClientEvent("FULFILL_ACTOR_TALK_TO", {UNID = self.UNID}, uPlayer.ID)
				-- Cannot activate another player's spouse that unlocks at the spouse level (but CAN activate another player's follower that unlocks at the folloer level)
				elseif self.repUnlock == NpcDefs.SPECIAL_LEVELS[2] and self.spouseOf ~= uPlayer.name then
					kgp.playerShowStatusInfo(uPlayer.ID, "Only their spouse can do that.", 3, 255, 0, 0)		
				else
					self:onActivate(event)
				end
			else
				self:onActivate(event)
			end
		end
	end
	return false
end

-- Called on activation
function onActivate(self, event)
	log("Class_ShopVendor - onActivate")
	local uPlayer = event.player
	if uPlayer == nil then return end

	if not ( next(self.trades) ) then
		kgp.playerShowStatusInfo(uPlayer.ID, "This Vendor has nothing for sale at the moment!", 3, 255, 0, 0)
		return
	end

    -- Close any open vendor menus
    Events.sendClientEvent("FRAMEWORK_CLOSE_ACTOR_MENU", {}, uPlayer.ID)
	if self.clothing then
		uPlayer.activeMenuPID = self.PID
		kgp.playerLoadMenu(uPlayer.ID, "Framework_ClothingVendor.xml")
	else
		-- EDIE: update this to be a generic shop menu 
		kgp.playerLoadMenu(uPlayer.ID, "Framework_Vendor.xml")
	end
	
	log("--- UPDATE_SHOP_VENDOR_ITEMS")
	-- sends the trades to the menu with the glid information 
	Events.sendClientEvent("UPDATE_SHOP_VENDOR_ITEMS", {trades = self.trades}, uPlayer.ID)
	
	-- sends the header information to vendors 
	Events.sendClientEvent("UPDATE_VENDOR_HEADER", {header = self.header, name = self.actorName, PID = self.PID, GLID = self.GLID}, uPlayer.ID)
	if self.isRider then
		Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = self.PID, interacting = true})
	end

	Events.sendClientEvent("FULFILL_ACTOR_TALK_TO", {UNID = self.UNID}, uPlayer.ID)
end

function onVendorClosed(self, event )
	if event.PID == self.PID then
		if self.isRider then
			Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = self.PID, interacting = false})
		end
	end
end