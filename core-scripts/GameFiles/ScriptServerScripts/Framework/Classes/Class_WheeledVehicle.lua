--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_WheeledVehicle.lua
-- Class for all wheeled vehicles.
include("Lib_SaveLoad.lua")
include("Lib_GameDefinitions.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local SaveLoad			= _G.SaveLoad
local GameDefinitions	= _G.GameDefinitions
local Events			= _G.Events
local Class				= _G.Class
local Players			= _G.Players

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------

Class.import("Inventory")

Class.createClass("Class_WheeledVehicle", "Class_Base")

Class.createProperties({
	"interactionRange"
})

-----------------------------------------------------------------
-- Class-specific constants
-----------------------------------------------------------------
local DEFAULT_NAME = "Car"
local DEFAULT_LABEL_USE = "Right-click to drive"
local PIN_SAVE_KEY = "DNC_pinCode"
local PIN_ACCESS_MEM_KEY = "DNC_pinCodeMemory"

local SUPER_JUMP_DEFAULT = {x = 13.86, y = 13.86}
local SUPER_JUMP_DISABLED = {x = 0, y = 0}

local DEFAULT_LEVEL = 1
local DEFAULT_OFFSET = {	
	x = 0.0,
	y = 0.0,
	z = 0.0
}

local DEFAULT_ROTATION = {	
	x = 0.0,
	y = 0.0,
	z = 0.0,
	w = 1.0
}

local DEFAULT_DIMENSIONS = {
	length = 5.0,
	width  = 2.0,
	height = 1.5
}

local DEFAULT_TOP_SPEED = 128.0
local DEFAULT_HORSEPOWER = 120.0
local DEFAULT_LOADED_SUSP = 0.5
local DEFAULT_UNLOADED_SUSP = 0.75
local DEFAULT_MASS = 1500.0

local DEFAULT_WHEEL_FRICTION = 2.5

local INTERACTION_RANGE = 30

local METERS_TO_FEET = 100.0 / (12.0 * 2.54)

local MILISECONDS_PER_SECOND = 1000
local SECONDS_PER_MINUTE = 60

local HEALTH_PERCENT_FOR_MAX_SPEED = 75
local HEALTH_PERCENT_FOR_MIN_SPEED = 5
local HEALTH_SPEED_SLOPE = 100 / (HEALTH_PERCENT_FOR_MAX_SPEED - HEALTH_PERCENT_FOR_MIN_SPEED)
local HEALTH_SPEED_INTERSEPT = -HEALTH_SPEED_SLOPE * HEALTH_PERCENT_FOR_MIN_SPEED


s_type = "WheeledVehicle"

-----------------------------------------------------------------
-- Class-specific functions
-----------------------------------------------------------------
local useVehicle --(self, playerID)
local enterVehicle --(self, playerID)
local leaveVehicle --(self, playerID)



function create(self, input)
	if input == nil then input = {} end

	self.tooltip = DEFAULT_LABEL_OPEN
	
	self.targetable = input.targetable or "true"
	self.level = input.level or DEFAULT_LEVEL
	self.interactionRange = input.interactionRange or INTERACTION_RANGE
	_super.create(self, input)
	
	self.name = input.name or DEFAULT_NAME
	
	if input.showDriver and input.driverAnim then
		self.driverAnim = input.driverAnim
	end

	self.ownerLocked = (input.ownerLocked == nil or input.ownerLocked == true) or false
	if self.ownerLocked then
		self.ownerName = self.owner
	end

	-- Handle Offset
	self.offset = {	x = input.offX or DEFAULT_OFFSET.x,
					y = input.offY or DEFAULT_OFFSET.y,
					z = input.offZ or DEFAULT_OFFSET.z
				  }
				  
	-- Handle Euler rotation -> Quaternian conversion here
	self.eulerRotX = 0
	self.eulerRotY = input.eulerRotY or 0
	self.eulerRotZ = 0

	local tempW = math.cos(self.eulerRotY/2)
	local tempX = 0
	local tempY = math.sin(self.eulerRotY/2)
	local tempZ = 0
	
	-- Revert to old quaternion values if they exist
	if input.offRotW then tempW = input.offRotW end
	if input.offRotX then tempX = input.offRotX end
	if input.offRotY then tempY = input.offRotY end
	if input.offRotZ then tempZ = input.offRotZ end
	
	-- Handle offset rotation
	self.rotation = {	x = tempX or DEFAULT_ROTATION.x,
						y = tempY or DEFAULT_ROTATION.y,
						z = tempZ or DEFAULT_ROTATION.z,
						w = tempW or DEFAULT_ROTATION.w
				    }
				    
	-- Handle dimensions
	self.dimensions = {	length = input.dimLength or DEFAULT_DIMENSIONS.length,
						width = input.dimWidth or DEFAULT_DIMENSIONS.width,
						height = input.dimHeight or DEFAULT_DIMENSIONS.height
					  }

	self.topSpeed = input.topSpeed or DEFAULT_TOP_SPEED
	self.horsepower = input.horsepower or DEFAULT_HORSEPOWER
	self.loadedSusp = input.loadedSusp or DEFAULT_LOADED_SUSP	
	self.unloadedSusp = input.unloadedSusp or DEFAULT_UNLOADED_SUSP
	self.mass = input.mass or DEFAULT_MASS

	self.wheelFriction = input.wheelFriction or DEFAULT_WHEEL_FRICTION
	
	self.enableSound = input.enableSound or false
	
	self.idleSound = (self.enableSound and input.idleSound) or 0
	self.skidSound = (self.enableSound and input.skidSound) or 0

	self.inUse = false

	-- The driver of this car
	self.driver = nil

	-- super jump properties for the vehicle 
	if input.superjumpEnabled == true then
		self.jumpVelocityMPS = SUPER_JUMP_DEFAULT -- default for the super jump ED-5252
		-- log("--- jumpVelocityMPS 13.86")
	else 
		self.jumpVelocityMPS = SUPER_JUMP_DISABLED
		-- log("--- jumpVelocityMPS 0")
	end 

	-- fuel for the car
	self.tick = 0
	self.unlimitedFuel = input.behaviorParams.unlimitedFuel -- legacy cars should not need fuel 
	
	self.fuelName = "fuel"
	if input.behaviorParams.fuelType then
		self.fuelType = input.behaviorParams.fuelType
		-- Get name of the fuel from its properties
		local uFuelProperty = self.m_uItemProperties and self.m_uItemProperties[self.fuelType]
		if uFuelProperty then
			self.fuelName = uFuelProperty.name
		end
	else
		self.fuelType = 0
	end  
	self.maxFuelTime = (input.behaviorParams.fuelTime or 1) * SECONDS_PER_MINUTE * MILISECONDS_PER_SECOND
	self.fuelTime = self.maxFuelTime
	

	self.wearAndTear = input.behaviorParams.wearAndTear
	self.maxWearAndTearTime = (input.behaviorParams.wearAndTearTime or 1) * SECONDS_PER_MINUTE * MILISECONDS_PER_SECOND

	-- Register for events that are important
	Events.registerHandler("PLAYER-DEPART", self.onDepart, self)
	Events.registerHandler("PLAYER_DIED", self.onPlayerDied, self)
	Events.registerHandler("OBJECT_DIED", self.onObjectDied, self)
	Events.registerHandler("OBJECT_DAMAGED", self.onObjectHealthUpdated, self)
	Events.registerHandler("OBJECT_HEALED", self.onObjectHealthUpdated, self)
	Events.registerHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)
	Events.registerHandler("FRAMEWORK_PLAYER_LEAVE_VEHICLE", self.onPlayerRequestedToLeaveVehicle, self)
	Events.registerHandler("PIN_CODE_ACCEPTED", self.onPinAccepted, self)
	Events.registerHandler("FRAMEWORK_REQUEST_OCCUPIED_VEHICLES", self.onOccupiedRequest, self)

	-- handler for fueling the vehicle 
	Events.registerHandler("GENERIC_USE", self.onGenericUse, self)

	-- handler for 
	Events.registerHandler("TIMER", self.onTimer, self)

    self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
end 

function destroy(self)
	_super.destroy(self)

	if self.driver ~= nil then
		leaveVehicle(self, self.driver)
	end

	Events.unregisterHandler("PLAYER-DEPART", self.onDepart, self)
	Events.unregisterHandler("PLAYER_DIED", self.onPlayerDied, self)
	Events.unregisterHandler("OBJECT_DIED", self.onObjectDied, self)
	Events.unregisterHandler("FRAMEWORK_PLAYER_CHANGE_MODE", self.onPlayerModeChanged, self)
	Events.unregisterHandler("PIN_CODE_ACCEPTED", self.onPinAccepted, self)

	Events.unregisterHandler("GENERIC_USE", self.onGenericUse, self)
	Events.unregisterHandler("TIMER", self.onTimer, self)

    if self.proxTrigger then
        Events.removeClientTrigger(self.proxTrigger)
    end
end

-- Timer handler
function onTimer(self, event)
	self:onServerTick(event)
end

-- Wrapper for onTimer
function onServerTick(self, event)
	if self.tick == 0 then self.tick = event.tick end
	local deltaTick = event.tick - self.tick
	self:burnFuel(deltaTick)
	self:damageVehicle(deltaTick)
	self.tick = event.tick
end

-- On object left click
function onLeftClick(self, event)
	local handEquipped = (event.player.handEquipped == true or event.player.handEquipped == nil)
	local doesPlayerOwn = self.owner == event.player.name
	-- if the hand tool is selected and the player does not own 
	if handEquipped and not doesPlayerOwn then
		self:onActivate(event)
	end
end

function onRightClick(self, event)
	self:onActivate(event)
end

-- On activate
function onActivate(self, event)
	-- Only valid players can activate the car
	if event.player and (event.player.mode ~= "God") and event.player.alive then
		useVehicle(self, event.player, false)
	end
end

validActivation = function(self, inputPlayer)
	useVehicle(self, event.player, true)
end

-- Depart handler
function onDepart(self, event)
	if self.driver and event.player.ID == self.driver.ID then
		leaveVehicle(self, self.driver)
	end
end

function onPlayerDied(self, event)
	if self.driver and event.player.ID == self.driver.ID then
		leaveVehicle(self, self.driver)
	end
end

function onObjectDied(self, event)
	if self.driver and event.object.PID == self.PID then
		leaveVehicle(self, self.driver)
	end
end

function onObjectHealthUpdated(self, event)
	if self.driver and ((event.healed and event.healed.PID == self.PID) or (event.attacked and event.attacked.PID == self.PID)) then
		if event.regenAmount then
			Events.sendClientEvent("GENERATE_FLOATER", {text = "+"..tostring(math.ceil(event.regenAmount)), target = self.PID, color = GameDefinitions.COLORS.GREEN}, self.driver.ID)
		elseif event.damage then
			Events.sendClientEvent("GENERATE_FLOATER", {text = "-"..tostring(math.ceil(event.damage)), target = self.PID, color = GameDefinitions.COLORS.RED}, self.driver.ID)
		end

		Events.sendClientEvent("FRAMEWORK_SET_VEHICLE_HEALTH", {health = self.health, maxHealth = self.maxHealth}, self.driver.ID)
	end
end

function onPlayerModeChanged(self, event)
	if self.driver and event.player.ID == self.driver.ID then
		leaveVehicle(self, self.driver)
	end
end

function onPlayerRequestedToLeaveVehicle(self, event)
	if self.driver and event.player.ID == self.driver.ID then
		leaveVehicle(self, self.driver)
	end
end

function onOccupiedRequest(self, event )
	if event.player and self.inUse then
		Events.sendClientEvent("FRAMEWORK_RETURN_VEHICLE_OCCUPIED", {PID = self.PID, occupied = 1}, event.player.ID)
	end
end

function leaveVehicle(self, player)
	-- log("Getting out of the car!")
	-- Assumes rotation only around the y axis.
	-- Calculations are based on the vehicle dimensions which are assumed to match the
	-- dynamic object dimensions (which would be preferred, but we don't have access to them).
	local pX, pY, pZ, pRX, pRY, pRZ = kgp.playerGetLocation(player.ID)
	
	local rotation_local = 2 * math.atan2(self.rotation.y, self.rotation.w)
	local veh_look = { x = -pRX * math.cos(rotation_local) + pRZ * math.sin(rotation_local),
	                   y = pRY,
	                   z = -pRX * math.sin(rotation_local) -pRZ * math.cos(rotation_local) }
	local veh_world_angle = math.atan2(veh_look.x, veh_look.z)
	local veh_offset_local = { x = - METERS_TO_FEET * self.offset.x, y = - METERS_TO_FEET * self.offset.y, z = - METERS_TO_FEET * self.offset.z } 
	local veh_offset_world = { x = veh_offset_local.x * math.cos(veh_world_angle) + veh_offset_local.z * math.sin(veh_world_angle),
	                           y = veh_offset_local.y,
	                           z = -veh_offset_local.x * math.sin(veh_world_angle) + veh_offset_local.z * math.cos(veh_world_angle) }
	local half_length = 0.5 * METERS_TO_FEET * self.dimensions.length
	local length_offset = -half_length - 1.5 -- move vehicle 1.5 feet in front of avatar to avoid collision.
	veh_offset_world.x = veh_offset_world.x + pRX * length_offset;
	veh_offset_world.y = veh_offset_world.y + pRY * length_offset + 0.5 * METERS_TO_FEET * self.dimensions.height;
	veh_offset_world.z = veh_offset_world.z + pRZ * length_offset;
	objPos = { x = pX + veh_offset_world.x, y = pY + veh_offset_world.y, z = pZ + veh_offset_world.z,
	           rx = veh_look.x, ry = 0, rz = veh_look.z }
	if objPos.rx == 0 and objPos.rz == 0 then
		objPos.rx = 1 -- Vehicle was pointing vertically. Pick an arbitrary horizontal direction to face.
	end

	kgp.playerLeaveVehicle(self.driver.ID, self.PID)
	kgp.playerShow(player.ID)
	kgp.gameItemSetPosRot(self.PID, objPos.x, objPos.y, objPos.z, objPos.rx, objPos.ry, objPos.rz)
	self.position = objPos
	
	local gender = player.gender
	local anims = gender and GameDefinitions.DEFAULT_ANIMS[gender]

	if anims ~= nil then
		kgp.playerSetAnimation( player.ID, anims.STAND)
		kgp.playerDefineAnimation( player.ID, anims.STAND, anims.RUN, anims.WALK, anims.JUMP )
	else
		log("ERROR DETECTED - ED-7420 : leaveVehicle unable to set animations due to invalid gender!  gender = "..tostring(gender)..", player.name = "..tostring(player and player.name))
	end

	player.inVehicle = false
	Events.sendClientEvent("FRAMEWORK_PLAYER_OCCUPIED", {occupied = false}, player.ID)
	player.occupiedVehicle = nil
	Events.sendClientEvent("FRAMEWORK_RETURN_VEHICLE_OCCUPIED", {PID = self.PID, occupied = 0})

	-- Log vehicle use
	local metricsData = {driveStartTime = self.driveStartTime or 0,
						 driveEndTime = kgp.gameGetCurrentTime() or 0}  	
	self:recordPlayerInteraction(player.name, "drive", metricsData, "wheeled_vehicle")
	
	self.inUse = false
	self.driver = nil
	self.driveStartTime = nil
	
	if self.proxTrigger == nil then
		self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
	end

	self:updateLabels()
end

function enterVehicle(self, player)
	-- log("Getting in the car!")
	Events.sendClientEvent("FRAMEWORK_PLAYER_OCCUPIED", {occupied = true}, player.ID)
	player.occupiedVehicle = self._dictionary
	self.inUse = true
	self.driver = player
	self.driveStartTime = kgp.gameGetCurrentTime() or 0
	player.inVehicle = true
	kgp.playerSpawnVehicle(
		{
			clientId=self.driver.ID,
			placementId=self.PID,
			meshOffset=self.offset,
			meshOrientation=self.rotation,
			dimensions={x=self.dimensions.width, y=self.dimensions.height, z=self.dimensions.length},
			maxSpeedKPH=self.topSpeed,
			horsepower=self.horsepower,
			suspHeightLoaded=self.loadedSusp,
			suspHeightUnloaded=self.unloadedSusp,
			massKG=self.mass,
			wheelFriction=self.wheelFriction,
			jumpVelocityMPS=self.jumpVelocityMPS, -- delta velocity: x: forward y: up
			throttleSoundGlid=self.idleSound,
			skidSoundGlid=self.skidSound
		}
		)

	-- if the driver has an animation, play that animation
	if self.driverAnim then
		kgp.playerDefineAnimation( player.ID, self.driverAnim, self.driverAnim, self.driverAnim, self.driverAnim )
		kgp.playerSetAnimation( player.ID, self.driverAnim )
	else
		kgp.playerHide(player.ID)
	end
	
	-- remove the proxTrigger while driving 
    if self.proxTrigger then
        Events.removeClientTrigger(self.proxTrigger)
		self.proxTrigger = nil
    end

    -- send the events! 
    Events.sendClientEvent("FRAMEWORK_RETURN_VEHICLE_OCCUPIED", {PID = self.PID, occupied = 1})
    Events.sendClientEvent("FRAMEWORK_SET_VEHICLE_FUEL", {fuelTime = math.ceil((self.fuelTime / self.maxFuelTime) * 100), maxFuelTime = 100}, player.ID)
    Events.sendClientEvent("FRAMEWORK_SET_VEHICLE_HEALTH", {health = self.health, maxHealth = self.maxHealth}, player.ID)

    -- update the labels on the car! 
    self:updateLabels()
end

-- Wrapper for onGenericUse - fueling the vehicle 
function onGenericUse(self, event)

	-- is this the correct target?
	if event.target == self.PID  then
		-- use the fuel if the fueltype is correct 
		if event.weapon == self.fuelType then

			--log("--- onGenericUse accepted for VEHICLE FUEL")
			Events.sendClientEvent("FRAMEWORK_GENERIC_ACCEPTED", {weapon = event.weapon, accepted = true, errorType = "none", food = self.fuelType}, event.attacker.ID)
			kgp.playerShowStatusInfo(event.attacker.ID, "Fuel added to ".. (self.name or "this vehicle").. "!", 3, 0, 255, 255)

			if self.fuelTime and self.maxFuelTime then
				-- update the fuel properties of the car
				self.fuelTime = self.maxFuelTime

				--Events.sendClientEvent("FRAMEWORK_SET_VEHICLE_FUEL", {fuelTime = self.fuelTime, maxFuelTime = self.maxFuelTime}, self.driver.ID)
			end 
		end
	end
end

function burnFuel( self, deltaTick )
	-- can't burn fuel if it's not occupied, if it has unlimited fuel, or it doesn't have a fuel type selected 
	if self.inUse == false or self.unlimitedFuel == true or self.fuelType == 0 then
		return
	end

	if self.fuelTime and self.maxFuelTime then
		self.fuelTime = math.max(self.fuelTime - deltaTick, 0)
		
		-- send an event with the current health
		Events.sendClientEvent("FRAMEWORK_SET_VEHICLE_FUEL", {fuelTime = math.ceil((self.fuelTime / self.maxFuelTime) * 100), maxFuelTime = 100}, self.driver.ID)

		-- kick the user and send them an alert that they are out of fuel 
		if self.fuelTime == 0 then
			kgp.playerShowStatusInfo(self.driver.ID, (self.name or "This vehicle").. " requires ".. self.fuelName .." before you can drive it.", 3, 255, 0, 0)
			leaveVehicle(self, self.driver)
		end
	end 
end 

function damageVehicle(self, deltaTick)
	if self.inUse == false or not self.wearAndTear == true then
		return
	end

	if self.health and self.maxHealth and self.maxWearAndTearTime then
		local removeHealth = deltaTick / self.maxWearAndTearTime * self.maxHealth
		--self.health = math.max(self.health - removeHealth, 0)
		Battle.objectRemoveHealth({PID = self.PID, health = removeHealth})
		-- send an event to show the current wear and tear 
		Events.sendClientEvent("FRAMEWORK_SET_VEHICLE_HEALTH", {health = self.health, maxHealth = self.maxHealth}, self.driver.ID)

		-- change the speed of the vehicle 
		local healthPercentage = ((self.health - removeHealth) / self.maxHealth) * 100
		
		if healthPercentage <= 5 then
			log("--- 0")
			-- max speed is 0
			-- jump velocity is 0
			-- horsepower is 0
			kgp.playerModifyVehicle({clientId = self.driver.ID, maxSpeedKPH = 0, horsepower = 0, jumpVelocityMPS = {x = 0, y = 0}})
		elseif healthPercentage <= 75 then 
			-- the equation
			local healthCoefficient = ((HEALTH_SPEED_SLOPE * healthPercentage) + HEALTH_SPEED_INTERSEPT) / 100
			log("--- ".. tostring(healthCoefficient))
			-- max speed declines proportionally
			local newSpeed = self.topSpeed * healthCoefficient

			-- jump velocity declines proportionally 
			local newSuperJump = {x = self.jumpVelocityMPS.x * healthCoefficient, y = self.jumpVelocityMPS.y * healthCoefficient}

			-- horsepower declines proportionally
			local newHorsePower = self.horsepower * healthCoefficient

			kgp.playerModifyVehicle({clientId = self.driver.ID, maxSpeedKPH = newSpeed, horsepower = newHorsePower, jumpVelocityMPS = newSuperJump})
		end

		-- kick the user and send them an alert that the vehicle is damaged 
		if math.ceil((self.health / self.maxHealth) * 100) < 5  then
			kgp.playerShowStatusInfo(self.driver.ID, (self.name or "This vehicle").. " is broken and needs to be repaired before driving.", 3, 255, 0, 0)
			leaveVehicle(self, self.driver)
		end 
	end  
end

function getPinCode(self)
	local PID = self.PID
	local saveLoc = PIN_SAVE_KEY.."_"..tostring(PID)
	local pinDataString = SaveLoad.loadGameData(saveLoc)

	local pinData = {}
	if pinDataString then
		pinData = Events.decode(pinDataString)
	end
	local tempPinCode = pinData.pinCode or nil

	return tempPinCode
end

function onPinAccepted(self, event)
	if event.PID and event.PID == self.PID then
		self:validActivation(event.player)
		self:savePinAccessMemory(event.player.name)
	end
end

function getPinAccessMemory(self)
	local PID = self.PID
	local saveLoc = PIN_ACCESS_MEM_KEY.."_"..tostring(PID)
	local pinDataString = SaveLoad.loadGameData(saveLoc)

	local pinMembers = {}
	if pinDataString then
		pinMembers = Events.decode(pinDataString)
	end

	return pinMembers
end

function savePinAccessMemory(self, playerName)
	local PID = event.PID
	local pinCode = event.pinCode
	local pinMembers = self:getPinAccessMemory()
	if pinMembers == nil then -- if never saved before
		pinMembers = {}
	elseif #pinMembers == 0 then
		pinMembers = {}
	end
	if not checkForEntry(pinMembers, playerName) then
		table.insert(pinMembers, playerName)
		local pinDataString = Events.encode(pinMembers)
		local saveLoc = PIN_ACCESS_MEM_KEY.."_"..tostring(PID)
		SaveLoad.saveGameData(saveLoc, pinDataString)
	end
end

-- Returns whether a search item exists in a table
checkForEntry = function(inputTable, searchItem)
	for i,v in pairs(inputTable) do
		if v and v == searchItem then
			return true
		end
	end
	return false
end

function onTooltipItems( self, event )
	if event.returnPID ~= self.PID then
		return
	end 

	if #event.itemProperties >= 1 then 
		self.fuelName = event.itemProperties[1].properties.name
	end 
end

-- Time to use the car! (Whatever that means.)
useVehicle = function(self, player, inputPreValidated)
	if not Players.validatePlayer(player) then
		return
	end

	-- If the care is in use and this is being called, then one of two things has happened: A user has tried to use 
	-- someone else's car, or the user of the car is trying to get out.
	if self.inUse == true then
		-- See if the user is trying to use someone else's car.
		if self.driver and self.driver.ID ~= player.ID then
			kgp.playerShowStatusInfo(player.ID, (self.name or "This vehicle").. " is occupied.", 3, 255, 0, 0)
		-- Otherwise, get out of the car.
		else
			-- Get out of the car!
			leaveVehicle(self, player)
		end
	-- OK, trying to get into an unoccupied car, then. Just do it.
	else
		
		if (player.inVehicle) then
			kgp.playerShowStatusInfo(player.ID, "You must exit the current vehicle to enter another.", 3, 255, 0, 0)
			return
		end
		-- Make sure they're close enough to use it.
		if  (self.driver == nil or (self.driver and self.driver.ID ~= player.ID)) and getDistanceToPoint(self, player.position) > self.interactionRange then
			kgp.playerShowStatusInfo(player.ID, "You must be closer to interact with this ".. (self.name or "object").. ".", 3, 255, 0, 0)
			return
		end	
		-- make sure the car has fuel 
		if (self.unlimitedFuel == false and self.fuelType ~= 0 and self.fuelTime == 0) then
			kgp.playerShowStatusInfo(player.ID, (self.name or "This vehicle").. " requires ".. self.fuelName .." before you can drive it.", 3, 255, 0, 0)
			return
		end
		-- make sure the car is driveable
		if (math.ceil((self.health / self.maxHealth) * 100) < 5) then
			kgp.playerShowStatusInfo(player.ID, (self.name or "This vehicle").. " is broken and needs to be repaired before driving.", 3, 255, 0, 0)
			return
		end 

		local pinCode = self:getPinCode()
		local pinMembers = self:getPinAccessMemory()

		local preValidated = checkForEntry(pinMembers, player.name) or inputPreValidated
		if (player.name == nil or (self.ownerName and self.ownerName ~= player.name)) and preValidated ~= true then
			if pinCode ~= nil then
				kgp.playerLoadMenu(player.ID, "Framework_PINCode.xml")
				Events.sendClientEvent("OBJECT_ACTIVATE_PIN", {PID = self.PID, name = self.name, owner = self.owner}, player.ID)
			else
				kgp.playerShowStatusInfo(player.ID, (self.name or "This vehicle").. " belongs to ".. tostring(self.ownerName or "someone else").. ".", 3, 255, 0, 0)
			end
			return
		end
		
		-- Get in the car!
		enterVehicle(self, player)
	end
end

function updateLabels(self)
	if self == nil then
		return
	end 
	
	if canShowLabels(self) then
		self:clearLabels()
		
		local labels = {}

		if self.inUse and self.driver then
			table.insert(labels, {tostring(self.driver.name), 0.013})
		end
		
		if #labels > 0 then
			self:addLabels(labels)
		end
	end
end

function canShowLabels(self)
	local worldSettings = SaveLoad.loadGameData("WorldSettings")
	worldSettings = Events.decode(worldSettings)
	return worldSettings.playerNames and not (self.driverAnim)
end