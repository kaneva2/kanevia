--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

include("Lib_Vecmath.lua")


-----------------------------------------------------------------
-- Class Specification
-----------------------------------------------------------------
Class.createClass("Class_InteractiveNode", "Class_Base")


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Math				= _G.Math
local Players			= _G.Players

-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local DEFAULT_INTERACTION_RANGE = 25
local DEFAULT_TRIGGER_RADIUS = 1
local DEFAULT_PLAYER_POSITIONS = {
	maleX		= 0, 
	maleY		= 0, 
	maleZ		= 0, 
	maleRot		= 180, 
	maleAnim	= 3720521, 
	femaleX		= 0, 
	femaleY		= 0, 
	femaleZ		= 0, 
	femaleRot	= 180, 
	femaleAnim	= 3392791
}
local DEFAULT_AVAILABLE_TEXT = "Click to Interact"
local DEFAULT_OCCUPIED_TEXT = "Object Occupied"
local PENDING_TIME = 2 --wait for 2 ticks before removing a player that never arrived into position



function create(self, input)
	self.tooltip = DEFAULT_AVAILABLE_TEXT
	_super.create(self, input)
	if input == nil then 
		input = {}
	end
	
	self.pendingAdditions = {} --Table of players who are scheduled to be added to a node
	self.nodes = {}

	self.interactionRange = input.interactionRange or DEFAULT_INTERACTION_RANGE
	self.triggerRadius = input.triggerRadius or DEFAULT_TRIGGER_RADIUS

	input.playerPositions = input.playerPositions or DEFAULT_PLAYER_POSITIONS

	self.playerPositions = {}
	for k, v in pairs(input.playerPositions) do
		self.playerPositions[tostring(k)] = {
			male = {
				x			= v.maleX,
				y			= v.maleY,
				z			= v.maleZ,
				rotation	= v.maleRot,
				animation	= v.maleAnim,
			},
			female = {
				x			= v.femaleX,
				y			= v.femaleY,
				z			= v.femaleZ,
				rotation	= v.femaleRot,
				animation	= v.femaleAnim,
			}
		}
	end

	self.availableText = input.availableText or DEFAULT_AVAILABLE_TEXT
	self.occupiedText = input.occupiedText or DEFAULT_OCCUPIED_TEXT

	local k, v
	for k, v in pairs(self.playerPositions) do
		if v.male.rotation == nil then v.male.rotation = 0 end
		if v.female.rotation == nil then v.female.rotation = 0 end
		self.nodes[k] = {player = nil, gender = nil, trigger = nil}
	end
	
	-- Register for timer event
	Events.registerHandler("TIMER", self.onTimer, self)
	-- Register players that leave the zone
	Events.registerHandler("PLAYER-DEPART", self.onDepart, self)
	
	self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
end

function destroy(self)
	_super.destroy(self)
	for node, data in pairs(self.nodes) do
		if data.player ~= nil then
			playerSetAnimation({user = data.player, animation = 0})
			self.removePlayerFromNode(self, data.player, self.getPlayerNode(self, data.player))
		end
	end
	
    if self.proxTrigger then
        Events.removeClientTrigger(self.proxTrigger)
    end
end

----------------------------------------
-- Event handlers
----------------------------------------

-- Depart handler
function onDepart(self, event)
	self.onPlayerDepart(self, event)
end

-- Wrapper for onDepart
function onPlayerDepart(self, event)
	for node, data in pairs(self.nodes) do
		if data.player ~= nil and data.player == event.player then
			self.removePlayerFromNode(self, event.player, self.getPlayerNode(self, event.player))
		end
	end
end

-- Call use functionality for node
function onUse(self,event)
	-- Fake a right click event
	event.count = 1 
	event._type = "MOUSE-RIGHT-CLICK" 
	event.modifiers =  { shift = false, ctrl = false, alt = false }
	
	if(event.PID and self.PID == event.PID) then
		event.clickTable = {}
		event.clickTable[1] = event.player.position.x or 1
		event.clickTable[2] = event.player.position.y or 1
		event.clickTable[3] = event.player.position.z or 1
		self:onActivate(event)
	end
end

-- On Right Click
function onRightClick(self, event)
 	self:onActivate(event) 
end

function onLeftClick(self, event)
	local handEquipped = (event.player.handEquipped == true or event.player.handEquipped == nil)
	local doesPlayerOwn = self.owner == event.player.name
	-- if the hand tool is selected and the player does not own 
	if handEquipped and not doesPlayerOwn then
		self:onActivate(event)
	end 
end

-- Called when a trigger is entered or exited
function onNodeTrigger(self, event)
	if event.triggerType == 1 and event.player then
		if self.pendingAdditions[event.player.ID] and self.nodes[self.pendingAdditions[event.player.ID].node] and
		   self.nodes[self.pendingAdditions[event.player.ID].node].trigger and self.nodes[self.pendingAdditions[event.player.ID].node].trigger == event.param then
			self.pendingAdditions[event.player.ID] = nil --player has arrived, no longer pending
		end
	elseif event.triggerType == 2 and event.player then
		self.removePlayerFromNode(self, event.player, self.getPlayerNode(self, event.player))
	end
end

-- Timer handler
function onTimer(self, event)
	self.onServerTick(self, event)
end

-- Wrapper for onTimer
function onServerTick(self, event)
	for k, v in pairs(self.pendingAdditions) do
		v.timer = v.timer - 1
		if v.timer <= 0 and self.nodes[v.node].player == v.player then
			self.removePlayerFromNode(self, v.player, self.getPlayerNode(self, v.player))
			playerSetAnimation({user = v.player, animation = 0})
			v = nil
		end
	end
end

----------------------------------------
-- Class functions
----------------------------------------

function onActivate(self, event)
	local uPlayer = event.player
	
	if not Players.validatePlayer(uPlayer) then
		return
	end
	
	local availableNode = self.getBestAvailableNode(self, event)
	if availableNode == nil then
		kgp.playerShowStatusInfo(uPlayer.ID, (self.name or "Object") .. " is already in use.", 3, 255, 0, 0)
		return
	end

	if self:isPointWithinRange(uPlayer.position, self.interactionRange) then
		-- If the player is within the interaction range
		self.addPlayerToNode(self, uPlayer, availableNode)
	else
		-- Status menu
		kgp.playerShowStatusInfo(uPlayer.ID, "You must be closer to interact with that object.", 3, 255, 0, 0)
	end
end

-- This function should be overridden for different types of seats
function getBestAvailableNode(self, event)
	local player = event.player
	local gender = nil
	if player == nil or Players.validatePlayerGender(player.gender) == false then
		return nil
	end
	
	if player.gender == "M" then
		gender = "male"
	else
		gender = "female"
	end
	
	local closestNode = nil
	local closestDistance = nil

	local k, v
	for k, v in pairs(self.nodes) do
		local tPositions = self.playerPositions[k]
		if tPositions == nil then return nil end
		if v.player == nil and tPositions[gender] ~= nil then
			local tClickPos = {x = event.clickTable[1], y = event.clickTable[2], z = event.clickTable[3]}
			local nodeDistance = Math.getVectorDistance(tClickPos, tPositions[gender])
			if closestNode == nil or nodeDistance < closestDistance then
				closestNode = k
				closestDistance = nodeDistance
			end
		end
	end
	return closestNode
end

function addPlayerToNode(self, player, node)
	if node == nil then return end
	if self.nodes[node] == nil then return end
	
	self.removePlayerFromNode(self, player, self.getPlayerNode(self, player))
	local animation = nil
	if player == nil or Players.validatePlayerGender(player.gender) == false then
		return
	end
	
	self.pendingAdditions[player.ID] = {player = player, timer = PENDING_TIME, node = node} --add player to pending list
	
	local gender = player.gender
	if gender == "M" then
		gender = "male"
	else
		gender = "female"
	end
	
	local tPositions = self.playerPositions[node]
	if tPositions then
		if tPositions[gender] then
			if tPositions[gender].animation ~= nil then
				animation = tPositions[gender].animation
			end
		end
	end

	local nodeGlobal = self.getNodeGlobal(self, node, gender)

	playerTeleport({user = player, x = nodeGlobal.x, y = nodeGlobal.y, z = nodeGlobal.z, rx = nodeGlobal.rx, ry = nodeGlobal.ry, rz = nodeGlobal.rz})
	if animation ~= nil then
		playerSetAnimation({user = player, animation = animation})
	end

	local tNode = self.nodes[node]
	tNode.player = player
	tNode.gender = gender
	tNode.position = nodeGlobal

	local trigger = Events.setTriggerOnPosition(nodeGlobal.x, nodeGlobal.y, nodeGlobal.z, self.triggerRadius, self.onNodeTrigger, self)
	self.nodes[node].trigger = trigger
	
	if self.isObjectFull(self) then
		self.onObjectOccupied(self)
	end
end

-- Removes a player from the dance floor
function removePlayerFromNode(self, player, node)
	if node == nil then return end
	local tNode = self.nodes[node]
	if tNode == nil then return end

	-- Delete this node's object and its trigger
	if tNode.trigger ~= nil then
		Events.removeTrigger(tNode.trigger)
	end
	
	tNode.player = nil
	tNode.trigger = nil
	tNode.gender = nil
	
	self.onObjectUnoccupied(self)
end

function getPlayerNode(self, player)
	if player == nil then
		return nil
	end

	local node = nil
	for key, data in pairs(self.nodes) do
		if data.player ~= nil and data.player == player then
			node = key
			break
		end
	end

	return node
end

function isObjectFull(self)
	for node, data in pairs(self.nodes) do
		if data.player == nil then
			return false
		end
	end
	
	return true
end

function getNodeOffset(self, node, gender)
	local nodeOffset = {x = 0, y = 0, z = 0}
	local rotationOffset = 0
	
	local tPositions = self.playerPositions[node]
	if tPositions then
		if tPositions[gender] then
			if tPositions[gender].x ~= nil then
				nodeOffset.x = tPositions[gender].x
			end
			if tPositions[gender].y ~= nil then
				nodeOffset.y = tPositions[gender].y
			end
			if tPositions[gender].z ~= nil then
				nodeOffset.z = tPositions[gender].z
			end
			if tPositions[gender].rotation ~= nil then
				rotationOffset = tPositions[gender].rotation
			end
		end
	end
	
	local offsetX = (nodeOffset.x * self.position.rz)+(nodeOffset.z * self.position.rx)
	local offsetZ = (nodeOffset.z * self.position.rz)-(nodeOffset.x * self.position.rx)
	
	local rotationX = self.position.rx
	local rotationZ = self.position.rz
	if rotationOffset ~= 0 then
		rotationX, rotationZ = Math.degreesToVector(Math.vectorToDegrees(rotationX, rotationZ) - rotationOffset)
	end
	
	return {x = offsetX, y = nodeOffset.y, z = offsetZ, rx = rotationX, ry = 0, rz = rotationZ}
end

function getNodeGlobal(self, node, gender)
	local nodeOffset = getNodeOffset(self, node, gender)
	
	return {x = self.position.x + nodeOffset.x, y = self.position.y + nodeOffset.y, z = self.position.z + nodeOffset.z, rx = nodeOffset.rx, ry = nodeOffset.ry, rz = nodeOffset.rz}
end

function onObjectOccupied(self)
	if self.proxTrigger then
		Events.removeClientTrigger(self.proxTrigger)
		self.proxTrigger = nil
	end
	self:updateTooltip(self.occupiedText)
end

function onObjectUnoccupied(self)
	if self.proxTrigger == nil then
		self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
	end
	self:updateTooltip(self.availableText)
end
