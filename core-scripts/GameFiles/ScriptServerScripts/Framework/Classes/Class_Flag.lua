--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_Collectible.lua
Class.createClass("Class_Flag", "Class_Object")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Events			= _G.Events

-----------------------------------------------------------------
-- Constant Definitions
-----------------------------------------------------------------
local DEFAULT_FLAG_OBJECT = "Settings Flag"
local DEFAULT_FLAG_RADIUS = 250
local DEFAULT_FLAG_HEIGHT = 50
local DEFAULT_FLAG_WIDTH = 50
local DEFAULT_FLAG_DEPTH = 50

local BOUND_CHK_BUFFER_Y = 1.5 -- Amount of leeway for checks BELOW the Y value (accounts for slight slopes and the fact that the player avatar is technically slightly beneath the ground)

local CREATOR_ONLY = true -- This object won't display for players

local FLAG_TYPES = 	{
	PVP = "pvp",
	PLAYER_BUILD = "playerBuild",
	PLAYER_DESTRUCTION = "playerDestruction"
}

s_type = "SystemFlag"

function create(self, input)
	input = input or {}
	input.creatorOnly = CREATOR_ONLY
	_super.create(self, input)



	self.name = input.name or DEFAULT_FLAG_OBJECT
	self.flagType = input.flagType or FLAG_TYPES.PVP
	self.enabled = input.enabled and input.enabled == "true"
	self.height = input.height or DEFAULT_FLAG_HEIGHT
	self.width = input.width or DEFAULT_FLAG_WIDTH
	self.depth = input.depth or DEFAULT_FLAG_DEPTH
	self.radius = input.radius or DEFAULT_FLAG_RADIUS
	
	self:setCollision(false)

	-- Update location of moved flag
	Events.registerHandler("FRAMEWORK_DYNAMIC_OBJECT_SAVE_SERVER", self.onDynamicObjectSave, self)

	kgp.gameItemSetPosRot(self.PID, self.position.x, self.position.y, self.position.z, 0, self.position.ry, 1)

	-- A box-shaped trigger with (0, 0, 0) at origin of the parent object
	local triggerMin = {x = -self.depth/2, y = -BOUND_CHK_BUFFER_Y, z = -self.width/2}
	local triggerMax = {x = self.depth/2, y = self.height, z = self.width/2}
	self.flagTrigger = Events.setTrigger({type = kgp.PrimitiveType.Cuboid, min = triggerMin, max = triggerMax }, self.onTrigger, self)

	self:registerSystemFlag({})
end

function onTrigger(self, event)
	local flag = {}
	flag.PID = self.PID
	flag.flagType = self.flagType
	flag.enabled = self.enabled

	Events.sendEvent("SYSTEM_FLAG_TRIGGER", {player = event.player, triggerType = event.triggerType, flagInfo = flag})
end 

function registerSystemFlag(self, event)
	local flagInfo = {}
	flagInfo.PID = self.PID
	flagInfo.position = {x = self.position.x, y = self.position.y, z = self.position.z}
	flagInfo.flagType = self.flagType
	flagInfo.enabled = self.enabled
	flagInfo.height = self.height
	flagInfo.width = self.width
	flagInfo.depth = self.depth
	flagInfo.radius = self.radius
	Events.sendEvent("REGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "flag", info = flagInfo})
end

function onDynamicObjectSave(self, event)
	if event.PID and event.PID == self.PID then
		local flagInfo = {}
		flagInfo.PID = self.PID
		flagInfo.position = {x = event.objPos.x, y = event.objPos.y, z = event.objPos.z}
		flagInfo.flagType = self.flagType
		flagInfo.enabled = self.enabled
		flagInfo.height = self.height
		flagInfo.width = self.width
		flagInfo.depth = self.depth
		flagInfo.radius = self.radius

		Events.sendEvent("REGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "flag", info = flagInfo})
	end
end

function destroy(self)
	_super.destroy(self)
	
	Events.sendEvent("UNREGISTER_SYSTEM_DYNAMIC_OBJECT", { objectType = "flag", PID = self.PID})
end