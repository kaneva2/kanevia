--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_Path.lua
-- Path consisting of waypoints used by riders

--include any libs that are not part of lib_common.lua
include("Lib_SaveLoad.lua")
include("Lib_GameDefinitions.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local Debug				= _G.Debug
local SaveLoad			= _G.SaveLoad
local GameDefinitions	= _G.GameDefinitions
local Events			= _G.Events
local Class				= _G.Class

Class.createClass("Class_PathRider", "Class_BaseMobile")

Class.useBehaviors({
	"Behavior_Sound"
})

Class.createProperties({
	"associatedPathPID"
})

local CREATOR_ONLY = false -- This object will display for players
local DEFAULT_MODE_LABELS = {God = {"name"}}

local DEFAULT_ACC = 2
--local DEFAULT_DEC = 2

s_type = "pathRider"

-----------------------------------------------------------------
-- Class-specific functions
-----------------------------------------------------------------
function create(self, input)
	--assign input to be passed to base class
	input = input or {}
	input.creatorOnly = CREATOR_ONLY
	input.modeLabels = DEFAULT_MODE_LABELS
	--create base class with the input needed
	self.registered = false
	Events.registerHandler("FRAMEWORK_OBJECT_REGISTERED", self.onRegistered, self)
	_super.create(self, input)
	
	--assign shared data
	if input.additionalPathInfo then
		self.associatedPathPID = input.additionalPathInfo.parentPathPID
		self.finalSpeed = input.additionalPathInfo.speed
		if input.additionalPathInfo.friction then
			self:setFriction(input.additionalPathInfo.friction)
		end
	end
	self.triggerEnabled = nil
	
	--create and assign locals to this specific class
	self.currentWaypointPID = nil
	self.destinationWaypointPID = nil
	self.currentState = nil
	self.triggerEnabled = nil
	
	self.tickOnEnteringIdle = 0
	self.easeIn = nil
	self.easeOut = nil
	
	self.acceleration = DEFAULT_ACC
	
	self.destX = nil
	self.destY = nil
	self.destZ = nil
	self.destRX = nil
	self.destRY = nil
	self.destRZ = nil
	
	self.noOfPlayersInteracting = 0
	
	self.infoRecvdFromCrntWaypoint = false
	self.infoRecvdFromDestWaypoint = false
	self.lookAtWhileMoving = nil
	self.pauseAtNextWaypoint = nil
	
	self.movementComplete = false
	
	--register events and handlers
	Events.registerHandler("PATHRIDER_ADDED_CONFIRMATION", self.pathRiderAddedConfirmation, self)
	Events.registerHandler("PATH_DESTROYED_CLEANUP", self.onPathDestroyedCleanup, self)
	Events.registerHandler("PATH_INFO_FOR_RIDER", self.onPathInfoForRider, self)
	Events.registerHandler("MOVEMENT_INFO_FOR_RIDER", self.onMovementInfoForRider, self)
	Events.registerHandler("WAYPOINT_INFO_FOR_RIDER", self.onWaypointInfoForRider, self)
	Events.registerHandler("TRIGGER_RIDER_PATHING", self.onTriggerRiderPathing, self)
	Events.registerHandler("PLAYER_RIDER_INTERACTION", self.onPlayerRiderInteraction, self)
	Events.registerHandler("DEL_PATH_WAYPOINT", self.delPathWaypoint, self)
	Events.registerHandler("UpdatePathWaypointPropertiesEvent", self.updatePathWaypointPropertiesEvent, self)
end

function destroy(self)
	Events.sendClientEvent("PathRiderPickedUpEvent", {riderPID = self.PID})
	_super.destroy(self)
	Events.unregisterHandler("FRAMEWORK_OBJECT_REGISTERED", self.onRegistered, self)
	Events.unregisterHandler("PATHRIDER_ADDED_CONFIRMATION", self.pathRiderAddedConfirmation, self)
	Events.unregisterHandler("PATH_DESTROYED_CLEANUP", self.onPathDestroyedCleanup, self)
	Events.unregisterHandler("PATH_INFO_FOR_RIDER", self.onPathInfoForRider, self)
	Events.unregisterHandler("MOVEMENT_INFO_FOR_RIDER", self.onMovementInfoForRider, self)
	Events.unregisterHandler("WAYPOINT_INFO_FOR_RIDER", self.onWaypointInfoForRider, self)
	Events.unregisterHandler("TRIGGER_RIDER_PATHING", self.onTriggerRiderPathing, self)
	Events.unregisterHandler("PLAYER_RIDER_INTERACTION", self.onPlayerRiderInteraction, self)
	Events.unregisterHandler("DEL_PATH_WAYPOINT", self.delPathWaypoint, self)
	Events.unregisterHandler("UpdatePathWaypointPropertiesEvent", self.updatePathWaypointPropertiesEvent, self)
end

function onRegistered(self, event)
	if event.PID == self.PID and self.registered == false then
		self.registered = true
		Events.sendEvent("ADD_PATH_RIDER", {newPathRiderPID = self.PID, parentPathPID = self.associatedPathPID})
	end
end

function pathRiderAddedConfirmation(self, event)
	if event.PID == self.PID then
		self.currentState = GameDefinitions.RIDER_STATES.onHold
		self.currentWaypointPID = event.crntWaypoint
		if event.noOfPathWaypoints > 1 then
			Events.sendEvent("REQUEST_PATH_INFO_FOR_RIDER", {riderPID = self.PID, parentPathPID = self.associatedPathPID, currentState = GameDefinitions.RIDER_STATES.idle,
															 currentWaypointPID = self.currentWaypointPID, destinationWaypointPID = self.destinationWaypointPID})
		end
		Events.sendClientEvent("PathRiderPlacedEvent", {riderPID = self.PID})
	end
end

function onPathDestroyedCleanup(self, event)
	if event.parentPathPID == self.associatedPathPID then
		Events.sendEvent("OBJECT_DESPAWN", {spawnerPID = self.associatedPathPID, despawnPID = self.PID})
	end
end

function onPathInfoForRider(self, event)
	if event.riderPID == self.PID then
		self.currentWaypointPID = event.currentWaypointPID
		self.destinationWaypointPID = event.destinationWaypointPID
		if event.crntAnim == nil then
			self:setAnimation(0)
		else
			self:setAnimation(event.crntAnim)
		end
		if event.crntParticle == nil then
			self:removeParticle()
		else
			self:setParticle(event.crntParticle)
		end
		if event.crntSound then
			playSound({soundID = event.crntSound})
		end
		if event.currentState == GameDefinitions.RIDER_STATES.onHold then
			self.currentState = GameDefinitions.RIDER_STATES.onHold
			return
		elseif event.currentState == GameDefinitions.RIDER_STATES.moving then
			if self.currentWaypointPID == nil then
				crntWaypointInfo = {crntWaypointPID = self.currentWaypointPID, easeOut = false}
				self:consolidateMoveInfo("current", crntWaypointInfo)
			end
		end
		Events.sendEvent("REQUEST_WAYPOINT_INFO_FOR_RIDER", {riderPID = self.PID, crntWaypointPID = self.currentWaypointPID, destWaypointPID = self.destinationWaypointPID, crntState = event.currentState})
	end
end

function onMovementInfoForRider(self, event)
	if event.riderPID == self.PID then
		if event.crntAnim == nil then
			self:setAnimation(0)
		else
			self:setAnimation(event.crntAnim)
		end
		if event.crntParticle == nil then
			self:removeParticle()
		else
			self:setParticle(event.crntParticle)
		end
		if event.crntSound then
			playSound({soundID = event.crntSound})
		end
	end
end

function onWaypointInfoForRider(self, event)
	if event.riderPID == self.PID then
		if event.currentState == GameDefinitions.RIDER_STATES.idle then
			self.currentState = GameDefinitions.RIDER_STATES.idle
			self.pauseTime = event.idleStateInfo.pauseTime
			self.tickOnEnteringIdle = 0
		elseif event.currentState == GameDefinitions.RIDER_STATES.moving then
			if self.currentWaypointPID == event.waypointPID then
				crntWaypointInfo = {crntWaypointPID = self.currentWaypointPID, easeOut = event.movingStateInfo.easeOut}
				self:consolidateMoveInfo("current", crntWaypointInfo)
			elseif self.destinationWaypointPID == event.waypointPID then
				destWaypointInfo = {destWaypointPID = self.destinationWaypointPID, easeIn = event.movingStateInfo.easeIn, rotationType = event.movingStateInfo.rotationType, pauseTime = event.movingStateInfo.pauseTime}
				self:consolidateMoveInfo("destination", destWaypointInfo)
			end
		end
	end
end

function onTriggerRiderPathing(self, event)
	if self.PID == event.riderPID and self.currentState == GameDefinitions.RIDER_STATES.onHold then
		Events.sendEvent("REQUEST_PATH_INFO_FOR_RIDER", {riderPID = self.PID, parentPathPID = self.associatedPathPID, currentState = GameDefinitions.RIDER_STATES.idle,
														 currentWaypointPID = self.currentWaypointPID, destinationWaypointPID = self.destinationWaypointPID})
	end
end

function delPathWaypoint(self, event)
	if self.destinationWaypointPID == event.delPathWaypointPID and self.currentState == GameDefinitions.RIDER_STATES.moving then
		self:stopMovement()
		self.destinationWaypointPID = nil
		Events.sendEvent("REQUEST_PATH_INFO_FOR_RIDER", {riderPID = self.PID, parentPathPID = self.associatedPathPID, currentState = GameDefinitions.RIDER_STATES.moving,
														 currentWaypointPID = self.currentWaypointPID, destinationWaypointPID = self.destinationWaypointPID})
	end
end

function updatePathWaypointPropertiesEvent(self, event)
	if event.pathWaypointPID == self.currentWaypointPID and self.currentState == GameDefinitions.RIDER_STATES.idle then
		newProps = event.pathWaypointProperties
		if newProps ~= nil then
			self.pauseTime = newProps.pauseTime or 0
		end
	end
end

function onPlayerRiderInteraction(self, event)
	if self.PID == event.riderPID or event.riderPID == nil then
		if event.interacting == true then
			self.noOfPlayersInteracting = self.noOfPlayersInteracting + 1
		else
			self.noOfPlayersInteracting = self.noOfPlayersInteracting - 1
			if self.noOfPlayersInteracting < 0 then
				self.noOfPlayersInteracting = 0
			end
		end
		if self.noOfPlayersInteracting > 0 and self.inMotion == true then
			self:pauseMovement()
			Events.sendEvent("REQUEST_RIDER_MOVEMENT_OPTIONS", {riderPID = self.PID, parentPathPID = self.associatedPathPID, movementState = "idle"})
		else
			if self.currentState == GameDefinitions.RIDER_STATES.onHold then
				Events.sendEvent("REQUEST_PATH_INFO_FOR_RIDER", {riderPID = self.PID, parentPathPID = self.associatedPathPID, currentState = GameDefinitions.RIDER_STATES.moving,
																 currentWaypointPID = self.currentWaypointPID, destinationWaypointPID = self.destinationWaypointPID})
			elseif self.currentState == GameDefinitions.RIDER_STATES.moving then
				self:resumeMovement()
				Events.sendEvent("REQUEST_RIDER_MOVEMENT_OPTIONS", {riderPID = self.PID, parentPathPID = self.associatedPathPID, movementState = "move"})
			end
		end
	end
end

function onMovementArrived(self, event)
	local riderAngle = Math.vectorToDegrees(self.position.rz, self.position.rx)
	self.currentState = GameDefinitions.RIDER_STATES.onHold
	self.movementComplete = true
	local destWaypointGS = kgp.objectGetGameState(self.destinationWaypointPID)
	if destWaypointGS.ID == nil or (self.lookAtWhileMoving == true and self.pauseAtNextWaypoint == false) or self.lookAtWhileMoving == false then
		onRotateComplete(self)
	else
		local destAngle = Math.vectorToDegrees(destWaypointGS.position.rz, destWaypointGS.position.rx)
		self:rotateBy({angle = riderAngle - destAngle, time = 5})
	end
end

function onRotateComplete(self, event)
	if self.movementComplete then
		self.currentWaypointPID = self.destinationWaypointPID
		self.movementComplete = false
		self.destinationWaypointPID = nil
		self.easeIn = nil
		self.easeOut = nil
		self.destX = nil
		self.destY = nil
		self.destZ = nil
		self.destRX = nil
		self.destRY = nil
		self.destRZ = nil
		self.infoRecvdFromCrntWaypoint = false
		self.infoRecvdFromDestWaypoint = false
		self.lookAtWhileMoving = nil
		if self.pauseAtNextWaypoint then
			Events.sendEvent("REQUEST_PATH_INFO_FOR_RIDER", {riderPID = self.PID, parentPathPID = self.associatedPathPID, currentState = GameDefinitions.RIDER_STATES.idle,
															 currentWaypointPID = self.currentWaypointPID, destinationWaypointPID = self.destinationWaypointPID})
		else
			Events.sendEvent("REQUEST_PATH_INFO_FOR_RIDER", {riderPID = self.PID, parentPathPID = self.associatedPathPID, currentState = GameDefinitions.RIDER_STATES.moving,
															 currentWaypointPID = self.currentWaypointPID, destinationWaypointPID = self.destinationWaypointPID})
		end
		self.pauseAtNextWaypoint = nil
	end
end

function OnPathRiderTick(self, event)
	if self.currentState == GameDefinitions.RIDER_STATES.idle then
		if self.tickOnEnteringIdle == 0 then
			self.tickOnEnteringIdle = event.tick
		elseif ((event.tick - self.tickOnEnteringIdle) > (self.pauseTime * 1000)) then
			if self.noOfPlayersInteracting == 0 then
				Events.sendEvent("REQUEST_PATH_INFO_FOR_RIDER", {riderPID = self.PID, parentPathPID = self.associatedPathPID, currentState = GameDefinitions.RIDER_STATES.moving,
																 currentWaypointPID = self.currentWaypointPID, destinationWaypointPID = self.destinationWaypointPID})
			end
			self.currentState = GameDefinitions.RIDER_STATES.onHold
		end
	end
end

-----------------------------------------------------------------
-- Local functions
-----------------------------------------------------------------
function consolidateMoveInfo(self, waypointType, waypointInfo)
	if waypointType == "current" then
		self.infoRecvdFromCrntWaypoint = true
		self.currentWaypointPID = waypointInfo.crntWaypointPID
		self.easeOut = waypointInfo.easeOut or false
	elseif waypointType == "destination" then
		self.infoRecvdFromDestWaypoint = true
		self.destinationWaypointPID = waypointInfo.destWaypointPID
		self.easeIn = waypointInfo.easeIn or false
		if waypointInfo.rotationType == "Hard" then
			self.lookAtWhileMoving = true
		else
			self.lookAtWhileMoving = false
		end
		if waypointInfo.pauseTime > 0 then
			self.pauseAtNextWaypoint = true
		else
			self.pauseAtNextWaypoint = false
		end
	end
	
	if self.infoRecvdFromCrntWaypoint and self.infoRecvdFromDestWaypoint then
		local destWaypointGS = kgp.objectGetGameState(self.destinationWaypointPID)
		if destWaypointGS.ID == nil then
			self.currentState = GameDefinitions.RIDER_STATES.onHold
			self.easeIn = nil
			self.easeOut = nil
			self.destX = nil
			self.destY = nil
			self.destZ = nil
			self.destRX = nil
			self.destRY = nil
			self.destRZ = nil
			self.infoRecvdFromCrntWaypoint = false
			self.infoRecvdFromDestWaypoint = false
			self.lookAtWhileMoving = nil
			self.pauseAtNextWaypoint = nil
			Events.sendEvent("REQUEST_PATH_INFO_FOR_RIDER", {riderPID = self.PID, crntWaypointPID = self.currentWaypointPID, destWaypointPID = self.destinationWaypointPID, crntState = GameDefinitions.RIDER_STATES.moving})
		else
			
			self.infoRecvdFromCrntWaypoint = false
			self.infoRecvdFromDestWaypoint = false
			self.destX = destWaypointGS.position.x
			self.destY = destWaypointGS.position.y
			self.destZ = destWaypointGS.position.z
			self.destRX = destWaypointGS.position.rx
			self.destRY = destWaypointGS.position.ry
			self.destRZ = destWaypointGS.position.rz
			self.speed = self.finalSpeed
			self.currentState = GameDefinitions.RIDER_STATES.moving
			local input = {x = self.destX, y = self.destY, z = self.destZ, lookAt = self.lookAtWhileMoving, rx = self.destRX, ry = self.destRY, rz = self.destRZ,
						   easeIn = self.easeIn, easeOut = self.easeOut, acceleration = self.acceleration}
			self:moveTo(input)
		end
	end
end