--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Controller.lua
Class.createClass("Controller", "Singleton")

local running = false
local options

local composeKeyString

--------------------------------------------------------------------
-- Constructor

function create( self )
	_super.create( self )

	self.PID = kgp.objectGetId()

	local name = Class.getClassName(self)

	-- register this class singleton as a service
	Class.registerAsService(name)

	-- initialize options as SharedData for persistency
	options = SharedData.new( {"Controller", name, "Config"} )

	-- Export config method
	Events.addLocalEvents({ "CONFIG" })
end


--------------------------------------------------------------------
-- Public methods

function start( self, user )
	-- register event handlers to exported functions
	if self._exports then
		Events.registerHandlersByExportTable(self, self._exports)
	end

	running = true
end

function stop( self, user )
	running = false
end

function isRunning()
	return running
end

function getConfig(key)
	local keyStr = composeKeyString(key)
	return options[keyStr]
end

--------------------------------------------------------------------
-- Handlers

function config(self, key, value)
	local keyStr = composeKeyString(key)
	options[keyStr] = value
end

--------------------------------------------------------------------
-- Private methods

function composeKeyString(key)
	local keyStr = ""
	if type(key)=="string" then
		keyStr = key
	elseif type(key)=="table" and #key~=0 then
		for _, v in ipairs(key) do
			keyStr = keyStr .. v .. "."
		end

		keyStr = string.sub(keyStr, 1, -2)
	else
		error("Invalid argument \"key\", expected: string or table, got " .. type(key), 2)
	end

	return keyStr
end