--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

include("Lib_Map.lua")
-- Bind libraries to a local variable for efficiency's sake
local MapDefinitions = _G.MapDefinitions
local Debug = _G.Debug 

MapHelper = {}

--------------------------------------
-- Local Constants
--------------------------------------


--------------------------------------
-- Local Function Declarations
--------------------------------------

local getMapObject				-- function(object, playerName, itemLookupTable)
local getMapObjectInfo			-- function(object, isOwner, itemLookupTable)
local isObjectPlaceableLoot		-- function(object)
local isObjectMonster			-- function(object, itemLookupTable)
local isObjectActiveEventFlag	-- function(object)
local isObjectPlayerCorpse		-- function(object, playerName)
local isObjectHarvestable		-- function(object, itemLookupTable)
local generatePlayerData		-- function(object)
local generateObjectData		-- function(object)


--------------------------------------
-- Exposed Function Definitions
--------------------------------------

function MapHelper.generateMapDataForPlayer(playerName, objectList, itemLookupTable)
	local mapObjectList = {}
	
	for key, object in pairs(objectList) do
	
		local mapObject = getMapObject(object, playerName, itemLookupTable)
		
		-- Add the object to the list of items to be sent off
		if mapObject then
			if mapObject.PID then
				mapObjectList[tostring(mapObject.PID)] = mapObject
			elseif mapObject.name then
				mapObjectList[mapObject.name] = mapObject
			end
		end
	end
	
	return mapObjectList
end


function MapHelper.generateSingleMapObject(object, itemLookupTable)
	return getMapObject(object, nil, itemLookupTable)
end


--------------------------------------
-- Local Function Definitions
--------------------------------------



getMapObject = function(object, playerName, itemLookupTable)
	-- NOTE: Player info is to be removed
	local isPlayer = object.type == "Player"
	if( isPlayer ) then
		-- NOTE: WE ARE NO LONGER HANDLING PLAYERS FOR MAPS ON THE SERVER SIDE
		do	
			return
		end
	
		-- Handle players
		local isRequestingPlayer = playerName and object.name == playerName 
		if not isRequestingPlayer then
			local playerData = generatePlayerData(object)
			return playerData
		end
	else
		-- Handle vendors, quest givers, claim flags, pets, LGPs, monsters, event flags, and corpses
		local mapObjectInfo = getMapObjectInfo(object, playerName, itemLookupTable)
		
		if mapObjectInfo then
			local objectData = generateObjectData(object, mapObjectInfo)
			return objectData, mapObjectInfo.player
		end
	end
end

-- If the playerName parameter is nil, then ownership is ignored
getMapObjectInfo = function(object, playerName, itemLookupTable)
	local isOwner = playerName == nil or object.owner == playerName
	
	if	object.type == "Vendor" then
		
		local returnTable = {
			type = MapDefinitions.LABEL_INDEX.VENDOR
		}
		return returnTable
	
	elseif object.type == "QuestGiver" then
		
		local returnTable = {
			type = MapDefinitions.LABEL_INDEX.QUEST_GIVER
		}
		
		return returnTable
	
	elseif object.type == "claimFlag" then
		
		local returnTable = {
			type = MapDefinitions.LABEL_INDEX.LANDCLAIM
		}
		
		return returnTable
	
	elseif (object.type == "Pet" and isOwner) then				--> Must be owned
		
		local returnTable = {
			type = MapDefinitions.LABEL_INDEX.PET,
			player = object.owner
		}
		
		return returnTable
	
	elseif (isObjectPlaceableLoot(object) and isOwner) then		--> Must be owned and generating loot
	
		local returnTable = {
			type = MapDefinitions.LABEL_INDEX.LOOT_PLACEABLE,
			player = object.owner
		}
		
		return returnTable
	
	elseif isObjectActiveEventFlag(object) then					--> Must be active
		
		local returnTable = {
			type = MapDefinitions.LABEL_INDEX.EVENT
		}
		
		return returnTable
	
	else
		local isCorpse, owner = isObjectPlayerCorpse(object, playerName)
		if isCorpse then					--> Must have dropped off from the player
			
			local returnTable = {
				type = MapDefinitions.LABEL_INDEX.CORPSE,
				player = owner
			}
			
			return returnTable
		end

		local isHarvestable, spawnRadius = isObjectHarvestable(object, itemLookupTable)
		if isHarvestable then
			
			local returnTable = {
				type = MapDefinitions.LABEL_INDEX.HARVESTABLE,
				radius = spawnRadius
			}
			
			return returnTable
		end
		
		local isMonster, spawnRadius =  isObjectMonster(object, itemLookupTable)
		if isMonster then
			local returnTable = {
				type = MapDefinitions.LABEL_INDEX.MONSTER,
				radius = spawnRadius
			}
			
			return returnTable
		end
	end
end



isObjectPlaceableLoot = function(object)
	return object.type == "placeable_loot" and object.doesGenerateLoot
end


isObjectMonster = function(object, itemLookupTable)
	if object.type == "spawner" then
		local spawnedObjectUNID = tonumber(object.spawnUNID)
		if spawnedObjectUNID then
			local spawnedObjectDef = itemLookupTable[tostring(spawnedObjectUNID)]
			if spawnedObjectDef then
				local isMonsterSpawner = spawnedObjectDef.behavior == "monster"
				if isMonsterSpawner then
					-- Now that we have established this is a monster spawner,
					-- determine whether we need to send a radius to the client
					local spawnerUNID = tonumber(object.UNID)
					if spawnerUNID then
						local spawnerDef = itemLookupTable[tostring(spawnerUNID)]
						if spawnerDef then
							local spawnerParams = spawnerDef.behaviorParams
							local maxSpawnCount = (spawnerParams and spawnerParams.maxSpawns) or 1
							local spawnRadius
							if maxSpawnCount > 1 then
								spawnRadius = spawnerDef.spawnRadius
							end
							
							return true, spawnRadius
						end
					end
				end
			end
		end
	end
	
	return false
end

isObjectActiveEventFlag = function(object)
	return object.type == "EventFlag" and object.status == "Occurring"
end


isObjectPlayerCorpse = function(object, playerName)
	local isCorpse = object.type == "Item Container" and object.isCorpse == true and (playerName == nil or playerName == object.corpseOwner)
	return isCorpse, object.corpseOwner
end

isObjectHarvestable = function(object, itemLookupTable)
	if object.type == "spawner" then
		local spawnerDefinition = itemLookupTable[tostring(object.UNID)]
		if spawnerDefinition then
			if spawnerDefinition.behavior == "spawner_harvestable" then
				local spawnerParams = spawnerDefinition.behaviorParams
				local maxSpawnCount = (spawnerParams and spawnerParams.maxSpawns) or 1
				local spawnRadius
				if maxSpawnCount > 1 then
					spawnRadius = spawnerDefinition.spawnRadius
				end
				return true, spawnRadius
			end 
		end
	end
	return false
end


-- Data content: NAME, POSITION, TYPE
generatePlayerData = function(object)
	local position = object.position
	local data = {
		name = object.name, 
		position = {
			x = position.x,
			y = position.y,
			z = position.z
		}, 
		type = MapDefinitions.LABEL_INDEX.PLAYERS
	}
	return data
end


-- Data content: UNID, PID, POSITION, TYPE, RADIUS (optional)
generateObjectData = function(object, mapObjectInfo)
	local position = object.position
	local data = {
		UNID = object.UNID,
		PID = object.PID,
		position = {
			x = position.x,
			y = position.y,
			z = position.z
		},
		type = mapObjectInfo.type,
		radius = mapObjectInfo.radius
	}
	return data
end

