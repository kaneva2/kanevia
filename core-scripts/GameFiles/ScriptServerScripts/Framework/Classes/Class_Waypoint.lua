--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_Waypoint.lua
-- Waypoint used by moving objects

Class.createClass("Class_Waypoint", "Class_Base")

local CREATOR_ONLY = true -- This object won't display for players
local DEFAULT_MODE_LABELS = {God = {"name"}}
local DEFAULT_INTERACTION_RANGE = 25

s_type = "waypoint"

function create(self, input)
	input = input or {}
	input.creatorOnly = CREATOR_ONLY
	input.modeLabels = DEFAULT_MODE_LABELS
	_super.create(self, input)
	
	if tonumber(input.animation) then
		self.animation = tonumber(input.animation)
		self:setAnimation(self.animation)
	end
	
	self:hide() -- Hide by default
	self:setCollision(false) -- Waypoints have no collision

	self.radius = input.radius or DEFAULT_INTERACTION_RANGE -- TO DO: get actual radius from quest configuration
	-- log("\n self.radius: "..tostring(self.radius))
	self.trigger = Events.setTrigger(self.radius, self.onTrigger, self)
end

function destroy(self)
	if self.isBeingRemoved then
		--Send to GameItemEditor and let them know waypoint is deleted
		Events.sendClientEvent("FRAMEWORK_WAYPOINT_DELTED", {PID = self.PID})
	end
	_super.destroy(self)
end

function onTrigger(self, event)
	if event.player == nil then return end
	if event.triggerType == 1 and event.player.mode ~= "God" then
		Events.sendClientEvent("FRAMEWORK_WAYPOINT_TRIGGERED", {PID = self.PID}, event.player.ID)
	end
end