--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_Seed.lua
--
-- Seed class inherited from Harvestable
--
----------------------------------------

include("Lib_SaveLoad.lua")
include("Lib_GameDefinitions.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local Debug				= _G.Debug
local SaveLoad			= _G.SaveLoad
local Events			= _G.Events
local Class				= _G.Class

local LOG_FUNCTION_CALL, LOG_PLACEABLE_LOOT_FILTER = Debug.generateCustomLog("Class_Seed - ")
Debug.enableLogForFilter(LOG_PLACEABLE_LOOT_FILTER, false) -- Set this to true if you want to get a complete log of Class_Seed function calls

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
Class.import("Inventory")

-- Create a Seed class inherited from Class_BaseItemContainerFromHarvestable that in turn is inherited from Class_Harvestable
Class.createClass("Class_Seed", "Class_BaseItemContainerFromHarvestable")

Class.createProperties({
	"spawnerPID",
	"spawnGLID",
	"requiredTool",
	"readyToPick",
	"isStaged",
	"stagesCount",
	"showLabels",
	"behavior",
})


-----------------------------------------------------------------
-- Constant definitions
-----------------------------------------------------------------
local DEFAULT_NAME = "Seed"
local SEED_KEY = "DNC_seed"

-- Time constants
-- TO DO: move these to a public library somewhere
local MILISECONDS_PER_SECOND = 1000
local SECONDS_PER_MINUTE = 60
local SECONDS_PER_HOUR = 3600
local SECONDS_PER_DAY = 86400
local MILISECONDS_PER_MINUTE = MILISECONDS_PER_SECOND * SECONDS_PER_MINUTE
-- local MILISECONDS_PER_MINUTE = 600 -- SPEED MODE

local MIN_INTERACTION_DISTANCE = 20
local MIN_INTERACTION_DISTANCE_SQRD = MIN_INTERACTION_DISTANCE  * MIN_INTERACTION_DISTANCE

--Indicator info
local ICON_OBJECT_FILTER = 3
local ICON_SPEED	= 0.2
local ICON_TRAVEL	= 0.2
local ICON_OFFSET_X = 0
local ICON_OFFSET_Y = 0.6
local ICON_OFFSET_Z = 0
local ICON_GLID = 4669333


s_type = "Seed"


----------------------------------------
-- Forward declarations for local functions
----------------------------------------

----------------------------------------
-- Constructor - Destructors
----------------------------------------

function create(self, input)
	LOG_FUNCTION_CALL("create")
	
	input = input or {}

	self.behavior = input.behavior

	_super.create(self, input)
		
	self:initFromInput(input)

	-- Load current state
	self:loadSeedState("default")

	-- Local properties
	self.isStaged				= true
	self.requiredToolName		= ""
	self.pickLootExists			= false
	self.harvestLootExists		= false
	self.harvestToolExists		= false
	self.highlighted			= false

	-- Timer variables
	self.currentStageTimer = self:getCurrentStageTimer() -- in milliseconds
	self.tick = 0 -- server time in milliseconds
	self.minuteTick = 0
	self.requiredItemsSatisfied = 0
	
	self:initDeadline()

	self:initItemProperties()
	
	if self.overridePickLoot then
		self:requestLoot()
	end
	
	-- Apply relevant labels
	self:updateLabels()
	self:updateClientOutline()

	self.timerEnabled = false
	-- Register Event Handlers
	if self.deadlineToGrow > 0 then
		self:setObjectTimerEnabled(true) -- Register for timer event (but only if you need it)
	end
	Events.registerHandler("GENERIC_USE", self.onGenericUse, self)
	Events.registerHandler("OBJECT_HARVEST", self.onHarvest, self)
	Events.registerHandler("OBJECT_PLACEMENT_GLID_UPDATED", self.onGLIDUpdated, self)
	Events.registerHandler("RETURN_LOOT", self.onLootReturned, self)
end


function destroy(self)
	LOG_FUNCTION_CALL("destroy")

	self:clearLabels()
	
	self:setObjectTimerEnabled(false)
	Events.unregisterHandler("GENERIC_USE", self.onGenericUse, self)
	Events.unregisterHandler("OBJECT_HARVEST", self.onHarvest, self)
	Events.unregisterHandler("OBJECT_PLACEMENT_GLID_UPDATED", self.onGLIDUpdated, self)
	Events.unregisterHandler("RETURN_LOOT", self.onLootReturned, self)
	
	self:enableClientOutline(false)
	
	-- Erase database content for this item if being removed from the world
	if( self.isBeingRemoved ) then
		-- Item is being removed from the world as opposed to the world getting shut down
		self:deleteSeedState("default")
	end
	
	_super.destroy(self)
end


----------------------------------------
-- Event handlers
----------------------------------------

-- On left click handler
function onRightClick(self, event)
	LOG_FUNCTION_CALL("onRightClick")
	
	local currentStage = self:getCurrentStage()

	-- if this final stage and ready to pick
	if currentStage.isFinalStage then
		if self.readyToPick then
			self:pickLoot(event)
		end
	end	
end

function onLeftClick(self, event)
	LOG_FUNCTION_CALL("onLeftClick")

	local currentStage = self:getCurrentStage()

	-- if this final stage and ready to pick
	if currentStage.isFinalStage then
		if self.readyToPick then
			self:pickLoot(event)
		end	
	end
end

-- Timer handler
function onTimer(self, event)
	--LOG_FUNCTION_CALL("onTimer")
	local currentStage = self:getCurrentStage()
	-- If seed is in the final stage and is not pickable, it no longer needs the timer
	if self:isInFinalStage() and not self.pickLootExists then
		self:setObjectTimerEnabled(false)
	else
		if self.tick == 0 then self.tick = event.tick end
		local deltaTick = (event.tick - self.tick) --* 1000 --> Uncomment this to speed things up
		self.currentTime = self.currentTime + deltaTick
		self.tick = event.tick
		
		self:handleGrowTimer()
		-- Update labels once per minute
		if self.minuteTick + MILISECONDS_PER_MINUTE < self.currentTime then
			self.minuteTick = self.currentTime
			self:updateLabels()
		end
	end
end

function onGenericUse(self, event)
	LOG_FUNCTION_CALL("onGenericUse")
	
	-- is this the correct target?
	if event.target == self.PID  then
		local weapon = event.weapon
		
		
		local currentStage = self:getCurrentStage()
		if currentStage then
			if currentStage.requiredItemUNID == nil then
				Events.sendClientEvent("FRAMEWORK_GENERIC_ACCEPTED", {weapon = event.weapon, accepted = false}, event.attacker.ID)
				kgp.playerShowStatusInfo(event.attacker.ID, "Only requires time to move onto next stage. Oh, and some love...", 3, 255, 0, 0)
			else
				if weapon == currentStage.requiredItemUNID then
					local isFinalStage = currentStage.isFinalStage
					if not isFinalStage then
						if self.deadlineToGrow == 0 then
							self:onPlayerFeed(event.attacker.name)
							-- send message back to original player that feed was valid
							Events.sendClientEvent("FRAMEWORK_GENERIC_ACCEPTED", {weapon = event.weapon, accepted = true}, event.attacker.ID)
						else
							Events.sendClientEvent("FRAMEWORK_GENERIC_ACCEPTED", {weapon = event.weapon, accepted = false}, event.attacker.ID)
							kgp.playerShowStatusInfo(event.attacker.ID, "Already advancing to next stage.", 3, 255, 0, 0)
						end
					else
						Events.sendClientEvent("FRAMEWORK_GENERIC_ACCEPTED", {weapon = event.weapon, accepted = false}, event.attacker.ID)
						kgp.playerShowStatusInfo(event.attacker.ID, "Already at final stage.", 3, 255, 0, 0)
					end
				else
					Events.sendClientEvent("FRAMEWORK_GENERIC_ACCEPTED", {weapon = event.weapon, accepted = false}, event.attacker.ID)
					kgp.playerShowStatusInfo(event.attacker.ID, "Requires " .. tostring(currentStage.requiredItemName) .. " to grow.", 3, 255, 0, 0)
				end
			end
		end
	end
end

function onGLIDUpdated(self, event)
	LOG_FUNCTION_CALL("onGLIDUpdated")
	
	if self.PID == event.PID then
		-- load old data table
		local loadString = SEED_KEY .. "_" .. tostring(event.prevPID)
		self:loadSeedState(loadString)
		self:saveSeedState("default")
		self:deleteSeedState(loadString)  -- delete old data table
		
		-- Do the same for the super class
		local items = self:loadContainerContentsFromDB(event.prevPID)
		self:saveContainerContentsToDB(event.PID)
		self:deleteContainerContentsFromDB(event.prevPID)
		self:resetItemsTo(items, true)
		
		self:updateLabels()
		self:updateClientOutline()
	end
end


function onLootReturned(self, event)
	LOG_FUNCTION_CALL("onLootReturned")
	
	if self.PID == event.returnPID then
		self.overridingLootItems = event.lootTable
	end
end

-- Called when this seed is being axed
function onHarvest(self, event)
	LOG_FUNCTION_CALL("onHarvest")
	if event.target == self.PID then
		local harvestStageTemp = "Stage " .. tostring(self.currentStageIndex)
		
		if self:isInFinalStage() then
			self:generateLootChest()
			harvestStageTemp = "Final Stage"
		end
		self:deathHandler()

		if event.attacker then
			self:recordPlayerInteraction(event.attacker.name, "harvest", {harvestStage = harvestStageTemp})
		end
	end
end



-- Called when the Loot Container Menu is closed on the client side.
function onItemContainerClosed(self, event)
	LOG_FUNCTION_CALL("onItemContainerClosed")
	
	_super.onItemContainerClosed(self, self.event)

	if( self.PID == event.PID ) then
		self:checkForLootGenerationReboot()
	end
end


----------------------------------------
-- Class methods
----------------------------------------

function initFromInput(self, input)
	LOG_FUNCTION_CALL("initFromInput")
	
	self.interactionRange = input.interactionRange or MIN_INTERACTION_DISTANCE
	
	-- Core item properties
	self.name = input.name or DEFAULT_NAME
	
	self.version = input.version or 1

	self.stages = {}
	self.stagesCount = 2
	
	--log("SEED VERSION: " .. tostring(self.version))
	
	-- Add the first stage
	local firstStage = {
		GLID				= input.GLID,
		requiredTime		= input.requiredTime or input.stage1Timer,	--> Still supporting variables from the old Class_Seed for backwards compatibility
		requiredItemUNID	= input.requiredItemUNID,
		requiredQuantity	= input.requiredQuantity or 1,
	}
	
	table.insert(self.stages, firstStage)
	
	-- Add intermediary stages
	if input.middleStages then
		for index, value in pairs(input.middleStages) do
			
			local stageEntry = {
				GLID				= value.GLID or value.stageGLID,			--> Still supporting variables from the old Class_Seed for backwards compatibility
				requiredTime		= value.requiredTime or value.stageTimer,	--> Still supporting variables from the old Class_Seed for backwards compatibility
				requiredItemUNID	= value.requiredItemUNID,
				requiredQuantity	= value.requiredQuantity or 1,
			}
			
			table.insert(self.stages, stageEntry)
			
			self.stagesCount = self.stagesCount + 1
		end
	end
	
	-- UNID's with the value of 0 count as non-existant
	for i, stage in pairs(self.stages) do
		-- Still supporting variables from the old Class_Seed for backwards compatibility
		if self.version == 1 then
			stage.requiredItemUNID = input.food
		end
		if stage.requiredItemUNID == 0 then
			stage.requiredItemUNID = nil
		end
	end
	
	-- Add the last stage
	local lastStage = {
		GLID			= input.finalGLID,
		requiredTime	= input.pickCooldown,
		isFinalStage	= true,
	}
	table.insert(self.stages, lastStage)

	if input.pickLoot then
		self.pickLootUNID = input.pickLoot
		self.pickLootStackSize = 1 -- default to be updated later
	end
	if input.pickLimit then
		self.pickLimit = input.pickLimit
	end
	if input.pickGLID then
		self.pickGLID = input.pickGLID
	end
	if input.requiredTool then
		self.requiredTool = input.requiredTool
	end
	if input.harvestGLID then
		self.harvestGLID = input.harvestGLID
	end

	
	--Toolbox.setUserdataAtIndex(self, "loot", input.loot)
	self["loot"] = input.loot
	
	
	if self.loot then
		local singles = self.loot.singles
		if singles then
			for i, v in pairs(singles) do
				v.drop		= v.drop or 100
				v.stackSize	= v.stackSize or GameDefinitions.DEFAULT_STACK_SIZE	--> I don't think we need this anymore...
			end
		else
			self.loot.singles = {}
		end
	else
		self.loot = {}
		self.loot.singles = {}
	end
	
	self.overridePickLoot = input.overridePickLoot == "true"
end

function initDeadline(self)
	LOG_FUNCTION_CALL("initDeadline")
	
	-- If we're on the first stage and the required item is not set 
	-- then we want the deadline to be set so that we can move on to
	-- the next stage.
	if self.deadlineToGrow == 0 then
		local currentStage = self:getCurrentStage()
		if not currentStage.requiredItemUNID and not currentStage.isFinalStage then
			self:resetDeadline()
		end
	end
end


function initItemProperties(self)
	LOG_FUNCTION_CALL("initItemProperties")
	
	-- Get the required item name for each stage
	for i, stage in pairs(self.stages) do
		if stage.requiredItemUNID then
			local uProperty = self.m_uItemProperties[stage.requiredItemUNID]
			if uProperty then
				stage.doesRequiredItemExist = true
				stage.requiredItemName = uProperty.name
			end
		end
	end
	
	self.pickLootExists = false
	local uLootProperties = self.m_uItemProperties[self.pickLootUNID]
	if uLootProperties then
		self.pickLootProperties = uLootProperties
		self.pickLootExists = true
		-- Get stack size if it exists
		if uLootProperties.stackSize then
			self.pickLootStackSize = uLootProperties.stackSize
		-- Use default stack size if none defined
		else
			-- Get default stack size based on item type
			if uLootProperties.itemType then
				local itemType = uLootProperties.itemType
				if itemType == 'weapon' or itemType == 'armor' or itemType == 'tool' then
					self.pickLootStackSize = 1
				else
					self.pickLootStackSize = GameDefinitions.DEFAULT_STACK_SIZE
				end
			-- If item type not defined, use default stack size
			else
				self.pickLootStackSize = GameDefinitions.DEFAULT_STACK_SIZE
			end
		end
	end
	
	self.harvestToolExists = false
	if self.requiredTool then
		local uProperty = self.m_uItemProperties[self.requiredTool]
		if uProperty then
			self.harvestToolExists = true
			self.requiredToolName = uProperty.name
		end
	end
	
	self.harvestLootExists = false
	-- Get harvest loot stack sizes
	if self.loot.singles then
		-- none = default = 100
		-- weapon, tool, armor = default 1
		-- loop through all loot

		for singlesIndex, singlesValue in pairs(self.loot.singles) do
			local uProperty = self.m_uItemProperties[singlesValue.UNID]
			if uProperty then
				self.harvestLootExists = true
				-- Get stack size if it exists
				if uProperty.stackSize then
					singlesValue.stackSize = uProperty.stackSize
				-- Use default stack size if none defined
				else
					-- Get default stack size based on item type
					if uProperty.itemType then
						local itemType = uProperty.itemType
						if itemType == 'weapon' or itemType == 'armor' or itemType == 'tool' then
							singlesValue.stackSize = 1
						else
							singlesValue.stackSize = GameDefinitions.DEFAULT_STACK_SIZE
						end
					-- If item type not defined, use default stack size
					else
						singlesValue.stackSize = GameDefinitions.DEFAULT_STACK_SIZE
					end
				end
			end
		end		
	end
end


function requestLoot(self)
	LOG_FUNCTION_CALL("requestLoot")
	
	local eventData = {
		returnPID			= self.PID,
		object				= self._dictionary,
	}
	Events.sendEvent("REQUEST_LOOT", eventData)
end


function deathHandler(self)
	LOG_FUNCTION_CALL("deathHandler")
	
	kgp.gameItemDeletePlacement(self.PID)
	Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = self.PID})
end

function handleGrowTimer(self)
	--LOG_FUNCTION_CALL("handleGrowTimer")
	
	local ETA = (self.deadlineToGrow - self.currentTime ) / MILISECONDS_PER_MINUTE

	if self.currentTime >= self.deadlineToGrow and self.deadlineToGrow > 0 then -- if deadline is passed and a deadline exists
		self:updateStage()
	end
end

function onPlayerFeed(self, playerName)
	LOG_FUNCTION_CALL("onPlayerFeed")
	
	self.requiredItemsSatisfied = self.requiredItemsSatisfied + 1
	
	local bRequiredItemsRetrieved = self:haveRequiredItemsBeenSatisfiedForCurrentStage() 
	
	if bRequiredItemsRetrieved then
		self.requiredItemsSatisfied = 0
		-- start grow timer
		self:resetDeadline()
	end
	
	self:updateLabels()
	self:saveSeedState("default")
	
	local currentStage = self:getCurrentStage()
	local info = {
		foodUNID = currentStage.requiredItemUNID, 
		foodName = currentStage.requiredItemName,
	}
	self:recordPlayerInteraction(playerName, "feed", info)

	-- Start tracking server ticks again to track seed growth
	if not self:isInFinalStage() then
		self:setObjectTimerEnabled(true) -- Register for timer event
	end
end

function generateLoot(self)
	LOG_FUNCTION_CALL("generateLoot")
	
	local items = self:getRandomLoot()
			
	-- NOTE: The items passed in to the BaseItemContainer must already have their properties set.
	-- We don't have time to wait for their properties to be retrieved from the Controller_Inventory.
	self:resetItemsTo(items)
end

function getRandomLoot(self)
	LOG_FUNCTION_CALL("getRandomLoot")
	local lootTable = {}
	
	if self.overridePickLoot and self.overridingLootItems then
		lootTable = self.overridingLootItems
	
		-- Request new random loot for the next time this seed gets picked
		self:requestLoot()
	else
		-- Generate your own random loot
		local STACK_SIZE = 10
		local lootCount = math.random(1, math.min(STACK_SIZE, self.pickLimit))

		local loot = {	
			UNID		= self.pickLootUNID, 
			count		= lootCount, 
			properties	= self.pickLootProperties
		}
		table.insert(lootTable, loot)
	end
	
	return lootTable
end

function hasLoot(self)
	LOG_FUNCTION_CALL("hasLoot")
	
	return not self:isContainerEmpty()
end

-- Checks whether we can go ahead with the generation of new loot
function checkForLootGenerationReboot(self)
	LOG_FUNCTION_CALL("checkForLootGenerationReboot")
	
	-- if this is the final stage
	if self:isInFinalStage() then
		-- We can only reboot once the seed is completely picked.
		-- local reboot = self:isContainerEmpty()
		
		-- NOTE: We no longer need the container to be empty in order to reboot it.
		local reboot = true
		if reboot then
			self:rebootSeedGeneration()
		end
	end
end

-- Restarts the cycle of seed generation for the seed that's been picked.
function rebootSeedGeneration(self)
	LOG_FUNCTION_CALL("rebootSeedGeneration")

	self.currentStageTimer = self:getCurrentStageTimer()
	
	self:resetDeadline()
	
	self.readyToPick = false

	--Start the timer
	self:setObjectTimerEnabled(true)
	
	self:updateClientOutline()
	
	self:updateLabels()
	self:saveSeedState("default")
end

function pickLoot(self, event)
	LOG_FUNCTION_CALL("pickLoot")
	
	if self.pickLootExists then
		local playerPosition = event.player.position
		local isInInteractionRange = self:getDistanceSqrdToPoint(playerPosition) <= MIN_INTERACTION_DISTANCE_SQRD
		
		if isInInteractionRange then
			-- If we have loot to show, then open the Loot Container Menu for the client
			if( self:hasLoot() ) then
				self:activateContainerForPlayer(event.player, false)
				
				-- Do some metric magic gathering
				self:recordPlayerInteraction(event.player.name, "pick")
			end
		end
	end
end

function resetDeadline(self)
	self.deadlineToGrow = self.currentTime + self.currentStageTimer -- deadline = current time + grow time
end


function updateStage(self)
	LOG_FUNCTION_CALL("growHandler")
	
	local currentStage = self:getCurrentStage()
	local prevGLID = currentStage and currentStage.GLID
	
	-- reset the deadline timer
	self.deadlineToGrow = 0

	-- if this is NOT the final stage
	if not self:isInFinalStage() then
		-- update currentStage
		self:advanceToNextStage()
		-- Growing finished, so stop tracking server tick
		self:setObjectTimerEnabled(false)
	end
	
	-- if this IS the final stage
	if self:isInFinalStage() then
		self.readyToPick = true -- Visually indicate that ready to pick?
		
		self:updateClientOutline();
		
		self:generateLoot()
	end

	self:saveSeedState("default")
	
	local currentStage = self:getCurrentStage()

	-- if the GLID changed, update it!
	local newGLID = self:getCurrentStageGLID()
	--log("newGLID: " .. tostring(newGLID) .. ", prevGLID: " .. tostring(prevGLID))
	if newGLID ~= prevGLID then
		Events.sendEvent("OBJECT_UPDATE_PLACEMENT_GLID", {PID = self.PID, GLID = newGLID})
	else -- otherwise, save the new stage data (happens as part of GLID updating process)
		self:updateLabels()
	end
end


function generateLootChest(self)
	LOG_FUNCTION_CALL("generateLootChest")

	local eventData = {
		object				= self._dictionary, 
		despawnTime			= 300, 
		canDropGemBox		= false, 
		spawnGLIDOverride	= self.harvestGLID,
	}
	Events.sendEvent("GENERATE_LOOT_CHEST", eventData)
end



function updateLabels(self)
	LOG_FUNCTION_CALL("updateLabels")
	
	local labels = {}
	local setHighlight = false
	local setLabel = false
	
	local currentStage = self:getCurrentStage()
	local isFinalStage = currentStage.isFinalStage

	-- Grow instructions labels
	if not isFinalStage and self.deadlineToGrow <= 0 then
		if currentStage.doesRequiredItemExist then
			local missingItemCount = currentStage.requiredQuantity - self.requiredItemsSatisfied
			self:updateTooltip("Requires " .. tostring(missingItemCount) .. " " .. tostring(currentStage.requiredItemName) .. " to advance. Use " .. self.requiredToolName .. " to destroy.")
			setLabel = true
			if not self.hasLabel then
				table.insert(labels, {"!", .05, .58, .11, .03})
				self.hasLabel = true
			end
		end
	-- Ready to pick/harvest labels
	elseif isFinalStage then
		if self.pickLootExists then
			if self.harvestToolExists and self.harvestLootExists then
				if self.readyToPick then			
					self:updateTooltip("Click to Open. Use " .. self.requiredToolName .. " to harvest.")
					
					--table.insert(labels, {"Ready to pick!", 0.013, 1, 1, 1})
					setHighlight = true
				else
					self:updateTooltip("Use " .. self.requiredToolName .. " to harvest. " .. self:getTimeRemainingString())
				end
			else
				if self.readyToPick then
					self:updateTooltip("Click to Open.")
					setHighlight = true
					--table.insert(labels, {"Ready to pick!", 0.013, 1, 1, 1})
				end
			end
		else
			if self.harvestToolExists and self.harvestLootExists then
				self:updateTooltip("Use " .. self.requiredToolName .. " to harvest.")
			else
				self:updateTooltip("At final stage.")
			end
		end
	-- Growth labels
	elseif self.deadlineToGrow > 0 then
		--table.insert(labels, {"Growing...", 0.013, 1, 1, 1})
		self:updateTooltip(self:getTimeRemainingString())
	end

	if setLabel  then
		if ( next(labels) ) then
			self:addLabels(labels)
		end
	else
		self:clearLabels()
		if self.hasLabel then
			self.hasLabel = nil
		end
	end

	self:setClientHighlight(setHighlight)
end


function getTimeRemainingString(self)
	LOG_FUNCTION_CALL("getTimeRemainingString")
	
	local message = ""
	local timeRemaining = (self.deadlineToGrow - self.currentTime) / 1000
	local days = math.floor(timeRemaining / SECONDS_PER_DAY)
	local hours = math.floor(timeRemaining / SECONDS_PER_HOUR)
	local minutes = math.floor(timeRemaining / SECONDS_PER_MINUTE)
	local seconds = math.floor(timeRemaining % SECONDS_PER_MINUTE)
	-- -- tracking seconds determined to be too resource intensive
	if timeRemaining < SECONDS_PER_MINUTE then
	-- message = message..tostring(seconds).." s"
	-- elseif timeRemaining >= SECONDS_PER_MINUTE and timeRemaining < SECONDS_PER_HOUR then
	-- if timeRemaining < 0 then
		message = message.."< 1 min"
	elseif timeRemaining >= SECONDS_PER_MINUTE and timeRemaining < SECONDS_PER_HOUR then
		message = message..tostring(minutes).." min"
	elseif timeRemaining >= SECONDS_PER_HOUR and timeRemaining < SECONDS_PER_DAY then
		minutes = math.floor((timeRemaining % SECONDS_PER_HOUR) / SECONDS_PER_MINUTE)
		message = message..tostring(hours).." hrs "..tostring(minutes).." min"
	elseif timeRemaining >= SECONDS_PER_DAY then
		hours = math.floor((timeRemaining % SECONDS_PER_DAY) / SECONDS_PER_HOUR)
		message = message..tostring(days).." days "..tostring(hours).." hrs"
	end
	if not self:isInFinalStage() then
		message = message.." until next stage."
	else
		message = message.." until ready to pick."
	end
	return message
end


----------------------------------------
-- Class Methods (Stage Management)
----------------------------------------


function haveRequiredItemsBeenSatisfiedForCurrentStage(self)
	local currentStage = self:getCurrentStage()
	
	if not currentStage.isFinalStage then		
		return self.requiredItemsSatisfied >= currentStage.requiredQuantity
	end
	
	return true
end


function getCurrentStageRequiredItemName(self)
	LOG_FUNCTION_CALL("getCurrentStageRequiredItemName")

	local currentStage = self:getCurrentStage()
	if currentStage then
		return currentStage.requiredItemName
	end
	
	return ""
end

function getCurrentStageTimer(self)
	LOG_FUNCTION_CALL("getCurrentStageTimer")

	local currentStage = self:getCurrentStage()
	if currentStage then
		return currentStage.requiredTime * MILISECONDS_PER_MINUTE
	end
	
	return 0
end

function getCurrentStageGLID(self)
	LOG_FUNCTION_CALL("getCurrentStageGLID")
	
	local currentStage = self:getCurrentStage()
	
	if( currentStage ) then
		return currentStage.GLID
	end
	
	return 0
end

function getFirstStage(self)
	return self.stages and self.stages[1]
end

function getCurrentStage(self)
	local currentStage = tonumber(self.currentStageIndex)
	if currentStage then
		if self.stages then
			return self.stages[currentStage + 1]
		else
			log("ERROR: stages are not set in Class_Seed")
		end
	else
		log("ERROR: Invalid stage encountered in Class_Seed, self.currentStageIndex: " .. tostring(self.currentStageIndex))
	end
end

function getFinalStage(self)
	local stageCount = tonumber(self.stagesCount)
	if stageCount and self.stages then
		return self.stages[stageCount - 1]
	end
end	

function isInFinalStage(self)
	local currentStage = self:getCurrentStage()
	return currentStage and currentStage.isFinalStage
end

function advanceToNextStage(self)
	local currentStageIndex = tonumber(self.currentStageIndex)
	local stageCount = tonumber(self.stagesCount)
	if currentStageIndex and currentStageIndex < stageCount - 1 then
		self.currentStageIndex = self.currentStageIndex + 1
		
		-- get the new timer
		self.currentStageTimer = self:getCurrentStageTimer()
		
		return true
	end
	
	return false
end


----------------------------------------
-- Class Methods (Client Highlight and Outline settings)
----------------------------------------

function updateClientOutline(self)
	LOG_FUNCTION_CALL("updateClientOutline")
	
	local shouldHighlightBeOn = self.readyToPick == true
	
	self:enableClientOutline(shouldHighlightBeOn)
end

function enableClientOutline(self, enable)
	LOG_FUNCTION_CALL("enableClientOutline")
	
	local isHighlightOn = self.proxTrigger ~= nil

	if enable then
		if not isHighlightOn then
			-- Turn the outline glow on
			self.proxTrigger = Events.setClientTrigger(self.interactionRange, self)
		end
	else
		if isHighlightOn then
			-- Turn the outline glow off
			Events.removeClientTrigger(self.proxTrigger)
			self.proxTrigger = nil
		end
	end
end 

-- Highlight the object on the client
function setClientHighlight(self, enable)
	LOG_FUNCTION_CALL("setClientHighlight")
	
	-- Get the owner and send it the lootable object PID to highlight
	if enable ~= self.highlighted then
		Events.sendClientEvent("FRAMEWORK_UPDATE_TIMED_STATE", {PID = self.PID, enable = enable})
		self.highlighted = enable
	end
end


----------------------------------------
-- Class Methods (Database Interactions)
----------------------------------------

function saveSeedState(self, saveString)
	LOG_FUNCTION_CALL("saveSeedState")
	
	local seedData = {}
	seedData.currentStage			= self.currentStageIndex
	seedData.readyToPick			= self.readyToPick
	seedData.deadlineToGrow			= self.deadlineToGrow
	seedData.requiredItemsSatisfied = self.requiredItemsSatisfied
	
	local seedDataString = Events.encode(seedData)
	
	local saveLoc = ""
	if saveString == "default" then
		saveLoc = SEED_KEY .. "_" .. tostring(self.PID), seedDataString
	else
		saveLoc = saveString
	end
	SaveLoad.saveGameData(saveLoc, seedDataString)
end


function loadSeedState(self, saveString)
	LOG_FUNCTION_CALL("loadSeedState")
	
	self.currentTime = (kgp.gameGetCurrentTime() * 1000) or 0 -- absolute time in milliseconds
	
	local saveLoc = ""
	if saveString == "default" then
		saveLoc = SEED_KEY .. "_" .. tostring(self.PID)
	else
		saveLoc = saveString
	end
	local seedDataString = SaveLoad.loadGameData(saveLoc)
	local seedData = {}
	if seedDataString then
		seedData = Events.decode(seedDataString)
	end
	self.currentStageIndex		= seedData.currentStage or 0
	self.readyToPick			= seedData.readyToPick or false
	self.requiredItemsSatisfied = seedData.requiredItemsSatisfied or 0
	self.deadlineToGrow			= seedData.deadlineToGrow or 0
	if self.deadlineToGrow == 0 and not self:isInFinalStage() then
		-- Loaded a seed that is done growing, so don't track server tick
		self:setObjectTimerEnabled(false)
	end
end

function deleteSeedState(self, saveString)
	LOG_FUNCTION_CALL("deleteSeedState")
	
	if saveString == "default" then
		saveLoc = SEED_KEY .. "_" .. tostring(self.PID)
	else
		saveLoc = saveString
	end
	
	SaveLoad.deleteGameData(saveLoc)
end

function setObjectTimerEnabled(self, enabled)
	if self.timerEnabled ~= enabled then
		if enabled then
			Events.registerHandler("TIMER", self.onTimer, self)
		else
			Events.unregisterHandler("TIMER", self.onTimer, self)
		end
		self.timerEnabled = enabled
	end
end


----------------------------------------
-- local functions
----------------------------------------

