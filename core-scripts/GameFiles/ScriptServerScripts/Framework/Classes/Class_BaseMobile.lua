--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Class_Mobile.lua
--
----------------------------------------


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Console		= _G.Console
local Class			= _G.Class

-----------------------------------------
-- Class specification
-----------------------------------------
Class.createClass("Class_BaseMobile", "Class_Base")

-- use behaviors
Class.useBehaviors({
	"Behavior_Movement",
})

-- public properties
Class.createProperties({
	"destination",
	"inMotion",
	"chaseTarget",
	"fleeTarget",
	"timeToDestination",
	"home",
	"speed",
	"lookAt"
})




----------------------------------------
-- Constants
----------------------------------------

local DEFAULT_SPEED = 3 -- Default speed (Kaneva units per second)


----------------------------------------
-- Public class variables
----------------------------------------
s_type = "Mobile"

-- Instantiate an instance of the Base class
function create(self, input)
	-- TODO: Error check input values
	if input == nil then input = {} end

	_super.create(self, input)

	-- The position needs to have been set in the Base class.
	self.home				= self.position
	
	self.destination		= self.position
	
	self.speed				= input.speed or DEFAULT_SPEED
	
	self.timeToDestination	= 0 -- How long will it take to reach object's desired destination?
	self.inMotion			= false -- Is this object currently moving?
	self.chaseTarget		= 0 -- PID/playerName this object is chasing
	self.fleeTarget			= 0 -- PID/playerName this object is fleeing
	self.lookAt				= false

	-- Setup Behaviors
	Behavior_Movement.setup(self)
end


function destroy(self)
	Behavior_Movement.cleanup(self)
	
	_super.destroy(self)
end

----------------------------------------
-- Behavior handlers
----------------------------------------

-- Called when a movement event has been completed
function onMovementArrived(self, event)
	-- Intentionally blank
end


----------------------------------------------
-- Behavior Wrappers
----------------------------------------------

-- Behavior_Movement

-- {x, y, z, buffer}
function moveTo(self, input)
	-- TODO: Error check inputs
	if self then
		Behavior_Movement.moveTo(self, input)
	else
		-- warn
	end
end

-- {ry, speed}
function rotateBy(self, input)
	-- TODO: Error check inputs
	if self then
		Behavior_Movement.rotateBy(self, input)
	else
		-- warn
	end
end

-- {PID / playerName, buffer}
function chase(self, input)
	if input.target then
		--input.targetSD = tempTarget
		
		Behavior_Movement.chase(self, input)
		return true
	else
		-- Return failed
		return false
	end
end

-- {PID / playerName}
function flee(self, input)
	if input.target then
		input.targetSD = tempTarget
		
		Behavior_Movement.flee(self, input)
		return true
	else
		-- Return failed
		return false
	end
end

function stopMovement(self)
	-- TODO: Error check input
	Behavior_Movement.stop(self)
end

function pauseMovement(self)
	-- TODO: Error check input
	Behavior_Movement.pause(self)
end

function resumeMovement(self)
	-- TODO: Error check input
	Behavior_Movement.play(self)
end

-- Teleports an object
function teleport(self, input)
	if tonumber(input.x)  == nil then Console.Write(Console.DEBUG, "teleport() - [x] must be a number. Got "..tostring(input.x));   return end
	if tonumber(input.y)  == nil then Console.Write(Console.DEBUG, "teleport() - [y] must be a number. Got "..tostring(input.y));   return end
	if tonumber(input.z)  == nil then Console.Write(Console.DEBUG, "teleport() - [z] must be a number. Got "..tostring(input.z));   return end
	if input.rx then
		if tonumber(input.rx) == nil then Console.Write(Console.DEBUG, "teleport() - [rx] must be a number. Got "..tostring(input.rx)); return end
	end
	if input.ry then
		if tonumber(input.ry) == nil then Console.Write(Console.DEBUG, "teleport() - [ry] must be a number. Got "..tostring(input.ry)); return end
	end
	if input.rz then
		if tonumber(input.rz) == nil then Console.Write(Console.DEBUG, "teleport() - [rz] must be a number. Got "..tostring(input.rz)); return end
	end
	
	Behavior_Movement.teleport(self, input)
end
