--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- use battle lib
include("Lib_SaveLoad.lua")
include("Lib_GameDefinitions.lua")



-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Debug				= _G.Debug -- Bind to a local variable for efficiency's sake
local GameDefinitions	= _G.GameDefinitions
local SaveLoad			= _G.SaveLoad
local Events			= _G.Events
local Class				= _G.Class
local Players			= _G.Players

local LOG_FUNCTION_CALL, LOG_FUNCTION_CALL_FILTER = Debug.generateCustomLog("Class_BaseItemContainer - ")
Debug.enableLogForFilter(LOG_FUNCTION_CALL_FILTER, false) -- Set this to true if you want to get a complete log of Class_BaseItemContainer function calls


-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------

Class.import("Inventory")

Class.createProperties({
	"interactionRange"
})

-----------------------------------------
-- Constants
-----------------------------------------
local DEFAULT_CONTAINER_SIZE = 20

local INTERACTION_RANGE = 20
local DEFAULT_LOCKED = false

local INVENTORY_KEY = "chestInventory"
local PIN_SAVE_KEY = "DNC_pinCode"
local PIN_ACCESS_MEM_KEY = "DNC_pinCodeMemory"

-----------------------------------------
-- Functions
-----------------------------------------
local isItemEmpty -- (item)
local getContainerInfoFromItems -- (items)

function create(self, input)
	LOG_FUNCTION_CALL("create")
	
	if input == nil then input = {} end
	
	self.behavior = input.behavior
	self.isRider = false
	if self.behavior then
		if string.sub(self.behavior, -6, -1) == "_rider" then
			self.isRider = true
		end
	end
	
	_super.create(self, input)
	
	self.interactionRange = self.interactionRange or INTERACTION_RANGE
	self.players = {}
	self.spawnerPID = input.spawnerPID
	
	self.canDropGemBox = input.canDropGemBox or false
	self.gemRolledFor = false

	local contents = input.items -- Save input to a local var to avoid manipulating the pointer data
	
	if input.spawnerPID == nil and contents == nil then
		-- No input, load from Database...
		if input.pickLoot then
			contents  = self:generateRandomLoot(input.pickLoot, input.pickLimit)
		else
			contents = self:loadContainerContentsFromDB()
		end
	end

	self:resetItemsTo(contents, true)
	
	self.trigger = Events.setTrigger(self.interactionRange, self.onTrigger, self)

	self.autoLoot = input.autoLoot or false

	self:setupTooltips()

	Events.registerHandler("UPDATE_CONTAINER_ITEM_SLOTS", self.onContainerSlotsUpdated, self)
	Events.registerHandler("ITEM_CONTAINER_CLOSED", self.onItemContainerClosed, self)
	Events.registerHandler("ITEM_PROPERTIES_UPDATE", self.onItemPropertiesUpdated, self)
	Events.registerHandler("OBJECT_DIED", self.onObjectDied, self)
	Events.registerHandler("OBJECT_PICKUP", self.onObjectPickup, self)
	Events.registerHandler("FRAMEWORK_GEM_CHEST_ROLL_RESULT", self.onGemChestRollResultReturned, self)
end

function destroy(self)
	LOG_FUNCTION_CALL("destroy")
	
	Events.unregisterHandler("UPDATE_CONTAINER_ITEM_SLOTS", self.onContainerSlotsUpdated, self)
	Events.unregisterHandler("ITEM_CONTAINER_CLOSED", self.onItemContainerClosed, self)
	Events.unregisterHandler("ITEM_PROPERTIES_UPDATE", self.onItemPropertiesUpdated, self)
	Events.unregisterHandler("OBJECT_DIED", self.onObjectDied, self)
	Events.unregisterHandler("OBJECT_PICKUP", self.onObjectPickup, self)
	Events.unregisterHandler("FRAMEWORK_GEM_CHEST_ROLL_RESULT", self.onGemChestRollResultReturned, self)

	if self.trigger then
		Events.removeTrigger(self.trigger)
	end
	
	-- Erase database content for this item if being removed from the world
	if( self.isBeingRemoved ) then
		-- Item is being removed from the world as opposed to the world getting shut down
		self:deleteContainerContentsFromDB()
	end

	_super.destroy(self)
end

-- Called when an object gets picked up
function onObjectPickup(self, event)
	LOG_FUNCTION_CALL("onObjectPickup")
	
	-- Did I just get picked up?
	if (event.PID == self.PID) then
		if self.spawnerPID == nil then
			self:deleteContainerContentsFromDB()
		end
	end
end

--------------------
-- Event handlers --
--------------------

function onTrigger(self, event)
	LOG_FUNCTION_CALL("onTrigger")
	
	local player = event.player

	if player == nil then
		return
	end

	-- If player exits trigger radius and actor is active, close dialog
	if event.triggerType == 2 and player.mode ~= "God" then
		Events.sendClientEvent("FRAMEWORK_CLOSE_ITEM_CONTAINER", {}, player.ID)
	end
end

function onContainerSlotsUpdated(self, event)
	LOG_FUNCTION_CALL("onContainerSlotsUpdated")
	
	if 	event == nil or 
		event.updatedSlots == nil or 
		event.PID ~= self.PID then 
		return 
	end
	
	for i, slot in ipairs(event.updatedSlots) do
		self.itemsList[slot.index] = slot.item
	end
	
	if self.spawnerPID == nil then
		self:saveContainerContentsToDB()
	end
end




-- When item properties have been retrieved from Controller_Inventory
function setupTooltips(self)
	LOG_FUNCTION_CALL("setupTooltips")
	if self.autoLoot then
		for i, item in pairs(self.itemsList) do
			local uProperties = self.m_uItemProperties[item.UNID]
			if uProperties and uProperties.name then
				self:updateTooltip("Click to take ".. uProperties.name)
			else
				self:updateTooltip("Click to Take")
			end
		end
	else
		self:updateTooltip("Click to Open")
	end
end



-- Called when a Loot Container menu is closed on one of the clients
function onItemContainerClosed(self, event)
	LOG_FUNCTION_CALL("onItemContainerClosed")
	if event.playerName then
		if event.PID == self.PID  or event.PID == nil then  --in case of a quick close before PID is established, remove user interaction from all
			self.players[event.playerName] = nil
			Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = self.PID, interacting = false})
		end
	end
end

function onItemPropertiesUpdated(self, event)
-- 	LOG_FUNCTION_CALL("onItemPropertiesUpdated")
	
 end

-- Called when an object dies
function onObjectDied(self, event)
	LOG_FUNCTION_CALL("onObjectDied")
	
	-- Did I just die?
	if (event.object.PID == self.PID) then
		if self.spawnerPID == nil then
			self:deleteContainerContentsFromDB()
		end
	end
end

-- A gem's drop result has been returned. Loot chest now
function onGemChestRollResultReturned(self, event)
	LOG_FUNCTION_CALL("onGemChestRollResultReturned")
	
	if event.PID ~= self.PID then return end -- Event is not for me
	self.gemRolledFor = true -- We'll never roll again for a gem for this chest
	
	if event.giveGem and event.gemInfo then
		local gemSlot
		-- Replace first empty slot with a gem chest
		for i, v in pairs(self.itemsList) do
			if v.UNID == 0 then
				gemSlot = i
				break
			end
		end
		if gemSlot then
			self.itemsList[gemSlot] = event.gemInfo
		end
	end
	
	if self.activated and self.autoLoot then
		local compactItemList = self:getCompactItemsList()
		Events.sendClientEvent("FRAMEWORK_ADD_SINGLE_LOOT", {spawnerPID = self.spawnerPID, PID = self.PID, itemList = compactItemList}, event.player.ID)
	else
		self:activateContainerForPlayer(event.player)
	end
end

---------------------
-- Class functions --
---------------------

function resetItemsTo(self, items, getPropertiesForItems)
	LOG_FUNCTION_CALL("resetItemsTo, PID: " .. tostring(self.PID))

	self.itemsList, self.containerSize = getContainerInfoFromItems(items)
	
	if( getPropertiesForItems ) then
		-- Request more info for these items from Game Items
	else
		-- If we don't need the properties, then save the changes to the DB
		self:saveContainerContentsToDB()
	end
end

function isPlayerRegistered(self, player)
	return player and self.players[player.name]
end


-- Placeholder function to be overriden by subclasses
function canPlayerOpenContainer(self, player, preValidated)
	return true
end

function resetItemAtIndex(self, index)
	self.itemsList[index] = GameDefinitions.EMPTY_ITEM
end

function isItemAtIndexEmpty(self, index)
	local item = self.itemsList[index]
	
	return isItemEmpty(item)
end

-- Activate this item
function activateContainerForPlayer(self, player, preValidated)
	LOG_FUNCTION_CALL("activateContainerForPlayer")
	
	if not Players.validatePlayer(player) then
		return
	end
	
	-- Player's can't activate item containers
	if player and player.mode ~= "God" then
		local pinCode = self:getPinCode()
		if player.inVehicle then
			kgp.playerShowStatusInfo(player.ID, "You cannot do that while in a vehicle.", 3, 255, 0, 0)
		elseif player.alive == nil or player.alive == false then
			kgp.playerShowStatusInfo(player.ID, "You cannot do that while dead.", 3, 255, 0, 0)
		elseif not self:isPointWithinRange(player.position, self.interactionRange) then
			kgp.playerShowStatusInfo(player.ID, "You must be closer to interact with that object.", 3, 255, 0, 0)
		elseif self:isContainerBusy() then
			kgp.playerShowStatusInfo(player.ID, "Container is busy.", 3, 255, 0, 0)
			log("ContainerIsBusyEvent: Current users on chest: ")
		elseif self.autoLoot == true or self.ctrlClick or self.activated then
		 	self:onAutoLootActivated(player)
		elseif self:canPlayerOpenContainer(player, preValidated) then
			if( not self:isPlayerRegistered(player) ) then
		        -- Close any Loot Container Menus that might be already open on the client.
		        Events.sendClientEvent("FRAMEWORK_CLOSE_ITEM_CONTAINER", {}, player.ID)
	        end
			self:openContainerForPlayer(player)
		elseif pinCode == nil then
			kgp.playerShowStatusInfo(player.ID, "You do not have access to this safe.", 3, 255, 0, 0)
		end
	end
end

function openContainerForPlayer(self, player)
	LOG_FUNCTION_CALL("openContainerForPlayer")
	
	if self.canDropGemBox and not self.gemRolledFor then
		-- Fire event to Controller_Inventory to roll for a gem chest
		Events.sendEvent("FRAMEWORK_ROLL_FOR_GEM_CHEST", {player = player, PID = self.PID}) -- We'll open the item container after this returns through FRAMEWORK_GEM_CHEST_ROLL_RESULT
	else
		-- Open Framework_ItemContainer menu and fill it with the loot
		kgp.playerLoadMenu(player.ID, "Framework_ItemContainer.xml")
		local compactItemList = self:getCompactItemsList()
		
		Events.sendClientEvent("UPDATE_ITEM_CONTAINER", {updatedItems = compactItemList, PID = self.PID, containerSize = self.containerSize}, player.ID)
		self.players[player.name] = player.ID
		if self.isRider then
			Events.sendEvent("PLAYER_RIDER_INTERACTION", {riderPID = self.PID, interacting = true})
		end
	end
end

function isContainerEmpty(self)
	local containerEmpty = true
	for _, item in pairs(self.itemsList) do
		if not isItemEmpty(item) then
			containerEmpty = false
			break
		end
	end
	
	return containerEmpty
end

function isContainerBusy(self)
	LOG_FUNCTION_CALL("isContainerBusy")
	
	return next(self.players)
end

-- Save contents to DB
function saveContainerContentsToDB(self, PID)
	LOG_FUNCTION_CALL("saveContainerContentsToDB, PID: " .. tostring(PID))
	
	PID = PID or self.PID
	
	if self.itemsList == nil then return end
	local saveTable = {}

	for index, item in pairs(self.itemsList) do
		local index = tonumber(index)
		local newEntry = {}
		newEntry.UNID = item.UNID
		newEntry.count = item.count
		
		if item.instanceData then
			newEntry.instanceData = {}
			for i, v in pairs(item.instanceData) do
				newEntry.instanceData[i] = v
			end
		end
		
		saveTable[index] = newEntry
	end
	
	SaveLoad.saveGameData(INVENTORY_KEY.."_" .. tostring(PID), Events.encode(saveTable))
end

-- Load contents from DB
function loadContainerContentsFromDB(self, PID)
	LOG_FUNCTION_CALL("loadContainerContentsFromDB")
	
	PID = PID or self.PID
	
	local gameData = SaveLoad.loadGameData(INVENTORY_KEY.."_" .. tostring(PID))
	
	if gameData ~= nil and gameData ~= "[]" then
		return Events.decode(gameData)
	end
end

-- Remove any saved data from DB
function deleteContainerContentsFromDB(self, PID)
	LOG_FUNCTION_CALL("deleteContainerContentsFromDB")

	PID = PID or self.PID
	
	SaveLoad.deleteGameData(INVENTORY_KEY .. "_" .. tostring(PID))
end

function onAutoLootActivated(self)
	return true
end

function generateRandomLoot(self, UNID, count)
	local itemTable = {}
	local item = {}
	item.UNID = UNID

	item.count = math.random(1, count)
	table.insert(itemTable, item)
	return itemTable
end

function getInstanceDataForItemProperty(itemProperties, randomizeDurability)
	if	itemProperties.itemType == GameDefinitions.ITEM_TYPES.WEAPON or 
		itemProperties.itemType == GameDefinitions.ITEM_TYPES.ARMOR or 
		itemProperties.itemType == GameDefinitions.ITEM_TYPES.TOOL then
		
		local itemDurability = GameDefinitions.DEFAULT_MAX_DURABILITY
		local durabilityType = itemProperties.durabilityType or "repairable"
		if durabilityType == "limited" then
			itemDurability = GameDefinitions.DURABILITY_BY_LEVEL[itemProperties.level]
		elseif durabilityType == "indestructible" then
			itemDurability = 100
		elseif randomizeDurability then
			itemDurability = GameDefinitions.getRandomItemDurability()
		end

		return	{	
			levelMultiplier	= GameDefinitions.getItemLevelMultiplier(),
			durability		= itemDurability
		}
	end
end

function printContainerContents(self)
	LOG_FUNCTION_CALL("printContainerContents - PID: " .. tostring(self.PID))
	
	local itemCount = 0
	for _, item in pairs(self.itemsList) do
		if not isItemEmpty(item) then
			itemCount = itemCount + 1
		end
	end
	
	log("PID: " .. tostring(self.PID) .. ", Item count: " .. tostring(itemCount))
	
	for i, item in pairs(self.itemsList) do
		if not isItemEmpty(item) then
			Debug.printTable("Item(" .. tostring(i) .. ")", item)
		end
	end
end

-- Used for posting the items list to the client
function getCompactItemsList(self)
	local compactList = {}
	
	for i, item in pairs(self.itemsList) do
		if not isItemEmpty(item) then
			-- Make sure the index is a string, otherwise the posting system complains when there is a gap in between two consecutive items.
			compactList[tostring(i)] = {
				UNID = item.UNID,
				count = item.count,
				instanceData = item.instanceData
			}
		end
	end
	
	return compactList
end

function getPinCode(self)
	local PID = self.PID
	local saveLoc = PIN_SAVE_KEY.."_"..tostring(PID)
	local pinDataString = SaveLoad.loadGameData(saveLoc)

	local pinData = {}
	if pinDataString then
		pinData = Events.decode(pinDataString)
	end
	local tempPinCode = pinData.pinCode or nil

	return tempPinCode
end

---------------------
-- Local functions --
---------------------


isItemEmpty = function(item)
	return (not item) or (item.UNID == 0) or (item.count == 0)
end

getContainerInfoFromItems = function(items)
	LOG_FUNCTION_CALL("getContainerInfoFromItems")
	
	-- Calculate container size
	local containerSize = DEFAULT_CONTAINER_SIZE
	if items then
		containerSize = math.ceil(#items / DEFAULT_CONTAINER_SIZE) * DEFAULT_CONTAINER_SIZE
	
		containerSize = containerSize > 0 and containerSize or DEFAULT_CONTAINER_SIZE
	end
	
	-- Fill out the container
	local container = {}
	for i = 1, containerSize do
		table.insert(container, GameDefinitions.EMPTY_ITEM)
	end
	
	if items then
		for i, item in ipairs(items) do
			-- Check for invalid container entry
			if not isItemEmpty(item) then
				-- Extract what we need to finish populating this item
				container[i] = item
			end
		end
	end
	
	return container, containerSize
end