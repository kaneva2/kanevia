--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- Class_ShopItem.lua
-- Class for all items that will appear in a shop, inherits Class_Item
----------------------------------------


-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Events			= _G.Events
local Class				= _G.Class

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------

Class.import("Battle")

Class.createClass("Class_ShopItem", "Class_Item")

Class.createProperties({
	"itemCost",
	"itemIcon",
	"itemEffects"
})

-----------------------------------------------------------------
-- Constant Definitions
-----------------------------------------------------------------
local TYPE_COINS = "coins"
local TYPE_XP = "xp"
local TYPE_HEALTH = "health"

local ITEM_TYPE_CONSUMABLE = "consumable"
local ITEM_TYPE_DRINK = "drink"

local DEFAULT_COST = 0
local DEFAULT_TYPE = ITEM_TYPE_CONSUMABLE --consumable, drink
local DEFAULT_CURRENCY = "coins"

local DEFAULT_ICON = "filestore11/6348325/7610764/OrangeJuice.jpg"

local DEFAULT_PURCHASE_SOUND = 4344041
local DEFAULT_PURCHASE_PARTICLE = 4202321

local DEFAULT_DRINK_ANIM_M = 3981067
local DEFAULT_DRINK_ANIM_F = 3956621
local DEFAULT_DRINK_ACCESSORY = 4373828

-----------------------------------------------------------------
-- Local variables
-----------------------------------------------------------------
local s_drinkers = {} --table of current drinkers, to remove drink accessories

function create(self, input)
	_super.create(self, input)
	
	if input == nil then input = {} end
	
	self.itemName = input.itemName or "Shop Item"
	
	self.itemType = input.itemType or DEFAULT_TYPE
	self.itemCost = input.itemCost or DEFAULT_COST
	self.itemCurrency = input.itemCurrency or DEFAULT_CURRENCY
	
	self.itemIcon = input.itemIcon or DEFAULT_ICON
	
	self.itemEffects = input.itemEffects or {}
	
	self.purchaseParticle = input.purchaseParticle or DEFAULT_PURCHASE_PARTICLE
	self.purchaseSound = input.purchaseSound or DEFAULT_PURCHASE_SOUND
	
	if self.itemType == ITEM_TYPE_DRINK then
		self.drinkAnimM = input.drinkAnimM or DEFAULT_DRINK_ANIM_M
		self.drinkAnimF = input.drinkAnimF or DEFAULT_DRINK_ANIM_F
		self.drinkAccessory = input.drinkAccessory or DEFAULT_DRINK_ACCESSORY
	end
	
	-- Register players that leave the zone
	Events.registerHandler("PLAYER-DEPART", self.onDepart, self)
end

function destroy(self)
	_super.destroy(self)
	--Clear player triggers
	for k, v in pairs(s_drinkers) do
		kgp.playerUnEquipItem(v, self.drinkAccessory)
		Events.removeTrigger(k)
	end
end

-- Depart handler
function onDepart(self, event)
	self.onPlayerDepart(self, event)
end

-- Wrapper for onDepart
function onPlayerDepart(self, event)
	if event.player then
		--Clear player triggers
		for k, v in pairs(s_drinkers) do
			if v == event.player.ID then 
				Events.removeTrigger(k)
				v = nil
			end
		end
	end
end

function onTrigger(self, event)
	if event.triggerType == 2 and event.player then
		if s_drinkers[event.param] and s_drinkers[event.param] == event.player.ID then
			--TODO: Should this kgp call be wrapped to base?
			kgp.playerUnEquipItem(event.player.ID, self.drinkAccessory)
		end
		
		--Clear player triggers
		for k, v in pairs(s_drinkers) do
			if v == event.player.ID then 
				Events.removeTrigger(k)
				v = nil
			end
		end
	end
end

function onPurchase(self, player)
	--Console.Write(Console.INFO, "onPurchase called for "..tostring(self.itemName))
	if self.itemCost <= 0 or (player[self.itemCurrency] and player[self.itemCurrency] >= self.itemCost) then
		if self.itemCurrency == TYPE_COINS then
			playerRemoveCoins({user = player, coins = self.itemCost})
		else
			player[self.itemCurrency] = (player[self.itemCurrency] or 0) - self.itemCost --deduct the item cost
		end
		
		for key, val in pairs(self.itemEffects) do
			if key == TYPE_COINS then
				if val >= 0 then
					playerAddCoins({user = player, coins = val})
				else
					playerRemoveCoins({user = player, coins = -val})
				end
			elseif key == TYPE_HEALTH then
				if val >= 0 then
					playerAddHealth({user = player, health = val})
				else
					playerRemoveHealth({user = player, health = -val})
				end
			elseif key == TYPE_XP then
				playerAddXP({user = player, XP = val})
			else
				player[key] = (player[key] or 0) + val
			end
		end		
		
		if self.purchaseParticle and self.purchaseParticle > 0 then
			playerSetParticle({user = player, particleGLID = self.purchaseParticle, particleKey = "purchaseParticle", playTime = 1})
		end
		
		if self.purchaseSound and self.purchaseSound > 0 then
			playerPlaySound({user = player, soundGLID = self.purchaseSound})
		end
		
		if self.itemType == ITEM_TYPE_DRINK then
			--TODO: Wrap kgp call to base?
			kgp.playerEquipItem(player.ID, self.drinkAccessory)
			if player.gender == "M" then
				playerSetAnimation({user = player, animation = self.drinkAnimM})
			else
				playerSetAnimation({user = player, animation = self.drinkAnimF})
			end
			
			--TODO: Getting the actual current player position here instead of pulling from shared data, as shared data is only updated on tick and it is vitally important to place the trigger at the actual position
			local playerPos = {}
			playerPos.x, playerPos.y, playerPos.z, _, _, _ = kgp.playerGetLocation(player.ID)
			local trigger = Events.setTriggerOnPosition(playerPos.x, playerPos.y, playerPos.z, 0.5, self.onTrigger, self)
			s_drinkers[trigger] = player.ID
		end
		
		return true
	else
		return false
	end
end
