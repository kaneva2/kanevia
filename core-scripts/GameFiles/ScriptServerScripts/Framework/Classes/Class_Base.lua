--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

include("Lib_Vecmath.lua")
include("Lib_Colors.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Console			= _G.Console
local Types				= _G.Types
local Events			= _G.Events
local Vecmath			= _G.Vecmath
local Colors			= _G.Colors
local Class				= _G.Class
local Players			= _G.Players

----------------------------------------
--
-- Class_Base.lua
--
-- Super class for all objects
--
-- Base variables. All unified behaviors should
-- use these as the basis of their functionality
-- STRING
-- name, image URL
-- NUMBER
-- health, PID, level, XP
-- TABLE
-- position(x,y,z,rx,ry,rz)
--
-- User-accessible class methods
-- show(self)
-- hide(self)
-- registerWithController(self)
--
----------------------------------------



-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
Class.import("Game")
Class.import("Battle")

Class.createClass("Class_Base")

-- use behaviors
Class.useBehaviors({
	"Behavior_Sound",
})

-- public properties
Class.createProperties({
	"UNID",
	"PID",
	"type",
	"level",
	"health",
	"name",
	"position",
	"targetable",
	"alive",
	"interactionRange",
	"spawnerPID",		-- Spawned objects have this property
	"loot",
	"owner",
	"tooltip",
	"creatorOnly", -- Hide or show object for players,
	"ownerOnly",   -- Will only show for player who owns object
	"modeLabels" -- Table of what labels this item displays for players by mode ex: {"God":"Banana","Player":"Poopeedoop"}
})


-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local DEFAULT_NAME 	      = "Base" -- Default name
local DEFAULT_LEVEL		  = 1
local DEFAULT_HEALTH      = 100 -- Default health
local DEFAULT_POSITION    = {
	x  = 0,
	y  = 0,
	z  = 0,
	rx = 0,
	ry = 0,
	rz = 1
} -- Default position

local DEFAULT_PID 	      = 0 -- Default PID
local DEFAULT_IMAGEURL    = "http://images.kaneva.com/filestore11/5509627/7500697/wat.jpg" -- Default image URL TODO: CHANGE
local DEFAULT_CREATOR_ONLY = false -- Default show all objects to players
local DEFAULT_LABEL_SIZE = 0.016
-- Modifies labels for titles assuming their variable for lable is called "actorTitle"
local ACTORTITLE_LABEL = {
	SIZE = 0.015,
	COLOR = {
		r = 0.9,
		g = 0.5,
		b = 0.2
	}
}

----------------------------------------
-- Public class variables
----------------------------------------
s_type = "Base"




----------------------------------------
-- Class constructor/destructor
----------------------------------------
-- Instantiate an instance of the Base class
function create(self, input)
	-- TODO: Error check input values
	if input == nil then input = {} end

	-- User-definable variables (Public, document for users)
	self.UNID	  = input.UNID
	self.PID 	  = input.PID or kgp.objectGetId() or DEFAULT_PID
	self.name     = input.name   or (Class.getClassName(self, true) .. ":" .. self.PID)
	self.level	  = input.level or DEFAULT_LEVEL
	self.health   = input.health or DEFAULT_HEALTH

	if input.position then --Generated objects need to pass in initial position
		self.position = {
			x	= input.position.x or DEFAULT_POSITION.x,
			y	= input.position.y or DEFAULT_POSITION.y,
			z	= input.position.z or DEFAULT_POSITION.z,
			rx	= input.position.rz or DEFAULT_POSITION.rx,
			ry	= input.position.ry or DEFAULT_POSITION.ry,
			rz	= input.position.rz or DEFAULT_POSITION.rz
		}
	else
		local baseX, baseY, baseZ, baseRX, baseRY, baseRZ = kgp.objectGetStartLocation(self.PID) -- Get object's location
		self.position = {
			x  = baseX or DEFAULT_POSITION.x,
			y  = baseY or DEFAULT_POSITION.y,
			z  = baseZ or DEFAULT_POSITION.z,
			rx = baseRX or DEFAULT_POSITION.rx,
			ry = baseRY or DEFAULT_POSITION.ry,
			rz = baseRZ or DEFAULT_POSITION.rz
		}
	end
	
	self.m_uItemProperties = input.itemProperties

	self.imageURL 		 = input.imageURL 	 or DEFAULT_IMAGEURL
	self.visible		 = {} -- Table of objects this can see

	
	self.creatorOnly = input.creatorOnly or DEFAULT_CREATOR_ONLY -- Does this object hide from players?
	self.ownerOnly = input.ownerOnly or DEFAULT_CREATOR_ONLY -- Does this object hide from players who don't own it?
	
	-- Custom modeLabels?
	if input.modeLabels then
		self.modeLabels = {}
		for labelMode, modeLabelTable in pairs(input.modeLabels) do
			self.modeLabels[labelMode] = {}
			for labelIndex, labelField in pairs(modeLabelTable) do
				if self[labelField] then
					local labelSize = DEFAULT_LABEL_SIZE
					local labelColor = {}
					if labelField == "actorTitle" then
						labelSize = ACTORTITLE_LABEL.SIZE
						labelColor = ACTORTITLE_LABEL.COLOR
					end
					self.modeLabels[labelMode][labelIndex] = {
						label = tostring(self[labelField]), 
						size = labelSize,
						color = {r = labelColor.r, g = labelColor.g, b = labelColor.b}
					}
				end
			end
		end
	end
	
	-- Base required fields
	self.type    = self.s_type or Class.getClassName(self)
	
	if input.owner then
		self.owner = input.owner
	end

	self.targetable = input.targetable or "false"
	self.alive = input.alive or true
	
	self.loot = input.loot
	
	-- CJW, 05.12.2016 - Commenting out as nested tables can now be copied in full. Keeping old code just in case things go awry.
	--[[
	-- Create singles loot field for SD if we have singles
	if input.loot and input.loot.singles then
		-- Instantiate SD table for singles (Can I also populate it on declaration?)
		self.loot.singles = {}
		for i,v in pairs(input.loot.singles) do
			self.loot.singles[i] = v
		end
	end
	-- Do the same for limitItems
	if input.loot and input.loot.limitItems then
		-- Instantiate SD table for singles (Can I also populate it on declaration?)
		self.loot.limitItems = {}
		for i,v in pairs(input.loot.limitItems) do
			self.loot.limitItems[i] = v
		end
	end
	--]]
	
	-- This value will get updated when the current object gets removed from gameplay. 
	self.isBeingRemoved = false

	-- TODO: Wrap all event handlers in public functions for overriding
	-- Register for left click event
	Events.registerHandler("MOUSE-LEFT-CLICK",  self.onLeftClick, self)
	-- Register for right click event
	Events.registerHandler("MOUSE-RIGHT-CLICK", self.onRightClick, self)
	
	Events.registerHandler("ON_USE_OBJECT", self.onUse, self)
	
	-- Register for events sent when an object is about to be detached from its class, which is a precursor to an object being removed from game.
	Events.registerHandler("OBJECT_REMOVE_CLASS", self.onClassBeingDetached, self)

	-- Register with controller
	Events.sendEvent("CONTROLLER_REGISTER", {sharedData = self._dictionary})
	
	if kgp.objectGetId() ~= self.PID then
		kgp.objectMapEvents(self.PID)
	end
end

-- Called on destroy, implemented here so that there are turtles all the way down
function destroy(self)
	local input = {PID = self.PID}
	
	-- Un-Register from the Controller (Controller_Game)
	Events.sendEvent("CONTROLLER_UNREGISTER", {sharedData = self._dictionary})
	
	Events.unregisterHandler("MOUSE-LEFT-CLICK",  self.onLeftClick, self)
	Events.unregisterHandler("MOUSE-RIGHT-CLICK", self.onRightClick, self)
	Events.unregisterHandler("ON_USE_OBJECT", self.onUse, self)
end


----------------------------------------
-- Event handlers
----------------------------------------

-- TODO: Wrap all event handlers in public functions for overriding
-- Left click handler
function onLeftClick(self, event)

end

-- Right click handler
function onRightClick(self, event)

end

-- On use handler
function onUse(self, event)
end

-- Register for events sent when an object is about to be removed.
function onClassBeingDetached(self, event)
	if( event.PID == self.PID ) then
		self.isBeingRemoved = true
	end
end


----------------------------------------
-- Public Class Methods
----------------------------------------

-- Register with the controller
function registerWithController(self)
	-- Register with the Controller (Controller_Master)
	Events.sendEvent("CONTROLLER_REGISTER", {sharedData = self._dictionary})
end

-- Records interaction metric
function recordPlayerInteraction(self, playerName, interaction, data, collectionPrefix)
	if playerName == nil then log("recordPlayerInteraction() - playerName is nil."); return end
	if interaction == nil then log("recordPlayerInteraction() - interaction is nil."); return end
	data = data or {}
	if type(data) ~= "table" then log("recordPlayerInteraction() - data must be either a table or nil."); return end

	local zoneInstanceId, zoneIndex, zoneType = kgp.gameGetCurrentInstance()

	data.username = playerName
	data.interaction = interaction
	data.zoneInstanceId = zoneInstanceId
	data.zoneType = zoneType
	data.gameItemName = self.name
	data.gameItemId = self.UNID
	data.timestamp = kgp.gameGetCurrentTime()

	kgp.scriptRecordMetric(string.lower(collectionPrefix or self.type) .. "_interaction_log", data)
end


function getOwnerData(self)
	return Players.extractUser(self.owner)
end

----------------------------------------------
-- Visual
----------------------------------------------

-- Make an object invisible.
function hide(self)
	kgp.objectHide(self.PID)
end

-- Show a previously hidden object.
function show(self)
	kgp.objectShow(self.PID)
end

-- Kill yourself
function kill(self)
	kgp.gameItemDeletePlacement(self.PID)
	Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = self.PID})
end

function setDrawDistance(self, distance)
	kgp.objectSetDrawDistance(self.PID, distance)
end

-- Change the size of an object.
function scale(self, ratio)
	kgp.objectScale(self.PID, ratio)
end

-- Add floating text to an object.
function addLabels(self, labels)
	kgp.objectAddLabels(self.PID, labels)
end

-- Remove floating text from an object.
function clearLabels(self)
	kgp.objectClearLabels(self.PID)
end

-- Sets the current animation of an object
function setAnimation(self, animation, elapseCallback)
    elapseCallback = elapseCallback or false
    local loopCtl = 0 -- use loop setting from the item
	if animation and animation >= 0 then
		kgp.objectSetAnimation(self.PID, animation, loopCtl, elapseCallback)
	end
end

-- Sets the texture of a dynamic object.
function setTexture(self, imagePath)
	kgp.objectSetTexture(self.PID, imagePath)
end

-- Pan the texture of an object.
function setTexturePanning(self, meshId, uvSetId, uIncrement, vIncrement)
	kgp.objectSetTexturePanning(self.PID, meshId, uvSetId, uIncrement, vIncrement)
end

-- Attaches the specified particle Id on an object.
function setParticle(self, particle, bone, slotId, offset, dir, up)
	if particle and particle > 0 then
		bone	= bone or ""
		slotId	= slotId or 0
		offset	= Math.validateVector(offset, "offset", 5, Math.createVector(0, 0, 0))
		dir		= Math.validateVector(dir, "dir", 6, Math.createVector(-1, 0, 0))
		up		= Math.validateVector(up, "up", 7, Math.createVector(0, 1, 0))
		kgp.objectSetParticle(self.PID, particle, bone, slotId, offset.x, offset.y, offset.z, dir.x, dir.y, dir.z, up.x, up.y, up.z)
	end
end

-- Removes particle effect from an object.
function removeParticle(self, bone, slotId)
	bone = bone or ""
	slotId = slotId or 0
	kgp.objectRemoveParticle(self.PID, bone, slotId)
end

-- Updates item's tooltip
function updateTooltip(self, tooltip)
	self.tooltip = tooltip
	Events.sendClientEvent("FRAMEWORK_MODIFY_BATTLE_OBJECT", {ID = self.PID, modifiedFields = {tooltip = tooltip}})
end

-- Highlight the outline of an object.
function setOutline(self, enabled, col)
	local color = Colors.validateColor(col, "color", 3, Colors.createColor(0, 0, 0))
	kgp.objectSetOutline(self.PID, enabled, color.r, color.g, color.b)
end

-- Play a sequence of synchronized effects on an object.
function setEffectTrack(self, effectTable)
	kgp.objectSetEffectTrack(self.PID, effectTable)
end

function setCollision(self, enabled)
	if enabled==true then
		enabled = 1
	elseif enabled==false then
		enabled = 0
	end
	kgp.objectSetCollision(self.PID, enabled)
end

function setFriction(self, enabled)
	kgp.objectSetFriction(self.PID, enabled)
end

function lookAt(self, target)
	-- Look at object?
	if Class.isInstanceOf(target, "Class_Base") then
		kgp.objectLookAt(self.PID, 3, target.PID)
	-- Look at player perhaps?
	else
		-- Attempt ot extract a user
		local user = Players.extractUser(target)
		if user then
			kgp.objectLookAt(self.PID, 4, user.ID)
		end
	end
end

-- Behavior_Sound

function generateSound(input)
	-- TODO: Error check input
	return Behavior_Sound.generate(input)
end

function configSound(input)
	-- TODO: Error check input
	Behavior_Sound.config(input)
end

function playSound(input)
	-- TODO: Error check input
	Behavior_Sound.play(input)
end

function stopSound(input)
	-- TODO: Error check input
	Behavior_Sound.stop(input)
end

function deleteSound(input)
	-- TODO: Error check input
	Behavior_Sound.delete(input)
end

-- Player

function playerSetPhysics(input)
	if input.user == nil then Console.Write(Console.DEBUG, "playerSetPhysics() - [user] is nil."); return end

	Game.playerSetPhysics(input)
end

function playerTeleport(input)
	-- Error check inputs
	if input.user == nil then Console.Write(Console.DEBUG, "playerTeleport() - [user] is nil."); return end
	if tonumber(input.x) == nil then Console.Write(Console.DEBUG, "playerTeleport() - [x] must be a number. Got "..tostring(input.x)); return end
	if tonumber(input.y) == nil then Console.Write(Console.DEBUG, "playerTeleport() - [y] must be a number. Got "..tostring(input.y)); return end
	if tonumber(input.z) == nil then Console.Write(Console.DEBUG, "playerTeleport() - [z] must be a number. Got "..tostring(input.z)); return end
	if tonumber(input.rx) == nil then Console.Write(Console.DEBUG, "playerTeleport() - [rx] must be a number. Got "..tostring(input.rx)); return end
	if tonumber(input.ry) == nil then Console.Write(Console.DEBUG, "playerTeleport() - [ry] must be a number. Got "..tostring(input.ry)); return end
	if tonumber(input.rz) == nil then Console.Write(Console.DEBUG, "playerTeleport() - [rz] must be a number. Got "..tostring(input.rz)); return end
	
	Game.playerTeleport(input)
end

-- Add health
function playerAddHealth(input)
	-- Error check inputs
	if input.user == nil then Console.Write(Console.DEBUG, "playerAddHealth() - [user] is nil."); return end
	if tonumber(input.health) then
		if input.health < 0 then Console.Write(Console.DEBUG, "playerAddHealth() - [health] must be greater than 0."); return end
	else
		Console.Write(Console.DEBUG, "playerAddHealth() - [health] must be a number. Got "..tostring(input.health)); return
	end
	Battle.playerAddHealth(input)
end

-- Remove health
function playerRemoveHealth(input)
	-- Error check inputs
	if input.user == nil then Console.Write(Console.DEBUG, "playerRemoveHealth() - [user] is nil."); return end
	if tonumber(input.health) then
		if input.health < 0 then Console.Write(Console.DEBUG, "playerRemoveHealth() - [health] must be greater than 0."); return end
	else
		Console.Write(Console.DEBUG, "playerRemoveHealth() - [health] must be a number. Got "..tostring(input.health)); return
	end
	Battle.playerRemoveHealth(input)
end

-- Set health
function playerSetHealth(input)
	-- Error check inputs
	if input.user == nil then Console.Write(Console.DEBUG, "playerSetHealth() - [user] is nil."); return end
	if tonumber(input.health) then
		if input.health < 0 then Console.Write(Console.DEBUG, "playerSetHealth() - [health] must be greater than 0."); return end
	else
		Console.Write(Console.DEBUG, "playerSetHealth() - [health] must be a number. Got "..tostring(input.health)); return
	end
	Battle.playerSetHealth(input)
end

-- Add energy
function playerAddEnergy(input)
	-- Error check inputs
	if input.user == nil then Console.Write(Console.DEBUG, "playerAddEnergy() - [user] is nil."); return end
	if tonumber(input.energy) then
		if input.energy < 0 then Console.Write(Console.DEBUG, "playerAddEnergy() - [energy] must be greater than 0."); return end
	else
		Console.Write(Console.DEBUG, "playerAddEnergy() - [energy] must be a number. Got "..tostring(input.energy)); return
	end
	Game.playerAddEnergy(input)
end

-- Remove energy
function playerRemoveEnergy(input)
	-- Error check inputs
	if input.user == nil then Console.Write(Console.DEBUG, "playerRemoveEnergy() - [user] is nil."); return end
	if tonumber(input.energy) then
		if input.energy < 0 then Console.Write(Console.DEBUG, "playerRemoveEnergy() - [energy] must be greater than 0."); return end
	else
		Console.Write(Console.DEBUG, "playerRemoveEnergy() - [energy] must be a number. Got "..tostring(input.energy)); return
	end
	Game.playerRemoveEnergy(input)
end

-- Set Energy
function playerSetEnergy(input)
	-- Error check inputs
	if input.user == nil then Console.Write(Console.DEBUG, "playerSetEnergy() - [user] is nil."); return end
	if tonumber(input.energy) then
		if input.energy < 0 then Console.Write(Console.DEBUG, "playerSetEnergy() - [energy] must be greater than 0."); return end
	else
		Console.Write(Console.DEBUG, "playerSetEnergy() - [energy] must be a number. Got "..tostring(input.energy)); return
	end
	Game.playerSetEnergy(input)
end

function playerPlaySound(input)
	-- Error check inputs
	if input.user == nil then Console.Write(Console.DEBUG, "playerPlaySound() - [user] is nil."); return end
	if tonumber(input.soundGLID) == nil then Console.Write(Console.DEBUG, "playerPlaySound() - [soundGLID] is nil."); return end
	if input.playTime then
		if tonumber(input.playTime) == nil then Console.Write(Console.DEBUG, "playerPlaySound() - [playTime] must be a number. Got "..tostring(input.playTime)); return end
	end

	Game.playerPlaySound(input)
end

function playerStopSound(input)
	-- Error check inputs
	if input.user == nil then Console.Write(Console.DEBUG, "playerStopSound() - [user] is nil."); return end
	if tonumber(input.soundGLID) == nil then Console.Write(Console.DEBUG, "playerStopSound() - [soundGLID] must be a number. Got "..tostring(input.soundGLID)); return end

	Game.playerStopSound(input)
end

-- Registers a specific item for the health bar
function enableHealthBar(self)
	Game.enableHealthBar({})
end

-- Registers a specific item for the energy ba
function enableEnergyBar(self)
	Game.enableEnergyBar({})
end

-- Sets an animation on a player
function playerSetAnimation(input)
	-- Error check inputs
	if input.user == nil then Console.Write(Console.DEBUG, "playerSetAnimation() - [user] is nil"); return end
	if type(input.animation) ~= "number" then Console.Write(Console.DEBUG, "playerSetAnimation() - [animation] must be a number. Got "..tostring(input.animation)); return end
	
	Game.playerSetAnimation(input)
end


-- Places a player into place mode
function playerPlaceItem(input)
	-- Error check inputs
	if input.user == nil then log("playerPlaceItem() - No user passed in."); return end
	local userTemp = input.user
	input.user = Players.extractUser(input.user)
	if input.user == nil then log("playerPlaceItem() Failed to extract user from ["..tostring(userTemp).."]"); return end
	if input.user.ID == nil then log("playerPlaceItem() user.ID is nil."); return end
	
	if input.GLID == nil then log("playerPlaceItem() - No GLID passed in."); return end
	if type(input.GLID) ~= "number" then log("playerPlaceItem() - GLID passed in isn't a number."); return end
	
	Game.playerPlaceItem(input)
end


----------------------------------------------
-- Proximity checking
-- CJW, NOTE: These should be moved into a library!!
----------------------------------------------

-- {x, y, z}
function getDistanceToPoint(self, input)
	return math.sqrt( getDistanceSqrdToPoint(self, input) )
end

function getDistanceSqrdToPoint(self, input)
	local diffX = self.position.x - (input.x or 0)
	local diffY = self.position.y - (input.y or 0)
	local diffZ = self.position.z - (input.z or 0)
	return diffX * diffX + diffY * diffY + diffZ * diffZ
end

function isPointWithinRange(self, point, range)
	local distanceSqrd = self:getDistanceSqrdToPoint(point)
	
	return distanceSqrd <= range * range
end


----------------------------------------------
-- Media
----------------------------------------------

-- Set a specific swf to play on an object.
function setFlash(self, url, flashParams, userFilter)
	if userFilter then
		kgp.objectSetFlash(self.PID, url, flashParams, userFilter)
	else
		kgp.objectSetFlash(self.PID, url, flashParams)
	end
end

function clearFlash(self)
	kgp.objectSetFlash(self.PID, "", "")
end

-- Set the playlist on a media object (TV, radio, etc)
function setPlaylist(self, playlistID, turnOff)
	return kgp.objectSetPlayList(self.PID, playlistID, turnOff )
end

-- Set the current track of the current playlist.
function playTrack(self, track)
	return kgp.objectSetNextPlayListItem(self.PID, track)
end

-- Get the next track from the current playlist.
function getNextTrack(self)
	return kgp.objectGetNextPlayListItem(self.PID)
end


----------------------------------------------
-- Game Tools Helper Functions
-- CJW, NOTE: These should be moved into a library!!
----------------------------------------------

-- Send message to the ticker
function writeToTicker(input)
	-- Error check color inputs
	if type(input.color) ~= "table" then 
		input.color = {r = 255, g = 255, b = 255} 
	else
		if type(input.color.r) ~= "number" then 
			input.color.r = 255 
		else
			input.color.r = math.min(math.abs(input.color.r), 255)
		end
		if type(input.color.g) ~= "number" then 
			input.color.g = 255
		else
			input.color.g = math.min(math.abs(input.color.g), 255)
		end
		if type(input.color.b) ~= "number" then 
			input.color.b = 255
		else
			input.color.b = math.min(math.abs(input.color.b), 255)
		end
	end

	input.message = tostring(input.message)
	
	-- Error check user input if one exist
	if input.user then
		local userTemp = input.user
		input.user = Players.extractUser(input.user)
		if input.user == nil then log("writeToTicker() Failed to extract user from ["..tostring(userTemp).."]"); return end
		if input.user.ID == nil then log("writeToTicker() user.ID is nil."); return end
	end
	
	Game.writeToTicker(input)
end



