--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_Mover.lua
-- Parent class for creating moving objects with waypoints

Class.createClass("Class_Mover", "Class_BaseMobile")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Events			= _G.Events

-----------------------------------------------------------------
-- Constant Definitions
-----------------------------------------------------------------
local DEFAULT_NAME = "Mover"
local DEFAULT_PAUSE_TIME = 3 -- Amount of time in seconds to wait at each waypoint
local DEFAULT_SPEED = 8 -- Kaneva Units per second
s_type = "mover"

-----------------------------------------------------------------
-- Class-specific functions
-----------------------------------------------------------------
local moveObject --(self)
local getCurrWaypoint --(waypoints)
local getDestWaypoint --(waypoints)


-- Breathe life into moving platforms
function create(self, input)
	if input == nil then input = {} end
	self.name = input.name or DEFAULT_NAME
	_super.create(self, input)
	
	self.speed = input.speed or DEFAULT_SPEED -- Movement speed of the platform
	
	-- Instance-specific variables used for movement
	self.tick = nil -- Last tick time value
	self.timePaused = 0 -- Amount of time waiting at a waypoint
	self.moving = false -- Are we currently in motion?
	self.currWaypoint = nil -- The current waypoint PID
	self.destWaypoint = nil -- The destination waypoint PID
	self.stopDelay = input.stopDelay or DEFAULT_PAUSE_TIME
	
	-- Data for movement waypoints
	self.waypoints = {} -- Table of object PIDs associated with the mover
	self.waypointInfo = nil -- SharedData pointers for associated waypoints
	
	self:setFriction(true) -- Platforms have friction
	
	Events.registerHandler("RETURN_MAP_BY_TYPE",  self.onMapReturn, self) -- Event response on requesting waypoint data from the controller
	
	-- Object association events, used for querying the controller and processing the response
	Events.registerHandler("FRAMEWORK_RETURN_OBJECT_ASSOCIATIONS", self.onReturnObjectAssociations, self)
	Events.sendEvent("FRAMEWORK_REQUEST_OBJECT_ASSOCIATIONS", {PID = self.PID})
end 

-- Cleanup on aisle Class_Mover!
function destroy(self)
	_super.destroy(self)
	Events.unregisterHandler("RETURN_MAP_BY_TYPE",  self.onMapReturn, self)
	Events.unregisterHandler("FRAMEWORK_RETURN_OBJECT_ASSOCIATIONS", self.onReturnObjectAssociations, self)
end

-- Timer to cycle the object
function onTimer(self, event)
	if self.tick == nil then self.tick = event.tick return end -- First tick, get a baseline
	local deltaTick = event.tick - self.tick -- Time elasped since the last tick
	if self.moving == false then -- We aren't moving, how long have we been waiting?
		self.timePaused = self.timePaused + deltaTick --Increment time

		-- If we have waited that this point long enough, move on to the next waypoint
		if self.timePaused >= (self.stopDelay * 1000) then
			moveObject(self)
		end
	end
	self.tick = event.tick -- Update instance tick
end

-- onArrived event handler to set object state
function onMovementArrived(self, event)	
	self.moving = false -- no longer in motion
	self.currWaypoint = self.destWaypoint -- Set current waypoint to destination
	self.destWaypoint = getDestWaypoint(self, self.waypoints) -- Get the next destination waypoint
	self.timePaused = 0 -- Start wait timer at 0 (counts up)
end

-- Table of objects associated with this PID received from the game controller
function onReturnObjectAssociations(self, event)
	if event.PID ~= self.PID then return end -- Ignore if this event is not for us
	self.waypoints = event.objectAssociations -- Associated objects in this case is a list of waypoints plus the original mover	
	Events.sendEvent("REQUEST_MAP_BY_TYPE", {type = "waypoint", PID = self.PID}) -- Request data about waypoints from the controller
end

-- Return event for REQUEST_MAP_BY_TYPE, should be a list of all waypoints
function onMapReturn(self, event)
	if event.PID == nil or event.PID ~= self.PID then return end -- Ignore if this event is not for us
	-- Validate the map type, we are expecting waypoints
	local mapType = ""
	for i, v in pairs(event.map) do
		mapType = v.type
		break
	end

	self.waypointInfo = {} -- Initialize the waypoint info table
	if mapType == "waypoint" then -- Sanity check
		for key, value in pairs(event.map) do -- Iterate through the returned map of waypoints and compare to our list of associated waypoints
			if tonumber(key) ~= self.PID then -- Object cannot be a waypoint for itself
				for _, waypointID in pairs(self.waypoints) do -- Iterate through our list of associated waypoints
					if tonumber(key) == tonumber(waypointID) then -- If this waypoint is associated, then add it to the info table
						self.waypointInfo[tonumber(waypointID)] = value -- sharedData pointer for the waypoint
					end
				end
			end
		end
	end
	
	if self.currWaypoint == nil then self.currWaypoint = getCurrWaypoint(self, self.waypoints) end -- Get the first waypoint on the list
	if self.destWaypoint == nil then self.destWaypoint = getDestWaypoint(self, self.waypoints) end -- Get the initial destination from the list
	
	-- If we have a valid start point, move the platform to that position
	-- TODO: Should this move be saved?
	if self.currWaypoint and self.waypointInfo[self.currWaypoint] then
		local moveInput = self.waypointInfo[self.currWaypoint].position -- Destination is the position of the waypoint
		self:teleport(moveInput)
	end
end

-- Move the object to the destination point
moveObject = function(self)
	if self.moving == false then -- Sanity check, are we already in motion?
		if self.destWaypoint and self.waypointInfo[self.destWaypoint] then -- If there is a valid destination and we have object info for the destination...
			self.moving = true -- Start motion
			local moveInput = self.waypointInfo[self.destWaypoint].position
			self:moveTo(moveInput) -- Move to the destination location
		else
			self.timePaused = 0 -- Reset wait time, something wasn't ready yet
		end
	end
end

-- Get the starting waypoint for object movement
-- TODO: This currently simply grabs the first non-platform PID from the list
getCurrWaypoint = function(self, waypoints)
	if waypoints == nil or #waypoints <= 2 then return nil end
	for _, waypointID in pairs(waypoints) do
		if waypointID ~= self.PID then
			return waypointID
		end
	end
	
	return nil
end

-- Get the destination waypoint based on the current position
-- TODO: This currently simply grabs the first non-platform and non-current waypoint PID from the list
getDestWaypoint = function(self, waypoints)
	if waypoints == nil or self.currWaypoint == nil or #waypoints <= 2 then return nil end
	for _, waypointID in pairs(waypoints) do
		if waypointID ~= self.PID and waypointID ~= self.currWaypoint then
			return waypointID
		end
	end
	
	return nil
end