--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_PathWaypoint.lua
-- PathWaypoint used by Riders

--include any libs that are not part of lib_common.lua
include("Lib_SaveLoad.lua")
include("Lib_GameDefinitions.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Toolbox			= _G.Toolbox
local Debug				= _G.Debug
local SaveLoad			= _G.SaveLoad
local GameDefinitions	= _G.GameDefinitions
local Events			= _G.Events
local Class				= _G.Class

Class.createClass("Class_PathWaypoint", "Class_Base")

Class.createProperties({
	"parentPathPID",
	"prevPathWaypointPID",
	"nextPathWaypointPID"
})

local CREATOR_ONLY = true -- This object won't display for players

s_type = "pathWaypoint"

-----------------------------------------------------------------
-- Class-specific functions
-----------------------------------------------------------------
function create(self, input)
	-- assign input to be passed to base class
	input = input or {}
	input.creatorOnly = CREATOR_ONLY
	input.modeLabels = nil
	-- create base class with the input needed
	self.registered = false
	Events.registerHandler("FRAMEWORK_OBJECT_REGISTERED", self.onRegistered, self)
	_super.create(self, input)
	kgp.gameItemSetPosRot(self.PID, self.position.x, self.position.y, self.position.z, self.position.rx, self.position.ry, self.position.rz)
	
	-- assign shared data
	if input.additionalPathInfo then
		self.parentPathPID = input.additionalPathInfo.parentPathPID
		self.prevPathWaypointPID = input.additionalPathInfo.prevPathWaypointPID
		self.nextPathWaypointPID = nil
	end
	
	-- create and assign variables local to this specific class
	self.pauseTime = 0
	self.rotationType = "Smooth"
	self.easeIn = false
	self.easeOut = false
	self.collisionTrigger = false
	self.triggerSize = 0
	self.deleted = false
	self.playersInsideTriggerVolume = {}
	
	self:setCollision(false) -- Waypoints have no collision
	
	-- register events and handlers
	Events.registerHandler("PATHWAYPOINT_ADDED_CONFIRMATION", self.pathWaypointAddedConfirmation, self)
	Events.registerHandler("WAYPOINT_UPDATE_NAME", self.pathWaypointUpdateName, self)
	Events.registerHandler("SAVE_PATHWAYPOINT_DATA", self.savePathWaypoint, self)
	--Events.registerHandler("DEL_PATH_WAYPOINT", self.delPathWaypoint, self)
	Events.registerHandler("UPDATE_WAYPOINT_LINKAGES", self.updateWaypointLinkages, self)
	Events.registerHandler("OBJECT_PICKUP", self.onObjectPickup, self)
	Events.registerHandler("PATH_DESTROYED_CLEANUP", self.onPathDestroyedCleanup, self)
	Events.registerHandler("REQUEST_WAYPOINT_INFO_FOR_RIDER", self.onRequestWaypointInfoForRider, self)
	Events.registerHandler("RequestForPathWaypointPropertiesEvent", self.onPathWaypointsPropertiesRequested, self)
	Events.registerHandler("UpdatePathWaypointPropertiesEvent", self.updatePathWaypointPropertiesEvent, self)
	Events.registerHandler("RequestForAllSiblingWaypointPIDs", self.onRequestForAllSiblingWaypointPIDs, self)
	
end

function destroy(self)
	_super.destroy(self)
	if self.trigger then
		Events.removeTrigger(self.trigger)
		if self.playersInsideTriggerVolume then
			for i, v in pairs(self.playersInsideTriggerVolume) do
				if v ~= nil then
					Events.sendEvent("WAYPOINT_TRIGGER_EVENT", {parentPathPID = self.parentPathPID, triggerType = 2})
				end
			end
		end
	end
	Events.unregisterHandler("FRAMEWORK_OBJECT_REGISTERED", self.onRegistered, self)
	Events.unregisterHandler("PATHWAYPOINT_ADDED_CONFIRMATION", self.pathWaypointAddedConfirmation, self)
	Events.unregisterHandler("WAYPOINT_UPDATE_NAME", self.pathWaypointUpdateName, self)
	Events.unregisterHandler("SAVE_PATHWAYPOINT_DATA", self.savePathWaypoint, self)
	--Events.unregisterHandler("DEL_PATH_WAYPOINT", self.delPathWaypoint, self)
	Events.unregisterHandler("UPDATE_WAYPOINT_LINKAGES", self.updateWaypointLinkages, self)
	Events.unregisterHandler("OBJECT_PICKUP", self.onObjectPickup, self)
	Events.unregisterHandler("PATH_DESTROYED_CLEANUP", self.onPathDestroyedCleanup, self)
	Events.unregisterHandler("REQUEST_WAYPOINT_INFO_FOR_RIDER", self.onRequestWaypointInfoForRider, self)
	Events.unregisterHandler("RequestForPathWaypointPropertiesEvent", self.onPathWaypointsPropertiesRequested, self)
	Events.unregisterHandler("UpdatePathWaypointPropertiesEvent", self.updatePathWaypointPropertiesEvent, self)
	Events.unregisterHandler("RequestForAllSiblingWaypointPIDs", self.onRequestForAllSiblingWaypointPIDs, self)
end

function onRegistered(self, event)
	if event.PID == self.PID and self.registered == false then
		self.registered = true
		self:loadPathWaypointData()
	end
end

function pathWaypointAddedConfirmation(self, event)
	if event.PID == self.PID then
		self:savePathWaypointData()
	end
end

function pathWaypointUpdateName(self, event)
	if event.waypointPID == self.PID then
		self.name = event.newName
		self:clearLabels()
		local labels = {}
		table.insert(labels, {self.name, 0.016})
		self:addLabels(labels)
		self:savePathWaypointData()
	end
end

function savePathWaypoint(self, event)
	if self.PID == event.PID1 or self.PID == event.PID2 or self.PID == event.PID3 then
		self:savePathWaypointData()
	end
end

function onObjectPickup(self, event)
	if (event.PID == self.PID) then
		self.deleted = true
		Events.sendEvent("DEL_PATH_WAYPOINT", {delPathWaypointPID = self.PID, parentPathPID = self.parentPathPID})
		self:delPathWaypoint()
	end
end

function onPathDestroyedCleanup(self, event)
	if event.parentPathPID == self.parentPathPID then
		Events.sendEvent("ACTIVE_DO_DESTROYED", {activeDODestroyedPID = self.PID})
		self:delPathWaypoint()
	end
end

function updateWaypointLinkages(self, event)
	if self.PID == event.prevWaypointPID then
		self.nextPathWaypointPID = event.nextWaypointPID
		self:savePathWaypointData()
	elseif self.PID == event.nextWaypointPID then
		self.prevPathWaypointPID = event.prevWaypointPID
		self:savePathWaypointData()
	end
end
function delPathWaypoint(self, event)
	if event == nil or event.delPathWaypointPID == self.PID then
		SaveLoad.deleteGameData("pathWaypoint_"..tostring(self.PID))
		kgp.gameItemDeletePlacement(self.PID)
		Events.sendEvent("OBJECT_REMOVE_CLASS", {PID = self.PID})
	end
end

function onRequestWaypointInfoForRider(self, event)
	idleInfo = {}
	moveInfo = {}
	waypointMatch = false
	if event.crntState == GameDefinitions.RIDER_STATES.idle and self.PID == event.crntWaypointPID then
		idleInfo.pauseTime = self.pauseTime
		waypointMatch = true
	elseif event.crntState == GameDefinitions.RIDER_STATES.moving then
		if self.PID == event.crntWaypointPID then
			moveInfo.easeOut = self.easeOut
			waypointMatch = true
		elseif self.PID == event.destWaypointPID then
			moveInfo.easeIn = self.easeIn
			moveInfo.rotationType = self.rotationType
			moveInfo.pauseTime = self.pauseTime
			waypointMatch = true
		end
	end
	if waypointMatch then
		Events.sendEvent("WAYPOINT_INFO_FOR_RIDER", {riderPID = event.riderPID, waypointPID = self.PID, currentState = event.crntState,
													 idleStateInfo = idleInfo, movingStateInfo = moveInfo})
	end
end

function onPathWaypointsPropertiesRequested(self, event)
	local player = event.player
	if player == nil then
		return
	end
	if event.pathWaypointPID == self.PID then
		Events.sendEvent("AddInfoToPathWaypointPropertiesEvent", {pauseTime = self.pauseTime, rotationType = self.rotationType, easeIn = self.easeIn and 1 or 0, easeOut = self.easeOut and 1 or 0,
																  collisionTrigger = self.collisionTrigger and 1 or 0, triggerSize = self.triggerSize, playerID = player.ID, 
																  pathWaypointPID = self.PID, parentPID = self.parentPathPID})
	end
end

function updatePathWaypointPropertiesEvent(self, event)
	if event.pathWaypointPID == self.PID then
		newProps = event.pathWaypointProperties
		if newProps ~= nil then
			self.pauseTime = newProps.pauseTime
			self.rotationType = newProps.rotationType
			self.easeIn = newProps.easeIn == 1
			self.easeOut = newProps.easeOut == 1
			self.collisionTrigger = newProps.collisionTrigger == 1
			self.triggerSize = newProps.triggerSize
			if self.trigger == nil and self.collisionTrigger == true then
				self.trigger = Events.setTrigger(self.triggerSize or 0, self.onWaypointTrigger, self)
				Events.sendEvent("UPDATE_COLLISION_STATE_TO_PARENT", {waypointPID = self.PID, parentPathPID = self.parentPathPID, collisionEnabled = self.collisionTrigger})
			elseif self.trigger ~= nil and self.collisionTrigger == true then
				Events.removeTrigger(self.trigger)
				if self.playersInsideTriggerVolume then
					for i, v in pairs(self.playersInsideTriggerVolume) do
						if v ~= nil then
							Events.sendEvent("WAYPOINT_TRIGGER_EVENT", {parentPathPID = self.parentPathPID, triggerType = 2})
						end
					end
				end
				self.playersInsideTriggerVolume = {}
				self.trigger = nil
				self.trigger = Events.setTrigger(self.triggerSize or 0, self.onWaypointTrigger, self)
			elseif self.trigger ~= nil and self.collisionTrigger == false then
				Events.removeTrigger(self.trigger)
				if self.playersInsideTriggerVolume then
					for i, v in pairs(self.playersInsideTriggerVolume) do
						if v ~= nil then
							Events.sendEvent("WAYPOINT_TRIGGER_EVENT", {parentPathPID = self.parentPathPID, triggerType = 2})
						end
					end
				end
				self.playersInsideTriggerVolume = {}
				self.trigger = nil
				Events.sendEvent("UPDATE_COLLISION_STATE_TO_PARENT", {waypointPID = self.PID, parentPathPID = self.parentPathPID, collisionEnabled = self.collisionTrigger})
			end
		end
		self:savePathWaypointData()
	end
end

function onRequestForAllSiblingWaypointPIDs(self, event)
	if event.pathWaypointPID == self.PID then
		Events.sendEvent("ClientRequestForAllChildPathPIDs", {pathWaypointPID = self.PID, parentPathPID = self.parentPathPID})
	end
end

function onWaypointTrigger(self, event)
	local player = event.player
	if player == nil then
		return
	end
	if event.triggerType == 1 then
		self.playersInsideTriggerVolume[tostring(player.ID)] = player.ID
	elseif event.triggerType == 2 then
		self.playersInsideTriggerVolume[tostring(player.ID)] = nil
	end
	Events.sendEvent("WAYPOINT_TRIGGER_EVENT", {parentPathPID = self.parentPathPID, triggerType = event.triggerType})
end

-----------------------------------------------------------------
-- Local functions
-----------------------------------------------------------------
function savePathWaypointData(self)
	if self.deleted == false then
		local pathWaypointData = {}
		pathWaypointData.pauseTime = self.pauseTime
		pathWaypointData.rotationType = self.rotationType
		pathWaypointData.easeIn = self.easeIn
		pathWaypointData.easeOut = self.easeOut
		pathWaypointData.collisionTrigger = self.collisionTrigger
		pathWaypointData.triggerSize = self.triggerSize
		pathWaypointData.parentPathPID = self.parentPathPID
		pathWaypointData.prevPathWaypointPID = self.prevPathWaypointPID
		pathWaypointData.nextPathWaypointPID = self.nextPathWaypointPID
		pathWaypointData.name = self.name
		-- encode the data and save it
		SaveLoad.saveGameData("pathWaypoint_".. tostring(self.PID), Events.encode(pathWaypointData))
	end
end

function loadPathWaypointData(self)
	-- set the current time 
	self.currentTime = (kgp.gameGetCurrentTime()) or 0 -- absolute time in seconds

	-- get the data from the 
	local pathWaypointDataString = SaveLoad.loadGameData("pathWaypoint_".. tostring(self.PID))
	local pathWaypointData = {}

	-- decode the string 
	if pathWaypointDataString then
		pathWaypointData = Events.decode(pathWaypointDataString)
	else
		pathWaypointData = nil
	end

	if pathWaypointData then 
		-- updating the state information 
		self.pauseTime = pathWaypointData.pauseTime
		self.rotationType = pathWaypointData.rotationType
		self.easeIn = pathWaypointData.easeIn
		self.easeOut = pathWaypointData.easeOut
		self.collisionTrigger = pathWaypointData.collisionTrigger
		self.triggerSize = pathWaypointData.triggerSize
		self.parentPathPID = pathWaypointData.parentPathPID
		self.prevPathWaypointPID = pathWaypointData.prevPathWaypointPID
		self.nextPathWaypointPID = pathWaypointData.nextPathWaypointPID
		self.name = pathWaypointData.name
		self:clearLabels()
		local labels = {}
		table.insert(labels, {self.name, 0.016})
		self:addLabels(labels)
	else
		-- if it is being placed for the first time
		if self.parentPathPID == nil then
			-- not the first waypoint
			Events.sendEvent("UPDATE_PATH_WAYPOINT_LINKAGES_ON_ADD", {crntPathWaypointPID = self.PID, prevPathWaypointPID = self.prevPathWaypointPID})
		elseif self.prevPathWaypointPID == nil then
			-- the first waypoint
			Events.sendEvent("ADD_PATH_WAYPOINT", {newPathWaypointPID = self.PID, parentPathPID = self.parentPathPID, prevPathWaypointPID = nil})
		end
	end
	
	if self.collisionTrigger == true then
		if self.trigger ~= nil then
			Events.removeTrigger(self.trigger)
			if self.playersInsideTriggerVolume then
				for i, v in pairs(self.playersInsideTriggerVolume) do
					if v ~= nil then
						Events.sendEvent("WAYPOINT_TRIGGER_EVENT", {parentPathPID = self.parentPathPID, triggerType = 2})
					end
				end
			end
			self.playersInsideTriggerVolume = {}
			self.trigger = nil
			Events.sendEvent("UPDATE_COLLISION_STATE_TO_PARENT", {waypointPID = self.PID, parentPathPID = self.parentPathPID, collisionEnabled = false})
		end
		self.trigger = Events.setTrigger(self.triggerSize or 0, self.onWaypointTrigger, self)
		Events.sendEvent("UPDATE_COLLISION_STATE_TO_PARENT", {waypointPID = self.PID, parentPathPID = self.parentPathPID, collisionEnabled = self.collisionTrigger})
	else
		if self.trigger ~= nil then
			Events.removeTrigger(self.trigger)
			if self.playersInsideTriggerVolume then
				for i, v in pairs(self.playersInsideTriggerVolume) do
					if v ~= nil then
						Events.sendEvent("WAYPOINT_TRIGGER_EVENT", {parentPathPID = self.parentPathPID, triggerType = 2})
					end
				end
			end
			self.trigger = nil
			Events.sendEvent("UPDATE_COLLISION_STATE_TO_PARENT", {waypointPID = self.PID, parentPathPID = self.parentPathPID, collisionEnabled = self.collisionTrigger})
		end
	end
end