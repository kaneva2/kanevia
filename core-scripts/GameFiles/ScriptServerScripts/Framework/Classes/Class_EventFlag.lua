--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

--Class_EventFlag

include("Lib_SaveLoad.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local SaveLoad			= _G.SaveLoad
local Events			= _G.Events

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------
Class.createClass("Class_EventFlag", "Class_Base")

Class.createProperties({
	"interactionRange",
	"ownerName"
})

-----------------------------------------------------------------
-- Constants
-----------------------------------------------------------------
local SECONDS_PER_DAY = 86400
local SECONDS_PER_HOUR = 3600
local MONTH_TABLE = {	
	[1] = "January",
	[2] = "February",
	[3] = "March",
	[4] = "April",
	[5] = "May",
	[6] = "June",
	[7] = "July",
	[8] = "August",
	[9] = "September",
	[10] = "October",
	[11] = "November",
	[12] = "December",
}

local DAY_NAMES = {"Thursday", "Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday"}

local PVP_AREA = 20
local BOUND_CHK_BUFFER_Y = 1.5 -- Amount of leeway for checks BELOW the Y value (accounts for slight slopes and the fact that the player avatar is technically slightly beneath the ground)

-----------------------------------------------------------------
-- Static members of the class
-----------------------------------------------------------------
s_type = "EventFlag"



function create(self, input)
	input = input or {}
	_super.create(self, input)

	self.tooltip = "Right-click to interact with event flag"
	self.name = "Player Event Flag"
	self.noDestruct = "false"
	self.eventStatus = "none"
	self.eventMessage = {}
	self.eventID = -1
	self.startTime = -1
	self.endTime = -1
	self.tick = 0
	self.initialInvite = false
	self.currentTime = kgp.gameGetCurrentTime() or 0
	self.ownerOnly = true
	self.interactionRange = 1
		
	-- Let you know the very first tick of this event flag. This is used to know weather or not to teleport player
	self.m_bIsFirstTick = false
	
	self:setCollision(false)

	self:loadEventFlag()

	local ownerInfo = kgp.objectGetOwner(self.PID)
	self.ownerName = ""
	if ownerInfo then
		self.ownerName = ownerInfo.ownerName
	end
	self.owner = self.ownerName
	Events.registerHandler("REGISTER_EVENT_FLAGS", self.registerEventFlag, self)
	Events.registerHandler("PLAYER_EVENT_INVITE", self.eventInvite, self)
	Events.registerHandler("TIMER", self.onTimer, self) -- Register for timer event
	Events.registerHandler("FRAMEWORK_EVENT_UPDATED", self.eventUpdated, self)
	Events.registerHandler("FRAMEWORK_DESTROY_EVENT_FLAG", self.destroyFlag, self)
	
	--self.proxTrigger = Events.setClientTrigger(25, self) -- PJD - Hidden object highlighting issue
end

function onRightClick(self, event)
	self:onActivate(event)
end

function onUse(self, event)
	if(event.PID and self.PID == event.PID) then
		self:onActivate(event)
	end
end

function onActivate(self, event)
	-- Only valid players can activate the flag
	if event.player and (event.player.mode ~= "God") then
		if event.player.name==self.owner then
			kgp.playerLoadMenu(event.player.ID, "Framework_EventFlag.xml")
			Events.sendClientEvent("UPDATE_EVENT_FLAG_MENU", {PID = self.PID, info = self.eventMessage, status = self.eventStatus}, event.player.ID)
		else
			kgp.playerShowStatusInfo(event.player.ID, "You do not have access to this flag.", 3, 255, 0, 0)
		end
	end
end

--register Flags
function registerEventFlag(self, event)
	self:formatDateString(event.eventsInfo)
	Events.sendEvent("FRAMEWORK_HIDE_OBJECT", {hide = true, PID = self.PID, owner = self.owner})
end

--TODO: Unregister all events once finished
function destroy(self)
	Events.unregisterHandler("TIMER", self.onTimer, self)
	Events.unregisterHandler("REGISTER_EVENT_FLAGS", self.registerEventFlag, self)
	Events.unregisterHandler("PLAYER_EVENT_INVITE", self.eventInvite, self)
	Events.unregisterHandler("FRAMEWORK_EVENT_UPDATED", self.eventUpdated, self)
	Events.unregisterHandler("FRAMEWORK_DESTROY_EVENT_FLAG", self.destroyFlag, self)
	
	if self.proxTrigger then
		Events.removeClientTrigger(self.proxTrigger)
	end

	if self.flagTrigger then
		Events.removeTrigger(self.flagTrigger)
	end

	 Events.sendClientEvent("FRAMEWORK_WORLD_MAP_OBJECT_REMOVED", {PID = self.PID})

	Events.sendEvent("UNREGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "event", PID = self.PID})
	_super.destroy(self)
end

--timer handler
function onTimer(self, event)
	self:onServerTick(event)
end

function onServerTick(self, event)
	if self.tick == 0 then self.tick = event.tick / 1000 end
	local deltaTick = event.tick / 1000 - self.tick
	self.currentTime = self.currentTime + deltaTick
	self.tick = event.tick / 1000
	if self.endTime > 0  then
		self:handleEventTimer()
	elseif tonumber(self.createTime) + (2 * SECONDS_PER_DAY) < self.currentTime then
	 	self:removeEvent()
		self:destroy()
	end
end

function eventInvite(self, event)
	if self.eventStatus == "Occurring" then
		self.eventMessage.owner = self.owner

		kgp.playerLoadMenu(event.player.ID, "Framework_EventFlagInvite.xml")
		Events.sendClientEvent("FRAMEWORK_EVENT_FLAG_START", {PID = self.PID, info = self.eventMessage, status = self.eventStatus}, event.player.ID)
	end
end

function destroyFlag(self, event)
	if event.PID == self.PID then
		self:removeEvent()
	end
end

function removeEvent(self)
	kgp.gameItemDeletePlacement(self.PID)
	SaveLoad.deleteGameData("EventFlag_".. tostring(self.PID))
	if self.eventStatus == "Scheduled" then
		--Need support to make webcall from server
		Events.sendClientEvent("FRAMEWORK_REMOVE_EVENT", {eventID = self.eventID})
	end
end
----------------------------------------
-- local functions
----------------------------------------

function  loadEventFlag(self)
	self.createTime = SaveLoad.loadGameData("EventFlag_".. tostring(self.PID))
	if not self.createTime then
		self.createTime = self.currentTime
	end

	self:saveEventFlag()
end

function saveEventFlag(self)
	
	SaveLoad.saveGameData("EventFlag_".. tostring(self.PID), self.createTime)
end

function handleEventTimer(self)

	self.ownerOnly = true
	if self.currentTime > self.endTime then
		self:removeEvent()
		self:destroy()
	elseif self.currentTime >= self.startTime then
		if self.eventStatus ~= "Occurring" then
			self.eventStatus = "Occurring"
			self:createPVPFlag()
			Events.sendEvent("EVENT_FLAG_OCCURRING", {PID = self.PID, teleport = tostring(self.m_bIsFirstTick == false)})
			Events.sendEvent("FRAMEWORK_HIDE_OBJECT", {hide = false, PID = self.PID, owner = self.owner})
		end
		self.eventMessage.remainingTime = math.ceil((self.endTime - self.currentTime) /60 )
	end
	self.m_bIsFirstTick = true
end

function formatDateString(self, message)
	for eventStr in string.gmatch(message, "<Event>(.-)</Event>") do
		pid = string.match(eventStr, "<ObjPlacementId>(.-)</ObjPlacementId>")
		if tostring(pid) == tostring(self.PID) then
			self.eventID = string.match(eventStr, "<EventId>(.-)</EventId>")
			local startDate = string.match(eventStr, "<StartTime>(.-)</StartTime>")
			
			local title = string.match(eventStr, "<Title>(.-)</Title>")
			local description = string.match(eventStr, "<Details>(.-)</Details>")
			local endDate = string.match(eventStr, "<EndTime>(.-)</EndTime>")

			createMessage(self, self.eventID, startDate, title, description, endDate)
			self.eventStatus = "Scheduled"
			return
		end
	end
end

function  createPVPFlag(self)
	-- A box-shaped trigger with (0, 0, 0) at origin of the parent object
	local triggerMin = {x = -PVP_AREA/2, y = -BOUND_CHK_BUFFER_Y, z = -PVP_AREA/2}
	local triggerMax = {x = PVP_AREA/2, y = PVP_AREA, z =PVP_AREA/2}
	self.flagTrigger = Events.setTrigger({type = kgp.PrimitiveType.Cuboid, min = triggerMin, max = triggerMax }, self.onTrigger, self)
	self:registerSystemFlag({})
end

function onTrigger(self, event)
	local flag = {}
	flag.PID = self.PID
	flag.flagType = "pvp"
	flag.enabled = false

	Events.sendEvent("SYSTEM_FLAG_TRIGGER", {player = event.player, triggerType = event.triggerType, flagInfo = flag})
end 

function registerSystemFlag(self, event)
	local flagInfo = {}
	flagInfo.PID = self.PID
	flagInfo.position = {x = self.position.x, y = self.position.y, z = self.position.z}
	flagInfo.flagType = "pvp"
	flagInfo.enabled = false
	flagInfo.height = PVP_AREA
	flagInfo.depth = PVP_AREA
	flagInfo.width = PVP_AREA
	Events.sendEvent("REGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "flag", info = flagInfo})
end

function eventUpdated(self, event)
	if event.PID == self.PID then
		local newEvent = self.eventStatus == "none"
		self.eventID = event.eventID
		createMessage(self, self.eventID, event.startTime, event.title, event.description, event.endTime)
		self.eventStatus = "Scheduled"
		local flagInfo = {}
		flagInfo.PID = self.PID
		if newEvent then
			Events.sendEvent("REGISTER_SYSTEM_DYNAMIC_OBJECT", {objectType = "event", info = flagInfo})
		end
	end
end

function createMessage(self, eventID, startDate, title, description, endDate)

	local month, day, year = string.match(startDate, "(%d+)/(%d+)/(%d+)")
		
	local hour, minute, second = string.match(startDate, "(%d+):(%d+):(%d+)")
	local PMorAM = string.lower(string.sub(startDate, #startDate -1, #startDate))

	local ordinal = "th"
	local dayNumber = tonumber(day)
		if dayNumber == 1 or dayNumber == 21 or dayNumber == 31  then
			ordinal = "st"
		elseif dayNumber == 2 or dayNumber == 22 then
			ordinal = "nd"
		elseif dayNumber == 3 or dayNumber == 23 then
			ordinal = "rd"
		end

		trueHour = getTrueHour(hour, PMorAM)

		self.startTime =  kgp.gameDateToEpochTime(tonumber(year), tonumber(month), tonumber(dayNumber), tonumber(trueHour),tonumber(minute), tonumber(second))
	 	local dayOfWeek = DAY_NAMES[math.fmod(math.floor(self.startTime/SECONDS_PER_DAY), 7)+1]

	 	month = MONTH_TABLE[tonumber(month)]

	 	self.eventMessage.month = month
		self.eventMessage.day = day..ordinal
		self.eventMessage.hour = hour
		self.eventMessage.minute = minute
		self.eventMessage.PMorAM = PMorAM
		self.eventMessage.title = title
		self.eventMessage.description = description
		self.eventMessage.dayOfWeek = dayOfWeek
		self.eventMessage.eventID = self.eventID

		month, day, year = string.match(endDate, "(%d+)/(%d+)/(%d+)")
		
		hour, minute, second = string.match(endDate, "(%d+):(%d+):(%d+)")

		PMorAM = string.lower(string.sub(endDate, #endDate -1, #endDate))

		trueHour = getTrueHour(hour, PMorAM)

		self.endTime =  kgp.gameDateToEpochTime(tonumber(year), tonumber(month), tonumber(day), tonumber(trueHour),tonumber(minute), tonumber(second))

		--Adjust to time zone
		local localTime = kgp.gameGetCurrentLocalTime()
		self.currentTime = kgp.gameGetCurrentTime()
		self.startTime = self.startTime  + (self.currentTime - localTime )
		self.endTime = self.endTime + (self.currentTime - localTime )
end

function getTrueHour(hour, PMorAM)

	if PMorAM == "pm" and tonumber(hour) ~= 12 then
		trueHour = tonumber(hour) + 12
	elseif PMorAM == "am" and tonumber(hour) == 12 then
		trueHour = 0
	else
		trueHour = hour
	end
	return trueHour
end