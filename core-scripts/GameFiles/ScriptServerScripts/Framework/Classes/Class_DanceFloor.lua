--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_DanceFloor.lua
Class.createClass("Class_DanceFloor", "Class_InteractiveNode")

local DEFAULT_NODE_PARTICLE = 4351674
local DEFAULT_DANCE_MALE = 3838335
local DEFAULT_DANCE_FEMALE = 3838335

function create(self, input)
	if input == nil then 
		input = {}
	end
	_super.create(self, input)
	
	self.panSpeed = input.panSpeed
	self.nodeParticle = input.nodeParticle or DEFAULT_NODE_PARTICLE
	
	self.syncedAnimM = input.syncedAnimM or DEFAULT_DANCE_MALE
	self.syncedAnimF = input.syncedAnimF or DEFAULT_DANCE_FEMALE
	
	self.floorTexture = input.floorTexture
	if self.floorTexture then
		self:setTexture(self.floorTexture)
	end

	if self.panSpeed ~= nil and self.panSpeed.x ~= nil and self.panSpeed.y ~= nil and (self.panSpeed.x ~= 0 or self.panSpeed.y ~= 0) then
		self:setTexturePanning(0, 0, self.panSpeed.x, self.panSpeed.y)
	end
	
	-- TODO: Create Behavior_Particle
	if self.nodeParticle ~= nil and self.nodeParticle > 0 then
		for k, v in pairs(self.playerPositions) do
			self:setParticle(self.nodeParticle, "", tonumber(k), {x = v.male.x, y = v.male.y, z = v.male.z})
		end
	end
end

function destroy(self)
	_super.destroy(self)
	for k, v in pairs(self.playerPositions) do
		self:removeParticle("", tonumber(k))
	end
end

function addPlayerToNode(self, player, node)
	_super.addPlayerToNode(self, player, node)
	
	self:removeParticle("", tonumber(node))
	
	if self.isObjectFull(self) then
		for node, data in pairs(self.nodes) do
			if data.player ~= nil then
				local animation = self.syncedAnimM
				if data.player.gender == "F" then
					animation = self.syncedAnimF
				end
				playerSetAnimation({user = data.player, animation = animation})
			end
		end
	end
end

function removePlayerFromNode(self, player, node)
	_super.removePlayerFromNode(self, player, node)
	
	if node == nil then return end
	if self.playerPositions[node] == nil then return end
	
	if self.nodeParticle ~= nil and self.nodeParticle > 0 and node ~= nil then
		self:setParticle(self.nodeParticle, "", tonumber(node), {x = self.playerPositions[node].male.x, y = self.playerPositions[node].male.y, z = self.playerPositions[node].male.z})
	end
	
	for node, data in pairs(self.nodes) do
		if data.player ~= nil then
			if data.player.gender == "M" then
				gender = "male"
			else
				gender = "female"
			end

			playerSetAnimation({user = data.player, animation = self.playerPositions[node][gender].animation})
		end
	end
end