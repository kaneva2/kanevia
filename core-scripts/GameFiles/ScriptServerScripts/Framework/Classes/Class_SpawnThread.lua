--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Class_SpawnThread.lua
include("Lib_Vecmath.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Console			= _G.Console
local Events			= _G.Events
local Class				= _G.Class
local Math				= _G.Math

-----------------------------------------------------------------
-- Class specification
-----------------------------------------------------------------

Class.import("Spawn") --Utilizes the Spawn Controller

Class.createClass("Class_SpawnThread")

Class.createProperties({
	"PID",
	"managedPIDs",
	"threadLoad",
	"peakLoad"
})

-----------------------------------------------------------------
-- Global variable definitions
-----------------------------------------------------------------
s_type = "SpawnThread"

-----------------------------------------------------------------
-- Static variable definitions
-----------------------------------------------------------------
local s_uItemProperties = nil


-----------------------------------------------------------------
-- Constant definitions
-----------------------------------------------------------------

local DEFAULT_PID = 0 -- Default PID
local INSTANCE_INDEX = -1


-----------------------------------------------------------------
-- Local function declarations
-----------------------------------------------------------------

local addManagedPID --(self, PID)
local removeManagedPID --(self, PID)
local updateThreadLoad --(self)
local getObjectClass --(className)


function create(self, input)
	if input == nil then input = {} end
	self.objectList = {}
	self.objectList[INSTANCE_INDEX] = {}
	
	self.PID = input.PID or kgp.objectGetId() or DEFAULT_PID
	self.managedPIDs = {}
	self.threadLoad = 0
	self.peakLoad = 0
	
	Events.registerHandler("THREAD_OBJECT_SPAWN", self.threadSpawnObject, self)
	Events.registerHandler("THREAD_OBJECT_DESPAWN", self.threadDespawnObject, self)
	
	Events.registerHandler("THREAD_OBJECT_INSTANCE_CLASS", self.threadInstanceClass, self)
	Events.registerHandler("THREAD_OBJECT_REMOVE_CLASS", self.threadRemoveClass, self)
	
	Events.registerHandler("THREAD_OBJECT_UPDATE", self.threadUpdateObject, self)
	Events.registerHandler("THREAD_OBJECT_UPDATE_CHECK", self.threadUpdateObjectCheck, self)
	Events.registerHandler("THREAD_OBJECT_RESPAWN", self.threadRespawnObject, self)
	
	Events.registerHandler("THREAD_OBJECT_UPDATE_PLACEMENT_GLID", self.threadUpdatePlacementGLID, self)
	
	Spawn.registerThread(self._dictionary)
	
	-- Create a read-only global reference to the "item properties" shared data.
	-- Note that each "Class" instance created within this thread has access to this data.
	s_uItemProperties = SharedData.new("gameItemProperties", true)
end

--[[ Some objects need to be warned before they are the server shuts the world down. ]]--
function warnImminentDestruction(self)
	for PID, objectTable in pairs(self.objectList) do
		for _, instancedObject in pairs(objectTable) do
			if instancedObject and instancedObject.object then			
				if(instancedObject.object.onServerSpindown and type(instancedObject.object.onServerSpindown)  == "function") then
					instancedObject.object:onServerSpindown()
				end
			end
		end
	end
end

function stopThread(self)
	self:warnImminentDestruction()
end

function destroy(self)

	Events.unregisterHandler("THREAD_OBJECT_SPAWN", self.threadSpawnObject, self)
	Events.unregisterHandler("THREAD_OBJECT_DESPAWN", self.threadDespawnObject, self)
	
	Events.unregisterHandler("THREAD_OBJECT_INSTANCE_CLASS", self.threadInstanceClass, self)
	Events.unregisterHandler("THREAD_OBJECT_REMOVE_CLASS", self.threadRemoveClass, self)
	
	Events.unregisterHandler("THREAD_OBJECT_UPDATE", self.threadUpdateObject, self)
	Events.unregisterHandler("THREAD_OBJECT_UPDATE_CHECK", self.threadUpdateObjectCheck, self)
	Events.unregisterHandler("THREAD_OBJECT_RESPAWN", self.threadRespawnObject, self)
	
	Events.unregisterHandler("THREAD_OBJECT_UPDATE_PLACEMENT_GLID", self.threadUpdatePlacementGLID, self)
	
	--TODO: Unregister from controller
end


function threadSpawnObject(self, event)
	if getObjectClass(event.spawnClass) == nil then Console.Write(Console.DEBUG, "Error: Thread does not recognize specified class ("..tostring(event.spawnClass)..")!") return end
	if self.objectList[event.spawnerPID] == nil then self.objectList[event.spawnerPID] = {} end
	
	if (event.location.rx or 0) == 0 and (event.location.rz or 1) == 0 then
		--TODO: PJD - An error was encountered during testing and we need to track down the cause!
		Console.Write(Console.DEBUG, "Error: Spawn location was passed in with an invalid rotation (rx = "..tostring(event.location.rx)..", ry = "..tostring(event.location.ry)..", rz = "..tostring(event.location.rz)..")!")
		event.location.rz = 1
	end

	local objectPID
	if event.spawnInput and event.spawnInput.meshType and event.spawnInput.meshType ~= 1 then
		local tempCharConfig = { -- Placeholder default values
	        scaleY = 90,
	        scaleXZ = 90,
	        dbIndex = 12,
	        faceIndex = 4,
	        skinColor = 0,
	        faceCover = 5,
	        beardColor = 2,
	        hairIndex = 1,
	        hairColor = 24,
	        eyebrowIndex = 0,
	        eyeColor = 0,
	        GLIDS = { 1247, 1248, 1249 },
	    }

	    if event.spawnInput.meshGLID and type(event.spawnInput.meshGLID) == "string"  then
		    tempCharConfig = event.spawnInput.meshGLID
		    tempCharConfig = string.gsub(tempCharConfig, "\'", "\"")	
		    tempCharConfig = validateNPCData(tempCharConfig)

		else
			tempCharConfig = cjson.encode(tempCharConfig)
		end

        objectPID = kgp.npcSpawn{
            charConfig		= tempCharConfig,
            pos				= { x = event.location.x, y = event.location.y, z = event.location.z }, 
            rotDeg			= Math.vectorToDegrees(event.location.rx or 0, event.location.rz or 1),
            glidAnimSpecial	= 0,
            glidsArmed		= {},
        }
    else
        objectPID = kgp.objectGenerate(tonumber(event.GLID), event.location.x, event.location.y, event.location.z, event.location.rx or 0, event.location.ry or 0, event.location.rz or 1)
    end



	local input = event.spawnInput
	input.PID = objectPID
	input.spawnerPID = event.spawnerPID
	input.position = {x = event.location.x, y = event.location.y, z = event.location.z, rx = event.location.rx or 0, ry = event.location.ry or 0, rz = event.location.rz or 1}
	input.itemProperties = s_uItemProperties
	local object = getObjectClass(event.spawnClass).new(input)
	if object then
		addManagedPID(self, event.spawnerPID, objectPID)
		object.spawnType = event.spawnType
		self.objectList[event.spawnerPID][objectPID] = {object = object, weight = event.classWeight}
		updateThreadLoad(self)
		if event.spawnerPID > 0 and event.spawnType then
			Events.sendEvent("OBJECT_SPAWNED", {spawnerPID = event.spawnerPID, spawnType = event.spawnType, spawnPID = objectPID})
		end
	end
end

function threadDespawnObject(self, event)
	local tObject = event.spawnerPID > INSTANCE_INDEX and self.objectList[event.spawnerPID]
	if tObject == nil then return end
	
	for objectPID, objectData in pairs(tObject) do
		local object = objectData.object
		if (event.despawnPID == nil or event.despawnPID == objectPID) and (event.spawnType == nil or event.spawnType == object.spawnType) then
			removeManagedPID(self, event.spawnerPID, objectPID)
			if event.spawnerPID > 0 and event.spawnType then
				Events.sendEvent("OBJECT_DESPAWNED", {spawnerPID = event.spawnerPID, spawnType = event.spawnType, spawnPID = objectPID, refresh = event.refresh})
			end
			kgp.objectDelete(objectPID)
			if objectData then
				delete(objectData.object)
				tObject[objectPID] = nil
			end
			
			if event.despawnPID and event.despawnPID == objectPID then updateThreadLoad(self) return end
			if not event.despawnAll then updateThreadLoad(self) return end
		end
	end
	
	updateThreadLoad(self)
end

function threadInstanceClass(self, event)
	if getObjectClass(event.instanceClass) == nil then Console.Write(Console.DEBUG, "Error: Thread does not recognize specified class ("..tostring(event.instanceClass)..")!") return end
	if self.objectList[INSTANCE_INDEX][event.PID] ~= nil then return end --Class already instanced on object, need to remove it first!
	
	event.input.PID = event.PID
	event.input.itemProperties = s_uItemProperties
	

    -- BEGIN PLACED PAPER DOLL HACK
	local placementProperties = event.input
    if placementProperties and placementProperties.meshType and placementProperties.meshType ~= 1 then
        local tempCharConfig = {
            scaleY = 90,
            scaleXZ = 90,
            dbIndex = 12,
            faceIndex = 4,
            skinColor = 0,
            faceCover = 5,
            beardColor = 2,
            hairIndex = 1,
            hairColor = 24,
            eyebrowIndex = 0,
            eyeColor = 0,
            GLIDS = { 1247, 1248, 1249 }
        }

		if placementProperties.meshGLID and type(placementProperties.meshGLID) == "string" then
			tempCharConfig = placementProperties.meshGLID
			tempCharConfig = string.gsub(tempCharConfig, "\'", "\"")
			tempCharConfig = validateNPCData(tempCharConfig)
			
		else
			tempCharConfig = cjson.encode(tempCharConfig)
		end

		local position = {}
        position.x, position.y, position.z, position.rx, position.ry, position.rz = kgp.objectGetStartLocation(event.PID)

		if (position.rx or 0) == 0 and (position.rz or 0) == 0 then
			position.rz = 1
		end

		event.input.position = position

        log("threadInstanceClass: calling npcSpawn on PID " .. tostring(event.PID))
        kgp.npcSpawn{
            PID = event.PID,
            charConfig = tempCharConfig,
            pos = { x = position.x or 0, y = position.y or 0, z = position.z or 0 }, 
            rotDeg = Math.vectorToDegrees(position.rx or 0, position.rz or 1),
            glidAnimSpecial = 0,
            glidsArmed = {}
        }
    end
    -- END PLACED PAPER DOLL HACK
    local object = getObjectClass(event.instanceClass).new(event.input)

	if object then
		addManagedPID(self, event.PID)
		self.objectList[INSTANCE_INDEX][event.PID] = {object = object, weight = event.classWeight, peak = event.peakWeight, instanceInput = event.input, instanceClass = event.instanceClass}
		updateThreadLoad(self)
	end
end

function threadRemoveClass(self, event)
	local tObject = self.objectList[INSTANCE_INDEX]
	if tObject[event.PID] == nil then return end --No class attached
	
	removeManagedPID(self, event.PID)
	delete(tObject[event.PID].object)
	tObject[event.PID] = nil
	updateThreadLoad(self)
end

function threadUpdateObject(self, event)
	event.UNID = tonumber(event.UNID)
	for PID, instancedObject in pairs(self.objectList[INSTANCE_INDEX]) do
		if instancedObject and instancedObject.object and (instancedObject.object.UNID == event.UNID or (instancedObject.instanceInput and instancedObject.instanceInput.spawnInput and instancedObject.instanceInput.spawnInput.UNID == event.UNID)) then
			if event.delete == nil or event.delete ~= true then
				local instanceInput = instancedObject.instanceInput
				local updateInput = instanceInput
				if (instancedObject.instanceInput and instancedObject.instanceInput.spawnInput and instancedObject.instanceInput.spawnInput.UNID == event.UNID) then
					updateInput = instanceInput.spawnInput
					if event.GLID then
						instancedObject.instanceInput.spawnGLID = event.GLID
					end
					event.classWeight = instancedObject.weight
					event.peakWeight = instancedObject.peak
				end

				-- force the DO to appear at the same position as the spawner
				if instancedObject.object.spawnCoords then
					instancedObject.object.spawnCoords = instancedObject.object.position
				end

				if updateInput then
					for k, v in pairs(event) do
						updateInput[k] = v
					end

					-- if instancedObject.object.spawnCoords then
					-- 	instancedObject.object.spawnCoords = instancedObject.object.position
					-- end
					
					self:threadRemoveClass({PID = PID})
					self:threadInstanceClass({PID = PID, input = instanceInput, instanceClass = instancedObject.instanceClass, classWeight = event.classWeight, peakWeight = event.peakWeight, update = true})
				end
			else
				if instancedObject and instancedObject.object and instancedObject.object.s_type == "path" then
					instancedObject.object.completeDestruction = true
				end
				self:threadRemoveClass({PID = PID})
				kgp.gameItemDeletePlacement(PID)
			end
		end
	end
end

function threadUpdateObjectCheck(self, event)
	event.UNID = tonumber(event.UNID)
	local loadDelta = 0
	for PID, instancedObject in pairs(self.objectList[INSTANCE_INDEX]) do
		if instancedObject and instancedObject.object and instancedObject.object.UNID == event.UNID then
			loadDelta = loadDelta + (event.peakWeight - instancedObject.peak)
		end
	end
	Spawn.reportThreadUpdateDelta(event.pendingID, loadDelta)
end

function threadRespawnObject(self, event)
	event.PID = tonumber(event.PID)

	for PID, instancedObject in pairs(self.objectList[INSTANCE_INDEX]) do
		if instancedObject and instancedObject.object and instancedObject.object.PID == event.PID then
			if instancedObject.instanceInput.behavior and string.find(instancedObject.instanceInput.behavior, "spawner_") then
				instancedObject.object.spawnCoords = instancedObject.object.position
				self:threadDespawnObject({spawnerPID = PID, spawnType = instancedObject.object.spawnType, despawnAll = true, refresh = true})
			end
			return
		end
	end
end

function threadUpdatePlacementGLID(self, event)
	event.PID = tonumber(event.PID)
	event.GLID = tonumber(event.GLID)
	
	local instancedObject = self.objectList[INSTANCE_INDEX][event.PID]
	if instancedObject == nil then return end
	
	local ownerInfo = kgp.objectGetOwner(event.PID)
	
	if ownerInfo then
		self:threadRemoveClass({PID = event.PID})
		kgp.gameItemDeletePlacement(event.PID)
	
		local newPlacement = kgp.gameItemPlaceAsUser(ownerInfo.ownerId, event.GLID, instancedObject.object.UNID,
													 instancedObject.object.position.x, instancedObject.object.position.y, instancedObject.object.position.z,
													 instancedObject.object.position.rx, instancedObject.object.position.ry, instancedObject.object.position.rz)
													 
		if newPlacement then
			instancedObject.instanceInput.owner = instancedObject.object.owner
			instancedObject.instanceInput.PID = newPlacement
			Events.sendEvent("FRAMEWORK_UPDATE_OBJECT_PLACEMENT", {PID = newPlacement, prevPID = event.PID}) --handled by Controller_Inventory to ensure our associated item data is updated
			self:threadInstanceClass({PID = newPlacement, input = instancedObject.instanceInput, instanceClass = instancedObject.instanceClass, classWeight = instancedObject.classWeight, peakWeight = instancedObject.peakWeight})
			Events.sendEvent("OBJECT_PLACEMENT_GLID_UPDATED", {PID = newPlacement, prevPID = event.PID})
		end		
	end
end

function addManagedPID(self, PID, objPID)
	if PID == 0 and objPID ~= nil then PID = objPID end
	PID = tostring(PID)
	if self.managedPIDs[PID] == nil then
		self.managedPIDs[PID] = 1
	else
		self.managedPIDs[PID] = self.managedPIDs[PID] + 1
	end
end

function removeManagedPID(self, PID, objPID)
	if PID == 0 and objPID ~= nil then PID = objPID end
	PID = tostring(PID)
	if self.managedPIDs[PID] == nil then return end
	self.managedPIDs[PID] = self.managedPIDs[PID] - 1
	if self.managedPIDs[PID] <= 0 then self.managedPIDs[PID] = nil end
end

function updateThreadLoad(self)
	local threadLoad = 0
	local peakLoad = 0
	for PID, objectData in pairs(self.objectList) do
		for instancePID, instanceObjectData in pairs(objectData) do
			threadLoad = threadLoad + (instanceObjectData.weight or 0)
			peakLoad = peakLoad + (instanceObjectData.peak or 0)
		end
	end
	
	self.threadLoad = threadLoad
	self.peakLoad = peakLoad
end

function getObjectClass(className)
	if _G[className] then
		return _G[className]
	end
	
	return Class.importClass(className)
end


--This function will validate the properties of the NPC paperdoll to make sure they are valid
--For this instance of the validation, the face index should using zero based table, so we need to remove one from the faceIndex
function validateNPCData(npcData)
	local tempCharConfig = Events.decode(npcData)
	--fall back to default in worse-case scenario
	if not tempCharConfig then 
		 tempCharConfig = {
            scaleY = 90,
            scaleXZ = 90,
            dbIndex = 12,
            faceIndex = 4,
            skinColor = 0,
            faceCover = 5,
            beardColor = 2,
            hairIndex = 1,
            hairColor = 24,
            eyebrowIndex = 0,
            eyeColor = 0,
            GLIDS = { 1247, 1248, 1249 }
        }
		
	else
		tempCharConfig["faceIndex"] = tempCharConfig["faceIndex"] - 1
	end

	tempCharConfig = Events.encode(tempCharConfig)
	return tempCharConfig
end

