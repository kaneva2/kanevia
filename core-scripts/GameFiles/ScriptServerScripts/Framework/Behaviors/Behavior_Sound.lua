--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Behavior_Sound.lua
--
----------------------------------------

Class.createBehavior("Behavior_Sound")

-- Table of all sounds generated
local s_sounds = {} -- {soundID:data}

local DEFAULT_PITCH = 1
local DEFAULT_VOLUME = 1
local DEFAULT_DISTANCE = 25
local DEFAULT_ATTENUATION = 1
local DEFAULT_LOOPING = 1
local DEFAULT_LOOPDELAY = 0
local DEFAULT_INNERCONEANGLE = 360
local DEFAULT_OUTERCONEANGLE = 360
local DEFAULT_OUTERCONEGAIN = 1

-- Generate a new sound
function generate(input)
	local soundGLID = input.soundGLID
	local position = { x = 0,  y = 0,  z = 0,
					  rx = 1, ry = 0, rz = 0}
	if input.position then
		position = {x  = input.position.x  or 0,
					y  = input.position.y  or 0,
					z  = input.position.z  or 0,
					rx = input.position.rx or 1,
					ry = input.position.ry or 0,
					rz = input.position.rz or 0
				   }
	end
	local PID 			 = input.PID
	local ID  			 = input.ID
	
	local ambient 		 = input.ambient
	
	local pitch 		 = input.pitch
	local volume		 = input.volume
	local distance 		 = input.distance
	local attenuation 	 = input.attenuation
	local looping 		 = input.looping
	local loopDelay 	 = input.loopDelay
	local innerConeAngle = input.innerConeAngle
	local outerConeAngle = input.outerConeAngle
	local outerConeGain  = input.outerConeGain
	
	local playNow		 = input.playNow
	
	if soundGLID then
		local sound
		if ambient then
			sound = kgp.soundGenerateAmbient(soundGLID)
		else
			sound = kgp.soundGenerate(soundGLID, position.x, position.y, position.z, position.rx, position.ry, position.rz)
		end
		
		-- Sound generated successfully?
		if sound then
			if sound > 0 then
				-- Add sound to s_sounds table
				s_sounds[sound] = {GLID 		  = soundGLID,
								   ambient		  = ambient,
								   pitch 		  = pitch or DEFAULT_PITCH,
								   volume 		  = volume or DEFAULT_VOLUME,
								   distance 	  = distance or DEFAULT_DISTANCE,
								   attenuation 	  = attenuation or DEFAULT_ATTENUATION,
								   looping 		  = looping or DEFAULT_LOOPING,
								   loopDelay 	  = loopDelay or DEFAULT_LOOPDELAY,
								   innerConeAngle = innerConeAngle or DEFAULT_INNERCONEANGLE,
								   outerConeAngle = outerConeAngle or DEFAULT_OUTERCONEANGLE,
								   outerConeGain  = outerConeGain or DEFAULT_OUTERCONEGAIN,
								   attachedTo	  = PID or ID or nil,
								   playing		  = playNow or false
								  }

				-- Any modifications to be made?
				if pitch or volume or distance or attenuation or looping or loopDelay or innerConeAngle or outerConeAngle or outerConeGain then
					config({soundID		   = sound,
						    pitch 		   = pitch,
							volume 		   = volume,
							distance	   = distance,
							attenuation    = attenuation,
							looping 	   = looping,
							loopDelay 	   = loopDelay,
							innerConeAngle = innerConeAngle,
							outerConeAngle = outerConeAngle,
							outerConeGain  = outerConeGain
						   })
				end
				
				-- Don't attach an ambient sound
				if ambient == nil or ambient == false then
					-- Attach sound to Object?
					if PID then
						kgp.soundAttachToObject(sound, PID)
					-- Attach sound to Player?
					elseif ID then
						kgp.soundAttachToPlayer(sound, ID)
					end
				end
				
				-- Play the sound now?
				if playNow then
					local success = kgp.soundStart(sound)
				end
				
				return sound
			end
		end
	end
end

-- Config a sound effect
function config(input)
	local soundID = input.soundID
	
	-- TODO: Input checks
	if s_sounds[soundID] then
		s_sounds[soundID].pitch 		 = input.pitch 			or s_sounds[soundID].pitch
		s_sounds[soundID].volume 		 = input.volume			or s_sounds[soundID].volume
		s_sounds[soundID].distance 		 = input.distance 		or s_sounds[soundID].distance
		s_sounds[soundID].attenuation 	 = input.attenuation 	or s_sounds[soundID].attenuation
		s_sounds[soundID].looping 		 = input.looping 		or s_sounds[soundID].looping
		s_sounds[soundID].loopDelay 	 = input.loopDelay 		or s_sounds[soundID].loopDelay
		s_sounds[soundID].innerConeAngle = input.innerConeAngle or s_sounds[soundID].innerConeAngle
		s_sounds[soundID].outerConeAngle = input.outerConeAngle or s_sounds[soundID].outerConeAngle
		s_sounds[soundID].outerConeGain  = input.outerConeGain  or s_sounds[soundID].outerConeGain
		
		kgp.soundConfig(soundID, s_sounds[soundID].pitch, s_sounds[soundID].volume, s_sounds[soundID].distance, s_sounds[soundID].attenuation, s_sounds[soundID].looping, s_sounds[soundID].loopDelay, 
								 s_sounds[soundID].innerConeAngle, s_sounds[soundID].outerConeAngle, s_sounds[soundID].outerConeGain)
	end
end

-- Play a sound
function play(input)
	local soundID   = input.soundID
	local looping   = input.looping
	-- Was a pre-generated sound ID passed in?
	if soundID then
		-- Is this a registered sound?
		if s_sounds[soundID] then
			-- Does this sound need configuring first?
			if looping then
				if looping ~= s_sounds[soundID].looping then
					config({soundID = soundID, looping = looping})
				end
			end
			s_sounds[soundID].playing = true
		end
		-- Attempt to play sound
		kgp.soundStart(soundID)
	end
end

-- Stop a playing sound
function stop(input)
	local soundID = input.soundID
	
	-- Was a soundID passed in?
	if soundID then
		-- Stop playing this sound
		kgp.soundStop(soundID)
		
		-- Is this a registered sound?
		if s_sounds[soundID] then
			s_sounds[soundID].playing = false
		end
	end
end

-- Delete a sound
function delete(input)
	local soundID = input.soundID
	
	if soundID then
		kgp.soundDelete(soundID)
		
		-- Is this a registered sound?
		if s_sounds[soundID] then
			-- Clear it out
			s_sounds[soundID] = nil
		end
	end
end