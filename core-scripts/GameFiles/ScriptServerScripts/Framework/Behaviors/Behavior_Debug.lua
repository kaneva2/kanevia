--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Behavior_Debug.lua
--
----------------------------------------

Class.createBehavior("Behavior_Debug")

local s_functions = {}

-- Register functions and their expected parameters for future error-checking
function registerFunc(funcName, funcData)
	-- Is this a registered function?
	if s_functions[funcName] == nil then
		-- TODO: Construct func input table
		s_functions[funcName] = {}
	end
end

-- Check parameters for a given function to ensure proper types
function checkParams(funcName, ...)
	-- Is this a registered function?
	if s_functions[funcName] then
		-- TODO: Check each input against expected type
	end
end

-- Helpful warning formatting message
function warn(self, level, warning)
	log(string.upper(tostring(level))..": "..tostring(Class.getClassName(_thisClass)).." - ["..tostring(self.PID).."] - "..tostring(warning))
end