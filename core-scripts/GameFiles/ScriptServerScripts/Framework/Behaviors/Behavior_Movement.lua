--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
--
-- Behavior_Movement.lua
--
-- moveTo(self, newX, newY, newZ)
-- chase(self, PID/PlayerID)
-- stop(self)
-- pause(self)
-- play(self)

----------------------------------------
include("Lib_Vecmath.lua")

-----------------------------------------------------------------
-- Bind global libraries to local variables for efficiency's sake
-----------------------------------------------------------------
local Math = _G.Math

-----------------------------------------------------------------
-- Behaviour Specification
-----------------------------------------------------------------
Class.createBehavior("Behavior_Movement")

-----------------------------------------------------------------
-- Static variables
-----------------------------------------------------------------
local s_tick = {}
local s_moveTimers = {} -- Table of objects in motion {PID:timerID}
local s_rotateTimers = {} -- Table of objects rotating {PID:timerID}
local s_buffer = {}
local s_offset = {}

----------------------------------------
-- Private function
----------------------------------------

-- Chases designated target
local function chaseTarget(self)
	-- Do we even need this function anymore?
	if type(self.chaseTarget) == "userdata" then
		if self.inMotion then
			if s_offset[self.PID] then
				s_offset[self.PID].targetVector = {x = self.chaseTarget.position.rx, y = self.chaseTarget.position.ry, z = self.chaseTarget.position.rz}
			end

			moveTo(self, {x = self.chaseTarget.position.x, y = self.chaseTarget.position.y, z = self.chaseTarget.position.z, lookAt = true})
		end
	end	
end

-- Flees designated target
local function fleeTarget(self)
	-- Do we even need this function anymore?
	if type(self.fleeTarget) == "userdata" then
		if self.inMotion then
			local xDiff = self.fleeTarget.position.x - self.position.x
			local zDiff = self.fleeTarget.position.z - self.position.z

			local destX = self.position.x - xDiff
			local destY = self.position.y
			local destZ = self.position.z - zDiff

			moveTo(self, {x = destX, y = destY, z = destZ, lookAt = true})
		end
	end	
end

-- Called when a movement is completed
function movementComplete(self)
	if s_moveTimers[self.PID] == nil then return end

	-- Clear this PID from our pending timer
	if s_moveTimers[self.PID] then
		s_moveTimers[self.PID] = nil
	end
	-- Set current position to destination
	self.position.x  = self.destination.x
	self.position.y  = self.destination.y
	self.position.z  = self.destination.z
	self.position.rx = self.destination.rx
	self.position.ry = self.destination.ry
	self.position.rz = self.destination.rz
	
	-- Stop movement queues if we're not in chase mode
	self.timeToDestination = 0
	if self.chaseTarget == 0 and self.fleeTarget == 0 then
		self.inMotion = false
		s_buffer[self.PID] = 0
	end
	
	-- Call movement complete event
	if self.onMovementArrived then
		self:onMovementArrived()
	else
		-- warn
	end
end

-- Called when a rotation is completed
function rotateComplete(self)
	-- Clear this PID from our pending timer
	if s_rotateTimers[self.PID] then
		s_rotateTimers[self.PID] = nil
	end
	
	-- Call movement complete event
	if self.onRotateComplete then
		self:onRotateComplete()
	else
		-- warn
	end
end

----------------------------------------
-- init
----------------------------------------

-- Setup behavior for included class
function setup(self)
	-- Register for timer events
	Events.registerHandler("TIMER", onTimer, self)
	s_buffer[self.PID] = 0
end

function cleanup(self)
	Events.unregisterHandler("TIMER", onTimer, self)
end

----------------------------------------
-- Event handler
----------------------------------------

-- Calld every server tick
function onTimer(self, event)
	if self.onTimer then
		self:onTimer(event)
	end
	
	if s_tick[self.PID] == nil then
		s_tick[self.PID] = event.tick
		return
	end

	local tickDiff = event.tick - s_tick[self.PID]

	if self.inMotion then
		if self.chaseTarget ~= 0 then
			chaseTarget(self)
		elseif self.fleeTarget ~= 0 then
			fleeTarget(self)
		end
		
		-- Update position
		updatePosition(self, tickDiff)

		-- Subtract from timeToDestination
		self.timeToDestination = self.timeToDestination - tickDiff
	end

	s_tick[self.PID] = event.tick
end

----------------------------------------
-- Local functions
----------------------------------------

function updatePosition(self, tickDiff)
	if self.timeToDestination ~= nil and self.timeToDestination > 0 then
		self.position = {
			x  = self.position.x + tickDiff * ( (self.destination.x - self.position.x) / self.timeToDestination),
			y  = self.position.y + tickDiff * ( (self.destination.y - self.position.y) / self.timeToDestination),
			z  = self.position.z + tickDiff * ( (self.destination.z - self.position.z) / self.timeToDestination),
			rx = self.position.rx,
			ry = self.position.ry,
			rz = self.position.rz
		}
	else
		self.position.x  = self.destination.x
		self.position.y  = self.destination.y
		self.position.z  = self.destination.z
		self.position.rx = self.destination.rx
		self.position.ry = self.destination.ry
		self.position.rz = self.destination.rz
	end
end

local function isHorizontallyAlignedWithDest(self, destX, destZ)
	result = (((self.position.x - destX) < 0.01) and ((self.position.x - destX) > -0.01)) and (((self.position.z - destZ) < 0.01) and ((self.position.z - destZ) > -0.01))
	return result
end

-- Move method
function moveTo(self, input)	
	if self == nil then
		log("Error in Behavior_Movement::MoveTo - self is invalid")
		return
	end
	
	local newX = input.x or 0
	local newY = input.y or 0
	local newZ = input.z or 0
	self.lookAt = false
	if input.lookAt ~= nil then
		self.lookAt = input.lookAt
	end

	-- offset set in the properties 
	-- log("--- moveTo")
	-- log("--- ".. tostring(self.eulerRotY))
	-- local offsetRX, offsetRZ = Math.degreesToVector(self.eulerRotY)
	-- log("--- offsetRX ".. tostring(offsetRX))
	-- log("--- offsetRZ ".. tostring(offsetRZ))

	--Is the object's Y position locked?
	newY = self.lockYPosition and self.position.y or input.y

	if input.buffer then s_buffer[self.PID] = input.buffer end
	if input.offset then s_offset[self.PID] = input.offset end

	-- Unregister from update events from the old target
	local distanceToNode = self:getDistanceToPoint({x = newX, y = newY, z = newZ})
	
	-- Only move if outside the buffer
	if distanceToNode > s_buffer[self.PID] and distanceToNode ~= 0 and self.speed ~= 0 then

		if s_offset[self.PID] and s_offset[self.PID].targetVector then
			local zVector = s_offset[self.PID].targetVector
			local xVector = crossProduct(s_offset[self.PID].targetVector)

			newX = newX + xVector.x * s_offset[self.PID].x
			newZ = newZ + zVector.x * s_offset[self.PID].x
			newX = newX + xVector.z * s_offset[self.PID].z
			newZ = newZ + zVector.z * s_offset[self.PID].z
		end

		local totalDistance = distanceToNode - s_buffer[self.PID]
		local distanceChange = totalDistance/distanceToNode
		local xDiff = newX - self.position.x
		local yDiff = newY - self.position.y
		local zDiff = newZ - self.position.z
		local newX = self.position.x + (xDiff * distanceChange)
		local newY = self.position.y + (yDiff * distanceChange)
		local newZ = self.position.z + (zDiff * distanceChange)
		
		--Ankit ----- taking easeIn and easeOut into consideration
		local totalDur = 0
		local accDecDur = 0
		local constVelDur = 0
		local accDecDistFrac = 0
		local constVelDistFrac = 0
		if not input.easeIn and not input.easeOut then
			totalDur = distanceToNode / self.speed
			accDecDur = 0
			constVelDur = totalDur
			accDecDistFrac = 0
			constVelDistFrac = 1
		else
			local easeInOutMultiplier = (input.easeIn and 1 or 0) + (input.easeOut and 1 or 0)
			local accDecDist = (easeInOutMultiplier * self.speed * self.speed) / (2 *  input.acceleration)
			if accDecDist >= distanceToNode then
				local acc = (easeInOutMultiplier * self.speed * self.speed) / (2 *  distanceToNode)
				accDecDur = self.speed / acc
				constVelDur = 0
				accDecDistFrac = 1
				constVelDistFrac = 0
				totalDur = easeInOutMultiplier * accDecDur
				if not input.easeOut then
					accDecDur = 0
					accDecDistFrac = 0
				end
			else
				accDecDur = self.speed / input.acceleration
				accDecDistFrac = accDecDist / distanceToNode
				constVelDistFrac = 1 - accDecDistFrac
				constVelDur = constVelDistFrac * distanceToNode / self.speed
				totalDur = constVelDur + (easeInOutMultiplier * accDecDur)
				if not input.easeOut then
					accDecDur = 0
					accDecDistFrac = 0
				end
			end
			accDecDistFrac = accDecDistFrac / easeInOutMultiplier
		end
		totalDur = totalDur * 1000
		accDecDur = accDecDur * 1000
		constVelDur = constVelDur * 1000

		self.timeToDestination = totalDur

		local newRX, newRZ
		-- Face object in question if prompted
		-- Don't rotate if there is no x or z change
		if self.lookAt and (isHorizontallyAlignedWithDest(self, newX, newZ) == false) then
			-- Rotate object in the direction he'll be moving
			if s_rotateTimers[self.PID] then Events.cancelTimer(s_rotateTimers[self.PID]) end
			local tempRY = math.atan2( (newZ - self.position.z), (newX - self.position.x) ) * (180 / math.pi) - 180
			-- log("--- tempRY ".. tostring(tempRY))
			local tempRX, tempRZ = Math.degreesToVector(tempRY + (self.eulerRotY or 0))

			tempRX = tempRX
			tempRZ = tempRZ

			kgp.objectMove(self.PID, 1, 0, 1, 0, 1, self.position.x, self.position.y, self.position.z, tempRX, 0, tempRZ)
			self.position.rx = tempRX
			self.position.rz = tempRZ
			self.destination.rx = tempRX
			self.destination.rz = tempRZ
		else
			newRX, newRZ = Math.degreesToVector(Math.vectorToDegrees(input.rx or 0, input.rz or 0) + (self.eulerRotY or 0))
		end

		-- Does this object already have a movement timer pending?
		if s_moveTimers[self.PID] then Events.cancelTimer(s_moveTimers[self.PID]) s_moveTimers[self.PID] = nil end
		-- Move the object to this new location
		s_moveTimers[self.PID] = Events.setTimer((self.timeToDestination/1000), movementComplete, self)
		
		if newRX and newRZ then
			-- Does this object already have a movement timer pending?
			if s_rotateTimers[self.PID] then Events.cancelTimer(s_rotateTimers[self.PID]) end
			s_rotateTimers[self.PID] = Events.setTimer(self.timeToDestination/1000, rotateComplete, self)
			kgp.objectMove(self.PID, self.timeToDestination, accDecDur, constVelDur, accDecDistFrac, constVelDistFrac, newX, newY, newZ, newRX, 0, newRZ)
			self.destination = {x = newX, y = newY, z = newZ, rx = newRX, ry = self.position.ry, rz = newRZ}
		else
			kgp.objectMove(self.PID, self.timeToDestination, accDecDur, constVelDur, accDecDistFrac, constVelDistFrac, newX, newY, newZ)
			self.destination = {x = newX, y = newY, z = newZ, rx = self.destination.rx, ry = self.destination.ry, rz = self.destination.rz}
		end

		self.inMotion = true
	else
		if self.behavior and string.sub(self.behavior, -6, -1) == "_rider" and distanceToNode < 0.5 and self.speed ~= 0 then
			self.destination = {x = newX, y = newY, z = newZ, rx = self.position.rx, ry = self.position.ry, rz = self.position.rz}
			s_moveTimers[self.PID] = Events.setTimer(1, movementComplete, self)
		end
	end
end

function crossProduct( v )
	local upVector = {x = 0, y = 1, z = 0}
	return { x = v.y*upVector.z - v.z*upVector.y, y = v.z*upVector.x - v.x*upVector.z, z = v.x*upVector.y - v.y*upVector.x }
end

-- Rotates an object
function rotateBy(self, input)	
	local angle = input.angle or 0
	local time = input.time or 0
	--Console.Write(Console.INFO, "ROTATEBY: self.position.rx = "..tostring(self.position.rx)..", self.position.rz = "..tostring(self.position.rz))
	local newAngle = Math.vectorToDegrees(self.position.rz, self.position.rx) - angle
	kgp.objectRotate(self.PID, time*1000, 0, newAngle, 0)
	local newRX, newRZ = Math.degreesToVector(newAngle + (self.eulerRotY or 0))
	self.position.rx = newRX
	self.position.rz = newRZ
	
	-- Does this object already have a movement timer pending?
	if s_rotateTimers[self.PID] then
		Events.cancelTimer(s_rotateTimers[self.PID])
	end
	
	-- Add object to rotate event queue
	if time > 0 then
		s_rotateTimers[self.PID] = Events.setTimer(time, rotateComplete, self)
	else
		rotateComplete(self)
	end
end

-- Will chase an object until prompted to do otherwise
function chase(self, input)
	local target   = input.target
	--local targetSD = input.targetSD
	local buffer   = input.buffer
	local offset   = input.offset

	if buffer then s_buffer[self.PID] = buffer end
	if offset then s_offset[self.PID] = offset end
	
	-- Did we get a proper object?
	if target.position then
		self.chaseTarget = target
		chaseTarget(self)
	end
	self.inMotion = true
end

-- Will flee an object until prompted to do otherwise
function flee(self, input)
	local target   = input.target
	local offset   = input.offset

	if offset then s_offset[self.PID] = offset end
	
	-- Did we get a proper object?
	if target.position then
		self.fleeTarget = target
		fleeTarget(self)
	end
	self.inMotion = true
end

-- Call to completely clear a movement request
function stop(self)
	-- Does this object already have a movement timer pending?
	if s_moveTimers[self.PID] then
		Events.cancelTimer(s_moveTimers[self.PID])
		s_moveTimers[self.PID] = nil
	end
	
	-- Clear destination
	self.destination = {
		x  = self.position.x,
		y  = self.position.y,
		z  = self.position.z,
		rx = self.position.rx, 
		ry = self.position.ry,
		rz = self.position.rz
	}
	
	self.inMotion = false

	-- offset set in the properties 
	local offsetRX, offsetRZ = Math.degreesToVector(self.eulerRotY or 0)

	-- Bounce object to current known position
	kgp.objectMove(self.PID, self.timeToDestination, 0, self.timeToDestination, 0, 1, self.position.x, self.position.y, self.position.z, self.position.rx, 0, self.position.rz)
	self.timeToDestination = 0
	
	-- Were we chasing someone?
	if self.chaseTarget ~= 0 then
		self.chaseTarget = 0
	end
	
	if self.fleeTarget ~= 0 then
		self.fleeTarget = 0
	end
	
	s_buffer[self.PID] = 0
	s_offset[self.PID] = nil
end

-- Pauses movement without clearing everything out
function pause(self)
	-- Does this object already have a movement timer pending?
	if s_moveTimers[self.PID] then
		-- Cancel it
		Events.cancelTimer(s_moveTimers[self.PID])
		s_moveTimers[self.PID] = nil
	end
	kgp.objectMove(self.PID, 0, 0, 0, 0, 1, self.position.x, self.position.y, self.position.z, self.position.rx, self.position.ry, self.position.rz) 
	self.inMotion = false
end

-- Resumes movement
function play(self)
	if self.chaseTarget ~= 0 or self.fleeTarget ~= 0 then
		self.inMotion = true
	else
		moveTo(self, {x = self.destination.x, y = self.destination.y, z = self.destination.z, rx = self.destination.rx, ry = self.destination.ry, rz = self.destination.rz, lookAt = self.lookAt,
					  easeIn = self.easeIn, easeOut = self.easeOut, acceleration = self.acceleration})
	end
end

-- Teleports an obejct to a given location
function teleport(self, input)
	-- offset set in the properties 
	-- local offsetRX, offsetRZ = Math.degreesToVector(self.eulerRotY)

	input.rx, input.rz = Math.degreesToVector(Math.vectorToDegrees(input.rx or 0, input.rz or 0) + (self.eulerRotY or 0))
	
	self.position.x  = input.x or 0
	self.position.y  = input.y or 0
	self.position.z  = input.z or 0
	self.position.rx = input.rx or self.position.rx
	self.position.ry = input.ry or self.position.ry
	self.position.rz = input.rz or self.position.rz

	kgp.objectMove(self.PID, 0, 0, 0, 0, 1, self.position.x, self.position.y, self.position.z, self.position.rx, self.position.ry, self.position.rz)
	
	-- Was this object moving when you teleported it?
	if self.inMotion then
		-- If you're chasing a target, rechase them
		if self.chaseTarget ~= 0 then
			chaseTarget(self)
		elseif self.fleeTarget ~= 0 then
			fleeTarget(self)
		-- If we were in motion somewhere, requeue that motion
		elseif self.inMotion then
			moveTo(self, {x = self.destination.x, y = self.destination.y, z = self.destination.z})
		end
	end
end


