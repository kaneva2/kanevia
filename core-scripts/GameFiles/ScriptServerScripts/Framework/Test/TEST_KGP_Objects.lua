--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- kgp object APIs

TEST("Interface", function(test)
	test:assertType(kgp, "table")
	test:expectMethod(kgp, "objectGenerate")
	test:expectMethod(kgp, "objectMove")
	test:expectMethod(kgp, "objectRotate")
	test:expectMethod(kgp, "objectScale")
	test:expectMethod(kgp, "objectSetDrawDistance")
	test:expectMethod(kgp, "objectGetId")
	test:expectMethod(kgp, "objectGetStartLocation")
	test:expectMethod(kgp, "objectSendMessage")
	test:expectMethod(kgp, "objectDelete")
	test:expectMethod(kgp, "objectMapEvents")
	test:expectMethod(kgp, "objectSetTexture")
	test:expectMethod(kgp, "objectSetAnimation")
	test:expectMethod(kgp, "objectSetScript")
	test:expectMethod(kgp, "objectSetParticle")
	test:expectMethod(kgp, "objectSetParticleForPlayer")
	test:expectMethod(kgp, "objectRemoveParticle")
	test:expectMethod(kgp, "objectRemoveParticleForPlayer")
	test:expectMethod(kgp, "objectSetFriction")
	test:expectMethod(kgp, "objectSetTexturePanning")
	test:expectMethod(kgp, "objectShow")
	test:expectMethod(kgp, "objectHide")
	test:expectMethod(kgp, "objectSetEffectTrack")
	test:expectMethod(kgp, "objectAddLabels")
	test:expectMethod(kgp, "objectAddLabelsForPlayer")
	test:expectMethod(kgp, "objectClearLabels")
	test:expectMethod(kgp, "objectClearLabelsForPlayer")
	test:expectMethod(kgp, "objectSetOutline")
	test:expectMethod(kgp, "objectSetFlash")
	test:expectMethod(kgp, "objectGetData")
	test:expectMethod(kgp, "objectSetCollision")
	test:expectMethod(kgp, "objectLookAt")
	test:expectMethod(kgp, "objectGetNextPlayListItem")
	test:expectMethod(kgp, "objectSetNextPlayListItem")
	test:expectMethod(kgp, "objectEnumeratePlayList")
	test:expectMethod(kgp, "objectSetPlayList")
	test:expectMethod(kgp, "objectSetMouseOver")
	test:expectMethod(kgp, "objectGetGameState")
end)

---------------------------------------------------------------------
-- Assets
KAN_ANIM_GLIDs_ALL = { {glid=1, ms=1430}, {glid=99, ms=13}, {glid=170, ms=0}, {glid=208, ms=2475}, {glid=541, ms=15840} }
UGC_ANIM_GLIDs_DEV = { {glid=3318323, ms=1865}, {glid=3330665, ms=2000}, {glid=3315458, ms=8647}, {glid=3330873, ms=22000}, {glid=3461810, ms=1} }
UGC_ANIM_GLIDs_PRO = UGC_ANIM_GLIDs_DEV -- for now, until duration data are available in production 

local gameId = kgp.gameGetCurrentGameId() -- calling API at loading phase: non-blocking API only
KAN_ANIM_GLIDs = KAN_ANIM_GLIDs_ALL
if gameId == 5316 then
    UGC_ANIM_GLIDs = UGC_ANIM_GLIDs_DEV
else
    UGC_ANIM_GLIDs = UGC_ANIM_GLIDs_PRO
end

---------------------------------------------------------------------
-- Test kgp.getEffectDuration
TEST("getEffectDuration-KanevaAnimations", function(test)
    test:assertMethod(kgp, "getEffectDuration")
    for _, anim in ipairs(KAN_ANIM_GLIDs) do
        test:expectEQ(kgp.getEffectDuration(anim.glid), anim.ms)
    end
end)

TEST("getEffectDuration-UGCAnimations", function(test)
    test:assertMethod(kgp, "getEffectDuration")
    for _, anim in ipairs(UGC_ANIM_GLIDs) do
        test:expectEQ(kgp.getEffectDuration(anim.glid), anim.ms)
    end
end)

---------------------------------------------------------------------
-- Test kgp.objectSetAnimation elapse callbacks
TEST("objectSetAnimation-Setup", function(test)
    test:assertGE(UGC_ANIM_GLIDs[1].ms, 100) -- make sure the animation is long enough
    test:assertGT(UGC_ANIM_GLIDs[2].ms, UGC_ANIM_GLIDs[1].ms + 100) -- make sure the 2nd animation is long enough

    test.data.anim1 = UGC_ANIM_GLIDs[1]
    test.data.anim2 = UGC_ANIM_GLIDs[2]

    test:assertMethod(kgp, "objectSetAnimation")
    test:assertMethod(kgp, "objectGenerate")
    local PID = kgp.objectGenerate(GLID_SINGULARITY, 0, 0, 0, 0, 0, 1)
    test.data.PID = PID
    test:assertGT(test.data.PID, 0)

    test.data.kgp_elapsed_backup = _G.kgp_elapsed
    _G.kgp_elapsed = function(objectPID, effectType, duration, effectGLID) 
        test:log("kgp_elasped - PID=" .. tostring(objectPID) .. ", type=" .. tostring(effectType) .. ", duration=" .. tostring(duration) .. ", effectGLID=" .. tostring(effectGLID))
        local expectedEffectGLID = test.data.animInProg and test.data.animInProg.glid
        if effectGLID ~= expectedEffectGLID then
            test:log("kgp_elasped effect GLID mismatch - expect " .. tostring(expectedEffectGLID) .. ", got" .. tostring(effectGLID))
        end
        test.data.animInProg = nil 
    end
end)

-- Basic test
TEST("objectSetAnimation-Basic-Start", function(test)
    test:assertGT(test.data.PID, 0)

    kgp.objectSetAnimation(test.data.PID, test.data.anim1.glid, 0, true)
    test.data.animInProg = test.data.anim1
    return test.data.anim1.ms / 2   -- tell test controller to wait for this amount of time before executing next test
end)

TEST("objectSetAnimation-Basic-HalfTime", function(test)
    test:assertGT(test.data.PID, 0)

    test:expectTrue(test.data.animInProg == test.data.anim1)
    return test.data.anim1.ms / 2 + 100   -- tell test controller to wait for this amount of time before executing next test
end)

TEST("objectSetAnimation-Basic-Elapsed", function(test)
    test:assertGT(test.data.PID, 0)

    test:expectNil(test.data.animInProg)
end)

-- Overlap test
TEST("objectSetAnimation-TwoAnims-Start", function(test)
    test:assertGT(test.data.PID, 0)

    kgp.objectSetAnimation(test.data.PID, test.data.anim1.glid, 0, true)
    test.data.animInProg = test.data.anim1
    return test.data.anim1.ms / 2   -- tell test controller to wait for this amount of time before executing next test
end)

TEST("objectSetAnimation-TwoAnims-HalfTime1-ChangeAnim", function(test)
    test:assertGT(test.data.PID, 0)

    test:expectTrue(test.data.animInProg == test.data.anim1)

    kgp.objectSetAnimation(test.data.PID, test.data.anim2.glid, 0, true)
    test.data.animInProg = test.data.anim2
    return test.data.anim2.ms / 2   -- tell test controller to wait for this amount of time before executing next test
end)

TEST("objectSetAnimation-TwoAnims-HalfTime2", function(test)
    test:assertGT(test.data.PID, 0)

    test:expectTrue(test.data.animInProg == test.data.anim2)
    return test.data.anim2.ms / 2 + 100   -- tell test controller to wait for this amount of time before executing next test
end)

TEST("objectSetAnimation-TwoAnims-Elapsed", function(test)
    test:assertGT(test.data.PID, 0)

    test:expectNil(test.data.animInProg)  
end)

-- Animation callback cancellation
TEST("objectSetAnimation-Cancel-Start", function(test)
    test:assertGT(test.data.PID, 0)

    kgp.objectSetAnimation(test.data.PID, test.data.anim1.glid, 0, true)
    test.data.animInProg = test.data.anim1
    return test.data.anim1.ms / 2   -- tell test controller to wait for this amount of time before executing next test
end)

TEST("objectSetAnimation-Cancel-HalfTime-Cancel", function(test)
    test:assertGT(test.data.PID, 0)

    -- Validate current in-progress animation
    test:expectTrue(test.data.animInProg == test.data.anim1)

    -- Cancel animation callback by setting another animation without callback
    kgp.objectSetAnimation(test.data.PID, test.data.anim2.glid)
    return test.data.anim2.ms / 2 + 100   -- tell test controller to wait for this amount of time before executing next test
end)

TEST("objectSetAnimation-Cancel-Elapsed", function(test)
    test:assertGT(test.data.PID, 0)

    -- Second animation elapsed - animInProg should stay the old value because callback never occurred
    test:expectTrue(test.data.animInProg == test.data.anim1)
    test.data.animInProg = nil
end)

TEST("objectSetAnimation-Teardown", function(test)
    _G.kgp_elapsed = test.data.kgp_elapsed_backup

    test:assertGT(test.data.PID, 0)
    test:assertMethod(kgp, "objectDelete")
    kgp.objectDelete(test.data.PID)
    test.data.PID = nil
end)

---------------------------------------------------------------------
