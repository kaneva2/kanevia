--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- Framework_Test.lua
-- Automates test of entire framework
-- on a element-by-element basis
--
-- Merged in Framework_Test_Hub logic - YC 03/2015
----------------------------------------

kgp.scriptDoFile("Lib_Test.lua")

-- Load test manifest
kgp.scriptDoFile("TEST.lua")

TestManifest.debug()

local function log(message)
	kgp.gameLogMessage("Framework_Test: " .. tostring(message))
end

local function processEvent(eventType, encoded, handler)
	local event, err = cjson.decode(encoded or "")
	if event==nil then
		log(tostring(eventType) .. ": received invalid event data")
	else
		handler(event)
	end
end

-- Use standard kgp callbacks to avoid dependencies with Lib_Event
function kgp_menuevent(user, params)
	log("received " .. tostring(params[1]) .. ", from client: " .. tostring(user))
	if params[1]=="FRAMEWORK_TEST_INIT" then

		-- Client requests test initializations
			log("FRAMEWORK_TEST_INIT")
			local progress, msg = TestManifest.acquire(user)
			if progress then
				-- No need to launch test hub any more. All test progresses are handled right here in this script through Lib_test.

				--TODO: get rid of this message and use something more meaningful
				-- Fake a test object generated event
				Events.sendClientEvent("FRAMEWORK_TEST_OBJECT_GENERATED", {PID = 0}, user)

				--FRAMEWORK_TEST_HUB_INIT is no longer needed - we are now sending manifest to client through TestManifest.acquire
			else
				log("FRAMEWORK_TEST_INIT error: " .. tostring(msg))
			end

	elseif params[1]=="FRAMEWORK_TEST_CLEANUP" then

		-- Client requests test cleanup
			log("FRAMEWORK_TEST_CLEANUP")
			local res, msg = TestManifest.release(user)
			if not res then
				log("FRAMEWORK_TEST_CLEANUP error: " .. tostring(msg))
			end

	elseif params[1]=="BEGIN_TEST" then

		-- Client requests test to be started
		processEvent(params[1], params[2], function(event)
				local test = event.test
				local progress = TestManifest.getProgress(user)

				-- Test all?
				if test == "All" then
					log("Testing All")
					g_testing = "All"
					progress:rewind()
					progress:testNext()

				-- Test an individual class?
				else
					if progress:seek(nil, test) then
						log("Testing " .. tostring(test))
						g_testing = test
						progress:testOne()
					end
				end
			end)

	end
end

function kgp_message( message, sender )
	log("Test controller received " .. tostring(message) .. ", from container: " .. tostring(sender))

	local testOwner = TestContainer.checkTestCaseCompletionMessage(message)
	if testOwner~=nil then
		local progress = TestManifest.getProgress(testOwner)
		if progress==nil then
			log("Framework_Test: No active test progress found for NETID " .. tostring(testOwner))
			--TODO: notify client
			return
		end

		if g_testing == "All" then
			-- Run next test case
			if not progress:testNext() then
				-- not more tests
				progress:complete()
			end
		else
			-- Single test case: completed
			progress:complete()
		end
	end
end

function kgp_arrive(user)
	log("kgp_arrive " .. tostring(user))
	local perm = kgp.playerGetPermissions(user)
	if perm == 1 then
		TestManifest.addTester(user)
	end
end

function kgp_depart(user)
	log("kgp_depart " .. tostring(user))
	local perm = kgp.playerGetPermissions(user)
	if perm == 1 then
		TestManifest.removeTester(user)
	end
end
