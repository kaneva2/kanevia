--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_vendor.lua
-- Tests all Vendor methods
----------------------------------------

Class.importClass("Class_Vendor")

local NAME = "Vendor"

local s_vendor = ""

-- Node Objects
Vendor = {}
Vendor.__index = Vendor

-- Creates a node object with the given PID
function Vendor.create()
	local vendor = {}
	vendor.name = NAME
	vendor.totalTests = 11
	setmetatable(vendor, Vendor)
	
	return vendor
end

function Vendor.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default Vendor class")
	s_vendor = Class_Vendor.new()
	
	self.log("Vendor instantiated ["..tostring(s_vendor).."] \n")
	
	-- Test onLeftClick
	self.log(""); self.log("Testing onLeftClick")
	s_vendor:onLeftClick({player = g_testerSD})
	self.logResult(true, "onLeftClick")
	self.log("onLeftClick TEST COMPLETE")
	
	-- Test onShopPurchase
	self.log("")
	self.log("Testing onShopPurchase")
	s_vendor:onShopPurchase({identifier = s_vendor.PID, player = g_testerSD, itemId = 1})
	self.logResult(true, "onShopPurchase")
	self.log("onShopPurchase TEST COMPLETE")
	
	-- Test onMenuClosed
	self.log("")
	self.log("Testing onMenuClosed")
	s_vendor:onMenuClosed({identifier = s_vendor.PID})
	self.logResult(true, "onMenuClosed")
	self.log("onMenuClosed TEST COMPLETE")
	
	-- Test onPlayerArrive
	self.log("")
	self.log("Testing onPlayerArrive")
	s_vendor:onPlayerArrive({player = g_testerSD})
	self.logResult(true, "onPlayerArrive")
	self.log("onPlayerArrive TEST COMPLETE")
	
	-- Test onDepart
	self.log("")
	self.log("Testing onDepart")
	s_vendor:onDepart({player = g_testerSD})
	self.logResult(true, "onDepart")
	self.log("onDepart TEST COMPLETE")
	
	-- Test onPlayerDepart
	self.log("")
	self.log("Testing onPlayerDepart")
	s_vendor:onPlayerDepart({player = g_testerSD})
	self.logResult(true, "onPlayerDepart")
	self.log("onPlayerDepart TEST COMPLETE")
	
	-- Test onGetPlayerList
	self.log("")
	self.log("Testing onGetPlayerList")
	s_vendor:onGetPlayerList({map = {player = g_testerSD}})
	self.logResult(true, "onGetPlayerList")
	self.log("onGetPlayerList TEST COMPLETE")
	
	-- Test sortItemListByCost
	self.log("")
	self.log("Testing sortItemListByCost")
	local sortedList = s_vendor:sortItemListByCost({doop = {itemCost = 3},
													poop = {itemCost = 1},
													scoop = {itemCost = 2}
												   })
	if sortedList[1].itemCost == 1 and sortedList[2].itemCost == 2 and sortedList[3].itemCost == 3 then
		self.logResult(true, "sortItemListByCost")
	else
		local error = "Failed to sort: sortedList = ["..tostring(sortedList[1].itemCost)..", "..tostring(sortedList[2].itemCost)..", "..tostring(sortedList[3].itemCost).."]"
		self.logResult(false, "sortItemListByCost", error)
	end
	self.log("sortItemListByCost TEST COMPLETE")
	
	s_vendor.itemList = {{itemCurrency = "coins"}}
	
	-- Test checkCoinHUD
	self.log("")
	self.log("Testing checkCoinHUD")
	s_vendor:checkCoinHUD(g_tester)
	self.logResult(true, "checkCoinHUD")
	self.log("checkCoinHUD TEST COMPLETE")
	
	-- Test checkXPHUD
	self.log("")
	self.log("Testing checkXPHUD")
	s_vendor:checkXPHUD(g_tester)
	self.logResult(true, "checkXPHUD")
	self.log("checkXPHUD TEST COMPLETE")
	
	-- Test checkHealthHUD
	self.log("")
	self.log("Testing checkHealthHUD")
	s_vendor:checkHealthHUD(g_tester)
	self.logResult(true, "checkHealthHUD")
	self.log("checkHealthHUD TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Vendor.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Vendor.logResult(result, funcName, err)
	if result then Vendor.log("PASS")
	else Vendor.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end