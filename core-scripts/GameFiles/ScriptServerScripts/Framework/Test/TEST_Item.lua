--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_item.lua
-- Tests all item methods
----------------------------------------

Class.importClass("Class_Item")

local NAME = "Item"

local s_item = ""

-- Node Objects
Item = {}
Item.__index = Item

-- Creates a node object with the given PID
function Item.create()
	local item = {}
	item.name = NAME
	item.totalTests = 0
	setmetatable(item, Item)
	
	return item
end

function Item.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default item class")
	s_item = Class_Character.new()
	
	self.log("Item instantiated ["..tostring(s_item).."]")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Item.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Item.logResult(result, funcName, err)
	if result then Item.log("PASS")
	else Item.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end