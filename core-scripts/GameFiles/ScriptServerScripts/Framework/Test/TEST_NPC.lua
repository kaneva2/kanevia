--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_npc.lua
-- Tests all npc methods
----------------------------------------

Class.importClass("Class_NPC")

local NAME = "NPC"

local s_npc = ""

-- Node Objects
NPC = {}
NPC.__index = NPC

local STATE = {IDLE = 0}

-- Creates a node object with the given PID
function NPC.create()
	local npc = {}
	npc.name = NAME
	npc.totalTests = 10
	setmetatable(npc, NPC)
	
	return npc
end

function NPC.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default monster class")
	s_npc = Class_NPC.new()
	
	self.log("NPC instantiated ["..tostring(s_npc).."] \n")
	
	-- Test onLeftClick
	self.log("")
	self.log("Testing onLeftClick")
	s_npc:onLeftClick({player = g_testerSD})
	self.logResult(true, "onLeftClick")
	self.log("onLeftClick TEST COMPLETE")
	
	-- Test onTimer
	self.log("")
	self.log("Testing onTimer")
	s_npc:onTimer()
	self.logResult(true, "onTimer")
	self.log("onTimer TEST COMPLETE")
	
	-- Test onServerTick
	self.log("")
	self.log("Testing onServerTick")
	s_npc:onServerTick()
	self.logResult(true, "onServerTick")
	self.log("onServerTick TEST COMPLETE")
	
	-- Test speak
	self.log("")
	self.log("Testing speak")
	s_npc:speak(g_tester)
	self.logResult(true, "speak")
	self.log("speak TEST COMPLETE")
	
	-- Test handleState
	self.log("")
	self.log("Testing handleState")
	for id, state in pairs(STATE) do
		s_npc:handleState(state)
	end
	self.logResult(true, "handleState")
	self.log("handleState TEST COMPLETE")
	
	-- Test handleIdleState
	self.log("")
	self.log("Testing handleIdleState")
	s_npc:handleIdleState()
	self.logResult(true, "handleIdleState")
	self.log("handleIdleState TEST COMPLETE")
	
	-- Test enterIdleState
	self.log("")
	self.log("Testing enterIdleState")
	s_npc:enterIdleState()
	self.logResult(true, "enterIdleState")
	self.log("enterIdleState TEST COMPLETE")
	
	-- spawnCorpse
	self.log("")
	self.log("Testing spawnCorpse")
	s_npc:spawnCorpse()
	self.logResult(true, "spawnCorpse")
	self.log("spawnCorpse TEST COMPLETE")
	
	-- Test die
	self.log("")
	self.log("Testing die")
	s_npc:die()
	self.logResult(true, "die")
	self.log("die TEST COMPLETE")
	
	-- Test respawn
	self.log("")
	self.log("Testing respawn")
	s_npc:respawn()
	self.logResult(true, "respawn")
	self.log("respawn TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function NPC.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function NPC.logResult(result, funcName, err)
	if result then NPC.log("PASS")
	else NPC.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end