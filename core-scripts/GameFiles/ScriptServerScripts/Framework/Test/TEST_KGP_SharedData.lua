--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Script server shared data API test

TEST( "Interface", function( test )
	test:assertType(kgp, "table")
	test:expectMethod(kgp, "dictGenerate")
	test:expectMethod(kgp, "dictClearValues")
	test:expectMethod(kgp, "dictGetInfo")
	test:expectMethod(kgp, "dictGetAllKeys")
	test:expectMethod(kgp, "dictIterate")
end)

local testName = "playerTest.chirho"
local testValues = { health = 12, title = "qwerty jammer", alive = true, position = { x=3, y=4, z=5 } }
local testName2 = "playerTest.gnoph"

TEST( "DictionaryQueries", function( test )
	local player, name, refCount

	test:log("[........] kgp.dictGenerate(dict=" .. testName .. ")")
	player = kgp.dictGenerate(testName)
	test:assertType(player, "userdata")

	test:log("[........] kgp.dictGetInfo()")
	name, refCount = kgp.dictGetInfo(player)
	test:expectEQ(name, testName)
	test:expectEQ(refCount, 1)

	test:log("[........] Field: health")
	test:expectUndefinedField(player, "health")
	player.health = testValues.health
	test:expectField(player, "health", testValues.health)

	test:log("[........] Field: title")
	test:expectUndefinedField(player, "title")
	player.title = testValues.title
	test:expectField(player, "title", testValues.title)

	test:log("[........] Field: alive")
	test:expectUndefinedField(player, "alive")
	player.alive = testValues.alive
	test:expectField(player, "alive", testValues.alive)

	-- Cleanup
	test:log("[........] player = nil")
	player = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()
end)

TEST( "ReferenceCounts", function( test )
	local player, name, refCount

	-- Setup
	test:log("[........] kgp.dictGenerate(dict='" .. testName .. "')")
	player = kgp.dictGenerate(testName)
	player.health = testValues.health
	player.title = testValues.title
	player.alive = testValues.alive

	test:log("[........] kgp.dictGetInfo()")
	name, refCount = kgp.dictGetInfo(player)
	test:expectEQ(refCount, 1)

	-- Additional dictGenerate will reuse existing userdata so dictionary ref count will remain the same
	test:log("[........] kgp.dictGenerate(dict='" .. testName .. "') #2")
	local player2 = kgp.dictGenerate(testName)

	test:log("[........] kgp.dictGetInfo()")
	name, refCount = kgp.dictGetInfo(player)
	test:expectEQ(refCount, 1)

	-- Local assignment affects the lua ref count for userdata but not dictionary ref count in the engine
	test:log("[........] player3 = player2")
	local player3 = player2

	test:log("[........] kgp.dictGetInfo()")
	name, refCount = kgp.dictGetInfo(player)
	test:expectEQ(refCount, 1)

	test:log("[........] player2 = nil")
	player2 = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()

	test:log("[........] kgp.dictGetInfo()")
	name, refCount = kgp.dictGetInfo(player)
	test:expectEQ(refCount, 1)

	test:log("[........] player3 = nil")
	player3 = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()

	test:log("[........] kgp.dictGetInfo()")
	name, refCount = kgp.dictGetInfo(player)
	test:expectEQ(refCount, 1)

	test:log("[........] player = nil")
	player = nil

	-- Garbage collection test: dictionary should be disposed then recreated, losing all data
	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()

	test:log("[........] kgp.dictGenerate(dict=" .. testName .. ")")
	player = kgp.dictGenerate(testName)

	test:log("[........] kgp.dictGetInfo()")
	name, refCount = kgp.dictGetInfo(player)
	test:expectEQ(refCount, 1)

	test:log("[........] Field: health")
	test:expectUndefinedField(player, "health")

	test:log("[........] Field: title")
	test:expectUndefinedField(player, "title")

	test:log("[........] Field: alive")
	test:expectUndefinedField(player, "alive")

	-- Cleanup
	test:log("[........] player = nil")
	player = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()
end)

TEST( "Iterators", function( test )
	local player, name, refCount

	test:log("[........] kgp.dictGenerate(dict='" .. testName .. "')")
	player = kgp.dictGenerate(testName)
	player.health = testValues.health
	player.title = testValues.title
	player.alive = testValues.alive

	-- Iterator test
	for k, _ in pairs(player) do
		test:log("[........] Field: " .. tostring(k))
		test:expectField(player, k, testValues[k])
	end

	-- Cleanup
	test:log("[........] player = nil")
	player = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()
end)

TEST( "ReadOnlyDictionary", function( test )
	local player

	test:log("[........] kgp.dictGenerate(dict=\"" .. testName .. "\", readonly=true)")
	player = kgp.dictGenerate(testName, true)

	test:expectUndefinedField(player, "health")

	-- No effect
	player.health = 12

	-- Still nil
	test:expectUndefinedField(player, "health")

	-- Cleanup
	test:log("[........] player = nil")
	player = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()
end)

TEST( "ChildDictionaryQueries", function( test )
	local player, name, refCount
	local positionDict

	test:log("[........] kgp.dictGenerate(dict=" .. testName .. ")")
	player = kgp.dictGenerate(testName)

	test:log("[........] kgp.dictGetInfo()")
	name, refCount = kgp.dictGetInfo(player)
	test:expectEQ(refCount, 1)

	test:log("[........] Field: position")
	test:expectUndefinedField(player, "position")

	test:log("[........] positionDict = kgp.dictGenerate(nil)")
	positionDict = kgp.dictGenerate(nil)
	test:assertType(positionDict, "userdata")

	test:log("[........] kgp.dictGetInfo(positionDict)")
	name, refCount = kgp.dictGetInfo(positionDict)
	test:expectType(name, "string")
	test:expectEQ(refCount, 1)

	test:log("[........] player.position = positionDict")
	player.position = positionDict

	test:log("[........] kgp.dictGetInfo(positionDict)")
	name, refCount = kgp.dictGetInfo(positionDict)
	test:expectEQ(refCount, 2)

	test:assertFieldType(player, "position", "userdata")

	test:log("[........] player.position.x = " .. tostring(testValues.position.x))
	player.position.x = testValues.position.x
	test:log("[........] player.position.y = " .. tostring(testValues.position.y))
	player.position.y = testValues.position.y
	test:log("[........] player.position.z = " .. tostring(testValues.position.z))
	player.position.z = testValues.position.z

	test:log("[........] Check player.position fields")
	test:expectField(player.position, "x", testValues.position.x)
	test:expectField(player.position, "y", testValues.position.y)
	test:expectField(player.position, "z", testValues.position.z)

	test:log("[........] Check positionDict fields")
	test:expectField(positionDict, "x", testValues.position.x)
	test:expectField(positionDict, "y", testValues.position.y)
	test:expectField(positionDict, "z", testValues.position.z)

	-- Cleanup
	test:log("[........] player = nil")
	player = nil

	test:log("[........] positionDict = nil")
	positionDict = nil
	
	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()
end)

TEST( "ChildDictionaryIterators", function( test )
	local player

	test:log("[........] kgp.dictGenerate(dict=" .. testName .. ")")
	player = kgp.dictGenerate(testName)
	player.health = testValues.health
	player.title = testValues.title
	player.alive = testValues.alive
	player.position = testValues.position
	test:assertFieldType(player, "position", "userdata")

	for k, v in pairs(player) do
		test:log("[........] player." .. tostring(k))
		if type(v)=="userdata" then
			test:assertType(testValues[k], "table")
			for kk, vv in pairs(v) do
				test:log("[........] player." .. tostring(k) .. "." .. tostring(kk))
				test:expectField(v, kk, testValues[k][kk])
			end
		else
			test:expectField(player, k, testValues[k])
		end
	end

	-- Cleanup
	test:log("[........] player = nil")
	player = nil
	
	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()
end)

TEST( "ChildDictionaryInterReferencing", function( test )
	local player, name, refCount
	local player2

	test:log("[........] kgp.dictGenerate(dict=" .. testName .. ")")
	player = kgp.dictGenerate(testName)
	player.position = testValues.position
	test:assertFieldType(player, "position", "userdata")
	
	-- Nested table import creates a temporary userdata on the stack. use GC to remove the reference.
	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()

	test:log("[........] player2 = kgp.dictGenerate(dict=" .. testName2 .. ")")
	player2 = kgp.dictGenerate(testName2)
	test:assertType(player2, "userdata")

	test:log("[........] player2.pos = player.position")
	player2.pos = player.position
	test:assertFieldType(player2, "pos", "userdata")

	for k, v in pairs(player2.pos) do
		test:log("[........] player2.pos." .. tostring(k))
		test:expectField(player2.pos, k, testValues.position[k])
	end

	test:log("[........] kgp.dictGetInfo(player.position)")
	name, refCount = kgp.dictGetInfo(player.position)
	test:expectEQ(refCount, 3)

	test:log("[........] kgp.dictGetInfo(player2.pos)")
	name, refCount = kgp.dictGetInfo(player2.pos)
	test:expectEQ(refCount, 3)

	test:log("[........] player2.pos.z = 100")
	player2.pos.z = 100
	test:expectField(player2.pos, "z", 100)
	test:expectField(player.position, "z", 100)

	test:log("[........] player.position.z = " .. tostring(testValues.position.z))
	player.position.z = testValues.position.z
	test:expectField(player2.pos, "z", testValues.position.z)
	test:expectField(player.position, "z", testValues.position.z)

	test:log("[........] player2.pos = nil")
	player2.pos = nil
	test:expectUndefinedField(player2, "pos")

	-- reference count should drop instantaneously
	test:log("[........] kgp.dictGetInfo(player.position)")
	name, refCount = kgp.dictGetInfo(player.position)
	test:expectEQ(refCount, 2)

	-- Cleanup
	test:log("[........] player = nil")
	player = nil

	test:log("[........] player2 = nil")
	player2 = nil
	
	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()
end)

TEST( "ChildDictionaryReferenceCounts", function( test )
	local player, childDictName, refCount

	test:log("[........] kgp.dictGenerate(dict=" .. testName .. ")")
	player = kgp.dictGenerate(testName)
	player.position = testValues.position
	test:assertFieldType(player, "position", "userdata")
	
	test:log("[........] kgp.dictGetInfo(player.position)")
	childDictName, refCount = kgp.dictGetInfo(player.position)
	test:expectEQ(refCount, 2)	-- refCount is 2 because the temporary value returned from player.position is a userdata that holds an additional reference to the dictionary

	-- make an explicit instance to the child dictionary
	test:log("[........] temp = kgp.dictGenerate(dict=" .. childDictName .. ")")
	local temp = kgp.dictGenerate(childDictName)
	test:assertType(temp, "userdata")

	-- The same userdata is returned because we cache dictionary userdata in the registry
	test:expectRefEQ(temp, player.position)

	for k, v in pairs(temp) do
		test:log("[........] temp." .. tostring(k))
		test:expectField(temp, k, testValues.position[k])
	end

	test:log("[........] kgp.dictGetInfo(player.position)")
	_, refCount = kgp.dictGetInfo(player.position)
	test:expectEQ(refCount, 2)	-- refCount is 2 because temp === player.position

	test:log("[........] temp = nil")
	temp = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()

	test:log("[........] kgp.dictGetInfo(player.position)")
	_, refCount = kgp.dictGetInfo(player.position)
	test:expectEQ(refCount, 2)	-- refCount is 2 because the temporary value returned from player.position is a userdata that holds an additional reference to the dictionary

	-- Final disposal test
	test:log("[........] CHILD-DICTIONARY FINAL DISPOSAL")

	-- Make a backup
	test:log("[........] temp = player.position")
	temp = player.position

	-- optional 1: set player.position = nil
	test:log("[........] player.position = nil")
	player.position = nil

	test:expectUndefinedField(player, "position")

	test:log("[........] kgp.dictGetInfo(temp)")
	_, refCount = kgp.dictGetInfo(temp)
	test:expectEQ(refCount, 1)	-- the only reference is temp

	-- restore player.position
	test:log("[........] player.position = temp")
	player.position = temp

	test:log("[........] kgp.dictGetInfo(temp)")
	_, refCount = kgp.dictGetInfo(temp)
	test:expectEQ(refCount, 2)	-- refCount = 2 (temp/player.position + player)

	-- option 2: set player.position to a number
	test:log("[........] player.position = 3")
	player.position = 3

	test:log("[........] kgp.dictGetInfo(temp)")
	_, refCount = kgp.dictGetInfo(temp)
	test:expectEQ(refCount, 1)	-- the only reference is temp

	-- restore player.position
	test:log("[........] player.position = temp")
	player.position = temp

	-- option 3: set player.position to a string
	test:log("[........] player.position = \"abc\"")
	player.position = "abc"

	test:log("[........] kgp.dictGetInfo(temp)")
	_, refCount = kgp.dictGetInfo(temp)
	test:expectEQ(refCount, 1)	-- the only reference is temp

	-- restore player.position
	test:log("[........] player.position = temp")
	player.position = temp

	-- option 4: set player to nil
	test:log("[........] player = nil")
	player = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()

	test:log("[........] kgp.dictGetInfo(temp)")
	_, refCount = kgp.dictGetInfo(temp)
	test:expectEQ(refCount, 1)	-- the only reference is temp

	-- now let go all of them
	test:log("[........] temp = nil")
	temp = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()

	-- child dictionary should be disposed by now
	-- test by opening it explicitly
	test:log("[........] temp = kgp.dictGenerate(dict=" .. childDictName .. ")")
	temp = kgp.dictGenerate(childDictName)

	test:expectUndefinedField(temp, "x")
	test:expectUndefinedField(temp, "y")
	test:expectUndefinedField(temp, "z")

	test:log("[........] temp = nil")
	temp = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()
end)

TEST( "NestedTables", function( test )
	local master
	local testPosition = { x = 3, y = 2, z = 1, type = "anchor" }
	local testNPC = { shirt="white", age = 6, onTrigger = function() end, colors = { red=true, blue=true } }

	local testArray = { 1, 5, 6, "Jonny", "Aaron" }

	test:log("[........] SharedData.new({\"yc\", \"dicttest\"})")
	master = SharedData.new({"yc", "dicttest"})
	test:expectType(master, "userdata")

	test:log("[........] master.name = \"chirho\"")
	master.name = "chirho"
	test:expectEQ(master.name, "chirho")

	test:log("[........] master.position = {}")
	master.position = {}
	test:expectFieldType(master, "position", "userdata")

	for k, _ in pairs(master.position) do
		-- Report an error
		test:fail("master.position is not empty (key: " .. tostring(k) .. " found)")
	end

	test:log("[........] master.position = testPosition")
	local oldPosition = master.position
	master.position = testPosition
	test:expectFieldType(master, "position", "userdata")
	test:expectRefNE(master.position, oldPosition)

	-- ASSERT master position contains accurate keys and values
	for k, _ in pairs(master.position) do
		test:expectField(master.position, k, testPosition[k])
	end

	for k, v in pairs(testPosition) do
		test:expectField(master.position, k, v)
	end

	-- Test invalid values
	test:log("[........] master.NPC = testNPC")
	master.NPC = testNPC
	test:expectFieldType(master, "NPC", "userdata")

	-- ASSERT master.NPC contains only supported keys and values
	for k, _ in pairs(master.NPC) do
		test:expectField(master.NPC, k, testNPC[k])
	end

	for k, v in pairs(testNPC) do
		if type(k)=="string" and (type(v)=="string" or type(v)=="number" or type(v)=="boolean") then
			test:expectField(master.NPC, k, v)
		else
			test:expectUndefinedField(master.NPC, k)
		end
	end

	-- child table
	test:log("[........] master.NPC.colors = testNPC.colors")
	master.NPC.colors = testNPC.colors
	test:expectFieldType(master.NPC, "colors", "userdata")

	for k, _ in pairs(master.NPC.colors) do
		test:expectField(master.NPC.colors, k, testNPC.colors[k] )
	end

	for k, v in pairs(testNPC.colors) do
		if type(k)=="string" and (type(v)=="string" or type(v)=="number" or type(v)=="boolean") then
			test:expectField(master.NPC.colors, k, v)
		else
			test:expectUndefinedField(master.NPC.colors, k)
		end
	end

	test:log("[........] master.NPC = nil")
	master.NPC = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()

	test:expectUndefinedField(master, "NPC")

	-- numeric keys
	test:log("[........] master.array = {}")
	master.array = {}
	for k, v in pairs(testArray) do
		test:log("[........] master.array[" .. tostring(k) .. "] = " .. tostring(v))
		master.array[k] = v
	end

	for k, v in pairs(master.array) do
		test:expectEQ(v, testArray[tonumber(k)])
	end

	for k, v in pairs(testArray) do
		if (type(k)=="string" or type(k)=="number") and (type(v)=="string" or type(v)=="number" or type(v)=="boolean") then
			test:expectEQ(master.array[k], v)
		else
			test:expectNil(master.array[tostring(k)])
		end
	end

	test:log("[........] master = nil")
	master = nil

	test:log("[........] force garbage collection")
	test:assertMethod(kgp, "scriptGC")
	kgp.scriptGC()
end)
