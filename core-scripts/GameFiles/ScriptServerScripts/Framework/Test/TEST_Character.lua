--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_character.lua
-- Tests all character methods
----------------------------------------

Class.importClass("Class_Character")

local NAME = "Character"

local s_character = ""

-- Node Objects
Character = {}
Character.__index = Character

-- Creates a node object with the given PID
function Character.create()
	local character = {}
	character.name = NAME
	character.totalTests = 3
	setmetatable(character, Character)
	
	return character
end

function Character.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default character class")
	s_character = Class_Character.new()
	
	self.log("Character instantiated ["..tostring(s_character).."]")
	
	-- Test onRespawn
	self.log("")
	self.log("Testing onRespawn")
	s_character:onRespawn()
	self.logResult(true, "onRespawn")
	self.log("onRespawn TEST COMPLETE")
	
	-- Test die
	self.log("")
	self.log("Testing die")
	s_character:die()
	self.logResult(true, "die")
	self.log("die TEST COMPLETE")
	
	-- Test respawn
	self.log("")
	self.log("Testing respawn")
	s_character:respawn()
	self.logResult(true, "respawn")
	self.log("respawn TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Character.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Character.logResult(result, funcName, err)
	if result then Character.log("PASS")
	else Character.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end