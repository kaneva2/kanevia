--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Script Testing Master Sheet
-- Created: YFC 03/2015

-- Usage: TestManifest.addGroup( <Group>, { <Test-Case-Def>, ... }, [ <Group-Fixture-Class> ] )
-- <Test-Case-Def>		:- <Test-Case>							# Test case with group test fixture
--						 | { <Test-Case>, <Fixture-Class> }		# Test case with custom test fixture
-- <Test-Case>			:- string								# provides the test case name
-- <Fixture-Class>		:- string								# provides the custom test fixture class name
-- <Group-Fixture-Class>:- string								# provides a class name that overrides the per-group test fixture class (default is "Test")

-- Sanity check for the test framework - only run as needed
-- TestManifest.addGroup( "STF", {
-- 	"Lib_Test",
-- })

-- Lua
TestManifest.addGroup( "Lua", {
	"Lang_Lua",
})

-- kgp APIs
TestManifest.addGroup( "APIs", {
	"KGP_Players",
	"KGP_Objects",
	"KGP_SharedData",
	"KGP_GameStates",
})

-- Core Libraries
TestManifest.addGroup( "Libraries", {
	"Lib_Events",
})

-- Controllers
TestManifest.addGroup( "Controllers", {
	-- add tests here
})

-- Classes
TestManifest.addGroup( "Classes", {
	"Actor",
	"Base",
	"Character",
--	"Collectible", -- Deprecated
	"DanceFloor",
	"InteractiveNode",
	"Item",
--	"JetPack", -- Deprecated
--	"Monster", -- Deprecated
--	"MovingObject", -- Deprecated
--	"NPC", -- Deprecated
	"Seat",
	"ShopItem",
	"SpawnPoint",
	"Teleporter",
	"Trap",
	"Vendor",
--	"Weapon", -- Deprecated
}, "TestLegacy")
