--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_teleporter.lua
-- Tests all Teleporter methods
----------------------------------------

Class.importClass("Class_Teleporter")

local NAME = "Teleporter"

local s_teleporter = ""

-- Node Objects
Teleporter = {}
Teleporter.__index = Teleporter

-- Creates a node object with the given PID
function Teleporter.create()
	local teleporter = {}
	teleporter.name = NAME
	teleporter.totalTests = 9
	setmetatable(teleporter, Teleporter)
	
	return teleporter
end

function Teleporter.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default Teleporter class")
	s_teleporter = Class_Teleporter.new()
	
	self.log("Teleporter instantiated ["..tostring(s_teleporter).."] \n")
	
	-- Test onArrive
	self.log("")
	self.log("Testing onArrive")
	s_teleporter:onArrive({player = g_testerSD})
	self.logResult(true, "onArrive")
	self.log("onArrive TEST COMPLETE")
	
	-- Test onPlayerArrive
	self.log("")
	self.log("Testing onPlayerArrive")
	s_teleporter:onPlayerArrive({player = g_testerSD})
	self.logResult(true, "onPlayerArrive")
	self.log("onPlayerArrive TEST COMPLETE")
	
	-- Test onTimer
	self.log("")
	self.log("Testing onTimer")
	s_teleporter:onTimer({tick = 1000})
	self.logResult(true, "onTimer")
	self.log("onTimer TEST COMPLETE")
	
	-- Test onServerTick
	self.log("")
	self.log("Testing onServerTick")
	s_teleporter:onServerTick({tick = 1000})
	self.logResult(true, "onServerTick")
	self.log("onServerTick TEST COMPLETE")
	
	-- Test onTrigger
	self.log("")
	self.log("Testing onTrigger")
	s_teleporter:onTrigger({player = g_testerSD, triggerType = 1})
	self.logResult(true, "onTrigger")
	self.log("onTrigger TEST COMPLETE")
	
	-- Test onMapReturn
	self.log("")
	self.log("Testing onMapReturn")
	local selfPID = s_teleporter.PID
	s_teleporter:onMapReturn({map = {type = "Player", {player = g_testerSD}}})
	s_teleporter:onMapReturn({map = {type = "teleporter", [selfPID] = {teleporterID = 1}}})
	self.logResult(true, "onMapReturn")
	self.log("onMapReturn TEST COMPLETE")
	
	-- Test requestMapByType
	self.log("")
	self.log("Testing requestMapByType")
	s_teleporter:requestMapByType({type = "teleporter", user = g_testerSD})
	self.logResult(true, "requestMapByType")
	self.log("requestMapByType TEST COMPLETE")
	
	-- Test onNotifyTeleporter
	self.log("")
	self.log("Testing onNotifyTeleporter")
	s_teleporter:onNotifyTeleporter({fromTeleporter = s_teleporter.PID})
	self.logResult(true, "onNotifyTeleporter")
	self.log("onNotifyTeleporter TEST COMPLETE")
	
	-- Test destroy
	self.log("")
	self.log("Testing destroy")
	s_teleporter:destroy()
	self.logResult(true, "destroy")
	self.log("destroy TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Teleporter.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Teleporter.logResult(result, funcName, err)
	if result then Teleporter.log("PASS")
	else Teleporter.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end