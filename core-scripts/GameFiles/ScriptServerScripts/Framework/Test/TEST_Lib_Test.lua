--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Sanity check for the script testing framework itself

TEST( "Fixture", function( test )
	local asserts = {
		"True", "False", "Nil", 
		"EQ", "NE", 
		"GT", "LT", 
		"GE", "LE", 
		"InList", "NotInList", 
		"Near", "AwayFrom", 
		"RefEQ", "RefNE", 
		"Type",
		"UndefinedField", "Field", "FieldType", "Method",
	}

	test:assertType(Test, "table")
	for _, assertName in ipairs(asserts) do
		test:expectMethod(Test, "expect" .. assertName)
		test:expectMethod(Test, "assert" .. assertName)
	end
end)

TEST( "HelloWorld", function( test )
    test:expectTrue(true)
end)

TEST( "AlwaysFail", function( test )
    test:expectTrue(false)
end)

TEST( "AssertionFailHalfway", function( test )
    test:expectTrue(true)
    test:assertTrue(false)
    test:log("shouldn't be here")
end)

TEST( "NilFuncTest", function( test )
    test:log("calling a non-existent function")
	local __NIL_FUNCTION = nil
	local res, msg = pcall(function() __NIL_FUNCTION("this will throw a lua error") end)
	test:expectFalse(res)
	test:assertType(msg, "string")
	test:log("CAUGHT " .. msg)
end)

TEST( "NilMethodTest", function( test )
    test:log("calling a non-existent method")
	local table = {}
	local res, msg = pcall(function() table.test("this will throw a lua error") end)
	test:expectFalse(res)
	test:assertType(msg, "string")
	test:log("CAUGHT " .. msg)
end)

TEST( "NilTableTest", function( test )
    test:log("Index a non-existent table")
	local __NIL_TABLE = nil
	local res, msg = pcall(function() __NIL_TABLE[1] = 1 end)
	test:expectFalse(res)
	test:assertType(msg, "string")
	test:log("CAUGHT " .. msg)
end)

TEST( "BasicAsserts", function( test )
	local nil1
	local tab1 = { 9, true, "hi" }
	local tab2 = { 9, true, "hi" }
	local tab3 = tab1

    test:expectTrue(true)
    test:expectFalse(false)
    
	test:expectEQ(3, 3)
	test:expectEQ(3.1416, 3.1416)
	test:expectEQ(3.1415926, 3.1415927)		-- within tolerance
    test:expectEQ("hello", "hello")
    test:expectEQ(true, true)
    test:expectEQ(false, false)

    test:expectNE(3, 4)
	test:expectNE(3.1415, 3.1416)			-- beyond tolerance
    test:expectNE("hello", "world")
    test:expectNE(true, false)
    test:expectNE(false, true)

    test:expectNil(nil)
	test:expectNil(aNil)
	test:expectNil(tab1[4])
	test:expectNil(tab1.badKey)

	test:expectGT(3, 2)
	test:expectLT(3, 4)
	test:expectGE(3, 2)
	test:expectLE(3, 4)
	test:expectGE(3, 3)
	test:expectLE(3, 3)

	test:expectInList(9, tab1)
	test:expectInList(true, tab1)
	test:expectInList("hi", tab1)
	test:expectNotInList(10, tab1)
	test:expectNotInList(false, tab1)
	test:expectNotInList("hey", tab1)

	test:expectNear(3.09, 3, 0.1)
	test:expectAwayFrom(2.9, 3, 0.09)

	test:expectRefEQ(tab1, tab3)
	test:expectRefEQ(5, 5)
	test:expectRefEQ("hello", "hello")
	test:expectRefNE(tab1, tab2)
	test:expectRefNE(4, 5)
	test:expectRefNE("hello", "hello world")

	test:expectType(78, "number")
	test:expectType(true, "boolean")
	test:expectType("world", "string")
	test:expectType(nil, "nil")
	test:expectType(function() end, "function")
	test:expectType(tab1, "table")
end)

TEST( "AllFailed", function( test )
	local nil1
	local tab1 = { 9, true, "hi" }
	local tab2 = { 9, true, "hi" }
	local tab3 = tab1

    test:expectTrue(false)
    test:expectFalse(true)
    
	test:expectNE(3, 3)
	test:expectNE(3.1416, 3.1416)
	test:expectNE(3.1415926, 3.1415927)		-- within tolerance
    test:expectNE("hello", "hello")
    test:expectNE(true, true)
    test:expectNE(false, false)

    test:expectEQ(3, 4)
	test:expectEQ(3.1415, 3.1416)			-- beyond tolerance
    test:expectEQ("hello", "world")
    test:expectEQ(true, false)
    test:expectEQ(false, true)

    test:expectNil(3)
	test:expectNil(tab1)
	test:expectNil(tab1[1])

	test:expectLT(3, 2)
	test:expectGT(3, 4)
	test:expectLE(3, 2)
	test:expectGE(3, 4)
	test:expectLT(3, 3)
	test:expectGT(3, 3)

	test:expectNotInList(9, tab1)
	test:expectNotInList(true, tab1)
	test:expectNotInList("hi", tab1)
	test:expectInList(10, tab1)
	test:expectInList(false, tab1)
	test:expectInList("hey", tab1)

	test:expectAwayFrom(3.09, 3, 0.1)
	test:expectNear(2.9, 3, 0.09)

	test:expectRefNE(tab1, tab3)
	test:expectRefNE(5, 5)
	test:expectRefNE("hello", "hello")
	test:expectRefEQ(tab1, tab2)
	test:expectRefEQ(4, 5)
	test:expectRefEQ("hello", "hello world")

	test:expectType(78, "string")
	test:expectType(true, "number")
	test:expectType("world", "boolean")
	test:expectType(nil, "function")
	test:expectType(function() end, "table")
	test:expectType(tab1, "nil")
end)
