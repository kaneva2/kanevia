--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_seat.lua
-- Tests all seat methods
----------------------------------------

Class.importClass("Class_Seat")

local NAME = "Seat"

local s_seat = ""

-- Node Objects
Seat = {}
Seat.__index = Seat

local STATE = {IDLE = 0}

-- Creates a node object with the given PID
function Seat.create()
	local seat = {}
	seat.name = NAME
	seat.totalTests = 4
	setmetatable(seat, Seat)
	
	return seat
end

function Seat.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default monster class")
	s_seat = Class_Seat.new()
	
	self.log("Seat instantiated ["..tostring(s_seat).."] \n")
	
	-- Test onServerTick
	self.log("")
	self.log("Testing onServerTick")
	s_seat:onServerTick()
	self.logResult(true, "onServerTick")
	self.log("onServerTick TEST COMPLETE")
	
	-- Test onProximity
	self.log("")
	self.log("Testing onProximity")
	s_seat:onProximity({triggerType = 1, player = g_testerSD})
	self.logResult(true, "onProximity")
	self.log("onProximity TEST COMPLETE")
	
	-- Test addPlayerToNode
	self.log("")
	self.log("Testing addPlayerToNode")
	s_seat:addPlayerToNode(g_testerSD, "1")
	self.logResult(true, "addPlayerToNode")
	self.log("addPlayerToNode TEST COMPLETE")
	
	-- Test removePlayerFromNode
	self.log("")
	self.log("Testing removePlayerFromNode")
	s_seat:removePlayerFromNode(g_testerSD, "1")
	self.logResult(true, "removePlayerFromNode")
	self.log("removePlayerFromNode TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Seat.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Seat.logResult(result, funcName, err)
	if result then Seat.log("PASS")
	else Seat.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end