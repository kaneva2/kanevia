--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_weapon.lua
-- Tests all Weapon methods
----------------------------------------

Class.importClass("Class_Weapon")

local NAME = "Weapon"

local s_weapon = ""

-- Node Objects
Weapon = {}
Weapon.__index = Weapon

-- Creates a test object
function Weapon.create()
	local weapon = {}
	weapon.name = NAME
	weapon.totalTests = 23
	setmetatable(weapon, Weapon)
	
	return weapon
end

function Weapon.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default Weapon class")
	s_weapon = Class_Weapon.new()
	
	self.log("Weapon instantiated ["..tostring(s_weapon).."] \n")
	
	-- Test onAttack
	self.log("")
	self.log("Testing onAttack")
	s_weapon:onAttack({player = g_testerSD, weaponType = 1})
	self.logResult(true, "onAttack")
	self.log("onAttack TEST COMPLETE")
	
	-- Test onQueryMapResp
	self.log("")
	self.log("Testing onQueryMapResp")
	s_weapon:onQueryMapResp({result = {doop = g_testerSD}, attacker = g_testerSD})
	self.logResult(true, "onQueryMapResp")
	self.log("onQueryMapResp TEST COMPLETE")
	
	-- Test onTrigger
	self.log("")
	self.log("Testing onTrigger")
	s_weapon:onTrigger({triggerType = 1, player = g_testerSD})
	self.logResult(true, "onTrigger")
	self.log("onTrigger TEST COMPLETE")
	
	-- Test onEquip
	self.log("")
	self.log("Testing onEquip")
	s_weapon:onEquip({_name = g_testerName, player = g_testerSD})
	self.logResult(true, "onEquip")
	self.log("onEquip TEST COMPLETE")
	
	-- Test onRemove
	self.log("")
	self.log("Testing onRemove")
	s_weapon:onRemove({_name = g_testerName, player = g_testerSD})
	self.logResult(true, "onRemove")
	self.log("onRemove TEST COMPLETE")
	
	-- Test getAttackBounds
	self.log("")
	self.log("Testing getAttackBounds")
	local pass1, pass2 = false, false
	local test1 = s_weapon:getAttackBounds(g_testerSD, "ranged")
	local test2 = s_weapon:getAttackBounds(g_testerSD, "melee")
	if test1[1].lookAt and test1[1].frustum and test1[1].upAxis then pass1 = true end
	if test2[1].cylinder then pass2 = true end
	if pass1 == false then
		local error = "Failed to return ranged attack bounds"
		self.logResult(false, "getAttackBounds", error)
	elseif pass2 == false then
		local error = "Failed to return melee attack bounds"
		self.logResult(false, "getAttackBounds", error)
	else
		self.logResult(true, "getAttackBounds")
	end
	self.log("getAttackBounds TEST COMPLETE")
	
	-- Test getTargetArmorFactor
	self.log("")
	self.log("Testing getTargetArmorFactor")
	local armor = s_weapon:getTargetArmorFactor({currArmorFactor = {2}})
	if armor == 2 then
		self.logResult(true, "getTargetArmorFactor")
	else
		local error = "Failed to return armor factor"
		self.logResult(false, "getTargetArmorFactor", error)
	end
	self.log("getTargetArmorFactor TEST COMPLETE")
	
	-- Test inRange
	self.log("")
	self.log("Testing inRange")
	local inRange = s_weapon:inRange({ID = 1}, g_testerSD)
	self.log("return["..tostring(inRange).."]")
	if inRange then
		self.logResult(true, "inRange")
	else
		local error = "Return invalid"
		self.logResult(false, "inRange", error)
	end
	self.log("inRange TEST COMPLETE")
	
	-- Test damage
	self.log("")
	self.log("Testing damage")
	s_weapon:damage(g_testerSD, g_testerSD, 1)
	self.logResult(true, "damage")
	self.log("damage TEST COMPLETE")
	
	-- Test applyMovingAnims
	self.log("")
	self.log("Testing applyMovingAnims")
	s_weapon:applyMovingAnims(g_testerSD)
	self.logResult(true, "applyMovingAnims")
	self.log("applyMovingAnims TEST COMPLETE")
	
	-- Test applyAttackingAnims
	self.log("")
	self.log("Testing applyAttackingAnims")
	s_weapon:applyAttackingAnims(g_testerSD)
	self.logResult(true, "applyAttackingAnims")
	self.log("applyAttackingAnims TEST COMPLETE")
	
	-- Test applyAttackEffect
	self.log("")
	self.log("Testing applyAttackEffect")
	s_weapon:applyAttackEffect(g_testerSD)
	self.logResult(true, "applyAttackEffect")
	self.log("applyAttackEffect TEST COMPLETE")
	
	-- Test cancelAttackEffect
	self.log("")
	self.log("Testing cancelAttackEffect")
	s_weapon:cancelAttackEffect(g_testerSD)
	self.logResult(true, "cancelAttackEffect")
	self.log("cancelAttackEffect TEST COMPLETE")
	
	-- Test applyHitEffect
	self.log("")
	self.log("Testing applyHitEffect")
	s_weapon:applyHitEffect(g_testerSD)
	self.logResult(true, "applyHitEffect")
	self.log("applyHitEffect TEST COMPLETE")
	
	-- Test cancelHitEffect
	self.log("")
	self.log("Testing cancelHitEffect")
	s_weapon:cancelHitEffect(g_testerSD)
	self.logResult(true, "cancelHitEffect")
	self.log("cancelHitEffect TEST COMPLETE")
	
	-- Test applyDamageEffect
	self.log("")
	self.log("Testing applyDamageEffect")
	s_weapon:applyDamageEffect(g_testerSD)
	self.logResult(true, "applyDamageEffect")
	self.log("applyDamageEffect TEST COMPLETE")
	
	-- Test cancelDamageEffect
	self.log("")
	self.log("Testing cancelDamageEffect")
	s_weapon:cancelDamageEffect(g_testerSD)
	self.logResult(true, "cancelDamageEffect")
	self.log("cancelDamageEffect TEST COMPLETE")
	
	-- Test nextQueryID
	self.log("")
	self.log("Testing nextQueryID")
	s_weapon.queryID = nil
	local result1 = s_weapon:nextQueryID()
	local result2 = s_weapon:nextQueryID()
	if result1 ~= 1 then
		local error = "First call failed to return 1"
		self.logResult(false, "nextQueryID", error)
	elseif result2 ~= 2 then
		local error = "Subsequent calls failed to increment queryID"
		self.logResult(false, "nextQueryID", error)
	else
		self.logResult(true, "nextQueryID")
	end
	self.log("nextQueryID TEST COMPLETE")
	
	-- Test getDistance
	self.log("")
	self.log("Testing getDistance")
	local testA = {x=0, y=0, z=0}
	local testB = {x=10, y=10, z=10}
	local expectedResult = 17.32
	local result = s_weapon.getDistance(testA, testB)
	local diff = math.abs(result - expectedResult)
	if (result - expectedResult) <= .5 then
		self.logResult(true, "getDistance")
	else
		local error = "Returned result["..tostring(result).."]. Delta["..tostring(diff).."]"
		self.logResult(false, "getDistance", error)
	end
	self.log("getDistance TEST COMPLETE")
	
	-- Test onArrive
	self.log("")
	self.log("Testing onArrive")
	s_weapon:onArrive({player = g_testerSD})
	self.logResult(true, "onArrive")
	self.log("onArrive TEST COMPLETE")
	
	-- Test onPlayerArrive
	self.log("")
	self.log("Testing onPlayerArrive")
	s_weapon:onPlayerArrive({player = g_testerSD})
	self.logResult(true, "onPlayerArrive")
	self.log("onPlayerArrive TEST COMPLETE")
	
	-- Test onGetPlayerList
	self.log("")
	self.log("Testing onGetPlayerList")
	s_weapon:onGetPlayerList({map = {player = g_testerSD}})
	self.logResult(true, "onGetPlayerList")
	self.log("onGetPlayerList TEST COMPLETE")
	
	-- Test destroy
	self.log("")
	self.log("Testing destroy")
	s_weapon:destroy()
	self.logResult(true, "destroy")
	self.log("destroy TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Weapon.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Weapon.logResult(result, funcName, err)
	if result then Weapon.log("PASS")
	else Weapon.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end