--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_collectible.lua
-- Tests all collectible methods
----------------------------------------

Class.importClass("Class_Collectible")

local NAME = "Collectible"

local s_collectible = ""

-- Node Objects
Collectible = {}
Collectible.__index = Collectible

local TEST_COLLECTSOUND = 3314134
local TEST_COLLECTPARTICLE = 3930876

-- Creates a node object with the given PID
function Collectible.create()
	local collectible = {}
	collectible.name = NAME
	collectible.totalTests = 12
	setmetatable(collectible, Collectible)
	
	return collectible
end

function Collectible.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default character class")
	s_collectible = Class_Collectible.new()
	
	self.log("Character instantiated ["..tostring(s_collectible).."]")
	
	local fauxTick = {tick = 1000}
	
	-- Test onTimer
	self.log("")
	self.log("Testing onTimer")
	s_collectible:onTimer(fauxTick)
	self.logResult(true, "onTimer")
	self.log("onTimer TEST COMPLETE")
	
	-- Test onServerTick
	self.log("")
	self.log("Testing onServerTick")
	s_collectible:onServerTick(fauxTick)
	self.logResult(true, "onServerTick")
	self.log("onServerTick TEST COMPLETE")
	
	local fauxTrigger = {triggerType = 1,
						 player = g_testerSD
						}
	
	-- Test onTrigger
	self.log("")
	self.log("Testing onTrigger")
	s_collectible:onTrigger(fauxTrigger)
	self.logResult(true, "onTrigger")
	self.log("onTrigger TEST COMPLETE")
	
	local fauxArrive = {player = g_testerSD}
	
	-- Test onArrive
	self.log("")
	self.log("Testing onArrive")
	s_collectible:onArrive(fauxArrive)
	self.logResult(true, "onArrive")
	self.log("onArrive TEST COMPLETE")
	
	-- Test onPlayerArrive
	self.log("")
	self.log("Testing onPlayerArrive")
	s_collectible:onPlayerArrive(fauxArrive)
	self.logResult(true, "onPlayerArrive")
	self.log("onPlayerArrive TEST COMPLETE")
	
	-- Test onGetPlayerList
	self.log("")
	self.log("Skipping onGetPlayerList() test")
	s_collectible:onGetPlayerList({map = {player = g_testerSD}})
	self.logResult(true, "onGetPlayerList")
	self.log("onGetPlayerList TEST COMPLETE")
	
	-- Test pickedUp
	self.log("")
	self.log("Testing pickedUp")
	s_collectible:pickedUp()
	self.logResult(true, "pickedUp")
	self.log("pickedUp TEST COMPLETE")
	
	-- Test respawn
	self.log("")
	self.log("Testing respawn")
	s_collectible:respawn()
	self.logResult(true, "respawn")
	self.log("respawn TEST COMPLETE")
	
	-- Set up class for collection
	s_collectible.collectSound = TEST_COLLECTSOUND
	s_collectible.collectParticle = TEST_COLLECTPARTICLE
	s_collectible.modifiers = {coins  = 10,
							   health = 10,
							   xp 	  = 10
							  }
	
	-- Test collect
	self.log("")
	self.log("Testing collect")
	s_collectible:collect(g_testerSD)
	self.logResult(true, "collect")
	self.log("collect TEST COMPLETE")
	
	-- Test checkCoinHUD
	self.log("")
	self.log("Testing checkCoinHUD")
	s_collectible:checkCoinHUD(g_testerSD)
	self.logResult(true, "checkCoinHUD")
	self.log("checkCoinHUD TEST COMPLETE")
	
	-- Test checkXPHUD
	self.log("")
	self.log("Testing checkXPHUD")
	s_collectible:checkXPHUD(g_testerSD)
	self.logResult(true, "checkXPHUD")
	self.log("checkXPHUD TEST COMPLETE")
	
	-- Test checkHealthHUD
	self.log("")
	self.log("Testing checkHealthHUD")
	s_collectible:checkHealthHUD(g_testerSD)
	self.logResult(true, "checkHealthHUD")
	self.log("checkHealthHUD TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Collectible.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Collectible.logResult(result, funcName, err)
	if result then Collectible.log("PASS")
	else Collectible.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end