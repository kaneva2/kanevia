--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_spawnpoint.lua
-- Tests all Spawn Point methods
----------------------------------------

Class.importClass("Class_SpawnPoint")

local NAME = "SpawnPoint"

local s_spawnPoint = ""

-- Node Objects
SpawnPoint = {}
SpawnPoint.__index = SpawnPoint

-- Creates a node object with the given PID
function SpawnPoint.create()
	local spawnPoint = {}
	spawnPoint.name = NAME
	spawnPoint.totalTests = 1
	setmetatable(spawnPoint, SpawnPoint)
	
	return spawnPoint
end

function SpawnPoint.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default Spawn Point class")
	s_spawnPoint = Class_SpawnPoint.new()
	
	self.log("Spawn Point instantiated ["..tostring(s_spawnPoint).."] \n")
	
	-- Test destroy
	self.log("")
	self.log("Testing destroy")
	s_spawnPoint:destroy()
	self.logResult(true, "destroy")
	self.log("destroy TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function SpawnPoint.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function SpawnPoint.logResult(result, funcName, err)
	if result then SpawnPoint.log("PASS")
	else SpawnPoint.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end