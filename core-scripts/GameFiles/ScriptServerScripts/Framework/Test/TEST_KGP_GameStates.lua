--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- kgp game state APIs

TEST( "Interface", function( test )
	test:assertType(kgp, "table")
	test:expectMethod(kgp, "playerGetGameState")
	test:expectMethod(kgp, "objectGetGameState")
end)

TEST( "PermObjectGetGameState", function( test )
	test:assertType(kgp, "table")
	test:assertMethod(kgp, "objectGetGameState")
	test:expectType(kgp.objectGetGameState(kgp.objectGetId()), "userdata")
end)

local genPID

TEST( "GenObjectGetGameState", function( test )
	test:assertType(kgp, "table")

	test:assertMethod(kgp, "objectGenerate")
	test:assertType(GLID_SINGULARITY, "number")
	genPID = kgp.objectGenerate(GLID_SINGULARITY, 0, 0, 0, 0, 0, 1)
	test:assertType(genPID, "number")

	test:assertMethod(kgp, "objectGetGameState")
	local gs = kgp.objectGetGameState(genPID)
	test:expectType(gs, "userdata")
end)

local duration = 5000
local waitTime1 = 2000
local waitTime2 = duration - waitTime1
local startTime
local finalPos = { x = 10, y = 5, z = 20 }
local finalRot = { x = 0.5, y = 0, z = 0.866 }

TEST( "GenObjectInstanceMoveAndRot", function( test )
	test:assertType(kgp, "table")

	test:assertMethod(kgp, "objectGetGameState")
	test:assertType(genPID, "number")
	local gs = kgp.objectGetGameState(genPID)
	test:assertType(gs, "userdata")

	local position = gs.position
	test:assertType(position, "table")
	test:expectField(position, "x", 0)
	test:expectField(position, "y", 0)
	test:expectField(position, "z", 0)
	test:expectField(position, "rx", 0)
	test:expectField(position, "ry", 0)
	test:expectField(position, "rz", 1)

	test:assertMethod(kgp, "objectMove")
	kgp.objectMove(genPID, 0, finalPos.x, finalPos.y, finalPos.z, finalRot.x, finalRot.y, finalRot.z)

	position = gs.position
	test:assertType(position, "table")
	test:expectField(position, "x", finalPos.x)
	test:expectField(position, "y", finalPos.y)
	test:expectField(position, "z", finalPos.z)
	test:expectField(position, "rx", finalRot.x)
	test:expectField(position, "ry", finalRot.y)
	test:expectField(position, "rz", finalRot.z)
end)

TEST( "GenObjectTimedMove", function( test )
	test:assertType(kgp, "table")

	test:assertMethod(kgp, "objectGetGameState")
	test:assertType(genPID, "number")
	local gs = kgp.objectGetGameState(genPID)
	test:assertType(gs, "userdata")

	test:assertMethod(kgp, "objectMove")
	kgp.objectMove(genPID, 0, 0, 0, 0, 0, 0, 1)

	local position = gs.position
	test:assertType(position, "table")
	test:expectField(position, "x", 0)
	test:expectField(position, "y", 0)
	test:expectField(position, "z", 0)

	test:log("[.....] Object move started, duration: " .. tostring(duration) .. " ms")
	kgp.objectMove(genPID, duration, finalPos.x, finalPos.y, finalPos.z, 0, 0, 1)
	test:assertMethod(kgp, "scriptGetTick")
	startTime = kgp.scriptGetTick()

	test:log("[.....] Wait for " .. tostring(waitTime1) .. " ms")
	return waitTime1
end)

TEST( "GenObjectTimedMoveProgess", function( test )
	test:assertType(kgp, "table")

	test:assertMethod(kgp, "scriptGetTick")
	local elapsed = kgp.scriptGetTick() - startTime
	test:log("[.....] Elapsed: " .. tostring(elapsed) .. " ms")
	test:assertLT(elapsed, duration)
	
	test:assertMethod(kgp, "objectGetGameState")
	test:assertType(genPID, "number")
	local gs = kgp.objectGetGameState(genPID)
	test:assertType(gs, "userdata")

	local position = gs.position
	test:assertType(position, "table")
	test:expectField(position, "x", finalPos.x * elapsed / duration)
	test:expectField(position, "y", finalPos.y * elapsed / duration)
	test:expectField(position, "z", finalPos.z * elapsed / duration)

	test:log("[.....] Wait until movement completed")
	return waitTime2
end)

TEST( "GenObjectTimedMoveCompleted", function( test )
	test:assertType(kgp, "table")

	test:assertMethod(kgp, "scriptGetTick")
	local elapsed = kgp.scriptGetTick() - startTime
	test:log("[.....] Elapsed: " .. tostring(elapsed) .. " ms")
	test:assertGE(elapsed, duration)

	test:assertMethod(kgp, "objectGetGameState")
	test:assertType(genPID, "number")
	local gs = kgp.objectGetGameState(genPID)
	test:assertType(gs, "userdata")

	local position = gs.position
	test:assertType(position, "table")
	test:expectField(position, "x", finalPos.x)
	test:expectField(position, "y", finalPos.y)
	test:expectField(position, "z", finalPos.z)
end)

TEST( "GenObjectTimedRotate", function( test )
	test:assertType(kgp, "table")

	test:assertMethod(kgp, "objectGetGameState")
	test:assertType(genPID, "number")
	local gs = kgp.objectGetGameState(genPID)
	test:assertType(gs, "userdata")

	test:assertMethod(kgp, "objectMove")
	kgp.objectMove(genPID, 0, 0, 0, 0, 0, 0, 1)

	local position = gs.position
	test:assertType(position, "table")
	test:expectField(position, "rx", 0)
	test:expectField(position, "ry", 0)
	test:expectField(position, "rz", 1)

	test:log("[.....] Object rotation started, duration: " .. tostring(duration) .. " ms")
	kgp.objectMove(genPID, duration, 0, 0, 0, finalRot.x, finalRot.y, finalRot.z)
	test:assertMethod(kgp, "scriptGetTick")
	startTime = kgp.scriptGetTick()

	test:log("[.....] Wait for " .. tostring(waitTime1) .. " ms")
	return waitTime1
end)

TEST( "GenObjectTimedRotateProgess", function( test )
	test:assertType(kgp, "table")

	test:assertMethod(kgp, "scriptGetTick")
	local elapsed = kgp.scriptGetTick() - startTime
	test:log("[.....] Elapsed: " .. tostring(elapsed) .. " ms")
	test:assertLT(elapsed, duration)
	
	test:assertMethod(kgp, "objectGetGameState")
	test:assertType(genPID, "number")
	local gs = kgp.objectGetGameState(genPID)
	test:assertType(gs, "userdata")

	local position = gs.position
	test:assertType(position, "table")
	test:expectField(position, "rx", finalRot.x * elapsed / duration)
	test:expectField(position, "ry", finalRot.y * elapsed / duration)
	test:expectField(position, "rz", (finalRot.z - 1) * elapsed / duration + 1)

	test:log("[.....] Wait until rotation completed")
	return waitTime2
end)

TEST( "GenObjectTimedRotateCompleted", function( test )
	test:assertType(kgp, "table")

	test:assertMethod(kgp, "scriptGetTick")
	local elapsed = kgp.scriptGetTick() - startTime
	test:log("[.....] Elapsed: " .. tostring(elapsed) .. " ms")
	test:assertGE(elapsed, duration)

	test:assertMethod(kgp, "objectGetGameState")
	test:assertType(genPID, "number")
	local gs = kgp.objectGetGameState(genPID)
	test:assertType(gs, "userdata")

	local position = gs.position
	test:assertType(position, "table")
	test:expectField(position, "rx", finalRot.x)
	test:expectField(position, "ry", finalRot.y)
	test:expectField(position, "rz", finalRot.z)
end)

TEST( "GenObjectDelete", function( test )
	test:assertType(kgp, "table")
	test:assertType(genPID, "number")

	test:assertMethod(kgp, "objectDelete")
	kgp.objectDelete(genPID)

	test:assertMethod(kgp, "objectGetGameState")
	local gs = kgp.objectGetGameState(genPID)
	-- test:expectType(gs, "userdata")

	-- test:expectNil(gs.position)
end)
