--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_interactivenode.lua
-- Tests all interactive node methods
----------------------------------------

Class.importClass("Class_InteractiveNode")

local NAME = "InteractiveNode"

local s_interactiveNode = ""

-- Node Objects
InteractiveNode = {}
InteractiveNode.__index = InteractiveNode

local TEST_ANIMATION = 3448878

-- Creates a node object with the given PID
function InteractiveNode.create()
	local interactiveNode = {}
	interactiveNode.name = NAME
	interactiveNode.totalTests = 18
	setmetatable(interactiveNode, InteractiveNode)
	
	return interactiveNode
end

function InteractiveNode.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default interactive node class")
	s_interactiveNode = Class_InteractiveNode.new()
	
	self.log("Interactive Node instantiated ["..tostring(s_interactiveNode).."]")
	
	local fauxTick = {tick = 1000}
	
	-- Test onDepart
	self.log("")
	self.log("Testing onDepart")
	s_interactiveNode:onDepart({player = g_testerSD})
	self.logResult(true, "onDepart")
	self.log("onDepart TEST COMPLETE")
	
	-- Test onPlayerDepart
	self.log("")
	self.log("Testing onPlayerDepart")
	s_interactiveNode:onPlayerDepart({player = g_testerSD})
	self.logResult(true, "onPlayerDepart")
	self.log("onPlayerDepart TEST COMPLETE")
	
	local fauxClick = {player = g_testerSD,
					   clickTable = {0, 0, 0}
					  }
	
	-- Test onLeftClick
	self.log("")
	self.log("Testing onLeftClick")
	s_interactiveNode:onLeftClick(fauxClick)
	self.logResult(true, "onLeftClick")
	self.log("onLeftClick TEST COMPLETE")
	
	local fauxTrigger = {triggerType = 1,
						 player = g_testerSD,
						 param = 1
						}
	
	-- Test onTrigger
	self.log("")
	self.log("Testing onTrigger")
	s_interactiveNode:onTrigger(fauxTrigger)
	self.logResult(true, "onTrigger")
	self.log("onTrigger TEST COMPLETE")
	
	-- Test onTimer
	self.log("")
	self.log("Testing onTimer")
	s_interactiveNode:onTimer()
	self.logResult(true, "onTimer")
	self.log("onTimer TEST COMPLETE")
	
	-- Test onServerTick
	self.log("")
	self.log("Testing onServerTick")
	s_interactiveNode:onServerTick()
	self.logResult(true, "onServerTick")
	self.log("onServerTick TEST COMPLETE")
	
	-- Adding faux nodes
	s_interactiveNode.playerPositions = {}
	s_interactiveNode.playerPositions["1"] = {male={x=0, y=0.25, z=0, rotation = 180, animation = TEST_ANIMATION}, female={x=0, y=0.25, z=0, rotation = 180, animation = TEST_ANIMATION}}
	s_interactiveNode.playerPositions["2"] = {male={x=10, y=0, z=10, rotation = 180, animation = TEST_ANIMATION}, female={x=10, y=0, z=10, rotation = 180, animation = TEST_ANIMATION}}
	s_interactiveNode.nodes["1"] = {player = nil, gender = nil, trigger = nil}
	s_interactiveNode.nodes["2"] = {player = nil, gender = nil, trigger = nil}
	
	local fauxClick1 = {player = g_testerSD,
					    clickTable = {0, 0, 0}
					   }
	local fauxClick2 = {player = g_testerSD,
					    clickTable = {10, 0, 10}
					   }
	
	-- Test getBestAvailableNode
	self.log("")
	self.log("Testing getBestAvailableNode")
	local result1 = s_interactiveNode:getBestAvailableNode(fauxClick1)
	local result2 = s_interactiveNode:getBestAvailableNode(fauxClick2)
	if result1 == "1" and result2 == "2" then
		self.logResult(true, "getBestAvailableNode")
	else
		local error = "result1["..tostring(result1).."] Expected 1. result2["..tostring(result2).."] Expected 2. FAIL"
		self.logResult(false, "getBestAvailableNode", error)
	end
	self.log("getBestAvailableNode TEST COMPLETE")
	
	local pass1, pass2, pass3 = false, false, false
	-- Test getPlayerNode
	self.log("")
	self.log("Testing getPlayerNode")
	
		local node = s_interactiveNode:getPlayerNode(g_testerSD)
		if node == nil then pass1 = true
		else pass1 = false end
		
		-- Test addPlayerToNode
		self.log("")
		self.log("Testing addPlayerToNode")
		s_interactiveNode:addPlayerToNode(g_testerSD, "2")
		self.logResult(true, "addPlayerToNode")
		self.log("addPlayerToNode TEST COMPLETE")
		
		local node = s_interactiveNode:getPlayerNode(g_testerSD)
		if node == "2" then pass2 = true
		else pass2 = false end
		
		-- Test removePlayerFromNode
		self.log("")
		self.log("Testing removePlayerFromNode")
		s_interactiveNode:removePlayerFromNode(g_testerSD, "2")
		self.logResult(true, "removePlayerFromNode")
		self.log("removePlayerFromNode TEST COMPLETE")
		
		local node = s_interactiveNode:getPlayerNode(g_testerSD)
		if node == nil then pass3 = true
		else pass3 = false end
	
	self.logResult(true, "getPlayerNode")
	self.log("getPlayerNode TEST COMPLETE")
	
	-- Test isObjectFull
	self.log("")
	self.log("Testing isObjectFull")
	local test1 = s_interactiveNode:isObjectFull()
	-- Fill the object up
	s_interactiveNode.nodes["1"] = {player = g_testerSD, gender = nil, trigger = nil}
	s_interactiveNode.nodes["2"] = {player = g_testerSD, gender = nil, trigger = nil}
	local test2 = s_interactiveNode:isObjectFull()
	if test1 == false and test2 == true then
		self.logResult(true, "isObjectFull")
	else
		local error = "Failed to detect full object"
		self.logResult(false, "isObjectFull", error)
	end
	self.log("isObjectFull TEST COMPLETE")
	
	-- Modify interactive node's position to test getNodeOffset
	s_interactiveNode.position.rx = .5
	s_interactiveNode.position.rz = .2
	
	-- Test getNodeOffset
	self.log("")
	self.log("Testing getNodeOffset")
	local offset = s_interactiveNode:getNodeOffset("2", "male")
	
	local testX = (10 * s_interactiveNode.position.rz)+(10 * s_interactiveNode.position.rx)
	local testY = 0
	local testZ = (10 * s_interactiveNode.position.rz)-(10 * s_interactiveNode.position.rx)
	local testRX, testRZ = s_interactiveNode.degreesToVector(s_interactiveNode.vectorToDegrees(s_interactiveNode.position.rx, s_interactiveNode.position.rz) + 180)
	local testRY = 0
	
	local diffX  = math.abs(testX)  - math.abs(offset.x)
	local diffY  = math.abs(testY)  - math.abs(offset.y)
	local diffZ  = math.abs(testZ)  - math.abs(offset.z)
	local diffRX = math.abs(testRX) - math.abs(offset.rx)
	local diffRY = math.abs(testRY) - math.abs(offset.ry)
	local diffRZ = math.abs(testRZ) - math.abs(offset.rz)
	
	local passX, passY, passZ, passRX, passRY, passRZ = false, false, false, false, false, false
	if diffX  < 0.2 then passX  = true end
	if diffY  < 0.2 then passY  = true end
	if diffZ  < 0.2 then passZ  = true end
	if diffRX < 0.2 then passRX = true end
	if diffRY < 0.2 then passRY = true end
	if diffRZ < 0.2 then passRZ = true end
	
	if passX and passY and passZ and passRX and passRY and passRZ then
		self.logResult(true, "getNodeOffset")
	else
		local error = "Expected ["..tostring(testX)..", "..tostring(testY)..", "..tostring(testZ)..", "..tostring(testRX)..", "..tostring(testRY)..", "..tostring(testRZ).."]. Returned ["..tostring(offset.x)..", "..tostring(offset.y)..", "..tostring(offset.z)..", "..tostring(offset.rx)..", "..tostring(offset.ry)..", "..tostring(offset.rz).."]"
		self.logResult(false, "getNodeOffset", error)
	end
	self.log("getNodeOffset TEST COMPLETE")
	
	-- Test getNodeGlobal
	self.log("")
	self.log("Testing getNodeGlobal")
	local global = s_interactiveNode:getNodeGlobal("2", "male")
	
	local testX2 = s_interactiveNode.position.x + testX
	local testY2 = s_interactiveNode.position.y + testY
	local testZ2 = s_interactiveNode.position.z + testZ
	
	local diffX = math.abs(testX2) - math.abs(global.x)
	local diffY = math.abs(testY2) - math.abs(global.y)
	local diffZ = math.abs(testZ2) - math.abs(global.z)
	
	local passX, passY, passZ = false, false, false
	
	if diffX < 0.2 then passX = true end
	if diffY < 0.2 then passY = true end
	if diffZ < 0.2 then passZ = true end
	
	if passX and passY and passZ then
		self.logResult(true, "getNodeGlobal")
	else
		local error = "Expected ["..tostring(testX2)..", "..tostring(testY2)..", "..tostring(testZ2).."]. Returned ["..tostring(global.x)..", "..tostring(global.y)..", "..tostring(global.z).."]"
		self.logResult(false, "getNodeGlobal", error)
	end
	self.log("getNodeGlobal TEST COMPLETE")
	
	-- Test onObjectOccupied
	self.log("")
	self.log("Testing onObjectOccupied")
	s_interactiveNode:onObjectOccupied()
	self.logResult(true, "onObjectOccupied")
	self.log("onObjectOccupied TEST COMPLETE")
	
	-- Test onObjectUnoccupied
	self.log("")
	self.log("Testing onObjectUnoccupied")
	s_interactiveNode:onObjectUnoccupied()
	self.logResult(true, "onObjectUnoccupied")
	self.log("onObjectUnoccupied TEST COMPLETE")
	
	-- Test getDistance
	self.log("")
	self.log("Testing getDistance")
	local testA = {x=0, y=0, z=0}
	local testB = {x=10, y=10, z=10}
	local expectedResult = 17.32
	local result = s_interactiveNode.getDistance(testA, testB)
	local diff = math.abs(result - expectedResult)
	if (result - expectedResult) <= .5 then
		self.logResult(true, "getDistance")
	else
		local error =  "Returned result["..tostring(result).."]. Delta["..tostring(diff).."]"
		self.logResult(false, "getDistance", error)
	end
	self.log("getDistance TEST COMPLETE")
	
	-- Test updatePosition
	self.log("")
	self.log("Testing updatePosition")
	s_interactiveNode:updatePosition()
	local posX, posY, posZ, posRX, posRY, posRZ = kgp.objectGetStartLocation(s_interactiveNode.PID)
	
	local passX, passY, passZ, passRX, passRY, passRZ = false, false, false, false, false, false
	
	local diffX  = math.abs(posX)  - math.abs(s_interactiveNode.position.x)
	local diffY  = math.abs(posY)  - math.abs(s_interactiveNode.position.y)
	local diffZ  = math.abs(posZ)  - math.abs(s_interactiveNode.position.z)
	local diffRX = math.abs(posRX) - math.abs(s_interactiveNode.position.rx)
	local diffRY = math.abs(posRY) - math.abs(s_interactiveNode.position.ry)
	local diffRZ = math.abs(posRZ) - math.abs(s_interactiveNode.position.rz)
	
	if diffX  < 0.2 then passX  = true end
	if diffY  < 0.2 then passY  = true end
	if diffZ  < 0.2 then passZ  = true end
	if diffRX < 0.2 then passRX = true end
	if diffRY < 0.2 then passRY = true end
	if diffRZ < 0.2 then passRZ = true end
	
	if passX and passY and passZ and passRX and passRY and passRZ then
		self.logResult(true, "updatePosition")
	else
		local error = "Expected ["..tostring(posX)..", "..tostring(posY)..", "..tostring(posZ)..", "..tostring(posRX)..", "..tostring(posRY)..", "..tostring(posRZ).."] Returned ["..tostring(s_interactiveNode.position.x)..", "..tostring(s_interactiveNode.position.y)..", "..tostring(s_interactiveNode.position.z)..", "..tostring(s_interactiveNode.position.rx)..", "..tostring(s_interactiveNode.position.ry)..", "..tostring(s_interactiveNode.position.rz).."]"
		self.logResult(false, "updatePosition", error)
	end
	self.log("updatePosition TEST COMPLETE")
	
	-- Test destroy
	self.log("")
	self.log("Testing destroy")
	s_interactiveNode:destroy()
	self.logResult(true, "destroy")
	self.log("destroy TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function InteractiveNode.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function InteractiveNode.logResult(result, funcName, err)
	if result then InteractiveNode.log("PASS")
	else InteractiveNode.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end