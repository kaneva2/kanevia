--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_base.lua
-- Tests all base methods
----------------------------------------

kgp.scriptDoFile("KanevaConstants.lua")

Class.importClass("Class_Base")

local NAME = "Base"

local s_base = ""

-- Node Objects
Base = {}
Base.__index = Base

local s_playerSetParticleKey = 0

local s_self = ""

local TEST_PLAYEREMOTE = 3448878
local TEST_ANIMATION = 3885419
local TEST_TEXTURE = "http://images.kaneva.com/filestore11/5509627/7478511/danceUfloor.jpg"
local TEST_FLASH = "http://dagobah.net/flash/LINK.swf"
local TEST_PARTICLE = 4211728
local TEST_SOUND = 4403322

-- Creates a node object with the given PID
function Base.create()
	local base = {}
	base.name = NAME
	base.totalTests = 67
	setmetatable(base, Base)
	
	s_self = base
	return base
end

function Base.test(self)
	-- Phase 1 (start)
	if g_testPhase == 1 then
		s_self.log("BEGINNING TEST")
		
		-- Instantiation
		s_self.log("Instantiating default base class")
		s_base = Class_Base.new()
		
		s_self.log("Base instantiated ["..tostring(s_base).."]")
		
		-- Test onMovementArrived
		s_self.log("")
		s_self.log("Testing onMovementArrived")
		s_base:onMovementArrived()
		s_self.logResult(true, "onMovementArrived")
		s_self.log("onMovementArrived TEST COMPLETE")
		
		-- Test onLevelUp
		s_self.log("")
		s_self.log("Testing onLevelUp")
		s_base:onLevelUp()
		s_self.logResult(true, "onLevelUp")
		s_self.log("onLevelUp TEST COMPLETE")
		
		-- Test onLeftClick
		s_self.log("")
		s_self.log("Testing onLeftClick")
		s_base:onLeftClick()
		s_self.logResult(true, "onLeftClick")
		s_self.log("onLeftClick TEST COMPLETE")
		
		-- Test onRightClick
		s_self.log("")
		s_self.log("Testing onRightClick")
		s_base:onRightClick()
		s_self.logResult(true, "onRightClick")
		s_self.log("onRightClick TEST COMPLETE")
		
		-- Test onCollide
		s_self.log("")
		s_self.log("Testing onCollide")
		s_base:onCollide()
		s_self.logResult(true, "onCollide")
		s_self.log("onCollide TEST COMPLETE")
		
		-- Test onTrigger
		s_self.log("")
		s_self.log("Testing onTrigger")
		s_base:onTrigger()
		s_self.logResult(true, "onTrigger")
		s_self.log("onTrigger TEST COMPLETE")
		
		-- Test onPlayerLevelUp
		s_self.log("")
		s_self.log("Testing onPlayerLevelUp")
		s_base:onPlayerLevelUp()
		s_self.logResult(true, "onPlayerLevelUp")
		s_self.log("onPlayerLevelUp TEST COMPLETE")
		
		-- Test registerWithController
		s_self.log("")
		s_self.log("Testing registerWithController")
		s_base:registerWithController()
		s_self.logResult(true, "registerWithController")
		s_self.log("registerWithController TEST COMPLETE")
		
		-- Test hide
		s_self.log("")
		s_self.log("Testing hide")
		s_base:hide()
		s_self.logResult(true, "hide")
		s_self.log("hide TEST COMPLETE")
		
		-- Test show
		s_self.log("")
		s_self.log("Testing show")
		s_base:show()
		s_self.logResult(true, "show")
		s_self.log("show TEST COMPLETE")
		
		-- Test scale
		s_self.log("")
		s_self.log("Testing scale")
		s_base:scale(2)
		s_self.logResult(true, "scale")
		s_self.log("scale TEST COMPLETE")
		
		-- Test addLabels
		s_self.log("")
		s_self.log("Testing addLabels")
		s_base:addLabels({{"Scaled by 2", 0.02, 1, 1, 1}})
		s_self.logResult(true, "addLabels")
		s_self.log("addLabels TEST COMPLETE")
		
		-- Test clearLabels
		s_self.log("")
		s_self.log("Testing clearLabels")
		s_base:clearLabels()
		s_self.logResult(true, "clearLabels")
		s_self.log("clearLabels TEST COMPLETE")
		
		-- Test setAnimation
		s_self.log("")
		s_self.log("Testing setAnimation")
		s_base:setAnimation(TEST_ANIMATION)
		s_self.logResult(true, "setAnimation")
		s_self.log("setAnimation TEST COMPLETE")
		
		-- Test setTexture
		s_self.log("")
		s_self.log("Testing setTexture")
		s_base:setTexture(TEST_TEXTURE)
		s_self.logResult(true, "setTexture")
		s_self.log("setTexture TEST COMPLETE")
		
		-- Test setTexturePanning
		s_self.log("")
		s_self.log("Testing setTexturePanning")
		s_base:setTexturePanning(0, 0, 0.01, 0.01)
		s_self.logResult(true, "setTexturePanning")
		s_self.log("setTexturePanning TEST COMPLETE")
		
		-- Test setParticle
		s_self.log("")
		s_self.log("Testing setParticle")
		s_base:setParticle(TEST_PARTICLE)
		s_self.logResult(true, "setParticle")
		s_self.log("setParticle TEST COMPLETE")
		
		-- Test removeParticle
		s_self.log("")
		s_self.log("Testing removeParticle")
		s_base:removeParticle()
		s_self.logResult(true, "removeParticle")
		s_self.log("removeParticle TEST COMPLETE")
		
		-- Test setMouseOver
		s_self.log("")
		s_self.log("Testing setMouseOver")
		s_base:setMouseOver(0, "TEST TOOLTIP")
		s_self.logResult(true, "setMouseOver")
		s_self.log("setMouseOver TEST COMPLETE")
		
		-- Test setOutline
		s_self.log("")
		s_self.log("Testing setOutline")
		s_base:setOutline(true, {r=255, g=255, b=255})
		s_self.logResult(true, "setOutline")
		s_self.log("setOutline TEST COMPLETE")
		
		-- Test setEffectTrack
		-- TODO: This causes a crash. Why?
		s_self.log("")
		s_self.log("Skipping setEffectTrack test. A crash currently occurs here now.")
		--s_self.log("Testing setEffectTrack")
		local test_effect = {
							 {EFFECT_CODES.ANIMATION, 0, TEST_ANIMATION},
							 {EFFECT_CODES.SOUND, 0, TEST_SOUND},
							 {EFFECT_CODES.PARTICLE, 0, 3000, "Bip01", 0, TEST_PARTICLE}
							}
		--s_base:setEffectTrack(test_effect)
		--s_self.log("setEffectTrack TEST COMPLETE")
		
		-- Test setCollision
		s_self.log("")
		s_self.log("Testing setCollision")
		s_base:setCollision(false)
		s_self.logResult(true, "setCollision")
		s_self.log("setCollision TEST COMPLETE")
		
		-- Test setFriction
		s_self.log("")
		s_self.log("Testing setFriction")
		s_base:setFriction(false)
		s_self.logResult(true, "setFriction")
		s_self.log("setFriction TEST COMPLETE")
		
		-- Test lookAt
		s_self.log("")
		s_self.log("Testing lookAt")
		s_base:lookAt(g_tester)
		s_self.logResult(true, "lookAt")
		s_self.log("lookAt TEST COMPLETE")
		
		-- Test moveTo
		s_self.log("")
		s_self.log("Testing moveTo")
		s_base:moveTo({x = s_base.position.x+5, y = s_base.position.y+5, z = s_base.position.z+5, lookAt = true})
		s_self.logResult(true, "moveTo")
		s_self.log("moveTo TEST COMPLETE")
		
		-- Test rotateBy
		s_self.log("")
		s_self.log("Testing rotateBy")
		s_base:rotateBy({angle = 90, time = 1})
		s_self.logResult(true, "rotateBy")
		s_self.log("rotateBy TEST COMPLETE")
		
		-- Test chase
		s_self.log("")
		s_self.log("Testing chase")
		local success = s_base:chase({target = g_tester, buffer = 5})
		if success then
			s_self.logResult(true, "chase")
		else
			local error = "Failed to obtain target"
			s_self.logResult(false, "chase", error)
		end
		s_self.log("chase TEST COMPLETE")
		
		-- Test getDistanceToPoint
		s_self.log("")
		s_self.log("Testing getDistanceToPoint")
		local test = {x=10, y=10, z=10}
		local expectedResult = math.sqrt(math.pow((s_base.position.x - test.x),2) + math.pow((s_base.position.y - test.y),2) + math.pow((s_base.position.z - test.z),2))
		local result = s_base:getDistanceToPoint({x=10, y=10, z=10})
		local diff = math.abs(expectedResult) - math.abs(result)
		if diff <= .5 then
			s_self.logResult(true, "getDistanceToPoint")
		else
			local error = "Returned result["..tostring(result).."]. Delta["..tostring(diff).."]"
			s_self.logResult(false, "getDistanceToPoint", error)
		end
		s_self.log("getDistanceToPoint TEST COMPLETE")
		
		-- Test stopMovement
		s_self.log("")
		s_self.log("Testing stopMovement")
		s_base:stopMovement()
		s_self.logResult(true, "stopMovement")
		s_self.log("stopMovement TEST COMPLETE")
		
		-- Test pauseMovement
		s_self.log("")
		s_self.log("Testing pauseMovement")
		s_base:pauseMovement()
		s_self.logResult(true, "pauseMovement")
		s_self.log("pauseMovement TEST COMPLETE")
		
		-- Test resumeMovement
		s_self.log("")
		s_self.log("Testing resumeMovement")
		s_base:resumeMovement()
		s_self.logResult(true, "resumeMovement")
		s_self.log("resumeMovement TEST COMPLETE")
		
		-- Test teleport
		s_self.log("")
		s_self.log("Testing teleport")
		s_base:teleport({x = s_base.position.x, y = s_base.position.y+10, z = s_base.position.z})
		s_self.logResult(true, "teleport")
		s_self.log("teleport TEST COMPLETE")
		
		-- Test generateSound
		s_self.log("")
		s_self.log("Testing generateSound")
		local testSound = s_base.generateSound({ID = g_tester, soundGLID = TEST_SOUND, {x = 0, y = 0, z = 0, rx = 0, ry = 0, rz = 1}, pitch = 1, volume = 0.5, distance = 100,
												attenuation = 1, looping = 0, loopdelay = 0, innerconeangle = 360, outerconeangle = 360, outerconegain = 0.5})
		if testSound then
			s_self.logResult(true, "generateSound")
		else
			local error = "Failed to generate sound"
			s_self.logResult(false, "generateSound", error)
		end
		s_self.log("generateSound TEST COMPLETE")
		
		-- Test configSound
		s_self.log("")
		s_self.log("Testing configSound")
		s_base:configSound({soundID = testSound,
							pitch = 2,
							volume = 0.5,
							distance = 50,
							attenuation = 0,
							looping = 1,
							loopDelay = 1000,
							innerConeAngle = 360,
							outerConeAngle = 360,
							outerConeGain = 1
						   })
		s_self.logResult(true, "configSound")
		s_self.log("configSound TEST COMPLETE")
		
		-- Test playSound
		s_self.log("")
		s_self.log("Testing playSound")
		s_base:playSound({soundId = testSound})
		s_self.logResult(true, "playSound")
		s_self.log("playSound TEST COMPLETE")
		
		-- Test stopSound
		s_self.log("")
		s_self.log("Testing stopSound")
		s_base:stopSound({soundId = testSound})
		s_self.logResult(true, "stopSound")
		s_self.log("stopSound TEST COMPLETE")
		
		-- Test deleteSound
		s_self.log("")
		s_self.log("Testing deleteSound")
		s_base:deleteSound({soundId = testSound})
		s_self.logResult(true, "deleteSound")
		s_self.log("deleteSound TEST COMPLETE")
		
		-- Test playerSetPhysics
		s_self.log("")
		s_self.log("Testing playerSetPhysics")
		s_base:playerSetPhysics({lock = true})
		s_base:playerSetPhysics({maxSpeed = 2,
								 minSpeed = 2,
								 accellerate = 2,
								 decellerate = 2,
								 curTransY = 3,
								 rotationDragConstant = 1,
								 rotationalGroundDrag = 1,
								 jumpPower = 10,
								 groundDrag = 2,
								 strafeReduction = 2,
								 crossSectionalArea = 2,
								 airControl = 5,
								 jumpRecoverTime = 3
								})
		s_base:playerSetPhysics({reset = true})
		s_self.logResult(true, "playerSetPhysics")
		s_self.log("playerSetPhysics TEST COMPLETE")
		
		-- Test playerTeleport
		s_self.log("")
		s_self.log("Testing playerTeleport")
		s_base:playerTeleport({x  = g_testerSD.position.x,
							   y  = g_testerSD.position.y + 10,
							   z  = g_testerSD.position.z,
							   rx = g_testerSD.position.rx,
							   ry = g_testerSD.position.ry,
							   rz = g_testerSD.position.rz
							  })
		s_self.logResult(true, "playerTeleport")
		s_self.log("playerTeleport TEST COMPLETE")
		
		-- Test playerAddXP
		s_self.log("")
		s_self.log("Testing playerAddXP")
		s_base:playerAddXP({XP = 50})
		s_self.logResult(true, "playerAddXP")
		s_self.log("playerAddXP TEST COMPLETE")
		
		-- Test playerAddCoins
		s_self.log("")
		s_self.log("Testing playerAddCoins")
		s_base:playerAddCoins({user = g_testerSD, coins = 5})
		s_self.logResult(true, "playerAddCoins")
		s_self.log("playerAddCoins TEST COMPLETE")
		
		-- Test playerRemoveCoins
		s_self.log("")
		s_self.log("Testing playerRemoveCoins")
		s_base:playerRemoveCoins({user = g_testerSD, coins = 4})
		s_self.logResult(true, "playerRemoveCoins")
		s_self.log("playerRemoveCoins TEST COMPLETE")
		
		-- Test playerAddHealth
		s_self.log("")
		s_self.log("Testing playerAddHealth")
		s_base:playerAddHealth({user = g_testerSD, health = 10})
		s_self.logResult(true, "playerAddHealth")
		s_self.log("playerAddHealth TEST COMPLETE")
		
		-- Test playerRemoveHealth
		s_self.log("")
		s_self.log("Testing playerRemoveHealth")
		s_base:playerRemoveHealth({user = g_testerSD, health = 5})
		s_self.logResult(true, "playerRemoveHealth")
		s_self.log("playerRemoveHealth TEST COMPLETE")
		
		-- Test playerSetHealth
		s_self.log("")
		s_self.log("Testing playerSetHealth")
		s_base:playerSetHealth({user = g_testerSD, health = 50})
		s_self.logResult(true, "playerSetHealth")
		s_self.log("playerSetHealth TEST COMPLETE")
		
		-- Test playerPlaySound
		s_self.log("")
		s_self.log("Testing playerPlaySound")
		s_base:playerPlaySound({user = g_testerSD, playTime = 3, soundGLID = TEST_SOUND})
		s_self.logResult(true, "playerPlaySound")
		s_self.log("playerPlaySound TEST COMPLETE")
		
		-- Test playerStopSound
		s_self.log("")
		s_self.log("Testing playerStopSound")
		s_base:playerStopSound({soundGLID = TEST_COUND})
		s_self.logResult(true, "playerStopSound")
		s_self.log("playerStopSound TEST COMPLETE")
		
		-- Test playerEnableCoinHUD
		s_self.log("")
		s_self.log("Testing playerEnableCoinHUD")
		s_base:playerEnableCoinHUD({user = g_testerSD})
		s_self.logResult(true, "playerEnableCoinHUD")
		s_self.log("playerEnableCoinHUD TEST COMPLETE")
		
		-- Test playerEnableXPHUD
		s_self.log("")
		s_self.log("Testing playerEnableXPHUD")
		s_base:playerEnableXPHUD({user = g_testerSD})
		s_self.logResult(true, "playerEnableXPHUD")
		s_self.log("playerEnableXPHUD TEST COMPLETE")
		
		-- Test playerEnableHealthHUD
		s_self.log("")
		s_self.log("Testing playerEnableHealthHUD")
		s_base:playerEnableHealthHUD({user = g_testerSD})
		s_self.logResult(true, "playerEnableHealthHUD")
		s_self.log("playerEnableHealthHUD TEST COMPLETE")
		
		-- Test playerSetParticle
		s_self.log("")
		s_self.log("Testing playerSetParticle")
		s_playerSetParticleKey = s_base.playerSetParticle({user = g_tester,
														   particleGLID = TEST_PARTICLE,
														   particleKey = "test",
														   playTime = 10,
														   boneName = "Bip01 L Finger11",
														   offset = {x = 3, y = 0, z = 0},
														   forwardDir = {x = 1, y = 0, z = 0},
														   upDir = {x = 0, y = 1, z = 0}
														  })
		s_self.log("playerSetParticle returned particle key["..tostring(s_playerSetParticleKey).."]")
		if s_playerSetParticleKey == false then
			local error = "Failed to generate particle key"
			s_self.logResult(false, "playerSetParticle", error)
		else
			s_self.logResult(true, "playerSetParticle")
		end
		s_self.log("playerSetParticle TEST COMPLETE")
	
		--s_self.log("Pausing for 2 seconds.")
		
		--g_delay = 2
		
	-- Phase 2
	--elseif g_testPhase == 2 then
	
		-- Test playerRemoveParticle
		s_self.log("")
		s_self.log("Testing playerRemoveParticle")
		local success = s_base.playerRemoveParticle({user = g_tester, particleKey = s_playerSetParticleKey})
		if success then
			s_self.logResult(true, "playerRemoveParticle")
		else
			local error = "Failed to remove particle"
			s_self.logResult(false, "playerRemoveParticle", error)
		end
		s_self.log("playerRemoveParticle TEST COMPLETE")
	
		-- Test playerSetAnimation
		s_self.log("")
		s_self.log("Testing playerSetAnimation")
		s_base:playerSetAnimation({user = g_tester, animation = TEST_PLAYEREMOTE})
		s_self.logResult(true, "playerSetAnimation")
		s_self.log("playerSetAnimation TEST COMPLETE")
		
		-- Test validatePlayerPosition
		s_self.log("")
		s_self.log("Testing validatePlayerPosition")
		local success = s_base.validatePlayerPosition(g_testerSD.position)
		if success then
			s_self.logResult(true, "validatePlayerPosition")
		else
			local error = "Failed to validate position"
			s_self.logResult(false, "validatePlayerPosition", error)
		end
		s_self.log("validatePlayerPosition TEST COMPLETE")
		
		-- Test playerValidateNumber
		s_self.log("")
		s_self.log("Testing playerValidateNumber")
		local fail = s_base.playerValidateNumber("banana")
		local pass = s_base.playerValidateNumber(5)
		if pass and fail == false then 
			s_self.logResult(true, "playerValidateNumber")
		elseif pass and fail then
			local error = "Failed to invalidate \"banana\""
			s_self.logResult(false, "playerValidateNumber", error)
		elseif pass == false and fail == false then
			local error = "Failed to validate 5"
			s_self.logResult(false, "playerValidateNumber", error)
		end
		s_self.log("playerValidateNumber TEST COMPLETE")
	
		-- Test validatePlayerGender
		s_self.log("")
		s_self.log("Testing validatePlayerGender")
		local fail1 = s_base.validatePlayerGender("Q")
		local fail2 = s_base.validatePlayerGender(500)
		local pass1 = s_base.validatePlayerGender("M")
		local pass2 = s_base.validatePlayerGender("F")
		if fail1 then
			local error = "Failed to invalidate \"Q\" gender"
			s_self.logResult(false, "validatePlayerGender", error)
		elseif fail2 then
			local error = "Failed to invalidate 500"
			s_self.logResult(false, "validatePlayerGender", error)
		elseif pass1 == false then
			local error = "Failed to validate \"M\" gender"
			s_self.logResult(false, "validatePlayerGender", error)
		elseif pass2 == false then
			local error = "Failed to validate \"F\" gender"
			s_self.logResult(false, "validatePlayerGender", error)
		elseif pass1 and pass2 and fail1 == false and fail2 == false then
			s_self.logResult(true, "validatePlayerGender")
		end
		s_self.log("validatePlayerGender TEST COMPLETE")
		
		-- Test playerValidateString
		s_self.log("")
		s_self.log("Testing playerValidateString")
		local fail = s_base.playerValidateString(500)
		local pass = s_base.playerValidateString("poop")
		if fail then
			local error = "Failed to invalidate 500"
			s_self.logResult(false, "playerValidateString", error)
		elseif pass == false then
			local error = "Failed to validate \"poop\""
			s_self.logResult(false, "playerValidateString", error)
		elseif pass and fail == false then
			s_self.logResult(true, "playerValidateString")
		end
		s_self.log("playerValidateString TEST COMPLETE")
		
		-- Test setFlash
		s_self.log("")
		s_self.log("Testing setFlash")
		s_base:setFlash(TEST_FLASH, "")
		s_self.logResult(true, "setFlash")
		s_self.log("setFlash TEST COMPLETE")
		
		-- Test clearFlash
		s_self.log("")
		s_self.log("Testing clearFlash")
		s_base:clearFlash()
		s_self.logResult(true, "clearFlash")
		s_self.log("clearFlash TEST COMPLETE")
		
		-- Test setPlaylist and playTrack
		s_self.log("")
		s_self.log("Skipping setPlaylist(), playTrack() test. Playlists must be built for the world first.")
		--s_self.log("Testing setPlaylist")
		--s_base:setPlaylist(
		--s_self.log("setPlaylist TEST COMPLETE")
		
		-- Test getNextTrack
		s_self.log("")
		s_self.log("Testing getNextTrack")
		s_base:getNextTrack()
		s_self.logResult(true, "getNextTrack")
		s_self.log("getNextTrack TEST COMPLETE")
		
		-- Test vectorToDegrees
		s_self.log("")
		s_self.log("Testing vectorToDegrees(3, 4)")
		local angle = s_base.vectorToDegrees(3, 4)
		local expectedReturn = 53.1
		if (expectedReturn - math.abs(angle)) <= 0.1 then
			s_self.logResult(true, "vectorToDegrees")
		else
			local error = "Expected angle["..tostring(expectedReturn).."] Actual return["..tostring(angle).."]"
			s_self.logResult(false, "vectorToDegrees", error)
		end
		s_self.log("vectorToDegrees TEST COMPLETE")
		
		-- Test degreesToVector
		s_self.log("")
		s_self.log("Testing degreesToVector(53.120102354)")
		local x, z = s_base.degreesToVector(53.120102354)
		local expectedX = 0.6
		local expectedZ = 0.8
		if ((expectedX - math.abs(x)) <= 0.1) and ((expectedZ - math.abs(z)) <= 0.1) then
			s_self.logResult(true, "degreesToVector")
		else
			local error = "Expected x,z[("..tostring(expectedX)..", "..tostring(expectedZ)..")] Actual return[("..tostring(x)..", "..tostring(z)..")]"
			s_self.logResult(false, "degreesToVector", error)
		end
		s_self.log("degreesToVector TEST COMPLETE")
		
		-- Test convertTableToIndexed
		s_self.log("")
		s_self.log("Testing convertTableToIndexed")
		local testTable = s_base:convertTableToIndexed({doop = 1, poop = 2, scoop = 3})
		if testTable[1] and testTable[2] and testTable[3] then
			s_self.logResult(true, "convertTableToIndexed")
		else
			local error = "Failed to convert has table to indexed table"
			s_self.logResult(false, "convertTableToIndexed", error)
		end
		s_self.log("convertTableToIndexed TEST COMPLETE")
		
		-- Test sortTableByParameter
		s_self.log("")
		s_self.log("Testing sortTableByParameter")
		local tableToSort = {doop  = {index = 3, name = "doop"},
							 poop  = {index = 1, name = "poop"},
							 scoop = {index = 2, name = "scoop"}
							}
		local sorted = s_base.sortTableByParameter(tableToSort, "index")
		if sorted[1].name == "poop" and sorted[2].name == "scoop" and sorted[3].name == "doop" then
			s_self.logResult(true, "sortTableByParameter")
		else
			local error = "Failed to sort table by index"
			s_self.logResult(false, "sortTableByParameter", error)
		end
		s_self.log("sortTableByParameter TEST COMPLETE")
		
		-- Test sortIndexedTableByParameter
		s_self.log("")
		s_self.log("Testing sortIndexedTableByParameter")
		local tableToSort = {{index = 3, name = "doop"},
							 {index = 1, name = "poop"},
							 {index = 2, name = "scoop"}
							}
		local sorted = s_base.sortIndexedTableByParameter(tableToSort, "index")
		if sorted[1].name == "poop" and sorted[2].name == "scoop" and sorted[3].name == "doop" then
			s_self.logResult(true, "sortIndexedTableByParameter")
		else
			local error = "Failed to sort table by index"
			s_self.logResult(false, "sortIndexedTableByParameter", error)
		end
		s_self.log("sortIndexedTableByParameter TEST COMPLETE")
		
		-- Test rest
		s_self.log("")
		s_self.log("Testing rest")
		local tableToSort = {{index = 1, name = "poop"},
							 {index = 2, name = "scoop"},
							 {index = 3, name = "doop"}
							}
		local result = s_base.rest(tableToSort)
		if result[1].name == "scoop" and result[2].name == "doop" then
			s_self.logResult(true, "rest")
		else
			local error = "Failed to extract last elements in table"
			s_self.logResult(false, "rest", error)
		end
		s_self.log("rest TEST COMPLETE")
		
		-- Test concat
		s_self.log("")
		s_self.log("Testing concat")
		local indexed1 = {"poop", "doop", "scoop"}
		local indexed2 = {"drop", "plop", "shop"}
		
		local hashed1 = {doop = {name = "doop"}, poop = {name = "poop"}, scoop = {name = "scoop"}}
		local hashed2 = {drop = {name = "drop"}, plop = {name = "plop"}, shop = {name = "shop"}}
		
		local expected = {doop = {}, poop = {}, scoop = {}, drop = {}, plop = {}, shop = {}}
		local expectedCount = 6
		
		local test1 = s_base.concat(indexed1, indexed2)
		local test2 = s_base.concat(hashed1, hashed2)
		
		local pass1, pass2 = false, false
		local testCount = 0
		for i,v in pairs(test1) do
			if expected[v] then
				testCount = testCount + 1
			else break end
		end
		if testCount == expectedCount then pass1 = true end
		
		testCount = 0
		for i,v in pairs(test2) do
			if expected[v.name] then
				testCount = testCount + 1
			else break end
		end
		if testCount == expectedCount then pass2 = true end
		
		if pass1 == false then
			local error = "Failed to concatenate indexed table"
			s_self.logResult(false, "concat", error)
		elseif pass2 == false then
			local error = "Failed to concatenate hash table"
			s_self.logResult(false, "concat", error)
		else
			s_self.logResult(true, "concat")
		end
		
		s_self.log("concat TEST COMPLETE")
		
		-- Test destroy
		s_self.log("")
		s_self.log("Testing destroy")
		s_base:destroy()
		s_self.logResult(true, "destroy")
		s_self.log("destroy TEST COMPLETE")
		
		-- Test complete
		s_self.log("TEST COMPLETE")
		
		g_testPhase = 1

		testNext()
	end
end

function Base.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Base.logResult(result, funcName, err)
	if result then Base.log("PASS")
	else Base.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end