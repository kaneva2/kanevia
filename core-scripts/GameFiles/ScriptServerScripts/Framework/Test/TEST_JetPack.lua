--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_jetpack.lua
-- Tests all jetpack methods
----------------------------------------

Class.importClass("Class_JetPack")

local NAME = "JetPack"

local s_jetpack = ""

-- Node Objects
JetPack = {}
JetPack.__index = JetPack

-- Creates a node object with the given PID
function JetPack.create()
	local jetpack = {}
	jetpack.name = NAME
	jetpack.totalTests = 12
	setmetatable(jetpack, JetPack)
	
	return jetpack
end

function JetPack.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default JetPack")
	s_jetpack = Class_JetPack.new()
	
	self.log("JetPack instantiated ["..tostring(s_jetpack).."]")
	
	-- Test onLeftClick
	self.log("")
	self.log("Testing onLeftClick")
	s_jetpack:onLeftClick(g_testerSD)
	self.logResult(true, "onLeftClick")
	self.log("onLeftClick TEST COMPLETE")
	
	-- Test onTimer
	self.log("")
	self.log("Testing onTimer")
	s_jetpack:onTimer({})
	self.logResult(true, "onTimer")
	self.log("onTimer TEST COMPLETE")
	
	-- Test onServerTick
	self.log("")
	self.log("Testing onServerTick")
	s_jetpack:onServerTick({})
	self.logResult(true, "onServerTick")
	self.log("onServerTick TEST COMPLETE")
	
	-- Test onArrive
	self.log("")
	self.log("Testing onArrive")
	s_jetpack:onArrive({player = g_testerSD})
	self.logResult(true, "onArrive")
	self.log("onArrive TEST COMPLETE")
	
	-- Test onPlayerArrive
	self.log("")
	self.log("Testing onPlayerArrive")
	s_jetpack:onPlayerArrive({player = g_testerSD})
	self.logResult(true, "onPlayerArrive")
	self.log("onPlayerArrive TEST COMPLETE")
	
	-- Test onCollide
	self.log("")
	self.log("Testing onCollide")
	s_jetpack:onCollide({player = g_testerSD})
	self.logResult(true, "onCollide")
	self.log("onCollide TEST COMPLETE")
	
	-- Test onGetPlayerList
	self.log("")
	self.log("Testing onGetPlayerList")
	s_jetpack:onGetPlayerList({map = {{ID = g_tester}}})
	self.logResult(true, "onGetPlayerList")
	self.log("onGetPlayerList TEST COMPLETE")
	
	-- Test onKeyDown
	self.log("")
	self.log("Testing onKeyDown")
	s_jetpack:onKeyDown({player = g_testerSD, identifier = "jetPack", key = " "})
	self.logResult(true, "onKeyDown")
	self.log("onKeyDown TEST COMPLETE")
	
	-- Test onKeyUp
	self.log("")
	self.log("Testing onKeyUp")
	s_jetpack:onKeyUp({player = g_testerSD, identifier = "jetPack", key = " "})
	self.logResult(true, "onKeyUp")
	self.log("onKeyUp TEST COMPLETE")
	
	-- Test onJetPackRemove
	self.log("")
	self.log("Testing onJetPackRemove")
	s_jetpack:onJetPackRemove({playerName = g_testerName})
	self.logResult(true, "onJetPackRemove")
	self.log("onJetPackRemove TEST COMPLETE")
	
	-- Test onPlayerDied
	self.log("")
	self.log("Testing onPlayerDied")
	s_jetpack:onPlayerDied({player = g_testerSD})
	self.logResult(true, "onPlayerDied")
	self.log("onPlayerDied TEST COMPLETE")
	
	-- Test destroy
	self.log("")
	self.log("Testing destroy")
	s_jetpack:destroy()
	self.logResult(true, "destroy")
	self.log("destroy TEST COMPLETE")
	
	-- Test equipJetPack
	--self.log("")
	--self.log("equipJetPack() and 
	-- self.log("Testing equipJetPack("..tostring(g_testerSD)..")")
	-- s_jetpack:equipJetPack(g_testerSD)
	-- self.log("equipJetPack("..tostring(g_testerSD)..") TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function JetPack.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function JetPack.logResult(result, funcName, err)
	if result then JetPack.log("PASS")
	else JetPack.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end