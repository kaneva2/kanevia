--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_pet.lua
-- Tests all pet methods
----------------------------------------

Class.importClass("Class_Pet")

local NAME = "Pet"

local s_pet = ""

-- Node Objects
Pet = {}
Pet.__index = Pet

-- Creates a node object with the given PID
function Pet.create()
	local pet = {}
	pet.name = NAME
	pet.totalTests = 12
	setmetatable(pet, Pet)
	
	return pet
end

function Pet.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default pet")
	s_pet = Class_Pet.new()
	
	self.log("Pet instantiated ["..tostring(s_pet).."]")
	
	-- States for Pet -- TODO: If Pet's states are ever expanded, this table will need to expand accordingly
	local STATE = {IDLE      = 0,
				   CHASE     = 1,
				   RETURNING = 3
				  }
	
	-- Test setState
	self.log("")
	self.log("Testing setState")
	for name,state in pairs(STATE) do
		self.log("Testing setState("..tostring(state)..")")
		s_pet:setState(state)
	end
	self.logResult(true, "setState")
	self.log("setState TEST COMPLETE")
	
	-- Test enterIdleState
	self.log("")
	self.log("Testing enterIdleState")
	s_pet:enterIdleState()
	self.logResult(true, "enterIdleState")
	self.log("enterIdleState TEST COMPLETE")
	
	-- Test enterChaseState
	self.log("")
	self.log("Testing enterChaseState")
	s_pet:enterChaseState()
	self.logResult(true, "enterChaseState")
	self.log("enterChaseState TEST COMPLETE")
	
	-- Test enterReturningState
	self.log("")
	self.log("Testing enterReturningState")
	s_pet:enterReturningState()
	self.logResult(true, "enterReturningState")
	self.log("enterReturningState TEST COMPLETE")
	
	-- Test handleState
	self.log("")
	self.log("Testing handleState")
	for name, state in pairs(STATE) do
		s_pet:handleState(state)
	end
	self.logResult(true, "handleState")
	self.log("handleState TEST COMPLETE")
	
	-- Test handleIdleState
	self.log("")
	self.log("Testing handleIdleState")
	s_pet:handleIdleState()
	self.logResult(true, "handleIdleState")
	self.log("handleIdleState TEST COMPLETE")
	
	-- Test handleChaseState
	self.log("")
	self.log("Testing handleChaseState")
	s_pet:handleChaseState()
	self.logResult(true, "handleChaseState")
	self.log("handleChaseState TEST COMPLETE")
	
	-- Test handleFidgit
	self.log("")
	self.log("Testing handleFidgit")
	s_pet:handleFidgit()
	self.logResult(true, "handleFidgit")
	self.log("handleFidgit TEST COMPLETE")
	
	-- Test onMovementArrived
	self.log("")
	self.log("Testing onMovementArrived")
	s_pet:onMovementArrived()
	self.logResult(true, "onMovementArrived")
	self.log("onMovementArrived TEST COMPLETE")
	
	local x,y,z,rx,ry,rz = kgp.playerGetLocation(g_tester)
	local fauxPlayer = {ID = g_tester,
						name = kgp.playerGetName(g_tester),
						position = {x  = x,  y  = y,  z  = z,
									rx = rx, ry = ry, rz = rz}
					   }
	local fauxEvent = {player = fauxPlayer}
	-- Test onLeftClick
	self.log("")
	self.log("Testing onLeftClick")
	s_pet:onLeftClick(fauxEvent)
	self.logResult(true, "onLeftClick")
	self.log("onLeftClick TEST COMPLETE")
	
	-- Test onDepart
	self.log("")
	self.log("Testing onDepart")
	s_pet:onDepart(fauxEvent)
	self.logResult(true, "onDepart")
	self.log("onDepart TEST COMPLETE")
	
	-- Test onPlayerDepart
	self.log("")
	self.log("Testing onPlayerDepart")
	s_pet:onPlayerDepart(fauxEvent)
	self.logResult(true, "onPlayerDepart")
	self.log("onPlayerDepart TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Pet.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Pet.logResult(result, funcName, err)
	if result then Pet.log("PASS")
	else Pet.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end
