--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- actor.lua
-- Tests all actor methods
----------------------------------------

Class.importClass("Class_Actor")

local NAME = "Actor"

local s_actor = ""

-- Node Objects
Actor = {}
Actor.__index = Actor

-- Creates a node object with the given PID
function Actor.create()
	local actor = {}
	actor.name = NAME
	actor.totalTests = 7
	setmetatable(actor, Actor)
	
	return actor
end

function Actor.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default Actor")
	s_actor = Class_Actor.new()
	
	self.log("Actor instantiated ["..tostring(s_actor).."]")
	
	-- Test playGreetSound
	self.log("")
	self.log("Testing playGreetSound")
	s_actor:playGreetSound(g_tester)
	self.logResult(true, "playGreetSound")
	self.log("playerGreetSound TEST COMPLETE")
	
	-- Test getDistance
	self.log("")
	self.log("Testing getDistance between (0,0,0) to (10,10,10).")
	local testA = {x=0, y=0, z=0}
	local testB = {x=10, y=10, z=10}
	local expectedResult = 17.32
	local result = s_actor.getDistance(testA, testB)
	local diff = math.abs(result - expectedResult)
	if (result - expectedResult) <= .5 then
		self.logResult(true, "getDistance")
	else
		local error = "Returned result["..tostring(result).."]. Delta["..tostring(diff).."]"
		self.logResult(false, "getDistance", error)
	end
	self.log("getDistance TEST COMPLETE")
	
	-- Test updatePosition
	self.log("")
	self.log("Testing updatePosition")
	local x,y,z,rx,ry,rz = kgp.objectGetStartLocation(g_PID)
	s_actor:updatePosition()
	if x == s_actor.position.x and y == s_actor.position.y and z == s_actor.position.z and
	   rx == s_actor.position.rx and ry == s_actor.position.ry and rz == s_actor.position.rz then
		self.logResult(true, "updatePosition")
	else
		local error = "Position failed to update"
		self.logResult(false, "updatePosition", error)
	end
	self.log("updatePosition TEST COMPLETE")
	
	-- Generate fauxPlayer
	local x,y,z,rx,ry,rz = kgp.playerGetLocation(g_tester)
	local fauxPlayer = {ID = g_tester,
						name = kgp.playerGetName(g_tester),
						position = {x  = x,  y  = y,  z  = z,
									rx = rx, ry = ry, rz = rz}
					   }
	local fauxEvent = {player = fauxPlayer}
	-- Test leftClick
	self.log("")
	self.log("Testing onLeftClick")
	s_actor:onLeftClick(fauxEvent)
	self.logResult(true, "onLeftClick")
	self.log("onLeftClick TEST COMPLETE")
	
	-- Test onArrive
	self.log("")
	self.log("Testing onArrive")
	s_actor:onArrive(fauxEvent)
	self.logResult(true, "onArrive")
	self.log("onArrive TEST COMPLETE")
	
	-- Test onPlayerArrive
	self.log("")
	self.log("Testing onPlayerArrive")
	s_actor:onPlayerArrive(fauxEvent)
	self.logResult(true, "onPlayerArrive")
	self.log("onPlayerArrive TEST COMPLETE")
	
	-- Test onGetPlayerList
	self.log("")
	self.log("Testing onGetPlayerList")
	s_actor:onGetPlayerList({map = {player = g_testerSD}})
	self.logResult(true, "onGetPlayerList")
	self.log("onGetPlayerList TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Actor.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Actor.logResult(result, funcName, err)
	if result then Actor.log("PASS")
	else Actor.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end