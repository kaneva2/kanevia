--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_shopitem.lua
-- Tests all Shop Item methods
----------------------------------------

Class.importClass("Class_ShopItem")

local NAME = "ShopItem"

local s_shopItem = ""

-- Node Objects
ShopItem = {}
ShopItem.__index = ShopItem

local TEST_ACCESSORY = 4373370
local DEFAULT_DRINK_ANIM_M = 3981067
local DEFAULT_DRINK_ANIM_F = 3956621

-- Creates a node object with the given PID
function ShopItem.create()
	local shopItem = {}
	shopItem.name = NAME
	shopItem.totalTests = 5
	setmetatable(shopItem, ShopItem)
	
	return shopItem
end

function ShopItem.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default Shop Item class")
	s_shopItem = Class_ShopItem.new()
	
	self.log("Shop Item instantiated ["..tostring(s_shopItem).."] \n")
	
	-- Test onDepart
	self.log("")
	self.log("Testing onDepart")
	s_shopItem:onDepart({player = g_testerSD})
	self.logResult(true, "onDepart")
	self.log("onDepart TEST COMPLETE")
	
	-- Test onPlayerDepart
	self.log("")
	self.log("Testing onPlayerDepart")
	s_shopItem:onPlayerDepart({player = g_testerSD})
	self.logResult(true, "onPlayerDepart")
	self.log("onPlayerDepart TEST COMPLETE")
	
	-- Test onTrigger
	self.log("")
	self.log("Testing onTrigger")
	s_shopItem:onTrigger({triggerType = 1, player = g_testerSD, param = g_tester})
	self.logResult(true, "onTrigger")
	self.log("onTrigger TEST COMPLETE")
	
	-- Test onPurchase
	self.log("")
	self.log("Testing onPurchase")
	
	-- Coins test
	s_shopItem.itemName = "Coins Test"
	s_shopItem.itemCost = 1
	s_shopItem.itemIcon = "filestore11/6348325/7610766/PeachJuice.jpg"
	s_shopItem.itemEffects = {coins = 5}
	s_shopItem.itemType = "drink"
	s_shopItem.drinkAccessory = 4373370
	s_shopItem.drinkAnimM = DEFAULT_DRINK_ANIM_M
	s_shopItem.drinkAnimF = DEFAULT_DRINK_ANIM_F
	
	s_shopItem:onPurchase(g_testerSD)
	
	s_shopItem.itemEffects = {coins = -1}
	s_shopItem:onPurchase(g_testerSD)
	
	-- XP test
	s_shopItem.itemName = "XP Test"
	s_shopItem.itemEffects = {xp = 10}
	
	s_shopItem:onPurchase(g_testerSD)
	
	s_shopItem.itemEffects = {xp = -10}
	s_shopItem:onPurchase(g_testerSD)
	
	-- HP test
	s_shopItem.itemName = "HP Test"
	s_shopItem.itemEffects = {health = 10}
	
	s_shopItem:onPurchase(g_testerSD)
	
	s_shopItem.itemEffects = {health = -10}
	s_shopItem:onPurchase(g_testerSD)
	
	self.logResult(true, "onPurchase")
	self.log("onPurchase TEST COMPLETE")
	
	-- Test destroy
	self.log("")
	self.log("Testing destroy")
	s_shopItem:destroy()
	self.logResult(true, "destroy")
	self.log("destroy TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function ShopItem.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function ShopItem.logResult(result, funcName, err)
	if result then ShopItem.log("PASS")
	else ShopItem.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end