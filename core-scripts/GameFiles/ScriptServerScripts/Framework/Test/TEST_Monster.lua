--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_monster.lua
-- Tests all monster methods
----------------------------------------

Class.importClass("Class_Monster")

local NAME = "Monster"

local s_monster = ""

-- Node Objects
Monster = {}
Monster.__index = Monster

-- States for NPC
local STATE = {IDLE    = 0,
			   WANDER = 1,
			   CHASING = 2,
			   RETURNING = 4,
			   RESPAWNING = 5,
			  }

-- Creates a node object with the given PID
function Monster.create()
	local monster = {}
	monster.name = NAME
	monster.totalTests = 21
	setmetatable(monster, Monster)
	
	return monster
end

function Monster.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default monster class")
	s_monster = Class_Monster.new()
	
	self.log("Monster instantiated ["..tostring(s_monster).."]")
	
	-- Test setState
	self.log("")
	self.log("Testing setState")
	for id, state in pairs(STATE) do
		s_monster:setState(state)
	end
	self.logResult(true, "setState")
	self.log("setState TEST COMPLETE")
	
	-- Test enterIdleState
	self.log("")
	self.log("Testing enterIdleState")
	s_monster:enterIdleState()
	self.logResult(true, "enterIdleState")
	self.log("enterIdleState TEST COMPLETE")
	
	-- Test enterWanderState
	self.log("")
	self.log("Testing enterWanderState")
	s_monster:enterWanderState()
	self.logResult(true, "enterWanderState")
	self.log("enterWanderState TEST COMPLETE")
	
	-- Test enterChasingState
	self.log("")
	self.log("Testing enterChasingState")
	s_monster:enterChasingState()
	self.logResult(true, "enterChasingState")
	self.log("enterChasingState TEST COMPLETE")
	
	-- Test enterReturningState
	self.log("")
	self.log("Testing enterReturningState")
	s_monster:enterReturningState()
	self.logResult(true, "enterReturningState")
	self.log("enterReturningState TEST COMPLETE")
	
	-- Test enterRespawningState
	self.log("")
	self.log("Testing enterRespawningState")
	s_monster:enterRespawningState()
	self.logResult(true, "enterRespawningState")
	self.log("enterRespawningState TEST COMPLETE")
	
	-- Test handleState
	self.log("")
	self.log("Testing handleState")
	for id, state in pairs(STATE) do
		s_monster:handleState(state)
	end
	self.logResult(true, "handleState")
	self.log("handleState TEST COMPLETE")
	
	-- Test handleIdleState
	self.log("")
	self.log("Testing handleIdleState")
	s_monster:handleIdleState()
	self.logResult(true, "handleIdleState")
	self.log("handleIdleState TEST COMPLETE")
	
	-- Test handleChasingState
	self.log("")
	self.log("Testing handleChasingState")
	s_monster:handleChasingState()
	self.logResult(true, "handleChasingState")
	self.log("handleChasingState TEST COMPLETE")
	
	-- Test setWanderLocation
	self.log("")
	self.log("Testing setWanderLocation")
	s_monster:setWanderLocation()
	self.logResult(true, "setWanderLocation")
	self.log("setWanderLocation TEST COMPLETE")
	
	-- Test die
	self.log("")
	self.log("Testing die")
	s_monster:die()
	self.logResult(true, "die")
	self.log("die TEST COMPLETE")
	
	-- Test respawn
	self.log("")
	self.log("Testing respawn")
	s_monster:respawn()
	self.logResult(true, "respawn")
	self.log("respawn TEST COMPLETE")
	
	-- Test onTimer
	self.log("")
	self.log("Testing onTimer")
	s_monster:onTimer({tick = 1000})
	self.logResult(true, "onTimer")
	self.log("onTimer TEST COMPLETE")
	
	-- Test onServerTick
	self.log("")
	self.log("Testing onServerTick")
	s_monster:onServerTick({tick = 1000})
	self.logResult(true, "onServerTick")
	self.log("onServerTick TEST COMPLETE")
	
	-- Test onMovementArrived
	self.log("")
	self.log("Testing onMovementArrived")
	for id, state in pairs(STATE) do
		s_monster:onMovementArrived(state)
	end
	self.logResult(true, "onMovementArrived")
	self.log("onMovementArrived TEST COMPLETE")
	
	-- Test onTrigger
	self.log("")
	self.log("Testing onTrigger")
	s_monster:onTrigger({triggerType = 1, player = g_testerSD})
	self.logResult(true, "onTrigger")
	self.log("onTrigger TEST COMPLETE")
	
	-- Test onDamage
	self.log("")
	self.log("Testing onDamage")
	s_monster:onDamage({damage = 10, attacker = g_testerSD})
	self.logResult(true, "onDamage")
	self.log("onDamage TEST COMPLETE")
	
	-- Test onPlayerDied
	self.log("")
	self.log("Testing onPlayerDied")
	s_monster:onPlayerDied({player = g_testerSD})
	self.logResult(true, "onPlayerDied")
	self.log("onPlayerDied TEST COMPLETE")
	
	-- Test onArrive
	self.log("")
	self.log("Testing onArrive")
	s_monster:onArrive({player = g_testerSD})
	self.logResult(true, "onArrive")
	self.log("onArrive TEST COMPLETE")
	
	-- Test onPlayerArrive
	self.log("")
	self.log("Testing onPlayerArrive")
	s_monster:onPlayerArrive({player = g_testerSD})
	self.logResult(true, "onPlayerArrive")
	self.log("onPlayerArrive TEST COMPLETE")
	
	-- Test onGetPlayerList
	self.log("")
	self.log("Testing onGetPlayerList")
	s_monster:onGetPlayerList({map = {player = g_testerSD}})
	self.logResult(true, "onGetPlayerList")
	self.log("onGetPlayerList TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Monster.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Monster.logResult(result, funcName, err)
	if result then Monster.log("PASS")
	else Monster.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end