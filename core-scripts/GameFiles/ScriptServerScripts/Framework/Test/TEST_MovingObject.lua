--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_movingobject.lua
-- Tests all monster methods
----------------------------------------

Class.importClass("Class_MovingObject")

local NAME = "MovingObject"

local s_movingObject = ""

-- Node Objects
MovingObject = {}
MovingObject.__index = MovingObject

-- Creates a node object with the given PID
function MovingObject.create()
	local movingObject = {}
	movingObject.name = NAME
	movingObject.totalTests = 9
	setmetatable(movingObject, MovingObject)
	
	return movingObject
end

function MovingObject.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default monster class")
	s_movingObject = Class_MovingObject.new()
	
	self.log("Moving Object instantiated ["..tostring(s_movingObject).."]")
	
	-- Test onTimer
	self.log("")
	self.log("Testing onTimer")
	s_movingObject:onTimer({tick = 1000})
	self.logResult(true, "onTimer")
	self.log("onTimer TEST COMPLETE")
	
	-- Test onServerTick
	self.log("")
	self.log("Testing onServerTick")
	s_movingObject:onServerTick({tick = 1000})
	self.logResult(true, "onServerTick")
	self.log("onServerTick TEST COMPLETE")
	
	-- Test onLeftClick
	self.log("")
	self.log("Testing onLeftClick")
	s_movingObject:onLeftClick({player = g_testerSD})
	self.logResult(true, "onLeftClick")
	self.log("onLeftClick TEST COMPLETE")
	
	-- Test onCollide
	self.log("")
	self.log("Testing onCollide")
	s_movingObject:onCollide({player = g_testerSD})
	self.logResult(true, "onCollide")
	self.log("onCollide TEST COMPLETE")
	
	-- Test onTrigger
	self.log("")
	self.log("Testing onTrigger")
	s_movingObject:onTrigger({triggerType = 1})
	self.logResult(true, "onTrigger")
	self.log("onTrigger TEST COMPLETE")
	
	-- Test onMovementArrived
	self.log("")
	self.log("Testing onMovementArrived")
	s_movingObject:onMovementArrived()
	self.logResult(true, "onMovementArrived")
	self.log("onMovementArrived TEST COMPLETE")
	
	-- Test onRotateComplete
	self.log("")
	self.log("Testing onRotateComplete")
	s_movingObject:onRotateComplete()
	self.logResult(true, "onRotateComplete")
	self.log("onRotateComplete TEST COMPLETE")
	
	local x,y,z,rx,ry,rz = kgp.objectGetStartLocation(s_movingObject.PID)
	s_movingObject.move = {x = 2, y = 2, z = 2}
	-- Test updateLocs
	self.log("")
	self.log("Testing updateLocs")
	s_movingObject:updateLocs()
	local startLoc = s_movingObject.startLoc
	local endLoc   = s_movingObject.endLoc
	local move 	   = s_movingObject.move
	if startLoc.x == x and startLoc.y == y and startLoc.z == z and startLoc.rx == rx and startLoc.ry == ry and startLoc.rz == rz and
	   endLoc.x == (x + move.x) and endLoc.y == (y + move.y) and endLoc.z == (z + move.z) then
		self.logResult(true, "updateLocs")
	else
		local error = "Updated coordinates don't match actual location"
		self.logResult(false, "updateLocs", error)
	end
	self.log("updateLocs TEST COMPLETE")
	
	-- Test destroy
	self.log("")
	self.log("Testing destroy")
	s_movingObject:destroy()
	self.logResult(true, "destroy")
	self.log("destroy TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE \n")
	
	testNext()
end

function MovingObject.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function MovingObject.logResult(result, funcName, err)
	if result then MovingObject.log("PASS")
	else MovingObject.log("FAIL - ["..tostring(err).."]") end
	
	MovingObject.log("reportResult("..tostring(result)..", "..tostring(NAME)..", "..tostring(funcName)..", "..tostring(err)..")")
	reportResult(result, NAME, funcName, err)
end