--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Unit test for Lib_Events.lua

local testEvent = {
	sender = { ID = 1546513154, name = "chirho" },
	name = "Test",
	health = 100,
	level = 10,
	attacking = true,
}

local timerEvent = {
	ID = 95,
	message = "TIMEZUP",
}

TEST( "Interface", function( test )
	test:assertType(Events, "table")
	test:expectMethod(Events, "encode")
	test:expectMethod(Events, "decode")
	test:expectMethod(Events, "sendEvent")
	test:expectMethod(Events, "sendClientEvent")
	test:expectMethod(Events, "registerHandler")
	test:expectMethod(Events, "unregisterHandler")
	test:expectMethod(Events, "tellAllPlayers")
	test:expectMethod(Events, "tellAllDevelopers")
	test:expectMethod(Events, "setTimer")
	test:expectMethod(Events, "cancelTimer")
	test:expectMethod(Events, "setTrigger")
	test:expectMethod(Events, "setTriggerOnPosition")
	test:expectMethod(Events, "removeTrigger")
end)

TEST( "Encoding", function( test )
	test:assertType(Events, "table")

	test:assertMethod(Events, "encode")
	local encoded = Events.encode(testEvent)
	test:expectType(encoded, "string")

	test:assertMethod(Events, "decode")
	local event2 = Events.decode(encoded)
	test:expectEQ(testEvent, event2)
end)

local eventReceived
local waitTime = 1000

TEST( "Sending", function( test )
	test:assertType(Events, "table")

	test:assertMethod(Events, "registerHandler")
	Events.registerHandler("LOCAL_EVENT1", function(event)
		eventReceived = event
	end)

	eventReceived = nil
	test:assertMethod(Events, "sendEvent")
	Events.sendEvent("LOCAL_EVENT1", testEvent)
	test:log("[.....] LOCAL_EVENT1 Sent")

	test:log("[.....] Wait for " .. tostring(waitTime) .. " ms")
	return waitTime		-- 1 second delay before next task
end)

TEST( "Receiving", function( test )
	test:log("[.....] " .. tostring(waitTime) .. " ms elapsed")
	if eventReceived~=nil then
		test:log("[.....] LOCAL_EVENT1 received")
	else
		test:log("[.....] LOCAL_EVENT1 NOT received")
	end
	test:expectEQ(testEvent, eventReceived)
end)

local timerDuration = 0.5
local timeSet, timeAlerted

TEST( "TimerSet", function( test )
	test:assertType(Events, "table")

	test:assertMethod(Events, "setTimer")
	test:assertMethod(kgp, "scriptGetTick")
	eventReceived = nil
	timeAlerted = nil
	Events.setTimer(timerDuration, function(event)
		timeAlerted = kgp.scriptGetTick()
		eventReceived = event
	end, nil, timerEvent)

	test:log("[.....] Timer set, duration: " .. tostring(timerDuration) .. " seconds")
	timeSet = kgp.scriptGetTick()

	test:log("[.....] Wait for " .. tostring(waitTime) .. " ms")
	return waitTime		-- 1 second delay before next task
end)

TEST( "TimerAlert", function( test )
	test:log("[.....] " .. tostring(waitTime) .. " ms elapsed")
	if timeAlerted~=nil then
		test:log("[.....] Timer alerted")
	else
		test:log("[.....] Timer NOT alerted")
	end

	test:expectEQ(timerEvent, eventReceived)
	test:assertNE(timeAlerted, nil)

	local elapsed = timeAlerted - timeSet
	test:log("[.....] Timer alerted after " .. tostring(elapsed) .. " ms")
	test:expectGE(elapsed, timerDuration*1000)
	test:expectLE(elapsed, timerDuration*1000 + 100)	-- 100 ms upper bound tolerance
end)
