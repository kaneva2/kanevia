--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_object.lua
-- Tests all object methods
----------------------------------------

Class.importClass("Class_Object")

local NAME = "Object"

local s_object = ""

-- Node Objects
Object = {}
Object.__index = Object

local STATE = {IDLE = 0}

-- Creates a node object with the given PID
function Object.create()
	local object = {}
	object.name = NAME
	object.totalTests = 0
	setmetatable(object, Object)
	
	return object
end

function Object.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default monster class")
	s_object = Class_Object.new()
	
	self.log("Object instantiated ["..tostring(s_object).."] \n")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Object.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Object.logResult(result, funcName, err)
	if result then Object.log("PASS")
	else Object.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end