--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_trap.lua
-- Tests all Trap methods
----------------------------------------

Class.importClass("Class_Trap")

local NAME = "Trap"

local s_trap = ""

-- Node Objects
Trap = {}
Trap.__index = Trap

-- Creates a node object with the given PID
function Trap.create()
	local trap = {}
	trap.name = NAME
	trap.totalTests = 8
	setmetatable(trap, Trap)
	
	return trap
end

function Trap.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default Trap class")
	s_trap = Class_Trap.new()
	
	self.log("Trap instantiated ["..tostring(s_trap).."] \n")
	
	-- Test onArrive
	self.log("")
	self.log("Testing onArrive")
	s_trap:onArrive({player = g_testerSD})
	self.logResult(true, "onArrive")
	self.log("onArrive TEST COMPLETE")
	
	-- Test onPlayerArrive
	self.log("")
	self.log("Testing onPlayerArrive")
	s_trap:onPlayerArrive({player = g_testerSD})
	self.logResult(true, "onPlayerArrive")
	self.log("onPlayerArrive TEST COMPLETE")
	
	-- Test onGetPlayerList
	self.log("")
	self.log("Testing onGetPlayerList")
	s_trap:onGetPlayerList({map = {player = g_testerSD}})
	self.logResult(true, "onGetPlayerList")
	self.log("onGetPlayerList TEST COMPLETE")
	
	-- Test onTimer
	self.log("")
	self.log("Testing onTimer")
	s_trap:onTimer({tick = 1000})
	self.logResult(true, "onTimer")
	self.log("onTimer TEST COMPLETE")
	
	-- TestonServerTick
	self.log("")
	self.log("Testing onServerTick")
	s_trap:onServerTick({tick = 1000})
	self.logResult(true, "onServerTick")
	self.log("onServerTick TEST COMPLETE")
	
	-- Test onTrigger
	self.log("")
	self.log("Testing onTrigger")
	s_trap:onTrigger({triggerType = 1, player = g_testerSD})
	s_trap:onTrigger({triggerType = 2, player = g_testerSD})
	self.logResult(true, "onTrigger")
	self.log("onTrigger TEST COMPLETE")
	
	-- Test onPlayerDied
	self.log("")
	self.log("Testing onPlayerDied")
	s_trap:onPlayerDied({player = g_testerSD})
	self.logResult(true, "onPlayerDied")
	self.log("onPlayerDied TEST COMPLETE")
	
	-- Test destroy
	self.log("")
	self.log("Testing destroy")
	s_trap:destroy()
	self.logResult(true, "destroy")
	self.log("destroy TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function Trap.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function Trap.logResult(result, funcName, err)
	if result then Trap.log("PASS")
	else Trap.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end