--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

----------------------------------------
-- TEST_dancefloor.lua
-- Tests all dance floor methods
----------------------------------------

Class.importClass("Class_DanceFloor")

local NAME = "DanceFloor"

local s_danceFloor = ""

-- Node Objects
DanceFloor = {}
DanceFloor.__index = DanceFloor

-- Creates a node object with the given PID
function DanceFloor.create()
	local danceFloor = {}
	danceFloor.name = NAME
	danceFloor.totalTests = 3
	setmetatable(danceFloor, DanceFloor)
	
	return danceFloor
end

function DanceFloor.test(self)
	self.log("BEGINNING TEST")
	
	-- Instantiation
	self.log("Instantiating default dance floor class")
	s_danceFloor = Class_DanceFloor.new()
	
	self.log("Dance Floor instantiated ["..tostring(s_danceFloor).."]")
	
	-- Test addPlayerToNode
	self.log("")
	self.log("Testing addPlayerToNode")
	s_danceFloor:addPlayerToNode(g_testerSD, "1")
	-- Add a dancable node
	s_danceFloor.playerPositions= {male={x=0, y=0.25, z=0}, female={x=0, y=0.25, z=0}}
	s_danceFloor:addPlayerToNode(g_testerSD, "1")
	self.logResult(true, "addPlayerToNode")
	self.log("addPlayerToNode TEST COMPLETE")
	
	-- Test removePlayerFromNode
	self.log("")
	self.log("Testing removePlayerFromNode")
	s_danceFloor:removePlayerFromNode(g_testerSD, "1")
	self.logResult(true, "removePlayerFromNode")
	self.log("removePlayerFromNode TEST COMPLETE")

	-- Test destroy
	self.log("")
	self.log("Testing destroy")
	s_danceFloor:destroy()
	self.logResult(true, "destroy")
	self.log("destroy TEST COMPLETE")
	
	-- Test complete
	self.log("TEST COMPLETE")
	
	testNext()
end

function DanceFloor.log(message)
	if g_tester then
		kgp.playerSendEvent(g_tester, "FRAMEWORK_DEBUGGER", "LOG", tostring(NAME).." - "..tostring(message))
	end
end

-- Log standard function return
function DanceFloor.logResult(result, funcName, err)
	if result then DanceFloor.log("PASS")
	else DanceFloor.log("FAIL - ["..tostring(err).."]") end
	
	reportResult(result, NAME, funcName, err)
end