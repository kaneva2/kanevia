--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------

-- Lua language test

TEST( "Packages", function( test )
	-- Available packages
	test:assertType  (math,   "table")
	test:assertMethod(math,   "abs")
	test:assertType  (string, "table")
	test:assertMethod(string, "sub")
	test:assertType  (table,  "table")
	test:assertMethod(table,  "insert")
	test:assertType  (debug,  "table")
	test:assertMethod(debug,  "sethook")

	-- Global namespace
	test:assertType(type, "function")
	test:assertType(rawget, "function")
	test:assertType(rawset, "function")
	test:assertType(tonumber, "function")
	test:assertType(tostring, "function")
	test:assertType(pairs, "function")
	test:assertType(ipairs, "function")
	test:assertType(next, "function")
	test:assertType(rawget, "function")
	test:assertType(rawset, "function")
	test:assertType(_G, "table")

	-- Disabled packages
	test:assertType(coroutine, "nil")
	test:assertType(io, "nil")
	test:assertType(file, "nil")
end)

TEST( "DataTypes", function( test )
    test:expectType(123,   "number")
    test:expectType("abc", "string")
    test:expectType({},    "table")
	test:expectType(true,  "boolean")
	test:expectType(false, "boolean")
	test:expectType(nil,   "nil")
    test:expectType(function() end, "function")

	local str = "3.14"
	local num = tonumber(str)
	test:assertType(num, "number")
	test:expectEQ(num, 3.14)
end)

TEST( "Randomizations", function( test )
	test:assertType(math, "table")
	test:assertType(math.random, "function")
	test:assertType(math.floor, "function")

	local r1 = math.random()
	local r2 = math.random()
	test:assertNE(r1, r2)

	local SAMPLE_SIZE = 100000
	local NUM_BUCKETS = 10
	local TOLERANCE = 0.05		-- +/- 5%

	local buckets = {}
	for i = 1, SAMPLE_SIZE do
		local r = math.random()
		local bucketId = math.floor(r*NUM_BUCKETS)+1
		buckets[bucketId] = (buckets[bucketId] or 0) + 1
	end

	for i = 1, NUM_BUCKETS do
		test:expectNear(buckets[i], SAMPLE_SIZE/NUM_BUCKETS, SAMPLE_SIZE*TOLERANCE)
	end
end)

TEST( "LogicsAndBranches", function( test )
	if not true then
		test:fail("if not true")
	end
	if not 1 then
		test:fail("if not 1")
	end
	if not "hello" then
		test:fail("if not \"hello\"")
	end
	if not {} then
		test:fail("if not {}")
	end
	if not function() end then
		test:fail("if not function() end")
	end
	if false then
		test:fail("if false")
	end
	if nil then
		test:fail("if false")
	end

	test:expectGT(3, 2)
	test:expectLT("hello", "hi")
	test:expectTrue(true or false)
	test:expectFalse(true and false)
	test:expectEQ(nil or "test", "test")
	test:expectEQ(nil or 4, 4)
	test:expectEQ(false or "test", "test")
	test:expectEQ(false or 4, 4)
end)

TEST( "Maths", function( test )
	test:expectMethod(math, "abs")
	test:expectMethod(math, "min")
	test:expectMethod(math, "max")
	test:expectMethod(math, "pow")
	test:expectMethod(math, "sqrt")
	test:expectMethod(math, "sin")
	test:expectMethod(math, "cos")
	test:expectMethod(math, "tan")
	test:expectMethod(math, "atan")
	test:expectMethod(math, "atan2")

	test:expectEQ(0.142857 * 0.4375, 0.0624999375)
	test:expectEQ(math.min(2.7, -3.1), -3.1)
	test:expectEQ(math.max(2.7, -3.1), 2.7)
	test:expectEQ(math.pow(2, 1/12), 1.0594630943593)
	test:expectEQ(math.sqrt(2), 1.414213562373)
	test:expectEQ(math.sin(3.141592654*30/180), 0.5)
	test:expectEQ(math.cos(3.141592654*30/180), 0.8660254037844)
	test:expectEQ(math.atan2(0.5, 0.8660254037844), 3.141592654*30/180)
	test:expectEQ(math.atan2(-0.5, 0.8660254037844), -3.141592654*30/180)
	test:expectEQ(math.atan2(0.5, -0.8660254037844), 3.141592654*150/180)
	test:expectEQ(math.atan2(-0.5, -0.8660254037844), -3.141592654*150/180)
end)

TEST( "Strings", function( test )
	test:assertMethod(string, "byte")
	test:assertMethod(string, "char")
	test:assertMethod(string, "find")
	test:assertMethod(string, "format")
	test:assertMethod(string, "gmatch")
	test:assertMethod(string, "gsub")
	test:assertMethod(string, "len")
	test:assertMethod(string, "lower")
	test:assertMethod(string, "match")
	test:assertMethod(string, "rep")
	test:assertMethod(string, "reverse")
	test:assertMethod(string, "sub")
	test:assertMethod(string, "upper")

	test:expectEQ(string.sub("this is a test", 1, 4), "this")
	test:expectEQ(string.sub("this is a test", 6, -6), "is a")
	test:expectEQ(string.sub("this is a test", -4, -1), "test")
	test:expectEQ(string.len("this is a test"), 14)

	local i=0
	local tokens = {"this", "is", "a", "test"}
	for token in string.gmatch("this is a test", "%a+") do
		i = i + 1
		test:expectEQ(token, tokens[i])
	end

	test:expectEQ(string.upper("HellO WorlD"), "HELLO WORLD")
	test:expectEQ(string.lower("HellO WorlD"), "hello world")
	test:expectEQ(string.reverse("dlrow olleh"), "hello world")
	test:expectEQ(string.rep("hello ", 2), "hello hello ")
	test:expectEQ(string.gsub("hello world", "(%w+)", "'%1'"), "'hello' 'world'")
	test:expectEQ({string.find("hello world", "(l+)")}, {3, 4, "ll"})
	test:expectEQ({string.find("hello world", "world")}, {7, 11})
	test:expectNil(string.find("hello world", "World"))
	test:expectEQ(string.format("hello %s", "world"), "hello world")
	test:expectEQ(string.format("hello %d", 123), "hello 123")
	test:expectEQ(string.format("hello %05d", 123), "hello 00123")
	test:expectEQ(string.format("hello %x", 123), "hello 7b")
	test:expectEQ(string.format("hello %q", "world"), "hello \"world\"")
end)

TEST( "Tables", function( test )
	test:assertMethod(table, "concat")
	test:assertMethod(table, "insert")
	test:assertMethod(table, "remove")
	test:assertMethod(table, "maxn")
	test:assertMethod(table, "sort")

	-- index array
	local tokens = {"this", "is", "a", "test"}
	test:expectEQ(table.remove(tokens, 3), "a")
	test:expectEQ(table.remove(tokens, 2), "is")
	test:expectEQ(tokens, {"this", "test"})
	test:expectEQ(table.remove(tokens), "test")
	table.insert(tokens, "world")
	test:expectEQ(tokens, {"this", "world"})
	test:expectEQ(table.remove(tokens, 1), "this")
	table.insert(tokens, 1, "hello")
	test:expectEQ(tokens, {"hello", "world"})
	test:expectEQ(table.concat(tokens, " "), "hello world")

	-- sparse index array
	test:expectEQ(#tokens, 2)
	test:expectNil(tokens[100])
	tokens[100] = "kaneva"
	test:expectEQ(#tokens, 2)
	test:expectEQ(tokens[100], "kaneva")

	-- associative array
	local object = {}
	object.name = "table"
	object.price = 100
	object.getTag = function(self) return "A " .. self.name .. " listed for " .. tostring(self.price) .. " credits" end
	test:expectField(object, "name", "table")
	test:expectField(object, "price", 100)
	test:expectMethod(object, "getTag")
	test:expectEQ(object:getTag(), "A table listed for 100 credits")
end)

TEST( "Metatables", function( test )
	-- metatable
	local object = {}
	object.name = "table"
	object.price = 100
	local Table = { type = "Furniture" }
	test:expectNil(object["type"])
	setmetatable(object, {
		__index = Table, 
		__call = 
			function(self) 
				return "A " .. self.name .. " listed for " .. tostring(self.price) .. " credits"
			end
	})
	test:expectEQ(object(), "A table listed for 100 credits")
	test:expectField(object, "type", "Furniture")

	test:assertType(rawget, "function")
	test:expectNil(rawget(object, "type"))

	object.comment = "Comment in object table"
	test:expectNil(Table.comment)
	test:expectField(object, "comment", "Comment in object table")
	test:expectType(rawget(object, "comment"), "string")

	-- Union of sets with __concat metamethod
	local set1 = { 3, 5, 6 }
	local set2 = { 4, 10, 6, 3 }
	setmetatable(set1, { 
		__concat = function(lhs, rhs)
			local res = {}
			for _, v in ipairs(lhs) do
				table.insert(res, v)
			end
			for _, v in ipairs(rhs) do
				table.insert(res, v)
			end
			table.sort(res)
			for i=#res,2,-1 do
				if res[i]==res[i-1] then
					table.remove(res, i)
				end
			end
			return res
		end
	})
	local merged = set1 .. set2
	test:expectEQ(merged, { 3, 4, 5, 6, 10 })

	-- Vector ops with metamethods
	local vec1 = { x = 10, y = 5, z = 2 }
	local vec2 = { x = -4, y = 2000, z = 14 }
	local vec
	setmetatable(vec1, { 
		__unm = function(lhs) return {x=-lhs.x, y=-lhs.y, z=-lhs.z} end,
		__add = function(lhs, rhs) return {x=lhs.x+rhs.x, y=lhs.y+rhs.y, z=lhs.z+rhs.z} end,
		__sub = function(lhs, rhs) return {x=lhs.x-rhs.x, y=lhs.y-rhs.y, z=lhs.z-rhs.z} end,
		__mul = function(lhs, rhs) return {x=lhs.x*rhs.x, y=lhs.y*rhs.y, z=lhs.z*rhs.z} end,
		__div = function(lhs, rhs) return {x=lhs.x/rhs, y=lhs.y/rhs, z=lhs.z/rhs} end,
		__mod = function(lhs, rhs) return {x=lhs.x%rhs, y=lhs.y%rhs, z=lhs.z%rhs} end,
		__pow = function(lhs, rhs) return {x=lhs.x^rhs, y=lhs.y^rhs, z=lhs.z^rhs} end,
	})
	vec = - vec1
	test:expectEQ(vec.x, -10)
	test:expectEQ(vec.y, -5)
	test:expectEQ(vec.z, -2)
	vec = vec1 + vec2
	test:expectEQ(vec.x, 6)
	test:expectEQ(vec.y, 2005)
	test:expectEQ(vec.z, 16)
	vec = vec1 - vec2
	test:expectEQ(vec.x, 14)
	test:expectEQ(vec.y, -1995)
	test:expectEQ(vec.z, -12)
	vec = vec1 * vec2
	test:expectEQ(vec.x, -40)
	test:expectEQ(vec.y, 10000)
	test:expectEQ(vec.z, 28)
	vec = vec1 / 2
	test:expectEQ(vec.x, 5)
	test:expectEQ(vec.y, 2.5)
	test:expectEQ(vec.z, 1)
	vec = vec1 % 2
	test:expectEQ(vec.x, 0)
	test:expectEQ(vec.y, 1)
	test:expectEQ(vec.z, 0)
	vec = vec1 ^ 3
	test:expectEQ(vec.x, 1000)
	test:expectEQ(vec.y, 125)
	test:expectEQ(vec.z, 8)
end)

TEST( "HigherOrderFunctions", function( test )
	-- Curry
	local makePower = function(exp)
		return
			function(base)
				return math.pow(base, exp)
			end
	end
	
	local sq = makePower(2)
	test:expectEQ(sq(25), 625)

	local sqrt = makePower(1/2)
	test:expectEQ(sqrt(25), 5)

	local cu = makePower(3)
	test:expectEQ(cu(27), 19683)

	local curt = makePower(1/3)
	test:expectEQ(curt(27), 3)

	-- Compound
	local compound = function(f1, f2)
		return
			function(...)
				return f1(f2(...))
			end
	end

	local ident = compound(sqrt, sq)
	test:expectEQ(ident(100), 100)

	local sincos = function(theta) return math.sin(theta), math.cos(theta) end
	local len2d = function(x, y) return math.sqrt(x*x + y*y) end

	local alwaysOne = compound(len2d, sincos)
	test:expectEQ(alwaysOne(math.random()), 1)

	local range = function(min, max)
		local tmp = {}
		for i=min, max do
			table.insert(tmp, i)
		end
		return tmp
	end

	local arrayOp = function(op)
		return
			function(array)
				local tmp = {}
				for i, v in ipairs(array) do
					tmp[i] = op(v)
				end
				return tmp
			end
	end

	local squareSeries = compound(arrayOp(sq), range)
	test:expectEQ(squareSeries(4, 9), {16, 25, 36, 49, 64, 81})
end)

TEST( "MapReduce", function( test )
	local map = function(array, op)
		local tmp = {}
		for i, v in ipairs(array) do
			tmp[i] = op(v)
		end
		return tmp
	end

	local reduce = function(array, op, acc)
		for i, v in ipairs(array) do
			acc = op(acc, v)
		end
		return acc
	end

	local inc = function(n) return (n or 0) + 1 end
	local sum = function(s, v) return (s or 0) + v end

	local text = {"Lorem", "ipsum", "dolor", "sit", "amet,", "consectetuer", "adipiscing", "elit.", "Aenean", "commodo", "ligula", "eget", "dolor.", "Aenean", "massa."}
	local wordCount = reduce(map(text, string.len), inc)
	local charCount = reduce(map(text, string.len), sum)
	test:expectEQ(wordCount, 15)
	test:expectEQ(charCount, 91)
end)

TEST( "Closures", function( test )
	local accumulator = function()
		local sum = 0
		return
			function(v)
				sum = sum + (v or 0)
				return sum
			end
	end

	local acc1 = accumulator()
	local acc2 = accumulator()
	acc1(1) 	acc2(2)
	acc1(3) 	acc2(4)
	acc1(5) 	acc2(6)
	acc1(7) 	acc2(8)
	acc1(9) 	acc2(10)

	test:assertEQ(acc1(), 25)
	test:assertEQ(acc2(), 30)
end)
