--------------------------------------------------------------------------------
-- Copyright Kaneva 2001-2020
-- This file is licensed under the PolyForm Noncommercial License 1.0.0
-- https://polyformproject.org/licenses/noncommercial/1.0.0/ 
--------------------------------------------------------------------------------



------------------------------------------------------------------------------------------------------------
--STRUCTURES
------------------------------------------------------------------------------------------------------------

Player = {}
Player.__index = Player

function Player.create(id)
	local player = {}             -- our new object
	setmetatable(player,Player)
	player.id = id
	player.name = kgp.playergetname(id)
	return player
end

------------------------------------------------------------------------------------------------------------
--STRUCTURES
------------------------------------------------------------------------------------------------------------

GameZone = {}
GameZone.__index = GameZone

function GameZone.create(id, name)
	local gameZone = {}             -- our new object
	setmetatable(gameZone,GameZone)
	gameZone.id = id
	gameZone.name = name
	gameZone.instances = {}
	return gameZone
end


------------------------------------------------------------------------------------------------------------
--STRUCTURES
------------------------------------------------------------------------------------------------------------

GameInstance = {}
GameInstance.__index = GameInstance

function GameInstance.create(id)
	local gameInstance = {}             -- our new object
	setmetatable(gameInstance,GameInstance)
	gameInstance.id = id
	gameInstance.locked = false
	gameInstance.players = {}
	return gameInstance
end

------------------------------------------------------------------------------------------------------------
--GAME VARIABLES
------------------------------------------------------------------------------------------------------------
MAX_PLAYERS_PER_ZONE = 8

players = {}
game_zones = {}
updateTimer = 5000
g_curTick = -1

GAME_MASTER_NAME = "Game Master"
CHOOSE_INSTANCE = "GameZone_ChooseInstance.xml"
CHOOSE_GAME = "GameZone_ChooseGame.xml"

--EVENT_FILTERS
REQUEST_ZONE_INFO = 1
RESPONSE_ZONE_INFO = 2
REQUEST_INSTANCE_INFO = 3
RESPONSE_INSTANCE_INFO = 4
CREATE_INSTANCE = 5
JOIN_INSTANCE = 6
CMD_LOOT = 2009

------------------------------------------------------------------------------------------------------------
--GAME HANDLERS
------------------------------------------------------------------------------------------------------------

--when the script loads this is called, user: the id of the person who arrives first in the zone or drops the script
function kgp_start(user) 
	addPlayer(user)
	tellPlayers("User: " .. kgp.playergetname(user) .. " arrived")
	kgp.playerloadmenu(user, "3DAppsWelcome.xml")
	updateGameInstances()
end

function kgp_timer(tick) --occurs every ~second, tick in milliseconds
	tickDiff = 0
	if(g_curTick ~= -1) then
		tickDiff = tick - g_curTick
	end
	g_curTick = tick
	updateTimer = updateTimer - tickDiff
	
	if updateTimer < 0 then
		updateTimer = 5000
		updateGameInstances()
	end
end

function kgp_arrive(user) --when a player arrives
    index = getPlayerIndex(user)
    if index == -1 then --player not already there
    	addPlayer(user)
		tellPlayers("User: " .. kgp.playergetname(user) .. " arrived")
		kgp.playerloadmenu(user, "3DAppsWelcome.xml")
    end
end

function kgp_depart(user) --when a player departs
	local player = getPlayer(user)
	if player ~= nil then
		tellPlayers("User: " .. player.name .. " departed")
		removePlayer(user)
	end
end

function kgp_menuevent( user, params ) --handler for a menu event sent from the client UI, params: arbitrary number of arguments sent from the client
	local filter = tonumber(params[1])
	
	if filter == CMD_LOOT then --NPC was triggered in lobby
		kgp.playerloadmenu(user, CHOOSE_INSTANCE)
		sendGameInstancesInfo(user, -1)
	elseif filter == REQUEST_ZONE_INFO then
		sendGameZoneInfo(user)
	elseif filter == REQUEST_INSTANCE_INFO then
		local gameZoneId = tonumber(params[2])
		sendGameInstancesInfo(user, gameZoneId)
	elseif filter == CREATE_INSTANCE then
		local gameZoneId = tonumber(params[2])
		createInstance(user, gameZoneId)
	elseif filter == JOIN_INSTANCE then
		local instanceId = tonumber(params[2])
		joinInstance(user, instanceId)
	end
end
------------------------------------------------------------------------------------------------------------
--HELPER FUNCTIONS
------------------------------------------------------------------------------------------------------------

function addPlayer(id)
	for i,v in ipairs(players) do
		if v.id == id then -- player already exists
			return
		end
	end
	local player = Player.create(id)
	table.insert(players, player)
	sendPlayerStartData(id)
end

function removePlayer(id)
	for i,v in ipairs(players) do
		if v.id == id then
			table.remove(players, i)
			return
		end
	end
end

function getPlayerIndex(user)
	index = -1
	for i,v in ipairs(players) do
		if v.id == user then
			return i
		end
	end
	return index
end

function getPlayer(user)
	for i,v in ipairs(players) do
		if v.id == user then
			return v
		end
	end
	return nil
end

function tellPlayers(message)
	for i,v in ipairs(players) do
		kgp.playertell(v.id, message, GAME_MASTER_NAME, 1)
	end
end

function sendPlayerStartData(user)

end

function createInstance(user, gameZoneId)
	local instanceID = kgp.gamecreateinstance(gameZoneId, user)
	if instanceID == -1 then
		kgp.playertell(user, "Could not create new app instance", GAME_MASTER_NAME, 1)
	else
		kgp.playertell(user, "Creation Successful! Spawning to new app instance..." .. instanceID, GAME_MASTER_NAME, 1)
		updateGameInstances()
	end
end

function joinInstance(user, instanceId)
	local success = kgp.playerjoininstance(instanceId, user)
	if success == 0 then
		kgp.playertell(user, "Failed to join.", GAME_MASTER_NAME, 1)
	else
		kgp.playertell(user, "Join Successful! Waiting to Spawn...", GAME_MASTER_NAME, 1)
		updateGameInstances()
	end
end

function lockInstanceCheck(gameInstance)
	if gameInstance ~= nil then
		if gameInstance.locked == 0 then -- it was unlocked, if  we reached max players lets lock it now
			if #gameInstance.players >= MAX_PLAYERS_PER_ZONE then
				return kgp.gamelockinstance(gameInstance.id, true)
			end
		elseif gameInstance.locked == 1 then -- its already locked, if we want to unlock it now, we do so here
			if #gameInstance.players < MAX_PLAYERS_PER_ZONE then
				return kgp.gamelockinstance(gameInstance.id, false)
			end
		end
	end
	return gameInstance.locked
end

function sendGameZoneInfo(user)
	argument_table = {}
	table.insert(argument_table, #game_zones)
	for i,v in ipairs(game_zones) do
		table.insert(argument_table, v.id)
		table.insert(argument_table, v.name)
		table.insert(argument_table, #v.instances)
	end
	kgp.playersendevent(user, RESPONSE_ZONE_INFO, argument_table)
end

function sendGameInstancesInfo(user, gameZoneId)
	argument_table = {}
	for i,v in ipairs(game_zones) do
		if v.id == gameZoneId or gameZoneId == -1 then
			table.insert(argument_table, v.id)
			table.insert(argument_table, #v.instances)
			local instances = v.instances
			for k,gameInstance in ipairs(instances) do
				table.insert(argument_table, gameInstance.id)
				table.insert(argument_table, tostring(gameInstance.locked))
				table.insert(argument_table, #gameInstance.players)
				for l, player in ipairs(gameInstance.players) do
					table.insert(argument_table, player.name)
				end
			end
			break
		end
	end
	kgp.playersendevent(user, RESPONSE_INSTANCE_INFO, argument_table)
end

function updateGameInstances()
	temp_game_zones = {}
	
	local ret = kgp.gamegetzonesinfo()
	if ret == nil then
		return
	end
	for i,zoneInfo in ipairs(ret) do
		local zoneId = zoneInfo[1]
		local appName = zoneInfo[2]
		local instanceTable = zoneInfo[3]
		
		local gameInstances = {}
		for j,gameInstanceId in ipairs(instanceTable) do
			local instanceId, locked, players = kgp.gamegetinstanceinfo(gameInstanceId)
			if instanceId ~= nil then
				local gameInstancePlayers = {}
				
				for k,playerId in ipairs(players) do
					local player = Player.create(playerId)
					table.insert(gameInstancePlayers, player)
				end
				local gameInstance = GameInstance.create(instanceId)
				gameInstance.locked = locked
				gameInstance.players = gameInstancePlayers
				
				local newLock = lockInstanceCheck(gameInstance)
				if gameInstance.locked ~= newLock then
					gameInstance.locked = newLock
				end
				
				table.insert(gameInstances, gameInstance)
			end
		end
		
		local gameZone = GameZone.create(zoneId, appName)
		gameZone.instances = gameInstances
		table.insert(temp_game_zones, gameZone)
	end
	game_zones = temp_game_zones
end

function getGameInstance(instanceId)
	for i,v in ipairs(game_zones) do
		for j,w in ipairs(v.instances) do
			if w.id == instanceId then
				return w
			end
		end
	end
	return nil
end