***NO COPYRIGHT***  

***All files in this folder are not copyright anything, use it as you like***  

# Kanevia Database

The Kanevia game engine is a fork of the original proprietary Kaneva game engine that has now been open-sourced under the PolyForm Noncommercial License above and may be used or modified by anyone for any non-commercial purpose.  Negotiations for commercial licensing are possible however only through direct agreement with Chris Klaus, owner of the original Kaneva game engine.
The original Kaneva however used an insanely obtuse and ugly database schema that we no longer need.
This folder contains the new Kanevia database schema.

MySQL 5.7 was chosen as the database server

## Requires
   - MySQL 5.7 - https://dev.mysql.com/downloads/mysql/5.7.html

   
## Setup Development Environment   
1. Install latest MySQL 5.7
