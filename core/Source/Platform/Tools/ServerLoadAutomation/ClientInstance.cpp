/******************************************************************************
 ClientInstance.cpp

 Copyright (c) 2004-2009 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "ClientInstance.h"
#include "event\events\GotoPlaceEvent.h"
#include "ChatType.h"
#include "event\events\AttribEvents.h"
#include "event\events\RenderTextEvent.h"
#include "event\events\DynamicObjectEvents.h"
#include "event\events\AutomationEvent.h"
#include "event\events\ShutdownEvent.h"
#include "event\events\DDREvent.h"
#include "event\events\TextEvent.h"
#include "Common/KEPUtil/WebCall.h"
#include "Common/KEPUtil/URL.h"
#include "Plugins/App/WebServicesProxy.h"
#include "Event/Events/ZoneCustomizationDownloadHandler.h"
#include "Event/Events/ClientSpawnEvent.h"
#include "Event/Events/SoundEvents.h"
#include "Event/Events/ParticleEvents.h"
#include "Event/Events/TriggerEvent.h"
#include "Event/Events/ScriptClientEvent.h"
#include "Event/Events/GenericEvent.h"
#include "ResourceManagers/DownloadManager.h"
#include <GetSet.h>
#include <GetBrowserPage.h>

#include "Common/include/NetworkCfg.h"
#include "Common/include/KEPConfig.h"
#include "ClientInstanceIds.h"

#define ZONE_URL 0
#define ZONE_SERVER 1
#define ZONE_PORT 2
#define ZONE_DDR 3

#define TELL_CMD 2035 //tell command

enum CLIENT_STATE
{
	NOT_CONNECTED = 0,
	CONNECTING,
	CONNECTED,
	LOGGING_IN,
	LOGGED_IN,
	ZONING,
	PENDING_SPAWN_RCV,
	PENDING_SPAWN_SND,
	SPAWN_STRUCT_RCV,
	SPAWN_STRUCT_SND,
	SPAWNED,
};

typedef struct _CLIENT_STATE_TIME
{
	CLIENT_STATE	state;
	ULONG			timestamp;
} CLIENT_STATE_TIME;

struct Trigger: public GetSet
{
	int placementId;
	float radius;
	int action;
	int intParam;
	string strParam;
	bool triggered;

	Trigger(): placementId(0), radius(0.0f), action(0), intParam(0), triggered(false) {}

	DECLARE_GETSET
};

struct Placement: public GetSet
{
	int placementId;
	float x, y, z;
	float rx, ry, rz;
	float radius;
	int globalId;

	Placement( int pid, float ax, float ay, float az, float arx, float ary, float arz, float arad, int aGlobalId )
		: placementId(pid), x(ax), y(ay), z(az), rx(arx), ry(ary), rz(arz), radius(arad), globalId(aGlobalId) {}

	DECLARE_GETSET
};

static const ULONG PROTOCOL_VERSION = (ULONG)0x00008026;
static ULONG GetContentVersion( IN LPCTSTR dir );
static const string StateToString( CLIENT_STATE state );

Logger audit_logger = Logger::getInstance( "AuditLog" );
Logger alert_logger = Logger::getInstance( "AlertLog" );


// trimmed down version of ClientEngine::ParseBrowserConnectResult
// to parse login web call results to get game ids
static bool ParseBrowserConnectResult( string resultText, string & resultCode, string & resultDescription, int& gameId, string & gameName,
													int &parentGameId )
{
	bool done = false;

	string::size_type ii = 0;

   locale loc ( "" );

    for ( int i = 0; ii != string::npos; i++ )
	{
		string::size_type prev = ii;

		ii = resultText.find( "\t", ii );
		string token;
		if ( ii == string::npos )
			token = resultText.substr( prev );
		else
		{
			token = resultText.substr( prev, ii-prev );
			ii++;
		}

		switch (i)
		{
		case 0:
			resultCode.assign(token);
			if ( token.empty() || !isdigit( token.at(0), loc ) )
				return false;
			break;
		case 1:
			resultDescription.assign(token);
			break;
		case 2:
			/*userId.assign(token);
			if ( token.empty() || !isdigit( token.at(0), loc ) )
				return false;
			*/
			break;
		case 3:
			// username.assign(token);
			break;
		case 4:
			// serverip.assign(token);
			break;
		case 5:
			/* serverport.assign(token);
			if ( token.empty() || !isdigit( token.at(0), loc ) )
				return false;
			*/
			break;
		case 6:
			/*zoneindex.assign(token);
			if ( token.empty() || !isdigit( token.at(0), loc ) )
				return false;
			*/				
			break;
		case 7:
			// gender.assign(token);
			done = true;
			
			break;
		case 8:
			if ( token.empty() || !isdigit( token.at(0), loc ) )
				return false;
			gameId = atol( token.c_str() );		
			break;
		case 9:
			gameName.assign( token.c_str() );
			break;
		case 10:
			if ( token.empty() || !isdigit( token.at(0), loc ) )
				return false;
			parentGameId = atoi( token.c_str() );
			break;
		}

	}

	return done;
}

// helper class for sending events
class NetworkSender : public ISendHandler
{
public:
	NetworkSender( IClientNetwork *sender, ULONG timeout = 1000 ) : m_sender(sender), m_timeout(timeout) {}

	virtual EVENT_PROCESSING_RET  SendEventTo( IN IEvent *e )
	{
		PCBYTE buff;
		ULONG len;
		
		e->GetOutputBuffer( buff, len );
		
		for ( NETIDVect::iterator i = e->To().begin(); i != e->To().end(); i++ )
		{
			NETRET netRet = m_sender->Send( *i, buff, len, ((e->Flags() & EF_GUARANTEED) == EF_GUARANTEED) ? NSENDFLAG_GUARANTEED : NSENDFLAG_NOCOMPLETE, m_timeout );
		}

		return EPR_OK;
	}

	IClientNetwork *m_sender;
	ULONG			m_timeout;
};

class ClientInstance : public IClientNetworkCallback, public jsThread, public IGame, public GetSet
{
public:
	ClientInstance();
	~ClientInstance();

	void	SwitchClientState( CLIENT_STATE newState );
	bool	Connect();
	void	SendData( BYTE * data, DWORD size, DWORD flags, NETID sendTo, DWORD timeout );
	bool	LogPlayerIntoServer();
	static	EVENT_PROC_RC	LogonResponseEventHandler(	IN ULONG lparam, 
																			IN IDispatcher *disp, 
																			IN IEvent *e );
	EVENT_PROC_RC			HandleLogonResponseEvent(	IN LPCTSTR connectionErrorCode,
																			IN ULONG hResultCode, 
																			IN ULONG characterCount,
																			IN LOGON_RET replyCode  );
	static EVENT_PROC_RC  AutomationEventHandler( IN ULONG lparam, 
																		IN IDispatcher *disp, IN IEvent *e );
	short	GetAttribCRC(LPATTRIB_STRUCT attribStruct);
	bool	SendSelectCharacterAttribMessage();
	static	EVENT_PROC_RC PendingSpawnEventHandler(	IN ULONG lparam, 
																			IN IDispatcher *disp, 
																			IN IEvent *e  );
	short	GetSpawnCRC(LPCTSTR worldName, short dbIndex, short currentTrace );
	bool	SendSpawnMsgToServer( int dbIndex, long zoneIndex, string worldFile );
	bool	HandleAttribMessage( LPATTRIB_STRUCT formattedMessage, NETID idFromLoc );
	static EVENT_PROC_RC	ProcessRenderTextEvent( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
	EVENT_PROC_RC			HandleRenderTextEvent( LPCTSTR msg, ChatType ct );
	bool	LogPlayerOffServer( bool reconnectingToOtherServer = false);
	static EVENT_PROC_RC	GotoPlaceHandler(	IN ULONG lparam, 
																	IN IDispatcher *disp, 
																	IN IEvent *e );

	EVENT_PROC_RC ProcessClientSpawnEvent( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );

	static EVENT_PROC_RC ClientSpawnEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
	static EVENT_PROC_RC AddDynamicObjectHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
	static EVENT_PROC_RC RemoveDynamicObjectHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
	static EVENT_PROC_RC MoveDynamicObjectHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
	static EVENT_PROC_RC AddTriggerHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
	static EVENT_PROC_RC RemoveTriggerHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
	static EVENT_PROC_RC ScriptClientEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
	static EVENT_PROC_RC ZoneCustomizationDLCompleteEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e  );

	float	GetMovMsgCrC(LPSHRUNK_TOSVRMOV_STRUCT packet);
	LPSHRUNK_TOSVRMOV_STRUCT ShrinkDataCP(LPMOVE_STRUCT OLD_DATA, LPSHRUNK_TOSVRMOV_STRUCT SHRUNK_DATA);
	bool	SendMovMsg( ULONG currentTimeDiff );
	static EVENT_PROC_RC ProcessObjUrlEvent( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
	EVENT_PROC_RC HandleObjUrlEvent();
	void	SetInitialZoneTime();
	bool	SpawnPlayerToZone( ULONG zi, ULONG instanceId );
	bool	SpawnPlayerToURL( const char *stpURL, const char *server, const char *port );
	bool	SendDDRStartGameEvent( int objectId );
	bool	SendDDRRecordScoreEvent( int objectId );
	bool	SendDDRGameEndEvent( int objectId );
	bool	SendTextEvent( string chatMessage, ChatType ct, int command = 0 );
	bool	SendAttribMessage( DWORD flags, NETID sendTo, int attribute, int uniInt, long referenceID, int uniInt2, short uniShort3 );
	bool	FillInLoginURL( INOUT string &s, IN string emailaddress );
	bool	FillInAuthURL( INOUT string &s, IN string userName, IN string password );
	bool	GetBrowserPage( string url );

	bool	CreateCharacterOnServer( string characterName, CharacterCreateStruct *pCharCreate );

	bool CheckChat(string textReceived);
	bool CheckTell(string textReceived);

	void CleanUpEvents();

	ULONG				_doWork();

	bool				Start( LPCTSTR baseDir, LPCTSTR user, LPCTSTR pw, LPCTSTR server, UINT port, LPCTSTR email, ULONG zoneWait, 
							   LPCTSTR loginURL, LPCTSTR authURL, vector<vector<string>> webcalls, 
							   LPCTSTR gameName, LPCTSTR tellTarget, LPCTSTR tellSender, 
							   LPCTSTR scriptName, LPCTSTR startingUrl, CharacterCreateStruct *pCharCreate, 
							   LPCTSTR zoneCustUrl );
	jsThread::StopRet	Shutdown();

	virtual NETRET		Connected( IN LOGON_RET ret, IN NETRET result, IN ULONG sessionProtocolVersion ) override;
	virtual NETRET		Disconnected( IN string readon, IN HRESULT hr, IN BOOL report, IN LOGON_RET replyCode, IN bool connecting) override;
	virtual bool		DataFromServer( IN NETID from, IN const BYTE * data, IN ULONG len ) override;

	ULONG QueueEvent( IN IEvent *e, IN ULONG secsInFuture = 0, IN bool inMilliSec = false ) 
									{ return m_dispatcher.QueueEvent( e, secsInFuture, inMilliSec ); }

	EVENT_ID GetEventId(IN LPCTSTR name) { return m_dispatcher.GetEventId(name); }
	IEvent * MakeEvent(EVENT_ID id, NETID from = 1UL) { return m_dispatcher.MakeEvent(id, from); }


    ClientInstance& setWebProxy( const WebServicesProxy& proxy ) {
        _webProxy = proxy;
        return *this;
    }

	// IGame/IKEPObject override
	IGetSet *Values() { return this; }
									
	virtual ULONG   GetArrayCount( IN ULONG indexOfArray );
	virtual IGetSet *GetObjectInArray( IN ULONG indexOfArray, IN ULONG indexInArray );
	virtual double	  GetNumber( IN ULONG index );

	CLIENT_STATE_TIME	m_prevState;
	CLIENT_STATE_TIME	m_curState;

	vector<vector<string>>	m_zones;
	vector<vector<string>>	m_webcalls;
	bool					m_startZoning;
	bool					m_stopZoning;
	ULONG					m_initialZoneWaitTime;
	ULONG					m_zoneWaitTime;
	ULONG					m_chatWaitTime;
	string					m_chatMessage;
	ULONG					m_tellWaitTime;
	string					m_tellMessage;
	string					m_tellTarget;
	string					m_tellSender;
	ULONG					m_attachAttribWaitTime;
	ULONG					m_invAttribWaitTime;

    ClientInstance&         setGameId(unsigned long gameId ) { 
        _gameId = gameId;
        return *this;
    }
protected:

	IClientNetwork	   *m_clientNetwork;
	Dispatcher			m_dispatcher;

	string				m_baseDir;
	string				m_username;
	string				m_password;
	string				m_servername;
	UINT				m_port;
	string				m_email;
	string				m_loginURL;
	string				m_authURL;
	string				m_gameName;
	string				m_url;		// current url
	int					m_gameId;
	int					m_parentGameId;
	string				m_scriptName;	// optional name of script to execute when logged in
	CharacterCreateStruct* m_pCharCreate;

	GotoPlaceEvent	   *m_reconnectEvent;
	short				m_networkDefinedId;

	bool				m_gotServerResponse;
	ULONG				m_timeMessageSent;
	ULONG				m_timeTellSent;

	ULONG				m_zoneSentTime;
	bool				m_loggedOutZoneTime;

	// for scripting support
	BladeFactory 		m_eventBladeFactory;
	EventBladeVector	m_eventBlades;
	Logger				m_logger;

	int					m_zoneIndex;
	int					m_instanceId;


	IEventHandlerPtr	m_zoneCustomizationDownloadHandler;
	DownloadManager	   *m_dlMgr;
	string				m_zoneCustomizationsUrl;
	map<int, Placement>	m_dynamicObjects;
	map<int, Trigger>	m_dynamicTriggers;

	D3DVECTOR			m_playerPosition;
	D3DVECTOR			m_playerOrientation;
	int					m_playerAnim;
	int					m_playerAnim2;
	int					m_playerAnimOverride;
	bool				m_firstZone;

	ULONG				m_sessionProtocolVersion;
	
    WebServicesProxy    _webProxy;

    unsigned long       _gameId;  // todo: resolve ambiguity between this member var and 
                                    // m_gameId.  This is a configuration value.

	DECLARE_GETSET

};

ClientInstance::ClientInstance() :  IGame(ENGINE_CLIENT),
								    jsThread("ClientInstance"),
									m_clientNetwork(0),
									m_dispatcher(ESEC_CLIENT),
									m_reconnectEvent(0),
									m_networkDefinedId(-1),
									m_chatMessage(""),
									m_tellMessage(""),
									m_tellTarget(""),
									m_tellSender(""),
									m_startZoning(false),
									m_initialZoneWaitTime(0),
									m_zoneWaitTime(0),
									m_tellWaitTime(0),
									m_attachAttribWaitTime(0),
									m_invAttribWaitTime(0),
									m_loggedOutZoneTime(true),
									m_pCharCreate(NULL),
									m_logger(Logger::getInstance(LOGGER_NAME)),
									m_eventBladeFactory(ENGINE_CLIENT),
									m_gameId(0),
									m_parentGameId(0),
									m_dlMgr(NULL),
									m_playerAnim(0),
									m_playerAnim2(-1),
									m_playerAnimOverride(0),
									m_firstZone(true),
									m_sessionProtocolVersion(0)
{
	m_prevState.state = NOT_CONNECTED;
	m_prevState.timestamp = TickCount();

	m_curState.state = NOT_CONNECTED;
	m_curState.timestamp = TickCount();

	m_playerPosition.x = 0.0f;
	m_playerPosition.y = 0.0f;
	m_playerPosition.z = 0.0f;
	m_playerOrientation.x = 0.0f;
	m_playerOrientation.y = 0.0f;
	m_playerOrientation.z = 1.0f;
}

ClientInstance::~ClientInstance()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if ( m_clientNetwork )
	{
		m_clientNetwork->Delete();
		m_clientNetwork = 0;
	}
}

void ClientInstance::CleanUpEvents()
{
	m_dispatcher.StopProcessingEvents();
}

void ClientInstance::SwitchClientState( CLIENT_STATE newState )
{
	ULONG switchTime = TickCount();
	
	LOG4CPLUS_DEBUG( Logger::getInstance( LOGGER_NAME ), StateToString(m_curState.state) << " -> " << StateToString(newState) << " : " << TickDiff(switchTime, m_curState.timestamp));
	
	m_prevState.state = m_curState.state;
	m_prevState.timestamp = m_curState.timestamp;

	m_curState.state = newState;
	m_curState.timestamp = switchTime;

	if ( m_curState.state == ZONING && m_loggedOutZoneTime )
	{
		m_zoneSentTime = m_curState.timestamp;
		m_loggedOutZoneTime = false;
	}
	else if ( m_curState.state == SPAWN_STRUCT_SND )
	{
		ULONG timediff = TickDiff( m_curState.timestamp,  m_zoneSentTime );
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), m_username << " : " << m_servername << " : ZONE : " << timediff );
		m_loggedOutZoneTime = true;
	}
}

bool ClientInstance::Connect()
{
	if ( !m_clientNetwork )  {
        bool disableNATTraversal = false;
        try
        {
            string path = PathAdd( m_baseDir, NETCFG_CFG );
            KEPConfig cfg;
            cfg.Load(path.c_str(), NETCFG_ROOT );
            cfg.GetValue(   DISABLENATTRAVERSAL_TAG
                , disableNATTraversal
                , disableNATTraversal);
        }
        catch ( KEPException * e )
        {
            LOG4CPLUS_ERROR( m_logger, "Error loading " << NETCFG_CFG << ": " << e->ToString() );
            e->Delete();
        }

		m_clientNetwork = MakeClientNetwork( m_baseDir.c_str(), LOGGER_NAME, disableNATTraversal );
    }

	if ( !m_clientNetwork ) 
        return false;

	m_dispatcher.SetSendHandler( new NetworkSender(m_clientNetwork) );
	m_dispatcher.SetNetId( -1 );

	TCHAR curDir[_MAX_PATH];
	_getcwd( curDir, _MAX_PATH );
	CONTEXT_VALIDATION cntxValidation;

	// hash the m_password
	MD5_DIGEST_STR pw;
	CString userName( m_username.c_str() );
	userName.MakeLower();
	hashString((m_password+(LPCTSTR)userName).c_str(), pw);
	lstrcpyn(cntxValidation.password,pw.s, sizeof(cntxValidation.password) );
	lstrcpyn(cntxValidation.username,m_username.c_str(), sizeof(cntxValidation.username));

	cntxValidation.m_category           = LOGON_CLIENT;
	cntxValidation.protocolVersionInfo  = PROTOCOL_VERSION;
	cntxValidation.contentVersionInfo   = GetContentVersion(curDir);
	vector<ServerListItem> servers = _webProxy.enumerateGameServers(_gameId);
	string serverName   = this->m_servername;
	unsigned short port = this->m_port;

    // for now just take the first server.  Later, should it become necessary, we can add logic 
    // to randomize.
    // 

    if ( servers.size() > 0 ) {
        serverName = servers[0]._serverName;
        port = atoi( servers[0]._port.c_str() );
    }

	return !FAILED(m_clientNetwork->Connect( this, m_port, serverName.c_str(), (BYTE *)&cntxValidation, sizeof(cntxValidation) ));
}

void ClientInstance::SendData( BYTE * data, DWORD size, DWORD flags, NETID sendTo, DWORD timeout )
{
	if ( m_clientNetwork )
	{
		m_clientNetwork->Send( sendTo, data, size, flags, timeout);
	}
}

bool ClientInstance::LogPlayerIntoServer()
{
	LPLOG_STRUCT lpChatMessage = NULL;
	DWORD               messageSize;
	messageSize = sizeof(LOG_STRUCT);
	lpChatMessage = (LPLOG_STRUCT) new BYTE[messageSize];
	lpChatMessage->m_category = LOG_TO_SERVER_CAT;
	
	lstrcpy(lpChatMessage->logInName, m_username.c_str());
	lstrcpy(lpChatMessage->logInPass, m_password.c_str());

	SendData((BYTE *)lpChatMessage, messageSize, NSENDFLAG_GUARANTEED | NSENDFLAG_PRIORITY_HIGH, SERVER_NETID, 60000);

	if (lpChatMessage) 
	{
		delete lpChatMessage;
	}
	lpChatMessage = 0;

	return true;
}

EVENT_PROC_RC ClientInstance::LogonResponseEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	ClientInstance *me = (ClientInstance*)lparam;
	LogonResponseEvent *lrEvent = dynamic_cast<LogonResponseEvent*>(e);
	return me->HandleLogonResponseEvent( lrEvent->m_connectionErrorCode.c_str(), lrEvent->m_hResultCode, lrEvent->m_characterCount, lrEvent->m_replyCode );
}

EVENT_PROC_RC  ClientInstance::AutomationEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	ClientInstance *me = (ClientInstance*)lparam;
	AutomationEvent *aeEvent = dynamic_cast<AutomationEvent*>(e);

	if ( aeEvent->Filter() == AutomationEvent::AE_SHUTDOWN )
	{
		disp->QueueEvent( new ShutdownEvent() );
		me->_setTerminated();
	}
	
	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC  ClientInstance::HandleLogonResponseEvent(  IN LPCTSTR connectionErrorCode,
																				IN ULONG hResultCode, 
																				IN ULONG characterCount,
																				IN LOGON_RET replyCode  )
{
	if ( hResultCode == S_OK && replyCode == LR_OK )
	{
		SwitchClientState( LOGGED_IN );
	}

	return EVENT_PROC_RC::OK;
}

short ClientInstance::GetAttribCRC(LPATTRIB_STRUCT attribStruct)
{
	long genCRC = 0;

	genCRC = attribStruct->attribute +
			 attribStruct->uniInt +
			 attribStruct->uniInt2;

	while (labs(genCRC) > 30000)
		genCRC = genCRC / 10;

	short finalValue = (short) genCRC;

	return finalValue;
}

bool ClientInstance::SendSelectCharacterAttribMessage()
{
	LPATTRIB_STRUCT lpChatMessage = NULL;
	DWORD messageSize;
	messageSize = sizeof(ATTRIB_STRUCT);
	lpChatMessage = (LPATTRIB_STRUCT) new BYTE[messageSize];

	lpChatMessage->m_category = 6;
	lpChatMessage->attribute = 9;  //GetAttributeType::SELECT_CHARACTER
	lpChatMessage->uniInt = 0;
	lpChatMessage->idRefrence  = 0;
	lpChatMessage->uniInt2     = -1;
	lpChatMessage->uniShort3   = 8;
	lpChatMessage->CRCcheck    = GetAttribCRC(lpChatMessage);

	BYTE* newMessage = new BYTE[messageSize+1];
	newMessage[0] = 24;
	
	for ( unsigned int i=0; i < messageSize;  i++ )
	{
		newMessage[i+1] = ((BYTE*)lpChatMessage)[i];
	}

	SendData((BYTE *)newMessage, messageSize+1, NSENDFLAG_NOCOMPLETE | NSENDFLAG_PRIORITY_HIGH, SERVER_NETID, 60000);

	if (lpChatMessage)
	{
		delete lpChatMessage;
		lpChatMessage=0;
	}

	return true;
}

EVENT_PROC_RC ClientInstance::PendingSpawnEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e  )
{
	// default handler for this event assumes we are ready to spawn, simply reply back to server immediately
	ClientInstance *me = (ClientInstance*)lparam;
	PendingSpawnEvent *pse = dynamic_cast<PendingSpawnEvent *>(e);
	
	LONG zoneIndex;
	USHORT spawnId;
	bool initialSpawn;
	long coverCharge;
	long zoneInstanceId;

	pse->extractInfo(zoneIndex, spawnId, initialSpawn, coverCharge, zoneInstanceId);

	me->SwitchClientState( PENDING_SPAWN_RCV );

	me->m_dispatcher.QueueEvent(new PendingSpawnEvent(zoneIndex, spawnId, SERVER_NETID, initialSpawn, coverCharge, zoneInstanceId));

	me->SwitchClientState( PENDING_SPAWN_SND );

	return EVENT_PROC_RC::CONSUMED;	
}

short ClientInstance::GetSpawnCRC(LPCTSTR worldName, short dbIndex, short currentTrace )
{
	long genCRC = 0;

	for (size_t loop=0;loop<::strlen(worldName);loop++)
		genCRC += (int)(worldName[loop]);

	genCRC += (dbIndex + currentTrace);
	while (labs(genCRC) > 30000)
		genCRC = genCRC / 10;

	short finalValue = (short) genCRC;

	return finalValue;
}

bool ClientInstance::SendSpawnMsgToServer( int dbIndex, long zoneIndex, string worldFile )
{
	LPFROM_CLIENT_SPAWN_STRUCT lpSpawnMessage = NULL;
	DWORD messageSize;
	messageSize = sizeof(FROM_CLIENT_SPAWN_STRUCT)+worldFile.size();
	lpSpawnMessage = (LPFROM_CLIENT_SPAWN_STRUCT) new BYTE[messageSize];

	lpSpawnMessage->m_category      = 8;					// SPAWN_TO_SERVER_CAT
	lpSpawnMessage->currentTrace    = 1;					// Always 1 because EntObjRuntime will always only contain one entity
	lpSpawnMessage->dbIndex         = dbIndex;				// comes from SPAWN_TO_SERVER_CAT message
	lpSpawnMessage->usePresets      = -1;					// only set if entity is AI
	lpSpawnMessage->characterInUse  = 0;					// GL_multiplayerObject->m_currentCharacterInUse set from PlayCharacter glue
	lpSpawnMessage->aiCfgIndex		= 0;					// only set if entity is AI
	lpSpawnMessage->zoneIndex		= zoneIndex;			// comes from SPAWN_TO_SERVER_CAT message
	lstrcpy(lpSpawnMessage->worldName, worldFile.c_str());
	lpSpawnMessage->CRCcheck = GetSpawnCRC(lpSpawnMessage->worldName, lpSpawnMessage->dbIndex, lpSpawnMessage->currentTrace);

	SendData((BYTE *)lpSpawnMessage,
			 messageSize,
			 NSENDFLAG_GUARANTEED,
			 NULL,	// send to server
			 60000);

	if (lpSpawnMessage)
		delete lpSpawnMessage;
	lpSpawnMessage=0;

	SwitchClientState( SPAWN_STRUCT_SND );

	return TRUE;
}

bool ClientInstance::HandleAttribMessage( LPATTRIB_STRUCT formattedMessage, NETID idFromLoc )
{
	if ( formattedMessage->attribute == AE_PLAYER_CREATED_HEALTH )
	{
		m_networkDefinedId = formattedMessage->uniInt;
		//m_dispatcher.SetNetId(m_networkDefinedId);

		LOG4CPLUS_TRACE( Logger::getInstance( LOGGER_NAME ), "NETID from AE_PLAYER_CREATED_HEALTH: " << formattedMessage->uniInt );
	}
	return true;
}

EVENT_PROC_RC ClientInstance::ProcessRenderTextEvent( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	ClientInstance *me = (ClientInstance*)lparam;
	RenderTextEvent *rte = dynamic_cast<RenderTextEvent *>(e);

	string text;
	ChatType ct;

	//Doing this instead of extract info because
	//a tell is private, but does not have any
	//info for toDistribution or fromRecipient
	int temp;
	*(e->InBuffer())	>> text
						>> temp;

	ct = (ChatType)temp;

	return me->HandleRenderTextEvent( text.c_str(), ct );
}

EVENT_PROC_RC ClientInstance::HandleRenderTextEvent( LPCTSTR msg, ChatType ct )
{
	string textReceived;

	CString lowerReceived( msg );
	lowerReceived.MakeLower();
	textReceived = (LPCTSTR)lowerReceived;

	switch ( ct )
	{
		case ctTalk:
		{
			m_gotServerResponse = CheckChat(textReceived);
			break;
		}
		case ctPrivate:
		{
			m_gotServerResponse = CheckTell(textReceived);
			break;
		}
		default:
			LOG4CPLUS_WARN( Logger::getInstance( LOGGER_NAME ), "Unkown chat type received!" );
	}

	return EVENT_PROC_RC::OK;
}

bool ClientInstance::CheckChat(string textReceived)
{
	string textListen;

	ULONG timediff = TickDiff( TickCount(), m_timeMessageSent );

	textListen = m_username;
	textListen.append(" says > ");
	textListen.append(m_chatMessage);
	CString lowerTextListen( textListen.c_str() );
	lowerTextListen.MakeLower();
	textListen = (LPCTSTR)lowerTextListen;

	if ( textReceived == textListen )
	{
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), m_username << " : " << m_servername << " : CHAT TEXT : " << timediff );
	}
	else
	{
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), "Mismatched chat message! - " << m_username );
	}

	return true;
}

bool ClientInstance::CheckTell(string textReceived)
{
	string textListen;

	ULONG timediff = TickDiff( TickCount(), m_timeTellSent );

	//if we find 'you' at the beginning of the string then it must be the echo of the tell we sent.
	if ( strstr(textReceived.c_str(), "you") == textReceived.c_str() )
	{
		LOG4CPLUS_TRACE( Logger::getInstance( LOGGER_NAME ), "Received echo: " << m_username << " : " << m_servername << " : TELL TEXT : " << timediff << " : " << m_tellSender );
		return true;
	}

	textListen = m_tellSender;
	textListen.append(" tells you > ");
	textListen.append(m_tellMessage);
	CString lowerTextListen( textListen.c_str() );
	lowerTextListen.MakeLower();
	textListen = (LPCTSTR)lowerTextListen;

	if ( textReceived == textListen )
	{
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), m_username << " : " << m_servername << " : TELL TEXT : " << timediff << " : " << m_tellSender );
	}
	else
	{
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), "Mismatched tell message! - " << m_username << " sender: " << m_tellSender);
	}
	
	return true;
}

EVENT_PROC_RC ClientInstance::ProcessObjUrlEvent( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	ClientInstance *me = (ClientInstance*)lparam;
	UpdateDynamicObjectUrlEvent *doue = dynamic_cast<UpdateDynamicObjectUrlEvent *>(e);

	return me->HandleObjUrlEvent();
}

EVENT_PROC_RC ClientInstance::HandleObjUrlEvent()
{
	//ULONG timediff = TickDiff( TickCount(), m_mediaStartTime );
	//LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), m_username << " : " << m_servername << " : MEDIA : " << timediff );

	return EVENT_PROC_RC::OK;
}

bool ClientInstance::LogPlayerOffServer( bool reconnectingToOtherServer )
{
	ClientExitEvent *e = new ClientExitEvent( reconnectingToOtherServer );
	e->AddTo( SERVER_NETID );
	e->SetFlags( EF_GUARANTEED | EF_SENDIMMEDIATE );

	m_dispatcher.QueueEvent( e );
	Sleep(1000); // sleep to let DPlay send, real client used 500, but too short for us

	// reset state to reconnect to the other server
	if ( reconnectingToOtherServer )
	{
		SwitchClientState( NOT_CONNECTED );
	}

	return true;
}

EVENT_PROC_RC ClientInstance::GotoPlaceHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	ClientInstance *me = (ClientInstance*)lparam;
	
	GotoPlaceEvent *gpe = dynamic_cast<GotoPlaceEvent *>(e);
	gpe->ExtractInfo();

	string gameName;

	StpUrl::ExtractGameName(gpe->url, gameName);

	if ( gpe->type == GotoPlaceEvent::GOTO_URL )
	{
		if ( STLCompareIgnoreCase(gameName.c_str(), me->m_gameName.c_str()) == 0 )
		{
			e->OutBuffer()->SetFromReadable(e->InBuffer());
			e->AddTo(SERVER_NETID);	
			disp->QueueEvent(e);
			return EVENT_PROC_RC::OK;
		}

		if ( gpe->port != 0 && gpe->server.length() > 0 )
		{
			me->m_gameName = gameName;

			me->m_servername = gpe->server;
			me->m_port = gpe->port;

			me->LogPlayerOffServer( true );

			me->m_reconnectEvent = new GotoPlaceEvent();
			me->m_reconnectEvent->GotoUrl( 0, gpe->url.c_str() );
			me->m_reconnectEvent->AddTo( SERVER_NETID );
		}
	}

	return EVENT_PROC_RC::OK;
}

float ClientInstance::GetMovMsgCrC(LPSHRUNK_TOSVRMOV_STRUCT packet)
{
	float    genCRC = 0.0f;

	genCRC =(((float)packet->Xpos) +
			 ((float)packet->Ypos) +
			 ((float)packet->Zpos) +
			 ((float)packet->animGLID2nd) +
			 ((float)packet->animGLID));

	genCRC = genCRC * .1f;

	return genCRC;
}

LPSHRUNK_TOSVRMOV_STRUCT ClientInstance::ShrinkDataCP(LPMOVE_STRUCT OLD_DATA, LPSHRUNK_TOSVRMOV_STRUCT SHRUNK_DATA)
{

	SHRUNK_DATA->m_category          = 4;
	SHRUNK_DATA->Xpos               = OLD_DATA->Xpos;  //shorts are inherently signed
	SHRUNK_DATA->Ypos               = OLD_DATA->Ypos;
	SHRUNK_DATA->Zpos               = OLD_DATA->Zpos;
	SHRUNK_DATA->OrientateDirX      = (byte) (127 * OLD_DATA->OrientateDirX);
	SHRUNK_DATA->OrientateDirY      = (byte) (127 * OLD_DATA->OrientateDirY);
	SHRUNK_DATA->OrientateDirZ      = (byte) (127 * OLD_DATA->OrientateDirZ);
	SHRUNK_DATA->OrientateUpX       = (byte) (127 * OLD_DATA->OrientateUpX);
	SHRUNK_DATA->OrientateUpY       = (byte) (127 * OLD_DATA->OrientateUpY);
	SHRUNK_DATA->OrientateUpZ       = (byte) (127 * OLD_DATA->OrientateUpZ);
	SHRUNK_DATA->FreeLookPitch      = (signed char)(OLD_DATA->FreeLookPitch);
	SHRUNK_DATA->animGLID           = OLD_DATA->animGLID;
	SHRUNK_DATA->animGLID2nd	    = OLD_DATA->animGLID2nd;
	SHRUNK_DATA->idReference        = OLD_DATA->idRefrence;
	SHRUNK_DATA->CRCCHECK           = GetMovMsgCrC(SHRUNK_DATA);

	return(SHRUNK_DATA);
}

bool ClientInstance::SendMovMsg( ULONG currentTimeDiff )
{
	LPMOVE_STRUCT lpChatMessage = NULL;
	DWORD messageSize;
	messageSize = sizeof(MOVE_STRUCT);
	lpChatMessage = new MOVE_STRUCT;
	if (lpChatMessage == NULL)
	{
		return false;
	}
	lpChatMessage->m_category       = 2;
	lpChatMessage->FreeLookYaw    = 0.0f;
	lpChatMessage->FreeLookPitch  = 0.0f;
	lpChatMessage->Xpos           = m_playerPosition.x;
	lpChatMessage->Ypos           = m_playerPosition.y;
	lpChatMessage->Zpos           = m_playerPosition.z;
	lpChatMessage->idRefrence     = m_networkDefinedId;
	lpChatMessage->OrientateDirX  = m_playerOrientation.x;
	lpChatMessage->OrientateDirY  = m_playerOrientation.y;
	lpChatMessage->OrientateDirZ  = m_playerOrientation.z;
	lpChatMessage->OrientateUpX   = 0.0f;
	lpChatMessage->OrientateUpY   = 1.0f;
	lpChatMessage->OrientateUpZ   = 0.0f;
	lpChatMessage->animGLID       = m_playerAnim;
	lpChatMessage->animGLID2nd    = m_playerAnim2;
	if( m_playerAnimOverride!=0 )
	{
		lpChatMessage->animGLID       = m_playerAnimOverride;
		lpChatMessage->animGLID2nd    = -1;
	}

	LPSHRUNK_TOSVRMOV_STRUCT shrunkMessage = NULL;
	DWORD               shrunkMessageSize;
	shrunkMessageSize = sizeof(SHRUNK_TOSVRMOV_STRUCT);
	shrunkMessage = new SHRUNK_TOSVRMOV_STRUCT;
	if (shrunkMessage == NULL)
	{
		return false;
	}

	shrunkMessage->tmel = currentTimeDiff;
	shrunkMessage = ShrinkDataCP(lpChatMessage,shrunkMessage);

	SendData( (BYTE *)shrunkMessage, shrunkMessageSize, NSENDFLAG_NOCOMPLETE | NSENDFLAG_PRIORITY_HIGH, SERVER_NETID, 60000 );

	if (lpChatMessage) delete lpChatMessage;
	lpChatMessage=0;
	
	if (shrunkMessage) delete shrunkMessage;
	shrunkMessage=0;
	
	return true;
}

void ClientInstance::SetInitialZoneTime()
{
	_sleep(500);
	srand(GetTickCount());
	m_initialZoneWaitTime = rand() % m_zoneWaitTime;
}

bool ClientInstance::SpawnPlayerToZone( ULONG zi, ULONG instanceId )
{
	ZoneIndex gotoZoneIndex( zi );
	GotoPlaceEvent *gpe = new GotoPlaceEvent();
	gpe->GotoZone( gotoZoneIndex, instanceId, 0 );
	gpe->AddTo( SERVER_NETID );

	m_dispatcher.QueueEvent( gpe );

	return true;
}

bool ClientInstance::SpawnPlayerToURL( const char *stpURL, const char *server, const char *port )
{
	GotoPlaceEvent *gpe = new GotoPlaceEvent();
	gpe->GotoUrl( 0, stpURL, atoi(port), server );

	LOG4CPLUS_DEBUG(Logger::getInstance(LOGGER_NAME), "Requesting spawn to: " << stpURL);

	m_dispatcher.QueueEvent( gpe );

	return true;
}

bool ClientInstance::SendDDRStartGameEvent( int objectId )
{
	DDREvent *e = new DDREvent();
	e->SetFilter( DDREvent::DDR_START_LEVEL );
	(*e->OutBuffer()) << 369 << objectId << 3307 << 1 << true;
	e->AddTo( SERVER_NETID );
	e->EncryptEvent();
	m_dispatcher.QueueEvent( e );

	return true;
}

bool ClientInstance::SendDDRRecordScoreEvent( int objectId )
{
	DDREvent *e = new DDREvent();
	e->RecordScore( 369, objectId, 1, 0, 0, false, false, false, false, false );
	e->AddTo( SERVER_NETID );
	m_dispatcher.QueueEvent( e );

	return true;
}

bool ClientInstance::SendDDRGameEndEvent( int objectId )
{
	DDREvent *e = new DDREvent();
	e->PlayerExit( 369, objectId, true );
	e->AddTo( SERVER_NETID );
	m_dispatcher.QueueEvent( e );

	return true;
}

bool ClientInstance::SendTextEvent( string chatMessage, ChatType ct, int command )
{
	TextEvent *e = new TextEvent( chatMessage.c_str(), command, m_networkDefinedId, ct );
	e->AddTo( SERVER_NETID );
	m_dispatcher.QueueEvent( e );

	return true;
}

bool ClientInstance::SendAttribMessage( DWORD flags, NETID sendTo, int attribute, int uniInt, long referenceID, int uniInt2, short uniShort3 )
{
	LPATTRIB_STRUCT lpChatMessage = NULL;
	DWORD messageSize;
	messageSize = sizeof(ATTRIB_STRUCT);
	lpChatMessage = (LPATTRIB_STRUCT) new BYTE[messageSize];

	lpChatMessage->m_category = 6;
	lpChatMessage->attribute = attribute;  
	lpChatMessage->uniInt = uniInt;
	lpChatMessage->idRefrence  = (short)referenceID;
	lpChatMessage->uniInt2     = uniInt2;
	lpChatMessage->uniShort3   = uniShort3;
	lpChatMessage->CRCcheck    = GetAttribCRC(lpChatMessage);

	BYTE* newMessage = new BYTE[messageSize+1];
	newMessage[0] = 24;
	
	for ( unsigned int i=0; i < messageSize;  i++ )
	{
		newMessage[i+1] = ((BYTE*)lpChatMessage)[i];
	}

	SendData((BYTE *)newMessage, messageSize+1, NSENDFLAG_NOCOMPLETE | NSENDFLAG_PRIORITY_HIGH, SERVER_NETID, 60000);

	if (lpChatMessage)
	{
		delete lpChatMessage;
		lpChatMessage=0;
	}

	if ( newMessage )
	{
		delete newMessage;
		newMessage=0;
	}

	return TRUE;
}

bool ClientInstance::FillInLoginURL( INOUT string &s, IN string emailaddress )
{
	bool ret = false;
	if ( ! emailaddress.empty() )
	{
		string::size_type p = s.find( "{0}" );
		if ( p != string::npos )
		{
			s = s.replace( p, 3, emailaddress);
			ret = true;
		}
	}
	return ret;
}

bool ClientInstance::FillInAuthURL( INOUT string &s, IN string userName, IN string password )
{
	bool ret = false;
	string::size_type p = s.find( "{0}" );
	if ( p != string::npos )
	{
		s = s.replace( p, 3, userName );
		if ( (p = s.find( "{1}" )) != string::npos )
		{
			s = s.replace( p, 3, password );
			// Once all servers are using STAR code,
			// the third parameter (game Id) should be required
			// before returning true; for now, set ret to true 
			// for backwards compatibility
			ret = true;
			/*
			if ( (p = s.find( "{2}" )) != string::npos )
			{
				s = s.replace( p, 3, numToString(m_gameId,"%d").c_str() );
			}
			*/
		}
	}
	return ret;
}

bool ClientInstance::GetBrowserPage( string url )
{
	Logger logFile( Logger::getInstance(LOGGER_NAME) );

	// ignored parameters
	string resultText;
	DWORD httpStatusCode;
	string httpStatusDescription;
	
	return ::GetBrowserPage( url, resultText, httpStatusCode, httpStatusDescription);
}

ULONG ClientInstance::_doWork()
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED);
	ULONG startMoveTime = TickCount();
	ULONG startZoneTime = TickCount();
	ULONG startTextTime = TickCount();
	ULONG startAttachAttribTime = TickCount();
	ULONG startInvAttribTime = TickCount();

	ULONG currentZoneWaitTime = (ULONG)m_zoneWaitTime;
	bool initialZoneAttempt = true;

	vector<ULONG> webcallTimes;
	webcallTimes.resize(m_webcalls.size(), TickCount());

	while ( !isTerminated() )
	{
		ULONG timeDiff;
		// Send MOV messages to the server after we are logged in 
		// in order to get events from the guarenteed message cycle
		if ( m_curState.state >= LOGGED_IN )
		{
			if ( m_networkDefinedId != -1 )
			{
				timeDiff = TickDiff( TickCount(), startMoveTime );
				if ( timeDiff > 1000 )
				{
					startMoveTime = TickCount();
					SendMovMsg( startMoveTime );
				}
			}
		}

		switch(m_curState.state)
		{
		case NOT_CONNECTED:
		{
			if ( Connect() ) 
			{
				SwitchClientState( CONNECTING );
			}
			else
			{
				_sleep(5000);
			}			
		}
		break;
		case CONNECTING:
			break;
		case CONNECTED:
		{	
			if ( LogPlayerIntoServer() )
			{
				SwitchClientState( LOGGING_IN );
			}
		}
		break;
		case LOGGING_IN:
			break;
		case LOGGED_IN:
		{
			if( m_pCharCreate )
			{
				CreateCharacterOnServer( m_username, m_pCharCreate );
				m_pCharCreate = NULL;
			}

			if ( !m_reconnectEvent )
			{
				if ( SendSelectCharacterAttribMessage() )
				{
					SwitchClientState( ZONING );
					
					if ( m_loginURL.length() > 0 && m_authURL.length() > 0 )
					{
						string loginURL = m_loginURL; 
						FillInLoginURL( loginURL, m_email );

						string resultText;
						DWORD httpStatusCode;
						string httpStatusDescription;
						
						if ( ::GetBrowserPage( loginURL, resultText, httpStatusCode, httpStatusDescription) )
						{
							string resultCode;
							string resultDescription;
							string gameName;
							ParseBrowserConnectResult( resultText, resultCode, resultDescription, m_gameId, gameName, m_parentGameId );
						}

						string authURL = m_authURL; 
						FillInAuthURL( authURL, m_username, m_password );
						GetBrowserPage( authURL );
					}
				}
			}
			else
			{
				// Switched servers, no need to send SELECT_CHARACTER again
				// New server will send PendingSpawnEvent back after
				// it gets the GotoPlaceEvent 
				m_dispatcher.QueueEvent( m_reconnectEvent );
				m_reconnectEvent = 0;
				SwitchClientState( ZONING );
			}
		}
		break;
		case ZONING:
			break;
		case PENDING_SPAWN_RCV:
			break;
		case PENDING_SPAWN_SND:
			break;
		case SPAWN_STRUCT_RCV:
			break;
		case SPAWN_STRUCT_SND:
			break;
		case SPAWNED:
		{
			if ( m_networkDefinedId != -1 )
			{
				if ( m_startZoning && (m_zones.size() > 0) )
				{
					if ( initialZoneAttempt )  
					{
						startZoneTime = TickCount(); // start the zone timer when m_startZoning is first initialized
						//currentZoneWaitTime = m_initialZoneWaitTime;
						initialZoneAttempt = false;
					}
					timeDiff = TickDiff( TickCount(), startZoneTime );
					if ( timeDiff > currentZoneWaitTime )
					{
						SwitchClientState( ZONING );
						srand( (UINT)time(NULL) );
						unsigned int i = rand() % m_zones.size();
						m_firstZone = false;
						SpawnPlayerToURL( m_zones[i][ZONE_URL].c_str(), m_zones[i][ZONE_SERVER].c_str(), m_zones[i][ZONE_PORT].c_str() );

						startZoneTime = m_zoneSentTime = TickCount();

						currentZoneWaitTime = m_zoneWaitTime;

						if ( m_zones[i].size() > ZONE_DDR )
						{
							SendDDRStartGameEvent( atoi(m_zones[i][ZONE_DDR].c_str()) );
							SendDDRRecordScoreEvent( atoi(m_zones[i][ZONE_DDR].c_str()) );
							SendDDRGameEndEvent( atoi(m_zones[i][ZONE_DDR].c_str()) );
						}
					}
				}
				
				timeDiff = TickDiff( TickCount(), m_timeMessageSent );
				if ( m_startZoning && (m_chatWaitTime > 0) && (timeDiff > m_chatWaitTime) )
				{
					if ( m_chatMessage.size() > 0 ) 
						SendTextEvent( m_chatMessage, ctSystem );
					m_gotServerResponse = false;
					m_timeMessageSent = TickCount();
				}

				timeDiff = TickDiff( TickCount(), m_timeTellSent );
				if ( m_startZoning && (m_tellWaitTime > 0) && (timeDiff > m_tellWaitTime) )
				{
					if ( m_tellMessage.size() > 0 ) 
					{
						string msg =  m_tellTarget + " " + m_tellMessage;
						SendTextEvent( msg, ctTalk, TELL_CMD );
					}
					m_gotServerResponse = false;
					m_timeTellSent = TickCount();
				}

				timeDiff = TickDiff( TickCount(), startAttachAttribTime );
				if ( m_startZoning && (m_attachAttribWaitTime > 0) && (timeDiff > m_attachAttribWaitTime) )
				{
					SendAttribMessage( NSENDFLAG_GUARANTEED, SERVER_NETID, 1, 0, m_networkDefinedId, m_networkDefinedId, FALSE );
					startAttachAttribTime = TickCount();
				}

				timeDiff = TickDiff( TickCount(), startInvAttribTime );
				if ( m_startZoning && (m_invAttribWaitTime > 0) && (timeDiff > m_invAttribWaitTime) )
				{
					SendAttribMessage( NSENDFLAG_GUARANTEED, SERVER_NETID, 19,51, m_networkDefinedId, 0, 0 );
					startInvAttribTime = TickCount();
				}

				// loop through all the web calls, determine if enough time has passed between calls, make the call if so
				if ( m_startZoning )
				{
					vector<vector<string>>::iterator it; 
					int i = 0;
					for ( it = m_webcalls.begin(); it < m_webcalls.end(); it++ )
					{
						vector<string> currentWebCall = *it;
						timeDiff = TickDiff( TickCount(), webcallTimes[i] );
						if ( timeDiff > (unsigned)atoi(currentWebCall[1].c_str()) )
						{
							GetBrowserPage( currentWebCall[0] );
							webcallTimes[i] = TickCount();
						}
						i++;
					}
				}
			}
		}
		break;
		default:
			LOG4CPLUS_ERROR( Logger::getInstance( LOGGER_NAME ), "Undefined m_curState.state = " << m_curState.state );
		}

		ULONG start = TickCount();
	
		IEvent *e = 0; 
		// wait for 1 sec, processing any events that come in while waiting
		while ( !isTerminated() && TickDiff( TickCount(), start) < 1000 )
			     
		{
			if ( !m_dispatcher.ProcessNextEvent(100) ) 
				break;
		}
	}

	LogPlayerOffServer();
	
	m_clientNetwork->Delete();
	m_clientNetwork = 0;
	// no longer delete this since stop called mutiple times 

	FreeAllBlades( m_eventBlades );
	
	return 0;
}

// where the lua and python scripts live, under baseDir
// DLLs will be in SCRIPT_DIR\..\bladescripts(d)
#define SCRIPT_DIR "ClientScripts"

bool ClientInstance::Start( LPCTSTR baseDir, LPCTSTR user, LPCTSTR pw, LPCTSTR server, UINT port, LPCTSTR email, ULONG zoneWait, 
	LPCTSTR loginURL, LPCTSTR authURL, vector<vector<string>> webcalls, 
	LPCTSTR gameName, LPCTSTR tellTarget, LPCTSTR tellSender, LPCTSTR scriptName, 
	LPCTSTR startingUrl, CharacterCreateStruct *pCharCreate,
	LPCTSTR zoneCustUrl )
{
	m_baseDir = baseDir;	
	m_username = user;
	m_password = pw;
	m_servername = server;
	m_port = port;
	m_email = email;
	m_webcalls = webcalls;
	m_gameName = gameName;
	m_tellTarget = tellTarget;
	m_tellSender = tellSender;

	m_zoneWaitTime = zoneWait;

	m_loginURL = loginURL;
	m_authURL = authURL;

	m_scriptName = scriptName;

	m_pCharCreate = pCharCreate;

	m_zoneCustomizationsUrl = zoneCustUrl;

	m_dispatcher.SetGame(this);

	string downloadDirectory;
	if( !m_baseDir.empty() )
	{
		downloadDirectory = m_baseDir + "\\temp";
	}

	m_dlMgr = new DownloadManager( m_dispatcher, downloadDirectory, 1, PR_NORMAL );

	RegisterEvent<ClientExitEvent>(m_dispatcher);
	RegisterEvent<PendingSpawnEvent>(m_dispatcher);
	RegisterEvent<GotoPlaceEvent>(m_dispatcher);
	RegisterEvent<TextEvent>(m_dispatcher);
	RegisterEvent<UpdateDynamicObjectUrlEvent>(m_dispatcher);
	RegisterEvent<DDREvent>(m_dispatcher);
	RegisterEvent<AutomationEvent>(m_dispatcher);
	RegisterEvent<ClientSpawnEvent>(m_dispatcher);
	RegisterDynamicObjectEvents(m_dispatcher);
	RegisterParticleEvents(m_dispatcher);
	RegisterSoundEvents(m_dispatcher);
	RegisterEvent<AddTriggerEvent>(m_dispatcher);
	RegisterEvent<RemoveTriggerEvent>(m_dispatcher);
	
	m_dispatcher.RegisterEvent( "ZoneCustomizationDLCompleteEvent", PackVersion(1,0,0,0), GenericEvent::CreateEvent, 0, EP_LOW);

	if ( startingUrl && strlen( startingUrl ) > 0 )
	{
		m_reconnectEvent = new GotoPlaceEvent();
		m_reconnectEvent->GotoUrl( 0, startingUrl );
		m_reconnectEvent->AddTo( SERVER_NETID );
	}
	

	// load scripting events and  handlers
	InitScriptBlades( m_logger, &m_dispatcher, "", PathAdd( baseDir, SCRIPT_DIR ).c_str(), false, NULL, m_eventBladeFactory, m_eventBlades );

	IEventHandlerPtr syncHandler = IEventHandlerPtr( new EventSyncHandler( m_dispatcher ) );
 	m_dispatcher.RegisterHandler( syncHandler, EventSyncEvent::ClassVersion(), EventSyncEvent::ClassId() );

	m_dispatcher.RegisterHandler( LogonResponseEventHandler, (ULONG)this, _T("ClientInstance::LogonResponseHandler"), LogonResponseEvent::ClassVersion(), LogonResponseEvent::ClassId(), EP_NORMAL );
	m_dispatcher.RegisterHandler( PendingSpawnEventHandler, (ULONG)this, "PendingSpawnEvent", PackVersion(1,0,0,0), PendingSpawnEvent::ClassId(), EP_NORMAL );		
	m_dispatcher.RegisterHandler( GotoPlaceHandler, (ULONG)this, "GotoPlaceEvent", GotoPlaceEvent::ClassVersion(), GotoPlaceEvent::ClassId(), EP_NORMAL );
	m_dispatcher.RegisterHandler( ProcessRenderTextEvent, (ULONG)this, "RenderTextEvent", RenderTextEvent::ClassVersion(), RenderTextEvent::ClassId(), EP_NORMAL );
	m_dispatcher.RegisterHandler( ProcessObjUrlEvent, (ULONG)this, "UpdateDynamicObjectUrlEvent", UpdateDynamicObjectUrlEvent::ClassVersion(), UpdateDynamicObjectUrlEvent::ClassId(), EP_NORMAL );	
	m_dispatcher.RegisterHandler( AutomationEventHandler, (ULONG)this, "AutomationEventHandler", AutomationEvent::ClassVersion(), AutomationEvent::ClassId(), EP_NORMAL );	

	ZoneCustomizationDownloadHandler *zch = new ZoneCustomizationDownloadHandler( m_dispatcher, m_logger );
	m_zoneCustomizationDownloadHandler = IEventHandlerPtr( zch  );
	m_dispatcher.RegisterHandler( m_zoneCustomizationDownloadHandler, PackVersion(1,0,0,0), zch->zoneCustomId(), EP_HIGH );

	m_dispatcher.RegisterHandler( ClientSpawnEventHandler, (ULONG)this, "ClientSpawnEvent", ClientSpawnEvent::ClassVersion(), ClientSpawnEvent::ClassId(), EP_NORMAL );
	m_dispatcher.RegisterHandler( AddDynamicObjectHandler, (ULONG)this, "AddDynamicObjectEvent", AddDynamicObjectEvent::ClassVersion(), AddDynamicObjectEvent::ClassId(), EP_NORMAL );
	m_dispatcher.RegisterHandler( RemoveDynamicObjectHandler, (ULONG)this, "RemoveDynamicObjectEvent", RemoveDynamicObjectEvent::ClassVersion(), RemoveDynamicObjectEvent::ClassId(), EP_NORMAL );
	m_dispatcher.RegisterHandler( MoveDynamicObjectHandler, (ULONG)this, "MoveDynamicObjectEvent", MoveDynamicObjectEvent::ClassVersion(), MoveDynamicObjectEvent::ClassId(), EP_NORMAL );
	m_dispatcher.RegisterHandler( AddTriggerHandler, (ULONG)this, "AddTriggerEvent", AddTriggerEvent::ClassVersion(), AddTriggerEvent::ClassId(), EP_NORMAL );
	m_dispatcher.RegisterHandler( RemoveTriggerHandler, (ULONG)this, "RemoveTriggerEvent", RemoveTriggerEvent::ClassVersion(), RemoveTriggerEvent::ClassId(), EP_NORMAL );
	m_dispatcher.RegisterHandler( ScriptClientEventHandler, (ULONG)this, "ScriptClientEvent", ScriptClientEvent::ClassVersion(), ScriptClientEvent::ClassId(), EP_NORMAL );
	m_dispatcher.RegisterHandler( ZoneCustomizationDLCompleteEventHandler, (ULONG)this, "ZoneCustomizationDLCompleteEvent", PackVersion(1,0,0,0), m_dispatcher.GetEventId("ZoneCustomizationDLCompleteEvent"), EP_HIGH );

	start();
	return true;
}

jsThread::StopRet ClientInstance::Shutdown()
{
	jsThread::StopRet ret = jsThread::srStoppedOk;
	if ( isRunning() )
		ret = stop(5000, true); // stop(waitTime, kill if timed out)

	if( m_dlMgr )
	{
		m_dlMgr->CancelAll();
		delete m_dlMgr;
		m_dlMgr = 0;
	}

	delete this;
	return ret;
}

// ***********************************
// IClientNetworkCallback Functions
// ***********************************
NETRET ClientInstance::Connected( IN LOGON_RET replyCode, IN NETRET result, IN ULONG sessionProtocolVersion )
{
	m_sessionProtocolVersion = sessionProtocolVersion;
	SwitchClientState( CONNECTED );
	wakeUp();
	return NET_OK;
}

NETRET ClientInstance::Disconnected(IN string reason, IN HRESULT hr, IN BOOL report, IN LOGON_RET replyCode, IN bool connecting)
{
	// This will be called when switching servers or on final log off
	// either way no state needs to be changed because connecting to 
	// the new server does not wait for disconnect from previous server
	LOG4CPLUS_DEBUG ( Logger::getInstance( LOGGER_NAME ), "Disconnected : " << reason );
	wakeUp();
	return NET_OK;
}

bool ClientInstance::DataFromServer( IN NETID from, IN const BYTE *data, IN ULONG len )
{
	LPEXMSG_CATEGORY identityMsg = ( LPEXMSG_CATEGORY ) data;
	switch (identityMsg->m_category)
	{
		case SPAWN_FROM_SERVER_CAT:
		{
			LPFROM_SERVER_SPAWN_STRUCT	formattedMessage = ( LPFROM_SERVER_SPAWN_STRUCT	) data;

			string worldName = formattedMessage->worldName;
			if (len > sizeof(FROM_SERVER_SPAWN_STRUCT) + worldName.size() )
			{
				m_url = (formattedMessage->worldName + worldName.size() + 1 );
				StpUrl::ExtractGameName(m_url, m_gameName);
			}
			
			SwitchClientState( SPAWN_STRUCT_RCV );

			m_playerPosition.x = formattedMessage->x;
			m_playerPosition.y = formattedMessage->y;
			m_playerPosition.z = formattedMessage->z;

			SendSpawnMsgToServer( formattedMessage->dbIndex, formattedMessage->zoneIndex, formattedMessage->worldName );
			m_zoneIndex = formattedMessage->zoneIndex; //ZoneIndex(formattedMessage->zoneIndex).getClearedMappedInstanceId();
			m_instanceId = formattedMessage->instanceId;

			m_dispatcher.QueueEvent( new ClientSpawnEvent( ZoneIndex(formattedMessage->zoneIndex).getClearedMappedInstanceId(), formattedMessage->instanceId, 
				formattedMessage->x,formattedMessage->y,formattedMessage->z ) );

			//delete formattedMessage;
			//formattedMessage = 0;
			return true;
		}
		break;
		case ENTITYGUPDATE_TO_CLIENT_CAT:
		{
			BYTE *blockCast = (BYTE *) data;
			ENTITYGUPDATE_HEADER * formattedMessage = (ENTITYGUPDATE_HEADER *) data;
			int	dataTracer = sizeof ( ENTITYGUPDATE_HEADER );

			if ( formattedMessage->m_attributeMsgCount > 0 )
			{			
				int byteSize = sizeof ( ATTRIB_STRUCT ) *formattedMessage->m_attributeMsgCount;
				ATTRIB_STRUCT *attribArray = new ATTRIB_STRUCT[formattedMessage->m_attributeMsgCount];
				BYTE *atCast = (BYTE *)attribArray;

				for ( int i = 0; i < byteSize; i++ )
				{
					atCast[i] = blockCast[dataTracer];
					dataTracer++;
				}

				for ( int atLoop = 0; atLoop < formattedMessage->m_attributeMsgCount; atLoop++ )
				{
					LPATTRIB_STRUCT attribMessage = &attribArray[atLoop];
					HandleAttribMessage( attribMessage, from );
					//LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), "Entity Update Attribute : " << (int)attribMessage->attribute );
				}

				delete[] attribArray;
				attribArray = 0;
			}	

			if ( formattedMessage->m_equipMsgCount > 0 )
			{
				int				byteSize = sizeof ( EQUIP_STRUCT ) * formattedMessage->m_equipMsgCount;
				EQUIP_STRUCT	*equipArray = new EQUIP_STRUCT[formattedMessage->m_equipMsgCount];
				BYTE			*atCast = (BYTE *) equipArray;
				for ( int i = 0; i < byteSize; i++ )
				{
					atCast[i] = blockCast[dataTracer];
					dataTracer++;
				}

				delete[] equipArray;
				equipArray = 0;
			}

			if ( formattedMessage->m_txtMsgCount > 0 )
			{
				for ( int txtLoop = 0; txtLoop < formattedMessage->m_txtMsgCount; txtLoop++ )
				{		// txt msg loop
					int txtMsgSz = blockCast[dataTracer];
					dataTracer++;
					if ( txtMsgSz > 0 )
					{	// valid sz
						ChatType ct = *((ChatType*)(&blockCast[dataTracer]));
						dataTracer+=4;
						TCHAR	*tempBlock = new TCHAR[txtMsgSz-4];
						for ( int i = 0; i < txtMsgSz; i++ )
						{
							tempBlock[i] = blockCast[dataTracer];
							dataTracer++;
						}

						HandleRenderTextEvent( tempBlock, ct );
						delete[] tempBlock;
						tempBlock = 0;
					}	// end valid sz
				}		// end txt msg loop
			}

			if ( formattedMessage->m_updateStatMsgCount > 0 )
			{
				int statBlockSize = blockCast[dataTracer];
				dataTracer++;
				if ( statBlockSize > 0 )
				{
					BYTE	*statUpdateBlock = new BYTE[statBlockSize];
					for ( int i = 0; i < statBlockSize; i++ )
					{
						statUpdateBlock[i] = blockCast[dataTracer];
						dataTracer++;
					}
					delete[] statUpdateBlock;
					statUpdateBlock = 0;
				}
			}

			if ( formattedMessage->m_miscMsgCount > 0 )
			{	
				for ( int miscLoop = 0; miscLoop < formattedMessage->m_miscMsgCount; miscLoop++ )
				{
					BYTE	castByteTemp[2];
					castByteTemp[0] = blockCast[dataTracer];
					dataTracer++;
					castByteTemp[1] = blockCast[dataTracer];
					dataTracer++;

					short	miscBlockSize = 0;
					CopyMemory ( &miscBlockSize, castByteTemp, sizeof ( short ) );

					// int miscBlockSize = blockCast[dataTracer];
					// dataTracer++;
					if ( miscBlockSize > 0 )
					{	// 1
						BYTE	*miscUpdateBlock = new BYTE[miscBlockSize];
						for ( int i = 0; i < miscBlockSize; i++ )
						{
							miscUpdateBlock[i] = blockCast[dataTracer];
							dataTracer++;
						}

						//HandleMiscBlocksClientSide ( miscUpdateBlock, miscBlockSize );
						if ( miscUpdateBlock )
						{
							delete[] miscUpdateBlock;
						}

						miscUpdateBlock = 0;
					}	// end 1
				}
			}

			if ( formattedMessage->m_eventCount > 0 )
			{
				for ( int eventLoop = 0; eventLoop < formattedMessage->m_eventCount; eventLoop++ )
				{
					BYTE	castByteTemp[2];
					castByteTemp[0] = blockCast[dataTracer];
					dataTracer++;
					castByteTemp[1] = blockCast[dataTracer];
					dataTracer++;

					short	miscBlockSize = 0;
					CopyMemory ( &miscBlockSize, castByteTemp, sizeof ( short ) );

					// int miscBlockSize = blockCast[dataTracer];
					// dataTracer++;
					if ( miscBlockSize > 0 )
					{
						try
						{
							IEvent *e = m_dispatcher.MakeEvent( &blockCast[dataTracer], miscBlockSize, from );

							if ( e )
							{
								if ( e->Id() > 0 ) // never allow client to send <= 0
									m_dispatcher.ProcessOneEvent(e);
								else
									e->Delete();
							}
						}
						catch ( KEPException *e )
						{
							LOG4CPLUS_ERROR(  Logger::getInstance( LOGGER_NAME ), "Event Exception Error: " << (ULONG)*(EVENT_ID*)data << " " << e->m_msg );
							e->Delete();
						}
					}
					dataTracer += miscBlockSize;
				}
			}

			return true;
		}
		break;
	case ENTITYPOS_TO_CLIENT_CAT:
		{
			BYTE				*dataBlock = (BYTE *) data;
			ENTITYPOS_HEADER *	header = ( ENTITYPOS_HEADER * ) data;
			int					headerSize = sizeof ( ENTITYPOS_HEADER );
			int					movStructSize = sizeof ( SHRUNK_MOVE_STRUCT );
			if( header->m_entPosCount>0 )
			{
				int						indexInto = headerSize;
				SHRUNK_MOVE_STRUCT		*movBlockData = NULL;
				movBlockData = new SHRUNK_MOVE_STRUCT[header->m_entPosCount];

				int		byteCount = sizeof ( SHRUNK_MOVE_STRUCT ) * header->m_entPosCount;
				BYTE	*conversionBlock = (BYTE *) movBlockData;	// new BYTE[byteCount];
				for ( int shiftLoop = 0; shiftLoop < byteCount; shiftLoop++ )
				{
					conversionBlock[shiftLoop] = dataBlock[shiftLoop + indexInto];
				}

				indexInto += byteCount;
				for( int i=0; i<header->m_entPosCount; i++ )
				{
					movBlockData[i].animGLID;
				}

				delete [] movBlockData;
			}
		}
		break;
	default:
		if ( (BYTE)identityMsg->m_category >= MIN_EVENT_BYTE && len >= sizeof( EventHeader ) )
		{
			IEvent *e = m_dispatcher.MakeEvent( data, len, from, ESEC_SERVER );
			if ( e )
			{
				m_dispatcher.QueueEvent( e );
				wakeUp();
			}
		}
		else
		{
			stringstream oss;
			string category("");
			oss << category << (int)identityMsg->m_category;
			category = oss.str();
			LOG4CPLUS_TRACE( Logger::getInstance( LOGGER_NAME ), "Unhandled message from server category: " << category );
		}

		return true;
	}

	wakeUp();
	return true;
}

bool ClientInstance::CreateCharacterOnServer( string characterName, CharacterCreateStruct *pCharCreate )
{
	if ( characterName.size() == 0 || characterName.size() > 25 || pCharCreate==NULL )
	{
		return false;
	}

	CREATECHARHEADER_DATA header;
	header.m_category			= CREATE_CHAR_SERVER_CAT;
	header.m_nameLength			= characterName.size();
	header.m_spawnCfg			= pCharCreate->spawnCfg;
	header.m_entityType			= pCharCreate->EDBIndex;
	header.m_attachmentCount	= 0;
	header.m_defCfgCount		= pCharCreate->dim_slots.size();
	header.m_team               = pCharCreate->team;
	header.m_scaleX             = pCharCreate->scaleX;
	header.m_scaleY             = pCharCreate->scaleY;
	header.m_scaleZ             = pCharCreate->scaleZ;

	int		headerSize = sizeof ( CREATECHARHEADER_DATA ); // constant size of the header
	int		itemStructSize = sizeof ( ITEMDATA_STRUCT );   // size of each attachable item
	int		cfgStructSize = sizeof( CUSTOMMAT_STRUCT );    // size of each deformable material

	int		totalSize = ( ( header.m_defCfgCount * cfgStructSize ) + ( header.m_attachmentCount * itemStructSize ) ) +
		headerSize + header.m_nameLength; // Calculation for total size of the message
	BYTE	*dataBlock = new BYTE[totalSize];

	CopyMemory ( dataBlock, &header, headerSize );

	for ( int j = 0; j < header.m_nameLength; j++ )
	{
		dataBlock[( j + headerSize )] = characterName[j];
	}

	if ( ( header.m_defCfgCount + header.m_attachmentCount ) > 0 )
	{
		ITEMDATA_STRUCT *itemDataStruct = NULL;
		CUSTOMMAT_STRUCT *cfgDataStruct = NULL;
		if(header.m_attachmentCount > 0)
		{
			itemDataStruct = new ITEMDATA_STRUCT[header.m_attachmentCount];
		}
		if(header.m_defCfgCount > 0)
		{
			cfgDataStruct = new CUSTOMMAT_STRUCT[header.m_defCfgCount];
		}

#if 0
		// set data
		int				trace = 0;
		if ( pMovementObj->m_currentlyArmedItems )
		{
			for ( POSITION armedPos = pMovementObj->m_currentlyArmedItems->GetHeadPosition (); armedPos != NULL; )
			{		// update configuration/customized
				CItemObj	*itemPtr = (CItemObj *) pMovementObj->m_currentlyArmedItems->GetNext ( armedPos );
				if ( itemPtr->m_equiped )
				{	// make sure hes wearin it
					itemDataStruct[trace].m_GLID = itemPtr->m_globalID;
					trace++;
				}	// end make sure hes wearin it
			}		// end update configuration/customized
		}
#endif

		int loop = 0;
		for ( BYTE i = 0; i < pCharCreate->dim_slots.size(); i++ )
		{
			cfgDataStruct[loop].m_equipId = pCharCreate->dim_slots[i].glid; 
			cfgDataStruct[loop].m_dim = pCharCreate->dim_slots[i].slot; 
			cfgDataStruct[loop].m_cfg = pCharCreate->dim_slots[i].config;
			cfgDataStruct[loop].m_mat = pCharCreate->dim_slots[i].mat;
			cfgDataStruct[loop].m_R = -1;
			cfgDataStruct[loop].m_G = -1;
			cfgDataStruct[loop].m_B = -1;
			loop++;
		}

		BYTE	*cast = (BYTE *) itemDataStruct;
		BYTE	*cast2 = (BYTE *) cfgDataStruct;

		for ( int i = 0; i < ( ( totalSize - headerSize - (cfgStructSize * header.m_defCfgCount)) - header.m_nameLength ); i++ )
		{
			dataBlock[( headerSize + i + header.m_nameLength )] = cast[i];
		}
		for ( int i = headerSize + (itemStructSize * header.m_attachmentCount) + header.m_nameLength ; i < totalSize ; i++ )
		{
			dataBlock[ i  ] = cast2[i - headerSize - (itemStructSize * header.m_attachmentCount) - header.m_nameLength];
		}

		if ( itemDataStruct )
		{
			delete[] itemDataStruct;
			itemDataStruct = 0;
		}
		if ( cfgDataStruct )
		{
			delete[] cfgDataStruct;
			cfgDataStruct = 0;
		}
	}

	SendData((BYTE *)dataBlock, totalSize, NSENDFLAG_GUARANTEED, NULL, 60000);

	if (dataBlock)
		delete [] dataBlock;

	return true;
}

EVENT_PROC_RC ClientInstance::ProcessClientSpawnEvent( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	ClientSpawnEvent *cse = dynamic_cast<ClientSpawnEvent *>(e);
	jsVerifyReturn( cse != 0, EVENT_PROC_RC::NOT_PROCESSED );	

	LONG zoneIndex;
	LONG instanceId;
	float x, y, z;
	cse->extractInfo( zoneIndex, instanceId, x, y, z );

#ifndef USE_CView
	if ( m_dlMgr )
	{

		CString url( m_zoneCustomizationsUrl.c_str() );
		url.Replace( "{0}", numToString( zoneIndex, "%d" ).c_str());
		url.Replace( "{1}", numToString( instanceId, "%d" ).c_str() );
		if ( url.Find("{2}") >= 0 && url.Find("{3}") >= 0 && url.Find("{4}") >= 0 )
		{
			url.Replace( "{2}", numToString( x, "%.2f" ).c_str() );
			url.Replace( "{3}", numToString( y, "%.2f" ).c_str() );
			url.Replace( "{4}", numToString( z, "%.2f" ).c_str() );
		}

		DownloadHandler *dh = dynamic_cast<DownloadHandler*>(m_zoneCustomizationDownloadHandler.operator->());
		jsVerifyReturn( dh != 0, EVENT_PROC_RC::NOT_PROCESSED );	

		dh->DownloadCanceled();

		string urlStr = url.GetBuffer();
		if (urlStr.empty())
			return EVENT_PROC_RC::CONSUMED;

		m_dlMgr->DownloadFileAsync( urlStr, dh );
	}
#endif

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientInstance::ClientSpawnEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	jsVerifyReturn( lparam != 0, EVENT_PROC_RC::NOT_PROCESSED );
	ClientInstance *me = (ClientInstance*)lparam;

	if ( me->m_parentGameId != 0 )
		return EVENT_PROC_RC::NOT_PROCESSED;
	else 		
		return me->ProcessClientSpawnEvent( lparam, disp, e  );
}

EVENT_PROC_RC ClientInstance::AddDynamicObjectHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	jsVerifyReturn( lparam != 0, EVENT_PROC_RC::NOT_PROCESSED );
	ClientInstance *me = (ClientInstance*)lparam;

	AddDynamicObjectEvent * adoe = dynamic_cast<AddDynamicObjectEvent *>(e);
	jsAssert(adoe!=NULL);

	int playerId, placementId, animationId, assetId, friendId, globalId, inventory_sub_type, gameItemId;
	string playlistSwf, playlistParams,textureUrl;
	D3DVECTOR position, direction, slide;
	bool attachable, interactive, canPlayFlash, hasCollision;
	D3DVECTOR boundBox_min, boundBox_max;
	bool derivable;

	adoe->ExtractInfo(
		playerId,
		globalId,
		position.x, position.y, position.z,
		direction.x, direction.y, direction.z,
		slide.x, slide.y, slide.z,
		placementId,
		animationId,
		assetId,
		textureUrl,
		friendId,
		playlistSwf,
		playlistParams,
		attachable, interactive, canPlayFlash, hasCollision,
		boundBox_min.x, boundBox_min.y, boundBox_min.z, 
		boundBox_max.x, boundBox_max.y, boundBox_max.z,
		derivable, inventory_sub_type, gameItemId
	);

	jsAssert( placementId!=0 );
	me->m_dynamicObjects.insert( make_pair(placementId, Placement(placementId, position.x, position.y, position.z, direction.x, direction.y, direction.z, 1, globalId )));

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientInstance::RemoveDynamicObjectHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	jsVerifyReturn( lparam != 0, EVENT_PROC_RC::NOT_PROCESSED );
	ClientInstance *me = (ClientInstance*)lparam;

	RemoveDynamicObjectEvent * rdoe = dynamic_cast<RemoveDynamicObjectEvent *>(e);
	jsAssert(rdoe!=NULL);

	int placementId = 0;
	rdoe->ExtractInfo(placementId);

	jsAssert( placementId!=0 );
	map<int, Placement>::iterator it = me->m_dynamicObjects.find(placementId);
	if( it!=me->m_dynamicObjects.end() )
	{
		me->m_dynamicObjects.erase(it);
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientInstance::MoveDynamicObjectHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	jsVerifyReturn( lparam != 0, EVENT_PROC_RC::NOT_PROCESSED );
	ClientInstance *me = (ClientInstance*)lparam;

	MoveDynamicObjectEvent * mdoe = dynamic_cast<MoveDynamicObjectEvent *>(e);
	jsAssert(mdoe!=NULL);

	int placementId = 0;
	double x, y, z;
	DWORD time;
	mdoe->ExtractInfo(placementId, x, y, z, time);

	jsAssert( placementId!=0 );
	map<int, Placement>::iterator it = me->m_dynamicObjects.find(placementId);
	if( it!=me->m_dynamicObjects.end() )
	{
		it->second.x = (float) x;
		it->second.y = (float) y;
		it->second.z = (float) z;
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientInstance::AddTriggerHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	jsVerifyReturn( lparam != 0, EVENT_PROC_RC::NOT_PROCESSED );
	ClientInstance *me = (ClientInstance*)lparam;

	AddTriggerEvent * ate = dynamic_cast<AddTriggerEvent *>(e);
	jsAssert(ate!=NULL);

	LONG placementId = 0;
	USHORT action = 0;
	float radius = 0.0f;
	int intParam = 0;
	string strParam;
	ate->ExtractInfo(placementId, action, radius, intParam, strParam);

	jsAssert(placementId!=0);
	map<int, Placement>::iterator itDO = me->m_dynamicObjects.find(placementId);
	if( itDO!=me->m_dynamicObjects.end() )
	{
		Trigger & trg = me->m_dynamicTriggers[placementId];
		trg.placementId = placementId;
		trg.radius = radius;
		trg.action = action;
		trg.intParam = intParam;
		trg.strParam = strParam;
		trg.triggered = false;
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientInstance::RemoveTriggerHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	jsVerifyReturn( lparam != 0, EVENT_PROC_RC::NOT_PROCESSED );
	ClientInstance *me = (ClientInstance*)lparam;

	RemoveTriggerEvent * rte = dynamic_cast<RemoveTriggerEvent *>(e);
	jsAssert(rte!=NULL);

	LONG placementId = 0;
	rte->ExtractInfo(placementId);

	jsAssert(placementId!=0);
	map<int, Placement>::iterator itDO = me->m_dynamicObjects.find(placementId);
	if( itDO!=me->m_dynamicObjects.end() )
	{
		map<int, Trigger>::iterator it = me->m_dynamicTriggers.find(placementId);
		if( it!=me->m_dynamicTriggers.end() )
		{
			me->m_dynamicTriggers.erase(it);
		}
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientInstance::ScriptClientEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	jsVerifyReturn( lparam != 0, EVENT_PROC_RC::NOT_PROCESSED );
	ClientInstance *me = (ClientInstance*)lparam;

	ScriptClientEvent * sce = dynamic_cast<ScriptClientEvent *>(e);
	jsAssert(sce!=NULL);

	switch( sce->GetEventType() )
	{
	case ScriptClientEvent::PlayerTeleport:
		{
			double x,y,z,rx,ry,rz;
			*sce->InBuffer() >> x >> y >> z >> rx >> ry >> rz;
			me->m_playerPosition.x = (float)x;
			me->m_playerPosition.y = (float)y;
			me->m_playerPosition.z = (float)z;
			me->m_playerOrientation.x = (float)rx;
			me->m_playerOrientation.y = (float)ry;
			me->m_playerOrientation.z = (float)rz;
		}
		break;
	case ScriptClientEvent::PlayerSetAnimation:
		{
			ULONG clientId;
			GLID animGLID;
			(*sce->InBuffer()) >> clientId >> animGLID;
			me->m_playerAnimOverride = animGLID;
			LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), "PlayerSetAnimation: " << animGLID );
		}
		break;
	default:
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ClientInstance::ZoneCustomizationDLCompleteEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e  )
{
	jsVerifyReturn( lparam != 0, EVENT_PROC_RC::NOT_PROCESSED );
	ClientInstance *me = (ClientInstance*)lparam;
	
	EVENT_ID eId = me->m_dispatcher.GetEventId("ZoneCustomizationDLCompleteEvent");
	if( eId != BAD_EVENT_ID )
	{
		IEvent *e = me->m_dispatcher.MakeEvent( eId );
		if( e )
		{
			e->AddTo( SERVER_NETID );
			me->m_dispatcher.QueueEvent( e );
		}
	}

	jsAssert( me->m_curState.state==SPAWN_STRUCT_SND );
	me->SwitchClientState( SPAWNED );
	return EVENT_PROC_RC::OK;
}

// ***********************************
// Static Data and Functions
// ***********************************
map <ULONG, ClientInstance*> threads;

ClientInstance* getInstance( ULONG threadId )
{
	map<ULONG, ClientInstance*>::iterator findThread;
	findThread = threads.find(threadId);

	if ( findThread != threads.end() )
	{
		ClientInstance *currentInstance = findThread->second;
		return currentInstance;
	}
	else
	{
		return 0;
	}
}

static ULONG GetContentVersion( IN LPCTSTR dir )
{
	string path;

	// for client, use current dir, server will pass in the game dir
	if ( dir )
		path = dir;

	path += "\\";
	path += VERSIONINFO_DAT;
	char version[100];
	int len = GetPrivateProfileString( TEXT("CURRENT"), TEXT("VERSION"), "", version, 99, path.c_str() );

	ULONG ver = 0;
	int ints[4];
	int count = sscanf_s( version, "%d.%d.%d.%d", &ints[0], &ints[1], &ints[2], &ints[3] );
	int i,j;
	for ( i = 4-count, j = count-1; i < 4; i++, j-- )
	{
		ver = ver + (ints[j] << (8*i));
	}

	return ver;
}

static const string StateToString( CLIENT_STATE state )
{
	switch ( state )
	{
	case NOT_CONNECTED:
		return "NOT_CONNECTED";
	    break;
	case CONNECTING:
		return "CONNECTING";
	    break;
	case CONNECTED:
		return "CONNECTED";
	    break;
	case LOGGING_IN:
		return "LOGGING_IN";
	    break;
	case LOGGED_IN:
		return "LOGGED_IN";
	    break;
	case ZONING:
		return "ZONING";
	    break;
	case PENDING_SPAWN_RCV:
		return "PENDING_SPAWN_RCV";
	    break;
	case PENDING_SPAWN_SND:
		return "PENDING_SPAWN_SND";
		break;
	case SPAWN_STRUCT_RCV:
		return "SPAWN_STRUCT_RCV";
		break;
	case SPAWN_STRUCT_SND:
		return "SPAWN_STRUCT_SND";
		break;
	case SPAWNED:
		return "SPAWNED";
	default:
		return "Undefined state = " + state; 
	}
}

// ***********************************
// DLL Interface Functions
// ***********************************
__declspec( dllexport ) bool InitializeClientInstance(	IN IClientInstanceCallback *callback
                                                        , WebServicesProxy&           webProxy
                                                        , unsigned long             gameId
														,LPCTSTR baseDir,
														LPCTSTR user, 
														LPCTSTR pw, 
														LPCTSTR server, 
														UINT port,
														LPCTSTR email,
														ULONG zoneWait,
														LPCTSTR loginURL,
														LPCTSTR authURL,
														vector<vector<string>> webcalls,
														LPCTSTR gameName,
														LPCTSTR tellTarget,
														LPCTSTR tellSender,
														LPCTSTR scriptName,
														LPCTSTR startingUrl,
														CharacterCreateStruct *pCharCreate,
														LPCTSTR zoneCustUrl,
														OUT ULONG &threadId )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	StringResource = AfxGetResourceHandle(); 
	ClientInstance *newInstance = new ClientInstance();
    newInstance->setWebProxy( webProxy );
    newInstance->setGameId(gameId);
	newInstance->Start(baseDir, user, pw, server, port, email, zoneWait, loginURL, authURL, webcalls, gameName, tellTarget, tellSender, scriptName, startingUrl, pCharCreate, zoneCustUrl );
	newInstance->SetInitialZoneTime();
	threadId = newInstance->threadID();
	threads.insert( make_pair(threadId, newInstance) );
	return true;
}

__declspec( dllexport ) bool ShutdownClientInstance( ULONG threadId )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	ClientInstance *currentInstance = getInstance( threadId );
	if ( currentInstance )
	{
		currentInstance->CleanUpEvents();
		return ( currentInstance->Shutdown() == jsThread::srStoppedOk );
	}
	else
	{
		return false;
	}
}

__declspec( dllexport ) bool HasSentPendingSpawn( ULONG threadId )
{
	ClientInstance *currentInstance = getInstance( threadId );
	if ( currentInstance )
	{
		return (currentInstance->m_curState.state >= PENDING_SPAWN_SND);
	}
	else
	{
		return false;
	}
}

__declspec( dllexport) bool SpawnCompleted( ULONG threadId )
{
	ClientInstance *currentInstance = getInstance( threadId );
	if ( currentInstance )
	{
		return (currentInstance->m_curState.state == SPAWNED);
	}
	else
	{
		return false;
	}
}

__declspec( dllexport ) bool LogPlayerOffServer( ULONG threadId )
{
	ClientInstance *currentInstance = getInstance( threadId );
	if ( currentInstance )
	{
		Sleep(100); // don't have all exit at once
		currentInstance->stop(0); // signal it to stop, we'll check again later
		return true;
	}
	else
	{
		return false;
	}
}

__declspec( dllexport ) bool StartZoning( ULONG threadId, vector<vector<string>> zones )
{
	ClientInstance *currentInstance = getInstance( threadId );
	if ( currentInstance )
	{
		currentInstance->m_zones = zones;
		currentInstance->m_startZoning = true;
		currentInstance->QueueEvent( new AutomationEvent( AutomationEvent::AE_START_ZONING ) );
		return true;
	}
	else
	{
		return false;
	}
}

__declspec( dllexport ) UINT RunningCount( bool onlyCareIfOne )
{
	int count = 0;
	map<ULONG, ClientInstance*>::iterator i = threads.begin();
	while ( i != threads.end() )
	{
		if ( i->second->isRunning() )
		{
			if ( onlyCareIfOne )
				return 1;
			else
				count++;
		}	
		i++;
	}
	return count;
}


__declspec( dllexport ) bool StopZoning( ULONG threadId )
{
	ClientInstance *currentInstance = getInstance( threadId );
	if ( currentInstance )
	{
		currentInstance->m_startZoning = false;
		currentInstance->QueueEvent( new AutomationEvent( AutomationEvent::AE_STOP_ZONING ) );
		return true;
	}
	else
	{
		return false;
	}
}

__declspec( dllexport ) bool SendChatMessages( ULONG threadId, string chatMessage, UINT chatWaitTime )
{
	ClientInstance *currentInstance = getInstance( threadId );
	if ( currentInstance )
	{
		currentInstance->m_chatMessage = chatMessage;
		currentInstance->m_chatWaitTime = chatWaitTime;
		return true;
	}
	else
	{
		return false;
	}
}

__declspec( dllexport ) bool SendTellMessages( ULONG threadId, string tellMessage, UINT tellWaitTime )
{
	ClientInstance *currentInstance = getInstance( threadId );
	if ( currentInstance )
	{					
		currentInstance->m_tellMessage = tellMessage;
		currentInstance->m_tellWaitTime = tellWaitTime;
		return true;
	}
	else
	{
		return false;
	}
}

__declspec( dllexport ) bool SendAttachAttribMessages( ULONG threadId, UINT attribWaitTime )
{
	ClientInstance *currentInstance = getInstance( threadId );
	if ( currentInstance )
	{
		currentInstance->m_attachAttribWaitTime = attribWaitTime;
		return true;
	}
	else
	{
		return false;
	}
}

__declspec( dllexport ) bool SendInvAttribMessages( ULONG threadId, UINT attribWaitTime )
{
	ClientInstance *currentInstance = getInstance( threadId );
	if ( currentInstance )
	{
		currentInstance->m_invAttribWaitTime = attribWaitTime;
		return true;
	}
	else
	{
		return false;
	}
}

ULONG ClientInstance::GetArrayCount( IN ULONG indexOfArray )
{
	switch( indexOfArray )
	{
	case CLIENTINSTANCEIDS_PLACEMENTS:
		return m_dynamicObjects.size();
	case CLIENTINSTANCEIDS_TRIGGERS:
		return m_dynamicTriggers.size();
	}
	return GetSet::GetArrayCount( indexOfArray );
}

IGetSet * ClientInstance::GetObjectInArray( IN ULONG indexOfArray, IN ULONG indexInArray )
{
	ULONG trace = 0;
	switch( indexOfArray )
	{
	case CLIENTINSTANCEIDS_PLACEMENTS:
		for( map<int, Placement>::iterator it = m_dynamicObjects.begin(); it!=m_dynamicObjects.end(); ++it, ++trace )
		{
			if( trace==indexInArray )
			{
				return &it->second;
			}
		}
		return NULL;
		break;
	case CLIENTINSTANCEIDS_TRIGGERS:
		for( map<int, Trigger>::iterator it = m_dynamicTriggers.begin(); it!=m_dynamicTriggers.end(); ++it, ++trace )
		{
			if( trace==indexInArray )
			{
				return &it->second;
			}
		}
		return NULL;
		break;
	}

	return GetSet::GetObjectInArray( indexOfArray, indexInArray );
}

double ClientInstance::GetNumber( IN ULONG index )
{
	if (index==CLIENTINSTANCEIDS_TICKCOUNT)
	{
		return (double)GetTickCount();
	}

	return GetSet::GetNumber(index);
}

static const int gsIntArray = gsSimpleArray | gsInt;
static const int gsObjectArray = gsSimpleArray | gsObjectPtr;

BEGIN_GETSET_IMPL(ClientInstance, IDS_CLIENTINST_NAME)
	GETSET2(m_gameName, gsString, GS_FLAGS_DEFAULT | GS_FLAG_INSTANCE_NAME, 0, 0, IDS_CLIENTINST_GAME_NAME, IDS_CLIENTINST_GAME_NAME )
	GETSET2(m_baseDir, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_BASEDIR, IDS_CLIENTINST_BASEDIR )
	GETSET2(m_username, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_USER, IDS_CLIENTINST_USER )
	GETSET2(m_servername, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_SERVER, IDS_CLIENTINST_SERVER )
	GETSET2(m_port, gsUint, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_PORT, IDS_CLIENTINST_PORT )
	GETSET2(m_email, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_EMAIL, IDS_CLIENTINST_EMAIL )
	GETSET2(m_url, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_URL, IDS_CLIENTINST_URL )
	GETSET2(m_gameId, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_GAMEID, IDS_CLIENTINST_GAMEID)
	GETSET2(m_parentGameId, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_PARENTGAMEID, IDS_CLIENTINST_PARENTGAMEID )
	GETSET2(m_scriptName, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_SCRIPTNAME, IDS_CLIENTINST_SCRIPTNAME )
	GETSET2(m_networkDefinedId, gsShort, GS_FLAGS_ACTION, 0, 0, IDS_CLIENTINST_NETWORKDEFINEDID, IDS_CLIENTINST_NETWORKDEFINEDID)
	GETSET2(m_zoneIndex, gsInt, GS_FLAGS_ACTION, 0, 0, IDS_CLIENTINST_ZONEINDEX, IDS_CLIENTINST_ZONEINDEX)
	GETSET2(m_instanceId, gsInt, GS_FLAGS_ACTION, 0, 0, IDS_CLIENTINST_INSTANCEID, IDS_CLIENTINST_INSTANCEID)
	GETSET2(m_playerPosition, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_PLAYERPOSITION, IDS_CLIENTINST_PLAYERPOSITION )	
	GETSET2(m_playerOrientation, gsD3dvector, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_PLAYERORIENTATION, IDS_CLIENTINST_PLAYERORIENTATION )	
	GETSET2(m_playerAnim, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_PLAYERANIM, IDS_CLIENTINST_PLAYERANIM )	
	GETSET2(m_playerAnim2, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_PLAYERANIM2, IDS_CLIENTINST_PLAYERANIM2 )
	GETSET2(m_playerAnimOverride, gsInt, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_PLAYERANIMOVERRIDE, IDS_CLIENTINST_PLAYERANIMOVERRIDE)
	GETSET_CUSTOM(NULL, gsObjectArray, GS_FLAGS_ACTION, 0, 0, CLIENTINST, PLACEMENTS, 0, 0)
	GETSET_CUSTOM(NULL, gsObjectArray, GS_FLAGS_ACTION, 0, 0, CLIENTINST, TRIGGERS, 0, 0)
	GETSET_CUSTOM(NULL, gsInt, GS_FLAGS_ACTION, 0, 0, CLIENTINST, TICKCOUNT, 0, 0)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(Trigger, IDS_TRIGGER_NAME)
	GETSET2(placementId, gsInt, GS_FLAGS_ACTION, 0, 0, IDS_TRIGGER_PLACEMENTID, IDS_TRIGGER_PLACEMENTID)
	GETSET2(radius, gsFloat, GS_FLAGS_ACTION, 0, 0, IDS_TRIGGER_RADIUS, IDS_TRIGGER_RADIUS)
	GETSET2(action, gsInt, GS_FLAGS_ACTION, 0, 0, IDS_TRIGGER_ACTION, IDS_TRIGGER_ACTION)
	GETSET2(intParam, gsInt, GS_FLAGS_ACTION, 0, 0, IDS_TRIGGER_INTPARAM, IDS_TRIGGER_INTPARAM)
	GETSET2(strParam, gsString, GS_FLAGS_ACTION, 0, 0, IDS_TRIGGER_STRPARAM, IDS_TRIGGER_STRPARAM)
	GETSET2(triggered, gsBool, GS_FLAGS_ACTION, 0, 0, IDS_TRIGGER_TRIGGERED, IDS_TRIGGER_TRIGGERED)
END_GETSET_IMPL

BEGIN_GETSET_IMPL(Placement, IDS_PLACEMENT_NAME)
	GETSET2(placementId, gsInt, GS_FLAGS_ACTION, 0, 0, IDS_PLACEMENT_PLACEMENTID, IDS_PLACEMENT_PLACEMENTID)
	GETSET2(x, gsFloat, GS_FLAGS_ACTION, 0, 0, IDS_PLACEMENT_X, IDS_PLACEMENT_X)
	GETSET2(y, gsFloat, GS_FLAGS_ACTION, 0, 0, IDS_PLACEMENT_Y, IDS_PLACEMENT_Y)
	GETSET2(z, gsFloat, GS_FLAGS_ACTION, 0, 0, IDS_PLACEMENT_Z, IDS_PLACEMENT_Z)
	GETSET2(rx, gsFloat, GS_FLAGS_ACTION, 0, 0, IDS_PLACEMENT_RX, IDS_PLACEMENT_RX)
	GETSET2(ry, gsFloat, GS_FLAGS_ACTION, 0, 0, IDS_PLACEMENT_RY, IDS_PLACEMENT_RY)
	GETSET2(rz, gsFloat, GS_FLAGS_ACTION, 0, 0, IDS_PLACEMENT_RZ, IDS_PLACEMENT_RZ)
	GETSET2(radius, gsFloat, GS_FLAGS_ACTION, 0, 0, IDS_PLACEMENT_RADIUS, IDS_PLACEMENT_RADIUS)
	GETSET2(globalId, gsInt, GS_FLAGS_ACTION, 0, 0, IDS_PLACEMENT_GLOBALID, IDS_PLACEMENT_GLOBALID)
END_GETSET_IMPL
