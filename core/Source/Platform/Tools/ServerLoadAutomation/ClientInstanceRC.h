//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ClientInstance.rc
//
#define IDS_CLIENTINST_NAME             101
#define IDS_CLIENTINST_GAME_NAME        102
#define IDS_APP_TITLE                   103
#define IDS_CLIENTINST_BASEDIR          103
#define IDS_CLIENTINST_USER             104
#define IDS_CLIENTINST_SERVER           105
#define IDS_CLIENTINST_PORT             106
#define IDS_CLIENTINST_EMAIL            107
#define IDS_CLIENTINST_URL              108
#define IDS_CLIENTINST_GAMEID           109
#define IDS_CLIENTINST_PARENTGAMEID     110
#define IDS_CLIENTINST_SCRIPTNAME       111
#define IDS_CLIENTINST_PLAYERPOSITION   112
#define IDS_CLIENTINST_PLAYERORIENTATION 113
#define IDS_CLIENTINST_PLAYERANIM       114
#define IDS_CLIENTINST_PLAYERANIM2      115
#define IDS_TRIGGER_NAME                116
#define IDS_TRIGGER_RADIUS              117
#define IDS_TRIGGER_ACTION              118
#define IDS_TRIGGER_INTPARAM            119
#define IDS_TRIGGER_STRPARAM            120
#define IDS_TRIGGER_TRIGGERED           121
#define IDS_PLACEMENT_NAME              122
#define IDS_PLACEMENT_X                 123
#define IDS_PLACEMENT_Y                 124
#define IDS_PLACEMENT_Z                 125
#define IDS_PLACEMENT_RX                126
#define IDS_PLACEMENT_RY                127
#define IDS_PLACEMENT_RZ                128
#define IDS_PLACEMENT_RADIUS            129
#define IDS_PLACEMENT_GLOBALID          130
#define IDS_PLACEMENT_PLACEMENTID       131
#define IDS_CLIENTINST_PLACEMENTS       132
#define IDS_CLIENTINST_NETWORKDEFINEDID 133
#define IDS_CLIENTINST_TRIGGERS         134
#define IDS_CLIENTINST_ZONEINDEX        135
#define IDS_CLIENTINST_PLACEMENTS_HELP  136
#define IDS_CLIENTINST_INSTANCEID       137
#define IDS_CLIENTINST_TRIGGERS_HELP    138
#define IDS_TRIGGER_PLACEMENTID         139
#define IDS_CLIENTINST_TICKCOUNT        140
#define IDS_CLIENTINST_TICKCOUNT_HELP   141
#define IDS_CLIENTINST_PLAYERANIMOVERRIDE 142

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
