#pragma once

#include "stdafx.h"
#include "resource.h"
#include <direct.h>
#include <sys\stat.h>

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>

#include <sstream>
#include <iostream>
#include <fstream>

#include "ClientInstance.h"
#include "Plugins/App/WebServicesProxy.h"

#define LOGGER_NAME "ClientInstance"

CWinApp theApp;

using namespace std;
using namespace log4cplus;

class ServerLoadAutomation : public IClientInstanceCallback
{
public:
	ServerLoadAutomation();
	
	vector<vector<string>> user_info;
	vector<vector<string>> zones_info;
	vector<vector<string>> webcalls_info;
	
	void InitLogger();
	bool InitConfig( int argc, TCHAR* argv[] );
	virtual void LogMessage( LPCTSTR msg );
	
	vector<string> ParseDataLine( string dataLine );
	bool ReadDataFile( string file_name, vector<vector<string>> &out_vector, int count = -1, int offset = 0 );

	ULONG StartClientInstance( LPCTSTR user, 
								 LPCTSTR pw, 
								 LPCTSTR server, 
								 UINT port,
								 LPCTSTR email,
								 ULONG zoneWait,
								 vector<vector<string>> webcalls,
								 LPCTSTR gameName,
								 LPCTSTR tellTarget,
								 LPCTSTR tellSender, 
								 LPCTSTR scriptName,
								 LPCTSTR startingUrl,
								 CharacterCreateStruct *pCharCreate);
	unsigned int 	user_count;
	unsigned int 	zone_wait_ms;
	unsigned int 	attach_wait_ms;
	unsigned int 	inv_wait_ms;
	unsigned int 	text_msg_wait_ms;
	unsigned int 	test_run_ms;
	string 			extra_text_msg;
	unsigned int 	concurrent_logons;
	string			login_url;
	string			auth_url;
	string			zone_cust_url;
	unsigned int	tell_msg_wait_ms;
	string			extra_tell_msg;
	string			script_name;
    WebServicesProxy    _webServices;          // performs interactions with wok web
                                            // services on our behalf.
    unsigned long       _gameId;
};

