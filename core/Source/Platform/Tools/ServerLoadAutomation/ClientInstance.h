/******************************************************************************
 ServerLoadAutomation.h

 Copyright (c) 2004-2009 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#ifndef _SERVERLOADAUTOMATION_H_
#define _SERVERLOADAUTOMATION_H_

#ifdef BUILD_SERVERLOAD
#define DLLIMPORT __declspec( dllexport ) 
#else
#define DLLIMPORT __declspec( dllimport )
#endif

#define LOGGER_NAME "ClientInstance"

#include "stdafx.h"
#include "ClientInstanceRC.h"
#include <direct.h>

#include "KEPHelpers.h"
#include "INetwork.h"

#include "Event\EventSystem\Dispatcher.h"
#include "Event/Base/ISendHandler.h"
#include <CStructuresMisl.h>
#include "event\events\LogonResponseEvent.h"
#include "event\events\PendingSpawnEvent.h"
#include "event\events\ClientExitEvent.h"
#include "Event\EventSystem\EventSyncEventHandler.h"
#include "event\events\EventSyncEvent.h"
#include "Plugins/App/WebServicesProxy.h"

#include <vector>

class IClientInstanceCallback
{
public:
	virtual void LogMessage( IN LPCTSTR msg )
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState());
	}
};

struct DimConfigStruct
{
	int glid;
	int slot;
	int config;
	int mat;
};

struct CharacterCreateStruct
{
	char gender;
	int EDBIndex;
	vector<DimConfigStruct> dim_slots;
	int spawnCfg;
	int team;
	float scaleX, scaleY, scaleZ;
};

DLLIMPORT bool InitializeClientInstance( IN IClientInstanceCallback *callback,
                                            WebServicesProxy&       webServices
                                            , unsigned long         gameId
										    , LPCTSTR baseDir,
										 LPCTSTR user, 
										 LPCTSTR pw, 
										 LPCTSTR server, 
										 UINT port,
										 LPCTSTR email,
										 ULONG zoneWait,
										 LPCTSTR loginURL,
										 LPCTSTR authURL,
										 vector<vector<string>> webcalls,
										 LPCTSTR gameName,
										 LPCTSTR tellTarget,
										 LPCTSTR tellSender,
										 LPCTSTR scriptName,
										 LPCTSTR startingUrl,
										 CharacterCreateStruct *pCharCreate,
										 LPCTSTR zoneCustUrl,
										 OUT ULONG &threadId );

DLLIMPORT bool ShutdownClientInstance( ULONG threadId );

DLLIMPORT bool HasSentPendingSpawn( ULONG threadId );

DLLIMPORT bool SpawnCompleted( ULONG threadId );

DLLIMPORT bool LogPlayerOffServer( ULONG threadId );

DLLIMPORT UINT RunningCount(  bool onlyCareIfOne );

DLLIMPORT bool StartZoning( ULONG threadId, vector<vector<string>> zones );

DLLIMPORT bool StopZoning( ULONG threadId );

DLLIMPORT bool SendChatMessages( ULONG threadId, string chatMessage, UINT chatWaitTime );

DLLIMPORT bool SendAttachAttribMessages( ULONG threadId, UINT attribWaitTime );

DLLIMPORT bool SendInvAttribMessages( ULONG threadId, UINT attribWaitTime );

DLLIMPORT bool SendTellMessages( ULONG threadId, string chatMessage, UINT chatWaitTime );

#endif
