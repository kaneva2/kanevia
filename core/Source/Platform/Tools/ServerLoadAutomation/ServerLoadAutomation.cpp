#include "ServerLoadAutomation.h"
#include "KEPConfig.h"
#include "common\keputil\Algorithms.h"
extern HINSTANCE StringResource;

#define USER_NAME 0
#define USER_PW 1
#define USER_SERVER 2
#define USER_PORT 3
#define USER_EMAIL 4
#define USER_GAME_NAME 5
#define USER_TELL_TARGET 6
#define USER_TELL_SENDER 7
#define USER_STARTING_URL 8
#define USER_PLAYERCREATE_GENDER 9
#define USER_PLAYERCREATE_EDBINDEX 10
#define USER_PLAYERCREATE_BODYSLOTS 11
#define USER_PLAYERCREATE_HEADSLOTS 12
#define USER_PLAYERCREATE_HEADGLID 13
#define USER_PLAYERCREATE_SPAWNCFG 14
#define USER_PLAYERCREATE_TEAM 15
#define USER_PLAYERCREATE_SCALEX 16
#define USER_PLAYERCREATE_SCALEY 17
#define USER_PLAYERCREATE_SCALEZ 18

ServerLoadAutomation::ServerLoadAutomation() :
	user_count(0),
	zone_wait_ms(0),
	attach_wait_ms(0),
	inv_wait_ms(0),
	text_msg_wait_ms(0),
	tell_msg_wait_ms(0),
	test_run_ms(0),
	concurrent_logons(0),
	login_url(""),
	auth_url("")
    , _webServices()
{
}

void ServerLoadAutomation::InitLogger() {
    static ConfigureAndWatchThread *m_configureThread(0);

    char logDir[_MAX_PATH];
    char baseDir[_MAX_PATH];
    baseDir[0] = '\0';
    _getcwd( baseDir, _MAX_PATH );
    strncpy_s( logDir, _countof(logDir), baseDir, _MAX_PATH );
    strncat_s( baseDir, _countof(baseDir) , "\\ServerLoadAutomationLog.cfg", _MAX_PATH );

    KEP::LoggingSubsystem logging(baseDir); // ::start(baseDir);
    logging.start("Begin run");
}

/// \brief initialize the SLA by parsing args and loading files
bool ServerLoadAutomation::InitConfig(int argc, TCHAR* argv[])
{
	if ( argc < 2 )
	{
		fprintf( stderr, "WOK Automated Server Load Tool (ASLT)\n\n"
						 "Usage:\n"
						 "    ServerLoadAutomation <configfile> [-u <count> [<offset>]] [-s <scriptname>]\n"
						 "      where <configfile> is XML configuration file\n"
						 "            <count> is override count of users to login\n"
						 "            <offset> is override offset in users file\n"
						 "            <scriptname> is the name of a script to load and run\n\n"
						 "      See twiki for full details\n\n"
						 );
		return false;		
	}

	// parse args
	LPCTSTR configFile = argv[1];

	int user_count = -1; // all
	int user_offset = 0; // start from top

	int zone_count = -1; // all
	int zone_offset = 0; // start from top
	
	int webcalls_count = -1;
	int webcalls_offset = 0;

	string zones_file;
	string users_file;
	string webcalls_file;
	
	try
	{
		KEPConfig cfg;
		cfg.Load(configFile, "ASLT");
		
		zones_file			= cfg.Get("ZonesFile", "zones.dat");
		zone_count			= cfg.Get("ZoneCount", -1);
		zone_offset			= cfg.Get("ZoneOffset",0);
		
		users_file			= cfg.Get("UsersFile", "users.dat");
		user_count			= cfg.Get("UserCount", -1);
		user_offset			= cfg.Get("UserOffset",0);

		zone_wait_ms		= cfg.Get("ZoneWaitSec",300)*1000; 
		attach_wait_ms		= cfg.Get("AttachAttribWaitSec",37)*1000; 
		inv_wait_ms			= cfg.Get("InvAttribWaitSec",37)*1000; 

		text_msg_wait_ms	= cfg.Get("TextWaitSec",60)*1000; 
		extra_text_msg		= cfg.Get("ExtraTextMsg","");

		tell_msg_wait_ms    = cfg.Get("TellWaitSec",60)*1000;
		extra_tell_msg      = cfg.Get("ExtraTellMsg","");

		webcalls_file       = cfg.Get("WebCallsFile", "webcalls.dat");
		webcalls_count      = cfg.Get("WebCallsCount", -1);
		webcalls_offset     = cfg.Get("WebCallsOffset",0);

        _webServices.setServicesHost(cfg.Get("WebServices/Host", "localhost"));
        _webServices.setServicesPort(cfg.Get("WebServices/Port", 80 ));
        _gameId = cfg.Get("GameId", 0 );

		login_url = cfg.Get("LoginURL","");
		auth_url = cfg.Get("AuthURL","");
		zone_cust_url = cfg.Get("ZoneCustomizationUrl", "");

		test_run_ms = cfg.Get("TotalRunMin",5)*60;
		test_run_ms = cfg.Get("TotalRunSec", (int)test_run_ms ); // for debugging let sec override, not documented
		test_run_ms *= 1000;
		
		concurrent_logons = cfg.Get("ConncurrentLogons",1);
		
		if ( test_run_ms == 0 || 
			 zone_wait_ms == 0 || 
			 concurrent_logons == 0 )		{
			fprintf( stderr, "Zero config value in file.  Validate values.\n" );
			return false;
		}
	}
	catch ( KEPException *e )	{
		fprintf( stderr, "Error reading \"%s\": %s\n", configFile, e->ToString().c_str() );
		e->Delete();
		return false;
	}
	
	// check for override of user counts	
	for ( int i = 2; i < argc; i++  )
	{
		if ( strcmp( argv[i], "-u" ) == 0 && i+1 < argc )
		{
			i++;
			user_count = atoi(argv[i]);
			if ( i+1 < argc && isdigit(argv[i+1][0] ) )
			{
				i++;
				user_offset = atoi( argv[i] );
			}
		}
		else if ( strcmp( argv[i], "-s" ) == 0 && i+1 < argc )
		{
			i++;
			script_name = argv[i];
		}
	}
	
	if ( user_offset < 0 || zone_offset < 0 )	{
		fprintf( stderr, "Invalid offset\n" );
		return false;
    }
	if ( !ReadDataFile( zones_file, zones_info, zone_count, zone_offset ) )
		return false;

	if ( !ReadDataFile( users_file, user_info, user_count, user_offset ) )
		return false;

	if ( !ReadDataFile( webcalls_file, webcalls_info, webcalls_count, webcalls_offset ) )
		return false;

	fprintf( stderr, "Starting with config values from \"%s\" \n", configFile );
	fprintf( stderr, "    zones_file = %s\n", zones_file.c_str() );
	fprintf( stderr, "    zone_count = %d\n", zone_count );
	fprintf( stderr, "    zone_offset = %d\n\n", zone_offset );
	fprintf( stderr, "    users_file = %s\n", users_file.c_str() );
	fprintf( stderr, "    user_count = %d\n", user_count );
	fprintf( stderr, "    user_offset = %d\n\n", user_offset );
	fprintf( stderr, "    webcalls_file = %s\n", webcalls_file.c_str() );
	fprintf( stderr, "    webcalls_count = %d\n", webcalls_count );
	fprintf( stderr, "    webcalls_offset = %d\n\n", webcalls_offset );
	fprintf( stderr, "    zone_wait_ms = %d\n", zone_wait_ms );
	fprintf( stderr, "    attach_wait_ms = %d\n", attach_wait_ms );
	fprintf( stderr, "    inv_wait_ms = %d\n", inv_wait_ms );
	fprintf( stderr, "    text_msg_wait_ms = %d\n\n", text_msg_wait_ms );
	fprintf( stderr, "    extra_text_msg = %s\n\n", extra_text_msg.c_str() );
	fprintf( stderr, "    tell_msg_wait_ms = %d\n\n", tell_msg_wait_ms );
	fprintf( stderr, "    extra_tell_msg = %s\n\n", extra_tell_msg.c_str() );
	fprintf( stderr, "    test_run_ms = %d\n", test_run_ms );
	fprintf( stderr, "    concurrent_logons = %d\n\n", concurrent_logons );
	fprintf( stderr, "    Login URL = %s\n", login_url.c_str() );
	fprintf( stderr, "    Auth URL = %s\n\n", auth_url.c_str() );
	fprintf( stderr, "    Zone Customization URL = %s\n\n", zone_cust_url.c_str() );
	fprintf( stderr, "    Script name = %s\n\n", script_name.c_str() );
	
	return true;	
}

void ServerLoadAutomation::LogMessage( LPCTSTR msg )
{
	LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), msg );
}

vector<string> ServerLoadAutomation::ParseDataLine( string dataLine )		{
	vector<string> returnVector;

	// If the string is empty...don't bother.
	//
	if ( dataLine.size() <= 0 ) return returnVector;

	// If the line is a comment...don't bother.
	//
    if ( dataLine.at(0) == ';' ) return returnVector;

	size_t start_pos = 0;
	size_t found_pos;

	do 
	{
		found_pos = dataLine.find( "|", start_pos );
		if ( found_pos != string::npos )
		{
			returnVector.push_back(dataLine.substr(start_pos, found_pos-start_pos));
			start_pos = found_pos+1;
		}
	}
	while ( found_pos != string::npos );

	returnVector.push_back(dataLine.substr(start_pos));
	return returnVector;
}

bool ServerLoadAutomation::ReadDataFile(	string						file_name
											, vector<vector<string>> &	out_vector
											, int						count /*= -1*/
											, int						offset /*= 0*/		)
{
	// Guard clause.
	//
	if ( count <= 0 ) return true;

	ifstream config_to_read;
	config_to_read.open(file_name.c_str());


	if (!config_to_read.is_open())		{
		fprintf( stderr,  "Error reading \"%s\"", file_name.c_str() );
		return false;
	}

	int lineCount = 0;

	while( !config_to_read.eof()
		   && out_vector.size() < (unsigned int)count)			{

		string current_line;
		getline(config_to_read, current_line);
		vector<string> current_info = ParseDataLine( current_line );

		// if there are no elements (i.e. an empty or commented line) 
		// don't bother...just get the next line.
		//
		if ( current_info.size() <= 0 ) continue;

		// Test and increment our line count to see if we are in
		// the offset window.  If not just the get the next line.
		//
		if ( lineCount++ < offset  ) continue;

		out_vector.push_back( current_info );
	}

	config_to_read.close();

	if ( out_vector.size() < (unsigned int)count )
		fprintf( stderr, "Data file \"%s\" had less the count items in it.  Going with min.", file_name.c_str() );
	
	return true;
}

ULONG ServerLoadAutomation::StartClientInstance(    LPCTSTR                     user
                                                    , LPCTSTR                   pw
                                                    , LPCTSTR                   server
                                                    , UINT                      port
                                                    , LPCTSTR                   email
                                                    , ULONG                     zoneWait
                                                    , vector<vector<string>>    webcalls
                                                    , LPCTSTR                   gameName
                                                    , LPCTSTR                   tellTarget
                                                    , LPCTSTR                   tellSender
                                                    , LPCTSTR                   scriptName
                                                    , LPCTSTR                   startingUrl
                                                    , CharacterCreateStruct*    pCharCreate )
{
	ULONG threadId = -1;
	TCHAR curDir[_MAX_PATH];
	_getcwd( curDir, _MAX_PATH );
		
	InitializeClientInstance(   this
                                , _webServices
                                , _gameId
                                , curDir
                                , user
                                , pw
                                , server
                                , port
                                , email
                                , zoneWait
                                , login_url.c_str()
                                , auth_url.c_str()
                                , webcalls
                                , gameName
                                , tellTarget
                                , tellSender
                                , scriptName
                                , startingUrl
                                , pCharCreate
                                , zone_cust_url.c_str()
                                , threadId );

	return threadId;
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		return 1;
	}
	CoInitializeEx(NULL, COINIT_MULTITHREADED);
	StringResource = ::GetModuleHandle(NULL);
	ServerLoadAutomation *sla = new ServerLoadAutomation();
	sla->InitLogger();

	if ( !sla->InitConfig( argc, argv ) ) return 1;

	vector<ULONG> threadIds;
	if ( ! sla->user_info.size() ) {
		fprintf( stderr, "No users configured\n" );
		return nRetCode;
	}

	printf( "Starting....\n" );
	
	unsigned int i = 0;
	while ( i < sla->user_info.size()  )
	{
		ULONG startTime = TickCount();
		ULONG end = min( i + sla->concurrent_logons, sla->user_info.size() );
		for ( ULONG j = i; j < end; j++ )
		{
			string tellTarget = "";
			if ( sla->user_info[j].size() > USER_TELL_TARGET )
			{
				tellTarget = sla->user_info[j][USER_TELL_TARGET];
			}

			string tellSender = "";
			if ( sla->user_info[j].size() > USER_TELL_SENDER )
			{
				tellSender = sla->user_info[j][USER_TELL_SENDER];
			}

			string starting_url;
			if ( sla->user_info[j].size() > USER_STARTING_URL )
			{
				starting_url = sla->user_info[j][USER_STARTING_URL];
			}
			
			CharacterCreateStruct *pCharCreate = NULL;
			if( sla->user_info[j].size() > USER_PLAYERCREATE_SPAWNCFG )
			{
				char gender = 'M';
				int EDBIndex = 0;
				int bodySlots = 0;
				int headSlots = 0;
				int headGlid = 0;
				int spawnCfg = 0;

				if( sla->user_info[j][USER_PLAYERCREATE_GENDER].size()==1 )
				{
					gender = sla->user_info[j][USER_PLAYERCREATE_GENDER].at(0);
				}

				EDBIndex = atoi( sla->user_info[j][USER_PLAYERCREATE_EDBINDEX].c_str() );
				bodySlots = atoi( sla->user_info[j][USER_PLAYERCREATE_BODYSLOTS].c_str() );
				headSlots = atoi( sla->user_info[j][USER_PLAYERCREATE_HEADSLOTS].c_str() );
				headGlid = atoi( sla->user_info[j][USER_PLAYERCREATE_HEADGLID].c_str() );
				spawnCfg = atoi( sla->user_info[j][USER_PLAYERCREATE_SPAWNCFG].c_str() );

				if( EDBIndex>0 && bodySlots>0 && headSlots>0 && headGlid>0 && spawnCfg>0 )
				{
					int team = 2;
					if( sla->user_info[j].size() > USER_PLAYERCREATE_TEAM )
					{
						team = atoi( sla->user_info[j][USER_PLAYERCREATE_TEAM].c_str() );
						if( team==0 )
						{
							team = 2;
						}
					}

					pCharCreate = new CharacterCreateStruct;
					pCharCreate->gender = gender;
					pCharCreate->EDBIndex = EDBIndex;
					for( int i=0; i<bodySlots; i++ )
					{
						pCharCreate->dim_slots.push_back( DimConfigStruct() );
						pCharCreate->dim_slots.back().glid = 0;
						pCharCreate->dim_slots.back().slot = i;
						pCharCreate->dim_slots.back().config = 0;
						pCharCreate->dim_slots.back().mat = 0;
					}
					for( int i=0; i<headSlots; i++ )
					{
						pCharCreate->dim_slots.push_back( DimConfigStruct() );
						pCharCreate->dim_slots.back().glid = headGlid;
						pCharCreate->dim_slots.back().slot = i;
						pCharCreate->dim_slots.back().config = 0;
						pCharCreate->dim_slots.back().mat = 0;
					}
					pCharCreate->spawnCfg = spawnCfg;
					pCharCreate->team = team;
					pCharCreate->scaleX = 1.0f;
					pCharCreate->scaleY = 1.0f;
					pCharCreate->scaleZ = 1.0f;

					if( sla->user_info[j].size() > USER_PLAYERCREATE_SCALEZ )
					{
						float scaleX = (float)atof( sla->user_info[j][USER_PLAYERCREATE_SCALEX].c_str() );
						float scaleY = (float)atof( sla->user_info[j][USER_PLAYERCREATE_SCALEY].c_str() );
						float scaleZ = (float)atof( sla->user_info[j][USER_PLAYERCREATE_SCALEZ].c_str() );
						if( scaleX>0 && scaleY>0 && scaleZ>0 )
						{
							pCharCreate->scaleX = scaleX;
							pCharCreate->scaleY = scaleY;
							pCharCreate->scaleZ = scaleZ;
						}
					}
				}
			}

			ULONG threadId = sla->StartClientInstance( sla->user_info[j][USER_NAME].c_str(), sla->user_info[j][USER_PW].c_str(), 
														sla->user_info[j][USER_SERVER].c_str(), atoi((sla->user_info[j][USER_PORT]).c_str()),
														sla->user_info[j][USER_EMAIL].c_str(),
														sla->zone_wait_ms,
														sla->webcalls_info,
														sla->user_info[j][USER_GAME_NAME].c_str(),
														tellTarget.c_str(),
														tellSender.c_str(),
														sla->script_name.c_str(),
														starting_url.c_str(),
														pCharCreate);

			threadIds.push_back(threadId);
		}
		
		for ( ULONG j = i; j < end; j++ )
		{
			while ( !SpawnCompleted( threadIds[j] ) && TickDiff(TickCount(),startTime) < 60000 ) // wait at most a minute 
				Sleep(10);
		
			printf( "Users: %d\r", ++i );
		}
	}
	ULONG start = TickCount();
	printf( "\nAll connected.  Will now run for %d secs\n", (int)(sla->test_run_ms / 1000) );
	
	vector<ULONG>::iterator it;

	LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), "Iterating threads" );

	for ( it = threadIds.begin(); it < threadIds.end(); it++ )
	{
		ULONG currentThreadId = *it;
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), "Calling Start Zoning" );

		StartZoning( currentThreadId, sla->zones_info );
		if ( sla->attach_wait_ms > 0 )
		{
			SendAttachAttribMessages( currentThreadId, sla->attach_wait_ms );
		}

		if ( sla->inv_wait_ms > 0 )
		{
			SendInvAttribMessages( currentThreadId, sla->inv_wait_ms );
		}
		
		if ( sla->text_msg_wait_ms > 0 )
		{
			SendChatMessages( currentThreadId, sla->extra_text_msg, sla->text_msg_wait_ms );
		}

		if ( sla->tell_msg_wait_ms > 0 )
		{
			SendTellMessages( currentThreadId, sla->extra_tell_msg, sla->tell_msg_wait_ms );
		}
	}

	ULONG diff = 0;
	ULONG totSec = sla->test_run_ms /1000;
	while ( (diff = TickDiff( TickCount(), start )) < sla->test_run_ms )
	{
		Sleep(100);
		printf ( "%5d\r", totSec - (ULONG)(diff/1000) );
		if ( RunningCount( true ) == 0 )
			break;
	}
	printf ( "\nStopping...\n" );

	for ( it = threadIds.begin(); it < threadIds.end(); it++ )
	{
		ULONG currentThreadId = *it;
		StopZoning( currentThreadId );
	}

	for ( it = threadIds.begin(); it < threadIds.end(); it++ )
	{
		ULONG currentThreadId = *it;
		LogPlayerOffServer( currentThreadId );
	}

	for ( it = threadIds.begin(); it < threadIds.end(); it++ )
	{
		ULONG currentThreadId = *it;
		ShutdownClientInstance( currentThreadId );
	}
	
	return nRetCode;
}
