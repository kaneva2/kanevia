/******************************************************************************
 lua-cjson.h - DRF

 DLL Export Declarations For lua-cjson open source library.
 
 Copyright (c) 2013 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

//#include "LuaHelper.h"

int __declspec(dllexport) luaopen_cjson(lua_State *l);

int __declspec(dllexport) luaopen_cjson_safe(lua_State *l);
