///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <windows.h>
#include <dsound.h>

#include <log4cplus/Logger.h>

#define AUDIO_NUM_STREAMS		   12	// max. number of simulteaneous playing streams
#define AUDIO_NUM_CHANNELS		    2
#define AUDIO_SAMPLE_RATE		44100
#define AUDIO_BITS_PER_SAMPLE	   16
#define AUDIO_SAMPLE_MSEC		   30	// duration of a single block
#define NUM_BUFFER_SEGMENTS			2	// number of blocks in a sound buffer

#define AUDIO_BYTES_PER_SEC		(AUDIO_NUM_CHANNELS * AUDIO_SAMPLE_RATE * AUDIO_BITS_PER_SAMPLE / 8)
#define AUDIO_BLOCK_SIZE		( ( AUDIO_BYTES_PER_SEC * AUDIO_SAMPLE_MSEC ) / 1000 )

class AudioStream
{
	public:

		virtual void DoBlock(void *pBlock, int nBlockSize) = 0;
};

class Audio
{
	public:

		static Audio *GetInstance();

		int PlayStream(AudioStream *pStream, const WAVEFORMATEX &format);
		
		void CancelStream(int index);

	private:

		Audio();
		~Audio();

		static DWORD WINAPI BufferServiceRoutine(Audio *pAudio);

		LPDIRECTSOUNDBUFFER8 CreateSoundBuffer( const WAVEFORMATEX *pWaveFormat, HANDLE *pEvents );
		HRESULT SetupBufferNotifications( LPDIRECTSOUNDBUFFER8 pSoundBuffer8, HANDLE *pEvents );

		int GetFreeStreamIndex() const;

		void DoSample(int stream, int sample);
		void ConvertMono16ToStereo16(USHORT *pDst, const USHORT *pSrc, DWORD nSamples);

	private:

		static Audio *m_pAudio;

		LPDIRECTSOUND8 m_pDSound;

		HANDLE m_hServiceThread;

		AudioStream *m_pStreams[AUDIO_NUM_STREAMS];
		LPDIRECTSOUNDBUFFER8 m_pBuffers[AUDIO_NUM_STREAMS];
		WAVEFORMATEX m_pFormat[AUDIO_NUM_STREAMS];

		// The 0th element is the quit notification, the rest
		// are buffer ready notifications
		HANDLE m_hAudioEvents[1 + 2 * AUDIO_NUM_STREAMS];

		log4cplus::Logger m_logger;

		CRITICAL_SECTION m_cs;
};
