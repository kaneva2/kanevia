///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#include "WindowsMedia.h"

#include "KepHelpers.h"

#include "LogHelper.h"

SampleBuffer::SampleBuffer()
	: m_dwSampleOffset(0)
{
	InitializeCriticalSection( &m_cs );
}

SampleBuffer::~SampleBuffer()
{
	DeleteCriticalSection( &m_cs );
}

int SampleBuffer::Read(BYTE *pBlock, int nBytes)
{
	BYTE *pDstBytes = static_cast<BYTE *>( pBlock );
	int nReadSoFar = 0;

	EnterCriticalSection( &m_cs );

	while ( nReadSoFar < nBytes && m_lSamples.size() > 0 )
	{
		INSSBuffer *pSample = m_lSamples.front();
		DWORD dwSampleLen = 0;
		BYTE *pSrcBytes = 0;

		pSample->GetLength( &dwSampleLen );
		pSample->GetBuffer( &pSrcBytes );

		if ( !pSrcBytes )
		{
			pSample->Release();
			m_lSamples.pop_front();
		}
		else
		{
			dwSampleLen -= m_dwSampleOffset;

			if ( dwSampleLen <= static_cast<DWORD>( nBytes - nReadSoFar ) )
			{
				// requested block is larger than or same size as current sample,
				// copy current sample into buffer and then dispose of it

				memcpy( pDstBytes, pSrcBytes + m_dwSampleOffset, dwSampleLen );

				pDstBytes += dwSampleLen;
				nReadSoFar += dwSampleLen;

				m_lSamples.pop_front();
				pSample->Release();

				m_dwSampleOffset = 0;
			}
			else
			{
				// requested block is smaller than current sample,
				// keep current sample around after copy

				memcpy( pDstBytes, pSrcBytes + m_dwSampleOffset, nBytes - nReadSoFar );
				m_dwSampleOffset += nBytes - nReadSoFar;
				nReadSoFar = nBytes;
			}
		}
	}

	LeaveCriticalSection( &m_cs );

	return nReadSoFar;
}

void SampleBuffer::Write(INSSBuffer *pSample)
{
	pSample->AddRef();

	EnterCriticalSection( &m_cs );
	m_lSamples.push_back( pSample );
	LeaveCriticalSection( &m_cs );
}

void SampleBuffer::Clear()
{
	EnterCriticalSection( &m_cs );

	while ( m_lSamples.size() > 0 )
	{
		INSSBuffer *pSample = m_lSamples.front();

		if ( pSample )
			pSample->Release();

		m_lSamples.pop_front();
	}

	m_dwSampleOffset = 0;

	LeaveCriticalSection( &m_cs );
}

/***** WMSDK Stuff ******/

#include <wmsdk.h>
#include <malloc.h>

#define TEXTURE_LOCK_FLAGS		0

WMTexture::WMTexture()
	: m_cRef(1),
	  m_pTexture(NULL),
	  m_pReader(NULL),
	  m_logger(Logger::getInstance(L"Instance")),
	  m_bEndOfStream(false),
	  m_iAudioIndex(-1),
	  m_hEvent(NULL),
	  m_pAudioSamples(NULL),
	  m_nAudioSamplesProcessed(0)
{
	m_hEvent = CreateEvent( NULL, FALSE, FALSE, NULL );

	ZeroMemory( m_pfnStreamEvents, sizeof( m_pfnStreamEvents ) );

	m_pfnStreamEvents[WMT_OPENED]			 = &WMTexture::OnStreamOpened;
	m_pfnStreamEvents[WMT_CLOSED]			 = &WMTexture::OnStreamClosed;
	m_pfnStreamEvents[WMT_STARTED]			 = &WMTexture::OnStreamStarted;
	m_pfnStreamEvents[WMT_STOPPED]			 = &WMTexture::OnStreamStopped;
	m_pfnStreamEvents[WMT_SOURCE_SWITCH]	 = &WMTexture::OnStreamSourceSwitched;
	m_pfnStreamEvents[WMT_END_OF_FILE]		 = &WMTexture::OnStreamEndOfFile;
	m_pfnStreamEvents[WMT_END_OF_STREAMING]	 = &WMTexture::OnStreamEndOfStream;
	m_pfnStreamEvents[WMT_BUFFERING_START]	 = &WMTexture::OnStreamStartedBuffering;
	m_pfnStreamEvents[WMT_BUFFERING_STOP]    = &WMTexture::OnStreamStoppedBuffering;

	memset( &m_sWaveFormat, 0, sizeof( m_sWaveFormat ) );

	m_pAudioSamples = new SampleBuffer();
}

WMTexture::~WMTexture()
{
	StopReader();

	CloseHandle( m_hEvent );
	m_hEvent = 0;
}

void WMTexture::StopReader()
{
	if ( m_pReader )
	{
		HRESULT hr = m_pReader->Stop();
		if ( SUCCEEDED( hr ) )
		{
			WaitForEvent();
			if ( SUCCEEDED( m_hrAsync ) )
			{
				hr = m_pReader->Close();
				WaitForEvent();
			}
		}
	}
}

HRESULT WMTexture::DoOpen(const std::string& url)
{
	HRESULT hr = E_FAIL;

	// Convert the url to wchar array: get the required buffer length
	int numChars = MultiByteToWideChar(
		CP_ACP,
		0,
		url.c_str(),
		-1,
		NULL,
		0);

	if ( numChars > 0 )
	{
		// MultiByteToWideChar() returns a length that includes null terminator
		WCHAR *wszUrl = new WCHAR[numChars];

		if ( wszUrl )
		{
			// Fill the buffer with wchars
			MultiByteToWideChar(
				CP_ACP,
				0,
				url.c_str(),
				-1,
				wszUrl,
				numChars);

			// Open the reader object
			LogInfo("Opening URL " << url );
			hr = m_pReader->Open( wszUrl, this, NULL );

			delete[] wszUrl;
		}
	}

	return hr;
}

HRESULT WMTexture::SetupAudioStream()
{
	HRESULT hr = E_FAIL;

	m_iAudioOutputNum = FindOutputIndexByTypeID( WMMEDIATYPE_Audio );

	if ( m_iAudioOutputNum != -1 )
	{
		WAVEFORMATEX wfx;
		ZeroMemory( &wfx, sizeof( wfx ) );

		// Only these 3 fields really matter
		// since we're only using this structure
		// to select a stream format

		// First try to find a stereo format
		wfx.nChannels		= AUDIO_NUM_CHANNELS;
		wfx.nSamplesPerSec	= AUDIO_SAMPLE_RATE;
		wfx.wBitsPerSample	= AUDIO_BITS_PER_SAMPLE;

		hr = EnumOutputFormats(
			m_iAudioOutputNum,
			&WMTexture::ChooseAudioFormat,
			(void *) &wfx);

		if ( FAILED( hr ) )
		{
			// No stereo format found, try for a mono format
			wfx.nChannels = 1;

			hr = EnumOutputFormats(
				m_iAudioOutputNum,
				&WMTexture::ChooseAudioFormat,
				(void *) &wfx);

			if ( FAILED( hr ) )
			{
				LogError("Failed to find a suitable audio output format, stream will play with no audio" );
				m_iAudioOutputNum = -1;
			}
		}

		if ( SUCCEEDED( hr ) )
		{
			// Succeeded in finding a wave format, store the format for later
			memcpy( &m_sWaveFormat, &wfx, sizeof( WAVEFORMATEX ) );
		}
	}

	return hr;
}

HRESULT WMTexture::SetupVideoStream(LPDIRECT3DDEVICE9 pDevice)
{
	HRESULT hr = E_FAIL;

	m_iVideoOutputNum = FindOutputIndexByTypeID( WMMEDIATYPE_Video );

	if ( m_iVideoOutputNum != -1)
	{
		hr = EnumOutputFormats(
			m_iVideoOutputNum,
			&WMTexture::ChooseVideoFormat,
			(void *) &WMMEDIASUBTYPE_RGB32 );

		if ( SUCCEEDED( hr ) )
		{
			hr = InitDisplaySurface( pDevice );

			ClearTexture( 0x00000000 );
		}
		else
		{
			LogError("No acceptable video formats found, available formats are:" );
			
			int i = 0;
			EnumOutputFormats(
				m_iVideoOutputNum,
				reinterpret_cast<EnumOutputFormatProc>(&WMTexture::LogVideoFormat),
				&i);
		}
	}

	return hr;
}

typedef struct
{
	GUID guid;
	const char *desc;
} GuidToString;

static const GuidToString SUBTYPE_GUID_DESCRIPTIONS[] =
{
	{ WMMEDIASUBTYPE_RGB1,       "WMMEDIASUBTYPE_RGB1"		 },
	{ WMMEDIASUBTYPE_RGB4,       "WMMEDIASUBTYPE_RGB4"		 },
	{ WMMEDIASUBTYPE_RGB8,       "WMMEDIASUBTYPE_RGB8"		 },
	{ WMMEDIASUBTYPE_RGB565,     "WMMEDIASUBTYPE_RGB565"	 },
	{ WMMEDIASUBTYPE_RGB555,     "WMMEDIASUBTYPE_RGB555"	 },
	{ WMMEDIASUBTYPE_RGB24,      "WMMEDIASUBTYPE_RGB24"		 },
	{ WMMEDIASUBTYPE_RGB32,      "WMMEDIASUBTYPE_RGB32"		 },
	{ WMMEDIASUBTYPE_I420,       "WMMEDIASUBTYPE_I420"		 },
	{ WMMEDIASUBTYPE_IYUV,       "WMMEDIASUBTYPE_IYUV"		 },
	{ WMMEDIASUBTYPE_YV12,       "WMMEDIASUBTYPE_YV12"		 },
	{ WMMEDIASUBTYPE_YUY2,       "WMMEDIASUBTYPE_YUY2"		 },
	{ WMMEDIASUBTYPE_UYVY,       "WMMEDIASUBTYPE_UYVY"		 },
	{ WMMEDIASUBTYPE_YVYU,       "WMMEDIASUBTYPE_YVYU"		 },
	{ WMMEDIASUBTYPE_YVU9,       "WMMEDIASUBTYPE_YVU9"		 },
	{ WMMEDIASUBTYPE_PCM,        "WMMEDIASUBTYPE_PCM"		 },
	{ WMMEDIASUBTYPE_DRM,        "WMMEDIASUBTYPE_DRM"		 },
	{ WMSCRIPTTYPE_TwoStrings,   "WMSCRIPTTYPE_TwoStrings"	 },
	{ WMMEDIASUBTYPE_WebStream,  "WMMEDIASUBTYPE_WebStream"	 },
	{ WMMEDIASUBTYPE_VIDEOIMAGE, "WMMEDIASUBTYPE_VIDEOIMAGE" }
};

const char *SubtypeGUIDToString(const GUID *pGUID)
{
	for (int i = 0; i < sizeof( SUBTYPE_GUID_DESCRIPTIONS ) / sizeof( SUBTYPE_GUID_DESCRIPTIONS[0] ); i++)
		if ( memcmp( pGUID, &SUBTYPE_GUID_DESCRIPTIONS[i].guid, sizeof( GUID ) ) == 0 )
			return SUBTYPE_GUID_DESCRIPTIONS[i].desc;

	return "<<< unknown >>>";
}

HRESULT WMTexture::LogVideoFormat(const WM_MEDIA_TYPE *pMediaType, int *pIndex)
{
	LogInfo("[" << (*pIndex)++ << "] subtype=" << SubtypeGUIDToString( &pMediaType->subtype ) );
	return E_FAIL;
}

HRESULT WMTexture::Initialize(const std::string& url, LPDIRECT3DDEVICE9 pDevice)
{
	HRESULT hr;

	m_pDevice = pDevice;

	// Create the windows media reader object
	hr = WMCreateReader( NULL, WMT_RIGHT_PLAYBACK, &m_pReader );

	if ( m_pReader )
	{
		hr = DoOpen( url );
		if ( SUCCEEDED( hr ) )
		{
			m_hrAsync = E_FAIL; // reset status

			// Wait for the WMT_OPENED notification to come through
			bool opened = WaitForEvent( OPEN_TIMEOUT );

			if ( opened && SUCCEEDED( m_hrAsync ) )
			{
				HRESULT hrVideo, hrAudio;

				// Setup the 2 streams
				hrAudio = SetupAudioStream();
				hrVideo = SetupVideoStream( pDevice );

				// Start the reader if there is either a video stream or an audio stream present
				if ( SUCCEEDED( hrAudio ) || SUCCEEDED( hrVideo) )
				{
					hr = m_pReader->Start( 0, 0, 1.0f, NULL );
					// Don't wait for the WMT_STARTED event here, as it
					// happens after buffering is complete, and so can take a
					// noticeable amount of time to arrive.
				}
			}
			else
			{
				// Timed out on IWMReader::Open(), bail out
				hr = m_pReader->Close();
			}
		}
	}

	return hr;
}

LPDIRECT3DTEXTURE9 WMTexture::Texture()
{
	return m_pTexture;
}

bool WMTexture::CheckStatus()
{
	return m_bEndOfStream;
}

const char* WMTexture::LastErrorMsg() const
{
	return NULL;
}

void WMTexture::ClearTexture(DWORD dwColor)
{
	D3DLOCKED_RECT lockRect = { 0, 0 };
	HRESULT hr;

	if( NULL != m_pTexture )
	{
		hr = m_pTexture->LockRect( 0, &lockRect, NULL, TEXTURE_LOCK_FLAGS );

		if ( SUCCEEDED( hr ) && lockRect.pBits )
		{
			for (int i = 0; i < m_uVideoHeight; i++)
			{
				BYTE *pBits = static_cast<BYTE *>( lockRect.pBits );
				DWORD *pDstPtr = reinterpret_cast<DWORD *>( pBits + ( m_uVideoHeight - i - 1 ) * lockRect.Pitch );

				for (int j = 0; j < m_uVideoWidth; j++)
					pDstPtr[j] = dwColor;
			}

			m_pTexture->UnlockRect( 0 );
		}
	}
}

// pSampleData is in 32 bit XRGB format
void WMTexture::DoVideoSample(const BYTE *pSampleData)
{
	if ( m_pTexture )
	{
		D3DLOCKED_RECT lockRect = { 0, 0 };
		HRESULT hr;

		hr = m_pTexture->LockRect( 0, &lockRect, NULL, TEXTURE_LOCK_FLAGS );

		if ( SUCCEEDED( hr ) && lockRect.pBits )
		{
			const BYTE *pSrcPtr = pSampleData;
			int copyLen = 4 * m_uVideoWidth;

			// Copy the source image to dest image line by line.
			// Flip it vertically in the process.
			for (int i = 0; i < m_uVideoHeight; i++)
			{
				BYTE *pDstPtr = static_cast<BYTE *>( lockRect.pBits ) + ( m_uVideoHeight - i - 1 ) * lockRect.Pitch;

				memcpy( pDstPtr, pSrcPtr, copyLen);
				pSrcPtr += copyLen;
			}

			m_pTexture->UnlockRect( 0 );
		}
	}
}

void WMTexture::DoBlock_STEREO(void *pBlock, int nBlockSize)
{
	BYTE *pBytes = static_cast<BYTE *>( pBlock );

	int nRead = m_pAudioSamples->Read( pBytes, nBlockSize );

	USHORT *pSamples = reinterpret_cast<USHORT *>( pBytes );
	DWORD nSamplesRead = nRead / sizeof(USHORT);
	m_usLastAudioSample = pSamples[nSamplesRead - 1];

	// extend the last sample out when a buffer underrun
	// occurs, this prevents the annoying clicking sound
	if ( nRead < nBlockSize )
	{
		for (DWORD i = nSamplesRead; i < nBlockSize / sizeof(USHORT); i++)
			pSamples[i] = m_usLastAudioSample;
	}
}

void WMTexture::DoBlock_MONO(void *pBlock, int nBlockSize)
{
	BYTE *pBytes = static_cast<BYTE *>( pBlock );

	m_pAudioSamples->Read( pBytes, nBlockSize );

	// don't support sample extension for 8-bit buffer for now
	m_usLastAudioSample = 0;
}

// Called by the sound code when it needs a block
// of data for a sample buffer.
void WMTexture::DoBlock(void *pBlock, int nBlockSize)
{
	if ( m_pAudioSamples )
	{
		if ( m_sWaveFormat.nChannels == 2 )
			DoBlock_STEREO( pBlock, nBlockSize );
		else
			DoBlock_MONO( pBlock, nBlockSize );
	}
}

HRESULT CALLBACK WMTexture::OnSample(
	DWORD dwOutputNum,
	QWORD /*cnsSampleTime*/,
	QWORD /*cnsSampleDuration*/,
	DWORD /*dwFlags*/,
	INSSBuffer *pSample,
	void * /*pvContext*/)
{
	BYTE *pBufferContents = 0;

	/*HRESULT hr =*/ pSample->GetBuffer( &pBufferContents );

	if ( pBufferContents )
	{
		if ( dwOutputNum == (DWORD) m_iVideoOutputNum )
		{
			DoVideoSample( pBufferContents );
		}
		else if ( dwOutputNum == (DWORD) m_iAudioOutputNum )
		{
			if ( m_pAudioSamples )
				m_pAudioSamples->Write( pSample );

			// Start playback if this is the first audio sample to arrive
			if ( m_nAudioSamplesProcessed == 0 )
				m_iAudioIndex = Audio::GetInstance()->PlayStream( this, m_sWaveFormat );

			++m_nAudioSamplesProcessed;
		}
	}

	return S_OK;
}

void WMTexture::StartAudio()
{
	m_nAudioSamplesProcessed = 0;
}

void WMTexture::StopAudio()
{
	// Stop the audio stream playback
	if ( m_iAudioIndex != -1 )
	{
		Audio::GetInstance()->CancelStream( m_iAudioIndex );
		m_iAudioIndex = -1;
	}

	if ( m_pAudioSamples )
		m_pAudioSamples->Clear();
}

void WMTexture::LogStats()
{
	IWMReaderAdvanced *pReaderAdv = NULL;

	HRESULT hr = m_pReader->QueryInterface( IID_IWMReaderAdvanced, reinterpret_cast<void **>( &pReaderAdv ) );

	if ( pReaderAdv )
	{
		WM_READER_STATISTICS stats;
		ZeroMemory( &stats, sizeof( stats ) );

		stats.cbSize = sizeof( stats );

		hr = pReaderAdv->GetStatistics( &stats );

		if ( SUCCEEDED( hr ) )
		{
			LogInfo("Connection Stats = { Bw: " << stats.dwBandwidth / 1000 << "Kbits, "
				"#PktRecv: " << stats.cPacketsReceived << ", "
				"#PktRecover: " << stats.cPacketsRecovered << ", "
				"#PktLost: " << stats.cPacketsLost << ", "
				"Quality: " << stats.wQuality << "% }" );
		}

		pReaderAdv->Release();
	}
}

void WMTexture::OnStreamOpened(HRESULT hr)
{
	if ( SUCCEEDED( hr ) )
	{
		// Print out the play mode

		IWMReaderAdvanced2 *pReaderAdv2 = NULL;

		m_pReader->QueryInterface( IID_IWMReaderAdvanced2, reinterpret_cast<void **>( &pReaderAdv2 ) );

		if ( pReaderAdv2 )
		{
			WMT_PLAY_MODE playMode;

			if ( SUCCEEDED( pReaderAdv2->GetPlayMode( &playMode ) ) )
			{
				const char *playModeStr[] =
				{
					"WMT_PLAY_MODE_AUTOSELECT",
					"WMT_PLAY_MODE_LOCAL",
					"WMT_PLAY_MODE_DOWNLOAD",
					"WMT_PLAY_MODE_STREAMING"
				};

				if ( playMode >= 0 && playMode <= 3 )
					LogInfo("WMT_PLAY_MODE = " << playModeStr[playMode] );
			}

			pReaderAdv2->Release();
		}

		m_bEndOfStream = false;
	}

	m_hrAsync = hr;
	SetEvent( m_hEvent );
}

void WMTexture::OnStreamClosed(HRESULT hr)
{
	m_hrAsync = hr;
	SetEvent( m_hEvent );
}

void WMTexture::OnStreamStarted(HRESULT hr)
{
	// WMT_STARTED
	//   Called when playback begins.  
	if ( SUCCEEDED( hr ) )
		StartAudio();
}

void WMTexture::OnStreamStopped(HRESULT hr)
{
	// WMT_STOPPED
	//   Called when playback stops.  Doesn't necessarily signify
	//   end-of-file or end-of-stream.
	StopAudio();
	m_hrAsync = hr;
	SetEvent( m_hEvent );
}

void WMTexture::OnStreamSourceSwitched(HRESULT hr)
{
	// WMT_SOURCE_SWITCH
	//   Not totally clear on this event, but it looks like
	//   when it comes in the outputs can change, so you need
	//   to enum the outputs and set everything up again.
	//   The video setup is coded to avoid recreating the texture
	//   if the dimensions have not changed.  If the dimensions
	//   do change, then this code won't work, because WMTexture::Texture()
	//   needs to be called again.

	if ( SUCCEEDED( hr ) )
	{
		StopAudio();
		SetupAudioStream();

		SetupVideoStream( m_pDevice );

		ClearTexture( 0x00000000 );

		hr = m_pReader->Start( 0, 0, 1.0f, NULL );

		LOG4CPLUS_INFO( m_logger, "Switched sources: hr = " << (void *) hr );
	}
}

void WMTexture::OnStreamEndOfFile(HRESULT hr)
{
	// WMT_EOF
	//   Sent after a playlist entry ends.

	StopAudio();

	if ( hr == S_OK )
	{
		// We've reached the end of a server playlist

		if ( m_pTexture )
		{
			m_pTexture->Release();
			m_pTexture = 0;
		}

		m_bEndOfStream = true;		
	}
}

void WMTexture::OnStreamEndOfStream(HRESULT /*hr*/)
{
	// Don't do jack in response to this event.
	// It can come at seemingly arbitrary times,
	// like before WMT_STARTED is even sent.

	// WMT_END_OF_STREAMING
	//   The stream is finished, shut everything down.
}

void WMTexture::OnStreamStartedBuffering(HRESULT /*hr*/)
{
	m_dwBufferingStartTime = fTime::TimeMs();
}

void WMTexture::OnStreamStoppedBuffering(HRESULT /*hr*/)
{
	TimeMs dwBufferingTime = fTime::ElapsedMs(m_dwBufferingStartTime);
	LogInfo("Buffering completed after " << FMT_TIME << dwBufferingTime << " ms" );
	LogStats();
}

HRESULT CALLBACK WMTexture::OnStatus(WMT_STATUS status, HRESULT hr, WMT_ATTR_DATATYPE /*dwType*/, BYTE * /*pValue*/, void * /*pvContext*/)
{
	if ( status >= 0 && status <= sizeof( m_pfnStreamEvents ) / sizeof( m_pfnStreamEvents[0] ) )
	{
		StreamEventHandler handler = m_pfnStreamEvents[status];
		if ( handler )
			(this->*handler)( hr );
		return S_OK;
	}
	else
	{
		return E_FAIL;
	}
}

bool WMTexture::WaitForEvent(DWORD dwTimeout)
{
	DWORD dwWaitResult = WaitForSingleObject( m_hEvent, dwTimeout );
	if ( dwWaitResult == WAIT_OBJECT_0 )
		return true;
	else if ( dwWaitResult == WAIT_TIMEOUT )
		return false;
	else
		return false;
}

int WMTexture::FindOutputIndexByTypeID(const GUID &guidType)
{
	DWORD pcOutputs = 0;
	HRESULT hr;

	hr = m_pReader->GetOutputCount( &pcOutputs );

	for (DWORD i = 0; i < pcOutputs; i++)
	{
		IWMOutputMediaProps *props = 0;

		hr = m_pReader->GetOutputProps( i, &props );

		if ( props )
		{
			GUID guid;

			hr = props->GetType( &guid );

			if ( SUCCEEDED( hr ) && memcmp( &guid, &guidType, sizeof( guid ) ) == 0 )
				return i; // Found a matching output
		}
	}

	// No output found matching guidType
	return -1;
}

HRESULT WMTexture::ChooseAudioFormat(
	const WM_MEDIA_TYPE *pMediaType,
	void *pContext)
{
	const WAVEFORMATEX *pDesired = static_cast<const WAVEFORMATEX *>( pContext );
	const WAVEFORMATEX *pWaveFormat = reinterpret_cast<const WAVEFORMATEX *>( pMediaType->pbFormat );

	if ( pWaveFormat->nChannels == pDesired->nChannels &&
		 pWaveFormat->nSamplesPerSec == pDesired->nSamplesPerSec &&
		 pWaveFormat->wBitsPerSample == pDesired->wBitsPerSample )
	{
		return S_OK;
	}

	return E_FAIL;
}

HRESULT WMTexture::ChooseVideoFormat(
	const WM_MEDIA_TYPE *pMediaType,
	void *pContext)
{
	const GUID *pFormatGUID = static_cast<const GUID *>( pContext );

	if ( memcmp( &pMediaType->subtype, pFormatGUID, sizeof( GUID ) ) == 0 )
	{
		if ( memcmp( &pMediaType->formattype, &WMFORMAT_VideoInfo, sizeof( GUID ) ) == 0 &&
			 pMediaType->cbFormat >= sizeof( WMVIDEOINFOHEADER ) )
		{
			memcpy( &m_sVideoInfoHdr, pMediaType->pbFormat, sizeof( WMVIDEOINFOHEADER ) );				

			// This format is acceptable
			return S_OK;
		}
	}

	// This format does not meet our requirements
	return E_FAIL;
}

HRESULT WMTexture::EnumOutputFormats(DWORD dwOutput, EnumOutputFormatProc pCallback, void *pContext)
{
	DWORD dwFormatCount = 0;
	HRESULT hr;

	if( S_OK != (hr = m_pReader->GetOutputFormatCount( dwOutput, &dwFormatCount )) )
	{
		LogError("IWMReader::GetOutputFormatCount(..) returned bad result: " << hr << "." );
		return E_FAIL;
	}

	for (DWORD dwFormat = 0; dwFormat < dwFormatCount; dwFormat++)
	{
		IWMOutputMediaProps *pFormatProps = 0;

		// Get the output format object
		if( S_OK != (hr = m_pReader->GetOutputFormat( dwOutput, dwFormat, &pFormatProps )) )
		{
			LogWarn("IWMReader::GetOutputFormat(..) dwFormat = " << dwFormat << " returned bad result: " << hr << "." );
			continue;
		}

		if ( pFormatProps )
		{
			DWORD pcbType = 0;

			// Find out how much to allocate for the media type structure
			hr = pFormatProps->GetMediaType( NULL, &pcbType );

			if( S_OK == hr && pcbType > 0 )
			{
				// Allocate the media type structure on the stack
				WM_MEDIA_TYPE *pMediaType = static_cast<WM_MEDIA_TYPE *>( _alloca( pcbType ) );

				if ( pMediaType )
				{
					// Fill in the media type structure
					hr = pFormatProps->GetMediaType( pMediaType, &pcbType );

					if( SUCCEEDED( hr ) )
					{
						// Call the callback function
						hr = (this->*pCallback)( pMediaType, pContext );

						if ( SUCCEEDED( hr ) )
						{
							// Callback selected this format, set it to output
							if ( SUCCEEDED( m_pReader->SetOutputProps( dwOutput, pFormatProps ) ) )
								return hr;
						}
					}
				}
			}
		}
	}

	return E_FAIL;
}

HRESULT WMTexture::InitDisplaySurface(LPDIRECT3DDEVICE9 pDevice)
{
	HRESULT hr = S_OK;

	if ( !m_pTexture ||
	      m_uVideoWidth != m_sVideoInfoHdr.bmiHeader.biWidth ||
	      m_uVideoHeight != m_sVideoInfoHdr.bmiHeader.biHeight )
	{
		// release the old texture, if any
		if ( m_pTexture )
		{
			m_pTexture->Release();
			m_pTexture = 0;
		}

		m_uVideoWidth  = m_sVideoInfoHdr.bmiHeader.biWidth;
		m_uVideoHeight = m_sVideoInfoHdr.bmiHeader.biHeight;

		hr = D3DXCreateTexture(
			pDevice,
			m_uVideoWidth,
			m_uVideoHeight,
			1,
			0,
			D3DFMT_X8R8G8B8,
			D3DPOOL_MANAGED,
			&m_pTexture);
		if( FAILED( hr ) )
		{
			LogError("D3DxCreateTexture() FAILED - " << hr );
		}
	}

	return hr;
}

//------------------------------------------------------------------------------
// Implementation of IUnknown methods
//------------------------------------------------------------------------------
HRESULT STDMETHODCALLTYPE WMTexture::QueryInterface( /* [in] */ REFIID riid,
                                                   /* [out] */ void __RPC_FAR *__RPC_FAR *ppvObject ) 
{
    if( ( IID_IWMReaderCallback == riid ) ||
        ( IID_IUnknown == riid ) )
    {
        *ppvObject = static_cast< IWMReaderCallback* >( this );
        AddRef();
        return( S_OK );
    }

    *ppvObject = NULL;
    return( E_NOINTERFACE );
}

ULONG STDMETHODCALLTYPE WMTexture::AddRef()
{
    return( InterlockedIncrement( &m_cRef ) );
}

ULONG STDMETHODCALLTYPE WMTexture::Release()
{
    if( 0 == InterlockedDecrement( &m_cRef ) )
    {
        delete this;
        return( 0 );
    }

    return( m_cRef );
}

const char *WMTexture::WMT_STATUS_STRINGS[NUM_STATUS_EVENTS] =
{
	"WMT_ERROR",
	"WMT_OPENED",
	"WMT_BUFFERING_START",
	"WMT_BUFFERING_STOP",
	"WMT_EOF",
	"WMT_END_OF_SEGMENT",
	"WMT_END_OF_STREAMING",
	"WMT_LOCATING",
	"WMT_CONNECTING",
	"WMT_NO_RIGHTS",
	"WMT_MISSING_CODEC",
	"WMT_STARTED",
	"WMT_STOPPED",
	"WMT_CLOSED",
	"WMT_STRIDING",
	"WMT_TIMER",
	"WMT_INDEX_PROGRESS",
	"WMT_SAVEAS_START",
	"WMT_SAVEAS_STOP",
	"WMT_NEW_SOURCEFLAGS",
	"WMT_NEW_METADATA",
	"WMT_BACKUPRESTORE_BEGIN",
	"WMT_SOURCE_SWITCH",
	"WMT_ACQUIRE_LICENSE",
	"WMT_INDIVIDUALIZE",
	"WMT_NEEDS_INDIVIDUALIZATION",
	"WMT_NO_RIGHTS_EX",
	"WMT_BACKUPRESTORE_END",
	"WMT_BACKUPRESTORE_CONNECTING",
	"WMT_BACKUPRESTORE_DISCONNECTING",
	"WMT_ERROR_WITHURL",
	"WMT_RESTRICTED_LICENSE",
	"WMT_CLIENT_CONNECT",
	"WMT_CLIENT_DISCONNECT",
	"WMT_NATIVE_OUTPUT_PROPS_CHANGED",
	"WMT_RECONNECT_START",
	"WMT_RECONNECT_END",
	"WMT_CLIENT_CONNECT_EX",
	"WMT_CLIENT_DISCONNECT_EX",
	"WMT_SET_FEC_SPAN",
	"WMT_PREROLL_READY",
	"WMT_PREROLL_COMPLETE",
	"WMT_CLIENT_PROPERTIES",
	"WMT_LICENSEURL_SIGNATURE_STATE"
};
