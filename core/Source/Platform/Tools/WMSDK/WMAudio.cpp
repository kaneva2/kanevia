///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#include "WMAudio.h"

#include "IClientEngine.h" // for HWND

using namespace log4cplus;

Audio *Audio::m_pAudio = NULL;

Audio::Audio()
	: m_pDSound(NULL),
	  m_logger(Logger::getInstance(L"Audio"))
{
	auto pICE = KEP::IClientEngine::Instance();
	if (!pICE)
		return;

	HRESULT hr;

	InitializeCriticalSection( &m_cs );

	EnterCriticalSection( &m_cs );

	hr = DirectSoundCreate8( &DSDEVID_DefaultPlayback, &m_pDSound, NULL );

	// this check will fail if they don't
	// have a sound card or valid sound driver
	if (m_pDSound)
	{
		hr = m_pDSound->SetCooperativeLevel(
			pICE->GetWindowHandle(),
			DSSCL_PRIORITY );

		m_hAudioEvents[0] = CreateEvent( NULL, TRUE, FALSE, NULL );

		WAVEFORMATEX wfx;
		ZeroMemory( &wfx, sizeof( wfx ) );

		wfx.cbSize			= sizeof( wfx );
		wfx.wFormatTag		= WAVE_FORMAT_PCM;
		wfx.nChannels		= AUDIO_NUM_CHANNELS;
		wfx.nSamplesPerSec	= AUDIO_SAMPLE_RATE;
		wfx.wBitsPerSample	= AUDIO_BITS_PER_SAMPLE;
		wfx.nBlockAlign		= wfx.nChannels * wfx.wBitsPerSample / 8;
		wfx.nAvgBytesPerSec	= wfx.nSamplesPerSec * wfx.nBlockAlign;

		for (int i = 0; i < AUDIO_NUM_STREAMS; i++)
		{
			m_pStreams[i] = NULL;

			for (int j = 0; j < NUM_BUFFER_SEGMENTS; j++)
				m_hAudioEvents[1 + j + NUM_BUFFER_SEGMENTS * i] = CreateEvent( NULL, TRUE, FALSE, NULL );

			m_pBuffers[i] = CreateSoundBuffer( &wfx, &m_hAudioEvents[1 + NUM_BUFFER_SEGMENTS * i] );
		}

		memset( m_pFormat, 0, sizeof( m_pFormat ) );

		m_hServiceThread = CreateThread(
			NULL,
			0,
			(LPTHREAD_START_ROUTINE) BufferServiceRoutine,
			this,
			0,
			NULL);
	}

	LeaveCriticalSection( &m_cs );
}

int Audio::PlayStream(AudioStream *pStream, const WAVEFORMATEX &format)
{
	int stream = -1;

	if ( m_pDSound && pStream )
	{
		EnterCriticalSection( &m_cs );

		stream = GetFreeStreamIndex();

		if ( stream != -1 )
		{
			LPDIRECTSOUNDBUFFER8 buffer = m_pBuffers[stream];
			DWORD nBlockSize0 = 0, nBlockSize1 = 0;
			BYTE *pBlock0 = NULL, *pBlock1 = NULL;

			HRESULT hr = buffer->Lock(
				0,
				NUM_BUFFER_SEGMENTS * AUDIO_BLOCK_SIZE,
				(void **) &pBlock0,
				&nBlockSize0,
				(void **) &pBlock1,
				&nBlockSize1,
				DSBLOCK_ENTIREBUFFER );

			if ( SUCCEEDED( hr ) )
			{
				memcpy( &m_pFormat[stream], &format, sizeof( WAVEFORMATEX ) );

				memset( pBlock0, 0, NUM_BUFFER_SEGMENTS * AUDIO_BLOCK_SIZE );

				for (int i = 0; i < NUM_BUFFER_SEGMENTS; i++)
					pStream->DoBlock( pBlock0 + i * AUDIO_BLOCK_SIZE, AUDIO_BLOCK_SIZE );

				hr = buffer->Unlock( pBlock0, nBlockSize0, pBlock1, nBlockSize1 );

				hr = buffer->SetCurrentPosition( 0 );
				hr = buffer->Play( 0, 0, DSBPLAY_LOOPING );

				if ( SUCCEEDED( hr ) )
					m_pStreams[stream] = pStream;
			}
		}

		LeaveCriticalSection( &m_cs );
	}

	return stream;
}

void Audio::CancelStream(int index)
{
	if ( m_pDSound && index >= 0 && index < AUDIO_NUM_STREAMS )
	{
		EnterCriticalSection( &m_cs );

		LPDIRECTSOUNDBUFFER8 buffer = m_pBuffers[index];
		
		if ( buffer )
			buffer->Stop();

		m_pStreams[index] = NULL;

		// Reset the buffer events to make sure that no
		// last-minute notifications come through
		for (int i = 0; i < NUM_BUFFER_SEGMENTS; i++)
			ResetEvent( m_hAudioEvents[1 + i + NUM_BUFFER_SEGMENTS * index] );

		LeaveCriticalSection( &m_cs );
	}
}

Audio *Audio::GetInstance()
{
	if ( m_pAudio == NULL )
		m_pAudio = new Audio();

	return m_pAudio;
}

HRESULT Audio::SetupBufferNotifications(LPDIRECTSOUNDBUFFER8 pSoundBuffer8, HANDLE *pEvents)
{
	LPDIRECTSOUNDNOTIFY8 pNotify = 0;
	HRESULT hr = E_FAIL;

	hr = pSoundBuffer8->QueryInterface( IID_IDirectSoundNotify8, (void **) &pNotify );

	if ( pNotify )
	{
		DSBPOSITIONNOTIFY notifications[NUM_BUFFER_SEGMENTS];

		for (int i = 0; i < NUM_BUFFER_SEGMENTS; i++)
		{
			notifications[i].dwOffset		= ( i + 1 ) * AUDIO_BLOCK_SIZE - 1;
			notifications[i].hEventNotify	= pEvents[i];
		}

		hr = pNotify->SetNotificationPositions( NUM_BUFFER_SEGMENTS, notifications );

		pNotify->Release();
	}

	return hr;
}

// pEvents is a pointer to NUM_BUFFER_SEGMENTS event handles for this sound buffer's notifications
LPDIRECTSOUNDBUFFER8 Audio::CreateSoundBuffer(const WAVEFORMATEX *pWaveFormat, HANDLE *pEvents)
{
	LPDIRECTSOUNDBUFFER pSoundBuffer = 0;
	LPDIRECTSOUNDBUFFER8 pSoundBuffer8 = 0;
	HRESULT hr;

	DSBUFFERDESC bd;
	ZeroMemory( &bd, sizeof( bd ) );

	bd.dwSize			= sizeof( bd );
	bd.dwFlags			= DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLPOSITIONNOTIFY | DSBCAPS_LOCSOFTWARE | DSBCAPS_GLOBALFOCUS;
	bd.dwBufferBytes	= NUM_BUFFER_SEGMENTS * AUDIO_BLOCK_SIZE;
	bd.lpwfxFormat		= (LPWAVEFORMATEX) pWaveFormat;
	bd.guid3DAlgorithm	= DS3DALG_DEFAULT;

	hr = m_pDSound->CreateSoundBuffer( &bd, &pSoundBuffer, NULL );

	if ( pSoundBuffer )
	{
		hr = pSoundBuffer->QueryInterface( IID_IDirectSoundBuffer8, (void **) &pSoundBuffer8 );

		// Now that we have a pointer to a version 8 sound buffer,
		// release the old interface pointer
		pSoundBuffer->Release();
		pSoundBuffer = NULL;

		if ( pSoundBuffer8 )
		{
			hr = SetupBufferNotifications( pSoundBuffer8, pEvents );

			if ( SUCCEEDED( hr ) )
				return pSoundBuffer8;

			// Failed setting up notifications, bail out
			pSoundBuffer8->Release();
			pSoundBuffer8 = NULL;
		}
	}

	return NULL;
}

void Audio::ConvertMono16ToStereo16(USHORT *pDst, const USHORT *pSrc, DWORD nSize)
{
	int nSamples = nSize / sizeof(USHORT);
	//int offset = nSamples / 2;
	int offset = 0;

	for (int i = 0; i < nSamples; i++)
	{
		pDst[2 * i] = pSrc[offset + i];
		pDst[2 * i + 1] = pSrc[offset + i];
	}
}

void Audio::DoSample(int stream, int sample)
{
	// Lock the buffer region and copy AUDIO_BLOCK_SIZE
	// bytes worth of data from the source

	if ( m_pBuffers[stream] && m_pStreams[stream] )
	{
		DWORD nSampleBytes0 = 0, nSampleBytes1 = 0;
		BYTE *pSample0 = NULL, *pSample1 = NULL;

		// pSample1 and nSampleBytes1 should always be 0
		// since we're not doing any wrapping locks.
		HRESULT hr = m_pBuffers[stream]->Lock(
			sample * AUDIO_BLOCK_SIZE,
			AUDIO_BLOCK_SIZE,
			(void **) &pSample0,
			&nSampleBytes0,
			(void **) &pSample1,
			&nSampleBytes1,
			0);

		if ( SUCCEEDED( hr ) )
		{
			const WAVEFORMATEX &wfx = m_pFormat[stream];

			if ( wfx.nChannels == 2 )
			{
				// stereo
				m_pStreams[stream]->DoBlock( pSample0, nSampleBytes0 );
			}
			else if ( wfx.nChannels == 1 )
			{
				USHORT tmp[AUDIO_BLOCK_SIZE / 2];

				memset( tmp, 0, AUDIO_BLOCK_SIZE / 2 );
				m_pStreams[stream]->DoBlock( tmp, AUDIO_BLOCK_SIZE / 2 );

				ConvertMono16ToStereo16( reinterpret_cast<USHORT *>(pSample0), tmp, AUDIO_BLOCK_SIZE / 2 );
			}

			hr = m_pBuffers[stream]->Unlock( pSample0, nSampleBytes0, pSample1, nSampleBytes1 );
		}
	}
}

DWORD WINAPI Audio::BufferServiceRoutine(Audio *pAudio)
{
//	SetThreadPriority( GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL );

	for ( ; ; )
	{
		// Wait for the next sample to be ready
		DWORD dwNumEvents = 1 + NUM_BUFFER_SEGMENTS * AUDIO_NUM_STREAMS;
		DWORD dwWaitResult = WaitForMultipleObjects( dwNumEvents, pAudio->m_hAudioEvents, FALSE, INFINITE );

		if ( dwWaitResult == WAIT_OBJECT_0 )
		{
			// Stop notification received, exit loop
			LOG4CPLUS_INFO( pAudio->m_logger, "Audio::BufferServiceRoutine(): received stop notification, exiting" );
			return 0;
		}
		else if ( dwWaitResult >= WAIT_OBJECT_0 + 1 && dwWaitResult <= WAIT_OBJECT_0 + NUM_BUFFER_SEGMENTS * AUDIO_NUM_STREAMS )
		{
			// Determine which buffer and sample just became ready.
			int eventIndex = dwWaitResult - WAIT_OBJECT_0;
			int stream = ( eventIndex - 1 ) / NUM_BUFFER_SEGMENTS;
			int sample = ( eventIndex - 1 ) % NUM_BUFFER_SEGMENTS;
			
			EnterCriticalSection( &pAudio->m_cs );
			pAudio->DoSample( stream, sample );
			LeaveCriticalSection( &pAudio->m_cs );

			ResetEvent( pAudio->m_hAudioEvents[eventIndex] );
		}
		else
		{
			// Unrecognized wait result, exit loop
			LOG4CPLUS_INFO(
				pAudio->m_logger,
				"Audio::BufferServiceRoutine(): dwWaitResult = " << dwWaitResult << ", not recognized, exiting" );

			return (DWORD) -1;
		}
	}
}

int Audio::GetFreeStreamIndex() const
{
	for (int i = 0; i < AUDIO_NUM_STREAMS; i++)
		if ( m_pStreams[i] == NULL )
			return i;

	return -1;
}
