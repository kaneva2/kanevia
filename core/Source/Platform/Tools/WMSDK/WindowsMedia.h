///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPCommon.h"

#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <wmsdk.h>

#include "WMAudio.h"
#include "Core/Util/HighPrecisionTime.h"
#include <list>

class SampleBuffer
{
	public:

		SampleBuffer();
		virtual ~SampleBuffer();

		int Read(BYTE *pBlock, int nBytes);
		void Write(INSSBuffer *pBuffer);
		void Clear();

	private:

		CRITICAL_SECTION m_cs;
		std::list<INSSBuffer *> m_lSamples;
		DWORD m_dwSampleOffset;
};


#define NUM_STATUS_EVENTS 44	// number of enumerants in WMT_STATUS, taken from SDK docs

class WMTexture : public IWMReaderCallback, public AudioStream
{
	public:

		WMTexture();
		virtual ~WMTexture();

		HRESULT Initialize( const std::string& url, LPDIRECT3DDEVICE9 device );

		LPDIRECT3DTEXTURE9 Texture();

		bool CheckStatus();

		const char* LastErrorMsg() const;

		void DoBlock(void *pBlock, int nSize);

		// IWMReaderCallback
		HRESULT CALLBACK OnSample(DWORD dwOutputNum, QWORD cnsSampleTime, QWORD cnsSampleDuration, DWORD dwFlags, INSSBuffer *pSample, void *pvContext);

		// IWMStatusCallback
		HRESULT CALLBACK OnStatus(WMT_STATUS status, HRESULT hr, WMT_ATTR_DATATYPE dwType, BYTE *pValue, void *pvContext);

		//
		//Methods of IUnknown
		//
		HRESULT STDMETHODCALLTYPE QueryInterface( /* [in] */ REFIID riid,
												  /* [out] */ void __RPC_FAR *__RPC_FAR *ppvObject ); 
		ULONG STDMETHODCALLTYPE AddRef();
		ULONG STDMETHODCALLTYPE Release();

	protected:

		HRESULT DoOpen(const std::string& url);
		
		void StopReader();

		void StartAudio();
		void StopAudio();

		bool WaitForEvent(DWORD dwTimeout = INFINITE);

		// Retrieves the index of the first output that
		// has a majortype = guidType, or -1 if none found
		int FindOutputIndexByTypeID(const GUID &guidType);

		// The enum proc gets called back with the media type pointer and the caller-supplied context info
		typedef HRESULT (WMTexture::*EnumOutputFormatProc)(const WM_MEDIA_TYPE *pMediaType, void *pContext);

		HRESULT EnumOutputFormats( DWORD dwOutput, EnumOutputFormatProc pCallback, void *pContext );

		HRESULT SetupAudioStream();
		HRESULT ChooseAudioFormat(const WM_MEDIA_TYPE *pMediaType, void *pContext);
		HRESULT InitSoundBuffer();

		HRESULT SetupVideoStream(LPDIRECT3DDEVICE9 pDevice);
		HRESULT ChooseVideoFormat(const WM_MEDIA_TYPE *pMediaType, void *pContext);
		HRESULT InitDisplaySurface( LPDIRECT3DDEVICE9 pDevice );
		HRESULT LogVideoFormat(const WM_MEDIA_TYPE *pMediaType, int *pIndex);

		void ClearTexture(DWORD dwColor);
		void DoVideoSample(const BYTE *pSampleData);
		void DoAudioSample(const BYTE *pSampleData, DWORD dwLen);

		void DoBlock_STEREO(void *pBlock, int nSize);
		void DoBlock_MONO(void *pBlock, int nSize);

		void LogStats();
		
		// Stream Event Handlers
		void OnStreamOpened(HRESULT hr);
		void OnStreamClosed(HRESULT hr);
		void OnStreamStarted(HRESULT hr);
		void OnStreamStopped(HRESULT hr);
		void OnStreamSourceSwitched(HRESULT hr);
		void OnStreamEndOfFile(HRESULT hr);
		void OnStreamEndOfStream(HRESULT hr);
		void OnStreamStartedBuffering(HRESULT hr);
		void OnStreamStoppedBuffering(HRESULT hr);

	private:

		int m_iVideoOutputNum, m_iAudioOutputNum;
		
		int m_nAudioSamplesProcessed;

		int m_uVideoWidth, m_uVideoHeight;
		LPDIRECT3DTEXTURE9 m_pTexture;
		LPDIRECT3DDEVICE9 m_pDevice;

		IWMReader *m_pReader;
		SampleBuffer *m_pAudioSamples;

		HANDLE m_hEvent;

		HRESULT m_hrAsync;

		bool m_bEndOfStream;
		
		TimeMs m_dwBufferingStartTime;
		
		// This is to prevent clicking on buffer underruns
		USHORT m_usLastAudioSample;

		// this is the stream ID that we got back when we
		// asked the audio system to play our sound
		// (not to be confused with the output number
		// returned from IWMReader)
		int m_iAudioIndex;

		WMVIDEOINFOHEADER m_sVideoInfoHdr;
		WAVEFORMATEX m_sWaveFormat;

		log4cplus::Logger m_logger;

		// IUnknown cruft
		LONG m_cRef;

		typedef void (WMTexture::*StreamEventHandler)(HRESULT hr);
		StreamEventHandler m_pfnStreamEvents[NUM_STATUS_EVENTS];

		static const char *WMT_STATUS_STRINGS[NUM_STATUS_EVENTS];

		// Wait for the IWMReader::Open() call for at most 5 seconds
		// before failing and returning
		static const DWORD OPEN_TIMEOUT = 5000;
};
