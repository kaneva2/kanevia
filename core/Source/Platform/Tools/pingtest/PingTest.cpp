// pingtest.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <pdh.h>
#include <log4cplus/logger.h>
#include <log4cplus/ndc.h>
#include <log4cplus/configurator.h>
using namespace log4cplus;

#include "server/KEPServer/KEPServerEnums.h"

#include "tools/network/pinger.h"
#include "common/include/Alert.h"
#include "common/keputil/CommandLine.h"
#include "common/keputil/SocketSubsystem.h"
using namespace KEP;

Logger alert_logger( Logger::getInstance( "Alert" ) );
extern HINSTANCE StringResource;

class MemoryPoller {
public:
	MemoryPoller() : _hquery(0), _hcounter(0) {
		PDH_STATUS status = PdhOpenQuery(NULL, 0, &_hquery);

		if (status != ERROR_SUCCESS)        return;

		status = PdhAddCounter(	_hquery
								, _T("\\Process(pingtest)\\Private Bytes")
								, 0
								, &_hcounter);

		if (status == ERROR_SUCCESS)    return;
		PdhCloseQuery(_hquery);
		_hquery = 0;
	}

	virtual ~MemoryPoller() {
		if ( _hquery == 0 ) return;
		PdhCloseQuery(_hquery);
		_hquery = 0;
	}

	unsigned long poll( ) {
		if ( _hquery == 0 ) return 0;
		if ( _hcounter == 0 ) return 0;
		PDH_STATUS status = PdhCollectQueryData(_hquery);
		if (status != ERROR_SUCCESS)    return 0;
		PDH_RAW_COUNTER value;
		DWORD dwType;
		status = PdhGetRawCounterValue(_hcounter, &dwType, &value);
		if (status != ERROR_SUCCESS)    return 0;
//		printf("%lld %lld \n", value.TimeStamp, value.FirstValue);
		LOG4CPLUS_INFO( Logger::getInstance( "STARTUP" ), "METRIC:PRIVATEBYTES=" << value.FirstValue );
		return 0;
	}
private:
	PDH_HQUERY	_hquery;
	PDH_HCOUNTER _hcounter;

 };

// get the logging setup.
// hopefully, this will let me know if we're finding the message resource
// successfully
//
int _tmain(int argc, _TCHAR* argv[])
{
	::CoInitialize( 0 );

	StringResource = GetModuleHandle(0);

	SocketSubSystem ss;
	char achost[512];
	gethostname(achost,512);

	CommandLine cl;
	CommandLine::StringParameter& logConfig			= cl.addString( "--logcfg"
																	, "%(AppName)log.cfg"
																	, "Log configuration file" );
	CommandLine::StringParameter& basedir			= cl.addString(	"--basedir"
																	, "."
																	, "Base directory" );
	CommandLine::StringParameter& instanceName		= cl.addString( "--instancename"
																	, achost
																	, "Instance Name" );
	CommandLine::StringParameter& hostName			= cl.addString( "--hostname"
																	, achost
																	, "Override your host name."	);
	CommandLine::StringParameter& registrationKey	= cl.addString( "--regkey"
																	, "Server registration key"	);

															
//	cout << logConfig.value() << endl;					
//	string hs = cl.helpString();
//	cout << hs << endl;
	cl.parse( argc, argv );


	ConfigureAndWatchThread *m_configureThread;
	m_configureThread = new ConfigureAndWatchThread(logConfig, 10000);
	::Sleep(10);


	LOG4CPLUS_INFO( Logger::getInstance("STARTUP"), "***************** BEGIN RUN ***************" );
	LOG4CPLUS_INFO( Logger::getInstance("STARTUP"), "***************** BEGIN RUN ***************" );	
	LOG4CPLUS_INFO( Logger::getInstance("STARTUP"), "***************** BEGIN RUN ***************" );
	unsigned long maxplayers = 1000;

  
	PINGER_HANDLE h = MakePinger(	((std::string)instanceName).c_str()
									, ((std::string)basedir).c_str()
									, "Pinger"
									, ((std::string)registrationKey).c_str()
//									, "36U5300GmzZ7DbfYaPqFSpA"
									, ((std::string)hostName).c_str()
									, 25857
									, 32806
									, 130
									, "4.0.9.1421"
									, 33554432
									, &maxplayers
									, false
									, 1
									, 10 );

	int n = 0;
	MemoryPoller mp;
	while ( true ) {
		UpdatePinger(	h
						, ssRunning
						, n++
						, false );
		mp.poll();
		Sleep(90000);	

//		GetPingerGameId( h );
//		GetPingerParentGameId( h );
//		char acName[512];
//		GetPingerGameName( h, acName, 512 );
//		GetPingerServerId( h );
	}

	return 0;
}

