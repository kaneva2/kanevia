/******************************************************************************
 StateMachine.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma warning (disable : 4786)

#include "wkgtypes.h"
#include "State.h"
#include "Transition.h"
#include "statemachine.h"
#include "xmldocument.h"
#include "filestream.h"
#include "xmlnode.h"
#include "xmlnodeattributes.h"
#include "Trigger.h"
#include "StateFactory.h"

namespace wkg
{

namespace state_machine
{

StateMachine::StateMachine() : 
	m_curr_state(0),
	m_name("")
{
}

StateMachine::~StateMachine()
{
	m_curr_state = 0;

	for(int32 i = 0; i < m_states.size(); i++)
	{
		delete m_states[i];
	}

	m_states.clear();
}

bool
StateMachine::Load(const char *filename)
{
	XMLDocument *xml_doc = XMLDocument::CreateFromFile(filename);
	if(xml_doc)
	{
		return Load(xml_doc);
	}
	else
	{
		ASSERT(0);
		return false;
	}
}

bool
StateMachine::Load(XMLDocument *doc)
{
	XMLNode *curr = doc->GetRootNode();

	if(curr)
	{
		curr = curr->GetFirstChild()->GetFirstChild();
		m_name = curr->GetAttributes()->FindAttributeValue("name");

		GetStates(curr->GetFirstChild());
		AssignPointers();
		m_curr_state = *(m_states.begin());

		return true;
	}

	return false;
}

State*
StateMachine::CreateState(std::string newName)
{
	return new State(newName, this);
}

State*
StateMachine::GetState()
{
	return m_curr_state;
}

void
StateMachine::AddState(State *newState)
{
	m_states.push_back(newState);
}


bool
StateMachine::SetState(std::string stateName)
{
	State *new_state = FindState(stateName);

	if(new_state)
	{
		m_curr_state = new_state;
		return true;
	}

	return false;
}

void
StateMachine::SendTrigger(Trigger *trig)
{
	int32 num_trans = m_curr_state->GetNumTransitions();

	for(int i = 0; i < num_trans; ++i)
	{
		Transition *trans = m_curr_state->GetTransition(i);
		std::string trans_name = trans->GetMsgName();
		if(trans_name == trig->GetName())
		{
			State *old_state = m_curr_state;

			m_curr_state->OnExit();
			m_curr_state = trans->GetTargetState();
			ASSERT(m_curr_state);
			m_curr_state->OnEnter();

			// wait until the state transition is complete before triggering
			//  the finish triggers
			old_state->TriggerFinishTriggers();

			return;
		}
	}
}

void
StateMachine::OnUpdate(uint32 ms)
{
	m_curr_state->OnUpdate(ms);
}

State *
StateMachine::FindState(std::string stateName)
{
	SMIterator itor = m_states.begin();
	SMIterator iend = m_states.end();
	for(; itor != iend; ++itor)
	{
		if(stateName == (*itor)->GetName())
		{
			return *itor;
		}
	}

	return 0;
}

void
StateMachine::GetStates(XMLNode *curNode)
{
//	LoadState(curNode);
//#if 0
	while(curNode)
	{
		// Create state, attach its transitions, and add it to state vector
		State *newState = StateFactory::Create(curNode, this);

		m_map[newState->GetName()] = newState;
		CreateTransitions(newState, curNode->GetFirstChild());
		AddState(newState);

		curNode = curNode->GetNextSibling();
	}
//#endif
}

void
StateMachine::CreateTransitions(State* theState, XMLNode* curNode)
{
	while(curNode)
	{
		std::string targetName = curNode->GetAttributes()->FindAttributeValue("target");
		std::string msgName = curNode->GetAttributes()->FindAttributeValue("msg");
		Transition* trans = new Transition(msgName, targetName);
		theState->AddTransition(trans);

		curNode = curNode->GetNextSibling();
	}
}

void
StateMachine::AssignPointers()
{
	SMIterator itor = m_states.begin();
	SMIterator iend = m_states.end();

	std::string targetName = "";
	for(; itor != iend; ++itor)
	{
		for(int32 i = 0; i < (*itor)->GetNumTransitions(); i++)
		{
			targetName = (*itor)->GetTransition(i)->GetTargetName();
			(*itor)->GetTransition(i)->SetTargetState(m_map[targetName]);
			ASSERT(m_map[targetName]);
		}
	}
}


}

}