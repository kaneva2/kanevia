///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>
#include <map>

#include "compileOpts.h"
#include "precompiled.h"

#include "wkgdebug.h"

#ifdef SINGLEPRECISION
typedef float			real;
#else
typedef double			real;
#endif

typedef unsigned __int64	uint64;
typedef unsigned long		uint32;
typedef unsigned short		uint16;
typedef __int64				int64;
typedef signed long			int32;
typedef signed short		int16;
typedef unsigned char		uint8;
typedef signed char			int8;

#include "wkgconst.h"
#include "wkgfunc.h"

namespace wkg {

#ifdef USE_SERIALIZATION
class Stream;
#	define SERIALIZABLE				\
		bool Load(Stream*);			\
		bool Save(Stream*) const;
#else
#	define SERIALIZABLE
#endif

}

#define WKG_ZENABLE D3DZB_TRUE
