///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace wkg {

class VertexShader {
public:
	VertexShader();
	virtual ~VertexShader();

	virtual bool Init(const char *vsoFilename, D3DVERTEXELEMENT9 *e = 0);
	virtual bool Init(uint32 fvf);

	virtual bool CreateShader();
	virtual bool LoadFunction(const char *vsoFilename);

	virtual bool CreateDeclaration(D3DVERTEXELEMENT9 *e);

	virtual void LoadConstants() {};

	virtual bool Activate();

protected:
	uint32 *m_function;

	bool m_is_fvf;
	uint32 m_fvf;

	IDirect3DVertexShader9 *m_handle;
	IDirect3DVertexDeclaration9 *m_declaration;

};

}


