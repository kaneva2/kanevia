///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include "factoryrelease.h"

#ifdef _DEBUG
#   include "typeinfo.h"
#endif

namespace wkg {

template <typename T, typename D> class Factory : public FactoryRelease<T> {
public:
	Factory();
	virtual ~Factory();

	T *Get(const T *data);
	T *Get(const D &desc);

	virtual void Release(const T *data);
	void Release(const D &desc);

protected:
	class t_node {
	public:
		T       *data;
		D       descriptor;
	};

	typedef std::vector<t_node *> NodeVector;
	typedef typename NodeVector::iterator NodeIterator;

	T *Get(NodeIterator itor);
	void Release(NodeIterator itor);

	NodeIterator FindNode(const T *data) const;
	NodeIterator FindNode(const D &desc) const;

	virtual T *Create(const D &desc) const = 0;
	bool AddNode(const T *data, const D &desc);

protected:
	mutable NodeVector m_nodes;

};

template <typename T, typename D>
Factory<T, D>::Factory() {
	// our node vector is already initialized with no elements in it
}

template <typename T, typename D>
Factory<T, D>::~Factory() {
	// iterate over our list and print a debug statement for each resource
	//  leak.  free those leaked.
	NodeIterator itor;
	for (itor = m_nodes.begin(); itor < m_nodes.end(); itor++) {
#ifdef _DEBUG
		const type_info &t = typeid(T);

		DEBUGMSG("WARNING: class Factory<%s>: resource leak of %d bytes at %x.\n",
			t.name(), sizeof((*itor)->data) + sizeof(*itor), itor);
#endif
		delete (*itor)->data;
		delete (*itor);
	}
}

template <typename T, typename D>
T *Factory<T, D>::Get(typename Factory<T, D>::NodeIterator itor) {
	if (itor != m_nodes.end()) {
		(*itor)->data->AddRef();
		return (*itor)->data;
	} else {
		return (T *) 0;
	}
}

template <typename T, typename D>
void Factory<T, D>::Release(typename Factory<T, D>::NodeIterator itor) {
	// the actual deletion of the object is the resposibility of the
	//  user or the object
	if (itor != m_nodes.end()) {
		delete *itor;
		m_nodes.erase(itor);
	} else {
#ifdef _DEBUG
		const type_info &t = typeid(T);

		DEBUGMSG("class Factory<%s>: Attempt to release object already released!", t.name());
#endif
	}
}

template <typename T, typename D>
T *Factory<T, D>::Get(const T *data) {
	ASSERT(data);
	T *obj = Get(FindNode(data));
	return obj;
}

template <typename T, typename D>
T *Factory<T, D>::Get(const D &desc) {
	T *obj = Get(FindNode(desc));
	if (!obj) {
		obj = Create(desc);
		if (obj) {
			obj->SetFactoryRelease(this);
			if (!AddNode(obj, desc)) {
				obj->Release();
				obj = 0;
			}
		}
	}
	return obj;
}

template <typename T, typename D>
void Factory<T, D>::Release(const T *data) {
	ASSERT(data);
	Release(FindNode(data));
}

template <typename T, typename D>
void Factory<T, D>::Release(const D &desc) {
	Release(FindNode(desc));
}

template <typename T, typename D>
typename Factory<T, D>::NodeIterator Factory<T, D>::FindNode(const T * const data) const {
	NodeIterator itor;
	for (itor = m_nodes.begin(); itor < m_nodes.end(); itor++) {
		if ((*itor)->data == data) return itor;
	}

	return itor;
}

template <typename T, typename D>
typename Factory<T, D>::NodeIterator Factory<T, D>::FindNode(const D &desc) const {
	NodeIterator itor;
	for (itor = m_nodes.begin(); itor < m_nodes.end(); itor++) {
		if (desc == (*itor)->descriptor) return itor;
	}

	return itor;
}

template <typename T, typename D>
bool Factory<T, D>::AddNode(const T *data, const D &desc) {
	t_node *new_node = new t_node;

	ASSERT(new_node && data);
	if (!new_node || !data) {
		return false;
	}

	new_node->data = const_cast<T *>(data);
	new_node->descriptor = desc;

	m_nodes.push_back(new_node);

	return true;
}

}


