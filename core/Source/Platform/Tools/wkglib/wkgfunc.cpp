///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"

#include <math.h>
#include <string.h>
#include <stdlib.h>

namespace wkg {

real
sqrt(real arg) {
	return static_cast<real>(::sqrt(static_cast<double>(arg)));
}

double
sqrt(double arg) {
	return (::sqrt(static_cast<double>(arg)));
}

real
cos(real arg) {
	return static_cast<real>(::cos(static_cast<double>(arg)));
}

real
acos(real arg) {
	return static_cast<real>(::acos(static_cast<double>(arg)));
}

real
sin(real arg) {
	return static_cast<real>(::sin(static_cast<double>(arg)));
}

real
asin(real arg) {
	return static_cast<real>(::asin(static_cast<double>(arg)));
}

real
tan(real arg) {
	return static_cast<real>(::tan(static_cast<double>(arg)));
}

real
atan(real arg) {
	return static_cast<real>(::atan(static_cast<double>(arg)));
}

void
memcpy(void* dest, const void* src, uint32 amt) {
	::memcpy(dest, src, amt);
}

void
memset(void* dest, int32 val, uint32 amt) {
	::memset(dest, val, amt);
}

void
memmove(void* dest, const void* src, uint32 amt) {
	::memmove(dest, src, amt);
}

void
memzero(void* dest, uint32 amt) {
	::memset(dest, 0, amt);
}

int32
memcmp(void* dest, const void* src, uint32 amt) {
	return ::memcmp(dest, src, amt);
}

#ifndef NO_WKG_STRING

int32
strlen(const char* src) {
	return (uint32)::strlen(src);
}

uint32
strcpy_s(char* dest, uint32 bufSize, const char* src) {
	return ::strcpy_s(dest, bufSize, src);
}


int32
strcmp(const char* s1, const char* s2) {
	return ::strcmp(s1, s2);
}

int32
stricmp(const char* s1, const char* s2) {
	return ::_stricmp(s1, s2);
}

#endif

int32
atoi(const char* str) {
	return ::atoi(str);
}

real
atof(const char* str) {
	return static_cast<real>(::atof(str));
}

}
