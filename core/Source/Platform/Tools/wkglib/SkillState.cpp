/******************************************************************************
 SkillState.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "SkillState.h"
#include "Trigger.h"
#include "StateMachine.h"
#include "Skill.h"

namespace wkg
{

using namespace state_machine;

namespace game
{

SkillState::SkillState(std::string newName, StateMachine* parent, Actor *owner) : 
	State(newName, parent),
	Skill(newName, owner)
{
}

SkillState::~SkillState()
{
}

void
SkillState::OnEnter()
{
	SetCountdown(GetDuration());
}

void
SkillState::OnExit()
{
}

void
SkillState::OnUpdate(uint32 ms)
{
	State::OnUpdate(ms);
}

}

}