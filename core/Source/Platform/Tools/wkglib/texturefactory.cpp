///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include "texture.h"
#include "texturefactory.h"

namespace wkg {

TextureFactory::TextureFactory() :
	Singleton <TextureFactory>(),
	Factory<Texture, std::string>(),
	createCube(false) {}

TextureFactory::~TextureFactory() {}

Texture*
TextureFactory::Create(const std::string& filename) const {
	Texture* result = new Texture();

	if (result) {
		bool ok;

		if (createCube)
			ok = result->LoadCubeMap(filename.c_str());
		else {
			ok = result->Load(filename.c_str());
			if (!ok) {
				// now see if this texture exists in just the default directory
				char fn[255];
				strcpy_s(fn, _countof(fn), "textures");
				const char *res = strrchr(filename.c_str(), '\\');
				if (!res)
					res = strrchr(filename.c_str(), '/');

				if (res) {
					strcat_s(fn, _countof(fn), res);
					ok = result->Load(fn);
				}
			}
		}

		if (!ok) {
			result->Release();
			result = 0;
		}
	}

	createCube = false;
	return result;
}

Texture*
TextureFactory::GetCubeTexture(const std::string& filename) {
	TextureFactory::NodeIterator iter = FindNode(filename);

	if (iter == m_nodes.end()) {
		createCube = true;

		return Get(filename);
	} else
		return Get(iter);
}

}
