///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include "refcounter.h"
#include <atlcomcli.h>

namespace KEP {
class ITexture;
}

namespace wkg {

class Texture : public RefCounter<Texture> {
public:
	Texture();
	virtual ~Texture();

	bool Load(const char* filename);
	bool LoadCubeMap(const char* filename);

	bool LoadD3DExternal(const char* displayName, uint32 textureKey);
	void SetExternalD3DTexture(IDirect3DBaseTexture9* tex) {
		m_tex = tex;
	}
	void SetExternalTexture(KEP::ITexture* pTexture) {
		m_pTexture = pTexture;
	}
	KEP::ITexture* GetExternalTexture() const { return m_pTexture; }

	uint32 GetExternalKey() const {
		return m_externalKey;
	}
	bool IsExternalTextureReady() const {
		return m_tex != NULL;
	}

	bool Create(uint32 width, uint32 height,
		D3DFORMAT Fmt = D3DFMT_A8R8G8B8,
		uint32 Usage = 0,
		uint32 Levels = 1,
		D3DPOOL Pool = D3DPOOL_MANAGED);

	IDirect3DTexture9* Interface() const;
	IDirect3DBaseTexture9* BaseInterface() const;
	IDirect3DCubeTexture9* CubeInterface() const;

	std::string GetFilename() const;
	int32 GetSourceImageWidth() const;
	int32 GetSourceImageHeight() const;

protected:
	KEP::ITexture* m_pTexture = nullptr;
	CComPtr<IDirect3DBaseTexture9> m_tex;
	std::string m_filename;
	int32 m_srcWidth;
	int32 m_srcHeight;
	uint32 m_externalKey;

	static int32 m_total_size;
};

inline Texture::Texture()
	: m_tex(0), m_srcWidth(0), m_srcHeight(0), m_externalKey(0) {}

inline Texture::~Texture() {
	if (m_externalKey == 0 && m_tex)
		m_total_size -= m_srcWidth * m_srcHeight * 4;

	m_tex = 0;
}

inline IDirect3DTexture9* Texture::Interface() const {
	return static_cast<IDirect3DTexture9*>(static_cast<IDirect3DBaseTexture9*>(m_tex));
}

inline IDirect3DBaseTexture9* Texture::BaseInterface() const {
	return m_tex;
}

inline IDirect3DCubeTexture9* Texture::CubeInterface() const {
	return static_cast<IDirect3DCubeTexture9*>(static_cast<IDirect3DBaseTexture9*>(m_tex));
}

inline std::string Texture::GetFilename() const {
	return m_filename;
}

inline int32 Texture::GetSourceImageWidth() const {
	return m_srcWidth;
}

inline int32 Texture::GetSourceImageHeight() const {
	return m_srcHeight;
}

}
