/******************************************************************************
 Realtime.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "timer.h"

namespace wkg 
{

Timer::Timer()
{
	m_elapsed = 0;
	m_age = 0;
	m_last_refresh = 0;
	m_paused = false;

	LARGE_INTEGER tmp;
	QueryPerformanceFrequency(&tmp);
	m_timer_frequency = tmp.QuadPart;
}

void 
Timer::Refresh()
{
	if(!m_paused)
	{
		LARGE_INTEGER tmp;
		__int64 this_time;
		QueryPerformanceCounter(&tmp);
		this_time = tmp.QuadPart;

		// will cause a problem if the sys clock wraps to exactly 0 when we call
		if(m_last_refresh == 0)
			m_last_refresh = this_time;

		if(this_time >= m_last_refresh)
		{
			m_elapsed = this_time - m_last_refresh;
			m_elapsed = (m_elapsed * 1000) / m_timer_frequency;
		}

		m_last_refresh = this_time;

		m_age += m_elapsed;
	}
}

void 
Timer::Pause()
{
	m_paused = true;
}


uint32 
Timer::Unpause()
{
	m_paused = false;

	return GetElapsedSincePause();
}

uint32 
Timer::GetElapsedSincePause() const
{
	LARGE_INTEGER tmp;
	__int64 this_time;
	QueryPerformanceCounter(&tmp);
	this_time = tmp.QuadPart;

	if(m_last_refresh == 0)
		return 0;

	uint32 elapsed = (this_time - m_last_refresh);
	elapsed = (1000 * elapsed) / m_timer_frequency;

	return elapsed;
}

}

