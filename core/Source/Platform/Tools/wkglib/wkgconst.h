///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define WKG_PI				(static_cast<real>(3.14159265359))
#define WKG_PIOVER2			(static_cast<real>(1.570796326794895))

#ifdef SINGLEPRECISION
#	define MAXREAL			1.0e38f
#	define TOLERANCEf		0.00001f
#else
#	define MAXREAL			1.0e308
#	define TOLERANCEf		0.000001
#endif
