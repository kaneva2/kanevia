///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Core/Math/Vector.h>

namespace wkg {

class Units {
protected:
	static const real UNITS_TO_FOOT;	// length
	static const real UNITS_TO_GRAM;	// mass
	static const real UNITS_TO_NEWTON;	// force

public:

	static real ToMeters(real Units);
	static real ToFeet(real Units);

	static real FromMeters(real Meters);	// to Units
	static real FromFeet(real Feet);		// to Units

	static real MetersToFeet(real Meters);
	static real FeetToMeters(real Feet);

	static KEP::Vector3f ToMeters(const KEP::Vector3f& Units);
	static KEP::Vector3f ToFeet(const KEP::Vector3f& Units);
	static KEP::Vector3f FromMeters(const KEP::Vector3f& Meters);
	static KEP::Vector3f FromFeet(const KEP::Vector3f& Feet);

	static real ToGrams(real Units);
	static real ToKGrams(real Units);
	static real ToSlugs(real Units);

	static real FromGrams(real Grams);		// to Units
	static real FromKGrams(real Kilograms);	// to Units
	static real FromSlugs(real Slugs);		// to Units

	static real GramsToSlugs(real Grams);
	static real SlugsToGrams(real Slugs);

	static real ToNewtons(real Units);
	static real ToPounds(real Units);

	static real FromNewtons(real Newtons);	// to Units
	static real FromPounds(real Pounds);	// to Units

	static real NewtonsToPounds(real Newtons);
	static real PoundsToNewtons(real Pounds);
};

inline real
Units::FeetToMeters(real Feet) {
	return Feet * 0.3048f;
}

inline real
Units::MetersToFeet(real Meters) {
	return Meters / 0.3048f;
}

inline real
Units::FromFeet(real Feet) {
	return Feet * UNITS_TO_FOOT;
}

inline real
Units::ToFeet(real Units) {
	return Units / UNITS_TO_FOOT;
}

inline real
Units::FromMeters(real Meters) {
	return FromFeet(MetersToFeet(Meters));
}

inline real
Units::ToMeters(real Units) {
	return FeetToMeters(ToFeet(Units));
}

inline KEP::Vector3f
Units::ToMeters(const KEP::Vector3f& Units) {
	return KEP::Vector3f(ToMeters(Units.x), ToMeters(Units.y), ToMeters(Units.z));
}

inline KEP::Vector3f
Units::ToFeet(const KEP::Vector3f& Units) {
	return KEP::Vector3f(ToFeet(Units.x), ToFeet(Units.y), ToFeet(Units.z));
}

inline KEP::Vector3f
Units::FromMeters(const KEP::Vector3f& Meters) {
	return KEP::Vector3f(FromMeters(Meters.x), FromMeters(Meters.y), FromMeters(Meters.z));
}

inline KEP::Vector3f
Units::FromFeet(const KEP::Vector3f& Feet) {
	return KEP::Vector3f(FromFeet(Feet.x), FromFeet(Feet.y), FromFeet(Feet.z));
}

inline real
Units::GramsToSlugs(real Grams) {
	return Grams / 14590.0f;
}

inline real
Units::SlugsToGrams(real Slugs) {
	return Slugs * 14590.0f;
}


inline real
Units::ToGrams(real Units) {
	return Units / UNITS_TO_GRAM;
}

inline real
Units::FromGrams(real Grams) {
	return Grams * UNITS_TO_GRAM;
}

inline real
Units::ToKGrams(real Units) {
	return ToGrams(Units) * 0.001f;
}

inline real
Units::FromKGrams(real Kilograms) {
	return FromGrams(Kilograms * 1000.0f);
}

inline real
Units::ToSlugs(real Units) {
	return GramsToSlugs(ToGrams(Units));
}

inline real
Units::FromSlugs(real Slugs) {
	return FromGrams(SlugsToGrams(Slugs));
}

inline real
Units::NewtonsToPounds(real Newtons) {
	return Newtons * 0.2248f;
}

inline real
Units::PoundsToNewtons(real Pounds) {
	return Pounds * 4.448f;
}

inline real
Units::ToNewtons(real Units) {
	return Units / UNITS_TO_NEWTON;
}

inline real
Units::FromNewtons(real Newtons) {
	return Newtons * UNITS_TO_NEWTON;
}

inline real
Units::ToPounds(real Units) {
	return NewtonsToPounds(ToNewtons(Units));
}

inline real
Units::FromPounds(real Pounds) {
	return FromNewtons(PoundsToNewtons(Pounds));
}

}
