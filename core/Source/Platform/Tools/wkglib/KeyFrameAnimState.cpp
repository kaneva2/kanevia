/******************************************************************************
 KeyFrameAnimState.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "AnimatedMesh.h"
#include "SkeletalAnimation.h"
#include "StateMachine.h"
#include "AnimStateMachine.h"
#include "MessageQueue.h"
#include "Message.h"
#include "WorldObjectList.h"
#include "AudioManager.h"
#include "KeyFrameMessage.h"
#include "KeyFrameAnimState.h"

namespace wkg
{

namespace state_machine
{

KeyFrameAnimState::KeyFrameAnimState(std::string name, StateMachine* parent_sm) : 
	AnimState(name, parent_sm),
	m_currKeyFrame(-1),
	m_lastKeyFrame(-1)
{
}

KeyFrameAnimState::~KeyFrameAnimState()
{
}

void
KeyFrameAnimState::OnEnter()
{
	AnimState::OnEnter();
}

void
KeyFrameAnimState::OnExit()
{
	AnimState::OnExit();
}

void
KeyFrameAnimState::OnUpdate(uint32 ms)
{
	AnimState::OnUpdate(ms);

	AnimStateMachine* animsm = dynamic_cast<AnimStateMachine*>(m_parentSM);
	AnimatedMesh* mesh = animsm->GetAnimatedMesh();
	SkeletalAnimation* skel = mesh->GetAnimation(m_name.c_str());

	m_lastKeyFrame = m_currKeyFrame;
	m_currKeyFrame = skel->GetCurrFrame();
	if(m_currKeyFrame == m_lastKeyFrame)
	{
		return;
	}
	if(m_currKeyFrame < m_lastKeyFrame)
	{
		m_lastKeyFrame = -1;
	}

	int32 frame_idx = GetKeyFrameInRange(m_lastKeyFrame, m_currKeyFrame);
	if(frame_idx >= 0)
	{
		DoKeyFrameAction(frame_idx);
	}
}

int32
KeyFrameAnimState::GetKeyFrameInRange(int32 lo, int32 hi)
{
	for(int i = 0; i < m_keyFrameList.size(); ++i)
	{
		int32 frame = (m_keyFrameList[i])->GetFrame();
		if((frame > lo) && (frame <= hi))
		{
			return i;
			
		}
	}

	return -1;
}

void
KeyFrameAnimState::DoKeyFrameAction(int32 idx)
{
	AnimStateMachine* animsm = dynamic_cast<AnimStateMachine*>(m_parentSM);
	int32 uid = animsm->GetOwnerUID();
	std::string message = (m_keyFrameList[idx])->GetMessage();
//	Message* msg = new Message(message, uid, uid);
//	MessageQueue::Instance()->QueueMessage(msg);
	if("KFM_FIRE" == (m_keyFrameList[idx])->GetMessage())
	{
		Message* msg = new Message((m_keyFrameList[idx])->GetMessage(), uid, uid);
		MessageQueue::Instance()->QueueMessage(msg);
	}
//	if("FOOTSTEP" == (m_keyFrameList[idx])->GetMessage())
//	{
//		engine::WorldObject* wo = engine::WorldObjectList::Instance()->GetByID(animsm->GetOwnerUID());
//		AudioManager::Instance()->Play("FOOTSTEPB", wo);
//		DEBUGMSG("%i", idx);
//	}
}

void
KeyFrameAnimState::AddKeyFrameMessage(std::string msg, int32 frame)
{
	KeyFrameMessage* kfm = new KeyFrameMessage(msg, frame);
	m_keyFrameList.push_back(kfm);
}


}
}

