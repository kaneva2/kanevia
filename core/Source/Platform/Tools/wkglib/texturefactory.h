///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include "singleton.h"
#include "factory.h"

namespace wkg {

class Texture;

class TextureFactory :
	public Singleton <TextureFactory>,
	public Factory<Texture, std::string> {
public:
	TextureFactory();
	virtual ~TextureFactory();

	Texture* GetCubeTexture(const std::string& filename);

protected:
	virtual Texture* Create(const std::string& filename) const;

protected:
	mutable bool createCube;
};

}

