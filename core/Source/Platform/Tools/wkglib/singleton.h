///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "assert.h"

namespace wkg {

template <typename T> class Singleton {
public:
	Singleton();	// initialized
	~Singleton();

	static T *Instance() { /*ASSERT(ms_instance); */return ms_instance;
	}

	static bool IsCreated() {
		return ms_instance != 0;
	}

protected:
	static T *ms_instance;
};

template <typename T> T*
Singleton<T>::ms_instance = (T *) 0;

template <typename T>
Singleton<T>::Singleton() {
	assert(!ms_instance);
	ms_instance = static_cast<T *>(this);
}

template <typename T>
Singleton<T>::~Singleton() {
	assert(ms_instance);
	ms_instance = static_cast<T *>(0);
}

}
