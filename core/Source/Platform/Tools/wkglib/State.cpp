/******************************************************************************
 State.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/


#include "wkgtypes.h"
#include "Transition.h"
#include "Trigger.h"
#include "StateMachine.h"
#include "State.h"

namespace wkg
{

namespace state_machine
{

State::State() :
	m_countdown(0),
	m_parentSM(0)
{

}

State::State(std::string newName, StateMachine* parent) : 
	m_parentSM(parent),
	m_name(newName),
	m_countdown(0)
{
}

State::~State()
{

}

Transition*
State::GetTransition(std::string target_name)
{
	TransitionIterator itor = m_transitions.begin();
	TransitionIterator iend = m_transitions.end();
	for(; itor != iend; ++itor)
	{
		if((*itor)->GetTargetName() == target_name)
		{
			return (*itor);
		}
	}
	return 0;
}

std::string
State::GetName()
{
	return m_name;
}

void
State::SetName(const std::string &name)
{
	m_name = name;
}

void
State::AddTransition(Transition *trans)
{
	m_transitions.push_back(trans);
}

Transition*
State::GetTransition(int32 idx)
{
	return m_transitions[idx];
}

int32
State::GetNumTransitions()
{
	return m_transitions.size();
}

void
State::OnUpdate(uint32 ms)
{
	if(ms > m_countdown)
	{
		Trigger *trig = new Trigger("TIMES_UP");
		m_parentSM->SendTrigger(trig);
		m_countdown = -1;
	}
	else
	{
		m_countdown -= ms;
	}
}

void 
State::TriggerFinishTriggers()
{
	std::vector<t_finish_trigger>::iterator itor = m_finish_triggers.begin();
	while(itor != m_finish_triggers.end())
	{
		m_parentSM->SendTrigger(itor->trigger);
		if(ONE_TIME == itor->type)
		{
			itor = m_finish_triggers.erase(itor);
		}
		else
		{
			itor++;
		}
	}
}

}

}

