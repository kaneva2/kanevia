///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "DynVB.h"

namespace wkg {

class DynIVB : public DynVB {
public:
	DynIVB();
	~DynIVB();

	LPDIRECT3DINDEXBUFFER9 GetIB();

	bool Prepare(LPDIRECT3DVERTEXBUFFER9 pVB, uint8 vertSize, LPDIRECT3DINDEXBUFFER9 pIB, int32 numBatches = 4);

	bool CanAppend(int32 numVerts, uint16 numIndices, uint8** ppVert, uint16** ppIVert, uint16* pINext);		// returns false if no room currently available (i.e. needs BatchReady())
	bool BatchReady(int32* pStartVert, int32* pNumVerts, uint16* pStartIdx, uint16* pNumIdx);

protected:
	LPDIRECT3DINDEXBUFFER9 pIB;
	uint16 istartByte, ibNext;
	uint16* piBatch;
};

inline LPDIRECT3DINDEXBUFFER9
DynIVB::GetIB() {
	return pIB;
}

}
