/******************************************************************************
 CgInterface.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Singleton.h"
#include <Cg/cgD3D.h>

namespace wkg {
namespace engine {

class CG : public cgDirect3D, public Singleton<CG>
{
	public:
		CG();
		~CG();	// note: must release all cg programs and binditerators prior to this call!

		bool Init();

		cgContextContainer* GetContext();

	protected:
		cgContextContainer* context;
};

inline CG::CG()
	: context(0)
{
}

inline cgContextContainer* CG::GetContext()
{
	return context;
}

}}
