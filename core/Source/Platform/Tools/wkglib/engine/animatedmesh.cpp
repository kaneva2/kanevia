/******************************************************************************
 animatedmesh.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"

#include "wkbfile.h"
#include "wkafile.h"
#include "wkgkeyframe.h"
#include "keyframecontroller.h"
#include "positionkeyframecontroller.h"
#include "orientationkeyframecontroller.h"
#include "posorienttomatrix44controller.h"
#include "posmplexcontroller.h"
#include "orientmplexcontroller.h"
#include "orientationkeyframecontroller.h"
#include "positionkeyframecontroller.h"
#include "skeletalanimation.h"
#include "animation.h"
#include "xformnode.h"

#include "animatedmesh.h"

namespace wkg
{

namespace engine
{

AnimatedMesh::AnimatedMesh() : SkinnedMesh()
{
}

AnimatedMesh::~AnimatedMesh()
{
	for(uint32 i = 0; i < m_animations.size(); i++)
	{
		delete m_animations[i];
	}
	m_animations.clear();

	for(i = 0; i < m_pos_mplex.size(); i++)
	{
		delete m_pos_mplex[i];
	}
	m_pos_mplex.clear();

	for(i = 0; i < m_orient_mplex.size(); i++)
	{
		delete m_orient_mplex[i];
	}
	m_orient_mplex.clear();

	for(i = 0; i < m_pomc.size(); i++)
	{
		delete m_pomc[i];
	}
	m_pomc.clear();
}

void 
AnimatedMesh::PlayAnimation(const char *anim_name, int32 mode, real transition_time, bool reset)
{
	// find the animation
	std::map<std::string, int32>::iterator itor = m_animation_map.find(anim_name);
	ASSERT(itor != m_animation_map.end());

	int32 i = m_animation_map[anim_name];

	m_animations[i]->Play(mode, reset);
	for(uint32 j = 0; j < m_pos_mplex.size(); j++)
	{
		m_pos_mplex[j]->BeginTransition(transition_time);
		m_pos_mplex[j]->SetPlaying(i, true);
		m_orient_mplex[j]->BeginTransition(transition_time);
		m_orient_mplex[j]->SetPlaying(i, true);
	}
}

void 
AnimatedMesh::StopAnimation(const char *anim_name, real transition_time)
{
	// find the animation
	std::map<std::string, int32>::iterator itor = m_animation_map.find(anim_name);
	ASSERT(itor != m_animation_map.end());

	int32 i = m_animation_map[anim_name];

	m_animations[i]->Stop();
	for(uint32 j = 0; j < m_pos_mplex.size(); j++)
	{
		m_pos_mplex[j]->BeginTransition(transition_time);
		m_pos_mplex[j]->SetPlaying(i, false);
		m_orient_mplex[j]->BeginTransition(transition_time);
		m_orient_mplex[j]->SetPlaying(i, false);
	}
}

void 
AnimatedMesh::StopAllAnimations()
{
	for(uint32 i = 0; i < m_animations.size(); i++)
	{
		m_animations[i]->Stop();
		for(uint32 j = 0; j < m_pos_mplex.size(); j++)
		{
			m_pos_mplex[j]->SetPlaying(i, false);
			m_orient_mplex[j]->SetPlaying(i, false);
		}
	}
}

bool 
AnimatedMesh::IsUsed(int32 idx)
{
	std::map<std::string, int32>::iterator itor = m_animation_map.begin();
	for(; itor != m_animation_map.end(); itor++)
	{
		if(itor->second == idx) return true;
	}

	return false;
}

SkeletalAnimation *
AnimatedMesh::GetCachedAnimation(const char *filename, int32 &idx)
{
	// iterate through our animations and see if we have one that's 
	//  not currently assigend
	std::vector<SkeletalAnimation *>::iterator itor = m_animations.begin();
	std::vector<SkeletalAnimation *>::iterator iend = m_animations.end();

	for(; itor != iend; itor++)
	{
		if(!stricmp((*itor)->GetName(), filename))
		{
			if(!IsUsed(itor - m_animations.begin()))
			{
				idx = itor - m_animations.begin();
				return (*itor);
			}
		}
	}

	return 0;
}

bool 
AnimatedMesh::AddAnimation(const char *anim_name, const char *filename)
{
	// check to see if we already have this animation
	if(GetAnimation(anim_name)) return true;

	SkeletalAnimation *ska = 0;

	int32 idx = 0;
	ska = GetCachedAnimation(filename, idx);
	if(ska)
	{
		m_animation_map[anim_name] = idx;
		return true;
	}

	ska = new SkeletalAnimation();
	ASSERT(ska);

	ska->Create(filename, filename);
	m_animations.push_back(ska);

	idx = m_animations.size() - 1;
	m_animation_map[anim_name] = idx;

	for(uint32 i = 0; i < uint32(m_num_bones); i++)
	{
		PosOrientToMatrix44Controller *pomc;
		if(m_pomc.size() > i)
		{
//			delete m_pomc[i];
//			m_pomc[i] = new PosOrientToMatrix44Controller();
			pomc = m_pomc[i];
			ASSERT(pomc);
			pomc->SetTarget(&(m_bones[i]->m_matrix));
		}
		else
		{
			pomc = new PosOrientToMatrix44Controller();
			ASSERT(pomc);
			pomc->SetTarget(&(m_bones[i]->m_matrix));
			m_pomc.push_back(pomc);
		}
		pomc->ClearControllables();

		PosMPlexController *pos_mplex;
		if(m_pos_mplex.size() > i)
		{
//			delete m_pos_mplex[i];
//			m_pos_mplex[i] = new PosMPlexController();
			pos_mplex = m_pos_mplex[i];
			ASSERT(pos_mplex);
		}
		else
		{
			pos_mplex = new PosMPlexController();
			ASSERT(pos_mplex);
			m_pos_mplex.push_back(pos_mplex);
		}
		pos_mplex->ClearControllables();

		OrientMPlexController *orient_mplex;
		if(m_orient_mplex.size() > i)
		{
//			delete m_orient_mplex[i];
//			m_orient_mplex[i] = new OrientMPlexController();
			orient_mplex = m_orient_mplex[i];
			ASSERT(orient_mplex);
		}
		else
		{
			orient_mplex = new OrientMPlexController();
			ASSERT(orient_mplex);
			m_orient_mplex.push_back(orient_mplex);
		}
		orient_mplex->ClearControllables();

		Animation *anim = ska->GetAnimationByName(m_bones[i]->GetName());
		if(anim)
		{
			int32 idx = orient_mplex->AddEntry(ska->GetBoneWeight(i));
			anim->GetOrientationkeyframeController()->ClearControllables();
			anim->GetOrientationkeyframeController()->
				SetTarget(orient_mplex->GetEntry(idx));
			anim->GetOrientationkeyframeController()->
				AddControllable(orient_mplex);

			idx = pos_mplex->AddEntry(ska->GetBoneWeight(i));
			anim->GetPositionKeyframeController()->ClearControllables();
			anim->GetPositionKeyframeController()->
				SetTarget(pos_mplex->GetEntry(idx));
			anim->GetPositionKeyframeController()->
				AddControllable(pos_mplex);

			orient_mplex->SetTarget(&(pomc->m_orientation));
			pos_mplex->SetTarget(&(pomc->m_position));

			pos_mplex->AddControllable(pomc);
			orient_mplex->AddControllable(pomc);

		}
		else
		{
			DEBUGMSG("WARNING: can't find bone %s to apply animation %s",
				m_bones[i]->GetName(), anim_name);
		}
	}

	return true;
}

void
AnimatedMesh::ClearAnimations()
{
	StopAllAnimations();
	m_animation_map.clear();
}


SkeletalAnimation *
AnimatedMesh::GetAnimation(const char *anim_name)
{
	// find the animation
	std::map<std::string, int32>::iterator itor = m_animation_map.find(anim_name);
	if(itor != m_animation_map.end())
	{
		return m_animations[itor->second];
	}

	return 0;
}

}

}

