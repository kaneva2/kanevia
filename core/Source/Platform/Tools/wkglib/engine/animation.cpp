/******************************************************************************
 animation.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "keyframecontroller.h"
#include "positionkeyframecontroller.h"
#include "orientationkeyframecontroller.h"
#include "skeletalanimation.h"
#include "animnotify.h"
#include "xformnode.h"

#include "animation.h"

#define FPS (30)
#define MSPF (1000 / FPS)

namespace wkg
{

namespace engine
{

Animation::Animation() :
	m_kfcp(0),
	m_kfco(0),
	m_pkfc(0),
	m_okfc(0),
	m_mode(PLAY_NONE)
{
}

Animation::~Animation()
{
	delete m_kfcp;
	m_kfcp = 0;

	delete m_kfco;
	m_kfco = 0;

	delete m_pkfc;
	m_pkfc = 0;

	delete m_okfc;
	m_okfc = 0;
}

void 
Animation::Create(int32 frame_start, int32 frame_end)
{
	m_pkfc = new PositionKeyframeController();
	m_okfc = new OrientationKeyframeController();

	m_kfcp = new KeyframeController(
		frame_start, frame_end, 
		&(m_pkfc->m_frame),
		&(m_pkfc->m_next_frame),
		&(m_pkfc->m_partial_frame),
		MSPF);

	m_kfcp->AddControllable(m_pkfc);

	m_kfco = new KeyframeController(
		frame_start, frame_end, 
		&(m_okfc->m_frame),
		&(m_okfc->m_next_frame),
		&(m_okfc->m_partial_frame),
		MSPF);

	m_kfco->AddControllable(m_okfc);
}

void
Animation::AddPositionKeyframe(int32 frame, Vector3 &position)
{
	WKGPositionKeyframe *pkf = new WKGPositionKeyframe();
	pkf->m_frame = frame;
	pkf->m_position = position;
	m_pkfc->AddPositionKeyframe(pkf);
}

void
Animation::AddOrientationKeyframe(int32 frame, Quaternion &orientation)
{
	WKGOrientationKeyframe *okf = new WKGOrientationKeyframe();
	okf->m_frame = frame;
	okf->m_orientation = orientation;
	m_okfc->AddOrientationKeyframe(okf);
}

void 
Animation::Start(int32 mode, bool reset)
{
	m_mode = mode;

	if(reset)
	{
		m_kfcp->Reset();
		m_kfco->Reset();
	}

	switch(mode)
	{
	case PLAY_LOOPING:
		m_kfcp->SetLooping(true);
		m_kfco->SetLooping(true);
		break;

	case PLAY_ONCE:
		m_kfcp->SetLooping(false);
		m_kfcp->AddKeyframeNotify(this);

		m_kfco->SetLooping(false);
		m_kfco->AddKeyframeNotify(this);
		break;

	default:
		ASSERT(0);
		break;
	}

	m_kfcp->StartUpdates();
	m_kfco->StartUpdates();
}

void 
Animation::Stop()
{
	m_kfcp->StopUpdates();
	m_kfco->StopUpdates();

	// just to be sure
	m_kfcp->RemoveKeyframeNotify(this);
	m_kfco->RemoveKeyframeNotify(this);

	m_mode = PLAY_NONE;
}


bool 
Animation::IsPlaying()
{
	// if either the position or orientation are running, then we're running
	return m_kfcp->IsRunning() || m_kfco->IsRunning();
}

void
Animation::AnimDone(KeyframeController *which)
{
	// the controller automatically stops if we're not looping, and the only time
	//  this would be called is if we're not looping, so no need to stop anything
	//  here

	// also remember we get two calls here: one for position, one for animation

	// since this anim is done playing, we're not interested in any further updates
	if(!IsPlaying())
	{
		which->RemoveKeyframeNotify(this);

		std::vector<AnimNotify *>::iterator itor = m_an.begin();
		std::vector<AnimNotify *>::iterator iend = m_an.end();
		for(; itor < iend; itor++)
		{
			(*itor)->AnimDone(this);
		}

		m_mode = PLAY_NONE;
	}
}

void 
Animation::AddAnimNotify(AnimNotify *an)
{
//#ifdef _DEBUG
	std::vector<AnimNotify *>::iterator itor = m_an.begin();
	std::vector<AnimNotify *>::iterator iend = m_an.end();
	for(; itor < iend; itor++)
	{
		if(an == *itor)
		{
//			ASSERT(0);
			return;
		}
	}
//#endif

	m_an.push_back(an);
}

void 
Animation::RemoveAnimNotify(AnimNotify *an)
{
	std::vector<AnimNotify *>::iterator itor = m_an.begin();
	std::vector<AnimNotify *>::iterator iend = m_an.end();
	for(; itor < iend; itor++)
	{
		if(an == *itor)
		{
			m_an.erase(itor);
			return;
		}
	}
}

int32 
Animation::GetCurrFrame()
{
	return m_pkfc->m_frame;
}

void 
Animation::BeginTransition(real transition_time)
{
	m_pkfc->BeginTransition();
	m_okfc->BeginTransition();

	m_kfcp->BeginTransition(transition_time);
	m_kfco->BeginTransition(transition_time);
}

}

}

