/******************************************************************************
 DecalManager.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "decalmanager.h"
#include "device.h"
#include "texture.h"
#include "texturefactory.h"
#include "colortexvertex.h"
#include "renderstate.h"
#include "vsfactory.h"
#include "psfactory.h"
#include "vertexshader.h"
#include "pixelshader.h"
#include "matrix44.h"
#include "vector3.h"
#include "worldobject.h"
#include "quaternion.h"
#include "DynIVB.h"

namespace wkg
{

namespace engine 
{

// default decal lifetime of 5 minutes - long enough for 
//  someone to get bored while standing there waiting for 
//  them to disappear
DecalManager::DecalManager()
:	m_VB(NULL),
	m_IB(NULL),
	m_maxLife(60*1000*5)	
{
}


DecalManager::~DecalManager()
{
	ClearDecals();
}


void DecalManager::AddDecal(const char* szTextureName,
							const Vector3& pos,
							const Vector3& normal,
							real width,
							real height,
							bool bPermanent,
							WorldObject* pRefObj,
							DecalHandle* pHandle)
// If you pass in a refernce object that decal is to be attached to,
// position and normal are assumed to be in object space.
{
	Decal* pDecal = new Decal;
	pDecal->pTexture = TextureFactory::Instance()->Get(szTextureName);

	Vector3 fn(ABS(normal.x), ABS(normal.y), ABS(normal.z));

	Vector3 up, right;
	int8 major = 0;

	// find the major axis
	if (fn.y > fn.x && fn.y > fn.z)
		major = 1;
	else if (fn.z > fn.x && fn.z > fn.y)
		major = 2;

	// build right vector by hand
	if (fn.x == 1 || fn.y == 1 || fn.z == 1)
	{
		if ((major == 0 && normal.x > 0) || major == 1)
			right.Set(0, 0, 1);
		else if (major == 0)
			right.Set(0, 0, -1);
		else 
			right.Set(-1 * normal.z, 0, 0);
	}
	else
	{
		Vector3 axis;;
		if (0 == major)
			axis.Set(1.0f, 0.0f, 0.0f);
		else if (1 == major)
			axis.Set(0.0f, 1.0f, 0.0f);
		else
			axis.Set(0.0f, 0.0f, 1.0f);

		right = normal.Cross(axis);
	}

	up = normal.Cross(right);
	up.Normalize();
	right.Normalize();

	right *= width/2.0f;
	up *= height/2.0f;

	pDecal->pos = pos;
	pDecal->verts[0] = pos - right - up;
	pDecal->verts[1] = pos + right - up;
	pDecal->verts[2] = pos - right + up;
	pDecal->verts[3] = pos + right + up;
	pDecal->lifetime = 0;
	pDecal->alpha = 0xff;
	pDecal->pRefObj = pRefObj;
	pDecal->bPermanent = bPermanent;

	DecalList* pList = NULL;
	TexDecalIterator found = m_texDecalMap.find(pDecal->pTexture);
	if (found != m_texDecalMap.end())
	{
		// release texture because m_texDecalMap already stores reference to it
		pDecal->pTexture->Release();
		pList = found->second;
	}
	else
	{
		std::pair<TexDecalIterator, bool> retVal;
		retVal = m_texDecalMap.insert(TexDecalMap::value_type(pDecal->pTexture, new DecalList));
		if (retVal.second)
		{
			pList = retVal.first->second;
		}
		else	// insert failed, now clean up
		{
			pDecal->pTexture->Release();
			delete pDecal;
		}
	}

	if (pList)
	{
		if (pHandle)
			*pHandle = pList->insert(pList->end(), pDecal);
		else
			pList->push_back(pDecal);
	}
	else
	{
		if (pHandle)
			*pHandle = (DecalHandle)0;
	}

}


void DecalManager::RemoveDecal(DecalHandle& pHandle)
{
// Since we don't know if handle is valid, we have to look it up in the map
	TexDecalIterator iTex = m_texDecalMap.begin();
	TexDecalIterator endTex = m_texDecalMap.end();
	DecalIterator iDecal;
	DecalIterator endDecal;

	for (; iTex != endTex; ++iTex)
	{
		endDecal = iTex->second->end();
		for (iDecal = iTex->second->begin(); iDecal != endDecal; ++iDecal)
		{
			if (*iDecal == *pHandle)
			{
				// To kill the decal we add to it's lifetime, so it will fade out normally
				(*iDecal)->lifetime = m_maxLife + 1;
				(*iDecal)->bPermanent = false;	
				break;
			}
		}
	}
}


void DecalManager::ClearDecals()
{
	TexDecalIterator iTex = m_texDecalMap.begin();
	TexDecalIterator endTex = m_texDecalMap.end();
	DecalIterator iDecal;
	DecalIterator endDecal;

	for (; iTex != endTex; ++iTex)
	{
		endDecal = iTex->second->end();
		for (iDecal = iTex->second->begin(); iDecal != endDecal; ++iDecal)
		{
			delete *iDecal;
		}

		delete iTex->second;
		iTex->first->Release();
	}

	m_texDecalMap.clear();
}

void DecalManager::Update(int32 msElapsed)
{
	DecalIterator iDecal;
	DecalIterator endDecal;
	TexDecalIterator iTexture;
	TexDecalIterator endTexture;
	DecalList* pList;
	Decal* pDecal;

	endTexture = m_texDecalMap.end();
	iTexture = m_texDecalMap.begin();

	while(iTexture != endTexture)
	{
		pList = iTexture->second;
		endDecal = pList->end();
		iDecal = pList->begin();

		while (iDecal != endDecal)
		{
			pDecal = *iDecal;

			pDecal->lifetime += msElapsed;
			if (!pDecal->bPermanent && pDecal->lifetime > m_maxLife)
			{
				if (pDecal->alpha > 0)
				{
					--pDecal->alpha;
				}
				else
				{
					delete *iDecal;
					iDecal = pList->erase(iDecal);
					continue;
				}
			}

			++iDecal;
		}

		if (pList->size() <= 0)
		{
			iTexture->first->Release();
			delete iTexture->second;
			iTexture = m_texDecalMap.erase(iTexture);
			continue;
		}

		++iTexture;
	}
}


void DecalManager::Render()
{
	DecalIterator iDecal;
	DecalIterator endDecal;
	TexDecalIterator iTexture;
	TexDecalIterator endTexture;
	DecalList* pList;
	Device* pDevice = Device::Instance();
	Decal* pDecal;
	RenderState rs;
	Vector3 offsetPos;
	Matrix44 objectMat;
	DynIVB divb;
	int32 vCount, vStart;
	ColorTexVertex* pVert;
	uint16* piVert;
	uint16 ivCount, ivStart, ivNext;

	if (m_texDecalMap.size() <= 0)
		return;

	if (!divb.Prepare(m_VB, sizeof(ColorTexVertex), m_IB))
		return;

	VSFactory::Instance()->GetShader("ColorTexVertFVF")->Activate();
	pDevice->SetPixelShader(0);

	rs.Set(D3DRS_LIGHTING, FALSE);
	rs.Set(D3DRS_ZWRITEENABLE, FALSE);
	rs.Set(D3DRS_ZENABLE, WKG_ZENABLE);
	rs.Set(D3DRS_COLORVERTEX, TRUE);
	rs.Set(D3DRS_ALPHABLENDENABLE, TRUE);
	rs.Set(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	rs.Set(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	rs.Set(D3DRS_DEPTHBIAS, 15);
	rs.Set(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_COLOR1);
	rs.Set(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);

	rs.SSet(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);	// CLAMP is necessary with min/max filtering to prevent anti-alias wrap
	rs.SSet(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	rs.Set(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
	rs.Set(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	rs.Set(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
	rs.Set(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	rs.Set(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	rs.Set(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

	rs.SetReal(D3DRS_DEPTHBIAS , -0.0001f);

	pDevice->SetRenderState(rs);
	pDevice->SetStreamSource(0, divb.GetVB(), 0, divb.GetVertSize());
	pDevice->SetTexture(0, (Texture*)0);

	pDevice->SetTransform(D3DTS_WORLD, &Matrix44::IDENTITY);

	endTexture = m_texDecalMap.end();
	for (iTexture = m_texDecalMap.begin(); iTexture != endTexture; ++iTexture)
	{
		pList = iTexture->second;
		pDevice->SetTexture(0, iTexture->first);

		endDecal = pList->end();
		
		for (iDecal = pList->begin(); iDecal != endDecal; ++iDecal)
		{
			pDecal = *iDecal;

			if (divb.CanAppend(4, 6, (uint8**)&pVert, &piVert, &ivNext))
			{
				if (pDecal->pRefObj)
				{
					objectMat = pDecal->pRefObj->GetOrientation();
					objectMat.SetTranslation(pDecal->pRefObj->GetPosition());
					offsetPos = pDecal->verts[0];
					offsetPos.PtMult(objectMat);
					pVert[0].x = offsetPos.x;
					pVert[0].y = offsetPos.y;
					pVert[0].z = offsetPos.z;
					offsetPos = pDecal->verts[1];
					offsetPos.PtMult(objectMat);
					pVert[1].x = offsetPos.x;
					pVert[1].y = offsetPos.y;
					pVert[1].z = offsetPos.z;
					offsetPos = pDecal->verts[2];
					offsetPos.PtMult(objectMat);
					pVert[2].x = offsetPos.x;
					pVert[2].y = offsetPos.y;
					pVert[2].z = offsetPos.z;
					offsetPos = pDecal->verts[3];
					offsetPos.PtMult(objectMat);
					pVert[3].x = offsetPos.x;
					pVert[3].y = offsetPos.y;
					pVert[3].z = offsetPos.z;
				}
				else
				{
					pVert[0].x = pDecal->verts[0].x;
					pVert[0].y = pDecal->verts[0].y;
					pVert[0].z = pDecal->verts[0].z;
					pVert[1].x = pDecal->verts[1].x;
					pVert[1].y = pDecal->verts[1].y;
					pVert[1].z = pDecal->verts[1].z;
					pVert[2].x = pDecal->verts[2].x;
					pVert[2].y = pDecal->verts[2].y;
					pVert[2].z = pDecal->verts[2].z;
					pVert[3].x = pDecal->verts[3].x;
					pVert[3].y = pDecal->verts[3].y;
					pVert[3].z = pDecal->verts[3].z;
				}

				pVert[0].tu = 0.0f;	pVert[0].tv = 0.0f;
				pVert[1].tu = 1.0f;	pVert[1].tv = 0.0f;
				pVert[2].tu = 0.0f;	pVert[2].tv = 1.0f;
				pVert[3].tu = 1.0f;	pVert[3].tv = 1.0f;

				pVert[0].diffuse =
				pVert[1].diffuse =
				pVert[2].diffuse =
				pVert[3].diffuse = D3DCOLOR_RGBA(0xff, 0xff, 0xff, pDecal->alpha);

				piVert[0] = ivNext + 0;
				piVert[1] = ivNext + 1;
				piVert[2] = ivNext + 2;
				piVert[3] = ivNext + 3;
				piVert[4] = ivNext + 2;
				piVert[5] = ivNext + 1;
			}
			else
			{
				if (divb.BatchReady(&vStart, &vCount, &ivStart, &ivCount))
				{
					Device::Instance()->SetIndices(divb.GetIB());
					Device::Instance()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, vStart, 0, vCount, ivStart, ivCount / 3);
				}
			}
		}

		if (divb.BatchReady(&vStart, &vCount, &ivStart, &ivCount))
		{
			Device::Instance()->SetIndices(divb.GetIB());
			Device::Instance()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, vStart, 0, vCount, ivStart, ivCount / 3);
		}
	}
}


void DecalManager::ResizeDecal(DecalHandle& handle, real width, real height)
{
}


void DecalManager::OrientDecal(DecalHandle& handle, const Quaternion& orient)
{
	Vector3 v;
	Decal* pDecal = NULL;
	TexDecalIterator iTexture;
	TexDecalIterator endTexture;
	DecalIterator iDecal;
	DecalIterator endDecal;
	endTexture = m_texDecalMap.end();
	for (iTexture = m_texDecalMap.begin(); iTexture != endTexture; ++iTexture)
	{
		endDecal = iTexture->second->end();
		for (iDecal = iTexture->second->begin(); iDecal != endDecal; ++iDecal)
		{
			if (*iDecal == *handle)
			{
				pDecal = *iDecal;
				break;
			}
		}
	}

	if (!pDecal)
		return;

	v.Set(pDecal->verts[0].x, pDecal->verts[0].y, pDecal->verts[0].z);
	v -= pDecal->pos;
	v.PtMult(orient);
	v += pDecal->pos;
	pDecal->verts[0].x = v.x;
	pDecal->verts[0].y = v.y;
	pDecal->verts[0].z = v.z;

	v.Set(pDecal->verts[1].x, pDecal->verts[1].y, pDecal->verts[1].z);
	v -= pDecal->pos;
	v.PtMult(orient);
	v += pDecal->pos;
	pDecal->verts[1].x = v.x;
	pDecal->verts[1].y = v.y;
	pDecal->verts[1].z = v.z;

	v.Set(pDecal->verts[2].x, pDecal->verts[2].y, pDecal->verts[2].z);
	v -= pDecal->pos;
	v.PtMult(orient);
	v += pDecal->pos;
	pDecal->verts[2].x = v.x;
	pDecal->verts[2].y = v.y;
	pDecal->verts[2].z = v.z;

	v.Set(pDecal->verts[3].x, pDecal->verts[3].y, pDecal->verts[3].z);
	v -= pDecal->pos;
	v.PtMult(orient);
	v += pDecal->pos;
	pDecal->verts[3].x = v.x;
	pDecal->verts[3].y = v.y;
	pDecal->verts[3].z = v.z;
}

}
}


