/******************************************************************************
 renderstate.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <map>

namespace wkg {
namespace engine {

class RenderState
{
	public:
		enum Basis
		{
			NEW,		// based upon default
			DELTA		// based upon current
		};
		RenderState(Basis = NEW);
		~RenderState();

		void Set(D3DRENDERSTATETYPE, uint32);
		void SetReal(D3DRENDERSTATETYPE, real);

		void Set(int32 stage, D3DTEXTURESTAGESTATETYPE, uint32);
		void SetReal(int32 stage, D3DTEXTURESTAGESTATETYPE, real);

		void SSet(int32 stage, D3DSAMPLERSTATETYPE, uint32);
		void SSetReal(int32 stage, D3DSAMPLERSTATETYPE, real);

		uint32 Get(D3DRENDERSTATETYPE) const;
		real GetReal(D3DRENDERSTATETYPE) const;

		uint32 Get(int32 stage, D3DTEXTURESTAGESTATETYPE) const;
		real GetReal(int32 stage, D3DTEXTURESTAGESTATETYPE) const;

		uint32 SGet(int32 stage, D3DSAMPLERSTATETYPE) const;
		real SGetReal(int32 stage, D3DSAMPLERSTATETYPE) const;

		static RenderState& GetDefault();

		enum Constants
		{
			RS_MAX			= 256,
			TSS_MAX			= 36,
			TSS_MAXSTAGE	= 8,
			SSS_MAX			= 32,
			SSS_MAXSTAGE	= 8,
		};
		class RSList;
		class TSSList;
		class SSSList;
		static const RSList& GetRSList();
		static const TSSList& GetTSSList();
		static const SSSList& GetSSSList();

	protected:
		RenderState(int32, int32 dummyArgToDistinguishConstructorForDefaultObj);
		static RenderState def;
		static RSList rslist;
		static TSSList tsslist;
		static SSSList ssslist;

	protected:
		uint32 rsValue[RS_MAX];
		uint32 tssValue[TSS_MAXSTAGE][TSS_MAX];
		uint32 sssValue[SSS_MAXSTAGE][SSS_MAX];
};

class RenderState::RSList
{
	public:
		RSList();

		int32 GetCount() const;

		D3DRENDERSTATETYPE operator[](int32) const;

	protected:
		uint32 list[RenderState::RS_MAX];
		int32 count;
};

class RenderState::TSSList
{
	public:
		TSSList();

		int32 GetCount() const;

		D3DTEXTURESTAGESTATETYPE operator[](int32) const;

	protected:
		uint32 list[RenderState::TSS_MAX];
		int32 count;
};

class RenderState::SSSList
{
	public:
		SSSList();

		int32 GetCount() const;

		D3DSAMPLERSTATETYPE operator[](int32) const;

	protected:
		uint32 list[RenderState::SSS_MAX];
		int32 count;
};

inline
RenderState::~RenderState()
{
}

inline RenderState& 
RenderState::GetDefault()
{
	return def;
}

inline const RenderState::RSList& 
RenderState::GetRSList()
{
	return rslist;
}

inline const RenderState::TSSList& 
RenderState::GetTSSList()
{
	return tsslist;
}

inline const RenderState::SSSList& 
RenderState::GetSSSList()
{
	return ssslist;
}

inline int32
RenderState::RSList::GetCount() const
{
	return count;
}

inline D3DRENDERSTATETYPE 
RenderState::RSList::operator[](int32 index) const
{
	ASSERT(index >= 0 && index < count);
	return D3DRENDERSTATETYPE(list[index]);
}

inline int32
RenderState::TSSList::GetCount() const
{
	return count;
}

inline D3DTEXTURESTAGESTATETYPE 
RenderState::TSSList::operator[](int32 index) const
{
	ASSERT(index >= 0 && index < count);
	return D3DTEXTURESTAGESTATETYPE(list[index]);
}

inline int32
RenderState::SSSList::GetCount() const
{
	return count;
}

inline D3DSAMPLERSTATETYPE 
RenderState::SSSList::operator[](int32 index) const
{
	ASSERT(index >= 0 && index < count);
	return D3DSAMPLERSTATETYPE(list[index]);
}

}

}
