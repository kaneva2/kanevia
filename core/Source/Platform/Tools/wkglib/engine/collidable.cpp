/******************************************************************************
 collidable.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "collisionmanager.h"
#include "boundingvolume.h"
#include "vector3.h"
#include "collidable.h"

namespace wkg
{

namespace engine
{

Collidable::Collidable() : m_volume(0), m_slide(false)
{
}

Collidable::~Collidable()
{
	CollisionManager::Instance()->RemoveCollidable(this);
}

void 
Collidable::OnCollide(Collidable *other, Collision *c)
{
}

void 
Collidable::EnableCollisions(bool e)
{
	if(e)
		CollisionManager::Instance()->AddCollidable(this);
	else
		CollisionManager::Instance()->RemoveCollidable(this);
}

Collision *
Collidable::Collide(Collidable *other, uint32 ms)
{
	return m_volume->Collide(other->m_volume, ms);
}

void 
Collidable::SetBoundingVolume(BoundingVolume *volume)
{ 
	m_volume = volume; 
}

real 
Collidable::GetBVSphereRadius()
{
	return m_volume->GetBVSphereRadius();
}


}

}


