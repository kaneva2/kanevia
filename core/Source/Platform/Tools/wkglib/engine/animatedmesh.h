/******************************************************************************
 animatedmesh.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <map>
#include <string>
#include "skinnedmesh.h"
#include "animation.h"

namespace wkg
{

class WKAFile;
class SkeletalAnimation;
class KeyframeController;
class PosMPlexController;
class OrientMPlexController;
class PosOrientToMatrix44Controller;

class AnimatedMesh : public SkinnedMesh
{

	public:
		AnimatedMesh();
		virtual ~AnimatedMesh();

		virtual void PlayAnimation(
			const char *anim_name, 
			int32 mode = Animation::PLAY_LOOPING, 
			real transition_time = 100.0f,
			bool reset = true);

		virtual void StopAnimation(
			const char *anim_name,
			real transition_time = 100.0f);
		virtual void StopAllAnimations();

		virtual bool AddAnimation(const char *anim_name, const char *filename);
		virtual void ClearAnimations();
		
		SkeletalAnimation* GetAnimation(const char *anim_name);

	protected:
		SkeletalAnimation *GetCachedAnimation(const char *filename, int32 &idx);
		bool IsUsed(int32 idx);

	protected:
		vector<PosMPlexController *>				m_pos_mplex;
		vector<OrientMPlexController *>			m_orient_mplex;
		vector<PosOrientToMatrix44Controller *>	m_pomc;
		vector<SkeletalAnimation *>				m_animations;

		map<string, int32>					m_animation_map;

		map<string, SkeletalAnimation *>		m_anim_cache;
};

}
