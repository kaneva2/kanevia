/******************************************************************************
 physics.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "units.h"
#include "Collision.h"
#include "rigidbody.h"
#include "bvsphere.h"
#include "octnode.h"
#include "physics.h"
#include "actor.h"
#include "bvellipsoid.h"
#include "bvsphere.h"
#include "actor.h"
#include "collisionmanager.h"
#include "worldobject.h"
#include "prop.h"
#include "scenegraph.h"
#include "meshnode.h"
#include "actor.h"

namespace wkg
{

namespace engine
{

dGeomID g_geomid = 0;

void 
nearCallback(void *data, dGeomID o1, dGeomID o2)
{
	Physics *p = (Physics *)data;
	p->NearCallback(o1, o2);
}

Physics::Physics() : 
	m_static_world(0),
	m_gravity(0.0f, 0.0f, 0.0f)
{
	m_world = dWorldCreate();
	m_space = dHashSpaceCreate(0);
	m_contactgroup = dJointGroupCreate(0);

	SetGravity(Vector3(0.0f, -512.0f, 0.0f));

//	dWorldSetERP(m_world, 0.01f);
//	dWorldSetCFM(m_world, 0.000001f);

	dWorldSetAutoDisableFlag(m_world, 1);
	dWorldSetAutoDisableLinearThreshold(m_world, wkg::Units::FromMeters(0.01f));
	dWorldSetAutoDisableAngularThreshold(m_world, 0.01f);
	dWorldSetAutoDisableSteps(m_world, 10);
	dWorldSetAutoDisableTime(m_world, 0);
}

Physics::~Physics()
{
	dJointGroupDestroy(m_contactgroup);
	dSpaceDestroy(m_space);
	dWorldDestroy(m_world);
}

void 
Physics::AddRigidBody(RigidBody *b)
{
	m_bodies.push_back(b);
}

void 
Physics::AddSimpleBody(RigidBody *b)
{
	m_simple_bodies.push_back(b);
}

void 
Physics::RemoveRigidBody(RigidBody *b)
{
	std::vector<RigidBody *>::iterator itor = m_bodies.begin();
	std::vector<RigidBody *>::iterator iend = m_bodies.end();

	for(; itor < iend; itor++)
	{
		if(b == (*itor))
		{
			m_bodies.erase(itor);
			return;
		}
	}

	itor = m_simple_bodies.begin();
	iend = m_simple_bodies.end();

	for(; itor < iend; itor++)
	{
		if(b == (*itor))
		{
			m_simple_bodies.erase(itor);
			return;
		}
	}
}

void 
Physics::SetGravity(Vector3 &g)
{
	m_gravity = g;

	dWorldSetGravity(m_world, m_gravity.x, m_gravity.y, m_gravity.z);
}

struct t_craptar
{
	Collision *c;
	WorldObject *wo;
};

std::vector<t_craptar> g_collisions;

void
Physics::StaticWorldUpdate(RigidBody *rb, uint32 ms)
{
	Vector3 src = rb->GetPosition();

	Vector3 dst(src);
	if(rb->m_body)
	{
		// integration is handled by the phys sim
		const dReal *dp = dBodyGetPosition(rb->m_body);
		dst = Vector3(dp[0], dp[1], dp[2]);
		dst -= rb->GetBVOffset();
	}
	else
	{
		rb->m_velocity += m_gravity * (real)ms / 1000.0f;
		dst = rb->m_position;
		dst += rb->m_velocity * (real)ms / 1000.0f;
	}
	Vector3 original_dst(dst);
	Vector3 original_src(src);

	real delta_y = dst.y - src.y;
	dst.y = src.y;

	WorldObject *wo = 0;

	Vector3 radius(0.0f, 0.0f, 0.0f);
	BVEllipsoid *bve = dynamic_cast<BVEllipsoid *>(rb->GetBoundingVolume());
	if(bve)
	{
		radius = bve->GetRadius();
		wo = bve->GetWorldObject();
	}

	BVSphere *bvs = dynamic_cast<BVSphere *>(rb->GetBoundingVolume());
	if(bvs)
	{
		radius = Vector3(bvs->GetRadius(), bvs->GetRadius(), bvs->GetRadius());
		wo = bvs->GetWorldObject();
	}


	if(bvs || bve)
	{
		src += rb->GetBVOffset();
		dst += rb->GetBVOffset();
		Vector3 n, poc;
		bool b = false;

#if 1
		if(rb->ShouldSlide())
		{
			b = m_static_world->CollideDynamicEllipsoid(src, dst, radius, &n, &poc);

			src = dst;
			dst.y += delta_y;

			Vector3 gn, gpoc;
			bool gb = m_static_world->CollideDynamicEllipsoid(src, dst, radius, &gn, &gpoc);
			if(gb) 
				rb->m_on_ground = true;
			else
				rb->m_on_ground = false;

			if(!b && gb)
			{
				n = gn;
				poc = gpoc;
				b = true;
			}
		}
		else
		{
			dst.y += delta_y;
			b = m_static_world->CollideDynamicEllipsoid(src, dst, radius, &n, &poc, 0);

			if(b) 
				rb->m_on_ground = true;
			else
				rb->m_on_ground = false;
		}
#endif

		dst -= rb->GetBVOffset();
		src -= rb->GetBVOffset();

		rb->SetPosition(dst);

		if(b)
		{
			if(!rb->m_body)
			{
				Vector3 osrc = rb->GetPosition();
				Vector3 v = dst - osrc;
				v /= ((real)ms / 1000.0f);
				rb->SetVelocity(v);
			}

			rb->SetPosition(dst);

			Collision *c = new Collision();
			c->m_dst = dst;
			c->m_poc = poc;
			c->m_n = n;

			c->m_t = (dst - original_src).Length() / (original_dst - original_src).Length();

			if(rb->m_body)
			{
				dContact contact;
				memzero(&contact, sizeof(dContact));

				contact.surface.mode = 0;//dContactBounce;// | dContactSoftERP;
				contact.surface.mu = 0.0f;//rb->m_mu;
				contact.surface.soft_erp = 0.01f;
				contact.surface.mu2 = 0;
				contact.surface.bounce = rb->m_cor;
				contact.surface.bounce_vel = 0.1f;

				Vector3 odedst(dst);
				odedst += rb->GetBVOffset();

				contact.geom.pos[0] = odedst.x;
				contact.geom.pos[1] = odedst.y;
				contact.geom.pos[2] = odedst.z;

				contact.geom.normal[0] = n.x;
				contact.geom.normal[1] = n.y;
				contact.geom.normal[2] = n.z;

				contact.geom.depth = 0.01f;

				contact.geom.g1 = rb->m_geom[0];
				contact.geom.g2 = 0;

				dJointID djid = dJointCreateContact(m_world, m_contactgroup, &contact);
				dJointAttach(djid, rb->m_body, 0);
			}

			t_craptar tct;
			tct.c = c;
			tct.wo = wo;

			g_collisions.push_back(tct);
		}
	}
}

bool
Physics::ExistCollision(WorldObject *wo)
{
	for(int32 i = 0; i < g_collisions.size(); i++)
	{
		if(g_collisions[i].wo == wo) return true;
	}
	return false;
}

bool
Physics::IsBodyInSim(WorldObject *wo)
{
	std::vector<RigidBody *>::iterator itor = m_bodies.begin();
	std::vector<RigidBody *>::iterator iend = m_bodies.end();

	for(; itor < iend; itor++)
	{
		if(wo == (*itor))
		{
			return true;
		}
	}

	itor = m_simple_bodies.begin();
	iend = m_simple_bodies.end();
	for(; itor < iend; itor++)
	{
		if(wo == (*itor))
		{
			return true;
		}
	}

	return false;
}

const real STEPSIZE = 0.002f;

void 
Physics::Update(uint32 ms)
{
	g_collisions.clear();

	// first, divide the impulsive forces by the time that has passed
	std::vector<RigidBody *>::iterator iRB = m_bodies.begin();
	std::vector<RigidBody *>::iterator endRB = m_bodies.end();
	for(; iRB < endRB; iRB++)
	{
		(*iRB)->ApplyImpulsesToForce(ms);
	}

#if 1
	if(!ms) return;

	dWorldSetQuickStepNumIterations(m_world, 10);

	static real time_to_go = 0.0f;
	time_to_go += (real)ms / 1000.0f;

	if(time_to_go > .3f) time_to_go = .3f;

	while(time_to_go > STEPSIZE)
	{
		dSpaceCollide(m_space, this, &nearCallback);
		dWorldQuickStep(m_world, STEPSIZE);
		dJointGroupEmpty(m_contactgroup);

		iRB = m_bodies.begin();
		endRB = m_bodies.end();
		for(; iRB < endRB; iRB++)
		{
			game::Actor *a = dynamic_cast<game::Actor *>(*iRB);
			if(a)
				a->SetOrientation(a->GetOrientation());
		}
		time_to_go -= STEPSIZE;
	}

#else

	static real time_to_step_to_go = 0.0f;
	time_to_step_to_go += (real)ms / 1000.0f;
	if(time_to_step_to_go > .333f) time_to_step_to_go = .333f;

	while(time_to_step_to_go > STEPSIZE)
	{
		dSpaceCollide(m_space, this, &nearCallback);

		dWorldStep(m_world, STEPSIZE);

		dJointGroupEmpty(m_contactgroup);

		time_to_step_to_go -= STEPSIZE;
	}

#if 0
	if(time_to_step_to_go > 0.0f)
	{
		dSpaceCollide(m_space, this, &nearCallback);
		dWorldStep(m_world, time_to_step_to_go);
		dJointGroupEmpty(m_contactgroup);
	}
#endif

#endif

	iRB = m_bodies.begin();
	endRB = m_bodies.end();
	for(; iRB < endRB; iRB++)
	{
		StaticWorldUpdate((*iRB), ms);
		(*iRB)->UpdateStateFromSim();
	}

	iRB = m_simple_bodies.begin();
	endRB = m_simple_bodies.end();
	for(; iRB < endRB; iRB++)
	{
		StaticWorldUpdate((*iRB), ms);
	}

	int32 nc = g_collisions.size();
	for(int32 i = 0; i < g_collisions.size(); i++)
	{
		if(IsBodyInSim(g_collisions[i].wo))
		{
			CollisionManager::Instance()->AddCollision(g_collisions[i].wo, 0, g_collisions[i].c);
		}
	}
}

void
Physics::NearCallback(dGeomID o1, dGeomID o2)
{
	int32 i;

	// exit without doing anything if the two bodies are connected by a joint
	dBodyID b1 = dGeomGetBody(o1);
	dBodyID b2 = dGeomGetBody(o2);

	RigidBody *rb1 = 0;
	RigidBody *rb2 = 0;

	if(b1)
		rb1 = (RigidBody *)dBodyGetData(b1);

	if(b2)
		rb2 = (RigidBody *)dBodyGetData(b2);

	if(!b1 && !b2) return;

	if(b1 && b2 && dAreConnected(b1,b2)) 
		return;

	// up to 16 contacts per object
	dContact contact[16];
	memset(&contact[0], 0, sizeof(dContact) * 16);

	for(i = 0; i < 16; i++) 
	{
//		if(rb1 && rb2)
//		{
			contact[i].surface.mode = 0;//dContactBounce;// | dContactSoftERP;
//		}
//		else
//		{
//			contact[i].surface.mode = dContactBounce | dContactSoftERP;
//		}

		contact[i].surface.soft_erp = 0.1f;

		if(rb1 && rb2)
		{
			contact[i].surface.mode |= dContactMu2;
			contact[i].surface.mu = rb1->m_mu;
			contact[i].surface.mu2 = rb2->m_mu;
			contact[i].surface.bounce = 0.0f;//(rb1->m_cor + rb2->m_cor) / 2.0f;
		}
		else if(rb1 || rb2)
		{
			contact[i].surface.mu2 = 0;
			if(rb1)
			{
				contact[i].surface.mu = rb1->m_mu;
				contact[i].surface.bounce = 0.0f;//rb1->m_cor;
			}
			else
			{
				contact[i].surface.mu = rb2->m_mu;
				contact[i].surface.bounce = 0.0f;//rb2->m_cor;
			}
		}

		contact[i].surface.bounce_vel = 0.01f;
	}

	int32 numc = dCollide(o1, o2, 3, &contact[0].geom, sizeof(dContact));
	if(numc) 
	{
		for(i = 0; i < numc; i++) 
		{
			dJointID c = dJointCreateContact(m_world, m_contactgroup, contact + i);
			dJointAttach(c, b1, b2);

			

//			DEBUGMSG("COLLISION:");
			dMass mass;
			if(b1)
			{
				dBodyGetMass(b1, &mass);
//				DEBUGMSG("b1: %.4f", mass.mass);
			}

			if(b2)
			{
				dBodyGetMass(b2, &mass);
//				DEBUGMSG("b2: %.4f", mass.mass);
			}
		}


		Collision *c = new Collision();
		c->m_dst = Vector3(0.0f, 0.0f, 0.0f);
		c->m_poc = Vector3(0.0f, 0.0f, 0.0f);
		c->m_n = Vector3(0.0f, 0.0f, 0.0f);
		c->m_t = 0.0f;

		CollisionManager::Instance()->AddCollision(rb1, rb2, c);

	}
}

void 
Physics::SetStaticWorld(OctNode *root, std::vector<Vector3> &in_verts)
{
//	dCreatePlane(m_space, 0.0f, 1.0f, 0.0f, 0.0f); 

//	dGeomID box = dCreateBox(m_space, 12000.0f, 84.0f, 12000.0f);
//	dGeomSetPosition(box, 0, 0, 0.0);
//	dMatrix3 boxRotation;
//	memset(boxRotation, 0, sizeof(dMatrix3));
//	dRFromAxisAndAngle(boxRotation, 0.0f, 0.0f, 0.0f, 1.0f);
//	dGeomSetRotation(box, boxRotation);

	m_static_world = root;

	return;

	dTriMeshDataID triid = dGeomTriMeshDataCreate();

	struct StridedVertex 
	{
		dVector3 vertex;
	};
	int vertexStride = sizeof (StridedVertex);

	struct StridedTri 
	{
		int indices[3];
	};
	int triStride = sizeof (StridedTri);

	static StridedVertex *verts = new StridedVertex[in_verts.size()];
	static StridedTri *indices = new StridedTri[in_verts.size()/3];

	for(int32 i = 0; i < in_verts.size(); i++)
	{
		verts[i].vertex[0] = in_verts[i].x;
		verts[i].vertex[1] = in_verts[i].y;
		verts[i].vertex[2] = in_verts[i].z;

		verts[i].vertex[3] = 0.0f;

		indices[i / 3].indices[i % 3] = i;
	}

#if 0
	for(i = 0; i < in_verts.size() / 3; i++)
	{
		Vector3 v1;
		v1.x = verts[i * 3 + 0].vertex[0];
		v1.y = verts[i * 3 + 0].vertex[1];
		v1.z = verts[i * 3 + 0].vertex[2];

		Vector3 v2;
		v2.x = verts[i * 3 + 1].vertex[0];
		v2.y = verts[i * 3 + 1].vertex[1];
		v2.z = verts[i * 3 + 1].vertex[2];

		Vector3 v3;
		v3.x = verts[i * 3 + 2].vertex[0];
		v3.y = verts[i * 3 + 2].vertex[1];
		v3.z = verts[i * 3 + 2].vertex[2];

		Vector3 centroid = (v1 + v2 + v3) / 3.0f;

		v1 += (v1 - centroid).Normalize() * 10.0f;
		v2 += (v2 - centroid).Normalize() * 10.0f;
		v3 += (v3 - centroid).Normalize() * 10.0f;

		verts[i * 3 + 0].vertex[0] = v1.x;
		verts[i * 3 + 0].vertex[1] = v1.y;
		verts[i * 3 + 0].vertex[2] = v1.z;

		verts[i * 3 + 1].vertex[0] = v2.x;
		verts[i * 3 + 1].vertex[1] = v2.y;
		verts[i * 3 + 1].vertex[2] = v2.z;

		verts[i * 3 + 2].vertex[0] = v3.x;
		verts[i * 3 + 2].vertex[1] = v3.y;
		verts[i * 3 + 2].vertex[2] = v3.z;
	}
#endif

	dGeomTriMeshDataBuildSingle(
		triid,
		(const void *)(verts),
        vertexStride, 
		in_verts.size(),
		(const void *)indices, 
		in_verts.size(),
		triStride);

	// add it into the simulation
	g_geomid = dCreateTriMesh(m_space, triid, 0, 0, 0);

	dGeomSetPosition(g_geomid, 0, 0, 0.0);
	dMatrix3 Rotation;
	memset(Rotation, 0, sizeof(dMatrix3));
	dRFromAxisAndAngle(Rotation, 0.0f, 0.0f, 0.0f, 1.0f);
	dGeomSetRotation(g_geomid, Rotation);

//	dGeomSetCategoryBits(g_geomid, 0x01);
//	dGeomSetCollideBits(g_geomid, 0xff - 0x04);
}

void 
Physics::CreatePropChain(
						  Vector3 &start, 
						  Vector3 &delta, 
						  int32 num,
						  std::string &body_name, 
						  std::vector<game::Prop *> &bodies)
{

	dJointGroupID djgroup = dJointGroupCreate(0);

	Vector3 curr_pos = start;
	game::Prop *prev_p = 0;
	for(int32 i = 0; i < num; i++)
	{
		game::Prop *p = new game::Prop();
		p->Load(body_name.c_str());
		p->AddToSim();
		p->EnableCollisions(true);
		p->SetPosition(curr_pos);

		if(prev_p)
		{
			dJointID djid = dJointCreateBall(m_world, djgroup);
			dJointAttach(djid, prev_p->m_body, p->m_body);
			Vector3 jpos(curr_pos - (delta / 2.0f));
			dJointSetBallAnchor(djid, jpos.x, jpos.y, jpos.z);
		}
#if 0
		else
		{
			dJointID djid = dJointCreateBall(m_world, djgroup);
			dJointAttach(djid, 0, p->m_body);
			Vector3 jpos(curr_pos - (delta / 2.0f));
			dJointSetBallAnchor(djid, jpos.x, jpos.y, jpos.z);
		}
#endif

		SceneGraph::Instance()->GetRoot()->AddChild(p->GetNode());

		bodies.push_back(p);

		curr_pos += delta;

		prev_p = p;
	}
}

void 
Physics::CreateBallJoint(RigidBody *b1, RigidBody *b2, Vector3 &pos)
{
	dJointID djid = dJointCreateBall(m_world, 0);

	if(b1 && b2)
		dJointAttach(djid, b1->m_body, b2->m_body);
	else if(b1)
		dJointAttach(djid, b1->m_body, 0);
	else if(b2)
		dJointAttach(djid, b2->m_body, 0);

	dJointSetBallAnchor(djid, pos.x, pos.y, pos.z);
}

}

}


