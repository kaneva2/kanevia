/******************************************************************************
 Model.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "rigidbody.h"

namespace wkg 
{

class Node;
class XMLDocument;
class XMLNode;

namespace engine 
{

class Mesh;
class MeshNode;

class Model : public RigidBody
{
public:
	Model();
	virtual ~Model();

	Mesh* GetMesh() { return m_pMesh; }
	MeshNode* GetNode() { return m_pNode; }

	bool Load(const char* filename);

	void SetParent(Node* pParentNode);

	virtual void SetPosition(const Vector3 &position);
	virtual void SetOrientation(const Quaternion &orientation);

protected:
	virtual bool Load(XMLDocument *doc);
	virtual bool LoadXMLNode(XMLNode* curr);

protected:
	Mesh		*m_pMesh;
	MeshNode	*m_pNode;
	MeshNode	*m_pSNode;
	Node		*m_pParentNode;

};


}

}


