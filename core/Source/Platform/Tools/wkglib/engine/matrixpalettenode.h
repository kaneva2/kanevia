/******************************************************************************
 matrixpalettenode.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "XFormNode.h"
#include <vector>

namespace wkg
{

namespace engine
{

class MatrixPaletteNode : public Node
{
public:
	MatrixPaletteNode()
	{
	}

	virtual ~MatrixPaletteNode()
	{
	}

	void AddNode(XFormNode *node, Matrix44 inv_base_transform)
	{
		m_nodes.push_back(node);
		m_inverse_base_transforms.push_back(inv_base_transform);
	}

	Matrix44 GetMatrix(int32 idx)
	{
		if(idx < 0) 
		  return Matrix44::IDENTITY;
		ASSERT(uint32(idx) < m_nodes.size());	// otherwise it'll *create* an item
		// return the inverse base transform times the current node transform
		//  the inverse base transform takes us from model space to local (parent) space.
		return m_inverse_base_transforms[idx] * m_nodes[idx]->GetLocalToWorld();
	}

protected:
	std::vector<XFormNode *> m_nodes;
	std::vector<Matrix44> m_inverse_base_transforms;
};

}

}