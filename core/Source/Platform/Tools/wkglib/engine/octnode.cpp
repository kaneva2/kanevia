/******************************************************************************
 octnode.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"

#include <algorithm>

#include "mesh.h"
#include "vector3.h"
#include "meshtypes.h"
#include "intersect.h"
#include "distance.h"
#include "sphere.h"
#include "triangle.h"
#include "capsule.h"
#include "units.h"
#include "collisionmanager.h"
#include "vector4.h"
#include "filestream.h"

#include "octnode.h"

#define CONSTRAIN_TRIS

#define NUM_TRIS (265)
#define MIN_VOLUME (12.0f * 50.0f)

namespace wkg
{

namespace engine
{

uint32 OctNode::ms_child_flags[8] =
{
	LEFT  | LOWER | BACK,
	LEFT  | LOWER | FRONT,
	LEFT  | UPPER | BACK,
	LEFT  | UPPER | FRONT,
	RIGHT | LOWER | BACK,
	RIGHT | LOWER | FRONT,
	RIGHT | UPPER | BACK,
	RIGHT | UPPER | FRONT,
};

OctNode::OctNode()
{
	memzero(m_children, sizeof(OctNode *) * 8);
}

OctNode::~OctNode()
{
	for(int32 i = 0; i < 8; i++)
		delete m_children[i];

	m_children[i] = 0;
}

bool 
OctNode::AddMesh(Mesh *mesh, const Matrix44 &xform)
{
	std::vector<Vector3> vecs;
	mesh->VecsFromVerts(vecs, xform);

	return AddGeometry(vecs);
}

void 
OctNode::GetChildBounds(int32 child_idx, Vector3 &min, Vector3 &max)
{
	uint32 child = ms_child_flags[child_idx];

	if(child & LEFT) 
	{
		min.x = this->min.x;
		max.x = this->min.x + (this->max.x - this->min.x) / 2.0f;
	}
	else if(child & RIGHT)
	{
		min.x = this->min.x + (this->max.x - this->min.x) / 2.0f;
		max.x = this->max.x;
	}

	if(child & LOWER)
	{
		min.y = this->min.y;
		max.y = this->min.y + (this->max.y - this->min.y) / 2.0f;
	}
	else if(child & UPPER)
	{
		min.y = this->min.y + (this->max.y - this->min.y) / 2.0f;
		max.y = this->max.y;
	}

	if(child & BACK)
	{
		min.z = this->min.z;
		max.z = this->min.z + (this->max.z - this->min.z) / 2.0f;
	}
	else if(child & FRONT)
	{
		min.z = this->min.z + (this->max.z - this->min.z) / 2.0f;
		max.z = this->max.z;
	}
}

bool
OctNode::AddGeometry(std::vector<Vector3> &verts)
{
	std::vector<uint32> poly_ids;
	for(uint32 i = 0; i < verts.size(); i += 3)
	{
		poly_ids.push_back(i / 3);
	}

	return AddGeometryHelper(verts, poly_ids);
}

bool
OctNode::AddGeometryHelper(std::vector<Vector3> &verts, std::vector<uint32> &poly_ids)
{
	// this function assumes nothing about the input verts, so pick
	//  out only those we're interested in

	std::vector<Vector3>::iterator vi = verts.begin();
	std::vector<Vector3>::iterator ve = verts.end();
	for(; vi < ve; vi += 3)
	{
		if(Intersect::AABBTri(*this, Triangle(&vi[0])))
		{
			m_verts.push_back(*vi);
			m_verts.push_back(*(vi + 1));
			m_verts.push_back(*(vi + 2));

			m_poly_ids.push_back(poly_ids[(vi - verts.begin()) / 3]);
		}
	}

	if(!m_verts.size()) 
		return false;

	// if our target triangles are within our range, then add them
	//  otherwise, push them down
	if(max.x - min.x < MIN_VOLUME &&
	   max.y - min.y < MIN_VOLUME &&
	   max.z - min.z < MIN_VOLUME
#ifdef CONSTRAIN_TRIS
	 && m_verts.size() < NUM_TRIS * 3)
#else 
	  )
#endif
	{
		// we're in business - we're within our target
		return true;
	}
	else
	{
		// the new verts, plus our current verts (if any) are too many
		//  for us, so push them further down the tree.

		for(int32 i = 0; i < 8; i++)
		{
			if(!m_children[i])
			{
				m_children[i] = new OctNode();
				Vector3 min, max;
				GetChildBounds(i, min, max);
				m_children[i]->Set(min, max);
			}
			if(!m_children[i]->AddGeometryHelper(m_verts, m_poly_ids))
			{
				if(!m_children[i]->HasChildren())
				{
					delete m_children[i];
					m_children[i] = 0;
				}
			}
		}

		m_verts.clear();
	}

	return true;
}

bool 
OctNode::HasChildren()
{
	return 
	   (m_children[0] ||
		m_children[1] ||
		m_children[2] ||
		m_children[3] ||
		m_children[4] ||
		m_children[5] ||
		m_children[6] ||
		m_children[7]);
}

void 
OctNode::DFSRender()
{
	Node::DFSRender();

	for(int32 i = 0; i < 8; i++)
	{
		if(m_children[i])
			m_children[i]->DFSRender();
	}
}

void 
OctNode::DFSUpdate(Matrix44 &m)
{
	Node::DFSUpdate(m);

	for(int32 i = 0; i < 8; i++)
	{
		if(m_children[i])
			m_children[i]->DFSUpdate(m);
	}
}

bool 
OctNode::CollideCapsule(Capsule &c, Vector3 *n)
{
	if(Intersect::CapsuleAABB(c, *this))
	{
		if(m_verts.size())
		{
			// test our geometry
			for(uint32 v = 0; v < m_verts.size() / 3; v++)
			{
				Triangle tri(&m_verts[v * 3]);
				if(Intersect::CapsuleTri(c, tri))
				{
					if(n)
						*n = tri.GetNormal().Normalize();
					return true;
				}
			}
		}
		else
		{
			for(int32 i = 0; i < 8; i++)
			{
				if(m_children[i])
				{
					if(m_children[i]->CollideCapsule(c, n))
						return true;
				}
			}
		}
	}

	return false;
}

bool 
OctNode::CollideSphere(Vector3 &dst, real r, Vector3 *n, Vector3 *poc)
{
	Sphere sphere(dst, r);
	if(Intersect::AABBSphere(*this, sphere))
	{
		if(m_verts.size())
		{
			// test our geometry
			for(uint32 v = 0; v < m_verts.size() / 3; v++)
			{
				Triangle tri(&m_verts[v * 3]);
				if(Intersect::SphereTri(sphere, tri))
				{
					if(n)
					{
						*n = tri.GetNormal().Normalize();
					}

					if(poc)
					{
						// get the distance from the tri to the center
						//  of the sphere
						real dist = Distance::SqrTriPoint(tri, dst);
						dist = wkg::sqrt(dist);
						dist -= r;
						(*poc) = dst + tri.GetNormal().Normalize() * dist;
					}

					return true;
				}
			}
		}
		else
		{
			for(int32 i = 0; i < 8; i++)
			{
				if(m_children[i])
				{
					if(m_children[i]->CollideSphere(dst, r, n))
						return true;
				}
			}
		}
	}

	return false;
}

#define COLLISION_EPSILION (0.001f)

int32 g_collision_recursion_depth = 0;

bool 
OctNode::CollideDynamicEllipsoid(Vector3 &src, Vector3 &out_dst, 
								 Vector3 r, Vector3 *out_n, Vector3 *out_poc,
								 int32 recurse)
{
	Vector3 n;
	Vector3 poc;
	Vector3 original_dst = out_dst;

	g_collision_recursion_depth = recurse;

	bool b = CollideDynamicEllipsoidHelper(src, out_dst, r, &n, &poc);

	if(b)
	{

		if(out_n)
			(*out_n) = n;

		if(out_poc)
			(*out_poc) = poc;
	}

	return b;
}

bool 
OctNode::CollideDynamicSphere(Vector3 &src, Vector3 &out_dst, 
								 real r, Vector3 *out_n, Vector3 *out_poc,
								 int32 recurse)
{
	Vector3 n;
	Vector3 poc;
	Vector3 original_dst = out_dst;

	g_collision_recursion_depth = recurse;

	bool b = CollideDynamicEllipsoidHelper(src, out_dst, Vector3(r, r, r), &n, &poc);

	if(b)
	{

		if(out_n)
			(*out_n) = n;

		if(out_poc)
			(*out_poc) = poc;
	}

	return b;
}

bool
GetLowestRoot(real a, real b, real c, real max_r, real &root)
{
	real determinant = b * b - 4.0f * a * c;

//	if(determinant > -0.1 && determinant < 0.1)
//	{
//		determinant = 0.0f;
//	}

	if(determinant < 0.0f) 
	{
//		if(EQUAL(determinant, 0.0f))
//		{
//			determinant = 0.0f;
//		}
//		else
//		{
//			DEBUGMSG("neg det!");
			return false;
//		}
	}

	// calculate the two roots
	real sqrtd = sqrt(determinant);
	real r1 = (-b - sqrtd) / (2.0f * a);
	real r2 = (-b + sqrtd) / (2.0f * a);

	if(r1 > r2)
	{
		real temp = r2;
		r2 = r1;
		r1 = temp;
	}

//	if(EQUAL(0.0f, r1))
//	{
//		r1 = 0.0f;
//		root = r1;
//		return true;
//	}

//	if(EQUAL(0.0f, r2))
//	{
//		r2 = 0.0f;
//		root = r2;
//		return true;
//	}

//	DEBUGMSG("%.6f, %.6f, %.6f", determinant, r1, r2);

	if(r1 > 0.0f && r1 < max_r)
	{
		root = r1;
		return true;
	}

	// it's possible that we want x2
	if(r2 > 0.0 && r2 < max_r)
	{
		root = r2;
		return true;
	}

	root = max_r;

	return false;
}

#define PLANE_IN(a)((uint32&)a)

bool
PointInTri(const Vector3 &p, 
		   const Vector3 &pa, 
		   const Vector3 &pb, 
		   const Vector3 &pc)
{
	Vector3 e10 = pb - pa;
	Vector3 e20 = pc - pa;

	real a = e10.Dot(e10);
	real b = e10.Dot(e20);
	real c = e20.Dot(e20);
	real ac_bb = (a * c) - (b * b);
	
	Vector3 vp(p.x - pa.x, p.y - pa.y, p.z - pa.z);

	real d = vp.Dot(e10);
	real e = vp.Dot(e20);
	real x = (d * c) - (e * b);
	real y = (e * a) - (d * b);
	real z = x + y - ac_bb;

	return 0 != ((PLANE_IN(z) & ~(PLANE_IN(x)|PLANE_IN(y))) & 0x80000000);

}

bool
CheckTriangle(const Vector3 &origin, const Vector3 &velocity, 
			  const Vector3 &p1, const Vector3 &p2, const Vector3 &p3,
			  double &out_t, double &dist_to_collision, Vector3 &out_collision_point)
{
	Vector3 n = (p2 - p1).Cross(p3 - p1);
	n.Normalize();

	Vector3 n_inv = -n;

	real d_center_plane = (origin - p1).Dot(n);

	double t0 = 0.0;
	double t1 = 0.0;

	bool embedded = false;

	real normal_dot_velocity = n.Dot(velocity);
	if(EQUAL(0.0f, normal_dot_velocity))
	{
		// either collision the whole way or never a collision
		if(d_center_plane < 1.0f)
		{
			// we're embedded the entire way
			t0 = 0.0f;
			t1 = 1.0f;
			embedded = true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		// calculate t0 and t1 when the sphere overlaps the plane
		t0 = (1.0 - d_center_plane) / normal_dot_velocity;
		t1 = (-1.0 - d_center_plane) / normal_dot_velocity;

		if(t1 < t0)
		{
			double temp = t1;
			t1 = t0;
			t0 = temp;
		}

		if(t0 > 1.0 || t1 < 0.0)
		{
			return false;
		}

		if(t0 < 0.0f) t0 = 0.0;
		if(t1 < 0.0f) t1 = 0.0;
		if(t0 > 1.0f) t0 = 1.0;
		if(t1 > 1.0f) t1 = 1.0;
	}


	// now if there exist a collision, it must be within these two times
	Vector3 collision_point;
	bool found_collision = false;
	real t = 1.0;

#if 1
	if(!embedded)
	{
		Vector3 plane_intersection_point = origin - n + velocity * t0;

		if(PointInTri(plane_intersection_point, p1, p2, p3))
		{
			// we have a collision!
			found_collision = true;
			t = t0;
			collision_point = plane_intersection_point;
		}
	}
#endif

	if(!found_collision)
	{
		real velocity_length_sqr = velocity.LengthSqr();

		real a, b, c;
		real new_t;

		// for each vertex or edge, solve a quadratic equation
		// check against verts
#if 1
		a = velocity_length_sqr;

		// P1
		b = 2.0f * (velocity.Dot(origin - p1));
		c = (p1 - origin).LengthSqr() - 1.0f;
		if(GetLowestRoot(a, b, c, t, new_t))
		{
			t = new_t;
			found_collision = true;
			collision_point = p1;
		}

		// P2
		b = 2.0f * (velocity.Dot(origin - p2));
		c = (p2 - origin).LengthSqr() - 1.0f;
		if(GetLowestRoot(a, b, c, t, new_t))
		{
			t = new_t;
			found_collision = true;
			collision_point = p2;
		}

		// P3
		b = 2.0f * (velocity.Dot(origin - p3));
		c = (p3 - origin).LengthSqr() - 1.0f;
		if(GetLowestRoot(a, b, c, t, new_t))
		{
			t = new_t;
			found_collision = true;
			collision_point = p3;
		}
#endif


		// now check against edges
		// p1->p2
		Vector3 edge = p2 - p1;
		Vector3 origin_to_vert = p1 - origin;
		real edge_len_sqr = edge.LengthSqr();
		real edge_dot_vel = edge.Dot(velocity);
		real edge_dot_origin_to_vert = edge.Dot(origin_to_vert);

		// calculate parameters for equation
		a = edge_len_sqr * - velocity_length_sqr + 
			edge_dot_vel * edge_dot_vel;

		b = edge_len_sqr * (2.0f * velocity.Dot(origin_to_vert)) -
			2.0f * edge_dot_vel * edge_dot_origin_to_vert;

		c = edge_len_sqr * (1.0f - origin_to_vert.LengthSqr()) +
			edge_dot_origin_to_vert * edge_dot_origin_to_vert;

		if(GetLowestRoot(a, b, c, t, new_t))
		{
			// check if intersection is within line segment
			real f = (edge_dot_vel * new_t - edge_dot_origin_to_vert) / edge_len_sqr;

			if(f >= 0.0f && f <= 1.0f)
			{
				// took place within segment
				t = new_t;
				found_collision = true;
				collision_point = p1 + edge * f;
			}
			else if(EQUAL(0.0f, f))
			{
				ASSERT(0);
			}
		}

#if 1
		// p2 -> p3
		edge = p3 - p2;
		origin_to_vert = p2 - origin;
		edge_len_sqr = edge.LengthSqr();
		edge_dot_vel = edge.Dot(velocity);
		edge_dot_origin_to_vert = edge.Dot(origin_to_vert);

		// calculate parameters for equation
		a = edge_len_sqr * - velocity_length_sqr + 
			edge_dot_vel * edge_dot_vel;

		b = edge_len_sqr * (2.0f * velocity.Dot(origin_to_vert)) -
			2.0f * edge_dot_vel * edge_dot_origin_to_vert;

		c = edge_len_sqr * (1.0f - origin_to_vert.LengthSqr()) +
			edge_dot_origin_to_vert * edge_dot_origin_to_vert;

		if(GetLowestRoot(a, b, c, t, new_t))
		{
			// check if intersection is within line segment
			real f = (edge_dot_vel * new_t - edge_dot_origin_to_vert) / edge_len_sqr;

			if(f >= 0.0f && f <= 1.0f)
			{
				// took place within segment
				t = new_t;
				found_collision = true;
				collision_point = p2 + edge * f;
			}
			else if(EQUAL(0.0f, f))
			{
				ASSERT(0);
			}
		}

		// p3 -> p1
		edge = p1 - p3;
		origin_to_vert = p3 - origin;
		edge_len_sqr = edge.LengthSqr();
		edge_dot_vel = edge.Dot(velocity);
		edge_dot_origin_to_vert = edge.Dot(origin_to_vert);

		// calculate parameters for equation
		a = edge_len_sqr * - velocity_length_sqr + 
			edge_dot_vel * edge_dot_vel;

		b = edge_len_sqr * (2.0f * velocity.Dot(origin_to_vert)) -
			2.0f * edge_dot_vel * edge_dot_origin_to_vert;

		c = edge_len_sqr * (1.0f - origin_to_vert.LengthSqr()) +
			edge_dot_origin_to_vert * edge_dot_origin_to_vert;

		if(GetLowestRoot(a, b, c, t, new_t))
		{
			// check if intersection is within line segment
			real f = (edge_dot_vel * new_t - edge_dot_origin_to_vert) / edge_len_sqr;

			if(f >= 0.0f && f <= 1.0f)
			{
				// took place within segment
				t = new_t;
				found_collision = true;
				collision_point = p3 + edge * f;
			}
			else if(EQUAL(0.0f, f))
			{
				ASSERT(0);
			}
		}
#endif
	}

	// set result
	if(found_collision)
	{
		real rdist_to_collision = velocity.Length() * t;

		dist_to_collision = sqrt(
			double(
			velocity.x * velocity.x + 
			velocity.y * velocity.y + 
			velocity.z * velocity.z));

		dist_to_collision *= t;
//		DEBUGMSG("%.4f", t);

		out_t = t;
		out_collision_point = collision_point;
	}

	return found_collision;
}

void
OctNode::CollideDynamicEllipsoidHelperHelper(const Vector3 &origin, const Vector3 &velocity, const Vector3 &r, 
	bool &found_collision, double &nearest_dist_to_collision, Vector3 &nearest_collision_point, double &nearest_t)
{
	// compute a matrix transform that puts the triangles in unit sphere space
	real sx = (1.0f / r.x);
	real sy = (1.0f / r.y);
	real sz = (1.0f / r.z);

	Vector3 world_o(origin);
	world_o.x /= sx; world_o.y /= sy; world_o.z /= sz;

	Vector3 world_v(velocity);
	world_v.x /= sx; world_v.y /= sy; world_v.z /= sz;

	LineSegment seg;
	seg.SetEndPoints(world_o, world_o + world_v);

	real greatest_r = r.x;
	if(r.y > greatest_r) greatest_r = r.y;
	if(r.z > greatest_r) greatest_r = r.z;

	Capsule c(seg, greatest_r);

	Vector3 out_n;

	if(CollideCapsule(c, &out_n))
	{
		// now for each triangle, collide with it
		int32 vsize = m_verts.size();
		for(int32 i = 0; i < vsize; i += 3)
		{
			Vector3 p1, p2, p3;

			p1 = m_verts[i + 0];
			p1.x *= sx; p1.y *= sy; p1.z *= sz;

			p2 = m_verts[i + 1];
			p2.x *= sx; p2.y *= sy; p2.z *= sz;
			
			p3 = m_verts[i + 2];
			p3.x *= sx; p3.y *= sy; p3.z *= sz;

			double t;
			Vector3 collision_point;
			double dist_to_collision;

			bool b = CheckTriangle(origin, velocity, p1, p2, p3, 
				t, dist_to_collision, collision_point);

			if(b)
			{
				if(found_collision && EQUAL(dist_to_collision, nearest_dist_to_collision))
				{
//					DEBUGMSG("multiple contacts");
				}

				if(!found_collision || (dist_to_collision < nearest_dist_to_collision))
				{
					if(found_collision)
					{
						ASSERT(t < nearest_t);
					}

					found_collision = true;
					nearest_dist_to_collision = dist_to_collision;
					nearest_collision_point = collision_point;
					nearest_t = t;
				}
			}
		}

#if 1
		for(i = 0; i < 8; i++)
		{
			if(m_children[i])
			{
				m_children[i]->CollideDynamicEllipsoidHelperHelper(origin, velocity, r, 
					found_collision, nearest_dist_to_collision, nearest_collision_point, nearest_t);
			}
		}
#endif
	}
}

bool 
OctNode::CollideDynamicEllipsoidHelper(const Vector3 &in_src, Vector3 &out_dst, 
									Vector3 r, Vector3 *out_n, Vector3 *out_poc)
{
	// make sure we're moving
	if((in_src - out_dst).Length() < COLLISION_EPSILION)
	{
		out_dst = in_src;
		return false;
	}

	Vector3 original_dst = out_dst;

	// compute a matrix transform that puts the triangles in unit sphere space
	real sx = (1.0f / r.x);
	real sy = (1.0f / r.y);
	real sz = (1.0f / r.z);

	// multiple simultaneous collisions possible
	bool found_collision = false;
	double nearest_dist_to_collision;
	Vector3 nearest_collision_point;
	double nearest_t;

	Vector3 origin = in_src;
	origin.x *= sx; origin.y *= sy; origin.z *= sz;

	Vector3 velocity = out_dst - in_src;
	velocity.x *= sx; velocity.y *= sy; velocity.z *= sz;

	Vector3 original_dst_espace = original_dst;
	original_dst_espace.x *= sx; original_dst_espace.y *= sy; original_dst_espace.z *= sz;
	
	CollideDynamicEllipsoidHelperHelper(origin, velocity, r, 
		found_collision, nearest_dist_to_collision, nearest_collision_point, nearest_t);

	if(found_collision)
	{
		Vector3 v = velocity;
		v.Normalize();

		Vector3 intersect_point = nearest_collision_point;
		Vector3 new_base_point = origin;

		v *= nearest_dist_to_collision;
		new_base_point += v;

		Vector3 slide_plane_origin = intersect_point;
		Vector3 slide_plane_normal = new_base_point - intersect_point;
		slide_plane_normal.Normalize();

		Vector3 displacement = slide_plane_normal;
		displacement *= COLLISION_EPSILION;
		new_base_point += displacement;

		real dist_to_slide_plane = slide_plane_normal.Dot(original_dst_espace - new_base_point);

//		DEBUGMSG("Original vel: %.4f, ToSlide: %.4f", 
//			(original_dst_espace-origin).Length(), dist_to_slide_plane);
		Vector3 new_dst = original_dst_espace - slide_plane_normal * dist_to_slide_plane;

		Vector3 new_r3_src = new_base_point;
		new_r3_src.x /= sx;
		new_r3_src.y /= sy;
		new_r3_src.z /= sz;

		Vector3 new_r3_dst = new_dst;
		new_r3_dst.x /= sx;
		new_r3_dst.y /= sy;
		new_r3_dst.z /= sz;

#if 1
		g_collision_recursion_depth--;
		if(g_collision_recursion_depth > 0)
		{
			Vector3 new_out_n;
			Vector3 new_out_poc;
			bool b = CollideDynamicEllipsoidHelper(
				new_r3_src, new_r3_dst, r, &new_out_n, &new_out_poc);

			new_r3_src = new_r3_dst;
		}
#endif

		out_dst = new_r3_src;

		(*out_n) = slide_plane_normal;
		(*out_n).x /= sx;
		(*out_n).y /= sy;
		(*out_n).z /= sz;
		(*out_n).Normalize();

		(*out_poc) = nearest_collision_point;
		(*out_poc).x /= sx;
		(*out_poc).y /= sy;
		(*out_poc).z /= sz;

//		DEBUGMSG("COL");
//		out_dst.Dump();
	}

	return found_collision;
}


bool
OctNode::PolygonContains(Vector3 &p, Vector3 &v1, Vector3 &v2, Vector3 &v3)
{
	// if the point is outside any edge, it's outside the poly
	// edge 1
	Vector3 v1_v2 = v2 - v1;
	Vector3 v1_p = p - v1;
	if(v1_v2.x * v1_p.y - v1_p.x * v1_v2.y < 0.0f)//-COLLISION_EPSILION)
		return false;

	// edge 2
	v1_v2 = v3 - v2;
	v1_p = p - v2;
	if(v1_v2.x * v1_p.y - v1_p.x * v1_v2.y < 0.0f)//-COLLISION_EPSILION)
		return false;

	// edge 3
	v1_v2 = v1 - v3;
	v1_p = p - v3;
	if(v1_v2.x * v1_p.y - v1_p.x * v1_v2.y < 0.0f)//-COLLISION_EPSILION)
		return false;


	return true;
}

}

}

#ifdef USE_SERIALIZATION

#include "Stream.h"

namespace wkg 
{

namespace engine 
{

bool 
OctNode::Load(Stream* stream)
{
	uint32 i;
	uint8 children;
	uint32 numVerts;
	Vector3 vert;
	bool result;

	result = stream->Read(&children)
		&& AABB::Load(stream)
		&& stream->Read(&numVerts);

	for (i = 0; result && i < numVerts; i++)
	{
		result = vert.Load(stream);
		if (result)
			m_verts.push_back(vert);
	}

	for (i = 0; result && i < 8; i++)
	{
		if (children & (1 << i))
		{
			m_children[i] = new OctNode();
			if (m_children[i])
				result = m_children[i]->Load(stream);
			else
				result = false;
		}
	}

	return result;
}

bool 
OctNode::Save(Stream* stream) const
{
	uint32 i;
	uint8 children;
	uint32 numVerts;
	bool result;

	children = 0;

	for (i = 0; i < 8; i++)
		if (m_children[i])
			children |= (1 << i);

	numVerts = m_verts.size();

	result = stream->Write(children)
		&& AABB::Save(stream)
		&& stream->Write(numVerts);

	for (i = 0; result && i < numVerts; i++)
		result = m_verts[i].Save(stream);

	for (i = 0; result && i < 8; i++)
		if (m_children[i])
			result = m_children[i]->Save(stream);

	return result;
}

}

}

#endif

