/******************************************************************************
 shadowmapmanager.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "singleton.h"
#include "node.h"
namespace wkg
{

namespace engine
{

class Light;

class ShadowMapManager : public Singleton<ShadowMapManager>, public Node
{
public:
	ShadowMapManager();
	~ShadowMapManager();

	virtual void DFSRender() const;

	void CreateShadowMap(Light *l) const;

	void UpdateShadowMap(Light *l) const;
	Texture *GetShadowMap(Light *l, int32 direction) const;

	Vector4 GetCurrLightRange();
	Vector4 GetLightRange(Light *l) const;

	Matrix44 GetTextureProjection(Light *l, int32 direction) const;
	Matrix44 GetProjection(Light *l) const;
	Matrix44 GetViewTransform(Light *l, int32 direction) const;

	enum { SM_DOWN = 1, SM_LEFT, SM_RIGHT, SM_BACK, SM_FORWARD };

protected:
	Texture *CreateShadowMap() const;
	void SaveRenderState() const;
	void SetRenderState(Light *l, int32 direction) const;

	void RestoreRenderState() const;

protected:

	mutable std::map<Light *, std::map<int32, Texture *> > m_shadowmaps;

	mutable LPDIRECT3DSURFACE9 m_old_target;
	mutable D3DVIEWPORT9 m_old_viewport;
	mutable Matrix44 m_old_proj;
	mutable Matrix44 m_old_view;

	mutable Vector4 m_curr_light_range;

};

}

}
