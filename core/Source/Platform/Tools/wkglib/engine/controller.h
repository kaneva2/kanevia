/******************************************************************************
 controller.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "controllable.h"

namespace wkg
{

namespace engine
{

class Controller
{
public:
	Controller()
	{
	}

	virtual ~Controller()
	{
		m_changed = true;
	}

	void AddControllable(Controllable *controllable)
	{
		m_controllables.push_back(controllable);
	}

	void ClearControllables()
	{
		m_controllables.clear();
	}

	void RemoveControllable(Controllable *controllable)
	{
		ControllableIterator itor = m_controllables.begin();
		ControllableIterator iend = m_controllables.end();
		for(; itor < iend; itor++)
		{
			if(*itor == controllable)
			{
				// assumes only one instance of this controllable
				//  in the vector
				m_controllables.erase(itor);
				return;
			}
		}
	}

	void ControllerUpdate()
	{

		for(uint32 i = 0; i < m_controllables.size();)
		{
			m_changed = false;

			m_controllables[i]->ControllableUpdate();

			if(!m_changed) 
				i++;
		}
	}

protected:
	static bool m_changed;
	ControllableVector m_controllables;
};


}

}
