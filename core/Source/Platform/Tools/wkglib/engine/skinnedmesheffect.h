/******************************************************************************
 skinnedmesheffect.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "effectshader.h"

namespace wkg
{

namespace engine
{

class SkinnedMesh;

class SkinnedMeshEffect : public EffectShader
{
public:
	SkinnedMeshEffect();
	virtual ~SkinnedMeshEffect();

	virtual void Prepare(Mesh *mesh);
	virtual void PrepareSection(MeshSection *section);

protected:
	SkinnedMesh *m_mesh;
};

}

}
