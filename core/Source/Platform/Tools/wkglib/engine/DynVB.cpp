/******************************************************************************
 DynVB.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "DynVB.h"

namespace wkg {
namespace engine {

DynVB::DynVB()
{
	pVB = 0;
	vertSize = 0;
	maxVerts = maxBatches = vertsPerBatch = startByte = vbNext = 0;
	pBatch = 0;
}

DynVB::~DynVB()
{
	ASSERT(0 == pBatch);	// make sure the vert buf isn't still locked

	// do nothing... we don't own the vert buf
}

bool 
DynVB::Prepare(LPDIRECT3DVERTEXBUFFER9 pVB, uint8 vertSize, int32 numBatches)
{
	D3DVERTEXBUFFER_DESC desc;

	ASSERT(!this->pVB && !pBatch && !this->vertSize && !maxVerts && !maxBatches && !vertsPerBatch && !startByte && !vbNext);

	if (pVB && D3D_OK == pVB->GetDesc(&desc) && desc.Usage & D3DUSAGE_DYNAMIC && desc.Usage & D3DUSAGE_WRITEONLY)
	{
		this->pVB = pVB;
		this->vertSize = vertSize;
		maxVerts = desc.Size / vertSize;
		maxBatches = numBatches;
		vertsPerBatch = maxVerts / maxBatches;
		ASSERT(vertsPerBatch >= 3);

		startByte = maxVerts;	// force lock at beginning of vb
		vbNext = vertsPerBatch;	// ... no room for more
		pBatch = 0;				// ... none queued

		return true;
	}
	else
	{
		pVB = 0;
		vertSize = 0;
		maxVerts = maxBatches = vertsPerBatch = startByte = vbNext = 0;
		pBatch = 0;

		return false;
	}
}

bool
DynVB::CanAppend(int32 numVerts, uint8** ppVert)
{
	if (vbNext + numVerts >= vertsPerBatch)
	{
		uint32 lockFlags;

		if (pBatch)
		{
			*ppVert = 0;
			return false;
		}

		startByte += vertsPerBatch;		// move past the entire batch (regardless of whether it was fully used) to simplify locking
		vbNext = 0;						// (i.e. lock only on batch boundaries)

		if (startByte + vertsPerBatch <= maxVerts)
			lockFlags = D3DLOCK_NOOVERWRITE;			// next batch fits within this vert buf
		else
		{
			startByte = 0;								// entire vert buf is full...get a new one and lock at the beginning
			lockFlags = D3DLOCK_DISCARD;
		}

		if (D3D_OK == pVB->Lock(startByte * vertSize, vertsPerBatch * vertSize, (void**)&pBatch, lockFlags))
		{
			*ppVert = pBatch;
			vbNext += numVerts;

			return true;
		}
		else	// d3d problem...probably lost device; try to start new vert buf on next attempt
		{
			*ppVert = 0;
			startByte = maxVerts;	// force lock at beginning of vb
			vbNext = vertsPerBatch;	// ... no room for more
			pBatch = 0;				// ... none queued

			return false;
		}
	}
	else
	{
		*ppVert = pBatch + vbNext * vertSize;
		vbNext += numVerts;

		return true;
	}
}

bool
DynVB::BatchReady(int32* pStartVert, int32* pNumVerts)
{
	if (pBatch)
	{
		pVB->Unlock();
		*pStartVert = startByte;
		*pNumVerts = vbNext;

		vbNext = vertsPerBatch;		// move to next batch
		pBatch = 0;					// signal that we've already sent the last batch

		return true;
	}

	*pStartVert = 0;
	*pNumVerts = 0;

	return false;
}

}}