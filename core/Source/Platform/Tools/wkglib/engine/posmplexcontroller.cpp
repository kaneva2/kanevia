/******************************************************************************
 posmplexcontroller.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "posmplexcontroller.h"
#include "vector3.h"

namespace wkg
{

namespace engine
{

PosMPlexController::PosMPlexController() : 
	TimedUpdatable(),
	m_target(0),
	m_transitioning(false),
	m_first(true)
{
	StartUpdates();
}

PosMPlexController::~PosMPlexController()
{
	for(uint32 i = 0; i < m_positions.size(); i++)
	{
		delete m_positions[i];
	}
	m_positions.clear();
}

void 
PosMPlexController::TimedUpdate(int32 mspf)
{
	if(m_transitioning)
	{
		m_transition_time_left -= mspf;
		if(m_transition_time_left < 0.0f)
		{
			m_transition_time_left = 0.0f;
			m_transitioning = false;
		}
	}
}

void 
PosMPlexController::ControllableUpdate()
{
	if(!m_target) return;
	if(!m_positions.size()) return;

	Vector3 target;

	// iterate over our entries and choose the one with the highest
	//  priority
	int32 highest = -1;
	int32 highest_idx = -1;
	for(uint32 i = 0; i < m_priority.size(); i++)
	{
		if(m_playing[i] && m_priority[i] > highest)
		{
			highest_idx = i;
			highest = m_priority[i];
		}
	}

	if(highest_idx >= 0)
	{
		ASSERT(highest_idx < m_positions.size());

		target = (*m_positions[highest_idx]);

	}

	if(m_transitioning)
	{
		real t = (m_transition_time - m_transition_time_left) / m_transition_time;
		target = m_transition_begin + (target - m_transition_begin) * t;
	}

	(*m_target) = target;

	ControllerUpdate();
}

int32 
PosMPlexController::AddEntry(int32 priority)
{
	Vector3 *new_pos = new Vector3();
	ASSERT(new_pos);
	m_positions.push_back(new_pos);
	m_priority.push_back(priority);
	m_playing.push_back(false);
	return m_positions.size() - 1;
}

Vector3 *
PosMPlexController::GetEntry(int32 idx)
{
	ASSERT(idx >= 0 && idx < m_positions.size());	// otherwise it'll *add* an entry
	return m_positions[idx];
}

}

}
