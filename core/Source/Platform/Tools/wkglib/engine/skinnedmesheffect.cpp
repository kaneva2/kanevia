/******************************************************************************
 skinnedmesheffect.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "skinnedmesh.h"
#include "device.h"
#include "meshopts.h"
#include "matrix44.h"
#include "vector4.h"
#include "meshtypes.h"
#include "mesh.h"
#include "meshsection.h"
#include "matrixpalettenode.h"
#include "skinnedvsconst.h"
#include "lightmanager.h"
#include "cameramanager.h"
#include "light.h"
#include "shadowmapmanager.h"
#include "camera.h"
#include "skinnedmesheffect.h"

namespace wkg
{

namespace engine
{

SkinnedMeshEffect::SkinnedMeshEffect() : EffectShader(), m_mesh(0)
{
}

SkinnedMeshEffect::~SkinnedMeshEffect()
{
}

void 
SkinnedMeshEffect::PrepareSection(MeshSection *section)
{
	if(m_mesh)
	{
		// load the matrix palette
		for(int32 b = 0; b < MAX_BONES_PER_SECTION; b++)
		{
			Matrix44 m = m_mesh->GetMatrixPalette()->GetMatrix(section->m_bones[b]);

			Device::Instance()->SetVertexShaderConstantF(100 + BONEMATRIX0 + b * 3, m, 3);

			m.Transpose();
			Device::Instance()->SetVertexShaderConstantF(BONEMATRIX0 + b * 3, m, 3);
		}
	}
}

void 
SkinnedMeshEffect::Prepare(Mesh *sm)
{
	SkinnedMesh *mesh = dynamic_cast<SkinnedMesh *>(sm);
	if(!mesh)
		return;

	m_mesh = mesh;

	// load the constants
	Vector4 v(0.5f, 1.0f, BONEMATRIX0, 3.0f);
	SetVector("Consts", v);

	// find the closest light
	Vector3 our_pos(0.0f, 0.0f, 0.0f);
	if(mesh->GetSkeletonRoot())
	{
		Matrix44 mesh_posm = mesh->GetSkeletonRoot()->GetLocalToWorld();
		our_pos = Vector3(mesh_posm._41, mesh_posm._42, mesh_posm._43);
	}

	Vector4 l(0.0f, 0.0f, 0.0f, 0.1f);
	Light *light = LightManager::Instance()->GetClosestLight(our_pos);
	if(light)
	{
		l = Vector4(
			light->GetPosition().x, 
			light->GetPosition().y, 
			light->GetPosition().z,
			0.1f);
	}

	SetVector("LightPos", l);

	Matrix44 world, view, proj;
	Device::Instance()->GetTransform(D3DTS_WORLD, &world);
	Device::Instance()->GetTransform(D3DTS_VIEW, &view);
	Device::Instance()->GetTransform(D3DTS_PROJECTION, &proj);

	// and load the transpose of the view-proj matrix
	SetMatrix("World", world);

	Matrix44 vp;
	vp = view * proj;
	SetMatrix("ViewProj", vp);

	Matrix44 wvp;
	wvp = world * view * proj;
	SetMatrix("WorldViewProj", wvp);


	Vector4 light_range = ShadowMapManager::Instance()->GetCurrLightRange();
	SetVector("LightRange", light_range);

}

}

}