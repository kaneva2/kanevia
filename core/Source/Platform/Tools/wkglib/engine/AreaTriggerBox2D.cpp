/******************************************************************************
 AreaTriggerBox2D.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "WorldObject.h"
#include "WorldObjectList.h"
#include "Vector3.h"
#include "AreaTriggerBox2D.h"

namespace wkg 
{

namespace engine
{

AreaTriggerBox2D::AreaTriggerBox2D(std::string name, int32 waitingFor) : 
	AreaTrigger(name, waitingFor),
	m_size(0),
	m_position(Vector3(0.0f, 0.0f, 0.0f))
{
}

AreaTriggerBox2D::AreaTriggerBox2D(std::string name, int32 waitingFor, real size, Vector3& v) : 
	AreaTrigger(name, waitingFor),
	m_size(size),
	m_position(v)
{
}

AreaTriggerBox2D::~AreaTriggerBox2D()
{
}

void
AreaTriggerBox2D::OnUpdate(uint32 ms)
{
	AreaTrigger::OnUpdate(ms);
}

WorldObject*
AreaTriggerBox2D::CollisionDetected()
{
	WorldObject* wo = WorldObjectList::Instance()->GetWorldObject(m_waitingFor);
	Vector3 pos = wo->GetPosition();
	if(wo &&
		(pos.x >= (m_position.x - m_size)) && (pos.x <= (m_position.x + m_size)) &&
		(pos.y >= (m_position.y - m_size)) && (pos.y <= (m_position.y + m_size)) &&
		(pos.z >= (m_position.z - m_size)) && (pos.z <= (m_position.z + m_size)))
	{
		m_size = -1;
		return wo;
	}
	return 0;
}


}
}