/******************************************************************************
 positionkeyframecontroller.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "positionkeyframecontroller.h"

namespace wkg
{

namespace engine
{

PositionKeyframeController::PositionKeyframeController() :
	Controllable(),
	Controller(),
	m_frame(0), 
	m_next_frame(0),
	m_partial_frame(0.0f),
	m_position(0)
{
}

PositionKeyframeController::~PositionKeyframeController()
{
}

void 
PositionKeyframeController::ControllableUpdate()
{
	if(!m_position) return;
	// update the position based on the keyframe list we have

	Vector3 curr_pos;
	Vector3 next_pos;

	bool curr_found = false;
	bool next_found = false;
	
	if(m_frame == -1)
	{
		// special case for transitions
		curr_found = true;
		curr_pos = m_begin_transition;
	}

	// for now, let's not do any keyframe interpolation
	WKGPositionKeyframeIterator itor = m_position_keyframes.begin();
	WKGPositionKeyframeIterator iend = m_position_keyframes.end();

	for(; itor < iend; itor++)
	{
		if((*itor)->m_frame == m_frame)
		{
			curr_found = true;
			curr_pos = (*itor)->m_position;

			if(next_found)
				break;
		}
		if((*itor)->m_frame == m_next_frame)
		{
			next_found = true;
			next_pos = (*itor)->m_position;

			if(curr_found)
				break;
		}
	}

	if(curr_found && next_found && itor != iend)
	{
		(*m_position) = curr_pos + ((next_pos - curr_pos) * m_partial_frame);
		ControllerUpdate();
	}
}

}

}