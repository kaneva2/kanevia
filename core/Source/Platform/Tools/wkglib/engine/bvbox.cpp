/******************************************************************************
 bvbox.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "obb.h"
#include "matrix44.h"
#include "quaternion.h"
#include "sphere.h"
#include "worldobject.h"
#include "intersect.h"
#include "collision.h"
#include "bvoctree.h"
#include "bvsphere.h"
#include "bvbox.h"

namespace wkg
{

namespace engine
{

BVBox::BVBox(WorldObject *wo, Vector3 &size) : 
	BoundingVolume(wo),
	m_size(size)
{
}

BVBox::~BVBox()
{
}

Collision *
BVBox::Collide(BoundingVolume *other, uint32 ms)
{
	BVSphere *bvsphere;
	bvsphere = dynamic_cast<BVSphere *>(other);
	if(bvsphere && bvsphere->GetRadius() < 6.0f)
	{
		OBB obb;

		obb.SetExtent(0, m_size.x/2.0f);
		obb.SetExtent(1, m_size.y/2.0f);
		obb.SetExtent(2, m_size.z/2.0f);

		Matrix44 m = (Matrix44)(m_world_object->GetOrientation());
		obb.SetAxis(0, Vector3(m._11, m._12, m._13));
		obb.SetAxis(1, Vector3(m._21, m._22, m._23));
		obb.SetAxis(2, Vector3(m._31, m._32, m._33));

		obb.SetCenter(m_world_object->GetPosition() + m_world_object->GetBVOffset());

		Vector3 from(bvsphere->GetWorldObject()->GetPosition());
		Vector3 to(from);

		from += bvsphere->GetWorldObject()->GetBVOffset();
		to += bvsphere->GetWorldObject()->GetBVOffset();

		to += (bvsphere->GetWorldObject()->GetVelocity() * (real)ms) / 1000.0f;

		LineSegment seg;
		seg.SetEndPoints(from, to);

		if(Intersect::SegOBB(seg, obb))
		{
			Vector3 intersection_points[2];
			int32 num_intersections;

			if(Intersect::FindIntersection(seg, obb, num_intersections, intersection_points))
			{
				DEBUGMSG("INTERSECTION");
				Collision *c = new Collision;

				c->m_poc = intersection_points[0];
				c->m_dst = intersection_points[0];
				c->m_n = (to - from).Normalize();
				c->m_t = (intersection_points[0] - from).Length() / (from - to).Length();

				return c;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}

//	DEBUGMSG("don't know how to collide provided bv types!");

//	ASSERT(0);

	return 0;
}

}

}

