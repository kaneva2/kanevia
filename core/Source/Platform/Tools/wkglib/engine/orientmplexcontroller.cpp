/******************************************************************************
 orientmplexcontroller.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "quaternion.h"
#include "orientmplexcontroller.h"

namespace wkg
{

namespace engine
{

OrientMPlexController::OrientMPlexController() : 
	TimedUpdatable(),
	m_target(0),
	m_transitioning(false),
	m_first(true)
{
	StartUpdates();
}

OrientMPlexController::~OrientMPlexController()
{
	for(uint32 i = 0; i < m_orientations.size(); i++)
	{
		delete m_orientations[i];
	}
	m_orientations.clear();
}

void 
OrientMPlexController::TimedUpdate(int32 mspf)
{
	if(m_transitioning)
	{
		m_transition_time_left -= mspf;
		if(m_transition_time_left < 0.0f)
		{
			m_transition_time_left = 0.0f;
			m_transitioning = false;
		}
	}
}

void 
OrientMPlexController::ControllableUpdate()
{
	if(!m_target) return;
	if(!m_orientations.size()) return;

	// iterate over our entries and choose the one with the highest
	//  priority
	int32 highest = -1;
	int32 highest_idx = -1;
	for(uint32 i = 0; i < m_priority.size(); i++)
	{
		if(m_playing[i] && m_priority[i] > highest)
		{
			highest_idx = i;
			highest = m_priority[i];
		}
	}

	Quaternion target;
	if(highest_idx >= 0)
	{
		ASSERT(highest_idx < m_orientations.size());

		target = (*m_orientations[highest_idx]);
	}

	if(m_transitioning)
	{
		real t = (m_transition_time - m_transition_time_left) / m_transition_time;
		target = m_transition_begin.Slerp(target, t);
	}

	(*m_target) = target;

	ControllerUpdate();
}

int32 
OrientMPlexController::AddEntry(int32 priority)
{
	Quaternion *new_orient = new Quaternion();
	m_orientations.push_back(new_orient);
	m_priority.push_back(priority);
	m_playing.push_back(false);
	return m_orientations.size() - 1;
}

Quaternion *
OrientMPlexController::GetEntry(int32 idx)
{
	ASSERT(idx >= 0 && idx < m_orientations.size());	// otherwise it'll *add* an entry
	return m_orientations[idx];
}

}

}
