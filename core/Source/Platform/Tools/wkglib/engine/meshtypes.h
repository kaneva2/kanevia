/******************************************************************************
 meshtypes.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "wkgvertex.h"
#include "wkgface.h"
#include "wkgindex.h"
#include "wkgcolor.h"
#include "wkgmaterial.h"
#include "wkgbone.h"
#include "wkgkeyframe.h"
#include "wkgmeshsection.h"
#include "wkgboneweight.h"



