/******************************************************************************
 KeyFrame.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#ifndef _KEYFRAME_H_
#define _KEYFRAME_H_

namespace wkg {

class KeyFrame
{
    public:
		KeyFrame(int32 msecs) : m_msecs(msecs) {}

	    int32 GetDuration() const	{ return m_msecs; }
	    void SetDuration(int32 ms)  { m_msecs = ms; }

    protected:
		int32	m_msecs;
};


template <typename T> class KeyFrameTemplate : public KeyFrame
{
    public:
	    KeyFrameTemplate(const T& data, int32 msecs);

		const T& GetData() const  { return m_kfData; }

    protected:
		T m_kfData;
};


template <typename T>
KeyFrameTemplate<T>::KeyFrameTemplate(const T& data, int32 msecs)
:	KeyFrame(msecs),
	m_kfData(data)
{
}

}
#endif

