/******************************************************************************
 texturefactory.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <string>
#include "singleton.h"
#include "factory.h"

namespace wkg 
{

class Texture;

class TextureFactory : 
	public Singleton <TextureFactory>, 
	public Factory<Texture, string>
{
    public:
	    TextureFactory();
	    virtual ~TextureFactory();

		Texture* GetCubeTexture(const string& filename);

    protected:
        virtual Texture* Create(const string& filename) const;

	protected:
		mutable bool createCube;
};

}

