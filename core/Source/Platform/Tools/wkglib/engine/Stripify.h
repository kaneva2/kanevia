/******************************************************************************
 Stripify.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg {

class Stripify
{
	public:
		static bool Init();		// once for entire app
		static void Uninit();	// once for entire app (ok to call even if Init() failed)

		struct PrimitiveGroup;

		static void SetStitchStrips(bool bStitchStrips);	// create one big strip (true) or many small strips (false)...default = true
		static void SetMinStripSize(uint32 minSize);		// in triangles...anything below this will be gathered into a separate list...default = 0
		static void SetListsOnly(bool bListsOnly);			// create optimized lists instead of strips...default = false

		static void GenerateStrips(const uint16* in_indices, uint32 in_numIndices, PrimitiveGroup** primGroups, uint16* numGroups);	// you are responsible for deleting primGroups

		static void RemapIndices(PrimitiveGroup* in_out_primGroups, uint16 numGroups, uint16 numVerts, 
			uint8* lockedVertBuf, uint32 vbVertSize,
			uint16* in_out_indices[]);						// improve spatial locality in vert buffer; note: in_out_indices is array of index pointers--one pointer per PrimitiveGroup

	public:
		struct PrimitiveGroup
		{
			public:
				enum Type { LIST, STRIP, FAN };

				PrimitiveGroup();
				~PrimitiveGroup();

			public:
				Type type;
				uint32 numIndices;
				uint16* indices;
		};
};

inline 
Stripify::PrimitiveGroup::PrimitiveGroup() 
	: type(STRIP), numIndices(0), indices(0) 
{
}

inline 
Stripify::PrimitiveGroup::~PrimitiveGroup()
{
	delete [] indices;
	indices = 0;
}

}