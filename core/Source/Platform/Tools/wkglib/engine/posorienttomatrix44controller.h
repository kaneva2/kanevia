/******************************************************************************
 posorienttomatrix44controller.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "controllable.h"
#include "controller.h"
#include "vector3.h"
#include "quaternion.h"
#include "matrix44.h"

namespace wkg
{

namespace engine
{

class PosOrientToMatrix44Controller : public Controllable, public Controller
{
public:
	PosOrientToMatrix44Controller();
	virtual ~PosOrientToMatrix44Controller();

	void SetTarget(Matrix44 *t) { m_matrix = t; }

	virtual void ControllableUpdate();

	Matrix44 *m_matrix;

	Vector3 m_position;
	Quaternion m_orientation;
};

}

}

