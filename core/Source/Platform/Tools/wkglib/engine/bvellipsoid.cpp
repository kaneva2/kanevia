/******************************************************************************
 bvellipsoid.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "worldobject.h"
#include "intersect.h"
#include "collision.h"
#include "bvoctree.h"
#include "bvsphere.h"
#include "ellipsoid.h"
#include "collisionmanager.h"
#include "bvellipsoid.h"

namespace wkg
{

namespace engine
{

BVEllipsoid::BVEllipsoid(WorldObject *wo, Vector3 radius) : 
	BoundingVolume(wo),
	m_radius(radius)
{
}

BVEllipsoid::~BVEllipsoid()
{
}

Collision *
BVEllipsoid::Collide(BoundingVolume *other, uint32 ms)
{
	BVSphere *bvsphere = dynamic_cast<BVSphere *>(other);

	if(bvsphere && bvsphere->GetRadius() < 6.0f)
	{
		Ellipsoid e;

//		Matrix44 m = (Matrix44)(m_world_object->GetOrientation());
//		e.SetA(m);
		// assume an axis aligned ellipsoid for now

		Matrix44 m;
		m.Identity();
		m.Scale(1.0f/m_radius.x, 1.0f/m_radius.y, 1.0f/m_radius.z);
		e.SetA(m);

		e.SetCenter(m_world_object->GetPosition() + m_world_object->GetBVOffset());

		Vector3 from(bvsphere->GetWorldObject()->GetPosition());
		Vector3 to(from);

		from += bvsphere->GetWorldObject()->GetBVOffset();
		to += bvsphere->GetWorldObject()->GetBVOffset();

		to += (bvsphere->GetWorldObject()->GetVelocity() * (real)ms) / 1000.0f;


#if 0
		Vector3 delta = (to - from);
		delta /= 32.0f;
		for(int32 i = 0; i < 32; i++)
		{
			CollisionManager::Instance()->AddRenderCollision(from + delta * (real)i, 0xff00ff00);
		}
#endif

		LineSegment seg;
		seg.SetEndPoints(from, to);

		Vector3 intersection_points[2];
		int32 num_intersections;

		if(Intersect::FindIntersection(seg, e, num_intersections, intersection_points))
		{
//			DEBUGMSG("INTERSECTION");
			Collision *c = new Collision;

			c->m_poc = intersection_points[0];
			c->m_dst = intersection_points[0];
			c->m_n = (to - from).Normalize();
			c->m_t = (intersection_points[0] - from).Length() / (from - to).Length();

			CollisionManager::Instance()->AddRenderCollision(c->m_poc, 0xffff0000);

			return c;
		}
		else
		{
			return 0;
		}
	}
	else if(bvsphere)
	{
		return 0;
	}

	BVEllipsoid *bve = dynamic_cast<BVEllipsoid *>(other);
	if(bve)
	{
		return 0;
	}

	BVOctree *bvot = dynamic_cast<BVOctree *>(other);
	if(other)
	{
		return other->Collide(this, ms);
	}

	DEBUGMSG("don't know how to collide provided bv types!");

	ASSERT(0);

	return 0;
}

}

}

