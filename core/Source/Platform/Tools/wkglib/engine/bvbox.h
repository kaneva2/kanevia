/******************************************************************************
 bvbox.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "boundingvolume.h"
#include "sphere.h"

namespace wkg
{

namespace engine
{

class WorldObject;

class BVBox : public BoundingVolume
{
public:
	BVBox(WorldObject *wo, Vector3 &size);
	virtual ~BVBox();

	const Vector3 &GetSize() const { return m_size; }
	virtual Collision *Collide(BoundingVolume *other, uint32 ms);

	virtual real GetBVSphereRadius() 
	{ 
		if(m_size.x > m_size.y && m_size.x > m_size.z) return m_size.x;
		if(m_size.y > m_size.z) return m_size.y;
		return m_size.z;
	}

protected:
	Vector3 m_size;
};

}

}

