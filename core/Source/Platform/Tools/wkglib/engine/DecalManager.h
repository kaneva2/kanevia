/******************************************************************************
 DecalManager.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#ifndef _DECALMANAGER_H
#define _DECALMANAGER_H

#include <list>
#include <map>
#include "vector3.h"
#include "singleton.h"

namespace wkg {

class Quaternion;

namespace engine {

class Texture;
class WorldObject;

// Todo - add memory management and fade out for decal death

class DecalManager : public Singleton<DecalManager>
{
	public:
		class Decal
		{
			public:
				uint32 lifetime;
				Vector3 pos;
				Vector3 verts[4];
				uint8 alpha;
				Texture* pTexture;
				WorldObject* pRefObj;
				bool bPermanent;
		};

		typedef std::list<Decal*> DecalList;
		typedef DecalList::iterator DecalIterator;
		typedef DecalList::const_iterator DecalHandle;

		DecalManager();
		virtual ~DecalManager();

		void AddDecal(const char* szTextureName,
					  const Vector3& pos,
					  const Vector3& normal,
					  real width = 1.0f,
					  real height = 1.0f,
					  bool bPermanent = false,
					  WorldObject* pRefObj = NULL,
					  DecalHandle* pHandle = NULL);
		void RemoveDecal(DecalHandle& handle);
		void ResizeDecal(DecalHandle& handle, real width, real height);
		void OrientDecal(DecalHandle& handle, const Quaternion& orient);
		void ClearDecals();
		void SetDecalLifetime(uint32 maxLife)  { m_maxLife = maxLife; }
		void Update(int32 msElapsed);
		void Render();

		void SetVBIB(PDIRECT3DVERTEXBUFFER9, PDIRECT3DINDEXBUFFER9);

	protected:
		typedef std::map<Texture*, DecalList*> TexDecalMap;
		typedef TexDecalMap::iterator TexDecalIterator;

		TexDecalMap				m_texDecalMap;
		uint32					m_maxLife;

		PDIRECT3DVERTEXBUFFER9	m_VB;
		PDIRECT3DINDEXBUFFER9	m_IB;
};

inline void 
DecalManager::SetVBIB(PDIRECT3DVERTEXBUFFER9 vb, PDIRECT3DINDEXBUFFER9 ib)
{
	m_VB = vb;
	m_IB = ib;
}

}

}

#endif
