/******************************************************************************
 skeletalanimation.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "quaternion.h"
#include "vector3.h"
#include "animation.h"
#include "meshtypes.h"
#include "node.h"
#include "xformnode.h"
#include "wkafile.h"
#include "posmplexcontroller.h"
#include "orientmplexcontroller.h"
#include "orientationkeyframecontroller.h"
#include "positionkeyframecontroller.h"
#include "posorienttomatrix44controller.h"
#include "wkafile.h"
#include "animmotionnotify.h"

#include "skeletalanimation.h"


namespace wkg
{

namespace engine
{

std::map<std::string, WKAFile *> SkeletalAnimation::m_wka_cache;

SkeletalAnimation::SkeletalAnimation() : 
	m_is_playing(false),
	m_frame_start(0),
	m_frame_end(0),
	m_velocity(0.0f, 0.0f, 0.0f),
	m_mode(Animation::PLAY_NONE)
{
}

SkeletalAnimation::~SkeletalAnimation()
{
	for(uint32 i = 0; i < m_animations.size(); i++)
	{
		delete m_animations[i];
	}

	m_animations.clear();
	m_name = "";
	m_is_playing = false;
}

bool 
SkeletalAnimation::Create(const char *name, const char *filename)
{
	m_name = name;

	WKAFile *wka = m_wka_cache[filename];
	if(!wka)
	{
		wka = new WKAFile();
		if(!wka->Load(filename)) return false;
		m_wka_cache[filename] = wka;
	}

	WKGBoneWeight *bone_weights = wka->GetBoneWeights();

	// create one Animation object for each bone
	int32 num_bones = wka->GetNumBoneWeights();
	for(int32 i = 0; i < num_bones; i++)
	{
		Animation *a = new Animation();
		ASSERT(a);
		a->SetName(bone_weights[i].m_name);
		m_animations.push_back(a);
	}

	int32 num_keyframes = wka->GetNumKeyframes();
	if(num_keyframes)
	{
		WKGKeyframe *keyframes = wka->GetKeyframes();

		int32 frame_start = 0xffff;
		int32 frame_end = 0;
		for(int32 kf = 0; kf < num_keyframes; kf++)
		{
			if(keyframes[kf].m_frame < frame_start)
				frame_start = keyframes[kf].m_frame;

			if(keyframes[kf].m_frame > frame_end)
				frame_end = keyframes[kf].m_frame;
		}

		ASSERT(frame_start != 0xffff && 
			frame_start < num_keyframes && 
			frame_end < num_keyframes);

		m_frame_start = frame_start;
		m_frame_end = frame_end;
		m_last_frame = frame_start;

		int32 frame_num = frame_end - frame_start + 1;

		Vector3 base_position;
		m_offsets.clear();
		m_offsets.reserve(frame_num);

		// find our base position and offsets
		bool found = false;
		for(kf = 0; kf < num_keyframes; kf++)
		{
			if((keyframes[kf].m_frame == frame_start) &&
			   (!stricmp(keyframes[kf].m_name, "ROOT")))
			{
				base_position = keyframes[kf].m_position;
				found = true;
				break;
			}
		}

		if(found)
		{
			for(kf = 0; kf < num_keyframes; kf++)
			{
				if(!stricmp(keyframes[kf].m_name, "ROOT"))
				{
					if(keyframes[kf].m_frame == frame_start)
					{
						m_offsets[keyframes[kf].m_frame - frame_start] = Vector3(0.0f, 0.0f, 0.0f);
					}
					else
					{	
						ASSERT(keyframes[kf].m_frame - frame_start < frame_num);
//						m_offsets[keyframes[kf].m_frame - frame_start] = 
//							keyframes[kf].m_position - base_position;
						m_offsets[keyframes[kf].m_frame - frame_start] = Vector3(0.0f, 0.0f, 0.0f);

					}
				}
			}
		}
		else
		{
			for(kf = 0; kf < frame_num; kf++)
			{
				m_offsets[kf] = Vector3(0.0f, 0.0f, 0.0f);
			}
		}

		for(kf = frame_num - 1; kf > 0; kf--)
		{
			m_offsets[kf] = m_offsets[kf] - m_offsets[kf - 1];
		}
		m_offsets[0] = m_offsets[frame_num-1];

		for(int32 i = 0; i < num_bones; i++)
		{
			// tell the position and orientation multiplexor to 
			//  add a new entry for this new animation
			m_animations[i]->Create(frame_start, frame_end);
			m_weights.push_back(bone_weights[i].m_weight);

			if(0 == i)
			{
				m_animations[i]->GetPositionKeyframeController()->AddControllable(this);
			}

			// now add the keyframes for this bone
			bool found = false;
			for(int32 kf = 0; kf < num_keyframes; kf++)
			{
				if(!stricmp(m_animations[i]->GetName(), keyframes[kf].m_name))
				{
					found = true;

					Vector3 pos = keyframes[kf].m_position;
					if(!stricmp(keyframes[kf].m_name, "ROOT"))
					{
//						pos -= m_offsets[keyframes[kf].m_frame - frame_start];
					}

					m_animations[i]->AddPositionKeyframe(
						keyframes[kf].m_frame, 
						pos);

					keyframes[kf].m_orientation.Normalize();
					m_animations[i]->AddOrientationKeyframe(
						keyframes[kf].m_frame, 
						keyframes[kf].m_orientation);
				}
			}

			if(!found)
			{
				DEBUGMSG("WARNING: no keyframes found for bone %s", bone_weights[i].m_name);
			}
		}
	}

	return false;
}

int32 
SkeletalAnimation::GetCurrFrame()
{
	if(m_animations.size())
	{
		return m_animations[0]->GetCurrFrame();
	}
	else
	{
		return m_frame_start;
	}
}

void 
SkeletalAnimation::Play(int32 mode, bool reset)
{
	m_mode = mode;
	m_last_frame = m_frame_start;

	for(uint32 i = 0; i < m_animations.size(); i++)
	{
		m_animations[i]->Start(mode, reset);
		if(Animation::PLAY_ONCE == mode)
		{
			// setup a callback for us
			m_animations[i]->AddAnimNotify(this);
		}

	}

	m_is_playing = true;
}

void 
SkeletalAnimation::Stop()
{
	m_mode = Animation::PLAY_NONE;
	for(uint32 i = 0; i < m_animations.size(); i++)
	{
		m_animations[i]->Stop();

		// just to be sure
		m_animations[i]->RemoveAnimNotify(this);
	}

	m_is_playing = false;
}

Animation *
SkeletalAnimation::GetAnimationByName(const char *name)
{
	for(uint32 i = 0; i < m_animations.size(); i++)
	{
		if(!stricmp(m_animations[i]->GetName(), name))
			return m_animations[i];
	}
	return 0;
}

void 
SkeletalAnimation::AnimDone(Animation *which)
{
	which->RemoveAnimNotify(this);

	bool totally_done = true;
	for(int32 i = 0; i < m_animations.size(); i++)
	{
		if(m_animations[i]->IsPlaying())
		{
//			DEBUGMSG("%d still playing", i);
			totally_done = false;
			break;
		}
	}

	if(totally_done)
	{
		// all of the animations for each bone is done, so we're done
		m_is_playing = false;
		m_mode = Animation::PLAY_NONE;
	}
}

void 
SkeletalAnimation::AddAnimMotionNotify(AnimMotionNotify *amn)
{
#ifdef _DEBUG
	std::vector<AnimMotionNotify *>::iterator itor = m_amn.begin();
	std::vector<AnimMotionNotify *>::iterator iend = m_amn.end();
	for(; itor < iend; itor++)
	{
		if(amn == *itor)
		{
			ASSERT(0);
		}
	}
#endif

	m_amn.push_back(amn);
}

void 
SkeletalAnimation::RemoveAnimMotionNotify(AnimMotionNotify *amn)
{
	std::vector<AnimMotionNotify *>::iterator itor = m_amn.begin();
	std::vector<AnimMotionNotify *>::iterator iend = m_amn.end();
	for(; itor < iend; itor++)
	{
		if(amn == *itor)
		{
			m_amn.erase(itor);
			return;
		}
	}
}

void 
SkeletalAnimation::ControllableUpdate()
{
	// this broke with transitions, and it's not used now anyway
#if 0
	int32 curr_frame = GetCurrFrame();

	Vector3 total_delta(0.0f, 0.0f, 0.0f);

	while(m_last_frame != curr_frame)
	{
//		total_delta += m_offsets[m_last_frame - m_frame_start];

		m_last_frame++;

		if(m_last_frame > m_frame_end)
			m_last_frame = m_frame_start;
	}

	std::vector<AnimMotionNotify *>::iterator itor = m_amn.begin();
	std::vector<AnimMotionNotify *>::iterator iend = m_amn.end();
	for(; itor < iend; itor++)
	{
//		(*itor)->OnMove(total_delta);
	}
#endif
}

void 
SkeletalAnimation::BeginTransition(real transition_time)
{
	for(int32 i = 0; i < m_animations.size(); i++)
	{
		if(m_animations[i]->IsPlaying())
		{
			m_animations[i]->BeginTransition(transition_time);
		}
	}
}

}

}


