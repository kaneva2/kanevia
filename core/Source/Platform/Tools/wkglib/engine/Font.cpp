/******************************************************************************
 Font.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Font.h"
#include "Texture.h"
#include "BMP.h"

#include "Device.h"
#include "RenderState.h"
#include "VertexShader.h"
#include "DynIVB.h"
#include "FontVert.h"

namespace wkg 
{

namespace engine 
{

#define METROWS	2	// # ABC metric rows
#define PADROWS	1	// # rows between encoding and glyph (to avoid texture picking up encoding info when displaying text)
#define UNIROWS	2	// # 8-bit rows to encode unicode index


Font::Font()
{
	cellSizeX = cellSizeY = 0;
	ascent = numRows = numCols = rowHei = numGlyphs = 0;
	glyphTex = 0;

	state.color = 0xffffffff;
	state.pVB = 0;
	state.pIB = 0;
	state.screenVS = 0;
	state.worldVS = 0;

	SetZPlane(0.0f);
}

Font::~Font()
{
	if (glyphTex)
		glyphTex->Release();
	glyphTex = 0;

	delete state.screenVS;
	delete state.worldVS;
	state.screenVS = state.worldVS = 0;

	//
	// we don't own pVB or pIB
	//
}

bool 
Font::Load(const TCHAR* filename)
{
	BMP bmp;
	LPDIRECT3DSURFACE9 pSurf, pDestSurf;
	D3DLOCKED_RECT lrc;
	int32 y, x;
	uint8 const* src;
	uint8* dest;
	
	ASSERT(glyphTex == 0 && glyphMap.size() == 0 && state.screenVS == 0 && state.worldVS == 0);

	state.screenVS = new VertexShader();
	state.worldVS = new VertexShader();
	if (!state.screenVS || !state.screenVS->Init(FontScreenVert::FVF) ||
		!state.worldVS || !state.worldVS->Init(FontWorldVert::FVF))
		goto abort;

	if (!bmp.Load(filename))
		goto abort;

	glyphTex = new Texture();
	if (!glyphTex || !glyphTex->Create(bmp.GetWidth(), bmp.GetHeight(), D3DFMT_A4R4G4B4))
		goto abort;

	if (D3D_OK != Device::Instance()->CreateOffscreenPlainSurface(bmp.GetWidth(), bmp.GetHeight(), D3DFMT_A4R4G4B4, D3DPOOL_SYSTEMMEM, &pSurf, 0))
		goto abort;

	if (D3D_OK != pSurf->LockRect(&lrc, NULL, 0))
	{
		pSurf->Release();
		goto abort;
	}

	src = bmp.GetBits();
	dest = (uint8*)lrc.pBits;

	for (y = 0; y < bmp.GetHeight(); y++)
	{
		for (x = 0; x < bmp.GetWidth(); x++)
		{
			dest[x * 2 + 0] = (src[x * 3 + 0] & 0xf0) | (src[x * 3 + 0] >> 4);
			dest[x * 2 + 1] = (src[x * 3 + 1] & 0xf0) | (src[x * 3 + 2] >> 4);
		}

		src += bmp.GetWidth() * 3;
		dest += lrc.Pitch;
	}

	pSurf->UnlockRect();

	if (D3D_OK != glyphTex->Interface()->GetSurfaceLevel(0, &pDestSurf))
	{
		pSurf->Release();
		goto abort;
	}

	if (D3D_OK != Device::Instance()->UpdateSurface(pSurf, 0, pDestSurf, 0))
	{
		pDestSurf->Release();
		pSurf->Release();
		goto abort;
	}

	pDestSurf->Release();
	pSurf->Release();

	ExtractFontMetrics(&bmp);
	ExtractGlyphMetrics(&bmp);

	return true;

abort:
	delete state.screenVS;
	delete state.worldVS;
	state.screenVS = state.worldVS = 0;
	if (glyphTex)
		glyphTex->Release();
	glyphTex = 0;

	return false;
}

void 
Font::Measure(const TCHAR* text, int32* wid, int32* hei) const
{
	if (text)
	{
		*hei = cellSizeY;
		*wid = 0;
		while (*text)
		{
			GMCIter& met = glyphMap.find(*text);
			if (met != glyphMap.end())
			{
				*wid += met->second.a + met->second.b + met->second.c;
			}
			++text;
		}
	}
	else
	{
		*wid = *hei = 0;
	}
}

void
Font::Draw(const TCHAR* text, void (*AddVert)(uint8* vertBase, int32 vertOfs, const Font::DrawState& ds, real x, real y, real z, real tu, real tv)) const
{
	if (text)
	{
#		define ADDGLYPH(sx, sy, sw, sh, tl, tt, tr, tb)					\
			AddVert(pVert, 0, state, sx, sy, base.z, tl, tt);			\
			AddVert(pVert, 1, state, sx + sw, sy, base.z, tr, tt);		\
			AddVert(pVert, 2, state, sx, sy + sh, base.z, tl, tb);		\
			AddVert(pVert, 3, state, sx + sw, sy + sh, base.z, tr, tb);	\
			(*piVert++) = ivNext + 0; \
			(*piVert++) = ivNext + 1; \
			(*piVert++) = ivNext + 2; \
			(*piVert++) = ivNext + 2; \
			(*piVert++) = ivNext + 1; \
			(*piVert++) = ivNext + 3;
		int32 vCount, vStart;
		uint8* pVert;
		uint16* piVert;
		uint16 ivCount, ivStart, ivNext;
		char lastch;
		Vector3 base(0.0f, (real)(-(int32)ascent), 0.0f);
		RenderState rs;

		DynIVB divb;
		if (!divb.Prepare(state.pVB, (uint8)state.vertSize, state.pIB, 1))
			return;

		rs.Set(D3DRS_DITHERENABLE, TRUE);
		rs.Set(D3DRS_CULLMODE, D3DCULL_NONE);
		rs.Set(D3DRS_ALPHABLENDENABLE, TRUE);
		rs.Set(D3DRS_LIGHTING, FALSE);
		rs.Set(D3DRS_COLORVERTEX, TRUE);
		rs.Set(D3DRS_ZWRITEENABLE, FALSE);
		rs.Set(D3DRS_ZENABLE, FALSE);
		rs.Set(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		rs.Set(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		rs.Set(D3DRS_ALPHATESTENABLE, TRUE);
		rs.Set(D3DRS_ALPHAREF, 0x08);
		rs.Set(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
		rs.Set(D3DRS_STENCILENABLE, FALSE);
		rs.Set(D3DRS_FOGENABLE, FALSE);
		rs.SSet(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
		rs.SSet(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
		rs.Set(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
		rs.Set(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);
		rs.Set(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		rs.Set(0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);
		rs.Set(0, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);
#ifdef HACK_MIP_OFF
		rs.Set(0, D3DTSS_MINFILTER, D3DTEXF_NONE);
		rs.Set(0, D3DTSS_MAGFILTER, D3DTEXF_NONE);
		rs.Set(0, D3DTSS_MIPFILTER, D3DTEXF_NONE);
#endif

		Device::Instance()->SetRenderState(rs);
		Device::Instance()->SetTexture(0, glyphTex);
		Device::Instance()->SetStreamSource(0, divb.GetVB(), 0, divb.GetVertSize());

		lastch = 0;
		GMCIter& met = glyphMap.end();

		while (*text)
		{
			if (*text != lastch)
				met = glyphMap.find(*text);

			if (met != glyphMap.end())
			{
				if (divb.CanAppend(4, 6, &pVert, &piVert, &ivNext))
				{
					base.x += met->second.a;
					ADDGLYPH(base.x, base.y, met->second.b, cellSizeY, met->second.tl, met->second.tt, met->second.tr, met->second.tb);
					base.x += met->second.b + met->second.c;
				}
				else
				{
					if (divb.BatchReady(&vStart, &vCount, &ivStart, &ivCount))
					{
						Device::Instance()->SetIndices(divb.GetIB());
						Device::Instance()->DrawIndexedPrimitive(
							D3DPT_TRIANGLELIST, 
							vStart,
							0, 
							vCount, 
							ivStart, 
							ivCount / 3);
					}
					else
						break;	// d3d problem...probably lost device
				}
			}
			lastch = *text++;
		}

		if (divb.BatchReady(&vStart, &vCount, &ivStart, &ivCount))
		{
			Device::Instance()->SetIndices(divb.GetIB());
			Device::Instance()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, vStart, 0, vCount, ivStart, ivCount / 3);
		}

#		undef ADDGLYPH
	}
}

void
Font::AddScreenVert(uint8* vertBase, int32 vertOfs, const DrawState& ds, real x, real y, real z, real tu, real tv)
{
	FontScreenVert* pVert = ((FontScreenVert*)vertBase) + vertOfs;

	pVert->pos.x = -0.5f + ds.origin.x + x*ds.cosAng - y*ds.sinAng;	// -0.5 bias aligns texels one-to-one with pixels
	pVert->pos.y = -0.5f + ds.origin.y + x*ds.sinAng + y*ds.cosAng;
	pVert->pos.z = ds.origin.z;
	pVert->diffuse = ds.color;
	pVert->rhw = 1.0f;
	pVert->tu = tu;
	pVert->tv = tv;
}

void
Font::AddWorldVert(uint8* vertBase, int32 vertOfs, const DrawState& ds, real x, real y, real z, real tu, real tv)
{
	FontWorldVert* pVert = ((FontWorldVert*)vertBase) + vertOfs;

	pVert->pos.Set(x, y, z);
	pVert->diffuse = ds.color;
	pVert->tu = tu;
	pVert->tv = tv;
}

void
Font::Draw(int32 x, int32 yBaseline, const TCHAR* text) const
{
	state.screenVS->Activate();
	state.vertSize = sizeof(FontScreenVert);
	state.origin.x = real(x);
	state.origin.y = real(yBaseline);
	// state.origin.z is set by SetZPlane()
	state.cosAng = 1.0f;
	state.sinAng = 0.0f;
	Draw(text, AddScreenVert);
}

void 
Font::DrawRotated(int32 x, int32 yBaseline, real degrees, const TCHAR* text) const
{
	state.screenVS->Activate();
	state.vertSize = sizeof(FontScreenVert);
	state.origin.x = real(x);
	state.origin.y = real(yBaseline);
	// state.origin.z is set by SetZPlane()
	state.cosAng = cos(DEG_TO_RAD(-degrees));	// negate to convert from LHS to RHS
	state.sinAng = sin(DEG_TO_RAD(-degrees));
	Draw(text, AddScreenVert);
}

void 
Font::Draw(const TCHAR* text) const
{
	state.worldVS->Activate();
	state.vertSize = sizeof(FontWorldVert);
	Draw(text, AddWorldVert);
}

void
Font::ExtractFontMetrics(const BMP* bmp)
{
	cellSizeX = ExtractWhiteCount(bmp, 0, 0, 1, 0, bmp->GetWidth());
	cellSizeY = ExtractWhiteCount(bmp, 0, 1, 0, 1, bmp->GetHeight());
	ascent = ExtractWhiteCount(bmp, 1, 1, 0, 1, cellSizeY);
	rowHei = METROWS + PADROWS + cellSizeY + PADROWS + UNIROWS;
	numGlyphs = ExtractByte(bmp, 2, 1, 0, 1);
	numGlyphs <<= 8;
	numGlyphs += ExtractByte(bmp, 3, 1, 0, 1);
	ASSERT(numGlyphs <= 0xffffL);
	numCols = bmp->GetWidth() / cellSizeX;
	numRows = (numGlyphs + (numCols - 1)) / numCols;
}

void
Font::ExtractGlyphMetrics(const BMP* bmp)
{
	GlyphMetric gm;
	int32 x, y;
	uint32 i;
	uint16 code;

	x = cellSizeX;		// skip first cell (font metric info)
	y = 0; 

	for (i = 0; i < numGlyphs - 1; i++)		// -1 since we skipped the first cell
	{
		gm.a = ExtractPosNeg(bmp, x, y,				RGB(255, 0, 0), RGB(0, 255, 0));
		gm.c = ExtractPosNeg(bmp, x + ABS(gm.a), y, RGB(0, 0, 255), RGB(255, 255, 0));
		gm.b = ExtractWhiteCount(bmp, x, y + 1, 1, 0, cellSizeX);
		gm.tl = x / real(bmp->GetWidth());
		gm.tt = (y + METROWS + PADROWS) / real(bmp->GetHeight());
		gm.tr = (x + gm.b) / real(bmp->GetWidth());
		gm.tb = (y + METROWS + PADROWS + cellSizeY) / real(bmp->GetHeight());
		code = (uint16)ExtractByte(bmp, x, y + METROWS + PADROWS + cellSizeY + PADROWS, 1, 0);
		code <<= 8;
		code += (uint16)ExtractByte(bmp, x, y + METROWS + PADROWS + cellSizeY + PADROWS + 1, 1, 0);

		glyphMap.insert(GlyphMap::value_type(code, gm));

		x += cellSizeX;
		if (x + cellSizeX > uint32(bmp->GetWidth()))
		{
			y += rowHei;
			x = 0;
		}
	}
}

int32
Font::ExtractWhiteCount(const BMP* bmp, int32 x, int32 y, int32 xinc, int32 yinc, int32 limit) const
{
	int32 result = 0;
	uint8 const* bits = bmp->GetBits() + (y * bmp->GetWidth() * 3) + x * 3;
	struct _rgb { uint8 r, g, b; } white;

	ASSERT(xinc == 1 || yinc == 1);		// no practical reason to support other increments

	white.r = white.g = white.b = 0xff;

	if (xinc)
	{
		while (limit-- > 0)
		{
			if (0 == memcmp(bits, &white, 3))
				++result;
			else
				break;

			bits += 3;
		}
	}
	else if (yinc)
	{
		while (limit-- > 0)
		{
			if (0 == memcmp(bits, &white, 3))
				++result;
			else
				break;

			bits += bmp->GetWidth() * 3;
		}
	}
	else
	{
		ASSERT(0);	// no practical reason to support diagonal extraction
	}

	return result;
}

int32 
Font::ExtractPosNeg(const BMP* bmp, int32 x, int32 y, uint32 posRGB, uint32 negRGB) const
{
	int32 result = 0;
	uint8 const* bits = bmp->GetBits() + (y * bmp->GetWidth() * 3) + x * 3;
	struct _rgb { uint8 r, g, b; } pos, neg;

	pos.r = uint8((posRGB >> 16) & 0xff);
	pos.g = uint8((posRGB >> 8) & 0xff);
	pos.b = uint8((posRGB >> 0) & 0xff);
	neg.r = uint8((negRGB >> 16) & 0xff);
	neg.g = uint8((negRGB >> 8) & 0xff);
	neg.b = uint8((negRGB >> 0) & 0xff);

	ASSERT(posRGB != negRGB && posRGB && negRGB);

	while (1)
	{
		if (0 == memcmp(bits, &pos, 3))
			++result;
		else if (0 == memcmp(bits, &neg, 3))
			--result;
		else
			break;

		bits += 3;
		ASSERT(bits < bmp->GetBits() + (y * bmp->GetWidth() * 3) + (x + cellSizeX) * 3);
	}

	return result;
}

int32
Font::ExtractByte(const BMP* bmp, int32 x, int32 y, int32 xinc, int32 yinc) const
{
	int32 result = 0;
	uint8 const* bits = bmp->GetBits() + (y * bmp->GetWidth() * 3) + x * 3;

	ASSERT(xinc == 1 || yinc == 1);		// no practical reason to support other increments

	if (xinc)
	{
		for (int32 i = 0; i < 8; i++)
		{
			if (*bits)
				result |= (0x80 >> i);

			bits += 3;
		}
	}
	else if (yinc)
	{
		for (int32 i = 0; i < 8; i++)
		{
			if (*bits)
				result |= (0x80 >> i);

			bits += bmp->GetWidth() * 3;
		}
	}
	else
	{
		ASSERT(0);	// no practical reason to support diagonal extraction
	}

	return result;
}

}}