/******************************************************************************
 posmplexcontroller.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "controllable.h"
#include "controller.h"
#include "timedupdatable.h"
#include "vector3.h"

namespace wkg
{

namespace engine
{

class PosMPlexController : public Controllable, public Controller, public TimedUpdatable
{
public:
	PosMPlexController();
	virtual ~PosMPlexController();

	void ControllableUpdate();

	int32 AddEntry(int32 priority);
	Vector3 *GetEntry(int32 idx);

	void SetPlaying(uint32 idx, bool playing) 
	{ 
		if(idx < m_playing.size())
			m_playing[idx] = playing; 
	}

	void SetTarget(Vector3 *v) { m_target = v; }

	void BeginTransition(real time) 
	{ 
		if(m_target && !m_first)
		{
			m_transition_begin = *m_target; 
			m_transitioning = true;
			m_transition_time_left = m_transition_time = time;
		}
		else if(!m_first)
		{
			m_first = false;
		}
	}

	virtual void TimedUpdate(int32 mspf);

protected:
	std::vector<Vector3 *> m_positions;
	std::vector<int32> m_priority;
	std::vector<bool> m_playing;

	Vector3 *m_target;
	Vector3 m_transition_begin;
	real m_transition_time;
	real m_transition_time_left;
	bool m_transitioning;
	bool m_first;
};

}

}