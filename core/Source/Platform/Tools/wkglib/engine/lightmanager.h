/******************************************************************************
 lightmanager.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <vector>
#include "wkgcolor.h"
#include "singleton.h"
#include "timedupdatable.h"

namespace wkg
{

class XMLDocument;
class XMLNode;
class Light;
class MeshSection;

class LightManager : public Singleton<LightManager>, public TimedUpdatable
{
public:
	LightManager();
	~LightManager();

	int32 GetNumLights();

	void AddLight(Light *light);
	Light *GetLight(int32 idx);
	Light *GetClosestLight(const Vector3 &pos);

	bool Load(const char *lightfile);

	virtual void TimedUpdate(int32 mspf);

	WKGColor GetAmbient() { return WKGColor(0.1f, 0.1f, 0.1f, 1.0f); }

	int32 GetLightsForMeshSection(MeshSection *section, Light **lights, int32 max_lights);
	int32 GetClosestLights(const Vector3 &pos, Light **lights, int32 max_lights);

protected:
	bool Load(XMLDocument *xml_doc);
	bool LoadLights(XMLNode *node);

protected:
	vector<Light *>m_lights;
};

}

