/******************************************************************************
 WorldObjectList.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "WorldObject.h"
#include "WorldObjectList.h"

#include <algorithm>
#include <vector>

namespace wkg
{

namespace engine
{

WorldObjectList::WorldObjectList()
{
}

WorldObjectList::~WorldObjectList()
{
}

void
WorldObjectList::AddToList(WorldObject* wo)
{
	m_wo_map[wo->GetUniqueID()] = wo;
	m_wo_vector.push_back(wo);
}

void
WorldObjectList::RemoveFromList(WorldObject* wo)
{
	m_wo_map.erase(wo->GetUniqueID());
	std::vector<WorldObject *>::iterator itor = std::find(m_wo_vector.begin(), m_wo_vector.end(), wo);
	if(itor && itor != m_wo_vector.end())
	{
		m_wo_vector.erase(itor);
	}
}

WorldObject*
WorldObjectList::GetWorldObject(int32 id)
{
	return m_wo_map[id];
}

void
WorldObjectList::OnUpdate(uint32 ms)
{
	int32 size = m_wo_vector.size();
	for(int32 i = 0; i < size; ++i)
	{
		WorldObject* wo = m_wo_vector[i];
		if(wo)
		{
			wo->OnUpdate(ms);
		}	
	}

#if 0
	std::map<int32, WorldObject*>::iterator itor = m_wo_map.begin();
	std::map<int32, WorldObject*>::iterator iend = m_wo_map.end();

	for(; itor != iend; ++itor)
	{
		WorldObject* wo = itor->second;
		if(wo)
		{
			wo->OnUpdate(ms);
		}
	}
#endif
}

WorldObject*
WorldObjectList::GetClosestWorldObject(Vector3& pos)
{
	WorldObject* retval = 0;
	real retval_dist = 5000.0f;
	std::map<int32, WorldObject*>::iterator itor = m_wo_map.begin();
	std::map<int32, WorldObject*>::iterator iend = m_wo_map.end();
	for(; itor != iend; ++itor)
	{
		WorldObject* wo = itor->second;
		if(wo)
		{
			Vector3 wo_pos = wo->GetPosition();
			wo_pos -= pos;
			if(wo_pos.Length() <= retval_dist)
			{
				retval_dist = wo_pos.Length();
				retval = wo;
			}
		}
	}
	return retval;
}

WorldObject*
WorldObjectList::GetByID(int32 id)
{
	return m_wo_map[id];
}


}
}