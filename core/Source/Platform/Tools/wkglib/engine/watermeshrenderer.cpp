/******************************************************************************
 watermeshrenderer.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/


#include "wkgtypes.h"
#include "effectmeshrenderer.h"
#include "watermeshrenderer.h"

namespace wkg
{

namespace engine
{

WaterMeshRenderer::WaterMeshRenderer(EffectShader *shader) :
	EffectMeshRenderer(shader)
{

}

}

}

