/******************************************************************************
 bonevisualizer.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/


#pragma once

#include "meshnode.h"

namespace wkg
{

namespace engine
{

class BoneVisualizer : public MeshNode
{
public:
	BoneVisualizer();
	~BoneVisualizer();
	
	virtual void OnUpdate();

	void SetLength(real len) { m_length = len; }

protected:
	real m_length;
	Mesh *m_mesh;
};

}

}

