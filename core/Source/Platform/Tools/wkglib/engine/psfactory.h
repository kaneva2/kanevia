/******************************************************************************
 psfactory.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#ifndef _PS_FACTORY_H_
#define _PS_FACTORY_H_

#include <string>
#include <vector>
#include "singleton.h"

namespace wkg
{

namespace engine
{

class PixelShader;

class PSFactory : public Singleton<PSFactory>
{
public:
	PSFactory();
	virtual ~PSFactory();

	void RegisterShader(PixelShader *vs, std::string name);
	PixelShader *GetShader(std::string name);

protected:
	class ShaderEntry
	{
		public:
			std::string m_name;
			PixelShader *m_shader;
	};

	typedef std::vector<ShaderEntry> ShaderVector;
	typedef ShaderVector::iterator ShaderIterator;

	ShaderVector m_shaders;	
};

}

}

#endif

