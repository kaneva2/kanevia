/******************************************************************************
 lightmapeffect.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "effectshader.h"

namespace wkg
{

namespace engine
{

class LightmapEffect : public EffectShader
{
public:
	LightmapEffect();
	virtual ~LightmapEffect();

	virtual void Prepare(Mesh *mesh);

};

}

}

