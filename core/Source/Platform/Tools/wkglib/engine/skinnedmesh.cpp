/******************************************************************************
 skinnedmesh.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "xformnode.h"
#include "wkbfile.h"
#include "meshtypes.h"
#include "matrixpalettenode.h"

#include "skinnedmesh.h"

//#define LOAD_ANIMATION

#ifdef LOAD_ANIMATION

#include "wkgkeyframe.h"
#include "keyframecontroller.h"
#include "positionkeyframecontroller.h"
#include "orientationkeyframecontroller.h"
#include "posorienttomatrix44controller.h"

#endif

//#ifdef _DEBUG
//#define VISUALIZE_SKELETON
//#endif

#ifdef VISUALIZE_SKELETON
#include "bonevisualizer.h"
#endif

extern int32 g_vert_data;

namespace wkg
{

namespace engine
{

SkinnedMesh::SkinnedMesh() : 
	Mesh(),
	m_num_bones(0),
	m_bones(0),
	m_skeletal_root(0),
	m_palette_node(0),
	m_weights_vb(0)
{
}

SkinnedMesh::~SkinnedMesh()
{
	// should not delete skeletal root because it is assumed to
	// be deleted in the scene graph delete
//	delete m_skeletal_root;
//	m_skeletal_root = 0;

	if(m_weights_vb)
		m_weights_vb->Release();
	m_weights_vb = 0;
}

bool 
SkinnedMesh::Load(WKBFile &wkb)
{
	// load our default vertex data 
	//  (position, normal, texture)
	Mesh::Load(wkb);

	// and now nab our weight info
	WKGVertexWeights *vw = new WKGVertexWeights[m_num_verts];
	WKGWeightedVertex *weighted_verts = wkb.GetVerts();

	for(uint32 v = 0; v < m_num_verts; v++)
	{
		vw[v].m_bones[0] = weighted_verts[v].m_bones[0];
		vw[v].m_bones[1] = weighted_verts[v].m_bones[1];
		ASSERT(weighted_verts[v].m_bones[0] >= 0 && weighted_verts[v].m_bones[0] < MAX_BONES_PER_SECTION);
		ASSERT(weighted_verts[v].m_bones[1] >= 0 && weighted_verts[v].m_bones[1] < MAX_BONES_PER_SECTION);
		vw[v].m_weights[0] = weighted_verts[v].m_weights[0];
		vw[v].m_weights[1] = weighted_verts[v].m_weights[1];
	}

	SetVertexWeights(m_num_verts, vw);

	delete [] vw;
	vw = 0;

	// now for the skeletal specific info
	m_num_bones = wkb.GetNumBones();
	WKGBone *bones= wkb.GetBones();

	// i suppose it's allright to create an animated mesh with no bones...
	if(!m_num_bones || !bones) 
	{
		DEBUGMSG("WARNING: animated mesh created with no bones!");
		return true;
	}

	// construct a mini-scene graph for our skeleton
	m_skeletal_root = new XFormNode();

	// create all the nodes
	m_bones = new XFormNode *[m_num_bones];
	for(int32 i = 0; i < m_num_bones; i++)
	{
		XFormNode *node = new XFormNode();

		Matrix44 matrix;
		Matrix44 pos;
		Matrix44 orient;

		pos.Identity();
		pos.SetTranslation(bones[i].m_position);

		orient = (Matrix44)bones[i].m_orientation;

		matrix = orient * pos;

		node->SetMatrix(matrix);
		node->SetName(bones[i].m_name);
		m_bones[i] = node;
	}

	// now link them all
	for(i = 0; i < m_num_bones; i++)
	{
		if(bones[i].m_parent >= 0)
		{
			m_bones[bones[i].m_parent]->AddChild(m_bones[i]);
		}
		else
		{
			m_skeletal_root->AddChild(m_bones[i]);
		}
	}

	// let's update the scene graph of this model to form the
	//  local to model matrices
	Matrix44 mi(Matrix44::IDENTITY);
	m_skeletal_root->DFSUpdate(mi);

	m_palette_node = new MatrixPaletteNode();
	m_skeletal_root->AddChild(m_palette_node);

	for(i = 0; i < m_num_bones; i++)
	{
		Matrix44 inv_base_transform = m_bones[i]->GetLocalToWorld();
		inv_base_transform.Inverse();

		m_palette_node->AddNode(m_bones[i], inv_base_transform);
	}

#ifdef LOAD_ANIMATION
	int32 num_keyframes = wkb.GetNumKeyframes();
	if(num_keyframes)
	{
		WKGKeyframe *keyframes = wkb.GetKeyframes();

		int32 frame_start = 0xffff;
		int32 frame_end = 0;
		for(int32 kf = 0; kf < num_keyframes; kf++)
		{
			if(keyframes[kf].m_frame < frame_start)
				frame_start = keyframes[kf].m_frame;

			if(keyframes[kf].m_frame > frame_end)
				frame_end = keyframes[kf].m_frame;
		}


		// and setup the controllers if there is any animation
		for(i = 0; i < m_num_bones; i++)
		{
			PosOrientToMatrix44Controller *pomc = new PosOrientToMatrix44Controller(&(m_bones[i]->m_matrix));

			PositionKeyframeController *pkfc = new PositionKeyframeController(&(pomc->m_position));
			pkfc->AddControllable(pomc);

			OrientationKeyframeController *okfc = new OrientationKeyframeController(&(pomc->m_orientation));
			okfc->AddControllable(pomc);

			KeyframeController *kfcp = new KeyframeController(frame_start, frame_end, &(pkfc->m_frame));
			kfcp->AddControllable(pkfc);

			KeyframeController *kfco = new KeyframeController(frame_start, frame_end, &(okfc->m_frame));
			kfco->AddControllable(okfc);

			// now add the keyframes for this bone
			for(int32 kf = 0; kf < num_keyframes; kf++)
			{
				if(keyframes[kf].m_bone == i)
				{
					WKGPositionKeyframe *pkf = new WKGPositionKeyframe();
					pkf->m_frame = keyframes[kf].m_frame;
					pkf->m_position = keyframes[kf].m_position;

					WKGOrientationKeyframe *okf = new WKGOrientationKeyframe();
					okf->m_frame = keyframes[kf].m_frame;
					okf->m_orientation = keyframes[kf].m_orientation;

					pkfc->AddPositionKeyframe(pkf);
					okfc->AddOrientationKeyframe(okf);
				}
			}

			kfco->StartUpdates();
			kfcp->StartUpdates();
		}
	}
#endif


	// if we wish to visualize the skeleton, then create our 
	//  bone visualizers
#ifdef VISUALIZE_SKELETON
	for(i = 0; i < m_num_bones; i++)
	{
		if(bones[i].m_parent >= 0)
		{
			BoneVisualizer *bv = new BoneVisualizer();
			bv->SetLength(bones[i].m_position.Length());
			m_bones[bones[i].m_parent]->AddChild(bv);
		}
	}

#endif

	return true;
}

void 
SkinnedMesh::SetVertexWeights(uint32 num_verts, WKGVertexWeights *v)
{
	HRESULT hr = Device::Instance()->CreateVertexBuffer(
		sizeof(WKGVertexWeights) * num_verts, 
		D3DUSAGE_WRITEONLY, 0, 
		D3DPOOL_MANAGED,
		&m_weights_vb, 0);

	g_vert_data += sizeof(WKGVertexWeights) * num_verts;
//	DEBUGMSG("vert data: %d", g_vert_data);

	if(FAILED(hr))
	{
		ASSERT(0);
	}

	void *dst;
	hr = m_weights_vb->Lock(0, 0, &dst, 0);
	ASSERT(SUCCEEDED(hr));

	memcpy(dst, v, sizeof(WKGVertexWeights) * num_verts);

	hr = m_weights_vb->Unlock();
	ASSERT(SUCCEEDED(hr));
}

}

}


