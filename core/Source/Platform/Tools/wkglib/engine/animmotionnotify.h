/******************************************************************************
 animmotionnotify.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

namespace engine
{

class AnimMotionNotify
{
	public:
		virtual void OnMove(Vector3 &m) = 0;
};

}

}

