/******************************************************************************
 worldcollidable.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "collidable.h"

namespace wkg
{

namespace engine
{

class WorldCollidable : public Collidable
{
public:
	WorldCollidable() : Collidable() { }
	virtual ~WorldCollidable() { }
};

}

}

