/******************************************************************************
 watereffectshader.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "matrix44.h"
#include "device.h"
#include "watereffectshader.h"

namespace wkg
{

namespace engine
{

// DWDTODO:
//  Fill in the balnks in terms of vertex stream data
//  Load the textures
//  Set the textures during prepare
//  Set dynamic vertex constants during prepare

bool 
WaterEffectShader::Create(const char *filename)
{
	bool res = EffectShader::Create(filename);

	HRESULT hr;

	hr = D3DXCreateTextureFromFile(
		Device::Instance()->GetD3DDevicePointer(), 
		"textures\\water\\oceangradient1.bmp", 
		&m_pTex);

	if (FAILED(hr))
		return false;
    
	hr = D3DXCreateTextureFromFile(
		Device::Instance()->GetD3DDevicePointer(), 
		"textures\\water\\Waterbump3.bmp", 
		&m_pHeightTex);

	if (FAILED(hr))
		return false;
    
	hr = D3DXCreateCubeTextureFromFile(
		Device::Instance()->GetD3DDevicePointer(),
		"textures\\water\\ocean6.dds", 
		&m_pCubeTex);

	if (FAILED(hr))
		return false;

	// Create and fill the bumpmap
    if( FAILED( InitBumpMap() ) )
        return false;

	return res;
}

void 
WaterEffectShader::Prepare(Mesh *mesh)
{
 	Device::Instance()->GetD3DDevicePointer()->SetTexture(0, m_psBumpMap);
 	Device::Instance()->GetD3DDevicePointer()->SetTexture(1, m_psBumpMap);
 
	Device::Instance()->GetD3DDevicePointer()->SetTexture(2, m_pCubeTex);
	Device::Instance()->GetD3DDevicePointer()->SetTexture(3, m_pTex);


	Matrix44 world, view, proj, wvp;

	Device::Instance()->GetTransform(D3DTS_WORLD, &world);
	Device::Instance()->GetTransform(D3DTS_VIEW, &view);
	Device::Instance()->GetTransform(D3DTS_PROJECTION, &proj);

	wvp = world * view * proj;
	wvp.Transpose();


	// c0    - { 0.0,  0.5, 1.0, 2.0}
    // c1    - { 4.0, .5pi, pi, 2pi}  
	// c2    - {1, -1/3!, 1/5!, -1/7!  }  //for sin
	// c3    - {1/2!, -1/4!, 1/6!, -1/8!  }  //for cos
	// c4-7 - Composite World-View-Projection Matrix
	// c8     - ModelSpace Camera Position
	// c9     - ModelSpace Light Position
	// c10   - {fixup factor for taylor series imprecision, }(1.02, 0.1, 0, 0)
	// c11   - {waveHeight0, waveHeight1, waveHeight2, waveHeight3}  (0.4, 0.5, 0.025, 0.025)
	// c12   - {waveOffset0, waveOffset1, waveOffset2, waveOffset3}  (0.0, 0.2, 0.0, 0.0) 
	// c13   - {waveSpeed0, waveSpeed1, waveSpeed2, waveSpeed3}   (0.2, 0.15, 0.4, 0.4)
	// c14   - {waveDirX0, waveDirX1, waveDirX2, waveDirX3}   (0.25, 0.0, -0.7, -0.8)
	// c15   - {waveDirY0, waveDirY1, waveDirY2, waveDirY3}   (0.0, 0.15, -0.7, 0.1)
	// c16    - { time, sin(time)}
	// c17    - {basetexcoord distortion x0, y0, x1, y1} (0.031, 0.04, -0.03, 0.02)
    // c18   - World Martix

	D3DXVECTOR4 c0( 0.0f,  0.5f, 1.0f, 2.0f );
	D3DXVECTOR4 c1( 4.0f, 0.5f*D3DX_PI, D3DX_PI, 2.0f*D3DX_PI);
	D3DXVECTOR4 c2( 1.0f,      -1.0f/6.0f,  1.0f/120.0f, -1.0f/5040.0f);
	D3DXVECTOR4 c3( 1.0f/2.0f, -1.0f/24.0f, 1.0f/720.0f, -1.0f/40320.0f );

	// Get the camera position
	Matrix44 view2world = view;
	view2world.Inverse();
	D3DXVECTOR4 c8(view2world._41, view2world._42, view2world._43, 1.0f);

	D3DXVECTOR4 c10( 1.02f, 0.1f, 0.0f, 0.0f );
	D3DXVECTOR4 c11( 0.4f, 0.5f, 0.025f, 0.025f);
	D3DXVECTOR4 c12( 0.0f, 0.2f, 0.0f, 0.0f );
	D3DXVECTOR4 c13( 0.2f, 0.15f, 0.4f, 0.4f);
	D3DXVECTOR4 c14( 02.5f, 0.0f, -7.0f, -8.0f);
	D3DXVECTOR4 c15( 0.0f, 1.5f, -7.0f, 1.0f);

	// DWDTODO: Get time
	static float fTime = 1.0f;
//	fTime += 0.0001f;
	D3DXVECTOR4 c16( (float)fTime*0.75f, sinf(fTime), 0.0f, 0.0f);
	D3DXVECTOR4 c17( 0.031f, 0.04f, -0.03f, 0.02f);
		
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  0, c0,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  1, c1,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  2, c2,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  3, c3,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  8, c8,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  10, c10,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  11, c11,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  12, c12,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  13, c13,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  14, c14,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  15, c15,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  16, c16,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  17, c17,  1 );

	
   	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  4, &wvp,  4 );
    
	Matrix44 world_transpose = world;
	world_transpose.Transpose();
    Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(  18, &world_transpose,  4 );

        
	//c0 -  Common Const (0, 0.5, 1, 0.25)
	//c1 - highlightColor (0.8, 0.76, 0.62, 1)

	D3DXVECTOR4 c0p( 0.0f, 0.5f, 1.0f, 0.25f);
	D3DXVECTOR4 c1p( 0.8f, 0.76f, 0.62f, 1.0f);

	Device::Instance()->GetD3DDevicePointer()->SetPixelShaderConstant(  0, c0p,  1 );
	Device::Instance()->GetD3DDevicePointer()->SetPixelShaderConstant(  1, c1p,  1 );
}

HRESULT 
WaterEffectShader::InitBumpMap()
{
    LPDIRECT3DTEXTURE8 psBumpSrc = m_pHeightTex;
    D3DSURFACE_DESC    d3dsd;
    D3DLOCKED_RECT     d3dlr;

    psBumpSrc->GetLevelDesc( 0, &d3dsd );
    // Create the bumpmap's surface and texture objects
    if( FAILED( Device::Instance()->GetD3DDevicePointer()->CreateTexture( d3dsd.Width, d3dsd.Height, 1, 0, 
        D3DFMT_V8U8, D3DPOOL_MANAGED, &m_psBumpMap ) ) )
    {
        return E_FAIL;
    }

    // Fill the bits of the new texture surface with bits from
    // a private format.
    psBumpSrc->LockRect( 0, &d3dlr, 0, 0 );
    DWORD dwSrcPitch = (DWORD)d3dlr.Pitch;
    BYTE* pSrcTopRow = (BYTE*)d3dlr.pBits;
    BYTE* pSrcCurRow = pSrcTopRow;
    BYTE* pSrcBotRow = pSrcTopRow + (dwSrcPitch * (d3dsd.Height - 1) );

    m_psBumpMap->LockRect( 0, &d3dlr, 0, 0 );
    DWORD dwDstPitch = (DWORD)d3dlr.Pitch;
    BYTE* pDstTopRow = (BYTE*)d3dlr.pBits;
    BYTE* pDstCurRow = pDstTopRow;
    BYTE* pDstBotRow = pDstTopRow + (dwDstPitch * (d3dsd.Height - 1) );

    for( DWORD y=0; y<d3dsd.Height; y++ )
    {
        BYTE* pSrcB0; // addr of current pixel
        BYTE* pSrcB1; // addr of pixel below current pixel, wrapping to top if necessary
        BYTE* pSrcB2; // addr of pixel above current pixel, wrapping to bottom if necessary
        BYTE* pDstT;  // addr of dest pixel;

        pSrcB0 = pSrcCurRow;

        if( y == d3dsd.Height - 1)
            pSrcB1 = pSrcTopRow;
        else
            pSrcB1 = pSrcCurRow + dwSrcPitch;

        if( y == 0 )
            pSrcB2 = pSrcBotRow;
        else
            pSrcB2 = pSrcCurRow - dwSrcPitch;

        pDstT = pDstCurRow;

        for( DWORD x=0; x<d3dsd.Width; x++ )
        {
            LONG v00; // Current pixel
            LONG v01; // Pixel to the right of current pixel, wrapping to left edge if necessary
            LONG vM1; // Pixel to the left of current pixel, wrapping to right edge if necessary
            LONG v10; // Pixel one line below.
            LONG v1M; // Pixel one line above.

            v00 = *(pSrcB0+0);
            
            if( x == d3dsd.Width - 1 )
                v01 = *(pSrcCurRow);
            else
                v01 = *(pSrcB0+4);
            
            if( x == 0 )
                vM1 = *(pSrcCurRow + (4 * (d3dsd.Width - 1)));
            else
                vM1 = *(pSrcB0-4);
            v10 = *(pSrcB1+0);
            v1M = *(pSrcB2+0);

            LONG iDu = (vM1-v01); // The delta-u bump value
            LONG iDv = (v1M-v10); // The delta-v bump value
 
       
            *pDstT++ = (BYTE)(iDu / 2);
            *pDstT++ = (BYTE)(iDv / 2);
                   

            // Move one pixel to the right (src is 32-bpp)
            pSrcB0+=4;
            pSrcB1+=4;
            pSrcB2+=4;
        }

        // Move to the next line
        pSrcCurRow += dwSrcPitch;
        pDstCurRow += dwDstPitch;
    }

    m_psBumpMap->UnlockRect(0);
    psBumpSrc->UnlockRect(0);

    return S_OK;
}

}

}

