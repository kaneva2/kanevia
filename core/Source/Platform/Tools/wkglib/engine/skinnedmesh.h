/******************************************************************************
 skinnedmesh.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "mesh.h"

namespace wkg
{

class XFormNode;
class WKGVertexWeights;
class MatrixPaletteNode;

class SkinnedMesh : public Mesh
{
public:
	SkinnedMesh();
	~SkinnedMesh();

	virtual bool Load(WKBFile &wkbfile);

	int32 GetNumBones() { return m_num_bones; }
	XFormNode** GetBones() { return m_bones; }
	XFormNode* GetSkeletonRoot() { return m_skeletal_root; }
	MatrixPaletteNode* GetMatrixPalette() { return m_palette_node; }
	LPDIRECT3DVERTEXBUFFER9 GetWeightVB() { return m_weights_vb; }

protected:
	// this creates the vertex buffer for our weight info
	void SetVertexWeights(uint32 num_verts, WKGVertexWeights *v);

protected:
	int32 m_num_bones;
	// this is all the nodes for each bone
	XFormNode **m_bones;
	// this is the scene graph for our skeleton
	XFormNode *m_skeletal_root;
	// and this gives us the matrices we need for paletting
	MatrixPaletteNode *m_palette_node;

	// our vertex weight info
	LPDIRECT3DVERTEXBUFFER9 m_weights_vb;
};

}

