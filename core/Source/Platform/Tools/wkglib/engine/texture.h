/******************************************************************************
 texture.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <string>
#include "refcounter.h"

namespace wkg {
namespace engine {

class Texture : public RefCounter<Texture>
{
    public:
		Texture();
	    virtual ~Texture();

		bool Load(const char* filename, bool warn = true);
		bool LoadCubeMap(const char* filename);

		bool Create(uint32 width, uint32 height, 
			D3DFORMAT Fmt = D3DFMT_A8R8G8B8, 
			uint32 Usage = 0, 
			uint32 Levels = 1, 
			D3DPOOL Pool = D3DPOOL_DEFAULT);

	    LPDIRECT3DTEXTURE9 Interface() const;
	    LPDIRECT3DBASETEXTURE9 BaseInterface() const;
	    LPDIRECT3DCUBETEXTURE9 CubeInterface() const;

		std::string GetFilename() const;
		int32 GetSourceImageWidth() const;
		int32 GetSourceImageHeight() const;

    protected:
	    LPDIRECT3DBASETEXTURE9 m_tex;
		std::string m_filename;
		int32 m_srcWidth;
		int32 m_srcHeight;

		static int32 m_total_size;
};

inline Texture::Texture()
	: m_tex(0),	m_srcWidth(0), m_srcHeight(0)
{
}

inline Texture::~Texture()
{
	if (m_tex)
	{
		m_tex->Release();
		m_tex = 0;

		m_total_size -= m_srcWidth * m_srcHeight * 4;

	}
}

inline LPDIRECT3DTEXTURE9 Texture::Interface() const 
{
	return LPDIRECT3DTEXTURE9(m_tex); 
}

inline LPDIRECT3DBASETEXTURE9 Texture::BaseInterface() const 
{
	return m_tex; 
}

inline LPDIRECT3DCUBETEXTURE9 Texture::CubeInterface() const 
{ 
	return LPDIRECT3DCUBETEXTURE9(m_tex);
}

inline std::string Texture::GetFilename() const 
{ 
	return m_filename;
}

inline int32 Texture::GetSourceImageWidth() const 
{ 
	return m_srcWidth; 
}

inline int32 Texture::GetSourceImageHeight() const 
{
	return m_srcHeight;
}

}}
