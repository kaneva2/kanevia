/******************************************************************************
 skeletalanimation.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

// a class to aid in the building of a controller graph for
//  a number of named keyframed bones

#include <string>
#include <vector>
#include "vector3.h"
#include "animation.h"
#include "animnotify.h"
#include "controllable.h"

namespace wkg
{

class WKAFile;
class Node;

namespace engine
{

class Animation;
class PosMPlexController;
class OrientMPlexController;
class PosOrientToMatrix44Controller;
class AnimMotionNotify;

class SkeletalAnimation : public AnimNotify, public Controllable
{
	public:
		SkeletalAnimation();
		virtual ~SkeletalAnimation();

		bool Create(const char *name, const char *filename);

		void Play(int32 mode = Animation::PLAY_LOOPING, bool reset = true);
		void Stop();

		const char *GetName() { return m_name.c_str(); }

		int32 GetStartFrame() { return m_frame_start; }
		int32 GetEndFrame()   { return m_frame_end;   }

		int32 GetBoneWeight(int32 idx) 
		{ 
			if(idx >= 0 && uint32(idx) < m_weights.size())
				return m_weights[idx]; 
			else
				return 0;
		}

		Animation *GetAnimation(int32 idx) 
		{ 
			//  otherwise it'll add an entry
			if(idx >= 0 && uint32(idx) < m_animations.size())
				return m_animations[idx]; 
			else
				return 0;
		}

		Animation *GetAnimationByName(const char *name);


		// when you start an animation, you can specify the mode:
		//  Animation::PLAY_LOOPING or Animation::PLAY_ONCE
		// if looping is selected, it starts immediately and never stops.
		// if play once is selected, it starts immediately (ie, a call to IsPlaying() will
		//  reutrn true), and finishes when all the animations of all the bones are done,
		//  so IsPlaying will go false only then.
		// in effect, if you want to play an animation once, and find out if it's done,
		//  then use this function.
		bool IsPlaying() { return m_is_playing; }

		const Vector3& GetVelocity() { return m_velocity; }
		void SetVelocity(const Vector3& vel) { m_velocity = vel; }
		void SetVelocity(real x, real y, real z) { m_velocity.Set(x, y, z); }

		void AnimDone(Animation *which);

		std::vector<Vector3> &GetOffsets() { return m_offsets; }

		int32 GetCurrFrame();

		void AddAnimMotionNotify(AnimMotionNotify *amn);
		void RemoveAnimMotionNotify(AnimMotionNotify *amn);

		virtual void ControllableUpdate();

		void BeginTransition(real transition_time);

	protected:
		std::vector<Animation *> m_animations;
		std::vector<int32> m_weights;

		std::vector<Vector3> m_offsets;

		bool m_is_playing;
		std::string m_name;

		int32 m_frame_start;
		int32 m_frame_end;

		Vector3 m_velocity;

		int32 m_last_frame;

		int32 m_mode;

		std::vector<AnimMotionNotify *> m_amn;

		static std::map<std::string, WKAFile *> m_wka_cache;
};

}

}