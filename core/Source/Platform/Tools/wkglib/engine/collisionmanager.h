/******************************************************************************
 collisionmanager.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "singleton.h"
#include "vector3.h"
#include "Collision.h"
#include <set>

namespace wkg
{

namespace engine
{

class OctNode;
class Collidable;
class Collision;

struct t_collision
{
	Collidable *c1;
	Collidable *c2;
	Collision *c;
};

class CollisionManager : public Singleton<CollisionManager>
{
public:
	CollisionManager() {}
	~CollisionManager() {}

	int32 Find(Collidable *c)
	{
		for(int32 i = 0; i < m_collidables.size(); i++)
		{
			if(m_collidables[i] == c) return i;
		}
		return -1;
	}

	void AddCollidable(Collidable *c) 
	{
		if(Find(c) < 0)
			m_collidables.push_back(c);
	}

	void RemoveCollidable(Collidable *c)
	{
		int32 idx = Find(c);
		if(idx >= 0)
			m_collidables.erase(m_collidables.begin() + idx);
	}

	void Collide(uint32 ms);

	void Render();

	// for debug rendering
	void AddRenderCollision(Vector3 c, uint32 color = 0xffff0000);
	void RenderCollisions();

	void RenderCollision(Vector3 c, Vector3 size, Vector3 axis[3]);

	struct box_render_collisions
	{
		Vector3 c;
		Vector3 s;
		Vector3 a[3];
	};

	void AddRenderCollision(Vector3 c, Vector3 s, Vector3 a[3])
	{
		box_render_collisions brc;
		brc.c = c;
		brc.s = s;
		brc.a[0] = a[0];
		brc.a[1] = a[1];
		brc.a[2] = a[2];
		
		m_box_collisions_to_render.push_back(brc);
	}

	void AddCollision(Collidable *c1, Collidable *c2, Collision *c)
	{
		t_collision tc;
		tc.c1 = c1;
		tc.c2 = c2;
		tc.c = c;

		if(EQUAL(0.0f, c->m_t))
		{
			m_unsorted_collisions.push_back(tc);
		}
		else
		{
			m_collisions.push_back(tc);
		}

	}

	void Callbacks();

	void DeleteMe(Collidable *c)
	{
		m_to_delete.insert(c);
	}


protected:

	std::set<Collidable *> m_to_delete;

	void RenderCollision(const Vector3 &c, uint32 color);
	void ClearRenderCollisions();
	
	void Collide(Collidable *c1, Collidable *c2, uint32 ms);
	void SortCollisions();

protected:
	std::vector<Collidable *> m_collidables;

	std::vector<Vector3> m_collisions_to_render;
	std::vector<uint32> m_colors;

	std::vector<t_collision> m_collisions;
	std::vector<t_collision> m_unsorted_collisions;

	std::vector<box_render_collisions> m_box_collisions_to_render;
};

}

}