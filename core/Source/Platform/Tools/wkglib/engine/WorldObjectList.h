/******************************************************************************
 WorldObjectList.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "singleton.h"
#include <map>

namespace wkg
{

namespace engine
{
class WorldObject;

class WorldObjectList : public Singleton<WorldObjectList>
{
public:
	WorldObjectList();
	virtual ~WorldObjectList();

	void AddToList(WorldObject* wo);
	void RemoveFromList(WorldObject* wo);
	WorldObject* GetWorldObject(int32 id);
	WorldObject* GetClosestWorldObject(Vector3& pos);

	int32 GetNum() { return m_wo_vector.size(); }
	WorldObject *Get(int32 idx) { return m_wo_vector[idx]; }	
	WorldObject* GetByID(int32 id);

	virtual void OnUpdate(uint32 ms);

protected:
	std::map<int32, WorldObject*> m_wo_map;
	std::vector <WorldObject*> m_wo_vector;

};
}
}