/******************************************************************************
 keyframecontroller.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "timedupdatable.h"
#include "controller.h"

namespace wkg
{

namespace engine
{

class KeyframeNotify;

class KeyframeController : public TimedUpdatable, public Controller
{
	public:
		KeyframeController(int32 startf, int32 endf, 
			int32 *frame, int32 *next_frame, real *partial_frame, 
			int32 mspf);

		virtual ~KeyframeController();

		virtual void TimedUpdate(int32 mspf);

		bool IsLooping() { return m_looping; }
		void SetLooping(bool bEnable = true) { m_looping = bEnable; }

		void AddKeyframeNotify(KeyframeNotify *kfn);
		void RemoveKeyframeNotify(KeyframeNotify *kfn);

		void BeginTransition(real transition_time);

		void AnimDone();

		void Reset()
		{
			(*m_frame) = m_start;
		}

	protected:
		int32 m_mspf;
		int32 *m_frame;
		int32 *m_next_frame;
		real *m_partial_frame;

		int32 m_start;
		int32 m_end;

		int32 m_accumulator;

		bool m_looping;

		real m_transition_time;
		real m_transition_time_left;
		bool m_transitioning;

		std::vector<KeyframeNotify *> m_kfn;
};


}

}



