/******************************************************************************
 Stripify.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Stripify.h"

#include <nvidia/NvTriStrip.h>

namespace wkg {

bool 
Stripify::Init()
{
	uint32 cacheSize;

	// todo: determine which GPU were talking to and set cache size appropriately
	//
	cacheSize = CACHESIZE_GEFORCE3;

	::SetCacheSize(cacheSize);

	return true;
}

void
Stripify::Uninit()
{
}

void 
Stripify::SetStitchStrips(bool bStitchStrips)
{
	::SetStitchStrips(bStitchStrips);
}

void 
Stripify::SetMinStripSize(uint32 minSize)
{
	::SetMinStripSize(minSize);
}

void 
Stripify::SetListsOnly(bool bListsOnly)
{
	::SetListsOnly(bListsOnly);
}

#if 1
void 
Stripify::GenerateStrips(const uint16* in_indices, uint32 in_numIndices, Stripify::PrimitiveGroup** primGroups, uint16* numGroups)
{
	::GenerateStrips(in_indices, in_numIndices, (::PrimitiveGroup**)primGroups, numGroups);
}

void 
Stripify::RemapIndices(Stripify::PrimitiveGroup* pg, uint16 numGroups, uint16 numVerts, 
	uint8* vb, uint32 vbVertSize,
	uint16* idx[])
{
	Stripify::PrimitiveGroup* newPG = 0;
	int32 group, oldNdx, newNdx;
	uint16 i;
	uint8* newvb;

	newvb = new uint8[numVerts * vbVertSize];
	if (!newvb)
		return;		// not remapped

	::RemapIndices((::PrimitiveGroup*)pg, numGroups, numVerts, (::PrimitiveGroup**)&newPG);

	// move verts into new order (note that this will do numIndices copies instead of numVerts copies,
	// which is extraneous.  Deal with it! ;-)
	//
	for (group = 0; group < numGroups; group++)
	{
		for (i = 0; i < newPG[group].numIndices; i++)
		{
			oldNdx = pg[group].indices[i];
			newNdx = newPG[group].indices[i];

			memcpy(newvb + newNdx * vbVertSize, vb + oldNdx * vbVertSize, vbVertSize);
		}
	}

	memcpy(vb, newvb, numVerts * vbVertSize);
	delete [] newvb;

	for (i = 0; i < numGroups; i++)
	{
		ASSERT(pg[i].type == newPG[i].type &&
			pg[i].numIndices == newPG[i].numIndices);

		delete [] pg[i].indices;
		pg[i].indices = newPG[i].indices;
		newPG[i].indices = 0;

		memcpy(idx[i], pg[i].indices, pg[i].numIndices * sizeof(uint16));
	}

	delete [] newPG;
}
#else
extern IDirect3DDevice8* dev;
ID3DXMesh* mesh = 0;
void 
Stripify::GenerateStrips(const uint16* in_indices, uint32 in_numIndices, Stripify::PrimitiveGroup** primGroups, uint16* numGroups)
{
	uint16 i;
	uint16 minIdx = 65535, maxIdx = 0;

	for (i = 0; i < in_numIndices; i++)
	{
		if (in_indices[i] > maxIdx)
			maxIdx = in_indices[i];
		if (in_indices[i] < minIdx)
			minIdx = in_indices[i];
	}
	int32 numVerts = maxIdx - minIdx + 1;

	uint16* idx;
	DWORD* adj;
	ASSERT((in_numIndices % 3) == 0);
	D3DXCreateMeshFVF(in_numIndices / 3, numVerts, D3DXMESH_SYSTEMMEM, D3DFVF_DIFFUSE, dev, &mesh);
	mesh->LockIndexBuffer(0, (BYTE**)&idx);
	memcpy(idx, in_indices, in_numIndices * sizeof(uint16));
	mesh->UnlockIndexBuffer();
	adj = new DWORD[3 * mesh->GetNumFaces()];
	mesh->GenerateAdjacency(0.0f, adj);

	DWORD* v;
	mesh->LockVertexBuffer(0, (BYTE**)&v);	// track where each vert is moved
	for (int32 j = 0; j < numVerts; j++)
		v[j] = j;
	mesh->UnlockVertexBuffer();

	ID3DXMesh* newMesh;
	mesh->Optimize(D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_STRIPREORDER, adj, 0, 0, 0, &newMesh);
	delete [] adj;
	mesh->Release();
	mesh = newMesh;

	LPDIRECT3DINDEXBUFFER8 stripIB;
	uint32 stripIBCount;

	D3DXConvertMeshSubsetToSingleStrip(mesh, 0, D3DXMESH_IB_SYSTEMMEM, &stripIB, &stripIBCount);

	*primGroups = new Stripify::PrimitiveGroup[1];
	*numGroups = 1;

	stripIB->Lock(0, 0, (BYTE**)&idx, 0);
	(*primGroups)[0].type = Stripify::PrimitiveGroup::STRIP;
	(*primGroups)[0].numIndices = stripIBCount;
	(*primGroups)[0].indices = new uint16[stripIBCount];
	memcpy((*primGroups)[0].indices, idx, stripIBCount * sizeof(uint16));
	stripIB->Unlock();
	stripIB->Release();
}

void 
Stripify::RemapIndices(Stripify::PrimitiveGroup* pg, uint16 numGroups, uint16 numVerts, 
	uint8* vb, uint32 vbVertSize,
	uint16* idx[])
{
	memcpy(idx[0], pg[0].indices, pg[0].numIndices * sizeof(uint16));

	DWORD* remap;
	uint8* newvb = new uint8[numVerts * vbVertSize];
	mesh->LockVertexBuffer(0, (BYTE**)&remap);
	for (int32 i = 0; i < numVerts; i++)
		memcpy(newvb + i * vbVertSize, vb + remap[i] * vbVertSize, vbVertSize);
	memcpy(vb, newvb, numVerts * vbVertSize);
	mesh->UnlockVertexBuffer();
	delete [] newvb;

	mesh->Release();
	mesh = 0;
}
#endif

}