/******************************************************************************
 Model.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "model.h"
#include "vector4.h"
#include "scenegraph.h"
#include "meshfactory.h"
#include "mesh.h"
#include "meshnode.h"
#include "XMLNode.h"
#include "XMLNodeAttributes.h"
#include "XMLDocument.h"
#include "AnimatedMesh.h"
#include "meshrenderer.h"
#include "effectshader.h"
#include "shadowmapmanager.h"
#include "effectmeshrenderer.h"

namespace wkg 
{

namespace engine 
{

Model::Model() :
	RigidBody(),
	m_pMesh(0),
	m_pNode(0),
	m_pSNode(0),
	m_pParentNode(0)
{
}

Model::~Model()
{
	if (m_pMesh)
	{
		m_pMesh->Release();
		m_pMesh = 0;
	}

	if (m_pParentNode && m_pNode)
	{
		m_pParentNode->RemoveChild(m_pNode);
	}

	delete m_pNode;
	m_pNode = 0;
}

bool
Model::Load(const char* filename)
{
#if 0

	bool res = false;
	XMLDocument *xml_doc = XMLDocument::CreateFromFile(filename);
	if(xml_doc)
	{
		res = Load(xml_doc);
	}

	delete xml_doc;
	return res;
#endif
#if 1
	MeshDescriptor msd;

	msd.filename = filename;
	msd.type = MeshDescriptor::STANDARD_MESH;
	m_pMesh = MeshFactory::Instance()->Get(msd);
	if (!m_pMesh || 0 == m_pMesh->GetNumSections())
		return false;

	m_pNode = new MeshNode();
	m_pNode->SetMesh(m_pMesh);

	EffectShader *es = new EffectShader();
	es->Create("effects/material.fx");
	m_pNode->SetRenderer(new EffectMeshRenderer(es));


	m_pSNode = new MeshNode();
	m_pSNode ->SetMesh(m_pMesh);
	es = new EffectShader();
	es->Create("effects/depth.fx");
	m_pSNode ->SetRenderer(new EffectMeshRenderer(es));
	ShadowMapManager::Instance()->AddChild(m_pSNode);

	return true;
#endif
}

bool
Model::Load(XMLDocument *doc)
{
	bool res = true;
	XMLNode *curr = doc->GetRootNode();
	curr = curr->GetFirstChild()->GetFirstChild();

	while(curr)
	{
		res = LoadXMLNode(curr);
		if(!res)
		{
			// This assertion failing means the LoadXMLNode function couldn't load the node
			ASSERT(res);			
		}
		curr = curr->GetNextSibling();
	}
	return res;
}

bool
Model::LoadXMLNode(XMLNode* curr)
{
	std::string node_type = curr->GetAttributes()->FindAttributeValue("node_type");
	if("Model" == node_type)
	{
		curr = curr->GetFirstChild();
		std::string mesh_filename = curr->GetAttributes()->FindAttributeValue("mesh_filename");
		m_pMesh = new AnimatedMesh();
		((Mesh *)m_pMesh)->Load(mesh_filename.c_str());
	}
	else if("WorldObject" == node_type)
	{
		return WorldObject::LoadXMLNode(curr);
	}
	return false;
}

void Model::SetParent(Node* pParentNode)
{
	if(!m_pNode) return;
	Matrix44 L2W(m_pNode->GetLocalToWorld());
	Matrix44 mat(m_pNode->GetMatrix());
	mat = mat*L2W;

	if (m_pParentNode)
	{
		m_pParentNode->RemoveChild(m_pNode);
	}
	
	if (pParentNode)
	{
		pParentNode->AddChild(m_pNode);
		XFormNode* pXFormNode = dynamic_cast<XFormNode*>(pParentNode);
		if (pXFormNode)
		{
			Matrix44 parentW2L(pXFormNode->GetLocalToWorld());
//			parentW2L.Inverse();
//			mat *= parentW2L;
		}
	}

	m_pNode->SetMatrix(mat);			

	m_pParentNode = pParentNode;
}

void Model::SetPosition(const Vector3 &position)
{
	if(!m_pNode) return;
	RigidBody::SetPosition(position);
	Matrix44 m(m_pNode->GetMatrix());
	m.NoTranslation();
	m._41 = position.x;
	m._42 = position.y;
	m._43 = position.z;
	m_pNode->SetMatrix(m);
	if(m_pSNode)
		m_pSNode->SetMatrix(m);
}

void Model::SetOrientation(const Quaternion &orientation)
{
	if(!m_pNode) return;
	RigidBody::SetOrientation(orientation);
	Matrix44 m(m_pNode->GetMatrix());
	m.NoRotation();
	Matrix44 rot(orientation);
	m = rot*m;
	m_pNode->SetMatrix(m);
	if(m_pSNode)
		m_pSNode->SetMatrix(m);
}

}

}
