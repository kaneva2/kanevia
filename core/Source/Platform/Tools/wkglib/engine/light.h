/******************************************************************************
 light.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

namespace engine
{

class Light
{
public:
	Light() : m_position(0.0f, 0.0f, 0.0f) { }
	~Light() { }

	Vector3 GetPosition() const				{ return m_position; }
	void SetPosition(const Vector3 &pos)	{ m_position = pos;  }

	Vector3 GetColor() const				{ return m_color; }
	void SetColor(const Vector3 &color)		{ m_color = color;  }

	Vector3 GetFalloff() const				{ return m_falloff; }
	void SetFalloff(const Vector3 &falloff)	{ m_falloff = falloff;  }

protected:
	Vector3 m_position;
	Vector3 m_color;
	Vector3 m_falloff;
};

}

}

