/******************************************************************************
 worldobject.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "XMLDocument.h"
#include "XMLNode.h"
#include "XMLNodeAttributes.h"
#include "boundingvolume.h"
#include "vector3.h"
#include "collision.h"
#include "BVEllipsoid.h"
#include "BVBox.h"
#include "BVSphere.h"
#include "worldobjectlist.h"
#include "worldobject.h"

namespace wkg
{

namespace engine
{
//std::vector<WorldObject*> WorldObject::m_wo_list;

WorldObject::WorldObject() : 
	Collidable(),
	Messenger(),
	m_position(0.0f, 0.0f, 0.0f),
	m_orientation(0.0f, 0.0f, 0.0f, 1.0f),
	m_velocity(0.0f, 0.0f, 0.0f),
	m_omega(0.0f, 0.0f, 0.0f),
	m_bv_offset(0.0f, 0.0f, 0.0f),
	m_mass(1.0f)
{
	WorldObjectList::Instance()->AddToList(this);
}

WorldObject::~WorldObject()
{
	WorldObjectList::Instance()->RemoveFromList(this);
}

void 
WorldObject::SetPosition(const Vector3 &position)	
{
	// each world object has a base position which may be different from the 
	//  position of the bounding volume.  an example would be a bounding sphere
	//  around a player, where the center is at the player's hips, but the player's
	//  base position is at his feet.
	m_position = position; 
}

void 
WorldObject::SetOrientation(const Quaternion &orientation) 
{ 
	m_orientation = orientation; 
}

void 
WorldObject::SetVelocity(const Vector3 &velocity)
{
	m_velocity = velocity; 
}

void 
WorldObject::SetAngularVelocity(Vector3 &v)
{
	m_omega = v;
}

void 
WorldObject::SetMass(real mass)
{
	m_mass = mass;
}

void 
WorldObject::SetBVOffset(Vector3 &bvoffset)
{
	m_bv_offset = bvoffset;
}

void 
WorldObject::OnCollide(Collidable *other, Collision *c)
{
}

bool
WorldObject::LoadXMLNode(XMLNode* curr)
{
	std::string node_type = curr->GetAttributes()->FindAttributeValue("node_type");
	if("WorldObject" == node_type)
	{
		curr = curr->GetFirstChild();

		std::string bv_type = curr->GetAttributes()->FindAttributeValue("bv_type");
		m_bulletParticleHit = curr->GetAttributes()->FindAttributeValue("bullet_particle_hit");

		std::string sbv_x = curr->GetAttributes()->FindAttributeValue("bounding_volume_x");
		std::string sbv_y = curr->GetAttributes()->FindAttributeValue("bounding_volume_y");
		std::string sbv_z = curr->GetAttributes()->FindAttributeValue("bounding_volume_z");
		if(sbv_x.size() && sbv_y.size() && sbv_z.size())
		{
			real bv_x = atof(sbv_x.c_str());
			real bv_y = atof(sbv_y.c_str());
			real bv_z = atof(sbv_z.c_str());
			
			if("ellipsoid" == bv_type)
			{
				SetBoundingVolume(new BVEllipsoid(this, Vector3(bv_x, bv_y, bv_z)));
			}
			else if("box" == bv_type)
			{
				SetBoundingVolume(new BVBox(this, Vector3(bv_x, bv_y, bv_z)));
			}
			else if("sphere" == bv_type)
			{
				SetBoundingVolume(new BVSphere(this, bv_x));
			}
			else
			{
				SetBoundingVolume(new BVEllipsoid(this, Vector3(bv_x, bv_y, bv_z)));
			}

		}

		std::string sbv_off_x = curr->GetAttributes()->FindAttributeValue("bounding_offset_x");
		std::string sbv_off_y = curr->GetAttributes()->FindAttributeValue("bounding_offset_y");
		std::string sbv_off_z = curr->GetAttributes()->FindAttributeValue("bounding_offset_z");
		if(sbv_off_x.size() && sbv_off_y.size() && sbv_off_z.size())
		{
			real bv_off_x = atof(sbv_off_x.c_str());
			real bv_off_y = atof(sbv_off_y.c_str());
			real bv_off_z = atof(sbv_off_z.c_str());

			SetBVOffset(Vector3(bv_off_x, bv_off_y, bv_off_z));
		}

		std::string smass = curr->GetAttributes()->FindAttributeValue("mass");
		if(smass.size())
		{
			real mass = atof(smass.c_str());
			SetMass(mass);
		}

		return true;
	}
	return false;
}

}

}
