/******************************************************************************
 lightmapeffect.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "matrix44.h"
#include "vector4.h"
#include "camera.h"
#include "uimanager.h"
#include "ui.h"
#include "globals.h"
#include "lightmapeffect.h"

namespace wkg
{

namespace engine
{

LightmapEffect::LightmapEffect() : EffectShader()
{
}

LightmapEffect::~LightmapEffect()
{
}

void 
LightmapEffect::Prepare(Mesh *mesh)
{
	Matrix44 world, view, proj, wvp;

	Device::Instance()->GetTransform(D3DTS_WORLD, &world);
	Device::Instance()->GetTransform(D3DTS_VIEW, &view);
	Device::Instance()->GetTransform(D3DTS_PROJECTION, &proj);

	real bias;
	Globals::Instance()->GetValue("LightmapBias", &bias);

	Camera *pCam = wkg::game::UIManager::Instance()->GetCurrentUI()->GetCamera();
	D3DXMatrixPerspectiveFovLH(
		&proj, 
		DEG_TO_RAD(pCam->GetFOVYDegrees()), 
		pCam->GetAspectRatio(), 
		pCam->GetNearDistance() + bias, 
		pCam->GetFarDistance() + bias);
//	Device::Instance()->SetTransform(D3DTS_PROJECTION, &proj);

	wvp = world * view * proj;
	wvp.Transpose();
	Device::Instance()->SetVertexShaderConstantF(0, &wvp, 4);

	Matrix44 world_inv = world;
	world_inv.Inverse();
	world_inv.Transpose();
	Device::Instance()->SetVertexShaderConstantF(4, &world_inv, 4);


	Matrix44 view2world = view;
	view2world.Inverse();
	Vector4 cam(0.0f, 0.0f, 0.0f, 1.0f);
	cam *= view2world;

	Device::Instance()->SetVertexShaderConstantF(8, &cam,  1);
//	cam.Dump();

	world.Transpose();
	Device::Instance()->SetVertexShaderConstantF(9, &world, 1);
}

}

}

