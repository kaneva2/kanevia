/******************************************************************************
 physics.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "singleton.h"
#include "ode.h"
#include "vector3.h"
#include <vector>

namespace wkg
{

namespace game
{
class Prop;
}

namespace engine
{

class RigidBody;
class Collidable;
class WorldObject;
class OctNode;

class Physics : public Singleton<Physics>
{
	public:
		// used to hide the ode glue from the interface
		friend class RigidBody;

		Physics();
		virtual ~Physics();

		void SetGravity(Vector3 &gravity);

		void Update(uint32 ms);

		void NearCallback(dGeomID o1, dGeomID o2);
		
		void AddRigidBody(RigidBody *b);
		void AddSimpleBody(RigidBody *b);
		void RemoveRigidBody(RigidBody *b);

		bool IsBodyInSim(WorldObject *wo);

		void SetStaticWorld(OctNode *root, std::vector<Vector3> &verts);
		void StaticWorldUpdate(RigidBody *rb, uint32 ms);

		void CreateBallJoint(RigidBody *b1, RigidBody *b2, Vector3 &pos);

		void CreatePropChain(
			Vector3 &start, 
			Vector3 &delta, 
			int32 num,
			std::string &body_name, 
			std::vector<game::Prop *> &bodies);

		dSpaceID GetSpace()			{	return m_space;	}

	protected:
		bool ExistCollision(WorldObject *wo);

	protected:
		std::vector<RigidBody *> m_bodies;
		std::vector<RigidBody *> m_simple_bodies;

		Vector3 m_gravity;

		dWorldID m_world;
		dSpaceID m_space;

		dJointGroupID m_contactgroup;

		OctNode *m_static_world;
};

}

}


