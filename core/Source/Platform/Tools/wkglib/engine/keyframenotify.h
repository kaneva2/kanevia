/******************************************************************************
 keyframenotify.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg
{

class KeyframeController;

class KeyframeNotify
{
	public:
		virtual void AnimDone(KeyframeController *which) = 0;
};

}

