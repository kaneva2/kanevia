/******************************************************************************
 bvellipsoid.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "boundingvolume.h"

namespace wkg
{

namespace engine
{

class WorldObject;

class BVEllipsoid : public BoundingVolume
{
public:
	BVEllipsoid(WorldObject *wo, Vector3 radius);
	virtual ~BVEllipsoid();

	Vector3 GetRadius()				{ return m_radius; }
	virtual Collision *Collide(BoundingVolume *other, uint32 ms);

	virtual real GetBVSphereRadius() 
	{ 
		if(m_radius.x > m_radius.z) return m_radius.x;
		return m_radius.z;
	}

protected:
	Vector3 m_radius;
};

}

}

