/******************************************************************************
 keyframecontroller.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"

#include "keyframenotify.h"
#include "keyframecontroller.h"

namespace wkg
{

namespace engine
{

KeyframeController::KeyframeController(int32 startf, int32 endf, 
									   int32 *frame, int32 *next_frame,
									   real *partial_frame,
									   int32 mspf) :
	TimedUpdatable(),
	m_frame(frame),
	m_next_frame(next_frame),
	m_partial_frame(partial_frame),
	m_mspf(mspf),
	m_start(startf),
	m_end(endf),
	m_accumulator(0),
	m_looping(false),
	m_transition_time(0.0f),
	m_transitioning(false)

{
	ASSERT(m_frame);
	*m_frame = startf;
}

KeyframeController::~KeyframeController()
{
}

void 
KeyframeController::BeginTransition(real transition_time)
{
	m_transitioning = true;
	m_transition_time = m_transition_time_left = transition_time;
}

void 
KeyframeController::TimedUpdate(int32 mspf)
{
	if(m_transitioning)
	{
		// special case for transition
		(*m_frame) = -1;
		(*m_next_frame) = m_start;
		m_transition_time_left -= mspf;
		if(m_transition_time_left < 0.0f)
		{
			m_transition_time_left = 0.0f;
			m_transitioning = false;
		}

		(*m_partial_frame) = 1.0f - (m_transition_time_left / m_transition_time);

	}
	else
	{
		int32 delta = 1;
		if(mspf < 0)
		{
			delta = -delta;
			mspf = -mspf;
		}

		bool anim_done = false;
		m_accumulator += mspf;
		while(m_accumulator > m_mspf)
		{	
			// normal case - within the bounds
			(*m_frame) += delta;

			if((*m_frame) > m_end)
			{
				if(m_looping)
				{
					(*m_frame) = m_start;
				}
				else
				{
					(*m_frame) = m_end;
					anim_done = true;
					AnimDone();
					break;
				}
			}

			// special case - animating backward
			if((*m_frame) < m_start)
			{
				if(m_looping)
				{
					(*m_frame) = m_end;
				}
				else
				{
					(*m_frame) = m_start;
					anim_done = true;
					AnimDone();
					break;
				}
			}

			m_accumulator -= m_mspf;
		}

		if(anim_done)
		{
			(*m_next_frame) = (*m_frame);
			(*m_partial_frame) = 0.0f;
		}
		else
		{
			(*m_next_frame) = (*m_frame) + delta;
			if(*m_next_frame < m_start)
			{
				if(m_looping)
					(*m_next_frame) = m_end;
				else
					(*m_next_frame) = m_start;
			}
			else if(*m_next_frame > m_end)
			{
				if(m_looping)
					(*m_next_frame) = m_start;
				else
					(*m_next_frame) = m_end;
			}

			(*m_partial_frame) = (real)m_accumulator / (real)m_mspf;
		}
	}

	ControllerUpdate();
}

void
KeyframeController::AnimDone()
{
	StopUpdates();

	std::vector<KeyframeNotify *>::iterator itor = m_kfn.begin();
	std::vector<KeyframeNotify *>::iterator iend = m_kfn.end();
	for(; itor < iend; itor++)
	{
		(*itor)->AnimDone(this);
	}
}

void 
KeyframeController::AddKeyframeNotify(KeyframeNotify *kfn)
{
//#ifdef _DEBUG
	std::vector<KeyframeNotify *>::iterator itor = m_kfn.begin();
	std::vector<KeyframeNotify *>::iterator iend = m_kfn.end();
	for(; itor < iend; itor++)
	{
		if(kfn == *itor)
		{
//			ASSERT(0);
			return;
		}
	}
//#endif

	m_kfn.push_back(kfn);
}

void 
KeyframeController::RemoveKeyframeNotify(KeyframeNotify *kfn)
{
	std::vector<KeyframeNotify *>::iterator itor = m_kfn.begin();
	std::vector<KeyframeNotify *>::iterator iend = m_kfn.end();
	for(; itor < iend; itor++)
	{
		if(kfn == *itor)
		{
			m_kfn.erase(itor);
			return;
		}
	}
}

}

}



