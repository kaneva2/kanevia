/******************************************************************************
 toonshader.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "effectshader.h"

namespace wkg
{

namespace engine
{

class ToonShader : public EffectShader
{
public:
	virtual bool Create(const char *filename);
	virtual void Prepare(Mesh *mesh);

protected:
	LPDIRECT3DTEXTURE8 m_tex;

};

}

}

