/******************************************************************************
 watereffectshader.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "effectshader.h"

namespace wkg
{

namespace engine
{

class WaterEffectShader : public EffectShader
{
public:
	virtual bool Create(const char *filename);
	virtual void Prepare(Mesh *mesh);

protected:
    HRESULT InitBumpMap();

protected:
	LPDIRECT3DTEXTURE8 m_pTex;
	LPDIRECT3DTEXTURE8 m_pHeightTex;
	LPDIRECT3DTEXTURE8 m_psBumpMap;
	LPDIRECT3DCUBETEXTURE8 m_pCubeTex;
};

}

}

