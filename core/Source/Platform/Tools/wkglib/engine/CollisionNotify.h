/******************************************************************************
 CollisionNotify.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#ifndef _COLLISIONNOTIFY_H_
#define _COLLISIONNOTIFY_H_

namespace wkg 
{

namespace engine
{

class Collidable;

class CollisionNotify
{
	public:
		CollisionNotify();
		virtual ~CollisionNotify();

		virtual void OnCollide(Collidable *other);

	protected:
};

}
}
#endif

