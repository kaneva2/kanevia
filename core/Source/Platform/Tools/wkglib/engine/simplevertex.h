/******************************************************************************
 simplevertex.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#ifndef _SIMPLE_VERTEX_H_
#define _SIMPLE_VERTEX_H_

#define FVF_SIMPLE_VERTEX (D3DFVF_XYZ)

namespace wkg
{

namespace engine
{

class SimpleVertex
{
	public:
		real x, y, z;
};

}

}

#endif

