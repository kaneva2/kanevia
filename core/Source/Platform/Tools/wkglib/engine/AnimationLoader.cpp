/******************************************************************************
 AnimationLoader.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "AnimatedMesh.h"
#include "FileStream.h"
#include "XMLDocument.h"
#include "XMLNode.h"
#include "XMLNodeAttributes.h"


#pragma warning (disable : 4786)
#pragma warning (disable : 4503)
#include "AnimationLoader.h"

namespace wkg
{

namespace game
{

AnimationLoader::AnimationLoader()
{
}

AnimationLoader::~AnimationLoader()
{
}

bool
AnimationLoader::Load(std::string filename)
{
	XMLDocument* xml_doc = CreateXMLDoc(filename);
	if(xml_doc)
	{
		XMLNode *curr = xml_doc->GetRootNode();
		curr = curr->GetFirstChild()->GetFirstChild();
		ASSERT(curr);

		while(curr)
		{
			std::string name = curr->GetAttributes()->FindAttributeValue("name");

			NameAnimMap name_anim_map;

			Load(curr->GetFirstChild(), name_anim_map);
			m_anim_set_map[name] = name_anim_map;

			curr = curr->GetNextSibling();
		}
	}

	return true;
}

void
AnimationLoader::Load(XMLNode* curr, NameAnimMap &name_anim_map)
{
	std::string name;
	std::string filename;
	while(curr)
	{
		name = curr->GetAttributes()->FindAttributeValue("name");
		filename = curr->GetAttributes()->FindAttributeValue("filename");

		name_anim_map[name] = filename;

		curr = curr->GetNextSibling();
	}
}

bool 
AnimationLoader::ApplyAllToAnimatedMesh(engine::AnimatedMesh *mesh)
{
	mesh->ClearAnimations();
	AnimSetMap::iterator itor = m_anim_set_map.begin();
	for(; itor != m_anim_set_map.end(); itor++)
	{
		NameAnimMap::iterator itor2 = itor->second.begin();
		for(; itor2 != itor->second.end(); itor2++)
		{
			mesh->AddAnimation(itor2->second.c_str(), itor2->second.c_str());
		}
		mesh->ClearAnimations();
	}
	return true;
}

void 
AnimationLoader::DumpSet(std::string set)
{
	NameAnimMap name_anim_map = m_anim_set_map[set];
	NameAnimMap::iterator itor = name_anim_map.begin();
	for(; itor != name_anim_map.end(); itor++)
	{
//		DEBUGMSG("\t%s:%s", itor->first.c_str(), itor->second.c_str());
	}
}

void 
AnimationLoader::Dump()
{
	AnimSetMap::iterator itor = m_anim_set_map.begin();
	for(; itor != m_anim_set_map.end(); itor++)
	{
//		DEBUGMSG("%s", itor->first.c_str());
		DumpSet(itor->first);
	}
}

bool 
AnimationLoader::ApplySetToAnimatedMesh(std::string set, engine::AnimatedMesh *mesh)
{
	mesh->ClearAnimations();
	NameAnimMap name_anim_map = m_anim_set_map[set];
	NameAnimMap::iterator itor = name_anim_map.begin();
	for(; itor != name_anim_map.end(); itor++)
	{
//		DEBUGMSG("Adding %s, %s", itor->first.c_str(), itor->second.c_str());
		mesh->AddAnimation(itor->first.c_str(), itor->second.c_str());
	}

	return true;
}


XMLDocument*
AnimationLoader::CreateXMLDoc(std::string filename)
{
	FileStream fs;
	if(fs.Open(filename.c_str(), FileStream::ReadOnly))
	{
		XMLDocument* xml_doc = new XMLDocument();
		if(xml_doc)
		{
			if(xml_doc->Open(fs))
			{
				return xml_doc;
			}
		}
	}
	return 0;
}



}
}