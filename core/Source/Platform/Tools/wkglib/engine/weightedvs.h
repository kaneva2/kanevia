/******************************************************************************
 weightedvs.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "vertexshader.h"

namespace wkg
{

namespace engine
{

class WeightedVS : public VertexShader
{
public:
	WeightedVS();
	virtual ~WeightedVS();
	virtual bool CreateDeclaration();

};

}

}