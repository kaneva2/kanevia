/******************************************************************************
 WorldObject.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Messenger.h"
#include "quaternion.h"
#include "vector3.h"
#include "collidable.h"

namespace wkg 
{

class Vector3;
class Quaternion;
class Sphere;
class XMLNode;

namespace engine 
{

class WorldObject : public Collidable, public Messenger
{
public:
	WorldObject();
	virtual ~WorldObject();

	virtual const Vector3 &GetPosition() const			{ return m_position;	}
	virtual const Quaternion &GetOrientation() const	{ return m_orientation; }
	const Vector3 &GetVelocity() const					{ return m_velocity;	}
	const real GetMass() const							{ return m_mass;		}
	const Vector3& GetBVOffset()						{ return m_bv_offset;	}

	virtual void SetPosition(const Vector3 &position);
	virtual void SetOrientation(const Quaternion &orientation);
	virtual void SetVelocity(const Vector3 &velocity);
	virtual void SetMass(real mass);
	virtual void SetAngularVelocity(Vector3 &v);
	virtual void SetBVOffset(Vector3 &bvoffset);
	virtual bool IsInSim() { return false; }

	std::string GetParticleBulletHit()					{	return m_bulletParticleHit;	}

	virtual void OnCollide(Collidable *other, Collision *c);
	bool LoadXMLNode(XMLNode* curr);
	virtual void OnUpdate(uint32 ms) {}

protected:

protected:
	Vector3 m_position;
	Quaternion m_orientation;

	Vector3 m_velocity;
	Vector3 m_omega;

	Vector3 m_bv_offset;

	real m_mass;
	std::string m_bulletParticleHit;
};

}

}