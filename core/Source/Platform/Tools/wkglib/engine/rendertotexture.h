/******************************************************************************
 rendertotexture.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

namespace engine
{

class RenderToTexture
{
public:
	RenderToTexture(int32 width = 0, int32 height = 0);
	~RenderToTexture();

	virtual void Prepare();
	virtual void Restore();

	LPDIRECT3DTEXTURE9 GetTexture() const { return m_texture; }

protected:
	LPDIRECT3DTEXTURE9 m_texture;
	IDirect3DSurface9 *m_old_zbuf;
	IDirect3DSurface9 *m_old_rt;
};

}

}

