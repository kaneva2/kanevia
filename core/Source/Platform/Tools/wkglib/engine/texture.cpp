/******************************************************************************
 texture.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "texture.h"
#include "device.h"

namespace wkg 
{

namespace engine 
{

//#ifdef MANAGED_TEXTURES
#	define POOL	D3DPOOL_MANAGED
//#else
//#	define POOL	D3DPOOL_DEFAULT
//#endif

int32 Texture::m_total_size = 0;

bool Texture::Load(const char* of, bool warn_if_no_exist)
{
	// see if a dds exist for this tga
	if(!stricmp(&of[strlen(of) - 3], "tga"))
	{
		char tmp[MAX_PATH];
		strcpy(tmp, of);
		strcpy(&tmp[strlen(tmp) - 3], "dds");
		if(Load(tmp, false))
			return true;
	}

	LPDIRECT3DTEXTURE9 tex;
	D3DXIMAGE_INFO info;
	D3DFORMAT fmt = D3DFMT_A8R8G8B8;
	char* ext;
	HRESULT hr;

	if (!of)
		return false;

	char filename[255];
	strcpy(filename, of);

	ext = strrchr(filename, '.');
	if (ext && 0 == strcmpi(ext, ".dds"))
		fmt = D3DFMT_UNKNOWN;	// take the format from the file, itself

	hr = D3DXCreateTextureFromFileEx(
		Device::Instance(), 
		filename, 
		D3DX_DEFAULT,		// width
		D3DX_DEFAULT,		// height
//		128,		// width
//		128,		// height
		4,					// miplevels
		0,					// usage
		fmt,				// format
		POOL,				// pool
		D3DX_DEFAULT,		// filter
		D3DX_DEFAULT,		// mipfilter
		0x00000000,			// colorkey
		&info,				// srcinfo
		0,					// palette
		&tex);

	if (D3D_OK == hr)
	{
		m_srcWidth = info.Width/4;
		m_srcHeight = info.Height/4;

		m_filename = filename;

		tex->Release();

		hr = D3DXCreateTextureFromFileEx(
			Device::Instance(), 
			filename, 
			m_srcWidth,		// width
			m_srcHeight,		// height
			4,					// miplevels
			0,					// usage
			fmt,				// format
			POOL,				// pool
			D3DX_DEFAULT,		// filter
			D3DX_DEFAULT,		// mipfilter
			0x00000000,			// colorkey
			&info,				// srcinfo
			0,					// palette
			&tex);
		m_tex = tex;

		m_srcWidth = info.Width;
		m_srcHeight = info.Height;

//		DEBUGMSG("%.d x %.d", m_srcWidth, m_srcHeight);

//		m_total_size += m_srcWidth * m_srcHeight * 4;

//		DEBUGMSG("TOTAL TEX SIZE: %d", m_total_size);
//		DEBUGMSG("%s", filename);

		return true;
	}

	if(warn_if_no_exist)
	{
		DBPRINTLN(("*** Unable to load texture: [%s]", filename));
	}

	m_tex = 0;
	m_srcWidth = m_srcHeight = 0;

	return false;
}

bool Texture::LoadCubeMap(const char* filename)
{
	LPDIRECT3DCUBETEXTURE9 tex;
	D3DXIMAGE_INFO info;
	HRESULT hr;

	hr = D3DXCreateCubeTextureFromFileEx(
		Device::Instance(), 
		filename, 
		D3DX_DEFAULT,		// width & height (square texture)
		4,					// miplevels
		0,					// usage
		D3DFMT_UNKNOWN,		// format
		POOL,				// pool
		D3DX_DEFAULT,		// filter
		D3DX_DEFAULT,		// mipfilter
		0x00000000,			// colorkey
		&info,				// srcinfo
		0,					// palette
		&tex);

	if (D3D_OK == hr)
	{
		m_tex = tex;
		m_srcWidth = info.Width;
		m_srcHeight = info.Height;
		m_filename = filename;

		m_total_size += m_srcWidth * m_srcHeight * 4;
//		DEBUGMSG("TOTAL TEX SIZE: %d", m_total_size);

		return true;
	}

	DBPRINTLN(("*** Unable to load cube map: [%s]", filename));
	m_tex = 0;
	m_srcWidth = m_srcHeight = 0;

	return false;
}

bool Texture::Create(uint32 Width, uint32 Height, D3DFORMAT Fmt, uint32 Usage, uint32 Levels, D3DPOOL Pool)
{
	HRESULT hr;
	LPDIRECT3DTEXTURE9 tex;

	hr = Device::Instance()->CreateTexture(
		Width, 
		Height, 
		Levels, 
		Usage, 
		Fmt, 
		Pool, 
		&tex, 0);

	if (D3D_OK == hr)
	{
		m_tex = tex;
		m_srcWidth = Width;
		m_srcHeight = Height;

		m_total_size += m_srcWidth * m_srcHeight * 4;

//		DEBUGMSG("TOTAL TEX SIZE: %d", m_total_size);

		return true;
	}

	DBPRINTLN(("*** Unable to create %d x %d texture (0x%08x)", Width, Height, hr));
	m_tex = 0;
	m_srcWidth = m_srcHeight = 0;

	return false;
}

}}
