/******************************************************************************
 bvoctree.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "octnode.h"
#include "collision.h"
#include "bvsphere.h"
#include "bvellipsoid.h"
#include "worldobject.h"
#include "bvoctree.h"

namespace wkg
{

namespace engine
{

BVOctree::BVOctree() :
	m_octree(0)
{
}

BVOctree::~BVOctree()
{
}

Collision *
BVOctree::Collide(BoundingVolume *other, uint32 ms)
{
	// we know how to do sphere and ellpsoid collisions with the octree now
	BVSphere *bvs = dynamic_cast<BVSphere *>(other);
	if(bvs)
	{
		Vector3 src = bvs->GetWorldObject()->GetPosition();
		src += bvs->GetWorldObject()->GetBVOffset();

		Vector3 dst = src + (bvs->GetWorldObject()->GetVelocity() * (real)ms) / 1000.0f;

		real r = bvs->GetRadius();

		Vector3 n;
		Vector3 poc;

		bool c = false;
		c = m_octree->CollideDynamicSphere(src, dst, r, &n, &poc, 0);

		if(c)
		{
			dst -= bvs->GetWorldObject()->GetBVOffset();

			Collision *c = new Collision();
			c->m_dst = dst;
			c->m_poc = poc;
			c->m_n = n;
			return c;
		}
		else
		{
			return 0;
		}
	}

	BVEllipsoid *bve = dynamic_cast<BVEllipsoid *>(other);
	if(bve)
	{
		Vector3 src = bve->GetWorldObject()->GetPosition();
		src += bve->GetWorldObject()->GetBVOffset();

		Vector3 dst = src + (bve->GetWorldObject()->GetVelocity() * (real)ms) / 1000.0f;

		Vector3 r = bve->GetRadius();

		Vector3 n;
		Vector3 poc;

		bool c = false;
		c = m_octree->CollideDynamicEllipsoid(src, dst, r, &n, &poc, 0);

		if(c)
		{
			dst -= bve->GetWorldObject()->GetBVOffset();

			Collision *c = new Collision();
			c->m_dst = dst;
			c->m_poc = poc;
			c->m_n = n;
			return c;
		}
		else
		{
			return 0;
		}
	}

	ASSERT(0);
	return 0;
}

}

}

