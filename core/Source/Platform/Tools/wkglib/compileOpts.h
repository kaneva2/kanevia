///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define SINGLEPRECISION
#define MANAGED_TEXTURES
#define USE_32BIT_INDICES
#define USE_SERIALIZATION
