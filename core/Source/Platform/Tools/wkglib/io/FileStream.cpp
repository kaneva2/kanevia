///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"

#include "FileStream.h"

#include <stdio.h>

namespace wkg {

bool
FileStream::Open(const char* filename, OpenMode flags) {
	ASSERT(!fp);
	fp = 0;
	errno_t err = 0;

	if (flags & CreateAlways)
		err = fopen_s(&fp, filename, "w+b");
	else if (flags & ReadOnly)
		err = fopen_s(&fp, filename, "rb");
	else
		err = fopen_s(&fp, filename, "r+b");

	return err == 0 && fp != 0;
}

void
FileStream::Close() {
	if (fp) {
		fclose(fp);
		fp = 0;
	}
}

bool
FileStream::IsEOF() {
	return feof(fp) != 0;
}

uint32
FileStream::GetFileSize() {
	if (fp) {
		uint32 result;
		int32 start;

		start = ftell(fp);
		if (0 == fseek(fp, 0, SEEK_END)) {
			result = (uint32) ftell(fp);
			fseek(fp, start, SEEK_SET);
			return result;
		}
	}
	return 0;
}

uint32
FileStream::Read(uint8* dest, uint32 destMax) {
	return (uint32) fread(dest, 1, destMax, fp);
}

uint32
FileStream::Write(uint8* src, uint32 srcAmt) {
	return (uint32) fwrite(src, 1, srcAmt, fp);
}

}
