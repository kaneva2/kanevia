/******************************************************************************
 State.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <vector>
#include <string>
#include <map>

namespace wkg
{

namespace state_machine
{

class Transition;
class StateMachine;
class Trigger;

class State
{

public:
	State();
	State(std::string newName, StateMachine* parent);
	virtual ~State();

	virtual void OnEnter()				{}
	virtual void OnExit()				{} 
	virtual void OnUpdate(uint32 ms);

	void AddTransition(Transition* trans);
	Transition	*GetTransition(std::string name);
	Transition *GetTransition(int32 idx);
	int32 GetNumTransitions();

	void SetName(const std::string &name);
	std::string GetName();

	uint32 GetCountdown()					{	return m_countdown;			}
	void SetCountdown(uint32 countdown)		{	m_countdown = countdown;	}

	enum FinishTriggerType { ONE_TIME = 0, ONGOING = 1 };
	void AddFinishTrigger(Trigger *trigger, FinishTriggerType type)
	{
		t_finish_trigger t;
		t.trigger = trigger;
		t.type = type;

		m_finish_triggers.push_back(t);
	}

	void TriggerFinishTriggers();

protected:
	typedef std::vector<Transition *> TransitionVector;
	typedef TransitionVector::iterator TransitionIterator;

	StateMachine			*m_parentSM;
	std::string				m_name;

	TransitionVector		m_transitions;
	uint32					m_countdown;

	struct t_finish_trigger
	{
		Trigger *trigger;
		FinishTriggerType type;
	};
	std::vector<t_finish_trigger> m_finish_triggers;

};

}

}
