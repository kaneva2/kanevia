///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "factoryrelease.h"

namespace wkg {

template <typename T> class RefCounter {
public:
	RefCounter() : m_ref_count(1), m_factory_release(0) {};

	virtual void Release() {
		m_ref_count--;
		if (!m_ref_count) {
			// tell our factory we don't exist anymore
			if (m_factory_release) {
				m_factory_release->Release((T*)this);
			}

			delete this;
		}
	}

	virtual void AddRef() {
		m_ref_count++;
	}

	virtual void SetFactoryRelease(FactoryRelease<T> *fr) {
		m_factory_release = fr;
	}

protected:
	int32 m_ref_count;
	FactoryRelease<T> *m_factory_release;

protected:

	// can only be called from Release()
	virtual ~RefCounter() {
		if (m_ref_count)
			DEBUGMSG("Warning: Refcounter being deleted before reaching 0 references");
	};

};

}



