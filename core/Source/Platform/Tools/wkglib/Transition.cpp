/******************************************************************************
 Transition.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include <string>
#include "wkgtypes.h"
#include "State.h"
#include "Transition.h"

namespace wkg
{

namespace state_machine
{

Transition::Transition() : 
	m_targetName(""),
	m_targetState(0)
{
}

Transition::Transition(std::string msgName, std::string targetName) : 
	m_targetName(targetName),
	m_msgName(msgName),
	m_targetState(0)
{
}

Transition::~Transition()
{
	// the states are freed up by the state machine
}

}

}
