/******************************************************************************
 KeyFrameMessage.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

namespace state_machine
{

class KeyFrameMessage
{
public:
	KeyFrameMessage(std::string msg, int32 frame);
	~KeyFrameMessage();

	int32 GetFrame()						{	return m_frame;		}
	std::string GetMessage()				{	return m_message;	}
	void SetFrame(int32 newFrame)			{	m_frame = newFrame;	}
	void SetMessage(std::string msg)		{	m_message = msg;	}

protected:
	int32			m_frame;
	std::string		m_message;

};

}
}