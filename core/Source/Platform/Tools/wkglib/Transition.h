/******************************************************************************
 Transition.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <string>

namespace wkg
{

namespace state_machine
{

class State;

class Transition
{

public:
	Transition();
	Transition(std::string msgName, std::string targetName);
	virtual ~Transition();

	std::string GetMsgName()						{	return m_msgName;			}
	std::string GetTargetName()						{	return m_targetName;		}
	State *GetTargetState()							{	return m_targetState;		}
	void SetTargetName(std::string newName)			{	m_targetName = newName;		}
	void SetTargetState(State* newState)			{	m_targetState = newState;	}
	
protected:
	std::string					m_msgName;
	std::string					m_targetName;
	State						*m_targetState;

};

}

}
