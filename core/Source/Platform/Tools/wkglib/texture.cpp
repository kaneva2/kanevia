///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include "texture.h"
#include "device.h"

namespace wkg {

#define POOL	D3DPOOL_MANAGED

int32 Texture::m_total_size = 0;

bool Texture::Load(const char* of) {
	// see if a dds exist for this tga
	if (!stricmp(&of[strlen(of) - 3], "tga")) {
		char tmp[MAX_PATH];
		strcpy_s(tmp, _countof(tmp), of);
		uint32 len = strlen(tmp);
		strcpy_s(&tmp[len - 3], _countof(tmp) - len, "dds");
		if (Load(tmp))
			return true;
	}

	LPDIRECT3DTEXTURE9 tex;
	D3DXIMAGE_INFO info;
	D3DFORMAT fmt = D3DFMT_A8R8G8B8;
	char* ext;
	HRESULT hr;

	if (!of)
		return false;

	char filename[255];
	strcpy_s(filename, _countof(filename), of);

	ext = strrchr(filename, '.');
	if (ext && 0 == _strcmpi(ext, ".dds"))
		fmt = D3DFMT_UNKNOWN;	// take the format from the file, itself

	hr = D3DXCreateTextureFromFileExA(
		Device::Instance(),
		filename,
		D3DX_DEFAULT,		// width
		D3DX_DEFAULT,		// height
		0,					// miplevels
		0,					// usage
		fmt,				// format
		POOL,				// pool
		D3DX_DEFAULT,		// filter
		D3DX_DEFAULT,		// mipfilter
		0x00000000,			// colorkey
		&info,				// srcinfo
		0,					// palette
		&tex);

	if (D3D_OK == hr) {
		m_filename = filename;

		m_tex = tex;

		m_srcWidth = info.Width;
		m_srcHeight = info.Height;

		return true;
	}

	m_tex = 0;
	m_srcWidth = m_srcHeight = 0;

	return false;
}

bool Texture::LoadCubeMap(const char* filename) {
	LPDIRECT3DCUBETEXTURE9 tex;
	D3DXIMAGE_INFO info;
	HRESULT hr;

	hr = D3DXCreateCubeTextureFromFileExA(
		Device::Instance(),
		filename,
		D3DX_DEFAULT,		// width & height (square texture)
		0,					// miplevels
		0,					// usage
		D3DFMT_UNKNOWN,		// format
		POOL,				// pool
		D3DX_DEFAULT,		// filter
		D3DX_DEFAULT,		// mipfilter
		0x00000000,			// colorkey
		&info,				// srcinfo
		0,					// palette
		&tex);

	if (D3D_OK == hr) {
		m_tex = tex;
		m_srcWidth = info.Width;
		m_srcHeight = info.Height;
		m_filename = filename;

		m_total_size += m_srcWidth * m_srcHeight * 4;

		return true;
	}

	DBPRINTLN(("*** Unable to load cube map: [%s]", filename));
	m_tex = 0;
	m_srcWidth = m_srcHeight = 0;

	return false;
}

bool Texture::Create(uint32 Width, uint32 Height, D3DFORMAT Fmt, uint32 Usage, uint32 Levels, D3DPOOL Pool) {
	HRESULT hr;
	LPDIRECT3DTEXTURE9 tex;

	hr = Device::Instance()->CreateTexture(
		Width,
		Height,
		Levels,
		Usage,
		Fmt,
		Pool,
		&tex, 0);

	if (D3D_OK == hr) {
		m_tex = tex;
		m_srcWidth = Width;
		m_srcHeight = Height;

		m_total_size += m_srcWidth * m_srcHeight * 4;

		return true;
	}

	DBPRINTLN(("*** Unable to create %d x %d texture (0x%08x)", Width, Height, hr));
	m_tex = 0;
	m_srcWidth = m_srcHeight = 0;

	return false;
}

bool Texture::LoadD3DExternal(const char* displayName, uint32 externalKey) {
	m_filename = displayName;
	m_externalKey = externalKey;
	m_tex = NULL;
	return true;
}


}
