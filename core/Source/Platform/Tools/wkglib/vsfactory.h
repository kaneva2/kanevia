///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>
#include "singleton.h"

namespace wkg {

class VertexShader;

class VSFactory : public Singleton<VSFactory> {
public:
	VSFactory();
	virtual ~VSFactory();

	void RegisterShader(VertexShader *vs, std::string name);
	VertexShader *GetShader(std::string name);

protected:
	class ShaderEntry {
	public:
		std::string m_name;
		VertexShader *m_shader;
	};

	typedef std::vector<ShaderEntry> ShaderVector;
	typedef ShaderVector::iterator ShaderIterator;

	ShaderVector m_shaders;
};

}
