///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include "DynIVB.h"

namespace wkg {

DynIVB::DynIVB() {
	pIB = 0;
	istartByte = ibNext = 0;
	piBatch = 0;
}

DynIVB::~DynIVB() {}

bool
DynIVB::Prepare(LPDIRECT3DVERTEXBUFFER9 pVB_, uint8 vertSize_, LPDIRECT3DINDEXBUFFER9 pIB_, int32 numBatches) {
	D3DVERTEXBUFFER_DESC vbdesc;
	D3DINDEXBUFFER_DESC ibdesc;

	if (pVB_ && pIB_ &&
		D3D_OK == pVB_->GetDesc(&vbdesc) && vbdesc.Usage & D3DUSAGE_DYNAMIC && vbdesc.Usage & D3DUSAGE_WRITEONLY &&
		D3D_OK == pIB_->GetDesc(&ibdesc) && ibdesc.Usage & D3DUSAGE_DYNAMIC && ibdesc.Usage & D3DUSAGE_WRITEONLY && ibdesc.Format == D3DFMT_INDEX16) {
		this->pVB = pVB_;
		this->pIB = pIB_;
		this->vertSize = vertSize_;
		maxVerts = MIN(vbdesc.Size / vertSize_, ibdesc.Size / sizeof(uint16));
		maxBatches = numBatches;
		vertsPerBatch = maxVerts / maxBatches;
		if (vertsPerBatch > 0xffff)		// uint16 can't address more than 64k
			vertsPerBatch = 0xffff;

		startByte = maxVerts;	// force lock at beginning of vb
		vbNext = vertsPerBatch;	// ... no room for more
		pBatch = 0;				// ... none queued
		UINT isb = ibdesc.Size / sizeof(uint16);
		istartByte = (uint16) isb;
		ibNext = (uint16) vertsPerBatch;
		piBatch = 0;

		return true;
	} else {
		this->pVB = 0;
		this->pIB = 0;
		this->vertSize = 0;
		maxVerts = maxBatches = vertsPerBatch = startByte = vbNext = 0;
		pBatch = 0;
		istartByte = ibNext = 0;
		piBatch = 0;

		return false;
	}
}

bool
DynIVB::CanAppend(int32 numVerts, uint16 numIndices, uint8** pVert, uint16** pIVert, uint16* pINext) {
	if (ibNext + numIndices >= vertsPerBatch) {
		uint32 lockFlags;

		if (pBatch) {
			*pVert = 0;
			*pIVert = 0;
			*pINext = 0;

			return false;
		}

		startByte += vertsPerBatch;		// move past the entire batch (regardless of whether it was fully used) to simplify locking
		vbNext = 0;						// (i.e. lock only on batch boundaries)
		istartByte += (uint16) vertsPerBatch;
		ibNext = 0;

		if (istartByte + vertsPerBatch <= maxVerts)
			lockFlags = D3DLOCK_NOOVERWRITE;			// next batch fits within this vert buf
		else {
			startByte = 0;								// entire vert buf is full...get a new one and lock at the beginning
			istartByte = 0;
// TESTING!!!! Undo this change!!
			//lockFlags = D3DLOCK_DISCARD;
			lockFlags = D3DLOCK_NOOVERWRITE;
		}

		if (D3D_OK == pVB->Lock(startByte * vertSize, vertsPerBatch * vertSize, (void**) &pBatch, lockFlags)) {
			if (D3D_OK == pIB->Lock(istartByte * sizeof(uint16), vertsPerBatch * sizeof(uint16), (void**) &piBatch, lockFlags)) {
				*pVert = pBatch;
				*pIVert = piBatch;
				*pINext = (uint16) vbNext;
				vbNext += numVerts;
				ibNext += numIndices;

				return true;
			} else
				pVB->Unlock();
		}

		// d3d problem...probably lost device; try to start new vert buf/index buf on next attempt
		//
		startByte = maxVerts;	// force lock at beginning of vb
		vbNext = vertsPerBatch;	// ... no room for more
		pBatch = 0;				// ... none queued
		istartByte = (uint16) vertsPerBatch;
		ibNext = (uint16) vertsPerBatch;
		piBatch = 0;
		*pVert = 0;
		*pIVert = 0;
		*pINext = 0;

		return false;
	} else {
		*pVert = pBatch + vbNext * vertSize;
		*pIVert = piBatch + ibNext;
		*pINext = (uint16) vbNext;
		vbNext += numVerts;
		ibNext += numIndices;

		return true;
	}
}

bool
DynIVB::BatchReady(int32* pStartVert, int32* pNumVerts, uint16* pStartIdx, uint16* pNumIdx) {
	if (pBatch && piBatch) {
		pVB->Unlock();
		pIB->Unlock();
		*pStartVert = startByte;
		*pNumVerts = vbNext;
		*pStartIdx = istartByte;
		*pNumIdx = ibNext;

		vbNext = vertsPerBatch;		// move to next batch
		pBatch = 0;					// signal that we've already sent the last batch
		ibNext = (uint16) vertsPerBatch;
		piBatch = 0;

		return true;
	}

	*pStartVert = 0;
	*pNumVerts = 0;
	*pStartIdx = 0;
	*pNumIdx = 0;

	return false;
}

}