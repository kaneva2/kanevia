#pragma once

#include <Core/Math/Simd.h>
#include <Core/Math/Vector.h>

namespace wkg {

class AABB {
public:
	AABB();						// init
	AABB(const KEP::Vector3f& min, const KEP::Vector3f& max);

	void Add(const KEP::Simd4f& point);
	void Add(const KEP::Vector3f& point);
	void Add(const KEP::Vector4f& point);
	void Add(KEP::Vector3f *points, int32 numPoints);
	void Add(const AABB& box);
	void ExpandMin(const KEP::Simd4f& amount); // Expands box in min directions by amount
	void ExpandMax(const KEP::Simd4f& amount); // Expands box in max directions by amount
	void Expand(const KEP::Simd4f& amount); // Expands box in both positive and negative six directions by amount

	void SetEmpty();			// set so that box contains nothing
	bool IsEmpty() const { return ((m_min > m_max).BooleanMask() & 0x7) == 0x7; }
	//void Set(const Vector3f&, const Vector3&);
	//void SetMin(const Vector3f&);
	//void SetMax(const Vector3f&);

	const KEP::Simd4f& GetMin() const;
	const KEP::Simd4f& GetMax() const;

	KEP::Simd4f GetCenter() const;
	KEP::Simd4f GetDiag() const;	// distance from center to corner (in each dimension)
	KEP::Simd4f GetExtent() const;	// distance from center to center of each face (in each dimension)

	//bool Contains(const Vector3f& point) const;
	//bool Contains(const AABB&) const;

	SERIALIZABLE

public:
	KEP::Simd4f m_min;
	KEP::Simd4f m_max;
};

inline void
AABB::SetEmpty() {
	m_min.Set(FLT_MAX);
	m_max.Set(-FLT_MAX);
}

inline
AABB::AABB() {
	SetEmpty();
}

inline
AABB::AABB(const KEP::Vector3f& min, const KEP::Vector3f& max) {
	m_min.Set(min.x, min.y, min.z, 0);
	m_max.Set(max.x, max.y, max.z, 0);
}

inline void
AABB::Add(const KEP::Vector3f& point) {
	Add(KEP::Vector4f(point,0));
}

inline void
AABB::Add(const KEP::Simd4f& ssePoint) {
	m_min = Min(m_min, ssePoint);
	m_max = Max(m_max, ssePoint);
}

inline void
AABB::Add(const KEP::Vector4f& point) {
	__m128 ssePoint = _mm_load_ps(point.RawData());
	Add(ssePoint);
}

inline void
AABB::Add(KEP::Vector3f *points, int32 numPoints) {
	while (numPoints-- > 0)
		Add(points[numPoints]);
}

inline void AABB::Add(const AABB& box) {
	Add(box.GetMin());
	Add(box.GetMax());
}

inline void AABB::ExpandMin(const KEP::Simd4f& amount) {
	m_min -= amount;
}

inline void AABB::ExpandMax(const KEP::Simd4f& amount) {
	m_max += amount;
}

inline void AABB::Expand(const KEP::Simd4f& amount) {
	ExpandMin(amount);
	ExpandMax(amount);
}


inline const KEP::Simd4f&
AABB::GetMin() const {
	return m_min;
}

inline const KEP::Simd4f&
AABB::GetMax() const {
	return m_max;
}

inline KEP::Simd4f
AABB::GetCenter() const {
	return KEP::Simd4f(0.5f) * (m_min + m_max);
}

inline KEP::Simd4f
AABB::GetDiag() const {
	return m_max - m_min;
}

inline KEP::Simd4f
AABB::GetExtent() const {
	return KEP::Simd4f(0.5f) * (m_max - m_min);
}
/*
inline void
AABB::SetMin(const Vector3f& min_) {
	this->min = min_;
}

inline void
AABB::SetMax(const Vector3f& max_) {
	this->max = max_;
}

inline void
AABB::Set(const Vector3f& min_, const Vector3f& max_) {
	SetMin(min);
	SetMax(max);
}

inline bool
AABB::Contains(const Vector3f& point) const {
	return point.x >= min.x && point.x <= max.x
		&& point.y >= min.y && point.y <= max.y
		&& point.z >= min.z && point.z <= max.z;
}

inline bool
AABB::Contains(const AABB& other) const {
	return Contains(other.min) && Contains(other.max);
}
*/
}
