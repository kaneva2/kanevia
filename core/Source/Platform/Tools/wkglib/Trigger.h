/******************************************************************************
 Trigger.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <string>

namespace wkg
{

namespace state_machine
{

class Trigger
{

public:
	Trigger(std::string name);
	virtual ~Trigger();

	std::string GetName();

protected:
	std::string m_name;
};

}

}
