///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include "vsfactory.h"
#include "vertexshader.h"

namespace wkg {

VSFactory::VSFactory() {}

VSFactory::~VSFactory() {
	ShaderIterator itor = m_shaders.begin();
	ShaderIterator end = m_shaders.end();
	for (; itor < end; itor++) {
		delete itor->m_shader;
	}

	m_shaders.clear();
}

void
VSFactory::RegisterShader(VertexShader *vs, std::string name) {
	ShaderEntry entry;
	entry.m_name = name;
	entry.m_shader = vs;
	m_shaders.push_back(entry);
}

VertexShader *
VSFactory::GetShader(std::string name) {
	VertexShader *shader = (VertexShader *) 0;

	ShaderIterator itor = m_shaders.begin();
	ShaderIterator end = m_shaders.end();
	for (; itor < end; itor++) {
		if (itor->m_name == name) {
			shader = itor->m_shader;
			break;
		}
	}

	return shader;
}

}
