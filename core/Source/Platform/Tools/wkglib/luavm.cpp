/******************************************************************************
 luavm.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"

#include "LuaHelper.h"

#include "luavm.h"

namespace wkg
{

LuaVM::LuaVM()
{
	m_state = lua_open();
}

LuaVM::~LuaVM()
{
	lua_close(m_state);
	m_state = 0;
}

bool
LuaVM::CallFunction(const std::string &func)
{
	lua_getglobal(m_state, func.c_str());
	if(!lua_isfunction(m_state, -1))
	{
		ASSERT(0);
		return false;
	}

	lua_call(m_state, 0, 0);
	return true;
}

bool 
LuaVM::LoadFile(const std::string &filename)
{
	int res = lua_dofile(m_state, filename.c_str());
	if(res)
	{
		ASSERT(0);
		return false;
	}

	return true;
}

bool 
LuaVM::RunFile(const std::string &filename)
{
	int res = lua_dofile(m_state, filename.c_str());
	if(res)
	{
		ASSERT(0);
		return false;
	}

	lua_getglobal(m_state, "main");
	if(!lua_isfunction(m_state, -1))
	{
		ASSERT(0);
		return false;
	}

	lua_call(m_state, 0, 0);

	return true;
}

bool
LuaVM::GetGlobalString(const char *global_name, char *out_val)
{
	lua_getglobal(m_state, global_name);
	if(!lua_isstring(m_state, -1))
	{
		ASSERT(0);
		strcpy(out_val, "");
		return false;
	}

	strcpy(out_val, lua_tostring(m_state, -1));

	return true;
}

}


