/******************************************************************************
 SkillState.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "Skill.h"
#include "State.h"

namespace wkg
{

using namespace state_machine;

namespace game
{

class Skill;

class SkillState : public State, public Skill
{
public:
	SkillState(std::string newName, StateMachine* parent, Actor *owner = 0);
	virtual ~SkillState();

	virtual void OnEnter();
	virtual void OnExit();
	virtual void OnUpdate(uint32 ms);

protected:

};

}

}

