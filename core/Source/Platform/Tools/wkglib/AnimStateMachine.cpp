/******************************************************************************
 AnimStateMachine.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "AnimationLoader.h"
#include "AnimState.h"
#include "AnimStateMachine.h"
#include "AnimatedMesh.h"

namespace wkg
{

using namespace engine;

namespace state_machine
{

AnimStateMachine::AnimStateMachine(AnimatedMesh* am) : 
	StateMachine(),
	m_animated_mesh(am),
	m_owner_uid(-1)
{
}

AnimStateMachine::~AnimStateMachine()
{
	//delete m_animated_mesh;
	m_animated_mesh = 0;
}

void
AnimStateMachine::AddAnimations()
{
#if 0
	///////////////////////hackish///////////////////////
	game::AnimationLoader::Instance()->Load(m_animated_mesh, "default", "zero_animationset.xml");

	SMVector::iterator itor = m_states.begin();
	SMVector::iterator iend = m_states.end();
	for(; itor != iend; ++itor)
	{
		AnimState* anim_state = dynamic_cast<AnimState *>(*itor);
		std::string name = anim_state->GetName();
		std::string filename = anim_state->GetAnimFilename();
		m_animated_mesh->AddAnimation(name.c_str(), filename.c_str());
	}
#endif
}

}

}