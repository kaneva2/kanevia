/******************************************************************************
 StateFactory.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "wkgtypes.h"
#include <string>
#include "State.h"
#include "XMLNode.h"
#include "XMLNodeAttributes.h"
#include "AnimState.h"
#include "AnimStateMachine.h"
#include "Message.h"
#include "StateMachine.h"
#include "SkillChangeWeapon.h"
#include "SkillHolsterWeapon.h"
#include "SkillDrawWeapon.h"
#include "SkillFireWeapon.h"
#include "SkillReloadWeapon.h"
#include "SkillStateMachine.h"
#include "SkillState.h"
#include "AIStateMachine.h"
#include "AIState.h"
#include "AIStateChasePlayer.h"
#include "AIStateAttackPlayer.h"
#include "AIStateWaitForPlayer.h"
#include "AIStateWaitForItem.h"
#include "LuaAIState.h"
#include "KeyFrameAnimState.h"
#include "SkillStrongArmThrow.h"
#include "StateFactory.h"

namespace wkg
{

namespace state_machine
{

State*
StateFactory::Create(XMLNode* curNode, StateMachine* parent_sm)
{
	State *res = 0;
	std::string stateType = curNode->GetAttributes()->FindAttributeValue("type");
	if(stateType == "State")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");
		res = new State(name, parent_sm);
	}
	else if(stateType == "AnimState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		AnimState::ASPlayMode play_type = AnimState::AS_LOOPING;
		std::string splay_type = curNode->GetAttributes()->FindAttributeValue("play_type");
		if(splay_type.c_str() && splay_type.size())
		{
			if(splay_type == "ONCE") play_type = AnimState::AS_ONCE;
		}

		real transition_duration = 100.0f;
		std::string stransition_duration = curNode->GetAttributes()->FindAttributeValue("transition_duration");
		if(stransition_duration.c_str() && stransition_duration.size())
		{
			transition_duration = atof(stransition_duration.c_str());
		}

		Message *m = 0;
		std::string anim_done_message = curNode->GetAttributes()->FindAttributeValue("done_message");
		if(anim_done_message.c_str() && anim_done_message.size())
		{
			AnimStateMachine *animsm = dynamic_cast<AnimStateMachine *>(parent_sm);
			if(animsm)
				m = new Message(anim_done_message, animsm->GetOwnerUID(), animsm->GetOwnerUID());
		}

		AnimStateMachine* anim_sm = dynamic_cast<AnimStateMachine*>(parent_sm);
		AnimState *as = new AnimState(name, anim_sm);
		as->SetTransitionTime(transition_duration);
		as->SetPlayMode(play_type);
		if(m) 
			as->RegisterAnimDoneMessage(m);

		res = as;
	}
	else if(stateType == "SkillState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::SkillStateMachine *ssm = dynamic_cast<game::SkillStateMachine *>(parent_sm);
		if(!ssm) 
		{
			DEBUGMSG("trying to instantiate a skill state inside a non-skill state machine");
			return 0;
		}

		res = new game::SkillState(name, ssm);
	}
	else if(stateType == "ChangeWeaponSkillState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");
		game::SkillStateMachine *ssm = dynamic_cast<game::SkillStateMachine *>(parent_sm);
		if(!ssm) 
		{
			DEBUGMSG("trying to instantiate a skill state inside a non-skill state machine");
			return 0;
		}

		res = new game::SkillChangeWeapon(name, ssm);
	}
	else if(stateType == "HolsterWeaponSkillState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::SkillStateMachine *ssm = dynamic_cast<game::SkillStateMachine *>(parent_sm);
		if(!ssm) 
		{
			DEBUGMSG("trying to instantiate a skill state inside a non-skill state machine");
			return 0;
		}

		res = new game::SkillHolsterWeapon(name, ssm);
	}
	else if(stateType == "DrawWeaponSkillState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::SkillStateMachine *ssm = dynamic_cast<game::SkillStateMachine *>(parent_sm);
		if(!ssm) 
		{
			DEBUGMSG("trying to instantiate a skill state inside a non-skill state machine");
			return 0;
		}

		res = new game::SkillDrawWeapon(name, ssm);
	}	
	else if(stateType == "FireWeaponSkillState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::SkillStateMachine *ssm = dynamic_cast<game::SkillStateMachine *>(parent_sm);
		if(!ssm) 
		{
			DEBUGMSG("trying to instantiate a skill state inside a non-skill state machine");
			return 0;
		}

		res = new game::SkillFireWeapon(name, ssm);
	}
	else if(stateType == "ReloadWeaponSkillState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::SkillStateMachine *ssm = dynamic_cast<game::SkillStateMachine *>(parent_sm);
		if(!ssm) 
		{
			DEBUGMSG("trying to instantiate a skill state inside a non-skill state machine");
			return 0;
		}

		res = new game::SkillReloadWeapon(name, ssm);
	}
	else if(stateType == "StrongArmThrowSkillState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::SkillStateMachine *ssm = dynamic_cast<game::SkillStateMachine *>(parent_sm);
		if(!ssm) 
		{
			DEBUGMSG("trying to instantiate a skill state inside a non-skill state machine");
			return 0;
		}

		res = new game::SkillStrongArmThrow(name, ssm);
	}
	else if(stateType == "AIState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::AIStateMachine *aism = dynamic_cast<game::AIStateMachine *>(parent_sm);
		if(!aism) 
		{
			DEBUGMSG("trying to instantiate a ai-state inside a non-ai-statemachine");
			return 0;
		}

		res = new game::AIState(name, aism);
	}
	else if(stateType == "AIStateChasePlayer")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::AIStateMachine *aism = dynamic_cast<game::AIStateMachine *>(parent_sm);
		if(!aism) 
		{
			DEBUGMSG("trying to instantiate a ai-state inside a non-ai-statemachine");
			return 0;
		}

		res = new game::AIStateChasePlayer(name, aism);
	}
	else if(stateType == "AIStateAttackPlayer")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::AIStateMachine *aism = dynamic_cast<game::AIStateMachine *>(parent_sm);
		if(!aism) 
		{
			DEBUGMSG("trying to instantiate a ai-state inside a non-ai-statemachine");
			return 0;
		}

		res = new game::AIStateAttackPlayer(name, aism);
	}
#if 0
	else if(stateType == "AIStateTalkToPlayer")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::AIStateMachine *aism = dynamic_cast<game::AIStateMachine *>(parent_sm);
		if(!aism) 
		{
			DEBUGMSG("trying to instantiate a ai-state inside a non-ai-statemachine");
			return 0;
		}

		res = new game::AIStateTalkToPlayer(name, aism);
	}
#endif
	else if(stateType == "AIStateWaitForPlayer")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::AIStateMachine *aism = dynamic_cast<game::AIStateMachine *>(parent_sm);
		if(!aism) 
		{
			DEBUGMSG("trying to instantiate a ai-state inside a non-ai-statemachine");
			return 0;
		}

		res = new game::AIStateWaitForPlayer(name, aism);
	}
	else if(stateType == "AIStateWaitForItem")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		game::AIStateMachine *aism = dynamic_cast<game::AIStateMachine *>(parent_sm);
		if(!aism) 
		{
			DEBUGMSG("trying to instantiate a ai-state inside a non-ai-statemachine");
			return 0;
		}

		res = new game::AIStateWaitForItem(name, aism);
	}
	else if(stateType == "LuaAIState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");
		std::string lua_filename = curNode->GetAttributes()->FindAttributeValue("lua_filename");

		game::AIStateMachine *aism = dynamic_cast<game::AIStateMachine *>(parent_sm);
		if(!aism) 
		{
			DEBUGMSG("trying to instantiate a LuaAIState inside a non-ai-statemachine");
			return 0;
		}

		res = new game::LuaAIState(name, aism, lua_filename);
	}
	else if(stateType == "KeyFrameAnimState")
	{
		std::string name = curNode->GetAttributes()->FindAttributeValue("name");

		KeyFrameAnimState::ASPlayMode play_type = KeyFrameAnimState::AS_LOOPING;
		std::string splay_type = curNode->GetAttributes()->FindAttributeValue("play_type");
		if(splay_type.c_str() && splay_type.size())
		{
			if(splay_type == "ONCE") play_type = AnimState::AS_ONCE;
		}

		real transition_duration = 100.0f;
		std::string stransition_duration = curNode->GetAttributes()->FindAttributeValue("transition_duration");
		if(stransition_duration.c_str() && stransition_duration.size())
		{
			transition_duration = atof(stransition_duration.c_str());
		}

		Message *m = 0;
		std::string anim_done_message = curNode->GetAttributes()->FindAttributeValue("done_message");
		if(anim_done_message.c_str() && anim_done_message.size())
		{
			AnimStateMachine *animsm = dynamic_cast<AnimStateMachine *>(parent_sm);
			if(animsm)
				m = new Message(anim_done_message, animsm->GetOwnerUID(), animsm->GetOwnerUID());
		}


		AnimStateMachine* anim_sm = dynamic_cast<AnimStateMachine*>(parent_sm);
		KeyFrameAnimState *as = new KeyFrameAnimState(name, anim_sm);

		int32 num_keyframes = atoi(curNode->GetAttributes()->FindAttributeValue("num_keyframes").c_str());
		for(int32 i = 1; i <= num_keyframes; ++i)
		{
			char crap[255];
			sprintf(crap, "KeyFrameNum%d", i);
			char crap2[255];
			sprintf(crap2, "KeyFrameMsg%d", i);
			std::string frame_attribute = crap;
			std::string msg_attribute = crap2;

			int32 frame = atoi(curNode->GetAttributes()->FindAttributeValue(frame_attribute).c_str());
			std::string msg = curNode->GetAttributes()->FindAttributeValue(msg_attribute);
			as->AddKeyFrameMessage(msg, frame);
		}

		as->SetTransitionTime(transition_duration);
		as->SetPlayMode(play_type);
		if(m) 
			as->RegisterAnimDoneMessage(m);

		res = as;
	}
	else
	{
		ASSERT(0);
		DEBUGMSG("Unknown state type");
	}


	return res;
}

}

}


