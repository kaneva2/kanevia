///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#undef ASSERT

#ifdef _DEBUG
#	define ASSERT(t)		{ __pragma(warning(disable:4127)) if (!(t)) __asm { int 3 } }
#	define DBUG(line)		line
#	define DBPRINT(args)	dbprint##args
#	define DBPRINTLN(args)	dbprintln##args
#	define DEBUGMSG dbprintln
extern void dbprint(const char*, ...);
extern void dbprintln(const char*, ...);
#else
#	define ASSERT(t)
#	define DBUG(line)
#	define DBPRINT(args)
#	define DBPRINTLN(args)
#	define DEBUGMSG
#endif
