///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "wkgtypes.h"
#include <stdarg.h>
#include <stdio.h>

#ifndef OutputDebugString
extern "C" __declspec(dllimport) void __stdcall OutputDebugStringA(char*);
#endif

void
dbprint(const char* fmt, ...) {
	va_list args;
	static char buf[1024];

	va_start(args, fmt);

	vsprintf_s(buf, _countof(buf), fmt, args);
	OutputDebugStringA(buf);

	va_end(args);
}

void
dbprintln(const char* fmt, ...) {
	va_list args;
	static char buf[1024];

	va_start(args, fmt);

	vsprintf_s(buf, _countof(buf), fmt, args);
	OutputDebugStringA(buf);
	OutputDebugStringA("\r\n");

	va_end(args);
}
