/******************************************************************************
 AnimState.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <algorithm>
#include "state.h"

namespace wkg
{

class Message;
namespace engine
{
	class AnimatedMesh;
} 

namespace state_machine
{

class AnimStateMachine;

class AnimState : public State
{
public:
	AnimState(std::string name, StateMachine* parent_sm);
	virtual ~AnimState();

	virtual void OnEnter();
	virtual void OnExit();
	virtual void OnUpdate(uint32 ms);


	engine::AnimatedMesh* GetAnimatedMesh();

	void SetTransitionTime(real time) { m_transition_time = time; }

	enum ASPlayMode { AS_ONCE = 0, AS_LOOPING = 1 };
	void SetPlayMode(ASPlayMode mode) { m_play_mode = mode; }

	void RegisterAnimDoneMessage(Message *m)
	{
		m_anim_done_messages.push_back(m);
	}

	void UnRegisterAnimDoneMessage(Message *m)
	{
		std::vector<Message *>::iterator itor = 
			std::find(m_anim_done_messages.begin(), m_anim_done_messages.end(), m);
		if(itor && itor != m_anim_done_messages.end())
		{
			m_anim_done_messages.erase(itor);
		}
	}

protected:
	real m_transition_time;
	int32 m_play_mode;

	std::vector<Message *> m_anim_done_messages;
};

}

}

