///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#undef MIN
#undef MAX
#undef ABS
#undef SQR
#undef CLAMP
#undef LTERP
#undef EQUAL
#undef DEG_TO_RAD
#undef RAD_TO_DEG

namespace wkg {

template<class T> inline T MIN(T a, T b) {
	return a < b ? a : b;
};
template<class T> inline T MIN3(T a, T b, T c) {
	return MIN(MIN(a, b), c);
}
template<class T> inline T MAX(T a, T b) {
	return a > b ? a : b;
};
template<class T> inline T MAX3(T a, T b, T c) {
	return MAX(MAX(a, b), c);
}
template<class T> inline T ABS(T a) {
	return a >= 0 ? a : -a;
};
template<class T> inline T SQR(T x) {
	return x * x;
};
template<class T> inline T CLAMP(T x, T lo, T hi) {
	return MIN(MAX(x, lo), hi);
}
template<class T> inline T LTERP(T f1, T f2, float t) {
	return f1 + (f2 - f1) * t;
}
template<class T> inline T DEG_TO_RAD(T x) {
	return (x * WKG_PI) / 180.0f;
}
template<class T> inline T RAD_TO_DEG(T x) {
	return (x * 180.0f) / WKG_PI;
}

#ifdef SINGLEPRECISION
template<> inline float ABS(float r) {
	union rl {
		float f; uint32 ul;
	}; ((rl*) &r)->ul &= 0x7fffffffUL; return r;
}
#else
template<> inline float ABS(float r) {
	union rl {
		float d; struct _ul {
			uint32 lo, hi;
		} ul;
	}; ((rl*) &r)->ul.hi &= 0x7fffffffUL; return r;
}
#endif

// Helper function to stuff a 32-bit float value into a 32-bit uint32 argument w/o conversion
inline uint32 FtoDW(float f) {
	return *((uint32*) &f);
}

// this must come after any specializations for ABS
template<class T> inline bool EQUAL(T f1, T f2, float tolerance = TOLERANCEf) {
	return ABS(f1 - f2) <= tolerance;
}

void memcpy(void* dest, const void* src, uint32 amt);
void memset(void* dest, int32 val, uint32 amt);
void memmove(void*, const void*, uint32);	// allows overlap
void memzero(void*, uint32);

#ifndef NO_WKG_STRING
int32 strlen(const char*);
uint32 strcpy_s(char*, uint32 bufLen, const char*);
int32 strcmp(const char*, const char*);
int32 stricmp(const char*, const char*);
#endif

int32 atoi(const char*);
float atof(const char*);

float cos(float);
float acos(float);
float sin(float);
float asin(float);
float tan(float);
float atan(float);
float sqrt(float);
double sqrt(double);

#ifndef SINGLEPRECISION
inline float MIN(float a, float b) {
	return MIN(float(a), b);
}
inline float MAX(float a, float b) {
	return MAX(float(a), b);
}
inline float CLAMP(float x, float lo, float hi) {
	return CLAMP(x, float(lo), float(hi));
}
inline bool EQUAL(float f1, float f2, float tolerance = TOLERANCEf) {
	return EQUAL(f1, float(f2), tolerance);
}
#endif

inline void MessageBox(const char *message) {
	::MessageBoxA(0, message, "Message", MB_OK);
}

}
