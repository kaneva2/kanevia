///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <d3d.h>
#include "singleton.h"
#include "texture.h"
#include "renderstate.h"
#include <Core/Math/Matrix.h>

namespace wkg {

class Device : public Singleton<Device>, public IDirect3DDevice9 {
public:
	static RenderState *m_rs;
	static D3DCAPS9 m_caps;
	static LPDIRECT3D9 m_d3d;

	Device() {
		m_d3d = 0;
		m_rs = new RenderState();
	}

	~Device() {
		if (m_d3d)
			m_d3d->Release();
		m_d3d = 0;

		delete m_rs;
		m_rs = 0;
	}

	void ForceDefaultRenderState();
	const RenderState& GetRenderState() const {
		return *m_rs;
	}

	void SetRenderState(const RenderState& rs);
	void RestoreRenderState(const RenderState &rs);
	void CaptureRenderState(RenderState &rs);

	static bool Init(Device **dev, HWND hwnd, bool windowed, int32 w = 0, int32 h = 0);
	static bool Init(Device **dev, HWND hwnd, bool windowed, int32 w, int32 h, IDirect3DDevice9 *d);

	HRESULT SetTexture(int32 stage, IDirect3DTexture9 *t);
	HRESULT SetTexture(int32 stage, const Texture *t);

	HRESULT SetTransform(D3DTRANSFORMSTATETYPE state, const KEP::Matrix44f *m);
	HRESULT SetTransform(D3DTRANSFORMSTATETYPE state, const D3DXMATRIX *m);

	HRESULT GetTransform(D3DTRANSFORMSTATETYPE state, KEP::Matrix44f *m);

	HRESULT SetVertexShaderConstantF(uint32 idx, const KEP::Matrix44f *m, uint32 n) {
		return ((IDirect3DDevice9 *)this)->SetVertexShaderConstantF(idx, (float *) m, n);
	}

	HRESULT SetVertexShaderConstantF(uint32 idx, const KEP::Vector4f *v, uint32 n) {
		return ((IDirect3DDevice9 *)this)->SetVertexShaderConstantF(idx, (float *) v, n);
	}

	HRESULT SetVertexShaderConstantF(uint32 idx, const float *v, uint32 n) {
		return ((IDirect3DDevice9 *)this)->SetVertexShaderConstantF(idx, v, n);
	}

};

} // namespace wkg


