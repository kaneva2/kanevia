///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace wkg {

class DynVB {
public:
	DynVB();
	~DynVB();

	LPDIRECT3DVERTEXBUFFER9 GetVB();
	uint8 GetVertSize();

	bool Prepare(LPDIRECT3DVERTEXBUFFER9 pVB, uint8 vertSize, int32 numBatches = 4);

	bool CanAppend(int32 numVerts, uint8** ppVert);			// returns false if no room currently available (i.e. needs BatchReady())
	bool BatchReady(int32* pStartVert, int32* pNumVerts);

protected:
	LPDIRECT3DVERTEXBUFFER9 pVB;
	uint8 vertSize;
	int32 maxVerts, maxBatches, vertsPerBatch;
	int32 startByte, vbNext;
	uint8* pBatch;
};

inline LPDIRECT3DVERTEXBUFFER9
DynVB::GetVB() {
	return pVB;
}

inline uint8
DynVB::GetVertSize() {
	return vertSize;
}

}

