#include "stdafx.h"
#include "MetricsListener.h"
#include "ClientImpl.h"
#include "SLACommon.h"



MetricsListener::MetricsListener() 
    :   StateChangeListener()
        , _logger( Logger::getInstance("ClientMetrics"))
		, _timer(  Timer::running )
{}

MetricsListener::~MetricsListener() 
{}

void 
MetricsListener::stateChanged( ClientInstance& clientInstance ) {
    CLIENT_STATE_TIME& prev = clientInstance.m_prevState;
    LOG4CPLUS_INFO( _logger,  clientInstance.userName() << " " << prev.stateToString().c_str() << ":{" << prev._timer.elapsed() << "}");
}


