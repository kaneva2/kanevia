#include "ClientInstance.h"
#include "ClientImpl.h"
#include "Event/Events/AutomationEvent.h"           // Seems terribly wrong to me that we keep our tool specific event
                                                    // in the platform
//////////////////////////
/// Client
/////////////////////////
Client* Client::createInstance( CountDownLatch&         completionLatch
                                , const UserConfig&     userConfig
                                , const HostConfig&     hostConfig
                                , const ClientConfig&   clientConfig ) {
    ClientInstance *newInstance = new ClientInstance(completionLatch, userConfig, hostConfig,clientConfig);
    newInstance->Start();
    newInstance->SetInitialZoneTime();
    return new Client(newInstance);
}


Client::~Client() {
    delete _impl;
}



unsigned long 
    Client::threadId() {
        return _impl->threadID();
}

bool Client::logPlayerOffServer(unsigned long msWait )
{
    LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "Client::logPlayerOffServer()");
    _impl->stop(msWait);
    return true;
}

Client& Client::setWebProxy( WebServicesProxy& webServices ){
    _impl->setWebProxy(webServices);
    return *this;
}

Client& Client::setMailer( MailerPlugin * mailer, jsFastSync * sync ){
    _impl->setMailer(mailer, sync);
    return *this;
}


Client& Client::queue( ClientCommand::Ptr cmd ) {
    _impl->queueCommand(cmd);
    return *this;
}

Client& Client::setGameId( unsigned long gameId ){
    _impl->setGameId(gameId);
    return *this;
}

Client& Client::addStateChangeListener( StateChangeListener& listener ) {
    _impl->addStateChangeListener( listener );
    return *this;
}

Client& Client::removeStateChangeListener( StateChangeListener& listener ) {
    _impl->removeStateChangeListener( listener );
    return *this;
}

bool Client::shutdown() {
    LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "Client::shutdown()");
    _impl->CleanUpEvents();
    return _impl->Shutdown() == jsThread::srStoppedOk;
}

bool Client::hasSentPendingSpawn() {
    LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "Client::hasSentPendingSpawn()");
    return (_impl->m_curState.state >= PENDING_SPAWN_SND);
}

bool Client::startZoning( vector<vector<string> > zones ) {
    _impl->m_zones = zones;
    _impl->m_startZoning = true;
    _impl->QueueEvent( new AutomationEvent( AutomationEvent::AE_START_ZONING ) );
    return true;
}

bool Client::stopZoning() {
    _impl->m_startZoning = false;
    _impl->QueueEvent( new AutomationEvent( AutomationEvent::AE_STOP_ZONING ) );
    return true;
}

bool Client::sendChatMessages( string chatMessage, UINT chatWaitTime )    {
    LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "Client::sendChatMessages()");
    _impl->m_chatMessage = chatMessage;
    _impl->m_chatWaitTime = chatWaitTime;
    return true;
}

bool Client::sendTellMessages( string tellMessage, UINT tellWaitTime ){
    LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "Client::SendTellMessages()");
    _impl->m_tellMessage = tellMessage;
    _impl->m_tellWaitTime = tellWaitTime;
    return true;
}

bool Client::sendAttachAttribMessages( UINT attribWaitTime )    {
    LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "Client::sendAttachAttribMessages()");
        _impl->m_attachAttribWaitTime = attribWaitTime; // and how does this actually incur "send attrib message"?
    return true;
}

bool Client::sendInvAttribMessages( UINT attribWaitTime ) {
    LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "Client::sendInvAttribMessages()");
        _impl->m_invAttribWaitTime = attribWaitTime;
    return true;
}

bool Client::isRunning() {
    return _impl->isRunning();
}

