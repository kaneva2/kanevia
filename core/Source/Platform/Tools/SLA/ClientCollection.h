#ifndef __CLIENTCOLLECTION_H__
#define __CLIENTCOLLECTION_H__

#include <vector>
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include "ClientInstance.h"
#include "ConfigurationElements.h"
#include "Plugins/App/WebServicesProxy.h"
#include "Common/keputil/AsynchronousCompletionToken.h"

#include "Common/KEPUtil/PluginHostConfigurator.h"
#include "Common/KEPUTIL/Mailer.h"

class MetricsListener;

// Here, rather than expose the ClientInstance class, we introduce a layer that 
// uses the ostensibly more generic "thread id".
// Wouldn't be my first choice, but such that it is.  In the meantime, let's introduce
// an intermediate layer, say "clientcollection" and a light weight wrapper for clientInstance
// 
class __declspec(dllexport) ClientCollection {
public:
    ClientCollection();
    virtual ~ClientCollection();

    ClientCollection& initialize(int argc, TCHAR* argv[] );
    ACTPtr start( /* IClientInstanceCallback * callback */ );
    MetricsListener& getMetricsAgent() {
        // Should likely provide a locking mechanism for this
        if (_metricsListener == 0 )
            throw NullPointerException(SOURCELOCATION, "No metrics agent available!");
        return *_metricsListener;
    }

private:
    bool InitializeClientInstance(   CountDownLatch& completionLatch
        , const UserConfig&             ui
        , const HostConfig&             targetHost
        , const ClientConfig&           client        );


    CharacterCreateStruct * createCharacter( vector<string>& ui, CharacterCreateStruct * pCharCreate );
    ClientConfig& clientConfig() {return _clientConfig;}
    UserConfig& userConfig() {return _ui; }


    unsigned int 	user_count;
//    unsigned int 	test_run_ms;
    unsigned int 	concurrent_logons;

    void seedRandom() {
        srand( CTickCount() );
    }

    std::string getRandomURL() {
        if ( _zones_info.size() <= 0 ) 
            throw RangeException(SOURCELOCATION);

        unsigned int i = rand() % _zones_info.size();
        return _zones_info[i][0];
    }

//    void assertValid(){
//        if ( !_user_info.size() ) throw ConfigException(SOURCELOCATION, "No users configured.");       //        fprintf( stderr, "No users configured\n" );
//    }
    typedef map <ULONG, Client*> ClientMap;
    ClientMap clients;



    // Non-static versions - implement with static versions
#if 0
    Client* getClient( ULONG threadId );
    UINT RunningCount(               bool onlyCareIfOne );

    bool ShutdownClientInstance(     ULONG threadId );
    bool HasSentPendingSpawn(        ULONG threadId );
    bool LogPlayerOffServer(         ULONG threadId );
    bool StartZoning(                ULONG threadId, vector<vector<string>> zones );
    bool StopZoning(                 ULONG threadId );
    bool SendChatMessages(           ULONG threadId, string chatMessage, UINT chatWaitTime );
    bool SendAttachAttribMessages(   ULONG threadId, UINT attribWaitTime );
    bool SendInvAttribMessages(      ULONG threadId, UINT attribWaitTime );
    bool SendTellMessages(           ULONG threadId, string chatMessage, UINT chatWaitTime );
    ClientCollection& logPlayerOffServer() ;
    ClientCollection& shutDownClientInstance();
    ClientCollection& stopZoning();
    ClientCollection& zoneAndActions();
    vector<unsigned long>   _threadIds;
#endif

    StringTable             _user_info;
    StringTable             _zones_info;        // used exclusively by zoneAndActions
    StringTable             _webcalls_info;

    UserConfig              _ui;
    ClientConfig            _clientConfig;
    HostConfig              _hostConfig;
    WebServicesProxy        _webServices;
    unsigned long           _gameId;
    MetricsListener*        _metricsListener;
    PluginHost              _pluginHost;
    MailerPlugin*           _mailer;
	jsFastSync				_syncEmailAlerts;
};
#endif