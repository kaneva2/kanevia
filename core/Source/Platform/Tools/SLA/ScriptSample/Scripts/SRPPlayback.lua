-- no need to doFile(KEP.lua) since it should have been included already

CMD_LOC = 2008
CMD_SRP	= 13

PLAYBACKFILE = "woknettest_api_test.krec"
PLAYBACKSPEED = ".2"

--[[
	/srp record <playername> [<comment>]
	To start recording the player on next login.  
	/srp play <filename>
	To playback a recording.  The filename is local to the server's GameFiles folder
	/srp list <prefix> [<start> [<count>] ]
 	To list recordings starting with the prefix, * for all files
	/srp stop
 	To stop all recordings.  Playback can only run if recordings are stopped.
	/srp status
 	Display the current status of the SRP tool
	/srp dump <fname>
 	Dump out a recording to a text file in human readable format.
	/srp pdd <fname> <diffApp and params>
	play-dump-diff plays <fname>, dumps <fname> and playback, then calls <diffApp>
]]--

function sendTextEvent( cmdId, text )

	local event    = Dispatcher_MakeEvent( KEP.dispatcher, "TextEvent" )

	Event_EncodeNumber( event, 0 ) -- ignored? playerId
	Event_EncodeString( event, text)
	Event_EncodeNumber( event, 0 ) -- chattype.talk
	Event_SetFilter( event, cmdId )
	Event_AddTo( event, KEP.SERVER_NETID )

	Dispatcher_QueueEvent( KEP.dispatcher, event )
end 

-- called by load script when we can start running
function onStart()
	print( "onStart" )
	sendTextEvent( CMD_LOC, "" )
	sendTextEvent( 0, "Hello, I'm in onStart" )
	sendTextEvent( CMD_SRP, "stop" )
end

-- called by load script when we should stop running
function onStop()
	print( "onStop!!!!" )
end

-- called by load script every TIMER_INTERVAL milliseconds
function onTick( tickCnt )
	-- do nothing 
end

function onText( msg, chatType )

	ParentHandler_LogMessage( KEP.parent, KEP.INFO_LOG_LEVEL,  "ASLT got text: "..msg )
	if msg == "All logging stopped" then 
		sendTextEvent( CMD_SRP, "pdd "..PLAYBACKFILE.." -m "..PLAYBACKSPEED.." lua52 API_Test_Parser.lua" )
		sendTextEvent( 0, "Sent the srp play command" )
	elseif msg == "Playback Dump Diff complete" then
		sendTextEvent( 0, "Got complete msg, script is stopping ASLT" )
		StopProcessing()
	end 

end 


