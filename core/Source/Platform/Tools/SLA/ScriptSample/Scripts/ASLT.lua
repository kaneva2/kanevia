dofile("KEP.lua")
dofile("ClientInstanceIds.lua")

---------------------------------------------------------------------
--
-- Script to load another script at runtime based on the ClientInstanceIds.SCRIPTNAME
-- value on the game object (which is the command line parameter passed into 
-- the ASLT exe).  
-- 
-- This script will load the other on and then call functions in that script, if they exists.
-- The script must at least have onStart().  
-- 
-- onTick(count) will be called every TIMER_INTERVAL milliseconds.  The loaded script 
-- has access to all the globals and loaded scripts in this script and can change that.
--
-- onText(msg,chatType) will be called whenever a text message comes down from the
-- server.
-- 
-- onStop is called if the exe is stopping.  Otherwise, the script can call the StopProcessing()
-- global function to stop the exe, when it has finished processing.
--
-- Note that if the script is is the Scripts folder it's possible to register new events and 
-- handlers in the script since it is processed like any other event handling script
-- Otherwise it may still do so in the onStart() function.
---------------------------------------------------------------------

-- filter values indicate the action
AE_START_ZONING = 1 -- eng->script indicator that zoning can start, logged on ok, etc
AE_STOP_ZONING = 2  -- eng->script indicator that zoning should stop
AE_SHUTDOWN = 3		-- script->eng script want to stop the engine

-- are we supposed to be running?  used to stop async stuff
gRunning = false
gTimerHitCount = 0

AUTOMATION_EVENT = "AutomationEvent"

-- event used by this script
ASLT_EVENT = "AsltEvent"
-- filter values
ASLT_TIMER = 1

-- milliseconds between our "Ticks"
TIMER_INTERVAL = 1000

-- global fns from loaded dofile
gOnStop = nil
gOnTick = nil
gOnText = nil

---------------------------------------------------------------------
--
-- helper fn to show all the values from the game object
--
---------------------------------------------------------------------
function DumpGameValues( dispatcher )

	g = Dispatcher_GetGameAsGetSet( dispatcher )

	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "Dumping GetSet values" )
	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "    GAMENAME = "..GetSet_GetString( g, ClientInstanceIds.GAMENAME ) )
	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "    BASEDIR = "..GetSet_GetString( g, ClientInstanceIds.BASEDIR ) )
	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "    USERNAME = "..GetSet_GetString( g, ClientInstanceIds.USERNAME ) )
	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "    SERVERNAME = "..GetSet_GetString( g, ClientInstanceIds.SERVERNAME ) )
	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "    EMAIL = "..GetSet_GetString( g, ClientInstanceIds.EMAIL ) )
	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "    URL = "..GetSet_GetString( g, ClientInstanceIds.URL ) )
	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "    SCRIPTNAME = "..GetSet_GetString( g, ClientInstanceIds.SCRIPTNAME ) )

	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "    PORT = "..tostring(GetSet_GetNumber( g, ClientInstanceIds.PORT ) ))
	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "    GAMEID = "..tostring(GetSet_GetNumber( g, ClientInstanceIds.GAMEID ) ))
	ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "    PARENTGAMEID = "..tostring(GetSet_GetNumber( g, ClientInstanceIds.PARENTGAMEID ) ))
end 

---------------------------------------------------------------------
--
-- start running the script they want us to run
--
---------------------------------------------------------------------
function StartScript()

	local scriptName = GetSet_GetString( g, ClientInstanceIds.SCRIPTNAME )
	if string.len(scriptName) > 0 then
		ParentHandler_LogMessage( KEP.parent, KEP.INFO_LOG_LEVEL,  "ASLT Loading Script: "..tostring(scriptName) )
		dofile(scriptName)
		
		env = getfenv (0) -- global vars
		gOnStop = env.onStop
		gOnTick = env.onTick
		gOnText = env.onText

		if env.onStart ~= nil then
			env.onStart()
		else			
			ParentHandler_LogMessage( KEP.parent, KEP.INFO_LOG_LEVEL,  "ASLT Script missing entry points" )
		end 
		
	else		
		ParentHandler_LogMessage( KEP.parent, KEP.INFO_LOG_LEVEL,  "ASLT No script specified to run" )
	end 
    
end

---------------------------------------------------------------------
--
-- stop running the script
--
---------------------------------------------------------------------
function StopScript()
	if env.onStop ~= nil then
		env.onStop()
	end 		
end 

---------------------------------------------------------------------
--
-- handle events from the automaiton engine
--
---------------------------------------------------------------------
function AutomationEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	ParentHandler_LogMessage( KEP.parent, KEP.DEBUG_LOG_LEVEL,  "ASLT AutomationEvent: "..tostring(filter) )

	if filter == AE_START_ZONING then
	
		gRunning = true
		e = Dispatcher_MakeEvent( dispatcher, ASLT_EVENT )
		Event_SetFilter( e, ASLT_TIMER )
		Dispatcher_QueueEventInFutureMS( dispatcher, e, TIMER_INTERVAL )

		DumpGameValues( dispatcher )

		StartScript()
		
	elseif filter == AE_STOP_ZONING then
	
		gRunning = false;
		StopScript()
		
	end
	
	return KEP.EPR_OK -- let C++ log or do whatever
end

---------------------------------------------------------------------
--
-- standard render text event 
--
---------------------------------------------------------------------
function RenderTextEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	local msg = Event_DecodeString( event )
	local chatType = Event_DecodeNumber( event )

	if gOnText then
		gOnText( msg, chatType )
	else
		ParentHandler_LogMessage( KEP.parent, KEP.TRACE_LOG_LEVEL,  "ASLT RenderText: "..msg )
	end		

	return KEP.EPR_OK -- let C++ log or do whatever
end

---------------------------------------------------------------------
--
-- helper fn to stop running the ASLT tool.  Called if script wants to end exe
--
---------------------------------------------------------------------
function StopProcessing()
	ev = Dispatcher_MakeEvent( KEP.dispatcher, AUTOMATION_EVENT )
	Event_SetFilter( ev, AE_SHUTDOWN )
	Dispatcher_QueueEvent( KEP.dispatcher, ev )
	ParentHandler_LogMessage( KEP.parent, KEP.DEBUG_LOG_LEVEL,  "ASLT AutomationEvent: shutting thread down" )
end

---------------------------------------------------------------------
--
-- handle our own little events
--
---------------------------------------------------------------------
TEST = 0
function testTick()

	if TEST ~= 0 then 
		ParentHandler_LogMessage( KEP.parent, KEP.DEBUG_LOG_LEVEL, "AsltEventHandler Timer! "..gTimerHitCount )
		
		if gTimerHitCount == 3 then
		
			-- arbitrary zoning to the mall
			g = Dispatcher_GetGameAsGetSet( dispatcher )
			local url = "kaneva://"..GetSet_GetString( g, ClientInstanceIds.GAMENAME ).."/Mall.zone"
			
			ev = Dispatcher_MakeEvent( dispatcher, KEP.GOTOPLACE_EVENT_NAME )
			Event_SetFilter( ev, KEP.GOTO_URL )
			Event_EncodeNumber( ev, 0 ) -- 0 = char
			Event_EncodeString ( ev, url) 
			Dispatcher_QueueEvent( dispatcher, ev )
			ParentHandler_LogMessage( KEP.parent, KEP.DEBUG_LOG_LEVEL,  "ASLT AutomationEvent: sending to the mail" )
		elseif gTimerHitCount == 100 then
			ev = Dispatcher_MakeEvent( dispatcher, AUTOMATION_EVENT )
			Event_SetFilter( ev, AE_SHUTDOWN )
			Dispatcher_QueueEvent( dispatcher, ev )
			ParentHandler_LogMessage( KEP.parent, KEP.DEBUG_LOG_LEVEL,  "ASLT AutomationEvent: shutting thread down" )
		end
	end 
end

---------------------------------------------------------------------
--
-- handle the script-specific event
--
---------------------------------------------------------------------
function AsltEventHandler(dispatcher, fromNetid, event, eventid, filter, objectid)

	if gRunning then 
		if filter == ASLT_TIMER then
			Dispatcher_QueueEventInFutureMS( dispatcher, event, TIMER_INTERVAL )
			gTimerHitCount = gTimerHitCount + 1

			testTick()
			
			if gOnTick ~= nill then
				gOnTick( gTimerHitCount )
			end 				
		end 
	end 

	return KEP.EPR_OK -- let C++ log or do whatever
end


---------------------------------------------------------------------
--
-- Called from C to initialize all the handlers in this script
--
---------------------------------------------------------------------
function InitializeKEPEventHandlers( dispatcher, handler, debugLevel )

	KEP.parent = handler
	KEP.dispatcher = dispatcher
	KEP.debugLevel = debugLevel
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "AutomationEventHandler", 1,0,0,0, AUTOMATION_EVENT, KEP.MED_PRIO+1 ) 
	ParentHandler_RegisterEventHandler( dispatcher, handler, "AsltEventHandler", 1,0,0,0, ASLT_EVENT, KEP.MED_PRIO+1 ) 
	
	ParentHandler_RegisterEventHandler( dispatcher, handler, "RenderTextEventHandler", 1,0,0,0, "RenderTextEvent", KEP.MED_PRIO+1 ) 

	ParentHandler_LogMessage( KEP.parent, KEP.INFO_LOG_LEVEL,  "ASLT Handlers registered" )
end

---------------------------------------------------------------------
--
-- Called from C to initialize all the events
--
---------------------------------------------------------------------
function InitializeKEPEvents( dispatcher, handler, debugLevel )
	KEP.dispatcher = dispatcher
	Dispatcher_RegisterEvent( dispatcher, 1,0,0,0, ASLT_EVENT, KEP.MED_PRIO )
end
