#ifndef __METRICS_LISTENER_H__
#define __METRICS_LISTENER_H__

#include "StateChangeListener.h"
#include "ClientImpl.h"

class MetricsListener : public StateChangeListener {
public:
    MetricsListener();
    virtual ~MetricsListener();
    void stateChanged( ClientInstance& clientInstance );

    string toXML() {
        return "<sla><metrics/></sla>";
    }
private:
    Timer   _timer;
    Logger  _logger;
};
#endif