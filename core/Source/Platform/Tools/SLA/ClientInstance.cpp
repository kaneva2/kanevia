/******************************************************************************
 ClientInstance.cpp

 Copyright (c) 2004-2009 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include <sstream>
#include "ClientInstance.h"
#include "ClientCommand.h"
#include "event\events\GotoPlaceEvent.h"
#include "ChatType.h"
#include "common/keputil/Algorithms.h"
#include "Core/Util/ChainedException.h"
#include "common/keputil/Runnable.h"
#include "common/keputil/Timer.h"
#include "common/keputil/Algorithms.h"
#include "event\events\AttribEvents.h"
#include "event\events\GetAttributeEvent.h"
#include "event\events\RenderTextEvent.h"
#include "event\events\DynamicObjectEvents.h"
#include "event\events\AutomationEvent.h"
#include "event\events\ShutdownEvent.h"
#include "event\events\DDREvent.h"
#include "event\events\TextEvent.h"
#include "event\events\GenericEvent.h"
#include "common/include/CoreStatic/CPacketQueClass.h"
#include "DPlayHandler.h"
#include <GetSet.h>
#include <GetBrowserPage.h>
#include "ClientImpl.h"
#include "Event\Events\GotoPlaceEvent.h"
#include "Commands/ConnectCommand.h"
#include "Commands/LoginCommand.h"
#include "Commands/SelectCharacterCommand.h"
#include "Commands/SendEntityGUpdateCommand.h"
#include "Commands/SendTextCommand.h"
#include "Commands/SleepCommand.h"
#include "Commands/ArrestorCommand.h"
#include "Commands/LogoutCommand.h"
#include "Commands/HomeCommand.h"
#include "Commands/SpawnToRandomURL.h"
#include "Commands/SpawnToURLCommand.h"
#include "Commands/ZoneCommand.h"
#include "Commands/Move.h"
#include "Commands/SendText.h"

using namespace KEP;

#define ZONE_URL 0
#define ZONE_SERVER 1
#define ZONE_PORT 2
#define ZONE_DDR 3

#define TELL_CMD 2035 //tell command

static const ULONG PROTOCOL_VERSION = (ULONG)0x00008026;
static ULONG GetContentVersion( IN LPCTSTR dir );
static const string StateToString( CLIENT_STATE state );

Logger audit_logger = Logger::getInstance( "AuditLog" );
Logger alert_logger = Logger::getInstance( "AlertLog" );


// trimmed down version of ClientEngine::ParseBrowserConnectResult
// to parse login web call results to get game ids
static bool ParseBrowserConnectResult( string resultText, string & resultCode, string & resultDescription, int& gameId, string & gameName,
													int &parentGameId )
{
    locale loc ( "" );
    vector<string> toks;
    StringHelpers::explode(toks, resultText.c_str(), "\t" );
    int i = 0;  
    for (   vector<string>::iterator iter = toks.begin()            ; iter != toks.end()            ; iter++ ) { //int i = 0; ii != kstring::npos; i++ )

        string& token = *iter;
		switch (i++)		{
		case 0: resultCode.assign(token);	if ( token.empty() || !isdigit( token.at(0), loc ) )				return false;			break;
		case 1:	resultDescription.assign(token);			break;
		case 2: break; // userId.assign(token);	            if ( token.empty() || !isdigit( token.at(0), loc ) )				return false;
		case 3:	break; // username.assign(token);
		case 4:	break; // serverip.assign(token);
		case 5:	break; // serverport.assign(token);			if ( token.empty() || !isdigit( token.at(0), loc ) )				return false;
		case 6: break; // zoneindex.assign(token);			if ( token.empty() || !isdigit( token.at(0), loc ) )				return false;
		case 7:	break; // gender.assign(token); done = true;
        case 8:
            if ( token.empty() || !isdigit( token.at(0), loc ) )
				return false;
			gameId = atol( token.c_str() );		
			break;
		case 9:			gameName.assign( token.c_str() );			break;
		case 10:
			if ( token.empty() || !isdigit( token.at(0), loc ) )
				return false;
			parentGameId = atoi( token.c_str() );
			break;
		}
	}

	return toks.size() >= 8;
}

// helper class for isendng events
class NetworkSender : public ISendHandler   {
public:
	NetworkSender( IClientNetwork *sender, ULONG timeout = 1000 ) : m_sender(sender), m_timeout(timeout) {}

	virtual EVENT_PROCESSING_RET  SendEventTo( IN IEvent *e )	{
		PCBYTE buff;
		ULONG len;
		
		e->GetOutputBuffer( buff, len );
		
		for ( NETIDVect::iterator i = e->To().begin(); i != e->To().end(); i++ )
			NETRET netRet = m_sender->Send( *i, buff, len, ((e->Flags() & EF_GUARANTEED) == EF_GUARANTEED) ? NSENDFLAG_GUARANTEED : NSENDFLAG_NOCOMPLETE, m_timeout );

		return EPR_OK;
	}

	IClientNetwork *m_sender;
	ULONG			m_timeout;
};

class BlockingFlag {
public:
    BlockingFlag(bool initialState = false, bool manualreset = true ) 
        : _event(0)    {
        _event = ::CreateEvent(0,manualreset,initialState,0);
    }

    virtual ~BlockingFlag() {
        ::CloseHandle(_event);
    }

    BlockingFlag& clear() {
        if ( _event )
            ::ResetEvent(_event);
        return *this;
    }

    BlockingFlag& signal() {
        if ( _event )
            ::SetEvent(_event);
        return *this;
    }

    jsWaitRet waitForSignal(unsigned long timeout = INFINITE_WAIT ) {
        switch( ::WaitForSingleObject(_event, timeout ) ) {
        case WAIT_OBJECT_0: return WR_OK;
        case WAIT_TIMEOUT:  return WR_TIMEOUT;
        default:            return WR_ERROR;
        };
    }

private:
    HANDLE  _event;
};


class MovementEngine {
public:
    class MyRunnable : public KEP::Runnable {
    public:
        MyRunnable( ClientInstance&     instance
                    , BlockingFlag&     sync 
                    , bool&             running
                    , Logger            logger ) 
            :   _instance(instance) 
                , _sync(sync)
                , _running(running)
                , _logger(logger)
        {}

        virtual ~MyRunnable() 
        {
            int n = 1;
        }
#if 1
        RunResult run() {
            _running = true;
            while ( true ) {
                _sync.waitForSignal();
                // not resuming...why?
                if (!_running) break;
                LOG4CPLUS_DEBUG( _logger, "Sending move!" );
                _instance.SendMovMsg(0);
                ::Sleep(333);   // Magic number...approximately 1/3 of a second...could likely turn this down.
            }
            return RunResult();
        }
#else
        RunResult run() {
            _running = true;
            while ( _running ) {
                _sync.waitForSignal(333);
                // not resuming...why?
                if (!_running) break;
                LOG4CPLUS_DEBUG( _logger, "Sending move!" );
                _instance.SendMovMsg(0);
//                ::Sleep(333);   // Magic number...approximately 1/3 of a second...could likely turn this down.
            }
            return RunResult();
        }
#endif


        //        MovementEngine& _engine;
        ClientInstance& _instance;
        bool&           _running;
        BlockingFlag&   _sync;
        Logger          _logger;
    };
    // end MyRunnable

    MovementEngine() 
        :   _instance(0)
            , _running(false)
            , _executor("MovementEngine")
            , _logger( Logger::getInstance("SLA.Client.MovementEngine") )
    {}

    virtual ~MovementEngine() {
        int n = 1;
    }

    MovementEngine& pause()     { 
        LOG4CPLUS_INFO( _logger, "MovementEngine::pause()" );
        _pauseSem.clear(); 
        return *this;   
    }

    MovementEngine& resume()    { 
        LOG4CPLUS_INFO( _logger, "MovementEngine::resume()" );
        _pauseSem.signal(); 
        return *this; 
    }

    MovementEngine& start( ClientInstance& instance )     { 
        LOG4CPLUS_INFO( _logger, "MovementEngine::start()" );
        _instance = &instance;
        MyRunnable* runnable = new MyRunnable(  *_instance
                                                , _pauseSem
                                                , _running
                                                , _logger );
        _executor.start();
        _executor.execute( ExecutablePtr( new Executable(KEP::RunnablePtr(runnable))));
        return *this;                     
    }

    MovementEngine& stop()      { 
        LOG4CPLUS_INFO( _logger, "MovementEngine::stop()" );
        _executor.stop();
        _running = false; 
        _pauseSem.signal();
        return *this;   
    }

    Logger              _logger;
    jsThreadExecutor    _executor;
    ClientInstance*     _instance;
    bool                _running;
    BlockingFlag        _pauseSem;
};


ClientInstance::ClientInstance( CountDownLatch&         completionLatch
                                , const UserConfig&       user
                                , const HostConfig&     host
                                , const ClientConfig&   client ) 
    :   _completionLatch(completionLatch)
        , _loopExited(false)
        , _stateChangeMC()
        , _eventServiceThread("EventServiceThread")
        , _dplayHandler(this)
        , gs_username(user.name())
        , gs_servername(host.host())
        , gs_port(host.port())
        , gs_baseDir(client.baseDir())
        , IGame(ENGINE_CLIENT)
        , _userInfo( user )
        , _clientConfig(client)
        , jsThread( (string("ClientInstance:") + user.name()).c_str())
        , m_clientNetwork(0)
        , m_dispatcher(ESEC_CLIENT)
        , m_reconnectEvent(0)
        , m_networkDefinedId(-1)
        , m_chatMessage("")
        , m_tellMessage("")
        , m_startZoning(false)
        , m_initialZoneWaitTime(0)
        , m_tellWaitTime(0)
        , m_attachAttribWaitTime(0)
        , m_invAttribWaitTime(0)
        , m_loggedOutZoneTime(true)
		, m_sessionProtocolVersion(0)
        , m_logger(Logger::getInstance(LOGGER_NAME))
        , m_eventBladeFactory(ENGINE_CLIENT)
        , gs_gameId(0)
        , gs_parentGameId(0)
        , _input() 
        , _movementEngine(new MovementEngine())
		, _mailer(NULL)
		, _syncEmailAlerts(NULL)
		, _messageSentTimer( Timer::running )
		, _tellSentTimer( Timer::running )
		, _zoneTimer( Timer::running  )
{
	m_prevState.state = NOT_CONNECTED;
	m_curState.state = NOT_CONNECTED;
}

ClientInstance::~ClientInstance()   {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//    jsFastLock lock(_StateChangeSync);
	if ( m_clientNetwork ) {
	    m_clientNetwork->Delete();
        m_clientNetwork = 0;
    }
    delete _movementEngine;
}

void ClientInstance::CleanUpEvents()    {	m_dispatcher.StopProcessingEvents();    }

ClientInstance& ClientInstance::startMoving() {
    _movementEngine->resume();
    return *this;
}

ClientInstance& ClientInstance::stopMoving() {
    _movementEngine->pause();
    return *this;
}




// Move this to separate state machine.
//
void ClientInstance::SwitchClientState( CLIENT_STATE newState ) {
    CLIENT_STATE_TIME newStateTime(newState);
	
	LOG4CPLUS_DEBUG(    Logger::getInstance( LOGGER_NAME )
                        , "[" << gs_username << "] " << m_curState.str() << " -> " << newStateTime.str() ); //StateToString(m_curState.state) << " -> " << StateToString(newState) << " : " << m_curState._timer.elapsed()));
	
    m_curState._timer.pause();

    m_prevState = m_curState;
    m_curState  = newStateTime;
    m_curState._timer.reset();

    // not clear why these specialty state changes exist.
    // In either case they would be better handled by
    // a StateListener
    // Now call our listeners
    _stateChangeMC.fireStateChanged(*this);

	if ( m_curState.state == ZONING && m_loggedOutZoneTime )	{
        _zoneTimer = m_curState._timer;
		m_loggedOutZoneTime = false;
	}
	else if ( m_curState.state == SPAWN_STRUCT_SND )	{
        ULONG timediff = m_curState._timer - _zoneTimer;
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), _userInfo.name().c_str() << " : " << gs_servername.c_str() << " : ZONE : " << timediff );
		m_loggedOutZoneTime = true;
	}
    
    wakeUp();
}


ClientInstance&
    ClientInstance::addStateChangeListener( StateChangeListener& listener ) {   
        _stateChangeMC.addStateChangeListener(listener);
//        _stateChangeVectors.insert( SCLVectorPairT(&listener,new StackTrace()) );
//        _stateChangeListeners.insert( pair<StateChangeListener*,StateChangeListener*>(&listener,&listener) ); 
        return *this;
}
ClientInstance&
ClientInstance::removeStateChangeListener( StateChangeListener& listener ) {   
    _stateChangeMC.removeStateChangeListener(listener);
        // Note that we can only schedule the removes.   We have no guarantees
        // that the listener isn't removing itself during iteration
        //
//        _pending_removes.push_back(&listener);
        //    SCLCollectionT::iterator i = _stateChangeListeners.find(&listener);
        //    if ( i != _stateChangeListeners.end() ) _stateChangeListeners.erase(i);
        return *this;
}



const CLIENT_STATE_TIME& 
ClientInstance::getState() {
    return m_curState;
}


ClientInstance& ClientInstance::queueCommand( ClientCommand::Ptr cmd ) {
    _input.push(cmd);
    return *this;
}

bool ClientInstance::Connect()  {
	if ( !m_clientNetwork ) {
		m_clientNetwork = MakeClientNetwork( gs_baseDir.c_str(), LOGGER_NAME, false );
    }

	if ( !m_clientNetwork ) 
        return false;
	

    SwitchClientState( CONNECTING ); // Let LoginCommand LogPlayerIntoServer set the state.

    m_dispatcher.SetSendHandler( new NetworkSender(m_clientNetwork) );
	m_dispatcher.SetNetId( -1 );

	TCHAR curDir[_MAX_PATH];
	_getcwd( curDir, _MAX_PATH );
	CONTEXT_VALIDATION cntxValidation;

	// hash the m_password
	MD5_DIGEST_STR pw;
    string userName(gs_username);
    StringHelpers::lower(userName);
	hashString((_userInfo.password()+userName).c_str(), pw);
	lstrcpyn(cntxValidation.password,pw.s, sizeof(cntxValidation.password) );
	lstrcpyn(cntxValidation.username, gs_username.c_str(), sizeof(cntxValidation.username));

	cntxValidation.m_category           = LOGON_CLIENT;
	cntxValidation.protocolVersionInfo  = PROTOCOL_VERSION;
	cntxValidation.contentVersionInfo   = GetContentVersion(curDir);

    string serverName   = gs_servername;
    unsigned short port = gs_port;
#if 1
    vector<ServerListItem> servers = _webServices.enumerateGameServers(_gameId, _clientConfig.excludedHosts() );



#else
    ServerListItem i;
    i._serverName = "bandrew-lptp2.kaneva.com";
    i._port="25857";
    vector<ServerListItem> servers;
    servers.push_back(i);
#endif



    if ( servers.size() == 0 )
        return false;
    {
        srand(CTickCount());
        int     r = rand();
        size_t  c = servers.size();
        int ndx = r % c;
        serverName = servers[ndx]._ipAddress;
        port = atoi(servers[ndx]._port.c_str());
    }
    
	HRESULT hr = m_clientNetwork->Connect(  &_dplayHandler
                                            , port
                                            , serverName.c_str()
                                            , (BYTE *)&cntxValidation
											, sizeof(cntxValidation) );
	return!FAILED( hr );
}

void ClientInstance::SendData( BYTE * data, DWORD size, DWORD flags, NETID sendTo, DWORD timeout )  {
	if ( !m_clientNetwork ) return;
    if ( S_OK != m_clientNetwork->Send( sendTo, data, size, flags, timeout)  )
        throw NetworkException2();
}

bool ClientInstance::LogPlayerIntoServer(){
    SwitchClientState( LOGGING_IN ); // Let LoginCommand LogPlayerIntoServer set the state.

	LPLOG_STRUCT    lpLoginMessage = NULL;
	DWORD           messageSize;
	messageSize = sizeof(LOG_STRUCT);
	lpLoginMessage = (LPLOG_STRUCT) new BYTE[messageSize];
	lpLoginMessage->m_category = LOG_TO_SERVER_CAT;
	
	lstrcpy(lpLoginMessage->logInName, _userInfo.name().c_str());
	lstrcpy(lpLoginMessage->logInPass, _userInfo.password().c_str());

	SendData(   (BYTE *)lpLoginMessage
                , messageSize
                , NSENDFLAG_GUARANTEED 
                    | NSENDFLAG_PRIORITY_HIGH
                , SERVER_NETID
                , 60000);

	if (lpLoginMessage) 
		delete lpLoginMessage;
	lpLoginMessage = 0;

	return true;
}


ClientInstance& 
ClientInstance::sendZoneDownloadComplete() {
//    ClientInstance *currentInstance = getInstance( threadId );
//        if ( currentInstance )
//        {
    // Switch the client state to zone complete
//            currentInstance->m_zones = zones;
//            currentInstance->m_startZoning = true;
    // If there is a script running...
//    QueueEvent( new AutomationEvent( AutomationEvent::AE_START_ZONING ) );

    EVENT_ID eId = eventMap().GetEventId("ZoneCustomizationDLCompleteEvent");
    if( eId == BAD_EVENT_ID )            
        throw ChainedException( SOURCELOCATION, "Event not found [ZoneCustomizationDLCompleteEvent" );

    IEvent *e = eventMap().MakeEvent( eId );
    if( !e ) 
        throw ChainedException( SOURCELOCATION, "Memory error.  Failed to allocate event.");

    e->AddTo( SERVER_NETID );
    QueueEvent( e );

    return *this;
}

//GOTO URL HAS TO CHANGE STATE TO ZONING AND THEN BLOCK UNTIL IT IS ZONED.  THIS way
//    client doesn't exit while debug server.

ClientInstance& ClientInstance::sendEntityGUpdate() {
    BYTE	*dataPacket = 0;
    ULONG    messageSize = 0;

	jsAssert( m_sessionProtocolVersion!=0 );
    CPacketObjList::PackLists(  dataPacket
                                , messageSize
                                , ENTITYGUPDATE_TO_SERVER_CAT
                                , 0 // GL_multiplayerObject->m_attributeOutBox
                                , 0 // GL_multiplayerObject->m_dmgOutBox
                                , 0 // GL_multiplayerObject->m_txtOutBox
                                , 0 // GL_multiplayerObject->m_equipOutBox
                                , 0 // statistics
                                , 0 // GL_multiplayerObject->m_txtgroupOutBox
                                , 0 // GL_multiplayerObject->m_miscOutBox
                                , 0 //GL_multiplayerObject->m_guaranteedEventOutBox );
								, m_sessionProtocolVersion );

    if ( !dataPacket ) 
        return *this;  // nothing to update.
        // throw ChainedException();

    SendData(   dataPacket
                , messageSize
                , NSENDFLAG_GUARANTEED | NSENDFLAG_PRIORITY_HIGH
                , SERVER_NETID
                , 1000 ); // Why do we need a timeout on a send operation? GL_multiplayerObject->m_packetTimeout );

    delete[] dataPacket;
    dataPacket = 0;
    return *this;
}


EVENT_PROC_RC  ClientInstance::AutomationEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e ){
	ClientInstance *me = (ClientInstance*)lparam;
	AutomationEvent *aeEvent = dynamic_cast<AutomationEvent*>(e);

	if ( aeEvent->Filter() == AutomationEvent::AE_SHUTDOWN )	{
		disp->QueueEvent( new ShutdownEvent() );
		me->_setTerminated();
	}
	
	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ClientInstance::LogonResponseEventHandler( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )    {
    ClientInstance *me = (ClientInstance*)lparam;
    KEP::LogonResponseEvent* lrEvent = dynamic_cast<KEP::LogonResponseEvent*>(e);
    return me->handleLoginResponseEvent( lrEvent );
}

EVENT_PROC_RC  ClientInstance::handleLoginResponseEvent(  LogonResponseEvent* event ) {
    if ( event == 0 ) throw NullPointerException(SOURCELOCATION);
    if ( event->m_hResultCode == S_OK && event->m_replyCode == LR_OK )	
		SwitchClientState( LOGGED_IN );

	return EVENT_PROC_RC::OK;
}

short ClientInstance::GetAttribCRC(LPATTRIB_STRUCT attribStruct)        {
	long genCRC     = attribStruct->attribute 
                        + attribStruct->uniInt 
                        + attribStruct->uniInt2;

	while (labs(genCRC) > 30000)
		genCRC = genCRC / 10;

    return (short) genCRC;
}

bool ClientInstance::SendSelectCharacterAttribMessage()     {
	LPATTRIB_STRUCT lpAttribMessage = NULL;
	DWORD messageSize;
	messageSize                 = sizeof(ATTRIB_STRUCT);
	lpAttribMessage             = (LPATTRIB_STRUCT) new BYTE[messageSize];
	lpAttribMessage->m_category =  ATTRIB_TO_CLIENT_CAT; // 6;        //
	lpAttribMessage->attribute  =  GetAttributeType::SELECT_CHARACTER; // 9;  
    
	lpAttribMessage->uniInt     = 0;        // ???????
	lpAttribMessage->idRefrence = 0;        // ???????
	lpAttribMessage->uniInt2    = -1;       // ???????
	lpAttribMessage->uniShort3  = 8;        // ???????              // It would appear that meaning uni* varies on
                                                                    // the context.  So what does it all mean.
	lpAttribMessage->CRCcheck   = GetAttribCRC(lpAttribMessage);
	BYTE* newMessage            = new BYTE[messageSize+1];
	newMessage[0]               = 24;
	
	for ( unsigned int i=0; i < messageSize;  i++ )
		newMessage[i+1] = ((BYTE*)lpAttribMessage)[i];

	SendData((BYTE *)newMessage, messageSize+1, NSENDFLAG_NOCOMPLETE | NSENDFLAG_PRIORITY_HIGH, SERVER_NETID, 60000);

	if (lpAttribMessage)	{
		delete lpAttribMessage;
		lpAttribMessage=0;
	}

	return true;
}

EVENT_PROC_RC 
ClientInstance::PendingSpawnEventHandler(   ULONG           lparam
                                            , IDispatcher*  disp
                                            , IEvent*    e  ){
	// default handler for this event assumes we are ready to spawn, simply reply back to server immediately
	PendingSpawnEvent *pse = dynamic_cast<PendingSpawnEvent *>(e);
	
	LONG    zoneIndex;
	USHORT  spawnId;
	bool    initialSpawn;
	long coverCharge;
	long zoneInstanceId;

	pse->extractInfo(zoneIndex, spawnId, initialSpawn, coverCharge, zoneInstanceId);

    ClientInstance *me = (ClientInstance*)lparam;
	me->SwitchClientState( PENDING_SPAWN_RCV );
	me->m_dispatcher.QueueEvent(new PendingSpawnEvent(zoneIndex, spawnId, SERVER_NETID, initialSpawn, coverCharge, zoneInstanceId));
	me->SwitchClientState( PENDING_SPAWN_SND );
	return EVENT_PROC_RC::CONSUMED;	
}

short ClientInstance::GetSpawnCRC(LPCTSTR worldName, short dbIndex, short currentTrace ){
	long genCRC = 0;
	for (size_t loop=0;loop<::strlen(worldName);loop++)
		genCRC += (int)(worldName[loop]);

	genCRC += (dbIndex + currentTrace);
	while (labs(genCRC) > 30000)
		genCRC = genCRC / 10;

	short finalValue = (short) genCRC;
	return finalValue;
}

bool ClientInstance::SendSpawnMsgToServer( int dbIndex, long zoneIndex, string worldFile )
{
	LPFROM_CLIENT_SPAWN_STRUCT lpSpawnMessage = NULL;
	DWORD messageSize;
	messageSize = sizeof(FROM_CLIENT_SPAWN_STRUCT)+worldFile.size();
	lpSpawnMessage = (LPFROM_CLIENT_SPAWN_STRUCT) new BYTE[messageSize];

	lpSpawnMessage->m_category      = 8;					// SPAWN_TO_SERVER_CAT
	lpSpawnMessage->currentTrace    = 1;					// Always 1 because EntObjRuntime will always only contain one entity
	lpSpawnMessage->dbIndex         = dbIndex;				// comes from SPAWN_TO_SERVER_CAT message
	lpSpawnMessage->usePresets      = -1;					// only set if entity is AI
	lpSpawnMessage->characterInUse  = 0;					// GL_multiplayerObject->m_currentCharacterInUse set from PlayCharacter glue
	lpSpawnMessage->aiCfgIndex		= 0;					// only set if entity is AI
	lpSpawnMessage->zoneIndex		= zoneIndex;			// comes from SPAWN_TO_SERVER_CAT message
	lstrcpy(lpSpawnMessage->worldName, worldFile.c_str());
	lpSpawnMessage->CRCcheck = GetSpawnCRC(lpSpawnMessage->worldName, lpSpawnMessage->dbIndex, lpSpawnMessage->currentTrace);

	SendData((BYTE *)lpSpawnMessage,
			 messageSize,
			 NSENDFLAG_GUARANTEED,
			 NULL,	// send to server
			 60000);

	if (lpSpawnMessage)
		delete lpSpawnMessage;
	lpSpawnMessage=0;
	SwitchClientState( SPAWN_STRUCT_SND );
	return TRUE;
}

bool ClientInstance::HandleAttribMessage( LPATTRIB_STRUCT formattedMessage, NETID idFromLoc )   {
	if ( formattedMessage->attribute != AE_PLAYER_CREATED_HEALTH ) return true;

	m_networkDefinedId = formattedMessage->uniInt;
	//m_dispatcher.SetNetId(m_networkDefinedId);
    LOG4CPLUS_TRACE( Logger::getInstance( LOGGER_NAME ), "NETID from AE_PLAYER_CREATED_HEALTH: " << formattedMessage->uniInt );
	return true;
}

EVENT_PROC_RC ClientInstance::ProcessRenderTextEvent( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	ClientInstance *me = (ClientInstance*)lparam;
	RenderTextEvent *rte = dynamic_cast<RenderTextEvent *>(e);
	kstring text;
	ChatType ct;

	//Doing this instead of extract info because
	//a tell is private, but does not have any
	//info for toDistribution or fromRecipient
	int temp;
	*(e->InBuffer())	>> text
						>> temp;

	ct = (ChatType)temp;

	return me->HandleRenderTextEvent( text.c_str(), ct );
}

EVENT_PROC_RC ClientInstance::HandleRenderTextEvent( LPCTSTR msg, ChatType ct ){
	string textReceived;
	CString lowerReceived( msg );
	lowerReceived.MakeLower();
	textReceived = (LPCTSTR)lowerReceived;

	switch ( ct )	{
		case ctTalk:	    m_gotServerResponse = CheckChat(textReceived);			break;
		case ctPrivate:		m_gotServerResponse = CheckTell(textReceived);			break;
		default:			LOG4CPLUS_WARN( Logger::getInstance( LOGGER_NAME ), "Unkown chat type received!" );
	}

	return EVENT_PROC_RC::OK;
}

bool ClientInstance::CheckChat(string textReceived)
{
	string textListen;

	ULONG timediff = _messageSentTimer.elapsed();

	textListen = _userInfo.name(); // m_username;
	textListen.append(" says > ");
	textListen.append(m_chatMessage);
	CString lowerTextListen( textListen.c_str() );
	lowerTextListen.MakeLower();
	textListen = (LPCTSTR)lowerTextListen;

	if ( textReceived == textListen )	{
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), _userInfo.name().c_str() << " : " << gs_servername.c_str() << " : CHAT TEXT : " << timediff );
	}
	else	{
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), "Mismatched chat message! - " << _userInfo.name().c_str() );
	}

	return true;
}

bool ClientInstance::CheckTell(string textReceived){
	string textListen;

	ULONG timediff = _tellSentTimer.elapsed();

	//if we find 'you' at the beginning of the string then it must be the echo of the tell we sent.
	if ( strstr(textReceived.c_str(), "you") == textReceived.c_str() )
	{
		LOG4CPLUS_TRACE( Logger::getInstance( LOGGER_NAME ), "Received echo: " << _userInfo.name().c_str() << " : " << gs_servername.c_str() << " : TELL TEXT : " << timediff << " : " << _userInfo.tellSrce().c_str() );
		return true;
	}

	textListen = _userInfo.tellSrce();
	textListen.append(" tells you > ");
	textListen.append(m_tellMessage);
	CString lowerTextListen( textListen.c_str() );
	lowerTextListen.MakeLower();
	textListen = (LPCTSTR)lowerTextListen;

	if ( textReceived == textListen )	{
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), _userInfo.name().c_str() << " : " << gs_servername.c_str() << " : TELL TEXT : " << timediff << " : " << _userInfo.tellSrce().c_str() );
	}
	else	{
		LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), "Mismatched tell message! - " << _userInfo.name().c_str() << " sender: " << _userInfo.tellSrce().c_str());
	}
	
	return true;
}

EVENT_PROC_RC ClientInstance::ProcessObjUrlEvent( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e )
{
	ClientInstance *me = (ClientInstance*)lparam;
	UpdateDynamicObjectUrlEvent *doue = dynamic_cast<UpdateDynamicObjectUrlEvent *>(e);

	return me->HandleObjUrlEvent();
}

EVENT_PROC_RC ClientInstance::HandleObjUrlEvent() {
	//ULONG timediff = TickDiff( TickCount(), m_mediaStartTime );
	//LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), m_username << " : " << m_servername << " : MEDIA : " << timediff );

	return EVENT_PROC_RC::OK;
}

bool ClientInstance::LogPlayerOffServer( bool reconnectingToOtherServer ){
    stopMoving();

    ClientExitEvent *e = new ClientExitEvent( reconnectingToOtherServer );
    e->AddTo( SERVER_NETID );
	e->SetFlags( EF_GUARANTEED | EF_SENDIMMEDIATE );
	m_dispatcher.QueueEvent( e );
	Sleep(1000); // sleep to let DPlay send, real client used 500, but too short for us

	// reset state to reconnect to the other server
	if ( reconnectingToOtherServer )	
		SwitchClientState( NOT_CONNECTED );

	return true;
}



// This handler is very different from that ClientEngine::gotoPlaceHandler()
EVENT_PROC_RC ClientInstance::GotoPlaceHandler(   ULONG           lparam
                                                                        , IDispatcher*  disp
                                                                        , IEvent*       e ) {
	ClientInstance *me = (ClientInstance*)lparam;
	
	GotoPlaceEvent *gpe = dynamic_cast<GotoPlaceEvent *>(e);
	gpe->ExtractInfo();
	string gameName;
	StpUrl::ExtractGameName(gpe->url, gameName);

	if ( gpe->type != GotoPlaceEvent::GOTO_URL ) 
        return EVENT_PROC_RC::OK;

	if ( STLCompareIgnoreCase(gameName.c_str(), me->_userInfo.gameName().c_str()) == 0 )	    {
		e->AddTo(SERVER_NETID);	

        // Why doesn't this cause this method to be called?
        // Does it make it into clientengine::GotoPlaceHandler?  Not likely as that isn't even linked in.
		disp->QueueEvent(e);
		return EVENT_PROC_RC::OK;
	}

	if ( !(gpe->port != 0 && gpe->server.length() > 0 ) ) 
        return EVENT_PROC_RC::OK;

	me->_userInfo.setGameName( gameName );
	me->gs_servername       = gpe->server;
	me->gs_port             = gpe->port;
	me->LogPlayerOffServer( true );
	me->m_reconnectEvent    = new GotoPlaceEvent();
	me->m_reconnectEvent->GotoUrl( 0, gpe->url.c_str() );
	me->m_reconnectEvent->AddTo( SERVER_NETID );
	return EVENT_PROC_RC::OK;
}



EVENT_PROC_RC ClientInstance::GotoPlaceFailureHandler(   ULONG           lparam
                    , IDispatcher*  disp
    , IEvent*       e ) {
        ClientInstance *me = (ClientInstance*)lparam;

        GotoPlaceEvent *gpe = dynamic_cast<GotoPlaceEvent *>(e);
        if ( !gpe ) {
            return EVENT_PROC_RC::OK;
        }
        gpe->ExtractInfo();
        string gameName;
        StpUrl::ExtractGameName(gpe->url, gameName);

        if ( gpe->type != GotoPlaceEvent::GOTO_URL ) 
            return EVENT_PROC_RC::OK;

        if ( STLCompareIgnoreCase(gameName.c_str(), me->_userInfo.gameName().c_str()) == 0 )	    {
            e->AddTo(SERVER_NETID);	

            // Why doesn't this cause this method to be called?
            // Does it make it into clientengine::GotoPlaceHandler?  Not likely as that isn't even linked in.
            disp->QueueEvent(e);
            return EVENT_PROC_RC::OK;
        }

        if ( !(gpe->port != 0 && gpe->server.length() > 0 ) ) 
            return EVENT_PROC_RC::OK;

        me->_userInfo.setGameName( gameName );
        me->gs_servername       = gpe->server;
        me->gs_port             = gpe->port;
        me->LogPlayerOffServer( true );
        me->m_reconnectEvent    = new GotoPlaceEvent();
        me->m_reconnectEvent->GotoUrl( 0, gpe->url.c_str() );
        me->m_reconnectEvent->AddTo( SERVER_NETID );
        return EVENT_PROC_RC::OK;
}

float ClientInstance::GetMovMsgCrC(LPSHRUNK_TOSVRMOV_STRUCT packet){
	float    genCRC = 0.0f;
	genCRC =(((float)packet->Xpos) +
			 ((float)packet->Ypos) +
			 ((float)packet->Zpos) +
			 ((float)packet->animGLID2nd) +
			 ((float)packet->animGLID));
	genCRC = genCRC * .1f;
	return genCRC;
}

#define UNKNOWN_CATEGORY_4 4
LPSHRUNK_TOSVRMOV_STRUCT ClientInstance::ShrinkDataCP(  LPMOVE_STRUCT OLD_DATA
                                                        , LPSHRUNK_TOSVRMOV_STRUCT SHRUNK_DATA)     {

	SHRUNK_DATA->m_category         = UNKNOWN_CATEGORY_4;
	SHRUNK_DATA->Xpos               = OLD_DATA->Xpos;  //shorts are inherently signed
	SHRUNK_DATA->Ypos               = OLD_DATA->Ypos;
	SHRUNK_DATA->Zpos               = OLD_DATA->Zpos;
	SHRUNK_DATA->OrientateDirX      = (byte) (127 * OLD_DATA->OrientateDirX);
	SHRUNK_DATA->OrientateDirY      = (byte) (127 * OLD_DATA->OrientateDirY);
	SHRUNK_DATA->OrientateDirZ      = (byte) (127 * OLD_DATA->OrientateDirZ);
	SHRUNK_DATA->OrientateUpX       = (byte) (127 * OLD_DATA->OrientateUpX);
	SHRUNK_DATA->OrientateUpY       = (byte) (127 * OLD_DATA->OrientateUpY);
	SHRUNK_DATA->OrientateUpZ       = (byte) (127 * OLD_DATA->OrientateUpZ);
	SHRUNK_DATA->FreeLookPitch      = (signed char)(OLD_DATA->FreeLookPitch);
	SHRUNK_DATA->animGLID           = (short) OLD_DATA->animGLID;
	SHRUNK_DATA->animGLID2nd	    = (short) OLD_DATA->animGLID2nd;
	SHRUNK_DATA->idReference        = OLD_DATA->idRefrence;
	SHRUNK_DATA->CRCCHECK           = GetMovMsgCrC(SHRUNK_DATA);

	return(SHRUNK_DATA);
}

bool ClientInstance::SendMovMsg( ULONG currentTimeDiff )        {

	LPMOVE_STRUCT lpChatMessage = NULL;
	DWORD messageSize;
	messageSize = sizeof(MOVE_STRUCT);
	lpChatMessage = new MOVE_STRUCT;
	if (lpChatMessage == NULL)
		return false;

    lpChatMessage->m_category       = 2;
	lpChatMessage->FreeLookYaw    = 0.0f;
	lpChatMessage->FreeLookPitch  = 0.0f;
	lpChatMessage->Xpos           = -19.0f;
	lpChatMessage->Ypos           = 0.0f;
	lpChatMessage->Zpos           = -14.0f;
	lpChatMessage->idRefrence     = m_networkDefinedId;
	lpChatMessage->OrientateDirX  = 0.0f;
	lpChatMessage->OrientateDirY  = 0.0f;
	lpChatMessage->OrientateDirZ  = 1.0f;
	lpChatMessage->OrientateUpX   = 0.0f;
	lpChatMessage->OrientateUpY   = 1.0f;
	lpChatMessage->OrientateUpZ   = 0.0f;
	lpChatMessage->animGLID       = 0;
	lpChatMessage->animGLID2nd	  = -1;

	LPSHRUNK_TOSVRMOV_STRUCT shrunkMessage = NULL;
	DWORD               shrunkMessageSize;
	shrunkMessageSize = sizeof(SHRUNK_TOSVRMOV_STRUCT);

    
    shrunkMessage = new SHRUNK_TOSVRMOV_STRUCT;
	if (shrunkMessage == NULL)
		return false;

	shrunkMessage->tmel = currentTimeDiff;
	shrunkMessage = ShrinkDataCP(lpChatMessage,shrunkMessage);

	SendData( (BYTE *)shrunkMessage, shrunkMessageSize, NSENDFLAG_NOCOMPLETE | NSENDFLAG_PRIORITY_HIGH, SERVER_NETID, 60000 );

	if (lpChatMessage) delete lpChatMessage;
	lpChatMessage=0;
	
	if (shrunkMessage) delete shrunkMessage;
	shrunkMessage=0;
	
	return true;
}

void ClientInstance::SetInitialZoneTime() {
	_sleep(500);
	srand(GetTickCount());
	m_initialZoneWaitTime = rand() % _clientConfig.zoneWaitMS();
}

bool ClientInstance::SpawnPlayerToZone( ULONG zi, ULONG instanceId ) {
	ZoneIndex gotoZoneIndex( zi );
	GotoPlaceEvent *gpe = new GotoPlaceEvent();
	gpe->GotoZone( gotoZoneIndex, instanceId, 0 );
	gpe->AddTo( SERVER_NETID );
	m_dispatcher.QueueEvent( gpe );
	return true;
}

ClientInstance&
ClientInstance::goHome() {
    stringstream ss;
    ss << "kaneva://World Of Kaneva/"
        << this->_userInfo.name()
        << ".home";

    LOG4CPLUS_DEBUG( Logger::getInstance("SLA.Client"), "goHome(" << ss.str() << ")" );
    SpawnPlayerToURL(   ss.str().c_str()
                        , _userInfo.server().c_str()
                        , _userInfo.port() );
    return *this;
}

bool ClientInstance::SpawnPlayerToURL(  const char *stpURL
                                        , const char *server
                                        , unsigned short port )   {
    SwitchClientState(ZONING);
	GotoPlaceEvent *gpe = new GotoPlaceEvent();
	gpe->GotoUrl( 0, stpURL, port, server );
	LOG4CPLUS_DEBUG(Logger::getInstance("SLA.Client"), "Requesting spawn to: " << stpURL);
	m_dispatcher.QueueEvent( gpe );
	return true;
}

bool ClientInstance::SendDDRStartGameEvent( int objectId )  {
	DDREvent *e = new DDREvent();
	e->SetFilter( DDREvent::DDR_START_LEVEL );
	(*e->OutBuffer()) << 369 << objectId << 3307 << 1 << true;
	e->AddTo( SERVER_NETID );
	e->EncryptEvent();
	m_dispatcher.QueueEvent( e );
	return true;
}

bool ClientInstance::SendDDRRecordScoreEvent( int objectId )    {
	DDREvent *e = new DDREvent();
	e->RecordScore( 369, objectId, 1, 0, 0, false, false, false, false, false );
	e->AddTo( SERVER_NETID );
	m_dispatcher.QueueEvent( e );

	return true;
}

bool ClientInstance::SendDDRGameEndEvent( int objectId )    {
	DDREvent *e = new DDREvent();
	e->PlayerExit( 369, objectId, true );
	e->AddTo( SERVER_NETID );
	m_dispatcher.QueueEvent( e );
	return true;
}

bool ClientInstance::SendTextEvent( string chatMessage, ChatType ct, int command )   {
	TextEvent *e = new TextEvent( chatMessage.c_str(), command, m_networkDefinedId, ct );
	e->AddTo( SERVER_NETID );
	m_dispatcher.QueueEvent( e );
	return true;
}

bool ClientInstance::SendAttribMessage( DWORD       flags
                                        , NETID     sendTo
                                        , int       attribute
                                        , int       uniInt
                                        , long      referenceID
                                        , int       uniInt2
                                        , short     uniShort3       )           {
	LPATTRIB_STRUCT lpChatMessage = NULL;
	DWORD messageSize;
	messageSize = sizeof(ATTRIB_STRUCT);
	lpChatMessage = (LPATTRIB_STRUCT) new BYTE[messageSize];

	lpChatMessage->m_category = 6;
	lpChatMessage->attribute = attribute;  
	lpChatMessage->uniInt = uniInt;
	lpChatMessage->idRefrence  = (short)referenceID;
	lpChatMessage->uniInt2     = uniInt2;
	lpChatMessage->uniShort3   = uniShort3;
	lpChatMessage->CRCcheck    = GetAttribCRC(lpChatMessage);

	BYTE* newMessage = new BYTE[messageSize+1];
	newMessage[0] = 24;
	
	for ( unsigned int i=0; i < messageSize;  i++ )	
		newMessage[i+1] = ((BYTE*)lpChatMessage)[i];

	SendData((BYTE *)newMessage, messageSize+1, NSENDFLAG_NOCOMPLETE | NSENDFLAG_PRIORITY_HIGH, SERVER_NETID, 60000);

	if (lpChatMessage)	{
		delete lpChatMessage;
		lpChatMessage=0;
	}

	if ( newMessage )	{
		delete newMessage;
		newMessage=0;
	}

	return TRUE;
}

bool ClientInstance::FillInLoginURL(    INOUT string&   s
                                        , IN string     emailaddress ) {
	bool ret = false;
	if ( emailaddress.empty() ) return false;
	string::size_type p = s.find( "{0}" );
	if ( p == string::npos ) return false;
	s = s.replace( p, 3, emailaddress);
    return true;
}

bool ClientInstance::FillInAuthURL( INOUT string &s, IN string userName, IN string password )
{
	bool ret = false;
	string::size_type p = s.find( "{0}" );
	if ( p != string::npos )
	{
		s = s.replace( p, 3, userName );
		if ( (p = s.find( "{1}" )) != string::npos )
		{
			s = s.replace( p, 3, password );
			// Once all servers are using STAR code,
			// the third parameter (game Id) should be required
			// before returning true; for now, set ret to true 
			// for backwards compatibility
			ret = true;
			/*
			if ( (p = s.find( "{2}" )) != string::npos )
			{
				s = s.replace( p, 3, numToString(m_gameId,"%d").c_str() );
			}
			*/
		}
	}
	return ret;
}


class EventThread : public KEP::Runnable {
public:
    EventThread( ClientInstance& clientInstance ) : _clientInstance(clientInstance)
    {}

    RunResult run() {
        //		while ( !isTerminated() && start.elapsed() < 1000 )		{
        //			if ( !m_dispatcher.ProcessNextEvent(100) ) 
        //				break;
        //		}
        _clientInstance.serviceEvents();
        return RunResult();
    }
private:
    ClientInstance& _clientInstance;
};

void ClientInstance::serviceEvents() {
    LOG4CPLUS_DEBUG( Logger::getInstance(LOGGER_NAME), "Enter event service loop!");

     // This is really unclear.  Note that the implication is that we are asking
     // if the service event thread, when in reality we are asking if the client's thread
     // is still running.  I.e. calling eventServiceThread.stop() does nothing...God
     //
     // Option 1:  this up one level. 

    while ( !_loopExited ) { //  && start.elapsed() < 1000 )		{
        m_dispatcher.ProcessNextEvent(1000);    // process next event, but only wait 1s.
                                                // Would prefer to go full block here.  
                                                // But not capable...yet.
   	}
    LOG4CPLUS_DEBUG( Logger::getInstance(LOGGER_NAME), "Exited event service loop!");
}

ClientInstance&
ClientInstance::startEventProcessing() {
    _eventServiceThread.start();
    _eventServiceThread.execute(ExecutablePtr(new Executable(RunnablePtr(new EventThread(*this) ))));
    return *this;
}

ClientInstance&
ClientInstance::stopEventProcessing() {
    _eventServiceThread.execute(ExecutablePtr(new Executable(RunnablePtr(new jsThreadExecutor::Arrestor() ) ) ) );
    _loopExited = true;
    return *this;
}

bool ClientInstance::GetBrowserPage( string url )           {
	// ignored parameters
	string resultText;
	DWORD httpStatusCode;
	string httpStatusDescription;
	return ::GetBrowserPage( url resultText, httpStatusCode, httpStatusDescription);
}


//class MovementEngine;
ULONG ClientInstance::_doWork() {
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

    _movementEngine->start(*this);

    startEventProcessing();
    m_dispatcher.StartProcessingEvents();

    Timer startMoveTimer(Timer::running);
    Timer startZoneTimer(Timer::running);
    Timer startAttachAttribTimer(Timer::running);
    Timer startInvAttribTimer(Timer::running);

	ULONG currentZoneWaitTime   = (ULONG)_clientConfig.zoneWaitMS();
	bool initialZoneAttempt     = true;

    // TODO: Call to TickCount()...s/b 86'd
    // TODO: Webcalls becomes an action and hence this, along
    //       with other references to webcalls_info, will be 
    //       relocated if not removed entirely.
    //
    webcallTimes.resize(_userInfo._webcalls_info.size(), Timer(Timer::running));

// This only works while we are preloading the commands
// else we need to decouple the listeners
    while ( true ) {
        try {
            LOG4CPLUS_DEBUG( Logger::getInstance("SLA.Client"), "Popping command" );
            ClientCommand::Ptr curr;
        
            // This loop comes to full stop until, and only until, in
            // other words we will wait indefinitely until a command is
            // returned.  When an external actor is ready to break this
            // object down, it submits an "Arrestor" command.  This is a
            // command that simply throws an ArrestedException.  When
            // this exception is caught here we leave.
            //
            _input.pop(curr);
            LOG4CPLUS_DEBUG( Logger::getInstance("SLA.Client"), "Executing command" );
            curr->doCommand(*this);
        }
        catch( const ArrestedException&  ce  ) {
            LOG4CPLUS_DEBUG( Logger::getInstance("SLA.Client"), "Client arrested." <<  ce.toString() );
            break;
        }
        catch( const ChainedException& ce ) {
            LOG4CPLUS_ERROR( Logger::getInstance("SLA.Client"), "Exception raised in main loop: " << ce.toString() );
            break;
        }
    }
    stopEventProcessing();
//    _loopExited = true;
    //	LogPlayerOffServer();
	
	m_clientNetwork->Delete();
	m_clientNetwork = 0;
	// no longer delete this since stop called mutiple times 

	FreeAllBlades( m_eventBlades );
	
    m_dispatcher.StopProcessingEvents();

    _movementEngine->stop();
    // eventThread uses an event dispatcher that uses polling rather than
    // full block.  This has a polling frequency of 1s...so we need to sleep
    // at least that long.  Will revisit this.  Movement engine should be 
    // using an arrestor
    //
    ::Sleep(2000);

    _completionLatch.countDown();

    //    cout << "Stopping event loop" << endl;
    //    ptr->act()->getResults();

    return 0;
}

void 
ClientInstance::SpawnSent(  bool &initialZoneAttempt
                            , Timer &startZoneTimer
                            , ULONG &currentZoneWaitTime                            ) 
{
    if ( m_networkDefinedId == -1 )	return; // break;
    if ( !m_startZoning )           return; // break;
    if ( m_zones.size() <= 0 )		return; 
    if ( initialZoneAttempt )  					{
        startZoneTimer.reset();
        currentZoneWaitTime = m_initialZoneWaitTime;
        initialZoneAttempt = false;
    }

    if ( startZoneTimer.elapsed() <= currentZoneWaitTime ) return;				

    SwitchClientState( ZONING );

    // Randomly select a zone from list of zones
    //
    goToRandomURL();

    startZoneTimer.reset();
    _zoneTimer.reset();
//    m_zoneSentTime      = TickCount();
    currentZoneWaitTime = _clientConfig.zoneWaitMS();
}

void 
ClientInstance::goToRandomURL() {
    if ( m_zones.size() <= 0 ) return;

    srand( (UINT)time(NULL) );
    unsigned int i = rand() % m_zones.size();

    // Transmit our spawn.
    //
    SpawnPlayerToURL(   m_zones[i][ZONE_URL].c_str()
                        , m_zones[i][ZONE_SERVER].c_str()
                        , (unsigned short)atoi(m_zones[i][ZONE_PORT].c_str()) );
}

//ClientInstance& 
void ClientInstance::doZone()       {
    SwitchClientState( ZONING );

    if (    _clientConfig.loginURL().toString().length() > 0 
        && _clientConfig.authURL().toString().length() > 0 )					{
            string loginURL = _clientConfig.loginURL().toString(); 
            FillInLoginURL( loginURL, _userInfo.email().c_str() );  //                        gs_email );
            string resultText;
            DWORD httpStatusCode;
            string httpStatusDescription;

            if ( ::GetBrowserPage( loginURL, resultText, httpStatusCode, httpStatusDescription) )						{
                string resultCode;
                string resultDescription;
                string gameName;
                ParseBrowserConnectResult( resultText, resultCode, resultDescription, gs_gameId, gameName, gs_parentGameId );
            }

            string authURL = _clientConfig.authURL().toString(); 
            FillInAuthURL( authURL, _userInfo.name(), _userInfo.password() );
            resultText = "";
            ::GetBrowserPage( authURL, resultText, httpStatusCode, httpStatusDescription );
    }
}


// todo: extract function:
//      ClientInstance::makeWebCall(<url>).  Then add a command to invoke.
//      When this is done it can be scripted and then it becomes incumbent
//      on the script to throttle?  Leave throttle in place for now.  costs us
//      nothing to do this.  Actually, yes it does, because that means that
//      have to use the sdf structure and track the last called time.
//
ClientInstance&
ClientInstance::makeWebCalls() {
    // Expects sdf records of following format:
    //  0:url
    //  1:throttle (cannot be executed more than once in specified time frame)
    //
    vector<vector<string>>::iterator it; 
    int i = 0;
    for (   it = _userInfo._webcalls_info.begin()
            ; it < _userInfo._webcalls_info.end()
            ; it++ )					{
        vector<string> currentWebCall = *it;
        if ( webcallTimes[i].elapsed() > (unsigned)atoi(currentWebCall[1].c_str()) )        {
            GetBrowserPage( currentWebCall[0] );
            webcallTimes[i].reset();
        }
        i++;
    }
    return *this;
}

ClientInstance&
ClientInstance::makeWebCall( const URL& url ) {
    GetBrowserPage( url.toString() );
    return *this;
}

// where the lua and python scripts live, under baseDir
// DLLs will be in SCRIPT_DIR\..\bladescripts(d)
#define SCRIPT_DIR "Scripts"


ClientInstance&
ClientInstance::registerEvents() {
    RegisterEvent<ClientExitEvent>(m_dispatcher);
    RegisterEvent<PendingSpawnEvent>(m_dispatcher);
    RegisterEvent<GotoPlaceEvent>(m_dispatcher);

    m_dispatcher.RegisterEvent( "BadGotoPlaceURLEvent"
                                , PackVersion(1,0,0,0)
                                , GenericEvent::CreateEvent
                                , 0
                                , EP_NORMAL
                                , ESEC_CLIENT 
                                | ESEC_SERVER, ESEC_CLIENT      );

    RegisterEvent<TextEvent>(m_dispatcher);
    RegisterEvent<UpdateDynamicObjectUrlEvent>(m_dispatcher);
    RegisterEvent<DDREvent>(m_dispatcher);
    RegisterEvent<AutomationEvent>(m_dispatcher);
    return *this;
}

ClientInstance&
ClientInstance::registerEventHandlers() {
    // I would really like to see this change as follows:
    // 
    //       m_dispatcher.RegisterHandler(  IEventHandlerPtr(new SyncHandler()), EP_NORMAL )
    //
    // Instead of passing in things like ClassVersion, ClassId etc..  Always uses EventHandler class
    // rather than a funciton pointer.  Then Dispatcher can ask the handler for this information like
    // name, class id, class version etc.
    //
    //
    IEventHandlerPtr syncHandler = IEventHandlerPtr( new EventSyncHandler( m_dispatcher ) );
    m_dispatcher.RegisterHandler(   syncHandler
                                    , EventSyncEvent::ClassVersion()
                                    , EventSyncEvent::ClassId() );

    m_dispatcher.RegisterHandler(   LogonResponseEventHandler
                                    , (ULONG)this
                                    , _T("ClientInstance::LogonResponseHandler")
                                    , LogonResponseEvent::ClassVersion()
                                    , LogonResponseEvent::ClassId()
                                    , EP_NORMAL );

    m_dispatcher.RegisterHandler(   PendingSpawnEventHandler
                                    , (ULONG)this
                                    , "PendingSpawnEvent"
                                    , PackVersion(1,0,0,0)
                                    , PendingSpawnEvent::ClassId()
                                    , EP_NORMAL );		

    m_dispatcher.RegisterHandler(   GotoPlaceHandler
                                    , (ULONG)this
                                    , "GotoPlaceEvent"
                                    , GotoPlaceEvent::ClassVersion()
                                    , GotoPlaceEvent::ClassId()
                                    , EP_NORMAL );

    m_dispatcher.RegisterHandler(   GotoPlaceFailureHandler
                                    , (ULONG)this
                                    , "BadGotoPlaceURLEvent"
                                    , PackVersion(1,0,0,0)
                                    , 0
                                    , EP_NORMAL );

//    m_dispatcher.RegisterEvent( "BadGotoPlaceURLEvent"
//        , PackVersion(1,0,0,0)
//        , GenericEvent::CreateEvent
//        , EP_NORMAL
//        , ESEC_CLIENT 
//        | ESEC_SERVER, ESEC_CLIENT      );


    m_dispatcher.RegisterHandler(   ProcessRenderTextEvent
                                    , (ULONG)this
                                    , "RenderTextEvent"
                                    , RenderTextEvent::ClassVersion()
                                    , RenderTextEvent::ClassId()
                                    , EP_NORMAL );

    m_dispatcher.RegisterHandler(   ProcessObjUrlEvent
                                    , (ULONG)this, "UpdateDynamicObjectUrlEvent"
                                    , UpdateDynamicObjectUrlEvent::ClassVersion()
                                    , UpdateDynamicObjectUrlEvent::ClassId()
                                    , EP_NORMAL );	

    // Provides handling for Automation events, that is, events specific to SLA. e.g. StartZoning,StopZoning,Shutdown
    //
    m_dispatcher.RegisterHandler(   AutomationEventHandler
                                    , (ULONG)this
                                    , "AutomationEventHandler"
                                    , AutomationEvent::ClassVersion()
                                    , AutomationEvent::ClassId()
                                    , EP_NORMAL );	
    return *this;
}

bool ClientInstance::Start()    {
	m_dispatcher.SetGame(this);
    registerEvents();

    // What is this doing?  If the user info specified a starting url
	if ( _userInfo.startingURL().toString().length() > 0 )	{
		m_reconnectEvent = new GotoPlaceEvent();
		m_reconnectEvent->GotoUrl( 0, _userInfo.startingURL().toString().c_str() );
		m_reconnectEvent->AddTo( SERVER_NETID );
	}
#if 0       // Disabled to just get rid of the noise
	// load scripting events and  handlers
	InitScriptBlades(   m_logger
                        , &m_dispatcher
                        , PathAdd(gs_baseDir, SCRIPT_DIR ).c_str()
                        , true
                        , 0
                        , m_eventBladeFactory
                        , m_eventBlades );
#endif
    registerEventHandlers();
    start();                    // Start the command/event loop (doWork())
	return true;
}

jsThread::StopRet ClientInstance::Shutdown()    {
	jsThread::StopRet ret = jsThread::srStoppedOk;
	if ( isRunning() )
		ret = stop(5000, true); // stop(waitTime, kill if timed out)
	delete this;
	return ret;
}

IClientNetwork& ClientInstance::clientNetwork() const   { if ( m_clientNetwork == 0 ) throw NullPointerException(SOURCELOCATION); return *m_clientNetwork; }
Dispatcher&     ClientInstance::eventDispatcher()       { return m_dispatcher; }
const EventMap& ClientInstance::eventMap()              { return m_dispatcher.eventMap();}

bool ClientInstance::CreateCharacterOnServer( string characterName, CharacterCreateStruct *pCharCreate )    {
	if ( characterName.size() == 0 || characterName.size() > 25 || pCharCreate==NULL )
		return false;

	CREATECHARHEADER_DATA header;
	header.m_category			= CREATE_CHAR_SERVER_CAT;
	header.m_nameLength			= characterName.size();
	header.m_spawnCfg			= pCharCreate->spawnCfg;
	header.m_entityType			= pCharCreate->EDBIndex;
	header.m_attachmentCount	= 0;
	header.m_defCfgCount		= pCharCreate->dim_slots.size();
	header.m_team               = pCharCreate->team;
	header.m_scaleX             = pCharCreate->scaleX;
	header.m_scaleY             = pCharCreate->scaleY;
	header.m_scaleZ             = pCharCreate->scaleZ;

	int		headerSize      = sizeof ( CREATECHARHEADER_DATA );                 // constant size of the header
	int		itemStructSize  = sizeof ( ITEMDATA_STRUCT );                       // size of each attachable item
	int		cfgStructSize   = sizeof( CUSTOMMAT_STRUCT );                       // size of each deformable material

	int		totalSize = (   ( header.m_defCfgCount * cfgStructSize ) 
                            + ( header.m_attachmentCount * itemStructSize ) ) 
                            + headerSize + header.m_nameLength;                 // Calculation for total size of the message

	BYTE	*dataBlock = new BYTE[totalSize];

	CopyMemory ( dataBlock, &header, headerSize );

	for ( int j = 0; j < header.m_nameLength; j++ )
		dataBlock[( j + headerSize )] = characterName[j];

	if ( ( header.m_defCfgCount + header.m_attachmentCount ) > 0 )	{
		ITEMDATA_STRUCT *itemDataStruct = NULL;
		CUSTOMMAT_STRUCT *cfgDataStruct = NULL;
		if(header.m_attachmentCount > 0)
		{
			itemDataStruct = new ITEMDATA_STRUCT[header.m_attachmentCount];
		}
		if(header.m_defCfgCount > 0)
		{
			cfgDataStruct = new CUSTOMMAT_STRUCT[header.m_defCfgCount];
		}

#if 0
		// set data
		int				trace = 0;
		if ( pMovementObj->m_currentlyArmedItems )
		{
			for ( POSITION armedPos = pMovementObj->m_currentlyArmedItems->GetHeadPosition (); armedPos != NULL; )
			{		// update configuration/customized
				CItemObj	*itemPtr = (CItemObj *) pMovementObj->m_currentlyArmedItems->GetNext ( armedPos );
				if ( itemPtr->m_equiped )
				{	// make sure hes wearin it
					itemDataStruct[trace].m_GLID = itemPtr->m_globalID;
					trace++;
				}	// end make sure hes wearin it
			}		// end update configuration/customized
		}
#endif

		int loop = 0;
		for ( BYTE i = 0; i < pCharCreate->dim_slots.size(); i++ )
		{
			cfgDataStruct[loop].m_equipId = pCharCreate->dim_slots[i].glid; 
			cfgDataStruct[loop].m_dim = pCharCreate->dim_slots[i].slot; 
			cfgDataStruct[loop].m_cfg = pCharCreate->dim_slots[i].config;
			cfgDataStruct[loop].m_mat = pCharCreate->dim_slots[i].mat;
			cfgDataStruct[loop].m_R = -1;
			cfgDataStruct[loop].m_G = -1;
			cfgDataStruct[loop].m_B = -1;
			loop++;
		}

		BYTE	*cast = (BYTE *) itemDataStruct;
		BYTE	*cast2 = (BYTE *) cfgDataStruct;

		for ( int i = 0; i < ( ( totalSize - headerSize - (cfgStructSize * header.m_defCfgCount)) - header.m_nameLength ); i++ )
			dataBlock[( headerSize + i + header.m_nameLength )] = cast[i];
		for ( int i = headerSize + (itemStructSize * header.m_attachmentCount) + header.m_nameLength ; i < totalSize ; i++ )
			dataBlock[ i  ] = cast2[i - headerSize - (itemStructSize * header.m_attachmentCount) - header.m_nameLength];

		if ( itemDataStruct )		{
			delete[] itemDataStruct;
			itemDataStruct = 0;
		}
		if ( cfgDataStruct )		{
			delete[] cfgDataStruct;
			cfgDataStruct = 0;
		}
	}

	SendData((BYTE *)dataBlock, totalSize, NSENDFLAG_GUARANTEED, NULL, 60000);

	if (dataBlock)
		delete [] dataBlock;

	return true;
}

// ***********************************
// Static Data and Functions
// ***********************************
static ULONG GetContentVersion( IN LPCTSTR dir )    {
	string path;

	// for client, use current dir, server will pass in the game dir
	if ( dir )
		path = dir;

	path += "\\";
	path += VERSIONINFO_DAT;
	char version[100];
	int len = GetPrivateProfileString( TEXT("CURRENT"), TEXT("VERSION"), "", version, 99, path.c_str() );

	ULONG ver = 0;
	int ints[4];
	int count = sscanf_s( version, "%d.%d.%d.%d", &ints[0], &ints[1], &ints[2], &ints[3] );
	int i,j;
	for ( i = 4-count, j = count-1; i < 4; i++, j-- )
		ver = ver + (ints[j] << (8*i));

	return ver;
}



const ErrorSpec& ClientInstance::getError() {
    return _errorSpec;
}


ClientInstance& ClientInstance::setError( const ErrorSpec& errorSpec ) {
    _errorSpec = errorSpec;
    SwitchClientState(IN_ERROR);
    return *this;
}

ClientInstance& ClientInstance::setWebProxy( WebServicesProxy& webServices ) {
    _webServices = webServices;
    return *this;
}

ClientInstance& ClientInstance::setMailer( MailerPlugin * mailer, jsFastSync * sync ) {
    _mailer = mailer;
	_syncEmailAlerts = sync;
    return *this;
}

ClientInstance& ClientInstance::sendEmailAlert( const ErrorSpec& errorSpec ) {
	jsFastLock lock( *_syncEmailAlerts );

	std::stringstream subject;
	std::stringstream body;

	subject << "SLA Error on " << gs_servername << ":" << gs_port;

	
	const CLIENT_STATE_TIME& state = ( m_curState.state == IN_ERROR ) ? m_prevState : m_curState;
	string sstate = state.stateToString();
	body	<< "SLA Client failure!"								<< endl	
			<< endl
			<< "client state: " << sstate							<< endl
			<< " target host: " << gs_servername << ":" << gs_port	<< endl
			<< "   user name: " << gs_username						<< endl
			<< "       Error: " << errorSpec.first					<< endl
			<< "     Message: " << errorSpec.second					<< endl;

    sendEmailAlert( subject.str(), body.str() );
	return *this;
}

ClientInstance& ClientInstance::sendEmailAlert( string header, string body)
{
	jsFastLock lock( *_syncEmailAlerts );

	static int ii = 0;

	ii++;

    if ( _mailer )
    {
        try
        {
            _mailer->send( header.c_str(), body.c_str() );
        }
        catch(MailException * e)
        {
			LOG4CPLUS_ERROR( Logger::getInstance("SLA.Client"), "Mailer exception raised: " << e->ToString() );
        }
    }

	if (ii > 1)
	{
			LOG4CPLUS_ERROR( Logger::getInstance("SLA.Client"), "BRETT JSLOCK" );
	}
	else
		ii = 0;
	return *this;
}

ClientInstance& ClientInstance::setGameId( unsigned long gameId ) {
    _gameId = gameId;
    return *this;
}

ClientInstance& 
ClientInstance::spawnTo(    const std::string&      worldName
                            , int                   dbIndex
                            , long                  zoneIndex
                            , const std::string&    gameName
                            , const std::string&    url )                   {
    if (gameName.length() > 0 ) gs_gameName = gameName;
    if ( url.length() > 0 )     gs_url      = url;
//    SwitchClientState( SPAWN_STRUCT_RCV );
    SendSpawnMsgToServer( dbIndex, zoneIndex, worldName );

    // Are effectively done with the zone...except that the
    // server has a safety against servicing multiple spawns
    // If we don't undo the safety,  it will won't service the
    // anymore.
    sendZoneDownloadComplete();
    return *this;
}


BEGIN_GETSET_IMPL(ClientInstance, IDS_CLIENTINST_NAME)
	GETSET2(gs_gameName, gsString, GS_FLAG_READ_ONLY | GS_FLAG_INSTANCE_NAME, 0, 0, IDS_CLIENTINST_GAME_NAME, IDS_CLIENTINST_GAME_NAME ) // Yeah, I don't know that user should be able to set this.
	GETSET2(gs_baseDir, gsString, GS_FLAGS_DEFAULT, 0, 0, IDS_CLIENTINST_BASEDIR, IDS_CLIENTINST_BASEDIR )
	GETSET2(gs_username, gsString, GS_FLAG_READ_ONLY, 0, 0, IDS_CLIENTINST_USER, IDS_CLIENTINST_USER )
	GETSET2(gs_servername, gsString, GS_FLAG_READ_ONLY, 0, 0, IDS_CLIENTINST_SERVER, IDS_CLIENTINST_SERVER )
	GETSET2(gs_port, gsUint, GS_FLAG_READ_ONLY, 0, 0, IDS_CLIENTINST_PORT, IDS_CLIENTINST_PORT )
	GETSET2(gs_email, gsString, GS_FLAG_READ_ONLY, 0, 0, IDS_CLIENTINST_EMAIL, IDS_CLIENTINST_EMAIL )
	GETSET2(gs_url, gsString, GS_FLAG_READ_ONLY, 0, 0, IDS_CLIENTINST_URL, IDS_CLIENTINST_URL )
	GETSET2(gs_gameId, gsInt, GS_FLAG_READ_ONLY, 0, 0, IDS_CLIENTINST_GAMEID, IDS_CLIENTINST_GAMEID)
	GETSET2(gs_parentGameId, gsInt, GS_FLAG_READ_ONLY, 0, 0, IDS_CLIENTINST_PARENTGAMEID, IDS_CLIENTINST_PARENTGAMEID )
	GETSET2(gs_scriptName, gsString, GS_FLAG_READ_ONLY, 0, 0, IDS_CLIENTINST_SCRIPTNAME, IDS_CLIENTINST_SCRIPTNAME )
END_GETSET_IMPL

#if 0
// Next step...allow to specify this in scripted/interpretive format.
//
queue( ClientCommand::Ptr(new ConnectCommand()));
queue( ClientCommand::Ptr(new LoginCommand()));
queue( ClientCommand::Ptr(new SelectCharacterCommand()));
queue( ClientCommand::Ptr(new ZoneCommand()));
queue( ClientCommand::Ptr(new SendEntityGUpdateCommand()));         // doesn't actually do anything, since we have nothing to update.
queue( ClientCommand::Ptr(new SleepCommand(10000)));

for ( int n = 0; n < m_zones.size(); n++ ) {
    queue( ClientCommand::Ptr(new GoToRandomURL()));
    queue( ClientCommand::Ptr(new MoveCommand()));
    queue( ClientCommand::Ptr(new SendTextCommand("Help! I\'ve fallen and I can't get up!")));
    queue( ClientCommand::Ptr(new SleepCommand(1000)));
}   

//    queue( ClientCommand::Ptr(new HomeCommand() ) );
//    queue( ClientCommand::Ptr(new SpawnToURLCommand("kaneva://World of Kaneva/BasaltGreen.home")));
//    queue( ClientCommand::Ptr(new SpawnToRandomURL()));
queue( ClientCommand::Ptr(new LogoutCommand()));
queue( ClientCommand::Ptr(new ArrestorCommand()));
#endif





// illustrates event sequences
#if 0
while ( !isTerminated() )	{
    try {
        // Send MOV messages to the server after we are logged in 
        // in order to get events from the guaranteed message cycle

        // This should be a mov event followed by a 1 sec wait idle
        //

        // TODO: This should be scripted via commands.
        //		if ( m_curState.state >= LOGGED_IN )
        //			if ( startMoveTimer.elapsed() > 1000 )              // Send a move message every one second
        //                if ( m_networkDefinedId != -1 )
        //					SendMovMsg( startMoveTimer.reset().elapsed() );

        // Note that this switch statement illustrates the state progression of a client.
        //
        switch(m_curState.state)		{
        case NOT_CONNECTED:		{
            // remove sampling
            ConnectCommand().doCommand(*this);  // this needs to be completely async
            //            if ( this->getState().state != CONNECTED )
            //   			    _sleep(5000); // Couldn't connect.  Sleep 5secs and then retry.
                                }
                                break;
        case CONNECTING:			break;
        case CONNECTED:		{ 
            LoginCommand().doCommand(*this); break;		
                            }
        case LOGGING_IN:			break;
        case LOGGED_IN:		{
            // Should be it's own state?
            if( _ui.characterCreate() )			{
                CreateCharacterOnServer( _ui.name(), _ui.characterCreate() );
                _ui.setCharacterCreate(NULL);  // interesting.  used like a queueing system.
            }

            if ( m_reconnectEvent )			            {
                // Switched servers, no need to send SELECT_CHARACTER again
                // New server will send PendingSpawnEvent back after
                // it gets the GotoPlaceEvent 
                m_dispatcher.QueueEvent( m_reconnectEvent );
                m_reconnectEvent = 0;
                SwitchClientState( ZONING );
                break;
            }
            // NEXTSTEP - break this out to it's own command to see if this unplugs the torrent.
            // if ( SendSelectCharacterAttribMessage() )				{
            SelectCharacterCommand().doCommand(*this);

            doZone();

            // Send entity update command.
            //
            SendEntityGUpdateCommand().doCommand(*this);

            /// NEXT STEP.

            break;
                            }
        case ZONING:			                    break;
        case PENDING_SPAWN_RCV:			            break;
        case PENDING_SPAWN_SND:			            break;
        case SPAWN_STRUCT_RCV:			            break;
        case SPAWN_STRUCT_SND:		

            SpawnSent(  initialZoneAttempt
                , startZoneTimer
                , currentZoneWaitTime
                //                        , startAttachAttribTimer
                //                        , startInvAttribTimer   
                );
            break;
        default:
            LOG4CPLUS_ERROR( Logger::getInstance( LOGGER_NAME ), "Undefined m_curState.state = " << m_curState.state );
        }
        //        IEvent *e = 0; 
        // Put this in it's own thread
        //
        //        Timer start;	
        //		// wait for 1 sec, processing any events that come in while waiting
        //		while ( !isTerminated() && start.elapsed() < 1000 )		{
        //			if ( !m_dispatcher.ProcessNextEvent(100) ) 
        //				break;
        //		}
    }
    catch( const ChainedException& e ) {
        LOG4CPLUS_ERROR( Logger::getInstance("ClientInstance"), e.toString() );
    }
}
#endif
