#ifndef __STATECHANGELISTENER_H__
#define __STATECHANGELISTENER_H__
#include "ClientInstance.h"

//class ClientInstance;
class StateChangeListener {
public:
    virtual void stateChanged( ClientInstance& clientInstance ) = 0;
};
#endif
