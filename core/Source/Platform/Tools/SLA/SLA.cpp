#include "SLA.h"
#include "MetricsListener.h"
#include "Common/KEPUtil/PluginHostConfigurator.h"

//class MetricsEndpoint : public HTTPServer::RequestHandler {
//public:
//};


PluginHost              _pluginHost;

class MyHandler : public HTTPServer::RequestHandler{
public:
    MyHandler() 
      : HTTPServer::RequestHandler("metrics")
    {}

    int handle( const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint ) {
        endPoint 
            <<  (HTTPServer::Response() 
                << Header( "Content-Type", "text/xml;charset=UTF-8" ) 
//                << _clients.getMetricsAgent().toXML() 
                << "OK"
                );
        return 1;
    }
};



SLA::SLA() 
{}

void SLA::InitLogger() {
	static ConfigureAndWatchThread *m_configureThread(0);

	char logDir[_MAX_PATH];
	char baseDir[_MAX_PATH];
	baseDir[0] = '\0';
	_getcwd( baseDir, _MAX_PATH );
	strncpy_s( logDir, _countof(logDir), baseDir, _MAX_PATH );
	strncat_s( baseDir, _countof(baseDir) , "\\SLALog.cfg", _MAX_PATH );

    KEP::LoggingSubsystem logging(baseDir); // ::start(baseDir);
    logging.start("Begin run");
}

SLA& SLA::initialize(int argc, TCHAR* argv[])   {
//    InitLogger();

	if ( argc < 2 )	{
        stringstream ss;
        ss << "WOK Automated Server Load Tool (ASLT)" << endl << endl
						 << "Usage:\n"
						 << "    SLA <configfile> [-u <count> [<offset>]] [-s <scriptname>]\n"
						 << "      where <configfile> is XML configuration file\n"
						 << "            <count> is override count of users to login\n"
						 << "            <offset> is override offset in users file\n"
						 << "            <scriptname> is the name of a script to load and run\n\n"
						 << "      See twiki for full details\n\n";
        fprintf( stderr, ss.str().c_str());
        throw ChainedException( SOURCELOCATION, ss.str()  );
	}



    // check for override of user counts	
    _clients.initialize( argc, argv );

	return *this;	
}

SLA& SLA::run() {
    printf( "Starting....\n" );

    _clients.start(/*this*/);    
    return *this;
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])  {
    try {
        if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))	{
            _tprintf(_T("Fatal Error: MFC initialization failed\n"));
            return 1;
        }
        CoInitializeEx(NULL, COINIT_MULTITHREADED);
        StringResource = ::GetModuleHandle(NULL);

        TIXMLConfig cfg;
//        TIXMLConfigHelper(excfg).load(configFile);
//        KEPConfigAdapter cfg(excfg,false);
        PluginHostConfigurator().configure(PluginHost::singleton(), TIXMLConfigHelper(cfg).load( argv[1] ) );

        HTTPServer* webEmbed = PluginHost::singleton().getInterfaceNoThrow<HTTPServer>("HTTPServer");
        MyHandler handler;
        if ( webEmbed ) {
            // add our endpoint
            webEmbed->registerHandler( handler, "/metrics" );
        }

        SLA::InitLogger();

        for ( int lcv1 = 0
                ; lcv1 < 1
                ; lcv1++ ) {

            // MILESTONE 1: Initialize SLAughter
            //
            SLA sla; //  = new SLA();
            sla.initialize( argc, argv )
               .run();
            ::Sleep(5000);
        }
        return 0;
    }
    catch( const ChainedException& ce ) {
        cout << ce.toString() << endl;
        return ce.errorSpec().first;
    }
    catch(...) {
        cout << "Unknown error!" << endl;
        return 1;
    }
}

