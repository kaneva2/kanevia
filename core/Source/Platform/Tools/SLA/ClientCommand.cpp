#include "ClientCommand.h"

bool ClientCommand::operator==( const ClientCommand& other) const {
    return &other == this;
}

ClientCommand::~ClientCommand() {
}
