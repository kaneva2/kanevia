#include "stdafx.h"
#include "ClientCollection.h"
#include "common/KEPUtil/ExtendedConfig.h"
#include "Common/KEPUtil/SDFHelper.h"
#include "Common/KEPUtil/Timer.h"
#include "ClientInstance.h"
#include "Commands/Commands.h"
#include "MetricsListener.h"

using namespace log4cplus;

ClientCollection::ClientCollection() : _metricsListener(new MetricsListener()), _mailer(NULL), _syncEmailAlerts(NULL)
{}

ClientCollection::~ClientCollection()   {
    for ( ClientMap::iterator iter = clients.begin()
            ; iter != clients.end()
            ; iter ++  ) {
        Client* c = iter->second;
        delete c;
    }
    clients.clear();
}

ClientCollection& 
ClientCollection::initialize( int argc, TCHAR* argv[] )   {

    string configFile = "aslt.xml";
    if ( argc >= 2 ) 
        configFile = argv[1];
    string zones_file;
    int zone_count      = -1; // all
    int zone_offset     = 0; // start from top

    string users_file;
    int user_count      = -1; // all
    int user_offset     = 0; // start from top

    string webcalls_file;
    int webcalls_count = -1;
    int webcalls_offset = 0;

    try	{
        // Change to extended config
        //

        // todo: use applications config file.
        TIXMLConfig excfg;
        TIXMLConfigHelper(excfg).load(configFile);
        KEPConfigAdapter cfg(excfg,false);

        _webServices.setServicesHost(cfg.Get("WebServices/Host", "localhost"));
        _webServices.setServicesPort(cfg.Get("WebServices/Port", 80 ));
        _gameId = cfg.Get("GameId", 0 );

	    _mailer = PluginHost::singleton().getInterfaceNoThrow<MailerPlugin>("Mailer");
        if ( ! _mailer )
        {
            LOG4CPLUS_ERROR( Logger::getInstance("SLA"), "Running without a Mailer");
        }

        zones_file			= cfg.Get("zones/file", "zones.dat");
        zone_count			= cfg.Get("zones/count", -1);
        zone_offset			= cfg.Get("zones/offset",0);

        users_file			= cfg.Get("users/file", "users.dat");
        user_count			= cfg.Get("users/count", -1);
        user_offset			= cfg.Get("users/offset",0);

        webcalls_file       = cfg.Get("web_calls/file", "webcalls.dat");
        webcalls_count      = cfg.Get("web_calls/count", -1);
        webcalls_offset     = cfg.Get("web_calls/offset",0);

        // load list of excluded hosts
        TIXMLConfig::Item excludedHosts = excfg.find( "exclude/hosts");
        StringHelpers::StringVector excludedHostsVec;
        if ( excludedHosts != TIXMLConfig::nullItem() ) {
            for ( TIXMLConfig::Item excludedHost = excludedHosts.firstChild("host")
                    ; excludedHost != TIXMLConfig::nullItem()
                    ; excludedHost = excludedHost.nextSibling() ) {
                const string* s1 = excludedHost.attribute("name");
                if ( s1 != NULL )
                    excludedHostsVec.push_back(*s1);
            }
        }
        if ( excludedHostsVec.size() > 1 ) {
            clientConfig().excludeHosts(excludedHostsVec);
        }


        unsigned long zwms		= cfg.Get("ZoneWaitSec",300)*1000; 
        if ( zwms == 0 )      throw ChainedException(SOURCELOCATION, "Zero config value in file [ZoneWaitSec]." );
        clientConfig().setZoneWaitMS(zwms);

        concurrent_logons   = cfg.Get("session/ConncurrentLogons",1);
        if ( concurrent_logons == 0 ) throw ChainedException(SOURCELOCATION, "Zero config value in file [ConncurrentLogons]." );

        clientConfig().setAttachWaitMS(  cfg.Get("AttachAttribWaitSec",37)*1000 ) 
                      .setInvWaitMS(     cfg.Get("InvAttribWaitSec",37)*1000)         // default to 37 seconds...why 37?
                      .setTextWaitMS(    cfg.Get("TextWaitSec",60)*1000 )           // default to 60 seconds address in client config default constructor
                      .setTextMsg(       cfg.Get("ExtraTextMsg",""))
                      .setTellWaitMS(    cfg.Get("TellWaitSec",60)*1000 )           // default to 60 seconds address in client config default constructor
                      .setExtraTellMsg(  cfg.Get("ExtraTellMsg","") )
                      .setLoginURL(      URL::parse( cfg.Get("LoginURL","") ) )
                      .setAuthURL(       URL::parse( cfg.Get("AuthURL","") ) );
    }
    catch ( KEPException *e )	{

        fprintf( stderr, "Error reading \"%s\": %s\n", configFile, e->ToString().c_str() );
        e->Delete();
        throw ChainedException(SOURCELOCATION, "Error reading " + string(configFile) + ":" + e->ToString() );
    }

    // check for override of user counts	
    for ( int i = 2; i < argc; i++  )	{
        if ( strcmp( argv[i], "-u" ) == 0 && i+1 < argc )		{
            i++;
            user_count = atoi(argv[i]);
            if ( i+1 < argc && isdigit(argv[i+1][0] ) )			{
                i++;
                user_offset = atoi( argv[i] );
            }
        }
        else if ( strcmp( argv[i], "-s" ) == 0 && i+1 < argc )		{
            i++;
            userConfig().setScriptName(argv[i]);
        }
    }

    // Note that as we move these to commands, most of this configuration data goes into the command
    //
    if ( user_offset < 0 ) throw ChainedException(SOURCELOCATION, "Invalid user offset [" + user_offset + string("]") );
    if ( zone_offset < 0 ) throw ChainedException(SOURCELOCATION, "Invalid zone offset [" + zone_offset + string("]") );
    if ( !SDFHelper::readDataFile( users_file, _user_info, user_count, user_offset ) )		            throw ChainedException(SOURCELOCATION, "Error reading data file [" + users_file + "]" );
    if ( !SDFHelper::readDataFile( zones_file, _zones_info, zone_count, zone_offset ) )		            throw ChainedException(SOURCELOCATION, "Error reading data file [" + zones_file + "]" );
    if ( !SDFHelper::readDataFile( webcalls_file, _webcalls_info, webcalls_count, webcalls_offset ) ) 	throw ChainedException(SOURCELOCATION, "Error reading data file [" + webcalls_file + "]" );

    // Until actions are decoupled, go ahead and pass a copy of the webcall store to the default user config
    // TODO: Remove this when actions are decoupled.  Remove webcalls_info from user config.
    _ui._webcalls_info = _webcalls_info;


    // todo.  use toString() methods here.  and then move to another function to isolate the ui operations here.
    //
    fprintf( stderr, "Starting with config values from \"%s\" \n", configFile.c_str() );
    fprintf( stderr, "    zones_file = %s\n"            , zones_file.c_str() );
    fprintf( stderr, "    zone_count = %d\n"            , zone_count );
    fprintf( stderr, "    zone_offset = %d\n"         , zone_offset );
    fprintf( stderr, "    users_file = %s\n"            , users_file.c_str() );
    fprintf( stderr, "    user_count = %d\n"            , user_count );
    fprintf( stderr, "    user_offset = %d\n"         , user_offset );
    fprintf( stderr, "    webcalls_file = %s\n"         , webcalls_file.c_str() );
    fprintf( stderr, "    webcalls_count = %d\n"        , webcalls_count );
    fprintf( stderr, "    webcalls_offset = %d\n"     , webcalls_offset );
    fprintf( stderr, "    concurrent_logons = %d\n"   , concurrent_logons );
    fprintf( stderr, "    zone_wait_ms = %d\n"          , clientConfig().zoneWaitMS() );
    fprintf( stderr, "    attach_wait_ms = %d\n"        , clientConfig().attacheWaitMS() );
    fprintf( stderr, "    inv_wait_ms = %d\n"           , clientConfig().invWaitMS() );
    fprintf( stderr, "    text_msg_wait_ms = %d\n"    , clientConfig().textWaitMS() );
    fprintf( stderr, "    extra_text_msg = %s\n"      , clientConfig().textMsg().c_str() );
    fprintf( stderr, "    tell_msg_wait_ms = %d\n"    , clientConfig().tellWaitMS() );
    fprintf( stderr, "    extra_tell_msg = %s\n"      , clientConfig().extraTellMsg().c_str() );
    fprintf( stderr, "    Login URL = %s\n"             , clientConfig().loginURL().toString().c_str() );
    fprintf( stderr, "    Auth URL = %s\n"            , clientConfig().authURL().toString().c_str() );
    fprintf( stderr, "    Script name = %s\n"         , userConfig().scriptName().c_str() );
    
    if ( !_user_info.size() ) throw ConfigException(SOURCELOCATION, "No users configured.");       //        fprintf( stderr, "No users configured\n" );

    return *this;	
}

CharacterCreateStruct * 
ClientCollection::createCharacter( vector<string>& ui, CharacterCreateStruct * pCharCreate ) 
{
    int EDBIndex    = atoi( ui[USER_PLAYERCREATE_EDBINDEX].c_str() );
    int bodySlots   = atoi( ui[USER_PLAYERCREATE_BODYSLOTS].c_str() );
    int headSlots   = atoi( ui[USER_PLAYERCREATE_HEADSLOTS].c_str() );
    int headGlid    = atoi( ui[USER_PLAYERCREATE_HEADGLID].c_str() );
    int spawnCfg    = atoi( ui[USER_PLAYERCREATE_SPAWNCFG].c_str() );

    if( !(  EDBIndex>0 
            && bodySlots>0 
            && headSlots>0 
            && headGlid>0 
            && spawnCfg>0 )) 
        return pCharCreate;

    char    gender = 'M';
    if( ui[USER_PLAYERCREATE_GENDER].size()==1 )            {
        gender = ui[USER_PLAYERCREATE_GENDER].at(0);
    }


    int team = 2;
    if( ui.size() > USER_PLAYERCREATE_TEAM )                {
        team = atoi( ui[USER_PLAYERCREATE_TEAM].c_str() );
        if( team==0 )						{
            team = 2;
        }
    }

    // initialize body slots
    //
    for( int i=0; i<bodySlots; i++ )
        pCharCreate->dim_slots.push_back( DimConfigStruct(i) );


    // Initialize over head slots
    //
    for( int i=0; i<headSlots; i++ )
        pCharCreate->dim_slots.push_back( DimConfigStruct(i,headGlid) );

    pCharCreate             = new CharacterCreateStruct;
    pCharCreate->gender     = gender;
    pCharCreate->EDBIndex   = EDBIndex;
    pCharCreate->spawnCfg   = spawnCfg;
    pCharCreate->team       = team;
    pCharCreate->scaleX     = 1.0f;
    pCharCreate->scaleY     = 1.0f;
    pCharCreate->scaleZ     = 1.0f;

    if( ui.size() > USER_PLAYERCREATE_SCALEZ )                {
        float scaleX = (float)atof( ui[USER_PLAYERCREATE_SCALEX].c_str() );
        float scaleY = (float)atof( ui[USER_PLAYERCREATE_SCALEY].c_str() );
        float scaleZ = (float)atof( ui[USER_PLAYERCREATE_SCALEZ].c_str() );
        if( scaleX>0 && scaleY>0 && scaleZ>0 )                        {
            pCharCreate->scaleX = scaleX;
            pCharCreate->scaleY = scaleY;
            pCharCreate->scaleZ = scaleZ;
        }
    }
    return pCharCreate;
}

ACTPtr
ClientCollection::start( /* IClientInstanceCallback * callback */ ) {
    printf( "Starting....\n" );

    unsigned int lcv1 = 0;

    CountDownLatch completionLatch( _user_info.size() ) ;

    // Loop through each of the user_info structures.
    //
    while ( lcv1 < _user_info.size()  ) {
        Timer   startTimer(Timer::running );
        ULONG   end = min( lcv1 + concurrent_logons, _user_info.size() );


        // what is the point in this inner loop.  Multiple logins for same account with multiple characters.
        //
        for ( ULONG j = lcv1; j < end; j++ )    {
            CharacterCreateStruct *xpCharCreate = NULL;

            if( _user_info[j].size() > USER_PLAYERCREATE_SPAWNCFG )        {
                xpCharCreate = createCharacter(_user_info[j], xpCharCreate);
            }

            // Start with the base configuration
            //
            UserConfig ui( _ui );
            ui.setName(     _user_info[j][USER_NAME]        )
              .setPassword( _user_info[j][USER_PW]          )
              .setEMail(    _user_info[j][USER_EMAIL]       )
              .setServer(   _user_info[j][USER_SERVER]      )
              .setPort(     atoi(_user_info[j][USER_PORT].c_str() ) )        
              .setTellTrgt( ((_user_info[j].size() > USER_TELL_TARGET  ) ? _user_info[j][USER_TELL_TARGET]  : "" ) )
              .setTellSrce( ((_user_info[j].size() > USER_TELL_SENDER  ) ? _user_info[j][USER_TELL_SENDER]  : "" ) )
              .setStartingURL(_user_info[j].size() > USER_STARTING_URL
                                        ? URL::parse(_user_info[j][USER_STARTING_URL])
                                        : URL() )
              .setGameName( _user_info[j][USER_GAME_NAME] )
              .setCharacterCreate(xpCharCreate);

            // Yeah, don't really like the binding of server to player.
            //
            HostConfig targetHost(  _user_info[j][USER_SERVER]
                                    , atoi((_user_info[j][USER_PORT]).c_str()));   // not clear why port is part of this construct, but we shall see.

            TCHAR curDir[_MAX_PATH];
            _getcwd( curDir, _MAX_PATH );
            string s(curDir);
            clientConfig().setBaseDir( s );

            InitializeClientInstance(   completionLatch
                                        , ui
                                        , targetHost
                                        , _clientConfig           );
        }

        lcv1++;
    }

    completionLatch.await();

    for (   ClientMap::iterator iter = clients.begin()
            ; iter != clients.end()
            ; iter++ ) {
        iter->second->removeStateChangeListener(*_metricsListener);
    }



    // Yep, we're not quite ready for this yet.  Else we wouldn't need the countdown latch
    // Ideally, it would be nice to have an ACTPtr that is aware of CountDownLatch.

    // Step one.  Create a monitor thread that trips this ACT on countdown.
    // step two.  Modify/extend ACT to simply block on a countdown.
    //
    return ACTPtr();
}

bool 
ClientCollection::InitializeClientInstance(	CountDownLatch&         completionLatch
                                            , const UserConfig&     ui
                                            , const HostConfig&     targetHost
                                            , const ClientConfig&   clientConfig   )       {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "InitializeClientInstance()");
            AFX_MANAGE_STATE(AfxGetStaticModuleState());
        StringResource = AfxGetResourceHandle(); 

        Client* client = Client::createInstance(completionLatch, ui,targetHost,clientConfig );
        client->addStateChangeListener( *_metricsListener );
        unsigned long threadId = client->threadId();
        client->setWebProxy(_webServices);
        client->setMailer(_mailer, &_syncEmailAlerts);
        client->setGameId(_gameId);
        seedRandom();
//        client->setZones(_zones_info);
        // Program the client here.
        client->queue( ClientCommand::Ptr(new ConnectCommand()));
        client->queue( ClientCommand::Ptr(new LoginCommand()));
        client->queue( ClientCommand::Ptr(new SelectCharacterCommand()));
        client->queue( ClientCommand::Ptr(new ZoneCommand()));
        client->queue( ClientCommand::Ptr(new SendEntityGUpdateCommand()));         // doesn't actually do anything, since we have nothing to update.
//        client->queue( ClientCommand::Ptr(new SleepCommand( 10000 )));

        for ( size_t n = 0; n < _zones_info.size(); n++ ) {
            unsigned int i = rand() % _zones_info.size();

            client->queue( ClientCommand::Ptr(new GoToURLCommand(getRandomURL())));
//            client->queue( ClientCommand::Ptr(new GoToURLCommand(_zones_info[i][0])));
//            client->queue( ClientCommand::Ptr(new MoveCommand()));
//            client->queue( ClientCommand::Ptr(new SendTextCommand("Help! I\'ve fallen and I can't get up!")));
//            client->queue( ClientCommand::Ptr(new SleepCommand(10000)));
        }   

        //    queue( ClientCommand::Ptr(new HomeCommand() ) );
        //    queue( ClientCommand::Ptr(new SpawnToURLCommand("kaneva://World of Kaneva/BasaltGreen.home")));
        //    queue( ClientCommand::Ptr(new SpawnToRandomURL()));
        client->queue( ClientCommand::Ptr(new LogoutCommand()));
        client->queue( ClientCommand::Ptr(new ArrestorCommand()));

        // Add this to a collection.  We have to do this to insure that we can remove
        // our state change listener.
        clients.insert( make_pair(threadId,client) );
        return true;
}

#if 0
Client* 
ClientCollection::getClient( ULONG threadId )   {
    map<ULONG, Client*>::iterator findThread;
    findThread = clients.find(threadId);

    if ( findThread == clients.end() ) 
        throw ElementNotFoundException(SOURCELOCATION);
    return (Client*)findThread->second;
}

// should be obsolete now
//
ClientCollection& 
    ClientCollection::logPlayerOffServer() {
        for ( vector<ULONG>::iterator it = _threadIds.begin(); it < _threadIds.end(); it++ )	{
            ULONG currentThreadId = *it;
            ClientCollection::LogPlayerOffServer( currentThreadId );
        }
        return *this;
}

ClientCollection&  
    ClientCollection::shutDownClientInstance() {
        for ( vector<ULONG>::iterator it = _threadIds.begin()            ; it < _threadIds.end()            ; it++ )	{
            ULONG currentThreadId = *it;
            ClientCollection::ShutdownClientInstance( *it );
        }
        return *this;
}

ClientCollection& 
    ClientCollection::stopZoning() {
        printf ( "\nStopping...\n" );
        for ( vector<ULONG>::iterator it = _threadIds.begin(); it < _threadIds.end(); it++ )	{
            ULONG currentThreadId = *it;
            ClientCollection::StopZoning( currentThreadId );
        }
        return *this;
}

ClientCollection& 
    ClientCollection::zoneAndActions()  {
        for ( vector<ULONG>::iterator it = _threadIds.begin(); it < _threadIds.end(); it++ )	{
            ULONG currentThreadId = *it;
            LOG4CPLUS_INFO( log4cplus::Logger::getInstance( LOGGER_NAME ), "Calling Start Zoning" );

            // Alert the client's script that it can start zoning.
            // Likely this is obsolete in the future.
            //
            StartZoning( currentThreadId, _zones_info );

            //ClientInstance *currentInstance = getInstance( threadId );
            // Instead here, acquire lightweight wrapper and forward from there.
            // This stuff, really belongs in the scripting side of things.  
            // I.e. each client should be running independently, rather than
            // being controlled by this central loops
            //
            if ( _clientConfig.attacheWaitMS() > 0 )      
                SendAttachAttribMessages(   currentThreadId
                , _clientConfig.attacheWaitMS() );  // Send Attachment Attib Messages if attach wait specified.
            if ( _clientConfig.invWaitMS() > 0 )			
                SendInvAttribMessages(  currentThreadId
                , _clientConfig.invWaitMS() );          // Send Inventory Attrib Messages if inventory wait specified.
            if ( _clientConfig.textWaitMS() > 0 )	
                SendChatMessages(   currentThreadId
                , _clientConfig.textMsg()
                , _clientConfig.textWaitMS() );             // Send Chat message if Chat wait specified.

            // todo: push this logic into client
            if ( _clientConfig.tellWaitMS() > 0 )	
                SendTellMessages(   currentThreadId
                , _clientConfig.extraTellMsg()
                , _clientConfig.tellWaitMS() );             // Send Tell if tell wait specified.   Is this still valid in light of kepchat.
        }
        return *this;
}

bool ClientCollection::ShutdownClientInstance( ULONG threadId ){
    LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "ClientCollection::ShutdownClientInstance()");
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    return getClient( threadId )->shutdown();
}

bool ClientCollection::HasSentPendingSpawn( ULONG threadId )              {
    LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "ClientCollection::HasSentPendingSpawn()");
    return getClient( threadId )->hasSentPendingSpawn();
}

bool 
    ClientCollection::LogPlayerOffServer( ULONG threadId )               {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "ClientCollection::LogPlayerOffServer()");
        Client* currentInstance = getClient( threadId );
        Sleep(100); // don't have all exit at once  // and in a properly architected app, they wouldn't be.
        return currentInstance->logPlayerOffServer(0); // signal it to stop, we'll check again later
}

bool 
    ClientCollection::StartZoning( ULONG threadId, vector<vector<string>> zones )    {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "ClientCollection::StartZoning()");
        return getClient( threadId )->startZoning(zones);
}

UINT 
    ClientCollection::RunningCount( bool onlyCareIfOne )   {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "ClientCoillection::RunningCount()")
            int count = 0;
        for (   ClientMap::iterator i = clients.begin()
            ; i != clients.end()
            ; i++    )	{
                if ( i->second->isRunning() == false ) continue;
                if ( onlyCareIfOne )	return 1;
                else				    count++;
        }
        return count;
}

bool ClientCollection::StopZoning( ULONG threadId ){
    LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "ClientCollection::StopZoning()")
        return getClient( threadId )->stopZoning();
    //        StopZoningTask task;
    //    getClient( threadId )->queueTask(task);
    //    return 0;
}
bool 
    ClientCollection::SendChatMessages( ULONG threadId, string chatMessage, UINT chatWaitTime )    {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "ClientCollection::SendChatMessages()");
        return getClient( threadId )->sendChatMessages(chatMessage, chatWaitTime );
}

bool 
    ClientCollection::SendTellMessages( ULONG threadId, string tellMessage, UINT tellWaitTime ){
        LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "ClientCollection::SendTellMessages()" );
        return getClient( threadId )->sendTellMessages(tellMessage, tellWaitTime );
}

bool 
    ClientCollection::SendAttachAttribMessages( ULONG threadId, UINT attribWaitTime )    {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "ClientCollection::SendAttachAttribMessages()");
        return getClient( threadId )->sendAttachAttribMessages(attribWaitTime);
}

bool 
    ClientCollection::SendInvAttribMessages( ULONG threadId, UINT attribWaitTime ) {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA"), "ClientCollection::SendInvAttribMessages()");
        return getClient( threadId )->sendInvAttribMessages(attribWaitTime);
}
#endif


