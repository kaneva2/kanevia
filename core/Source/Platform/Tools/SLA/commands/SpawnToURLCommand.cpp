#include "stdafx.h"
#include "ClientImpl.h"
#include "Commands/SpawnToURLCommand.h"


// We need to listen to the client.  When it's state turns to SPAWN_STRUCT_SND
// The rezone is considered complete
//

/**
 * Part of the StateChangeListener contract.  Called
 * whenever client being listened changes it's state.
 */
void GoToURLCommand::stateChanged(ClientInstance& client ) {
    if ( client.getState().state == SPAWN_STRUCT_SND )                  
    _latch.countDown();                                             // ISSUE #1: Logs indicate that this gets switched...so how 
                                                                    // does it not get unlatched.  Add the time out and monitor the frequency.
    // The following should prolly be on another listener
    // that simply listens for state changes into and
    // out of SPAWN_STRUCT_SND.  This allows localization of
    // move start and stop logic
    // e.g.:
    // if ( state == SPAWN_STRUCT_SND )
    //        lock.release
    // else
    //        lock.obtain
    //
    // for now simply put the release right here.
    client.startMoving();

}

GoToURLCommand::GoToURLCommand() 
    : _url() 
      , _latch(1)
{}

GoToURLCommand::GoToURLCommand( const std::string& url )
    : _url(url)
    , _latch(1)
{}

GoToURLCommand::GoToURLCommand(const GoToURLCommand& other ) 
    :   _url(other._url)
        , _latch(other._latch)
{}

GoToURLCommand::~GoToURLCommand()
{}

GoToURLCommand& 
GoToURLCommand::operator=( const GoToURLCommand& rhs ) {
    _url = rhs._url;
    return *this;
}

bool 
GoToURLCommand::operator==( const GoToURLCommand& rhs ) {
    if ( this == &rhs ) return true;
    return _url == rhs._url;
}

GoToURLCommand&
GoToURLCommand::doCommand( ClientInstance& instance ){
    try {
        instance.stopMoving();
        instance.addStateChangeListener(*this);
        instance.SpawnPlayerToURL(_url.c_str(),"",0);
        if ( !_latch.await(60000) )   {                                              // ISSUE #1: Specify a timeout value at throw an exception if it never unlatches
            stringstream ss;
            ss  << instance.name()
                << ":GotoURL [" 
                << _url.c_str()
                << "] ISSUE #1: Latch timed out";
            throw ChainedException( SOURCELOCATION
                                    , ss.str() );
        }
        instance.removeStateChangeListener(*this);
    }
    catch( const ChainedException& ce ) {
        instance.removeStateChangeListener(*this);
        throw ce;
    }
    catch( ... ) {
        instance.removeStateChangeListener(*this);
        throw ChainedException(SOURCELOCATION, "Unknown Exception!");
    }
    return *this;
}