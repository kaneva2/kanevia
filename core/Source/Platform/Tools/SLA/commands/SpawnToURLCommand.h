#ifndef __SPAWNTOURLCOMMAND_H__
#define __SPAWNTOURLCOMMAND_H__

#include "ClientCommand.h"
#include "Common/KEPUtil/CountDownLatch.h"
#include "StateChangeListener.h"


class GoToURLCommand : public ClientCommand, StateChangeListener {
public:
    GoToURLCommand();
    GoToURLCommand( const std::string& url );
    GoToURLCommand(const GoToURLCommand& other );
    virtual ~GoToURLCommand();
    GoToURLCommand&  operator=( const GoToURLCommand& rhs );
    bool                operator==( const GoToURLCommand& rhs );
    GoToURLCommand&  doCommand( ClientInstance& instance );

    void stateChanged(ClientInstance& client ); 
private:
    std::string _url;   // This is a kaneva url, which does not conform to IETF standards.  I.e.
                        // we cannot use the URL class.
    CountDownLatch  _latch;
};
#endif