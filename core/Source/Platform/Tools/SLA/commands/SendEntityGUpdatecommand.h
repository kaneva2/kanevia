#ifndef __SENDENTITYGUPDATECOMMAND_H__
#define __SENDENTITYGUPDATECOMMAND_H__

#include "ClientInstance.h"
#include "ClientCommand.h"

class SendEntityGUpdateCommand : public ClientCommand {
public:
    SendEntityGUpdateCommand( ); //{}
    virtual ~SendEntityGUpdateCommand(); // {}

    SendEntityGUpdateCommand& doCommand(ClientInstance& clientInstance ); 
#if 0
    {
        try {
            clientInstance.sendEntityGUpdate();
        }
        catch( const ChainedException& e1 )         {
            ChainedException e2;
            e2.chain(e1);
            throw e2;
        }
        return *this;
    }
#endif
private:
};


#endif