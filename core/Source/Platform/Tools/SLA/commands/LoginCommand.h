#ifndef __LOGINCOMMAND_H__
#define __LOGINCOMMAND_H__

#include "ClientCommand.h"
#include "ClientInstance.h"
#include "Common/KEPUtil/CountDownLatch.h"
#include "StateChangeListener.h"

/**
 * Command that manipulates the client to login to a server (read authenticate)
 * Implements StateChangeListener in order to block while
 * connecting.
 */
class LoginCommand : public ClientCommand, public StateChangeListener {
public:
    LoginCommand();
    virtual ~LoginCommand();
    void stateChanged( ClientInstance& clientInstance );
    LoginCommand& doCommand( ClientInstance& client );

private:
    unsigned long   _timeoutMillis;
    CountDownLatch  _latch;     // When this countdown goes to zero, the command
                                // will unblock.
};
#endif