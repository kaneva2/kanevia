#include "stdafx.h"
#include "SendEntityGUpdatecommand.h"
#include "ClientImpl.h"

SendEntityGUpdateCommand::SendEntityGUpdateCommand()
{}

SendEntityGUpdateCommand::~SendEntityGUpdateCommand() 
{}

SendEntityGUpdateCommand& 
SendEntityGUpdateCommand::doCommand(ClientInstance& clientInstance ) {
    LOG4CPLUS_TRACE( Logger::getInstance("SLA.Client.Command.SendEntityGUpdateCommand"), "SLA.Client.Command.SendEntityGUpdate" );
    try {
            clientInstance.sendEntityGUpdate();
    }
    catch( const ChainedException& e1 )         {
        ChainedException e2(TRACETHROW, SOURCELOCATION, "Error executing SendEntityGUpdateCommand");
        e2.chain(e1);
        throw e2;
    }
    return *this;
}