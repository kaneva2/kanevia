#ifndef __SENDTEXTCOMMAND_H__
#define __SENDTEXTCOMMAND_H__

#include "ClientInstance.h"
#include "ClientCommand.h"

class SendTextCommand : public ClientCommand    {
public:
    SendTextCommand( const std::string& text ) : _text(text), ClientCommand()
    {}

    SendTextCommand() 
        :   ClientCommand()
            , _text()
    {}

    SendTextCommand(const SendTextCommand& other ) 
        :   ClientCommand(other)
            , _text(other._text)    {
    }

    virtual ~SendTextCommand()    {
    }

    SendTextCommand& operator=(const SendTextCommand& rhs ) {
        _text = rhs._text;
        ClientCommand::operator=(rhs);
        return *this;
    }

    bool operator==( const SendTextCommand& rhs ) {
        if ( this == &rhs ) return true;
        return  _text == rhs._text;
    }

    SendTextCommand& 
    doCommand( ClientInstance& instance )     {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA.Client.Command.SendText"), "SLA.Client.Command.SendTextCommand");
        instance.SendTextEvent(_text, ctSystem /* KEP::ChatType::ctSystem */ );
        return *this;
    }
private:
    std::string _text;
};
#endif