#ifndef __ARRESTORCOMMAND_H__
#define __ARRESTORCOMMAND_H__

#include "ClientCommand.h"

class ArrestorCommand : public ClientCommand {
public:
    ArrestorCommand();
    ArrestorCommand( const ArrestorCommand& other );
    virtual ~ArrestorCommand();
    ArrestorCommand& operator=( const ArrestorCommand& rhs );
    bool operator==(const ArrestorCommand& rhs );
    ArrestorCommand& doCommand( ClientInstance& instance );
};


#endif