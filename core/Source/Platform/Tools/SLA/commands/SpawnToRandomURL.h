#ifndef __GoToRANDOM_H__
#define __SPAWNTORANDOM_H__

#include "ClientImpl.h"
#include "ClientCommand.h"

class GoToRandomURL : public ClientCommand {
public:
    GoToRandomURL() : ClientCommand() {}
    GoToRandomURL( const GoToRandomURL& other ) : ClientCommand(other) {}

    virtual ~GoToRandomURL() {}

    GoToRandomURL& operator=( const GoToRandomURL& rhs ) {
        ClientCommand::operator=(rhs);
        return *this;
    }

    bool operator==( const GoToRandomURL& rhs ) {
        return ClientCommand::operator==(rhs);
    }

    GoToRandomURL& doCommand( ClientInstance& instance ) {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA.Client.Command.SpawnURL"),"SLA.Client.Command.SpawnRandomURL::doCommand()");
        // Firing too soon.  Should be tied to 
        instance.goToRandomURL();
        return *this;
    }
};


#endif