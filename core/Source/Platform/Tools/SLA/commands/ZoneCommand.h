#ifndef __ZONECOMMAND_H__
#define __ZONECOMMAND_H__

#include "ClientInstance.h"
#include "ClientCommand.h"

class ZoneCommand : public ClientCommand, public StateChangeListener {
public:
    ZoneCommand() 
        :   ClientCommand()
            , StateChangeListener()
            , _latch(1) 
    {}

    ZoneCommand(const ZoneCommand& other ) 
        :   ClientCommand(other)
            , StateChangeListener(other)
            , _latch(1) 
    {}

    virtual ~ZoneCommand() {}

    ZoneCommand& operator=(const ZoneCommand& rhs ) {
        ClientCommand::operator=(rhs);
        return *this;
    }

    bool operator==( const ZoneCommand& rhs ) {
        return this == &rhs;
    }

    ZoneCommand& 
    doCommand( ClientInstance& instance )     {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA.Client.Command.Zone"), "SLA.Client.Command.ZoneCommand");
//        instance.addStateChangeListener(*this);
        instance.doZone();
//        _latch.await(60000);
//        instance.removeStateChangeListener(*this);
        return *this;
    }

    void
    stateChanged( ClientInstance& clientInstance ) {
        if ( clientInstance.getState().state ==  SPAWN_STRUCT_SND ) 
            _latch.countDown();  // State changed to connection.
    }
private:
    CountDownLatch  _latch;
};
#endif