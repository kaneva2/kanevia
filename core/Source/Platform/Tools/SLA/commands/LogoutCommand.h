#ifndef __LOGOUTCOMMAND_H__
#define __LOGOUTCOMMAND_H__
#include "ClientCommand.h"

class LogoutCommand : public ClientCommand {
public:
    LogoutCommand();
    LogoutCommand( const LogoutCommand& other );
    virtual ~LogoutCommand();
    LogoutCommand& operator=( const LogoutCommand& rhs );
    bool operator==( const LogoutCommand& rhs );
    LogoutCommand& doCommand( ClientInstance& instance );
};
#endif