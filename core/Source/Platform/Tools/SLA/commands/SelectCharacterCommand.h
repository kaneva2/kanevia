#ifndef __SELECTCHARACTERCOMMAND_H__
#define __SELECTCHARACTERCOMMAND_H__


#include "ClientCommand.h"
#include "StateChangeListener.h "
#include "common/KEPUtil/CountDownLatch.h"

/**
 * Command that manipulates the client to login to a server (read authenticate)
 * Implements StateChangeListener in order to block while
 * connecting.
 */
class SelectCharacterCommand : 
    public ClientCommand
    , public StateChangeListener {
public:
    SelectCharacterCommand();
    virtual ~SelectCharacterCommand();
    void stateChanged( ClientInstance& clientInstance );
    SelectCharacterCommand& doCommand( ClientInstance& client ); // {  
private:
    CountDownLatch  _latch;
};

#endif