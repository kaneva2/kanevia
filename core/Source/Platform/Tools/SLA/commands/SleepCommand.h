#ifndef __SLEEPCOMMAND_H__
#define __SLEEPCOMMAND_H__
#include "ClientCommand.h"

class SleepCommand : public ClientCommand {
public:
    SleepCommand( unsigned long timeoutMillis );
    SleepCommand( const SleepCommand& other );
    virtual ~SleepCommand();

    SleepCommand&   operator=( const SleepCommand& rhs );
    bool            operator==( const SleepCommand& rhs );
    SleepCommand&   doCommand( ClientInstance& instance );
private:
    unsigned long _timeoutMillis;
};
#endif
