#include "stdafx.h"
#include "SelectCharacterCommand.h"
#include "ClientImpl.h"

/**
 * Command that manipulates the client to login to a server (read authenticate)
 * Implements StateChangeListener in order to block while
 * connecting.
 */
SelectCharacterCommand::SelectCharacterCommand() : _latch(1)
{}
    
SelectCharacterCommand::~SelectCharacterCommand() 
{}

void 
SelectCharacterCommand::stateChanged( ClientInstance& clientInstance )  {
    if ( clientInstance.getState().state != LOGGED_IN ) return;
    _latch.countDown();  // State changed to connection.
                            // Unblock the calling thread.
}

SelectCharacterCommand& 
SelectCharacterCommand::doCommand( ClientInstance& client ) {  
    LOG4CPLUS_TRACE( Logger::getInstance("SLA.Client.Command.SelectCharacter"), "SelectCharacter" );
    Timer timer(Timer::running);

    // when the receipt goes off the stack the biding is unbound.
//        client.addStateChangeListener(*this);
        client.SendSelectCharacterAttribMessage();

//        client.SwitchClientState( LOGGING_IN ); // Let LoginCommand LogPlayerIntoServer set the state.
//        _latch.await(60000);    // Block until state changes to connected
                                // or 60s.  s/b configurable.  Will throw 
                                // if it times out
//        cout << "SelectCharacter in [" << timer.elapsed() << "]" << endl;
//        client.removeStateChangeListener(*this);
        return *this;  
    }

