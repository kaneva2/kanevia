#ifndef __COMMANDS_H__
#define __COMMANDS_H__
#include "Commands/ConnectCommand.h"
#include "Commands/LoginCommand.h"
#include "Commands/SelectCharacterCommand.h"
#include "Commands/SendEntityGUpdateCommand.h"
#include "Commands/SendTextCommand.h"
#include "Commands/SleepCommand.h"
#include "Commands/ArrestorCommand.h"
#include "Commands/LogoutCommand.h"
#include "Commands/HomeCommand.h"
#include "Commands/SpawnToRandomURL.h"
#include "Commands/SpawnToURLCommand.h"
#include "Commands/ZoneCommand.h"
#include "Commands/Move.h"
#include "Commands/SendText.h"
#endif