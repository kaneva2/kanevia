#include "stdafx.h"
#include "ConnectCommand.h"
#include "ClientImpl.h"

const std::string logPath = "SLA.Client.Command.Connect";

/**
 * Command that manipulates the client to connect to a server.
 * Implements StateChangeListener in order to block while
 * connecting.
 */
ConnectCommand::ConnectCommand() : _latch(1)    
{}

ConnectCommand::~ConnectCommand()     
{}

void
ConnectCommand::stateChanged( ClientInstance& clientInstance ) {
    switch( clientInstance.getState().state ) {
        case  CONNECTED:
        case IN_ERROR:
            _latch.countDown();  // State changed to connection. Unblock the calling thread.
            break;  
        default:
            break;
            // unexpected
    };            
}

ConnectCommand& 
ConnectCommand::doCommand( ClientInstance& client ) {  
    LOG4CPLUS_TRACE( Logger::getInstance(logPath), "SLA.Client.Command.Connect" );

    Timer timer( Timer::running );
    client.addStateChangeListener(*this);

    // Hmmm..seems like we should likely be passing creds in...instead of binding client creds on construction
    //
    bool didConnect = client.Connect();
    if ( !didConnect ) {
        client.removeStateChangeListener(*this);
        client.SwitchClientState(NOT_CONNECTED);
        throw ArrestedException(SOURCELOCATION, "Connection failed!");
    }

    //        client.SwitchClientState( CONNECTING ); // Let LoginCommand LogPlayerIntoServer set the state.
    // or 60s.  s/b configurable.
    if ( !_latch.await(60000) )  {   // Block until state changes to connected
        client.removeStateChangeListener(*this);
        client.SwitchClientState(NOT_CONNECTED);
        throw ArrestedException(SOURCELOCATION, "Timeout while connecting!" );
    }

    if ( client.getState().state == IN_ERROR)  {
        client.removeStateChangeListener(*this);
        throw ArrestedException(SOURCELOCATION, client.getError() );
    }

    LOG4CPLUS_DEBUG( Logger::getInstance(logPath), "Connected in [" << timer.elapsed() << "]" );
    client.removeStateChangeListener(*this);

    return *this;  
}

