#include "stdafx.h"
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include "ArrestorCommand.h"
#include "ClientImpl.h"

using namespace log4cplus;

ArrestorCommand::ArrestorCommand() {}
ArrestorCommand::ArrestorCommand( const ArrestorCommand& other ) : ClientCommand(other) {}

ArrestorCommand::~ArrestorCommand() {    }

ArrestorCommand& 
ArrestorCommand::operator=( const ArrestorCommand& rhs )  {
    ClientCommand::operator=(rhs);
    return *this;
}

bool 
ArrestorCommand::operator==(const ArrestorCommand& rhs ) { return &rhs == this;    }

ArrestorCommand&
ArrestorCommand::doCommand( ClientInstance& instance ) {
    LOG4CPLUS_INFO( Logger::getInstance("SLA.Client.Command.Arrestor"), instance.userName() << " SLA.Client.Command.Arrestor" );
    throw ArrestedException();
}

