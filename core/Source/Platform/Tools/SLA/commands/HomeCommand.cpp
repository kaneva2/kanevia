#include "stdafx.h"
#include "ClientImpl.h"
#include "Commands/HomeCommand.h"

HomeCommand::HomeCommand() 
//    : _url() 
{}

HomeCommand::HomeCommand( const std::string& url )
//    : _url(url)
{}

HomeCommand::HomeCommand(const HomeCommand& other ) 
//    : _url(other._url)
{}

HomeCommand::~HomeCommand()
{}

HomeCommand& 
HomeCommand::operator=( const HomeCommand& rhs ) {
//    _url = rhs._url;
    return *this;
}

bool 
HomeCommand::operator==( const HomeCommand& rhs ) {
    if ( this == &rhs ) return true;
//    return _url == rhs._url;
    return false;
}

HomeCommand&
HomeCommand::doCommand( ClientInstance& instance ){
//    instance.SpawnPlayerToURL(_url.c_str(),"",0);
    instance.goHome();
    return *this;
}