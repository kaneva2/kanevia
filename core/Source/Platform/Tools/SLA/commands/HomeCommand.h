#ifndef __HOMECOMMAND_H__
#define __HOMECOMMAND_H__

#include "ClientCommand.h"

class HomeCommand : public ClientCommand {
public:
    HomeCommand();
    HomeCommand( const std::string& url );
    HomeCommand(const HomeCommand& other );
    virtual ~HomeCommand();
    HomeCommand&  operator=( const HomeCommand& rhs );
    bool          operator==( const HomeCommand& rhs );
    HomeCommand&  doCommand( ClientInstance& instance );
private:
//    std::string _url;   // This is a kaneva url, which does not conform to IETF standards.  I.e.
    // we cannot use the URL class.
};
#endif