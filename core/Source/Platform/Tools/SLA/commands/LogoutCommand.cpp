#include "stdafx.h"
#include "ClientImpl.h"
#include "LogoutCommand.h"

LogoutCommand::LogoutCommand() 
    : ClientCommand() 
{}

LogoutCommand::LogoutCommand( const LogoutCommand& other ) 
    : ClientCommand(other)
{}

LogoutCommand::~LogoutCommand() 
{}

LogoutCommand& 
LogoutCommand::operator=( const LogoutCommand& rhs ) {
    ClientCommand::operator=(rhs);
    return *this;
}

bool 
LogoutCommand::operator==( const LogoutCommand& rhs ) {
    return ClientCommand::operator==(rhs);
}

LogoutCommand& 
LogoutCommand::doCommand( ClientInstance& instance ) {
    LOG4CPLUS_TRACE( Logger::getInstance("SLA.Client.Command.Logout"), "SLA.Client.Command.Logout" );
    instance.LogPlayerOffServer(false);
    return *this;
}

