#if 0
#include "stdafx.h"
#include "SendTextCommand.h"
#include "clientImpl.h"
#include "common\include\ChatType.h"

SendTextCommand::SendTextCommand( string chatMessage, ChatType chatType, int command ) 
    :   _chatMessage(chatMessage)
    , _chatType(chatType) 
    , _command( command )
{}


//////////////////  NOTE THAT WE DON'T SUPPORT _command YET!
SendTextCommand& SendTextCommand::doCommand( ClientInstance& client ) {
    LOG4CPLUS_TRACE( Logger::getInstance("SLA.Client.Command.SendText"), "SendText [" << _chatMessage << "]" );
    client.SendTextEvent(_chatMessage, _chatType, _command );
    return *this;
}
#endif