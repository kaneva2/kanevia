#include "stdafx.h"
#include "SleepCommand.h"
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>

using namespace log4cplus;

SleepCommand::SleepCommand( unsigned long timeoutMillis ) : _timeoutMillis(timeoutMillis)         
{}

SleepCommand::SleepCommand( const SleepCommand& other ) : _timeoutMillis(other._timeoutMillis)    
{}

SleepCommand::~SleepCommand()                                                             
{}

SleepCommand& 
SleepCommand::operator=( const SleepCommand& rhs )                                  
{ _timeoutMillis = rhs._timeoutMillis; return *this;    }

bool 
SleepCommand::operator==( const SleepCommand& rhs )                                          { 
    if ( this == &rhs ) return true; 
    return _timeoutMillis == rhs._timeoutMillis;
}

SleepCommand& 
SleepCommand::doCommand( ClientInstance& ) {
    LOG4CPLUS_INFO( Logger::getInstance("SLA.Client.Command.Sleep"), "Sleep [" << _timeoutMillis << "]" );
    ::Sleep(_timeoutMillis);
    return *this;
}