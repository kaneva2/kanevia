#ifndef __MOVECOMMAND_H__
#define __MOVECOMMAND_H__


#include "ClientInstance.h"
#include "ClientCommand.h"

class MoveCommand : public ClientCommand
{
public:
    MoveCommand() 
        :   ClientCommand()
    {}

    MoveCommand(const MoveCommand& other ) 
        :   ClientCommand(other)
    {}

    virtual ~MoveCommand() 
    {}

    MoveCommand& operator=(const MoveCommand& rhs ) {
        ClientCommand::operator=(rhs);
        return *this;
    }

    bool operator==( const MoveCommand& rhs ) {
        return this == &rhs;
    }

    MoveCommand& 
    doCommand( ClientInstance& instance )     {
        LOG4CPLUS_TRACE( Logger::getInstance("SLA.Client.Command.Zone"), "SLA.Client.Command.MoveCommand");
        instance.SendMovMsg(CTickCount());
        return *this;
    }
};
#endif