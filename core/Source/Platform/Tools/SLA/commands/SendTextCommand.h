#if 0
//#ifndef __SENDTEXTCOMMAND_H__
#define __SENDTEXTCOMMAND_H__

#ifndef __CLIENTCOMMAND_H__
#include "ClientCommand.h"
#endif

#ifndef _CHATTYPE_H_
#include "common\include\ChatType.h"
#endif


class SendTextCommand : public ClientCommand {
public:
    SendTextCommand( string chatMessage, ChatType chatType, int command = 0 );
    SendTextCommand& doCommand( ClientInstance& client ); 
private:
    std::string      _chatMessage;
    ChatType    _chatType;
    int         _command;
};


#endif