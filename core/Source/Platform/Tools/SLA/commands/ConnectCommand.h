#ifndef __CONNECTCOMMAND_H__
#define __CONNECTCOMMAND_H__
#include "ClientCommand.h"
#include "StateChangeListener.h"
#include "common/KEPUtil/CountDownLatch.h"

/**
 * Command that manipulates the client to connect to a server.
 * Implements StateChangeListener in order to block while
 * connecting.
 */
class ConnectCommand : public ClientCommand, public StateChangeListener {
public:
    ConnectCommand();
    virtual ~ConnectCommand();
    void stateChanged( ClientInstance& clientInstance );
    ConnectCommand& doCommand( ClientInstance& client );

private:
    CountDownLatch  _latch;     // When this countdown goes to zero, the command
                                // will unblock.
};

#endif