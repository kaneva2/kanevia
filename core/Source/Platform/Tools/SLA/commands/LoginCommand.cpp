#include "stdafx.h"
#include "tools\jsTools-4.0.1\js.h"
#include "tools\jsTools-4.0.1\jsLock.h"
#include "Common/KEPUtil/Timer.h"
#include "LoginCommand.h"
#include "ClientImpl.h"

const string logPath = "SLA.Client.Command.Login";

// Block until state changes to connected
// or 60s.  s/b configurable.  Will throw 
// if it times out
LoginCommand::LoginCommand() : _latch(1), _timeoutMillis(60000)    
{}
    
LoginCommand::~LoginCommand()     
{}

void 
LoginCommand::stateChanged( ClientInstance& clientInstance ) {

    if ( clientInstance.getState().state == LOGGING_IN )
        return;     // Expected

    if ( clientInstance.getState().state != LOGGED_IN ) {
        LOG4CPLUS_DEBUG( Logger::getInstance(logPath), "Unexpected state");
        return;
    }
    LOG4CPLUS_DEBUG( Logger::getInstance(logPath), "Successful login!");
    _latch.countDown();  // State changed to connection.
                             // Unblock the calling thread.
}

LoginCommand& 
LoginCommand::doCommand( ClientInstance& client ) {  
    LOG4CPLUS_TRACE( Logger::getInstance(logPath), "SLA.Client.Command.Login" );

	Timer timer(Timer::running);
    client.addStateChangeListener(*this);
    client.LogPlayerIntoServer();
    

    if ( _latch.await(_timeoutMillis) ) {
        LOG4CPLUS_DEBUG( Logger::getInstance(logPath),  "Login in [" << timer.elapsed() << "]" );
    }
    else {
        if ( client.getState().state != LOGGED_IN ) {
            client.removeStateChangeListener(*this);
            stringstream ss;
            ss << client.name() << " Login failed [" << timer.elapsed() << "]"; 
            LOG4CPLUS_DEBUG( Logger::getInstance(logPath),  ss.str().c_str() );
            throw ChainedException(TRACETHROW,SOURCELOCATION,ss.str());
        }
    }
    client.removeStateChangeListener(*this);
    return *this;  
}