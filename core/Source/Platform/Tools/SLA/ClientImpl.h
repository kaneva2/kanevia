#ifndef __CLIENTINTERNAL_H__
#define __CLIENTINTERNAL_H__
#include "tools/jsTools-4.0.1/js.h"
#include "tools/jsTools-4.0.1/jsThread.h"

#include "ConfigurationElements.h"
#include "Common/KEPUtil/Timer.h"
#include "Common/include/ChatType.h"
#include "Core/Util/BlockingQueue.h"
#include "Common/KEPUtil/URL.h"
#include "Core/Util/Executor.h"
#include "Common/KEPUtil/CountDownLatch.h"
#include "Common/keputil/errorspec.h"
#include "Event/EventSystem/Dispatcher.h"               // Move to clientinternal.h
#include "Event/EventMap.h"
#include "Event/Events/LogonResponseEvent.h"
#include <GetSet.h>
#include "SLACommon.h"
#include "DPlayHandler.h"
#include "ClientCommand.h"
#include "Plugins/App/WebServicesProxy.h"
#include "StateChangeEventMulticaster.h"

namespace KEP {
    class GotoPlaceEvent;
}




class StateChangeListener;
class MovementEngine;

class ClientInstance : 
    public jsThread
    , public IGame
    , public GetSet             {
public:
    friend class DPlayHandler;
    ClientInstance( CountDownLatch&   completionLatch
                    , const UserConfig&     config
                    , const HostConfig&     host
                    , const ClientConfig&   clientConfig                            );

    ~ClientInstance();


    const ErrorSpec& getError();

    /**
     * Add an object that listens to state change events
     * 
     * This doesn't play nice with things on the stack.
     * 
     */
    ClientInstance& addStateChangeListener(  StateChangeListener&    listener );
    ClientInstance& removeStateChangeListener(  StateChangeListener&    listener );

//    previousState();
//  unsigned long previousStateDuration();
    CLIENT_STATE_TIME previousState() const;

public:

    ClientInstance&     makeWebCalls();
    ClientInstance&     makeWebCall( const URL& url );

    void	SwitchClientState( CLIENT_STATE newState );
    const CLIENT_STATE_TIME& getState();

    bool	Connect();

    /**
     * Send data at data, of size size to NETID
     * @throws NetworkException in the event that send fails
     */
    void	SendData( BYTE * data, DWORD size, DWORD flags, NETID sendTo, DWORD timeout );
    bool	LogPlayerIntoServer();
        IN IDispatcher *disp, 
    static	EVENT_PROC_RC	LogonResponseEventHandler(	IN ULONG lparam, 
        IN IEvent *e );

//    class LogonResponseEvent;
    EVENT_PROC_RC     handleLoginResponseEvent(  LogonResponseEvent* event );

//    EVENT_PROC_RC			HandleLogonResponseEvent(	LPCTSTR connectionErrorCode
//                                                                            , ULONG hResultCode
//                                                                            , ULONG characterCount
//                                                                            , LOGON_RET replyCode  );

    static EVENT_PROC_RC  AutomationEventHandler( IN ULONG lparam, 
        IN IDispatcher *disp, IN IEvent *e );
    short	GetAttribCRC(LPATTRIB_STRUCT attribStruct);
    bool	SendSelectCharacterAttribMessage();
    ClientInstance& sendZoneDownloadComplete();
    ClientInstance& sendEntityGUpdate();
    
    /**
     * Beginning sending move messages to the server
     */
    ClientInstance& startMoving();

    /**
     * Stop sending move messages to the server.
     */
    ClientInstance& stopMoving();


    static	EVENT_PROC_RC PendingSpawnEventHandler(	IN ULONG lparam, 
        IN IDispatcher *disp, 
        IN IEvent *e  );
    short	GetSpawnCRC(LPCTSTR worldName, short dbIndex, short currentTrace );
    bool	SendSpawnMsgToServer( int dbIndex, long zoneIndex, string worldFile );
    bool	HandleAttribMessage( LPATTRIB_STRUCT formattedMessage, NETID idFromLoc );
    static EVENT_PROC_RC	ProcessRenderTextEvent( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
    EVENT_PROC_RC			HandleRenderTextEvent( LPCTSTR msg, ChatType ct );
    bool	LogPlayerOffServer( bool reconnectingToOtherServer = false);
    
    ClientInstance& setError( const ErrorSpec& errorSpec ); 
    
    static EVENT_PROC_RC	GotoPlaceHandler(	IN ULONG lparam, 
        IN IDispatcher *disp, 
        IN IEvent *e );

    static EVENT_PROC_RC  GotoPlaceFailureHandler(   ULONG           lparam
                                                                            , IN IDispatcher *disp
                                                                            , IN IEvent *e );


    float	GetMovMsgCrC(LPSHRUNK_TOSVRMOV_STRUCT packet);
    LPSHRUNK_TOSVRMOV_STRUCT ShrinkDataCP(LPMOVE_STRUCT OLD_DATA, LPSHRUNK_TOSVRMOV_STRUCT SHRUNK_DATA);
    bool	SendMovMsg( ULONG currentTimeDiff );
    static EVENT_PROC_RC ProcessObjUrlEvent( IN ULONG lparam, IN IDispatcher *disp, IN IEvent *e );
    EVENT_PROC_RC HandleObjUrlEvent();
    void	SetInitialZoneTime();
    bool	SpawnPlayerToZone( ULONG zi, ULONG instanceId );
//    bool	SpawnPlayerToURL( const char *stpURL, const char *server, const char *port );
    bool	SpawnPlayerToURL( const char *stpURL, const char *server, unsigned short port );

    ClientInstance& goHome();

    bool	SendDDRStartGameEvent( int objectId );
    bool	SendDDRRecordScoreEvent( int objectId );
    bool	SendDDRGameEndEvent( int objectId );

    bool	SendTextEvent(      string      chatMessage
                                , ChatType  ct
                                , int       command = 0 );

    bool	SendAttribMessage(  DWORD       flags
                                , NETID     sendTo
                                , int       attribute
                                , int       uniInt
                                , long      referenceID
                                , int       uniInt2
                                , short     uniShort3 );

    // TODO: Obsolesce this method using URL class with logicals
    bool	FillInLoginURL( INOUT string &s, IN string emailaddress );

    // TODO: Obsolesce this method using URL class with logicals
    bool	FillInAuthURL( INOUT string &s, IN string userName, IN string password );

    bool	GetBrowserPage( string url );

    bool	CreateCharacterOnServer( string characterName, CharacterCreateStruct *pCharCreate );

    bool CheckChat(string textReceived);
    bool CheckTell(string textReceived);

    ClientInstance& spawnTo(    const std::string&      worldName
                                , int                   dbIndex
                                , long                  zoneIndex
                                , const std::string&    gameName
                                , const std::string&    url );
    void CleanUpEvents();

    ULONG				_doWork();

    // Gotta get rid of these params before we can move it to a command.
    //
    void SpawnSent(     bool &initialZoneAttempt
                        , Timer &startZoneTimer
                        , ULONG &currentZoneWaitTime
//                        , Timer &startAttachAttribTimer
//                        , Timer &startInvAttribTimer 
                        );

    void                goToRandomURL();
    void                doZone();
    bool				Start();
    jsThread::StopRet	Shutdown();

	void				SetSessionProtocolVersion( ULONG ver ) { m_sessionProtocolVersion = ver; }

//    virtual NETRET		Connected( IN LOGON_RET ret, IN NETRET result );
//    virtual NETRET		Disconnected( IN kstring readon, IN HRESULT hr, IN BOOL report, IN LOGON_RET replyCode );
//    virtual bool		    DataFromServer( IN NETID from, IN const BYTE * data, IN ULONG len );

    ULONG QueueEvent( IN IEvent *e, IN ULONG secsInFuture = 0, IN bool inMilliSec = false ) 
    { return m_dispatcher.QueueEvent( e, secsInFuture, inMilliSec ); }

    // IGame/IKEPObject override
    IGetSet *Values() { return this; }

    CLIENT_STATE_TIME	m_prevState;
    CLIENT_STATE_TIME	m_curState;

    vector<vector<string>>	m_zones;
    bool					m_startZoning;
    bool					m_stopZoning;
    ULONG					m_initialZoneWaitTime;
    ULONG					m_chatWaitTime;
    string					m_chatMessage;
    ULONG					m_tellWaitTime;
    string					m_tellMessage;
    ULONG					m_attachAttribWaitTime;
    ULONG					m_invAttribWaitTime;

    IClientNetwork&         clientNetwork() const;
    Dispatcher&             eventDispatcher();
    const EventMap&         eventMap();
    ClientInstance&         setWebProxy( WebServicesProxy& webServices );
    ClientInstance&         setMailer( MailerPlugin * mailer, jsFastSync * sync );
    ClientInstance&         setGameId( unsigned long gameId );
    ClientInstance&         queueCommand( jsRefCountPtr<ClientCommand> cmd );


    /**
     * Starts a loop that processes inbound events from the EventDispatcher.
     * To be called from another thread.
     */
    void serviceEvents();

    ClientInstance& startEventProcessing();
    ClientInstance& stopEventProcessing();

    const std::string&  userName() {
        return gs_username;
    }

	ClientInstance& sendEmailAlert(const ErrorSpec& errorSpec );
	ClientInstance& sendEmailAlert(string header, string body);


protected:

    DPlayHandler        _dplayHandler;
    IClientNetwork	   *m_clientNetwork;
    Dispatcher			m_dispatcher;


    // getset vars
    // Must be initialized
    string				gs_baseDir;
    string				gs_username;
    //	string				gs_password;
    string				gs_servername;                  // are updated whenever we go to a new zone.
    UINT				gs_port;
    string				gs_email;
    string				gs_url;		// current url
    int					gs_gameId;
    int					gs_parentGameId;
    string              gs_gameName;
    string              gs_scriptName;

    KEP::GotoPlaceEvent*     m_reconnectEvent;
    short				m_networkDefinedId;

    bool				m_gotServerResponse;
    Timer               _messageSentTimer;
    Timer               _tellSentTimer;
    Timer               _zoneTimer;
    ULONG				xm_zoneSentTime;
    bool				m_loggedOutZoneTime;
	ULONG				m_sessionProtocolVersion;

    // for scripting support
    BladeFactory 		m_eventBladeFactory;
    EventBladeVector	m_eventBlades;
    Logger				m_logger;

    // Note that this macro leaves the us in access class protected.
    DECLARE_GETSET



private:
    ClientInstance&     registerEvents();
    ClientInstance&     registerEventHandlers();
    WebServicesProxy     _webServices;              // An agent to handle calls to web services as needed.
    unsigned long       _gameId;
    MailerPlugin *      _mailer;

    MovementEngine*     _movementEngine;
    jsThreadExecutor    _eventServiceThread;        // An executor that will run logic to service inbound events.
    bool                _loopExited;                // Due to short comings and incompatibilities between both
                                                    // jsThread and KEPUtil::jsThreadExecutor need to flag
                                                    // that the main loop has exited.


    UserConfig                          _userInfo;
    ClientConfig                        _clientConfig;
    vector<Timer>                       webcallTimes;

    // 
    BlockingQueue<ClientCommand::Ptr>   _input;
    ErrorSpec                           _errorSpec;
    StateChangeEventMulticaster         _stateChangeMC;
    CountDownLatch&                     _completionLatch;
	jsFastSync *						_syncEmailAlerts;
};
#endif