#ifndef __STATECHANGEMULTICASTER_H__
#define __STATECHANGEMULTICASTER_H__

#include "StateChangeListener.h"

class ClientInstance;
class StateChangeEventMulticaster {
public:
    virtual ~StateChangeEventMulticaster();

    StateChangeEventMulticaster& addStateChangeListener(  StateChangeListener&    listener );
    StateChangeEventMulticaster& removeStateChangeListener(  StateChangeListener&    listener );
    StateChangeEventMulticaster& fireStateChanged(ClientInstance&);
private:
    template <typename KeyT, typename CollectedT >
    class CollectionBinding {
    public:
        typedef CollectionBinding<KeyT,CollectedT>      BindingT;
        typedef std::map<KeyT,CollectedT>               MapT;

        CollectionBinding( KeyT key, MapT& collection )
            :   _key(key)
            , _collection(collection)
            , _relaxed(false)
            , _bound(true)
        {}

        virtual ~CollectionBinding() {
            if ( _bound & !_relaxed ) unbind();
        }
        typedef jsRefCountPtr< BindingT > Ptr;

        BindingT& relax() {
            _relaxed = true;
            return *this;
        }

        BindingT& unbind() {
            if ( !_bound ) return *this;
            MapT::iterator iter = _collection.find(_key);
            if ( iter == _collection.end() ) return *this;
            _collection.erase(iter);
            _bound = false;
            return *this;
        }

    private:
        MapT&                           _collection;
        KeyT                            _key;
        bool                            _relaxed;
        bool                            _bound;
    };




    StateChangeEventMulticaster&        finalizeRemoves();


    typedef std::pair<StateChangeListener*,StackTrace*>                     SCLVectorPairT;
    typedef std::map<StateChangeListener*,StackTrace*>                      SCLVectorsT;
    typedef std::map<StateChangeListener*,StateChangeListener*>             SCLCollectionT;
    typedef CollectionBinding<StateChangeListener*,StateChangeListener*>    SCLBindingT;

    SCLCollectionT                      _stateChangeListeners;

    // we can leave this here, but after we track down our access violation
    // it holds no value for us.
    SCLVectorsT                         _stateChangeVectors;
    vector<StateChangeListener*>        _pending_removes;
    jsFastSync                          _removeSync;
    jsFastSync                          _listenerSync;
};
#endif