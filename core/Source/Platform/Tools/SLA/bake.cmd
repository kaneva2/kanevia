@rem bak <recipe> <configuration>

call package_%2.cmd %2

@rem Skip file copy if destination folder does not exist
@if not exist \package echo *** %0: Package folder does not exist. Receipts not copied && goto :eof

copy recipes\%1\* \package\sla.%2
