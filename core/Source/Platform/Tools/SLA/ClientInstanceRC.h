//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ClientInstance.rc
//
#define IDS_CLIENTINST_NAME             101
#define IDS_CLIENTINST_GAME_NAME        102
#define IDS_APP_TITLE                   103
#define IDS_CLIENTINST_BASEDIR          103
#define IDS_CLIENTINST_USER             104
#define IDS_CLIENTINST_SERVER           105
#define IDS_CLIENTINST_PORT             106
#define IDS_CLIENTINST_EMAIL            107
#define IDS_CLIENTINST_URL              108
#define IDS_CLIENTINST_GAMEID           109
#define IDS_CLIENTINST_PARENTGAMEID     110
#define IDS_CLIENTINST_SCRIPTNAME       111

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
