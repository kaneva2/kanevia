#ifndef __DPLAYHANDLER_H__
#define __DPLAYHANDLER_H__

#include "Common/include/CoreStatic/CStructuresMisl.h"  // Move to dplayhandler.h
#include "tools/Network/INetwork.h"

class ClientInstance;
class DPlayHandler 
    : public KEP::IClientNetworkCallback {
public:
    DPlayHandler(ClientInstance* subject);
    virtual ~DPlayHandler();
	NETRET Connected(IN LOGON_RET replyCode, IN NETRET result, IN ULONG sessionProtocolVersion) override;
	NETRET Disconnected(IN kstring reason, IN HRESULT hr, IN BOOL report, IN LOGON_RET replyCode, IN bool connecting) override;
	bool DataFromServer(IN NETID from, IN const BYTE *data, IN ULONG len) override;

    bool handleDefault( LPEXMSG_CATEGORY identityMsg, ULONG len, const BYTE * data, NETID from ) ;
#if 0
    {
        if ( (BYTE)identityMsg->m_category >= MIN_EVENT_BYTE && len >= sizeof( EventHeader ) )
        {
            IEvent *e = _subject->eventDispatcher().MakeEvent( data, len, from, ESEC_SERVER );
            if ( e )
            {
                _subject->eventDispatcher().QueueEvent( e );
                _subject->wakeUp();
            }
        }
        else
        {
            stringstream oss;
            string category("");
            oss << category << (int)identityMsg->m_category;
            category = oss.str();
            LOG4CPLUS_TRACE( Logger::getInstance( LOGGER_NAME ), "Unhandled message from server category: " << category );
        }

        return true;
    }
#endif
    bool gupdateToClient( const BYTE * data, NETID from ) ;
#if 0
    {
        {
            BYTE *blockCast = (BYTE *) data;
            ENTITYGUPDATE_HEADER * formattedMessage = (ENTITYGUPDATE_HEADER *) data;
            int	dataTracer = sizeof ( ENTITYGUPDATE_HEADER );

            if ( formattedMessage->m_attributeMsgCount > 0 )
            {			
                int byteSize = sizeof ( ATTRIB_STRUCT ) *formattedMessage->m_attributeMsgCount;
                ATTRIB_STRUCT *attribArray = new ATTRIB_STRUCT[formattedMessage->m_attributeMsgCount];
                BYTE *atCast = (BYTE *)attribArray;

                for ( int i = 0; i < byteSize; i++ )
                {
                    atCast[i] = blockCast[dataTracer];
                    dataTracer++;
                }

                for ( int atLoop = 0; atLoop < formattedMessage->m_attributeMsgCount; atLoop++ )
                {
                    LPATTRIB_STRUCT attribMessage = &attribArray[atLoop];
                    _subject->HandleAttribMessage( attribMessage, from );
                    //LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), "Entity Update Attribute : " << (int)attribMessage->attribute );
                }

                delete[] attribArray;
                attribArray = 0;
            }	

            if ( formattedMessage->m_equipMsgCount > 0 )
            {
                int				byteSize = sizeof ( EQUIP_STRUCT ) * formattedMessage->m_equipMsgCount;
                EQUIP_STRUCT	*equipArray = new EQUIP_STRUCT[formattedMessage->m_equipMsgCount];
                BYTE			*atCast = (BYTE *) equipArray;
                for ( int i = 0; i < byteSize; i++ )
                {
                    atCast[i] = blockCast[dataTracer];
                    dataTracer++;
                }

                delete[] equipArray;
                equipArray = 0;
            }

            if ( formattedMessage->m_txtMsgCount > 0 )
            {
                for ( int txtLoop = 0; txtLoop < formattedMessage->m_txtMsgCount; txtLoop++ )
                {		// txt msg loop
                    int txtMsgSz = blockCast[dataTracer];
                    dataTracer++;
                    if ( txtMsgSz > 0 )
                    {	// valid sz
                        ChatType ct = *((ChatType*)(&blockCast[dataTracer]));
                        dataTracer+=4;
                        TCHAR	*tempBlock = new TCHAR[txtMsgSz-4];
                        for ( int i = 0; i < txtMsgSz; i++ )
                        {
                            tempBlock[i] = blockCast[dataTracer];
                            dataTracer++;
                        }

                        _subject->HandleRenderTextEvent( tempBlock, ct );
                        delete[] tempBlock;
                        tempBlock = 0;
                    }	// end valid sz
                }		// end txt msg loop
            }

            if ( formattedMessage->m_updateStatMsgCount > 0 )
            {
                int statBlockSize = blockCast[dataTracer];
                dataTracer++;
                if ( statBlockSize > 0 )
                {
                    BYTE	*statUpdateBlock = new BYTE[statBlockSize];
                    for ( int i = 0; i < statBlockSize; i++ )
                    {
                        statUpdateBlock[i] = blockCast[dataTracer];
                        dataTracer++;
                    }
                    delete[] statUpdateBlock;
                    statUpdateBlock = 0;
                }
            }

            if ( formattedMessage->m_miscMsgCount > 0 )
            {	
                for ( int miscLoop = 0; miscLoop < formattedMessage->m_miscMsgCount; miscLoop++ )
                {
                    BYTE	castByteTemp[2];
                    castByteTemp[0] = blockCast[dataTracer];
                    dataTracer++;
                    castByteTemp[1] = blockCast[dataTracer];
                    dataTracer++;

                    short	miscBlockSize = 0;
                    CopyMemory ( &miscBlockSize, castByteTemp, sizeof ( short ) );

                    // int miscBlockSize = blockCast[dataTracer];
                    // dataTracer++;
                    if ( miscBlockSize > 0 )
                    {	// 1
                        BYTE	*miscUpdateBlock = new BYTE[miscBlockSize];
                        for ( int i = 0; i < miscBlockSize; i++ )
                        {
                            miscUpdateBlock[i] = blockCast[dataTracer];
                            dataTracer++;
                        }

                        //HandleMiscBlocksClientSide ( miscUpdateBlock, miscBlockSize );
                        if ( miscUpdateBlock )
                        {
                            delete[] miscUpdateBlock;
                        }

                        miscUpdateBlock = 0;
                    }	// end 1
                }
            }

            if ( formattedMessage->m_eventCount > 0 )
            {
                for ( int eventLoop = 0; eventLoop < formattedMessage->m_eventCount; eventLoop++ )
                {
                    BYTE	castByteTemp[2];
                    castByteTemp[0] = blockCast[dataTracer];
                    dataTracer++;
                    castByteTemp[1] = blockCast[dataTracer];
                    dataTracer++;

                    short	miscBlockSize = 0;
                    CopyMemory ( &miscBlockSize, castByteTemp, sizeof ( short ) );

                    // int miscBlockSize = blockCast[dataTracer];
                    // dataTracer++;
                    if ( miscBlockSize > 0 )
                    {
                        try
                        {
                            IEvent *e = _subject->eventDispatcher().MakeEvent( &blockCast[dataTracer], miscBlockSize, from );

                            if ( e )
                            {
                                if ( e->Id() > 0 ) // never allow client to send <= 0
                                    _subject->eventDispatcher().ProcessOneEvent(e);
                                else
                                    e->Delete();
                            }
                        }
                        catch ( KEPException *e )
                        {
                            LOG4CPLUS_ERROR(  Logger::getInstance( LOGGER_NAME ), "Event Exception Error: " << (ULONG)*(EVENT_ID*)data << " " << e->m_msg );
                            e->Delete();
                        }
                    }
                    dataTracer += miscBlockSize;
                }
            }

            return true;
        }
    }
#endif
    bool fromServerSpawnStruct( const BYTE * data, ULONG len ) ;


#if 0
    {
        LPFROM_SERVER_SPAWN_STRUCT	formattedMessage = ( LPFROM_SERVER_SPAWN_STRUCT	) data;

        string worldName = formattedMessage->worldName;
        if (len > sizeof(FROM_SERVER_SPAWN_STRUCT) + worldName.size() )
        {
            _subject->gs_url = (formattedMessage->worldName + worldName.size() + 1 );
            string gameName;
            StpUrl::ExtractGameName(_subject->gs_url, gameName );
            _subject->gs_gameName = gameName;
        }

        _subject->SwitchClientState( SPAWN_STRUCT_RCV );
        _subject->SendSpawnMsgToServer( formattedMessage->dbIndex, formattedMessage->zoneIndex, formattedMessage->worldName );

        //delete formattedMessage;
        //formattedMessage = 0;
        return true;
    }
#endif
private:
    ClientInstance* _subject;
};
#endif