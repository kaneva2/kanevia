#pragma once

#include "stdafx.h"
#include "ClientCollection.h"
#include "Common/KEPUtil/Plugin.h"
#include "Common/KEPUTIL/HTTPServer.h"
#include "Common/KEPUTIL/Mailer.h"

class SLA // : public HTTPServer::RequestHandler
{
public:
	SLA();

    SLA&    initialize( int argc, TCHAR* argv[] );
    SLA&    run(); 
    //virtual int handle( const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint );
    static void InitLogger();

private:
    ClientCollection        _clients;
    TIXMLConfig             _config;
//    PluginHost              _pluginHost;
};