@rem Skip if environment variables are not defined
@if %KANEVA_DEV_HOME%.==. echo *** %0: Environment variable `KANEVA_DEV_HOME' not defined && goto :eof
@if %KD_CONTEXT%.==.      echo *** %0: Environment variable `KD_CONTEXT' not defined      && goto :eof
@if %KD_PROJECT_ROOT%.==. echo *** %0: Environment variable `KD_PROJECT_ROOT' not defined && goto :eof
@if %KD_SOURCE_ROOT%.==.  echo *** %0: Environment variable `KD_SOURCE_ROOT' not defined  && goto :eof
@if %KD_SOURCE_DIR%.==.   echo *** %0: Environment variable `KD_SOURCE_DIR' not defined   && goto :eof
@if not exist \package    echo *** %0: Package folder does not exist. Binaries not copied && goto :eof

copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\%KD_SOURCE_ROOT%\%KD_SOURCE_DIR%\%1\SLA.exe               \package\SLA.%1
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\%KD_SOURCE_ROOT%\%KD_SOURCE_DIR%\%1\SLA.pdb               \package\SLA.%1
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\%KD_SOURCE_ROOT%\%KD_SOURCE_DIR%\Client.%1\SLAClient.dll  \package\SLA.%1 
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\%KD_SOURCE_ROOT%\%KD_SOURCE_DIR%\Client.%1\SLAClient.pdb  \package\SLA.%1 
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\log4cplus%2.dll                  \package\SLA.%1 
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\keputil%2.dll                    \package\SLA.%1 
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\keputil%2.pdb                    \package\SLA.%1

copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\CorePluginLibrary.dll            \package\SLA.%1 
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\CorePluginLibrary.pdb            \package\SLA.%1
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\mysqlpp_d.dll                    \package\SLA.%1
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\mysqlpp.dll                    \package\SLA.%1
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\libmysql.dll                    \package\SLA.%1


copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\CrashRpt1402%2.dll               \package\SLA.%1
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bind\vld_x86.dll                       \package\SLA.%1
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\CorePluginLibrary.dll            \package\SLA.%1
copy %KANEVA_DEV_HOME%\%KD_CONTEXT%\%KD_PROJECT_ROOT%\deploy\gamecontent\bin%2\CorePluginLibrary.pdb            \package\SLA.%1
