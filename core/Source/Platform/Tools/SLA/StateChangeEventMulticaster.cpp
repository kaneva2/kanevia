#include "StateChangeEventMulticaster.h"

StateChangeEventMulticaster::~StateChangeEventMulticaster() {
//    jsFastLock lock(_StateChangeSync);
//    _stateChangeVectors.insert( SCLVectorPairT(&listener,new StackTrace()) );
//        typedef std::map<StateChangeListener*,StackTrace*>                      SCLVectorsT;
    for ( SCLVectorsT::iterator i = _stateChangeVectors.begin()
            ; i != _stateChangeVectors.end()
            ; i++ ) {
        delete i->second;
    }
    _stateChangeVectors.clear();
}


// This has become a bit larger than it's original inception.  Consider
// extracting this out into an event multicaster
//
StateChangeEventMulticaster&
StateChangeEventMulticaster::fireStateChanged(ClientInstance& instance) {
    jsFastLock lock( _listenerSync );
    finalizeRemoves();
    for ( SCLCollectionT::iterator iter = _stateChangeListeners.begin()
        ; iter != _stateChangeListeners.end()
        ; iter++ ) {
            StateChangeListener* listener = iter->second;
            listener->stateChanged(instance);           // ISSUE #2 This listener has already been released...
            //    * There are no pending removes.  Either it never calls removeListener, or it was
            //      freed before it could.  In the former case, simply find the unmatched addListener().
            //      In the former, seems that this could potential have manifested even without this listener mech.

            // 1. The former scenario exposed an unmatched add...however it's not clear that it fixes that
            //    scenario, as it the listener would never be freed until after the clients are done running
            //    i.e. after all state changes are complete.
            //
            // 2. Confirmed.  Former scenario is not the issue. 
            //      The bad listener is in _stateChangeListeners.  perhaps we change the key, say to a string.
            //      Fair amount of work.  But should expose the issue.  Ooooh...we can keep a separate
            //      collection and store a stacktrace!!!! That tells us where the listener is added but not
            //      not where the listener is freed.  But puts me much closer to finding the issue.
            //
            //  Bingo....got it!  I've been pushing ref ptrs...unfortunately I can't push ref pointers
            // into this reference list because jsRefCountPtr<Command> != jsRefCountPtr<Listener>.  At least
            // not as far as the compiler is concerned and unbound ref count ptrs is equally distructive.
            // So _doWork cannot actually pop the command.
            //      option 1.  break out the listener from the command.  perhaps, but how can the command
            //                      be cleaned up.  Command could be deleted before it's even complete 
            //                      execution.  Because the wait times out and doesn't get removed?
            //      option 2.  don't pop the command. -- doesn't work do to the looping mechanism
            //                      already being started.  Also precludes dynamic queueing of events as the queue
            //                      will continue to grow.
            //
    }
    finalizeRemoves();
    return *this;
}

StateChangeEventMulticaster&
StateChangeEventMulticaster::addStateChangeListener( StateChangeListener& listener ) {   
    jsFastLock lock( _listenerSync);
    _stateChangeVectors.insert( SCLVectorPairT(&listener,new StackTrace()) );
    _stateChangeListeners.insert( pair<StateChangeListener*,StateChangeListener*>(&listener,&listener) ); 
    return *this;
}

StateChangeEventMulticaster&
StateChangeEventMulticaster::removeStateChangeListener( StateChangeListener& listener ) {   
    jsFastLock lock( _removeSync);

    // Note that we can only schedule the removes.   We have no guarantees
    // that the listener isn't removing itself during iteration
    //
    _pending_removes.push_back(&listener);

    //    SCLCollectionT::iterator i = _stateChangeListeners.find(&listener);
    //    if ( i != _stateChangeListeners.end() ) _stateChangeListeners.erase(i);
    return *this;
}

StateChangeEventMulticaster&
StateChangeEventMulticaster::finalizeRemoves() {
    jsFastLock lock( _removeSync);
    for ( vector<StateChangeListener*>::iterator iter = _pending_removes.begin()
        ; iter != _pending_removes.end()
        ; iter++ ) {
            SCLCollectionT::iterator i = _stateChangeListeners.find(*iter);
            if ( i != _stateChangeListeners.end() ) _stateChangeListeners.erase(i);
    }
    _pending_removes.clear();       // Okay, a new problem
    // ISSUE #3 Expression: vector interators incompatible
    return *this;
}
