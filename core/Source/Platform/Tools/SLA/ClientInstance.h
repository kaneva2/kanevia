/******************************************************************************
 ClientInstance.h

 Copyright (c) 2004-2009 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#ifndef __CLIENTINSTANCE_H__
#define __CLIENTINSTANCE_H__

#ifdef BUILD_SERVERLOAD
#define DLLIMPORT __declspec( dllexport ) 
#else
#define DLLIMPORT __declspec( dllimport )
#endif

#include "stdafx.h"
#include "ClientInstanceRC.h"
#include <direct.h>

#include "KEPHelpers.h"
#include "INetwork.h"
#include "common/keputil/URL.h"
#include "common/KEPUtil/CountDownLatch.h"
#include "Event\EventSystem\Dispatcher.h"
#include "Event/Base/ISendHandler.h"
#include <CStructuresMisl.h>
#include "event\events\LogonResponseEvent.h"
#include "event\events\PendingSpawnEvent.h"
#include "event\events\ClientExitEvent.h"
#include "Event\EventSystem\EventSyncEventHandler.h"
#include "event\events\EventSyncEvent.h"
#include "Plugins/App/WebServicesProxy.h"
#include "Common/KEPUTIL/Mailer.h"
#include <vector>

#include "ConfigurationElements.h"
#include "ClientCommand.h"
//#include "StateChangeListener.h"

class IClientInstanceCallback
{
public:
	virtual void LogMessage( IN LPCTSTR msg )
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState());
	}
};


/**
 * Class comprised of 0 or more client instance threads.
 * Provides logic that applies to all clients.
 */
class StateChangeListener;
class ClientInstance; // *currentInstance = getInstance( threadId );
//class SLATask;
class Client {
public:
    Client( ClientInstance* impl ) : _impl(impl) {}
    virtual ~Client();

    bool shutdown();
    bool hasSentPendingSpawn();
    bool logPlayerOffServer(unsigned long msWait );
    bool startZoning( vector<vector<string> > zones );
    bool stopZoning();
    bool sendChatMessages( string chatMessage, UINT chatWaitTime );
    bool sendTellMessages( string tellMessage, UINT tellWaitTime );
    bool sendAttachAttribMessages( UINT attribWaitTime );
    bool sendInvAttribMessages( UINT attribWaitTime );
    bool isRunning();
    unsigned long threadId();

    static Client* createInstance(  CountDownLatch&         comletionLatch
                                    , const UserConfig&     userConfig
                                    , const HostConfig&     hostConfig
                                    , const ClientConfig&   clientConfig    );

    Client& setWebProxy( WebServicesProxy& webServices );
    Client& setGameId( unsigned long gameId );
    Client& queue( ClientCommand::Ptr cmd );
    Client& addStateChangeListener( StateChangeListener& listener );
    Client& removeStateChangeListener( StateChangeListener& listener );
    Client& setMailer( MailerPlugin * mailer, jsFastSync * sync );

private:
    ClientInstance* _impl;
	jsFastSync * _syncEmailAlerts;
};
#endif
