#ifndef __CLIENTCOMMAND_H__
#define __CLIENTCOMMAND_H__

//#include "ClientImpl.h"
#include "Core/Util/ChainedException.h"
#if 0
#include "Commands/ConnectCommand.h"
#include "Commands/LoginCommand.h"
#include "Commands/SelectCharacterCommand.h"
#include "Commands/SendEntityGUpdatecommand.h"
#include "Commands/SendTextCommand.h"
#include "Commands/SleepCommand.h"
#include "Commands/ArrestorCommand.h"
#endif

// Exceptions
//
CHAINED( ArrestedException );       // todo: move to core.

// Forward References
//
class ClientInstance;

class ClientCommand {
public:
    virtual ~ClientCommand();
    virtual ClientCommand& doCommand(ClientInstance& clientInstance) = 0;
    typedef jsRefCountPtr<ClientCommand> Ptr;
    virtual bool operator==( const ClientCommand& other) const;
};
#endif