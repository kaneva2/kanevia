#ifndef __CONFIGURATIONELEMENTS_H__
#define __CONFIGURATIONELEMENTS_H__

#include <vector>
#include <string>

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>



#include "common/KEPUtil/URL.h"

#define LOGGER_NAME "ClientInstance"

typedef vector< vector< string > > StringTable;


enum UserRecordFields {
    USER_NAME   // 0
    , USER_PW // 1
    , USER_SERVER //2
    , USER_PORT //3
    , USER_EMAIL //4
    , USER_GAME_NAME //5
    , USER_TELL_TARGET //6
    , USER_TELL_SENDER //7
    , USER_STARTING_URL //8
    , USER_PLAYERCREATE_GENDER //9
    , USER_PLAYERCREATE_EDBINDEX //10
    , USER_PLAYERCREATE_BODYSLOTS //11
    , USER_PLAYERCREATE_HEADSLOTS //12
    , USER_PLAYERCREATE_HEADGLID //13
    , USER_PLAYERCREATE_SPAWNCFG //14
    , USER_PLAYERCREATE_TEAM //15
    , USER_PLAYERCREATE_SCALEX //16
    , USER_PLAYERCREATE_SCALEY //17
    , USER_PLAYERCREATE_SCALEZ // 18
};

struct DimConfigStruct
{
    int glid;
    int slot;
    int config;
    int mat;

    DimConfigStruct(unsigned int sl, unsigned long gl = 0 ) : slot(sl),glid(gl),config(0),mat(0)
    {}

};

// Serves as an input parameter for character creation operations
//
struct CharacterCreateStruct
{
    char gender;
    int EDBIndex;
    std::vector<DimConfigStruct> dim_slots;
    int spawnCfg;
    int team;
    float scaleX, scaleY, scaleZ;
};


class UserConfig {
public:
    UserConfig(   ) 
        :   _name("")
        , _password("")
        , _email("") 
        , _tellTrgt("")
        , _tellSrce("")
        , _startingURL()
        , _gameName()
        , _scriptName()
        , _characterCreate(0)
        , _server()
        , _port(0)
    {}

    UserConfig( const std::string&          name
                , const std::string&        password
                , const std::string&        email       
                , const std::string&        server
                , unsigned short            port
                , const std::string&        tellTrgt
                , const std::string&        tellSrce
                , const URL&                startingURL
                , const std::string&        gameName
                , const std::string&        scriptName          
                , CharacterCreateStruct*    characterCreate                ) 
        :   _name(name)
        , _password(password)
        , _email(email)
        , _tellTrgt(tellTrgt)
        , _tellSrce(tellSrce)
        , _startingURL(startingURL)
        , _gameName(gameName)
        , _scriptName(scriptName)
        , _characterCreate( characterCreate )
        , _server(server)
        , _port(port)
    {}

    UserConfig( const UserConfig& other ) 
        :   _name(other._name)
        , _password(other._password)
        , _email(other._email)
        , _tellTrgt(other._tellTrgt)
        , _tellSrce(other._tellSrce)
        , _startingURL(other._startingURL)
        , _gameName(other._gameName)
        , _scriptName(other._scriptName)
        , _characterCreate(other._characterCreate)
        , _webcalls_info( other._webcalls_info)
        , _server(other._server)
        , _port(other._port)
    {}

    UserConfig& operator=( const UserConfig& rhs ) {
        _name       = rhs._name;
        _password   = rhs._password;
        _email      = rhs._email;
        _tellTrgt   = rhs._tellTrgt;
        _tellSrce   = rhs._tellSrce;
        _startingURL = rhs._startingURL;
        _gameName  = rhs._gameName;
        _scriptName = rhs._scriptName;
        _characterCreate = rhs._characterCreate;
        _webcalls_info = rhs._webcalls_info;
        _server         = rhs._server;
        _port           = rhs._port;
        return *this;
    }

    virtual ~UserConfig() {}

    UserConfig& setServer( const std::string& server ) { _server = server; return *this; }
    const std::string& server() { return _server; }
    
    UserConfig& setPort( unsigned short port ) { _port = port; return *this; }
    unsigned short port() { return _port; }

    UserConfig& setEMail(  const std::string& emailAddy )  { _email = emailAddy; return *this; }     
    const std::string& email() { return _email; }

    UserConfig& setPassword( const std::string& password ) { _password = password; return *this; }
    const std::string& password() { return _password; }

    UserConfig& setTellTrgt( const std::string& tellTrgt ) { _tellTrgt = tellTrgt; return *this; }
    const std::string& tellTrgt() { return _tellTrgt; }

    UserConfig& setTellSrce( const std::string& tellSrce ) { _tellSrce = tellSrce; return *this; }
    const std::string& tellSrce() { return _tellSrce; }

    UserConfig& setStartingURL( const URL url ) { _startingURL = url; return *this; }
    const URL& startingURL() const { return _startingURL; }

    UserConfig& setGameName( const std::string& gameName)   { 
        _gameName = gameName; 
        return *this;     
    }

    const std::string& gameName() const                     { 
        return _gameName; 
    }

    UserConfig& setScriptName( const std::string& scriptName ) { _scriptName = scriptName; return *this; }
    const std::string& scriptName() const { return _scriptName;}

    UserConfig& setName( const std::string userName ) { _name = userName; return *this; }
    const std::string& name() const { return _name; }

    UserConfig setCharacterCreate( CharacterCreateStruct* characterCreate ) { _characterCreate = characterCreate; return *this; }
    CharacterCreateStruct* characterCreate() const { return _characterCreate; }

    typedef vector< vector< string > > StringTable;
//    StringTable xuser_info;
//    StringTable zones_info;
    StringTable _webcalls_info;

private:
    std::string     _server;
    unsigned short  _port;

    std::string _scriptName;
    std::string _gameName;
    std::string _name;
    std::string _password;
    std::string _email;
    std::string _tellTrgt;
    std::string _tellSrce;
    URL         _startingURL;
    CharacterCreateStruct*  _characterCreate;
};

class HostConfig {
public:
    HostConfig() : _host(""), _port(0) 
    {}

    HostConfig( const std::string host, unsigned short port ) : _host(host), _port(port) 
    {}

    HostConfig( const HostConfig& other ) : _host(other._host), _port(other._port) {}

    HostConfig& operator=( const HostConfig& rhs ) {
        _host = rhs._host;
        _port = rhs._port;
        return *this;
    }
    virtual ~HostConfig() {}

    const std::string&  host() const { return _host; }
    unsigned short      port() const { return _port; }
private:
    std::string     _host;
    unsigned short  _port;
};

class ClientConfig {
public:
    ClientConfig() 
        :   _baseDir()
        , _zoneWaitMS(0)  // is there an appropriate default for this? 
        , _loginURL()
        , _authURL()
        , _extraTellMsg()
        , _tellWaitMS(0)
        , _textMsg()
        , _textWaitMS(0)
        , _invWaitMS(0)
        , _attachWaitMS(0)
    {}

    ClientConfig( const ClientConfig& other ) 
        :   _baseDir(other._baseDir)
        , _zoneWaitMS( other._zoneWaitMS)
        , _loginURL( other._loginURL )
        , _authURL( other._authURL )
        , _extraTellMsg( other._extraTellMsg )
        , _tellWaitMS( other._tellWaitMS )
        , _textMsg( other._textMsg)
        , _textWaitMS( other._textWaitMS )
        , _invWaitMS( other._invWaitMS )
        , _attachWaitMS( other._attachWaitMS )
    {}

    ClientConfig& operator=( const ClientConfig& rhs ) {
        _baseDir        = rhs._baseDir;
        _zoneWaitMS     = rhs._zoneWaitMS;
        _loginURL       = rhs._loginURL;
        _authURL        = rhs._authURL;
        _extraTellMsg   = rhs._extraTellMsg;
        _tellWaitMS     = rhs._tellWaitMS;
        _textMsg        = rhs._textMsg;
        _textWaitMS     = rhs._textWaitMS;
        _invWaitMS      = rhs._invWaitMS;
        _attachWaitMS   = rhs._attachWaitMS;
        return *this;
    }

    bool operator==( const ClientConfig& rhs ) const {
        if ( this ==   &rhs ) return true;
        if ( _baseDir != rhs._baseDir ) return false;
        if ( _zoneWaitMS != rhs._zoneWaitMS ) return false;
        if ( _loginURL != rhs._loginURL ) return false;
        if ( !(_authURL == rhs._authURL ) ) return false;
        if ( _extraTellMsg != rhs._extraTellMsg ) return false;
        if ( _tellWaitMS != rhs._tellWaitMS ) return false;
        if ( _textMsg != rhs._textMsg ) return false;
        if ( _textWaitMS != rhs._textWaitMS ) return false;
        if ( _invWaitMS != rhs._invWaitMS ) return false;
        if ( _attachWaitMS != rhs._attachWaitMS ) return false;
        return true;
    }

    virtual ~ClientConfig() {}

    ClientConfig&       setBaseDir( const std::string& baseDir )    { _baseDir = baseDir; return *this;         }
    const std::string&  baseDir() const                             { return _baseDir;                          }

    ClientConfig& setZoneWaitMS( unsigned long ms )                 { _zoneWaitMS = ms; return *this;           }
    unsigned long zoneWaitMS() const                                { return _zoneWaitMS;                       }

    ClientConfig& setLoginURL( const URL url )                      { 
        _loginURL = url; 
        return *this;            
    }
    const URL& loginURL() const                                     { 
        return _loginURL;                         
    }

    ClientConfig& setAuthURL( const URL url )                       { 
        _authURL = url; 
        return *this;             
    }
    const URL& authURL() const                                      { 
        return _authURL;                          
    }

    ClientConfig& setExtraTellMsg( const std::string& tellMsg  )    { _extraTellMsg = tellMsg; return *this;    }
    const std::string& extraTellMsg() const                         { return _extraTellMsg;                     }

    ClientConfig& setTellWaitMS( unsigned long ms )                 { _tellWaitMS = ms; return *this;           }
    unsigned long tellWaitMS() const                                { return _tellWaitMS;                       }

    ClientConfig& setTextMsg( const std::string& tellMsg  )         { _extraTellMsg = tellMsg; return *this;    }
    const std::string& textMsg() const                              { return _extraTellMsg;                     }

    ClientConfig& setTextWaitMS( unsigned long ms )                 { _textWaitMS = ms; return *this;           }
    unsigned long textWaitMS() const                                { return _textWaitMS;                       }

    ClientConfig& setInvWaitMS( unsigned long ms )                  { _invWaitMS = ms; return *this;            }
    unsigned long invWaitMS()                                       { return _invWaitMS;                        }

    ClientConfig& setAttachWaitMS( unsigned long ms )               { _attachWaitMS = ms; return *this;         }
    unsigned long attacheWaitMS()                                   { return _attachWaitMS;                     }

    ClientConfig& excludeHosts( const StringHelpers::StringVector& excludedHosts ) {
        _excludedHosts = excludedHosts;
        return *this;
    }

    vector<string>& excludedHosts() {
        return _excludedHosts;
    }
private:
    StringHelpers::StringVector _excludedHosts;
    std::string     _baseDir;
    unsigned long 	_zoneWaitMS;
    URL             _loginURL;
    URL             _authURL;
    std::string	    _extraTellMsg;
    unsigned long	_tellWaitMS;

    std::string     _textMsg;
    unsigned long   _textWaitMS;

    unsigned long 	_invWaitMS;
    unsigned long   _attachWaitMS;
};
#endif
