#include "stdafx.h"
#include <sstream>

#include "Event\EventSystem\Dispatcher.h"
#include "Event\Base\IEvent.h"
#include "Common\include\ChatType.h"
#include "Common\include\CoreStatic\CStructuresMisl.h"
#include "Common\include\StpUrl.h"
#include "DPlayHandler.h"
#include "ClientImpl.h"

using namespace std;
DPlayHandler::DPlayHandler(ClientInstance* subject) : _subject(subject)
{}

DPlayHandler::~DPlayHandler()
{}

NETRET 
DPlayHandler::Connected(    IN LOGON_RET    replyCode
                            , IN NETRET     result
							, IN ULONG      sessionProtocolVersion ){
    LOG4CPLUS_TRACE( Logger::getInstance(LOGGER_NAME), "DPlayHandler::Connected( " << replyCode << "," << result << " )" );
	_subject->SetSessionProtocolVersion( sessionProtocolVersion );
    _subject->SwitchClientState( CONNECTED );
    // _subject->sendEmailAlert("SLA Logon Success on " +_subject->gs_servername, "Details: " +_subject->gs_username);
    return NET_OK;
}

NETRET 
DPlayHandler::Disconnected( string          reason
                            , HRESULT       hr
                            , BOOL          report
                            , LOGON_RET     replyCode
							, bool          connecting)   {

    // This will be called when switching servers or on final log off
    // either way no state needs to be changed because connecting to 
    // the new server does not wait for disconnect from previous server
    // (Though it would be nice to know wouldn't it?)
    //
    // Additional:  This is also called when the connection fails while connecting.
    // E.g. credentials don't match...connect credentials seems silly.
    //                               
    if (  _subject->getState().state == CONNECTING ) {
        LOG4CPLUS_ERROR(  Logger::getInstance( LOGGER_NAME ), "Connection failed");
		_subject->sendEmailAlert( ErrorSpec( replyCode, reason ) )
			     .setError(ErrorSpec( replyCode, reason ) );
        return NET_OK;
    }
    else {
        LOG4CPLUS_DEBUG ( Logger::getInstance( LOGGER_NAME ), "Disconnected : " << reason );
        _subject->SwitchClientState(NOT_CONNECTED);
    }

    _subject->wakeUp();

    return NET_OK;
}

void defaultCategory(unsigned catId, const string& catName) {
    LOG4CPLUS_TRACE( Logger::getInstance("DPlayHandler"), "DefaultHandler [" << catId << "][" << catName << "]" );
}
bool 
DPlayHandler::DataFromServer( IN NETID from, IN const BYTE *data, IN ULONG len )    {
    LPEXMSG_CATEGORY identityMsg = ( LPEXMSG_CATEGORY ) data;
    switch (identityMsg->m_category)    {
    case SPAWN_TO_SERVER_CAT:           
        fromServerSpawnStruct(data, len); break;
    case ENTITYGUPDATE_TO_CLIENT_CAT:   
        gupdateToClient(data, from); break;
    case TIMESYNC_TO_CLIENT_CAT:        
        defaultCategory(identityMsg->m_category, "TIMESYNC_TO_CLIENT_CAT"); break;
    default:                            
        handleDefault(identityMsg, len, data, from); break;
    }
    _subject->wakeUp();
    return true;
}

bool DPlayHandler::fromServerSpawnStruct(   const BYTE*     data
                                            , ULONG         len         ) {
    LPFROM_SERVER_SPAWN_STRUCT	formattedMessage = ( LPFROM_SERVER_SPAWN_STRUCT	) data;
    string  worldName   = formattedMessage->worldName;
    int     dbIndex     = formattedMessage->dbIndex;
    long    zoneIndex   = formattedMessage->zoneIndex;
    string  gameName    = "";
    string  url         = "";
    char*   pcURL       = 0;
    if (len > sizeof(FROM_SERVER_SPAWN_STRUCT) + worldName.size() )    {
        pcURL = (formattedMessage->worldName + worldName.size() + 1 );
        url = pcURL;
        StpUrl::ExtractGameName(pcURL, gameName );
    }
    _subject->SwitchClientState( SPAWN_STRUCT_RCV);

    _subject->spawnTo(worldName, dbIndex, zoneIndex, gameName, url );
    return true;
}


// This method underscores my concern.  Note all of the marshalling logic 
// interwoven with the business logic.  Another problem is that not having
// a single class that knows how to marshal an ENTITYGUPDATE message means
// that if the parsing changed on the development thread, this parser, 
// gathering dust on the shelf becomes out of date.  I agree with Bret as 
// to the cause of this, copy to get 'er done, but it would not have been
// necessary to copy this logic at all if it had ever been broken out.
//

class SimpleChatObject {
public:
    SimpleChatObject() {}
    SimpleChatObject( ChatType ct, const std::string& msg ) : _ct(ct), _msg(msg)
    {}

    SimpleChatObject( const SimpleChatObject& other ) : _ct(other._ct), _msg(other._msg)    {    
    }

    virtual ~SimpleChatObject() {
    }

    SimpleChatObject& operator=( const SimpleChatObject& rhs ) {
        _ct = rhs._ct;
        _msg = rhs._msg;
        return *this;
    }

    bool operator==( const SimpleChatObject& rhs ) {
        if ( &rhs == this )     return true;
        if ( rhs._ct != _ct )   return false;
        if ( rhs._msg != _msg )  return false;
        return true;
    }

private:
    ChatType    _ct;
    string      _msg;
};
// So having said that, let's take a shot at extracting a marshaller
//
class EntityUpdate {
public:
    EntityUpdate() {
        throw UnsupportedException(SOURCELOCATION);
    }

    EntityUpdate( const EntityUpdate& other ) 
    {}

    virtual ~EntityUpdate()     {
        delete[] attribArray;
    }

    EntityUpdate& operator=(const EntityUpdate& rhs ) {
        return *this;
    }

    bool operator==( const EntityUpdate& rhs ) {
        if ( &rhs == this ) return true;
        return false;
    }

    static EntityUpdate marshal(    const BYTE*         data
                                    , NETID             from
                                    , const EventMap&   eventMap ) {
        BYTE *blockCast = (BYTE *) data;
        ENTITYGUPDATE_HEADER * formattedMessage = (ENTITYGUPDATE_HEADER *) data;
        int	dataTracer = sizeof ( ENTITYGUPDATE_HEADER );
        EntityUpdate ret;
        if ( formattedMessage->m_attributeMsgCount > 0 )        {			
            int byteSize = sizeof ( ATTRIB_STRUCT ) *formattedMessage->m_attributeMsgCount;
            ret.attribArray = new ATTRIB_STRUCT[formattedMessage->m_attributeMsgCount];
            BYTE *atCast = (BYTE *)ret.attribArray;

            for ( int i = 0; i < byteSize; i++ )        {
                atCast[i] = blockCast[dataTracer];
                dataTracer++;
            }

//            for (   int atLoop = 0
//                    ; atLoop < formattedMessage->m_attributeMsgCount
//                    ; atLoop++ )        {
//                    LPATTRIB_STRUCT attribMessage = &ret.attribArray[atLoop];
//            }
        }	

        if ( formattedMessage->m_equipMsgCount > 0 )  { // It would seem that we chew this up.
            int				byteSize = sizeof ( EQUIP_STRUCT ) * formattedMessage->m_equipMsgCount;
            ret.equipArray = new EQUIP_STRUCT[formattedMessage->m_equipMsgCount];
            BYTE			*atCast = (BYTE *) ret.equipArray;
            for ( int i = 0; i < byteSize; i++ )
            {
                atCast[i] = blockCast[dataTracer];
                dataTracer++;
            }

            // It would appear that we throw this away.
            //
        }

        // We don't do text anymore...
        if ( formattedMessage->m_txtMsgCount > 0 )        {
            for ( int txtLoop = 0; txtLoop < formattedMessage->m_txtMsgCount; txtLoop++ )
            {		// txt msg loop
                int txtMsgSz = blockCast[dataTracer];
                dataTracer++;
                if ( txtMsgSz > 0 )
                {	// valid sz
                    ChatType ct = *((ChatType*)(&blockCast[dataTracer]));
                    dataTracer+=4;
                    TCHAR	*tempBlock = new TCHAR[txtMsgSz-4];
                    for ( int i = 0; i < txtMsgSz; i++ )
                    {
                        tempBlock[i] = blockCast[dataTracer];
                        dataTracer++;
                    }

                    SimpleChatObject o(ct,string(tempBlock));
                    ret._chatMessages.push_back( o );
//                    _subject->HandleRenderTextEvent( tempBlock, ct );
//                    delete[] tempBlock;
//                    tempBlock = 0;
                }	// end valid sz
            }		// end txt msg loop
        }

        if ( formattedMessage->m_updateStatMsgCount > 0 )        {
            int statBlockSize = blockCast[dataTracer];
            dataTracer++;
            if ( statBlockSize > 0 )
            {
                BYTE	*statUpdateBlock = new BYTE[statBlockSize];
                for ( int i = 0; i < statBlockSize; i++ )
                {
                    statUpdateBlock[i] = blockCast[dataTracer];
                    dataTracer++;
                }
                delete[] statUpdateBlock;
                statUpdateBlock = 0;
            }

            // Again, looks like we ignore it.
        }

        if ( formattedMessage->m_miscMsgCount > 0 )
        {	
            for ( int miscLoop = 0; miscLoop < formattedMessage->m_miscMsgCount; miscLoop++ )
            {
                BYTE	castByteTemp[2];
                castByteTemp[0] = blockCast[dataTracer];
                dataTracer++;
                castByteTemp[1] = blockCast[dataTracer];
                dataTracer++;

                short	miscBlockSize = 0;
                CopyMemory ( &miscBlockSize, castByteTemp, sizeof ( short ) );

                // int miscBlockSize = blockCast[dataTracer];
                // dataTracer++;
                if ( miscBlockSize > 0 )
                {	// 1
                    BYTE	*miscUpdateBlock = new BYTE[miscBlockSize];
                    for ( int i = 0; i < miscBlockSize; i++ )
                    {
                        miscUpdateBlock[i] = blockCast[dataTracer];
                        dataTracer++;
                    }

                    //HandleMiscBlocksClientSide ( miscUpdateBlock, miscBlockSize );
                    if ( miscUpdateBlock )
                    {
                        delete[] miscUpdateBlock;
                    }

                    miscUpdateBlock = 0;
                }	// end 1
            }
        }

        if ( formattedMessage->m_eventCount > 0 )        {
            for (   int eventLoop = 0
                    ; eventLoop < formattedMessage->m_eventCount
                    ; eventLoop++ )            {
                BYTE	castByteTemp[2];
                castByteTemp[0] = blockCast[dataTracer];
                dataTracer++;
                castByteTemp[1] = blockCast[dataTracer];
                dataTracer++;

                short	miscBlockSize = 0;
                CopyMemory ( &miscBlockSize, castByteTemp, sizeof ( short ) );

                // int miscBlockSize = blockCast[dataTracer];
                // dataTracer++;
                if ( miscBlockSize > 0 )                {
                    try                    {
                        PCBYTE          tempp   = &blockCast[dataTracer];
                        unsigned long   temps   = (unsigned long)miscBlockSize;
                        NETID           tempf   = from;
                        IEvent*         e       = eventMap.MakeEvent(   tempp       // blockCast[dataTracer]
                                                                        , temps     // (unsigned long)miscBlockSize
                                                                        , tempf     // from
                                                                        , (unsigned short)0 
                                                                        );  // from );

                        if ( e )                        {
                            if ( e->Id() > 0 ){ // never allow client to send <= 0
//                                _subject->eventDispatcher().ProcessOneEvent(e);
//                                ret._events.push_back(e);
                            }
                            else
                                e->Delete();
                        }
                    }
                    catch ( KEPException *e )                    {
                        LOG4CPLUS_ERROR(  Logger::getInstance( LOGGER_NAME ), "Event Exception Error: " << (ULONG)*(EVENT_ID*)data << " " << e->m_msg );
                        e->Delete();
                    }
                }
                dataTracer += miscBlockSize;
            }
        }

        return ret;
    }
private:            
    ATTRIB_STRUCT*              attribArray;    // = new ATTRIB_STRUCT[formattedMessage->m_attributeMsgCount];
    EQUIP_STRUCT*               equipArray;     // = new EQUIP_STRUCT[formattedMessage->m_equipMsgCount];
    vector<SimpleChatObject>    _chatMessages;
//    vector<IEvent*>             _events;
//    EventMap&                   _eventMap;
};


bool DPlayHandler::gupdateToClient( const BYTE * data, NETID from )        {
    BYTE *blockCast = (BYTE *) data;
    ENTITYGUPDATE_HEADER * formattedMessage = (ENTITYGUPDATE_HEADER *) data;
    int	dataTracer = sizeof ( ENTITYGUPDATE_HEADER );

    // Update entity attribute
    //
    if ( formattedMessage->m_attributeMsgCount > 0 )
    {			
        int byteSize = sizeof ( ATTRIB_STRUCT ) *formattedMessage->m_attributeMsgCount;
        ATTRIB_STRUCT *attribArray = new ATTRIB_STRUCT[formattedMessage->m_attributeMsgCount];
        BYTE *atCast = (BYTE *)attribArray;

        for ( int i = 0; i < byteSize; i++ )        {
            atCast[i] = blockCast[dataTracer];
            dataTracer++;
        }

        for (   int atLoop = 0
                ; atLoop < formattedMessage->m_attributeMsgCount
                ; atLoop++ )        {
            LPATTRIB_STRUCT attribMessage = &attribArray[atLoop];
            _subject->HandleAttribMessage( attribMessage, from );
            //LOG4CPLUS_INFO( Logger::getInstance( LOGGER_NAME ), "Entity Update Attribute : " << (int)attribMessage->attribute );
        }

        delete[] attribArray;
        attribArray = 0;
    }	

    // Update entity equipment
    //
    if ( formattedMessage->m_equipMsgCount > 0 )  { // It would seem that we chew this up.
        int				byteSize = sizeof ( EQUIP_STRUCT ) * formattedMessage->m_equipMsgCount;
        EQUIP_STRUCT	*equipArray = new EQUIP_STRUCT[formattedMessage->m_equipMsgCount];
        BYTE			*atCast = (BYTE *) equipArray;
        for ( int i = 0; i < byteSize; i++ )
        {
            atCast[i] = blockCast[dataTracer];
            dataTracer++;
        }

        delete[] equipArray;
        equipArray = 0;
    }

    // We don't do text anymore...
    if ( formattedMessage->m_txtMsgCount > 0 )
    {
        for ( int txtLoop = 0; txtLoop < formattedMessage->m_txtMsgCount; txtLoop++ )
        {		// txt msg loop
            int txtMsgSz = blockCast[dataTracer];
            dataTracer++;
            if ( txtMsgSz > 0 )
            {	// valid sz
                ChatType ct = *((ChatType*)(&blockCast[dataTracer]));
                dataTracer+=4;
                TCHAR	*tempBlock = new TCHAR[txtMsgSz-4];
                for ( int i = 0; i < txtMsgSz; i++ )
                {
                    tempBlock[i] = blockCast[dataTracer];
                    dataTracer++;
                }

                _subject->HandleRenderTextEvent( tempBlock, ct );
                delete[] tempBlock;
                tempBlock = 0;
            }	// end valid sz
        }		// end txt msg loop
    }

    if ( formattedMessage->m_updateStatMsgCount > 0 )
    {
        int statBlockSize = blockCast[dataTracer];
        dataTracer++;
        if ( statBlockSize > 0 )
        {
            BYTE	*statUpdateBlock = new BYTE[statBlockSize];
            for ( int i = 0; i < statBlockSize; i++ )
            {
                statUpdateBlock[i] = blockCast[dataTracer];
                dataTracer++;
            }
            delete[] statUpdateBlock;
            statUpdateBlock = 0;
        }
    }

    if ( formattedMessage->m_miscMsgCount > 0 )
    {	
        for ( int miscLoop = 0; miscLoop < formattedMessage->m_miscMsgCount; miscLoop++ )
        {
            BYTE	castByteTemp[2];
            castByteTemp[0] = blockCast[dataTracer];
            dataTracer++;
            castByteTemp[1] = blockCast[dataTracer];
            dataTracer++;

            short	miscBlockSize = 0;
            CopyMemory ( &miscBlockSize, castByteTemp, sizeof ( short ) );

            // int miscBlockSize = blockCast[dataTracer];
            // dataTracer++;
            if ( miscBlockSize > 0 )
            {	// 1
                BYTE	*miscUpdateBlock = new BYTE[miscBlockSize];
                for ( int i = 0; i < miscBlockSize; i++ )
                {
                    miscUpdateBlock[i] = blockCast[dataTracer];
                    dataTracer++;
                }

                //HandleMiscBlocksClientSide ( miscUpdateBlock, miscBlockSize );
                if ( miscUpdateBlock )
                {
                    delete[] miscUpdateBlock;
                }

                miscUpdateBlock = 0;
            }	// end 1
        }
    }

    if ( formattedMessage->m_eventCount > 0 )
    {
        for ( int eventLoop = 0; eventLoop < formattedMessage->m_eventCount; eventLoop++ )
        {
            BYTE	castByteTemp[2];
            castByteTemp[0] = blockCast[dataTracer];
            dataTracer++;
            castByteTemp[1] = blockCast[dataTracer];
            dataTracer++;

            short	miscBlockSize = 0;
            CopyMemory ( &miscBlockSize, castByteTemp, sizeof ( short ) );

            // int miscBlockSize = blockCast[dataTracer];
            // dataTracer++;
            if ( miscBlockSize > 0 )
            {
                try
                {
                    IEvent *e = _subject->eventDispatcher().MakeEvent( &blockCast[dataTracer], miscBlockSize, from );

                    if ( e )
                    {
                        if ( e->Id() > 0 ) // never allow client to send <= 0
                            _subject->eventDispatcher().ProcessOneEvent(e);
                        else
                            e->Delete();
                    }
                }
                catch ( KEPException *e )
                {
                    LOG4CPLUS_ERROR(  Logger::getInstance( LOGGER_NAME ), "Event Exception Error: " << (ULONG)*(EVENT_ID*)data << " " << e->m_msg );
                    e->Delete();
                }
            }
            dataTracer += miscBlockSize;
        }
    }
#if 1
#endif
    return true;
}




bool DPlayHandler::handleDefault( LPEXMSG_CATEGORY identityMsg
                                    , ULONG len, const BYTE * data, NETID from ) {
    if ( !((BYTE)identityMsg->m_category >= MIN_EVENT_BYTE && len >= sizeof( EventHeader ) ) )   {
        LOG4CPLUS_TRACE( Logger::getInstance( LOGGER_NAME ), "Unhandled message from server category: " << (int)identityMsg->m_category );
        return true;
    }

    IEvent *e = _subject->eventDispatcher().MakeEvent( data, len, from, ESEC_SERVER );
    if ( e )    {
        _subject->eventDispatcher().QueueEvent( e );
        _subject->wakeUp();
    }
    return true;

}
