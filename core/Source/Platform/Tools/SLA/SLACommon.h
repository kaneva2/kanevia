#ifndef __SLACOMMON_H__
#define __SLACOMMON_H__
#define LOGGER_NAME "ClientInstance"
#include <sstream>

CHAINED( NetworkException2 );

enum CLIENT_STATE{
    NOT_CONNECTED = 0,
    CONNECTING,
    CONNECTED,
    LOGGING_IN,
    LOGGED_IN,
    ZONING,                     // Client has initiated a rezone by sending a GTPE to the server.
    PENDING_SPAWN_RCV,          // The server has sent a pending spawn to this client in response to the GTP
    PENDING_SPAWN_SND,          // This client has sent it's response to the PSE from the server.
    SPAWN_STRUCT_RCV,           // This client has received the spawn structure from the server (spawn struct v pending?)
    SPAWN_STRUCT_SND,           // This client has sent it's response?
    IN_ERROR
};

typedef struct _CLIENT_STATE_TIME
{
    /**
     * Default constructor
     */
    _CLIENT_STATE_TIME() 
		:	_timer(Timer::running)
    {}

    /**
     * Construct and initialize with suppled state and
     * tick count.
     * Default constructor on timer.
     */
    _CLIENT_STATE_TIME( CLIENT_STATE st ) 
        :   state(st)
            , _timer(Timer::running)
    {}
    
    _CLIENT_STATE_TIME& operator=(const _CLIENT_STATE_TIME& rhs ) {
        state = rhs.state;
        _timer = rhs._timer;
        return *this;
    }

    std::string str() {
        std::stringstream ss;
        ss << StateToString(state) << " : " << _timer.elapsed();
        return ss.str();
    }

    const string stateToString() const {
        return StateToString( state );
    }

    static const string StateToString( const CLIENT_STATE state ) {
        switch ( state )	{
        case NOT_CONNECTED:		return "NOT_CONNECTED";
        case CONNECTING:		return "CONNECTING";
        case CONNECTED:		    return "CONNECTED";
        case LOGGING_IN:		return "LOGGING_IN";
        case LOGGED_IN:		    return "LOGGED_IN";
        case ZONING:		    return "ZONING";
        case PENDING_SPAWN_RCV:	return "PENDING_SPAWN_RCV";
        case PENDING_SPAWN_SND:	return "PENDING_SPAWN_SND";
        case SPAWN_STRUCT_RCV:	return "SPAWN_STRUCT_RCV";
        case SPAWN_STRUCT_SND:	return "SPAWN_STRUCT_SND";
		case IN_ERROR:			return "IN_ERROR";
        default:		        return "Undefined state = " + state; 
        }
    }

    CLIENT_STATE	state;
    Timer           _timer;
} CLIENT_STATE_TIME;


#endif