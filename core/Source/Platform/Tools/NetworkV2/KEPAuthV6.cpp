///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/KEPUtil/URL.h"
#include "Common/KEPUtil/Algorithms.h"
#include "common/KEPUtil/WebCall.h"
#include "KEPAuthV6.h"

namespace KEP {

static const std::string WEB_CALLER_KEP_AUTH_V6 = "KEPAuthV6";

AuthResponse::AuthResponse() : AbstractResponse()
		, _userInfo()
{}

AuthResponse& AuthResponse::parse( const std::string& content ) {
	AbstractResponse::parse(content);

	// Our data will be contained in the payload or Data section of the response
	// which, if it exists should contained as an XML string in _data;
	//
	TiXmlDocument doc;
	doc.Parse(data().c_str() );
	TiXmlElement* pDataEle = doc.FirstChildElement("Data");
	if ( pDataEle == 0 ) return *this;

	TiXmlElement* pUserEle = pDataEle->FirstChildElement("user");
	if ( pUserEle == 0 ) throw KEPAuthException();
	std::stringstream ss;
	ss << (*pUserEle);
	std::string ps = ss.str();
	_userInfo = ss.str(); //pDataEle->GetText();
	return *this;
}

// Note removal of superfluous passwordOk, while it was set in this function, it was never used externally
//
int KEPAuthV6::mapResultCode(	int			result
								, const char*	userName
								, ULONG		gameId		)		{
	// map to older return code w/o exposing KEPAUTH rcs
	switch ( result )	{
		case KEPAUTH_FAILED:				return LOGIN_FAILED;
		case KEPAUTH_SUCCESS:				return LOGIN_OK;
		case KEPAUTH_USER_NOT_FOUND:		return LOGIN_NOT_FOUND;
		case KEPAUTH_PASSWORD_INVALID:		return LOGIN_PASSWORD_INVALID;
		case KEPAUTH_NOT_VALIDATED:			return LOGIN_NOT_VALIDATED;
		case KEPAUTH_ACCOUNT_DELETED:		return LOGIN_DELETED;
		case KEPAUTH_ACCOUNT_LOCKED:		return LOGIN_LOCKED;
		case KEPAUTH_ALREADY_LOGGED_IN:		return LOGIN_ALREADY_LOGGED_ON;
		case KEPAUTH_GAME_FULL:				return LOGIN_GAME_FULL;
		case KEPAUTH_NOT_AUTHORIZED:		return LOGIN_NOT_AUTHORIZED;
		case KEPAUTH_NO_GAME_ACCESS:
			return LOGIN_NO_GAME_ACCESS; 
		case KEPAUTH_WRONG_SERVER:	 		
			return LOGIN_FAILED;
		default:
			// could be failed, etc. for now, we just use this 
			return LOGIN_NOT_FOUND;
	}
}

/**
 * Sample URL
 * 
 * 		KEPAuth/KKEPAuthV6.aspx?action=login&username=testuser&password=testpass&userIpAddress=10.10.10.10&gameId=3296&serverId=1
 * 
 * 		KEPAuth/KKEPAuthV6.aspx?action=logout&userId=4&reasonCode=1&serverId=1
 * 
 * Return results you must handle, some of these are new as SOAP would have handled automatically…
 * 
 * 	Login
 * 
 * 		<Result>
 * 			<ReturnCode>-1</ReturnCode>
 * 			<ResultDescription>userName, userIpAddress, password, gameId or serverId param not specified</ResultDescription>
 * 		</Result>
 * 
 * 		<Result>
 * 			<ReturnCode>-1</ReturnCode>
 * 			<ResultDescription>action param not specified</ResultDescription>
 * 		</Result>
 * 
 * 		<Result>
 * 			<ReturnCode>-1</ReturnCode>
 * 			<ResultDescription>Invalid parameter value passed in</ResultDescription>
 * 		</Result>
 * 
 * 		<Result>
 * 			<ReturnCode>1</ReturnCode>
 * 			<ResultDescription>Sucess</ResultDescription>
 * 					userInfo = "<user>";
 * 					userInfo += "<u>" + u.UserId.ToString() + "</u>";
 * 					userInfo += "<r>" + u.Role.ToString() + "</r>";  // this is Kaneva role
 * 					userInfo += "<g>" + u.Gender+ "</g>";
 * 
 * 					DateTime bd = Convert.ToDateTime(u.BirthDate);
 * 					std::string birthDateStr = bd.ToString("yyyy-MM-dd", System.Globalization.DateTimeFormatInfo.InvariantInfo); // get back to MySQL format
 * 
 * 					userInfo += "<b>" + birthDateStr + "</b>";
 * 					userInfo += "<m>" + (u.ShowMature? "1" : "0") + "</m>";
 * 					userInfo += "<c>" + u.Country + "</c>";
 * 					userInfo += "<p>" + roleId + "</p>"; // game prive id
 * 					userInfo += "</user>";
 * 		</Result>
 * 
 * 		<Result>
 * 			<ReturnCode>2</ReturnCode>
 * 			<ResultDescription>Not found</ResultDescription>
 * 		</Result>
 * 
 * 		<Result>
 * 			<ReturnCode>3</ReturnCode>
 * 			<ResultDescription>Invalid Password</ResultDescription>
 * 		</Result>
 * 
 * 		<Result>
 * 			<ReturnCode>" + (int)authCode + "</ReturnCode>
 * 			<ResultDescription>Bad auth code of " + authCode.ToString() + " for " + userName + " and gameId of " + gameId + "</ResultDescription>
 * 		</Result>
 * 
 * 		<Result>
 * 			<ReturnCode>6</ReturnCode>
 * 			<ResultDescription>Account Deleted</ResultDescription>
 * 		</Result>
 * 
 * 		<Result>
 * 			<ReturnCode>7</ReturnCode>
 * 			<ResultDescription>Account Locked</ResultDescription>
 * 		</Result>
 * 
 * 		<Result>
 * 			<ReturnCode>14</ReturnCode>
 * 			<ResultDescription>Banned</ResultDescription>
 * 		</Result>
 * 
 * 	All Return codes
 * 		FAILED				= 0,	// 0 not authenticated
 * 		SUCCESS				= 1,    // 1 successuful authentication
 * 		USER_NOT_FOUND		= 2,    // 2 user not retrieved from database
 * 		INVALID_PASSWORD	= 3,    // 3 invalid password
 * 		NO_GAME_ACCESS		= 4,    // 4 user was authenticated, but not to game supplied
 * 		NOT_VALIDATED		= 5,    // 5 user has not validated the account
 * 		ACCOUNT_DELETED		= 6,	// 6 user account was deleted
 * 		ACCOUNT_LOCKED		= 7,    // 7 user account was locked
 * 		ALREADY_LOGGED_IN	= 8,	// 8 user alreay logged in, performed by DS web service
 * 		GAME_FULL			= 9,    // 9 Max user limit hit for the game
 * 		NOT_AUTHORIZED		= 10,   // 10 mature, age, access pass, etc.
 * 		WRONG_SERVER		= 11,   // 11 mismatched server info serverId doesn't match serverId for actualIp
 * 		ADMIN_ONLY			= 12,   // 12 the server is admin_only and you're not an admin
 * 		PROMPT_ALLOW		= 13,   // 13 the player must be prompted before going on to patch 3dApp
 * 		GAME_BANNED			= 14    // 14 the game (3DApp) has been banned
 */
int KEPAuthV6::logon(	const char*			userName
						, const char*		ipAddress
						, const char*		password
//						, BYTE			clientType
						, Logger&		logger
						, const char*		authServerUrl
						, ULONG			gameId
						, LONG			serverId
						, std::string&  faultString
						, std::string&	userInfo )		{
	
	// check the credentials against the userid/pw
	// must have game id to be able to auth
//	if ( clientType == LOGON_AI )		return LOGIN_NOT_FOUND;		
	if ( authServerUrl == 0 )			return LOGIN_NOT_FOUND;
	if ( *authServerUrl == 0 )			return LOGIN_NOT_FOUND;

	if( gameId == 0 )	{
		LOG4CPLUS_WARN( logger, loadStr( IDS_NO_GAME_ID ) );
		return LOGIN_NO_PING;
	}

	try {
		//	KEPAuth/KKEPAuthV6.aspx?action=login&username=testuser&password=testpass&userIpAddress=10.10.10.10&gameId=3296&serverId=1
		//
		URL url = URL::parse(authServerUrl);
		url.setParam( "action", "login"  )
		   .setParam( "username", userName )
		   .setParam( "password", password )
		   .setParam( "userIpAddress", ipAddress )
		   .setParam( "gameId", StringHelpers::parseNumber<unsigned long>(gameId ))
		   .setParam( "serverId", StringHelpers::parseNumber<long>(serverId ));

		std::string	statusdescription;	// imminent call to GetBrowserPage will populate 

        WebCallOpt wco;
        wco.url         = url.toString();
        wco.response    = true;
        WebCallActPtr p = WebCaller::WebCallAndWait(WEB_CALLER_KEP_AUTH_V6, wco);

        // Check low level failure
        //
        if ( p->IsError() ) {
		    url.setParam( "password", "sanitized" );
			faultString = formatError( "logon", url, "NET", p->ErrorCode(), p->ErrorStr() );
			LOG4CPLUS_ERROR( logger, faultString );
			return LOGIN_FAILED;
		}

        if ( p->StatusCode() != 200 ) {
            url.setParam( "password", "sanitized" );
            faultString = formatError( "logon", url, "HTTP", p->StatusCode(), p->StatusStr() );
            LOG4CPLUS_ERROR( logger, faultString );
            return LOGIN_FAILED;
        }

		AuthResponse response;
		response.parse( p->ResponseStr() );

		if ( response.code() == 0 ) {
			
			// Note result param used in this resource string is superfluous in HTTP environment

			// todo: make sure return codes are matched.
			userInfo = response.userInfo();
			return LOGIN_OK;
		}
		
		// DRF - Most Likely Bad Password
		faultString = response.message();
		return LOGIN_PASSWORD_INVALID;
	}
	catch( const ChainedException& exc ) {
		throw KEPAuthException().chain( exc );
	}
}

/**
 * 
 *	Logout
 *
 *  KEPAuth/KKEPAuthV6.aspx?action=logout&userId=4&reasonCode=1&serverId=1
 * 
 *	<Result>
 *		<ReturnCode>" + (int)authCode  + "</ReturnCode>
 *		<ResultDescription>Logout</ResultDescription>
 *	</Result>
 */
int KEPAuthV6::logout(	int						userId
						, int					reasonCode
						, int					serverId
						, Logger&				logger
						, const std::string&	authServerURL
						, std::string&			faultString ) {


	try {
		// KEPAuth/KKEPAuthV6.aspx?action=logout&userId=4&reasonCode=1&serverId=1

		URL url = URL::parse(authServerURL);
        url.setParam( "action"      , "logout"  )
            .setParam( "userId"     , StringHelpers::parseNumber<int>(userId ) )
            .setParam( "reasonCode" , StringHelpers::parseNumber<int>(reasonCode ) )
            .setParam( "serverId"   , StringHelpers::parseNumber<int>(serverId) );
        WebCallOpt wco;
        wco.url         = url.toString();
        wco.response    = true;
        WebCallActPtr p = WebCaller::WebCallAndWait( WEB_CALLER_KEP_AUTH_V6, wco);

        if ( p->IsError() ) {
            url.setParam( "password", "sanitized" );
            faultString = formatError( "logout", url, "NET", p->ErrorCode(), p->ErrorStr() );
            LOG4CPLUS_ERROR( logger, faultString );
            return LOGIN_FAILED;
        }

        if ( p->StatusCode() != 200 ) {
            url.setParam( "password", "sanitized" );
            faultString = formatError( "logout", url, "HTTP", p->StatusCode(), p->StatusStr() );
            LOG4CPLUS_ERROR( logger, faultString );
            return LOGIN_FAILED;
        }

		return LOGIN_OK;
	}
	catch ( const ChainedException& exc ) {
		throw KEPAuthException().chain( exc );
	}
}

std::string KEPAuthV6::formatError( const std::string&      action
                                    , const URL&            url
                                    , const std::string&    errorClass
                                    , long                  code
                                    , const std::string&    msg ) {
    std::stringstream ss;
    ss 	<< "KEPAuthV6::" << action << " infrastructure failure! status[" 
        << code 
        << "] URL[" 
        << url.toString()
        << "] description[" 
        << msg
        << "]";

    return ss.str();
}

} // namespace KEP