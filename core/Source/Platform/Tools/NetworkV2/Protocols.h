///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "CStructuresMisl.h"
#include "KEPConstants.h"

// Data structures used in client-server protocol negotiation

// Versioned message types
enum VersionedMessageTypes
{
	PC_MSG_SPAWN_STATIC_MO = 0,

	// Add more message types here

	NUM_VERSIONED_MSG_TYPES
};

// Protocol version definitions
struct ProtocolVersionInfo
{
	ULONG  globalVersion;
	USHORT messageVersions[NUM_VERSIONED_MSG_TYPES];
};

// Protocol compatibility for client-server negotiations
class ProtocolCompatibility
{
public:
	typedef std::map<ULONG, ProtocolVersionInfo> ProtocolMap;

	ProtocolCompatibility(); 

	// Return minimum version supported
	ULONG getMinVersion() const;

	// Return maximum (latest) version supported
	ULONG getMaxVersion() const;

	// Negotiate protocol based on peer info and pick an active version
	ULONG negotiate( ULONG peerMinVer, ULONG peerMaxVer );

	// Return message version based on global protocol version and message type
	int getMessageVersionByType( ULONG globalVersion, int messageType ) const
	{ 
		jsVerifyReturn(globalVersion!=INVALID_PROTOCOL_VERSION, 0);
		jsVerifyReturn(messageType>=0 && messageType<NUM_VERSIONED_MSG_TYPES, 0);

		ProtocolMap::const_iterator it = m_supportedVersions.find(globalVersion);
		jsVerifyReturn(it!=m_supportedVersions.end(), 0);

		return it->second.messageVersions[messageType];
	}

	int getLatestMessageVersionByType( int messageType ) const
	{
		jsVerifyReturn(messageType>=0 && messageType<NUM_VERSIONED_MSG_TYPES, 0);
		jsVerifyReturn(m_latestVersion!=NULL, 0);

		return m_latestVersion->messageVersions[messageType];
	}

	bool isConversionRequired( int messageType, USHORT msgVer ) const
	{
		return getLatestMessageVersionByType(messageType)!=msgVer;
	}

	static ProtocolCompatibility& Instance() { return ms_instance; }

private:
	ProtocolMap m_supportedVersions;
	ProtocolVersionInfo * m_latestVersion;
	ULONG m_legacyVersionNumber;

	static ProtocolCompatibility ms_instance;
};
