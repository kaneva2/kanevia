#pragma once

#include "Common/Include/KEPCommon.h"

#include "INetwork.h"

#include <string>
#include "Common/KEPUtil/URL.h"
#include "common\include\EngineTypes.h"

class jsThread;

namespace KEP {

/// \brief Interface for logging sends and receives
///
/// this implements the INetwork* classes for passing data thru, and letting
/// the derived class log or playback.  Basically an interloper in the network classes
class INetworkLogger : public IServerNetwork, public IServerNetworkCallback
{
public:
	enum
	{
		etValidateLogin,
		etClientConnected,
		etDataFromClient,
		etSend,
		etClientDisconnected
	} EventType;

	// INetwork overrides	
	virtual NETRET Send( NETID to, const BYTE *data, ULONG bytes )
	{
		return m_serverPassThru->Send( to, data, bytes );
	}

	virtual const std::string& Name() const
	{ 
		return m_serverPassThru->Name(); 
	}

	// IServerNetwork overrides
	virtual NETRET Listen( 
		IServerNetworkCallback *callback, 
		eEngineType engineType,
		ULONG port, 
		const char* hostName = "" ) 
	{ 
		return m_serverPassThru->Listen( callback, engineType, port, hostName ); 
	}
		
	virtual std::string GetIPAddress( NETID id )
	{ 
		return m_serverPassThru->GetIPAddress( id ); 
	}

	virtual NETRET DisconnectClient( NETID id, eDisconnectReason r )
	{
		return m_serverPassThru->DisconnectClient(id, r);
	}

	virtual const URL& authURL() const
	{
		return m_serverPassThru->authURL();
	}

	INetworkLogger&	setInstanceName(const std::string & name)
	{
		m_serverPassThru->setInstanceName(name);
		return *this;
	}

	// IServerNetworkCallback overrides
	virtual bool DataFromClient( NETID from, const BYTE *data, ULONG len ) 
	{
		return m_callbackPassThru->DataFromClient(from, data, len);
	}
	
	virtual LOGON_RET ValidateLogon(	
		NETID from, 
		CONTEXT_VALIDATION *context, 
		const char* ipAddress, 
		const char* userInfoXML )
	{
		return m_callbackPassThru->ValidateLogon( from, context, ipAddress, userInfoXML );
	}

	virtual NETRET ClientConnected( NETID id, const char* playerName )
	{
		return m_callbackPassThru->ClientConnected(id, playerName);
	}

	virtual NETRET ClientDisconnected( NETID id, eDisconnectReason reason )
	{
		return m_callbackPassThru->ClientDisconnected(id, reason);
	}

	
	// our own method!	
	virtual std::string ProcessCommand( const char* commandline, NETID replyNetId ) = 0;
	virtual void AddPlaybackPendingLogin( const char* ) = 0;
	virtual bool ThreadComplete( jsThread *t ) = 0;

protected:	
	
	INetworkLogger( const char* baseDir, IServerNetwork *serverPassThru, IServerNetworkCallback *callbackPassThru ) : IServerNetwork(), 
		m_serverPassThru( serverPassThru ), m_callbackPassThru( callbackPassThru ) 
    {}
	virtual ~INetworkLogger() {}
	
	IServerNetwork 			*m_serverPassThru;
	IServerNetworkCallback 	*m_callbackPassThru;
};

} // namespace KEP