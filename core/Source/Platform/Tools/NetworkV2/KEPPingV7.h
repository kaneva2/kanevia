///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <Log4CPlus/Logger.h>
#include "Core/Util/ChainedException.h"
#include "Server\KEPServer\KEPServerEnums.h"
#include "NetworkHelpers.h"
#include "Tools/NetworkV2/Pinger/Pinger.h"
#include "Tools/NetworkV2/Pinger/Common.h"
#include "Server/KEPServer/KEPServerEnums.h"

#include "common\include\EngineTypes.h"

namespace KEP {

/**
 * Response parser specialized for parsing responses from the Ping Initialize 
 * action.
 */
class __declspec(dllexport) PingInitializeResponse : public AbstractResponse {
public:

	PingInitializeResponse();

	virtual ~PingInitializeResponse();

	/**
	 * @throws KEPPingException
	 */
	PingInitializeResponse& parse( const std::string& content );

	unsigned long		serverId() const		{ return _serverId;	}
	unsigned long		maxCount() const		{ return _maxCount;	}
	unsigned long		gameId() const			{ return _gid;		}
	unsigned long		parentGameId() const	{ return _pgid;		}
	bool				status() const			{ return _status;	}
	const std::string&	gameName() const		{ return _gname;	}
	const std::string&  hash() const			{ return _hash;		}

private:
	unsigned long	_serverId;
	unsigned long	_maxCount;
	unsigned long	_gid;
	unsigned long	_pgid;
	bool			_status;
	std::string		_gname;
	std::string		_hash;
};

class _declspec(dllexport) PingPingResponse : public AbstractResponse {
public:
	PingPingResponse();
	PingPingResponse& parse( const std::string& content );
	const std::string& hash() const;
private:
	std::string		_hash;
};


class _declspec(dllexport) PingGetCountResponse : public AbstractResponse {
public:
	PingGetCountResponse();
	PingGetCountResponse& parse( const std::string& content );
	bool status() const;
	const std::string& hash() const;
	unsigned long maxPlayers() const;

private:
	bool			_status;
	unsigned long	_maxPlayerCount;
	std::string		_hash;
};


/**
 * KEPPingV7 differs from V6 only in that the response format/values are modified to facilitate
 * simplified parsing and success/fail/error determination.  Unfortunately the V6 parser/marshalling
 * logic is not decoupled from V6 protocol.  If it had been we could simply reimplemented the parser
 * and be done with it.  Furthermore, there is no value in refactoring V6 to allow for this 
 * decoupling, as this could potentially destabilize code that becomes obsolete after V7 is
 * pervasive.
 *
 */
class KEPPingV7 {
private:
	KEPPingV7();

	// Can we get rid of this?
	static std::string formatError( long code, const std::string& msg );

public:
	enum {
		PING_OK					= 0
		, PING_BAD_IP			= 1
		, PING_NOT_FOUND		= 2
		, PING_ASSET_VERSION	= 3
		, PING_PROTOCOL_VERSION = 4
		, PING_UNKNOWN_ERROR	= 5
	} PING_ERR_CODES;


	KEPPingV7(	Logger&					logger
				, int&					gameId
				, int&					parentGameId
				, int& 					serverId
				, const std::string&	pingServerUrl
				, const std::string&	regKey
				, const std::string&	hostName
				, const std::string&	waterMark
				, eEngineType engineType
				, const std::string&	ipAddress
				, unsigned short		port
				, ULONG					protocolVersion
				, LONG					schemaVersion
				, const std::string&	serverVersion
				, ULONG					assetVersion
				, ULONG					timeoutMs
				, LONG&					numberOfPlayers
				, LONG&					currentMaxPlayers
				, int&					maxAllowedPlayers
				, bool&					adminOnly
				)  
		:	m_logger( logger )
			, m_gameId( gameId )
			, m_parentGameId( parentGameId )
			, m_serverId( serverId )
			, m_pingServerUrl( pingServerUrl )
			, m_regKey(regKey)
			, m_hostName(hostName)
			, m_waterMark(waterMark)
			, m_engineType(engineType)
			, m_ipAddress(ipAddress)
			, m_port(port)
			, m_protocolVersion( protocolVersion )
			, m_schemaVersion(schemaVersion)
			, m_serverVersion(serverVersion)
			, m_assetVersion(assetVersion)
			, m_timeoutMs( timeoutMs )
			, m_numberOfPlayers( numberOfPlayers )
			, m_currentMaxPlayers( currentMaxPlayers )
			, m_maxAllowedPlayers( maxAllowedPlayers )
			, m_adminOnly( adminOnly )
	{}

	virtual ~KEPPingV7() 
	{}

	/**
	 * @throws KEPPingException
	 */
	HRESULT sendPing(		EServerState		state
							, char*				unused
							, std::string&		result
							, std::string&		faultString		);

	/**
	 * @throws KEPPingException
	 */
	HRESULT sendInitialize(	const std::string&	unused
							, int&				newMaxAllowedPlayers
							, bool&				active
							, std::string&		gameName
							, std::string&		result
							, std::string&		faultCode		);

	/**
	 * @throws KEPPingException
	 */
	HRESULT sendGetCount(	EServerState		state
							, char*				unused
							, int&				newMaxAllowedPlayers
							, bool&				active
							, std::string&		result
							, std::string&		faultCode		);
private:
	Logger			m_logger;
	int&			m_gameId;
	int&			m_parentGameId;
	int& 			m_serverId;
	std::string		m_pingServerUrl;
	std::string		m_regKey;
	std::string		m_hostName;
	std::string		m_waterMark;
	eEngineType m_engineType;
	std::string		m_ipAddress;
	unsigned short	m_port;
	ULONG			m_protocolVersion;
	LONG			m_schemaVersion;
	std::string		m_serverVersion;
	ULONG			m_assetVersion;
	ULONG			m_timeoutMs;
	LONG&			m_numberOfPlayers;
	LONG&			m_currentMaxPlayers;		// max players currently set on this server
	int&			m_maxAllowedPlayers;		// max allowed players for this license -- Not clear if this need be a reference. yet.
												// comments in Pinger.cpp imply that yes, it is dynamic.

	bool&			m_adminOnly;				// Can be changed externally to this class via PingThd::update().  So needs to be a reference
};

} // namespace KEP