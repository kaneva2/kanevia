///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include "Core/Util/ChainedException.h"

namespace KEP {

int ValidateLogonViaWS(const char* userName,
	const char* password,
	const char* ipAddress,
	Logger &logger,
	const char* authServerUrl,
	ULONG gameId,
	LONG serverId,
	std::string &userInfo);

int LogoutUserViaWS(int userId,
	enum KEP::eDisconnectReason reasonCode,
	int serverId,
	Logger &logger,
	const std::string& authServerUrl);

char mapLogonStatus(int ret);


void logVersion(Logger &logger);


// This model is unwieldy, just to simply be able to avoid writing accessors
// for the data members.
// Requires all data be available when instantiated.  But never explicitly 
// parses the application specific payload contained in the Data component
// of the response.  
class __declspec(dllexport) GeneralizedResponse {
public:
	typedef std::shared_ptr<GeneralizedResponse> GeneralizedResponsePtr;
	GeneralizedResponse( int code, const std::string& message, const std::string& data ) 
		: _returnCode(code), _message(message), _data(data) 
	{}

	const int			_returnCode;
	const std::string	_message;
	const std::string	_data;

	void operator=(GeneralizedResponse&) = delete; // Bypasses warning about class being implicitly non-assignable. (C4512)
};

/**
 * Basic Interface for all ping/auth rest responses.  This interface is 
 * premised on the expectation that all non-SOAP responses from the 
 * Ping/Auth web services have the basic composition of:
 *
 *      code			Single numeric value that can be used to immediately
 *						if the call resulted in an application error. 
 *						(code != 0)
 *
 *      message			String that describes the return code. (e.g. "OK")
 *
 *      data			In the event that the code indicates success, data
 *						containing the response from the remote service.
 */
class _declspec(dllexport) Response {
public:
	/**
	 * @throws GeneralizedResponseParser::Exception
	 */
	virtual Response& parse( const std::string& content )	= 0;

	/**
	 * Obtain a reference to a string representation of the response
	 * message.
	 */
	virtual const std::string& message() const				= 0;

	/**
	 * Obtain a reference to string representation of the
	 * response data.
	 */
	virtual const std::string& data() const					= 0;

	/**
	 * Obtain the result code of response
	 */
	virtual int code() const								= 0;
};

class __declspec(dllexport) GeneralizedResponseParser {
public:
	CHAINEDT( Exception, GeneralizedResponseParser::Exception  );
	/**
	 * @throws GeneralizedResponseParser::Exception
	 */
	static GeneralizedResponse::GeneralizedResponsePtr parse( const std::string& restResults );
};

class __declspec(dllexport) AbstractResponse : public Response {
public:
	AbstractResponse();
	virtual ~AbstractResponse();

	/**
	 * @throws GeneralizedResponseParser::Exception
	 */
	AbstractResponse&	parse( const std::string& content );
	int					code() const	{ return _response->_returnCode; }
	const std::string&	message() const { return _response->_message; }
	const std::string&	data() const	{ return _response->_data;	}
private:
	GeneralizedResponse::GeneralizedResponsePtr _response;
};

} // namespace KEP