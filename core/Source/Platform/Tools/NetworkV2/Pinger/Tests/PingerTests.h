///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Common/KEPUtil/SocketSubsystem.h"
#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"
#include "Tools/NetworkV2/Pinger/Pinger.h"



TEST(PingerTests, URLTEST1)
{
	string surl = "http://dev-auth.kaneva.com/kepauth/kepauthv6.aspx";
	URL url = surl;
	ASSERT_STREQ(surl.c_str(), url.str().c_str());
}

TEST(PingerTests, DISCO_Test01)
{
	SocketSubSystem sss;

	// logger name is starting to feel out of place
	//
	//			?action=Initialize
	PingerConfigurator pconfig;
	pconfig.setPingServerURL( URL::parse("dev-ping.kaneva.com/kepping/KEPPingV7.aspx"))
		.setGameKey("0000000000000000000000000000")
		.setInstanceName("test-server")
		.setEngineType(1)
		.setHostName("test-server.kaneva.com")
		.setPort(25857)
		.setProtocolVersion(32807)
		.setSchemaVersion(257)
		.setServerVersion("4.0.1.154")
		.setAssetVersion(33554432);
	MakePinger(pconfig);
}