///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef _PINGERTHREAD_H_
#define _PINGERTHREAD_H_
#include "Core/Util/HighPrecisionTime.h"
#include <jsSemaphore.h>
#include <jsThread.h>
#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"
#include "Core/Util/fast_mutex.h"

namespace KEP {

/* ============================================================================
Background thread to do the pinging...
============================================================================ */
class PingThd : public jsThread
{
public:

	const LONG SERVER_MAX;

	static const LONG MIN_USERS_ON_THROTTLE = 5; // 11/05 changed so both go to 5
	static const LONG MAX_UNPINGED_MINUTES = 30; // if can't ping in w/i 30 minutes, throttle them

	PingThd(const PingerConfigurator& conf);

	// engineId is the internal id from Kaneva, can save it to increase performance by 
	// avoiding looking on Kaneva
	bool initialize();

	int				gameId() const;
	int				parentGameId() const;
	LONG			serverId() const;
	const std::string &	gameName() const;
	time_t			getLastPingTime();

	// returns the max allowed number of players
	LONG update(enum EServerState newState, LONG numberOfPlayers, LONG currentMaxNumber, bool adminOnly);
	void invalidate();

protected:
	virtual ULONG _doWork();

	/**
	* @throws KEPPingException
	*/
	void sendPing(EServerState state);

	/**
	* @throws KEPPingException
	*/
	bool sendInitialize();

	/**
	* throws KEPPingException
	*/
	void sendGetCount(EServerState state);

	/**
	* @throws KEPPingException
	*/
	HRESULT sendInitializeV7(const std::string& s, int &newMaxAllowedPlayers, bool &active, std::string &gameName, std::string &result, std::string &faultCode);

	/**
	* @throws KEPPingException
	*/
	HRESULT sendPingv7(EServerState	state
		, char			*s
		, std::string		&result
		, std::string		&faultString);

	/**
	* @throws KEPPingException
	*/
	HRESULT sendGetCountV7(EServerState state, char *s, int &newMaxAllowedPlayers, bool &active, std::string &result, std::string &faultString);

	bool checkResult(std::string &result, const char* str, bool active, int newMaxAllowedPlayers, const std::string& faultString);
	void checkLimit(LONG newMaxAllowedPlayers);

	ULONG			m_sleepTimeMs;	// how long to sleep before next check in
	ULONG			m_sleepTimeSec; // when to ping in.  will use this iterval e.g. on "exactly" every 5th minute of time()
	std::string		m_gameName;		// returned from pinger

	// parms for Ping WebService 
	// static ones
	//std::string		m_key;
	int				m_gameId;
	int				m_parentGameId;
	LONG			m_numberOfPlayers;
	int				m_serverId;			// our Id from the server.
	bool			m_initOk;
	bool			m_throttled; 		// are we throttled?
	ULONG			m_timeoutMs;		// for SOAP calls

	// dynamic ones
	EServerState	m_state;
	LONG			m_currentMaxPlayers;		// max players currently set on this server
	const int		m_maxAllowedPlayers;		// max allowed players for this license
	fast_recursive_mutex		m_sync;		// protect the methods above
	fast_recursive_mutex		&getSync() { return m_sync; }
	jsSemaphore		m_sema;
	Logger			m_logger;
	ULONG			m_maxUnpingMS;
	Timer			m_timerLastPing;
//	ULONG			m_lastPingTime;
	time_t          m_lastPingTimeFormatted;
	int				m_randomTime;
	std::string		m_watermark;			// watermark
	int				m_pingVersion;			// extracted from URL
	bool			m_adminOnly;
	Logger			alert_logger;
	PingerConfigurator _conf;
};

} // namespace KEP
#endif