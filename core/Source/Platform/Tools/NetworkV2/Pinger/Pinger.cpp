///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <Common/KEPUtil/ExtendedConfig.h>
#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"
#include "Tools/NetworkV2/Pinger/Pinger.h"
#include "Tools/NetworkV2/Pinger/ConcretePinger.h"
#include "PingerConfigurator.h"

#define PREVIOUS_NORM_LICENSE_MAX  2000  // This is what wok server runs for license limit...when it was license limited.
										 // We have no idea how important or unimportant it might be to set this value
										 // way out there.  E.g. would hate find out that it affected load balancing.

namespace KEP {

PINGER_HANDLE MakePinger(const PingerConfigurator& configurator)
{
	// This was verifying that a reference to the max players was passed (not null)
	// This is now outlawed.
	// and hence this is detritis
	//
	ConcretePinger *pinger = new ConcretePinger(configurator);

	if (pinger == 0)
		return 0;

	try {
		if (!pinger->InitPinger() )		{
			delete pinger;
			return 0;
		}
	}
	catch (const ConfigurationException&  ce)		{
		LOG4CPLUS_ERROR(Logger::getInstance(Utf8ToUtf16(configurator.loggerName())), ce.str().c_str());
	}

	return pinger;
}

} // namespace KEP