///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Tools/NetworkV2/Pinger/Pinger.h"
#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"

namespace KEP {

class PingThd;
class PingerConfigurator;

class ConcretePinger : public Pinger
{
public:
	ConcretePinger(const PingerConfigurator& pingConf);

	bool InitPinger();

	bool InitPinger(	PingerConfigurator*	configurator	);

	virtual ULONG UpdatePinger(enum EServerState state, long numberOfPlayers, bool adminOnly);
	virtual void InvalidatePinger();

	~ConcretePinger();

	void SetGameId(LONG id);

	virtual long GetGameId() const;

	ConcretePinger& setPingIntervalMin(unsigned long _pingIntervalMin);
	ConcretePinger& setPingServerURL(const URL& pingServerURL);

	void SetParentGameId(LONG id);

	virtual long GetParentGameId() const;

	void SetServerId(LONG id);

	LONG GetServerId() const;

	void SetGameName(const std::string &s);

	ULONG GetGameName(char *s, ULONG slen);

	time_t GetLastPingTime();

private:
	Logger			m_logger;

	// ping settings
	PingThd*		m_pingThd;
	LONG			m_gameId;
	LONG			m_parentGameId;
	std::string		m_gameName;
	LONG 			m_serverId;
	LONG			m_pingIntervalMin;
	std::string		m_pingServerUrl;
	unsigned long   xm_maxPlayers;
	IServerNetwork* _server;
	PingerConfigurator _conf;
};

} // namespace KEP