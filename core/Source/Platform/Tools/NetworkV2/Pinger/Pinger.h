///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common/include/kepmacros.h"
#include "Server/KEPServer/KEPServerEnums.h"
#include "Common/KEPUtil/URL.h"

namespace KEP {

class Pinger {
public:
	Pinger() {}
	virtual ~Pinger() {};

	virtual void InvalidatePinger() = 0;
	virtual unsigned long UpdatePinger(enum EServerState state, long numberOfPlayers, bool adminOnly) = 0;
	virtual long GetGameId() const = 0;
	virtual long GetParentGameId() const = 0;
	virtual unsigned long GetGameName(char *s, ULONG slen) = 0;
	virtual long GetServerId() const = 0;
	virtual time_t GetLastPingTime() = 0;
	virtual Pinger& setPingIntervalMin(unsigned long _pingIntervalMin) = 0;
	virtual Pinger& setPingServerURL(const URL& pingServerURL) = 0;
};

class NullPinger : public Pinger
{
public:
	NullPinger()															{}
	virtual ~NullPinger()													{}
	virtual void InvalidatePinger()											{}
	virtual unsigned long UpdatePinger(	enum EServerState, long /* numberOfPlayers*/, bool /*adminOnly*/) { return 0;		}
	virtual long GetGameId()  const											{ return 0;		}
	virtual long GetParentGameId() const									{ return 0;		}	
	virtual unsigned long GetGameName(char *s, ULONG slen)					{ return 0;		}
	virtual long GetServerId() const										{ return 0;		}
	virtual time_t GetLastPingTime()										{ return 0;		}
	virtual Pinger& setPingIntervalMin(unsigned long _pingIntervalMin)		{ return *this; }
	virtual Pinger& setPingServerURL(const URL& pingServerURL)				{ return *this; }
};

typedef std::shared_ptr<Pinger> PingerPtr;

typedef void *PINGER_HANDLE;

PINGER_HANDLE MakePinger(const PingerConfigurator& configurator);

} // namespace KEP
	