///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Common/KEPUtil/URL.h>
#include <common\include\EngineTypes.h>

namespace KEP {

class PingerConfigurator {
public:
	PingerConfigurator();

	PingerConfigurator(const PingerConfigurator& other)
		: _devBuild(other._devBuild),
		  _pingIntervalMin(other._pingIntervalMin),
		  _protocolVersion(other._protocolVersion),
		  _schemaVersion(other._schemaVersion),
		  _assetVersion(other._assetVersion),
		  _timeoutSecs(other._timeoutSecs),
		  _maxPlayers(other._maxPlayers),
		  _engineType(other._engineType),
		  _port(other._port),
		  _pingServerURL(other._pingServerURL),
		  _gameKey(other._gameKey),
		  _ipAddress(other._ipAddress),
		  _serverVersion(other._serverVersion),
		  _loggerName(other._loggerName),
		  _instanceName(other._instanceName)
	{
	}

	PingerConfigurator& operator=(const PingerConfigurator& other)
	{
		if (this == &other)
			return *this;
		_devBuild = other._devBuild;
		_pingIntervalMin = other._pingIntervalMin;
		_protocolVersion = other._protocolVersion;
		_schemaVersion = other._schemaVersion;
		_assetVersion = other._assetVersion;
		_timeoutSecs = other._timeoutSecs;
		_maxPlayers = other._maxPlayers;
		_engineType = other._engineType;
		_port = other._port;
		_pingServerURL = other._pingServerURL;
		_gameKey = other._gameKey;
		_ipAddress = other._ipAddress;
		_serverVersion = other._serverVersion;
		_loggerName = other._loggerName;
		_instanceName = other._instanceName;
		return *this;
	}


	friend bool operator==(const PingerConfigurator& lhs, const PingerConfigurator& rhs)
	{
		return lhs._devBuild == rhs._devBuild
			&& lhs._pingIntervalMin == rhs._pingIntervalMin
			&& lhs._protocolVersion == rhs._protocolVersion
			&& lhs._schemaVersion == rhs._schemaVersion
			&& lhs._assetVersion == rhs._assetVersion
			&& lhs._timeoutSecs == rhs._timeoutSecs
			&& lhs._maxPlayers == rhs._maxPlayers
			&& lhs._engineType == rhs._engineType
			&& lhs._port == rhs._port
			&& lhs._pingServerURL == rhs._pingServerURL
			&& lhs._gameKey == rhs._gameKey
			&& lhs._ipAddress == rhs._ipAddress
			&& lhs._serverVersion == rhs._serverVersion
			&& lhs._loggerName == rhs._loggerName
			&& lhs._instanceName == rhs._instanceName;
	}

	friend bool operator!=(const PingerConfigurator& lhs, const PingerConfigurator& rhs)
	{
		return !(lhs == rhs);
	}


	/**
	* throws ConfigurationException
	*/
	PingerConfigurator& initialize(const std::string& baseDir);

	const URL&			pingServerURL()				const;
	PingerConfigurator& setPingServerURL(const URL pingServerURL);

	unsigned long		pingIntervalMin()		const;
	PingerConfigurator&	setPingIntervalMin(unsigned long pingIntervalMin);

	const std::string&  gameKey() const;
	PingerConfigurator& setGameKey(const std::string& gameKey);

	const std::string&	ipAddress() const;
	PingerConfigurator& setIPAddress(const std::string ipAddress);

	unsigned short		port() const;
	PingerConfigurator& setPort(unsigned short  port);

	unsigned long		protocolVersion() const;
	PingerConfigurator& setProtocolVersion(unsigned long protocolVersion);

	unsigned long		schemaVersion() const;
	PingerConfigurator& setSchemaVersion(unsigned long schemaVersion);

	const std::string&	serverVersion() const;
	PingerConfigurator&	setServerVersion(const std::string& basic_string);

	unsigned long		assetVersion() const;
	PingerConfigurator& setAssetVersion(unsigned long assetVersion);

	bool				devBuild() const;
	PingerConfigurator& setDevBuild(bool devBuild);

	eEngineType engineType() const;
	PingerConfigurator& setEngineType(eEngineType engineType);
	bool isEngineTypeServer() const {
		return engineType() == eEngineType::Server;
	}
	bool isEngineTypeDispatcher() const {
		return engineType() == eEngineType::DispatcherServer;
	}

	unsigned long		timeoutSecs() const;
	PingerConfigurator& setTimeoutSecs(unsigned long timeoutSecs);

	const std::string&	loggerName() const;
	PingerConfigurator& setLoggerName(const std::string& loggerName);

	const std::string&	instanceName() const;
	PingerConfigurator& setInstanceName(const std::string instanceName);

	unsigned long		maxPlayers() const;
	PingerConfigurator& setMaxPlayers(unsigned long maxPlayers);

	std::string str() const
	{
		std::stringstream ss;
		ss << ".setDevBuild(\"" << _devBuild << "\")"
			<< ".setPingIntervalMin(\"" << _pingIntervalMin << "\")"
			<< ".setProtocolVersion(\"" << _protocolVersion << "\")"
			<< ".setSchemaVersion(\"" << _schemaVersion << "\")"
			<< ".setAssetVersion(\"" << _assetVersion << "\")"
			<< ".setTimeoutSecs(\"" << _timeoutSecs << "\")"
			<< ".setMaxPlayers(\"" << _maxPlayers << "\")"
			<< ".setEngineType(\"" << (unsigned long) _engineType << "\")"
			<< ".setPort(\"" << _port << "\")"
			<< ".setPingServerURL(\"" << _pingServerURL.str() << "\")"
			<< ".setGameKey(\"" << _gameKey << "\")"
			<< ".setIPAddress(\"" << _ipAddress << "\")"
			<< ".setServerVersion(\"" << _serverVersion << "\")"
			<< ".setLoggerName(\"" << _loggerName << "\")"
			<< ".setInstanceName(\"" << _instanceName << "\");";
		return ss.str();
	}
private:
	bool			_devBuild;
	unsigned long	_pingIntervalMin;
	unsigned long   _protocolVersion;
	unsigned long	_schemaVersion;
	unsigned long	_assetVersion;
	unsigned long   _timeoutSecs;
	unsigned long	_maxPlayers;			// In fact this may be next.  Get a good build and checkin.  This needs to be resolved now.
									
	eEngineType	_engineType;
	unsigned short  _port;				// whose port?
	URL				_pingServerURL;
	std::string		_gameKey;
	std::string		_ipAddress;			// Name of this pingers host machine as returned by gethost()
	std::string		_serverVersion;
	std::string		_loggerName;
	std::string		_instanceName;

};

} // namespace KEP