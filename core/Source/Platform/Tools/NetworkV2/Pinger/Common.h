///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#ifndef __PINGCOMMON_H_
#define __PINGCOMMON_H_

#include "Core/Util/ChainedException.h"

/**
* @file
* General exception thrown by ping operations.
*/
CHAINED(KEPPingException);
#endif