///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"
#include "Common/KEPUtil/ExtendedConfig.h"
#include "common/include/NetworkCfg.h"
#include "Tools/NetworkV2/Pinger/Pinger.h"
#include "Common/include/KEPException.h"
#include <common/keputil/SocketSubsystem.h>

namespace KEP {

const long defaultPingIntervalMin = 3;


PingerConfigurator::PingerConfigurator()
	: _devBuild(false)
	, _pingIntervalMin(0)
	, _protocolVersion(0)
	, _schemaVersion(0)
	, _timeoutSecs(0)
	, _engineType(eEngineType::None)
	, _port(0)
	, _pingServerURL()
	, _gameKey()
	, _ipAddress()
	, _serverVersion("")
	, _loggerName("")
	, _instanceName("")
	, _maxPlayers(0)
{ }


PingerConfigurator& PingerConfigurator::initialize(const std::string& baseDir)					{
	std::string path = PathAdd(baseDir, NETCFG_CFG);
	std::string pingServerUrl;

	try		{
		KEPConfigAdapter cfg;
		cfg.Load(path.c_str(), NETCFG_ROOT);
		_pingIntervalMin = std::min(5, std::max(0, cfg.Get(PINGINTERVAL_TAG, defaultPingIntervalMin)));
		_pingServerURL = URL::parse(cfg.Get(PINGSERVERURL_TAG,""));
	}
	catch (KEPException * e)						{
		// KEPException cannot be chained to a chained exception
		// Build up an error message
		//
		ErrorSpec espec(e->m_err, e->m_msg);
		std::stringstream ss;
		ss  << "Exception raised configuring pinger ["
			<< espec
			<< "]";

		e->Delete();
		throw ConfigurationException(	TRACETHROW
										, SOURCELOCATION
										, espec );
	}
	catch (const ChainedException& e) {
		ConfigurationException nete(SOURCELOCATION, "Failed to initialize pinger configuration!");
		nete.chain(e);
		throw nete;
	}
	return *this;
}
#ifdef disabled
PingerConfigurator& 
PingerConfigurator::configure(KEP::Pinger* pinger)						{
	pinger->setPingServerURL(_pingServerURL)
		  .setPingIntervalMin(_pingIntervalMin);
	return *this;
}
#endif
const URL& 
PingerConfigurator::pingServerURL()				const{
	return _pingServerURL;
}

PingerConfigurator& 
PingerConfigurator::setPingServerURL(const URL pingServerURL)			{
	_pingServerURL = pingServerURL;
	return *this;
}

unsigned long 
PingerConfigurator::pingIntervalMin()		const						{
	return _pingIntervalMin;
}

PingerConfigurator& 
PingerConfigurator::setPingIntervalMin(unsigned long pingIntervalMin)	{
	_pingIntervalMin = pingIntervalMin;
	return *this;
}

const std::string&  
PingerConfigurator::gameKey() const										{
	return _gameKey;
}

PingerConfigurator& 
PingerConfigurator::setGameKey(const std::string& gameKey)				{
	_gameKey = gameKey;
	return *this;
}

const std::string& 
PingerConfigurator::ipAddress() const									{
	return _ipAddress;
}

PingerConfigurator& 
PingerConfigurator::setIPAddress(const std::string ipAddress)				{
	_ipAddress = ipAddress;
	return *this;
}

unsigned short 
PingerConfigurator::port() const										{
	return _port;
}

PingerConfigurator& 
PingerConfigurator::setPort(unsigned short  port)						{
	_port = port;
	return *this;
}

unsigned long 
PingerConfigurator::protocolVersion() const								{
	return _protocolVersion;
}

PingerConfigurator& 
PingerConfigurator::setProtocolVersion(unsigned long protocolVersion)	{
	_protocolVersion = protocolVersion;
	return *this;
}

unsigned long 
PingerConfigurator::schemaVersion() const								{
	return _schemaVersion;
}

PingerConfigurator& 
PingerConfigurator::setSchemaVersion(unsigned long schemaVersion)		{
	_schemaVersion = schemaVersion;
	return *this;
}

const std::string& 
PingerConfigurator::serverVersion() const								{
	return _serverVersion;
}

PingerConfigurator& 
PingerConfigurator::setServerVersion(const std::string& basic_string)	{
	_serverVersion = basic_string;
	return *this;
}

unsigned long 
PingerConfigurator::assetVersion() const								{
	return _assetVersion;
}

PingerConfigurator& 
PingerConfigurator::setAssetVersion(unsigned long assetVersion)			{
	_assetVersion = assetVersion;
	return *this;
}

bool 
PingerConfigurator::devBuild() const									{
	return _devBuild;
}

PingerConfigurator& 
PingerConfigurator::setDevBuild(bool devBuild)							{
	_devBuild = devBuild;
	return *this;
}

eEngineType 
PingerConfigurator::engineType() const									{
	return _engineType;
}

PingerConfigurator& 
PingerConfigurator::setEngineType(eEngineType engineType)				{
	_engineType = engineType;
	return *this;
}

unsigned long 
PingerConfigurator::timeoutSecs() const									{
	return _timeoutSecs;
}

PingerConfigurator& 
PingerConfigurator::setTimeoutSecs(unsigned long timeoutSecs)			{
	_timeoutSecs = timeoutSecs;
	return *this;
}

const std::string& 
PingerConfigurator::loggerName() const									{
	return _loggerName;
}

PingerConfigurator& 
PingerConfigurator::setLoggerName(const std::string& loggerName)		{
	_loggerName = loggerName;
	return *this;
}

const std::string& 
PingerConfigurator::instanceName() const								{
	return _instanceName;
}

PingerConfigurator& 
PingerConfigurator::setInstanceName(const std::string instanceName)		{
	_instanceName = instanceName;
	return *this;
}

unsigned long
PingerConfigurator::maxPlayers() const									{
	return _maxPlayers;
}

PingerConfigurator&
PingerConfigurator::setMaxPlayers(unsigned long maxPlayers)			{
	_maxPlayers = maxPlayers;
	return *this;
}

} // namespace KEP