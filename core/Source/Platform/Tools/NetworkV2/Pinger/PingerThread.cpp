///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <jsThread.h>

#include <common/include/NetworkCfg.h>
#include "Common/Include/LogHelper.h"
#include "Core/Util/OS.h"
#include <Server/KEPServer/KEPServerEnums.h>
#include "Tools/NetworkV2/Pinger/PingerThread.h"
#include "Tools/NetworkV2/Pinger/Common.h"
#include "Tools/NetworkV2/Pinger/Constants.h"
#include "Tools/NetworkV2/KEPPingV7.h"
#include "Common/KEPUtil/SocketSubsystem.h"
#include "Core/Crypto/Crypto_MD5.h"

namespace KEP {

PingThd::PingThd( const PingerConfigurator& conf)
	:	jsThread("PingThread", PR_LOW)
		, _conf(conf)
		, SERVER_MAX(PREVIOUS_NORM_LICENSE_MAX)
		, m_maxAllowedPlayers(PREVIOUS_NORM_LICENSE_MAX)
		, m_logger(Logger::getInstance(L"Pinger"))
		, m_watermark(gWatermark)
		, m_sleepTimeMs(5 * 60 * 1000) // default to 5 min
		, m_sleepTimeSec(60 * std::min(5UL, std::max(1UL, _conf.pingIntervalMin()))) // default to 5 min
		, m_gameId(0)
		, m_parentGameId(0)
//		, m_currentMaxPlayers(0)
		, m_serverId(0)
		, m_state(ssStarting)
		, m_numberOfPlayers(0)
		, m_maxUnpingMS(MAX_UNPINGED_MINUTES * 60 * 1000)
		, m_timerLastPing()
		, m_lastPingTimeFormatted(NULL)
		, m_initOk(false)
		, m_throttled(false)
		, m_pingVersion(0)
		, m_adminOnly(false)
		, m_randomTime(0)
		, m_timeoutMs(std::min(5000UL, _conf.timeoutSecs() * 1000UL))
		, alert_logger(log4cplus::Logger::getInstance(L"AlertLog"))
		, m_currentMaxPlayers(_conf.maxPlayers())
{}

bool
PingThd::initialize()
{
//	bool ret = false;

	// 1-5 min
//	m_sleepTimeSec = 60 * min(5, max(1, _conf.pingIntervalMin()));

	// get the settings from the config
	// validate them.  We must have a destination
	// to talk to
//	m_currentMaxPlayers = _conf.maxPlayers();

	// No more than 5 seconds
//	m_timeoutMs = min(5000,_conf.timeoutSecs() * 1000); // timeoutSec * 10000;

	std::string tempUrl(_conf.pingServerURL().str());
	STLToLower(tempUrl);
	std::string::size_type pos = tempUrl.find("/keppingv");
	if (pos != std::string::npos && tempUrl.size() >= pos + 9)
		m_pingVersion = tempUrl.at(pos + 9) - '0'; // convert char to number, we validate later
		
	if (_conf.pingServerURL().str().empty())		{
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_NO_PING_SERVER));
		return false;
	}
	std::string s1(_conf.ipAddress());
	std::stringstream s3;

	s3 << _conf.gameKey().substr(0, 6) << "..." << _conf.gameKey().substr(_conf.gameKey().length() - 2);

	LOG4CPLUS_INFO(m_logger, loadStr(IDS_PING_STARTED, 0, ::GetModuleHandle(0))        );
	LOG4CPLUS_INFO(m_logger, "    Url: " << _conf.pingServerURL().str() );
	LOG4CPLUS_INFO(m_logger, "    Interval (minutes): " << m_sleepTimeSec / 60);
	LOG4CPLUS_INFO(m_logger, "    Key: " << s3.str().c_str());
	LOG4CPLUS_INFO(m_logger, "    My ip:port: " << s1 << ":" << _conf.port());
	LOG4CPLUS_INFO(m_logger, "    Game Id: " << m_gameId);
	LOG4CPLUS_INFO(m_logger, "    Game Name: " << m_gameName.c_str());
	LOG4CPLUS_INFO(m_logger, "         Host Name: " << SocketSubSystem::hostName() );
	LOG4CPLUS_INFO(m_logger, "         Player limits: " << m_currentMaxPlayers << "/" << m_maxAllowedPlayers);

	if (!sendInitialize())
		return false;
	return start();
}

int PingThd::gameId() const			{
	return m_gameId;
}

int PingThd::parentGameId() const	{
	return m_parentGameId;
}

LONG PingThd::serverId() const		{
	return m_serverId;
}

const std::string &
PingThd::gameName() const	{
	LogTrace(m_gameName.c_str());
	return m_gameName;
}

time_t 
PingThd:: getLastPingTime(){
	return m_lastPingTimeFormatted;
}

	// returns the max allowed number of players
LONG 
PingThd::update(	enum EServerState	newState
					, LONG				numberOfPlayers
					, LONG				currentMaxNumber
					, bool				adminOnly				)		{
		LogTrace("");
		EServerState oldState = m_state;
		{
			std::lock_guard<fast_recursive_mutex> lock(getSync());
			m_state = newState;

			m_numberOfPlayers = numberOfPlayers;
			m_currentMaxPlayers = currentMaxNumber;
			m_adminOnly = adminOnly;
		} // lock scope

		if (m_initOk && m_currentMaxPlayers > m_maxAllowedPlayers && m_serverId > 0)
		{
			// knock them down 
			LOG4CPLUS_WARN(m_logger, loadStr(IDS_MAX_LIMIT_WARNING) << m_currentMaxPlayers << " > " << m_maxAllowedPlayers);
		}
		if (m_initOk && m_state != oldState) // ping now!
		{
			m_sema.increment();
			// why?  maybe was to allow m_maxAllowedPlayers to get updated
			// we'll wait for next pass jsSleep( 10 );
		}
		return m_maxAllowedPlayers;

	}

void 
PingThd::invalidate()	{
		LogTrace("");
		//Clear flag
		m_initOk = false;

		//Wake up ping thread to re-initialize
		m_sema.increment();

		//Wait until initialization done
		for (int i = 0; i<20 && !m_initOk; i++)
		{
			Sleep(250);
		}
	}

ULONG 
PingThd::_doWork()	{
	CoInitialize(0);

	getNDC().push(Utf8ToUtf16(_conf.instanceName()));
	int i = 0;
	time_t now;

	while (!isTerminated())		{
		try {
			now = time(&now);
			if (!m_initOk)			{					// Use a countdown and block until it is initialized.
				LogDebug("SendInitialize");
				sendInitialize(); // keep trying
				m_sema.waitAndDecrement(3000);
				continue;
			}

			if (m_state != ssRunning) 			{
				m_sema.waitAndDecrement(1000); // don't ping until start running
				if (m_state != ssRunning)
					continue;
			}

			LogDebug("Fully Running");

			if (m_throttled || i == RECHECK_COUNT && _conf.gameKey().length() > 0) { // only send GetCount if have a key
				sendGetCount(m_state);
				i = 0;
			}
			else			{
				sendPing(m_state);
				i++;
			}

			// how long to sleep until will ping in on next interval on the 
			// even interval minute
			int diff = (m_randomTime + (m_sleepTimeSec - (now % m_sleepTimeSec))) * 1000;
			m_sema.waitAndDecrement(diff);

			if (isTerminated()) 
				break;

			if (!m_throttled && m_timerLastPing.ElapsedMs() > m_maxUnpingMS)			{
				std::string s(loadStrPrintf(IDS_PING_TIMEOUT_THROTTLE, std::min(MIN_USERS_ON_THROTTLE, SERVER_MAX)));
				LOG4CPLUS_ERROR(m_logger, s);
				LOG4CPLUS_ERROR(alert_logger, s);
				m_timerLastPing.Reset();
				time(&m_lastPingTimeFormatted);
				std::lock_guard<fast_recursive_mutex> lock(getSync());
				m_throttled = true;
			}
		}
		catch (KEPPingException& exc) {
			LOG4CPLUS_ERROR(m_logger, exc.str().c_str());
			LOG4CPLUS_ERROR(alert_logger, exc.str().c_str());
		}
	}

	try {
		sendPing(ssStopped);  // send stop
	}
	catch (KEPPingException& exc) {
		LOG4CPLUS_ERROR(m_logger, exc.str().c_str());
		LOG4CPLUS_ERROR(alert_logger, exc.str().c_str());
	}

	getNDC().pop();
	CoUninitialize();
	delete this;
	return 0;
}

void 
PingThd::sendPing(EServerState state)		{
	if (m_serverId <= 0)		{
		TimeMs diff = m_timerLastPing.ElapsedMs();
		diff = m_maxUnpingMS > diff ? m_maxUnpingMS - diff : 0;
		LOG4CPLUS_ERROR(m_logger, loadStrPrintf(IDS_NO_PING_ERROR, diff / 60000) << loadStr(IDS_NO_ENGINE_ID));
		return;
	}

	std::string result;
	std::string faultString;
	HRESULT hr = E_INVALIDARG;
	char s[256];
	_snprintf_s(s, _countof(s), _TRUNCATE, "KEPPING%d%d%d", m_serverId, state, m_numberOfPlayers);

	switch (m_pingVersion)
	{
	case 4:
	case 5:
	case 6:
		hr = HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED);
		break;
	case 7:
		hr = sendPingv7(state, s, result, faultString);
		break;
	default:
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_BAD_URL) << " " << _conf.pingServerURL().str());
	}

	if (SUCCEEDED(hr))		{
		m_timerLastPing.Reset();
		time(&m_lastPingTimeFormatted);

		// don't get active or maxAllowedPlayers from ping, so pass in these values
		bool active = true;
		checkResult(result, s, active, m_maxAllowedPlayers, std::string(faultString.c_str()));
		LOG4CPLUS_DEBUG(m_logger, "Pinged: Id: " << m_serverId << " State: " << state);
	}
	else		{
		std::string msg;
		if (m_throttled)			{
			LOG4CPLUS_ERROR(m_logger, loadStr(IDS_STILL_THROTTLED) << faultString.c_str() << ":" << numToString(hr) << ":" << errorNumToString(hr, msg));
			LOG4CPLUS_ERROR(alert_logger, loadStr(IDS_STILL_THROTTLED) << faultString.c_str() << ":" << numToString(hr) << ":" << errorNumToString(hr, msg));
		}
		else			{
			TimeMs diff = m_timerLastPing.ElapsedMs();
			diff = m_maxUnpingMS > diff ? m_maxUnpingMS - diff : 0;
			std::string str(loadStrPrintf(IDS_NO_PING_ERROR, diff / 60000));
			LOG4CPLUS_ERROR(m_logger, str << faultString.c_str() << ":" << numToString(hr) << ":" << errorNumToString(hr, msg));
			LOG4CPLUS_ERROR(alert_logger, str << faultString.c_str() << ":" << numToString(hr) << ":" << errorNumToString(hr, msg));
		}
	}
}

bool 
PingThd::sendInitialize()	{
	bool ret = false;
	std::string result;
	std::string gameName;

	bool active = false; // Initialize to silence warning.
	HRESULT hr = S_OK;
	std::string faultCode;

	int newMaxAllowedPlayers = m_currentMaxPlayers;

	std::stringstream ss;
	ss << "KEPPING" << _conf.gameKey() << _conf.ipAddress() << _conf.port();
	std::string s = ss.str(); // drf - replaced char[256]

	switch (m_pingVersion)
	{
	case 4:
	case 5:
	case 6:
		hr = HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED);
		break;
	case 7:
		hr = sendInitializeV7(s, newMaxAllowedPlayers, active, gameName, result, faultCode);
		break;
	default:
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_BAD_URL) << " " << _conf.pingServerURL().str());
		return false;
	}

	if (SUCCEEDED(hr))	{
		if (checkResult(result, s.c_str(), active, newMaxAllowedPlayers, std::string(faultCode.c_str())))		{
			m_gameName = gameName;
			m_randomTime = m_gameId % 30; // vary ping times for STAR so don't get flooded [0-30] sec
			LOG4CPLUS_INFO(m_logger, "Random time will make ping in at " << m_randomTime << " for top of minute");

				m_initOk = true;
				LOG4CPLUS_DEBUG(m_logger, "Ping Init: Active: " << active << " New max allowed: " << newMaxAllowedPlayers << " EId: " << m_serverId << " GId: " << m_gameId);
				m_timerLastPing.Reset(); // for unthrottling, reset last ping time.
				time(&m_lastPingTimeFormatted);
				ret = true;
		}
		else				{
				// if SOAP succeeded, but init failed, chances are we shouldn't ever try again 
				// since something is wrong such as not registered, version, mismatch etc., which requires a 
				// server restart
				LOG4CPLUS_ERROR(m_logger, "Ping thread ending due to previous initalize error");

		}
	}
	else		{
		std::string msg;
		TimeMs diff = m_timerLastPing.ElapsedMs();
		diff = m_maxUnpingMS > diff ? m_maxUnpingMS - diff : 0;
		LOG4CPLUS_ERROR(m_logger, loadStrPrintf(IDS_NO_PING_ERROR, diff / 60000) << faultCode.c_str() << ":" << numToString(hr) << ":" << errorNumToString(hr, msg));
	}
	return ret;
}

void 
PingThd::sendGetCount(EServerState state)	{
	if (m_serverId <= 0)		{
		TimeMs diff = m_timerLastPing.ElapsedMs();
		diff = m_maxUnpingMS > diff ? m_maxUnpingMS - diff : 0;
		LOG4CPLUS_ERROR(m_logger, loadStrPrintf(IDS_NO_PING_ERROR, diff / 60000) << loadStr(IDS_NO_ENGINE_ID));
		return;
	}

	int newMaxAllowedPlayers = std::min((int)m_currentMaxPlayers, m_maxAllowedPlayers);

	bool active = false;
	std::string result;
	HRESULT hr = S_OK;
	std::string faultString;
	std::string faultCode;

	char s[256];
	_snprintf_s(s, _countof(s), _TRUNCATE, "KEPPING%d%d%d", m_serverId, state, m_numberOfPlayers);

	switch (m_pingVersion)
	{
	case 4:
	case 5:
	case 6:
		hr = HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED);
		break;
	case 7:
		hr = sendGetCountV7(state, s, newMaxAllowedPlayers, active, result, faultCode);
		break;
	default:
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_BAD_URL) << " " << _conf.pingServerURL().str());
		return;
	}

	if (SUCCEEDED(hr))		{
		checkResult(result, s, active, newMaxAllowedPlayers, std::string(faultCode.c_str()));
		LOG4CPLUS_DEBUG(m_logger, "Ping GetCount: Active: " << active << " New max allowed: " << newMaxAllowedPlayers);
		m_timerLastPing.Reset();
		time(&m_lastPingTimeFormatted);
	}
	else		{
		std::string msg;
		if (m_throttled)			{
			LOG4CPLUS_ERROR(m_logger, loadStr(IDS_STILL_THROTTLED) << faultString.c_str() << ":" << numToString(hr) << ":" << errorNumToString(hr, msg));
			LOG4CPLUS_ERROR(alert_logger, loadStr(IDS_STILL_THROTTLED) << faultString.c_str() << ":" << numToString(hr) << ":" << errorNumToString(hr, msg));
		}
		else			{
			TimeMs diff = m_timerLastPing.ElapsedMs();
			diff = m_maxUnpingMS > diff ? m_maxUnpingMS - diff : 0;
			std::string str(loadStrPrintf(IDS_NO_PING_ERROR, diff / 60000));
			LOG4CPLUS_ERROR(m_logger, str << faultString.c_str() << ":" << numToString(hr) << ":" << errorNumToString(hr, msg));
			LOG4CPLUS_ERROR(alert_logger, str << faultString.c_str() << ":" << numToString(hr) << ":" << errorNumToString(hr, msg));
		}
	}
}

HRESULT 
PingThd::sendInitializeV7(		const std::string&	s
								, int &			newMaxAllowedPlayers
								, bool &		active
								, std::string &	gameName
								, std::string &	result
								, std::string &	faultCode)	{
	HRESULT hr = S_OK;
	int unused(PREVIOUS_NORM_LICENSE_MAX);
	KEPPingV7 p(m_logger
			, m_gameId
			, m_parentGameId
			, m_serverId
			, _conf.pingServerURL().str()
			, _conf.gameKey()
			, SocketSubSystem::hostName()
			, m_watermark
			, _conf.engineType()
			, _conf.ipAddress()
			, _conf.port()
			, _conf.protocolVersion()
			, _conf.schemaVersion()
			, _conf.serverVersion()
			, _conf.assetVersion()
			, m_timeoutMs
			, m_numberOfPlayers
			, m_currentMaxPlayers
			, unused
			, m_adminOnly);

		std::string sGameName;
		std::string sResult;
		std::string sFaultCode;
		hr = p.sendInitialize(s, newMaxAllowedPlayers, active, sGameName, sResult, sFaultCode);
		gameName = sGameName.c_str();
		result = sResult.c_str();
		faultCode = sFaultCode.c_str();
		return hr;
	}

HRESULT 
PingThd::sendPingv7(	EServerState		state
						, char			*	s
						, std::string		&result
						, std::string		&faultString)	{
	HRESULT hr = S_OK;
	int unused(PREVIOUS_NORM_LICENSE_MAX);
	KEPPingV7 p(	m_logger
					, m_gameId
					, m_parentGameId
					, m_serverId
					, _conf.pingServerURL().str()
					, _conf.gameKey()
					, SocketSubSystem::hostName() //_conf.hostName()
					, m_watermark
					, _conf.engineType()
					, _conf.ipAddress()
					, _conf.port()
					, _conf.protocolVersion()
					, _conf.schemaVersion()
					, _conf.serverVersion()
					, _conf.assetVersion()
					, m_timeoutMs
					, m_numberOfPlayers
					, m_currentMaxPlayers
					, unused
					, m_adminOnly);

	std::string sResult;
	std::string sFaultString;
	hr = p.sendPing(state, s, sResult, sFaultString);
	result = sResult.c_str();
	faultString = sFaultString.c_str();
	return hr;
}

HRESULT 
PingThd::sendGetCountV7(	EServerState		state
							, char *			s
							, int &				newMaxAllowedPlayers
							, bool &			active
							, std::string&		result
							, std::string&		faultString)	{
	HRESULT hr = S_OK;
	int unused(PREVIOUS_NORM_LICENSE_MAX);

	KEPPingV7 p(	m_logger
					, m_gameId
					, m_parentGameId
					, m_serverId
					, _conf.pingServerURL().str()
					, _conf.gameKey()
					, SocketSubSystem::hostName() //_conf.hostName()
					, m_watermark
					, _conf.engineType()
					, _conf.ipAddress()
					, _conf.port()
					, _conf.protocolVersion()
					, _conf.schemaVersion()
					, _conf.serverVersion()
					, _conf.assetVersion()
					, m_timeoutMs
					, m_numberOfPlayers
					, m_currentMaxPlayers
					, unused
					, m_adminOnly);

	std::string sResult;
	std::string sFaultString;
	hr = p.sendGetCount(	state
							, s				// unused
							, newMaxAllowedPlayers
							, active
							, sResult
							, sFaultString);

	result = sResult.c_str();
	faultString = sFaultString.c_str();
	return hr;
}

bool 
PingThd::checkResult(	std::string&			result
						, const char*			str
						, bool					active
						, int					newMaxAllowedPlayers
						, const std::string&	faultString		)	{
	MD5_DIGEST_STR hash;
	hashString(str, hash);

	if (result != hash.s)		{
		loadStr(IDS_INCORRECT_PING_RESPONSE);
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_INCORRECT_PING_RESPONSE) << " " << (faultString.length() == 0 ? result.c_str() : faultString.c_str()));
		if (result.length() > 0 && result[0] == IP_CHANGED) //ISP could dynamically reassign IP for external server.  Reinitialize ping with new IP.
			m_initOk = false;
	}
	else if (!active)	{
			LOG4CPLUS_ERROR(m_logger, loadStr(IDS_INACTIVE_ACCOUNT));
	}
	else		{
		checkLimit(newMaxAllowedPlayers);
		return true;
	}
	return false;
}

void 
PingThd::checkLimit(LONG newMaxAllowedPlayers)	{
	if (m_initOk) { // don't check during init
		// if it goes down, change it, otherwise, they must restart for it
		// to take effect
		if (m_maxAllowedPlayers < newMaxAllowedPlayers)		{
			LOG4CPLUS_WARN(m_logger, loadStr(IDS_LICENSE_CHANGE_UP) << m_maxAllowedPlayers << " -> " << newMaxAllowedPlayers);
		}
		else if (m_maxAllowedPlayers > newMaxAllowedPlayers)		{
			LOG4CPLUS_WARN(m_logger, loadStr(IDS_LICENSE_CHANGE_DOWN) << m_maxAllowedPlayers << " -> " << newMaxAllowedPlayers);
		}
	}

	if (m_throttled)	{
	LOG4CPLUS_WARN(m_logger, loadStr(IDS_UNTHROTTLED) << m_maxAllowedPlayers << " -> " << std::min(newMaxAllowedPlayers, SERVER_MAX));
	LOG4CPLUS_WARN(alert_logger, loadStr(IDS_UNTHROTTLED) << m_maxAllowedPlayers << " -> " << std::min(newMaxAllowedPlayers, SERVER_MAX));
		m_throttled = false;
	}
	std::lock_guard<fast_recursive_mutex> lock(getSync());
}

} // namespace KEP