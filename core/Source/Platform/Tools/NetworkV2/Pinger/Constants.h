///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////



const unsigned long PREVIOUS_NORM_LICENSE_MAX = 2000;  // This is what wok server runs for license limit...when it was license limited.
// We have no idea how important or unimportant it might be to set this value
// way out there.  E.g. would hate find out that it affected load balancing.


const char IP_CHANGED = 'I';
const int RECHECK_COUNT = 10;
#ifndef WATERMARK
#error Must define WATERMARK
#else
const char* gWatermark = WATERMARK;
#endif


