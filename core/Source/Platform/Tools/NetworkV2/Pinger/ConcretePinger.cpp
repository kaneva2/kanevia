///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/Include/LogHelper.h"
#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"
#include "Tools/NetworkV2/Pinger/ConcretePinger.h"
#include "Tools/NetworkV2/Pinger/PingerThread.h"

namespace KEP {

ConcretePinger::ConcretePinger(const PingerConfigurator& pingConf)
	:	_conf(pingConf)
		, m_pingThd(0)
		, m_pingServerUrl(pingConf.pingServerURL().str())  // todo: convert url type to URL
		, m_gameId(0)
		, m_parentGameId(0)
		, m_pingIntervalMin(pingConf.pingIntervalMin())
		, _server(0)						
		, m_logger(Logger::getInstance(Utf8ToUtf16(pingConf.loggerName())))
{}

ConcretePinger::~ConcretePinger()	{
	LogTrace("");
	if (m_pingThd)	{
		m_pingThd->stop();
		m_pingThd = 0; // it will delete itself
	}
}

time_t ConcretePinger::GetLastPingTime()	{
	LogTrace("");
	if (m_pingThd) return m_pingThd->getLastPingTime(); else return NULL;
}

bool ConcretePinger::InitPinger(PingerConfigurator* configurator)	{
	
	LogTrace("");
	bool ret = true;

	if (m_pingThd)	{
		DELETE_AND_ZERO(m_pingThd);
	}

	m_pingThd = new PingThd(*configurator); // ->loggerName().c_str(), configurator->devBuild());

	if (!m_pingThd->initialize())	{
		DELETE_AND_ZERO(m_pingThd);
		return false;
	}

	SetGameId(m_pingThd->gameId());
	SetParentGameId(m_pingThd->parentGameId());
	SetServerId(m_pingThd->serverId());
	SetGameName(m_pingThd->gameName());
	return ret;
}

bool ConcretePinger::InitPinger()
{
	LogTrace("");
	bool ret = true;

	if (m_pingThd)	{
		DELETE_AND_ZERO(m_pingThd);
	}

	m_pingThd = new PingThd(_conf); // _conf.loggerName().c_str(), _conf.devBuild());

	if (!m_pingThd->initialize() )	{
		DELETE_AND_ZERO(m_pingThd);
		return false;
	}

	SetGameId(m_pingThd->gameId());
	SetParentGameId(m_pingThd->parentGameId());
	SetServerId(m_pingThd->serverId());
	SetGameName(m_pingThd->gameName());
	return ret;
}

ULONG ConcretePinger::UpdatePinger(	enum EServerState	state
									, long				numberOfPlayers
									, bool				adminOnly	){
	LogTrace("");
	if (!m_pingThd)
		return PingThd::MIN_USERS_ON_THROTTLE; // limit to MIN_USERS_ON_THROTTLE if ping thread not running

	xm_maxPlayers = m_pingThd->update(state, numberOfPlayers, xm_maxPlayers, adminOnly);

	// TODO: Get this hooked up to allow pinger to throttle the server.
	// Not the cleanest of abstractions, but it will have to do for now.
	if (_server)
		_server->setMaxConnections(xm_maxPlayers);

	SetGameId(m_pingThd->gameId());
	SetParentGameId(m_pingThd->parentGameId());
	SetGameName(m_pingThd->gameName());

	if (GetServerId() == 0 && m_pingThd->serverId() != 0)
		SetServerId(m_pingThd->serverId());

	return xm_maxPlayers;
}

void ConcretePinger::InvalidatePinger()		{
	LogTrace("");
	if (!m_pingThd) return;

	m_pingThd->invalidate();
	SetGameId(m_pingThd->gameId());
	SetParentGameId(m_pingThd->parentGameId());
	SetGameName(m_pingThd->gameName());
	if (GetServerId() == 0 && m_pingThd->serverId() != 0)
		SetServerId(m_pingThd->serverId());
}

ConcretePinger& ConcretePinger::setPingIntervalMin(unsigned long _pingIntervalMin){
	m_pingIntervalMin = _pingIntervalMin;
	return *this;
}

ConcretePinger& ConcretePinger::setPingServerURL(const URL& pingServerURL){
	m_pingServerUrl = pingServerURL.str();
	return *this;
}

void ConcretePinger::SetGameId(LONG id)	{
	LogTrace(StringHelpers::parseNumber(id).c_str());
	m_gameId = id;
}

long ConcretePinger::GetGameId() const	{
	LogTrace(StringHelpers::parseNumber(m_gameId).c_str());
	return m_gameId;
}

void ConcretePinger::SetParentGameId(LONG id)	{
	LogTrace(StringHelpers::parseNumber(id).c_str());
	m_parentGameId = id;
}

ULONG ConcretePinger::GetGameName(char *s, ULONG slen) 	{
	LogTrace(m_gameName.c_str());
	if (slen > m_gameName.size())		{
		strcpy_s(s, slen, m_gameName.c_str());
	}
	return m_gameName.size();
}

void ConcretePinger::SetGameName(const std::string &s)	{
	LogTrace(s.c_str());
	m_gameName = s;
}

LONG ConcretePinger::GetServerId() const	{
	LogTrace(StringHelpers::parseNumber(m_serverId).c_str());
	return m_serverId;
}

void ConcretePinger::SetServerId(LONG id)	{
	LogTrace(StringHelpers::parseNumber(id).c_str());
	m_serverId = id;
}

long ConcretePinger::GetParentGameId() const	{
	LogTrace(StringHelpers::parseNumber(m_parentGameId).c_str());
	return m_parentGameId;
}

} // namespace KEP