///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Tools/NetworkV2/KEPAuthV5.h"

static Logger logger = Logger::getInstance(L"PlatformTest");

class KEPAuthV5Tests : public ::testing::Test{
public:
	KEPAuthV5Tests() {
		StringResource = ::GetModuleHandle(NULL);
	}
};

TEST_F( KEPAuthV5Tests, BadAuthCodeTest ) {
	string userInfo;
	string faultString;
	EXPECT_EQ(	KEPAuthV5::logon(	"test"
									,"0.0.0.0"
									, "00000000000000000000000000000000"
//									, LOGON_CLIENT    // INVALID LOGON TYPE.  AI cannot auth.
									, logger
									, "http://3d-wok.kaneva.com:8080/kepauth/kepauthv5.aspx"
									, 1 // gameid
									, 2 // serverid
									, faultString
									, userInfo )
				, LOGIN_FORCE_OFF );
	EXPECT_STREQ( "Bad auth code of NO_GAME_ACCESS for test and gameId of 1", faultString.c_str() );
	EXPECT_STREQ( "", userInfo.c_str() );
}

TEST_F( KEPAuthV5Tests, InvalidPasswordTest ) {
	string userInfo;
	string faultString;

	EXPECT_EQ(	KEPAuthV5::logon(	"test"
									,"0.0.0.0"
									, "invalidpassword"
//									, LOGON_CLIENT    // INVALID LOGON TYPE.  AI cannot auth.
									, logger
									, "http://3d-wok.kaneva.com:8080/kepauth/kepauthv5.aspx"
									, 1 // gameid
									, 2 // serverid
									, faultString
									, userInfo )
				, LOGIN_FORCE_OFF );
	EXPECT_STREQ( "Invalid Password", faultString.c_str() );
	EXPECT_STREQ( "", userInfo.c_str() );
}




// Successful auth
//
TEST_F( KEPAuthV5Tests, AuthSuccessTest ) {
	string userInfo;
	string faultString;

	EXPECT_EQ(	KEPAuthV5::logon(	"test"
									, "0.0.0.0"
									, "00000000000000000000000000000000"
//									, LOGON_CLIENT
									, logger
									, "http://pv-wok.kaneva.com:8080/kepauth/kepauthv5.aspx"
									, 3298				// gameid
									, 2					// serverid
									, faultString
									, userInfo )
				, LOGIN_OK );
	EXPECT_STREQ( "", faultString.c_str() );
	EXPECT_STREQ(	"<user><u>4701506</u><r>2</r><g>F</g><b>1979-12-01</b><m>0</m><c>US</c><p>0</p></user>"
					, userInfo.c_str() );
}

// Verify matches existing behavior.  When passing in null url
// returns LOGIN_NOT_FOUND
//
TEST_F( KEPAuthV5Tests, NullURLTest ) {
	string userInfo;
	string faultString;

	EXPECT_EQ(	KEPAuthV5::logon(	"test"
									,"0.0.0.0"
									, "00000000000000000000000000000000"
//									, LOGON_CLIENT
									, logger
									, 0					// NULL auth url
									, 5354				// gameid
									, 2					// serverid
									, faultString
									, userInfo )
				, LOGIN_NOT_FOUND );
	EXPECT_STREQ( "", userInfo.c_str() );
	EXPECT_STREQ( "", faultString.c_str() );
	EXPECT_STREQ( "", userInfo.c_str() );
}

TEST_F( KEPAuthV5Tests, InvalidURLTest01 ) {
	string userInfo;
	string faultString;

	EXPECT_EQ(	KEPAuthV5::logon(	"test"
									,"0.0.0.0"
									, "00000000000000000000000000000000"
//									, LOGON_CLIENT
									, logger
									, "InvalidURL"
									, 5354				// gameid
									, 2					// serverid
									, faultString
									, userInfo )
				, LOGIN_FAILED );
	EXPECT_STREQ( "KEPAuthV5::logon infrastructure failure! status[500] URL[InvalidURL?action=login&username=testuser&password=sanitized&userIpAddress=0.0.0.0&gameId=5354&serverId=2] description[::HttpSendRequest failed. Make sure you have internet connection, and you are not behind an firewall]"
					, faultString.c_str() );
	EXPECT_STREQ( "", userInfo.c_str() );
}

TEST_F( KEPAuthV5Tests, DISABLED_InvalidURLTest02 ) {
	string userInfo;
	string faultString;

	EXPECT_EQ(	KEPAuthV5::logon(	"test"
									,"0.0.0.0"
									, "00000000000000000000000000000000"
//									, LOGON_CLIENT
									, logger
									, "INVALIDURLhttp://asdfasdfadsfa.kaneva.com"
									, 5354				// gameid
									, 2					// serverid
									, faultString
									, userInfo )
				, LOGIN_FAILED );
	EXPECT_STREQ( "KEPAuthV5::logon infrastructure failure! status[500] URL[InvalidURL?action=login&username=testuser&password=sanitized&userIpAddress=0.0.0.0&gameId=5354&serverId=2] description[::HttpSendRequest failed. Make sure you have internet connection, and you are not behind an firewall]"
					, faultString.c_str() );
	EXPECT_STREQ( "", userInfo.c_str() );
}

// Verify matches existing behavior
// Empry url return LOGIN_NOT_FOUND
//
TEST_F( KEPAuthV5Tests, EmptyURLTest ) {
	string userInfo;
	string faultString;

	EXPECT_EQ(	KEPAuthV5::logon(	"test"
									,"0.0.0.0"
									, "00000000000000000000000000000000"
//									, LOGON_CLIENT
									, logger
									, ""				// NULL auth url
									, 5354				// gameid
									, 2					// serverid
									, faultString
									, userInfo )
				, LOGIN_NOT_FOUND );
	EXPECT_STREQ( "", userInfo.c_str() );
	EXPECT_STREQ( "", faultString.c_str() );
	EXPECT_STREQ( "", userInfo.c_str() );
}

// Verify existing behavior
// Passing LOGON_AI returns LOGIN_NOT_FOUND
TEST_F( KEPAuthV5Tests, LoginAITest ) {
	string userInfo;
	string faultString;

	EXPECT_EQ(	KEPAuthV5::logon(	"test"
									,"0.0.0.0"
									, "00000000000000000000000000000000"
//									, LOGON_AI
									, logger
									, "invalid url but not relevant to test"
									, 5354				// gameid
									, 2					// serverid
									, faultString
									, userInfo )
				, LOGIN_NOT_FOUND );
	EXPECT_STREQ( "", userInfo.c_str() );
	EXPECT_STREQ( "", faultString.c_str() );
	EXPECT_STREQ( "", userInfo.c_str() );
}

// Verify existing behavior
// Passing LOGON_AI returns LOGIN_NOT_FOUND
TEST_F( KEPAuthV5Tests, BadGameIdTest ) {
	string userInfo;
	string faultString;

	EXPECT_EQ(	KEPAuthV5::logon(	"test"
									,"0.0.0.0"
									, "00000000000000000000000000000000"
//									, LOGON_CLIENT
									, logger
									, "invalid url but not relevant to test"
									, 0					// gameid
									, 2					// serverid
									, faultString
									, userInfo )
				, LOGIN_NO_PING );
	EXPECT_STREQ( "", userInfo.c_str() );
	EXPECT_STREQ( "", faultString.c_str() );
	EXPECT_STREQ( "", userInfo.c_str() );
}


// Successful auth
//
TEST_F( KEPAuthV5Tests, LogoutTest1 ) {
	string userInfo;
	string faultString;

	// MD5_DIGEST_STR pw;
	// CStringA userName( m_userName );
	// userName.MakeLower();
	// hashString(m_passWord+userName, pw);

	EXPECT_EQ(	KEPAuthV5::logon(	"test"
									, "0.0.0.0"
									, "00000000000000000000000000000000"
//									, LOGON_CLIENT
									, logger
									, "http://pv-wok.kaneva.com:8080/kepauth/kepauthv5.aspx"
									, 3298				// gameid
									, 3					// serverid
									, faultString
									, userInfo )
				, LOGIN_OK );
	EXPECT_STREQ( "", faultString.c_str() );
	EXPECT_STREQ(	"<user><u>4701506</u><r>2</r><g>F</g><b>1979-12-01</b><m>0</m><c>US</c><p>0</p></user>"
					, userInfo.c_str() );
	EXPECT_EQ(	LOGIN_OK,
				KEPAuthV5::logout( 4701506, 1, 3, logger,  "http://pv-wok.kaneva.com:8080/kepauth/kepauthv5.aspx", faultString ) );

//		DISC_NORMAL = 1,	// normal disconnect

}
