///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "common/keputil/URL.h"
#include <Log4CPlus/Logger.h>
#include "tools/networkV2/KEPPingV7.h"
#include "tools/networkV2/Tests/TestConstants.h"

struct PingV7Inputs {
	PingV7Inputs() {
		url.setHost( serviceHost )
		   .setPath( "kepping/keppingv7.aspx" );
	}

	URL url;
};

/**
 * Ping takes many inputs that are sourced from disparate locations
 * Ping also provides many outputs.  To facilitate targeting
 * ping testing at varied environments (e.g. game a in preview 
 * from my laptop, game b in tech preview from your laptop, etc),
 * We configure the ping tests by populating structures that
 * represent these logical sources.  For example, gamekey is stored
 * in a structure that holds information about the game
 * being target by the test.  Another example would be storing
 * clienthostname, the name of the machine hosting the pinger client,
 * in a client data structure, e.g. data about the machine hosting
 * the pinger client.
 * 
 * These logical sources are then used to populate input objects
 * for each of the operations (e.g. auth::auth, ping::getcount etc)
 *
 * Structures that are common between the ping and auth service calls
 * reside in the file TestConstants.h.  Structures that are specific
 * to the function/operations (e.g. ping.ping) reside in the 
 * individual test files (e.g. keppingv7tests.h)
 * 
 * These sources can be switched by a define in TestConstants.h
 */

/**
 * Some pinger inputs never change, these values are held in PingConstData.  
 * If the need should arise that we need to start modifying any of these 
 * values, move it out of this structure to an appropriate structure
 */
struct PingConstData  {
	PingConstData() 
		: watermark( "WATERMARK" )
		, timeoutMS( 100000 )
		, logger( Logger::getInstance( L"PlatformTest" ) )
		, engineType(1)
		, engineTypeStr("1")
	{}

	std::string		watermark;
	unsigned long	timeoutMS;
	Logger			logger;
	unsigned int	engineType;
	std::string		engineTypeStr;
};

/**
 * Information about the location (URL) of the PingService
 */
struct ServiceData {
	ServiceData( LPCSTR  url ) : _serviceEndPoint(URL::parse(url)){}
	URL		_serviceEndPoint;
};




PingConstData constData;		





// Structures for output from various library and rest calls
// Ping instantiation outputs
// Ping updates these variables that are bound when the pinger is 
// instantiated.
//
struct PingInstOutputs {
	bool	adminOnly;
	int		gameId;
	int		parentGameId;
	int		serverId;
	LONG	numberOfPlayers;
	LONG    currentMaxPlayers;
	int		maxAllowedPlayers;
};

/**
 * Ping init operation effectively takes no inputs as it's only argument is never
 * used.  I could have lefter this struct out, but I put it in to underscore
 * the need to refactor the ping() signature to take no arguments.  Possible
 * exception would be to pass an input struct.  That way any future  modifications
 * of the ping action inputs need not change the signature of ping Init().
 */
struct PingInitInputs {
	char*	unused;
};

/**
 * 
 */
struct PingPingInputs {
	PingPingInputs() : serverState( ssRunning ) {}
	char*			unused;
	EServerState	serverState;
};

struct PingCountInputs {
	PingCountInputs() : serverState(ssRunning) {}
	char*			unused;
	EServerState	serverState;
};

// outputs from action=Initialize
struct PingInitOutputs {
	bool		active;
	string		gameName;
	string		hash;
	string		faultString;
	int			newMaxAllowedPlayers;
};

/**
 * Data structure for capturing output from sendPing()
 */
struct PingPingOutputs {
	PingPingOutputs() 
		:	newMaxAllowedPlayers(99)
			, currentMaxPlayers(99)
			, numberOfPlayers(0)
			, maxAllowedPlayers(99)
			, hash()							// not required.  Initialized 
			, faultString()
	{}
	int		newMaxAllowedPlayers;
	LONG    currentMaxPlayers;
	LONG	numberOfPlayers;
	int		maxAllowedPlayers;
	string	hash;												// not required.  Initialized 
	string	faultString;
};

struct PingCountOutputs {
	PingCountOutputs() 	{}
	int		newMaxAllowedPlayers; //	= 99;
	string	result;												// not required.  Initialized 
	string	faultString;
	bool	active; //					= false;
};

// Here we instantiate GameData structures for some 
// servers of interest.
//
#ifdef TARGET_TECHPREVIEW
	ServiceData			serviceData( "http://3d-wok.kaneva.com:8080/kepping/KEPPingV7.aspx" );

	// Data specific to the suject client machine
#endif

// Inputs for instantiation of KEPPingV7 objects
//
struct PingInstInputs {
	PingInstInputs( URL				endPoint
					, LPCSTR		clientHost 
					, LPCSTR		clientIP
					, LPCSTR		gameKey
					, LPCSTR		/*gameHash*/ ) 
		:	_endPoint(endPoint)
			, timeoutMS(	constData.timeoutMS )
			, _clientHost( clientHost )
			, _clientIP( clientIP )
			, _gameKey( gameKey )
			, _sClientPort(			clientData._port )
			, _protocolVersion(		clientData._protocolVersion )
			, _lProtocolVersion(	clientData._lProtocolVersion )
			, _lSchemaVersion(		clientData._lSchemaVersion )
			, _schemaVersion(		clientData._schemaVersion )
			, _serverVersion(		clientData._serverVersion )
			, _assetVersion(		clientData._assetVersion  )
			, _lAssetVersion(		clientData._lAssetVersion )
			, _waterMark(			constData.watermark )
			, logger(				constData.logger )
			, engineType(			constData.engineType )
			, engineTypeStr(		constData.engineTypeStr )
	{
		stringstream ss;
		ss << _sClientPort;
		_clientPort = ss.str();
	}

	URL				_endPoint;
	std::string		_clientHost;		//			= "test-server";
	std::string		_clientIP;			//		= "localhost";
	std::string		_gameKey;			//			= "529500123456789abcdef";

	unsigned short	_sClientPort;
	std::string		_clientPort;

	std::string		_protocolVersion;
	unsigned long	_lProtocolVersion;

	std::string		_schemaVersion;
	unsigned long	_lSchemaVersion;


	std::string		_serverVersion;

	std::string		_assetVersion;
	unsigned long	_lAssetVersion;

	std::string		_waterMark;
	unsigned int	engineType;
	std::string		engineTypeStr;

	unsigned long	    timeoutMS;
	log4cplus::Logger	logger;
};




PingInstInputs		instInputs(	serviceData._serviceEndPoint
								, clientData._hostname.c_str()
								, clientData._hostip.c_str()
								, gameData._gameKey.c_str()
								, gameData._initHash.c_str() );




class PingLiveTests : public ::testing::Test {
public:
	PingLiveTests() :  ::testing::Test()	
	{}

	bool callURL( URL&		url, Response& p ) {
		try {
			GetBrowserPageOutputs browserOutputs;
			size_t resultSize = 0;
			bool ret = GetBrowserPage(	
				url.toString()
				, browserOutputs.resultText
				, resultSize
				, browserOutputs.statusCode
				, browserOutputs.statusMessage
			);
			EXPECT_TRUE( ret )	<< "Unexpected failure [" 
							<< browserOutputs.statusCode 
							<< "][" << browserOutputs.statusMessage.c_str() 
							<< "] calling url[" 
							<< url.toString().c_str() << "]";
			if ( !ret ) return ret;

			p.parse( browserOutputs.resultText );
		}
		catch( GeneralizedResponseParser::Exception& ) {
			return false;
		}
		return true;
	}
};

class PingV7PingLiveTests : public PingLiveTests {
public:
	PingV7PingLiveTests() 
		:	PingLiveTests()	
	{}

	URL _initURL;
	URL _pingURL;
	URL _getCountURL;

	virtual void SetUp()				{
		PingV7Inputs inputs;
		_initURL = inputs.url;
		_initURL.setParam("action",					"Initialize" )
				.setParam( "key",					instInputs._gameKey )
				.setParam( "hostname",				instInputs._clientHost	)
				.setParam( "watermark",				instInputs._waterMark )
				.setParam( "type",					instInputs.engineTypeStr )
				.setParam( "ipAddress",				instInputs._clientIP )
				.setParam( "port",					instInputs._clientPort )
				.setParam( "protocolVersion",		instInputs._protocolVersion )
				.setParam( "schemaVersion",			instInputs._schemaVersion )
				.setParam( "serverVersion",			instInputs._serverVersion )
				.setParam( "assetVersion",			instInputs._assetVersion );
		_pingURL = inputs.url;
		_pingURL.setParam( "action",				"Ping" )
				.setParam( "serverId",				gameData._serverIdStr )
				.setParam( "port",					clientData._sport.c_str() )
				.setParam( "statusId",				"1" )
				.setParam( "numberOfPlayers",		"5" )
				.setParam( "maxNumberOfPlayers",	"100" )
				.setParam( "adminOnly",				"false" );
		_getCountURL = inputs.url;
		_getCountURL.setParam( "action",			"GetCount" )
					.setParam( "key",				gameData._gameKey )
					.setParam( "serverId",			gameData._serverIdStr )
					.setParam( "port",				clientData._sport )
					.setParam( "statusId",			"1" )
					.setParam( "numberOfPlayers",	"5" )
					.setParam( "adminOnly",			"false" );

//		KEPPing/KEPPingV6.aspx?key=mykey&action=GetCount&serverId=1&port=1&statusId=1&numberOfPlayers=5&adminOnly=false
//		_pingUrlKEPPing/KEPPingV6.aspx?action=Ping&serverId=1&port=1&statusId=1&numberOfPlayers=5&maxNumberOfPlayers=100&adminOnly=false

		GetBrowserPageOutputs	browserOutputs;
		// cout << "CALL: " << _initURL.toString() << endl;
		size_t resultSize = 0;
		ASSERT_TRUE(GetBrowserPage( 
			_initURL.toString().c_str()
			, browserOutputs.resultText
			, resultSize
			, browserOutputs.statusCode
			, browserOutputs.statusMessage
		) )
						<< "Unexpected failure [" << browserOutputs.statusCode << "][" << browserOutputs.statusMessage.c_str() << "] calling url[" << _initURL.toString().c_str() << "]";
		// cout << "RESP: " << browserOutputs.resultText << endl;

		PingInitializeResponse response;
		response.parse( browserOutputs.resultText );

		EXPECT_TRUE(		response.status() );
		EXPECT_EQ(			0,							response.code() );
		EXPECT_STREQ(		"OK",						response.message().c_str() );
		EXPECT_EQ(			gameData._serverId,			response.serverId() );
		EXPECT_EQ(			gameData._gameId,			response.gameId() );
		EXPECT_EQ(			2000,						response.maxCount() );		
		EXPECT_STREQ(		gameData._gameName.c_str(),	response.gameName().c_str() );
		EXPECT_EQ(			gameData._parentGameId,		response.parentGameId() );
		EXPECT_STREQ(		gameData._initHash.c_str(),	response.hash().c_str() );
	}

	virtual void TearDown()				{}
};



// Test fixure for V7 Initialization tests.  Builds up init url
//
class PingV7InitializeLiveTests : public PingLiveTests { // ::testing::Test {
public:
	PingV7InitializeLiveTests() 
		: PingLiveTests()	
	{}

	URL _initURL;
	virtual void SetUp()				{
		PingV7Inputs inputs;
		_initURL = inputs.url;
		_initURL.setParam("action",			"Initialize" )
				.setParam( "key",				gameData._gameKey )
				.setParam( "hostname",			clientData._hostname  )
				.setParam( "watermark",			instInputs._waterMark )
				.setParam( "type",				instInputs.engineTypeStr )
				.setParam( "ipAddress",			clientData._hostip )
				.setParam( "port",				clientData._sport )
				.setParam( "protocolVersion",	instInputs._protocolVersion )		// This should initialize from clientData
				.setParam( "schemaVersion",		instInputs._schemaVersion )
				.setParam( "serverVersion",		instInputs._serverVersion )
				.setParam( "assetVersion",		instInputs._assetVersion );
	}

	virtual void TearDown()				{}
};




TEST_F( PingV7InitializeLiveTests, DISABLED_VolatileInitSuccessTest )							{
	PingInitializeResponse actualInitResponse;
	callURL( _initURL, actualInitResponse );
	EXPECT_TRUE( actualInitResponse.status() );
	EXPECT_EQ(			0,							actualInitResponse.code() );
	EXPECT_STREQ(		"OK",						actualInitResponse.message().c_str() );
	EXPECT_EQ(			2000,						actualInitResponse.maxCount() );		
	EXPECT_EQ(			gameData._serverId,			actualInitResponse.serverId() );
	EXPECT_EQ(			gameData._gameId, 			actualInitResponse.gameId() );
	EXPECT_STREQ(		gameData._gameName.c_str(),	actualInitResponse.gameName().c_str() );
	EXPECT_EQ(			gameData._parentGameId,		actualInitResponse.parentGameId() );
	EXPECT_STREQ(		gameData._initHash.c_str(),	actualInitResponse.hash().c_str() );
}

TEST_F( PingV7InitializeLiveTests, DISABLED_VolatileInitMissingParamTest )							{
	PingInitializeResponse response;
	callURL( 	_initURL.unsetParam( "protocolVersion" ), response );
	EXPECT_EQ(			-1,			response.code() );
	EXPECT_STREQ(		"key, hostname, type, ipAddress, port, protocolVersion, schemaVersion, serverVersion, or assetVersion param not specified"
							, response.message().c_str() );
	EXPECT_EQ(			0,		response.serverId() );
	EXPECT_EQ(			0,		response.gameId() );
	EXPECT_EQ(			0,		response.maxCount() );		
	EXPECT_STREQ(		"",	response.gameName().c_str() );
	EXPECT_EQ(			0,			response.parentGameId() );
	EXPECT_FALSE( response.status() );
	EXPECT_STREQ(		"", response.hash().c_str() );
}

TEST_F( PingV7InitializeLiveTests, DISABLED_VolatileBadKeyActionTest )							{
	PingInitializeResponse response;
	try {
		callURL(_initURL.setParam( "key", "adfadadfafa" ), response );

		EXPECT_EQ(			4,								response.code() );
		EXPECT_STREQ(		"Server not found for hostname: "
							,	response.message().substr(0,31).c_str() );
		EXPECT_EQ(			0,								response.serverId() );
		EXPECT_EQ(			0,								response.gameId() );
		EXPECT_EQ(			1,								response.maxCount() );		
		EXPECT_STREQ(		"",								response.gameName().c_str() );
		EXPECT_EQ(			0,								response.parentGameId() );
		EXPECT_TRUE( response.status() );
		EXPECT_STREQ(		"",								response.hash().c_str() );
	} 
	catch( const ChainedException& exc ) {
		FAIL() << "Test failed with exception: " << exc.toString().c_str() << endl;
	}
}

TEST_F( PingV7InitializeLiveTests, DISABLED_VolatileBadHostNameTest )							{
	try {
		PingInitializeResponse response;
		callURL( _initURL.setParam( "hostname", "adfadadfafa" ), response );
		EXPECT_TRUE( response.status() );
		std::stringstream ss;
		ss << "Server not found for hostname: adfadadfafa at: localhost:" << clientData._port << ". Probably not registered";
		EXPECT_STREQ(		ss.str().c_str(),			response.message().c_str() );
		EXPECT_EQ(			4,							response.code() );
		EXPECT_EQ(			0,							response.serverId() );
		EXPECT_EQ(			0,							response.gameId() );
		EXPECT_EQ(			2000,						response.maxCount() );		
		EXPECT_EQ(			gameData._parentGameId,		response.parentGameId() );
		EXPECT_STREQ(		"",							response.hash().c_str() );
		EXPECT_STREQ(		gameData._gameName.c_str(),	response.gameName().c_str() );
	} 
	catch( KEPPingException& )	{	}
}

// This call succeeds.  It should fail due to the protocolVersion.
//
TEST_F( PingV7InitializeLiveTests, DISABLED_VolatileBadProtocolVersionTest )							{
	try {
		PingInitializeResponse response;
		callURL( _initURL.setParam( "protocolVersion", "99" ), response );
		ASSERT_EQ(		    gameData._parentGameId,			response.parentGameId() );
		if ( gameData._parentGameId == 0 )		{
			EXPECT_EQ(			0,								response.code() );			
			EXPECT_STREQ(		"OK"
							, response.message().c_str() );
			// redundant as we've already tested this.
			// EXPECT_EQ(			gameData._parentGameId,								response.serverId() );
			EXPECT_EQ(			gameData._gameId,				response.gameId() );
			EXPECT_EQ(			2000,							response.maxCount() );		
			EXPECT_STREQ(		gameData._gameName.c_str(),		response.gameName().c_str() );
			EXPECT_STREQ(		gameData._initHash.c_str(),		response.hash().c_str() );
			EXPECT_TRUE( response.status() );
		}
		else {
			EXPECT_EQ(			6,								response.code() );
			EXPECT_STREQ(		"Parent game's protcol version doesn't match this one's"
								, response.message().c_str() );
			EXPECT_EQ(			0,								response.serverId() );
			EXPECT_EQ(			0,								response.gameId() );
			EXPECT_EQ(			1,								response.maxCount() );		
			EXPECT_STREQ(		"",								response.gameName().c_str() );
			EXPECT_STREQ(		"",								response.hash().c_str() );
			EXPECT_TRUE( response.status() );
		}
	} 
	catch( KEPPingException& )	{	}
}

TEST_F( PingV7InitializeLiveTests, DISABLED_VolatileInvalidActionTest )							{
	try {
		PingInitializeResponse response;
		callURL(_initURL.setParam( "action", "invalidaction" ), response );
		EXPECT_EQ(			-2,					response.code() ) << "TODO: THIS SHOULD RETURN NON-ZERO";
		EXPECT_STREQ(		"Unknown Action",	response.message().c_str() );
		EXPECT_EQ(			0,					response.serverId() );
		EXPECT_EQ(			0,					response.gameId() );
		EXPECT_EQ(			0,					response.maxCount() );		
		EXPECT_STREQ(		"",					response.gameName().c_str() );
		EXPECT_EQ(			0,					response.parentGameId() );
		EXPECT_FALSE( response.status() );
		EXPECT_STREQ(		"",					response.hash().c_str() );
	} 
	catch( KEPPingException& )	{	}
}

TEST_F( PingV7InitializeLiveTests, DISABLED_VolatileMissingActionTest )							{
	try {
		PingInitializeResponse response;
		callURL( _initURL.unsetParam( "action" ), response );
		EXPECT_EQ(			-1,					response.code() ) << "TODO: THIS SHOULD RETURN NON-ZERO";
		EXPECT_STREQ(		"action param not specified",	response.message().c_str() );
		EXPECT_EQ(			0,					response.serverId() );
		EXPECT_EQ(			0,					response.gameId() );
		EXPECT_EQ(			0,					response.maxCount() );		
		EXPECT_STREQ(		"",					response.gameName().c_str() );
		EXPECT_EQ(			0,					response.parentGameId() );
		EXPECT_FALSE( response.status() );
		EXPECT_STREQ(		"",					response.hash().c_str() );
	} 
	catch( KEPPingException& )	{	}
}


////// Test Ping function via direct call to web service
//////
#if 1

TEST_F( PingV7PingLiveTests, DISABLED_VolatilePingSuccessTest )							{
	try {
		PingPingResponse response;
		callURL( _pingURL, response );
		ASSERT_EQ(		0,								response.code() );
		EXPECT_STREQ(	"OK",							response.message().c_str() );
		EXPECT_STREQ(	gameData._pingHash.c_str(),		response.hash().c_str() );
	} 
	catch( KEPPingException& )	{	}
}

TEST_F( PingV7PingLiveTests, DISABLED_VolatilePingBadParamTest )							{
	try {
		PingPingResponse response;
		callURL( _pingURL.unsetParam( "serverId" ), response );
		EXPECT_EQ(		-1,									response.code() );
		EXPECT_STREQ(	"serverId, port, statusId, numberOfPlayers, maxNumberOfPlayers, or adminOnly param not specified"
						, response.message().c_str() );
		EXPECT_STREQ(	"", response.hash().c_str() );
	} 
	catch( KEPPingException& ) {	}
}

/**
 * This message is pointless, since we would never be able to init if the ip was invalid
 */
TEST_F( PingV7PingLiveTests, DISABLED_DISABLED_InvalidIPTest )							{
	try {
		PingPingResponse response;
		callURL(_pingURL.setParam( "ipAddress", "asdfasd" ), response );
		EXPECT_EQ(		-1,									response.code() );
		EXPECT_STREQ(	"serverId, port, statusId, numberOfPlayers, maxNumberOfPlayers, or adminOnly param not specified"
						, response.message().c_str() );
		EXPECT_STREQ(	"", response.hash().c_str() );
	} 
	catch( KEPPingException& ) {	}
}

//			"<Result code="0">
//<Description>OK</Description>
//<Data  maxNumberOfPlayers="2000"  status="True">8f9593ea20c403f5a8afec655b9c8f18</Data></Result>"
//			EXPECT_STREQ(	"", response.hash().c_str() );
TEST_F( PingV7PingLiveTests, DISABLED_VolatileGetCountSuccessTest )							{
	try {
		PingGetCountResponse response;
		callURL( _getCountURL, response );
		ASSERT_TRUE(	response.status() );
		EXPECT_EQ(		0,							response.code() );
		EXPECT_STREQ(	"OK",						response.message().c_str() );
		EXPECT_EQ(		2000,						response.maxPlayers() );
		EXPECT_STREQ(	gameData._pingHash.c_str(),	response.hash().c_str() );
	} 
	catch( KEPPingException& ) {	}
}

TEST_F( PingV7PingLiveTests, DISABLED_VolatileGetCountMissingParamTest )							{
	try {
		PingGetCountResponse response;
		callURL( _getCountURL.unsetParam( "serverId" ), response );
		EXPECT_EQ(		-1,			response.code() );
		EXPECT_STREQ(	"key, serverId, port, statusId, numberOfPlayers, or adminOnly param not specified", response.message().c_str() );
	} 
	catch( const ChainedException& exc ) {		
		FAIL() << "Test failed with exception: " << exc.toString().c_str() << endl << "Is web service current?" << endl;
	}
}

// All of these tests should run on v7 as well as they do v6.  So pulling those tests forward
// todo: Test escalation on multiple auth failures
// todo: Test ssl
//
// There is no value in testing for missing or unknown action since KEPPingV6 always assigns the correct action
TEST( PingV7PingerTests, DISABLED_VolatileInitSuccessTest ) {
	int		gameId					= 99;								// not required.  Set by init call
	int		parentGameId			= 99;								// not required.  Set by init call
	int		serverId				= 99;								// not required.  Set by init call
	string	gameName				= "99";								// not required.  Set by init call
	int		newMaxAllowedPlayers	= 99;
	string	result;														// not required.  Initialized 
	string	faultString;
	bool	active					= false;
	char*	s						= "not used";

	PingInstOutputs instOutputs;

	KEPPingV7 p(	instInputs.logger
					, gameId											// gameid
					, parentGameId										// parent game id
					, serverId											// server id
					, serviceData._serviceEndPoint.toString()			// ping server url
					, gameData._gameKey									// reg key
					, clientData._hostname								// hostname
					, instInputs._waterMark								// watermark
					, instInputs.engineType								// engine type
					, clientData._hostip								// ipaddress
					, clientData._port									// port
					, instInputs._lProtocolVersion						// protocol version
					, instInputs._lSchemaVersion						// schema version
					, instInputs._serverVersion							// server version
					, instInputs._lAssetVersion							// asset version
					, instInputs.timeoutMS								// timout ms	
					, instOutputs.numberOfPlayers
					, instOutputs.currentMaxPlayers
					, instOutputs.maxAllowedPlayers
					, instOutputs.adminOnly					);

	EXPECT_EQ( S_OK, p.sendInitialize(	s
										, newMaxAllowedPlayers
										, active
										, gameName
										, result
										, faultString ) );

	EXPECT_TRUE( active );
	EXPECT_STREQ( "",							faultString.c_str() );
	EXPECT_STREQ( gameData._initHash.c_str(),	result.c_str() );
	EXPECT_STREQ( gameData._gameName.c_str(),	gameName.c_str() );
	EXPECT_EQ( 2000,							newMaxAllowedPlayers );
	EXPECT_EQ( gameData._gameId,				gameId );
	EXPECT_EQ( gameData._parentGameId,			parentGameId );
	EXPECT_EQ( gameData._serverId,				serverId );
}


TEST( KEPPingV7PingerTests, DISABLED_VolatileInitFailureTest ) {
	PingInstOutputs instOutputs = {0};
	PingInitOutputs initOutputs = {0};
	PingInitInputs  initInputs = {0};

	// Now that we know what is required.
	// lets set values here and see if they get set to anything on failure.
	//
	KEPPingV7 p(	instInputs.logger
					, instOutputs.gameId													// gameid
					, instOutputs.parentGameId												// parent game id
					, instOutputs.serverId													// server id
					, serviceData._serviceEndPoint.toString()			// ping server url
					, gameData._gameKey													// reg key
					, "xtest-server"											// hostname
					, instInputs._waterMark												// watermark
					, instInputs.engineType															// engine type
					, clientData._hostip										// ipaddress
					, clientData._port											// port
					, instInputs._lProtocolVersion
					, instInputs._lSchemaVersion
					, instInputs._serverVersion
					, instInputs._lAssetVersion
					, instInputs.timeoutMS
					, instOutputs.numberOfPlayers
					, instOutputs.currentMaxPlayers
					, instOutputs.maxAllowedPlayers
					, instOutputs.adminOnly					);

	EXPECT_FALSE( S_OK == p.sendInitialize(	initInputs.unused
											, initOutputs.newMaxAllowedPlayers
											, initOutputs.active
											, initOutputs.gameName
											, initOutputs.hash
											, initOutputs.faultString ) );

	// todo: make this vary with the environment _clientIP:_clientPort values

	stringstream ss;
	ss << "Server not found for hostname: xtest-server at: localhost:" << clientData._port << ". Probably not registered";
	
	EXPECT_STREQ( ss.str().c_str(), initOutputs.faultString.c_str() );
	EXPECT_STREQ( "", initOutputs.hash.c_str() );
	EXPECT_STREQ( "", initOutputs.gameName.c_str() );
	EXPECT_FALSE( initOutputs.active );
	EXPECT_EQ( 1, initOutputs.newMaxAllowedPlayers );
	EXPECT_EQ( 0, instOutputs.gameId );
	EXPECT_EQ( 0, instOutputs.parentGameId );
	EXPECT_EQ( 0, instOutputs.serverId );
}

TEST( KEPPingV7PingerTests, DISABLED_VolatilePingSuccessTest ) {
	try {
		PingInstOutputs	instOutputs;
		KEPPingV7 p(	instInputs.logger
						, instOutputs.gameId
						, instOutputs.parentGameId
						, instOutputs.serverId
						, instInputs._endPoint.toString().c_str()
						, instInputs._gameKey
						, instInputs._clientHost
						, instInputs._waterMark
						, instInputs.engineType
						, instInputs._clientIP
						, instInputs._sClientPort
						, instInputs._lProtocolVersion
						, instInputs._lSchemaVersion
						, instInputs._serverVersion
						, instInputs._lAssetVersion
						, instInputs.timeoutMS
						, instOutputs.numberOfPlayers
						, instOutputs.currentMaxPlayers
						, instOutputs.maxAllowedPlayers
						, instOutputs.adminOnly				);

		PingInitOutputs initOutputs = {0};
		PingInitInputs  initInputs = {0};
		EXPECT_EQ( S_OK, p.sendInitialize(	initInputs.unused
											, initOutputs.newMaxAllowedPlayers
											, initOutputs.active
											, initOutputs.gameName
											, initOutputs.hash
											, initOutputs.faultString ) );

		EXPECT_TRUE( initOutputs.active );
		EXPECT_STREQ(	"",							initOutputs.faultString.c_str() );
		EXPECT_STREQ(	gameData._initHash.c_str(),	initOutputs.hash.c_str() );
		EXPECT_STREQ(	gameData._gameName.c_str(),	initOutputs.gameName.c_str() );
		EXPECT_EQ(		2000,						initOutputs.newMaxAllowedPlayers );
		EXPECT_EQ(		gameData._gameId,			instOutputs.gameId );
		EXPECT_EQ(		gameData._parentGameId,		instOutputs.parentGameId );
		EXPECT_EQ(		gameData._serverId,			instOutputs.serverId );

		PingPingInputs	pingInputs;
		PingPingOutputs	pingOutputs;
		EXPECT_EQ( S_OK, p.sendPing(	pingInputs.serverState
										, pingInputs.unused // likely never used
										, pingOutputs.hash
										, pingOutputs.faultString ) );
		EXPECT_STREQ( "",				pingOutputs.faultString.c_str() );

		// WTF?  SEND PING RETURNS COUNT HASH?
		EXPECT_STREQ( gameData._countHash.c_str(),		pingOutputs.hash.c_str() );
	}
	catch( const ChainedException& exc ) {
		FAIL() << "Test failed with exception:\n" << exc.toString().c_str() << endl;
	}
}
#endif

TEST( PingV7PingerTests, DISABLED_VolatileGetCountSuccessTest ) {
	PingInstOutputs	instOutputs;
	try {
		KEPPingV7 p(	instInputs.logger
						, instOutputs.gameId						// REFERENCE gameid			Will be populated during Initialize
						, instOutputs.parentGameId					// REFERENCE parent game id Will be populated during Initialize
						, instOutputs.serverId						// server id				Will be populated during Initialize
						, instInputs._endPoint.toString().c_str()	// ping server url
						, instInputs._gameKey						// reg key
						, instInputs._clientHost					// hostname
						, instInputs._waterMark						// watermark
						, instInputs.engineType						// engine type
						, instInputs._clientIP						// ipaddress
						, instInputs._sClientPort					// port
						, instInputs._lProtocolVersion				// protocol version
						, instInputs._lSchemaVersion				// schema version
						, instInputs._serverVersion					// server version
						, instInputs._lAssetVersion					// asset version
						, instInputs.timeoutMS						// timout ms	
						, instOutputs.numberOfPlayers				// REFERENCE
						, instOutputs.currentMaxPlayers				// REFERENCE
						, instOutputs.maxAllowedPlayers				// REFERENCE
						, instOutputs.adminOnly			);			// REFERENCE Not modified by KEPPingV6, but modified externally

		PingInitInputs  initInputs = {0};
		PingInitOutputs initOutputs = {0};

		EXPECT_EQ( S_OK, p.sendInitialize(	initInputs.unused
											, initOutputs.newMaxAllowedPlayers
											, initOutputs.active
											, initOutputs.gameName
											, initOutputs.hash
											, initOutputs.faultString ) );

		EXPECT_TRUE(	initOutputs.active );
		EXPECT_STREQ(	"",									initOutputs.faultString.c_str() );
		EXPECT_STREQ(	gameData._initHash.c_str(),		initOutputs.hash.c_str() );
		EXPECT_STREQ(	gameData._gameName.c_str(),	initOutputs.gameName.c_str() );
		EXPECT_EQ(		2000,						initOutputs.newMaxAllowedPlayers );
		EXPECT_EQ(		gameData._gameId,			instOutputs.gameId );
		EXPECT_EQ(		gameData._parentGameId,		instOutputs.parentGameId );
		EXPECT_EQ(		gameData._serverId,			instOutputs.serverId );

		initOutputs.active				= false;
		instOutputs.maxAllowedPlayers	= 99;



		PingCountInputs countInputs;
		PingCountOutputs countOutputs;
 		EXPECT_EQ( S_OK, p.sendGetCount(	countInputs.serverState
											, countInputs.unused
											, countOutputs.newMaxAllowedPlayers
											, countOutputs.active
											, countOutputs.result
											, countOutputs.faultString ) );

		EXPECT_TRUE( countOutputs.active );
		EXPECT_EQ(		2000,								countOutputs.newMaxAllowedPlayers );
		EXPECT_STREQ(	gameData._countHash.c_str(),			countOutputs.result.c_str() );
		EXPECT_STREQ(	"",									countOutputs.faultString.c_str() );
	}
	catch( const ChainedException& exc ) {
		FAIL() << "Test failed with exception:\n" << exc.toString().c_str() << endl;
	}
}

// Disabled - Ping failure due to can be replicated here as changing the ip will simply cause
// the init to fail.
//
TEST( KEPPingV7Tests, DISABLED_DISABLED_PingInvalidIpTest ) {
	PingInstOutputs instOutputs;
	KEPPingV7 p(	instInputs.logger
					, instOutputs.gameId
					, instOutputs.parentGameId
					, instOutputs.serverId
					, instInputs._endPoint.toString().c_str()
					, instInputs._gameKey
					, "xtest-server"			// bad hostname, or at least it for this test to work, it should be.
					, instInputs._waterMark
					, instInputs.engineType
					, instInputs._clientIP
					, instInputs._sClientPort
					, instInputs._lProtocolVersion
					, instInputs._lSchemaVersion
					, instInputs._serverVersion
					, instInputs._lAssetVersion
					, instInputs.timeoutMS
					, instOutputs.numberOfPlayers
					, instOutputs.currentMaxPlayers
					, instOutputs.maxAllowedPlayers
					, instOutputs.adminOnly			  );

	PingInitInputs  initInputs = {0};
	PingInitOutputs initOutputs = {0};
	EXPECT_EQ( S_OK, p.sendInitialize(	initInputs.unused
										, initOutputs.newMaxAllowedPlayers
										, initOutputs.active
										, initOutputs.gameName
										, initOutputs.hash
										, initOutputs.faultString ) );

	EXPECT_TRUE(	initOutputs.active );
	EXPECT_STREQ(	"",							initOutputs.faultString.c_str() );
	EXPECT_STREQ(	gameData._initHash.c_str(),	initOutputs.hash.c_str() );  // should get determinant value back for this.
	EXPECT_STREQ(	gameData._gameName.c_str(),	initOutputs.gameName.c_str() );

	// Note that these params were introduced by reference when the pinger was instantiated.  Note that this only happens once,
	// so it begs the question, why are doing this this way? (as opposed to simply using accessor methods on the pinger.)
	// The only scenario that would require this is places where we are checking these params by reference rather than accessing
	// the pinger.  Scary really.
	//
	EXPECT_EQ(		2000,						initOutputs.newMaxAllowedPlayers	);
	EXPECT_EQ(		gameData._gameId,			instOutputs.gameId					);
	EXPECT_EQ(		gameData._parentGameId,		instOutputs.parentGameId			);
	EXPECT_EQ(		gameData._serverId,			instOutputs.serverId				);

	PingPingOutputs pingOutputs;
	PingPingInputs  pingInputs;
 	EXPECT_EQ( S_OK, p.sendPing(			pingInputs.serverState
											, pingInputs.unused
											, pingOutputs.hash
											, pingOutputs.faultString ) );

	EXPECT_STREQ(	gameData._initHash.c_str(),	pingOutputs.hash.c_str() );
	EXPECT_STREQ(	"",							pingOutputs.faultString.c_str() );  // "serverId, port, statusId, numberOfPlayers, maxNumberOfPlayers, or adminOnly param not specified"
}


////// Parser Test.  These do nothing but test the parser
////// It doesn't communicate with the Ping service.
//////
TEST( PingV7ParserTests, GeneralizedResponseParserTest01 )	{
	std::stringstream			ss;

	ss  << "<Result code=\"6\">"
		<<		"<Description>ServerId not found</Description>"
		<<		"<Data maxNumberOfPlayers=\"0\"  status=\"False\"/>"
		<< "</Result>";
	try {
		GeneralizedResponse::GeneralizedResponsePtr pResp = GeneralizedResponseParser::parse(ss.str());
		EXPECT_EQ( 6,														pResp->_returnCode );
		EXPECT_STREQ( "ServerId not found",									pResp->_message.c_str() );
		EXPECT_STREQ( "<Data maxNumberOfPlayers=\"0\" status=\"False\" />", pResp->_data .c_str() );
	}
	catch( const ChainedException& exc ) {
		FAIL() << "Test failed with exception: " << exc.toString().c_str() << endl;
	}
}

TEST( PingV7ParserTests, GeneralizedResponseParserTest02 )	{
	try {
		std::stringstream			ss;
		ss  << "<Result code=\"6\">"
			<<		"<Description>ServerId not found</Description>"
			<< "</Result>";

		GeneralizedResponse::GeneralizedResponsePtr pResp = GeneralizedResponseParser::parse(ss.str());
		EXPECT_EQ(		6,						pResp->_returnCode );
		EXPECT_STREQ(	"ServerId not found",	pResp->_message.c_str() );
		EXPECT_STREQ(	"",						pResp->_data .c_str() );
	}
	catch( const ChainedException& exc ) {
		FAIL() << "Test failed with exception: " << exc.toString().c_str() << endl;
	}
}

/**
 * Assert that a standard response, missing the Description
 * element throws a GeneralizedResponseParser::Exception
 */
TEST( PingV7ParserTests, DISABLED_GeneralizedResponseParserTest03 )	{
#if 1
    FAIL() << "Fix this test.  Raises RTE";
#else
	try {
		GeneralizedResponseParser p;
		GeneralizedResponse::GeneralizedResponsePtr pResp = p.parse("<Result code=\"6\"/>");
		FAIL() << "Failed to raise expected exception\n";
	}
	catch ( GeneralizedResponseParser::Exception& )		{ 
		// Fully expect this as response spec states that Description element must
		// be present.
	}
#endif
}

TEST( PingV7ParserTests, PingInitializeTest01 )						{
	std::stringstream ss;
	ss	<< "<Result code=\"0\">"
		<<		"<Description>OK</Description>"
		<<		"<Data serverId=\"4195\" maxCount=\"2000\"  status=\"True\"  gameId=\"8340\"  parentGameId=\"5354\"  gameName=\"ringo_0_test_server_601640819\">"
		<<			"00000000000000000000000000000000"
		<<		"</Data>"
		<< "</Result>";
	try {
		PingInitializeResponse response;
		response.parse( ss.str() );
	}
	catch( const ChainedException& exc ) {
		FAIL() << "Test failed with exception: " << exc.toString().c_str() << endl;
	}
}

TEST( PingV7ParserTests, InitSuccessTest ) {
	try {
		std::stringstream ss;
		ss	<< "<Result code=\"0\">"
			<< "<Description>OK</Description>"
			<< "<Data serverId=\"4195\" maxCount=\"2000\"  status=\"True\"  gameId=\"8340\"  parentGameId=\"5354\"  gameName=\"ringo_0_test_server_601640819\">00000000000000000000000000000000</Data>"
			<< "</Result>";

		PingInitializeResponse response;
		response.parse( ss.str() );
		EXPECT_EQ(			0,									response.code() );
		EXPECT_STREQ(		"OK",								response.message().c_str() );
		EXPECT_EQ(			4195,								response.serverId() );
		EXPECT_EQ(			8340,								response.gameId() );
		EXPECT_EQ(			2000,								response.maxCount() );		
		EXPECT_STREQ(		"ringo_0_test_server_601640819",	response.gameName().c_str() );
		EXPECT_EQ(			5354,								response.parentGameId() );
		EXPECT_TRUE( response.status() );
		EXPECT_STREQ( "00000000000000000000000000000000",		response.hash().c_str() );
	}
	catch( const ChainedException& exc ) {
		FAIL() << "Test failed with exception: " << exc.toString().c_str() << endl;
	}
}

TEST( PingV7ParserTests, MissingParamTest ) {
	try {
		std::stringstream ss;
		ss  << "<Result code=\"-1\">"
			<< "<Description>key, hostname, type, ipAddress, port, protocolVersion, schemaVersion, serverVersion, or assetVersion param not specified</Description>"
			<< "</Result>";

		PingInitializeResponse response;
		response.parse( ss.str() );
		EXPECT_EQ(			-1,			response.code() );
		EXPECT_STREQ(		"key, hostname, type, ipAddress, port, protocolVersion, schemaVersion, serverVersion, or assetVersion param not specified"
						, response.message().c_str() );
		EXPECT_EQ(			0,			response.serverId() );
		EXPECT_EQ(			0,			response.gameId() );
		EXPECT_EQ(			0,			response.maxCount() );		
		EXPECT_STREQ(		"",			response.gameName().c_str() );
		EXPECT_EQ(			0,			response.parentGameId() );
		EXPECT_FALSE(		response.status() );
		EXPECT_STREQ(		"", response.hash().c_str() );
	}
	catch( const ChainedException& exc ) {
		FAIL() << "Test failed with exception: " << exc.toString().c_str() << endl;
	}
}
