///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#ifndef __TESTCONSTANTS_H__
#define __TESTCONSTANTS_H__
#include "Common\KEPUtil\URL.h"

#define NewMode

// Switch environments here.
// enable/disable the following defines as needed
#define TARGET_PREVIEW
//#define TARGET_TECHPREVIEW


/**
 * Collect  information about the client and the client environment.
 * This data includes more than just info about the physical host.
 * It also contains information about the host's kaneva environment
 * (e.g. schema version, protocol version etc.)
 */
struct ClientData {
	ClientData( const std::string& hostname
				, const std::string& hostip
				, unsigned short port ) 
		:	_hostname(hostname)
			, _hostip(hostip)
			, _port(port)  
			, _protocolVersion(		"32806"			)
			, _lProtocolVersion(	32806			)
			, _lSchemaVersion(		130				)
			, _schemaVersion(		"130"			)
			, _serverVersion(		"4.0.9.1421"	)
			, _assetVersion(		"33554432"		)
			, _lAssetVersion(		33554432		)
	{
		stringstream ss;
		ss << _port;
		_sport = ss.str();
	}
	unsigned short	_port;
	std::string		_sport;
	std::string		_hostname;
	std::string		_hostip;
	std::string		_protocolVersion;
	unsigned long	_lProtocolVersion;
	std::string		_schemaVersion;
	unsigned long	_lSchemaVersion;
	std::string		_serverVersion;
	std::string		_assetVersion;
	unsigned long	_lAssetVersion;
};

/**
 * Structure to capture information about the game that these
 * tests are targeting.
 */
struct GameData {
	GameData(	const std::string&		gameKey
				, unsigned long			gameId
				, const std::string&	gameName
				, unsigned long			serverId
				, unsigned long			parentGameId
				, const std::string&	initHash
				, const std::string&	pingHash 
				, const std::string&	countHash	)
				: _serverId( serverId )
					, _gameName(gameName )
					, _gameId(	gameId )
					, _parentGameId( parentGameId )
					, _initHash( initHash ) 
					, _pingHash( pingHash )
					, _countHash( countHash )
					, _gameKey( gameKey )	{
		{
			std::stringstream ss;
			ss << gameId;
			_sgameid = ss.str();
		}
		{
			std::stringstream ss;
			ss << _serverId;
			_serverIdStr = ss.str();
		}
	}

	unsigned long	_serverId;
	std::string		_serverIdStr;
	std::string		_gameName;
	unsigned long	_gameId;
	std::string		_sgameid;
	unsigned long	_parentGameId;
	std::string		_initHash;
	std::string		_pingHash;
	std::string		_countHash;
	std::string		_gameKey;
};


// Common output structures
//
// outputs from GetBrowserPage
struct GetBrowserPageOutputs {
	std::string resultText;
	DWORD		statusCode;
	std::string statusMessage;
};

#ifdef TARGET_TECHPREVIEW
	std::string			serviceHost( "http://3d-wok.kaneva.com:8080" );

	// Data specific to the suject client machine
	ClientData			clientData( "test-server"
									, "localhost"
									, 27099 );								// The client app's port

	// Data specific to the subject game
	GameData			gameData(	"0000000000000000000000000000"
									, 8340										// game id
									, "ringo_0_test_server_601640819"			// game name
									, 4195										// server  id
									, 5354										// parent game id
									, "123456789abcdef0123456789abcdef0"		// init hash
									, "23456789abcdef0123456789abcdef01"		// ping hash
									, "3456789abcdef0123456789abcdef012"	);	// count hash
#endif
#endif





// Op input structures
//
#ifdef xTARGET_PREVIEW
PingTestInputs		PingV6Inputs(	URL::parse("http://pv-wok.kaneva.com:8080/kepping/KEPPingV6.aspx")
									, "test-server"
									, "localhost" 
									, "00000000000000000000000"
									, "123456789abcdef0123456789abcdef0" );
PingTestInputs		PingV7Inputs(	URL::parse("http://pv-wok.kaneva.com:8080/kepping/KEPPingV7.aspx")
									, "test-server"
									, "localhost" 
									, "00000000000000000000000"
									, "123456789abcdef0123456789abcdef0" );
#endif

