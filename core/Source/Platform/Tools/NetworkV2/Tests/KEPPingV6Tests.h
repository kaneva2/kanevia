///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "log4cplus/logger.h"
#include "common/keputil/Algorithms.h"
#include "tools/networkV2/KEPPingV6.h"
#include "tools/networkV2/Tests/TestConstants.h"


//std::string servicePath = "kepping/keppingv6.aspx";


struct PingV6Inputs {
	PingV6Inputs() {
		url.setHost( serviceHost )
		   .setPath( "kepping/keppingv6.aspx" );
	}

	URL url;
};

struct PingV6Outputs {
	PingV6Outputs() 
		:	gameId(99)								// not required.  Set by init call
			, parentGameId(99)								// not required.  Set by init call
			, serverId(99)								// not required.  Set by init call
			, gameName( "99" )								// not required.  Set by init call
			, maxAllowedPlayers(99)
			, adminOnly(false)
	{}
	int		gameId;
	int		parentGameId;
	int		serverId;
	string	gameName;
	int		maxAllowedPlayers;
	bool	adminOnly;
};

// todo: Test escalation on multiple auth failures
// todo: Test ssl
//
// There is no value in testing for missing or unknown action since KEPPingV6 always assigns the correct action
TEST( KEPPingV6Tests, InitSuccessTest ) {
	// This one we're going to muck with to determine the absolute minimum of data required
	// to get it to succeed
//	string gameKey		= "5295004O8340Gk5ia3xSwLoR5tyA";	// match required
#ifdef NewMode
	PingV6Inputs inputs;
	PingV6Outputs outputs;
	string hostname		= clientData._hostname;					// match required.  What happens when a server is moved. 
#else
	string hostname		= PingV6Inputs._clientHost;					// match required.  What happens when a server is moved. 
#endif	
	short port			= 27099;							// match required


	// Now that we know what is required.
	// lets set values here and see if they get set to anything on failure.
	//
	int		gameId					= 99;								// not required.  Set by init call 8340;
	int		parentGameId			= 99;								// not required.  Set by init call
	int		serverId				= 99;								// not required.  Set by init call
	string	gameName				= "99";								// not required.  Set by init call
	int		newMaxAllowedPlayers	= 99;
	LONG    numberOfPlayers			= 0;
	LONG    currentMaxPlayers       = 99;
//	int		maxAllowedPlayers		= 99;
//	bool	adminOnly				= false;
	string	result;														// not required.  Initialized 
	string	faultString;
	bool	active					= false;
	char*	s						= "not used";


	KEPPingV6 p(	log4cplus::Logger::getInstance( L"PlatformTest" )
					, gameId													// gameid
					, parentGameId												// parent game id
					, serverId													// server id
					, inputs.url.toString().c_str()
//					, "http://3d-wok.kaneva.com:8080/kepping/KEPPingV6.aspx"	// ping server url
//					, gameKey													// reg key
					, gameData._gameKey
					, hostname													// hostname
					, "WATERMARK"												// watermark
					, 1															// engine type
#ifdef NewMode
					, clientData._hostip
#else
					, PingV6Inputs._clientIP												// ipaddress
#endif
					, port														// port
					, 32806														// protocol version
					, 130														// schema version
					, "4.0.9.1421"												// server version
					, 33554432													// asset version
					, 100000													// timout ms	
					, numberOfPlayers
					, currentMaxPlayers
					, outputs.maxAllowedPlayers
					, outputs.adminOnly
					);

	EXPECT_EQ( S_OK, p.sendInitialize(	s
										, newMaxAllowedPlayers
										, active
										, gameName
										, result
										, faultString ) );

	EXPECT_STREQ( "", faultString.c_str() );
//	EXPECT_STREQ( PingV6Inputs._gameHash.c_str(), result.c_str() );  // should get determinant value back for this.
	EXPECT_STREQ( gameData._initHash.c_str(), result.c_str() );  // should get determinant value back for this.
	EXPECT_STREQ( "ringo_0_test_server_601640819", gameName.c_str() );
	EXPECT_TRUE( active );
	EXPECT_EQ( 2000, newMaxAllowedPlayers );
	EXPECT_EQ( 8340, gameId );
	EXPECT_EQ( 5354, parentGameId );
	EXPECT_EQ( 4195, serverId );
}

TEST( KEPPingV6Tests, InitFailureTest ) {
#ifdef NewMode
	PingV6Inputs inputs;
#else
	PingInputs inputs;
#endif
	// This one we're going to muck with to determine the absolute minimum of data required
	// to get it to succeed
#ifdef NewMode
#else
	string gameKey		= "0000000000000000000000000000";	// match required
#endif
	string hostname		= "test-server";					// match required.  What happens when a server is moved. 
	short port			= 27099;							// match required


	// Now that we know what is required.
	// lets set values here and see if they get set to anything on failure.
	//
	int		gameId					= 99;								// not required.  Set by init call
	int		parentGameId			= 99;								// not required.  Set by init call
	int		serverId				= 99;								// not required.  Set by init call
	string	gameName				= "99";								// not required.  Set by init call
	int		newMaxAllowedPlayers	= 99;
	LONG	numberOfPlayers			= 0;
	LONG    currentMaxPlayers       = 99;
	int		maxAllowedPlayers		= 99;
	bool	adminOnly				= false;
	string	result;														// not required.  Initialized 
	string	faultString;
	bool	active					= false;
	char*	s						= "not used";


	KEPPingV6 p(	log4cplus::Logger::getInstance( L"PlatformTest" )
					, gameId													// gameid
					, parentGameId												// parent game id
					, serverId													// server id
					, inputs.url.toString().c_str()	// ping server url
#ifdef NewMode
					, gameData._gameKey						// reg key
#else
					, gameKey						// reg key
#endif
					, hostname													// hostname
					, "WATERMARK"												// watermark
					, 1															// engine type
#ifdef NewMode
					, clientData._hostip
					, clientData._port
#else
					, PingV6Inputs._clientIP												// ipaddress
					, PingV6Inputs._sClientPort														// port
#endif
					, 32806														// protocol version
					, 130														// schema version
					, "4.0.9.1421"												// server version
					, 33554432													// asset version
					, 100000													// timout ms	
					, numberOfPlayers
					, currentMaxPlayers
					, maxAllowedPlayers
					, adminOnly
					);

	EXPECT_FALSE( S_OK == p.sendInitialize(	s
											, newMaxAllowedPlayers
											, active
											, gameName
											, result
											, faultString ) );

	EXPECT_TRUE( KEP::StringHelpers::startsWith( "Server not found for hostname: ", faultString.c_str() ) );
	EXPECT_TRUE( KEP::StringHelpers::endsWith( ". Probably not registered", faultString.c_str() ) );
	EXPECT_STREQ( "", result.c_str() );
	EXPECT_STREQ( "", gameName.c_str() );
	EXPECT_FALSE( active );
	EXPECT_EQ( 1, newMaxAllowedPlayers );
	EXPECT_EQ( 0, gameId );
	EXPECT_EQ( 0, parentGameId );
	EXPECT_EQ( 0, serverId );

}

TEST( KEPPingV6Tests, PingSuccessTest ) {
	// This one we're going to muck with to determine the absolute minimum of data required
	// to get it to succeed
	string gameKey		= "5295004O8340Gk5ia3xSwLoR5tyA";	// match required
#ifdef NewMode
	string hostname		= clientData._hostname;					// match required.  What happens when a server is moved. 
#else
	string hostname		= PingV6Inputs._clientHost;					// match required.  What happens when a server is moved. 
#endif
	short port			= 27099;							// match required


	// Now that we know what is required.
	// lets set values here and see if they get set to anything on failure.
	//
	int		gameId					= 99;								// not required.  Set by init call
	int		parentGameId			= 99;								// not required.  Set by init call
	int		serverId				= 99;								// not required.  Set by init call
	string	gameName				= "99";								// not required.  Set by init call
	int		newMaxAllowedPlayers	= 99;
	LONG    currentMaxPlayers       = 99;
	LONG	numberOfPlayers			= 0;
	int		maxAllowedPlayers		= 99;
	bool	adminOnly				= false;
	string	resultString;												// not required.  Initialized 
	string	faultString;
	bool	active					= false;
	char*	s						= "not used";


	KEPPingV6 p(	log4cplus::Logger::getInstance( L"PlatformTest" )
					, gameId													// REFERENCE gameid			Will be populated during Initialize
					, parentGameId												// REFERENCE parent game id Will be populated during Initialize
					, serverId													// server id				Will be populated during Initialize
					, "http://3d-wok.kaneva.com:8080/kepping/KEPPingV6.aspx"	// ping server url
					, gameKey													// reg key
					, hostname													// hostname
					, "WATERMARK"												// watermark
					, 1															// engine type
#ifdef NewMode
					, clientData._hostip										// ipaddress
#else
					, PingV6Inputs._clientIP										// ipaddress
#endif
					, port														// port
					, 32806														// protocol version
					, 130														// schema version
					, "4.0.9.1421"												// server version
					, 33554432													// asset version
					, 100000													// timout ms	
					, numberOfPlayers											// REFERENCE
					, currentMaxPlayers											// REFERENCE
					, maxAllowedPlayers											// REFERENCE
					, adminOnly													// REFERENCE Not modified by KEPPingV6, but modified externally
					);

	EXPECT_EQ( S_OK, p.sendInitialize(	s
										, newMaxAllowedPlayers
										, active
										, gameName
										, resultString
										, faultString ) );

	EXPECT_STREQ( "", faultString.c_str() );
#ifdef NewMode
	EXPECT_STREQ( gameData._initHash.c_str(), resultString.c_str() );  // should get determinant value back for this.
#else
	EXPECT_STREQ( PingV6Inputs._gameHash.c_str(), resultString.c_str() );  // should get determinant value back for this.
#endif
	EXPECT_STREQ( "ringo_0_test_server_601640819", gameName.c_str() );
	EXPECT_TRUE( active );
	EXPECT_EQ( 2000, newMaxAllowedPlayers );
	EXPECT_EQ( 8340, gameId );
	EXPECT_EQ( 5354, parentGameId );
	EXPECT_EQ( 4195, serverId );

 	EXPECT_EQ( S_OK, p.sendPing(	ssRunning
									, s // likely never used
									, resultString
									, faultString ) );

	EXPECT_STREQ( "bc832f3e83ba70a592698f793c9985ad", resultString.c_str() );
	EXPECT_STREQ( "", faultString.c_str() );
}

TEST( KEPPingV6Tests, GetCountSuccessTest ) {
	// This one we're going to muck with to determine the absolute minimum of data required
	// to get it to succeed
	string gameKey		= "5295004O8340Gk5ia3xSwLoR5tyA";	// match required
#ifdef NewMode
	string hostname		= clientData._hostname;					// match required.  What happens when a server is moved. 
#else
	string hostname		= PingV6Inputs._clientHost;					// match required.  What happens when a server is moved. 
#endif
	short port			= 27099;							// match required


	// Now that we know what is required.
	// lets set values here and see if they get set to anything on failure.
	//
	int		gameId					= 99;								// not required.  Set by init call
	int		parentGameId			= 99;								// not required.  Set by init call
	int		serverId				= 99;								// not required.  Set by init call
	string	gameName				= "99";								// not required.  Set by init call
	int		newMaxAllowedPlayers	= 99;
	LONG    currentMaxPlayers       = 99;
	LONG	numberOfPlayers			= 0;
	int		maxAllowedPlayers		= 99;
	bool	adminOnly				= false;
	string	resultString;												// not required.  Initialized 
	string	faultString;
	bool	active					= false;
	char*	s						= "not used";


	KEPPingV6 p(	log4cplus::Logger::getInstance( L"PlatformTest" )
					, gameId													// REFERENCE gameid			Will be populated during Initialize
					, parentGameId												// REFERENCE parent game id Will be populated during Initialize
					, serverId													// server id				Will be populated during Initialize
					, "http://3d-wok.kaneva.com:8080/kepping/KEPPingV6.aspx"	// ping server url
					, gameKey													// reg key
					, hostname													// hostname
					, "WATERMARK"												// watermark
					, 1															// engine type
#ifdef NewMode
					, clientData._hostip
#else
					, PingV6Inputs._clientIP												// ipaddress
#endif
					, port														// port
					, 32806														// protocol version
					, 130														// schema version
					, "4.0.9.1421"												// server version
					, 33554432													// asset version
					, 100000													// timout ms	
					, numberOfPlayers											// REFERENCE
					, currentMaxPlayers											// REFERENCE
					, maxAllowedPlayers											// REFERENCE
					, adminOnly													// REFERENCE Not modified by KEPPingV6, but modified externally
					);

	EXPECT_EQ( S_OK, p.sendInitialize(	s
										, newMaxAllowedPlayers
										, active
										, gameName
										, resultString
										, faultString ) );

	EXPECT_STREQ( "", faultString.c_str() );
#ifdef NewMode
	EXPECT_STREQ( gameData._initHash.c_str(), resultString.c_str() );  // should get determinant value back for this.
#else
	EXPECT_STREQ( PingV6Inputs._gameHash.c_str(), resultString.c_str() );  // should get determinant value back for this.
#endif
	EXPECT_STREQ( "ringo_0_test_server_601640819", gameName.c_str() );
	EXPECT_TRUE( active );
	EXPECT_EQ( 2000, newMaxAllowedPlayers );
	EXPECT_EQ( 8340, gameId );
	EXPECT_EQ( 5354, parentGameId );
	EXPECT_EQ( 4195, serverId );

 	EXPECT_EQ( S_OK, p.sendGetCount( ssRunning
									, s // likely never used
									, newMaxAllowedPlayers
									, active
									, resultString
									, faultString ) );


	EXPECT_STREQ( "bc832f3e83ba70a592698f793c9985ad", resultString.c_str() );
	EXPECT_STREQ( "", faultString.c_str() );
}




// Disabled - Ping failure due to can be replicated here as changing the ip will simply cause
// the init to fail.
//
TEST( KEPPingV6Tests, DISABLED_PingInvalidIpTest ) {
	// This one we're going to muck with to determine the absolute minimum of data required
	// to get it to succeed
	string gameKey		= "5295004O8340Gk5ia3xSwLoR5tyA";	// match required
	string hostname		= "xtest-server";					// match required.  What happens when a server is moved. 
	short port			= 27099;							// match required


	// Now that we know what is required.
	// lets set values here and see if they get set to anything on failure.
	//
	int		gameId					= 99;								// not required.  Set by init call
	int		parentGameId			= 99;								// not required.  Set by init call
	int		serverId				= 99;								// not required.  Set by init call
	string	gameName				= "99";								// not required.  Set by init call
	int		newMaxAllowedPlayers	= 99;
	LONG    currentMaxPlayers       = 99;
	LONG	numberOfPlayers			= 0;
	int		maxAllowedPlayers		= 99;
	bool	adminOnly				= false;
	string	resultString;												// not required.  Initialized 
	string	faultString;
	bool	active					= false;
	char*	s						= "not used";

	KEPPingV6 p(	log4cplus::Logger::getInstance( L"PlatformTest" )
					, gameId													// REFERENCE gameid			Will be populated during Initialize
					, parentGameId												// REFERENCE parent game id Will be populated during Initialize
					, serverId													// server id				Will be populated during Initialize
					, "http://3d-wok.kaneva.com:8080/kepping/KEPPingV6.aspx"	// ping server url
					, gameKey													// reg key
					, hostname													// hostname
					, "WATERMARK"												// watermark
					, 1															// engine type
#ifdef NewMode
					, clientData._hostip										// ipaddress
#else
					, PingV6Inputs._clientIP										// ipaddress
#endif
					, port														// port
					, 32806														// protocol version
					, 130														// schema version
					, "4.0.9.1421"												// server version
					, 33554432													// asset version
					, 100000													// timout ms	
					, numberOfPlayers											// REFERENCE
					, currentMaxPlayers											// REFERENCE
					, maxAllowedPlayers											// REFERENCE
					, adminOnly													// REFERENCE Not modified by KEPPingV6, but modified externally
					);

	EXPECT_EQ( S_OK, p.sendInitialize(	s
										, newMaxAllowedPlayers
										, active
										, gameName
										, resultString
										, faultString ) );

	EXPECT_STREQ( "", faultString.c_str() );
#ifdef NewMode
	EXPECT_STREQ( gameData._initHash.c_str(), resultString.c_str() );  // should get determinant value back for this.
#else
	EXPECT_STREQ( PingV6Inputs._gameHash.c_str(), resultString.c_str() );  // should get determinant value back for this.
#endif
	EXPECT_STREQ( "ringo_0_test_server_601640819", gameName.c_str() );
	EXPECT_TRUE( active );
	EXPECT_EQ( 2000, newMaxAllowedPlayers );
	EXPECT_EQ( 8340, gameId );
	EXPECT_EQ( 5354, parentGameId );
	EXPECT_EQ( 4195, serverId );
 	EXPECT_EQ( S_OK, p.sendPing(	ssRunning
									, s // likely never used
									, resultString
									, faultString ) );
	EXPECT_STREQ( "123456789abcdef0123456789abcdef0", resultString.c_str());
	EXPECT_STREQ( "", faultString.c_str() );  // "serverId, port, statusId, numberOfPlayers, maxNumberOfPlayers, or adminOnly param not specified"
}