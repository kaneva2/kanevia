///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Tools/NetworkV2/KEPAuthV6.h"
#include "TestConstants.h"

std::string servicePath = "kepauth/kepauthv6.aspx";

struct AuthInputs {
	AuthInputs() 
		:	clientIP("0.0.0.0")
			, userName( "test" )
			, userPassword("00000000000000000000000000000000")
			, logger( Logger::getInstance( L"PlatformTest" ) )
//			, clientType( LOGON_CLIENT )
			, serverId(2)
			, gameId( gameData._gameId )
	{
		url.setHost( serviceHost )
 		   .setPath( servicePath );
	}

	std::string		clientIP;
	std::string		userName;
	std::string		userPassword;
	Logger			logger;
//	BYTE			clientType;
	URL				url;
	unsigned int	serverId;
	unsigned int	gameId;
};

struct AuthOutputs {
	string userInfo;
	string faultString;
};


int CallAuth(	AuthInputs&		inputs
				, AuthOutputs&	outputs ) {
	return KEPAuthV6::logon(	inputs.userName.c_str()
								, inputs.clientIP.c_str()
								, inputs.userPassword.c_str()
//								, inputs.clientType
								, inputs.logger
								, inputs.url.toString().c_str()
								, inputs.gameId
								, inputs.serverId
								, outputs.faultString
								, outputs.userInfo );
}


// Parser tests.  Verify that the auth web service is holding up it's end of the
// contract
//

//0x00d63688 "http://pv-wok.kaneva.com:8080/kepauth/kepauthv6.aspx
// ?action=login
// &username=banneduser
// &password=123456789abcdef0123456789abcdef0
// &userIpAddress=0.0.0.0
// &gameId=3298&serverId=2"
TEST( AuthV6ParserTests, UserBannedTest )							{
	AuthResponse response;
	response.parse( "<Result code=\"7\"><Description>Account Locked</Description><Result>" );

	EXPECT_EQ(		7,					response.code() );
	EXPECT_STREQ(	"Account Locked",	response.message().c_str() );
	EXPECT_STREQ(	"",					response.userInfo().c_str() );
}


class KEPAuthV6Tests : public ::testing::Test{
public:
	KEPAuthV6Tests() 
		: logger( Logger::getInstance(L"PlatformTest") ) 	{
		StringResource = ::GetModuleHandle(NULL);
	}
	Logger logger;
};

TEST_F( KEPAuthV6Tests, DISABLED_VolatileABadAuthCodeTest ) {
	AuthInputs	inputs;
	AuthOutputs outputs;
	inputs.gameId = 1;
	EXPECT_EQ(  LOGIN_FORCE_OFF, CallAuth( inputs, outputs ) );
	EXPECT_STREQ( "Bad auth code of NO_GAME_ACCESS for test and gameId of 1", outputs.faultString.c_str() );
	EXPECT_STREQ( "", outputs.userInfo.c_str() );
}

TEST_F( KEPAuthV6Tests, DISABLED_VolatileInvalidPasswordTest ) {
	AuthInputs	inputs;
	AuthOutputs outputs;
	inputs.userPassword = "invalidpassword";
	EXPECT_EQ(	LOGIN_FORCE_OFF, CallAuth( inputs, outputs ) );
	EXPECT_STREQ( "Invalid Password", outputs.faultString.c_str() );
	EXPECT_STREQ( "", outputs.userInfo.c_str() );
}

TEST_F( KEPAuthV6Tests, DISABLED_VolatileAuthSuccessTest ) {
	AuthInputs inputs;
	AuthOutputs outputs;
	EXPECT_EQ(	LOGIN_OK, CallAuth( inputs, outputs ) );
	EXPECT_STREQ(	"", outputs.faultString.c_str() );
	EXPECT_STREQ(	"<user><u>4701506</u><r>2</r><g>F</g><b>1979-12-01</b><m>0</m><c>US</c><p>0</p></user>", outputs.userInfo.c_str() );
}

TEST_F( KEPAuthV6Tests, DISABLED_VolatileAccountNotFoundTest ) {
	AuthInputs	inputs;
	AuthOutputs outputs;
	inputs.userName = "jptadfadfasdfest46";
	try {
		ASSERT_EQ( LOGIN_OK, CallAuth( inputs, outputs ) );
		EXPECT_STREQ(	"Not found",	outputs.faultString.c_str() );
		EXPECT_STREQ(	"",				outputs.userInfo.c_str() );
	}
	catch( const ChainedException& exc ) {
		FAIL() << exc.toString();
		exc.printStack();
	}
	catch( ... ) {
		FAIL() << "Unchecked exception detected.  Still Waiting on correct format of Not Found error from auth service.";
	}
}

//<Result code="7">
//<Description>Account Locked</Description>
//</Result>"
TEST_F( KEPAuthV6Tests, DISABLED_VolatileUserBannedTest ) {
	AuthInputs inputs;
	AuthOutputs outputs;
	inputs.userName		= "banneduser";
	inputs.userPassword = "00000000000000000000000000000000";
	try {
		ASSERT_EQ( LOGIN_FORCE_OFF, CallAuth( inputs, outputs ) );
		EXPECT_STREQ(	"Account Locked",	outputs.faultString.c_str() );
		EXPECT_STREQ(	"",					outputs.userInfo.c_str() );
	}
	catch( const ChainedException& exc ) {
		FAIL() << exc.toString();
		exc.printStack();
	}
	catch( ... ) {
		FAIL() << "Unchecked exception detected";
	}
}

// Verify matches existing behavior.  When passing in null url
// returns LOGIN_NOT_FOUND
//
TEST_F( KEPAuthV6Tests, NullURLTest ) {
	AuthInputs inputs;
	AuthOutputs outputs;

	EXPECT_EQ(	LOGIN_NOT_FOUND
				, KEPAuthV6::logon(	inputs.userName.c_str()
									, inputs.clientIP.c_str()
									, inputs.userPassword.c_str()
//									, inputs.clientType
									, inputs.logger
									, 0					// NULL auth url
									, inputs.gameId		
									, inputs.serverId	
									, outputs.faultString
									, outputs.userInfo ) );
	EXPECT_STREQ( "", outputs.userInfo.c_str() );
	EXPECT_STREQ( "", outputs.faultString.c_str() );
}

TEST_F( KEPAuthV6Tests, InvalidURLTest01 ) {
	string userInfo;
	string faultString;

	EXPECT_EQ(	KEPAuthV6::logon(	"test"
									,"0.0.0.0"
									, "00000000000000000000000000000000"
//									, LOGON_CLIENT
									, logger
									, "InvalidURL"
									, 5354				// gameid
									, 2					// serverid
									, faultString
									, userInfo )
				, LOGIN_FAILED );
    std::string msg = "KEPAuthV6::logon infrastructure failure! status[12006] URL[InvalidURL?action=login&username=testuser&password=sanitized&userIpAddress=0.0.0.0&gameId=5354&serverId=2] description[UNRECOGNIZED_SCHEME]";

	EXPECT_STREQ(   msg.c_str()
					, faultString.c_str() );
	EXPECT_STREQ( "", userInfo.c_str() );
}

TEST_F( KEPAuthV6Tests, DISABLED_InvalidURLTest02 ) {
	AuthInputs inputs;
	AuthOutputs outputs;
	inputs.url = URL::parse( "http://asdfasdfadsfa.kaneva.com" );
	ASSERT_EQ(  LOGIN_FAILED, CallAuth(inputs, outputs ) );
	EXPECT_STREQ( "KEPAuthV6::logon infrastructure failure! status[500] URL[InvalidURL?action=login&username=testuser&password=sanitized&userIpAddress=0.0.0.0&gameId=5354&serverId=2] description[::HttpSendRequest failed. Make sure you have internet connection, and you are not behind an firewall]"
					, outputs.faultString.c_str() );
	EXPECT_STREQ( "", outputs.userInfo.c_str() );
}

// Verify matches existing behavior
// Empry url return LOGIN_NOT_FOUND
//
TEST_F( KEPAuthV6Tests, EmptyURLTest ) {
	AuthInputs inputs;
	AuthOutputs outputs;
	inputs.url = URL();
	ASSERT_EQ( LOGIN_NOT_FOUND, CallAuth( inputs, outputs ) );
	EXPECT_STREQ( "", outputs.userInfo.c_str() );
	EXPECT_STREQ( "", outputs.faultString.c_str() );
}

// Verify existing behavior
// Passing LOGON_AI returns LOGIN_NOT_FOUND
TEST_F( KEPAuthV6Tests, LoginAITest ) {
	AuthInputs	inputs;
	AuthOutputs outputs;
	inputs.clientType = LOGON_AI;
	ASSERT_EQ(	LOGIN_NOT_FOUND, CallAuth( inputs, outputs ) );
	EXPECT_STREQ( "", outputs.userInfo.c_str() );
	EXPECT_STREQ( "", outputs.faultString.c_str() );
}

// Verify existing behavior
// Passing LOGON_AI returns LOGIN_NOT_FOUND
TEST_F( KEPAuthV6Tests, BadGameIdTest ) {
	AuthInputs	inputs;
	AuthOutputs outputs;
	inputs.gameId = 0;
	EXPECT_EQ(LOGIN_NO_PING, CallAuth(inputs, outputs ));
	EXPECT_STREQ( "", outputs.userInfo.c_str() );
	EXPECT_STREQ( "", outputs.faultString.c_str() );
}

// MD5_DIGEST_STR pw;
// CStringA userName( m_userName );
// userName.MakeLower();
// hashString(m_passWord+userName, pw);
TEST_F( KEPAuthV6Tests, DISABLED_VolatileLogoutTest1 ) {
	AuthInputs inputs;
	AuthOutputs outputs;

	ASSERT_EQ(	LOGIN_OK, CallAuth( inputs, outputs ) );
	EXPECT_STREQ( "", outputs.faultString.c_str() );
	EXPECT_STREQ( "<user><u>4701506</u><r>2</r><g>F</g><b>1979-12-01</b><m>0</m><c>US</c><p>0</p></user>"
					, outputs.userInfo.c_str() );
	EXPECT_EQ(	LOGIN_OK,	KEPAuthV6::logout( 4701506, 1, 3, inputs.logger,  inputs.url.toString().c_str(), outputs.faultString ) );
}
