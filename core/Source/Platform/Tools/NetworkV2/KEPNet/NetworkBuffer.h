///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Tools/NetworkV2/KEPNet/NetworkBuffer.h"

CHAINED(NetworkBufferException);
CHAINED_DERIVATIVE(NetworkBufferException, NetworkBufferOverflowException );
CHAINED_DERIVATIVE(NetworkBufferException, NetworkBufferUnderflowException  );


// If we end up having to implement a compact() function, that is to drop any prior-read data and
// leave only the pending data, then modify this implementation to using a rotating
// buffer.  This is because with a circular buffer, compacting the data is the simple operation
// of moving a pointer as opposed to actually moving a bunch of data.  This won't really be 
// necessary while we are using UDP.  So don't waste time implementing it now.  Note
// using a circular buffer will increase the overhead of resizing the buffer.  As yet
// there is no identified need to resize the buffer anyway.
// 
// Also, note that as yet, there has been no identified need for tighter integration with std::stream.
//
class NetworkBuffer
{
public:
	NetworkBuffer(size_t size)
		: _buf(new unsigned char[size])
		, _writeoffset(0)
		, _readoffset(0)
		, _capacity(size)
		, _size(0) {
		_readoffset = _buf;
		_writeoffset = _buf;
	}

	NetworkBuffer( const unsigned char* buf, size_t size )
		: _buf(new unsigned char[size])
		, _writeoffset(0)
		, _readoffset(0)
		, _capacity(size)
		, _size(0) {

		_readoffset = _buf;
		_writeoffset = _buf;

		writeRaw(buf, size);
	}

	virtual ~NetworkBuffer() {
		delete[] _buf;
	}




	// Input operations
	//

	/**
	 * Stream a std::string into the buffer.
	 */
	NetworkBuffer& operator<<(const std::string& cval)	{ return write(cval);	}

	/**
	 * Stream a null terminated string into the buffer
	 */
	NetworkBuffer& operator<<(const char* cval)			{ return write((const unsigned char*)cval);	}

	NetworkBuffer& write(const std::string& cval)		{ return write(cval.c_str()); }


	/**
	* Write operators for intrinsic types
	* (e.g. int,short, et al)
	*
	* @throws BufferOverflowException
	*/
	template<class operandT>
	NetworkBuffer& operator<<(operandT val) {
		size_t len = sizeof(operandT);
		assertNoOverflow(len, "Overflow Assertion 2");

		*((operandT*)_writeoffset) = val;
		_writeoffset += sizeof(operandT);

		calcSize();
		return *this;
	}

	/**
	* Write a buffer of data.
	* @throws BufferOverflowException
	*/
	NetworkBuffer& write(const unsigned char* cval) {
		return write(cval, strlen((const char*)cval));
		//size_t len = strlen((const char*)cval);
		//assertNoOverflow(len, "Overflow Assertion 1");
		//(*this) << (unsigned short)len;
		//memcpy(_writeoffset, cval, len);
		//_writeoffset += len;
		//calcSize();
		//return *this;
	}

	NetworkBuffer& write(const unsigned char* cval, size_t len ) {
		
		assertNoOverflow(len, "Overflow Assertion 1");
		(*this) << (unsigned short)len;
		memcpy(_writeoffset, cval, len);
		_writeoffset += len;
		calcSize();
		return *this;
	}












	// Output operations
	/**
	* @throws NetworkBufferUnderflowException
	*/
	template<typename operandT>
	operandT read() {
		operandT v;
		(*this) >> v;
		return v;
	}

	/**
	* @throws NetworkBufferUnderflowException
	*/
	template<>
	std::string read() {
		std::string s;
		read(s);
		return s;
	}

	/**
	* @throws NetworkBufferUnderflowException
	*/
	NetworkBuffer& operator>>(std::string& cval) {
		read(cval);
		return *this;
	}

	/**
	 * @throws NetworkBufferUnderflowException
	 */
	template<class operandT>
	NetworkBuffer& operator>>(operandT& assignee) {
		size_t len = sizeof(operandT);
		assertNoUnderflow(len, "Underflow Assertion 1");
		assignee = *((operandT*)_readoffset);
		_readoffset += len;
		return *this;
	}

	/**
	 * @throws NetworkBufferUnderflowException
	 */
	NetworkBuffer& read(std::string& cval) {
		unsigned short s;
		(*this) >> s;
		assertNoUnderflow(s, "Underflow Assertion 2");
		cval.assign((const char*)_readoffset, s);
		_readoffset += s;
		return *this;
	}





	// It remains to be seen what kind of mark/drain/fill semantics are needed
	// at this time.
	NetworkBuffer& rewind() {
		_writeoffset = _buf;
		_readoffset = _buf;
		return *this;
	}

	size_t		capacity()	const { return _capacity; }
	size_t		size()		const { return _size; }
	const unsigned char* buf()		const { return _buf; }
private:

	NetworkBuffer& writeRaw(const unsigned char* cval, size_t len) {

		assertNoOverflow(len, "Overflow Assertion 1");
		//		(*this) << (unsigned short)len;
		memcpy(_writeoffset, cval, len);
		_writeoffset += len;
		calcSize();
		return *this;
	}


	void calcSize() {
		_size = _writeoffset - _buf;
	}

	void assertNoUnderflow(size_t sz, const std::string& context)	{
		if ((sz + _readoffset) > (_buf + _capacity))
			throw NetworkBufferUnderflowException(SOURCELOCATION, ErrorSpec() << context);
	}

	void assertNoOverflow(size_t sz, std::string context) {
		if (sz + _size > _capacity)
			throw NetworkBufferOverflowException(SOURCELOCATION, ErrorSpec() << context);
	}

	unsigned char*	_buf;
	unsigned char*	_writeoffset;
	unsigned char*	_readoffset;
	size_t  _capacity;
	size_t  _size;
};


