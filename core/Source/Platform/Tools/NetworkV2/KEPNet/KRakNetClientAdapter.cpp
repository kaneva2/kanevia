///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/Include/LogHelper.h"
#include <CStructuresMisl.h>
#include "common/keputil/CommLogger.h"
#include "Tools/NetworkV2/KEPNet/KEPNet.h"
#include "KRakNetClientAdapter.h"

namespace KEP {

static CommLogger		commLogger;

KRakNetClientAdapter::KRakNetClientAdapter( bool /*disableNATTraversal*/)
	:	m_callback(0)
		, m_logger(Logger::getInstance(L"KRakNetClientAdapter")	)			{
	// The KRakNetClientAdapter class inserts itself between the KRakNet client
	// and application. So it maintains a reference to it's own callback (e.g. CMultiplayerObject)
	// and registers itself as KRakNetClient's event handler.
	//
	KRakNetClient::setHandler(*this);
}

KRakNetClientAdapter::~KRakNetClientAdapter()	{
	try {
		disconnect();
		stopOrThrow()->wait();
	}
	catch( const KRakNetException& e)	{ LogError(e.str());	}
	catch (const std::exception& e)		{ LogError(e.what());	}
}

NETRET KRakNetClientAdapter::Connect(	IClientNetworkCallback*		callback
										, ULONG						port
										, const char*				hostName
										, BYTE *					userData
										, DWORD						userDataSize		)	{
	m_callback = callback;


	try {
		startOrThrow()->wait();

		// todo: No magic numbers
		// 
		ACTPtr	connectComplete = KRakNetClient::connectOrThrow(hostName, port);
		int		waitret			= connectComplete->wait(10000);
		if ( waitret != jsWaitRet::WR_OK) {
			LogError("Timeout while connecting");
			return NET_TIMEDOUT;
		}

		if (connectComplete->inError())		{
			const ChainedException& ce = connectComplete->chainedException();
			LogError(ce.str());
			return NET_NOCONNECTION;
		}
		// Okay, we have our low stack connection
		//
		// Send our credentials
		KRakNetClient::SendOrThrow(userData, userDataSize, KEPAuthRequest ); 
		return NET_OK;
	}
	catch (const KRakNetException& e)		{
		LogError(e.errorSpec().str());
	}
	catch (const std::exception& e )	{
		LogError(e.what());
	}
	return NET_NOCONNECTION;
}

const std::string& KRakNetClientAdapter::Name() const	{
	return Name();
}

NETRET  
KRakNetClientAdapter::Send(	NETID			to
							, const BYTE*	data
							, ULONG			size
//							, ULONG			timeout	
) {
	// Hmmm...really?  no exceptions?  Always succeeds?
	// todo:
	try {
		KRakNetClient::SendOrThrow(data, size);
		return NET_OK;
	}
	catch (const KRakNetException& ke)	{
		LogError(ke.str());
		return NET_EXCEPTION;
	}
}

NETRET		
KRakNetClientAdapter::Connected(	LOGON_RET	ret
									, NETRET	result
									, ULONG			sessionProtocolVersion	){
	// we've already handled this via the wait in the connect call.  Easier to handle
	// it there.
	//
	return NET_OK;
	// This call needs to be made when authenticated
	//	return m_callback->Connected(ret, result, sessionProtocolVersion);
}

NETRET 
KRakNetClientAdapter::Disconnected(		const std::string&	reason
										, HRESULT			hr
										, BOOL				report
										, LOGON_RET	replyCode
										, bool				connecting)		{
	return m_callback->Disconnected(reason, hr, report, replyCode, connecting);
}

bool		
KRakNetClientAdapter::DataFromServer(	NETID			netId
										, const BYTE *		data
										, unsigned long		datalen			) {
	return m_callback->DataFromServer(0, data, datalen);
}

/**
 * This method overrides 
 */
KRakNetClientAdapter& 
KRakNetClientAdapter::handleInboundPacket(RakNet::Packet* p)			{
	NETID netid = RakNet::RakNetGUID::ToUint32(p->guid);
	commLogger.logReceive(netid, p->data, p->length, "KRAKNET::KRakNetClientAdapter::handleInboundPacket()");
	if (!m_callback)
		return *this;

	try {
		switch (p->data[0])
		{
		case KanevaPacket:
			m_callback->DataFromServer(	p->guid == serverGUID() 
												? 0
												: RakNet::RakNetGUID::ToUint32(p->guid)
										, p->data + 1, p->length - 1);
			break;
		case KEPAuthResponse:
		{
			KEPNET_CONNECT_REPLY_CONTEXT * reply = (KEPNET_CONNECT_REPLY_CONTEXT*)(p->data + 1);

			m_callback->Connected(reply->returnCode, reply->netret, reply->protocolVersion);
			break;
		}
		default:
			throw ChainedException(SOURCELOCATION, "Unrecognized packet");
		}	
	}
	catch (const KRakNetException e)	{
		LogError( "Exception [" << e.str() << "] raised while processing message..." << std::endl );
	}
	return *this;
}

KRakNetClientAdapter& 
KRakNetClientAdapter::handleConnectCompletion(RakNet::Packet* p)		{
	KRakNetClient::handleConnectCompletion(p);
	return *this;
}

KRakNetClientAdapter& 
KRakNetClientAdapter::setTimeoutTimeMillis(unsigned long millis, NETID netid )	{
	try {
		KRakNetClient::setTimeoutTimeMillisOrThrow(millis, netid);
	}
	catch (const KRakNetException& e)	{ LogError(e.str()); }
	catch (const std::exception& e)		{ LogError(e.what()); }
	return *this;
}

unsigned long
KRakNetClientAdapter::timeoutTimeMillis( NETID netid ) const {
	try {
		return KRakNetClient::timeoutTimeMillisOrThrow(netid);
	}
	catch (const KRakNetException& e)	{ LogError(e.str());	}
	catch (const std::exception& e)		{ LogError(e.what());	}
}

KRakNetClientAdapter& 
KRakNetClientAdapter::synchronizeTimeoutTimeMillis(unsigned long millis)	{
	try {
		KRakNetClient::synchronizeTimeoutTimeMillisOrThrow(millis);
	}
	catch (const KRakNetException& e) { LogError(e.str()); }
	catch (const std::exception& e) { LogError(e.what()); }
	return *this;
}

// take a stab at the state management
//
// todo: reduce this when we get back from the server side 
// handling of this.
//		if (_authPending.wait(100000000) != jsWaitRet::WR_OK)
//	{
//			LogError("Timeout authenticating");
//			return FALSE;
//		}

// Call into our listener and let them know that we are connected with the
// the appropriate ret, result (the difference unclear) and protocolVersion
// We'll have to figure out how to dig this out when we get back from serverside
//		return m_callback->Connected(0,0,0);
//	return m_callback->DataFromServer(netId, data, datalen);
//	if (data[1] == 0x3f && data[2] == 0x0b)
//		Algorithms::Noop();

// Okay.  The easiest hook in is as transport layer only.  It's easier to 
// integrate because it's truer the separation.  Let's stay away from their
// plugins...unless we write it.  In either case, I'm not overly enthusiastic
// about this library.
//
// The documentation can be a bit confusing at times and the absence of 
// real code to illustrate how to do things, in the noob sense any way.
//
// It is implemented in non-blocking mode, but without using a socket select
// This means they are polling themselves.  It's true they are running
// non-blocking sockets, but without availing themselves of socket select
// they must poll.  Silly in my opinion.
//
// For now...to at least draw a line in the coupling sand, we will push 
// all of this to a function that handles kaneva data.
//
// It occurs to me that we may have an issue with memory management.  Do we currently 
// Where was this line of responsibility drawn under direct play?
// Incidentally, this makes the argument of a pool of buffers.  This way
// we can avoid the copying of data and the allocations (at least no more after
// initial packet allocation).
//
// At this point we need to start mapping connected clients, specifically
// map the NETID (a 32bit representation of the senders GUID).  This will allows
// us to not have to change any of existing peer identification methods.  It does
// have the overhead of requireing a hashed search of the map to resolve the 32bit id
// to the sendiers guid/address.
//
// An alternative is to redefine NETID as a container which, depending on the implementation
// may contain a RakNetGUID or a DPlay NETID.  This precludes map management and 
// searching.
//
// Another alternative is to simply place the RakNetGUID on the heap and then 
// return a pointer to the RakNetGUID.  This pointer value can be passed around as
// a 32bit number.  It is true that this value only has meaning on localhost, (i.e.
// an id on this machine in all likelihood is useless on another machine).  However
// we have the translation at our finger types so who cares.  One issue is that
// the RakNetGUID cannot be relocated without updating the id.  A very problematic
// proposition.
// 
// For right now, let's go with the map.  
//
//				HandleKanevaData(RakNet::GetGuidFromSystemAddress(RakNetGUID::p->data,p->length);

//RakNet::BitStream bs(p->data+1, p->length-1, false);
//// RakNet::BitStream::RakNet::BitStream bs;

//unsigned char auth;
//unsigned char logonStatus;
//unsigned long protocolVersionInfo;
//bs.Read(auth);
//bs.Read(logonStatus);
//bs.Read(protocolVersionInfo);

// I'm attempting to acquire the guid of the server, after it has 
// accepted me.  I would have expected this guid to be present here
// But this one looks wonky.
//
// Going to modify check server side via GetMyGUID()
//_serverGUID = peer->GetGuidFromSystemAddress(p->systemAddress);
//_serverAddress = p->systemAddress;

//_connectACT->complete();
//_state = Connected;

//KEPNET_CONNECT_REPLY_CONTEXT * reply = (KEPNET_CONNECT_REPLY_CONTEXT*)((p->data)+1);
//if (m_callback != 0)
//	// Unlike dplay, rejection is handled by a completely different code path in raknet.
//	// If we get here, it can only be because it is Okay!
//	//
//	m_callback->Connected(reply->returnCode, NET_OK, reply->protocolVersion);		// This will become an integration point

} // namespace KEP