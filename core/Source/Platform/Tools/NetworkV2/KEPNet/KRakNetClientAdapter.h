///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Common/Include/LogHelper.h"

#include "INetwork.h"
#include "KRakNet/KRakNetClient.h"

namespace KEP {

/**
 * Adapts KRakNetClient to the application layer.  KRakNetClient is 
 * syntatically equivalent to DPlay Client ala the IClientNetwork 
 * interface.  Hower, KRakNet is functionally different due to the
 * nature of it's underlying implementation, RakNet.
 *
 * For example the dplay provider treats 
 * connection/authentication/authorization as a single operation 
 * e.g.   connect(targer,usercredentials
 *
 * RakNet treats these as two operations.  More accurately, the RakNet 
 * layer does not care about authentication, at the application level, user 
 * credentials are placed in sent with the connect message (DPlay has the 
 * specific ability to embed data on it's connection operation.
 * 
 * So this adapter needs to insert itself between the application and the net 
 * provider in order to send the creds after the connection is complete and 
 * mimic the behavior of the dplay provider. 
 *
 * After we've retired DPlay we can relax this of course.
 */
class KRakNetClientAdapter 
	:	public KRakNetClient
		, public IClientNetwork
		, protected IClientNetworkCallback					{

public:
	KRakNetClientAdapter(bool /*disableNATTraversal*/);

	virtual ~KRakNetClientAdapter();

	virtual NETRET			Send(			NETID					to
											, const BYTE*			data
											, ULONG					bytes
//											, ULONG					timeout		
	) override;

	virtual const std::string&	Name() const override;

	virtual NETRET			Connect(		IClientNetworkCallback*callback
											, ULONG					port
											, const char*			hostName
											, BYTE *				userData
											, DWORD					userDataSize	) override;


	NETRET					Connected(		LOGON_RET
											, NETRET
											, ULONG);

	NETRET					Disconnected(	const std::string &
											, HRESULT
											, BOOL
											, LOGON_RET
											, bool) override;

	bool						DataFromServer(	NETID		netId
												, const BYTE *	data
												, unsigned long	datalen );

	/**
	* Some network implementations allow for some tolerance of latency added to the
	* pipeline (i.e. blocked in debugger).  Set this tolerance by indicating
	* how long the connection will be able to stay intact (in millis), in the absence of
	* being able to send a guaranteed message. Hence 0 = zero tolerance.
	*
	* @return reference to self
	*/
	virtual KRakNetClientAdapter& setTimeoutTimeMillis(unsigned long millis, NETID netid = SERVER_NETID) override;
	virtual unsigned long timeoutTimeMillis(NETID netid = SERVER_NETID) const override;
	virtual KRakNetClientAdapter& synchronizeTimeoutTimeMillis(unsigned long millis) override ;

protected:
	virtual KRakNetClientAdapter&	handleInboundPacket(RakNet::Packet*		p) override;
	virtual KRakNetClientAdapter&	handleConnectCompletion(RakNet::Packet* p) override;

private:
	ACT							_authPending;
	IClientNetworkCallback *	m_callback;
	log4cplus::Logger			m_logger;
};

} // namespace KEP