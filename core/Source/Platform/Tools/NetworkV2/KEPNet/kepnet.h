///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Reply data DPN_MSGID_INDICATE_CONNECT
struct DP_CONNECT_REPLY_CONTEXT {
	char returnCode;			// the original return code
	ULONG protocolVersion;		// protocol version for client
};

struct KEPNET_CONNECT_REPLY_CONTEXT : public DP_CONNECT_REPLY_CONTEXT {
	unsigned long netret;		// dplay returns an hresult when replying to DPN_INDICATECONNECT. 
	// being that raknet is not dplay, we need to open up some storage
	// in this structure to send this.
	//
	// furthermore, the dplay layer returns DPPNERRs (e.g. DPNERR_HOSTREJECTEDCONNECTION)
	// Obviously, this is DPLAY centric.  So we replace these values with their
	// NETRET equivalents.
};

class KEPNet {
public:
	static const unsigned long MaxIncomingConnections = 600;
};
