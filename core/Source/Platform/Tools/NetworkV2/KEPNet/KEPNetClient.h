///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "KEPNet.h"
#include "KrakNet/KrakNet.h"
#include "KEPNet/KRakNetClientAdapter.h"

namespace KEP {

/**
 * Interface KEPNet layer client.  KEPNet layer provides access to the transport layer
 * 
 */
class KEPNetClient : public IClientNetwork
{
public:
	KEPNetClient()
	{}

	virtual ~KEPNetClient()
	{}

private:


};

class ConcreteKEPNetClient : public IClientNetwork
{
	
};

} // namespace KEP