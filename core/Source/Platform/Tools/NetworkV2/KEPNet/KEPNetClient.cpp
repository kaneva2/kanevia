///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KEPNetClient.h"
#include "KRakNetClientAdapter.h"
#include "INetwork.h"

namespace KEP {

IClientNetwork *MakeClientNetwork(
	const char*						baseDir
	, const char*					loggerName
	, bool							disableNATTraversal
	, INetwork::NetworkProvider		provider /* = INetwork::DPlay */
) {
	switch (provider) {
		case INetwork::KRakNet:		return new KRakNetClientAdapter(disableNATTraversal);
		default:					throw IllegalArgumentException(SOURCELOCATION);
	}
}

KEPNetClientPtr MakeClientNetwork(
	bool							disableNATTraversal
	, INetwork::NetworkProvider		provider /* = INetwork::DPlay */
) {
	switch (provider) {
		case INetwork::KRakNet:		return KEPNetClientPtr(new KRakNetClientAdapter(disableNATTraversal));
		default:					throw IllegalArgumentException(SOURCELOCATION);
	}
}

} // namespace KEP