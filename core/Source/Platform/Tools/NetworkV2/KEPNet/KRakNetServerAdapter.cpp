///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/Include/LogHelper.h"
#include "common/KEPUtil/Algorithms.h"
#include <KEPException.h>
#include <CStructuresMisl.h>
#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"
#include "Tools/NetworkV2/Pinger/Pinger.h"
#include "Tools/NetworkV2/KEPNet/KRakNetServerAdapter.h"
#include "Tools/NetworkV2/NetworkHelpers.h"
#include "Tools/NetworkV2/NetworkStrings.h"
#include "Tools/NetworkV2/AuthConfiguration.h"
#include "Tools/NetworkV2/KEPNet/KEPNet.h"
#include "NetworkCfg.h"
#include "common/keputil/CommLogger.h"
using namespace log4cplus;

namespace KEP {

static CommLogger		commLogger;

KRakNetServerAdapter::KRakNetServerAdapter( IServerNetworkCallback*		callback
											, const PingerConfigurator& pingerConf
											, const AuthConfiguration&	authConf
											, const std::string&		instanceName
											, const std::string&		loggerName
											, eEngineType engineType	) 
	:	m_callback(callback)
		, m_protocolVersion(0)
		, m_schemaVersion(0)
		, m_pingTimeoutSec(10)
		, _maxConnections(1200) // todo: no magic number default max connections
		, m_baseDir("")
        , m_logger(Logger::getInstance(Utf8ToUtf16(loggerName)))
        , super()
        , m_onlyAllowLoopBack(false)
		, m_disableNATTraversal(false)
{
	super::setName(instanceName);
	logVersion( m_logger );
}
 
NETRET KRakNetServerAdapter::Listen(	IServerNetworkCallback*	    /* unused */
										, eEngineType engineType
										, ULONG					port
										, const char*			hostName /*= ""*/)		{
	LogInfo("engineType = " << (int) engineType << ", port = " << port);

	try	{
		super::setCallback(*this)
			  .initializeOrThrow(port)
			  .startOrThrow();
		return NET_OK;
	}
	catch( const KRakNetException& e )		{		LogError(e.str());	}
	catch( const std::exception& e )		{		LogError(e.what());	}
	return NET_NOTREADY;
}

Monitored::DataPtr KRakNetServerAdapter::monitor() const  {
	return KRakNetServer::monitor();
}



// This is the business logic of a player connecting to the server.
// WILL HANDLE user authentication/authorization
// Some payload other dplay message.  Thats coming
//
LOGON_RET
KRakNetServerAdapter::indicateConnect(	NETID					id
										, const std::string&	ipAddress
										, CONTEXT_VALIDATION*	cntxValidation) {
	std::string userName(cntxValidation->username);
	std::string password(cntxValidation->password);

	LOGON_RET logOnStatus = LR_OK;

	if (logOnStatus == LR_OK) {
		std::string userInfo;
		int ret = LOGIN_NO_PING;

		if (pinger().GetGameId() != 0) {
			ret = ValidateLogonViaWS(userName.c_str()
				, password.c_str()
				, ipAddress.c_str()
				, m_logger
				, authURL().str().c_str() //m_authServerUrl.c_str()
				, pinger().GetGameId()
				, pinger().GetServerId()
				, userInfo);
		}

		switch (ret)
		{
		case LOGIN_OK: // pass in server id if ok
			logOnStatus = m_callback->ValidateLogon(id, cntxValidation, ipAddress.c_str(), userInfo.c_str());
			break;

		case LOGIN_NOT_FOUND: // if not found, give caller chance to lookup locally in svr file, etc.
		case LOGIN_NO_PING:   // added 5/08 to allow local validation if no ping
		{
			LOGON_RET lr = m_callback->ValidateLogon(id, cntxValidation, ipAddress.c_str(), "<user/>");
			if (lr != LR_ACCT_NOT_FOUND || ret != LOGIN_NO_PING)
				logOnStatus = lr;
			else
				logOnStatus = mapLogonStatus(ret);
		}
		break;

		case LOGIN_PASSWORD_INVALID:
			logOnStatus = LR_PASSWORD_INVALID;
			break;

		default:
			logOnStatus = mapLogonStatus(ret);
			break;
		}
	}

	if (authURL().str().empty())
		LOG4CPLUS_ERROR(m_logger, loadStr(IDS_NO_AUTH_URL));

	return logOnStatus;
}

//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//-----------------------------------------------------------------------------//
//KRakNetServerAdapter& 
//KRakNetServerAdapter::setMaxBandwidthPerPerson(unsigned long maxBandwidth)	{
//	m_maxBandwidthPerPerson = maxBandwidth;
//	return *this;
//}

//KRakNetServerAdapter& 
//KRakNetServerAdapter::setMaxPacketsPerChar(unsigned long maxPackets)	{
//	m_maxPacketsPerChar = maxPackets;
//	return *this;
//}

KRakNetServerAdapter& 
KRakNetServerAdapter::allowLoopBackOnly(){
	m_onlyAllowLoopBack = true;
	return *this;
}

bool 
KRakNetServerAdapter::isLoopBackOnly()	{
	return m_onlyAllowLoopBack;
}

KRakNetServerAdapter& 
KRakNetServerAdapter::disableNATTraversal()	{
	m_disableNATTraversal = true;
	return *this;
}

KRakNetServerAdapter& 
KRakNetServerAdapter::setPort(unsigned short port)	{
	m_port = port;
	return *this;
}

unsigned short 
KRakNetServerAdapter::port()	{
	return m_port;
}

KRakNetServerAdapter& 
KRakNetServerAdapter::setHostName(std::string HostName)	{
	m_IPAddress = HostName;
	return *this;
}

const std::string& 
KRakNetServerAdapter::hostName() const	{
	return m_IPAddress;
}

unsigned long* 
KRakNetServerAdapter::maxConnectionsPtr()	{
	return m_maxConnections;
}

NETRET 
KRakNetServerAdapter::Send(	NETID			to
							, const BYTE*	data
							, ULONG			bytes )	{
	try									{
		super::SendOrThrow(to, data, bytes);
		return NET_OK;
	}
	catch (const KRakNetException&)		{
		return NET_GENERIC;
	}
}

/**
* Access the servers address map.
*/
std::string 
KRakNetServerAdapter::GetIPAddress(NETID netId)	{
	try { return super::GetIPAddressOrThrow(netId); }
	catch (const KRakNetException& e) { LogError(e.str()); }
	catch (const std::exception& e) { LogError(e.what()); }
}

const std::string& 
KRakNetServerAdapter::Name() const					{
	return super::Name();
}

NETRET 
KRakNetServerAdapter::DisconnectClient(NETID netId, eDisconnectReason reason)	{
	try {
		LogInfo("netId=" << netId << " reason=" << reason);
		return super::DisconnectClientOrThrow(netId, reason);
	}
	catch (const KRakNetException& e)		{
		LogError(e.str());
		return (NETRET)e.errorSpec().first;
	}
	catch (const std::exception& e )	{
		LogError(e.what());
		return -1;
	}
}

void KRakNetServerAdapter::AuthenticateUser(	RakNet::Packet* p
												, NETID			netid) {
	try {
		// The connect/auth are still  unified per client usage, we we still do our
		// fakey here to make it appear as a single operation
		//
		// Now however we do have authenticate() as part of API, so this can be
		// decomposed at some point in the near future.
		// TODO: Put this structure on the stack
		//
		CONTEXT_VALIDATION* cntxValidation = (CONTEXT_VALIDATION*)(p->data + 1);
		LOGON_RET logOnStatus = indicateConnect(	netid 
													, KRakNetServer::GetIPAddressOrThrow(netid)
													, cntxValidation	);

		std::shared_ptr<KEPNET_CONNECT_REPLY_CONTEXT> reply( new KEPNET_CONNECT_REPLY_CONTEXT );
		reply->returnCode		= logOnStatus;
		reply->protocolVersion	= cntxValidation->protocolVersionInfo;
		reply->netret			= logOnStatus == LR_OK 
		             			  ? NET_OK 
		             			  : NET_HOSTREJECTEDCONNECTION;

		CommLogger::singleton().log(netid, "003 Send KEPAuthResponse");
		KRakNetServer::SendOrThrow(	netid
	                    			, (unsigned char*)reply.operator->()
	                    			, sizeof(KEPNET_CONNECT_REPLY_CONTEXT)
									, KEPAuthResponse);;
	}
	catch (const KRakNetException& e) { LogError(e.str()); }
	catch (const std::exception& e) { LogError(e.what()); }
}

KRakNetServerAdapter&
KRakNetServerAdapter::handleInboundPacket(RakNet::Packet* p)		{
	NETID netid = RakNet::RakNetGUID::ToUint32(p->guid);
	commLogger.logReceive(netid, p->data, p->length,"KRAKNET::KrakNetServerAdapter::handleInboundPacket()");

	if (!m_callback)
		return *this;
	try {
		switch (p->data[0])			{
		case KanevaPacket:
			// Creates an interesting, yet very ugly little loop.  Don't call the super method as it is only
			// going to route right back into this class as a DataFromClient call.
			// Let's go and and dispatch it ourselves right here.
			if (m_callback > 0)
				m_callback->DataFromClient(RakNet::RakNetGUID::ToUint32(p->guid), p->data + 1, p->length - 1);
			break;
		case KEPAuthRequest:
			AuthenticateUser(p, netid);
		default:
			break;
		}
		commLogger.logReceive(netid, p->data + 1, p->length - 1, "KRAKNET");
	}
	catch (const KRakNetException& e)	{ LogError(e.str());	}
	catch (const std::exception& e)		{ LogError(e.what());	}
	return *this;
}

KRakNetServerAdapter& 
KRakNetServerAdapter::setTimeoutTimeMillis(unsigned long millis, NETID netid ) {
	try {
		KRakNetServer::setTimeoutTimeMillisOrThrow(millis,netid);
	}
	catch (const KRakNetException& e) { LogError(e.str()); }
	catch (const std::exception& e) { LogError(e.what()); }
	return *this;
}

unsigned long
KRakNetServerAdapter::timeoutTimeMillis( NETID netid ) const	{
	try {
		return peer->GetTimeoutTime(resolveAddressOrThrow(netid).systemAddress);
	}
	catch (const KRakNetException& e) { LogError(e.str()); }
	catch (const std::exception& e) { LogError(e.what()); }
	return LONG_MAX;
}

KRakNetServerAdapter&
KRakNetServerAdapter::setMaxConnections(unsigned long maxConnections) {
	_maxConnections = maxConnections;
	return *this;
}

} // namespace KEP