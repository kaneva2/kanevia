///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/include/LogHelper.h"
#include "common/keputil/Algorithms.h"
#include <CStructuresMisl.h>
#include "Tools/NetworkV2/NetworkHelpers.h"
#include "Tools/NetworkV2/KEPNet/KRakNetServerAdapter.h"		// RakNet implementation includes
#include "Tools/NetworkV2/KEPNet/KEPNetServer.h"
#include "Tools/NetworkV2/Pinger/Pinger.h"
#include <Common/include/KEPException.h>

#include <common\include\EngineTypes.h>

using namespace log4cplus;

namespace KEP {

static const char* const KEPNet_SERVERNAME = "KEP";

KEPNetServer::DefaultServerFactory::DefaultServerFactory(
	IServerNetworkCallback* callback
	, INetwork::NetworkProvider				provider
	, const PingerConfigurator& pingerConf
	, const AuthConfiguration&	authConf
	, const std::string&		instanceName
	, const std::string&		loggerName
	, eEngineType engineType
)
	: _instanceName(instanceName)
	, _loggerName(loggerName)
	, _engineType(engineType)
	, _pingerConf(pingerConf)
	, _authConf(authConf)
	, _provider(provider)
	, _callback(callback)
{}

KEPNetServerPtr KEPNetServer::DefaultServerFactory::makeServer()	{
	switch (_provider)
	{
	case INetwork::KRakNet:
		LOG4CPLUS_INFO(Logger::getInstance(L"KEPNetServer"), "Creating KRakNet Server");
		return KEPNetServerPtr(new KEPNetServer(_callback, _pingerConf, _authConf, _instanceName, _loggerName, _engineType));
	default:
		throw UnsupportedException(SOURCELOCATION, "Unkown Network Provider!");
	};
}

IServerNetwork* KEPNetServer::DefaultServerFactory::makeNakedServer()	{
	switch (_provider)
	{
	case INetwork::KRakNet:
		LOG4CPLUS_INFO(Logger::getInstance(L"KEPNetServer"), "Creating KRakNet Server");
		return new KEPNetServer(_callback, _pingerConf, _authConf, _instanceName, _loggerName, _engineType);
	default:
		throw UnsupportedException(SOURCELOCATION, "Unkown Network Provider!");
	};
}


IServerNetwork* MakeServerNetwork(	IServerNetworkCallback*	callback
												, const PingerConfigurator& configurator
												, const AuthConfiguration& authConf
												, const std::string& instanceName
												, const std::string& loggerName
												, eEngineType engineType /*=1 server*/
												, INetwork::NetworkProvider provider /* = KEPNet::DPlay */		)		{
	KEPNetServer::DefaultServerFactory factory(callback, provider, configurator, authConf, instanceName, loggerName, engineType);
	return factory.makeNakedServer();
}

KEPNetServerPtr MakeServerNetwork(	IServerNetworkCallback* callback
										, INetwork::NetworkProvider provider /* = KEPNet::DPlay */
										, const PingerConfigurator& configurator
										, const AuthConfiguration& authConf
										, const std::string& instanceName
										, const std::string& loggerName
										, eEngineType engineType /*=1 server*/				)		{
	return KEPNetServer::DefaultServerFactory(	callback
												, provider
												, configurator
												, authConf
												, instanceName
												, loggerName
												, engineType).makeServer();
}

Monitored::DataPtr KEPNetServer::monitor() const	{
	BasicMonitorData * p = new BasicMonitorData();
	(*p) << "<KEPNetServer />";
	return Monitored::DataPtr(p);
}


KEPNetServer::KEPNetServer( IServerNetworkCallback*		callback
							, const PingerConfigurator&	pingerConf
							, const AuthConfiguration&	authConf
							, const std::string&		serverInstanceName
							, const std::string&		loggerName
							, eEngineType engineType	)
	:	m_logger(Logger::getInstance(L"KEPNetServer"))
		, m_callback(callback)
		, m_authServerUrl(authConf._authURL)
		, m_pinger( PingerPtr(new NullPinger()))
		, m_gameId(0)
		, m_parentGameId(0)
		, m_serverId(0)
		, m_pingerConfigurator(pingerConf)
		, KRakNetServerAdapter( callback
								, pingerConf
								, authConf
								, serverInstanceName
								, loggerName
								, engineType)
{
	LogTrace("");
	logVersion(m_logger);
}

KEPNetServer&
KEPNetServer::setInstanceName(const std::string &)
{
	return *this;
}

LOGON_RET
KEPNetServer::ValidateLogon(NETID					id
	, CONTEXT_VALIDATION *	credentials
	, const char *			data
	, const char *			datalen) {
	return m_callback->ValidateLogon(id, credentials, data, datalen);
}

// On connect, start a session object and place it in a hash here.  The concept of session
// starts here.
//
// Frankly, I think the practice of peeking into the packet is incorrect.  It should be
// routed as an inbound packet just like anything else.  
//
// My guess is however that this was done to make sure that the listener (CMultiplayer)
// doesn't do anything until they are auth'ed in.  There are better ways to do this.
//
const URL&
KEPNetServer::authURL() const {
	return m_authServerUrl;
}

NETRET
KEPNetServer::ClientConnected(NETID				netId
	, const char *			uh) {
	return (m_callback > 0) ? m_callback->ClientConnected(netId, uh) : NET_OK;
}

NETRET
KEPNetServer::ClientDisconnected(NETID					netId
	, eDisconnectReason	reason) {
	return (m_callback > 0) ? m_callback->ClientDisconnected(netId, reason) : NET_OK;
}

bool
KEPNetServer::DataFromClient(NETID netId, const BYTE * data, ULONG datalen) {
	return (m_callback > 0) ? m_callback->DataFromClient(netId, data, datalen) : true;
}

NETRET 
KEPNetServer::Listen(	IServerNetworkCallback*	    callback /* unused */
						, eEngineType engineType
						, ULONG						port
						, const char*				hostName /*= ""*/) {

	LogInfo("engineType = " << (int) engineType << ", port = " << port);
	HRESULT hr = NET_OK;

	try {
		if (!InitPinger()) {
			return NET_NOTREADY;
		}
		KRakNetServerAdapter::Listen(callback, engineType,port,hostName);
	}
	catch (KEPException *e) {
		hr = e->m_err;
		e->Delete();
	}
	return hr;
}


bool
KEPNetServer::InitPinger() {
	LogTrace("");

	// Perhaps...but then it shouldn't be incumbent on the server to
	// manage the life cycle of the pinger.
	if (!m_pingerConfigurator.gameKey().empty()) {
		Pinger* pp = (Pinger*)MakePinger(m_pingerConfigurator);
		if (pp)
			m_pinger = PingerPtr(pp);
		return pp != 0;
	}
	else if (m_pingerConfigurator.isEngineTypeServer()) {
		allowLoopBackOnly();
		return true;
	}
	else if (m_pingerConfigurator.isEngineTypeDispatcher())
		return true;
	else
		return false;
}

// Would appear that this will return once of the values fron INetwork logon codes.
// Thankfully this method's return value never tested.  
int KEPNetServer::LogoffCleanup(int userId, enum eDisconnectReason reasonCode) {
	if (m_serverId == 0)
		m_serverId = pinger().GetServerId();
	if (m_gameId == 0) {
		m_gameId = pinger().GetGameId();
		m_parentGameId = pinger().GetParentGameId();
	}

	return LogoutUserViaWS(	userId
							, reasonCode
							, m_serverId
							, m_logger
							, authURL().str().c_str());
}

} // namespace KEP