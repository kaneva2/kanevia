///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "Tools/NetworkV2/KEPNet/Tests/NetworkBufferTests.h"
#if 0
#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"
#include "Tools/NetworkV2/AuthConfiguration.h"
#include "Tools/NetworkV2/INetwork.h"
#include <Common/KEPUtil/SocketSubsystem.h>
#include <Server/KEPServer/KEPServerEnums.h>
#include "Tools/NetworkV2/KRakNet/KrakNetServer.h"
#include <Common/include/CoreStatic/CStructuresMisl.h>
#include <atlstr.h>
#include "Tools/NetworkV2/Protocols.h"
#include "Core/Crypto/Crypto_MD5.h"

// Move this out to be used.
class __declspec(dllexport) COMSystem {
public:
	COMSystem()	{		CoInitialize(0);	}
	virtual ~COMSystem()	{		CoUninitialize();	}
};


class MyCallback : public IServerNetworkCallback
{
public:
	// only server
	virtual bool DataFromClient(NETID from, const BYTE *data, ULONG len) {		return true;	};
	virtual LOGON_RET ValidateLogon(NETID id, CONTEXT_VALIDATION *context,
		const char* ipAddress,
		const char* userInfoXML)	{		return LR_OK;	}

	virtual NETRET ClientConnected(NETID id, const char* playerName)  {		return S_OK;	}
	virtual NETRET ClientDisconnected(NETID id, eDisconnectReason reason) {		return S_OK;	}
};

class MyClientNetworkCallback : public IClientNetworkCallback
{
public:

	// only client
	virtual NETRET Connected(LOGON_RET logonRet, NETRET hr, ULONG sessionProtocolVersion) override {	return S_OK; };
	virtual NETRET Disconnected(const std::string& reason, HRESULT hr, BOOL report, LOGON_RET logonRet, bool connecting) override	{ return S_OK; }
	virtual bool DataFromServer(NETID from, const BYTE *data, ULONG len) override { return true; }
};

// Test fixture that sets up a KEPNetServer
class KEPNetTest : public ::testing::Test
{
public:
	void SetUp() {
		Algorithms::Noop();
		pingConf.setDevBuild(true)
				.setPingIntervalMin(1)
				.setProtocolVersion(32807)
				.setSchemaVersion(258)
				.setAssetVersion(33554432)
				.setTimeoutSecs(10)
				.setMaxPlayers(2000)
				.setEngineType(1)
				.setPort(25857)
				.setPingServerURL("http://dev-ping.kaneva.com/kepping/KEPPingV7.aspx")
				.setGameKey("0000000000000000000000000000")
				.setIPAddress("test-server")
				.setServerVersion("4.0.1.1")
				.setLoggerName("Pinger")
				.setInstanceName("Local Test Server");
		authConf._authURL = "http://dev-auth.kaneva.com/kepauth/kepauthv6.aspx";
	}
	AuthConfiguration	authConf;
	PingerConfigurator	pingConf;
	SocketSubSystem		sss;
	MyCallback cb;
};


class DPlayAdapterTest : public KEPNetTest
{
public:
	COMSystem	_comSys;
};



TEST_F(KEPNetTest, DISABLED_DPlayInstantiationTest01)
{
	CoInitialize(NULL);			// <-- Implement this the same was as SocketSubSystem

	KEPNetServerPtr s = MakeServerNetwork(&cb, INetwork::DPlay, pingConf, authConf, "Test Server", "KEPNetTest");

	try  {
		s->Listen(&cb, 1, 32807);


		// Okay, updatePinger, called by the game server in his render loop and
		// startup logic. Silly really since there is nothing getting in the way of starting the
		// server.
		// should simply go ahead and let the pinger run
		//
		s->UpdatePinger(ssRunning, 0, false/* m_adminOnly*/);


		cout << "Night night!" << endl;
		::Sleep(5000);
		cout << "Rise and shine" << endl;
	}
	catch (const ChainedException& exc)
	{
		FAIL() << exc.str().c_str();
	}
}


TEST_F(KEPNetTest, DISABLED_KRakNetInstantiationTest01)
{
	try {
		KEPNetServerPtr s = MakeServerNetwork(&cb, INetwork::KRakNet, pingConf, authConf, "Test Server", "KEPNetTest");
		s->Listen(&cb, 1, 32807);

		// Okay, updatePinger, called by the game server in his render loop and
		// startup logic. Silly really since there is nothing getting in the way of starting the
		// server.
		// should simply go ahead and let the pinger run
		//
		s->UpdatePinger(ssRunning, 0, false/* m_adminOnly*/);

		cout << "Night night!" << endl;
		::Sleep(5000);
		cout << "Rise and shine" << endl;
	}
	catch (const ChainedException& exc)
	{
		FAIL() << exc.str().c_str();
	}
}

enum LogonType {
	LogonTypeUnused0
	, LogonTypeAI
	, LogonTypeClient
	, LogonTypeUnknown
};

void FillInLogonContext(	const std::string&			stdUserName
							, const std::string&		stdPassWord
							, unsigned long				m_contentVersion
							, unsigned long				m_protocolVersion
							, LogonType					m_logonType
							, CONTEXT_VALIDATION_EX&	msgEx
							, bool						hashPassword	)
{
	// hash the password
	CStringA m_userName(stdUserName.c_str());
	CStringA m_passWord(stdPassWord.c_str());
	CStringA userName(m_userName);
	userName.MakeLower();
	lstrcpynA(msgEx.username, m_userName, sizeof(msgEx.username));

	if (hashPassword)
	{
		MD5_DIGEST_STR pw;
		hashString(m_passWord + userName, pw);
		lstrcpynA(msgEx.password, pw.s, sizeof(msgEx.password));

		for (int i = 0; i < m_passWord.GetLength(); i++)
			m_passWord.SetAt(i, ' '); // clear memory
		m_passWord = pw.s; // set to hash since used in SendLogFast later
	}
	else
	{
		lstrcpynA(msgEx.password, (const char *)m_passWord, sizeof(msgEx.password));
	}

	msgEx.SetCategory( (MSG_CAT) m_logonType);
	msgEx.protocolVersionInfo	= m_protocolVersion;
	msgEx.contentVersionInfo	= m_contentVersion;
	msgEx.minProtocolVersion	= ProtocolCompatibility::Instance().getMinVersion();

	// Sanity check
	jsAssert(msgEx.protocolVersionInfo != INVALID_PROTOCOL_VERSION);
	jsAssert(msgEx.minProtocolVersion != INVALID_PROTOCOL_VERSION);
}

// This attempts to replicate the behavior of the real world when connecting/authing a client with the server.
// This is in preparation for replicating the behaviour in KRakNet.
//
TEST_F(KEPNetTest, DISABLED_KRakNetConnectionTest01)	{
	MyClientNetworkCallback clientCallback;
	KEPNetServerPtr			s = MakeServerNetwork(	&cb
													, INetwork::KRakNet
													, pingConf
													, authConf
													, "Test Server"
													, "KEPNetTest"													 );

	CONTEXT_VALIDATION_EX cntxValidation;
	FillInLogonContext("testuser"				// Me thinks we require the web to resolve the username.
												// Not cool really, we already go to the server to identify 
												// our target server.  Leave it for now.
						, "testpass"
						, 1		// Content version
						, 2		// Protoco version
						, LogonTypeClient
						, cntxValidation
						, true);

	try  {
		s->Listen(&cb, 1, 32807);

		// Okay, updatePinger, called by the game server in his render loop and
		// startup logic. Silly really since there is nothing getting in the way of starting the
		// server.
		// should simply go ahead and let the pinger run
		//
		s->UpdatePinger(ssRunning, 0, false/* m_adminOnly*/);

		KEPNetClientPtr c = MakeClientNetwork(false, INetwork::KRakNet );
		
		// NETRET ret = m_clientNetwork->Connect(this, m_port, getIPAddress().c_str(), (BYTE *)&cntxValidation, sizeof(cntxValidation));
		c->Connect(&clientCallback /* callback */
					, 32807
					, "localhost"
					, (BYTE *)&cntxValidation
					, sizeof(cntxValidation));

		//cout << "Night night!" << endl;
		Sleep(2000);

		//c->Disconnect()->wait();
		//s->stop()->wait();


		//cout << "Rise and shine" << endl;
	}
	catch (const ChainedException& exc)
	{
		FAIL() << exc.str().c_str();
	}
}


// This attempts to replicate the behavior of the real world when connecting/authing a client with the server.
// This is in preparation for replicating the behaviour in KRakNet.
//
TEST_F(DPlayAdapterTest, DISABLED_DPlayConnectionTest01)	{
	MyClientNetworkCallback clientCallback;
	KEPNetServerPtr s = MakeServerNetwork( &cb, INetwork::DPlay, pingConf, authConf, "Test Server", "KEPNetTest");

	CONTEXT_VALIDATION_EX cntxValidation;
	FillInLogonContext(	"testuser"				// Me thinks we require the web to resolve the username.
															// Not cool really, we already go to the server to identify 
															// our target server.  Leave it for now.
						,"testpass"
						, 1		// Content version
						, 2		// Protoco version
						, LogonTypeClient
						, cntxValidation
						, true);
	// NETRET ret = m_clientNetwork->Connect(this, m_port, getIPAddress().c_str(), (BYTE *)&cntxValidation, sizeof(cntxValidation));

	try  {
		s->Listen(&cb, 1, 32807);

		// Okay, updatePinger, called by the game server in his render loop and
		// startup logic. Silly really since there is nothing getting in the way of starting the
		// server.
		// should simply go ahead and let the pinger run
		//
		s->UpdatePinger(ssRunning, 0, false/* m_adminOnly*/);

		KEPNetClientPtr c = MakeClientNetwork(false, INetwork::DPlay);
		c->Connect( &clientCallback /* callback */
					, 32807
					, "localhost"
					, (BYTE *)&cntxValidation
					, sizeof(cntxValidation) );

		//cout << "Night night!" << endl;
		Sleep(2000);
		//cout << "Rise and shine" << endl;
	}
	catch (const ChainedException& exc)
	{
		FAIL() << exc.str().c_str();
	}
}

#endif