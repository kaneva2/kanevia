#pragma once

class NetworkBuffer
{
public:
	NetworkBuffer(size_t size)
		: buf(new char[size]), writeoffset(0)
		, readoffset(0)
	{
		readoffset = buf;
		writeoffset = buf;
	}

	virtual ~NetworkBuffer()
	{
		delete[] buf;
	}

	NetworkBuffer& operator<<(const std::string& cval) {
		write(cval);
		return *this;
	}

	NetworkBuffer& operator>>(std::string& cval) {
		read(cval);
		return *this;
	}

	NetworkBuffer& operator<<(const char* cval) {
		write(cval);
		return *this;
	}


	// Read/Write operators for intrinsic types
	// (e.g. int,short, et al
	//
	template<class operandT>
	NetworkBuffer& operator<<(operandT val) {
		*((operandT*)writeoffset) = val;
		writeoffset += sizeof(operandT);
		return *this;
	}

	template<class operandT>
	NetworkBuffer& operator>>(operandT& assignee) {
		assignee = *((operandT*)readoffset);
		readoffset += sizeof(operandT);
		return *this;
	}

	NetworkBuffer& write(const std::string& cval) {
		write(cval.c_str());
		return *this;
	}

	NetworkBuffer& write(const char* cval) {
		size_t len = strlen(cval);
		(*this) << (unsigned short)len;
		strcpy(writeoffset, cval);
		writeoffset += len;
		return *this;
	}

	NetworkBuffer& read(std::string& cval) {
		unsigned short s;
		(*this) >> s;
		cval.assign(readoffset, s);
		readoffset += s;
		return *this;
	}

private:
	char* buf;
	char* writeoffset;
	char* readoffset;
};

