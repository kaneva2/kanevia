///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Tools/NetworkV2/KEPNet/NetworkBuffer.h"



TEST( NetworkBufferTests, TDD01){
	NetworkBuffer buffer(14);

	ASSERT_EQ(14, buffer.capacity());

	unsigned short usvar;
	unsigned long ulvar;
	string svar1;
	string svar2;

	buffer << (unsigned short)1;  // 2
	ASSERT_EQ(2, buffer.size());
	buffer << (unsigned long)2;
	ASSERT_EQ(6, buffer.size());  // 4
	buffer << "T1";
	ASSERT_EQ(10, buffer.size()); // 4
	buffer << "T2";
	ASSERT_EQ(14, buffer.size()); // 4


	// Test overflow protection
	//
	ASSERT_THROW(buffer << "T3", NetworkBufferOverflowException);
	ASSERT_THROW(buffer << (unsigned short)1, NetworkBufferOverflowException);
	ASSERT_THROW(buffer << (unsigned long)2, NetworkBufferOverflowException);

	// We can read from the buffer using streaming operators
	//
	buffer	>> usvar >> ulvar >> svar1 >> svar2;

	ASSERT_EQ(1, usvar);
	ASSERT_EQ(2, ulvar);
	ASSERT_STREQ("T1", svar1.c_str());
	ASSERT_STREQ("T2", svar2.c_str());

	buffer.rewind();

	// Or we can simply read (using the the template argument as a type clue)
	//
	ASSERT_EQ(1, buffer.read<unsigned short>());
	ASSERT_EQ(2, buffer.read<unsigned long>());
	ASSERT_STREQ("T1", buffer.read<std::string>().c_str());
	ASSERT_STREQ("T2", buffer.read<std::string>().c_str());

	// Test underflow protection
	//
	ASSERT_THROW(buffer.read<std::string>(), NetworkBufferUnderflowException);
	ASSERT_THROW(buffer.read<unsigned short>(), NetworkBufferUnderflowException);
}
