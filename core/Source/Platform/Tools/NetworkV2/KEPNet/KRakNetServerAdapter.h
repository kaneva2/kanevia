///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Tools/networkV2/INetwork.h"
#include "KRakNet/KRakNetServer.h"
#include <dplay8.h>
#include <vector>
#include "Core/Util/ChainedException.h"
#include "Tools/NetworkV2/KRakNet/KRakNetServer.h"
//#include "Tools/NetworkV2/KEPNet/KEPNetServer.h"

namespace KEP {

// from BaseEngine's enum 
#define Server 1
#define AI 3
#define DispatcherServer 5
#define ENGINE_TELL 6

typedef void *PING_HANDLE;

// todo: modify to derive from KEPNetServer interface.
class KRakNetServerAdapter 
	:	public		KRakNetServer
		, public	IServerNetwork
		, protected IServerNetworkCallback		{ 

	typedef KRakNetServer			super;

public:
	KRakNetServerAdapter(	IServerNetworkCallback*	callback
						    , const PingerConfigurator&	pingerConf
							, const					AuthConfiguration& authConf
							, const					std::string& baseDir
							, const					std::string& loggerName
							, eEngineType engineType	);

	virtual Monitored::DataPtr monitor() const override;

	virtual NETRET Listen( IServerNetworkCallback *callback, eEngineType engineType, ULONG port, const char* hostName = "" );

	NETRET Send(NETID to, const BYTE* data, ULONG bytes);

	/**
	 * Access the servers address map.
	 */
	std::string GetIPAddress(NETID netId);

	/**
	* Some network implementations allow for some tolerance of latency added to the
	* pipeline (i.e. blocked in debugger).  Set this tolerance by indicating
	* how long the connection will be able to stay intact (in millis), in the absence of
	* being able to send a guaranteed message. Hence 0 = zero tolerance.
	*
	* @return reference to self
	* @see INetwork::setTimeoutTimeMills()
	*/
	virtual KRakNetServerAdapter& setTimeoutTimeMillis(unsigned long millis, NETID netid = SERVER_NETID) override;
	virtual unsigned long timeoutTimeMillis(NETID netid = SERVER_NETID) const override;
	virtual const std::string& Name() const override;
	virtual NETRET DisconnectClient(NETID netId, eDisconnectReason reason) override;

	/**
	* Alter server to only accept connections from the local machine.
	* N.B. You can can turn it off, but you can't turn it back on.
	*/
	KRakNetServerAdapter& allowLoopBackOnly();
	bool isLoopBackOnly();
	KRakNetServerAdapter& disableNATTraversal();
	KRakNetServerAdapter& setPort(unsigned short port);
	unsigned short port();
	KRakNetServerAdapter& setHostName(std::string HostName);
	const std::string&				hostName() const;
	unsigned long*					maxConnectionsPtr();

protected:
	virtual KRakNetServerAdapter& handleInboundPacket(RakNet::Packet* p);
	virtual KRakNetServerAdapter& setMaxConnections(unsigned long maxConnections);

private:
	void AuthenticateUser(RakNet::Packet* p, NETID netid);


	virtual LOGON_RET indicateConnect(	NETID					id
										, const std::string&	ipAddress
										, CONTEXT_VALIDATION*	cntxValidation	);

	unsigned long m_limitSleep;
	unsigned long* m_maxConnections;
	unsigned short m_port;
	std::string m_IPAddress;
	std::string				m_instanceName;
	ULONG					m_protocolVersion;
	LONG					m_schemaVersion;
	std::string				m_serverVersion;
	ULONG					m_assetVersion;
	ULONG 					m_pingTimeoutSec;
    std::string             m_baseDir;
	Logger                  m_logger;
	bool					m_onlyAllowLoopBack;
	bool					m_disableNATTraversal;
	IServerNetworkCallback*	m_callback;
	unsigned long			_maxConnections;
};

} // namespace KEP
