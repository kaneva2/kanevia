///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Tools/NetworkV2/KEPNet/KEPNet.h"
#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"
#include "Tools/NetworkV2/AuthConfiguration.h"
#include "Tools/NetworkV2/KEPNet/KRakNetServerAdapter.h"

#include "common\include\EngineTypes.h"

namespace KEP {

/**
	* Ability to switch implementations at runtime is accomplished through the use
	* of a factory which will build a server network using a specific network provider
	* This class defines the interface for a server
	*/
class KEPNetServerFactory
{
public:
	virtual KEPNetServerPtr makeServer() = 0;
	virtual IServerNetwork* makeNakedServer() = 0;
};

class KEPNetServer 
	: public KRakNetServerAdapter	
{
public:
	class DefaultServerFactory : public KEPNetServerFactory {
	public:
		DefaultServerFactory(	IServerNetworkCallback*			callback
								, INetwork::NetworkProvider		provider
								, const PingerConfigurator&		pingerConf
								, const AuthConfiguration&		authConf
								, const std::string&			instanceName
								, const std::string&			loggerName
								, eEngineType engineType);

		KEPNetServerPtr makeServer();
		IServerNetwork* makeNakedServer();

	private:
		IServerNetworkCallback*		_callback;
		std::string					_instanceName;
		std::string					_loggerName;
		eEngineType _engineType;
		AuthConfiguration			_authConf;
		PingerConfigurator			_pingerConf;
		INetwork::NetworkProvider	_provider;
	};

	KEPNetServer(	IServerNetworkCallback*		callback
					, const PingerConfigurator& configuration
					, const AuthConfiguration&	authConf
					, const std::string&		serverInstanceName
					, const std::string&		loggerName
					, eEngineType engineType	);

	virtual Monitored::DataPtr monitor() const override;


	virtual KEPNetServer&	setInstanceName(const std::string &) override;

	// This is an application level function...and hence should be called from the adapter
	virtual LOGON_RET		ValidateLogon(NETID id, CONTEXT_VALIDATION *, const char *, const char *) override;

	virtual const URL&		authURL() const override;
	virtual NETRET			ClientDisconnected(NETID, eDisconnectReason) override;
	virtual NETRET			ClientConnected(NETID, const char *) override;
	/**
	* @throws RakNetException in the event that the NETID is invalid.
	* maybe InvalidNETIDException is called for?
	*/
	virtual bool			DataFromClient(NETID, const BYTE *, ULONG) override;

	virtual NETRET Listen(	IServerNetworkCallback *callback
							, eEngineType engineType
							, ULONG					port
							, const char*			hostName = "") override;

	// Provided for the adapter 
	bool						InitPinger();
	// Listen will InitPinger, only use this if you don't need to listen
	// All of the following functions exist for the pinger (should get rid of the pinger handle idea)
	// Anyway, at some point in the future, the responsibility of owning the pinger is placed elsewhere
	// and then all this goes away.
	//
	virtual int			LogoffCleanup(int userId, enum eDisconnectReason reasonCode) override;

	virtual Pinger& pinger() override {
		return *((Pinger*)m_pinger.get());
	};
private:
	Logger						m_logger;
	IServerNetworkCallback*		m_callback;
	URL							m_authServerUrl;
	PingerPtr					m_pinger;
	PingerConfigurator	m_pingerConfigurator;
	mutable LONG				m_serverId;
	ULONG						m_parentGameId;
	ULONG						m_gameId;
};

} // namespace KEP
