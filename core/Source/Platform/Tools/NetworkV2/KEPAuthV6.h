///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Audit.h>
#include <Alert.h>

#include "Tools/NetworkV2/INetwork.h"
#include "Tools/NetworkV2/NetworkStrings.h"
#include "TinyXML/tinyxml.h"
#include "Common/include/KEPHelpers.h"
#include "Common/include/KEPAuthConstants.h"
#include "Common/include/AuditStrings.h"
#include "NetworkHelpers.h"
#include "common/KEPUtil/URL.h"

namespace KEP {

/**
 * @file
 * General exception thrown by ping operations.
 */
CHAINED( KEPAuthException );

class __declspec(dllexport) AuthResponse : public AbstractResponse {
public:
	AuthResponse();

	/**
	 * @throws GeneralizedResponseParser::Exception
	 */
	AuthResponse&		parse( const std::string& content );
	const std::string&	userInfo() const		{ return _userInfo;	}

private:
	std::string		_userInfo;
};


class KEPAuthV6 {
public:
	/**
	 *	From Constants.cs 
	 */
	enum {
		FAILED					= 0		// 0 not authenticated
		, SUCCESS				= 1     // 1 successuful authentication
		, USER_NOT_FOUND		= 2     // 2 user not retrieved from database
		, INVALID_PASSWORD		= 3     // 3 invalid password
		, NO_GAME_ACCESS		= 4     // 4 user was authenticated, but not to game supplied
		, NOT_VALIDATED			= 5     // 5 user has not validated the account
		, ACCOUNT_DELETED		= 6		// 6 user account was deleted
		, ACCOUNT_LOCKED		= 7     // 7 user account was locked
		, ALREADY_LOGGED_IN		= 8		// 8 user alreay logged in, performed by DS web service
		, GAME_FULL				= 9     // 9 Max user limit hit for the game
		, NOT_AUTHORIZED		= 10    // 10 mature, age, access pass, etc.
		, WRONG_SERVER			= 11    // 11 mismatched server info serverId doesn't match serverId for actualIp
		, ADMIN_ONLY			= 12    // 12 the server is admin_only and you're not an admin
		, PROMPT_ALLOW			= 13    // 13 the player must be prompted before going on to patch 3dApp
		, GAME_BANNED			= 14    // 14 the game (3DApp) has been banned
	} AuthCodes;

	// Note removal of superfluous passwordOk, while it was set in this function, it was never used externally
	//
	static int mapResultCode(	int			result
								, const char*	userName
								, ULONG		gameId		);

	// common V6 handling of response from WS
	/**
	 * @param faultString	Reference to string.  ResultDescription will be placed in this string.
	 * @userInfo			Reference to string.  userInfo will be placed in this string.
	 * @return 
	 */
//	static int processReSTResponse(	//HRESULT				hr
//									//, 
//									 string			results
//									, const char*			userName
//									, string&			faultString
//									, ULONG			gameId
//									, Logger&		logger
//									, std::string&	userInfo );
	/**
	 * Sample URL
	 * 
	 * 		KEPAuth/KKEPAuthV6.aspx?action=login&username=testuser&password=test[ass&userIpAddress=10.10.10.10&gameId=3296&serverId=1
	 * 
	 * 		KEPAuth/KKEPAuthV6.aspx?action=logout&userId=4&reasonCode=1&serverId=1
	 * 
	 * Return results you must handle, some of these are new as SOAP would have handled automatically…
	 * 
	 * 	Login
	 * 
	 * 		<Result>
	 * 			<ReturnCode>-1</ReturnCode>
	 * 			<ResultDescription>userName, userIpAddress, password, gameId or serverId param not specified</ResultDescription>
	 * 		</Result>
	 * 
	 * 		<Result>
	 * 			<ReturnCode>-1</ReturnCode>
	 * 			<ResultDescription>action param not specified</ResultDescription>
	 * 		</Result>
	 * 
	 * 		<Result>
	 * 			<ReturnCode>-1</ReturnCode>
	 * 			<ResultDescription>Invalid parameter value passed in</ResultDescription>
	 * 		</Result>
	 * 
	 * 		<Result>
	 * 			<ReturnCode>1</ReturnCode>
	 * 			<ResultDescription>Sucess</ResultDescription>
	 * 					userInfo = "<user>";
	 * 					userInfo += "<u>" + u.UserId.ToString() + "</u>";
	 * 					userInfo += "<r>" + u.Role.ToString() + "</r>";  // this is Kaneva role
	 * 					userInfo += "<g>" + u.Gender+ "</g>";
	 * 
	 * 					DateTime bd = Convert.ToDateTime(u.BirthDate);
	 * 					string birthDateStr = bd.ToString("yyyy-MM-dd", System.Globalization.DateTimeFormatInfo.InvariantInfo); // get back to MySQL format
	 * 
	 * 					userInfo += "<b>" + birthDateStr + "</b>";
	 * 					userInfo += "<m>" + (u.ShowMature? "1" : "0") + "</m>";
	 * 					userInfo += "<c>" + u.Country + "</c>";
	 * 					userInfo += "<p>" + roleId + "</p>"; // game prive id
	 * 					userInfo += "</user>";
	 * 		</Result>
	 * 
	 * 		<Result>
	 * 			<ReturnCode>2</ReturnCode>
	 * 			<ResultDescription>Not found</ResultDescription>
	 * 		</Result>
	 * 
	 * 		<Result>
	 * 			<ReturnCode>3</ReturnCode>
	 * 			<ResultDescription>Invalid Password</ResultDescription>
	 * 		</Result>
	 * 
	 * 		<Result>
	 * 			<ReturnCode>" + (int)authCode + "</ReturnCode>
	 * 			<ResultDescription>Bad auth code of " + authCode.ToString() + " for " + userName + " and gameId of " + gameId + "</ResultDescription>
	 * 		</Result>
	 * 
	 * 		<Result>
	 * 			<ReturnCode>6</ReturnCode>
	 * 			<ResultDescription>Account Deleted</ResultDescription>
	 * 		</Result>
	 * 
	 * 		<Result>
	 * 			<ReturnCode>7</ReturnCode>
	 * 			<ResultDescription>Account Locked</ResultDescription>
	 * 		</Result>
	 * 
	 * 		<Result>
	 * 			<ReturnCode>14</ReturnCode>
	 * 			<ResultDescription>Banned</ResultDescription>
	 * 		</Result>
	 * 
	 * 	All Return codes
	 * 		FAILED				= 0,	// 0 not authenticated
	 * 		SUCCESS				= 1,    // 1 successuful authentication
	 * 		USER_NOT_FOUND		= 2,    // 2 user not retrieved from database
	 * 		INVALID_PASSWORD	= 3,    // 3 invalid password
	 * 		NO_GAME_ACCESS		= 4,    // 4 user was authenticated, but not to game supplied
	 * 		NOT_VALIDATED		= 5,    // 5 user has not validated the account
	 * 		ACCOUNT_DELETED		= 6,	// 6 user account was deleted
	 * 		ACCOUNT_LOCKED		= 7,    // 7 user account was locked
	 * 		ALREADY_LOGGED_IN	= 8,	// 8 user alreay logged in, performed by DS web service
	 * 		GAME_FULL			= 9,    // 9 Max user limit hit for the game
	 * 		NOT_AUTHORIZED		= 10,   // 10 mature, age, access pass, etc.
	 * 		WRONG_SERVER		= 11,   // 11 mismatched server info serverId doesn't match serverId for actualIp
	 * 		ADMIN_ONLY			= 12,   // 12 the server is admin_only and you're not an admin
	 * 		PROMPT_ALLOW		= 13,   // 13 the player must be prompted before going on to patch 3dApp
	 * 		GAME_BANNED			= 14    // 14 the game (3DApp) has been banned
	 *
	 *	@throws KEPAuthException
	 */
	static int logon(	const char*			userName
						, const char*		ipAddress
						, const char*		password
//						, BYTE			clientType
						, Logger&		logger
						, const char*		authServerUrl
						, ULONG			gameId
						, LONG			serverId
						, std::string&  faultString
						, std::string&	userInfo		);

	/**
	 * @throws KEPAuthException
	 */
	static int logout(	int						userId
						, int					reasonCode
						, int					serverId
						, Logger&				logger
						, const std::string&	authServerURL
						, std::string&			faultString );

private:    
    static std::string formatError( const std::string&      action
                                    , const KEP::URL&            url
                                    , const std::string&    errorClass
                                    , long                  code
                                    , const std::string&    msg );
};

} // namespace KEP