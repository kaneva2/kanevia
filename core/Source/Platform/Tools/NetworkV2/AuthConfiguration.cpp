///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <log4cplus/logger.h>
#include <Common/include/NetworkCfg.h>
#include "AuthConfiguration.h"
#include <Common/KEPUtil/ExtendedConfig.h>
#include "Common/include/KEPException.h"
#include <common/keputil/SocketSubsystem.h>

using namespace log4cplus;

namespace KEP {

AuthConfiguration& AuthConfiguration::initialize(const std::string& basedir)	{
	std::string	path = PathAdd(basedir, NETCFG_CFG);
	try	{
		KEPConfigAdapter cfg;
		cfg.Load(NETCFG_CFG);
		std::string authURL;
		cfg.GetValue(AUTHSERVERURL_TAG, authURL, "");
		_authURL = URL::parse(authURL);
	}
	catch ( const URLParseException& e )
	{
		NetworkException ne(SOURCELOCATION, ErrorSpec(-1, "Failed to initialize AuthConfiguration due to bad auth url!"));
		ne.chain(e);
		throw ne;
	}
	catch (KEPException * e)		{
		ConfigurationException configException(SOURCELOCATION, ErrorSpec(e->m_err, e->m_msg));
		e->Delete();
		throw configException;
	}
	return *this;
}

} // namespace KEP