///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Tools/NetworkV2/Pinger/PingerConfigurator.h"
#include "Core/Util/Monitored.h"
#include "Tools/NetworkV2/Pinger/Pinger.h"

// * Rename RakNetComponent::send() to RakNetComponent::Send()
// 1) Copy class DPlayServer to class KEPNetServer
// 2) Rename class DPlayServer to DPlayServerAdapter
// 3) Copy class DPlayServerAdapter to RakNetServerAdapter
// 4) Pull MakeServerNetwork() upto KEPNetServer
// 5) Add KRakNet/DPlay compile switch (this is only temporary)
// 6) Enable KRakNet


// !) Redefine DplayServer and Dplayserverbase relationship from isa to hasa ala letter/envelope idiom
// 2) Move DPlay/DPlayServer* to ./KEPNetServer*
//     * remove includes of DPlayCommon which holds DPlay specific references.
// 3) Rename DPlayServer to KEPNetServer
// 4) Extract class KEPNetClient from DPlayClient implemented as a passthru to DPlayClient or KRakNetClient on compile switch
//
// n) Extract KEPNetClient Interface and derive DPlayClient and KRakNetClient from this interface to allow for
//    runtime switch of network layers (instead of compile time switch where interfaces just happen to be the same.)
// n) Extract KEPNetServer Interface and derive DPlayServer and KRakNetServer from this interface to allow for
//    runtime switch of network layers (instead of compile time switch where interfaces just happen to be the same.)
// n) 

/**********************************************************

	Networking interface classes

	Base classes for a networking abstraction.  First
	implementation is DirectPlay, thus you may note
	some similarities.
	
**********************************************************/
#include <windows.h>
#include "SDKCommon.h"
#include "Common\include\KEPCommon.h"

#include <log4cplus/Logger.h>

#include "INetworkErrors.h"

namespace KEP {

class AuthConfiguration;
struct CONTEXT_VALIDATION;

// constants
typedef ULONG NETID;
typedef std::vector<NETID> NETIDVect;

const NETID SERVER_NETID = 1;	// just happens to be the same as DPlay....

typedef char LOGON_RET;

const LOGON_RET LR_INVALID				= '\x0';
const LOGON_RET LR_ACCT_NOT_FOUND 		= 'a';
const LOGON_RET LR_EMAIL				= 'b';
const LOGON_RET LR_GAME_FULL			= 'c';
const LOGON_RET LR_DELETED				= 'd';
const LOGON_RET LR_SVR_NOT_PUBLIC		= 'e';
const LOGON_RET LR_CONNECTION_FAILED    = 'f';
const LOGON_RET LR_NO_GAME_ACCESS		= 'g';
const LOGON_RET LR_SUSPENDED 			= 'h';
const LOGON_RET LR_IP_LOG_ERR 			= 'i';
const LOGON_RET LR_NOT_AUTHORIZED		= 'j';
const LOGON_RET LR_OK					= 'k';
const LOGON_RET LR_ALREADY_LOGGED_ON 	= 'l';
const LOGON_RET LR_CONNECTION_LOST      = 'm'; // this is a network level event and does not belong here
const LOGON_RET LR_SERVER_MAX			= 'n';
const LOGON_RET LR_LOCKED				= 'o';
const LOGON_RET LR_INV_PW				= 'p'; // drf - seems 'r' occurs instead
const LOGON_RET LR_VERSION				= 'v';
const LOGON_RET LR_ACCT_HOLD			= 'x';
const LOGON_RET LR_NO_PING				= 'z';
const LOGON_RET LR_CONNECTION_TERMINATED = 'u'; // drf - added
const LOGON_RET LR_USER_INFO			= 'q'; // drf - added
const LOGON_RET LR_PASSWORD_INVALID		= 'r'; // drf - added

const char* const UNDEFINED_IP_ADDRESS = "Undefined";

inline std::string LR_GetConnectionCode(const LOGON_RET& logonRet) {

	switch (logonRet) {
		case LR_OK: return "Login Success";
		case LR_IP_LOG_ERR: return "Login Failed - Invalid IP Address";
		case LR_SERVER_MAX: return "Login Failed - Server Full";
		case LR_ACCT_NOT_FOUND: return "Login Failed - Account Not Found";
		case LR_EMAIL: return "Login Failed - Email Invalid (1)";
		case LR_VERSION: return "Login Failed - Software Update Required";
		case LR_ACCT_HOLD: return "Login Failed - Account Expired";
		case LR_ALREADY_LOGGED_ON: return "Login Failed - Account Already Logged In";
		case LR_SVR_NOT_PUBLIC: return "Login Failed - Server Private";
		case LR_SUSPENDED: return "Login Failed - Account Suspended";
		case LR_NO_GAME_ACCESS: return "Login Failed - No Game Access";
		case LR_INV_PW: return "Login Failed - Password Invalid (1)";
		case LR_DELETED: return "Login Failed - Account Deleted";
		case LR_CONNECTION_FAILED: return "Login Failed - Connection Failed";
		case LR_LOCKED: return "Login Failed - Account Locked";
		case LR_NO_PING: return "Login Failed - Server Not Responding";
		case LR_GAME_FULL: return "Login Failed - Game Full";
		case LR_NOT_AUTHORIZED: return "Login Failed - Access Pass Required";
		case LR_CONNECTION_LOST: return "Login Failed - Connection Lost";
		// drf - added
		case LR_CONNECTION_TERMINATED: return "Login Failed - Connection Terminated";
		case LR_USER_INFO: return "Login Failed - User Info Invalid";
		case LR_PASSWORD_INVALID: return "Login Failed - Password Invalid";
	}

	return "Login Failed - Network Error"; // unrecognized network error
}

enum eDisconnectReason
{
	// 1-4 match DPlay
	DISC_NORMAL = 1,	// normal disconnect
	DISC_LOST,			// connection was lost, so disconnecting
	DISC_TERMINATED, 	// session was terminated
	DISC_FORCE,			// server disconnected the client
	DISC_TIMEOUT,		// disconnecting client due to timeout
	DISC_BANNED,		// server has banned the client	
	DISC_SECURITY_VIO,	// server disconnected the client that violated security
	DISC_COMM_ASSIGN,
	DISC_UNKNOWN_NETID,	// client disconnected from server and something doesnt know
	DISC_PLAYER_ZONED,	// player zoned to another server
	DISC_SERVER_RESTART // value for login_log table on server restart cleanup
};

#define LOGIN_FORCE_OFF -1
#define LOGIN_OK 0
#define LOGIN_NOT_FOUND 1
#define LOGIN_SUSPENDED 2
#define LOGIN_ALREADY_LOGGED_ON 3
#define LOGIN_NONPAYMENT 4
#define LOGIN_SERVER_PRIVATE 5
#define LOGIN_NO_GAME_ACCESS 6
#define LOGIN_PASSWORD_INVALID 7
#define LOGIN_NOT_VALIDATED 8
#define LOGIN_DELETED	9
#define LOGIN_FAILED	10
#define LOGIN_LOCKED	11
#define LOGIN_NO_PING	12
#define LOGIN_GAME_FULL 13
#define LOGIN_NOT_AUTHORIZED 14 

class IClientNetworkCallback
{
public:

	// only client
	virtual NETRET Connected( LOGON_RET logonRet, NETRET hr, ULONG sessionProtocolVersion ) = 0;	
	virtual NETRET Disconnected(const std::string& reason, HRESULT hr, BOOL report, LOGON_RET logonRet, bool connecting) = 0;
	virtual bool DataFromServer( NETID from, const BYTE *data, ULONG len ) = 0;	
};
	
class IServerNetworkCallback
{
public:
	// only server
	virtual bool DataFromClient( NETID from, const BYTE *data, ULONG len ) = 0;	
	virtual LOGON_RET ValidateLogon(	NETID id
										, CONTEXT_VALIDATION *context
										, const char* ipAddress
										, const char* userInfoXML ) = 0;

	virtual NETRET ClientConnected( NETID id, const char* playerName ) = 0;	
	virtual NETRET ClientDisconnected( NETID id, eDisconnectReason reason ) = 0;	
};

class INetwork  
{
public:
	enum NetworkProvider
	{
		DPlay
		, KRakNet
	};
	static const NetworkProvider defaultNetworkProvider = KRakNet;


    /**
     * Given a NetworkProvider, obtain a string representation.
     */
	static std::string NetworkProviderName(NetworkProvider netProvider ) {
		static std::string providerNames[] = {
			"DPlay",
			"KRakNet",
		};
		return providerNames[netProvider];
	}

	/**
	 * Given the string representation of a NetworkProvider, acquire it's enumeration
	 * value.  In the event that an invalid name is specified, it will return the
	 * default provider enum.
	 *
	 * @param providerName		The name of the NetworkProvider.
	 * 							Valid values include dplay and kraknet.
	 *							Note that the name is case insensitive.
	 * @return The NetworkProvider enumeration value.
	 */
	static NetworkProvider NetworkProviderWithName(const std::string& providerName) {
		StringHelpers::lower(providerName);
		if (providerName == "dplay")
			return INetwork::DPlay;
		else if (providerName == "kraknet")
			return INetwork::KRakNet;
		else
			return INetwork::defaultNetworkProvider;
	}

	virtual NETRET Send(NETID to, const BYTE *data, ULONG bytes) = 0;

    virtual const std::string& Name() const = 0;

	/**
	* Some network implementations allow for some tolerance of latency added to the
	* pipeline (i.e. blocked in debugger).  Set this tolerance by indicating
	* how long the connection will be able to stay intact (in millis), in the absence of
	* being able to send a guaranteed message. Hence 0 = zero tolerance.
	*
	* @return reference to self
	*/
	virtual INetwork& setTimeoutTimeMillis(unsigned long millis, NETID netid = SERVER_NETID ) = 0;
	virtual unsigned long timeoutTimeMillis(NETID netid = SERVER_NETID) const = 0;

protected:	
    INetwork()     {}
	virtual ~INetwork() {}
};

class AbstractNetwork : public INetwork, public Monitored	{
public:
	virtual Monitored::DataPtr monitor() const override {
		BasicMonitorData* p = new BasicMonitorData();
		(*p) << "<AbstractNetwork/>";
		return Monitored::DataPtr(p);
	}
};

class IServerNetworkBase : public AbstractNetwork
{
	typedef AbstractNetwork super;
public:
	IServerNetworkBase() : super() 
	{}

	virtual ~IServerNetworkBase() 
	{}


	virtual NETRET Listen(	IServerNetworkCallback *callback
							, eEngineType engineType,
							ULONG port, const char* hostName = "") = 0;
	virtual NETRET DisconnectClient(NETID, eDisconnectReason r) = 0;

};

class IServerNetwork : public IServerNetworkBase
{
public:
	virtual const URL&	authURL() const = 0;
	
	virtual std::string GetIPAddress( NETID id ) = 0;

	virtual int LogoffCleanup( int userId, enum eDisconnectReason reasonCode ) = 0;
	virtual IServerNetwork& setInstanceName(			const std::string&  instanceName		)	= 0;


	/**
	* Throttles the maximum number of inbound connections to the specified amount.
	* Note that behavior is only partially define in the event the server already has more
	* connections that is allowed.  The only guarantee made is that no more connections
	* will be accepted until the number of connections is less than the new max.
	*
	* @return reference to self
	*/
	virtual IServerNetwork&		setMaxConnections(unsigned long maxConnections) = 0;

	virtual Pinger&		pinger() = 0;
protected:
	IServerNetwork() : IServerNetworkBase() {}
};

typedef std::shared_ptr<IServerNetwork> KEPNetServerPtr;

class IClientNetwork : public AbstractNetwork
{
	typedef AbstractNetwork super;
public:
	virtual NETRET Connect( IClientNetworkCallback * callback, ULONG port, const char* hostName, BYTE * userData, DWORD userDataSize ) = 0;
	virtual IClientNetwork& synchronizeTimeoutTimeMillis(unsigned long millis) = 0;

protected:
	IClientNetwork() : super() {}
};

typedef std::shared_ptr<IClientNetwork> KEPNetClientPtr;

IClientNetwork *MakeClientNetwork( const char* unused, const char* loggerName, bool disableNATTraversal
									, INetwork::NetworkProvider	networkProvider = INetwork::defaultNetworkProvider );

KEPNetClientPtr MakeClientNetwork(bool disableNATTraversal, INetwork::NetworkProvider	neworkProvider = INetwork::defaultNetworkProvider);

// It is tempting to use a refptr here, but it would have to ripple too far out and hence is beyond the scope of this
// initiative. 
//
IServerNetwork * MakeServerNetwork(	IServerNetworkCallback*	callback
									, const PingerConfigurator&		configurator
									, const AuthConfiguration& authConf
									, const std::string&	serverInstanceName
									, const std::string&	loggerName
									, eEngineType engineType = eEngineType::Server
									, INetwork::NetworkProvider		networkProvider = INetwork::defaultNetworkProvider );

// We can however create a new signature that returns what we want.  Since I would prefer to have the 
// provider first let's simply define it and move on with testing
KEPNetServerPtr MakeServerNetwork(	IServerNetworkCallback*	callback
									, INetwork::NetworkProvider		networkProvider 
									, const PingerConfigurator&		configurator
									, const AuthConfiguration& authConf
									, const std::string&	serverInstanceName
									, const std::string&	loggerName
									, eEngineType engineType = eEngineType::Server);


} // namespace KEP
