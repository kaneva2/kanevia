///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <string>
#include <sstream>
#include "INetwork.h"
#include <KEPAuthConstants.h>
#include <Audit.h>
#include <Alert.h>
#include "NetworkStrings.h"
#include "TinyXML/tinyxml.h"
#include "Tools\NetworkV2\KEPAuthV6.h"
#include "Tools\NetworkV2\NetworkHelpers.h"

using namespace std;

namespace KEP {

void logVersion( Logger &logger ) { 
	string s =
#ifdef _AFX
		loadStr(IDS_NETWORK_VERSION);
#else
		loadStr(IDS_NETWORK_VERSION, 0, ::GetModuleHandle(0));
#endif
	LOG4CPLUS_INFO( logger, s << __TIMESTAMP__ ); 
}

static int mapResultCode( int result, bool &passwordOk, const char* userName, ULONG gameId  )
{
	int ret = LOGIN_FAILED;
	
	/*
	From Constants.cs 
	FAILED = 0,				// 0 not authenticated
	SUCCESS = 1,			// 1 successuful authentication
	USER_NOT_FOUND = 2,		// 2 user not retrieved from database
	INVALID_PASSWORD = 3,	// 3 invalid password
	NO_GAME_ACCESS = 4,		// 4 user was authenticated, but not to game supplied
	NOT_VALIDATED = 5,		// 5 user has not validated the account
	ACCOUNT_DELETED = 6,	// 6 user account was deleted
	ACCOUNT_LOCKED = 7,		// 7 user account was locked
	ALREADY_LOGGED_IN = 8   // 8 user alreay logged in, performed by DS web service
 	GAME_FULL = 9,		    // 9 Max user limit hit for the game
      NOT_AUTHORIZED = 10	    // 10 mature, age, access pass, etc.
      WRONG_SERVER = 11      // 11 mismatched server info serverId doesn't match serverId for actualIp
	*/

	// map to older return code w/o exposing KEPAUTH rcs
	switch ( result )
	{
		case KEPAUTH_FAILED:
			ret = LOGIN_FAILED;
			break;
			
		case KEPAUTH_SUCCESS:
			passwordOk = true;
			ret = LOGIN_OK;
			break;
		
		case KEPAUTH_USER_NOT_FOUND:
			ret = LOGIN_NOT_FOUND;
			break;
		
		case KEPAUTH_PASSWORD_INVALID:
			ret = LOGIN_PASSWORD_INVALID;
			break;
		
		case KEPAUTH_NO_GAME_ACCESS:
			ret = LOGIN_NO_GAME_ACCESS; 
			break;
			
		case KEPAUTH_NOT_VALIDATED:
			ret = LOGIN_NOT_VALIDATED;
			break;
						
		case KEPAUTH_ACCOUNT_DELETED:
			ret = LOGIN_DELETED;
			break;

		case KEPAUTH_ACCOUNT_LOCKED:
			ret = LOGIN_LOCKED;
			break;

		case KEPAUTH_ALREADY_LOGGED_IN:
			ret = LOGIN_ALREADY_LOGGED_ON;
			break;

		case KEPAUTH_GAME_FULL:
			ret = LOGIN_GAME_FULL;
			break;

		case KEPAUTH_NOT_AUTHORIZED:
			ret = LOGIN_NOT_AUTHORIZED;
			break;

		case KEPAUTH_WRONG_SERVER:
			ret = LOGIN_FAILED;
			break;

		default:
			// could be failed, etc. for now, we just use this 
			ret = LOGIN_NOT_FOUND;
			break;
	}

	return ret;
	
}

// common v3+ handling of response from WS
static int processLoginResponse(	HRESULT hr, CComBSTR &userInfoBSTR, 
							  const char* userName, const char* faultString,
 							  ULONG gameId, 
							  Logger &logger, 
							  int result,
							  std::string &userInfo )
{
	int ret = LOGIN_NOT_FOUND;
	static int consecutive_fail_count = 0;
	bool passwordOk = false;
	
	if ( hr != S_OK )
	{
		consecutive_fail_count++;
		std::string s = loadStrPrintf( IDS_KEPAUTH_ERROR, consecutive_fail_count, userName, hr, faultString);
		LOG4CPLUS_ERROR( logger,  s );
		ret = LOGIN_PASSWORD_INVALID; // could be trying to spoof us with bad auth ws
	}
	else 
	{
		consecutive_fail_count = 0;
		char *s;
		size_t newlen = wcslen( userInfoBSTR );
		userInfo = "<user/>";
		
		if ( newlen > 0 && newlen < 4096 ) // limit going too wild
		{
			s = new char[newlen*2+1]; // since mbcs, may not be 1:2
			
			if ( wcstombs_s( &newlen, s, newlen*2, userInfoBSTR, newlen*2 ) == 0 && 
				 strlen(s) > 0 )
			{
				userInfo = s;
			}
			
			delete[] s;
		}
			
		ret = mapResultCode( result, passwordOk, userName, gameId );
	}

	return ret;
}

int ValidateLogonViaWS( 
	const char* userName,
	const char* password,
	const char* ipAddress,
	Logger &logger, 
	const char* authServerUrl,
	ULONG gameId, 
	LONG serverId,
	std::string &userInfo 
) {
	static int authVersion = 0;

	if ( authVersion == 0 )	{
		// We have not identified the Auth protocol version.  Extract protocol version
		// from the url.
		string tempUrl( authServerUrl );
		STLToLower(tempUrl);
		string::size_type pos = tempUrl.find( "/kepauthv" );
		if ( pos != string::npos && tempUrl.size() >= pos+9 )
			authVersion = tempUrl.at(pos+9)-'0'; // convert char to number, we validate later
	}

	try {
		switch ( authVersion )	{
			case 6:			{
					string faultString;
					return KEPAuthV6::logon(
						userName, 
						ipAddress, 
						password, 
//						clientType, 
						logger, 
						authServerUrl, 
						gameId,  
						serverId, 
						faultString, 
						userInfo
					);
				}
			case 2:
			case 3:
			case 4:
			case 5:
				// no longer supported
			default:
				// Like pinger we should prolly differentiate default by indicating likely
				// invalid url.  For leave behavior as it has always been done.  report login_failed
				//
				return LOGIN_FAILED;
		}
	}
	catch( const ChainedException& exc ) {
		LOG4CPLUS_ERROR( logger,  exc.str().c_str() );
	}
	return LOGIN_FAILED;
}

int LogoutUserViaWS( int userId, 
						  enum eDisconnectReason reasonCode,
						  int serverId,
						  Logger &logger,
						  const std::string& authServerUrl )
{
//	if ( !(authServerUrl && *authServerUrl ) ) return LOGIN_NOT_FOUND;

	int			ret			= LOGIN_NOT_FOUND;
	static int	authVersion = 0;

	if ( authVersion == 0 )
	{
		string tempUrl(authServerUrl);
		STLToLower(tempUrl);
		string::size_type pos = tempUrl.find( "/kepauthv" );
		if ( pos != string::npos && tempUrl.size() >= pos+9 )
		{
			authVersion = tempUrl.at(pos+9)-'0'; // convert char to number, we validate later
		}
	}

	switch( authVersion ) {
		case 6: 
			{
				// Unlike logon, fault is handled at this level rather than calling level
				string faultString;
				ret = KEPAuthV6::logout(userId, reasonCode, serverId, logger, authServerUrl, faultString );

				if ( ret == LOGIN_FAILED )		{
					static int consecutive_fail_count = 0;
					consecutive_fail_count++;
					std::string s = loadStrPrintf( IDS_KEPAUTH_ERROR, consecutive_fail_count, numToString(userId,"%d").c_str(), ret, faultString.c_str());
					LOG4CPLUS_ERROR( logger,  s );
				}
			}
			break;
		default:
			break;
	};
	return ret;
}


AbstractResponse::AbstractResponse() : _response((GeneralizedResponse*)0) {}
AbstractResponse::~AbstractResponse() {}


/**
 * @throws GeneralizedResponseParser::Exception
 */
AbstractResponse& AbstractResponse::parse( const std::string& content ) {
	_response = GeneralizedResponseParser::parse(content);
	return *this;
}

/**
 * @throws GeneralizedResponseParser::Exception
 */
GeneralizedResponse::GeneralizedResponsePtr GeneralizedResponseParser::parse( const std::string& restResults ) {
	TiXmlDocument xmldoc;
	xmldoc.Parse( restResults.c_str() );

	TiXmlElement* resultEle	= xmldoc.FirstChildElement( "Result" );
	if ( resultEle == 0 )	throw GeneralizedResponseParser::Exception(SOURCELOCATION, "Missing results" );

	const char* pcode = resultEle->Attribute( "code" );
	if ( pcode == 0 )		throw GeneralizedResponseParser::Exception(SOURCELOCATION, "Missing response code" );

	TiXmlElement* descEle	= resultEle->FirstChildElement( "Description" );
	if ( descEle == 0 )		throw GeneralizedResponseParser::Exception(SOURCELOCATION, "Missing response description");
	
	TiXmlElement* dataEle	= resultEle->FirstChildElement( "Data" );
	stringstream ss;
	if ( dataEle > 0 )	ss << *(dataEle);

	return GeneralizedResponse::GeneralizedResponsePtr(new GeneralizedResponse(	atoi(pcode)
																, descEle->GetText()
																, ss.str() ));
}

char mapLogonStatus(int ret)
{
	switch (ret)
	{
	case LOGIN_FORCE_OFF:
	case LOGIN_OK:
	case LOGIN_NOT_FOUND:
		jsAssert(false); // these are handled not by us
		return LR_SUSPENDED;

	case LOGIN_NONPAYMENT:
	case LOGIN_SUSPENDED:
		return LR_SUSPENDED;

	case LOGIN_ALREADY_LOGGED_ON:
		return LR_ALREADY_LOGGED_ON;

	case LOGIN_SERVER_PRIVATE:
		return LR_SVR_NOT_PUBLIC;

	case LOGIN_NO_GAME_ACCESS:
		return LR_NO_GAME_ACCESS;

	case LOGIN_PASSWORD_INVALID:
		return LR_PASSWORD_INVALID;

	case LOGIN_NOT_VALIDATED:
		return LR_ACCT_HOLD;

	case LOGIN_DELETED:
		return LR_DELETED;

	case LOGIN_FAILED:
		return LR_CONNECTION_FAILED;

	case LOGIN_LOCKED:
		return LR_LOCKED;

	case LOGIN_NO_PING:
		return LR_NO_PING;

	case LOGIN_GAME_FULL:
		return LR_GAME_FULL;

	case LOGIN_NOT_AUTHORIZED:
		return LR_NOT_AUTHORIZED;

	default:
		jsAssert(false);
		return LR_SUSPENDED;

	}
}

} // namespace KEP