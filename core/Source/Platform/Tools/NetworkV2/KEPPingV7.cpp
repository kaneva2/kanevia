///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <windows.h>
#include "Common/KEPUtil/URL.h"
#include "Common/KEPUtil/Algorithms.h"
#include "Common/KEPUtil/WebCall.h"
#include "KEPPingV7.h"
#include "TinyXML/tinyxml.h"

namespace KEP {

static const std::string WEB_CALLER_KEP_PING_V7 = "KEPPingV7";

static bool         bdummy = false;
static LONG         ldummy = 0;
static int          ndummy = 0;
static Logger       nullLogger(Logger::getInstance(L"null"));

KEPPingV7::KEPPingV7()
	: m_logger(nullLogger), m_numberOfPlayers(ldummy), m_currentMaxPlayers(ldummy), m_maxAllowedPlayers(ndummy), m_adminOnly(bdummy), m_gameId( ndummy ), m_parentGameId( ndummy ), m_serverId( ndummy )
{}

std::string KEPPingV7::formatError( long code, const std::string& msg ) {
	std::stringstream ss;
	ss << "faultString: HTTPErr: " << code << " detail: " << msg;
	return ss.str();
}

PingInitializeResponse::PingInitializeResponse() : AbstractResponse()
		, _serverId(0)
		, _maxCount(0)
		, _gid(0)
		, _pgid(0)
		, _status(false)
		, _gname()
		, _hash()
{}

PingInitializeResponse::~PingInitializeResponse() {}

/**
 * @throws	GeneralizedReponseParser::Exception
 */
PingInitializeResponse& PingInitializeResponse::parse( const std::string& content ) {
	AbstractResponse::parse(content);

	// Our data will be contained in the payload or Data section of the response
	// which, if it exists should contained as an XML string in _data;
	//
	TiXmlDocument doc;
	doc.Parse(data().c_str() );
	TiXmlElement* pele = doc.FirstChildElement("Data");
	if ( pele == 0 ) return *this;

	const char* serverId	= pele->Attribute("serverId");
	if ( serverId == 0 )	throw GeneralizedResponseParser::Exception();
	const char* maxCount	= pele->Attribute("maxCount" );		if ( serverId == 0 )	throw GeneralizedResponseParser::Exception();
	const char* status		= pele->Attribute("status" );		if ( status == 0 )		throw GeneralizedResponseParser::Exception();
	const char* gid			= pele->Attribute("gameId" );		if ( gid == 0 )			throw GeneralizedResponseParser::Exception();
	const char* pgid		= pele->Attribute("parentGameId");	if ( pgid == 0 )		throw GeneralizedResponseParser::Exception();
	const char* gname		= pele->Attribute( "gameName" );	if ( gname == 0 )		throw GeneralizedResponseParser::Exception();

	const char* hash		= pele->GetText();
	if ( hash != 0 )		_hash		= hash;

	_serverId	= atoi(serverId);
	_maxCount	= atoi(maxCount);
	_gid		= atoi(gid);
	_pgid		= atoi(pgid);
	_status		= (_strcmpi( status, "True" ) == 0);
	_gname		= gname;

	return *this;
}

PingGetCountResponse::PingGetCountResponse() : AbstractResponse()
		, _status(false)
		, _maxPlayerCount(0)
		, _hash()
{}

/**
	<Result code="0">
		<Description>OK</Description>
		<Data  maxNumberOfPlayers="2000"  status="True">8f9593ea20c403f5a8afec655b9c8f18</Data></Result>"
	@throws GeneralizedResponseParser::Exception
*/
PingGetCountResponse& PingGetCountResponse::parse( const std::string& content ) {
	AbstractResponse::parse(content);

	// Our data will be contained in the payload or Data section of the response
	// which, if it exists should contained as an XML string in _data;
	//
	TiXmlDocument doc;
	doc.Parse(data().c_str() );
	TiXmlElement* pele		= doc.FirstChildElement("Data");
	if ( pele == 0 ) return *this;

	const char* hash		= pele->GetText();
	if ( hash != 0 )		_hash		= hash;

	const char* maxPlayers = pele->Attribute( "maxNumberOfPlayers" );	if ( maxPlayers == 0 )	throw GeneralizedResponseParser::Exception();
	const char* status = pele->Attribute( "status" );					if ( status  == 0 )		throw GeneralizedResponseParser::Exception();

	this->_maxPlayerCount = atoi(maxPlayers);
	_status  = _strcmpi( status, "true" ) == 0 ? true : false;
	return *this;
}

unsigned long		PingGetCountResponse::maxPlayers() const { return _maxPlayerCount; }
const std::string&	PingGetCountResponse::hash() const { return _hash; }
bool				PingGetCountResponse::status() const { return _status; }

PingPingResponse::PingPingResponse() 
	:	AbstractResponse()
		, _hash()
{}

/**
 * @throws GeneralizedResponseParser::Exception
 */
PingPingResponse& PingPingResponse::parse( const std::string& content ) {
	AbstractResponse::parse(content);

	// Our data will be contained in the payload or Data section of the response
	// which, if it exists should contained as an XML string in _data;
	//
	TiXmlDocument doc;
	doc.Parse(data().c_str() );
	TiXmlElement* pele = doc.FirstChildElement("Data");
	if ( pele == 0 ) return *this;
	const char* hash		= pele->GetText();
	if ( hash != 0 )		_hash		= hash;

	return *this;
}

const std::string& PingPingResponse::hash() const {	return _hash;}

HRESULT KEPPingV7::sendInitialize(const std::string& s // not used.
									, int&			newMaxAllowedPlayers
									, bool&			active
									, std::string&	gameName
									, std::string&	resultString
									, std::string&	faultString )   {
	active					= false;
	gameName				= "";
	resultString			= "";
	faultString				= "";
	newMaxAllowedPlayers	= 1;
	m_serverId				= 0;
	m_gameId				= 0;
	m_parentGameId			= 0;
	
	try {
		// KEPPing/KEPPingV7.aspx?action=Initialize&key=whatsthis&hostname=host&watermark=wm&type=1&ipAddress=10.10.10.10&port=80&protocolVersion=1&schemaVersion=1&serverVersion=1&assetVersion=1
		URL url = URL::parse(std::string(m_pingServerUrl) );
		url.setParam( "action",				"Initialize" )
		   .setParam( "key",				m_regKey )
	       .setParam( "hostname",			m_hostName )
	       .setParam( "watermark",			m_waterMark )
	       .setParam( "type",				StringHelpers::parseNumber<long int>((long) m_engineType )) // should likely be unsigned, but for now match current var type
	       .setParam( "ipAddress",			m_ipAddress )
	       .setParam( "port",				StringHelpers::parseNumber<unsigned short>(m_port ))
	       .setParam( "protocolVersion",	StringHelpers::parseNumber<unsigned long>(m_protocolVersion) )
	       .setParam( "schemaVersion",		StringHelpers::parseNumber<long>(m_schemaVersion ) )
	       .setParam( "serverVersion",		m_serverVersion )
	       .setParam( "assetVersion",		StringHelpers::parseNumber<unsigned long>(m_assetVersion ) );

        // Ping Web Call
		WebCallOpt wco;
        wco.url         = url.str();
        wco.response    = true;
        wco.timeoutMs   = m_timeoutMs;
        WebCallActPtr p = WebCaller::WebCallAndWait(WEB_CALLER_KEP_PING_V7, wco);
        if ( p->IsError() ) {
            faultString = formatError(p->ErrorCode(), p->ErrorStr() ); 
            return p->ErrorCode();
        }
        if ( p->StatusCode() != 200 ) {
            faultString = formatError( p->StatusCode(), p->StatusStr() );
            return p->StatusCode();
        }
		PingInitializeResponse parser;
		parser.parse( p->ResponseStr() );

		if ( parser.code() != 0 ) {
			faultString = parser.message();
			return S_FALSE;
		}

		resultString			= parser.hash();
		newMaxAllowedPlayers	= parser.maxCount();
		gameName				= parser.gameName();
		m_gameId				= parser.gameId();
		m_serverId				= parser.serverId();
		m_parentGameId			= parser.parentGameId();
		active					= parser.status();
		return S_OK;
	}
	catch ( const ChainedException& exc  ) {
		throw KEPPingException( SOURCELOCATION, "Failed to initialize!" ).chain( exc );
	}
}

HRESULT KEPPingV7::sendPing(	EServerState	state
								, char*			s
								, std::string&	resultString
								, std::string&	faultString )	{
	try {
		HRESULT hr = S_OK;

		// Would be nice if we had a URL parser/builder
		// NOTE: That in this case "action" is lower case per JB.  This certainly can't be.
		//
		//		KEPPing/KEPPingV7.aspx?action=Ping&serverId=1&port=1&statusId=1&numberOfPlayers=5&maxNumberOfPlayers=100&adminOnly=false
		//
		URL url = URL::parse( m_pingServerUrl );

		std::stringstream ss(m_pingServerUrl);
		url.setParam( "action",				"Ping" )
		   .setParam( "serverId",				StringHelpers::parseNumber<int>(m_serverId) )
		   .setParam( "port",					StringHelpers::parseNumber<unsigned short>(m_port) )
		   .setParam( "statusId",				StringHelpers::parseNumber<int>(state) )
		   .setParam( "numberOfPlayers",		StringHelpers::parseNumber<long>(m_numberOfPlayers) )
		   .setParam( "maxNumberOfPlayers",		StringHelpers::parseNumber<long>(std::min((long)m_currentMaxPlayers, (long)m_maxAllowedPlayers)) )
		   .setParam( "adminOnly",  (m_adminOnly ? "true" : "false") );

		// Ping Web Call
		WebCallOpt wco;
        wco.url         = url.toString();
        wco.response    = true;
        wco.timeoutMs   = m_timeoutMs;
        WebCallActPtr p = WebCaller::WebCallAndWait(WEB_CALLER_KEP_PING_V7, wco);
        if ( p->IsError() ) {
            faultString = formatError(p->ErrorCode(), p->ErrorStr() ); 
            return p->ErrorCode();
        }
        if ( p->StatusCode() != 200 ) {
            faultString = formatError( p->StatusCode(), p->StatusStr() );
            return p->StatusCode();
        }


		PingPingResponse response;
		response.parse( p->ResponseStr() );

		if ( response.code() != 0 ) {
			faultString = response.message();
			return S_FALSE;
		}

		resultString = response.hash();
		return hr;
	}
	catch( const ChainedException& exc ) {
		throw KEPPingException().chain( exc );
	}
} 

HRESULT KEPPingV7::sendGetCount(	EServerState	state
									, char*			s
									, int&			newMaxAllowedPlayers
									, bool&			active
									, std::string&	resultString
									, std::string&	faultString )		{
	try {
		HRESULT hr = S_OK;

		URL url = URL::parse( m_pingServerUrl );
		url.setParam( "Action",				"GetCount" )
		   .setParam( "key",				m_regKey )
		   .setParam( "serverId",			StringHelpers::parseNumber<long>( m_serverId ) )
		   .setParam( "port",				StringHelpers::parseNumber<unsigned short>( m_port ) )
		   .setParam( "statusId",			StringHelpers::parseNumber<int>( state ) )
		   .setParam( "numberOfPlayers",	StringHelpers::parseNumber<long>( m_numberOfPlayers ) )
		   .setParam( "adminonly",			(m_adminOnly ? "true" : "false") );

		// Ping Web Call
		WebCallOpt wco;
        wco.url         = url.toString();
        wco.response    = true;
        wco.timeoutMs   = m_timeoutMs;
        WebCallActPtr p = WebCaller::WebCallAndWait(WEB_CALLER_KEP_PING_V7, wco);
        if ( p->IsError() ) {
            faultString = formatError(p->ErrorCode(), p->ErrorStr() ); 
            return p->ErrorCode();
        }
        if ( p->StatusCode() != 200 ) {
            faultString = formatError( p->StatusCode(), p->StatusStr() );
            return p->StatusCode();
        }
		PingGetCountResponse response;
		response.parse( p->ResponseStr() );

		if ( response.code() != 0 ) {
			faultString = response.message();
			return S_FALSE;
		}
		resultString			= response.hash();
		active					= response.status();
		newMaxAllowedPlayers	= response.maxPlayers();
		return hr;
	}
	catch( const ChainedException& exc ) {
		throw KEPPingException().chain( exc );
	}
}

} // namespace KEP