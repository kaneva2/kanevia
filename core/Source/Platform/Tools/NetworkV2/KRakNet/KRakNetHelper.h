///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

class KRakNetHelper {
  public:
	static std::string describeBuffer(const unsigned char*	data
	                             , unsigned long length
	                             , bool appendLineFeed = true)	{
		std::stringstream ss;
		ss << "[" << (int)data[0] << "][" << RakNetNameForId(data[0]) << "]" << std::endl;
		ByteBufRuler::formatTo(ss, data, length);
		if (appendLineFeed)
			ss << std::endl;
		return ss.str();
	}
};

} // namespace KEP