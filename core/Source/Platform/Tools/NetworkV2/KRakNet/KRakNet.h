///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#ifdef _DEBUG
#pragma comment(lib, "RakNet_VS2015_LibStatic_Debug_Win32.lib" )
#else
#pragma comment(lib, "RakNet_VS2015_LibStatic_Release_Win32.lib" )
#endif


