///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common/keputil/CommLogger.h"
#include "KRakNetComponent.h"

namespace KEP {

static CommLogger		commLogger;
const PacketPriority	Priority = IMMEDIATE_PRIORITY;


KRakNetComponent::KRakNetComponent(const std::string& name /*= "<unnamed>" */)
	: peer(0)
	  , _startACT(new ACT())
	  , _stopACT(new ACT())
	  , _stop(false)
	  , _RakNetSignal(::CreateEvent(0, false, false, 0)) // auto reset
	  , _misfires(0)
	  , _name(name)

{

	peer = RakNet::RakPeerInterface::GetInstance();
#ifdef KRAKNET_EXPOSE_RAKPLUGINPEERINTERFACE
	peer->AttachPlugin(this);
#endif
	peer->setReceiveEvent(_RakNetSignal);

	// Note that this will turn down sensitivity.  I.e. I can't readily descriminate
	// timeout between blockage due to developer vs blockage due to server down.
	// So, by doing this we increase the likelihood that clients won't detect a dropped server.
	//
	// It would appear that this method allows us to descriminate which connection (for server side).
	// So, if we can find a way to communicate this to the server, we'll be golden.
	//
	// Initial steps would be to at least allow the configuration on both end points.	
#ifdef _DEBUG
	peer->SetTimeoutTime(300000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);
#else
	peer->SetTimeoutTime(30000, RakNet::UNASSIGNED_SYSTEM_ADDRESS); 
#endif
}

KRakNetComponent::~KRakNetComponent()	{
	if (peer->IsActive())
		// Unfortunately, the time it takes for the polling thread
		// to even detect the shutdown is relatively non-deterministic.
		// We have taken steps to insure that the poll exits ASAP
		// but this may not be quick enough depending on what is
		// actually happening in the loop.
		// For the sake of avoiding a nasty NPE we say we will wait as long
		// 10seconds before we raise the alarm. This should rarely be the case.
		//
		if (stopOrThrow()->wait(10000) != WR_OK)
			// should just crash most likely
			// For now do nothing
			Algorithms::Noop();


	peer->setReceiveEvent(nullptr);

	RakNet::RakPeerInterface::DestroyInstance(peer);


	// if we set it here, exception raised because peer has already been destroyed.
	// if we place it before the destoy call,

	::CloseHandle(_RakNetSignal);

	if (_t.joinable()) 
		_t.join();
}

KRakNetComponent&
KRakNetComponent::SendOrThrow(	const RakNet::AddressOrGUID&	addr
								, const unsigned char*			buf
								, size_t						bufsize
								, KRakNetId						packetTypeId	/* = KanevaPacket*/
								, bool							broadcast /*= false */)	{

	//	commLogger.logSend(0, 0, buf, bufsize, "KRAKNET");

	RakNet::BitStream bs;
	bs.Write(static_cast<unsigned char>(packetTypeId));
	bs.WriteAlignedBytes(buf, bufsize);
	return SendOrThrow(bs, addr, broadcast);
}

KRakNetComponent&
KRakNetComponent::SendOrThrow(	const RakNet::BitStream&		bs
                        , const RakNet::AddressOrGUID&	addr
                        , bool							broadcast /* = false */ )				{
	commLogger.logSend(RakNet::RakNetGUID::ToUint32(addr.rakNetGuid), 0, bs.GetData(), bs.GetNumberOfBytesUsed(), "KRAKNET");
	int ret = peer->Send(	&bs
	                        , Priority								// understood
	                        , RELIABLE_ORDERED							// understood
	                        //, IMMEDIATE_PRIORITY								// understood
	                        //, UNRELIABLE							// understood
	                        , 0											// ????
	                        , addr
	                        , broadcast);									// broadcast flag
	if (ret <= 0)
		throw KRakNetException(SOURCELOCATION, ErrorSpec(NET_EXCEPTION, "Failed to send!"));

	return *this;
}

Monitored::DataPtr KRakNetComponent::monitor() const {
	BasicMonitorData* p = new BasicMonitorData();
	(*p) << "<KRakNetComponent>"
		<< "<ConnectionCount>"
		<< peer->NumberOfConnections()
		<< "</ConnectionCount>"
		<< "</KRakNetComponent>";
	return Monitored::DataPtr(p);
}

const std::string&
KRakNetComponent::Name() const {
	return _name;
}

KRakNetComponent&
KRakNetComponent::setName(const std::string name)	{
	_name = name;
	return *this;
}

unsigned short
KRakNetComponent::port() const	{
	return peer->GetMyBoundAddress().GetPort();
}

ACTPtr
KRakNetComponent::startOrThrow()	{
	_t = std::thread(&KRakNetComponent::work, this);
	OS::setThreadName(_t, _name);
	return _startACT;
}

ACTPtr
KRakNetComponent::stopOrThrow()	{
	_stop = true;
	::SetEvent(_RakNetSignal);
	return _stopACT;
}

unsigned long
KRakNetComponent::work() {
	_startACT->complete();

	while (!_stop) {
		switch (::WaitForSingleObject(_RakNetSignal, INFINITE /*timeoutMS*/ )) {
			case WAIT_OBJECT_0:
				break;		// breaks the switch
			default:
				continue; //  return WR_ERROR;
		};

		// Who knows how long we've been waiting.  Let's see if anyone
		// instructed the component to stop
		//
		if (_stop)
			break;

		if (!poll())
			_misfires++;
	}
	_stopACT->complete();
	return 0;
}

unsigned long 
KRakNetComponent::timeoutTimeMillisOrThrow(NETID netid)  const //- Cause compile to fail because
												 // RakPeer::GetTimeoutTime isn't declared as const.
{
	NetIdMapT::const_iterator iter = _idmap.find(netid);
	if (iter == _idmap.cend() )
		throw KRakNetException(SOURCELOCATION, ErrorSpec(-1) << "Peer with id [" << netid << "] not found!");
	return peer->GetTimeoutTime(iter->second.systemAddress	);
}

KRakNetComponent&
KRakNetComponent::setTimeoutTimeMillisOrThrow(unsigned long millis, NETID netid )	{
	peer->SetTimeoutTime(millis, resolveAddressOrThrow(netid).systemAddress);
	return *this;
}

KRakNetComponent& 
KRakNetComponent::mapAddress(RakNet::RakNetGUID guid) {
	auto netId = RakNet::RakNetGUID::ToUint32(guid);
	_idmap.insert(NetIdMapEntryT(netId, RakNet::AddressOrGUID(guid)));
	return *this;
}

NETID
KRakNetComponent::guid2NETID(RakNet::RakNetGUID guid) {
	return RakNet::RakNetGUID::ToUint32(guid);
}

KRakNetComponent& 
KRakNetComponent::unmapAddress(NETID netid)
{
	_idmap.erase(netid);
	return *this;
}

RakNet::AddressOrGUID 
KRakNetComponent::resolveAddressOrThrow(NETID netid) const	{
	NetIdMapT::const_iterator iter = _idmap.find(netid);
	if (iter == _idmap.cend())
		throw KRakNetException(SOURCELOCATION, ErrorSpec(-1) << "Peer with id [" << netid << "] not found!");
	return iter->second;
}

} // namespace KEP