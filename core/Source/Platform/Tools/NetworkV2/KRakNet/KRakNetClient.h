///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Core/Util/AsynchronousCompletionToken.h"
#include "Tools/NetworkV2/KRakNet/KRakNetComponent.h"
#include "Tools/NetworkV2/INetwork.h"
#include "Tools/NetworkV2/KEPNet/NetworkBuffer.h"

namespace KEP {

const std::string StateName[] = {
	"Instantiated"
	, "xInitialized"
	, "Started"
	, "Connecting"
	, "Connected"
};


class KRakNetClient : public KRakNetComponent {
  public:
	KRakNetClient();
	KRakNetClient(std::string name);
	virtual ~KRakNetClient();

	KRakNetClient&			setHandler(IClientNetworkCallback&			handler				);
	virtual ACTPtr			startOrThrow() override;
	ACTPtr					connectOrThrow(	const std::string&				remoteIPAddress
											, unsigned short				port				);

	ACTPtr					disconnect(void);

	virtual KRakNetClient&	SendOrThrow(	const							unsigned char* buf
											, size_t						bufsize
											, KRakNetId						packetTypeId = KanevaPacket
	                                        , bool							broadcast = false	);

	virtual KRakNetClient&	SendOrThrow(	const RakNet::BitStream&		bs
											, bool							broadcast = false);

	virtual KRakNetClient&	SendOrThrow(	const RakNet::BitStream&		bs
											, const RakNet::AddressOrGUID&	addr
											, bool							broadcast = false	) override;


	// For now assume that server receives and sets
	// A more complete solution would be and ackack
	// Also, if and when we implement a server side analog to this method
	// we may want to modify to check for a veto response from the
	// server.
	//
	// We are in very early stages of this development so for now we'll simply
	// rely on the guaranteedness of RakNet
	//
	// Or possible mod, implement dupack
	// Prefereable to bind this to a response listener.  If no ack in 
	// specified time, veto is assumed.
	//
	virtual KRakNetClient& synchronizeTimeoutTimeMillisOrThrow(unsigned long millis);
	//{

	//	// This scatter logic should be co-located with the gather logic.
	//	// This marshalling packaged with the protocol
	//	//
	//	NetworkBuffer buffer(255);
	//	buffer << (unsigned char)0xaa; // version. (inactive)
	//	buffer << (unsigned short)0xbbbb; // sequence.  (inactive)
	//	buffer << "SYNT";
	//	buffer << millis;
	//	Send(buffer.buf(), buffer.size(),KRakNetPacket );
	//	KRakNetComponent::setTimeoutTimeMillisOrThrow(millis, guid2NETID(_serverGUID));

	//	return *this;
	//}

	//KRakNetClient& synchronizeTimeoutTimeMillis(unsigned long TimeoutTimeMillis) {
	//	// Write a buffer with the KRakNet message id
	//	//
	//	NetworkBuffer buf(255);
	//	buf << "CCP"
	//		<< (unsigned char)0   // the protocol version
	//		<< (unsigned short)0; // sequence
	//	Send((const unsigned char*)buf.buf(), buf.size());
	//	return *this;
	//}

	NETID serverId()	{
		return RakNet::RakNetGUID::ToUint32(_serverGUID);
	}

	NETID KRakNetClient::netId() const			{
		return guid2NETID(peer->GetMyGUID());
	}

	unsigned long timeoutTimeMillisOrThrow(NETID netid = SERVER_NETID) const override;
	KRakNetClient& setTimeoutTimeMillisOrThrow(unsigned long millis, NETID netid = SERVER_NETID) override;

  protected:
	const RakNet::RakNetGUID& serverGUID() {
		return this->_serverGUID;
	}
	virtual KRakNetClient& handleUnrecognizedPacket(	RakNet::Packet*					packet				) override;
	virtual KRakNetClient& handleInboundPacket(		RakNet::Packet*					packet				) override;
	virtual KRakNetClient& handleConnectCompletion(	RakNet::Packet*					packet				);
	virtual KRakNetClient& handleLostConnection(		RakNet::Packet*					packet				);
	virtual KRakNetClient& handleEvictedConnection(	RakNet::Packet*					packet				);

	enum State	{
		Instantiated
		, xInitialized
		, Started
		, Connecting
		, Connected
	};

	State state() const;
	const std::string& stateName(State) const;

  private:
	/**
	* Returns flag indicating that the polling loop should exit
	*/
	bool					poll();


	// Step 1: Isolate access to the state.  Someone is setting it and it's not clear
	//			who.
	State					controlledaccess_state;

	KRakNetClient& setState(State s);


	ACTPtr					_connectACT;
	ACTPtr					_disconnectACT;
	bool						_stop;
	RakNet::RakNetGUID			_serverGUID;
	RakNet::SystemAddress		_serverAddress;
	IClientNetworkCallback*_handler;
	fast_mutex				_sync;
};

} // namespace KEP