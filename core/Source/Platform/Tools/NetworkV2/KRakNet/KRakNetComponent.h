///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "Core/Util/ChainedException.h"
#include "Core/Util/AsynchronousCompletionToken.h"
#include "RakNet/RakPeer.h"
#include "RakNet/MessageIdentifiers.h"
#include "Core/Util/Runnable.h"
#include "Core/Util/ThreadPool.h"
#include "Common/KEPUtil/SynchronizedMap.h"

#include "Tools/NetworkV2/KRakNet/KRakNet.h"
#include "Tools/NetworkV2/KRakNet/RakNetMessages.h"
#include "Tools/NetworkV2/KRakNet/KRakNetHelper.h"
#include "RakNet/RakNetTypes.h"
#include "RakNet/PluginInterface2.h"
#include <common/include/LogHelper.h>

namespace KEP {

CHAINED(KRakNetException);


//#define RKVERBOSE

// Okay, in a single threaded pool we cannot stop the thread pool from one of the thread pool threads.
// No really nice way to get the thread pool shut down.  At least not without somemechanism like a compound ACT
// In reality however, thread pool was like using a cannon where a pea shooter would have sufficed.  Look at
// ThreadExecutor
//
// Packet Id definitions.  In RakNet the first byte is used as a discriminator.  The possible values for
// this byte are defined in the RakNet file MessageIdentifiers.h (found in tools).  The intent is that
// implementers of RakNet based systems would append there specific id's to the end of their enumeration
// (ID_USER_PACKET_ENUM) The value of this id is currently in the 140's, it assumed that RakNet can
// always add more ids into their part of the space.  With on 255 possible values, this means that are
// a little over 100 available ids.  If we  were to implement a real protocol with this, we could
// easily deplete the remaining values.  For this reason, it would have been my preference to use the
// remaining space to identify "families" of messages or protocol families (KEP being one), and then
// wrap each of the protocol families up in their own carrier.
//
// However, the reality is this an evolving solution and to take this short cut (adding discriminators
// to the packet id list) to be just fine for now.
//
enum KRakNetId {
	KanevaPacket = ID_USER_PACKET_ENUM

	// Specilizations.  I would prefer a carrier protocol we we make these
	// distinctions in packet types, and someday will be able to justify
	// putting it in place.  However for now, RakNet supports this ready
	// extension, lets use it.
	//
	, KEPAuthRequest
	, KEPAuthResponse

	// Packet destined for process by the KRakNet layer.
	// 
	, KRakNetPacket
};


/**
 * KRakNetComponent serves as a base class for both KRakNetServer and KRakNetClient.
 * As such it contains common logic and data (e.g. Polling loop with it's thread management,
 * logical name, port binding etc.
 */
class KRakNetComponent : public Monitored
#ifdef KRAKNET_EXPOSE_RAKPLUGINPEERINTERFACE
	, public  RakNet::PluginInterface2
#endif
{
  public:
    /**
	 * Construct with string specifying a logical name for
	 * this component.
	 * @param name		The logical name of this component.
	 *					Defaults to "<unnamed>"
	 */
	KRakNetComponent(const std::string& name = "<unnamed>");

	virtual ~KRakNetComponent();

	/**
	 * @see Monitored
	 */
	virtual Monitored::DataPtr monitor() const override;

	/**
	 * Give the component a logical name for logging/monitor/debug.
	 *
	 * @param name		The new name of the component. 
	 */
	KRakNetComponent& setName(const std::string name);

	/**
	 * Acquire the components logical name. Note that the default
	 * name is "<unnamed>".
	 */
	virtual const std::string&			Name() const;

	/**
	 * Identify the port being used for network traffic.
	 */
	unsigned short port() const;

	// Note some confusion here as timeout time can mean very different things depending on whether the
	// component is acting as a server or as a client.  
	// For example for a client, that is exclusively bound to a single server, then we are clearly talking about
	// the connection to the server.  In a server component, setting this generally, will cause the timeout
	// to be applied to subsequent connections.  Server version of this would a net id to determine which
	// peer should be set.
	// To this end this base method will take a net id
	//


	/**
	 * KRakNet allows for some tolerance of latency added to the pipeline (i.e. blocked in debugger).
	 * Set this tolerance by indicating
	 * how long the connection will be able to stay intact (in millis), in the absence of
	 * being able to send a guaranteed message. Hence 0 = zero tolerance.
	 *
	 * @return reference to self
	 */
	virtual KRakNetComponent& setTimeoutTimeMillisOrThrow(unsigned long millis, NETID netid = SERVER_NETID);

	/**
	 * Obtain the timeout value of the connection identified by netId
	 * @param netid   The NETID of the connection in question.  Defaults to server connection
	 * @throw KRakNetException in the event the timeout cannot be acquired.  This failure is
	 * most likely due to an invalid netid.
	 */
	virtual unsigned long timeoutTimeMillisOrThrow(NETID netid = SERVER_NETID) const;

	/**
     * Start the component's handler thread.
	 * @throws KRakNetException in the event of a failure.
	 */
	virtual ACTPtr				startOrThrow();

	/**
	 * Stop the components handler thread.
	 * @throw KRakNetException in the event of a failure.
	 */
	ACTPtr						stopOrThrow();

protected:
	// Derivatives need access to the RakNet Peer structure.
	// For now simply leave protected
	//
	RakNet::RakPeerInterface* peer;

	bool stopping() const {
		return _stop;
	};

	/**
	* Note that KanevaPacket type is implied.  That is to say that all code above
	* this level need not worry about the packet type unless the code is using
	* the BitStream overload.
	*/
	virtual KRakNetComponent& SendOrThrow(	const RakNet::AddressOrGUID&	addr
											, const unsigned char*			buf
											, size_t						bufsize
											, KRakNetId						packetTypeId = KanevaPacket
											, bool							broadcast = false);

	/**
	* Note that it is incumbent of callers of this method to place the packet type
	* in the first byte.
	*/
	virtual KRakNetComponent& SendOrThrow(	const RakNet::BitStream&		bs
											, const RakNet::AddressOrGUID&	addr
											, bool							broadcast = false);


	virtual KRakNetComponent& handleInboundPacket(		RakNet::Packet*			packet) = 0;
	virtual KRakNetComponent& handleUnrecognizedPacket(	RakNet::Packet*			packet) = 0;

	KRakNetComponent&
	mapAddress(RakNet::AddressOrGUID addr);



  private:
	  virtual bool				poll() = 0;


	ACTPtr		_startACT;
	ACTPtr		_stopACT;
	std::string		_name;
	bool			_stop;
	HANDLE          _RakNetSignal;

	virtual unsigned long		work();
	std::thread		_t;

	// Collection to map long representation of a RakNet Address to the actual
	// RakNet Address. This id done because our system is rife with references to NETID
	// which is compatible with the long representation of RakNet Address.
	// Given a long value we can't derive the address, hence we have to perform this
	// mapping ourselves.
	//
	// The significance of entries in to this map depends on whether this is a server
	// or a client.  If it is a server, this collection references all of the connected clients.
	// If this is a client, it should only hold a single guid
	typedef SynchronizedMap<NETID, RakNet::AddressOrGUID>	NetIdMapT;
	typedef std::pair<NETID,	RakNet::AddressOrGUID>		NetIdMapEntryT;
	NetIdMapT												_idmap;
protected:
	RakNet::AddressOrGUID	resolveAddressOrThrow(NETID netid) const;
	KRakNetComponent&		unmapAddress(NETID netid);
	KRakNetComponent&		mapAddress(RakNet::RakNetGUID guid);
	static NETID		guid2NETID(RakNet::RakNetGUID guid); // { return ::RakNetGUID::ToUint32(guid)
private:

	unsigned long _misfires;
};


#ifdef KRAKNET_EXPOSE_RAKPLUGINPEERINTERFACE

/// System called RakPeerInterface::PushBackPacket
/// \param[in] data The data being sent
/// \param[in] bitsUsed How many bits long \a data is
/// \param[in] remoteSystemAddress The player we sent or got this packet from
virtual void
OnPushBackPacket(const char* data, const RakNet::BitSize_t bitsUsed, RakNet::SystemAddress remoteSystemAddress) {
	cout << "OnPushBackPacket()" << endl;
	::SetEvent(_RakNetSignal);
}

/// Called when we got a new connection
/// \param[in] systemAddress Address of the new connection
/// \param[in] rakNetGuid The guid of the specified system
/// \param[in] isIncoming If true, this is ID_NEW_INCOMING_CONNECTION, or the equivalent
virtual void
OnNewConnection(const RakNet::SystemAddress& systemAddress, RakNet::RakNetGUID rakNetGUID, bool isIncoming) {
	::SetEvent(_RakNetSignal);
	cout << "OnNewConnection()" << endl;
}

/// OnReceive is called for every packet.
/// \param[in] packet the packet that is being returned to the user
/// \return True to allow the game and other plugins to get this message, false to absorb it
virtual RakNet::PluginReceiveResult
OnReceive(RakNet::Packet* packet) {
	cout << "OnReceive()" << endl;
	::SetEvent(_RakNetSignal);
	return RakNet::RR_CONTINUE_PROCESSING;
}

/// Called when the interface is attached
virtual void OnAttach(void) override {
	cout << "OnAttach()" << endl;
}

/// Called when the interface is detached
virtual void OnDetach(void) override {
	cout << "OnDetach()" << endl;
}

/// Update is called every time a packet is checked for .
virtual void Update(void) override {
	cout << "Update()" << endl;
}

/// Called when RakPeer is initialized
virtual void OnRakPeerStartup(void) override {
	cout << "OnRakPeerStartup()" << endl;
}

/// Called when RakPeer is shutdown
virtual void OnRakPeerShutdown(void) override {
	cout << "OnRakPeerShutdown()" << endl;
}

/// Called when a connection is dropped because the user called RakPeer::CloseConnection() for a particular system
/// \param[in] systemAddress The system whose connection was closed
/// \param[in] rakNetGuid The guid of the specified system
/// \param[in] lostConnectionReason How the connection was closed: manually, connection lost, or notification of disconnection
virtual void OnClosedConnection(const RakNet::SystemAddress& systemAddress, RakNet::RakNetGUID rakNetGUID, RakNet::PI2_LostConnectionReason lostConnectionReason) override {
	(void)systemAddress;
	(void)rakNetGUID;
	(void)lostConnectionReason;
	cout << "OnClosedConnection()" << endl;
}

/// Called when we got a new connection
/// \param[in] systemAddress Address of the new connection
/// \param[in] rakNetGuid The guid of the specified system
/// \param[in] isIncoming If true, this is ID_NEW_INCOMING_CONNECTION, or the equivalent
//virtual void OnNewConnection(const RakNet::SystemAddress &systemAddress, RakNet::RakNetGUID rakNetGUID, bool isIncoming) override {
//	(void)systemAddress; (void)rakNetGUID; (void)isIncoming;
//	cout << "OnNewConnection()" << endl;
//}

/// Called when a connection attempt fails
/// \param[in] packet Packet to be returned to the user
/// \param[in] failedConnectionReason Why the connection failed
virtual void OnFailedConnectionAttempt(RakNet::Packet* packet, RakNet::PI2_FailedConnectionAttemptReason failedConnectionAttemptReason) override {
	(void)packet;
	(void)failedConnectionAttemptReason;
	cout << "OnFailedConnectionAttempt()" << endl;
}

/// Queried when attached to RakPeer
/// Return true to call OnDirectSocketSend(), OnDirectSocketReceive(), OnReliabilityLayerNotification(), OnInternalPacket(), and OnAck()
/// If true, then you cannot call RakPeer::AttachPlugin() or RakPeer::DetachPlugin() for this plugin, while RakPeer is active
virtual bool UsesReliabilityLayer(void) const override {
	cout << "UsesReliabilityLayer()" << endl;
	return false;
}

/// Called on a send to the socket, per datagram, that does not go through the reliability layer
/// \pre To be called, UsesReliabilityLayer() must return true
/// \param[in] data The data being sent
/// \param[in] bitsUsed How many bits long \a data is
/// \param[in] remoteSystemAddress Which system this message is being sent to
virtual void OnDirectSocketSend(const char* data, const RakNet::BitSize_t bitsUsed, RakNet::SystemAddress remoteSystemAddress) override {
	(void)data;
	(void)bitsUsed;
	(void)remoteSystemAddress;
	cout << "OnDirectSocketSend()" << endl;
}

/// Called on a receive from the socket, per datagram, that does not go through the reliability layer
/// \pre To be called, UsesReliabilityLayer() must return true
/// \param[in] data The data being sent
/// \param[in] bitsUsed How many bits long \a data is
/// \param[in] remoteSystemAddress Which system this message is being sent to
virtual void OnDirectSocketReceive(const char* data, const RakNet::BitSize_t bitsUsed, RakNet::SystemAddress remoteSystemAddress) override {
	(void)data;
	(void)bitsUsed;
	(void)remoteSystemAddress;
	cout << "OnDirectSocketReceive()" << endl;
}

/// Called when the reliability layer rejects a send or receive
/// \pre To be called, UsesReliabilityLayer() must return true
/// \param[in] bitsUsed How many bits long \a data is
/// \param[in] remoteSystemAddress Which system this message is being sent to
virtual void OnReliabilityLayerNotification(const char* errorMessage, const RakNet::BitSize_t bitsUsed, RakNet::SystemAddress remoteSystemAddress, bool isError)  override {
	(void)errorMessage;
	(void)bitsUsed;
	(void)remoteSystemAddress;
	(void)isError;
	cout << "OnReliabilityLayerNotification()" << endl;
}

/// Called on a send or receive of a message within the reliability layer
/// \pre To be called, UsesReliabilityLayer() must return true
/// \param[in] internalPacket The user message, along with all send data.
/// \param[in] frameNumber The number of frames sent or received so far for this player depending on \a isSend .  Indicates the frame of this user message.
/// \param[in] remoteSystemAddress The player we sent or got this packet from
/// \param[in] time The current time as returned by RakNet::GetTimeMS()
/// \param[in] isSend Is this callback representing a send event or receive event?
virtual void OnInternalPacket(RakNet::InternalPacket* internalPacket, unsigned frameNumber, RakNet::SystemAddress remoteSystemAddress, RakNet::TimeMS time, int isSend) override {
	(void)internalPacket;
	(void)frameNumber;
	(void)remoteSystemAddress;
	(void)time;
	(void)isSend;
	cout << "OnInternalPacket()" << endl;
}

/// Called when we get an ack for a message we reliably sent
/// \pre To be called, UsesReliabilityLayer() must return true
/// \param[in] messageNumber The numerical identifier for which message this is
/// \param[in] remoteSystemAddress The player we sent or got this packet from
/// \param[in] time The current time as returned by RakNet::GetTimeMS()
virtual void OnAck(unsigned int messageNumber, RakNet::SystemAddress remoteSystemAddress, RakNet::TimeMS time) 	override {
	(void)messageNumber;
	(void)remoteSystemAddress;
	(void)time;
	cout << "OnAck()" << endl;
}
#endif

} // namespace KEP