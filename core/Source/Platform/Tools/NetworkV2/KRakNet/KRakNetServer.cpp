///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/include/LogHelper.h"
#include "Tools/Networkv2/KEPNet/KEPNet.h"
#include "Tools/NetworkV2/KRakNet/KRakNetServer.h"

#include "common/keputil/CommLogger.h"
#include <Tools/NetworkV2/KEPNet/NetworkBuffer.h>

namespace KEP {

static CommLogger		commLogger;

Monitored::DataPtr KRakNetServer::monitor() const {
	BasicMonitorData* p = new BasicMonitorData();
	(*p) << "<KRakNetServer>"
		 << KRakNetComponent::monitor()
		 << "</KRakNetServer>";
	return Monitored::DataPtr(p);
}

// todo: Mechanism for specifying handler for inbound user messages
//
class NilServerCallback : public IServerNetworkCallback {
  public:
	bool DataFromClient(NETID
	                    , const BYTE*
	                    , ULONG) {
		return true;
	}

	LOGON_RET	ValidateLogon(NETID id, CONTEXT_VALIDATION*, const char*, const char*) {
		return ' ';
	}

	NETRET		ClientConnected(NETID, const char*) {
		return 0;
	}

	NETRET		ClientDisconnected(NETID, eDisconnectReason) {
		return 0;
	}
};

KRakNetServer&
KRakNetServer::SendOrThrow(	NETID				to
							, const	unsigned char*	data
							, unsigned long			dataLen
							, KRakNetId				packetTypeId /* = KanevaPacket */	)		{
	try {
		KRakNetComponent::SendOrThrow(resolveAddressOrThrow(to), data, dataLen, packetTypeId, false);
	}
	catch( ChainedException& ce )	{
		KRakNetException ke(SOURCELOCATION, ErrorSpec(-1) << "Failed to send packet!");
		ke.chain(ce);
		throw ke;
	}
	return *this;
}

KRakNetServer&
KRakNetServer::SendOrThrow(	NETID					to
							, const RakNet::BitStream&  bs) {
	KRakNetComponent::SendOrThrow(bs, resolveAddressOrThrow(to) );
	return *this;
}

KRakNetServer::KRakNetServer()
	:	KRakNetComponent("Server")
	, m_logger(Logger::getInstance(L"KRakNetServer"))
	, _handler(0) {
}

KRakNetServer::~KRakNetServer() {
	
}

KRakNetServer&
KRakNetServer::initializeOrThrow(unsigned short port)		{
	RakNet::SocketDescriptor socketDescriptor;
	socketDescriptor.port = port; // SERVER_PORT;
	RakNet::StartupResult rakRet = peer->Startup(KEPNet::MaxIncomingConnections, &socketDescriptor, 1);
	if (rakRet != RakNet::RAKNET_STARTED)
		throw KRakNetException(SOURCELOCATION, ErrorSpec(rakRet, "Failed to initialize Server"));

	// N.B. MaximumIncomingConnections is not the same as max connections specified in the RakPeerInterface::Startup() method.
	// DO NOT REMOVE THIS.  IT WILL CAUSE THE SERVER TO STOP ACCEPTING ANY CONNECTIONS.
	//
	peer->SetMaximumIncomingConnections(KEPNet::MaxIncomingConnections);
	return *this;
}

ACTPtr
KRakNetServer::startOrThrow()								{
	return KRakNetComponent::startOrThrow();
}

NETRET
KRakNetServer::DisconnectClientOrThrow(NETID clientId, eDisconnectReason reason )	{
	LogTrace("");
	peer->CloseConnection(resolveAddressOrThrow(clientId), true);
	LogInfo("Disconnecting NETID " << clientId << " with reason of " << reason );
	unmapAddress(clientId);

	return NET_OK;
}

unsigned int
KRakNetServer::ConnectionCount(void) const	{
	unsigned short numberOfSystems;
	peer->GetConnectionList(0, &numberOfSystems);
	return numberOfSystems;
}


// A note on options for stopping the polling loop:
//
// In the preferred non-blocking mode we would kill the poll by connecting
// to the server and sending a kill packet (validated of course).  This
// method has the benefit of allowing us to finish processing any work in
// progress.
//
// However, we are operating in polling mode which allows us to take the
// simpler route of using a kill flat
//
bool
KRakNetServer::poll()						{
	int count(0);
	for (RakNet::Packet* p = peer->Receive()
	                         ; p != nullptr && !stopping()
	     ; p = peer->Receive())					{

		count++;
		//		commLogger.logReceive(RakNet::RakNetGUID::ToUint32(p->guid), p->data, p->length);
		switch (p->data[0])			{
			case ID_CONNECTION_LOST:				// fall through
			case ID_DISCONNECTION_NOTIFICATION:
				handleClientDisconnect(p);
				break;
			case ID_NEW_INCOMING_CONNECTION:
				handleInboundConnection(p);
				break;
			case KRakNetPacket:
				try {
					handleKRakNetPacketOrThrow(p);
				}
				catch ( const KRakNetException& e )				{
					LogError(e.str());
				}
				catch( const std::exception& e )				{
					LogError(e.what());
				}
				break;
			default:
				// Is it a RakNet message id?  I.e. an id defined and used by RakNet.
				//
				if (p->data[0] < ID_USER_PACKET_ENUM) {
					// It is a RakNet message, but it's not one that we currently handle.
					// That's not to say that we shouldn't, we simply haven't encountered it
					// yet.  Throw an exception.  Note that there is no handling for this exception.
					// That's fine, as I want this to blow up!
					//
					LOG4CPLUS_FATAL(Logger::getInstance(L"KRakNetServer"), "Unhandled RakNet message [" << p->data[0] << "]");
					throw KRakNetException(ErrorSpec(0, "Unhandled RakNet message: " + p->data[0]));
				}

				commLogger.logReceive(RakNet::RakNetGUID::ToUint32(p->guid), p->data, p->length, "KRakNetServer::poll()");
				handleInboundPacket(p);
				break;
		}
		peer->DeallocatePacket(p);
	}
	return count > 0;
}

std::string
KRakNetServer::GetIPAddressOrThrow(NETID clientId) {
	return peer->GetSystemAddressFromGuid(resolveAddressOrThrow(clientId).rakNetGuid).ToString(false);
}

KRakNetServer&
KRakNetServer::setCallback(IServerNetworkCallback& handler)	{
	_handler = &handler;
	return *this;
}

KRakNetServer&
KRakNetServer::setHostName(const std::string&  hostName) {
	_hostName = hostName;
	// TODO: IMPLEMENT Allows binding specific interfaces (as opposed to the default of *)
	//
	return *this;
}

// TODO: THIS TOO.  STARTING TO LOOK LIKE THIS FOR USE BY THE PINGER.  WHICH CASE IT'S SIMPLY A MATTER OF STORING THE STRING HERE
const std::string&
KRakNetServer::hostName() const {
	return _hostName;
}

// This method exemplifies one of the short comings with this nascent
// implementation.  The scatter logic is kept with the client.  And 
// here, in the server we have the gather logic.  This is why a message
// class is appropriate where the scatter/gather logic is co-located.
//
KRakNetServer&
KRakNetServer::handleKRakNetPacketOrThrow(RakNet::Packet* p)	{
	NetworkBuffer buf((const unsigned char*)&(p->data[1]),p->length - 1);

	unsigned char version;
	unsigned short sequence;
	unsigned long millis;
	std::string protocol;
	buf >> version;
	buf >> sequence;
	buf >> protocol;
	buf >> millis;

	NETID netid = guid2NETID(p->guid);

	setTimeoutTimeMillisOrThrow(millis,netid);

	std::stringstream ss1;
	ss1 << "At clients request I have set the timeout value for NETID["
		<< netid
		<< "] to ["
		<< timeoutTimeMillisOrThrow(netid)
		<< "]";

	LogWarn( ss1.str() );
	return *this;
}

KRakNetServer&
KRakNetServer::handleInboundConnection(RakNet::Packet* p)		{

	// I see this as a real problem.
	// We should be making the distinction between
	// connecting and authenticating and authorizing.
	// These are three separate phases.  In other words, we should not
	// be tying up the network thread with authentication ops
	// Just accept the connection and move on.  Later ops would
	// auth.
	//
	auto netId = RakNet::RakNetGUID::ToUint32(p->guid);
	mapAddress(p->guid);



	CommLogger::singleton().log(netId, "KRakNetServer::handleInboundConnection()" );

	if (!_handler)
		return *this;

	try {
		_handler->ClientConnected(netId, "");
	} catch (const KRakNetException& e) {
		LogError(e.str());
	}
	return *this;
}

KRakNetServer&
KRakNetServer::handleInboundPacket(RakNet::Packet* p)		{
	if (!_handler)
		return *this;

	try {
		NETID netid = RakNet::RakNetGUID::ToUint32(p->guid);
		commLogger.logReceive(netid, p->data + 1, p->length - 1, "KRAKNET");
		// And here we have a problem.  because we cannot assume that that consumers are prepared do handle seeing
		// the RakNet ids.
		//
		// Or should they be?  We know that in the context of KRakNetServerAdapter, our derivative IS our handler.
		//
		//
		_handler->DataFromClient(RakNet::RakNetGUID::ToUint32(p->guid), p->data + 1, p->length - 1);
	} 
	catch (const KRakNetException& e) {
		LogError(e.str());
	}
	return *this;
}

KRakNetServer&
KRakNetServer::handleClientDisconnect(RakNet::Packet* p)		{
	if (_handler != nullptr)
		_handler->ClientDisconnected(RakNet::RakNetGUID::ToUint32(p->guid), DISC_NORMAL);
	return *this;
}

KRakNetServer&
KRakNetServer::handleUnrecognizedPacket(RakNet::Packet* p)		{
	std::stringstream ss;
	ByteBufRuler::formatTo(ss, p->data, p->length);
	LogError("Unrecognized packet");
	LogError(ss.str());
	return *this;
}

bool
KRakNetServer::isLocal(const std::string&			address) const	{
	return peer->IsLocalIP(address.c_str());
}

} // namespace KEP