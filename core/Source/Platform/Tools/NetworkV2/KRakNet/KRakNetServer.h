///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "RakNet/RakPeer.h"
#include "Common/KEPUtil/Algorithms.h"
#include "Core/Util/ChainedException.h"
#include "Core/Util/Monitored.h"
#include "Core/Util/AsynchronousCompletionToken.h"
#include "Tools/NetworkV2/INetwork.h"
#include "Tools/NetworkV2/KRakNet/KRakNetComponent.h"
#include <common/KEPUtil/SynchronizedMap.h>

namespace KEP {

/**
 * Adding monitoring.
 *		1) misfire count
 *		2) size of idmap (dump of id map?)
 *      3) getConnectCount() (should equal size of id map)
 */
class KRakNetServer : public KRakNetComponent				{
  public:
	KRakNetServer();
	virtual ~KRakNetServer();

	/**
	 * Initialize this KRakNetServer instance.
	 * Specifically, initializes and starts the underlying RakNet peer component
	 * and sets the maximum incoming connections on the peer.
	 *
	 * @throws KRakNetException in the event that peer initialization fails.
	 * @return Reference to self.
	 */
	KRakNetServer&		initializeOrThrow(			unsigned short				port);

	/**
	 * @see Monitored.h
	 */
	virtual Monitored::DataPtr monitor() const override;

	/**
	* Non-blocking call to start listening for network activity
	*/
	ACTPtr				startOrThrow();

	/**
	 * Given the NETID of a client connected to this KRakNetServer, disconnect 
	 * with the client.
	 *
	 * @param NETID		the NETID of the client to be disconnected.
	 * @param reason	and eDisconnectReason that is transmitted to the client
	 *					to let them know the reason for the disconnect.
	 * @throw			KRakNetException in the event that the client identified by
	 *					clientId is not connected to this KRakNetServer instance.
	 * @return			
	 */
	NETRET				DisconnectClientOrThrow(NETID						clientId
												, eDisconnectReason			reason		);

	/**
	 * Determine the number of connected clients.
	 * @retrurn the number of connected clients.
	 */
	unsigned int		ConnectionCount(void) const;

	/**
	 * Associate an IServerNetworkCallback with this server.
	 * @param	handler		Reference to the IServerNetworkCallback instance to be 
	 *						associated with this KRakNetServer instance.
	 * @return reference to self.
	 */
	KRakNetServer&		setCallback(		IServerNetworkCallback&		handler		);

	/**
	 * Given the network id of a client connected to this server, return the components IP 
	 * address as a string.
	 *
	 * @param The id of the client in question.
	 * @throws RakNetException if the NETID is invalid, i.e. not connected.
	 */
	std::string			GetIPAddressOrThrow(		NETID					clientId	);

	/**
	 * Send data to a client. 
	 * @param		to				NETID of client to which the data will be sent.
	 * @param		bs				RakNet::Bitstream containing the transmission data.
	 * @throws		KRakNetException In the event that the client identified by "to"
	 *				is not currently connected.
	 * @return reference to self.
	 */
	KRakNetServer&		SendOrThrow(		NETID					to
	                                        , const RakNet::BitStream&	bs			);

	/**
	 * Send data to a client.
	 *
	 * @param		to				NETID of client to the data will be sent
	 * @param		data			buffer containing data to be transmitted.
	 * @param		dataLen			size of the buffer identified by "data"
	 * @param		packetTypeId	A value from the KRakNetId enumeration.
	 * @throws		KRakNetException In the event that the client identified by "to"
	 *				is not currently connected.
	 * @return reference to self.
	 */
	KRakNetServer&		SendOrThrow(		NETID					to
	                                        , const unsigned char*		data
	                                        , unsigned long				dataLen
	                                        , KRakNetId					packetTypeId = KanevaPacket );

	/**
	 * Set the of this KRakNetServer's host
	 * @param  hostName			The name of this KRakNetServer's host.
	 * @return reference to self
	 */
	KRakNetServer&		setHostName(		const std::string&			hostName	);

	/**
	 * Acquire the name of this KRakNetServer's host.
	 */
	const std::string&	hostName() const;

	/**
	* Poll for network events.
	* @throws KRakNetException in the event that an unexpected packet type should appear.
	*/
	virtual bool		poll() override;

  protected:
	bool			isLocal(				const std::string&			address		) const;

	virtual KRakNetServer& handleInboundConnection(	RakNet::Packet*				p			);
	virtual KRakNetServer& handleInboundPacket(		RakNet::Packet*				p			) override;
	virtual KRakNetServer& handleClientDisconnect(	RakNet::Packet*				p			);
	virtual KRakNetServer& handleUnrecognizedPacket(	RakNet::Packet*				p			) override;

	/**
	 * Packet directed to this layer.
	 */
	virtual KRakNetServer& handleKRakNetPacketOrThrow(		RakNet::Packet*				p			);


private:


	log4cplus::Logger								m_logger;
	IServerNetworkCallback*							_handler;
	std::string										_hostName;
};

} // namespace KEP