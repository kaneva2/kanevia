///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common/include/KEPCommon.h"
#include "common/keputil/CommLogger.h"
#include "KRakNetClient.h"

namespace KEP {

static CommLogger		commLogger;

char* ConnectionMessages[] = {
	"CONNECTION_ATTEMPT_STARTED"
	, "INVALID_PARAMETER"
	, "CANNOT_RESOLVE_DOMAIN_NAME"
	, "ALREADY_CONNECTED_TO_ENDPOINT"
	, "CONNECTION_ATTEMPT_ALREADY_IN_PROGRESS"
	, "SECURITY_INITIALIZATION_FAILED"
};

KRakNetClient::KRakNetClient()
	: KRakNetComponent("client<unnamed>")
	, controlledaccess_state(Instantiated)
	, _connectACT(new ACT())
	, _disconnectACT(new ACT())
	, _stop(false)
	, _handler(0) {
}

KRakNetClient::KRakNetClient(std::string name)
	: KRakNetComponent(name)
	, controlledaccess_state(Instantiated)
	, _connectACT(new ACT())
	, _stop(false)
	, _handler(0) {
}

KRakNetClient::~KRakNetClient() {
}

KRakNetClient&
KRakNetClient::setHandler(IClientNetworkCallback& handler)	{
	_handler = &handler;
	return *this;
}

ACTPtr
KRakNetClient::connectOrThrow(const std::string& remoteIPAddress, unsigned short port)	{
	std::lock_guard<fast_mutex> lock(_sync);


	// If we blocked above, we can assume that another thread was in here trying to connect
	// the client to the server.
	//
	if (state() == Connected)
		return _connectACT;

	if (state() == Connecting)
		return _connectACT;

	// Clear to connect, release any waiters
	// and reset the ACT for the imminent connect attempt
	_connectACT->complete();
	_connectACT = ACTPtr(new ACT());

	// At the very least we have to be "started" before we can connect
	if (state() != Started )
		throw KRakNetException(SOURCELOCATION, ErrorSpec(NET_UNINITIALIZED)
													<< "Unexpected state  [" 
													<< state() 
													<< "] encountered while connecting to [" 
													<< remoteIPAddress.c_str() 
													<< "]" );

	CommLogger::singleton().log(0, "ConnecKRakNetClient::Connect()");
	int ret = peer->Connect(	remoteIPAddress.c_str()
	                            , (unsigned short)port
	                            , 0
	                            , 0
	                            , 0 );
	if (ret != RakNet::CONNECTION_ATTEMPT_STARTED)
		throw KRakNetException(SOURCELOCATION, ErrorSpec(NET_GENERIC, ConnectionMessages[ret]));
	setState(Connecting);

	return _connectACT;
}

ACTPtr KRakNetClient::disconnect(void)	{
	RakNet::SystemAddress a = peer->GetSystemAddressFromIndex(0);
	peer->CloseConnection(a
	                      , true
	                      , 0);
	setState(Started);
	return _disconnectACT;
}

ACTPtr
KRakNetClient::startOrThrow(void)	{
	LOG4CPLUS_TRACE(Logger::getInstance(L"KRakNetClient"), "KRakNetClient::start()");
	if (state() != Instantiated) {
		ACTPtr ret(new ACT());
		ret->complete();
		return ret;
	}

	ACTPtr ret = KRakNetComponent::startOrThrow();
	RakNet::SocketDescriptor socketDescriptor;
	socketDescriptor.port = 0;
	RakNet::StartupResult RakRet = peer->Startup(1, &socketDescriptor, 1);
	if ( RakRet != RakNet::RAKNET_STARTED)
		throw KRakNetException(SOURCELOCATION, ErrorSpec(RakRet, "Failed to start client"));
	setState(Started);
	return ret;
}

KRakNetClient&
KRakNetClient::SendOrThrow(	const unsigned char*			buf
							, size_t						bufsize
							, KRakNetId						packetTypeId	/* = KanevaPacket */
							, bool							broadcast		/* = false */		)			{

	KRakNetComponent::SendOrThrow(_serverAddress, buf, bufsize, packetTypeId, broadcast);
	return *this;
}

KRakNetClient&
KRakNetClient::SendOrThrow(	const RakNet::BitStream&		bs
							, bool							broadcast		/*= false */)	{
	KRakNetComponent::SendOrThrow(bs, _serverAddress, broadcast);
	return *this;
}

KRakNetClient&
KRakNetClient::SendOrThrow(	const RakNet::BitStream&		bs
							, const RakNet::AddressOrGUID&	addr
							, bool							broadcast		/*= false*/)	{
	KRakNetComponent::SendOrThrow(bs, addr, broadcast);
	return *this;
}

KRakNetClient& 
KRakNetClient::handleLostConnection( RakNet::Packet* /*unused*/ )		{
	LOG4CPLUS_WARN(Logger::getInstance(L"KRakNetClient"), "Connection lost");
	setState(Started);
	_handler->Disconnected(		"connection_lost"
	                            , E_FAIL
	                            , true
	                            , DISC_LOST		// 	LR_CONNECTION_LOST
	                            , false			);
	return *this;
}

KRakNetClient& KRakNetClient::handleEvictedConnection(RakNet::Packet*)	{
	LOG4CPLUS_WARN(Logger::getInstance(L"KRakNetClient"), "Connection evicted");
	setState(Started);
	if (_handler)
		_handler->Disconnected("connection_evicted"
								, E_FAIL					// under dplay, this would have held an HRESULT that came from deep in the
								// bowels of DirectPlay.  With poor documentation and antiquity obscuring
								// the proper values here, will need to play this one by ear.  Stepping the
								// call doesn't seem to indicate that it is of consequence.
	                            , false
	                            , DISC_FORCE		// under dplay there is a blur between the game layer and network layer and the game
	                            , false			);
	return *this;
}

bool KRakNetClient::poll()		{
	if (_stop)
		return false;

	if (peer == 0)
		return false;

	// this issue gets easier when poll is simply passed the packet.  I.e.
	// the loop and control exists in the executor though this would only
	// work in a reactive mode (as opposed to polling mode).
	// Consider:
	//
	//   foreach packet : packets
	//            handle(packet);
	//
	// in polling mode, we have to test for halt
	//
	int count(0);
	for (RakNet::Packet* p = peer->Receive()
	                         ; p != nullptr
	     ; p = peer->Receive())		{
		count++;
#ifdef RKVERBOSE
		std::stringstream ss;
		ss << name() << " received: " << endl << KRakNetHelper::describeBuffer(p->data, p->length);
		cout << ss.str();
#endif
		switch (p->data[0])			{
			case ID_CONNECTION_REQUEST_ACCEPTED:
				handleConnectCompletion(p);
				break;
			case ID_CONNECTION_ATTEMPT_FAILED:
				_connectACT->complete(KRakNetException(SOURCELOCATION, ErrorSpec(ID_CONNECTION_ATTEMPT_FAILED, "ID_CONNECTION_ATTEMPT_FAILED")));
				setState(Started);
				break;
			case ID_CONNECTION_BANNED:
				_connectACT->complete(KRakNetException(SOURCELOCATION, ErrorSpec(p->data[0], "ID_CONNECTION_BANNED")));
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				setState(Started);
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				handleEvictedConnection(p);
				break;
			case ID_DOWNLOAD_PROGRESS:
				// This message, not really a network packet, but more accurately a message
				// from RakNet that we can do some progress handling while the packet is
				// being pulled off the wire.
				// @see
				//		http://www.jenkinssoftware.com/raknet/manual/Doxygen/classRakNet_1_1RakPeerInterface.html#4112544503b05a7b7dd2dd56adfbc648
				//
				// We are currently not using this setting and hence we should never receive
				// this message.
				//
				LOG4CPLUS_FATAL(Logger::getInstance(L"KRakNetClient"), "PARTIAL DETECTED!!!");
				throw KRakNetException(ErrorSpec(0, "Unsupported RakNet message ID_DOWNLOAD_PRGORESS"));
			case ID_CONNECTION_LOST:
				handleLostConnection(p);
				break;
			case ID_ALREADY_CONNECTED:
				break;

			case ID_INVALID_PASSWORD:
				break;
			case ID_INCOMPATIBLE_PROTOCOL_VERSION:
				break;
			default:
				// Is it a RakNet message id?  I.e. an id defined and used by RakNet.
				//
				if (p->data[0] < ID_USER_PACKET_ENUM) {
					// It is a RakNet message, but it's not one that we currently handle.
					// That's not to say that we shouldn't, we simply haven't encountered it
					// yet.  Throw an exception.  Note that there is no handling for this exception.
					// That's fine, as I want this to blow up!
					//
					LOG4CPLUS_FATAL(Logger::getInstance(L"KRakNetClient"), "Unhandled RakNet message [" << p->data[0] << "]");
					throw KRakNetException(ErrorSpec(0, "Unhandled RakNet message: " + p->data[0]));
				}

				commLogger.logReceive(RakNet::RakNetGUID::ToUint32(p->guid), p->data, p->length, "KRakNetServer::poll()");
				this->handleInboundPacket(p);
		}
		peer->DeallocatePacket(p);
	}
	return count > 0;
}

KRakNetClient&
KRakNetClient::handleUnrecognizedPacket(RakNet::Packet* p)		{
	commLogger.logReceive(RakNet::RakNetGUID::ToUint32(p->guid), p->data, p->length, "KRAKNET::KRakNetClient::handleUnrecognizedPacket()");
	std::stringstream ss;
	ByteBufRuler::formatTo(ss, p->data, p->length);
	LOG4CPLUS_ERROR(Logger::getInstance(L"KRakNetClient"), "Unrecognized inbound packet!");
	LOG4CPLUS_ERROR(Logger::getInstance(L"KRakNetClient"), ss.str() );
	return *this;
}

KRakNetClient& KRakNetClient::handleInboundPacket(RakNet::Packet* p)			{
	NETID netid = RakNet::RakNetGUID::ToUint32(p->guid);
	commLogger.logReceive(netid, p->data, p->length);
	if (!_handler)
		return *this;
	try {
		_handler->DataFromServer(RakNet::RakNetGUID::ToUint32(p->guid), p->data + 1, p->length - 1);
	} 
	catch (const KRakNetException e)	{
		LOG4CPLUS_ERROR(Logger::getInstance(L"KRakNetClient"), e.str() );
	}
	return *this;
}

KRakNetClient& KRakNetClient::handleConnectCompletion(RakNet::Packet* p)		{
	// I'm attempting to acquire the guid of the server, after it has
	// accepted me.  I would have expected this guid to be present here
	// But this one looks wonky.
	//
	// Going to modify check server side via GetMyGUID()
	_serverGUID = peer->GetGuidFromSystemAddress(p->systemAddress);
	_serverAddress = p->systemAddress;

	_connectACT->complete();
	setState(Connected);
	mapAddress(p->guid);

	if (_handler)
		_handler->Connected(0, 0, 0);		// This will become an integration point
	return *this;
}

KRakNetClient&
KRakNetClient::setTimeoutTimeMillisOrThrow(unsigned long millis, NETID netid ) {
	KRakNetComponent::setTimeoutTimeMillisOrThrow((netid == SERVER_NETID)
		? guid2NETID(_serverGUID)
		: netid);
	return *this;
}

unsigned long 
KRakNetClient::timeoutTimeMillisOrThrow(NETID netid ) const	{
	return KRakNetComponent::timeoutTimeMillisOrThrow(	(netid == SERVER_NETID)
									? guid2NETID(_serverGUID)
									: netid );
}

KRakNetClient&
KRakNetClient::setState(State s)	{
	LOG4CPLUS_DEBUG(Logger::getInstance(L"KRakNetClient"), stateName(controlledaccess_state) << " ===>" << stateName(s));
	controlledaccess_state = s;
	return *this;
}

KRakNetClient::State
KRakNetClient::state() const	{
	return controlledaccess_state;
}

const std::string&
KRakNetClient::stateName(State s) const				{
	return StateName[s];
}

KRakNetClient& 
KRakNetClient::synchronizeTimeoutTimeMillisOrThrow(unsigned long millis) {

	// This scatter logic should be co-located with the gather logic.
	// This marshalling packaged with the protocol
	//
	NetworkBuffer buffer(255);
	buffer << (unsigned char)0xaa; // version. (inactive)
	buffer << (unsigned short)0xbbbb; // sequence.  (inactive)
	buffer << "SYNT";
	buffer << millis;
	SendOrThrow(buffer.buf(), buffer.size(), KRakNetPacket);
	KRakNetComponent::setTimeoutTimeMillisOrThrow(millis, guid2NETID(_serverGUID));

	return *this;
}





// Okay.  The easiest hook in is as transport layer only.  It's easier to
// integrate because it's truer the separation.  Let's stay away from their
// plugins...unless we write it.  In either case, I'm not overly enthusiastic
// about this library.
//
// The documentation can be a bit confusing at times and the absence of
// real code to illustrate how to do things, in the noob sense any way.
//
// It is implemented in non-blocking mode, but with using a socket select
// This means they are polling themselves.  It's true they are running
// non-blocking sockets, but without availing themselves of socket select
// they must poll.  Silly in my opinion.
//
// For now...to at least draw a line in the coupling sand, we will push all of this to a function that handles
// kaneva data.
//
// It occurs to me that we may have an issue with memory management.  Do we currently
// Where was this line of responsibility drawn under direct play?
// Incidentally, this makes the argument for a pool of buffers.  This way
// we can avoid the copying of data and the allocations (at least no more after
// initial packet allocation).
//
// At this point we need to start mapping connected clients, specifically
// map the NETID (a 32bit representation of the senders GUID).  This will allows
// us to not have to change any of existing peer identification methods.  It does
// have the overhead of requireing a hashed search of the map to resolve the 32bit id
// to the sendiers guid/address.
//
// An alternative is to redefine NETID as a container which, depending on the
// implementation, may contain a RakNetGUID or a DPlay NETID.  This precludes map
// management and searching.
//
// Another alternative is to simply place the RakNetGUID on the heap and then
// return a pointer to the RakNetGUID.  This pointer value can be passed around as
// a 32bit number.  It is true that this value only has meaning on localhost, (i.e.
// an id on this machine in all likelihood is useless on another machine).  However
// we have the translation at our finger tips so who cares.  One issue is that
// the RakNetGUID cannot be relocated without updating the id.  A very problematic
// proposition.
//
// For right now, let's go with the map.
//
//				HandleKanevaData(RakNet::GetGuidFromSystemAddress(RakNetGUID::p->data,p->length);

} // namespace KEP