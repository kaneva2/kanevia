///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#define IDS_STRING101                   13000
#define IDS_KEPAUTH_ERROR               13000
#define IDS_DEV_MODE                    13001
#define IDS_NETWORK_VERSION             13002
#define IDS_NETWORK_BUILD               13002
#define IDS_NO_HOSTNAME                 13003
#define IDS_NO_GAME_ID                  13004
#define IDS_KEPAUTH_OK                  13005
#define IDS_STRING13006                 13006
#define IDS_KEPAUTH_FORCE               13006
#define IDS_BAD_URL                     13007
#define IDS_STILL_THROTTLED             13008
#define IDS_UNTHROTTLED                 13009
#define IDS_STRING13010                 13010
#define IDS_KEPAUTH_GENERAL_ERROR       13010
#define IDS_NO_AUTH_URL                 13112
#define IDS_NO_PING_SERVER              13120
#define IDS_NO_PING_ERROR               13121
#define IDS_PING_STARTED                13122
#define IDS_INCORRECT_PING_RESPONSE     13123
#define IDS_MAX_LIMIT_WARNING           13136
#define IDS_LICENSE_CHANGE_UP           13137
#define IDS_LICENSE_CHANGE_DOWN         13139
#define IDS_INACTIVE_ACCOUNT            13140
#define IDS_UDP_SERVER_STARTED          13141
#define IDS_CLIENT_LIMIT                13142
#define IDS_ZERO_ADDRESS                13143
#define IDS_SENDQ_FAILED                13144
#define IDS_SEND_MSGCOUNT_WARN          13145
#define IDS_SEND_MSGSIZE_WARN           13146
#define IDS_CONNLOST_SIZE               13147
#define IDS_INVALID_FLAGS               13148
#define IDS_INVALID_PLAYER_SIZE         13149
#define IDS_NO_SERVER_IF                13150
#define IDS_PING_TIMEOUT_THROTTLE       13151
#define IDS_NO_ENGINE_ID                13152
#define IDS_SENDTO_FAILED               13153

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
