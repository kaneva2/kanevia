#include <stdafx.h>
#include "PlaybackThread.h"
#include "NetworkLoggerHelpers.h"

#include "Event\Events\GetAttributeEvent.h"
#include "Event\Events\DynamicObjectEvents.h"
#include "KEPException.h"
#include "CommandHeader.h"
#include "..\INetworkLogger.h"

// IPacketProcessor override to handle structs for dumping or playback
bool PlaybackThread::ProcessStruct( IN NETID id, IN bool isSend, IN bool nested, IN int categoryId, IN const BYTE *data, IN ULONG len )
{
	bool ret = true;
	
	if ( !isSend )
	{
		// set data on structs from client
		switch ( categoryId )
		{
			case SHRUNK_TO_SERVER_CAT:
				{
					LPSHRUNK_TOSVRMOV_STRUCT mov = (LPSHRUNK_TOSVRMOV_STRUCT)data;
					mov->idReference = MapNetworkAssignedId(id);
				}
				break;
				
			case COMMAND_DATA_TO_SERVER_CAT:
				{
					LPCOMMAND_DATA cmd = (LPCOMMAND_DATA)data;
					cmd->m_networkID = MapNetworkAssignedId(id);
				}
				break;
				
			case ATTRIB_TO_SERVER_CAT:
				{
					LPATTRIB_STRUCT attr = (LPATTRIB_STRUCT)data;
					attr->idRefrence = MapNetworkAssignedId(id);
				}
				break;

#ifdef DRF_KACHING
			case FIRE_TO_SERVER_CAT:
				{
					LPFIRE_STRUCT fire = (LPFIRE_STRUCT)data;
					fire->idRefrence = MapNetworkAssignedId(id);
				}
				break;
#endif

			case SPAWN_TO_SERVER_CAT:
				{
					LPFROM_CLIENT_SPAWN_STRUCT spawn = (LPFROM_CLIENT_SPAWN_STRUCT)data;
					spawn->zoneIndex = m_lastPendingSpawnZone;
				}
				break;
		}

	}
	
	if ( categoryId == ENTITYGUPDATE_TO_SERVER_CAT || categoryId == ENTITYGUPDATE_TO_CLIENT_CAT ||
		 categoryId == ENTITYGUPDATE_TO_SERVER_OLD || categoryId == ENTITYGUPDATE_TO_CLIENT_OLD )
	{
		ret = crackGuaranteedPacket( *this, id, isSend, data, len );
	}
	else if ( categoryId == ENTITYPOS_TO_CLIENT_CAT || categoryId == ENTITYPOS_TO_CLIENT_OLD )
	{
		ret = crackNonGuaranteedPacket( *this, id, isSend, data, len );
	}
	

	if ( ret && !isSend && !nested )
		m_callback->DataFromClient(m_myNetId, data, len );

	return ret;
}


// IPacketProcessor override to handle IEvnts for dumping or playback
bool PlaybackThread::ProcessEvent( IN NETID id, IN bool isSend, IN bool nested, IN EventHeader *header, IN const BYTE *data, IN ULONG len )
{
	bool ret = true;

	if ( header->Id() == PENDING_SPAWN_ID )
	{
		if ( !isSend )
		{
			if ( m_lastPendingSpawnId == 0 )
			{
				// need to wait for it
				m_waitEventId = PENDING_SPAWN_ID;
				checkForWaits( 1000 );
				if ( m_lastPendingSpawnId == 0 )
				{
					// didn't get it
					LOG4CPLUS_WARN( m_logger, "Didn't get pending spawn event from server" );
					return false;
				}
			}
			// set it on recv to match what server sent
			header->SetObjectId(m_lastPendingSpawnId);
			header->SetFilter( m_lastPendingSpawnZone );
		}
	}
	else if ( header->Id() == ADD_DYN_OBJ_EVENT_ID )
	{
		if ( isSend ) // server sends down the dynObjId
		{
			AddDynamicObjectEvent ade( header->Id(), header, data+sizeof(EventHeader), len - sizeof(EventHeader), id, EP_NORMAL );
			try
			{
				int placementId;
				ade.ExtractBasicInfo(placementId); 
				FLOCK;
				// see if have placement id of zero indicating got real server SEND before playback got to SEND
				ObjSerialNumberMap::iterator i = m_dynObjMap.find(0);
				if ( i != m_dynObjMap.end() )
				{
					m_dynObjMap[placementId] = i->second;
					m_dynObjMap.erase( i );
					LOG4CPLUS_TRACE( m_logger, "Found existing add of dyn object in playback " << placementId );
				}
				else
				{
					m_dynObjMap[placementId] = 0;  // initialize new one to zero
					LOG4CPLUS_TRACE( m_logger, "Found new add of dyn object in playback " << placementId );
				}
			}
			catch ( KEPException *e )
			{
				LOG4CPLUS_WARN( m_logger, "Exception decoding render locale text event: " << e->ToString() );
				e->Delete();
			}
		}
		
	}
	else if ( header->Id() == MOVE_DYN_OBJ_EVENT_ID ||
			  header->Id() == DELETE_DYN_OBJ_EVENT_ID ||
			  header->Id() == UPDATE_DYN_OBJ_EVENT_ID ||
			  header->Id() == UPDATE_FRIEND_DYN_OBJ_EVENT_ID ||
			  header->Id() == UPDATE_DYN_OBJ_EVENT_ID )
	{
		if ( !isSend )
		{
			ObjSerialNumberMap::iterator i = m_dynObjMap.find( header->ObjectId() );
			if ( i != m_dynObjMap.end() )
				header->SetObjectId(i->second);
			else
				LOG4CPLUS_WARN( m_logger, "Missing mapped object id for dyn object " << header->ObjectId() ); 
		}
	}
	else if ( header->Id() == TEXT_EVENT_ID && header->Filter() == CMD_SRP )
	{
		LOG4CPLUS_INFO( m_logger, "Skipping SRP text event in playback" );
		ret = false; // don't send these
	}

	if ( ret && !isSend && !nested )
		m_callback->DataFromClient(m_myNetId, data, len );

	return ret;
}


// IPacketProcessor override to handle attrib structs for dumping or playback
bool PlaybackThread::ProcessAttrib( IN NETID id, IN bool isSend, IN bool nested, IN ATTRIB_STRUCT *attrib, IN const BYTE *data, IN ULONG len )
{
	bool ret = true;

	if ( !isSend )
	{
		// idReference always our network assigned id
		attrib->idRefrence = MapNetworkAssignedId(id);		
		
		switch ( attrib->attribute ) 
		{
			case GetAttributeType::GET_ATTACHMENT_STATES:
				attrib->uniInt2 = MapNetworkAssignedId(id);
				break;
			case GetAttributeType::SELECT_CHARACTER:					
				m_waitEventId = PENDING_SPAWN_ID;	// need to wait for pending spawn before continuing
				break;
		}
	}
		
	return ret;
}


bool PlaybackThread::ProcessEquip( IN NETID id, IN bool isSend, IN bool nested, IN EQUIP_STRUCT *attrib, IN const BYTE *data, IN ULONG len ) 
{
	bool ret = true;
	attrib->idRefrence = MapNetworkAssignedId( id );
	return ret;
}


void PlaybackThread::processValidate( IN CONTEXT_VALIDATION *context, 
											IN const char* ipAddress, 
										   	IN const char* userInfoXML ) 
{
	m_callback->AddPlaybackPendingLogin( context->username );
	
	m_callback->ValidateLogon( context, ipAddress, userInfoXML );
	
	LOG4CPLUS_TRACE( m_logger, "ValidateLogin for " << ipAddress );
}

void PlaybackThread::processConnect( IN NETID myNetId, IN const char* playerName )
{
	m_callback->ClientConnected(m_myNetId, playerName);
	
	LOG4CPLUS_TRACE( m_logger, "ClientConnected for " << playerName << " net id is " << myNetId );
}

bool PlaybackThread::ProcessPacket( IN NETID id, IN bool isSend, IN const BYTE *data, IN ULONG msgSize )
{	
	if ( !isSend && !checkForWaits() )
		return false;
	else 
		return true;
}

void PlaybackThread::processDisconnect( IN eDisconnectReason reason )
{

	m_callback->ClientDisconnected(m_myNetId, reason );
	LOG4CPLUS_TRACE( m_logger, "Disconnect for " << m_myNetId );
}

// see if we're waiting for something
bool PlaybackThread::checkForWaits( IN ULONG timeoutMs )
{
	jsFastLock lock( getSync() );
	if ( m_waitEventId != BAD_EVENT_ID || m_waitAttrib != 0 || m_waitStruct != 0 )
	{
		EVENT_ID waitEventId = m_waitEventId;
		int waitAttrib = m_waitAttrib;
		int waitStruct = m_waitStruct;
		
		lock.unlock();
		_sleep( timeoutMs );
		if ( m_waitEventId != BAD_EVENT_ID || m_waitAttrib != 0 || m_waitStruct != 0 )
		{
			char msg[255];

			if ( waitEventId != BAD_EVENT_ID )
			{
				sprintf_s( msg, _countof(msg), "Didn't get event needed of %s", m_dispatcher.GetEventName(waitEventId).c_str() );
			}
			else if ( waitAttrib != 0 )
			{
				sprintf_s( msg, _countof(msg), "Didn't get attrib needed of id %d", waitAttrib );
			}
			else if ( waitStruct != 0 )
			{
				sprintf_s( msg, _countof(msg), "Didn't get struct need of id %d", waitStruct );
			}

			SendMsgToAdmin( m_replyNetId, m_dispatcher, msg );
			LOG4CPLUS_WARN( m_logger, msg );
			_setTerminated();
			return false;
		}
		else if ( m_logger.isEnabledFor( TRACE_LOG_LEVEL ) )
		{
			if ( waitEventId != BAD_EVENT_ID )
			{
				LOG4CPLUS_TRACE( m_logger, "Got expected event of " << m_dispatcher.GetEventName(waitEventId) );
			}
			else if ( waitAttrib != 0 )
			{
				LOG4CPLUS_TRACE( m_logger, "Got expected attrib of " << waitAttrib );
			}
			else if ( waitStruct != 0 )
			{
				LOG4CPLUS_TRACE( m_logger, "Got expected struct of category " << waitStruct );
			}
			return true;
		}
	}
	return true;
}

void PlaybackThread::cleanup()
{
	SendMsgToAdmin( m_replyNetId, m_dispatcher, "Playback complete" );
	
	PlaybackReader::cleanup();
	
	if ( m_callback->ThreadComplete( this ) )
		delete this;
}


