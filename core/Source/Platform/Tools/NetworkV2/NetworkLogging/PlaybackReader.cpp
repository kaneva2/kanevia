#include <stdafx.h>
#include "PlaybackReader.h"

#include "NetworkLoggerHelpers.h"
#include "..\INetworkLogger.h"
#include "Event\Base\IEvent.h"
#include "Event\Base\IDispatcher.h"
#include "Event\Events\RenderTextEvent.h"

NETID PlaybackReader::m_nextNetId = 1; // 0 = Invalid, 1 = SERVER_NETID

// read helper that throws on error
inline ULONG readData( OUT void *data, IN ULONG len, IN FILE *f, const char* throwString )
{
	ULONG ret;
	ret = fread( data, 1, len, f );
	if ( ret != len )
		throw throwString;
	return ret;
}

// read in data that is prefixed by a ULONG length, returns bytes read into data
inline ULONG readLenAndData( OUT void *data, IN ULONG maxLen, IN FILE *f, const char* throwString )
{
	ULONG ret;
	ULONG len; 
	ret = fread( &len, sizeof(len), 1, f );
	if ( ret != 1 )
		throw throwString;
	if ( len > maxLen )
		throw throwString;
	return readData( data, len, f, throwString );
}

ULONG PlaybackReader::_doWork()
{
	ULONG len = 0;
	
	ULONG baseTickCount = 0;			// tick count when recording started
	BYTE readBuffer[8192];
	NETID netId;						// of last packet read
	
	string verboseId;					// for dumping, the verbose name of cat/eventid
	
	// read the file and pass in the data to the server
	char s[100];
	memset( s, 0, 100 );
	fread( s, 1, _countof(EYE_CATCHER)-1, m_infile );
	if ( strcmp( s, EYE_CATCHER ) != 0 )
	{
		LOG4CPLUS_WARN( m_logger, "Bad file tag found in playback file " << s );
		cleanup();
		return FALSE;
	}
	memset( s, 0, 100 );
	fread( s, 1, _countof(FILE_VERSION)-1, m_infile );
	if ( strcmp( s, FILE_VERSION ) != 0 )
	{
		LOG4CPLUS_WARN( m_logger, "Bad file version found in " << s );
		cleanup();
		return FALSE;
	}
	// skip date timestamp which is YYMMDDHHMMSS
	fread( s, 1, TIMESTAMP_LEN, m_infile );
	
	size_t bytesRead = 0;
	try 
	{
		while ( !isTerminated() && (bytesRead = fread( m_tag, 1, EYECATCHER_SIZE, m_infile )) != 0 )
		{
			m_lastTell = ftell( m_infile ) - bytesRead;
			if ( bytesRead == EYECATCHER_SIZE )
			{
				m_tag[EYECATCHER_SIZE] = '\0';
			
				switch ( m_tag[0] )
				{
					case 'V': // validate
						{
						CONTEXT_VALIDATION context;
						CHAR ipAddress[100];
						CHAR userInfoXML[1024];

						readTickCount( baseTickCount, netId, false );
						len = readLenAndData( &context, sizeof(context), m_infile, "Error reading CONTEXT" );
						if ( len != sizeof(context) )
							throw "Wrong size of CONTEXT";
						len = readLenAndData( &ipAddress[0], _countof(ipAddress)-1, m_infile, "Error reading ipAddress" );
						ipAddress[len] = '\0';
						len = readLenAndData( &userInfoXML[0], _countof(userInfoXML)-1, m_infile, "Error reading UserXML" );
						userInfoXML[len] = '\0';

						processValidate( &context, ipAddress, userInfoXML );
						
						}
						break;
						
					case 'C': // connect 
						if ( readTickCount( baseTickCount, netId, false ) )
						{
							CHAR playerName[100];
							len = readLenAndData( &playerName[0], _countof(playerName)-1, m_infile, "Error reading playerName" );
							playerName[len] = '\0';
							m_myNetId = nextNetId();

							processConnect( m_myNetId, playerName );
						}
						break;
						
					case 'R': // receive from client
						if ( readTickCount( baseTickCount, netId ) )
						{
							len = readLenAndData(readBuffer, _countof(readBuffer), m_infile, "Error reading RECV data" );
							
							crackPacket( *this, m_myNetId, false, readBuffer, len );
						}
						break;
						
					case 'S': // send to client
						if ( readTickCount( baseTickCount, netId, false ) )
						{
							len = readLenAndData(readBuffer, _countof(readBuffer), m_infile, "Error reading RECV data" );
							crackPacket( *this, m_myNetId, true, readBuffer, len );
						}
						break;
						
					case 'D': // discconnect 
						if ( readTickCount( baseTickCount, netId ) )
						{
							eDisconnectReason reason;
							readData( &reason, sizeof(reason), m_infile, "Error reading disconnect reason" );

							processDisconnect( reason );
							_setTerminated();
						}
						break;
						
					case 'E': // end w/o disconnect
						_setTerminated();
						LOG4CPLUS_TRACE( m_logger, "Ending playback for " << m_myNetId );
						break;
						
					default:
						SendMsgToAdmin( m_replyNetId, m_dispatcher, "Bad m_tag found in playback file, stopping" );
						LOG4CPLUS_WARN( m_logger, "Bad m_tag found in playback file " << m_tag );
						_setTerminated();
						break;
				}
			}
		}
	}
	catch ( const char* msg )
	{
		LOG4CPLUS_WARN( m_logger, "ERROR in playback: " << msg );
	}

	cleanup();
	
	return TRUE;
}


/// \brief read the tickcount from the file and sleep for the time between this and the 
/// time of the prev packet to playback at the same speed 
bool PlaybackReader::readTickCount( INOUT ULONG &prevTickCount, OUT NETID &id, IN bool sleepToo )
{
	ULONG nextTickCount = 0;
	readData( &id, sizeof(id), m_infile, "Error reading netId" );
	readData( &nextTickCount, sizeof(nextTickCount), m_infile, "Error reading base tick count" );
	if ( m_sleepMultiplier > 0.0 && sleepToo )
	{
		ULONG sleepTime = (nextTickCount - prevTickCount);
		if ( sleepTime > 10000 )
		{
			LOG4CPLUS_WARN( m_logger, "Got sleep time > 10 seconds: " << sleepTime );
			sleepTime = 10000;
		}
		fTime::SleepMs( sleepTime * m_sleepMultiplier );
		prevTickCount = nextTickCount;
	}
	else if ( prevTickCount == 0 )
	{
		prevTickCount = nextTickCount;
	}
	return !isTerminated();
}

void PlaybackReader::cleanup()
{
	fclose( m_infile );
}

