#include <stdafx.h>
#include "PlaybackDumper.h"
#include "NetworkLoggerHelpers.h"
#include "Event\Base\IDispatcher.h"
#include "Event\Events\RenderTextEvent.h"
#include "Event\Events\RenderLocaleTextEvent.h"
#include "Event\Events\ScriptClientEvent.h"
#include "KEPException.h"
#include "ChatType.h"
#include "Tools/NetworkV2/Protocols.h"

// since some category ids are overloaded between client and server need two arrays
//  this if from client
static const char* RECV_CAT_STRINGS[] = {
		"UNKNOWN 0",
		"LOG_TO_SERVER_CAT",
		"MOVE_CAT",
		"FIRE_TO_SERVER_CAT",
		"SHRUNK_TO_SERVER_CAT",
		"EQUIP_TO_CLIENT_CAT",
		"ATTRIB_TO_CLIENT_CAT",
		"PING_CAT",
		"SPAWN_TO_SERVER_CAT",
		"UNKNOWN 9",
		"ITEM_LOC_TO_CLIENT_CAT",
		"CLAN_TEXT_MSG_CAT",
		"MENUBUILD_TO_SERVER_CAT",
		"UNKNOWN 13",
		"UNKNOWN 14",
		"UNKNOWN 15",
		"UNKNOWN 16",
		"AI_SHRUNK_MOVE_TO_SERVER_CAT",
		"COMMAND_DATA_TO_SERVER_CAT",
		"CREATE_CHAR_SERVER_CAT",
		"HIT_PLAYER_CAT",
		"ADD_ACCT_TO_SERVER_CAT",
		"EXMSG_TO_SERVER_CAT",
		"UKNOWN 23",
		"ATTRIB_TO_SERVER_CAT",
		"STOREBLOCK_CAT",
		"ENTITYGUPDATE_TO_SERVER_OLD (Guaranteed, Deprecated)",
		"UNKNOWN 27",
		"MSGHANDLER_TO_CLIENT_CAT",
		"LOOTBLOCK_CAT",
		"TIMESYNC_TO_CLIENT_CAT",
		"UNKNOWN 31",
		"DAMAGEINFO_DATA_CAT",
		"BLOCKQUEST_CAT",
		"SKILLABILITIES_CAT",
		"SKILLS_CAT",
		"BLOCKQUEST_BOOKIE_CAT",
		"UNKNOWN 37",
		"ENTITYGUPDATE_TO_CLIENT_OLD (Guaranteed, Deprecated)"
		"UNKNOWN 39",
		"AIDYNSPAWN_TO_CLIENT_CAT",
		"ENTITYGUPDATE_TO_SERVER_CAT (Guaranteed)",
		"ENTITYGUPDATE_TO_CLIENT_CAT (Guaranteed)"
		"UNKNOWN 43",
};

//  this if from server
static const char* SEND_CAT_STRINGS[] = {
		"UNKNOWN 0",
		"LOG_TO_SERVER_CAT",
		"MOVE_CAT",
		"FIRE_TO_SERVER_CAT",
		"SHRUNK_TO_SERVER_CAT",
		"EQUIP_TO_CLIENT_CAT",
		"ATTRIB_TO_CLIENT_CAT",
		"HOUSING_TO_CLIENT_CAT",
		"SPAWN_FROM_SERVER_CAT",
		"ITEM_CHECK_TO_CLIENT_CAT",
		"ITEM_LOC_TO_CLIENT_CAT",
		"CLAN_TEXT_MSG_CAT",
		"MENUBUILD_TO_CLIENT_CAT",
		"UNKNOWN 13",
		"UNKNOWN 14",
		"UNKNOWN 15",
		"UNKNOWN 16",
		"AI_SHRUNK_MOVE_TO_SERVER_CAT",
		"COMMAND_DATA_TO_SERVER_CAT",
		"ENTITYPOS_TO_CLIENT_OLD (NonGuaranteed, Deprecated)",
		"BLOCKITEM_LIST_CAT",
		"ADD_ACCT_TO_SERVER_CAT",
		"STAT_TO_CLIENT_CAT",
		"UKNOWN 23",
		"BANKBLOCK_CAT",
		"STOREBLOCK_CAT",
		"HOUSE_STOREBLOCK_CAT",
		"UNKNOWN 27",
		"MSGHANDLER_TO_CLIENT_CAT",
		"LOOTBLOCK_CAT",
		"TIMESYNC_TO_CLIENT_CAT",
		"UNKNOWN 31",
		"DAMAGEINFO_DATA_CAT",
		"BLOCKQUEST_CAT",
		"SKILLABILITIES_CAT",
		"SKILLS_CAT",
		"BLOCKQUEST_BOOKIE_CAT",
		"UNKNOWN 37",
		"ENTITYGUPDATE_TO_CLIENT_OLD (Guaranteed, Deprecated)",
		"UNKNOWN 39",
		"AIDYNSPAWN_TO_CLIENT_CAT",
		"UNKNOWN 41",
		"ENTITYGUPDATE_TO_CLIENT_CAT (Guaranteed)"
		"ENTITYPOS_TO_CLIENT_CAT (NonGuaranteed)",
};


const char* const RECV_ATTRIB_STRINGS[] = {
	"UNKNOWN 0",
	"GET_ATTACHMENT_STATES", // 1;
	"REMOVE_PLAYER_BY_NETID", // 2;
	"UNKNOWN 3", 
	"REQUEST_ITEM_INFO", // 4;
	"UNKNOWN 5", 
	"UNKNOWN 6", 
	"UNKNOWN 7", 
	"CREATE_PLAYER", // 8;
	"SELECT_CHARACTER", // 9;
	"DELETE_PLAYER", // 10;
	"PLAYER_IS_LOOTING", // 11;
	"UNKNOWN 12", 
	"UNKNOWN 13", 
	"GET_CHARACTER_LIST", // 14;
	"SET_INVENTORY_ITEM", // 15;
	"AI_REQUEST_UPDATE", // 16;
	"ARM_NEXT_ITEM", // 17;
	"SET_DEFORMABLE_ITEM", // 18;
	"GET_PLAYER_INVENTORY", // 19;
	"UNKNOWN 20", 
	"BUY_ITEM", // 21;
	"SELL_ITEM", // 22;
	"LOOT_ITEM", // 23;
	"BANK_DEPOSIT_ITEM", // 24;
	"BANK_WITHDRAW_ITEM", // 25;
	"REQUEST_PORTAL", // 26;
	"USE_ITEM", // 27;
	"UNKNOWN 28", 
	"CANCEL_TRANSACTION", // 29;
	"ACCEPT_TRADE_TERMS", // 30;
	"DESTROY_ITEM_BY_GLID", // 31;
	"QUEST_HANDLING", // 32;
	"USE_SKIL", // 33;
	"UNKNOWN 34", 
	"UNKNOWN 35", 
	"UNKNOWN 36", 
	"EXPAND_ITEM_DATA", // 37;
	"REQUEST_HOUSE_CONFIG", // 38;
	"BUY_HOUSE_ITEM", // 39;
	"ADD_HOUSE_ITEM", // 40;
	"REMOVE_HOUSE_ITEM", // 41;
	"REMOVE_HOUSE_CASH", // 42;
	"REORG_BANK_ITEMS", // 43;
	"REORD_INVENTORY_ITEMS", // 44;
	"REQUEST_QUEST_JOURNAL", // 45;
	"REPORT_MURDER", // 46;
	"GET_PLAYER_INVENTORY_ATTACHABLE", // 47;
	"TRY_ON_ITEM" // 48;
};


typedef struct 
{
	const char* name;
	int 	id;
} ServerAttrib;

static ServerAttrib SEND_ATTRIB_STRINGS[] = {
	{"AE_NO_TYPE",0},
	{"AE_PLAYER_HEALTH",3},
	{"AE_PLAYER_CREATED_HEALTH",8},
	{"AE_MOUNT",9},
	{"AE_DELETE_ENTITY",10},
	{"AE_DEMOUNT",11},
	{"AE_AI_KILL",12},
	{"AE_DELETE_CHAR",14},
	{"AE_CHANGE_DEFCFG",15},
	{"AE_PLAYER_CREDITS",19},
	{"AE_PLAYER_INFO",21},
	{"AE_BANK_CREDITS",22}, 
	{"AE_OPEN_MENU",24}, 
	{"AE_CLOSE_MENU",25}, 
	{"AE_INVALID_GROUP_JOIN",26}, 
	{"AE_AMMO_DATA",27},
	{"AE_FORCE_JET",28},
	{"AE_LOOT_DATA",29},
	{"AE_QUEST_MENU",33},
	{"AE_PLAYER_INVEN",51},
	{"AE_VENDOR_INVEN",52},
	{"AE_TRADE_OFFER",59},
	{"AE_SKILLS",141},
	{"AE_GENERIC_TYPE",142},
	{"AE_ARMITEM_TYPE",143},
	{0,0}
};


static const char* MapServerAttribId( int attribId )
{
	for ( int i = 0; i < _countof( SEND_ATTRIB_STRINGS ); i++ )
	{
		if ( SEND_ATTRIB_STRINGS[i].id == attribId )
			return SEND_ATTRIB_STRINGS[i].name;
	}
	return "Unknown";
}

// copied from IGameState
static const char* MapDisconnectReason( ULONG disconnectReason ) 
{
	//
	// in addition to logging, these strings are used in the login_log.reason_code enum
	// in the database, so the strings here should match the db enum values 
	//

	switch ( disconnectReason )
	{
		case DISC_NORMAL:
			return "NORMAL";
		case DISC_LOST:
			return "LOST";
		case DISC_TERMINATED:
			return "TERMINATED";
		case DISC_FORCE:
			return "FORCE";
		case DISC_TIMEOUT:
			return "TIMEOUT";
		case DISC_BANNED:
			return "BANNED";
		case DISC_SECURITY_VIO:
			return "SECURITY_VIO";
		case DISC_COMM_ASSIGN:
			return "COMM_ASSIGN";
		case DISC_PLAYER_ZONED:
			return "PLAYER_ZONED";
		case DISC_SERVER_RESTART:
			return "SERVER_RESTART";
		default:
			return "UNKNOWN";
	}
}

PlaybackDumper::PlaybackDumper(	IN FILE *f, IN FILE *dumpFile, IN bool showOffsets, IN bool logMoves, 
					IN IDispatcher &dispatcher, IN NETID replyNetId ,
					IN INetworkLogger *callback, bool autoStart ) : 
				PlaybackReader( replyNetId, f, dispatcher, callback, autoStart),
				m_dumpFile( dumpFile ),
				m_logOffsets( showOffsets ),
				m_movementUpdates(0),
				m_logMoves( logMoves ),
				m_deleteMe( true )
{
	jsAssert( f && dumpFile && callback );
	m_sleepMultiplier = 0.0; // we never sleep
}


// IPacketProcessor override to handle structs for dumping or playback
bool PlaybackDumper::ProcessStruct( IN NETID id, IN bool isSend, IN bool nested, IN int categoryId, IN const BYTE *data, IN ULONG len )
{
	bool ret = true;
	
	string catName;
	if ( isSend )
	{
		catName = categoryId < _countof(SEND_CAT_STRINGS) ? SEND_CAT_STRINGS[categoryId] : "UNKNOWN";
	}
	else
	{
		catName = categoryId < _countof(RECV_CAT_STRINGS) ? RECV_CAT_STRINGS[categoryId] : "UNKNOWN";
	}

	if ( categoryId == SHRUNK_TO_SERVER_CAT )
		m_movementUpdates++;
	else 
		checkMovementCount();

	if ( !nested && categoryId != SHRUNK_TO_SERVER_CAT || (m_logMoves && m_movementUpdates == 1) )
	{
		if ( m_logOffsets )
			fprintf( m_dumpFile, "%8x ", m_lastTell );
			
		fprintf( m_dumpFile, "%s Cat:   %4d %s\n",
								m_tag,
								categoryId,
								catName.c_str() );
	}

	if ( categoryId == ATTRIB_TO_SERVER_CAT )
	{
		// multiple items, skipping first byte for whatever reason
		len -= 1;
		data += 1;
		int count = len / sizeof(ATTRIB_STRUCT);
		jsAssert( len % sizeof(ATTRIB_STRUCT) == 0 );
		fprintf( m_dumpFile, "         \tattribCount = %2d\n", count );
		for ( int i = 0; i < count; i++ )
		{
			ProcessAttrib( id, isSend, false, (LPATTRIB_STRUCT)data, data, len );
			data += sizeof( ATTRIB_STRUCT );
			len -= sizeof( ATTRIB_STRUCT );
		}
	}
	else if ( categoryId == ATTRIB_TO_CLIENT_CAT )
	{
		ProcessAttrib( id, isSend, false, (LPATTRIB_STRUCT)data, data, len );
	}
	else if ( categoryId == ENTITYGUPDATE_TO_SERVER_CAT || categoryId == ENTITYGUPDATE_TO_CLIENT_CAT || 
			  categoryId == ENTITYGUPDATE_TO_SERVER_OLD || categoryId == ENTITYGUPDATE_TO_CLIENT_OLD )
	{
		jsVerifyReturn( 
			(categoryId == ENTITYGUPDATE_TO_SERVER_CAT || categoryId == ENTITYGUPDATE_TO_CLIENT_CAT) && len >= sizeof(ENTITYGUPDATE_HEADER) ||
			(categoryId == ENTITYGUPDATE_TO_SERVER_OLD || categoryId == ENTITYGUPDATE_TO_CLIENT_OLD) && len >= sizeof(ENTITYGUPDATE_HEADER_OLD), false );

		ENTITYGUPDATE_HEADER * guarHeader = (ENTITYGUPDATE_HEADER *)data;

		// Convert to latest message format if necessary
		ENTITYGUPDATE_HEADER tmpHeader;
		int msgVer = ProtocolHelper::parseMessageVersion(data);
		if( ProtocolCompatibility::Instance().isConversionRequired(PC_MSG_ENTITYGUPDATE, msgVer) )
		{
			// Convert old message format
			ProtocolHelper::upConvert(data, &tmpHeader);
			guarHeader = &tmpHeader;
		}

		if ( guarHeader->m_dmgMsgCount > 0 )
			fprintf( m_dumpFile, "         \tdmgMsgCount        = %2d\n", guarHeader->m_dmgMsgCount );
		// should  always be zero 
		if ( guarHeader->m_txtMsgCount )
			fprintf( m_dumpFile, "         \ttxtMsgCount        = %2d\n", guarHeader->m_txtMsgCount );
		if ( guarHeader->m_equipMsgCount > 0 )
			fprintf( m_dumpFile, "         \tequipMsgCount      = %2d\n", guarHeader->m_equipMsgCount );
		if ( guarHeader->m_updateStatMsgCount > 0 )
			fprintf( m_dumpFile, "         \tupdateStatMsgCount = %2d\n", guarHeader->m_updateStatMsgCount );
		// should always zero 
		if( guarHeader->m_grpTxtMsgCount )
			fprintf( m_dumpFile, "         \tgrpTxtMsgCount     = %2d\n", guarHeader->m_grpTxtMsgCount );
		if ( guarHeader->m_miscMsgCount > 0 )
			fprintf( m_dumpFile, "         \tmiscMsgCount-trade = %2d\n", guarHeader->m_miscMsgCount );
		if ( guarHeader->m_attributeMsgCount )
			fprintf( m_dumpFile, "         \tattributeMsgCount  = %2d\n", guarHeader->m_attributeMsgCount );
		if ( guarHeader->m_eventCount )
			fprintf( m_dumpFile, "         \teventCount         = %2d\n", guarHeader->m_eventCount );

		return crackGuaranteedPacket( *this, id, isSend, data, len );
	}
	else if ( categoryId == ENTITYPOS_TO_CLIENT_CAT || categoryId == ENTITYPOS_TO_CLIENT_OLD )
	{
		jsVerifyReturn( 
			categoryId == ENTITYPOS_TO_CLIENT_CAT && len >= sizeof( ENTITYPOS_HEADER ) ||
			categoryId == ENTITYPOS_TO_CLIENT_OLD && len >= sizeof( ENTITYPOS_HEADER_OLD ), false );
		ENTITYPOS_HEADER * nonGuarHeader = (ENTITYPOS_HEADER *)data;

		// Convert to latest message format if necessary
		ENTITYPOS_HEADER tmpHeader;
		int msgVer = ProtocolHelper::parseMessageVersion(data);
		if( ProtocolCompatibility::Instance().isConversionRequired(PC_MSG_ENTITYPOS, msgVer) )
		{
			// Convert old message format
			ProtocolHelper::upConvert(data, &tmpHeader);
			nonGuarHeader = &tmpHeader;
		}
		
		if ( nonGuarHeader->m_damageInfoMsgCount > 0 )
			fprintf( m_dumpFile, "         \tdamageInfoMsgCount  = %2d\n", nonGuarHeader->m_damageInfoMsgCount );
		if ( nonGuarHeader->m_detailedMsgCount > 0 )
			fprintf( m_dumpFile, "         \tdetailedMsgCount    = %2d\n", nonGuarHeader->m_detailedMsgCount );
		if ( nonGuarHeader->m_entPosCount > 0 )
			fprintf( m_dumpFile, "         \tentPosCount         = %2d\n", nonGuarHeader->m_entPosCount );
		if ( nonGuarHeader->m_eventCount > 0 )
			fprintf( m_dumpFile, "         \teventCount          = %2d\n", nonGuarHeader->m_eventCount );
		if ( nonGuarHeader->m_fireMsgCount > 0 )
			fprintf( m_dumpFile, "         \tfireMsgCount        = %2d\n", nonGuarHeader->m_fireMsgCount );
		if ( nonGuarHeader->m_generalMsgCount > 0 )
			fprintf( m_dumpFile, "         \tgeneralMsgCount     = %2d\n", nonGuarHeader->m_generalMsgCount );
		if ( nonGuarHeader->m_itemCfgMsgCount > 0 )
			fprintf( m_dumpFile, "         \titemCfgMsgCount     = %2d\n", nonGuarHeader->m_itemCfgMsgCount );
		if ( nonGuarHeader->m_staticCount > 0 )
			fprintf( m_dumpFile, "         \tstaticCount         = %2d\n", nonGuarHeader->m_staticCount );

		return crackNonGuaranteedPacket( *this, id, isSend, data, len );
	}
	else if ( categoryId == DETAILEDMSG_DATA_CAT && nested )
	{
		// nested in non-guaranteed
		jsVerifyReturn( len >= sizeof( DETAILEDMSG_DATA ), false );
		LPDETAILEDMSG_DATA msg = (LPDETAILEDMSG_DATA)data;
		fprintf ( m_dumpFile, "         \t\tDetail: %4d %d %d %d\n", msg->m_data, msg->m_miscInt, msg->m_miscInt2, msg->m_miscInt3 );
	}
	else if ( categoryId == MSGHANDLER_TO_CLIENT_CAT && nested )
	{
		// nested in non-guaranteed
		jsVerifyReturn( len >= sizeof( MSGHANDLER ), false );
		LPMSGHANDLER msg = (LPMSGHANDLER)data;
		fprintf ( m_dumpFile, "         \t\tMsg:   %4d \"%s\"\n", msg->m_data, loadLocaleString(2000+msg->m_data).c_str() );
	}
	
	return ret;
}


// IPacketProcessor override to handle IEvnts for dumping or playback
bool PlaybackDumper::ProcessEvent( IN NETID id, IN bool isSend, IN bool nested, IN EventHeader *header, IN const BYTE *data, IN ULONG len )
{
	bool ret = true;

	checkMovementCount();
	if ( !nested )
	{
		if ( m_logOffsets )
			fprintf( m_dumpFile, "%8x ", m_lastTell );
			
		fprintf( m_dumpFile, "%s Event: %4d %s f = %d o = %d\n",
							m_tag,
							header->Id(),
							m_dispatcher.GetEventName(header->Id()).c_str(),
							header->Filter(),
							header->ObjectId() );
	}
	else									
		fprintf( m_dumpFile, "         \t\tEvent:  %4d %s f = %d o = %d\n",
							header->Id(),
							m_dispatcher.GetEventName(header->Id()).c_str(),
							header->Filter(),
							header->ObjectId() );

	// show text in dump 
	if ( header->Id() == RENDER_TEXT_EVENT_ID )
	{
		RenderTextEvent rde( header->Id(), header, data+sizeof(EventHeader), len - sizeof(EventHeader), id, EP_NORMAL );
		try
		{
			// looks like rde->ExtractInfo is not a good thing to do, pulls out more than in event
			string text;
			(*rde.InBuffer()) >> text;
			fprintf( m_dumpFile, "         \t\t\t\"%s\"\n", text.c_str() );
		}
		catch ( KEPException *e )
		{
			LOG4CPLUS_WARN( m_logger, "Exception decoding render text event: " << e->ToString() );
			e->Delete();
		}
	}
	else if ( header->Id() == RENDER_LOCALE_TEXT_EVENT_ID )
	{
		RenderLocaleTextEvent rde( header->Id(), header, data+sizeof(EventHeader), len - sizeof(EventHeader), id, EP_NORMAL );
		try
		{
			string text;
			ChatType ct;
			string toDistribution, fromRecipient;
			rde.ExtractInfo( text, ct );
			fprintf( m_dumpFile, "         \t\t\t\"%s\"\n", text.c_str() );
		}
		catch ( KEPException *e )
		{
			LOG4CPLUS_WARN( m_logger, "Exception decoding render locale text event: " << e->ToString() );
			e->Delete();
		}
	}
	else if ( header->Id() == TEXT_EVENT_ID )
	{
		TextEvent rde( header->Id(), header, data+sizeof(EventHeader), len - sizeof(EventHeader), id, EP_NORMAL );
		try
		{
			ULONG commandId;
			int fromNetworkAssignedId;
			string text;
			ChatType ct;
			rde.ExtractInfo( commandId, fromNetworkAssignedId, text, ct );
			fprintf( m_dumpFile, "         \t\t\t\"%s\"\n", text.c_str() );
		}
		catch ( KEPException *e )
		{
			LOG4CPLUS_WARN( m_logger, "Exception decoding text event: " << e->ToString() );
			e->Delete();
		}
	}
	else if ( header->Id() == SCRIPT_CLIENT_EVENT_ID )
	{
		ScriptClientEvent rde( header->Id(), header, data+sizeof(EventHeader), len - sizeof(EventHeader), id, EP_NORMAL );
		try
		{
			string text;
			auto eventType = rde.GetEventType();
			switch( eventType )
			{
				case ScriptClientEvent::PlayerTell:
				{
					string msg, title;
					int placementId, isTell;
					ULONG clientId = 0;

					(*rde.InBuffer()) >> msg >> title >> placementId >> isTell >> clientId;

					if ( isTell )
						text = title + TELL_TEXT + msg; 
					else
						text = title + SAY_TEXT + msg;

					break;
				}
				default:
					break;
			}

			fprintf( m_dumpFile, "         \t\t\t\"%s\"\n", text.c_str() );
		}
		catch ( KEPException *e )
		{
			LOG4CPLUS_WARN( m_logger, "Exception decoding text event: " << e->ToString() );
			e->Delete();
		}
	}

	return ret;
}


// IPacketProcessor override to handle attrib structs for dumping or playback
bool PlaybackDumper::ProcessAttrib( IN NETID id, IN bool isSend, IN bool nested, IN ATTRIB_STRUCT *attrib, IN const BYTE *data, IN ULONG len )
{
	bool ret = true;

	const char* name;
	if ( isSend )
		name = MapServerAttribId( attrib->attribute );
	else
		name = attrib->attribute < _countof(RECV_ATTRIB_STRINGS) ? RECV_ATTRIB_STRINGS[attrib->attribute] : "UNKNOWN";
		
	fprintf( m_dumpFile, "         \t\tAttrib: %4d %s %d %d %d %d\n", attrib->attribute, name,
						     attrib->uniInt, attrib->uniInt2, (int)attrib->uniShort3, (int)attrib->idRefrence );

	return ret;
}


void PlaybackDumper::processValidate( IN CONTEXT_VALIDATION *context, 
									  IN const char* ipAddress, 
									  IN const char* userInfoXML ) 
{									   
	if ( m_logOffsets )
		fprintf( m_dumpFile, "%8x ", m_lastTell );
	fprintf( m_dumpFile, "%s %s\n", m_tag, ipAddress );
}

void PlaybackDumper::processConnect( IN NETID myNetId, IN const char* playerName ) 
{									   
	if ( m_logOffsets )
		fprintf( m_dumpFile, "%8x ", m_lastTell );
	fprintf( m_dumpFile, "%s %s", m_tag, playerName );
	if ( m_logMoves )
		fprintf( m_dumpFile, " - NetId = %d", myNetId );
	fprintf( m_dumpFile, "\n" );		
}

void PlaybackDumper::processDisconnect( IN eDisconnectReason reason )
{

	if ( m_logOffsets )
		fprintf( m_dumpFile, "%8x ", m_lastTell );
	fprintf( m_dumpFile, "%s %d %s", m_tag, reason, MapDisconnectReason(reason) );
	if ( m_logMoves )
		fprintf( m_dumpFile, " - NetId = %d", m_myNetId );
	fprintf( m_dumpFile, "\n" );		
}

void PlaybackDumper::cleanup()
{
	SendMsgToAdmin( m_replyNetId, m_dispatcher, "Dump complete" );
	
	fclose( m_dumpFile );

	PlaybackReader::cleanup();

	if ( m_deleteMe )
		delete this;
}

void PlaybackDumper::checkMovementCount()
{
	if ( m_logMoves && m_movementUpdates > 1  )
	{
		fprintf( m_dumpFile, "         \t\t\tskipped %d\n", m_movementUpdates );
	}
	m_movementUpdates = 0;
}

bool PlaybackDumper::ProcessPacket( IN NETID id, IN bool isSend, IN const BYTE *data, IN ULONG msgSize ) 
{ 
	if ( isSend )
		checkMovementCount();
	return true; 
}

