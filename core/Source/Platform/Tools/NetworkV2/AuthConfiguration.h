///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "common/keputil/url.h"
#include <Common/include/NetworkCfg.h>

namespace KEP {

class AuthConfiguration
{
public:
	AuthConfiguration() 
		: _authURL(){}


	AuthConfiguration& initialize(const std::string& basedir);
	
	AuthConfiguration(const AuthConfiguration& authConf)
		: _authURL(authConf._authURL)
	{
		
	}

	AuthConfiguration& operator=(const AuthConfiguration& authConf) {
		_authURL = authConf._authURL;
		return *this;
	}


	URL _authURL;
};

} // namespace KEP