#
# little script to make a watermarked binaris install
#
#
#
#
use strict;
use Digest::MD5 qw(md5 md5_hex md5_base64);

print STDERR "\n";

if ( $#ARGV < 0 )
{
	print STDERR "Supply new or existing watermark string\n\n" ;
	
	open ( NAMES, "EliteNames.txt" ) || die "Can't open EliteNames.txt $!\n";
	print STDERR "Existing strings are (quoted):\n";
	while ( <NAMES> )
	{
		chomp;
		my $x;
		($x) = split /\t/;
		next if ( $x =~ /^#/ || length($x) == 0 );
		print STDERR "\t\"$x\"\n";
	}
	print STDERR "\n\n";
	exit(0);
}

checkName( $ARGV[0] );

waterMarkIt( $ARGV[0] );

buildIt();

print STDERR "\nComplete.\n\n";

sub checkOutput($)
{
  my $rc;
  $rc = @_[0];
  
  print "\texit code is $rc\n";
  if ( $rc != 0 )
  {
    open ERR, "err.out";
    print $_ while (<ERR>);
    close ERR;
    die "Error bulding\n";
  }
}

sub buildIt()
{
	print STDERR "Building... watch for errors\n";

  checkOutput(system "devenv wmNetwork.vcproj /rebuild Debug > err.out");
  
	print STDERR "\tdebug complete...building release...\n";
	
  checkOutput(system "devenv wmNetwork.vcproj /rebuild Release > err.out");
}

sub checkName($)
{
	my $name;
	$name = $_[0];
	
	open ( NAMES, "EliteNames.txt" ) || die "Can't open EliteNames.txt $!\n";
	while ( <NAMES> )
	{
		chomp;
		my $x;
		($x) = split /\t/;

		if ( $x eq $name )
		{
			print STDERR "Found existing name.\n";
			return;
		}
	}

	close( NAMES );

	print STDERR "New name, attempting checkout and update...\n";
	
	die "Must set SSUSER, SSPWD, and SSDIR env. variables\n" if ( length($ENV{"SSPWD"}) == 0  || length($ENV{"SSUSER"}) == 0 || length($ENV{"SSDIR"}) == 0 );

	# not found if get here
	`ss checkout -I-Y -GF -GWR "\$/kep/source/Shared/Tools/Network/EliteNames.txt"`;

	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst);
	
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	
	open ( NAMES, ">>EliteNames.txt" ) || die "Can't open EliteNames.txt $!\n";
	$year += 1900;
	print NAMES	"$name\t".md5_base64($_[0])."\tAdded $mon/$mday/$year $hour:$min\n";
	close NAMES;
	
	`ss checkin -GF "\$/kep/source/Shared/Tools/Network/EliteNames.txt" "-CAdd new elite source"`;
}

sub waterMarkIt($)
{
	my $newWatermark; 
	$newWatermark = md5_base64($_[0]);

	print STDERR "Setting water mark for $ARGV[0] => $newWatermark\n";

	open ( INPUT, "network.vcproj" )|| die "Can't open network.vcproj $!\n";
	open ( OUTPUT, ">wmnetwork.vcproj" ) || die "Can't open wmnetwork.vcproj $!\n";

	while (<INPUT>)
	{
		s/KANEVAROCKS/$newWatermark/g;
		print OUTPUT $_;
		
	}

	print STDERR "Output written to wmnetwork.vcproj\n";
}
