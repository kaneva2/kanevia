///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <windows.h>
#include <vector>

namespace KEP {

// constants
typedef ULONG NETID;
typedef std::vector<NETID> NETIDVect;
typedef char LOGON_RET;
} // namespace
