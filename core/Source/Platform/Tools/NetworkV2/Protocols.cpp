///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Protocols.h"

ProtocolCompatibility ProtocolCompatibility::ms_instance;

static ProtocolVersionInfo gSupportedVersionsRaw[] = {
//	{ 0x8029, { 0 } }, // drf - ED-5456 - message optimization (phase 1)
//	{ 0x8030, { 1 } }, // yc - SPAWN_STATIC_MO message version 1 - added scaling vector
//	{ 0x8031, { 0 } }, // drf - ED-5456 - message optimization (phase 2)
//	{ 0x8032, { 1 } }, // re-enable SPAWN_STATIC_MO message version 1
	{ 0x8033, { 1 } }, // drf - Engine message format change (ULONG max size, breaking)

	// Add more versions here
	// Be sure to also update MSG_HEADER::msgVer in CStructuresMisl.h

	// End of array
	{ INVALID_PROTOCOL_VERSION	},
};

ProtocolCompatibility::ProtocolCompatibility() : m_latestVersion(NULL), m_legacyVersionNumber(INVALID_PROTOCOL_VERSION)
{
	// Convert version array into map
	for( int i=0; gSupportedVersionsRaw[i].globalVersion!=INVALID_PROTOCOL_VERSION; i++ )
	{
		const ProtocolVersionInfo & pv = gSupportedVersionsRaw[i];
		m_supportedVersions.insert( std::make_pair(pv.globalVersion, pv) );
	}

	ProtocolMap::reverse_iterator it = m_supportedVersions.rbegin();
	if( it!=m_supportedVersions.rend() )
	{
		m_latestVersion = &it->second;
	}
}

ULONG ProtocolCompatibility::getMinVersion() const
{
	ProtocolMap::const_iterator it = m_supportedVersions.begin();
	if( it!=m_supportedVersions.end() ) {
		return it->first;
	}
	return INVALID_PROTOCOL_VERSION;
}

ULONG ProtocolCompatibility::getMaxVersion() const
{
	ProtocolMap::const_reverse_iterator it = m_supportedVersions.rbegin();
	if( it!=m_supportedVersions.rend() ) {
		return it->first;
	}
	return INVALID_PROTOCOL_VERSION;
}

ULONG ProtocolCompatibility::negotiate( ULONG peerMinVer, ULONG peerMaxVer )
{
	// Intersect version ranges
	int minVer = std::max(peerMinVer, getMinVersion());
	int maxVer = std::min(peerMaxVer, getMaxVersion());

	// Check if compatible
	if( minVer>maxVer )
	{
		// Incompatible
		return INVALID_PROTOCOL_VERSION;
	}
	
	// Use maximum version supported by both party
	return maxVer;
}
