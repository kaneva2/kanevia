///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "GetBrowserPage.h"

#include "RyeolHttpClient.h"

#include <sstream>

using namespace std;

#define UPLOAD_CHUNK_SIZE 256*1024	// 256KB

std::string formatError( const std::string& errorClass, DWORD code, const std::string& msg ) {
    std::stringstream ss;
    ss << "GetBrowserPage: " << errorClass << " Error [" << code << "] " << msg;
    return ss.str();
}


class thdParm
{
public:
	thdParm(HINTERNET &_hConnection, HINTERNET &_hInternet, Ryeol::CHttpClientA *&_pobjHttpReq, Ryeol::CHttpResponseA *& _pobjHttpRes, const char* &_url, const PostValuesList & values ) :
		pobjHttpReq(_pobjHttpReq), pobjHttpRes(_pobjHttpRes),  hInternet(_hInternet), hConnection(_hConnection), url(_url), valuesToPost(values) {}

	Ryeol::CHttpClientA *& pobjHttpReq;
	Ryeol::CHttpResponseA *& pobjHttpRes;
	HINTERNET &hInternet;
	HINTERNET &hConnection;
	const char* &url;
	DWORD httpStatusCode;
	std::string httpStatusDescription;
	PostValuesList valuesToPost;
};


DWORD WINAPI BackgroundConnect( LPVOID vThreadParm )
{
	thdParm *p = (thdParm*)vThreadParm;
	try
	{
		p->pobjHttpRes = p->pobjHttpReq->RequestGetEx(p->hInternet, p->hConnection, p->url,
														Ryeol::HTTPCLIENT_DEF_REQUEST_FLAGS_NOCACHE &
														~INTERNET_FLAG_KEEP_CONNECTION ) ;
	}
	catch (Ryeol::httpclientexceptionA & e)
	{
		char buffer[MAX_PATH];
		sprintf_s(buffer, _countof(buffer), "%s Make sure you have internet connection, and you are not behind an firewall\n",e.errmsg());
		p->httpStatusCode = 500;
		p->httpStatusDescription = buffer;
		
		return 1;
	}
//#define BREAKIT__
#ifndef BREAKIT__
	catch (std::exception& e)  // These are synonyms, but compiler is not figuring this out.
	{
		// N.B.: In upgraded system, we are ending up
		// here because client is build with UNICODE
		// however, in this case the "A" variant was thrown
		// NOT the "W" variant
		//
		p->httpStatusCode = 500;
		p->httpStatusDescription = std::string("Unknown exception raised: ") + e.what();
		return 1;
	}
	catch (...) {
		p->httpStatusCode = 500;
		p->httpStatusDescription = "Unknown exception raised";
		return 1;
	}
#endif
	return 0;
}

DWORD WINAPI BackgroundPostConnect( LPVOID vThreadParm )
{
	thdParm *p = (thdParm*)vThreadParm;
	try
	{
		p->pobjHttpRes = p->pobjHttpReq->RequestPostEx(p->hInternet, p->hConnection, p->url,
														Ryeol::HTTPCLIENT_DEF_REQUEST_FLAGS_NOCACHE &
														~INTERNET_FLAG_KEEP_CONNECTION ) ;
	}
	catch (Ryeol::httpclientexceptionA & e)
	{
		char buffer[MAX_PATH];
		sprintf_s(buffer, _countof(buffer), "%s Make sure you have internet connection, and you are not behind an firewall\n",e.errmsg());
		p->httpStatusCode = 500;
		p->httpStatusDescription = buffer;
		
		return 1;
	}
	return 0;
}

DWORD WINAPI BackgroundPostFormConnect( LPVOID vThreadParm )
{
	thdParm *p = (thdParm*)vThreadParm;
	try
	{
		// Post other values along with upload file as we can't upload to a URL with param postfix (somehow weird HTTP 500 error returned)
		for(auto it = p->valuesToPost.begin(); it!=p->valuesToPost.end(); ++it )
		{
			std::string key = it->first;
			if( !key.empty() )
			{
				if( key[0]=='@' )
				{
					//File
					key = key.substr(1);
					p->pobjHttpReq->AddParam ( key.c_str(), it->second.c_str(), Ryeol::CHttpClientA::ParamFile );
				}
				else
				{
					//Value
					p->pobjHttpReq->AddParam( key.c_str(), it->second.c_str() );
				}
			}
		}

		p->pobjHttpReq->BeginUploadEx(p->url) ;

		Ryeol::CHttpResponseA* resp = NULL ;
		while ( !resp )
		{
			resp = p->pobjHttpReq->Proceed ( UPLOAD_CHUNK_SIZE );
			fTime::SleepMs(20);
		}

		p->pobjHttpRes = resp;
	}
	catch (Ryeol::httpclientexceptionA & e)
	{
		char buffer[MAX_PATH];
		sprintf_s(buffer, _countof(buffer), "%s Make sure you have internet connection, and you are not behind an firewall\n",e.errmsg());
		p->httpStatusCode = 500;
		p->httpStatusDescription = buffer;
		
		return 1;
	}
	return 0;
}


using namespace std;

// does string or file or both	
static bool GetBrowserPage(
	const std::string& url,
	FILE* outputFile, 
	std::string* resultText,
	size_t& resultSize,
	DWORD& httpStatusCode, 
	std::string& httpStatusDescription,
	bool usePost, 
	TimeMs timeoutMs,
	const PostValuesList& valuesToPost )
{
	Ryeol::CHttpClientA* pobjHttpReq = NULL;
	Ryeol::CHttpResponseA* pobjHttpRes = NULL;
	
	bool ret = false;
	resultSize = 0;
	httpStatusCode = 0;
	httpStatusDescription.clear();

	const DWORD NotLoggedIn = 490; // Coordinated with Kaneva web site to signify user is not logged in.

	HINTERNET hInternet = NULL ;
	HINTERNET hConnection = NULL ;

	// get request on background thd since timeout doesn't work
	int retries = 1;
	bool ok = false;
	
	for ( int i = 0; i < retries && !ok; i++ )
	{
		try
		{						
			pobjHttpReq = new Ryeol::CHttpClientA();

			pobjHttpReq->SetInternet ( "Kaneva Platform" ) ;
			hInternet = pobjHttpReq->OpenInternet () ;

			hConnection = pobjHttpReq->OpenConnection(hInternet, url.c_str());

			if ( hConnection!=NULL)
			{
				DWORD dwTimeoutMs = (DWORD) timeoutMs;
				if(FALSE==::InternetSetOption (
				    hConnection
					,INTERNET_OPTION_RECEIVE_TIMEOUT
					,&dwTimeoutMs
					,sizeof (dwTimeoutMs)) )
				{
					char buffer[MAX_PATH];
					httpStatusCode = GetLastError();
					sprintf_s(buffer,_countof(buffer),"Set internet receive timeout failed.Error:%d", httpStatusCode);
					httpStatusDescription = buffer;

					if(pobjHttpReq)
					{
						delete pobjHttpReq;
						pobjHttpReq = NULL ;
					}

					if ( hConnection != NULL ) 
						::InternetCloseHandle (hConnection) ;

					if ( hInternet != NULL ) 
						::InternetCloseHandle (hInternet) ;

					return false;
				}

				if ( strstr(url.c_str(), "gzip=true") != 0 ) // accept compressed reults set
				{
					pobjHttpReq->AddHeader("Accept-Encoding", "gzip");

					BOOL b = TRUE;
					if ( FALSE == ::InternetSetOption(
							hConnection
							, INTERNET_OPTION_HTTP_DECODING
							, &b
							, sizeof(b)) )
					{
						char buffer[MAX_PATH];
						httpStatusCode = GetLastError();
						sprintf_s(buffer, _countof(buffer), "Set internet compressed decoding failed. Error:%d", httpStatusCode);
						httpStatusDescription = buffer;

						if ( pobjHttpReq )
						{
							delete pobjHttpReq;
							pobjHttpReq = NULL;
						}

						if ( hConnection != NULL ) 
							::InternetCloseHandle(hConnection);

						if ( hInternet != NULL ) 
							::InternetCloseHandle(hInternet);
						
						return false;
					}
				}
			}
			else
			{
				httpStatusCode = 500;
				httpStatusDescription = "Cannot find an internet connection";

				if(pobjHttpReq)
				{
					delete pobjHttpReq;
					pobjHttpReq = NULL ;
				}

				if ( hConnection != NULL ) 
					::InternetCloseHandle (hConnection) ;

				if ( hInternet != NULL ) 
					::InternetCloseHandle (hInternet) ;

				return false;
			}

			// Send a request
			const char* urlStr = url.c_str();
			thdParm parm( hConnection, hInternet, pobjHttpReq, pobjHttpRes, urlStr, valuesToPost );

			// Determine thread routine
			LPTHREAD_START_ROUTINE threadProc = !usePost ? BackgroundConnect : valuesToPost.empty() ? BackgroundPostConnect : BackgroundPostFormConnect;

			// Create a worker thread
			HANDLE	hThread;
			DWORD	dwThreadID;
			hThread = CreateThread(
							NULL,			// Pointer to thread security attributes
							0, 			  	// Initial thread stack size, in bytes
							threadProc,	    // Pointer to thread function
							&parm, 		  	// The argument for the new thread
							0, 			  	// Creation flags
							&dwThreadID	  	// Pointer to returned thread identifier
						);
			MSG msg;

			Timer timer;
			DWORD dwret = WAIT_TIMEOUT;

			// Wait for the call to InternetConnect in worker function to complete
			while( ((dwret = WaitForSingleObject( hThread, 0 )) == WAIT_TIMEOUT) 
				&& (timer.ElapsedMs() < timeoutMs) )
			{
				if ( AfxGetApp() ) // server background thread doesn't have App
				{
					while ( ::PeekMessage ( &msg, NULL, 0, 0, PM_NOREMOVE ) )
					{
						if ( !AfxGetApp()->PumpMessage () )
						{
							::PostQuitMessage ( 0 );
							return FALSE;
						}
					}
				}
				else
				{
					while ( ::PeekMessage ( &msg, NULL, 0, 0, PM_REMOVE ) )
					{
						TranslateMessage(&msg);
						DispatchMessage (&msg);
					}
				}
				fTime::SleepMs(1);
			}

			bool timeout = false;
		    if ( dwret == WAIT_TIMEOUT )
		    {
			   	timeout=true;
				if ( hConnection )
					pobjHttpReq->KillSendRequest();

		        // Wait until the worker thread exits
		        Timer timer2;
				while( ((dwret = WaitForSingleObject( hThread, 0 )) == WAIT_TIMEOUT) 
					&& (timer2.ElapsedMs() < 10000) )	// MSDN code waited infinite
				{
					if ( AfxGetApp() ) // server background thread doesn't have App
					{
						while ( ::PeekMessage ( &msg, NULL, 0, 0, PM_NOREMOVE ) )
						{
							if ( !AfxGetApp()->PumpMessage () )
							{
								::PostQuitMessage ( 0 );
								return FALSE;
							}
						}
					}
					else
					{
						while ( ::PeekMessage ( &msg, NULL, 0, 0, PM_REMOVE ) )
						{
							TranslateMessage(&msg);
							DispatchMessage (&msg);
						}
					}
					fTime::SleepMs(1);
				}
		    }

			//if ( dwret == WAIT_FAILED )
			//	string msg;

	       // The state of the specified object (thread) is signaled
	       DWORD dwExitCode = 9999;
	       if ( GetExitCodeThread( hThread, &dwExitCode ) && dwExitCode == 0 )
				ok = true;

		   if ( dwExitCode == 1 )
		   {
				// got exception in it
			   httpStatusCode = parm.httpStatusCode;
			   httpStatusDescription = parm.httpStatusDescription;
		   }
		   else if ( dwExitCode == STILL_ACTIVE )
		   {
			   static int times=0;

			   TerminateThread( hThread, 1 );

				if(pobjHttpReq)
				{
					delete pobjHttpReq;
					pobjHttpReq = NULL ;
				}
				
				if(pobjHttpRes)
				{
					delete pobjHttpRes ;
					pobjHttpRes = NULL ;
				}
				
				if ( hConnection != NULL ) 
					::InternetCloseHandle (hConnection) ;
				
				if ( hInternet != NULL ) 
					::InternetCloseHandle (hInternet) ;
				
				CloseHandle (hThread);

				times++;
				if(times>2)
					return false;
				else
					continue;
		   }

	       CloseHandle (hThread);
		   if(!pobjHttpRes)
		   {
			   ret = false;
			   goto end;
		   }
			if ( ok )
			{
				DWORD status = pobjHttpRes->GetStatus();
				if ( status == NotLoggedIn)
				{
				}

				httpStatusCode = status;
				httpStatusDescription.assign(pobjHttpRes->GetStatusText());

				if ( status == HTTP_STATUS_OK)
					ret = true;

				// Give page regardless of error, caller may find helpful
				const DWORD     cbBuff = 1024 * 10 ;
				BYTE            byBuff[cbBuff] ;
				DWORD           dwRead ;

				// Reads the data stream returned by the HTTP server.
				while ( (dwRead = pobjHttpRes->ReadContent (byBuff, cbBuff - 1)) != 0 )
				{
					resultSize += dwRead ;
					if ( resultText )
						resultText->append(reinterpret_cast<LPCSTR> (byBuff), dwRead );
					
					if ( outputFile )
						fwrite( byBuff, 1, dwRead, outputFile );
				}
			}
		}

		catch (Ryeol::httpclientexceptionA & e)
		{
			httpStatusCode = e.LastError();
		}

		end:
		if(pobjHttpReq)
		{
			delete pobjHttpReq;
			pobjHttpReq = NULL ;
		}

		if(pobjHttpRes)
		{
			delete pobjHttpRes ;
			pobjHttpRes = NULL ;
		}

		if ( hConnection != NULL ) 
			::InternetCloseHandle (hConnection) ;

		if ( hInternet != NULL ) 
			::InternetCloseHandle (hInternet) ;
	}

	return ret;
}

//Get the browser page to a file
bool PostBrowserPageToFile(const std::string& url, const std::string& fname, DWORD &httpStatusCode, std::string & httpStatusDesc, TimeMs timeoutMs)
{
	if ( fname.empty() )
		return false;

	FILE *f = 0;
	if ( fopen_s( &f, fname.c_str(), "wb" ) != 0 )
		return false;
	size_t resultSize = 0;
	bool ret = GetBrowserPage( url, f, NULL, resultSize, httpStatusCode, httpStatusDesc, true, timeoutMs, PostValuesList() );
	fclose(f);
	return ret;
}

//Get the browser page to a string
bool PostBrowserPage(const std::string& url, std::string & resultText, DWORD &httpStatusCode, std::string & httpStatusDesc, TimeMs timeoutMs)
{
	size_t resultSize = 0;
	return GetBrowserPage( url, NULL, &resultText, resultSize, httpStatusCode, httpStatusDesc, true, timeoutMs, PostValuesList() );
}

//Post with values and file uploads
bool PostBrowserPage(const std::string& url, std::string & resultText, DWORD &httpStatusCode, std::string & httpStatusDesc, TimeMs timeoutMs, const PostValuesList & valuesToPost)
{
	size_t resultSize = 0;
	return GetBrowserPage( url, NULL, &resultText, resultSize, httpStatusCode, httpStatusDesc, true, timeoutMs, valuesToPost );
}

