// HTTPClientApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "log4cplus/logger.h"
using namespace log4cplus;

#include "tools/httpclient/GetBrowserPage.h"
#include "common\keputil\commandline.h"
#include "Common\KEPUtil\Application.h"
#include "Common\include\KEPException.h"
#include "Common\include\KEPConfig.h"

int _tmain(int argc, _TCHAR* argv[])		{
	CommandLine& cmdLine =	Application::singleton().commandLine();		// short hand for the Application's singleton command line object.
	CommandLine::StringParameter&  logCfgFile		= cmdLine.addString( "--logcfg", "%(AppName)Log.cfg", "Logging configuration file" );
	CommandLine::StringParameter&  appCfgFile		= cmdLine.addString( "--cfg", "%(AppName).cfg", "Logging configuration file" );
	CommandLine::BooleanParameter& verbose			= cmdLine.addBoolean( "--showcontent", false, "Show response content" );
	cmdLine.parse( argc, argv );

	std::string	appName	= Application::singleton().commandLine().executable().fname();
	std::transform(appName.begin(), appName.end(), appName.begin(), ::tolower);

	KEP::LoggingSubsystem( logCfgFile ).start();

	Logger		logger			= Logger::getInstance( appName );
	std::string appConfigName	= appCfgFile.value();

	try {
		TiXmlDocument configXML;
		configXML.LoadFile( appConfigName.c_str() );

		TiXmlNode* pnode = configXML.FirstChild( "HTTPClientApp" );

		if ( pnode == 0 ) return 0;



		std::string resultText;
		DWORD		httpStatusCode;
		std::string httpStatusDescription;

		for  ( TiXmlElement* element = pnode->FirstChildElement()
				; element != 0 
				; element = element->NextSiblingElement() ) {
			LOG4CPLUS_DEBUG( logger, "URL:[" << element->GetText() << "]" );
			GetBrowserPage( element->GetText()
							, resultText
							, httpStatusCode
							, httpStatusDescription
							, 30000 );
			LOG4CPLUS_INFO( logger, httpStatusCode << " " << httpStatusDescription << " " << (verbose.value() ? resultText : ""));
		}
	}
	catch( KEPException* pexc ) { LOG4CPLUS_ERROR( logger, pexc->ToString() );		}
	catch( ... )				{ LOG4CPLUS_ERROR( logger, "Unknown exception" );	}
	return 0;
}






//					element->GetText();
//		KEP::KEPConfig& config = Application::singleton().configuration();
//		TiXmlHandle root = config.GetElement( "HTTPClientApp" );
//		Application::singleton().configuration()				// Load the configuration
//		                        .Load(	appConfigName.c_str()	// Name of the file (by default <appname>.cfg)
//										, appName.c_str());		// Root Node Element Name (just always use the AppName



