///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#if 0

/* Misc */
#ifndef __cplusplus
#define bool BOOL
#define true TRUE
#define false FALSE
#endif

#ifndef __cplusplus
#include "debug.h"
typedef void *LOGGER;
#define LOG4CPLUS_ERROR(logger, string) purple_debug_error( "kaneva", "%s\n", string )
#define LOG4CPLUS_WARN(logger, string)	purple_debug_warning( "kaneva", "%s\n", string )
#define LOG4CPLUS_INFO(logger, string)	purple_debug_info( "kaneva", "%s\n", string )
#define LOG4CPLUS_DEBUG
#define LOG4CPLUS_TRACE
#endif

/* String Helper */
#ifdef __cplusplus
#include <string>
#include <sstream>
using namespace std;
#endif

#ifndef _MSC_VER
#define _strtoui64 strtoull
#define sprintf_s _snprintf
#define _itoa_s(i,a,c,r) _itoa(i,a,r)
#endif

#ifdef __cplusplus

#define STRING string
#define STREMPTY ""
#define STRCOPY(STR1, STR2) (STR1 = STR2)
#define STRCONCAT(STR1, STR2) (STR1 += (STR2))
#define STRFREE(STR) ((void)0)
#define GETCSTR(STR) ((STR).c_str())
#define STRISEMPTY(STR) ((STR).empty())
#define STRLEN(STR) ((STR).size())

#else

#include <gtk/gtk.h>
typedef GString *STRING;
#define STREMPTY NULL
#define STRCOPY(STR1, STR2) \
	if(STR1!=NULL) g_string_free(STR1, TRUE); \
	(STR1 = g_string_new(STR2))
#define STRCONCAT(STR1, STR2) (STR1 = g_string_append((STR1), (STR2)))
#define STRFREE(STR) if(STR!=NULL) g_string_free((STR), TRUE)
#define GETCSTR(STR) ((STR)->str)
#define STRISEMPTY(STR) (!GETCSTR(STR) || *GETCSTR(STR)==0)
#define STRLEN(STR) (GETCSTR(STR)? strlen(GETCSTR(STR)) : 0)

#endif

/* Get Browser Page */
#ifdef __cplusplus
#ifdef KANEVA_LAUNCHER
#include "Common\KEPUtil\WebCall.h"
#define GETUTMGIF( url, resultText, resultSize, httpStatusCode, httpStatusDesc ) \
	GetBrowserPage( url, resultText, resultSize, httpStatusCode, httpStatusDesc)
#else
#include "GetBrowserPage.h"
#define GETUTMGIF( url, resultText, resultSize, httpStatusCode, httpStatusDesc ) \
	GetBrowserPage( url, resultText, resultSize, httpStatusCode, httpStatusDesc)
#endif
#else
bool GetBrowserPage(const string& url, STRING *resultText, DWORD *httpStatusCode, STRING *httpStatusDesc);
#define GETUTMGIF( url, resultText, resultSize, httpStatusCode, httpStatusDesc ) \
	GetBrowserPage( url, &resultText, &httpStatusCode, &httpStatusDesc)
#endif
#endif