///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <windows.h>

#include "common\include\kepcommon.h"

#include <vector>
#include <string>

#include "common/KEPUtil/WebCall.h"

typedef std::vector<std::pair<std::string, std::string> > PostValuesList;

/**
 *
 * @param url					URL to GET
 * @param fname					Name of file into which the response content will be 
 *								written.  Use to download files.
 * @param logFile				Logger to be used by GetBrowserPage
 * @param httpStatusCode		Reference to int which will contain the http 
 *								response code upon return.
 * @param httpStatusDescription	Reference to string which will corresponding message
 *								for httpResponseCode
 * @returns 0 on sucess, not zero on fail
 */

#define GBP_TIMEOUT WEB_CALL_OPT_TIMEOUT_MS

bool PostBrowserPageToFile(const std::string& url, const std::string& fname, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs = GBP_TIMEOUT);
bool PostBrowserPage(const std::string& url, std::string& resultText, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs = GBP_TIMEOUT);
bool PostBrowserPage(const std::string& url, std::string& resultText, DWORD& httpStatusCode, std::string& httpStatusDesc, TimeMs timeoutMs, const PostValuesList & valuesToPost);
