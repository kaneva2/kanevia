///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#include <windows.h>
#include <stdio.h>
#include "NTService.h"
#include <string>
#include <vector>
#include "Core/Util/Unicode.h" // Included using a relative path until this file is moved to the KEPUtil directory.

// static variables
CNTService* CNTService::m_pThis = NULL;

CNTService::CNTService(const char* szServiceName, const char *szServiceDesc ) 
{
    // copy the address of the current object so we can access it from
    // the static member callback functions. 
    // WARNING: This limits the application to only one CNTService object. 
    m_pThis = this;
    
    // Set the default service name and version
	m_strServiceName = szServiceName;
	m_strServiceDesc = szServiceDesc;
    m_iMajorVersion = 1;
    m_iMinorVersion = 0;
    m_hEventSource = NULL;

    // set up the initial service status 
    m_hServiceStatus = NULL;
    m_Status.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    m_Status.dwCurrentState = SERVICE_STOPPED;
    m_Status.dwControlsAccepted = SERVICE_ACCEPT_STOP;
    m_Status.dwWin32ExitCode = 0;
    m_Status.dwServiceSpecificExitCode = 0;
    m_Status.dwCheckPoint = 0;
    m_Status.dwWaitHint = 0;
    m_bIsRunning = false;

	// install option
	m_startType = SERVICE_DEMAND_START; 
}

CNTService::~CNTService()
{
    DebugMsg(L"CNTService::~CNTService()");
    if (m_hEventSource) {
        ::DeregisterEventSource(m_hEventSource);
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// Default command line argument parsing

// Returns TRUE if it found an arg it recognised, FALSE if not
// Note: processing some arguments causes output to stdout to be generated.
BOOL CNTService::ParseStandardArgs(int argc, char* argv[])
{
    // See if we have any command line args we recognise
    if (argc <= 1) 
		return FALSE;

	/* parms are:
	-v = get version info, stops processing when hits this
	-i [name [desc [dependencies] ] ] = install
	-u [name] = uninstall
	-p [parameter string]... = adds this strings up to next param, to the end of service name
	         must preceed -i (up to 1K)
	*/

	for ( int i = 1; i < argc; i++ )
	{
	    if (_stricmp(argv[i], "-v") == 0) 
		{
	        // Spit out version info
	        wprintf(L"%s Version %d.%d\n",
	               KEP::Utf8ToUtf16(m_strServiceName).c_str(), m_iMajorVersion, m_iMinorVersion);
	        wprintf(L"The service is %s installed\n",
	               IsInstalled() ? L"currently" : L"not");
	        return TRUE; // say we processed the argument

	    } 
		else if  (_stricmp(argv[i], "-p") == 0) 
		{
			while ( (i+1) < argc && argv[i+1][0] != '-' )
			{
				m_strParams += " ";
				i++;
				bool addQuotes = strchr( argv[i], ' ' ) != 0;
				if ( addQuotes )
					m_strParams += "\"";
				m_strParams += argv[i];
				if ( addQuotes )
					m_strParams += "\"";
			}
		}
		else if (_stricmp(argv[i], "-i") == 0  || _stricmp(argv[i], "-iu") == 0 || 
		         _stricmp(argv[i], "-a") == 0  || _stricmp(argv[i], "-au") == 0 )
		{
			bool updateIfInstalled = _stricmp(argv[i], "-iu") == 0 || _stricmp(argv[i], "-au") == 0;
			bool alreadyInstalled = false;

			if ( argv[i][1] == 'a' )
				m_startType =  SERVICE_AUTO_START;  // automatically start				
			
			if ( (i+1) < argc && argv[i+1][0] != '-' )
				m_strServiceName = argv[++i];

			// Request to install.
	        if (IsInstalled()) 
			{
				alreadyInstalled = true;
				if ( !updateIfInstalled )
				{
		            wprintf(L"%s is already installed\n", KEP::Utf8ToUtf16(m_strServiceName).c_str());
		            m_Status.dwWin32ExitCode = ERROR_SERVICE_EXISTS;
		        }
	        } 
	        
			if ( !alreadyInstalled || updateIfInstalled )
	        {
				// 6/05 allow override of name + desc
				if ( (i+1) < argc && argv[i+1][0] != '-' )
				{
					m_strServiceDesc = argv[++i];
				}
				
				if ( (i+1) < argc && argv[i+1][0] != '-' )
				{
					// 7/05 allow dependency override (Q&D)
					m_dependencies = argv[++i];
				}

	            // Try and install the copy that's running
	            if (Install(m_strParams.c_str(), alreadyInstalled)) 
				{
	                wprintf(L"%s installed\n", KEP::Utf8ToUtf16(m_strServiceName).c_str());
	            } 
				else 
	            {
	                wprintf(L"%s failed to install. Error %d\n", KEP::Utf8ToUtf16(m_strServiceName).c_str(), GetLastError());
	            }
	        }
	        return TRUE; // say we processed the argument

	    } 
		else if (_stricmp(argv[i], "-u") == 0 ) 
		{
			if ( (i+1) < argc && argv[i+1][0] != '-' )
				m_strServiceName = argv[++i];

	        // Request to uninstall.
	        if (!IsInstalled()) 
			{
	            wprintf(L"%s is not installed\n", KEP::Utf8ToUtf16(m_strServiceName).c_str());
	            m_Status.dwWin32ExitCode = ERROR_SERVICE_NOT_FOUND;
	        } 
			else 
	        {
	            // Try and remove the copy that's installed
	            if (Uninstall()) 
				{
	                // Get the executable file path
	                wchar_t wszFilePath[_MAX_PATH];
	                ::GetModuleFileNameW(NULL, wszFilePath, _countof(wszFilePath));
	                wprintf(L"%s removed. (You must delete the file (%s) yourself.)\n",
	                       KEP::Utf8ToUtf16(m_strServiceName).c_str(), wszFilePath);
	            } 
				else 
				{
	                wprintf(L"Could not remove %s. Error %d\n", KEP::Utf8ToUtf16(m_strServiceName).c_str(), GetLastError());
	            }
	        }
	        return TRUE; // say we processed the argument
	    
	    }
		else if (_stricmp(argv[i], "-udep") == 0 )  // Southern version of uninstall
		{
			if ( (i+1) < argc && argv[i+1][0] != '-' )
				m_strServiceName = argv[++i];

	        // Request to uninstall.
	        if (!IsInstalled()) 
			{
	            wprintf(L"%s is not installed\n", KEP::Utf8ToUtf16(m_strServiceName).c_str());
	            m_Status.dwWin32ExitCode = ERROR_SERVICE_NOT_FOUND;
	        } 
			else 
	        {
	            // Try and remove the copy that's installed
	            if (UninstallWithDependents()) 
				{
	                // Get the executable file path
	                wchar_t wszFilePath[_MAX_PATH];
	                ::GetModuleFileNameW(NULL, wszFilePath, _countof(wszFilePath));
	                wprintf(L"%s and all its dependents were removed. (You must delete the files yourself.)\n",
	                       KEP::Utf8ToUtf16(m_strServiceName).c_str());
	            } 
				else 
				{
	                wprintf(L"Could not remove %s. Error %d\n", KEP::Utf8ToUtf16(m_strServiceName).c_str(), GetLastError());
	            }
	        }
	        return TRUE; // say we processed the argument
	    
	    }
	}
	
    // Don't recognise the args
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Install/uninstall routines

// Test if the service is currently installed
BOOL CNTService::IsInstalled()
{
    BOOL bResult = FALSE;

    // Open the Service Control Manager
    SC_HANDLE hSCM = ::OpenSCManager(NULL, // local machine
                                     NULL, // ServicesActive database
                                     SC_MANAGER_ALL_ACCESS); // full access
    if (hSCM) {

        // Try to open the service
        SC_HANDLE hService = ::OpenService(hSCM,
                                           KEP::Utf8ToUtf16(m_strServiceName).c_str(),
                                           SERVICE_QUERY_CONFIG);
        if (hService) {
            bResult = TRUE;
            ::CloseServiceHandle(hService);
        }

        ::CloseServiceHandle(hSCM);
    }
    
    return bResult;
}

BOOL CNTService::Install( const char* extraParams, bool updateExisting )
{
	// pass in extraParams with leading space
    // Open the Service Control Manager
    SC_HANDLE hSCM = ::OpenSCManager(NULL, // local machine
                                     NULL, // ServicesActive database
                                     SC_MANAGER_ALL_ACCESS); // full access
    if (!hSCM) return FALSE;

    // Get the executable file path
    wchar_t wszFilePath[_MAX_PATH+PARM_MAX];
    ::GetModuleFileNameW(NULL, wszFilePath, _countof(wszFilePath));

    // make registry entries to support logging messages
    // Add the source name as a subkey under the Application
    // key in the EventLog service portion of the registry.
    std::wstring wstrKey = L"SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\"
		+ KEP::Utf8ToUtf16(m_strServiceName);
    HKEY hKey = NULL;
    if (::RegCreateKeyW(HKEY_LOCAL_MACHINE, wstrKey.c_str(), &hKey) != ERROR_SUCCESS) {
        return FALSE;
    }

    // Add the Event ID message-file name to the 'EventMessageFile' subkey.
    ::RegSetValueExW(hKey,
                    L"EventMessageFile",
                    0,
                    REG_EXPAND_SZ, 
                    (CONST BYTE*)wszFilePath,
                    (wcslen(wszFilePath) + 1) * sizeof(wszFilePath[0]) );     

    // Set the supported types flags.
    DWORD dwData = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE;
    ::RegSetValueEx(hKey,
                    L"TypesSupported",
                    0,
                    REG_DWORD,
                    (CONST BYTE*)&dwData,
                     sizeof(DWORD));
    ::RegCloseKey(hKey);


	if ( extraParams )
		wcscat_s( wszFilePath, _countof(wszFilePath), KEP::Utf8ToUtf16(extraParams).c_str() );

	SC_HANDLE hService = 0;
	if ( updateExisting )
	{
        hService = ::OpenService(hSCM,
                                   KEP::Utf8ToUtf16(m_strServiceName).c_str(),
                                   SERVICE_CHANGE_CONFIG );
		if ( hService )
		{
			if ( !ChangeServiceConfig( hService, 
									  	SERVICE_WIN32_OWN_PROCESS,
									  	m_startType,
									  	SERVICE_ERROR_NORMAL,
									  	wszFilePath,
                                        NULL,	// lpLoadOrderGroup 
                                        NULL,	// lpdwTagId 
									  	KEP::Utf8ToUtf16(m_dependencies).c_str(),
									  	NULL, // lpServiceStartName,
									  	NULL, // lpPassword,
									  	NULL // lpDisplayName -- not changing the name
									) )
			{
				m_Status.dwWin32ExitCode = GetLastError();
			    ::CloseServiceHandle(hService);
				::CloseServiceHandle(hSCM);
				return FALSE;
			}
		}
	}
	else
	{
    	// Create the service
    	hService = ::CreateService(hSCM,
                                         KEP::Utf8ToUtf16(m_strServiceName).c_str(),
                                         KEP::Utf8ToUtf16(m_strServiceName).c_str(),
                                         SERVICE_ALL_ACCESS,
                                         SERVICE_WIN32_OWN_PROCESS,
                                         m_startType,        // start condition
                                         SERVICE_ERROR_NORMAL,
                                         wszFilePath,
                                         NULL,	// lpLoadOrderGroup 
                                         NULL,	// lpdwTagId 
                                         KEP::Utf8ToUtf16(m_dependencies).c_str(),  // lpDependencies 
                                         NULL,	// lpServiceStartName 
                                         NULL); // lpPassword 
	}
	
    if (!hService) {
		m_Status.dwWin32ExitCode = GetLastError();
        ::CloseServiceHandle(hSCM);
        return FALSE;
    }

	// set the description
	std::wstring wstrServiceDesc = KEP::Utf8ToUtf16(m_strServiceDesc);
	SERVICE_DESCRIPTION sd;
	ZeroMemory( &sd, sizeof( sd ) );
	sd.lpDescription = const_cast<wchar_t*>(wstrServiceDesc.c_str()); // Remove const for non-modifying API call.
	ChangeServiceConfig2( hService, SERVICE_CONFIG_DESCRIPTION, &sd );

	// set the failure actions
	SERVICE_FAILURE_ACTIONS sfa;
	ZeroMemory( &sfa, sizeof( sfa ) );
	sfa.dwResetPeriod = 86400; // seconds in one day
	sfa.lpRebootMsg = NULL;
	sfa.lpCommand = NULL;

	SC_ACTION sa[3]; // specify 3 recover actions
	sfa.cActions = 3;
	sa[0].Delay = 60000;
	sa[0].Type = SC_ACTION_RESTART;
	sa[1].Delay = 60000;
	sa[1].Type = SC_ACTION_RESTART;
	sa[2].Delay = 60000;
	sa[2].Type = SC_ACTION_NONE;

	sfa.lpsaActions = sa;
	ChangeServiceConfig2( hService, SERVICE_CONFIG_FAILURE_ACTIONS, (void *)&sfa );

    // tidy up
    ::CloseServiceHandle(hService);
    ::CloseServiceHandle(hSCM);

	LogEvent(EVENTLOG_INFORMATION_TYPE, EVMSG_INSTALLED, m_strServiceName.c_str());

    return TRUE;
}

BOOL CNTService::Uninstall( SC_HANDLE hSCM, const char* serviceName, bool exeMustMatch )
{
    if (!hSCM) return FALSE;

	std::wstring wstrServiceName = KEP::Utf8ToUtf16(serviceName);
    BOOL bResult = FALSE;
    SC_HANDLE hService = ::OpenServiceW(hSCM,
                                       wstrServiceName.c_str(),
                                       DELETE | SERVICE_QUERY_CONFIG );
    if (hService) 
    {
    	if ( exeMustMatch )
    	{
    		// make sure 
    		LPQUERY_SERVICE_CONFIG scCfg = 0;
    		DWORD bytesNeeded;
    		::QueryServiceConfig( hService, scCfg, 0, &bytesNeeded );
    		if ( GetLastError() == ERROR_INSUFFICIENT_BUFFER )
    		{
    			scCfg = (LPQUERY_SERVICE_CONFIG)malloc(bytesNeeded);
	    		if ( ::QueryServiceConfig( hService, scCfg, bytesNeeded, &bytesNeeded ) )
	    		{
	                wchar_t wszFilePath[_MAX_PATH];
	                ::GetModuleFileName(NULL, wszFilePath, _countof(wszFilePath));
	                wchar_t wfname[_MAX_PATH];
	                wchar_t wextname[_MAX_PATH];
	                _wsplitpath_s( wszFilePath, NULL, 0, NULL, 0, wfname, _countof(wfname), wextname, _countof(wextname) );
	                wcscat_s( wfname, wextname );
	    			if ( wcsstr( scCfg->lpBinaryPathName, wfname ) != 0 )
	    			{
	    				bResult = TRUE;
	    			}
    			}
    			free( (void*)scCfg );
    		}
    	}
    	else
    		bResult = TRUE;

    	if ( bResult )
    	{
	        if (::DeleteService(hService)) 
	        {
	            LogEvent(EVENTLOG_INFORMATION_TYPE, EVMSG_REMOVED, serviceName);
	            bResult = TRUE;
	        } 
	        else 
	        {
		        m_Status.dwWin32ExitCode = GetLastError();
	            LogEvent(EVENTLOG_ERROR_TYPE, EVMSG_NOTREMOVED, serviceName);
	        }
        }
        ::CloseServiceHandle(hService);
    }

	return bResult;    
}

BOOL CNTService::Uninstall()
{
    // Open the Service Control Manager
    SC_HANDLE hSCM = ::OpenSCManager(NULL, // local machine
                                     NULL, // ServicesActive database
                                     SC_MANAGER_ALL_ACCESS); // full access

	BOOL bResult = Uninstall( hSCM, m_strServiceName.c_str() );
	
    ::CloseServiceHandle(hSCM);
    
    return bResult;
}

BOOL CNTService::UninstallWithDependents()
{
    SC_HANDLE hSCM = ::OpenSCManager(NULL, // local machine
                                     NULL, // ServicesActive database
                                     SC_MANAGER_ALL_ACCESS); // full access
    if (!hSCM) 
    {
		m_Status.dwWin32ExitCode = GetLastError();
    	return FALSE;
    }

    bool bResult = FALSE;

	SC_HANDLE hService = ::OpenService( hSCM, KEP::Utf8ToUtf16(m_strServiceName).c_str(), SERVICE_ENUMERATE_DEPENDENTS | DELETE | SERVICE_QUERY_CONFIG );

	if ( hService ) 
	{
		DWORD bytesNeeded = 0;
		DWORD serviceCount = 0;
		LPENUM_SERVICE_STATUS lpServices = 0;

		// first get size we need to allocate
		EnumDependentServices( hService, SERVICE_STATE_ALL, lpServices, 0, &bytesNeeded, &serviceCount );
		
		m_Status.dwWin32ExitCode = GetLastError();
		if ( m_Status.dwWin32ExitCode == ERROR_MORE_DATA )
		{
			lpServices = (LPENUM_SERVICE_STATUS)malloc( bytesNeeded );
			if ( EnumDependentServices( hService, SERVICE_STATE_ALL, lpServices, bytesNeeded, &bytesNeeded, &serviceCount ) )
			{
				for ( DWORD i = 0; i < serviceCount; i++ )
				{
					// we'll skip checking if dependent uninstall failed
					Uninstall( hSCM, KEP::Utf16ToUtf8(lpServices[i].lpServiceName).c_str(), false );
				}
			}
			else
			{
				m_Status.dwWin32ExitCode = GetLastError();
		    	bResult = FALSE;
			}
			free( (void*)lpServices );
		}
		else
		{
	    	bResult = FALSE;
		}

		
	    ::CloseServiceHandle(hService);
	    
		Uninstall( hSCM, m_strServiceName.c_str() );
	}
	else
	{
		m_Status.dwWin32ExitCode = GetLastError();
	}
	
    ::CloseServiceHandle(hSCM);

	return bResult;		

}

///////////////////////////////////////////////////////////////////////////////////////
// Logging functions

// This function makes an entry into the application event log
void CNTService::LogEvent(WORD wType, DWORD dwID,
                          const char* pszS1,
                          const char* pszS2,
                          const char* pszS3)
{
	std::wstring astr[3];
	const char* apsz[3] = { pszS1, pszS2, pszS3 };
    const wchar_t* ps[3];
	for( size_t i = 0; i < 3; ++i ) {
		if( apsz[i] ) {
			astr[i] = KEP::Utf8ToUtf16(apsz[i]);
			ps[i] = astr[i].c_str();
		} else {
			ps[i] = nullptr;
		}
	}

    int iStr = 0;
    for (int i = 0; i < 3; i++) {
        if (ps[i] != NULL) iStr++;
    }
        
    // Check the event source has been registered and if
    // not then register it now
    if (!m_hEventSource) {
        m_hEventSource = ::RegisterEventSource(NULL,  // local machine
                                               KEP::Utf8ToUtf16(m_strServiceName).c_str()); // source name
    }

    if (m_hEventSource) {
        ::ReportEvent(m_hEventSource,
                      wType,
                      0,
                      dwID,
                      NULL, // sid
                      (WORD) iStr,
                      0,
                      ps,
                      NULL);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Service startup and registration

BOOL CNTService::StartService()
{
	std::wstring wstrServiceName = KEP::Utf8ToUtf16(m_strServiceName);
    SERVICE_TABLE_ENTRY st[] = {
        {const_cast<wchar_t*>(wstrServiceName.c_str()), ServiceMain}, // Cast for non-modifying API call.
        {NULL, NULL}
    };

    DebugMsg(L"Calling StartServiceCtrlDispatcher()");
    BOOL b = ::StartServiceCtrlDispatcher(st);
    DebugMsg(L"Returned from StartServiceCtrlDispatcher()");
    return b;
}

// static member function (callback)
void CNTService::ServiceMain(DWORD dwArgc, wchar_t** lpszArgv)
{
    // Get a pointer to the C++ object
    CNTService* pService = m_pThis;
    
    pService->DebugMsg(L"Entering CNTService::ServiceMain()");
    // Register the control request handler
    pService->m_Status.dwCurrentState = SERVICE_START_PENDING;
    pService->m_hServiceStatus = RegisterServiceCtrlHandlerW(KEP::Utf8ToUtf16(pService->m_strServiceName).c_str(),
                                                           Handler);
    if (pService->m_hServiceStatus == NULL) {
        pService->LogEvent(EVENTLOG_ERROR_TYPE, EVMSG_CTRLHANDLERNOTINSTALLED);
        return;
    }

	KEP::CommandLineUtf8 cl(dwArgc, lpszArgv);


    // Start the initialisation
    if (pService->Initialize(cl.GetArgC(), cl.GetArgV())) {

        // Do the real work. 
        // When the Run function returns, the service has stopped.
        pService->m_bIsRunning = true;
        pService->m_Status.dwWin32ExitCode = 0;
        pService->m_Status.dwCheckPoint = 0;
        pService->m_Status.dwWaitHint = 0;
        pService->Run();
    }

    // Tell the service manager we are stopped
    pService->SetCurrentState(SERVICE_STOPPED);

    pService->DebugMsg(L"Leaving CNTService::ServiceMain()");
}

///////////////////////////////////////////////////////////////////////////////////////////
// status functions

void CNTService::SetCurrentState(DWORD dwState)
{
    DebugMsg(L"CNTService::SetCurrentState(%lu, %lu)", m_hServiceStatus, dwState);
	m_Status.dwCurrentState = dwState;
	if ( m_hServiceStatus != 0 ) // only call if running as service
		::SetServiceStatus(m_hServiceStatus, &m_Status);
}

///////////////////////////////////////////////////////////////////////////////////////////
// Service initialization

BOOL CNTService::Initialize(DWORD dwArgc, char** lpszArgv)
{
    DebugMsg(L"Entering CNTService::Initialize()");

    // Start the initialization
    SetCurrentState(SERVICE_START_PENDING);
    
    // Perform the actual initialization
    BOOL bResult = OnInit(dwArgc, lpszArgv); 
    
    // Set final state
    m_Status.dwWin32ExitCode = GetLastError();
    m_Status.dwCheckPoint = 0;
    m_Status.dwWaitHint = 0;
    if (!bResult) {
        LogEvent(EVENTLOG_ERROR_TYPE, EVMSG_FAILEDINIT);
        SetCurrentState(SERVICE_STOPPED);
        return FALSE;    
    }
    
    LogEvent(EVENTLOG_INFORMATION_TYPE, EVMSG_STARTED);
    SetCurrentState(SERVICE_RUNNING);

    DebugMsg(L"Leaving CNTService::Initialize()");
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// main function to do the real work of the service

// This function performs the main work of the service. 
// When this function returns the service has stopped.
void CNTService::Run()
{
    DebugMsg(L"Entering CNTService::Run()");

    while (m_bIsRunning) {
        DebugMsg(L"Sleeping...");
        Sleep(5000);
    }

    // nothing more to do
    DebugMsg(L"Leaving CNTService::Run()");
}

//////////////////////////////////////////////////////////////////////////////////////
// Control request handlers

// static member function (callback) to handle commands from the
// service control manager
void CNTService::Handler(DWORD dwOpcode)
{
    // Get a pointer to the object
    CNTService* pService = m_pThis;
    
    pService->DebugMsg(L"CNTService::Handler(%lu)", dwOpcode);
    switch (dwOpcode) {
    case SERVICE_CONTROL_STOP: // 1
        pService->SetCurrentState(SERVICE_STOP_PENDING);
        pService->OnStop();
        pService->m_bIsRunning = false;
        pService->LogEvent(EVENTLOG_INFORMATION_TYPE, EVMSG_STOPPED);
        break;

    case SERVICE_CONTROL_PAUSE: // 2
        pService->OnPause();
        break;

    case SERVICE_CONTROL_CONTINUE: // 3
        pService->OnContinue();
        break;

    case SERVICE_CONTROL_INTERROGATE: // 4
        pService->OnInterrogate();
        break;

    case SERVICE_CONTROL_SHUTDOWN: // 5
        pService->OnShutdown();
        break;

    default:
        if (dwOpcode >= SERVICE_CONTROL_USER) {
            if (!pService->OnUserControl(dwOpcode)) {
                pService->LogEvent(EVENTLOG_ERROR_TYPE, EVMSG_BADREQUEST);
            }
        } else {
            pService->LogEvent(EVENTLOG_ERROR_TYPE, EVMSG_BADREQUEST);
        }
        break;
    }

    // Report current status
    pService->DebugMsg(L"Updating status (%lu, %lu)",
                       pService->m_hServiceStatus,
                       pService->m_Status.dwCurrentState);
    ::SetServiceStatus(pService->m_hServiceStatus, &pService->m_Status);
}
        
// Called when the service is first initialized
BOOL CNTService::OnInit(DWORD /*dwArgc*/, char** /*lpszArgv*/)
{
    DebugMsg(L"CNTService::OnInit()");
	return TRUE;
}

// Called when the service control manager wants to stop the service
void CNTService::OnStop()
{
    DebugMsg(L"CNTService::OnStop()");
}

// called when the service is interrogated
void CNTService::OnInterrogate()
{
    DebugMsg(L"CNTService::OnInterrogate()");
}

// called when the service is paused
void CNTService::OnPause()
{
    DebugMsg(L"CNTService::OnPause()");
}

// called when the service is continued
void CNTService::OnContinue()
{
    DebugMsg(L"CNTService::OnContinue()");
}

// called when the service is shut down
void CNTService::OnShutdown()
{
    DebugMsg(L"CNTService::OnShutdown()");
}

// called when the service gets a user control message
BOOL CNTService::OnUserControl(DWORD dwOpcode)
{
    DebugMsg(L"CNTService::OnUserControl(%8.8lXH)", dwOpcode);
    return FALSE; // say not handled
}

const char* CNTService::ServiceName() const
{
	return m_strServiceName.c_str();
}

////////////////////////////////////////////////////////////////////////////////////////////
// Debugging support

void CNTService::DebugMsg(const wchar_t* pwszFormat, ...)
{
    wchar_t buf[1024];
    swprintf_s(buf, _countof(buf), L"[%s](%lu): ", KEP::Utf8ToUtf16(m_strServiceName).c_str(), GetCurrentThreadId());
	va_list arglist;
	va_start(arglist, pwszFormat);
    vswprintf_s(&buf[wcslen(buf)], _countof(buf)-wcslen(buf), pwszFormat, arglist);
	va_end(arglist);
    wcscat_s(buf, _countof(buf), L"\n");
    OutputDebugStringW(buf);
}

