///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <windows.h>
#include "ntservmsg.h" // Event message ids
#include <string>

#define SERVICE_CONTROL_USER 128

class CNTService
{
public:
    CNTService(const char* szServiceName, const char* szServiceDesc = "");
    virtual ~CNTService();
    BOOL ParseStandardArgs(int argc, char* argv[]);
    BOOL IsInstalled();
    BOOL Install(const char* extraParams = 0, bool updateExisting = false);
    BOOL Uninstall();
	BOOL UninstallWithDependents();
    void LogEvent(WORD wType, DWORD dwID,
                  const char* pszS1 = NULL,
                  const char* pszS2 = NULL,
                  const char* pszS3 = NULL);
    BOOL StartService();
    BOOL Initialize(DWORD dwArgc, char** lpszArgv);
    virtual void Run();
	virtual BOOL OnInit(DWORD dwArgc, char** lpszArgv);
    virtual void OnStop();
    virtual void OnInterrogate();
    virtual void OnPause();
    virtual void OnContinue();
    virtual void OnShutdown();
    virtual BOOL OnUserControl(DWORD dwOpcode);
    void DebugMsg(const wchar_t* pszFormat, ...);
	BOOL Uninstall( SC_HANDLE hSCM, const char* serviceName, bool exeMustMatch = true );

	const char* ServiceName() const;
	bool IsRunning() const { return m_bIsRunning; }
	void SetIsRunning(bool b) { m_bIsRunning = b; }
	const SERVICE_STATUS& GetServiceStatus() const { return m_Status; }

protected:
    void SetCurrentState(DWORD dwState);
	void AddControlsAccepted(DWORD dwControlsAccepted)
	{
		m_Status.dwControlsAccepted |= dwControlsAccepted;
	}

	const SERVICE_STATUS_HANDLE& GetServiceStatusHandle() const { return m_hServiceStatus; }
	void UpdateStatus(ULONG newState, ULONG waitHint = 500)
	{
		m_Status.dwCheckPoint++;
		m_Status.dwWaitHint = waitHint;
		SetCurrentState(newState);  // NTService didn't have setting of checkpoints
	}

private:
    // static member functions
    static void WINAPI ServiceMain(DWORD dwArgc, wchar_t** lpszArgv);
    static void WINAPI Handler(DWORD dwOpcode);

private:
    // data members
    #define PARM_MAX 1024
	std::string m_strServiceName;
	std::string m_strServiceDesc;
	std::string m_strParams;
    int m_iMajorVersion;
    int m_iMinorVersion;
    SERVICE_STATUS_HANDLE m_hServiceStatus;
    SERVICE_STATUS m_Status;
    bool m_bIsRunning;
	
	// install options
	DWORD 		m_startType;
	std::string	m_dependencies;

    // static data
    static CNTService* m_pThis; // nasty hack to get object ptr

private:
    HANDLE m_hEventSource;

};
