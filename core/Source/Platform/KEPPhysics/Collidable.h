///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "PhysicsBase.h"
#include "Core/Math/Matrix.h"
#include "Core/Util/Parameter.h"

namespace KEP {

class IMemSizeGadget;

namespace Physics {

struct CollidableProperties {
	CollidableProperties() :
			m_CollisionGroupMask(0xffff), m_CollidesWithMask(0xffff), m_fSlidingFriction(1), m_fRollingFriction(0), m_fRestitution(1) {}

	unsigned short m_CollisionGroupMask;
	unsigned short m_CollidesWithMask;

	// Friction coefficients. 0: no friction, 1: sticky, >1: unusually sticky and uncommon but possible
	float m_fSlidingFriction;
	float m_fRollingFriction;

	float m_fRestitution; // 0: inelastic collision, 1: elastic collision
};

class Collidable : public virtual PhysicsBase {
public:
	Collidable(PhysicsFactory& factory) :
			PhysicsBase(factory) {}

	// If we want to allow changing collision groups dynamically.
	//virtual void SetCollisionGroups(unsigned int mask) = 0;
	//virtual void SetCollidesWithGroups(unsigned int mask) = 0;

	virtual void SetCollisionMasks(unsigned int groupMask, unsigned int collidesWithMask) = 0;
	virtual void GetCollisionMasks(Out<unsigned int> groupMask, Out<unsigned int> collidesWithMask) const = 0;
	virtual void SetIsCollidable(bool bCollidable) = 0;
	virtual Matrix44f GetTransformMetric() const = 0;
	virtual bool SetTransformMetric(const Matrix44f& transform, bool bIgnoreScale = false) = 0;
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;
};

} // namespace Physics
} // namespace KEP
