///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <memory>

namespace KEP {
namespace Physics {

// Making all user data derive from a single object simplifies usage and gives us
// more useful debug information.
struct UserData {
	virtual ~UserData() {}
};

} // namespace Physics
} // namespace KEP
