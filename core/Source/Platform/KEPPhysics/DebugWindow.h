///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "PhysicsBase.h"
#include "Declarations.h"
#include "Core/Math/Matrix.h"

namespace KEP {
namespace Physics {

struct DebugWindowOptions {
	DebugWindowOptions() :
			m_hWndParent(nullptr) {}

	HWND m_hWndParent;
};

struct CameraProperties {
	CameraProperties() :
			m_ptCameraPositionMeters(0, 0, 0), m_vViewDirection(0, 0, 1), m_vViewUp(0, 1, 0), m_fRelativeViewWidth(.5f), m_fRelativeViewHeight(.5f), m_fNear(.125f), m_fFar(4096) {}
	Vector3f m_ptCameraPositionMeters;
	Vector3f m_vViewDirection;
	Vector3f m_vViewUp;

	// Ratio of view width/height to the distance from the camera.
	float m_fRelativeViewWidth;
	float m_fRelativeViewHeight;
	float m_fNear;
	float m_fFar;
};

class DebugWindow : public virtual PhysicsBase {
public:
#pragma warning(push)
#pragma warning(disable : 4589) // Spurious warning due to compiler bug. Due to be fixed in VS2015 update 1.
	DebugWindow(PhysicsFactory& factory) :
			PhysicsBase(factory) {}
#pragma warning(pop)

	virtual bool IsValid() = 0;
	virtual bool Show(bool bShow) = 0;
	virtual void SetTitle(const char* pszTitle) = 0;

	virtual bool RenderBegin() = 0;
	virtual bool RenderEnd() = 0;
	virtual bool Render(Universe* pUniverse) = 0;
	virtual bool Render(Avatar* pAvatar, const Matrix44f& transform) = 0;

	virtual void SetCameraProperties(const CameraProperties& camProps) = 0;
};

} // namespace Physics
} // namespace KEP
