///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "PhysicsBase.h"
#include "Collidable.h"
#include "Declarations.h"
#include "Core/Util/Parameter.h"
#include "Core/Math/Units.h"
#include <boost/optional.hpp>

namespace KEP {
namespace Physics {

enum eVehicleControlType {
	Left,
	Right,
	Forward,
	Reverse,
	Brake,
	TurboBoost,

	Count // Number of control types.
};

struct VehicleControlCommand {
	double m_dTime; // Real time of command
	eVehicleControlType m_Type; // Control being affected
	bool m_bActivate; // Whether control is activated or deactivated.
};

struct VehicleProperties : public CollidableProperties {
	enum class DriveWheels {
		Front,
		Rear,
		All
	};

	VehicleProperties() :
			m_DriveWheels(DriveWheels::All), m_fEngineHorsepower(120.0f), m_fWheelThicknessMeters(0.5f), m_fWheelRadiusMeters(0.5f), m_fMassKG(1500.0f), m_fSuspensionLoadedLength(0.5f), m_fSuspensionUnloadedLength(0.75f), m_fSuspensionDampingRelaxation(0.88f), m_fSuspensionDampingCompression(0.83f), m_fSuspensionAttachmentHeight(0.01f), m_fWheelFriction(2.5f), m_fRollFactor(0.1f), m_vDimensionsM(FeetToMeters(6.0f), FeetToMeters(4.60f), FeetToMeters(16.0f)) // typical sedan size (after subtracting ground clearance: .15)
			,
			m_vRelativeMassCenter(0.0f, -1.0f, 0.0f) // Center of mass at bottom center of vehicle.
			//, m_vRelativeMassCenter(0.0f, 0.0f, 0.0f)
			,
			m_fMaxSpeedKPH(MilesToKM(60.0f)),
			m_fMaxReverseSpeedKPH(MilesToKM(20.0f)),
			m_fAerodynamicDragCoeff(0.30f) // Somewhat sporty car: .3, Very low drag car: .2, Truck: .6
			,
			m_fFrictionDragN(250.0f),
			m_fBrakeDecelKPHPS(49.0f) // Best road cars: 49.0k/h/s, Typical car: 33.3 k/h/s (13.6 and 9.2 meters/s/s)
			,
			m_fTurboUsageFrequency(1.0f) // Delay required between consecutive uses of turbo boost.
			,
			m_vTurboVelocityMPS(9.8f, 9.8f) // 1G upward velocity, so will hit max height at 1 second and be airborne for 2 seconds.
			,
			m_iOtherVehicleCollisionGroups(0) {
		m_fRestitution = 0.5f; // Different default restitution for vehicles.
	}

	float GetRestingGroundClearance() const {
		return m_fSuspensionLoadedLength - m_fSuspensionAttachmentHeight + m_fWheelRadiusMeters;
	}

	DriveWheels m_DriveWheels;
	float m_fWheelThicknessMeters;
	float m_fWheelRadiusMeters;

	float m_fMassKG;
	float m_fSuspensionLoadedLength; // Length of the suspension in meters when the vehicle is resting on level ground.
	float m_fSuspensionUnloadedLength; // Length of the suspension in meters when the wheels are off the ground.
	float m_fSuspensionDampingRelaxation; // Resistance to change when suspension is extending.
	float m_fSuspensionDampingCompression; // Resistance to change when suspension is compressing.
	float m_fSuspensionAttachmentHeight; // Height above bottom of box to attach suspension. Needs to be positive.
	float m_fWheelFriction;
	float m_fRollFactor; // Only this fraction of true body roll is applied to vehicle.

	Vector3f m_vDimensionsM; // X: left/right, Y: top/bottom (excluding wheels), Z: front/back
	Vector3f m_vRelativeMassCenter; // Center of mass of object relative to center of vehicle box. For each coordinate, 0 is the center and -1/+1 are the sides of the vehicle box.

	float m_fMaxSpeedKPH; // Kilometers / hour
	float m_fMaxReverseSpeedKPH; // Kilometers / hour

	float m_fAerodynamicDragCoeff; // Coefficient of aerodynamic drag. Proportional to frontal area and square of velocity.
	float m_fFrictionDragN; // Drag due to moving parts. Constant force resisting movement. Force measure in Newtons.
	float m_fEngineHorsepower;
	float m_fBrakeDecelKPHPS;

	float m_fTurboUsageFrequency; // Seconds.
	Vector2f m_vTurboVelocityMPS; // Meters per second. X coordinate is forward, Y coordinate is up.

	// Group(s) mask for vehicles controlled by other players. Extra distance will be maintained between objects of this type to avoid collision issues.
	unsigned int m_iOtherVehicleCollisionGroups;
};

class Vehicle : public virtual Collidable {
public:
	Vehicle(PhysicsFactory& factory) :
			Collidable(factory) {}

	virtual const VehicleProperties& GetVehicleProperties() = 0;
	virtual void UpdateVehicleProperties(const VehicleProperties& props) = 0; // Vehicle dimension and center of mass are not updated with this call.
	virtual void ApplyControl(const VehicleControlCommand& cmd) = 0;
	virtual void ClearControls() = 0;
	virtual float GetSpeedKPH() = 0;
	virtual float GetSkidFactor() = 0; // 0 for no skid. 1 for max skid.
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;
};

} // namespace Physics
} // namespace KEP
