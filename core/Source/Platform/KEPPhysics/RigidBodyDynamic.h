///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "PhysicsBase.h"
#include "Collidable.h"
#include "Core/Math/Quaternion.h"

namespace KEP {
namespace Physics {

struct RigidBodyDynamicProperties : public CollidableProperties {
	RigidBodyDynamicProperties() :
			m_fMass(0), m_ptMassCenter(0, 0, 0), m_vRotationalInertia(0), m_qRotationalInertiaOrientation(0, 0, 0, 1), m_fFrictionCoefficient(0), m_fRollingFriction(1), m_fRestitution(1) {}

	float m_fMass;
	Vector3f m_ptMassCenter; // Center of mass of object in its local coordinate system.

	Vector3f m_vRotationalInertia; // Magnitude of rotational inertia along X, Y, and Z axes
	Quaternionf m_qRotationalInertiaOrientation; // Rotation of X, Y, and Z axes to get rotational inertia in local coordinates.

	float m_fFrictionCoefficient;
	float m_fRollingFriction;
	float m_fRestitution; // 0: inelastic collision, 1: elastic collision
};

class RigidBodyDynamic : public virtual Collidable {
protected:
	RigidBodyDynamic(PhysicsFactory& factory) :
			Collidable(factory) {}
};

} // namespace Physics
} // namespace KEP
