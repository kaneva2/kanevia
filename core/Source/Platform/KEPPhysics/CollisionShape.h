///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "PhysicsBase.h"
#include "Declarations.h"
#include "Core/Math/Quaternion.h"
#include <vector>

namespace KEP {

class IMemSizeGadget;

namespace Physics {

enum class eCollisionShapeType {
	None,
	Sphere,
	Box,
	Capsule,
	ConvexHull,
	Mesh,
};

struct CollisionShapeSphereProperties {
	float m_fRadiusMeters;
};
struct CollisionShapeBoxProperties {
	Vector3f m_vDimensionsMeters;
};
struct CollisionShapeCapsuleProperties {
	float m_fCylinderHeightMeters = 0;
	float m_fRadiusMeters = 0;
	int m_iAxis = 1;
};
struct CollisionShapeConvexHullProperties {
	std::vector<std::pair<const Vector3f*, size_t>> m_aPointData;
};

// Need to determine whether we need to support 16 or 32 bit indices and individual triangles or connected strips.
// Might need all combinations.
struct CollisionShapeMeshProperties {
	//std::shared_ptr<void> m_pMeshStorage;
	CollisionShapeMeshProperties(const Vector3f* pFirstPoint, size_t nPoints, const unsigned short* pFirstIndex, size_t nIndices, bool bIsTriangleStrip) :
			m_pFirstPoint(pFirstPoint), m_nPoints(nPoints), m_pFirstIndex(pFirstIndex), m_nIndices(nIndices), m_bIsTriangleStrip(bIsTriangleStrip) {}

	const Vector3f* m_pFirstPoint;
	size_t m_nPoints;
	//size_t m_nPointStride;

	const unsigned short* m_pFirstIndex;
	size_t m_nIndices;
	//size_t m_nIndexStride;
	bool m_bIsTriangleStrip;
};

class CollisionShape : public virtual PhysicsBase {
public:
	virtual eCollisionShapeType GetType() = 0;
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;

protected:
	CollisionShape(PhysicsFactory& factory) :
			PhysicsBase(factory) {}
};

} // namespace Physics
} // namespace KEP
