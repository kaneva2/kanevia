///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef KEPPHYSICS_EXPORTS
#define KEPPHYSICS_API __declspec(dllexport)
#else
#define KEPPHYSICS_API __declspec(dllimport)
#endif
