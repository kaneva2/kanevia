///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "UserData.h"
#include "Core/Math/Vector.h"
#include <memory>

namespace KEP {
namespace Physics {

class PhysicsFactory;

// Base class for physics interface objects.
// Enables creation of shared_ptrs from raw pointers to the objects.
// Disables copying.
// Enables verification that objects come from the same physics factory to avoid interoperation problems.
// Holds user-defined data (as a UserData pointer) so users can store information with physics objects,
// or link them back to their owners.
class PhysicsBase : public std::enable_shared_from_this<PhysicsBase> {
public:
	PhysicsBase(PhysicsFactory& factory) :
			m_PhysicsFactory(factory) {}

	bool HasSameFactoryAs(const PhysicsBase& physicsObject) const { return &m_PhysicsFactory == &physicsObject.m_PhysicsFactory; }

	bool UsesFactory(PhysicsFactory& factory) const { return &m_PhysicsFactory == &factory; }

	void SetUserData(const std::shared_ptr<UserData>& pUserData) { m_pUserData = pUserData; }
	const std::shared_ptr<UserData>& GetUserData() { return m_pUserData; }

protected:
	PhysicsBase() = delete;
	virtual ~PhysicsBase() = default;

	// No copying.
	PhysicsBase(const PhysicsBase&) = delete;
	void operator=(const PhysicsBase&) = delete;

private:
	PhysicsFactory& m_PhysicsFactory;
	std::shared_ptr<UserData> m_pUserData;
};

} // namespace Physics
} // namespace KEP
