///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "PhysicsBase.h"
#include "Collidable.h"
#include "Core/Math/Matrix.h"
#include "Core/Math/Units.h"

namespace KEP {

class IMemSizeGadget;

namespace Physics {

struct RigidBodyStaticProperties : public CollidableProperties {
	RigidBodyStaticProperties() {}
};

class RigidBodyStatic : public virtual Collidable {
public:
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const {}

protected:
	RigidBodyStatic(PhysicsFactory& factory) :
			Collidable(factory) {}
};

} // namespace Physics
} // namespace KEP
