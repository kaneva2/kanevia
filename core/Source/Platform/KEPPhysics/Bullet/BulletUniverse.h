///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "../Universe.h"
#include "BulletRigidBody.h"
#include "BulletMathCast.h"
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/mem_fun.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <map>

struct SensorOverlapFilterCallback;
// The pair cache overlapFilterCallback for the world and for the ghost object must return equivalent results.
// If the overlap filter callback for the world does not match the ghost object pair caches, you will get
// inconsistent results. For example, when the ghost object has collision flags set to collide with a kinematic
// object. The default collision configuration for the world overlapFilterCallback will unexpectedly not collide
// with a kinematic object.
//
// #MLB_NOTE SensorOverlapFilterCallback
//
// The necessity for this may be the result of a defect in the Bullet sw or it may be a consequence of
// something wrong that I'm doing. Consequently, this should be reviewed and monitored to make sure
// the result is consistent and correct.
//
struct SensorOverlapFilterCallback : btOverlapFilterCallback {
	// return true when pairs need collision
	virtual bool needBroadphaseCollision(btBroadphaseProxy* proxy0, btBroadphaseProxy* proxy1) const;
};

namespace KEP {

class IMemSizeGadget;

namespace Physics {

class SweepTestArguments {
public:
	// Options for how to calculate collisions.
	struct Options {
		uint16_t m_iCollisionGroupMask = 0xffff;
		uint16_t m_iCollidesWithMask = 0xffff;
		float m_fKeepDistFromObstacles = 0.0f;

		std::function<bool(const btCollisionObject*)> m_Filter;
	};

	// Describes what's sweeping and where.
	struct Sweep {
		Sweep(const btCollisionShape* pShape = nullptr, const btVector3& vShapeOffset = btVector3(0, 0, 0)) :
				m_pShape(pShape), m_vShapeOffset(vShapeOffset) {}
		void ApplyShapeOffset() {
			m_btTransFrom.setOrigin(m_vShapeOffset * m_btTransFrom.getBasis() + m_btTransFrom.getOrigin());
		}
		const btCollisionShape* m_pShape = nullptr;
		btTransform m_btTransFrom = btTransform::getIdentity();
		btTransform m_btTransTo = btTransform::getIdentity();
		btVector3 m_vShapeOffset = btVector3(0, 0, 0);
	};

public:
	SweepTestArguments() {}

	SweepTestArguments(const btCollisionShape* pShape, const Vector3f& vShapeOffset = Vector3f::Zero()) :
			m_Sweep(pShape, ToBullet(vShapeOffset)) {}

	SweepTestArguments(const btCollisionShape* pShape, const btTransform& btTransFrom, const btTransform& btTransTo, const Vector3f& vShapeOffset = Vector3f::Zero()) {
		SetObject(pShape, vShapeOffset);
		EndPoints(btTransFrom, btTransTo);
	}
	SweepTestArguments(const btCollisionShape* pShape, const Matrix44f& mtxFrom, const Matrix44f& mtxTo, const Vector3f& vShapeOffset = Vector3f::Zero()) {
		SetObject(pShape, vShapeOffset);
		EndPoints(mtxFrom, mtxTo);
	}
	SweepTestArguments(const btCollisionShape* pShape, const Vector3f& ptFrom, const Vector3f& ptTo, const Vector3f& vShapeOffset = Vector3f::Zero()) {
		SetObject(pShape, vShapeOffset);
		EndPoints(ptFrom, ptTo);
	}
	SweepTestArguments(const btCollisionShape* pShape, const btMatrix3x3& mtxOrientation, const Vector3f& ptFrom, const Vector3f& ptTo, const Vector3f& vShapeOffset = Vector3f::Zero()) {
		SetObject(pShape, vShapeOffset);
		EndPoints(mtxOrientation, ptFrom, ptTo);
	}

	SweepTestArguments& SetObject(const btCollisionShape* pShape) {
		m_Sweep.m_pShape = pShape;
		return *this;
	}
	SweepTestArguments& SetObject(const btCollisionShape* pShape, const Vector3f& vShapeOffset) {
		m_Sweep.m_pShape = pShape;
		m_Sweep.m_vShapeOffset = ToBullet(vShapeOffset);
		return *this;
	}
	SweepTestArguments& EndPoints(const btTransform& btTransFrom, const btTransform& btTransTo) {
		m_Sweep.m_btTransFrom = btTransFrom;
		m_Sweep.m_btTransTo = btTransTo;
		return *this;
	}
	SweepTestArguments& EndPoints(const Matrix44f& mtxFrom, const Matrix44f& mtxTo) {
		m_Sweep.m_btTransFrom = ToBullet(mtxFrom);
		m_Sweep.m_btTransTo = ToBullet(mtxTo);
		return *this;
	}
	SweepTestArguments& EndPoints(const Vector3f& ptFrom, const Vector3f& ptTo) {
		EndPoints(btMatrix3x3::getIdentity(), ptFrom, ptTo);
		return *this;
	}
	SweepTestArguments& EndPoints(const btMatrix3x3& mtxOrientation, const Vector3f& ptFrom, const Vector3f& ptTo) {
		m_Sweep.m_btTransFrom.setBasis(mtxOrientation);
		m_Sweep.m_btTransTo.setBasis(mtxOrientation);
		m_Sweep.m_btTransFrom.setOrigin(ToBullet(ptFrom));
		m_Sweep.m_btTransTo.setOrigin(ToBullet(ptTo));
		return *this;
	}
	SweepTestArguments& CollisionMasks(uint16_t iCollisionGroupMask, uint16_t iCollidesWithMask) {
		m_Options.m_iCollisionGroupMask = iCollisionGroupMask;
		m_Options.m_iCollidesWithMask = iCollidesWithMask;
		return *this;
	}
	SweepTestArguments& KeepDistFromObstacles(float fDist) {
		m_Options.m_fKeepDistFromObstacles = fDist;
		return *this;
	}
	SweepTestArguments& SetFilter(std::function<bool(const btCollisionObject*)> filter) {
		m_Options.m_Filter = std::move(filter);
		return *this;
	}

	const btCollisionShape* GetShape() const { return m_Sweep.m_pShape; }
	const btTransform& GetFrom() const { return m_Sweep.m_btTransFrom; }
	const btTransform& GetTo() const { return m_Sweep.m_btTransTo; }
	const btVector3& GetShapeOffset() const { return m_Sweep.m_vShapeOffset; }
	uint16_t GetCollisionGroupMask() const { return m_Options.m_iCollisionGroupMask; }
	uint16_t GetCollidesWithMask() const { return m_Options.m_iCollidesWithMask; }
	float GetKeepDistFromObstacles() const { return m_Options.m_fKeepDistFromObstacles; }
	const std::function<bool(const btCollisionObject*)>& GetFilter() const { return m_Options.m_Filter; }
	const Sweep& GetSweep() const { return m_Sweep; }
	const Options& GetOptions() const { return m_Options; }

private:
	Sweep m_Sweep;

	Options m_Options;
};

struct SweepTestResult {
	bool m_bCollided;
	//Vector3f m_ptFinal;
	float m_fRelativeMovement;
	Vector3f m_ptContact;
	Vector3f m_vContactNormal;
	const btCollisionObject* m_pCollisionObject;
};

struct ContactTestResult {
	// VS2013 does not generate a default move constructor so we must create it ourselves.
	ContactTestResult(ContactTestResult&& other) :
			m_aContacts(std::move(other.m_aContacts)) {}
	void operator=(ContactTestResult&& other) { m_aContacts = std::move(other.m_aContacts); }

	ContactTestResult() {}
	struct Contact {
		//BulletRigidBody* m_pObject;
		const btCollisionObject* m_pObject;
		Vector3f m_ptOnOtherObject;
		Vector3f m_vNormalOnOtherObject;
		float m_fDistance; // When positive, objects are separated. When negative they are penetrating.
	};
	std::vector<Contact> m_aContacts;
};

class BulletAvatar;
class BulletSensor;

using SensorMap = std::map<int, std::shared_ptr<BulletSensor>>;

class BulletUniverse : public Universe {
public:
	BulletUniverse(PhysicsFactory& factory);
	~BulletUniverse();
	bool Init(const UniverseOptions& options);

	// Universe API functions.
public:
	virtual bool Add(RigidBodyStatic* pRigidBodyStatic) override;
	virtual bool Add(const std::shared_ptr<RigidBodyStatic>& pRigidBodyStatic) override;
	virtual bool Add(const std::shared_ptr<RigidBodyDynamic>& pRigidBodyDynamic) /*override*/;
	virtual bool Add(const std::shared_ptr<Avatar>& pAvatar) /*override*/;
	virtual bool Add(const std::shared_ptr<Vehicle>& pVehicle) override;
	virtual bool Add(const std::shared_ptr<Sensor>& pSensor) override;
	virtual bool FindSensor(int pid, std::shared_ptr<Sensor>& pSensor) override;
	virtual bool Remove(RigidBodyStatic* pRigidBodyStatic) override;
	virtual bool Remove(Vehicle* pVehicle) override;
	virtual bool Remove(Sensor* pSensor) override;
	virtual bool RemoveAll() override;

	virtual void Step(double dFrameStartRealTime, double dDeltaPhysicsTime) override;
	virtual bool BackOutFromCollision(const Collidable& collidable, const Vector3f& vBackOutDirection, Out<float> distance, float fDistFromObstacles = 0.01f) override;
	virtual bool Collide(const Collidable& collidable, const Matrix44f& mtxFrom, const Matrix44f& mtxTo, Out<float> relativeMovement, const CollideOptions& collideOptions) override;
	virtual bool Collide(const Collidable& collidable, const Vector3f& ptFrom, const Vector3f& ptTo, Out<float> relativeMovement, const CollideOptions& collideOptions) override;
	virtual bool RayTest(const Vector3f& ptStart, const Vector3f& vDir, float fMaxDist, RayTestOptions options, Out<RayTestResult> result) override;

	// Output physics profiling data.
	virtual void DumpProfile() override;
	virtual void DumpProfile(std::ostringstream& strm) override;
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const override final;

	// Internal usage
public:
	btDynamicsWorld* GetBTWorld() { return m_pDynamicsWorld.get(); }
	void ReconfiguredRigidBody(BulletRigidBody* pRigidBody); // Some changes to objects require removing and re-adding to Universe.
	void GetRigidBodies(Out<std::vector<BulletRigidBody*>> apRigidBodies); // Access to objects needed for debug rendering.
	double GetRealTimeForInputProcessing() const; // Times up until which input needs to be processed (for the current step).
	const Vector3f& GetGravity() const { return m_UniverseOptions.m_vGravityMPS2; }
	float GetGravityMagnitude() const { return m_fGravityMagnitude; }

	// Calls callback for each object near the input bounding box.
	// Returned objects should be near in the sense that their bounding boxes (or other bounding volumes) intersect the input box.
	void NearbyObjectTest(const Vector3f& ptBBoxMin, const Vector3f& ptBBoxMax, const std::function<void(const btBroadphaseProxy* pBroadphaseProxy)>& callback);

	// Positive distance means objects are separated. Negative depth means penetration is occurring.
	ContactTestResult ContactTest(btCollisionObject* pCollisionObject, float fMaxSeparation, std::vector<const btCollisionObject*> apExcludeObjects);
	ContactTestResult ContactTest(btCollisionObject* pCollisionObject, float fMaxSeparation, const std::function<bool(const btCollisionObject*)>& filter = nullptr);

	struct SweepTestOptions {
		SweepTestOptions() :
				m_fDistFromObstacles(0), m_iCollisionGroupMask(0xffff), m_iCollidesWithMask(0xffff) {}

		uint16_t m_iCollisionGroupMask;
		uint16_t m_iCollidesWithMask;
		float m_fDistFromObstacles;
	};

	// Only convex objects and compound objects composed of convex objects can do SweepTests.
	SweepTestResult SweepTest(const SweepTestArguments& args);
	SweepTestResult SweepTest(const SweepTestArguments::Sweep& sweep, const SweepTestArguments::Options& options);

	std::shared_ptr<SensorOverlapFilterCallback> GetSensorOverlapFilterCallback() const { return m_pSensorOverlapFilterCallback; }

private:
	SweepTestResult ConvexSweepTestImpl(const SweepTestArguments::Sweep& sweep, const SweepTestArguments::Options& options);
	SweepTestResult CompoundConvexSweepTestImpl(const SweepTestArguments::Sweep& sweep, const SweepTestArguments::Options& options);

public:
#if 0
	struct CollisionCheckResult {
		struct CollisionPoint {
			Vector3f m_ptTestObject;
			Vector3f m_ptCollidedObject;
			const btCollisionObject* m_pCollidedObject;
		};
		std::vector<CollisionPoint> m_aCollisionPoints;
	};
	CollisionCheckResult CollisionCheck(btCollisionObject* pCollisionObject, const Vector3f& ptLocation, const Vector3f& vShapeOffset);
#endif
private:
	static void staticInternalPreTickCallback(btDynamicsWorld* pDynamicsWorld, btScalar timeStep);
	void InternalPreTickCallback(btDynamicsWorld* pDynamicsWorld, btScalar timeStep);
	static void staticInternalPostTickCallback(btDynamicsWorld* pDynamicsWorld, btScalar timeStep);
	void InternalPostTickCallback(btDynamicsWorld* pDynamicsWorld, btScalar timeStep);

	bool PreAddRigidBody(const std::shared_ptr<BulletRigidBody>& pRigidBody);
	bool AddBulletRigidBody(std::shared_ptr<BulletRigidBody> pRigidBody);
	bool RemoveBulletRigidBody(BulletRigidBody* pRigidBody);
	void RemovingRigidBody(BulletRigidBody* pRigidBody);
	void RemoveAllSensors();
	void GetMemSizeofRigidBodySet(IMemSizeGadget* pMemSizeGadget) const;

private:
	UniverseOptions m_UniverseOptions;
	float m_fGravityMagnitude;

	float m_fBulletPartialTime; // This should mirror the m_localTime variable in btDiscreteDynamicsWorld

	double m_dInputRealTimeAtNextStep;
	double m_dThisStepSize;
	std::unique_ptr<btDefaultCollisionConfiguration> m_pCollisionConfiguration;
	std::unique_ptr<btSequentialImpulseConstraintSolver> m_pConstraintSolver;
	std::unique_ptr<btDbvtBroadphase> m_pBroadPhase;
	std::unique_ptr<btCollisionDispatcher> m_pCollisionDispatcher;
	std::unique_ptr<btDynamicsWorld> m_pDynamicsWorld;

	struct KeyAccessor {
		typedef const BulletRigidBody* result_type;
		const BulletRigidBody* operator()(const std::shared_ptr<BulletRigidBody>& p) const { return p.get(); }
	};
	typedef boost::multi_index_container<
		std::shared_ptr<BulletRigidBody>,
		boost::multi_index::indexed_by<
			boost::multi_index::hashed_unique<KeyAccessor>>>
		RigidBodySet;

	RigidBodySet m_setRigidBodiesInUniverse;

	std::shared_ptr<SensorOverlapFilterCallback> m_pSensorOverlapFilterCallback;
	SensorMap m_SensorMap;
};

} // namespace Physics
} // namespace KEP
