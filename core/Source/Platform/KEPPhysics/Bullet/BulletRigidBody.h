///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "BulletCollidable.h"

class btRigidBody;

namespace KEP {

class IMemSizeGadget;

namespace Physics {

class BulletUniverse;
class BulletFactory;

// Base class for BulletRigidBodyStatic and BulletRigidBodyDynamic to handle common behaviors.
class BulletRigidBody : public BulletCollidable {
public:
	BulletRigidBody(BulletFactory& factory);
	virtual ~BulletRigidBody() {}

	virtual btRigidBody* GetBTRigidBody() const = 0;
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;

private:
};

} // namespace Physics
} // namespace KEP
