///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BulletCollisionShape.h"
#include "BulletFactory.h"
#include "Core/Util/Parameter.h"
#include "../../Common/include/LogHelper.h"
#include "../../Common/include/IMemSizeGadget.h"
#include "Core/Math/Intersect.h"
#include "BulletCast.h"
#include <assert.h>
#include <iomanip>

static LogInstance("Physics");

namespace KEP {
namespace Physics {

btIndexedMesh CollisionShapeMeshProperties_to_btIndexedMesh(const CollisionShapeMeshProperties& props, InOut<std::vector<unsigned short>> aTriIndices) {
	btIndexedMesh indexedMesh;

	bool bIsStrip = props.m_bIsTriangleStrip;
	if (bIsStrip) {
		// Need to convert strips to triangles for bullet.
		size_t nTriangles = props.m_nIndices < 3 ? 0 : props.m_nIndices - 2;
		if (aTriIndices.get().size() + nTriangles * 3 > aTriIndices.get().capacity()) {
			LogError("Need to preallocate storage for indices to avoid invalidating pointers");
			nTriangles = 0;
		}

		size_t nNondegenerateTriangles = 0;
		unsigned short* pIndices = aTriIndices.get().data() + aTriIndices.get().size();
		for (size_t i = 0; i < nTriangles; ++i) {
			unsigned short idx0 = props.m_pFirstIndex[i + 0];
			unsigned short idx1 = props.m_pFirstIndex[i + 1 + (i & 1)];
			unsigned short idx2 = props.m_pFirstIndex[i + 2 - (i & 1)];
			if (idx0 != idx1 && idx1 != idx2 && idx2 != idx0) {
				//if( idx0 != 357 || idx1 != 359 || idx2 != 360 )
				//	continue;

				++nNondegenerateTriangles;
				aTriIndices.get().push_back(idx0);
				aTriIndices.get().push_back(idx1);
				aTriIndices.get().push_back(idx2);
			}
		}
		indexedMesh.m_numTriangles = nNondegenerateTriangles;
		indexedMesh.m_triangleIndexBase = reinterpret_cast<const unsigned char*>(pIndices);
		indexedMesh.m_triangleIndexStride = 3 * sizeof(*pIndices);
	} else {
		// Reference triangle indexes directly.
		indexedMesh.m_numTriangles = props.m_nIndices / 3;
		indexedMesh.m_triangleIndexBase = reinterpret_cast<const unsigned char*>(props.m_pFirstIndex);
		indexedMesh.m_triangleIndexStride = 3 * sizeof(*props.m_pFirstIndex);
	}
	indexedMesh.m_numVertices = props.m_nPoints;
	indexedMesh.m_vertexBase = reinterpret_cast<const unsigned char*>(props.m_pFirstPoint);
	//indexedMesh.m_vertexStride = props.m_nStride;
	indexedMesh.m_vertexStride = sizeof(*props.m_pFirstPoint);

	return indexedMesh;
}

// Optimization for large meshes:
// Split large meshes into parts each containing less than (1<<MAX_NUM_PARTS_IN_BITS) meshes.
// This allows us to use quantized AAB compression which is supposed to save memory and give better performance.
// I suspect we will want to use a btCompoundShape object to hold the parts to make this division transparent to the rest of the code.
std::unique_ptr<btBvhTriangleMeshShape> CreateBTMeshShapeFromMultipleProps(const CollisionShapeMeshProperties* paProps, size_t nProps, const Vector3f& vScale) {
	// A btTriangleIndexVertexArray, but with included index data from possible conversion of triangle strips.
	struct VertexArrayDescriptionWithData : public btTriangleIndexVertexArray {
		~VertexArrayDescriptionWithData() {}
		std::vector<unsigned short> m_aTriIndices; // For converting from strips to triangles
	};

	class TriMeshShapeWithVertexArrayDescription : public btBvhTriangleMeshShape {
	public:
		// Can only use quantized AABB compression if the number of parts is less than (1<<MAX_NUM_PARTS_IN_BITS)
		explicit TriMeshShapeWithVertexArrayDescription(std::unique_ptr<btTriangleIndexVertexArray>&& pArrayDescription, std::vector<unsigned short>&& aTriIndices) :
				btBvhTriangleMeshShape(pArrayDescription.get(), pArrayDescription->getNumSubParts() < (1 << MAX_NUM_PARTS_IN_BITS)) {
			m_pArrayDescription = std::move(pArrayDescription);
		}

	private:
		std::unique_ptr<btTriangleIndexVertexArray> m_pArrayDescription;
	};

	size_t nTriIndicesNeeded = 0;
	for (size_t i = 0; i < nProps; ++i) {
		if (paProps[i].m_bIsTriangleStrip) {
			size_t nTriangles = paProps[i].m_nIndices >= 3 ? paProps[i].m_nIndices - 2 : 0;
			nTriIndicesNeeded += nTriangles * 3;
		}
	}

	std::unique_ptr<VertexArrayDescriptionWithData> pTIVA = std::make_unique<VertexArrayDescriptionWithData>();
	pTIVA->setScaling(ToBullet(vScale));
	std::vector<unsigned short>& aTriIndices = pTIVA->m_aTriIndices;
	aTriIndices.reserve(nTriIndicesNeeded);
	for (size_t i = 0; i < nProps; ++i) {
		btIndexedMesh indexedMesh = CollisionShapeMeshProperties_to_btIndexedMesh(paProps[i], inout(aTriIndices));
#if 0 // Log points
		for( size_t i = 0; i < indexedMesh.m_numTriangles; ++i )
		{
			const Vector3f* pPoints = (const Vector3f*) indexedMesh.m_vertexBase;
			const unsigned short* pIndices = (unsigned short*) indexedMesh.m_triangleIndexBase;
			size_t idx0 = pIndices[i*3+0];
			size_t idx1 = pIndices[i*3+1];
			size_t idx2 = pIndices[i*3+2];
			const Vector3f& pt0 = pPoints[idx0];
			const Vector3f& pt1 = pPoints[idx1];
			const Vector3f& pt2 = pPoints[idx2];

			LogInfo("" << std::setw(4) << idx0 << " " << std::setw(4) <<idx1 << " " << std::setw(4) << idx2
				<< " Pt1:"
				<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pPoints[idx0].x
				<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pPoints[idx0].y
				<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pPoints[idx0].z
				<< " Pt2:"
				<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pPoints[idx1].x
				<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pPoints[idx1].y
				<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pPoints[idx1].z
				<< " Pt3:"
				<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pPoints[idx2].x
				<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pPoints[idx2].y
				<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pPoints[idx2].z
				);
		}
#endif
		if (indexedMesh.m_numTriangles > 0) {
			pTIVA->addIndexedMesh(indexedMesh, PHY_SHORT);
		}
	}

	std::unique_ptr<TriMeshShapeWithVertexArrayDescription> pMeshShape;
	if (pTIVA->getNumSubParts() != 0)
		pMeshShape = std::make_unique<TriMeshShapeWithVertexArrayDescription>(std::move(pTIVA), std::move(aTriIndices));

	return std::unique_ptr<btBvhTriangleMeshShape>(pMeshShape.release());
}

BulletCollisionShape::BulletCollisionShape(BulletFactory& factory, const CollisionShapeSphereProperties& props) :
		CollisionShape(factory), PhysicsBase(factory), m_ShapeType(eCollisionShapeType::Sphere) {
	if (props.m_fRadiusMeters < 0)
		return;

	m_pBTCollisionShape = std::make_unique<btSphereShape>(props.m_fRadiusMeters);
	m_pBTCollisionShape->setUserPointer(this);
}

BulletCollisionShape::BulletCollisionShape(BulletFactory& factory, const CollisionShapeBoxProperties& props) :
		CollisionShape(factory), PhysicsBase(factory), m_ShapeType(eCollisionShapeType::Box) {
	for (float coord : props.m_vDimensionsMeters) {
		if (coord < 0)
			return;
	}
	m_pBTCollisionShape = std::make_unique<btBoxShape>(ToBullet(0.5f * props.m_vDimensionsMeters));
	m_pBTCollisionShape->setUserPointer(this);
}

BulletCollisionShape::BulletCollisionShape(BulletFactory& factory, const CollisionShapeCapsuleProperties& props) :
		CollisionShape(factory), PhysicsBase(factory), m_ShapeType(eCollisionShapeType::Capsule) {
	if (props.m_fCylinderHeightMeters > 0 && props.m_fRadiusMeters > 0 && props.m_iAxis < 3) {
		switch (props.m_iAxis) {
			case 0:
				m_pBTCollisionShape = std::make_unique<btCapsuleShapeX>(props.m_fRadiusMeters, props.m_fCylinderHeightMeters);
				break;
			case 1:
				m_pBTCollisionShape = std::make_unique<btCapsuleShape>(props.m_fRadiusMeters, props.m_fCylinderHeightMeters);
				break;
			case 2:
				m_pBTCollisionShape = std::make_unique<btCapsuleShapeZ>(props.m_fRadiusMeters, props.m_fCylinderHeightMeters);
				break;
		}
	}
	if (m_pBTCollisionShape)
		m_pBTCollisionShape->setUserPointer(this);
}

BulletCollisionShape::BulletCollisionShape(BulletFactory& factory, const CollisionShapeConvexHullProperties& props) :
		CollisionShape(factory), PhysicsBase(factory), m_ShapeType(eCollisionShapeType::ConvexHull) {
	std::unique_ptr<btConvexHullShape> pConvexHullShape = std::make_unique<btConvexHullShape>();
	for (const auto& pointList : props.m_aPointData) {
		for (size_t i = 0; i < pointList.second; ++i)
			pConvexHullShape->addPoint(ToBullet(pointList.first[i]), false);
	}
	pConvexHullShape->recalcLocalAabb();
	m_pBTCollisionShape = std::move(pConvexHullShape);
	m_pBTCollisionShape->setUserPointer(this);
}

BulletCollisionShape::BulletCollisionShape(BulletFactory& factory, const CollisionShapeMeshProperties& props, const Vector3f& vScale) :
		CollisionShape(factory), PhysicsBase(factory), m_ShapeType(eCollisionShapeType::Mesh) {
	m_pBTCollisionShape = CreateBTMeshShapeFromMultipleProps(&props, 1, vScale);
	if (!m_pBTCollisionShape)
		return;
	m_pBTCollisionShape->setUserPointer(this);
}

BulletCollisionShape::BulletCollisionShape(BulletFactory& factory, const std::vector<CollisionShapeMeshProperties>& aProps, const Vector3f& vScale) :
		CollisionShape(factory), PhysicsBase(factory), m_ShapeType(eCollisionShapeType::Mesh) {
	m_pBTCollisionShape = CreateBTMeshShapeFromMultipleProps(aProps.data(), aProps.size(), vScale);
	if (!m_pBTCollisionShape)
		return;
	m_pBTCollisionShape->setUserPointer(this);
}

BulletCollisionShape::~BulletCollisionShape() {
}

void BulletCollisionShape::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_pBTCollisionShape);
	}
}

} // namespace Physics
} // namespace KEP
