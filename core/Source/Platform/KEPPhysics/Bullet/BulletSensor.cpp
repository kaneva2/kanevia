///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "Core/Math/Vector.h"
#include "Core/Math/Matrix.h"
#include "Core/Math/Primitive.h"
#include "CoreStatic/PhysicsUserData.h"

#include "BulletSensor.h"
#include "BulletCast.h"
#include "BulletMathCast.h"
#include "BulletFactory.h"

#include <bullet/BulletCollision/CollisionDispatch/btGhostObject.h>

#include "../common/include/IMemSizeGadget.h"
#include "../../Common/include/LogHelper.h"
static LogInstance("Instance");

namespace KEP {
namespace Physics {

BulletSensor::BulletSensor(BulletFactory& factory) :
		Sensor(factory), BulletCollidable(factory), PhysicsBase(factory), Collidable(factory) {
}

BulletSensor::~BulletSensor() {
	btCollisionWorld* pWorld = GetCurrentUniverse()->GetBTWorld();
	// All btGhostObjects should be removed from the world before they are destructed.
	// Reference the btGhostObject::~btGhostObject in btGhostObject.cpp
	if (pWorld) {
		pWorld->removeCollisionObject(m_pGhostObject);
	}

	m_pOverlapFilterCallback.reset();
	delete m_pGhostPairCallback;
	delete m_pGhostObject;
	delete m_pGhostShape;
}

bool BulletSensor::Init(const SensorProperties& props) {
	btCollisionWorld* pWorld = GetCurrentUniverse()->GetBTWorld();
	if (!pWorld)
		return false;

	m_pGhostShape = GetCollisionShape(props);
	if (!m_pGhostShape)
		return false;

	// Create the initial transform from the properties
	btTransform transform = ToBullet(props.m_transform);

	// configure the ghost object for sensor trigger behavior
	m_pGhostObject = new btPairCachingGhostObject();
	m_pGhostObject->setWorldTransform(transform);
	m_pGhostObject->setCollisionShape(m_pGhostShape);
	m_pGhostObject->setCollisionFlags(m_pGhostObject->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
	SetUserData(std::make_shared<PhysicsBodyData>(props.m_objectType, props.m_Pid));
	m_pGhostObject->setUserPointer(static_cast<BulletSensor*>(this));

	// Configure the ghost pair callback
	m_pGhostPairCallback = new btGhostPairCallback();
	pWorld->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(m_pGhostPairCallback);

	// Configure the pair cache overlapFilterCallback for the world.
	// If the overlap filter callback for the world and for the ghost object pair caches
	// do not match, you will get inconsistent results.
	m_pOverlapFilterCallback = GetCurrentUniverse()->GetSensorOverlapFilterCallback();
	m_pGhostObject->getOverlappingPairCache()->setOverlapFilterCallback(m_pOverlapFilterCallback.get());

	// collides with all object types except other sensor triggers and static objects
	int collisionGroupMask = CollisionGroup::SensorTrigger;
	int collidesWithMask = (CollisionMask::All ^ (CollisionGroup::SensorTrigger));
	SetCollisionMasks(collisionGroupMask, collidesWithMask);

	pWorld->addCollisionObject(m_pGhostObject, collisionGroupMask, collidesWithMask);

	return true;
}

// #MLB_TODO - Need to confirm collision shape properties have already been set to meters
btCollisionShape* BulletSensor::GetCollisionShape(const SensorProperties& props) {
	const BoxShapeSensorProperties& boxProps = static_cast<const Physics::BoxShapeSensorProperties&>(props);
	m_pGhostShape = new btBoxShape(ToBullet(boxProps.m_BoxShapeSensorProps.m_vDimensionsMeters));
	return m_pGhostShape;
}

btCollisionObject* BulletSensor::GetBTCollisionObject() const {
	return m_pGhostObject;
}

// #MLB_TODO - Determine if the sensor needs the capability to be disabled
// If the sensor can never be disabled then this is correct, it's always on,
// otherwise an update will be required to enable that capability.
void BulletSensor::SetIsCollidable(bool bCollidable) {
}

bool BulletSensor::GetCollisionObjectPid(const btCollisionObject* pCollisionObject, int& pid) const {
	pid = -1;
	Collidable* pCollidable = static_cast<Collidable*>(pCollisionObject != nullptr ? pCollisionObject->getUserPointer() : nullptr);
	if (pCollidable) {
		PhysicsBodyData* pBodyData = dynamic_cast<PhysicsBodyData*>(pCollidable->GetUserData().get());
		if (pBodyData && pBodyData->m_ObjectType == ObjectType::DYNAMIC) {
			pid = pBodyData->m_iObjectId;
		}
	}
	return (pid != -1);
}

int BulletSensor::GetSensorPid() const {
	int pid = 0;
	GetCollisionObjectPid(GetBTCollisionObject(), pid);
	return pid;
}

bool BulletSensor::IsObjectBounded(int pid) const {
	CollisionObjectPair searchPair = std::make_pair(m_pGhostObject, pid);
	auto aBoundedPair = m_boundedPairs.find(searchPair);
	return (aBoundedPair != m_boundedPairs.end());
}

bool BulletSensor::IsObjectBounded(const btBoxShape& shape0, const btBoxShape& shape1) {
	bool result = true;
	int numVerts = shape0.getNumVertices();
	for (int i = 0; i < numVerts && result; ++i) {
		btVector3 pt;
		shape0.getVertex(i, pt);
		result = shape1.isInside(pt, 0);
	}

	return result;
}

bool BulletSensor::IsObjectBounded(const btCollisionObject* pTrgtClsnObj) {
	bool result = false;
	btBoxShape* pGhostBoxShape = dynamic_cast<btBoxShape*>(m_pGhostShape);
	if (!pTrgtClsnObj || !pGhostBoxShape)
		return result;

	const btCollisionShape* pTrgtClsnShape = pTrgtClsnObj->getCollisionShape();
	const btBoxShape* pTrgtClsnBoxShape = dynamic_cast<const btBoxShape*>(pTrgtClsnShape);

	if (!pTrgtClsnBoxShape) {
		// Target Collision Object isn't a btBoxShape, create one based on
		// the collision object shape.
		btTransform xform = pTrgtClsnObj->getWorldTransform().inverse();
		btVector3 aabbMax, aabbMin;
		pTrgtClsnShape->getAabb(xform, aabbMin, aabbMax);
		btBoxShape boxShape((aabbMax - aabbMin) * 0.5f);

		result = IsObjectBounded(boxShape, *pGhostBoxShape);

	} else {
		result = IsObjectBounded(*pTrgtClsnBoxShape, *pGhostBoxShape);
	}

	return result;
}

void BulletSensor::Update(btCollisionWorld* pWorld) {
	CollisionObjectPairSet pairsThisUpdate;
	btManifoldArray manifoldArray;
	btBroadphasePairArray& pairArray = m_pGhostObject->getOverlappingPairCache()->getOverlappingPairArray();

	for (int i = 0; i < pairArray.size(); ++i) {
		manifoldArray.clear();

		const btBroadphasePair& pair = pairArray[i];

		btBroadphasePair* collisionPair = pWorld->getPairCache()->findPair(pair.m_pProxy0, pair.m_pProxy1);

		if (!collisionPair)
			continue;

		if (collisionPair->m_algorithm)
			collisionPair->m_algorithm->getAllContactManifolds(manifoldArray);

		for (int j = 0; j < manifoldArray.size(); j++) {
			btPersistentManifold* pManifold = manifoldArray[j];

			if (pManifold->getNumContacts() > 0) {
				// Get the two collision objects involved in the collision
				const btCollisionObject* pBody0 = pManifold->getBody0();
				const btCollisionObject* pBody1 = pManifold->getBody1();

				// Always create the pair in a predictable order
				bool const swapped = (pBody0 == m_pGhostObject);
				const btCollisionObject* pGhostClsnObj = swapped ? pBody0 : pBody1;
				const btCollisionObject* pTrgtClsnObj = swapped ? pBody1 : pBody0;

				int trgtClsnObjPid = -1;
				if (GetCollisionObjectPid(pTrgtClsnObj, trgtClsnObjPid)) {
					CollisionObjectPair thisPair = std::make_pair(pGhostClsnObj, trgtClsnObjPid);
					pairsThisUpdate.insert(thisPair);

					// Also track when the bounding volume of a pair is completely
					// within the bounds of the sensor object.
					auto aBoundedPair = m_boundedPairs.find(thisPair);
					bool isFound = aBoundedPair != m_boundedPairs.end();
					bool isBounded = IsObjectBounded(pTrgtClsnObj);

					if (isBounded && !isFound) {
						// Object is bounded by the sensor, but is not a member
						// of the bounded pairs set.
						m_boundedPairs.insert(thisPair);

					} else if (!isBounded && isFound) {
						// Object is no longer bounded by the sensor
						m_boundedPairs.erase(aBoundedPair);
					}
				}
			}
		}
	}

	// Create another list for pairs that
	// were removed this update
	CollisionObjectPairSet removedPairs;

	// Take the difference between collision pairs from
	// the last update, and this update.
	std::set_difference(m_pairsLastUpdate.begin(), m_pairsLastUpdate.end(),
		pairsThisUpdate.begin(), pairsThisUpdate.end(),
		std::inserter(removedPairs, removedPairs.begin()));

	m_pairsLastUpdate = pairsThisUpdate;
}

void BulletSensor::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_pGhostObject);
		pMemSizeGadget->AddObject(m_pGhostShape);
		pMemSizeGadget->AddObject(m_pairsLastUpdate);
		pMemSizeGadget->AddObject(m_boundedPairs);
	}
}

} // namespace Physics
} // namespace KEP
