///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <set>

#include "KEPConstants.h"
#include "BulletCollidable.h"
#include "../Sensor.h"

struct SensorOverlapFilterCallback;
class btCollisionWorld;
class btCollisionShape;
class btGhostPairCallback;
class btPairCachingGhostObject;

namespace KEP {

class IMemSizeGadget;

namespace Physics {

class BulletFactory;

using CollisionObjectPair = std::pair<const btCollisionObject*, int>;
using CollisionObjectPairSet = std::set<CollisionObjectPair>;

class BulletSensor : public Sensor, virtual public BulletCollidable {
public:
	BulletSensor(BulletFactory& factory);
	virtual ~BulletSensor();
	bool Init(const SensorProperties& props);

	virtual void SetIsCollidable(bool bCollidable);
	virtual btCollisionObject* GetBTCollisionObject() const override final;
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const override final;
	virtual bool IsObjectBounded(int pid) const override final;

	int GetSensorPid() const;
	void Update(btCollisionWorld* pWorld);

private:
	bool GetCollisionObjectPid(const btCollisionObject*, int& pid) const;
	btCollisionShape* GetCollisionShape(const SensorProperties& props);
	bool IsObjectBounded(const btCollisionObject* pTrgtClssnObj);
	bool IsObjectBounded(const btBoxShape& shape0, const btBoxShape& shape1);

private:
	std::shared_ptr<SensorOverlapFilterCallback> m_pOverlapFilterCallback;
	btPairCachingGhostObject* m_pGhostObject = nullptr;
	btGhostPairCallback* m_pGhostPairCallback = nullptr;
	btCollisionShape* m_pGhostShape = nullptr;
	CollisionObjectPairSet m_pairsLastUpdate;
	CollisionObjectPairSet m_boundedPairs;
};

} // namespace Physics
} // namespace KEP
