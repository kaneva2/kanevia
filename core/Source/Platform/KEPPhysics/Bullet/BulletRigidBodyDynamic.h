///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "../RigidBodyDynamic.h"
#include "../Collidable.h"
#include "BulletRigidBody.h"

struct btDefaultMotionState;

namespace KEP {
namespace Physics {

class CollisionShape;
class BulletFactory;
class BulletCollisionShape;

class BulletRigidBodyDynamic
		: public RigidBodyDynamic,
		  public BulletRigidBody {
public:
	BulletRigidBodyDynamic(BulletFactory& factory);
	virtual ~BulletRigidBodyDynamic();
	bool Init(const RigidBodyDynamicProperties& props, CollisionShape* pShape, float fScale);

	virtual btRigidBody* GetBTRigidBody() const;
	virtual btCollisionObject* GetBTCollisionObject() const;

private:
	std::unique_ptr<btDefaultMotionState> m_pMotionState;
	std::shared_ptr<const BulletCollisionShape> m_pCollisionShape;
	std::unique_ptr<btRigidBody> m_pRigidBody;
};

} // namespace Physics
} // namespace KEP
