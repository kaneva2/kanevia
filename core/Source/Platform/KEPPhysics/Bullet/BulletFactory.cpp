///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BulletFactory.h"

#include "BulletAvatar.h"
#include "BulletDebugWindow.h"
#include "BulletCollisionShape.h"
#include "BulletRigidBodyDynamic.h"
#include "BulletRigidBodyStatic.h"
#include "BulletUniverse.h"
#include "BulletVehicle.h"
#include "BulletSensor.h"

namespace KEP {
namespace Physics {

namespace {
BulletFactory s_BulletFactory;
} // anonymous namespace

BulletFactory* GetBulletFactory() {
	return &s_BulletFactory;
}

KEPPHYSICS_API PhysicsFactory* GetPhysicsFactory(eFactoryType type) {
	if (type == eFactoryType::Bullet)
		return GetBulletFactory();
	return nullptr;
}

std::shared_ptr<DebugWindow> BulletFactory::CreateDebugWindow(const DebugWindowOptions& opt) {
	std::shared_ptr<BulletDebugWindow> pBulletDebugWindow = BulletDebugWindow::CreateBulletDebugWindow(*this, opt);
	return pBulletDebugWindow;
}

std::shared_ptr<Universe> BulletFactory::CreateUniverse(const UniverseOptions& opt) {
	std::shared_ptr<BulletUniverse> pBulletUniverse = std::make_shared<BulletUniverse>(*this);
	if (!pBulletUniverse->Init(opt))
		return nullptr;

	return pBulletUniverse;
}

std::shared_ptr<Avatar> BulletFactory::CreateAvatar(const AvatarProperties& props) {
	std::shared_ptr<BulletAvatar> pBulletAvatar = std::make_shared<BulletAvatar>(*this);
	if (!pBulletAvatar->Init(props))
		return nullptr;
	return pBulletAvatar;
}

std::shared_ptr<Vehicle> BulletFactory::CreateVehicle(const VehicleProperties& props) {
	std::shared_ptr<BulletVehicle> pBulletVehicle = std::make_shared<BulletVehicle>(*this);
	if (!pBulletVehicle->Init(props))
		return nullptr;
	return pBulletVehicle;
}

std::shared_ptr<Sensor> BulletFactory::CreateSensor(const SensorProperties& props) {
	std::shared_ptr<BulletSensor> pBulletSensor = std::make_shared<BulletSensor>(*this);
	if (!pBulletSensor->Init(props))
		return nullptr;
	return pBulletSensor;
}

std::shared_ptr<RigidBodyStatic> BulletFactory::CreateRigidBodyStatic(const RigidBodyStaticProperties& props, CollisionShape* pShape, const Vector3f& vShapeOffset, float fScale) {
	std::shared_ptr<BulletRigidBodyStatic> pBulletRigidBodyStatic = std::make_shared<BulletRigidBodyStatic>(*this);
	if (!pBulletRigidBodyStatic->Init(props, pShape, vShapeOffset, fScale))
		return nullptr;

	return pBulletRigidBodyStatic;
}

std::shared_ptr<CollisionShape> BulletFactory::CreateCollisionShape(const CollisionShapeCapsuleProperties& props) {
	std::shared_ptr<BulletCollisionShape> pCollisionShape = std::make_shared<BulletCollisionShape>(*this, props);
	if (!pCollisionShape->GetBTCollisionShape())
		pCollisionShape.reset();
	return pCollisionShape;
}

std::shared_ptr<CollisionShape> BulletFactory::CreateCollisionShape(const CollisionShapeMeshProperties& props, const Vector3f& vScale) {
	std::shared_ptr<BulletCollisionShape> pCollisionShape = std::make_shared<BulletCollisionShape>(*this, props, vScale);
	if (!pCollisionShape->GetBTCollisionShape())
		pCollisionShape.reset();
	return pCollisionShape;
}

std::shared_ptr<CollisionShape> BulletFactory::CreateCollisionShape(const std::vector<CollisionShapeMeshProperties>& props, const Vector3f& vScale) {
	std::shared_ptr<BulletCollisionShape> pCollisionShape = std::make_shared<BulletCollisionShape>(*this, props, vScale);
	if (!pCollisionShape->GetBTCollisionShape())
		pCollisionShape.reset();
	return pCollisionShape;
}

std::shared_ptr<CollisionShape> BulletFactory::CreateCollisionShape(const CollisionShapeBoxProperties& props) {
	std::shared_ptr<BulletCollisionShape> pCollisionShape = std::make_shared<BulletCollisionShape>(*this, props);
	if (!pCollisionShape->GetBTCollisionShape())
		pCollisionShape.reset();
	return pCollisionShape;
}

std::shared_ptr<CollisionShape> BulletFactory::CreateCollisionShape(const CollisionShapeSphereProperties& props) {
	std::shared_ptr<BulletCollisionShape> pCollisionShape = std::make_shared<BulletCollisionShape>(*this, props);
	if (!pCollisionShape->GetBTCollisionShape())
		pCollisionShape.reset();
	return pCollisionShape;
}

} // namespace Physics
} // namespace KEP
