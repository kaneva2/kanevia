///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BulletVehicle.h"
#include "BulletCast.h"
#include "BulletFactory.h"
#include "Core/Math/Distance.h"
#include "Core/Math/Interpolate.h"
#include "Core/Math/TransformUtil.h"
#include "Core/Util/Memory.h"

#include "../common/include/IMemSizeGadget.h"
#include "../../Common/include/LogHelper.h"
static LogInstance("Instance");
#include <iomanip>

namespace KEP {
namespace Physics {

// Axis directions
static const int kiRightAxisIndex = 0;
static const int kiUpAxisIndex = 1;
static const int kiForwardAxisIndex = 2;

// Distance around vehicle to start pushing away from other vehicles.
static const float kfVehicleCollisionBufferDistance = 0.5f; // Half a meter.
static const float kfVehicleCollisionMaxAccel = 4 * 9.8f; // 4 G

class BulletVehicle::MyRayCastVehicle : public btRaycastVehicle {
public:
	MyRayCastVehicle(BulletVehicle* pBulletVehicle, const btVehicleTuning& tuning, btRigidBody* chassis, btVehicleRaycaster* raycaster) :
			btRaycastVehicle(tuning, chassis, raycaster), m_pBulletVehicle(pBulletVehicle) {}

	virtual void updateAction(btCollisionWorld* collisionWorld, btScalar step) {
		// Note: Vehicle's very first step has gravity applied but suspension effects don't take effect until next step.
		// This can cause some slight bouncing when the vehicle is created.
		// If it matters, we can try zeroing out the velocity on the first call to updateAction.
		bool bBraking = false;
		m_pBulletVehicle->ApplyForces(step, out(bBraking));
		updateVehicle(step);
		m_pBulletVehicle->PostUpdate(step);
	}

private:
	BulletVehicle* m_pBulletVehicle;
};

class BulletVehicle::MyRayCaster : public btVehicleRaycaster {
public:
	MyRayCaster(BulletVehicle* pBulletVehicle) :
			m_pBulletVehicle(pBulletVehicle) {}

	virtual void* castRay(const btVector3& from, const btVector3& to, btVehicleRaycasterResult& result) {
		class ClosestExceptForOne : public btCollisionWorld::ClosestRayResultCallback {
		public:
			ClosestExceptForOne(btRigidBody* pException, const btVector3& from, const btVector3& to) :
					btCollisionWorld::ClosestRayResultCallback(from, to), m_pException(pException) {}

			virtual btScalar addSingleResult(btCollisionWorld::LocalRayResult& rayResult, bool normalInWorldSpace) {
				if (rayResult.m_collisionObject == m_pException)
					return 1.0;

				if (rayResult.m_collisionObject) {
					const btBroadphaseProxy* pBTBroadphaseProxy = rayResult.m_collisionObject->getBroadphaseHandle();
					if ((pBTBroadphaseProxy->m_collisionFilterGroup & m_collisionFilterMask) == 0)
						return 1.0;
					if ((m_collisionFilterGroup & pBTBroadphaseProxy->m_collisionFilterMask) == 0)
						return 1.0;
				}

				return ClosestRayResultCallback::addSingleResult(rayResult, normalInWorldSpace);
			}

		protected:
			btRigidBody* m_pException;
		};

		ClosestExceptForOne rayCallback(m_pBulletVehicle->m_pChassisRigidBody.get(), from, to);
		unsigned int groupMask, collidesWithMask;
		m_pBulletVehicle->GetCollisionMasks(out(groupMask), out(collidesWithMask));
		rayCallback.m_collisionFilterMask = collidesWithMask;
		rayCallback.m_collisionFilterGroup = groupMask;

		BulletUniverse* pUniverse = m_pBulletVehicle->GetCurrentUniverse();
		pUniverse->GetBTWorld()->rayTest(from, to, rayCallback);
		if (rayCallback.hasHit()) {
			const btRigidBody* pRigidBody = btRigidBody::upcast(rayCallback.m_collisionObject);
			if (pRigidBody && pRigidBody->hasContactResponse()) {
				result.m_hitPointInWorld = rayCallback.m_hitPointWorld;
				result.m_hitNormalInWorld = rayCallback.m_hitNormalWorld;
				result.m_hitNormalInWorld.normalize();
				result.m_distFraction = rayCallback.m_closestHitFraction;
				return const_cast<btRigidBody*>(pRigidBody); // btRayCastVehicle API requires this to be non-const even though it doesn't modify the object.
			}
		}

		return nullptr;
	}

private:
	BulletVehicle* m_pBulletVehicle;
};

BulletVehicle::BulletVehicle(BulletFactory& factory) :
		Vehicle(factory), PhysicsBase(factory), Collidable(factory), BulletRigidBody(factory), m_ptCenterOfMass(0, 0, 0) {
}

// Make sure vehicle properties are valid
void BulletVehicle::ValidateVehicleProperties() {
	m_VehicleProperties.m_fAerodynamicDragCoeff = Clamp(m_VehicleProperties.m_fAerodynamicDragCoeff, 0.0f, 10.0f);
	m_VehicleProperties.m_fFrictionDragN = std::max(m_VehicleProperties.m_fFrictionDragN, 0.0f);
	m_VehicleProperties.m_fMassKG = std::max(m_VehicleProperties.m_fMassKG, 0.0f);
	for (size_t i = 0; i < 3; ++i)
		m_VehicleProperties.m_vDimensionsM[i] = std::max(m_VehicleProperties.m_vDimensionsM[i], 0.0f);
	m_VehicleProperties.m_fMaxSpeedKPH = std::max(m_VehicleProperties.m_fMaxSpeedKPH, 0.0f);
	m_VehicleProperties.m_fMaxReverseSpeedKPH = std::max(m_VehicleProperties.m_fMaxReverseSpeedKPH, 0.0f);
	m_VehicleProperties.m_fSuspensionLoadedLength = std::max(m_VehicleProperties.m_fSuspensionLoadedLength, .01f);
	float fMinUnloadedLength = m_VehicleProperties.m_fSuspensionLoadedLength + .01f;
	m_VehicleProperties.m_fSuspensionUnloadedLength = std::max(m_VehicleProperties.m_fSuspensionUnloadedLength, m_VehicleProperties.m_fSuspensionLoadedLength + .01f);
	m_VehicleProperties.m_fSuspensionAttachmentHeight = std::max(m_VehicleProperties.m_fSuspensionAttachmentHeight, 0.01f);
}

bool BulletVehicle::Init(const VehicleProperties& props) {
	m_VehicleProperties = props;
#if 0 // Supercar test settings
	m_VehicleProperties.m_fEngineHorsepower = 1200.0f;
	m_VehicleProperties.m_fMaxSpeedKPH = 600.0f;
	m_VehicleProperties.m_fAerodynamicDragCoeff = 0.2f;
	//m_VehicleProperties.m_fAerodynamicDragCoeff = 0.0f;
	//m_VehicleProperties.m_fFrictionDragN = 0.0f;
	m_VehicleProperties.m_fSuspensionLoadedLength = 0.2f;
	m_VehicleProperties.m_fSuspensionUnloadedLength = 0.3f;
	m_VehicleProperties.m_fBrakeDecelKPHPS = 100.0f;
#elif 0
	m_VehicleProperties.m_vDimensionsM.Set(2.0f, .95f, 4.5f);
	m_VehicleProperties.m_fMaxSpeedKPH = 435.0f;
	m_VehicleProperties.m_fEngineHorsepower = 500.0f;
	m_VehicleProperties.m_fSuspensionLoadedLength = 0.15f;
	m_VehicleProperties.m_fSuspensionUnloadedLength = 0.15f;
	m_VehicleProperties.m_fMassKG = 1360.0f;
#endif
	ValidateVehicleProperties();
	m_ptCenterOfMass = m_VehicleProperties.m_vRelativeMassCenter * m_VehicleProperties.m_vDimensionsM * 0.5f;

	float fMass = m_VehicleProperties.m_fMassKG;

	// Continuous collision detection does not work for compound shapes.
	// Box shape requires its center of mass to be in the center of the box.
	// In order to use both CCD and an offset center of mass, we will use a convex hull shape.
#if 0
	m_pChassisBoxShape = std::make_shared<btBoxShape>( ToBullet(0.5f * m_VehicleProperties.m_vDimensionsM) );
	m_pChassisRootShape = std::make_unique<btCompoundShape>();
	btTransform localTrans;
	localTrans.setIdentity();
	// Comment from Bullet vehicle demo code: localTrans effectively shifts the center of mass with respect to the chassis
	localTrans.setOrigin( ToBullet(-m_ptCenterOfMass) );
	m_pChassisRootShape->addChildShape(localTrans, m_pChassisBoxShape.get());
#else
	Vector3f vMassOffset = 0.5f * m_VehicleProperties.m_vRelativeMassCenter * m_VehicleProperties.m_vDimensionsM;
	Vector3f aptsHull[8];
	float fMaxOffsetFromOriginSquared = 0;
	for (size_t i = 0; i < 8; ++i) {
		Vector3f vMult(
			i & 1 ? 1 : -1,
			i & 2 ? 1 : -1,
			i & 4 ? 1 : -1);
		// Convex hull collision shapes add a collision margin (which defaults to CONVEX_DISTANCE_MARGIN). We subtract it out here so the user gets the expected dimension.
		// (btBoxShape does the same thing internally)
		aptsHull[i] = -vMassOffset + (0.5f * m_VehicleProperties.m_vDimensionsM - Vector3f(CONVEX_DISTANCE_MARGIN, CONVEX_DISTANCE_MARGIN, CONVEX_DISTANCE_MARGIN)) * vMult;
		float fOffsetFromOrigin = aptsHull[i].LengthSquared();
		if (fOffsetFromOrigin > fMaxOffsetFromOriginSquared)
			fMaxOffsetFromOriginSquared = fOffsetFromOrigin;
	}
	float fMaxOffsetFromOrigin = Sqrt(fMaxOffsetFromOriginSquared);
	std::shared_ptr<btConvexHullShape> m_pConvexHullShape;
	m_pConvexHullShape = AlignedNew<btConvexHullShape>(&aptsHull[0][0], std::extent<decltype(aptsHull)>::value, sizeof(aptsHull[0]));
	m_pChassisRootShape = m_pConvexHullShape;
#endif

	btVector3 localInertia(0, 0, 0);
	m_pChassisRootShape->calculateLocalInertia(fMass, localInertia);
	btTransform transformStart = btTransform::getIdentity();
	btTransform transformCenterOfMassOffset = btTransform::getIdentity();
	m_pMotionState = std::make_unique<btDefaultMotionState>(transformStart, transformCenterOfMassOffset);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(fMass, m_pMotionState.get(), m_pChassisRootShape.get(), localInertia);
	rbInfo.m_restitution = m_VehicleProperties.m_fRestitution;
	m_pChassisRigidBody = std::make_unique<btRigidBody>(rbInfo);
	m_pChassisRigidBody->setUserPointer(static_cast<BulletRigidBody*>(this));
	m_pChassisRigidBody->setActivationState(DISABLE_DEACTIVATION);

	// Enable CCD for the vehicle.
	// It seems that bullet does CCD with spheres centered on the object's center of mass.
	// Rather than using the sphere as a quick preliminary test, Bullet seems to use it as the only CCD test.
	// If the sphere collides, the object collides.
	// This is extremely problematic for us, as our center of mass may not even be contained in our convex object, and we can't use an offset sphere.
	// I'm going to edit the bullet source to perform the CCD test using the actual convex shape.
	float fMinDim = *std::min_element(m_VehicleProperties.m_vDimensionsM.begin(), m_VehicleProperties.m_vDimensionsM.end());
	m_pChassisRigidBody->setCcdMotionThreshold(0.5f * fMinDim);
	m_pChassisRigidBody->setCcdSweptSphereRadius(fMaxOffsetFromOrigin);

	// Create vehicle-vehicle collision shape.
	Vector3f vVehicleCollisionHalfDim = 0.5f * m_VehicleProperties.m_vDimensionsM + Vector3f(kfVehicleCollisionBufferDistance);
	m_pOtherVehicleCollisionShape = std::make_unique<btBoxShape>(ToBullet(vVehicleCollisionHalfDim));
	m_pOtherVehicleCollisionObject = std::make_unique<btCollisionObject>();
	m_pOtherVehicleCollisionObject->setCollisionShape(m_pOtherVehicleCollisionShape.get());

	CreateBTVehicle();

	LogInfo("Created vehicle");
	LogInfo("Drive Wheels: " << (m_VehicleProperties.m_DriveWheels == VehicleProperties::DriveWheels::All ? "All" : m_VehicleProperties.m_DriveWheels == VehicleProperties::DriveWheels::Front ? "Front" : "Rear"));
	LogInfo("Power (HP): " << m_VehicleProperties.m_fEngineHorsepower);
	LogInfo("Brake decel (KPH/S): " << m_VehicleProperties.m_fBrakeDecelKPHPS);
	LogInfo("Mass (KG): " << m_VehicleProperties.m_fMassKG);
	LogInfo("Suspension attachment height (M): " << m_VehicleProperties.m_fSuspensionAttachmentHeight);
	LogInfo("Suspension loaded length (M): " << m_VehicleProperties.m_fSuspensionLoadedLength);
	LogInfo("Suspension unloaded length (M): " << m_VehicleProperties.m_fSuspensionUnloadedLength);
	LogInfo("Wheel radius (M): " << m_VehicleProperties.m_fWheelRadiusMeters);
	LogInfo("Max speed (KPH): " << m_VehicleProperties.m_fMaxSpeedKPH);

	return true;
}

void BulletVehicle::GetWheelTransforms(Out<Matrix44f[4]> aWheelTransforms) {
	size_t nWheels = m_pBTVehicle->getNumWheels();

	btMatrix3x3 basis = m_pChassisRigidBody->getWorldTransform().getBasis();
	//Vector3f offset = -FromBullet(basis * ToBullet(m_ptCenterOfMass));
	Vector3f offset(0, 0, 0);
	for (size_t i = 0; i < 4; ++i) {
		if (i < nWheels)
			aWheelTransforms.get()[i] = FromBullet(m_pBTVehicle->getWheelTransformWS(i));
		else
			aWheelTransforms.get()[i].MakeIdentity();
		aWheelTransforms.get()[i].GetTranslation() += offset;
	}
}

bool BulletVehicle::CreateBTVehicle() {
	btRaycastVehicle::btVehicleTuning vehicleTuning; // We set values in the wheels directly, so no need to set them in vehicleTuning.
	m_pVehicleRaycaster = std::make_unique<MyRayCaster>(this);
	m_pBTVehicle = std::make_unique<MyRayCastVehicle>(this, vehicleTuning, m_pChassisRigidBody.get(), m_pVehicleRaycaster.get());

	m_pBTVehicle->setCoordinateSystem(kiRightAxisIndex, kiUpAxisIndex, kiForwardAxisIndex);
#if 1
	// Create basic wheels, then set their properties in the call to UpdateBTVehicleWithCurrentProps
	Vector3f vAxleDir(-1, 0, 0);
	Vector3f vWheelDir(0, -1, 0);
	for (size_t i = 0; i < 4; ++i) {
		btWheelInfo& wheelInfo = m_pBTVehicle->addWheel(
			btVector3(0, 0, 0),
			ToBullet(vWheelDir),
			ToBullet(vAxleDir),
			0,
			1,
			vehicleTuning,
			i < 2);
	}

	// Update wheels with proper values.
	UpdateBTVehicleWithCurrentProps();
#else
	float fWheelRadius = m_VehicleProperties.m_fWheelRadiusMeters;
	//float fWheelThickness = m_VehicleProperties.m_fWheelThicknessMeters;
	//static const float fSuspensionRestLength = 1.0f;
	//static const float fSuspensionStiffness = 15.0f; // Acceleration per meter of suspension compression.
	float fMaxSuspensionAccelerationG = 10; // 10 G
	float fMaxSuspensionAcceleration = 9.8f * fMaxSuspensionAccelerationG; // 10 G
	float fMaxSuspensionForce = m_VehicleProperties.m_fMassKG * fMaxSuspensionAcceleration;

	float fSuspensionRestLength = m_VehicleProperties.m_fSuspensionUnloadedLength;
	float fSuspensionCompressionLoaded = m_VehicleProperties.m_fSuspensionUnloadedLength - m_VehicleProperties.m_fSuspensionLoadedLength;
	// Note: Bullet scales the stiffness by vehicle mass.
	float fSuspensionStiffness = 0.25f * 9.8f / fSuspensionCompressionLoaded;
	float fMaxSuspensionTravelCm = fSuspensionCompressionLoaded * fMaxSuspensionAccelerationG * 100.0f;

	// Wheels
	const Vector3f& vChassisDim = m_VehicleProperties.m_vDimensionsM;
	Vector3f vWheelOffset(0.5f * vChassisDim.X(), -0.5f * vChassisDim.Y() + m_VehicleProperties.m_fSuspensionAttachmentHeight, 0.5f * vChassisDim.Z() - fWheelRadius - .1f);
	m_avWheelOffsets[0].Set(-vWheelOffset.X(), vWheelOffset.Y(), vWheelOffset.Z()); // Left front
	m_avWheelOffsets[1].Set(vWheelOffset.X(), vWheelOffset.Y(), vWheelOffset.Z()); // Right front
	m_avWheelOffsets[2].Set(-vWheelOffset.X(), vWheelOffset.Y(), -vWheelOffset.Z()); // Left rear
	m_avWheelOffsets[3].Set(vWheelOffset.X(), vWheelOffset.Y(), -vWheelOffset.Z()); // Right rear

	// (0,0,1) is the forward direction.
	Vector3f vAxleDir(-1, 0, 0);
	Vector3f vWheelDir(0, -1, 0);
	for (size_t i = 0; i < 4; ++i) {
		btWheelInfo& wheelInfo = m_pBTVehicle->addWheel(
			ToBullet(m_avWheelOffsets[i] - m_ptCenterOfMass),
			ToBullet(vWheelDir),
			ToBullet(vAxleDir),
			fSuspensionRestLength,
			fWheelRadius,
			vehicleTuning,
			i < 2);

		wheelInfo.m_suspensionStiffness = fSuspensionStiffness;
		wheelInfo.m_wheelsDampingRelaxation = m_VehicleProperties.m_fSuspensionDampingRelaxation;
		wheelInfo.m_wheelsDampingCompression = m_VehicleProperties.m_fSuspensionDampingCompression;
		wheelInfo.m_frictionSlip = m_VehicleProperties.m_fWheelFriction;
		wheelInfo.m_rollInfluence = m_VehicleProperties.m_fRollFactor;
		wheelInfo.m_maxSuspensionForce = fMaxSuspensionForce;
		wheelInfo.m_maxSuspensionTravelCm = fMaxSuspensionTravelCm;
	}
#endif

	return true;
}

void BulletVehicle::CalculateControlDurations(float fStep, Out<BulletVehicleControlApplication> controlApplication) {
	BulletUniverse* pUniverse = GetCurrentUniverse();
	double dEndInputTime = pUniverse->GetRealTimeForInputProcessing();
	double dStartInputTime = dEndInputTime - fStep;
	auto itr = m_VehicleControl.m_aCommands.begin();

	// Events occurring before start of this step take effect immediately.
	for (; itr != m_VehicleControl.m_aCommands.end() && itr->m_dTime <= dStartInputTime; ++itr) {
		if (itr->m_Type < std::extent<decltype(m_VehicleControl.m_aControlActivated)>::value)
			m_VehicleControl.m_aControlActivated[itr->m_Type] = itr->m_bActivate;
		//if( dStartInputTime > itr->m_dTime + .0001 )
		//	LogInfo("Missed input at " << std::fixed << std::setprecision(4) << dStartInputTime);
	}

	// Initialize activation duration to zero.
	float afControlDuration[eVehicleControlType::Count];
	for (size_t i = 0; i < eVehicleControlType::Count; ++i)
		afControlDuration[i] = m_VehicleControl.m_aControlActivated[i] ? fStep : 0.0f;

	// Events occurring during this step have a partial effect
	for (; itr != m_VehicleControl.m_aCommands.end() && itr->m_dTime <= dEndInputTime; ++itr) {
		if (itr->m_bActivate == m_VehicleControl.m_aControlActivated[itr->m_Type])
			continue; // No change in state.
		m_VehicleControl.m_aControlActivated[itr->m_Type] = itr->m_bActivate;
		double dDurationUntilEnd = dEndInputTime - itr->m_dTime;
		if (itr->m_bActivate)
			afControlDuration[itr->m_Type] += dDurationUntilEnd;
		else
			afControlDuration[itr->m_Type] -= dDurationUntilEnd;
	}

	// erase processed events.
	m_VehicleControl.m_aCommands.erase(m_VehicleControl.m_aCommands.begin(), itr);
	m_VehicleControl.m_aControlActivated[eVehicleControlType::TurboBoost] = false; // Deactivate turbo boost after each activation.

	// Apply durations.
	// We would get more precise control by calculating control forces at each control event change time, but calculating
	// at the end of each step is probably good enough for a first pass.
	controlApplication.get().m_fSideDuration = afControlDuration[eVehicleControlType::Right] - afControlDuration[eVehicleControlType::Left];
	controlApplication.get().m_fForwardDuration = afControlDuration[eVehicleControlType::Forward] - afControlDuration[eVehicleControlType::Reverse];
	controlApplication.get().m_fBrakeDuration = afControlDuration[eVehicleControlType::Brake];
	controlApplication.get().m_bTurboActivated = afControlDuration[eVehicleControlType::TurboBoost] > 0;
}

void BulletVehicle::ApplyForces(float fStep, Out<bool> bBraking) {
	if (fStep <= 0)
		return;

	BulletVehicleControlApplication controlApplication;
	CalculateControlDurations(fStep, out(controlApplication));
	float fSideDuration = controlApplication.m_fSideDuration;
	float fForwardDuration = controlApplication.m_fForwardDuration;
	float fBrakeDuration = controlApplication.m_fBrakeDuration;
	bool bTurboBoost = controlApplication.m_bTurboActivated;

	if (fBrakeDuration > 0.0f) {
		// Disable turbo boost and engine power when brake is applied. Leave steering enabled.
		bTurboBoost = false;
		fForwardDuration = 0.0f;
	}

	// Determine whether TurboBoost is allowed to be activated.
	BulletUniverse* pUniverse = GetCurrentUniverse();
	double dEndInputTime = pUniverse->GetRealTimeForInputProcessing();
	if (bTurboBoost) {
		size_t nWheelsInContact = NumberOfWheelsInContactWithGround();
		if (nWheelsInContact < 2)
			bTurboBoost = false;

		if (dEndInputTime < m_VehicleControl.m_dTurboLastActivateTime + m_VehicleProperties.m_fTurboUsageFrequency)
			bTurboBoost = false;
	}
	if (bTurboBoost)
		m_VehicleControl.m_dTurboLastActivateTime = dEndInputTime;

	float fInverseStep = 1.0f / fStep;

	Vector3f vForward = FromBullet(m_pChassisRigidBody->getWorldTransform().getBasis().getColumn(kiForwardAxisIndex));
	Vector3f vUp = FromBullet(m_pChassisRigidBody->getWorldTransform().getBasis().getColumn(kiUpAxisIndex));

	Vector3f vVelocity = FromBullet(m_pChassisRigidBody->getLinearVelocity());
	Vector3f vVelocityDir = vVelocity.GetNormalized();
	float fSpeed = std::abs(vVelocity.Dot(vVelocityDir)); // Absolute value to counter roundoff making it negative.
	float fForwardVelocity = vVelocity.Dot(vForward);

	if (fForwardVelocity * fForwardDuration < 0 && fabs(fForwardVelocity) > 1.0f) {
		// Engine power being applied opposite of motion. Convert to brake.
		fBrakeDuration = std::abs(fForwardDuration);
		fForwardDuration = 0;
	}

	// Steering rate: Change in steering angle when control is applied in radians per second.
#if 1
	const float fSteerRate = DegreesToRadians(60.0f);
#else
	const float fSteerRateLowSpeed = DegreesToRadians(30.0f);
	const float fSteerLowSpeed = KPHToMPS(20.0f);
	const float fSteerRateHighSpeed = DegreesToRadians(10.0f);
	const float fSteerHighSpeed = KPHToMPS(240.0f);
	float fSteerRate = InterpolateLinearClamped(fSteerRateLowSpeed, fSteerRateHighSpeed, fSteerLowSpeed, fSteerHighSpeed, fSpeed);
	float fSteerRate = InterpolateLinearClamped(fSteerLowSpeed, fSteerHighSpeed, fSpeed, fSteerRateLowSpeed, fSteerRateHighSpeed);
#endif
	const float fMaxSteer = DegreesToRadians(30.0f); // Max steering angle in radians.

	// Rate at which steering reverts to neutral when no control applied in radians per second.
#if 1
	const float fSteerRelaxRateLow = DegreesToRadians(30.0f);
	const float fSteerRelaxRateHigh = DegreesToRadians(60.0f);
	const float fSteerRelaxLowSpeed = KPHToMPS(10.0f);
	const float fSteerRelaxHighSpeed = KPHToMPS(40.0f);
#else
	const float fSteerRelaxRateLow = DegreesToRadians(30.0f);
	const float fSteerRelaxRateHigh = DegreesToRadians(60.0f);
	const float fSteerRelaxLowSpeed = KPHToMPS(10.0f);
	const float fSteerRelaxHighSpeed = KPHToMPS(40.0f);
#endif
	float fSteerRelaxRate = InterpolateLinearClamped(fSteerRelaxRateLow, fSteerRelaxRateHigh, fSpeed, fSteerRelaxLowSpeed, fSteerRelaxHighSpeed);
	float fCurrentSteer = m_pBTVehicle->getSteeringValue(0);
	float fAbsCurrentSteer = std::abs(fCurrentSteer);
	float fNoSideDuration = fStep - std::abs(fSideDuration); // Duration that NO sideways force is applied. We allow the wheels to straighten during this period.
	float fNewSteer;

	// Apply steering relaxation
	float fRelaxation = fNoSideDuration * fSteerRelaxRate;
	if (fRelaxation >= fAbsCurrentSteer)
		fNewSteer = 0;
	else
		fNewSteer = std::copysign(fAbsCurrentSteer - fRelaxation, fCurrentSteer);

	// Apply steering control
	fNewSteer += fSideDuration * fSteerRate;
	if (std::abs(fNewSteer) > fMaxSteer)
		fNewSteer = std::copysign(fMaxSteer, fNewSteer);

	// Update vehicle steering.
	m_pBTVehicle->setSteeringValue(fNewSteer, 0);
	m_pBTVehicle->setSteeringValue(fNewSteer, 1);
#if 0
	// Apply steering torque
	static float fSteeringTorque = 10000.0f;
	float fSteeringTorqueToApply = fSteeringTorque * fSideDuration;
	if( std::abs(fSteeringTorqueToApply) > 0.1f )
		m_pChassisRigidBody->applyTorqueImpulse(vForward * -fSteeringTorqueToApply);
#endif

	float fMass = m_VehicleProperties.m_fMassKG;
	float fInvMass = m_pChassisRigidBody->getInvMass();
	// Apply mechanical and aerodynamic drag
	float fDragForce = 0;
	if (fSpeed > 0) {
		const float fAirDensity = 1.184f; // In kg/m^3 at 25 degrees Celsius. (77F)
		float fDragCoefficient = m_VehicleProperties.m_fAerodynamicDragCoeff;
		const Vector3f& vVehicleDim = m_VehicleProperties.m_vDimensionsM;
		float fFrontalArea = vVehicleDim.X() * vVehicleDim.Y();
		// Drag equation: 1/2 * density * v^2 * area * drag_coeff
		float fAeroDragForce = 0.5f * fAirDensity * Square(fSpeed) * fFrontalArea * fDragCoefficient;
		float fMechanicalDragForce = m_VehicleProperties.m_fFrictionDragN; // Constant force opposing motion. TODO: Apply this through the wheels instead of center of mass.
		fDragForce = fAeroDragForce + fMechanicalDragForce;
		float fDragDeceleration = fDragForce * fInvMass;
		float fDragDeltaVelocity = fDragDeceleration * fStep;
		float fRelativeDragDeltaVel = fDragDeltaVelocity / fSpeed;
		if (fRelativeDragDeltaVel > 1)
			fRelativeDragDeltaVel = 1;
		Vector3f vDragDeltaVelocity = -fRelativeDragDeltaVel * vVelocity;
		m_pChassisRigidBody->applyCentralImpulse(fMass * ToBullet(vDragDeltaVelocity));

		// Update velocity variables after applying drag.
		vVelocity = FromBullet(m_pChassisRigidBody->getLinearVelocity());
		fSpeed = std::abs(vVelocity.Dot(vVelocityDir));
	}

	// Apply engine force
	float fEnginePower = HorsepowerToWatts(m_VehicleProperties.m_fEngineHorsepower);
	float fEngineWork = fEnginePower * fForwardDuration; // power * time.  Will be negative if net power is applied in the reverse direction.
	float fForwardSpeed = vVelocity.Dot(vForward);
	float fCurrentFwdEnergy = 0.5f * fMass * Square(fForwardSpeed); // 1/2 * m * v^2
	float fSignedCurrentFwdEnergy = std::copysign(fCurrentFwdEnergy, fForwardSpeed); // Use signed energy. Negative is moving backwards.
	float fSignedNewFwdEnergy = fSignedCurrentFwdEnergy + fEngineWork;

	// Validate new energy
	float fMaxForwardSpeed = KPHToMPS(m_VehicleProperties.m_fMaxSpeedKPH);
	float fMaxReverseSpeed = KPHToMPS(m_VehicleProperties.m_fMaxReverseSpeedKPH);
	float fMaxReverseEnergy = -0.5f * fMass * Square(fMaxReverseSpeed);
	float fMaxForwardEnergy = 0.5f * fMass * Square(fMaxForwardSpeed);
	fMaxReverseEnergy = std::min(fMaxReverseEnergy, fSignedCurrentFwdEnergy);
	fMaxForwardEnergy = std::max(fMaxForwardEnergy, fSignedCurrentFwdEnergy);
	fSignedNewFwdEnergy = Clamp(fSignedNewFwdEnergy, fMaxReverseEnergy, fMaxForwardEnergy);

	// Calculate force to attain this energy
	float fNewSpeedSquared = 2 * std::abs(fSignedNewFwdEnergy) * fInvMass;
	float fNewSpeed = std::copysign(Sqrt(fNewSpeedSquared), fSignedNewFwdEnergy);
	float fForwardAcceleration = fStep > 0 ? (fNewSpeed - fForwardSpeed) * fInverseStep : 0;
	const float kfMinAcceleration = .01f; // Anything less than this is clamped to zero.
	if (std::abs(fForwardAcceleration) < kfMinAcceleration)
		fForwardAcceleration = 0.0f;
	float fForwardForce = fForwardAcceleration * fMass;
	float fEngineForce = fForwardForce;

	// Calculate braking force to apply.
	float fBrakeDecel = (fBrakeDuration * fInverseStep) * KPHToMPS(m_VehicleProperties.m_fBrakeDecelKPHPS);
	float fBrakeForce = fBrakeDecel * fMass;

	if (bTurboBoost) {
		const Vector3f& vTurboX = vForward;
		//const Vector3f& vTurboY = vUp;
		// This removes any vehicle roll when determining the direction to use for the Y vector.
		Vector3f vTurboY = (vUp - vUp.GetPartParallelTo(vForward.Cross(Vector3f(0, 1, 0)))).GetNormalized();
		Vector3f vTurbo = vTurboX * m_VehicleProperties.m_vTurboVelocityMPS.X() + vTurboY * m_VehicleProperties.m_vTurboVelocityMPS.Y();
		m_pChassisRigidBody->applyCentralImpulse(ToBullet(vTurbo * fMass));
		fEngineForce = 0;
		fBrakeForce = 0;
	} else if (std::abs(fEngineForce) >= fBrakeForce && fBrakeDuration < fStep * (1 - 8 * FLT_EPSILON)) // Brake always overrides engine if held down entire step, even if engine is more powerful.
	{
		fEngineForce -= std::copysign(fBrakeForce, fEngineForce);
		fBrakeForce = 0;
	} else {
		fEngineForce = 0;
		fBrakeForce -= std::abs(fEngineForce);
		// Don't use much more brake force than necessary.
		// There is significant instability in btRaycastVehicle's brake application that is visible at low velocity.
		// Instability seems reduced after a number of other bug fixes and workarounds.
		//float fMaxDeceleration = fSpeed * fInverseStep;
		//float fMaxBrakeForce = fMaxDeceleration * fMass;
		//if( fBrakeForce > fMaxBrakeForce )
		//	fBrakeForce = fMaxBrakeForce;
	}

	// Limit the force applied to the wheels to reduce flipping of body.
	float fGravity = pUniverse->GetGravityMagnitude(); // Assumes gravity is in -Y direction.
	float fWheelAttachmentY = m_avWheelOffsets[WheelIndex_LeftFront].Y(); // Assumes all equal radius wheels. Otherwise we will need a weighted average based on CoM position relative to wheels.
	Vector3f v3FrontWheelAttachLocal = (0.5f * (m_avWheelOffsets[WheelIndex_LeftFront] + m_avWheelOffsets[WheelIndex_RightFront]));
	Vector3f v3RearWheelAttachLocal = (0.5f * (m_avWheelOffsets[WheelIndex_LeftRear] + m_avWheelOffsets[WheelIndex_RightRear]));
	// Note: suspension lengths are from previous frame. We would need to modify or re-implement btVehicle to grab the values for this frame.
	float fSuspensionLengthFront = 0.5 * (m_pBTVehicle->getWheelInfo(WheelIndex_LeftFront).m_raycastInfo.m_suspensionLength + m_pBTVehicle->getWheelInfo(WheelIndex_RightFront).m_raycastInfo.m_suspensionLength);
	float fSuspensionLengthRear = 0.5 * (m_pBTVehicle->getWheelInfo(WheelIndex_LeftRear).m_raycastInfo.m_suspensionLength + m_pBTVehicle->getWheelInfo(WheelIndex_RightRear).m_raycastInfo.m_suspensionLength);
	Vector3f v3FrontWheelContactLocal = v3FrontWheelAttachLocal - Vector3f(0, m_VehicleProperties.m_fWheelRadiusMeters + fSuspensionLengthFront, 0);
	Vector3f v3RearWheelContactLocal = v3RearWheelAttachLocal - Vector3f(0, m_VehicleProperties.m_fWheelRadiusMeters + fSuspensionLengthRear, 0);
	float fSuspensionSpringFactor = fMass / (m_VehicleProperties.m_fSuspensionUnloadedLength - m_VehicleProperties.m_fSuspensionLoadedLength);
	float fSuspensionCompressionFront = fSuspensionLengthFront - m_VehicleProperties.m_fSuspensionUnloadedLength;
	float fSuspensionCompressionRear = fSuspensionLengthRear - m_VehicleProperties.m_fSuspensionUnloadedLength;
	float fSuspensionForceFront = fSuspensionCompressionFront * fSuspensionSpringFactor;
	float fSuspensionForceRear = fSuspensionCompressionRear * fSuspensionSpringFactor;
	Matrix44f mtxWorldTransform = FromBullet(m_pChassisRigidBody->getWorldTransform());
	Vector3f v3FrontWheelContact = TransformPoint(mtxWorldTransform, v3FrontWheelContactLocal);
	Vector3f v3RearWheelContact = TransformPoint(mtxWorldTransform, v3RearWheelContactLocal);
	Vector3f v3CenterOfMass = TransformPoint(mtxWorldTransform, m_ptCenterOfMass);
	Vector3f v3FrontForceOffset = v3FrontWheelContact - v3CenterOfMass;
	Vector3f v3RearForceOffset = v3RearWheelContact - v3CenterOfMass;
	Vector2f v2FrontForceOffset(Sqrt(Square(v3FrontForceOffset.X()) + Square(v3FrontForceOffset.Z())), v3FrontForceOffset.Y());
	Vector2f v2RearForceOffset(Sqrt(Square(v3RearForceOffset.X()) + Square(v3RearForceOffset.Z())), v3RearForceOffset.Y());

	float fWheelsForce = fEngineForce != 0 ? fEngineForce : vVelocityDir.Dot(vForward) > 0 ? -fBrakeForce : fBrakeForce;
	float fMaxWheelsForce = 8 * fGravity * m_VehicleProperties.m_fMassKG; // 8G acceleration per wheel max.
	if (fWheelsForce != 0) {
		float fGravityTorque, fWheelsTorque;
		Vector3f vForwardOnFlat(vForward.X(), 0, vForward.Z());
		vForwardOnFlat.Normalize();
		float fLiftFactor = vUp.Dot(vForwardOnFlat);
		if (fWheelsForce > 0) {
			// Accelerating
			fGravityTorque = fGravity * fMass * v2RearForceOffset.X();
			fWheelsTorque = -fWheelsForce * v2RearForceOffset.Y();
			fLiftFactor = -fLiftFactor;
		} else {
			// Decelerating
			fGravityTorque = fGravity * fMass * v2FrontForceOffset.X();
			fWheelsTorque = fWheelsForce * v2FrontForceOffset.Y();
		}
		static const float fLiftLimit = .5f; // Cosine of the lift angle.
		static const float fMultiplierLow = 0.9f;
		static const float fMultiplierHigh = 2.0f;
		float fTorqueFactor = InterpolateLinearClamped(fMultiplierHigh, fMultiplierLow, fLiftFactor, 0.0f, fLiftLimit); // Weight gravity torque more at favorable angles.
		if (fWheelsTorque > 0) {
			float fForceFactor = fTorqueFactor * fGravityTorque / fWheelsTorque;
			fMaxWheelsForce = std::abs(fWheelsForce) * fForceFactor;
		}
	}

	if (fEngineForce > fMaxWheelsForce)
		fEngineForce = fMaxWheelsForce;
	else if (fEngineForce < -fMaxWheelsForce)
		fEngineForce = -fMaxWheelsForce;
	if (fBrakeForce > fMaxWheelsForce)
		fBrakeForce = fMaxWheelsForce;

#if 0
	LogInfo("Speed: " << fSpeed << "mps " << KPHToMPH(MPSToKPH(fSpeed)) << "MPH  Applying force: " << fEngineForce << " , " << fBrakeForce << "  Max: " << fMaxWheelsForce
		<< " Angle: " << RadiansToDegrees(asin(vForward.Y())) << " Drag force: " << fDragForce
		//<< " Accel: " << MPSToMPH(std::abs(fWheelsForce) / fMass) << "MPH/S"
		<< " Accel: " << std::max(std::abs(fWheelsForce), std::abs(fEngineForce)) / fMass << "MPS/S"
		);
#endif

	// Divide the force among the four wheels.
	float fWheelEngineForce = fEngineForce;
	if (m_VehicleProperties.m_DriveWheels == VehicleProperties::DriveWheels::All)
		fWheelEngineForce *= 0.25f;
	else
		fWheelEngineForce *= 0.5f;
	float fWheelBrakeForce = fBrakeForce * 0.25f * fStep; // Multiply by step since Bullet treats brake value as an impulse, not a force.
	for (size_t iWheel = 0; iWheel < 4; ++iWheel) {
		float fThisWheelEngineForce = fWheelEngineForce;
		if (iWheel < 2 && m_VehicleProperties.m_DriveWheels == VehicleProperties::DriveWheels::Rear)
			fThisWheelEngineForce = 0;
		else if (iWheel >= 2 && m_VehicleProperties.m_DriveWheels == VehicleProperties::DriveWheels::Front)
			fThisWheelEngineForce = 0;
		m_pBTVehicle->applyEngineForce(fThisWheelEngineForce, iWheel);
		m_pBTVehicle->setBrake(fWheelBrakeForce, iWheel);
	}

	bBraking.set(std::abs(fBrakeForce) > 0);

	HandleVehicleVehicleCollisions(fStep);
}

void BulletVehicle::HandleVehicleVehicleCollisions(float fStep) {
	if (this->m_VehicleProperties.m_iOtherVehicleCollisionGroups != 0 && kfVehicleCollisionBufferDistance > 0) {
		unsigned int otherVehicleGroups = this->m_VehicleProperties.m_iOtherVehicleCollisionGroups;

		// Align box with vehicle chassis.
		btTransform boxToWorldTransform = m_pChassisRigidBody->getWorldTransform();
		boxToWorldTransform.setOrigin(boxToWorldTransform.getOrigin() - boxToWorldTransform.getBasis() * ToBullet(m_ptCenterOfMass));
		m_pOtherVehicleCollisionObject->setWorldTransform(boxToWorldTransform);
		btTransform boxToLocalTransform = boxToWorldTransform.inverse();

		unsigned int iGroupMask = this->GetCollisionGroupMask();
		unsigned int iCollidesWithMask = this->GetCollidesWithMask();
		auto CollisionFilter = [this, otherVehicleGroups](const btCollisionObject* pBTCollisionObject) -> bool {
			const btBroadphaseProxy* pProxy = pBTCollisionObject->getBroadphaseHandle();
			if (!(pProxy->m_collisionFilterGroup & otherVehicleGroups))
				return false;
			if (pBTCollisionObject == this->m_pChassisRigidBody.get())
				return false;
			return true;
		};
		BulletUniverse* pUniverse = GetCurrentUniverse();
		ContactTestResult contactTestResult = pUniverse->ContactTest(m_pOtherVehicleCollisionObject.get(), 0, CollisionFilter);

		// Sort contacts by object and distance so we can process each object only once at the point of maximum penetration.
		std::sort(contactTestResult.m_aContacts.begin(), contactTestResult.m_aContacts.end(),
			[](const ContactTestResult::Contact& left, const ContactTestResult::Contact& right) {
				if (left.m_pObject != right.m_pObject)
					return left.m_pObject < right.m_pObject;
				return left.m_fDistance < right.m_fDistance;
			});

		Vector3f ptMax = 0.5f * m_VehicleProperties.m_vDimensionsM;
		Vector3f ptMin = -ptMax;
		const btCollisionObject* pPrevObj = nullptr;
		for (const ContactTestResult::Contact& contact : contactTestResult.m_aContacts) {
			if (contact.m_pObject == pPrevObj)
				continue; // Only process largest penetration.

			pPrevObj = contact.m_pObject;
			BulletRigidBody* pBulletRigidBody = FromBullet(contact.m_pObject);
			if (!pBulletRigidBody || !(pBulletRigidBody->GetCollisionGroupMask() & otherVehicleGroups))
				continue; // Not a vehicle

			Vector3f vPenetration = contact.m_fDistance * contact.m_vNormalOnOtherObject;
			Vector3f ptContactLocal = FromBullet(boxToLocalTransform * ToBullet(contact.m_ptOnOtherObject));
			Vector3f vContactDirectionToObject;
			float fDistSq = DistanceSquaredBoxToPoint(ptContactLocal, ptMin, ptMax, &vContactDirectionToObject);
			if (fDistSq >= Square(kfVehicleCollisionBufferDistance))
				continue; // Not close enough.

			float fRelativeAccel = (kfVehicleCollisionBufferDistance - Sqrt(fDistSq)) / kfVehicleCollisionBufferDistance;
			float fAccel = kfVehicleCollisionMaxAccel * fRelativeAccel * fStep;
			btVector3 vDirectionToObjectWorld = boxToWorldTransform.getBasis() * ToBullet(vContactDirectionToObject.GetNormalized());
			btVector3 vAccel = vDirectionToObjectWorld * (-fAccel * m_VehicleProperties.m_fMassKG);
			m_pChassisRigidBody->applyCentralImpulse(vAccel);
		}
	}
}

void BulletVehicle::PostUpdate(float fStep) {
}

float BulletVehicle::GetSkidFactor() {
	float fSkidForce = 0;
	int nWheels = m_pBTVehicle->getNumWheels();
	for (size_t i = 0; i < nWheels; ++i) {
		// m_skidInfo is 1 for no skidding and approaches zero for nothing but skidding, so it's more of a
		// measure of grip than skid.
		const btWheelInfo& wheelInfo = m_pBTVehicle->getWheelInfo(i);
		float fThisWheelGrip = wheelInfo.m_skidInfo;
		float fThisWheelDownForce = wheelInfo.m_wheelsSuspensionForce;
		float fThisWheelSkid = 1 - fThisWheelGrip;
		fSkidForce += fThisWheelSkid * fThisWheelDownForce;
	}
	BulletUniverse* pUniverse = GetCurrentUniverse();
	float fGravity = pUniverse->GetGravityMagnitude();
	float fRelativeSkid = fSkidForce / (m_VehicleProperties.m_fMassKG * fGravity);
	return Clamp(fRelativeSkid, 0, 1);
}

size_t BulletVehicle::NumberOfWheelsInContactWithGround() const {
	size_t nWheelsInContact = 0;
	for (int i = 0; i < 4; ++i) {
		const btWheelInfo& wheelInfo = m_pBTVehicle->getWheelInfo(i);
		if (wheelInfo.m_raycastInfo.m_isInContact)
			++nWheelsInContact;
	}
	return nWheelsInContact;
}

void BulletVehicle::AddingToUniverse(BulletUniverse* pUniverse) {
	BulletRigidBody::AddingToUniverse(pUniverse);
}

btRigidBody* BulletVehicle::GetBTRigidBody() const {
	return m_pChassisRigidBody.get();
}

btActionInterface* BulletVehicle::GetBTVehicle() {
	return m_pBTVehicle.get();
}

btCollisionObject* BulletVehicle::GetBTCollisionObject() const {
	return m_pChassisRigidBody.get();
}

const VehicleProperties& BulletVehicle::GetVehicleProperties() {
	return m_VehicleProperties;
}

void BulletVehicle::UpdateVehicleProperties(const VehicleProperties& props) {
	// Change all properties except for center of mass and dimensions.
	// These should be updated, but is more complicated and not currently needed.
	Vector3f vOldMassCenter = m_VehicleProperties.m_vRelativeMassCenter;
	Vector3f vOldDim = m_VehicleProperties.m_vDimensionsM;
	m_VehicleProperties = props;
	m_VehicleProperties.m_vRelativeMassCenter = vOldMassCenter;
	m_VehicleProperties.m_vDimensionsM = vOldDim;

	// Fix bad property values.
	ValidateVehicleProperties();

	// Apply changes.
	m_pChassisRigidBody->setRestitution(m_VehicleProperties.m_fRestitution);
	m_pChassisRigidBody->setMassProps(m_VehicleProperties.m_fMassKG, m_pChassisRigidBody->getLocalInertia());
	UpdateBTVehicleWithCurrentProps();
}

void BulletVehicle::UpdateBTVehicleWithCurrentProps() {
	float fMaxSuspensionAccelerationG = 10; // 10 G
	float fMaxSuspensionAcceleration = 9.8f * fMaxSuspensionAccelerationG; // 10 G
	float fMaxSuspensionForce = m_VehicleProperties.m_fMassKG * fMaxSuspensionAcceleration;
	float fSuspensionRestLength = m_VehicleProperties.m_fSuspensionUnloadedLength;
	float fSuspensionCompressionLoaded = m_VehicleProperties.m_fSuspensionUnloadedLength - m_VehicleProperties.m_fSuspensionLoadedLength;
	float fWheelRadius = m_VehicleProperties.m_fWheelRadiusMeters;
	// Note: Bullet scales the stiffness by vehicle mass.
	float fSuspensionStiffness = 0.25f * 9.8f / fSuspensionCompressionLoaded;
	float fMaxSuspensionTravelCm = fSuspensionCompressionLoaded * fMaxSuspensionAccelerationG * 100.0f;

	// Wheels
	const Vector3f& vChassisDim = m_VehicleProperties.m_vDimensionsM;
	Vector3f vWheelOffset(0.5f * vChassisDim.X(), -0.5f * vChassisDim.Y() + m_VehicleProperties.m_fSuspensionAttachmentHeight, 0.5f * vChassisDim.Z() - fWheelRadius - .1f);
	m_avWheelOffsets[0].Set(-vWheelOffset.X(), vWheelOffset.Y(), vWheelOffset.Z()); // Left front
	m_avWheelOffsets[1].Set(vWheelOffset.X(), vWheelOffset.Y(), vWheelOffset.Z()); // Right front
	m_avWheelOffsets[2].Set(-vWheelOffset.X(), vWheelOffset.Y(), -vWheelOffset.Z()); // Left rear
	m_avWheelOffsets[3].Set(vWheelOffset.X(), vWheelOffset.Y(), -vWheelOffset.Z()); // Right rear

	for (size_t i = 0; i < 4; ++i) {
		btWheelInfo& wheelInfo = m_pBTVehicle->getWheelInfo(i);
		wheelInfo.m_chassisConnectionPointCS = ToBullet(m_avWheelOffsets[i] - m_ptCenterOfMass);
		wheelInfo.m_suspensionStiffness = fSuspensionStiffness;
		wheelInfo.m_wheelsDampingRelaxation = m_VehicleProperties.m_fSuspensionDampingRelaxation;
		wheelInfo.m_wheelsDampingCompression = m_VehicleProperties.m_fSuspensionDampingCompression;
		wheelInfo.m_frictionSlip = m_VehicleProperties.m_fWheelFriction;
		wheelInfo.m_rollInfluence = m_VehicleProperties.m_fRollFactor;
		wheelInfo.m_maxSuspensionForce = fMaxSuspensionForce;
		wheelInfo.m_maxSuspensionTravelCm = fMaxSuspensionTravelCm;
		wheelInfo.m_suspensionRestLength1 = fSuspensionRestLength;
		wheelInfo.m_wheelsRadius = fWheelRadius;
	}
}

bool BulletVehicle::SetTransformMetric(const Matrix44f& transform, bool bIgnoreScale) // Note: Matrix must not be scaled for vehicles.
{
	if (!IsRigidTransform(transform))
		return false;

	Matrix44f adjustedTransform;
	PreTranslate(transform, m_ptCenterOfMass, out(adjustedTransform));
	btTransform bulletTransform = ToBullet(adjustedTransform);
	m_pChassisRigidBody->setCenterOfMassTransform(bulletTransform);
	m_pMotionState->setWorldTransform(bulletTransform);
	m_pChassisRigidBody->setLinearVelocity(btVector3(0, 0, 0));
	m_pChassisRigidBody->setAngularVelocity(btVector3(0, 0, 0));
	m_pChassisRigidBody->setInterpolationLinearVelocity(btVector3(0, 0, 0));
	m_pChassisRigidBody->setInterpolationAngularVelocity(btVector3(0, 0, 0));

	if (GetCurrentUniverse())
		GetCurrentUniverse()->ReconfiguredRigidBody(this);

	return true;
}

void BulletVehicle::ApplyControl(const VehicleControlCommand& cmd) {
	auto Compare = [](const VehicleControlCommand& left, const VehicleControlCommand& right) { return left.m_dTime < right.m_dTime; };
	auto itr = std::upper_bound(m_VehicleControl.m_aCommands.end(), m_VehicleControl.m_aCommands.end(), cmd, Compare);
	m_VehicleControl.m_aCommands.insert(itr, cmd);
}

void BulletVehicle::ClearControls() {
	m_VehicleControl.Clear();
}

float BulletVehicle::GetSpeedKPH() {
	return m_pBTVehicle->getCurrentSpeedKmHour();
}

void BulletVehicle::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_pChassisRigidBody);
		pMemSizeGadget->AddObject(m_pOtherVehicleCollisionShape);
		pMemSizeGadget->AddObject(m_pOtherVehicleCollisionObject);
	}
	/*----#MLB_TODO_IMemSizeGadget In Progress ------------------------------------ 
	m_pVehicleRaycaster					// std::unique_ptr<MyRayCaster>
	m_pBTVehicle                       	// std::unique_ptr<MyRayCastVehicle>
	m_pMotionState                      // std::unique_ptr<btMotionState> 			
	m_pChassisRootShape                 // std::shared_ptr<btConvexHullShape> 		
	m_VehicleControl                   	// BulletVehicleControl 					
	-------------------------------------------------------------------------------*/
}

} // namespace Physics
} // namespace KEP
