///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BulletAvatar.h"
#include "BulletCast.h"
#include "BulletFactory.h"
#include "BulletUniverse.h"
#include "Core/Math/Interpolate.h"
#include "Core/Math/MinimalBoundingSphere.h"

#include "../../Common/include/IMemSizeGadget.h"
#include "../../Common/include/LogHelper.h"
static LogInstance("Instance");

static int kiMoveMethod = 3;

// A state of collision with an object at the start of a move will prevent movement.
// The symptom seen is that because of roundoff error, an avatar can stand on a vertical surface, or be unable to slide parallel to it.
// We move back out from a collision detected at the start of a move and retry the move to try to fix this issue.

// Distance to move away from a current collision.
static const float kfCollisionBackOutLength = .002f; // 2mm.
// If backing out from a collision doesn't increase movement by at least this much, don't use the back out. This should be larger than kfCollisionBackOutLength.
static const float kfMinimumBackOutEffect = .01f; // 1cm
// Distance to stay away from colliding items.
static const float kfCollisionMargin = .01f;
const float kfBelowGroundTestDistance = 0.1f;

namespace KEP {
namespace Physics {

struct BulletAvatar::StepResult {
	void AddCollision(const SweepTestResult& sweepResult) {
		if (sweepResult.m_bCollided)
			m_aCollisions.push_back({ sweepResult.m_pCollisionObject, sweepResult.m_ptContact, sweepResult.m_vContactNormal });
	}

	Vector3f m_ptFinal;
	struct Collision {
		const btCollisionObject* m_pObject;
		Vector3f m_ptContact;
		Vector3f m_vContactNormal;
	};
	std::vector<Collision> m_aCollisions;
};

BulletAvatar::BulletAvatar(BulletFactory& factory) :
		Avatar(factory), PhysicsBase(factory), BulletRigidBody(factory), Collidable(factory), m_fRadius(0), m_fHeight(0) {
}

bool BulletAvatar::Init(const AvatarProperties& props) {
	float fWidth = props.m_fWidthMeters;
	float fHeight = props.m_fHeightMeters;
	if (fWidth <= 0 || fHeight <= 0)
		return false;

	m_fRadius = 0.5f * fWidth;
	m_fHeight = fHeight;
	float fHorizontalShapeRadius = m_fRadius + kfCollisionMargin;

	float fHeightMinusStep = fHeight - GetStepHeight();
	if (fWidth > fHeightMinusStep)
		return false; // Can't create a capsule with these dimensions.

	m_pBTCollisionShape = std::make_unique<btCapsuleShape>(m_fRadius, fHeight - 2 * m_fRadius);
	// Horizontal capsule is slightly taller than standard capsule to ensure that it fully encloses the standard, otherwise we can have penetration issues.
	// It would be better to widen the standard capsule uniformly, but that would require a custom collision shape.
	//m_pBTHorizontalCollisionShape = std::make_unique<btCapsuleShape>(fHorizontalShapeRadius, fHeight - 2 * m_fRadius);
	// Widen horizontal shape using local scaling, which it appears btCapsule implements.
	float fHorizontalScale = (m_fRadius + kfCollisionMargin) / m_fRadius;
	m_pBTHorizontalCollisionShape = std::make_unique<btCapsuleShape>(m_fRadius, fHeight - 2 * m_fRadius);
	m_pBTHorizontalCollisionShape->setLocalScaling(btVector3(fHorizontalScale, 1.0f, fHorizontalScale));

	m_pBTStepCollisionShape = std::make_unique<btCapsuleShape>(m_fRadius, fHeightMinusStep - 2 * m_fRadius);
	m_pBTStepCollisionShape->setLocalScaling(btVector3(fHorizontalScale, 1.0f, fHorizontalScale));

	m_pBTSphereCollisionShape = std::make_unique<btSphereShape>(m_fRadius);

	btScalar mass = props.m_fMassKG;
	btTransform transformStart = btTransform::getIdentity();
	btTransform transformCenterOfMassOffset = btTransform::getIdentity(); // Static object, so transform is irrelevant.
	m_pMotionState = std::make_unique<btDefaultMotionState>(transformStart, transformCenterOfMassOffset);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, m_pMotionState.get(), m_pBTCollisionShape.get());
	m_pRigidBody = std::make_unique<btRigidBody>(rbInfo);
	return true;
}

btRigidBody* BulletAvatar::GetBTRigidBody() const {
	return m_pRigidBody.get();
}

btCollisionObject* BulletAvatar::GetBTCollisionObject() const {
	return m_pRigidBody.get();
}

SweepTestArguments BulletAvatar::GetSweepTestArgsForMove(const std::vector<const btCollisionObject*>& apIgnore, eAvatarShapeType shapeType) {
	btConvexShape* pShape;
	Vector3f vShapeOffset = GetCollisionShapeOffset();
	switch (shapeType) {
		case eAvatarShapeType::Standard:
			pShape = m_pBTCollisionShape.get();
			break;
		case eAvatarShapeType::HorizontalFull:
			pShape = m_pBTHorizontalCollisionShape.get();
			break;
		case eAvatarShapeType::HorizontalStep:
			pShape = m_pBTStepCollisionShape.get();
			vShapeOffset.y += 0.5f * GetStepHeight();
			break;
		case eAvatarShapeType::Sphere:
			pShape = m_pBTSphereCollisionShape.get();
			vShapeOffset.y = m_fRadius;
			break;
	};
	SweepTestArguments stArgs(pShape, vShapeOffset);
	stArgs.KeepDistFromObstacles(kfCollisionMargin);
	stArgs.CollisionMasks(GetCollisionGroupMask(), GetCollidesWithMask());
	stArgs.SetFilter([&apIgnore](const btCollisionObject* pTestObject) {
		return !std::binary_search(apIgnore.begin(), apIgnore.end(), pTestObject);
	});
	return stArgs;
}

bool BulletAvatar::SweepTest(Universe* pUniverse, const Vector3f& ptFrom, const Vector3f& ptTo, const std::vector<const Collidable*>* papIgnoreCollidable, Out<float> relativeMovement) {
	BulletUniverse* pBulletUniverse = BulletCast(pUniverse);
	if (!pBulletUniverse) {
		relativeMovement.set(0);
		return false;
	}

	btConvexShape* pAvatarShape = GetCollisionShape();
	Vector3f vShapeOffset = GetCollisionShapeOffset();

	std::vector<const btCollisionObject*> apIgnore;
	if (papIgnoreCollidable) {
		apIgnore.reserve(papIgnoreCollidable->size());
		for (const Collidable* pCollidable : *papIgnoreCollidable) {
			const BulletCollidable* pBulletCollidable = dynamic_cast<const BulletCollidable*>(pCollidable); // May want to make derivation from Collidable non-virtual.
			if (pBulletCollidable) {
				const btCollisionObject* pBTCollisionObject = pBulletCollidable->GetBTCollisionObject();
				if (pBTCollisionObject)
					apIgnore.push_back(pBTCollisionObject);
			}
		}
	}
	SweepTestResult result = pBulletUniverse->SweepTest(GetSweepTestArgsForMove(apIgnore).EndPoints(ptFrom, ptTo));
	relativeMovement.set(result.m_fRelativeMovement);
	return result.m_bCollided;
}

bool BulletAvatar::FixCurrentCollision(BulletUniverse* pBulletUniverse, Out<std::vector<const btCollisionObject*>> currentColliders) {
	static const float kfContactTolerance = kfCollisionMargin;
	static const float kfMaxCorrectionDistance = .0254f;
	static const float kfExtraCorrectionForRoundoff = 0; //.0001f;
	ContactTestResult contactResult = pBulletUniverse->ContactTest(m_pRigidBody.get(), kfMaxCorrectionDistance);

	if (!contactResult.m_aContacts.empty()) {
		// Objects that are intersecting, but have too much penetration to autocorrect
		std::vector<const btCollisionObject*> apDeepPenetrators;
		// Directions of penetration of objects that can be corrected.
		std::vector<Vector3f> avCollisionDirections;

		for (size_t i = 0; i < contactResult.m_aContacts.size();) {
			bool bAutoCorrectable = true;
			size_t iObjectBegin = i;
			size_t iObjectEnd = iObjectBegin;
			// Find range of contacts that represent a single object.
			float fMinDist = HUGE_VAL;
			do {
				float fDist = contactResult.m_aContacts[iObjectEnd].m_fDistance;
				fMinDist = std::min(fDist, fMinDist);
				if (fDist < -kfMaxCorrectionDistance)
					bAutoCorrectable = false; // Penetrates more than kfMaxCorrectionDistance.
				++iObjectEnd;
			} while (iObjectEnd < contactResult.m_aContacts.size() && contactResult.m_aContacts[iObjectBegin].m_pObject == contactResult.m_aContacts[iObjectEnd].m_pObject);
			//LogInfo("Object " << iObjectBegin << ": " << contactResult.m_aContacts[iObjectBegin].m_pObject << " Dist: " << fMinDist);

			// Collect penetration directions for  correctable objects, put non-correctable objects in list.
			if (bAutoCorrectable) {
				for (size_t j = iObjectBegin; j < iObjectEnd; ++j) {
					const ContactTestResult::Contact& contact = contactResult.m_aContacts[j];
					if (contact.m_fDistance < kfContactTolerance) // This is a penetration.
						avCollisionDirections.push_back(contact.m_vNormalOnOtherObject);
				}
			} else {
				apDeepPenetrators.push_back(contactResult.m_aContacts[iObjectBegin].m_pObject);
			}
			i = iObjectEnd;
		}
		//LogInfo("Contact "  << i << ": " << contact.m_pObject << " Distance: " << contact.m_fDistance);

		// Apply autocorrection of position for small penetrations.
		if (!avCollisionDirections.empty()) {
			Sphere<float> sphere;
			if (CreateMinimalBoundingSphere(avCollisionDirections, out(sphere))) {
				float fDistance = 0;
				float fMaxAllowedDistance = kfMaxCorrectionDistance;
				Vector3f vDirection = sphere.GetCenter();
				if (vDirection.LengthSquared() != 0) {
					vDirection.Normalize();
					bool bOpposingDirection = false;
					for (const ContactTestResult::Contact& contact : contactResult.m_aContacts) {
						assert(std::abs(contact.m_vNormalOnOtherObject.LengthSquared() - 1) < 1e-4); // I think the normals are already normalized. Verify.
						float fCosine = vDirection.Dot(contact.m_vNormalOnOtherObject);

						if (fCosine > 0) {
							float fEffectiveDistance = contact.m_fDistance - kfContactTolerance;
							if (-fEffectiveDistance > fCosine * fDistance)
								fDistance = -fEffectiveDistance / fCosine;
						} else {
							// Contact is on the opposite side of our correction direction.
							// If there is penetration, no correction is possible.
							// Otherwise the closeness of the contact may limit the amount of allowed correction.
							if (contact.m_fDistance < kfContactTolerance) {
								bOpposingDirection = true;
								break;
							}
							float fEffectiveDistance = contact.m_fDistance + kfContactTolerance;
							if (fEffectiveDistance < -fCosine * fMaxAllowedDistance)
								fMaxAllowedDistance = fEffectiveDistance / -fCosine;
						}
					}

					if (!bOpposingDirection && fDistance <= fMaxAllowedDistance) {
						// Apply correction.
						Vector3f vOffset = vDirection * fDistance;
						m_pRigidBody->getWorldTransform().getOrigin() += ToBullet(vOffset);
						//LogInfo("Correction offset: " << vOffset.X() << "," << vOffset.Y() << "," << vOffset.Z());

						// Retest contact and update penetrators after adjustment.
						contactResult = pBulletUniverse->ContactTest(m_pRigidBody.get(), 0.0f);

						apDeepPenetrators.clear();
						for (const ContactTestResult::Contact& contact : contactResult.m_aContacts) {
							if (contact.m_fDistance < 0.0f) {
								if (apDeepPenetrators.empty() || apDeepPenetrators.back() != contact.m_pObject)
									apDeepPenetrators.push_back(contact.m_pObject);
							}
						}
					}
				}
			}
		}

		// Ignore remaining penetrating objects.
		if (!apDeepPenetrators.empty()) {
			std::sort(apDeepPenetrators.begin(), apDeepPenetrators.end());
			apDeepPenetrators.erase(std::unique(apDeepPenetrators.begin(), apDeepPenetrators.end()), apDeepPenetrators.end());
			currentColliders.get().swap(apDeepPenetrators);
			//LogInfo("Colliding with " << currentColliders.get().size() << " object(s).");
		}
	}

	return true;
}

bool BulletAvatar::AdjustAfterMove(Universe* pUniverse, const Vector3f& previousPosition, const Vector3f& currentPosition, Out<AvatarMoveResult> result) {
	BulletUniverse* pBulletUniverse = BulletCast(pUniverse);
	if (!pBulletUniverse)
		return false;

	//LogInfo("Fixing collision at " << (previousPosition + GetCollisionShapeOffset()));

	// Update rigid body to starting position for collision tests.
	m_pRigidBody->setWorldTransform(btTransform(btMatrix3x3::getIdentity(), ToBullet(previousPosition + GetCollisionShapeOffset())));

	std::vector<const btCollisionObject*> apIgnore;
	FixCurrentCollision(pBulletUniverse, out(apIgnore));

	Vector3f ptStart = FromBullet(m_pRigidBody->getWorldTransform().getOrigin()) - GetCollisionShapeOffset();
	Vector3f ptEnd = ptStart + (currentPosition - previousPosition);

	bool bHorizontalMovement = ptStart.X() != ptEnd.X() || ptStart.Z() != ptEnd.Z();
	StepResult stepResult;
	if (bHorizontalMovement) {
		// Step is not appropriate when movement is on a platform.
		// The avatar should first move with the platform (and do collision checks that ignore the platform)
		// then step for any additional horizontal movement.

		// Moving horizontally with possible up/down motion.
		void (BulletAvatar::*StepMove)(BulletUniverse * pBulletUniverse, const Vector3f& previousPosition, const Vector3f& currentPosition, const std::vector<const btCollisionObject*>& apIgnore, Out<StepResult> result);
		StepMove = &BulletAvatar::StepMove1;
		if (kiMoveMethod == 1)
			StepMove = &BulletAvatar::StepMove1;
		if (kiMoveMethod == 2)
			StepMove = &BulletAvatar::StepMove2;
		if (kiMoveMethod == 3)
			StepMove = &BulletAvatar::StepMove3;

		(this->*StepMove)(pBulletUniverse, ptStart, ptEnd, apIgnore, out(stepResult));
	} else if (ptEnd.Y() <= ptStart.Y()) {
		// Pure downward movement.
		MoveDown(pBulletUniverse, ptStart, ptEnd, apIgnore, out(stepResult));
	} else {
		// Pure upward movement.
		Vector3f ptFrom = ptStart;
		Vector3f ptTo = ptEnd;
		SweepTestResult moveResult = pBulletUniverse->SweepTest(GetSweepTestArgsForMove(apIgnore).EndPoints(ptFrom, ptTo));
		stepResult.m_ptFinal = InterpolateLinear(ptFrom, ptTo, moveResult.m_fRelativeMovement);
	}

	result.get().m_ptFinal = stepResult.m_ptFinal;
	result.get().m_aCollisions.reserve(stepResult.m_aCollisions.size());
	for (const StepResult::Collision& collision : stepResult.m_aCollisions) {
		BulletRigidBody* pBulletRigidBody = static_cast<BulletRigidBody*>(collision.m_pObject->getUserPointer());
		Collidable* pCollidable = dynamic_cast<Collidable*>(pBulletRigidBody);
		result.get().m_aCollisions.push_back(AvatarMoveResult::Collision{ pCollidable, collision.m_ptContact, collision.m_vContactNormal });
	}

	return true;
}

void BulletAvatar::MoveDownSimple(BulletUniverse* pBulletUniverse, const Vector3f& ptFrom, const Vector3f& ptTargetTo, const std::vector<const btCollisionObject*>& apIgnore, Out<SweepTestResult> result) {
	btConvexShape* pAvatarShape = GetCollisionShape();
	Vector3f vShapeOffset = GetCollisionShapeOffset();

	// Step down
	// Extend collision slightly below ground level to test for support.
	Vector3f ptTo = ptTargetTo;
	ptTo.y -= kfBelowGroundTestDistance;
	result.get() = pBulletUniverse->SweepTest(GetSweepTestArgsForMove(apIgnore, eAvatarShapeType::Sphere).EndPoints(ptFrom, ptTo));

	// We test below the ground for support, but we don't allow actual movement below the ground.
	float fActualDistance = ptFrom.y - ptTargetTo.y;
	float fTestedDistance = ptFrom.y - ptTo.y;
	float& fRelativeMovement = result.get().m_fRelativeMovement;
	if (fActualDistance > 0) {
		float fMovement = fRelativeMovement * fTestedDistance;
		if (fMovement > fActualDistance)
			fRelativeMovement = 1; // Can't move more than actual distance moving.
		else
			fRelativeMovement = fMovement / fActualDistance; // Calculate relative movement based on actual distance moving.
	} else {
		fRelativeMovement = 0;
	}
}

void BulletAvatar::MoveDown(BulletUniverse* pBulletUniverse, const Vector3f& ptSource, const Vector3f& ptTarget, const std::vector<const btCollisionObject*>& apIgnore, Out<StepResult> result) {
	Vector3f ptFinal = ptTarget;
	SweepTestResult firstMoveResult;
	SweepTestResult secondMoveResult;
	SweepTestResult* pMoveResult = &firstMoveResult; // We may do two tests. This points to the results of the one we want to keep.
	MoveDownSimple(pBulletUniverse, ptSource, ptTarget, apIgnore, out(firstMoveResult));

	// Prevent going underground and capture the collision surface
	if (firstMoveResult.m_bCollided) {
		ptFinal = InterpolateLinear(ptSource, ptTarget, firstMoveResult.m_fRelativeMovement);

		const float kfMaxCosineVerticalToBackOut = 0.5f; // at least 30 degrees away from vertical
		if (std::abs(firstMoveResult.m_vContactNormal.y) < kfMaxCosineVerticalToBackOut) {
			// See if this is a collision with the side of the avatar (due to roundoff).
			Vector3f vBackOut = firstMoveResult.m_vContactNormal;
			vBackOut.Y() = 0;
			vBackOut.Normalize();
			float fBackoutLength = kfCollisionBackOutLength;
			if (vBackOut.Dot(firstMoveResult.m_ptContact - ptSource) > 0)
				fBackoutLength = -fBackoutLength;
			vBackOut *= kfCollisionBackOutLength;
			Vector3f ptBackOutFrom = ptSource + vBackOut;
			Vector3f ptBackOutTo = ptTarget + vBackOut;

			MoveDownSimple(pBulletUniverse, ptBackOutFrom, ptBackOutTo, apIgnore, out(secondMoveResult));
			Vector3f ptAlternateFinal = InterpolateLinear(ptBackOutFrom, ptBackOutTo, secondMoveResult.m_fRelativeMovement);
			// Only apply the backout if it has a nontrivial effect.
			if (DistanceSquared(ptFinal, ptAlternateFinal) > Square(kfMinimumBackOutEffect)) {
				ptFinal = ptAlternateFinal;
				pMoveResult = &secondMoveResult;
			}
		}
	}

	result.get().m_ptFinal = ptFinal;
	result.get().AddCollision(*pMoveResult);
}

BulletAvatar::StepResult BulletAvatar::MoveAndFollowCollision(BulletUniverse* pBulletUniverse, const Vector3f& vFollowPlaneNormal, const SweepTestArguments& sta) {
	BulletAvatar::StepResult stepResult;
	bool bRecordCollisions = true;
	Vector3f ptFrom = FromBullet(sta.GetFrom().getOrigin());
	Vector3f ptTo = FromBullet(sta.GetTo().getOrigin());
	SweepTestResult sweepResult = pBulletUniverse->SweepTest(sta);
	if (sweepResult.m_bCollided) {
		if (bRecordCollisions)
			stepResult.AddCollision(sweepResult);

		// Try to continue movement along the obstacle.
		// This may be invalid if there is an active penetration with the object.
		// The caller is currently handling this case, so for now we ignore it.
		Vector3f ptCollision = InterpolateLinear(ptFrom, ptTo, sweepResult.m_fRelativeMovement);
		Vector3f vRemainingMove = ptTo - ptCollision;
		ptFrom = ptCollision;
		Vector3f vMoveDirection = sweepResult.m_vContactNormal.Cross(vFollowPlaneNormal);
		Vector3f vPerpMove = vRemainingMove.GetProjectedOnto(vMoveDirection);
		if (sweepResult.m_fRelativeMovement == 0) {
			// Currently in collision state. Try to back out of it.
			// If we don't, there's a good chance we'll have an immediate collision when doing the perpendicular movement.
			Vector3f vBackOut = sweepResult.m_vContactNormal;
			float fBackOutLength = kfCollisionBackOutLength;
			if (vBackOut.Dot(sweepResult.m_ptContact - ptFrom) > 0)
				fBackOutLength = -fBackOutLength;
			vBackOut *= fBackOutLength;
			ptFrom += vBackOut;
		}
		ptTo = ptFrom + vPerpMove;

		SweepTestArguments sta2 = sta;
		sta2.EndPoints(ptFrom, ptTo);
		sweepResult = pBulletUniverse->SweepTest(sta2);
		if (bRecordCollisions)
			stepResult.AddCollision(sweepResult);
	}
	stepResult.m_ptFinal = InterpolateLinear(ptFrom, ptTo, sweepResult.m_fRelativeMovement);
	return stepResult;
}

void BulletAvatar::StepMove1(BulletUniverse* pBulletUniverse, const Vector3f& previousPosition, const Vector3f& currentPosition, const std::vector<const btCollisionObject*>& apIgnore, Out<StepResult> stepResult) {
	btConvexShape* pAvatarShape = GetCollisionShape();
	Vector3f vShapeOffset = GetCollisionShapeOffset();
	float fStepHeight = GetStepHeight();

	// Step up.
	Vector3f ptFrom = previousPosition;
	Vector3f ptTo = ptFrom + Vector3f(0, fStepHeight, 0);
	SweepTestResult stepUpResult = pBulletUniverse->SweepTest(GetSweepTestArgsForMove(apIgnore).EndPoints(ptFrom, ptTo));
	// result.get().AddCollision(stepUpResult); // Don't report collisions due to movement that's intended to avoid obstacles.

	// Move horizontally.
	ptFrom = InterpolateLinear(ptFrom, ptTo, stepUpResult.m_fRelativeMovement);
	stepResult.get() = MoveAndFollowCollision(pBulletUniverse, Vector3f(0, 1, 0), GetSweepTestArgsForMove(apIgnore, eAvatarShapeType::HorizontalFull).EndPoints(ptFrom, currentPosition));

	// Step down if necessary.
	ptFrom = stepResult.get().m_ptFinal;
	if (ptFrom.y > currentPosition.y) {
		ptTo.Set(ptFrom.x, currentPosition.y, ptFrom.z);
		MoveDown(pBulletUniverse, ptFrom, ptTo, apIgnore, stepResult);
	} else {
		stepResult.get().m_ptFinal = ptFrom;
	}
}

void BulletAvatar::StepMove2(BulletUniverse* pBulletUniverse, const Vector3f& previousPosition, const Vector3f& currentPosition, const std::vector<const btCollisionObject*>& apIgnore, Out<StepResult> stepResult) {
	btConvexShape* pAvatarShape = GetCollisionShape();
	Vector3f vShapeOffset = GetCollisionShapeOffset();
	float fStepHeight = GetStepHeight();

	// Move horizontally.
	stepResult.get() = MoveAndFollowCollision(pBulletUniverse, Vector3f(0, 1, 0), GetSweepTestArgsForMove(apIgnore, eAvatarShapeType::HorizontalStep).EndPoints(previousPosition, currentPosition));
	Vector3f ptFinalHorizontal = stepResult.get().m_ptFinal;

	Vector3f ptFrom = ptFinalHorizontal;
	ptFrom.y += GetStepHeight(); // Measure from the bottom of the reduced capsule.
	Vector3f ptTo(ptFrom.x, currentPosition.y, ptFrom.z);
	MoveDown(pBulletUniverse, ptFrom, ptTo, apIgnore, stepResult);

	//Check for clearance.

	// Sphere check from top of head at end of horizontal movement (which we know to be valid)
	// to a point that will encompass the avatar's height when measured from the result of MoveDown()
	ptFrom.y = ptFinalHorizontal.y + m_fHeight - 2 * m_fRadius;
	ptTo.y = stepResult.get().m_ptFinal.y + m_fHeight - 2 * m_fRadius;
	SweepTestResult upClearanceResult = pBulletUniverse->SweepTest(GetSweepTestArgsForMove(apIgnore, eAvatarShapeType::Sphere).EndPoints(ptFrom, ptTo));
	if (!upClearanceResult.m_bCollided || upClearanceResult.m_fRelativeMovement >= 1)
		return; // We have clearance, Clarence

	// Determine whether we would have clearance if we didn't move as far.
	float fAdditionalClearanceNeeded = ptTo.y - InterpolateLinear(ptFrom.y, ptTo.y, upClearanceResult.m_fRelativeMovement);
	const Vector3f& vTopNormal = upClearanceResult.m_vContactNormal;
	Vector3f vBottomNormal = stepResult.get().m_aCollisions.empty() ? Vector3f(0, 1, 0) : stepResult.get().m_aCollisions.back().m_vContactNormal;
	Vector3f vHorizontalMove = ptFinalHorizontal - previousPosition;
	vHorizontalMove.y = 0;

	auto ProjectOntoPlane = [](const Vector3f& v, const Vector3f& vPlaneNormal) -> Vector3f {
		return v.GetPartPerpendicularTo(vPlaneNormal);
	};
	auto DyDxz = [](const Vector3f& v) -> float {
		float xz_sq = v.x * v.x + v.z * v.z;
		if (xz_sq == 0)
			return 0;
		return v.y / sqrt(xz_sq);
	};
	float dydxz_top = DyDxz(ProjectOntoPlane(vHorizontalMove, vTopNormal));
	float dydxz_bottom = DyDxz(ProjectOntoPlane(vHorizontalMove, vBottomNormal));
	float dydxz_total = dydxz_bottom - dydxz_top;
	if (dydxz_total > 0) {
		// Backing off should increase the clearance.
		float backoffDist = fAdditionalClearanceNeeded / dydxz_total + kfCollisionMargin;
		if (backoffDist * backoffDist < vHorizontalMove.LengthSquared()) {
			// Backoff distance is not more than full movement distance.
			Vector3f ptBackoff = ptFinalHorizontal - vHorizontalMove.GetNormalized() * backoffDist;
			ptFrom = ptBackoff;
			ptFrom.y = previousPosition.y + GetStepHeight();
			ptTo = ptBackoff;
			ptTo.y = currentPosition.y;
			SweepTestResult clearanceResultDown = pBulletUniverse->SweepTest(GetSweepTestArgsForMove(apIgnore, eAvatarShapeType::Sphere).EndPoints(ptFrom, ptTo));
			if (clearanceResultDown.m_fRelativeMovement > 0) {
				// There is some downward clearance, so step height is not violated.
				float fBackoffBottomY = InterpolateLinear(ptFrom.y, ptTo.y, clearanceResultDown.m_fRelativeMovement);
				ptTo.y = fBackoffBottomY + m_fHeight - 2 * m_fRadius;
				SweepTestResult clearanceResultUp = pBulletUniverse->SweepTest(GetSweepTestArgsForMove(apIgnore, eAvatarShapeType::Sphere).EndPoints(ptFrom, ptTo));
				if (!clearanceResultUp.m_bCollided) {
					// There is enough vertical clearance for the avatar at this position.
					// Verify that it is possible to get here from start point.
					bool bBlocked = true;
					Vector3f vMoveDelta = ptBackoff - previousPosition;
					vMoveDelta.y = 0;
					if (0) {
						//if( vMoveDelta.LengthSquared() < Square(2*m_fRadius) ) {
						// Start and end position are close enough together that their overlap implies movement shouldn't be blocked.
						// This will actually allow movement through spaces slightly narrower than the avatar, but the earlier
						// horizontal movement test should prevent any errors that are obviously wrong except in contrived cases.
						bBlocked = false;
					} else {
						ptTo = ptFrom;
						ptTo.y = fBackoffBottomY + GetStepHeight();
						ptFrom = previousPosition;
						ptFrom.y += GetStepHeight();
						SweepTestResult sweepResult = pBulletUniverse->SweepTest(GetSweepTestArgsForMove(apIgnore, eAvatarShapeType::Sphere).EndPoints(ptFrom, ptTo));
						if (sweepResult.m_bCollided)
							bBlocked = true;
					}
					if (!bBlocked) {
						stepResult.get().m_ptFinal.Set(ptTo.x, fBackoffBottomY, ptTo.z);
						stepResult.get().AddCollision(clearanceResultDown);
						return;
					}
				}
			}
		}
	}

	// Unable to move horizontally. Apply vertical movement only.
	stepResult.get().m_aCollisions.clear();
	ptFrom = previousPosition;
	ptTo = ptFrom;
	ptTo.y = currentPosition.y;
	if (ptTo.y <= ptFrom.y) {
		MoveDown(pBulletUniverse, ptFrom, ptTo, apIgnore, stepResult);
	} else {
		SweepTestResult moveUpResult = pBulletUniverse->SweepTest(GetSweepTestArgsForMove(apIgnore).EndPoints(ptFrom, ptTo));
		stepResult.get().m_ptFinal = InterpolateLinear(ptFrom, ptTo, moveUpResult.m_fRelativeMovement);
	}
}

void BulletAvatar::StepMove3(BulletUniverse* pBulletUniverse, const Vector3f& previousPosition, const Vector3f& currentPosition, const std::vector<const btCollisionObject*>& apIgnore, Out<StepResult> stepResult) {
	btConvexShape* pAvatarShape = GetCollisionShape();
	Vector3f vShapeOffset = GetCollisionShapeOffset();
	float fStepHeight = GetStepHeight();

	// Move horizontally.
	stepResult.get() = MoveAndFollowCollision(pBulletUniverse, Vector3f(0, 1, 0), GetSweepTestArgsForMove(apIgnore, eAvatarShapeType::HorizontalStep).EndPoints(previousPosition, currentPosition));
	Vector3f ptFinalHorizontal = stepResult.get().m_ptFinal;

	Vector3f ptFrom = ptFinalHorizontal;
	ptFrom.y += GetStepHeight(); // Measure from the bottom of the reduced capsule.
	Vector3f ptTo(ptFrom.x, currentPosition.y, ptFrom.z);
	MoveDown(pBulletUniverse, ptFrom, ptTo, apIgnore, stepResult);

	//Check for clearance.

	// Sphere check from top of head at end of horizontal movement (which we know to be valid)
	// to a point that will encompass the avatar's height when measured from the result of MoveDown()
	ptFrom.y = ptFinalHorizontal.y + m_fHeight - 2 * m_fRadius;
	ptTo.y = stepResult.get().m_ptFinal.y + m_fHeight - 2 * m_fRadius;
	SweepTestResult upClearanceResult = pBulletUniverse->SweepTest(GetSweepTestArgsForMove(apIgnore, eAvatarShapeType::Sphere).EndPoints(ptFrom, ptTo));
	if (!upClearanceResult.m_bCollided || upClearanceResult.m_fRelativeMovement >= 1)
		return; // We have clearance, Clarence

	// Fall back to StepMove1 algorithm.
	stepResult.get().m_aCollisions.clear();
	StepMove1(pBulletUniverse, previousPosition, currentPosition, apIgnore, stepResult);
}

void BulletAvatar::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_pBTCollisionShape);
		pMemSizeGadget->AddObject(m_pBTHorizontalCollisionShape);
		pMemSizeGadget->AddObject(m_pBTSphereCollisionShape);
		pMemSizeGadget->AddObject(m_pBTSphereCollisionShape);
		pMemSizeGadget->AddObject(m_pRigidBody);
	}
}

#if 0
void BulletAvatar::NextMovementStep(float fTimeDelta)
{
}

void BulletAvatar::GetPosition(Out<Vector3f> position, Out<Quaternionf> orientation) const
{
}

void BulletAvatar::SetTargetVelocity(const Vector3f& v, float fAcceleration)
{
}

void BulletAvatar::SetTargetOrientation(const Quaternionf& q, float fAngularAcceleration)
{
}
#endif

} // namespace Physics
} // namespace KEP
