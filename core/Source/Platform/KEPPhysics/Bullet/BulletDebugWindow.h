///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../DebugWindow.h"

#include <map>
#include <memory>

namespace KEP {
namespace Physics {

class BulletDebugWindow : public DebugWindow {
public:
	BulletDebugWindow(PhysicsFactory& factory, const DebugWindowOptions& opt);
	~BulletDebugWindow();
	static std::shared_ptr<BulletDebugWindow> CreateBulletDebugWindow(PhysicsFactory& factory, const DebugWindowOptions& opt);

	virtual bool IsValid();
	virtual bool Show(bool bShow);
	virtual void SetTitle(const char* pszTitle);
	virtual bool RenderBegin();
	virtual bool RenderEnd();
	virtual bool Render(Universe* pUniverse);
	virtual bool Render(Avatar* pAvatar, const Matrix44f& transform);
	virtual void SetCameraProperties(const CameraProperties& camProps);

private:
	static LRESULT APIENTRY StaticWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static LRESULT APIENTRY WndProc(UINT message, WPARAM wParam, LPARAM lParam);

	bool CreateWindowsWindow();
	void Destroy();
	uint32_t NextColor() {
		m_CurrentColor = m_CurrentColor * 0x729671 + 0x819872;
		return m_CurrentColor;
	}
	static uint32_t ColorFromPointer(const void* p) {
		uint32_t i = reinterpret_cast<uint32_t>(p);
		i = (i >> 8) | (i << 24);
		i *= 0x887e9a71;
		i += 0x124565;
		return i;
	}

private:
	bool InitializeOpenGL();
	bool ConfigureDefaultOpenGLSettings();

	DebugWindowOptions m_Options;
	HWND m_hWnd;
	HGLRC m_hGLRC;
	std::string m_strTitle;
	CameraProperties m_CameraProperties;

	uint32_t m_CurrentColor;

	static const wchar_t* s_pszClassName;
	static std::map<HWND, std::shared_ptr<BulletDebugWindow>> s_WindowMap;
};

} // namespace Physics
} // namespace KEP
