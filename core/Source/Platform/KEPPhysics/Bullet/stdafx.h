/******************************************************************************
 stdafx.h

 
 Copyright (c) 2015 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <WinSDKVer.h>
#define _WIN32_WINNT 0x501
#include <SDKDDKVer.h>

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>

#include <bullet/btBulletDynamicsCommon.h>