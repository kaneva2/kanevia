///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../Collidable.h"

struct btDefaultMotionState;
class btCollisionObject;

namespace KEP {

class IMemSizeGadget;

namespace Physics {

class BulletUniverse;
class BulletFactory;

// Base class for Bullet collision objects
class BulletCollidable : public virtual Collidable {
public:
	BulletCollidable(BulletFactory& factory);
	virtual ~BulletCollidable() {}

	// Collidable interface
	virtual void SetCollisionMasks(unsigned int groupMask, unsigned int collidesWithMask);
	virtual void GetCollisionMasks(Out<unsigned int> groupMask, Out<unsigned int> collidesWithMask) const;

	virtual void SetIsCollidable(bool bCollidable);
	virtual Matrix44f GetTransformMetric() const;
	virtual bool SetTransformMetric(const Matrix44f& transform, bool bIgnoreScale = false) override;

	BulletUniverse* GetCurrentUniverse() { return m_pUniverse; }

	virtual btCollisionObject* GetBTCollisionObject() const = 0;
	virtual Vector3f GetGraphicsGeometryOffset() const { return Vector3f(0, 0, 0); } // Difference between position stored by bullet and position stored by client.

	// Only BulletUniverse should be calling these functions.
	virtual void AddingToUniverse(BulletUniverse* pUniverse); // First step in adding to universe.
	virtual void RemovingFromUniverse();
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const override;

public:
	unsigned int GetCollisionGroupMask() const { return m_iCollisionGroups; }
	unsigned int GetCollidesWithMask() const { return m_bCollidable ? m_iCollidesWithGroups : 0; }

private:
	void UpdateCollidable();

private:
	bool m_bCollidable;
	int m_iCollisionGroups;
	int m_iCollidesWithGroups;

	BulletUniverse* m_pUniverse; // should only be set by BulletUniverse.
};

} // namespace Physics
} // namespace KEP
