///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "../PhysicsFactory.h"

namespace KEP {
namespace Physics {

struct UniverseOptions;

class BulletFactory : public PhysicsFactory {
	virtual std::shared_ptr<DebugWindow> CreateDebugWindow(const DebugWindowOptions& opt) override;

	virtual std::shared_ptr<Universe> CreateUniverse(const UniverseOptions& opt) override;
	virtual std::shared_ptr<Avatar> CreateAvatar(const AvatarProperties& props) override;
	virtual std::shared_ptr<Vehicle> CreateVehicle(const VehicleProperties& props) override;
	virtual std::shared_ptr<Sensor> CreateSensor(const SensorProperties& props) override;
	virtual std::shared_ptr<RigidBodyStatic> CreateRigidBodyStatic(const RigidBodyStaticProperties& props, CollisionShape* pShape, const Vector3f& vShapeOffset, float fScale) override;

	virtual std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeCapsuleProperties& props) override;
	virtual std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeMeshProperties& props, const Vector3f& vScale) override;
	virtual std::shared_ptr<CollisionShape> CreateCollisionShape(const std::vector<CollisionShapeMeshProperties>& props, const Vector3f& vScale) override;
	virtual std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeSphereProperties& props) override;
	virtual std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeBoxProperties& props) override;
};

BulletFactory* GetBulletFactory();

} // namespace Physics
} // namespace KEP
