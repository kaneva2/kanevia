///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BulletUniverse.h"
#include "BulletRigidBodyStatic.h"
#include "BulletRigidBodyDynamic.h"
#include "BulletAvatar.h"
#include "BulletCast.h"
#include "Core/Math/Interpolate.h"
#include "Core/Math/Intersect.h" // for test code.
#include "Core/Math/TransformUtil.h"
#include "../common/include/IMemSizeGadget.h"
#include "../../Common/include/LogHelper.h"
#include "Core/Util/HighPrecisionTime.h"
#include "CoreStatic/PhysicsUserData.h"
#include "BulletSensor.h"
#include <bullet/BulletCollision/NarrowPhaseCollision/btRaycastCallback.h>
#include <bullet/LinearMath/btQuickprof.h>
#include <iomanip>
#include <sstream>

static LogInstance("Physics");

// #MLB_BulletSensor - needBroadphaseCollision
bool SensorOverlapFilterCallback::needBroadphaseCollision(btBroadphaseProxy* proxy0, btBroadphaseProxy* proxy1) const {
	btCollisionObject* pBody0 = reinterpret_cast<btCollisionObject*>(proxy0->m_clientObject);
	btCollisionObject* pBody1 = reinterpret_cast<btCollisionObject*>(proxy1->m_clientObject);
	bool isSensorObject = (proxy0->m_collisionFilterGroup & KEP::CollisionGroup::SensorTrigger) || (proxy1->m_collisionFilterGroup & KEP::CollisionGroup::SensorTrigger);
	bool isKinematicObject = pBody0->isKinematicObject() || pBody1->isKinematicObject();
	bool collides = isSensorObject && isKinematicObject;

	if (!collides) {
		collides = (proxy0->m_collisionFilterGroup & proxy1->m_collisionFilterMask) != 0;
		collides = collides && (proxy1->m_collisionFilterGroup & proxy0->m_collisionFilterMask);
	}

	return collides;
};

namespace KEP {
namespace Physics {

BulletUniverse::BulletUniverse(PhysicsFactory& factory) :
		PhysicsBase(factory), Universe(factory), m_fBulletPartialTime(0), m_fGravityMagnitude(0), m_dInputRealTimeAtNextStep(0), m_dThisStepSize(0) {
	m_pCollisionConfiguration = std::make_unique<btDefaultCollisionConfiguration>();
	m_pCollisionDispatcher = std::make_unique<btCollisionDispatcher>(m_pCollisionConfiguration.get());
	m_pBroadPhase = std::make_unique<btDbvtBroadphase>();
	m_pConstraintSolver = std::make_unique<btSequentialImpulseConstraintSolver>();
	m_pDynamicsWorld = std::make_unique<btDiscreteDynamicsWorld>(m_pCollisionDispatcher.get(), m_pBroadPhase.get(), m_pConstraintSolver.get(), m_pCollisionConfiguration.get());
	//m_pDynamicsWorld->setGravity( btVector(0, -9.8f, 0) );
	m_pDynamicsWorld->getDispatchInfo().m_useContinuous = true;
	//m_pDynamicsWorld->getDispatchInfo().m_dispatchFunc = btDispatcherInfo::DISPATCH_CONTINUOUS;
	m_pDynamicsWorld->setInternalTickCallback(staticInternalPreTickCallback, this, true);
	m_pDynamicsWorld->setInternalTickCallback(staticInternalPostTickCallback, this, false);
	m_pDynamicsWorld->setForceUpdateAllAabbs(false);

	// #MLB_BulletSensor - All BulletSensors share the same broad phase over lap filter callback
	m_pSensorOverlapFilterCallback = std::make_shared<SensorOverlapFilterCallback>();
}

BulletUniverse::~BulletUniverse() {
	RemoveAllSensors();
	m_pSensorOverlapFilterCallback.reset();
	m_pDynamicsWorld.reset();
	m_pConstraintSolver.reset();
	m_pBroadPhase.reset();
	m_pCollisionDispatcher.reset();
	m_pCollisionConfiguration.reset();
}

bool BulletUniverse::Init(const UniverseOptions& options) {
	if (options.m_fDesiredStepDuration <= 0)
		return false;

	if (options.m_nMaxSubStepCount <= 0)
		return false;

	m_UniverseOptions = options;
	m_pDynamicsWorld->setGravity(ToBullet(m_UniverseOptions.m_vGravityMPS2));

	m_fGravityMagnitude = m_UniverseOptions.m_vGravityMPS2.Length();

	return true;
}

// Static function for Bullet callback. Forwards to member function callback.
void BulletUniverse::staticInternalPreTickCallback(btDynamicsWorld* pDynamicsWorld, btScalar timeStep) {
	BulletUniverse* pBulletUniverse = static_cast<BulletUniverse*>(pDynamicsWorld->getWorldUserInfo());
	pBulletUniverse->InternalPreTickCallback(pDynamicsWorld, timeStep);
}

void BulletUniverse::InternalPreTickCallback(btDynamicsWorld* pDynamicsWorld, btScalar timeStep) {
}

// Static function for Bullet callback. Forwards to member function callback.
void BulletUniverse::staticInternalPostTickCallback(btDynamicsWorld* pDynamicsWorld, btScalar timeStep) {
	BulletUniverse* pBulletUniverse = static_cast<BulletUniverse*>(pDynamicsWorld->getWorldUserInfo());
	pBulletUniverse->InternalPostTickCallback(pDynamicsWorld, timeStep);
}

void BulletUniverse::InternalPostTickCallback(btDynamicsWorld* pDynamicsWorld, btScalar timeStep) {
	m_dInputRealTimeAtNextStep += m_dThisStepSize;
	//LogInfo("Input time update: " << std::fixed << std::setprecision(4) << m_dInputRealTimeAtNextStep);
}

bool BulletUniverse::Add(const std::shared_ptr<RigidBodyDynamic>& pRigidBodyDynamic) {
	std::shared_ptr<BulletRigidBodyDynamic> pBulletRigidBodyDynamic = BulletCast(pRigidBodyDynamic);
	return this->AddBulletRigidBody(std::move(pBulletRigidBodyDynamic));
}

bool BulletUniverse::Add(RigidBodyStatic* pRigidBodyStatic) {
	return pRigidBodyStatic ? AddBulletRigidBody(std::shared_ptr<BulletRigidBodyStatic>(pRigidBodyStatic->shared_from_this(), BulletCast(pRigidBodyStatic))) : false;
}

bool BulletUniverse::Add(const std::shared_ptr<RigidBodyStatic>& pRigidBodyStatic) {
	std::shared_ptr<BulletRigidBodyStatic> pBulletRigidBodyStatic = BulletCast(pRigidBodyStatic);
	return AddBulletRigidBody(std::move(pBulletRigidBodyStatic));
}

bool BulletUniverse::Add(const std::shared_ptr<Avatar>& pAvatar) {
	std::shared_ptr<BulletAvatar> pBulletAvatar = BulletCast(pAvatar);
	return AddBulletRigidBody(std::move(pBulletAvatar));
}

bool BulletUniverse::Add(const std::shared_ptr<Vehicle>& pVehicle) {
	std::shared_ptr<BulletVehicle> pBulletVehicleShared = BulletCast(pVehicle);

	BulletVehicle* pBulletVehicle = pBulletVehicleShared.get();
	if (!this->AddBulletRigidBody(std::move(pBulletVehicleShared)))
		return false;

	btActionInterface* pBTVehicle = pBulletVehicle->GetBTVehicle();
	m_pDynamicsWorld->addAction(pBTVehicle);

	return true;
}

// #MLB_BulletSensor - Add(const std::shared_ptr<Sensor>& pSensor)
bool BulletUniverse::Add(const std::shared_ptr<Sensor>& pSensor) {
	std::shared_ptr<BulletSensor> pBulletSensor = BulletCast(pSensor);
	if (m_pBroadPhase != nullptr) {
		// Set the overlap filter callback if this is the first sensor
		if (m_SensorMap.empty()) {
			m_pBroadPhase->getOverlappingPairCache()->setOverlapFilterCallback(m_pSensorOverlapFilterCallback.get());
		}

		auto result = m_SensorMap.emplace(pBulletSensor->GetSensorPid(), pBulletSensor);
		return result.second;
	}
	return false;
}

static void IterateMesh(const btBvhTriangleMeshShape* pMeshShape, btTriangleCallback* callback) {
	if (pMeshShape == nullptr)
		return;

	const btStridingMeshInterface* pMeshInterface = pMeshShape->getMeshInterface();
	size_t nSubParts = pMeshInterface->getNumSubParts();
	for (size_t iPart = 0; iPart < nSubParts; ++iPart) {
		const unsigned char* pFirstPoint;
		int iPointStride;
		PHY_ScalarType iPointType;
		int iPointCount;

		const unsigned char* pFirstIndex;
		int iIndexStride;
		PHY_ScalarType iIndexType;
		int nTriangles;
		pMeshInterface->getLockedReadOnlyVertexIndexBase(
			&pFirstPoint, iPointCount, iPointType, iPointStride,
			&pFirstIndex, iIndexStride, nTriangles, iIndexType,
			iPart);
		if (iPointType != PHY_FLOAT || iIndexType != PHY_SHORT) {
			LogError("Unexpected mesh format: " << iPointType << ", " << iIndexType);
			continue;
		}

		for (size_t i = 0; i < nTriangles; ++i) {
			unsigned short idx0 = reinterpret_cast<const unsigned short*>(pFirstIndex + i * iIndexStride)[0];
			unsigned short idx1 = reinterpret_cast<const unsigned short*>(pFirstIndex + i * iIndexStride)[1];
			unsigned short idx2 = reinterpret_cast<const unsigned short*>(pFirstIndex + i * iIndexStride)[2];

			const Vector3f& pt0 = *reinterpret_cast<const Vector3f*>(pFirstPoint + idx0 * iPointStride);
			const Vector3f& pt1 = *reinterpret_cast<const Vector3f*>(pFirstPoint + idx1 * iPointStride);
			const Vector3f& pt2 = *reinterpret_cast<const Vector3f*>(pFirstPoint + idx2 * iPointStride);

			btVector3 aTriPoints[3] = { ToBullet(pt0), ToBullet(pt1), ToBullet(pt2) };
			callback->processTriangle(aTriPoints, iPart, i);
		}
		pMeshInterface->unLockReadOnlyVertexBase(iPart);
	}
}

static int s_iRigidBodyNumber = 0;

bool BulletUniverse::PreAddRigidBody(const std::shared_ptr<BulletRigidBody>& pRigidBody) {
	if (!pRigidBody)
		return false;

	if (pRigidBody->GetCurrentUniverse() != nullptr)
		return false; // Already in some BulletUniverse.

	pRigidBody->AddingToUniverse(this);

	if (!m_setRigidBodiesInUniverse.insert(pRigidBody).second)
		assert(false); // Already in this BulletUniverse. (Should have failed above.)

	return true;
}

bool BulletUniverse::AddBulletRigidBody(std::shared_ptr<BulletRigidBody> pRigidBodyShared) {
	BulletRigidBody* pRigidBody = pRigidBodyShared.get();

	//if( !PreAddRigidBody(pRigidBody) )
	//	return false;
	if (!pRigidBody)
		return false;

	if (pRigidBody->GetCurrentUniverse() != nullptr)
		return false; // Already in some BulletUniverse.

	pRigidBody->AddingToUniverse(this);

	if (!m_setRigidBodiesInUniverse.insert(std::move(pRigidBodyShared)).second)
		assert(false); // Already in this BulletUniverse. (Should have failed above.)

	btRigidBody* pBTRigidBody = pRigidBody->GetBTRigidBody();
	unsigned int groupMask, collidesWithMask;
	pRigidBody->GetCollisionMasks(out(groupMask), out(collidesWithMask));
	m_pDynamicsWorld->addRigidBody(pBTRigidBody, groupMask, collidesWithMask);

#if 0
size_t iCurrentRigidBody = s_iRigidBodyNumber++;
static size_t iMin = 9, iMax = 9;
if( iCurrentRigidBody < iMin || iCurrentRigidBody > iMax )
return false;

	btCollisionShape* pBTCollisionShape = pBTRigidBody->getCollisionShape();
	btConcaveShape* pMeshShape = nullptr;
	if( pBTCollisionShape->isConcave() )
		pMeshShape = static_cast<btConcaveShape*>(pBTCollisionShape);

	if( pMeshShape ) {
		class Callback : public btTriangleCallback
		{
		public:
			Callback() : m_bContainsPoint(false) {}
			virtual ~Callback() {}
			virtual void processTriangle(btVector3* triangle, int partId, int triangleIndex)
			{
				//if( PointInTriangle<0,2>(Vector2f(-200,125), pPoints[idx0], pPoints[idx1], pPoints[idx2]) )
				Vector3f pt0 = TransformPoint(m_Matrix, FromBullet(triangle[0]));
				Vector3f pt1 = TransformPoint(m_Matrix, FromBullet(triangle[1]));
				Vector3f pt2 = TransformPoint(m_Matrix, FromBullet(triangle[2]));
				//if( PointInTriangle<0,2>(Vector2f(-200,125), pt0, pt1, pt2) )
				if( PointInTriangle<0,2>(Vector2f(-68.46f,-149.9f), pt0, pt1, pt2) )
					m_bContainsPoint = true;
				LogInfo("" << std::setw(5) << triangleIndex
					<< " Pt1:"
					<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pt0.x
					<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pt0.y
					<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pt0.z
					<< " Pt2:"
					<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pt1.x
					<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pt1.y
					<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pt1.z
					<< " Pt3:"
					<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pt2.x
					<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pt2.y
					<< " " << std::fixed << std::setw(9) << std::setprecision(3) << pt2.z
					);
			}
			bool m_bContainsPoint;
			Matrix44f m_Matrix;
		} callback;
		const btTransform& transform = pBTRigidBody->getWorldTransform();
		callback.m_Matrix = FromBullet(transform);
		//IterateMesh(pMeshShape, &callback);
		pMeshShape->processAllTriangles(&callback, btVector3(-FLT_MAX, -FLT_MAX, -FLT_MAX), btVector3(FLT_MAX, FLT_MAX, FLT_MAX));
	}
#endif

	return true;
}

bool BulletUniverse::Remove(RigidBodyStatic* pRigidBodyStatic) {
	BulletRigidBodyStatic* pBulletRigidBody = BulletCast(pRigidBodyStatic);
	if (!pBulletRigidBody)
		return false;

	return RemoveBulletRigidBody(pBulletRigidBody);
}

bool BulletUniverse::Remove(Vehicle* pVehicle) {
	BulletVehicle* pBulletVehicle = BulletCast(pVehicle);
	if (!pBulletVehicle)
		return false;

	m_pDynamicsWorld->removeAction(pBulletVehicle->GetBTVehicle());
	return RemoveBulletRigidBody(pBulletVehicle);
}

// #MLB_BulletSensor - Remove(Sensor* pSensor)
bool BulletUniverse::Remove(Sensor* pSensor) {
	BulletSensor* pBulletSensor = BulletCast(pSensor);
	auto it = m_SensorMap.find(pBulletSensor->GetSensorPid());
	if (it != m_SensorMap.end()) {
		it->second.reset();
		m_SensorMap.erase(it);
		if (m_SensorMap.empty()) {
			// Reset the overlap filter callback to the default
			m_pBroadPhase->getOverlappingPairCache()->setOverlapFilterCallback(nullptr);
			m_pSensorOverlapFilterCallback.reset();
		}
		return true;
	}

	return false;
}

bool BulletUniverse::RemoveAll() {
	s_iRigidBodyNumber = 0;
	for (auto& pRigidBody : m_setRigidBodiesInUniverse) {
		BulletVehicle* pBulletVehicle = dynamic_cast<BulletVehicle*>(pRigidBody.get());
		if (pBulletVehicle)
			m_pDynamicsWorld->removeAction(pBulletVehicle->GetBTVehicle());

		RemovingRigidBody(pRigidBody.get());
	}

	m_setRigidBodiesInUniverse.clear();

	RemoveAllSensors();

	return true;
}

// #MLB_BulletSensor - RemoveAllSensors()
void BulletUniverse::RemoveAllSensors() {
	for (auto it : m_SensorMap) {
		it.second.reset();
	}
	m_SensorMap.clear();
}

bool BulletUniverse::RemoveBulletRigidBody(BulletRigidBody* pRigidBody) {
	auto itr = m_setRigidBodiesInUniverse.find(pRigidBody);
	if (itr == m_setRigidBodiesInUniverse.end())
		return false;

	RemovingRigidBody(itr->get());
	m_setRigidBodiesInUniverse.erase(itr);
	return true;
}

// Handles removal of BulletRigidBody from Universe, except for removal from m_setRigidBodiesInUniverse
void BulletUniverse::RemovingRigidBody(BulletRigidBody* pRigidBody) {
	pRigidBody->RemovingFromUniverse();
	m_pDynamicsWorld->removeRigidBody(pRigidBody->GetBTRigidBody());
}
#if 0
BulletUniverse::CollisionCheckResult BulletUniverse::CollisionCheck(btCollisionObject* pCollisionObject, const Vector3f& ptLocation, const Vector3f& vShapeOffset)
{
	static_assert(false, "Unfinished code");
	// A/B objects need to be verified to make sure I don't have them reversed.
	struct ContactResult : public btCollisionWorld::ContactResultCallback {
		virtual btScalar addSingleResult(
			btManifoldPoint& cp,
			const btCollisionObjectWrapper* colObj0Wrap, int partId0, int index0,
			const btCollisionObjectWrapper* colObj1Wrap, int partId1, int index1 )
		{
			const btVector3& ptA = cp.getPositionWorldOnA();
			const btVector3& ptB = cp.getPositionWorldOnB();
			m_Result.m_aCollisionPoints.emplace_back(
				CollisionCheckResult::CollisionPoint{
					FromBullet(ptA),
					FromBullet(ptB),
					colObj1Wrap ? colObj1Wrap->getCollisionObject() : nullptr } );
			return 0;
		}
		BulletUniverse::CollisionCheckResult m_Result;
	};

	ContactResult callback;
	m_pDynamicsWorld->contactTest(pCollisionObject, callback);

	return std::move(callback.m_Result);
}
#endif

#if 0
bool BulletUniverse::BackOutFromCollision(const Collidable& collidable, const Vector3f& vBackOutDirection, Out<float> distance)
{
	const BulletCollidable* pBulletCollidable = dynamic_cast<const BulletCollidable*>(&collidable);
	if( !pBulletCollidable )
		return false;

	btCollisionObject* pBTCollisionObject = pBulletCollidable->GetBTCollisionObject();
	if( !pBTCollisionObject )
		return false;

	struct ContactResult : public btCollisionWorld::ContactResultCallback {
		ContactResult() : m_fMaxDist(0) {}
		virtual btScalar addSingleResult(
			btManifoldPoint& cp,
			const btCollisionObjectWrapper* colObj0Wrap, int partId0, int index0,
			const btCollisionObjectWrapper* colObj1Wrap, int partId1, int index1 )
		{
			const btVector3& ptA = cp.getPositionWorldOnA();
			const btVector3& ptB = cp.getPositionWorldOnB();
			Vector3f vDelta = FromBullet(ptA) - FromBullet(ptB);

			assert(false); // This heuristic only works for penetrations mostly in the same direction as vBackOutDirection
			float fDot = vDelta.Dot(m_vNormalizedDir);
			if( fDot > 0 ) {
				float fDist = vDelta.LengthSquared() / fDot;
				if( fDist > m_fMaxDist )
					m_fMaxDist = fDist;
			}
			return 0;
		}
		Vector3f m_vNormalizedDir;
		float m_fMaxDist;
	};

	unsigned int groupMask;
	unsigned int collidesWithMask;
	pBulletCollidable->GetCollisionMasks(out(groupMask), out(collidesWithMask));

	ContactResult callback;
	callback.m_collisionFilterGroup = groupMask;
	callback.m_collisionFilterMask = collidesWithMask;
	callback.m_vNormalizedDir = vBackOutDirection;
	if( !callback.m_vNormalizedDir.Normalize() )
		return false; // Bad direction.

	m_pDynamicsWorld->contactTest(pBTCollisionObject, callback);
	distance.set(callback.m_fMaxDist);

	return true;
}
#else
bool BulletUniverse::BackOutFromCollision(const Collidable& collidable, const Vector3f& vBackOutDirection, Out<float> distance, float fDistFromObstacles) {
	Vector3f vNormalizedDirection(vBackOutDirection);
	if (!vNormalizedDirection.Normalize())
		return false;

	const BulletCollidable* pBulletCollidable = dynamic_cast<const BulletCollidable*>(&collidable);
	if (!pBulletCollidable)
		return false;

	btCollisionObject* pBTCollisionObject = pBulletCollidable->GetBTCollisionObject();
	if (!pBTCollisionObject)
		return false;

	btCollisionShape* pBTCollisionShape = pBTCollisionObject->getCollisionShape();
	if (!pBTCollisionShape)
		return false;

	const btTransform& transform = pBTCollisionObject->getWorldTransform();
	btVector3 min, max;
	pBTCollisionShape->getAabb(transform, min, max);
	Vector3f dim(FromBullet(max) - FromBullet(min));
	Vector3f vAbsDirection(std::abs(vNormalizedDirection.X()), std::abs(vNormalizedDirection.Y()), std::abs(vNormalizedDirection.Z()));
	float fOffset = vAbsDirection.Dot(dim);

	btTransform transformFrom = transform;
	btTransform transformTo = transform;
	transformFrom.getOrigin() += ToBullet(fOffset * vBackOutDirection);

	SweepTestArguments::Options options;
	options.m_iCollisionGroupMask = pBulletCollidable->GetCollisionGroupMask();
	options.m_iCollidesWithMask = pBulletCollidable->GetCollidesWithMask();
	options.m_fKeepDistFromObstacles = fDistFromObstacles;
	SweepTestResult sweepTestResult = SweepTest(
		SweepTestArguments(pBTCollisionShape)
			.EndPoints(transformFrom, transformTo)
			.KeepDistFromObstacles(fDistFromObstacles)
			.CollisionMasks(pBulletCollidable->GetCollisionGroupMask(), pBulletCollidable->GetCollidesWithMask()));

	distance.set(sweepTestResult.m_fRelativeMovement * fOffset);
	return true;
}
#endif

void BulletUniverse::NearbyObjectTest(const Vector3f& ptBBoxMin, const Vector3f& ptBBoxMax, const std::function<void(const btBroadphaseProxy* pBroadphaseProxy)>& callback) {
	struct BroadphaseCallback : public btBroadphaseAabbCallback {
		BroadphaseCallback(const std::function<void(const btBroadphaseProxy* pBroadphaseProxy)>& callback) :
				m_Callback(callback) {}

		virtual bool process(const btBroadphaseProxy* proxy) {
			if (!proxy)
				return true;

			m_Callback(proxy);
			return true;
		}
		const std::function<void(const btBroadphaseProxy* pBroadphaseProxy)>& m_Callback;
	};

	BroadphaseCallback broadphaseCallback(callback);
	m_pBroadPhase->aabbTest(ToBullet(ptBBoxMin), ToBullet(ptBBoxMax), broadphaseCallback);
}

ContactTestResult BulletUniverse::ContactTest(btCollisionObject* pCollisionObject, float fMaxSeparation, std::vector<const btCollisionObject*> apExcludeObjects) {
	const btBroadphaseProxy* pBTBroadphaseProxy = pCollisionObject->getBroadphaseHandle();
	uint16_t groupMask = pBTBroadphaseProxy ? pBTBroadphaseProxy->m_collisionFilterGroup : -1;
	uint16_t collidesWithMask = pBTBroadphaseProxy ? pBTBroadphaseProxy->m_collisionFilterMask : -1;
	std::sort(apExcludeObjects.begin(), apExcludeObjects.end());
	return ContactTest(pCollisionObject, fMaxSeparation, [groupMask, collidesWithMask, &apExcludeObjects](const btCollisionObject* pBTCollisionObject) {
		if (std::binary_search(apExcludeObjects.begin(), apExcludeObjects.end(), pBTCollisionObject))
			return false; // Ignore object in provided list.

		const btBroadphaseProxy* pBroadphaseProxy = pBTCollisionObject->getBroadphaseHandle();
		if (!(pBroadphaseProxy->m_collisionFilterGroup & collidesWithMask) || !(pBroadphaseProxy->m_collisionFilterMask & groupMask))
			return false; // Ignore object with non-matching collision flags.

		return true;
	});
}

ContactTestResult BulletUniverse::ContactTest(btCollisionObject* pCollisionObject, float fMaxSeparation, const std::function<bool(const btCollisionObject*)>& filter) {
	class ContactCallback : private btBroadphaseAabbCallback {
	public:
		ContactCallback(btDynamicsWorld* pDynamicsWorld, btCollisionObject* pBTCollisionObject, const std::function<bool(const btCollisionObject*)>& filter, float fMaxSeparation) :
				m_GroupMask(-1), m_CollidesWithMask(-1), m_pBTCollisionObject(pBTCollisionObject), m_pConvexShape(nullptr), m_fMaxSeparation(fMaxSeparation), m_filter(filter) {
			btBroadphaseProxy* pBTBroadphaseProxy = pBTCollisionObject->getBroadphaseHandle();
			if (pBTBroadphaseProxy) {
				m_GroupMask = pBTBroadphaseProxy->m_collisionFilterGroup;
				m_CollidesWithMask = pBTBroadphaseProxy->m_collisionFilterMask;
			}

			m_btTransformShape = pBTCollisionObject->getWorldTransform();
			m_pDynamicsWorld = pDynamicsWorld;
			m_pDispatcher = m_pDynamicsWorld->getDispatcher();
		}

	private:
		virtual bool process(const btBroadphaseProxy* proxy) {
			btCollisionObject* pBTCollisionObject = (btCollisionObject*)proxy->m_clientObject;
			if (pBTCollisionObject == m_pBTCollisionObject)
				return true; // Don't report contact with self.
			if (!m_filter(pBTCollisionObject))
				return true;
			if (!(proxy->m_collisionFilterGroup & m_CollidesWithMask) || !(proxy->m_collisionFilterMask & m_GroupMask))
				return true;

			btCollisionObjectWrapper ob0(0, m_pConvexShape, m_pBTCollisionObject, m_btTransformShape, -1, -1);
			btCollisionObjectWrapper ob1(0, pBTCollisionObject->getCollisionShape(), pBTCollisionObject, pBTCollisionObject->getWorldTransform(), -1, -1);

			btCollisionAlgorithm* pAlgorithm = m_pDispatcher->findAlgorithm(&ob0, &ob1);
			if (!pAlgorithm)
				return true; // These object types can't collide.

			struct ManifoldResult : public btManifoldResult {
				ManifoldResult(const btCollisionObjectWrapper* body0Wrap, const btCollisionObjectWrapper* body1Wrap, ContactTestResult* pContactTestResult, float fMaxSeparation) :
						btManifoldResult(body0Wrap, body1Wrap), m_pContactTestResult(pContactTestResult), m_fMaxSeparation(fMaxSeparation) {
				}

				virtual void addContactPoint(const btVector3& normalOnBInWorld, const btVector3& pointInWorld, btScalar depth) override {
					if (depth > m_fMaxSeparation)
						return;

					const btCollisionObjectWrapper* pBodyAWrap = m_body0Wrap;
					const btCollisionObjectWrapper* pBodyBWrap = m_body1Wrap;
					bool isSwapped = m_manifoldPtr->getBody0() != m_body0Wrap->getCollisionObject();
					if (isSwapped)
						std::swap(pBodyAWrap, pBodyBWrap);

					//BulletRigidBody* pBulletRigidBody = static_cast<BulletRigidBody*>(pBodyBWrap->getCollisionObject()->getUserPointer());
					m_pContactTestResult->m_aContacts.push_back(ContactTestResult::Contact{ pBodyBWrap->getCollisionObject(), FromBullet(pointInWorld), FromBullet(normalOnBInWorld), depth });

					//btVector3 pointA = pointInWorld + normalOnBInWorld * depth;
					//btVector3 localA = pBodyAWrap->getWorldTransform().invXform(pointA);
					//btVector3 localB = pBodyBWrap->getWorldTransform().invXform(pointInWorld);
				}

				ContactTestResult* m_pContactTestResult;
				float m_fMaxSeparation;
			};

			ManifoldResult contactPointResult(&ob0, &ob1, &m_ContactTestResult, m_fMaxSeparation);

			pAlgorithm->processCollision(&ob0, &ob1, m_pDynamicsWorld->getDispatchInfo(), &contactPointResult);

			// Bullet requires manual management of the memory for the collision algorithm.
			pAlgorithm->~btCollisionAlgorithm();
			m_pDispatcher->freeCollisionAlgorithm(pAlgorithm);
			return true;
		}

	public:
		ContactTestResult ContactTest() {
			if (!m_pBTCollisionObject)
				return ContactTestResult{};

			btCollisionShape* pCollisionShape = m_pBTCollisionObject->getCollisionShape();
			if (!pCollisionShape)
				return ContactTestResult{};

			if (!pCollisionShape->isConvex()) {
				LogWarn("Collision contact tests cannot be performed on non-convex shapes."); // Although we can add the ability to handle compound collections of convex shapes as we did with sweep tests.
				return ContactTestResult{};
			}
			m_pConvexShape = static_cast<btConvexShape*>(pCollisionShape);

			btVector3 aabbMin, aabbMax;
			m_pConvexShape->getAabb(m_btTransformShape, aabbMin, aabbMax);
			m_pDynamicsWorld->getBroadphase()->aabbTest(aabbMin, aabbMax, *this);
			return std::move(m_ContactTestResult);
		}

		btCollisionObject* m_pBTCollisionObject;
		btConvexShape* m_pConvexShape;
		btTransform m_btTransformShape;
		float m_fMaxSeparation;
		unsigned int m_GroupMask;
		unsigned int m_CollidesWithMask;
		btDynamicsWorld* m_pDynamicsWorld;
		btDispatcher* m_pDispatcher;
		ContactTestResult m_ContactTestResult;
		const std::function<bool(const btCollisionObject*)>& m_filter;
	};

	std::function<bool(const btCollisionObject*)> nullFilter = [](const btCollisionObject*) { return true; };
	ContactCallback callback(m_pDynamicsWorld.get(), pCollisionObject, filter ? filter : nullFilter, fMaxSeparation);
	return callback.ContactTest();
}
#if 0
SweepTestResult BulletUniverse::ConvexSweepTest(btConvexShape* pShape, const Matrix44f& mtxFrom, const Matrix44f& mtxTo, const Vector3f& vShapeOffset, const SweepTestOptions& options)
{
	return ConvexSweepTest(pShape, ToBullet(mtxFrom), ToBullet(mtxTo), vShapeOffset, options);
}
#endif
SweepTestResult BulletUniverse::SweepTest(const SweepTestArguments& args) {
	return SweepTest(args.GetSweep(), args.GetOptions());
}

SweepTestResult BulletUniverse::SweepTest(const SweepTestArguments::Sweep& sweep, const SweepTestArguments::Options& options) {
	const btCollisionShape* pShape = sweep.m_pShape;
	if (pShape->isConvex())
		return ConvexSweepTestImpl(sweep, options);
	if (pShape->isCompound())
		return CompoundConvexSweepTestImpl(sweep, options);

	// Fall back to using bounding box of object.
	// May want to make this configurable: use bounding box, bounding sphere, bounding hull, point, or null object.
	// Alternatively, give the user an API to construct one of the above shapes based on an existing object.
	btTransform identity;
	identity.setIdentity();
	btVector3 aabbMin, aabbMax;
	pShape->getAabb(identity, aabbMin, aabbMax);
	btBoxShape boxShape(0.5f * (aabbMax - aabbMin));
	SweepTestArguments::Sweep boxSweep = sweep;
	boxSweep.m_vShapeOffset += 0.5f * (aabbMin + aabbMax);
	boxSweep.m_pShape = &boxShape;
	return ConvexSweepTestImpl(boxSweep, options);

	//return SweepTestResult{false, 0.0f}; // Can't calculate collision for non-convex shapes.
}

SweepTestResult BulletUniverse::CompoundConvexSweepTestImpl(const SweepTestArguments::Sweep& sweep, const SweepTestArguments::Options& options) {
	assert(sweep.m_pShape->isCompound()); // Caller (i.e. SweepTest()) handles this check.
	const btCompoundShape* pCompoundShape = static_cast<const btCompoundShape*>(sweep.m_pShape);

	SweepTestResult result{ false, 1.0f }; // Default to no collision.
	SweepTestArguments::Sweep childSweep;
	auto nChildShapes = pCompoundShape->getNumChildShapes();
	for (decltype(nChildShapes) i = 0; i < nChildShapes; ++i) {
		const btCollisionShape* pChildShape = pCompoundShape->getChildShape(i);
		const btConvexShape* pConvexChildShape = static_cast<const btConvexShape*>(pChildShape);
		const btTransform& childTransform = pCompoundShape->getChildTransform(i);
		childSweep.m_btTransFrom = childTransform * sweep.m_btTransFrom;
		childSweep.m_btTransTo = childTransform * sweep.m_btTransTo;
		childSweep.m_pShape = pChildShape;
		childSweep.m_vShapeOffset = sweep.m_vShapeOffset;
		SweepTestResult childResult = SweepTest(childSweep, options);
		if (childResult.m_bCollided && (!childResult.m_bCollided || childResult.m_fRelativeMovement < result.m_fRelativeMovement))
			result = childResult;
	}
	return result;
}

SweepTestResult BulletUniverse::ConvexSweepTestImpl(const SweepTestArguments::Sweep& sweep, const SweepTestArguments::Options& options) {
	assert(sweep.m_pShape->isConvex()); // Caller (i.e. SweepTest()) handles this check.
	const btConvexShape* pBTConvexShape = static_cast<const btConvexShape*>(sweep.m_pShape);
	const btTransform& btTransFrom = sweep.m_btTransFrom;
	const btTransform& btTransTo = sweep.m_btTransTo;
	const btVector3& vShapeOffset = sweep.m_vShapeOffset;
	bool bHasRotation = (btTransFrom.getRotation() != btTransTo.getRotation());
	bool bHasTranslation = (btTransFrom.getOrigin() != btTransTo.getOrigin());

	if (!bHasRotation && !bHasTranslation) {
		// No movement. Perhaps we should check if there is current penetration with another object.
		return SweepTestResult{
			false,
			0.0
		};
	}

	btTransform btTransformFrom = btTransFrom;
	btTransform btTransformTo = btTransTo;

	Vector3f ptFrom = FromBullet(btTransFrom.getOrigin());
	Vector3f ptTo = FromBullet(btTransTo.getOrigin());
	Vector3f vDesiredMovement = ptTo - ptFrom;
	float fDesiredMoveLengthSquared = vDesiredMovement.LengthSquared(); // Nonzero since bHasTranslation is true
	float fReciprocalMoveLength = 0;
	if (fDesiredMoveLengthSquared != 0)
		fReciprocalMoveLength = ReciprocalSqrt(fDesiredMoveLengthSquared);
	float fDesiredMoveLength = fDesiredMoveLengthSquared * fReciprocalMoveLength;
	float fTestMoveLength = fDesiredMoveLength;

	float fScaleMovement = 1.0f;
	float fKeepDistFromObstacles = 0;
	if (bHasTranslation && !bHasRotation) {
		// Bullet transforms the points and they may be equal after roundoff which causes Bullet to assert.
		// We correct for this by enlarging the separation between the points.
		// We also need to handle the m_fKeepDistFromObstacles option.
		fKeepDistFromObstacles = options.m_fKeepDistFromObstacles;
		fTestMoveLength += fKeepDistFromObstacles;
		if (fTestMoveLength < .1f)
			fTestMoveLength = .1f;
		fScaleMovement = fTestMoveLength * fReciprocalMoveLength;
	}
	Vector3f ptTestTo = ptFrom + fScaleMovement * vDesiredMovement;
	btTransformTo.setOrigin(ToBullet(ptTestTo));

	btTransformFrom.getOrigin() += btTransformFrom.getBasis() * vShapeOffset;
	btTransformTo.getOrigin() += btTransformTo.getBasis() * vShapeOffset;

	// The normalInWorldSpace parameter to btCollisionWorld::ClosestConvexResultCallback::addSingleResult is always set to false
	// although it seems it should be true. We'll work around the problem by forcing it to true here.
	struct MyConvexResultCallback : public btCollisionWorld::ClosestConvexResultCallback {
		MyConvexResultCallback(const btVector3& convexFromWorld, const btVector3& convexToWorld, const std::function<bool(const btCollisionObject*)>& filter) :
				ClosestConvexResultCallback(convexFromWorld, convexToWorld), m_Filter(filter) {
			m_vMoveDirection = convexToWorld - convexFromWorld;
		}
		virtual bool needsCollision(btBroadphaseProxy* pBroadphaseProxy) const override {
			if (!(pBroadphaseProxy->m_collisionFilterGroup & m_collisionFilterMask) || !(pBroadphaseProxy->m_collisionFilterMask & m_collisionFilterGroup))
				return false;
			if (m_Filter && !m_Filter(static_cast<const btCollisionObject*>(pBroadphaseProxy->m_clientObject)))
				return false;
			return true;
		}
		virtual btScalar addSingleResult(btCollisionWorld::LocalConvexResult& convexResult, bool normalInWorldSpace) {
			assert(normalInWorldSpace); // Current version of bullet always sets this to true.
			if (convexResult.m_hitNormalLocal.dot(m_vMoveDirection) > 64 * FLT_EPSILON)
				return FLT_MAX;
			return btCollisionWorld::ClosestConvexResultCallback::addSingleResult(convexResult, true);
		};

	private:
		const std::function<bool(const btCollisionObject*)>& m_Filter;
		btVector3 m_vMoveDirection;
	};
	MyConvexResultCallback callback(btTransformFrom.getOrigin(), btTransformTo.getOrigin(), options.m_Filter);
	callback.m_collisionFilterGroup = options.m_iCollisionGroupMask;
	callback.m_collisionFilterMask = options.m_iCollidesWithMask;
	this->m_pDynamicsWorld->convexSweepTest(pBTConvexShape, btTransformFrom, btTransformTo, callback);
	if (callback.hasHit()) {
		float fCollideFraction = callback.m_closestHitFraction;
		if (bHasTranslation && !bHasRotation) {
			float fCollideDist = fCollideFraction * fTestMoveLength;
			fCollideDist -= fKeepDistFromObstacles;
			if (fCollideDist < 0)
				fCollideDist = 0;
			fCollideFraction = fCollideDist * fReciprocalMoveLength;
		}
		if (fCollideFraction < 1) {
			return SweepTestResult{
				true,
				fCollideFraction,
				FromBullet(callback.m_hitPointWorld),
				FromBullet(callback.m_hitNormalWorld),
				callback.m_hitCollisionObject
			};
		}
	}

	return SweepTestResult{
		false,
		1.0f
	};
}
#if 0
SweepTestResult BulletUniverse::ConvexSweepTest(btConvexShape* pShape, const btTransform& btTransFrom, const btTransform& btTransTo, const Vector3f& vShapeOffset, const SweepTestOptions& options)
{
	bool bHasRotation = (btTransFrom.getRotation() != btTransTo.getRotation());
	bool bHasTranslation = (btTransFrom.getOrigin() != btTransTo.getOrigin());

	//if( !bHasRotation )
	//	return ConvexSweepTest(pShape, FromBullet(btTransFrom.getOrigin()), FromBullet(btTransTo.getOrigin()), vShapeOffset, btTransFrom.getBasis(), fDistFromObstacles);
	if( !bHasRotation && !bHasTranslation ) {
		return SweepTestResult {
			false,
			0.0
 		};
	}

	btTransform btTransformFrom = btTransFrom;
	btTransform btTransformTo = btTransTo;

	float fAdjustFactor = 1.0f;
	Vector3f ptFrom = FromBullet(btTransFrom.getOrigin());
	Vector3f ptTo = FromBullet(btTransTo.getOrigin());
	Vector3f vMovement = ptTo - ptFrom;
	float len2 = vMovement.LengthSquared();
	float fRecipLenMove = 1.0f;
	if( bHasTranslation && !bHasRotation ) {
		// Bullet transforms the points and they may be equal after roundoff which causes Bullet to assert.
		// We correct for this by enlarging the separation between the points.
		fRecipLenMove = ReciprocalSqrt(len2);
		fAdjustFactor = (len2 < 1 ? fRecipLenMove : 1) + .1f;
		Vector3f ptToAdjusted = ptFrom + fAdjustFactor * vMovement;
		btTransformTo.setOrigin( ToBullet(ptToAdjusted) );
	}

	btTransformFrom.getOrigin() += ToBullet(vShapeOffset);
	btTransformTo.getOrigin() += ToBullet(vShapeOffset);

	// The normalInWorldSpace parameter to btCollisionWorld::ClosestConvexResultCallback::addSingleResult is always set to false
	// although it seems it should be true. We'll work around the problem by forcing it to true here.
	struct MyConvexResultCallback : public btCollisionWorld::ClosestConvexResultCallback {
		MyConvexResultCallback(const btVector3& convexFromWorld, const btVector3& convexToWorld) : ClosestConvexResultCallback(convexFromWorld, convexToWorld) {}
		virtual	btScalar addSingleResult(btCollisionWorld::LocalConvexResult& convexResult,bool normalInWorldSpace) {
			const btBroadphaseProxy* pBroadphaseProxy = convexResult.m_hitCollisionObject->getBroadphaseHandle();
			if(    !(pBroadphaseProxy->m_collisionFilterGroup & m_collisionFilterMask)
				|| !(pBroadphaseProxy->m_collisionFilterMask & m_collisionFilterGroup) )
				return FLT_MAX;
			return btCollisionWorld::ClosestConvexResultCallback::addSingleResult(convexResult, true);
		};
	};
	MyConvexResultCallback callback(btTransformFrom.getOrigin(), btTransformTo.getOrigin());
	callback.m_collisionFilterGroup = options.m_iCollisionGroupMask;
	callback.m_collisionFilterMask = options.m_iCollidesWithMask;
	this->m_pDynamicsWorld->convexSweepTest(pShape, btTransformFrom, btTransformTo, callback);
	if( callback.hasHit() && callback.m_closestHitFraction * fAdjustFactor < 1 ) {
		float fCollideDist = callback.m_closestHitFraction;
		Vector3f vNormal = FromBullet(callback.m_hitNormalWorld);
		if( bHasTranslation && !bHasRotation ) {
			Vector3f vDirection = vMovement * fRecipLenMove; // Normalized
			float fMoveLength = fRecipLenMove * len2;
			fCollideDist *= fAdjustFactor * fMoveLength;
			assert(std::abs(vNormal.Dot(vNormal) - 1.0f) < .01f); // Expect a normalized normal.
			float fCosine = std::abs(vDirection.Dot(vNormal));
			if( fCosine != 0 )
				fCollideDist -= options.m_fDistFromObstacles / fCosine;
			else
				fCollideDist = 0;
			if( fCollideDist < 0 )
				fCollideDist = 0;
		}
		return SweepTestResult {
			true,
			fCollideDist * fRecipLenMove,
			FromBullet(callback.m_hitPointWorld),
			vNormal,
			callback.m_hitCollisionObject
 		};
	} else {
		return SweepTestResult {
			false,
			1.0f
		};
	}
}

SweepTestResult BulletUniverse::ConvexSweepTest(btConvexShape* pShape, const Vector3f& ptFrom, const Vector3f& ptTo, const Vector3f& vShapeOffset, const btMatrix3x3& orientation, const SweepTestOptions& options)
{
#if 1
	btTransform btTransformFrom(orientation, ToBullet(ptFrom));
	btTransform btTransformTo(orientation, ToBullet(ptTo));
	Matrix44f mtxFrom = FromBullet(btTransformFrom);
	Matrix44f mtxTo = FromBullet(btTransformTo);
	return ConvexSweepTest(pShape, mtxFrom, mtxTo, vShapeOffset, options);
#else
	Vector3f vMovement = ptTo - ptFrom;
	if( vMovement == Vector3f::Zero() )
		return SweepTestResult{false, 1.0f};

	// Bullet transforms the points and they may be equal after roundoff and then Bullet asserts.
	// We correct for this by enlarging the separation between the points.
	float len2 = vMovement.LengthSquared();
	const float fRecipLenMove = ReciprocalSqrt(len2);
	const float fAdjustFactor = (len2 < 1 ? fRecipLenMove : 1) + .1f;
	Vector3f ptToAdjusted = ptFrom + fAdjustFactor * vMovement;

	btTransform btTransformFrom(orientation, ToBullet(ptFrom + vShapeOffset));
	btTransform btTransformTo(orientation, ToBullet(ptToAdjusted + vShapeOffset));

	// The normalInWorldSpace parameter to btCollisionWorld::ClosestConvexResultCallback::addSingleResult is always set to false
	// although it seems it should be true. We'll work around the problem by forcing it to true here.
	struct MyConvexResultCallback : public btCollisionWorld::ClosestConvexResultCallback {
		MyConvexResultCallback(const btVector3& convexFromWorld, const btVector3& convexToWorld) : ClosestConvexResultCallback(convexFromWorld, convexToWorld) {}
		virtual	btScalar addSingleResult(btCollisionWorld::LocalConvexResult& convexResult,bool normalInWorldSpace) {
			return btCollisionWorld::ClosestConvexResultCallback::addSingleResult(convexResult, true);
		};
	};
	MyConvexResultCallback callback(btTransformFrom.getOrigin(), btTransformTo.getOrigin());
	this->m_pDynamicsWorld->convexSweepTest(pShape, btTransformFrom, btTransformTo, callback, fDistFromObstacles);
	if( callback.hasHit() && callback.m_closestHitFraction * fAdjustFactor < 1 ) {
		Vector3f vDirection = vMovement * fRecipLenMove; // Normalized
		float fMoveLength = fRecipLenMove * len2;
		float fCollideDist = callback.m_closestHitFraction * fAdjustFactor * fMoveLength;
		Vector3f vNormal = FromBullet(callback.m_hitNormalWorld);
		assert(std::abs(vNormal.Dot(vNormal) - 1.0f) < .01f); // Expect a normalized normal.
		float fCosine = std::abs(vDirection.Dot(vNormal));
		if( fCosine != 0 )
			fCollideDist -= fDistFromObstacles / fCosine;
		else
			fCollideDist = 0;
		if( fCollideDist < 0 )
			fCollideDist = 0;
		return SweepTestResult {
			true,
			fCollideDist * fRecipLenMove,
			FromBullet(callback.m_hitPointWorld),
			vNormal,
			callback.m_hitCollisionObject
 		};
	} else {
		return SweepTestResult {
			false,
			1.0f
		};
	}
#endif
}

SweepTestResult BulletUniverse::ConvexSweepTest(btConvexShape* pShape, const Vector3f& ptFrom, const Vector3f& ptTo, const Vector3f& vShapeOffset, const SweepTestOptions& options)
{
	return ConvexSweepTest(pShape, ptFrom, ptTo, vShapeOffset, btMatrix3x3::getIdentity(), options);
}
#endif

bool BulletUniverse::Collide(const Collidable& collidable, const Vector3f& ptFrom, const Vector3f& ptTo, Out<float> relativeMovement, const CollideOptions& collideOptions) {
	Matrix44f mtxFrom = collidable.GetTransformMetric();
	Matrix44f mtxTo = mtxFrom;
	mtxFrom.GetTranslation() = ptFrom;
	mtxTo.GetTranslation() = ptTo;
	return Collide(collidable, mtxFrom, mtxTo, relativeMovement, collideOptions);
}

bool BulletUniverse::Collide(const Collidable& collidable, const Matrix44f& mtxFrom, const Matrix44f& mtxTo, Out<float> relativeMovement, const CollideOptions& collideOptions) {
	relativeMovement.set(0.0f); // Initialize return value to zero movement for error conditions.

	const BulletCollidable* pBulletCollidable = dynamic_cast<const BulletCollidable*>(&collidable);
	if (!pBulletCollidable)
		return false;

	btCollisionObject* pBTCollisionObject = pBulletCollidable->GetBTCollisionObject();
	if (!pBTCollisionObject)
		return false;

	btCollisionShape* pBTCollisionShape = pBTCollisionObject->getCollisionShape();
	if (!pBTCollisionShape)
		return false;

	uint16_t iCollisionGroupMask, iCollidesWithMask;
	if (collideOptions.GetUseCollidableMasks()) {
		iCollisionGroupMask = pBulletCollidable->GetCollisionGroupMask();
		iCollidesWithMask = pBulletCollidable->GetCollidesWithMask();
	} else {
		iCollisionGroupMask = collideOptions.GetCollisionGroupMask();
		iCollidesWithMask = collideOptions.GetCollidesWithMask();
	}

	std::function<bool(const Collidable*)> userFilter = collideOptions.GetFilter();
	std::function<bool(const btCollisionObject*)> filter = [&collidable, userFilter](const btCollisionObject* pBTCollisionObject) -> bool {
		const BulletRigidBody* pRigidBody = static_cast<BulletRigidBody*>(pBTCollisionObject->getUserPointer());
		static bool bNullBodyReturnValue = true; // Originally false.
		if (!pRigidBody)
			return bNullBodyReturnValue;
		if (pRigidBody == &collidable)
			return false;
		if (userFilter)
			return userFilter(pRigidBody);
		return true;
	};
	SweepTestResult sweepTestResult = SweepTest(
		SweepTestArguments(pBTCollisionShape, -pBulletCollidable->GetGraphicsGeometryOffset())
			.EndPoints(mtxFrom, mtxTo)
			.KeepDistFromObstacles(collideOptions.GetKeepDistFromObstacles())
			.SetFilter(filter)
			.CollisionMasks(iCollisionGroupMask, iCollidesWithMask));

	relativeMovement.set(sweepTestResult.m_fRelativeMovement);
	return sweepTestResult.m_bCollided;
}

bool BulletUniverse::RayTest(const Vector3f& ptStart, const Vector3f& vDir, float fMaxDist, RayTestOptions options, Out<RayTestResult> result) {
	struct MyCallback : btCollisionWorld::RayResultCallback {
	public:
		MyCallback() {
		}

		virtual bool needsCollision(btBroadphaseProxy* pBroadphaseProxy) const {
			if (!(pBroadphaseProxy->m_collisionFilterGroup & m_collisionFilterMask))
				return false;
			if (m_CollidableFilter) {
				const btCollisionObject* pCollisionObject = static_cast<const btCollisionObject*>(pBroadphaseProxy->m_clientObject);
				if (!m_CollidableFilter(static_cast<BulletRigidBody*>(pCollisionObject->getUserPointer())))
					return false;
			}
			return true;
		}

		virtual btScalar addSingleResult(btCollisionWorld::LocalRayResult& rayResult, bool normalInWorldSpace) {
			int iPart = 0;
			int iIndex = 0;
			if (rayResult.m_localShapeInfo) {
				iPart = rayResult.m_localShapeInfo->m_shapePart;
				iIndex = rayResult.m_localShapeInfo->m_triangleIndex;
			}

			if (m_SubPartFilter) {
				BulletRigidBody* pRigidBody = CastUserData(rayResult.m_collisionObject);
				if (!m_SubPartFilter(pRigidBody, iPart, iIndex))
					return 1;
			}

			assert(normalInWorldSpace); // Current version of Bullet always sets this to true. If this changes, we need to convert.
			m_closestHitFraction = rayResult.m_hitFraction;
			m_collisionObject = rayResult.m_collisionObject;
			m_iPart = iPart;
			m_iIndex = iIndex;
			m_vNormalWS = rayResult.m_hitNormalLocal;
			return rayResult.m_hitFraction;
		}

		btVector3 m_vNormalWS; // world space normal
		int m_iPart = 0;
		int m_iIndex = 0;

		std::function<bool(Collidable* pCollidable)> m_CollidableFilter;
		std::function<bool(Collidable* pCollidable, size_t iPart, size_t iTriangle)> m_SubPartFilter;
	} myCallback;

	myCallback.m_flags = btTriangleRaycastCallback::kF_UseSubSimplexConvexCastRaytest;
	myCallback.m_flags |= btTriangleRaycastCallback::kF_FilterBackfaces;
	myCallback.m_CollidableFilter = std::move(options.m_CollidableFilter);
	myCallback.m_SubPartFilter = std::move(options.m_SubPartFilter);

	float fTMax = fMaxDist;
	if (fMaxDist > 128.0f) {
		// Calculate end point of test segment as intersection with far point of world bounding box.
		btVector3 aabbMin, aabbMax;
		m_pBroadPhase->getBroadphaseAabb(aabbMin, aabbMax);
		for (size_t i = 0; i < 3; ++i) {
			if (vDir[i] == 0)
				continue;
			float fCoord = vDir[i] < 0 ? aabbMin[i] : aabbMax[i];
			float fDiff = fCoord - ptStart[i];
			float fT = fDiff / vDir[i];
			fTMax = std::min(fT, fTMax);
		}
	}

	if (fTMax < 0)
		return false;
	btVector3 ptFrom = ToBullet(ptStart);
	btVector3 ptTo = ptFrom + ToBullet(vDir) * fTMax;

	m_pDynamicsWorld->rayTest(ptFrom, ptTo, myCallback);
	if (!myCallback.m_collisionObject)
		return false;

	const btCollisionObject* pBTCollisionObject = myCallback.m_collisionObject;
	BulletRigidBody* pRigidBody = static_cast<BulletRigidBody*>(pBTCollisionObject->getUserPointer());
	result.get().m_pHitObject = pRigidBody;
	result.get().m_fRelativeHitLocation = myCallback.m_closestHitFraction * fTMax;
	result.get().m_iPart = myCallback.m_iPart;
	result.get().m_iIndex = myCallback.m_iIndex;
	result.get().m_vNormal = FromBullet(myCallback.m_vNormalWS);
	return true;
}

// Some changes to objects require removing and re-adding to Universe.
// E.g. when an object's transform changes, it needs to be removed and re-added to the Universe
// so that Bullet knows its current location.
void BulletUniverse::ReconfiguredRigidBody(BulletRigidBody* pRigidBody) {
	if (!pRigidBody || pRigidBody->GetCurrentUniverse() != this)
		return;

	btRigidBody* pBTRigidBody = pRigidBody->GetBTRigidBody();
	m_pDynamicsWorld->removeRigidBody(pBTRigidBody);
	unsigned int groupMask, collidesWithMask;
	pRigidBody->GetCollisionMasks(out(groupMask), out(collidesWithMask));
	m_pDynamicsWorld->addRigidBody(pBTRigidBody, groupMask, collidesWithMask);
}

void BulletUniverse::GetRigidBodies(Out<std::vector<BulletRigidBody*>> apRigidBodies) {
	apRigidBodies.get().reserve(m_setRigidBodiesInUniverse.size());
	for (const auto& pSharedRigidBody : m_setRigidBodiesInUniverse)
		apRigidBodies.get().push_back(pSharedRigidBody.get());
}

void BulletUniverse::Step(double dFrameStartRealTime, double dDeltaPhysicsTime) {
	if (dDeltaPhysicsTime <= 0) {
		LogWarn("Physics time is frozen! dt=" << dDeltaPhysicsTime);
		return;
	}

	int nMaxSteps = m_UniverseOptions.m_nMaxSubStepCount;
	double dStepDuration = m_UniverseOptions.m_fDesiredStepDuration;

	double dMaxDeltaTime = nMaxSteps * dStepDuration;
	double dDeltaTime = std::min(dMaxDeltaTime, dDeltaPhysicsTime);

	// Duplicate Bullet's calculation of partial time (time between simulation steps).
	// m_fBulletPartialTime should equal the btDiscreteDynamicsWorld::m_localTime calculated in the following stepSimulation().
	// m_localTime is a protected member of btDiscreteDynamicsWorld so we can't easily access it directly. If this code calculates a different value
	// due to roundoff or other errors, we should just derive from btDiscreteDynamicsWorld and expose the variable.
	float fDeltaTime = dDeltaTime;
	float fStepDuration = dStepDuration;
	m_fBulletPartialTime += fDeltaTime;
	int nSimulationSteps = m_fBulletPartialTime / fStepDuration;
	float fFullStepTime = nSimulationSteps * fStepDuration;
	m_fBulletPartialTime -= fFullStepTime;

	m_dInputRealTimeAtNextStep = dFrameStartRealTime - fFullStepTime - m_fBulletPartialTime; // Input time lags the simulation time, otherwise we will need future input to process a full step.
	//LogInfo("Input time: " << std::fixed << std::setprecision(4) << m_dInputRealTimeAtNextStep);
	m_dThisStepSize = dStepDuration;
	m_pDynamicsWorld->stepSimulation(fDeltaTime, nSimulationSteps, fStepDuration);

	// #MLB_BulletSensor - Step() may need to distribute Sensor updates
	for (auto pSensor : m_SensorMap) {
		pSensor.second->Update(m_pDynamicsWorld.get());
	}

	// #MLB_BulletProfile - Profiling
	DumpProfile();
}

// #MLB_BulletSensor - FindSensor
bool BulletUniverse::FindSensor(int pid, std::shared_ptr<Sensor>& pSensor) {
	auto it = m_SensorMap.find(pid);
	if (it != m_SensorMap.end()) {
		pSensor = it->second;
		return true;
	}
	return false;
}

// Times up until which input needs to be processed (for the current step).
double BulletUniverse::GetRealTimeForInputProcessing() const {
	return m_dInputRealTimeAtNextStep;
}

// #MLB_BulletProfile - BT_NO_PROFILE
void BulletUniverse::DumpProfile() {
#ifndef BT_NO_PROFILE
	// Empty Output - Pair with call to CProfileManager::Set_Output(EmptyOutput),
	// which does not display any profile data.
	std::function<void(const char*)> EmptyOutput = [](const char* buffer) {};

	// Log profile data at a rate less than every frame
	// Ideally, add a configuration var to manage update frequency
	static Timer minTimer;
	const size_t updateFrequency = 48;
	if (minTimer.ElapsedMs() > static_cast<TimeMs>(updateFrequency)) {
		// Set_Output to nullptr which forces the use of the default
		// OutputDebugString, which displays profile data to the debug window.
		CProfileManager::Set_Output();
		CProfileManager::dumpAll();
		minTimer.Reset();
	}
#endif
}

void BulletUniverse::DumpProfile(std::ostringstream& strm) {
#ifndef BT_NO_PROFILE
	std::function<void(const char*)> Output = [&strm](const char* buffer) {
		strm << buffer;
	};

	CProfileManager::Set_Output(Output);
	CProfileManager::dumpAll();
#endif
}

void UniverseOptions::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	pMemSizeGadget->AddObjectSizeof(this);
}

void BulletUniverse::GetMemSizeofRigidBodySet(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeInBytes(&m_setRigidBodiesInUniverse, sizeof(m_setRigidBodiesInUniverse))) {
		for (const auto& pSharedRigidBody : m_setRigidBodiesInUniverse)
			pMemSizeGadget->AddObject(pSharedRigidBody);
	}
}

void BulletUniverse::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_pCollisionConfiguration);
		pMemSizeGadget->AddObject(m_pBroadPhase);
		pMemSizeGadget->AddObject(m_pCollisionDispatcher);
		pMemSizeGadget->AddObject(m_pConstraintSolver);
		pMemSizeGadget->AddObject(m_UniverseOptions);
		pMemSizeGadget->AddObject(m_pDynamicsWorld);
		pMemSizeGadget->AddObject(m_SensorMap);
		GetMemSizeofRigidBodySet(pMemSizeGadget);
	}
}

} // namespace Physics
} // namespace KEP
