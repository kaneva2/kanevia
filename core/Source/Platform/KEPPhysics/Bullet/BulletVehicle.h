///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "BulletRigidBody.h"
#include "../Vehicle.h"
#include "Core/Math/RigidTransform.h"
#include <deque>

class btCollisionObject;
class btConvexShape;

namespace KEP {

class IMemSizeGadget;

namespace Physics {

class BulletFactory;

struct BulletVehicleControl {
	BulletVehicleControl() { Clear(); }
	void Clear() {
		for (bool& bActivated : m_aControlActivated)
			bActivated = false;
		m_dTurboLastActivateTime = -HUGE_VAL;
	}

	std::deque<VehicleControlCommand> m_aCommands;
	bool m_aControlActivated[eVehicleControlType::Count];
	double m_dTurboLastActivateTime;
};

struct BulletVehicleControlApplication {
	float m_fForwardDuration;
	float m_fSideDuration;
	float m_fBrakeDuration;
	bool m_bTurboActivated;
};

class BulletVehicle
		: public Vehicle,
		  public BulletRigidBody {
public:
	enum eWheelIndex {
		WheelIndex_LeftFront,
		WheelIndex_RightFront,
		WheelIndex_LeftRear,
		WheelIndex_RightRear
	};

public:
	BulletVehicle(BulletFactory& factory);
	bool Init(const VehicleProperties& props);

	// Vehicle overrides
	virtual const VehicleProperties& GetVehicleProperties();
	virtual void UpdateVehicleProperties(const VehicleProperties& props);
	virtual void ApplyControl(const VehicleControlCommand& cmd);
	virtual void ClearControls();
	virtual float GetSpeedKPH();
	virtual float GetSkidFactor();

	// BulletRigidBody overrides
	virtual btRigidBody* GetBTRigidBody() const;
	virtual btCollisionObject* GetBTCollisionObject() const;
	virtual bool SetTransformMetric(const Matrix44f& transform, bool bIgnoreScale = false) override;

	virtual void AddingToUniverse(BulletUniverse* pUniverse);
	btActionInterface* GetBTVehicle();

	virtual Vector3f GetGraphicsGeometryOffset() const { return -m_ptCenterOfMass; } // Difference between position stored by bullet and position stored by client.
	void GetWheelTransforms(Out<Matrix44f[4]> aWheelTransforms);

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const override final;

private:
	void ValidateVehicleProperties();
	bool CreateBTVehicle();
	void UpdateBTVehicleWithCurrentProps();
	void CalculateControlDurations(float fStep, Out<BulletVehicleControlApplication> controlApplication);
	void ApplyForces(float fStep, Out<bool> bBraking);
	void PostUpdate(float fStep);
	size_t NumberOfWheelsInContactWithGround() const;
	void HandleVehicleVehicleCollisions(float fStep);

private:
	class MyRayCastVehicle;
	class MyRayCaster;

	VehicleProperties m_VehicleProperties;
	Vector3f m_ptCenterOfMass; // Derived from m_VehicleProperties
	Vector3f m_avWheelOffsets[4]; // Offsets from center of vehicle box. Indexed be eWheelIndex

	std::unique_ptr<MyRayCaster> m_pVehicleRaycaster;
	std::unique_ptr<MyRayCastVehicle> m_pBTVehicle;

	std::unique_ptr<btMotionState> m_pMotionState;
	std::shared_ptr<btConvexHullShape> m_pChassisRootShape;
	std::unique_ptr<btRigidBody> m_pChassisRigidBody;

	// For handling collision against another vehicle object.
	std::unique_ptr<btBoxShape> m_pOtherVehicleCollisionShape;
	std::unique_ptr<btCollisionObject> m_pOtherVehicleCollisionObject;

	BulletVehicleControl m_VehicleControl;
};

} // namespace Physics
} // namespace KEP
