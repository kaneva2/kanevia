///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "BulletRigidBodyStatic.h"
#include "BulletFactory.h"
#include "BulletCast.h"
#include "BulletCollisionShape.h"
#include "Core/Math/TransformUtil.h"

#include "../../Common/include/IMemSizeGadget.h"
#include "../../Common/include/LogHelper.h"
static LogInstance("Physics");

namespace KEP {
namespace Physics {

BulletRigidBodyStatic::BulletRigidBodyStatic(BulletFactory& factory) :
		PhysicsBase(factory), RigidBodyStatic(factory), BulletRigidBody(factory), Collidable(factory) {
}

BulletRigidBodyStatic::~BulletRigidBodyStatic() {
}

bool BulletRigidBodyStatic::Init(const RigidBodyStaticProperties& props, CollisionShape* pShape, const Vector3f& vShapeOffset, float fScale) {
	BulletCollisionShape* pBulletShape = BulletCast(pShape);
	if (pBulletShape == nullptr)
		return false;

	SetCollisionMasks(props.m_CollisionGroupMask, props.m_CollidesWithMask);

	m_vShapeOffset = vShapeOffset;
	m_fScale = fScale;
	m_pCollisionShape = std::shared_ptr<const BulletCollisionShape>(pBulletShape->shared_from_this(), pBulletShape);
	CreateScaleShape(Vector3f(fScale));

	btScalar mass = 0; // No mass for static rigid bodies.
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, nullptr, GetCollisionShape());
	rbInfo.m_restitution = props.m_fRestitution;
	m_pRigidBody = std::make_unique<btRigidBody>(rbInfo);
	m_pRigidBody->setCollisionFlags(m_pRigidBody->getCollisionFlags() | btCollisionObject::CF_STATIC_OBJECT);
	m_pRigidBody->setUserPointer(static_cast<BulletRigidBody*>(this));
	return true;
}

btRigidBody* BulletRigidBodyStatic::GetBTRigidBody() const {
	return m_pRigidBody.get();
}

btCollisionObject* BulletRigidBodyStatic::GetBTCollisionObject() const {
	return m_pRigidBody.get();
}

bool BulletRigidBodyStatic::SetTransformMetric(const Matrix44f& transform, bool bIgnoreScale) {
	Vector3f vScaleFactors(1, 1, 1);
	Matrix44f normalizedTransform;
	for (size_t i = 0; i < 3; ++i) {
		const Vector3f& row3 = transform[i].SubVector<3>();
		float fLen2 = row3.LengthSquared();
		float fRecipLen = ReciprocalSqrt_Exact(fLen2);
		vScaleFactors[i] = std::sqrt(fLen2);
		normalizedTransform[i].SubVector<3>() = row3 * fRecipLen;
		normalizedTransform(i, 3) = 0;
	}
	normalizedTransform.GetTranslation() = transform.GetTranslation() + m_vShapeOffset * vScaleFactors;
	normalizedTransform(3, 3) = 1;

	btTransform bulletTransform = ToBullet(normalizedTransform);
	m_pRigidBody->setWorldTransform(bulletTransform);

	if (bIgnoreScale)
		vScaleFactors.Set(1, 1, 1);
	else
		vScaleFactors *= m_fScale;
	CreateScaleShape(vScaleFactors);
	m_pRigidBody->setCollisionShape(GetCollisionShape());

	BulletUniverse* pUniverse = GetCurrentUniverse();
	if (pUniverse) {
		//GetCurrentUniverse()->ReconfiguredRigidBody(this);
		pUniverse->GetBTWorld()->updateSingleAabb(m_pRigidBody.get());
	}

	return true;
}

void BulletRigidBodyStatic::CreateScaleShape(const Vector3f& vScaleFactors) {
	// Save old scale shape for possible reuse.
	std::unique_ptr<btCollisionShape> pOldScaleShape = std::move(m_pScaleShape);

	static const float kfScaleTolerance = std::numeric_limits<float>::epsilon() * (1 << (3 * std::numeric_limits<float>::digits / 4));
	bool bUniformScale = false;
	if (std::abs(vScaleFactors.X() - vScaleFactors.Y()) < vScaleFactors.X() * kfScaleTolerance || std::abs(vScaleFactors.Y() - vScaleFactors.Z()) < vScaleFactors.Y() * kfScaleTolerance || std::abs(vScaleFactors.Z() - vScaleFactors.X()) < vScaleFactors.Z() * kfScaleTolerance) {
		bUniformScale = true;
	}

	// If uniform scale is 1.0, no scaling shape is needed. Otherwise create a scaling shape.
	if (bUniformScale && std::abs(vScaleFactors.X() - 1) < kfScaleTolerance)
		return;

	btCollisionShape* pCollisionShape = m_pCollisionShape->GetBTCollisionShape();
	if (pCollisionShape->getShapeType() == TRIANGLE_MESH_SHAPE_PROXYTYPE) {
		// Mesh shapes need to use btScaledBvhTriangleMeshShape, which supports nonuniform scaling.
		if (pOldScaleShape && pOldScaleShape->getShapeType() == SCALED_TRIANGLE_MESH_SHAPE_PROXYTYPE) {
			// Reuse old scale object.
			static_cast<btScaledBvhTriangleMeshShape*>(pOldScaleShape.get())->setLocalScaling(ToBullet(vScaleFactors));
			m_pScaleShape = std::move(pOldScaleShape);
		} else {
			m_pScaleShape = std::make_unique<btScaledBvhTriangleMeshShape>(
				static_cast<btBvhTriangleMeshShape*>(pCollisionShape),
				ToBullet(vScaleFactors));
		}
	} else if (pCollisionShape->isConvex()) {
		// Convex shapes can only take a uniform scale.
		if (bUniformScale) {
			float fScaleFactor = vScaleFactors.X();
			if (std::abs(fScaleFactor - 1) > kfScaleTolerance) {
				/* Can't reuse btUniformScalingShape objects because they have no API to adjust the scale factor. (Bullet v2.83)
				if( pOldScaleShape && pOldScaleShape->getShapeType() == UNIFORM_SCALING_SHAPE_PROXYTYPE ) {
					// Reuse old scale object.
					static_cast<btUniformScalingShape*>(pOldScaleShape.get())->setUniformScaling(ToBullet(vScaleFactors));
					m_pScaleShape = std::move(pOldScaleShape);
				} else
				*/
				{
					m_pScaleShape = std::make_unique<btUniformScalingShape>(
						static_cast<btConvexShape*>(pCollisionShape),
						fScaleFactor);
				}
			}
		} else {
			LogError("Only uniform scaling is supported for non-mesh shapes.  Scale = (" << vScaleFactors.X() << "," << vScaleFactors.Y() << "," << vScaleFactors.Z() << ")");
		}
	} else if (pCollisionShape->getShapeType() == EMPTY_SHAPE_PROXYTYPE) {
		// No scaling needs to be done for empty shapes.
	} else {
		// Non-convex shapes can't be scaled.
		LogError("Scaling not supported for this shape. Scale = (" << vScaleFactors.X() << "," << vScaleFactors.Y() << "," << vScaleFactors.Z() << ")");
	}
}

btCollisionShape* BulletRigidBodyStatic::GetCollisionShape() {
	if (m_pScaleShape)
		return m_pScaleShape.get();

	return m_pCollisionShape->GetBTCollisionShape();
}

void BulletRigidBodyStatic::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_pCollisionShape);
		pMemSizeGadget->AddObject(m_pRigidBody);
		pMemSizeGadget->AddObject(m_pScaleShape);
	}
}

} // namespace Physics
} // namespace KEP
