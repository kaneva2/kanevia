///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "BulletCollisionShape.h"
#include "BulletFactory.h"
#include "BulletAvatar.h"
#include "BulletRigidBodyDynamic.h"
#include "BulletRigidBodyStatic.h"
#include "BulletUniverse.h"
#include "BulletVehicle.h"
#include "BulletSensor.h"
#include "BulletMathCast.h"
#include "Core/Math/Matrix.h"
#include <vector>

namespace KEP {
namespace Physics {

// Helper template for accessing bullet implementation type from physics interface type.
template <typename T>
struct BulletTypeFromInterfaceType {};

template <>
struct BulletTypeFromInterfaceType<Avatar> {
	typedef BulletAvatar BulletType;
};

template <>
struct BulletTypeFromInterfaceType<CollisionShape> {
	typedef BulletCollisionShape BulletType;
};

template <>
struct BulletTypeFromInterfaceType<RigidBodyDynamic> {
	typedef BulletRigidBodyDynamic BulletType;
};

template <>
struct BulletTypeFromInterfaceType<RigidBodyStatic> {
	typedef BulletRigidBodyStatic BulletType;
};

template <>
struct BulletTypeFromInterfaceType<Universe> {
	typedef BulletUniverse BulletType;
};

template <>
struct BulletTypeFromInterfaceType<Vehicle> {
	typedef BulletVehicle BulletType;
};

template <>
struct BulletTypeFromInterfaceType<Sensor> {
	typedef BulletSensor BulletType;
};

template <typename T>
using BulletTypeFromInterfaceType_t = typename BulletTypeFromInterfaceType<T>::BulletType;

// Cast pointers to bullet implementation objects
template <typename InterfaceType>
inline BulletTypeFromInterfaceType_t<InterfaceType>* BulletCast(InterfaceType* pInterfaceType) {
	if (pInterfaceType && pInterfaceType->UsesFactory(*GetBulletFactory()))
		return static_cast<BulletTypeFromInterfaceType_t<InterfaceType>*>(pInterfaceType);
	return nullptr;
}

template <typename InterfaceType>
inline const BulletTypeFromInterfaceType_t<InterfaceType>* BulletCast(const InterfaceType* pInterfaceType) {
	if (pInterfaceType && pInterfaceType->UsesFactory(*GetBulletFactory()))
		return static_cast<const BulletTypeFromInterfaceType_t<InterfaceType>*>(pInterfaceType);
	return nullptr;
}

// Cast shared_ptr to bullet implementation objects
template <typename InterfaceType>
inline std::shared_ptr<BulletTypeFromInterfaceType_t<InterfaceType>> BulletCast(const std::shared_ptr<InterfaceType>& pInterfaceType) {
	if (pInterfaceType && pInterfaceType->UsesFactory(*GetBulletFactory()))
		return std::static_pointer_cast<BulletTypeFromInterfaceType_t<InterfaceType>>(pInterfaceType);
	return nullptr;
}

template <typename InterfaceType>
inline std::shared_ptr<const BulletTypeFromInterfaceType_t<InterfaceType>> BulletCast(const std::shared_ptr<const InterfaceType>& pInterfaceType) {
	if (pInterfaceType && pInterfaceType->UsesFactory(*GetBulletFactory()))
		return std::static_pointer_cast<const BulletTypeFromInterfaceType_t<InterfaceType>>(pInterfaceType);
	return nullptr;
}

// Cast reference to bullet implementation objects
template <typename InterfaceType>
inline BulletTypeFromInterfaceType_t<InterfaceType>* BulletCast(InterfaceType& interfaceType) {
	if (interfaceType.UsesFactory(*GetBulletFactory()))
		return static_cast<BulletTypeFromInterfaceType_t<InterfaceType>*>(&interfaceType);
	return nullptr;
}

template <typename InterfaceType>
inline const BulletTypeFromInterfaceType_t<InterfaceType>* BulletCast(const InterfaceType& interfaceType) {
	if (interfaceType.UsesFactory(*GetBulletFactory()))
		return static_cast<const BulletTypeFromInterfaceType_t<InterfaceType>*>(&interfaceType);
	return nullptr;
}

// Convert collision/rigid body object pointers
inline BulletRigidBody* FromBullet(const btCollisionObject* pBTCollisionObject) {
	if (!pBTCollisionObject)
		return nullptr;
	return static_cast<BulletRigidBody*>(pBTCollisionObject->getUserPointer());
}

inline BulletRigidBody* CastUserData(const btCollisionObject* pCollisionObject) {
	return static_cast<BulletRigidBody*>(pCollisionObject->getUserPointer());
}

inline BulletCollisionShape* CastUserData(const btCollisionShape* btCollisionShape) {
	return static_cast<BulletCollisionShape*>(btCollisionShape->getUserPointer());
}

} // namespace Physics
} // namespace KEP
