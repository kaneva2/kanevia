///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BulletRigidBodyDynamic.h"
#include "BulletFactory.h"

namespace KEP {
namespace Physics {

BulletRigidBodyDynamic::BulletRigidBodyDynamic(BulletFactory& factory) :
		RigidBodyDynamic(factory), PhysicsBase(factory), BulletRigidBody(factory), Collidable(factory) {
}

BulletRigidBodyDynamic::~BulletRigidBodyDynamic() {
}

bool BulletRigidBodyDynamic::Init(const RigidBodyDynamicProperties& props, CollisionShape* pShape, float fScale) {
	return false;
}

btRigidBody* BulletRigidBodyDynamic::GetBTRigidBody() const {
	return m_pRigidBody.get();
}

btCollisionObject* BulletRigidBodyDynamic::GetBTCollisionObject() const {
	return m_pRigidBody.get();
}

} // namespace Physics
} // namespace KEP
