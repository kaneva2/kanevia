///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "BulletCollidable.h"
#include "BulletCast.h"
#include "Core/Math/TransformUtil.h"
#include "../common/include/IMemSizeGadget.h"

namespace KEP {
namespace Physics {

BulletCollidable::BulletCollidable(BulletFactory& factory) :
		Collidable(factory), PhysicsBase(factory), m_bCollidable(true), m_iCollisionGroups(0xffff), m_iCollidesWithGroups(0xffff), m_pUniverse(nullptr) {
}

void BulletCollidable::SetIsCollidable(bool bCollidable) {
	if (m_bCollidable != bCollidable) {
		m_bCollidable = bCollidable;
		UpdateCollidable();
	}
}

void BulletCollidable::SetCollisionMasks(unsigned int groupMask, unsigned int collidesWithMask) {
	if (groupMask == m_iCollisionGroups && m_iCollidesWithGroups == collidesWithMask)
		return;

	m_iCollisionGroups = groupMask;
	m_iCollidesWithGroups = collidesWithMask;
	UpdateCollidable();
}

void BulletCollidable::GetCollisionMasks(Out<unsigned int> groupMask, Out<unsigned int> collidesWithMask) const {
	groupMask.set(GetCollisionGroupMask());
	collidesWithMask.set(GetCollidesWithMask());
}

void BulletCollidable::UpdateCollidable() {
	btCollisionObject* pBTCollisionObject = GetBTCollisionObject();
	if (!pBTCollisionObject)
		return;
#if 1
	btBroadphaseProxy* pBTBroadphaseProxy = pBTCollisionObject->getBroadphaseHandle();
	if (!pBTBroadphaseProxy)
		return;

	pBTBroadphaseProxy->m_collisionFilterGroup = m_iCollisionGroups;
	pBTBroadphaseProxy->m_collisionFilterMask = m_bCollidable ? m_iCollidesWithGroups : 0;
#else
	BulletUniverse* pUniverse = GetCurrentUniverse();
	if (!pUniverse)
		return;

	btDynamicsWorld* pBTWorld = pUniverse->GetBTWorld();

	unsigned int iCollisionGroups, iCollidesWithGroups;
	GetCollisionMasks(out(iCollisionGroups), out(iCollidesWithGroups));

	btRigidBody* pBTRigidBody = btRigidBody::upcast(pBTCollisionObject);
	if (pBTRigidBody) {
		pBTWorld->removeRigidBody(pBTRigidBody);
		pBTWorld->addRigidBody(pBTRigidBody, iCollisionGroups, iCollidesWithGroups);
	} else {
		pBTWorld->removeCollisionObject(pBTCollisionObject);
		pBTWorld->addCollisionObject(pBTCollisionObject, iCollisionGroups, iCollidesWithGroups);
	}
#endif
}

Matrix44f BulletCollidable::GetTransformMetric() const {
	Matrix44f transform;
	btCollisionObject* pBTCollisionObject = GetBTCollisionObject();

	if (pBTCollisionObject) {
		transform = FromBullet(pBTCollisionObject->getWorldTransform());
		PreTranslate(GetGraphicsGeometryOffset(), inout(transform));
	} else {
		transform.MakeTranslation(GetGraphicsGeometryOffset());
	}

	return transform;
}

bool BulletCollidable::SetTransformMetric(const Matrix44f& transform, bool bIgnoreScale) {
	return false;
}

void BulletCollidable::AddingToUniverse(BulletUniverse* pUniverse) {
	m_pUniverse = pUniverse;
}

void BulletCollidable::RemovingFromUniverse() {
	m_pUniverse = nullptr;
}

void BulletCollidable::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
	}
}

} // namespace Physics
} // namespace KEP
