///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../CollisionShape.h"
#include <vector>

class btCollisionShape;

namespace KEP {
namespace Physics {

class BulletFactory;

class BulletCollisionShape : public CollisionShape {
public:
	BulletCollisionShape(BulletFactory& factory, const CollisionShapeSphereProperties& props);
	BulletCollisionShape(BulletFactory& factory, const CollisionShapeBoxProperties& props);
	BulletCollisionShape(BulletFactory& factory, const CollisionShapeCapsuleProperties& props);
	BulletCollisionShape(BulletFactory& factory, const CollisionShapeConvexHullProperties& props);
	BulletCollisionShape(BulletFactory& factory, const CollisionShapeMeshProperties& props, const Vector3f& vScale);
	BulletCollisionShape(BulletFactory& factory, const std::vector<CollisionShapeMeshProperties>& props, const Vector3f& vScale);
	~BulletCollisionShape();

	btCollisionShape* GetBTCollisionShape() const { return m_pBTCollisionShape.get(); }
	virtual eCollisionShapeType GetType() override { return m_ShapeType; }
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	std::unique_ptr<btCollisionShape> m_pBTCollisionShape;
	//std::vector<size_t> m_aMeshStartIndices;
	eCollisionShapeType m_ShapeType;
};

} // namespace Physics
} // namespace KEP
