///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Core/Math/Matrix.h"
#include "Core/Math/Quaternion.h"
#include <bullet/btBulletDynamicsCommon.h>
#include <vector>

namespace KEP {
namespace Physics {

// Convert KEPMath types to/from bullet engine types.
inline btVector3 ToBullet(const Vector3f& v) {
	return btVector3(v.x, v.y, v.z);
}

inline Vector3f FromBullet(const btVector3& v) {
	return Vector3f(v.x(), v.y(), v.z());
}

inline btQuaternion ToBullet(const Quaternionf& q) {
	return btQuaternion(q.x, q.y, q.z, q.w);
}

inline Quaternionf FromBullet(const btQuaternion& q) {
	return Quaternionf(q.x(), q.y(), q.z(), q.w());
}

inline btTransform ToBullet(const Matrix44f& m) {
	btTransform transform;
	transform.setFromOpenGLMatrix(&m.RawData()[0][0]);
	return transform;
}

inline Matrix44f FromBullet(const btTransform& transform) {
	Matrix44f m;
	transform.getOpenGLMatrix(&m.RawData()[0][0]);
	return m;
}

} // namespace Physics
} // namespace KEP
