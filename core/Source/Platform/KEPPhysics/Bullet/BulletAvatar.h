///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "BulletRigidBody.h"
#include "../Avatar.h"
#include "Core/Math/RigidTransform.h"

class btCollisionObject;
class btConvexShape;

namespace KEP {

class IMemSizeGadget;

namespace Physics {

struct SweepTestResult;
class SweepTestArguments;
class BulletFactory;

class BulletAvatar
		: public Avatar,
		  public BulletRigidBody {
public:
	BulletAvatar(BulletFactory& factory);
	bool Init(const AvatarProperties& props);

	virtual bool AdjustAfterMove(Universe* pUniverse, const Vector3f& previousPosition, const Vector3f& currentPosition, Out<AvatarMoveResult> result);
	virtual bool SweepTest(Universe* pUniverse, const Vector3f& ptFrom, const Vector3f& ptTo, const std::vector<const Collidable*>* papIgnoreCollidable, Out<float> relativeMovement);

	virtual btRigidBody* GetBTRigidBody() const;
	virtual btCollisionObject* GetBTCollisionObject() const;
	virtual btConvexShape* GetCollisionShape() const { return m_pBTCollisionShape.get(); }

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const override final;

	Vector3f GetCollisionShapeOffset() const { return Vector3f(0, 0.5f * m_fHeight, 0); }

	// Capsule shapes add their radius to the effective step height since we allow standing on a non-central part of the capsule.
	// We could use a cylinder for the horizontal part of step movements to remove this effect.
	float GetStepHeight() const { return 0.5f; }

	float GetHeight() const { return m_fHeight; }
	float GetRadius() const { return m_fRadius; }

private:
	struct StepResult;
	void StepMove1(BulletUniverse* pBulletUniverse, const Vector3f& previousPosition, const Vector3f& currentPosition, const std::vector<const btCollisionObject*>& apIgnore, Out<StepResult> result);
	void StepMove2(BulletUniverse* pBulletUniverse, const Vector3f& previousPosition, const Vector3f& currentPosition, const std::vector<const btCollisionObject*>& apIgnore, Out<StepResult> stepResult);
	void StepMove3(BulletUniverse* pBulletUniverse, const Vector3f& previousPosition, const Vector3f& currentPosition, const std::vector<const btCollisionObject*>& apIgnore, Out<StepResult> stepResult);
	void MoveDownSimple(BulletUniverse* pBulletUniverse, const Vector3f& ptFrom, const Vector3f& ptTo, const std::vector<const btCollisionObject*>& apIgnore, Out<SweepTestResult> result);
	void MoveDown(BulletUniverse* pBulletUniverse, const Vector3f& ptFrom, const Vector3f& ptTo, const std::vector<const btCollisionObject*>& apIgnore, Out<StepResult> result); // Includes roundoff correction.
	bool FixCurrentCollision(BulletUniverse* pBulletUniverse, Out<std::vector<const btCollisionObject*>> currentColliders);
	StepResult MoveAndFollowCollision(BulletUniverse* pBulletUniverse, const Vector3f& vFollowPlaneNormal, const SweepTestArguments& sta);

	enum class eAvatarShapeType {
		Standard, // Capsule with height m_fHeight and radius m_fRadius
		HorizontalFull, // Capsule with height m_fHeight, slightly wider than m_fRadius.
		HorizontalStep, // Capsule of height (m_fHeight - GetStepHeight())
		Sphere, // Sphere of radius m_fRadius
	};
	SweepTestArguments GetSweepTestArgsForMove(const std::vector<const btCollisionObject*>& apIgnore, eAvatarShapeType shapeType = eAvatarShapeType::Standard);

private:
	//RigidTransformf m_CurrentTransform;
	float m_fRadius; // Radius of avatar
	float m_fHeight; // Overall height of avatar from lowest to highest point.

	std::unique_ptr<btDefaultMotionState> m_pMotionState; // To keep physics and client avatars in sync. Not currently used since avatar rigid body is not kept in the world.
	std::unique_ptr<btConvexShape> m_pBTCollisionShape; // Capsule shape
	std::unique_ptr<btConvexShape> m_pBTHorizontalCollisionShape; // Fatter shape for horizontal tests. Prevents getting stuck on walls.
	std::unique_ptr<btConvexShape> m_pBTStepCollisionShape; // Shape with step height removed from bottom.
	std::unique_ptr<btConvexShape> m_pBTSphereCollisionShape; // Sphere with radius equal to m_fRadius.
	std::unique_ptr<btRigidBody> m_pRigidBody; // Rigid body for avatar. Not needed for collision, but Bullet requires it for intersection tests (in our FixCurrentCollision())
};

} // namespace Physics
} // namespace KEP
