///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "../RigidBodyStatic.h"
#include "BulletRigidBody.h"
#include "../Collidable.h"

struct btDefaultMotionState;
class btCollisionShape;

namespace KEP {

class IMemSizeGadget;

namespace Physics {

class CollisionShape;
class BulletFactory;
class BulletCollisionShape;

class BulletRigidBodyStatic
		: public RigidBodyStatic,
		  public BulletRigidBody {
public:
	BulletRigidBodyStatic(BulletFactory& factory);
	virtual ~BulletRigidBodyStatic();

	bool Init(const RigidBodyStaticProperties& props, CollisionShape* pShape, const Vector3f& vShapeOffset, float fScale);

	virtual bool SetTransformMetric(const Matrix44f& transform, bool bIgnoreScale = false) override;

	virtual btRigidBody* GetBTRigidBody() const;
	virtual btCollisionObject* GetBTCollisionObject() const override final;

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const override final;

private:
	void CreateScaleShape(const Vector3f& vScaleFactors);
	btCollisionShape* GetCollisionShape();

private:
	std::shared_ptr<const BulletCollisionShape> m_pCollisionShape;
	std::unique_ptr<btRigidBody> m_pRigidBody;
	std::unique_ptr<btCollisionShape> m_pScaleShape;
	Vector3f m_vShapeOffset = Vector3f(0, 0, 0);
	float m_fScale = 1.0f;
};

} // namespace Physics
} // namespace KEP
