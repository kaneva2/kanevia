///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "BulletRigidBody.h"
#include "BulletCast.h"

namespace KEP {
namespace Physics {

BulletRigidBody::BulletRigidBody(BulletFactory& factory) :
		Collidable(factory), PhysicsBase(factory), BulletCollidable(factory) {
}

} // namespace Physics
} // namespace KEP
