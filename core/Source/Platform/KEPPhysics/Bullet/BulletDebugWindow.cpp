///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BulletDebugWindow.h"

#include "BulletCast.h"
#include "BulletUniverse.h"
#include "BulletVehicle.h"

#include "../../Common/include/LogHelper.h"
#include <iomanip>
#include <gl/GL.h>

static LogInstance("Physics");

#define WIN_WIDTH_DEFAULT 1024
#define WIN_HEIGHT_DEFAULT 700

namespace KEP {
namespace Physics {

// All BulletDebugWindows, indexed by HWND so we can look up the correct BulletDebugWindow in the MessageProc
std::map<HWND, std::shared_ptr<BulletDebugWindow>> BulletDebugWindow::s_WindowMap;

BulletDebugWindow::BulletDebugWindow(PhysicsFactory& factory, const DebugWindowOptions& opt) :
		DebugWindow(factory), PhysicsBase(factory), m_hGLRC(0), m_hWnd(nullptr), m_CurrentColor(0), m_Options(opt) {
}

BulletDebugWindow::~BulletDebugWindow() {
}

// Window procedure for BulletDebugWindow windows.
LRESULT APIENTRY BulletDebugWindow::StaticWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	if (message == WM_CREATE) {
		CREATESTRUCT* pCreateStruct = reinterpret_cast<CREATESTRUCT*>(lParam);
		BulletDebugWindow* pBulletDebugWindow = static_cast<BulletDebugWindow*>(pCreateStruct->lpCreateParams);
		std::shared_ptr<BulletDebugWindow> pSharedBulletDebugWindow = std::dynamic_pointer_cast<BulletDebugWindow>(pBulletDebugWindow->shared_from_this());
		if (pSharedBulletDebugWindow) {
			s_WindowMap.insert(std::make_pair(hWnd, pSharedBulletDebugWindow));
			pSharedBulletDebugWindow->m_hWnd = hWnd;
		}
		//itr = s_WindowMap.insert(std::make_pair(hWnd, std::make_unique<BulletDebugWindow>(hWnd))).first;
		//s_WindowMap[hWnd] = std::make_unique<BulletDebugWindow>(hWnd);
	}

	auto itr = s_WindowMap.find(hWnd);
	if (itr != s_WindowMap.end()) {
		BulletDebugWindow* pBulletDebugWindow = itr->second.get();

		switch (message) {
			case WM_CREATE:
				pBulletDebugWindow->InitializeOpenGL();
				break;
			case WM_NCDESTROY:
				pBulletDebugWindow->m_hWnd = NULL;
				s_WindowMap.erase(itr);
				break;
		}
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

bool BulletDebugWindow::InitializeOpenGL() {
	HDC hDC = GetDC(m_hWnd);

	PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),
		1, //version
		PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		32,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0, // RGBA bit layout
		0,
		0,
		0,
		0,
		0, //accumulation buffer
		16, // depth
		0, // stencil
		0, // aux
		PFD_MAIN_PLANE,
		0,
		0,
		0,
		0,
	};
	int iPixelFormat = ChoosePixelFormat(hDC, &pfd);
	if (iPixelFormat == 0) {
		LogWarn("ChoosePixelFormat failed");
		return false;
	}

	if (!SetPixelFormat(hDC, iPixelFormat, &pfd)) {
		LogWarn("SetPixelFormat failed");
		return false;
	}

	m_hGLRC = wglCreateContext(hDC);
	if (!m_hGLRC) {
		LogError("wglCreateContext failed");
		return false;
	}
	if (!wglMakeCurrent(hDC, m_hGLRC)) {
		LogError("wglMakeCurrent failed");
		return false;
	}

	ConfigureDefaultOpenGLSettings();
	return true;
}

bool BulletDebugWindow::ConfigureDefaultOpenGLSettings() {
	// Default OpenGL settings.
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);
	glDisable(GL_CULL_FACE);
	glDisable(GL_CULL_FACE);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	struct LightData {
		Vector4f ambient;
		Vector4f diffuse;
		Vector4f specular;
		Vector4f position;
	};

	static LightData aLightData[2] = {
		{
			Vector4f(.25f, .25f, .25f, 1.0f), // ambient
			Vector4f(.75f, .75f, .75f, 1.0f), // diffuse
			Vector4f(0.0f, 0.0f, 0.0f, 1.0f), // specular
			Vector4f(1.0f, 1.0f, 1.0f, 0.0f) // position
		},
		{
			Vector4f(0.0f, 0.0f, 0.0f, 0.0f), // ambient
			Vector4f(.5f, .5f, .5f, 1.0f), // diffuse
			Vector4f(0.0f, 0.0f, 0.0f, 1.0f), // specular
			Vector4f(-1.0f, -0.75f, -1.0f, 0.0f) // position
		},
	};
	static size_t nLights = std::extent<decltype(aLightData)>::value;
	for (size_t i = 0; i < GL_MAX_LIGHTS; ++i) {
		if (i < nLights) {
			glLightfv(GL_LIGHT0 + i, GL_AMBIENT, aLightData[i].ambient.RawData());
			glLightfv(GL_LIGHT0 + i, GL_DIFFUSE, aLightData[i].diffuse.RawData());
			glLightfv(GL_LIGHT0 + i, GL_SPECULAR, aLightData[i].specular.RawData());
			glLightfv(GL_LIGHT0 + i, GL_POSITION, aLightData[i].position.RawData());
			glEnable(GL_LIGHT0 + i);
		} else {
			glDisable(GL_LIGHT0 + i);
		}
	}

	return true;
}

std::shared_ptr<BulletDebugWindow> BulletDebugWindow::CreateBulletDebugWindow(PhysicsFactory& factory, const DebugWindowOptions& opt) {
	std::shared_ptr<BulletDebugWindow> pBulletDebugWindow;
	auto deleter = [](BulletDebugWindow* p) {
		if (p)
			p->Destroy();
		delete p;
	};
	pBulletDebugWindow.reset(new BulletDebugWindow(factory, opt), deleter);
	return pBulletDebugWindow;
}

bool BulletDebugWindow::CreateWindowsWindow() {
	if (m_hWnd)
		return true;

	static ATOM s_atomClass = 0;
	if (s_atomClass == 0) {
		WNDCLASS wndClass;
		wndClass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
		wndClass.lpfnWndProc = BulletDebugWindow::StaticWndProc;
		wndClass.cbClsExtra = 0;
		wndClass.cbWndExtra = 0;
		wndClass.hInstance = GetModuleHandle(0);
		wndClass.hIcon = 0;
		wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		wndClass.hbrBackground = NULL;
		wndClass.lpszMenuName = NULL;
		wndClass.lpszClassName = L"BulletDebugWindowClass";
		s_atomClass = RegisterClass(&wndClass);
	}
	HWND hWndParent = m_Options.m_hWndParent;
	HWND hWnd = CreateWindowEx(0, reinterpret_cast<const wchar_t*>(s_atomClass), Utf8ToUtf16(m_strTitle).c_str(), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 0, 0, 0, 0, hWndParent, NULL, NULL, this);
	if (!hWnd) {
		LogError("Window creation error");
		return false;
	}

	SetWindowPos(hWnd, HWND_TOP, 0, 0, WIN_WIDTH_DEFAULT, WIN_HEIGHT_DEFAULT, SWP_SHOWWINDOW | SWP_NOACTIVATE);
	return true;
}

bool BulletDebugWindow::IsValid() {
	return m_hWnd != NULL;
}

bool BulletDebugWindow::Show(bool bShow) {
	if (bShow) {
		if (!m_hWnd) {
			if (!CreateWindowsWindow())
				return false;
			ShowWindow(m_hWnd, SW_SHOWNOACTIVATE);
		}
		return true;
	} else {
		Destroy();
		return true;
	}
}

void BulletDebugWindow::SetTitle(const char* pszTitle) {
	if (pszTitle == m_strTitle)
		return;

	m_strTitle = pszTitle;
	if (m_hWnd)
		SetWindowTextW(m_hWnd, Utf8ToUtf16(m_strTitle).c_str());
}

void BulletDebugWindow::Destroy() {
	if (!m_hWnd)
		return;
	wglMakeCurrent(0, 0);
	wglDeleteContext(m_hGLRC);
	DestroyWindow(m_hWnd);
}

static inline void glColor(const Vector4f& vColor) {
	glColor4f(vColor.X(), vColor.Y(), vColor.Z(), vColor.W());
}

static inline void glVertex(const Vector3f& pt) {
	glVertex3f(pt.X(), pt.Y(), pt.Z());
}

static void RenderBoxWireFrame(const Vector3f& vMin, const Vector3f& vMax, const Vector4f& vColor = Vector4f(1, 1, 1, 1)) {
	Vector4f vBlack(0, 0, 0, 1);

	Vector3f apts[24] = {
		Vector3f(vMin.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMin.Z()),
		Vector3f(vMin.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMin.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMin.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMin.Z()),

		Vector3f(vMax.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMin.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMin.Z()),

		Vector3f(vMin.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMin.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMin.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMin.Z()),
	};

	glEnableClientState(GL_VERTEX_ARRAY);

	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, vColor.RawData());
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, vBlack.RawData());
	glVertexPointer(3, GL_FLOAT, 0, apts);
	glDrawArrays(GL_LINES, 0, 24);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, vBlack.RawData());

	glDisableClientState(GL_VERTEX_ARRAY);
}

static void RenderBox(const Vector3f& vMin, const Vector3f& vMax) {
	// Back face culling is disabled, so it's simpler to render opposite box sides using the same winding direction.
	static const Vector3f aNormals[24] = {
		//Vector3f(-1,0,0), Vector3f(-1,0,0), Vector3f(-1,0,0), Vector3f(-1,0,0),
		Vector3f(1, 0, 0),
		Vector3f(1, 0, 0),
		Vector3f(1, 0, 0),
		Vector3f(1, 0, 0),
		Vector3f(1, 0, 0),
		Vector3f(1, 0, 0),
		Vector3f(1, 0, 0),
		Vector3f(1, 0, 0),
		//Vector3f(0,-1,0), Vector3f(0,-1,0), Vector3f(0,-1,0), Vector3f(0,-1,0),
		Vector3f(0, 1, 0),
		Vector3f(0, 1, 0),
		Vector3f(0, 1, 0),
		Vector3f(0, 1, 0),
		Vector3f(0, 1, 0),
		Vector3f(0, 1, 0),
		Vector3f(0, 1, 0),
		Vector3f(0, 1, 0),
		//Vector3f(0,0,-1), Vector3f(0,0,-1), Vector3f(0,0,-1), Vector3f(0,0,-1),
		Vector3f(0, 0, 1),
		Vector3f(0, 0, 1),
		Vector3f(0, 0, 1),
		Vector3f(0, 0, 1),
		Vector3f(0, 0, 1),
		Vector3f(0, 0, 1),
		Vector3f(0, 0, 1),
		Vector3f(0, 0, 1),
	};

	Vector3f apts[24] = {
		Vector3f(vMin.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMin.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMin.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMin.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMax.Z()),

		Vector3f(vMin.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMin.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMin.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMin.Z()),

		Vector3f(vMin.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMin.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMin.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMin.Z()),
		Vector3f(vMin.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMin.Y(), vMax.Z()),
		Vector3f(vMax.X(), vMax.Y(), vMax.Z()),
		Vector3f(vMin.X(), vMax.Y(), vMax.Z()),
	};

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, apts);
	glNormalPointer(GL_FLOAT, 0, aNormals);
	glDrawArrays(GL_QUADS, 0, 24);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}

struct VertexPN {
	Vector3f m_ptPosition;
	Vector3f m_vNormal;
};

static void RenderTriangles(const VertexPN* pVerts, size_t n) {
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glVertexPointer(3, GL_FLOAT, sizeof(*pVerts), &pVerts->m_ptPosition);
	glNormalPointer(GL_FLOAT, sizeof(*pVerts), &pVerts->m_vNormal);
	glDrawArrays(GL_TRIANGLES, 0, n);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}

// type: GL_TRIANGLES, GL_TRIANGLE_STRIP, GL_LINES, GL_QUADS
static void RenderIndices(int type, const VertexPN* pVerts, unsigned short* pIndices, size_t nIndices) {
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glVertexPointer(3, GL_FLOAT, sizeof(*pVerts), &pVerts->m_ptPosition);
	glNormalPointer(GL_FLOAT, sizeof(*pVerts), &pVerts->m_vNormal);
	glDrawElements(type, nIndices, GL_UNSIGNED_SHORT, pIndices);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}

static void RenderMesh(const btBvhTriangleMeshShape* pMeshShape) {
	if (pMeshShape == nullptr)
		return;

	std::vector<VertexPN> aVertices;
	aVertices.reserve(1024);

	const btStridingMeshInterface* pMeshInterface = pMeshShape->getMeshInterface();
	size_t nSubParts = pMeshInterface->getNumSubParts();
	for (size_t iPart = 0; iPart < nSubParts; ++iPart) {
		const unsigned char* pFirstPoint;
		int iPointStride;
		PHY_ScalarType iPointType;
		int iPointCount;

		const unsigned char* pFirstIndex;
		int iIndexStride;
		PHY_ScalarType iIndexType;
		int nTriangles;
		pMeshInterface->getLockedReadOnlyVertexIndexBase(
			&pFirstPoint, iPointCount, iPointType, iPointStride,
			&pFirstIndex, iIndexStride, nTriangles, iIndexType,
			iPart);
		if (iPointType != PHY_FLOAT || iIndexType != PHY_SHORT) {
			LogError("Unexpected mesh format: " << iPointType << ", " << iIndexType);
			continue;
		}

		for (size_t i = 0; i < nTriangles; ++i) {
			unsigned short idx0 = reinterpret_cast<const unsigned short*>(pFirstIndex + i * iIndexStride)[0];
			unsigned short idx1 = reinterpret_cast<const unsigned short*>(pFirstIndex + i * iIndexStride)[1];
			unsigned short idx2 = reinterpret_cast<const unsigned short*>(pFirstIndex + i * iIndexStride)[2];

			const Vector3f& pt0 = *reinterpret_cast<const Vector3f*>(pFirstPoint + idx0 * iPointStride);
			const Vector3f& pt1 = *reinterpret_cast<const Vector3f*>(pFirstPoint + idx1 * iPointStride);
			const Vector3f& pt2 = *reinterpret_cast<const Vector3f*>(pFirstPoint + idx2 * iPointStride);

			Vector3f vNormal = (pt1 - pt0).Cross(pt2 - pt0);

			aVertices.emplace_back(VertexPN{ pt0, vNormal });
			aVertices.emplace_back(VertexPN{ pt1, vNormal });
			aVertices.emplace_back(VertexPN{ pt2, vNormal });
		}
		pMeshInterface->unLockReadOnlyVertexBase(iPart);
	}

	btVector3 meshScaling = pMeshInterface->getScaling();
	bool bApplyScaling = (meshScaling.x() != 1 || meshScaling.y() != 1 || meshScaling.z() != 1);
	if (bApplyScaling) {
		glPushMatrix();
		glScalef(meshScaling.x(), meshScaling.y(), meshScaling.z());
	}
	RenderTriangles(aVertices.data(), aVertices.size());
	if (bApplyScaling)
		glPopMatrix();
}

static bool GetBoundingBox(const btCollisionShape* pBTCollisionShape, Out<Vector3f> vMin, Out<Vector3f> vMax) {
	if (!pBTCollisionShape)
		return false;

	btVector3 vBTMin, vBTMax;
	pBTCollisionShape->getAabb(btTransform::getIdentity(), vBTMin, vBTMax);
	vMin.get() = FromBullet(vBTMin);
	vMax.get() = FromBullet(vBTMax);
	return true;
}

static bool GetBoundingBox(const btRigidBody* pBTRigidBody, Out<Vector3f> vMin, Out<Vector3f> vMax) {
	if (!pBTRigidBody)
		return false;

	btVector3 vBTMin, vBTMax;
	pBTRigidBody->getAabb(vBTMin, vBTMax);
	vMin.get() = FromBullet(vBTMin);
	vMax.get() = FromBullet(vBTMax);
	return true;
}

static void RenderBoundingBox(const btCollisionShape* pBTCollisionShape, bool bWireFrame) {
	if (pBTCollisionShape == nullptr)
		return;
	btTransform identityTransform = btTransform::getIdentity();
	btVector3 vBTMin, vBTMax;
	pBTCollisionShape->getAabb(btTransform::getIdentity(), vBTMin, vBTMax);
	Vector3f vMin, vMax;
	GetBoundingBox(pBTCollisionShape, out(vMin), out(vMax));
	if (bWireFrame)
		RenderBoxWireFrame(vMin, vMax);
	else
		RenderBox(vMin, vMax);
}

static inline void RenderCylinder(const Vector3f& pos, const Vector3f& axis, float fHeight, float fRadius) {
	size_t iMin = 0;
	for (size_t i = 1; i < 3; ++i) {
		if (std::abs(axis[i]) < std::abs(axis[iMin]))
			iMin = i;
	}
	Vector3f vTowardTop = axis.GetNormalized();
	Vector3f vTowardBottom = -vTowardTop;
	Vector3f vHalfHeight = vTowardTop * (0.5f * fHeight);
	Vector3f vRadiusX = Vector3f::Basis(iMin).Cross(axis).GetNormalized() * fRadius;
	Vector3f vRadiusY = axis.Cross(vRadiusX);

	static const float rsqrt2 = ReciprocalSqrt(2.0f);
	Vector2f aptsCircle8[] = {
		Vector2f(1, 0),
		Vector2f(rsqrt2, rsqrt2),
		Vector2f(0, 1),
		Vector2f(-rsqrt2, rsqrt2),
		Vector2f(-1, 0),
		Vector2f(-rsqrt2, -rsqrt2),
		Vector2f(0, -1),
		Vector2f(rsqrt2, -rsqrt2),
	};
	Vector3f ptTop = pos + vHalfHeight;
	Vector3f ptBottom = pos - vHalfHeight;
	VertexPN aVertexPN[32];
	for (size_t i = 0; i < 8; ++i) {
		Vector3f vRadius = aptsCircle8[i][0] * vRadiusX + aptsCircle8[i][1] * vRadiusY;
		aVertexPN[i].m_ptPosition = ptTop + vRadius;
		aVertexPN[i].m_vNormal = vTowardTop;
		aVertexPN[8 + i].m_ptPosition = ptBottom + vRadius;
		aVertexPN[8 + i].m_vNormal = vTowardBottom;
		aVertexPN[16 + 2 * i].m_ptPosition = aVertexPN[i].m_ptPosition;
		aVertexPN[16 + 2 * i].m_vNormal = vRadius;
		aVertexPN[16 + 2 * i + 1].m_ptPosition = aVertexPN[8 + i].m_ptPosition;
		aVertexPN[16 + 2 * i + 1].m_vNormal = vRadius;
	}

	static unsigned short aIndices[] = {
		16,
		17,
		18,
		19,
		20,
		21,
		22,
		23,
		24,
		25,
		26,
		27,
		28,
		29,
		30,
		31,
		16,
		17,

		17,
		0,

		0,
		1,
		7,
		2,
		6,
		3,
		5,
		4,

		4,
		8,
		8,

		8,
		9,
		15,
		10,
		14,
		11,
		13,
		12,

	};

	RenderIndices(GL_TRIANGLE_STRIP, aVertexPN, aIndices, sizeof(aIndices) / sizeof(aIndices[0]));
}

static inline void RenderSphere(const Vector3f& pos, float fRadius) {
	static const float fSqrtPt8 = std::sqrt(.8f); // = 0.89442719099991587856366946749251
	static const float fSineFifth = std::sin(2 * M_PI / 5);
	static const float fCosineFifth = std::cos(2 * M_PI / 5);
	static const float fSineTwoFifth = std::sin(4 * M_PI / 5);
	static const float fCosineTwoFifth = std::cos(4 * M_PI / 5);
	static Vector3f apts[12] = {
		/*
		Vector3f(0, 0, 1),
		Vector3f(0.894427, 0, 0.447214),
		Vector3f(0.276393, 0.850651, 0.447214),
		Vector3f(-0.723607, 0.525731, 0.447214),
		Vector3f(-0.723607, -0.525731, 0.447214),
		Vector3f(0.276393, -0.850651, 0.447214),
		Vector3f(0.723607, 0.525731, -0.447214),
		Vector3f(-0.276393, 0.850651, -0.447214),
		Vector3f(-0.894427, 0, -0.447214),
		Vector3f(-0.276393, -0.850651, -0.447214),
		Vector3f(0.723607, -0.525731, -0.447214),
		Vector3f(0, 0, -1),
		*/
		Vector3f(0, 0, 1),
		Vector3f(fSqrtPt8, 0, 0.5 * fSqrtPt8),
		Vector3f(fSqrtPt8 * fCosineFifth, fSqrtPt8 * fSineFifth, 0.5 * fSqrtPt8),
		Vector3f(fSqrtPt8 * fCosineTwoFifth, fSqrtPt8 * fSineTwoFifth, 0.5 * fSqrtPt8),
		Vector3f(fSqrtPt8 * fCosineTwoFifth, -fSqrtPt8 * fSineTwoFifth, 0.5 * fSqrtPt8),
		Vector3f(fSqrtPt8 * fCosineFifth, -fSqrtPt8 * fSineFifth, 0.5 * fSqrtPt8),
		Vector3f(-fSqrtPt8 * fCosineTwoFifth, fSqrtPt8 * fSineTwoFifth, -0.5 * fSqrtPt8),
		Vector3f(-fSqrtPt8 * fCosineFifth, fSqrtPt8 * fSineFifth, -0.5 * fSqrtPt8),
		Vector3f(-fSqrtPt8, 0, -0.5 * fSqrtPt8),
		Vector3f(-fSqrtPt8 * fCosineFifth, -fSqrtPt8 * fSineFifth, -0.5 * fSqrtPt8),
		Vector3f(-fSqrtPt8 * fCosineTwoFifth, -fSqrtPt8 * fSineTwoFifth, -0.5 * fSqrtPt8),
		Vector3f(0, 0, -1),
	};
	size_t aIdx[60] = {
		0, 1, 2,
		0, 2, 3,
		0, 3, 4,
		0, 4, 5,
		0, 5, 1,

		1, 6, 2,
		2, 7, 3,
		3, 8, 4,
		4, 9, 5,
		5, 10, 1,

		6, 2, 7,
		7, 3, 8,
		8, 4, 9,
		9, 5, 10,
		10, 1, 6,

		11, 6, 7,
		11, 7, 8,
		11, 8, 9,
		11, 9, 10,
		11, 10, 6
	};
	static Vector3f avNormals[20] = {
		Vector3f(0.4911235, 0.3568221, 0.7946545),
		Vector3f(-0.1875925, 0.5773503, 0.7946545),
		Vector3f(-0.607062, 0, 0.7946545),
		Vector3f(-0.1875925, -0.5773503, 0.7946545),
		Vector3f(0.4911235, -0.3568221, 0.7946545),
		Vector3f(0.7946545, 0.5773503, 0.1875925),
		Vector3f(-0.3035311, 0.9341723, 0.1875925),
		Vector3f(-0.982247, 0, 0.1875924),
		Vector3f(-0.3035311, -0.9341723, 0.1875925),
		Vector3f(0.7946545, -0.5773503, 0.1875925),
		Vector3f(-0.3035311, -0.9341723, 0.1875925),
		Vector3f(0.7946545, -0.5773503, 0.1875925),
		Vector3f(0.7946545, 0.5773503, 0.1875925),
		Vector3f(-0.3035311, 0.9341723, 0.1875925),
		Vector3f(-0.982247, 0, 0.1875924),
		Vector3f(-0.1875925, -0.5773503, 0.7946545),
		Vector3f(0.4911235, -0.3568221, 0.7946545),
		Vector3f(0.4911235, 0.3568221, 0.7946545),
		Vector3f(-0.1875925, 0.5773503, 0.7946545),
		Vector3f(-0.607062, 0, 0.7946545),
	};
	/*
	for( size_t i = 0; i < 60; i += 3 ) {
		Vector3f apt[3] = { apts[aIdx[i]], apts[aIdx[i+1]], apts[aIdx[i+2]]};
		Vector3f v1 = apt[1] - apt[0];
		Vector3f v2 = apt[2] - apt[0];
		Vector3f vN = v1.Cross(v2);
		vN.Normalize();
		avNormals[i/3] = vN;
	}
	*/

	VertexPN aVertexPN[60];
	for (size_t i = 0; i < 60; ++i) {
		aVertexPN[i].m_ptPosition = pos + fRadius * apts[aIdx[i]];
		aVertexPN[i].m_vNormal = avNormals[i / 3];
		std::swap(aVertexPN[i].m_ptPosition.y, aVertexPN[i].m_ptPosition.z);
		std::swap(aVertexPN[i].m_vNormal.y, aVertexPN[i].m_vNormal.z);
	}

	RenderTriangles(aVertexPN, sizeof(aVertexPN) / sizeof(aVertexPN[0]));
}

static inline void RenderCapsule(const Vector3f& pos, const Vector3f& axis, float fHeight, float fRadius) {
	size_t iMin = 0;
	for (size_t i = 1; i < 3; ++i) {
		if (std::abs(axis[i]) < std::abs(axis[iMin]))
			iMin = i;
	}
	Vector3f vTowardTop = axis.GetNormalized();
	Vector3f vTowardBottom = -vTowardTop;
	Vector3f vHalfHeight = vTowardTop * (0.5f * fHeight);
	Vector3f vRadiusNmlzX = Vector3f::Basis(iMin).Cross(axis).GetNormalized();
	Vector3f vRadiusNmlzY = axis.Cross(vRadiusNmlzX);

	static const float rsqrt2 = ReciprocalSqrt(2.0f);
	Vector2f aptsCircle8[] = {
		Vector2f(1, 0),
		Vector2f(rsqrt2, rsqrt2),
		Vector2f(0, 1),
		Vector2f(-rsqrt2, rsqrt2),
		Vector2f(-1, 0),
		Vector2f(-rsqrt2, -rsqrt2),
		Vector2f(0, -1),
		Vector2f(rsqrt2, -rsqrt2),
	};
	Vector3f ptCylTop = pos + vHalfHeight;
	Vector3f ptCylBottom = pos - vHalfHeight;
	float fReducedRadius = fRadius * rsqrt2;
	Vector3f ptReducedTop = ptCylTop + fReducedRadius * vTowardTop;
	Vector3f ptReducedBottom = ptCylBottom - fReducedRadius * vTowardTop;
	VertexPN aVertexPN[34];
	for (size_t i = 0; i < 32; i += 4) {
		Vector3f vRadius = aptsCircle8[i / 4][0] * vRadiusNmlzX + aptsCircle8[i / 4][1] * vRadiusNmlzY;
		aVertexPN[i + 0].m_ptPosition = ptReducedTop + vRadius * fReducedRadius;
		aVertexPN[i + 0].m_vNormal = aVertexPN[i + 0].m_ptPosition - ptCylTop;
		aVertexPN[i + 1].m_ptPosition = ptCylTop + vRadius * fRadius;
		aVertexPN[i + 1].m_vNormal = vRadius;
		aVertexPN[i + 2].m_ptPosition = ptCylBottom + vRadius * fRadius;
		aVertexPN[i + 2].m_vNormal = vRadius;
		aVertexPN[i + 3].m_ptPosition = ptReducedBottom + vRadius * fReducedRadius;
		aVertexPN[i + 3].m_vNormal = aVertexPN[i + 3].m_ptPosition - ptCylBottom;
	}
	aVertexPN[32].m_ptPosition = ptCylTop + vTowardTop * fRadius;
	aVertexPN[32].m_vNormal = vTowardTop;
	aVertexPN[33].m_ptPosition = ptCylBottom - vTowardTop * fRadius;
	aVertexPN[33].m_vNormal = -vTowardTop;

	static unsigned short aIndices[] = {
		32,
		32,
		0,
		4,
		1,
		5,
		2,
		6,
		3,
		7,
		33,
		33,
		32,
		32,
		4,
		8,
		5,
		9,
		6,
		10,
		7,
		11,
		33,
		33,
		32,
		32,
		8,
		12,
		9,
		13,
		10,
		14,
		11,
		15,
		33,
		33,
		32,
		32,
		12,
		16,
		13,
		17,
		14,
		18,
		15,
		19,
		33,
		33,
		32,
		32,
		16,
		20,
		17,
		21,
		18,
		22,
		19,
		23,
		33,
		33,
		32,
		32,
		20,
		24,
		21,
		25,
		22,
		26,
		23,
		27,
		33,
		33,
		32,
		32,
		24,
		28,
		25,
		29,
		26,
		30,
		27,
		31,
		33,
		33,
		32,
		32,
		28,
		0,
		29,
		1,
		30,
		2,
		31,
		3,
		33,
		33,
	};

	RenderIndices(GL_TRIANGLE_STRIP, aVertexPN, aIndices, sizeof(aIndices) / sizeof(aIndices[0]));
}

static void RenderCollisionShape(const btCollisionShape* pBTCollisionShape) {
	if (pBTCollisionShape) {
		switch (pBTCollisionShape->getShapeType()) {
			case SCALED_TRIANGLE_MESH_SHAPE_PROXYTYPE: {
				const btScaledBvhTriangleMeshShape* pScaledMeshShape = dynamic_cast<const btScaledBvhTriangleMeshShape*>(pBTCollisionShape);
				const btBvhTriangleMeshShape* pMeshShape = nullptr;
				if (pScaledMeshShape)
					pMeshShape = pScaledMeshShape->getChildShape();
				//glPushMatrix();
				Vector3f vScale = FromBullet(pScaledMeshShape->getLocalScaling());
				glScalef(vScale.X(), vScale.Y(), vScale.Z());
				if (pMeshShape)
					RenderMesh(pMeshShape);
				else
					RenderBoundingBox(pBTCollisionShape, false);
				break;
			}
			case TRIANGLE_MESH_SHAPE_PROXYTYPE: {
				const btBvhTriangleMeshShape* pMeshShape = dynamic_cast<const btBvhTriangleMeshShape*>(pBTCollisionShape);
				if (pMeshShape)
					RenderMesh(pMeshShape);
				else
					RenderBoundingBox(pBTCollisionShape, false);
				break;
			}
			case EMPTY_SHAPE_PROXYTYPE:
				break;
			case SPHERE_SHAPE_PROXYTYPE: {
				const btSphereShape* pSphere = dynamic_cast<const btSphereShape*>(pBTCollisionShape);
				RenderSphere(Vector3f(0, 0, 0), pSphere->getRadius());
				break;
			}
			case CAPSULE_SHAPE_PROXYTYPE: {
				const btCapsuleShape* pCapsule = dynamic_cast<const btCapsuleShape*>(pBTCollisionShape);
				float fHalfHeight = pCapsule->getHalfHeight();
				float fRadius = pCapsule->getRadius();
				Vector3f vAxis(0, 0, 0);
				vAxis[pCapsule->getUpAxis()] = 1;
				//RenderCylinder(Vector3f(0,0,0), vAxis, 2*fHalfHeight, fRadius);
				RenderCapsule(Vector3f(0, 0, 0), vAxis, 2 * fHalfHeight, fRadius);
				break;
			}
			case UNIFORM_SCALING_SHAPE_PROXYTYPE: {
				const btUniformScalingShape* pScalingShape = dynamic_cast<const btUniformScalingShape*>(pBTCollisionShape);
				float fScale = pScalingShape->getUniformScalingFactor();
				glScalef(fScale, fScale, fScale);
				RenderCollisionShape(pScalingShape->getChildShape());
				break;
			}
			default: {
				RenderBoundingBox(pBTCollisionShape, false);
				break;
			}
		}
	}
}

static void RenderCollisionShape(BulletCollisionShape* pShape) {
	if (pShape == nullptr)
		return;
	btCollisionShape* pBTCollisionShape = pShape->GetBTCollisionShape();
	RenderCollisionShape(pBTCollisionShape);
}

static void RenderBoxAt(const Vector3f& ptPosition, float fSize) {
	float fHalfSize = 0.5f * fSize;
	Vector3f vHalfSize(fHalfSize, fHalfSize, fHalfSize);
	RenderBox(ptPosition - vHalfSize, ptPosition + vHalfSize);
}

static inline Vector4f ToFloatColor(uint32_t clr) {
	static const float fInv255 = 1.0f / 255.0f;
	return Vector4f(
		((clr >> 16) & 0xff) * fInv255,
		((clr >> 8) & 0xff) * fInv255,
		((clr >> 0) & 0xff) * fInv255,
		1.0f);
}

static Matrix44f CreateViewMatrix(const CameraProperties& camProps) {
	Matrix44f mtxCamera;
	const Vector3f& ptPos = camProps.m_ptCameraPositionMeters;
	const Vector3f& vForward = camProps.m_vViewDirection;
	const Vector3f vUp = camProps.m_vViewUp.GetPartPerpendicularTo(vForward);
	Vector3f vRight = vForward.Cross(vUp);
	if (vRight != Vector3f::Zero()) {
		Vector3f vX = vRight.GetNormalized();
		Vector3f vY = vUp.GetNormalized();
		Vector3f vZ = -vForward.GetNormalized();
		mtxCamera.SetColumn(0, vX, -ptPos.Dot(vX));
		mtxCamera.SetColumn(1, vY, -ptPos.Dot(vY));
		mtxCamera.SetColumn(2, vZ, -ptPos.Dot(vZ));
		mtxCamera.SetColumn(3, Vector4f(0, 0, 0, 1));
	} else {
		mtxCamera.MakeIdentity();
	}

	return mtxCamera;
}

static Matrix44f CreateProjectionMatrix(const CameraProperties& camProps, float fViewportWidth, float fViewportHeight) {
	float fScaleX, fScaleY;
	if (camProps.m_fRelativeViewWidth * fViewportHeight > camProps.m_fRelativeViewHeight * fViewportWidth) {
		// Width constrained
		fScaleX = 1.0 / camProps.m_fRelativeViewWidth;
		fScaleY = fScaleX * fViewportWidth / fViewportHeight;
	} else {
		// Height constrained
		fScaleY = 1.0 / camProps.m_fRelativeViewHeight;
		fScaleX = fScaleY * fViewportHeight / fViewportWidth;
	}
	float fNear = camProps.m_fNear;
	float fFar = camProps.m_fFar;

	// Note that the reversal of the X coordinate reverses the expected forward/backward faces.
	// Counter-clockwise faces in a right-handed coordinate system become clockwise after this projection.
	// If face culling is enabled, everything works out because the normal gets reversed for the back
	// (original CCW, now CW) faces because 2-sided lighting is enabled.
	glMatrixMode(GL_PROJECTION);
	float fReciprocalNearMinusFar = 1.0f / (fNear - fFar);
	Matrix44f mtxProjection{
		-fScaleX, 0.0f, 0.0f, 0.0f, // Negative so that we match the the client's left-handed coordinate system.
		0.0f, fScaleY, 0.0f, 0.0f,
		0.0f, 0.0f, (fFar + fNear) * fReciprocalNearMinusFar, -1.0f,
		0.0f, 0.0f, 2 * fFar * fNear * fReciprocalNearMinusFar, 0.0f
	};
	return mtxProjection;
}

bool BulletDebugWindow::RenderBegin() {
	if (!m_hWnd)
		return false;

	HDC hDC = GetDC(m_hWnd);

	RECT rcClip;
	if (GetClipBox(hDC, &rcClip) == NULLREGION)
		return false;

	RECT rc;
	GetClientRect(m_hWnd, &rc);
	float fWindowWidth = rc.right - rc.left;
	float fWindowHeight = rc.bottom - rc.top;

	wglMakeCurrent(hDC, m_hGLRC);
	glViewport(0, 0, fWindowWidth, fWindowHeight);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Matrix44f mtxProjection = CreateProjectionMatrix(m_CameraProperties, fWindowWidth, fWindowHeight);
	glLoadMatrixf(mtxProjection.RawData()[0]);

	glMatrixMode(GL_MODELVIEW);

	m_CurrentColor = 0;

	return true;
}

bool BulletDebugWindow::RenderEnd() {
	bool bSucceeded = true;
	if (!SwapBuffers(GetDC(m_hWnd))) {
		LogError("SwapBuffers error");
		bSucceeded = false;
	}

	wglMakeCurrent(NULL, NULL);
	return bSucceeded;
}

bool BulletDebugWindow::Render(Universe* pUniverse) {
	BulletUniverse* pBulletUniverse = BulletCast(pUniverse);
	if (!pBulletUniverse)
		return false;

	Matrix44f mtxCamera = CreateViewMatrix(m_CameraProperties);

	static volatile bool bRenderNonCollidable = false;
	std::vector<BulletRigidBody*> apRigidBodies;
	pBulletUniverse->GetRigidBodies(out(apRigidBodies));
	for (BulletRigidBody* pRigidBody : apRigidBodies) {
		const btCollisionObject* pBTCollisionObject = pRigidBody->GetBTCollisionObject();
		if (pBTCollisionObject->getBroadphaseHandle()->m_collisionFilterMask == 0) {
			if (!bRenderNonCollidable)
				continue;
			//if( pBTCollisionObject->getBroadphaseHandle()->m_collisionFilterGroup == 0 )
			//	continue;
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		} else {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}

		//Matrix44f worldTransform = pRigidBody->GetTransformMetric();
		Matrix44f worldTransform = FromBullet(pBTCollisionObject->getWorldTransform());
		Matrix44f modelViewMatrix = worldTransform * mtxCamera;
		glLoadMatrixf(modelViewMatrix.RawData()[0]);

		//glColor(ToFloatColor(ColorFromPointer(pBTCollisionObject)));
		glColor(ToFloatColor(NextColor()));
		RenderCollisionShape(pBTCollisionObject->getCollisionShape());

		BulletVehicle* pVehicle = dynamic_cast<BulletVehicle*>(pRigidBody);
		if (pVehicle) {
			glColor(ToFloatColor(NextColor()));
			Matrix44f aWheelTransforms[4];
			pVehicle->GetWheelTransforms(out(aWheelTransforms));
			float fRadius = pVehicle->GetVehicleProperties().m_fWheelRadiusMeters;
			float fThickness = pVehicle->GetVehicleProperties().m_fWheelThicknessMeters;
			for (size_t i = 0; i < 4; ++i) {
				modelViewMatrix = aWheelTransforms[i] * mtxCamera;
				glLoadMatrixf(modelViewMatrix.RawData()[0]);
				RenderCylinder(Vector3f(0, 0, 0), Vector3f(1, 0, 0), fThickness, fRadius);
			}
		}
	}

	return true;
}

bool BulletDebugWindow::Render(Avatar* pAvatar, const Matrix44f& transform) {
	BulletAvatar* pBulletAvatar = BulletCast(pAvatar);
	if (!pBulletAvatar)
		return false;

	Matrix44f mtxCamera = CreateViewMatrix(m_CameraProperties);
	Matrix44f mtxModelView = transform * mtxCamera;
	glLoadMatrixf(mtxModelView.RawData()[0]);
	Vector3f vOffset = pBulletAvatar->GetCollisionShapeOffset();
	glTranslatef(vOffset.X(), vOffset.Y(), vOffset.Z());

	Vector3f vMin, vMax;
	GetBoundingBox(pBulletAvatar->GetCollisionShape(), out(vMin), out(vMax));
	float fRadius = pBulletAvatar->GetRadius();
	float fHeight = pBulletAvatar->GetHeight();
	Vector3f vBoxOffset(.1f, .1f, .1f);
	//RenderBoxWireFrame(vMin+vBoxOffset, vMax-vBoxOffset);
	glColor(ToFloatColor(NextColor()));
	RenderCapsule(0.5f * (vMin + vMax), Vector3f(0, 1, 0), fHeight - 2 * fRadius, fRadius);

	return true;
}

void BulletDebugWindow::SetCameraProperties(const CameraProperties& camProps) {
	m_CameraProperties = camProps;
}

} // namespace Physics
} // namespace KEP
