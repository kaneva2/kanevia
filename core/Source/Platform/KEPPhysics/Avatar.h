///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "PhysicsBase.h"
#include "Collidable.h"
#include "Declarations.h"
#include "Core/Util/Parameter.h"
#include "Core/Math/Quaternion.h"

namespace KEP {

class IMemSizeGadget;

namespace Physics {

struct AvatarProperties : public CollidableProperties {
	AvatarProperties() :
			m_fMassKG(75.0f), m_fWidthMeters(.75f), m_fHeightMeters(1.75f), m_ptMassCenter(0, 0, 0), m_vRotationalInertia(0, 0, 0), m_qRotationalInertiaOrientation(0, 0, 0, 1) {}

	float m_fMassKG;

	// Dimensions of the capsule used for collision.
	float m_fWidthMeters;
	float m_fHeightMeters;

	Vector3f m_ptMassCenter; // Center of mass of object in its local coordinate system.

	Vector3f m_vRotationalInertia; // Magnitude of rotational inertia along X, Y, and Z axes
	Quaternionf m_qRotationalInertiaOrientation; // Rotation of X, Y, and Z axes to get rotational inertia in local coordinates.
};

struct AvatarMoveResult {
	AvatarMoveResult() {}

	// This variable is always set. It is set to the end point of the path if no collision occurs.
	Vector3f m_ptFinal;

	struct Collision {
		Collidable* m_pCollidable;
		Vector3f m_ptContact;
		Vector3f m_vContactNormal;
	};
	std::vector<Collision> m_aCollisions;
};

class Avatar : public virtual Collidable {
public:
	Avatar(PhysicsFactory& factory) :
			Collidable(factory) {}

	virtual bool AdjustAfterMove(Universe* pUniverse, const Vector3f& previousPosition, const Vector3f& currentPosition, Out<AvatarMoveResult> result) = 0;
	virtual bool SweepTest(Universe* pUniverse, const Vector3f& ptFrom, const Vector3f& ptTo, const std::vector<const Collidable*>* papIgnoreCollidable, Out<float> relativeMovement) = 0;
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;
};

} // namespace Physics
} // namespace KEP
