///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {
namespace Physics {

struct AvatarProperties;
struct CollisionShapeBoxProperties;
struct CollisionShapeCapsuleProperties;
struct CollisionShapeMeshProperties;
struct CollisionShapeSphereProperties;
struct DebugWindowOptions;
struct RigidBodyDynamicProperties;
struct RigidBodyStaticProperties;
struct UniverseOptions;
struct VehicleProperties;
struct SensorProperties;

class Avatar;
class Collidable;
class CollisionShape;
class RigidBodyDynamic;
class RigidBodyStatic;
class Universe;
class Vehicle;
class Sensor;

class DebugWindow;
} // namespace Physics
} // namespace KEP
