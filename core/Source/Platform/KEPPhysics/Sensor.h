///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "PhysicsBase.h"
#include "Collidable.h"
#include "CollisionShape.h"
#include "Declarations.h"

namespace KEP {

class IMemSizeGadget;

namespace Physics {

struct SensorProperties : CollidableProperties {
	ObjectType m_objectType = ObjectType::NONE;
	int m_Pid = 0;
	Matrix44f m_transform;
};

struct BoxShapeSensorProperties : SensorProperties {
	CollisionShapeBoxProperties m_BoxShapeSensorProps;
};

class Sensor : public virtual Collidable {
public:
	Sensor(PhysicsFactory& factory) :
			Collidable(factory), PhysicsBase(factory) {}
	virtual bool IsObjectBounded(int pid) const = 0;
	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;
};

} // namespace Physics
} // namespace KEP
