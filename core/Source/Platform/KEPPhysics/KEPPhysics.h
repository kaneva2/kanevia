///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Avatar.h"
#include "Collidable.h"
#include "CollisionShape.h"
//#include "DebugWindow.h"
#include "PhysicsFactory.h"
#include "RigidBodyDynamic.h"
#include "RigidBodyStatic.h"
#include "Universe.h"
#include "UserData.h"
