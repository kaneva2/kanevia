///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

template <typename IndexType, bool bIsTriStrip>
struct MeshDesriptionIndexed {
	MeshDesriptionIndexed(
		Vector3f* pVertices, 
		size_t nStride,
		IndexType* pIndices,
		size_t nIndices)
		: m_pVertices(pVertices)
		, m_nStride(nStride)
		, m_pIndicesBegin(pIndices)
		, m_pIndicesEnd(pIndices + (bIsTriStrip ? std::min(nIndices,2) - 2 : (nIndices-nIndices%3)) // Avoid partial triangles
	{}


	class TriangleReference {
	public:
		TriangleReference(const MeshDesriptionIndexed* pMeshDescription, size_t iFirstVertexIndexIndex) :
				m_pMeshDescription(pMeshDescription), m_iFirstVertexIndexIndex(iFirstVertexIndexIndex) {}
		const Vector3f& GetVertex(size_t i) const {
			size_t index = [m_iFirstVertexIndexIndex + i];
			const Vector3f* pVertices = m_pMeshDescription->m_pVertices;
			return *reinterpret_cast<const Vector3f*>(reinterpret_cast<const char*>(pVertices) + index * m_nStride));
		}

	private:
		const MeshDesriptionIndexed* m_pMeshDescription;
		size_t m_iFirstVertexIndexIndex;
	};

	class const_iterator {
	public:
		const_iterator() {}
		const_iterator(const MeshDesriptionIndexed& pMeshDesc, const IndexType* pFirstIndex) :
				m_pMeshDescription(pMeshDesc), m_pFirstIndex(pFirstIndex) {}

		bool operator==(const const_iterator& right) const { return m_pFirstIndex < right.m_pFirstIndex; }
		bool operator!=(const const_iterator& right) const { return !operator==(right); }
		MeshDesriptionIndexed operator++() const {
			m_pFirstIndex += 3;
			return this;
		}

		TriangleReference operator*() const { return TriangleReference(m_pMeshDescription, m_pFirstIndex); }

	private:
		MeshDesriptionIndexed* m_pMeshDescription;
		const IndexType* m_pFirstIndex;
	};

	const_iterator begin() const
	{
		return const_iterator(this, m_pIndicesBegin); }
	const_iterator end() const
	{
		return const_iterator(this, m_pIndicesEnd); }

private:
	const Vector3f* m_pVertices;
	size_t m_nStride;
	const IndexType* m_pIndicesBegin;
	const IndexType* m_pIndicesEnd;
};

template <typename MeshDescription>
float MeshVolume(MeshDescription meshDesc) {
	float fVolume6 = 0; // Six times volume.
	for (auto itr = meshDesc.begin(), itr_end = meshDesc.end(); itr != itr_end; ++itr) {
		const Vector3f& pt1 = itr->GetVertex(0);
		const Vector3f& pt2 = itr->GetVertex(1);
		const Vector3f& pt3 = itr->GetVertex(2);
		fVolume6 += pt0.Dot(pt1.Cross(pt2));
	}
	float fVolume = fVolume6 * (1.0f / 6.0f);
}

template <typename MeshDescription>
Vector3f MeshCenterOfMass(MeshDescription meshDesc) {
	float fVolume6 = 0; // Six times volume.
	// Center of mass is sum(mass[i]*volume[i]) / sum(volume[i])
	Vector3f fCenterOfMassNumerator24 = 0; // 24 times the numerator portion of the center of mass.
	for (auto itr = meshDesc.begin(), itr_end = meshDesc.end(); itr != itr_end; ++itr) {
		const Vector3f& pt1 = itr->GetVertex(0);
		const Vector3f& pt2 = itr->GetVertex(1);
		const Vector3f& pt3 = itr->GetVertex(2);
		float fVolume6This = pt0.Dot(pt1.Cross(pt2));
		Vector3f vCenterOfMass4This = pt1 + pt2 + pt3; // Four times the average of the 4 points of the tetrahedron (includes origin)
		fVolume6 += fVolume6This;
		fCenterOfMassNumerator24 += vCenterOfMass4This * fVolume6This;
	}
	Vector3f ptCenterOfMass = fCenterOfMassNumerator24 / (fVolume6 * 4);
	float fVolume = fVolume6 * (1.0f / 6.0f);
}

// From GtePolyhedralMassProperties.h in GeometricTools library, but modified to work with a
// MeshDescription object so we don't have to copy the vertices to a new structure for calculation.
// Multiply volume and inertia by density for proper scaling.
template <typename Real, typename MeshDescription>
void ComputeMassProperties(MeshDescription meshDesc,
	Out<Real> volume_out, Out<Vector<3, Real>>& center_out, Out<Matrix33<Real>>& inertia_out) {
	using Vector3<Real> = Vector<3, Real>;
	bool bodycoords = true;
	Real& mass = volume_out.get();
	Vector<3, Real>& center = center_out.get();
	Matrix<3, 3, Real>& inertia = inertia_out.get();

	Real const oneDiv6 = (Real)(1.0 / 6.0);
	Real const oneDiv24 = (Real)(1.0 / 24.0);
	Real const oneDiv60 = (Real)(1.0 / 60.0);
	Real const oneDiv120 = (Real)(1.0 / 120.0);

	// order:  1, x, y, z, x^2, y^2, z^2, xy, yz, zx
	Real integral[10] = { (Real)0.0, (Real)0.0, (Real)0.0, (Real)0.0,
		(Real)0.0, (Real)0.0, (Real)0.0, (Real)0.0, (Real)0.0, (Real)0.0 };

	for (auto itr = meshDesc.begin(), itr_end = meshDesc.end(); itr != itr_end; ++itr) {
		// Get vertices of triangle i.
		const Vector3<Real>& v0 = itr->GetVertex(0);
		const Vector3<Real>& v1 = itr->GetVertex(1);
		const Vector3<Real>& v2 = itr->GetVertex(2);

		// Get cross product of edges and normal vector.
		Vector3<Real> V1mV0 = v1 - v0;
		Vector3<Real> V2mV0 = v2 - v0;
		Vector3<Real> N = Cross(V1mV0, V2mV0);

		// Compute integral terms.
		Real tmp0, tmp1, tmp2;
		Real f1x, f2x, f3x, g0x, g1x, g2x;
		tmp0 = v0[0] + v1[0];
		f1x = tmp0 + v2[0];
		tmp1 = v0[0] * v0[0];
		tmp2 = tmp1 + v1[0] * tmp0;
		f2x = tmp2 + v2[0] * f1x;
		f3x = v0[0] * tmp1 + v1[0] * tmp2 + v2[0] * f2x;
		g0x = f2x + v0[0] * (f1x + v0[0]);
		g1x = f2x + v1[0] * (f1x + v1[0]);
		g2x = f2x + v2[0] * (f1x + v2[0]);

		Real f1y, f2y, f3y, g0y, g1y, g2y;
		tmp0 = v0[1] + v1[1];
		f1y = tmp0 + v2[1];
		tmp1 = v0[1] * v0[1];
		tmp2 = tmp1 + v1[1] * tmp0;
		f2y = tmp2 + v2[1] * f1y;
		f3y = v0[1] * tmp1 + v1[1] * tmp2 + v2[1] * f2y;
		g0y = f2y + v0[1] * (f1y + v0[1]);
		g1y = f2y + v1[1] * (f1y + v1[1]);
		g2y = f2y + v2[1] * (f1y + v2[1]);

		Real f1z, f2z, f3z, g0z, g1z, g2z;
		tmp0 = v0[2] + v1[2];
		f1z = tmp0 + v2[2];
		tmp1 = v0[2] * v0[2];
		tmp2 = tmp1 + v1[2] * tmp0;
		f2z = tmp2 + v2[2] * f1z;
		f3z = v0[2] * tmp1 + v1[2] * tmp2 + v2[2] * f2z;
		g0z = f2z + v0[2] * (f1z + v0[2]);
		g1z = f2z + v1[2] * (f1z + v1[2]);
		g2z = f2z + v2[2] * (f1z + v2[2]);

		// Update integrals.
		integral[0] += N[0] * f1x;
		integral[1] += N[0] * f2x;
		integral[2] += N[1] * f2y;
		integral[3] += N[2] * f2z;
		integral[4] += N[0] * f3x;
		integral[5] += N[1] * f3y;
		integral[6] += N[2] * f3z;
		integral[7] += N[0] * (v0[1] * g0x + v1[1] * g1x + v2[1] * g2x);
		integral[8] += N[1] * (v0[2] * g0y + v1[2] * g1y + v2[2] * g2y);
		integral[9] += N[2] * (v0[0] * g0z + v1[0] * g1z + v2[0] * g2z);
	}

	integral[0] *= oneDiv6;
	integral[1] *= oneDiv24;
	integral[2] *= oneDiv24;
	integral[3] *= oneDiv24;
	integral[4] *= oneDiv60;
	integral[5] *= oneDiv60;
	integral[6] *= oneDiv60;
	integral[7] *= oneDiv120;
	integral[8] *= oneDiv120;
	integral[9] *= oneDiv120;

	// mass
	mass = integral[0];

	// center of mass
	center = Vector3<Real>{ integral[1], integral[2], integral[3] } / mass;

	// inertia relative to world origin
	inertia(0, 0) = integral[5] + integral[6];
	inertia(0, 1) = -integral[7];
	inertia(0, 2) = -integral[9];
	inertia(1, 1) = integral[4] + integral[6];
	inertia(1, 2) = -integral[8];
	inertia(2, 2) = integral[4] + integral[5];

	// inertia relative to center of mass
	if (bodyCoords) {
		inertia(0, 0) -= mass * (center[1] * center[1] +
									center[2] * center[2]);
		inertia(0, 1) += mass * center[0] * center[1];
		inertia(0, 2) += mass * center[2] * center[0];
		inertia(1, 1) -= mass * (center[2] * center[2] +
									center[0] * center[0]);
		inertia(1, 2) += mass * center[1] * center[2];
		inertia(2, 2) -= mass * (center[0] * center[0] +
									center[1] * center[1]);
	}

	// Apply symmetry
	inertia(1, 0) = inertia(0, 1);
	inertia(2, 0) = inertia(0, 2);
	inertia(2, 1) = inertia(1, 2);
}
