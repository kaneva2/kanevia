///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "PhysicsDLL.h"
#include "PhysicsBase.h"
#include "Declarations.h"
#include "Core/Math/Matrix.h"
#include "Core/Util/Parameter.h"

#include <functional>

namespace KEP {

class IMemSizeGadget;

namespace Physics {

struct UniverseOptions {
	UniverseOptions() :
			m_vGravityMPS2(0, -9.8f, 0), m_fDesiredStepDuration(1.0f / 60.0f) // 60 fps simulation
			,
			m_nMaxSubStepCount(60) // Accuracy is not compromised until frame time > 1 second
	{}

	Vector3f m_vGravityMPS2;

	float m_fDesiredStepDuration; // Step duration used if below conditions can be satisfied.
	int m_nMaxSubStepCount; // Larger values may take longer to simulate when frames fall behind.

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
};

struct CollideOptions {
	CollideOptions& SetUseCollidableMasks(bool bUseCollidableMasks) {
		m_bUseCollidableMasks = bUseCollidableMasks;
		return *this;
	}
	CollideOptions& SetCollisionMasks(uint16_t iCollisionGroupMask, uint16_t iCollidesWithMask) {
		m_bUseCollidableMasks = false;
		m_iCollisionGroupMask = iCollisionGroupMask;
		m_iCollidesWithMask = iCollidesWithMask;
		return *this;
	}
	CollideOptions& SetKeepDistFromObstacles(float fKeepDistFromObstacles) {
		m_fKeepDistFromObstacles = fKeepDistFromObstacles;
		return *this;
	}
	CollideOptions& SetFilter(std::function<bool(const Collidable*)> filter) {
		m_Filter = std::move(filter);
		return *this;
	}

	bool GetUseCollidableMasks() const { return m_bUseCollidableMasks; }
	uint16_t GetCollisionGroupMask() const { return m_iCollisionGroupMask; }
	uint16_t GetCollidesWithMask() const { return m_iCollidesWithMask; }
	float GetKeepDistFromObstacles() const { return m_fKeepDistFromObstacles; }

	const std::function<bool(const Collidable*)>& GetFilter() const { return m_Filter; }

private:
	bool m_bUseCollidableMasks = true; // When true, ignores m_iCollisionGroupMask and m_iCollidesWithMask and gets those values from the object being collided.
	uint16_t m_iCollisionGroupMask = 0xffff;
	uint16_t m_iCollidesWithMask = 0xffff;
	float m_fKeepDistFromObstacles = 0.0f;

	std::function<bool(const Collidable*)> m_Filter;
};

struct RayTestResult {
	float m_fRelativeHitLocation = (std::numeric_limits<float>::max)();
	Collidable* m_pHitObject = nullptr;
	Vector3f m_vNormal = Vector3f(0, 0, 0);
	size_t m_iPart = 0;
	size_t m_iIndex = 0;
};

struct RayTestOptions {
	RayTestOptions(uint16_t iCollisionGroupMask = 0xffff) { m_iCollisionGroupMask = iCollisionGroupMask; }

	RayTestOptions(const RayTestOptions&) = default;
	RayTestOptions(RayTestOptions&&) = default;
	RayTestOptions& operator=(const RayTestOptions&) = default;
	RayTestOptions& operator=(RayTestOptions&&) = default;

	RayTestOptions& SetCollidableFilter(std::function<bool(Collidable*)> collidableFilter) {
		m_CollidableFilter = std::move(collidableFilter);
		return *this;
	}
	RayTestOptions& SetSubPartFilter(std::function<bool(Collidable* pCollidable, size_t iPart, size_t iTriangle)> subPartFilter) {
		m_SubPartFilter = std::move(subPartFilter);
		return *this;
	}
	RayTestOptions& SetCollisionGroupMask(uint16_t iMask) {
		m_iCollisionGroupMask = iMask;
		return *this;
	}

	uint16_t m_iCollisionGroupMask;
	std::function<bool(Collidable*)> m_CollidableFilter;
	std::function<bool(Collidable* pCollidable, size_t iPart, size_t iTriangle)> m_SubPartFilter;
};

class Universe : public virtual PhysicsBase {
protected:
	Universe(PhysicsFactory& factory) :
			PhysicsBase(factory) {}

public:
	virtual bool Add(RigidBodyStatic* pRigidBodyStatic) = 0;
	virtual bool Add(const std::shared_ptr<RigidBodyStatic>& pRigidBodyStatic) = 0;
	virtual bool Add(const std::shared_ptr<Vehicle>& pVehicle) = 0;
	virtual bool Add(const std::shared_ptr<Sensor>& pSensor) = 0;
	virtual bool FindSensor(int pid, std::shared_ptr<Sensor>& pSensor) = 0;
	virtual bool Remove(RigidBodyStatic* pRigidBodyStatic) = 0;
	virtual bool Remove(Vehicle* pVehicle) = 0;
	virtual bool Remove(Sensor* pSensor) = 0;
	virtual bool RemoveAll() = 0;

	// fDistFromObstacles, if a collision is detected, the distance will be adjusted by (fDistFromObstacles / cosine(collision_normal)) to stay
	// the specified distance from colliding.
	virtual bool BackOutFromCollision(const Collidable& collidable, const Vector3f& vBackOutDirection, Out<float> relativeMovement, float fDistFromObstacles = 0.01f) = 0;
	virtual bool Collide(const Collidable& collidable, const Matrix44f& mtxFrom, const Matrix44f& mtxTo, Out<float> relativeMovement, const CollideOptions& collideOptions = CollideOptions()) = 0;
	virtual bool Collide(const Collidable& collidable, const Vector3f& ptFrom, const Vector3f& ptTo, Out<float> relativeMovement, const CollideOptions& collideOptions = CollideOptions()) = 0;
	virtual bool RayTest(const Vector3f& ptStart, const Vector3f& vDir, float fMaxDist, RayTestOptions options, Out<RayTestResult> result) = 0;

	// Frame RealTime is used for user input processing. Delta physics time is used for simulation.
	virtual void Step(double dFrameStartRealTime, double dDeltaPhysicsTime) = 0;

	// Output physics profiling data.
	virtual void DumpProfile() = 0;
	virtual void DumpProfile(std::ostringstream& strm) = 0;

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const = 0;
};

} // namespace Physics
} // namespace KEP
