///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "PhysicsDLL.h"
#include "Declarations.h"
#include <memory>
#include <vector>
#include <Core/Math/Vector.h>

namespace KEP {
namespace Physics {

class PhysicsFactory {
public:
	virtual std::shared_ptr<DebugWindow> CreateDebugWindow(const DebugWindowOptions& opt) = 0;

	virtual std::shared_ptr<Universe> CreateUniverse(const UniverseOptions& opt) = 0;
	virtual std::shared_ptr<Avatar> CreateAvatar(const AvatarProperties& props) = 0;
	virtual std::shared_ptr<Vehicle> CreateVehicle(const VehicleProperties& props) = 0;
	virtual std::shared_ptr<Sensor> CreateSensor(const SensorProperties& props) = 0;
	virtual std::shared_ptr<RigidBodyStatic> CreateRigidBodyStatic(const RigidBodyStaticProperties& props, CollisionShape* pShape, const Vector3f& vShapeOffset, float fScale) = 0;

	virtual std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeCapsuleProperties& props) = 0;
	virtual std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeMeshProperties& props, const Vector3f& vScale) = 0;
	virtual std::shared_ptr<CollisionShape> CreateCollisionShape(const std::vector<CollisionShapeMeshProperties>& props, const Vector3f& vScale) = 0;
	virtual std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeSphereProperties& props) = 0;
	virtual std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeBoxProperties& props) = 0;
};

enum class eFactoryType {
	Bullet,
};

KEPPHYSICS_API PhysicsFactory* GetPhysicsFactory(eFactoryType type = eFactoryType::Bullet);

inline std::shared_ptr<DebugWindow> CreateDebugWindow(const DebugWindowOptions& opt) {
	return GetPhysicsFactory()->CreateDebugWindow(opt);
}
inline std::shared_ptr<Universe> CreateUniverse(const UniverseOptions& opt) {
	return GetPhysicsFactory()->CreateUniverse(opt);
}
inline std::shared_ptr<Avatar> CreateAvatar(const AvatarProperties& props) {
	return GetPhysicsFactory()->CreateAvatar(props);
}
inline std::shared_ptr<Vehicle> CreateVehicle(const VehicleProperties& props) {
	return GetPhysicsFactory()->CreateVehicle(props);
}
inline std::shared_ptr<Sensor> CreateSensor(const SensorProperties& props) {
	return GetPhysicsFactory()->CreateSensor(props);
}
inline std::shared_ptr<RigidBodyStatic> CreateRigidBodyStatic(const RigidBodyStaticProperties& props, CollisionShape* pShape, const Vector3f& vShapeOffset, float fScale) {
	return GetPhysicsFactory()->CreateRigidBodyStatic(props, pShape, vShapeOffset, fScale);
}
inline std::shared_ptr<RigidBodyStatic> CreateRigidBodyStatic(const RigidBodyStaticProperties& props, CollisionShape* pShape, float fScale) {
	return CreateRigidBodyStatic(props, pShape, Vector3f(0, 0, 0), fScale);
}

inline std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeCapsuleProperties& props) {
	return GetPhysicsFactory()->CreateCollisionShape(props);
}
inline std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeMeshProperties& props, const Vector3f& vScale = Vector3f(0, 0, 0)) {
	return GetPhysicsFactory()->CreateCollisionShape(props, vScale);
}
inline std::shared_ptr<CollisionShape> CreateCollisionShape(const std::vector<CollisionShapeMeshProperties>& props, const Vector3f& vScale = Vector3f(0, 0, 0)) {
	return GetPhysicsFactory()->CreateCollisionShape(props, vScale);
}
inline std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeBoxProperties& props) {
	return GetPhysicsFactory()->CreateCollisionShape(props);
}
inline std::shared_ptr<CollisionShape> CreateCollisionShape(const CollisionShapeSphereProperties& props) {
	return GetPhysicsFactory()->CreateCollisionShape(props);
}

} // namespace Physics
} // namespace KEP
