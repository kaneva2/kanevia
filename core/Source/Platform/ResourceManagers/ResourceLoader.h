///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <IResource.h>
#include <mutex>
#include <condition_variable>
#include <map>
#include <atomic>
#include <set>
#include <Event/Base/IEventHandler.h> // for EVENT_PROC_RC

namespace KEP {

class ContentMetadata;
class ResourceDownloadCompletionEvent;

class ResourceLoader {
	typedef IResource::eQueuedState eQueuedState; // Create alias for frequently used type defined in IResource scope.

public:
	static ResourceLoader& Instance() { return s_Instance; }
	void Init();
	void Shutdown();
	void CancelAll();
	void Wake();

	bool Add(IResource* pResource, GLID glidForMetadata);
	void WaitForCompletion(IResource* pResource);
	EVENT_PROC_RC ProcessDownloadCompletionEvent(ResourceDownloadCompletionEvent* rdce);

private:
	ResourceLoader();
	~ResourceLoader();

	void ResourceMetadataReady(const ContentMetadata& md, IResource* pResource);
	static bool Errored(IResource* pResource, IResource::Error errorCode);
	static bool ResourceFinishedLoading(IResource* pResource, eQueuedState expectedState, IResource::Error errorCode = IResource::Error::ERROR_NONE);
	static bool VerifyChangeState(IResource* pResource, eQueuedState oldState, eQueuedState newState);
	static bool VerifyState(IResource* pResource, eQueuedState expectedState);
	static bool ChangeState(IResource* pResource, eQueuedState oldState, eQueuedState newState);

private:
	// forward declarations.
	class LoadThreadData;
	class CreateThreadData;
	template <IResource::eQueuedState stateInQueue>
	class ResourceQueue;

	static ResourceLoader s_Instance;
	std::unique_ptr<LoadThreadData> m_pLoadThreadData;
	std::unique_ptr<CreateThreadData> m_pCreateThreadData;

	// For lazy thread initialization. Don't want to create threads in a static constructor.
	std::mutex m_mtxThreadsInitialized;
	std::atomic<bool> m_bThreadsInitialized = false;

	// To support legacy synchronous operations. Should be removed as soon as possible.
	std::mutex m_mtxWaitingResources;
	std::condition_variable m_cvWaitingResources;
	std::multimap<IResource*, bool> m_mapWaitingResources;

	// Objects in this set will be loaded once their metadata is ready.
	std::mutex m_mtxWaitingMetadata;
	std::set<IResource*> m_setLoadAwaitingMetadata;
};

} // namespace KEP
