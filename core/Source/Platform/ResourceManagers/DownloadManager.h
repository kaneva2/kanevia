///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\include\KEPHelpers.h"

#include "DownloadPriority.h"
#include "DownloadHandler.h"
#include <string>
#include <vector>
#include <js.h>
#include <jsSemaphore.h>
#include <jsThreadTypes.h>
#include "Core/Util/fast_mutex.h"
#include <Event/Base/IEvent.h>

// NOTE: Web Caller Shared With Patch Downloader (HTTPWorker.cpp)!
#define DM_WEBCALLER "Downloader"
#define DM_THREADS 4
#define DM_TIMEOUT_MS (60.0 * 1000.0)
#define DM_THREAD_JS_PRIORITY (jsThdPriority) PR_NORMAL
#define DM_PRELOAD_PRIORITY DL_PRIO_LOW // queue priority (not thread!)

namespace KEP {

class IDispatcher;
class IClientEngine;
class DownloadManagerThread;
typedef std::vector<IEventSharedPtr> EventVector;
enum class ResourceType;

class DownloadInfo {
private:
	// no copy
	DownloadInfo(const DownloadInfo& rhs);
	DownloadInfo& operator=(const DownloadInfo& rhs);

public:
	// Download URL to disk file, with optional event callback
	DownloadInfo(
		ResourceType jobType,
		const std::string& url,
		DownloadPriority prio,
		const std::string& path,
		FileSize expectedFileSize,
		bool decompress,
		IEvent* e,
		IDispatcher* disp,
		bool preload) :
			m_jobType(jobType),
			m_url(url),
			m_priority(prio),
			m_path(path),
			m_expectedFileSize(expectedFileSize),
			m_compressed(decompress),
			m_dispatcher(disp),
			m_handler(nullptr),
			m_preload(preload) {
		if (e != 0) {
			m_events.push_back(e);
		}
	}

	// Download URL with data handler (handler is required)
	DownloadInfo(
		ResourceType jobType,
		const std::string& url,
		DownloadPriority prio,
		DownloadHandler* handler,
		bool preload) :
			m_jobType(jobType),
			m_url(url),
			m_priority(prio),
			m_expectedFileSize(0),
			m_compressed(false),
			m_dispatcher(nullptr),
			m_handler(handler),
			m_preload(preload) {
		ASSERT(m_handler != nullptr);
	}

	// Returns true if the referenced file is already on disk and of the expected size
	// or false if not, optionally deleting the file if the size is not as expected.
	bool FileOk(bool deleteIfBad);

	// Returns true if the referenced url is valid (not empty and begins with 'http').
	bool UrlOk();

	void FireProgressCompleteEvent(int retVal, fast_recursive_mutex* pMutex);

	const std::string& getUrl() const {
		return m_url;
	}

	const std::string& getPath() const {
		return m_path;
	}

	bool isPreload() const {
		return m_preload;
	}

	ResourceType getJobType() const {
		return m_jobType;
	}

	bool isCompressed() const {
		return m_compressed;
	}

	const EventVector& getEvents() const {
		return m_events;
	}

	DownloadHandler* getHandler() const {
		return m_handler;
	}

	void addEvent(IEvent* e) {
		m_events.push_back(e);
	}

	virtual DownloadPriority getPriority() const {
		ASSERT(m_priority != DL_PRIO_DYN);
		if (m_priority == DL_PRIO_DYN) {
			// Shouldn't be here
			// A class use DL_PRIO_DYN should override getPriority() function
			return DL_PRIO_MEDIUM;
		}
		return m_priority;
	}

private:
	ResourceType m_jobType;
	std::string m_url;
	DownloadPriority m_priority;
	std::string m_path;
	FileSize m_expectedFileSize;
	bool m_compressed;
	EventVector m_events;
	IDispatcher* m_dispatcher;
	DownloadHandler* m_handler;
	bool m_preload;
};

class DownloadManager {
public:
	static DownloadManager* s_pDM;
	static DownloadManager* Ptr() {
		return s_pDM;
	}

	static IClientEngine* s_pCE;
	static IClientEngine* PtrCE() {
		return s_pCE;
	}

	DownloadManager(IDispatcher& disp, IClientEngine* pCE);

	~DownloadManager();

	bool DownloadFileSync(ResourceType jobType, const std::string& url, DownloadHandler* handler);
	bool DownloadFileAsync(DownloadInfo* pDI);

	// When called, the background thread will toss out events to indicate (to the ZoneDownloader) the
	// progress of any pre-load files that are still queued.
	bool ArmPreLoadProgress(IEvent* e);

	void CancelAll();
	void CancelJobsByType(ResourceType jobType, bool bSendEvents);

	unsigned int TotalPreloads();

	DownloadInfo* popNextTask();

protected:
	bool DownloadFileSync(DownloadInfo* pDI);

	typedef std::vector<DownloadInfo*> DownloadInfoQueue;

	DownloadInfoQueue m_queue;

	typedef std::vector<DownloadManagerThread*> DownloadThreadVector;

	bool m_shutdown;

	fast_recursive_mutex m_sync;
	jsSemaphore m_sema;
	IDispatcher& m_dispatcher;

	size_t m_threadCount;
	jsThdPriority m_threadPriority;
	DownloadThreadVector m_threadVector;

	IEvent* m_preLoadProgressEvent;
	unsigned int m_totalPreLoads;
	unsigned int m_remainingPreLoads;

	void QueueDeleteAll();
	bool QueueFind(const std::string& url, IEvent* e = nullptr);
	void QueuePush(DownloadInfo* info);

	friend class DownloadManagerThread;
};

} // namespace KEP
