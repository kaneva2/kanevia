///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <direct.h>

#include "SkeletonManager.h"
#include "SkeletonProxy.h"
#include "KEPHelpers.h"
#include "RuntimeSkeleton.h"
#include "ResourceType.h"

namespace KEP {

TypedResourceManager<SkeletonProxy>* TypedResourceManager<SkeletonProxy>::ms_pInstance = nullptr;

SkeletonRM::SkeletonRM(const std::string& id) :
		TypedResourceManager(id) {
	// Make sure we have a runtime skeleton that can be used
	// for GLID 0, i.e. an object that doesn't have a skeleton.
	std::shared_ptr<RuntimeSkeleton> skel = RuntimeSkeleton::New();
	AddRuntimeSkeleton(0, skel.get());
}

SkeletonRM::~SkeletonRM() {
}

// \brief Return a path pointing to where resource should be stored locally
// \param gameFilesDir a path pointed to "GameFiles" folder of current game
//@@Assumption: caller must pass in a path pointed to "GameFiles" folder of current game.
std::string SkeletonRM::GetResourceLocalStore(const std::string& gameFilesDir) {
	std::string skelDir;
	if (gameFilesDir == "~") // If parent folder is not provided, use game default
		skelDir = IResource::BaseAnimPath(eAnimPath::CharacterAnimations);
	else
		skelDir = PathAdd(gameFilesDir, "AnimatedSkeletalSystem");

	// Create directory if not already exists
	FileHelper::CreateFolderDeep(skelDir);

	return skelDir;
}

//! \brief Compose a file name for serialization based on hash code provided.
//! \param hash Hash code for the resource to be serialized.
std::string SkeletonRM::GetResourceFileName(UINT64 hash) {
	UINT64 glid = hash;
	CStringA fileName;
	fileName.Format("%u.ass", glid);
	return fileName.GetString();
}

std::string SkeletonRM::GetResourceFilePath(UINT64 hash) {
	return PathAdd(IResource::BaseAnimPath(eAnimPath::CharacterAnimations), GetResourceFileName(hash));
}

SkeletonProxy* SkeletonRM::GetSkeletonProxy(int glid, bool bAutoFetch) {
	auto pSP = static_cast<SkeletonProxy*>(FindResource(glid));
	if (pSP || !bAutoFetch)
		return pSP;

	// Add the resource to the ResourceManager's map of managed resources.
	// Download of the data starts to happen when GetData is called on
	// the resource.
	UINT64 resKey = (UINT64)glid;
	AddResource(resKey);

	// Because a clone is stored by AddResource, I need to get and
	// return it, not the tmp that was created above.
	pSP = static_cast<SkeletonProxy*>(FindResource(glid));

	return pSP;
}

SkeletonProxy* SkeletonRM::AddRuntimeSkeleton(GLID glid, RuntimeSkeleton* pRS) {
	// Will either get the extant one, or create a new one and add it to the resource
	// cache.  This might not work as intended (since this is a local resource that
	// I'm going to initialize right away).  Do some experiments and find out.
	SkeletonProxy* pSP = GetSkeletonProxy(glid, true);
	if (!pSP || !pSP->StateIsInitializedNotLoaded())
		return pSP;

	// If a new one was created by GetSkeletonProxy, initialize it here.  Right now,
	// downloading loose skeleton files won't work.  They are going to arrive in the
	// DOB files for dynamic objects that have skeletons.
	pRS->Clone(&pSP->m_skeleton, NULL, NULL, NULL, NULL, true, false);
	pSP->SetState(IResource::State::LOADED_AND_CREATED);

	return pSP;
}

void SkeletonRM::DestroyRuntimeSkeleton(GLID glid) {
	SkeletonProxy* pSP = GetSkeletonProxy(glid, false);
	if (!pSP || !pSP->StateIsLoadedAndCreated())
		return;

	pSP->m_skeleton.reset();
	pSP->SetState(IResource::State::INITIALIZED_NOT_LOADED);
}

} // namespace KEP