///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "KEPMacros.h"
#include "jsThread.h"
#include "jsSemaphore.h"
#include <vector>
#include "log4cplus/logger.h"
using namespace log4cplus;
#include "Core/Util/Unicode.h"
#include "Core/Util/fast_mutex.h"

namespace KEP {

class ProgressHandler;
class InternetManagerThread;
class Dispatcher;
class IEvent;

struct HandlerRec {
	ProgressHandler* m_pHandler;
	DWORD m_dwStateMask;
	bool m_bDeleteOnCompletion;

	HandlerRec(ProgressHandler* hndlr, DWORD stateMask, bool bDeleteOnCompletion) {
		m_pHandler = hndlr;
		m_dwStateMask = stateMask;
		m_bDeleteOnCompletion = bDeleteOnCompletion;
	}
};

typedef std::vector<HandlerRec> HandlerVector;
typedef std::pair<std::string, std::string> ValuePair;
typedef std::vector<ValuePair> ValuesList;

enum TaskState {
	// Overall task state
	TS_START = 0x0001, // Start task
	TS_COMPLETE = 0x0004, // Task completed
	TS_FAILED = 0x0008, // Task failed
	TS_CANCELED = 0x0002, // Task canceled

	// Task pre-processing
	TS_PREP_START = 0x0010, // Start Preprocessing
	TS_PREP_PROGRESS = 0x0020, // Preprocessing in progress
	TS_PREP_COMPLETE = 0x0040, // Preprocessing completed
	TS_PREP_FAILED = 0x0080, // Preprocessing failed

	// Task processing
	TS_PROC_START = 0x0100, // Start processing task
	TS_PROC_PROGRESS = 0x0200, // Processing in progress
	TS_PROC_COMPLETE = 0x0400, // Processing completed
	TS_PROC_FAILED = 0x0800, // Processing failed

	// Task post-processing
	TS_PSTP_START = 0x1000, // Start post-processing task
	TS_PSTP_PROGRESS = 0x2000, // Postprocessing in progress
	TS_PSTP_COMPLETE = 0x4000, // Postprocessing completed
	TS_PSTP_FAILED = 0x8000, // Postprocessing failed

	// Major events (everything except progress events)
	TS_TASK_MAJOR_EVENTS = TS_START | TS_COMPLETE | TS_FAILED | TS_CANCELED,
	TS_PREP_MAJOR_EVENTS = TS_PREP_START | TS_PREP_COMPLETE | TS_PREP_FAILED,
	TS_PROC_MAJOR_EVENTS = TS_PROC_START | TS_PROC_COMPLETE | TS_PROC_FAILED,
	TS_PSTP_MAJOR_EVENTS = TS_PSTP_START | TS_PSTP_COMPLETE | TS_PSTP_FAILED,

	TS_ALL_MAJOR_EVENTS = TS_TASK_MAJOR_EVENTS | TS_PREP_MAJOR_EVENTS | TS_PROC_MAJOR_EVENTS | TS_PSTP_MAJOR_EVENTS,

	// Progress events
	TS_ALL_PROGRESSES = TS_PREP_PROGRESS | TS_PROC_PROGRESS | TS_PSTP_PROGRESS,

	// Mask for listener of all events
	TS_ALL_EVENTS = TS_ALL_MAJOR_EVENTS | TS_ALL_PROGRESSES,
};

class TaskInfo {
public:
	TaskInfo(const std::string& taskType,
		LONG taskId,
		const char* path,
		const char* url,
		DWORD bytes = 0,
		Dispatcher* disp = NULL);

	virtual ~TaskInfo();

	void AddValue(const std::string& key, const std::string& value);
	void ClearValues();

	void AddEvent(IEvent* e, DWORD stateMask = TS_ALL_EVENTS);
	void AddHandler(ProgressHandler* h, DWORD stateMask = TS_ALL_EVENTS, bool bDeleteOnCompletion = true);

	std::string m_taskType;
	LONG m_taskId;

	int m_returnCode;
	std::string m_url;
	ValuesList m_valuesToPost;

	std::vector<std::string> m_files;
	DWORD m_totalBytes; // number of bytes to be processed in this task, 0 means to be determined

	// Notifications
	HandlerVector m_handlers; // notify by callback handlers

	Dispatcher* m_pDispatcher; // for event dispatching, if NULL no event can be dispatched.
};

enum {
	TSK_PRIO_HIGH = 300,
	TSK_PRIO_MEDIUM = 200,
	TSK_PRIO_LOW = 100
};

typedef int TaskPriority;
typedef std::pair<TaskInfo*, TaskPriority> TaskInfoPair;
typedef std::vector<TaskInfoPair> TaskInfoQueue;

// Higher level abstraction for Upload/Download manager
class InternetManager {
public:
	InternetManager(const char* name, Dispatcher& disp, int threadCount, jsThdPriority thdPrio);
	virtual ~InternetManager() {
		CancelAllTasks();
		TerminateThreads();
	}

	virtual void StartThreads();
	virtual void TerminateThreads();

	virtual void CancelAllTasks();
	virtual void AdjustPriority(const std::string& url, int prio);
	virtual bool QueueTask(TaskInfo* info, TaskPriority prio);

protected:
	virtual bool isAlreadyQueued(const char* url, TaskPriority prio, IEvent* e = NULL);
	virtual InternetManagerThread* createWorkerThread() = 0;
	virtual LONG getNextTaskId();

protected:
	std::string m_name;
	Logger m_logger;
	Dispatcher& m_dispatcher;
	const char* m_gameDir;
	int m_threadCount;
	jsThdPriority m_threadPriority;
	fast_recursive_mutex m_sync;
	jsSemaphore m_sema;

	TaskInfoQueue m_queue;

	std::vector<InternetManagerThread*> m_thds;
	bool m_started;

	LONG m_lastTaskId;
};

class InternetManagerThread : public jsThread {
public:
	InternetManagerThread(
		InternetManager& parent, Dispatcher& dispatcher, TaskInfoQueue& queue, fast_recursive_mutex& sync,
		const char* name, jsThdPriority prio = PR_NORMAL, jsThreadType ttype = TT_NORMAL) :
			jsThread(name, prio, ttype),
			m_logger(Logger::getInstance(Utf8ToUtf16(name))), m_cancelCurrent(false), m_currentDL(NULL), m_parent(parent), m_dispatcher(dispatcher), m_taskQueue(queue), m_taskQueueSync(sync), m_taskType(name) {
	}

	virtual ~InternetManagerThread() {}

	ULONG _doWork();

	void CancelCurrent() {
		m_cancelCurrent = true;
	}

	TaskInfo* CurrentDL() {
		return m_currentDL;
	}

protected:
	virtual ULONG GetBufferSize() = 0;
	virtual ULONG GetProgressInterval() {
		return 1000; // default interval
	}
	virtual void UpdateProgress(TaskInfo* info, TaskState state, float percent = 0.0f, DWORD bytesProcessed = 0, DWORD bytesTotal = 0);

	// Pre-streaming analysis: queue locked before this function
	virtual bool PreProcess(TaskInfo* info) = 0;

	// Actual streaming process: queue unlocked before entering
	virtual bool ProcessTask(TaskInfo* info) = 0;

	// Post-streaming analysis: queue locked before this function
	virtual void PostProcess(TaskInfo* info) = 0;

	// Post-streaming analysis: queue locked before this function
	virtual void PostProcessWhenFailed(TaskInfo* info) = 0;

protected:
	bool m_cancelCurrent;

	InternetManager& m_parent;
	TaskInfoQueue& m_taskQueue;
	fast_recursive_mutex& m_taskQueueSync;

	TaskInfo* m_currentDL;

	char* m_buffer;
	Dispatcher& m_dispatcher;

	Logger m_logger;
	std::string m_taskType;
};

class ProgressHandler {
public:
	ProgressHandler(const std::string& taskType, LONG taskId) :
			m_taskType(taskType), m_taskId(taskId) {}
	virtual ~ProgressHandler() {}

	// overall state handler
	virtual void started() = 0;
	virtual void complete() = 0;
	virtual void canceled() = 0;
	virtual void failed() = 0;

	// pre-processing state handler (optional)
	virtual void prepStarted() {}
	virtual void prepInProgress(float /*percent*/, DWORD /*bytesProcessed*/, DWORD /*bytesTotal*/) {}
	virtual void prepCompleted() {}
	virtual void prepFailed() {}

	// processing state handler (optional)
	virtual void procStarted() {}
	virtual void procInProgress(float /*percent*/, DWORD /*bytesProcessed*/, DWORD /*bytesTotal*/) {}
	virtual void procCompleted() {}
	virtual void procFailed() {}

	// post-processing state handler (optional)
	virtual void pstpStarted() {}
	virtual void pstpInProgress(float /*percent*/, DWORD /*bytesProcessed*/, DWORD /*bytesTotal*/) {}
	virtual void pstpCompleted() {}
	virtual void pstpFailed() {}

protected:
	std::string m_taskType;
	LONG m_taskId;
};

} // namespace KEP
