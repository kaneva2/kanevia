///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "ResourceManager.h"

namespace KEP {

class AnimationProxy;
class CBoneAnimationObject;
class CSkeletonAnimation;
class CBoneList;
class CSkeletonAnimHeader;

class AnimationRM : public TypedResourceManager<AnimationProxy> {
public:
	AnimationRM(const std::string& id);

	~AnimationRM();

	// TODO: remove this
	static AnimationRM* Instance() {
		return static_cast<AnimationRM*>(TypedResourceManager<AnimationProxy>::Instance());
	}

	AnimationProxy* GetAnimationProxy(const GLID& glid, bool bAutoFetch = true, bool bConvertLegacy = false);

	virtual UINT GetVersion() const {
		return m_Version;
	}

	// Asset Converter Only!
	bool AddSkeletonAnimation(CSkeletonAnimation* pSA);

	/// A convenience function that allows me to initialize a header without
	/// using depricated CSkeletonAnimation objects in the interface.
	bool InitializeSkeletonAnimHeaderByIndex(CSkeletonAnimHeader* pSAH, UINT animIndex);
	bool InitializeSkeletonAnimHeaderByGlid(CSkeletonAnimHeader* pSAH, const GLID& animGlid);

	UINT GetSkeletonAnimationCount() const {
		return m_skeletonAnimations.size();
	}

	CSkeletonAnimation* GetSkeletonAnimationByIndex(UINT animIndex) const;
	CSkeletonAnimation* GetSkeletonAnimationByHash(UINT64 animHash) const;
	CSkeletonAnimation* GetSkeletonAnimationByGlid(const GLID& animGlid) const;

	void loadLooseFileInfo();

protected:
	// \brief Return a path pointing to where resource should be stored locally
	// \param gameFilesDir a path pointed to "GameFiles" folder of current game
	//@@Assumption: caller must pass in a path pointed to "GameFiles" folder of current game.
	virtual std::string GetResourceLocalStore(const std::string& gameFilesDir) override;

	//! \brief Compose a file name for serialization based on hash code provided.
	//! \param hash Hash code for the resource to be serialized.
	virtual std::string GetResourceFileName(UINT64 hash) override;

	virtual std::string GetResourceFilePath(UINT64 hash) override;

	virtual std::string GetResourceURL(UINT64 hash) override;

	virtual bool IsUGCItem(UINT64 hash) const override {
		return IS_UGC_GLID((GLID)hash);
	}

private:
	UINT64 IndexToHashCode(UINT index) const;

	// some of these need to turn into <vector> or <list>
	// Actually, no, because they will be removed when I can read
	// loose animations, anyway.
	std::vector<CSkeletonAnimation*> m_skeletonAnimations; /// global animation table
	std::vector<CSkeletonAnimation*> m_skeletonAnimations_del; /// bin for deleted, still running animations

	UINT m_Version; /// animation table version
	UINT m_CodeVersion; /// code version
	BOOL m_Loaded; /// global animation table has been loaded
};

} // namespace KEP
