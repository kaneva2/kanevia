///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <direct.h>

#include "SkeletalAnimationManager.h"
#include "CSkeletonAnimation.h"

#include "AnimationProxy.h"
#include "RuntimeSkeleton.h"
#include "SerializeHelper.h"
#include "CBoneAnimationClass.h"
#include "CBoneClass.h"
#include "Utils/ReUtils.h"
#include "Event/Base/IEvent.h"
#include "DownloadManager.h"
#include "IClientEngine.h"
#include "CGlobalInventoryClass.h"
#include "ResourceType.h"
#include "common\include\CompressHelper.h"
#include "LogHelper.h"
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/key_extractors.hpp>

static LogInstance("Instance");

namespace KEP {

TypedResourceManager<AnimationProxy>* TypedResourceManager<AnimationProxy>::ms_pInstance = nullptr;

AnimationRM::AnimationRM(const std::string& id) :
		TypedResourceManager<AnimationProxy>(id) {
	m_Version = 0;
	m_CodeVersion = 1;
	m_Loaded = FALSE;
}

AnimationRM::~AnimationRM() {
	for (const auto& pSA : m_skeletonAnimations)
		if (pSA)
			delete pSA;
	m_skeletonAnimations.clear();

	for (const auto& pSA : m_skeletonAnimations_del)
		if (pSA)
			delete pSA;
	m_skeletonAnimations_del.clear();
}

CSkeletonAnimation* AnimationRM::GetSkeletonAnimationByIndex(UINT animIndex) const {
	return (animIndex < m_skeletonAnimations.size()) ? m_skeletonAnimations[animIndex] : NULL;
}

CSkeletonAnimation* AnimationRM::GetSkeletonAnimationByHash(UINT64 animHash) const {
	for (const auto& pSA : m_skeletonAnimations) {
		if (pSA && (pSA->m_hashCode == animHash))
			return pSA;
	}
	return nullptr;
}

CSkeletonAnimation* AnimationRM::GetSkeletonAnimationByGlid(const GLID& animGlid) const {
	for (const auto& pSA : m_skeletonAnimations) {
		if (pSA && (pSA->m_animGlid == animGlid))
			return pSA;
	}
	return nullptr;
}

bool AnimationRM::InitializeSkeletonAnimHeaderByIndex(CSkeletonAnimHeader* pSAH, UINT animIndex) {
	auto pSA = GetSkeletonAnimationByIndex(animIndex);
	if (!pSA)
		return false;
	return InitializeSkeletonAnimHeaderByGlid(pSAH, pSA->m_animGlid);
}

bool AnimationRM::InitializeSkeletonAnimHeaderByGlid(CSkeletonAnimHeader* pSAH, const GLID& animGlid) {
	if (!pSAH)
		return false;

	pSAH->m_animType = eAnimType::None;
	pSAH->m_animVer = 0;
	pSAH->m_animLooping = 0;
	pSAH->m_animMinTimeMs = (TimeMs)0.0;
	pSAH->m_hashCode = 0;
	pSAH->m_name = "";
	pSAH->m_animGlid = GLID_INVALID;

	auto pSA = GetSkeletonAnimationByGlid(animGlid);
	if (!pSA)
		return false;

	pSAH->m_animType = pSA->m_animType;
	pSAH->m_animVer = pSA->m_animVer;
	pSAH->m_animLooping = pSA->m_animLooping;
	pSAH->m_animMinTimeMs = pSA->m_animMinTimeMs;
	pSAH->m_hashCode = pSA->m_hashCode;
	pSAH->m_animGlid = pSA->m_animGlid;
	pSAH->m_name = pSA->m_name;
	return true;
}

// \brief Return a path pointing to where resource should be stored locally
// \param gameFilesDir a path pointed to "GameFiles" folder of current game
//@@Assumption: caller must pass in a path pointed to "GameFiles" folder of current game.
std::string AnimationRM::GetResourceLocalStore(const std::string& gameFilesDir) {
	// Create Animation Folder (deep)
	std::string animDir = (gameFilesDir == "~") ? IResource::BaseAnimPath(eAnimPath::CharacterAnimations) : PathAdd(gameFilesDir, "CharacterAnimations");
	if (!FileHelper::CreateFolderDeep(animDir)) {
		LogError("CreateFolderDeep() FAILED - '" << animDir << "'");
	}
	return animDir;
}

//! \brief Compose a file name for serialization based on hash code provided.
//! \param hash Hash code for the resource to be serialized.
std::string AnimationRM::GetResourceFileName(UINT64 hash) {
	UINT64 glid = hash;
	CStringA fileName;
	if ((glid <= GLID_CORE_MAX) && IS_CORE_GLID((GLID)glid))
		fileName.Format("ac%09I64u.dat", glid); // special naming for core assets
	else
		fileName.Format("a%010I64u.dat", glid);
	return fileName.GetString();
}

std::string AnimationRM::GetResourceFilePath(UINT64 hash) {
	return PathAdd(IResource::BaseAnimPath(eAnimPath::CharacterAnimations), GetResourceFileName(hash));
}

std::string AnimationRM::GetResourceURL(UINT64 hash) {
	GLID glid = (GLID)hash;
	if (IsUGCItem(glid))
		return ""; // UGC Asset URL will be determined before downloading

	std::string url;
	StrBuild(url, GetPatchAssetLocation() << "GameFiles_CharacterAnimations/" << GetResourceFileName(hash));
	url = ::CompressTypeExtAdd(url);
	return url;
}

bool AnimationRM::AddSkeletonAnimation(CSkeletonAnimation* pSA) {
	if (!pSA || (pSA->m_numBones > 256))
		return false;

	// Assign GLID if newly imported
	if (!IS_VALID_GLID(pSA->m_animGlid)) {
		// Assign regular GLID
		pSA->m_animGlid = CGlobalInventoryObjList::GetInstance()->AllocateGLID();
	} else if (pSA->m_animGlid == GLID_LOCAL_BASE) {
		// Assign local GLID for UGC animations
		pSA->m_animGlid = AllocateLocalGLID();
	}
	m_skeletonAnimations.push_back(pSA);

	return true;
}

UINT64 AnimationRM::IndexToHashCode(UINT index) const {
	return m_skeletonAnimations[index]->m_hashCode;
}

AnimationProxy* AnimationRM::GetAnimationProxy(const GLID& glid, bool bAutoFetch, bool bConvertLegacy) {
	// Get Animation Proxy
	auto pAP = static_cast<AnimationProxy*>(FindResource(glid));
	if (pAP || !bAutoFetch)
		return pAP;

	// Add the resource to the ResourceManager's map of managed resources.
	// Download of the data starts to happen when GetData is called on
	// the resource.
	UINT64 resKey = (UINT64)glid;
	pAP = AddResource(resKey, IS_LOCAL_GLID(glid));
	if (bConvertLegacy) {
		auto pSA = GetSkeletonAnimationByGlid(glid);
		if (!pSA) {
			LogError("Converting legacy animation: NOT FOUND - " << glid);
		} else if (!pAP->LoadFromSkeletonAnimation(pSA)) {
			LogError("Converting legacy animation: FAILED - " << glid);
		} else {
			LogInfo("Converting legacy animation: SUCCEEDED - " << glid);
			pAP->SetState(IResource::LOADED_AND_CREATED);
		}
	}

	return pAP;
}

void AnimationRM::loadLooseFileInfo() {
	std::string filePath = PathAdd(IResource::BaseAnimPath(eAnimPath::CharacterAnimations), "textureinfo.dat");
	ResourceManager::loadLooseFileInfo(filePath);
}

} // namespace KEP
