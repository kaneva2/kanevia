///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#define D3D_OVERLOADS
#include "d3d9.h"
#include <direct.h>

#include "ReMeshCreate.h"
#include "Event/Base/IEvent.h"

#include "MeshProxy.h"
#include "DownloadManager.h"
#include "ReD3DX9StaticMesh.h"
#include "ReD3DX9SkinMesh.h"
#include "Utils/ReUtils.h"
#include "TextureObj.h"
#include "IClientEngine.h"
#include "CEXMeshClass.h"
#include "CDeformableMesh.h"
#include "CReferenceLibrary.h"
#include "CWldObject.h"
#include "CBoneClass.h"
#include "ReD3DX9.h"
#include "Utils/ReUtils.h"
#include "DynamicObj.h"
#include "CHierarchyMesh.h"
#include "ResourceType.h"
#include "CompressHelper.h"
#include "LogHelper.h"
#include "TextureProvider.h"
#include <algorithm>
#undef min
#undef max

namespace KEP {
	
static LogInstance("Instance");

TypedResourceManager<MeshProxy>* TypedResourceManager<MeshProxy>::ms_pInstance;
Vector3f MeshRM::ORIGIN(0.0f, 0.0f, 0.0f);

MeshRM::MeshRM(const std::string& id) :
		TypedResourceManager<MeshProxy>(id) {
	for (size_t i = 0; i < _countof(m_MeshPool); ++i)
		m_MeshPool[i] = NULL;

	for (size_t i = 0; i < _countof(m_MeshCache); ++i)
		m_MeshCache[i] = NULL;

	m_CurrentBoneList = NULL;
	m_CurrentLOD = 0xffffffff;
	m_LoadProgressively = FALSE;
}

BOOL MeshRM::Initialize(UINT maxMeshes) {
	std::lock_guard<fast_mutex> lock(m_mtxPool);
	m_maxMeshes = maxMeshes;

	for (size_t i = 0; i < _countof(m_MeshPool); ++i) {
		m_MeshPool[i] = new ReMeshPool(m_maxMeshes);
		if (!m_MeshPool[i])
			return FALSE;
	}

	for (size_t i = 0; i < _countof(m_MeshCache); ++i) {
		m_MeshCache[i] = new ReMeshCache(m_maxMeshes);
		if (!m_MeshCache[i])
			return FALSE;
	}
	return TRUE;
}

ReMesh* MeshRM::CreateReStaticMesh(const ReMeshData* pSrcData, bool strip) {
	if (!pSrcData)
		return NULL;

	ReD3DX9DeviceState* dev = NULL;

	auto pICE = IClientEngine::Instance();
	if (pICE)
		dev = pICE->GetReDeviceState();
	else
		dev = ReD3DX9DeviceState::Instance();

	ReMesh* mesh = new ReD3DX9StaticMesh(dev);

	ReMeshData* pData = 0;

	mesh->Access(&pData);

	if (pData) {
		pData->NumV = pSrcData->NumV;
		pData->NumI = pSrcData->NumI;
		pData->PrimType = pSrcData->PrimType;
		pData->NumUV = pSrcData->NumUV;

		if (!mesh->Allocate()) {
			delete mesh;
			return NULL;
		}

		if (pData->pP) {
			for (DWORD v = 0; v < pData->NumV; v++)
				pData->pP[v] = pSrcData->pP[v];
		}

		if (pData->pN) {
			for (DWORD n = 0; n < pData->NumV; n++)
				pData->pN[n] = pSrcData->pN[n];
		}

		if (pData->pUv) {
			for (DWORD t = 0; t < pData->NumUV * pData->NumV; t++)
				pData->pUv[t] = pSrcData->pUv[t];
		}

		if (pData->pI) {
			memcpy(pData->pI, pSrcData->pI, sizeof(USHORT) * pData->NumI);
		}

		mesh->Build(strip);
		mesh->CreateShader(NULL);
	} else {
		// ReMesh::Access() failed ...
		delete mesh;
		return NULL;
	}

	return mesh;
}

ReMesh* MeshRM::CreateReStaticMesh(CEXMeshObj* meshObj, const Vector3f& pivot /*= ORIGIN*/, const char* subDir /*= NULL*/) {
	// get data for new Remesh
	ReD3DX9DeviceState* dev = NULL;
	auto pICE = IClientEngine::Instance();
	if (pICE)
		dev = pICE->GetReDeviceState(); // Try client version if exists
	else
		dev = ReD3DX9DeviceState::Instance(); // Otherwise use singleton -- this is to allow use of ReD3DX9DeviceState without a client engine

	auto HWCaps = dev->GetRenderState()->GetHardwareCaps();
	ReMesh* newMesh = new ReD3DX9StaticMesh(dev);
	ReMeshData* pData = NULL;
	// get the mesh array data
	newMesh->Access(&pData);
	// fill in mesh array data
	pData->NumV = meshObj->m_abVertexArray.m_vertexCount;
	pData->NumI = meshObj->m_abVertexArray.m_indecieCount;
	pData->PrimType = meshObj->m_abVertexArray.m_triStripped ? ReMeshPrimType::TRISTRIP : ReMeshPrimType::TRILIST;

	// create the ReMaterial
	CreateReMaterial(meshObj->m_materialObject, subDir ? subDir : "");

	// can't use vertex shaders....use fixed function pipeline
	if (HWCaps < ReD3DX9MINVERTEXSHADERSUPPORT) {
		if (meshObj->m_abVertexArray.m_vertexArray0) {
			pData->NumUV = 1;

		} else if (meshObj->m_abVertexArray.m_vertexArray1) {
			pData->NumUV = 1;
		} else if (meshObj->m_abVertexArray.m_vertexArray2) {
			pData->NumUV = 2;
		} else if (meshObj->m_abVertexArray.m_vertexArray3) {
			pData->NumUV = 3;
		} else {
			pData->NumUV = 4;
		}
	} else {
		if (meshObj->m_abVertexArray.m_vertexArray0) {
			pData->NumUV = 1;
		} else if (meshObj->m_abVertexArray.m_vertexArray1) {
			pData->NumUV = 1;
		} else if (meshObj->m_abVertexArray.m_vertexArray2) {
			pData->NumUV = 2;
		} else if (meshObj->m_abVertexArray.m_vertexArray3) {
			pData->NumUV = 3;
		} else {
			pData->NumUV = 4;
		}
	}

	// allocate the internal data arrays
	if (!newMesh->Allocate()) {
		delete newMesh;
		return NULL;
	}

	// fill in the index array
	USHORT* indexes = meshObj->m_abVertexArray.m_ushort_indexArray;
	for (DWORD i = 0; i < pData->NumI; i++) {
		pData->pI[i] = indexes[i];
	}

	// Use pivot point -- As of 03/05/09 pivot point is the center point of the BASE mesh for world
	// objects and origin for other objects. Caller should ensure they pass in correct pivot points.
	//
	// We might introduce independent pivot points (and independent matrices) in the future. At the
	// mean time, a shared pivot point is used among different LODs to make sure they line-up correctly.

	// fill in the vertex data
	if (meshObj->m_abVertexArray.m_vertexArray0) {
		ABVERTEX0L* abvtx = meshObj->m_abVertexArray.m_vertexArray0;
		for (DWORD i = 0; i < pData->NumV; i++) {
			// subtract the world space bound box center
			pData->pP[i].x = abvtx[i].x - pivot.x;
			pData->pP[i].y = abvtx[i].y - pivot.y;
			pData->pP[i].z = abvtx[i].z - pivot.z;
			pData->pN[i].x = abvtx[i].nx;
			pData->pN[i].y = abvtx[i].ny;
			pData->pN[i].z = abvtx[i].nz;
			pData->pUv[i].x = 0.f;
			pData->pUv[i].y = 0.f;
		}
	} else if (meshObj->m_abVertexArray.m_vertexArray1) {
		ABVERTEX1L* abvtx = meshObj->m_abVertexArray.m_vertexArray1;
		for (DWORD i = 0; i < pData->NumV; i++) {
			pData->pP[i].x = abvtx[i].x - pivot.x;
			pData->pP[i].y = abvtx[i].y - pivot.y;
			pData->pP[i].z = abvtx[i].z - pivot.z;
			pData->pN[i].x = abvtx[i].nx;
			pData->pN[i].y = abvtx[i].ny;
			pData->pN[i].z = abvtx[i].nz;
			pData->pUv[i].x = abvtx[i].tx0.tu;
			pData->pUv[i].y = abvtx[i].tx0.tv;
		}
	} else if (meshObj->m_abVertexArray.m_vertexArray2) {
		ABVERTEX2L* abvtx = meshObj->m_abVertexArray.m_vertexArray2;
		for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 2) {
			pData->pP[i].x = abvtx[i].x - pivot.x;
			pData->pP[i].y = abvtx[i].y - pivot.y;
			pData->pP[i].z = abvtx[i].z - pivot.z;
			pData->pN[i].x = abvtx[i].nx;
			pData->pN[i].y = abvtx[i].ny;
			pData->pN[i].z = abvtx[i].nz;
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			pData->pUv[j + 1].x = abvtx[i].tx1.tu;
			pData->pUv[j + 1].y = abvtx[i].tx1.tv;
		}
	} else if (meshObj->m_abVertexArray.m_vertexArray3) {
		ABVERTEX3L* abvtx = meshObj->m_abVertexArray.m_vertexArray3;
		for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 3) {
			pData->pP[i].x = abvtx[i].x - pivot.x;
			pData->pP[i].y = abvtx[i].y - pivot.y;
			pData->pP[i].z = abvtx[i].z - pivot.z;
			pData->pN[i].x = abvtx[i].nx;
			pData->pN[i].y = abvtx[i].ny;
			pData->pN[i].z = abvtx[i].nz;
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			pData->pUv[j + 1].x = abvtx[i].tx1.tu;
			pData->pUv[j + 1].y = abvtx[i].tx1.tv;
			pData->pUv[j + 2].x = abvtx[i].tx2.tu;
			pData->pUv[j + 2].y = abvtx[i].tx2.tv;
		}
	} else if (meshObj->m_abVertexArray.m_vertexArray4) {
		ABVERTEX4L* abvtx = meshObj->m_abVertexArray.m_vertexArray4;
		for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 4) {
			pData->pP[i].x = abvtx[i].x - pivot.x;
			pData->pP[i].y = abvtx[i].y - pivot.y;
			pData->pP[i].z = abvtx[i].z - pivot.z;
			pData->pN[i].x = abvtx[i].nx;
			pData->pN[i].y = abvtx[i].ny;
			pData->pN[i].z = abvtx[i].nz;
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			pData->pUv[j + 1].x = abvtx[i].tx1.tu;
			pData->pUv[j + 1].y = abvtx[i].tx1.tv;
			pData->pUv[j + 2].x = abvtx[i].tx2.tu;
			pData->pUv[j + 2].y = abvtx[i].tx2.tv;
			pData->pUv[j + 3].x = abvtx[i].tx3.tu;
			pData->pUv[j + 3].y = abvtx[i].tx3.tv;
		}
	} else if (meshObj->m_abVertexArray.m_vertexArray5) {
		ABVERTEX5L* abvtx = meshObj->m_abVertexArray.m_vertexArray5;
		for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 4) {
			pData->pP[i].x = abvtx[i].x - pivot.x;
			pData->pP[i].y = abvtx[i].y - pivot.y;
			pData->pP[i].z = abvtx[i].z - pivot.z;
			pData->pN[i].x = abvtx[i].nx;
			pData->pN[i].y = abvtx[i].ny;
			pData->pN[i].z = abvtx[i].nz;
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			pData->pUv[j + 1].x = abvtx[i].tx1.tu;
			pData->pUv[j + 1].y = abvtx[i].tx1.tv;
			pData->pUv[j + 2].x = abvtx[i].tx2.tu;
			pData->pUv[j + 2].y = abvtx[i].tx2.tv;
			pData->pUv[j + 3].x = abvtx[i].tx3.tu;
			pData->pUv[j + 3].y = abvtx[i].tx3.tv;
		}
	} else if (meshObj->m_abVertexArray.m_vertexArray6) {
		ABVERTEX6L* abvtx = meshObj->m_abVertexArray.m_vertexArray6;
		for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 4) {
			pData->pP[i].x = abvtx[i].x - pivot.x;
			pData->pP[i].y = abvtx[i].y - pivot.y;
			pData->pP[i].z = abvtx[i].z - pivot.z;
			pData->pN[i].x = abvtx[i].nx;
			pData->pN[i].y = abvtx[i].ny;
			pData->pN[i].z = abvtx[i].nz;
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			pData->pUv[j + 1].x = abvtx[i].tx1.tu;
			pData->pUv[j + 1].y = abvtx[i].tx1.tv;
			pData->pUv[j + 2].x = abvtx[i].tx2.tu;
			pData->pUv[j + 2].y = abvtx[i].tx2.tv;
			pData->pUv[j + 3].x = abvtx[i].tx3.tu;
			pData->pUv[j + 3].y = abvtx[i].tx3.tv;
		}
	} else if (meshObj->m_abVertexArray.m_vertexArray7) {
		ABVERTEX7L* abvtx = meshObj->m_abVertexArray.m_vertexArray7;
		for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 4) {
			pData->pP[i].x = abvtx[i].x - pivot.x;
			pData->pP[i].y = abvtx[i].y - pivot.y;
			pData->pP[i].z = abvtx[i].z - pivot.z;
			pData->pN[i].x = abvtx[i].nx;
			pData->pN[i].y = abvtx[i].ny;
			pData->pN[i].z = abvtx[i].nz;
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			pData->pUv[j + 1].x = abvtx[i].tx1.tu;
			pData->pUv[j + 1].y = abvtx[i].tx1.tv;
			pData->pUv[j + 2].x = abvtx[i].tx2.tu;
			pData->pUv[j + 2].y = abvtx[i].tx2.tv;
			pData->pUv[j + 3].x = abvtx[i].tx3.tu;
			pData->pUv[j + 3].y = abvtx[i].tx3.tv;
		}
	} else if (meshObj->m_abVertexArray.m_vertexArray8) {
		ABVERTEX8L* abvtx = meshObj->m_abVertexArray.m_vertexArray8;
		for (DWORD i = 0, j = 0; i < pData->NumV; i++, j += 4) {
			pData->pP[i].x = abvtx[i].x - pivot.x;
			pData->pP[i].y = abvtx[i].y - pivot.y;
			pData->pP[i].z = abvtx[i].z - pivot.z;
			pData->pN[i].x = abvtx[i].nx;
			pData->pN[i].y = abvtx[i].ny;
			pData->pN[i].z = abvtx[i].nz;
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			pData->pUv[j + 1].x = abvtx[i].tx1.tu;
			pData->pUv[j + 1].y = abvtx[i].tx1.tv;
			pData->pUv[j + 2].x = abvtx[i].tx2.tu;
			pData->pUv[j + 2].y = abvtx[i].tx2.tv;
			pData->pUv[j + 3].x = abvtx[i].tx3.tu;
			pData->pUv[j + 3].y = abvtx[i].tx3.tv;
		}
	}

	// build the internal data
	newMesh->Build();

	// Call CreateShader to Obtain m_VertexType according to Charles.
	// Otherwise newly imported mesh won't render until zone reloaded. [Yanfeng 11/2008]
	newMesh->CreateShader(NULL);
	return newMesh;
}

ReMesh* MeshRM::CreateReSkinMeshPersistent(CDeformableVisObj* visObj, bool bRecalcVertexFromOffsetVectors, RMP_ID rmpId) {
	ReD3DX9DeviceState* dev = NULL;
	auto pICE = IClientEngine::Instance();
	if (pICE)
		dev = pICE->GetReDeviceState(); // Try client version if exists
	else
		dev = ReD3DX9DeviceState::Instance(); // Otherwise use singleton -- this is to allow use of ReD3DX9DeviceState without a client engine

	const CBoneList* bonesList = GetCurrentBoneList();
	const Matrix44fA16* boneMats = bonesList->getBoneMatrices();
	DWORD numB = bonesList->GetCount();
	DWORD numV = visObj->m_mesh->m_abVertexArray.m_vertexCount;
	DWORD numI = visObj->m_mesh->m_abVertexArray.m_indecieCount;
	ReMeshPrimType primType = visObj->m_mesh->m_abVertexArray.m_triStripped ? ReMeshPrimType::TRISTRIP : ReMeshPrimType::TRILIST;
	/*DWORD				HWCaps		=*/dev->GetRenderState()->GetHardwareCaps();
	DWORD lod = GetCurrentLOD();

	// return if "Optimize()" failed
	if (visObj->m_mesh->m_abVertexArray.m_vertexCount != (UINT)visObj->GetAssignmentCount()) {
		return NULL;
	}

	// the new mesh
	ReSkinMesh* newMesh = NULL;

	newMesh = new ReD3DX9SkinMesh(dev);

	// access mesh data
	ReMeshData* pData = NULL;
	ReSkinMeshData* pSkinData = NULL;
	newMesh->Access(&pData, &pSkinData);

	// get number of uv sets
	if (visObj->m_mesh->m_abVertexArray.m_vertexArray0) {
		pData->NumUV = 1;
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray1) {
		pData->NumUV = 1;
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray2) {
		// extra uv sets are not valid by default
		pData->NumUV = 1;
		// check that the multiple uv's are actually there
		ABVERTEX2L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray2;
		for (DWORD i = 0; i < numV; i++) {
			// non-zero uv found
			if (abvtx[i].tx1.tu != 0.f || abvtx[i].tx1.tv != 0.f) {
				// extra uv sets are valid
				pData->NumUV = 2;
				break;
			}
		}
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray3) {
		// extra uv sets are not valid by default
		pData->NumUV = 1;
		// check that the multiple uv's are actually there
		ABVERTEX3L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray3;
		for (DWORD i = 0; i < numV; i++) {
			// non-zero uv found
			if (abvtx[i].tx1.tu != 0.f || abvtx[i].tx1.tv != 0.f ||
				abvtx[i].tx2.tu != 0.f || abvtx[i].tx2.tv != 0.f) {
				// extra uv sets are valid
				pData->NumUV = 3;
				break;
			}
		}
	} else {
		// extra uv sets are not valid by default
		pData->NumUV = 1;
		// check that the multiple uv's are actually there
		ABVERTEX4L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray4;
		for (DWORD i = 0; i < numV; i++) {
			// non-zero uv found
			if (abvtx[i].tx1.tu != 0.f || abvtx[i].tx1.tv != 0.f ||
				abvtx[i].tx2.tu != 0.f || abvtx[i].tx2.tv != 0.f ||
				abvtx[i].tx3.tu != 0.f || abvtx[i].tx3.tv != 0.f) {
				// extra uv sets are valid
				pData->NumUV = 4;
				break;
			}
		}
	}

	// HARDCODE the wpv of the lod's  NOTE: there should be more control over this!!!!!
	switch (lod) {
		case 0:
			pSkinData->Wpv = 4;
			break;

		case 1:
			pSkinData->Wpv = 3;
			break;

		case 2:
			pSkinData->Wpv = 2;
			break;

		default:
			pSkinData->Wpv = 1;
			break;
	}

	// initialize counts
	pData->NumV = numV;
	pData->NumI = numI;
	pData->PrimType = primType;
	pSkinData->NumB = numB;

	// allocate the mesh data arrays
	if (!newMesh->Allocate()) {
		delete newMesh;
		return NULL;
	}

	// fill in the triangle index array
	USHORT* indexes = visObj->m_mesh->m_abVertexArray.m_ushort_indexArray;
	for (DWORD i = 0; i < numI; i++) {
		pData->pI[i] = indexes[i];
	}

	// get skin-space images of all vertexes...
	for (DWORD i = 0; i < numV; i++) {
		// initialize the vertex data
		pData->pP[i].x =
			pData->pP[i].y =
				pData->pP[i].z =
					pData->pN[i].x =
						pData->pN[i].y =
							pData->pN[i].z = 0.f;

		// get double-precison weights
		DWORD nonZeroweights = 0;
		DOUBLE weightSum = 0.;
		DWORD boneSpacenum[64];
		DWORD validBoneindex[64];
		DOUBLE nonZeroweight[64];

		// get double-precision weights for max precision
		for (DWORD j = 0; j < visObj->m_blendedVertexAssignments[i].vertexBoneWeights.size(); j++) {
			const VertexBoneWeight& boneWeight = visObj->m_blendedVertexAssignments[i].vertexBoneWeights[j];
			if (boneWeight.weight > 0.f) {
				if (nonZeroweights < 64) {
					nonZeroweight[nonZeroweights] = (double)boneWeight.weight;
					validBoneindex[nonZeroweights] = boneWeight.boneIndex;
					boneSpacenum[nonZeroweights++] = j;
					weightSum += (double)boneWeight.weight;
				}
			}
		}

		if (weightSum == 0.) {
			continue;
		}

		if (bRecalcVertexFromOffsetVectors) {
			// Old impl recalculates skin-space vertex positions by using average value from bone-space
			// vertex positions. It introduces additional floating point errors and it's unnecessary at
			// least for Collada import. I temporarily Keep this part for backward compatibility with KDM/KAO
			// files from max plugin, which can be removed if I can verify that the alternative approach
			// (see 'else' section) works for them as well. -- YC Jan 2010

			// get weight renormalization factor
			DOUBLE weightFactor = 1. / weightSum;

			// double-precision accumulation results
			DOUBLE vresult_x, vresult_y, vresult_z, nresult_x, nresult_y, nresult_z;
			// initialize accumulation
			vresult_x = vresult_y = vresult_z = nresult_x = nresult_y = nresult_z = 0.;
			// get the skin-space image of vertex by accumulating bone-space images

			for (DWORD j = 0; j < nonZeroweights; j++) {
				// renormalize the weights to sum to 1 for max precision
				DOUBLE weight = nonZeroweight[j] * weightFactor;
				// get the valid bone index matrix
				const Matrix44f* boneMatrix = &boneMats[validBoneindex[j]];
				// get the bone's object-space image of this vertex and normal
				const Vector3f& pos3 = visObj->m_blendedVertexAssignments[i].vertexBoneWeights[boneSpacenum[j]].positionInBoneSpace;
				const Vector3f& nml3 = visObj->m_blendedVertexAssignments[i].vertexBoneWeights[boneSpacenum[j]].normalInBoneSpace;
				D3DXVECTOR4 vtx(pos3.x, pos3.y, pos3.z, 1.f);
				D3DXVECTOR4 nrm(nml3.x, nml3.y, nml3.z, 0.f);

				D3DXVec4Normalize((D3DXVECTOR4*)&nrm, (D3DXVECTOR4*)&nrm);

				// transform from bone-space to skin-space
				vresult_x += (((*boneMatrix)[0][0] * (DOUBLE)vtx.x) +
								 ((*boneMatrix)[1][0] * (DOUBLE)vtx.y) +
								 ((*boneMatrix)[2][0] * (DOUBLE)vtx.z) +
								 ((*boneMatrix)[3][0])) *
							 weight;

				vresult_y += (((*boneMatrix)[0][1] * (DOUBLE)vtx.x) +
								 ((*boneMatrix)[1][1] * (DOUBLE)vtx.y) +
								 ((*boneMatrix)[2][1] * (DOUBLE)vtx.z) +
								 ((*boneMatrix)[3][1])) *
							 weight;

				vresult_z += (((*boneMatrix)[0][2] * (DOUBLE)vtx.x) +
								 ((*boneMatrix)[1][2] * (DOUBLE)vtx.y) +
								 ((*boneMatrix)[2][2] * (DOUBLE)vtx.z) +
								 ((*boneMatrix)[3][2])) *
							 weight;

				nresult_x += (((*boneMatrix)[0][0] * (DOUBLE)nrm.x) +
								 ((*boneMatrix)[1][0] * (DOUBLE)nrm.y) +
								 ((*boneMatrix)[2][0] * (DOUBLE)nrm.z)) *
							 weight;

				nresult_y += (((*boneMatrix)[0][1] * (DOUBLE)nrm.x) +
								 ((*boneMatrix)[1][1] * (DOUBLE)nrm.y) +
								 ((*boneMatrix)[2][1] * (DOUBLE)nrm.z)) *
							 weight;

				nresult_z += (((*boneMatrix)[0][2] * (DOUBLE)nrm.x) +
								 ((*boneMatrix)[1][2] * (DOUBLE)nrm.y) +
								 ((*boneMatrix)[2][2] * (DOUBLE)nrm.z)) *
							 weight;
			}

			// store the skin-space image of vertex
			pData->pP[i].x = (FLOAT)vresult_x;
			pData->pP[i].y = (FLOAT)vresult_y;
			pData->pP[i].z = (FLOAT)vresult_z;
			// store the skin-space image of normal
			pData->pN[i].x = (FLOAT)nresult_x;
			pData->pN[i].y = (FLOAT)nresult_y;
			pData->pN[i].z = (FLOAT)nresult_z;
		} else {
			// Currently this part is only used for COLLADA import. Might also work with KDM/KAO.
			// See comments above. - YC Jan 2010.

			// Use existing mesh data directly
			ABVERTEX vtx = visObj->m_mesh->m_abVertexArray.GetVertex(i);

			// store the skin-space image of vertex
			pData->pP[i].x = (FLOAT)vtx.x;
			pData->pP[i].y = (FLOAT)vtx.y;
			pData->pP[i].z = (FLOAT)vtx.z;
			// store the skin-space image of normal
			pData->pN[i].x = (FLOAT)vtx.nx;
			pData->pN[i].y = (FLOAT)vtx.ny;
			pData->pN[i].z = (FLOAT)vtx.nz;
		}

		pData->pN[i].Normalize();
	}

	// buffer the texture coordinates
	if (visObj->m_mesh->m_abVertexArray.m_vertexArray0) {
		//ABVERTEX0L*	abvtx	=	visObj->m_mesh->m_abVertexArray.m_vertexArray0;
		for (DWORD i = 0; i < numV; i++) {
			pData->pUv[i].x = 0.f;
			pData->pUv[i].y = 0.f;
		}
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray1) {
		ABVERTEX1L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray1;
		for (DWORD i = 0; i < numV; i++) {
			pData->pUv[i].x = abvtx[i].tx0.tu;
			pData->pUv[i].y = abvtx[i].tx0.tv;
		}
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray2) {
		ABVERTEX2L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray2;
		for (DWORD i = 0, j = 0; i < numV; i++, j += 1) {
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			if (pData->NumUV == 2) {
				pData->pUv[j + 1].x = abvtx[i].tx1.tu;
				pData->pUv[j + 1].y = abvtx[i].tx1.tv;
				j += 1;
			}
		}
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray3) {
		ABVERTEX3L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray3;
		for (DWORD i = 0, j = 0; i < numV; i++, j += 1) {
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			if (pData->NumUV == 3) {
				pData->pUv[j + 1].x = abvtx[i].tx1.tu;
				pData->pUv[j + 1].y = abvtx[i].tx1.tv;
				pData->pUv[j + 2].x = abvtx[i].tx2.tu;
				pData->pUv[j + 2].y = abvtx[i].tx2.tv;
				j += 2;
			}
		}
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray4) {
		ABVERTEX4L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray4;
		for (DWORD i = 0, j = 0; i < numV; i++, j += 1) {
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			if (pData->NumUV == 4) {
				pData->pUv[j + 1].x = abvtx[i].tx1.tu;
				pData->pUv[j + 1].y = abvtx[i].tx1.tv;
				pData->pUv[j + 2].x = abvtx[i].tx2.tu;
				pData->pUv[j + 2].y = abvtx[i].tx2.tv;
				pData->pUv[j + 3].x = abvtx[i].tx3.tu;
				pData->pUv[j + 3].y = abvtx[i].tx3.tv;
				j += 3;
			}
		}
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray5) {
		ABVERTEX5L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray5;
		for (DWORD i = 0, j = 0; i < numV; i++, j += 1) {
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			if (pData->NumUV == 4) {
				pData->pUv[j + 1].x = abvtx[i].tx1.tu;
				pData->pUv[j + 1].y = abvtx[i].tx1.tv;
				pData->pUv[j + 2].x = abvtx[i].tx2.tu;
				pData->pUv[j + 2].y = abvtx[i].tx2.tv;
				pData->pUv[j + 3].x = abvtx[i].tx3.tu;
				pData->pUv[j + 3].y = abvtx[i].tx3.tv;
				j += 3;
			}
		}
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray6) {
		ABVERTEX6L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray6;
		for (DWORD i = 0, j = 0; i < numV; i++, j += 1) {
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			if (pData->NumUV == 4) {
				pData->pUv[j + 1].x = abvtx[i].tx1.tu;
				pData->pUv[j + 1].y = abvtx[i].tx1.tv;
				pData->pUv[j + 2].x = abvtx[i].tx2.tu;
				pData->pUv[j + 2].y = abvtx[i].tx2.tv;
				pData->pUv[j + 3].x = abvtx[i].tx3.tu;
				pData->pUv[j + 3].y = abvtx[i].tx3.tv;
				j += 3;
			}
		}
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray7) {
		ABVERTEX7L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray7;
		for (DWORD i = 0, j = 0; i < numV; i++, j += 1) {
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			if (pData->NumUV == 4) {
				pData->pUv[j + 1].x = abvtx[i].tx1.tu;
				pData->pUv[j + 1].y = abvtx[i].tx1.tv;
				pData->pUv[j + 2].x = abvtx[i].tx2.tu;
				pData->pUv[j + 2].y = abvtx[i].tx2.tv;
				pData->pUv[j + 3].x = abvtx[i].tx3.tu;
				pData->pUv[j + 3].y = abvtx[i].tx3.tv;
				j += 3;
			}
		}
	} else if (visObj->m_mesh->m_abVertexArray.m_vertexArray8) {
		ABVERTEX8L* abvtx = visObj->m_mesh->m_abVertexArray.m_vertexArray8;
		for (DWORD i = 0, j = 0; i < numV; i++, j += 1) {
			pData->pUv[j].x = abvtx[i].tx0.tu;
			pData->pUv[j].y = abvtx[i].tx0.tv;
			if (pData->NumUV == 4) {
				pData->pUv[j + 1].x = abvtx[i].tx1.tu;
				pData->pUv[j + 1].y = abvtx[i].tx1.tv;
				pData->pUv[j + 2].x = abvtx[i].tx2.tu;
				pData->pUv[j + 2].y = abvtx[i].tx2.tv;
				pData->pUv[j + 3].x = abvtx[i].tx3.tu;
				pData->pUv[j + 3].y = abvtx[i].tx3.tv;
				j += 3;
			}
		}
	}

	newMesh->BuildBoneWeights(visObj->m_blendedVertexAssignments);

	// build any internal data required
	newMesh->Build();

	// set the CEXMeshFlag indicating its vertex array is replaced by ReMesh
	visObj->m_mesh->m_VertexArrayValid = FALSE;

	// check for dups
	ReMesh* mesh = AddMesh((ReMesh*)newMesh, rmpId);

	// no dup found
	if (mesh == NULL) {
		return newMesh;
	}
	// dup found....use it and delete the new mesh
	else {
		delete newMesh;
		return mesh;
	}
}

void MeshRM::CreateReMaterials(CDeformableMesh* dMesh, const char* subDir) {
	// get the global D3D9 resource manager
	ReD3DX9DeviceState* dev = NULL;
	auto pICE = IClientEngine::Instance();
	if (pICE)
		dev = pICE->GetReDeviceState(); // Try client version if exists
	else
		dev = ReD3DX9DeviceState::Instance(); // Otherwise use singleton -- this is to allow use of ReD3DX9DeviceState without a client engine

	if (dMesh->m_material) {
		CreateReMaterial(dMesh->m_material, subDir ? subDir : "");
	}

	if (dMesh->m_supportedMaterials) {
		for (POSITION pos = dMesh->m_supportedMaterials->GetHeadPosition(); pos != NULL;) {
			CMaterialObject* mat = (CMaterialObject*)dMesh->m_supportedMaterials->GetNext(pos);
			CreateReMaterial(mat, subDir ? subDir : "");
		}
	}
}

void MeshRM::RenderMeshCache(RMC_ID rmcId, bool overrideLighting) {
	ReMESHCACHESORTKEY key;
	if (rmcId == RMC_ID_2)
		key = ReMESHCACHESORTCAMERAZ;
	else if (rmcId == RMC_ID_7)
		key = ReMESHCACHESORTORDER;
	else
		key = NA_ReMESHCACHESORTKEY;

	// DRF - Crash Fix
	std::lock_guard<fast_mutex> lock(ReMesh::m_mutex);

	size_t i = (size_t)rmcId;
	if (m_MeshCache[i])
		m_MeshCache[i]->Render(m_pDev, key, !overrideLighting);
}

ABBOX MeshRM::ComputeBoundingBoxWithMatrix(ReMesh* mesh, const Matrix44f* pMatrix) {
	ABBOX box;

	// get the platform-independent ReMesh data
	ReMeshData* pData = nullptr;
	if (mesh != nullptr)
		mesh->Access(&pData);

	if (pData == NULL || pData->NumV == 0) {
		// No points.
		if (pMatrix != nullptr) {
			box.minX = box.maxX = (*pMatrix)(3, 0);
			box.minY = box.maxY = (*pMatrix)(3, 1);
			box.minZ = box.maxZ = (*pMatrix)(3, 2);
		} else {
			box.minX = box.maxX = box.minY = box.maxY = box.minZ = box.maxZ = 0; // Default value
		}
		// prevent degenerates
		if (box.minX == box.maxX)
			box.maxX = box.maxX + .01f;
		if (box.minY == box.maxY)
			box.maxY = box.maxY + .01f;
		if (box.minZ == box.maxZ)
			box.maxZ = box.maxZ + .01f;
	} else {
		// compute bounding box
		const Vector3f* pVertexPoints = pData->pP;

		//#define NO_SSE
#if defined(NO_SSE)
		Vector3f vMin(FLT_MAX, FLT_MAX, FLT_MAX);
		Vector3f vMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);
		if (pMatrix != nullptr) {
			for (DWORD i = 0; i < pData->NumV; i++) {
				Vector3f v = TransformPointToHomogeneousVector(*pMatrix, pVertexPoints[i]);
				for (int i = 0; i < 3; ++i) {
					vMin[i] = std::min<float>(vMin[i], v[i]);
					vMax[i] = std::max<float>(vMax[i], v[i]);
				}
			}
		} else {
			for (DWORD i = 0; i < pData->NumV; i++) {
				const Vector3f& v = pVertexPoints[i]; // Assuming identity if matrix is not provided
				for (int i = 0; i < 3; ++i) {
					vMin[i] = std::min<float>(vMin[i], v[i]);
					vMax[i] = std::max<float>(vMax[i], v[i]);
				}
			}
		}
		for (int i = 0; i < 3; ++i)
			vMax[i] = std::max<float>(vMax[i], vMin[i] + .01f);
		box.minX = vMin[0];
		box.minY = vMin[1];
		box.minZ = vMin[2];
		box.maxX = vMax[0];
		box.maxY = vMax[1];
		box.maxZ = vMax[2];
#else
		__m128 box_min = _mm_set_ps(FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX);
		__m128 box_max = _mm_set_ps(-FLT_MAX, -FLT_MAX, -FLT_MAX, -FLT_MAX);

		__declspec(align(16)) Vector4f v(0, 0, 0, 0);
		if (pMatrix != nullptr) {
			for (DWORD i = 0; i < pData->NumV; i++) {
				v = TransformPointToHomogeneousVector(*pMatrix, pVertexPoints[i]);
				__m128 v128 = _mm_loadu_ps(&v[0]);
				box_min = _mm_min_ps(box_min, v128);
				box_max = _mm_max_ps(box_max, v128);
			}
		} else {
			for (DWORD i = 0; i < pData->NumV; i++) {
				v.SubVector<3>() = pVertexPoints[i]; // Assuming identity if matrix is not provided
				__m128 v128 = _mm_loadu_ps(&v[0]);
				box_min = _mm_min_ps(box_min, v128);
				box_max = _mm_max_ps(box_max, v128);
			}
		}
		box_max = _mm_max_ps(box_max, _mm_add_ps(box_min, _mm_set1_ps(.01f)));

		float af[4];
		_mm_storeu_ps(af, box_min);
		box.minX = af[0];
		box.minY = af[1];
		box.minZ = af[2];
		_mm_storeu_ps(af, box_max);
		box.maxX = af[0];
		box.maxY = af[1];
		box.maxZ = af[2];
#endif
	}

	return box;
}

static size_t MaterialTextureSlots(CMaterialObject* matObj) {
	if (!matObj)
		return 0;
	switch (matObj->GetCustomTextureOption()) {
		case CMaterialObject::NO_CUSTOM_TEXTURE:
			return 0;
		case CMaterialObject::CUSTOM_FIRST_TEXTURE_SLOT:
			return 1;
		default:
			return NUM_CMATERIAL_TEXTURES;
	}
}

static bool MaterialUseAlpha(CMaterialObject* matObj) {
	return matObj && ((matObj->m_blendMode == CMaterialObject::BLEND_ALPHA) || (matObj->m_blendMode == CMaterialObject::BLEND_ALPHA_ALT));
}

// Must be consistent with ClientEngine::buildTexturePath. Temporary. To be removed. See CreateReMaterial.
static bool isCustomTexturePath(const std::string& path) {
	return STLHasExtension(path, "ct") || STLHasExtension(path, "fp") || STLHasExtension(path, "gt") ||
		   STLHasExtension(path, "ct_thumb") || STLHasExtension(path, "fp_thumb") || STLHasExtension(path, "gt_thumb");
}

void MeshRM::CreateReMaterial(CMaterialObject* matObj, const std::string& subDir, const TextureColorMap* pAltColorMap) {
	if (!matObj) {
		LogError("matObj is null");
		return;
	}

	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return;

	// Process custom textures if specified
	if (matObj->GetCustomTextureProvider()) {
		auto pTDB = pICE->GetTextureDataBase();
		if (!pTDB)
			return;

		bool useAlpha = MaterialUseAlpha(matObj);

		// Add All Material Textures To Texture Database
		size_t texSlots = MaterialTextureSlots(matObj);
		for (size_t texSlot = 0; texSlot < texSlots; ++texSlot) {
			// Skip Empty Texture Slots (nameHash=0)
			DWORD origNameHash = matObj->m_texNameHashOriginal[texSlot];
			if (origNameHash == 0 || !ValidTextureNameHash(origNameHash))
				continue;

			D3DCOLOR colorKey = matObj->m_texColorKey[texSlot];

			// Build Custom Texture Path ({subid} -> origNameHash)
			unsigned textureKey = origNameHash;
			std::string primaryUrl = matObj->GetCustomTextureProvider()->getUrl(textureKey, false);

			// customTexturePath is a URL.  Get the filename from it.
			size_t ofsSlashPrimary = primaryUrl.find_last_of("\\/");
			std::string primaryFileName = primaryUrl.substr(ofsSlashPrimary == std::string::npos ? 0 : ofsSlashPrimary + 1);

			// Add Full Resolution Texture To Texture Database
			DWORD primaryNameHash = INVALID_TEXTURE_NAMEHASH;
			pTDB->TextureAdd(primaryNameHash, primaryFileName, subDir, primaryUrl, colorKey, eTexturePath::CustomTexture);
			if (!ValidTextureNameHash(primaryNameHash)) {
				LogError("AddTextureToDatabase() FAILED - texName='" << primaryFileName << "' texUrl='" << primaryUrl << "'");
			} else {
				CTextureEX* pTex = pTDB->TextureGet(primaryNameHash);
				ASSERT(pTex != nullptr);

				if (pTex) {
					pTex->SetEncryptionHashOverride(origNameHash);

					if (pAltColorMap) {
						// Alternative color used to construct a single pixel texture before the full res texture is ready
						auto itrColor = pAltColorMap->find(origNameHash);
						if (itrColor != pAltColorMap->end()) {
							pTex->SetAltColor(itrColor->second);
						}
					}

					// Not yet chained and low res textures found
					std::string lowResUrl = matObj->GetCustomTextureProvider()->getUrl(textureKey, true);
					if (!useAlpha && !pTex->IsChainHead() && !lowResUrl.empty()) {
						// Build texture chain (mechanism for progressive loading of low->high resolution textures)
						pTex->InitChain();

						// Check if path template contains quality macro
						bool multiQuality = false;
						if (lowResUrl.find(TEXTURE_QUALITY_SUFFIX_MACRO) != std::string::npos) {
							multiQuality = true;
						}

						// Chain low-res texture by available qualities (low to high)
						for (const auto& pr : CTextureEX::GetEnabledQualityLevels()) {
							const auto& quality = pr.first;
							const auto& suffix = pr.second;

							if (quality == TEX_DDS) {
								// TEX_DDS is the head
								continue;
							}

							if (multiQuality) {
								STLStringSubstitute(lowResUrl, TEXTURE_QUALITY_SUFFIX_MACRO, suffix);
							}

							size_t ofsSlashLowRes = lowResUrl.find_last_of("\\/");
							std::string lowResFileName = lowResUrl.substr(ofsSlashLowRes == std::string::npos ? 0 : ofsSlashLowRes + 1);

							// Add Progressive Resolution Texture To Texture Database
							DWORD lowResNameHash = INVALID_TEXTURE_NAMEHASH;
							pTDB->TextureAdd(lowResNameHash, lowResFileName, subDir, lowResUrl, colorKey, eTexturePath::CustomTexture);
							if (!ValidTextureNameHash(lowResNameHash)) {
								LogError("AddTextureToDatabase(LOW_RES) FAILED - texName='" << lowResFileName << "' texUrl='" << lowResUrl << "'");
							} else {
								auto pLowResTex = pTDB->TextureGet(lowResNameHash);
								ASSERT(pLowResTex != nullptr);

								if (pLowResTex) {
									pLowResTex->SetTranscodeJPG(); // HARD CODED: JPG flag = true for low res textures
									pTex->ChainToLowerRes(pLowResTex, quality);
								}
							}

							if (!multiQuality) {
								break;
							}
						}
					} else if (useAlpha && !pTex->IsChainHead() && !lowResUrl.empty()) {
						// Check if we want to replace translucent DDS with JPG or 1-pixel texture
						if (CTextureEX::IsForcingOnePixel()) {
							// Replace if texture experiment calls for 1-pixel texture rendering for all UGC DOs
							pTex->InitChain(); // Init a head-only chain.
						}
					}
				}

				matObj->m_texNameHash[texSlot] = primaryNameHash;
				matObj->m_texFileName[texSlot] = primaryFileName;
			}
		}
	} else {
		// Kaneva items only - reset custom textures
		for (size_t i = 0; i < NUM_CMATERIAL_TEXTURES; i++) {
			if (!ValidTextureFileName(matObj->m_texFileName[i])) {
				// No texture in this entry
				continue;
			}

			// Sanity check: either
			//  1) m_texFileNameOriginal is never set (not from MaterialCompressor or CMaterialObject serialization), or
			//  2) m_texFileName is the same as m_texFileNameOriginal, or
			//  3) m_texFileName holds a custom texture
			assert(
				matObj->m_texFileNameOriginal[i].empty() ||
				matObj->m_texFileName[i] == matObj->m_texFileNameOriginal[i] ||
				ValidTextureFileName(matObj->m_texFileNameOriginal[i]) && isCustomTexturePath(matObj->m_texFileName[i]));

			// isCustomTexturePath() condition check is for bug prevention and should be removed along with assertion above once we verify all test cases
			if (ValidTextureFileName(matObj->m_texFileNameOriginal[i]) &&
				matObj->m_texFileNameOriginal[i] != matObj->m_texFileName[i] &&
				isCustomTexturePath(matObj->m_texFileName[i])) {
				// Restore texture file name
				matObj->m_texFileName[i] = matObj->m_texFileNameOriginal[i];
				// Recompute name hash
				matObj->m_texNameHash[i] = TextureDatabase::TextureNameHash(matObj->m_texFileName[i], matObj->m_texColorKey[i]);
				assert(matObj->m_texNameHash[i] == matObj->m_texNameHashOriginal[i]);
			}
		}
	}

	// get the global D3D9 resource manager
	ReD3DX9DeviceState* dev = NULL;
	if (pICE)
		dev = pICE->GetReDeviceState(); // Try client version if exists
	else
		dev = ReD3DX9DeviceState::Instance(); // Otherwise use singleton -- this is to allow use of ReD3DX9DeviceState without a client engine

	// allocate the GPU upload bones if HW shader present
	dev->GetRenderState()->GetHardwareCaps();

	// get the ReRenderState data of this material
	ReRenderStateData data;
	matObj->GetReRenderState(&data, subDir);

	// Material Has Moving Texture ?
	if (matObj->m_enableTextureMovement) {
		D3DXVECTOR2 uvIncr[2];

		// Set Moving Textures Velocity Vector
		uvIncr[0].x = -matObj->m_textureMovement[0].tv;
		uvIncr[0].y = -matObj->m_textureMovement[0].tu;
		uvIncr[1].x = -matObj->m_textureMovement[1].tv;
		uvIncr[1].y = -matObj->m_textureMovement[1].tu;

		ReMaterialPtr reMat(new ReD3DX9Material_TexMat(dev, &data, (FLOAT*)uvIncr));
		matObj->SetReMaterial(reMat);
	} else {
		ReMaterialPtr reMat(new ReD3DX9Material(dev, &data));
		matObj->SetReMaterial(reMat);
	}
}

//! \brief Return a path pointing to where resource should be stored locally
//! \param gameFilesDir a path pointed to "GameFiles" folder of current game
//@@Assumption: caller must pass in a path pointed to "GameFiles" folder of current game.
std::string MeshRM::GetResourceLocalStore(const std::string& gameFilesDir) {
	std::string meshDir;
	if (gameFilesDir == "~") // If parent folder is not provided, use game default
		meshDir = IResource::BaseMeshPath(eMeshPath::CharacterMeshes);
	else
		meshDir = PathAdd(gameFilesDir, "CharacterMeshes");

	// Create directory if not already exists
	FileHelper::CreateFolderDeep(meshDir);

	return meshDir;
}

//! \brief Compose a file name for serialization based on hash code provided.
//! \param hash Hash code for the resource to be serialized.
std::string MeshRM::GetResourceFileName(UINT64 hash) {
	CStringA fileName;
	fileName.Format("%I64u.rmsh", hash);
	return fileName.GetString();
}

std::string MeshRM::GetResourceFilePath(UINT64 hash) {
	return PathAdd(IResource::BaseMeshPath(eMeshPath::CharacterMeshes), GetResourceFileName(hash));
}

std::string MeshRM::GetResourceURL(UINT64 hash) {
	std::string url;
	StrBuild(url, GetPatchAssetLocation() << "GameFiles_CharacterMeshes/" << GetResourceFileName(hash));
	url = ::CompressTypeExtAdd(url);
	return url;
}

//! Store each mesh in its own archive (a series of loose meshes).
//! StoreReMeshes should only write out a loose mesh file if it does not already exists
//! otherwise start-up performance will be affected when AddMesh is called [Yanfeng 11/2008].
bool MeshRM::StoreReMeshes(ReSkinMesh** meshes, UINT num) {
	if (!meshes)
		return true;

	// Write to default CharacterMeshes folder
	std::string pathStore = GetResourceLocalStore("~");
	ScopedDirectoryChanger sdc(pathStore);

	for (UINT i = 0; i < num; i++) {
		UINT64 templ = meshes[i]->GetHash();
		std::string fileName = GetResourceFileName(templ);
		std::wstring fileNameW = Utf8ToUtf16(fileName);

		CFileStatus fs;
		if (CFile::GetStatus(fileNameW.c_str(), fs)) {
			// Already exists, skip writing
			continue;
		}

		CFile fl;
		CFileException exc;
		if (!fl.Open(fileNameW.c_str(), CFile::modeCreate | CFile::modeWrite, &exc)) {
			return false;
		} else {
			CArchive single_mesh_archive(&fl, CArchive::store);
			StoreReMeshes((ReMesh**)meshes + i, 1, single_mesh_archive, MeshRM::RMP_ID_PERSISTENT);
			single_mesh_archive.Close();
			fl.Close();
		}
	}

	return true;
}

// write the mesh array "meshes" to the CArchive "ar" ONLY if has unique hash code
// TODO: sort by hash code before writing the meshes so "LoadReMesh" can quickly dup-check (binary search)
bool MeshRM::StoreReMeshes(ReMesh** meshes, UINT numMeshes, CArchive& ar, RMP_ID rmpId) {
	if (!meshes)
		return true;

	UINT* meshIndex = new UINT[numMeshes];
	UINT numUniquemeshes = 0;

	// go through the mesh list
	for (UINT i = 0; i < numMeshes; i++) {
		if (meshes[i] == NULL) {
			ASSERT(false);
			continue;
		}

		BOOL duped = FALSE;

		// get the hash code of the next mesh
		ReMeshData* desc0;
		meshes[i]->Access(&desc0);

		// see if it's in the hash code list
		for (UINT j = 0; j < numUniquemeshes; j++) {
			ReMeshData* desc1;
			meshes[meshIndex[j]]->Access(&desc1);
			// found a dup, do NOT stream
			if (desc0->Hash == desc1->Hash) {
				duped = TRUE;
				break;
			}
		}
		if (!duped) {
			meshIndex[numUniquemeshes++] = i;
		}
	}

	// write the number of meshes
	ar << numUniquemeshes;

	// create a ReMesh streaming object
	ReMeshStream streamer;

	// write the skinned or static meshes
	if (rmpId == MeshRM::RMP_ID_MIXED) {
		for (UINT i = 0; i < numUniquemeshes; i++) {
			ReSkinMesh* skin = dynamic_cast<ReSkinMesh*>(meshes[meshIndex[i]]);
			if (skin) {
				ar << MeshRM::RMP_ID_PERSISTENT;
				streamer.WriteSkin(skin, ar);
			} else {
				ar << MeshRM::RMP_ID_DYNAMIC;
				streamer.Write(meshes[meshIndex[i]], ar);
			}
		}
	} else if (rmpId == MeshRM::RMP_ID_PERSISTENT) {
		for (UINT i = 0; i < numUniquemeshes; i++)
			streamer.WriteSkin(static_cast<ReSkinMesh*>(meshes[meshIndex[i]]), ar);
	} else {
		for (UINT i = 0; i < numUniquemeshes; i++)
			streamer.Write(meshes[meshIndex[i]], ar);
	}

	// delete the hash code table
	delete[] meshIndex;

	return true;
}

void MeshRM::LoadReMeshes(CArchive& ar, ReD3DX9DeviceState* dev, RMP_ID rmpId) {
	UINT numMeshes;

	// get an ReMeshStream object
	ReMeshStream streamer;

	// read the number of meshes
	ar >> numMeshes;

	// stream the ReMeshes
	for (UINT i = 0; i < numMeshes; i++) {
		switch (rmpId) {
			case RMP_ID_PERSISTENT: {
				// the new ReSkinMesh
				ReSkinMesh* newMesh;
				// stream the ReMesh
				streamer.ReadSkin(&newMesh, ar, dev);
				if (newMesh) {
					ReMesh* mesh = FindMesh(newMesh->GetHash(), rmpId);
					if (!mesh)
						AddMesh((ReMesh*)newMesh, rmpId);
					else
						delete newMesh;
				}
				break;
			}

			case RMP_ID_MIXED: { // Not Real Pool - Not In Array!
				// Special case for pools of mixed type.  Thank you charles.
				int type;
				ar >> type;
				if (type == RMP_ID_PERSISTENT) {
					// the new ReSkinMesh
					ReSkinMesh* newMesh;
					// stream the ReMesh
					streamer.ReadSkin(&newMesh, ar, dev);
					if (newMesh) {
						ReMesh* mesh = FindMesh(newMesh->GetHash(), RMP_ID_DYNAMIC);
						if (!mesh)
							AddMesh((ReMesh*)newMesh, RMP_ID_DYNAMIC);
						else
							delete newMesh;
					}
				} else {
					// the new ReSkinMesh
					ReMesh* newMesh;
					// stream the ReMesh
					streamer.Read(&newMesh, ar, dev);
					if (newMesh) {
						ReMesh* mesh = FindMesh(newMesh->GetHash(), RMP_ID_DYNAMIC);
						if (!mesh)
							mesh = FindMesh(newMesh->GetHash(), RMP_ID_DYNAMIC); // always check dynamic pool now.
						if (!mesh)
							AddMesh(newMesh, RMP_ID_DYNAMIC);
						else
							delete newMesh;
					}
				}
				break;
			}

			case RMP_ID_LIBRARY:
			case RMP_ID_DYNAMIC:
			default: {
				// the new ReSkinMesh
				ReMesh* newMesh;
				// stream the ReMesh
				streamer.Read(&newMesh, ar, dev);
				if (newMesh) {
					ReMesh* mesh = FindMesh(newMesh->GetHash(), rmpId);
					if (!mesh)
						AddMesh(newMesh, rmpId);
					else
						delete newMesh;
				}
				break;
			}
		}
	}
}

MeshProxy* MeshRM::GetMeshProxy(UINT64 hash) {
	auto pMP = static_cast<MeshProxy*>(ResourceManager::FindResource(hash));
	if (pMP)
		return pMP;

	// Add the resource to the ResourceManager's map of managed resources.
	// Download of the data starts to happen when GetData is called on
	// the MeshProxy.
	pMP = static_cast<MeshProxy*>(AddResource(hash));
	return pMP;
}

bool MeshRM::LoadMeshCache(ReMesh** meshes, DWORD num, RMC_ID rmcId) {
	if (!meshes)
		return false;

	// DRF - Crash Fix
	std::lock_guard<fast_mutex> lock(ReMesh::m_mutex);

	auto pRMC = GetMeshCache(rmcId);
	if (!pRMC)
		return false;

	pRMC->BeginCache();
	pRMC->AddMeshes(meshes, num);
	pRMC->EndCache();
	return true;
}

bool MeshRM::LoadMeshCache(ReMesh* mesh, RMC_ID rmcId) {
	if (!mesh)
		return false;

	// DRF - Crash Fix
	std::lock_guard<fast_mutex> lock(ReMesh::m_mutex);

	auto pRMC = GetMeshCache(rmcId);
	if (!pRMC)
		return false;

	pRMC->BeginCache();
	pRMC->AddMesh(mesh);
	pRMC->EndCache();
	return true;
}

bool MeshRM::LoadMeshCache(ReMesh* mesh) {
	if (!mesh)
		return false;

	MeshRM::RMC_ID meshCacheId = RMC_ID_0;
	const ReMaterial* pMaterial = mesh->GetMaterial();
	if (pMaterial) {
		const ReRenderStateData* pRenderStateData = pMaterial->GetRenderStateData();
		//bool bConfiguredSortOrder = (pRenderStateData->SortOrder >= (2 << 4)) | (pRenderStateData->Mode.Zenable == FALSE); // RMC_ID_7
		bool bAlphaTest = pRenderStateData->Mode.Alphatestenable != 0; // RMC_ID_1
		bool bNoZWriteAndNoAlphaTest = (pRenderStateData->Mode.Zwriteenable == FALSE); // RMC_ID_2
		bool bMultiTex = mesh->GetNumUV() > 1; // RMC_ID_4
		meshCacheId = bMultiTex ? RMC_ID_4 : meshCacheId; // Multitextured
		meshCacheId = bNoZWriteAndNoAlphaTest ? RMC_ID_2 : meshCacheId; // Transparent
		meshCacheId = bAlphaTest ? RMC_ID_1 : meshCacheId; // Decal

		// Disable configured sort order. It may not be used and it causes problems with paperdolls.
		// Some accessories have a configured sort order, but movement objects ignore it.
		// Creating dynamic objects from these movement objects results in this setting being applied,
		// giving incorrect results.
		// Example: Avatar hair seems to have this setting, and therefore goes into mesh cache 7. This
		// mesh cache gets rendered before the transparent meshes, so the transparent meshes are drawn after
		// the hair. Since the hair has alpha transparency, it disables Z writes. Therefore all transparent
		// objects get drawn on top of the hair.
		//meshCacheId = bConfiguredSortOrder ? RMC_ID_7 : meshCacheId; // Fixed sort order
	}
	return LoadMeshCache(mesh, meshCacheId);
}

ReMesh* MeshRM::AddMesh(ReMesh* mesh, RMP_ID rmpId) {
	std::lock_guard<fast_mutex> lock(m_mtxPool);
	size_t i = (size_t)rmpId;

	if (i >= _countof(m_MeshPool)) {
		// Can't add mesh. Pool does not exist.
		//delete mesh; // Some callers expect mesh to remain valid.
		return nullptr;
	}

	ReMesh* pExistingMesh = m_MeshPool[i]->AddMesh(mesh);
	if (pExistingMesh) {
		// No need to add.
		//delete mesh; // Some callers expect mesh to remain valid.
		return pExistingMesh;
	}

	return nullptr;
}

ReMesh* MeshRM::AllocateReStaticMesh(const std::vector<std::pair<unsigned, unsigned>>& vertexAndIndexCounts, unsigned uvCount, bool triStrip) {
	ReD3DX9DeviceState* dev = NULL;
	auto pICE = IClientEngine::Instance();
	if (pICE)
		dev = pICE->GetReDeviceState(); // Try client version if exists
	else
		dev = ReD3DX9DeviceState::Instance(); // Otherwise use singleton -- this is to allow use of ReD3DX9DeviceState without a client engine

	ReMesh* mesh = new ReD3DX9StaticMesh(dev);

	// get the mesh array data
	ReMeshData* pData = NULL;
	mesh->Access(&pData);

	// calculate total vertex and index count
	unsigned totalVertexCount = 0, totalIndexCount = 0;
	unsigned meshCount = vertexAndIndexCounts.size();
	bool padding = false;

	for (auto pr : vertexAndIndexCounts) {
		// Vertex count
		auto vertexCount = pr.first;
		totalVertexCount += vertexCount;

		// Index count
		auto indexCount = pr.second;
		totalIndexCount += indexCount;
		if (triStrip && meshCount > 1) {
			padding = true;
			auto triCount = indexCount - 2;
			if (triCount % 2 == 1) {
				// If current mesh has odd number of triangles
				// Add five degenerated triangles to connect meshes while maintaining winding order
				totalIndexCount += 3; // 3 extra vertices: 1 at the beginning, 2 at the end
			} else {
				// Otherwise add four degenerated triangles to connect meshes
				totalIndexCount += 2; // 2 extra vertices: 1 at the beginning, 1 at the end
			}
		}
	}

	if (padding) {
		totalIndexCount--; // First mesh does not use leading vertex. This is to keep original winding order intact.
	}

	// fill in mesh array data
	pData->NumV = totalVertexCount;
	pData->NumI = totalIndexCount;
	pData->NumUV = uvCount == 0 ? 1 : uvCount;
	pData->PrimType = triStrip ? ReMeshPrimType::TRISTRIP : ReMeshPrimType::TRILIST;

	if (!mesh->Allocate()) {
		ASSERT(false);
		delete mesh;
		return nullptr;
	}

	// Initialize buffer
	memset(pData->pMem, 0, pData->DataSize);

	// Reset buffer filling offsets
	pData->NextVertexOffset = 0;
	pData->NextIndexOffset = 0;
	pData->PadDegeneratedTriangles = padding;
	return mesh;
}

void MeshRM::AppendEXMesh(ReMesh* mesh, CEXMeshObj* pEXMesh) {
	// get the mesh array data
	ReMeshData* pData = NULL;
	mesh->Access(&pData);

	ASSERT((pData->PrimType == ReMeshPrimType::TRISTRIP) == pEXMesh->IsTriStrip());
	ASSERT(pData->NumUV == 1 && pEXMesh->GetVertexUVCount() == 0 || pData->NumUV == pEXMesh->GetVertexUVCount());

	// append EXMesh into ReMesh
	BYTE *pBufBase, *pBufPtr;
	unsigned remainingBufSize;
	unsigned fillCount;

	/////////////////////////////////////////////////
	// Vertex

	// Position vectors
	pBufBase = pData->getPosPtr(0);
	pBufPtr = pData->getPosPtr(pData->NextVertexOffset);
	remainingBufSize = pData->getPosBufSize() - (pBufPtr - pBufBase);
	fillCount = pEXMesh->FillPositionBuffer(pBufPtr, remainingBufSize, pData->getPosStride());
	ASSERT(fillCount == pEXMesh->GetVertexCount());

	// Normal vectors
	pBufBase = pData->getNmlPtr(0);
	pBufPtr = pData->getNmlPtr(pData->NextVertexOffset);
	remainingBufSize = pData->getNmlBufSize() - (pBufPtr - pBufBase);
	fillCount = pEXMesh->FillNormalBuffer(pBufPtr, remainingBufSize, pData->getNmlStride());
	ASSERT(fillCount == pEXMesh->GetVertexCount());

	// UV coordinates
	pBufBase = pData->getUVsPtr(0);
	pBufPtr = pData->getUVsPtr(pData->NextVertexOffset);
	remainingBufSize = pData->getUVsBufSize() - (pBufPtr - pBufBase);
	if (pEXMesh->GetVertexUVCount() > 0) {
		fillCount = pEXMesh->FillUVCoordsBuffer(pBufPtr, remainingBufSize, pData->getUVsStride());
		ASSERT(fillCount == pEXMesh->GetVertexCount());
	} // otherwise, use zero value as the bufer is initialized

	// Update vertex count
	pData->NextVertexOffset += pEXMesh->GetVertexCount();

	// Update base index
	auto currBaseIndex = pData->NextBaseIndex;
	pData->NextBaseIndex += pEXMesh->GetVertexCount();

	/////////////////////////////////////////////////
	// Indices
	unsigned prependIndexOffset = pData->NextIndexOffset;
	if (pData->PadDegeneratedTriangles && pData->NextIndexOffset != 0) {
		pData->NextIndexOffset++; // Reserve space for leading dummy vertex (except for the first mesh)
	}

	pBufBase = pData->getIdxPtr(0);
	pBufPtr = pData->getIdxPtr(pData->NextIndexOffset);
	remainingBufSize = pData->getIdxBufSize() - (pBufPtr - pBufBase);
	fillCount = pEXMesh->FillIndexBufferShort(pBufPtr, remainingBufSize, currBaseIndex, pData->getIdxStride());
	ASSERT(fillCount == pEXMesh->GetIndexCount());

	// Update index count
	pData->NextIndexOffset += pEXMesh->GetIndexCount();

	// Padding degenerated triangles if necessary
	if (pData->PadDegeneratedTriangles) {
		ASSERT(pData->PrimType == ReMeshPrimType::TRISTRIP);
		if (fillCount == pEXMesh->GetIndexCount()) { // Don't proceed if buffer is already too small for the mesh itself
			// Duplicate first vertex
			if (prependIndexOffset != 0) {
				short firstIndex = *(short*)(pData->getIdxPtr(prependIndexOffset + 1));
				*(short*)(pData->getIdxPtr(prependIndexOffset)) = firstIndex;
			}

			// Repeat last vertex 1 or 2 times
			short lastIndex = *(short*)(pData->getIdxPtr(pData->NextIndexOffset - 1));
			size_t recurrence = pEXMesh->GetIndexCount() % 2 + 1; // If odd, repeat twice, otherwise repeat once

			// Make sure we still have enough space left in buffer
			pBufPtr = pData->getIdxPtr(pData->NextIndexOffset);
			remainingBufSize = pData->getIdxBufSize() - (pBufPtr - pBufBase);

			if (remainingBufSize < pData->getIdxStride() * recurrence) {
				// buffer too small
				ASSERT(false);
			} else {
				for (auto i = 0; i < recurrence; i++) {
					// Duplicate last vertex
					*(short*)(pData->getIdxPtr(pData->NextIndexOffset)) = lastIndex;
					pData->NextIndexOffset++;
				}
			}
		}
	}
}

void MeshRM::BuildReMesh(ReMesh* mesh, bool convertToTriStrip) {
	ReMeshData* pData = 0;
	mesh->Access(&pData);
	ASSERT(pData->NumV == pData->NextVertexOffset);
	ASSERT(pData->NumI == pData->NextIndexOffset);

	mesh->Build(convertToTriStrip);
	mesh->CreateShader(NULL);
}

} // namespace KEP
