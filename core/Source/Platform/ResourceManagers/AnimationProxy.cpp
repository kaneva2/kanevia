///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "AnimationProxy.h"

#include "SkeletalAnimationManager.h"
#include "CSkeletonAnimation.h"
#include "CBoneAnimationClass.h"
#include "DownloadPriority.h"
#include "CBoneClass.h"
#include "IClientEngine.h"
#include "ContentService.h"
#include "common/include/IMemSizeGadget.h"

#include "LogHelper.h"
static LogInstance("Instance");

#define MAGIC_HEADER_ANIMPROXY 0x7041 // 'Ap' - Magic word to indicate the use of schema version other than legacy format with header size

namespace KEP {

AnimationProxy::AnimationProxy(ResourceManager* pAnimRM, UINT64 resKey, bool isLocal) :
		IResource(pAnimRM, resKey, isLocal) {
	Init();
	SetState(IResource::State::INITIALIZED_NOT_LOADED);
}

AnimationProxy::AnimationProxy(AnimationRM* pAnimRM, CSkeletonAnimation* pSA) :
		IResource(pAnimRM, 0, true) {
	Init();
	if (pSA && LoadFromSkeletonAnimation(pSA))
		SetState(State::LOADED_AND_CREATED);
}

void AnimationProxy::Init() {
	m_animationData.Reset();
}

bool AnimationProxy::LoadFromSkeletonAnimation(const CSkeletonAnimation* pSA) {
	if (!pSA)
		return false;

	m_animationData.m_numBones = pSA->m_numBones;
	m_animationData.m_animDurationMs = pSA->m_animDurationMs;
	SetHashKey(pSA->m_animGlid);

	m_animationData.m_ppBoneAnimationObjs = new CBoneAnimationObject*[m_animationData.m_numBones];
	for (UINT i = 0; i < m_animationData.m_numBones; i++) {
		auto pBAO = pSA->GetBoneAnimationObj(i);
		if (pBAO)
			pBAO->Clone(&m_animationData.m_ppBoneAnimationObjs[i]);
	}

	if (pSA->m_skeletonDef)
		m_animationData.m_skeletonDef = new SkeletonDef(*pSA->m_skeletonDef);
	else
		m_animationData.m_skeletonDef = nullptr;

	return true;
}

AnimationProxy::~AnimationProxy() {
	if (!StateIsLoadedAndCreated())
		return;

	if (m_animationData.m_ppBoneAnimationObjs) {
		for (UINT i = 0; i < m_animationData.m_numBones; i++) {
			if (m_animationData.m_ppBoneAnimationObjs[i]) {
				delete m_animationData.m_ppBoneAnimationObjs[i];
				m_animationData.m_ppBoneAnimationObjs[i] = nullptr;
			}
		}
		delete[] m_animationData.m_ppBoneAnimationObjs;
		m_animationData.m_ppBoneAnimationObjs = nullptr;
	}

	if (m_animationData.m_skeletonDef) {
		delete m_animationData.m_skeletonDef;
		m_animationData.m_skeletonDef = nullptr;
	}

	m_animationData.m_numBones = 0;
}

const AnimationProxy::AnimationData* AnimationProxy::GetAnimDataPtr() const {
	return &m_animationData;
}

bool AnimationProxy::HasAnimData() const {
	return StateIsLoadedAndCreated();
}

void AnimationProxy::StartLoad() {
	if (StateIsLoadedAndCreated() || StateIsErrored())
		return; // Already loaded or can't load.

	AnimationRM::Instance()->QueueResource(this);
}

const AnimationProxy::AnimationData* AnimationProxy::GetAnimData(bool bImmediate) {
	// Already Created ?
	if (StateIsLoadedAndCreated())
		return &m_animationData;

	// Ready To Read ?
	if (!StateIsInitializedNotLoaded())
		return nullptr;

	// Read From Memory And Create Animation
	GLID glid = (GLID)GetHashKey();
	auto pSA = AnimationRM::Instance()->GetSkeletonAnimationByGlid(glid);
	if (pSA) {
		if (!LoadFromSkeletonAnimation(pSA))
			return nullptr;
		SetState(State::LOADED_AND_CREATED);
		return &m_animationData;
	}

	// Read Immediately Or Queue Asynchronously ?
	if (bImmediate) {
		if (AnimationRM::Instance()->QueueFileForRead(this, true, false) && StateIsLoadedAndCreated())
			return &m_animationData;
	} else
		AnimationRM::Instance()->QueueResource(this);

	return nullptr;
}

bool AnimationProxy::CreateResource() {
	ASSERT(GetRawBuffer() != NULL && GetRawSize() != 0);
	bool retVal = true;
	CMemFile file;
	file.Attach(GetRawBuffer(), GetRawSize(), 0);
	CArchive animArchive(&file, CArchive::load);

	// DRF - Added - HashKey Is Glid
	auto hashKey = GetHashKey();
	SetGlid((GLID)hashKey);

	// This reads an animation that's been written using
	// AnimationProxy::Write().
	// I have the opportunity here, to create my own file
	// format that's independent of CSkeletonAnimation,
	// I think.  I don't need to follow the format that's in
	// the .ANM file, in other words.
	if (!Read(animArchive)) {
		animArchive.Abort();
		retVal = false;
	} else {
		animArchive.Close();
		SetState(State::LOADED_AND_CREATED);
	}

	// clean up
	file.Detach();
	FreeRawBuffer();

	return retVal;
}

void AnimationProxy::Write(CArchive& ar) const {
	unsigned short magic = MAGIC_HEADER_ANIMPROXY;
	unsigned short schemaVersion = 1; // version 1

	ar << magic; // magic header to differentiate new file format (with ordinal version#) from old (with header size)
	ar << schemaVersion;

	ar << m_animationData.m_version;
	ar << m_animationData.m_numBones;
	ar << (UINT)m_animationData.m_animDurationMs;
	ar << m_animationData.m_animLooping;

	// write the animation data
	// following the pattern used in CSkeletalAnimation of
	// using a temporary so that the original isn't modified

	// NOTE: CArchive uses a CObject map and to avoid writing out multiple references of a shared CObject
	// I have to create an array to hold all temp pointers first and then dispose them once serialization is done.
	CBoneAnimationObject** ppBAO_new = new CBoneAnimationObject*[m_animationData.m_numBones];
	for (UINT i = 0; i < m_animationData.m_numBones; i++) {
		m_animationData.m_ppBoneAnimationObjs[i]->Clone(&ppBAO_new[i]);
		ppBAO_new[i]->RegenerateKeyFrames(0.25f, 0.05f); // UGC animation: angle tolerate = 0.25, pos tolerate = 0.05
		ppBAO_new[i]->m_compressed = TRUE;
		ar << ppBAO_new[i];
	}

	// Write skeleton definition
	bool hasSkeletonDef = m_animationData.m_skeletonDef != NULL;
	ar << hasSkeletonDef;
	if (hasSkeletonDef)
		m_animationData.m_skeletonDef->Write(ar);
	;

	// Now dispose temp buffers
	for (UINT i = 0; i < m_animationData.m_numBones; i++)
		delete ppBAO_new[i];
	delete ppBAO_new;
}

bool AnimationProxy::Validate(const AnimationData& data) {
	// Try to validate the animation data, before allocating memory.
	// These should be totally unreasonable numbers that we'll never see
	// in a valid file.  I don't check the version, because we don't have
	// any sytem for assigning them that I can rely on right now, so I can't
	// check for an invalid one AFAIK.
	bool ok = ((data.m_animDurationMs <= 3600000.0) && (data.m_animLooping <= 1 || data.m_animLooping == (UINT)-1) && (data.m_numBones <= 1000));
	if (!ok) {
		LogError("INVALID ANIMATION - numBones=" << data.m_numBones << " animLengthMs=" << FMT_TIME << data.m_animDurationMs);
	}
	return ok;
}

// Let the exception flow up to AnimationProxy::Read()
bool AnimationProxy::ReadLegacy(CArchive& ar) {
	//NOTE: m_headerSize0	=	sizeof(UINT) * 4 + sizeof(UINT64);
	//first 4 bytes (m_version) has been processed by AnimationProxy::Read
	//Now continue with sizeof(UINT)*3 + sizeof(UINT64)--note that following three fields was serialized twice in legacy code
	ar >> m_animationData.m_numBones;
	UINT animLen = 0;
	ar >> animLen;
	m_animationData.m_animDurationMs = (TimeMs)animLen;
	int fps;
	ar >> fps;
	UINT64 badData;
	ar >> badData;
	ar >> m_animationData.m_numBones;
	ar >> animLen;
	m_animationData.m_animDurationMs = (TimeMs)animLen;
	ar >> fps;
	m_animationData.m_animLooping = (UINT)-1;

	if (m_animationData.m_version == 0 && m_animationData.m_numBones == 0) {
		LogError("Invalid empty animation data");
		return false;
	}

	LogWarn("LEGACY ANIMATION - version=" << m_animationData.m_version << " bones=" << m_animationData.m_numBones);

	// DRF - Crash Fix
	bool isValid = Validate(m_animationData);
	if (!isValid) {
		LogError("Validate() FAILED");
		return false;
	}

	// read the animation data the read function handles compressed, or otherwise, data
	m_animationData.m_ppBoneAnimationObjs = new CBoneAnimationObject*[m_animationData.m_numBones];
	for (size_t i = 0; i < m_animationData.m_numBones; i++) {
		if (ar.IsBufferEmpty()) {
			LogWarn("BUFFER EMPTY - bone " << (i + 1) << " of " << m_animationData.m_numBones);
			//			return false; // ignore for now - IsBufferEmpty() might be wrong
		}
		ar >> m_animationData.m_ppBoneAnimationObjs[i];
	}

	return true;
}

// ED-6193 - DRF - NPC Incompatible Animations - GLID -> defIndex
enum DEF_INDEX {
	DEF_INDEX_MALE,
	DEF_INDEX_FEMALE,
	DEF_INDEXES
};
const static std::map<GLID, DEF_INDEX> s_animDefsMap = {
	{ 136, DEF_INDEX_MALE }, // Male Disco
	{ 59, DEF_INDEX_FEMALE } // Female Disco
};
const static std::vector<std::string> s_skeletonDefBoneNames[DEF_INDEXES] = {
	{ // Male Movement Object Bone Names (57 bones)
		"Bip01", "Bip01 Pelvis", "Bip01 Spine", "Bip01 Spine1", "Bip01 Spine2", "Bip01 Neck", "Bip01 Head", "bone - lip_Rcorner", "bone - lip_Lcorner", "bone - lip_Upper", "bone - cheeks", "bone - brow_Redge", "bone - brow_Center", "bone - brow_Ledge", "bone - CAM", "bone - R_eye", "bone - L_eye", "bone - eyelid_Rupper", "bone - eyelid_Lupper", "bone - jaw", "bone - lip_Lower", "Bip01 L Clavicle", "Bip01 L UpperArm", "Bip01 L Forearm", "Bip01 L Hand", "Bip01 L Finger0", "Bip01 L Finger01", "Bip01 L Finger1", "Bip01 L Finger11", "Bip01 L Finger2", "Bip01 L Finger21", "Bip01 L Finger3", "Bip01 L Finger31", "Bip01 L ForeTwist", "Bip01 R Clavicle", "Bip01 R UpperArm", "Bip01 R Forearm", "Bip01 R Hand", "Bip01 R Finger0", "Bip01 R Finger01", "Bip01 R Finger1", "Bip01 R Finger11", "Bip01 R Finger2", "Bip01 R Finger21", "Bip01 R Finger3", "Bip01 R Finger31", "Bip01 R ForeTwist", "Bip01 L Thigh", "Bip01 L Calf", "Bip01 L Foot", "Bip01 L Toe0", "Bip01 R Thigh", "Bip01 R Calf", "Bip01 R Foot", "Bip01 R Toe0", "bone - L_mount", "bone - R_mount" },
	{ // Female Movement Object Bone Names (57 bones)
		"Bip01", "Bip01 Pelvis", "Bip01 Spine", "Bip01 Spine1", "Bip01 Spine2", "Bip01 Neck", "Bip01 Head", "bone - lip_Rcorner", "bone - lip_Lcorner", "bone - lip_Upper", "bone - cheeks", "bone - brow_Redge", "bone - brow_Center", "bone - brow_Ledge", "bone - CAM", "bone - R_eye", "bone - L_eye", "bone - eyelid_Rupper", "bone - eyelid_Lupper", "bone - jaw", "bone - lip_Lower", "Bip01 L Clavicle", "Bip01 L UpperArm", "Bip01 L Forearm", "Bip01 L Hand", "Bip01 L Finger0", "Bip01 L Finger01", "Bip01 L Finger1", "Bip01 L Finger11", "Bip01 L Finger2", "Bip01 L Finger21", "Bip01 L Finger3", "Bip01 L Finger31", "bone - L_mount", "Bip01 L ForeTwist", "Bip01 R Clavicle", "Bip01 R UpperArm", "Bip01 R Forearm", "Bip01 R Hand", "Bip01 R Finger0", "Bip01 R Finger01", "Bip01 R Finger1", "Bip01 R Finger11", "Bip01 R Finger2", "Bip01 R Finger21", "Bip01 R Finger3", "Bip01 R Finger31", "bone - R_mount", "Bip01 R ForeTwist", "Bip01 L Thigh", "Bip01 L Calf", "Bip01 L Foot", "Bip01 L Toe0", "Bip01 R Thigh", "Bip01 R Calf", "Bip01 R Foot", "Bip01 R Toe0" }
};

bool AnimationProxy::Read(CArchive& ar) {
	try {
		unsigned short magic;
		ar >> magic;

		// Legacy Or New Format ?
		if (magic != MAGIC_HEADER_ANIMPROXY) {
			// Read Legacy Format
			unsigned short tmp;
			ar >> tmp;
			m_animationData.m_version = (tmp << 16) + magic; // store first 4 bytes into m_version
			m_animationData.m_skeletonDef = NULL; // Fix skeletonDef pointer overwritten by legacy serialization code
			if (!ReadLegacy(ar)) {
				LogError("ReadLegacy() FAILED");
				return false;
			}

			return true;
		}

		// Read New Format
		unsigned short schemaVersion;
		ar >> schemaVersion;
		ar >> m_animationData.m_version;
		ar >> m_animationData.m_numBones;
		UINT animLen = 0;
		ar >> animLen;
		m_animationData.m_animDurationMs = (TimeMs)animLen;
		if (schemaVersion == 0) {
			UINT fps;
			ar >> fps;
			m_animationData.m_animLooping = (UINT)-1; // Unknown -- reverse get from header
		} else {
			// v1 and later
			ar >> m_animationData.m_animLooping; // significant -- update header with this
		}

		// DRF - Crash Fix
		bool isValid = Validate(m_animationData);
		if (!isValid) {
			LogError("Validate() FAILED");
			return false;
		}

		// read the animation data the read function handles compressed, or otherwise, data
		m_animationData.m_ppBoneAnimationObjs = new CBoneAnimationObject*[m_animationData.m_numBones];
		for (size_t i = 0; i < m_animationData.m_numBones; i++) {
			if (ar.IsBufferEmpty()) {
				LogWarn("BUFFER EMPTY - bone " << (i + 1) << " of " << m_animationData.m_numBones);
				//			return false; // ignore for now - IsBufferEmpty() might be wrong
			}
			ar >> m_animationData.m_ppBoneAnimationObjs[i];
		}

		// Read skeleton definition
		// ED-6193 - DRF - NPC Incompatible Animations
		// It appears that all UGC animations have these bone names data
		// We are just missing the names for old studio animations so we must
		// manually assign those.  With these names it is able to match up the
		// animation bones to the actual avatar bones by name.  Without these
		// it just takes them in order which leads to torn up avatars and NPCs.
		bool hasSkeletonDef = false;
		ar >> hasSkeletonDef;
		m_animationData.m_skeletonDef = NULL;
		if (hasSkeletonDef) {
			m_animationData.m_skeletonDef = new SkeletonDef;
			m_animationData.m_skeletonDef->Read(ar);
		} else {
			// ED-6193 - DRF - NPC Incompatible Animations - Added
			// - First check if specified in content metadata
			// - Second check if hardcoded in the list of studio animations
			DEF_INDEX defIndex = DEF_INDEXES;
			GLID glid = GetGlid();
			if (IS_UGC_GLID(glid)) {
				// Check Content Metadata
				ContentMetadata md(glid, true);
				if (md.IsValid() && (md.getUseType() == USE_TYPE_ANIMATION)) {
					auto useValue = (USE_TYPE_ANIMATION_USE_VALUE)md.getUseValue();
					if (useValue == USE_TYPE_ANIMATION_USE_VALUE::MALE)
						defIndex = DEF_INDEX_MALE;
					else if (useValue == USE_TYPE_ANIMATION_USE_VALUE::FEMALE)
						defIndex = DEF_INDEX_FEMALE;
				}
			} else {
				// Check Hardcoded List Of Studio Animations
				auto itr = s_animDefsMap.find(glid);
				if (itr != s_animDefsMap.end())
					defIndex = itr->second;
			}

			// Create Animation Skeleton Definition?
			if (defIndex < DEF_INDEXES) {
				m_animationData.m_skeletonDef = new SkeletonDef;
				for (const auto& boneName : s_skeletonDefBoneNames[(size_t)defIndex])
					m_animationData.m_skeletonDef->AddBone(boneName, 0, "");
			}
		}
	}

	catch (CException* e) {
		LogError("EXCEPTION(CException)");
		e->Delete();
		return false;
	}

	catch (...) {
		LogError("EXCEPTION(...)");
		return false;
	}

	return true;
}

BYTE* AnimationProxy::GetSerializedData(FileSize& dataLen) {
	CMemFile memFile;
	CArchive ar(&memFile, CArchive::store);
	Write(ar);
	ar.Close();
	memFile.Flush();
	dataLen = (FileSize)memFile.GetLength();
	return dataLen ? memFile.Detach() : nullptr;
}

void AnimationProxy::BakeEditingAttributes(float speedFactor, TimeMs cropStart, TimeMs cropEnd, BOOL looping) {
	// must be in main thread
	jsVerifyReturnVoid(m_animationData.m_ppBoneAnimationObjs != NULL);

	AnimKeyFrame** newBoneAnimContentsArrays = new AnimKeyFrame*[m_animationData.m_numBones];
	int* newBoneAnimFrmCnts = new int[m_animationData.m_numBones];
	TimeMs* newBoneAnimLens = new TimeMs[m_animationData.m_numBones];

	for (UINT boneLoop = 0; boneLoop < m_animationData.m_numBones; boneLoop++) {
		newBoneAnimContentsArrays[boneLoop] = NULL;
		newBoneAnimFrmCnts[boneLoop] = 0;
		newBoneAnimLens[boneLoop] = 0.0;

		CBoneAnimationObject* pBoneAnimObj = m_animationData.m_ppBoneAnimationObjs[boneLoop];
		if (pBoneAnimObj->m_animKeyFrames <= 0) // do nothing if no data
			continue;

		// 1. Cropping
		CBoneObject cropHelper;
		int startingFrame = 0;
		int endingFrame = pBoneAnimObj->m_animKeyFrames - 1;

		// 1.0 Validate cropping parameters
		TimeMs startTime = cropStart;
		TimeMs endTime = cropEnd;
		if (endTime < 0.0) {
			LogWarn("INVALID cropEndTime=" << endTime);
			endTime = pBoneAnimObj->m_animDurationMs;
		}

		if (endTime > pBoneAnimObj->m_animDurationMs)
			endTime = pBoneAnimObj->m_animDurationMs;

		if (startTime > endTime)
			startTime = endTime;

		if (startTime < 0.0)
			startTime = 0.0;

		if (startTime == endTime) {
			if (startTime == 0.0)
				endTime += 1.0;
			else
				startTime -= 1.0;
		}

		// 1.1 Determine frame boundaries
		bool bPadEndingFrame = false; // true if an additional frame is needed to store ending state

		startingFrame = pBoneAnimObj->GetKeyFrameIndexAtTime(startTime);

		if (endTime >= 0.0 && (endTime < m_animationData.m_animDurationMs)) {
			endingFrame = pBoneAnimObj->GetKeyFrameIndexAtTime(endTime);

			if (endingFrame < pBoneAnimObj->m_animKeyFrames - 1) { // Crop in the middle of animation
				bPadEndingFrame = true; // An extra frame is needed to store ending state
				endingFrame++;
			}
		}

		newBoneAnimFrmCnts[boneLoop] = endingFrame - startingFrame + 1;
		newBoneAnimLens[boneLoop] = endTime - startTime;

		if (newBoneAnimFrmCnts[boneLoop] > 0) {
			// 1.2 Crop frames
			AnimKeyFrame* animContents = new AnimKeyFrame[newBoneAnimFrmCnts[boneLoop]];
			newBoneAnimContentsArrays[boneLoop] = animContents;

			TimeMs firstFrameLen = 0.0;
			if (startingFrame > 0.0)
				firstFrameLen = m_animationData.m_ppBoneAnimationObjs[boneLoop]->m_pAnimKeyFrames[startingFrame].m_timeMs - m_animationData.m_ppBoneAnimationObjs[boneLoop]->m_pAnimKeyFrames[startingFrame - 1].m_timeMs;
			else
				firstFrameLen = m_animationData.m_ppBoneAnimationObjs[boneLoop]->m_pAnimKeyFrames[startingFrame].m_timeMs;

			if (firstFrameLen == 0.0)
				firstFrameLen = 1.0; // minimal duration: 1ms (just for still frame to render)

			for (int frameLoop = 0; frameLoop < newBoneAnimFrmCnts[boneLoop]; frameLoop++) {
				animContents[frameLoop] = m_animationData.m_ppBoneAnimationObjs[boneLoop]->m_pAnimKeyFrames[frameLoop + startingFrame];
				animContents[frameLoop].m_timeMs -= startTime;
			}

			// 1.3 Adjust first and last frame
			if (animContents[0].m_timeMs != firstFrameLen) {
				// Adjust frame so it will match crop marker
				AnimKeyFrame& animData = animContents[0];
				Matrix44f startPose;
				pBoneAnimObj->InterpolateKeyFrames(startTime, &startPose, false);
				animData.up = startPose.GetRowY();
				animData.at = startPose.GetRowZ();
				animData.pos = startPose.GetTranslation();
			}

			if (newBoneAnimFrmCnts[boneLoop] > 1) {
				if (animContents[newBoneAnimFrmCnts[boneLoop] - 2].m_timeMs > endTime - 1.0) {
					// Adjust 2nd-to-last frame so it will match crop marker
					AnimKeyFrame& animData = animContents[newBoneAnimFrmCnts[boneLoop] - 2];
					animData.m_timeMs = endTime - 1.0;
				}

				if (bPadEndingFrame) {
					// Adjust last frame so it will match crop marker
					AnimKeyFrame& animData = animContents[newBoneAnimFrmCnts[boneLoop] - 1];
					Matrix44f endPose;
					pBoneAnimObj->InterpolateKeyFrames(endTime, &endPose, false);
					animData.up = endPose.GetRowY();
					animData.at = endPose.GetRowZ();
					animData.pos = endPose.GetTranslation();

					TimeMs et = endTime + speedFactor;
					animData.m_timeMs = et;
				}
			}

			// 2. Scale timeline
			if (speedFactor != 1.0f && speedFactor > 0.0f) {
				for (int frameLoop = 0; frameLoop < newBoneAnimFrmCnts[boneLoop]; frameLoop++)
					animContents[frameLoop].m_timeMs = animContents[frameLoop].m_timeMs / speedFactor;
				newBoneAnimLens[boneLoop] = newBoneAnimLens[boneLoop] / speedFactor;
			}

			// 2.2 make sure first frame's timestamp is not zero (while cropped at key-frame boundary)
			if (animContents[0].m_timeMs == 0.0)
				animContents[0].m_timeMs = 1.0;
		}

	} // end bone loop

	// Copy and realize animation data
	for (UINT boneLoop = 0; boneLoop < m_animationData.m_numBones; boneLoop++) {
		CBoneAnimationObject* pBoneAnimObj = m_animationData.m_ppBoneAnimationObjs[boneLoop];
		if (pBoneAnimObj->m_animKeyFrames <= 0) // do nothing if no data
			continue;

		AnimKeyFrame* pOldCntArr = pBoneAnimObj->m_pAnimKeyFrames;
		pBoneAnimObj->m_animDurationMs = newBoneAnimLens[boneLoop];
		pBoneAnimObj->m_animKeyFrames = newBoneAnimFrmCnts[boneLoop];
		pBoneAnimObj->m_pAnimKeyFrames = newBoneAnimContentsArrays[boneLoop];
		delete[] pOldCntArr;
	} // end bone loop

	if (cropEnd == -1.0)
		cropEnd = m_animationData.m_animDurationMs;
	m_animationData.m_animDurationMs = std::max((TimeMs)((cropEnd - cropStart) / speedFactor), 1.0);
	m_animationData.m_animLooping = looping;

	delete newBoneAnimLens;
	delete newBoneAnimFrmCnts;
	delete newBoneAnimContentsArrays;
}

void AnimationProxy::AnimationData::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		for (size_t i = 0; i < m_numBones; i++) {
			pMemSizeGadget->AddObject(m_ppBoneAnimationObjs[i]);
		}
		pMemSizeGadget->AddObject(m_skeletonDef);
	}
}

void AnimationProxy::GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
	if (pMemSizeGadget->AddObjectSizeof(this)) {
		pMemSizeGadget->AddObject(m_animationData);
	}
}

} // namespace KEP
