///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "InternetManager.h"
#include "log4cplus/logger.h"
using namespace log4cplus;

#define MAX_UPLOAD_SIZE 10 * 1024 * 1024 // Limit upload size to 10MB: this is a hard cap regardless of upload types

#define UM_THREADS 1
#define UM_THREAD_JS_PRIORITY (jsThdPriority) PR_NORMAL

namespace KEP {

class UploadManager : public InternetManager {
public:
	UploadManager(Dispatcher& disp);

	bool UploadFileAsync(const char* uploadFile, const char* url, TaskPriority prio, ValuesList* pAddlValues = NULL, bool authFirst = false, const char* authUrl = NULL, IEvent* e = NULL, DWORD eventMask = 0);
	bool UploadFileAsync(const char* uploadName, char* dataBuf, DWORD dataBufLen, const char* url, TaskPriority prio, ValuesList* pAddlValues = NULL, bool authFirst = false, const char* authUrl = NULL, IEvent* e = NULL, DWORD eventMask = 0);
	bool UploadMultiFilesAsync(const std::vector<std::string>& uploadFiles, const char* url, TaskPriority prio, ValuesList* pAddlValues = NULL, bool authFirst = false, const char* authUrl = NULL, IEvent* e = NULL, DWORD eventMask = 0);
	//bool UploadFileSync( const char* uploadFile, const char* url, ProgressHandler *handler, ValuesList* pAddlValues = NULL, bool authFirst = false, const char* authUrl = NULL );

	static UploadManager* Ptr();
	static const ULONG BUFF_SIZE = 32768;

protected:
	InternetManagerThread* createWorkerThread();

	static UploadManager* pInst;
};

} // namespace KEP
