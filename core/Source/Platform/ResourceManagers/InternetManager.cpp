///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "InternetManager.h"
#include "Event/EventSystem/Dispatcher.h"
#include <algorithm>
#include <functional>

namespace KEP {

static LogInstance("Instance");

InternetManager::InternetManager(const char* name, Dispatcher& disp, int threadCount, jsThdPriority thdPrio) :
		m_logger(Logger::getInstance(Utf8ToUtf16(name))), m_name(name), m_dispatcher(disp), m_threadCount(threadCount), m_threadPriority(thdPrio), m_sync(), m_started(false), m_lastTaskId(0) {
	jsVerifyDo(threadCount > 0 && threadCount < 5, return );
}

void InternetManager::CancelAllTasks() {
	// tell each thread to cancel
	// wakeup any sleeping threads
	for (std::vector<InternetManagerThread*>::iterator i = m_thds.begin(); i != m_thds.end(); i++) {
		(*i)->CancelCurrent();
	}

	// clear the list
	{
		std::lock_guard<fast_recursive_mutex> lock(m_sync);

		for (size_t i = 0; i < m_queue.size(); i++) {
			TaskInfo* info = m_queue[i].first;
			delete info;
		}

		m_queue.clear();
	}
}

void InternetManager::AdjustPriority(const std::string& url, int prio) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	// Walk the list looking for this url
	for (TaskInfoQueue::iterator i = m_queue.begin(); i != m_queue.end(); i++) {
		TaskInfoPair& p = (*i);
		const TaskInfo* info = p.first;

		// If the url is in the list and has a different priority
		if (info->m_url == url && p.second != prio) {
			p.second = prio;
			make_heap(m_queue.begin(), m_queue.end());

			// No two entries in m_queue can have the same url so we're done here
			break;
		}
	}
}

bool InternetManager::isAlreadyQueued(const char* /*url*/, TaskPriority /*prio*/, IEvent* /*e*/) {
	// return false by default
	// sub-classes should override this function if duplicated tasks are undesired
	return false;
}

bool InternetManager::QueueTask(TaskInfo* info, TaskPriority prio) {
	bool found;
	{
		std::lock_guard<fast_recursive_mutex> lock(m_sync);

		found = isAlreadyQueued(info->m_url.c_str(), prio);

		if (!found) {
			// not found anywhere, add to queue
			m_queue.push_back(TaskInfoPair(info, prio));

			// this moves the element at the end to its proper position in the heap
			push_heap(m_queue.begin(), m_queue.end());

			LogInfo("name=" << m_name << " url=" << info->m_url);
		}
	}

	m_sema.increment(); // wake up a threads

	return !found;
}

LONG InternetManager::getNextTaskId() {
	return InterlockedIncrement(&m_lastTaskId);
}

ULONG InternetManagerThread::_doWork() {
	m_buffer = (char*)malloc(GetBufferSize());
	if (!m_buffer)
		return (ULONG)-1;

	std::unique_lock<fast_recursive_mutex> lock(m_taskQueueSync, std::defer_lock);
	TaskPriority priority;

	while (!isTerminated()) {
		lock.lock();

		if (!m_taskQueue.empty()) {
			// this moves the highest priority element to the back
			pop_heap(m_taskQueue.begin(), m_taskQueue.end());
			m_currentDL = m_taskQueue.back().first;
			priority = m_taskQueue.back().second;
			LogInfo("url=" << m_currentDL->m_url << " prio=" << m_taskQueue.back().second);

			m_taskQueue.pop_back();
		}

		bool resPreProc = false;
		if (m_currentDL != NULL) {
			UpdateProgress(m_currentDL, TS_START);
			resPreProc = PreProcess(m_currentDL);
		}

		lock.unlock();

		if (m_currentDL != 0) {
			m_cancelCurrent = false;

			bool res = false;
			if (resPreProc) {
				res = ProcessTask(m_currentDL);
			}

			lock.lock();

			if (res) {
				PostProcess(m_currentDL);
				UpdateProgress(m_currentDL, TS_COMPLETE, 100.0f);
			} else {
				PostProcessWhenFailed(m_currentDL);
				UpdateProgress(m_currentDL, TS_FAILED);
			}

			delete m_currentDL;
			m_currentDL = 0;

			lock.unlock();
		} else {
			// Give up CPU if no task found
			Sleep(1);
		}
	}

	free(m_buffer);
	m_buffer = NULL;

	return 0;
}

void InternetManager::StartThreads() {
	jsVerifyReturnVoid(!m_started);

	for (int i = 0; i < m_threadCount; i++) {
		InternetManagerThread* workerThread = createWorkerThread();
		m_thds.push_back(workerThread);
		m_thds.back()->start();
	}

	m_started = true;
}

void InternetManager::TerminateThreads() {
	jsVerifyReturnVoid(m_started);

	for (std::vector<InternetManagerThread*>::iterator it = m_thds.begin(); it != m_thds.end(); ++it) {
		InternetManagerThread* workerThread = *it;
		workerThread->stop(30, false); //@@Watch: duration
	}

	//@@BUGBUG: memory leak
	m_thds.clear();
	m_started = false;
}

class ProgressHandler_EventNotify : public ProgressHandler {
public:
	ProgressHandler_EventNotify(const std::string& taskType, LONG taskId, Dispatcher* dispatcher, IEvent* e) :
			ProgressHandler(taskType, taskId), m_templateEvent(e), m_pDispatcher(dispatcher) {
		ASSERT(e != NULL);
	}

public:
	// overall state handler
	virtual void started() {
		notifyByEvent(TS_START);
	}
	virtual void complete() {
		notifyByEvent(TS_COMPLETE, 100.0f);
	}
	virtual void failed() {
		notifyByEvent(TS_FAILED);
	}
	virtual void canceled() {
		notifyByEvent(TS_CANCELED);
	}

	// pre-processing state handler (optional)
	virtual void prepStarted() {
		notifyByEvent(TS_PREP_START);
	}
	virtual void prepInProgress(float percent, DWORD bytesProcessed, DWORD bytesTotal) {
		notifyByEvent(TS_PREP_PROGRESS, percent, bytesProcessed, bytesTotal);
	}
	virtual void prepCompleted() {
		notifyByEvent(TS_PREP_COMPLETE, 100.0f);
	}
	virtual void prepFailed() {
		notifyByEvent(TS_PREP_FAILED);
	}

	// processing state handler (optional)
	virtual void procStarted() {
		notifyByEvent(TS_PROC_START);
	}
	virtual void procInProgress(float percent, DWORD bytesProcessed, DWORD bytesTotal) {
		notifyByEvent(TS_PROC_PROGRESS, percent, bytesProcessed, bytesTotal);
	}
	virtual void procCompleted() {
		notifyByEvent(TS_PROC_COMPLETE, 100.0f);
	}
	virtual void procFailed() {
		notifyByEvent(TS_PROC_FAILED);
	}

	// post-processing state handler (optional)
	virtual void pstpStarted() {
		notifyByEvent(TS_PSTP_START);
	}
	virtual void pstpInProgress(float percent, DWORD bytesProcessed, DWORD bytesTotal) {
		notifyByEvent(TS_PSTP_PROGRESS, percent, bytesProcessed, bytesTotal);
	}
	virtual void pstpCompleted() {
		notifyByEvent(TS_PSTP_COMPLETE, 100.0f);
	}
	virtual void pstpFailed() {
		notifyByEvent(TS_PSTP_FAILED);
	}

protected:
	virtual void notifyByEvent(TaskState state, float percent = 0.0f, DWORD bytesProcessed = 0, DWORD bytesTotal = 0) {
		jsVerifyReturnVoid(m_pDispatcher != NULL && m_templateEvent != NULL);

		// Create an event with same ID as template
		IEvent* e = m_pDispatcher->MakeEvent(m_templateEvent->EventId());

		// Copy attributes
		e->SetFilter(m_templateEvent->Filter());
		e->SetObjectId(m_templateEvent->ObjectId());

		// Copy from and to
		e->SetFrom(m_templateEvent->From());
		for (NETIDVect::iterator it = m_templateEvent->To().begin(); it != m_templateEvent->To().end(); ++it)
			e->AddTo(*it);

		// Copy all data from template
		e->OutBuffer()->SetFromReadable(m_templateEvent->InBuffer());

		// Append progress information
		(*e->OutBuffer()) << m_taskType << m_taskId << state << percent << bytesProcessed << bytesTotal;
		m_pDispatcher->QueueEvent(e);
	}

private:
	IEvent* m_templateEvent;
	Dispatcher* m_pDispatcher;
	//bool m_bIsTaskProgressEvent;
};

TaskInfo::TaskInfo(const std::string& taskType, LONG taskId, const char* path, const char* url, DWORD bytes /*= 0 */, Dispatcher* disp /*= NULL*/) :
		m_taskType(taskType), m_taskId(taskId), m_totalBytes(bytes), m_returnCode(0), m_pDispatcher(disp) {
	if (url != NULL)
		m_url = url;
	if (path != NULL)
		m_files.push_back(path);
}

TaskInfo::~TaskInfo() {
	for (HandlerVector::iterator itHndlr = m_handlers.begin(); itHndlr != m_handlers.end(); ++itHndlr) {
		HandlerRec& rec = *itHndlr;
		if (rec.m_bDeleteOnCompletion && rec.m_pHandler) {
			delete rec.m_pHandler;
			rec.m_pHandler = NULL;
		}
	}

	m_handlers.clear();
}

void TaskInfo::AddValue(const std::string& key, const std::string& value) {
	m_valuesToPost.push_back(make_pair(key, value));
}

void TaskInfo::ClearValues() {
	m_valuesToPost.clear();
}

void TaskInfo::AddEvent(IEvent* e, DWORD stateMask /*= TS_ALL_EVENTS */) {
	AddHandler(new ProgressHandler_EventNotify(m_taskType, m_taskId, m_pDispatcher, e), stateMask, true);
}

void TaskInfo::AddHandler(ProgressHandler* h, DWORD stateMask /*= TS_ALL_EVENTS*/, bool bDeleteOnCompletion /*= true */) {
	m_handlers.push_back(HandlerRec(h, stateMask, bDeleteOnCompletion));
}

void InternetManagerThread::UpdateProgress(TaskInfo* info, TaskState state, float percent /*= 0.0f*/, DWORD bytesProcessed /*= 0*/, DWORD bytesTotal /*= 0 */) {
	std::lock_guard<fast_recursive_mutex> lock(m_taskQueueSync);

	// Call progress handlers
	for (HandlerVector::iterator it = info->m_handlers.begin(); it != info->m_handlers.end(); it++) {
		if (it->m_pHandler != NULL && (it->m_dwStateMask & state) != 0) {
			switch (state) {
				case TS_START:
					it->m_pHandler->started();
					break;
				case TS_COMPLETE:
					it->m_pHandler->complete();
					break;
				case TS_FAILED:
					it->m_pHandler->failed();
					break;
				case TS_CANCELED:
					it->m_pHandler->canceled();
					break;

				case TS_PREP_START:
					it->m_pHandler->prepStarted();
					break;
				case TS_PREP_PROGRESS:
					it->m_pHandler->prepInProgress(percent, bytesProcessed, bytesTotal);
					break;
				case TS_PREP_COMPLETE:
					it->m_pHandler->prepCompleted();
					break;
				case TS_PREP_FAILED:
					it->m_pHandler->prepFailed();
					break;

				case TS_PROC_START:
					it->m_pHandler->procStarted();
					break;
				case TS_PROC_PROGRESS:
					it->m_pHandler->procInProgress(percent, bytesProcessed, bytesTotal);
					break;
				case TS_PROC_COMPLETE:
					it->m_pHandler->procCompleted();
					break;
				case TS_PROC_FAILED:
					it->m_pHandler->procFailed();
					break;

				case TS_PSTP_START:
					it->m_pHandler->pstpStarted();
					break;
				case TS_PSTP_PROGRESS:
					it->m_pHandler->pstpInProgress(percent, bytesProcessed, bytesTotal);
					break;
				case TS_PSTP_COMPLETE:
					it->m_pHandler->pstpCompleted();
					break;
				case TS_PSTP_FAILED:
					it->m_pHandler->pstpFailed();
					break;
			}
		}
	}
}

} // namespace KEP
