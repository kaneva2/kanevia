///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IResource.h"
#include "ResourceType.h"
#include <memory>

namespace KEP {
class ReMesh;
class MeshRM;
template <typename T>
class TypedResourceManager;

/*!
* MeshProxy is used to represent a mesh resource that's downloaded and/or saved
* on the local HD.
*/
#ifdef MESH_PROXY_SERIALIZE
class MeshProxy : public CObject, public IResource
#else
class MeshProxy : public IResource
#endif
{
public:
	// Settings for TypedResourceManager
	const static ResourceType _ResType = ResourceType::MESH;

protected:
	// define default parameter to satisfy CObject serialization
	MeshProxy(ResourceManager* library, UINT64 meshHash = 0); // protected constructor called by correspoding RM class only

public:
#ifdef MESH_PROXY_SERIALIZE
	DECLARE_SERIAL(MeshProxy);
#endif
	/*!
	* \brief
	* Construct a CMesh
	*
	* \param meshHash
	* A hash that uniquely identifies the mesh
	*
	* Write detailed description for ReMeshArchive here.
	*
	* \see
	* CTextureEX
	*/
	MeshProxy(const MeshProxy& mesh);

	virtual ~MeshProxy() {}

	virtual std::string ToStr() const override {
		std::string str;
		StrBuild(str, "MP<" << this << ">");
		return str;
	}

	virtual bool CreateResource() override;

	/*!
	* Build a ReMesh from the downloaded data.
	*
	* Call this instead of GetData.  This method calls GetData
	* and casts the result to ReMesh.  I could use
	* covariant return types on GetData(), which would be
	* neater if the compiler supports it.
	*
	* \return A new ReMesh object (managed by the caller?)
	*/
	//ReMesh* GetReMesh();
	std::unique_ptr<ReMesh> GetReMeshClone();
	bool LoadMesh();

protected:
#ifdef MESH_PROXY_SERIALIZE
	/*!
	* Serialize the mesh data.
	*
	* Store uses CArchive to serialize the internal ReMesh object data.
	* Used to read and write a MeshProxy to disk.
	*
	* TODO: The actual implementation should be in a persistance layer
	* class that doesn't cause issues when we try to re-factor.
	*/
	void Serialize(CArchive& ar);
#endif

	bool IsSerializedDataCompressible() const {
		return false; // No UGC for this resource so far, kaneva assets are always compressed by Editor
	}

private:
	ReMesh* m_pMesh;

	friend class TypedResourceManager<MeshProxy>;
};

} // namespace KEP
