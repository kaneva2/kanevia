///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SkeletonProxy.h"
#include "SkeletonManager.h"
#include "RuntimeSkeleton.h"

namespace KEP {

SkeletonProxy::SkeletonProxy(ResourceManager* library, UINT64 resKey) :
		IResource(library, resKey), m_skeleton(NULL) {
	SetState(IResource::State::INITIALIZED_NOT_LOADED);
}

SkeletonProxy::SkeletonProxy(const SkeletonProxy& skeleton) :
		IResource(skeleton) {
	if (skeleton.m_skeleton)
		skeleton.m_skeleton->Clone(&m_skeleton, NULL, NULL);
	else
		m_skeleton = NULL;
}

// Return the m_skeleton using GetRuntimeSkeleton(), which will cause it to be (down-)loaded, if necessary.
RuntimeSkeleton* SkeletonProxy::GetRuntimeSkeleton() {
	auto pRM = GetResourceManager();
	if (!pRM)
		return NULL;

	if (StateIsLoadedAndCreated())
		return m_skeleton.get();

	if (StateIsInitializedNotLoaded())
		pRM->QueueResource(this);

	return NULL;
}

bool SkeletonProxy::CreateResource() {
	ASSERT(false);
	return false;
}

} // namespace KEP
