///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "MeshProxy.h"
#include "ReMeshCreate.h"
#include "ReMesh.h"
#include "ReSkinMesh.h"
#include "ReMaterial.h"
#include "ReMeshStream.h"
#include "IClientEngine.h"

namespace KEP {

MeshProxy::MeshProxy(ResourceManager* library, UINT64 meshHash) :
		IResource(library, meshHash), m_pMesh(NULL) {
	SetState(IResource::State::INITIALIZED_NOT_LOADED);
}

MeshProxy::MeshProxy(const MeshProxy& mesh) :
		IResource(mesh) {
	// Use the yucky Clone symantics used in Render Engine.
	if (mesh.m_pMesh)
		mesh.m_pMesh->Clone(&m_pMesh);
	else
		m_pMesh = NULL;
}

#ifdef MESH_PROXY_SERIALIZE
void MeshProxy::Serialize(CArchive& ar) {
	CObject::Serialize(ar);

	if (ar.IsStoring()) {
		// Writing.
		ar.SetObjectSchema(1); // replace with a constant or #define
		ASSERT(GetRawSize() != 0 && GetRawBuffer() != NULL);
		ar << (UINT)GetRawSize();
		ar.Write(GetRawBuffer(), (UINT)GetRawSize());
	} else {
		ar.GetObjectSchema();
		size_t fileSize;
		ar >> fileSize;
		ASSERT(GetRawSize() == fileSize && GetRawSize() != 0 && GetRawBuffer() != nullptr);
		ar.Read(GetRawBuffer(), (UINT)GetRawSize());
	}
}
#endif

std::unique_ptr<ReMesh> MeshProxy::GetReMeshClone() {
	std::unique_ptr<ReMesh> pClonedMesh;
	if (LoadMesh()) {
		ReMesh* pClonedMeshRaw;
		m_pMesh->Clone(&pClonedMeshRaw);
		pClonedMesh.reset(pClonedMeshRaw);
	}

	return pClonedMesh;
}

bool MeshProxy::LoadMesh() {
	if (StateIsLoadedAndCreated())
		return true;

	if (StateIsInitializedNotLoaded()) {
		auto pRM = dynamic_cast<MeshRM*>(GetResourceManager());
		if (!pRM)
			return nullptr;

		// Check in memory incase the mesh was loaded from EDB.
		char* tmp = NULL;
		std::string name = GetFileName();
		UINT64 hash = _strtoui64(name.c_str(), &tmp, 10);
		ReMesh* rm = pRM->FindMesh(hash, MeshRM::RMP_ID_PERSISTENT);
		if (rm) {
			SetState(State::LOADED_AND_CREATED);
			m_pMesh = rm;
			return true;
		}

		pRM->QueueResource(this);
	}

	return false;
}

bool MeshProxy::CreateResource() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	CMemFile file;
	file.Attach(GetRawBuffer(), (UINT)GetRawSize(), 0);
	CArchive meshArchive(&file, CArchive::load);
	m_pMesh = NULL;

	// read the number of meshes
	UINT numMeshes;
	meshArchive >> numMeshes;

	// There can be only one!
	ASSERT(numMeshes == 1);
	if (numMeshes == 1) {
		// stream the ReMesh
		ReMeshStream streamer;

		// How do I know if I'm dealing with skin meshes or
		// not?  The read (and write) functions on the mesh
		// serializer are overloaded and do different things,
		// so this is something I need to handle somewhere.
		ReSkinMesh* newMesh = nullptr;
		streamer.ReadSkin(&newMesh, meshArchive, pICE->GetReDeviceState());
		m_pMesh = newMesh;
	}
	file.Detach();
	FreeRawBuffer();

	if (m_pMesh)
		SetState(LOADED_AND_CREATED);

	//	Remove this resource from the list of loading files
	auto pRM = dynamic_cast<MeshRM*>(GetResourceManager());
	assert(pRM != nullptr);
	if (pRM) {
		assert(m_pMesh != nullptr);
		if (m_pMesh != nullptr) {
			// Add ReMesh to persistent pool for lookup by hash
			pRM->AddMesh(m_pMesh, MeshRM::RMP_ID_PERSISTENT);
		}
	}

	//
	// If the load failed, I need to try to re-download it (it might
	// have been corrupted).  There currently isn't a good way to do
	// that.  I could kick it off here, if I make DeleteLocalResource
	// public.  Normally, it's done when 'validate' fails in the
	// resource manager, but that doesn't work for reMesh right now.
	// Maybe I can integrate it that way instead?
	//
	if (m_pMesh == NULL) {
		meshArchive.Abort();
		return FALSE;
	}

	return TRUE;
}

#ifdef MESH_PROXY_SERIALIZE
IMPLEMENT_SERIAL(MeshProxy, CObject, 0)
#endif

} // namespace KEP
