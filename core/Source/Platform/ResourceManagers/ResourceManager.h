///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IResource.h"
#include "Event/Base/IEventHandler.h"
#include <jsThread.h>
#include <mutex>
#include <list>
#include "Core/Util/fast_mutex.h"
#include "Core/Util/WindowsAPIWrappers.h"

namespace KEP {
class IResource;
enum {
	RES_PRIO_MY_DELTA = 300, // Amount that current player's resources get boosted over others
};

struct fileInfo {
	std::string fileName;
	std::string filePath;
	std::string fileHash; // md5 string (32 chars)
	FileSize fileSize;
};

class ResourceManager {
public:
	std::string m_id; // drf - resource manager identifier

	ResourceManager(const std::string& id, ResourceType resourceType);
	virtual ~ResourceManager();

	size_t GetResourceLoadOrder() const { return m_iResourceLoadOrder; }

	virtual BOOL QueueResource(IResource* pRes, GLID itemGLID = GLID_INVALID);
	virtual void cancelAllPendingTasks();

	virtual UINT GetResourceCount();
	virtual void GetAllResourceKeys(std::vector<UINT64>& keys);
	virtual IResource* GetResource(UINT64 hash) {
		return FindResource(hash);
	}

	void RemoveResource(UINT64 hash);
	void RemoveAllResources();
	void RemoveResourcesByState(IResource::State state);

	// ED-8437 - Clear Errored Resources On Rezone
	void ClearErroredResources();

	/*!
	* \brief
	* Load or cause to be downloaded the data associated with a resource object.
	*
	* \param pRes
	* The resource that is initialized but not created
	*
	* \param bImmediate
	* use bImmediate to load the file blocking (right away)
	*
	* \param attemptAsyncDownload
	* If true, then the server will be asked for the file, even if bImmediate == true.
	*
	* \returns
	* Returns false if the file was not found
	*/
	virtual bool QueueFileForRead(IResource* pRes, bool bImmediate, bool attemptAsyncDownload) final;

	static bool VerifyFileIntegrity(IResource* pRes);

	//! \brief Allocate a local GLID for a newly imported item
	static int AllocateLocalGLID();

	ResourceType GetResourceType() const {
		return m_supportedResourceType;
	}

	static std::string GetPatchAssetLocation();

	virtual void setQueueEnabled(bool bEnabled) {
		m_queueEnabled = bEnabled;
	}
	bool IsQueueEnabled() const {
		return m_queueEnabled;
	}

	//! \brief Return true if the specified resource is UGC
	virtual bool IsUGCItem(UINT64 hash) const = 0;

	bool RegisterResourceLoadedCallback(uint64_t hash, const std::shared_ptr<void>& pCallbackOwner, std::function<void()> callback);
	size_t UnregisterResourceLoadedCallback(void* pCallbackOwner, uint64_t hash);
	static size_t UnregisterAllResourceLoadedCallbacks(void* pCallbackOwner); // Unregisters for systems other than ResourceManager

	// Transform unique ID to allow it to coexist with GLID for now
	static const UINT64 UNIQUE_ID_TO_RESOURCE_KEY_BASE = 5000000000ull;
	static const UINT64 UNIQUE_ID_TO_RESOURCE_KEY_MAX = 5999999999ull;
	static UINT64 GetResourceKeyByUniqueId(unsigned uniqueId) {
		return (UINT64)uniqueId + UNIQUE_ID_TO_RESOURCE_KEY_BASE;
	}
	static bool IsResourceKeyFromUniqueId(UINT64 resKey) {
		return resKey >= UNIQUE_ID_TO_RESOURCE_KEY_BASE && resKey <= UNIQUE_ID_TO_RESOURCE_KEY_MAX;
	}

	void SuspendLoad(bool bSuspend);
	bool IsLoadSuspended() const {
		return m_iSuspendCount.load(std::memory_order_acquire) > 0;
	}

	//! \brief Compose a file name for serialization based on hash code provided.
	//! \param hash Hash code for the resource to be serialized.
	virtual std::string GetResourceFileName(UINT64 hash) = 0;
	virtual std::string GetResourceFilePath(UINT64 hash) = 0;

	//! \brief Compose a download URL
	//! \param hash Hash code for the resource to be serialized.
	virtual std::string GetResourceURL(UINT64 hash) = 0;

	virtual void LogResources(bool verbose = false) const;

	static void ResourceReady(IResource* pResource);

protected:
	/*!
	* \brief
	* Find a resource by its hash value.
	*
	* \param hash
	* The resource's identifying hash.
	*
	* \returns
	* A resource, which should not be deleted by the caller.
	*
	* The function uses the hash to locate a resource, which
	* is then returned by pointer, but continues to be
	* managed by ResourceManager.  This is where smart pointers
	* might be more useful...
	*/
	IResource* FindResource(UINT64 hash);

	//! \brief Return a path pointing to where resource should be stored locally
	//! \param gameFilesDir a path pointed to "GameFiles" folder of current game
	//@@Assumption: caller must pass in a path pointed to "GameFiles" folder of current game.
	virtual std::string GetResourceLocalStore(const std::string& gameFilesDir) = 0;

	void loadLooseFileInfo(const std::string& infoFile);

	IResource* Add(std::shared_ptr<IResource> pNewResource);

	fileInfo* FindFileInfo(const std::string& name) {
		auto itr = m_looseFileInfo.find(name);
		if (itr != m_looseFileInfo.end())
			return &itr->second;
		return nullptr;
	}

private:
	static bool DeleteResourceFile(IResource* pRes);

private:
	// Thread-safe map for holding resources. Nothing resource-specific so probably a good
	// idea to templatize this to use it elsewhere.
	class ManagedResourceMap {
	public:
		template <typename Callable> // void callable(iResource* pResource)
		void ForEach(Callable&& callable) const {
			std::lock_guard<fast_mutex> lock(m_mtxResourceMap);
			for (const auto& pr : m_mapResources)
				callable(pr.second.get());
		}
		template <typename Callable> // void callable(uint64_t iHash)
		void ForEachKey(Callable&& callable) const {
			std::lock_guard<fast_mutex> lock(m_mtxResourceMap);
			for (const auto& pr : m_mapResources)
				callable(pr.first);
		}
		size_t Size() const {
			std::lock_guard<fast_mutex> lock(m_mtxResourceMap);
			return m_mapResources.size();
		}
		std::shared_ptr<IResource> LookUp(uint64_t iHash) {
			std::lock_guard<fast_mutex> lock(m_mtxResourceMap);
			auto itr = m_mapResources.find(iHash);
			if (itr != m_mapResources.end())
				return itr->second;
			return nullptr;
		}
		IResource* Add(std::shared_ptr<IResource> p) {
			if (!p)
				return nullptr;
			return m_mapResources.emplace(std::piecewise_construct, std::forward_as_tuple(p->GetHashKey()), std::forward_as_tuple(std::move(p))).first->second.get();
		}
		bool Remove(uint64_t key) {
			std::lock_guard<fast_mutex> lock(m_mtxResourceMap);
			return m_mapResources.erase(key) == 1;
		}
		void RemoveAll() {
			std::lock_guard<fast_mutex> lock(m_mtxResourceMap);
			m_mapResources.clear();
		}

	private:
		mutable fast_mutex m_mtxResourceMap;
		std::map<uint64_t, std::shared_ptr<IResource>> m_mapResources;
	};

	ManagedResourceMap m_mapManaged;

	ResourceType m_supportedResourceType;

	bool m_queueEnabled;

	std::atomic<size_t> m_iSuspendCount = 0; // When >0 loading will be suspended.

	size_t m_iResourceLoadOrder = SIZE_MAX; // Determines the order that different resource types are loaded.

	std::map<std::string, fileInfo> m_looseFileInfo;

	static std::atomic<int> s_NextLocalGLID; /// next available local GLID (always negative)
};

template <typename T>
class TypedResourceManager : public ResourceManager {
public:
	TypedResourceManager(const std::string& id) :
			ResourceManager(id, T::_ResType) {
		assert(ms_pInstance == nullptr);
		ms_pInstance = this;
	}

	~TypedResourceManager() {
		assert(ms_pInstance == this);
		ms_pInstance = nullptr;
	}

	static TypedResourceManager<T>* Instance() {
		return ms_pInstance;
	}

	/*!
	* \brief
	* Add a resource to the manager.
	*
	* \param args
	* The arguments to be passed to the resource constructor
	*
	* The resource passed in is cloned and the clone is
	* added to the resource manager's database, not the
	* argument.  The resource passed to this function can
	* be deleted after the call to AddResources.
	*/
	template <typename... Args>
	T* AddResource(UINT64 resHash, Args... args) {
		T* pExistingResource = static_cast<T*>(FindResource(resHash));
		if (pExistingResource)
			return pExistingResource;

		// Add New Resource
		std::shared_ptr<T> pResource(new T(this, resHash, std::forward<Args>(args)...));
		if (!pResource->IsLocalResource()) {
			// If streaming from server, set expected size and hash for verification
			fileInfo* pFileInfo = FindFileInfo(pResource->GetFileName());
			if (pFileInfo) {
				pResource->SetExpectedSize(pFileInfo->fileSize);
				pResource->SetExpectedHash(pFileInfo->fileHash);
			}
		}
		return static_cast<T*>(ResourceManager::Add(std::move(pResource)));
	}

private:
	static TypedResourceManager<T>* ms_pInstance;
};

template <typename T, typename... Args>
T* AddResourceToManager(UINT64 resKey, Args... args);

} // namespace KEP