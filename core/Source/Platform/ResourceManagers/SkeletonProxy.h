///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IResource.h"
#include "ResourceType.h"
#include <memory>

namespace KEP {

class SkeletonRM;
class RuntimeSkeleton;
template <typename T>
class TypedResourceManager;

/*!
* SkeletonProxy is used to represent a mesh resource that's downloaded and/or saved
* on the local HD.
*/
class SkeletonProxy : public IResource {
public:
	// Settings for TypedResourceManager
	const static ResourceType _ResType = ResourceType::SKELETON;

protected:
	//
	// Default guid is used for dynamic objects that don't
	// have any skinned meshes or animations.
	//
	SkeletonProxy(ResourceManager* library, UINT64 resKey); // protected constructor called by manager class.

public:
	SkeletonProxy(const SkeletonProxy& mesh);

	virtual ~SkeletonProxy() {}

	virtual std::string ToStr() const override {
		std::string str;
		StrBuild(str, "SP<" << this << ">");
		return str;
	}

	virtual bool CreateResource() override;

	/*!
	* \brief
	* Build a RuntimeSkeleton from the downloaded data.
	*
	* Call this instead of GetData.  This method calls GetData
	* and casts the result to RuntimeSkeleton.
	*
	* \return A RuntimeSkeleton object.
	*/
	virtual RuntimeSkeleton* GetRuntimeSkeleton();

protected:
	// Derived from IResource
	bool IsSerializedDataCompressible() const {
		return false; // dont care, loose data files not used for skeleton so far
	}

private:
	std::shared_ptr<RuntimeSkeleton> m_skeleton;

	friend class SkeletonRM;
	friend class TypedResourceManager<SkeletonProxy>;
};

} // namespace KEP
