///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ResourceManager.h"
#include <mutex>

namespace KEP {

class CGlobalInventoryObjList;
class StreamableDynamicObject;
class DynamicObjectResource;

/*!
 * \brief
 * This class represents a dynamic object library.
 *
 * This class encapsulates and exposes dynamic object funcationality to the client and
 * editor.  It handles serialization and conversion of the legacy system.  It is also
 * designed to be modified to add dynamic objects on the fly for the next phase of this
 * project.
 */
class DynamicObjectRM : public TypedResourceManager<DynamicObjectResource> {
public:
	DynamicObjectRM(const std::string& id);
	~DynamicObjectRM();

	// textureinfo.dat - TODO: move to ResourceManager class
	void loadLooseFileInfo();

protected:
	// Resource Manager stuff
	virtual std::string GetResourceLocalStore(const std::string& gameFilesDir) override;
	virtual std::string GetResourceFileName(UINT64 hash) override;
	virtual std::string GetResourceFilePath(UINT64 hash) override;
	virtual std::string GetResourceURL(UINT64 hash) override;

	bool IsUGCItem(UINT64 hash) const override {
		return IsResourceKeyFromUniqueId(hash) || IS_UGC_GLID((GLID)hash);
	}
};

} // namespace KEP
