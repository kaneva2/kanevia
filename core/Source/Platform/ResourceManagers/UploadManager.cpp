///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UploadManager.h"

#include <wininet.h>
#include <algorithm>
#include <sys/stat.h>
#include "KEPHelpers.h"
#include "IClientEngine.h"
#include "Event/EventSystem/Dispatcher.h"

#include "Client/ClientEngine_HttpClient.h"

#include "common\KEPUtil\Helpers.h"
#include "UGC\UGCImport.h"

#include "jsEnumFiles.h"

#include "LogHelper.h"
static LogInstance("Instance");

#define UPLOAD_CHUNK_SIZE 256 * 1024 // 256KB

namespace KEP {

UploadManager* UploadManager::pInst = NULL;

class UploadTaskInfo : public TaskInfo {
public:
	UploadTaskInfo(LONG taskId,
		const char* path,
		const char* url,
		Dispatcher* disp = NULL,
		char* buffer = NULL,
		DWORD bufSize = 0,
		bool authFirst = false,
		const char* authUrl = NULL) :
			TaskInfo("UPLOAD", taskId, path, url, bufSize, disp),
			m_authFirst(authFirst), m_fileBuf(buffer) {
		if (authUrl)
			m_authUrl = authUrl;

		m_fileBuf = buffer; // If m_fileBuf is not null, data will come from or go to a memory buffer other than a file
	};

	// Instead of specified by m_path, data to upload can be passed by memory pointer
	// Once m_fileBuf is specified, m_path will serve as an identification other than file path
	char* m_fileBuf;
	// m_path now means a name if m_fileBuf!=NULL

	std::string m_authUrl;
	bool m_authFirst;
};

class UploadThread : public InternetManagerThread {
public:
	UploadThread(UploadManager& parent, Dispatcher& dispatcher, TaskInfoQueue& queue, fast_recursive_mutex& sync, jsThdPriority prio = PR_NORMAL, jsThreadType ttype = TT_NORMAL) :
			InternetManagerThread(parent, dispatcher, queue, sync, "UploadManager", prio, ttype) {
	}

protected:
	ULONG GetBufferSize() override {
		return UploadManager::BUFF_SIZE;
	}

	// Pre-streaming analysis: queue locked before this function
	bool PreProcess(TaskInfo* info) override;

	// Actual streaming process: queue unlocked before entering
	bool ProcessTask(TaskInfo* info) override;

	// Post-streaming analysis: queue locked before this function
	void PostProcess(TaskInfo* /*info*/) override {}

	// Post-streaming analysis: queue locked before this function
	void PostProcessWhenFailed(TaskInfo* /*info*/) override {}

protected:
	friend class UploadManager;
};

bool UploadThread::PreProcess(TaskInfo* info) {
	if (!info)
		return false;

	UpdateProgress(info, TS_PREP_START);

	UploadTaskInfo* pULInfo = static_cast<UploadTaskInfo*>(info);
	if (!pULInfo) {
		UpdateProgress(info, TS_PREP_FAILED);
		return false;
	}

	// compute upload size if data is from a file
	if (pULInfo->m_fileBuf == NULL && !pULInfo->m_files.empty() && pULInfo->m_totalBytes == 0) {
		for (auto it = pULInfo->m_files.begin(); it != pULInfo->m_files.end(); ++it) {
			if (jsFileExists(it->c_str(), false)) {
				CFileStatus status;
				if (CFile::GetStatus(Utf8ToUtf16(*it).c_str(), status)) {
					if (status.m_size <= MAX_UPLOAD_SIZE) {
						UpdateProgress(info, TS_PREP_COMPLETE);
						pULInfo->m_totalBytes += (DWORD)status.m_size;
						return true;
					} else {
						LogWarn("file size is too big - size=" << status.m_size);
					}
				} else {
					LogWarn("file does not exist - '" << it->c_str() << "'");
				}
			} else {
				LogWarn("file does not exist - '" << it->c_str() << "'");
			}
		}
	} else {
		return true;
	}

	UpdateProgress(info, TS_PREP_FAILED);
	return false;
}

bool UploadThread::ProcessTask(TaskInfo* info) {
	if (!info)
		return false;

	UpdateProgress(info, TS_PROC_START);

	bool authFirst = false;
	std::string authUrl;

	UploadTaskInfo* pULInfo = static_cast<UploadTaskInfo*>(info);
	if (!pULInfo) {
		UpdateProgress(info, TS_PROC_FAILED);
		return false;
	}

	authFirst = pULInfo->m_authFirst;
	authUrl = pULInfo->m_authUrl;

	int retVal = -1;

	Ryeol::CHttpClientA objHttpReq;
	std::string tempFileName;

	objHttpReq.SetInternet("Kaneva Client Engine");

	bool bAuthOK = true;
	if (authFirst) {
		Ryeol::CHttpResponseA* authResp = NULL;

		try {
			authResp = objHttpReq.RequestGet(authUrl.c_str());
			bAuthOK = authResp->GetStatus() == HTTP_STATUS_OK;
		} catch (Ryeol::httpclientexceptionA& e) {
			LogWarn("requestGet validation failed:" << pULInfo->m_url << ", exception: " << e.errmsg());
			bAuthOK = false;
		}

		if (authResp)
			delete authResp;
	}

	if (bAuthOK) {
		// Post other values along with upload file as we can't upload to a URL with param postfix (somehow weird HTTP 500 error returned)
		for (auto it = pULInfo->m_valuesToPost.begin(); it != pULInfo->m_valuesToPost.end(); ++it)
			objHttpReq.AddParam(it->first.c_str(), it->second.c_str());

		if (pULInfo->m_fileBuf) {
			// Write Data To File '<pathApp>\MapsModels\uploaded\<hashName>.dat'
			std::string hashName = GetDataHash(pULInfo->m_fileBuf, pULInfo->m_totalBytes);
			std::string filePath = PathAdd(CreatePathMapsModelsUploaded(), hashName + ".dat");

			// Only If Doesn't Already Exist
			if (!FileHelper::Exists(filePath)) {
				FileHelper fh(filePath, GENERIC_WRITE, CREATE_ALWAYS);
				if (fh.IsOpen()) {
					DWORD tx = fh.Tx(pULInfo->m_fileBuf, pULInfo->m_totalBytes);
					fh.Close();
					if (tx != pULInfo->m_totalBytes) {
						LogError("FileHelper::Tx() FAILED - '" << filePath << "'");
						FileHelper::Delete(filePath);
					}
				} else {
					LogError("FileHelper::IsOpen() FAILED - '" << filePath << "'");
				}
			}

			// Upload File
			if (FileHelper::Exists(filePath)) {
				objHttpReq.AddParam("upload", filePath.c_str(), Ryeol::CHttpClientA::ParamFile);
			} else {
				LogError("FileHelper::Exists() FAILED - '" << filePath << "'");
			}
		} else {
			for (const auto& filePath : pULInfo->m_files)
				objHttpReq.AddParam("upload", filePath.c_str(), Ryeol::CHttpClientA::ParamFile);
		}

		Ryeol::CHttpResponseA* resp = NULL;
		Ryeol::CHttpPostStatA stat;

		try {
			objHttpReq.BeginUploadEx(pULInfo->m_url.c_str());

			float prevPercent = -1;
			while (!resp) {
				resp = objHttpReq.Proceed(UPLOAD_CHUNK_SIZE);
				objHttpReq.Query(stat);

				if (stat.IsActive()) {
					DWORD postedBytes = stat.PostedByte();
					DWORD totalBytes = stat.TotalByte();
					float percent = floor(postedBytes * 100.0f / totalBytes + 0.5f);
					if (percent != prevPercent) {
						UpdateProgress(info, TS_PROC_PROGRESS, percent, postedBytes, totalBytes);
						prevPercent = percent;
					}
				}

				Sleep(0);
			}
		} catch (Ryeol::httpclientexceptionA& e) {
			LogError("upload failed:" << pULInfo->m_url << ", exception: " << e.errmsg());
		}

		if (resp) {
			retVal = resp->GetStatus() == 200 ? 0 : -1;
			delete resp;
		}

		// Delete temporary file if exists
		if (!tempFileName.empty())
			_unlink(tempFileName.c_str());
	}

	// Notify upload listeners of task completion
	if (retVal != 0) {
		//	If retval isnt 0, upload failed
		pULInfo->m_returnCode = retVal;
		UpdateProgress(info, TS_PROC_FAILED);
	} else {
		//	File upload successful
		UpdateProgress(info, TS_PROC_COMPLETE, 100.0f, info->m_totalBytes, info->m_totalBytes);
	}

	return retVal == NO_ERROR;
}

UploadManager::UploadManager(Dispatcher& disp) :
		InternetManager("UploadManager", disp, UM_THREADS, UM_THREAD_JS_PRIORITY) {
	pInst = this;
	StartThreads();
}

bool UploadManager::UploadFileAsync(const char* uploadFile, const char* url, TaskPriority prio, ValuesList* pAddlValues /*= NULL*/, bool authFirst /*= false*/, const char* authUrl /*= NULL*/, IEvent* e /*= NULL*/, DWORD eventMask /*= 0*/) {
	UploadTaskInfo* info = new UploadTaskInfo(getNextTaskId(), uploadFile, url, &m_dispatcher, NULL, 0, authFirst, authUrl);
	if (pAddlValues)
		info->m_valuesToPost = *pAddlValues;
	info->AddEvent(e, eventMask);

	bool res = QueueTask(info, prio);
	if (!res)
		delete info;

	return res;
}

bool UploadManager::UploadFileAsync(const char* uploadName, char* dataBuf, DWORD dataBufLen, const char* url, TaskPriority prio, ValuesList* pAddlValues /*= NULL*/, bool authFirst /*= false*/, const char* authUrl /*= NULL*/, IEvent* e /*= NULL*/, DWORD eventMask /*= 0*/) {
	UploadTaskInfo* info = new UploadTaskInfo(getNextTaskId(), uploadName, url, &m_dispatcher, dataBuf, dataBufLen, authFirst, authUrl);
	if (pAddlValues)
		info->m_valuesToPost = *pAddlValues;
	info->AddEvent(e, eventMask);

	bool res = QueueTask(info, prio);
	if (!res)
		delete info;

	return res;
}

bool UploadManager::UploadMultiFilesAsync(const std::vector<std::string>& uploadFiles, const char* url, TaskPriority prio, ValuesList* pAddlValues /*= NULL*/, bool authFirst /*= false*/, const char* authUrl /*= NULL*/, IEvent* e /*= NULL*/, DWORD eventMask /*= 0 */) {
	UploadTaskInfo* info = new UploadTaskInfo(getNextTaskId(), NULL, url, &m_dispatcher, NULL, 0, authFirst, authUrl);
	for (auto it = uploadFiles.begin(); it != uploadFiles.end(); ++it)
		info->m_files.push_back(*it);

	if (pAddlValues)
		info->m_valuesToPost = *pAddlValues;
	info->AddEvent(e, eventMask);

	bool res = QueueTask(info, prio);
	if (!res)
		delete info;

	return res;
}

InternetManagerThread* UploadManager::createWorkerThread() {
	return new UploadThread(*this, m_dispatcher, m_queue, m_sync, m_threadPriority);
}

UploadManager* UploadManager::Ptr() {
	if (!pInst) {
		LogInstance("Instance");
		LogError("null Ptr");
	}
	return pInst;
}

} // namespace KEP
