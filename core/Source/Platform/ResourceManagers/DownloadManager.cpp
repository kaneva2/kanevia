///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "DownloadManager.h"
#include "ResourceManager.h"
#include "IClientEngine.h"
#include "Event/Base/IEvent.h"
#include "Event/Base/IDispatcher.h"
#include "Common\KEPUtil\WebCall.h"
#include "Common\KepUtil\kpack.h"
#include "Common\include\CompressHelper.h"

static LogInstance("DM");

namespace KEP {

DownloadManager* DownloadManager::s_pDM = NULL;
IClientEngine* DownloadManager::s_pCE = NULL;

// Download File Return Codes
enum {
	DOWNLOAD_FILE_CORRUPT = -1,
	DOWNLOAD_FILE_OK = 0
	// ... WebCaller::StatusCodes
};

bool DownloadInfo::FileOk(bool deleteIfBad) {
	if (!FileHelper::Exists(m_path))
		return false;
	if (m_expectedFileSize == 0)
		return true;
	auto fileSize = FileHelper::Size(m_path);
	if (fileSize != m_expectedFileSize) {
		LogError("UNEXPECTED FILE SIZE (" << fileSize << " != " << m_expectedFileSize << ") '" << m_path << "' " << (deleteIfBad ? " Deleting..." : ""));
		if (deleteIfBad)
			FileHelper::Delete(m_path);
		return false;
	}
	return true;
}

bool DownloadInfo::UrlOk() {
	if (m_url.empty())
		return false;
	if (!STLBeginWithIgnoreCase(m_url, "http")) {
		LogError("BAD URL '" << m_url << "'");
		return false;
	}
	return true;
}

void DownloadInfo::FireProgressCompleteEvent(int retVal, fast_recursive_mutex* pMutex) {
	if (!m_dispatcher || !pMutex)
		return;

	if (m_events.empty())
		return;

	// For Each Registered Event Handler
	std::lock_guard<fast_recursive_mutex> lock(*pMutex);
	for (const auto& pEvent : m_events) {
		if (!pEvent)
			continue;

		// Broadcast Progress Complete Event
		bool ok = (retVal == 0);
		pEvent->SetFilter(ok ? 200 : 1); // 200=ok, 1=error
		(*pEvent->OutBuffer()) << retVal << m_path.c_str();
		m_dispatcher->QueueEvent(pEvent.get());
	}
	m_events.clear();
}

class DownloadManagerThread : public jsThread {
public:
	static const ULONG PROGRESS_INTERVAL = 1000;

	static bool UncompressAndUnpackFile(const std::string& filePath);

	DownloadManagerThread(DownloadManager& mgr, IDispatcher& disp, jsThdPriority prio) :
			jsThread("DownloadManager", prio),
			m_dispatcher(disp),
			m_parent(mgr),
			m_pDI_current(nullptr) {}

	virtual ~DownloadManagerThread() {}

	static int DownloadFile(DownloadInfo* pDI, IDispatcher& dispatcher, fast_recursive_mutex& mutexRef);

	bool DownloadFile(DownloadInfo* pDI) {
		return (DownloadFile(pDI, m_dispatcher, m_parent.m_sync) == DOWNLOAD_FILE_OK);
	}

	DownloadInfo* CurrentDownloadInfo() {
		return m_pDI_current;
	}

protected:
	virtual ULONG _doWork();

private:
	DownloadManager& m_parent;
	IDispatcher& m_dispatcher;
	DownloadInfo* m_pDI_current;
};

int DownloadManagerThread::DownloadFile(DownloadInfo* pDI, IDispatcher& dispatcher, fast_recursive_mutex& mutexRef) {
	// Download To File ?
	std::string downloadFilePath = pDI->getPath();
	bool downloadFile = !downloadFilePath.empty();

	// File Already Downloaded Ok ? (delete if bad)
	if (downloadFile && pDI->FileOk(true)) {
		pDI->FireProgressCompleteEvent(DOWNLOAD_FILE_OK, &mutexRef);
		return DOWNLOAD_FILE_OK;
	}

	// Compressed Files Append Extension (same as url)
	// DRF - BUG? - What to do if flagged as compressed but url isn't?
	bool downloadFileCompressed = (downloadFile && pDI->isCompressed());
	if (downloadFileCompressed) {
		auto compressType = ::CompressType(pDI->getUrl());
		if (compressType == COMPRESS_TYPE_NONE)
			compressType = COMPRESS_TYPE_DEFAULT;
		downloadFilePath = ::CompressTypeExtAdd(downloadFilePath, compressType);
	}

	// Synchronous Webcall
	WebCallOpt wco;
	wco.url = pDI->getUrl();
	wco.timeoutMs = DM_TIMEOUT_MS;
	wco.response = true;
	wco.responseFilePath = downloadFilePath;
	//	wco.cbFunc = DownloadAsyncFileCallback;
	//	wco.pAddData = new FileContext(pPP, fileEntry, filePathTo);
	auto wca = WebCaller::WebCallAndWait(DM_WEBCALLER, wco);
	int rv = DOWNLOAD_FILE_OK;
	if (wca->IsError() || !wca->IsStatusOk()) {
		rv = wca->StatusCode();
		LogError("WebCallAndWait() FAILED - '" << wco.url << "' -> '" << downloadFilePath << "'");
		goto getout;
	}

	// File Download -or- Call Handler
	if (downloadFile) {
		// Uncompress And Unpack File
		if (!DownloadManagerThread::UncompressAndUnpackFile(downloadFilePath)) {
			LogError("UncompressAndUnpackFile() FAILED - '" << downloadFilePath << "'");
			rv = DOWNLOAD_FILE_CORRUPT;
		}
	} else {
		// Call Handler ?
		if (pDI->getHandler() != nullptr) {
			// Call Handler (expects null termination, webcall guarantees that)
			wca->responseChars.push_back(NULL);
			char* rxChars = (char*)wca->ResponseChars();
			size_t rxSize = wca->ResponseSize();
			//			LogInfo("HANDLER - '" << wco.url << "' -> rxSize=" << rxSize );
			pDI->getHandler()->BytesDownloaded(rxChars, rxSize);
			pDI->getHandler()->DownloadComplete();
		}
	}

getout:

	// Broadcast Progress Complete Events
	pDI->FireProgressCompleteEvent(rv, &mutexRef);
	return rv;
}

bool DownloadManagerThread::UncompressAndUnpackFile(const std::string& filePath) {
	// Valid File  ?
	if (filePath.empty())
		return true;
	if (!FileHelper::Exists(filePath)) {
		LogError("Exists() FAILED - '" << filePath << "'");
		return false;
	}

	// Uncompressed/Unpacked File Has Extensions Stripped
	std::string filePathOut = filePath;

	// Uncompress File ? (.gz, .lzma, ...)
	bool isCompressed = (::CompressType(filePath) != COMPRESS_TYPE_NONE);
	if (isCompressed) {
		filePathOut = ::CompressTypeExtDel(filePath);
		if (!::UncompressFile(filePath, true)) {
			LogError("UncompressFile() FAILED - '" << filePath << "'");
			return false;
		}
	}

	// Unpack File ? (.pak)
	bool isPacked = StrSameFileExt(filePathOut, "pak");
	if (isPacked) {
		// Valid File ?
		if (!FileHelper::Exists(filePathOut)) {
			LogError("Exists() FAILED - '" << filePathOut << "'");
			return false;
		}

		// Unpack File (strip file extension)
		Timer timer;
		std::string pathOnly = StrStripFileNameExt(filePathOut);
		size_t numFiles = 0;
		try {
			KPack::UnpackFiles(filePathOut.c_str(), (char*)pathOnly.c_str(), numFiles);
		}
		catch (const std::exception & exception) {
			LogError("UnpackFile() FAILED - '" << filePathOut << "' - " << exception.what());
			return false;
		}

		// Log Unpacked Files
		std::string timeStr;
		TimeMs timeMs = timer.ElapsedMs();
		if (timeMs) {
			StrBuild(timeStr, " " << timeMs << "ms");
		}
		LogInfo("UNPACKED (" << numFiles << " files" << timeStr << ") - '" << filePathOut << "'");
	}

	return true;
}

ULONG DownloadManagerThread::_doWork() {
	while (!isTerminated()) {
		// Get next task with highest priority
		{
			std::lock_guard<fast_recursive_mutex> lock(m_parent.m_sync);
			m_pDI_current = m_parent.popNextTask(); // m_currentDL updates must be protected by the global lock
		}

		// Download File Synchronously
		if (m_pDI_current != nullptr) {
			// Do Download File Synchronously (blocking)
			int rv = DownloadFile(m_pDI_current, m_dispatcher, m_parent.m_sync);
			if (rv != DOWNLOAD_FILE_OK) {
				LogError("DownloadFile() FAILED - rv=" << rv << " '" << m_pDI_current->getUrl() << "'");
			}
			//LogInfo("Download completed: '" << m_pDI_current->getPath() << "'");

			// Broadcast PreloadProgress Events (updates progress bars)
			if (m_pDI_current->isPreload() && m_parent.m_preLoadProgressEvent) {
				m_parent.m_remainingPreLoads--;
				if (m_parent.m_totalPreLoads) {
					// there are some left, calculate the percent complete
					float percent = (float)(m_parent.m_totalPreLoads - m_parent.m_remainingPreLoads) / (float)m_parent.m_totalPreLoads;
					// do I need to get a % from 0 to 100 or 200?  some confusion in the code, i think
					percent *= 200.f;
					IEvent* pEvent = m_dispatcher.MakeEvent(m_parent.m_preLoadProgressEvent->EventId());
					pEvent->SetFilter((ULONG)percent);
					pEvent->SetObjectId(m_parent.m_preLoadProgressEvent->ObjectId());
					m_dispatcher.QueueEvent(pEvent);
				} else {
					// we've finished
					m_parent.m_preLoadProgressEvent->SetFilter(200);
					m_dispatcher.QueueEvent(m_parent.m_preLoadProgressEvent);
					m_parent.m_preLoadProgressEvent = nullptr; // it's deleted elsewhere
				}
			}

			// Delete File DownloadInfo
			DownloadInfo* pDI = m_pDI_current;
			{
				std::lock_guard<fast_recursive_mutex> lock(m_parent.m_sync);
				m_pDI_current = nullptr;
			}
			pDI->FireProgressCompleteEvent(rv, &(m_parent.m_sync));
			delete pDI;
		}

		m_parent.m_sema.waitAndDecrement();
	}

	return 0;
}

DownloadManager::DownloadManager(IDispatcher& disp, IClientEngine* pCE) :
		m_shutdown(false),
		m_dispatcher(disp),
		m_threadCount(DM_THREADS),
		m_threadPriority(DM_THREAD_JS_PRIORITY),
		m_preLoadProgressEvent(0) {
	// DRF - Set Static Instances
	s_pDM = this;
	s_pCE = pCE;

	// Create DownloadManager Webcaller
	WebCaller::GetInstance(DM_WEBCALLER, true, m_threadCount);

	for (size_t i = 0; i < m_threadCount; i++) {
		m_threadVector.push_back(new DownloadManagerThread(*this, disp, m_threadPriority));
		m_threadVector.back()->start();
	}
}

DownloadManager::~DownloadManager() {
	m_shutdown = true;

	s_pCE = nullptr;

	// Cancel All Downloads
	CancelAll();

	// Stop All Download Threads
	for (const auto& pDMT : m_threadVector) {
		if (!pDMT)
			continue;
		pDMT->stop(0, false);
	}

	// Wake All Threads If Waiting
	m_sema.increment(m_threadVector.size());

	// Give Them 100ms to Exit
	fTime::SleepMs(100);

	// Delete All Threads (now)
	for (const auto& pDMT : m_threadVector) {
		if (!pDMT)
			continue;
		pDMT->stop(0, true);
		delete pDMT;
	}

	m_threadVector.clear();

	// avoid leaking on the off chance we're still doing a pre-download here
	if (m_preLoadProgressEvent) {
		m_preLoadProgressEvent->Delete();
		m_preLoadProgressEvent = nullptr;
	}

	s_pDM = nullptr;
}

bool DownloadManager::ArmPreLoadProgress(IEvent* pEvent) {
	m_totalPreLoads = TotalPreloads();
	if (m_totalPreLoads) {
		m_remainingPreLoads = m_totalPreLoads;
		m_preLoadProgressEvent = pEvent;
	} else {
		pEvent->SetFilter(200);
		m_dispatcher.QueueEvent(pEvent);
		m_preLoadProgressEvent = nullptr;
	}
	return true;
}

bool DownloadManager::DownloadFileSync(DownloadInfo* pDI) {
	if (!pDI || m_shutdown)
		return false;

	// Url Ok ?
	if (!pDI->UrlOk())
		return false;

	// File Already Downloaded Ok ? (delete if bad)
	if (pDI->FileOk(true))
		return true;

	// Download File Synchronously
	return (DownloadManagerThread::DownloadFile(pDI, m_dispatcher, m_sync) == DOWNLOAD_FILE_OK);
}

bool DownloadManager::DownloadFileSync(ResourceType jobType, const std::string& url, DownloadHandler* handler) {
	DownloadInfo DI(jobType, url, DL_PRIO_SYNC, handler, false);
	return DownloadFileSync(&DI);
}

bool DownloadManager::DownloadFileAsync(DownloadInfo* pDI) {
	if (!pDI || m_shutdown)
		return false;

	// Url Ok ?
	if (!pDI->UrlOk()) {
		delete pDI;
		return false;
	}

	{
		std::lock_guard<fast_recursive_mutex> lock(m_sync);

		// Assuming up to one event per request
		IEvent* pEvent = nullptr;
		if (!pDI->getEvents().empty()) {
			ASSERT(pDI->getEvents().size() == 1);
			pEvent = pDI->getEvents()[0].get();
		}

		// Warning! Race condition. Two simultaneous requests for the same URL can fail QueueFind
		// and then both get added to the queue. This can result in multiple threads trying to download
		// the same file at the same time.
		// Push To Download Queue If Not Already There
		bool found = QueueFind(pDI->getUrl(), pEvent);
		if (found) {
			delete pDI;
			return true;
		}

		// Queue task
		QueuePush(pDI);
	}

	// Wake Up Threads
	m_sema.increment();
	return true;
}

bool DownloadManager::QueueFind(const std::string& url, IEvent* pEvent) {
	if (!pEvent)
		return false;

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	// Check if currently being downloaded
	for (const auto& pDMT : m_threadVector) {
		if (!pDMT)
			continue;

		auto pDI = pDMT->CurrentDownloadInfo();
		if (!pDI || pDI->getUrl() != url)
			continue;

		pDI->addEvent(pEvent);
		return true;
	}

	// Check if already queued
	for (const auto& pDI : m_queue) {
		if (!pDI || pDI->getUrl() != url)
			continue;

		pDI->addEvent(pEvent);
		return true;
	}

	// Not found
	return false;
}

void DownloadManager::QueueDeleteAll() {
	if (m_queue.empty())
		return;

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	// Delete All Entries In Download Queue
	for (const auto& pDI : m_queue) {
		if (pDI)
			delete pDI;
	}
	m_queue.clear();
}

void DownloadManager::QueuePush(DownloadInfo* pDI) {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);
	m_queue.push_back(pDI);
}

void DownloadManager::CancelAll() {
#if 0
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	// tell each thread to cancel, wakeup any sleeping threads
	LogInfo("Canceling All Downloads...");
	for (const auto& pDMT : m_threadVector) {
		if (!pDMT)
			continue;
		pDMT->CancelCurrent();
	}
#endif

	// Delete All Queue Entries
	QueueDeleteAll();
}

void DownloadManager::CancelJobsByType(ResourceType jobType, bool bSendEvents) {
	if (m_queue.empty())
		return;

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	for (auto it = m_queue.begin(); it != m_queue.end();) {
		const DownloadInfo* pDI = *it;
		if (!pDI)
			continue;

		// Don't Cancel Preloads (job type must match)
		if (pDI->isPreload() || pDI->getJobType() != jobType) {
			++it;
			continue;
		}

		it = m_queue.erase(it);

		if (bSendEvents) {
			// Send "download canceled" events so the requestors can process them
			for (const IEventSharedPtr& pEvent : pDI->getEvents()) {
				if (!pEvent)
					continue;
				pEvent->SetFilter(ULONG_MAX); // Download canceled
				m_dispatcher.QueueEvent(pEvent.get());
			}
		}

		delete pDI;
	}
}

unsigned int DownloadManager::TotalPreloads() {
	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	// Return the number of elements in the queue that were added by the pre-load functions.
	return std::count_if(m_queue.begin(), m_queue.end(), [](const DownloadInfo* info) {
		return info != nullptr && info->isPreload();
	});
}

DownloadInfo* DownloadManager::popNextTask() {
	if (m_queue.empty() || m_shutdown)
		return nullptr;

	std::lock_guard<fast_recursive_mutex> lock(m_sync);

	// Search the queue linearly and find the task with highest priority
	// Linear search is used to support dynamic priority.
	auto itrHit = m_queue.end();
	DownloadPriority maxPrio = DL_PRIO_NO;
	for (auto itr = m_queue.begin(); itr < m_queue.end(); ++itr) {
		auto pDI = *itr;
		if (!pDI)
			continue;

		if (pDI->getPriority() > maxPrio) {
			maxPrio = pDI->getPriority();
			itrHit = itr;
		}
	}
	if (itrHit == m_queue.end())
		return nullptr;

	auto pDI = *itrHit;
	m_queue.erase(itrHit);
	return pDI;
}

} // namespace KEP
