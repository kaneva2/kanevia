///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IResource.h"
#include "ResourceManager.h"
#include "ResourceType.h"
#include "TextureObj.h"

namespace KEP {

class TextureRM : public ResourceManager {
public:
	TextureRM(const std::string& id) :
			ResourceManager(id, ResourceType::TEXTURE) {
		ms_instance = this;
		CTextureEX::ms_pTextureRM = this;
	}

	virtual ~TextureRM() {
		ms_instance = NULL;
	}

	static TextureRM* Instance() {
		return ms_instance;
	}

protected:
	static TextureRM* ms_instance;

	virtual std::string GetResourceLocalStore(const std::string& /*gameFilesDir*/) override {
		return "";
	}
	virtual std::string GetResourceFileName(UINT64 /*hash*/) override {
		return "";
	}
	virtual std::string GetResourceFilePath(UINT64 /*hash*/) override {
		return "";
	}
	virtual std::string GetResourceURL(UINT64 /*hash*/) override {
		return "";
	}

	virtual bool IsUGCItem(UINT64 /*hash*/) const override {
		return false;
	}
};

} // namespace KEP