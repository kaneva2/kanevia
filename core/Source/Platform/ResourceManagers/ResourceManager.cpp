///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ResourceManager.h"

#include "IResource.h"
#include "Event/Base/IEvent.h"
#include "Event/Base/IDispatcher.h"
#include "Event/Events/GenericEvent.h"
#include "Event/Events/ResourceDownloadCompletionEvent.h"
#include "DownloadManager.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "Core/Crypto/Crypto_KRX.h"
#include "IClientEngine.h"
#include "config.h"
#include "ContentService.h"
#include "common\include\CompressHelper.h"
#include "LogHelper.h"
#include "Common/include/KEPHelpers.h"
#include <Core/Util/CallbackSystem.h>
#include <Core/Util/Stream.h>
#include <Common/include/PreciseTime.h>
#include <ResourceType.h>
#include <SetThreadName.h>
#include <Core/Util/Stream.h>
#include "ResourceLoader.h"

static LogInstance("Instance");

namespace KEP {

std::atomic<int> ResourceManager::s_NextLocalGLID = GLID_LOCAL_BASE;

static size_t ResourceTypeToLoadOrder(ResourceType rt) {
	ResourceType aSortedResourceTypes[] = {
		ResourceType::DYNAMIC_OBJECT,
		ResourceType::EQUIPPABLE,
		ResourceType::MESH,
		ResourceType::SKELETON,
		ResourceType::ANIMATION,
		ResourceType::TEXTURE,
		ResourceType::PARTICLE,
		ResourceType::SOUND,
	};

	const size_t iArraySize = std::extent<decltype(aSortedResourceTypes)>::value;
	static_assert(size_t(ResourceType::NUM_RESOURCE_TYPES) == iArraySize, "Need ordering for new resource");
	for (size_t i = 0; i < iArraySize; ++i) {
		if (aSortedResourceTypes[i] == rt)
			return i;
	}
	return iArraySize;
}

ResourceManager::ResourceManager(const std::string& id, ResourceType resourceType) :
		m_id(id), m_supportedResourceType(resourceType), m_queueEnabled(true), m_iResourceLoadOrder(ResourceTypeToLoadOrder(resourceType)) {
}

// DRF - Never Called!
ResourceManager::~ResourceManager() {
#if (DRF_QUICK_QUIT_CLEANUP == 1)

	// Flag Shutdown (helps terminate worker threads)
	Shutdown();

	SetEvent(m_hEventNotSuspended.GetHandle());

	// The doWork thread checks the status of the isTerminated flag whenever
	// it gets out of the wait, so I need to either let the wait timeout
	// or better, signal the wait so the thread checks isTerminated.
	// Either way, it could get killed while it's doing some long job.
	mLoadThread->stop(0, false);

	LONG lCount = 0;
	ReleaseSemaphore(hQueueSema, 1, &lCount); //Try waking up worker thread

	// close semaphore handles, this will cause the worker thread to exit
	CloseHandle(hQueueSema);
	CloseHandle(hReadSema);

	mLoadThread->join(1000); // give it time to exit, if it's working, but not too long
	mLoadThread->reset(); // workaround to close thread handle - jsThread destructor desn't
	delete mLoadThread;

	MutexGuardLock lock(m_resourcesManagedSync);
	for (const auto& it : m_resourcesManaged) {
		IResource* pRes = it.second;
		if (pRes)
			delete pRes;
	}
#endif
}

IResource* ResourceManager::FindResource(UINT64 hash) {
	return m_mapManaged.LookUp(hash).get();
}

IResource* ResourceManager::Add(std::shared_ptr<IResource> pNewResource) {
	return m_mapManaged.Add(pNewResource);
}

void ResourceManager::RemoveResource(UINT64 hash) {
	m_mapManaged.Remove(hash);
}

void ResourceManager::RemoveAllResources() {
	m_mapManaged.RemoveAll();
}

void ResourceManager::RemoveResourcesByState(IResource::State state) {
	std::vector<IResource*> apToErase;
	m_mapManaged.ForEach([&apToErase, state](IResource* pResource) {
		if (pResource && pResource->GetState() == state)
			apToErase.push_back(pResource);
	});
	for (IResource* pResource : apToErase)
		m_mapManaged.Remove(pResource->GetHashKey());
}

bool ResourceManager::DeleteResourceFile(IResource* pRes) {
	if (!pRes)
		return false;

	std::string filePath = pRes->GetFilePath();
	bool ok = FileHelper::Delete(filePath);
	if (!ok)
		LogError(/*m_id <<*/ " FileHelper::Delete() FAILED - '" << filePath << "'");
	return ok;
}

void ResourceManager::ClearErroredResources() {
	m_mapManaged.ForEach([](IResource* pResource) {
		if (pResource->IsErrored())
			pResource->ClearErrored();
	});
}

void ResourceManager::LogResources(bool verbose) const {
	// Log Resource Counts
	size_t nManaged = m_mapManaged.Size();
	LogInfo(m_id << " managed=" << nManaged);

	// Log resources managed
	if (nManaged) {
		m_mapManaged.ForEach([this, verbose](IResource* pResource) {
			if (pResource->IsErrored())
				_LogWarn(m_id << " ERRORED " << pResource->ToStr());
			else if (verbose)
				_LogInfo(m_id << " ...     " << pResource->ToStr());
		});
	}
}
bool ResourceManager::QueueFileForRead(IResource* pRes, bool bImmediate, bool attemptAsyncDownload) {
	if (!pRes || pRes->GetState() == IResource::State::ERRORED)
		return false;

	if (pRes->GetState() == IResource::State::LOADED_AND_CREATED)
		return false;

	if (bImmediate && !attemptAsyncDownload) {
		if (ResourceLoader::Instance().Add(pRes, GLID_INVALID))
			ResourceLoader::Instance().WaitForCompletion(pRes);
		return false; // Original API for this function returned true only if resource was waiting for data.
	} else {
		return ResourceLoader::Instance().Add(pRes, GLID_INVALID);
	}
}

constexpr size_t MAX_VERIFICATION_FAILURES = 3;
bool ResourceManager::VerifyFileIntegrity(IResource* pRes) {
	if (!pRes)
		return false;

	// Get Resource File Path
	std::string filePath = pRes->GetFilePath();

	// Prevent Infinite Loops
	int verificationFailures = pRes->GetVerificationFailures();
	if (verificationFailures >= MAX_VERIFICATION_FAILURES) {
		LogError("FAILED MAX_VERIFICATION_FAILURES '" << filePath << "'");
		return true;
	}

	// Skip Files With No Expected Size or Hash
	auto expectedSize = pRes->GetExpectedSize();
	std::string expectedHash = pRes->GetExpectedHash();
	if (!expectedSize || expectedHash.empty())
		return true;

	// DRF - Only Verify UGC Files Not Already Verified
	GLID glid = pRes->GetGlid();
	if (glid == GLID_INVALID)
		return true;
	auto pCE = DownloadManager::PtrCE();
	if (!pCE)
		return true;
	ContentMetadata* pMD = pCE->GetCachedUGCMetadataPtr(glid);
	if (!pMD || (pMD->LocalHashGet() == expectedHash))
		return true;

	// Get File MD5 Hash
	MD5_DIGEST_STR md5;
	hashData(pRes->GetRawBuffer(), pRes->GetRawSize(), md5);
	std::string hash = md5.s;
	bool hashLenOk = (hash.size() == RESOURCE_HASH_LENGTH);

	// Check Verification Last Hash Match
	std::string lastHash = pRes->GetVerificationLastHash();
	bool lastHashOk = hashLenOk && (STLCompareIgnoreCase(hash, lastHash) == 0);
	if (lastHashOk) {
		LogWarn(/*m_id <<*/ " OK - Last Hash Match '" << filePath << "'");
		if (pMD)
			pMD->LocalHashSet(hash);
		return true;
	}

	// Check Expected Hash Match
	bool expectedHashOk = hashLenOk && (STLCompareIgnoreCase(hash, expectedHash) == 0);
	if (!expectedHashOk) {
		LogError(/*m_id <<*/ " FAILED - (" << hash << " != " << expectedHash << ") '" << filePath << "'");
		pRes->IncVerificationFailures();
		pRes->SetVerificationLastHash(hash);
		return false;
	}

	if (pMD)
		pMD->LocalHashSet(hash);

	return true;
}

BOOL ResourceManager::QueueResource(IResource* pRes, GLID itemGLID) {
	// Queue Enabled ?
	if (!m_queueEnabled) {
		switch (pRes->m_ResourceManagerData.GetState()) {
			case IResource::eQueuedState::COMPLETE:
				return FALSE;
			case IResource::eQueuedState::NOT_QUEUED:
				// New resource request can be safely discarded at zoning (when RM queue is disabled) as they will be re-queued by client engine when needed
				LogWarn(m_id << " [type:" << (int)GetResourceType() << "]"
							 << " queue disabled, ignore new task, hash=" << pRes->GetHashKey());
				return FALSE;
			default:
				// This isn't good. Objects should be removed from queue when queue is disabled, so state should
				// only be COMPLETE or NOT_QUEUED
				LogError("INVALID STATE - FIX ME!");
				assert(false);
				return FALSE;
		}
	}

	// Non-GLID based resource managers should override IsUGCItem to return false
	GLID glidForMetadata = (itemGLID != GLID_INVALID) ? itemGLID : (GLID)pRes->GetHashKey();
	if (!IsUGCItem(pRes->GetHashKey()))
		glidForMetadata = GLID_INVALID; // No metadata needed.

	return ResourceLoader::Instance().Add(pRes, glidForMetadata);
}

void ResourceManager::loadLooseFileInfo(const std::string& infoFile) {
	// Open Resource File Info (eg. textureinfo.dat)
	FILE* fp;
	if (fopen_s(&fp, infoFile.c_str(), "r") || !fp) {
		LogError("fopen() FAILED - '" << infoFile << "'");
		return;
	}

	// Parse Resource File Info CSV[filePath, fileName, fileHash, fileSize]
	char filePath[MAX_PATH];
	char fileName[MAX_PATH];
	char fileHash[MAX_PATH];
	int fileSize;
	while (true) {
		filePath[0] = fileName[0] = fileHash[0] = fileSize = 0;
		if (fscanf_s(fp, "%s %s %s %d",
				filePath,
				_countof(filePath),
				fileName,
				_countof(fileName),
				fileHash,
				_countof(fileHash),
				&fileSize) == EOF) {
			break;
		}

		// Insert File Info Entry
		fileInfo newFile;
		newFile.filePath = StrStripRightLast(filePath, ",");
		newFile.fileName = StrStripRightLast(fileName, ",");
		newFile.fileHash = StrStripRightLast(fileHash, ",");
		newFile.fileSize = (FileSize)fileSize;
		m_looseFileInfo.insert(std::make_pair(fileName, newFile));
	}

	fclose(fp);
}

int ResourceManager::AllocateLocalGLID() {
	return s_NextLocalGLID.fetch_add(-1, std::memory_order_relaxed);
}

UINT ResourceManager::GetResourceCount() {
	return m_mapManaged.Size();
}

void ResourceManager::GetAllResourceKeys(std::vector<UINT64>& keys) {
	m_mapManaged.ForEachKey([&keys](uint64_t key) {
		keys.push_back(key);
	});
}

std::string ResourceManager::GetPatchAssetLocation() {
	auto pCE = DownloadManager::PtrCE();
	return pCE ? pCE->PatchUrl() : "";
}

void ResourceManager::cancelAllPendingTasks() {
	// Warn if queue is not turned off
	ASSERT(!IsQueueEnabled());

	ResourceLoader::Instance().CancelAll();
}

void ResourceManager::ResourceReady(IResource* pRes) {
	if (!pRes)
		return;

	if (pRes->m_ResourceManagerData.IsLoading()) {
		LogError("Trying to set a resource ready that is still loading. " << (pRes->GetResourceManager() ? pRes->GetResourceManager()->m_id : "") << " glid:" << pRes->GetGlid() << " hash:" << pRes->GetHashKey());
		assert(false);
		return;
	}

	CallbackSystem::Instance()->CallbackReady(pRes->GetResourceManager(), pRes->GetHashKey());
}

bool ResourceManager::RegisterResourceLoadedCallback(uint64_t hash, const std::shared_ptr<void>& pCallbackOwner, std::function<void()> callback) {
	CallbackSystem::Instance()->RegisterCallback(this, hash, pCallbackOwner.get(), pCallbackOwner, std::move(callback));

	// Potential race condition since resource could be loaded on another thread while registering.
	// Need to add pCallbackOwner to list before checking whether object is loaded, otherwise callback can be skipped.
	// If it is loaded, we need to call ResourceReady in order to move the callback to m_pCallbacksReady.
	IResource* pRes = FindResource(hash);
	if (!pRes)
		return true;

	if (!pRes->m_ResourceManagerData.IsLoading())
		ResourceReady(pRes);

	return true;
}

size_t ResourceManager::UnregisterAllResourceLoadedCallbacks(void* pCallbackOwner) {
	return CallbackSystem::Instance()->UnregisterAllCallbacksWithOwner(pCallbackOwner);
}

size_t ResourceManager::UnregisterResourceLoadedCallback(void* pCallbackOwner, uint64_t hash) {
	return CallbackSystem::Instance()->UnregisterCallback(this, hash, pCallbackOwner);
}

void ResourceManager::SuspendLoad(bool bSuspend) {
	if (bSuspend) {
		m_iSuspendCount.fetch_add(+1, std::memory_order_acq_rel);
	} else {
		size_t iOldValue = m_iSuspendCount.fetch_add(-1, std::memory_order_acq_rel);
		if (iOldValue == 1)
			ResourceLoader::Instance().Wake();
	}
}

} // namespace KEP
