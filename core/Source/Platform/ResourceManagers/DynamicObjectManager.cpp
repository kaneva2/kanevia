///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "DynamicObjectManager.h"

#include "StreamableDynamicObj.h"
#include "IClientEngine.h"
#include "EquippableSystem.h"
#include "CGlobalInventoryClass.h"
#include "UnicodeMFCHelpers.h"
#include "config.h"
#include "ResourceType.h"
#include "common\include\CompressHelper.h"
#include "DownloadPriorityDistanceModel.h"
#include "LogHelper.h"
#include "ContentService.h"
#include "common\include\IMemSizeGadget.h"

static LogInstance("Instance");

namespace KEP {

class DynamicObjectResource : public TResource<StreamableDynamicObject> {
protected:
	bool IsSerializedDataCompressible() const override {
		return true; // override
	}

public:
	DynamicObjectResource(ResourceManager* resourceManager, UINT64 hashKey, const GLID& glid, bool isLocal) :
			TResource<StreamableDynamicObject>(resourceManager, hashKey, isLocal),
			m_data(nullptr) {
		SetGlid(glid);
		SetState(IResource::State::INITIALIZED_NOT_LOADED);

		// Set default distance
		InitCamDistance(GetDownloadPriorityModel().getDefaultDistance(GetResourceType()));
	}

	virtual ~DynamicObjectResource() {
		if (m_data != nullptr) {
			delete m_data;
			m_data = nullptr;
		}
	}

	virtual std::string ToStr() const override {
		std::string str;
		StrBuild(str, "DOP<" << this << ":" << GetGlid() << ">");
		return str;
	}

	virtual bool CreateResource() override;

	virtual BYTE* GetSerializedData(FileSize& dataLen) override;

	void setData(StreamableDynamicObject* data) {
		m_data = data;
		SetState(IResource::LOADED_AND_CREATED);
	}

	StreamableDynamicObject* getData() {
		if (!IsLocalResource() && StateIsInitializedNotLoaded()) {
			// Queue object for loading, but return NULL Since object isn't loaded
			auto pRM = GetResourceManager();
			if (pRM)
				pRM->QueueResource(this, GetGlid());
			return nullptr;
		}

		return StateIsLoadedAndCreated() ? m_data : nullptr;
	}

	virtual StreamableDynamicObject* GetAsset() override {
		return getData();
	}

	virtual void SetAsset(StreamableDynamicObject* pDynObj) override {
		setData(pDynObj);
	}

	virtual void GetMemSize(IMemSizeGadget* pMemSizeGadget) const {
		if (pMemSizeGadget->AddObjectSizeof(this)) {
			pMemSizeGadget->AddObject(m_data);
		}
	}

private:
	StreamableDynamicObject* m_data;
};

bool DynamicObjectResource::CreateResource() {
	auto pICE = IClientEngine::Instance();
	if (!pICE)
		return false;

	auto pRM = dynamic_cast<DynamicObjectRM*>(GetResourceManager());
	if (!pRM)
		return false;

	bool ret = true;

	if (!GetRawBuffer() || (GetRawSize() == 0)) {
		LogError("GetRawBuffer() null");
		return false;
	}

	CMemFile file;
	file.Attach(GetRawBuffer(), (UINT)GetRawSize(), 0);
	CArchive ar(&file, CArchive::load);
	ar.m_strFileName = Utf8ToUtf16(GetFileName()).c_str();

	StreamableDynamicObject* pNewDynObj = nullptr;
	if (GetURL().find("equippables") == std::string::npos) {
		// This reads a loose dol file
		ASSERT(GetGlid() != GLID_INVALID);
		pNewDynObj = StreamableDynamicObject::readObject(ar, GetGlid());
	} else {
		// Convert from a equippable item - TODO: move to client engine
		std::unique_ptr<CArmedInventoryObj> pArmedInvObj = pICE->GetEquippableRM()->LoadEquippableItemFromLooseArchive(GetGlid(), ar);
		pNewDynObj = StreamableDynamicObject::convertFromAccessory(pArmedInvObj.get(), GetGlid());
	}

	// clean up
	ar.Close();
	file.Detach();
	FreeRawBuffer();

	assert(!m_data); // There used to be a race condition that could cause multiple initialization. If it shows up again, we need to fix it.
	m_data = pNewDynObj;

	if (!m_data) {
		LogError("FAILED - glid=" << GetGlid());
		ret = false;
	} else {
		SetState(LOADED_AND_CREATED);
	}

	return ret;
}

BYTE* DynamicObjectResource::GetSerializedData(FileSize& dataLen) {
	dataLen = 0;
	jsVerifyReturn(m_data != nullptr, nullptr);
	BYTE* dataBuffer = NULL;
	m_data->writeObject(dataBuffer, dataLen);
	return dataBuffer;
}

DynamicObjectRM::DynamicObjectRM(const std::string& id) :
		TypedResourceManager<DynamicObjectResource>(id) {}

DynamicObjectRM::~DynamicObjectRM() {
	RemoveAllResources();
}

std::string DynamicObjectRM::GetResourceLocalStore(const std::string& gameFilesDir) {
	std::string dolDir;
	if (gameFilesDir == "~") // If parent folder is not provided, use game default
		dolDir = IResource::mBaseDolPath[0];
	else
		dolDir = PathAdd(gameFilesDir, "DynamicObjects");

	// Create directory if not already exists
	FileHelper::CreateFolderDeep(dolDir);

	return dolDir;
}

std::string DynamicObjectRM::GetResourceFileName(UINT64 hash) {
	UINT64 glid = hash;
	CStringA fileName;
	if ((glid <= GLID_CORE_MAX) && IS_CORE_GLID((GLID)glid))
		fileName.Format("dc%09I64u.dat", glid); // special naming for core assets
	else
		fileName.Format("d%010I64u.dat", glid);
	return fileName.GetString();
}

std::string DynamicObjectRM::GetResourceFilePath(UINT64 hash) {
	return PathAdd(IResource::mBaseDolPath[0], GetResourceFileName(hash));
}

std::string DynamicObjectRM::GetResourceURL(UINT64 hash) {
	GLID glid = (GLID)hash;
	if (IsUGCItem(glid))
		return ""; // UGC Asset URL will be determined before downloading

	std::string url;
	StrBuild(url, GetPatchAssetLocation() << "GameFiles_DynamicObjects/" << GetResourceFileName(hash));
	url = ::CompressTypeExtAdd(url);
	return url;
}

void DynamicObjectRM::loadLooseFileInfo() {
	std::string filePath = PathAdd(IResource::mBaseDolPath[0], "textureinfo.dat");
	ResourceManager::loadLooseFileInfo(filePath);
}

TypedResourceManager<DynamicObjectResource>* TypedResourceManager<DynamicObjectResource>::ms_pInstance = nullptr;

// AddResourceToManager Specialization
template <>
TResource<StreamableDynamicObject>* AddResourceToManager(UINT64 resKey, GLID globalId, bool isLocal) {
	auto pResMgr = DynamicObjectRM::Instance();
	assert(pResMgr != nullptr);

	if (pResMgr != nullptr) {
		return pResMgr->AddResource(resKey, globalId, isLocal);
	} else {
		return nullptr;
	}
}
} // namespace KEP
