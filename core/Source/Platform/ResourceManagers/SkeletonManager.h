///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "ResourceManager.h"

namespace KEP {

class SkeletonProxy;
class RuntimeSkeleton;

class SkeletonRM : public TypedResourceManager<SkeletonProxy> {
public:
	SkeletonRM(const std::string& id);
	~SkeletonRM();

	// TODO: remove this
	static SkeletonRM* Instance() { return static_cast<SkeletonRM*>(TypedResourceManager<SkeletonProxy>::Instance()); }

	virtual SkeletonProxy* GetSkeletonProxy(int glid, bool bAutoFetch = true);

	// \brief Add an existing runtime skeleton
	// \param glid skeleton GLID
	// \param skeleton the runtime skeleton data
	// \return a skeleton proxy that's fully initialised
	SkeletonProxy* AddRuntimeSkeleton(GLID glid, RuntimeSkeleton* skeleton);

	void DestroyRuntimeSkeleton(GLID glid);

protected:
	// \brief Return a path pointing to where resource should be stored locally
	// \param gameFilesDir a path pointed to "GameFiles" folder of current game
	//@@Assumption: caller must pass in a path pointed to "GameFiles" folder of current game.
	virtual std::string GetResourceLocalStore(const std::string& gameFilesDir) override;

	//! \brief Compose a file name for serialization based on hash code provided.
	//! \param hash Hash code for the resource to be serialized.
	virtual std::string GetResourceFileName(UINT64 hash) override;

	virtual std::string GetResourceFilePath(UINT64 hash) override;
	virtual std::string GetResourceURL(UINT64 hash) override {
		return ""; // Skeleton data are currently embedded with other assets
	}

	virtual bool IsUGCItem(UINT64 /*hash*/) const override {
		return false; // Return false to indicate that skeletons do not require separate download
	}

private:
	std::vector<SkeletonProxy*> m_skeletons; /// bin for deleted, still running animations
};

} // namespace KEP
