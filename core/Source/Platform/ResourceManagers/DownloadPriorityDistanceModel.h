///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ResourceType.h"

namespace KEP {

class DownloadPriorityDistanceModel {
private:
	// Distance scaling bias on the forward axis (in camera space), distorting concentric
	// circles into egg-like shapes in favor of downloads in front of the camera.
	const double RAD_FRWD_SCALE = 1.8; // Entity rooted at 1.8 units directly in front of the viewpoint has a normalized distance of 1.0
	const double RAD_BKWD_SCALE = 0.6; // Entity rooted at 0.6 units directly behind the viewpoint has a normalized distance of 1.0

	// Distance factor offset bias by resource type and resolution.
	const double CHARMESH_DIST_BIAS = 0.0; // No distance offset for character meshes
	const double DYNOBJ_DIST_BIAS = 0.0; // No distance offset for dynamic objects
	const double TEX_DIST_BIAS_MIN = 2000.0; // Textures of lowest resolution at the origin has the same priority as the mesh at 2000 units (normalized) away

	// Default resource distance before we have chance to actual calculate them
	const double DYNOBJ_DIST_DEFAULT = 2000.0; // For dynamic objects, default to 2000 units away from camera when inited
	const double OTHER_DIST_DEFAULT = 0.0; // For other assets, default to 0

	// Normalized distance are placed into buckets of adjacent half-open intervals on radius.
	// The length of intervals increase as the target gets far away from the origin:
	// length[k] = scale * base ^ k, where k is the bucket ID. With base = 1.2, scale = 10,
	// 31 buckets cover as far as 14000 units (~2.6 mi) from the origin. Texture distance
	// bias of 2000 units equals to first 21 buckets.
	const double DIST_MODEL_BASE = 1.2; // > 1
	const double DIST_MODEL_SCALE = 10; // > 0

	// Number of buckets
	const unsigned NUM_PRIORITY_BUCKETS = 32;

	// Sample bucket boundaries for base = 1.2, scale = 10
	// 10, 22, 36, 53, 74, 99, 129, 164, 207, 259, 321, 395, 484, 591, 720, 874, 1059, ..., 8192, 9840, 11818, 14192
	// 0 ~ 10: first bucket, 10 ~ 22: 2nd bucket, etc.

	// Min bucket ID for each resource type
	size_t m_baseBucketIndices[ResourceType::NUM_RESOURCE_TYPES];

	unsigned getBaseBucketIndex(ResourceType type, unsigned prioRangeSel) const {
		ASSERT(type < ResourceType::NUM_RESOURCE_TYPES);
		if (type > ResourceType::NONE && type < ResourceType::NUM_RESOURCE_TYPES) {
			return m_baseBucketIndices[(size_t)type] + prioRangeSel * NUM_PRIORITY_BUCKETS;
		}
		return 0;
	}

public:
	DownloadPriorityDistanceModel() {
		// Calculate base bucket ID for each resource type
		for (size_t i = 0; i < (size_t)ResourceType::NUM_RESOURCE_TYPES; i++) {
			m_baseBucketIndices[i] = 0;
		}
		// Convert distance bias into base bucket ID
		m_baseBucketIndices[(size_t)ResourceType::MESH] = getPriorityBucket(ResourceType::NONE, 0, CHARMESH_DIST_BIAS);
		m_baseBucketIndices[(size_t)ResourceType::DYNAMIC_OBJECT] = getPriorityBucket(ResourceType::NONE, 0, DYNOBJ_DIST_BIAS);
		m_baseBucketIndices[(size_t)ResourceType::TEXTURE] = getPriorityBucket(ResourceType::NONE, 0, TEX_DIST_BIAS_MIN);
	}

	double getDefaultDistance(ResourceType type) const {
		switch (type) {
			case ResourceType::DYNAMIC_OBJECT:
				return DYNOBJ_DIST_DEFAULT;
			default:
				return OTHER_DIST_DEFAULT;
		}
	}

	double getNormalizedDistance(float x, float y, float z) const {
		// Scale forward axis
		z *= z >= 0 ? RAD_FRWD_SCALE : RAD_BKWD_SCALE;

		// Calculate normalized distance
		return sqrt(x * x + y * y + z * z);
	}

	// Coordinates in camera space -> bucket index (base + 0 ~ base + 30, 31 for out-of-bound)
	unsigned getPriorityBucket(ResourceType type, unsigned prioRangeSel, double dist) const {
		// Only texture use range selector
		ASSERT(prioRangeSel == 0 || type == ResourceType::TEXTURE || type == ResourceType::CUSTOM_TEXTURE);

		// Base bucket index
		unsigned baseIndex = getBaseBucketIndex(type, prioRangeSel);
		unsigned relIndex = 0;

		ASSERT(dist >= 0);
		if (dist > 1E-4) {
			// For geometric sequence: scale*base^0, scale*base^1, ... scale*base^31, where scale > 0, base > 1
			// sum of first N items in the sequence: S(N) = scale * (1 - base^N) / (1-base)
			// For a given value D, return minimum N where S(N) >= D, or 32 if S(31) < D.
			// S(N) >= D
			//	=> scale * (1 - base^N) / (1-base) >= D
			//	=> (1 - base^N) / (1-base) >= D / scale			( scale > 0 )
			//	=> 1 - base^N <= D / scale * (1-base)			( base  > 1 )
			//	=> base^N >= D / scale * (base-1) + 1
			//	=> N >= log{base} (D / scale * (base-1) + 1)	( logarithm is strictly increasing )
			//
			// Alternatively this can be done with binary search in a sorted array. There is no significant advantage in performance
			// one way or another, based on a test staged according to our case.
			relIndex = std::min(NUM_PRIORITY_BUCKETS - 1, (unsigned)(log(dist / DIST_MODEL_SCALE * (DIST_MODEL_BASE - 1) + 1) / log(DIST_MODEL_BASE)));
		}

		return baseIndex + relIndex;
	}
};

} // namespace KEP
