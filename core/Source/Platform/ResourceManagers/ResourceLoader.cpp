///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ResourceLoader.h"
#include "ResourceType.h"
#include "ResourceManager.h"
#include "SetThreadName.h"
#include "Core/Util/Mutex.h"
#include "Core/Util/Parameter.h"
#include "Core/Util/Stream.h"
#include "Core/Util/WindowsAPIWrappers.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "Core/Crypto/Crypto_KRX.h"
#include "ContentService.h"
#include "IClientEngine.h"
#include "Event/Events/ResourceDownloadCompletionEvent.h"
#include "DownloadManager.h"
#include "Core/Util/CallbackSystem.h"
#include "LogHelper.h"
#include "Common/include/CompressHelper.h"
#include "Common/include/PreciseTime.h"
#include "config.h"
#include "Event/Base/IDispatcher.h"

static LogInstance("Instance");

namespace KEP {

static constexpr size_t knMaxConcurrentDownloads = 8;
static constexpr size_t knMaxConcurrentReads = 8;
static constexpr size_t knNumberOfReadThreads = 2; // 1 might be enough. Need to test on slow machines once this code gets to preview.
ResourceLoader ResourceLoader::s_Instance;

template <IResource::eQueuedState stateInQueue>
class ResourceLoader::ResourceQueue {
	static constexpr IResource::eQueuedState stateBeforeQueue = IResource::eQueuedState(static_cast<int>(stateInQueue) - 1);
	static constexpr IResource::eQueuedState stateAfterQueue = IResource::eQueuedState(static_cast<int>(stateInQueue) + 1);

public:
	ResourceQueue() {
	}
	template <typename Callable>
	void ForEach(Callable&& callable) const {
		std::lock_guard<fast_mutex> lock(m_mtxResources);
		for (auto& pr : m_mapResources) {
			for (const auto& pResource : pr.second)
				callable(pResource);
		}
	}
	size_t Size() const {
		std::lock_guard<fast_mutex> lock(m_mtxResources);
		size_t sz = 0;
		for (auto& pr : m_mapResources)
			sz += pr.second.size();
		return sz;
	}
	IResource* Dequeue();
	void DequeueAll(IResource::eQueuedState newState) {
		std::lock_guard<fast_mutex> lock(m_mtxResources);
		for (auto& pr : m_mapResources) {
			for (IResource* pResource : pr.second)
				pResource->m_ResourceManagerData.ChangeState(stateInQueue, newState);
		}
		m_mapResources.clear();
	}
	bool Enqueue(IResource* pResource, IResource::eQueuedState expectedState = stateBeforeQueue) {
		ResourceManager* pRM = pResource->GetResourceManager();
		if (!pRM)
			return false;

		std::lock_guard<fast_mutex> lock(m_mtxResources);
		if (!pResource->m_ResourceManagerData.ChangeState(expectedState, stateInQueue))
			return false;
		m_mapResources[pRM].push_back(pResource);
		return true;
	}

private:
	mutable fast_mutex m_mtxResources;
	struct CompareByPriority {
		bool operator()(ResourceManager* pLeft, ResourceManager* pRight) const {
			return pLeft->GetResourceLoadOrder() < pRight->GetResourceLoadOrder();
		}
	};
	std::map<ResourceManager*, std::vector<IResource*>, CompareByPriority> m_mapResources;
};

static inline void DeleteResourceFile(IResource* pResource) {
	if (!pResource)
		return;

	if (!FileHelper::Delete(pResource->GetFilePath()))
		LogError("Error deleting invalid resource file: " << pResource->GetFilePath());
}

// Alertable to allow asynchronous file reads while waiting for mutex.
class AlertableMutex {
public:
	AlertableMutex() {
		m_hMutex = CreateMutex(nullptr, FALSE, nullptr);
	}
	~AlertableMutex() {
		CloseHandle(m_hMutex);
	}
	void lock() {
		WaitForSingleObjectEx(m_hMutex, INFINITE, FALSE);
	}
	void unlock() {
		ReleaseMutex(m_hMutex);
	}
	HANDLE native_handle() {
		return m_hMutex;
	}

private:
	HANDLE m_hMutex = NULL;
};

class AlertableConditionVariable {
public:
	AlertableConditionVariable() {
	}
	~AlertableConditionVariable() {
	}
	bool wait_for(std::unique_lock<AlertableMutex>& mtxLock, std::chrono::milliseconds duration) {
		return wait(mtxLock, duration.count());
	}
	bool wait(std::unique_lock<AlertableMutex>& mtxLock, DWORD dwTimeoutMs = INFINITE) {
		// One event per thread created on demand to avoid creating too many events.
		static thread_local std::shared_ptr<EventHandleWrapper> s_phEvent;
		if (!s_phEvent)
			s_phEvent = std::make_shared<EventHandleWrapper>(CreateEvent(nullptr, TRUE, FALSE, nullptr));
		HANDLE hEvent = s_phEvent->GetHandle();
		ResetEvent(hEvent);
		{
			std::lock_guard<fast_mutex> lock(m_mtx);
			m_listEventHandles.push_back(s_phEvent);
		}

		// Can't use unique_lock to unlock here because we reacquire the mutex without using unique_lock
		// This confuses unique_lock and triggers an assert, so we manipulate the mutex directly.
		mtxLock.mutex()->unlock();

		HANDLE handles[2] = { hEvent, mtxLock.mutex()->native_handle() };
		while (true) {
			// Wait for multiple objects makes the order of notification and releasing of mutex irrelevant.
			// If we wait first, then acquire the mutex, we could be woken briefly only to immediately wait
			// again incurring the cost of a thread switch twice instead of only once.
			DWORD waitResult = WaitForMultipleObjectsEx(2, handles, TRUE, dwTimeoutMs, TRUE);

			// If the wait succeeds, the thread that set our event should have removed it from the list.
			if (waitResult == WAIT_OBJECT_0 || waitResult == WAIT_OBJECT_0 + 1)
				return true;

			// Warning! We do not own the mutex at this point. We only get it if the wait succeeds.

			if (waitResult != WAIT_IO_COMPLETION && waitResult != WAIT_TIMEOUT) {
				DWORD dwLastError = GetLastError();
				LogError("WaitForMultipleObjectsEx failed with unexpected return value " << waitResult << " GetLastError() = " << dwLastError);
				assert(false);
			}

			// Alternative API not currently needed - exit wait after an I/O event. Need to manually acquire the mutex.
			bool bReturnFalseOnIO = false;
			if (waitResult == WAIT_TIMEOUT || (waitResult == WAIT_IO_COMPLETION && bReturnFalseOnIO)) {
				{
					// Since our wait did not succeed and we're not going to continue waiting, our event needs to be removed from the list.
					std::lock_guard<fast_mutex> lock(m_mtx);
					auto itr = std::find(m_listEventHandles.begin(), m_listEventHandles.end(), s_phEvent);
					if (itr != m_listEventHandles.end()) {
						m_listEventHandles.erase(itr);
					} else {
						// Event was set after the wait exited for I/O. This is a common occurrence.
						// Clear the event before returning.
						// Note that the wait (duration 0) is done inside the lock, otherwise the assert could be wrong.
						if (WaitForSingleObject(hEvent, 0) != WAIT_OBJECT_0)
							assert(false); // Event should have been set if is no longer in the list.
					}
				}

				// Should caller expect to hold the mutex in this case?
				// Probably, otherwise the unique_lock surrounding the condition_variable wait will try
				// to release a mutex it doesn't hold.
				mtxLock.mutex()->lock(); // See not above where mutex is unlocked.
				return false; // Even with a false return, caller needs to check his conditions before waiting again.
			}
		}
	}
	void notify_all() {
		std::list<std::shared_ptr<EventHandleWrapper>> handles;
		{
			std::lock_guard<fast_mutex> lock(m_mtx);
			handles.swap(m_listEventHandles);
		}
		for (const auto& phEvent : handles)
			SetEvent(phEvent->GetHandle());
	}
	void notify_one() {
		std::shared_ptr<EventHandleWrapper> phEvent;
		{
			std::lock_guard<fast_mutex> lock(m_mtx);
			if (m_listEventHandles.empty())
				return;
			phEvent = m_listEventHandles.front();
			m_listEventHandles.pop_front();
		}
		SetEvent(phEvent->GetHandle());
	}

private:
	fast_mutex m_mtx;
	// Using shared_ptr so event remains valid after thread exits.
	// Alternatively, we can call SetEvent() while holding the lock, but
	// testing suggests this can be slow.
	std::list<std::shared_ptr<EventHandleWrapper>> m_listEventHandles;
};

// Data used by load thread
class ResourceLoader::LoadThreadData {
	struct OverlappedReadResult;

public:
	bool Init(size_t nThreads);
	void Wake();
	bool AddToPreprocessQueue(IResource* pResource);
	bool AddToDownloadQueue(IResource* pResource, eQueuedState expectedState);
	bool StartDownload(IResource* pResource);
	void DownloadCompleted(IResource* pResource, int resultFilter);
	bool ProcessDownloadCompleted(IResource* pResource, int resultFilter);
	void SetTerminate();
	void WaitTerminate();
	bool ClearQueues();

private:
	void LoadThreadProc(size_t iThread);
	IResource* ReadCallback(DWORD error, DWORD nBytesRead, OverlappedReadResult* pOverlapped);
	static void WINAPI ReadCallbackStatic(DWORD error, DWORD nBytesRead, LPOVERLAPPED lpOverlapped);
	IResource* DequeueNext();
	static FileHandleWrapper OpenFile(IResource* pResource, Out<size_t> fileSizeOut);
	bool StartReading(IResource* pResource);

	std::vector<IResource*> GetNeedProcess_WithLock();
	void Preprocess_WithoutLock(const std::vector<IResource*>& apNeedProcess, Out<std::vector<IResource*>> apNeedDownload, Out<std::vector<IResource*>> apNeedRead);
	void EnqueuePreprocessed_WithLock(const std::vector<IResource*>& apNeedDownload, const std::vector<IResource*>& apNeedRead);

	OverlappedReadResult* AllocateOverlapped(FileHandleWrapper hFile, IResource* pResource);
	std::unique_ptr<OverlappedReadResult> GetOverlapped(OVERLAPPED* pOverlapped);

private:
	struct OverlappedReadResult : public OVERLAPPED {
		OverlappedReadResult(FileHandleWrapper hFile, IResource* pResource) :
				m_hFile(std::move(hFile)), m_pResource(pResource) {
			memset(static_cast<OVERLAPPED*>(this), 0, sizeof(OVERLAPPED));
		}
		FileHandleWrapper m_hFile;
		IResource* m_pResource;
	};

	std::mutex m_mtxOverlapped;
	std::vector<std::unique_ptr<OverlappedReadResult>> m_apOverlapped;
	std::vector<std::thread> m_aLoadThreads; // For reading and downloading. More than one because Windows' CreateFile empirically measured to block for a long time.

	AlertableMutex m_mtx;
	AlertableConditionVariable m_condVar;
	//std::mutex m_mtx;
	//std::condition_variable m_condVar;

	bool m_bTerminate = true; // Call Init() before starting threads.
	std::map<IResource*, IEventSharedPtr> m_mapActiveDownloads;
	std::set<IResource*> m_setActiveReads;
	std::vector<IResource*> m_apNeedProcess; // Not yet determined whether load or download is required.
	ResourceQueue<eQueuedState::IN_DOWNLOAD_QUEUE> m_qNeedDownload;
	ResourceQueue<eQueuedState::IN_READ_QUEUE> m_qNeedRead;
};

class ResourceLoader::CreateThreadData {
public:
	bool Init(size_t nThreads);
	void Wake();
	void Add(IResource* pResource);
	void SetTerminate();
	void WaitTerminate();
	void ClearQueues();

private:
	void CreateThreadProc(size_t iThread);
	eCreateResourceResult DoCreateResource(IResource* pResource);
	IResource* DequeueNext(size_t iThread);

private:
	std::vector<std::thread> m_aCreateThreads;

	std::mutex m_mtx;
	std::condition_variable m_condVar;
	bool m_bTerminate = true;
	ResourceQueue<eQueuedState::IN_CREATE_QUEUE> m_qNeedCreate;
	std::set<ResourceManager*> m_setResourcesSuspended;
};

ResourceLoader::ResourceLoader() {
	m_pCreateThreadData = std::make_unique<CreateThreadData>();
	m_pLoadThreadData = std::make_unique<LoadThreadData>();
}

ResourceLoader::~ResourceLoader() {
	Shutdown();
}

void ResourceLoader::Init() {
	{
		std::lock_guard<std::mutex> lock(m_mtxThreadsInitialized);
		if (!m_bThreadsInitialized.load(std::memory_order_acquire)) {
			SYSTEM_INFO sysInfo;
			GetSystemInfo(&sysInfo);
			size_t nProcessors = (std::max<DWORD>)(1, sysInfo.dwNumberOfProcessors);
			m_pCreateThreadData->Init(nProcessors);
#if 1
			m_pLoadThreadData->Init(knNumberOfReadThreads);
#else
			// Random number of read threads to test various counts without rebuilding.
			size_t nReadThreads = 1 << (rand() % 4);
			LogInfo("Read thread count: " << nReadThreads);
			m_pLoadThreadData->Init(nReadThreads);
#endif
			m_bThreadsInitialized.store(true, std::memory_order_release);
		}
	}
}

void ResourceLoader::Shutdown() {
	CancelAll();
}

void ResourceLoader::CancelAll() {
	{
		std::lock_guard<std::mutex> lock(m_mtxWaitingMetadata);
		for (IResource* pResource : m_setLoadAwaitingMetadata)
			VerifyChangeState(pResource, eQueuedState::WAITING_METADATA, eQueuedState::NOT_QUEUED);
		m_setLoadAwaitingMetadata.clear();
	}

	// Tell threads to terminate.
	m_pLoadThreadData->SetTerminate();
	m_pCreateThreadData->SetTerminate();
	m_pLoadThreadData->WaitTerminate();
	m_pCreateThreadData->WaitTerminate();
	m_pLoadThreadData->ClearQueues();
	m_pCreateThreadData->ClearQueues();
	m_bThreadsInitialized.store(false, std::memory_order_release);
}

void ResourceLoader::Wake() {
	m_pLoadThreadData->Wake();
	m_pCreateThreadData->Wake();
}

void ResourceLoader::ResourceMetadataReady(const ContentMetadata& md, IResource* pResource) {
	// Get Resource Metadata Asset Url
	std::string assetUrl, fileHash;
	FileSize fileSize = 0;
	unsigned _uniqueId = 0;
	md.getPrimaryAsset(assetUrl, fileSize, fileHash, _uniqueId);
	pResource->SetURL(assetUrl.c_str());
	pResource->SetExpectedSize(fileSize);
	pResource->SetExpectedHash(fileHash);
	pResource->SetGlid(md.getGlobalId());

	m_pLoadThreadData->AddToPreprocessQueue(pResource);
}

// Returns true if loading started.
bool ResourceLoader::Add(IResource* pResource, GLID glidForMetadata) {
	if (!pResource)
		return false;

	if (!ChangeState(pResource, IResource::eQueuedState::NOT_QUEUED, IResource::eQueuedState::ADDING))
		return false;

	if (!m_bThreadsInitialized.load(std::memory_order_acquire))
		Init();

	if (!IS_VALID_GLID(glidForMetadata) || pResource->IsURLAvailable())
		return m_pLoadThreadData->AddToPreprocessQueue(pResource); // No need for metadata.

	IClientEngine* pClientEngine = DownloadManager::PtrCE();

	// Add resource using metadata.
	ContentMetadata md;
	if (pClientEngine->GetCachedUGCMetadata(glidForMetadata, md)) {
		ResourceMetadataReady(md, pResource);
		return true;
	}

	// Need to wait for the metadata.
	// Add this resource to the list of those waiting for metadata.
	{
		std::lock_guard<std::mutex> lock(m_mtxWaitingMetadata);
		m_setLoadAwaitingMetadata.insert(pResource);
		VerifyChangeState(pResource, eQueuedState::ADDING, eQueuedState::WAITING_METADATA);
	}

	// Callback for handling metadata arrival.
	auto MetadataReadyCallback = [this, pClientEngine, glidForMetadata, pResource]() {
		// Remove resource from list waiting for metadata
		{
			std::lock_guard<std::mutex> lock(m_mtxWaitingMetadata);
			if (m_setLoadAwaitingMetadata.erase(pResource) == 0) {
				assert(pResource->m_ResourceManagerData.GetState() != eQueuedState::WAITING_METADATA);
				return; // No longer want to continue loading this object. Rezone probably occurred between metadata request and receipt.
			}

			// Put back in ADDING state so we can add it to the queue.
			VerifyChangeState(pResource, eQueuedState::WAITING_METADATA, eQueuedState::ADDING);
		}

		// Start load with metadata, or error if no metadata available.
		ContentMetadata md;
		if (pClientEngine->GetCachedUGCMetadata(glidForMetadata, md)) {
			ResourceMetadataReady(md, pResource);
		} else {
			LogError("GetCachedUGCMetadata() FAILED - glid<" << glidForMetadata << ">");
			Errored(pResource, IResource::Error::NO_METADATA);
		}
	};

	// Request metadata with our callback.
	ContentService::Instance()->ContentMetadataCacheAsync(glidForMetadata, MetadataReadyCallback, this);
	return true;
}

void ResourceLoader::WaitForCompletion(IResource* pResource) {
	{
		std::unique_lock<std::mutex> lock(m_mtxWaitingResources);
		auto itr = m_mapWaitingResources.insert(std::make_pair(pResource, false));
		if (pResource->m_ResourceManagerData.IsLoading()) {
			do {
				assert(pResource->m_ResourceManagerData.IsLoading()); // State shouldn't change without setting our flag. (Unless resource was unloaded and started loading again.)
				m_cvWaitingResources.wait(lock);
			} while (itr->second == false); // This can be false if another thread is waiting for completion of a different resource.
		}
		m_mapWaitingResources.erase(itr);
	}
}

EVENT_PROC_RC ResourceLoader::ProcessDownloadCompletionEvent(ResourceDownloadCompletionEvent* rdce) {
	IResource* pResource = rdce->getResource();
	if (pResource)
		m_pLoadThreadData->DownloadCompleted(pResource, rdce->Filter());
	else
		assert(false);
	return EVENT_PROC_RC::OK;
}

bool ResourceLoader::Errored(IResource* pResource, IResource::Error errorCode) {
	eQueuedState currentState = pResource->m_ResourceManagerData.GetState();
	if (currentState == eQueuedState::NOT_QUEUED || currentState == eQueuedState::COMPLETE) {
		// Resource is not managed by loader. It is not safe to change its state.
		return false;
	}
	pResource->m_ResourceManagerData.SetState(eQueuedState::PROCESSING_ERROR);
	return ResourceFinishedLoading(pResource, eQueuedState::PROCESSING_ERROR, errorCode);
}

bool ResourceLoader::ResourceFinishedLoading(IResource* pResource, eQueuedState expectedState, IResource::Error errorCode) {
	pResource->SetError(errorCode);
	if (errorCode != IResource::Error::ERROR_NONE)
		pResource->SetState(IResource::State::ERRORED);
	pResource->FreeRawBuffer();
	if (!ChangeState(pResource, expectedState, eQueuedState::COMPLETE)) {
		assert(false); // bad resource state
		if (!pResource->m_ResourceManagerData.IsLoading())
			return false; // unsafe to change state
	}
	//LogInfo("Finished resource " << pResource->GetFilePath() << " glid = " << pResource->GetGlid());

	{
		std::unique_lock<std::mutex> lock(Instance().m_mtxWaitingResources);
		auto rng = Instance().m_mapWaitingResources.equal_range(pResource);
		if (rng.first != rng.second) {
			for (auto itr = rng.first; itr != rng.second; ++itr)
				itr->second = true;
			Instance().m_cvWaitingResources.notify_all(); // There should be only one. Wake them all and let them check themselves.
		}
	}
	ResourceManager::ResourceReady(pResource);

	return true;
}

bool ResourceLoader::VerifyState(IResource* pResource, eQueuedState expectedState) {
	eQueuedState state = pResource->m_ResourceManagerData.GetState();
	assert(state == expectedState);
	return state == expectedState;
}

bool ResourceLoader::VerifyChangeState(IResource* pResource, eQueuedState oldState, eQueuedState newState) {
	bool bChangeStateSucceeded = ChangeState(pResource, oldState, newState);
	assert(bChangeStateSucceeded);
	return bChangeStateSucceeded;
}

bool ResourceLoader::ChangeState(IResource* pResource, eQueuedState oldState, eQueuedState newState) {
	if (pResource->m_ResourceManagerData.ChangeState(oldState, newState)) {
		// Keep track of the number of resources in each state. This may be useful for debugging.
		//s_aResourceStateCounts[static_cast<size_t>(newState)].fetch_add(1, std::memory_order_acq_rel);
		//s_aResourceStateCounts[static_cast<size_t>(oldState)].fetch_add(-1, std::memory_order_acq_rel);
		return true;
	}
	return false;
}

bool ResourceLoader::LoadThreadData::Init(size_t nThreads) {
	if (!m_aLoadThreads.empty())
		return false; // Currently running.

	m_bTerminate = false;

	for (size_t i = 0; i < nThreads; ++i)
		m_aLoadThreads.push_back(std::thread([i, this]() { this->LoadThreadProc(i); }));
	return true;
}

void ResourceLoader::LoadThreadData::Wake() {
	m_condVar.notify_one();
}

bool ResourceLoader::LoadThreadData::AddToPreprocessQueue(IResource* pResource) {
	//LogInfo("Starting resource " << pResource->GetFilePath() << " glid = " << pResource->GetGlid());

	{
		std::lock_guard<decltype(m_mtx)> lock(m_mtx);

		if (!ChangeState(pResource, eQueuedState::ADDING, eQueuedState::IN_PREPROCESS_QUEUE))
			return false;

		if (m_apNeedProcess.empty())
			m_condVar.notify_one();
		m_apNeedProcess.push_back(pResource);
	}

	return true;
}

bool ResourceLoader::LoadThreadData::AddToDownloadQueue(IResource* pResource, eQueuedState expectedState) {
	VerifyState(pResource, expectedState);
	{
		std::lock_guard<decltype(m_mtx)> lock(m_mtx);

		if (!m_qNeedDownload.Enqueue(pResource, expectedState)) {
			assert(false);
			Errored(pResource, IResource::Error::LOAD_ERROR);
			return false;
		}
		if (m_mapActiveDownloads.size() < knMaxConcurrentDownloads)
			m_condVar.notify_one(); // Wake a thread to handle the download.
	}

	return true;
}

bool ResourceLoader::LoadThreadData::StartDownload(IResource* pResource) {
	VerifyState(pResource, eQueuedState::WAITING_DOWNLOAD);

	DownloadManager* pDownloadManager = DownloadManager::Ptr();
	IClientEngine* pClientEngine = DownloadManager::PtrCE();
	if (pDownloadManager && pClientEngine) {
		// Download Resource File (asynchronously)
		IEventSharedPtr e = new ResourceDownloadCompletionEvent(pResource->GetResourceManager(), pResource);
		bool bCompressed = (::CompressType(pResource->GetURL()) != COMPRESS_TYPE_NONE);
		if (pClientEngine->DownloadResourceAsync(pResource, bCompressed, e.get(), pResource->IsPreload())) {
			std::lock_guard<decltype(m_mtx)> lock(m_mtx);
			auto itr = m_mapActiveDownloads.find(pResource);
			if (itr != m_mapActiveDownloads.end()) {
				itr->second = std::move(e);
				return true;
			} else {
				assert(false); // Resource should be in the download queue.
				return false; // Don't remove from map since apparently a separate download was started for it.
			}
		}
	}

	{
		std::lock_guard<decltype(m_mtx)> lock(m_mtx);
		auto itr = m_mapActiveDownloads.find(pResource);
		if (m_mapActiveDownloads.erase(pResource) != 1)
			assert(false);
	}
	Errored(pResource, IResource::Error::DOWNLOAD_FAILURE);
	return false;
}

void ResourceLoader::LoadThreadData::DownloadCompleted(IResource* pResource, int resultFilter) {
	ProcessDownloadCompleted(pResource, resultFilter);
	m_condVar.notify_one(); // We can start another download and/or start reading downloaded file.
}

bool ResourceLoader::LoadThreadData::ProcessDownloadCompleted(IResource* pResource, int resultFilter) {
	std::lock_guard<decltype(m_mtx)> lock(m_mtx);
	if (m_mapActiveDownloads.erase(pResource) != 1) {
		LogWarn("Invalid download notification. Could be caused by download starting before rezone and finishing after.");
		return false;
	}
	if (!VerifyChangeState(pResource, eQueuedState::WAITING_DOWNLOAD, eQueuedState::PROCESSING_DOWNLOAD)) {
		LogError("Invalid state of downloaded object");
		Errored(pResource, IResource::Error::LOAD_ERROR);
		return false;
	}

	// Successful download. Queue it to read.
	if (resultFilter == 200) {
		m_qNeedRead.Enqueue(pResource);
		return true;
	}

	// Cancelled download. Remove resource from loading process.
	if (resultFilter == -1) {
		LogWarn("CANCELLED Download '" << pResource->GetURL() << "'");
		pResource->SetState(IResource::State::INITIALIZED_NOT_LOADED);
		pResource->SetError(IResource::Error::ERROR_NONE);
		VerifyChangeState(pResource, eQueuedState::PROCESSING_DOWNLOAD, eQueuedState::NOT_QUEUED);
		return false;
	}

	// Some error occurred
	if (resultFilter == 1)
		LogError("FAILED Download '" << pResource->GetURL() << "'");
	else
		LogError("Unknown Download Error " << resultFilter << ": '" << pResource->GetURL() << "'");

	Errored(pResource, IResource::Error::DOWNLOAD_FAILURE);
	return false;
}

void ResourceLoader::LoadThreadData::SetTerminate() {
	{
		std::lock_guard<decltype(m_mtx)> lock(m_mtx);
		m_bTerminate = true;
	}
	m_condVar.notify_all();
}

void ResourceLoader::LoadThreadData::WaitTerminate() {
	for (std::thread& thread : m_aLoadThreads)
		thread.join();
	m_aLoadThreads.clear();
}

bool ResourceLoader::LoadThreadData::ClearQueues() {
	std::lock_guard<decltype(m_mtx)> lock(m_mtx);
	if (!m_bTerminate) {
		// Can't safely, completely clear queues if threads are processing.
		assert(false); // Should have terminated first.
		return false;
	}

	// Cancel pending downloads through download manager.
	// Downloads completed could have pending events. We cancel those, but there may be a
	// race condition where it's too late to cancel the download but the event hasn't been
	// queued to the dispatcher yet. So we can get some download callbacks that we don't
	// want to process. Since the resources were removed from m_mapActiveDownloads, the
	// events will be ignored.
	DownloadManager* pDM = DownloadManager::Ptr();
	std::set<ResourceType> setDownloadResourceTypes; // To only cancel once per type.
	for (const auto& pr : m_mapActiveDownloads) {
		IResource* pResource = pr.first;
		ResourceType resourceType = pResource->GetResourceType();
		if (pDM) {
			if (setDownloadResourceTypes.insert(resourceType).second)
				pDM->CancelJobsByType(resourceType, false);
			DownloadManager::PtrCE()->GetDispatcher()->CancelEvent(pr.second.get());
		}
		VerifyChangeState(pResource, eQueuedState::WAITING_DOWNLOAD, eQueuedState::NOT_QUEUED);
	}

	// Clear list of pending downloads
	m_mapActiveDownloads.clear();

	// Cancel pending reads
	for (const auto& pOL : m_apOverlapped)
		CancelIoEx(pOL->m_hFile.GetHandle(), pOL.get());
	m_apOverlapped.clear();

	// Clear list of pending reads
	for (IResource* pResource : m_setActiveReads)
		VerifyChangeState(pResource, eQueuedState::WAITING_READ, eQueuedState::NOT_QUEUED);
	m_setActiveReads.clear();

	// Clear queues
	for (IResource* pResource : m_apNeedProcess)
		ChangeState(pResource, eQueuedState::IN_PREPROCESS_QUEUE, eQueuedState::NOT_QUEUED);
	m_apNeedProcess.clear();
	m_qNeedDownload.DequeueAll(eQueuedState::NOT_QUEUED);
	m_qNeedRead.DequeueAll(eQueuedState::NOT_QUEUED);

	return true;
}

void ResourceLoader::LoadThreadData::LoadThreadProc(size_t iThread) {
	SetThreadName(StreamToString("ResourceLoad ", iThread).c_str());
	while (true) {
		IResource* pResource = DequeueNext();
		if (!pResource)
			break;

		if (pResource->m_ResourceManagerData.GetState() == eQueuedState::WAITING_DOWNLOAD) {
			if (!StartDownload(pResource)) {
				// Download is not pending. Could have errored or been added back to download queue.
			}
		} else if (pResource->m_ResourceManagerData.GetState() == eQueuedState::WAITING_READ) {
			if (!StartReading(pResource)) {
				// Read is not pending. Could have errored or been added back to download queue.
			}
		} else {
			assert(false); // DequeueNext should have returned a resource waiting to be downloaded or read.
			Errored(pResource, IResource::Error::READ_FAILURE);
			LogError("Unexpected state when trying to load resource " << pResource->GetFilePath() << " state = " << static_cast<int>(pResource->m_ResourceManagerData.GetState()));
		}
	}
}

IResource* ResourceLoader::LoadThreadData::ReadCallback(DWORD error, DWORD nBytesRead, OverlappedReadResult* pOverlapped) {
	IResource* pResource = pOverlapped->m_pResource;
	if (!pResource) {
		assert(false); // We should always populate the resource pointer.
		return nullptr;
	}
	VerifyState(pResource, eQueuedState::WAITING_READ);
	//LogInfo("ReadCallback for " << pResource->GetFilePath());

	DWORD dwBytesRead = 0;
	BOOL bGetOverlappedResult = GetOverlappedResult(pOverlapped->m_hFile.GetHandle(), pOverlapped, &dwBytesRead, FALSE);
	pOverlapped->m_hFile.Close(); //  Close file as soon as we're done with it.

	{
		std::lock_guard<decltype(m_mtx)> lock(m_mtx);
		if (m_setActiveReads.erase(pResource) != 1) {
			LogError("ReadCallback received for resource that is not being read: " << pResource->GetFileName());
			Errored(pResource, IResource::Error::LOAD_ERROR);
			return nullptr;
		}

		VerifyChangeState(pResource, eQueuedState::WAITING_READ, eQueuedState::PROCESSING_READ);

		// Check for read errors.
		if (error != 0 || !bGetOverlappedResult || dwBytesRead != pResource->GetRawSize()) {
			DWORD dwLastError = GetLastError();
			LogError("Overlapped read error for resource file " << pResource->GetFileName() << ". Error code = " << error << " Last error = " << dwLastError << " Bytes read = " << dwBytesRead);

			// Delete Resource Raw Buffer & File & Re-read
			pResource->FreeRawBuffer();
			if (!m_qNeedRead.Enqueue(pResource, eQueuedState::PROCESSING_READ))
				assert(false); // unexpected state.
			return nullptr;
		}

		// Verify File Integrity
		if (!ResourceManager::VerifyFileIntegrity(pResource)) {
			LogError(" VerifyFileIntegrity() FAILED - '" << pResource->GetFilePath() << "'");

			// Delete Resource Raw Buffer & File & Re-Download
			pResource->FreeRawBuffer();
			DeleteResourceFile(pResource);
			if (!m_qNeedDownload.Enqueue(pResource, eQueuedState::PROCESSING_READ))
				assert(false); // unexpected state.
			return nullptr;
		}
	}

	return pResource; // Return the resource to be created.
}

void WINAPI ResourceLoader::LoadThreadData::ReadCallbackStatic(DWORD error, DWORD nBytesRead, LPOVERLAPPED lpOverlapped) {
	if (!lpOverlapped) {
		assert(false); // Our overlapped structure was lost.
		return;
	}

	LoadThreadData* pLoadThreadData = ResourceLoader::Instance().m_pLoadThreadData.get();
	std::unique_ptr<OverlappedReadResult> pOL = pLoadThreadData->GetOverlapped(lpOverlapped);
	IResource* pResourceToCreate = pLoadThreadData->ReadCallback(error, nBytesRead, pOL.get());
	if (pResourceToCreate)
		ResourceLoader::Instance().m_pCreateThreadData->Add(pResourceToCreate);
	pLoadThreadData->m_condVar.notify_one(); // We can start another read.
}

IResource* ResourceLoader::LoadThreadData::DequeueNext() {
	IResource* pResource = nullptr;

	{
		std::unique_lock<decltype(m_mtx)> lock(m_mtx);
		while (true) {
			pResource = nullptr;
			if (m_bTerminate)
				break;

			// Preprocess newly added resources.
			if (!m_apNeedProcess.empty()) {
				std::vector<IResource*> apNeedProcess = GetNeedProcess_WithLock();

				// Process incoming IResources without holding lock.
				std::vector<IResource*> apNeedDownload;
				std::vector<IResource*> apNeedRead;
				{
					lock.unlock();
					Preprocess_WithoutLock(apNeedProcess, out(apNeedDownload), out(apNeedRead));
					lock.lock();
				}

				EnqueuePreprocessed_WithLock(apNeedDownload, apNeedRead);
			} else {
				// Return a resource to download if available.
				if (m_mapActiveDownloads.size() < knMaxConcurrentDownloads) {
					pResource = m_qNeedDownload.Dequeue();
					if (pResource) {
						//LogInfo("Downloading resource type " << (int)pResource->GetResourceType() << ": " << pResource->GetFilePath());
						if (m_mapActiveDownloads.insert(std::make_pair(pResource, nullptr)).second)
							break;
						LogError("Putting resource in download queue that is already there: " << pResource->GetFileName());
						assert(false);
					}
				}

				// Return a resource to read if available.
				if (m_setActiveReads.size() < knMaxConcurrentReads) {
					pResource = m_qNeedRead.Dequeue();
					if (pResource) {
						//LogInfo("Reading resource type " << (int)pResource->GetResourceType() << ": " << pResource->GetFilePath() << " glid: " << pResource->GetGlid());
						if (m_setActiveReads.insert(pResource).second)
							break;
						LogError("Putting resource in read queue that is already there: " << pResource->GetFileName());
						assert(false);
					}
				}

				// Wait for another resource to be ready
				m_condVar.wait(lock);
			}
		}
	}

	if (pResource)
		m_condVar.notify_one();
	return pResource;
}

FileHandleWrapper ResourceLoader::LoadThreadData::OpenFile(IResource* pResource, Out<size_t> fileSizeOut) {
	std::string strFilePath = pResource->GetFilePath();

	FileHandleWrapper hFile(CreateFile(
		Utf8ToUtf16(strFilePath).c_str(), // file
		GENERIC_READ, // open for reading
		FILE_SHARE_READ | FILE_SHARE_DELETE, // share for reading and delete (conflict with CFile::Remove)
		nullptr, // no security
		OPEN_EXISTING, // existing file only
		FILE_FLAG_OVERLAPPED, // async reading
		nullptr)); // no attr. template

	size_t fileSize = hFile ? GetFileSize(hFile.GetHandle(), nullptr) : 0;
	fileSizeOut.set(fileSize);
	if (hFile) {
		size_t expectedSize = pResource->GetExpectedSize();
		if (expectedSize != 0 && expectedSize != fileSize) {
			LogError(/*m_id <<*/ " UNEXPECTED FILE SIZE (" << fileSize << " != " << expectedSize << ") '" << strFilePath << "'");

			//pResource->FreeRawBuffer();
			DeleteResourceFile(pResource);
			pResource->SetExpectedSize(0); // prevent infinite loop if expected size source file is incorrect.
			hFile.Close();
		}
	}
	return hFile;
}

// Return value indicates whether read is in progress, not whether an error occurred.
bool ResourceLoader::LoadThreadData::StartReading(IResource* pResource) {
	VerifyState(pResource, eQueuedState::WAITING_READ);

	bool bRedownload = false;
	size_t fileSize = 0;
	FileHandleWrapper hFile = OpenFile(pResource, out(fileSize));
	if (hFile) {
		OverlappedReadResult* pOverlapped = AllocateOverlapped(std::move(hFile), pResource);

		// (Re)Allocate Resource Raw Buffer
		pResource->SetRawBuffer(std::make_unique<BYTE[]>(fileSize), fileSize);

		// Start async read.
		if (ReadFileEx(pOverlapped->m_hFile.GetHandle(), pResource->GetRawBuffer(), fileSize, pOverlapped, ReadCallbackStatic))
			return true;

		LogError(/*m_id <<*/ " ReadFileEx() FAILED - '" << pResource->GetFilePath() << "'");

		// Since we won't be getting a callback, free the allocated OverlappedReadResult.
		std::unique_ptr<OverlappedReadResult> pUniqueOverlapped = GetOverlapped(pOverlapped);
	}

	// This resource is not being actively read, so remove it from the set.
	{
		std::lock_guard<decltype(m_mtx)> lock(m_mtx);
		if (m_setActiveReads.erase(pResource) != 1)
			assert(false);
	}

	// Download file again. Override expected state because we're going back to an earlier state.
	if (bRedownload)
		AddToDownloadQueue(pResource, eQueuedState::WAITING_READ);
	else
		Errored(pResource, IResource::Error::READ_FAILURE);

	return false;
}

std::vector<IResource*> ResourceLoader::LoadThreadData::GetNeedProcess_WithLock() {
	std::vector<IResource*> apNeedProcess;
	m_apNeedProcess.swap(apNeedProcess);
	for (IResource* pResource : apNeedProcess)
		VerifyChangeState(pResource, eQueuedState::IN_PREPROCESS_QUEUE, eQueuedState::PREPROCESSING);
	return apNeedProcess;
}

void ResourceLoader::LoadThreadData::Preprocess_WithoutLock(const std::vector<IResource*>& apNeedProcess, Out<std::vector<IResource*>> apNeedDownload, Out<std::vector<IResource*>> apNeedRead) {
	for (IResource* pResource : apNeedProcess) {
		VerifyState(pResource, eQueuedState::PREPROCESSING);
		std::string filePath = pResource->GetFilePath();
		//LogInfo("Preprocessing resource: " << filePath);

		if (FileHelper::Exists(filePath))
			apNeedRead.get().push_back(pResource);
		else
			apNeedDownload.get().push_back(pResource);
	}
}

void ResourceLoader::LoadThreadData::EnqueuePreprocessed_WithLock(const std::vector<IResource*>& apNeedDownload, const std::vector<IResource*>& apNeedRead) {
	for (IResource* pResource : apNeedDownload) {
		VerifyState(pResource, eQueuedState::PREPROCESSING);
		m_qNeedDownload.Enqueue(pResource);
	}
	for (IResource* pResource : apNeedRead) {
		VerifyState(pResource, eQueuedState::PREPROCESSING);
		m_qNeedRead.Enqueue(pResource, eQueuedState::PREPROCESSING);
	}
}

ResourceLoader::LoadThreadData::OverlappedReadResult* ResourceLoader::LoadThreadData::AllocateOverlapped(FileHandleWrapper hFile, IResource* pResource) {
	std::lock_guard<std::mutex> lock(m_mtxOverlapped);
	m_apOverlapped.push_back(std::make_unique<OverlappedReadResult>(std::move(hFile), pResource));
	return m_apOverlapped.back().get();
}

std::unique_ptr<ResourceLoader::LoadThreadData::OverlappedReadResult> ResourceLoader::LoadThreadData::GetOverlapped(OVERLAPPED* pOverlapped_) {
	OverlappedReadResult* pOverlapped = static_cast<OverlappedReadResult*>(pOverlapped_);
	std::unique_ptr<OverlappedReadResult> ret;
	std::lock_guard<std::mutex> lock(m_mtxOverlapped);
	for (auto itr = m_apOverlapped.begin(); itr != m_apOverlapped.end(); ++itr) {
		if (itr->get() == pOverlapped) {
			ret = std::move(*itr);
			m_apOverlapped.erase(itr);
			break;
		}
	}
	return ret;
}

bool ResourceLoader::CreateThreadData::Init(size_t nThreads) {
	if (!m_aCreateThreads.empty())
		return false; // Currently running.

	m_bTerminate = false;
	for (size_t i = 0; i < nThreads; ++i)
		m_aCreateThreads.push_back(std::thread([i, this]() { this->CreateThreadProc(i); }));
	return true;
}

void ResourceLoader::CreateThreadData::Wake() {
	m_condVar.notify_one();
}

void ResourceLoader::CreateThreadData::Add(IResource* pResource) {
	VerifyState(pResource, eQueuedState::PROCESSING_READ);
	{
		std::lock_guard<std::mutex> lock(m_mtx);
		m_qNeedCreate.Enqueue(pResource);
	}
	m_condVar.notify_one();
}

IResource* ResourceLoader::CreateThreadData::DequeueNext(size_t iThread) {
	std::unique_lock<std::mutex> lock(m_mtx);
	while (true) {
		if (m_bTerminate)
			return nullptr;

		IResource* pResource = m_qNeedCreate.Dequeue();
		if (pResource)
			return pResource;

		// Wait for condition variable to be notified.
		// If we suspended some resources (i.e. textures) set a timeout for the retry.
		// Only use timeout for one of the threads. An external timer system would be better.
		if (iThread != 0 || m_setResourcesSuspended.empty()) {
			m_condVar.wait(lock);
		} else {
			if (m_condVar.wait_for(lock, std::chrono::seconds(1)) == std::cv_status::timeout) {
				std::set<ResourceManager*> setResourcesSuspended;
				setResourcesSuspended.swap(m_setResourcesSuspended);
				{
					// For safety, unlock mutex while we disable SuspendLoad on the resource managers.
					lock.unlock();
					for (ResourceManager* pRM : setResourcesSuspended)
						pRM->SuspendLoad(false);
					lock.lock();
				}
			}
		}
	}
}

void ResourceLoader::CreateThreadData::SetTerminate() {
	{
		std::lock_guard<std::mutex> lock(m_mtx);
		m_bTerminate = true;
	}
	m_condVar.notify_all();
}
void ResourceLoader::CreateThreadData::WaitTerminate() {
	for (std::thread& thread : m_aCreateThreads)
		thread.join();
	m_aCreateThreads.clear();
}
void ResourceLoader::CreateThreadData::ClearQueues() {
	std::lock_guard<std::mutex> lock(m_mtx);
	m_qNeedCreate.DequeueAll(eQueuedState::NOT_QUEUED);
}

void ResourceLoader::CreateThreadData::CreateThreadProc(size_t iThread) {
	SetThreadName(StreamToString("ResourceCreate ", iThread).c_str());
	while (true) {
		IResource* pResource = DequeueNext(iThread);
		if (!pResource)
			break;
		//LogInfo("Creating resource type " << (int)pResource->GetResourceType() << ": " << pResource->GetFilePath() << " glid: " << pResource->GetGlid());

		VerifyState(pResource, eQueuedState::PROCESSING_CREATE);
		eCreateResourceResult res = DoCreateResource(pResource);
		if (res == eCreateResourceResult::RETRY) {
			ResourceManager* pRM = pResource->GetResourceManager();

			std::lock_guard<std::mutex> lock(m_mtx);
			if (m_setResourcesSuspended.insert(pRM).second) {
				pRM->SuspendLoad(true);
				m_qNeedCreate.Enqueue(pResource, eQueuedState::PROCESSING_CREATE);
			}
		}
	}
}

eCreateResourceResult ResourceLoader::CreateThreadData::DoCreateResource(IResource* pResource) {
	VerifyState(pResource, eQueuedState::PROCESSING_CREATE);

	std::string filePath = pResource->GetFilePath();
	// Is Resource Encrypted ?
	bool isKRX = pResource->IsKRXEncrypted();
	if (isKRX) {
		// Decrypt
		std::string method;
		std::unique_ptr<BYTE[]> out_buf;
		auto rawBuffer = pResource->GetRawBuffer();
		auto rawSize = pResource->GetRawSize();
		method = "KRX";
		int krxHashLen = 0;
		unsigned char* krxHash = pResource->GetEncryptionHashOverride(krxHashLen);
		out_buf.reset(KRXDecrypt(rawBuffer, rawSize, KRX_SALT, krxHash, krxHashLen));

		// Decrypt Failure ?
		if (!out_buf) {
			LogError(method << "Decrypt() FAILED - '" << filePath << "'");
			Errored(pResource, IResource::Error::READ_FAILURE);
			return eCreateResourceResult::FAIL;
		}

		// Re-Assign Resource Raw Buffer With Decrypted Data
		pResource->SetRawBuffer(std::move(out_buf), rawSize);
	}

	// Attempt Device Create Resource
	// DRF - WARNING - Minimized client fails this for texture creation in DxDevice. Since the
	// TextureRM runs on a separate thread the simple solution was to sleep in CreateResource()
	// until the client is again in view and try again.  If this fails it is very difficult to
	// recover and best to just invalidate the resource.
	eCreateResourceResult createResult = eCreateResourceResult::FAIL;
	try {
		createResult = pResource->CreateResourceEx();
	} catch (CArchiveException* e) {
		wchar_t errMsg[MAX_PATH] = L"";
		e->GetErrorMessage(errMsg, _countof(errMsg));
		LogError(" Caught CArchiveException '" << filePath << "' " << Utf16ToUtf8(errMsg));
		e->Delete();
	} catch (CException* e) {
		wchar_t errMsg[MAX_PATH];
		e->GetErrorMessage(errMsg, _countof(errMsg));
		LogError(" Caught CException '" << filePath << "' " << Utf16ToUtf8(errMsg));
		e->Delete();
	} catch (...) {
		LogError(" Caught Exception '" << filePath << "'");
	}

	switch (createResult) {
		default:
			assert(false);
			createResult = eCreateResourceResult::FAIL;
			// Fall through to FAIL case.
		case eCreateResourceResult::FAIL:
			// WARNING - We already verified file integrity if we got this far so this will only
			// cause an infinite loop.  If something got this far and failed it is probably just an
			// invalid or bad resource.
			LogError(pResource->GetResourceManager()->m_id << " CreateResource() FAILED - '" << filePath << "'");
			Errored(pResource, IResource::Error::CREATE_FAILURE);
			break;

		case eCreateResourceResult::SUCCESS:
			pResource->SetState(IResource::State::LOADED_AND_CREATED);
			ResourceFinishedLoading(pResource, eQueuedState::PROCESSING_CREATE);
			break;

		case eCreateResourceResult::RETRY:
			break;
	}

	return createResult;
}

template <IResource::eQueuedState stateInQueue>
IResource* ResourceLoader::ResourceQueue<stateInQueue>::Dequeue() {
	std::lock_guard<fast_mutex> lock(m_mtxResources);

	for (auto& pr : m_mapResources) {
		auto& listResources = pr.second;
		if (listResources.empty())
			continue;

		ResourceManager* pRM = pr.first;
		if (pRM->IsLoadSuspended())
			continue;

		// Just do a linear walk on the resources looking for the one
		// with the highest priority
		auto itrHighest = listResources.begin();
		int iHighestPriority = (*itrHighest)->GetDownloadPriority();
		for (auto itr = itrHighest; itr != listResources.end(); ++itr) {
			IResource* pResource = *itr;
			int iPriority = pResource->GetDownloadPriority();
			if (iPriority > iHighestPriority) {
				iHighestPriority = iPriority;
				itrHighest = itr;
			}
		}

		// DRF - Added
		//if( iHighestPriority == DL_PRIO_NO )
		//	return nullptr;

		// Get the one with the highest priority to return it
		IResource* pResult = *itrHighest;

		// Erase element
		listResources.erase(itrHighest);

		// Reset resource queued state since it's no longer queued
		VerifyChangeState(pResult, stateInQueue, stateAfterQueue);
		return pResult;
	}

	return nullptr;
}

} // namespace KEP
