///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "..\..\..\RenderEngine\ReMeshCache.h"
#include "ResourceManager.h"
#include "..\Common\include\CoreStatic\IResource.h"
#include "ItemTextureInfo.h"

namespace KEP {

class CBoneList;
class CDeformableMesh;
class CDeformableVisObj;
class CDynamicPlacementObj;
class CEXMeshObj;
class CHiarcleVisObj;
class CMaterialObject;
class CReferenceObj;
class CWldObject;
class MeshProxy;
class ReD3DX9DeviceState;
class ReMesh;
class ReMeshPool;
class ReMeshCache;
class ReSkinMesh;

class MeshRM : public TypedResourceManager<MeshProxy> {
public:
	/// RenderMesh Pool Identifiers (serialized - do not re-number)
	enum RMP_ID {
		RMP_ID_PERSISTENT = 0, /// persistent skinned meshes (.edb)
		RMP_ID_LIBRARY = 1, /// retained library meshes (.rlb)
		RMP_ID_LEVEL = 2, /// transient level meshes (.exg)
		RMP_ID_DYNAMIC = 3, /// persistent dynamic obj meshes (.dol)
		RMP_IDS = 4,
		// MIXED POOL - NOT ARRAY!
		RMP_ID_MIXED = 5 /// mixed dynamic obj meshes
	};

	/// RenderMesh Cache Identifiers (serialized - do not re-number)
	enum RMC_ID {
		RMC_ID_0 = 0, /// opaque meshes static
		RMC_ID_1 = 1, /// 1-bit alpha meshes static
		RMC_ID_2 = 2, /// multibit alpha transparent meshes static
		RMC_ID_3 = 3, /// fixed function pipe (uv scrolling) meshes static
		RMC_ID_4 = 4, /// multitextured meshes static
		RMC_ID_5 = 5, /// meshes skinned
		RMC_ID_6 = 6, /// multitextured meshes skinned
		RMC_ID_7 = 7, /// sort-order material meshes
		RMC_ID_8 = 8, /// widgets/3D overlays
		RMC_IDS = 9
	};

	// TODO: remove this
	static MeshRM* Instance() { return static_cast<MeshRM*>(TypedResourceManager<MeshProxy>::Instance()); }

	static Vector3f ORIGIN;

public:
	MeshRM(const std::string& id);

	virtual ~MeshRM() {}

public:
	virtual BOOL Initialize(UINT maxMeshes = 16384);

	/*!
	 * \brief
	 * Get the MeshProxy.
	 *
	 * \param hash
	 * The Mesh's hash.
	 *
	 * \param pool
	 * The mesh pool that should be searched for the mesh
	 *
	 * \returns
	 * The MeshProxy, which should not be deleted.
	 *
	 * This function does not cause the mesh to be loaded, it only returns
	 * the IResource that represents the data.  It's probably better if
	 * CMesh was actually a smart pointer, because it's managed by the
	 * ResourceManager and shouldn't be deleted by the caller.
	 */
	virtual MeshProxy* GetMeshProxy(UINT64 hash);

	/*!
	 * Return the mesh data directly from the render engine.
	 *
	 * I'm not sure that this should be a public function.  This function is returning what amounts to
	 * internal render engine state that isn't a clone.
	 */
	virtual ReMesh* FindMesh(UINT64 hash, RMP_ID rmpId) {
		std::lock_guard<fast_mutex> lock(m_mtxPool);
		size_t i = (size_t)rmpId;
		return (i < _countof(m_MeshPool)) ? m_MeshPool[i]->FindMesh(hash) : NULL;
	}

	virtual std::unique_ptr<ReMesh> CloneMesh(UINT64 hash, RMP_ID rmpId) {
		std::unique_ptr<ReMesh> pMeshClone;
		ReMesh* pMesh = FindMesh(hash, rmpId);
		if (pMesh) {
			ReMesh* pMeshCloneRaw = nullptr;
			pMesh->Clone(&pMeshCloneRaw);
			pMeshClone.reset(pMeshCloneRaw);
		}
		return pMeshClone;
	}

	//
	// store and load meshes are used during serialization of objects that contain meshes.  It's not at all clear that
	// this is a good interface.
	//
	/*!
	 * Store all the meshes in a single archive.
	 */
	virtual bool StoreReMeshes(ReMesh** meshes, UINT num, CArchive& ar, RMP_ID rmpId);

	/*!
	 * Store each mesh in its own archive (a series of loose meshes). Skip writing if already exists.
	 */
	virtual bool StoreReMeshes(ReSkinMesh** meshes, UINT num);

	/*!
	 * \brief
	 * This function reads the archive written to in ReMeshCreate::StoreReMeshes()
	 *
	 * \param ar
	 * The archive to read
	 *
	 * \param dev
	 * Device state that can be got from IClientEngine
	 *
	 * \param pool
	 * The pool to load the meshes into.
	 *
	 * Write detailed description for LoadReMeshes here.
	 */
	virtual void LoadReMeshes(CArchive& ar, ReD3DX9DeviceState* dev, RMP_ID rmpId = RMP_ID_PERSISTENT);

	/*!
	 * Add a ReMaterial to the CMaterialObject
	 */
	void CreateReMaterial(CMaterialObject* mat, const std::string& subDir = "", const TextureColorMap* altColorMap = nullptr);

	virtual void DestroyMeshPools() {
		std::lock_guard<fast_mutex> lock(m_mtxPool);
		for (size_t i = 0; i < _countof(m_MeshPool); ++i) {
			if (m_MeshPool[i])
				m_MeshPool[i]->Destroy();
		}
	}

	virtual void ResetMeshPool(RMP_ID rmpId) {
		std::lock_guard<fast_mutex> lock(m_mtxPool);
		size_t i = (size_t)rmpId;
		if (i < _countof(m_MeshPool))
			m_MeshPool[i]->Reset();
	}

	/*!
	 * Return a pointer to the internal mesh cache.
	 * GetMeshCache is used in cskeletonclass to organize the meshe cache, e.g. to sort meshes
	 * by material.  GetMeshCache is called, followed by AddMeshes on the mesh cache itself.
	 * This is used internally and by CSkeletonClass, which is an issue IMO.
	 */
	virtual ReMeshCache* GetMeshCache(RMC_ID rmcId) {
		size_t i = (size_t)rmcId;
		return (i < _countof(m_MeshCache)) ? m_MeshCache[i] : NULL;
	}

	virtual ReMesh* AddMesh(ReMesh* mesh, RMP_ID rmpId);

	virtual ReMesh* CreateReStaticMesh(const ReMeshData* pDesc, bool strip = true);
	virtual ReMesh* CreateReStaticMesh(CEXMeshObj* meshObj, const Vector3f& pivot = ORIGIN, const char* subDir = NULL);

	// Create ReStaticMesh with buffer allocated
	virtual ReMesh* AllocateReStaticMesh(const std::vector<std::pair<unsigned, unsigned>>& vertexAndIndexCounts, unsigned uvCount, bool triStrip);

	virtual void AppendEXMesh(ReMesh* mesh, CEXMeshObj* pEXMesh);

	virtual void BuildReMesh(ReMesh* mesh, bool convertToTriStrip = true);

	virtual void CreateReMaterials(CDeformableMesh* dMesh, const char* subDir = "CharacterTextures");

	/// This is called by a depicated import function CDeformableVisObj::DoOldImport()
	virtual ReMesh* CreateReSkinMeshPersistent(CDeformableVisObj* visObj, bool bRecalcVertexFromOffsetVectors = true, RMP_ID rmpId = RMP_ID_PERSISTENT);

	virtual ABBOX ComputeBoundingBoxWithMatrix(ReMesh* mesh, const Matrix44f* pMatrix);

	bool LoadMeshCache(ReMesh* mesh);
	bool LoadMeshCache(ReMesh* mesh, RMC_ID rmcId);
	bool LoadMeshCache(ReMesh** meshes, DWORD num, RMC_ID rmcId);

	virtual void ResetMeshCaches(ReD3DX9DeviceState* dev) {
		m_pDev = dev;
		for (size_t i = 0; i < _countof(m_MeshCache); ++i)
			m_MeshCache[i]->Reset();
	}

	virtual void RenderMeshCache(RMC_ID rmcId, bool overrideLighting);

	virtual const CBoneList* GetCurrentBoneList() {
		return m_CurrentBoneList;
	}
	virtual void SetCurrentBoneList(const CBoneList* boneList) {
		m_CurrentBoneList = boneList;
	}
	virtual DWORD GetCurrentLOD() {
		return m_CurrentLOD;
	}
	virtual void SetCurrentLOD(DWORD lod) {
		m_CurrentLOD = lod;
	}
	//virtual void				SetProgressiveLoadFlag(DWORD flag)					{
	//	m_LoadProgressively = flag;
	//}
	virtual DWORD GetProgressiveLoadFlag() {
		return m_LoadProgressively;
	}

protected:
	//! \brief Return a path pointing to where resource should be stored locally
	//! \param gameFilesDir a path pointed to "GameFiles" folder of current game
	//@@Assumption: caller must pass in a path pointed to "GameFiles" folder of current game.
	virtual std::string GetResourceLocalStore(const std::string& gameFilesDir) override;

	//! \brief Compose a file name for serialization based on hash code provided.
	//! \param hash Hash code for the resource to be serialized.
	virtual std::string GetResourceFileName(UINT64 hash) override;

	virtual std::string GetResourceFilePath(UINT64 hash) override;

	virtual std::string GetResourceURL(UINT64 hash) override;

	virtual bool IsUGCItem(UINT64 /*hash*/) const override {
		return false;
	}

private:
	fast_mutex m_mtxPool;
	ReD3DX9DeviceState* m_pDev;
	ReMeshPool* m_MeshPool[RMP_IDS];
	ReMeshCache* m_MeshCache[RMC_IDS];
	const CBoneList* m_CurrentBoneList;
	DWORD m_CurrentLOD;
	BOOL m_LoadProgressively;
	UINT m_maxMeshes;
};

} // namespace KEP