///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "IResource.h"
#include "ResourceType.h"

namespace KEP {

class AnimationRM;
class CBoneAnimationObject;
class CSkeletonAnimation;
class SkeletonDef;
template <typename T>
class TypedResourceManager;
class IMemSizeGadget;

class AnimationProxy : public IResource {
public:
	const static ResourceType _ResType = ResourceType::ANIMATION;

	struct AnimationData {
		UINT m_version;
		UINT m_numBones;
		TimeMs m_animDurationMs;
		UINT m_animLooping;
		CBoneAnimationObject** m_ppBoneAnimationObjs;
		SkeletonDef* m_skeletonDef;

		AnimationData() {
			Reset();
		}

		void Reset() {
			m_version = 0;
			m_numBones = 0;
			m_animDurationMs = (TimeMs)0.0;
			m_animLooping = (UINT)-1;
			m_ppBoneAnimationObjs = nullptr;
			m_skeletonDef = nullptr;
		}

		void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;
	};

protected:
	// protected constructor to be accessed by corresponding RM class only
	AnimationProxy(ResourceManager* pAnimRM, UINT64 resKey, bool isLocal);
	AnimationProxy(const AnimationProxy& rhs) = delete;

	virtual bool IsSerializedDataCompressible() const override {
		return true;
	}

public:
	/// This exists so that I can convert files in the ANM
	/// archive into loose animation files.
	AnimationProxy(AnimationRM* pAnimRM, CSkeletonAnimation* pSA);

	virtual ~AnimationProxy();

	virtual std::string ToStr() const override {
		std::string str;
		StrBuild(str, "AP<" << this << ">");
		return str;
	}

	virtual bool CreateResource() override;

	virtual BYTE* GetSerializedData(FileSize& dataLen) override;

	void Write(CArchive& ar) const;

	bool Read(CArchive& ar);
	bool ReadLegacy(CArchive& ar);

	bool Validate(const AnimationData& data);

	BOOL EqualTo(AnimationProxy* pAP) const {
		return GetHashKey() == pAP->GetHashKey();
	}

	const AnimationData* GetAnimData(bool bImmediate = false);
	const AnimationData* GetAnimDataPtr() const;

	bool HasAnimData() const;

	void BakeEditingAttributes(float speedFactor, TimeMs cropStart, TimeMs cropEnd, BOOL looping);

	UINT GetAnimLooping() const {
		return m_animationData.m_animLooping;
	}
	void SetAnimLooping(UINT looping) {
		m_animationData.m_animLooping = looping;
	}

	TimeMs GetAnimDurationMs() {
		return m_animationData.m_animDurationMs;
	}
	void SetAnimDurationMs(TimeMs timeMs) {
		m_animationData.m_animDurationMs = timeMs;
	}

	void StartLoad();

	void GetMemSize(IMemSizeGadget* pMemSizeGadget) const;

private:
	void Init();

	bool LoadFromSkeletonAnimation(const CSkeletonAnimation* pSA);

	/// This data is only valid if GetData != 0.
	AnimationData m_animationData;

	friend class AnimationRM;
	friend class TypedResourceManager<AnimationProxy>;
};

} // namespace KEP
