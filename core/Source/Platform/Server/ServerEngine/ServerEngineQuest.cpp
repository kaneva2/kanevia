///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common/include/kepcommon.h"
#include "ServerEngine.h"

#include "CCommerceClass.h"
#include "CBankClass.h"
#include "CSafeZones.h"
#include "CSecureTradeClass.h"
#if (DRF_OLD_PORTALS == 1)
#include "CPortalSysClass.h"
#endif
#include "CElementsClass.h"
#include "CCollisionClass.h"
#include "CGmPageClass.h"
#include "CWorldChannelRules.h"
#include "CBattleSchedulerClass.h"
#if (DRF_OLD_VENDORS == 1)
#include "CServerItemGen.h"
#endif
#include "CServerItemClass.h"
#include "CAccountClass.h"
#include "CAIICollectionQuest.h"
#include "CQuestJournal.h"
#include "CCurrencyClass.h"
#include "CInventoryClass.h"
#include "CPlayerClass.h"
#include "CSkillClass.h"
#include "CMultiplayerClass.h"
#include "LocaleStrings.h"

namespace KEP {

BOOL ServerEngine::HandleQuestSys(
	CPlayerObject *pPO,
	CAICollectionQuestObj *questPtr,
	CServerItemObj *svrPtr,
	NETID idFromLoc,
	int &questSelection) {
	float expGained = 0.0f;
	if (!pPO || !questPtr || !svrPtr)
		return FALSE;

	if (questPtr->m_reward && questPtr->m_reward->GetCount()) {
		if (pPO->m_inventory) {
			if (!pPO->m_inventory->CanTakeObjects(m_multiplayerObj->m_maxItemLimit_onChar, 1)) { // overloaded
				RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::Talk, LS_QUEST_NO_INVENT_ROOM, svrPtr->m_displayedName);
				return FALSE;
			} // end overloaded
		}
	}

	if (questPtr->m_requiredSkillLevel > 0) {
		if (pPO->playerOrigRef()) { // EVALUATE SKILL/LEVE REQ
			CSkillObject *sklRefPtr = pPO->playerOrigRef()->m_skills->GetSkillByType(questPtr->m_requiredSkillType);
			if (sklRefPtr) { // have the skill
				CStringA titleTemp;
				int curLevel = 1;

				CSkillObject *skillPtrRef = m_skillDatabase->GetSkillByType(sklRefPtr->m_skillType);
				if (skillPtrRef) {
					sklRefPtr->EvaluateLevelAndTitle(titleTemp, curLevel, skillPtrRef);
					if (curLevel < questPtr->m_requiredSkillLevel) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::Talk, LS_QUEST_MIN_LEVEL,
							svrPtr->m_displayedName,
							StringHelpers::parseNumber(questPtr->m_requiredSkillLevel).c_str(),
							skillPtrRef->m_skillName);
						return FALSE;
					}
				}
			} // end have a skill
			else { // dont have the skill at all
				sklRefPtr = m_skillDatabase->GetSkillByType(questPtr->m_requiredSkillType);
				if (sklRefPtr) { // known skill
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::Talk, LS_QUEST_MIN_LEVEL,
						svrPtr->m_displayedName,
						StringHelpers::parseNumber(questPtr->m_requiredSkillLevel).c_str(),
						sklRefPtr->m_skillName);

					return FALSE;
				} // end skill know skill
				else { // unknown skill
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::Talk, LS_QUEST_MIN_LEVEL_UNKNOWN);

					return FALSE;
				} // end unknown skill
			} // end dont have the skill at all
		} // END EVALUATE SKILL LEVEL REQ
	}

	if (questPtr->EvaluateQuest(pPO->m_inventory, pPO->m_skills, m_skillDatabase, &expGained)) {
		pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY; // in case they had change

		// success
		if (questPtr->m_completedKeyEnabled > 0) {
			if (pPO->playerOrigRef()) { // quest log
				if (!pPO->playerOrigRef()->m_questJournal) {
					pPO->playerOrigRef()->m_questJournal = new CQuestJournalList();
				}
				if (questPtr->m_completedKeyEnabled == 2) { // keep quest uncomplete show only hint
					pPO->playerOrigRef()->m_questJournal->LogQuest(questPtr->m_title,
						NULL,
						svrPtr->m_displayedName,
						questPtr->m_optionalIntruction,
						m_globalInventoryDB,
						FALSE,
						questSelection,
						questPtr->m_completedKeyEnabled,
						questPtr->m_mobToHuntID,
						questPtr->m_maxDropCount,
						questPtr->m_dropGlid,
						questPtr->m_dropPercentage,
						questPtr->m_autocomplete,
						questPtr->m_skillTypeBonus,
						questPtr->m_expBonus,
						questPtr->m_maxExpLimit,
						questPtr->m_progessBasedByQuestJournal,
						questPtr->m_undeletable,
						questPtr->m_closeJournalEntryByName);
				} // end keep quest uncomplete show only hint
				else { // normal success
					pPO->playerOrigRef()->m_questJournal->LogQuest(questPtr->m_title,
						questPtr->m_requiredItems,
						svrPtr->m_displayedName,
						questPtr->m_optionalIntruction,
						m_globalInventoryDB,
						TRUE,
						questSelection,
						questPtr->m_completedKeyEnabled,
						questPtr->m_mobToHuntID,
						questPtr->m_maxDropCount,
						questPtr->m_dropGlid,
						questPtr->m_dropPercentage,
						questPtr->m_autocomplete,
						questPtr->m_skillTypeBonus,
						questPtr->m_expBonus,
						questPtr->m_maxExpLimit,
						questPtr->m_progessBasedByQuestJournal,
						questPtr->m_undeletable,
						questPtr->m_closeJournalEntryByName);
				} // end normal success

				pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY | PlayerMask::SKILLS | PlayerMask::QUESTS;
				pPO->m_timeDbUpdate = TIME_UNDEFINED; // move up in queue
			} // end quest log
		}

		if (questPtr->m_cashReward > 0) { // cash reward
			if (pPO->m_cash) {
				if (pPO->m_cash->m_amount < 9000000) {
					UpdatePlayersCash(pPO, questPtr->m_cashReward, TransactionType::QUEST);
				}
			}
		} // end cash reward

#if (DRF_OLD_VENDORS == 1)
		if (questPtr->m_miscAction > 0) { // execute command
			if (questPtr->m_miscAction == 1) { // request Store data
				CCommerceObj *cmPtr = m_commerceDB->GetEstablishmentByPosition(pPO->InterpolatedPos(), pPO->m_currentChannel);
				if (cmPtr)
					SendMsgBlockStoreToClient(cmPtr->m_uniqueID, pPO);
			} // end request Store data
		} // end execute command
#endif

		if (questPtr->m_spawnAiCfg > -1 && questPtr->m_spawnOnFail == FALSE) { // spawn ai
			if (fTime::ElapsedMs(questPtr->m_spawnStamp) > 5000) {
				questPtr->m_spawnStamp = fTime::TimeMs();
			} else { // too soon to respawn
				RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::Talk, LS_TOO_BUSY, svrPtr->m_displayedName);
				return FALSE;
			} // end too soon to respawn
		} // end spawn ai

		if (questPtr->m_openDialogWindow) {
			TellClientToOpenMenu(questPtr->m_menuIdToOpen, pPO->m_netId, FALSE, 0);
		}

		if (!questPtr->m_questCompletionMessage.Trim().IsEmpty()) {
			CStringA completedMsgRaw;
			completedMsgRaw = svrPtr->m_displayedName + ">" + questPtr->m_questCompletionMessage;
			SendExtendedTextToChatWindow(pPO, completedMsgRaw);
		}

		if (expGained > 0.0f) {
			RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::Talk, LS_GAINED_EXPERIENCE);
		}

		if (questPtr->m_spawnToLocationOverride) { // must be called last
			BOOL continuePortaling = TRUE;
			auto pAO = pPO->m_pAccountObject;
			if (pAO) {
				if (questPtr->m_portalLimitMsg != "Reserved" && questPtr->m_portalLimitMsg.GetLength() > 0) { // evaluate local client content-he may not have the world to load
					if (pAO->m_localClientContentVersion < questPtr->m_portalLimitID) {
						SendExtendedTextToChatWindow(pPO, questPtr->m_portalLimitMsg);
						continuePortaling = FALSE;
					}
				} // end evaluate local client content-he may not have the world to load
			}

			if (continuePortaling) {
				if (questPtr->m_makeTargetRebirthPoint) {
					HandleReassignRespawnByDetails(pPO, idFromLoc, questPtr->m_spawnPosition, questPtr->m_respawnZoneIndex);
				}

				ZoneIndex newZoneIndex = pPO->createInstanceId(questPtr->m_respawnZoneIndex);

				if (newZoneIndex == INVALID_ZONE_INDEX)
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::Talk, LS_SPAWN_BAD);
				else {
					// if quest has an instance id, honor it
					if (questPtr->m_respawnZoneIndex.GetInstanceId() != 0) {
						newZoneIndex.setInstanceId(questPtr->m_respawnZoneIndex.GetInstanceId());
					}

					int charIndex = 0;
					if (pAO)
						charIndex = pAO->m_currentcharInUse;

					// fire event to send them there
					GotoPlaceEvent *e = new GotoPlaceEvent();
					e->GotoZone(newZoneIndex, newZoneIndex.GetInstanceId(),
						charIndex,
						questPtr->m_spawnPosition.x, questPtr->m_spawnPosition.y, questPtr->m_spawnPosition.z, questPtr->m_rotation);
					e->SetFrom(idFromLoc);
					m_dispatcher.QueueEvent(e);
				}
			}
		} // end must be called last
	} // end success
	else { // send info on quest
		if (questPtr->m_spawnAiCfg > -1 && questPtr->m_spawnOnFail == TRUE) { // spawn ai
			if (fTime::ElapsedMs(questPtr->m_spawnStamp) > 5000) {
				questPtr->m_spawnStamp = fTime::TimeMs();
			} else { // too soon to respawn
				RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::Talk, LS_TOO_BUSY, svrPtr->m_displayedName);
				return FALSE;
			} // end too soon to respawn
		} // end spawn ai

		if (questPtr->m_openDialogWindow)
			TellClientToOpenMenu(questPtr->m_menuIdToOpen, pPO->m_netId, FALSE, 0);

		if (!questPtr->m_questDesription.Trim().IsEmpty()) {
			CStringA completedMsgRaw = svrPtr->m_displayedName + ">" + questPtr->m_questDesription;
			SendExtendedTextToChatWindow(pPO, completedMsgRaw);
		}
	} // end send info on quest

	return TRUE;
}

BOOL ServerEngine::AddExperienceToSkillByType(int type, float m_expBonus, float m_thisGainCap, CPlayerObject *targetPayer) {
	if (m_expBonus > 0.0f) {
		if (!targetPayer->m_skills) {
			targetPayer->m_skills = new CSkillObjectList();
		}

		CSkillObject *skillPtr = targetPayer->m_skills->GetSkillByType(type);

		//
		// see if we have used this skill before-if not add it to our
		// database
		//
		if (!skillPtr) { // need to add this skill
			CSkillObject *dbSkillPtr = m_skillDatabase->GetSkillByType(type);
			if (!dbSkillPtr) {
				return FALSE;
			}

			dbSkillPtr->CloneMinimum(&skillPtr);
			targetPayer->m_skills->AddTail(skillPtr);
		} // end need to add this skill

		if (skillPtr) {
			if (skillPtr->m_currentExperience < m_thisGainCap) { // you can still gain on this
				skillPtr->m_currentExperience += m_expBonus;
				targetPayer->m_dbDirtyFlags |= PlayerMask::SKILLS;

				// expGained = m_expBonus;
			} // end you can still gain on this
			else {
				return FALSE;
			}
		}
	}

	return TRUE;
}

BOOL ServerEngine::SendExtendedTextToChatWindow(CPlayerObject *targetPayer, const char *_text) {
	CStringA text(_text); // TODO: get rid of text, and just use the ptr

	int stringLength = text.GetLength();

	int packetCount = 1;
	if (stringLength > 200) {
		packetCount = (stringLength / 200) + 1;
	}

	CStringA currentString;
	for (int pkLoop = 0; pkLoop < packetCount; pkLoop++) {
		int currentStringLength = 200;
		if (stringLength < 200) {
			currentStringLength = stringLength;
		} else {
			while (currentStringLength < stringLength && currentStringLength < 250) {
				if (text.GetAt(currentStringLength) == ' ') {
					break;
				}

				currentStringLength++;
			}
		}

		if (currentStringLength > 0) {
			currentString = text.Left(currentStringLength);
			text.Delete(0, currentStringLength);
			stringLength = stringLength - currentStringLength;

			m_multiplayerObj->QueueOrSendRenderTextEvent(targetPayer->m_netId, currentString, eChatType::Talk);
		}
	}

	return TRUE;
}

} // namespace KEP