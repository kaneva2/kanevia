///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ServerEngine.h"
#include "CStructuresMisl.h"
#include "CMultiplayerClass.h"
#include "CMovementObj.h"
#include "RuntimeSkeleton.h"
#include "AIclass.h"
#include "CWorldChannelRules.h"
#include "CServerItemClass.h"
#include "CAccountClass.h"
#include "CClanSystemClass.h"
#include "CCommerceClass.h"
#include "CCurrencyClass.h"
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "CNoterietyClass.h"
#include "CPacketQueClass.h"
#include "CPlayerClass.h"
#include "CSkillClass.h"
#include "CStatClass.h"
#include "CWorldAvailableClass.h"
#include "CQuestJournal.h"
#include "CCfgClass.h"
#include "Engine\Messages\MsgSpawnStaticMovementObj.h"
#include "Tools/NetworkV2/Protocols.h"
#include "LocaleStrings.h"

namespace KEP {

static LogInstance("Instance");

///////////////////////////////////////////////////////////////////////////////
// Message Senders
///////////////////////////////////////////////////////////////////////////////

bool ServerEngine::SendMsgMoveToClient(CPlayerObject* pPO, bool sendForce) {
	return (m_multiplayerObj && pPO) ? m_multiplayerObj->SendMsgMoveToClient(pPO->m_netId, pPO, sendForce) : false;
}

bool ServerEngine::SendMsgUpdateToClient(CPlayerObject* pPO, bool sendForce) {
	if (!pPO)
		return false;

	// Limit Update Messages To Client
	if (!sendForce && (pPO->m_msgUpdateToClientTimer.ElapsedMs() < msgUpdateMinTimeMs))
		return false;
	pPO->m_msgUpdateToClientTimer.Reset();

	BYTE* pData = nullptr;
	size_t dataSize = 0;
	CPacketObjList::EncodeMsgUpdate(
		pData,
		dataSize,
		MSG_CAT::UPDATE_TO_CLIENT,
		pPO->m_pMsgOutboxAttrib,
		pPO->m_pMsgOutboxEquip,
		pPO->m_pMsgOutboxGeneral,
		pPO->m_pMsgOutboxEvent,
		false);
	if (!pData)
		return false;

	bool ok = m_multiplayerObj->MsgTx(pData, dataSize, pPO->m_netId);
	delete[] pData;

	return ok;
}

// Sent as payload data within MSG_CAT::MOVE_TO_CLIENT
bool ServerEngine::QueueMsgSpawnStaticMOToClient(NETID netIdTo, CServerItemObj* pSIO) {
	if (!pSIO)
		return false;

	auto pPO = GetPlayerObjectByNetId(netIdTo, false);
	if (!pPO) {
		LogError("GetPlayerObjectByNetId() FAILED - netId=" << netIdTo);
		return false;
	}

	LogInfo(pSIO->ToStr());

	// Add Armed Glids (both old style and new)
	std::vector<GLID> allArmedGlids;
	if (pSIO->m_contents) {
		for (POSITION posLoc = pSIO->m_contents->GetHeadPosition(); posLoc != NULL;) {
			auto pIO = (CInventoryObj*)pSIO->m_contents->GetNext(posLoc);
			if (pIO && pIO->isArmed()) {
				allArmedGlids.push_back(pIO->m_itemData->m_globalID);
			}
		}
	}
	std::copy(pSIO->m_glidsArmed.begin(), pSIO->m_glidsArmed.end(), std::back_inserter(allArmedGlids));
	assert(allArmedGlids.size() <= BYTE_MAX);

	// Create Message
	MsgSpawnStaticMovementObj msg(
		ProtocolCompatibility::Instance().getMessageVersionByType(pPO->getCurrentProtocolVersion(), PC_MSG_SPAWN_STATIC_MO),
		pSIO->m_type,
		pSIO->m_displayedName.GetString(),
		pSIO->m_charConfig,
		allArmedGlids,
		pSIO->m_dbRef,
		pSIO->m_scale,
		pSIO->m_netTraceId,
		pSIO->m_position,
		pSIO->m_rotationAdjust,
		pSIO->m_glidAnimSpecial,
		pSIO->m_placementId);

	// Encode Message Into Byte Stream
	size_t dataSize = msg.getEncodedSize();
	auto dataPtr = std::make_unique<BYTE[]>(dataSize);
	bool res = msg.encode(dataPtr.get(), dataSize);
	if (!res) {
		LogError("msg.encode() FAILED");
		return false;
	}

	// Queue Message Packet
	pPO->m_pMsgOutboxSpawnStaticMO->AddPacket(dataSize, dataPtr.get());

	return true;
}

// Sent as payload data within MSG_CAT::UPDATE_TO_CLIENT
bool ServerEngine::QueueMsgEquipToClient(const std::vector<GLID>& glidsSpecial, long netTraceId, IPlayer* player) {
	// Valid Player ?
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO)
		return false;

	// Valid Glids ?
	auto glids = glidsSpecial.size();
	if (glids == 0)
		return false;
	if (glids > 255) {
		LogError("Too Many Glids - glids=" << glids << " > 255");
		return false;
	}

	// Construct Message
	DWORD messageSize = sizeof(EQUIP_DATA) + (glids * sizeof(GLID));
	BYTE* pBytes = new BYTE[messageSize];
	EQUIP_DATA* pMsg = (EQUIP_DATA*)pBytes;
	if (!pMsg)
		return false;
	pMsg->netTraceId = (short)netTraceId;
	pMsg->glids = (unsigned char)glids;

	// Append Glids
	GLID* pGlids = (GLID*)(pBytes + sizeof(EQUIP_DATA));
	for (size_t i = 0; i < glidsSpecial.size(); ++i)
		pGlids[i] = glidsSpecial[i]; // drf - special glid (<0=disarm, 0=disarmAll, >0=arm)

	// Queue Message Packet
	if (!pPO) {
		if (m_multiplayerObj->m_pMsgOutboxEquip)
			m_multiplayerObj->m_pMsgOutboxEquip->AddPacket(messageSize, pBytes);
	} else if (pPO->m_pMsgOutboxEquip) {
		pPO->m_pMsgOutboxEquip->AddPacket(messageSize, pBytes);
	}

	delete pMsg;

	return true;
}

// Sent as payload data within MSG_CAT::UPDATE_TO_CLIENT broadcast to all players
bool ServerEngine::QueueMsgEquipToAllClients(IPlayer* player, const std::vector<GLID>& glidsSpecial) {
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO)
		return false;

	// Valid Glids ?
	if (glidsSpecial.size() == 0)
		return false;

	// First Inform Entity
	bool ok = QueueMsgEquipToClient(glidsSpecial, 0, pPO);

	// Broadcast To All Players On Server (actually playing, not ai, within range)
	for (POSITION pos = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); pos;) {
		auto pPO_Svr = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(pos);
		if (!pPO_Svr || !pPO_Svr->m_inGame || (pPO_Svr->m_netId == 0) || pPO_Svr->m_aiControlled || (pPO_Svr->m_netTraceId == pPO->m_netTraceId) // same player
			|| (pPO_Svr->m_currentChannel != pPO->m_currentChannel) // not same world
		)
			continue;

		if (!QueueMsgEquipToClient(glidsSpecial, pPO->m_netTraceId, pPO_Svr))
			ok = false;
	}

	return ok;
}

// Sent as payload data within MSG_CAT::UPDATE_TO_CLIENT
bool ServerEngine::QueueMsgGeneralToClient(unsigned char msgType, CPlayerObject* pPO) {
	if (!pPO || pPO->m_aiControlled)
		return false;

	// Too Many Messages ? (MSG_UPDATE::m_generalMessages is unsigned short)
	if (pPO->m_pMsgOutboxGeneral->GetCount() >= USHORT_MAX)
		return false;

	GENERAL_DATA msg;
	size_t dataSize = sizeof(GENERAL_DATA);
	msg.m_data = msgType;
	pPO->m_pMsgOutboxGeneral->AddPacket(dataSize, (BYTE*)&msg);

	return true;
}

// Sends Player Inventory
BOOL ServerEngine::SendMsgBlockItemToClient(
	CInventoryObjList* pIOL,
	BOOL filterNonTranferable,
	NETID sendTo,
	int visualID,
	CCurrencyObj* pCO,
	CPlayerObject* pPO,
	BOOL includeArmed,
	int useTypeId) {
	if (pCO) {
		ATTRIB_DATA attribData;
		attribData.aeType = eAttribEventType::PlayerCredits;
		attribData.aeInt1 = pCO->m_amount;
		m_multiplayerObj->QueueMsgAttribToClient(sendTo, attribData, pPO);
	}

	int inventoryItemCount = pIOL ? pIOL->GetCount() : 0;
	if (!inventoryItemCount)
		return TRUE;

	MSG_BLOCK_ITEM_TO_CLIENT* pMsgBlockItem = NULL;
	int itemCount = 0;
	int msgSize = 0;
	if (!pIOL->GenerateMsgBlockItemToClient(includeArmed, &pMsgBlockItem, itemCount, msgSize, filterNonTranferable, useTypeId))
		return FALSE;

	pMsgBlockItem->m_header.m_miscInt = visualID;

	// Send Message
	bool ok = m_multiplayerObj->MsgTx((BYTE*)pMsgBlockItem, msgSize, sendTo);

	delete[] pMsgBlockItem;

	return ok ? TRUE : FALSE;
}

#if (DRF_OLD_VENDORS == 1)

bool ServerEngine::SendMsgBlockStoreToClient(int storeID, CPlayerObject* pPO) {
	if (!pPO)
		return false;

	CCommerceObj* pCO = m_commerceDB->GetEstablishmentByID(storeID);
	if (!pCO)
		return false;

	if (m_gameStateDb)
		m_gameStateDb->RefreshInventory(pPO);

	int playerInventoryCount = 0;
	for (POSITION invPos = pPO->m_inventory->GetHeadPosition(); invPos != NULL;) {
		auto pIO = (CInventoryObj*)pPO->m_inventory->GetNext(invPos);
		if (!pIO->m_itemData->m_nonDrop &&
			!pIO->m_itemData->m_permanent &&
			!pIO->isArmed())
			playerInventoryCount++;
	}

	if (m_gameStateDb) {
		try {
			pPO->m_cash->m_amount = m_gameStateDb->GetPlayersCash(pPO);
		} catch (KEPException* e) {
			LogError("Error getting player's cash for purchase " << pPO->m_charName << " " << e->m_msg);
			e->Delete();
		}
	}

	int storeInventoryCount = pCO->m_storeInventory->GetCount();
	int msgSize = MSG_BLOCK_STORE_ITEM_TO_CLIENT::computeSize(playerInventoryCount + storeInventoryCount);
	MSG_BLOCK_STORE_ITEM_TO_CLIENT* storeBlock = (MSG_BLOCK_STORE_ITEM_TO_CLIENT*)(new BYTE[msgSize]);
	storeBlock->m_header.SetCategory(MSG_CAT::BLOCK_STORE_TO_CLIENT);
	storeBlock->m_header.m_menuBind = pCO->m_menuBind;
	storeBlock->m_header.m_storeItemCount = storeInventoryCount;
	storeBlock->m_header.m_playerCash = pPO->m_cash->m_amount;
	storeBlock->m_header.m_storeCash = 0;

	// send storeid so client can retrieve store inventory item data from the web instead of from the game server.
	storeBlock->m_header.m_storeId = storeID;

	if (playerInventoryCount + storeInventoryCount > 0) {
		int itemIndex = 0;
		for (POSITION cPos = pCO->m_storeInventory->GetHeadPosition(); cPos != NULL;) {
			auto pSIO = (CStoreInventoryObj*)pCO->m_storeInventory->GetNext(cPos);
			storeBlock->m_data[itemIndex].m_GLID = pSIO->m_globalInventoryID;
			storeBlock->m_data[itemIndex].m_quanity = 1;
			itemIndex++;
		}

		for (POSITION invPos = pPO->m_inventory->GetHeadPosition(); invPos != NULL;) {
			auto pIO = (CInventoryObj*)pPO->m_inventory->GetNext(invPos);
			if (!pIO->m_itemData->m_nonDrop &&
				!pIO->m_itemData->m_permanent &&
				!pIO->isArmed()) {
				storeBlock->m_data[itemIndex].m_GLID = pIO->m_itemData->m_globalID;
				storeBlock->m_data[itemIndex].m_quanity = pIO->getQty();

				//if marked as gift send down as negative to mark as non tradable
				if (pIO->inventoryType() & (IT_GIFT | IT_UNLIMITED | IT_INFINITE))
					storeBlock->m_data[itemIndex].m_GLID = -storeBlock->m_data[itemIndex].m_GLID;

				itemIndex++;
			}
		}
	}

	// Send Message
	bool ok = m_multiplayerObj->MsgTx((BYTE*)storeBlock, msgSize, pPO->m_netId);

	delete[] storeBlock;

	return ok;
}

#endif

///////////////////////////////////////////////////////////////////////////////
// Message Handlers
///////////////////////////////////////////////////////////////////////////////

bool ServerEngine::HandleMsgs(BYTE* pData, NETID netIdFrom, size_t dataSize, bool& bDisposeMessage) {
	// Reset Return Values
	bDisposeMessage = true;

	if (!pData || !dataSize) {
		LogError("INVALID MESSAGE - netId = " << netIdFrom << " Disconnecting...");
		m_multiplayerObj->DisconnectClient(netIdFrom, DISC_SECURITY_VIO);
		return false;
	}

	m_messageCount++;

	// Reset Msg Rx Timer (prevent server disconnect after 30sec)
	auto pPO = GetPlayerObjectByNetId(netIdFrom, true);
	if (pPO)
		pPO->m_timerMsgRx.Reset();

	// Handle Message
	MSG_HEADER* pMsgHdr = (MSG_HEADER*)pData;
	MSG_CAT msgCat = pMsgHdr->GetCategory();
	switch (msgCat) {
		case MSG_CAT::EVENT:
			return HandleMsgEvent(pData, dataSize, netIdFrom, m_backgroundDefaultHandling);

		case MSG_CAT::LOG_TO_SERVER:
			return HandleMsgLogToServer(pData, netIdFrom);

		case MSG_CAT::ATTRIB_TO_SERVER:
			return HandleMsgAttribToServer(pData, dataSize, netIdFrom);

		case MSG_CAT::SPAWN_TO_SERVER:
			return HandleMsgSpawnToServer(pData, netIdFrom);

		case MSG_CAT::UPDATE_TO_SERVER:
			return HandleMsgUpdateToServer(pData, netIdFrom);

		case MSG_CAT::MOVE_TO_SERVER:
			return HandleMsgMoveToServer(pData, dataSize, netIdFrom);

		case MSG_CAT::CREATE_CHAR_TO_SERVER:
			return HandleMsgCreateCharToServer(pData, dataSize, netIdFrom);
	}

	LogError("MSG_CAT UNKNOWN - msgCat=" << (size_t)msgCat << " netId=" << netIdFrom << " Disconnecting...");
	m_multiplayerObj->DisconnectClient(netIdFrom, DISC_SECURITY_VIO);

	return false;
}

bool ServerEngine::HandleMsgEvent(BYTE* pData, size_t dataSize, NETID netIdFrom, bool async) {
	if (!pData || (dataSize < sizeof(EventHeader)))
		return false;

	IEvent* pEvent = nullptr;
	try {
		if (async) {
			// Process Event Asynchronously
			BYTE* pDataCpy = new BYTE[dataSize];
			memcpy(pDataCpy, pData, dataSize);
			pEvent = m_dispatcher.MakeEvent(pDataCpy, dataSize, netIdFrom);
			if (!pEvent) {
				LogFatal("dispatcher.MakeEvent() FAILED");
				delete[] pDataCpy;
				return false;
			}
			pEvent->InBuffer()->FreeOnDelete((PBYTE)pDataCpy);
		} else {
			// Process Event Synchronously
			pEvent = m_dispatcher.MakeEvent(pData, dataSize, netIdFrom);
			if (!pEvent) {
				LogFatal("dispatcher.MakeEvent() FAILED");
				return false;
			}
		}
	} catch (KEPException* e) {
		LogError("Caught Exception - netId = " << netIdFrom << " Disconnecting...");
		m_multiplayerObj->DisconnectClient(netIdFrom, DISC_SECURITY_VIO);
		e->Delete();
		return false;
	}

	if (pEvent->EventId() <= 0) {
		LogFatal("INVALID EVENT ID - eventId=" << pEvent->EventId());
		pEvent->Delete();
		return false;
	}

	if (async)
		m_dispatcher.QueueEvent(pEvent);
	else
		m_dispatcher.ProcessOneEvent(pEvent);

	return true;
}

bool ServerEngine::HandleMsgLogToServer(BYTE* pData, NETID netIdFrom) {
	auto pMsg = (MSG_LOG_TO_SERVER*)pData;
	if (!pMsg)
		return false;

	LogInfo("'" << pMsg->logInName << "' timeLogToServer=" << pMsg->timeLogToServer);

	// Measure Server Processing Time
	Timer timer;

	LOGON_RET lr = m_multiplayerObj->VerifyAccount(pMsg->logInName);
	if (lr != LR_OK) {
		LogError("VerifyAccount() FAILED - '" << pMsg->logInName << "' netId = " << netIdFrom << " Disconnecting...");
		m_multiplayerObj->DisconnectClient(netIdFrom, DISC_COMM_ASSIGN);
		return false;
	}

	CAccountObject* pAO = NULL;
	int logonStatus = m_multiplayerObj->m_runtimeAccountDB->EstablishLinkToAccount(pMsg->logInName, pMsg->logInPass, netIdFrom, &pAO);
	if (pAO && (logonStatus == ELA_RECONNECT)) {
		NETID oldNETID = pAO->m_netId;
		pAO->m_netId = netIdFrom;

		//Clear pending spawn info from previous session
		pAO->GetSpawnInfo().reset();

		//Disconnect old NETID
		m_multiplayerObj->DisconnectClient(oldNETID, DISC_NORMAL);

		// sync event stuff
		EventSyncEvent* event = new EventSyncEvent(m_dispatcher.eventMap(), pAO->m_pPlayerObjectList->GetCount());

		// queue it to be sent
		event->AddTo(pAO->m_netId);
		event->SetFlags(EF_SENDIMMEDIATE);
		m_dispatcher.QueueEvent(event);

		auto pPO_cur = pAO->GetCurrentPlayerObject();
		if (!pPO_cur) {
			LogError("RECONNECT FAILED - '" << pMsg->logInName << "' netID=" << netIdFrom);
			return false;
		}

		pPO_cur->m_netId = pAO->m_netId;
		pPO_cur->setCurrentProtocolVersion(pAO->getCurrentProtocolVersion());

		// Reset Move Data
		pPO_cur->MoveDataReset();

		auto pPO_old = GetPlayerObjectByNetId(oldNETID, false);
		if (pPO_old) {
			//Player still in runtime.  Update NETID.
			pPO_old->m_netId = pAO->m_netId;
			pPO_old->setCurrentProtocolVersion(pAO->getCurrentProtocolVersion());

			// Reset Move Data
			pPO_old->MoveDataReset();
		}

		// Send Message Log To Client
		TimeMs timeProcessing = timer.ElapsedMs();
		m_multiplayerObj->SendMsgLogToClient(
			netIdFrom,
			pMsg->timeLogToServer, // drf - added
			timeProcessing // drf - added
		);

		LogInfo("RECONNECT OK - " << pPO_cur->ToStr());
		return true;
	}

	if (!pAO || (logonStatus != ELA_LOGON)) {
		if (pAO) {
			LogError("EstablishLinkToAccount() FAILED - '" << pAO->m_accountName << "' netId=" << netIdFrom << " Disconnecting...");
		} else {
			LogError("EstablishLinkToAccount() FAILED - logonStatus=" << logonStatus << " netId=" << netIdFrom << " Disconnecting...");
		}
		m_multiplayerObj->DisconnectClient(netIdFrom, DISC_COMM_ASSIGN);
		return false;
	}

	// get any details from DB
	if (!m_gameStateDb)
		return true;

	// create a new account for filling in
	// since we need to grab some values set in EstablishLinkToAccount
	CAccountObject* pAO_new = new CAccountObject(pAO->m_accountName, "");

	pAO_new->m_netId = pAO->m_netId;
	pAO_new->setCurrentProtocolVersion(pAO->getCurrentProtocolVersion());
	pAO_new->m_serverId = pAO->m_serverId;
	pAO_new->m_registeredVersion = pAO->m_registeredVersion;
	pAO_new->m_accountNumber = pAO->m_accountNumber;
	pAO_new->m_sentLogoff = FALSE;
	pAO_new->StampLogOn();
	pAO_new->m_initializedHousingStates = FALSE;
	pAO_new->m_currentIP = pAO->m_currentIP;
	pAO_new->m_securityIndexer = 0; // reset indxer
	pAO_new->m_loggedOn = pAO->m_loggedOn;
	pAO_new->m_logonTimer.Reset().Start();
	pAO_new->m_gender = pAO->m_gender;
	pAO_new->m_birthDate = pAO->m_birthDate;
	pAO_new->m_country = pAO->m_country;
	pAO_new->m_showMature = pAO->m_showMature;
	pAO_new->m_administratorAccess = pAO->m_administratorAccess;
	pAO_new->m_canSpawn = pAO->m_canSpawn;
	pAO_new->m_gm = pAO->m_gm;

	GetAccountEvent* e = new GetAccountEvent(
		pAO_new,
		netIdFrom,
		pAO->m_serverId,
		pAO->m_accountName,
		pAO->m_currentIP.c_str(),
		pAO->m_gender,
		pAO->m_birthDate.c_str(),
		pAO->m_showMature);
	m_gameStateDispatcher->QueueEvent(e);

	// Reset Move Data
	auto pPO = pAO->GetCurrentPlayerObject();
	if (pPO)
		pPO->MoveDataReset();

	auto pPO_new = pAO_new->GetCurrentPlayerObject();
	if (pPO_new)
		pPO_new->MoveDataReset();

	// Send Message Log To Client
	TimeMs timeProcessing = timer.ElapsedMs();
	m_multiplayerObj->SendMsgLogToClient(
		netIdFrom,
		pMsg->timeLogToServer, // drf - added
		timeProcessing // drf - added
	);

	LogInfo("OK - '" << pAO->m_accountName << "' netID=" << netIdFrom << " timeProcessing=" << timeProcessing);

	return true;
}

void ServerEngine::FireGetAttributeEvent(const ATTRIB_DATA& attribData, NETID netIdFrom) {
	auto pEvent = new GetAttributeEvent(attribData);
	pEvent->SetFrom(netIdFrom);
	if (m_backgroundAttribToServer)
		m_dispatcher.QueueEvent(pEvent);
	else
		m_dispatcher.ProcessOneEvent(pEvent);
}

bool ServerEngine::HandleMsgAttribToServer(BYTE* pData, size_t dataSize, NETID netIdFrom) {
	auto pMsgHdr = (MSG_ATTRIB_BLOCK*)pData;
	if (!pMsgHdr)
		return false;

	// Skip Header
	pData += sizeof(MSG_HEADER);

	// Handle Attribute Updates
	size_t attrMsgs = (size_t)pMsgHdr->computePayloadCount(dataSize);
	for (size_t i = 0; i < attrMsgs; i++) {
		auto pMsg = (ATTRIB_DATA*)pData;
		FireGetAttributeEvent(*pMsg, netIdFrom);
		pData += sizeof(ATTRIB_DATA);
	}

	return true;
}

bool ServerEngine::HandleMsgAttrib(BYTE* pData, NETID netIdFrom) {
	auto pMsg = (ATTRIB_DATA*)pData;
	if (!pMsg)
		return false;

	switch (pMsg->aeType) {
		case eAttribEventType::GetMovementObjectCfg:
			return (AttribGetMovementObjectCfg(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::GetStaticMovementObjectCfg:
			return (AttribGetStaticMovementObjectCfg(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::SelectCharacter:
			return (AttribClientRequestPlayer(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::DeletePlayer:
			return (AttribDeleteCharacterFromAccount(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::GetCharacterList:
			return (AttribRequestCharList(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::SetInventoryItem:
			return (AttribSetInvenItemSwap(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::SetDeformableItem:
			return (AttribSetDeformableItem(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::GetInventoryPlayer:
		case eAttribEventType::GetInventoryBank:
		case eAttribEventType::GetInventoryHouse:
		case eAttribEventType::GetInventoryAll:
			return (AttribRequestCharInventory(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::BuyItem:
			return (AttribRequestBuyItemFromStore(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::SellItem:
			return (AttribRequestSellItemToStore(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::BankDepositItem:
			return (AttribRequestToBankItem(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::BankWithdrawItem:
			return (AttribTakeItemFromBank(pMsg, netIdFrom) == TRUE);

#if (DRF_OLD_PORTALS == 1)
		case eAttribEventType::RequestPortal:
			return (AttribRequestPortal(pMsg, netIdFrom) == TRUE);
#endif

		case eAttribEventType::UseItem:
			return (AttribUseItem(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::TryOnItem:
			return (AttribTryOnItem(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::CancelTransaction:
			return (AttribCancelTransaction(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::AcceptTradeTerms:
			return (AttribAcceptTradeTerms(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::DestroyItemByGlid:
			return (AttribDestroyItemByGlid(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::DestroyBankItemByGlid:
			return (AttribDestroyBankItemByGlid(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::QuestHandling:
			return (AttribQuestHandling(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::UseSkill: {
			auto pPO = GetPlayerObjectByNetId(netIdFrom, false);
			if (!pPO) {
				LogError("Cannot acquire runtime player inside skill ability use for NETID " << netIdFrom);
				return FALSE;
			}
			return (AttribSkillAbilityUse(pPO, pMsg->aeShort2, (pMsg->aeInt2 - 1), pMsg->aeInt1) == TRUE);
		}

		case eAttribEventType::BuyHouseItem:
			return (AttribBuyItemFromHouseStore(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::AddHouseItem:
			return (AttribAddItemToHouseStore(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::RemoveHouseItem:
			return (AttribRemoveItemFromHouseStore(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::RemoveHouseCash:
			return (AttribRemoveCashFromHouseStore(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::ReorgBankItems:
			return (AttribRequestToOrganize2BankItems(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::ReorgInventoryItems:
			return (AttribRequestToOrganize2InventoryItems(pMsg, netIdFrom) == TRUE);

		case eAttribEventType::GetInventoryPlayerAttachable:
			return (AttribRequestCharAttachableInventory(pMsg, netIdFrom) == TRUE);
	}

	return true;
}

bool ServerEngine::HandleMsgSpawnToServer(BYTE* pData, NETID netIdFrom) {
	if (!pData)
		return false;

	auto pAO = GetAccountObjectByNetId(netIdFrom);
	if (!pAO) {
		LogError("Error locating account in HandleSpawnMessagesServerSide for NETID " << netIdFrom);
		return false;
	}

	auto pPOByAccountNumber = GetPlayerObjectByAliveAndAccountNumber(pAO->m_accountNumber);
	if (pPOByAccountNumber) {
		LogError("ALREADY SPAWNED - " << pPOByAccountNumber->ToStr());
		return false;
	}

	ZoneIndex previousZoneIndex;

	auto pPO_new = new CPlayerObject();

	pPO_new->AllocatePacketLists();

	pPO_new->m_pAccountObject = pAO;

	auto pMsg = (MSG_SPAWN_TO_SERVER*)pData;

	// get the channel from supported worlds list
	ZoneIndex zoneIndex = ZoneIndex(pMsg->zoneIndex);
	zoneIndex.clearInstanceAndType();
	pPO_new->m_currentChannel = m_multiplayerObj->m_currentWorldsUsed->AddWorldIfAvailableByIndex(zoneIndex.toLong(), pMsg->worldName);
	pPO_new->m_currentWorldMap = pMsg->worldName;

	// get database and give the start inventory to player if first time spawning/creating character
	auto pMORM = m_movObjRefModelList->getObjByIndex(pMsg->dbIndex);
	if (!pMORM) {
		LogError("Player " << pAO->m_accountName << " attempted to spawn unknown database entity of index " << pMsg->dbIndex);
		return false;
	} else {
		if (pMORM->getModelType() == CEntityPropertiesCtrl::MODEL_TYPE_DISABLED) {
			LogError("Player " << pAO->m_accountName << " attempted to spawn bad entity of index " << pMsg->dbIndex);
			return false;
		}

		// valid attach start inventory
		if (pMsg->usePresets == 0) {
			// ai server spawn
			pPO_new->m_admin = pAO->m_administratorAccess;
			pPO_new->m_gm = pAO->m_gm;
			pPO_new->m_spawnAbility = pAO->m_canSpawn;
			pPO_new->m_dbCfg = pMsg->dbIndex;

			pPO_new->m_aiControlled = TRUE;
			if (pMORM->getSkills())
				pMORM->getSkills()->Clone(&pPO_new->m_skills); // assign start skills
			else
				pPO_new->m_skills = new CSkillObjectList(); // give default

			pPO_new->m_inventory = new CInventoryObjList();
			pPO_new->m_cash = new CCurrencyObj();
			pPO_new->m_cash->m_amount = 0;
			pPO_new->m_giftCredits = new CCurrencyObj();
			pPO_new->m_giftCredits->m_amount = 0;
			pPO_new->m_charName = pMORM->getName().c_str();
			pPO_new->m_title = pMORM->getTitle().c_str();
			pPO_new->m_curEDBIndex = pPO_new->m_dbCfg;

			auto pAIO = m_AIOL->GetByIndex(pMsg->aiCfgIndex);
			if (pAIO) {
				if (pAIO->m_overriderMaxEnergy > 0)
					pPO_new->m_dbDirtyFlags |= PlayerMask::POSITION;

				pPO_new->m_charName = pAIO->m_nameOvveride;

				if (pAIO->m_enableCashApplied) {
					int range = pAIO->m_maxCash - pAIO->m_minCash;
					if (range > 0) {
						int final = rand() % range;
						pPO_new->m_cash->m_amount = pAIO->m_minCash + final; // set pocket cash
					}
				}

				BOOL useOverride = FALSE;
				if (pAIO->m_inventoryCfg) {
					if (pAIO->m_inventoryCfg->GetCount() > 0)
						useOverride = TRUE;
				}

				if (useOverride) {
					// valid inventory db
					for (POSITION invCfgPos = pAIO->m_inventoryCfg->GetHeadPosition(); invCfgPos != NULL;) {
						auto pIO = (CInventoryObj*)pAIO->m_inventoryCfg->GetNext(invCfgPos);
						if (pIO->isArmed()) {
							pPO_new->m_inventory->AddInventoryItem(1, pIO->isArmed(), pIO->m_itemData, pIO->inventoryType()); // add inven obj
						} else {
							int resultThrow = rand() % 100;
							if (resultThrow < pAIO->m_percentageOfDropping)
								pPO_new->m_inventory->AddInventoryItem(1, pIO->isArmed(), pIO->m_itemData, pIO->inventoryType()); // add inven obj
						}
					}
				} else if (pMORM->getAutoCfg()) {
					pPO_new->m_charName = pMORM->getName().c_str();
					for (POSITION posLoc = pMORM->getAutoCfg()->GetHeadPosition(); posLoc != NULL;) {
						auto pCO = static_cast<const CCfgObj*>(pMORM->getAutoCfg()->GetNext(posLoc));
						if (pCO->m_quanity > 0) {
							auto pGIO = m_globalInventoryDB->GetByGLID(pCO->m_globalCFG);
							if (!pGIO)
								continue;

							for (int iLoop = 0; iLoop < pCO->m_quanity; iLoop++)
								pPO_new->m_inventory->AddInventoryItem(1, pCO->m_armed != FALSE, pGIO->m_itemData, IT_GIFT);
						}
					}
				}

				// DIM CFG
				if (pMORM->getSkeleton()->getSkinnedMeshCount() > 0) {
					// valid cfg
					pPO_new->m_baseMeshes = pMORM->getSkeleton()->getSkinnedMeshCount();
					pPO_new->m_charConfig.initBaseItem(pMORM->getSkeleton()->getSkinnedMeshCount());

					for (size_t slot = 0; slot < pPO_new->m_baseMeshes; slot++) {
						if (slot < pAIO->m_baseMeshes) {
							pPO_new->m_charConfig.setItemSlotMeshId(GOB_BASE_ITEM, slot, pAIO->m_pMeshIds[slot]);
							// TODO materials and RGB not supported here
						}
					}
				}
			} else if (pMORM->getAutoCfg()) {
				// configure through edb old school
				for (POSITION posLoc = pMORM->getAutoCfg()->GetHeadPosition(); posLoc != NULL;) {
					// inital cfg list
					auto cfgPtr = static_cast<const CCfgObj*>(pMORM->getAutoCfg()->GetNext(posLoc));
					if (cfgPtr->m_quanity > 0) {
						CGlobalInventoryObj* invObj = m_globalInventoryDB->GetByGLID(cfgPtr->m_globalCFG);
						if (!invObj)
							continue;

						for (int iLoop = 0; iLoop < cfgPtr->m_quanity; iLoop++)
							pPO_new->m_inventory->AddInventoryItem(1, cfgPtr->m_armed != FALSE, invObj->m_itemData, IT_GIFT);
					}
				}
			}
		} else {
			// user spawn
			if (pMsg->characterInUse < 0) {
				LogError("characterInUse < 0 when spawning for NETID " << netIdFrom);
				return false;
			}

			POSITION charPos = pAO->m_pPlayerObjectList->FindIndex(pMsg->characterInUse);
			if (!charPos) {
				LogError("Invalid character selection when spawning for account " << pAO->m_accountName);
				return false;
			}

			auto pPO = (CPlayerObject*)pAO->m_pPlayerObjectList->GetAt(charPos);
			previousZoneIndex = pPO->m_previousZoneIndex;
			if (pPO->m_stats)
				pPO->m_stats->Clone(&pPO_new->m_stats);

			if (pPO->m_giftCredits == 0 || pPO->m_cash == 0) {
				LogError("Character doesn't have cash object, probably problem loading." << pPO->m_charName);
				return false;
			}

			pPO->m_cash->Clone(&pPO_new->m_cash);
			pPO->m_giftCredits->Clone(&pPO_new->m_giftCredits);
			pPO_new->m_skillDBisReferenced = TRUE;
			pPO_new->m_skills = pPO->m_skills;
			pPO_new->CopyDynamicallyAddedMembers(pPO); // 10/05 copy any dyanmically added data
			pPO_new->GetSet::SetDirty(pPO->GetSet::IsDirty()); // ccopy dirty flag from original

			if (m_worldChannelRuleDB->DoesThisRuleExistInThisChannel(pPO_new->m_currentChannel, CWordlChannelRuleObj::ARENA_AREA)) {
				if (pPO->m_preArenaInventory == 0) {
					// going into arena first time, copy off the inventory, so we can restore it
					pPO->m_inventory->Clone(&pPO->m_preArenaInventory);
				}
			} else if (pPO->m_preArenaInventory) {
				// coming out of an arena, restore inventory to what it was
				pPO->m_preArenaInventory->Clone(&pPO->m_inventory);
				pPO->m_preArenaInventory->SafeDelete();
				DELETE_AND_ZERO(pPO->m_preArenaInventory);
			}
			pPO->m_inventory->Clone(&pPO_new->m_inventory);

			if (pPO->m_reconfigInven) {
				pPO->m_reconfigInven->Clone(&pPO_new->m_reconfigInven);
			}
			pPO_new->setPlayerOrigRef(pPO);

			pPO_new->m_baseMeshes = pPO->m_baseMeshes;
			if (pPO_new->m_baseMeshes > 0) {
				pPO_new->m_charConfig.initBaseItem(pPO_new->m_baseMeshes);
				pPO_new->m_charConfig = pPO->m_charConfig;
			}

			pAO->m_currentcharInUse = pMsg->characterInUse; // set

			pPO_new->m_handle = pPO->m_handle;
			pPO_new->m_clanHandle = pPO->m_clanHandle;
			pPO_new->m_housingZoneIndex = pPO->m_housingZoneIndex;
			pPO_new->m_charName = pPO->m_charName;
			pPO_new->m_displayName = pPO->m_charName; // set displayName equal to player name on zone
			pPO_new->m_title = pPO->m_title;
			pPO_new->m_displayTitle = pPO->m_title;
			pPO_new->m_nameColor = pPO->m_nameColor;

			// copy any remaining guaranteeed msgs if they were copied no less than 30 seconds ago
			//			TimeSec timeSec = pPO->m_updateAcctTimer.ElapsedSec();
			//			if (timeSec < 30.0) {
			CPacketObjList::MovePackets(pPO_new->m_pMsgOutboxAttrib, pPO->m_pMsgOutboxAttrib);
			CPacketObjList::MovePackets(pPO_new->m_pMsgOutboxEquip, pPO->m_pMsgOutboxEquip);
			CPacketObjList::MovePackets(pPO_new->m_pMsgOutboxGeneral, pPO->m_pMsgOutboxGeneral);
			CPacketObjList::MovePackets(pPO_new->m_pMsgOutboxEvent, pPO->m_pMsgOutboxEvent);
			//			} else {
			//				LogError("UPDATE_ACCT_TIMEOUT - PURGING MESSAGES - timeSec=" << timeSec << " " << pPO->ToStr());
			//				if (pPO->m_pMsgOutboxAttrib)
			//					pPO->m_pMsgOutboxAttrib->SafeDelete();
			//				if (pPO->m_pMsgOutboxEquip)
			//					pPO->m_pMsgOutboxEquip->SafeDelete();
			//				if (pPO->m_pMsgOutboxGeneral)
			//					pPO->m_pMsgOutboxGeneral->SafeDelete();
			//				if (pPO->m_pMsgOutboxEvent)
			//					pPO->m_pMsgOutboxEvent->SafeDelete();
			//			}
			pPO->m_updateAcctTimer.Reset();

			pPO_new->m_admin = pAO->m_administratorAccess;
			pPO_new->m_gm = pAO->m_gm;
			pPO_new->m_spawnAbility = pAO->m_canSpawn;
			pPO_new->m_dbDirtyFlags |= PlayerMask::POSITION;

			if (pPO_new->m_clanHandle != NO_CLAN) {
				CClanSystemObj* clanPtr = m_multiplayerObj->m_clanDatabase->GetClanByHandle(pPO_new->m_clanHandle);
				if (!clanPtr) {
					pPO_new->m_clanHandle = NO_CLAN; // clear clan
					pPO_new->m_clanName = "";
				} else {
					if (clanPtr->m_memberList) {
						if (clanPtr->m_memberList->GetByName(pPO->m_charName) != NULL || pPO_new->m_clanHandle == pPO_new->m_handle) {
							// verify roster/or clan leader
							pPO_new->m_clanName = clanPtr->m_abbrev;
						} else {
							pPO_new->m_clanHandle = NO_CLAN; // clear clan
							pPO_new->m_clanName = "";
						}
					}
				}
			}

			pPO_new->m_accountNumberRef = pAO->m_accountNumber;
			pPO_new->m_altCfgIndex = pPO->m_altCfgIndex;
			pPO_new->m_curEDBIndex = pPO->m_curEDBIndex;
			pPO_new->m_originalEDBCfg = pPO->m_originalEDBCfg;
			pPO_new->m_dbCfg = pMsg->dbIndex;
			pPO_new->m_aiCfgOfMounted = pPO->m_aiCfgOfMounted;
			pPO_new->m_scaleFactor.x = pPO->m_scaleFactor.x;
			pPO_new->m_scaleFactor.y = pPO->m_scaleFactor.y;
			pPO_new->m_scaleFactor.z = pPO->m_scaleFactor.z;
			pPO_new->m_scratchScaleFactor = pPO_new->m_scaleFactor;

			if (pPO->m_noteriety)
				pPO->m_noteriety->Clone(&pPO_new->m_noteriety);
			else
				pPO_new->m_noteriety = new CNoterietyObj();

			pPO_new->m_movementInterpolator = pPO->m_movementInterpolator;

			if (pPO->m_passList)
				pPO_new->m_passList = new PlayerPassList(*pPO->m_passList);
		} // end user spawn
	} // end valid attach start inventory

	pPO_new->m_netTraceId = m_multiplayerObj->GetNextFreeNetTraceId();
	pPO_new->m_dynamicRadius = pMORM->getBoundingRadius();
	pPO_new->m_inGame = TRUE;
	pPO_new->m_skillUseTimeStamp = fTime::TimeMs(); // interupt his skill use

	// Reset Msg Rx Timer (prevent server disconnect after 30sec)
	pPO_new->m_timerMsgRx.Reset();

	// see if it is instanced
	ZoneIndex zi;
	zi.fromLong(pMsg->zoneIndex);

	if (zi.isInstanceType() &&
		zi.GetInstanceId() == 0) { // check for non-zero to allow to goto friend's house, clan zone
		ZoneIndex tempZi = pPO_new->createInstanceId(zi);
		if (tempZi == INVALID_ZONE_INDEX) {
			LogError("Bad Zone Instance " << pPO_new->ToStr() << " "
										  << numToString(zi.toLong(), "%d") << " "
										  << numToString(pPO_new->m_handle) << " "
										  << numToString(pPO_new->m_clanHandle));
			return false;
		}
		zi = tempZi;
	}

	if (zi.isInstanced()) {
		CWorldAvailableObj* wa = m_multiplayerObj->m_currentWorldsUsed->GetByZoneIndex(zi);
		if (wa == 0 ||
			wa->m_worldName.CompareNoCase(pMsg->worldName) != 0) {
			LogWarn("Player attempted to spawn to invalid world id: " << numToString(zi.toLong()) << " " << zi.GetInstanceId());
			if (wa == 0) {
				LogWarn(" ... since couldn't find zone ");
			} else {
				LogWarn(" ... names didn't match " << wa->m_worldName << " != " << pMsg->worldName);
			}
			return false;
		}
		pPO_new->m_currentChannel.setInstanceAndTypeFrom(zi);
	}
	pPO_new->m_currentZoneIndex = zi;

	m_multiplayerObj->m_runtimePlayersOnServer->AddTail(pPO_new);

	// When player spawns give them a scratchInventory of things they are wearing for script manipulation.
	if (pPO_new->m_scratchInventory) {
		pPO_new->m_scratchInventory->SafeDelete();
		delete pPO_new->m_scratchInventory;
		pPO_new->m_scratchInventory = NULL;
	}
	pPO_new->m_scratchInventory = new CInventoryObjList();
	for (POSITION iPos = pPO_new->m_inventory->GetHeadPosition(); iPos != NULL;) {
		CInventoryObj* invenDBPtr = (CInventoryObj*)pPO_new->m_inventory->GetNext(iPos);
		if (invenDBPtr->isArmed()) {
			CInventoryObj* clone;
			invenDBPtr->Clone(&clone);
			pPO_new->m_scratchInventory->AddTail(clone);
		}
	}

	pPO_new->m_netId = netIdFrom; // use for routing messages only
	pPO_new->setCurrentProtocolVersion(pAO->getCurrentProtocolVersion());

	pPO_new->m_parentZoneInstanceId = pAO->GetSpawnInfo().getParentZoneInstanceId();
	pPO_new->m_currentZonePriv = pAO->GetSpawnInfo().getZonePermissions();
	if (pPO_new->m_currentZonePriv == ZP_UNKNOWN) {
		LogWarn("unknown player zone permissions"
				<< ", player " << pPO_new->m_charName << " (NETID: " << pPO_new->m_netId << ")"
				<< ", zone " << pPO_new->m_currentZoneIndex.toLong());

		// Use ZP_NONE by default
		pPO_new->m_currentZonePriv = ZP_NONE;
	}

	// Erase zone permissions in pending spawn info now to prevent it from being used in future spawns
	// Normally ZoneDownloadCompleteEventHandler will do the job. This is in case of race condition.
	pAO->GetSpawnInfo().clearZoneInfo();

	LogInfo(pPO_new->ToStr());

	//See if the player is involved in p2p animation
	P2pAnimTerminate(pPO_new->m_netId);

	pPO_new->m_interactionStates = AVAILABLE;

	// send communication data / networkassigned id /energy current
	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::PlayerCreatedHealth;
	attribData.aeShort1 = pMsg->currentTrace;
	attribData.aeInt1 = pPO_new->m_netTraceId;
	m_multiplayerObj->QueueMsgAttribToClient(netIdFrom, attribData, pPO_new);

	// Update Player Cash Amount
	attribData.aeType = eAttribEventType::PlayerCredits;
	attribData.aeShort1 = 0;
	attribData.aeInt1 = pPO_new->m_cash->m_amount;
	m_multiplayerObj->QueueMsgAttribToClient(netIdFrom, attribData, pPO_new);

	pAO->m_arenaLiscense = 0;

	PassList* zonePassList = ValidateArmedInventoryForZone(pPO_new, zi);
	m_dispatcher.QueueEvent(new PassListEvent(pPO_new->m_netId,
		pPO_new->m_passList ? &pPO_new->m_passList->ids() : 0,
		pPO_new->m_passList ? &pPO_new->m_passList->expirations() : 0,
		zonePassList ? &zonePassList->ids() : 0));

	// fire new event to let folks know someone spawned, if anyone is listening
	if (m_dispatcher.HasHandler(m_playerSpawnedEventId)) {
		IEvent* e = m_dispatcher.MakeEvent(m_playerSpawnedEventId);
		e->SetFilter(pMsg->zoneIndex);
		e->SetFrom(pPO_new->m_netId);
		(*e->OutBuffer()) << pPO_new->m_netTraceId
						  << (ULONG) dynamic_cast<IGetSet*>(pPO_new)
						  << pPO_new->m_aiControlled
						  << pMsg->worldName
						  << previousZoneIndex.toLong();
		m_dispatcher.ProcessSynchronousEvent(e);
	}

	return true;
}

bool ServerEngine::HandleMsgUpdateToServer(BYTE* pData, NETID netIdFrom) {
	auto pMsgHdr = (MSG_UPDATE*)pData;
	if (!pMsgHdr)
		return false;

	// Skip Message Header
	pData += sizeof(MSG_UPDATE);

	// Handle Attribute Updates
	auto attribMsgs = (size_t)pMsgHdr->m_attribMsgs;
	for (size_t i = 0; i < attribMsgs; i++) {
		auto pMsg = (ATTRIB_DATA*)pData;
		FireGetAttributeEvent(*pMsg, netIdFrom);
		pData += sizeof(ATTRIB_DATA);
	}

	// Handle Equip Updates
	auto equipMsgs = (size_t)pMsgHdr->m_equipMsgs;
	for (size_t i = 0; i < equipMsgs; i++) {
		auto pMsg = (EQUIP_DATA*)pData;
		HandleMsgEquipToServer((BYTE*)pMsg, netIdFrom);
		pData += sizeof(EQUIP_DATA) + (pMsg->glids * sizeof(GLID));
	}

	// Handle General Updates (CURRENTLY NOT USED)
	auto generalMsgs = (size_t)pMsgHdr->m_generalMsgs;
	for (size_t i = 0; i < generalMsgs; i++) {
		// auto pMsg = (GENERAL_DATA *) pData;
		// HandleMsgGeneralToServer((BYTE*) pMsg, netIdFrom);
		pData += sizeof(GENERAL_DATA);
	}

	// Handle Event Updates (asynchronously)
	auto eventMsgs = (size_t)pMsgHdr->m_eventMsgs;
	for (size_t i = 0; i < eventMsgs; i++) {
		auto msgSize = *(ULONG*)pData;
		pData += sizeof(ULONG); // LEN_FMT_ULONG
		if (!msgSize)
			continue;
		HandleMsgEvent(pData, msgSize, netIdFrom, true);
		pData += msgSize;
	}

	return true;
}

bool ServerEngine::HandleMsgMoveToServer(BYTE* pData, size_t sizeData, NETID netIdFrom) {
	auto pMsg = (MSG_MOVE_TO_SERVER*)pData;
	if (!pMsg)
		return false;

	// Get Player Being Moved
	auto netTraceId = pMsg->netTraceId;
	auto pPO = GetPlayerObjectByNetTraceId(netTraceId);
	if (!pPO) {
		LogError("GetPlayerObjByNetTraceId() FAILED - netTraceId=" << netTraceId);
		return false;
	}

	// Handle Message Move To Server (updates avatar position & move data)
	pPO->HandleMsgMoveToServer(pData);

	// Send Message Move To Client (all other player positions, don't force)
	m_multiplayerObj->SendMsgMoveToClient(netIdFrom, pPO, false);

	return true;
}

bool ServerEngine::HandleMsgCreateCharToServer(BYTE* pData, size_t dataSize, NETID netIdFrom) {
	auto pMsgHdr = (MSG_CREATE_CHAR_TO_SERVER*)pData;
	if (!pMsgHdr)
		return false;

	if (dataSize < sizeof(MSG_CREATE_CHAR_TO_SERVER)) {
		LogError("MSG SIZE INVALID - netId=" << netIdFrom << " Disconnecting...");
		m_multiplayerObj->DisconnectClient(netIdFrom, DISC_SECURITY_VIO);
		return false;
	}

	// The size of the message header + the number of attached items + the number of deformable meshes
	size_t packetSize =
		sizeof(MSG_CREATE_CHAR_TO_SERVER) +
		(sizeof(CHAR_ITEM_DATA) * pMsgHdr->m_numCharItems) +
		(sizeof(CHAR_CONFIG_ITEM_DATA) * pMsgHdr->m_numCharConfigItems) +
		pMsgHdr->m_nameLength;

	if (dataSize != packetSize) {
		LogError("MSG SIZE INVALID - netId=" << netIdFrom << " Disconnecting...");
		m_multiplayerObj->DisconnectClient(netIdFrom, DISC_SECURITY_VIO);
		return false;
	}

	BYTE* dataBlock = (BYTE*)pData; // cast to simple byte array
	size_t byteOffset = sizeof(MSG_CREATE_CHAR_TO_SERVER); // skip header

	CAccountObject* pAO = GetAccountObjectByNetId(netIdFrom);
	if (!pAO) {
		LogError("GetAccountObjectByNetId() FAILED - netId=" << netIdFrom);
		return false;
	}

	if (pAO->m_aiServer) {
		LogError("AI_SERVER - '" << pAO->m_accountName << "' netId=" << netIdFrom << " Disconnecting...");
		m_multiplayerObj->DisconnectClient(netIdFrom, DISC_SECURITY_VIO);
		return false;
	}

	// Extract character name
	CStringA name;
	name.SetString((char*)&dataBlock[byteOffset], pMsgHdr->m_nameLength);
	byteOffset += pMsgHdr->m_nameLength;
	name.Trim();

	byteOffset += pMsgHdr->m_numCharItems * sizeof(CHAR_ITEM_DATA);

	CHAR_CONFIG_ITEM_DATA* cfgDataStruct = (CHAR_CONFIG_ITEM_DATA*)&dataBlock[byteOffset];
	byteOffset += pMsgHdr->m_numCharConfigItems * sizeof(CHAR_CONFIG_ITEM_DATA);

	// 5/07 added support for updating an existing player
	CPlayerObject* pPO_update = 0;
	if (pAO->m_pPlayerObjectList) {
		for (POSITION pos = pAO->m_pPlayerObjectList->GetHeadPosition(); pos != 0;) {
			auto pPO_find = (CPlayerObject*)pAO->m_pPlayerObjectList->GetNext(pos);
			if (pPO_find->m_charName.CompareNoCase(name) == 0) {
				pPO_update = pPO_find;

				if (m_gameStateDb != 0 && pPO_find->m_currentZoneIndex == INVALID_ZONE_INDEX) { // must not have logged on to load from db
					try {
						if (!pPO_find->m_pAccountObject)
							pPO_find->m_pAccountObject = pAO;
						m_gameStateDb->GetPlayer(pPO_find);
					} catch (KEPException* e) {
						LogError("Error loading player from db, update rejected " << name << " " << e->m_msg);
						e->Delete();
						return false;
					}
				}
				break;
			}
		}
	}

	pMsgHdr->m_entityType = CheckGender(pAO, pMsgHdr->m_entityType);

	CMovementObj* pMO = m_movObjRefModelList->getObjByIndex(pMsgHdr->m_entityType);
	if (!pMO) {
		// invalid spawn index
		LogError("Player " << name << " attempted to create from and unknown database entity");
		return false;
	}

	// make sure name is globally unique
	if (pPO_update == 0 && m_multiplayerObj->findPlayerByName(name) != 0) {
		LogError("Duplicate player name, create rejected " << name);
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pAO->m_netId, eChatType::Talk, LS_DUPLICATE_PLAYER_NAME, name);
		return false;
	}

	if (pMsgHdr->m_numCharConfigItems + pMsgHdr->m_numCharItems <= 0)
		return false;

	// make sure they dont create too many characters
	if (!pAO->m_pPlayerObjectList)
		pAO->m_pPlayerObjectList = new CPlayerObjectList();

	if (pPO_update == 0 && pAO->m_pPlayerObjectList->GetCount() >= m_multiplayerObj->m_clientCharacterMaxCount) {
		LogError("Account " << pAO->m_accountName << " has no room to create new characters.");
		return false;
	}

	CPlayerObject* pPO_new = 0;
	auto_ptr_safe_delete<CPlayerObject> _pPO_new(0); // more leak fixes if return soon
	if (pPO_update == 0) {
		// init new player
		pPO_new = new CPlayerObject();
		_pPO_new = auto_ptr_safe_delete<CPlayerObject>(pPO_new);
		pPO_new->m_inventory = new CInventoryObjList();
		pPO_new->m_bankInventory = new CInventoryObjList();
		pPO_new->m_bankCash = new CCurrencyObj();
		pPO_new->m_cash = new CCurrencyObj();
		pPO_new->m_cash->m_amount = m_multiplayerObj->m_initialCash;
		pPO_new->m_bankCash->m_amount = m_multiplayerObj->m_initialBankCash;

		pPO_new->m_giftCredits = new CCurrencyObj();

		pPO_new->m_skills = new CSkillObjectList();
		pPO_new->m_handle = m_multiplayerObj->m_accountDatabase->GetNewLocalPlayerHandleID();
	} else {
		pPO_new = pPO_update;

		// wipe out stuff we set on new player creation
		pPO_new->m_charConfig.disposeAllItems();
	}

	pPO_new->m_scaleFactor.x = pMsgHdr->m_scaleX;
	pPO_new->m_scaleFactor.y = pMsgHdr->m_scaleY;
	pPO_new->m_scaleFactor.z = pMsgHdr->m_scaleZ;

	pPO_new->m_scratchScaleFactor = pPO_new->m_scaleFactor;

	if (!pPO_new->m_questJournal)
		pPO_new->m_questJournal = new CQuestJournalList();

	// get database and give the start inventory to player if first time
	// creating character
	// end invalid spawn index
	if (pMO->getModelType() == CEntityPropertiesCtrl::MODEL_TYPE_DISABLED) { //
		LogError("Creating a char with old EDB! " << pMsgHdr->m_entityType << " " << pAO->m_accountName);
		return false;
	}

	if (pPO_update != 0 && m_statDatabase)
		m_statDatabase->Clone(&pPO_new->m_stats);

	pPO_new->m_baseMeshes = pMO->getSkeleton()->getSkinnedMeshCount();
	if (pPO_new->m_baseMeshes > 0)
		pPO_new->m_charConfig.initBaseItem(pPO_new->m_baseMeshes);

	// if updating don't give them new starting inv.
	if (pPO_update == 0 && pMO->getAutoCfg()) {
		for (POSITION posLoc = pMO->getAutoCfg()->GetHeadPosition(); posLoc != NULL;) {
			auto cfgPtr = static_cast<const CCfgObj*>(pMO->getAutoCfg()->GetNext(posLoc));
			if (cfgPtr->m_quanity > 0) {
				CItemObj* itemRef = pMO->getItem(cfgPtr->m_globalCFG);
				if (!itemRef) {
					LogError("Invalid GLID in starting inventory: " << cfgPtr->m_globalCFG << " found when creating " << name);
					continue;
				}

				for (int iLoop = 0; iLoop < cfgPtr->m_quanity; iLoop++) {
					pPO_new->m_inventory->AddInventoryItem(1, cfgPtr->m_armed != FALSE, itemRef, IT_GIFT);
				}
			}
		}
	}

	pPO_new->m_dbCfg = pMsgHdr->m_entityType;
	pPO_new->m_curEDBIndex = pMsgHdr->m_entityType;
	pPO_new->m_originalEDBCfg = pMsgHdr->m_entityType;
	if (m_multiplayerObj->m_spawnPointCfgs == NULL || m_multiplayerObj->m_currentWorldsUsed == NULL) {
		LogError("Server has no valid preset spawn/world Cfgs");
		return false;
	}

	if (pMsgHdr->m_spawnCfg < 0) {
		LogError("Bounds FindIndex CharacterCreationOnAccount()");
		return false;
	}

	CSpawnCfgObj* spwnPtr = m_multiplayerObj->m_spawnPointCfgs->FindId(pMsgHdr->m_spawnCfg);
	if (!spwnPtr) {
		LogError("Invalid spawn cfg of " << pMsgHdr->m_spawnCfg);
		return false;
	}

	CWorldAvailableObj* wldRefObject = m_multiplayerObj->m_currentWorldsUsed->GetByZoneIndex(spwnPtr->m_zoneIndex);
	if (!wldRefObject) {
#ifndef REFACTOR_INSTANCEID
		LogError("Invalid world spawn ref for index " << spwnPtr->m_zoneIndex);
#else
		LogError("Invalid world spawn ref for index " << spwnPtr->m_zoneIndex.toLong());
#endif
		return false;
	}

	// set
	pPO_new->m_currentZoneIndex = spwnPtr->m_zoneIndex;
	pPO_new->m_currentWorldMap = wldRefObject->m_worldName;

	int rotation = 0;
	if (pPO_update == 0) {
		MovementData md = pPO_new->InterpolatedMovementData();
		md.pos = spwnPtr->m_position;
		pPO_new->MovementInterpolatorInit(md);

		rotation = spwnPtr->m_rotation;
		if (spwnPtr->m_zoneIndex.isHousing())
			pPO_new->m_housingZoneIndex = spwnPtr->m_zoneIndex;
		pPO_new->m_bornSpawnCfgIndex = pMsgHdr->m_spawnCfg;
		pPO_new->m_charName = name;

		pPO_new->m_respawnPosition.x = spwnPtr->m_position.x;
		pPO_new->m_respawnPosition.y = spwnPtr->m_position.y;
		pPO_new->m_respawnPosition.z = spwnPtr->m_position.z;
		pPO_new->m_respawnZoneIndex = pPO_new->createInstanceId(spwnPtr->m_zoneIndex);
	}

	// assign def cfg
	if (pMsgHdr->m_numCharConfigItems > 0) {
		// first get counts of each type/equip
		GLID_OR_BASE glidOrBase = (GLID_OR_BASE)cfgDataStruct[0].m_ccGlidOrBase;
		BYTE slotCount = 0;
		for (int i = 0; i < pMsgHdr->m_numCharConfigItems; i++) {
			if (glidOrBase != cfgDataStruct[i].m_ccGlidOrBase) {
				pPO_new->m_charConfig.addItem(glidOrBase, slotCount);
				slotCount = 0;
				glidOrBase = cfgDataStruct[i].m_ccGlidOrBase;
			}
			slotCount++;
		}
		pPO_new->m_charConfig.addItem(glidOrBase, slotCount); // do last one (or first if only one)

		for (size_t i = 0; i < (size_t)pMsgHdr->m_numCharConfigItems; i++) {
			if (cfgDataStruct[i].m_ccMeshId >= 0) {
				// Set given deformable mesh index to the given configuration
				pPO_new->m_charConfig.setItemSlotMeshId(cfgDataStruct[i].m_ccGlidOrBase, cfgDataStruct[i].m_ccSlot, cfgDataStruct[i].m_ccMeshId);
				pPO_new->m_charConfig.setItemSlotMaterialId(cfgDataStruct[i].m_ccGlidOrBase, cfgDataStruct[i].m_ccSlot, cfgDataStruct[i].m_ccMatlId);
				pPO_new->m_charConfig.setItemSlotColor(cfgDataStruct[i].m_ccGlidOrBase, cfgDataStruct[i].m_ccSlot, cfgDataStruct[i].m_ccR, cfgDataStruct[i].m_ccG, cfgDataStruct[i].m_ccB);
			}
		}
	}

	// see if we can add if via the database
	try {
		if (m_gameStateDb != 0) {
			// see if someone wants to check for valid config
			// note that it is considered to be a hack if this
			// fails and we do nothing (handler can do anything it wants)
			if (m_dispatcher.HasHandler(m_validatePlayerId)) {
				IEvent* e = m_dispatcher.MakeEvent(m_validatePlayerId);
				jsAssert(e != 0);
				if (e != 0) {
					e->SetFilter(0);
					e->SetFrom(netIdFrom);
					(*e->OutBuffer()) << (ULONG)(IGetSet*)pPO_new;
					pPO_new->m_charConfig.insertIntoEvent(e); // since not available via getset

					m_dispatcher.ProcessSynchronousEventNoRelease(e); // Keep the event

					// filter is return code
					if (e->Filter() != 0) {
						if (pPO_update != 0) {
							// reload the player if updating
							m_gameStateDb->GetPlayer(pPO_new);
						}

						e->Release(); // Release now
						return false;
					}

					e->Release(); // Release now
				}
			}

			if (pPO_update != 0) {
				auto pPO_rt = GetPlayerObjectByNetId(netIdFrom, false);
				if (pPO_rt) {
					//Update this now so when the player spawns again the ActivateMovementEntity on the client
					//receives the new actor to use, otherwise the update will happen in a later part of the
					//spawn process and the actor will not appear to have updated.  It would take two spawns to
					//get the update without this section.
					pPO_rt->m_dbCfg = pPO_new->m_dbCfg;
					pPO_rt->m_curEDBIndex = pPO_new->m_curEDBIndex;
					pPO_rt->m_originalEDBCfg = pPO_new->m_originalEDBCfg;
				}

				// don't update *everything*
				// not IGameState::SKILLS | IGameState::BANK_INVENTORY | IGameState::GETSETS | IGameState::NOTERIETIES | IGameState::PLAYER_HOUSE_INVENTORY | IGameState::CURRENT_POSITION |IGameState::HOUSING | IGameState::CLAN_HOUSE_INVENTORY |
				m_gameStateDb->UpdatePlayer(pPO_new, 
					PlayerMask::STATS |
					PlayerMask::QUESTS |
					PlayerMask::CLAN |
					PlayerMask::PLAYER |
					PlayerMask::INVENTORY |
					PlayerMask::DEFCONFIG |
					PlayerMask::RESPAWN_POINT |
					PlayerMask::SPAWN_INFO |
					PlayerMask::PLAYER_EDB);

				PlayerMask::ALL_DATA & ~(PlayerMask::BANK_INVENTORY | PlayerMask::CLAN);

			} else {
				m_gameStateDb->CreatePlayer(pAO, pPO_new);
				pAO->m_pPlayerObjectList->AddTail(pPO_new);
				_pPO_new.release(); // don't delete newPobj since succeeded!
			}
		}

		// successful
		LogInfo(pAO->m_accountName << " created a new character " << pPO_new->m_charName << " with dbconfig of " << pPO_new->m_dbCfg);
		if (m_dispatcher.HasHandler(m_playerCreatedId)) {
			IEvent* e = m_dispatcher.MakeEvent(m_playerCreatedId);
			jsAssert(e != 0);
			if (e != 0) {
				e->SetFrom(netIdFrom);
				(*e->OutBuffer()) << (ULONG)(IGetSet*)pPO_new;

				m_dispatcher.ProcessSynchronousEvent(e);
			}
		}

		return true;
	} catch (KEPException* e) {
		LogError("Error creating account for account: " << pAO->m_accountName << " :" << e->ToString());
		e->Delete();
		return false;
	}
}

bool ServerEngine::HandleMsgEquipToServer(BYTE* pData, NETID netIdFrom) {
	if (!pData)
		return false;

	auto pMsg = (EQUIP_DATA*)pData;

	// Unpack Message Parameters
	int netTraceId = (int)pMsg->netTraceId; // from (short)
	size_t glids = (size_t)pMsg->glids; // from (unsigned char)
	GLID* pGlids = (GLID*)(((BYTE*)pMsg) + sizeof(EQUIP_DATA));

	// Get Player
	auto pPO = GetPlayerObjectByNetTraceId(netTraceId);
	if (!pPO || !pPO->m_inventory) {
		LogError("GetPlayerObjectByNetTraceId() FAILED - netTraceId=" << netTraceId);
		return false;
	}

	// Process Special Glids
	for (size_t i = 0; i < glids; ++i) {
		// Extract GLID (<0=disarm >0=arm -9999=notTryOn)
		GLID glidSpecial = pGlids[i];
		bool notTryOn = (glidSpecial == -9999);
		GLID glid = (notTryOn ? 0 : abs(glidSpecial));

		// Can't Be Try On If In Global Inventory
		auto pIO = (CInventoryObj*)pPO->m_inventory->VerifyInventory(glid);
		bool isTryOn = !(pIO || notTryOn);

		// Try On Item ? (not in global inventory)
		if (isTryOn) {
			if (!TryOnInventoryItem(pPO, glidSpecial, netIdFrom)) {
				LogError("TryOnInventoryItem() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
				return false;
			}
			return true;
		}

		// Equip Inventory Item
		if (!EquipInventoryItem(pPO, glidSpecial, netIdFrom)) {
			LogError("EquipInventoryItem() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
			return false;
		}

		// Update 3D App Inventory (sync with global inventory)
		m_gameStateDb->updateInventory3DApp(pPO->m_handle, pPO->m_inventory, PlayerMask::INVENTORY);

		// Send PlayerArmItemEvent To Player With Special Glid
		IEvent* e = m_dispatcher.MakeEvent(m_playerArmItemEventId);
		if (!e)
			return false;
		e->SetFrom(netTraceId);
		(*e->OutBuffer()) << netTraceId << (int)glidSpecial;
		if (!m_dispatcher.ProcessSynchronousEvent(e)) {
			return false;
		}
	}

	return true;
}

} // namespace KEP