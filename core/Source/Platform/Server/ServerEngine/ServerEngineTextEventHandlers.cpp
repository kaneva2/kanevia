///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ServerEngine.h"

#include "Audit.h"
#include "CAccountClass.h"
#include "CMultiplayerClass.h"
#include "CCommerceClass.h"
#include "CBankClass.h"
#include "CSafeZones.h"
#include "CSecureTradeClass.h"
#if (DRF_OLD_PORTALS == 1)
#include "CPortalSysClass.h"
#endif
#include "CElementsClass.h"
#include "CCollisionClass.h"
#include "CGmPageClass.h"
#include "CWorldChannelRules.h"
#include "CBattleSchedulerClass.h"
#if (DRF_OLD_VENDORS == 1)
#include "CServerItemGen.h"
#endif
#include "CServerItemClass.h"
#include "CAIICollectionQuest.h"
#include "CQuestJournal.h"
#include "CCurrencyClass.h"
#include "CPlayerClass.h"
#include "CWorldAvailableClass.h"
#include <PassList.h>
#include "IGameState.h"
#include "LocaleStrings.h"
#include "BlockType.h"

namespace KEP {

static LogInstance("ServerEngine");

BOOL ServerEngine::LootInventoryRequest(CPlayerObject* pPO, NETID idFromLoc) {
	if (!m_multiplayerObj->m_runtimePlayersOnServer)
		return FALSE;

	if (!pPO || pPO->m_aiControlled || pPO->m_currentChannel.isArena() || (pPO->m_gm == TRUE && pPO->m_admin == FALSE))
		return FALSE;

	auto pSIO = m_multiplayerObj->m_pServerItemObjMap->GetItemInRadius(
		pPO->InterpolatedPos(),
		(pPO->m_dynamicRadius * 2.0f),
		pPO->m_currentChannel);
	if (!pSIO) {
		QueueMsgGeneralToClient(LS_GM_NOTHING_HERE, pPO);
		return TRUE;
	}

	if (pSIO->m_type == SI_TYPE_AI_QUEST) {
		if (pSIO->m_collectionQuestDB) {
			if (pSIO->m_collectionQuestDB->GetCount() > 0) {
				// valid
				int textMenuLength = 0;
				int showCount = 1;
				if (pPO->playerOrigRef()) {
					if (pPO->playerOrigRef()->m_questJournal) {
						CQuestJournalObj* journalEntry = pPO->playerOrigRef()->m_questJournal->GetBySeer(pSIO->m_displayedName);
						if (journalEntry) {
							showCount = journalEntry->m_currentProgression + 1;

							// auto select option
							CAICollectionQuestObj* questTemp = (CAICollectionQuestObj*)pSIO->m_collectionQuestDB->GetByIndex(journalEntry->m_currentProgression);
							if (questTemp) {
								if (questTemp->m_progessBasedByQuestJournal) {
									if (!questTemp->m_listProgressionOptions) {
										int selectionTemp = journalEntry->m_currentProgression;
										HandleQuestSys(pPO, questTemp, pSIO, idFromLoc, selectionTemp);
										return TRUE;
									}
								}
							}
						}
					}
				}

				int loopTraceTemp = 0;
				int fullEntryCountLogged = 0;
				int fullEntryCountSel = 0;
				for (POSITION qPos = pSIO->m_collectionQuestDB->GetHeadPosition(); qPos != NULL;) {
					// acquire full data length
					CAICollectionQuestObj* questPtr = (CAICollectionQuestObj*)pSIO->m_collectionQuestDB->GetNext(qPos);
					if (questPtr->m_progessBasedByQuestJournal) {
						if (loopTraceTemp >= showCount) {
							continue;
						}
					}

					textMenuLength += questPtr->m_title.GetLength() + 1;
					fullEntryCountSel = loopTraceTemp;
					loopTraceTemp++;
					fullEntryCountLogged++;
				}

				if (fullEntryCountLogged == 1) {
					// only one selection-auto select this one
					CAICollectionQuestObj* questTemp = (CAICollectionQuestObj*)pSIO->m_collectionQuestDB->GetByIndex(fullEntryCountSel);
					if (questTemp) {
						HandleQuestSys(pPO, questTemp, pSIO, idFromLoc, fullEntryCountSel);
						return TRUE;
					}
				}

				if (textMenuLength <= 0) {
					CStringA ignoreMsg = pSIO->m_noResponseMsg;
					m_multiplayerObj->QueueOrSendRenderTextEvent(idFromLoc, ignoreMsg, eChatType::Talk);
				}
			}
		}
	}

	return true;
}

static bool canDoTrade(CPlayerObject* pPO_from, CPlayerObject* pPO_dest) {
	return !pPO_dest->m_aiControlled && (pPO_dest->m_currentChannel == pPO_from->m_currentChannel) && (pPO_dest->m_netTraceId != pPO_from->m_netTraceId) && (pPO_dest->m_interactionStates == AVAILABLE);
}

BOOL ServerEngine::TradeInventoryRequest(CPlayerObject* pPO_initiator, NETID idFromLoc, CPlayerObject* pPO_dest) {
	if (!m_multiplayerObj->m_runtimePlayersOnServer)
		return FALSE;

	if (!pPO_initiator)
		return FALSE;

	if (pPO_dest) {
		if (m_secureTradeDB->IsPlayerInvolvedInTrade(pPO_initiator->m_netTraceId) ||
			m_secureTradeDB->IsPlayerInvolvedInTrade(pPO_dest->m_netTraceId)) {
			RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::System, LS_PLAYER_TRADING);
			return FALSE;
		}
	}

	if (pPO_dest != 0) { // passing in a player to trade with
		if (!canDoTrade(pPO_initiator, pPO_dest))
			return FALSE;
	} else { // find dest play via radius
		for (POSITION posLoc = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); posLoc != NULL;) {
			auto pPO_find = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(posLoc);
			if (!canDoTrade(pPO_initiator, pPO_find))
				continue;

			Vector3f pos_find = pPO_find->InterpolatedPos();
			Vector3f pos_initiator = pPO_initiator->InterpolatedPos();
			if (!MatrixARB::BoundingSphereCheck(
					pPO_find->m_dynamicRadius,
					pPO_initiator->m_dynamicRadius,
					pos_find.x,
					pos_find.y,
					pos_find.z,
					pos_initiator.x,
					pos_initiator.y,
					pos_initiator.z))
				continue;

			if (!m_secureTradeDB->IsPlayerInvolvedInTrade(pPO_find->m_netTraceId)) {
				pPO_dest = pPO_find;
				break;
			}
		}
	}

	if (pPO_dest != 0) {
		if (m_gameStateDb && m_gameStateDb->IsUserBlocked(pPO_initiator->m_pAccountObject, 0, pPO_dest->m_pAccountObject->m_serverId, BlockType::Ignore, pPO_dest->m_charName)) {
			return FALSE;
		}

#define Trade_MenuID 68
		ATTRIB_DATA attribData;
		attribData.aeType = eAttribEventType::OpenMenu;
		attribData.aeInt1 = pPO_dest->m_netTraceId;
		attribData.aeInt2 = Trade_MenuID;
		m_multiplayerObj->QueueMsgAttribToClient(idFromLoc, attribData);

		attribData.aeInt1 = pPO_initiator->m_netTraceId;
		m_multiplayerObj->QueueMsgAttribToClient(pPO_dest->m_netId, attribData);

		if (m_gameStateDb) {
			const char* playerName = pPO_initiator->m_charName;
			try {
				pPO_initiator->m_cash->m_amount = m_gameStateDb->GetPlayersCash(pPO_initiator);
				playerName = pPO_dest->m_charName;
				pPO_dest->m_cash->m_amount = m_gameStateDb->GetPlayersCash(pPO_dest);
			} catch (KEPException* e) {
				LogError("Error getting player's cash for trade " << playerName << " " << e->m_msg);
				e->Delete();
			}
		}

		// no need to refresh inventory here since caller gets inv separately

		if (pPO_initiator->m_inventory) {
			SendMsgBlockItemToClient(pPO_initiator->m_inventory, TRUE, idFromLoc, 57, pPO_initiator->m_cash, pPO_initiator, FALSE);
		}

		if (pPO_dest->m_inventory) {
			SendMsgBlockItemToClient(pPO_dest->m_inventory, TRUE, pPO_dest->m_netId, 57, pPO_dest->m_cash, pPO_dest, FALSE);
		}

		pPO_initiator->m_interactionStates |= TRADING;
		pPO_dest->m_interactionStates |= TRADING;

		// open secure Trade Object
		m_secureTradeDB->InitiateTrade(pPO_initiator->m_netTraceId,
			pPO_dest->m_netTraceId,
			m_multiplayerObj->m_runtimePlayersOnServer);

		// send each others passlist so know what they can take
		m_dispatcher.QueueEvent(new PassListEvent(pPO_initiator->m_netId, pPO_dest->m_netTraceId, pPO_dest->m_passList ? &pPO_dest->m_passList->ids() : 0, pPO_dest->m_passList ? &pPO_dest->m_passList->expirations() : 0));
		m_dispatcher.QueueEvent(new PassListEvent(pPO_dest->m_netId, pPO_initiator->m_netTraceId, pPO_initiator->m_passList ? &pPO_initiator->m_passList->ids() : 0, pPO_initiator->m_passList ? &pPO_initiator->m_passList->expirations() : 0));

		return TRUE;
	} else {
		return FALSE;
	}
}

BOOL ServerEngine::HandleReassignRespawn(CPlayerObject* pPO, NETID idFromLoc) {
	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogError("Error locating account for NETID " << idFromLoc);
		return FALSE;
	}

	if (!pAO->m_pPlayerObjectList) {
		LogError("No characters created yet for account " << pAO->m_accountName);
		return FALSE;
	}

	if (!(pPO->m_currentZoneIndex.isHousing() && pPO->m_currentZoneIndex.GetInstanceId() == pPO->m_handle) &&
		!pPO->m_currentZoneIndex.isPermanent()) {
		SendTextMessage(pPO, "You can only set rebirth to your apartment or a permanent zone", eChatType::System);
		return FALSE;
	}

	auto pPO_cur = pAO->GetCurrentPlayerObject();
	if (pPO_cur) {
		// valid char on account
		ZoneIndex zoneIndex = m_multiplayerObj->m_currentWorldsUsed->GetZeroBasedIndex(pPO->m_currentWorldMap);
		if (zoneIndex != INVALID_ZONE_INDEX) {
			Vector3f pos = pPO->InterpolatedPos();
			pPO_cur->m_respawnPosition.x = pos.x;
			pPO_cur->m_respawnPosition.y = pos.y + 2.0; // 2' above the ground
			pPO_cur->m_respawnPosition.z = pos.z;
			pPO_cur->m_respawnZoneIndex = pPO->m_currentZoneIndex;

			pPO->m_dbDirtyFlags |= PlayerMask::RESPAWN_POINT;

			QueueMsgGeneralToClient(LS_GM_RESPAWN, pPO);
		}
	}

	return TRUE;
}

BOOL ServerEngine::HandleReassignRespawnByDetails(CPlayerObject* pPO, NETID idFromLoc, Vector3f position, const ZoneIndex zoneIndex) {
	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogError("Error locating account for NETID " << idFromLoc << " in HandleReassignRespawnByDetails");
		return FALSE;
	}

	if (!pAO->m_pPlayerObjectList) {
		LogError("No characters created yet for account " << pAO->m_accountName);
		return FALSE;
	}

	auto pPO_cur = pAO->GetCurrentPlayerObject();
	if (pPO_cur) {
		// valid char on account
		if (zoneIndex != INVALID_ZONE_INDEX) {
			pPO_cur->m_respawnPosition.x = position.x;
			pPO_cur->m_respawnPosition.y = position.y;
			pPO_cur->m_respawnPosition.z = position.z;
			pPO_cur->m_respawnZoneIndex = zoneIndex; // pData->m_currentWorldID;  //wi is this ok?

			pPO->m_dbDirtyFlags |= PlayerMask::RESPAWN_POINT;

			QueueMsgGeneralToClient(LS_GM_RESPAWN, pPO);
		}
	}

	return TRUE;
}

} // namespace KEP