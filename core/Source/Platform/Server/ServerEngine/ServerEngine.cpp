///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ServerEngine.h"
#include "Shlwapi.h"
#include "Common/include/UgcConstants.h"
#include "Common/KEPUtil/Plugin.h"
#include "core/CorePluginLibrary/Metrics.h"
#include "EngineStrings.h"
#include "resource.h"
#include "ServerSerialize.h"
#include "DBStrings.h"
#include "Audit.h"
#include "Common/include/logtimings.h"
#include <comutil.h>
#include "CCommerceClass.h"
#include "CBankClass.h"
#include "CSafeZones.h"
#include "CSecureTradeClass.h"
#if (DRF_OLD_PORTALS == 1)
#include "CPortalSysClass.h"
#endif
#include "CElementsClass.h"
#include "CCollisionClass.h"
#include "CGmPageClass.h"
#include "CWorldChannelRules.h"
#include "CBattleSchedulerClass.h"
#if (DRF_OLD_VENDORS == 1)
#include "CServerItemGen.h"
#endif
#include "CServerItemClass.h"
#include "CAIRaidClass.h"
#include "CElementsClass.h"
#include "CAccountClass.h"
#include "CMovementObj.h"
#include "RuntimeSkeleton.h"
#include "CArmedInventoryClass.h"
#include "CFireRangeClass.h"
#include "CMissileClass.h"
#include "CDamagePathClass.h"
#include "CABD3DFunctionLib.h"
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "AIclass.h"
#include "CPacketQueClass.h"
#include "CPhysicsEmuClass.h"
#include "CPlayerClass.h"
#include "CTriggerClass.h"
#include "CSkillClass.h"
#include "CTitleObject.h"
#include "CWorldAvailableClass.h"
#include "CEXScriptClass.h"
#include "KEPConfigurationsXML.h"
#include "SlashCommand.h"
#include "CMultiplayerClass.h"
#include "TriggerFile.h"
#include "ScriptAttributes.h"
#include <SerializeHelper.h>
#include "ScriptPlayerInfo.h"
#include "UseTypes.h"
#include <PassList.h>
#include "PassGroups.h"
#include "WebIDsHelper.h"
#include "Common/KEPUtil/DistributionInfo.h"
#include "Common/KEPUtil/ExtendedConfig.h"
#include "Common/include/DBConfigValues.h"
#include "Common/KEPUTIL/ParamString.h"
#include "common/KEPUtil/VLDWrapper.h"
#include "../Tools/HttpClient/GetBrowserPage.h"
#include "common/KEPUtil/LocationRegistry.h"
#include "../Tools/NetworkV2/Protocols.h"
#include "UnicodeMFCHelpers.h"
#include "..\..\Client\CharConfigStatic.cpp"
#include "Engine/Messages/MsgSpawnStaticMovementObj.h"
#include "include\Core\Math\Rotation.h"
#include "CFileFactory.h"
#include "../KEPServer/KEPServerEnums.h"
#include "../Database/MySqlGameState.h"
#include "../Database/GameStateDispatcher.h"
#include "SerializeFromFile.h"
#include "time.h"
#include "LocaleStrings.h"

// Including this header file works around DLLMain initialization issue when
// Using both boost/thread and MFC in a DLL.
// MFC code has this assertion in AfxCoreInitModule():
//
// ASSERT(AfxGetModuleState() != AfxGetAppModuleState());
//
// This is caused by boost::thread's DLLMain replacement function defined in
// [boost_source]/libs/thread/src/win32/tss_pi.cpp
// Including the following header causes the boost code to ensure the MFC initialization code gets executed.
#include "boost/thread/win32/mfc_thread_init.hpp"

using namespace mysqlpp;

namespace KEP {

#define SERVER_GBP_THREADS 4

#define HEARTBEAT_MONITOR_WINDOW_CLASS L"kanevaKGPServerHBMonitorClass"
#define HEARTBEAT_MONITOR_WINDOW_TITLE L"kanevaKGPServerHBMonitorWindow"
#define KGPSERVER_HEARTBEAT_MESSAGE L"kanevaMsgKGPServerHeartbeat"
#define KGPSERVER_ALERT_MESSAGE L"kanevaMsgKGPServerAlert"
#define HEARTBEAT_MONITOR_HWND_REFRESH_INTERVAL_MS (60.0 * MS_PER_SEC)
#define KGPSERVER_HEARTBEAT_INTERVAL_MS (10.0 * MS_PER_SEC)
#define SPAWN_EXPIRY_REVIEW_INTERVAL_MS (10.0 * MS_PER_SEC)
#define SPAWN_EXPIRY_TIMEOUT_MS (60.0 * MS_PER_SEC)

//Events for Incubator AlertService module
#define KGPSERVER_EVENT_BASE 3000000
#define KGPSERVER_EVENT_NETWORK_INIT_FAILED 3000101
#define KGPSERVER_EVENT_DB_INIT_FAILED 3000102
#define KGPSERVER_EVENT_MAX 3999999

#define SEND_STRING_LIST 50 //This is the value the client sends from login and the server returns when retrieving characters.
//Cant find any real documentation for it other than the lua.

#define CC_ZONE_INDEX_PLAIN 49 //zone_index for character creation.

static ULONG SERVER_VERSION = PackVersion(1, 0, 0, 0);
static const TimeMs MAX_LOGON_TIME_MS = 10.0 * 1000.0; // how long should logon second step take before considered failed
static const char* const SAVEONEXIT_TAG = "SaveOnExit";
static const char* const GAMESTATE_TAG = "GameStateFileName";
static const char* const KEY_TAG = "RegistrationKey";
static const char* const BROADCASTOPENARENA_TAG = "BroadcastOpenArena";
static const char* const ADMIN_ONLY_TAG = "AdminOnly";
const char* const ENGINEKEY_TAG = "RegistrationKey";
static const char* const LOGINMSG_DELAY_TAG = "LoginMsgDelay";

static const char* const PLAYER_METADATA_FILE = "PlayerMD.xml";

static const char* const IPADDR_CONFIG = "ipaddress.xml";
static const char* const IPADDR_CONFIG_ROOT = "Config";
static const char* const DEVELOPER_CONFIG = "developer.cfg";
static const char* const DEVELOPER_CONFIG_ROOT = "developer";
static const char* const OAUTH_TAG = "oauth";
static const char* const OAUTH_APPID_TAG = "appid";
static const char* const OAUTH_KEY_TAG = "key";
static const char* const OAUTH_SECRET_TAG = "secret";
static const char* const TMPLT_TAG = "template";
static const char* const TMPLT_ACHIEVEMENT_TAG = "achievement";
static const char* const TMPLT_ACHIEVEMENT_NAME_ATTR = "name";
static const char* const TMPLT_PREMIUMITEM_TAG = "premiumitem";
static const char* const TMPLT_PREMIUMITEM_NAME_ATTR = "name";
static const char* const WEBIDS_LUA_PATH = "..\\ScriptServerScripts\\SavedScripts\\WebIDs.lua";
static const char* const SOURCE_DIR_WEBIDS_LUA_PATH = "GameFiles\\ScriptServerScripts\\SavedScripts\\WebIDs.lua";

static const int SPAWN_ERROR = -1;
static const int SPAWN_LOCAL = 0;
static const int SPAWN_REMOTE = 1;

#ifdef _DEBUG
static const std::string ScriptBladesSubDir = "BladeScriptsd";
#else
static const std::string ScriptBladesSubDir = "BladeScripts";
#endif

static LogInstance("ServerEngine");
LogInstanceAs(ServerEngine::m_timingLogger, "Timing");

/**
* Safe string replacement for unsafe ctime(), good until Dec 31, 3000.
*/
std::string ctimeSafe(const time_t* tmr) {
#define CTIME_STR_LEN 26
	char ctimeStr[CTIME_STR_LEN];
	ctime_s(ctimeStr, CTIME_STR_LEN, tmr);
	return std::string(ctimeStr);
}

static EVENT_ID pingerEventId = BAD_EVENT_ID;

CAccountObject* ServerEngine::GetAccountObjectByNetId(const NETID& netId) const {
	return (m_multiplayerObj && m_multiplayerObj->m_runtimeAccountDB) ?
			   m_multiplayerObj->m_runtimeAccountDB->GetAccountObjectByNetId(netId) :
			   NULL;
}

CPlayerObject* ServerEngine::GetPlayerObjectByServerId(LONG playerServerId) const {
	if (!m_multiplayerObj || !m_multiplayerObj->m_accountDatabase)
		return NULL;

	auto pAO = m_multiplayerObj->m_accountDatabase->GetAccountObjectByServerId(playerServerId);
	if (!pAO)
		return NULL;

	auto pPO = pAO->GetCurrentPlayerObject();
	if (!pPO)
		return NULL;

	PLAYER_HANDLE playerHandle = pPO->m_handle;
	return GetPlayerObjectByPlayerHandle(playerHandle);
}

CPlayerObject* ServerEngine::GetPlayerObjectByName(const std::string& userName) const {
	return (m_multiplayerObj && m_multiplayerObj->m_runtimePlayersOnServer) ?
			   m_multiplayerObj->m_runtimePlayersOnServer->GetPlayerObjectByName(userName) :
			   nullptr;
}

CPlayerObject* ServerEngine::GetPlayerObjectByNetTraceId(int netTraceId) const {
	return (m_multiplayerObj && m_multiplayerObj->m_runtimePlayersOnServer) ?
			   m_multiplayerObj->m_runtimePlayersOnServer->GetPlayerObjectByNetTraceId(netTraceId) :
			   nullptr;
}

CPlayerObject* ServerEngine::GetPlayerObjectByNetId(const NETID& netId, bool includeSpawning) const {
	return (m_multiplayerObj && m_multiplayerObj->m_runtimePlayersOnServer) ?
			   m_multiplayerObj->m_runtimePlayersOnServer->GetPlayerObjectByNetId(netId, includeSpawning) :
			   nullptr;
}

CPlayerObject* ServerEngine::GetPlayerObjectByPlayerHandle(const PLAYER_HANDLE& handle) const {
	return (m_multiplayerObj && m_multiplayerObj->m_runtimePlayersOnServer) ?
			   m_multiplayerObj->m_runtimePlayersOnServer->GetPlayerObjectByPlayerHandle(handle) :
			   nullptr;
}

CPlayerObject* ServerEngine::GetPlayerObjectByAliveAndAccountNumber(int accNum) const {
	return (m_multiplayerObj && m_multiplayerObj->m_runtimePlayersOnServer) ?
			   m_multiplayerObj->m_runtimePlayersOnServer->GetPlayerObjectByAccountNumber(accNum) :
			   nullptr;
}

IPlayer* ServerEngine::GetPlayerOffAccountByNetId(NETID id, int index) {
	// index references the position of the player object on the account
	// the first player object will be used if no index is given
	if (index == -1)
		index = 0;

	IPlayer* pPlayer = 0;

	CAccountObject* pAO = GetAccountObjectByNetId(id);
	if (pAO != 0) {
		POSITION posLoc = pAO->m_pPlayerObjectList->FindIndex(index);
		if (posLoc) {
			pPlayer = (CPlayerObject*)pAO->m_pPlayerObjectList->GetAt(posLoc);
		}
	}

	return pPlayer;
}

IPlayer* ServerEngine::GetPlayerByName(const std::string& userName) {
	return dynamic_cast<IPlayer*>(GetPlayerObjectByName(userName));
}

IPlayer* ServerEngine::GetPlayerByNetId(const NETID& netId, bool includeSpawning) {
	return dynamic_cast<IPlayer*>(GetPlayerObjectByNetId(netId, includeSpawning));
}

IPlayer* ServerEngine::GetPlayerByNetTraceId(int netTraceId) {
	return dynamic_cast<IPlayer*>(GetPlayerObjectByNetTraceId(netTraceId));
}

// for reloading blades
EVENT_PROC_RC ServerEngine::AdminCommandHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* m_serverEngine = (ServerEngine*)lparam;
	jsVerifyReturn(m_serverEngine != 0, EVENT_PROC_RC::NOT_PROCESSED);

	// gm and admin
	static ULONG MIN_SERVER_ADMIN_ID = 5000;
	static ULONG MAX_SERVER_ADMIN_ID = 5999;

	ULONG intCmdId = e->Filter();
	if (intCmdId < MIN_SERVER_ADMIN_ID || intCmdId > MAX_SERVER_ADMIN_ID)
		return EVENT_PROC_RC::NOT_PROCESSED;

	auto pEvent = dynamic_cast<TextEvent*>(e);
	if (!pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	SlashCommand commandId;
	int networkAssignedId;
	std::string cmdText;
	eChatType chatType;
	pEvent->ExtractInfo(commandId, networkAssignedId, cmdText, chatType);

	auto pPO = m_serverEngine->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO || !pPO->m_admin || commandId != SlashCommand::RELOADBLADE)
		return EVENT_PROC_RC::CONSUMED;

	::STLToLower(cmdText);

	// find the event blade they want to reload
	EventBladeVector::iterator i;
	for (auto& pEB : m_serverEngine->m_eventBlades) {
		std::string bladeName = pEB->Name();
		if (cmdText.empty()) { // sent them the names
			m_serverEngine->SendTextMessage(pPO, bladeName.c_str(), eChatType::Talk);
		} else {
			STLToLower(bladeName);
			if (cmdText == bladeName || bladeName.find(cmdText) != std::string::npos) {
				if (!pEB->ReRegisterEventHandlers(&m_serverEngine->m_dispatcher, IEvent::EncodingFactory())) {
					LogError("ReRegisterEventHandlers() FAILED - '" << pEB->Name() << "'");
				}
				return EVENT_PROC_RC::CONSUMED;
			}
		}
	}

	LogError("Event Not Found");
	m_serverEngine->SendTextMessage(pPO, (loadStr(IDS_EVENT_NOT_FOUND) + cmdText).c_str(), eChatType::Talk);
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ServerEngine::RespawnToRebirthHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* m_serverEngine = (ServerEngine*)lparam;
	jsVerifyReturn(m_serverEngine != 0, EVENT_PROC_RC::NOT_PROCESSED);

	auto pPO = m_serverEngine->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO)
		return EVENT_PROC_RC::NOT_PROCESSED;

	m_serverEngine->RespawnToRebirthPoint(pPO);
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ServerEngine::TargetedPositionEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	TargetedPositionEvent* tpe = dynamic_cast<TargetedPositionEvent*>(e);
	jsVerifyReturn(tpe != 0, EVENT_PROC_RC::NOT_PROCESSED);

	auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
	if (pPO) {
		double x, y, z, nx, ny, nz;
		tpe->ExtractInfo(x, y, z, nx, ny, nz);

		pPO->SetNumber(me->GetPlayerLastClickedXId(), x);
		pPO->SetNumber(me->GetPlayerLastClickedYId(), y);
		pPO->SetNumber(me->GetPlayerLastClickedZId(), z);

		pPO->SetNumber(me->GetPlayerLastClickedSurfaceNormalXId(), nx);
		pPO->SetNumber(me->GetPlayerLastClickedSurfaceNormalYId(), ny);
		pPO->SetNumber(me->GetPlayerLastClickedSurfaceNormalZId(), nz);
	}

	return EVENT_PROC_RC::CONSUMED;
}

/// handle the response from the background
EVENT_PROC_RC ServerEngine::ZoneCustomizationBackgroundEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ZoneCustomizationBackgroundEvent* zce = dynamic_cast<ZoneCustomizationBackgroundEvent*>(e);
	jsVerifyReturn(zce != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;

	auto pPO = me->GetPlayerObjectByNetId(e->From(), false);

	// if failed and not UGC, re-add the item
	if (zce->ret != 0 && !zce->tryon) {
		if (pPO == 0) {
			if (e->From() != 0) {
				// lost player after update queued!
				LogWarn("Lost player after zone customization " << zce->charName);
			}
			return EVENT_PROC_RC::NOT_PROCESSED;
		}

		// Do not re-add if the item was not consumed (user is GM or item is unlimited)
		if (!pPO->m_gm && !(zce->inventorySubType & (IT_UNLIMITED | IT_INFINITE))) {
			if (zce->type == ZoneCustomizationBackgroundEvent::ZC_ADD || zce->type == ZoneCustomizationBackgroundEvent::ZC_ADD_FRAME) {
				CGlobalInventoryObj* invObj = CGlobalInventoryObjList::GetInstance()->GetByGLID(zce->globalId);
				if (invObj) {
					pPO->m_inventory->AddInventoryItem(1, FALSE, invObj->m_itemData, zce->inventorySubType, false);
				}
			}
		}
	}

	if (zce->replyEvent || zce->broadcastEvent) {
		if (zce->broadcastEvent) {
#ifndef REFACTOR_INSTANCEID
			LONG channel = zce->channel;
			LONG zoneId = zce->zoneIndex;
			LONG instanceId = zce->zoneIndex.GetInstanceId();
#else
			LONG channel = zce->channel.toLong();
			LONG zoneId = zce->zoneIndex.toLong();
			LONG instanceId = zce->zoneIndex.GetInstanceId();
#endif

			if (!zce->tryon) {
				if (zce->broadcastEvent->EventId() == AddDynamicObjectEvent::ClassId()) {
					ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::AddPlacementObject, zce->objPlacementId, zoneId, instanceId, zce->From());
					sse->SetFrom(zce->From());
					me->m_dispatcher.QueueEvent(sse);
				} else if (zce->broadcastEvent->EventId() == RemoveDynamicObjectEvent::ClassId()) {
					ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::RemovePlacementObject, zce->objPlacementId, zoneId, zce->From());
					sse->SetFrom(e->From());
					// Let the ScriptServerEvent handler forward it to the script server.
					// Just call the handler directly, rather than going through the dispatcher.
					me->m_dispatcher.ProcessSynchronousEvent(sse);
				} else if (zce->broadcastEvent->EventId() == MoveAndRotateDynamicObjectEvent::ClassId()) {
					ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::MovePlacementObject, zce->objPlacementId, zoneId, zce->From());
					sse->SetFrom(e->From());
					*sse->OutBuffer() << zce->x << zce->y << zce->z << zce->dx << zce->dy << zce->dz << zce->sx << zce->sy << zce->sz << zce->time;
					// Let the ScriptServerEvent handler forward it to the script server.
					// Just call the handler directly, rather than going through the dispatcher.
					me->m_dispatcher.ProcessSynchronousEvent(sse);
				}
			}

			me->BroadcastEvent(zce->broadcastEvent, channel);
		}

		if (zce->replyEvent)
			me->m_dispatcher.QueueEvent(zce->replyEvent);

		if (pPO == 0) {
			if (e->From() != 0) {
				// lost player after update queued!
				LogWarn("Lost player after zone customization " << zce->charName);
			}
			return EVENT_PROC_RC::NOT_PROCESSED;
		}

		// was it a try on and does it expire?
		if (zce->ret == 0 && zce->tryon && zce->expiredDuration > 0) {
			me->QueueMsgGeneralToClient(LS_GM_ITEM_USE_OK, pPO); // inform use ok

			// Send client expiration duration for the item.
			TryOnExpireEvent* expireEvent = new TryOnExpireEvent();
			expireEvent->InformExpiration(pPO->m_handle, zce->globalId, zce->baseGlid, (USE_TYPE)zce->useType, zce->expiredDuration, zce->objPlacementId);
			expireEvent->AddTo(e->From());
			me->m_dispatcher.QueueEvent(expireEvent);
		}
	}

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::BroadcastEventBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	BackgroundDBReplyEvent* bebe = dynamic_cast<BackgroundDBReplyEvent*>(e);
	jsVerifyReturn(lparam != 0 && bebe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;

	if (bebe->ret == 0 && bebe->event && !bebe->event->To().empty()) {
		me->m_dispatcher.QueueEvent(bebe->event);
		bebe->event = 0; // don't let bebe delete it.
	}

	return EVENT_PROC_RC::CONSUMED;
}

/// background event handler for change zone.  called when db finished processing it
EVENT_PROC_RC ServerEngine::ChangeZoneBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ChangeZoneBackgroundEvent* cze = dynamic_cast<ChangeZoneBackgroundEvent*>(e);
	jsVerifyReturn(cze != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	auto pPO = me->GetPlayerObjectByNetId(cze->From(), true);
	if (pPO == 0) {
		// lost player after update queued!
		LogWarn("Lost player after deed upgrade " << cze->charName);
		return ret;
	}

	if (cze->ret == 0) {
		// in this case no GotoPlaceEvent will be fired because we stay in current zone. Need to carry over the zone permissions for upcoming spawn request.
		auto pAO = pPO->m_pAccountObject;
		if (pAO) {
			// Save current zone privilege in pending spawn info
			pAO->GetSpawnInfo().setZonePermissions(pPO->m_currentZonePriv);
			pAO->GetSpawnInfo().setParentZoneInstanceId(pPO->m_parentZoneInstanceId);

			// Update flag to force sending PlayerDepart event on spawn
			auto pPlayerOnAccount = pAO->GetCurrentPlayerObject(); // Getting player object from account instead of runtime list - See SendMsgSpawnToClient
			assert(pPlayerOnAccount != nullptr);
			if (pPlayerOnAccount != nullptr) {
				pPlayerOnAccount->m_forcePlayerDepartEventOnSpawn = true;
			}
		}

		// zone them to their new zone
		me->SpawnToPointInWld(pPO, cze->spawnPointCfg->m_position, cze->newZoneIndex, DT_ValidatedZone, cze->spawnPointCfg->m_rotation, cze->url.c_str());

		ret = EVENT_PROC_RC::CONSUMED;
	} else {
		if (!pPO->m_gm && cze->reAddInvOnFail)
			me->AddInventoryItemToPlayer(pPO, 1, FALSE, cze->globalId, cze->invType, false); // give it back to them

		// if apt, reset the apt
		if (pPO->playerOrigRef()->m_respawnZoneIndex.isHousing()) {
			pPO->m_housingZoneIndex = cze->oldZoneIndex;
			pPO->m_dbDirtyFlags &= ~PlayerMask::RESPAWN_POINT;
			pPO->playerOrigRef()->m_respawnZoneIndex = cze->oldZoneIndex;
			pPO->playerOrigRef()->m_dbDirtyFlags &= ~PlayerMask::RESPAWN_POINT;
#ifndef REFACTOR_INSTANCEID
			CSpawnCfgObj* sc = me->m_multiplayerObj->GetSpawnPointCfg(cze->oldZoneIndex, 0);
#else
			CSpawnCfgObj* sc = me->m_multiplayerObj->GetSpawnPointCfg(cze->oldZoneIndex.toLong(), 0);
#endif
			if (sc != 0) {
				pPO->playerOrigRef()->m_respawnPosition = sc->m_position;
			} else {
				LogWarn("Spawn point missing for reset of apartment for " << pPO->m_charName << " for zone " << numToString(cze->oldZoneIndex.toLong()));
			}
		}

		LogWarn(loadStr(IDS_CHANGE_APT_DBERR) << pPO->m_charName << " " << cze->ret);
		RenderLocaleTextEvent::SendLocaleTextTo(me->m_dispatcher, pPO->m_netId, eChatType::System, LS_CANT_UPGRADE_ZONE_138);
	}

	return ret;
}

EVENT_PROC_RC ServerEngine::ControlDynamicObjectEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	ControlDynamicObjectEvent* evt = dynamic_cast<ControlDynamicObjectEvent*>(e);
	jsVerifyReturn(evt != 0, EVENT_PROC_RC::NOT_PROCESSED);

	START_TIMING(m_timingLogger, "DB:SetDynamicObjectAnimationEventHandler");

	std::vector<int> ctlArgs;
	int placementId;
	ULONG ctlId;
	evt->ExtractInfo(placementId, ctlId, ctlArgs);

	if (placementId <= 0) {
		LogWarn("Received SetDynamicObjectAnimationEvent with a local (or zero) placement ID -- check Client implementation");
	}

	// make sure the player is a gm or owner
	auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO) {
		LogError("Player not found by NET ID " << e->From());
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	if (pPO->m_currentZonePriv > ZP_MODERATOR) {
		LogError("Moderators have limited permissions - " << pPO->ToStr());
		me->SendTextMessage(pPO, "Moderators have limited permissions.", eChatType::System);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	if (ctlArgs[0] == 0) {
#ifndef REFACTOR_INSTANCEID
		me->ObjectControl(placementId, pPO->m_currentZoneIndex, e->From(), ctlId, ctlArgs);
#else
		me->ObjectControl(placementId, pPO->m_currentZoneIndex.toLong(), e->From(), ctlId, ctlArgs);
#endif
		return EVENT_PROC_RC::OK;
	}

	// validate anim in players inventory
	auto pIO = (CInventoryObj*)pPO->m_inventory->VerifyInventory(ctlArgs[0]);
	if (!pIO) {
		me->SendTextMessage(pPO, "Animation must be player's inventory to use.", eChatType::System);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	PassList* zonePassList = me->GetZonePassList(pPO->m_currentZoneIndex);
	if (!PassList::canPlayerUseItem(pPO->m_passList, pIO->m_itemData->m_passList)) {
		me->SendTextMessage(pPO, "Player does not have the required pass to use animation.", eChatType::System);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	if (!PassList::canUseItemInZone(pIO->m_itemData->m_passList, zonePassList)) {
		me->SendTextMessage(pPO, "The animation cannot be used in this zone due to pass violations.", eChatType::System);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	// act on the control message using the same function used when the script server sends
	// its version of a DO control message
#ifndef REFACTOR_INSTANCEID
	me->ObjectControl(placementId, pPO->m_currentZoneIndex, e->From(), ctlId, ctlArgs);
#else
	me->ObjectControl(placementId, pPO->m_currentZoneIndex.toLong(), e->From(), ctlId, ctlArgs);
#endif
	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::RotateDynamicObjectEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	RotateDynamicObjectEvent* evt = dynamic_cast<RotateDynamicObjectEvent*>(e);
	jsVerifyReturn(evt != 0, EVENT_PROC_RC::NOT_PROCESSED);

	START_TIMING(m_timingLogger, "DB:RotateDynamicObjectEventHandler");

	int placementId;
	double dx, dy, dz, sx, sy, sz;
	DWORD timeMs;

	evt->ExtractInfo(placementId, dx, dy, dz, sx, sy, sz, timeMs);
	if (placementId < 0) {
		LogWarn("Received RotateDynamicObjectEvent with a local placement ID -- check Client implementation");
	}

	// update the table, making sure the player is a gm or owner
	auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO) {
		LogError("Player not found by NET ID " << e->From());
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	if (pPO->m_currentZonePriv > ZP_MODERATOR) {
		LogError(loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
		me->SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	// _finite returns 0 if +/- inifinity or NaN
	if (_finite(dx) == 0 ||
		_finite(dy) == 0 ||
		_finite(dz) == 0 ||
		_finite(sx) == 0 ||
		_finite(sy) == 0 ||
		_finite(sz) == 0) {
		LogInfo("Position out of range in RotateDynamicObjectEventHandler for " << pPO->m_charName << " " << dx << "," << dy << "," << dz << " " << sx << "," << sy << "," << sz);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	// Queue event for background processing
	ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
	zce->type = ZoneCustomizationBackgroundEvent::ZC_ROTATE;
	zce->playerId = pPO->m_handle;
	zce->charName = pPO->m_charName;
	zce->objPlacementId = placementId;
	zce->dx = dx;
	zce->dy = dy;
	zce->dz = dz;
	zce->sx = sx;
	zce->sy = sy;
	zce->sz = sz;
	zce->time = timeMs;
	zce->channel = pPO->m_currentChannel;
	zce->zoneIndex = pPO->m_currentZoneIndex;
	zce->kanevaUserId = pPO->m_pAccountObject->m_serverId;
	zce->SetFrom(e->From());
	me->m_gameStateDispatcher->QueueEvent(zce);
	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::MoveDynamicObjectEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	return EVENT_PROC_RC::NOT_PROCESSED;
}

EVENT_PROC_RC ServerEngine::MoveAndRotateDynamicObjectEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	MoveAndRotateDynamicObjectEvent* evt = dynamic_cast<MoveAndRotateDynamicObjectEvent*>(e);
	jsVerifyReturn(evt != 0, EVENT_PROC_RC::NOT_PROCESSED);

	START_TIMING(m_timingLogger, "DB:MoveAndRotateDynamicObjectEventHandler");

	int placementId;
	double x, y, z, dx, dy, dz, sx, sy, sz;
	DWORD timeMs;
	evt->ExtractInfo(placementId, x, y, z, dx, dy, dz, sx, sy, sz, timeMs);

	if (placementId < 0) {
		LogWarn("Received MoveAndRotateDynamicObjectEvent with a local placement ID -- check Client implementation");
	}

	// update the table, making sure the player is a gm or owner
	auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO) {
		LogError("Player not found by NET ID " << e->From());
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	if (pPO->m_currentZonePriv > ZP_MODERATOR) {
		LogError(loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
		me->SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	// _finite returns 0 if +/- inifinity or NaN
	if (_finite(x) == 0 ||
		_finite(y) == 0 ||
		_finite(z) == 0 ||
		_finite(dx) == 0 ||
		_finite(dy) == 0 ||
		_finite(dx) == 0 ||
		_finite(sy) == 0 ||
		_finite(sx) == 0 ||
		_finite(sz) == 0) {
		LogDebug("Position out of range in MoveAndRotateDynamicObjectEventHandler for " << pPO->m_charName << " " << x << "," << y << "," << z << " " << dx << "," << dy << "," << dz << " " << sx << "," << sy << "," << sz);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
	zce->type = ZoneCustomizationBackgroundEvent::ZC_MOVE;
	zce->playerId = pPO->m_handle;
	zce->charName = pPO->m_charName;
	zce->objPlacementId = placementId;
	zce->x = x;
	zce->y = y;
	zce->z = z;
	zce->dx = dx;
	zce->dy = dy;
	zce->dz = dz;
	zce->sx = sx;
	zce->sy = sy;
	zce->sz = sz;
	zce->time = timeMs;
	zce->channel = pPO->m_currentChannel;
	zce->zoneIndex = pPO->m_currentZoneIndex;
	zce->kanevaUserId = pPO->m_pAccountObject->m_serverId;
	zce->SetFrom(e->From());
	me->m_gameStateDispatcher->QueueEvent(zce);
	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::PlaceDynamicObjectAtPositionEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	PlaceDynamicObjectAtPositionEvent* evt = dynamic_cast<PlaceDynamicObjectAtPositionEvent*>(e);
	jsVerifyReturn(evt != 0, EVENT_PROC_RC::NOT_PROCESSED);
	int glid;
	short invenType;
	float x, y, z;
	float dx, dy, dz;
	evt->ExtractInfo(glid, invenType, x, y, z, dx, dy, dz);
	auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
	if (pPO)
		me->PlaceDynamicObjectAtPosition(pPO->m_netTraceId, glid, invenType, x, y, z, dx, dy, dz);
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ServerEngine::UpdateDynamicObjectTextureEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	UpdateDynamicObjectTextureEvent* evt = dynamic_cast<UpdateDynamicObjectTextureEvent*>(e);
	jsVerifyReturn(evt != 0, EVENT_PROC_RC::NOT_PROCESSED);

	START_TIMING(m_timingLogger, "DB:UpdateDynamicObjectTextureEventHandler");

	// TODO: The player ID we're grabbing here is the player ID of the owner of the dynamice placement object,
	//		 passed via the event from the client. What we need to do is pull the owning player ID directly
	//		 from the DB or from the object as it exists on the server, making it server authoritative instead
	//		 of client authoritative. When we're ready for that, this one place is the only place that needs
	//		 to be changed to make it properly server authoritative. In the meantime, the client is authoritative,
	//		 but to hack the client to override this would require the users to actually hack the C-side of things
	//		 and know how to retrieve their own player ID to insert instead of the owner's player ID. Not terribly
	//		 likely. -- Mo/Jo
	int placementId, assetId, playerId;
	std::string assetUrl; // will be empty from client, we'll fill it in and send it back

	evt->ExtractInfo(placementId, assetId, assetUrl, playerId);
	if (placementId < 0) {
		LogWarn("Received UpdateDynamicObjectTextureEvent with a local placement ID -- check Client implementation");
	}

	// update the table, making sure the player is a gm or owner
	auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO) {
		LogError("Player not found by NET ID " << e->From());
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	if (pPO->m_currentZonePriv > ZP_MODERATOR && pPO->m_handle != playerId) {
		LogError(loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
		me->SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
	zce->type = ZoneCustomizationBackgroundEvent::ZC_UPDATE_TEXTURE;
	zce->playerId = pPO->m_handle;
	zce->charName = pPO->m_charName;
	zce->objPlacementId = placementId;
	zce->channel = pPO->m_currentChannel;
	zce->zoneIndex = pPO->m_currentZoneIndex;
	zce->kanevaUserId = pPO->m_pAccountObject->m_serverId;
	zce->assetId = assetId;
	zce->textureUrl = assetUrl;
	zce->SetFrom(e->From());
	me->m_gameStateDispatcher->QueueEvent(zce);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::UpdatePictureFrameFriendEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	UpdatePictureFrameFriendEvent* evt = dynamic_cast<UpdatePictureFrameFriendEvent*>(e);
	jsVerifyReturn(evt != 0, EVENT_PROC_RC::NOT_PROCESSED);

	START_TIMING(m_timingLogger, "DB:UpdatePictureFrameFriendEventHandler");

	int placementId, friendId;
	std::string friendImageUrl; // empty from client, we fill it in when it's sent down

	evt->ExtractInfo(placementId, friendId, friendImageUrl);
	if (placementId < 0) {
		LogWarn("Received UpdatePictureFrameFriendEvent with a local placement ID -- check Client implementation");
	}

	// update the table, making sure the player is a gm or owner
	auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO) {
		LogError("Player not found by NET ID " << e->From());
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	if (pPO->m_currentZonePriv > ZP_MODERATOR) {
		LogError(loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
		me->SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
	zce->type = ZoneCustomizationBackgroundEvent::ZC_UPDATE_FRIEND;
	zce->playerId = pPO->m_handle;
	zce->charName = pPO->m_charName;
	zce->objPlacementId = placementId;
	zce->channel = pPO->m_currentChannel;
	zce->zoneIndex = pPO->m_currentZoneIndex;
	zce->kanevaUserId = pPO->m_pAccountObject->m_serverId;
	zce->assetId = friendId;
	zce->SetFrom(e->From());
	me->m_gameStateDispatcher->QueueEvent(zce);
	return EVENT_PROC_RC::OK;
}

// for setting textures on floors/ceilings, etc.  -- WorldObjects
EVENT_PROC_RC ServerEngine::SetWorldObjectTextureEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	UpdateWorldObjectTextureEvent* evt = dynamic_cast<UpdateWorldObjectTextureEvent*>(e);
	jsVerifyReturn(evt != 0, EVENT_PROC_RC::NOT_PROCESSED);

	START_TIMING(m_timingLogger, "DB:SetWorldObjectTextureEventHandler");

	std::string objSerialNumber;
	int assetId;
	std::string textureUrl; // from client empty, we set it here

	evt->ExtractInfo(objSerialNumber, assetId, textureUrl);

	// update the table, making sure the player is a gm or owner
	auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO) {
		LogError("Player not found by NET ID " << e->From());
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	if (pPO->m_currentZonePriv > ZP_MODERATOR) {
		LogError(loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
		me->SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
	zce->type = ZoneCustomizationBackgroundEvent::ZC_UPDATE_WORLD_OBJ_TEXTURE;
	zce->playerId = pPO->m_handle;
	zce->charName = pPO->m_charName;
	zce->channel = pPO->m_currentChannel;
	zce->zoneIndex = pPO->m_currentZoneIndex;
	zce->kanevaUserId = pPO->m_pAccountObject->m_serverId;
	zce->assetId = assetId;
	zce->textureUrl = textureUrl;
	zce->worldObjSerialNum = objSerialNumber;
	zce->SetFrom(e->From());
	me->m_gameStateDispatcher->QueueEvent(zce);
	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::RemoveDynamicObjectEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	RemoveDynamicObjectEvent* evt = dynamic_cast<RemoveDynamicObjectEvent*>(e);
	jsVerifyReturn(evt != 0, EVENT_PROC_RC::NOT_PROCESSED);

	START_TIMING(m_timingLogger, "DB: RemoveDynamicObjectEventHandler");

	int placementId;

	evt->ExtractInfo(placementId);
	if (placementId < 0) {
		LogWarn("Received RemoveDynamicObjectEvent with a local placement ID -- check Client implementation");
	}

	// update the table, making sure the player is a gm or owner
	auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO) {
		LogError("Player not found by NET ID " << e->From());
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	if (pPO->m_currentZonePriv > ZP_MODERATOR) { // owner, moderator, gm only can remove
		LogError(loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
		me->SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
		return EVENT_PROC_RC::CONSUMED; // consume invalid request to prevent any other handler from processing it
	}

	ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
	zce->type = ZoneCustomizationBackgroundEvent::ZC_REMOVE;
	zce->playerId = pPO->m_handle;
	zce->charName = pPO->m_charName;
	zce->objPlacementId = placementId;
	zce->channel = pPO->m_currentChannel;
	zce->zoneIndex = pPO->m_currentZoneIndex;
	zce->kanevaUserId = pPO->m_pAccountObject->m_serverId;
	zce->SetFrom(e->From());
	me->m_gameStateDispatcher->QueueEvent(zce);
	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::ChangeZoneMapHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* server = (ServerEngine*)lparam;
	jsVerifyReturn(server != 0, EVENT_PROC_RC::NOT_PROCESSED);

	LONG networkAssignId, inventoryItemId, apartmentId, changeType, reAdd;
	short invType;
	bool reAddToInv = true;
	(*e->InBuffer()) >> networkAssignId >> inventoryItemId >> apartmentId >> changeType >> invType >> reAdd;

	if (reAdd == 0)
		reAddToInv = false;

	// local scripting send in only netassigned id
	auto pPO = server->GetPlayerObjectByNetTraceId(networkAssignId);
	if (!pPO)
		return EVENT_PROC_RC::NOT_PROCESSED;

	if (server->m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() == 0) {
		server->ChangeZoneMap(pPO, inventoryItemId, apartmentId, changeType, invType, reAddToInv);
	} else {
		server->ChangeZoneMapFromWeb(pPO, inventoryItemId, apartmentId, changeType, invType, reAddToInv);
	}

	return EVENT_PROC_RC::OK;
}

// returns true if not spawned and tried to send them to default loc
bool ServerEngine::SendGotoError(int rc, Dispatcher& dispatcher, NETID from, int defaultMsgId, int charIndex, const char* url, bool fromDefaultUrl) {
	// if they haven't spawned yet, spend them to default url
	// if coming from default url, never try to do it again to avoid infinite loop
	if (!fromDefaultUrl) {
		auto pPO = GetPlayerObjectByNetId(from, true);
		if (!pPO || pPO->m_currentZoneIndex == INVALID_ZONE_INDEX) { // they haven't spawned yet, try to send them to default loc
			GotoPlaceEvent* gpe = new GotoPlaceEvent();
			char gameName[500];
			m_multiplayerObj->m_serverNetwork->pinger().GetGameName(gameName, _countof(gameName));
			gpe->SetFrom(from);
			gpe->GotoUrl(charIndex, StpUrl::MakeUrlPrefix(gameName).c_str());
			m_dispatcher.QueueEvent(gpe);

			// send them info msg in future
			RenderLocaleTextEvent* e = new RenderLocaleTextEvent(LS_SEND_TO_DEF_LOC, eChatType::System);
			e->AddTo(from);
			dispatcher.QueueEventInFutureMs(e, 10 * MS_PER_SEC); // queue in 10 sec to allow them to get chat menu up

			return true;
		}
	}

	LogWarn("rc = " << rc << ", URL = " << (url ? url : "(NULL)"));

	int id = defaultMsgId;
	switch (rc) {
		case ZONE_FULL: // can't go there because zone is full
			id = LS_SPAWN_ZONE_FULL;
			break;

		case SERVER_FULL: // can't go there because server is full
			id = LS_SPAWN_SERVER_FULL;
			break;

		case PLAYER_NOT_LOGGED_ON: // the dest player isn't logged on anywhere
			id = LS_DEST_PLAYER_OFFLINE;
			break;

			// cast ZONE_NOT_FOUND: = PLAYER_NOT_FOUND
		case PLAYER_NOT_FOUND: // the dest player or zone wasn't found
			id = LS_DEST_NOT_FOUND;
			break;

		case PLAYER_NOT_ALLOWED: // you can't go there
			id = LS_SPAWN_NO_PERMISSION;
			break;

		case PLAYER_BLOCKED: // can't go there because blocked
			id = LS_SPAWN_SERVER_BLOCKED;
			break;

		case MISSING_PASS:
			id = LS_MISSING_PASS;
			break;

		case BAD_URL: // URL passed in doesn't parse correctly
		case WRONG_GAME: // URL refers to a different game, shouldn't have been given to this server
			id = LS_INVALID_URL;
			break;

		default:
			LogWarn("Got unknown rc in SendGotoError of " << rc);
			break;
	}

	// if have url, send bad url event with reason code along with text message
	if (url != 0) {
		EVENT_ID eId = m_dispatcher.GetEventId("BadGotoPlaceURLEvent");
		if (eId != BAD_EVENT_ID) {
			IEvent* e = m_dispatcher.MakeEvent(eId);
			if (e) {
				e->AddTo(from);
				e->SetFilter(rc);
				(*e->OutBuffer()) << NULL_CK(url);
				m_dispatcher.QueueEvent(e);
			}
		}
	}

	RenderLocaleTextEvent::SendLocaleTextTo(dispatcher, from, eChatType::System, id);

	return false; // didn't send them to default location
}

EVENT_PROC_RC ServerEngine::BroadcastDistrHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	jsVerifyReturn(e != 0 && e->EventId() == me->m_broadcastToDistrEventId, EVENT_PROC_RC::NOT_PROCESSED);

	std::string msg, broadcastList, charName;
	int chatType;
	ULONG filter;
	(*e->InBuffer()) >> msg >> broadcastList >> charName >> chatType;
	filter = e->Filter();
	me->BroadcastMessageToDistribution(msg.c_str(),
		e->From(),
		broadcastList.c_str(),
		charName.c_str(),
		(eChatType)chatType, false, filter);

	return EVENT_PROC_RC::CONSUMED;
}

// reply from client to let us know it's ready to spawn, most likely an ack to let us know it has d/l'ed all required files
EVENT_PROC_RC ServerEngine::PendingSpawnHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	PendingSpawnEvent* pse = dynamic_cast<PendingSpawnEvent*>(e);
	jsVerifyReturn(pse != 0, EVENT_PROC_RC::NOT_PROCESSED);

	LONG ignoredZoneIndex; // this may be different from on in spawn info if faking out zonedownloader.lua
	USHORT spawnId;
	bool initialSpawn;
	long coverCharge;
	long zoneInstanceId;

	pse->extractInfo(ignoredZoneIndex, spawnId, initialSpawn, coverCharge, zoneInstanceId);

	// check the acct to see if they have the matching spawn info
	auto pAO = me->m_multiplayerObj->m_accountDatabase->GetAccountObjectByNetId(e->From());
	auto pPO = me->GetPlayerObjectByNetId(e->From(), false);

	if (spawnId == 0) {
		//cancelled spawn request
		if (pAO != 0) {
			pAO->GetSpawnInfo().reset();

			if (!pPO) {
				GotoPlaceEvent* gpe = new GotoPlaceEvent();
				char gameName[500];
				me->m_multiplayerObj->m_serverNetwork->pinger().GetGameName(gameName, _countof(gameName));
				gpe->SetFrom(e->From());
				gpe->GotoUrl(pAO->m_currentcharInUse < 0 ? 0 : pAO->m_currentcharInUse, StpUrl::MakeUrlPrefix(gameName).c_str());
				me->m_dispatcher.QueueEvent(gpe);
			}
		}

		return EVENT_PROC_RC::CONSUMED;
	}

	if (!pAO)
		return EVENT_PROC_RC::CONSUMED;

	if (pAO->GetSpawnInfo().getSpawnId() != spawnId) {
		//		LogError("Mismatched SpawnInfo - '" << pAO->m_accountName);
		return EVENT_PROC_RC::CONSUMED;
	}

	bool ok = false;
	if (me->m_gameStateDb && pAO->GetSpawnInfo().getCoverCharge() > 0) {
		try {
			IGameState::EUpdateRet ret = me->m_gameStateDb->PayCoverCharge(pAO, pPO);
			if (IGameState::UPDATE_OK != ret) {
				// they don't have the cash or an error
				RenderLocaleTextEvent::SendLocaleTextTo(me->m_dispatcher, e->From(), eChatType::System, LS_NO_MONEY);
				if (initialSpawn) {
					// fire event to send them to their home, which may switch servers
					GotoPlaceEvent* gpe = new GotoPlaceEvent();
					gpe->GotoApartment(pAO->m_serverId, pAO->m_currentcharInUse < 0 ? 0 : pAO->m_currentcharInUse);
					gpe->SetFrom(pAO->m_netId);
					me->m_dispatcher.QueueEvent(gpe);
				}
			} else {
				ok = true;
			}
		} catch (KEPException* e) {
			LogWarn(e->m_msg);
			e->Delete();
		}
	} else {
		ok = true;
	}

	if (ok)
		me->m_multiplayerObj->SendMsgSpawnToClient(pAO, pPO, e->From());

	return EVENT_PROC_RC::CONSUMED;
}

/// handler for LogoffUserBackgroundEvent to send any reply to client
EVENT_PROC_RC ServerEngine::LogoffUserBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	LogoffUserBackgroundEvent* lue = dynamic_cast<LogoffUserBackgroundEvent*>(e);
	jsVerifyReturn(lparam != 0 && lue != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;

	if (lue->ret == 0) {
		if (lue->replyEvent)
			me->m_dispatcher.QueueEvent(lue->replyEvent);
	}

	return EVENT_PROC_RC::CONSUMED;
}

/// handler for Goto event from the background processing thread
EVENT_PROC_RC ServerEngine::GotoPlaceBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	GotoPlaceBackgroundEvent* gpbe = dynamic_cast<GotoPlaceBackgroundEvent*>(e);
	jsVerifyReturn(gpbe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	std::string server;
	ULONG port = 0;
	CAccountObject* pAO = me->m_multiplayerObj->m_accountDatabase->GetAccountObjectByNetId(gpbe->From());

	if (pAO == 0) {
		LogWarn("GotoPlaceBackgroundHandler lost account during background processing for NETID " << gpbe->From());
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	switch (gpbe->type) {
		case GotoPlaceEvent::GOTO_PLAYER: {
			if (gpbe->ret != PLAYER_FOUND) {
				me->SendGotoError(gpbe->ret, me->m_dispatcher, gpbe->From(), LS_DEST_PLAYER_OFFLINE, gpbe->charIndex);
				if (pAO->m_currentcharInUse < 0) {
					// initial goto, send them home
					GotoPlaceEvent* goHomeEvent = new GotoPlaceEvent();
					goHomeEvent->GotoApartment(pAO->m_serverId, 0); // 0 is selected player WOK Specific
					goHomeEvent->SetFrom(gpbe->From());
					me->m_dispatcher.QueueEvent(goHomeEvent);
				}
				return ret;
			}

			int ret = me->isSwitchingServers(pAO, gpbe, server, port);
			if (ret == SPAWN_REMOTE) {
				GotoPlaceEvent* switchEvent = new GotoPlaceEvent();
				switchEvent->GotoPlayer(gpbe->destKanevaUserId, pAO->m_currentcharInUse >= 0 ? pAO->m_currentcharInUse : gpbe->charIndex, server.c_str(), port);
				switchEvent->AddTo(gpbe->From());
				me->spawnToOtherServer(pAO, switchEvent);
				return EVENT_PROC_RC::CONSUMED;
			} else if (ret == SPAWN_ERROR)
				return EVENT_PROC_RC::NOT_PROCESSED;
			else if (ret == SPAWN_LOCAL) {
				// check on this server and override location in case db is out-of-date
				CPlayerObject* pPO = me->GetPlayerObjectByServerId(gpbe->destKanevaUserId);

				if (pPO != 0) {
					// This check was added to stop player's from being able to zone
					// into arenas.  Only GMs are allowed to zone into arenas.
					// The database's zoneIndex, x, y, and z values will be used for the
					// destination instead of the server's because the db is not updated
					// when players go into arenas.
					if (pAO->m_gm || !pPO->m_currentZoneIndex.isArena()) {
						gpbe->zoneIndex = pPO->m_currentZoneIndex;
						gpbe->zoneIndex.setInstanceId(pPO->m_currentZoneIndex.GetInstanceId());

						Vector3f pos = pPO->InterpolatedPos();
						gpbe->x = pos.x;
						gpbe->y = pos.y;
						gpbe->z = pos.z;
					}
				}
			}

		} break;

		case GotoPlaceEvent::GOTO_PLAYERS_APT: {
			if (gpbe->ret != PLAYER_FOUND) {
				if (gpbe->ret == PLAYER_NOT_FOUND)
					RenderLocaleTextEvent::SendLocaleTextTo(me->m_dispatcher, gpbe->From(), eChatType::System, LS_PLAYER_BADAPT);
				else
					me->SendGotoError(gpbe->ret, me->m_dispatcher, gpbe->From(), LS_PLAYER_BADAPT, gpbe->charIndex);
				return ret;
			}
			int ret = me->isSwitchingServers(pAO, gpbe, server, port);
			if (ret == SPAWN_REMOTE) {
				GotoPlaceEvent* switchEvent = new GotoPlaceEvent();
				switchEvent->GotoApartment(gpbe->destKanevaUserId, pAO->m_currentcharInUse >= 0 ? pAO->m_currentcharInUse : gpbe->charIndex, server.c_str(), port);
				switchEvent->AddTo(gpbe->From());
				me->spawnToOtherServer(pAO, switchEvent);
				return EVENT_PROC_RC::OK;
			} else if (ret == SPAWN_ERROR)
				return EVENT_PROC_RC::NOT_PROCESSED;

		} break;

		case GotoPlaceEvent::GOTO_ZONE_COORDS:
		case GotoPlaceEvent::GOTO_ZONE: {
			int ret = me->isSwitchingServers(pAO, gpbe, server, port);
			if (ret == SPAWN_REMOTE) {
				GotoPlaceEvent* switchEvent = new GotoPlaceEvent();
				if (gpbe->type == GotoPlaceEvent::GOTO_ZONE)
					switchEvent->GotoZone(gpbe->zoneIndex, gpbe->zoneIndex.GetInstanceId(), pAO->m_currentcharInUse >= 0 ? pAO->m_currentcharInUse : gpbe->charIndex, server.c_str(), port);
				else
					switchEvent->GotoZone(gpbe->zoneIndex, gpbe->zoneIndex.GetInstanceId(), pAO->m_currentcharInUse >= 0 ? pAO->m_currentcharInUse : gpbe->charIndex,
						gpbe->x, gpbe->y, gpbe->z, gpbe->rotation,
						server.c_str(), port);
				switchEvent->AddTo(gpbe->From());
				me->spawnToOtherServer(pAO, switchEvent);
				return EVENT_PROC_RC::OK;
			} else if (ret == SPAWN_ERROR)
				return EVENT_PROC_RC::NOT_PROCESSED;
		} break;

		case GotoPlaceEvent::GOTO_URL: {
			int ret = me->isSwitchingServers(pAO, gpbe, server, port);
			switch (ret) {
				case SPAWN_REMOTE: {
					GotoPlaceEvent* switchEvent = new GotoPlaceEvent();
					switchEvent->GotoUrl(pAO->m_currentcharInUse >= 0 ? pAO->m_currentcharInUse : gpbe->charIndex, gpbe->url.c_str(), port, server.c_str());
					switchEvent->AddTo(gpbe->From());
					me->spawnToOtherServer(pAO, switchEvent);
					return EVENT_PROC_RC::OK;
					break;
				}
				case SPAWN_LOCAL: {
					// check on this server and override location in case db is out-of-date
					CPlayerObject* pPO = me->GetPlayerObjectByServerId(gpbe->destKanevaUserId);

					// If .people URL, destKanevaUserId will be set by the store proc and we will need to handle it the same way as GOTO_PLAYER.
					if (pPO != 0) {
						// This check was added to stop player's from being able to zone
						// into arenas.  Only GMs are allowed to zone into arenas.
						// The database's zoneIndex, x, y, and z values will be used for the
						// destination instead of the server's because the db is not updated
						// when players go into arenas.
						if (pAO->m_gm || !pPO->m_currentZoneIndex.isArena()) {
							gpbe->zoneIndex = pPO->m_currentZoneIndex;
							gpbe->zoneIndex.setInstanceId(pPO->m_currentZoneIndex.GetInstanceId());

							Vector3f pos = pPO->InterpolatedPos();
							gpbe->x = pos.x;
							gpbe->y = pos.y;
							gpbe->z = pos.z;
						}
					}
					break;
				}
				case SPAWN_ERROR: {
					return EVENT_PROC_RC::NOT_PROCESSED;
					break;
				}
				default:
					break;
			}
		} break;

		default:
			jsAssert(false);
			break;
	}

	if (gpbe->ret == ZONE_FOUND && gpbe->zonePermissions == ZP_UNKNOWN) {
		// Bug detection: ZONE_FOUND but gpbe->zonePermissions not set.
		LogWarn("background handler didn't fill in zone permissions"
				<< ", player " << gpbe->playerId << " (NETID: " << gpbe->From() << ")"
				<< ", zone " << gpbe->zoneIndex.toLong());
	}

	bool canSpawn = gpbe->ret == ZONE_FOUND;

	SpawnInfo& spawnInfo = pAO->GetSpawnInfo();
	if (canSpawn) {
		if (pAO->m_gm) {
			spawnInfo.setCoverCharge(0, 0);
		} else {
			spawnInfo.setCoverCharge(gpbe->coverCharge, gpbe->zoneOwnerId);
		}

		assert(gpbe->zonePermissions != ZP_UNKNOWN);
		spawnInfo.setZonePermissions(gpbe->zonePermissions); // Scenario 1: spawned through GotoPlaceEvent
		spawnInfo.setParentZoneInstanceId(gpbe->parentZoneInstanceId);
	}

	me->spawnPlayer(canSpawn, gpbe->charIndex, gpbe->reconnecting, pAO, gpbe->From(), gpbe->type,
		gpbe->zoneIndex, gpbe->zoneIndex.GetInstanceId(), gpbe->scriptServerId,
		gpbe->x, gpbe->y, gpbe->z, gpbe->rotation, gpbe->url);

	return ret;
}

/// handle the response from the background
EVENT_PROC_RC ServerEngine::SpaceImportBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	SpaceImportBackgroundEvent* sibe = dynamic_cast<SpaceImportBackgroundEvent*>(e);
	jsVerifyReturn(sibe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;

	if (sibe->replyEvent)
		me->m_dispatcher.QueueEvent(sibe->replyEvent);

	return EVENT_PROC_RC::OK;
}

/// turn over the work to the background
EVENT_PROC_RC ServerEngine::SpaceImportHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	SpaceImportEvent* sie = dynamic_cast<SpaceImportEvent*>(e);
	jsVerifyReturn(sie != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;
	jsVerifyReturn(me != 0 && me->m_gameStateDispatcher, EVENT_PROC_RC::NOT_PROCESSED);

	sie->ExtractInfo();

	SpaceImportBackgroundEvent* sibe = new SpaceImportBackgroundEvent();

	// _SCR_ Do not assume player is in zone we want to import into -- instead use zone supplied in args
	sibe->currentZoneIndex = sie->m_newZoneIndex;

	sibe->newZoneIndex = sie->m_newZoneIndex;
	sibe->importZoneIndex = sie->m_importZoneIndex;
	sibe->importZoneInstanceId = sie->m_importZoneInstanceId;
	sibe->maintainScripts = sie->m_maintainScripts ? true : false;

	auto pPO = me->GetPlayerObjectByNetId(sie->From(), true);
	if (pPO) {
		sibe->playerId = pPO->m_handle;
	} else {
		LogError("SpaceImportHandler could not find player for NETID " << sie->From());
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	sibe->SetFrom(sie->From());
	me->m_gameStateDispatcher->QueueEvent(sibe);
	return EVENT_PROC_RC::OK;
}

/// handle the response from the background
EVENT_PROC_RC ServerEngine::ChannelImportBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ChannelImportBackgroundEvent* cibe = dynamic_cast<ChannelImportBackgroundEvent*>(e);
	jsVerifyReturn(cibe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;

	if (cibe->replyEvent)
		me->m_dispatcher.QueueEvent(cibe->replyEvent);

	return EVENT_PROC_RC::OK;
}

/// turn over the work to the background
EVENT_PROC_RC ServerEngine::ChannelImportHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ChannelImportEvent* cie = dynamic_cast<ChannelImportEvent*>(e);
	jsVerifyReturn(cie != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;
	jsVerifyReturn(me != 0 && me->m_gameStateDispatcher, EVENT_PROC_RC::NOT_PROCESSED);

	cie->ExtractInfo();
	LogInfo("begin ChannelImportHandler cie->dstZoneIndex[" << cie->m_dstZoneIndex << "] cie->dstInstanceId[" << cie->m_dstInstanceId << "] cie->baseZoneIndexPlain[" << cie->m_baseZoneIndexPlain << "] cie->srcZoneIndex[" << cie->m_srcZoneIndex << "] cie->srcInstanceId[" << cie->m_srcInstanceId << "] cie->maintainScripts[" << cie->m_maintainScripts << "]");

	auto pPO = me->GetPlayerObjectByNetId(cie->From(), true);
	if (!pPO) {
		LogError("ChannelImportHandler could not find player for NETID " << cie->From());
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// at this point player is valid and can be passed in
	ChannelImportBackgroundEvent* cibe = new ChannelImportBackgroundEvent();
	cibe->playerNetId = cie->From();
	cibe->player_id = pPO->m_handle;
	cibe->dstZoneIndex = cie->m_dstZoneIndex;
	cibe->dstInstanceId = cie->m_dstInstanceId;
	cibe->baseZoneIndexPlain = cie->m_baseZoneIndexPlain;
	cibe->srcZoneIndex = cie->m_srcZoneIndex;
	cibe->srcInstanceId = cie->m_srcInstanceId;
	cibe->maintainScripts = cie->m_maintainScripts;
	cibe->player = pPO;
	cibe->SetFrom(cie->From());

	me->m_gameStateDispatcher->QueueEvent(cibe);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::GotoPlaceHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	GotoPlaceEvent* gpe = dynamic_cast<GotoPlaceEvent*>(e);
	jsVerifyReturn(gpe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;
	jsVerifyReturn(me != 0 && me->m_gameStateDispatcher, EVENT_PROC_RC::NOT_PROCESSED);

	gpe->ExtractInfo();

	CAccountObject* pAO = me->GetAccountObjectByNetId(gpe->From());
	if (pAO == NULL) {
		LogWarn("GotoPlaceHandler lost account during background processing for NETID " << gpe->From());
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// make sure they have valid character (7/08)
	if (gpe->m_charIndex >= 0) {
		bool badPlayer = false;
		POSITION pos = pAO->m_pPlayerObjectList->FindIndex(gpe->m_charIndex);
		if (!pos) {
			LogWarn("Player using bad char index " << pAO->m_accountName << " " << gpe->m_charIndex);
			badPlayer = true;
		} else {
			CPlayerObject* pPO = dynamic_cast<CPlayerObject*>(pAO->m_pPlayerObjectList->GetAt(pos));
			if (pPO) {
				CMovementObj* pMO = me->m_movObjRefModelList->getObjByIndex(pPO->m_originalEDBCfg);
				if (pMO == 0 || pMO->getModelType() == CEntityPropertiesCtrl::MODEL_TYPE_DISABLED) {
					LogWarn("Player using bad edb index " << pAO->m_accountName << " " << pPO->m_originalEDBCfg);
					badPlayer = true;
				}
			}
		}

		if (badPlayer) {
			IEvent* pBadCharCfgEvent = me->m_dispatcher.MakeEvent(me->m_dispatcher.GetEventId("BadCharacterConfigEvent"));
			jsVerifyReturn(pBadCharCfgEvent != 0, EVENT_PROC_RC::NOT_PROCESSED);
			pBadCharCfgEvent->AddTo(gpe->From());
			me->m_dispatcher.QueueEvent(pBadCharCfgEvent);
			return EVENT_PROC_RC::NOT_PROCESSED;
		}
	}

	// grab player info
	int playerId = 0;
	bool completedAvatarSelect = false;
	bool currentlyInAvatarSelect = false;

	auto pPO_curOrDefault = pAO->GetCurrentPlayerObjectOrDefault();
	auto pPO_cur = pAO->GetCurrentPlayerObject();
	if (pPO_curOrDefault) {
		// if the account has a valid player (even if not currently active)
		playerId = pPO_curOrDefault->m_handle;
		completedAvatarSelect = me->CompletedAvatarSelect(pPO_curOrDefault);

		if (pPO_cur) {
			// if account has an active player, we can check their location
			currentlyInAvatarSelect = me->IsPlayerInAvatarSelectZone(pPO_cur);
		}
	} else {
		// if get here client sending events before logon sequence completed
		// only saw this in debug client when danced on GO button in friends menu
		LogWarn("Go goto place event for account with no players" << pAO->m_accountName);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// if the user has not selected an avatar yet, they need to be in the character creation zone
	if (!completedAvatarSelect) {
		if (currentlyInAvatarSelect) {
			LogTrace("GotoPlaceHandler player" << pAO->m_accountName << " already in character creation zone");
			return EVENT_PROC_RC::NOT_PROCESSED;
		}
		gpe->m_type = GotoPlaceEvent::GOTO_TUTORIAL_ZONE;
	}

	if (gpe->m_type == GotoPlaceEvent::GOTO_TUTORIAL_ZONE) {
		bool ok = false;
		// should we goto a specific place, or their apt?
		if (me->m_multiplayerObj->m_tutorialZoneIndex != INVALID_ZONE_INDEX) {
			gpe->m_type = GotoPlaceEvent::GOTO_ZONE_COORDS;
			gpe->m_zi = me->m_multiplayerObj->m_tutorialZoneIndex;
			gpe->m_instanceId = me->m_multiplayerObj->m_tutorialZoneInstanceId;

			// get the spawn point
#ifndef REFACTOR_INSTANCEID
			CSpawnCfgObj* cfg = me->m_multiplayerObj->GetSpawnPointCfg(me->m_multiplayerObj->m_tutorialZoneIndex, me->m_multiplayerObj->m_tutorialZoneSpawnIndex);
#else
			CSpawnCfgObj* cfg = me->m_multiplayerObj->GetSpawnPointCfg(me->m_multiplayerObj->m_tutorialZoneIndex.toLong(), me->m_multiplayerObj->m_tutorialZoneSpawnIndex);
#endif
			if (cfg) {
				gpe->m_x = cfg->m_position.x;
				gpe->m_y = cfg->m_position.y;
				gpe->m_z = cfg->m_position.z;
				gpe->m_rotation = cfg->m_rotation;
				ok = true;
			} else {
				LogWarn(loadStr(IDS_SPAWNPT_NOT_FOUND) << pAO->m_accountName);
			}
		}
		if (!ok) {
			// goto their apt
			gpe->m_type = GotoPlaceEvent::GOTO_PLAYERS_APT;
			gpe->m_destKanevaUserId = pAO->m_serverId;
		}
	}

	//Spawning in the same zone so dont bother with all the checks again.
	if (pPO_cur && gpe->m_zi == pPO_cur->m_currentZoneIndex) {
		me->SpawnToPoint(pPO_cur, gpe->m_x, gpe->m_y, gpe->m_z, pPO_cur->m_currentZoneIndex.toLong(),
			gpe->m_type == GotoPlaceEvent::GOTO_PLAYER ? DT_ValidatedPlayer : DT_ValidatedZone,
			gpe->m_rotation,
			gpe->m_url.c_str());

		return EVENT_PROC_RC::OK;
	}

	// using background event now for all calls.  Set it up with common stuff first
	GotoPlaceBackgroundEvent* gpbe = new GotoPlaceBackgroundEvent();
	gpbe->spawnPtIndex = DEFAULT_SPAWN_POINT;
	gpbe->type = gpe->m_type;
	gpbe->kanevaUserId = pAO->m_serverId;
	gpbe->charIndex = gpe->m_charIndex;
	gpbe->reconnecting = gpe->IsReconnecting();
	gpbe->SetFrom(gpe->From());
	gpbe->playerId = playerId;
	gpbe->serverId = me->m_multiplayerObj->m_serverNetwork->pinger().GetServerId();
	gpbe->isGm = (pAO->m_gm != FALSE) || gpe->m_asAdmin;
	gpbe->country = pAO->m_country;
	gpbe->birthDate = pAO->m_birthDate;
	gpbe->showMature = pAO->m_showMature;
	switch (gpe->m_type) {
		case GotoPlaceEvent::GOTO_PLAYER:
		case GotoPlaceEvent::GOTO_PLAYERS_APT: {
			gpbe->destKanevaUserId = gpe->m_destKanevaUserId;
			me->m_gameStateDispatcher->QueueEvent(gpbe);

			return EVENT_PROC_RC::OK;
		} break;

		case GotoPlaceEvent::GOTO_ZONE_COORDS: {
			// Coords have been specified, don't use the default spawn point.
			gpbe->spawnPtIndex = SPAWN_POINT_NOT_USED;
			gpbe->rotation = gpe->m_rotation;
			gpbe->x = gpe->m_x;
			gpbe->y = gpe->m_y;
			gpbe->z = gpe->m_z;
		}
		case GotoPlaceEvent::GOTO_ZONE: {
			// simple case when they know where they want to go
			LogTrace("GotoPlaceHandler player " << pAO->m_accountName << " going to zone " << numToString(gpe->m_zi.toLong()) << " " << gpe->m_instanceId);
			gpbe->zoneIndex = gpe->m_zi;
			gpbe->zoneIndex.setInstanceId(gpe->m_instanceId);
			gpbe->destKanevaUserId = gpe->m_destKanevaUserId;
			me->m_gameStateDispatcher->QueueEvent(gpbe);

			return EVENT_PROC_RC::OK;
		} break;

		case GotoPlaceEvent::GOTO_URL: {
			// client entered or selected a URL (4/08)
			std::string place;

			StpUrl::ExtractPlaceName(gpe->m_url, place);
			if (place.empty()) {
				gpbe->defaultUrl = true;
				if (!me->m_gameStateDb->GetDefaultPlaceUrl().empty()) {
					gpbe->url = PathAdd(gpe->m_url.c_str(), me->m_gameStateDb->GetDefaultPlaceUrl().c_str());
					LogTrace("GotoPlaceHandler player " << pAO->m_accountName << " going to default url " << gpbe->url);
				} else if (me->m_multiplayerObj->m_alwaysStartInHousingZone) {
					// send them home
					gpbe->url = PathAdd(gpe->m_url.c_str(), StpUrl::MakeAptUrlPath(pAO->m_accountName).c_str());
					LogTrace("GotoPlaceHandler player " << pAO->m_accountName << " going to default apt url " << gpbe->url);
				} else {
					LogError("GotoPlaceHandler: Bad config: Unable to determine url."
							 << "Enable AlwaysStartInHousing or specify DefaultPlaceUrl in networkdefinitions.xml");
				}
			} else {
				LogTrace("GotoPlaceHandler player " << pAO->m_accountName << " going to url " << gpe->m_url);
				gpbe->url = gpe->m_url.c_str();
			}

			me->m_gameStateDispatcher->QueueEvent(gpbe);
			return EVENT_PROC_RC::OK;
		} break;

		default: {
			gpbe->Delete();
			LogWarn("Bad goto type: " << gpe->m_type << " for account " << pAO->m_accountName);
		} break;
	}

	return EVENT_PROC_RC::NOT_PROCESSED;
}

/// used by old and new way of triggering spawns
void ServerEngine::spawnPlayer(bool canSpawn, int charIndex, bool reconnecting,
	CAccountObject* pAO, NETID from, int gotoType, ZoneIndex& zoneIndex_,
	LONG instanceId, ULONG scriptServerId,
	double x, double y, double z, int rotation,
	const std::string& url) {
	if (canSpawn) {
		if (pAO->GetSpawnInfo().isSet()) {
			LogWarn("spawnPlayer ignoring new spawn request for " << pAO->m_accountName << " because of pending spawn. NETID " << from);
			return;
		}

		//
		// This needs to be updated, but is this the right place?
		//
		pAO->m_scriptServerIdPrev = pAO->m_scriptServerId;
		pAO->m_scriptServerId = scriptServerId;

		// get the player object
		auto pPO = GetPlayerObjectByNetId(pAO->m_netId, true);

		// local zone, or initial re-spawn
		ZoneIndex zoneIndex(zoneIndex_.zoneIndex(), instanceId, zoneIndex_.GetType());

		if (pPO) {
			if (pPO->m_pAccountObject == pAO) {
				LogDebug("GotoPlaceHandler spawning player " << pPO->m_charName << " to " << numToString(zoneIndex.toLong()) << " " << x << "," << y << "," << z);
				SpawnToPoint(pPO, x, y, z, zoneIndex.toLong(),
					gotoType == GotoPlaceEvent::GOTO_PLAYER ? DT_ValidatedPlayer : DT_ValidatedZone,
					rotation,
					url.c_str());
			} else {
				LogError("Error in GotoPlaceHandler attempting to spawn player " << pPO->m_charName << " to " << numToString(zoneIndex.toLong()) << " with NETID of " << pPO->m_netId << " has different account " << pAO->m_accountName << " NETID " << pAO->m_netId);
			}
		} else { // initial spawn, and select player
			LogDebug("GotoPlaceHandler selecting and spawning player " << pAO->m_accountName << " charIndex=" << charIndex); // same line to avoid changing line numbers
			SelectPlayerAndSpawn(pAO, pAO->m_netId, -1, charIndex, &zoneIndex, (float)x, (float)y, (float)z,
				reconnecting, gotoType == GotoPlaceEvent::GOTO_PLAYER, rotation,
				url.c_str());
		}

	} else {
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pAO->m_netId, eChatType::System, LS_SPAWN_NO_PERMISSION);
		LogWarn(loadStrPrintf(IDS_SPAWN_PERMISSION, pAO->m_accountName, zoneIndex_.toLong()));
	}
}

bool ServerEngine::ChangeZoneMap(CPlayerObject* pPO, LONG inventoryItemId, LONG newZoneIndexLong, LONG changeType, short invType, bool reAddOnFail) {
	if (!pPO)
		return false;

	// only owner change can change apt/channel zone
	if ((pPO->m_currentZonePriv != ZP_OWNER) || (!pPO->m_currentZoneIndex.isChannel() && !pPO->m_currentZoneIndex.isHousing())) {
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_CANT_UPGRADE_ZONE);
		return false;
	}

	bool markDirty = false;
	if (!reAddOnFail)
		markDirty = true;

	// since this is called only from a local-only event
	// we're going to trust that the inventory item is correct
	// for this zone type
	if (!pPO->m_gm && !RemoveInventoryItemFromPlayer(pPO, 1, TRUE, inventoryItemId, invType, markDirty)) {
		LogError(pPO->ToStr() << " invItemId=" << inventoryItemId << " invType=" << invType);
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_CANT_UPGRADE_ZONE);
		return false;
	}

	ZoneIndex oldZone = pPO->m_currentZoneIndex;
	ZoneIndex newZoneIndex = (ZoneIndex)newZoneIndexLong;

	if (oldZone.isHousing()) {
		newZoneIndex.setAsHousing(pPO->m_currentZoneIndex.GetInstanceId());

		if (pPO->m_currentZonePriv == ZP_OWNER) { // don't do for gm
			ZoneIndex oldApt = pPO->m_housingZoneIndex;
			pPO->m_housingZoneIndex = newZoneIndex;
			pPO->m_dbDirtyFlags |= PlayerMask::RESPAWN_POINT;
		}

	} else if (oldZone.isChannel()) {
		newZoneIndex.setAsChannel(pPO->m_currentZoneIndex.GetInstanceId());
	} else {
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_CANT_UPGRADE_ZONE_137);
		return false;
	}

	jsVerifyReturn(oldZone.GetInstanceId() == newZoneIndex.GetInstanceId(), false); // must be same.

#ifndef REFACTOR_INSTANCEID
	CSpawnCfgObj* sc = m_multiplayerObj->GetSpawnPointCfg(newZoneIndex, 0);
#else
	CSpawnCfgObj* sc = m_multiplayerObj->GetSpawnPointCfg(newZoneIndex.toLong(), 0);
#endif
	if (sc == 0) {
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_NO_HOME);
		return false;
	}

	// if they are set to respawn here, reset it
	CPlayerObject* pPO_orig = pPO->playerOrigRef();
	if (pPO_orig->m_respawnZoneIndex.isHousing() && oldZone.isHousing()) {
		pPO_orig->m_respawnZoneIndex = newZoneIndex;
		pPO_orig->m_respawnPosition = sc->m_position;
		pPO_orig->m_dbDirtyFlags |= PlayerMask::RESPAWN_POINT;
	}

	// tell database to move all the items from apt to inventory
	ChangeZoneBackgroundEvent* e = new ChangeZoneBackgroundEvent();
	e->SetFrom(pPO->m_netId);
	e->spawnPointCfg = sc;
	e->changeType = changeType;
	e->globalId = inventoryItemId;
	e->invType = invType;
	e->newZoneIndex = newZoneIndex;
	e->oldZoneIndex = oldZone;
	e->charName = pPO->m_charName;
	e->playerId = pPO->m_handle;
	e->reAddInvOnFail = reAddOnFail;
	m_gameStateDispatcher->QueueEvent(e);

	return true;
}

bool ServerEngine::ChangeZoneMapFromWeb(CPlayerObject* player, LONG inventoryItemId, LONG _newZoneIndex, LONG changeType, short invType, bool reAddOnFail) {
	LogError("Deeds are not currently supported in Worlds.  Global id: " << inventoryItemId);

	return false;
}

// for group/clan chat
EVENT_PROC_RC ServerEngine::TextHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* m_serverEngine = (ServerEngine*)lparam;
	auto pEvent = dynamic_cast<TextEvent*>(e);
	if (!m_serverEngine || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	SlashCommand commandId;
	int networkAssignedId;
	std::string text;
	eChatType chatType;

	pEvent->ExtractInfo(commandId, networkAssignedId, text, chatType);
	if (commandId != SlashCommand::NONE || (chatType != eChatType::Group && chatType != eChatType::Clan))
		return EVENT_PROC_RC::NOT_PROCESSED;

	auto pPO = m_serverEngine->GetPlayerObjectByNetTraceId(networkAssignedId);
	if (!pPO)
		return EVENT_PROC_RC::NOT_PROCESSED;

	LogInfo(pPO->ToStr() << " commandId=" << static_cast<int>(commandId) << " '" << text << "'");

	switch (chatType) {
		case eChatType::Group:
			m_serverEngine->m_multiplayerObj->SendTextMessageToGroup(pPO, text.c_str());
			break;

		case eChatType::Clan:
			m_serverEngine->m_multiplayerObj->SendTextMessageToClan(pPO, text.c_str());
			break;
	}
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ServerEngine::GetAttributeHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* m_serverEngine = (ServerEngine*)lparam;
	GetAttributeEvent* pEvent = dynamic_cast<GetAttributeEvent*>(e);
	if (!m_serverEngine || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;
	ATTRIB_DATA attribData;
	pEvent->GetValues(attribData);
	m_serverEngine->HandleMsgAttrib((BYTE*)&attribData, e->From());
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ServerEngine::DisconnectClientHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;

	DisconnectedEvent* de = dynamic_cast<DisconnectedEvent*>(e);
	if (de) {
		CAccountObject* pAO = 0;
		NETID dpnidPlayer = 0;
		LONG minutesConnected = 0;
		ULONG disconnectReason = 0;

		de->ExtractInfo(dpnidPlayer, disconnectReason, minutesConnected);

		LocationRegistry::singleton().unregister(dpnidPlayer);

		pAO = me->m_multiplayerObj->m_accountDatabase->GetAccountObjectByNetId(dpnidPlayer);

		me->P2pAnimTerminate(dpnidPlayer);

		if (pAO) {
			std::string name = pAO->m_accountName; // refPtr gone after call
			if (me->m_multiplayerObj->LogoutCleanup(pAO, disconnectReason, nullptr, 0, &minutesConnected)) {
				LogWarn("Disconnecting " << name << ", NETID " << dpnidPlayer << ". Was connected " << minutesConnected << " min.  Disconnect reason " << disconnectReason << " " << IGameState::MapDisconnectReason(disconnectReason));
				de->SetMinutes(minutesConnected); // set minutes on the event since didn't have it when queued.  Others can get it

			} else {
				LogError("Error Disconnecting NETID " << dpnidPlayer << ".  Account not found. Was connected " << minutesConnected << " min.  Disconnect reason " << disconnectReason << " " << IGameState::MapDisconnectReason(disconnectReason));
				ret = EVENT_PROC_RC::CONSUMED; // don't pass it on
			}
		} else {
			LogWarn("Disconnecting NETID " << dpnidPlayer << ".  Account not found (probably cleanly exited).  Disconnect reason " << disconnectReason << " " << IGameState::MapDisconnectReason(disconnectReason));
		}
	}

	return ret;
}

void ServerEngine::PlayerScale(ScriptClientEvent* sce) {
	//
	// Update the cplayerobj with the new scale and then
	// broadcast the sce to all the connected clients.
	// The SCE should have the client id in it, so the
	// clients can apply the scale to the correct object.
	// cplayerobject's scale should be sent down with
	// an MovementObjectCfgEvent, so that takes care of new clients
	// too.
	//
	// The same ID should be in the from field, so maybe
	// I can use that instead of adding it to the buffer.
	// Experiment.
	//
	double x, y, z;
	int clientId;
	int zoneId;
	*sce->InBuffer() >> clientId >> zoneId >> x >> y >> z;
	auto pPO = GetPlayerObjectByNetId(clientId, true);
	if (!pPO)
		return;

	// Don't save the player's scale.  So now it won't persist across zoning
	pPO->m_scratchScaleFactor.x = x;
	pPO->m_scratchScaleFactor.y = y;
	pPO->m_scratchScaleFactor.z = z;

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::PlayerScale, clientId);
	*e->OutBuffer() << pPO->m_netTraceId << x << y << z;
	e->SetFrom(sce->From());
	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		m_dispatcher.QueueEvent(e);
	} else {
		BroadcastEventFromZoneIndex(e, zoneId);
	}
}

short ServerEngine::CheckGender(CAccountObject* acct, short edb_index) {
	//WOK specific.  Enforce consistnent gender.  No she-males.
	if (acct->m_gender == 'F') {
		if (!(edb_index == GENDER_FEMALE_SMALL || edb_index == GENDER_FEMALE_MED || edb_index == GENDER_FEMALE_LARGE)) {
			LogWarn("Account gender and update gender do not match.  Account: " << acct->m_gender << " Update: " << edb_index << " Setting gender for: " << acct->m_accountName);
			return GENDER_FEMALE_MED;
		} else
			return edb_index;
	} else if (acct->m_gender == 'M') {
		if (!(edb_index == GENDER_MALE_SMALL || edb_index == GENDER_MALE_MED || edb_index == GENDER_MALE_LARGE)) {
			LogWarn("Account gender and update gender do not match.  Account: " << acct->m_gender << " Update: " << edb_index << " Setting gender for: " << acct->m_accountName);
			return GENDER_MALE_MED;
		} else
			return edb_index;
	} else {
		LogError("Account gender unknown!  Account: " << acct->m_accountName << " Setting to a default female.");
		return GENDER_FEMALE_MED;
	}
}

void ServerEngine::PlayerCameraAttach(ScriptClientEvent* sce) {
	int clientId, targetId;
	*sce->InBuffer() >> clientId >> targetId;
	auto pPO = GetPlayerObjectByNetId(clientId, true);
	auto pPO_target = GetPlayerObjectByNetId(targetId, true);
	if (!pPO || !pPO_target)
		return;

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::PlayerCameraAttach, clientId, ScriptClientEvent::Scope_Player);
	*e->OutBuffer() << pPO_target->m_netTraceId;
	e->SetFrom(0);
	e->AddTo(clientId);
	m_dispatcher.QueueEvent(e);
}

void ServerEngine::SoundPlayOnClient(ScriptClientEvent* sce) {
	int zoneId, globalId;

	*sce->InBuffer() >> zoneId >> globalId;

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::SoundPlayOnClient, 0); //goes to everyone, send to server to broadcast
	*e->OutBuffer() << globalId;

	BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::SetCustomTextureOnAccessory(ScriptClientEvent* sce) {
	unsigned long playerId, accessoryGlid;
	std::string url;

	*sce->InBuffer() >> playerId >> accessoryGlid >> url;

	auto pPO = GetPlayerObjectByNetId(playerId, false);
	if (pPO) {
		ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::PlayerSetCustomTextureOnAccessory, 0); //goes to everyone, send to server to broadcast
		*e->OutBuffer() << pPO->m_netTraceId << accessoryGlid << url;
#ifndef REFACTOR_INSTANCEID
		BroadcastEventFromZoneIndex(e, pPO->m_currentZoneIndex);
#else
		BroadcastEventFromZoneIndex(e, pPO->m_currentZoneIndex.toLong());
#endif
	} else {
		LogError("Can't find player: " << playerId << " for setting custom texture on accessory");
	}
}

void ServerEngine::SoundAttachTo(ScriptClientEvent* sce) {
	ULONG soundPlacementId = 0, targetId = 0;
	ObjectType attachType = ObjectType::NONE;
	LONG zoneId = 0;
	ScriptClientEvent::DecodeSoundAttachTo(sce, soundPlacementId, targetId, reinterpret_cast<int&>(attachType), zoneId);

	if (attachType == ObjectType::MOVEMENT) {
		NETID netId = targetId;
		auto pPO = GetPlayerObjectByNetId(netId, false);
		if (pPO) {
			targetId = pPO->m_netTraceId;
		} else {
			LogWarn("Player Not Found: netId=" << netId);
			return;
		}
	}

	// clientId of 0 is ok, because this will be processed by the game server.
	ScriptClientEvent* e = ScriptClientEvent::EncodeSoundAttachTo(NULL, soundPlacementId, targetId, static_cast<int>(attachType), zoneId);
	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		// Retransmitted by TransmitGameStateTask
		ULONG clientId = 0;
		*sce->InBuffer() >> clientId; // Read destination clientId (padded by TransmitGameStateTask)
		e->AddTo(clientId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);

	return;
}

void ServerEngine::SoundAddModifier(ScriptClientEvent* sce) {
	// Decode Event Sent From Script
	ZoneIndex zoneId;
	int soundPlacementId;
	int input;
	int output;
	double gain;
	double offset;
	ScriptClientEvent::DecodeSoundAddModifier(sce, zoneId, soundPlacementId, input, output, gain, offset);

	// Re-Encode Event To Send To Client
	ScriptClientEvent* e = ScriptClientEvent::EncodeSoundAddModifier(NULL, zoneId, soundPlacementId, input, output, gain, offset);
	e->SetFrom(sce->From());
	BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::PlayerGetLocation(ScriptClientEvent* sce) {
	ULONG placementObj;
	NETID clientNetId;
	ULONG zoneId;
	*sce->InBuffer() >> zoneId >> clientNetId >> placementObj;

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerLocation, placementObj, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);

	float posX = 0;
	float posY = 0;
	float posZ = 0;
	float dirX = 1;
	float dirY = 0;
	float dirZ = 0;
	int result = 0;
	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (pPO) {
		Vector3f pos = pPO->InterpolatedPos();
		Vector3f dir = pPO->InterpolatedDir();
		posX = pos.x;
		posY = pos.y;
		posZ = pos.z;
		dirX = dir.x;
		dirY = dir.y;
		dirZ = dir.z;
		result = 1;
	} else {
		LogTrace("ServerEngine::PlayerGetLocation player missing from runtime!");
	}

	*sse->OutBuffer() << posX << posY << posZ
					  << dirX << dirY << dirZ
					  << result;

	// Let the ScriptServerEvent handler forward it to the script server.
	// Just call the handler directly, rather than going through the dispatcher.
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::PlayerSetTitle(ScriptClientEvent* sce) {
	ULONG placementObj;
	NETID clientNetId;
	std::string title;
	*sce->InBuffer() >> clientNetId >> placementObj >> title;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (!pPO)
		return;

	pPO->m_displayTitle = title.c_str();
	TitleEvent* e = new TitleEvent(title);
	*e->OutBuffer() << pPO->m_netTraceId;
#ifndef REFACTOR_INSTANCEID
	BroadcastEventFromZoneIndex(e, pPO->m_currentZoneIndex);
#else
	BroadcastEventFromZoneIndex(e, pPO->m_currentZoneIndex.toLong());
#endif
}

void ServerEngine::PlayerSetName(ScriptClientEvent* sce) {
	ULONG placementObj;
	NETID clientNetId;
	std::string name;
	int zoneId;
	*sce->InBuffer() >> clientNetId >> placementObj >> name >> zoneId;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (!pPO)
		return;

	pPO->m_displayName = name.c_str();
	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::PlayerSetName, clientNetId);
	*e->OutBuffer() << name << pPO->m_netTraceId;
	BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::PlayerUseItem(ScriptClientEvent* sce) {
	ULONG placementId, globalId, zoneId;
	NETID clientNetId;

	*sce->InBuffer() >> zoneId >> clientNetId >> globalId >> placementId;

	int usedItem = 0;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (pPO) {
		CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(globalId);
		if (pGIO) {
			bool result = false;
			if (pGIO->m_itemData->m_useType == USE_TYPE_NONE ||
				pGIO->m_itemData->m_useType == USE_TYPE_EQUIP ||
				pGIO->m_itemData->m_useType == USE_TYPE_ADD_DYN_OBJ) { //Clothing
				if (!pGIO->m_itemData->m_useAnywhere) {
					PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
					if (!PassList::canPlayerUseItem(pPO->m_passList, pGIO->m_itemData->m_passList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS_TO_REMOVE);
						LogInfo("Can't use item due to player missing pass " << pPO->m_charName << " " << globalId);
					} else {
						if (!PassList::canUseItemInZone(pGIO->m_itemData->m_passList, zonePassList)) {
							RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
							LogInfo("Can't use item due to zone missing pass " << pPO->m_charName << " " << globalId << numToString(pPO->m_currentZoneIndex, " 0x%x"));
						} else {
							result = EquipInventoryItem(pPO, globalId, clientNetId, true); // Pass true to fromScript parameter will give the player the item if he doesn't own it.
						}
					}
				}
			} else {
				ATTRIB_DATA attribData;
				attribData.aeShort1 = pPO->m_netTraceId;
				attribData.aeShort2 = IT_NORMAL;
				attribData.aeInt1 = globalId;
				result = (AttribUseItem(&attribData, clientNetId) == TRUE);
			}

			if (result) {
				usedItem = 1;
			} else {
				LogInfo("Failed to use item " << globalId << " for player with NETID " << clientNetId);
			}
		}
	} else {
		LogError("Could not find player object with NETID " << clientNetId);
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerUseItem, placementId, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);
	*sse->OutBuffer() << usedItem;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::PlayerEquipItem(ScriptClientEvent* sce) {
	int usedItem = 0;

	ULONG placementId, globalId, zoneId;
	NETID clientNetId;
	*sce->InBuffer() >> zoneId >> clientNetId >> globalId >> placementId;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (pPO) {
		//Check the inventory from GMs to make sure it was purchased from Kaneva at least once.
#ifndef REFACTOR_INSTANCEID
		if (!isItemAvailable(globalId, pPO->m_currentZoneIndex))
#else
		if (!isItemAvailable(globalId, pPO->m_currentZoneIndex.toLong()))
#endif
		{
			LogError("isItemAvailable() FAILED - '" << pPO->m_charName << "' glid<" << globalId << ">");
		} else {
			auto pGIO = (CGlobalInventoryObj*)m_globalInventoryDB->GetByGLID(globalId);
			if (pGIO) {
				bool result = true;

				if (pGIO->m_itemData->m_useType == USE_TYPE_NONE ||
					pGIO->m_itemData->m_useType == USE_TYPE_EQUIP ||
					pGIO->m_itemData->m_useType == USE_TYPE_ADD_DYN_OBJ) { //Clothing

					// Passes Check
					if (!pGIO->m_itemData->m_useAnywhere) {
						PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);

						// Player Passes
						if (result && !PassList::canPlayerUseItem(pPO->m_passList, pGIO->m_itemData->m_passList)) {
							RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS_TO_REMOVE);
							LogError("canPlayerUseItem() FAILED - '" << pPO->m_charName << "' glid<" << globalId << ">");
							result = false;
						}

						// Zone Passes
						if (result && !PassList::canUseItemInZone(pGIO->m_itemData->m_passList, zonePassList)) {
							RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
							LogError("canUseItemInZone() FAILED - '" << pPO->m_charName << "' glid<" << globalId << "> zoneIndex=" << pPO->m_currentZoneIndex);
							result = false;
						}
					}
				} else {
					LogError("BAD USE_TYPE - useType=" << pGIO->m_itemData->m_useType
													   << " '" << pPO->m_charName << "' glid<" << globalId << ">");
					result = false;
				}

				if (result) {
					// DRF - Actually Arm Item
					result = EquipInventoryItem(pPO, globalId, clientNetId, true);
					if (!result) {
						LogError("EquipInventoryItem() FAILED - '" << pPO->m_charName << "' glid<" << globalId << ">");
					} else {
						usedItem = 1;
					}
				}
			} else {
				LogError("GetByGLID() FAILED - '" << pPO->m_charName << "' glid<" << globalId << ">");
			}
		}
	} else {
		LogError("GetObjectByNetId() FAILED - netId=" << clientNetId);
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerEquipItem, placementId, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);
	*sse->OutBuffer() << usedItem;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::PlayerUnEquipItem(ScriptClientEvent* sce) {
	int usedItem = 0;

	ULONG placementId, globalId, zoneId;
	NETID clientNetId;
	*sce->InBuffer() >> zoneId >> clientNetId >> globalId >> placementId;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (pPO) {
		auto pGIO = (CGlobalInventoryObj*)m_globalInventoryDB->GetByGLID(globalId);
		if (pGIO && pGIO->m_itemData->m_disarmable) {
			bool result = true;

			// Passes Check
			for (auto exID = pGIO->m_itemData->m_exclusionGroupIDs.begin(); exID != pGIO->m_itemData->m_exclusionGroupIDs.end(); exID++) {
				PassList* nothingPassList = getNothingItemPassList(pPO->m_curEDBIndex, *exID);
				if (nothingPassList != 0) {
					PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);

					// Player Passes
					if (!PassList::canPlayerUseItem(pPO->m_passList, nothingPassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS_TO_REMOVE);
						LogError("canPlayerUseItem() FAILED - '" << pPO->m_charName << "' glid<" << globalId << ">");
						result = false;
						break;
					}

					// Zone Passes
					if (!PassList::canUseItemInZone(nothingPassList, zonePassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
						LogError("canUseItemInZone() FAILED - '" << pPO->m_charName << "' glid<" << globalId << "> zoneIndex=" << pPO->m_currentZoneIndex);
						result = false;
						break;
					}
				}
			}

			if (result) {
				// Actually Disarm Item
				result = UnEquipInventoryItem(pPO, globalId, true);
				if (!result) {
					LogError("UnEquipInventoryItem() FAILED - '" << pPO->m_charName << "' glid<" << globalId << ">");
				} else {
					usedItem = 1;
				}
			}
		} else {
			LogError("NOT DISARMABLE - '" << pPO->m_charName << "' glid<" << globalId << ">");
		}
	} else {
		LogError("GetObjectByNetId() FAILED - netId=" << clientNetId);
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerUnEquipItem, placementId, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);
	*sse->OutBuffer() << usedItem;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::PlayerRemoveAllAccessories(ScriptClientEvent* sce) {
	ULONG placementId, zoneId;
	NETID clientNetId;
	*sce->InBuffer() >> zoneId >> clientNetId >> placementId;
	bool error = false;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (!pPO) {
		LogError("GetObjectByNetId() FAILED - netId=" << clientNetId);
		error = true;
	}

	if (!error) {
		// Disarm All Items Of USE_TYPE_EQUIP
		if (!UnEquipAllInventoryItemsOfUseType(pPO, USE_TYPE_EQUIP, true)) {
			LogError("UnEquipAllInventoryItemsOfUseType(USE_TYPE_EQUIP) FAILED - '" << pPO->m_charName << "'");
			error = true;
		}
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerRemoveAllAccessories, placementId, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);
	*sce->OutBuffer() << (error ? "fail" : "success");
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::PlayerSaveEquipped(ScriptClientEvent* sce) {
	bool error = false;
	std::string glids;
	std::string excGroups;

	ULONG placementId, zoneId;
	NETID clientNetId;
	std::string outfitName;
	*sce->InBuffer() >> zoneId >> clientNetId >> outfitName >> placementId;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (!pPO) {
		LogError("GetObjectByNetId() FAILED - netId=" << clientNetId);
		error = true;
	}

	if (!error) {
		// Save Equipped Items As Player Outfit
		auto pPlayerOutfit = PlayerOutfitSave(pPO, outfitName, true);
		if (!pPlayerOutfit) {
			LogError("PlayerOutfitSave() FAILED - '" << pPO->m_charName << ":" << outfitName << "'");
		} else {
			// Return Player Outfit Glids (comma separated values)
			StringHelpers::implode(pPlayerOutfit->glidList, glids);
			// TODO - excGroups
		}
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerSaveEquipped, placementId, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);
	*sse->OutBuffer() << glids << excGroups;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::PlayerGetEquipped(ScriptClientEvent* sce) {
	bool error = false;
	std::string glids;
	std::string excGroups;

	ULONG placementId, zoneId;
	NETID clientNetId;
	*sce->InBuffer() >> zoneId >> clientNetId >> placementId;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (!pPO) {
		LogError("GetObjectByNetId() FAILED - netId=" << clientNetId);
		error = true;
	}

	if (!error) {
		// Get Current Player Outfit
		auto pPlayerOutfit = PlayerOutfitGetCurrent(pPO, true);
		if (!pPlayerOutfit) {
			LogError("PlayerOutfitGetCurrent() FAILED - '" << pPO->m_charName << "'");
		} else {
			// Return Player Outfit Glids (comma separated values)
			StringHelpers::implode(pPlayerOutfit->glidList, glids);
			// TODO - excGroups
		}
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerGetEquipped, placementId, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);
	*sse->OutBuffer() << glids << excGroups;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::PlayerSetEquipped(ScriptClientEvent* sce) {
	bool error = false;
	std::string glidsOut;
	std::string excGroups;

	ULONG placementId, zoneId;
	NETID clientNetId;
	std::string glidsIn;
	std::string outfitName;
	*sce->InBuffer() >> zoneId >> clientNetId >> glidsIn >> outfitName >> placementId;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (!pPO) {
		LogError("GetObjectByNetId() FAILED - netId=" << clientNetId);
		error = true;
	}

	if (!error) {
		// Explode Glid List (comma separated values)
		std::vector<GLID> glidVec;
		StringHelpers::explode(glidVec, glidsIn);
		size_t glidVecSize = glidVec.size();

		// Apply PassList Exceptions (AP, etc)
		PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
		for (size_t i = 0; i < glidVecSize; i++) {
			CGlobalInventoryObj* globalInvObj = m_globalInventoryDB->GetByGLID(glidVec[i]);
			if (!globalInvObj) {
				LogError("Could not find object glid " << glidVec[i] << " in global inventory");
				error = true;
			}

			if (!error && globalInvObj->m_itemData->m_useType != USE_TYPE_NONE && globalInvObj->m_itemData->m_useType != USE_TYPE_EQUIP && globalInvObj->m_itemData->m_useType != USE_TYPE_ADD_DYN_OBJ) { //Clothing
				LogError("Item wrong use type for equipping " << glidVec[i] << " for player netID=" << clientNetId);
				error = true;
			}

			if (!error && !globalInvObj->m_itemData->m_useAnywhere) {
				if (!PassList::canPlayerUseItem(pPO->m_passList, globalInvObj->m_itemData->m_passList)) {
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS_TO_REMOVE);
					LogError("Can't equip item due to player missing pass " << pPO->m_charName << " " << glidVec[i]);
					error = true;
				} else {
					if (!PassList::canUseItemInZone(globalInvObj->m_itemData->m_passList, zonePassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
						LogError("Can't equip item due to zone missing pass " << pPO->m_charName << " " << glidVec[i] << numToString(pPO->m_currentZoneIndex, " 0x%x"));
						error = true;
					}
				}
			}
		}

		if (!error) {
			// Step 1 - Disarm All Items
			if (!UnEquipAllInventoryItems(pPO, true)) {
				LogError("UnEquipAllInventoryItems() FAILED - '" << pPO->m_charName << "'");
			}

			// Step 2 - Equip Passed In Named Outfit
			if (outfitName.size() && !PlayerOutfitEquip(pPO, outfitName, clientNetId, true)) {
				LogError("PlayerOutfitEquip() FAILED - '" << pPO->m_charName << ":" << outfitName << "'");
			}

			// Step 3 - Equip Passed In Glid List
			if (glidVec.size() && !EquipInventoryItems(pPO, glidVec, clientNetId, true)) {
				LogError("EquipInventoryItems() FAILED - '" << pPO->m_charName << "'");
			}

			// Step 4 - Return Current Player Outfit (Glids & excGroups)
			auto pPlayerOutfit = PlayerOutfitGetCurrent(pPO, true);
			if (!pPlayerOutfit) {
				LogError("PlayerOutfitGetCurrent() FAILED - '" << pPO->m_charName << "'");
			} else {
				// Return Player Outfit Glids (comma separated values)
				StringHelpers::implode(pPlayerOutfit->glidList, glidsOut);
				// TODO - excGroups
			}
		}
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerSetEquipped, placementId, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);
	*sse->OutBuffer() << glidsOut << excGroups;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::PlayerCheckInventory(ScriptClientEvent* sce) {
	ULONG placementId, globalId, zoneId;
	NETID clientNetId;
	*sce->InBuffer() >> zoneId >> clientNetId >> globalId >> placementId;

	int inventoryCount = 0;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (pPO) {
		// verify item is in inventory
		if (pPO->m_inventory) {
			CInventoryObj* pIO = pPO->m_inventory->GetByGLID(globalId, IT_NORMAL);
			if (pIO) {
				inventoryCount = pIO->getQty();
			}
		}
	} else {
		LogError("Could not find player object with NETID " << clientNetId);
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerCheckInventory, placementId, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);
	*sse->OutBuffer() << inventoryCount;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::PlayerRemoveItem(ScriptClientEvent* sce) {
	ULONG placementId, globalId, zoneId;
	NETID clientNetId;
	*sce->InBuffer() >> zoneId >> clientNetId >> globalId >> placementId;

	int inventoryCount = 0;
	bool scriptItem = false;
	int gameId = 0;
	int kanevaUserId = 0;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (pPO) {
		gameId = m_multiplayerObj->m_serverNetwork->pinger().GetGameId();

		if (pPO->m_pAccountObject) {
			kanevaUserId = pPO->m_pAccountObject->m_serverId;

			if (pPO->m_inventory) {
				CInventoryObj* pIO = pPO->m_inventory->GetByGLID(globalId, IT_NORMAL);
				if (pIO != 0) {
					scriptItem = (pIO->m_itemData->m_useType == USE_TYPE_PREMIUM);

					if (pPO->m_inventory->DeleteByGLID(globalId, 1, false, IT_NORMAL, false)) {
						CInventoryObj* playerInvObj = pPO->m_inventory->GetByGLID(globalId, IT_NORMAL);
						if (playerInvObj) {
							inventoryCount = playerInvObj->getQty();
						} else {
							inventoryCount = 0;
						}
					} else {
						LogWarn("Can't remove inventory item for player: " << pPO->m_charName << " glid: " << globalId);
					}
				}
			}
		}
	} else {
		LogError("Could not find player object with NETID " << clientNetId);
	}

	if (scriptItem) {
		m_gameStateDb->UpdateGameInventorySync(kanevaUserId, gameId, globalId, inventoryCount);
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerRemoveItem, placementId, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);
	*sse->OutBuffer() << inventoryCount;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::PlayerAddItem(ScriptClientEvent* sce) {
	ULONG placementId, globalId, zoneId, quantity;
	NETID clientNetId;
	*sce->InBuffer() >> zoneId >> clientNetId >> globalId >> quantity >> placementId;

	int inventoryCount = -1;
	bool scriptItem = false;
	int gameId = 0;
	int kanevaUserId = 0;

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (pPO) {
		gameId = m_multiplayerObj->m_serverNetwork->pinger().GetGameId();
		if (pPO->m_pAccountObject) {
			kanevaUserId = pPO->m_pAccountObject->m_serverId;
			if (pPO->m_inventory) {
				CGlobalInventoryObj* pGIO = CGlobalInventoryObjList::GetInstance()->GetByGLID(globalId);
				if (!pGIO) {
					if (m_gameStateDb) {
						try {
							pGIO = m_gameStateDb->addCustomItem(globalId);
						} catch (KEPException*) {
							LogWarn("Exception trying to add item to player's inventory - glid: " << globalId);
						}
					}
				}

				if (pGIO) {
					scriptItem = (pGIO->m_itemData->m_useType == USE_TYPE_PREMIUM);
					if (scriptItem && pPO->m_inventory->AddInventoryItem(quantity, false, pGIO->m_itemData, IT_NORMAL, false)) {
						CInventoryObj* playerInvObj = pPO->m_inventory->GetByGLID(globalId, IT_NORMAL);
						if (playerInvObj) {
							inventoryCount = playerInvObj->getQty();
						} else {
							inventoryCount = 0;
						}
					} else {
						LogWarn("Can't add inventory item for player: " << pPO->m_charName << " glid: " << globalId);
					}
				} else {
					LogWarn("Exception trying to add item to player's inventory - glid: " << globalId);
				}
			}
		}
	} else {
		LogError("Could not find player object with NETID " << clientNetId);
	}

	if (scriptItem) {
		m_gameStateDb->UpdateGameInventorySync(kanevaUserId, gameId, globalId, inventoryCount);
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlayerAddItem, placementId, zoneId, 0, clientNetId);
	sse->SetFrom(clientNetId);
	*sse->OutBuffer() << inventoryCount;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::DeleteDynamicObject(ScriptClientEvent* sce) {
	ULONG objId = 0;
	LONG zoneId = 0;
	LONG instanceId = 0;
	ScriptClientEvent::DecodeObjectDelete(sce, objId, zoneId, instanceId);
	ZoneIndex zoneIndex(zoneId);

	// DRF - Added - Cleanup Paper Dolls
	m_multiplayerObj->m_pServerItemObjMap->DeleteByPlacementId(zoneIndex, objId);

	IEvent* e = new RemoveDynamicObjectEvent(objId);
	e->SetFrom(sce->From());
	e->SetFilter(RemoveDynamicObjectEvent::FILTER_SCRIPTSERVER);
	BroadcastEventFromZoneIndex(e, zoneId);
}

bool ServerEngine::isItemAvailable(int globalId, int zoneId) {
	if (!m_useScriptAvailableItemsTable) {
		// All items are available for scripting
		return true;
	}

	if (m_availableScriptItems.find(globalId) == m_availableScriptItems.end()) {
		std::stringstream ssErrMsg;
		ssErrMsg << "Script item not found in inventory: " << globalId;
		IEvent* e = new RenderTextEvent(ssErrMsg.str().c_str(), eChatType::System);
		if (e) {
			BroadcastEventFromZoneIndex(e, zoneId);
		}
		return false;
	}
	return true;
}

void ServerEngine::addScriptAvailableItemsToInventory(CInventoryObjList* pInvObjList) {
	if (!pInvObjList)
		return;

	int perfTrack_OBLIST1 = pInvObjList->GetCount();
	int perfTrack_GIL1 = CGlobalInventoryObjList::GetInstance()->GetCount();
	int perfTrack_SSAI1 = m_availableScriptItems.size();
	int perfTrack_WPI = 0;
	int perfTrack_WMG = 0;
	clock_t startTime = clock();

	for (std::set<int, std::less<int>>::iterator itr = m_availableScriptItems.begin(); itr != m_availableScriptItems.end(); itr++) {
		CGlobalInventoryObj* invObj = CGlobalInventoryObjList::GetInstance()->GetByGLID(*itr);
		if (invObj != NULL) {
			if (invObj->m_itemData->m_useType != USE_TYPE_PREMIUM) {
				CInventoryObj* newEntry = new CInventoryObj();
				newEntry->m_itemData = invObj->m_itemData;
				newEntry->setQty(1);
				newEntry->setArmed(false);
				newEntry->setInventoryType(IT_NORMAL);
				newEntry->clearDirty();
				pInvObjList->AddTail(newEntry);
			} else {
				perfTrack_WPI++;
			}
		} else {
			perfTrack_WMG++;
		}
	}

	clock_t clockTicksTaken = (clock() - startTime);
	int perfTrack_OBLIST2 = pInvObjList->GetCount();
	int perfTrack_GIL2 = CGlobalInventoryObjList::GetInstance()->GetCount();
	int perfTrack_SSAI2 = m_availableScriptItems.size();
	LogError("InvTrack _addSAII_1ObList1," << perfTrack_OBLIST1 << ",GIL1," << perfTrack_GIL1 << ",SSAI1," << perfTrack_SSAI1 << ",ObList2," << perfTrack_OBLIST2 << ",GIL2," << perfTrack_GIL2 << ",SSAI2," << perfTrack_SSAI2 << ",WPI," << perfTrack_WPI << ",WMG," << perfTrack_WMG << ",clockTicksTaken," << clockTicksTaken);
}

void ServerEngine::addScriptAvailableItemsToInventory(const std::vector<int>& glids, CInventoryObjList* pInvObjList) {
	// validate args
	if (glids.empty()) {
		LogWarn("developer warning calling function with empty glids -- early exit");
		return;
	}

	if (pInvObjList == NULL) {
		LogWarn("developer warning calling function with null pInvObjList -- early exit");
		return;
	}

	for (std::vector<int>::const_iterator itr = glids.begin(); itr != glids.end(); itr++) {
		int glid = *itr;
		CGlobalInventoryObj* invObj = CGlobalInventoryObjList::GetInstance()->GetByGLID(glid);
		if (invObj != NULL) {
			if (invObj->m_itemData->m_useType != USE_TYPE_PREMIUM) {
				CInventoryObj* newEntry = new CInventoryObj();
				newEntry->m_itemData = invObj->m_itemData;
				newEntry->setQty(1);
				newEntry->setArmed(false);
				newEntry->setInventoryType(IT_NORMAL);
				pInvObjList->AddTail(newEntry);
			} else {
			}
		} else {
		}
	}

	return;
}

std::set<int, std::less<int>> ServerEngine::getAvailableScriptItems() {
	return m_availableScriptItems;
}

void ServerEngine::HandleObjectGeneration(ScriptClientEvent* sce, LONG zoneId, LONG instanceId, ULONG globalId, ULONG placementId, int placementType, int gameItemId, double x, double y, double z, double dx, double dy, double dz, int ownerId) {
	// The script server hasn't been changed, so I need to calculate the slide vector based on
	// the assumption that up is still 0,1,0.
	Vector3f dir(dx, dy, dz);
	Vector3f up(0, 1.0f, 0);
	Vector3f slide;
	slide = up.Cross(dir);

	//Check the inventory from GMs to make sure it was purchased from Kaneva at least once.
	if (!isItemAvailable(globalId, zoneId)) {
		LogWarn("Item not found in available script items (gm inventory) - glid=" << globalId);
		return;
	}

	bool derivable = false;
	if (globalId != 0) {
		CGlobalInventoryObj* globalInvObj = m_globalInventoryDB->GetByGLID(globalId);
		if (!globalInvObj) {
			LogWarn("Invalid glid=" << globalId);
			return;
		}
		derivable = globalInvObj->m_itemData->m_derivable;
	}

	IEvent* e = NULL;
	switch (placementType) {
		case Placement_DynObj:
		case Placement_Frame:
			e = new AddDynamicObjectEvent(
				PLACEMENT_GENERATE,
				ownerId,
				globalId,
				x, y, z,
				dir.x, dir.y, dir.z,
				slide.x, slide.y, slide.z,
				placementId,
				0,
				0,
				"", // empty texture Url on add
				0, // leave friend id as 0
				"", "", // empty swf and params initially
				false, false, false, false, // default values for dynamic object attributes
				-0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f, // send a default boundbox
				derivable,
				IT_GIFT,
				gameItemId);
			break;

		case Placement_Sound:
			e = new AddSoundEvent(PLACEMENT_GENERATE, globalId, ObjTypeToInt(ObjectType::DYNAMIC), placementId, 0, x, y, z);
			break;
	}

	e->SetFrom(sce->From());

	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::GenerateDynamicObject(ScriptClientEvent* sce) {
	LONG zoneId = 0, instanceId = 0;
	ULONG globalId = 0, placementId = 0;
	int placementType = 0, gameItemId = 0;
	double x = 0, y = 0, z = 0, dx = 0, dy = 0, dz = 0;
	ScriptClientEvent::DecodeObjectGenerate(sce, placementId, globalId, zoneId, instanceId, x, y, z, dx, dy, dz, placementType, gameItemId);

	// Provide a default ownerId of 0.
	HandleObjectGeneration(sce, zoneId, instanceId, globalId, placementId, placementType, gameItemId, x, y, z, dx, dy, dz, 0);
}

void ServerEngine::GenerateOwnedDynamicObject(ScriptClientEvent* sce) {
	LONG zoneId = 0, instanceId = 0;
	ULONG globalId = 0, placementId = 0;
	int placementType = 0, gameItemId = 0, ownerId = 0;
	double x = 0, y = 0, z = 0, dx = 0, dy = 0, dz = 0;
	ScriptClientEvent::DecodeOwnedObjectGenerate(sce, placementId, globalId, zoneId, instanceId, x, y, z, dx, dy, dz, placementType, gameItemId, ownerId);

	HandleObjectGeneration(sce, zoneId, instanceId, globalId, placementId, placementType, gameItemId, x, y, z, dx, dy, dz, ownerId);
}

void ServerEngine::ScriptServerRotateObject(ScriptClientEvent* sce) {
	ULONG objId = 0;
	LONG zoneId = 0, instanceId = 0;
	double dx = 0, dy = 0, dz = 0, sx = 0, sy = 0, sz = 0, timeMs = 0;
	ScriptClientEvent::DecodeObjectRotate(sce, objId, zoneId, instanceId, dx, dy, dz, sx, sy, sz, timeMs);

	IEvent* e = new RotateDynamicObjectEvent((int)objId, dx, dy, dz, sx, sy, sz, timeMs);
	e->SetFrom(sce->From());
	BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::ScriptServerScaleObject(ScriptClientEvent* sce) {
	LONG objId;
	int zoneId, instanceId;
	double x, y, z;
	*sce->InBuffer() >> objId >> zoneId >> instanceId >> x >> y >> z;
	IEvent* e = new ScaleDynamicObjectEvent(objId, x, y, z);
	e->SetFrom(sce->From());

	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);

	return;
}

void ServerEngine::ScriptServerObjectSetDrawDistance(ScriptClientEvent* sce) {
	LONG objId;
	int zoneId, instanceId;
	double dist;
	*sce->InBuffer() >> objId >> zoneId >> instanceId >> dist;

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::ObjectSetDrawDistance, 0);
	*e->OutBuffer() << zoneId << objId << dist;
	BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::ScriptServerMoveObject(ScriptClientEvent* sce) {
	ULONG objId = 0;
	LONG zoneId = 0, instanceId = 0;
	double x = 0, y = 0, z = 0, timeMs = 0, accDur = 0, constVelDur = 0, accDistFrac = 0, constVelDistFrac = 0;
	ScriptClientEvent::DecodeObjectMove(sce, objId, zoneId, instanceId, x, y, z, timeMs, accDur, constVelDur, accDistFrac, constVelDistFrac);

	IEvent* e = new MoveDynamicObjectEvent((int)objId, x, y, z, timeMs, accDur, constVelDur, accDistFrac, constVelDistFrac);
	e->SetFrom(sce->From());

	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

//This is similar to ServerEngine::MoveDynamicObjectEventHandler but we dont want to check permissions
//so that others who enter the zone can move an object if necessary.
void ServerEngine::ScriptServerMoveAndRotateObject(ScriptClientEvent* sce) {
	int objId = 0;
	LONG zoneId = 0, instanceId = 0;
	double x = 0, y = 0, z = 0, dx = 0, dy = 0, dz = 0, sx = 0, sy = 0, sz = 0, timeMs = 0;
	*sce->InBuffer() >> objId >> zoneId >> instanceId >> x >> y >> z >> dx >> dy >> dz >> sx >> sy >> sz >> timeMs;

	IEvent* e = new MoveAndRotateDynamicObjectEvent(objId, x, y, z, dx, dy, dz, sx, sy, sz, timeMs);
	e->SetFrom(sce->From());

	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

// Broadcasts a UpdateDynamicObjectTextureEvent to the players
void ServerEngine::ObjectSetTexture(ScriptClientEvent* sce) {
	LONG objectId;
	int assetId;
	std::string textureURL;
	int playerId;
	int zoneId;
	*sce->InBuffer() >> objectId >> assetId >> textureURL >> playerId >> zoneId;

	IEvent* e = new UpdateDynamicObjectTextureEvent(objectId, assetId, textureURL.c_str(), playerId);
	e->SetFrom(sce->From());

	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

// Broadcasts a SetDynamicObjectTexturePanningEvent to zone
void ServerEngine::ObjectSetTexturePanning(ScriptClientEvent* sce) {
	LONG objectId;
	int meshId, uvSetId;
	float uIncr, vIncr;
	int zoneId;

	*sce->InBuffer() >> objectId >> meshId >> uvSetId >> uIncr >> vIncr >> zoneId;

	IEvent* e = new SetDynamicObjectTexturePanningEvent(objectId, meshId, uvSetId, uIncr, vIncr);
	e->SetFrom(sce->From());

	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

// This is where events from the script server come in and are
// processed before calling the overloaded ObjectControl method.
void ServerEngine::ObjectControl(ScriptClientEvent* sce) {
	LONG placementId;
	int zoneId, instanceId;
	ULONG ctlId;
	int argCount;
	std::vector<int> ctlArgs;

	*sce->InBuffer() >> placementId >> zoneId >> instanceId >> ctlId >> argCount;
	ctlArgs.resize(argCount);

	for (int i = 0; i < argCount; i++) {
		*sce->InBuffer() >> ctlArgs[i];
	}

	ObjectControl(placementId, zoneId, sce->From(), ctlId, ctlArgs, sce);
}

void ServerEngine::ObjectControl(LONG placementId, int zoneId, NETID netId_from, ULONG ctlId, std::vector<int>& ctlArgs, ScriptClientEvent* sce) {
	// NOTE: ObjectControl function is used by both scriptserver APIs and client menus. The requested change should persist only if it comes from client menu.
	// If the request is from an API, we will have a valid sce parameter. Otherwise sce will be NULL.
	if (!sce) {
		switch (ctlId) {
			case ANIM_START: {
				auto numArgs = ctlArgs.size();
				if (!numArgs)
					return;

				// Use a zone customization background event, like we do for updating dynamic object textures.
				int animGlidSpecial = ctlArgs[0]; // animGlidSpecial < 0 = reverse animation
				eAnimLoopCtl animLoopCtl = eAnimLoopCtl::Default;
				if (numArgs > 1)
					animLoopCtl = (eAnimLoopCtl)ctlArgs[1];
				AnimSettings animSettings;
				animSettings.setGlidSpecial(animGlidSpecial);
				animSettings.setLoopCtl(animLoopCtl);
				if (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0 && IS_UGC_GLID(animSettings.m_glid)) {
					// if in 3d app validate ownership of ugc animations
					CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(animSettings.m_glid);
					if (!pGIO || pGIO->m_itemData->m_useType != USE_TYPE_ANIMATION) {
						CStringA errMsg;
						errMsg.Format("Invalid animation GLID = %d.  Animation missing from app inventory or Glid is not an animation", animSettings.m_glid);
						LogError(errMsg.GetString());
						RenderTextEvent* te = new RenderTextEvent(errMsg.GetString(), eChatType::System);
						te->AddTo(netId_from);
						BroadcastEventFromZoneIndex(te, zoneId);
						return;
					}
				}
				ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
				zce->type = ZoneCustomizationBackgroundEvent::ZC_UPDATE_ANIMATION;
				zce->zoneIndex = zoneId;
				zce->objPlacementId = placementId;
				zce->globalId = animGlidSpecial; // drf - changed from assetId to globalId
				zce->animSettings = animSettings; // drf - added
				zce->SetFrom(netId_from);
				m_gameStateDispatcher->QueueEvent(zce);
			} break;

			case PARTICLE_STOP: {
				ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
				zce->type = ZoneCustomizationBackgroundEvent::ZC_UPDATE_PARTICLE;
				zce->zoneIndex = zoneId;
				zce->objPlacementId = placementId;
				zce->globalId = GLID_INVALID;
				zce->SetFrom(netId_from);
				m_gameStateDispatcher->QueueEvent(zce);
				return;
			} break;
			default:
				// Nothing we need to do on the server for other types of control messages.
				break;
		}
	}

	IEvent* e = new ControlDynamicObjectEvent(placementId, ctlId, ctlArgs);
	e->SetFrom(netId_from);

	if (sce && (sce->GetScope() == ScriptClientEvent::Scope_Player)) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::PlayerTell(ScriptClientEvent* sce) {
	std::string msg, title;
	int placementId, isTell;
	ULONG netId;
	*sce->InBuffer() >> msg >> title >> placementId >> isTell >> netId;
	msg = title + SAY_TEXT + msg;

	IEvent* pEvent = new TextEvent(msg.c_str(), SlashCommand::NONE, sce->From(), eChatType::ScriptObject);
	if (!pEvent)
		return;

	pEvent->SetFrom(netId);
	m_dispatcher.QueueEvent(pEvent);
}

void ServerEngine::PlayerSetAnimation(ScriptClientEvent* sce) {
	ULONG netId;
	GLID animGLID;

	*sce->InBuffer() >> netId >> animGLID;

	auto pPO = GetPlayerObjectByNetId(netId, true);
	if (!pPO)
		return;

	if (!ValidateAnimation(pPO, animGLID, animGLID))
		return;

	// Pass this scriptclientevent to Client
	sce->OutBuffer()->SetFromReadable(sce->InBuffer());
	sce->AddTo(netId);
	m_dispatcher.QueueEvent(sce);
}

void ServerEngine::PlayerSetEquippedItemAnimation(ScriptClientEvent* sce) {
	ULONG netId;
	ItemAnimation ia;

	*sce->InBuffer() >> netId >> ia.m_glidItem >> ia.m_glidAnim;
	auto pPO = GetPlayerObjectByNetId(netId, true);
	if (!pPO)
		return;

	if (ia.m_glidAnim != 0) {
		// only bother to validate an actual animation
		// it's always legal to try to turn off an animation
		ia = ValidateItemAnimation(pPO, ia);
		if (ia.m_glidAnim == 0) {
			// Unauthorized animation request
			// TODO: send an event back to scriptserverengine
			return;
		}
	}

	// I originally thought I could could process the event here, as if it was a request sent from the client, but
	// then I realized that the client validates that the item is equipped before sending the message, so I need
	// to send this to the client so it can do that validation.  The client will handle this message the same as
	// it would handle a request from a menu to do the same thing, i.e. send an event back to the game server for
	// validation and distribution to other clients in the zone.
	sce->OutBuffer()->SetFromReadable(sce->InBuffer());
	sce->AddTo(netId);
	m_dispatcher.QueueEvent(sce);
}

void ServerEngine::PlayerGotoUrl(ScriptClientEvent* sce) {
	std::string url;
	std::string gameName;
	NETID netId;
	*sce->InBuffer() >> url >> netId;

	GotoPlaceEvent* gpe = new GotoPlaceEvent();
	char temp[500];
	m_multiplayerObj->m_serverNetwork->pinger().GetGameName(temp, _countof(temp));
	std::string myGameName(temp);

	// not for this game, send to client
	gpe->GotoUrl(0, url.c_str());
	gpe->SetFrom(netId);
	StpUrl::ExtractGameName(url, gameName);
	if (STLCompareIgnoreCase(gameName, myGameName) == 0 ||
		atol(gameName.c_str()) == m_multiplayerObj->m_serverNetwork->pinger().GetGameId()) {
		gpe->m_asAdmin = true; // process this event asAdmin since from SS so we can send them anywhere
	} else {
		gpe->AddTo(netId);
	}
	m_dispatcher.QueueEvent(gpe);
}

void ServerEngine::PlayerDefineAnimation(ScriptClientEvent* sce) {
	ULONG netId;
	GLID standId, walkId, runId, jumpId;

	*sce->InBuffer() >> netId >> standId >> runId >> walkId >> jumpId;

	auto pPO = GetPlayerObjectByNetId(netId, true);
	if (!pPO)
		return;

	if (!ValidateAnimation(pPO, standId, walkId) || !ValidateAnimation(pPO, runId, jumpId))
		return;

	// Pass this scriptclientevent to Client
	sce->OutBuffer()->SetFromReadable(sce->InBuffer());
	sce->AddTo(netId);
	m_dispatcher.QueueEvent(sce);
}

void ServerEngine::ObjectSetParticle(ScriptClientEvent* sce) {
	ULONG clientId = 0;
	ULONG placementId = 0;
	LONG zoneId = 0, instanceId = 0;
	int glid = 0;
	std::string boneName;
	int particleSlotId = 0;
	double ofsX = 0, ofsY = 0, ofsZ = 0, dirX = 0, dirY = 0, dirZ = 0, upX = 0, upY = 0, upZ = 0;

	ScriptClientEvent::DecodeObjectSetParticle(sce, clientId, placementId, zoneId, instanceId, glid, boneName, particleSlotId,
		ofsX, ofsY, ofsZ, dirX, dirY, dirZ, upX, upY, upZ);

	GLID particleGLID = (GLID)glid;

	//Check the inventory from GMs to make sure it was purchased from Kaneva at least once.
	if (!isItemAvailable(particleGLID, zoneId)) {
		LogWarn("Particle item not found in available script items (gm inventory) - particleGlid=" << particleGLID);
		return;
	}

	CGlobalInventoryObj* globalInvObj = m_globalInventoryDB->GetByGLID(particleGLID);
	if (!globalInvObj) {
		LogWarn("Invalid particleGlid=" << particleGLID);
		return;
	}

	IEvent* e = new AddParticleEvent(particleGLID, ObjectType::DYNAMIC, placementId, boneName, particleSlotId,
		(float)ofsX, (float)ofsY, (float)ofsZ,
		(float)dirX, (float)dirY, (float)dirZ,
		(float)upX, (float)upY, (float)upZ);
	e->SetFrom(sce->From());

	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::ObjectRemoveParticle(ScriptClientEvent* sce) {
	ULONG clientId = 0, placementId = 0;
	LONG zoneId = 0, instanceId = 0;
	std::string boneName;
	int particleSlotId = 0;
	ScriptClientEvent::DecodeObjectRemoveParticle(sce, clientId, placementId, zoneId, instanceId, boneName, particleSlotId);

	IEvent* e = new RemoveParticleEvent(ObjectType::DYNAMIC, placementId, boneName, particleSlotId);
	e->SetFrom(sce->From());

	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::ObjectSetCollision(ScriptClientEvent* sce) {
	int zoneId, placementId, collisionValue;
	*sce->InBuffer() >> zoneId >> placementId >> collisionValue;

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::ObjectSetCollision, 0);
	*e->OutBuffer() << zoneId << placementId << collisionValue;
	BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::ObjectAddLabels(ScriptClientEvent* sce) {
	int zoneId;
	ULONG placementId;
	int numberOfLabels;
	*sce->InBuffer() >> zoneId >> placementId >> numberOfLabels;

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::ObjectAddLabels, 0);
	*e->OutBuffer() << placementId << numberOfLabels;
	for (int i = 0; i < numberOfLabels; ++i) {
		std::string text;
		float size, red, green, blue;
		*sce->InBuffer() >> text >> size >> red >> green >> blue;
		*e->OutBuffer() << text << size << red << green << blue;
	}

	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::ObjectSetMedia(ScriptClientEvent* sce) {
	// Decode Event Data
	ZoneIndex zoneId;
	ULONG objId;
	MediaParams mediaParams;
	ScriptClientEvent::DecodeObjectSetMedia(sce, zoneId, objId, mediaParams);

	m_gameStateDb->updateMediaPlayer(zoneId, 0, NULL, objId, mediaParams, true);
}

void ServerEngine::ObjectSetMediaVolumeAndRadius(ScriptClientEvent* sce) {
	// Decode Event Data
	ZoneIndex zoneId;
	ULONG objId;
	MediaParams mediaParams;
	ScriptClientEvent::DecodeObjectSetMediaVolumeAndRadius(sce, zoneId, objId, mediaParams);

	m_gameStateDb->updateMediaPlayerVolumeAndRadius(zoneId, 0, NULL, objId, mediaParams);
}

void ServerEngine::ObjectSetOutline(ScriptClientEvent* sce) {
	int zoneId;
	ULONG placementId;
	float red = 0;
	float green = 0;
	float blue = 0;
	bool show, setColor;
	*sce->InBuffer() >> zoneId >> placementId >> show >> setColor;
	if (setColor) {
		*sce->InBuffer() >> red >> green >> blue;
	}

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::ObjectSetOutline, 0);
	e->SetFrom(sce->From());
	*e->OutBuffer() << placementId << show << setColor;
	if (setColor) {
		*e->OutBuffer() << red << green << blue;
	}
	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::ObjectClearLabels(ScriptClientEvent* sce) {
	int zoneId;
	ULONG placementId;
	std::string labelText;
	*sce->InBuffer() >> zoneId >> placementId;

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::ObjectClearLabels, 0);
	e->SetFrom(sce->From());
	*e->OutBuffer() << placementId;
	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else
		BroadcastEventFromZoneIndex(e, zoneId);
}

void ServerEngine::ObjectSetEffectTrack(ScriptClientEvent* sce) {
	// No translation needed
	sce->SetScope(ScriptClientEvent::Scope_Zone); // original scope is player (why?)
	DispatchAddressedScriptClientEvent(sce);
}

void ServerEngine::PlayerSetEffectTrack(ScriptClientEvent* sce) {
	NETID clientNetId;
	ULONG zoneIndex;
	int numberOfEffects, numberOfStrings;

	*sce->InBuffer() >> zoneIndex >> clientNetId >> numberOfEffects >> numberOfStrings;

	// Translate clientNetId into networkAssignedID
	auto pPO = GetPlayerObjectByNetId((NETID)clientNetId, false);
	if (!pPO)
		return;

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::PlayerSetEffectTrack, 0, ScriptClientEvent::Scope_Zone);
	*e->OutBuffer() << zoneIndex << pPO->m_netTraceId << numberOfEffects << numberOfStrings;

	// transcode all the strings
	for (int i = 0; i < numberOfStrings; ++i) {
		std::string str;
		*sce->InBuffer() >> str;
		*e->OutBuffer() << str;
	}

	// transcode all effects
	for (int i = 0; i < numberOfEffects; ++i) {
		int numberOfArgs = 0;
		*sce->InBuffer() >> numberOfArgs;
		*e->OutBuffer() << numberOfArgs;

		for (int j = 0; j < numberOfArgs; ++j) {
			int arg;
			*sce->InBuffer() >> arg;
			*e->OutBuffer() << arg;
		}
	}

	//Broadcast it to all clients in the zone
	BroadcastEventFromZoneIndex(e, zoneIndex);
}

void ServerEngine::ObjectSetMouseOver(ScriptClientEvent* sce) {
	int zoneId;
	int type, id, mode, cursorType, category;
	std::string toolText;

	*sce->InBuffer() >> zoneId >> type >> id >> mode >> toolText >> cursorType >> category;

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::ObjectSetMouseOver, 0);
	e->SetFrom(sce->From());
	*e->OutBuffer() << type << id << mode << toolText << cursorType << category;

	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		ULONG netId = 0;
		*sce->InBuffer() >> netId;
		e->AddTo(netId);
		m_dispatcher.QueueEvent(e);
	} else {
		BroadcastEventFromZoneIndex(e, zoneId);
	}
}

void ServerEngine::PlayerSetPhysics(ScriptClientEvent* sce) {
	DispatchAddressedScriptClientEvent(sce);
}

void ServerEngine::PlayerSetVisibilityFlag(ScriptClientEvent* sce) {
	DispatchAddressedScriptClientEvent(sce);
}

void ServerEngine::PlayerSetParticle(ScriptClientEvent* sce) {
	ULONG zoneIndex;
	NETID clientNetId;
	std::string boneName;
	int particleSlotId;
	ULONG particleGLID;
	Vector3f offset, dir, up;

	(*sce->InBuffer()) >> zoneIndex;
	(*sce->InBuffer()) >> clientNetId >> particleGLID;

	//Check the inventory from GMs to make sure it was purchased from Kaneva at least once.
	if (!isItemAvailable(particleGLID, zoneIndex)) {
		LogWarn("Particle item not found in available script items (gm inventory) - particleGlid=" << particleGLID);
		return;
	}

	auto pPO = GetPlayerObjectByNetId(clientNetId, false);
	if (!pPO)
		return;

	// Additional parameters
	(*sce->InBuffer()) >> boneName >> particleSlotId >> offset.x >> offset.y >> offset.z >> dir.x >> dir.y >> dir.z >> up.x >> up.y >> up.z;

	AddParticleEvent* ape = new AddParticleEvent(particleGLID, ObjectType::MOVEMENT, pPO->m_netTraceId, boneName, particleSlotId,
		offset.x, offset.y, offset.z,
		dir.x, dir.y, dir.z,
		up.x, up.y, up.z);

	//Broadcast it to all clients in the zone
	BroadcastEventFromZoneIndex(ape, zoneIndex);
}

void ServerEngine::PlayerRemoveParticle(ScriptClientEvent* sce) {
	ULONG zoneIndex;
	NETID netId;
	std::string boneName;
	int particleSlotId;

	(*sce->InBuffer()) >> zoneIndex;
	(*sce->InBuffer()) >> netId;

	auto pPO = GetPlayerObjectByNetId(netId, false);
	if (!pPO)
		return;

	(*sce->InBuffer()) >> boneName >> particleSlotId;

	RemoveParticleEvent* rpe = new RemoveParticleEvent(ObjectType::MOVEMENT, pPO->m_netTraceId, boneName, particleSlotId);
	rpe->SetFrom(sce->From());

	//Broadcast it to all clients in the zone
	BroadcastEventFromZoneIndex(rpe, zoneIndex);
}

void ServerEngine::ScriptSetEnvironment(ScriptClientEvent* sce) {
	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		// Buffered scriptSetEnvironment event. Decode it first.
		ULONG zoneId;
		int size;
		double arg;
		*sce->InBuffer() >> zoneId >> size;
		for (int i = 0; i < size; i++)
			*sce->InBuffer() >> arg;

		// Extract player NETID from event
		ULONG netId = 0;
		*sce->InBuffer() >> netId;

		// Set to IEvent::m_from, will be picked up by DispatchAddressedScriptClientEvent as event destination
		sce->SetFrom(netId);

		// Rewind before forwarding
		sce->RewindInput();
	}

	DispatchAddressedScriptClientEvent(sce);
}

void ServerEngine::ScriptSetDayState(ScriptClientEvent* sce) {
	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		// Buffered scriptSetDayStae event. Decode it first.
		ULONG zoneId;
		int size;
		double arg;
		*sce->InBuffer() >> zoneId >> size;
		for (int i = 0; i < size; i++)
			*sce->InBuffer() >> arg;

		// Extract player NETID from event
		ULONG netId = 0;
		*sce->InBuffer() >> netId;

		// Set to IEvent::m_from, will be picked up by DispatchAddressedScriptClientEvent as event destination
		sce->SetFrom(netId);

		// Rewind before forwarding
		sce->RewindInput();
	}

	DispatchAddressedScriptClientEvent(sce);
}

void ServerEngine::ObjectLookAt(ScriptClientEvent* sce) {
	int zoneIndex;
	ULONG objectId;
	ObjectType targetType;
	ULONG targetNetId;

	(*sce->InBuffer()) >> zoneIndex >> objectId >> reinterpret_cast<int&>(targetType) >> targetNetId;
	if (targetType == ObjectType::MOVEMENT) {
		// Translate clientNetId into networkAssignedID
		auto pPO = GetPlayerObjectByNetId((NETID)targetNetId, false);
		if (pPO) {
			std::string targetBone;
			float offsetX, offsetY, offsetZ;
			bool followTarget, allowXZRotation;
			float forwardX, forwardY, forwardZ;

			(*sce->InBuffer()) >> targetBone >> offsetX >> offsetY >> offsetZ >> followTarget >> allowXZRotation >> forwardX >> forwardY >> forwardZ;

			ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::ObjectLookAt, 0, ScriptClientEvent::Scope_Zone);
			(*e->OutBuffer()) << zoneIndex << objectId << static_cast<int>(targetType) << pPO->m_netTraceId;
			(*e->OutBuffer()) << targetBone << offsetX << offsetY << offsetZ << followTarget << allowXZRotation << forwardX << forwardY << forwardZ;

			//Broadcast it to all clients in the zone
			BroadcastEventFromZoneIndex(e, zoneIndex);
		} else {
			LogWarn("Target player " << (NETID)targetNetId << " not found");
		}
	} else {
		sce->RewindInput();
		DispatchAddressedScriptClientEvent(sce);
	}
}

// Dispatching 'addressed' ScriptClientEvent to specified destination
void ServerEngine::DispatchAddressedScriptClientEvent(ScriptClientEvent* sce) {
	if (sce->GetScope() == ScriptClientEvent::Scope_Player) {
		sce->AddTo(sce->From());
		m_dispatcher.QueueEvent(sce);
	} else {
		int zoneId = 0;
		*sce->InBuffer() >> zoneId;
		BroadcastEventFromZoneIndex(sce, zoneId);
	}
}

void ServerEngine::BroadcastEventFromZoneIndex(IEvent* e, int zoneId) {
	if (!e || !m_multiplayerObj || !m_multiplayerObj->m_currentWorldsUsed)
		return;

	ZoneIndex zi(zoneId);
	CWorldAvailableObj* pWAO = m_multiplayerObj->m_currentWorldsUsed->GetByZoneIndex(zi);
	if (!pWAO)
		return;

#ifndef REFACTOR_INSTANCEID
	ChannelId broadcastChannel(pWAO->m_channel, zi.GetInstanceId(), zi.GetType());
	BroadcastEvent(e, broadcastChannel);
#else
	ChannelId broadcastChannel(pWAO->m_channel.toLong(), zi.GetInstanceId(), zi.GetType());
	BroadcastEvent(e, broadcastChannel.toLong());
#endif
}

EVENT_PROC_RC ServerEngine::ZoneDownloadCompleteEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ServerEngine*)lparam;
	if (!me || !e)
		return EVENT_PROC_RC::OK;

	LogInfo("received event from NETID " << e->From());

	auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
	if (!pPO)
		return EVENT_PROC_RC::OK;

	auto pAO = pPO->m_pAccountObject;
	if (!pAO)
		return EVENT_PROC_RC::OK;

	POSITION chPos = pAO->m_pPlayerObjectList->FindIndex(pAO->m_currentcharInUse);
	if (!chPos) {
		LogError("Missing character for account " << pAO->m_accountName << ", character index " << pAO->m_currentcharInUse << ", NETID " << e->From());
		pAO->GetSpawnInfo().reset();
		return EVENT_PROC_RC::OK;
	}

	CPlayerObject* pPO_find = (CPlayerObject*)pAO->m_pPlayerObjectList->GetAt(chPos);
	if (pPO_find) {
		// Grab previous zone index from last PlayerArrived event sent (could be different from CAccountObject::m_previousZoneIndex)
		NETID prevNetID = 0;
		ZoneIndex prevZoneIndex;
		unsigned prevPlayerArriveId = 0;
		if (!pAO->FetchPendingPlayerDepartInfo(0, prevNetID, prevZoneIndex, prevPlayerArriveId)) {
			prevZoneIndex.fromLong(0);
		}

		// Zone customizations done.  Player now ready to receive script events.
		LogInfo("Send PlayerArrive event for player " << pPO->m_charName << " with NETID " << pPO->m_netId);

		// Get parent zone instance ID
		const ZoneIndex& zoneIndex = pPO->m_currentZoneIndex;
		unsigned parentZoneInstanceId = 0, _pzi, _pzt, _pcid;
		me->m_gameStateDb->GetParentZoneInfo(nullptr, zoneIndex.GetInstanceId(), zoneIndex.GetType(), parentZoneInstanceId, _pzi, _pzt, _pcid);

		// Send PlayerArrive
		ScriptPlayerInfo playerInfo(pPO->m_charName.GetString(), pPO->m_handle, pAO->m_gender, pAO->m_age, pAO->m_showMature, pPO->m_currentZonePriv, pPO->m_title.GetString());
		me->m_multiplayerObj->SendPlayerArrivedEvent(pPO->m_netId, pPO->m_currentZoneIndex, prevNetID, prevZoneIndex, ++me->m_playerArriveCounter, playerInfo, parentZoneInstanceId, pAO);
	}

	//Player added to runtime list.  Attrib data sent.  Arrive message sent. Clear spawn info to allow future spawning.
	pAO->GetSpawnInfo().reset();

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::AvatarSelectionEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ServerEngine*)lparam;
	auto pEvent = dynamic_cast<AvatarSelectionEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	int avatarIndex = 0;
	int aptGlid = 0;
	int aptId = 0;
	pEvent->ExtractInfo(avatarIndex, aptGlid, aptId);

	// Handle Avatar Selection Event
	if (avatarIndex >= 0) {
		me->HandleAvatarSelection(avatarIndex, e->From(), aptGlid, aptId);
	} else {
		std::string name = "unkown";
		auto pPO = me->GetPlayerObjectByNetId(e->From(), true);
		if (pPO)
			name = pPO->m_charName;
	}

	return EVENT_PROC_RC::CONSUMED;
}

void ServerEngine::AvatarSelectionZoneChange(CPlayerObject* pPO, int aptGlid, int aptId) {
	ZoneIndex housingZoneIndex((LONG)pPO->GetNumber(PLAYERIDS_HOUSINGZONEINDEX));
	LONG zid = housingZoneIndex.zoneIndex();
	if (zid == CC_ZONE_INDEX_PLAIN) { //Current character creation zone
		int qty = 0;
		GetInventoryItemQty(pPO, aptGlid, qty);
		if (qty == 0) {
			AddInventoryItemToPlayer(pPO, 1, false, aptGlid);
			UpdatePlayer(pPO);
		} else {
			LogWarn("Apartment deed already exists for " << pPO->m_charName << "! Skipping add. Quantity: " << qty);
		}

		ChangeZoneMap(pPO, aptGlid, aptId, ApartmentUpgrade, IT_NORMAL, false);
	} else {
#ifndef REFACTOR_INSTANCEID
		LogError("Player " << pPO->m_charName << " is not in character creation zone. Housing zone index is " << housingZoneIndex);
#else
		LogError("Player " << pPO->m_charName << " is not in character creation zone. Housing zone index is " << housingZoneIndex.toLong());
#endif
	}
}

void ServerEngine::HandleAvatarSelection(int avatarIndex, NETID playerNetId, int aptGlid, int aptId) {
	auto pPO = GetPlayerObjectByNetId(playerNetId, true);
	if (!pPO || !m_gameStateDb)
		return;

	if (m_gameStateDb->ApplyAvatarSelection(avatarIndex, pPO))
		AvatarSelectionZoneChange(pPO, aptGlid, aptId);
}

EVENT_PROC_RC ServerEngine::ScriptClientEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ServerEngine*)lparam;
	auto pEvent = dynamic_cast<ScriptClientEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::CONSUMED;

	EVENT_PROC_RC ret = EVENT_PROC_RC::CONSUMED;
	auto eventType = pEvent->GetEventType();
	switch (eventType) {
		case ScriptClientEvent::PlayerGetLocation:
			me->PlayerGetLocation(pEvent);
			break;

		case ScriptClientEvent::ObjectDelete:
			me->DeleteDynamicObject(pEvent);
			break;

		case ScriptClientEvent::ObjectGenerate:
			me->GenerateDynamicObject(pEvent);
			break;

		case ScriptClientEvent::OwnedObjectGenerate:
			me->GenerateOwnedDynamicObject(pEvent);
			break;

		case ScriptClientEvent::ObjectRotate:
			me->ScriptServerRotateObject(pEvent);
			break;

		case ScriptClientEvent::ObjectMoveAndRotate:
			me->ScriptServerMoveAndRotateObject(pEvent);
			break;

		case ScriptClientEvent::ObjectMove:
			me->ScriptServerMoveObject(pEvent);
			break;

		case ScriptClientEvent::ObjectScale:
			me->ScriptServerScaleObject(pEvent);
			break;

		case ScriptClientEvent::ObjectSetDrawDistance:
			me->ScriptServerObjectSetDrawDistance(pEvent);
			break;

		case ScriptClientEvent::ObjectSetTexture:
			me->ObjectSetTexture(pEvent);
			break;

		case ScriptClientEvent::ObjectSetTexturePanning:
			me->ObjectSetTexturePanning(pEvent);
			break;

		case ScriptClientEvent::ObjectControl:
			me->ObjectControl(pEvent);
			break;

		case ScriptClientEvent::PlayerTell:
			me->PlayerTell(pEvent);
			break;

		case ScriptClientEvent::PlayerUseItem:
			me->PlayerUseItem(pEvent);
			break;

		case ScriptClientEvent::PlayerEquipItem:
			me->PlayerEquipItem(pEvent);
			break;

		case ScriptClientEvent::PlayerUnEquipItem:
			me->PlayerUnEquipItem(pEvent);
			break;

		case ScriptClientEvent::PlayerSaveEquipped:
			me->PlayerSaveEquipped(pEvent);
			break;

		case ScriptClientEvent::PlayerGetEquipped:
			me->PlayerGetEquipped(pEvent);
			break;

		case ScriptClientEvent::PlayerSetEquipped:
			me->PlayerSetEquipped(pEvent);
			break;

		case ScriptClientEvent::PlayerCheckInventory:
			me->PlayerCheckInventory(pEvent);
			break;

		case ScriptClientEvent::PlayerRemoveItem:
			me->PlayerRemoveItem(pEvent);
			break;

		case ScriptClientEvent::PlayerAddItem:
			me->PlayerAddItem(pEvent);
			break;

		case ScriptClientEvent::PlayerSetAnimation:
			me->PlayerSetAnimation(pEvent);
			break;

		case ScriptClientEvent::PlayerSetEquippedItemAnimation:
			me->PlayerSetEquippedItemAnimation(pEvent);
			break;

		case ScriptClientEvent::PlayerRemoveAllAccessories:
			me->PlayerRemoveAllAccessories(pEvent);
			break;

		case ScriptClientEvent::PlayerDefineAnimation:
			me->PlayerDefineAnimation(pEvent);
			break;

		case ScriptClientEvent::PlayerGotoUrl:
			me->PlayerGotoUrl(pEvent);
			break;

		case ScriptClientEvent::PlayerSetPhysics:
			me->PlayerSetPhysics(pEvent);
			break;

		case ScriptClientEvent::PlayerSetTitle:
			me->PlayerSetTitle(pEvent);
			break;

		case ScriptClientEvent::PlayerSetName:
			me->PlayerSetName(pEvent);
			break;

		case ScriptClientEvent::ObjectSetParticle:
			me->ObjectSetParticle(pEvent);
			break;

		case ScriptClientEvent::ObjectRemoveParticle:
			me->ObjectRemoveParticle(pEvent);
			break;

		case ScriptClientEvent::ObjectSetEffectTrack:
			me->ObjectSetEffectTrack(pEvent);
			break;

		case ScriptClientEvent::PlayerSetEffectTrack:
			me->PlayerSetEffectTrack(pEvent);
			break;

		case ScriptClientEvent::PlayerSetVisibilityFlag:
			me->PlayerSetVisibilityFlag(pEvent);
			break;

		case ScriptClientEvent::PlayerSetParticle:
			me->PlayerSetParticle(pEvent);
			break;

		case ScriptClientEvent::PlayerRemoveParticle:
			me->PlayerRemoveParticle(pEvent);
			break;

		case ScriptClientEvent::PlayerScale:
			me->PlayerScale(pEvent);
			break;

		case ScriptClientEvent::ScriptSetEnvironment:
			me->ScriptSetEnvironment(pEvent);
			break;

		case ScriptClientEvent::ScriptSetDayState:
			me->ScriptSetDayState(pEvent);
			break;

		case ScriptClientEvent::PlayerCameraAttach:
			me->PlayerCameraAttach(pEvent);
			break;

		case ScriptClientEvent::ObjectLookAt:
			me->ObjectLookAt(pEvent);
			break;

		case ScriptClientEvent::ObjectAddLabels:
			me->ObjectAddLabels(pEvent);
			break;

		case ScriptClientEvent::ObjectSetMedia:
			me->ObjectSetMedia(pEvent);
			break;

		case ScriptClientEvent::ObjectSetMediaVolumeAndRadius:
			me->ObjectSetMediaVolumeAndRadius(pEvent);
			break;

		case ScriptClientEvent::ObjectSetOutline:
			me->ObjectSetOutline(pEvent);
			break;

		case ScriptClientEvent::ObjectClearLabels:
			me->ObjectClearLabels(pEvent);
			break;

		case ScriptClientEvent::SoundAttachTo:
			me->SoundAttachTo(pEvent);
			break;

		case ScriptClientEvent::SoundAddModifier:
			me->SoundAddModifier(pEvent);
			break;

		case ScriptClientEvent::SoundPlayOnClient:
			me->SoundPlayOnClient(pEvent);
			break;

		case ScriptClientEvent::RenderLabelsOnTop:
			//Shouldn't be here
			break;

		case ScriptClientEvent::ObjectGetNextPlaylistItem:
			// The playlistmgr blade handles this event
			ret = EVENT_PROC_RC::NOT_PROCESSED;
			break;

		case ScriptClientEvent::ObjectSetNextPlaylistItem:
			// The playlistmgr blade handles this event
			ret = EVENT_PROC_RC::NOT_PROCESSED;
			break;

		case ScriptClientEvent::ObjectEnumeratePlaylist:
			// The playlistmgr blade handles this event
			ret = EVENT_PROC_RC::NOT_PROCESSED;
			break;

		case ScriptClientEvent::ObjectSetPlaylist:
			// The playlistmgr blade handles this event
			ret = EVENT_PROC_RC::NOT_PROCESSED;
			break;

		case ScriptClientEvent::ObjectSetCollision:
			me->ObjectSetCollision(pEvent);
			break;

		case ScriptClientEvent::ObjectSetMouseOver:
			me->ObjectSetMouseOver(pEvent);
			break;

		case ScriptClientEvent::PlayerSetCustomTextureOnAccessory:
			me->SetCustomTextureOnAccessory(pEvent);
			break;

		case ScriptClientEvent::PlaceGameItem:
			me->PlaceGameItem(pEvent);
			break;

		case ScriptClientEvent::ReplaceGameItem:
			me->ReplaceGameItem(pEvent);
			break;

		case ScriptClientEvent::PlayerSaveZoneSpawnPoint:
			me->PlayerSaveZoneSpawnPoint(pEvent);
			break;

		case ScriptClientEvent::PlayerDeleteZoneSpawnPoint:
			me->PlayerDeleteZoneSpawnPoint(pEvent);
			break;

		case ScriptClientEvent::PlayerSpawnVehicle:
			me->PlayerSpawnVehicle(pEvent);
			break;

		case ScriptClientEvent::PlayerLeaveVehicle:
			me->PlayerLeaveVehicle(pEvent);
			break;

		case ScriptClientEvent::NPCSpawn:
			me->NPCSpawn(pEvent);
			break;

		case ScriptClientEvent::GetItemInfo:
			me->ScriptGetItemInfo(pEvent);
			break;

		default:
			if (pEvent->From() != 0) {
				pEvent->OutBuffer()->SetFromReadable(pEvent->InBuffer());
				pEvent->AddTo(pEvent->From());
				me->m_dispatcher.QueueEvent(pEvent);
			}
			break;
	}

	return ret;
}

EVENT_PROC_RC ServerEngine::ScriptServerEventRayIntersectsHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ServerEngine*)lparam;
	auto pSSE = dynamic_cast<ScriptServerEvent*>(e);
	if (!me || !pSSE)
		return EVENT_PROC_RC::NOT_PROCESSED;

	if (pSSE->Filter() != ScriptServerEvent::PlayerRayIntersects)
		return EVENT_PROC_RC::CONSUMED;

	int objectID;
	ObjectType objectType;
	int zoneId;
	int instanceId;
	ULONG placementId;
	float distance;
	*pSSE->InBuffer() >> placementId >> zoneId >> instanceId >> objectID >> reinterpret_cast<int&>(objectType) >> distance;

	*pSSE->OutBuffer() << placementId << zoneId << instanceId;

	switch (objectType) {
		case ObjectType::MOVEMENT: {
			// Find the network id that can be used by the script server to ID the entity.
			CPlayerObject* player = me->GetPlayerObjectByNetTraceId(objectID);
			if (player) {
				objectID = player->m_netId;
			}
		} break;
	}

	*pSSE->OutBuffer() << objectID << ObjTypeToInt(objectType) << distance;

	return EVENT_PROC_RC::OK;
}

ItemAnimation ServerEngine::ValidateItemAnimation(CPlayerObject* player, ItemAnimation ia) {
	// If any of these fail, then just set this item's animation to 0.
	// validate anim in players inventory
	auto pIO = (CInventoryObj*)player->m_inventory->VerifyInventory(ia.m_glidAnim);
	if (ia.m_glidAnim != 0 && !pIO) {
		LogWarn(loadStrPrintf(IDS_DO_ANIM_NOT_IN_INVENTORY, ia.m_glidAnim, player->m_charName).c_str());
		ia.m_glidAnim = 0;
	}

	// validate item in player's inventory
	if (ia.m_glidAnim != 0 && !player->m_inventory->VerifyInventory(ia.m_glidItem)) {
		// Need a string for item not in inventory, if one doesn't already exist (check use item handler)
		LogWarn(loadStrPrintf(IDS_DO_ITEM_NOT_IN_INVENTORY, ia.m_glidItem, player->m_charName, ia.m_glidAnim).c_str());
		ia.m_glidAnim = 0;
	}

	if (ia.m_glidAnim != 0 && !m_gameStateDb->DoesItemSupportAnimation(ia.m_glidItem, ia.m_glidAnim)) {
		// Need a string for animation not supported on item.
		LogWarn(loadStrPrintf(IDS_ANIM_NOT_SUPPORTED_ON_DO, ia.m_glidAnim, ia.m_glidItem).c_str());
		ia.m_glidAnim = 0;
	}

	PassList* zonePassList = GetZonePassList(player->m_currentZoneIndex);
	if (ia.m_glidAnim != 0 && !PassList::canPlayerUseItem(player->m_passList, pIO->m_itemData->m_passList)) {
		LogWarn(loadStrPrintf(IDS_DO_PLAYER_NEEDS_PASS, player->m_charName, ia.m_glidAnim).c_str());
		ia.m_glidAnim = 0;
	}

	if (ia.m_glidAnim != 0 && !PassList::canUseItemInZone(pIO->m_itemData->m_passList, zonePassList)) {
		LogWarn(loadStrPrintf(IDS_DO_ZONE_NEEDS_PASS, ia.m_glidAnim).c_str());
		ia.m_glidAnim = 0;
	}
	return ia;
}

bool ServerEngine::SendClientAttribEventSystemService(NETID playerNetId) {
	// DRF - Tell Player's Client To Initiate System Service
	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::SystemService;
	return m_multiplayerObj->QueueMsgAttribToClient(playerNetId, attribData);
}

EVENT_PROC_RC ServerEngine::SystemServiceEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ServerEngine*)lparam;
	auto pEvent = dynamic_cast<SystemServiceEvent*>(e);
	if (!disp || !me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	// Extract User Id
	std::string userId;
	pEvent->ExtractInfo(userId);
	NETID fromNetId = pEvent->From();

	// Find Player By User Id
	auto pPO = me->GetPlayerObjectByName(userId);
	if (!pPO)
		return EVENT_PROC_RC::OK;

	// Send System Service To User Id Matched Client
	NETID toNetId = pPO->m_netId; //m_netTraceId;
	LogWarn("SendClientAttribEventSystemService -> '" << userId << "' toNetId=" << toNetId << " fromNetId=" << fromNetId);
	me->SendClientAttribEventSystemService(toNetId);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::UpdateEquippableAnimationEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ServerEngine*)lparam;
	auto pEvent = dynamic_cast<UpdateEquippableAnimationEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	std::vector<ItemAnimation> itemAnims;
	NETID netIdFrom = pEvent->From();
	NETID initiatorNetAssignedID = 0;
	pEvent->ExtractInfo(initiatorNetAssignedID, itemAnims);
	auto pPO = me->GetPlayerObjectByNetId(netIdFrom, false);
	if (!pPO)
		return EVENT_PROC_RC::OK;

	// Only process animations that can be legally applied to the item.
	// Record the animations in use on the CPlayerClass, so players spawning in later can get a full list
	// via the MovementObjectCfg event.  Replace the current list with the new list.
	pPO->m_itemAnimations.clear();
	for (const auto& ia : itemAnims)
		pPO->m_itemAnimations.push_back(me->ValidateItemAnimation(pPO, ia));

	// Broadcast the change
	IEvent* pEvent_ueae = new UpdateEquippableAnimationEvent(initiatorNetAssignedID, pPO->m_itemAnimations);
#ifndef REFACTOR_INSTANCEID
	me->BroadcastEvent(pEvent_ueae, pPO->m_currentChannel);
#else
	me->BroadcastEvent(pEvent_ueae, pPO->m_currentChannel.toLong());
#endif

	return EVENT_PROC_RC::OK;
}

void ServerEngine::GetItemLimits(ULONG& charLimit, ULONG& bankLimit) {
	charLimit = m_multiplayerObj->m_maxItemLimit_onChar;
	bankLimit = m_multiplayerObj->m_maxItemLimit_inBank;
}

EVENT_PROC_RC ServerEngine::ClientExitHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ServerEngine*)lparam;
	if (!me || !e)
		return EVENT_PROC_RC::NOT_PROCESSED;

	auto pAO = me->GetAccountObjectByNetId(e->From());
	if (!pAO)
		return EVENT_PROC_RC::OK;

	LogInfo("Client cleanly exiting: " << pAO->m_accountName);
	me->m_multiplayerObj->LogoutCleanup(pAO, e->Filter() ? DISC_PLAYER_ZONED : DISC_NORMAL);

	return EVENT_PROC_RC::OK;
}

ServerEngine::ServerEngine() :
		Engine(eEngineType::Server, SERVER_VERSION, "ServerEngine"), m_broadcastOpenArena(false), m_gameStateFileName("default.svr"), m_weaponFiredEventId(BAD_EVENT_ID), m_weaponDamagedEventId(BAD_EVENT_ID), m_skillUsedEventId(BAD_EVENT_ID), m_playerSpawnedEventId(BAD_EVENT_ID), m_spawnToBirthPointId(BAD_EVENT_ID), m_hasUseSkillHandler(0), m_adminOnly(false), m_playerCreatedId(BAD_EVENT_ID), m_playerArmItemEventId(BAD_EVENT_ID), m_playerUseItemEventId(BAD_EVENT_ID), m_playerEquipItemEventId(BAD_EVENT_ID), m_playerUnEquipItemEventId(BAD_EVENT_ID), m_gameStateDb(0), m_loginNotificationDelay(10), m_serverId(0), m_population(0), m_populationPeakPerf(0), m_populationPeakPing(0), m_messageCount(0), m_prevMessageCount(0), m_monitor(m_logger), m_mainThdId(0), m_gameStateDispatcher(0), m_playerLastClickedXId(BAD_EVENT_ID), m_playerLastClickedYId(BAD_EVENT_ID), m_playerLastClickedZId(BAD_EVENT_ID), m_playerLastClickedSurfaceNormalXId(BAD_EVENT_ID), m_playerLastClickedSurfaceNormalYId(BAD_EVENT_ID), m_playerLastClickedSurfaceNormalZId(BAD_EVENT_ID), m_changeZoneMapId(0), m_lastTargetPosId(0), m_broadcastToDistrEventId(0), m_validatePlayerId(0), m_uMsgKgpServerHeartbeat(0), m_uMsgKgpServerAlert(0), m_hwndHeartbeatMonitor(NULL), m_heartbeatMonitorHwndTimestamp(0), m_lastHeartbeatTimestamp(0), m_hEventPingerRefresh(NULL), m_backgroundDefaultHandling(true), m_backgroundAttribToServer(false), m_backgroundUpdateToServer(false)
#ifdef FIX_MTQUEUE
		,
		m_optimizeForSingleThread(false)
#endif
		,
		m_threadPool(5, std::string("ServerEngineThreadPool")),
		m_dbConfig(NULL),
		_metricsAgent(new NullMetricsAgent()),
		_isMyMetricsAgent(true),
		m_useScriptAvailableItemsTable(true),
		m_soundCustomizationReplyEventId(BAD_EVENT_ID),
		m_titleEvent(BAD_EVENT_ID),
		m_titleListEvent(BAD_EVENT_ID),
		m_playerArriveCounter(0),
		m_soundRESTHandler(*this) {

#ifdef _DEBUG
	// Apparently certain function we call during KGPServer/KEPServer initialization has set error
	// mode to STDERR. As a result, any server-side CRT errors including assertion failures would
	// be sent to stderr and could not be ignored/retried.
	//
	// Call _set_error_mode explicitly to force CRT errors to use the dialog so that the developer
	// will have a chance to intervene.
	_set_error_mode(_OUT_TO_MSGBOX);
#endif

	// Still some messiness.  We insure cleanup of plugins by using ref ptr.  But that requires
	// constantly doing dyncasts to specialize the plugin.
	try {
		// Note that for better decouple the name of this plugin should be configured for
		// the client component.  e.g. KGPServer.xml should specify e.g.
		// <kgpserver>
		//   <MetricsAgent name="Metrics">
		// </kgpserver>
		//
		MetricsAgent* pMetricsAgent = PluginHost::singleton().DEPRECATED_getInterface<MetricsAgent>("Metrics");

		// There is a plugin that provides MetricsAgent functionality.  Use it.
		//
		if (pMetricsAgent != 0) {
			// this means we need to delete the default implementation (NullMetricsAgent)
			//
			delete _metricsAgent;

			// Flag it as not being ours to delete.
			//
			_isMyMetricsAgent = false;

			// And finally, reference the plugin implementation
			//
			_metricsAgent = pMetricsAgent;
		}
	} catch (const PluginException& pe) {
		LogError(pe.str());
	}

	m_commerceDB = NULL;
	m_commerceDB = new CCommerceObjList();

	m_bankZoneList = NULL;
	m_bankZoneList = new CBankObjList();

	m_secureTradeDB = NULL;
	m_secureTradeDB = new CSecureTradeObjList();

#if (DRF_OLD_VENDORS == 1)
	m_serverItemGenerator = NULL;
	m_serverItemGenerator = new CServerItemGenObjList();
#endif

#if (DRF_OLD_PORTALS == 1)
	m_portalDatabase = new CPortalSysList();
#endif

	m_collisionDatabase = new CCollisionObjList();
	m_gmPageListRuntime = new CGmPageList();
	m_worldChannelRuleDB = new CWorldChannelRuleList();
	m_battleSchedulerSys = new CBattleSchedulerList();
}

bool ServerEngine::LoadConfigValues(const IKEPConfig* icfg) {
	// In reality at the upper level, this actually a KEPConfigAdapter.  So it should be that
	// we need only cast to that.
	KEPConfigAdapter* cfg = (KEPConfigAdapter*)(icfg);
	cfg->impl().paramSet().set("ScriptBladesSubDir", ScriptBladesSubDir);

	if (!Engine::LoadConfigValues(cfg))
		return false;

	if (m_scriptDir.empty())
		m_scriptDir = PathAdd(PathBase(), "ServerScripts");

	if (!cfg->GetValue(GAMESTATE_TAG, m_gameStateFileName)) {
		LogError("GetValue(GAMESTATE_TAG) FAILED");
		return false;
	}

	m_broadcastOpenArena = cfg->Get(BROADCASTOPENARENA_TAG, false);

	m_adminOnly = cfg->Get(ADMIN_ONLY_TAG, 0) != 0;

	m_key = cfg->Get(ENGINEKEY_TAG, "");

	m_loginNotificationDelay = cfg->Get(LOGINMSG_DELAY_TAG, 10);

	// Note: reading from DefaultServer.xml
	m_backgroundDefaultHandling = cfg->Get("backgroundDefaultHandling", true);
	m_backgroundAttribToServer = cfg->Get("backgroundAttribToServer", false);
	m_backgroundUpdateToServer = cfg->Get("backgroundUpdateToServer", false);

	_scriptBinDir = cfg->Get("ScriptBinDir", "");

	bqcMonitorConfig.emailHost = std::string(cfg->Get("EMailerHost", "mail.kanevia.com"));
	bqcMonitorConfig.infoThrottleMS = cfg->Get("BackgroundQCInfoThrottleMS", 0);
	bqcMonitorConfig.infoEMailFrom = std::string(cfg->Get("BackgroundQCInfoEMailFrom", ""));
	bqcMonitorConfig.infoEMailTo = std::string(cfg->Get("BackgroundQCInfoEMailTo", ""));
	bqcMonitorConfig.warnThreshold = cfg->Get("BackgroundQCWarnThreshold", 100);
	bqcMonitorConfig.warnThrottleMS = cfg->Get("BackgroundQCWarnThrottleMS", 60000);
	bqcMonitorConfig.warnEMailFrom = std::string(cfg->Get("BackgroundQCWarnEMailFrom", ""));
	bqcMonitorConfig.warnEMailTo = std::string(cfg->Get("BackgroundQCWarnEMailTo", ""));

#ifdef FIX_MTQUEUE
	m_optimizeForSingleThread = (cfg->Get("OptimizeForSingleThread", 1) == 1);
	LOG4CPLUS_INFO(Logger::getInstance(L"STARTUP"), "CONFIG: OptimizeForSingleThread: " << (m_optimizeForSingleThread ? "enabled" : "disabled"));
#endif
	m_useScriptAvailableItemsTable = cfg->Get("UseScriptAvailableItemsTable", 1) == 1;

	//Look for IP address overrides
	if (m_ipAddress == "AUTO") {
		//Check if we have an ip address config file in the local binary folder
		wchar_t szModulePathW[MAX_PATH + 1];
		memset(szModulePathW, 0, sizeof(szModulePathW));

		if (GetModuleFileName(GetModuleHandle(NULL), szModulePathW, MAX_PATH)) {
			wchar_t* pSlash = wcsrchr(szModulePathW, L'\\');
			if (pSlash) {
				*(pSlash + 1) = 0;

				std::string ipAddrCfgPath = Utf16ToUtf8(szModulePathW);
				ipAddrCfgPath.append(IPADDR_CONFIG);

				bool isDir = false;
				if (jsFileExists(ipAddrCfgPath.c_str(), &isDir) && !isDir) {
					try {
						KEPConfig ipAddrCfg;
						ipAddrCfg.Load(ipAddrCfgPath.c_str(), IPADDR_CONFIG_ROOT);

						std::string ipAddrOverride = ipAddrCfg.Get(IPADDR_TAG, "");
						if (!ipAddrOverride.empty()) {
							LogInfo("Override ip address: " << ipAddrOverride);
							m_ipAddress = ipAddrOverride;
						}
					} catch (KEPException* e) {
						LogError("Error reading local ip address config: " << e->ToString());
						delete e;
					}
				}
			}
		}
	}

	return true;
}

bool ServerEngine::Initialize(const char* baseDir, const char* configFile, const DBConfig& dbConfig) {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	LogInfo("baseDir='" << baseDir << "' configFile='" << configFile << "'");

	try {
		HTTPServer::ptr_t httpServer = PluginHost::singleton().getInterface<HTTPServer>("HTTPServer");
		httpServer->registerHandler(m_soundRESTHandler, "/sound");
	} catch (const PluginNotFoundException& e) {
		LogError(e.str() << " No REST handlers registered");
	}

	m_dbConfig = &dbConfig;

	CFileFactory::Initialize(new CFileFactory());

	{
		std::string dbServer;
		int dbPort;
		std::string dbName;
		std::string dbUser;
		std::string dbPassword;
		bool unused;

		dbConfig.Get(dbServer, dbPort, dbName, dbUser, dbPassword, unused);

		KGPConnectionFactory* f = new KGPConnectionFactory(dbServer, dbUser, dbPassword, dbName, (unsigned short)dbPort);
		std::shared_ptr<KGPConnectionFactory> pf(f);
		m_connectionPool = new KGPConnectionPool(pf);

		// Let's verify the connection
		//
		//		std::shared_ptr<KGP_Connection> pc = m_connectionPool->borrow();
		//		Algorithms::Noop();
		// Set up some rest endpoints that can be used to exercise the sound
		// crud
	}
	// need to init GSD early so event ids match up
	m_gameStateDispatcher = new GameStateDispatcher(this, m_dispatcher, m_instanceName.c_str(), dbConfig);
	if (!m_gameStateDispatcher->Init(PathBase().c_str())) {
		LogError("gameStateDispatcher->init() FAILED");
	}

	// Initialize GetBrowserPage web caller instance
	WebCaller::GetInstance(WEB_CALLER_GET_BROWSER_PAGE, true, SERVER_GBP_THREADS);

	if (Engine::Initialize(baseDir, configFile, dbConfig)) {
		m_multiplayerObj = new CMultiplayerObj(m_instanceName.c_str(), this);
		m_multiplayerObj->SetPathBase(PathBase());
		m_multiplayerObj->SetDispatcher(&m_dispatcher);
		m_multiplayerObj->SetGameStateDispatcher(m_gameStateDispatcher);

		m_multiplayerObj->SetContentVersion(m_multiplayerObj->GetContentVersion(PathBase().c_str()));

		FileName fileName = PathAdd(PathGameFiles(), m_autoexec);

		if (!RunScript(fileName))
			return false;

		if (!InitGameStates(m_ipAddress.c_str(), BaseEngine::port())) {
			LogError(loadStr(IDS_LOADGAMESTATE_FAILED));
		} else {
			m_dispatcher.SetGame(this);

			char disabledEdb[128];
			strncpy_s(disabledEdb, _countof(disabledEdb), m_multiplayerObj->m_disabledEdbs, _TRUNCATE);
			char* strTokContext = 0;
			char* p = strtok_s(disabledEdb, ",", &strTokContext);
			while (p) {
				CMovementObj* m = m_movObjRefModelList->getObjByTraceNumber(atol(p));
				if (m) {
					m->setModelType(CEntityPropertiesCtrl::MODEL_TYPE_DISABLED);
					LogInfo("Configuration disabled this EDB: " << p);
				} else {
					LogWarn("Couldn't find EDB specified to be disabled: " << p);
				}
				p = strtok_s(NULL, ",", &strTokContext);
			}

			if (m_multiplayerObj->m_currentWorldsUsed) {
				// fix up the channel ids since they must match the channel of the supported world
				for (POSITION pos = m_battleSchedulerSys->GetHeadPosition(); pos != NULL;) {
					CBattleSchedulerObj* bo = (CBattleSchedulerObj*)m_battleSchedulerSys->GetNext(pos);

					CWorldAvailableObj* wa = m_multiplayerObj->m_currentWorldsUsed->GetByZoneIndex(bo->m_zoneIndex);
					if (wa)
						bo->m_channel = wa->m_channel;
				}
			}

			// initialze the DB
			m_gameStateDb = new MySqlGameState(m_dispatcher, this, m_multiplayerObj, "Database", "Timing", dbConfig);
			try {
				m_gameStateDb->Initialize(PathBase().c_str());
			} catch (KEPException* e) {
				DELETE_AND_ZERO(m_gameStateDb);
				if (e->m_err != S_OK) {
					MessageBeep(-1);
					Sleep(100);
					MessageBeep(-1);
					LogError(loadStr(IDS_DB_INIT_ERROR) << e->ToString());
					e->Delete();

					SendAlertEventToAppController(KGPSERVER_EVENT_DB_INIT_FAILED);
					return false;
				} else { // it is disabled
					LogWarn(loadStr(IDS_DB_DISABLED) << " " << e->ToString());
					e->Delete();
				}
			}

			// Load durations for Kaneva stock animations
			LoadStockAnimationDurationMs();

			// could use IGameState::GetState(), but this is more efficient
			m_multiplayerObj->m_accountDatabase->m_gameStateDb = m_gameStateDb;
			m_multiplayerObj->m_gameStateDb = m_gameStateDb;

			// network init (moved here 9/08 since we want to ping in w/schema version
			//      must do after db init)
			m_multiplayerObj->m_restrictedMode = TRUE; // initially restrict server

			if (EnableServer()) {
				// At this point we have a game id...or should.  Store it in the ObjectRegistry so others can see it.  The opaqueness of the
				// layer between the application (kepserverimpl) and the blades is very opaque making it hard for the application to
				// query blade specific attribution.
				//
				ObjectRegistry::instance().registerObject("GameId", (void*)m_multiplayerObj->m_serverNetwork->pinger().GetGameId(), new NullDestructor());

				LogDebug("Server enabled");
				if (!m_adminOnly)
					m_multiplayerObj->m_restrictedMode = FALSE;
			} else {
				LogError("ServerEngine network initialization failed");
				SendAlertEventToAppController(KGPSERVER_EVENT_NETWORK_INIT_FAILED);
			}

			// Roll back previous change. Now always load script available items so that the item data will be loaded in GlobalInventoryList
			// We will come back to this part once we fixed all the API to load items on the fly.
			// Queue an event to load script available items table
			m_gameStateDb->LoadScriptServerItemAvailability();

			CPlayerObject cplayerObj;
			//register 3 temp variable on CPlayerObject to store the last position clicked
			//by user
			double maxD = (std::numeric_limits<double>::max)();
			m_playerLastClickedXId = cplayerObj.AddNewDoubleMember("LastClickedPosX", maxD);
			m_playerLastClickedYId = cplayerObj.AddNewDoubleMember("LastClickedPosY", maxD);
			m_playerLastClickedZId = cplayerObj.AddNewDoubleMember("LastClickedPosZ", maxD);

			m_playerLastClickedSurfaceNormalXId = cplayerObj.AddNewDoubleMember("LastClickedNormalX", maxD);
			m_playerLastClickedSurfaceNormalYId = cplayerObj.AddNewDoubleMember("LastClickedNormalY", maxD);
			m_playerLastClickedSurfaceNormalZId = cplayerObj.AddNewDoubleMember("LastClickedNormalZ", maxD);

			bool ret = InitEvents();
			LogCounts(m_logger, "after initial load");
			m_threadPool.start();
			return ret;
		}
	}

	return false;
}

class ServerEngineMonitorData : public Monitored::Data {
public:
};

Monitored::DataPtr ServerEngine::monitor() const {
	std::string serverStartTime = "Starting";
	std::string lastPingFormatted = "Starting";
	time_t brett;
	time(&brett);
	std::string tst = ctimeSafe(&brett);

	if (m_multiplayerObj &&
		m_multiplayerObj->m_serverNetwork &&
		m_multiplayerObj->m_serverNetwork->pinger().GetLastPingTime() != NULL) {
		time_t lastPingTimeFormatted = m_multiplayerObj->m_serverNetwork->pinger().GetLastPingTime();
		lastPingFormatted = ctimeSafe(&lastPingTimeFormatted);
	}

	BasicMonitorData* pd = new BasicMonitorData();
	pd->ss << "<ServerEngine "
		   << "HostName=\"" << m_serverHostName.c_str() << "\" "
		   << "IPAddress=\"" << m_serverIpAddress.c_str() << "\" "
		   << "Port=\"" << BaseEngine::port() << "\" "
		   << "StartTime=\"" << serverStartTime.c_str() << "\" "
		   << "LastPingTime=\"" << lastPingFormatted << "\" "
		   << "Population=\"" << m_population << "\" "
		   << "PeakPopulation=\"" << m_populationPeakPing << "\" "
		   << "ConnectionPoolSize=\"" << m_connectionPool->available() << "/" << m_connectionPool->size() << "\" "
		   << ">";

	// Get metrics on the mpo
	// GL_multiplayerObject
	pd->ss << m_gameStateDispatcher->monitor()->toString();
	pd->ss << m_multiplayerObj->monitor()->toString();
	pd->ss << "</ServerEngine>";
	return Monitored::DataPtr(pd);
}

#ifdef _DEBUG
EVENT_PROC_RC testFn(ULONG lparam, IDispatcher* disp, IEvent* e) {
	printf("Got e!\n");
	return EVENT_PROC_RC::OK;
}
#endif

// initialize the event by registering the well-known ones then
// loading all the event blades and letting them register stuff, too
bool ServerEngine::InitEvents() {
	// set the transport for sending, and we're always the server
	m_dispatcher.SetSendHandler(new OldNetworkSender(m_multiplayerObj));
	m_dispatcher.SetNetId(SERVER_NETID);

	RegisterEvent<TriggerEvent>(m_dispatcher);
	RegisterEvent<DeathEvent>(m_dispatcher);
	RegisterEvent<DisconnectedEvent>(m_dispatcher);
	RegisterEvent<InstanceZoneEvent>(m_dispatcher);
	RegisterEvent<StartAISpawnEvent>(m_dispatcher);
	RegisterEvent<StopAISpawnEvent>(m_dispatcher);
	RegisterEvent<ArenaEjectEvent>(m_dispatcher);
	RegisterEvent<NPCListenEvent>(m_dispatcher);
	RegisterEvent<MovementObjectCfgEvent>(m_dispatcher);
	RegisterEvent<VerifyStatEvent>(m_dispatcher);
	RegisterEvent<UseSkillEvent>(m_dispatcher);
	RegisterEvent<ClientExitEvent>(m_dispatcher);
	RegisterEvent<GotoPlaceEvent>(m_dispatcher);
	RegisterEvent<PassListEvent>(m_dispatcher);
	RegisterEvent<PendingSpawnEvent>(m_dispatcher);
	RegisterEvent<TryOnExpireEvent>(m_dispatcher);
	RegisterEvent<MountEvent>(m_dispatcher);
	RegisterTargetingEvents(m_dispatcher);
	RegisterEvent<P2pAnimRequestEvent>(m_dispatcher);
	RegisterEvent<P2pAnimPermissionEvent>(m_dispatcher);
	RegisterEvent<P2pAnimPermissionReplyEvent>(m_dispatcher);
	RegisterEvent<P2pAnimEvent>(m_dispatcher);
	RegisterEvent<AddTriggerEvent>(m_dispatcher);
	RegisterEvent<RemoveTriggerEvent>(m_dispatcher);
	RegisterEvent<UpdateEquippableAnimationEvent>(m_dispatcher);
	RegisterEvent<AvatarSelectionEvent>(m_dispatcher);
	RegisterEvent<ClanEvent>(m_dispatcher);
	RegisterEvent<TitleEvent>(m_dispatcher);
	RegisterEvent<SpaceImportEvent>(m_dispatcher);
	RegisterEvent<ChannelImportEvent>(m_dispatcher);
	RegisterEvent<UpdateScriptZoneSpinDownDelayEvent>(m_dispatcher); // A typed event used by multiple DLLs must be registered separately in each DLL to initialize the m_classId static member for the event class
	RegisterEvent<ZoneImportedEvent>(m_dispatcher);

	m_dispatcher.RegisterHandler(ServerEngine::TargetedPositionEventHandler, (ULONG)this, "TargetedPositionEvent", TargetedPositionEvent::ClassVersion(), TargetedPositionEvent::ClassId(), EP_NORMAL);

	// sent by client, so no need to save off id
	m_dispatcher.RegisterEvent("EntityCollisionEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

	m_dispatcher.RegisterEvent("SpaceImportResponseEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcher.RegisterEvent("ChannelImportResponseEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcher.RegisterEvent("ImportChannelListResponseEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

	//Ankit ----- Registering sound customization events received from client
	m_dispatcher.RegisterEvent("SoundCustomizationRequestEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcher.RegisterEvent("UpdateSoundCustomizationEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcher.RegisterEvent("RemoveSoundCustomizationEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	//Ankit ----- Registering sound customization events sent down from server
	m_soundCustomizationReplyEventId = m_dispatcher.RegisterEvent("SoundCustomizationReplyEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	//Ankit---- - Registering player title events received from client
	m_dispatcher.RegisterEvent("PlayerTitleEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcher.RegisterEvent("PlayerChangeTitleEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	//Ankit ----- Registering player title sent down from server
	m_titleEvent = m_dispatcher.RegisterEvent("TitleEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_titleListEvent = m_dispatcher.RegisterEvent("TitleListEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

	m_weaponFiredEventId = m_dispatcher.RegisterEvent("WeaponFiredEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_weaponDamagedEventId = m_dispatcher.RegisterEvent("WeaponDamagedEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_skillUsedEventId = m_dispatcher.RegisterEvent("SkillUsedEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_playerSpawnedEventId = m_dispatcher.RegisterEvent("PlayerSpawnedEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_spawnToBirthPointId = m_dispatcher.RegisterEvent("RebirthEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_playerUseItemEventId = m_dispatcher.RegisterEvent("PlayerUseItem", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_playerEquipItemEventId = m_dispatcher.RegisterEvent("PlayerEquipItem", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_playerUnEquipItemEventId = m_dispatcher.RegisterEvent("PlayerUnEquipItem", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_playerArmItemEventId = m_dispatcher.RegisterEvent("PlayerArmItem", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_playerCreatedId = m_dispatcher.RegisterEvent("PlayerCreatedEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_changeZoneMapId = m_dispatcher.RegisterEvent("ZoneChangeEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_broadcastToDistrEventId = m_dispatcher.RegisterEvent("BroadcastToDistr", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_validatePlayerId = m_dispatcher.RegisterEvent("ValidateNewPlayer", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	pingerEventId = m_dispatcher.RegisterEvent("PingCompleteEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

	m_dispatcher.RegisterEvent("ZoneCustomizationDLCompleteEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcher.RegisterEvent("BadGotoPlaceURLEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);

	std::string path = PathGameFiles();

	CTriggerObjectList list; // TODO: keep this somewhere?  once Triggers only here, yes, but now still in exgs, if save REMOVE SAFEDELETE BELOW!
	TriggerFile::RegisterAllTriggerEvents(path.c_str(), m_dispatcher, &list);
	list.Clear();

	if (m_gameStateDb != 0)
		m_gameStateDb->InitEvents();

	if (!InitScriptBlades(m_logger, &m_dispatcher, _scriptBinDir, m_scriptDir, IsBaseDirReadOnly(), m_dbConfig, m_eventBladeFactory, m_eventBlades)) {
		LogError("InitScriptBlades() FAILED");
		return false;
	}

	m_dispatcher.RegisterHandler(ServerEngine::AdminCommandHandler, (ULONG)this, "ServerEngine::AdminCommandHandler", TextEvent::ClassVersion(), TextEvent::ClassId(), EP_HIGH);
	m_dispatcher.RegisterHandler(ServerEngine::GetAttributeHandler, (ULONG)this, "ServerEngine::GetAttributeHandler", GetAttributeEvent::ClassVersion(), GetAttributeEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::TextHandler, (ULONG)this, "ServerEngine::TextHandler", TextEvent::ClassVersion(), TextEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::RespawnToRebirthHandler, (ULONG)this, "ServerEngine::RespawnToRebirthHandler", PackVersion(1, 0, 0, 0), m_spawnToBirthPointId, EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::DisconnectClientHandler, (ULONG)this, "ServerEngine::DisconnectClientHandler", PackVersion(1, 0, 0, 0), DisconnectedEvent::ClassId(), EP_HIGH);
	m_dispatcher.RegisterHandler(ServerEngine::ClientExitHandler, (ULONG)this, "ServerEngine::ClientExitHandler", PackVersion(1, 0, 0, 0), ClientExitEvent::ClassId(), EP_HIGH);
	m_dispatcher.RegisterHandler(ServerEngine::ChangeZoneMapHandler, (ULONG)this, "ServerEngine::ChangeZoneMapHandler", PackVersion(1, 0, 0, 0), m_changeZoneMapId, EP_HIGH);
	m_dispatcher.RegisterHandler(ServerEngine::GotoPlaceHandler, (ULONG)this, "ServerEngine::GotoPlaceHandler", GotoPlaceEvent::ClassVersion(), GotoPlaceEvent::ClassId(), EP_HIGH);
	m_dispatcher.RegisterHandler(ServerEngine::BroadcastDistrHandler, (ULONG)this, "ServerEngine::BroadcastDistrHandler", PackVersion(1, 0, 0, 0), m_broadcastToDistrEventId, EP_HIGH);
	m_dispatcher.RegisterHandler(ServerEngine::PendingSpawnHandler, (ULONG)this, "ServerEngine::PendingSpawnHandler", PackVersion(1, 0, 0, 0), PendingSpawnEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::P2pAnimRequestHandler, (ULONG)this, "ServerEngine::P2pAnimRequestHandler", P2pAnimRequestEvent::ClassVersion(), P2pAnimRequestEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::P2pAnimPermissionReplyHandler, (ULONG)this, "ServerEngine::P2pAnimPermissionReplyEvent", P2pAnimPermissionReplyEvent::ClassVersion(), P2pAnimPermissionReplyEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::P2pAnimEventHandler, (ULONG)this, "ServerEngine::P2pAnimEvent", P2pAnimEvent::ClassVersion(), P2pAnimEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::ControlDynamicObjectEventHandler, (ULONG)this, "ControlDynamicObjectEvent", ControlDynamicObjectEvent::ClassVersion(), ControlDynamicObjectEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::MoveDynamicObjectEventHandler, (ULONG)this, "MoveDynamicObjectEvent", MoveDynamicObjectEvent::ClassVersion(), MoveDynamicObjectEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::MoveAndRotateDynamicObjectEventHandler, (ULONG)this, "MoveAndRotateDynamicObjectEvent", MoveAndRotateDynamicObjectEvent::ClassVersion(), MoveAndRotateDynamicObjectEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::RotateDynamicObjectEventHandler, (ULONG)this, "RotateDynamicObjectEvent", RotateDynamicObjectEvent::ClassVersion(), RotateDynamicObjectEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::RemoveDynamicObjectEventHandler, (ULONG)this, "RemoveDynamicObjectEvent", RemoveDynamicObjectEvent::ClassVersion(), RemoveDynamicObjectEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::UpdateDynamicObjectTextureEventHandler, (ULONG)this, "UpdateDynamicObjectTextureEvent", UpdateDynamicObjectTextureEvent::ClassVersion(), UpdateDynamicObjectTextureEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::UpdatePictureFrameFriendEventHandler, (ULONG)this, "UpdatePictureFrameFriendEvent", UpdatePictureFrameFriendEvent::ClassVersion(), UpdatePictureFrameFriendEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::SetWorldObjectTextureEventHandler, (ULONG)this, "UpdateWorldObjectTextureEvent", UpdateWorldObjectTextureEvent::ClassVersion(), UpdateWorldObjectTextureEvent::ClassId(), EP_NORMAL);
	RegisterEvent<SystemServiceEvent>(m_dispatcher);
	m_dispatcher.RegisterHandler(ServerEngine::SystemServiceEventHandler, (ULONG)this, "SystemServiceEvent", SystemServiceEvent::ClassVersion(), SystemServiceEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::UpdateEquippableAnimationEventHandler, (ULONG)this, "UpdateEquippableAnimationEvent", UpdateEquippableAnimationEvent::ClassVersion(), UpdateEquippableAnimationEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::GotoPlaceBackgroundHandler, (ULONG)this, "ServerEngine::GotoPlaceBackgroundEvent", GotoPlaceBackgroundEvent::ClassVersion(), GotoPlaceBackgroundEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::ChangeZoneBackgroundHandler, (ULONG)this, "ServerEngine::ChangeZoneBackgroundEvent", ChangeZoneBackgroundEvent::ClassVersion(), ChangeZoneBackgroundEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::ZoneCustomizationBackgroundEventHandler, (ULONG)this, "ServerEngine::DynObjBackgroundEvent", ZoneCustomizationBackgroundEvent::ClassVersion(), ZoneCustomizationBackgroundEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterFilteredHandler(ServerEngine::GetPlayerBackgroundHandler, (ULONG)this, "ServerEngine::GetPlayerBackgroundHandler", GetResultSetsEvent::ClassVersion(), GetResultSetsEvent::ClassId(), GetResultSetsEvent::PLAYER);
	m_dispatcher.RegisterFilteredHandler(ServerEngine::GetAccountBackgroundHandler, (ULONG)this, "ServerEngine::GetAccountBackgroundHandler", GetResultSetsEvent::ClassVersion(), GetResultSetsEvent::ClassId(), GetResultSetsEvent::ACCOUNT);
	m_dispatcher.RegisterHandler(ServerEngine::CanSpawnToZoneBackgroundHandler, (ULONG)this, "ServerEngine::DynObjBackgroundEvent", CanSpawnToZoneBackgroundEvent::ClassVersion(), CanSpawnToZoneBackgroundEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::LogoffUserBackgroundHandler, (ULONG)this, "ServerEngine::LogoffUserBackgroundEvent", LogoffUserBackgroundEvent::ClassVersion(), LogoffUserBackgroundEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::BroadcastEventBackgroundHandler, (ULONG)this, "ServerEngine::BroadcastEventBackgroundEvent", BroadcastEventBackgroundEvent::ClassVersion(), BroadcastEventBackgroundEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::RefreshPlayerBackgroundHandler, (ULONG)this, "ServerEngine::RefreshPlayerBackgroundHandler", RefreshPlayerBackgroundEvent::ClassVersion(), RefreshPlayerBackgroundEvent::ClassId());
	m_dispatcher.RegisterFilteredHandler(ServerEngine::ScriptServerEventRayIntersectsHandler, (ULONG)this, "ServerEngine::ScriptServerEventRayIntersectsHandler", ScriptServerEvent::ClassVersion(), ScriptServerEvent::ClassId(), ScriptServerEvent::PlayerRayIntersects, false, 0, EP_HIGH); // Must use highest priority
	m_dispatcher.RegisterHandler(ServerEngine::ScriptClientEventHandler, (ULONG)this, "ServerEngine::ScriptClientEventHandler", ScriptClientEvent::ClassVersion(), ScriptClientEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::ZoneDownloadCompleteEventHandler, (ULONG)this, "ZoneCustomizationDLCompleteEvent", PackVersion(1, 0, 0, 0), m_dispatcher.GetEventId("ZoneCustomizationDLCompleteEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::AvatarSelectionEventHandler, (ULONG)this, "ServerEngine::AvatarSelectionEventHandler", AvatarSelectionEvent::ClassVersion(), AvatarSelectionEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::SpaceImportHandler, (ULONG)this, "ServerEngine::SpaceImportHandler", SpaceImportEvent::ClassVersion(), SpaceImportEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::SpaceImportBackgroundHandler, (ULONG)this, "ServerEngine::SpaceImportBackgroundHandler", SpaceImportBackgroundEvent::ClassVersion(), SpaceImportBackgroundEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::ChannelImportHandler, (ULONG)this, "ServerEngine::ChannelImportHandler", ChannelImportEvent::ClassVersion(), ChannelImportEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::ChannelImportBackgroundHandler, (ULONG)this, "ServerEngine::ChannelImportBackgroundHandler", ChannelImportBackgroundEvent::ClassVersion(), ChannelImportBackgroundEvent::ClassId(), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::PlaceDynamicObjectAtPositionEventHandler, (ULONG)this, "PlaceDynamicObjectAtPositionEvent", PlaceDynamicObjectAtPositionEvent::ClassVersion(), PlaceDynamicObjectAtPositionEvent::ClassId(), EP_NORMAL);

	//Ankit ----- registering handlers for sound customization events
	m_dispatcher.RegisterHandler(ServerEngine::SoundRequestEventHandler, (ULONG)this, "SoundCustomizationRequestEvent", GenericEvent::ClassVersion(), m_dispatcher.GetEventId("SoundCustomizationRequestEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::SoundUpdateEventHandler, (ULONG)this, "UpdateSoundCustomizationEvent", GenericEvent::ClassVersion(), m_dispatcher.GetEventId("UpdateSoundCustomizationEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::SoundRemoveEventHandler, (ULONG)this, "RemoveSoundCustomizationEvent", GenericEvent::ClassVersion(), m_dispatcher.GetEventId("RemoveSoundCustomizationEvent"), EP_NORMAL);
	//Ankit ----- registering handlers for player title events
	m_dispatcher.RegisterHandler(ServerEngine::PlayerTitleEventHandler, (ULONG)this, "PlayerTitleEvent", GenericEvent::ClassVersion(), m_dispatcher.GetEventId("PlayerTitleEvent"), EP_NORMAL);
	m_dispatcher.RegisterHandler(ServerEngine::PlayerChangeTitleEventHandler, (ULONG)this, "PlayerChangeTitleEvent", GenericEvent::ClassVersion(), m_dispatcher.GetEventId("PlayerChangeTitleEvent"), EP_NORMAL);

	m_dispatcher.RegisterHandler(ServerEngine::ZoneImportedEventHandler, (ULONG)this, "ZoneImportedEvent", ZoneImportedEvent::ClassVersion(), ZoneImportedEvent::ClassId(), EP_NORMAL);

	// queue 1 sec in future
	m_dispatcher.QueueEventInFutureMs(m_dispatcher.MakeEvent(pingerEventId), 1 * MS_PER_SEC);

	if (m_dispatcher.HasHandler(UseSkillEvent::ClassId()))
		m_hasUseSkillHandler = true;

#ifdef _DEBUG
	// just a little testing attached to an arbitrary command
	EVENT_ID id = m_dispatcher.RegisterEvent("TestEvent", PackVersion(1, 0, 0, 0), GenericEvent::CreateEvent, 0, EP_NORMAL);
	m_dispatcher.RegisterHandler(testFn, 0, "TestEvent", PackVersion(1, 0, 0, 0), id);
#endif

	return true;
}

int ServerEngine::GetGameId() const {
	int gameId = Engine::GetGameId();
	if (gameId == 0) {
		char szGameID[100];
		size_t len = 0;
		memset(szGameID, 0, sizeof(szGameID));
		getenv_s(&len, szGameID, "GAME_ID");
		if (szGameID[0])
			gameId = atoi(szGameID);
	}
	return gameId;
}

// DRF - This loop governs the max rate of player position updates
// and dispatcher and message event latency.  It is not on the main
// thread and ran at about 10hz before updating the following timeouts
// and about 100hz after.  This loop needs to happen frequently for
// good ui player update latency.
static const TimeMs processEventsUntilTimeoutMs = 10.0;
static const TimeMs waitForNextPacketTimeoutMs = 10.0;
bool ServerEngine::RenderLoop(bool canWait) {
	// Mark Says We Need This
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// DRF - Added
	static Timer timer;
	static int hz = 0;
	++hz;
	auto elapsedSec = timer.ElapsedSec();
	if (elapsedSec > 60.0) {
		timer.Reset();
		LogInfo("hz=" << (hz / elapsedSec));
		hz = 0;
	}

	ServerEngine::Callback();

	// Process Dispatcher Events Until Empty Or Timeout
	m_dispatcher.ProcessEventsUntil(processEventsUntilTimeoutMs);

	// Wait For Next Packet Until Arrives Or Timeout
	if (canWait)
		WaitForNextPacket((UINT)waitForNextPacketTimeoutMs);

	// Send Heartbeat To AppController (once every 10 seconds)
	SendHeartbeatToAppController();

	//Check if we need to invalidate pinger
	CheckForPingerRefresh();

	return true;
}

ServerEngine::~ServerEngine(void) {
	if (_isMyMetricsAgent)
		delete _metricsAgent;
	DeleteEdbNothingLists();
	if (m_hEventPingerRefresh)
		CloseHandle(m_hEventPingerRefresh);
}

/// \brief validate an animation can be used by the user in the space
///
/// \param playerObjectData the player changing animations
/// \param curAnim the primary animation switching to
/// \param secondaryAnim primary animation switching to
/// \return true if ok to use animation in zone, or not switching
bool ServerEngine::ValidateAnimation(CPlayerObject* pPO, GLID animGlid, GLID animGlid2, bool validateGlid) {
	bool ret = true;

	PassList* zonePassList = 0;

	if (animGlid != pPO->m_animGLID) {
		if (validateGlid && (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0) && IS_UGC_GLID(animGlid)) {
			CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(animGlid);
			if (!pGIO || pGIO->m_itemData->m_useType != USE_TYPE_ANIMATION) {
				LogError("Animation Not Found In Inventory - glid<" << animGlid << ">");
				return false;
			}
		}

		auto pPL = GetAnimationPassList(animGlid);
		if (pPL) {
			zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
			ret = PassList::canUseItemInZone(pPL, zonePassList);
			if (ret)
				ret = PassList::canPlayerUseItem(pPO->m_passList, pPL);
		}
	}

	if (ret && animGlid2 != pPO->m_animGLID2nd) {
		if (validateGlid && (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0) && IS_UGC_GLID(animGlid2)) {
			CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(animGlid2);
			if (!pGIO || pGIO->m_itemData->m_useType != USE_TYPE_ANIMATION) {
				LogError("Animation Not Found In Inventory - glid<" << animGlid2 << ">");
				return false;
			}
		}

		PassList* pPL = GetAnimationPassList(animGlid2);
		if (pPL) {
			if (zonePassList == 0)
				zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
			ret = PassList::canUseItemInZone(pPL, zonePassList);
			if (ret)
				ret = PassList::canPlayerUseItem(pPO->m_passList, pPL);
		}
	}

	return ret;
}

bool ServerEngine::DisconnectPlayer(NETID netId, const char* optionalLogMsg /*= 0*/) {
	auto pPO = GetPlayerObjectByNetId(netId, true);
	if (pPO)
		DisconnectPlayerAndLog(pPO, NULL_CK(optionalLogMsg));
	return (pPO != NULL);
}

void ServerEngine::DisconnectPlayerAndLog(CPlayerObject* pPO, CStringA errorMsg) {
	if (!pPO)
		return;
	LogError(errorMsg << " - " << pPO->ToStr() << " Disconnecting...");
	m_multiplayerObj->DisconnectClient(pPO->m_netId, DISC_FORCE);
}

BOOL ServerEngine::GetFirstCollisionY(int cId, Vector3f currentPosition, float yDist, Vector3f* returnPosition) {
	if (cId < 0)
		return FALSE;

	if (!m_collisionDatabase)
		return FALSE;

	POSITION posLoc = m_collisionDatabase->FindIndex(cId);
	if (!posLoc)
		return FALSE;

	auto pCO = (CCollisionObj*)m_collisionDatabase->GetAt(posLoc);

	currentPosition.y += 10.0f;

	Vector3f targetBuild;
	targetBuild.x = currentPosition.x;
	targetBuild.y = currentPosition.y - yDist;
	targetBuild.z = currentPosition.z;

	float highestContactLocation = 0;
	Vector3f collisionIntersect, normal;
	BOOL result = FALSE;
	Vector3f adjustVector;
	int objectHit = 0;
	result = pCO->CollisionSegmentIntersectAdvanced(
		currentPosition,
		targetBuild,
		1.0f, 0.0f, 1.0f,
		&collisionIntersect,
		&normal,
		&objectHit,
		&highestContactLocation,
		&adjustVector);
	if (result == TRUE) {
		returnPosition->x = collisionIntersect.x;
		returnPosition->y = highestContactLocation;
		returnPosition->z = collisionIntersect.z;
		return TRUE;
	}

	return FALSE;
}

void ServerEngine::RespawnToRebirthPoint(CPlayerObject* pPO) {
	if (!pPO || pPO->m_aiControlled)
		return;

	auto pAO = pPO->m_pAccountObject;
	if (!pAO)
		return;

	auto pPO_acct = pAO->GetCurrentPlayerObject();
	if (!pPO_acct)
		return;

	initiateSpawn(pPO, pAO, pPO_acct->m_respawnZoneIndex, pPO_acct->m_respawnPosition);
}

BOOL ServerEngine::EnableServer() {
	if (m_multiplayerObj->m_serverStarted == TRUE) {
		LogError("Server Already Started");
		return FALSE;
	}

	try {
		if (!m_multiplayerObj->EnableServer(m_key.c_str(), eEngineType::Server, InstanceName()))
			return FALSE;
		m_multiplayerObj->m_serverNetwork->pinger().UpdatePinger(ssStarting, 0, m_adminOnly);
		return TRUE;
	} catch (KEPException* e) {
		LogError(e->ToString());
		e->Delete();
	} catch (const ChainedException& e) {
		LogError(e.str());
	}
	return FALSE;
}

CSpawnCfgObj* ServerEngine::GetSpawnPtr(int spawnCfgIndex) {
	if (m_multiplayerObj->m_spawnPointCfgs == NULL || m_multiplayerObj->m_currentWorldsUsed == NULL) {
		LogError("Error getting spawn pointer. Server has no valid preset spawn/world Cfgs");
		return NULL;
	}

	if (spawnCfgIndex < 0)
		return NULL;

	CSpawnCfgObj* spwnPtr = m_multiplayerObj->m_spawnPointCfgs->FindId(spawnCfgIndex);
	if (!spwnPtr) {
		LogError("Invalid spawn config index " << spawnCfgIndex);
		return NULL;
	}

	return spwnPtr;
}

BOOL ServerEngine::AttribGetMovementObjectCfg(ATTRIB_DATA* pAttribData, NETID idFromLoc) {
	if (!pAttribData)
		return FALSE;

	// Decode Attribute Data
	auto netTraceId = pAttribData->aeShort1;
	auto isItem = (int)pAttribData->aeShort2; // client never sets but read anyway (always 0) ??!!
	// auto netTraceId = pAttribData->aeInt2; // client sets but never read (same as aeShort1) ??!!

	// Get Player Requesting Entity Config
	auto pPO = GetPlayerObjectByNetId(idFromLoc, false);
	if (pPO && pPO->m_aiControlled)
		pPO = NULL;

	// Get Player Being Configured
	auto pPO_ec = GetPlayerObjectByNetTraceId(netTraceId);
	if (!pPO_ec || !pPO_ec->m_inGame)
		return FALSE;

	// Compile PassList Into Bitmapped PASS_GROUP (1=AP, 2=VIP)
	int passGroups = PASS_GROUP_INVALID;
	if (pPO_ec->m_passList) {
		passGroups = PASS_GROUP_NONE;
		std::vector<int> passes = pPO_ec->m_passList->m_ids;
		for (size_t i = 0; i < passes.size(); ++i)
			passGroups |= passes[i];
	}

	// Get Entity Interpolated Move Data
	auto moveData = pPO_ec->InterpolatedMoveData();

	// Create Entity Config Event
	IEvent* pEvent = new MovementObjectCfgEvent(
		isItem,
		pPO_ec->m_netTraceId,
		pPO_ec->m_dbCfg,
		pPO_ec->m_aiCfgOfMounted,
		passGroups,
		pPO_ec->m_nameColor.r, pPO_ec->m_nameColor.g, pPO_ec->m_nameColor.b,
		pPO_ec->m_scratchScaleFactor,
		moveData.pos,
		moveData.dir,
		moveData.up,
		moveData.animGlid1,
		moveData.animGlid2,
		pPO_ec->m_itemAnimations);
	pEvent->AddTo(idFromLoc);

	// Add Physics Parameters To Self ?
	BOOL addPhysics = FALSE;
	signed char clanMember = 0;
	bool isSelf = pPO && (pPO_ec->m_handle == pPO->m_handle);
	if (isSelf) {
		pPO = NULL;
		clanMember = 1;
		addPhysics = TRUE;
	}

	bool isClever = pPO && (pPO_ec->m_clanHandle != NO_CLAN || pPO_ec->m_accountNumberRef == pPO->m_accountNumberRef) && (pPO_ec->m_clanHandle == pPO->m_clanHandle || pPO_ec->m_accountNumberRef == pPO->m_accountNumberRef);
	if (isClever)
		clanMember = 1;

	(*pEvent->OutBuffer()) << clanMember;
	(*pEvent->OutBuffer()) << pPO_ec->m_charName;
	(*pEvent->OutBuffer()) << pPO_ec->m_displayName;
	(*pEvent->OutBuffer()) << pPO_ec->m_lastName;
	(*pEvent->OutBuffer()) << pPO_ec->m_clanName;
	(*pEvent->OutBuffer()) << pPO_ec->m_title;

	pPO_ec->m_charConfig.insertIntoEvent(pEvent);

	// Encode Player Physics & Armed Glids
	size_t glidsArmed = 0;
	CInventoryObjList* pIOL = pPO_ec->m_scratchInventory;
	if (pIOL) {
		// Count Armed Items
		for (POSITION invenPos = pIOL->GetHeadPosition(); invenPos != NULL;) {
			auto pIO = (CInventoryObj*)pIOL->GetNext(invenPos);
			if (pIO->isArmed())
				glidsArmed++;
		}

		// Add Physics maxSpeed Parameter?
		if (addPhysics)
			glidsArmed++;
	}

	(*pEvent->OutBuffer()) << glidsArmed;

	if (glidsArmed) {
		// Add Glids Armed
		for (POSITION invenPos = pIOL->GetHeadPosition(); invenPos != NULL;) {
			auto pIO = (CInventoryObj*)pIOL->GetNext(invenPos);
			if (pIO->isArmed()) {
				int glidSpecial = (int)pIO->m_itemData->m_globalID;
				(*pEvent->OutBuffer()) << glidSpecial;
			}
		}

		// Add Physics maxSpeed Parameter ?
		if (addPhysics) {
			CMovementObj* pMORM = m_movObjRefModelList->getObjByIndex(pPO_ec->m_dbCfg);
			CMovementCaps* pMC = pMORM ? pMORM->getMovementCaps() : nullptr;
			const float maxSpeedDefault = 0.13f; // default max speed
			int glidSpecial = -(int)(1000.0f * (pMC ? pMC->GetMaxSpeed() : maxSpeedDefault));
			(*pEvent->OutBuffer()) << glidSpecial;
		}
	}

	// Send Event
	pEvent->SetFlags(EF_SENDIMMEDIATE);
	m_dispatcher.QueueEvent(pEvent);

	// Clear Player's Move Data Map For Full Fresh Movement Update
	if (pPO)
		pPO->MoveDataToClientMapClear();

	return TRUE;
}

bool ServerEngine::CompletedAvatarSelect(CPlayerObject* pPO) {
	return (m_gameStateDb && pPO) ? m_gameStateDb->IsAvatarTemplateApplied(pPO) : false;
}

bool ServerEngine::IsPlayerInAvatarSelectZone(CPlayerObject* pPO) {
	return pPO ? (pPO->m_currentZoneIndex.zoneIndex() == CC_ZONE_INDEX_PLAIN) : false;
}

void ServerEngine::FreeAll() {
	log4cplus::Logger logger(Logger::getInstance(L"SHUTDOWN"));
	LOG4CPLUS_TRACE(logger, "ServerEngine::FreeAll()");
	LogCounts(m_logger, "at start of FreeAll");
	m_threadPool.shutdown();

	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (m_multiplayerObj && m_multiplayerObj->m_serverNetwork) // if this is a disabled engine, will be null
		m_multiplayerObj->m_serverNetwork->pinger().UpdatePinger(ssStopping, 0, m_adminOnly);

	m_monitor.Terminate();

	m_dispatcher.StopProcessingEvents();

	for (const auto& apl : m_animPassLists)
		delete apl.second;

	if (m_gameStateDispatcher) {
		LOG4CPLUS_TRACE(logger, "GameStateDispatcher::~GameStateDispatcher(): Signal stopdispatcher");
		m_gameStateDispatcher->Shutdown();
		DELETE_AND_ZERO(m_gameStateDispatcher);
	}

	if (m_gameStateDb)
		DELETE_AND_ZERO(m_gameStateDb);

	if (m_bankZoneList) {
		m_bankZoneList->SafeDelete();
		delete m_bankZoneList;
		m_bankZoneList = 0;
	}

	if (m_secureTradeDB) {
		m_secureTradeDB->SafeDelete();
		delete m_secureTradeDB;
		m_secureTradeDB = 0;
	}

#if (DRF_OLD_VENDORS == 1)
	if (m_serverItemGenerator) {
		m_serverItemGenerator->SafeDelete();
		delete m_serverItemGenerator;
		m_serverItemGenerator = 0;
	}
#endif

	if (m_commerceDB) {
		m_commerceDB->SafeDelete();
		delete m_commerceDB;
		m_commerceDB = 0;
	}

#if (DRF_OLD_PORTALS == 1)
	if (m_portalDatabase) {
		m_portalDatabase->SafeDelete();
		delete m_portalDatabase;
		m_portalDatabase = 0;
	}
#endif

	if (m_collisionDatabase) {
		m_collisionDatabase->SafeDelete();
		delete m_collisionDatabase;
		m_collisionDatabase = 0;
	}

	if (m_gmPageListRuntime) {
		m_gmPageListRuntime->SafeDelete();
		delete m_gmPageListRuntime;
		m_gmPageListRuntime = 0;
	}

	if (m_worldChannelRuleDB) {
		m_worldChannelRuleDB->SafeDelete();
		delete m_worldChannelRuleDB;
		m_worldChannelRuleDB = 0;
	}

	if (m_battleSchedulerSys) {
		m_battleSchedulerSys->SafeDelete();
		delete m_battleSchedulerSys;
		m_battleSchedulerSys = 0;
	}
	CBattleSchedulerList::FreeData();

	// can't do too late since
	if (m_multiplayerObj && m_multiplayerObj->m_serverNetwork)
		m_multiplayerObj->m_serverNetwork->pinger().UpdatePinger(ssStopped, 0, m_adminOnly);

	Engine::FreeAll();

	CFileFactory::DeInitialize();
}

bool ServerEngine::ProcessScriptAttribute(CEXScriptObj* sctPtr) {
	bool ret = true;

	switch (sctPtr->m_actionAttribute) {
		case LOAD_STATIC_TEXTURE_BANK:
		case LOAD_DEFAULT_CAMERAS:
		case LOAD_COMM_CONTROLS:
		case LOAD_CONTROLS_DATABASE:
		case LOAD_DYNAMIC_OBJECTS:
		case LOAD_ICON_DB:
		case LOAD_SOUNDTRACK:
		case LOAD_SOUND_DATABASE:
		case LOAD_TEXT_FONT_COLLECTIONS_DATABASE:
		case LOAD_PARTICLE_SYSTEMS:
		case LOAD_SAFE_ZONES:
			// DEPRECATED
			break;

		case LOAD_ENTITY_DATABASE:
			::ServerSerialize = true;
			LoadEntityDatabase((sctPtr->m_miscString + ".Server").GetString());
			::ServerSerialize = false;
			break;

		case LOAD_COMMERCE_DATABASE:
			LoadGlobalCommerceData(sctPtr->m_miscString.GetString());
			break;

		case LOAD_BANK_ZONES:
			LoadBankZonesData(sctPtr->m_miscString.GetString());
			break;

		case LOAD_ITEM_GENERATOR:
#if (DRF_OLD_VENDORS == 1)
			LoadItemGeneratedData(sctPtr->m_miscString.GetString());
#endif
			break;

		case LOAD_PORTAL_DATABASE:
#if (DRF_OLD_PORTALS == 1)
			LoadPortalDatabase(sctPtr->m_miscString.GetString());
#endif
			break;

		case LOAD_ELEMENT_DATABASE:
			LoadElementDatabase(sctPtr->m_miscString.GetString());
			break;

		case LOAD_STATS_DATABASE:
			LoadStatsDatabase(sctPtr->m_miscString.GetString());
			break;

		case LOAD_ARENA_SYS:
			LoadArenaSys(sctPtr->m_miscString.GetString());
			break;

		case LOAD_WORLD_RULES:
			LoadWorldRules(sctPtr->m_miscString.GetString());
			break;

		case LOAD_HOUSE_ZONES:
			m_delayLoadHousingZone = sctPtr->m_miscString;
			break;

		case LOAD_NOTHING_PASSLISTS:
			LoadEdbNothingLists(sctPtr->m_miscString.GetString());
			break;

		case LOAD_MISSLE_DATABASE:
			::ServerSerialize = true;
			LoadMissileDatabase((sctPtr->m_miscString + ".Server").GetString());
			::ServerSerialize = false;
			break;

		case LOAD_EXPLOSION_DATABASE:
			::ServerSerialize = true;
			LoadExplosionFile((sctPtr->m_miscString + ".Server").GetString());
			::ServerSerialize = false;
			break;

		default:
			ret = Engine::ProcessScriptAttribute(sctPtr);
	}

	return ret;
}

void ServerEngine::Callback() {
	if (!m_multiplayerObj || !m_multiplayerObj->m_serverStarted)
		return;

	CAccountObjectList* pAOL_player = m_multiplayerObj->m_runtimeAccountDB;
	CAccountObjectList* pAOL_ai = m_multiplayerObj->m_connectedAIservers;

	// Update Pinger Every Minute
	if (pAOL_player && (m_forcePing || (m_timerPing.ElapsedSec() > 60.0))) {
		m_timerPing.Reset();
		m_forcePing = false;

		LogInfo("UpdatePinger(state=" << (int)ssRunning << " populationPeakPing=" << m_populationPeakPing << ")");
		m_multiplayerObj->m_serverNetwork->pinger().UpdatePinger(ssRunning, m_populationPeakPing, m_adminOnly);

		// Clean up any failed logons, remove any that are > 10 sec old
		POSITION pos, delPos;
		for (pos = pAOL_player->GetHeadPosition(); (delPos = pos) != NULL;) {
			auto pAO = (CAccountObject*)pAOL_player->GetNext(pos);
			if (!pAO)
				continue;

			if ((pAO->m_loggedOn == FALSE) && (pAO->m_logonTimer.ElapsedMs() > MAX_LOGON_TIME_MS)) {
				LogError("MAX LOGON TIME - netId=" << pAO->m_netId << " Disconnecting...");
				m_multiplayerObj->DisconnectClient(pAO->m_netId, DISC_TIMEOUT);
				pAOL_player->RemoveAt(delPos);
				if (pAO->m_serverId != 0)
					m_multiplayerObj->m_accountDatabase->SafeDeleteAccount(pAO);
			}
		}

#if 0 // DRF - TODO - Testing Only!
		if (FileHelper::Exists("\\crash.txt")) {
			LogFatal("===== FORCED CRASH =====");
			volatile int* ptr = NULL;
			*ptr = 3; // seg fault
		}
#endif

		try {
			_metricsAgent->reportMetric(
				"server_population",
				MetricsAgent::CounterKey(StringHelpers::parseNumber<unsigned long>(m_multiplayerObj->m_serverNetwork->pinger().GetServerId()),
					StringHelpers::parseNumber<unsigned long>(m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId()),
					StringHelpers::parseNumber<unsigned long>(m_multiplayerObj->m_serverNetwork->pinger().GetGameId())),
				MetricsAgent::CounterData(m_populationPeakPing));
		} catch (const MetricsException& me) {
			LogError("Failed to report metric [" << me.str().c_str() << "]");
		}

		m_populationPeakPing = 0;
	}

	// Update Population (Players minus AIServers)
	ULONG population = (pAOL_player ? pAOL_player->GetCount() : 0);
	ULONG populationAI = (pAOL_ai ? pAOL_ai->GetCount() : 0);
	if (populationAI > population) {
		if (m_population) {
			LogError("Invalid Population - population=" << population << " populationAI=" << populationAI);
		}
		population = 0;
		m_population = 0;
		m_populationPeakPerf = 0;
		m_populationPeakPing = 0;
	} else {
		// Subtract AI Servers From Population
		population -= populationAI;
		if (m_population != population) {
			m_population = population;
			LogWarn("Population Changed - population=" << population);
		}

		// Update Population Peak For Performance Metrics Update
		m_populationPeakPerf = MAX(population, m_populationPeakPerf);

		// Update Population Peak For Pinger Update
		m_populationPeakPing = MAX(population, m_populationPeakPing);
	}

	// Handle All Messages Coming From Clients
	int packetsProcessed = 0;
	BYTE* pData;
	NETID netIdFrom;
	size_t dataSize;
	bool bDisposeMessage;
	Timer timer;
	while (m_multiplayerObj->MsgRx(pData, dataSize, netIdFrom)) {
		EnterCriticalSection(&m_multiplayerObj->m_criticalSection);
		try {
			HandleMsgs(pData, netIdFrom, dataSize, bDisposeMessage);
		} catch (CException* e) {
			wchar_t szErrMsg[1024];
			e->GetErrorMessage(szErrMsg, sizeof(szErrMsg) / sizeof(szErrMsg[0]));
			LogError("Caught CException - " << Utf16ToUtf8(szErrMsg));
			e->Delete();
		} catch (std::exception* e) {
			LogError("Caught STL exception - " << e->what());
			delete e;
		} catch (std::exception& e) {
			LogError("Caught STL exception - " << e.what());
		} catch (...) {
			LogError("Caught Exception");
		}
		LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);

		if (bDisposeMessage)
			delete pData;

		packetsProcessed++;

		TimeMs timeMs = timer.ElapsedMs();
		if (timeMs > 10.0) {
			LogWarn("HandleMsgs() took " << FMT_TIME << timeMs << "ms to process " << packetsProcessed << " messages");
			break;
		}
	}

	EnterCriticalSection(&m_multiplayerObj->m_criticalSection);

	// Register All Player's Current Interpolated Locations
	if (m_multiplayerObj->m_runtimePlayersOnServer) {
		for (POSITION pos = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); pos;) {
			auto pPO = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(pos);
			if (!pPO)
				continue;

			// Send Message Updates To Player (don't force)
			SendMsgUpdateToClient(pPO, false);

			// Update Player Location Registry With Interpolated Position
			MovementData md = pPO->InterpolatedMovementData();
			LocationRegistry::singleton().registerPlayerLocation(
				pPO->m_netId,
				pPO->m_currentZoneIndex,
				std::pair<Vector3f, Vector3f>(md.pos, md.dir));
		}
	}

#if (DRF_OLD_VENDORS == 1)
	// Generate Static Movement Objects
	if (m_multiplayerObj->m_pServerItemObjMap) {
		// Add New Server Items (static mo's - city vendors, etc.) From Server Item Generator
		for (POSITION pos = m_serverItemGenerator->GetHeadPosition(); pos;) {
			auto pSIGO = (CServerItemGenObj*)m_serverItemGenerator->GetNext(pos);
			if (!pSIGO || pSIGO->m_existsOnServer)
				continue;
			CServerItemObj* pSIO = NULL;
			pSIGO->m_pSIO->Clone(&pSIO);
			pSIO->m_netTraceId = m_multiplayerObj->GetNextFreeNetTraceId();
			pSIO->m_spawnID = pSIGO->m_spawnGenID;
			LogInfo("ServerItemGenerator::AddItem() -> " << pSIO->ToStr());
			m_multiplayerObj->m_pServerItemObjMap->AddItem(pSIO);
			pSIGO->m_existsOnServer = TRUE;
		}
	}
#endif

	// Handle Timeouts On Players
	m_multiplayerObj->ServerTimeoutCallback();

	// Process and send PlayerDeparted events for expired pending re-spawns
	CheckForPendingRespawnExpiry();

	LeaveCriticalSection(&m_multiplayerObj->m_criticalSection);
}

BOOL ServerEngine::InitGameStates(const char* ip, int port) {
	if (m_multiplayerObj) {
		m_multiplayerObj->SafeDelete();
		delete m_multiplayerObj;
		m_multiplayerObj = 0;
	}

	if (SerializeFromFile(PathAdd(PathGameFiles(), m_gameStateFileName), m_multiplayerObj, m_logger)) {
		m_multiplayerObj->SetDispatcher(&m_dispatcher);
		m_multiplayerObj->SetGameStateDispatcher(m_gameStateDispatcher);
		m_multiplayerObj->SetPathBase(PathBase());
		m_multiplayerObj->m_accountDatabase->ExpandFromOptimized(m_skillDatabase, m_globalInventoryDB);

		std::string fname;
		try {
			fname = PathAdd(PathGameFiles(), SUPPORTED_WORLDS_CONFIG_FILE);
			m_multiplayerObj->m_currentWorldsUsed->SerializeFromXML(fname.c_str());
			fname = PathAdd(PathGameFiles(), SPAWN_POINTS_CONFIG_FILE);
			m_multiplayerObj->m_spawnPointCfgs->SerializeFromXML(fname.c_str());
			fname = PathAdd(PathGameFiles(), NETWORK_DEFINITION_ZONE_DB_FILE);
			m_multiplayerObj->SerializeFromXML(fname.c_str());
		} catch (KEPException* e) {
			LogError(loadStrPrintf(IDS_ERROR_LOADING_XML, fname.c_str(), e->m_msg.c_str()));
			e->Delete();
			return FALSE;
		}

		// do we have a housing zone that needs to be merged?
		if (!m_delayLoadHousingZone.empty()) {
			throw new std::exception();
			/*
			CHousingZoneDB* newHousingZoneDB = 0;
			if (SerializeFromFile(PathAdd(PathGameFiles(), m_delayLoadHousingZone), newHousingZoneDB, m_logger) &&
				newHousingZoneDB) {
				newHousingZoneDB->MergePlacementsFrom(m_multiplayerObj->m_housingDB, m_housingDB);

				// for placements left, we need to clean them up since they aren't in the new housing zone
				for (POSITION posLoc = m_multiplayerObj->m_housingDB->GetHeadPosition(); posLoc != NULL;) {
					CHousingZone* spnPtr = (CHousingZone*) m_multiplayerObj->m_housingDB->GetNext(posLoc);
					if (spnPtr->m_houseDatabase != 0) {
						for (POSITION placementLoc = spnPtr->m_houseDatabase->GetHeadPosition(); placementLoc != NULL; ) {
							CHPlacementObj* placement = (CHPlacementObj*) spnPtr->m_houseDatabase->GetNext(placementLoc);
							m_multiplayerObj->m_housingDB->RemoveHouseByPlayerHandle(placement->m_playerHandle);
						}
					}
				}
				// free old one
				m_multiplayerObj->m_housingDB->SafeDelete();
				delete m_multiplayerObj->m_housingDB;

				// add new one
				m_multiplayerObj->m_housingDB = newHousingZoneDB;
			}
			*/
		}
	}

	if (m_logger.isEnabledFor(TRACE_LOG_LEVEL)) {
		CStringA s;
		s.Format("Server mp is: %x", m_multiplayerObj);
		LogTrace(s);
	}

	if (ip)
		m_multiplayerObj->setIPAddress(ip);
	if (port > 0)
		m_multiplayerObj->m_port = port;

	// tell event timing logger about proper timing value
	LoggingTimer::SetTimeLimits((ULONG)m_multiplayerObj->GetRequestProcessingErrorTimeMS(),
		(ULONG)m_multiplayerObj->GetRequestProcessingInfoTimeMS());

	return TRUE;
}

// Called this to update a client's health data
BOOL ServerEngine::SendHealthUpdateToClient(NETID idFromLoc) {
	auto pPO = GetPlayerObjectByNetId(idFromLoc, false);
	if (!pPO)
		return FALSE;

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::PlayerHealth;
	attribData.aeShort1 = pPO->m_netTraceId;
	return m_multiplayerObj->QueueMsgAttribToClient(pPO->m_netId, attribData, pPO) ? TRUE : FALSE;
}

void ServerEngine::BroadcastMessage(short radius, const char* message, double x, double y, double z, LONG channel, NETID fromNetId, eChatType chatType, bool allInstances /*= false */) {
	Vector3f v;
	v.x = (float)x;
	v.y = (float)y;
	v.z = (float)z;
	m_multiplayerObj->BroadcastMessage(radius, message, v, channel, fromNetId, chatType, allInstances);
}

void ServerEngine::BroadcastMessageToDistribution(const char* message,
	NETID fromNetId,
	const char* toDistribution,
	const char* fromRecipient,
	eChatType chatType,
	bool fromRemote,
	ULONG filter) {
	if (chatType == eChatType::S2S || chatType == eChatType::Private || chatType == eChatType::System) {
		std::vector<PLAYER_HANDLE> remotePlayerIds;

		m_multiplayerObj->BroadcastMessageToDistribution(message, fromNetId, toDistribution, fromRecipient, remotePlayerIds, chatType, fromRemote, filter);
		return;
	}
}

bool ServerEngine::SendTextMessage(IPlayer* to, const char* msg, eChatType chatType) {
	auto pPO = dynamic_cast<CPlayerObject*>(to);
	if (!pPO)
		return false;
	return (m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, msg, chatType) != FALSE);
}

IGetSet* ServerEngine::Values() {
	return m_multiplayerObj;
}

// DRF - WARN - TODO - This Should Be Different Event!
// Called this to update a client's inventory data
BOOL ServerEngine::SendInventoryUpdateToClient(NETID idFromLoc) {
	auto pPO = GetPlayerObjectByNetId(idFromLoc, false);
	if (!pPO)
		return FALSE;
	ATTRIB_DATA attribData;
	attribData.aeShort1 = pPO->m_netTraceId;
	AttribGetMovementObjectCfg(&attribData, idFromLoc);
	return TRUE;
}

void ServerEngine::SpawnToPoint(IPlayer* p, double x, double y, double z,
	LONG zoneIndex, EDestinationType destType, int rotation,
	const char* url) {
	CPlayerObject* pPtr = dynamic_cast<CPlayerObject*>(p);
	if (pPtr) {
		Vector3f v;
		v.x = (float)x;
		v.y = (float)y;
		v.z = (float)z;
		if (destType == DT_Arena) {
			auto pAO = pPtr->m_pAccountObject;
			if (pAO)
				pAO->m_arenaLiscense = 1;
			url = 0; // don't track URLs for arenas
		}

		SpawnToPointInWld(pPtr, v, zoneIndex, destType, rotation, url);
	} else
		jsAssert(false);
}

/// spawn a player to another server, logging them off here and switching
/// them to the other server via the event passed in
void ServerEngine::spawnToOtherServer(CAccountObject* pAO, IEvent* switchEvent) {
	if (!pAO)
		return;

	// switching servers, fire event back to client with server and port
	// knock out of db first to avoid race of client spawning
	CPlayerObject* pPO = nullptr;
	if (m_gameStateDb != 0) {
		pPO = GetPlayerObjectByNetId(pAO->m_netId, false);
		if (pPO && pPO->playerOrigRef())
			pPO->playerOrigRef()->m_reconnecting = true; // suppress disconnect msg
	}
	m_multiplayerObj->LogoutCleanup(pAO, DISC_PLAYER_ZONED, pPO, 0, 0, switchEvent);
}

/// helper to fill out spawn point info for gotoplace
int ServerEngine::getSpawnPointConfig(GotoPlaceBackgroundEvent* gpe) {
	int ret = ZONE_NOT_FOUND;

	if (gpe->spawnPtIndex >= DEFAULT_SPAWN_POINT) {
		// should be local, if get here
#ifndef REFACTOR_INSTANCEID
		CSpawnCfgObj* spawn = m_multiplayerObj->GetSpawnPointCfg(gpe->zoneIndex, gpe->spawnPtIndex);
#else
		CSpawnCfgObj* spawn = m_multiplayerObj->GetSpawnPointCfg(gpe->zoneIndex.toLong(), gpe->spawnPtIndex);
#endif
		if (spawn) {
			gpe->x = spawn->m_position.x;
			gpe->y = spawn->m_position.y;
			gpe->z = spawn->m_position.z;
			gpe->rotation = spawn->m_rotation;
			ret = ZONE_FOUND;
		} else {
			LogWarn("Spawn Point Not Found - " << gpe->zoneIndex.ToStr());
		}
	} else if (gpe->spawnPtIndex == SPAWN_POINT_NOT_USED) {
		ret = ZONE_FOUND; // using x,y,x in event (no way to validate that data)
	} else {
		LogWarn("Spawn Point Not Found - " << gpe->zoneIndex.ToStr());
	}

	return ret;
}

/// can user do local spawn to url in event?
/// returns SPAWN_ERROR, SPAWN_REMOTE, SPAWN_LOCAL
int ServerEngine::isSwitchingServers(CAccountObject* acct, GotoPlaceBackgroundEvent* gpe,
	std::string& server, ULONG& port) {
	int ret = SPAWN_LOCAL;

	if (gpe->ret == ZONE_FOUND) {
		if (gpe->serverId == m_multiplayerObj->m_serverNetwork->pinger().GetServerId())
			gpe->serverId = 0; // staying here

		if (gpe->serverId != 0) {
			if (m_gameStateDb->MapServerId(gpe->serverId, server, port)) {
				ret = SPAWN_REMOTE; // switching
			} else {
				// MapServerId will log warn
				SendGotoError(ZONE_NOT_FOUND, m_dispatcher, gpe->From(), LS_DEST_NOT_FOUND, acct->m_currentcharInUse, gpe->url.c_str(), gpe->defaultUrl);
				ret = SPAWN_ERROR;
			}
		} else {
			gpe->ret = getSpawnPointConfig(gpe);
		}
	} else {
		SendGotoError(gpe->ret, m_dispatcher, gpe->From(), LS_SPAWN_NO_PERMISSION, gpe->charIndex, gpe->url.c_str(), gpe->defaultUrl);
		ret = SPAWN_ERROR;
	}

	return ret;
}

#if (DRF_OLD_VENDORS == 1)

void ServerEngine::DeSpawnStaticAIBySpawnId(int spawnID, LONG channelInstance) {
	m_multiplayerObj->m_pServerItemObjMap->DeleteBySpawnIDAndChannel(spawnID, ChannelId(channelInstance));
}

void ServerEngine::DeSpawnStaticAIByNetTraceId(int netTraceId) {
	m_multiplayerObj->m_pServerItemObjMap->DeleteByNetTraceId(netTraceId);
}

IGetSet* ServerEngine::SpawnStaticAI(CServerItemGenObj* pSIGO, double x, double y, double z, LONG channelInstance) {
	if (!pSIGO || !pSIGO->m_pSIO)
		return NULL;

	// Create New ServerItemObj -> Spawns Static MO (city vendors, etc.)
	CServerItemObj* pSIO = NULL;
	pSIGO->m_pSIO->Clone(&pSIO);
	pSIO->m_netTraceId = m_multiplayerObj->GetNextFreeNetTraceId();
	pSIO->m_spawnID = pSIGO->m_spawnGenID;
	if (x != -1 && y != -1 && z != -1) {
		pSIO->m_position.x = (float)x;
		pSIO->m_position.y = (float)y;
		pSIO->m_position.z = (float)z;
	}
	pSIO->m_channel = channelInstance;

	LogInfo("ServerItemGenerator::AddItem() -> " << pSIO->ToStr());
	m_multiplayerObj->m_pServerItemObjMap->AddItem(pSIO);

	return pSIO;
}

IGetSet* ServerEngine::SpawnStaticAIBySpawnId(int spawnID, double x, double y, double z, LONG channelInstance) {
	auto pSIGO = dynamic_cast<CServerItemGenObj*>(GetItemGenObjectBySpawnId(spawnID));
	return SpawnStaticAI(pSIGO, x, y, z, channelInstance);
}

#endif

BOOL ServerEngine::WaitForNextPacket(ULONG msToWait) {
	if (WR_OK == m_multiplayerObj->m_packetQue->semaphore().waitAndDecrement(msToWait))
		return TRUE;
	else
		return FALSE;
}

#if (DRF_OLD_VENDORS == 1)
IGetSet* ServerEngine::GetItemGenObjectBySpawnId(int spawnId) {
	for (POSITION pos = m_serverItemGenerator->GetHeadPosition(); pos != NULL;) {
		auto pSIGO = (CServerItemGenObj*)m_serverItemGenerator->GetNext(pos);
		if (pSIGO->m_spawnGenID == spawnId)
			return pSIGO;
	}
	return 0;
}
#endif

IGetSet* ServerEngine::GetItemObjectByNetTraceId(int netTraceId) {
	return m_multiplayerObj->m_pServerItemObjMap->GetObjectByNetTraceId(netTraceId);
}

IGetSet* ServerEngine::GetItemObjectBySpawnId(int spawnId) {
	return m_multiplayerObj->m_pServerItemObjMap->GetObjectBySpawnID(spawnId);
}

bool ServerEngine::TellClientToOpenMenu(LONG menuId, NETID playerNetId, BOOL closeAllOthers, int networkAssignIdOfTradee) {
	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::OpenMenu;
	attribData.aeShort2 = closeAllOthers ? ae_AllMenus : 0;
	attribData.aeInt1 = networkAssignIdOfTradee;
	attribData.aeInt2 = menuId;
	return m_multiplayerObj->QueueMsgAttribToClient(playerNetId, attribData);
}

IGetSet* ServerEngine::GetPlayersBestSkill(IPlayer* _player) {
	CSkillObject* ret = 0;
	CPlayerObject* player = dynamic_cast<CPlayerObject*>(_player);
	if (player)
		ret = player->m_skills->GetBestSkillWEval(m_skillDatabase);
	return ret;
}

BOOL ServerEngine::UseSkillOnPlayer(IPlayer* player, int skillId, int level, int ability) {
	return AttribSkillAbilityUse(dynamic_cast<CPlayerObject*>(player), skillId, level, ability);
}

BOOL ServerEngine::UseSkillActionOnPlayer(IPlayer* player, ULONG skillId, ULONG actionId, BOOL bUseAlternate, int& newLevel, std::string& levelMessage, std::string& gainMessage, int& percentToNextLevel, std::string& rewardsAtNextLevel, float& totalExperience) {
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO || !pPO->m_skills)
		return FALSE;

	bool added = false;
	CSkillObject* skillObj = nullptr;
	CSkillObject* skillPtrRef = nullptr;
	if (!pPO->m_skills->CheckSkill(skillId, m_skillDatabase, &skillObj, &skillPtrRef, added))
		return FALSE;

	if (!skillObj || !skillPtrRef)
		return FALSE;

	int percentBefore = skillObj->GetPercentageToNextLevel(skillPtrRef);

	if (skillObj->UseAction(actionId, skillPtrRef, bUseAlternate != FALSE, newLevel, levelMessage, gainMessage) != SKILLRET_SUCCESS)
		return FALSE;

	if (newLevel > 0) {
		// Send out level message
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::Talk, LS_SKILL_NEW_LEVEL, levelMessage.c_str());

		// Give reward items, reward credits and/or titles
		CLevelRewardObjectList* rewardList = skillObj->GetCurrentLevelRewards(skillPtrRef);

		for (POSITION posLoc = rewardList->GetHeadPosition(); posLoc != NULL;) {
			CLevelRewardObject* rewardPtr = (CLevelRewardObject*)rewardList->GetNext(posLoc);

			if (bUseAlternate) {
				if (rewardPtr->m_altRewardGLID != 0 && rewardPtr->m_altRewardQty > 0 && (rewardPtr->m_rewardPointType == IT_NORMAL || rewardPtr->m_rewardPointType == IT_GIFT)) {
					CGlobalInventoryObj* globalInvObj = m_globalInventoryDB->GetByGLID(rewardPtr->m_altRewardGLID);
					pPO->m_inventory->AddInventoryItem(rewardPtr->m_altRewardQty, FALSE, globalInvObj->m_itemData, rewardPtr->m_rewardPointType);
					pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;
				}

				if (rewardPtr->m_altRewardAmount > 0 && (rewardPtr->m_rewardPointType == IT_NORMAL || rewardPtr->m_rewardPointType == IT_GIFT)) {
					UpdatePlayersCash(pPO, rewardPtr->m_altRewardAmount, TransactionType::LEVEL_REWARD, 0, 0, rewardPtr->m_rewardPointType == IT_GIFT ? CurrencyType::GIFT_CREDITS_ON_PLAYER : CurrencyType::CREDITS_ON_PLAYER);
				}
			} else {
				if (rewardPtr->m_rewardGLID != 0 && rewardPtr->m_rewardQty > 0 && (rewardPtr->m_rewardPointType == IT_NORMAL || rewardPtr->m_rewardPointType == IT_GIFT)) {
					CGlobalInventoryObj* globalInvObj = m_globalInventoryDB->GetByGLID(rewardPtr->m_rewardGLID);
					pPO->m_inventory->AddInventoryItem(rewardPtr->m_rewardQty, FALSE, globalInvObj->m_itemData, rewardPtr->m_rewardPointType);
					pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;
				}

				if (rewardPtr->m_rewardAmount > 0 && (rewardPtr->m_rewardPointType == IT_NORMAL || rewardPtr->m_rewardPointType == IT_GIFT)) {
					UpdatePlayersCash(pPO, rewardPtr->m_rewardAmount, TransactionType::LEVEL_REWARD, 0, 0, rewardPtr->m_rewardPointType == IT_GIFT ? CurrencyType::GIFT_CREDITS_ON_PLAYER : CurrencyType::CREDITS_ON_PLAYER);
				}
			}

			// Send out gain message
			RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::Talk, LS_EARNED_ITEMS, gainMessage.c_str());
		}

		// Get new titleId from skill level object and add it to the list of possible titles the player can choose from
		CLevelRangeObject* currentLevel = skillPtrRef->m_levelRangeDB->GetLevelByExp((long)skillObj->m_currentExperience);
		int currentTitleId = 0;
		if (bUseAlternate) {
			currentTitleId = currentLevel->m_altTitleId;
		} else {
			currentTitleId = currentLevel->m_titleId;
		}

		if (currentTitleId != 0) {
			CTitleObjectList* titleList = m_skillDatabase->m_titles;
			if (titleList) {
				// create the new title object to be put onto the player
				CTitleObject* title = new CTitleObject();
				title->m_titleID = currentTitleId;

				// get the title text to put onto the player's new title object
				CTitleObject* titleRef = titleList->GetObjById(currentTitleId);
				if (titleRef) {
					title->m_titleName = titleRef->m_titleName;

					if (!pPO->m_titleList)
						pPO->m_titleList = new CTitleObjectList();

					pPO->m_titleList->AddTail(title);

					pPO->m_title = title->m_titleName.c_str();

					// DRF - WARN - TODO - This Should Be Different Event!
					ATTRIB_DATA attribData;
					attribData.aeShort1 = pPO->m_netTraceId;
					AttribGetMovementObjectCfg(&attribData, pPO->m_netId);

					pPO->m_dbDirtyFlags |= PlayerMask::TITLES;
				}
			}
		}
	}

	percentToNextLevel = skillObj->GetPercentageToNextLevel(skillPtrRef);
	rewardsAtNextLevel = skillObj->GetRewardsAtNextLevel(skillPtrRef, m_skillDatabase->m_titles, m_globalInventoryDB, bUseAlternate != FALSE);

	totalExperience = skillObj->m_currentExperience;

	pPO->m_dbDirtyFlags |= PlayerMask::SKILLS;

	int percentileBefore = (percentBefore / 25);
	int percentileAfter = (percentToNextLevel / 25);

	if (percentileBefore != percentileAfter) {
		if (m_gameStateDb != 0) {
			try {
				m_gameStateDb->UpdatePlayer(pPO);
			} catch (KEPException* e) {
				LogError("DB Error updating player skills - playerId=" << pPO->m_netId << " " << e->m_msg);
				e->Delete();
			}
		}
	}

	return TRUE;
}

void ServerEngine::KickEveryoneExcept(const ZoneIndex& zi, const ChannelId& channel, NETID netId, UINT id) {
	ULONG zType = zi.GetType();
	if (zType == CI_HOUSING || zType == CI_CHANNEL) {
		// only allow in apts and bb zones now
		std::vector<int> ids;

		if (id)
#ifndef REFACTOR_INSTANCEID
			BroadcastEvent(new RenderLocaleTextEvent(id, eChatType::System), channel);
#else
			BroadcastEvent(new RenderLocaleTextEvent(id, eChatType::System), channel.toLong());
#endif

		for (POSITION posLoc = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); posLoc != NULL;) {
			auto pPO = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(posLoc);
			if (!pPO || (pPO->m_currentZoneIndex != zi) || (pPO->m_netId == netId))
				continue;

			if (pPO->m_housingZoneIndex == INVALID_ZONE_INDEX) {
				RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_NO_HOME);
				LogWarn("Bad Home Spawn - " << pPO->m_charName << " " << numToString(pPO->m_housingZoneIndex));
			} else {
				// now fire event to send them to their home, which may switch servers
				GotoPlaceEvent* gpe = new GotoPlaceEvent();
				auto pAO_dest = pPO->m_pAccountObject;
				gpe->GotoApartment(pAO_dest->m_serverId, pAO_dest->m_currentcharInUse);
				gpe->SetFrom(pPO->m_netId);
				m_dispatcher.QueueEvent(gpe);
			}
		}
	}
}

void ServerEngine::UpdatePlayer(IPlayer* currentPlayer) {
	auto pPO = dynamic_cast<CPlayerObject*>(currentPlayer);
	if (pPO)
		m_gameStateDb->UpdatePlayer(pPO);
}

void ServerEngine::BroadcastEvent(IEvent* rte, LONG channel, NETID netID, short radius, double x, double y, double z, eChatType chatType, bool allInstances) {
	Vector3f v;
	v.x = (float)x;
	v.y = (float)y;
	v.z = (float)z;
	m_multiplayerObj->BroadcastEvent(radius, rte, v, channel == -1 ? INVALID_CHANNEL_ID : channel, netID, chatType, allInstances);
}

//! update an item in all inventories in the game
void ServerEngine::UpdateItemInAllInventories(CItemObj* obj,
	int updateType) {
	// first do all the players
	for (POSITION pos = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); pos != 0;) {
		CPlayerObject* player = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(pos);
		updateItemInPlayerInventory(player, player->m_inventory, obj, updateType, true);
		updateItemInPlayerInventory(player, player->playerOrigRef()->m_bankInventory, obj, updateType, false);
		if (player->m_preArenaInventory)
			updateItemInPlayerInventory(player, player->m_preArenaInventory, obj, updateType, false);

		if (player->m_reconfigInven)
			updateItemInPlayerInventory(player, player->m_reconfigInven, obj, updateType, false);
	}

	// pending trade will be caught at commit of the trade, if we get
	// delete/update while trade is in progress

	// AT THIS TIME, ONLY UGC ITEMS CAN BE AFFECTED.
	// FUTURE will need to wipe item from everywhere

	// now do any stores (CStoreInventoryObjList)

	// now to any house inventories (CHousePlacementClass)

	// clean up any quests CInventoryObjList)(CAICollectionQuestObj)
}

//! update a a players inventory based on a new item.  update type is above
void ServerEngine::updateItemInPlayerInventory(
	CPlayerObject* pPO,
	CInventoryObjList* pIOL,
	CItemObj* pItemObj,
	int updateType,
	bool checkArmed) {
	POSITION posLast;
	for (POSITION pos = pIOL->GetHeadPosition(); (posLast = pos) != 0;) {
		auto pIO = (CInventoryObj*)pIOL->GetNext(pos);
		if (!pIO || (pIO->m_itemData->m_globalID != pItemObj->m_globalID))
			continue;

		if (checkArmed && pIO->isArmed()) {
			if (updateType == DELETE_ITEM)
				pIO->setArmed(FALSE);

			// this will turn on default clothing if necessary
			ValidateArmedInventoryForZone(pPO, pPO->m_currentZoneIndex);
		}

		if (updateType == DELETE_ITEM) {
			RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher,
				pPO->m_netId,
				eChatType::System,
				LS_ITEM_DELETED,
				pIO->m_itemData->m_itemName);
			pIO->SafeDelete();
			delete pIO;
			pIOL->RemoveAt(posLast);
		}
	}
}

BOOL ServerEngine::SendStringListToClient(int userInt, NETID netIdTo) {
	auto pAO = GetAccountObjectByNetId(netIdTo);
	if (!pAO) {
		LogError("Error locating account");
		return FALSE;
	}

	// Send Message Log To Client
	m_multiplayerObj->SendMsgLogToClient(
		netIdTo,
		0.0, // drf - added
		0.0 // drf - added
	);

	size_t players = 0;
	if (pAO->m_pPlayerObjectList) {
		players = (size_t)pAO->m_pPlayerObjectList->GetCount();
	}

	// now send two strings per char, name, edb
	StringListEvent* e = new StringListEvent(players * 2, userInt, SERVER_NETID);
#define SENDING_TWO_STRING 1
	e->SetObjectId((OBJECT_ID)SENDING_TWO_STRING);
	e->AddTo(netIdTo);
	if (players) {
		for (POSITION pos = pAO->m_pPlayerObjectList->GetHeadPosition(); pos;) {
			auto pPO = (CPlayerObject*)pAO->m_pPlayerObjectList->GetNext(pos);
			if (!pPO)
				continue;
			(*e->OutBuffer()) << pPO->m_charName << numToString(pPO->m_originalEDBCfg, "%d");
		}
	}
	e->SetFlags(EF_SENDIMMEDIATE);
	m_dispatcher.QueueEvent(e);

	return TRUE;
}

#define IDS_KEPSERVERIMPL_SAVEALL 135

void ServerEngine::SaveConfigValues(IKEPConfig* cfg) {
	Engine::SaveConfigValues(cfg);

	cfg->SetValue(GAMESTATE_TAG, m_gameStateFileName.c_str());
	cfg->SetValue(KEY_TAG, m_key.c_str());
	cfg->SetValue(ADMIN_ONLY_TAG, m_adminOnly);
}

class InvItemXMLFormatter : public CGlobalInventoryObjList::Traverser {
public:
	InvItemXMLFormatter(std::string& res) :
			result(res) {}

	virtual bool evaluate(CGlobalInventoryObj& obj) {
		char s[34];
		_itoa_s(obj.m_itemData->m_globalID, s, _countof(s), 10);
		result += "<Item value=\"";
		result += s;
		result += "\">";
		result += obj.m_itemData->m_itemName;
		result += "</Item>";
		return false;
	}
	std::string& result;
};

// Actually sends heartbeat once every 10 seconds
void ServerEngine::SendHeartbeatToAppController() {
	// Execute Only Once Every 10 Seconds
	TimeMs timeMs = fTime::TimeMs();
	if ((timeMs - m_lastHeartbeatTimestamp) <= KGPSERVER_HEARTBEAT_INTERVAL_MS)
		return;

	//Get handle for heartbeat monitor window
	if (m_hwndHeartbeatMonitor == NULL || (timeMs - m_heartbeatMonitorHwndTimestamp) > HEARTBEAT_MONITOR_HWND_REFRESH_INTERVAL_MS) {
		m_heartbeatMonitorHwndTimestamp = timeMs;
		m_hwndHeartbeatMonitor = FindWindowW(HEARTBEAT_MONITOR_WINDOW_CLASS, HEARTBEAT_MONITOR_WINDOW_TITLE);
	}

	//Send heartbeat message
	if (m_hwndHeartbeatMonitor) {
		if (m_uMsgKgpServerHeartbeat == 0)
			m_uMsgKgpServerHeartbeat = RegisterWindowMessageW(KGPSERVER_HEARTBEAT_MESSAGE);

		if (m_uMsgKgpServerHeartbeat) {
			m_lastHeartbeatTimestamp = timeMs;
			UINT playerCount = 0;
			if (m_multiplayerObj->m_runtimePlayersOnServer)
				playerCount = m_multiplayerObj->m_runtimePlayersOnServer->GetCount();
			PostMessage(m_hwndHeartbeatMonitor, m_uMsgKgpServerHeartbeat, GetGameId(), playerCount);
		}
	}
}

void ServerEngine::SendAlertEventToAppController(int eventId) {
	//Get handle for heartbeat monitor window
	if (m_hwndHeartbeatMonitor == NULL) {
		m_heartbeatMonitorHwndTimestamp = fTime::TimeMs();
		m_hwndHeartbeatMonitor = FindWindowW(HEARTBEAT_MONITOR_WINDOW_CLASS, HEARTBEAT_MONITOR_WINDOW_TITLE);
	}

	//Send alert message
	if (m_hwndHeartbeatMonitor) {
		if (m_uMsgKgpServerAlert == 0) {
			m_uMsgKgpServerAlert = RegisterWindowMessageW(KGPSERVER_ALERT_MESSAGE);
		}

		if (m_uMsgKgpServerAlert) {
			PostMessage(m_hwndHeartbeatMonitor, m_uMsgKgpServerAlert, GetGameId(), (LPARAM)eventId);
		}
	}
}

void ServerEngine::CheckForPingerRefresh() {
	if (!m_multiplayerObj || !m_multiplayerObj->m_serverNetwork)
		return;

	//Check if named event initialized
	if (m_hEventPingerRefresh == NULL) {
		int gameId = GetGameId();
		if (gameId != 0) {
			std::wstringstream ssEventName;
			ssEventName << L"KGPSERVER_REFRESH_PINGER_" << gameId;
			m_hEventPingerRefresh = CreateEventW(NULL, FALSE, FALSE, ssEventName.str().c_str());
			if (m_hEventPingerRefresh != NULL) {
				LogInfo("Created named event for pinger refresh - gameID=" << gameId << " handle=" << m_hEventPingerRefresh);
			} else {
				LogWarn("Error creating named event for pinger refresh - gameID=" << gameId);
			}
		}
	}

	//Check if event signaled
	if (m_hEventPingerRefresh) {
		if (WaitForSingleObject(m_hEventPingerRefresh, 0) == WAIT_OBJECT_0) {
			//Invalidate pinger per external request: e.g. the app name has been changed by AppIncubator
			LogInfo("Invalidate pinger by request");
			m_multiplayerObj->m_serverNetwork->pinger().InvalidatePinger();
		}
	}
}

const IKEPConfig* ServerEngine::GetDBKEPConfig() const {
	return &m_dbConfig->config();
}

void ServerEngine::PlaceGameItem(ScriptClientEvent* sce) {
	ULONG success, scriptPlacementId, doGlid, gameItemId, clientId;
	int zoneId, instanceId;
	Vector3f pos, ori;

	*sce->InBuffer() >> success >> clientId >> doGlid >> gameItemId >> zoneId >> instanceId >> scriptPlacementId >> pos.x >> pos.y >> pos.z >> ori.x >> ori.y >> ori.z;

	auto pPO = GetPlayerObjectByNetId(clientId, false);
	if (!pPO) {
		LogError("Cannot find player placing a game item - gameItemId=" << gameItemId);
		success = 0;
	}
	CAccountObject* pAO = NULL;
	if (pPO)
		pAO = pPO->m_pAccountObject;

	if (!pAO && success != 0) {
		LogError("Error locating account");
		success = 0;
	}

	CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(doGlid);
	if (!pGIO && success != 0) {
		// There could be inventory issues with UGC talk to YC if this happens
		LogError("player " << pAO->m_accountName << " requested unknown item " << doGlid);
		QueueMsgGeneralToClient(LS_GM_LACK_INVENTORY_ITEM, pPO);
		success = 0;
	}

	ULONG objPlacementId = 0;
	if (success != 0) {
		objPlacementId = AddGameItemPlacement(pPO->m_currentZoneIndex, doGlid, pos, ori, (int)pPO->m_handle, gameItemId);
	}

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlaceGameItem, scriptPlacementId, zoneId, 0, clientId);
	sse->SetFrom(clientId);
	*sse->OutBuffer() << objPlacementId;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

void ServerEngine::ReplaceGameItem(ScriptClientEvent* sce) {
	ULONG doGlid, ownerId, gameItemId, scriptPlacementId;
	int zoneId, zoneInstanceId;
	Vector3f pos, ori;
	*sce->InBuffer() >> doGlid >> pos.x >> pos.y >> pos.z >> ori.x >> ori.y >> ori.z >> zoneId >> zoneInstanceId >> ownerId >> gameItemId >> scriptPlacementId;

	ZoneIndex zoneIndex(zoneId);
	zoneIndex.setInstanceId(zoneInstanceId);

	ULONG objPlacementId = AddGameItemPlacement(zoneIndex, doGlid, pos, ori, ownerId, gameItemId);

	ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::PlaceGameItem, scriptPlacementId, zoneId, zoneInstanceId, 0);
	*sse->OutBuffer() << objPlacementId;
	m_dispatcher.ProcessSynchronousEvent(sse);
}

IServerEngine::GlidState ServerEngine::checkGlid(GLID glid) {
	if (m_globalInventoryDB->GetByGLID(glid))
		return Loaded;
	if (m_failedLoadGlids.count(glid) > 0)
		return Error;
	return NotLoaded;
}

IServerEngine::GlidState ServerEngine::checkGlids(std::vector<GLID> const& glids, std::vector<GLID>& unloadedGlids) {
	GlidState result = Loaded;
	for (std::vector<GLID>::const_iterator itGlid = glids.begin(); itGlid != glids.end(); ++itGlid) {
		if (m_globalInventoryDB->GetByGLID(*itGlid) == NULL) {
			if (m_failedLoadGlids.count(*itGlid) > 0) {
				if (result != NotLoaded) {
					result = Error;
				}
			} else {
				result = NotLoaded;
				unloadedGlids.push_back(*itGlid);
			}
		}
	}
	return result;
}

IBackgroundDBEvent* ServerEngine::createLoadGlidEvent(std::vector<GLID> const& glids, IEvent* responseEvent) {
	return new LoadItemsBackgroundEvent(glids, responseEvent);
}

void ServerEngine::setFailedLoadGlid(GLID glid) {
	if (!IS_VALID_GLID(glid))
		return;
	m_failedLoadGlids.insert(glid);
}

void ServerEngine::updateScriptZoneState(const ZoneIndex& zoneIndex, unsigned spinDownDelayMins, bool spinUp) {
	LogInfo(zoneIndex.ToStr() << (spinUp ? " UP" : " DOWN"));

	GetGameStateDispatcher()->QueueEvent(new UpdateScriptZoneBackgroundEvent(zoneIndex.GetInstanceId(), zoneIndex.GetType(), spinDownDelayMins, spinUp));

	if (!spinUp) {
		// Zone is shutting down - cleanup Paper Doll NPCs
		m_multiplayerObj->m_pServerItemObjMap->DeleteByZoneIndex(zoneIndex);
	}
}

void ServerEngine::CheckForPendingRespawnExpiry() {
	// Check for player respawn timeouts ?
	if (m_spawnExpiryUpdateTimer.ElapsedMs() < SPAWN_EXPIRY_REVIEW_INTERVAL_MS)
		return;
	m_spawnExpiryUpdateTimer.Reset();

	// Send player depart explicitly for expired spawns.
	for (POSITION pos = m_multiplayerObj->m_runtimeAccountDB->GetHeadPosition(); pos != NULL;) {
		auto pAO = (CAccountObject*)m_multiplayerObj->m_runtimeAccountDB->GetNext(pos);
		if (pAO == nullptr) {
			continue;
		}

		NETID currentNetID = 0;
		ZoneIndex departZoneIndex;
		unsigned playerArriveId = 0;
		if (pAO->FetchExpiredPendingPlayerDepartInfo(SPAWN_EXPIRY_TIMEOUT_MS, currentNetID, departZoneIndex, playerArriveId)) {
			assert(currentNetID != 0 && departZoneIndex.toLong() != 0 && departZoneIndex != INVALID_ZONE_INDEX && playerArriveId > 0);

			// let the script server know that the player is leaving this server
			if (currentNetID != 0 && departZoneIndex.toLong() != 0 && departZoneIndex != INVALID_ZONE_INDEX) {
				LogWarn("PlayerDeparted -> <" << pAO->m_accountName << ":" << currentNetID << "> after spawn timeout");
				m_multiplayerObj->SendPlayerDepartedEvent(currentNetID, departZoneIndex, playerArriveId);
			}
		}
	}
}

void ServerEngine::PlayerSaveZoneSpawnPoint(ScriptClientEvent* sce) {
	int zoneId = 0, playerId = 0;
	ULONG clientId = 0;
	float x, y, z, rx, ry, rz;
	*sce->InBuffer() >> zoneId >> clientId >> playerId >> x >> y >> z >> rx >> ry >> rz;

	if (playerId == 0) {
		auto pPO = GetPlayerObjectByNetId(clientId, true);
		if (pPO)
			playerId = pPO->m_handle;
	}

	if (playerId != 0) {
		m_gameStateDb->SavePlayerSpawnPoint(playerId, zoneId, x, y, z, rx, ry, rz);
	} else {
		LogInfo("Player not found: " << clientId);
	}
}

void ServerEngine::PlayerDeleteZoneSpawnPoint(ScriptClientEvent* sce) {
	int zoneId = 0, playerId = 0;
	ULONG clientId = 0;
	*sce->InBuffer() >> zoneId >> clientId >> playerId;

	if (playerId == 0) {
		auto pPO = GetPlayerObjectByNetId(clientId, true);
		if (pPO)
			playerId = pPO->m_handle;
	}

	if (playerId != 0) {
		m_gameStateDb->DeletePlayerSpawnPoint(playerId, zoneId);
	} else {
		LogInfo("Player not found: " << clientId);
	}
}

void ServerEngine::PlayerSpawnVehicle(ScriptClientEvent* sce) {
	int zoneId, placementId;
	Vector3f offset;
	Quaternionf orientation;
	Vector3f dimensions;
	float speed, hp;
	float loadSusp, unloadSusp;
	float mass;
	float wheelFriction;
	Vector2f jumpVelocityMPS;
	GLID glidThrottle;
	GLID glidSkid;
	ULONG clientId;
	(*sce->InBuffer()) >> zoneId >> placementId >> offset >> orientation >> dimensions >> speed >> hp >> loadSusp >> unloadSusp >> mass >> wheelFriction >> jumpVelocityMPS >> glidThrottle >> glidSkid >> clientId;

	auto pPO = GetPlayerObjectByNetId(clientId, true);
	if (pPO) {
		ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::PlayerSpawnVehicle, clientId);
		*e->OutBuffer()
			<< placementId
			<< offset.x << offset.y << offset.z
			<< orientation.x << orientation.y << orientation.z << orientation.w
			<< dimensions.x << dimensions.y << dimensions.z
			<< speed << hp
			<< loadSusp << unloadSusp
			<< mass
			<< wheelFriction
			<< jumpVelocityMPS
			<< glidThrottle
			<< glidSkid
			<< pPO->m_netTraceId;
		e->SetFrom(sce->From());
		BroadcastEventFromZoneIndex(e, zoneId);
	}
}

void ServerEngine::PlayerLeaveVehicle(ScriptClientEvent* sce) {
	int zoneId, placementId;
	ULONG clientId;
	(*sce->InBuffer()) >> zoneId >> placementId >> clientId;

	auto pPO = GetPlayerObjectByNetId(clientId, true);
	if (pPO) {
		ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::PlayerLeaveVehicle, clientId);
		*e->OutBuffer() << placementId << pPO->m_netTraceId;
		e->SetFrom(sce->From());
		BroadcastEventFromZoneIndex(e, zoneId);
	}
}

void ServerEngine::ScriptGetItemInfo(ScriptClientEvent* sce) {
	long zoneIdLong;
	unsigned long placementId;
	GLID glid;
	(*sce->InBuffer()) >> zoneIdLong >> placementId >> glid;

	ZoneIndex zoneId(zoneIdLong);

	auto pInvObj = m_globalInventoryDB->GetByGLID(glid);
	auto sse = new ScriptServerEvent(ScriptServerEvent::ItemInfo, placementId, zoneId.toLong(), zoneId.getClearedInstanceId());
	if (pInvObj != nullptr) {
		*sse->OutBuffer() << 1 << glid << pInvObj->m_itemData->m_useType << pInvObj->m_itemData->m_actorGroup;
	} else {
		*sse->OutBuffer() << 0; // Failed
	}

	m_dispatcher.ProcessSynchronousEvent(sse);
}

ULONG ServerEngine::AddGameItemPlacement(const ZoneIndex& zoneIndex, ULONG doGlid, const Vector3f& pos, const Vector3f& ori, int ownerId, int gameItemId) {
	ULONG objPlacementId = m_gameStateDb->AddGameItemPlacement(zoneIndex, doGlid, pos, ori, ownerId, gameItemId);
	assert(objPlacementId > 0);

	if (objPlacementId > 0) {
		// send a generate to the people currently in the zone
		ScriptClientEvent* sce = ScriptClientEvent::EncodeOwnedObjectGenerate(
			NULL,
			objPlacementId,
			doGlid,
			zoneIndex.toLong(),
			zoneIndex.GetInstanceId(),
			(double)pos.x, (double)pos.y, (double)pos.z,
			(double)ori.x, (double)ori.y, (double)ori.z,
			Placement_DynObj,
			gameItemId,
			ownerId);

		m_dispatcher.QueueEvent(sce);
		ClearZoneDOMemCache(zoneIndex);
	}

	return objPlacementId;
}

void ServerEngine::NPCSpawn(ScriptClientEvent* sce) {
	long zoneIdLong;
	int siType; // SERVER_ITEM_TYPE
	int placementId;
	std::string name;
	std::string charConfigJson;
	int ccId;
	int dbIndex;
	Vector3f pos;
	double rotDeg;
	GLID glidAnimSpecial;
	std::vector<GLID> glidsArmed;
	int numGlidsArmed;
	(*sce->InBuffer()) >> zoneIdLong >> siType >> placementId >> charConfigJson >> ccId >> dbIndex >> pos.x >> pos.y >> pos.z >> rotDeg >> glidAnimSpecial >> numGlidsArmed;
	for (size_t i = 0; i < numGlidsArmed; ++i) {
		GLID glidArmed;
		(*sce->InBuffer()) >> glidArmed;
		glidsArmed.push_back(glidArmed);
	}

	name = "NPC:" + std::to_string(placementId);

	ZoneIndex zoneId = ZoneIndex(zoneIdLong);

	// Create Server Item Object (static movement object)
	CServerItemObj* pSIO = new CServerItemObj;
	pSIO->m_zoneIndex = zoneId;
	pSIO->m_type = (SERVER_ITEM_TYPE)siType;
	pSIO->m_displayedName = name.c_str();
	pSIO->m_dbRef = dbIndex;
	pSIO->m_netTraceId = m_multiplayerObj->GetNextFreeNetTraceId();
	pSIO->m_position = pos;
	pSIO->m_rotationAdjust = (int)rotDeg;
	pSIO->m_glidAnimSpecial = glidAnimSpecial;
	pSIO->m_glidsArmed = glidsArmed;
	pSIO->m_placementId = placementId;
	if (!charConfigJson.empty()) {
		// Parse JSON string into charConfig
		std::string err;
		if (!pSIO->parseJSON(charConfigJson, err)) {
			LogError("Error parsing charConfg in JSON: " << err);
			return;
		}
	} else {
		// Generate Static Char Config
		const CharConfig* pCharConfig = reinterpret_cast<const CharConfig*>(ccId);
		if (!StaticCharConfig(pCharConfig, dbIndex)) {
			LogError("StaticCharConfig() FAILED - charConfig=" << ccId);
			return;
		}

		pSIO->m_charConfig = *pCharConfig;
	}

	LogInfo("ServerItemGenerator::AddItem() -> " << pSIO->ToStr());
	m_multiplayerObj->m_pServerItemObjMap->AddItem(pSIO, true);
}

//Ankit ----- Event Handlers for Sound Customization Events sent from client and the threadpool runnables associated with them
EVENT_PROC_RC ServerEngine::SoundRequestEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	double zoneIndex = e->Filter();
	double instanceId;
	(*e->InBuffer()) >> instanceId;

	ZoneIndex zi(zoneIndex);
	ULONG zoneType = zi.GetType();

	if (zoneType == CI_PERMANENT)
		instanceId = 1;
	else if (zoneType == CI_ARENA) {
		instanceId = 1;
		ZoneIndex newZI(zi.zoneIndex(), 0, zi.GetType());
		zoneIndex = newZI.toLong();
	}

	ServerEngine* serverInstance = (ServerEngine*)GetInstance();
	serverInstance->GetSound(disp, e->From(), zoneIndex, instanceId);

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ServerEngine::SoundUpdateEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* serverInstance = (ServerEngine*)GetInstance();
	IPlayer* player = serverInstance->GetPlayerByNetId(e->From(), true);
	if (player == nullptr)
		return EVENT_PROC_RC::CONSUMED;
	double zoneIndex = (player->Values())->GetNumber(PLAYERIDS_CURRENTZONEINDEX);
	ZoneIndex zi(zoneIndex);
	int clearedZoneIndex = zi.getClearedInstanceId();
	int clearedInstanceId = zi.GetInstanceId();
	double currentChannel = (player->Values())->GetNumber(PLAYERIDS_CURRENTCHANNEL);
	int glid, pitch, gain, max_dist, roll_off, loop, loop_delay, cone_outer_gain, cone_inner_angle, cone_outer_angle, dir_x, dir_y, dir_z;

	(*e->InBuffer()) >> glid >> pitch >> gain >> max_dist >> roll_off >> loop >> loop_delay >> cone_outer_gain >> cone_inner_angle >> cone_outer_angle >> dir_x >> dir_y >> dir_z;

	serverInstance->UpdateSound(clearedZoneIndex, clearedInstanceId, e->ObjectId(), glid, pitch, gain, max_dist, roll_off, loop, loop_delay, cone_outer_gain, cone_inner_angle, cone_outer_angle, dir_x, dir_y, dir_z);

	IEvent* event = disp->MakeEvent(disp->GetEventId("UpdateSoundCustomizationEvent"));
	if (event != nullptr) {
		event->SetObjectId(e->ObjectId());
		(*event->OutBuffer()) << glid << pitch << gain << max_dist << roll_off << loop << loop_delay
							  << cone_outer_gain << cone_inner_angle << cone_outer_angle << dir_x << dir_y << dir_z;
		serverInstance->BroadcastEvent(event, currentChannel);
	}

	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ServerEngine::SoundRemoveEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* serverInstance = (ServerEngine*)GetInstance();
	serverInstance->RemoveSound(e->ObjectId());
	return EVENT_PROC_RC::OK;
}
void XmlInsertChildElement(TiXmlElement* parent, const char* elemTag, const char* elemText) {
	TiXmlText childText(elemText);
	TiXmlElement childElem(elemTag);
	childElem.InsertEndChild(childText);
	parent->InsertEndChild(childElem);
}

ACTPtr ServerEngine::GetSound(IDispatcher* disp, NETID requestor, int zoneIndex, int instanceId) { //ULONG lparam, IDispatcher* disp, IEvent* e) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"SoundLogger"), "ServerEngine::GetSound()");
	ServerEngine* serverInstance = (ServerEngine*)GetInstance();
	int ii = instanceId;
	int zi = zoneIndex;

	return serverInstance->m_threadPool.enqueue([=]() -> RunResultPtr {
		std::string xml;
		try {
			mysqlpp::Connection::thread_start();
			START_TIMING(m_timingLogger, "DB:getSoundsXMLAsString");
			std::shared_ptr<KGP_Connection> m_conn = serverInstance->m_connectionPool->borrow();

			//			string xmlString;
			BEGIN_TRY_SQL();
			Query query = m_conn->query();
			query << "SELECT obj_placement_id, pitch, gain, max_distance, roll_off, `loop`, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle "
					 "FROM sound_customizations WHERE zone_index=%0 AND instance_id=%1";
			query.parse();
			// need more testing
			//		This piece right here don't work 'cause zi, ii, is not staying intact after queuer exits.
			StoreQueryResult res = query.store(zi, ii);

			TiXmlDocument doc;
			doc.LinkEndChild(new TiXmlDeclaration("1.0", "", ""));
			TiXmlElement* root = new TiXmlElement("Result");
			doc.LinkEndChild(root);
			for (int i = 0; i < res.size(); i++) {
				Row r = res[i];

				TiXmlElement* sound_Tag = new TiXmlElement("S");
				root->LinkEndChild(sound_Tag);

				XmlInsertChildElement(sound_Tag, "o", r[0]);
				XmlInsertChildElement(sound_Tag, "p", r[1]);
				XmlInsertChildElement(sound_Tag, "g", r[2]);
				XmlInsertChildElement(sound_Tag, "m", r[3]);
				XmlInsertChildElement(sound_Tag, "r", r[4]);
				XmlInsertChildElement(sound_Tag, "l", r[5]);
				XmlInsertChildElement(sound_Tag, "d", r[6]);
				XmlInsertChildElement(sound_Tag, "x", r[7]);
				XmlInsertChildElement(sound_Tag, "y", r[8]);
				XmlInsertChildElement(sound_Tag, "z", r[9]);
				XmlInsertChildElement(sound_Tag, "og", r[10]);
				XmlInsertChildElement(sound_Tag, "ia", r[11]);
				XmlInsertChildElement(sound_Tag, "oa", r[12]);
			}

			TiXmlPrinter printer;
			doc.Accept(&printer);
			xml = printer.Str();
			END_TRY_SQL();

			if (disp) {
				IEvent* event = disp->MakeEvent(disp->GetEventId("SoundCustomizationReplyEvent"));
				if (event != nullptr) {
					(*event->OutBuffer()) << xml;
					event->AddTo(requestor);
					disp->QueueEvent(event);
				}
			}
		} catch (const std::exception& e) {
			LogError(e.what());
			return RunResultPtr(new StringRunResult(0, "<error/>"));
		}
		return RunResultPtr(new StringRunResult(0, xml));
	});
}

void ServerEngine::UpdateSound(int clearedZoneIndex, int clearedInstanceId, int objectId, int xglid, int pitch, int gain, int max_dist, int roll_off, int loop, int loop_delay, int cone_outer_gain, int cone_inner_angle, int cone_outer_angle, int dir_x, int dir_y, int dir_z) {
	m_threadPool.enqueue([=]() -> RunResultPtr {
		try {
			mysqlpp::Connection::thread_start();
			START_TIMING(m_timingLogger, "DB:updateSoundWithParams");
			std::shared_ptr<KGP_Connection> m_conn = m_connectionPool->borrow();
			BEGIN_TRY_SQL();
			Query query = m_conn->query();
			query << "INSERT INTO sound_customizations(obj_placement_id, zone_index, instance_id, pitch, gain, max_distance, roll_off, "
					 "`loop`, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle)"
					 "VALUES (%0, %1, %2, %3, %4, %5, %6, %7, %8, %9, %10, %11, %12, %13, %14)"
					 "ON DUPLICATE KEY UPDATE obj_placement_id=%0, zone_index=%1, instance_id=%2, pitch=%3, gain=%4, max_distance=%5, roll_off=%6, "
					 "`loop`=%7, loop_delay=%8, dir_x=%9, dir_y=%10, dir_z=%11, cone_outer_gain=%12, cone_inner_angle=%13, cone_outer_angle=%14";
			query.parse();

			query.execute(objectId, clearedZoneIndex, clearedInstanceId, pitch, gain, max_dist, roll_off, loop, loop_delay, dir_x, dir_y, dir_z, cone_outer_gain, cone_inner_angle, cone_outer_angle);

			END_TRY_SQL();
		} catch (const std::exception& e) {
			LogError(e.what());
		}
		return RunResultPtr();
	});
}

void ServerEngine::RemoveSound(int id) {
	m_threadPool.enqueue([=]() -> RunResultPtr {
		try {
			mysqlpp::Connection::thread_start();
			START_TIMING(m_timingLogger, "DB:deleteSoundWithPlacementId");
			std::shared_ptr<KGP_Connection> m_conn = m_connectionPool->borrow();
			BEGIN_TRY_SQL();
			Query query = m_conn->query();
			query << "DELETE FROM sound_customizations WHERE obj_placement_id=%0";
			query.parse();
			query.execute(id);

			END_TRY_SQL();
		} catch (const std::exception& e) {
			LogError(e.what());
		}
		return RunResultPtr();
	});
}

SoundRESTHandler::SoundRESTHandler(ServerEngine& server) :
		HTTPServer::RequestHandler("ServerControlHandler"), _server(server) {}

std::string getArg(const HTTPServer::Request& request, const std::string& tag) {
	std::map<std::string, std::string>::const_iterator i = request._paramMap.find(tag);
	if (i == request._paramMap.cend())
		throw ChainedException(ErrorSpec(-1) << "Missing required argument [" << tag << "]");
	return i->second.c_str();
}

int getIntArg(const HTTPServer::Request& request, const std::string& tag) {
	return atoi(getArg(request, tag).c_str());
}

// http://localhost:25857/sound?op=update&zi=0&ii=1&o=2&glid=3&p=4&g=5&m=6&r=7&l=8&d=9&og=10&ia=11&oa=12&x=13&y=14&z=15
// http://localhost:25857/sound?op=get&zi=0&ii=1
// http://localhost:22857/sound?op=remove&oid=2
//
// http://localhost:25857/sound?op=get&zi=805306387&ii=845118
int SoundRESTHandler::handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) {
	// LogTrace("");
	try {
		std::string op = getArg(request, "op");
		if (op == "update") {
			//	LOG4CPLUS_INFO(log4cplus::Logger::getInstance(L"SHUTDOWN")
			//		, "ServerControlHandler::handle(): Signalling SHUTDOWN");
			//	_server.Shutdown();
			int zoneIndex = getIntArg(request, "zi");
			int instanceid = getIntArg(request, "ii");
			int objectid = getIntArg(request, "o");
			//			int glid				= getIntArg(request, "glid");
			int pitch = getIntArg(request, "p");
			int gain = getIntArg(request, "g");
			int max_dist = getIntArg(request, "m");
			int roll_off = getIntArg(request, "r");
			int loop = getIntArg(request, "l");
			int loop_delay = getIntArg(request, "d");
			int cone_outer_gain = getIntArg(request, "og");
			int cone_inner_angle = getIntArg(request, "ia");
			int cone_outer_angle = getIntArg(request, "oa");
			int dir_x = getIntArg(request, "x");
			int dir_y = getIntArg(request, "y");
			int dir_z = getIntArg(request, "z");

			_server.UpdateSound(zoneIndex, instanceid, objectid, 0 /*unused*/, pitch, gain, max_dist, roll_off, loop, loop_delay, cone_outer_gain, cone_inner_angle, cone_outer_angle, dir_x, dir_y, dir_z);
			endPoint << (HTTPServer::Response()
						 << Header("Content-Type", "text/xml; charset=UTF-8")
						 << "<ok/>");
			return 1;
		}

		if (op == "get") {
			int zoneIndex = getIntArg(request, "zi");
			int instanceId = getIntArg(request, "ii");
			ACTPtr act = _server.GetSound(0, 0, zoneIndex, instanceId);
			act->wait();
			RunResultPtr pr = act->getResult();
			std::string s(((StringRunResult*)pr.get())->str());
			endPoint << (HTTPServer::Response()
						 << Header("Content-Type", "text/xml; charset=UTF-8")
						 << s // ((StringRunResult*)pr.get())->str()
			);
			return 1;
		}

		if (op == "remove") {
			int oid = getIntArg(request, "oid");
			_server.RemoveSound(oid);
			endPoint << (HTTPServer::Response()
						 << Header("Content-Type", "text/xml; charset=UTF-8")
						 << "<ok />");
			return 1;
		}

	} catch (const ChainedException& e) {
		endPoint << (HTTPServer::Response()
					 << Header("Content-Type", "text/xml; charset=UTF-8")
					 << "<error>"
					 << e.str()
					 << "</error>");
	}
	return 1;
}

//Ankit ----- Event Handlers for Player Title Events sent from client and the threadpool runnables associated with them
void ServerEngine::DispatchPlayerTitles(IDispatcher* disp, int playerId, NETID to) {
	m_threadPool.enqueue([=]() -> RunResultPtr {
		std::vector<std::pair<int, std::string>> playerTitleList;

		mysqlpp::Connection::thread_start();
		std::shared_ptr<KGP_Connection> m_conn = m_connectionPool->borrow();
		START_TIMING(m_timingLogger, "DB:getPlayerTitleList");
		BEGIN_TRY_SQL();
		Query query = m_conn->query();
		query << "SELECT t.title_id, t.name FROM titles t, player_titles pt WHERE pt.title_id = t.title_id AND pt.player_id = %0";
		query.parse();
		StoreQueryResult res = query.store(playerId);

		for (int i = 0; i < res.size(); i++) {
			Row r = res[i];
			playerTitleList.push_back(std::make_pair((int)r[0], (std::string)r[1]));
		}
		END_TRY_SQL();

		IEvent* ev = disp->MakeEvent(disp->GetEventId("TitleListEvent"));
		if (ev != nullptr) {
			(*ev->OutBuffer()) << playerTitleList.size();
			for (auto it = playerTitleList.begin(); it != playerTitleList.end(); it++)
				(*ev->OutBuffer()) << it->first << it->second;
			ev->AddTo(to);
			disp->QueueEvent(ev);
		}
		return nullptr;
	});
}

EVENT_PROC_RC ServerEngine::PlayerTitleEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	LOG4CPLUS_TRACE(Logger::getInstance(L"PoolLogger"), "PlayerTitleEventHandler()");
	int playerId;
	(*e->InBuffer()) >> playerId;
	ServerEngine* serverInstance = (ServerEngine*)GetInstance();
	serverInstance->DispatchPlayerTitles(disp, playerId, e->From());
	return EVENT_PROC_RC::CONSUMED;
}

void ServerEngine::ChangePlayerTitle(IDispatcher* disp, int playerId, int titleId, NETID fromNetId) {
	// string titleText = m_mySqlGameState->UpdatePlayerTitle(m_playerId, m_titleId);

	m_threadPool.enqueue([=]() -> RunResultPtr {
		mysqlpp::Connection::thread_start();
		std::shared_ptr<KGP_Connection> m_conn = m_connectionPool->borrow();

		START_TIMING(m_timingLogger, "DB:getPlayerTitle");
		std::string playerTitle = "";
		BEGIN_TRY_SQL();
		Query query = m_conn->query();
		query << "SELECT t.name FROM titles t, player_titles pt WHERE pt.title_id = %0 and t.title_id = %0 and pt.player_id = %1";
		query.parse();
		StoreQueryResult res = query.store(titleId, playerId);
		if (res.num_rows() > 0)
			playerTitle = (res.at(0))[0];
		//	END_TRY_SQL();

		//	START_TIMING(m_timingLogger, "DB:updatePlayerTitle");
		//	BEGIN_TRY_SQL();
		//		Query query = m_conn->query();
		query.reset();
		query << "UPDATE players SET title = %0q where player_id = %1";
		query.parse();
		SimpleResult res1 = query.execute(playerTitle, playerId);
		END_TRY_SQL();

		IPlayer* player = GetPlayerByNetId(fromNetId, true);
		if (player == 0) {
			LOG4CPLUS_ERROR(m_logger, "ServerEngine::ChangePlayerTitle() Failed to acquire player with net id [" << fromNetId << "]");
			return nullptr;
		}

		(player->Values())->SetString(PLAYERIDS_TITLE, playerTitle.c_str());
		int channel, netAssignId;
		channel = (player->Values())->GetNumber(PLAYERIDS_CURRENTCHANNEL);
		netAssignId = (player->Values())->GetNumber(PLAYERIDS_NETTRACEID);
		IEvent* ev = disp->MakeEvent(disp->GetEventId("TitleEvent"));
		(*ev->OutBuffer()) << playerTitle << netAssignId;
		BroadcastEvent(ev, channel, 0, 0, 0, 0, 0, eChatType::System, 0);
		return nullptr;
	});
}
EVENT_PROC_RC ServerEngine::PlayerChangeTitleEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	int playerId, titleId;
	(*e->InBuffer()) >> playerId >> titleId;
	ServerEngine* serverInstance = (ServerEngine*)GetInstance();
	serverInstance->ChangePlayerTitle(disp, playerId, titleId, e->From());
	return EVENT_PROC_RC::CONSUMED;
}

EVENT_PROC_RC ServerEngine::ZoneImportedEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	jsVerifyReturn(me != 0, EVENT_PROC_RC::NOT_PROCESSED);

	ZoneImportedEvent* zie = static_cast<ZoneImportedEvent*>(e);
	ZoneIndex zoneIndex;
	NETID netId = 0;
	zie->ExtractInfo(zoneIndex, netId);

	LogInfo(zoneIndex.ToStr() << ", requester NETID: " << netId);

	CPlayerObject* pPlayer = me->GetPlayerObjectByNetId(netId, true);
	assert(pPlayer != nullptr);
	if (pPlayer == nullptr) {
		return EVENT_PROC_RC::OK;
	}

	CAccountObject* pAccount = pPlayer->m_pAccountObject;
	assert(pAccount != nullptr);
	if (pAccount == nullptr) {
		return EVENT_PROC_RC::OK;
	}

	CPlayerObject* pPlayerOnAccount = pAccount->GetCurrentPlayerObject();
	assert(pPlayerOnAccount != nullptr);
	if (pPlayerOnAccount == nullptr) {
		return EVENT_PROC_RC::OK;
	}

	// Update flag to force sending PlayerDepart event on spawn
	pPlayerOnAccount->m_forcePlayerDepartEventOnSpawn = true;
	return EVENT_PROC_RC::OK;
}

// Load durations for Kaneva stock animations
void ServerEngine::LoadStockAnimationDurationMs() {
	assert(m_gameStateDb != nullptr);

	std::map<GLID, TimeMs> effectDurationMap;
	for (POSITION pos = m_movObjRefModelList->GetHeadPosition(); pos != nullptr;) {
		CMovementObj* pMovObj = static_cast<CMovementObj*>(m_movObjRefModelList->GetNext(pos));
		assert(pMovObj && pMovObj->getSkeleton());
		pMovObj->getSkeleton()->getAnimationDurations(effectDurationMap);
	}

	if (m_logger.isEnabledFor(DEBUG_LOG_LEVEL)) {
		for (auto pr : effectDurationMap) {
			LogDebug("[" << pr.first << "] " << pr.second);
		}
	}

	m_gameStateDb->AddEffectDurationMs(effectDurationMap);
}

} // namespace KEP