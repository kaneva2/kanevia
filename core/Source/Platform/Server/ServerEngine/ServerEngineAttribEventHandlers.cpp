///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ServerEngine.h"

#include "CCommerceClass.h"
#include "CBankClass.h"
#include "CSafeZones.h"
#include "CSecureTradeClass.h"
#if (DRF_OLD_PORTALS == 1)
#include "CPortalSysClass.h"
#endif
#include "CElementsClass.h"
#include "CCollisionClass.h"
#include "CGmPageClass.h"
#include "CWorldChannelRules.h"
#include "CBattleSchedulerClass.h"
#if (DRF_OLD_VENDORS == 1)
#include "CServerItemGen.h"
#endif
#include "CServerItemClass.h"
#include "CAccountClass.h"
#include "CAIICollectionQuest.h"
#include "CMultiplayerClass.h"
#include "CMovementObj.h"
#include "CGlobalInventoryClass.h"
#include "CHousingClass.h"
#include "CHousePlacementClass.h"
#include "CInventoryClass.h"
#include "CExplosionClass.h"
#include "CCurrencyClass.h"
#include "CPlayerClass.h"
#include "CSkillClass.h"
#include "CWorldAvailableClass.h"
#include "UseTypes.h"
#include <PassList.h>
#include "AIclass.h"
#include "CCfgClass.h"
#include "RuntimeSkeleton.h"
#include "IGameState.h"
#include "ItemAnimation.h"
#include "Audit.h"
#include "DBStrings.h"
#include "..\Database\GameStateDispatcher.h"
#include "AngleRounding.h"
#include <limits>
#include "resource.h"
#include "LocaleStrings.h"

namespace KEP {

static LogInstance("ServerEngine");

#define PENDING_PLAYER_GET -2

BOOL ServerEngine::AttribGetStaticMovementObjectCfg(ATTRIB_DATA* pMsg, NETID netIdFrom) {
	if (!pMsg || !m_multiplayerObj->m_pServerItemObjMap)
		return FALSE;

	//	auto pPO = GetPlayerObjectByNetId ( netIdFrom, true);
	//	if (!pPO) {
	//		LogError("GetPlayerObjectByNetId() FAILED - netId=" << netIdFrom );
	//		return FALSE;
	//	}

	int netTraceId = pMsg->aeInt1;

	//	long channelId = pPO->m_currentChannel.toLong();
	//	long zoneIndex = pPO->m_currentZoneIndex.toLong();

	auto pSIO = m_multiplayerObj->m_pServerItemObjMap->GetObjectByNetTraceId(netTraceId);
	if (!pSIO) {
		LogWarn("STATIC MO NOT FOUND - netTraceId=" << netTraceId);
		return FALSE;
	}

	return QueueMsgSpawnStaticMOToClient(netIdFrom, pSIO) ? TRUE : FALSE;
}

BOOL ServerEngine::AttribClientRequestPlayer(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	auto pAO = GetAccountObjectByNetId(idFromLoc);
	if (!pAO) {
		LogError("Error locating account by NETID: " << idFromLoc);
		return FALSE;
	}

	if (pAttrib->aeShort2 < 8) { // VERSION EXPLOIT
		LogError("Version Exploit " << pAO->m_accountName);
		return FALSE;
	}

	return SelectPlayerAndSpawn(pAO, idFromLoc, pAttrib->aeInt2, pAttrib->aeInt1);
}

BOOL ServerEngine::SelectPlayerAndSpawn(
	CAccountObject* pAO,
	NETID netIdFrom,
	int clientVersion,
	int selectedChar,
	ZoneIndex* overrideZone,
	float overrideX, float overrideY, float overrideZ,
	bool reconnecting,
	bool goingToPlayer,
	int rotation,
	const char* _url) {
	if (pAO->m_aiServer) {
		LogError("AI_SERVER - '" << pAO->m_accountName << "' netId=" << netIdFrom << " Disconnecting...");
		m_multiplayerObj->DisconnectClient(netIdFrom, DISC_SECURITY_VIO);
		return FALSE;
	}

	bool reconnected = false;

	LogDebug(pAO->m_accountName << " selecting char " << selectedChar);
	if (pAO->m_currentcharInUse == PENDING_PLAYER_GET) { // avoid double call to this fn
		LogWarn(pAO->m_accountName << " selecting char again! " << selectedChar);
		return FALSE;
	}

	// set client content version
	pAO->m_localClientContentVersion = clientVersion;
	if (pAO->m_currentcharInUse == selectedChar) {
		// reentering player on server
		POSITION rDBPosLast;
		for (POSITION rDBPos = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); (rDBPosLast = rDBPos) != NULL;) {
			// remove player
			auto pPO = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(rDBPos);
			if (pPO->m_accountNumberRef == pAO->m_accountNumber) {
				// need to update account
				if (m_multiplayerObj->UpdateAccount(pPO, pAO)) {
					LogInfo("Reconnecting...->" << pAO->m_accountName);
					rotation = (int)pPO->InterpolatedRotDeg();
					reconnected = true;
				} else {
					LogInfo("Failed Reconnecting...->" << pAO->m_accountName);
				}

				// remove old one since we re-create new one when they spawn
				m_multiplayerObj->m_runtimePlayersOnServer->RemoveAt(rDBPosLast);
				pPO->SafeDelete();
				delete pPO;
				pPO = 0;
			}
		} // end remove player
	} // end reentering player on server
	else if (pAO->m_currentcharInUse != -1) {
		// character is in use to not alow to play
		//
		// send client notification that a character on this account need to
		// time out befor logging in
		//
#define CharacterTimeoutMenuId 120
		LogWarn("Another character on this account needs to time out :" << pAO->m_accountName);
		TellClientToOpenMenu(CharacterTimeoutMenuId, netIdFrom, false, 0);
		return FALSE;
	} // end character is in use to not allow to play

	if (selectedChar < 0) {
		LogWarn("Bounds FindIndex() ClientRequestPlayer() :" << pAO->m_accountName);
		return FALSE;
	}

	POSITION cPos;
	if (pAO->m_pPlayerObjectList == NULL ||
		(cPos = pAO->m_pPlayerObjectList->FindIndex(selectedChar)) == NULL) {
		LogWarn("Character does not exist on account :" << pAO->m_accountName);
		return FALSE;
	}

	CPlayerObject* plObj = (CPlayerObject*)pAO->m_pPlayerObjectList->GetAt(cPos);
	std::string url(_url ? _url : "");

	// always make and event to finish this processing
	// usually will be an async event
	GetPlayerEvent* e = new GetPlayerEvent(netIdFrom, plObj->m_handle, pAO->m_serverId,
		pAO->m_gm != FALSE,
		overrideZone, overrideX, overrideY, overrideZ, rotation,
		reconnected, reconnecting, selectedChar,
		goingToPlayer,
		url.c_str());

	if (!reconnected && (plObj->m_timeDbUpdate != TIME_UNDEFINED))
		e->item = plObj;

	// check with the database to load the data about the character
	if (m_gameStateDispatcher != 0 && !reconnected && (plObj->m_timeDbUpdate == TIME_UNDEFINED)) {
		plObj->m_pAccountObject = pAO;
		// kick off event to get player from db
		m_gameStateDispatcher->QueueEvent(e);
	} else {
		// not async since reconnecting, just call handler to finish processing
		GetPlayerBackgroundHandler((ULONG)this, &m_dispatcher, e);
		e->Delete();
	}
	pAO->m_currentcharInUse = PENDING_PLAYER_GET; // avoid double call to this fn

	return TRUE;
}

///////////////////////////////////////////////////////////
/// final process in processing getting the account from the database
/// on login.  This is handler from event background db thread sent
EVENT_PROC_RC ServerEngine::GetAccountBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	GetAccountEvent* gae = dynamic_cast<GetAccountEvent*>(e);
	jsVerifyReturn(lparam != 0 && gae != 0, EVENT_PROC_RC::NOT_PROCESSED);
	jsVerifyReturn(gae->Filter() == (LONG)GetResultSetsEvent::PLAYER || gae->Filter() == (LONG)GetResultSetsEvent::ACCOUNT, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;

	if (gae->ret == 0) {
		CAccountObject* pAO = dynamic_cast<CAccountObject*>(gae->item);
		jsVerifyReturn(pAO != 0, EVENT_PROC_RC::NOT_PROCESSED);

		CAccountObject* pAO_old = 0;

		// find account this one is replacing in both m_runtimeAccountDB and m_accountDatabase
		for (POSITION posLoc = me->m_multiplayerObj->m_runtimeAccountDB->GetHeadPosition(); posLoc != NULL;) {
			CObject*& objRef = me->m_multiplayerObj->m_runtimeAccountDB->GetNext(posLoc);
			CAccountObject* pAO_find = (CAccountObject*)objRef;
			if (pAO_find->m_netId == gae->From()) {
				pAO_old = pAO_find;
				objRef = pAO; // replace value in array
				break;
			}
		}

		if (pAO_old == 0) {
			LogError("Lost account during background processing kaneva userId = " << gae->kanevaUserId);
			me->m_multiplayerObj->LogoutCleanup(pAO, DISC_LOST); //Generally lost the player connection
			pAO->SafeDelete();
			delete pAO;
			pAO = 0;
			return EVENT_PROC_RC::NOT_PROCESSED;
		}

		CAccountObject* pAO_oldOther = 0;
		// find account this one is replacing
		for (POSITION posLoc = me->m_multiplayerObj->m_accountDatabase->GetHeadPosition(); posLoc != NULL;) {
			CObject*& objRef = me->m_multiplayerObj->m_accountDatabase->GetNext(posLoc);
			CAccountObject* pAO_find = (CAccountObject*)objRef;
			if (pAO_find->m_netId == gae->From()) {
				pAO_oldOther = pAO_find;
				jsAssert(pAO_oldOther == pAO_old);
				pAO_find->SafeDelete();
				delete pAO_find;
				objRef = pAO; // replace value in array
				break;
			}
		}

		if (pAO_oldOther == 0) {
			LogError("Lost account during background processing kaneva userId = " << gae->kanevaUserId);
			return EVENT_PROC_RC::NOT_PROCESSED;
		}

		//No players were found for the account.  Disconnect.
		if (!pAO->m_pPlayerObjectList) {
			IEvent* rte = new RenderLocaleTextEvent(LS_PLAYER_NOT_FOUND, eChatType::System);
			rte->AddTo(e->From());
			rte->SetFlags(rte->Flags() | EF_SENDIMMEDIATE);
			me->m_dispatcher.QueueEvent(rte);
			LogError("ACCOUNT NOT FOUND - '" << pAO->m_accountName << "' Disconnecting...");
			me->m_multiplayerObj->DisconnectClient(e->From(), DISC_FORCE);
			return EVENT_PROC_RC::OK;
		}

		EventSyncEvent* event = new EventSyncEvent(me->m_dispatcher.eventMap(), pAO->m_pPlayerObjectList->GetCount());

		// queue it to be sent
		event->AddTo(e->From());
		me->m_dispatcher.QueueEvent(event);

		if (me->m_multiplayerObj->m_restrictedMode && !pAO->m_administratorAccess) {
			RenderLocaleTextEvent::SendLocaleTextTo(me->m_dispatcher, e->From(), eChatType::System, LS_LOGIN_SERVER_CLOSED);
			IEvent* rte = new RenderLocaleTextEvent(LS_LOGIN_SERVER_CLOSED, eChatType::System);
			rte->AddTo(e->From());
			rte->SetFlags(rte->Flags() | EF_SENDIMMEDIATE);
			me->m_dispatcher.QueueEvent(rte);
			LogError("ADMIN ONLY - netId=" << e->From() << " Disconnecting...");
			me->m_multiplayerObj->DisconnectClient(e->From(), DISC_COMM_ASSIGN);
		}
	} else {
		LONG parentGameId = me->m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId();
		LONG appGameId = me->m_multiplayerObj->m_serverNetwork->pinger().GetGameId();
		if (parentGameId == 0) {
			RenderLocaleTextEvent::SendLocaleTextTo(me->m_dispatcher, e->From(), eChatType::System, gae->ret);
			LogError("GetParentGameId() FAILED - netId=" << e->From() << " Disconnecting...");
			me->m_multiplayerObj->m_serverNetwork->DisconnectClient(e->From(), DISC_COMM_ASSIGN);
		} else {
			IEvent* noCharEvent = new Missing3DAppCharacterEvent(parentGameId, appGameId);
			noCharEvent->AddTo(e->From());
			me->m_dispatcher.QueueEvent(noCharEvent);
		}
	}

	return EVENT_PROC_RC::OK;
}

/*
 *Function Name:RefreshPlayerBackgroundHandler
 *
 *Description:Refreshes the player's inventory or bank inventory (storage). Event is caught in response to the
 *  		  mysql db background thread that performed the work. Note that the storage inventory (in particular)
 *  		  may be empty.
 *
 *Returns:	EVENT_PROC_RC::NOT_PROCESSED or EVENT_PROC_RC::OK
 *
 */

EVENT_PROC_RC ServerEngine::RefreshPlayerBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	RefreshPlayerBackgroundEvent* rpbe = dynamic_cast<RefreshPlayerBackgroundEvent*>(e);
	jsVerifyReturn(lparam != 0 && rpbe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;

	if (rpbe->ret != 0) {
		LogError("rpbe->ret != 0");
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// DRF - Sanity Check
	if (!me || !me->m_multiplayerObj || !me->m_multiplayerObj->m_runtimePlayersOnServer) {
		LogError("Invalid Arguments");
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// Get Player Object
	auto pPO = me->GetPlayerObjectByNetId(rpbe->From(), false);
	if (!pPO) {
		LogError("GetObjectByNetId() FAILED - netID=" << rpbe->ObjectId());
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("Error locating account in RefreshPlayerBackgroundHandler for " << pPO->m_charName);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	auto pPO_cur = pAO->GetCurrentPlayerObject();
	if (!pPO_cur) {
		LogWarn("Cannot access base character on account " << pAO->m_accountName << " " << 14352);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	// No Cash ?
	if (!pPO->m_cash) {
		LogError("player->cash is null - name=" << pPO->m_charName);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	bool updPlayerInventory = ((rpbe->updateFlags & PlayerMask::INVENTORY) != 0);
	bool updateBank = ((rpbe->updateFlags & PlayerMask::BANK_INVENTORY) != 0);

	// update the cash if it hasn't changed, otherwise assume something else
	// updated it since then.  Assume it is more recent and don't requeue event
	bool updCash = false;
	bool cashSameAmount = false;

	if (updPlayerInventory) {
		updCash = ((rpbe->updateFlags & PlayerMask::CASH) != 0);
		cashSameAmount = (pPO->m_cash->m_amount == rpbe->prevAmount);
	}

	if (updCash && cashSameAmount && updPlayerInventory) {
		pPO->m_cash->m_amount = rpbe->newAmount;
		LogInfo("Updated player->cash - name=" << pPO->m_charName << " amount=" << rpbe->newAmount);
	}

	// No Inventory ?
	if (updPlayerInventory && !pPO->m_inventory) {
		LogError("player->inventory is null - name=" << pPO->m_charName);
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	pPO->m_inventory->m_pendingRefresh = false;

	CInventoryObjList** ppIOL = NULL;
	if (updPlayerInventory) {
		ppIOL = &(pPO->m_inventory);
	} else if (updateBank) {
		ppIOL = &(pPO_cur->m_bankInventory);
	}

	if (*ppIOL && updPlayerInventory) {
		// DRF - Bug Fix - Reset Request Pending Flag
		(*ppIOL)->m_pendingRefresh = false;
	}

	if (updPlayerInventory || updateBank) {
		bool invIsDirty = false;
		bool sameCommitCount = true;
		if (*ppIOL != NULL && updPlayerInventory) {
			invIsDirty = (*ppIOL)->IsDirty();
			sameCommitCount = ((*ppIOL)->m_commitCount == rpbe->invCommitCount);
		}
		if (sameCommitCount && !invIsDirty) {
			if (rpbe->inventory) {
				// may be empty if nothing returned. This is a real issue because there is an issue with
				// the order of the counting.  If count goes to zero, could be deleted prematurely
				// Also, we are decrementing correctly based on the cleanup of the prior inventory.
				// but when we reset it we are not incrementing. Also, why is this code called twice?
				// iterate over the new inventory and increment the cleaner counters, rebuild and watch
				// the cleaner state over these positions.
				ULONG items = 0;
				for (POSITION pos = rpbe->inventory->GetHeadPosition(); pos != NULL;) {
					auto pIO = (CInventoryObj*)rpbe->inventory->GetNext(pos);
#ifdef DEPLOY_CLEANER
					CGlobalInventoryObjList::GetInstance()->cleaner()->itemAdded(pIO->m_itemData->m_globalID);
#endif
					++items;
				}

				if (*ppIOL != NULL) {
					(*ppIOL)->SafeDelete();
					delete *ppIOL;
				}

				if (updPlayerInventory) {
					// Don't use pointer. Copy the inventory.
					pPO->m_inventory = rpbe->inventory;
				} else if (updateBank) {
					// Don't use pointer. Copy the inventory.
					pPO_cur->m_bankInventory = rpbe->inventory;
				}

				rpbe->inventory = 0; // prevent delete in event's destructor since we took inventory off it

				LogInfo("Updated player->inventory - name=" << pPO->m_charName << " items=" << items);
			}

			// this always returns true, so no rc ck

			if (updPlayerInventory) {
				me->SendMsgBlockItemToClient(*ppIOL, FALSE, e->From(), rpbe->uniInt, pPO->m_cash, pPO, TRUE);
			} else if (updateBank) {
				me->SendMsgBlockItemToClient(*ppIOL, FALSE, e->From(), rpbe->uniInt, 0, pPO_cur, TRUE);
			}

		} else {
			if (invIsDirty) {
				// update the player in case has outstanding inv changes
				if (me->m_gameStateDb) {
					try {
						me->m_gameStateDb->UpdatePlayer(pPO);
					} catch (KEPException* e) {
						LogError("UpdatePlayer(" << pPO->m_charName << ") CAUGHT EXCEPTION - " << e->m_msg);
						e->Delete();
						return EVENT_PROC_RC::NOT_PROCESSED;
					}
				}
			}

			if (me->m_gameStateDispatcher && updPlayerInventory) {
				LogInfo("Inventory Changed While Processing - REQUEUING - name=" << pPO->m_charName);
				(*ppIOL)->m_pendingRefresh = true; // drf - added
				rpbe->PrepareToReQueue((*ppIOL)->m_commitCount, pPO->m_cash->m_amount);
				me->m_gameStateDispatcher->QueueEvent(rpbe);
			}
		}
	}

	return EVENT_PROC_RC::OK;
}

///////////////////////////////////////////////////////////
/// final process in processing getting the player from the database
/// on initial spawn.  This is handler from event background db thread sent
EVENT_PROC_RC ServerEngine::GetPlayerBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	GetPlayerEvent* gpe = dynamic_cast<GetPlayerEvent*>(e);
	jsVerifyReturn(lparam != 0 && gpe != 0, EVENT_PROC_RC::NOT_PROCESSED);
	jsVerifyReturn(gpe->Filter() == (LONG)GetResultSetsEvent::PLAYER || gpe->Filter() == (LONG)GetResultSetsEvent::ACCOUNT, EVENT_PROC_RC::NOT_PROCESSED);
	ServerEngine* me = (ServerEngine*)lparam;

	if (gpe->ret == 2) {
		RenderLocaleTextEvent::SendLocaleTextTo(me->m_dispatcher, gpe->From(), eChatType::System, LS_LOGIN_LOGGED_ON);
		return EVENT_PROC_RC::NOT_PROCESSED;
	} else if (gpe->ret != 0) {
		// they will have logged the error already
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	auto pAO = me->m_multiplayerObj->m_accountDatabase->GetAccountObjectByNetId(gpe->From());
	CPlayerObject* pPO = NULL;

	if (!gpe->reconnected) {
		if (pAO == NULL) {
			LOG4CPLUS_WARN(m_logger, "Lost account during background processing playerId = " << gpe->ObjectId());
			pPO = dynamic_cast<CPlayerObject*>(gpe->item);
			jsVerifyReturn(pPO != 0, EVENT_PROC_RC::NOT_PROCESSED);
			pPO->SafeDelete();
			delete pPO;
			pPO = 0;
			return EVENT_PROC_RC::NOT_PROCESSED;
		}

		// find player this one is replacing
		for (POSITION posLoc = pAO->m_pPlayerObjectList->GetHeadPosition(); posLoc != NULL;) {
			CObject*& objRef = pAO->m_pPlayerObjectList->GetNext(posLoc);
			auto pPO_find = (CPlayerObject*)objRef;
			if (pPO_find->m_handle == gpe->ObjectId()) {
				pPO = dynamic_cast<CPlayerObject*>(gpe->item);
				if (pPO != pPO_find) { // same one! probably pre-loaded player
					pPO_find->SafeDelete();
					delete pPO_find;
				}
				jsVerifyReturn(pPO != 0, EVENT_PROC_RC::NOT_PROCESSED);
				objRef = pPO; // replace value in array
				pPO->m_pAccountObject = pAO;
				break;
			}
		}

		if (pPO == 0) {
			LOG4CPLUS_WARN(m_logger, "Lost player during background processing playerId = " << gpe->ObjectId());
			pPO = dynamic_cast<CPlayerObject*>(gpe->item);
			jsVerifyReturn(pPO != 0, EVENT_PROC_RC::NOT_PROCESSED);
			pPO->SafeDelete();
			delete pPO;
			pPO = 0;
			return EVENT_PROC_RC::NOT_PROCESSED;
		}
	} else {
		pPO = pAO->GetCurrentPlayerObject();
		if (!pPO) {
			LOG4CPLUS_WARN(m_logger, "Reconnected player not found playerId = " << gpe->ObjectId());
			return EVENT_PROC_RC::NOT_PROCESSED;
		}
	}

	// Delete this block ???
	// we'll be optimistic and queue up this event now.
	if (!gpe->broadcastListArray.empty()) {
		std::string msg = pPO->m_charName;
		msg += " has logged in.";

		for (std::vector<std::string>::iterator i = gpe->broadcastListArray.begin(); i != gpe->broadcastListArray.end(); i++) {
			if (!i->empty()) {
				ULONG filter = RenderTextEvent::PLAYER_LOG_IN;
				IEvent* pBroadcastToDistrEvent = me->m_dispatcher.MakeEvent(me->m_broadcastToDistrEventId);
				(*pBroadcastToDistrEvent->OutBuffer()) << msg.c_str() << i->c_str() << pPO->m_charName << (int)eChatType::System;
				pBroadcastToDistrEvent->SetFilter(filter);
				me->m_dispatcher.QueueEventInFutureMs(pBroadcastToDistrEvent, me->m_loginNotificationDelay * MS_PER_SEC);
				// queue a few secs in future to let spawn finish
			}
		}
	}

	if (!gpe->reconnected && gpe->overrideZone == INVALID_ZONE_INDEX) {
		// if they are to start in housing zone, reset it now
		if (me->m_multiplayerObj->m_alwaysStartInHousingZone && !pPO->m_aiControlled &&
			pPO->m_housingZoneIndex != INVALID_ZONE_INDEX) {
			CSpawnCfgObj* sc = me->m_multiplayerObj->GetSpawnPointCfg(pPO->m_housingZoneIndex.toLong(), 0);
			if (sc) {
				CWorldAvailableObj* wldRefObject = me->m_multiplayerObj->m_currentWorldsUsed->GetByZoneIndex(pPO->m_housingZoneIndex.zoneIndex());
				if (wldRefObject)
					pPO->m_currentWorldMap = wldRefObject->m_worldName;
				else
					LOG4CPLUS_WARN(m_logger, loadStr(IDS_BAD_HOME_SPAWN) << pPO->m_charName << " " << numToString(pPO->m_housingZoneIndex));

				gpe->overrideZone = pPO->m_housingZoneIndex;
				gpe->overrideX = sc->m_position.x;
				gpe->overrideY = sc->m_position.y;
				gpe->overrideZ = sc->m_position.z;
				gpe->rotation = sc->m_rotation;

				// make sure they can go there, which also updates db
				me->initiateSpawn(pPO, pAO, pPO->m_housingZoneIndex, sc->m_position, gpe->rotation, gpe->goingToPlayer ? DT_Player : DT_Zone, gpe->url.c_str(), true);
				return EVENT_PROC_RC::OK;
			} else {
				LOG4CPLUS_WARN(m_logger, loadStr(IDS_BAD_HOME_SPAWN) << pPO->m_charName << " " << numToString(pPO->m_housingZoneIndex));
			}
		}
	}

	ZoneIndex zi;

	// are we overriding the spawn point?
	if (gpe->overrideZone != INVALID_ZONE_INDEX) {
		// note we do allow override to go into arena, since else skips arena check
		pPO->m_currentZoneIndex = gpe->overrideZone;

		MovementData md = pPO->InterpolatedMovementData();
		md.pos = Vector3f(gpe->overrideX, gpe->overrideY, gpe->overrideZ);
		pPO->MovementInterpolatorInit(md);

		CWorldAvailableObj* wldRefObject = me->m_multiplayerObj->m_currentWorldsUsed->GetByZoneIndex(pPO->m_currentZoneIndex);
		if (!wldRefObject) {
			LOG4CPLUS_WARN(m_logger, "Error 533..Invalid world spawn Ref");
		} else {
			pPO->m_currentWorldMap = wldRefObject->m_worldName;
		}
		zi = pPO->m_currentZoneIndex;

		Vector3f pos = pPO->InterpolatedPos();
		me->initiateSpawn(
			pPO,
			pAO,
			pPO->m_currentZoneIndex,
			pos,
			gpe->rotation,
			gpe->goingToPlayer ? DT_ValidatedPlayer : DT_ValidatedZone,
			gpe->url.c_str(),
			true);

		return EVENT_PROC_RC::OK;
	}

	ChannelId idByName = me->m_multiplayerObj->m_currentWorldsUsed->AddWorldIfAvailable(pPO->m_currentWorldMap);
	if (me->m_worldChannelRuleDB->DoesThisRuleExistInThisChannel(idByName, CWordlChannelRuleObj::ARENA_AREA)) {
		// cannot reconnect to arena
		pPO->m_currentZoneIndex = pPO->m_respawnZoneIndex;

		MovementData md = pPO->InterpolatedMovementData();
		md.pos = pPO->m_respawnPosition;
		pPO->MovementInterpolatorInit(md);

		CWorldAvailableObj* wldRefObject = me->m_multiplayerObj->m_currentWorldsUsed->GetByZoneIndex(pPO->m_respawnZoneIndex);
		if (!wldRefObject) {
			LOG4CPLUS_WARN(m_logger, "Error 534..Invalid world spawn Ref");
		} else {
			pPO->m_currentWorldMap = wldRefObject->m_worldName;
		}

		LOG4CPLUS_WARN(m_logger, "Eject From Arena");
	}
	zi = pPO->m_currentZoneIndex;

	// reset the clan, group, housing zone index in case something
	// changed since they last logged on, or they exited in somone else's zone

	if (!gpe->reconnected)
		pPO->m_currentZoneIndex = pPO->createInstanceId(pPO->m_currentZoneIndex);

	zi = pPO->m_currentZoneIndex;

	Vector3f pos = pPO->InterpolatedPos();
	me->initiateSpawn(
		pPO,
		pAO,
		pPO->m_currentZoneIndex,
		pos,
		gpe->rotation,
		gpe->goingToPlayer ? DT_Player : DT_Zone,
		gpe->url.c_str(),
		true);

	return EVENT_PROC_RC::OK;
}

BOOL ServerEngine::AttribDeleteCharacterFromAccount(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	auto pAO = GetAccountObjectByNetId(netIdFrom);
	if (!pAO || !pAO->m_pPlayerObjectList)
		return FALSE;

	if (pAttrib->aeInt1 < 0) {
		LogWarn("Invalid Range 2004 delete char " << pAO->m_accountName);
		return FALSE;
	}

	POSITION cPos = pAO->m_pPlayerObjectList->FindIndex(pAttrib->aeInt1);
	if (!cPos) {
		LogWarn("Character does not exist on account (delete process):" << pAO->m_accountName);
		return FALSE;
	}

	auto pPO = (CPlayerObject*)pAO->m_pPlayerObjectList->GetAt(cPos);
	if (pAO->m_currentcharInUse != -1) {
		if (pPO)
			LogWarn(loadStr(IDS_ATTEMPT_TO_DELETE_CHAR_INUSE) << pAO->m_accountName << " " << pPO->m_charName);
		return FALSE;
	}

	try {
		if (m_gameStateDb != 0) {
			m_gameStateDb->DeletePlayer(pAO->m_serverId, pPO);
		}
	} catch (KEPException* e) {
		LogWarn(e->m_msg);
		e->Delete();
	}

	pPO->SafeDelete();
	delete pPO;
	pPO = 0;
	pAO->m_pPlayerObjectList->RemoveAt(cPos);

	LogWarn("Client has deleted player on his account :" << pAO->m_accountName);

	// send notification to client that deletion
	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::DeleteChar;
	attribData.aeInt1 = pAttrib->aeInt1;
	return m_multiplayerObj->QueueMsgAttribToClient(netIdFrom, attribData) ? TRUE : FALSE;
}

BOOL ServerEngine::AttribRequestCharList(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	return SendStringListToClient(pAttrib->aeInt1, netIdFrom);
}

BOOL ServerEngine::AttribSetInvenItemSwap(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	auto pAO = GetAccountObjectByNetId(netIdFrom);
	if (!pAO || !pAO->m_pPlayerObjectList)
		return FALSE;

	int charSelection = pAO->m_pPlayerObjectList->GetCount() - 1;
	if (charSelection < 0) {
		LogWarn("No characters created yet");
		return FALSE;
	}

	CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(pAttrib->aeInt2);
	if (!pGIO) {
		LogWarn("Global inventory item not found in AttribSetInvenItemSwap. GLID:" << pAttrib->aeInt2);
		return FALSE;
	}

	POSITION charPos = pAO->m_pPlayerObjectList->FindIndex(charSelection);
	if (charPos) {
		// valid char on account
		auto pPO = (CPlayerObject*)pAO->m_pPlayerObjectList->GetAt(charPos);
		if (pPO->m_inventory) {
			for (POSITION posLoc = pPO->m_inventory->GetHeadPosition(); posLoc != NULL;) {
				auto pIO = (CInventoryObj*)pPO->m_inventory->GetNext(posLoc);
				for (const auto& exID : pIO->m_itemData->m_exclusionGroupIDs) {
					if (exID == pAttrib->aeInt1) {
						if (pIO->isArmed()) {
							LogError("Resetting current inventory item with given glid. Suspect implementation. Analyze.");
							pIO->m_itemData = pGIO->m_itemData;
							return TRUE;
						}
					}
				}
			}
		}
	}

	return TRUE;
}

CPlayerObject* ServerEngine::GetValidatedPlayerObjectByNetTraceId(int netTraceId) {
	if (!m_multiplayerObj || !m_multiplayerObj->m_runtimePlayersOnServer || !m_gameStateDispatcher)
		return NULL;

	// Get Player Object By NetId
	auto pPO = GetPlayerObjectByNetTraceId(netTraceId);
	if (!pPO) {
		LogError("GetObjectByNetTraceId() FAILED - netId=" << netTraceId);
		return NULL;
	}
	std::string playerName = pPO->PlayerName();

	// Valid Player Account Object ?
	if (!pPO->m_pAccountObject) {
		LogError("playerObject<" << pPO << "> accountObject is null - " << playerName);
		return NULL;
	}

	// Valid Player Gift Credits ?
	if (!pPO->m_giftCredits) {
		LogError("playerObject<" << pPO << "> giftCredits is null - " << playerName);
		return NULL;
	}

	// Valid Player Cash ?
	if (!pPO->m_cash) {
		LogError("playerObject<" << pPO << "> cash is null - " << playerName);
		return NULL;
	}

	// Valid Player Inventory?
	if (!pPO->m_inventory) {
		LogError("playerObject<" << pPO << "> inventory is null - " << playerName);
		return NULL;
	}

	return pPO;
}

BOOL ServerEngine::AttribSetDeformableItem(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	// Valid Arguments ?
	if (!pAttrib || !m_multiplayerObj || !m_multiplayerObj->m_runtimeAccountDB) {
		LogError("Invalid Arguments");
		return FALSE;
	}

	// Get Player Account
	auto pAO = GetAccountObjectByNetId(netIdFrom);
	if (!pAO) {
		LogError("GetAccountObjectByNetId() FAILED - id=" << netIdFrom);
		return FALSE;
	}

	if (!pAO->m_pPlayerObjectList) {
		LogError("accountObject<" << pAO << "> charactersOnAccount is null");
		return FALSE;
	}

	int charsOnAccount = pAO->m_pPlayerObjectList->GetCount();
	if (charsOnAccount < 1) {
		LogError("accountObject<" << pAO << "> charactersOnAccount < 1");
		return FALSE;
	}

	POSITION charPos = pAO->m_pPlayerObjectList->FindIndex(charsOnAccount - 1);
	if (!charPos) {
		LogError("accountObject<" << pAO << "> charsOnAccount->FindIndex(" << (charsOnAccount - 1) << ") FAILED");
		return FALSE;
	}

	auto pPO = (CPlayerObject*)pAO->m_pPlayerObjectList->GetAt(charPos);
	int deformableMeshCount = (pPO ? pPO->m_baseMeshes : 0);
	if (deformableMeshCount <= pAttrib->aeInt1) {
		LogError("deformableMeshCount " << deformableMeshCount << "<=" << pAttrib->aeInt1);
		return FALSE;
	}

	pPO->m_charConfig.setItemSlotMeshId(GOB_BASE_ITEM, pAttrib->aeInt1, pAttrib->aeInt2);
	pPO->m_dbDirtyFlags |= PlayerMask::DEFCONFIG;

	return TRUE;
}

BOOL ServerEngine::AttribRequestCharInventory(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	// Valid Arguments ?
	if (!pAttrib) {
		LogError("pAttrib is null");
		return FALSE;
	}

	// Get Validated Player By NetId
	int netId = pAttrib->aeShort1;
	auto pPO = GetValidatedPlayerObjectByNetTraceId(netId);
	if (!pPO) {
		LogError("GetValidatedPlayerByNetId() FAILED - netId=" << netId);
		return FALSE;
	}
	std::string playerName = pPO->PlayerName();

	// Inventory Already Pending Refresh ? (happens all the time but good to know)
	if (pPO->m_inventory->m_pendingRefresh) {
		LogWarn("Inventory Refresh Pending - IGNORING REQUEST - " << playerName);
		return FALSE;
	}

	// Update Player Incase Outstanding Inventory Changes
	if (m_gameStateDb) {
		try {
			m_gameStateDb->UpdatePlayer(pPO);
		} catch (KEPException* e) {
			LogWarn("gameStateDB->UpdatePlayer(" << playerName << ") CAUGHT EXCEPTION - " << e->m_msg);
			e->Delete();
		}
	}

	// Start New Inventory Request
	pPO->m_inventory->m_pendingRefresh = true;

	// here we need to translate between the numeric client attribute sent and the server side IGameState
	ULONG updateFlags = PlayerMask::CASH;
	switch (pAttrib->aeType) {
		case eAttribEventType::GetInventoryBank:
			updateFlags |= PlayerMask::BANK_INVENTORY;
			break;
		case eAttribEventType::GetInventoryHouse:
			updateFlags |= PlayerMask::PLAYER_HOUSE_INVENTORY;
			break;
		case eAttribEventType::GetInventoryAll:
			updateFlags |= PlayerMask::INVENTORY | PlayerMask::BANK_INVENTORY | PlayerMask::PLAYER_HOUSE_INVENTORY;
			break;
		case eAttribEventType::GetInventoryPlayer:
		default:
			updateFlags |= PlayerMask::INVENTORY;
			break;
	}

	// pass to background to get it
	auto pAO = pPO->m_pAccountObject;
	m_gameStateDispatcher->QueueEvent(
		new RefreshPlayerBackgroundEvent(
			netIdFrom,
			playerName.c_str(),
			pPO->m_handle,
			pAO->m_serverId,
			pAttrib->aeInt1,
			pPO->m_inventory->m_commitCount,
			updateFlags,
			pPO->m_cash->m_amount,
			pAO->m_gm != FALSE));

	return TRUE;
}

//    return all attachable (use type = 11) inventory
BOOL ServerEngine::AttribRequestCharAttachableInventory(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	// Valid Arguments ?
	if (!pAttrib) {
		LogError("pAttrib is null");
		return FALSE;
	}

	// Get Validated Player By NetId
	int netId = pAttrib->aeShort1;
	auto pPO = GetValidatedPlayerObjectByNetTraceId(netId);
	if (!pPO) {
		LogError("GetValidatedPlayerByNetId() FAILED - netId=" << netId);
		return FALSE;
	}
	std::string playerName = pPO->PlayerName();

	if (m_gameStateDb) {
		try {
			pPO->m_cash->m_amount = m_gameStateDb->GetPlayersCash(pPO);
		} catch (KEPException* e) {
			LogWarn("gameStateDB->GetPlayersCash() CAUGHT EXCEPTION - " << playerName << " " << e->m_msg);
			e->Delete();
		}
	}

	SendMsgBlockItemToClient(
		pPO->m_inventory,
		FALSE,
		netIdFrom,
		pAttrib->aeInt1,
		pPO->m_cash,
		pPO,
		TRUE,
		11); //use type id 11 is object attachment
	return TRUE;
}

BOOL ServerEngine::AttribRequestBuyItemFromStore(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	LogInfo("Buying Item...");

	// Valid Arguments ?
	if (!pAttrib) {
		LogError("pAttrib is null");
		return FALSE;
	}

	// Get Validated Player By NetId
	int netTraceId = pAttrib->aeShort1;
	LogInfo("... netId=" << netTraceId);
	auto pPO = GetValidatedPlayerObjectByNetTraceId(netTraceId);
	if (!pPO) {
		LogError("GetValidatedPlayerObjectByNetTraceId() FAILED - netTraceId=" << netTraceId);
		return FALSE;
	}
	std::string playerName = pPO->PlayerName();
	LogInfo("... playerName=" << playerName);

	int glid = pAttrib->aeInt1;
	LogInfo("... glid=" << glid);

	int quantity = pAttrib->aeShort2;
	if (quantity < 0 || quantity >= 30000)
		quantity = 1;
	LogInfo("... quantity=" << quantity);

	CurrencyType currencyType = static_cast<CurrencyType>(pAttrib->aeInt2);

	Vector3f pos = pPO->InterpolatedPos();
	CCommerceObj* cmPtr = m_commerceDB->GetEstablishmentByPosition(pos, pPO->m_currentChannel);
	if (!cmPtr) {
		LogError("GetEstablishmentByPosition() FAILED");
		return FALSE;
	}

	// found store zone
	if (!cmPtr->m_storeInventory->VerifyInventoryGLID(glid)) {
		LogError("VerifyInventoryGLID() FAILED - glid=" << glid);
		return FALSE;
	}

	// store inventory verified
	CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(glid);
	if (!pGIO) {
		LogError("globalInventoryDB->GetByGLID() FAILED - glid=" << glid);
		return FALSE;
	}

	// if gift item, must be using gift, & vice-versa, IT_* is a bit, could be both
	USHORT itemType = pGIO->m_itemData->m_itemType;
	bool creditMatch = false || ((itemType & IT_GIFT) && (currencyType == CurrencyType::GIFT_CREDITS_ON_PLAYER)) || ((itemType & IT_NORMAL) && (currencyType == CurrencyType::CREDITS_ON_PLAYER));
	if (!creditMatch) {
		LogError("Credit Mismatch - glid=" << glid);
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_NO_MONEY);
		return FALSE;
	}

	if (!PassList::canPlayerBuyItem(pPO->m_passList, pGIO->m_itemData->m_passList)) {
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS);
		LogError("Can't buy due to missing pass - playerName=" << playerName << " glid=" << glid);
		return FALSE;
	}

	int slotsRequested = 1;
	if (!pGIO->m_itemData->m_stackable) {
		slotsRequested = quantity;
	} else {
		// stackable prevent overbuy
		CInventoryObj* itemObjTemp = (CInventoryObj*)pPO->m_inventory->GetByGLID(glid);
		if (itemObjTemp) {
			if (itemObjTemp->getQty() > 30000) {
				itemObjTemp->setQty(30000);
			}

			int allowedBuyingQuan = 30000 - itemObjTemp->getQty();
			if (quantity > allowedBuyingQuan)
				quantity = allowedBuyingQuan;
		} else {
			// none currently found in inventory
			if (quantity > 30000)
				quantity = 30000;
		}
	}

	// overloaded ?
	int maxItems = m_multiplayerObj->m_maxItemLimit_onChar;
	bool canTake = pPO->m_inventory->CanTakeObjects(maxItems, slotsRequested);
	if (!canTake) {
		LogError("CanTakeObjects() FAILED - maxItems=" << maxItems << " slotsRequested=" << slotsRequested);
		QueueMsgGeneralToClient(LS_GM_OVERLOADED, pPO);
		return FALSE;
	}

	// Validate Currency Type
	__int64 playercashCnv = 0;
	if (currencyType == CurrencyType::CREDITS_ON_PLAYER) {
		playercashCnv = pPO->m_cash->m_amount;
	} else if (currencyType == CurrencyType::GIFT_CREDITS_ON_PLAYER) {
		playercashCnv = pPO->m_giftCredits->m_amount;
	} else if (currencyType == (CurrencyType) 0) { // used to pass in this for Credits on player, backward compatiblity
		currencyType = CurrencyType::CREDITS_ON_PLAYER;
		playercashCnv = pPO->m_cash->m_amount;
	} else {
		LogError("User using wrong currency to buy item " << playerName << " " << (int) currencyType);
		return FALSE;
	}

	// Add Item To Player Inventory
	if (currencyType == CurrencyType::CREDITS_ON_PLAYER) {
		pPO->m_inventory->AddInventoryItem(quantity, FALSE, pGIO->m_itemData, IT_NORMAL);
	} else if (currencyType == CurrencyType::GIFT_CREDITS_ON_PLAYER) {
		pPO->m_inventory->AddInventoryItem(quantity, FALSE, pGIO->m_itemData, IT_GIFT);
	}

	// Update Player Cash
	__int64 quanCnv = pAttrib->aeShort2;
	__int64 mCostCnv = pGIO->m_itemData->m_marketCost;
	__int64 totalCostCnv = quanCnv * mCostCnv;
	int changingAmt = -(pGIO->m_itemData->m_marketCost * quantity);
	bool canAfford = false;
	if (m_gameStateDb == 0) {
		canAfford = (playercashCnv >= totalCostCnv);
		if (canAfford) {
			if (currencyType == CurrencyType::CREDITS_ON_PLAYER) {
				pPO->m_cash->m_amount += changingAmt;
			} else if (currencyType == CurrencyType::GIFT_CREDITS_ON_PLAYER) {
				pPO->m_giftCredits->m_amount += changingAmt;
			}
		}
	} else {
		canAfford = (UpdatePlayersCash(pPO, changingAmt, TransactionType::BOUGHT_ITEM, glid, quantity, currencyType) == IGameState::UPDATE_OK);
	}

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::PlayerCredits;

	// Player Can Afford Item ?
	if (!canAfford) {
		LogError("UpdatePlayersCash() FAILED - Canceling Transaction");

		if (currencyType == CurrencyType::CREDITS_ON_PLAYER) {
			// update client cash amount
			attribData.aeInt1 = pPO->m_cash->m_amount;
			m_multiplayerObj->QueueMsgAttribToClient(netIdFrom, attribData, pPO);
			pPO->m_inventory->RemoveRecentAddByGlid(pAttrib->aeInt1, pAttrib->aeShort2, FALSE, IT_NORMAL);
		} else if (currencyType == CurrencyType::GIFT_CREDITS_ON_PLAYER) {
			// update client gift credit amount
			attribData.aeInt1 = pPO->m_giftCredits->m_amount;
			m_multiplayerObj->QueueMsgAttribToClient(netIdFrom, attribData, pPO);
			pPO->m_inventory->RemoveRecentAddByGlid(pAttrib->aeInt1, pAttrib->aeShort2, FALSE, IT_GIFT);
		}

		QueueMsgGeneralToClient(LS_GM_STORE_NEED_MORE_CASH, pPO);
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_NO_MONEY);
		return FALSE;
	}

	// Update Client Cash Amounts
	if (currencyType == CurrencyType::CREDITS_ON_PLAYER) {
		// update client cash amount
		attribData.aeInt1 = pPO->m_cash->m_amount;
		m_multiplayerObj->QueueMsgAttribToClient(netIdFrom, attribData, pPO);
	} else if (currencyType == CurrencyType::GIFT_CREDITS_ON_PLAYER) {
		// update client gift credit amount
		attribData.aeInt1 = pPO->m_giftCredits->m_amount;
		m_multiplayerObj->QueueMsgAttribToClient(netIdFrom, attribData, pPO);
	}

	QueueMsgGeneralToClient(LS_GM_CANT_LOOT_PLAYER_LEVEL_LOW, pPO);

	// Log Item Bought
	std::string msg;
	StrBuild(msg, "Bought Item - playerName=" << playerName << " qty=" << quantity << " glid=" << glid << " cash=" << changingAmt);
	LogInfo(msg);

	return TRUE;
}

BOOL ServerEngine::AttribRequestSellItemToStore(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	LogInfo("Selling Item...");

	// Valid Arguments ?
	if (!pAttrib) {
		LogError("pAttrib is null");
		return FALSE;
	}

	// Get Validated Player By NetId
	int netTraceId = pAttrib->aeShort1;
	LogInfo("... netTraceId=" << netTraceId);
	auto pPO = GetValidatedPlayerObjectByNetTraceId(netTraceId);
	if (!pPO) {
		LogError("GetValidatedPlayerByNetId() FAILED - netTraceId=" << netTraceId);
		return FALSE;
	}
	std::string playerName = pPO->PlayerName();
	LogInfo("... playerName=" << playerName);

	int glid = pAttrib->aeInt1;
	LogInfo("... glid=" << glid);

	int quantity = pAttrib->aeShort2;
	if (quantity < 0 || quantity >= 30000)
		quantity = 1;
	LogInfo("... quantity=" << quantity);

	Vector3f pos = pPO->InterpolatedPos();
	CCommerceObj* cmPtr = m_commerceDB->GetEstablishmentByPosition(pos, pPO->m_currentChannel);
	if (!cmPtr) {
		LogError("GetEstablishmentByPosition() FAILED");
		return FALSE;
	}

	// Get Glid
	CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(glid);
	if (!pGIO) {
		LogError("globalInventoryDB->GetByGLID() FAILED - glid=" << glid);
		return FALSE;
	}

	// Cannot Sell Default Items
	if (pGIO->m_itemData->m_defaultArmedItem) {
		LogError("Cannot Sell Default Items");
		return FALSE;
	}

	// Remove Item From Player's Inventory
	int glidIO = pGIO->m_itemData->m_globalID;
	if (!pPO->m_inventory->DeleteByGLID(glidIO, quantity, true, IT_NORMAL)) {
		LogError("inventory->DeleteByGLID() FAILED - glid=" << glidIO);
		return FALSE;
	}

	// Update Player Cash
	long amountChanging = pGIO->m_itemData->m_sellingPrice * quantity;
	bool ok = (UpdatePlayersCash(pPO, amountChanging, TransactionType::SOLD_ITEM, glidIO, quantity) == IGameState::UPDATE_OK);
	if (!ok) {
		LogError("UpdatePlayersCash() FAILED - Canceling Transaction");
		pPO->m_inventory->AddInventoryItem(quantity, FALSE, pGIO->m_itemData, IT_NORMAL);
	}

	// Update Client Cash Amounts
	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::PlayerCredits;
	attribData.aeInt1 = pPO->m_cash->m_amount;
	m_multiplayerObj->QueueMsgAttribToClient(netIdFrom, attribData, pPO);

	QueueMsgGeneralToClient(LS_GM_CANT_LOOT_PLAYER_LEVEL_LOW, pPO);

	// Log Item Sold
	if (ok) {
		std::string msg;
		StrBuild(msg, "Sold Item - playerName=" << playerName << " qty=" << quantity << " glid=" << glid << " cash=" << amountChanging);
		LogInfo(msg);
	}

	return ok ? TRUE : FALSE;
}

BOOL ServerEngine::AttribRequestToBankItem(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	auto pPO = GetPlayerObjectByNetTraceId(pAttrib->aeShort1);
	if (!pPO) {
		LogWarn("Cannot find current player that is banking");
		return FALSE;
	}

	if (pAttrib->aeShort2 < 0 || pAttrib->aeShort2 >= 30000)
		pAttrib->aeShort2 = 1;

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("Error locating account 13");
		return FALSE;
	}

	auto pPO_cur = pAO->GetCurrentPlayerObject();
	if (!pPO_cur) {
		LogWarn("Cannot access base character on account " << pAO->m_accountName << " " << 14352);
		return FALSE;
	}

	if (pPO_cur->m_bankInventory->GetCount() >= m_multiplayerObj->m_maxItemLimit_inBank) {
		QueueMsgGeneralToClient(LS_GM_BANK_OVERLOADED, pPO); // overloaded
		return FALSE;
	}

	// remove from client inventory
	if (pPO->m_inventory) {
		POSITION posLast;
		for (POSITION posLoc = pPO->m_inventory->GetHeadPosition(); (posLast = posLoc) != NULL;) {
			CInventoryObj* pIO = (CInventoryObj*)pPO->m_inventory->GetNext(posLoc);
			if (pIO->m_itemData->m_defaultArmedItem && pAttrib->aeInt1 == pIO->m_itemData->m_globalID) {
				return FALSE;
			}
		}

		if (pPO->m_inventory->TransferInventoryObjsTo(
				pPO_cur->m_bankInventory,
				pAttrib->aeInt1, // glid
				pAttrib->aeShort2, // qty
				true, // must be unarmed
				pAttrib->aeInt2 // invType
				)) {
			pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY | PlayerMask::BANK_INVENTORY; // cash is determined interally

			// 10/08 changed to update db synchronously since web will delete
			// out of table so pending bank update will give it back to you.
			if (m_gameStateDb) {
				m_gameStateDb->UpdatePlayer(pPO);
			}

			QueueMsgGeneralToClient(LS_GM_CANT_LOOT_PLAYER_LEVEL_LOW, pPO);
		}
	}

	// add item to bank
	return TRUE;
}

BOOL ServerEngine::AttribRequestToOrganize2BankItems(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	auto pPO = GetPlayerObjectByNetTraceId(pAttrib->aeShort1);
	if (!pPO) {
		LogWarn("Cannot find current player that is banking");
		return FALSE;
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("Error locating account 13");
		return FALSE;
	}

	Vector3f pos = pPO->InterpolatedPos();
	CBankObj* bankPtr = m_bankZoneList->GetBankByPosition(pos, pPO->m_currentChannel);
	if (bankPtr) {
		// in bank zone
		auto pPO_cur = pAO->GetCurrentPlayerObject();
		if (!pPO_cur) {
			LogWarn("Cannot access base character on account " << pAO->m_accountName << " " << 14352);
			return FALSE;
		}

		if (pPO_cur->m_bankInventory) {
			if (pPO_cur->m_bankInventory->ReorderInventory(pAttrib->aeInt1, pAttrib->aeInt2, 1)) {
				pPO->m_dbDirtyFlags |= PlayerMask::BANK_INVENTORY;
				pPO->m_timeDbUpdate = TIME_UNDEFINED; // force update next pass
			}
		}
	} else {
		// possible house vault
		CHPlacementObj* housePtr = GetHousePlacementObjFromArea(pPO);
		if (housePtr) {
			if (housePtr->m_vaultInstalled) {
				if (housePtr->m_vaultInventory) {
					// house that you can access
					if (housePtr->m_vaultInventory->GetCount() >= housePtr->m_vaultItemCapacity) {
						QueueMsgGeneralToClient(LS_GM_BANK_OVERLOADED, pPO); // overloaded
						return FALSE;
					}

					if (housePtr->m_vaultInventory) {
						if (housePtr->m_vaultInventory->ReorderInventory(pAttrib->aeInt1, pAttrib->aeInt2, 1)) {
							if (housePtr->m_playerHandle == pPO->m_handle)
								pPO->m_dbDirtyFlags |= PlayerMask::PLAYER_HOUSE_INVENTORY;
							else
								pPO->m_dbDirtyFlags |= PlayerMask::CLAN_HOUSE_INVENTORY;
							pPO->m_timeDbUpdate = TIME_UNDEFINED; // force update next pass
						}
					}
				} // end house that you can access
			}
		}
	}

	return TRUE;
}

BOOL ServerEngine::AttribRequestToOrganize2InventoryItems(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	auto pPO = GetPlayerObjectByNetTraceId(pAttrib->aeShort1);
	if (!pPO) {
		LogWarn("Cannot find current player that is organizing inventory: " << pAttrib->aeShort1);
		return FALSE;
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("Error locating account in AttribRequestToOrganize2InventoryItems for " << pPO->m_charName);
		return FALSE;
	}

	if (pPO->m_inventory) {
		if (pPO->m_inventory->ReorderInventory(pAttrib->aeInt1, pAttrib->aeInt2, 1)) {
			pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY; // cash is determined interally
			pPO->m_timeDbUpdate = TIME_UNDEFINED; // force update next pass
		}
	}

	return TRUE;
}

BOOL ServerEngine::AttribTakeItemFromBank(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	if (pAttrib->aeShort2 < 0 || pAttrib->aeShort2 >= 30000)
		pAttrib->aeShort2 = 1;

	auto pPO = GetPlayerObjectByNetTraceId(pAttrib->aeShort1);
	if (!pPO) {
		LogWarn("Cannot find current player take item from bank ");
		return FALSE;
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("Error locating account 14");
		return FALSE;
	}

	auto pPO_cur = pAO->GetCurrentPlayerObject();
	if (!pPO_cur) {
		LogWarn("Cannot access base character on account " << pAO->m_accountName << " " << 14352);
		return FALSE;
	}

	if (pPO->m_inventory) {
		if (!pPO->m_inventory->CanTakeObjects(m_multiplayerObj->m_maxItemLimit_onChar, 1)) {
			QueueMsgGeneralToClient(LS_GM_OVERLOADED, pPO);
			return FALSE;
		}
	}

	// remove from bank inventory
	BOOL inventoryValid = FALSE;
	if (pPO_cur->m_bankInventory) {
		LogInfo("Moving object from the bank");

		if (pPO_cur->m_bankInventory->TransferInventoryObjsTo(
				pPO->m_inventory,
				pAttrib->aeInt1,
				pAttrib->aeShort2,
				true,
				pAttrib->aeInt2)) {
			pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY | PlayerMask::BANK_INVENTORY; // cash is determined interally

			// 10/08 changed to update db synchronously since web will delete
			// out of table so pending bank update will give it back to you.
			if (m_gameStateDb) {
				m_gameStateDb->UpdatePlayer(pPO);
			}

			QueueMsgGeneralToClient(LS_GM_CANT_LOOT_PLAYER_LEVEL_LOW, pPO);
		}

		inventoryValid = TRUE;
	}

	return TRUE;
}

#if (DRF_OLD_PORTALS == 1)
BOOL ServerEngine::AttribRequestPortal(ATTRIB_DATA* pAttrib, NETID netIdFrom) {
	auto pPO = GetPlayerObjectByNetId(netIdFrom, false);
	if (!pPO) {
		LogWarn("cannot locating player that is attempting portal ");
		return FALSE;
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("Cannot locate account of portal client");
		return FALSE;
	}

	if (pPO->m_aiControlled) {
		LogWarn("AI cannot portal:" << pAO->m_accountName << " " << 19);
		return FALSE;
	}

	CPortalSysObj* portal = m_portalDatabase->ReturnPortal(pPO->InterpolatedPos(), pPO->m_currentChannel);
	if (!portal) {
		QueueMsgGeneralToClient(LS_GM_REQUEST_PORTAL_CANCELLED, pPO); // silent cancellation
		return FALSE;
	}

	if (portal->m_keyRequired != 0) {
		if (!pPO->m_inventory->UseIfKeyInInventory(portal->m_keyRequired)) {
			QueueMsgGeneralToClient(LS_GM_DONT_HAVE_KEY, pPO);
			// has no key-"i have no dukes"
			return FALSE;
		}
	}

	if (portal->m_targetZoneIndex == INVALID_ZONE_INDEX || portal->m_targetZoneIndex.zoneIndex() < 0) {
		LogWarn("Invalid world Index negative:" << pAO->m_accountName << " " << 16);
		QueueMsgGeneralToClient(LS_GM_REQUEST_PORTAL_CANCELLED, pPO); // silent cancellation
		return FALSE;
	}

	CWorldAvailableObj* worldPtr = m_multiplayerObj->m_currentWorldsUsed->GetByZoneIndex(portal->m_targetZoneIndex);
	if (!worldPtr) {
		LogWarn("Invalid world Index:" << pAO->m_accountName << " " << 17);
		QueueMsgGeneralToClient(LS_GM_REQUEST_PORTAL_CANCELLED, pPO); // silent cancellation
		return FALSE;
	}

	auto pPO_cur = pAO->GetCurrentPlayerObject();
	if (!pPO_cur)
		return FALSE;

	pPO_cur->m_currentWorldMap = worldPtr->m_worldName;

	ZoneIndex newZoneIndex = pPO_cur->createInstanceId(portal->m_targetZoneIndex);
	if (newZoneIndex == INVALID_ZONE_INDEX)
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_CANT_SPAWN_THERE);
	else {
		// if portal has an instance id, honor it
		if (portal->m_targetZoneIndex.GetInstanceId() != 0) {
			newZoneIndex.setInstanceId(portal->m_targetZoneIndex.GetInstanceId());
		}

		int charIndex = pAO->m_currentcharInUse;

		//11/1/07 Fire event so GotoPlace will do instance and cross server checks for us.
		GotoPlaceEvent* gotoZonePortal = new GotoPlaceEvent();

		gotoZonePortal->GotoZone(newZoneIndex,
			newZoneIndex.GetInstanceId(),
			charIndex,
			portal->m_wldPosition.x,
			portal->m_wldPosition.y,
			portal->m_wldPosition.z,
			portal->m_rotation);

		gotoZonePortal->SetFrom(netIdFrom);
		m_dispatcher.QueueEvent(gotoZonePortal);
	}
	return TRUE;
}
#endif

/// process the player using an item
BOOL ServerEngine::AttribUseItem(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	/*
		pAttrib:
			uniInt = GLID
			uniInt2 = unused
			uniShort3 = inventoryType
	*/
	auto pPO = GetPlayerObjectByNetTraceId(pAttrib->aeShort1);
	if (!pPO) {
		LogWarn("Cannot find player that is using item");
		return FALSE;
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("Error locating account 16");
		return FALSE;
	}

	CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(pAttrib->aeInt1);
	if (!pGIO) {
		//NOTE: This works fine for now because refreshInventory is currently handling dynamic addition of
		// UGC funritures and UGC meshes from database. RefreshInventory will always be called before an
		// attrib use message will be sent from client because user can only use an item from inventory
		// menu. However, UI process can be always be changed to break this: for example, certain client script
		// automatically use a NEWLY purchased furniture without opening of inv menu. In such case, server
		// will hit this point and report an invalid GLID request. -Yanfeng 6/4/2009
		// P.S. might also apply to EquipInventoryItem function, which is for equipable items like clothing.

		LOG4CPLUS_WARN(m_logger, "Invalid GLID request: "
									 << pAO->m_accountName << " "
									 << pAttrib->aeInt1
									 << " " << 147);
		return FALSE;
	}

	// verify item is in inventory
	if (!pPO->m_inventory)
		return FALSE;

	short invType = IT_GIFT | IT_NORMAL;
	if (pAttrib->aeShort2 > 0)
		invType = pAttrib->aeShort2;

	CInventoryObj* pIO = pPO->m_inventory->GetByGLID(pAttrib->aeInt1, invType);
	if (pIO == 0 || // not found
		(pAttrib->aeShort2 != 0 && pIO->inventoryType() != pAttrib->aeShort2)) { // wrong type
		QueueMsgGeneralToClient(LS_GM_LACK_INVENTORY_ITEM, pPO);
		return FALSE;
	}

	// verify usable
	if (pGIO->m_itemData->m_useType == USE_TYPE_NONE) {
		return FALSE;
	}

	int resultDyn = 0;

	switch (pGIO->m_itemData->m_useType) {
		case USE_TYPE_NONE:
			break;

		case USE_TYPE_EQUIP:
		case USE_TYPE_HEALING:
		case USE_TYPE_PLACE_HOUSE:
		case USE_TYPE_INSTALL_HOUSE:
		case USE_TYPE_SUMMON_CREATURE:
		case USE_TYPE_SKILL:
			// DEPRECATED
			break;

		case USE_TYPE_MENU: {
			// request to call menu
			ATTRIB_DATA attribData;
			attribData.aeType = eAttribEventType::OpenMenu;
			attribData.aeInt2 = pGIO->m_itemData->m_miscUseValue;
			m_multiplayerObj->QueueMsgAttribToClient(idFromLoc, attribData, pPO);
		} break;

		case USE_TYPE_COMBINE: // request to combine if possible
			if (pPO->m_inventory) {
				resultDyn = pPO->m_inventory->CombineToExpObj(pAttrib->aeInt1, pGIO->m_itemData->m_miscUseValue);
				if (resultDyn != 1) {
					// failed for specific
					if (resultDyn == 8) {
						// already exists error-inform client
						QueueMsgGeneralToClient(LS_GM_CONTAINER_HAS_THIS, pPO);
					}

					if (resultDyn == 2) {
						// target item does not exixt-inform client
						QueueMsgGeneralToClient(LS_GM_LACK_COMBINE_ITEM, pPO);
					}

					if (resultDyn == 5) {
						// error no exp data on item tagged with this use
						LogWarn("error no exp data on item tagged with this EXP use");
					}

					if (resultDyn == 0) {
						// could not find item
						LogWarn("could not find item-EXP object use");
					}
				} // end failed for specific
				else {
					QueueMsgGeneralToClient(LS_GM_COMBINED_SUCESSFULLY, pPO);
				}
			}

			break;

		case USE_TYPE_ADD_DYN_OBJ: { // add dynamic object
			MovementData md = pPO->InterpolatedMovementData();

			Vector3f roundedVec;
			AngleRounding::roundYRotation(md.dir, roundedVec, 5.0f);
			float dx = roundedVec.x;
			float dy = roundedVec.y;
			float dz = roundedVec.z;
			Vector3f slide = Cross(md.up, md.dir);
			AngleRounding::roundYRotation(slide, roundedVec, 5.0f); // For future reference dropping objects
			dx = roundedVec.x;
			dy = roundedVec.y;
			dz = roundedVec.z;
			PlaceDynamicObjectAtPosition(pPO->m_netTraceId, pAttrib->aeInt1, IT_ALL, md.pos.x, md.pos.y, md.pos.z, dx, dy, dz);
		} break;

		case USE_TYPE_ADD_ATTACH_OBJ: { // add attachable object
			// only need to send in handle, and miscUseValue, and glid
			//pass in 0 as asset id as no custom texture is used for new objects
			if (pPO->m_currentZonePriv > ZP_MODERATOR) { // 4/07 only owners to avoid looting of another player's gifts via add/remove dyn obj
				LogError(loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
				SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
			} else {
				ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
				zce->type = ZoneCustomizationBackgroundEvent::ZC_ADD_FRAME;
				zce->playerId = pPO->m_handle;
				zce->charName = pPO->m_charName;
				zce->globalId = pAttrib->aeInt1;
				zce->inventorySubType = pIO->inventoryType();
				zce->channel = pPO->m_currentChannel;
				zce->zoneIndex = pPO->m_currentZoneIndex;
				zce->kanevaUserId = pAO->m_serverId;
				zce->SetFrom(idFromLoc);

				double x, y, z;
				x = pPO->GetNumber(GetPlayerLastClickedXId());
				y = pPO->GetNumber(GetPlayerLastClickedYId());
				z = pPO->GetNumber(GetPlayerLastClickedZId());

				if (x > (std::numeric_limits<float>::max)() ||
					y > (std::numeric_limits<float>::max)() ||
					z > (std::numeric_limits<float>::max)() ||
					x == std::numeric_limits<double>::infinity() ||
					y == std::numeric_limits<double>::infinity() ||
					z == std::numeric_limits<double>::infinity()) {
					LogDebug("Position out of range in ServerAddAttachableObjectEventHandler for " << pPO->m_charName << " " << x << "," << y << "," << z);
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::System, LS_USE_FRAME_ON_WALL);
					zce->Delete();
					return FALSE;
				}

				zce->x = (float)x;
				zce->y = (float)y;
				zce->z = (float)z;

				Vector3f direction;
				direction.x = pPO->GetNumber(GetPlayerLastClickedSurfaceNormalXId());
				direction.y = pPO->GetNumber(GetPlayerLastClickedSurfaceNormalYId());
				direction.z = pPO->GetNumber(GetPlayerLastClickedSurfaceNormalZId());

				if (x > (std::numeric_limits<float>::max)() ||
					y > (std::numeric_limits<float>::max)() ||
					z > (std::numeric_limits<float>::max)() ||
					direction.x == std::numeric_limits<double>::infinity() ||
					direction.y == std::numeric_limits<double>::infinity() ||
					direction.z == std::numeric_limits<double>::infinity()) {
					LogDebug("Rotation out of range in ServerAddAttachableObjectEventHandler for " << pPO->m_charName << " " << direction.x << "," << direction.y << "," << direction.z);
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::System, LS_USE_FRAME_ON_WALL);
					zce->Delete();
					return FALSE;
				}
				Vector3f up;
				up.x = 0;
				up.y = 1.f;
				up.z = 0;
				Vector3f slide = Cross(up, direction);
				zce->dx = (float)direction.x;
				zce->dy = (float)direction.y;
				zce->dz = (float)direction.z;
				zce->sx = (float)slide.x;
				zce->sy = (float)slide.y;
				zce->sz = (float)slide.z;

				if (!pPO->m_gm || (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0)) {
					// make sure they have passes to place it here
					PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
					// TODO: this is bad since iterates currentInv to do this check , then delete, need checks in DeleteByGlid
					CInventoryObj* invObj = pPO->m_inventory->GetByGLID(pAttrib->aeInt1, pAttrib->aeShort2);
					if (invObj && !PassList::canUseItemInZone(invObj->m_itemData->m_passList, zonePassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
						LogInfo("Can't place object due to missing pass " << pPO->m_charName << " " << pAttrib->aeInt1 << numToString(pPO->m_currentZoneIndex, " 0x%x"));
						zce->Delete();
						return FALSE;
					}

					if (!pPO->m_gm) {
						if (!pPO->m_inventory->DeleteByGLID(pAttrib->aeInt1, 1, true, pAttrib->aeShort2, false)) {
							LogWarn("Can't add attachable object since can't remove inventory item " << pPO->m_charName << " glid " << pAttrib->aeInt1);
							zce->Delete();
							return FALSE; // can't delete since armed, etc.
						}
						pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;
					}
				}

				zce->derivable = pGIO->m_itemData->m_derivable;
				m_gameStateDispatcher->QueueEvent(zce);
			}
		} break;

		case USE_TYPE_MOUNT: {
			//Player is using a mount (horse, dragon, tank, whatever), tell him which one to spawn
#ifndef REFACTOR_INSTANCEID
			MountEvent* mEvent = new MountEvent(pGIO->m_itemData->m_miscUseValue, pPO->m_currentZoneIndex);
#else
			MountEvent* mEvent = new MountEvent(pGIO->m_itemData->m_miscUseValue, pPO->m_currentZoneIndex.toLong());
#endif
			if (mEvent) {
				mEvent->AddTo(pPO->m_netId);
				m_dispatcher.QueueEvent(mEvent);
			}

		} break;

		case USE_TYPE_SOUND: {
			// only need to send in handle, and miscUseValue, and glid
			//pass in 0 as asset id as no custom texture is used for new objects
			if (pPO->m_currentZonePriv > ZP_MODERATOR) { // 4/07 only owners to avoid looting of another player's gifts via add/remove dyn obj
				LOG4CPLUS_ERROR(m_logger, loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
				SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
			} else {
				if (!pPO->m_gm || (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0)) {
					// make sure they have passes to place it here
					PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
					// TODO: this is bad since iterates currentInv to do this check , then delete, need checks in DeleteByGlid
					CInventoryObj* invObj = pPO->m_inventory->GetByGLID(pAttrib->aeInt1, pAttrib->aeShort2);
					if (invObj && !PassList::canUseItemInZone(invObj->m_itemData->m_passList, zonePassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
						LogInfo("Can't place object due to zone missing pass " << pPO->m_charName << " " << pAttrib->aeInt1 << numToString(pPO->m_currentZoneIndex, " 0x%x"));
						return FALSE;
					}

					if (!pPO->m_gm) {
						if (!pPO->m_inventory->DeleteByGLID(pAttrib->aeInt1, 1, true, pAttrib->aeShort2, false)) {
							LogWarn("Can't add dyn object since can't remove inventory item " << pPO->m_charName << " glid " << pAttrib->aeInt1);
							return FALSE; // can't delete since armed, etc.
						}
						pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;
					}
				}

				MovementData md = pPO->InterpolatedMovementData();

				ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
				zce->type = ZoneCustomizationBackgroundEvent::ZC_ADD_SOUND;
				zce->playerId = pPO->m_handle;
				zce->charName = pPO->m_charName;
				zce->globalId = pAttrib->aeInt1;
				zce->inventorySubType = pIO->inventoryType();
				zce->x = md.pos.x;
				zce->y = md.pos.y;
				zce->z = md.pos.z;
				zce->dx = md.dir.x;
				zce->dy = md.dir.y;
				zce->dz = md.dir.z;
				Vector3f slide = Cross(md.up, md.dir);
				zce->sx = slide.x;
				zce->sy = slide.y;
				zce->sz = slide.z;
				zce->channel = pPO->m_currentChannel;
				zce->zoneIndex = pPO->m_currentZoneIndex;
				zce->kanevaUserId = pAO->m_serverId;
				zce->SetFrom(idFromLoc);
				zce->derivable = pGIO->m_itemData->m_derivable;
				m_gameStateDispatcher->QueueEvent(zce);
			}
		} break;

		case USE_TYPE_PARTICLE: {
			// only need to send in handle, and miscUseValue, and glid
			//pass in 0 as asset id as no custom texture is used for new objects
			if (pPO->m_currentZonePriv > ZP_MODERATOR) { // 4/07 only owners to avoid looting of another player's gifts via add/remove dyn obj
				LogError(loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
				SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
			} else {
				if (!pPO->m_gm || (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0)) {
					// make sure they have passes to place it here
					PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
					// TODO: this is bad since iterates currentInv to do this check , then delete, need checks in DeleteByGlid
					CInventoryObj* invObj = pPO->m_inventory->GetByGLID(pAttrib->aeInt1, pAttrib->aeShort2);
					if (invObj && !PassList::canUseItemInZone(invObj->m_itemData->m_passList, zonePassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
						LogInfo("Can't place object due to zone missing pass " << pPO->m_charName << " " << pAttrib->aeInt1 << numToString(pPO->m_currentZoneIndex, " 0x%x"));
						return FALSE;
					}
				}

				ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
				zce->type = ZoneCustomizationBackgroundEvent::ZC_UPDATE_PARTICLE;
				zce->playerId = pPO->m_handle;
				zce->charName = pPO->m_charName;
				zce->globalId = pAttrib->aeInt1;
				zce->inventorySubType = IT_NORMAL;
				zce->objPlacementId = pAttrib->aeInt2;
				zce->channel = pPO->m_currentChannel;
				zce->zoneIndex = pPO->m_currentZoneIndex;
				zce->kanevaUserId = pAO->m_serverId;
				zce->SetFrom(idFromLoc);
				m_gameStateDispatcher->QueueEvent(zce);
			}
		} break;

		default:
			if (pGIO->m_itemData->m_useType >= USE_TYPE_FIRST_EVENT ||
				pGIO->m_itemData->m_useType == USE_TYPE_OLD_SCRIPT) {
				IEvent* e = m_dispatcher.MakeEvent(m_playerUseItemEventId);
				if (e != 0) {
					e->SetFilter((ULONG)pGIO->m_itemData->m_useType);
#ifndef REFACTOR_INSTANCEID
					e->SetObjectId((OBJECT_ID)pPO->m_currentChannel);
#else
					e->SetObjectId((OBJECT_ID)pPO->m_currentChannel.toLong());
#endif
					e->SetFrom(pPO->m_netId);
					(*e->OutBuffer()) << pPO->m_netTraceId
									  << pGIO->m_itemData->m_miscUseValue
									  << invType
									  << pAttrib->aeInt1
									  << (int)pGIO->m_itemData->m_useType;
					m_dispatcher.QueueEvent(e);

					if (pGIO->m_itemData->m_useType == USE_TYPE_PREMIUM) {
						// don't send the ITEM_USE_OK message to the player
						return TRUE;
					}
				} else {
					jsAssert(false);
				}
			}

			break;
	} // end use actions

	QueueMsgGeneralToClient(LS_GM_ITEM_USE_OK, pPO); // inform use ok

	return TRUE;
}

BOOL ServerEngine::PlaceDynamicObjectAtPosition(NETID idFromLoc, unsigned int glid, short invenType, float x, float y, float z, float dx, float dy, float dz) {
	LogDebug("PlaceDynamicObjectAtPosition: glid=" << glid << ", invenType=" << invenType);

	bool placingFromBank = false;
	auto pPO = GetPlayerObjectByNetTraceId(idFromLoc);
	if (!pPO) {
		LogWarn("PlaceDynamicObjectAtPosition: Cannot find player placing a dynamic object " << glid);
		return FALSE;
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("PlaceDynamicObjectAtPosition: Error locating account");
		return FALSE;
	}

	CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(glid);
	if (!pGIO) {
		LOG4CPLUS_WARN(m_logger, "PlaceDynamicObjectAtPosition: player " << pAO->m_accountName << " requested unknown item " << glid);
		QueueMsgGeneralToClient(LS_GM_LACK_INVENTORY_ITEM, pPO);
		return FALSE;
	}

	// verify item is in inventory
	if (!pPO->m_inventory)
		return FALSE;

	bool validated = true;
	CInventoryObj* pIO = pPO->m_inventory->GetByGLID(glid, invenType);
	if (pIO == 0 || // not found
		(invenType != IT_ALL && pIO->inventoryType() != invenType)) { // wrong type
		placingFromBank = true; // Look in bank for object
		if (pPO->m_bankInventory == NULL) {
			validated = false;
		} else {
			pIO = pPO->m_bankInventory->GetByGLID(glid, invenType);
			if (pIO == 0 || // not found
				(invenType != IT_ALL && pIO->inventoryType() != invenType)) { // wrong type
				validated = false;
			}
		}
	}

	if (!validated) {
		LogWarn("PlaceDynamicObjectAtPosition: player " << pAO->m_accountName << " does not own item " << glid << ", invType " << invenType);
		QueueMsgGeneralToClient(LS_GM_LACK_INVENTORY_ITEM, pPO);
		return FALSE;
	}

	// verify usable
	if (pGIO->m_itemData->m_useType == USE_TYPE_NONE) {
		return FALSE;
	}
	// only need to send in handle, and miscUseValue, and glid
	//pass in 0 as asset id as no custom texture is used for new objects
	if (pPO->m_currentZonePriv > ZP_MODERATOR) { // 4/07 only owners to avoid looting of another player's gifts via add/remove dyn obj
		LOG4CPLUS_ERROR(m_logger, "PlaceDynamicObjectAtPosition: " << loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
		SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
		return FALSE;
	} else {
		if (!pPO->m_gm || (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0)) {
			// make sure they have passes to place it here
			PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
			// TODO: this is bad since iterates currentInv to do this check , then delete, need checks in DeleteByGlid
			CInventoryObj* invObj = pPO->m_inventory->GetByGLID(glid, invenType);
			if (invObj && !PassList::canUseItemInZone(invObj->m_itemData->m_passList, zonePassList)) {
				RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
				LogInfo("Can't place object due to zone missing pass " << pPO->m_charName << " " << glid << numToString(pPO->m_currentZoneIndex, " 0x%x"));
				return FALSE;
			}
		}

		ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
		zce->type = ZoneCustomizationBackgroundEvent::ZC_ADD;
		zce->playerId = pPO->m_handle;
		zce->charName = pPO->m_charName;
		zce->globalId = glid;
		zce->inventorySubType = pIO->inventoryType();
		zce->x = x;
		zce->y = y;
		zce->z = z;
		////////// BEGIN CHANGES TO DROP ALL ITEMS ON ROTATION GRID ////////////////////
		// To remove the rounding set zce->rxy&z to currentPlayer->m_TargetDirVector
		zce->dx = dx;
		zce->dy = dy;
		zce->dz = dz;
		Vector3f up, dir;
		up.x = 0.0f;
		up.y = 1.0f;
		up.z = 0.0f;
		dir.x = dx;
		dir.y = dy;
		dir.z = dz;
		Vector3f slide = up.Cross(dir);
		slide.Normalize();
		zce->sx = slide.x;
		zce->sy = slide.y;
		zce->sz = slide.z;
		////////// END ROTATION CHANGES ////////////////////////////////////////////////
		zce->channel = pPO->m_currentChannel;
		zce->zoneIndex = pPO->m_currentZoneIndex;
		zce->kanevaUserId = pAO->m_serverId;
		zce->SetFrom(pAO->m_netId); // NETID requried for ZoneCustomizationBackgroundEvent
		zce->derivable = pGIO->m_itemData->m_derivable;
		m_gameStateDispatcher->QueueEvent(zce);
		return TRUE;
	}
}

BOOL ServerEngine::AttribTryOnItem(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	/*
		pAttrib:
			uniInt = GLID
			uniInt2 = unused
			uniShort3 = inventoryType
	*/
	auto pPO = GetPlayerObjectByNetId(idFromLoc, true);
	if (!pPO) {
		LogWarn("Cannot find player that is using item");
		return FALSE;
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("Error locating account 16");
		return FALSE;
	}

	CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(pAttrib->aeInt1);
	if (!pGIO) {
		LogInfo("Global inventory page fault on GLID. " << pAO->m_accountName << " "
														<< "GLID:" << pAttrib->aeInt1);

		if (m_gameStateDb) {
			try {
				pGIO = m_gameStateDb->addCustomItem(pAttrib->aeInt1);
				if (pGIO == NULL) {
					LogWarn("Global inventory page fault on GLID. Not found in DB. " << pAO->m_accountName << " "
																					 << "GLID:" << pAttrib->aeInt1);
					return FALSE;
				}
			} catch (KEPException* e) {
				LogWarn("Error getting item data for " << pPO->m_charName << " GLID:" << pAttrib->aeInt1 << " " << e->m_msg);
				e->Delete();
			}
		}
	} else {
		// Set default for baked in items until complete db read of inventory is implemented.

		if ((pGIO->m_itemData->m_expiredDuration == 0) && (pGIO->m_itemData->m_baseGlid == 0)) {
			pGIO->m_itemData->m_expiredDuration = m_multiplayerObj->m_tryOnItemDefaultExpirationDuration;
		}
	}

	if (!pGIO) {
		LogWarn("Invalid GLID request: " << pAO->m_accountName << " " << 148);
		return FALSE;
	}

	// verify item is in inventory
	if (!pPO->m_inventory) {
		return FALSE;
	}

	// verify usable
	if (pGIO->m_itemData->m_useType == USE_TYPE_NONE) {
		return FALSE;
	}

	switch (pGIO->m_itemData->m_useType) {
			// use actions

		case USE_TYPE_NONE: // NONE, Clothing
		case USE_TYPE_EQUIP:
			QueueMsgGeneralToClient(LS_GM_ITEM_USE_OK, pPO); // inform use ok
			if (pGIO->m_itemData->m_expiredDuration != 0) {
				// Send client expiration duration for the item.
				TryOnExpireEvent* expireEvent = new TryOnExpireEvent();
				expireEvent->InformExpiration(pPO->m_handle, pGIO->m_itemData->m_globalID, pGIO->m_itemData->m_baseGlid, pGIO->m_itemData->m_useType, pGIO->m_itemData->m_expiredDuration, 0 /*placementId*/);
				expireEvent->AddTo(idFromLoc);
				m_dispatcher.QueueEvent(expireEvent);
			}
			break;

		case USE_TYPE_SOUND: { // add sound object
			// only need to send in handle, and miscUseValue, and glid
			//pass in 0 as asset id as no custom texture is used for new objects
			if (pPO->m_currentZonePriv >= ZP_MODERATOR) { // 4/07 only owners to avoid looting of another player's gifts via add/remove dyn obj
				LogError(loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
				SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
			} else {
				if (!pPO->m_gm || (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0)) {
					PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
					if (pGIO && !PassList::canUseItemInZone(pGIO->m_itemData->m_passList, zonePassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
						LogInfo("Can't use object due to zone missing pass " << pPO->m_charName << " " << pAttrib->aeInt1 << numToString(pPO->m_currentZoneIndex, " 0x%x"));
						return FALSE;
					}
				}

				ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
				zce->type = ZoneCustomizationBackgroundEvent::ZC_ADD_SOUND;
				zce->playerId = pPO->m_handle;
				zce->charName = pPO->m_charName;
				zce->globalId = pAttrib->aeInt1;
				zce->inventorySubType = IT_NORMAL;

				MovementData md = pPO->InterpolatedMovementData();
				zce->x = md.pos.x;
				zce->y = md.pos.y;
				zce->z = md.pos.z;
				zce->dx = md.dir.x;
				zce->dy = md.dir.y;
				zce->dz = md.dir.z;

				Vector3f slide = Cross(md.up, md.dir);
				zce->sx = slide.x;
				zce->sy = slide.y;
				zce->sz = slide.z;
				zce->channel = pPO->m_currentChannel;
				zce->zoneIndex = pPO->m_currentZoneIndex;
				zce->kanevaUserId = pAO->m_serverId;
				zce->expiredDuration = pGIO->m_itemData->m_expiredDuration;
				zce->useType = (int)pGIO->m_itemData->m_useType;
				zce->SetFrom(idFromLoc);
				zce->tryon = true;
				zce->derivable = pGIO->m_itemData->m_derivable;
				m_gameStateDispatcher->QueueEvent(zce);
			}
		} break;

		case USE_TYPE_ADD_DYN_OBJ: { // add dynamic object
			// only need to send in handle, and miscUseValue, and glid
			//pass in 0 as asset id as no custom texture is used for new objects
			if (pPO->m_currentZonePriv >= ZP_MODERATOR) { // 4/07 only owners to avoid looting of another player's gifts via add/remove dyn obj
				LOG4CPLUS_ERROR(m_logger, loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
				SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
			} else {
				if (!pPO->m_gm || (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0)) {
					PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
					if (pGIO && !PassList::canUseItemInZone(pGIO->m_itemData->m_passList, zonePassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
						LogInfo("Can't use object due to zone missing pass " << pPO->m_charName << " " << pAttrib->aeInt1 << numToString(pPO->m_currentZoneIndex, " 0x%x"));
						return FALSE;
					}
				}

				MovementData md = pPO->InterpolatedMovementData();

				ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
				zce->type = ZoneCustomizationBackgroundEvent::ZC_ADD;
				zce->playerId = pPO->m_handle;
				zce->charName = pPO->m_charName;
				zce->globalId = pAttrib->aeInt1;
				zce->inventorySubType = IT_NORMAL;
				zce->x = md.pos.x;
				zce->y = md.pos.y;
				zce->z = md.pos.z;
				zce->dx = md.dir.x;
				zce->dy = md.dir.y;
				zce->dz = md.dir.z;
				Vector3f slide = Cross(md.up, md.dir);
				zce->sx = slide.x;
				zce->sy = slide.y;
				zce->sz = slide.z;
				zce->channel = pPO->m_currentChannel;
				zce->zoneIndex = pPO->m_currentZoneIndex;
				zce->kanevaUserId = pAO->m_serverId;
				zce->expiredDuration = pGIO->m_itemData->m_expiredDuration;
				zce->useType = (int)pGIO->m_itemData->m_useType;
				zce->SetFrom(idFromLoc);
				zce->tryon = true;
				zce->derivable = pGIO->m_itemData->m_derivable;
				m_gameStateDispatcher->QueueEvent(zce);
			}
		} break;

		case USE_TYPE_ADD_ATTACH_OBJ: { // add attachable object
			if (pPO->m_currentZonePriv >= ZP_MODERATOR) { // 4/07 only owners to avoid looting of another player's gifts via add/remove dyn obj
				LOG4CPLUS_ERROR(m_logger, loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
				SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
			} else {
				if (!pPO->m_gm || (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0)) {
					PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
					if (pGIO && !PassList::canUseItemInZone(pGIO->m_itemData->m_passList, zonePassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
						LogInfo("Can't place object due to missing pass " << pPO->m_charName << " " << pAttrib->aeInt1 << numToString(pPO->m_currentZoneIndex, " 0x%x"));
						return FALSE;
					}
				}

				MovementData md = pPO->InterpolatedMovementData();

				ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
				zce->type = ZoneCustomizationBackgroundEvent::ZC_ADD_FRAME;
				zce->playerId = pPO->m_handle;
				zce->charName = pPO->m_charName;
				zce->globalId = pAttrib->aeInt1;
				zce->inventorySubType = IT_NORMAL;
				zce->x = md.pos.x;
				zce->y = md.pos.y;
				zce->z = md.pos.z;
				zce->dx = md.dir.x;
				zce->dy = md.dir.y;
				zce->dz = md.dir.z;
				Vector3f slide = Cross(md.up, md.dir);
				zce->sx = slide.x;
				zce->sy = slide.y;
				zce->sz = slide.z;
				zce->channel = pPO->m_currentChannel;
				zce->zoneIndex = pPO->m_currentZoneIndex;
				zce->kanevaUserId = pAO->m_serverId;
				zce->expiredDuration = pGIO->m_itemData->m_expiredDuration;
				zce->baseGlid = pGIO->m_itemData->m_baseGlid;
				zce->useType = (int)pGIO->m_itemData->m_useType;
				zce->SetFrom(idFromLoc);
				zce->tryon = true;
				zce->derivable = pGIO->m_itemData->m_derivable;
				m_gameStateDispatcher->QueueEvent(zce);
			}
		} break;

		case USE_TYPE_PARTICLE: {
			// only need to send in handle, and miscUseValue, and glid
			//pass in 0 as asset id as no custom texture is used for new objects
			if (pPO->m_currentZonePriv >= ZP_MODERATOR) { // 4/07 only owners to avoid looting of another player's gifts via add/remove dyn obj
				LogError(loadStr(IDS_LOCALACCT_CANT_SETDYN) << pPO->m_charName);
				SendTextMessage(pPO, loadStr(IDS_LOCALACCT_CANT_SETDYN).c_str(), eChatType::System);
			} else {
				if (!pPO->m_gm || (m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() != 0)) {
					PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);
					if (pGIO && !PassList::canUseItemInZone(pGIO->m_itemData->m_passList, zonePassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
						LogInfo("Can't place particle due to missing pass " << pPO->m_charName << " " << pAttrib->aeInt1 << numToString(pPO->m_currentZoneIndex, " 0x%x"));
						return FALSE;
					}
				}

				ZoneCustomizationBackgroundEvent* zce = new ZoneCustomizationBackgroundEvent();
				zce->type = ZoneCustomizationBackgroundEvent::ZC_UPDATE_PARTICLE;
				zce->playerId = pPO->m_handle;
				zce->charName = pPO->m_charName;
				zce->globalId = pAttrib->aeInt1;
				zce->inventorySubType = IT_NORMAL;
				zce->objPlacementId = pAttrib->aeInt2;
				zce->channel = pPO->m_currentChannel;
				zce->zoneIndex = pPO->m_currentZoneIndex;
				zce->kanevaUserId = pAO->m_serverId;
				zce->baseGlid = pGIO->m_itemData->m_baseGlid;
				zce->useType = (int)pGIO->m_itemData->m_useType;
				zce->tryon = true;
				zce->SetFrom(idFromLoc);
				m_gameStateDispatcher->QueueEvent(zce);
			}
		} break;
	} // end use actions

	return TRUE;
}

BOOL ServerEngine::AttribCancelTransaction(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	auto pPO = GetPlayerObjectByNetTraceId(pAttrib->aeShort1);
	if (!pPO) {
		LogWarn("Cannot aquire current player inside cancel transaction");
		return FALSE;
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("Cannot locate account inside cancel transaction");
		return FALSE;
	}

	pPO->m_interactionStates &= ~TRADING;

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::CloseMenu;
	attribData.aeInt1 = 68;
	m_multiplayerObj->QueueMsgAttribToClient(pPO->m_netId, attribData, pPO);

	// inform other player that trade is canceled ?
	int otherClient = 0;
	if (m_secureTradeDB->CancelTransaction(pAttrib->aeShort1, &otherClient)) {
		auto pPO_dest = GetPlayerObjectByNetTraceId(otherClient);
		if (pPO_dest) {
			pPO_dest->m_interactionStates &= ~TRADING;
			m_multiplayerObj->QueueMsgAttribToClient(pPO_dest->m_netId, attribData, pPO_dest);
			RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO_dest->m_netId, eChatType::System, LS_TRADE_CANCELED);
		}
	}

	return TRUE;
}

BOOL ServerEngine::AttribAcceptTradeTerms(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	auto pPO = GetPlayerObjectByNetTraceId(pAttrib->aeShort1);
	if (!pPO) {
		LogWarn("Accept terms section could nor aquire involved runtime player");
		return FALSE;
	}

	int otherClient = 0;
	m_secureTradeDB->AcceptTradeConditions(pAttrib->aeShort1, &otherClient);
	if (otherClient != 0) {
		auto pPO_dest = GetPlayerObjectByNetTraceId(otherClient);
		if (pPO_dest)
			RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO_dest->m_netId, eChatType::System, LS_TRADE_ACCEPTED);
	}
	return TRUE;
}

BOOL ServerEngine::AttribDestroyItemByGlid(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	/*
		pAttrib->idRefrence = NETID of player
		pAttrib->uniInt = GLID to delete
		pAttrib->uniInt2 = qty (if > 0, number of items, if stackable.  If <=0, delete all items)
		pAttrib->uniShort3 = CInventoryObj's IT_* inventory type.
	*/
	auto pPO = GetPlayerObjectByNetTraceId(pAttrib->aeShort1);
	if (!pPO) {
		LogWarn("Cannot find current player to destroy item ");
		return FALSE;
	}

	CInventoryObj* pIO = pPO->m_inventory->GetByGLID(pAttrib->aeInt1);
	if (pIO) {
		if (pIO->m_itemData->m_defaultArmedItem) {
			return FALSE;
		}
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO)
		return FALSE;

	if (pPO->m_inventory) {
		if (pPO->m_inventory->DeleteByGLID(
				pAttrib->aeInt1,
				pAttrib->aeInt2,
				true,
				pAttrib->aeShort2))
			pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;
	}

	return TRUE;
}

BOOL ServerEngine::AttribDestroyBankItemByGlid(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	auto pPO = GetPlayerObjectByNetTraceId(pAttrib->aeShort1);
	if (!pPO) {
		LogWarn("Cannot find current player to destroy bank item ");
		return FALSE;
	}

	auto pAO = pPO->m_pAccountObject;
	if (!pAO)
		return FALSE;

	auto pPO_cur = pAO->GetCurrentPlayerObject();
	if (!pPO_cur) {
		LogWarn("Cannot access base character on account " << pAO->m_accountName << " " << 14352);
		return FALSE;
	}

	if (pPO_cur->m_bankInventory == NULL) {
		LogWarn("Cannot find bank for " << pPO->m_charName);
		return FALSE;
	}

	CInventoryObj* pIO = pPO_cur->m_bankInventory->GetByGLID(pAttrib->aeInt1);
	if (pIO) {
		if (pIO->m_itemData->m_defaultArmedItem) {
			return FALSE;
		}
	}

	if (pPO_cur->m_bankInventory->DeleteByGLID(
			pAttrib->aeInt1,
			pAttrib->aeInt2,
			true,
			pAttrib->aeShort2)) {
		pPO->m_dbDirtyFlags |= PlayerMask::BANK_INVENTORY;

		//Event sent for all banking activities
		QueueMsgGeneralToClient(LS_GM_CANT_LOOT_PLAYER_LEVEL_LOW, pPO);
	}

	return TRUE;
}

BOOL ServerEngine::AttribQuestHandling(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	auto pPO = GetPlayerObjectByNetTraceId(pAttrib->aeShort1);
	if (!pPO) {
		LogWarn("Invalid player request (request quest)");
		return FALSE;
	}

	CServerItemObj* pSIO = m_multiplayerObj->m_pServerItemObjMap->GetObjectByNetTraceId(pAttrib->aeInt2);
	if (pSIO) {
		// quest based
		if (!pSIO->m_collectionQuestDB || pAttrib->aeInt1 < 0) {
			LogWarn("No valid quests on AI");
			return FALSE;
		}

		int showSelectionCurrent = 0;
		CAICollectionQuestObj* questPtr = pSIO->m_collectionQuestDB->GetByAvailIndex(
			pAttrib->aeInt1,
			pPO->m_curEDBIndex,
			&showSelectionCurrent);
		if (!questPtr) {
			return FALSE;
		}

		HandleQuestSys(pPO, questPtr, pSIO, idFromLoc, showSelectionCurrent);
	} else {
		LogWarn("Requested server item AI Style but failed");
	}

	return TRUE;
}

BOOL ServerEngine::AttribSkillAbilityUse(CPlayerObject* pPO, int skillId, int level, int ability) {
	jsVerifyReturn(pPO != 0, FALSE);
	NETID idFromLoc = pPO->m_netId;

	auto pAO = pPO->m_pAccountObject;
	if (!pAO) {
		LogWarn("cannot locate account inside skill ability use");
		return FALSE;
	}

	if (pPO->m_aiControlled) {
		LogWarn("AI cannot use skills 911");
		return FALSE;
	}

	// does this require an item?
	if (!pAO->m_pPlayerObjectList) {
		LogWarn("No characters created yet 01");
		return FALSE;
	}

	auto pPO_cur = pAO->GetCurrentPlayerObject();
	if (!pPO_cur) {
		LogWarn("Invalid cur char-skill use");
		return FALSE;
	}

	CSkillObject* skill = m_skillDatabase->GetSkillByType(skillId);
	if (skill) {
		CLevelRangeObject* lro = NULL;
		POSITION p = skill->m_levelRangeDB->FindIndex(level);
		if (p)
			lro = (CLevelRangeObject*)skill->m_levelRangeDB->GetAt(p);
		if (lro && lro->m_abilityDB) { // 11/05 added lro->m_abilityDB check to avoid crash
			return FALSE;
		} //if lro
	} //if skill

	TimeMs timeMs = fTime::TimeMs();

	if (!pPO->m_skills)
		pPO->m_skills = new CSkillObjectList();

	int specialInstruction = 0;
	int miscInt = 0;
	BOOL updateStats = FALSE; // gl area
	CSkillObject* newAddedSkillRetPtr = NULL;
	CLevelRangeObject* levelUpObj = NULL;
	CSkillObject* skillPtrReference = NULL;
	int effectBind = 0;

	int result = SKILLRET_INVALID;

	if (m_hasUseSkillHandler) {
		m_hasUseSkillHandler = false; // avoid recursion if event handling calls back

		IEvent* e = new UseSkillEvent(pPO, m_skillDatabase,
			skillId, // type
			level, // skill level used
			ability, // ability
			6000, //m_multiplayerObj->m_manditoryWaitBeforeSkillUse,
			pPO->m_skillUseTimeStamp);

		if (m_dispatcher.ProcessSynchronousEventNoRelease(e) != 0) { // Keep the event
			(*e->InBuffer()) >> result;
		}
		e->Release(); // Release now

		m_hasUseSkillHandler = true;

		if (result == SKILLRET_SCRIPT_HANDLED) // did script handle it 100%
			return TRUE;
		else
			result = SKILLRET_INVALID;
	}

	if (result == SKILLRET_INVALID) // handled by event?
		result = pPO->m_skills->UseSkillByType(skillId, // int type,
			m_skillDatabase,
			0, // expOfTarget,
			level, // skill level used
			ability, // ability index
			pPO->m_dbCfg,
			pPO->m_inventory,
			&specialInstruction,
			&newAddedSkillRetPtr,
			pPO->m_stats,
			&updateStats,
			&levelUpObj,
			&skillPtrReference,
			&miscInt,
			m_multiplayerObj->m_maxItemLimit_onChar,
			&effectBind,
			6000, //m_multiplayerObj->m_manditoryWaitBeforeSkillUse,
			pPO->m_skillUseTimeStamp);
	if (result == SKILLRET_TOO_SOON) {
		// too soon to use this skill
		QueueMsgGeneralToClient(LS_GM_CANT_USE_SKILL_YET, pPO);
		return FALSE;
	} // end too soon to use this skill

	if (result == SKILLRET_TOO_SOON_SKILL) {
		// too soon to use this skill
		QueueMsgGeneralToClient(LS_GM_SKILL_TIMER_NOT_MET, pPO);
		return FALSE;
	} // end too soon to use this skill

	pPO->m_skillUseTimeStamp = timeMs;

	if (result == SKILLRET_SUCCESS) {
		// success
		pPO->m_dbDirtyFlags |= PlayerMask::SKILLS;
		if (updateStats)
			pPO->m_dbDirtyFlags |= PlayerMask::STATS;
		QueueMsgGeneralToClient(LS_GM_ATTEMPT_SUCCEEDED, pPO);
	} else if (result == SKILLRET_ITEM_NOT_PRESENT) {
		QueueMsgGeneralToClient(LS_GM_ITEM_NOT_PRESENT, pPO);
	} else if (result == SKILLRET_LACK_THE_COMPONENT) {
		QueueMsgGeneralToClient(LS_GM_LACK_COMPONENT, pPO);
	} else if (result == SKILLRET_TOO_MUCH_INVENTORY) {
		QueueMsgGeneralToClient(LS_GM_OVERLOADED, pPO);
	} else if (result == SKILLRET_UNSUCCESSFUL) {
		QueueMsgGeneralToClient(LS_GM_FAILED_ATTEMPT, pPO);
	} else if (result == SKILLRET_LACK_SKILL) {
		QueueMsgGeneralToClient(LS_GM_LACK_SKILL, pPO);
	}

	if (newAddedSkillRetPtr) {
		// Notify new skill added
		std::string msgBuild;
		CSkillObject* skillPtrRef = m_skillDatabase->GetSkillByType(newAddedSkillRetPtr->m_skillType);
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::Talk, LS_SKILL_BEGIN, skillPtrRef ? skillPtrRef->m_skillName : "");
	}

	if (skillPtrReference) {
		if (levelUpObj != NULL) {
			// notify leveled up
			int levelNumber = 1;
			CStringA newTitle;

			CSkillObject* skillPtrRef = m_skillDatabase->GetSkillByType(skillPtrReference->m_skillType);
			if (skillPtrRef) {
				skillPtrReference->EvaluateLevelAndTitle(newTitle, levelNumber, skillPtrRef);

				RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, idFromLoc, eChatType::Talk, LS_SKILL_NEW_LEVEL,
					StringHelpers::parseNumber(levelNumber).c_str(),
					skillPtrRef->m_skillName);
			}
		} // end notify leveled up
	}

	return TRUE;
}

BOOL ServerEngine::AttribBuyItemFromHouseStore(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	auto pPO = GetPlayerObjectByNetId(idFromLoc, false);
	if (!pPO || !pPO->m_inventory)
		return FALSE;

	CHPlacementObj* pHPO = m_multiplayerObj->m_housingDB->GetHouseByPositionAndWldID(pPO->InterpolatedPos(), pPO->m_currentChannel);
	if (!pHPO) {
		LogWarn("Cannot find house config " << pAttrib->aeInt1);
		return FALSE;
	}

	if (!pHPO->m_storeInventory) {
		// has no store-inform player
		return FALSE;
	}

	if (pHPO->m_storeInventory->GetCount() < 1) {
		// must have somthing to send
		return FALSE;
	}

	CInventoryObj* pIO = pHPO->m_storeInventory->GetByGLID(pAttrib->aeInt1); // verify quanity and availability
	if (!pIO) {
		// not available
		// inform Client
		return FALSE;
	} // end not available

	if (!PassList::canPlayerBuyItem(pPO->m_passList, pIO->m_itemData->m_passList)) {
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS);
		LogInfo("Can't buy due to missing pass " << pPO->m_charName << " " << pAttrib->aeInt1);
		return FALSE;
	}

	// truncate quanity
	if (pAttrib->aeShort2 < 1)
		pAttrib->aeShort2 = 1;

	if (pAttrib->aeShort2 > pIO->getQty())
		pAttrib->aeShort2 = pIO->getQty(); // truncate to available stock

	// figure costs
	int playerCash = 0;
	if (!pPO->m_cash)
		return FALSE;

	playerCash = pPO->m_cash->m_amount;

	int totalCost = ((int)pAttrib->aeShort2) * pIO->m_itemData->m_marketCost;
	__int64 playercashCnv = pPO->m_cash->m_amount;
	__int64 quanCnv = pAttrib->aeShort2;
	__int64 mCostCnv = pIO->m_itemData->m_marketCost;
	__int64 totalCostCnv = quanCnv * mCostCnv;

	if (playercashCnv < totalCostCnv) {
		// cannot afford this-inform client
		QueueMsgGeneralToClient(LS_GM_STORE_NEED_MORE_CASH, pPO);
		return FALSE;
	} // end cannot afford this-inform client

	// transaction, will update cash+inventory as needed
	bool canAffordIt = UpdatePlayersCash(pPO, -totalCost, TransactionType::BOUGHT_HOUSE_ITEM, pAttrib->aeInt1, pAttrib->aeShort2) == IGameState::UPDATE_OK;

	if (canAffordIt) {
		pHPO->m_storeCurrentCash += totalCost;
		pHPO->m_storeInventory->TransferInventoryObjsTo(pPO->m_inventory, pAttrib->aeInt1, pAttrib->aeShort2);
	}

	ATTRIB_DATA attribData;
	attribData.aeType = eAttribEventType::PlayerCredits;
	attribData.aeInt1 = pPO->m_cash->m_amount;
	m_multiplayerObj->QueueMsgAttribToClient(idFromLoc, attribData, pPO);

	return TRUE;
}

BOOL ServerEngine::AttribAddItemToHouseStore(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	auto pPO = GetPlayerObjectByNetId(idFromLoc, false);
	if (!pPO || !pPO->m_inventory)
		return FALSE;

	CHPlacementObj* pHPO = m_multiplayerObj->m_housingDB->GetHousePlacementByPosPlayer(pPO->InterpolatedPos(), pPO->m_handle);
	// must be owner to do this
	if (!pHPO) {
		LogWarn("Cannot find house config " << pAttrib->aeInt1);
		return FALSE;
	}

	if (!pHPO->m_storeInventory) {
		// has no store-inform player
		return FALSE;
	}

	CInventoryObj* pIO = pPO->m_inventory->GetByGLID(pAttrib->aeInt1); // verify quanity and availability
	if (!pIO) {
		// not available
		// inform Client
		return FALSE;
	} // end not available

	if (pIO->m_itemData->m_nonTransferable) {
		return FALSE;
	}

	// check for shelf space
	if (pHPO->m_storeItemCapacity < pHPO->m_storeInventory->GetCount()) {
		// possibly too many items
		if (pIO->m_itemData->m_stackable) {
			// check to see if we can stack before not allowing additional items of tis type
			CInventoryObj* pIO_test = pHPO->m_storeInventory->GetByGLID(pAttrib->aeInt1); // veify theres sopmthing to stack onto
			if (!pIO_test) {
				QueueMsgGeneralToClient(LS_GM_STORE_INVENTORY_FULL, pPO);
				return FALSE;
			}
		} // endd check to see if we can stack before not allowing additional items of tis type
		else {
			// overloaded
			QueueMsgGeneralToClient(LS_GM_STORE_INVENTORY_FULL, pPO);
			return FALSE;
		} // overloaded
	} // end possibly too many items

	if (pAttrib->aeShort2 < 1)
		pAttrib->aeShort2 = 1;

	if (pAttrib->aeShort2 > pIO->getQty())
		pAttrib->aeShort2 = pIO->getQty();

	if (pAttrib->aeShort2 > 30000)
		pAttrib->aeShort2 = 30000;

	if (pAttrib->aeInt2 < 1)
		pAttrib->aeInt2 = 1;

	if (pAttrib->aeInt2 > 9000000)
		pAttrib->aeInt2 = 9000000;

	pPO->m_inventory->TransferInventoryObjsTo(pHPO->m_storeInventory, pAttrib->aeInt1, pAttrib->aeShort2);
	pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;
	return TRUE;
}

BOOL ServerEngine::AttribRemoveItemFromHouseStore(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	auto pPO = GetPlayerObjectByNetId(idFromLoc, false);
	if (!pPO || !pPO->m_inventory)
		return FALSE;

	CHPlacementObj* pHPO = m_multiplayerObj->m_housingDB->GetHousePlacementByPosPlayer(pPO->InterpolatedPos(), pPO->m_handle);
	// must be owner to do this
	if (!pHPO) {
		LogWarn("Cannot find house config " << pAttrib->aeInt1);
		return FALSE;
	}

	if (!pHPO->m_storeInventory)
		return FALSE;

	CInventoryObj* pIO = pHPO->m_storeInventory->GetByGLID(pAttrib->aeInt1); // verify quanity and availability
	if (!pIO)
		return FALSE;

	if (pAttrib->aeShort2 < 1)
		pAttrib->aeShort2 = 1;

	if (pAttrib->aeShort2 > pIO->getQty())
		pAttrib->aeShort2 = pIO->getQty();

	if (pHPO->m_storeInventory->TransferInventoryObjsTo(pPO->m_inventory, pAttrib->aeInt1, pAttrib->aeShort2))
		pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;

	return TRUE;
}

BOOL ServerEngine::AttribRemoveCashFromHouseStore(ATTRIB_DATA* pAttrib, NETID idFromLoc) {
	auto pPO = GetPlayerObjectByNetId(idFromLoc, false);
	if (!pPO || !pPO->m_inventory)
		return FALSE;

	CHPlacementObj* pHPO = m_multiplayerObj->m_housingDB->GetHousePlacementByPosPlayer(pPO->InterpolatedPos(), pPO->m_handle);
	// must be owner to do this
	if (!pHPO) {
		LogWarn("Cannot find house config " << pAttrib->aeInt1);
		return FALSE;
	}

	if (!pHPO->m_storeInventory)
		return FALSE;

	if (pPO->m_cash) {
		UpdatePlayersCash(pPO, pHPO->m_storeCurrentCash, TransactionType::HOUSE_BANKING);

		pHPO->m_storeCurrentCash = 0;
		QueueMsgGeneralToClient(LS_GM_STORE_TRANS_COMPLETE, pPO);

		ATTRIB_DATA attribData;
		attribData.aeType = eAttribEventType::PlayerCredits;
		attribData.aeInt1 = pPO->m_cash->m_amount;
		m_multiplayerObj->QueueMsgAttribToClient(idFromLoc, attribData, pPO);
	}

	return TRUE;
}

bool ServerEngine::VerifyStatRequirement(CPlayerObject* pPO, CStatBonusList* requiredStats) {
	bool ret = false;
	IEvent* e = new VerifyStatEvent(pPO, requiredStats);
	if (m_dispatcher.ProcessSynchronousEventNoRelease(e) != 0) { // Keep the event
		(*e->InBuffer()) >> ret;
	} else {
		ret = pPO->VerifyStatRequirement(requiredStats);
	}
	e->Release(); // Release now

	return ret;
}

} // namespace KEP