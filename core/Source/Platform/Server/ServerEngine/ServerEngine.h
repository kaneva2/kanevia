///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "common\include\KEPCommon.h"
#include "common/keputil/MongooseHTTPServer.h"

#include "Core/Util/ThreadPool.h"

class CEntityPropertiesCtrl {
public:
	enum { MODEL_TYPE_DISABLED = 99 };
};

#include "BladeFactory.h"
#include "Engine.h"
#include "ISender.h"
#include "Event\ServerEvents.h"
#include "IPlayer.h"
#include "InstanceId.h"
#include "IServerEngine.h"
#include "IGameState.h"
#include "Server/Database/MySqlUtil.h"
#include "Server/Database/GameStateDispatcher.h"
#include "CpuMonitor.h"
#include "TransactionType.h"

namespace KEP {

class IEventHandler;
class CAICollectionQuestObj;
class CAIRaidObj;
class CAIRaidObjList;
class CBankObjList;
class CBattleSchedulerObj;
class CBattleSchedulerList;
class CCollisionObjList;
class CCommerceObjList;
class CCurrencyObj;
class CHPlacementObj;
struct ItemAnimation;
struct PlayerOutfit;
class CSafeZoneObjList;
class CSecureTradeObjList;
class CServerItemGenObj;
class CServerItemObj;
class CSpawnCfgObj;
class CStatBonusList;
class CWorldChannelRuleList;

#if (DRF_OLD_VENDORS == 1)
class CServerItemGenObjList;
#endif
#if (DRF_OLD_PORTALS == 1)
class CPortalSysList;
#endif
class CGmPageList;
class GotoPlaceBackgroundEvent;
class MetricsAgent;
class ScriptClientEvent;

// for upgrading to another zone for apt or channel zones
const LONG ApartmentUpgrade = 1;
const LONG ChannelUpgrade = 2;

class SoundRESTHandler : public HTTPServer::RequestHandler {
public:
	ServerEngine& _server;
	SoundRESTHandler(ServerEngine& server);

	int handle(const HTTPServer::Request& request, HTTPServer::EndPoint& endPoint) override;
};

// DRF - IServerEngine must be inherited first otherwise ObjectRegistry::~ObjectHolder will crash on delete
class ServerEngine : public IServerEngine, public Engine, public ISender {
public:
	ServerEngine();
	~ServerEngine(void);

	int GetGameId() const; // override get game ID

	//! bits values to pass into UpdateItemInAllInventories
	static const int PASSLIST_UPDATE = 1;
	static const int DATA_UPDATE = 2;
	static const int DELETE_ITEM = 4;

	static ULONG BladeVersion() { return PackVersion(1, 0, 0, 0); }
	virtual Monitored::DataPtr monitor() const;

	//1 overrides from IEngine
	virtual bool LoadConfigValues(const IKEPConfig* cfg) override;
	virtual void SaveConfigValues(IKEPConfig* cfg) override;
	virtual bool Initialize(const char* baseDir, const char* configFile, const DBConfig& dbConfig) override;
	virtual bool RenderLoop(bool canWait = true) override;
	virtual void FreeAll() override;

	void KickEveryoneExcept(const ZoneIndex& zi, const ChannelId& channel, NETID netId, UINT id);

	CAccountObject* GetAccountObjectByNetId(const NETID& netId) const;

	CPlayerObject* GetPlayerObjectByServerId(LONG playerServerId) const;
	CPlayerObject* GetPlayerObjectByName(const std::string& userName) const;
	CPlayerObject* GetPlayerObjectByNetTraceId(int netTraceId) const;
	CPlayerObject* GetPlayerObjectByNetId(const NETID& netId, bool includeSpawning) const;
	CPlayerObject* GetPlayerObjectByPlayerHandle(const PLAYER_HANDLE& handle) const;
	CPlayerObject* GetPlayerObjectByAliveAndAccountNumber(int accNum) const;

	// Script Glue Only (GameGlue.cpp)
	IPlayer* GetPlayerByName(const std::string& userName) override;
	IPlayer* GetPlayerByNetId(const NETID& netId, bool includeSpawning) override;
	IPlayer* GetPlayerByNetTraceId(int netTraceId) override;
	IPlayer* GetPlayerOffAccountByNetId(NETID id, int index);

	//! export global inventory hidden by Engine
	CGlobalInventoryObjList* GlobalInventoryDB() {
		return m_globalInventoryDB;
	}

	void UpdateItemInAllInventories(CItemObj* obj, int updateType);

	ACTPtr GetSound(IDispatcher* disp, NETID requestor, int zoneIndex, int instanceId);

	void UpdateSound(int clearedZoneIndex, int clearedInstanceId, int objectId, int glid, int pitch, int gain, int max_dist, int roll_off, int loop, int loop_delay, int cone_outer_gain, int cone_inner_angle, int cone_outer_angle, int dir_x, int dir_y, int dir_z);

	void RemoveSound(int objectId);

#ifdef FIX_MTQUEUE
	bool m_optimizeForSingleThread; // ???
#endif

protected:
	BOOL InitGameStates(const char* ip, int port);
	BOOL WaitForNextPacket(ULONG msToWait);

	short CheckGender(CAccountObject* acct, short edb_index);

	virtual void Callback() override;

	// Message Senders (overidden)
	virtual bool SendMsgUpdateToClient(CPlayerObject* runtimePtr, bool sendForce) override;
	virtual bool SendMsgMoveToClient(CPlayerObject* pPO, bool sendForce) override;
	virtual bool QueueMsgEquipToClient(const std::vector<GLID>& glids, long netTraceId, IPlayer* pPlayer) override;
	virtual bool QueueMsgEquipToAllClients(IPlayer* pPlayer, const std::vector<GLID>& glids) override;

	// Message Senders (not overidden)
	bool QueueMsgGeneralToClient(unsigned char msgType, CPlayerObject* pPO);
	bool QueueMsgSpawnStaticMOToClient(NETID sendTo, CServerItemObj* pSIO);
#if (DRF_OLD_VENDORS == 1)
	bool SendMsgBlockStoreToClient(int storeID, CPlayerObject* pPO);
#endif

	// Message Handlers
	virtual bool HandleMsgs(BYTE* pData, NETID netIdFrom, size_t dataSize, bool& bDisposeMessage) override;
	bool HandleMsgEvent(BYTE* pData, size_t dataSize, NETID netIdFrom, bool async);
	bool HandleMsgLogToServer(BYTE* pData, NETID netIdFrom);
	bool HandleMsgAttribToServer(BYTE* pData, size_t dataSize, NETID netIdFrom);
	bool HandleMsgAttrib(BYTE* pData, NETID netIdFrom);
	bool HandleMsgSpawnToServer(BYTE* pData, NETID netIdFrom);
	bool HandleMsgUpdateToServer(BYTE* pData, NETID netIdFrom);
	bool HandleMsgMoveToServer(BYTE* pData, size_t dataSize, NETID netIdFrom);
	bool HandleMsgCreateCharToServer(BYTE* pData, size_t dataSize, NETID netIdFrom);
	bool HandleMsgEquipToServer(BYTE* pData, NETID netIdFrom);

	void FireGetAttributeEvent(const ATTRIB_DATA& attribData, NETID netIdFrom);

	// Sends Player Inventory
	BOOL SendMsgBlockItemToClient(
		CInventoryObjList* inventory,
		BOOL filterNonTranferable,
		NETID sendTo,
		int visualID,
		CCurrencyObj* cash,
		CPlayerObject* currentPlayer,
		BOOL includeArmed,
		int useTypeId = -1); //set use type id to -1 to return all types of items

	virtual bool ProcessScriptAttribute(CEXScriptObj* sctPtr) override;

private:
	SoundRESTHandler m_soundRESTHandler;

	CHPlacementObj* GetHousePlacementObjFromArea(CPlayerObject* playerObj);
	CSpawnCfgObj* GetSpawnPtr(int spawnCfgIndex);

	virtual bool DisconnectPlayer(NETID playerId, const char* optionalLogMsg = 0) override;
	void DisconnectPlayerAndLog(CPlayerObject* player, CStringA errorMsg);
	BOOL GetFirstCollisionY(int cId, Vector3f currentPosition, float yDist, Vector3f* returnPosition);

	void RespawnToRebirthPoint(CPlayerObject* pPO);

	BOOL EnableServer();

	bool ChangeZoneMap(CPlayerObject* pPO, LONG inventoryItemId, LONG apartmentId, LONG changeType, short invType, bool reAddOnFail);
	bool ChangeZoneMapFromWeb(CPlayerObject* pPO, LONG inventoryItemId, LONG apartmentId, LONG changeType, short invType, bool reAddOnFail);

	bool CompletedAvatarSelect(CPlayerObject* player);
	bool IsPlayerInAvatarSelectZone(CPlayerObject* player);

	/** DRF
	* Returns validated player object from netId or NULL if no valid player is found.
	*/
	CPlayerObject* GetValidatedPlayerObjectByNetTraceId(int netTraceId);

	BOOL AttribGetMovementObjectCfg(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribGetStaticMovementObjectCfg(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribClientRequestPlayer(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribDeleteCharacterFromAccount(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribRequestCharList(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribSetInvenItemSwap(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribSetDeformableItem(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribRequestCharInventory(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribRequestCharAttachableInventory(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribRequestBuyItemFromStore(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribRequestSellItemToStore(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribRequestToBankItem(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribRequestToOrganize2BankItems(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribRequestToOrganize2InventoryItems(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribTakeItemFromBank(ATTRIB_DATA* pAttrib, NETID netIdFrom);
#if (DRF_OLD_PORTALS == 1)
	BOOL AttribRequestPortal(ATTRIB_DATA* pAttrib, NETID netIdFrom);
#endif
	BOOL AttribUseItem(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribTryOnItem(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribCancelTransaction(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribAcceptTradeTerms(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribDestroyItemByGlid(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribDestroyBankItemByGlid(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribQuestHandling(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribSkillAbilityUse(CPlayerObject* player, int skillId, int level, int ability);
	BOOL AttribBuyItemFromHouseStore(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribAddItemToHouseStore(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribRemoveItemFromHouseStore(ATTRIB_DATA* pAttrib, NETID netIdFrom);
	BOOL AttribRemoveCashFromHouseStore(ATTRIB_DATA* pAttrib, NETID netIdFrom);

	BOOL SelectPlayerAndSpawn(CAccountObject* accountPtr, NETID netIdFrom, int clientVersion, int selectedChar,
		ZoneIndex* overrideZone = 0, float overrideX = 0.0, float overrideY = 0.0, float overrideZ = 0.0, bool reconnecting = false, bool goingToPlayer = false, int rotation = 0,
		const char* _url = 0);

	void initiateSpawn(CPlayerObject* currentPlayer, CAccountObject* accountPtr,
		const ZoneIndex& zoneIndex, const Vector3f& newPos,
		int rotation = 0,
		EDestinationType destType = DT_Zone,
		const char* url = 0,
		bool sendHomeIfFail = false);

	BOOL UpdatePlayersCash(IPlayer* player, long player1AmtChanging, TransactionType transactionType = TransactionType::MISC, int glid = 0, long qty = 1, CurrencyType currencyType = CurrencyType::CREDITS_ON_PLAYER);
	IGameState::EUpdateRet UpdatePlayersCash(CPlayerObject* player1, long player1AmtChanging, TransactionType transactionType = TransactionType::MISC, int glid = 0, long qty = 1, CurrencyType currencyType = CurrencyType::CREDITS_ON_PLAYER);

	BOOL PlaceDynamicObjectAtPosition(NETID netIdFrom, unsigned int glid, short invenType, float x, float y, float z, float dx, float dy, float dz);
	void updateItemInPlayerInventory(CPlayerObject* player,
		CInventoryObjList* dest,
		CItemObj* obj,
		int updateType,
		bool checkArmed);

	void SpawnToPointInWld(CPlayerObject* currentPlayer, const Vector3f& position, const ZoneIndex zoneIndex, EDestinationType destType = DT_Zone, int rotation = 0, const char* url = 0);

	BOOL HandleQuestSys(CPlayerObject* currentPlayer, CAICollectionQuestObj* questPtr, CServerItemObj* svrPtr, NETID netIdFrom, int& questSelection);

	BOOL AddExperienceToSkillByType(int type, float m_expBonus, float m_thisGainCap, CPlayerObject* targetPayer);
	BOOL SendExtendedTextToChatWindow(CPlayerObject* targetPayer, const char* text);

	BOOL LootInventoryRequest(CPlayerObject* looter, NETID netIdFrom);
	BOOL TradeInventoryRequest(CPlayerObject* initiatorOfTrade, NETID netIdFrom, CPlayerObject* destOfTrade = 0);
	BOOL HandleReassignRespawn(CPlayerObject* pData, NETID netIdFrom);
	BOOL HandleReassignRespawnByDetails(CPlayerObject* pData, NETID netIdFrom, Vector3f position, const ZoneIndex zoneIndex);
	BOOL LoadGlobalCommerceData(const std::string& fileName);
	BOOL LoadBankZonesData(const std::string& fileName);

#if (DRF_OLD_VENDORS == 1)
	BOOL LoadItemGeneratedData(const std::string& fileName);
#endif
#if (DRF_OLD_PORTALS == 1)
	BOOL LoadPortalDatabase(const std::string& fileName);
#endif

	BOOL LoadArenaSys(const std::string& fileName);
	BOOL LoadWorldRules(const std::string& fileName);
	BOOL LoadEdbNothingLists(const std::string& fileName);
	void DeleteEdbNothingLists();

	virtual bool InitEvents() override;

	bool VerifyStatRequirement(CPlayerObject* player, CStatBonusList* requiredStats);

	BOOL SendStringListToClient(int userInt, NETID sendTo);

public:
	virtual BOOL SendInventoryUpdateToClient(NETID netIdFrom);
	virtual BOOL SendHealthUpdateToClient(NETID netIdFrom);

	virtual void BroadcastMessage(
		short radius,
		const char* message,
		double x, double y, double z,
		LONG channel,
		NETID fromNetId,
		eChatType chatType,
		bool allInstances = false) override;

	virtual void BroadcastMessageToDistribution(
		const char* message,
		NETID fromNetId,
		const char* toDistribution,
		const char* fromRecipient,
		eChatType chatType = eChatType::System,
		bool fromRemote = false,
		ULONG filter = 0) override;

	// type 0 = just that person
	//      1 = group msg
	//      2 = clan msg
	// returns TRUE if message 1 < len < 250, or if new failed
	virtual bool SendTextMessage(IPlayer* to, const char* msg, eChatType chatType);

	virtual IGetSet* Values();

	virtual void SpawnToPoint(IPlayer* p, double x, double y, double z, LONG zoneIndex, EDestinationType destType = DT_Zone, int rotation = 0, const char* url = 0);

	bool KickAllPlayers(CPlayerObject* pPlayer, bool restartScripts);
	bool KickPlayer(CPlayerObject* pPlayer, CPlayerObject* pPeer, const std::string& peerName, bool allowKickSelf);

	virtual void TextHandler(SlashCommand commandId, const char* msg, NETID netIdFrom);

	void LogCounts(Logger& logger, const char* prefix);

#if (DRF_OLD_VENDORS == 1)
	virtual IGetSet* GetItemGenObjectBySpawnId(int spawnId);
#endif

	virtual IGetSet* GetItemObjectByNetTraceId(int networkAssignedID);
	virtual IGetSet* GetItemObjectBySpawnId(int spawnID);

#if (DRF_OLD_VENDORS == 1)
	virtual void DeSpawnStaticAIByNetTraceId(int netTraceId);

	virtual void DeSpawnStaticAIBySpawnId(int spawnID, LONG channelInstance);

	IGetSet* SpawnStaticAI(CServerItemGenObj* pSIGO, double x, double y, double z, LONG channelInstance);

	virtual IGetSet* SpawnStaticAIBySpawnId(int spawnID, double x, double y, double z, LONG channelInstance) override;
#endif

	// DRF - Added - ServerScript acts on m_scratchInventory, everything else on m_inventory!
	CInventoryObjList* ServerEngine::GetInventoryObjList(IPlayer* player, bool fromScript);

	// DRF - Added - Saves given player's equipped items as named player outfit.
	PlayerOutfit* PlayerOutfitSave(IPlayer* player, std::string outfitName, bool fromScript);

	// DRF - Added - Gets given player's named outfit as saved by SavePlayerOutfit().
	PlayerOutfit* PlayerOutfitGet(IPlayer* player, std::string outfitName, bool fromScript);

	// DRF - Added - Gets given player's 'CURRENT' outfit.
	PlayerOutfit* PlayerOutfitGetCurrent(IPlayer* player, bool fromScript);

	// DRF - Added - Equips given player's named outfit as saved by SavePlayerOutfit().
	PlayerOutfit* PlayerOutfitEquip(IPlayer* player, std::string outfitName, NETID fromNetId, bool fromScript);

	// DRF - Equips given player of given items (glidSpecial: <0=disarm, >0=arm)
	// This causes all clients in the zone to get updated as well.
	bool EquipInventoryItem(IPlayer* player, const GLID& glidSpecial, NETID netIdFrom, bool fromScript = false);
	bool EquipInventoryItems(IPlayer* player, const std::vector<GLID>& glidSpecial, NETID netIdFrom, bool fromScript = false);
	bool BatchEquipInventoryItem(IPlayer* player, const GLID& glidSpecial, NETID netIdFrom, bool fromScript, std::vector<GLID>& glidsOut);

	// DRF - Unequips given player of given item.
	// This causes all clients in the zone to get updated as well.
	bool UnEquipInventoryItem(IPlayer* player, const GLID& glid, bool fromScript);

	// DRF - Added - Unequips given player of all items.
	// This causes all clients in the zone to get updated as well.
	bool UnEquipAllInventoryItems(IPlayer* player, bool fromScript);

	// DRF - Added - Unequips given player of all items with matching useType.
	// This causes all clients in the zone to get updated as well.
	bool UnEquipAllInventoryItemsOfUseType(IPlayer* player, USE_TYPE useType, bool fromScript);

	virtual BOOL GetInventoryItemQty(IPlayer* player, const GLID& glid, int& itemQuantity);

	bool TryOnInventoryItem(IPlayer* player, const GLID& glidSpecial, NETID netIdFrom);

	// returns zonePassList to avoid another lookup, ok to be null
	PassList* ValidateArmedInventoryForZone(CPlayerObject* newPobj, ZoneIndex& zi);
	PassList* GetZonePassList(ZoneIndex& zi);

	bool ValidateAnimation(CPlayerObject* player, int animGLID, int animGLID2nd, bool validateGlid = true);

	BOOL AddInventoryItemToPlayer(IPlayer* player, int nQty, BOOL bArmed, const GLID& glid, short invType, bool markDirty = true);
	BOOL AddInventoryItemToPlayer(IPlayer* player, int nQty, BOOL bArmed, const GLID& glid) {
		return AddInventoryItemToPlayer(player, nQty, bArmed, glid, IT_NORMAL);
	}

	BOOL RemoveInventoryItemFromPlayer(CPlayerObject* player, int nQty, BOOL bArmed, const GLID& glid, short invTypes, bool markDirty);
	BOOL RemoveInventoryItemFromPlayer(IPlayer* player, int nQty, BOOL bArmed, const GLID& glid, short invTypes);

	bool SendClientAttribEventSystemService(NETID playerNetId);

	virtual bool TellClientToOpenMenu(LONG menuId, NETID playerNetId, BOOL closeAllOthers, int networkAssignIdOfTradee);

	virtual IGetSet* GetPlayersBestSkill(IPlayer* player);
	virtual BOOL UseSkillOnPlayer(IPlayer* player, int skillId, int level, int ability);
	virtual BOOL UseSkillActionOnPlayer(IPlayer* player, ULONG skillId, ULONG actionId, BOOL bUseAlternate, int& newLevel, std::string& levelMessage, std::string& gainMessage, int& percentToNextLevel, std::string& rewardsAtNextLevel, float& totalExperience);

	virtual void UpdatePlayer(IPlayer* currentPlayer);

	virtual void BroadcastEvent(IEvent* rte,
		LONG channel = -1, // if INVALID_CHANNEL_ID, all channels
		NETID netID = 0, // player NetId for ctClan, ctGroup
		short radius = 0, // 0 radius = to all
		double x = 0, double y = 0, double z = 0, // if non-zero radius, position
		eChatType chatType = eChatType::System,
		bool allInstances = false);

	// event handlers
	static EVENT_PROC_RC TextHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC AdminCommandHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC GetAttributeHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC RespawnToRebirthHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC DisconnectClientHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ClientExitHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ChangeZoneMapHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC TargetedPositionEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC GotoPlaceHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC BroadcastDistrHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC PendingSpawnHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC P2pAnimRequestHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC P2pAnimPermissionReplyHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC P2pAnimEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC P2pNotifyServerAnimCompleteEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);

	static EVENT_PROC_RC ControlDynamicObjectEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC MoveDynamicObjectEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC MoveAndRotateDynamicObjectEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC RotateDynamicObjectEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC RemoveDynamicObjectEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC UpdateDynamicObjectTextureEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC UpdatePictureFrameFriendEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC SetWorldObjectTextureEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC SystemServiceEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC UpdateEquippableAnimationEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC GotoPlaceBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ChangeZoneBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ZoneCustomizationBackgroundEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC GetPlayerBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC GetAccountBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC CanSpawnToZoneBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC LogoffUserBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC BroadcastEventBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC RefreshPlayerBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ScriptClientEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ScriptServerEventNewScriptHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ScriptServerEventRayIntersectsHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ZoneDownloadCompleteEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC AvatarSelectionEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC SpaceImportHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC SpaceImportBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ChannelImportHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ChannelImportBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC PlaceDynamicObjectAtPositionEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC SoundRequestEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC SoundUpdateEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC SoundRemoveEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC PlayerTitleEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC PlayerChangeTitleEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC ZoneImportedEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);

	bool SendGotoError(int rc, Dispatcher& dispatcher, NETID from, int defaultMsgId, int charIndex = 0, const char* url = 0, bool fromDefaultUrl = false);

	void ScriptServerMoveObject(ScriptClientEvent* sce);
	void ScriptServerMoveAndRotateObject(ScriptClientEvent* sce);
	void ScriptServerRotateObject(ScriptClientEvent* sce);
	void ScriptServerScaleObject(ScriptClientEvent* sce);
	void ScriptServerObjectSetDrawDistance(ScriptClientEvent* sce);
	void PlayerGetLocation(ScriptClientEvent* sce);
	void PlayerScale(ScriptClientEvent* sce);
	void PlayerSetTitle(ScriptClientEvent* sce);
	void PlayerSetName(ScriptClientEvent* sce);
	void PlayerUseItem(ScriptClientEvent* sce);
	void PlayerEquipItem(ScriptClientEvent* sce);
	void PlayerUnEquipItem(ScriptClientEvent* sce);
	void PlayerSaveEquipped(ScriptClientEvent* sce); // drf - added
	void PlayerGetEquipped(ScriptClientEvent* sce); // drf - added
	void PlayerSetEquipped(ScriptClientEvent* sce);
	void PlayerCheckInventory(ScriptClientEvent* sce);
	void PlayerRemoveItem(ScriptClientEvent* sce);
	void PlayerAddItem(ScriptClientEvent* sce);
	void GenerateDynamicObject(ScriptClientEvent* sce);
	void GenerateOwnedDynamicObject(ScriptClientEvent* sce);
	void DeleteDynamicObject(ScriptClientEvent* sce);
	void ObjectSetTexture(ScriptClientEvent* sce);
	void ObjectSetTexturePanning(ScriptClientEvent* sce);
	void ObjectControl(ScriptClientEvent* sce);
	void ObjectSetEffectTrack(ScriptClientEvent* sce);
	void PlayerSetEffectTrack(ScriptClientEvent* sce);
	void PlayerTell(ScriptClientEvent* sce);
	void PlayerGotoUrl(ScriptClientEvent* sce);
	void PlayerSetAnimation(ScriptClientEvent* sce);
	void PlayerSetEquippedItemAnimation(ScriptClientEvent* sce);
	void PlayerRemoveAllAccessories(ScriptClientEvent* sce);
	void PlayerDefineAnimation(ScriptClientEvent* sce);
	void PlayerSetPhysics(ScriptClientEvent* sce);
	void ObjectSetParticle(ScriptClientEvent* sce);
	void ObjectRemoveParticle(ScriptClientEvent* sce);
	void PlayerSetVisibilityFlag(ScriptClientEvent* sce);
	void PlayerSetParticle(ScriptClientEvent* sce);
	void PlayerRemoveParticle(ScriptClientEvent* sce);
	void DispatchAddressedScriptClientEvent(ScriptClientEvent* sce);
	void PlayerCameraAttach(ScriptClientEvent* sce);
	void SoundAttachTo(ScriptClientEvent* sce);
	void SoundAddModifier(ScriptClientEvent* sce);
	void SoundPlayOnClient(ScriptClientEvent* sce);
	void ScriptSetEnvironment(ScriptClientEvent* sce);
	void ScriptSetDayState(ScriptClientEvent* sce);
	void ObjectLookAt(ScriptClientEvent* sce);
	void ObjectAddLabels(ScriptClientEvent* sce);
	void ObjectClearLabels(ScriptClientEvent* sce);
	void ObjectSetOutline(ScriptClientEvent* sce);
	void ObjectSetMedia(ScriptClientEvent* sce); // DRF - Added
	void ObjectSetMediaVolumeAndRadius(ScriptClientEvent* sce); // DRF - Added
	void ObjectSetCollision(ScriptClientEvent* sce);
	void ObjectSetMouseOver(ScriptClientEvent* sce);
	void SetCustomTextureOnAccessory(ScriptClientEvent* sce);
	void PlaceGameItem(ScriptClientEvent* sce);
	void ReplaceGameItem(ScriptClientEvent* sce);
	void PlayerSaveZoneSpawnPoint(ScriptClientEvent* sce);
	void PlayerDeleteZoneSpawnPoint(ScriptClientEvent* sce);
	void PlayerSpawnVehicle(ScriptClientEvent* sce);
	void PlayerLeaveVehicle(ScriptClientEvent* sce);
	void ScriptGetItemInfo(ScriptClientEvent* sce);

	// NPC Functions
	void NPCSpawn(ScriptClientEvent* sce);

	// P2p Animation Functions
	void P2pAnimAdd(int p2pIndex, const P2pAnimData& p2pAnimData);
	void P2pAnimDel(NETID netTraceId);
	bool P2pAnimTerminate(NETID netId);

	ItemAnimation ValidateItemAnimation(CPlayerObject* player, ItemAnimation ia);
	void ObjectControl(LONG placementId, int zoneId, NETID from, ULONG ctlId, std::vector<int>& ctlArgs, ScriptClientEvent* sce = NULL);

	virtual void BroadcastEventFromZoneIndex(IEvent* e, int zoneId) override;

	void spawnPlayer(bool canSpawn, int charIndex, bool reconnecting,
		CAccountObject* acct, NETID from, int gotoType, ZoneIndex& zi,
		LONG instanceId, ULONG scriptServerId,
		double x, double y, double z, int rotation,
		const std::string& url);

	int isSwitchingServers(CAccountObject* acct, GotoPlaceBackgroundEvent* gpe,
		std::string& server, ULONG& port);
	void spawnToOtherServer(CAccountObject* acct, IEvent* switchEvent);
	int getSpawnPointConfig(GotoPlaceBackgroundEvent* gpe);

	ULONG GetPlayerLastClickedXId() const {
		return m_playerLastClickedXId;
	}
	ULONG GetPlayerLastClickedYId() const {
		return m_playerLastClickedYId;
	}
	ULONG GetPlayerLastClickedZId() const {
		return m_playerLastClickedZId;
	}

	ULONG GetPlayerLastClickedSurfaceNormalXId() const {
		return m_playerLastClickedSurfaceNormalXId;
	}
	ULONG GetPlayerLastClickedSurfaceNormalYId() const {
		return m_playerLastClickedSurfaceNormalYId;
	}
	ULONG GetPlayerLastClickedSurfaceNormalZId() const {
		return m_playerLastClickedSurfaceNormalZId;
	}

	void GetItemLimits(ULONG& charLimit, ULONG& bankLimit);

	GlidState checkGlid(GLID glid);
	GlidState checkGlids(std::vector<GLID> const& glids, std::vector<GLID>& unloadedGlids);
	IBackgroundDBEvent* createLoadGlidEvent(std::vector<GLID> const& glids, IEvent* responseEvent);
	void setFailedLoadGlid(GLID globalId);

	virtual void updateScriptZoneState(const ZoneIndex& zoneIndex, unsigned spinDownDelayMins, bool spinUp);

	virtual IGameStateDispatcher* GetGameStateDispatcher() { return m_gameStateDispatcher; }

	// WorldInventory/ScriptAvailableItems
	void addItemsToAvailableScriptItems(std::vector<int>& itemIds) {
		copy(itemIds.begin(), itemIds.end(), inserter(m_availableScriptItems, m_availableScriptItems.end()));
	}

	bool isItemAvailable(int globalId, int zoneId);

	bool isAvailableScriptItemsEmpty() {
		return m_availableScriptItems.empty();
	}

	std::set<int, std::less<int>> getAvailableScriptItems();
	void addScriptAvailableItemsToInventory(CInventoryObjList* pInvObjList);
	void addScriptAvailableItemsToInventory(const std::vector<int>& glids, CInventoryObjList* pInvObjList);

	/**
	* This function is called to handle AvatarSelectionEvents.  If a valid avatar is indicated
	* it will call HandleAvatarSelection() to create the avatar's new starting apartment based
	* on the given desired apartment deed and zone identifiers.
	*
	* \param avatarIndex holds the avatar's database index
	* \param playerNetId holds the player's current network identifier
	* \param aptGlid holds the desired starting apartment deed identifier
	* \param aptId holds the desired starting apartment zone identifier
	*/
	void HandleAvatarSelection(int avatarIndex, NETID playerNetId, int aptGlid, int aptId);

	/**
	* This function creates the given player's starting apartment based on the given desired
	* apartment deed and zone identifiers.
	*
	* \param player holds the player object to create the starting apartment for
	* \param aptGlid holds the desired starting apartment deed identifier
	* \param aptId holds the desired starting apartment zone identifier
	*/
	void AvatarSelectionZoneChange(CPlayerObject* player, int aptGlid, int aptId);

	ThreadPool& threadPool() {
		return m_threadPool;
	}

	// Custom GetSet
	virtual const IKEPConfig* GetDBKEPConfig() const;

	virtual unsigned long GetServerId() const override { return m_serverId; }

	ULONG AddGameItemPlacement(const ZoneIndex& zoneIndex, ULONG doGlid, const Vector3f& pos, const Vector3f& ori, int ownerId, int gameItemId);
	virtual void ClearZoneDOMemCache(ZoneIndex const& zi) override { m_gameStateDb->ClearZoneDOMemCache(zi); }

	virtual bool GetParentZoneInfo(KGP_Connection* conn, unsigned zoneInstanceId, unsigned zoneType, unsigned& parentZoneInstanceId, unsigned& parentZoneIndex, unsigned& parentZoneType, unsigned& parentCommunityId) override {
		return m_gameStateDb->GetParentZoneInfo(conn, zoneInstanceId, zoneType, parentZoneInstanceId, parentZoneIndex, parentZoneType, parentCommunityId);
	}

	virtual TimeMs GetEffectDurationMs(KGP_Connection* conn, GLID globalId) override {
		return m_gameStateDb->GetEffectDurationMs(conn, globalId);
	}

	virtual bool GetYouTubeSWFUrlsFromDB(std::string& lastKnownGoodUrl, std::string& overrideUrl) { return m_gameStateDb->GetYouTubeSWFUrls(lastKnownGoodUrl, overrideUrl); }

	void LoadStockAnimationDurationMs();

	const backgroundqueue_monitor_config& getBackgroundQueueMonitorConfig() const { return bqcMonitorConfig; }
	void DispatchPlayerTitles(IDispatcher* disp, int playerId, NETID to);
	void ChangePlayerTitle(IDispatcher* disp, int playerId, int titleId, NETID fromNetId);

private:
	PassList* getNothingItemPassList(int edbIndex, int dimIndex);

	// Internal function for hooking up ObjectGenerate and OwnedObjectGenerate. They both pump to the same client event. -- Mo/Jo
	void HandleObjectGeneration(ScriptClientEvent* sce, LONG zoneId, LONG instanceId, ULONG globalId, ULONG placementId, int placementType, int gameItemId, double x, double y, double z, double dx, double dy, double dz, int ownerId);

	PassList* GetAnimationPassList(GLID animGlid) {
		auto it = m_animPassLists.find(animGlid);
		if (it != m_animPassLists.end())
			return it->second;
		return 0;
	}

	void SendHeartbeatToAppController();
	void SendAlertEventToAppController(int eventId);
	void CheckForPingerRefresh();
	void CheckForPendingRespawnExpiry();

private:
	KGPConnectionPool* m_connectionPool;
	// DRF - Added
	static Logger m_timingLogger;

	GameStateDispatcher* m_gameStateDispatcher; // temp public until finish all background tasks

	MetricsAgent* _metricsAgent;
	bool _isMyMetricsAgent;

	CCommerceObjList* m_commerceDB;
	CBankObjList* m_bankZoneList;
	CSecureTradeObjList* m_secureTradeDB;

#if (DRF_OLD_VENDORS == 1)
	CServerItemGenObjList* m_serverItemGenerator;
#endif

#if (DRF_OLD_PORTALS == 1)
	CPortalSysList* m_portalDatabase;
#endif

	CCollisionObjList* m_collisionDatabase;
	CGmPageList* m_gmPageListRuntime;
	CWorldChannelRuleList* m_worldChannelRuleDB;
	CBattleSchedulerList* m_battleSchedulerSys;

	std::string m_gameStateFileName;
	bool m_adminOnly;

	bool m_forcePing = true;
	Timer m_timerPing;

	Timer m_spawnExpiryUpdateTimer;

	bool m_broadcastOpenArena;

	EVENT_ID m_weaponFiredEventId,
		m_weaponDamagedEventId,
		m_skillUsedEventId,
		m_playerSpawnedEventId,
		m_spawnToBirthPointId,
		m_playerUseItemEventId,
		m_playerEquipItemEventId,
		m_playerUnEquipItemEventId,
		m_playerArmItemEventId,
		m_playerCreatedId,
		m_changeZoneMapId,
		m_lastTargetPosId,
		m_broadcastToDistrEventId,
		m_validatePlayerId,
		m_soundCustomizationReplyEventId,
		m_titleListEvent,
		m_titleEvent;
	bool m_hasUseSkillHandler;
	std::string m_delayLoadHousingZone; // hold and load after SVR file
	std::string m_key; // from Kaneva
	IGameState* m_gameStateDb;
	int m_loginNotificationDelay; // secs to wait before saying "logged in"

	ULONG m_population; // drf - current population minus AIServers
	ULONG m_populationPeakPerf; // drf - population peak since last performance metrics update
	ULONG m_populationPeakPing; // drf - population peak since last pinger update

	LONGLONG m_messageCount; //< total number of messages processed
	LONGLONG m_prevMessageCount; //< number of message from last pass for deltas
	CpuMonitor m_monitor;
	ULONG m_mainThdId;

	bool m_backgroundDefaultHandling;
	bool m_backgroundAttribToServer;
	bool m_backgroundUpdateToServer;

	ConcreteThreadPool m_threadPool;

	bool m_useScriptAvailableItemsTable;

	std::string _scriptBinDir;

	//This is a map of the edb index to a map of the dim slots for that particular index.
	//The dim slots index maps to the pass list for that slot.  Each slot can have multiple passes.
	//See the .npl file for example.  A little ugly but flexible.
	std::map<int, std::map<int, PassList*>> m_edbNothingItemLists;

	ULONG m_serverId; // ours from Pinger

	std::set<int, std::less<int>> m_availableScriptItems; // set of objects available to the script server
	std::set<unsigned long long> m_failedLoadGlids;

	//last position clicked by user is stored as temp variables in CPlayerObject via IGetSet interface
	//here are the ids for x, y, z used to set or retrieve the value. They are initilized in the Initialize()
	//function
	ULONG m_playerLastClickedXId;
	ULONG m_playerLastClickedYId;
	ULONG m_playerLastClickedZId;

	//also stores the normal of the surface where user clicked
	ULONG m_playerLastClickedSurfaceNormalXId;
	ULONG m_playerLastClickedSurfaceNormalYId;
	ULONG m_playerLastClickedSurfaceNormalZId;

	UINT m_uMsgKgpServerHeartbeat;
	UINT m_uMsgKgpServerAlert;
	HWND m_hwndHeartbeatMonitor;
	DWORD m_heartbeatMonitorHwndTimestamp;
	DWORD m_lastHeartbeatTimestamp;

	HANDLE m_hEventPingerRefresh;

	typedef std::pair<INT, P2pAnimData> p2pAnimPair;
	typedef std::pair<NETID, INT> p2pPendingPair;
	typedef std::pair<NETID, NETID> p2pPlayersPair;

	typedef std::map<INT, P2pAnimData> p2pAnimMap; //INT is the animation index for the p2p animation
	typedef std::map<NETID, INT> p2pPendingMap; //INT is the animation index of the initiator
	typedef std::map<NETID, NETID> p2pPlayerMap; //Players involved in p2p animtion (keys for both players are used)
	typedef std::map<GLID, PassList*> AnimPassLists; //animId->PassList for it

	p2pAnimMap m_p2pAnimations;
	p2pPendingMap m_pendingP2pAnim;
	p2pPlayerMap m_p2pPlayers;

	AnimPassLists m_animPassLists;
	const DBConfig* m_dbConfig;

	backgroundqueue_monitor_config bqcMonitorConfig;

	std::atomic<unsigned> m_playerArriveCounter;
};

template <class item>
/// auto delete class to call SafeDelete and delete an object.
class auto_ptr_safe_delete {
public:
	auto_ptr_safe_delete(item* p = 0) :
			ptr(p) {}
	auto_ptr_safe_delete(auto_ptr_safe_delete& src) {
		operator=(src);
	}
	auto_ptr_safe_delete& operator=(auto_ptr_safe_delete& src) {
		if (this != &src) {
			ptr = src.ptr;
			src.ptr = 0;
		}
		return *this;
	}
	auto_ptr_safe_delete& operator=(auto_ptr_safe_delete&& src) {
		return *this = src;
	}

	void release() {
		ptr = 0;
	}

	~auto_ptr_safe_delete() {
		if (ptr) {
			ptr->SafeDelete();
			delete ptr;
		}
	}

private:
	item* ptr;
};

} // namespace KEP