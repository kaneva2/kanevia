///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ServerEngine.h"

#include "Audit.h"
#include "Alert.h"
#include "CCommerceClass.h"
#include "CBankClass.h"
#include "CSafeZones.h"
#include "CSecureTradeClass.h"
#if (DRF_OLD_PORTALS == 1)
#include "CPortalSysClass.h"
#endif
#include "CElementsClass.h"
#include "CCollisionClass.h"
#include "CGmPageClass.h"
#include "CWorldChannelRules.h"
#include "CBattleSchedulerClass.h"
#if (DRF_OLD_VENDORS == 1)
#include "CServerItemGen.h"
#endif
#include "RuntimeSkeleton.h"
#include "CHousingClass.h"
#include "CAccountClass.h"
#include "CAIRaidClass.h"
#include "CAIScriptClass.h"
#include "CClanSystemClass.h"
#include "CMultiplayerClass.h"
#include "UseTypes.h"
#include "MenuList.h"
#include <PassList.h>
#include "CHousePlacementClass.h"
#include "CPlayerClass.h"
#include "CMovementObj.h"
#include "CMissileClass.h"
#include "CExplosionClass.h"
#include "CArmedInventoryClass.h"
#include "CCfgClass.h"
#include "AIclass.h"
#include "CDamagePathClass.h"
#include "CGlobalInventoryClass.h"
#include "CInventoryClass.h"
#include "CSkillClass.h"
#include "CStatClass.h"
#include "CWorldAvailableClass.h"
#include "CNoterietyClass.h"
#include "CQuestJournal.h"
#include "resource.h"
#include "IGameState.h"
#include "SlashCommand.h"
#include "..\Database\GameStateDispatcher.h"
#include "Tools\NetworkV2\INetworkLogger.h"
#include "LocaleStrings.h"
#include "BlockType.h"

namespace KEP {

static LogInstance("Instance");

PassList* ServerEngine::getNothingItemPassList(int edbIndex, int dimIndex) {
	if (dimIndex == -1)
		dimIndex = 0;

	if (m_edbNothingItemLists.find(edbIndex) != m_edbNothingItemLists.end()) {
		if (m_edbNothingItemLists[edbIndex].find(dimIndex) != m_edbNothingItemLists[edbIndex].end()) {
			return m_edbNothingItemLists[edbIndex][dimIndex];
		}
	}

	return NULL;
}

//file format -- 6-14 This format is no longer used.  remove after merge.  REK
// edb1
// dim1 pass1 pass2 pass3..passN
// dim2 pass1 pass2 pass3..passN
// edb2
// dim1 pass1 pass2 pass3..passN
// dim2 pass1 pass2 pass3..passN
// ...
// edbN
// dim1 pass1 pass2 pass3..passN
// dim2 pass1 pass2 pass3..passN
BOOL ServerEngine::LoadEdbNothingLists(const std::string& cfgFile) {
	int edb_key = 0; //edb index from read from editor created file
	int id = 0; //pass id from
	int dim_index = 0; //current dim index
	PassList* dimPassList; //dim slot pass list

	//NEW 6-14 REK
	//Use tinyXML to make more robust
	//New format:
	//
	//<edb>
	//  <actor index="10">
	//    <dims>
	//      <dim index="0">
	//        <passes>
	//          <pass>4</pass>
	//        </passes>
	//      </dim>
	//      <dim index="1">
	//        <passes>
	//          <pass>4</pass>
	//        </passes>
	//      </dim>
	//    </dims>
	//  </actor>
	//</edb>

	TiXmlDocument doc(PathAdd(PathGameFiles(), cfgFile));
	BOOL loadOk = doc.LoadFile();

	if (loadOk) {
		DeleteEdbNothingLists();

		TiXmlHandle hDoc(&doc);

		TiXmlElement* actor = hDoc.FirstChild("edb").FirstChild("actor").Element();

		//iterate over all actor entries for EDB
		for (actor; actor; actor = actor->NextSiblingElement()) {
			actor->Attribute("index", &edb_key);

			TiXmlElement* dim = TiXmlHandle(actor).FirstChild("dims").FirstChild("dim").Element();

			std::map<int, PassList*> dim_map;

			//iterate over all of the dim slots per actor
			for (dim; dim; dim = dim->NextSiblingElement()) {
				dim->Attribute("index", &dim_index);

				//create a list of passes for the current dim slot
				dimPassList = new PassList();

				TiXmlElement* pass = TiXmlHandle(dim).FirstChild("passes").FirstChild("pass").Element();

				if (!pass) {
					LogError("No passes found in " << cfgFile << " for edb index " << edb_key << " dim " << dim_index);

					continue;
				}

				for (pass; pass; pass = pass->NextSiblingElement()) {
					id = atoi(pass->FirstChild()->Value());

					if (id > 0) {
						dimPassList->addPass(id);
					}
				}

				if (dimPassList->empty()) {
					delete dimPassList;
					dimPassList = 0;
				} else {
					dim_map[dim_index] = dimPassList;
					m_edbNothingItemLists[edb_key] = dim_map;
				}
			}
		}

		return TRUE;

	} else {
		LogError("Failed to load NPL file: " << cfgFile << " Reason: " << doc.ErrorDesc());
	}

	return FALSE;
}

void ServerEngine::DeleteEdbNothingLists() {
	typedef std::map<int, std::map<int, PassList*>>::iterator edbIter;
	typedef std::map<int, PassList*>::iterator dimIter;

	for (edbIter edb = m_edbNothingItemLists.begin(); edb != m_edbNothingItemLists.end(); edb++) {
		for (dimIter dim = edb->second.begin(); dim != edb->second.end(); dim++) {
			delete dim->second;
			dim->second = 0;
		}
	}

	m_edbNothingItemLists.erase(m_edbNothingItemLists.begin(), m_edbNothingItemLists.end());
}

/*
4 ways to call.
if bArmed is set it will only delete items that either armed or not armed on the player.
if bArmed is NOT set it will only delete items that NOT currently armed on the player.
if nQty is > 1 then it will delete the specified qty from the item and remove the item if nqty > current inventory qty
if nQty is -1 it will remove all occurences of the item in their inventory stacked or not.
*/
BOOL ServerEngine::RemoveInventoryItemFromPlayer(CPlayerObject* pPO, int nQty, BOOL bArmed, const GLID& glid, short invTypes, bool markDirty) {
	if (!pPO)
		return FALSE;

	// use either gift or normal this is only called when changing zone for apt/community zone
	BOOL ret = pPO->m_inventory->DeleteByGLID(glid, nQty, !bArmed, invTypes, markDirty);
	//	if ( ret )
	//		UpdateAndSendPlayerStatToClientMsg ( pPO, pPO->m_netId );	// update armour or other bonuses

	return ret;
}

BOOL ServerEngine::RemoveInventoryItemFromPlayer(IPlayer* player, int nQty, BOOL bArmed, const GLID& glid, short invTypes) {
	return RemoveInventoryItemFromPlayer(dynamic_cast<CPlayerObject*>(player), nQty, bArmed, glid, invTypes, true);
}

BOOL ServerEngine::AddInventoryItemToPlayer(IPlayer* playerObject, int nQty, BOOL bArmed, const GLID& glid, short invType, bool markDirty) {
	CPlayerObject* pPO = dynamic_cast<CPlayerObject*>(playerObject);
	if (!pPO)
		return FALSE;

	int vIndex = 0;
	if (glid < 0) {
		if (glid != -9999) {
			vIndex = abs(glid);
		}
	} else {
		vIndex = glid;
	}

	//get the itemdata from glidb
	CGlobalInventoryObj* pGIO = m_globalInventoryDB->GetByGLID(vIndex);
	if (pGIO) {
		pPO->m_inventory->AddInventoryItem(nQty, FALSE, pGIO->m_itemData, invType, markDirty);
		pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;
		if (bArmed)
			EquipInventoryItem(pPO, vIndex, pPO->m_netTraceId);
	} else {
		try {
			//Look up and add item to global inv
			pGIO = m_gameStateDb->addCustomItem((GLID)vIndex);
			if (pGIO) {
				pPO->m_inventory->AddInventoryItem(nQty, FALSE, pGIO->m_itemData, invType, markDirty);
				pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;
				if (bArmed)
					EquipInventoryItem(pPO, vIndex, pPO->m_netTraceId);
			} else {
				LogError("GLID not found in global inventory or database: " << vIndex << " " << pPO->ToStr());
			}
		} catch (KEPException* e) {
			LogError("Error getting item data for " << pPO->ToStr() << " GLID:" << vIndex << " " << e->m_msg);
			e->Delete();
		}
	}

	return TRUE;
}

inline bool groupMatch(CMovementObj* pMO1, CMovementObj* pMO2) {
	for (int tempi = 0; tempi < 4; tempi++) {
		if (pMO1->getActorGroupByIndex(tempi) == pMO2->getActorGroupByIndex(tempi) && pMO1->getActorGroupByIndex(tempi) != 0) {
			return true;
		}
	}
	return false;
}

BOOL ServerEngine::GetInventoryItemQty(IPlayer* playerObject, const GLID& glid, int& itemQuantity) {
	auto pPO = dynamic_cast<CPlayerObject*>(playerObject);
	if (!pPO)
		return FALSE;

	int vGlid = 0;
	if (glid < 0) {
		if (glid != -9999)
			vGlid = abs(glid);
	} else {
		vGlid = glid;
	}

	CInventoryObj* pIO = (CInventoryObj*)pPO->m_inventory->VerifyInventory(vGlid);
	if (!pIO) {
		LogError("VerifyInventory() FAILED - glid<" << vGlid << "> " << pPO->ToStr());
		return FALSE;
	}

	itemQuantity = pIO->getQty();
	return TRUE;
}

// DRF - Get player's inventory object list.
// - Calls from ServerScript use m_scratchInventory
// - Calls from Client use m_inventory
CInventoryObjList* ServerEngine::GetInventoryObjList(IPlayer* player, bool fromScript) {
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	return pPO ? (fromScript ? pPO->m_scratchInventory : pPO->m_inventory) : NULL;
}

// DRF - Added
PlayerOutfit* ServerEngine::PlayerOutfitSave(IPlayer* player, std::string outfitName, bool fromScript) {
	// Invalid Outfit Name ?
	if (outfitName.empty())
		return NULL;

	// Invalid Player ?
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO)
		return NULL;

	// Get Player's Inventory Object List
	auto pIOL = GetInventoryObjList(player, fromScript);
	if (!pIOL)
		return NULL;

	// Reserved Outfit Name "KANEVA" Means Non-Script "CURRENT"
	if (outfitName == "KANEVA")
		return PlayerOutfitGetCurrent(player, false);

	// Script Outfits Are Namespaced
	if (fromScript)
		StrBuild(outfitName, "SCRIPT." << outfitName);

	LogInfo(pPO->ToStr() << ":" << outfitName);

	// Save Armed Glids As Player Outfit
	PlayerOutfit playerOutfit;
	for (POSITION pos = pIOL->GetHeadPosition(); pos;) {
		auto pIO = (CInventoryObj*)pIOL->GetNext(pos);
		if (!pIO)
			continue;

		if (pIO->isArmed()) {
			GLID glidIO = pIO->m_itemData ? pIO->m_itemData->m_globalID : GLID_INVALID;
			LogInfo(" ... glid<" << glidIO << ">");
			playerOutfit.glidList.push_back(glidIO);
		}
	}

	// Save List As Outfit
	pPO->m_savedOutfits[outfitName] = playerOutfit;

	LogInfo("OK - glids=" << playerOutfit.glidList.size());

	return &(pPO->m_savedOutfits[outfitName]);
}

// DRF - Added
PlayerOutfit* ServerEngine::PlayerOutfitGet(IPlayer* player, std::string outfitName, bool fromScript) {
	// Invalid Outfit Name ?
	if (outfitName.empty())
		return NULL;

	// Invalid Player ?
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO)
		return NULL;

	// Reserved Outfit Name "KANEVA" Means Non-Script "CURRENT"
	if (outfitName == "KANEVA")
		return PlayerOutfitGetCurrent(player, false);

	// Script Outfits Are Namespaced
	if (fromScript)
		StrBuild(outfitName, "SCRIPT." << outfitName);

	// Return Previously Saved Player Outfit
	auto it = pPO->m_savedOutfits.find(outfitName);
	if (it == pPO->m_savedOutfits.end()) {
		LogError("NOT FOUND - " << pPO->ToStr() << ":" << outfitName);
		return NULL;
	}
	return &(it->second);
}

// DRF - Added
PlayerOutfit* ServerEngine::PlayerOutfitGetCurrent(IPlayer* player, bool fromScript) {
	// Save & Return Current Player Outfit
	return PlayerOutfitSave(player, "CURRENT", fromScript);
}

// DRF - Added
PlayerOutfit* ServerEngine::PlayerOutfitEquip(IPlayer* player, std::string outfitName, NETID fromNetId, bool fromScript) {
	// Valid Outfit Name ?
	if (outfitName.empty())
		return NULL;

	// Valid Player ?
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO)
		return NULL;

	// Reserved Outfit Name "KANEVA" Means Non-Script "CURRENT"
	PlayerOutfit* pPlayerOutfit = NULL;
	if (outfitName == "KANEVA") {
		outfitName = "CURRENT";
		pPlayerOutfit = PlayerOutfitGetCurrent(player, false);
	} else {
		pPlayerOutfit = PlayerOutfitGet(player, outfitName, fromScript);
	}
	if (!pPlayerOutfit) {
		LogError("PlayerOutfitGet() FAILED - " << pPO->ToStr() << ":" << outfitName);
		return NULL;
	}

	// Equip Player Outfit Glids
	size_t glids = pPlayerOutfit->glidList.size();
	LogInfo(pPO->ToStr() << ":" << outfitName << " glids=" << glids << " ...");
	if (!EquipInventoryItems(player, pPlayerOutfit->glidList, fromNetId, fromScript)) {
		LogError("EquipInventoryItems() FAILED - " << pPO->ToStr() << ":" << outfitName);
		return NULL;
	}

	return pPlayerOutfit;
}

static std::string FromStr(bool fromScript) {
	return fromScript ? "SCRIPT" : "NOT_SCRIPT";
}
bool ServerEngine::BatchEquipInventoryItem(IPlayer* player, const GLID& glidSpecial, NETID netIdFrom, bool fromScript, std::vector<GLID>& glidsOut) {
	// Valid Player ?
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO)
		return false;

	// Get Player's Inventory Object List
	auto pIOL = GetInventoryObjList(player, fromScript);
	if (!pIOL)
		return false;

	// Extract GLID (<0=disarm >0=arm -9999=notTryOn)
	bool disarm = (glidSpecial < 0);
	GLID glid = GLID_INVALID;
	if (glidSpecial < 0) {
		if (glidSpecial != -9999)
			glid = abs(glidSpecial);
	} else
		glid = glidSpecial;

	LogInfo(FromStr(fromScript) << " - " << pPO->ToStr() << (disarm ? " DISARM" : " ARM") << " glid<" << glid << ">");

	// Verify Glid In Player's Inventory
	auto pIO = (CInventoryObj*)pPO->m_inventory->VerifyInventory(glid);
	if (fromScript) {
		// the item we are equipping is not in the player's inventory, and we are on a 3D app server,
		// and the script wants us to arm the item even if its not in our inventory
		auto pGIO = (CGlobalInventoryObj*)CGlobalInventoryObjList::GetInstance()->GetByGLID(glid);
		if (pGIO) {
			// Add To Player's Scratch Inventory
			pPO->m_scratchInventory->AddInventoryItem(1, FALSE, pGIO->m_itemData, IT_GIFT);
			pIO = (CInventoryObj*)pPO->m_scratchInventory->VerifyInventory(glid);
		} else {
			LogError("GetByGLID() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
			return false;
		}
	}
	if (!pIO) {
		LogError("VerifyInventory() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
		return false;
	}

	// cannot equip a DO in WoK
	if (pIO->m_itemData->m_useType == USE_TYPE_ADD_DYN_OBJ && // Dynamic object
		m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() == 0) { // We're in WoK, not a 3D App
		LogError("Cannot Equip Dynamic Object In WoK - " << pPO->ToStr() << " glid<" << glid << ">");
		return false;
	}

	if (pIO->m_itemData->m_useType == USE_TYPE_ADD_DYN_OBJ) {
		// Check to assure only one UGC avatar armed at once
		for (POSITION testInvPos = pIOL->GetHeadPosition(); testInvPos;) {
			CInventoryObj* testItem = (CInventoryObj*)pIOL->GetNext(testInvPos);
			if (testItem->m_itemData->m_useType == USE_TYPE_ADD_DYN_OBJ && testItem->isArmed()) {
				LogError("ALREADY_ARMED - " << pPO->ToStr() << " glidToArm<" << glid << "> glidArmed<" << testItem->m_itemData->m_globalID << ">");
				return false;
			}
		}
	}

	bool proceed = true;

	// Verify stat/skill/actorgroup(gender) unless request comes from script server.
	if (!fromScript && pPO->m_stats) {
		auto pGIO = (CGlobalInventoryObj*)m_globalInventoryDB->GetByGLID(pIO->m_itemData->m_globalID);
		if (pGIO) {
			// found glinventory
			if (!VerifyStatRequirement(pPO, pGIO->m_itemData->m_requiredStats)) {
				// send info on what went wrong
				//				SendMsgDetailedToClient (MSG_CAT_DETAILED::GETSTATSBYITEM, pIO->m_itemData->m_globalID, 0, 0, pPO );
				proceed = false;
			}

			if (proceed) {
				if (pGIO->m_itemData->m_requiredSkillType > -1 && pGIO->m_itemData->m_requiredSkillLevel > 1) {
					// item requires a skill
					if (!pPO->m_skills) {
						// skills not present
						// aquire the skill name for this
						CSkillObject* sklRefPtr = m_skillDatabase->GetSkillByType(pGIO->m_itemData->m_requiredSkillType);
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_SKILL_LACKING, sklRefPtr ? sklRefPtr->m_skillName : "");
						proceed = false;
					} else {
						CSkillObject* sklRefPtr = pPO->m_skills->GetSkillByType(pGIO->m_itemData->m_requiredSkillType);
						if (sklRefPtr) {
							// have the skill
							CStringA titleTemp;
							int curLevel = 1;

							CSkillObject* skillPtrRef = m_skillDatabase->GetSkillByType(sklRefPtr->m_skillType);
							if (skillPtrRef)
								sklRefPtr->EvaluateLevelAndTitle(titleTemp, curLevel, skillPtrRef);

							if (curLevel < pGIO->m_itemData->m_requiredSkillLevel) {
								RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_SKILL_MIN_EQUIP,
									StringHelpers::parseNumber(pGIO->m_itemData->m_requiredSkillLevel).c_str(),
									sklRefPtr->m_skillName);
								proceed = false;
							}
						} // end have a skill
						else {
							// dont have the skill at all
							sklRefPtr = m_skillDatabase->GetSkillByType(pGIO->m_itemData->m_requiredSkillType);
							if (sklRefPtr) {
								// known skill
								RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_SKILL_MIN_EQUIP,
									StringHelpers::parseNumber(pGIO->m_itemData->m_requiredSkillLevel).c_str(),
									sklRefPtr->m_skillName);
								proceed = false;
							} else {
								RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_SKILL_LACKING);
								proceed = false;
							}

							proceed = false;
						}
					}
				}
			}

			auto DBPtr = (CMovementObj*)m_movObjRefModelList->getObjByIndex(pPO->m_dbCfg);
			if (DBPtr) {
				CItemObj* itemLocal = NULL;
				CGlobalInventoryObj* glbInvOb = CGlobalInventoryObjList::GetInstance()->GetByGLID(pIO->m_itemData->m_globalID);
				if (glbInvOb)
					itemLocal = glbInvOb->m_itemData;

				if ((!itemLocal) && (pIO->m_itemData->m_baseGlid != 0)) {
					glbInvOb = CGlobalInventoryObjList::GetInstance()->GetByGLID(pIO->m_itemData->m_baseGlid);
					if (glbInvOb)
						itemLocal = glbInvOb->m_itemData;
				}

				if (itemLocal && !itemLocal->MatchActorGroups(4, DBPtr->getActorGroups()))
					itemLocal = NULL;

				if (!itemLocal) {
					proceed = false;
					QueueMsgGeneralToClient(LS_GM_CANNOT_BE_WORN, pPO);
				}
			}
		} else {
			proceed = false;
		}
	}

	if (proceed) {
		PassList* pPL_zone = GetZonePassList(pPO->m_currentZoneIndex);

		// Arm Or Disarm ?
		if (disarm) {
			// Is Item Disarmable ?
			if (!pIO->m_itemData->m_disarmable)
				return false;

			// can disarm this, check security level to remove item
			for (const auto& exId : pIO->m_itemData->m_exclusionGroupIDs) {
				PassList* pPL_nothing = getNothingItemPassList(pPO->m_curEDBIndex, exId);
				if (!pPL_nothing)
					continue;

				// first see if player can arm "nothing" for this slot
				if (!PassList::canPlayerUseItem(pPO->m_passList, pPL_nothing)) {
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS_TO_REMOVE);
					LogError("DISARM::PassList::canPlayerUseItem() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
					return false;
				} else if (!PassList::canUseItemInZone(pPL_nothing, pPL_zone)) {
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
					LogError("DISARM::PassList::canUseItemInZone() FAILED - '" << pPO->ToStr() << " glid<" << glid << ">");
					return false;
				}
			}

			// Clear Inventory Object Armed Flag
			pIO->setArmed(FALSE);

			// Disarm Glid On Player
			glidsOut.push_back(glidSpecial);

		} else {
			// find items that will be swapped
			bool alreadyArmed = false;

			// first see if player can arm the item, in the zone
			if (!pIO->m_itemData->m_useAnywhere) {
				if (!PassList::canPlayerUseItem(pPO->m_passList, pIO->m_itemData->m_passList)) {
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS);
					LogError("ARM::PassList::canPlayerUseItem() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
					return false;
				} else if (!PassList::canUseItemInZone(pIO->m_itemData->m_passList, pPL_zone)) {
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
					LogError("ARM::PassList::canUseItemInZone() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
					return false;
				}
			}

			bool locationFound = false;
			std::vector<CInventoryObj*> defaultItems;
			std::vector<int> disarmedLocations;
			std::vector<int> tempDisarmLocs;

			for (POSITION iPos = pIOL->GetHeadPosition(); iPos != NULL;) {
				CInventoryObj* invenDBPtr = (CInventoryObj*)pIOL->GetNext(iPos);

				//remember our default items for possible future use.
				if (invenDBPtr->m_itemData->m_defaultArmedItem)
					defaultItems.push_back(invenDBPtr);

				if (invenDBPtr->isArmed()) {
					for (const auto& exId : pIO->m_itemData->m_exclusionGroupIDs) {
						for (const auto& exIdDb : invenDBPtr->m_itemData->m_exclusionGroupIDs) {
							//This item will disarm the other.
							if (CheckMountSlotConflict(exIdDb, exId)) {
								if (invenDBPtr->m_itemData->m_globalID == pIO->m_itemData->m_globalID) {
									alreadyArmed = true;
									return true;
								} else {
									// disarm on server item will replace
									invenDBPtr->setArmed(FALSE);
									//Save out for python (old method. unused)
									//glidOutDisarm = -(invenDBPtr->m_itemData->m_globalID);
									locationFound = true;
								}
							} else {
								tempDisarmLocs.push_back(exIdDb);
							}
						}

						if (locationFound) {
							//This item will replace the current item.  Remember the locations the current item will disarm.
							for (const auto& tdl : tempDisarmLocs)
								disarmedLocations.push_back(tdl);

							locationFound = false;
							tempDisarmLocs.clear();
							break;
						}

						//reset the temp disarm locations for the next check of the inv items.
						tempDisarmLocs.clear();
					}
				}
			}

			//One last check to make sure we arent about to try to disarm a location of the item we are arming.
			if (!disarmedLocations.empty()) {
				std::vector<int> tempDisLocs = disarmedLocations;
				disarmedLocations.clear();
				bool found = false;

				for (const auto& exIdDis : tempDisLocs) {
					for (const auto& exId : pIO->m_itemData->m_exclusionGroupIDs) {
						if (CheckMountSlotConflict(exId, exIdDis))
							found = true;
					}

					if (!found)
						disarmedLocations.push_back(exIdDis);
					else
						found = false;
				}
			}

			pIO->setArmed(TRUE);

			if (!alreadyArmed) {
				// Arm Glid On Player
				glidsOut.push_back(glidSpecial);

				//Now check for default items that need to be put on for disarmed items.
				for (const auto& daLoc : disarmedLocations) {
					for (const auto& pIO_def : defaultItems) {
						if (!pIO_def)
							continue;

						//Default items should only have 1 location.
						std::vector<int> defItemLocIDs = pIO_def->m_itemData->m_exclusionGroupIDs;
						if (!defItemLocIDs.empty() && defItemLocIDs[0] == daLoc) {
							//A default item location id matches a disarmed location id, equip default item.
							BatchEquipInventoryItem(pPO, pIO_def->m_itemData->m_globalID, netIdFrom, fromScript, glidsOut);
						}
					}
				}
			}
		}
	}

	pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;

	//	UpdateAndSendPlayerStatToClientMsg ( pPO, netIdFrom );	// update armour or other bonuses

	return true;
}

bool ServerEngine::EquipInventoryItems(IPlayer* player, const std::vector<GLID>& glidsSpecial, NETID netIdFrom, bool fromScript) {
	// Valid Player ?
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO)
		return false;

	size_t glids = glidsSpecial.size();
	LogInfo(FromStr(fromScript) << " - " << pPO->ToStr() << " glids=" << glids << " ...");

	// Batch Glids To Multi-Equip
	std::vector<GLID> glidsBatch;
	for (size_t i = 0; i < glids; ++i) {
		GLID glidSpecial = glidsSpecial[i];
		BatchEquipInventoryItem(player, glidSpecial, netIdFrom, fromScript, glidsBatch);
	}

	// Broadcast Multi-Equip Message
	return QueueMsgEquipToAllClients(player, glidsBatch);
}

bool ServerEngine::EquipInventoryItem(IPlayer* player, const GLID& glidSpecial, NETID netIdFrom, bool fromScript) {
	// Valid Player ?
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO)
		return false;

	// Arm Glid On Player
	std::vector<GLID> glidsSpecial;
	BatchEquipInventoryItem(player, glidSpecial, netIdFrom, fromScript, glidsSpecial);
	return QueueMsgEquipToAllClients(player, glidsSpecial);
}

bool ServerEngine::UnEquipInventoryItem(IPlayer* player, const GLID& glid, bool fromScript) {
	// Valid Player ?
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO)
		return false;

	// Get Player's Inventory Object List
	auto pIOL = GetInventoryObjList(player, fromScript);
	if (!pIOL)
		return false;

	LogInfo(FromStr(fromScript) << " - " << pPO->ToStr() << " glid<" << glid << ">");

	// use either gift or normal this is only called when changing zone for apt/community zone
	auto pIO = (CInventoryObj*)pIOL->GetByGLID(glid, IT_ALL);
	if (!pIO) {
		LogError("GetByGLID() FAILED - glid<" << glid << "> " << pPO->ToStr());
		return false;
	}

	// Clear Inventory Object Armed Flag
	pIO->setArmed(FALSE);

	// Disarm Glid On Player
	GLID glidSpecialDisarm = -glid;
	std::vector<GLID> glidsSpecial;
	glidsSpecial.push_back(glidSpecialDisarm);
	return QueueMsgEquipToAllClients(pPO, glidsSpecial);
}

bool ServerEngine::UnEquipAllInventoryItems(IPlayer* player, bool fromScript) {
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO || !pPO->m_scratchInventory)
		return false;

	// Get Player's Inventory Object List
	auto pIOL = GetInventoryObjList(player, fromScript);
	if (!pIOL)
		return false;

	LogInfo(FromStr(fromScript) << " - '" << pPO->m_charName << "'");

	// Clear All Inventory Armed Flags
	for (POSITION pos = pIOL->GetHeadPosition(); pos;) {
		auto pIO = (CInventoryObj*)pIOL->GetNext(pos);
		if (!pIO)
			continue;
		pIO->setArmed(FALSE);
	}

	// Disarm All Glids On Player
	const GLID glidSpecialDisarmAll = 0;
	std::vector<GLID> glidsSpecial;
	glidsSpecial.push_back(glidSpecialDisarmAll);
	return QueueMsgEquipToAllClients(pPO, glidsSpecial);
}

bool ServerEngine::UnEquipAllInventoryItemsOfUseType(IPlayer* player, USE_TYPE useType, bool fromScript) {
	auto pPO = dynamic_cast<CPlayerObject*>(player);
	if (!pPO)
		return false;

	// Get Player's Inventory Object List
	auto pIOL = GetInventoryObjList(player, fromScript);
	if (!pIOL)
		return false;

	LogInfo(FromStr(fromScript) << " - '" << pPO->m_charName << "' useType=" << useType);

	// Clear All Inventory Armed Flags
	for (POSITION pos = pIOL->GetHeadPosition(); pos;) {
		auto pIO = (CInventoryObj*)pIOL->GetNext(pos);
		if (!pIO)
			continue;

		USE_TYPE useTypeIO = pIO->m_itemData ? pIO->m_itemData->m_useType : USE_TYPE_INVALID;
		GLID glidIO = pIO->m_itemData ? pIO->m_itemData->m_globalID : GLID_INVALID;
		bool typeMatch = ((useType == USE_TYPE_INVALID) || (useType == useTypeIO));
		if (typeMatch)
			UnEquipInventoryItem(player, glidIO, fromScript);
	}

	return true;
}

// TryOnInventoryItem, mostly a dupe of EquipInventoryItem but with enough changes to necessitate duping. Should
// 	be combined back when custom item implementation is performed.
bool ServerEngine::TryOnInventoryItem(IPlayer* playerObject, const GLID& glidSpecial, NETID netIdFrom) {
	// Valid Player ?
	auto pPO = dynamic_cast<CPlayerObject*>(playerObject);
	if (!pPO)
		return false;

	auto pAO = pPO->m_pAccountObject;
	if (!pAO)
		return false;

	// Extract GLID (<0=disarm >0=arm -9999=notTryOn)
	bool disarm = (glidSpecial < 0);
	GLID glid = GLID_INVALID;
	if (glidSpecial < 0) {
		if (glidSpecial != -9999)
			glid = abs(glidSpecial);
	} else
		glid = glidSpecial;

	LogInfo(pPO->ToStr(false) << " glid<" << glid << ">");

	auto pGIO = (CGlobalInventoryObj*)m_globalInventoryDB->GetByGLID(glid);
	if (!pGIO) {
		LogWarn("GetByGLID() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");

		if (m_gameStateDb) {
			try {
				//NOTE: I'm upgrading this the same way as in AttribTryOnItem, even though we don't need it right away.
				// It's useful for derivable UGC mesh clothing in the future. --Yanfeng 6/4/2009
				pGIO = m_gameStateDb->addCustomItem(glid);
				if (!pGIO) {
					LogError("addCustomItem() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
					return false;
				}
			} catch (KEPException* e) {
				LogError("addCustomItem() EXCEPTION - '" << pPO->ToStr() << " glid<" << glid << "> - " << e->m_msg);
				e->Delete();
			}
		}
	} else {
		// Set default for baked in items until complete db read of inventory is implemented.
		if ((pGIO->m_itemData->m_expiredDuration == 0) && (pGIO->m_itemData->m_baseGlid == 0))
			pGIO->m_itemData->m_expiredDuration = m_multiplayerObj->m_tryOnItemDefaultExpirationDuration;
	}

	if (!pGIO)
		return false;

	// cannot equip a DO in WoK
	if (pGIO && pGIO->m_itemData &&
		pGIO->m_itemData->m_useType == USE_TYPE_ADD_DYN_OBJ && // Dynamic object
		m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId() == 0) { // We're in WoK, not a 3D App
		LogError("Cannot TryOn Dynamic Object In WoK - " << pPO->ToStr() << " glid<" << glid << ">");
		return false;
	}

	BOOL proceed = TRUE;
	if (pPO->m_stats) {
		// valid stat DB

		if (!VerifyStatRequirement(pPO, pGIO->m_itemData->m_requiredStats)) {
			// send info on what went wrong
			//			SendMsgDetailedToClient (MSG_CAT_DETAILED::GETSTATSBYITEM, pGIO->m_itemData->m_globalID, 0, 0, pPO );
			proceed = FALSE;
		}

		if (proceed) {
			if (pGIO->m_itemData->m_requiredSkillType > -1 && pGIO->m_itemData->m_requiredSkillLevel > 1) {
				if (!pPO->m_skills) {
					CSkillObject* sklRefPtr = m_skillDatabase->GetSkillByType(pGIO->m_itemData->m_requiredSkillType);
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_SKILL_LACKING, sklRefPtr ? sklRefPtr->m_skillName : "");
					proceed = FALSE;
				} else {
					CSkillObject* sklRefPtr = pPO->m_skills->GetSkillByType(pGIO->m_itemData->m_requiredSkillType);
					if (sklRefPtr) {
						CStringA titleTemp;
						int curLevel = 1;
						CSkillObject* skillPtrRef = m_skillDatabase->GetSkillByType(sklRefPtr->m_skillType);
						if (skillPtrRef)
							sklRefPtr->EvaluateLevelAndTitle(titleTemp, curLevel, skillPtrRef);

						if (curLevel < pGIO->m_itemData->m_requiredSkillLevel) {
							RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_SKILL_MIN_EQUIP,
								StringHelpers::parseNumber(pGIO->m_itemData->m_requiredSkillLevel).c_str(),
								sklRefPtr->m_skillName);
							proceed = FALSE;
						}
					} else {
						sklRefPtr = m_skillDatabase->GetSkillByType(pGIO->m_itemData->m_requiredSkillType);
						if (sklRefPtr) {
							RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_SKILL_MIN_EQUIP,
								StringHelpers::parseNumber(pGIO->m_itemData->m_requiredSkillLevel).c_str(),
								sklRefPtr->m_skillName);
							proceed = FALSE;
						} else {
							RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::Talk, LS_SKILL_LACKING);
							proceed = FALSE;
						}

						proceed = FALSE;
					}
				}
			}
		}

		auto pMORM = m_movObjRefModelList->getObjByIndex(pPO->m_dbCfg);
		if (pMORM) {
			CItemObj* itemLocal = NULL;
			CGlobalInventoryObj* glbInvOb = CGlobalInventoryObjList::GetInstance()->GetByGLID(pGIO->m_itemData->m_globalID);
			if (glbInvOb)
				itemLocal = glbInvOb->m_itemData;

			if ((!itemLocal) && (pGIO->m_itemData->m_baseGlid != 0)) {
				glbInvOb = CGlobalInventoryObjList::GetInstance()->GetByGLID(pGIO->m_itemData->m_baseGlid);
				if (glbInvOb)
					itemLocal = glbInvOb->m_itemData;
			}

			if (itemLocal) {
				if (!itemLocal->MatchActorGroups(4, pMORM->getActorGroups()))
					itemLocal = NULL;
			}

			if (!itemLocal) {
				proceed = FALSE;
				QueueMsgGeneralToClient(LS_GM_CANNOT_BE_WORN, pPO);
			}
		} else {
			proceed = FALSE;
		}
	}

	if (proceed) {
		PassList* zonePassList = GetZonePassList(pPO->m_currentZoneIndex);

		// Arm Or Disarm ?
		if (disarm) {
			// Disarm Item
			for (const auto& exId : pGIO->m_itemData->m_exclusionGroupIDs) {
				// can disarm this, check security level to remove item
				PassList* nothingPassList = getNothingItemPassList(pPO->m_curEDBIndex, exId);
				if (nothingPassList != 0) {
					// first see if player can arm "nothing" for this slot
					if (!PassList::canPlayerUseItem(pPO->m_passList, nothingPassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS_TO_REMOVE);
						LogError("DISARM::PassList::canPlayerUseItem() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
						return false;
					} else if (!PassList::canUseItemInZone(nothingPassList, zonePassList)) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
						LogError("DISARM::PassList::canUseItemInZone() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
						return false;
					}
				}
			}

			// Arm/Disarm Glid On Player
			std::vector<GLID> glidsSpecial;
			glidsSpecial.push_back(glidSpecial);
			QueueMsgEquipToAllClients(pPO, glidsSpecial);

		} else {
			// TryOn - Arm Item
			if (!pGIO->m_itemData->m_useAnywhere) {
				if (!PassList::canPlayerUseItem(pPO->m_passList, pGIO->m_itemData->m_passList)) {
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_MISSING_PASS);
					LogError("ARM::PassList::canPlayerUseItem() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
					return false;
				} else if (!PassList::canUseItemInZone(pGIO->m_itemData->m_passList, zonePassList)) {
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
					LogError("ARM::PassList::canUseItemInZone() FAILED - " << pPO->ToStr() << " glid<" << glid << ">");
					return false;
				}
			}

			LogInfo("Broadcasting TryOn - " << pPO->ToStr() << " glid<" << glid << ">");

			bool locationFound = false;
			std::vector<CInventoryObj*> defaultItems;
			std::vector<int> disarmedLocations;
			std::vector<int> tempDisarmLocs;

			for (POSITION posLoc = pPO->m_inventory->GetHeadPosition(); posLoc != NULL;) {
				CInventoryObj* playerInvObj = (CInventoryObj*)pPO->m_inventory->GetNext(posLoc);

				if (playerInvObj->m_itemData->m_defaultArmedItem) {
					//remember what default items the player has
					defaultItems.push_back(playerInvObj);
				}

				if (playerInvObj->isArmed()) {
					for (auto globalObjExID = pGIO->m_itemData->m_exclusionGroupIDs.begin(); globalObjExID != pGIO->m_itemData->m_exclusionGroupIDs.end(); globalObjExID++) {
						for (auto playerObjExID = playerInvObj->m_itemData->m_exclusionGroupIDs.begin(); playerObjExID != playerInvObj->m_itemData->m_exclusionGroupIDs.end(); playerObjExID++) {
							if (CheckMountSlotConflict(*globalObjExID, *playerObjExID)) {
								locationFound = true;
							} else {
								//The currently armed item has a location not covered by this location id for the custom item being armed.
								//remember exposed location.
								tempDisarmLocs.push_back(*playerObjExID);
							}
						}

						if (locationFound) {
							//This item will replace the current item.  Remember the locations the current item will disarm.
							for (auto tdl = tempDisarmLocs.begin(); tdl != tempDisarmLocs.end(); tdl++)
								disarmedLocations.push_back(*tdl);

							locationFound = false;
							tempDisarmLocs.clear();
							break;
						}

						//reset the temp disarm locations for the next check of the inv items.
						tempDisarmLocs.clear();
					}
				}
			}

			//One last check to make sure we arent about to try to disarm a location of the item we are arming.
			if (!disarmedLocations.empty()) {
				std::vector<int> tempDisLocs = disarmedLocations;
				disarmedLocations.clear();
				bool found = false;

				for (auto disLocs = tempDisLocs.begin(); disLocs != tempDisLocs.end(); disLocs++) {
					for (auto exIDs = pGIO->m_itemData->m_exclusionGroupIDs.begin(); exIDs != pGIO->m_itemData->m_exclusionGroupIDs.end(); exIDs++) {
						if (CheckMountSlotConflict(*exIDs, *disLocs))
							found = true;
					}

					if (!found)
						disarmedLocations.push_back(*disLocs);
					else
						found = false;
				}
			}

			// Arm/Disarm Glid On Player
			std::vector<GLID> glidsSpecial;
			glidsSpecial.push_back(glidSpecial);
			QueueMsgEquipToAllClients(pPO, glidsSpecial);
		}

		if (pGIO->m_itemData->m_expiredDuration != 0) {
			// Send client expiration duration for the item.
			TryOnExpireEvent* expireEvent = new TryOnExpireEvent();
			expireEvent->InformExpiration(pPO->m_handle, pGIO->m_itemData->m_globalID, pGIO->m_itemData->m_baseGlid, pGIO->m_itemData->m_useType, pGIO->m_itemData->m_expiredDuration, 0);
			expireEvent->AddTo(netIdFrom);
			m_dispatcher.QueueEvent(expireEvent);
		}
	}

	return true;
}

PassList* ServerEngine::GetZonePassList(ZoneIndex& zi) {
	PassList* zonePassList = 0;
	if (m_gameStateDb) {
		try {
			zonePassList = m_gameStateDb->GetZonePassList(zi);
		} catch (KEPException* e) {
			LogWarn("Error getting zonePassList: " << e->m_msg);
			e->Delete();
		}
	}
	return zonePassList;
}

// check to make sure they have things valid for this zone, disarming as needed
// returns zonePassList to avoid another lookup, ok to be null
PassList* ServerEngine::ValidateArmedInventoryForZone(CPlayerObject* newPobj, ZoneIndex& zi) {
	PassList* zonePassList = GetZonePassList(zi);

	//int slotIndex = 0;
	int cur_edb = newPobj->m_curEDBIndex;

	std::map<int, CInventoryObj*> defaultItems;
	std::map<int, CInventoryObj*> armedItems;

	//Go through the players armed items and ensure the zone allows the item
	for (POSITION iPos = newPobj->m_inventory->GetHeadPosition(); iPos != NULL;) {
		CInventoryObj* invenDBPtr = (CInventoryObj*)newPobj->m_inventory->GetNext(iPos);

		for (std::vector<int>::const_iterator locID = invenDBPtr->m_itemData->m_exclusionGroupIDs.begin(); locID != invenDBPtr->m_itemData->m_exclusionGroupIDs.end(); locID++) {
			int dim_slot = *locID;

			//Artists use -1 for shirt slot so convert
			if (dim_slot == -1)
				dim_slot = 0;

			//remeber default items if we need to arm an empty slot later
			if (invenDBPtr->m_itemData->m_defaultArmedItem) {
				defaultItems[dim_slot] = invenDBPtr;
			}

			if (invenDBPtr->isArmed()) {
				//if you cant wear the item in the zone disarm it
				if (!PassList::canUseItemInZone(invenDBPtr->m_itemData->m_passList, zonePassList)) {
					// negative glid == disarm
					EquipInventoryItem(newPobj, -invenDBPtr->m_itemData->m_globalID, newPobj->m_netId);
				} else {
					//remember which items are armed to check dim passes later for dim slots with out an armed item
					armedItems[dim_slot] = invenDBPtr;
				}
			}
		}
	}

	std::map<int, PassList*> dimsWithPasses = m_edbNothingItemLists[cur_edb];
	typedef std::map<int, PassList*>::const_iterator dWP;

	//iterate through dim slots with assigned passes and ensure its ok to NOT have an item armed in that dim slot
	//first is dim slot, second is pointer to passlist for dim slot
	for (dWP i = dimsWithPasses.begin(); i != dimsWithPasses.end(); i++) {
		if (!armedItems[i->first]) {
			//This dim slot has passes associated with it but no item armed.
			//Make sure its ok to not have an item armed in this location.
			if (!PassList::canUseItemInZone(i->second, zonePassList)) {
				//Must have an item armed here, so equip the default for the dim location, if there is one
				if (defaultItems[i->first]) {
					EquipInventoryItem(newPobj, defaultItems[i->first]->m_itemData->m_globalID, newPobj->m_netId);
				} else {
					LogWarn("Default item necessary for this dim, but not found! dim: " << i->first << " for player: " << newPobj->m_charName);
				}
			}
		}
	}

	return zonePassList;
}

// log counts to the logger for tracking leaks, etc.
void ServerEngine::LogCounts(Logger& logger, const char* prefix) {
	LOG4CPLUS_INFO(logger, "====== Dumping counts: " << prefix);

	// our or parent items
	LOG_VECTOR_COUNTS(logger, m_eventBlades, EventBladeVector);
	LOG_OBLIST_COUNTS(logger, m_elementDB, CElementObjList);
	LOG_OBLIST_COUNTS(logger, m_statDatabase, CStatObjList);
	LOG_OBLIST_COUNTS(logger, m_housingDB, CHousingObjList);
	LOG_OBLIST_COUNTS(logger, m_aiRaidDatabase, CAIRaidObjList);
	LOG_OBLIST_COUNTS(logger, m_movObjRefModelList, CMovementObjList);
	LOG_OBLIST_COUNTS(logger, m_missileRuntime, CMissileObjList);
	LOG_OBLIST_COUNTS(logger, m_globalInventoryDB, CGlobalInventoryObjList);
	LOG_OBLIST_COUNTS(logger, m_skillDatabase, CSkillObjectList);
	LOG_OBLIST_COUNTS(logger, m_explosionDBList, CExplosionObjList);
	LOG_OBLIST_COUNTS(logger, m_AIOL, CAIObjList);
	LOG_OBLIST_COUNTS(logger, m_pAISOL, CAIScriptObjList);
	LOG_OBLIST_COUNTS(logger, m_bankZoneList, CBankObjList);
	LOG_OBLIST_COUNTS(logger, m_secureTradeDB, CSecureTradeObjList);
#if (DRF_OLD_VENDORS == 1)
	LOG_OBLIST_COUNTS(logger, m_serverItemGenerator, CServerItemGenObjList);
#endif
	LOG_OBLIST_COUNTS(logger, m_commerceDB, CCommerceObjList);
#if (DRF_OLD_PORTALS == 1)
	LOG_OBLIST_COUNTS(logger, m_portalDatabase, CPortalSysList);
#endif
	LOG_OBLIST_COUNTS(logger, m_collisionDatabase, CCollisionObjList);
	LOG_OBLIST_COUNTS(logger, m_gmPageListRuntime, CGmPageList);
	LOG_OBLIST_COUNTS(logger, m_worldChannelRuleDB, CWorldChannelRuleList);
	LOG_OBLIST_COUNTS(logger, m_battleSchedulerSys, CBattleSchedulerList);
}

bool ServerEngine::KickAllPlayers(CPlayerObject* pPlayer, bool restartScripts) {
	if (!pPlayer)
		return false;

	// Must Be At least A Moderator
	if (!restartScripts && pPlayer->m_currentZonePriv > ZP_MODERATOR) {
		LogInfo("/kick all - Insufficient user privilege: " << pPlayer->ToStr());
		m_multiplayerObj->QueueOrSendRenderTextEvent(pPlayer->m_netId, "You must be an moderator/owner of current zone to use kick command.", eChatType::System);
		return false;
	} else if (restartScripts && !pPlayer->m_gm && !pPlayer->m_admin) { // Use m_gm/m_admin because m_currentZonePriv is set to ZP_OWNER in GM/ADMIN's home
		LogInfo("/kick ALL - Insufficient user privilege: " << pPlayer->ToStr());
		m_multiplayerObj->QueueOrSendRenderTextEvent(pPlayer->m_netId, "You must be an Admin/GM to use kick /ALL command.", eChatType::System);
		return false;
	}

	// Tell script server to set spin down delay to 0 if restartScripts is true
	bool kickSelf = false;
	if (restartScripts) {
		m_dispatcher.QueueEvent(new UpdateScriptZoneSpinDownDelayEvent(pPlayer->m_currentZoneIndex, 0, 0));
		kickSelf = true;
	}

	// For All Players
	for (POSITION pos = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); pos;) {
		auto pPeer = static_cast<CPlayerObject*>(m_multiplayerObj->m_runtimePlayersOnServer->GetNext(pos));
		// Validation
		assert(pPeer && pPeer->m_pAccountObject);
		if (!pPeer || !pPeer->m_pAccountObject) {
			continue;
		}

		if (!kickSelf && pPeer == pPlayer) {
			// Skip self
			continue;
		}

		if (restartScripts) {
			assert(pPeer->m_pAccountObject != nullptr);
			if (pPeer->m_pAccountObject) {
				CPlayerObject* pPlayerOnAccount = pPeer->m_pAccountObject->GetCurrentPlayerObject();
				assert(pPlayerOnAccount != nullptr);
				if (pPlayerOnAccount) {
					pPlayerOnAccount->m_forcePlayerDepartEventOnSpawn = true;
				}
			}
		}

		KickPlayer(pPlayer, pPeer, pPeer->m_charName.GetString(), kickSelf);
	}

	return true;
}

bool ServerEngine::KickPlayer(CPlayerObject* pPlayer, CPlayerObject* pPeer, const std::string& peerName, bool allowKickSelf) {
	if (!pPlayer || (!allowKickSelf && pPlayer == pPeer))
		return false;

	// Must Be At least A Moderator
	if (pPlayer->m_currentZonePriv > ZP_MODERATOR) {
		LogInfo("/kick " << peerName << " - Insufficient user privilege: " << pPlayer->ToStr());
		m_multiplayerObj->QueueOrSendRenderTextEvent(pPlayer->m_netId, "You must be an moderator/owner of current zone to use kick command.", eChatType::System);
		return false;
	}

	// Check if peer exists in the same world (or linked worlds)
	if (!pPeer || !pPeer->m_pAccountObject || !pPlayer->isPeerInSameOrLinkedWorld(pPeer)) {
		std::string message = "Cannot kick player: " + peerName + " is not in current world.";
		m_multiplayerObj->QueueOrSendRenderTextEvent(pPlayer->m_netId, message.c_str(), eChatType::System);
		return false;
	}

	// Only GMs Can Kick GMs
	if (pPeer->m_currentZonePriv == ZP_GM && pPlayer->m_currentZonePriv != ZP_GM) {
		LogInfo("ONLY GM CAN KICK GM - " << pPlayer->ToStr());
		return false;
	}

	// Player Being Kicked Must Have Valid Home
	if (pPeer->m_housingZoneIndex == INVALID_ZONE_INDEX) {
		RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPlayer->m_netId, eChatType::System, LS_NO_HOME);
		LogError("INVALID HOUSING ZONE INDEX - " << pPeer->ToStr());
		return false;
	}

	// Broadcast 'Who Kicked Who' To Everyone
	BroadcastEvent(new RenderLocaleTextEvent(LS_KICKED, eChatType::System, pPlayer->m_charName, pPeer->m_charName), pPeer->m_currentChannel);
	LogInfo(pPlayer->ToStr() << " kicked " << pPeer->ToStr());

	// Fire Event To Send Player Home
	auto pAO_kick = pPeer->m_pAccountObject;
	LONG parentGameId = m_multiplayerObj->m_serverNetwork->pinger().GetParentGameId();
	auto pEvent = new GotoPlaceEvent();
	if (parentGameId == 0) {
		pEvent->GotoApartment(pAO_kick->m_serverId, pAO_kick->m_currentcharInUse);
		pEvent->SetFrom(pAO_kick->m_netId);
	} else {
		std::string homeURL = StpUrl::MakeUrlPrefix(parentGameId) + "/" + StpUrl::MakeAptUrlPath(pAO_kick->m_accountName).c_str();
		pEvent->GotoUrl(0, homeURL.c_str());
		pEvent->AddTo(pAO_kick->m_netId);
	}
	m_dispatcher.QueueEvent(pEvent);

	return true;
}

#include "TextCommands.h"

void ServerEngine::TextHandler(SlashCommand commandId, const char* newString, NETID netIdFrom) {
	auto pPO = GetPlayerObjectByNetId(netIdFrom, false);
	if (!pPO)
		return;

	// found one
	CStringA newStringTrimmed = newString;
	newStringTrimmed.Trim();

	// handle text commands
	std::vector<std::string> csaAttr;

	//parse for commands with multiple attributes
	STLTokenize(newString, csaAttr);

	if (commandId == SlashCommand::NONE) {
		m_multiplayerObj->BroadcastMessage(500,
			pPO->m_charName + ">" + newStringTrimmed,
			pPO->InterpolatedPos(),
			pPO->m_currentChannel,
			pPO->m_netId);
		return;
	}

	//Admin Commands
	if (pPO->m_admin) {
		if (csaAttr.size() > 0) { //we have at least 1 attribute
			switch (commandId) {
				case SlashCommand::BLOCKIP:
					if (pPO->m_admin) {
						m_multiplayerObj->m_ipBlockList.add(csaAttr.at(0));
					}
					break;

				case SlashCommand::UNBLOCKIP:
					if (pPO->m_admin) {
						m_multiplayerObj->m_ipBlockList.remove(csaAttr.at(0));
					}
					break;

				case SlashCommand::BANPLAYER:
					if (pPO->m_admin) {
						bool banned = false;
						const char* msg = strstr(newString, csaAttr.at(0).c_str());
						if (msg != 0) {
							msg += csaAttr.at(0).length();
							while (*msg == ' ')
								msg++;
						} else
							msg = "";

						if (m_gameStateDb != 0) {
							try {
								banned = m_gameStateDb->SetPlayerBanStatus(csaAttr.at(0).c_str(), msg, pPO, true);
							} catch (KEPException* e) {
								LogError("DB Error banning player " << csaAttr.at(0) << " " << e->m_msg);
								e->Delete();
							}

						} else {
							m_multiplayerObj->BanIPByUsername(csaAttr.at(0));
							banned = true;
						}

						std::string adminMsg(csaAttr.at(0));
						if (banned) {
							adminMsg += " has been banned";

							auto pPO_dest = GetPlayerObjectByName(csaAttr.at(0));
							if (pPO_dest) {
								RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO_dest->m_netId, eChatType::System, LS_BANNED, msg);
								// queue this event in future to allow them to get the msg they've been banned
								IEvent* e = new ClientExitEvent();
								e->SetFrom(pPO_dest->m_netId);
								m_dispatcher.QueueEventInFutureMs(e, 3.0 * MS_PER_SEC);
							}
						} else
							adminMsg += " has not been banned.  GM or not found.";

						SendTextMessage(pPO, adminMsg.c_str(), eChatType::System);
					}
					break;

				case SlashCommand::UNBANPLAYER:
					if (pPO->m_admin) {
						bool unbanned = false;
						if (m_gameStateDb != 0) {
							try {
								unbanned = m_gameStateDb->SetPlayerBanStatus(csaAttr.at(0).c_str(), "", pPO, false);
							} catch (KEPException* e) {
								LogWarn("DB Error banning player " << csaAttr.at(0) << " " << e->m_msg);
								e->Delete();
							}
						} else {
							m_multiplayerObj->UnBanIPByUsername(csaAttr.at(0));
							unbanned = true;
						}

						std::string msg(csaAttr.at(0));
						if (unbanned)
							msg += " has been unbanned";
						else
							msg += " has not been unbanned.  GM or not found.";
						SendTextMessage(pPO, msg.c_str(), eChatType::System);
					}
					break;

				case SlashCommand::GENERATE:
					if (pPO->m_spawnAbility) {
						int glidID = atoi(csaAttr.at(0).c_str());

						if (glidID > 0 && glidID < 30000) {
							CGlobalInventoryObj* invObjLocal = m_globalInventoryDB->GetByGLID(glidID);
							if (invObjLocal) {
								pPO->m_inventory->AddInventoryItem(1, FALSE, invObjLocal->m_itemData, IT_GIFT);
								pPO->m_dbDirtyFlags |= PlayerMask::INVENTORY;
							}
						}
					}
					break;
#ifdef _DEBUG
				case SlashCommand::TEST: {
					int id = atoi(csaAttr.at(0).c_str());
					switch (id) {
						case 1:
							// just a little testing attached to an arbitrary command
							// REMOVED: RemoteDispatherEvent
							assert(false);
							break;
						case 2: {
							// script server goto test to send this player to url as if SS did it
							if (csaAttr.size() > 1) {
								IEvent* e = new ScriptClientEvent(ScriptClientEvent::PlayerGotoUrl, 0);
								(*e->OutBuffer()) << csaAttr[1] << netIdFrom;
								m_dispatcher.QueueEvent(e);
							}
						}
					}
					break;
				}
#endif
			} // switch commandId

		} // if casAttr

		if (pPO->m_gm) { //GM+Admin Commands
			if (commandId == SlashCommand::ZEROMURDERS) {
				if (pPO->m_noteriety) {
					pPO->m_noteriety->m_currentMurders = 0;
					pPO->m_dbDirtyFlags |= PlayerMask::NOTERIETIES;
				}
			}
		}

	} //admin commands

	//Admin or GM
	if (pPO->m_admin == TRUE || pPO->m_gm == TRUE) {
		switch (commandId) {
			case SlashCommand::HELPADMIN:
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "Admin Commands:", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/BLOCKIP {IP - Blocks an IP Address", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/UNBLOCKIP {IP} - UNBLOCKS an IP Address", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/BANPLAYER {PlayerName} - Bans a player", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/UNBANPLAYER {PlayerName} - Removes a player ban", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/GENERATE {GLID} - Adds an item to your inventory", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/REMOVEHOUSE - Stand on a house and it will remove it", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/PLAYERCOUNT - Returns the number of players online", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/VUP - Increase Vicinity tolerance", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/VDOWN - Decrease Vicinity tolerance", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/ZEROMURDERS - Resets the murder count", eChatType::Talk);
				// let fall through to show gm commands, too break;

			case SlashCommand::HELPGM:
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "GM Commands:", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/BROADCAST - Sends a message to the entire server", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/SPAWNGEN {AI Index} - Spawn an AI Bot at your location", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/GOTO {x y z} - Sends you to a location in the current zone", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/GOTOPLAYER {PlayerName} - Locates you to a player's position", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/SUMMON {PlayerName} - Summons a player to your current location", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/ANSWERNEXTPAGE - Sends you to the location of the last GM page", eChatType::Talk);
				break;

			case SlashCommand::BROADCAST:
				if (newString) {
					const char* msg = newString;
					if (*newString == '*') // send to other servers, too!
						msg++;

					// local broadcast
					m_multiplayerObj->BroadcastMessage(0, msg, Vector3f(0, 0, 0), INVALID_CHANNEL_ID, 0);

					if (msg != newString)
						// send out to any other servers
						BroadcastMessageToDistribution(msg, 0, "", "", eChatType::System, false);
				}
				break;

		} // end switch
	} // Admin or GM

	//GM Commands
	if (pPO->m_gm) {
		switch (commandId) {
			case SlashCommand::GOTO:
				if (csaAttr.size() >= 3) {
					ZoneIndex zoneIndex;
					if (csaAttr.size() >= 5) {
						// (3) = zoneIndex
						// (4) = instanceId
						ULONG zi = strtoul(csaAttr.at(3).c_str(), 0, 0);
						LONG i = atoi(csaAttr.at(4).c_str());
						if (zi > 0 && i > 0) {
							zoneIndex = (LONG)zi;
							zoneIndex.setInstanceId(i);
						}
					}
					if (zoneIndex.toLong() == 0) {
						zoneIndex = (m_multiplayerObj->m_currentWorldsUsed->GetZeroBasedIndex(pPO->m_currentWorldMap));
						if (pPO->m_currentChannel.isInstanced())
							zoneIndex.setInstanceAndTypeFrom(pPO->m_currentChannel);
					}
					Vector3f vpos;
					vpos.x = atoi(csaAttr.at(0).c_str());
					vpos.y = atoi(csaAttr.at(1).c_str());
					vpos.z = atoi(csaAttr.at(2).c_str());
					SpawnToPointInWld(pPO, vpos, zoneIndex);
				}
				break;

			case SlashCommand::GOTOPLAYER:
				if (csaAttr.size() > 0) {
					for (POSITION posLoc = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); posLoc != NULL;) {
						auto pPO_target = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(posLoc);
						if (!pPO_target->m_aiControlled) {
							if (pPO_target->m_charName == newString) {
								ZoneIndex zoneIndex(m_multiplayerObj->m_currentWorldsUsed->GetZeroBasedIndex(pPO_target->m_currentWorldMap));
								if (pPO_target->m_currentChannel.isInstanced())
									zoneIndex.setInstanceAndTypeFrom(pPO_target->m_currentChannel);
								SpawnToPointInWld(pPO, pPO_target->InterpolatedPos(), zoneIndex);
								break;
							}
						}
					}
				}
				break;

			case SlashCommand::SUMMON: {
				CStringA exactMatch(newString);
				if (exactMatch.GetLength() != 0) { // must have something
					CStringA subString;

					bool allPlayers = false;
					if (newString[::strlen(newString) - 1] == '*') { // allow trailing * for wildcards
						subString = newString;
						subString.TrimRight('*');
						subString.MakeLower();
						allPlayers = subString.IsEmpty();
					} else {
						exactMatch.MakeLower();
					}

					for (POSITION posLoc = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); posLoc != NULL;) {
						auto pPO_target = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(posLoc);
						if (!pPO_target->m_aiControlled) {
							CStringA name(pPO_target->m_charName);
							name.MakeLower();

							if ((allPlayers ||
									(subString.GetLength() > 0 && name.Left(subString.GetLength()) == subString) ||
									name == exactMatch) &&
								pPO_target != pPO) {
								// if not staying in same arena, fire eject event
								if (pPO_target->m_currentChannel.isArena() && pPO_target->m_currentChannel != pPO_target->m_currentChannel)
#ifndef REFACTOR_INSTANCEID
									m_dispatcher.QueueEvent(new ArenaEjectEvent(pPO_target->m_currentChannel, pPO_target->m_netId, pPO_target->m_netTraceId));
#else
									m_dispatcher.QueueEvent(new ArenaEjectEvent(pPO_target->m_currentChannel.toLong(), pPO_target->m_netId, pPO_target->m_netTraceId));
#endif

								SpawnToPointInWld(pPO_target, pPO->InterpolatedPos(), pPO->m_currentZoneIndex);

								if (!allPlayers)
									break;
							}
						}
					}
				}
				break;
			}

			case SlashCommand::ANSWERNEXTPAGE: {
				// request bank inventory
				POSITION nxtPagePos = m_gmPageListRuntime->GetTailPosition();
				if (nxtPagePos) {
					CGmPageObj* pagePtr = (CGmPageObj*)m_gmPageListRuntime->GetAt(nxtPagePos);
					CPlayerObject* pPO_target = GetPlayerObjectByPlayerHandle(pagePtr->m_handle);
					if (pPO_target) {
						// this converts a channel-instance into ZoneIndex instance for passing into SpawnToPointInWld
						ZoneIndex zoneIndex(m_multiplayerObj->m_currentWorldsUsed->GetZeroBasedIndex(pPO_target->m_currentWorldMap));
						if (pPO_target->m_currentChannel.isInstanced())
							zoneIndex.setInstanceAndTypeFrom(pPO_target->m_currentChannel);

						SpawnToPointInWld(pPO, pPO_target->InterpolatedPos(), zoneIndex);
					} else {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::Talk, LS_PLAYER_OFFLINE);
					}

					delete pagePtr;
					pagePtr = 0;
					m_gmPageListRuntime->RemoveAt(nxtPagePos);
				} else {
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::Talk, LS_NO_PAGES);
				}
				break;
			}

			// 1/31/07 only gm since can kill server/kill if too big 6/29/05 Allow who for everyone (or let script enforce)
			case SlashCommand::WHO: {
				int startPos = atoi(newString);
				static int MAX_TO_SEND = 10;

				CStringA message;
				message.Format("Current Players Online %d starting with %d: ", m_multiplayerObj->m_runtimePlayersOnServer->GetCount(),
					startPos);
				m_multiplayerObj->QueueOrSendRenderTextEvent(netIdFrom, message, eChatType::Talk);
				message = "";
				int nCount = 0;
				int i = 0;
				int totSent = 0;
				for (POSITION posLoc = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); posLoc != NULL;) {
					// account list
					CPlayerObject* pPO_target = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(posLoc);

					i++;
					if (i <= startPos)
						continue;

					totSent++;

					if (!pPO_target->m_aiControlled) {
						message += pPO_target->m_charName;
						message += ", ";
						nCount++;
						if (nCount > 5) {
							m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, message, eChatType::Talk);
							nCount = 0;
							message = "";
						}
					}
					if (totSent > MAX_TO_SEND) // only send n at a time
						break;
				}
				if (!message.IsEmpty())
					m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, message, eChatType::Talk);
				break;
			}

			case SlashCommand::PLACEOBJECT: {
				int objId = atoi(newString); // glid you must have in regular inv
				ATTRIB_DATA attribData;
				attribData.aeShort1 = pPO->m_netTraceId;
				attribData.aeShort2 = IT_NORMAL;
				attribData.aeInt1 = objId;
				AttribUseItem(&attribData, netIdFrom);
				break;
			}

			case SlashCommand::MOVEOBJECT: {
				int placementId = atoi(newString);
				MovementData md = pPO->InterpolatedMovementData();
				Vector3f slide = Cross(md.up, md.dir);
				IEvent* newEvt = new MoveAndRotateDynamicObjectEvent(
					placementId,
					md.pos.x,
					md.pos.y,
					md.pos.z,
					md.dir.x,
					md.dir.y,
					md.dir.z,
					slide.x,
					slide.y,
					slide.z);

				newEvt->SetFrom(netIdFrom);
				m_dispatcher.QueueEvent(newEvt);
				break;
			}

			case SlashCommand::REMOVEOBJECT: {
				int placementId = atoi(newString);
				IEvent* newEvt = new RemoveDynamicObjectEvent(placementId);
				newEvt->SetFrom(netIdFrom);
				m_dispatcher.QueueEvent(newEvt);
				break;
			}

		} // switch
	} // GM commands

	// ED-6323 - Clan Support
	switch (commandId) {
		case SlashCommand::CLANDISBAND: {
			LogInfo("/CLANDISBAND - " << pPO->ToStr());

			// Send Client ClanEvent::DISBAND
			IEvent* pEvent = new ClanEvent(ClanAction::DISBAND, pPO->m_clanName.GetString());
			pEvent->AddTo(pPO->m_netId);
			m_dispatcher.QueueEvent(pEvent);

			// Not In Clan ?
			if (pPO->m_clanHandle == NO_CLAN) {
				QueueMsgGeneralToClient(LS_GM_CLAN_YOU_LEFT, pPO);
				return;
			}

			// Remove From Clan
			if (!m_multiplayerObj->m_clanDatabase->DeleteClanByHandle(pPO->m_handle)) {
				// was not owner
				CClanSystemObj* pCSO = m_multiplayerObj->m_clanDatabase->GetClanByHandle(pPO->m_clanHandle);
				if (pCSO) {
					if (pCSO->m_memberList) {
						pCSO->RemoveMemberByHandle(pPO->m_handle);
						if (m_gameStateDb) {
							try {
								m_gameStateDb->RemoveMemberFromClan(pCSO->m_clanId, pPO->m_handle);
								pPO->m_dbDirtyFlags |= PlayerMask::PLAYER;
							} catch (KEPException* e) {
								LogError("DB Error RemoveMemberFromClan " << e->m_msg);
								e->Delete();
							}
						}
					}
				}
			} else {
				// was owner
				pPO->m_clanName = "";
				pPO->m_clanHandle = NO_CLAN;
				if (m_gameStateDb) {
					try {
						m_gameStateDb->DeleteClan(pPO->m_handle);
					} catch (KEPException* e) {
						LogError("DB Error DeleteClan " << e->m_msg);
						e->Delete();
					}
				}
			}
			pPO->m_clanHandle = NO_CLAN;
			QueueMsgGeneralToClient(LS_GM_CLAN_YOU_LEFT, pPO);
		}
			return;

		case SlashCommand::CLANCREATE: {
			LogInfo("/CLANCREATE - " << pPO->ToStr());

			// Already In Clan ?
			if (pPO->m_clanHandle != NO_CLAN) {
				QueueMsgGeneralToClient(LS_GM_ALREADY_IN_CLAN, pPO);
				return;
			}

			// Get Clan Name From Attribute
			CStringA clanName;
			for (UINT i = 0; i < csaAttr.size(); i++) {
				clanName += csaAttr.at(i).c_str();
				if (i + 1 < csaAttr.size())
					clanName += " ";
			}

			CClanSystemObj* newClan = 0;
			int result = m_multiplayerObj->m_clanDatabase->CreateClan(clanName, clanName, pPO->m_handle, &newClan);
			if (result == 1) {
				if (m_gameStateDb != 0) {
					ULONG clanId = 0;
					try {
						if (m_gameStateDb->CreateClan(pPO, clanName, clanId) && newClan) {
							newClan->m_clanId = clanId;
							pPO->m_dbDirtyFlags |= PlayerMask::PLAYER;

							// success
							QueueMsgGeneralToClient(LS_GM_NEW_CLAN_CREATED, pPO);
							pPO->m_clanHandle = pPO->m_handle;
							pPO->m_dbDirtyFlags |= PlayerMask::PLAYER;

							pPO->m_clanName = clanName;
							pPO->m_clanHandle = pPO->m_handle;

							// Send Client ClanEvent::Create
							IEvent* pEvent = new ClanEvent(ClanAction::CREATE, clanName.GetString());
							pEvent->AddTo(pPO->m_netId);
							m_dispatcher.QueueEvent(pEvent);
						} else {
							m_multiplayerObj->m_clanDatabase->DeleteClanByHandle(pPO->m_handle);
							QueueMsgGeneralToClient(LS_GM_CLAN_NAME_IN_USE, pPO);
						}
					} catch (KEPException* e) {
						LogError("DB Error CreateClan " << e->m_msg);
						e->Delete();
					}
				}
				return;
			} else if (result == 2) {
				QueueMsgGeneralToClient(LS_GM_CLAN_NAME_IN_USE, pPO);
				return;
			} else if (result == 3) {
				QueueMsgGeneralToClient(LS_GM_CLAN_NAME_TOO_LONG, pPO);
				return;
			}
		}
			return;

		case SlashCommand::CLANJOIN: {
			LogInfo("/CLANJOIN - " << pPO->ToStr());

			if (pPO->m_clanHandle != NO_CLAN) {
				QueueMsgGeneralToClient(LS_GM_ALREADY_IN_CLAN, pPO);
				return;
			}

			// Get Clan Name From Attribute
			CStringA clanName;
			for (UINT i = 0; i < csaAttr.size(); i++) {
				clanName += csaAttr.at(i).c_str();
				if (i + 1 < csaAttr.size())
					clanName += " ";
			}

			CClanSystemObj* pCSO = m_multiplayerObj->m_clanDatabase->GetClanByPreFix(clanName);
			if (!pCSO) {
				QueueMsgGeneralToClient(LS_GM_CLAN_NOT_FOUND, pPO);
				return;
			}

			auto pPO_leader = GetPlayerObjectByPlayerHandle(pCSO->m_guildMasterHandle);
			if (!pPO_leader) {
				QueueMsgGeneralToClient(LS_GM_CLAN_LEADER_NOT_FOUND, pPO);
			} else {
				RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO_leader->m_netId, eChatType::Talk, LS_CLAN_JOIN_REQ, pPO->m_charName);
				pPO->m_currentRequestToJoinClanHandle = pCSO->m_guildMasterHandle;
				QueueMsgGeneralToClient(LS_GM_CLAN_REQUEST_SENT, pPO);
				return;
			}

			// guildmaster not in game-see if it is the same account
			auto pAO = pPO->m_pAccountObject;
			if (!pAO || !pAO->m_pPlayerObjectList || !pAO->m_pPlayerObjectList->GetPlayerObjectByPlayerHandle(pCSO->m_guildMasterHandle))
				return;

			// char is on same account (auto approve)
			pPO->m_clanHandle = pCSO->m_guildMasterHandle;
			pPO->m_clanName = clanName;
			pPO->m_dbDirtyFlags |= PlayerMask::PLAYER;

			// Send Client ClanEvent::Create
			IEvent* pEvent = new ClanEvent(ClanAction::CREATE, clanName.GetString());
			pEvent->AddTo(pPO->m_netId);
			m_dispatcher.QueueEvent(pEvent);

			QueueMsgGeneralToClient(LS_GM_CLAN_ACCEPTED, pPO); // acceptance notification

			pCSO->AddMemberByHandle(pPO->m_handle, pPO->m_charName);

			if (m_gameStateDb) {
				try {
					m_gameStateDb->AddMemberToClan(pCSO->m_clanId, pPO->m_handle);
					pPO->m_dbDirtyFlags |= PlayerMask::PLAYER;
				} catch (KEPException* e) {
					LogError("DB Error AddMemberToClan " << e->m_msg);
					e->Delete();
				}
			}
		}
			return;

		case SlashCommand::CLANAPPROVE: {
			LogInfo("/CLANAPPROVE - " << pPO->ToStr());

			if (pPO->m_clanHandle != NO_CLAN) {
				CClanSystemObj* clan = m_multiplayerObj->m_clanDatabase->GetClanByHandle(pPO->m_handle);
				if (clan) {
					BOOL foundPlayerToApprove = FALSE;
					for (POSITION posLoc = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); posLoc != NULL;) {
						auto pPO_find = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(posLoc);
						if (pPO_find->m_currentRequestToJoinClanHandle == pPO->m_clanHandle) {
							if (!pPO_find->m_aiControlled) {
								if (pPO_find->m_charName == newString) {
									pPO_find->m_clanHandle = pPO->m_handle;
									pPO_find->m_dbDirtyFlags |= PlayerMask::PLAYER;
									QueueMsgGeneralToClient(LS_GM_CLAN_NEW_MEMBER_ACCEPTED, pPO);
									QueueMsgGeneralToClient(LS_GM_CLAN_ACCEPTED, pPO_find);
									pPO_find->m_currentRequestToJoinClanHandle = NO_CLAN; // not requesting anymore
									clan->AddMemberByHandle(pPO_find->m_handle, pPO_find->m_charName);
									if (m_gameStateDb) {
										try {
											m_gameStateDb->AddMemberToClan(clan->m_clanId, pPO_find->m_handle);
											pPO->m_dbDirtyFlags |= PlayerMask::PLAYER;
										} catch (KEPException* e) {
											LogError("DB Error AddMemberToClan " << e->m_msg);
											e->Delete();
										}
									}
									foundPlayerToApprove = TRUE;
									break;
								}
							}
						}
					}

					if (!foundPlayerToApprove)
						QueueMsgGeneralToClient(LS_GM_CLAN_CANT_FIND_PLAYER, pPO);
				}
			}
		}
			return;

		case SlashCommand::CLANREMOVE: {
			LogInfo("/CLANREMOVE - " << pPO->ToStr());

			if (pPO->m_clanHandle != NO_CLAN) {
				CClanSystemObj* pCSO = m_multiplayerObj->m_clanDatabase->GetClanByHandle(pPO->m_handle);
				if (pCSO) {
					if (pCSO->m_memberList) {
						PLAYER_HANDLE playerId = 0;
						if (pCSO->m_memberList->RemoveMemberByName(newString, &playerId)) {
							if (m_gameStateDb && playerId != 0) {
								try {
									m_gameStateDb->RemoveMemberFromClan(pCSO->m_clanId, playerId);
									auto pPO_remove = GetPlayerObjectByPlayerHandle(playerId);
									if (pPO_remove)
										pPO_remove->m_dbDirtyFlags |= PlayerMask::PLAYER;
								} catch (KEPException* e) {
									LogError("DB Error RemoveMemberFromClan " << e->m_msg);
									e->Delete();
								}
							}
							QueueMsgGeneralToClient(LS_GM_CLAN_MEMBER_REMOVED, pPO);
						} else {
							QueueMsgGeneralToClient(LS_GM_CLAN_CANT_FIND_MEMBER, pPO);
						}
					}
				}
			}
		}
			return;
	} // End Clan Support

	switch (commandId) {
		case SlashCommand::HELP:
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "Help Topics:", eChatType::Talk);
			if (pPO->m_admin == TRUE || pPO->m_gm == TRUE) {
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "GM Help - Type /HELPGM", eChatType::Talk);
				m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "Admin Help - Type /HELPADMIN", eChatType::Talk);
			}
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "General Help - Type /HELPGEN", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "Clan Help - Type /HELPCLAN", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "Chat Help - Type /HELPCHAT", eChatType::Talk);
			break;

		case SlashCommand::HELPGEN:
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "General Help:", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/LOCATION - Display Your current location", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/BUY - Open a store vendor menu", eChatType::Talk);
			break;

		case SlashCommand::HELPCLAN:
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "Clan Help:", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/CLANCREATE {Name} - Create a new clan", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/CLANDISBAND {Name} - Delete your clan", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/CLANJOIN {Name} - Send a request to join a clan", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/CLANAPPROVE {PlayerName} - Accept a player to your clan", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/CLANREMOVE {PlayerName} - Remove a player from your clan", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/CLANROSTER - List clan members", eChatType::Talk);
			break;

		case SlashCommand::HELPCHAT:
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "Chat Help:", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/TELL {Playername} {Message} - Say something privately to that player", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/REPLY - Reply privately to last player in private chat", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/WHISPER - Say something only to people close to you", eChatType::Talk);
			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "/SHOUT - Shout something to the entire place", eChatType::Talk);
			break;

		case SlashCommand::LOCATION: {
			CStringA csLocation;
			CStringA now(Utf16ToUtf8(CTime::GetCurrentTime().Format(L"%Y/%m/%d %H:%M")).c_str());

			MovementData md = pPO->InterpolatedMovementData();
			float rotDeg = pPO->InterpolatedRotDeg();

			if (pPO->m_gm) {
				csLocation.Format("x=%.1f y=%.1f z=%.1f rot=%.1f ip=%s port=%d serverId=%d gameId=%d zoneId=%x instanceId=%d time=%s",
					md.pos.x, md.pos.y, md.pos.z,
					rotDeg,
					m_ipAddress.c_str(),
					BaseEngine::port(),
					m_multiplayerObj->m_serverNetwork->pinger().GetServerId(),
					m_multiplayerObj->m_serverNetwork->pinger().GetGameId(),
					pPO->m_currentZoneIndex.GetULong(),
					pPO->m_currentZoneIndex.GetInstanceId(),
					now);
			} else {
				csLocation.Format("x=%.1f y=%.1f z=%.1f rot=%.1f serverId=%d gameId=%d zoneId=%x instanceId=%d time=%s",
					md.pos.x, md.pos.y, md.pos.z,
					rotDeg,
					m_multiplayerObj->m_serverNetwork->pinger().GetServerId(),
					m_multiplayerObj->m_serverNetwork->pinger().GetGameId(),
					pPO->m_currentZoneIndex.GetULong(),
					pPO->m_currentZoneIndex.GetInstanceId(),
					now);
			}

			m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, csLocation, eChatType::Talk);
			break;
		}

		case SlashCommand::LOOT:
			LootInventoryRequest(pPO, netIdFrom);
			break;

		case SlashCommand::REBIRTH:
			HandleReassignRespawn(pPO, netIdFrom);
			break;

		case SlashCommand::PAGEGM:
			m_gmPageListRuntime->AddPage(pPO->m_handle);
			QueueMsgGeneralToClient(LS_GM_GMPAGE_COMPLETE, pPO);
			break;

		case SlashCommand::BUY: {
#if (DRF_OLD_VENDORS == 1)
			Vector3f pos = pPO->InterpolatedPos();
			CCommerceObj* cmPtr = m_commerceDB->GetEstablishmentByPosition(pos, pPO->m_currentChannel);
			if (cmPtr) {
				// found store zone
				SendMsgBlockStoreToClient(cmPtr->m_uniqueID, pPO);
			}
#endif
			break;
		}

		case SlashCommand::GOHOME:
			if (pPO->m_housingZoneIndex == INVALID_ZONE_INDEX) {
				RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::System, LS_NO_HOME);
				LogError("INVALID HOUSING ZONE INDEX - " << pPO->ToStr());
			} else {
				// now fire event to send them to their home, which may switch servers
				GotoPlaceEvent* gpe = new GotoPlaceEvent();
				gpe->GotoApartment(pPO->m_pAccountObject->m_serverId, pPO->m_pAccountObject->m_currentcharInUse);
				gpe->SetFrom(netIdFrom);
				m_dispatcher.QueueEvent(gpe);
			}
			break;

		case SlashCommand::GOTOFRIEND:
			break;

	} // end switch

	if (csaAttr.size() > 0) { //we have at least 1 attribute
		switch (commandId) {
			case SlashCommand::TELL:
				if (newStringTrimmed.GetLength() > 1) {
					// strip off dest name
					const char* destUserName = csaAttr[0].c_str();
					const char* msgText = strstr(newString, destUserName);
					msgText += csaAttr[0].length();
					while (*msgText == ' ') // trim leading
						msgText++;

					CStringA csTemp = pPO->m_charName;
					csTemp += " tells ";
					// csTemp += destUserName;
					csTemp += "you";
					csTemp += " > ";
					csTemp += msgText;

					CStringA echoMsg = "You tell ";
					echoMsg += destUserName;
					echoMsg += " > ";
					echoMsg += msgText;

					// first see if they are on this server
					CPlayerObject* dest = 0;

					if (m_gameStateDb) {
						try {
							if (m_gameStateDb->IsUserBlocked(pPO->m_pAccountObject, 0, pPO->m_pAccountObject->m_serverId, BlockType::Ignore, destUserName)) {
								// echo to sender to fake out that they sent it
								m_multiplayerObj->QueueOrSendRenderTextEvent(netIdFrom, echoMsg, eChatType::Private);
								return;
							}
						} catch (KEPException* e) {
							LogError("DB Error IsUserBlocked " << e->m_msg);
							e->Delete();
						}
					}

					for (POSITION posLoc = m_multiplayerObj->m_runtimePlayersOnServer->GetHeadPosition(); (posLoc) != NULL;) {
						dest = (CPlayerObject*)m_multiplayerObj->m_runtimePlayersOnServer->GetNext(posLoc);
						if (dest->m_charName.CompareNoCase(destUserName) == 0)
							break;
						else
							dest = 0;
					}

					if (dest && dest->m_netId != 0) {
						m_multiplayerObj->QueueOrSendRenderTextEvent(dest->m_netId, csTemp, eChatType::Private);

						// echo to sender
						m_multiplayerObj->QueueOrSendRenderTextEvent(netIdFrom, echoMsg, eChatType::Private);
					} else { // player not on this server
						assert(false);
						LogError("/tell a remote user is NOT supported");
					}
				} else {
					RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, netIdFrom, eChatType::System,
						LS_PLAYER_NOT_FOUND,
						"\"\"");
				}
				break;

			case SlashCommand::SHOUT:
				m_multiplayerObj->BroadcastMessage(20000,
					pPO->m_charName + " Shouts > " + newString,
					pPO->InterpolatedPos(),
					pPO->m_currentChannel,
					pPO->m_netId);
				break;

			case SlashCommand::WHISPER:
				m_multiplayerObj->BroadcastMessage(50,
					pPO->m_charName + " Whispers > " + newString,
					pPO->InterpolatedPos(),
					pPO->m_currentChannel,
					pPO->m_netId);
				break;

			case SlashCommand::EMOTE:
				m_multiplayerObj->BroadcastMessage(1000,
					pPO->m_charName + " " + newString,
					pPO->InterpolatedPos(),
					pPO->m_currentChannel,
					pPO->m_netId,
					eChatType::Emote);
				break;

			case SlashCommand::TEAM:
				m_multiplayerObj->BroadcastMessage(0,
					newString,
					pPO->InterpolatedPos(),
					pPO->m_currentChannel,
					pPO->m_netId,
					eChatType::Team);
				break;

			case SlashCommand::TRIBE:
				m_multiplayerObj->BroadcastMessage(0,
					newString,
					pPO->InterpolatedPos(),
					pPO->m_currentChannel,
					pPO->m_netId,
					eChatType::Race);
				break;

			case SlashCommand::KICK: {
				std::string userName = csaAttr.at(0);
				if (userName == "all")
					KickAllPlayers(pPO, false);
				else if (userName == "ALL")
					KickAllPlayers(pPO, true);
				else if (pPO->m_charName.CompareNoCase(userName.c_str()) == 0)
					m_multiplayerObj->QueueOrSendRenderTextEvent(pPO->m_netId, "Cannot kick yourself", eChatType::System);
				else
					KickPlayer(pPO, GetPlayerObjectByName(userName), userName, false);
				break;
			}

			case SlashCommand::DESTROYSKILL: {
				CSkillObject* skillPtrRef = m_skillDatabase->GetSkillByName(csaAttr.at(0).c_str());
				if (skillPtrRef) {
					if (pPO->m_skills) {
						CSkillObject* onPlayerSkill = pPO->m_skills->GetSkillByType(skillPtrRef->m_skillType);
						if (onPlayerSkill) {
							onPlayerSkill->m_currentLevel = 1;
							onPlayerSkill->m_currentExperience = 5.0f;
							pPO->m_dbDirtyFlags |= PlayerMask::SKILLS;
							QueueMsgGeneralToClient(LS_GM_COMPLETED, pPO); // acceptance notification
						}
					}
				}
				break;
			}

			case SlashCommand::DESTROYJNLENTRY:
				if (pPO->playerOrigRef()) {
					if (pPO->playerOrigRef()->m_questJournal->DeleteEntryByNpcName(csaAttr.at(0).c_str())) {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::Talk, LS_JOURNAL_DEL);
						pPO->m_dbDirtyFlags |= PlayerMask::QUESTS;
					} else {
						RenderLocaleTextEvent::SendLocaleTextTo(m_dispatcher, pPO->m_netId, eChatType::Talk, LS_JOURNAL_DEL_MISSING);
					}
				}
				break;
		} // end switch
	} //Attr count > 0
}

CHPlacementObj* ServerEngine::GetHousePlacementObjFromArea(CPlayerObject* pPO) {
	// use this function to return houses that you can control
	if (pPO->m_aiControlled)
		return NULL;

	CHPlacementObj* pHPO = m_multiplayerObj->m_housingDB->GetHouseByPositionAndWldID(pPO->InterpolatedPos(), pPO->m_currentChannel);
	if (!pHPO)
		return NULL;

	if (pHPO->m_playerHandle == pPO->m_handle)
		return pHPO;

	if (pPO->m_clanHandle != NO_CLAN) {
		CClanSystemObj* pCSO = m_multiplayerObj->m_clanDatabase->GetClanByHandle(pPO->m_clanHandle);
		if (!pCSO)
			return NULL;

		if (pCSO->m_houseLink == pHPO->m_playerHandle)
			return pHPO;
	}

	return NULL;
}

void ServerEngine::SpawnToPointInWld(CPlayerObject* pPO, const Vector3f& newPos, const ZoneIndex zoneIndex, EDestinationType destType, int rotation, const char* url) {
	if (!pPO)
		return;

	auto pAO = pPO->m_pAccountObject;
	if (!pAO || pPO->m_aiControlled)
		return;

	initiateSpawn(pPO, pAO, zoneIndex, newPos, rotation, destType, url);
}

EVENT_PROC_RC ServerEngine::CanSpawnToZoneBackgroundHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	EVENT_PROC_RC ret = EVENT_PROC_RC::OK;
	CanSpawnToZoneBackgroundEvent* csze = dynamic_cast<CanSpawnToZoneBackgroundEvent*>(e);
	ServerEngine* me = (ServerEngine*)lparam;

	// find the account that started this
	auto pAO = me->m_multiplayerObj->m_accountDatabase->GetAccountObjectByNetId(e->From());
	if (pAO == 0) {
		LogWarn("Lost account during background processing playerId = " << csze->ObjectId());
		return EVENT_PROC_RC::NOT_PROCESSED;
	}

	if (me->m_gameStateDb != NULL && csze->ret == ZONE_FOUND) {
		// check for saved custom spawn points
		float x = 0.0f, y = 0.0f, z = 0.0f, rx = 0.0f, ry = 0.0f, rz = 0.0f;
		if (me->m_gameStateDb->GetPlayerSpawnPoint(csze->playerId, csze->zoneIndex, x, y, z, rx, ry, rz)) {
			// Override spawn point
			csze->spawnPt.x = x;
			csze->spawnPt.y = y;
			csze->spawnPt.z = z;
			csze->rotation = (int)(atan2(rx, rz) * 180.0f / M_PI);
			LogInfo("Player[" << csze->playerId << "] " << csze->zoneIndex.ToStr() << " spawn point = {" << x << ", " << y << ", " << z << "}");
		}
	}

	// finish can spawn, if needed
	if (me->m_gameStateDb != 0 && csze->isAsync) {
		if (csze->ret == ZONE_FOUND) {
			std::string server;
			ULONG port = 0;

			if (me->m_gameStateDb->MapServerId(csze->newServerId, server, port)) {
				if (!server.empty() && port != 0) {
					// sending them elsewhere, mark them as logged off now
					// to avoid lag of client doing it during switch
					auto pPO = me->GetPlayerObjectByAliveAndAccountNumber(pAO->m_accountNumber);
					if (pPO)
						pPO->playerOrigRef()->m_reconnecting = true; // avoids logout msg since just switching

					if (csze->selectedChar < 0)
						csze->selectedChar = 0; // WOK Specific if redirecting on initial spawn, they haven't picked char yet

					// need to goto another server
					// fire event back to client with server and port
					GotoPlaceEvent* switchEvent = new GotoPlaceEvent();
					switchEvent->GotoZone(csze->zoneIndex, csze->zoneIndex.GetInstanceId(), csze->selectedChar,
						csze->spawnPt.x, csze->spawnPt.y, csze->spawnPt.z, csze->rotation, server.c_str(), port);
					switchEvent->AddTo(e->From());

					me->spawnToOtherServer(pAO, switchEvent);

					return EVENT_PROC_RC::OK;
				} else {
					// Spawn locally
					SpawnInfo& spawnInfo = pAO->GetSpawnInfo();

					// no cover charge for GMs
					if (pAO->m_gm) {
						spawnInfo.setCoverCharge(0, 0);
					} else {
						spawnInfo.setCoverCharge(csze->coverCharge, csze->zoneOwnerId);
					}

					if (csze->zonePermissions != ZP_UNKNOWN) {
						spawnInfo.setZonePermissions(csze->zonePermissions); // Scenario 2: spawn triggered by other events than GotoPlaceEvent
					} else {
						// Bug detection: ZONE_FOUND but csze->zonePermissions not set.
						LogError("Background handler didn't fill in zone permissions"
								 << ", player " << csze->playerId
								 << ", zone " << csze->zoneIndex.toLong());
					}

					spawnInfo.setParentZoneInstanceId(csze->parentZoneInstanceId);
				}

			} else {
				// remote server not found, if will log msg
				csze->ret = ZONE_NOT_FOUND;
			}

			// ok, spawn on this server if get here

		} else {
			// can't access that zone, send them to their apt
			if (!me->SendGotoError(csze->ret, me->m_dispatcher, e->From(), LS_SPAWN_NO_PERMISSION, csze->selectedChar) &&
				csze->sendHomeIfFail) {
				LogWarn("Player can't spawn to zone, sending home " << pAO->m_accountName);

				// send them to their apartment by firing gotoplace event "from" them
				GotoPlaceEvent* switchEvent = new GotoPlaceEvent();
				switchEvent->GotoApartment(pAO->m_serverId, csze->selectedChar);
				switchEvent->SetFrom(e->From()); // fake out sender
				me->m_dispatcher.QueueEvent(switchEvent);
			}

			return EVENT_PROC_RC::OK;
		}
	}

	// local spawn if get here
	if (csze->ret == ZONE_FOUND) {
		std::string worldFile;
		CWorldAvailableObj* wldRefObject = me->m_multiplayerObj->m_currentWorldsUsed->GetByZoneIndex(csze->zoneIndex);
		if (!wldRefObject) {
#ifndef REFACTOR_INSTANCEID
			LogWarn("Invalid world spawn ref for zone index " << csze->zoneIndex << " for account " << pAO->m_accountName);
#else
			LogWarn("Invalid world spawn ref for zone index " << csze->zoneIndex.toLong() << " for account " << pAO->m_accountName);
#endif

			return EVENT_PROC_RC::NOT_PROCESSED;
		} else {
			worldFile = wldRefObject->m_worldName.GetString();
		}

		// If players are coming from another server or meet me 3D then this event
		// will have cover charge filled in from the database.  If they are staying
		// on the same server then the cover charge will be applied later in
		// CMultiplayerObj::SendSpawnMsgToClient
		bool sendHomeOnFail = false;
		if (csze->coverCharge > 0)
			sendHomeOnFail = true;

		//
		// Make sure the account object is updated with the correct script server Id.
		// Was on player object, but might not have a player object at this point, apparently.
		// Is this ok?
		//
		if (csze->isAsync) {
			pAO->m_scriptServerIdPrev = pAO->m_scriptServerId;
			pAO->m_scriptServerId = csze->scriptServerId;
		}

		if (me->m_multiplayerObj->SetMsgSpawnToClientOnAccountObject(pAO, csze->EDBIndex, csze->zoneIndex, worldFile, csze->url, csze->spawnPt, csze->rotation, sendHomeOnFail)) {
			me->m_multiplayerObj->SendPendingSpawnEventToClient(pAO->GetSpawnInfo(), e->From(), csze->zoneIndex, sendHomeOnFail);
		}
	}
	return ret;
}

void ServerEngine::initiateSpawn(
	CPlayerObject* pPO,
	CAccountObject* pAO,
	const ZoneIndex& zi,
	const Vector3f& newPos,
	int rotation,
	EDestinationType destType,
	const char* _url,
	bool sendHomeIfFail) {
	if (pAO->m_pPlayerObjectList->GetCount() == 0) {
		LogError("Attempting to zone w/o any characters for " << pAO->m_accountName);
		return; // no characters?!
	}

	CanSpawnToZoneBackgroundEvent* e = new CanSpawnToZoneBackgroundEvent(pAO->m_netId,
		pAO->m_serverId,
		pAO->m_gm ? true : false,
		pAO->m_country.c_str(),
		pAO->m_birthDate.c_str(),
		pAO->m_showMature,
		pAO->m_currentcharInUse,
		pPO->m_handle,
		pPO->m_altCfgIndex,
		pPO->m_curEDBIndex,
		zi, newPos,
		rotation,
		destType == DT_Player || destType == DT_ValidatedPlayer,
		_url, sendHomeIfFail);
	e->scriptServerId = pAO->m_scriptServerId;
	if (destType > DT_ValidatedPlayer) { // need check it
		m_gameStateDispatcher->QueueEvent(e); // need to check if can spawn, usual case
	} else {
		// just call it with data in event
		e->isAsync = false;
		CanSpawnToZoneBackgroundHandler((ULONG)this, &m_dispatcher, e);
		e->Delete();
	}
}

} // namespace KEP