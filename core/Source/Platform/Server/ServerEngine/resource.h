///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


#define IDS_LOADGAMESTATE_FAILED        3105
#define IDS_NO_GAME_STATE               3106
#define IDS_SERVER_LISTENING            3107
#define IDS_LOADING_GAME_STATE          3108
#define IDS_CURRENT_PLAYER_COUNT        3109
#define IDS_ARENA_CLONE_FAILED          3110
#define IDS_TICKCOUNT_CLOSE             3111
#define IDS_NO_AI_SERVER_FOUND          3113
#define IDS_ATTEMPT_TO_DELETE_CHAR_INUSE 3114
#define IDS_FALL_THRU_WORLD             3115
#define IDS_ADMIN_MODE                  3116
#define IDS_DEV_MODE                    3117
#define IDS_MESSAGE_BROADCAST           3118
#define IDS_AI_SERVER_CHAR_SEL          3119
#define IDS_BAD_ZONE_INSTANCE           3120
#define IDS_SAVE_STATE_DB_ERROR         3121
#define IDS_DB_DISABLED                 3122
#define IDS_DB_BACKUP_OFF               3123
#define IDS_GET_CHANGE_APT_DBERR        3124
#define IDS_CHANGE_APT_DBERR            3124
#define IDS_BANKCASH_DBERR              3125
#define IDS_SPAWNPT_NOT_FOUND           3126
#define IDS_ZONECK_DBERR                3127
#define IDS_STRING3128                  3128
#define IDS_SPAWN_PERMISSION            3128
#define IDS_ERROR_LOADING_XML           3129
#define IDS_SAVE_ON_EXIT_TURNED_ON      3130

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        3000
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         3000
#define _APS_NEXT_SYMED_VALUE           3000
#endif
#endif
