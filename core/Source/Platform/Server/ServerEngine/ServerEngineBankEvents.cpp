///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ServerEngine.h"

#include "CCommerceClass.h"
#include "CBankClass.h"
#include "CSafeZones.h"
#include "CSecureTradeClass.h"
#if (DRF_OLD_PORTALS == 1)
#include "CPortalSysClass.h"
#endif
#include "CElementsClass.h"
#include "CCollisionClass.h"
#include "CGmPageClass.h"
#include "CWorldChannelRules.h"
#include "CBattleSchedulerClass.h"
#if (DRF_OLD_VENDORS == 1)
#include "CServerItemGen.h"
#endif
#include "CCurrencyClass.h"
#include "CPlayerClass.h"

#include "IGameState.h"
#include "resource.h"

namespace KEP {

static LogInstance("ServerEngine");

BOOL ServerEngine::UpdatePlayersCash(IPlayer *player, long player1AmtChanging, TransactionType transactionType, int glid, long qty, CurrencyType currencyType) {
	BOOL ret = FALSE;

	CPlayerObject *cPlayer = dynamic_cast<CPlayerObject *>(player);

	if (!cPlayer) {
		LOG4CPLUS_ERROR(m_logger, "UpdatePlayersCash got null player reference, cancel cash update");
		return ret;
	}

	if (IGameState::UPDATE_OK == UpdatePlayersCash(cPlayer, player1AmtChanging, transactionType, glid, qty, currencyType))
		ret = TRUE;

	return ret;
}

IGameState::EUpdateRet ServerEngine::UpdatePlayersCash(CPlayerObject *player1, long player1AmtChanging, TransactionType transactionType, int glid, long qty, CurrencyType currencyType) {
	IGameState::EUpdateRet ret = IGameState::UPDATE_OK;

	if (m_gameStateDb != 0) {
		try {
			ret = m_gameStateDb->UpdatePlayersCash(player1, player1AmtChanging, transactionType, 0, 0, currencyType, glid, qty);
		} catch (KEPException *e) {
			LOG4CPLUS_ERROR(m_logger, "DB Error updating player's cash " << player1->m_charName << " " << e->m_msg);
			e->Delete();
			ret = IGameState::UPDATE_ERROR;
		}

	} else {
		if (currencyType == CurrencyType::CREDITS_ON_PLAYER) {
			if (player1->m_cash->m_amount + player1AmtChanging < 0)
				return IGameState::UPDATE_PLAYER1_INSUFFICIENT;

			player1->m_cash->m_amount += player1AmtChanging;
		} else if (currencyType == CurrencyType::GIFT_CREDITS_ON_PLAYER) {
			if (player1->m_giftCredits->m_amount + player1AmtChanging < 0)
				return IGameState::UPDATE_PLAYER1_INSUFFICIENT;

			player1->m_giftCredits->m_amount += player1AmtChanging;

		} else if (currencyType == CurrencyType::CREDITS_IN_BANK) {
			if (player1->m_bankCash->m_amount + player1AmtChanging < 0)
				return IGameState::UPDATE_PLAYER1_INSUFFICIENT;

			player1->m_bankCash->m_amount += player1AmtChanging;
		}
	}

	return ret;
}

} // namespace KEP