///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ServerEngine.h"
#include "CAccountClass.h"
#include "CPlayerClass.h"
#include "common\include\LogHelper.h"
#include "LocaleStrings.h"
#include "BlockType.h"

namespace KEP {

static LogInstance("ServerEngine");

void ServerEngine::P2pAnimAdd(int p2pIndex, const P2pAnimData& p2pAnimData) {
	m_p2pAnimations.insert(p2pAnimPair(p2pIndex, p2pAnimData));
	LogInfo("OK - index=" << p2pIndex);
}

void ServerEngine::P2pAnimDel(NETID netTraceId) {
	for (auto itr = m_p2pAnimations.begin(); itr != m_p2pAnimations.end(); ++itr) {
		P2pAnimData& p2pAnimData = itr->second;
		if (p2pAnimData.netTraceId_init == netTraceId || p2pAnimData.netTraceId_target == netTraceId) {
			LogInfo("OK - netTraceId=" << netTraceId << " index=" << itr->first);
			m_p2pAnimations.erase(itr);
			return;
		}
	}
	LogError("NOT FOUND - netTraceId=" << netTraceId);
}

bool ServerEngine::P2pAnimTerminate(NETID netId) {
	// Get P2P Requestor
	auto itr = m_p2pPlayers.find(netId);
	if (itr == m_p2pPlayers.end())
		return false;

	// Get P2P Partner
	NETID netId_partner = itr->second;

	// Remove Both Player Pairs From P2P Map
	m_p2pPlayers.erase(netId);
	m_p2pPlayers.erase(netId_partner);

	// Send Termination Event To Both Players
	P2pAnimData p2pAnimData;
	p2pAnimData.animState = eP2pAnimState::Terminate;
	IEvent* pEvent = new P2pAnimEvent(p2pAnimData);
	pEvent->AddTo(netId);
	pEvent->AddTo(netId_partner);
	m_dispatcher.QueueEvent(pEvent);

	// Flag No Longer P2P'ing
	auto pPO = GetPlayerObjectByNetId(netId, false);
	if (pPO)
		pPO->m_interactionStates &= ~P2PANIM;
	auto pPO_partner = GetPlayerObjectByNetId(netId_partner, false);
	if (pPO_partner)
		pPO_partner->m_interactionStates &= ~P2PANIM;

	LogInfo("OK - " << (pPO ? pPO->ToStr() : "<null>") << " -> " << (pPO_partner ? pPO_partner->ToStr() : "<null>"));

	// Remove Pending P2P
	auto itr_pend = m_pendingP2pAnim.find(netId);
	if (itr_pend != m_pendingP2pAnim.end())
		m_pendingP2pAnim.erase(netId);
	auto itr_pend_partner = m_pendingP2pAnim.find(netId_partner);
	if (itr_pend_partner != m_pendingP2pAnim.end())
		m_pendingP2pAnim.erase(netId_partner);

	// Delete P2p Animation Data
	if (!pPO)
		pPO = pPO_partner;
	if (pPO)
		P2pAnimDel(pPO->m_netTraceId);

	return true;
}

EVENT_PROC_RC ServerEngine::P2pAnimRequestHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ServerEngine*)lparam;
	auto pEvent = dynamic_cast<P2pAnimRequestEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	int p2pIndex = 0;
	NETID netId_init = pEvent->From();
	int netTraceId_target = 0;
	pEvent->ExtractInfo(netTraceId_target, p2pIndex);

	//Dont let initiator spam requests.
	auto p2pPending_iter = me->m_pendingP2pAnim.find(netId_init);
	if (p2pPending_iter != me->m_pendingP2pAnim.end())
		return EVENT_PROC_RC::OK;

	auto pPO_init = me->GetPlayerObjectByNetId(netId_init, false);
	auto pPO_target = me->GetPlayerObjectByNetTraceId(netTraceId_target);
	if (!pPO_init || !pPO_target)
		return EVENT_PROC_RC::OK;

	if (pPO_target->m_interactionStates != AVAILABLE) {
		RenderLocaleTextEvent::SendLocaleTextTo(me->m_dispatcher, pPO_init->m_netId, eChatType::System, LS_PLAYER_BUSY);
		return EVENT_PROC_RC::OK;
	}

	//Has target blocked initiator?
	if (me->m_gameStateDb && me->m_gameStateDb->IsUserBlocked(pPO_init->m_pAccountObject, 0, pPO_target->m_pAccountObject->m_serverId, BlockType::Ignore, pPO_target->m_charName))
		return EVENT_PROC_RC::OK;

	NETID netId_target = pPO_target->m_netId;

	//See if the requested target is already involved in p2p animation
	auto p2pPlayer_iter = me->m_p2pPlayers.find(netId_target);
	if (p2pPlayer_iter != me->m_p2pPlayers.end())
		return EVENT_PROC_RC::OK;

	auto p2pAnim_iter = me->m_p2pAnimations.find(p2pIndex);
	if (p2pAnim_iter == me->m_p2pAnimations.end()) {
		if (me->m_gameStateDb != 0) {
			BOOL animFound = me->m_gameStateDb->CheckForNewP2pAnimation(p2pIndex);
			if (!animFound)
				return EVENT_PROC_RC::OK;
		} else {
			LogError("Game state database null!");
			return EVENT_PROC_RC::OK;
		}
	}

	//Check zone and player access passes
	PassList* animPasses = me->GetAnimationPassList((GLID)(int)me->m_p2pAnimations[p2pIndex].animType_init);
	PassList* zonePassList = me->GetZonePassList(pPO_init->m_currentZoneIndex);
	if (!PassList::canUseItemInZone(animPasses, zonePassList)) {
		RenderLocaleTextEvent::SendLocaleTextTo(me->m_dispatcher, pPO_init->m_netId, eChatType::System, LS_ZONE_NEEDS_PASS);
		return EVENT_PROC_RC::OK;
	} else {
		if (!PassList::canPlayerUseItem(pPO_init->m_passList, animPasses)) {
			RenderLocaleTextEvent::SendLocaleTextTo(me->m_dispatcher, pPO_init->m_netId, eChatType::System, LS_MISSING_PASS);
			return EVENT_PROC_RC::OK;
		} else {
			if (!PassList::canPlayerUseItem(pPO_target->m_passList, animPasses)) {
				RenderLocaleTextEvent::SendLocaleTextTo(me->m_dispatcher, pPO_init->m_netId, eChatType::System, LS_P2P_PARTNER_NO_PASS);
				return EVENT_PROC_RC::OK;
			}
		}
	}

	me->m_pendingP2pAnim.insert(p2pPendingPair(netId_init, p2pIndex));

	// Send P2P Request Event To Target Player
	IEvent* pEvent_perm = new P2pAnimPermissionEvent(pPO_init->m_netTraceId, me->m_p2pAnimations[p2pIndex].animName);
	pEvent_perm->AddTo(netId_target);
	me->m_dispatcher.QueueEvent(pEvent_perm);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::P2pAnimPermissionReplyHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	auto me = (ServerEngine*)lparam;
	auto pEvent = dynamic_cast<P2pAnimPermissionReplyEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	int netTraceId_init = 0;
	int answer = 0;
	NETID netId_target = pEvent->From();
	pEvent->ExtractInfo(netTraceId_init, answer);

	auto pPO_init = me->GetPlayerObjectByNetTraceId(netTraceId_init);
	auto pPO_target = me->GetPlayerObjectByNetId(netId_target, false);
	if (!pPO_init || !pPO_target)
		return EVENT_PROC_RC::OK;

	NETID netId_init = pPO_init->m_netId;

	//The player agreed to a p2p animation, get it started.
	//Make sure everyone is still available.  It is possible to set
	//up a love triangle where everyone a request to the person on
	//their right, so we want to take the first one to respond and
	//lock the others out.
	if (answer && (pPO_init->m_interactionStates == AVAILABLE) && (pPO_target->m_interactionStates == AVAILABLE)) {
		//Put both into active p2pPlayers map
		me->m_p2pPlayers.insert(p2pPlayersPair(netId_init, netId_target));
		me->m_p2pPlayers.insert(p2pPlayersPair(netId_target, netId_init));

		// Get P2p Animation Data
		int p2pIndex = me->m_pendingP2pAnim[netId_init];
		P2pAnimData p2pAnimData = me->m_p2pAnimations[p2pIndex];
		p2pAnimData.netTraceId_init = pPO_init->m_netTraceId;
		p2pAnimData.netTraceId_target = pPO_target->m_netTraceId;

		// Schedule Synced Start Time (give 300ms to begin incase of event latency)
		p2pAnimData.animStartTimeMs = fTime::TimeMs() + 300.0;

		// Send P2P Begin Event To Both Players
		p2pAnimData.animState = eP2pAnimState::Begin;
		IEvent* pEvent_begin = new P2pAnimEvent(p2pAnimData);
		pEvent_begin->AddTo(netId_init);
		pEvent_begin->AddTo(netId_target);
		me->m_dispatcher.QueueEvent(pEvent_begin);

		pPO_init->m_interactionStates |= P2PANIM;
		pPO_target->m_interactionStates |= P2PANIM;
	}

	me->m_pendingP2pAnim.erase(netId_init);

	return EVENT_PROC_RC::OK;
}

EVENT_PROC_RC ServerEngine::P2pAnimEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ServerEngine* me = (ServerEngine*)lparam;
	auto pEvent = dynamic_cast<P2pAnimEvent*>(e);
	if (!me || !pEvent)
		return EVENT_PROC_RC::NOT_PROCESSED;

	// Decode Event Data
	P2pAnimData p2pAnimData;
	pEvent->ExtractInfo(p2pAnimData);

	if (p2pAnimData.animState == eP2pAnimState::Terminate)
		me->P2pAnimTerminate(pEvent->From());

	return EVENT_PROC_RC::OK;
}

} // namespace KEP