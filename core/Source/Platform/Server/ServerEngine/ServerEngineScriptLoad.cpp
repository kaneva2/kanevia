///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ServerEngine.h"

#include <KEPHelpers.h>
using namespace KEP;

#include "EngineStrings.h"

#include <comutil.h>

#include "CCommerceClass.h"
#include "CBankClass.h"
#include "CSafeZones.h"
#include "CSecureTradeClass.h"
#if (DRF_OLD_PORTALS == 1)
#include "CPortalSysClass.h"
#endif
#include "CElementsClass.h"
#include "CCollisionClass.h"
#include "CGmPageClass.h"
#include "CWorldChannelRules.h"
#include "CBattleSchedulerClass.h"
#if (DRF_OLD_VENDORS == 1)
#include "CServerItemGen.h"
#endif

#include "SerializeFromFile.h"
#include "SafeDeleteList.h"

static LogInstance("ServerEngine");

BOOL ServerEngine::LoadGlobalCommerceData(const std::string& fileName) {
	CCommerceObjList* temp = 0;
	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), temp, m_logger)) {
		LogError("SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	} else {
		SafeDeleteList(m_commerceDB);
		m_commerceDB = temp;
	}

	return TRUE;
}

BOOL ServerEngine::LoadBankZonesData(const std::string& fileName) {
	CBankObjList* temp = 0;
	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), temp, m_logger)) {
		LogError("SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	} else {
		SafeDeleteList(m_bankZoneList);
		m_bankZoneList = temp;
	}

	return TRUE;
}

#if (DRF_OLD_VENDORS == 1)
BOOL ServerEngine::LoadItemGeneratedData(const std::string& fileName) {
	CServerItemGenObjList* temp = 0;
	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), temp, m_logger)) {
		LogError("SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	} else {
		SafeDeleteList(m_serverItemGenerator);
		m_serverItemGenerator = temp;
		LogInfo("'" << fileName << "' -> serverItemGeneratorObjs=" << m_serverItemGenerator->GetCount());
	}

	return TRUE;
}
#endif

#if (DRF_OLD_PORTALS == 1)
BOOL ServerEngine::LoadPortalDatabase(const std::string& fileName) {
	CPortalSysList* temp = 0;
	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), temp, m_logger)) {
		LogError("SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	} else {
		SafeDeleteList(m_portalDatabase);
		m_portalDatabase = temp;
	}
	return TRUE;
}
#endif

BOOL ServerEngine::LoadArenaSys(const std::string& fileName) {
	CBattleSchedulerList* temp = 0;
	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), temp, m_logger)) {
		LogError("SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	} else {
		SafeDeleteList(m_battleSchedulerSys);
		m_battleSchedulerSys = temp;
	}

	return TRUE;
}

BOOL ServerEngine::LoadWorldRules(const std::string& fileName) {
	CWorldChannelRuleList* temp = 0;
	if (!SerializeFromFile(PathAdd(PathGameFiles(), fileName), temp, m_logger)) {
		LogError("SerializeFromFile() FAILED - '" << fileName << "'");
		return FALSE;
	} else {
		SafeDeleteList(m_worldChannelRuleDB);
		m_worldChannelRuleDB = temp;
	}

	return TRUE;
}
