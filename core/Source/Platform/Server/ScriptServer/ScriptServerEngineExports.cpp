///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Engine/Blades/BladeExports.h"

#include "ScriptServerEngine.h"

#include "Engine/Blades/IBlade.h"
using namespace KEP;

IMPLEMENT_CREATE(ScriptServerEngine)

BEGIN_BLADE_LIST
ADD_BLADE(ScriptServerEngine)
END_BLADE_LIST
