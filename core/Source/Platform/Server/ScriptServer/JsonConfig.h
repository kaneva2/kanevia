///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Tools/json11/json11.hpp"
#include <vector>
#include <map>
#include <string>

struct lua_State;

namespace KEP {

// Tags used in global.json (see global.json.example)
#define GS_TAG_MENUS "menus"
#define GS_TAG_SCRIPTS "scripts"
#define GS_TAG_SETTINGS "settings"
#define GS_TAG_SERVERCONFIGS "ServerConfigs"
#define GS_TAG_CLIENTCONFIGS "ClientConfigs"

// Prohibit use of some characters in settings path
#define GS_VALID_KEY_PATH_CHAR "abcdefghijklmnopqrstuvwxyz0123456789_"

// Wrapper class for json config data
class JsonConfig {
public:
	// Abstract accessor
	class Accessor {
	public:
		Accessor(const json11::Json& json) :
				m_json(json) {}

	protected:
		const json11::Json& m_json;
	};

	// Helper class that maps script paths based on json configuration
	class FileNameMapper : public Accessor {
	public:
		FileNameMapper(const json11::Json& json, const std::vector<std::string>& configGroups, const char* rootNodeName);
		std::string operator()(const std::string& path);

	private:
		std::map<std::string, std::string> m_aliasMap;
	};

	// Helper class that configures Lua vm with global values based on json configuration
	class LuaScriptConfigurator : public Accessor {
	public:
		LuaScriptConfigurator(const json11::Json& json, const std::vector<std::string>& configGroups);
		void operator()(lua_State* L);

	private:
		void luaPushJsonNodeValue(lua_State* L, const json11::Json& node);
		std::vector<std::string> m_configGroups;
	};

	// Helper class that provides configuration value by path (for C++ code)
	class ConfigProvider : public Accessor {
	public:
		ConfigProvider(const json11::Json& json, const std::vector<std::string>& configGroups, const char* rootNodeName);
		std::string operator()(const std::string& keyPath, const std::string* defaultValue);

	private:
		void scanNode(const json11::Json& node, const std::string& keyPath);
		std::map<std::string, std::string> m_configMap;
	};

public:
	JsonConfig(const std::string& jsonString) {
		m_json = json11::Json::parse(jsonString, m_jsonErr);
	}

	bool hasError() const {
		return !m_jsonErr.empty();
	}
	std::string getError() const {
		return m_jsonErr;
	}

	FileNameMapper getScriptNameMapper(const std::vector<std::string>& configGroups) {
		return FileNameMapper(m_json, configGroups, GS_TAG_SCRIPTS);
	}
	FileNameMapper getMenuNameMapper(const std::vector<std::string>& configGroups) {
		return FileNameMapper(m_json, configGroups, GS_TAG_MENUS);
	}
	LuaScriptConfigurator getLuaScriptConfigurator(const std::vector<std::string>& configGroups) {
		return LuaScriptConfigurator(m_json, configGroups);
	}
	ConfigProvider getServerConfigProvider(const std::vector<std::string>& configGroups) {
		return ConfigProvider(m_json, configGroups, GS_TAG_SERVERCONFIGS);
	}
	ConfigProvider getClientConfigProvider(const std::vector<std::string>& configGroups) {
		return ConfigProvider(m_json, configGroups, GS_TAG_CLIENTCONFIGS);
	}

#ifdef PLATFORM_TEST
	// For Testing only
	json11::Json getJsonRoot() const {
		return m_json;
	}
#endif

private:
	json11::Json m_json;
	std::string m_jsonErr;
};

} // namespace KEP
