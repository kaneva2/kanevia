///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptGlueCommon.h"
#include "ScriptEventArgs.h"
#include "Event/PlayListMgr/PLMMediaData.h"

template <typename T, T v>
struct ScriptValueHandler<std::integral_constant<T, v>> {
	static void Push(IScriptArgManager* pScriptArgManager, const std::integral_constant<T, v>& value) {
		PushScriptValue(pScriptArgManager, value.value);
	}
};

template <typename T>
struct ScriptValueHandler<KEP::ScriptPos<T>> {
	static bool Get(IScriptArgManager* pScriptArgManager, KEP::ScriptPos<T>* pValue) {
		return GetScriptTableValues(pScriptArgManager,
			"x", &(pValue->x), "y", &(pValue->y), "z", &(pValue->z));
	}
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::ScriptPos<T>& value) {
		pScriptArgManager->PushNewTable(6);
		AddScriptTableElems(pScriptArgManager,
			"x", value.x, "y", value.y, "z", value.z);
	}
};

template <typename T>
struct ScriptValueHandler<KEP::ScriptPosRot<T>> {
	static bool Get(IScriptArgManager* pScriptArgManager, KEP::ScriptPosRot<T>* pValue) {
		return GetScriptTableValues(pScriptArgManager,
			"x", &(pValue->x), "y", &(pValue->y), "z", &(pValue->z) "rx", &(pValue->rx), "ry", &(pValue->ry), "rz", &(pValue->rz));
	}
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::ScriptPosRot<T>& value) {
		pScriptArgManager->PushNewTable(6);
		AddScriptTableElems(pScriptArgManager,
			"x", value.x, "y", value.y, "z", value.z,
			"rx", value.rx, "ry", value.ry, "rz", value.rz);
	}
};

template <>
struct ScriptValueHandler<KEP::KeyModifierTable> {
	static bool Get(IScriptArgManager* pScriptArgManager, KEP::KeyModifierTable* pValue) {
		return GetScriptTableValues(pScriptArgManager,
			"shift", &(pValue->shift), "ctrl", &(pValue->ctrl), "alt", &(pValue->alt));
	}
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::KeyModifierTable& value) {
		pScriptArgManager->PushNewTable(3);
		AddScriptTableElems(pScriptArgManager,
			"shift", value.shift, "ctrl", value.ctrl, "alt", value.alt);
	}
};

template <typename T>
inline bool GetAndPopScriptValue(IScriptArgManager* pScriptArgManager, T&& pValue) {
	bool res = GetScriptValue(pScriptArgManager, pValue);
	pScriptArgManager->Pop();
	return res;
}

template <>
struct ScriptValueHandler<KEP::ScriptZoneInfo> {
	static bool Get(IScriptArgManager* pScriptArgManager, KEP::ScriptZoneInfo* pValue) {
		return pScriptArgManager->PushArrayElem(1) &&
			   GetAndPopScriptValue(pScriptArgManager, &pValue->zoneId) &&
			   pScriptArgManager->PushArrayElem(2) &&
			   GetAndPopScriptValue(pScriptArgManager, &pValue->appName) &&
			   pScriptArgManager->PushArrayElem(3) &&
			   GetAndPopScriptValue(pScriptArgManager, &pValue->instanceIdArray);
	}
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::ScriptZoneInfo& value) {
		pScriptArgManager->PushNewArray(3);
		PushScriptValue(pScriptArgManager, value.zoneId);
		pScriptArgManager->SetArrayElem(1);
		PushScriptValue(pScriptArgManager, value.appName);
		pScriptArgManager->SetArrayElem(2);
		PushScriptValue(pScriptArgManager, value.instanceIdArray);
		pScriptArgManager->SetArrayElem(3);
	}
};

template <>
struct ScriptValueHandler<KEP::PlaylistAsset*> {
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::PlaylistAsset* value) {
		if (value == nullptr) {
			PushScriptValue(pScriptArgManager, ScriptValue()); // Push nil
			return;
		}

		pScriptArgManager->PushNewTable(8);
		AddScriptTableElems(pScriptArgManager,
			"Name", value->GetAssetName(),
			"AssetID", value->GetAssetID(), // was string
			"AssetTypeID", static_cast<int>(value->GetAssetTypeID()), // was string
			"AssetSubTypeID", static_cast<int>(value->GetAssetSubTypeID()), // was string
			"OffsiteAssetID", value->GetOffsiteAssetID(),
			"RunTimeSeconds", value->GetRuntimeSeconds(), // was string
			"ThumbNailMediumPath", value->GetThumbNailMediumPath(),
			"MediaPath", value->GetMediaPath()
			//"PlayListID" no longer available
		);
	}
};

template <>
struct ScriptValueHandler<KEP::ScriptZoneInstanceInfo> {
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::ScriptZoneInstanceInfo& value) {
		if (value.instanceId != -1) {
			PushScriptValue(pScriptArgManager, value.instanceId);
			PushScriptValue(pScriptArgManager, value.locked);
			PushScriptValue(pScriptArgManager, value.players);
		} else {
			PushScriptValue(pScriptArgManager, ScriptValue()); // Push nil
			PushScriptValue(pScriptArgManager, ScriptValue()); // Push nil
			PushScriptValue(pScriptArgManager, ScriptValue()); // Push nil
		}
	}
};

template <>
struct ScriptValueHandler<KEP::ScriptItemInfo> {
	static void Push(IScriptArgManager* pScriptArgManager, const KEP::ScriptItemInfo& value) {
		AddScriptTableElems(pScriptArgManager,
			"globalId", value.globalId,
			"useType", value.useType,
			"actorGroup", value.actorGroup);
	}
};
