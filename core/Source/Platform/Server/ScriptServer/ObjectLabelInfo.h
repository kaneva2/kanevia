///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

struct ObjectLabelInfo {
	std::string m_label;
	float m_size;
	float m_red;
	float m_green;
	float m_blue;
};

} // namespace KEP
