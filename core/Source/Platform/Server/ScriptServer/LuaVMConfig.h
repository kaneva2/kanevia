///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

// Lua VM configuration options
// Isolated from ScriptServerEngine class
// YC 04/2015

#pragma once

#include <map>
#include <string>

#define DEFAULT_LUA_GC_PAUSE 200 // 200 is the Lua default
#define DEFAULT_LUA_GC_STEP 200 // 200 is the Lua default
#define DEFAULT_SCRIPT_MEMORY_LIMIT 1048576 // in bytes, before allocator fails
#define DEFAULT_YIELD_AFTER 50 // number of Lua instructions before yielding
#define DEFAULT_USE_CONSUMER_LOOP true
#define DEFAULT_USE_COCO true
#define DEFAULT_BLOCKED_SCRIPT_TIMEOUT 10000 // ms a script can be blocked before timing out
#define DEFAULT_MIN_DELAY_MILLIS 200
#define DEFAULT_TIMER_PERIOD 1000 // kgp_timer interval in milliseconds
#define DEFAULT_KGP_START_WAIT_TIMEOUT 5000
#define DEFAULT_USER_SCRIPT_SHUTDOWN_TIMEOUT 500
#define DEFAULT_SYSTEM_SCRIPT_SHUTDOWN_TIMEOUT 1000

namespace KEP {

class LuaVMConfig {
public:
	LuaVMConfig() :
			m_luaGCPause(DEFAULT_LUA_GC_PAUSE), m_luaGCStep(DEFAULT_LUA_GC_STEP), m_luaIsProtected(true), m_storedProcedureAccessEnabled(false), m_yieldAfter(DEFAULT_YIELD_AFTER), m_scriptMemoryLimit(DEFAULT_SCRIPT_MEMORY_LIMIT), m_blockedScriptTimeout(DEFAULT_BLOCKED_SCRIPT_TIMEOUT), m_APITimeoutIsFatal(false), m_restAPIDataTableName("__apireg"), m_useConsumerLoop(DEFAULT_USE_CONSUMER_LOOP), m_useCocoThread(DEFAULT_USE_COCO), m_minDelayMillis(DEFAULT_MIN_DELAY_MILLIS), m_timerPeriod(DEFAULT_TIMER_PERIOD), m_loadSystemScripts(false), m_loadSystemScriptsSynch(true), m_kgpStartWaitTimeout(DEFAULT_KGP_START_WAIT_TIMEOUT), m_userScriptShutdownTimeout(DEFAULT_USER_SCRIPT_SHUTDOWN_TIMEOUT), m_systemScriptShutdownTimeout(DEFAULT_SYSTEM_SCRIPT_SHUTDOWN_TIMEOUT) {
	}

	int getLuaGCPause() const {
		return m_luaGCPause;
	}
	void setLuaGCPause(int newVal) {
		m_luaGCPause = newVal;
	}
	int getLuaGCStep() const {
		return m_luaGCStep;
	}
	void setLuaGCStep(int newVal) {
		m_luaGCStep = newVal;
	}
	bool isLuaProtected() const {
		return m_luaIsProtected;
	}
	void setLuaProtected(bool newVal) {
		m_luaIsProtected = newVal;
	}
	bool isStoreProcAccessEnabled() const {
		return m_storedProcedureAccessEnabled;
	}
	void setStoreProcAccessEnabled(bool b) {
		m_storedProcedureAccessEnabled = b;
	}
	int getYieldAfter() const {
		return m_yieldAfter;
	}
	void setYieldAfter(int newVal) {
		m_yieldAfter = newVal;
	}
	bool useConsumerLoop() const {
		return m_useConsumerLoop;
	}
	void setUseConsumerLoop(bool newVal) {
		m_useConsumerLoop = newVal;
	}
	bool useCocoThread() const {
		return m_useCocoThread;
	}
	void setUseCocoThread(bool newVal) {
		m_useCocoThread = newVal;
	}
	unsigned getScriptMemoryLimit() const {
		return m_scriptMemoryLimit;
	}
	void setScriptMemoryLimit(unsigned newVal) {
		m_scriptMemoryLimit = newVal;
	}
	unsigned getBlockedScriptTimeout() const {
		return m_blockedScriptTimeout;
	}
	void setBlockedScriptTimeout(unsigned newVal) {
		m_blockedScriptTimeout = newVal;
	}
	bool getAPITimeoutIsFatal() const {
		return m_APITimeoutIsFatal;
	}
	void setAPITimeoutIsFatal(bool newVal) {
		m_APITimeoutIsFatal = newVal;
	}
	std::string getLuaCommonLibrary() const {
		return m_luaCommonLibrary;
	}
	void setLuaCommonLibrary(const std::string& libName) {
		m_luaCommonLibrary = libName;
	}
	std::string getScriptRootDir() const {
		return m_scriptRootDir;
	}
	void setScriptRootDir(const std::string& dir) {
		m_scriptRootDir = dir;
	}
	unsigned getMinDelayMillis() const {
		return m_minDelayMillis;
	}
	void setMinDelayMillis(unsigned newVal) {
		m_minDelayMillis = newVal;
	}
	unsigned getTimerPeriod() const {
		return m_timerPeriod;
	}
	void setTimerPeriod(unsigned newVal) {
		m_timerPeriod = newVal;
	}
	bool getLoadSystemScripts() const {
		return m_loadSystemScripts;
	}
	void setLoadSystemScripts(bool b) {
		m_loadSystemScripts = b;
	}
	bool getLoadSystemScriptsSynch() const {
		return m_loadSystemScriptsSynch;
	}
	void setLoadSystemScriptsSynch(bool b) {
		m_loadSystemScriptsSynch = b;
	}
	unsigned getKgpStartWaitTimeout() const {
		return m_kgpStartWaitTimeout;
	}
	void setKgpStartWaitTimeout(unsigned newVal) {
		m_kgpStartWaitTimeout = newVal;
	}
	unsigned getUserScriptShutdownTimeout() const {
		return m_userScriptShutdownTimeout;
	}
	void setUserScriptShutdownTimeout(unsigned newVal) {
		m_userScriptShutdownTimeout = newVal;
	}
	unsigned getSystemScriptShutdownTimeout() const {
		return m_systemScriptShutdownTimeout;
	}
	void setSystemScriptShutdownTimeout(unsigned newVal) {
		m_systemScriptShutdownTimeout = newVal;
	}

	typedef std::map<std::string, std::map<std::string, std::string>> RestAPIDataMap;

	RestAPIDataMap& getRestAPIDataMapRef() {
		return m_restAPIDataMap;
	}
	const RestAPIDataMap& getRestAPIDataMapRef() const {
		return m_restAPIDataMap;
	}
	std::string getRestAPIDataTableName() const {
		return m_restAPIDataTableName;
	}

private:
	// VM setup
	int m_luaGCPause; // Use of GC means that the required memory will be greater than the memory in use, but how
	int m_luaGCStep; // much depends on the values of Pause and Step below, which are arguments to lua_gc().
	bool m_luaIsProtected; // Selected Lua libraries will be disabled for protected Lua environment
	bool m_storedProcedureAccessEnabled; // Whether or not to enable store-procedure related kgp APIs
	int m_yieldAfter; // Number of Lua instructions before yielding
	bool m_useConsumerLoop; // If true, engine will run a consumer loop inside Lua worker thread to keep the worker thread alive forever
	bool m_useCocoThread; // If true, engine will call lua_newcthread to create a lua COCO thread with full yielding support
	unsigned m_scriptMemoryLimit; // Subsequent memory allocation will fail once per-VM memory limit is hit
	unsigned m_blockedScriptTimeout; // Maximum time (in milliseconds) a script is allowed to stay at blocked state
	bool m_APITimeoutIsFatal; // If false, timed-out API call will gracefully return nil.

	std::string m_luaCommonLibrary; // Common Lua library loaded for all script VMs (e.g. Lib_Common.lua)
	std::string m_scriptRootDir; // Root script folder path (ScriptServerScripts)

	// kgp API options
	unsigned m_minDelayMillis; // For APIs that support delays (queued for future execution), this is the minimum delay in milliseconds to prevent a script from ticking too fast.
	unsigned m_timerPeriod;

	// kgp event handler options
	bool m_loadSystemScripts; // If true, load system scripts from kgp_common.preload_scripts table
	bool m_loadSystemScriptsSynch; // If true, load system scripts synchronously (blocked loading and kgp_server calls)
	unsigned m_kgpStartWaitTimeout; // Wait timeout for synchronous kgp_start function call
	unsigned m_userScriptShutdownTimeout; // Script shutdown timeout period for smart objects and other user scripts
	unsigned m_systemScriptShutdownTimeout; // Script shutdown timeout period for system (controller) scripts

	// Global data for REST API functions
	RestAPIDataMap m_restAPIDataMap; // Per-game REST API data (for dev worlds only)
	std::string m_restAPIDataTableName;
};

}; // namespace KEP
