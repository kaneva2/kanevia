///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <fstream>
#include <string>
#include "DetachScriptTask.h"
#include "MySqlUtil.h"
#include "ScriptServerEngine.h"
#include "param_ids.h"
#include "KEPFileNames.h"
#include "LogHelper.h"

using namespace mysqlpp;

namespace KEP {

static LogInstance("Instance");
static Logger alert_logger(Logger::getInstance(L"Alert"));

DetachScriptTask::DetachScriptTask(
	//	log4cplus::Logger& logger,
	KGP_Connection* conn,
	fast_recursive_mutex& dbSync,
	ULONG objPlacementId) :
		ScriptServerTaskObject("DetachScriptTask"),
		//	m_logger(logger),
		m_conn(conn),
		m_dbSync(dbSync),
		m_objPlacementId(objPlacementId) {}

/*!
* \brief
* Write brief comment for ExecuteTask here.
*
* \throws <exception class>
* Description of criteria for throwing this exception.
*
* Write detailed description for ExecuteTask here.
*
* \remarks
* Write remarks for ExecuteTask here.
*
* \see
* Separate items with the '|' character.
*/
void DetachScriptTask::ExecuteTask() {
	//	_LogTrace("DetachScriptTask: " << GetName() << " has id of " << GetID());

	//
	// Get the file name from the database and delete it (the file) and
	// then delete the record itself.
	//
	{
		std::lock_guard<fast_recursive_mutex> lock(m_dbSync);
		BEGIN_TRY_SQL_FOR("Delete Server Script");

		// Detach the script from object
		Query query = m_conn->query();
		query << "DELETE FROM dynamic_object_parameters WHERE obj_placement_id = %0 AND param_type_id IN (%1,%2)";
		query.parse();

		//		_LogTrace("DetachScriptTask: " << query.str((unsigned int) m_objPlacementId, PARAM_SCRIPT_FILE_PATH, PARAM_SCRIPT_CREATED_DATE));
		SimpleResult res = query.execute((unsigned int)m_objPlacementId, PARAM_SCRIPT_FILE_PATH, PARAM_SCRIPT_CREATED_DATE);

		END_TRY_SQL_LOG_ONLY(m_conn);
	}

	// Delete action instance files if found
	std::string instanceScriptName = m_server->GetActionInstanceScriptName(m_objPlacementId);
	m_server->DeleteScript(ScriptServerEvent::ActionInstance, instanceScriptName);
	m_server->DeleteScript(ScriptServerEvent::ActionInstanceParams, instanceScriptName);
}

} // namespace KEP
