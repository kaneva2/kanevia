///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <map>
#include <memory>

enum class EffectCode;

namespace KEP {

class ScriptServerEngine;
class ServerScript;
class ScriptZone;
typedef std::shared_ptr<ServerScript> ServerScriptPtr;
typedef std::shared_ptr<ScriptZone> ScriptZonePtr;
typedef unsigned long ScriptPID; // Defined as unsigned long for now. We will eventually consolidate all types used for PID
class Scheduler;

class ScriptZone {
private:
	// Non-copyable
	ScriptZone(const ScriptZone&) = delete;
	void operator=(const ScriptZone&) = delete;

public:
	ScriptZone(ZoneIndex zoneIndex, unsigned parentZoneInstanceId) :
			m_zoneIndex(zoneIndex),
			m_zoneInstanceId(static_cast<unsigned>(m_zoneIndex.GetInstanceId())),
			m_parentZoneInstanceId(parentZoneInstanceId),
			m_stopping(false) {
	}

	// Movable
	ScriptZone(ScriptZone&& rhs) :
			m_zoneIndex(rhs.m_zoneIndex),
			m_zoneInstanceId(rhs.m_zoneInstanceId),
			m_parentZoneInstanceId(rhs.m_parentZoneInstanceId),
			m_scripts(std::move(rhs.m_scripts)),
			m_vms(std::move(rhs.m_vms)),
			m_pidMap(std::move(rhs.m_pidMap)) {}

	ZoneIndex getZoneIndex() const { return m_zoneIndex; }

	// Query for linked worlds
	unsigned getMasterZoneInstanceId() const { return m_parentZoneInstanceId == 0 ? m_zoneInstanceId : m_parentZoneInstanceId; } // Return parent if child zone. Otherwise return self.
	bool isChildZone() const { return m_parentZoneInstanceId != 0; }

	size_t getNumScripts() const {
		std::lock_guard<std::mutex> lock(m_mutex);
		return m_scripts.size();
	}

	ServerScriptPtr getScript(ScriptPID scriptId, bool useMap) {
		std::lock_guard<std::mutex> lock(m_mutex);

		auto itr = m_scripts.find(scriptId);
		if (itr != m_scripts.end()) {
			return itr->second;
		}

		// Search event map (m_ids) if no matching script in m_scripts
		if (useMap) {
			auto itrMap = m_pidMap.find(scriptId);
			if (itrMap != m_pidMap.end()) {
				// we found a mapping
				itr = m_scripts.find(itrMap->second);
				if (itr != m_scripts.end()) {
					return itr->second;
				}
			}
		}

		return ServerScriptPtr();
	}

	ServerScriptPtr getScriptByVM(void* vm) {
		std::lock_guard<std::mutex> lock(m_mutex);

		auto itr = m_vms.find(vm);
		if (itr != m_vms.end())
			return itr->second;
		return ServerScriptPtr();
	}

	template <unsigned long handlerMask>
	__forceinline static bool ScriptFilter(const ServerScriptPtr& pScript) {
		bool accept = !pScript->IsStopping() && (handlerMask == 0 || pScript->HasHandlerFor(handlerMask));
		return !accept; // return true if reject
	}

	template <unsigned long handlerMask>
	__forceinline static bool IdleScriptFilter(const ServerScriptPtr& pScript) {
		// Both IDLE and READY states are now considered idle state. SCHEDULED, RUNNING, BLOCKED and YIELDED are busy states.
		bool accept = !pScript->IsStopping() && (handlerMask == 0 || pScript->HasHandlerFor(handlerMask)) && (pScript->GetState() == ScriptState::IDLE || pScript->GetState() == ScriptState::READY);
		return !accept;
	}

	std::vector<ServerScriptPtr> getScriptsByFilter(const std::function<bool(const ServerScriptPtr&)>& pred) {
		// Copy scripts list first
		std::vector<ServerScriptPtr> outScripts = getAllScripts();

		// Filter over the copied list to prevent possible dead lock caused by the filter.
		auto endItr = std::remove_if(outScripts.begin(), outScripts.end(), pred);
		if (endItr != outScripts.end()) {
			outScripts.resize(endItr - outScripts.begin());
		}

		return outScripts;
	}

	std::vector<ServerScriptPtr> getAllScripts() {
		std::vector<ServerScriptPtr> outScripts;
		std::lock_guard<std::mutex> lock(m_mutex);

		outScripts.reserve(m_scripts.size());
		for (const auto& pr : m_scripts) {
			outScripts.push_back(pr.second);
		}
		return outScripts;
	}

	std::vector<ScriptPID> getAllScriptIDs() const;
	std::vector<ScriptPID> getScriptIDsAcceptingBroadcast(bool interZone) const;

	void mapPID(ScriptPID srcId, ScriptPID dstId) {
		std::lock_guard<std::mutex> lock(m_mutex);
		m_pidMap[srcId] = dstId;
	}

	void addScript(ScriptPID scriptId, const ServerScriptPtr& script);
	ServerScriptPtr removeScript(ScriptPID scriptId);

	void setZoneStopping() { m_stopping = true; }
	bool isZoneStopping() const { return m_stopping; }

	// Set pending effect completion event - return previous pending event if any
	ScriptServerEvent* setEffectCompletionEvent(int objectPID, EffectCode type, ScriptServerEvent* pEvent);

	// Clear pending effect completion event if matches and return true (caller should release the reference). Otherwise return false
	bool clearEffectCompletionEvent(int objectPID, EffectCode type, ScriptServerEvent* pEvent);

private:
	typedef std::map<ScriptPID, ServerScriptPtr> TScriptMap; // placement Id to TZoneScriptMap
	typedef std::map<void*, ServerScriptPtr> TLUAMap; // lua VM ptr to ServerScript
	typedef std::map<ScriptPID, ScriptPID> TPidMap; // placement Id to placement Id (for scripts getting touch events from other dynamic objects)
	typedef std::map<std::pair<int, EffectCode>, ScriptServerEvent*> EffectCompletionEventMap;

private:
	const ZoneIndex m_zoneIndex; // Zone index (read-only)
	const unsigned m_zoneInstanceId; //Zone instance ID for fast lookup (== m_zoneIndex.getInstanceId())
	const unsigned m_parentZoneInstanceId; //For linked worlds, 0 for parent or orphans
	std::atomic<bool> m_stopping;

	mutable std::mutex m_mutex; // protects following 3 maps
	TScriptMap m_scripts; // PID -> ServerScriptPtr
	TLUAMap m_vms; // vm -> ServerScriptPtr
	TPidMap m_pidMap; // PID -> PID (for objectMapEvent)

	mutable std::mutex m_effectCompletionEventsMutex; // protects following map
	EffectCompletionEventMap m_effectCompletionEventMap; // {PID, EffectCode} -> ScriptServerEvent*
};

class ScriptMap {
private:
	// Non-copyable
	ScriptMap(const ScriptMap&) = delete;
	void operator=(const ScriptMap&) = delete;

public:
	ScriptMap() {}

	/*!
	 * \brief
	 * Returns the server script corresponding to the given placement id.
	 *
	 * \param script Id
	 * The placement id of the object that you want to find the script for.
	 *
	 * \param useMap
	 * Allow Get to look for a mapped Id, if the given placement Id doesn't have a script already.
	 *
	 * \returns
	 * The server script for the placement object.
	 *
	 * If the placement object has a script, that's what get returned.  If there
	 * is not script and useMap = true, then Get() will search the id map to
	 * find a mapped server script and return that, if one exists.  In other words,
	 * script mapping only redirects if the target doesn't already have a script
	 * associated with it.
	 *
	 * \see
	 * ServerScript for the function mask.
	 */
	ServerScriptPtr getScriptByID(ScriptPID scriptId, ZoneIndex zoneIndex, bool useMap);
	ServerScriptPtr getScriptByVM(void* vm);

	// Create a new script zone (return false if already exists and not empty). Parent zone instance ID is for child zones (linked worlds), 0 for parent or orphans
	bool ScriptMap::createZone(ZoneIndex zoneIndex, unsigned parentZoneInstanceId) {
		std::lock_guard<fast_recursive_mutex> lock(m_zonesMutex);
		auto itr = m_zones.find(zoneIndex);
		if (itr != m_zones.end()) {
			// Already exists
			assert(itr->second->getNumScripts() == 0);
			return itr->second->getNumScripts() == 0;
		} else {
			m_zones.insert(std::make_pair(zoneIndex, std::make_shared<ScriptZone>(zoneIndex, parentZoneInstanceId)));
			return true;
		}
	}

	// Get a script zone by zone index (copy shared_ptr before return to avoid race condition, don't return const shared_ptr&)
	ScriptZonePtr getZone(ZoneIndex zoneIndex) const {
		std::lock_guard<fast_recursive_mutex> lock(m_zonesMutex);
		auto itr = m_zones.find(zoneIndex);
		if (itr != m_zones.end()) {
			return itr->second;
		}
		return ScriptZonePtr();
	}

	// Get a list of zones currently hosted (the returned list could run out of sync if container is modified afterwards - use only if sequential consistency is not important)
	std::vector<ScriptZonePtr> getAllZones() const {
		std::vector<ScriptZonePtr> allZones;
		std::lock_guard<fast_recursive_mutex> lock(m_zonesMutex);
		allZones.reserve(m_zones.size());
		for (const auto& prZones : m_zones) {
			allZones.push_back(prZones.second);
		}
		return allZones;
	}

private:
	mutable fast_recursive_mutex m_zonesMutex; // protects m_zones
	std::map<ZoneIndex, ScriptZonePtr> m_zones; //ZoneIndex to ScriptZone
};

}; //namespace KEP
