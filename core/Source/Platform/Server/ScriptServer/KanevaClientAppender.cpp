///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "KanevaClientAppender.h"
#include "ScriptServerEngine.h"

using namespace log4cplus;
using namespace KEP;

KanevaClientAppender::KanevaClientAppender(NETID client, const helpers::Properties& props) :
		Appender(props), m_client(client) {
}

KanevaClientAppender::~KanevaClientAppender() {
}

void KanevaClientAppender::append(const log4cplus::spi::InternalLoggingEvent& e) {
	std::wstringstream strm;
	this->layout->formatAndAppend(strm, e);
	ScriptServerEngine* engine = ScriptServerEngine::Instance();
	if (engine) {
		engine->SendPlayerTellToClient(m_client, Utf16ToUtf8(strm.str()).c_str(), "Log", true, 0);
	}
}

void KanevaClientAppender::close() {
}