///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

// YC 03/2015
// Refactor original ScriptServerEngine Dictionary class with abstraction

#pragma once

#include <vector>
#include <set>
#include <map>
#include <unordered_map>
#include <string>
#include <memory>
#include "js.h"
#include "KEPConstants.h"
#include "Core/Util/fast_mutex.h"

typedef struct lua_State lua_State;

namespace KEP {

class fast_recursive_mutex;
class GameStateManager;

class ILuaSharedDataProvider {
public:
	struct Key { // Wrapper struct to define Key as either
		typedef unsigned char IndexType;
		enum { NIL,
			ID,
			PID, // Basics
			POS,
			ORI,
			DEST,
			SPEED,
			TIME2DEST, // Movement
			NUM_WELLKNOWN_KEYS
		};

		union {
			const char* str; // a NULL-terminated string, or
			IndexType idx; // a numeric index (1-255, optimizations for reserved keys)
		};

		// Implicit conversion
		Key() :
				str(NULL) {}
		Key(const char* s) :
				str(s) {}
		Key(IndexType i) :
				str(NULL) {
			idx = i;
		}

		bool isNull() const {
			return str == NULL;
		}

		bool isIndex() const {
			return ((unsigned)str) <= MAX_INDEX;
		}

		Key toIndex() const {
			if (isIndex())
				return idx;
			auto itr = ms_propertyNameMap.find(str);
			return itr != ms_propertyNameMap.end() ? itr->second : Key::NIL;
		}

		static const IndexType MAX_INDEX = (1 << (sizeof(IndexType) * 8)) - 1;
		static unsigned ms_propertyCount;
		static std::vector<std::string> ms_propertyNames;
		static std::unordered_map<std::string, Key::IndexType> ms_propertyNameMap;

		static unsigned staticInit();
	};

	struct ChildRef { // Wrapper struct to distinguish child value from a string value
		const char* name;
		ChildRef(const char* n) :
				name(n) {}
	};

	virtual ~ILuaSharedDataProvider() {}
	virtual int get(Key key, void* buffer, size_t bufSize) = 0; // Get a value by key
	virtual void set(Key key, double val, std::string& childRemoved) = 0; // Store a numeric value by key
	virtual void set(Key key, const char* val, std::string& childRemoved) = 0; // Store a string value by key
	virtual void set(Key key, bool val, std::string& childRemoved) = 0; // Store a boolean value by key
	virtual void set(Key key, ChildRef val, std::string& childRemoved) = 0; // Store a child container by key
	virtual void set(Key key, lua_State* L, int index, std::string& childRemoved) = 0; // Store a lua stack value by key
	virtual void remove(Key key, std::string& childRemoved) = 0; // Remove key from the container
	virtual void clear(std::vector<std::string>& childrenToDeref) = 0; // Remove all keys from the container
	virtual void keys(std::set<std::string>& allKeys) = 0; // Get all keys
	virtual int type(Key key) = 0; // Return value type if key exists, other wise LUA_TNIL

	virtual void addRef() = 0; // Increment container reference
	virtual bool release(std::vector<std::string>& childrenToDeref) = 0; // Decrement container reference, return true if reference <= 0
	virtual int refs() = 0; // Get reference count

	virtual fast_recursive_mutex& sync() = 0; // Get synchronization object
};

// Basic ref count and sync implementation
class LuaSharedDataProvider : public ILuaSharedDataProvider {
public:
	LuaSharedDataProvider() :
			m_numRef(1) {}

	// addRef / release is always protected by global lock
	void addRef() {
		m_numRef++;
	}
	bool release(std::vector<std::string>& /*childrenToDeref*/) {
		return --m_numRef <= 0;
	}
	int refs() {
		return m_numRef;
	}

	fast_recursive_mutex& sync() {
		return m_sync;
	}

private:
	// Following two members are protected by global lock
	int m_numRef; // reference count: LUA userdata references, plus owning dictionary references if child
	fast_recursive_mutex m_sync;

	// Other data members should be protected by local lock (m_sync)
};

// Chaining two orphan (with no other parent references) containers
class LuaSharedDataChain : public LuaSharedDataProvider {
public:
	LuaSharedDataChain(ILuaSharedDataProvider* pri, ILuaSharedDataProvider* sec) :
			m_pri(pri), m_sec(sec) {} // pri and sec must be two orphan containers

	~LuaSharedDataChain() {
		if (m_pri) {
			delete m_pri;
			m_pri = NULL;
		}
		if (m_sec) {
			delete m_sec;
			m_sec = NULL;
		}
	}

	int get(Key key, void* buffer, size_t bufSize);
	void set(Key key, double val, std::string& childRemoved);
	void set(Key key, const char* val, std::string& childRemoved);
	void set(Key key, bool val, std::string& childRemoved);
	void set(Key key, ChildRef val, std::string& childRemoved);
	void set(Key key, lua_State* L, int index, std::string& childRemoved);
	void remove(Key key, std::string& childRemoved);
	int type(Key key);

	// Clear both containers
	void clear(std::vector<std::string>& childrenToDeref) {
		if (m_pri)
			m_pri->clear(childrenToDeref);
		if (m_sec)
			m_sec->clear(childrenToDeref);
	}

	// Return keys from both containers
	void keys(std::set<std::string>& allKeys) {
		if (m_pri)
			m_pri->keys(allKeys);
		if (m_sec)
			m_sec->keys(allKeys);
	}

	// Deref self, if reference count hits zero, also deref both children
	bool release(std::vector<std::string>& childrenToDeref) {
		if (LuaSharedDataProvider::release(childrenToDeref)) {
			if (m_pri)
				m_pri->release(childrenToDeref);
			if (m_sec)
				m_sec->release(childrenToDeref);
			return true;
		}
		return false;
	}

private:
	ILuaSharedDataProvider* m_pri;
	ILuaSharedDataProvider* m_sec;
};

// Basic key-value container for cross-VM shared Lua data (string keys only at this moment)
class LuaSharedDictionary : public LuaSharedDataProvider {
public:
	int get(Key key, void* buffer, size_t bufSize);
	void set(Key key, double val, std::string& childRemoved);
	void set(Key key, const char* val, std::string& childRemoved);
	void set(Key key, bool val, std::string& childRemoved);
	void set(Key key, ChildRef val, std::string& childRemoved);
	void set(Key key, lua_State* L, int index, std::string& childRemoved);
	void remove(Key key, std::string& childRemoved);
	void clear(std::vector<std::string>& childrenToDeref);
	void keys(std::set<std::string>& allKeys);
	int type(Key key);

	// addRef / release is always protected by global lock
	bool release(std::vector<std::string>& childrenToDeref);

private:
	void removeKey(Key key, std::string& childRemoved);

	// Following containers are protected by dictionary lock (m_sync)
	std::map<std::string, double> m_numericValues;
	std::map<std::string, std::string> m_stringValues;
	std::map<std::string, bool> m_boolValues;
	std::map<std::string, std::string> m_dictValues;
};

// Virtual container for game state access
class LuaGameEntity : public LuaSharedDataProvider {
public:
	LuaGameEntity(int entityType, ULONG entityId) :
			m_entityType(entityType), m_entityId(entityId) {}

	int get(Key key, void* buffer, size_t bufSize) {
		return getProperty(key, buffer, bufSize);
	}
	void set(Key key, double val, std::string& childRemoved) {
		key; // Read-only
		val;
		childRemoved;
	}
	void set(Key key, const char* val, std::string& childRemoved) {
		key; // Read-only
		val;
		childRemoved;
	}
	void set(Key key, bool val, std::string& childRemoved) {
		key; // Read-only
		val;
		childRemoved;
	}
	void set(Key key, ChildRef val, std::string& childRemoved) {
		key; // Read-only
		val;
		childRemoved;
	}
	void set(Key key, lua_State* L, int index, std::string& childRemoved) {
		key; // Read-only
		L;
		index;
		childRemoved;
	}
	void remove(Key key, std::string& childRemoved) {
		key; // List maintained by the engine - no action
		childRemoved;
	}
	void clear(std::vector<std::string>& childrenToDeref) {
		childrenToDeref; // List maintained by the engine - no action
	}
	void keys(std::set<std::string>& allKeys) {
		allProperties(allKeys);
	}
	int type(Key key) {
		return getProperty(key, NULL, 0);
	}

protected:
	virtual int getProperty(Key key, void* buffer, size_t bufSize);
	virtual void allProperties(std::set<std::string>& allKeys);

	int m_entityType;
	ULONG m_entityId;
};

// Virtual container for dynamic object properties
class LuaGameObject : public LuaGameEntity {
public:
	LuaGameObject(GameStateManager& gsm, ULONG entityId) :
			LuaGameEntity(static_cast<int>(ObjectType::DYNAMIC), entityId), m_gsm(gsm) {}

protected:
	virtual int getProperty(Key key, void* buffer, size_t bufSize);
	virtual void allProperties(std::set<std::string>& allKeys);

	GameStateManager& m_gsm;
};

// Virtual container for player properties
class LuaGamePlayer : public LuaGameEntity {
public:
	LuaGamePlayer(ULONG entityId) :
			LuaGameEntity(static_cast<int>(ObjectType::MOVEMENT), entityId) {}

protected:
	virtual int getProperty(Key key, void* buffer, size_t bufSize);
	virtual void allProperties(std::set<std::string>& allKeys);
};

} // namespace KEP
