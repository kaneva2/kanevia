///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ServerScript.h"
#include "Event/Events/ScriptServerEvent.h"
#include "Event/Events/ScriptClientEvent.h"
#include "ScriptServerEngine.h"
#include "Event/LuaEventHandler/LuaScriptVM.h"
#include "SaveScriptTask.h"
#include "KEPFileNames.h"
#include "LogHelper.h"

struct lua_State;

static LogInstance("ScriptServerEngine");

namespace KEP {

ServerScriptPtr ScriptMap::getScriptByVM(void* vm) {
	lua_State* L = reinterpret_cast<lua_State*>(vm);
	ZoneIndex zoneIndex = ServerScript::GetZoneIndexFromRegistry(L);
	assert(zoneIndex.toLong() != 0);

	if (zoneIndex.toLong() == 0) {
		LogWarn("Lua VM does not carry a valid zone index in the registry: 0x" << std::hex << std::setw(8) << std::setfill('0') << vm);
	} else {
		ScriptZonePtr pZone = getZone(zoneIndex);
		if (pZone) {
			return pZone->getScriptByVM(vm);
		}
	}

	return ServerScriptPtr();
}

// If we check m_scripts first, then if the object
// has a script it will 'override' other script's
// attempts to get the event.  If we check the
// m_ids first, the event will be redirected.
// It's best, I think, if a script always has
// the first chance to get the event, so this override
// will only work on objects that do not have scripts
// on them, unless the handlerMask is used to say
// that even if the script exists, override it if
// it doesn't implement the handler.
ServerScriptPtr ScriptMap::getScriptByID(ScriptPID scriptId, ZoneIndex zoneIndex, bool useMap) {
	ScriptZonePtr pZone = getZone(zoneIndex);
	if (pZone) {
		return pZone->getScript(scriptId, useMap);
	}

	return ServerScriptPtr();
}

std::vector<ScriptPID> ScriptZone::getAllScriptIDs() const {
	std::lock_guard<std::mutex> lock(m_mutex);

	std::vector<ScriptPID> allScriptIDs;
	for (const auto& pr : m_scripts) {
		allScriptIDs.push_back(pr.second->GetPlacementId());
	}
	return allScriptIDs;
}

std::vector<ScriptPID> ScriptZone::getScriptIDsAcceptingBroadcast(bool interZone) const {
	std::lock_guard<std::mutex> lock(m_mutex);

	std::vector<ScriptPID> scriptIDs;
	for (const auto& pr : m_scripts) {
		if (!interZone && pr.second->AcceptsInZoneBroadcast() || interZone && pr.second->AcceptsInterZoneBroadcast()) {
			scriptIDs.push_back(pr.second->GetPlacementId());
		}
	}
	return scriptIDs;
}

void ScriptZone::addScript(ScriptPID scriptId, const ServerScriptPtr& script) {
	std::lock_guard<std::mutex> lock(m_mutex);

	assert(m_scripts.find(scriptId) == m_scripts.end());
	m_scripts.insert(std::make_pair(scriptId, script));
	m_vms.insert(make_pair(script->LuaGlobalState(), script));
	m_vms.insert(make_pair(script->LuaThreadState(), script));
}

ServerScriptPtr ScriptZone::removeScript(ScriptPID scriptId) {
	std::lock_guard<std::mutex> lock(m_mutex);

	// Delete all references to scriptId from the pid map.
	// That is, all entries where some id mapped to this
	// (now removed) server script.
	for (auto itrMap = m_pidMap.begin(); itrMap != m_pidMap.end();) {
		if (itrMap->second == scriptId)
			itrMap = m_pidMap.erase(itrMap);
		else
			++itrMap;
	}

	auto itr = m_scripts.find(scriptId);
	if (itr == m_scripts.end()) {
		// Not found
		return false;
	}

	ServerScriptPtr pScript = itr->second;

	// Erase from m_vms map
	m_vms.erase(pScript->LuaGlobalState());
	m_vms.erase(pScript->LuaThreadState());

	// Erase from master map
	m_scripts.erase(itr);

	// Check if zone is stopped (no more outstanding scripts)
	if (m_stopping && m_scripts.empty()) {
		// Script list just turned empty - clear zone stopping flag - allowing script sever to forward player arrive events
		m_stopping = false;
	}

	return pScript;
}

ScriptServerEvent* ScriptZone::setEffectCompletionEvent(int objectPID, EffectCode type, ScriptServerEvent* pEvent) {
	ScriptServerEvent* pOldEvent = nullptr;
	auto key = std::make_pair(objectPID, type);

	std::lock_guard<std::mutex> lock(m_effectCompletionEventsMutex);
	auto itr = m_effectCompletionEventMap.find(key);
	if (itr != m_effectCompletionEventMap.end()) {
		pOldEvent = itr->second;
		m_effectCompletionEventMap.erase(itr);
	}

	if (pEvent != nullptr) {
		m_effectCompletionEventMap[key] = pEvent;
	}
	return pOldEvent;
}

bool ScriptZone::clearEffectCompletionEvent(int objectPID, EffectCode type, ScriptServerEvent* pEvent) {
	assert(pEvent != nullptr);

	auto key = std::make_pair(objectPID, type);

	std::lock_guard<std::mutex> lock(m_effectCompletionEventsMutex);
	auto itr = m_effectCompletionEventMap.find(key);
	if (itr != m_effectCompletionEventMap.end()) {
		if (itr->second == pEvent) {
			m_effectCompletionEventMap.erase(itr);
			return true;
		}
	}

	return false;
}

} // namespace KEP
