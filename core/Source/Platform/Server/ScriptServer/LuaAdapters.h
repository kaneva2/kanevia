///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

// YC 03/2015
// Interoperability between C data type and Lua stack

#pragma once

#include "LuaHelper.h"
#include "LuaStackHelper.h"

namespace KEP {

class ILuaAdapter {
public:
	virtual ~ILuaAdapter() {}
	virtual void push(void* vm) = 0; // Push a value to Lua stack
	virtual void get(void* vm, int index) = 0; // Retrieve a value from Lua stack
};

class LuaNil : public ILuaAdapter {
public:
	virtual void push(void* vm) {
		lua_pushnil((lua_State*)vm);
	}
	virtual void get(void* vm, int index) {
		vm; // Do nothing for nil
		index;
	}
};

// C primitive value <=> Lua primitive value
template <typename V>
class LuaPrimValue : public ILuaAdapter {
public:
	V val;
	LuaPrimValue(V prim) :
			val(prim) {}
	virtual void push(void* vm) {
		lua_pushAny((lua_State*)vm, val);
	}
	virtual void get(void* vm, int index) {
		val = lua_toAny<V>((lua_State*)vm, index);
	}
};

// C struct <=> Lua table
class ILuaStruct : public ILuaAdapter {
public:
	virtual const char* typeName() const = 0;

	// Inherits push and get which work with Lua table with string keys

	// Interface with Lua array
	virtual void pushArray(void* vm) = 0; // Push a Lua array to stack with all data members in sequential order (1-based)
	virtual void getArray(void* vm, int index) = 0; // Retrieve data members from a Lua array previously created by pushArray
};

// 3D vector in double ( x / y / z )
class LuaStructVector : public ILuaStruct {
public:
	double x, y, z;
	LuaStructVector(double xx = 0, double yy = 0, double zz = 0) :
			x(xx), y(yy), z(zz) {}

	virtual const char* typeName() const {
		return "Vector";
	}

	virtual void push(void* vm);
	virtual void get(void* vm, int index);
	virtual void pushArray(void* vm);
	virtual void getArray(void* vm, int index);
};

// Combination of position and rotation
class LuaStructPosRot : public LuaStructVector {
public:
	double rx, ry, rz;
	LuaStructPosRot(double xx = 0, double yy = 0, double zz = 0, double rxx = 0, double ryy = 0, double rzz = 1) :
			LuaStructVector(xx, yy, zz), rx(rxx), ry(ryy), rz(rzz) {}

	virtual const char* typeName() const {
		return "PosRot";
	}

	virtual void push(void* vm);
	virtual void get(void* vm, int index);
	virtual void pushArray(void* vm);
	virtual void getArray(void* vm, int index);
};

#define MAX_LUA_VALUE_CONTAINER_SIZE 256 // Hard-code to 256 for now

} // namespace KEP
