///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>

//Asynchronous API support
struct lua_State;

namespace KEP {

class IReadableBuffer;
class IWriteableBuffer;

struct AsyncUserArg {
	int m_luaType;
	int m_userInt;
	std::string m_userStr;
	AsyncUserArg() :
			m_luaType(-1), m_userInt(0) {}
};

IReadableBuffer& operator>>(IReadableBuffer& ibuf, AsyncUserArg& userArg);
IWriteableBuffer& operator<<(IWriteableBuffer& obuf, const AsyncUserArg& userArg);

typedef int (*LuaAsyncFunction)(lua_State* L, const AsyncUserArg& userArgs);

} // namespace KEP
