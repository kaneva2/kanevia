///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LuaAdapters.h"
#include <string>
using namespace std;

namespace KEP {

//////////////////////////////////////////////
// LuaCStructXYZ - 3D vector in double

void LuaStructVector::push(void* vm) {
	lua_State* L = (lua_State*)vm;
	lua_newtable(L);
	lua_setField(L, "x", x);
	lua_setField(L, "y", y);
	lua_setField(L, "z", z);
}

void LuaStructVector::get(void* vm, int index) {
	lua_State* L = (lua_State*)vm;
	x = lua_getField<double>(L, "x", index);
	y = lua_getField<double>(L, "y", index);
	z = lua_getField<double>(L, "z", index);
}

void LuaStructVector::pushArray(void* vm) {
	lua_State* L = (lua_State*)vm;
	lua_newtable(L);
	lua_setField(L, 1, x);
	lua_setField(L, 2, y);
	lua_setField(L, 3, z);
}

void LuaStructVector::getArray(void* vm, int index) {
	lua_State* L = (lua_State*)vm;
	x = lua_getField<double>(L, 1, index);
	y = lua_getField<double>(L, 2, index);
	z = lua_getField<double>(L, 3, index);
}

//////////////////////////////////////////////
// LuaCStructPosRot - Position and Rotation

void LuaStructPosRot::push(void* vm) {
	lua_State* L = (lua_State*)vm;
	LuaStructVector::push(vm);
	lua_setField(L, "rx", rx);
	lua_setField(L, "ry", ry);
	lua_setField(L, "rz", rz);
}

void LuaStructPosRot::get(void* vm, int index) {
	lua_State* L = (lua_State*)vm;
	LuaStructVector::get(vm, index);
	rx = lua_getField<double>(L, "rx", index);
	ry = lua_getField<double>(L, "ry", index);
	rz = lua_getField<double>(L, "rz", index);
}

void LuaStructPosRot::pushArray(void* vm) {
	lua_State* L = (lua_State*)vm;
	LuaStructVector::pushArray(vm);
	lua_setField(L, 4, rx);
	lua_setField(L, 5, ry);
	lua_setField(L, 6, rz);
}

void LuaStructPosRot::getArray(void* vm, int index) {
	lua_State* L = (lua_State*)vm;
	LuaStructVector::getArray(vm, index);
	rx = lua_getField<double>(L, 4, index);
	ry = lua_getField<double>(L, 5, index);
	rz = lua_getField<double>(L, 6, index);
}

} // namespace KEP