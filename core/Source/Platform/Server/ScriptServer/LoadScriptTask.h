///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptServerTaskObject.h"
#include "InstanceId.h"
#include "Tools\NetworkV2\INetwork.h"
#include <string>

namespace KEP {

class KGP_Connection;

class LoadScriptTask : public ScriptServerTaskObject {
public:
	LoadScriptTask(
		//		log4cplus::Logger& logger,
		KGP_Connection* conn,
		fast_recursive_mutex& dbSync,
		ZoneIndex zoneId,
		NETID clientNetId,
		const std::string& playerName,
		bool loadSystemScripts,
		bool loadSystemScriptsSynch,
		DWORD kgpStartWaitTimeout);

	virtual void ExecuteTask() override;

	void StopByZoneIndex(const ZoneIndex& zi) {
		if (m_zoneId == zi) {
			m_stopping = true;
		}
	}

	void LoadZoneSpinDownDelayMinutesFromDB(const ZoneIndex& zoneIndex);

private:
	//	log4cplus::Logger& m_logger;
	KGP_Connection* m_conn;
	fast_recursive_mutex& m_dbSync;
	const ZoneIndex m_zoneId;
	const NETID m_clientNetId;
	std::string m_playerName;
	bool m_loadSystemScripts; // Run scripts from kgp_common.preload_scripts before any DO scripts
	bool m_loadSystemScriptsSynch; // If true, run system scripts synchronously.
	DWORD m_kgpStartWaitTimeout; // Maximum wait duration for a blocking kgp_start function call
	bool m_stopping; // Must acquire m_eventHandlerCS before accessing this flag
};

}; // namespace KEP
