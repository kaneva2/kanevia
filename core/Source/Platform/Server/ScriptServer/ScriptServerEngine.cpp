///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ScriptServerEngine.h"
#include "Common/KEPUtil/VLDWrapper.h"
#include "ServerScript.h"
#include "Script_API.h"
#include "LuaAsyncFunc.h"
#include "LuaAdapters.h"
#include "LuaUserData.h"
#include "MySqlUtil.h"
#include "RunVMTask.h"
#include "SaveScriptTask.h"
#include "DetachScriptTask.h"
#include "LoadScriptTask.h"
#include "PublishTask.h"
#include "DeleteAssetTask.h"
#include "ScriptWebCallTask.h"
#include "TransmitGameStateTask.h"
#include "InventoryType.h"
#include "ScriptMap.h"
#include "JsonConfig.h"
#include "ObjectLabelInfo.h"
#include "ScriptRuntimeData.h"
#include "GameStateManager.h"
#include "IPlayer.h"
#include "KanevaClientAppender.h"
#include "GameItem.h"
#include "Common/KEPUtil/DistributionInfo.h"
#include "Engine/Blades/EngineBlade/IEngine.h"
#include "IServerEngine.h"
#include "Event/Events/ScriptServerEvent.h"
#include "Event/Events/ScriptClientEvent.h"
#include "Event/EventSystem/BEREncodingFactory.h"
#include "Event/Events/LoadItemsBackgroundEvent.h"
#include "Event/Events/UpdateScriptZoneSpinDownDelayEvent.h"
#include "Event/LuaEventHandler/LuaScriptVM.h"
#include "Event/PlayListMgr/PLMMediaData.h"
#include "HttpClient/GetBrowserPage.h"
#include "../KEPServer/KEPServerEnums.h"
#include "Core/Math/Rotation.h"
#include "Core/CorePluginLibrary/MessageQueue.h"
#include "KEPConstants.h"
#include "LogHelper.h"
#include "Common/KEPUtil/LocationRegistry.h"
#include "Scheduler.h"
#include "js.h"
#include "jsEnumFiles.h"

using namespace mysqlpp;

namespace KEP {

static LogInstance("ScriptServerEngine");

LogInstanceAs(script_logger, "ServerScript");

// Temporary - to be removed after 16.7 is released
static ScriptServerEvent* ConvertLegacyObjectClickEvent(ScriptServerEvent* se);

// Declare VM Associated Server Script Pointer
#define ASSERT_VM_SS(...)                                                    \
	ASSERT(vm);                                                              \
	if (!vm) {                                                               \
		LogError("vm is null");                                              \
		return __VA_ARGS__;                                                  \
	}                                                                        \
	ServerScriptPtr ss = m_zoneMap.getScriptByVM(vm);                        \
	ASSERT(ss);                                                              \
	if (!ss) {                                                               \
		LogError("Failed to find a script for vm " << IntToHexStr((int)vm)); \
		return __VA_ARGS__;                                                  \
	}

// Class static members
ScriptServerEngine* ScriptServerEngine::m_instance = NULL;

ULONG ScriptServerEngine::m_maxScriptsPerZone = DEFAULT_MAX_SCRIPTS_PER_ZONE;

ScriptServerEngine::ScriptServerEngine() :
		m_dbConfig(NULL),
		m_maxLoopTime(250),
		m_processEventTime(100),
		m_maxConnections(2000),
		m_conn(new KGP_Connection(use_exceptions)),
		m_myDispatcher(NULL),
		m_taskSystem(this),
		m_loadScriptTaskSystem(this),
		m_taskSystemThreadCount(TaskSystem::DEFAULT_WORKER_THREADS),
		m_patchURLRegistered(false),
		m_dictionaryAutoIndex(0),
		m_allowedUploadAssetTypes(DEFAULT_ALLOWED_UPLOAD_ASSET_TYPES),
		m_isolateDictionariesByZone(false),
		m_eventHandlerCS(),
		m_dbAccessCS(),
		m_zoneInfoMapMutex(),
		m_scriptAPIRegistry(NULL),
		m_worldExperimentUrl(NET_DEFAULT_WORLDEXPERIMENT_URL),
		m_globalJson(NULL),
		m_messageQueueAgent(new NullMessageQueueAgent()),
		m_isMyMessageQueueAgent(true),
		m_defaultSpinDownDelayMinutes(DEFAULT_SCRIPT_ZONE_SPIN_DOWN_DELAY_MINUTES),
		m_lastZoneSpinDownExpiryCheckTimeMs(0),
		m_pScheduler(std::make_unique<Scheduler>(m_logger, m_taskSystem)) {
	LogWarn("SSE<" << this << ">");

	if (m_instance)
		LogError("Singleton instance already exists");
	m_instance = this;

	ServerScript::InitEventHandlerMap();

	m_scriptAPIRegistry = new ScriptAPIRegistry();

	try {
		MessageQueueAgent* pMessageQueueAgent = PluginHost::singleton().DEPRECATED_getInterface<MessageQueueAgent>("MessageQueue");

		// There is a plugin that provides MessageQueueAgent functionality.  Use it.
		//
		if (pMessageQueueAgent != 0) {
			// this means we need to delete the default implementation (NullMessageQueueAgent)
			//
			delete m_messageQueueAgent;

			// Flag it as not being ours to delete.
			//
			m_isMyMessageQueueAgent = false;

			// And finally, reference the plugin implementation
			//
			m_messageQueueAgent = pMessageQueueAgent;
		}
	} catch (const PluginException& pe) {
		LogError(pe.str());
	}
}

ScriptServerEngine::~ScriptServerEngine() {
	LogWarn("SSE<" << this << ">");
	FreeAll();
}

bool ScriptServerEngine::SendToClient(IEvent* e) {
	if (!e)
		return false;

	LoadItemsBackgroundEvent* libe = dynamic_cast<LoadItemsBackgroundEvent*>(e);
	if (!libe) {
		if (e->From() != 0) {
			ScriptClientEvent* sce = dynamic_cast<ScriptClientEvent*>(e);
			if (!sce || (sce->GetRoutingOption() != ScriptClientEvent::Routing_ServerEngine))
				e->AddTo(e->From()); // Send directly to client
		}
		m_myDispatcher->QueueEvent(e);
	} else {
		IServerEngine::GetInstance()->GetGameStateDispatcher()->QueueEvent(libe);
	}

	return true;
}

bool ScriptServerEngine::SendDirectlyToClient(NETID clientId, IEvent* sce) {
	sce->AddTo(clientId);
	m_myDispatcher->QueueEvent(sce);
	return true;
}

bool ScriptServerEngine::SendDirectlyToZone(const ZoneIndex& zoneIndex, IEvent* sce) {
	IServerEngine::GetInstance()->BroadcastEventFromZoneIndex(sce, zoneIndex);
	return true;
}

bool ScriptServerEngine::ObjectDeleteCallback(void* vm, int objId) {
	ASSERT_VM_SS(false);

	// The GS will broadcast the change, but won't update the database (so placed
	// objects won't be removed when a new instance is created).
	ScriptClientEvent* sce = ScriptClientEvent::EncodeObjectDelete(NULL, objId, ss->GetZoneId().toLong(), ss->GetZoneId().GetInstanceId());
	SendToClient(sce);

	// Remove local references to the object and any associated script or script mapping.
	ss->GetGameStateManager()->RemoveGameStateObject(ss->GetZoneId(), objId);

	// Obtain script for the object being deleted
	ServerScriptPtr ssDel = ss->GetScriptZone()->getScript(objId, false);
	if (ssDel) {
		std::lock_guard<fast_recursive_mutex> lock(m_eventHandlerCS); // To avoid deadlock, double check any locking operation before we hit this line. Should be none.
		SendScriptShutdownEvent(ssDel, 0, "kgp.objectDelete");
	}

	return true;
}

ULONG ScriptServerEngine::ObjectGenerateCallback(
	void* vm,
	GLID glid,
	double x, double y, double z,
	double rx, double ry, double rz,
	int placementType, bool bufferCall) {
	ASSERT_VM_SS(0);

	ULONG newPlacementId = GetIncrementedZoneMaxPlacementId(ss->GetZoneId());

	// clientId of 0 is ok, because this will be processed by the game server.
	ScriptClientEvent* sce = ScriptClientEvent::EncodeObjectGenerate(NULL, newPlacementId, glid, ss->GetZoneId().toLong(), ss->GetZoneId().GetInstanceId(), x, y, z, rx, ry, rz, placementType, INVALID_GAMEITEM);

	if (bufferCall) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObject(ss->GetZoneId(), e, newPlacementId);
	}

	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		// Need information about the GLID
		std::vector<GLID> glids;
		glids.push_back(glid);
		IEvent* libe = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
		SendToClient(libe);
	} else {
		// Either we already cached the GLID or it's not needed (e.g. GLID 0 for NPC)
		SendToClient(sce);
	}

	return newPlacementId;
}

ScriptAPIRetType::BLOC ScriptServerEngine::GameItemPlaceCallback(
	void* vm,
	ULONG clientId,
	GLID glid,
	int gameItemId,
	double x, double y, double z,
	double rx, double ry, double rz) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ZoneIndex base_zone_id = ss->GetZoneId().GetBaseZoneIndex();
	ULONG newPlacementId = 1;

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlaceGameItem, 0);
	*sce->OutBuffer() << newPlacementId << clientId << (ULONG)glid << gameItemId
					  << ss->GetZoneId().toLong() << ss->GetZoneId().GetInstanceId() << ss->GetPlacementId()
					  << (float)x << (float)y << (float)z
					  << (float)rx << (float)ry << (float)rz;

	IEvent* eventToQueue = sce;
	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		eventToQueue = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
	}

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlaceGameItem, "gameItemPlace", eventToQueue);
}

ScriptAPIRetType::BLOC ScriptServerEngine::GameItemPlaceAsUserCallback(
	void* vm,
	ULONG kanevaUserId,
	GLID glid,
	int gameItemId,
	double x, double y, double z,
	double rx, double ry, double rz) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ULONG zoneIndex = ss->GetZoneId().GetBaseZoneIndex();
	ULONG zoneInstanceId = ss->GetZoneId().GetInstanceId();

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ReplaceGameItem, 0);
	*sce->OutBuffer() << glid << x << y << z << rx << ry << rz << zoneIndex << zoneInstanceId << kanevaUserId << gameItemId << ss->GetPlacementId();

	IEvent* eventToQueue = sce;
	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		eventToQueue = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
	}

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlaceGameItem, "gameItemPlaceAsUser", eventToQueue);
}

// AS - This is different from GameItemPlace and other move routines in that it directly updates the SQL
//      database with game item position and rotation for persistence across clients and game sessions.
bool ScriptServerEngine::GameItemSetPosRotCallback(
	void* vm,
	ULONG pid,
	double x, double y, double z,
	double rx, double ry, double rz) {
	ASSERT_VM_SS(false);

	Vector3f direction(rx, ry, rz);
	if (!direction.Normalize()) {
		LogError("rotation vecLen=" << direction.Length() << " too small");
		return false;
	}
	Vector3f up(0, 1, 0);
	Vector3f slide = up.Cross(direction);
	if (!slide.Normalize())
		slide.Set(1, 0, 0);

	bool result = false;
	unsigned int zone_instance_id = ss->GetZoneId().GetInstanceId();
	// Note: the SQL database expects player_index which is the same as the cleardInstance ID.
	unsigned int clearedInstance = ss->GetZoneId().getClearedInstanceId();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);
	Query query = m_conn->query();

	// Benjamin Whatley's suggestion so the update only affects game items...
	query << " UPDATE dynamic_objects AS do"
		  << " INNER JOIN dynamic_object_parameters dop ON do.obj_placement_id = dop.obj_placement_id AND dop.param_type_id = 33 "
		  << " SET do.position_x = %0, do.position_y = %1, do.position_z = %2, "
		  << " do.rotation_x = %3, do.rotation_y = %4, do.rotation_z = %5, "
		  << " do.slide_x = %9, do.slide_y = %10, do.slide_z = %11 "
		  << " WHERE do.obj_placement_id = %6 AND do.zone_instance_id = %7 AND do.zone_index = %8";

	query.parse();
	query.execute(
		(float)x, (float)y, (float)z,
		(float)rx, (float)ry, (float)rz,
		(UINT)pid, zone_instance_id, clearedInstance,
		slide.x, slide.y, slide.z);

	trans.commit();
	result = true;
	END_TRY_SQL_LOG_ONLY(m_conn);

	if (!result) {
		LogError("Caught Exception in TRY_SQL(update dynamic_objects)...\n");
		return false;
	}

	ScriptClientEvent* sce;
	// The ScriptClientEvent::ObjectMoveAndRotate event will update all other connected clients
	// with the new pos rot data passed with the ScriptClientEvent.
	sce = new ScriptClientEvent(ScriptClientEvent::ObjectMoveAndRotate, 0);
	*sce->OutBuffer()
		<< (int)pid
		<< ss->GetZoneId().toLong() << ss->GetZoneId().GetInstanceId()
		<< (float)x << (float)y << (float)z
		<< direction.x << direction.y << direction.z
		<< slide.x << slide.y << slide.z
		<< 0.0;

	IServerEngine::GetInstance()->ClearZoneDOMemCache(ss->GetZoneId());

	// Report new start location to game state manager
	ss->GetGameStateManager()->AddGameStateObjectStartLocation(ss->GetZoneId(), pid, x, y, z, direction.x, direction.y, direction.z, true, true);

	// AddGameStateObjectModifier may or may not be needed since the SQL database is already updated and the ObjectMoveAndRotate event
	// should sync all current clients. Will investigate with a test case and remove if redundant.
	// So, After testing it appears the AddGameStateObjectModifier is not needed. However, Jonny believes there might be a race condition
	// that could happen and is decided to leave this out unless we find any issues as is currently implmented.
	return SendToClient(sce);
}

bool ScriptServerEngine::SoundPlayOnClient(void* vm, int globalId, ULONG targetId) {
	ASSERT_VM_SS(false);

	ScriptClientEvent* sce;
	if (targetId == 0) {
		// clientId of 0 is ok, because this will be processed by the game server.
		sce = new ScriptClientEvent(ScriptClientEvent::SoundPlayOnClient, 0); //goes to everyone, send to server to broadcast
		*sce->OutBuffer() << ss->GetZoneId().toLong();
	} else {
		sce = new ScriptClientEvent(ScriptClientEvent::SoundPlayOnClient, targetId, ScriptClientEvent::Scope_Player); //single target, go directly to client
	}
	*sce->OutBuffer() << globalId;

	return SendToClient(sce);
}

bool ScriptServerEngine::SoundStopOnClient(void* vm, int globalId, ULONG targetId) {
	ASSERT_VM_SS(false);

	ScriptClientEvent* sce;
	if (targetId == 0) {
		// clientId of 0 is ok, because this will be processed by the game server.
		sce = new ScriptClientEvent(ScriptClientEvent::SoundStopOnClient, 0); //goes to everyone, send to server to broadcast
		*sce->OutBuffer() << ss->GetZoneId().toLong();
	} else {
		sce = new ScriptClientEvent(ScriptClientEvent::SoundStopOnClient, targetId, ScriptClientEvent::Scope_Player); //single target, go directly to client
	}
	*sce->OutBuffer() << globalId;

	return SendToClient(sce);
}

bool ScriptServerEngine::SoundAttachCallback(void* vm, int soundPlacementId, int attachType, ULONG targetId, bool bufferCall) {
	ASSERT_VM_SS(false);

	// clientId of 0 is ok, because this will be processed by the game server.
	ScriptClientEvent* sce = ScriptClientEvent::EncodeSoundAttachTo(NULL, (ULONG)soundPlacementId, targetId, attachType, ss->GetZoneId().toLong());

	if (bufferCall) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, soundPlacementId);
	}

	return SendToClient(sce);
}

ScriptAPIRetType::BLOC ScriptServerEngine::PlayerGetLocationCallback(void* vm, ULONG clientId) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerGetLocation, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << ss->GetPlacementId(); // So the game server knows where to send the reply.
	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerLocation, "playerGetLocation", sce);
}

ScriptAPIRetType::BLOC ScriptServerEngine::PlayerCastRayCallback(void* vm, ULONG clientId) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerCastRay, clientId);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << ss->GetPlacementId(); // placement Id needed in the response
	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerRayIntersects, "playerGetObjectInFront", sce);
}

std::string ScriptServerEngine::PlayerGetNameCallback(void* vm, ULONG clientId) {
	ASSERT_VM_SS("");

	std::string playerName = "";
	NETIDPlayerInfoMap::iterator info = m_players.find(clientId);
	if (info != m_players.end())
		playerName = (*info).second.m_name;
	return playerName;
}

std::string ScriptServerEngine::PlayerGetTitleCallback(void* vm, ULONG clientId) {
	ASSERT_VM_SS("");

	std::string playerTitle = "";
	NETIDPlayerInfoMap::iterator info = m_players.find(clientId);
	if (info != m_players.end())
		playerTitle = (*info).second.m_title;
	return playerTitle;
}

int ScriptServerEngine::PlayerGetPermissionsCallback(void* vm, ULONG clientId) {
	ASSERT_VM_SS(0);

	int playerPermissions = -1;
	NETIDPlayerInfoMap::iterator info = m_players.find(clientId);
	if (info != m_players.end())
		playerPermissions = (*info).second.m_permissions;
	return playerPermissions;
}

ScriptAPIRetType::BLOC ScriptServerEngine::PlayerUseItemCallback(void* vm, ULONG clientId, GLID glid) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerUseItem, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << glid << ss->GetPlacementId(); // So the game server knows where to send the reply.

	IEvent* eventToQueue = sce;
	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		eventToQueue = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
	}

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerUseItem, "playerUseItem", eventToQueue);
}

ScriptAPIRetType::BLOC ScriptServerEngine::PlayerEquipItemCallback(void* vm, ULONG clientId, GLID glid) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerEquipItem, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << glid << ss->GetPlacementId(); // So the game server knows where to send the reply.

	IEvent* eventToQueue = sce;
	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		eventToQueue = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
	}

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerEquipItem, "playerEquipItem", eventToQueue);
}

ScriptAPIRetType::BLOC ScriptServerEngine::PlayerUnEquipItemCallback(void* vm, ULONG clientId, GLID glid) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerUnEquipItem, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << glid << ss->GetPlacementId(); // So the game server knows where to send the reply.

	IEvent* eventToQueue = sce;
	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		eventToQueue = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
	}

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerUnEquipItem, "playerUnEquipItem", eventToQueue);
}

// DRF - Added
ScriptAPIRetType::BLOC ScriptServerEngine::PlayerSaveEquippedCallback(void* vm, ULONG clientId, const std::string& outfitName) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSaveEquipped, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << outfitName << ss->GetPlacementId();

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerSaveEquipped, "playerSaveEquipped", sce);
}

// DRF - Added
ScriptAPIRetType::BLOC ScriptServerEngine::PlayerGetEquippedCallback(void* vm, ULONG clientId) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerGetEquipped, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << ss->GetPlacementId();

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerGetEquipped, "playerGetEquipped", sce);
}

ScriptAPIRetType::BLOC ScriptServerEngine::PlayerSetEquippedCallback(void* vm, ULONG clientId, const std::string& glids, const std::string& outfitName) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSetEquipped, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << glids << outfitName << ss->GetPlacementId();

	// Explode Glid List (comma separated values)
	std::vector<GLID> glidVec;
	StringHelpers::explode(glidVec, glids);

	// Load Unloaded Glids
	std::vector<GLID> unloadedGlids;
	IEvent* eventToQueue = sce;
	if (IServerEngine::GetInstance()->checkGlids(glidVec, unloadedGlids) == IServerEngine::NotLoaded) {
		eventToQueue = IServerEngine::GetInstance()->createLoadGlidEvent(unloadedGlids, sce);
	}

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerSetEquipped, "playerSetEquipped", eventToQueue);
}

ScriptAPIRetType::BLOC ScriptServerEngine::PlayerCheckInventoryCallback(void* vm, ULONG clientId, int globalId) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerCheckInventory, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << globalId << ss->GetPlacementId(); // So the game server knows where to send the reply.

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerCheckInventory, "playerCheckInventory", sce);
}

void ScriptServerEngine::PlayerRemoveItemCallback(void* vm, ULONG clientId, int globalId) {
	ASSERT_VM_SS();

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerRemoveItem, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << globalId << ss->GetPlacementId(); // So the game server knows where to send the reply.

	m_pScheduler->scriptCallBlocked(ss, ScriptServerEvent::PlayerRemoveItem, "playerRemoveItem");

	SendToClient(sce);
}

ScriptAPIRetType::BLOC ScriptServerEngine::PlayerAddItemCallback(void* vm, ULONG clientId, GLID glid, int quantity) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerAddItem, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << glid << quantity << ss->GetPlacementId(); // So the game server knows where to send the reply.

	IEvent* eventToQueue = sce;
	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		eventToQueue = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
	}

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerAddItem, "playerAddItem", eventToQueue);
}

ScriptAPIRetType::BLOC ScriptServerEngine::PlayerJoinInstanceCallback(void* vm, int instanceId, ULONG clientId) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerJoinInstance, 0);
	*sce->OutBuffer() << ss->GetPlacementId() << ss->GetZoneId().toLong() << instanceId << clientId; // So the game server knows where to send the reply.

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerJoinInstance, "playerJoinInstance", sce);
}

void ScriptServerEngine::PlayerSetEquippedItemAnimationCallback(void* vm, ULONG clientId, GLID itemGLID, GLID animGLID) {
	ASSERT_VM_SS();

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSetEquippedItemAnimation, 0);
	*sce->OutBuffer() << clientId << itemGLID << animGLID;
	std::vector<GLID> glidVecIn;
	std::vector<GLID> unloadedGlids;
	glidVecIn.push_back(itemGLID);
	glidVecIn.push_back(animGLID);
	if (IServerEngine::GetInstance()->checkGlids(glidVecIn, unloadedGlids) == IServerEngine::NotLoaded) {
		IEvent* libe = IServerEngine::GetInstance()->createLoadGlidEvent(unloadedGlids, sce);
		SendToClient(libe);
	} else {
		SendToClient(sce);
	}
}

ScriptAPIRetType::BLOC ScriptServerEngine::PlayerRemoveAllAccessories(void* vm, ULONG clientId) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerRemoveAllAccessories, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << clientId << ss->GetPlacementId();

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::PlayerRemoveAllAccessories, "playerRemoveAllAccessories", sce);
}

void ScriptServerEngine::PlayerSetAnimationCallback(void* vm, ULONG clientId, GLID glid) {
	ASSERT_VM_SS();

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSetAnimation, 0);
	*sce->OutBuffer() << clientId << glid;

	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		IEvent* libe = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
		SendToClient(libe);
	} else {
		SendToClient(sce);
	}
}

void ScriptServerEngine::PlayerDefineAnimationCallback(void* vm, ULONG clientId, GLID standId, GLID runId, GLID walkId, GLID jumpId) {
	ASSERT_VM_SS();

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerDefineAnimation, 0);
	*sce->OutBuffer() << clientId << standId << runId << walkId << jumpId;
	SendToClient(sce);
}

bool ScriptServerEngine::PlayerSetPhysics(void* vm, ULONG clientId, std::vector<int> arg_vect) {
	ASSERT_VM_SS(false);

	int size = arg_vect.size();

	ScriptClientEvent* sce;
	if (clientId != 0)
		sce = new ScriptClientEvent(ScriptClientEvent::PlayerSetPhysics, clientId, ScriptClientEvent::Scope_Player);
	else
		sce = new ScriptClientEvent(ScriptClientEvent::PlayerSetPhysics, 0, ScriptClientEvent::Scope_Zone);
	*sce->OutBuffer() << ss->GetZoneId().toLong(); // this parameter is required either the scope is per zone or per player as client is expecting it
	*sce->OutBuffer() << size;
	for (int i = 0; i < size; i++)
		*sce->OutBuffer() << arg_vect[i];

	return SendToClient(sce);
}

char ScriptServerEngine::PlayerGetGender(void* vm, ULONG clientId) {
	ASSERT_VM_SS((char)0);

	NETIDPlayerInfoMap::iterator info = m_players.find(clientId);
	if (info != m_players.end())
		return info->second.m_gender;
	return 0;
}

bool ScriptServerEngine::PlayerSetTitleCallback(void* vm, ULONG clientId, const char* title) {
	ASSERT_VM_SS(false);

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSetTitle, 0);
	*sce->OutBuffer() << clientId << ss->GetPlacementId() << title; // So the game server knows where to send the reply.

	return SendToClient(sce);
}

bool ScriptServerEngine::PlayerScaleCallback(void* vm, ULONG clientId, double x, double y, double z) {
	ASSERT_VM_SS(false);

	// Use 0 (server) as the client net id in the event ctor, so that the server process it.
	// The server will create a SCE that can be broadcast to clients in the zone.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerScale, 0);
	*sce->OutBuffer() << clientId << ss->GetZoneId().toLong() << x << y << z;

	return SendToClient(sce);
}

bool ScriptServerEngine::PlayerSetNameCallback(void* vm, ULONG clientId, const char* name) {
	ASSERT_VM_SS(false);

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSetName, 0);
	*sce->OutBuffer() << clientId << ss->GetPlacementId() << name << ss->GetZoneId().toLong(); // So the game server knows where to send the reply.

	return SendToClient(sce);
}

bool ScriptServerEngine::PlayerMove(void* vm, ULONG clientId, double x, double y, double z, double rx, double ry, double rz, double speed, int anim, int timeout, int recalcInterval, int aiMode, bool noPlayerControl, bool noPhysics) {
	ASSERT_VM_SS(false);

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerMove, clientId);
	*sce->OutBuffer() << x << y << z << rx << ry << rz << speed << anim << timeout << recalcInterval << aiMode << noPlayerControl << noPhysics;

	return SendToClient(sce);
}

bool ScriptServerEngine::PlayerRenderLabelsOnTop(void* vm, ULONG clientId, bool onTop) {
	ASSERT_VM_SS(false);

	// send this directly to the client
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::RenderLabelsOnTop, clientId);
	*sce->OutBuffer() << onTop;

	return SendToClient(sce);
}

bool ScriptServerEngine::ObjectGetStartLocationCallback(
	void* vm,
	int objId,
	double& x, double& y, double& z,
	double& rx, double& ry, double& rz) {
	ASSERT_VM_SS(false);

	bool ret = false;
	x = y = z = ry = rz = 0;
	rx = 1;

	//Get cleared zone index for database
	ZoneIndex zoneIndex = ss->GetZoneId().GetBaseZoneIndex();
	unsigned int zone_index = zoneIndex.getClearedInstanceId();
	unsigned int zone_instance_id = zoneIndex.GetInstanceId();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();

	Query query = m_conn->query();
	query << "SELECT `position_x`, `position_y`, `position_z`, `rotation_x`, `rotation_y`, `rotation_z`, `slide_x`,  `slide_y`, `slide_z` "
			 "FROM  `dynamic_objects` "
			 "WHERE `obj_placement_id` =  %0 AND `zone_index` =  %1 AND `zone_instance_id` = %2;";

	query.parse();

	LogDebug(query.str(objId, zone_index, zone_instance_id));

	StoreQueryResult res = query.store(objId, zone_index, zone_instance_id);

	if (res.size() == 1) {
		Row r = res.at(0);

		x = r.at(0);
		y = r.at(1);
		z = r.at(2);

		rx = r.at(3);
		ry = r.at(4);
		rz = r.at(5);

		ret = true;
	}

	END_TRY_SQL_LOG_ONLY(m_conn);

	if (ret) {
		// Notify GameStateManager if start location is not previously recorded (skip if start location already exists in GSM)
		ss->GetGameStateManager()->AddGameStateObjectStartLocation(ss->GetZoneId(), objId, x, y, z, rx, ry, rz, false, false);
	}

	return ret;
}

bool ScriptServerEngine::ObjectRotateCallback(
	void* vm,
	int objId,
	double time,
	double x, double y, double z,
	bool bufferCall) {
	ASSERT_VM_SS(false);

	// We need to calculate direction and slide vectors from the world space rotations in x,y,z, because
	// it's those that are used internally.  Angles come from the UI in degrees, so convert to radians.
	Matrix44f rotMat, tmp;
	ConvertRotationX(ToRadians(x), out(rotMat));
	ConvertRotationY(ToRadians(y), out(tmp));
	rotMat = tmp * rotMat;
	ConvertRotationZ(ToRadians(z), out(tmp));
	rotMat = tmp * rotMat;

	Vector3f ax(1.f, 0, 0), az(0, 0, 1.f);
	Vector4f slide = TransformPointToHomogeneousVector(rotMat, ax);
	Vector4f dir = TransformPointToHomogeneousVector(rotMat, az);

	ObjectRotateHelper(ss, objId, time, dir.x, dir.y, dir.z, slide.x, slide.y, slide.z, bufferCall);
	return true;
}

bool ScriptServerEngine::ObjectScaleCallback(
	void* vm,
	int objId,
	double x, double y, double z,
	bool bufferCall) {
	ASSERT_VM_SS(false);

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectScale, 0);
	*sce->OutBuffer() << objId << ss->GetZoneId().toLong() << ss->GetZoneId().GetInstanceId() << x << y << z;

	if (bufferCall) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objId);
	}

	return SendToClient(sce);
}

bool ScriptServerEngine::ObjectMoveCallback(
	void* vm,
	int objId,
	double timeMs,
	double accDur, double constVelDur, double accDistFrac, double constVelDistFrac,
	double x, double y, double z,
	double dx, double dy, double dz,
	double ux, double uy, double uz,
	bool bufferCall) {
	ASSERT_VM_SS(false);

	// Convert up to a slide vector, because that's what's used all over the engine.
	if (_isnan(x) || _isnan(y) || _isnan(z)) {
		LogError("NaN for position - vm " << IntToHexStr((int)vm) << " placementId=" << ss->GetPlacementId());
		return false;
	}

	// Send ObjectMove Event To Client
	ScriptClientEvent* sce = ScriptClientEvent::EncodeObjectMove(NULL, (ULONG)objId, ss->GetZoneId().toLong(), ss->GetZoneId().GetInstanceId(), x, y, z, timeMs, accDur, constVelDur, accDistFrac, constVelDistFrac);

	if (bufferCall) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, (ULONG)objId);
	}

	SendToClient(sce);

	// Significant Direction Vector Indicates Rotate During Move
	// DRF - Bug Fix - Float Equality Is Touchy
	const double ZeroToleranceSquared = 1e-3;
	Vector3d DV(dx, dy, dz);
	if (DV.LengthSquared() >= ZeroToleranceSquared) {
		// DRF - Use Default Up Vector ?
		Vector3d UV(ux, uy, uz);
		if (UV.LengthSquared() < ZeroToleranceSquared)
			UV.y = 1.0;

		// Compute Slide Vector (SV = UV X DV)
		Vector3d SV = UV.Cross(DV);

		// Send ObjectRotate Event To Client
		ObjectRotateHelper(ss, objId, timeMs, dx, dy, dz, SV.x, SV.y, SV.z, bufferCall);
	}

	return true;
}

ULONG ScriptServerEngine::ObjectGetIdCallback(void* vm) {
	ASSERT_VM_SS(0);
	return ss->GetPlacementId();
}

void ScriptServerEngine::ObjectSendMessageCallback(
	void* vm,
	int objId,
	const char* msg,
	unsigned delay) {
	ASSERT_VM_SS();

	if (delay > 0) {
		delay = std::max(delay, m_globalVMConfig.getMinDelayMillis());
	}

	if (objId == 0) {
		SendObjectMessageEventToZone(ss->GetPlacementId(), ss->GetZoneId(), ss->GetScriptZone(), msg, delay, false);
	} else {
		SendObjectMessageEventToScript(ss->GetPlacementId(), ss->GetZoneId(), objId, msg, delay, false, false);
	}
}

bool ScriptServerEngine::ObjectSetCollisionCallback(
	void* vm,
	int objectId,
	bool collisionValue,
	bool bufferCall) {
	ASSERT_VM_SS(false);

	int zoneId = ss->GetZoneId().toLong();
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetCollision, 0);
	*sce->OutBuffer() << zoneId << objectId << collisionValue;

	if (bufferCall) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objectId);
	}

	return SendToClient(sce);
}

bool ScriptServerEngine::ObjectSetDrawDistanceCallback(
	void* vm,
	int objId,
	double dist,
	bool bufferCall) {
	ASSERT_VM_SS(false);

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetDrawDistance, 0);
	*sce->OutBuffer() << objId << ss->GetZoneId().toLong() << ss->GetZoneId().GetInstanceId() << dist;

	if (bufferCall) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objId);
	}

	return SendToClient(sce);
}

bool ScriptServerEngine::PlayerTeleportCallback(
	void* vm,
	ULONG clientId,
	double x, double y, double z,
	double rx, double ry, double rz) {
	ASSERT_VM_SS(false);

	// Teleport to the location specified by the user.  The location, zone index and instance id
	// can be found by the user using the "/l" command in the chat window.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerTeleport, clientId);
	*sce->OutBuffer() << x << y << z << rx << ry << rz;

	return SendToClient(sce);
}

bool ScriptServerEngine::PlayerGotoUrlCallback(void* vm, ULONG clientId, const char* url) {
	ASSERT_VM_SS(false);

	// Send the player to the specified platform url (currently kaneva://) e.g. kaneva://<mygame>/zone1.arena.2
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerGotoUrl, 0);
	*sce->OutBuffer() << url << clientId;

	return SendToClient(sce);
}

void ScriptServerEngine::PlayerTellCallback(
	void* vm,
	ULONG clientId,
	const char* str,
	const char* objectTitle,
	bool isTell) {
	ASSERT_VM_SS();

	SendPlayerTellToClient(clientId, str, objectTitle, isTell, ss->GetPlacementId());
}

void ScriptServerEngine::SendPlayerTellToClient(ULONG clientId, const char* str, const char* objectTitle, bool isTell, int placementId) {
	int tell = isTell ? 1 : 0;
	ULONG recipient = clientId;
	if (!isTell)
		recipient = 0;

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerTell, recipient);
	(*sce->OutBuffer()) << str << objectTitle << placementId << tell << clientId;
	SendToClient(sce);
}

/*!
 * \brief
 * Sends an Script Client event to the specified user.  Called from playersendevent API.
 *
 * \param vm
 * The vm that called the playersendevent API.
 *
 * \param clientId
 * A handle that represents the destination user.
 *
 * \param data
 * Custom data provided by API caller to encode within the event.
 *
 * \returns
 * no return value
 *
 * The specified user will receive a Script Client event with the
 * data encoded.  The event is intended to be consumed by a custom menu.
 *
 */
bool ScriptServerEngine::PlayerSendEventCallback(
	void* vm,
	ULONG clientId,
	const std::vector<std::string>& data) {
	ASSERT_VM_SS(false);

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSendEvent, clientId);
	(*sce->OutBuffer()) << (int)data.size();
	for (const auto& str : data)
		(*sce->OutBuffer()) << str;

	return SendToClient(sce);
}

/*!
 * \brief
 * Sends an Script Client event to the specified user.  Called from playerloadmenu API.
 *
 * \param vm
 * The vm that called the playerloadmenu API.
 *
 * \param clientId
 * A handle that represents the destination user.
 *
 * \param menuName
 * Name of the menu to be instantiated within the client.
 *
 * \returns
 * no return value
 *
 * The specified user will receive a Script Client event name of the menu to instantiate.
 *
 */
bool ScriptServerEngine::PlayerLoadMenuCallback(void* vm, ULONG clientId, const char* menuName) {
	ASSERT_VM_SS(false);

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerLoadMenu, clientId);
	(*sce->OutBuffer()) << menuName;

	return SendToClient(sce);
}

/*!
* \brief
* Return the enabled state of the game framework for the current world
*
* \param vm
* The vm that called the gameGetFrameworkEnabled API.
*
* \returns
* framework enabled true or false
*/
bool ScriptServerEngine::GameGetFrameworkEnabledCallback(void* vm) {
	ASSERT_VM_SS(false);

	unsigned int zone_instance_id = ss->GetZoneId().GetInstanceId();
	unsigned int zone_type = ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);

	std::string value = "F";
	BEGIN_TRY_SQL();
	Query objQuery = m_conn->query();
	objQuery << " SELECT framework_enabled FROM wok.script_game_framework_settings "
			 << " WHERE zone_instance_id = %0 AND zone_type = %1 ";

	objQuery.parse();

	LogTrace(objQuery.str(zone_instance_id, zone_type));
	StoreQueryResult objQueryResult = objQuery.store(zone_instance_id, zone_type);

	if (objQueryResult.num_rows() > 0) {
		value = objQueryResult.at(0).at(0);
	}
	END_TRY_SQL_LOG_ONLY(m_conn);

	return (value == "T");
}

/*!
* \brief
* Set the enabled state of the game framework for the current world
*
* \param vm
* The vm that called the gameSetFrameworkEnabled API.
*
* \param enable
* Flag to enable or disable game framework
*
* \returns
* success true or false
*/
bool ScriptServerEngine::GameSetFrameworkEnabledCallback(void* vm, bool enabled) {
	ASSERT_VM_SS(false);

	unsigned int zone_instance_id = ss->GetZoneId().GetInstanceId();
	unsigned int zone_type = ss->GetZoneId().GetType();

	bool success = false;
	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);
	Query objQuery = m_conn->query();
	if (enabled) {
		objQuery << " INSERT INTO wok.script_game_framework_settings(zone_instance_id, zone_type, framework_enabled) "
				 << " VALUES(%0, %1, 'T') "
				 << " ON DUPLICATE KEY UPDATE framework_enabled = 'T' ";
	} else {
		objQuery << " DELETE FROM wok.script_game_framework_settings "
				 << " WHERE zone_instance_id = %0 AND zone_type = %1 ";
	}

	objQuery.parse();
	objQuery.execute(zone_instance_id, zone_type);
	objQuery.reset();
	trans.commit();

	success = true;
	END_TRY_SQL_LOG_ONLY(m_conn);

	return success;
}

/*!
* \brief
* Query item by global ID and return metadata in a table
*
* \param globalId
* Item global ID
*
* \returns
* { globalId = globalId, useType = useType }
* More to be added
*/
ScriptAPIRetType::BLOC ScriptServerEngine::GameGetItemInfo(lua_State* vm, GLID glid) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::GetItemInfo, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << ss->GetPlacementId() << glid; // So the game server knows where to send the reply.

	IEvent* eventToQueue = sce;
	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		eventToQueue = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
	}

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::ItemInfo, "gameGetItemInfo", eventToQueue);
}

TimeMs ScriptServerEngine::GetEffectDuration(lua_State* vm, int globalId) {
	ASSERT_VM_SS(0);
	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	return IServerEngine::GetInstance()->GetEffectDurationMs(m_conn, globalId);
}

void ScriptServerEngine::SetEffectCompletionTimer(ServerScriptPtr& ss, int objectPID, EffectCode type, TimeMs duration, GLID effectGLID) {
	ScriptServerEvent* pEvent = new ScriptServerEvent(ScriptServerEvent::EffectCompletion, ss->GetPlacementId(), ss->GetZoneId().toLong(), ss->GetZoneId().GetInstanceId());
	*pEvent->OutBuffer() << objectPID << static_cast<int>(type) << duration << effectGLID;
	pEvent->AddRef();
	m_myDispatcher->QueueEventInFutureMs(pEvent, duration);

	ScriptServerEvent* pOldEvent = ss->GetScriptZone()->setEffectCompletionEvent(objectPID, EffectCode::EFX_ANIMATION, pEvent);
	if (pOldEvent != nullptr) {
		m_myDispatcher->CancelEvent(pOldEvent);
		pOldEvent->Release();
	}
}

void ScriptServerEngine::CancelEffectCompletionTimer(ServerScriptPtr& ss, int objectPID, EffectCode type) {
	ScriptServerEvent* pOldEvent = ss->GetScriptZone()->setEffectCompletionEvent(objectPID, EffectCode::EFX_ANIMATION, nullptr);
	if (pOldEvent != nullptr) {
		m_myDispatcher->CancelEvent(pOldEvent);
		pOldEvent->Release();
	}
}

/*!
 * \brief
 * Returns the frequency of the ScriptServerEvent::Timer event in milliseconds.
 *
 * \param vm
 * The vm that called the API.
 *
 * \returns
 * unsigned long - ScriptServerEvent::Timer event in milliseconds
 *
 * The developer calls this from scripting to determine the frequency in milliseconds of the ScriptServerEvent::Timer event.
 *
 */
unsigned long ScriptServerEngine::ScriptGetTimerFrequencyCallback(void* vm) {
	ASSERT_VM_SS(0);
	return m_globalVMConfig.getTimerPeriod();
}

/*!
 * \brief Returns the zone id of the zone in which the current script is running.
 * \returns the zone id
 * \see gameGetCurrentZone, gameGetZoneInfo
 */
int ScriptServerEngine::ScriptGetCurrentZoneCallback(void* vm) {
	ASSERT_VM_SS(0);
	return ss->GetZoneId().toLong();
}

/*!
 * \brief
 * Sends an event to python script (GameManager.py) to get all zone info. Called from gamegetzonesinfo API
 *
 * \param vm
 * The vm that called the gamegetzonesinfo API.
 *
 * \returns
 * no return value
 *
 * The developer calls this from scripting to get info about all the currently running zones for the game
 *
 */
ScriptAPIRetType::BLOC ScriptServerEngine::ScriptGetZonesInfoCallback(void* vm) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::GameZonesInfo, 0);
	*sce->OutBuffer() << ss->GetPlacementId() << ss->GetZoneId().toLong(); // So the game server knows where to send the reply.

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::GameZonesInfo, "gameGetZonesInfo", sce);
}

/*!
 * \brief
 * Sends an event to python script (GameManager.py) to get instance info. Called from gamegetinstanceinfo API
 *
 * \param vm
 * The vm that called the gamegetinstancesinfo API.
 *
 * \param instanceId
 * The instance that we want information about.
 *
 * \returns
 * no return value
 *
 * The developer calls this from scripting to get info about a currently running instance
 *
 */
ScriptAPIRetType::BLOC ScriptServerEngine::ScriptGetInstanceInfoCallback(void* vm, int instanceId) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::GameInstanceInfo, 0);
	*sce->OutBuffer() << ss->GetPlacementId() << ss->GetZoneId().toLong() << instanceId; // So the game server knows where to send the reply.

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::GameInstanceInfo, "gameGetInstanceInfo", sce);
}

/*!
 * \brief
 * Sends an event to python script (GameManager.py) to create a new instance and join the player to an instance. Called from gamecreateinstance API
 *
 * \param vm
 * The vm that called the gamecreateinstance API.
 *
 * \returns
 * no return value
 *
 * The developer calls this from scripting to get create a new instance of this zoneId
 *
 */
ScriptAPIRetType::BLOC ScriptServerEngine::ScriptCreateInstanceCallback(void* vm, ULONG zoneId, ULONG clientId) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::CreateInstance, 0);
	*sce->OutBuffer() << ss->GetPlacementId() << ss->GetZoneId().toLong() << zoneId << clientId; // So the game server knows where to send the reply.
	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::CreateInstance, "gameCreateInstance", sce);
}

/*!
 * \brief
 * Sends an event to python script (GameManager.py) to lock/unlock an instance for players joining. Called from gamelockinstance API
 *
 * \param vm
 * The vm that called the gamelockinstance API.
 *
 * \returns
 * no return value
 *
 * The developer calls this from scripting to get set the lock of an instance
 *
 */
ScriptAPIRetType::BLOC ScriptServerEngine::ScriptLockInstanceCallback(void* vm, int instanceId, bool lock) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::LockInstance, 0);
	*sce->OutBuffer() << ss->GetPlacementId() << ss->GetZoneId().toLong() << instanceId << (int)lock; // So the game server knows where to send the reply.

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::LockInstance, "gameLockInstance", sce);
}

void ScriptServerEngine::ScriptErrorCallback(void* vm, int errorCode, const char* str, bool setZombie, int stackLevel) {
	ASSERT_VM_SS();
	ScriptErrorCallback(ss, errorCode, str, setZombie, stackLevel);
}

void ScriptServerEngine::ScriptErrorCallback(const ServerScriptPtr& ss, int errorCode, const char* str, bool setZombie, int stackLevel) {
	std::ostringstream errorStr;
	std::string errorMsg(str ? str : "NULL");
	std::string prefix = "[";
	prefix.append(ss->GetFileName());
	prefix.append("] ");

	if (errorCode == ServerScript::RUNTIME_ERROR || errorCode == ServerScript::LOAD_ERROR || errorCode == ServerScript::CALL_ERROR) {
		// Error message from lua should already include enough stack information

		// Parse error message to remove server path information
		// Check if it begins with stack.short_src (...) or stack.source (\\\\ or drive letter)
		if (errorMsg.length() > 2 && // minimal length check to cover edge cases
			(STLBeginWith(errorMsg, "...") || STLBeginWith(errorMsg, "\\\\") || toupper(errorMsg[0]) >= 'A' && toupper(errorMsg[0]) <= 'Z' && errorMsg[2] == ':')) {
			// Find end of path.
			size_t pos = errorMsg.find(":", 3); // Currently lua formats source information as <source>:<lineNumber>:
			if (pos == std::string::npos) {
				std::string tmp(errorMsg);
				STLToLower(tmp);
				pos = tmp.find(".lua"); // Cover some additional possibilities since we don't control how lua constructs error messages
			}

			// Strip path information
			if (pos != std::string::npos) {
				size_t slashPos = errorMsg.rfind('\\');
				if (slashPos == std::string::npos) {
					slashPos = errorMsg.rfind('/');
				}
				if (slashPos != std::string::npos) {
					errorMsg = errorMsg.substr(slashPos + 1);
				}
			}
		}

		// Prefix with script name only
		errorStr << prefix << errorMsg;
	} else {
		// For API errors, we need to retrieve source information from call stack.
		int lineNumber = -1;
		std::string source;

		ss->LuaGetSourceInfo(stackLevel, source, lineNumber);

		// Formulate error string
		if (lineNumber != -1 && !source.empty()) {
			errorStr << prefix << source << ":" << lineNumber << ": " << errorMsg;
		} else {
			// Unable to retrieve current stack
			errorStr << prefix << "[<NO-SOURCE-INFO>] " << errorMsg;
		}
	}

	// Set the error message but do not set to zombie state on Bad Arguments
	ss->SetError(errorCode, errorStr.str().c_str(), setZombie);

	// Need to grab the zone index from the clientNetId map
	NETIDZoneIndexMap::iterator itr = m_playerZones.begin();

	while (itr != m_playerZones.end()) {
		ZoneIndex zi = itr->second;

		if (ss->GetZoneId().toLong() == zi.toLong()) {
			SendErrorToClient(ss, itr->first);
		}
		++itr;
	}
	LogError(errorStr.str().c_str());
}

bool ScriptServerEngine::ScriptLogMessageCallback(void* /*vm*/, const char* str) {
	LOG4CPLUS_INFO(script_logger, str);
	return true;
}

bool ScriptServerEngine::ScriptDoFileCallback(lua_State* vm, const char* filename, std::string& errMsg) {
	ASSERT_VM_SS(false);

	bool ret = false;
	std::string fullPath;

	//clean up the name to avoid directory switches
	const char* pSlash = std::max(strrchr(filename, '/'), strrchr(filename, '\\'));
	if (pSlash) {
		filename = pSlash + 1;
	}

	// Search in current and framework directories
	int found = GetScriptFullPathForRead(PathAdd(FRAMEWORKINCLUDEDIR, filename), fullPath) ||
				GetScriptFullPathForRead(PathAdd(FRAMEWORKBEHAVIORDIR, filename), fullPath) ||
				GetScriptFullPathForRead(PathAdd(FRAMEWORKCLASSDIR, filename), fullPath) ||
				GetScriptFullPathForRead(PathAdd(FRAMEWORKTESTDIR, filename), fullPath) ||
				((ss->GetAssetType() == ScriptServerEvent::ActionItemParams || ss->GetAssetType() == ScriptServerEvent::ActionInstanceParams) &&
					GetScriptFullPathForRead(PathAdd(ACTIONSCRIPTDIR, filename), fullPath)) || // ActionItems|ActionInstances\*.json can include ActionItems\*.lua
				GetScriptFullPathForRead(PathAdd(SAVEDSCRIPTDIR, filename), fullPath);

	if (found) {
		// Save existing hook
		lua_Hook savedHook = lua_gethook(vm);
		int savedHookMask = lua_gethookmask(vm);
		int savedHookCount = lua_gethookcount(vm);

		// Disable hook
		lua_sethook(vm, NULL, 0, 0);

		if (!luaL_dofile(vm, fullPath.c_str()))
			ret = true;
		else {
			errMsg = lua_tostring(vm, -1);
			ScriptErrorCallback(vm, ServerScript::CALL_ERROR, errMsg.c_str(), true); // source information at stack level 1 (default)

			LogWarn("Error calling lua dofile for [" << fullPath << "]: " << errMsg);
		}

		// Restore lua hook
		if (savedHook != NULL) {
			lua_sethook(vm, savedHook, savedHookMask, savedHookCount);
		}
	} else {
		errMsg = "Error calling lua dofile for [";
		errMsg.append(filename);
		errMsg.append("]: file not found");
		ScriptErrorCallback(vm, ServerScript::LOAD_ERROR, errMsg.c_str(), true); // source information at stack level 1 (default)
	}

	return ret;
}

bool ScriptServerEngine::ObjectSetTextureCallback(void* vm, int objectId, const char* textureURL, bool bufferCall) {
	ASSERT_VM_SS(false);

	bool result = false;
	// The UpdateDynamicObjectTexture event requires a playerID. This routes there, but the player ID
	// is irrelevant for this call. We'll fake it. -- Mo/Jo
	int fakePlayerID = 0;
	if (textureURL) {
		// Parse the textureURL to get the asset ID
		std::string texURL(textureURL);
		size_t startPosition, endPosition;

		endPosition = texURL.rfind("/");
		if (endPosition != std::string::npos) {
			startPosition = texURL.rfind("/", endPosition - 1) + 1;
			if (startPosition != std::string::npos) {
				std::string assetString = texURL.substr(startPosition, endPosition - startPosition);
				int assetId = atoi(assetString.c_str());
				if (assetId != 0 && assetId != INT_MIN && assetId != INT_MAX) {
					// Send a ScriptClientEvent to the client.
					ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetTexture, 0);
					(*sce->OutBuffer()) << objectId << assetId << textureURL << fakePlayerID << ss->GetZoneId().toLong();

					if (bufferCall) {
						ScriptClientEvent* e = new ScriptClientEvent(sce);
						ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objectId);
					}

					SendToClient(sce);
					result = true;
				} else {
					LogWarn("Failed to find a valid asset ID in the texture URL");
				}
			} else {
				LogWarn("Failed to find the beginning / in the texture URL");
			}
		} else if (texURL.length() == 0) {
			// textureURL is "" so reset the texture of the object
			ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetTexture, 0);
			(*sce->OutBuffer()) << objectId << -1 << textureURL << fakePlayerID << ss->GetZoneId().toLong();

			if (bufferCall) {
				ScriptClientEvent* e = new ScriptClientEvent(sce);
				ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objectId);
			}

			SendToClient(sce);
			result = true;
		} else {
			LogWarn("Failed to find the ending / in the texture URL");
		}
	} else {
		LogWarn("Failed because texture URL was NULL");
	}

	return result;
}

bool ScriptServerEngine::ObjectGetDataCallback(void* /*vm*/, const GLID& glid, std::vector<std::string>& retValues) {
	bool success = false;
	StoreQueryResult results;

	// Object Data Already Cached ?
	ItemDataCache::iterator idc = m_itemDataCache.find(glid);
	if (idc != m_itemDataCache.end()) {
		retValues = (*idc).second;
		return true;
	}

	// Cache Object Data
	{
		std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
		BEGIN_TRY_SQL();

		std::string queryStr = "SELECT name, description, use_type FROM items WHERE global_id = %0";

		Query genQuery = m_conn->query(queryStr);
		genQuery.parse();

		LogDebug("ObjectGetDataCallback: " + genQuery.str());

		results = genQuery.store((int)glid);

		success = true;

		END_TRY_SQL_LOG_ONLY(m_conn);
	}

	if (success) {
		if (results.size() > 0) {
			Row row = results.at(0);
			for (int c = 0; c < (int)row.size(); c++) { //columns
				std::string col = std::string(row.at(c).c_str());
				retValues.push_back(col);
			}
			m_itemDataCache.insert(ItemDataCache::value_type(glid, retValues));
		} else {
			LogWarn("Item not found - glid<" << glid << ">");
			success = false;
		}
	}

	return success;
}

bool ScriptServerEngine::ObjectGetOwnerCallback(void* /*vm*/, ULONG pid, std::vector<std::string>& retValues) {
	bool success = false;
	StoreQueryResult results;

	// Object Owner Already Cached ?
	ItemOwnerCache::iterator idc = m_itemOwnerCache.find(pid);
	if (idc != m_itemOwnerCache.end()) {
		retValues = (*idc).second;
		return true;
	}

	// Cache Owner Data
	{
		std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
		BEGIN_TRY_SQL();

		Query objQuery = m_conn->query();
		objQuery << "SELECT u.wok_player_id, u.username "
				 << " from wok.dynamic_objects do "
				 << " inner join kaneva.users u on do.player_id = u.wok_player_id "
				 << " where do.obj_placement_id = %0";

		objQuery.parse();

		LogDebug("ObjectGetOwnerCallback: " + objQuery.str());

		results = objQuery.store((int)pid);

		success = true;

		END_TRY_SQL_LOG_ONLY(m_conn);
	}

	if (success) {
		if (results.size() > 0) {
			Row row = results.at(0);
			for (int c = 0; c < (int)row.size(); c++) { //columns
				std::string col = std::string(row.at(c).c_str());
				retValues.push_back(col);
			}
			m_itemOwnerCache.insert(ItemOwnerCache::value_type(pid, retValues));
		} else {
			LogWarn("Item not found - pid<" << pid << ">");
			success = false;
		}
	}

	return success;
}

bool ScriptServerEngine::ObjectSetTexturePanningCallback(
	void* vm,
	int objId,
	int uvSetId,
	int meshId,
	double uIncr, double vIncr) {
	ASSERT_VM_SS(false);

	// Send a ScriptClientEvent to the client.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetTexturePanning, 0);
	(*sce->OutBuffer()) << objId << meshId << uvSetId << (float)uIncr << (float)vIncr << ss->GetZoneId().toLong();

	if (true) { //replace with bufferCall parameter if needed
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objId);
	}

	return SendToClient(sce);
}

void ScriptServerEngine::ObjectSetAnimationCallback(
	lua_State* vm,
	int objectId,
	GLID animGlidSpecial, // <0 = reverse animation
	eAnimLoopCtl animLoopCtl,
	bool elapseCallback,
	bool bufferCall) {
	ASSERT_VM_SS();

	GLID animGlid = abs(animGlidSpecial);
	TimeMs animDurationMs = GetEffectDuration(vm, animGlid);

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectControl, 0);
	(*sce->OutBuffer())
		<< objectId
		<< ss->GetZoneId().toLong()
		<< ss->GetZoneId().GetInstanceId()
		<< ANIM_START
		<< 3 // numArgs
		<< animGlidSpecial // <0 = reverse animation
		<< (int)animLoopCtl // drf - added
		<< animDurationMs;

	if (bufferCall) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objectId, ANIM_START);
	}

	if (elapseCallback) {
		// Set a timer to trigger completion call (cancels previous pending animation completion event on this object if any)
		SetEffectCompletionTimer(ss, objectId, EffectCode::EFX_ANIMATION, animDurationMs, animGlid);
	} else {
		// Cancels previous pending animation completion event on this object if any
		CancelEffectCompletionTimer(ss, objectId, EffectCode::EFX_ANIMATION);
	}

	if (IS_VALID_GLID(animGlid) && IServerEngine::GetInstance()->checkGlid(animGlid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(animGlid);
		IEvent* libe = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
		SendToClient(libe);
	} else {
		SendToClient(sce);
	}
}

void ScriptServerEngine::ObjectSetScriptCallback(
	void* vm,
	int objectId,
	const char* directory,
	const char* filename,
	const char* arg) {
	ASSERT_VM_SS();

	if (!ss->IsStopping()) {
		std::string fname(PathAdd(directory, filename));
		std::string sourceCode;
		if (LoadScriptFile(fname.c_str(), sourceCode) &&
			LoadScript(fname.c_str(), sourceCode, arg, ss->GetPlacementId(), objectId, ss->GetZoneId(), 0, false, 0)) {
			LogDebug(ss->LogSubject() << "Loaded script ok: " << filename);
		} else {
			LogWarn(ss->LogSubject() << "Failed to load script: " << filename);
		}

		// If current script got shutdown event during LoadScript call, also shutdown the newly spawned script
		if (ss->IsStopping()) {
			ServerScriptPtr pSS_new = ss->GetScriptZone()->getScript(objectId, false);
			if (pSS_new)
				SendScriptShutdownEvent(pSS_new, 0, "last player departed");
		}
	} else {
		// Current script is being stopped
		LogInfo(ss->LogSubject() << "objectSetScript rejected because current script is stopping");
	}
}

void ScriptServerEngine::ObjectControlCallback(
	void* vm,
	int objectId,
	unsigned long playerId,
	ULONG ctlId,
	const std::vector<int>* pCtlArgs,
	bool bufferCall) {
	ASSERT_VM_SS();

	// Pass object control command to all clients in the same zone
	// Send a ScriptClientEvent to ServerEngine.
	ScriptClientEvent* sce;
	if (playerId == 0) {
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectControl, 0);
	} else {
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectControl, playerId, ScriptClientEvent::Scope_Player);
	}
	(*sce->OutBuffer()) << objectId << ss->GetZoneId().toLong() << ss->GetZoneId().GetInstanceId() << ctlId;
	if (pCtlArgs) {
		(*sce->OutBuffer()) << (int)pCtlArgs->size();
		for (auto it = pCtlArgs->cbegin(); it != pCtlArgs->cend(); ++it) {
			(*sce->OutBuffer()) << *it;
		}
	} else {
		(*sce->OutBuffer()) << (int)0;
	}

	if (bufferCall && playerId == 0) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objectId, ctlId);
	}

	SendToClient(sce);
}

void ScriptServerEngine::ObjectSetParticleCallback(
	void* vm,
	ULONG clientId,
	int objId,
	GLID glid,
	const std::string& boneName,
	int particleSlotId,
	double ofsX, double ofsY, double ofsZ,
	double dirX, double dirY, double dirZ,
	double upX, double upY, double upZ,
	bool bufferCall) {
	ASSERT_VM_SS();

	ScriptClientEvent* sce = ScriptClientEvent::EncodeObjectSetParticle(
		NULL,
		clientId,
		objId,
		ss->GetZoneId().toLong(),
		ss->GetZoneId().GetInstanceId(),
		glid,
		boneName,
		particleSlotId,
		ofsX, ofsY, ofsZ,
		dirX, dirY, dirZ,
		upX, upY, upZ);

	if (clientId == 0) { // clientId of 0 is ok, because this will be processed by the game server.
		// objectSetParticle API
		sce->SetScope(ScriptClientEvent::Scope_Zone);
	} else {
		// objectSetParticleForPlayer API
		sce->SetScope(ScriptClientEvent::Scope_Player);
	}

	sce->SetRoutingOption(ScriptClientEvent::Routing_ServerEngine); // This event will be reinterpreted by server engine

	if (clientId == 0) {
		if (bufferCall) {
			ScriptClientEvent* e = new ScriptClientEvent(sce);
			ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objId, (ULONG)particleSlotId);
		}
	} else {
		// Do not buffer "...ForPlayer" API calls.
		// Instead, append clientId to the end of the buffer. ServerEngine will pick it up when it's being forwarded to client.
		*sce->OutBuffer() << clientId;
	}

	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		IEvent* libe = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
		SendToClient(libe);
	} else {
		SendToClient(sce);
	}
}

void ScriptServerEngine::ObjectRemoveParticleCallback(
	void* vm,
	ULONG clientId,
	int objId,
	const std::string& boneName,
	int particleSlotId) {
	ASSERT_VM_SS();

	ScriptClientEvent* sce = ScriptClientEvent::EncodeObjectRemoveParticle(NULL, clientId, objId, ss->GetZoneId().toLong(), ss->GetZoneId().GetInstanceId(), boneName, particleSlotId);
	if (clientId == 0) { // clientId of 0 is ok, because this will be processed by the game server.
		// objectRemoveParticle API
		sce->SetScope(ScriptClientEvent::Scope_Zone);
	} else {
		// objectRemoveParticleForPlayer API
		sce->SetScope(ScriptClientEvent::Scope_Player);
	}

	sce->SetRoutingOption(ScriptClientEvent::Routing_ServerEngine); // This event will be reinterpreted by server engine

	if (clientId == 0) {
		// It's the previous SetParticle we want to remove, not the previous RemoveParticle, that isn't cached anyway.
		ss->GetGameStateManager()->RemoveGameStateObjectModifier(ss->GetZoneId(), objId, ScriptClientEvent::ObjectSetParticle, (ULONG)particleSlotId);
	} else {
		// Do not buffer "...ForPlayer" API calls.
		// Instead, append clientId to the end of the buffer. ServerEngine will pick it up when it's forwarded to client.
		*sce->OutBuffer() << clientId;
	}

	SendToClient(sce);
}

void ScriptServerEngine::ObjectAddLabelsCallback(
	void* vm,
	ULONG clientId,
	ULONG objId,
	const std::vector<ObjectLabelInfo>& labels,
	bool bufferCall) {
	ASSERT_VM_SS();

	ScriptClientEvent* sce;
	if (clientId == 0) { // clientId of 0 is ok, because this will be processed by the game server.
		// objectAddLabels API
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectAddLabels, 0);
	} else {
		// objectAddLabelsForPlayer API
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectAddLabels, clientId, ScriptClientEvent::Scope_Player);
	}

	sce->SetRoutingOption(ScriptClientEvent::Routing_ServerEngine); // This event will be reinterpreted by server engine

	int numberOfLabels = labels.size();
	*sce->OutBuffer() << ss->GetZoneId().toLong() << objId << numberOfLabels;
	for (int i = 0; i < numberOfLabels; ++i) {
		*sce->OutBuffer() << labels[i].m_label << labels[i].m_size << labels[i].m_red << labels[i].m_green << labels[i].m_blue;
	}

	if (clientId == 0) {
		if (bufferCall) {
			ScriptClientEvent* e = new ScriptClientEvent(sce);
			ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objId);
		}
	} else {
		// Do not buffer "...ForPlayer" API calls.
		// Instead, append clientId to the end of the buffer. ServerEngine will pick it up when it's forwarded to client.
		*sce->OutBuffer() << clientId;
	}

	SendToClient(sce);
}

// DRF - Added
void ScriptServerEngine::SoundAddModifierCallback(
	void* vm,
	int soundPlacementId,
	int input,
	int output,
	double gain,
	double offset) {
	ASSERT_VM_SS();
	ZoneIndex zoneId = ss->GetZoneId();
	auto sce = ScriptClientEvent::EncodeSoundAddModifier(NULL, zoneId, soundPlacementId, input, output, gain, offset);
	//	ScriptClientEvent* e = new ScriptClientEvent( sce );
	//	ss->GetGameStateManager()->AddGameStateObjectModifier(zoneId, e, soundPlacementId);
	SendToClient(sce);
}

// DRF - Added
void ScriptServerEngine::ObjectSetMediaCallback(
	void* vm,
	ULONG objectId,
	const MediaParams& mediaParams) {
	ASSERT_VM_SS();

	ZoneIndex zoneId = ss->GetZoneId();

	auto sce = ScriptClientEvent::EncodeObjectSetMedia(NULL, zoneId, objectId, mediaParams);

	SendToClient(sce);
}

// DRF - Added
void ScriptServerEngine::ObjectSetMediaVolumeAndRadiusCallback(
	void* vm,
	ULONG objectId,
	const MediaParams& mediaParams) {
	ASSERT_VM_SS();

	ZoneIndex zoneId = ss->GetZoneId();

	auto sce = ScriptClientEvent::EncodeObjectSetMediaVolumeAndRadius(NULL, zoneId, objectId, mediaParams);

	SendToClient(sce);
}

void ScriptServerEngine::ObjectSetOutlineCallback(
	void* vm,
	ULONG objectId,
	bool show,
	bool setColor,
	double red, double green, double blue,
	bool bufferCall) {
	ASSERT_VM_SS();

	// clientId of 0 is ok, because this will be processed by the game server.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetOutline, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << objectId << show << setColor;
	if (setColor)
		*sce->OutBuffer() << (float)red << (float)green << (float)blue;

	if (bufferCall) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objectId);
	}

	SendToClient(sce);
}

void ScriptServerEngine::ObjectClearLabelsCallback(
	void* vm,
	ULONG clientId,
	ULONG objId,
	bool bufferCall) {
	ASSERT_VM_SS();

	ScriptClientEvent* sce;
	if (clientId == 0) { // clientId of 0 is ok, because this will be processed by the game server.
		// objectClearLabels API
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectClearLabels, 0);
	} else {
		// objectClearLabelsForPlayer API
		sce = new ScriptClientEvent(ScriptClientEvent::ObjectClearLabels, clientId, ScriptClientEvent::Scope_Player);
	}

	sce->SetRoutingOption(ScriptClientEvent::Routing_ServerEngine); // This event will be reinterpreted by server engine

	*sce->OutBuffer() << ss->GetZoneId().toLong() << objId;

	if (clientId == 0) {
		if (bufferCall) {
			ScriptClientEvent* e = new ScriptClientEvent(sce);
			ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objId);
		}
	} else {
		// Do not buffer "...ForPlayer" API calls.
		// Instead, append clientId to the end of the buffer. ServerEngine will pick it up when it's forwarded to client.
		*sce->OutBuffer() << clientId;
	}

	SendToClient(sce);
}

bool ScriptServerEngine::LoadConfigValues(const IKEPConfig* cfg) {
	m_maxLoopTime = cfg->Get("MaxLoopTimeMs", 250);
	m_processEventTime = cfg->Get("MaxProcessEventTimeMs", 100);
	m_key = cfg->Get("RegistrationKey", "");

	m_taskSystemThreadCount = MIN(MAX(cfg->Get(DEFAULT_WORKER_THREADS_TAG, (int) TaskSystem::DEFAULT_WORKER_THREADS), 1), TaskSystem::WORKER_THREAD_LIMIT);
	m_maxScriptsPerZone = MIN(MAX(cfg->Get("MaxScriptsPerZone", DEFAULT_MAX_SCRIPTS_PER_ZONE), 0), 1000000);

	// Bit mask for all allowed asset types for uploading
	m_allowedUploadAssetTypes = cfg->Get("AllowedUploadAssetTypes", (int)DEFAULT_ALLOWED_UPLOAD_ASSET_TYPES);

	// If true: dictionary names will be prefixed with zone index
	m_isolateDictionariesByZone = cfg->Get("IsolateDictionariesByZone", false);

	// Reading ScriptServerEngine.xml
	// Determine if lua VMs are to be created with full library support
	// A value of 1 indicates that vms are created with a subset of the
	// available libraries, have no coroutine support and has a handful
	// of functions disabled.
	// A value of 0 loads all lua libraries, with coroutine support and
	// no disabled functions.
	// Defaults to protected mode.
	m_globalVMConfig.setLuaProtected(cfg->Get("LuaProtected", true));
	m_globalVMConfig.setStoreProcAccessEnabled(cfg->Get("StoredProcedureAccess", false));

	m_globalVMConfig.setLuaGCPause(MIN(MAX(cfg->Get("LuaGCPause", DEFAULT_LUA_GC_PAUSE), 0), 500));
	m_globalVMConfig.setLuaGCStep(MIN(MAX(cfg->Get("LuaGCStep", DEFAULT_LUA_GC_STEP), 100), 500));
	m_globalVMConfig.setYieldAfter(MIN(MAX(cfg->Get("YieldAfter", DEFAULT_YIELD_AFTER), 1), 10000));
	// max 20Mb, min 1Kb
	m_globalVMConfig.setScriptMemoryLimit(MIN(MAX(cfg->Get("ScriptMemoryLimit", DEFAULT_SCRIPT_MEMORY_LIMIT), 1024), 16777216 * 2));
	// Common library to be loaded for all VMs (through dofile)
	m_globalVMConfig.setLuaCommonLibrary(cfg->Get("LuaCommonLibrary", ""));

	// Lua thread options
	m_globalVMConfig.setUseCocoThread(cfg->Get("UseCocoThread", DEFAULT_USE_COCO));
	m_globalVMConfig.setUseConsumerLoop(cfg->Get("UseConsumerLoop", DEFAULT_USE_CONSUMER_LOOP));

	// For APIs that support delays (queued for future execution), this is the minimum delay in milliseconds to prevent a script from ticking too fast.
	m_globalVMConfig.setMinDelayMillis(cfg->Get("MinDelayMillis", DEFAULT_MIN_DELAY_MILLIS));

	// kgp_timer callback interval
	m_globalVMConfig.setTimerPeriod(MIN(MAX(cfg->Get("TimerPeriod", DEFAULT_TIMER_PERIOD), 100), 600000));

	// Whether to load system scripts defined in kgp_common.preload_scripts table. Default is false (do not load system scripts).
	m_globalVMConfig.setLoadSystemScripts(cfg->Get("LoadSystemScripts", false));

	// Whether to load system scripts in a synchronous manner. Default is true.
	m_globalVMConfig.setLoadSystemScriptsSynch(cfg->Get("LoadSystemScriptsSynch", true));

	// If a script is loaded synchronously, how long should we wait for kgp_start to complete
	m_globalVMConfig.setKgpStartWaitTimeout(cfg->Get("KGPStartWaitTimeout", DEFAULT_KGP_START_WAIT_TIMEOUT));

	// Script shutdown timeout
	m_globalVMConfig.setUserScriptShutdownTimeout(MIN(MAX(cfg->Get("UserScriptShutdownTimeout", DEFAULT_USER_SCRIPT_SHUTDOWN_TIMEOUT), 0), 60000));
	m_globalVMConfig.setSystemScriptShutdownTimeout(MIN(MAX(cfg->Get("SystemScriptShutdownTimeout", DEFAULT_SYSTEM_SCRIPT_SHUTDOWN_TIMEOUT), 0), 60000));

	// API timeout
	m_globalVMConfig.setBlockedScriptTimeout(MIN(MAX(cfg->Get("BlockedScriptTimeout", DEFAULT_BLOCKED_SCRIPT_TIMEOUT), 5000), 60000));
	m_globalVMConfig.setAPITimeoutIsFatal(cfg->Get("APITimeoutIsFatal", false));

	// Spin-down delay timer
	m_defaultSpinDownDelayMinutes = static_cast<unsigned>(cfg->Get("DefaultScriptZoneSpinDownDelayMinutes", DEFAULT_SCRIPT_ZONE_SPIN_DOWN_DELAY_MINUTES));

	LogSettings();

	return true;
}

void ScriptServerEngine::LogSettings() {
	LogInfo("ScriptServerEngine initialized using the following values:");
	LogInfo("   MaxLoopTimeMs:              " << FMT_TIME << m_maxLoopTime);
	LogInfo("   ProcessEventTimeMs:         " << FMT_TIME << m_processEventTime);
	LogInfo("   RegistrationKey:            " << m_key);
	LogInfo("   YieldAfter:                 " << m_globalVMConfig.getYieldAfter());
	LogInfo("   TimerPeriod:                " << m_globalVMConfig.getTimerPeriod());
	LogInfo("   LuaGCPause:                 " << m_globalVMConfig.getLuaGCPause());
	LogInfo("   LuaGCStep:                  " << m_globalVMConfig.getLuaGCStep());
	LogInfo("   TaskSystemThreadCount:      " << m_taskSystemThreadCount);
	LogInfo("   ScriptMemoryLimit:          " << m_globalVMConfig.getScriptMemoryLimit());
	LogInfo("   BlockedScriptTimeout:       " << m_globalVMConfig.getBlockedScriptTimeout());
	LogInfo("   BaseDir:                    " << PathBase());
	LogInfo("   ScriptDir:                  " << m_scriptDir);
	LogInfo("   Secured VM:                 " << (m_globalVMConfig.isLuaProtected() ? "True" : "False"));
	LogInfo("   Max Scripts per Zone:       " << m_maxScriptsPerZone);
	LogInfo("   SP Access:                  " << (m_globalVMConfig.isStoreProcAccessEnabled() ? "True" : "False"));
	LogInfo("   LuaCommonLibrary:           " << m_globalVMConfig.getLuaCommonLibrary());
	LogInfo("   MinDelayMillis:             " << m_globalVMConfig.getMinDelayMillis());
	LogInfo("   UserScriptShutdownTimeout:  " << m_globalVMConfig.getUserScriptShutdownTimeout());
	LogInfo("   SystemScriptShutdownTimeout:" << m_globalVMConfig.getSystemScriptShutdownTimeout());
	LogInfo("   LoadSystemScripts:          " << (m_globalVMConfig.getLoadSystemScripts() ? "True" : "False"));
	LogInfo("   LoadSystemScriptsSynch:     " << (m_globalVMConfig.getLoadSystemScriptsSynch() ? "True" : "False"));
	LogInfo("   KGPStartWaitTimeout:        " << m_globalVMConfig.getKgpStartWaitTimeout());
	LogInfo("   AllowedUploadAssetTypes:    0x" << std::hex << std::setfill('0') << std::setw(8) << m_allowedUploadAssetTypes);
	LogInfo("   IsolateDictionariesByZone:  " << (m_isolateDictionariesByZone ? "True" : "False"));
	LogInfo("   Use COCO Thread:            " << (m_globalVMConfig.useCocoThread() ? "True" : "False"));
	LogInfo("   Use Consumer Loop in Thread:" << (m_globalVMConfig.useConsumerLoop() ? "True" : "False"));
	LogInfo("   Script Zone Spin Down Delay:" << m_defaultSpinDownDelayMinutes << " minutes");

	if (m_globalVMConfig.useCocoThread()) {
#ifndef LUACOCO_VERSION
		LogWarn("LUA-COCO library not linked. COCO threads might not be available.");
#endif
		if (!m_globalVMConfig.useConsumerLoop()) {
			LogWarn("COCO thread is used: forcing UseConsumerLoop to TRUE");
			m_globalVMConfig.setUseConsumerLoop(true);
		}
	}
}

bool ScriptServerEngine::Initialize(const char* /*baseDir*/, const char* /*configFile*/, const DBConfig& dbConfig) {
	jsAssert(m_myDispatcher != NULL);

	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	bool ret = true;

	m_scriptDir = PathAdd(PathBase(), PathAdd(GAMEFILES, SCRIPTSERVERDIR));

	LoadDevToolsFolders();

	// Load configuration
	KEPConfigAdapter netcfg;
	netcfg.Load(PathAdd(PathBase(), NETCFG_CFG).c_str(), NETCFG_ROOT);
	std::string memcacheUrl;
	netcfg.GetValue(MEMCACHEAPI_TAG, memcacheUrl, NET_DEFAULT_MEMCACHAPI_URL);
	netcfg.GetValue(WORLDEXPERIMENT_TAG, m_worldExperimentUrl, NET_DEFAULT_WORLDEXPERIMENT_URL);

	// Read global.json
	std::string globalConfigJsonString;
	if (!LoadTextFile(PathAdd(PathBase(), GLOBAL_JSON), globalConfigJsonString)) {
		_LogInfo("global.json does not exist");
		// clang-format off
		globalConfigJsonString = R"({" global ":{}})";	 // Empty object does not seem to work with json11 parser
		// clang-format on
	}

	m_globalJson = new JsonConfig(globalConfigJsonString);
	if (m_globalJson->hasError()) {
		_LogError("global.json parsing failed: " << m_globalJson->getError());
	}

	// connect to the database.
	my_init(); // just in case not called by mysql++
	initializeDBConnection(PathBase().c_str(), dbConfig, memcacheUrl);

	// Read api_registrations table
	loadRestAPIRegistrations();

	// Register event handlers
	// This ScriptServerEvent handler must always have a lower priority than ScriptServerEventBlade::ScriptServerEventHandler
	m_myDispatcher->RegisterHandler(ScriptServerEventHandler, (ULONG)this, "ScriptServerEventHandler", ScriptServerEvent::ClassVersion(), ScriptServerEvent::ClassId(), EP_LOW);
	m_myDispatcher->RegisterHandler(UpdateScriptZoneSpinDownDelayHandler, (ULONG)this, "UpdateScriptZoneSpinDownDelayHandler", UpdateScriptZoneSpinDownDelayEvent::ClassVersion(), UpdateScriptZoneSpinDownDelayEvent::ClassId(), EP_NORMAL);

	// Start VM scheduler
	m_pScheduler->start(m_myDispatcher);

	// Start the background threads.  Using only 1 thread for now until we can determine the cause of the 3D App threading crashes
	m_taskSystem.Initialize(m_taskSystemThreadCount);

	// A separate task system to allow loadScriptTask to block on kgp_start. Experimental.
	// Might replace it with a worker thread instead. TBD.
	m_loadScriptTaskSystem.Initialize(1); // Use one (1) thread only.

	// Queue the timer event.
	IEvent* timerEvent = new ScriptServerEvent(ScriptServerEvent::Timer);
	m_myDispatcher->QueueEventInFutureMs(timerEvent, m_globalVMConfig.getTimerPeriod());
	return ret;
}

void ScriptServerEngine::initializeDBConnection(const char* gameDir, const DBConfig& cfg, const std::string& memcacheUrl) {
	// read the config file for the database settings
	std::string dbServer, dbUser, dbPassword, dbClearPassword, dbName, dbCommon;
	int dbPort = 0;
	bool dontShowDlg; // we don't care about this one

	if (!cfg.Get(dbServer, dbPort, dbName, dbUser, dbPassword, dontShowDlg)) {
		throw new KEPException(loadStrPrintf(IDS_DB_BAD_CONFIG, PathAdd(gameDir, DB_CFG_NAME).c_str()), E_INVALIDARG);
	}

	dbCommon = cfg.config().Get(DB_COMMON, "");
	m_conn->setMemcacheAPIUrl(memcacheUrl);
	std::string errMsg;
	try {
		m_conn->set_option(new MultiStatementsOption(true)); // need this for calling SPs
		m_conn->set_option(new ReconnectOption(true));
		m_conn->connect(dbName.c_str(), dbCommon.c_str(), dbServer.c_str(), dbUser.c_str(), dbPassword.c_str(), dbPort);
	} catch (const mysqlpp::Exception& er) {
		errMsg = er.what();
		DELETE_AND_ZERO(m_conn);
	}

	if (!m_conn) {
		throw new KEPException(loadStrPrintf(IDS_DB_CONNECT_ERROR, errMsg.c_str()), E_INVALIDARG);
	}

	LogInfo(loadStr(IDS_DB_CONNECT) << dbServer << ":" << dbPort << ", " << dbName << ", " << dbUser);
}

void ScriptServerEngine::FreeAll() {
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_taskSystem.Finalize();

	if (m_globalJson) {
		delete m_globalJson;
		m_globalJson = NULL;
	}

	if (!m_instance) {
		LogError("m_instance is NULL");
	}

	delete m_conn;
	m_conn = 0;

	{
		std::lock_guard<fast_recursive_mutex> lock(m_dictionaryMapSync);
		for (DictionaryMap::iterator it = m_dictionaryMap.begin(); it != m_dictionaryMap.end();) {
			delete it->second;
			it = m_dictionaryMap.erase(it);
		}
	}

	if (m_scriptAPIRegistry) {
		delete m_scriptAPIRegistry;
		m_scriptAPIRegistry = NULL;
	}

	if (m_isMyMessageQueueAgent) {
		delete m_messageQueueAgent;
		m_messageQueueAgent = NULL;
	}

	m_instance = 0;
}

// Temporary - to be removed after 16.7 is released
static ScriptServerEvent* ConvertLegacyObjectClickEvent(ScriptServerEvent* se) {
	if (se->GetEventType() != ScriptServerEvent::ObjectClick_Deprecated) {
		// No conversion necessary
		return se;
	}

	se->RewindInput();

	// Basic args
	ScriptServerEvent::EventType eventType;
	ULONG objPlacementId;
	NETID clientNetId;
	int zoneId;
	int instanceId;
	se->ExtractInfo(eventType, objPlacementId, zoneId, instanceId, clientNetId);

	// Decode additional args
	bool rightClick;
	KeyModifierTable keyModifiers;
	ScriptPos<float> clickPos;
	ScriptLegacyArray<std::tuple<int, int>> objects;

	*se->InBuffer() >> rightClick >> keyModifiers >> clickPos >> objects;

	eventType = rightClick ? ScriptServerEvent::ObjectRightClick : ScriptServerEvent::ObjectLeftClick;
	ScriptServerEvent* pEvent = new ScriptServerEvent(eventType, objPlacementId, zoneId, instanceId, clientNetId);
	*pEvent->OutBuffer() << keyModifiers << clickPos << objects;

	// Set same decoder offset for the new event
	pEvent->ExtractInfo(eventType, objPlacementId, zoneId, instanceId, clientNetId);
	return pEvent;
}

EVENT_PROC_RC ScriptServerEngine::ScriptServerEventHandler(IEvent* e) {
	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;
	jsVerifyReturn(e != nullptr, ret);

	ScriptServerEvent* se = static_cast<ScriptServerEvent*>(e);

	ScriptServerEvent::EventType eventType;
	ULONG objPlacementId;
	NETID clientNetId;
	int zoneId;
	int instanceId;
	unsigned playerArriveId; // For PlayerArrive and PlayerDepart
	try {
		se->ExtractInfo(eventType, objPlacementId, zoneId, instanceId, clientNetId);

		ZoneIndex zoneIndex(zoneId);
		if (zoneId != 0) {
			if (instanceId != 0) {
				zoneIndex.setInstanceId(instanceId);
			} else {
				assert(zoneIndex.isInstanced());
			}
		} else if (se->From() != 0) {
			// Some client-side events do not provide zone ID. Look it up with player NET ID.
			auto itr = m_playerZones.find(se->From());
			if (itr == m_playerZones.end()) {
				LogError("playerZones.find() FAILED - clientNetId = " << se->From());
				return EVENT_PROC_RC::NOT_PROCESSED;
			}
			zoneIndex = itr->second;
		}

		//
		// This is the first stop for an event.  This is where the events are assigned to
		// VM-local queues for background processing or the VM is configured and a task is
		// created to handle it as soon as possible.
		//

		//Check events that need to be processed by all scripts
		switch (eventType) {
			// =======================================
			// Broadcast script event: to be forwarded to scripts
			case ScriptServerEvent::PlayerArrived:
				LogDebug("PlayerArrived received, clientNetId = " << clientNetId);
				*se->InBuffer() >> playerArriveId;
				HandlePlayerArrive(se, clientNetId, zoneIndex, playerArriveId);
				ret = EVENT_PROC_RC::CONSUMED;
				break;
			case ScriptServerEvent::PlayerDeparted:
				LogDebug("PlayerDeparted received, clientNetId = " << clientNetId);
				*se->InBuffer() >> playerArriveId;
				HandlePlayerDepart(se, clientNetId, zoneIndex, playerArriveId);
				ret = EVENT_PROC_RC::CONSUMED;
				break;
			case ScriptServerEvent::MenuEvent: {
				NETIDZoneIndexMap::iterator itr = m_playerZones.find(clientNetId);
				if (itr != m_playerZones.end()) {
					ZoneIndex zi = itr->second;
					SendMenuEventToZone(se, zi, clientNetId);
				}
				ret = EVENT_PROC_RC::CONSUMED;
				break;
			}
			case ScriptServerEvent::Timer: {
				handleTimerEvent();

				//
				// Re-queue the event for m_timerPeriod seconds in the future
				// after processing the event.
				//
				m_myDispatcher->QueueEventInFutureMs(se, m_globalVMConfig.getTimerPeriod());
				ret = EVENT_PROC_RC::CONSUMED;
				break;
			}

			// ================================================
			// Other non-script events
			case ScriptServerEvent::GetAllScripts: {
				NETIDZoneIndexMap::iterator itr = m_playerZones.find(clientNetId);
				if (itr != m_playerZones.end()) {
					ZoneIndex zi = itr->second;
					SendScriptInformationToClient(zi, clientNetId);
				}
				break;
			}
			case ScriptServerEvent::UploadScript:
				if (CheckPlayerPermissions(clientNetId, ZP_MODERATOR, "UploadScript"))
					Upload3DAppAssetEventHandler(se, zoneIndex, clientNetId);
				return EVENT_PROC_RC::CONSUMED;

			case ScriptServerEvent::DeleteScript:
				if (CheckPlayerPermissions(clientNetId, ZP_MODERATOR, "DeleteScript"))
					DeleteScriptEventHandler(se);
				return EVENT_PROC_RC::CONSUMED;

			case ScriptServerEvent::DeleteAsset:
				if (CheckPlayerPermissions(clientNetId, ZP_MODERATOR, "DeleteAsset"))
					DeleteAssetEventHandler(se, clientNetId);
				return EVENT_PROC_RC::CONSUMED;

			case ScriptServerEvent::AttachScript:
				if (CheckPlayerPermissions(clientNetId, ZP_MODERATOR, "AttachScript"))
					AttachScriptEventHandler(se, clientNetId, objPlacementId);
				return EVENT_PROC_RC::CONSUMED;

			case ScriptServerEvent::DetachScript:
				if (CheckPlayerPermissions(clientNetId, ZP_MODERATOR, "DetachScript"))
					DetachScriptEventHandler(se, clientNetId, objPlacementId);
				return EVENT_PROC_RC::CONSUMED;

			case ScriptServerEvent::DownloadAsset:
				if (CheckPlayerPermissions(clientNetId, ZP_MODERATOR, "DetachScript"))
					DownloadScriptEventHandler(se, clientNetId);
				return EVENT_PROC_RC::CONSUMED;

			case ScriptServerEvent::GetServerLog:
				if (CheckPlayerPermissions(clientNetId, ZP_MODERATOR, "GetServerLog"))
					GetServerLogEventHandler(se, clientNetId);
				return EVENT_PROC_RC::CONSUMED;

			case ScriptServerEvent::ProcessUGCUpload: {
				int uploadId, assetType;
				std::string name;
				(*se->InBuffer()) >> uploadId >> name >> assetType;
				return EVENT_PROC_RC::CONSUMED;
			}

				///////////////////////////////////////////////////////////////////
				// Events Using Game State Manager
				///////////////////////////////////////////////////////////////////

			case ScriptServerEvent::MovePlacementObject: {
				const auto& gsm = getGameStateManager(zoneIndex);
				if (!gsm || !gsm->ObjectExists(zoneIndex, objPlacementId))
					return EVENT_PROC_RC::CONSUMED;
				float x, y, z;
				float dx, dy, dz;
				float sx, sy, sz;
				DWORD time;
				*se->InBuffer() >> x >> y >> z >> dx >> dy >> dz >> sx >> sy >> sz >> time;
				if (_isnan(x) || _isnan(y) || _isnan(z)) {
					LogError("MovePlacementObject NaN for position");
				} else {
					ScriptClientEvent* pSCE = new ScriptClientEvent(ScriptClientEvent::ObjectMoveAndRotate, 0);
					*pSCE->OutBuffer() << objPlacementId << (int)zoneIndex << instanceId << x << y << z << dx << dy << dz << sx << sy << sz << time;
					gsm->AddGameStateObjectModifier(zoneIndex, pSCE, objPlacementId);
				}
				return EVENT_PROC_RC::CONSUMED;
			}

			case ScriptServerEvent::RemovePlacementObject: {
				const auto& gsm = getGameStateManager(zoneIndex);
				if (gsm)
					gsm->RemoveGameStateObject(zoneIndex, objPlacementId);
				m_taskSystem.QueueTask(new DetachScriptTask(m_conn, m_dbAccessCS, objPlacementId));

				ServerScriptPtr pScript = m_zoneMap.getScriptByID(objPlacementId, zoneIndex, false);
				if (pScript) {
					SendScriptShutdownEvent(pScript, clientNetId, "object removed manually");
				}
				return EVENT_PROC_RC::CONSUMED;
			}

			case ScriptServerEvent::AddPlacementObject: {
				const auto& gsm = getGameStateManager(zoneIndex);
				if (gsm)
					gsm->AppendPermanentObject(zoneIndex, objPlacementId);
				return EVENT_PROC_RC::CONSUMED;
			}

			case ScriptServerEvent::TransmitGameState: {
				const auto& gsm = getGameStateManager(zoneIndex);
				if (gsm)
					TransmitGameStateHandler(gsm, zoneIndex, clientNetId);
				return EVENT_PROC_RC::CONSUMED;
			}

			case ScriptServerEvent::ObjectClick_Deprecated:
				se = ConvertLegacyObjectClickEvent(se);
				eventType = se->GetEventType();
				// fall-through
			default:
				if (ServerScript::IsScriptEvent(eventType)) {
					assert(objPlacementId != 0);

					// Look for script by zoneIndex and PID. Currently we are sending client events (collide, trigger, etc)
					// to server even if the target object is not a game item. pScript will null if the requested PID is not
					// managed by the scripts. Ignore such events for now.
					ServerScriptPtr pScript = m_zoneMap.getScriptByID(objPlacementId, zoneIndex, true);
					if (pScript) {
						m_pScheduler->sendEventToScript(pScript, se);
						return EVENT_PROC_RC::CONSUMED;
					}
				} else {
					// Should not be here. Script events should have TEventHandler defined in ServerScript class.
					// If it's a expected non-script event, we should have it handled under a separate case above.
					assert(false);
				}
				break;
		}
	} catch (KEPException* ex) {
		LogWarn("Exception processing ScriptServerEvent::EventType " << (ULONG)eventType << ", clientNetId = " << clientNetId << ": " << ex->ToString());
		ex->Delete();
	}
	return ret;
}

EVENT_PROC_RC ScriptServerEngine::ScriptServerEventHandler(ULONG lparam, IDispatcher* /*disp*/, IEvent* e) {
	ScriptServerEngine* me = (ScriptServerEngine*)lparam;
	EVENT_PROC_RC ret = EVENT_PROC_RC::NOT_PROCESSED;
	jsVerifyReturn(lparam != 0, ret);

	std::lock_guard<fast_recursive_mutex> lock(me->m_eventHandlerCS);
	return me->ScriptServerEventHandler(e);
}

void ScriptServerEngine::AddPlayerToZone(NETID clientNetId, ZoneIndex zi, NETID prevNetID, ZoneIndex prevZi, unsigned playerArriveId, const ScriptPlayerInfo& playerInfo, unsigned parentZoneInstanceId) {
	LogInfo("<" << playerInfo.m_name << ":" << clientNetId << "> : " << zi.ToStr() << " ID[" << playerArriveId << "]");

	// Handle the removal of the player from the previous zone
	if (prevZi.toLong() != 0 && prevZi != INVALID_ZONE_INDEX) {
		RemovePlayerFromZone(prevNetID, prevZi, zi, playerInfo, false);
	}

	// Update the player ids, names, and zones
	m_playerIds.insert(NamePlayerIdMap::value_type(playerInfo.m_name, playerInfo.m_playerId));
	NETIDPlayerInfoMap::iterator itPlayer = m_players.find(clientNetId);
	if (itPlayer != m_players.end()) {
		// Update permission if player record already exists
		itPlayer->second.m_permissions = playerInfo.m_permissions;
	} else {
		m_players.insert(NETIDPlayerInfoMap::value_type(clientNetId, playerInfo));
	}
	m_playerZones.insert(NETIDZoneIndexMap::value_type(clientNetId, zi));

	// If they have permissions to upload, delete etc, then send them log messages too.
	if (CheckPlayerPermissions(clientNetId, ZP_MODERATOR, "AddPlayerToZone", true)) { // do not log error if permission denied as this is a optional check

		// should be at .../bins/ServerLog.cfg
		std::string pathLogCfg = PathFind(PathApp(), SERVER_LOG_PROP_FILE, false);
		LogDebug("pathLogCfg=" << pathLogCfg);

		log4cplus::helpers::Properties prop(Utf8ToUtf16(pathLogCfg).c_str());
		prop = prop.getPropertySubset(L"log4cplus.appender.KanevaClient.");
		if (prop.size() > 0) { // If there are some KanevaClient properties defined.
			std::stringstream str;
			str << "KanevaClientAppender_" << clientNetId;
			SharedAppenderPtr appender(new KanevaClientAppender(clientNetId, prop));
			appender->setName(Utf8ToUtf16(str.str()));
			script_logger.addAppender(appender);
		}
	}

	// Clear final departure time if previously set
	SetZoneFinalDepartureTime(zi, 0, true);

	// increment the current zone
	bool isNew = false;
	auto zonePop = IncrementZonePopulation(zi, isNew);
	assert(zonePop >= 1);

	if (!isNew) {
		// Existing zone

		// See if any of the scripts in the zone are interested in processing an arrived event.
		SendArrivedEventToZone(zi, clientNetId, playerInfo.m_name);

		ScriptServerEvent* sse = new ScriptServerEvent(ScriptServerEvent::TransmitGameState, 0, zi.toLong(), zi.GetInstanceId(), clientNetId);
		sse->SetFrom(clientNetId);
		m_myDispatcher->QueueEvent(sse);
	} else {
		// New zone

		// create or update wok.script_zones record
		updateScriptZoneState(zi, true);

		std::set<ULONG> permObjs;
		GetZonePermanentObjects(zi, permObjs);

		auto gsm = std::make_shared<GameStateManager>(zi, m_logger, permObjs);
		m_gameStates.insert(std::make_pair(zi, gsm));

		// Create script container for the zone
		if (!m_zoneMap.createZone(zi, parentZoneInstanceId)) {
			LogWarn("createZone - " << zi.ToStr() << " already exists");
		}

		// and load all the scripts in that zone from disk...
		// Queue up a bunch of background script loading tasks?
		LoadScriptTask* pTask = new LoadScriptTask(
			//			m_logger,
			m_conn,
			m_dbAccessCS,
			zi,
			clientNetId,
			playerInfo.m_name,
			m_globalVMConfig.getLoadSystemScripts(),
			m_globalVMConfig.getLoadSystemScriptsSynch(),
			m_globalVMConfig.getKgpStartWaitTimeout());
		m_pendingLoadScriptTasks.insert(pTask);
		m_loadScriptTaskSystem.QueueTask(pTask);
	}
}

void ScriptServerEngine::RemovePlayerFromZone(NETID clientNetId, ZoneIndex departedZone, ZoneIndex newZone, const ScriptPlayerInfo& playerInfo, bool disconnected) {
	bool shutdownScriptZone = false;

	// Remove the player from the playerZone mapping
	m_playerZones.erase(clientNetId);

	// If they have an appender sending them log messages, remove it from the logger.
	std::stringstream str;
	str << "KanevaClientAppender_" << clientNetId;
	script_logger.removeAppender(Utf8ToUtf16(str.str()));

	auto delayMins = GetZoneSpinDownDelayMinutes(departedZone);

	// Decrement the population count of the previous zone
	auto zonePop = DecrementZonePopulation(departedZone, departedZone != newZone && delayMins == 0); // Do not destroy game states if player simply rezones back
	if (zonePop == 0 && departedZone != newZone) {
		if (delayMins == 0) {
			LogInfo(departedZone.ToStr() << " is going down *NOW*");
			shutdownScriptZone = true;

			// Mark zone as stopping
			ScriptZonePtr pZone = m_zoneMap.getZone(departedZone);
			assert(pZone);
			if (pZone) {
				pZone->setZoneStopping();
			}

			// Remove game states
			m_gameStates.erase(departedZone);

			// Notify all pending load script tasks
			StopPendingLoadScriptTasks(departedZone);

			// reset wok.script_zones record
			updateScriptZoneState(departedZone, false);
		} else {
			// Delayed spin down
			LogInfo(departedZone.ToStr() << " is going down in " << delayMins << " minutes");
			SetZoneFinalDepartureTime(departedZone, fTime::TimeMs(), false);
		}
	}

	// Send a Departed event to all the scripts associated with the previous zone
	// Since playerGetName API does not work consistently during kgp_depart, I'll pass player name as an extra parameter in kgp_depart.
	SendDepartedEventToZone(departedZone, clientNetId, playerInfo, disconnected);

	if (shutdownScriptZone) {
		// Send shutdown and unload events now
		SendShutdownEventToZone(departedZone, "last player departed");
	}
}

void ScriptServerEngine::HandlePlayerArrive(ScriptServerEvent* sse, NETID clientNetId, const ZoneIndex& zi, unsigned playerArriveId) {
	assert(sse != nullptr);
	assert(zi.toLong() != 0);
	assert(playerArriveId > 0);

	// Check if the zone is stopping
	ScriptZonePtr pZone = m_zoneMap.getZone(zi);
	if (pZone && pZone->isZoneStopping()) {
		// Check and dispose existing record
		auto itr = m_pendingPlayerArrives.find(clientNetId);
		if (itr != m_pendingPlayerArrives.end()) {
			assert(playerArriveId > itr->second.playerArriveId);
			itr->second.pPlayerArriveEvent->Release();
			m_pendingPlayerArrives.erase(itr);
		}
		sse->AddRef();
		PendingPlayerArrive ppa{ sse, clientNetId, zi, playerArriveId };
		m_pendingPlayerArrives[clientNetId] = ppa;
		LogInfo(zi.ToStr() << " - queue PlayerArrive event because zone is still stopping, NETID: " << clientNetId << ", playerArriveId: " << playerArriveId);
		return;
	}

	// Extract all remaining information from ScriptServerEvent::PlayerArrived
	NETID prevNetID;
	int prevZoneId, prevInstanceId;
	unsigned parentZoneInstanceId;
	ScriptPlayerInfo playerInfo;
	(*sse->InBuffer()) >> prevNetID >> prevZoneId >> prevInstanceId >> playerInfo >> parentZoneInstanceId;

	ZoneIndex prevZi = ZoneIndex(prevZoneId);
	prevZi.setInstanceId(prevInstanceId);

	AddPlayerToZone(clientNetId, zi, prevNetID, prevZi, playerArriveId, playerInfo, parentZoneInstanceId);
}

void ScriptServerEngine::HandlePlayerDepart(ScriptServerEvent* sse, NETID clientNetId, const ZoneIndex& zi, unsigned playerArriveId) {
	// we get this event when the player logs out from the game or goes to a different server
	assert(playerArriveId > 0);

	assert(zi.toLong() != 0);
	if (zi.toLong() == 0) {
		return;
	}

	// Check if matching PlayerArrive is still pending
	auto itr = m_pendingPlayerArrives.find(clientNetId);
	if (itr != m_pendingPlayerArrives.end() && itr->second.playerArriveId == playerArriveId) {
		LogInfo(zi.ToStr() << " - discard pending PlayerArrive event, NETID: " << clientNetId << ", playerArriveId: " << playerArriveId);
		itr->second.pPlayerArriveEvent->Release();
		m_pendingPlayerArrives.erase(itr);

		LogInfo(zi.ToStr() << " - PlayerDepart event not processed, NETID: " << clientNetId << ", playerArriveId: " << playerArriveId);
		return;
	}

	ScriptPlayerInfo playerInfo;
	NETIDPlayerInfoMap::iterator infoIter = m_players.find(clientNetId);
	if (infoIter != m_players.end()) {
		playerInfo = infoIter->second;
	}

	RemovePlayerFromZone(clientNetId, zi, ZoneIndex(-1), playerInfo, true);

	// remove the player from the IDs and Names map because they are no longer on the server
	if (infoIter != m_players.end()) {
		m_playerIds.erase(playerInfo.m_name);
		m_players.erase(clientNetId);
	}
}

void ScriptServerEngine::ProcessPendingPlayerArrives() {
	// Scan queued PlayerArrive events to see if any of them is ready
	std::vector<PendingPlayerArrive> readyPlayerArrives;
	for (auto itr = m_pendingPlayerArrives.begin(); itr != m_pendingPlayerArrives.end();) {
		PendingPlayerArrive& ppa = itr->second;
		ScriptZonePtr pZone = m_zoneMap.getZone(ppa.zoneIndex);
		assert(pZone);
		if (pZone && !pZone->isZoneStopping()) {
			readyPlayerArrives.push_back(ppa);
			itr = m_pendingPlayerArrives.erase(itr);
		} else {
			++itr;
		}
	}
	// Process the "ready" list
	for (const PendingPlayerArrive& ppa : readyPlayerArrives) {
		HandlePlayerArrive(ppa.pPlayerArriveEvent, ppa.clientNetId, ppa.zoneIndex, ppa.playerArriveId);
		ppa.pPlayerArriveEvent->Release();
	}
}

void ScriptServerEngine::SendErrorToClient(const ServerScriptPtr& ss, NETID clientNetId) {
	if (ss) {
		ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ScriptError, clientNetId);
		int code;
		std::string msg;
		ss->GetError(code, msg);
		(*sce->OutBuffer()) << code << msg;
		SendToClient(sce);
	}
}

bool ScriptServerEngine::CreateFileFromChunks(std::string dir, std::string filename, int chunkNumber, BYTE* buffer, unsigned int bytesToWrite, std::string extension, unsigned long& SeekEnd) {
	//This function is used by NewScriptHandler and UploadScriptHandler to build the sourceCode sent in chunks by the client
	std::string errMsg;
	std::string fname(PathAdd(dir.c_str(), filename.c_str()));
	fname += ".tmp";

	DWORD openFlags = OPEN_ALWAYS;
	if (chunkNumber == 0) { // first chunk creates file
		openFlags = CREATE_ALWAYS;
	}
	std::wstring fnameW = Utf8ToUtf16(fname).c_str();
	std::wstring dirW = Utf8ToUtf16(dir).c_str();
	HANDLE file = ::CreateFileW(fnameW.c_str(), GENERIC_WRITE, 0, NULL, openFlags, FILE_ATTRIBUTE_NORMAL, NULL);
	if (file == INVALID_HANDLE_VALUE) {
		// the directory may not be created, create it and then try creating the file again
		if (!jsFileExists(dir.c_str(), NULL)) {
			//Create directory hierarchy
			SHCreateDirectoryEx(NULL, dirW.c_str(), NULL);
		}
		file = ::CreateFileW(fnameW.c_str(), GENERIC_WRITE, 0, NULL, openFlags, FILE_ATTRIBUTE_NORMAL, NULL);
		if (file == INVALID_HANDLE_VALUE) {
			LogError("Cannot open: " << fname << " " << errorNumToString(GetLastError(), errMsg));
			return false;
		}
	}

	if (::SetFilePointer(file, 0, 0, FILE_END) == INVALID_SET_FILE_POINTER) { // move to end
		LogError("Seek to end in : " << fname << " " << errorNumToString(GetLastError(), errMsg));
		::CloseHandle(file);
		if (!::DeleteFileW(fnameW.c_str())) {
			LogError("Failure to delete : " << fname << " after fatal error.  File may be corrupt.");
		}
		return false;
	}

	DWORD bytesWritten = 0;
	if (bytesToWrite > 0 && !::WriteFile(file, buffer, bytesToWrite, &bytesWritten, 0)) {
		LogError("Cannot write to: " << fname << " " << errorNumToString(GetLastError(), errMsg));
		::CloseHandle(file);
		if (!::DeleteFileW(fnameW.c_str())) {
			LogError("Failure to delete : " << fname << " after fatal error.  File may be corrupt.");
		}
		return false;
	}

	SeekEnd = ::GetFileSize(file, NULL);

	::CloseHandle(file);

	if (chunkNumber == ScriptServerEvent::LAST_CHUNK) { // last chunk
		std::string newFname(filename);
		newFname += ".";
		newFname += extension;
		if (!::MoveFileEx(fnameW.c_str(), Utf8ToUtf16(PathAdd(dir.c_str(), newFname.c_str())).c_str(), MOVEFILE_REPLACE_EXISTING)) {
			::DeleteFileW(fnameW.c_str());
			LogError("Failed to rename : " << fname << " to " << newFname << " " << errorNumToString(GetLastError(), errMsg));
			return false;
		} else {
			return true;
		}
	}
	return false;
}

bool ScriptServerEngine::SendAssetToClient(ScriptServerEvent::UploadAssetTypes assetType, int seekPosition, std::string fileName, NETID clientNetId) {
	std::string dir, extension;
	if (!GetSourceDirAndExtension(assetType, fileName, dir, extension)) {
		LogError("Failed unknown asset type for [" << fileName << "], type = " << assetType);
		return false;
	}

	std::string fullName = fileName + "." + extension;
	std::string fullPath = PathAdd(dir, fullName);
	std::wstring fullPathW = Utf8ToUtf16(fullPath);

	HANDLE file = ::CreateFile(fullPathW.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (file == INVALID_HANDLE_VALUE) {
		LogError("Cannot open: " << fullPath << ", err = " << GetLastError());
		return false;
	}

	DWORD fileSize = ::GetFileSize(file, 0);
	if (fileSize == INVALID_FILE_SIZE) {
		DWORD err = GetLastError();
		std::string errmsg;
		std::string msg = "Error getting file size of: " + fullPath;
		LogError(msg << " " << errorNumToString(err, errmsg));
		return false;
	}
	const unsigned long UPLOAD_SCRIPT_BUFFER_SIZE = 256;
	unsigned long buffer[UPLOAD_SCRIPT_BUFFER_SIZE];

	DWORD bytesRead;
	DWORD totBytesRead = 0;
	BOOL res = FALSE;
	short chunk = 0;
	if (seekPosition > 0) {
		if (SetFilePointer(file, seekPosition, 0, FILE_BEGIN) == INVALID_SET_FILE_POINTER) {
			std::string msg = "Error seeking to start of next message for file: " + fullPath;
			LogError(msg);
		}
	}
	while ((res = ::ReadFile(file, buffer, sizeof(buffer), &bytesRead, NULL)) == TRUE && bytesRead > 0 && totBytesRead < ScriptServerEvent::BYTES_BEFORE_ACK) {
		Sleep(ScriptServerEvent::FILE_TRANSFER_PACKET_DELAY);
		totBytesRead += bytesRead;
		ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::DownloadAsset, clientNetId);
		(*sce->OutBuffer()) << fileName;
		(*sce->OutBuffer()) << (unsigned long)sizeof(buffer); // bytesSent
		(*sce->OutBuffer()) << (unsigned long)bytesRead; // bytesRead
		for (unsigned int i = 0; i < UPLOAD_SCRIPT_BUFFER_SIZE; i++) {
			(*sce->OutBuffer()) << buffer[i];
		}
		(*sce->OutBuffer()) << ((seekPosition + totBytesRead) >= fileSize ? ScriptServerEvent::LAST_CHUNK : chunk);
		(*sce->OutBuffer()) << assetType;
		(*sce->OutBuffer()) << (unsigned long)seekPosition + totBytesRead;

		SendToClient(sce);
		chunk++;
	}
	::CloseHandle(file);
	// Send special there is more packet that will cause another download request after this one processes.
	if (totBytesRead >= ScriptServerEvent::BYTES_BEFORE_ACK) {
		ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::DownloadAsset, clientNetId);
		(*sce->OutBuffer()) << fileName;
		(*sce->OutBuffer()) << (unsigned long)0; // bytesSent
		(*sce->OutBuffer()) << (unsigned long)0; // bytesRead

		(*sce->OutBuffer()) << ScriptServerEvent::BUT_WAIT_THERES_MORE;
		(*sce->OutBuffer()) << assetType;
		(*sce->OutBuffer()) << (unsigned long)seekPosition + totBytesRead;

		SendToClient(sce);
	}
	if (res == FALSE) {
		std::string msg = "Error reading: " + fullPath;
		LogError(msg.c_str());
		return false;
	}

	return true;
}

//
// GetAllSourceDirsAndExtension
//
// Determine all source directories and extension.
//
// @@param assetType	- Asset type (see UploadAssetType)
// @@param baseName		- Asset base name (no extension, optional)
// @@param primaryDir	- Return full path to the primary source directory (where uploaded file is)
// @@param otherDirs	- Return a list of full path to the other source directories (shared ones etc)
// @@param extension	- File extension determined from asset type
//
// @@return true if asset type is recognized and valid.
//
bool ScriptServerEngine::GetAllSourceDirsAndExtension(ScriptServerEvent::UploadAssetTypes assType, std::string& primaryDir, std::vector<std::string>& otherDirs, std::string& extension) {
	// Init out params
	primaryDir.clear();
	otherDirs.clear();
	extension.clear();

	// Determine extension
	switch (assType) {
		case ScriptServerEvent::ServerScript:
		case ScriptServerEvent::SavedScript:
		case ScriptServerEvent::ActionItem:
		case ScriptServerEvent::ActionInstance:
		case ScriptServerEvent::MenuScript:
			extension = "lua";
			break;

		case ScriptServerEvent::ActionItemParams:
		case ScriptServerEvent::ActionInstanceParams:
			extension = "json";
			break;

		case ScriptServerEvent::MenuXml:
			extension = "xml";
			break;
		case ScriptServerEvent::MenuDDS:
			extension = "dds";
			break;
		case ScriptServerEvent::MenuTGA:
			extension = "tga";
			break;
		case ScriptServerEvent::MenuBMP:
			extension = "bmp";
			break;
		case ScriptServerEvent::MenuPNG:
			extension = "png";
			break;
		case ScriptServerEvent::MenuJPG:
			extension = "jpg";
			break;
		case ScriptServerEvent::MenuJPEG:
			extension = "jpeg";
			break;

		default:
			return false;
	}

	// Determine folder
	std::string sourceDir;
	switch (assType) {
		case ScriptServerEvent::ServerScript:
			sourceDir = PathAdd(GAMEFILES, PathAdd(SCRIPTSERVERDIR, SAVEDSCRIPTDIR));
			break;
		case ScriptServerEvent::SavedScript:
			sourceDir = PathAdd(GAMEFILES, PathAdd(SCRIPTSERVERDIR, SAVEDSCRIPTDIR));
			break;
		case ScriptServerEvent::ActionItem:
		case ScriptServerEvent::ActionItemParams:
			sourceDir = PathAdd(GAMEFILES, PathAdd(SCRIPTSERVERDIR, ACTIONSCRIPTDIR));
			break;
		case ScriptServerEvent::ActionInstance:
		case ScriptServerEvent::ActionInstanceParams:
			sourceDir = PathAdd(GAMEFILES, PathAdd(SCRIPTSERVERDIR, ACTIONINSTANCEDIR));
			break;
		case ScriptServerEvent::MenuScript:
			sourceDir = PathAdd(GAMEFILES, MENUSCRIPTSDIR);
			break;
		case ScriptServerEvent::MenuXml:
		case ScriptServerEvent::MenuDDS:
		case ScriptServerEvent::MenuTGA:
		case ScriptServerEvent::MenuBMP:
		case ScriptServerEvent::MenuPNG:
		case ScriptServerEvent::MenuJPG:
		case ScriptServerEvent::MenuJPEG:
			sourceDir = PathAdd(GAMEFILES, MENUSDIR);
			break;
		default:
			jsAssert(false);
			return false;
	}

	primaryDir = PathAdd(m_devToolsSourceFolder, sourceDir) + "\\";
	if (!m_devToolsAppSourceFolder.empty()) {
		otherDirs.push_back(primaryDir);
		primaryDir = PathAdd(m_devToolsAppSourceFolder, sourceDir) + "\\";
	}

	return true;
}

//
// GetSourceDirAndExtension
//
// Determine source directory and extension. For a asset of specified type and name
//
// @@param assetType	- Asset type (see UploadAssetType)
// @@param baseName		- Asset base name (no extension)
// @@param sourceDir	- Return full path to the source directory (where uploaded file is)
// @@param extension	- File extension determined from asset type
//
// @@return true if asset type is recognized and valid.
//
bool ScriptServerEngine::GetSourceDirAndExtension(ScriptServerEvent::UploadAssetTypes assetType, const std::string& baseName, std::string& sourceDir, std::string& extension) {
	std::string primarySourceDir;
	std::vector<std::string> otherSourceDirs;

	if (!GetAllSourceDirsAndExtension(assetType, primarySourceDir, otherSourceDirs, extension)) {
		//Invalid asset type
		return false;
	}

	//Default to primary directory
	sourceDir = primarySourceDir;

	//If file name not specified, use primary
	if (baseName.empty()) {
		return true;
	}

	// check if file exists
	std::string fullName = baseName + "." + extension;
	std::string fullPath = PathAdd(primarySourceDir, fullName);
	bool isDir = false;
	if (!jsFileExists(fullPath.c_str(), &isDir) || isDir) {
		// try other source folder
		for (std::vector<std::string>::const_iterator i = otherSourceDirs.begin(); i != otherSourceDirs.end(); ++i) {
			fullPath = PathAdd(*i, fullName);
			if (jsFileExists(fullPath.c_str(), &isDir) && !isDir) {
				// Found
				sourceDir = *i;
				return true;
			}
		}
	}

	// Not found anywhere: use primary directory anyway
	return true;
}

bool ScriptServerEngine::Upload3DAppAssetEventHandler(ScriptServerEvent* se, ZoneIndex /*zoneId*/, NETID clientNetId) {
	std::string filename, fullClientPath;
	short chunkNumber = 0;
	int assetType;
	unsigned long bytesSent;
	unsigned long bytesRead;
	unsigned long seekPos;
	(*se->InBuffer()) >> filename;
	(*se->InBuffer()) >> bytesSent;
	(*se->InBuffer()) >> bytesRead;
	unsigned int buffer_size = bytesSent / sizeof(unsigned long);
	unsigned long* buffer = new unsigned long[buffer_size];

	for (unsigned int i = 0; i < buffer_size; i++) {
		(*se->InBuffer()) >> buffer[i];
	}
	(*se->InBuffer()) >> chunkNumber;
	(*se->InBuffer()) >> assetType;
	(*se->InBuffer()) >> fullClientPath; // added full Client Path to event.

	if (((m_allowedUploadAssetTypes >> assetType) & 1) == 0) {
		LogError("Upload not allowed for asset type " << assetType << ", file name: " << filename);
		return false;
	}

	std::string dir, extension;
	if (!GetSourceDirAndExtension((ScriptServerEvent::UploadAssetTypes)assetType, "", dir, extension)) {
		delete[] buffer;
		LogError("Upload failed unknown asset type for " << filename);
		return false;
	}

	// Create/append source file
	bool success = CreateFileFromChunks(dir, filename, chunkNumber, (BYTE*)buffer, bytesRead, extension, seekPos);
	delete[] buffer;

	if (chunkNumber == ScriptServerEvent::LAST_CHUNK && !success) { // last chunk
		LogError("Failed to create file " << filename << ".lua");
		return false;
	} else if (chunkNumber == ScriptServerEvent::LAST_CHUNK) {
		// Upload completed - now publish it

		// Completion event - to be fired when publishing completes
		ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PublishAssetCompleted, clientNetId);
		(*sce->OutBuffer()) << assetType;
		(*sce->OutBuffer()) << filename;

		// Get publishing information
		std::string sourceDir, destDir, fullName, patchPrefix;
		bool addToPatch = false, compileLua = false;
		if (!GetPublishingInfo((ScriptServerEvent::UploadAssetTypes)assetType, filename, sourceDir, destDir, fullName, addToPatch, patchPrefix, compileLua)) {
			// Error getting publishing info
			(*sce->OutBuffer()) << false << "Unsupported type";
			SendToClient(sce);
			return false;
		}

		jsAssert(!sourceDir.empty());
		jsAssert(!destDir.empty());
		jsAssert(!fullName.empty());

		// Publish asynchronously
		PublishTask* pt = new PublishTask(getGameId(), sourceDir, destDir, fullName, addToPatch, patchPrefix, compileLua, sce);
		m_taskSystem.QueueTask(pt);
	} else if (chunkNumber == ScriptServerEvent::BUT_WAIT_THERES_MORE) {
		ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PublishResponseEvent, clientNetId);
		*sce->OutBuffer() << filename;
		*sce->OutBuffer() << fullClientPath;
		*sce->OutBuffer() << assetType;
		*sce->OutBuffer() << seekPos;

		SendToClient(sce);
	}
	return true;
}

bool ScriptServerEngine::GetServerLogEventHandler(ScriptServerEvent*, NETID clientNetId) {
	// Send each line of the log as a seperate 'tell' message to that player only.
	std::string cfgFileName = std::string(PathBase()) + "/" + SERVER_LOG_PROP_FILE;
	log4cplus::helpers::Properties prop(Utf8ToUtf16(cfgFileName));
	std::wstring logFileName = prop.getProperty(L"log4cplus.appender.ServerScriptLog.File");
	std::ifstream file(logFileName);
	if (!file.good()) {
		LogError("file.good() FAILED - '" << logFileName << "'");
		return false;
	}

	while (!file.eof()) {
		char buffer[1024];
		file.getline(buffer, sizeof buffer);
		SendPlayerTellToClient(clientNetId, buffer, "Log", true, 0);
	}

	return true;
}

bool ScriptServerEngine::DownloadScriptEventHandler(ScriptServerEvent* se, NETID clientNetId) {
	int assetType, seekPosition;
	std::string fileName;
	(*se->InBuffer()) >> assetType;
	(*se->InBuffer()) >> seekPosition;
	(*se->InBuffer()) >> fileName;

	if (!SendAssetToClient((ScriptServerEvent::UploadAssetTypes)assetType, seekPosition, fileName, clientNetId)) {
		LogError("SendAssetToClient() FAILED - assetType=" << assetType << " netId=" << clientNetId << " '" << fileName << "'");
		return false;
	}

	return true;
}

// Delete a patch asset file
bool ScriptServerEngine::DeleteAssetEventHandler(ScriptServerEvent* se, NETID clientNetId) {
	int assetType;
	std::string filename;
	(*se->InBuffer()) >> assetType;
	(*se->InBuffer()) >> filename;

	// Completion event
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PublishDeleteAssetCompleted, clientNetId);
	(*sce->OutBuffer()) << assetType;
	(*sce->OutBuffer()) << filename;

	// Get publishing information
	std::string sourceDir, destDir, fullName, patchPrefix;
	bool addToPatch = false, compileLua = false;
	if (!GetPublishingInfo((ScriptServerEvent::UploadAssetTypes)assetType, filename, sourceDir, destDir, fullName, addToPatch, patchPrefix, compileLua) || !addToPatch) {
		LogError("GetPublishingInfo() FAILED - '" << filename << "'");
		(*sce->OutBuffer()) << false << "Unsupported type";
		SendToClient(sce);
		return false;
	}

	jsAssert(!sourceDir.empty());
	jsAssert(!destDir.empty());
	jsAssert(!fullName.empty());

	m_taskSystem.QueueTask(new DeleteAssetTask(getGameId(), sourceDir, destDir, fullName, patchPrefix, sce));

	return true;
}

bool ScriptServerEngine::AttachScriptEventHandler(ScriptServerEvent* se, NETID clientNetId, ULONG objPlacementId) {
	std::string fileName;
	*se->InBuffer() >> fileName;

	LogInfo("objId=" << objPlacementId << " netId=" << clientNetId << " '" << fileName << "'");

	// Need to grab the zone index from the clientNetId map
	NETIDZoneIndexMap::iterator itr = m_playerZones.find(clientNetId);
	if (itr == m_playerZones.end()) {
		LogError("playerZones.find() FAILED - netId=" << clientNetId);
		return false;
	}
	ZoneIndex zi = itr->second;

	// Split folder and file name
	size_t ofsSlash = fileName.find_last_of("\\/");
	std::string folder;
	if (ofsSlash != std::string::npos) {
		folder = fileName.substr(0, ofsSlash);
		fileName = fileName.substr(ofsSlash + 1);
	}

	// Validate file name
	if (fileName.empty()) {
		LogError("Filename Empty");
		return false;
	}

	// Validate folder
	if (folder.empty() || folder != ACTIONSCRIPTDIR && folder != ACTIONINSTANCEDIR && folder == SAVEDSCRIPTDIR)
		folder = SAVEDSCRIPTDIR;

	std::string relPath = PathAdd(folder, fileName.c_str());
	std::string sourceCode;
	if (!LoadScriptFile(relPath.c_str(), sourceCode) || !LoadScript(relPath.c_str(), sourceCode, NULL, 0, objPlacementId, zi, clientNetId, false, 0)) {
		LogError("LoadScript() FAILED - '" << fileName << "'");
		return false;
	}

	QueueSaveScriptTask(objPlacementId, relPath);

	return true;
}

bool ScriptServerEngine::DetachScriptEventHandler(ScriptServerEvent* /*se*/, NETID clientNetId, ULONG objPlacementId) {
	LogInfo("objId=" << objPlacementId << " netId=" << clientNetId);

	auto itr = m_playerZones.find(clientNetId);
	if (itr == m_playerZones.end()) {
		LogError("playerZones.find() FAILED - netId=" << clientNetId);
		return false;
	}
	ZoneIndex zi = itr->second;

	ServerScriptPtr pSS = m_zoneMap.getScriptByID(objPlacementId, zi, false);
	if (!pSS) {
		LogError("getScriptByID() FAILED - objId=" << objPlacementId << " netId=" << clientNetId);
		return false;
	}

	m_taskSystem.QueueTask(new DetachScriptTask(m_conn, m_dbAccessCS, objPlacementId));
	SendScriptShutdownEvent(pSS, clientNetId, "script detached manually");

	return true;
}

bool ScriptServerEngine::DeleteScriptEventHandler(ScriptServerEvent* se) {
	std::string filename;
	*se->InBuffer() >> filename;

	size_t pos = filename.rfind('.');
	if (pos != std::string::npos)
		filename = filename.substr(0, pos);

	if (!DeleteScript(ScriptServerEvent::SavedScript, filename)) {
		LogError("DeleteScript() FIALED - '" << filename << "'");
		return false;
	}

	return true;
}

// This can be called from the background "LoadScript" thread, or the foreground event handler.
// This function will now load both JSON and LUA script.
// @@param filename - relative path from ScriptServerScript folder
bool ScriptServerEngine::LoadScript(const char* filename, const std::string& sourceCode, const char* arg, ULONG callerPID, ULONG objPlacementId, ZoneIndex zoneId, NETID clientNetId, bool isSystemScript, UINT kgpStartWaitTimeout) {
	// Obtain game state manager for current zone
	std::shared_ptr<GameStateManager> gsm;
	{
		std::lock_guard<fast_recursive_mutex> lock(m_eventHandlerCS);
		auto iter = m_gameStates.find(zoneId);
		if (iter != m_gameStates.end()) {
			gsm = iter->second;
		} else {
			LogWarn("GameStateManager not found for zone " << zoneId);
			ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ScriptError, clientNetId);
			int code = ServerScript::LOAD_ERROR;
			std::string msg = "Internal error: game state manager not found";
			(*sce->OutBuffer()) << code << msg;
			SendToClient(sce);
			return false;
		}
	}

	// Get script zone (created by LoadScriptTask, no removal at this moment)
	auto pZone = m_zoneMap.getZone(zoneId);
	assert(pZone);

	if (pZone->getNumScripts() >= m_maxScriptsPerZone) {
		// Log the error and send the user a message indicating the error, so they know
		// why it failed (they can't see the logs in a hosted environment.
		LogWarn("Number of scripts >= maximum scripts for this zone.  Maximum = " << m_maxScriptsPerZone);
		ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ScriptError, clientNetId);
		int code = ServerScript::LOAD_ERROR;
		std::string msg = "Number of scripts >= maximum scripts for this zone.";
		(*sce->OutBuffer()) << code << msg;
		SendToClient(sce);
		return false;
	}

	LogInfo("[" << zoneId.toLong() << "." << objPlacementId << "] Path: " << (filename ? filename : "NULL"));

	ServerScriptPtr pScript = pZone->getScript(objPlacementId, false);
	if (pScript) {
		//
		// The DO already has a script associated with it.  I need to kill the script
		// and replace it with this new one.
		//
		LogInfo("[" << zoneId.toLong() << "." << objPlacementId << "] Already has a script associated with it, which I'm replacing with the new one.");

		// Notify script to shutdown if not already
		std::lock_guard<fast_recursive_mutex> lock(m_eventHandlerCS);

		// Script is being shut down: queue restart request
		TimeMs timeout = pScript->IsSystemScript() ? m_globalVMConfig.getSystemScriptShutdownTimeout() : m_globalVMConfig.getUserScriptShutdownTimeout();
		pScript->QueueReloadRequest(clientNetId, filename, timeout);
		LogInfo("[" << zoneId.toLong() << "." << objPlacementId << "] Previous script VM is being terminated. Script loading is deferred.");

		SendScriptShutdownEvent(pScript, clientNetId, "new script loaded");
		return true;
	}

	//
	// Add the VM to the map for easy retrieval and add
	// the Id to either the ready or idle lists and
	// prepair for futher processing.
	//
	// this is done even if calls to loadstring and
	// pcall fail, because in those cases I want a
	// script in the ZOMBIE state to save the error
	// codes and messages.
	//
	// Always use my allocator, but don't record memory use until the
	// ServerScript object has been created.
	//

	// Create a ServerScript object to maintain VM state
	pScript = std::make_shared<ServerScript>(m_globalVMConfig, gsm, pZone, objPlacementId, isSystemScript,
		GetAssetTypeByRelPath(filename), filename);

	pScript->LuaVMInit(filename, m_scriptAPIRegistry);

	// Insert into the script container of the zone
	pZone->addScript(objPlacementId, pScript);

	// Apply global config
	auto configGroups = m_zoneConfigGroups[zoneId];
	jsAssert(m_globalJson != nullptr);
	m_globalJson->getLuaScriptConfigurator(configGroups)(pScript->LuaGlobalState());

	// Load script
	std::string errMsg;

	if (pScript->LuaLoadScript(STLHasExtension(filename, "json"), sourceCode, errMsg) != 0) {
		ScriptErrorCallback(pScript, ServerScript::LOAD_ERROR, errMsg.c_str(), true, 0);
		LogWarn(pScript->LogSubject() << "Error loading script " << pScript->GetFileName() << ": " << errMsg);
		return false;
	}

	if (pScript->LuaRunScript(errMsg) != 0) {
		ScriptErrorCallback(pScript, ServerScript::CALL_ERROR, errMsg.c_str(), true, 0);
		LogWarn(pScript->LogSubject() << "Error processing script " << pScript->GetFileName() << ": " << errMsg);
		return false;
	}

	LogDebug(pScript->LogSubject() << "Script loaded successfully: " << pScript->GetFileName());

	// Lock eventHandler mutex so that no AddEvent will be called while we are checking
	std::unique_lock<fast_recursive_mutex> lock(m_eventHandlerCS);

	// Ready to run VM
	if (kgpStartWaitTimeout > 0) {
		// Disable count hook for blocking calls
		pScript->DisableCountHook();
	}

	// Prepare Win32 event for completion wait
	pScript->ResetCallCompletionEvent();

	m_pScheduler->startScript(pScript, clientNetId, arg, callerPID); // Push a Start event even if the script does not have a kgp_start handler

	if (kgpStartWaitTimeout > 0) {
		// Wait until task completes or times out
		pScript->WaitForCallCompletion(kgpStartWaitTimeout);
	}

	// Restore count hook
	pScript->EnableCountHook(); //TODO: fix this - RunVMTask thread might still be working on the same lua_State this call is going to affect

	return true;
}

bool ScriptServerEngine::ObjectMapEventsCallback(void* vm, int objId) {
	ASSERT_VM_SS(false);

	ss->GetScriptZone()->mapPID(objId, ss->GetPlacementId());
	return true;
}

void ScriptServerEngine::ZoneGetStatsCallback(void* vm, unsigned int& numberOfScripts, unsigned long& totalMemoryUsed, std::vector<ScriptRuntimeData>& stateList) {
	ASSERT_VM_SS();

	// Get information about this particular zone.
	std::vector<ServerScriptPtr> allScripts = ss->GetScriptZone()->getAllScripts();
	numberOfScripts = allScripts.size();
	totalMemoryUsed = 0;
	for (const ServerScriptPtr& pScript : allScripts) {
		ScriptRuntimeData state;
		state.m_name = pScript->GetFileName();
		state.m_placementId = pScript->GetPlacementId();
		state.m_state = static_cast<int>(pScript->GetState());
		state.m_lifeTime = pScript->GetTotalTime();
		state.m_readyTime = pScript->GetTimeInState(ScriptState::READY);
		state.m_idleTime = pScript->GetTimeInState(ScriptState::IDLE);
		state.m_blockedTime = pScript->GetTimeInState(ScriptState::BLOCKED);
		stateList.push_back(state);
		totalMemoryUsed += pScript->GetCurrentMemory();
	}
}

bool ScriptServerEngine::ScriptGetCurrentGameIdCallback(void* vm, long& gameId) {
	ASSERT_VM_SS(false);

	gameId = 0;
	gameId = getGameId();
	return (gameId != 0);
}

bool ScriptServerEngine::ScriptGetCurrentInstanceCallback(void* vm, long& instanceId, long& zoneId, long& zoneType) {
	ASSERT_VM_SS(false);

	// Get information about this particular zone.
	instanceId = ss->GetZoneId().GetInstanceId();
	zoneId = ss->GetZoneId().getClearedInstanceId();
	zoneType = ss->GetZoneId().GetType();

	return true;
}

ScriptAPIRetType::ASYN ScriptServerEngine::ScriptMakeWebcallAsync(void* vm, std::string url, bool usePost, const AsyncUserArg& userArg) {
	ASSERT_VM_SS(ScriptAPIRetType::ASYN::Failed());

	ScriptWebCallTask* pTask = new ScriptWebCallTask(m_myDispatcher, url, ss->GetPlacementId(), ss->GetZoneId(), usePost, vm, true, userArg);
	// Queue web task right away
	m_taskSystem.QueueTask(pTask);

	// No return values. Result will come in through kgp_result when webcall completes.
	return ScriptAPIRetType::ASYN();
}

ScriptAPIRetType::BLOC ScriptServerEngine::ScriptMakeWebcallCallback(void* vm, std::string url, bool usePost, const AsyncUserArg& userArg) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptWebCallTask* pTask = new ScriptWebCallTask(m_myDispatcher, url, ss->GetPlacementId(), ss->GetZoneId(), usePost, vm, false, userArg);

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::ScriptMakeWebcall, "scriptMakeWebcall", nullptr, pTask);
}

bool ScriptServerEngine::ScriptCallStoredProcedureCallback(void* vm, std::vector<int> args, std::vector<std::vector<std::string>>& retValues) {
	//Argument error checking done at the API level

	bool success = false;
	lua_State* L = (lua_State*)vm;
	StoreQueryResult results;

	{
		std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
		BEGIN_TRY_SQL();

		char buf[65];
		std::string queryStr = "CALL " + std::string(lua_tolstring(L, 1, NULL)) + "( ";

		//Start at index 1 since 0 is the name which we already have
		for (int i = 1; i < (int)args.size(); i++) {
			if (i == 1)
				queryStr += "%";
			else
				queryStr += ", %";

			_itoa_s(i - 1, buf, 65, 10);
			queryStr += buf;

			if ((lua_type(L, i + 1) == LUA_TSTRING) || (lua_type(L, i + 1) == LUA_TBOOLEAN)) {
				queryStr += "q";
			}
		}

		queryStr += " )";

		Query genQuery = m_conn->query(queryStr);
		genQuery.parse();

		SQLQueryParms sqlp;

		//Start at index 1 since 0 is the name, which we already have
		for (int i = 1; i < (int)args.size(); i++) {
			switch (lua_type(L, i + 1)) {
				case LUA_TBOOLEAN:
					sqlp << SQLTypeAdapter(lua_toboolean(L, i + 1) != 0 ? "T" : "F");
					break;
				case LUA_TNUMBER:
					sqlp << SQLTypeAdapter(lua_tonumber(L, i + 1));
					break;
				case LUA_TSTRING:
					sqlp << SQLTypeAdapter(lua_tolstring(L, i + 1, NULL));
					break;
				default:
					ASSERT(false); //only numbers and strings should be going to stored procs
			}
		}

		LogDebug("ScriptCallStoredProcedureCallback: " + genQuery.str());

		results = genQuery.store(sqlp);

		checkMultipleResultSets(genQuery, "ScriptCallStoredProcedureCallback");

		success = true;

		END_TRY_SQL_LOG_ONLY(m_conn);
	}

	if (success) {
		for (int r = 0; r < (int)results.num_rows(); r++) { //rows
			Row row = results.at(r);
			std::vector<std::string> strRow;
			for (int c = 0; c < (int)row.size(); c++) { //columns
				std::string col = std::string(row.at(c).c_str());
				strRow.push_back(col);
			}
			retValues.push_back(strRow);
		}
	}

	return success;
}

bool ScriptServerEngine::ScriptSetEnvironmentCallback(void* vm, std::vector<double> args) {
	ASSERT_VM_SS(false);

	int size = args.size();

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ScriptSetEnvironment, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << size;

	for (int i = 0; i < size; i++)
		*sce->OutBuffer() << args[i];

	// Buffer this call for players who come in later
	ScriptClientEvent* e = new ScriptClientEvent(sce);
	ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, GameStateManager::ID_ENVIRONMENT);

	// Send event to server engine
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::ScriptSetDayStateCallback(void* vm, std::vector<double> args) {
	ASSERT_VM_SS(false);

	int size = args.size();

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ScriptSetDayState, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << size;

	for (int i = 0; i < size; i++)
		*sce->OutBuffer() << args[i];

	// Buffer this call for players who come in later
	ScriptClientEvent* e = new ScriptClientEvent(sce);
	ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, GameStateManager::ID_ENVIRONMENT);

	// Send event to server engine
	SendToClient(sce);
	return true;
}

void ScriptServerEngine::SetEffectTrackCallback(void* vm, int id, ScriptClientEvent::EventType eventType, const std::vector<std::vector<int>>& allEffects, const std::vector<std::string>& allStrings) {
	ASSERT_VM_SS();

	// processed by the game server right now (clientId 0)
	ScriptClientEvent* sce = new ScriptClientEvent(eventType, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << id << (int)allEffects.size() << (int)allStrings.size();

	// append all the strings
	for (auto itStrs = allStrings.begin(); itStrs != allStrings.end(); ++itStrs)
		*sce->OutBuffer() << *itStrs;

	// append all the effects, which include indices into the 'string table'
	for (auto itAll = allEffects.begin(); itAll != allEffects.end(); ++itAll) {
		// individual effect
		const std::vector<int>& effect = *itAll;
		*sce->OutBuffer() << (int)effect.size();
		for (auto it = effect.begin(); it != effect.end(); ++it) {
			*sce->OutBuffer() << *it;
		}
	}

	SendToClient(sce);
}

ScriptAPIRetType::BLOC ScriptServerEngine::ObjectGetNextPlayListItemCallback(void* vm, ULONG clientId) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectGetNextPlaylistItem, 0);
	*sce->OutBuffer() << clientId << ss->GetZoneId().toLong() << ss->GetPlacementId();

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::ObjectGetNextPlaylistItem, "objectGetNextPlayListItem", sce);
}

ScriptAPIRetType::BLOC ScriptServerEngine::ObjectSetNextPlayListItemCallback(void* vm, ULONG clientId, int playListItem) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetNextPlaylistItem, 0);
	*sce->OutBuffer() << clientId << playListItem << ss->GetZoneId().toLong() << ss->GetPlacementId();

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::ObjectSetNextPlaylistItem, "objectSetNextPlayListItem", sce);
}

ScriptAPIRetType::BLOC ScriptServerEngine::ObjectEnumeratePlayListCallback(void* vm, ULONG clientId) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectEnumeratePlaylist, 0);
	*sce->OutBuffer() << clientId << ss->GetZoneId().toLong() << ss->GetPlacementId();

	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::ObjectEnumeratePlaylist, "objectEnumeratePlayList", sce);
}

ScriptAPIRetType::BLOC ScriptServerEngine::ObjectSetPlayListCallback(void* vm, ULONG clientId, int playListID, int playListItem, bool stopBroadcast) {
	ASSERT_VM_SS(ScriptAPIRetType::BLOC::Failed());

	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetPlaylist, 0);
	*sce->OutBuffer() << clientId << playListID << playListItem << stopBroadcast << ss->GetZoneId().toLong() << ss->GetPlacementId();
	return ScriptAPIRetType::BLOC(ss, ScriptServerEvent::ObjectSetNextPlaylistItem, "objectSetPlayList", sce); // Expected unblock event is ObjectSetNextPlaylistItem
}

bool ScriptServerEngine::PlayerSetVisibilityFlagCallback(void* vm, ULONG clientId, bool bVisible) {
	ASSERT_VM_SS(false);

	// Send a ScriptClientEvent to the client.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSetVisibilityFlag, clientId, ScriptClientEvent::Scope_Player);
	(*sce->OutBuffer()) << clientId << (bVisible ? 1 : 0) << ss->GetZoneId().toLong();

	SendToClient(sce);

	return true;
}

unsigned ScriptServerEngine::IncrementZonePopulation(const ZoneIndex& zoneIndex, bool& isNew) {
	LogInfo(zoneIndex.ToStr());

	std::lock_guard<std::mutex> lock(m_zoneInfoMapMutex);

	auto itr = m_zoneInfoMap.find(zoneIndex);
	if (itr != m_zoneInfoMap.end()) {
		return ++itr->second.m_population;
	} else {
		isNew = true;
		m_zoneInfoMap.insert(std::make_pair(zoneIndex, ZoneInfo(1, 0, m_defaultSpinDownDelayMinutes)));
		return 1;
	}
}

unsigned ScriptServerEngine::DecrementZonePopulation(const ZoneIndex& zoneIndex, bool removeIfEmpty) {
	LogInfo(zoneIndex.ToStr());

	std::lock_guard<std::mutex> lock(m_zoneInfoMapMutex);

	auto itr = m_zoneInfoMap.find(zoneIndex);
	if (itr != m_zoneInfoMap.end()) {
		assert(itr->second.m_population > 0);
		unsigned newPop = --itr->second.m_population;
		if (newPop == 0 && removeIfEmpty) {
			m_zoneInfoMap.erase(itr);
		}
		return newPop;
	} else {
		assert(false);
		LogError(zoneIndex.ToStr() << " - Zone info not found");
		return 0;
	}
}

void ScriptServerEngine::SetZoneMaxPlacementId(const ZoneIndex& zoneIndex, ULONG maxPlacementId) {
	LogInfo(zoneIndex.ToStr() << " - max PID = " << maxPlacementId);

	std::lock_guard<std::mutex> lock(m_zoneInfoMapMutex);

	auto itr = m_zoneInfoMap.find(zoneIndex);
	if (itr != m_zoneInfoMap.end()) {
		itr->second.m_maxPlacementId = maxPlacementId;
	} else {
		assert(false);
		LogError(zoneIndex.ToStr() << " - Zone info not found");
	}
}

ULONG ScriptServerEngine::GetZoneMaxPlacementId(const ZoneIndex& zoneIndex) {
	std::lock_guard<std::mutex> lock(m_zoneInfoMapMutex);

	auto itr = m_zoneInfoMap.find(zoneIndex);
	if (itr != m_zoneInfoMap.end()) {
		return itr->second.m_maxPlacementId;
	} else {
		assert(false);
		LogError(zoneIndex.ToStr() << " - Zone info not found");
		return 0;
	}
}

ULONG ScriptServerEngine::GetIncrementedZoneMaxPlacementId(const ZoneIndex& zoneIndex) {
	std::lock_guard<std::mutex> lock(m_zoneInfoMapMutex);

	auto itr = m_zoneInfoMap.find(zoneIndex);
	if (itr != m_zoneInfoMap.end()) {
		return ++itr->second.m_maxPlacementId;
	} else {
		assert(false);
		LogError(zoneIndex.ToStr() << " - Zone info not found");
		return 0;
	}
}

void ScriptServerEngine::SetZoneSpinDownDelayMinutes(const ZoneIndex& zoneIndex, int minutes, bool ignoreIfNotFound) {
	LogInfo(zoneIndex.ToStr() << " - new val: " << minutes);

	if (minutes < 0) {
		minutes = m_defaultSpinDownDelayMinutes;
		LogInfo(zoneIndex.ToStr() << " - use default: " << minutes);
	}

	std::lock_guard<std::mutex> lock(m_zoneInfoMapMutex);
	auto itr = m_zoneInfoMap.find(zoneIndex);
	if (itr != m_zoneInfoMap.end()) {
		itr->second.m_spinDownDelayMinutes = minutes;
		if (minutes == 0) {
			// Force handleTimerEvent to sweep expired zone now
			m_lastZoneSpinDownExpiryCheckTimeMs = 0;
		}
	} else if (!ignoreIfNotFound) {
		assert(false);
		LogError(zoneIndex.ToStr() << " - Zone info not found");
	}
}

unsigned ScriptServerEngine::GetZoneSpinDownDelayMinutes(const ZoneIndex& zoneIndex) {
	std::lock_guard<std::mutex> lock(m_zoneInfoMapMutex);

	auto itr = m_zoneInfoMap.find(zoneIndex);
	if (itr != m_zoneInfoMap.end()) {
		return itr->second.m_spinDownDelayMinutes;
	} else {
		assert(false);
		LogError(zoneIndex.ToStr() << " - Zone info not found");
		return m_defaultSpinDownDelayMinutes;
	}
}

void ScriptServerEngine::SetZoneFinalDepartureTime(const ZoneIndex& zoneIndex, TimeMs timestamp, bool ignoreIfNotFound) {
	std::lock_guard<std::mutex> lock(m_zoneInfoMapMutex);

	auto itr = m_zoneInfoMap.find(zoneIndex);
	if (itr != m_zoneInfoMap.end()) {
		itr->second.m_finalDepartureTime = timestamp;
	} else if (!ignoreIfNotFound) {
		assert(false);
		LogError(zoneIndex.ToStr() << " - Zone info not found");
	}
}

void ScriptServerEngine::GetAllExpiredZones(std::set<ZoneIndex>& zones) {
	std::lock_guard<std::mutex> lock(m_zoneInfoMapMutex);

	for (auto itr = m_zoneInfoMap.begin(); itr != m_zoneInfoMap.end();) {
		bool expired = false;

		if (itr->second.m_finalDepartureTime != 0) {
			// Delayed spin-down
			assert(itr->second.m_population == 0);
			if (itr->second.m_population != 0) {
				LogError(itr->first.ToStr() << " - finalDepartureTime set but population is *NOT* zero");
			} else if (fTime::ElapsedMs(itr->second.m_finalDepartureTime) > itr->second.m_spinDownDelayMinutes * SEC_PER_MIN * MS_PER_SEC) {
				LogInfo(itr->first.ToStr() << " - expired");
				zones.insert(itr->first);
				expired = true;
			}
		}

		if (expired) {
			itr = m_zoneInfoMap.erase(itr);
		} else {
			++itr;
		}
	}
}

std::set<std::string> ScriptServerEngine::GetAllScriptFileNamesByType(ScriptServerEvent::UploadAssetTypes assetType) {
	std::set<std::string> filenames;

	std::string primarySourceDir, extension;
	std::vector<std::string> otherSourceDirs;
	if (!GetAllSourceDirsAndExtension(assetType, primarySourceDir, otherSourceDirs, extension)) {
		LogWarn("GetAllScriptFileNames failed unknown asset type " << assetType);
		return filenames;
	}

	GetScriptFileNamesFromFolder(primarySourceDir, extension, filenames);
	for (std::vector<std::string>::const_iterator i = otherSourceDirs.begin(); i != otherSourceDirs.end(); ++i) {
		GetScriptFileNamesFromFolder(*i, extension, filenames);
	}

	return filenames;
}

void ScriptServerEngine::GetScriptFileNamesFromFolder(const std::string& dir, const std::string& extension, std::set<std::string>& filenames) {
	WIN32_FIND_DATA find_data;
	HANDLE handle;

	std::string file_mask = dir;
	file_mask += "/*." + extension;

	bool done = false;
	handle = ::FindFirstFileW(Utf8ToUtf16(file_mask).c_str(), &find_data);

	if (INVALID_HANDLE_VALUE == handle)
		done = true;

	while (!done) {
		std::string filename = Utf16ToUtf8(find_data.cFileName);

		// don't add any temporary files
		// that were not cleaned up
		if ((filename.find("~$") == std::string::npos)) {
			filenames.insert(filename);
		}
		BOOL res = ::FindNextFile(handle, &find_data);
		if (!res)
			done = true;
	}
	::FindClose(handle);
}

void ScriptServerEngine::GetZonePermanentObjects(ZoneIndex zi, std::set<ULONG>& permObjs) {
	permObjs.clear();

	unsigned int zone_index = zi.getClearedInstanceId();
	unsigned int zone_instance_id = zi.GetInstanceId();

	if (zi.isArena()) //get template objects for all arenas
		zone_instance_id = 1;

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query objQuery = m_conn->query();
	objQuery << "SELECT obj_placement_id FROM dynamic_objects WHERE zone_index = %0 AND zone_instance_id = %1";
	objQuery.parse();

	LogTrace(objQuery.str(zone_index, zone_instance_id));
	StoreQueryResult objQueryResult = objQuery.store(zone_index, zone_instance_id);

	int num_rows = objQueryResult.num_rows();
	permObjs.insert(GameStateManager::ID_ENVIRONMENT); // For zone environment settings
	for (int i = 0; i < num_rows; i++) {
		permObjs.insert(objQueryResult.at(i).at(0));
	}
	END_TRY_SQL_LOG_ONLY(m_conn);
}

//
// LoadScriptFile
//
// Read full text from a disk file.
//
// @@param [IN]  fullPath
// @@param [OUT] text
//
bool ScriptServerEngine::LoadTextFile(const std::string& fullPath, std::string& text) {
	std::wstring fullPathW = Utf8ToUtf16(fullPath);

	HANDLE file = ::CreateFileW(fullPathW.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (file == INVALID_HANDLE_VALUE) {
		LogError("Cannot open: " << fullPath << ", err = " << GetLastError());
		return false;
	}
	char buffer[1024];
	DWORD bytesRead;
	text.clear();
	BOOL res = FALSE;
	while ((res = ReadFile(file, buffer, sizeof(buffer), &bytesRead, NULL)) == TRUE && bytesRead > 0) {
		text.append(buffer, bytesRead);
	}
	::CloseHandle(file);
	if (res == FALSE) {
		LogError("Error reading: " << fullPath);
		return false;
	}
	return true;
}

//
// LoadScriptFile
//
// Read lua script from a disk file. Automatically load custom script from AppSourceFolder if modifed by developer.
//
// @@param [IN]  fileName	- script relative path from (a) ScriptServerScript in SourceFolder or AppSourceFolder (if ReadOnlyFS), or (b) ScriptServerScript in ServerFolder (otherwise)
// @@param [OUT] sourceCode	- Lua source code read from file
//
bool ScriptServerEngine::LoadScriptFile(const char* fileName, std::string& sourceCode) {
	std::string fullPath;
	if (!GetScriptFullPathForRead(fileName, fullPath)) {
		LogError("[" << fileName << "] file not found [" << fullPath << "]");
		return false;
	}

	return LoadTextFile(fullPath, sourceCode);
}

void ScriptServerEngine::TransmitGameStateHandler(std::shared_ptr<GameStateManager> gsm, ZoneIndex zi, NETID netId) {
	if (!gsm->IsEmpty())
		m_taskSystem.QueueTask(new TransmitGameStateTask(netId, gsm, zi));
}

// IEventBlade overrides
bool ScriptServerEngine::RegisterEvents(IDispatcher* dispatcher, IEncodingFactory* factory) {
	jsAssert(dispatcher != NULL);

	// Event blade does not have local dispatcher instance. Make sure we make similar initialization calls as done in Dispatcher ctor.
	m_myDispatcher = dispatcher;
	IEvent::SetEncodingFactory(factory);

	RegisterEvent<ScriptServerEvent>(*dispatcher);
	RegisterEvent<ScriptClientEvent>(*dispatcher);
	RegisterEvent<UpdateScriptZoneSpinDownDelayEvent>(*dispatcher);
	return true;
}

bool ScriptServerEngine::RegisterEventHandlers(IDispatcher* dispatcher, IEncodingFactory* /*factory*/) {
	jsAssert(dispatcher != NULL && dispatcher == m_myDispatcher);

	// base dir for server script handler is gamefiles\serverscripts, so go up two levels
	std::string cfgFname = PathAdd(PathBase(), "..\\..\\ScriptServerEngine.xml");

	// set default base dir which is minimum we need from config
	// if config doesn't load we can run w/defaults
	SetPathBase(PathAdd(PathBase(), "..\\..").c_str(), IsBaseDirReadOnly());

	try {
		KEPConfig cfg;
		cfg.Load(cfgFname.c_str(), "EngineConfig");

		LoadConfigValues(&cfg);
	} catch (KEPException* e) {
		Logger logger(Logger::getInstance(L"ScriptServerEngine"));
		LOG4CPLUS_WARN(logger, "Failed to load ScriptServerEngine's event handling config file at \"" << cfgFname << "\" " << e->ToString());
		e->Delete();
		return false;
	}

	IGame* game = dispatcher->Game();
	if (game != 0 && game->EngineType() == eEngineType::Server) {
		jsAssert(m_dbConfig != NULL);
		if (m_dbConfig == NULL) {
			Logger logger(Logger::getInstance(L"ScriptServerEngine"));
			LOG4CPLUS_WARN(logger, "Failed to initialize event handler for ScriptServerEngine: DB connection not configured");
			return false;
		}

		Initialize(m_pathBase.c_str(), cfgFname.c_str(), *m_dbConfig);
	}

	return true;
}

void ScriptServerEngine::loadRestAPIRegistrations() {
	auto restAPIDataMap = m_globalVMConfig.getRestAPIDataMapRef();
	restAPIDataMap.clear();

	int gameId = getGameId();
	if (gameId == 0) {
		LogError(__FUNCTION__ "Unable to determine gameId of 3D App server");
		return;
	}

	if (m_conn->dbCommon() == NULL || *m_conn->dbCommon() == '\0') {
		LogWarn("DB_Common not defined in cfg. Skip loading from api_registrations table.");
		return;
	}

	// Update the game inventories sync table after the item was used successfully
	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();

	Query query = m_conn->query();

	query << "SELECT `grp`, `name`, `value` from `" << m_conn->dbCommon() << "`.`api_registrations` WHERE `game_id`=%0 ORDER BY `grp`, `name`";
	query.parse();

	LogTrace(query.str(gameId));
	StoreQueryResult res = query.store(gameId);

	std::string prevGroup, prevName;

	for (size_t i = 0; i < res.num_rows(); i++) {
		std::string group = res[i]["grp"];
		std::string name = res[i]["name"];
		std::string value = res[i]["value"];

		restAPIDataMap[group][name] = value;
	}

	END_TRY_SQL_LOG_ONLY(m_conn);
}

void ScriptServerEngine::PlayerSetParticleCallback(void* vm,
	ULONG clientId, GLID glid, const std::string& boneName, int particleSlotId,
	double offsetX, double offsetY, double offsetZ,
	double dirX, double dirY, double dirZ,
	double upX, double upY, double upZ) {
	ASSERT_VM_SS();

	// Send a ScriptClientEvent to the client.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSetParticle, 0);
	(*sce->OutBuffer()) << ss->GetZoneId().toLong()
						<< clientId << glid << boneName << particleSlotId
						<< (float)offsetX << (float)offsetY << (float)offsetZ
						<< (float)dirX << (float)dirY << (float)dirZ
						<< (float)upX << (float)upY << (float)upZ;
	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		IEvent* libe = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
		SendToClient(libe);
	} else {
		SendToClient(sce);
	}
}

void ScriptServerEngine::PlayerRemoveParticleCallback(void* vm, ULONG clientId, const std::string& boneName, int particleSlotId) {
	ASSERT_VM_SS();

	// Send a ScriptClientEvent to the client.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerRemoveParticle, 0);
	(*sce->OutBuffer()) << ss->GetZoneId().toLong() << clientId << boneName << particleSlotId;
	SendToClient(sce);
}

void ScriptServerEngine::PlayerLookAtCallback(void* vm,
	ULONG clientId, int targetType, ULONG targetId, const std::string& targetBone,
	double offsetX, double offsetY, double offsetZ,
	bool followTarget, bool cancelableByPlayer) {
	ASSERT_VM_SS();

	// Send a ScriptClientEvent to the client.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerLookAt, clientId, ScriptClientEvent::Scope_Player);
	(*sce->OutBuffer())
		<< targetType << targetId << targetBone
		<< (float)offsetX << (float)offsetY << (float)offsetZ
		<< followTarget << cancelableByPlayer;

	SendToClient(sce);
}

void ScriptServerEngine::ObjectLookAtCallback(void* vm,
	ULONG objectId, int targetType, ULONG targetId, const std::string& targetBone,
	double offsetX, double offsetY, double offsetZ,
	bool followTarget, bool allowXZRotation,
	double forwardX, double forwardY, double forwardZ) {
	ASSERT_VM_SS();

	// Send a ScriptClientEvent to the client.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectLookAt, 0, ScriptClientEvent::Scope_Zone);
	(*sce->OutBuffer()) << ss->GetZoneId().toLong()
						<< objectId << targetType << targetId << targetBone
						<< (float)offsetX << (float)offsetY << (float)offsetZ
						<< followTarget << allowXZRotation
						<< (float)forwardX << (float)forwardY << (float)forwardZ;

	if (true /*bufferCall*/) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objectId);
	}

	SendToClient(sce);
}

//returns true if the player has the minPermission or better (lower number)
bool ScriptServerEngine::CheckPlayerPermissions(NETID clientNetId, int minPermission, std::string caller, bool failSilently) {
	if (caller.empty())
		caller = "unknown";

	NETIDPlayerInfoMap::iterator player = m_players.find(clientNetId);
	if (player != m_players.end()) {
		if (player->second.m_permissions <= minPermission) {
			return true;
		}

		if (!failSilently) {
			LogWarn("Insufficient permissions " << player->second.m_name << " for " << caller);
		}
	} else {
		LogWarn("Unknown player " << clientNetId << " for " << caller);
	}

	return false;
}

// should be in .../bin/luac.exe
static std::string PathLuaCompiler() {
	static std::string path;
	if (path.empty())
		path = PathFind(PathApp(), "luac.exe");
	return path;
}

void ScriptServerEngine::LoadDevToolsFolders() {
	std::string distroInfoPath = PathAdd(PathBase(), DISTRO_INFO_FILENAME);
	try {
		std::string serverDir, sourceDir, patchDir, appServerDir, appSourceDir, appPatchDir;

		if (!ReadDistributionInfo(distroInfoPath, serverDir, sourceDir, patchDir, appServerDir, appSourceDir, appPatchDir, m_devToolsAppPatchUrl, m_devToolsAddServerUrl)) {
			LogWarn("Dev Tools: Unable to determine distro folders.");
		}

		// Make all paths absolute
		m_devToolsServerFolder = GetAbsolutePath(serverDir, PathBase());
		m_devToolsSourceFolder = GetAbsolutePath(sourceDir, PathBase());
		m_devToolsPatchFolder = GetAbsolutePath(patchDir, PathBase());
		m_devToolsAppServerFolder = GetAbsolutePath(appServerDir, PathBase());
		m_devToolsAppSourceFolder = GetAbsolutePath(appSourceDir, PathBase());
		m_devToolsAppPatchFolder = GetAbsolutePath(appPatchDir, PathBase());

		m_globalVMConfig.setScriptRootDir(PathAdd(m_devToolsServerFolder, PathAdd(GAMEFILES, SCRIPTSERVERDIR)));

	} catch (KEPException* e) {
		LogWarn("Dev Tools: " << e->m_msg);
		delete e;
	} catch (KEPException& e) {
		LogWarn("Dev Tools: " << e.m_msg);
	}

	// try figuring out luac.exe path
	m_devToolsLuaCompiler.clear();

	// DRF - Using PathFind()
	m_devToolsLuaCompiler = PathLuaCompiler();
	LogInfo("pathLuaCompiler=" << m_devToolsLuaCompiler);

	if (m_devToolsLuaCompiler.empty()) {
		// fall back to Program Files just in case (for incubator backward compatibility)
		wchar_t program_files[MAX_PATH];
		m_devToolsLuaCompiler = std::string("C:\\Program Files (x86)");
		HRESULT gotten = SHGetFolderPathW(0, CSIDL_PROGRAM_FILES, NULL, 0, program_files);
		if (gotten == S_OK) {
			m_devToolsLuaCompiler = Utf16ToUtf8(program_files);
		}

		m_devToolsLuaCompiler = m_devToolsLuaCompiler + "\\Kaneva\\Kaneva Platform\\bin\\luac.exe";
	}

	if (m_devToolsLuaCompiler.empty()) {
		LogWarn("Dev Tools: Unable to determine the full path for Luac.exe");
	} else {
		LogInfo("Dev Tools: Luac path [" << m_devToolsLuaCompiler.c_str() << "]");
	}
}

void ScriptServerEngine::ObjectSetMouseOverCallback(
	void* vm,
	int objType,
	ULONG objId,
	int mosMode,
	const std::string& tooltipStr,
	int cursorType,
	int mosCategory,
	bool bufferCall) {
	ASSERT_VM_SS();

	// clientId of 0 is ok, because this will be processed by the game server.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::ObjectSetMouseOver, 0);
	*sce->OutBuffer() << ss->GetZoneId().toLong() << objType << objId << mosMode << tooltipStr << cursorType << mosCategory;

	if (bufferCall) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, objId);
	}

	SendToClient(sce);
}

void ScriptServerEngine::PlayerAddIndicatorCallback(
	void* vm,
	ULONG clientId,
	int type,
	const std::string& id,
	double offsetX, double offsetY, double offsetZ,
	int glid, double speed, double moveY) {
	ASSERT_VM_SS();

	// clientId of 0 is ok, because this will be processed by the game server.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerAddIndicator, clientId, ScriptClientEvent::Scope_Player);
	*sce->OutBuffer() << type << id
					  << (float)offsetX << (float)offsetY << (float)offsetZ
					  << glid << (float)speed << (float)moveY;

	SendToClient(sce);
}

void ScriptServerEngine::PlayerClearIndicatorsCallback(
	void* vm,
	ULONG clientId,
	int type,
	const std::string& id) {
	ASSERT_VM_SS();

	// clientId of 0 is ok, because this will be processed by the game server.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerClearIndicators, clientId, ScriptClientEvent::Scope_Player);
	*sce->OutBuffer() << type << id;

	SendToClient(sce);
}

// GetScriptFullPathForRead
//
// Determine script full path from relative path. The full path is either based on (a) AppServerFolder/ServerFolder (if ReadOnlyFS),
// or (b) ServerFolder (otherwise).
//
// @@param relativePath		- relative path of a script.
// @@param fullPathScript	- full path to script
//
// @@return true if script exists.
bool ScriptServerEngine::GetScriptFullPathForRead(const std::string& relativePath, std::string& fullPathScript) {
	// Init out parameter
	fullPathScript.clear();

	if (!IsBaseDirReadOnly()) {
		// Unmanaged app or WOK (hopefully)
		return getScriptFullPathUnmanaged(relativePath, fullPathScript);
	}

	// Managed/hosted app
	std::string relFromRoot = PathAdd(PathAdd(GAMEFILES, SCRIPTSERVERDIR), relativePath);
	bool isDir = false;
	bool scriptExists = false;

	// First check AppServerFolder if configured
	if (!m_devToolsAppServerFolder.empty()) {
		// Check if file exists
		std::string path = PathAdd(m_devToolsAppServerFolder, relFromRoot);
		if (jsFileExists(path.c_str(), &isDir) && !isDir) {
			scriptExists = true;
			fullPathScript = path;
		}
	}

	// If not found under app directory, try shared ServerDir for script
	if (fullPathScript.empty() && !m_devToolsServerFolder.empty()) {
		std::string path = PathAdd(m_devToolsServerFolder, relFromRoot);
		if (jsFileExists(path.c_str(), &isDir) && !isDir) {
			scriptExists = true;
			fullPathScript = path;
		}
	}

	// If script not found in either location. Return AppServerDir for script path anyway.
	if (fullPathScript.empty() && !m_devToolsAppServerFolder.empty()) {
		fullPathScript = PathAdd(m_devToolsAppServerFolder, relFromRoot);
	}

	return scriptExists;
}

//
// GetScriptFullPathForWrite
//
// Determine script full path from relative path. The full path is either based on (a) AppServerFolder/ServerFolder (if ReadOnlyFS),
// or (b) ServerFolder (otherwise).
//
// @@param relativePath		- relative path of a script.
// @@param fullPathScript	- full path to script
//
// @@return true if script exists.
//
bool ScriptServerEngine::GetScriptFullPathForWrite(const std::string& relativePath, std::string& fullPathScript) {
	if (!IsBaseDirReadOnly()) {
		// Normal app or WOK (hopefully)
		return getScriptFullPathUnmanaged(relativePath, fullPathScript);
	}

	// Hosted app or similar
	std::string relFromRoot = PathAdd(PathAdd(GAMEFILES, SCRIPTSERVERDIR), relativePath);
	bool isDir = false;
	bool scriptExists = false;

	// AppServerDir
	if (!m_devToolsAppServerFolder.empty()) {
		fullPathScript = PathAdd(m_devToolsAppServerFolder, relFromRoot);
		scriptExists = jsFileExists(fullPathScript.c_str(), &isDir) && !isDir;
	}

	return scriptExists;
}

bool ScriptServerEngine::getScriptFullPathUnmanaged(const std::string& relativePath, std::string& fullPathScript) {
	// Init out parameters
	fullPathScript.clear();

	std::string relFromRoot = PathAdd(PathAdd(GAMEFILES, SCRIPTSERVERDIR), relativePath);
	bool isDir = false;
	bool scriptExists = false;

	if (!m_devToolsServerFolder.empty()) {
		std::string path = PathAdd(m_devToolsServerFolder, relFromRoot);
		if (jsFileExists(path.c_str(), &isDir) && !isDir) {
			scriptExists = true;
			fullPathScript = path;
		}
	}

	if (fullPathScript.empty()) {
		// use m_scriptDir.
		// m_scriptDir should already include GameFiles\ScriptServerScripts. Only postfix with relativePath.
		fullPathScript = PathAdd(m_scriptDir, relativePath);
		scriptExists = jsFileExists(fullPathScript.c_str(), &isDir) && !isDir;
	}

	return scriptExists;
}

//
// GetPublishingInfo
//
// Determine publishing information.
//
// @@param assetType	- Asset type (see UploadAssetType)
// @@param baseName		- Asset base name (without extension)
// @@param sourceDir	- Return full path to the source directory (where uploaded file is)
// @@param destDir		- Return full path to the destination directory (serverscript or patch)
// @@param fileName		- Return file name with extension
// @@param addToPatch		- Return true if need to add asset to patch
// @@param patchPrefix	- Return client patch folder such as "Menus" or "MenuScripts"
// @@param compileLua	- Return true if asset is a lua file and requires compiling
//
// @@return true if asset type is recognized and valid publishing info returned.
//
bool ScriptServerEngine::GetPublishingInfo(ScriptServerEvent::UploadAssetTypes assetType, const std::string& baseName,
	std::string& sourceDir, std::string& destDir, std::string& fullName,
	bool& addToPatch, std::string& patchPrefix, bool& compileLua) {
	// Init out parameters
	sourceDir.clear();
	destDir.clear();
	fullName.clear();
	addToPatch = false;
	patchPrefix.clear();
	compileLua = false;

	std::string extension;
	if (!GetSourceDirAndExtension(assetType, "", sourceDir, extension)) {
		LogError("Unable to determine source folder or extension for [" << baseName << "], type = " << assetType);
		return false;
	}
	fullName = baseName + "." + extension;

	// Determine patch root and server root for publishing
	std::string patchRoot, serverRoot;
	if (IsBaseDirReadOnly()) {
		patchRoot = m_devToolsAppPatchFolder;
		serverRoot = m_devToolsAppServerFolder;
	} else {
		patchRoot = IF_STR_EMPTY(m_devToolsAppPatchFolder, m_devToolsPatchFolder);
		serverRoot = IF_STR_EMPTY(m_devToolsAppServerFolder, IF_STR_EMPTY(m_devToolsServerFolder, m_scriptDir));
	}

	if (patchRoot.empty() || serverRoot.empty()) {
		jsAssert(false);
		LogError("Unable to determine patch destination folder. Bad configuration?");
		return false;
	}

	// Determine publishing actions
	switch (assetType) {
		case ScriptServerEvent::ServerScript:
		case ScriptServerEvent::SavedScript: // if serverRoot is empty, use empty for destDir
			destDir = PathAdd(serverRoot.c_str(), PathAdd(GAMEFILES, PathAdd(SCRIPTSERVERDIR, SAVEDSCRIPTDIR))) + "\\";
			break;
		case ScriptServerEvent::ActionItem:
		case ScriptServerEvent::ActionItemParams:
			destDir = PathAdd(serverRoot.c_str(), PathAdd(GAMEFILES, PathAdd(SCRIPTSERVERDIR, ACTIONSCRIPTDIR))) + "\\";
			break;
		case ScriptServerEvent::ActionInstance:
		case ScriptServerEvent::ActionInstanceParams:
			destDir = PathAdd(serverRoot.c_str(), PathAdd(GAMEFILES, PathAdd(SCRIPTSERVERDIR, ACTIONINSTANCEDIR))) + "\\";
			break;

		case ScriptServerEvent::MenuScript:
			destDir = patchRoot;
			addToPatch = true;
			compileLua = true;
			break;

		case ScriptServerEvent::MenuXml:
		case ScriptServerEvent::MenuDDS:
		case ScriptServerEvent::MenuTGA:
		case ScriptServerEvent::MenuBMP:
		case ScriptServerEvent::MenuPNG:
		case ScriptServerEvent::MenuJPG:
		case ScriptServerEvent::MenuJPEG:
			destDir = patchRoot;
			addToPatch = true;
			break;

		default:
			jsAssert(false);
			LogError("Unsupported asset type " << assetType << " for [" << baseName << "]");
			return false;
	}

	if (addToPatch) {
		switch (assetType) {
			case ScriptServerEvent::MenuXml:
			case ScriptServerEvent::MenuDDS:
			case ScriptServerEvent::MenuTGA:
			case ScriptServerEvent::MenuBMP:
			case ScriptServerEvent::MenuPNG:
			case ScriptServerEvent::MenuJPG:
			case ScriptServerEvent::MenuJPEG:
				patchPrefix = MENUSDIR;
				break;
			case ScriptServerEvent::MenuScript:
				patchPrefix = MENUSCRIPTSDIR;
				break;
			default:
				jsAssert(false);
				LogError("Unsupported patch asset type " << assetType << " for [" << baseName << "]");
				return false;
		}
	}

	return true;
}

ScriptServerEvent::UploadAssetTypes ScriptServerEngine::GetAssetTypeByRelPath(const std::string& relPath) {
	if (STLBeginWithIgnoreCase(relPath, "ActionItems\\")) {
		if (STLHasExtension(relPath, "json")) {
			return ScriptServerEvent::ActionItemParams;
		}

		jsAssert(STLHasExtension(relPath, "lua"));
		return ScriptServerEvent::ActionItem;
	}

	if (STLBeginWithIgnoreCase(relPath, "ActionInstances\\")) {
		if (STLHasExtension(relPath, "json")) {
			return ScriptServerEvent::ActionInstanceParams;
		}

		jsAssert(STLHasExtension(relPath, "lua"));
		return ScriptServerEvent::ActionInstance;
	}

	if (STLBeginWithIgnoreCase(relPath, "SavedScripts\\")) {
		jsAssert(STLHasExtension(relPath, "lua"));
		return ScriptServerEvent::SavedScript;
	}

	return ScriptServerEvent::UndefinedAssetType;
}

//
// RegisterAppPatchURL
//
// Blocking function to register app patch URL in wok
// It should be called from a worker thread like PublishTask
//
bool ScriptServerEngine::RegisterAppPatchURL() {
	if (m_patchURLRegistered) {
		return true;
	}

	if (m_devToolsAddServerUrl.empty()) {
		LogError("add_server_url not defined in distributioninfo.xml");
		return false;
	}

	int gameId = getGameId();
	if (gameId == 0) {
		LogError("Unable to determine gameId of 3D App server");
		return false;
	}

	std::stringstream ssGameId;
	ssGameId << gameId;

	std::string url(m_devToolsAddServerUrl); // same format as NEW_SERVER_URL except username and password already filled
	STLStringSubstitute(url, "{0}", ssGameId.str()); // Game ID
	STLStringSubstitute(url, "{1}", ""); // Server
	STLStringSubstitute(url, "{2}", m_devToolsAppPatchUrl); // Patch URL

	if (url.find("{3}") != std::string::npos || url.find("{4}") != std::string::npos) {
		//Invalid URL
		LogError("invalid add_server_url missing login credential");
	}

	std::string resultXML;
	DWORD httpStatusCode = 0;
	std::string httpStatusDescription;
	size_t resultSize = 0;
	if (!GetBrowserPage(url, resultXML, resultSize, httpStatusCode, httpStatusDescription) || (httpStatusCode != 200)) {
		LogWarn("Error calling web: [" << url << "] HTTP " << httpStatusCode << ": " << httpStatusDescription);
		return false;
	}

	KEPConfig cfg;
	if (!cfg.Parse(resultXML.c_str(), "Result")) {
		LogWarn("FAILED cfg.Parse()");
		return false;
	}
	int returnCode = cfg.Get("ReturnCode", -1);
	if (returnCode != 0) {
		LogWarn("Error registering patch URL. Server returned " << resultXML);
		return false;
	}

	LogInfo("Successfully registered patch URL [" << m_devToolsAppPatchUrl << "]");
	m_patchURLRegistered = true;
	return true;
}

bool ScriptServerEngine::PlayerClosePresetMenu(
	void* vm,
	ULONG clientId,
	const std::string& menuName) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerClosePresetMenu, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << menuName;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerMovePresetMenu(
	void* vm,
	ULONG clientId,
	const std::string& menuName,
	double x, double y) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerMovePresetMenu, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << menuName << (int)x << (int)y;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerOpenYesNoMenu(
	void* vm,
	ULONG clientId,
	const std::string& titleStr,
	const std::string& descStr,
	const std::string& yesStr,
	const std::string& noStr,
	const std::string& idStr) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerOpenYesNoMenu, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << titleStr << descStr << yesStr << noStr << idStr;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerOpenKeyListenerMenu(
	void* vm,
	ULONG clientId,
	const std::string& keysStr,
	const std::string& idStr) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerOpenKeyListenerMenu, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << keysStr << idStr;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerOpenTextTimerMenu(
	void* vm,
	ULONG clientId,
	const std::string& textStr,
	int timeInSec,
	const std::string& idStr) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerOpenTextTimerMenu, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << textStr << timeInSec << idStr;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerOpenButtonMenu(
	void* vm,
	ULONG clientId,
	const std::string& titleStr,
	const std::string& descStr,
	const std::string& btnStr,
	const std::string& idStr) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerOpenButtonMenu, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << titleStr << descStr << btnStr << idStr;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerOpenStatusMenu(
	void* vm,
	ULONG clientId,
	const std::string& titleStr,
	const std::string& statusStr,
	const std::string& idStr) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerOpenStatusMenu, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << titleStr << statusStr << idStr;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerOpenListBoxMenu(
	void* vm,
	ULONG clientId,
	const std::string& titleStr,
	const std::string& listStr,
	const std::string& btnStr,
	const std::string& idStr) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerOpenListBoxMenu, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << titleStr << listStr << btnStr << idStr;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerOpenShopMenu(
	void* vm,
	ULONG clientId,
	const std::string& titleStr,
	const std::string& idStr) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerOpenShop, clientId, ScriptClientEvent::Scope_Player);
	*sce->OutBuffer() << ss->GetPlacementId() << titleStr << idStr;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerAddItemToShopMenu(
	void* vm,
	ULONG clientId,
	int objId,
	const std::string& itemNameStr,
	int cost,
	const std::string& iconStr,
	const std::string& idStr) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerAddItemToShop, clientId, ScriptClientEvent::Scope_Player);
	*sce->OutBuffer() << ss->GetPlacementId() << objId << itemNameStr << cost << iconStr << idStr;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerUpdateCoinHUD(
	void* vm,
	ULONG clientId,
	const std::string& typeStr,
	int value) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerUpdateCoinHUD, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << typeStr << value;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerUpdateHealthHUD(
	void* vm,
	ULONG clientId,
	const std::string& typeStr,
	const std::string& targetStr,
	int health,
	int healthChange) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerUpdateHealthHUD, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << typeStr << targetStr << health << healthChange;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerUpdateXPHUD(
	void* vm,
	ULONG clientId,
	int level,
	int levelXP,
	int xpToNextLevel) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerUpdateXPHUD, clientId);
	(*sce->OutBuffer()) << ss->GetPlacementId() << level << levelXP << xpToNextLevel;
	SendToClient(sce);
	return true;
}

void ScriptServerEngine::PlayerAddHealthIndicator(
	void* vm,
	ULONG clientId,
	int type,
	const std::string& idStr,
	int healthPercentage) {
	ASSERT_VM_SS();
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerAddHealthIndicator, clientId, ScriptClientEvent::Scope_Player);
	*sce->OutBuffer() << ss->GetPlacementId() << type << idStr << healthPercentage;
	SendToClient(sce);
}

void ScriptServerEngine::PlayerRemoveHealthIndicator(
	void* vm,
	ULONG clientId,
	int type,
	const std::string& idStr) {
	ASSERT_VM_SS();
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerRemoveHealthIndicator, clientId, ScriptClientEvent::Scope_Player);
	*sce->OutBuffer() << ss->GetPlacementId() << type << idStr;
	SendToClient(sce);
}

bool ScriptServerEngine::PlayerShowStatusInfo(
	void* vm,
	ULONG clientId,
	const std::string& messageStr,
	int timeMs,
	int r, int g, int b) {
	ASSERT_VM_SS(false);
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerShowStatusInfo, clientId, ScriptClientEvent::Scope_Player);
	*sce->OutBuffer() << ss->GetPlacementId() << messageStr << timeMs << r << g << b;
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::PlayerSpawnVehicle(
	void* vm,
	ULONG clientId,
	int placementId,
	const Vector3f& offset,
	const Quaternionf& orientation,
	const Vector3f& dimensions,
	float speed, float hp,
	float loadSusp, float unloadSusp,
	float mass,
	float wheelFriction,
	Vector2f jumpVelocityMPS,
	GLID glidThrottle,
	GLID glidSkid) {
	ASSERT_VM_SS(false);

	// Create an event to send to all users currently in the zone.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSpawnVehicle, 0);
	*sce->OutBuffer() << ss->GetZoneId()
					  << placementId
					  << offset
					  << orientation
					  << dimensions
					  << speed << hp
					  << loadSusp << unloadSusp
					  << mass
					  << wheelFriction
					  << jumpVelocityMPS
					  << glidThrottle
					  << glidSkid
					  << clientId;

	// Store the game state of this event so that users entering the zone later will know about this.
	ScriptClientEvent* e = new ScriptClientEvent(sce);
	ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, (ULONG)placementId);

	// Send the primary event out to all users in the zone.
	SendToClient(sce);
	return true;
}

bool ScriptServerEngine::playerModifyVehicle(
	ULONG clientId,
	float maxSpeedKPH,
	float horsepower,
	Vector2f jumpVelocityMPS) {
	IServerEngine* pServerEngine = IServerEngine::GetInstance();
	IPlayer* pPlayer = pServerEngine->GetPlayerByNetId(clientId, true);
	if (!pPlayer)
		return false;

	ScriptClientEvent* e = new ScriptClientEvent(ScriptClientEvent::PlayerModifyVehicle, SERVER_NETID);
	*e->OutBuffer()
		<< maxSpeedKPH
		<< horsepower
		<< jumpVelocityMPS
		<< pPlayer->GetNetTraceId();

	SendToClient(e);
	return true;
}

bool ScriptServerEngine::PlayerLeaveVehicle(lua_State* vm, ULONG clientId, int placementId) {
	ASSERT_VM_SS(false);

	// Create and event to send to all users currently in the zone.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerLeaveVehicle, 0);
	*sce->OutBuffer() << ss->GetZoneId() << placementId << clientId;

	// Remove the old event in the game state system.
	ss->GetGameStateManager()->RemoveGameStateObjectModifier(ss->GetZoneId(), (ULONG)placementId, ScriptClientEvent::PlayerSpawnVehicle);

	// Send the primary event out to all users in the zone.
	SendToClient(sce);
	return true;
}

void ScriptServerEngine::PlayerSetCustomTextureOnAccessoryCallback(
	void* vm,
	ULONG clientId,
	GLID glid,
	const std::string& urlStr) {
	ASSERT_VM_SS();
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSetCustomTextureOnAccessory, 0);
	(*sce->OutBuffer()) << clientId << (ULONG)glid << urlStr;
	if (IS_VALID_GLID(glid) && IServerEngine::GetInstance()->checkGlid(glid) == IServerEngine::NotLoaded) {
		std::vector<GLID> glids;
		glids.push_back(glid);
		IEvent* libe = IServerEngine::GetInstance()->createLoadGlidEvent(glids, sce);
		SendToClient(libe);
	} else {
		SendToClient(sce);
	}
}

int ScriptServerEngine::getGameId() {
	// but if we ARE a blade (as we are currently), I need to do this
	IGame* game = m_myDispatcher->Game();
	IEngine* engine = dynamic_cast<IEngine*>(game);
	if (engine) {
		return engine->GetGameId();
	}

	return 0;
}

bool ScriptServerEngine::DictionaryGenerate(const char* name, HDICT& hDict, fast_recursive_mutex*& hDictMutex) {
	jsVerifyReturn(name != NULL, false);

	// Global lock is acquired for accessing Dictionary::m_numRef and m_hMutex, as well as manipulation of the global dictionary map
	// (affected functions: DictionaryGenerate/DictionaryGetRefCount/DictionaryAddRef/DictionaryRelease). In other cases, we have direct
	// access to the mutex of an existing dictionary and the dictionary itself. So there is no need to acquire global lock.
	std::lock_guard<fast_recursive_mutex> globalLock(m_dictionaryMapSync);

	DictionaryMap::iterator it = m_dictionaryMap.find(name);
	if (it != m_dictionaryMap.end()) {
		// Already created
		LogDebug("name=" << name << " - already created, increment reference instead");

		// m_numRef and m_hMutex are protected by global lock
		it->second->addRef();
		hDict = (HDICT)it->second;
		hDictMutex = &it->second->sync();
		return true;
	}

	// Create new (constructor sets m_numRef to 1)
	ILuaSharedDataProvider* dict = new LuaSharedDictionary();
	hDict = (HDICT)dict;
	hDictMutex = &dict->sync();

	// Insert into global map at the last step so we don't have lock it
	m_dictionaryMap.insert(std::make_pair(name, dict));
	return true;
}

int ScriptServerEngine::DictionaryGetValue(const DictUserData* udDict, const char* key, void* buffer, size_t bufSize) {
	jsVerifyReturn(udDict && udDict->IsValid() && key && buffer != NULL, LUA_TNONE);

	// vtable ptr for sanity check
	void** ppVT = (void**)buffer;

	std::lock_guard<fast_recursive_mutex> lock(*udDict->GetSync()); // Lock dictionary

	ILuaSharedDataProvider* dict = udDict->GetDict();
	int type = dict->get(key, buffer, bufSize);

	if (m_logger.getLogLevel() <= DEBUG_LOG_LEVEL && *ppVT != NULL) {
		const char* name = udDict->GetName();

		ILuaAdapter* value = (ILuaAdapter*)buffer;
		LuaPrimValue<double>* nVal = NULL;
		LuaPrimValue<std::string>* sVal = NULL;
		LuaPrimValue<bool>* bVal = NULL;
		ILuaStruct* luaStruct = NULL;

		switch (type) {
			case LUA_TNUMBER:
				jsVerify((nVal = dynamic_cast<LuaPrimValue<double>*>(value)) != nullptr);
				LogDebug("name=[" << name << "], key=[" << key << "]: returned " << (nVal != NULL ? nVal->val : 0));
				break;
			case LUA_TSTRING:
				jsVerify((sVal = dynamic_cast<LuaPrimValue<std::string>*>(value)) != nullptr);
				LogDebug("name=[" << name << "], key=[" << key << "]: returned \"" << (sVal != NULL ? sVal->val : "ERR") << "\"");
				break;
			case LUA_TBOOLEAN:
				jsVerify((bVal = dynamic_cast<LuaPrimValue<bool>*>(value)) != nullptr);
				LogDebug("name=[" << name << "], key=[" << key << "]: returned " << (bVal != NULL ? bVal->val ? "true" : "false" : "ERR"));
				break;
			case LUA_TUSERDATA:
				jsVerify((sVal = dynamic_cast<LuaPrimValue<std::string>*>(value)) != nullptr);
				LogDebug("name=[" << name << "], key=[" << key << "]: returned @{" << (sVal != NULL ? sVal->val : "ERR") << "}");
				break;
			case LUA_TTABLE:
				jsAssert((luaStruct = dynamic_cast<ILuaStruct*>(value)) != nullptr);
				LogDebug("name=[" << name << "], key=[" << key << "]: returned a {" << (luaStruct != NULL ? luaStruct->typeName() : "ERR") << "}");
				break;
			case LUA_TNIL:
				jsAssert(dynamic_cast<LuaNil*>(value) != nullptr);
				LogDebug("name=[" << name << "], key=[" << key << "]: NOT FOUND");
				break;
			default:
				LogDebug("name=[" << name << "], key=[" << key << "]: unknown return type " << type);
				break;
		}
	}

	return type;
}

bool ScriptServerEngine::DictionarySetValue(const DictUserData* udDict, const char* key, lua_State* vm, int index) {
	jsVerifyReturn(udDict != NULL && udDict->IsValid() && key != NULL && vm != NULL, false);

	const char* name = udDict->GetName();
	LogDebug("name=[" << name << "], key=[" << key << "] val index=" << index);

	std::string childRemoved;
	{
		std::lock_guard<fast_recursive_mutex> lock(*udDict->GetSync()); // Lock dictionary

		ILuaSharedDataProvider* dict = udDict->GetDict();
		dict->set(key, vm, index, childRemoved);
	}

	if (!childRemoved.empty()) {
		DictionaryRelease(childRemoved.c_str());
	}
	return true;
}

bool ScriptServerEngine::DictionarySetChildTableValue(const DictUserData* udDict, const char* key, const char* val) {
	jsVerifyReturn(udDict && udDict->IsValid() && key && val && *val, false);
	const char* name = udDict->GetName();

	LogDebug("name=[" << name << "], key=[" << key << "] val={" << val << "}");

	// Add reference to child dictionary
	DictionaryAddRef(val);

	std::string childRemoved;
	{
		std::lock_guard<fast_recursive_mutex> lock(*udDict->GetSync()); // Lock dictionary

		ILuaSharedDataProvider* dict = udDict->GetDict();
		dict->set(key, ILuaSharedDataProvider::ChildRef(val), childRemoved);
	}

	if (!childRemoved.empty()) {
		DictionaryRelease(childRemoved.c_str());
	}
	return true;
}

bool ScriptServerEngine::DictionaryRemoveValue(const DictUserData* udDict, const char* key) {
	jsVerifyReturn(udDict && udDict->IsValid() && key, false);
	const char* name = udDict->GetName();

	LogDebug("name=[" << name << "], key=[" << key << "]");

	std::string childRemoved;
	{
		std::lock_guard<fast_recursive_mutex> lock(*udDict->GetSync()); // Lock dictionary

		ILuaSharedDataProvider* dict = udDict->GetDict();
		dict->remove(key, childRemoved);
	}

	if (!childRemoved.empty()) {
		DictionaryRelease(childRemoved.c_str());
	}
	return true;
}

bool ScriptServerEngine::DictionaryClear(const DictUserData* udDict) {
	jsVerifyReturn(udDict && udDict->IsValid(), false);
	const char* name = udDict->GetName();
	LogDebug("name=" << name);

	std::vector<std::string> childrenToDeref;
	{
		std::lock_guard<fast_recursive_mutex> lock(*udDict->GetSync()); // Lock dictionary

		ILuaSharedDataProvider* dict = udDict->GetDict();
		dict->clear(childrenToDeref);
	}

	// Dereference all child dictionaries
	for (auto itDictVal = childrenToDeref.begin(); itDictVal != childrenToDeref.end(); ++itDictVal) {
		DictionaryRelease(itDictVal->c_str());
	}

	return true;
}

bool ScriptServerEngine::DictionaryAddRef(const char* name) {
	jsVerifyReturn(name != NULL, false);
	LogDebug("name=" << name);

	std::lock_guard<fast_recursive_mutex> globalLock(m_dictionaryMapSync); // Then lock the global list to retrieve the dictionary

	DictionaryMap::iterator itMap = m_dictionaryMap.find(name);
	if (itMap == m_dictionaryMap.end()) {
		return false;
	}

	ILuaSharedDataProvider* dict = itMap->second;
	dict->addRef(); // protected by global lock
	return true;
}

bool ScriptServerEngine::DictionaryRelease(const char* name) {
	jsVerifyReturn(name != NULL, false);
	LogDebug("name=" << name);

	std::unique_lock<fast_recursive_mutex> globalLock(m_dictionaryMapSync); // Then lock the global list to retrieve the dictionary

	DictionaryMap::iterator itMap = m_dictionaryMap.find(name);
	if (itMap == m_dictionaryMap.end()) {
		return false;
	}

	ILuaSharedDataProvider* dict = itMap->second;

	std::vector<std::string> childrenToDeref;
	if (dict->release(childrenToDeref)) { // protected by global lock
		// Dispose it
		m_dictionaryMap.erase(itMap);
		delete dict;

		// Give back global lock to avoid any possible dead locking while releasing references to child dictionaries
		globalLock.unlock();

		// Dereference all child dictionaries
		for (auto itDictVal = childrenToDeref.begin(); itDictVal != childrenToDeref.end(); ++itDictVal) {
			DictionaryRelease(itDictVal->c_str());
		}
	}
	return true;
}

int ScriptServerEngine::DictionaryGetRefCount(const DictUserData* udDict) {
	jsVerifyReturn(udDict && udDict->IsValid(), false);
	const char* name = udDict->GetName();
	LogDebug("name=" << name);

	std::lock_guard<fast_recursive_mutex> globalLock(m_dictionaryMapSync);

	ILuaSharedDataProvider* dict = udDict->GetDict();
	return dict->refs(); // protected by global lock
}

// Return all keys
bool ScriptServerEngine::DictionaryGetAllKeys(const DictUserData* udDict, std::set<std::string>& allKeys) {
	jsVerifyReturn(udDict && udDict->IsValid(), false);
	const char* name = udDict->GetName();
	LogDebug("name=" << name);

	allKeys.clear();

	std::lock_guard<fast_recursive_mutex> lock(*udDict->GetSync()); // Lock dictionary

	ILuaSharedDataProvider* dict = udDict->GetDict();
	dict->keys(allKeys);
	return true;
}

std::string ScriptServerEngine::DictionaryAutoGenerateName() {
	int index;
	{
		std::lock_guard<fast_recursive_mutex> lock(m_dictionaryAutoIndexSync);
		index = m_dictionaryAutoIndex++;
	}

	char name[sizeof(DICTIONARY_AUTONAME_PREFIX) + 20];
	sprintf_s(name, DICTIONARY_AUTONAME_PREFIX "%06d", index);
	return name;
}

std::string ScriptServerEngine::DictionaryGetDomainPrefix(void* vm) {
	if (!m_isolateDictionariesByZone)
		return "";

	ASSERT_VM_SS("");

	std::stringstream ssZoneIndex;
	ssZoneIndex << std::hex << std::setw(8) << std::setfill('0') << ss->GetZoneId().toLong() << DICTIONARY_DOMAIN_DELIMTER;
	return ssZoneIndex.str();
}

const char* ScriptServerEngine::DictionaryStripDomainPrefixFromName(const char* name) {
	if (m_isolateDictionariesByZone) {
		const char* pDelimiter = strstr(name, DICTIONARY_DOMAIN_DELIMTER);
		if (pDelimiter) {
			return pDelimiter + 1;
		}
	}

	return name;
}

void ScriptServerEngine::ScriptGetPlayersInZoneCallback(void* vm, std::set<ULONG>& players) {
	ASSERT_VM_SS();

	ZoneIndex zoneId = ss->GetZoneId();
	for (NETIDZoneIndexMap::const_iterator it = m_playerZones.begin(); it != m_playerZones.end(); ++it) {
		if (it->second == zoneId) {
			players.insert(it->first);
		}
	}
}

bool ScriptServerEngine::DeleteScript(ScriptServerEvent::UploadAssetTypes assetType, const std::string& baseName) {
	LogInfo("type=" << assetType << ", name=[" << baseName << "]");

	// Validate input
	if (!IsValidFileName(baseName) || baseName.size() > MAX_PATH) { // should be much shorter than MAX_PATH
		LogError("Invalid script name [" << baseName << "]");
		return false;
	}

	// In case of configuration error, we don't delete a file from the shared read-only folder
	if (IsBaseDirReadOnly() && m_devToolsAppSourceFolder.empty()) {
		jsAssert(false); // If base dir is read-only, m_devToolsAppSourceFolder must not be empty
		LogError("DevTools source folder not defined");
		return false;
	}

	// Determine source and server paths
	std::string sourceDir, destDir, fullName, patchPrefixIgnored;
	bool addToPatchIgnored = false, compileLuaIgnored = false;
	if (!GetPublishingInfo((ScriptServerEvent::UploadAssetTypes)assetType, baseName, sourceDir, destDir, fullName, addToPatchIgnored, patchPrefixIgnored, compileLuaIgnored)) {
		// Error getting publishing info
		LogError("Unsupported type " << assetType);
		return false;
	}

	std::string fullPath;
	bool isDir = false;

	// Delete from source folder
	fullPath = PathAdd(sourceDir, fullName);
	if (jsFileExists(fullPath.c_str(), &isDir) && !isDir) {
		if (!::DeleteFile(Utf8ToUtf16(fullPath).c_str())) {
			LogError("Error deleting script [" << baseName << "], path: [" << fullPath << "]");
			return false;
		}
	} else {
		// Suppress error if asset is an action item instance
		if (assetType != ScriptServerEvent::ActionInstance && assetType != ScriptServerEvent::ActionInstanceParams) {
			LogError("File not found for script [" << baseName << "], path: [" << fullPath << "]");
		}
		return false;
	}

	// Delete from server file folder
	fullPath = PathAdd(destDir, fullName);
	if (jsFileExists(fullPath.c_str(), &isDir) && !isDir) {
		if (!::DeleteFile(Utf8ToUtf16(fullPath).c_str())) {
			LogError("Error deleting script [" << baseName << "], path: [" << fullPath << "]");
			return false;
		}
	} else {
		LogError("File not found for script [" << baseName << "], path: [" << fullPath << "]");
		return false;
	}

	return true;
}

std::string ScriptServerEngine::GetActionInstanceScriptName(int placementId) {
	std::stringstream ssFileName;
	ssFileName << placementId;

	return ssFileName.str();
}

bool ScriptServerEngine::processLuaYield(const ServerScriptPtr& ss) {
	ScriptServerEvent::EventType unblockEventType;
	std::string reason;
	IEvent* eventToSend = NULL;
	ScriptServerTaskObject* taskToQueue = NULL;

	bool block = ss->RetrieveYieldInfo(unblockEventType, reason, eventToSend, taskToQueue);

	if (block) {
		// First mark script as BLOCKED
		m_pScheduler->scriptCallBlocked(ss, unblockEventType, reason);

		// Then queue event that could unblock the script
		if (eventToSend) {
			SendToClient(eventToSend);
		} else if (taskToQueue) {
			m_taskSystem.QueueTask(taskToQueue);
		}
	}

	return block;
}

void ScriptServerEngine::SendScriptShutdownEvent(const ServerScriptPtr& pScript, NETID clientNetId, const std::string& reason) {
	if (!pScript->IsStopping()) {
		LogInfo(pScript->LogSubject() << "Reason: " << reason);
		pScript->SetStopping();

		ScriptServerEvent* se = new ScriptServerEvent(ScriptServerEvent::ShutdownScript, pScript->GetPlacementId(), pScript->GetZoneId().toLong(), 0, clientNetId);
		(*se->OutBuffer()) << reason;
		m_pScheduler->sendEventToScript(pScript, se);

		// Also queue UnloadScript event
		m_pScheduler->sendEventToScript(pScript, new ScriptServerEvent(ScriptServerEvent::UnloadScript, pScript->GetPlacementId(), pScript->GetZoneId().toLong(), 0, clientNetId));
	}
}

bool ScriptServerEngine::GetZoneCommunityId(void* vm, unsigned int& communityId,
	unsigned int zoneInstanceId = 0, unsigned int zoneType = 0) {
	ASSERT_VM_SS(false);

	zoneInstanceId = (zoneInstanceId != 0) ? zoneInstanceId : ss->GetZoneId().GetInstanceId();
	zoneType = (zoneType != 0) ? zoneType : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query query = m_conn->query();
	query << "SELECT uwi.community_id "
		  << "FROM unified_world_ids uwi "
		  << "WHERE uwi.zone_instance_id = %0 AND uwi.zone_type = %1 ";

	query.parse();
	StoreQueryResult queryResult = query.store(zoneInstanceId, zoneType);

	int num_rows = queryResult.num_rows();
	if (num_rows > 1) {
		LogError("Error: Query returned more than one result");
		return false;
	}
	if (num_rows < 1) {
		LogError("Error: Unable to map community for zone.");
		return false;
	}

	communityId = queryResult.at(0).at(0);
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::GetParentZoneInfoCallback(void* vm, unsigned int& parentZoneInstanceId, unsigned int& parentZoneIndex,
	unsigned int& parentZoneType, unsigned int& parentCommunityId, unsigned int zoneInstanceId = 0, unsigned int zoneType = 0) {
	ASSERT_VM_SS(false);

	zoneInstanceId = (zoneInstanceId != 0) ? zoneInstanceId : ss->GetZoneId().GetInstanceId();
	zoneType = (zoneType != 0) ? zoneType : ss->GetZoneId().GetType();

	return GetParentZoneInfoFromDB(zoneInstanceId, zoneType, parentZoneInstanceId, parentZoneIndex, parentZoneType, parentCommunityId);
}

bool ScriptServerEngine::GetParentZoneInfoFromDB(unsigned int zoneInstanceId, unsigned int zoneType, unsigned int& parentZoneInstanceId, unsigned int& parentZoneIndex, unsigned int& parentZoneType, unsigned int& parentCommunityId) {
	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	return IServerEngine::GetInstance()->GetParentZoneInfo(m_conn, zoneInstanceId, zoneType, parentZoneInstanceId, parentZoneIndex, parentZoneType, parentCommunityId);
}

bool ScriptServerEngine::GetChildZoneIds(void* vm, std::vector<std::vector<ULONG>>& zoneIds,
	unsigned int zoneInstanceId = 0, unsigned int zoneType = 0) {
	ASSERT_VM_SS(false);

	zoneInstanceId = (zoneInstanceId != 0) ? zoneInstanceId : ss->GetZoneId().GetInstanceId();
	zoneType = (zoneType != 0) ? zoneType : ss->GetZoneId().GetType();

	// Get the community_id of the parent zone first. This prevents a join that would result in a full table scan.
	unsigned int parentCommunityId = 0;
	if (!GetZoneCommunityId(vm, parentCommunityId, zoneInstanceId, zoneType)) {
		return false;
	}

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query query = m_conn->query();
	query << "SELECT uwi.zone_instance_id, uwi.zone_index, uwi.zone_type "
		  << "FROM unified_world_ids uwi "
		  << "INNER JOIN kaneva.communities c ON uwi.community_id = c.community_id "
		  << "WHERE c.parent_community_id = %0 "
		  << "AND c.status_id = 1 ";

	query.parse();
	StoreQueryResult res = query.store(parentCommunityId);

	if (res.num_rows() > 0) {
		for (int i = 0; i < res.num_rows(); i++) {
			std::vector<ULONG> zoneInfo;
			zoneInfo.push_back(res.at(i).at(0));
			zoneInfo.push_back(res.at(i).at(1));
			zoneInfo.push_back(res.at(i).at(2));
			zoneIds.push_back(zoneInfo);
		}
	}

	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::GameItemIsPlaced(lua_State* vm, unsigned int gameItemId) {
	ASSERT_VM_SS(false);
	bool itemPlaced = true;

	unsigned int zoneInstanceId = ss->GetZoneId().GetInstanceId();
	unsigned int zoneIndex = ss->GetZoneId().getClearedInstanceId();
	unsigned int zoneType = ss->GetZoneId().GetType();
	unsigned int zoneCommunityId = 0;

	unsigned int parentZoneInstanceId = 0, parentZoneIndex = 0, parentZoneType = 0, parentCommunityId = 0;
	bool success = GetParentZoneInfoFromDB(zoneInstanceId, zoneType, parentZoneInstanceId, parentZoneIndex, parentZoneType, parentCommunityId);

	if (!success) {
		return itemPlaced; //an error occured, assume the item IS placed
	} else if (parentZoneInstanceId > 0) {
		zoneInstanceId = parentZoneInstanceId;
		zoneIndex = parentZoneIndex;
		zoneCommunityId = parentCommunityId;
	} else {
		if (!GetZoneCommunityId(vm, zoneCommunityId)) {
			return itemPlaced; //an error occured, assume the item IS placed
		}
	}

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query query = m_conn->query();

	query << "SELECT do.obj_placement_id "
		  << "FROM wok.dynamic_objects do "
		  << "INNER JOIN wok.dynamic_object_parameters dop ON do.obj_placement_id = dop.obj_placement_id "
		  << "AND dop.param_type_id = 33 "
		  << "AND dop.value = %0 "
		  << "WHERE do.zone_index = %2 AND do.zone_instance_id = %1 "
		  << "UNION "
		  << "SELECT do.obj_placement_id "
		  << "FROM unified_world_ids uwi "
		  << "INNER JOIN kaneva.communities c ON uwi.community_id = c.community_id "
		  << "INNER JOIN wok.dynamic_objects do ON uwi.zone_index = do.zone_index "
		  << "AND uwi.zone_instance_id = do.zone_instance_id "
		  << "INNER JOIN wok.dynamic_object_parameters dop ON do.obj_placement_id = dop.obj_placement_id "
		  << "AND dop.param_type_id = 33 "
		  << "AND dop.value = %0 "
		  << "WHERE c.parent_community_id = %3 AND c.status_id = 1 ";

	query.parse();
	StoreQueryResult queryResult = query.store(gameItemId, zoneInstanceId, zoneIndex, zoneCommunityId);

	int num_rows = queryResult.num_rows();
	if (num_rows == 0)
		itemPlaced = false;
	END_TRY_SQL_LOG_ONLY(m_conn);

	return itemPlaced;
}

bool ScriptServerEngine::GameItemGetByApp(void* vm, std::vector<std::vector<std::string>>& gameItems) {
	ASSERT_VM_SS(false);

	unsigned int zone_index = ss->GetZoneId().getClearedInstanceId();
	unsigned int zone_instance_id = ss->GetZoneId().GetInstanceId();

	if (ss->GetZoneId().isArena()) //get template objects for all arenas
		zone_instance_id = 1;

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query objQuery = m_conn->query();
	objQuery << " SELECT do.obj_placement_id AS PID, dop.VALUE AS UNID, p.name AS username "
			 << " FROM dynamic_objects do "
			 << " INNER JOIN dynamic_object_parameters dop ON do.obj_placement_id = dop.obj_placement_id "
			 << " AND dop.param_type_id = 33 "
			 << " INNER JOIN players p ON do.player_id = p.player_id "
			 << " WHERE do.zone_index = %0 AND do.zone_instance_id = %1";

	objQuery.parse();

	LogTrace(objQuery.str(zone_index, zone_instance_id));
	StoreQueryResult objQueryResult = objQuery.store(zone_index, zone_instance_id);

	int num_rows = objQueryResult.num_rows();
	for (int i = 0; i < num_rows; i++) {
		std::vector<std::string> objInfo;
		objInfo.push_back(std::string(objQueryResult.at(i).at(0).c_str()));
		objInfo.push_back(std::string(objQueryResult.at(i).at(1).c_str()));
		objInfo.push_back(std::string(objQueryResult.at(i).at(2).c_str()));
		gameItems.push_back(objInfo);
	}
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::GameItemGetByPlayer(void* vm, const std::string& playerNameStr, std::vector<std::pair<int, int>>& gameItems) {
	ASSERT_VM_SS(false);

	unsigned int zone_index = ss->GetZoneId().getClearedInstanceId();
	unsigned int zone_instance_id = ss->GetZoneId().GetInstanceId();
	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query objQuery = m_conn->query();
	objQuery << " SELECT do.obj_placement_id AS PID, dop.VALUE AS UNID "
			 << " FROM dynamic_objects do "
			 << " INNER JOIN dynamic_object_parameters dop ON do.obj_placement_id = dop.obj_placement_id "
			 << " AND dop.param_type_id = 33 "
			 << " INNER JOIN players p on do.player_id = p.player_id "
			 << " AND p.name = %0q "
			 << " WHERE do.zone_index = %1 AND do.zone_instance_id = %2 ";

	objQuery.parse();
	StoreQueryResult objQueryResult = objQuery.store(playerNameStr, zone_index, zone_instance_id);

	int num_rows = objQueryResult.num_rows();
	for (int i = 0; i < num_rows; i++) {
		gameItems.push_back(std::make_pair(objQueryResult.at(i).at(0), objQueryResult.at(i).at(1)));
	}
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::GameItemLoadAll(void* vm, std::vector<GameItem>& gameItems, unsigned int zone_instance_id = 0, unsigned int zone_type = 0) {
	ASSERT_VM_SS(false);

	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query objQuery = m_conn->query();
	objQuery << " SELECT sgi.game_item_id, sgi.name, sgi.item_type, COALESCE(sgi.game_item_glid, 0), COALESCE(sgi.glid, 0), sgi.level, sgi.rarity, sgit.inventory_compatible, "
			 << "   COALESCE(sngip.property_name, sgip.property_name) AS comb_property_name, COALESCE(sngip.property_value, sgip.property_value) "
			 << " FROM script_game_items sgi "
			 << " INNER JOIN script_game_item_types sgit ON sgi.item_type = sgit.item_type "
			 << " LEFT OUTER JOIN script_game_item_properties sgip ON sgi.zone_instance_id = sgip.zone_instance_id "
			 << "   AND sgi.zone_type = sgip.zone_type"
			 << "   AND sgi.game_item_id = sgip.game_item_id "
			 << "   AND sgi.game_item_glid IS NULL "
			 << " LEFT OUTER JOIN snapshot_script_game_item_properties sngip ON sgi.game_item_glid = sngip.game_item_glid "
			 << " WHERE sgi.zone_instance_id = %0 AND sgi.zone_type = %1 "
			 << " ORDER BY sgi.game_item_id, comb_property_name ASC ";

	objQuery.parse();

	// LogTrace(objQuery.str(zone_index, zone_instance_id));
	StoreQueryResult objQueryResult = objQuery.store(zone_instance_id, zone_type);

	int num_rows = objQueryResult.num_rows();

	int currentGameItemId = -1;
	GameItem g;
	for (int i = 0; i < num_rows; i++) {
		if (currentGameItemId != (int)objQueryResult.at(i).at(0)) {
			// Found a new one
			if (currentGameItemId > 0) {
				gameItems.push_back(g);
				g.properties.clear();
			}

			// Init
			g.giId = objQueryResult.at(i).at(0);
			g.name = objQueryResult.at(i).at(1);
			g.itemType = objQueryResult.at(i).at(2);
			g.giGLID = objQueryResult.at(i).at(3);
			g.glid = objQueryResult.at(i).at(4);
			g.level = objQueryResult.at(i).at(5);
			g.rarity = objQueryResult.at(i).at(6);
			g.inventoryCompatible = (objQueryResult.at(i).at(7).compare("T") == 0);
			currentGameItemId = g.giId;
		}

		if (objQueryResult.at(i).at(8).compare("NULL") != 0) {
			g.properties.push_back(std::make_pair(static_cast<std::string>(objQueryResult.at(i).at(8)),
				static_cast<std::string>(objQueryResult.at(i).at(9))));
		}
	}

	if (num_rows > 0) {
		gameItems.push_back(g);
	}
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::GameItemUpdate(void* vm, const GameItem& gameItem, unsigned int zone_instance_id, unsigned int zone_type) {
	ASSERT_VM_SS(false);

	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	// Wrap nullable numeric columns
	char buffer[65];
	_itoa_s(gameItem.giGLID, buffer, 65, 10);
	std::string giGLID = (gameItem.giGLID > 0 ? buffer : "NULL");

	_itoa_s(gameItem.glid, buffer, 65, 10);
	std::string glid = (gameItem.glid > 0 ? buffer : "NULL");

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);
	Query query = m_conn->query();

	// Update or insert
	query << " INSERT INTO script_game_items (zone_instance_id, zone_type, game_item_id, game_item_glid, name, item_type, glid, level, rarity) "
		  << " VALUES(%0, %1, %2, %3, %4q, %5q, %6, %7, %8q) "
		  << " ON DUPLICATE KEY UPDATE game_item_glid = %3, name = %4q, item_type = %5q, glid = %6, level = %7, rarity = %8q ";
	query.parse();
	query.execute(zone_instance_id, zone_type, gameItem.giId, giGLID, gameItem.name.c_str(), gameItem.itemType.c_str(), glid, gameItem.level, gameItem.rarity.c_str());
	query.reset();

	// Clear the properties
	query << " DELETE FROM script_game_item_properties "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 AND game_item_id = %2 ";
	query.parse();
	query.execute(zone_instance_id, zone_type, gameItem.giId);
	query.reset();

	// Refill the properties (only if GIGLID is nullified, i.e., this is a customized game item)
	if (gameItem.giGLID == 0 && gameItem.properties.size() > 0) {
		query << " INSERT INTO script_game_item_properties VALUES ";
		for (unsigned int i = 0; i < gameItem.properties.size(); i++) {
			if (i > 0) {
				query << ", ";
			}
			std::string safeData = gameItem.properties[i].second;
			replace(safeData.begin(), safeData.end(), '"', '\'');
			query << " (%0, %1, %2, \"" << gameItem.properties[i].first.c_str() << "\", \"" << safeData.c_str() << "\") ";
		}
		query.parse();
		query.execute(zone_instance_id, zone_type, gameItem.giId);
	}

	trans.commit();
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::GameItemDelete(void* vm, int giId, unsigned int zone_instance_id, unsigned int zone_type) {
	ASSERT_VM_SS(false);

	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);
	Query query = m_conn->query();

	// Update or insert
	query << " DELETE FROM script_game_items "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 AND game_item_id = %2 ";

	query.parse();

	std::string updateString = query.str(zone_instance_id, zone_type, giId);

	LogTrace(updateString);
	query.execute(updateString);

	query.reset();

	// Clear the properties
	query << " DELETE FROM script_game_item_properties "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 AND game_item_id = %2 ";

	query.parse();

	updateString = query.str(zone_instance_id, zone_type, giId);

	LogTrace(updateString);
	query.execute(updateString);

	trans.commit();
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::GameItemDeletePlacementCallback(void* vm, ULONG pid) {
	ASSERT_VM_SS(false);

	bool success = false;
	ZoneIndex base_zone_id = ss->GetZoneId().GetBaseZoneIndex();
	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	{
		BEGIN_TRY_SQL();
		Transaction trans(*m_conn);
		Query objQuery = m_conn->query();
		objQuery << " SELECT do.obj_placement_id AS PID "
				 << " FROM dynamic_objects do "
				 << " INNER JOIN dynamic_object_parameters dop ON do.obj_placement_id = dop.obj_placement_id "
				 << " AND dop.param_type_id = 33 "
				 << " WHERE do.obj_placement_id = %0 AND do.zone_index = %1 AND do.zone_instance_id = %2";

		objQuery.parse();
		StoreQueryResult objQueryResult = objQuery.store((UINT)pid, (int)base_zone_id.getClearedInstanceId(), (int)base_zone_id.GetInstanceId());

		int num_rows = objQueryResult.num_rows();
		if (num_rows < 1) {
			// For safety and security reasons this API will fail if the given pid
			// is not actually a game item.
			LogError("Error PID [" << pid << "] is not a game item");
			return false;
		}
		objQuery.reset();

		Query query = m_conn->query();
		query << " DELETE FROM dynamic_objects "
			  << " WHERE obj_placement_id = %0 ";
		query.parse();
		query.execute((UINT)pid);
		query.reset();

		query << " DELETE FROM dynamic_object_parameters "
			  << " WHERE obj_placement_id = %0 ";
		query.parse();
		query.execute((UINT)pid);
		query.reset();
		trans.commit();
		success = true;
		END_TRY_SQL_LOG_ONLY(m_conn);
	}

	// remove any scripts attached to the deleted object
	if (success) {
		// send out object delete event
		ss->GetGameStateManager()->RemoveGameStateObject(ss->GetZoneId(), pid);
		IServerEngine::GetInstance()->ClearZoneDOMemCache(ss->GetZoneId());
		ScriptClientEvent* sce = ScriptClientEvent::EncodeObjectDelete(NULL, pid, ss->GetZoneId().toLong(), ss->GetZoneId().GetInstanceId());
		SendToClient(sce);
	}

	return success;
}

bool ScriptServerEngine::GameItemGetNextId(void* vm, int& _nextID) {
	ASSERT_VM_SS(false);

	bool success = false;
	unsigned int zone_instance_id = ss->GetZoneId().GetInstanceId();
	unsigned int zone_type = ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query query = m_conn->query();

	// Update or insert
	query << " SELECT MAX(game_item_id) FROM script_game_items "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 ";
	query.parse();

	StoreQueryResult queryResult = query.store(zone_instance_id, zone_type);

	int num_rows = queryResult.num_rows();
	if (num_rows < 1) {
		LogError("Error: Query did not return a result");
		return false;
	} else if (num_rows > 1) {
		LogError("Error: Query returned more than one result");
		return false;
	}

	_nextID = queryResult.at(0).at(0) + 1;
	success = true;
	END_TRY_SQL_LOG_ONLY(m_conn);

	return success;
}

bool ScriptServerEngine::ScriptSavePlayerDataCallback_v2(void* vm, const char* playerName, const char* attribute, const char* value, unsigned int zone_instance_id, unsigned int zone_type) {
	ASSERT_VM_SS(false);

	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);
	Query query = m_conn->query();

	query << " INSERT INTO script_game_player_data(zone_instance_id, zone_type, username, attribute, value) "
		  << " VALUES (%0, %1, %2q, %3q, %4q) "
		  << " ON DUPLICATE KEY UPDATE attribute = %3q, value = %4q ";
	query.parse();
	query.execute(zone_instance_id, zone_type, playerName, attribute, value);

	checkMultipleResultSets(query, "script_save_player_data");

	trans.commit();
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::ScriptLoadAllPlayerDataCallback_v2(void* vm, const char* playerName, ScriptDataMap& playerData, unsigned int zone_instance_id, unsigned int zone_type) {
	ASSERT_VM_SS(false);

	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query query = m_conn->query();
	query << " SELECT attribute, value "
		  << " FROM script_game_player_data "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 AND username = %2q ";
	query.parse();
	StoreQueryResult res = query.store(zone_instance_id, zone_type, playerName);

	if (res.num_rows() > 0) {
		ScriptDataPair dataPair;

		for (UINT i = 0; i < res.num_rows(); i++) {
			dataPair.first = res.at(i).at(0);
			dataPair.second = res.at(i).at(1);

			playerData.insert(dataPair);
		}
	}

	LogDebug("Retrieved " << playerData.size() << " records for " << playerName);

	checkMultipleResultSets(query, "script_load_all_player_data");
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

std::string ScriptServerEngine::ScriptLoadPlayerDataCallback_v2(void* vm, const char* playerName, const char* attribute, unsigned int zone_instance_id, unsigned int zone_type) {
	ASSERT_VM_SS("");

	std::string value = "";

	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query query = m_conn->query();
	query << " SELECT value "
		  << " FROM script_game_player_data "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 AND username = %2q AND attribute = %3q ";
	query.parse();
	StoreQueryResult res = query.store(zone_instance_id, zone_type, playerName, attribute);

	if (res.num_rows() > 0) {
		value = res.at(0).at(0);
	}

	checkMultipleResultSets(query, "script_load_player_data");
	END_TRY_SQL_LOG_ONLY(m_conn);

	return value;
}

bool ScriptServerEngine::ScriptDeletePlayerDataCallback_v2(void* vm, const char* playerName, const char* attribute, unsigned int zone_instance_id, unsigned int zone_type) {
	ASSERT_VM_SS(false);

	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " DELETE FROM script_game_player_data "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 AND username = %2q AND attribute = %3q ";
	query.parse();
	query.execute(zone_instance_id, zone_type, playerName, attribute);

	checkMultipleResultSets(query, "script_delete_player_data");

	trans.commit();
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::ScriptDeleteAllPlayerDataCallback_v2(void* vm, const char* playerName, unsigned int zone_instance_id, unsigned int zone_type) {
	ASSERT_VM_SS(false);

	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " DELETE FROM script_game_player_data "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 AND username = %2q ";
	query.parse();
	query.execute(zone_instance_id, zone_type, playerName);

	checkMultipleResultSets(query, "script_delete_all_player_data");

	trans.commit();
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::ScriptSaveGameDataCallback_v2(void* vm, const char* attribute, const char* value, unsigned int zone_instance_id, unsigned int zone_type) {
	ASSERT_VM_SS(false);

	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " INSERT INTO script_game_custom_data(zone_instance_id, zone_type, attribute, value) "
		  << " VALUES (%0, %1, %2q, %3q) "
		  << " ON DUPLICATE KEY UPDATE attribute = %2q, value = %3q ";
	query.parse();
	query.execute(zone_instance_id, zone_type, attribute, value);

	checkMultipleResultSets(query, "script_save_game_data");

	trans.commit();
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::ScriptLoadAllGameDataCallback_v2(void* vm, ScriptDataMap& gameData) {
	ASSERT_VM_SS(false);

	unsigned int zone_instance_id = ss->GetZoneId().GetInstanceId();
	unsigned int zone_type = ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query query = m_conn->query();
	query << " SELECT attribute, value "
		  << " FROM script_game_custom_data "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 ";
	query.parse();
	StoreQueryResult res = query.store(zone_instance_id, zone_type);

	if (res.num_rows() > 0) {
		ScriptDataPair dataPair;
		for (UINT i = 0; i < res.num_rows(); i++) {
			dataPair.first = res.at(i).at(0);
			dataPair.second = res.at(i).at(1);
			gameData.insert(dataPair);
		}
	}

	checkMultipleResultSets(query, "script_load_all_game_data");
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

std::string ScriptServerEngine::ScriptLoadGameDataCallback_v2(void* vm, const char* attribute, unsigned int zone_instance_id, unsigned int zone_type) {
	ASSERT_VM_SS("");

	std::string value = "";
	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Query query = m_conn->query();
	query << " SELECT value "
		  << " FROM script_game_custom_data "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 AND attribute = %2q; ";
	query.parse();
	StoreQueryResult res = query.store(zone_instance_id, zone_type, attribute);

	if (res.num_rows() > 0)
		value = res.at(0).at(0);

	checkMultipleResultSets(query, "script_load_game_data");
	END_TRY_SQL_LOG_ONLY(m_conn);

	return value;
}

bool ScriptServerEngine::ScriptDeleteGameDataCallback_v2(void* vm, const char* attribute, unsigned int zone_instance_id, unsigned int zone_type) {
	ASSERT_VM_SS(false);

	zone_instance_id = (zone_instance_id != 0) ? zone_instance_id : ss->GetZoneId().GetInstanceId();
	zone_type = (zone_type != 0) ? zone_type : ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " DELETE FROM script_game_custom_data "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1 AND attribute = %2q; ";
	query.parse();
	query.execute(zone_instance_id, zone_type, (const char*)attribute);

	checkMultipleResultSets(query, "script_delete_game_data");

	trans.commit();
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::ScriptDeleteAllGameDataCallback_v2(void* vm) {
	ASSERT_VM_SS(false);

	unsigned int zone_instance_id = ss->GetZoneId().GetInstanceId();
	unsigned int zone_type = ss->GetZoneId().GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);
	Query query = m_conn->query();
	query << " DELETE FROM script_game_custom_data "
		  << " WHERE zone_instance_id = %0 AND zone_type = %1; ";
	query.parse();
	query.execute(zone_instance_id, zone_type);

	checkMultipleResultSets(query, "script_delete_all_game_data");

	trans.commit();
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

void ScriptServerEngine::StopPendingLoadScriptTasks(const ZoneIndex& zi) {
	// Notify all pending load scripts to stop loading, if zone index matches.
	for (std::set<LoadScriptTask*>::const_iterator it = m_pendingLoadScriptTasks.begin(); it != m_pendingLoadScriptTasks.end(); ++it) {
		(*it)->StopByZoneIndex(zi);
	}
}

void ScriptServerEngine::ScriptGetSourceInfo(void* vm, int stackLevel, std::string& source, int& lineNumber) {
	ASSERT_VM_SS();
	ss->LuaGetSourceInfo(stackLevel, source, lineNumber);
}

bool ScriptServerEngine::GameLoadGlobalConfig(const std::vector<std::string>& _propertyList, ScriptDataMap& _outMap) {
	// Can't very well load an empty property list.
	if (_propertyList.empty())
		return false;

	// Declaring this so that the escape_string function can be used.
	Query query = m_conn->query();

	// Build the SQL property list string.
	std::string propertyListStr = "(";
	for (unsigned int i = 0; i < _propertyList.size(); ++i) {
		if (i > 0)
			propertyListStr.append(",");
		propertyListStr.append("\"");

		// Copy the string and do an escape_string on it, theoretically preventing SQL
		// injection type issues.
		std::string curStr = _propertyList[i];
		query.escape_string(&curStr, curStr.c_str());

		propertyListStr.append(curStr.c_str());
		propertyListStr.append("\"");
	}
	propertyListStr.append(")");

	// Now run the query for the requested properties.
	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	// First build the query.
	query << " SELECT * "
		  << "   FROM script_game_item_global_configs "
		  << "  WHERE property_name IN %0; ";
	query.parse();

	// Now execute the query.
	StoreQueryResult res = query.store((const char*)propertyListStr.c_str());

	// Now parse the results of the query.
	if (res.num_rows() > 0) {
		ScriptDataPair dataPair;
		for (UINT i = 0; i < res.num_rows(); i++) {
			dataPair.first = res.at(i).at(0);
			dataPair.second = res.at(i).at(1);
			_outMap.insert(dataPair);
		}
	}

	checkMultipleResultSets(query, "game_load_global_config");
	END_TRY_SQL_LOG_ONLY(m_conn);

	return true;
}

bool ScriptServerEngine::GameInitFrameworkDB(void* vm) {
	ASSERT_VM_SS(false);

	unsigned int zone_instance_id = ss->GetZoneId().GetInstanceId();
	unsigned int zone_type = ss->GetZoneId().GetType();

	unsigned long long customDataRows = 0;
	unsigned long long gameItemRows = 0;

	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	BEGIN_TRY_SQL();
	Transaction trans(*m_conn);

	// Insert default script game custom data (World Settings, Environment Settings, etc.)
	Query query = m_conn->query();
	query << " INSERT INTO script_game_custom_data (zone_instance_id, zone_type, attribute, value) "
		  << " SELECT %0, %1, attribute, value "
		  << " FROM default_script_game_custom_data; ";
	query.parse();
	customDataRows = query.execute(zone_instance_id, zone_type).rows();

	if (customDataRows <= 0) {
		LogError("Error: Failed to insert default script_game_custom_data for zone_instance_id: " << zone_instance_id << " zone_type: " << zone_type);
		return false;
	}

	// Insert default script game items
	query.reset();
	query << " INSERT INTO script_game_items (zone_instance_id, zone_type, game_item_id, game_item_glid, glid, name, item_type, level, rarity) "
		  << " SELECT %0, %1, d.game_item_id, d.game_item_glid, s.glid, s.name, s.item_type, s.level, s.rarity "
		  << " FROM default_script_game_items d "
		  << " INNER JOIN snapshot_script_game_items s ON d.game_item_glid = s.game_item_glid "
		  << " WHERE d.automatic_insertion_start IS NOT NULL; ";
	query.parse();
	gameItemRows = query.execute(zone_instance_id, zone_type).rows();

	if (gameItemRows <= 0) {
		LogError("Error: Failed to insert default script_game_items for zone_instance_id: " << zone_instance_id << " zone_type: " << zone_type);
		return false;
	}

	trans.commit();
	END_TRY_SQL_LOG_ONLY(m_conn);

	// Return true only if all inserts completed successfully
	return (customDataRows > 0 && gameItemRows > 0);
}

void ScriptServerEngine::PlayerSaveZoneSpawnPointCallback(void* vm, ULONG clientId, float x, float y, float z, float rx, float ry, float rz, int playerIdOverride) {
	ASSERT_VM_SS();

	// clientId of 0 is ok, because this will be processed by the game server.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerSaveZoneSpawnPoint, 0);
	*sce->OutBuffer() << ss->GetZoneId() << clientId << playerIdOverride << x << y << z << rx << ry << rz;

	SendToClient(sce);
}

void ScriptServerEngine::PlayerDeleteZoneSpawnPointCallback(void* vm, ULONG clientId, int playerIdOverride) {
	ASSERT_VM_SS();

	// clientId of 0 is ok, because this will be processed by the game server.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::PlayerDeleteZoneSpawnPoint, 0);
	*sce->OutBuffer() << ss->GetZoneId() << clientId << playerIdOverride;

	SendToClient(sce);
}

void ScriptServerEngine::ObjectRotateHelper(const ServerScriptPtr& ss, int objId, double time, double dx, double dy, double dz, double sx, double sy, double sz, bool bufferCall) {
	ScriptClientEvent* sce = ScriptClientEvent::EncodeObjectRotate(NULL, (ULONG)objId, ss->GetZoneId().toLong(), ss->GetZoneId().GetInstanceId(), dx, dy, dz, sx, sy, sz, time);

	if (bufferCall) {
		ScriptClientEvent* e = new ScriptClientEvent(sce);
		ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, (ULONG)objId);
	}

	SendToClient(sce);
}

int ScriptServerEngine::ObjectGameStateGetValue(void* vm, ULONG pid, ILuaSharedDataProvider::Key key, void* buffer, size_t bufSize) {
	ASSERT_VM_SS(0);

	LuaGameObject container(*ss->GetGameStateManager().get(), pid);
	return container.get(key, buffer, bufSize);
}

int ScriptServerEngine::PlayerGameStateGetValue(void* vm, ULONG clientId, ILuaSharedDataProvider::Key key, void* buffer, size_t bufSize) {
	ASSERT_VM_SS(0);

	LuaGamePlayer container(clientId);
	return container.get(key, buffer, bufSize);
}

void ScriptServerEngine::QueueSaveScriptTask(ULONG placementId, const std::string& scriptPath) {
	auto pTask = new SaveScriptTask(m_conn, m_dbAccessCS, placementId, scriptPath);
	m_taskSystem.QueueTask(pTask);
}

void ScriptServerEngine::SetZoneConfigGroups(const ZoneIndex& zi, const std::vector<std::string>& configGroups) {
	m_zoneConfigGroups[zi] = configGroups;
}

bool ScriptServerEngine::ScriptRecordMetric(void* vm, std::string message) {
	ASSERT_VM_SS(false);

	try {
		m_messageQueueAgent->publishMessageToQueue("metrics", message, true);
	} catch (const MessageQueueException& me) {
		LogError("Failed to report script metric [" << me.str().c_str() << "]");
		return false;
	}

	return true;
}

int ScriptServerEngine::NPCSpawnCallback(
	void* vm,
	int siType, // SERVER_ITEM_TYPE
	int placementId,
	const std::string& charConfigJson,
	int ccId,
	int dbIndex,
	const Vector3f& pos,
	double rotDeg,
	GLID glidAnimSpecial,
	const std::vector<GLID>& glidsArmed) {
	ASSERT_VM_SS(false);

	if (placementId == 0) {
		// Dynamically spawned
		Matrix44f rotationMatrix;
		ConvertRotationY(ToRadians(rotDeg), out(rotationMatrix)); // Call same rotation transform as KEPClient for consistency (see ClientBaseEngine::SpawnStaticMovementObject)
		placementId = ObjectGenerateCallback(vm, GLID_INVALID, pos.x, pos.y, pos.z, rotationMatrix[2][0], rotationMatrix[2][1], rotationMatrix[2][2], Placement_DynObj);
		assert(placementId > 0);
	}

	// Create an event to send to all users currently in the zone.
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::NPCSpawn, 0);
	ZoneIndex zoneId = ss->GetZoneId();
	*sce->OutBuffer()
		<< zoneId.toLong()
		<< siType
		<< placementId
		<< charConfigJson
		<< ccId
		<< dbIndex
		<< pos.x << pos.y << pos.z
		<< rotDeg
		<< glidAnimSpecial
		<< glidsArmed.size();
	for (size_t i = 0; i < glidsArmed.size(); ++i)
		*sce->OutBuffer() << glidsArmed[i];

	// Send the primary event out to all users in the zone.
	SendToClient(sce);
	return placementId;
}

void ScriptServerEngine::handleTimerEvent() {
	// Check for script blocking timeout
	m_pScheduler->checkScriptBlockingTimeout(m_globalVMConfig.getBlockedScriptTimeout(), m_globalVMConfig.getAPITimeoutIsFatal());

	// Scan and process pending player arrive events
	ProcessPendingPlayerArrives();

	// Check for delayed spin-down every 60 seconds
	if (fTime::ElapsedMs(m_lastZoneSpinDownExpiryCheckTimeMs) > 60000) {
		m_lastZoneSpinDownExpiryCheckTimeMs = fTime::TimeMs();

		std::set<ZoneIndex> zonesToSpinDown;
		GetAllExpiredZones(zonesToSpinDown);
		for (const auto& zoneIndex : zonesToSpinDown) {
			// Remove game states
			m_gameStates.erase(zoneIndex);

			StopPendingLoadScriptTasks(zoneIndex);

			// reset wok.script_zones record
			updateScriptZoneState(zoneIndex, false);

			// Send a Departed event to all the scripts associated with the previous zone
			// In this particular case, playerGetName will continue to work. playerName is passed to kgp_depart() anyway for consistency.
			SendShutdownEventToZone(zoneIndex, "zone expired");
		}
	}

	// Forward timer event to individual scripts
	SendTimerEventToAllZones();
}

void ScriptServerEngine::updateScriptZoneState(const ZoneIndex& zoneIndex, bool spinUp) {
	IServerEngine::GetInstance()->updateScriptZoneState(zoneIndex, m_defaultSpinDownDelayMinutes, spinUp);
}

EVENT_PROC_RC ScriptServerEngine::UpdateScriptZoneSpinDownDelayHandler(ULONG lparam, IDispatcher* disp, IEvent* e) {
	ScriptServerEngine* me = reinterpret_cast<ScriptServerEngine*>(lparam);
	assert(me != nullptr);

	UpdateScriptZoneSpinDownDelayEvent* evt = dynamic_cast<UpdateScriptZoneSpinDownDelayEvent*>(e);
	jsVerifyReturn(evt != 0, EVENT_PROC_RC::NOT_PROCESSED);

	unsigned serverId = IServerEngine::GetInstance()->GetServerId();
	if (serverId == 0) {
		LogWarn("Server Id not initialized - event discarded");
		return EVENT_PROC_RC::CONSUMED;
	}

	ZoneIndex zoneIndex;
	int spinDownDelayMins = 0;
	unsigned tiedServerId = 0;
	evt->ExtractInfo(zoneIndex, spinDownDelayMins, tiedServerId);

	LogInfo(zoneIndex.ToStr() << ": spin-down delay = " << spinDownDelayMins << ", tied to server = " << tiedServerId);

	if (tiedServerId != 0 && serverId != tiedServerId) {
		// Zone is tied to a different server. Set spin down delay to 0 so that we can switch to the new server ASAP.
		LogInfo(zoneIndex.ToStr() << ": set spin down delay to 0 because zone is tied to a different server");
		spinDownDelayMins = 0;
	}

	me->SetZoneSpinDownDelayMinutes(zoneIndex, spinDownDelayMins, true);
	return EVENT_PROC_RC::CONSUMED;
}

void ScriptServerEngine::SendTimerEventToAllZones() {
	// Copy zone list
	std::vector<ScriptZonePtr> allZones = m_zoneMap.getAllZones();

	// Iterate through copied list without global lock
	for (const ScriptZonePtr& pZone : allZones) {
		//TODO: check if the zone is being stopped

		std::vector<ServerScriptPtr> filteredScripts = pZone->getScriptsByFilter(&ScriptZone::IdleScriptFilter<ServerScript::TIMER_BIT>);
		for (const ServerScriptPtr& pScript : filteredScripts) {
			// This works just like other kinds of event
			// handlers, but it operates on all the scripts.
			// This is a weakness; there's no quick way to figure
			// out which ones are idle without traversing the
			// entire map.

			// Check current snapshot of the script state, queue event if IDLE
			// The VM is idle, queue a task to process the event.
			m_pScheduler->sendEventToScript(pScript, new ScriptServerEvent(ScriptServerEvent::Timer, pScript->GetPlacementId(), pScript->GetZoneId().toLong(), 0, 0));
		}
	}
}

void ScriptServerEngine::SendArrivedEventToZone(ZoneIndex zoneIndex, NETID clientNetId, const std::string& playerName) {
	LogInfo("<" << playerName << ":" << clientNetId << "> zoneIndex=" << zoneIndex);
	ScriptZonePtr pZone = m_zoneMap.getZone(zoneIndex);
	if (!pZone) {
		LogInfo(zoneIndex.ToStr() << " - zone not found");
		return;
	}

	//TODO: check if the zone is being stopped

	std::vector<ServerScriptPtr> filteredScripts = pZone->getScriptsByFilter(&ScriptZone::ScriptFilter<ServerScript::ARRIVE_BIT>);
	for (const ServerScriptPtr& pScript : filteredScripts) {
		// The arrived event is sent to anyone that's interested in people
		// arriving in the zone instance.
		m_pScheduler->sendEventToScript(pScript, new ScriptServerEvent(
													 ScriptServerEvent::PlayerArrived,
													 pScript->GetPlacementId(),
													 pScript->GetZoneId().toLong(),
													 0,
													 clientNetId));
	}
}

void ScriptServerEngine::SendDepartedEventToZone(ZoneIndex zoneIndex, NETID clientNetId, const ScriptPlayerInfo& playerInfo, bool disconnected) {
	LogInfo("<" << playerInfo.m_name << ":" << clientNetId << "> " << zoneIndex.ToStr() << " disconnected=" << LogBool(disconnected));

	auto posrot = LocationRegistry::singleton().fetchPlayerLocationInPreviousZone(clientNetId);

	ScriptZonePtr pZone = m_zoneMap.getZone(zoneIndex);
	if (!pZone) {
		LogInfo(zoneIndex.ToStr() << " - zone not found");
		return;
	}

	std::vector<ServerScriptPtr> filteredScripts = pZone->getScriptsByFilter(&ScriptZone::ScriptFilter<ServerScript::DEPART_BIT>);

	// Now iterate the temporary list. During the iteration script in master container might be removed while processing ShutdownScript events.
	for (const ServerScriptPtr& pScript : filteredScripts) {
		assert(pScript);

		// The departed event is sent to anyone that's interested in people
		// departing from the zone instance.
		auto se = new ScriptServerEvent(
			ScriptServerEvent::PlayerDeparted,
			pScript->GetPlacementId(),
			pScript->GetZoneId().toLong(),
			0,
			clientNetId);

		(*se->OutBuffer())
			<< playerInfo
			<< posrot.first.x << posrot.first.y << posrot.first.z
			<< posrot.second.x << posrot.second.y << posrot.second.z;

		m_pScheduler->sendEventToScript(pScript, se);
	}
}

void ScriptServerEngine::SendShutdownEventToZone(ZoneIndex zoneIndex, const std::string& reason) {
	LogInfo(zoneIndex.ToStr());

	ScriptZonePtr pZone = m_zoneMap.getZone(zoneIndex);
	if (!pZone) {
		LogInfo(zoneIndex.ToStr() << " - zone not found");
		return;
	}

	std::vector<ServerScriptPtr> filteredScripts = pZone->getScriptsByFilter(&ScriptZone::ScriptFilter<0>);

	// Now iterate the temporary list. During the iteration data in master list m_scripts might be removed while processing ShutdownScript events.
	for (const auto& pScript : filteredScripts) {
		assert(pScript);
		SendScriptShutdownEvent(pScript, 0, reason);
	}
}

void ScriptServerEngine::SendMenuEventToZone(IEvent* e, ZoneIndex zoneIndex, NETID clientNetId) {
	ScriptZonePtr pZone = m_zoneMap.getZone(zoneIndex);
	if (!pZone) {
		LogInfo(zoneIndex.ToStr() << " - zone not found");
		return;
	}

	int param_count;
	(*e->InBuffer()) >> param_count;

	std::vector<std::string> params;
	for (int i = 0; i < param_count; i++) {
		std::string param;
		(*e->InBuffer()) >> param;
		params.push_back(param);
	}

	//TODO: check if the zone is being stopped

	std::vector<ServerScriptPtr> filteredScripts = pZone->getScriptsByFilter(ScriptZone::ScriptFilter<ServerScript::MENU_BIT>);

	for (const ServerScriptPtr& pScript : filteredScripts) {
		ScriptServerEvent* se = new ScriptServerEvent(
			ScriptServerEvent::MenuEvent,
			pScript->GetPlacementId(),
			pScript->GetZoneId().toLong(),
			0,
			clientNetId);

		*se->OutBuffer() << (int)params.size();
		for (size_t i = 0; i < params.size(); i++)
			*se->OutBuffer() << params[i];

		m_pScheduler->sendEventToScript(pScript, se);
	}
}

void ScriptServerEngine::SendObjectMessageEventToScript(ScriptPID srcId, ZoneIndex dstZoneIndex, ScriptPID dstId, const char* msg, unsigned delay, bool isBroadcast, bool isInterZone) {
	assert(dstId != 0);

	//TODO: check if the zone is being stopped

	ScriptServerEvent* pSSE = new ScriptServerEvent(ScriptServerEvent::ObjectMessage, dstId, dstZoneIndex.toLong());
	*pSSE->OutBuffer() << msg << srcId << isBroadcast << isInterZone;

	// TODO: send object message event directly to script (if there is no delay)

	m_myDispatcher->QueueEventInFutureMs(pSSE, delay);
}

void ScriptServerEngine::SendObjectMessageEventToZone(ScriptPID srcId, ZoneIndex dstZoneIndex, const ScriptZonePtr& pZone, const char* msg, unsigned delay, bool isInterZone) {
	assert(pZone);

	//TODO: check if the zone is being stopped

	std::vector<ScriptPID> scriptIds = pZone->getScriptIDsAcceptingBroadcast(isInterZone);
	for (ScriptPID dstId : scriptIds) {
		SendObjectMessageEventToScript(srcId, dstZoneIndex, dstId, msg, delay, true, isInterZone);
	}
}

void ScriptServerEngine::SendScriptInformationToClient(ZoneIndex zoneIndex, NETID clientNetId) {
	ScriptClientEvent* sce = new ScriptClientEvent(ScriptClientEvent::GetAllScripts, clientNetId);

	std::vector<ServerScriptPtr> allScripts;
	ScriptZonePtr pZone = m_zoneMap.getZone(zoneIndex);
	if (pZone) {
		allScripts = pZone->getAllScripts();
	} else {
		LogWarn("Zone not found - " << zoneIndex.ToStr());
	}

	(*sce->OutBuffer()) << allScripts.size();

	for (const ServerScriptPtr& pScript : allScripts) {
		(*sce->OutBuffer()) << pScript->GetPlacementId();
		(*sce->OutBuffer()) << pScript->GetFileName();
	}

	// List of all "saved" scripts
	auto filenames = GetAllScriptFileNamesByType(ScriptServerEvent::SavedScript);
	(*sce->OutBuffer()) << (int)filenames.size();
	for (const auto& fileName : filenames)
		*sce->OutBuffer() << fileName;

	// List of all script "action items"
	filenames = GetAllScriptFileNamesByType(ScriptServerEvent::ActionItem);
	(*sce->OutBuffer()) << (int)filenames.size();
	for (const auto& fileName : filenames)
		*sce->OutBuffer() << fileName;

	// List of all param-only "action items"
	filenames = GetAllScriptFileNamesByType(ScriptServerEvent::ActionItemParams);
	(*sce->OutBuffer()) << (int)filenames.size();
	for (const auto& fileName : filenames)
		*sce->OutBuffer() << fileName;

	SendToClient(sce);
}

void ScriptServerEngine::ScriptBroadcastMessage(lua_State* vm, const std::string& msg, bool currentZone, bool linkedZones) {
	ASSERT_VM_SS();

	if (!currentZone && !linkedZones) {
		LogWarn("Neither current zone or linked zones are selected for broadcast");
		return;
	}

	// Current zone index
	ZoneIndex currZoneInde = ss->GetZoneId();

	// Master zone instance ID (if child then get parent, otherwise self)
	unsigned masterZoneInstanceId = ss->GetScriptZone()->getMasterZoneInstanceId();

	// Broadcast message to other scripts in selected zone (current, linked or both)
	std::vector<ScriptZonePtr> allZones = m_zoneMap.getAllZones();
	for (const auto& pZone : allZones) {
		bool isCurrentZone = pZone->getZoneIndex() == ss->GetZoneId();
		if (currentZone && isCurrentZone ||
			linkedZones && !isCurrentZone && pZone->getMasterZoneInstanceId() == masterZoneInstanceId) {
			SendObjectMessageEventToZone(0, pZone->getZoneIndex(), pZone, msg.c_str(), 0, !isCurrentZone);
		}
	}
}

void ScriptServerEngine::ScriptSetMessagePolicy(lua_State* vm, bool acceptsInZoneBroadcast, bool acceptsInterZoneBroadcast) {
	ASSERT_VM_SS();
	ss->SetAcceptsInZoneBroadcast(acceptsInZoneBroadcast);
	ss->SetAcceptsInterZoneBroadcast(acceptsInterZoneBroadcast);
}

/***************************
Dynamic zone APIs for determining home world relationships with K-Town
***************************/
bool ScriptServerEngine::PlayerHasLinkedHome(lua_State* vm, const std::string& playerName) {
	return (PlayerGetLinkedHomeId(vm, playerName) != 0);
}

bool ScriptServerEngine::PlayerInLinkedHome(lua_State* vm, const std::string& playerName) {
	ASSERT_VM_SS(false);
	ULONG zoneInstanceId = ss->GetZoneId().GetInstanceId();
	ULONG homeId = PlayerGetLinkedHomeId(vm, playerName);
	return (zoneInstanceId == homeId && homeId != 0);
}

ULONG ScriptServerEngine::PlayerGetLinkedHomeId(lua_State* vm, const std::string& playerName) {
	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	ULONG ret = 0;
	BEGIN_TRY_SQL();
	Query objQuery = m_conn->query();
	objQuery << "SELECT home_community_id, zone_instance_id, zone_index, zone_type "
			 << " FROM kaneva.users u "
			 << " INNER JOIN kaneva.child_home_worlds chu ON chu.community_id = u.home_community_id "
			 << " INNER JOIN wok.channel_zones cz ON cz.zone_instance_id = u.wok_player_id AND cz.zone_type = 3 "
			 << " WHERE username = '%0' ";

	objQuery.parse();

	LogTrace(objQuery.str());
	StoreQueryResult objQueryResult = objQuery.store(playerName);

	if (objQueryResult.num_rows() > 0) {
		ret = objQueryResult.at(0).at(1);
	}
	END_TRY_SQL_LOG_ONLY(m_conn);
	return ret;
}

bool ScriptServerEngine::GameHasDynamicLink(lua_State* vm) {
	ASSERT_VM_SS(false);
	unsigned int zoneInstanceId = ss->GetZoneId().GetInstanceId();
	std::lock_guard<fast_recursive_mutex> lock(m_dbAccessCS);
	bool ret = false;
	BEGIN_TRY_SQL();
	Query objQuery = m_conn->query();
	objQuery << " SELECT community_id FROM kaneva.child_home_worlds chu "
			 << " INNER JOIN kaneva.users u ON chu.community_id = u.home_community_id "
			 << " WHERE u.wok_player_id = %0 ";

	objQuery.parse();

	LogTrace(objQuery.str());
	StoreQueryResult objQueryResult = objQuery.store(zoneInstanceId);

	if (objQueryResult.num_rows() > 0) {
		ret = true;
	}
	END_TRY_SQL_LOG_ONLY(m_conn);
	return ret;
}

} // namespace KEP