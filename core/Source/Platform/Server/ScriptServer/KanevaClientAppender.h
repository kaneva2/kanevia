#pragma once
///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include <log4cplus/logger.h>
#include <log4cplus/appender.h>
#include "Tools/NetworkV2/INetwork.h" // for NETID

namespace KEP {

class KanevaClientAppender : public log4cplus::Appender {
public:
	KanevaClientAppender(NETID client, const log4cplus::helpers::Properties& props);
	virtual ~KanevaClientAppender();
	virtual void append(const log4cplus::spi::InternalLoggingEvent& e);
	virtual void close();

private:
	NETID m_client;
};

} // namespace KEP