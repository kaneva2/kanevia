///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptServerTaskObject.h"
#include <string>

namespace KEP {

class fast_recursive_mutex;
class KGP_Connection;

class DetachScriptTask : public ScriptServerTaskObject {
public:
	DetachScriptTask(
		//		log4cplus::Logger& logger,
		KGP_Connection* conn,
		fast_recursive_mutex& dbSync,
		ULONG objPlacementId);

	virtual void ExecuteTask() override;

private:
	//	log4cplus::Logger& m_logger;
	KGP_Connection* m_conn;
	fast_recursive_mutex& m_dbSync;
	const ULONG m_objPlacementId;
};

}; // namespace KEP
