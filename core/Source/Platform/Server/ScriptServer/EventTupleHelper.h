// Helper functions for encoding/decoding a tuple against an event
// Created: YC 04/2015

#pragma once

#include <tuple>

namespace {

// EventTupleDecoder functor
template <typename Tuple, int startingIndex, int remaining = std::tuple_size<Tuple>::value>
struct EventTupleDecoder {
	void operator()(IReadableBuffer& ibuf, Tuple& tup) {
		ibuf >> get<startingIndex>(tup);
		EventTupleDecoder<Tuple, startingIndex + 1, remaining - 1>()(ibuf, tup);
	}
};

// Specialization for EventTupleDecoder to end the recursion
template <typename Tuple, int startingIndex>
struct EventTupleDecoder<Tuple, startingIndex, 0> {
	void operator()(IReadableBuffer&, Tuple&) {}
};

// EventTupleEncoder functor
template <typename Tuple, int startingIndex, int remaining = std::tuple_size<Tuple>::value>
struct EventTupleEncoder {
	void operator()(IWriteableBuffer& obuf, const Tuple& tup) {
		obuf << get<startingIndex>(tup);
		EventTupleEncoder<Tuple, startingIndex + 1, remaining - 1>()(obuf, tup);
	}
};

// Specialization for EventTupleEncoder to end the recursion
template <typename Tuple, int startingIndex>
struct EventTupleEncoder<Tuple, startingIndex, 0> {
	void operator()(IWriteableBuffer&, const Tuple&) {}
};

// IReadableBuffer & >> Tuple &
#define READABLE_BUFFER_DECODE_TUPLE(n)                                                        \
	template <TYPELIST_##n>                                                                    \
	inline IReadableBuffer& operator>>(IReadableBuffer& ibuf, std::tuple<TYPELIST_##n>& tup) { \
		EventTupleDecoder<std::tuple<TYPELIST_##n>, 0>()(ibuf, tup);                           \
		return ibuf;                                                                           \
	}

// IWritableBuffer & << const Tuple &
#define WRITEABLE_BUFFER_ENCODE_TUPLE(n)                                                               \
	template <TYPELIST_##n>                                                                            \
	inline IWriteableBuffer& operator>>(IWriteableBuffer& obuf, const std::tuple<TYPELIST_##n>& tup) { \
		EventTupleEncoder<std::tuple<TYPELIST_##n>, 0>()(obuf, tup);                                   \
		return obuf;                                                                                   \
	}

// Ugly expansion as vc2010 does not support variadic template args nor is_specialization_of predicate
#define TYPELIST_1 typename T1
#define TYPELIST_2 TYPELIST_1, typename T2
#define TYPELIST_3 TYPELIST_2, typename T3
#define TYPELIST_4 TYPELIST_3, typename T4
#define TYPELIST_5 TYPELIST_4, typename T5
#define TYPELIST_6 TYPELIST_5, typename T6
#define TYPELIST_7 TYPELIST_6, typename T7
#define TYPELIST_8 TYPELIST_7, typename T8
#define TYPELIST_9 TYPELIST_8, typename T9
#define TYPELIST_10 TYPELIST_9, typename T10

READABLE_BUFFER_DECODE_TUPLE(1);
READABLE_BUFFER_DECODE_TUPLE(2);
READABLE_BUFFER_DECODE_TUPLE(3);
READABLE_BUFFER_DECODE_TUPLE(4);
READABLE_BUFFER_DECODE_TUPLE(5);
READABLE_BUFFER_DECODE_TUPLE(6);
READABLE_BUFFER_DECODE_TUPLE(7);
READABLE_BUFFER_DECODE_TUPLE(8);
READABLE_BUFFER_DECODE_TUPLE(9);
READABLE_BUFFER_DECODE_TUPLE(10);

WRITEABLE_BUFFER_ENCODE_TUPLE(1);
WRITEABLE_BUFFER_ENCODE_TUPLE(2);
WRITEABLE_BUFFER_ENCODE_TUPLE(3);
WRITEABLE_BUFFER_ENCODE_TUPLE(4);
WRITEABLE_BUFFER_ENCODE_TUPLE(5);
WRITEABLE_BUFFER_ENCODE_TUPLE(6);
WRITEABLE_BUFFER_ENCODE_TUPLE(7);
WRITEABLE_BUFFER_ENCODE_TUPLE(8);
WRITEABLE_BUFFER_ENCODE_TUPLE(9);
WRITEABLE_BUFFER_ENCODE_TUPLE(10);

} // namespace
