///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "PublishTask.h"

#include "server/ScriptServer/ScriptServerEngine.h"
#include "Core/Crypto/Crypto_MD5.h"
#include "Client/ExternalLuac.h"
#include "KEPFileNames.h"
#include "CVersionInfo.h"
#include "jsEnumFiles.h"

#include "common\include\CompressHelper.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

fast_recursive_mutex PublishTask::m_publishSync;

//
// PublishTask::PublishTask
//
// Construct a publishing task.
//
// @@param server		- ScriptServerEngine
// @@param sourceDir	- Full path to the source directory (where uploaded file is)
// @@param destDir		- Full path to the destination directory (serverscript or patch)
// @@param fileName		- File name with extension
// @@param addToPatch	- true if need to add to patch
// @@param patchPrefix	- Client patch folder such as "Menus" or "MenuScripts"
// @@param compileLua	- true if input is a lua file and requires compiling
// @@param completionEvent	- Event to send when publishing completes.
// @@param serverId		- Event destination
//
PublishTask::PublishTask(
	//	Logger& logger,
	int gameId,
	const std::string& sourceDir,
	const std::string& destDir,
	const std::string& fileName,
	bool addToPatch,
	const std::string& patchPrefix,
	bool compileLua,
	ScriptClientEvent* completionEvent) :
		ScriptServerTaskObject("PublishTask"),
		//	m_logger(logger),
		m_gameId(gameId),
		m_sourceDir(sourceDir),
		m_destDir(destDir),
		m_fileName(fileName),
		m_addToPatch(addToPatch),
		m_patchPrefix(patchPrefix),
		m_compileLua(compileLua),
		m_completionEvent(completionEvent) {}

void PublishTask::ExecuteTask() {
	bool fatalError = false;
	std::string errorString;

	std::string sourcePath = PathAdd(m_sourceDir, m_fileName);
	std::string destPath = PathAdd(m_destDir, m_fileName);

	if (m_compileLua) {
		std::string luacResult = ExternalLuac::CallExternalLuaCompiler(m_server->m_devToolsLuaCompiler, sourcePath);
		if (luacResult.compare("Success") != 0) {
			fatalError = true;
			errorString = luacResult;
			LogError("ExternalLuac::CallExternalLuaCompiler() FAILED - " << errorString);
		} else {
			sourcePath.append("c"); // lua -> luac
		}
	}

	// Copy file
	if (!fatalError) {
		// Create dest dir if not exists
		bool isDir = false;
		if (!jsFileExists(m_destDir.c_str(), &isDir)) {
			//Create directory hierarchy
			SHCreateDirectoryExW(NULL, Utf8ToUtf16(m_destDir).c_str(), NULL);
		}

		// Make direct copy if not patch
		if (!m_addToPatch) {
			if (!::CopyFileW(Utf8ToUtf16(sourcePath).c_str(), Utf8ToUtf16(destPath).c_str(), FALSE)) {
				fatalError = true;
				errorString = "Failed copying file to distribution folder: " + m_destDir;
				LogError("CopyFile() FAILED - " << errorString);
			}
		}
	}

	// Add to patch if necessary
	if (m_addToPatch && !fatalError) {
		m_publishSync.lock();
		std::string patchVersionInfoPath = m_destDir + VERSIONINFO_DAT;
		std::string patchName = m_patchPrefix + "---" + m_fileName;
		std::string serverPatchName = patchName;
		serverPatchName = ::CompressTypeExtAdd(serverPatchName);

		CVersionInfo worldVIFile;
		if (worldVIFile.loadFile(patchVersionInfoPath) == false) {
			LogError("worldVIFile.loadFile() FAILED - '" << patchVersionInfoPath << "'");
			worldVIFile.clear();
		}

		worldVIFile.set_Version("2.0");
		worldVIFile.set_VIFType("WORLD");
		worldVIFile.set_GameId(std::to_string(static_cast<long long>(m_gameId)));
		worldVIFile.set_VIVersion("1.0");

		CStringA destinatonDir(m_patchPrefix.c_str());
		destinatonDir.Replace('_', '\\');

		// Create Destination Folder (deep)
		std::string filePathDest = ::CompressTypeExtAdd(PathAdd(m_destDir, patchName));
		std::string destDir = StrStripFileNameExt(filePathDest);
		if (!FileHelper::CreateFolderDeep(destDir)) {
			LogError("CreateFolderDeep() FAILED - '" << destDir << "'");
		} else {
			// Compress File To Destination
			if (!::CompressFile(sourcePath, filePathDest)) {
				LogError("CompressFile() FAILED - '" << sourcePath << "'");
			}
		}

		auto serverFileSize = FileHelper::Size(filePathDest);

		std::string md5 = Md5File(sourcePath);

		CVIFileEntry newEntry;
		newEntry.set_Filename(patchName);
		newEntry.set_MD5(md5);
		newEntry.set_Directory(destinatonDir.GetString());
		newEntry.set_ServerFile(serverPatchName);
		newEntry.set_Register("No");
		newEntry.set_ServerFileSize((size_t)serverFileSize);
		newEntry.set_Required("Yes");
		newEntry.set_Command("None");
		worldVIFile.getFileEntries()[patchName] = newEntry; // add and or replace entry

		if (worldVIFile.writeFile(patchVersionInfoPath) == false) {
			fatalError = true;
			errorString = "Patch generation failed.";
			LogError("worldVIFile.writeFile() FAILED");
		} else {
			m_server->RegisterAppPatchURL();
		}

		if (m_compileLua) {
			if (STLEndWithIgnoreCase(sourcePath, ".luac")) {
				::DeleteFileW(Utf8ToUtf16(sourcePath).c_str());
			} else {
				jsAssert(false);
			}
		}
		m_publishSync.unlock();
	}

	// Pad result to completion event
	(*m_completionEvent->OutBuffer()) << !fatalError;
	(*m_completionEvent->OutBuffer()) << errorString;

	m_server->SendToClient(m_completionEvent);
	return;
}

} // namespace KEP
