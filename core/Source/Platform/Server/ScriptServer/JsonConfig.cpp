///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "JsonConfig.h"
#include "LuaHelper.h"
#include <algorithm>
#include <sstream>
using namespace std;

namespace KEP {

JsonConfig::FileNameMapper::FileNameMapper(const json11::Json& json, const std::vector<std::string>& configGroups, const char* rootNodeName) :
		Accessor(json) {
	jsVerifyReturnVoid(rootNodeName != NULL);
	for (auto group : configGroups) {
		auto aliasesNode = json[group][rootNodeName];
		if (aliasesNode.is_object()) {
			for (auto alias : aliasesNode.object_items()) {
				jsAssert(alias.second.is_string());
				if (alias.second.is_string()) {
					string key = alias.first;
					std::transform(key.begin(), key.end(), key.begin(), ::tolower);
					m_aliasMap[key] = alias.second.string_value();
				}
			}
		}
	}
}

std::string
JsonConfig::FileNameMapper::operator()(const std::string& path) {
	string key = path;
	std::transform(key.begin(), key.end(), key.begin(), ::tolower);

	auto itr = m_aliasMap.find(key);
	if (itr != m_aliasMap.end()) {
		return itr->second;
	}

	auto ofsSlash = path.rfind("\\");
	if (ofsSlash == std::string::npos) {
		ofsSlash = path.rfind("/");
	}

	if (ofsSlash != std::string::npos && ofsSlash != path.size() - 1) {
		itr = m_aliasMap.find(key.substr(ofsSlash + 1));
		if (itr != m_aliasMap.end()) {
			return path.substr(0, ofsSlash + 1) + itr->second;
		}
	}

	return path;
}

JsonConfig::LuaScriptConfigurator::LuaScriptConfigurator(const json11::Json& json, const std::vector<std::string>& configGroups) :
		Accessor(json), m_configGroups(configGroups) {
}

void JsonConfig::LuaScriptConfigurator::operator()(lua_State* L) {
	for (auto group : m_configGroups) {
		const json11::Json& settingsNode = m_json[group][GS_TAG_SETTINGS][GS_TAG_SERVERCONFIGS];
		if (settingsNode.is_object()) {
			luaPushJsonNodeValue(L, settingsNode);
			lua_setglobal(L, GS_TAG_SERVERCONFIGS);
		}
	}
}

void JsonConfig::LuaScriptConfigurator::luaPushJsonNodeValue(lua_State* L, const json11::Json& node) {
	if (node.is_object()) {
		lua_newtable(L);
		for (auto child : node.object_items()) {
			luaPushJsonNodeValue(L, child.second);
			lua_setfield(L, -2, child.first.c_str());
		}
	} else if (node.is_array()) {
		lua_newtable(L);
		size_t arrayIndex = 0;
		for (auto child : node.array_items()) {
			luaPushJsonNodeValue(L, child);
			lua_rawseti(L, -2, arrayIndex++);
		}
	} else if (node.is_string()) {
		lua_pushstring(L, node.string_value().c_str());
	} else if (node.is_number()) {
		lua_pushnumber(L, node.number_value());
	} else if (node.is_bool()) {
		lua_pushboolean(L, node.bool_value() ? 1 : 0);
	} else {
		// nil or other unknown nodes
		lua_pushnil(L);
	}
}

JsonConfig::ConfigProvider::ConfigProvider(const json11::Json& json, const std::vector<std::string>& configGroups, const char* rootNodeName) :
		Accessor(json) {
	jsVerifyReturnVoid(rootNodeName != NULL);
	for (auto group : configGroups) {
		scanNode(json[group][GS_TAG_SETTINGS][rootNodeName], rootNodeName);
	}
}

std::string
JsonConfig::ConfigProvider::operator()(const std::string& keyPath, const std::string* defaultValue) {
	auto itr = m_configMap.find(keyPath);
	if (itr != m_configMap.end()) {
		return itr->second;
	}

	return defaultValue ? *defaultValue : "";
}

void JsonConfig::ConfigProvider::scanNode(const json11::Json& node, const std::string& keyPath) {
	if (node.is_object()) {
		for (auto child : node.object_items()) {
			const std::string& childName = child.first;

			// Validate lowercase key token
			string token = childName;
			std::transform(token.begin(), token.end(), token.begin(), ::tolower);

			if (strspn(token.c_str(), GS_VALID_KEY_PATH_CHAR) == token.length()) {
				// Valid
				scanNode(child.second, keyPath + "." + childName);
			} else {
				// Contains invalid characters
				jsAssert(false);
			}
		}
	} else if (node.is_array()) {
		size_t arrayIndex = 0;
		for (auto child : node.array_items()) {
			stringstream ss;
			ss << "[" << arrayIndex << "]";
			scanNode(child, keyPath + ss.str());
		}
	} else if (node.is_string()) {
		m_configMap[keyPath] = node.string_value();
	} else if (node.is_number()) {
		stringstream ss;
		ss << node.number_value();
		m_configMap[keyPath] = ss.str();
	} else if (node.is_bool()) {
		m_configMap[keyPath] = node.bool_value() ? "true" : "false";
	} else {
		// nil or other unknown nodes
	}
}

} // namespace KEP