///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "LuaHelper.h"
#include <string>
#include <tuple>

namespace KEP {

//////////////////////////////////////////////////////////////
// Push a C value onto Lua stack
template <typename T>
inline void lua_pushAny(lua_State* L, const T& data);

typedef const char* c_sz;

// Specializations for lua_pushAny
template <>
inline void lua_pushAny(lua_State* L, const std::string& data) {
	lua_pushstring(L, data.c_str()); // STL string
}
template <>
inline void lua_pushAny(lua_State* L, const c_sz& data) {
	lua_pushstring(L, data); // C string
}
template <>
inline void lua_pushAny(lua_State* L, const double& data) {
	lua_pushnumber(L, data); // double
}
template <>
inline void lua_pushAny(lua_State* L, const float& data) {
	lua_pushnumber(L, (double)data); // float
}
template <>
inline void lua_pushAny(lua_State* L, const int& data) {
	lua_pushinteger(L, data); // integer
}
template <>
inline void lua_pushAny(lua_State* L, const bool& data) {
	lua_pushboolean(L, data ? 1 : 0); // bool
}

//////////////////////////////////////////////////////////////
// Get a C value from Lua stack
template <typename T>
inline T lua_toAny(lua_State* L, int index);

// Specializations for lua_toAny
template <>
inline std::string lua_toAny(lua_State* L, int index) {
	const char* str = lua_tostring(L, index); // STL string
	return str ? str : std::string();
}
template <>
inline const char* lua_toAny(lua_State* L, int index) {
	return lua_tostring(L, index); // C string
}
template <>
inline double lua_toAny(lua_State* L, int index) {
	return lua_tonumber(L, index); // double
}
template <>
inline float lua_toAny(lua_State* L, int index) {
	return (float)lua_tonumber(L, index); // float
}
template <>
inline int lua_toAny(lua_State* L, int index) {
	return lua_tointeger(L, index); // integer
}
template <>
inline bool lua_toAny(lua_State* L, int index) {
	return lua_toboolean(L, index) != 0; // bool
}

//////////////////////////////////////////////////////////////
// Set a table field by key
template <typename V, typename K>
inline void lua_setField(lua_State* L, K key, V val, int tableIndex = -1) {
	lua_pushAny(L, key);
	lua_pushAny(L, val);
	lua_settable(L, tableIndex >= 0 ? tableIndex : tableIndex - 2);
}

//////////////////////////////////////////////////////////////
// Get a table field by key
template <typename V, typename K>
inline V lua_getField(lua_State* L, K key, int tableIndex) {
	lua_pushAny(L, key);
	lua_gettable(L, tableIndex >= 0 ? tableIndex : tableIndex - 1);
	return lua_toAny<V>(L, -1);
}

// Specialize lua_getField when string key is used
template <typename V>
inline V lua_getField(lua_State* L, const char* key, int tableIndex) {
	lua_getfield(L, tableIndex, key);
	return lua_toAny<V>(L, -1);
}

//////////////////////////////////////////////////////////////
// Push all members of a tuple onto lua stack, one by one
template <typename Tuple, int startingIndex, int remaining = tuple_size<Tuple>::value>
struct LuaTuplePushHelper {
	void operator()(lua_State* L, const Tuple& data) {
		lua_pushAny(L, get<startingIndex>(data));
		LuaTuplePushHelper<Tuple, startingIndex + 1, remaining - 1>()(L, data);
	}
};

// Specialization for LuaTuplePushHelper
template <typename Tuple, int startingIndex>
struct LuaTuplePushHelper<Tuple, startingIndex, 0> {
	void operator()(lua_State*, const Tuple&) {}
};

// wrapper function
template <typename Tuple>
void lua_pushTuple(lua_State* L, const Tuple& tup) {
	LuaTuplePushHelper<Tuple, 0>()(L, tup);
}

} // namespace KEP
