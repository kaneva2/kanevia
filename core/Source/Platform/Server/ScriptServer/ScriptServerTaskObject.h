///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "TaskObject.h"
#include <string>

#define SSTO_NONAME "noName"

namespace KEP {

static unsigned long nextID = 0;

class ScriptServerEngine;

class ScriptServerTaskObject : public TaskObject {
public:
	ScriptServerTaskObject(const char* name = SSTO_NONAME) :
			m_id(nextID++),
			m_name(name ? name : SSTO_NONAME) {
	}

	unsigned long GetID() const {
		return m_id;
	}
	std::string GetName() const {
		return m_name;
	}
	void SetServer(ScriptServerEngine* server) {
		m_server = server;
	}

private:
	unsigned long m_id;
	std::string m_name;

protected:
	ScriptServerEngine* m_server;
};

}; // namespace KEP
