///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "LuaVMConfig.h"
#include "InstanceId.h"
#include "Core/Util/HighPrecisionTime.h"
#include "Event/Events/ScriptServerEvent.h"
#include "ScriptEventArgs.h"

#include <list>
#include <memory>
#include <string>
#include <functional>

struct lua_State;
class IScriptArgManager;

namespace KEP {

class IEvent;
class GameStateManager;
class ScriptClientEvent;
class ScriptServerTaskObject;
class ScriptAPIRegistry;
class ScriptZone;
class ServerScript;
typedef std::shared_ptr<ServerScript> ServerScriptPtr;

enum class ScriptState {
	STARTING, // Initialization in progress (event queue disabled)
	IDLE, // Nothing pending, waiting to be activated by an event.
	READY, // On the scheduler queue, waiting to be scheduled
	SCHEDULED, // On the task queue, waiting to be run
	RUNNING, // Script is running on one of the worker threads
	BLOCKED, // Waiting for an API to be processed
	YIELDED, // Script is preempted/has yieled, to be rescheduled
	ZOMBIE, // Not running or processing incoming events, but not removed yet either.
	STOPPED, // Script is stopped (manually or by zero-population). It will be removed by scriptMap very soon
	NUM_STATES
};

class ServerScript {
public:
	enum ScriptError {
		NONE,
		RUNTIME_ERROR,
		LOAD_ERROR,
		CALL_ERROR,
		TIMEOUT_ERROR
	};

	// Return value from LuaPreCall and LuaResumeBlockingAPI (= numArgs if >=0)
	enum PreCallRes {
		// must be < 0
		PCR_SKIPPED = -1, // Not called (e.g. script has no handler)
		PCR_ERROR = -2, // Error occurred
	};

	// Return value from lua_yield
	enum YieldType {
		// NOTE: value of YLD_COMPLETED must match threadLoop code in LuaRunScript()
		YLD_COMPLETED = 0, // BLOCKED / COMPLETED will be pushed to stack before lua_yield is called
		YLD_BLOCKED,
		YLD_PREMPTED, // If prempted by count hook, we will not push this value into stack. lua_yield gives no return value instead (lua_gettop() == 0)
	};

	//
	// When adding to this, remember to update the list of handlers defined
	// in ServerScript.cpp.
	//
	enum ScriptEventBit {
		NULL_BIT = 0,
		START_BIT = 1 << 0,
		TOUCH_BIT = 1 << 1,
		TIMER_BIT = 1 << 2,
		ARRIVE_BIT = 1 << 3,
		DEPART_BIT = 1 << 4,
		MESSAGE_BIT = 1 << 5,
		MENU_BIT = 1 << 6,
		TRIGGER_BIT = 1 << 7,
		COLLIDE_BIT = 1 << 8,
		LC_PLAYER_BIT = 1 << 9,
		RC_PLAYER_BIT = 1 << 10,
		LC_OBJECT_BIT = 1 << 11,
		RC_OBJECT_BIT = 1 << 12,
		MENU_BUTTON_BIT = 1 << 13,
		MENU_CLOSED_BIT = 1 << 14,
		MENU_KEYDOWN_BIT = 1 << 15,
		MENU_LIST_SELECTION_BIT = 1 << 16,
		MENU_KEYUP_BIT = 1 << 17,
		MENU_SHOP_BUY_BIT = 1 << 18,
		STOP_BIT = 1 << 19,
		RESULT_BIT = 1 << 20,
		UNLOAD_BIT = 1 << 21, // unload script, set for all scripts
		ELAPSED_BIT = 1 << 22, // kgp_elapsed
	};

	// Pending job recorded by LuaYield() call
	struct LuaYieldInfo {
		bool block; // true if RunVMTask should block this script when returned from lua_resume
		ScriptServerEvent::EventType unblockEventType; // Expected event type for unblocking the script
		std::string reason; // Blocking reason
		IEvent* eventToSend; // Event to queue before entering blocking state (either a task or a event, but not both)
		ScriptServerTaskObject* taskToQueue; // Task to queue before entering blocking state (either a task or a event, but not both)

		LuaYieldInfo() :
				block(false), unblockEventType(ScriptServerEvent::None), eventToSend(NULL), taskToQueue(NULL) {}
		void clear() {
			block = false;
			unblockEventType = ScriptServerEvent::None;
			reason.clear();
			eventToSend = NULL;
			taskToQueue = NULL;
		}
	};

	// Pending reload request
	struct ReloadRequest {
		TimeMs timestamp;
		TimeMs ttl;
		ULONG clientNetId;
		std::string fileName;

		ReloadRequest() :
				timestamp(0), ttl(0), clientNetId(0) {}
		void clear() {
			timestamp = 0;
			ttl = 0;
			clientNetId = 0;
			fileName.clear();
		}
	};

#define START_HANDLER "kgp_start"
#define STOP_HANDLER "kgp_stop"
#define TOUCH_HANDLER "kgp_touch"
#define TIMER_HANDLER "kgp_timer"
#define ARRIVE_HANDLER "kgp_arrive"
#define DEPART_HANDLER "kgp_depart"
#define MESSAGE_HANDLER "kgp_message"
#define MENUEVENT_HANDLER "kgp_menuevent"
#define TRIGGER_HANDLER "kgp_trigger"
#define COLLIDE_HANDLER "kgp_collide"
#define LCLICK_PLAYER_HANDLER "kgp_playerLeftClick"
#define LCLICK_OBJECT_HANDLER "kgp_objectLeftClick"
#define RCLICK_PLAYER_HANDLER "kgp_playerRightClick"
#define RCLICK_OBJECT_HANDLER "kgp_objectRightClick"
#define MENU_BUTTON_HANDLER "kgp_menuButton"
#define MENU_CLOSED_HANDLER "kgp_menuClosed"
#define MENU_KEY_DOWN_HANDLER "kgp_menuKeyDown"
#define MENU_LIST_SELECTION_HANDLER "kgp_menuListSelection"
#define MENU_KEY_UP_HANDLER "kgp_menuKeyUp"
#define MENU_SHOP_BUY_HANDLER "kgp_menuShopBuy"
#define ASYNC_RESULT_HANDLER "kgp_result" // Response from an asynchronous API invocation. See kgp.scriptCallAsync.
#define ELAPSED_HANDLER "kgp_elapsed" // Time based call-back: e.g. effect completion

	ServerScript(const LuaVMConfig& luaVMConfig, const std::shared_ptr<GameStateManager>& pGSM, const std::shared_ptr<ScriptZone>& pScriptZone,
		ULONG placementId, bool isSystemScript, int assetType, const char* fileName);
	~ServerScript();

	ULONG GetPlacementId() const { return m_placementId; }
	ZoneIndex GetZoneId() const;
	std::string GetFileName() const { return m_fileName; }
	int GetAssetType() const { return m_assetType; }

	void SetAcceptsInZoneBroadcast(bool val) { m_acceptsInZoneBroadcast = val; }
	bool AcceptsInZoneBroadcast() const { return m_acceptsInZoneBroadcast; }

	void SetAcceptsInterZoneBroadcast(bool val) { m_acceptsInterZoneBroadcast = val; }
	bool AcceptsInterZoneBroadcast() const { return m_acceptsInterZoneBroadcast; }

	bool HasHandlerFor(ULONG eventMask) { return (m_handlerBitField & eventMask) != 0; }

	std::string GetCurrentGlueName() const { return m_currGlueName; }
	ULONG GetCurrentMemory() const { return m_currentMemory; }

	bool AddEvent(ScriptServerEvent* se, bool enableQueue);
	bool CheckReadiness(size_t& numArgs, bool& unblocked, bool& resumed); // READY | BLOCKED | YIELDED -> READY
	bool ProcessEvent(ScriptServerEvent* se, size_t& numArgs);

	ScriptState GetState() const {
		std::lock_guard<std::mutex> lock(m_stateMutex);
		return m_state;
	}

	bool SetState(ScriptState state, ScriptState& oldState) {
		std::lock_guard<std::mutex> lock(m_stateMutex);
		return SetStateNoLock(state, oldState);
	}

	bool SetState(ScriptState state, ScriptServerEvent::EventType unblockEventType, const std::string& reason, ScriptState& oldState) {
		std::lock_guard<std::mutex> lock(m_stateMutex);
		return SetStateNoLock(state, unblockEventType, reason, oldState);
	}

	DWORD GetTimeInState(ScriptState state) const { return m_times[static_cast<size_t>(state)]; }
	TimeMs GetTotalTime() const { return m_timerStart.ElapsedMs(); }

	void ResetYieldCount() { m_recentYieldCount = 0; }
	void IncrementYieldCount() { m_recentYieldCount++; }
	DWORD GetYieldCount() { return m_recentYieldCount; }

	bool IsStopping() const { return m_stopping; }
	void SetStopping() {
		m_stopping = true;
		m_stoppingTime = fTime::TimeMs();
	}

	DWORD WaitForScriptToStop(TimeMs timeout) { return WaitForSingleObject(m_stoppedEvent, (DWORD)timeout); }
	TimeMs GetElapsedStoppingTime() const { return fTime::ElapsedMs(m_stoppingTime); }

	bool GetBlockingInfo(TimeMs& duration, std::string& reason);

	void SetError(int code, const std::string& msg, bool setZombie = true);
	void GetError(int& code, std::string& msg);

	bool IsSystemScript() const { return m_isSystemScript; }

	// Obtain Lua source info at current PC
	void LuaGetSourceInfo(int stackLevel, std::string& source, int& lineNumber) const;

	// Initialize Lua global state, populate APIs, global constants and etc.
	void LuaVMInit(const char* filename, const ScriptAPIRegistry* scriptAPIs);

	// Load script into VM global state
	int LuaLoadScript(bool loadJson, const std::string& sourceCode, std::string& errMsg);

	// Run loaded script in VM global state
	int LuaRunScript(std::string& errMsg);

	// Run a C function in the global state to complete the pending API. The function argument is expected to schedule a thread to resume the script.
	int LuaResumeBlockingAPI(std::function<void(lua_State*)> func);

	// Run a in-script event handler function that is already on the stack. Return true if a handler is found and scheduled to run.
	int LuaPreCall(const char* glueName, std::function<void(lua_State*)> func);

	// Execute event handler
	int LuaCall(int numberOfArgs, bool callFunction, std::string& errMsg);

	// Call back from engine indicating the current event handler task is completed
	bool LuaCallTaskCompleted();

	// Dump Lua Stack
	void LuaStackDump();

	// Resume Lua execution
	int LuaResume(int numberOfArgs, std::string& errMsg);

	// Record yield information then call lua_yield
	int LuaYield(ScriptServerEvent::EventType unblockEventType, const std::string& reason, IEvent* e, ScriptServerTaskObject* task = NULL);

	// Retrieve and clear previously recorded yield information. Return LuaYieldInfo.block
	bool RetrieveYieldInfo(ScriptServerEvent::EventType& unblockEventType, std::string& reason, IEvent*& eventToSend, ScriptServerTaskObject*& taskToQueue);

	// Return LuaYieldInfo.block
	bool BlockPending() const { return m_yieldInfo.block; }

	// Queue a request to reload current script once it's terminated and unloaded
	bool QueueReloadRequest(ULONG clientNetId, const std::string& fileName, TimeMs timeout);

	// Return true if a pending reload request was previously queued and not yet expired
	bool FetchPendingReloadRequest(ULONG& clientNetId, std::string& fileName);

	// Convert script state into string
	static const char* GetStateName(ScriptState state);

	const std::shared_ptr<GameStateManager>& GetGameStateManager() const { return m_pGameStateManager; }
	const std::shared_ptr<ScriptZone>& GetScriptZone() const { return m_pZone; }

	bool TrackLuaMemoryAllocation(size_t oldSize, size_t newSize) {
		unsigned int new_memory = m_currentMemory + (newSize - oldSize);
		if (new_memory > m_luaVMConfig.getScriptMemoryLimit()) {
			return false;
		} else {
			m_currentMemory += newSize - oldSize;
			return true;
		}
	}

	void SetCurrentTask(const ScriptServerTaskObject* task) { m_currTask = task; }

	std::string LogSubject() const { return m_logSubject; }
	std::string LogSig() const { return m_logSig; }

	// Return Lua global state
	lua_State* LuaGlobalState() const {
		return m_luaMain;
	}

	// Return Lua thread state
	lua_State* LuaThreadState() const {
		return m_luaThread;
	}

	// Store zone index in LUA registry
	static void StoreZoneIndexInRegistry(lua_State* L, const ZoneIndex& zoneIndex);

	// Retrieve zone index
	static ZoneIndex GetZoneIndexFromRegistry(lua_State* L);

	void ResetCallCompletionEvent() { ::ResetEvent(m_callCompletionEvent); }
	bool WaitForCallCompletion(TimeMs timeout); // return true if completed, false if timeout or error.
	void DisableCountHook();
	void EnableCountHook();

	static bool InitEventHandlerMap();
	static bool IsScriptEvent(ScriptServerEvent::EventType eventType) { return GetEventHandlerInfo(eventType) != nullptr; }

private:
	void PopulateRestAPIDataForLua(lua_State* L);
	void EnableLuaMemoryTracking(lua_State* L);
	void SetCurrentGlueName(const std::string& glueName);
	bool SetStateNoLock(ScriptState state, ScriptState& oldState) {
		assert(state != ScriptState::BLOCKED);
		return SetStateNoLock(state, ScriptServerEvent::None, "", oldState);
	}
	bool SetStateNoLock(ScriptState state, ScriptServerEvent::EventType unblockEventType, const std::string& reason, ScriptState& oldState);

	void Unload(); // From ScriptServerEngine::UnloadScript
	void CachePlayerInfoInLuaRegistry(lua_State* L, NETID netID, const ScriptPlayerInfo& playerInfo); // for kgp_depart

	// Script event handler specialized by event type (specializations in ServerScript.cpp)
	template <ScriptServerEvent::EventType eventType>
	struct TEventHandler { static constexpr bool Specialized = false; };

	// Default AddEventHandler
	template <ScriptServerEvent::EventType eventType>
	static std::enable_if_t<!TEventHandler<eventType>::Specialized> AddEventHandler() {
		// Do nothing if TEventHandler is NOT specialized on eventType
		static_assert(!TEventHandler<eventType>::Specialized, "Sanity check failed");
	}

	// AddEventHandler for specialized handlers
	template <ScriptServerEvent::EventType eventType>
	static std::enable_if_t<TEventHandler<eventType>::Specialized> AddEventHandler() {
		// Add handler if TEventHandler is specialized on eventType
		HandlerInfo handlerInfo{
			[](ServerScript* pScript, ScriptServerEvent* se) { return CallEventHandlerWithArgs(se, &TEventHandler<eventType>::Handle, pScript); },
			TEventHandler<eventType>::HandlerBit,
			TEventHandler<eventType>::HandlerName,
		};
		s_eventHandlerInfoMap.insert(std::make_pair(eventType, handlerInfo));
	}

	typedef std::function<int(ServerScript*, ScriptServerEvent*)> EventHandlerFunc;

	struct HandlerInfo {
		EventHandlerFunc func;
		const ScriptEventBit bit; // the bit used to record the existance of the handler
		const char* name; // the name of the handler in LUA's global namespace.
	};

	static const HandlerInfo* GetEventHandlerInfo(ScriptServerEvent::EventType eventType) {
		auto itr = s_eventHandlerInfoMap.find(eventType);
		if (itr != s_eventHandlerInfoMap.end()) {
			return &itr->second;
		}
		return nullptr;
	}

private:
	mutable std::mutex m_stateMutex; // Protects following fields
	// === BEGIN protected by m_stateMutex ===
	ScriptState m_state; // Script state (requiring sequential consistency, set and read by event handler and VM threads)
	std::list<ScriptServerEvent*> m_eventQueue; // Script event queue
	ScriptServerEvent* m_unblockEvent; // Received unblock event (to be processed)
	TimeMs m_blockedAt;
	ScriptServerEvent::EventType m_unblockEventType; // The expected event type to unblock current script
	std::string m_blockingReason;
	std::string m_errorMsg; // Additional error info
	int m_errorCode;
	// === END protected by m_stateMutex ===

	Timer m_timerStart;
	Timer m_timerStateChange;
	DWORD m_times[ScriptState::NUM_STATES]; // store total time in each state since startTime

	DWORD m_recentYieldCount; // Counter of preemptive yields since most recent event handler call
	std::atomic<bool> m_stopping; // true if script is being shut down (requiring sequential consistency, set by event handler thread, read by EH + VM threads)
	std::atomic<DWORD> m_stoppingTime; // Timestamp when stopping is set to TRUE (along with m_stopping)
	HANDLE m_stoppedEvent; // Event to signal when script is stopped
	HANDLE m_callCompletionEvent; // A event signaling when a event callback is completed
	bool m_isSystemScript;
	LuaYieldInfo m_yieldInfo; // Pending job recorded by LuaYield() call
	ReloadRequest m_pendingReloadRequest; // Carry information when a script reload request is pending (i.e. current script is being stopped)
	std::shared_ptr<GameStateManager> m_pGameStateManager;
	std::string m_logSubject;
	std::string m_logSig;
	const LuaVMConfig& m_luaVMConfig; // VM options
	lua_State* m_luaMain; // Lua global state
	lua_State* m_luaThread; // Coroutine state
	const ScriptServerTaskObject* m_currTask; // task processing current event handler call (RunVMTask)
	ULONG m_placementId;
	std::shared_ptr<ScriptZone> m_pZone;
	ULONG m_handlerBitField; // When loaded, we check to see which handlers the VM defines.
	ULONG m_currentMemory;
	std::string m_fileName;
	std::string m_currGlueName;
	int m_assetType; // ScriptServerEvent::UploadAssetTypes - UndefinedAssetType (for Controllers), SavedScript, ActionItem, ActionInstance, ActionItemParams or ActionInstanceParams
	std::atomic<bool> m_acceptsInZoneBroadcast; // kgp_message callback policies
	std::atomic<bool> m_acceptsInterZoneBroadcast;

	typedef std::map<ScriptServerEvent::EventType, HandlerInfo> EventHandlerInfoMap;
	static EventHandlerInfoMap s_eventHandlerInfoMap; // EventType -> Handler Information
};

}; // namespace KEP
