///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <fstream>
#include <string>
#include "SaveScriptTask.h"
#include "MySqlUtil.h"
#include "ScriptServerEngine.h"
#include "param_ids.h"
#include "LogHelper.h"

using namespace mysqlpp;

namespace KEP {

static LogInstance("Instance");
static Logger alert_logger(Logger::getInstance(L"Alert"));

SaveScriptTask::SaveScriptTask(
	//	log4cplus::Logger& logger,
	KGP_Connection* conn,
	fast_recursive_mutex& dbSync,
	ULONG objPlacementId,
	const std::string& filename) :
		ScriptServerTaskObject("SaveScriptTask"),
		//	m_logger(logger),
		m_conn(conn),
		m_dbSync(dbSync),
		m_objPlacementId(objPlacementId),
		m_filename(filename) {}

/*!
* \brief
* Write brief comment for ExecuteTask here.
*
* \throws <exception class>
* Description of criteria for throwing this exception.
*
* Write detailed description for ExecuteTask here.
*
* \remarks
* Write remarks for ExecuteTask here.
*
* \see
* Separate items with the '|' character.
*/
void SaveScriptTask::ExecuteTask() {
	std::ostringstream fileNameStream;
	fileNameStream << m_filename;
	const std::string& fileName = fileNameStream.str();

	// write the DB record
	std::lock_guard<fast_recursive_mutex> lock(m_dbSync);

	BEGIN_TRY_SQL_FOR("Save Server Script");

	Transaction trans(*m_conn);
	Query query = m_conn->query();

	query << "INSERT INTO dynamic_object_parameters "
			 "(obj_placement_id, param_type_id, value) "
			 "VALUES (%0, %1, %2q) "
			 "ON DUPLICATE KEY UPDATE value = %2q;"
			 "INSERT INTO dynamic_object_parameters "
			 "(obj_placement_id, param_type_id, value) "
			 "VALUES (%0, %3, NOW()) "
			 "ON DUPLICATE KEY UPDATE value = NOW();";
	query.parse();

	//	_LogTrace("SaveScriptTask: " << query.str((unsigned int) m_objPlacementId, PARAM_SCRIPT_FILE_PATH, fileName.c_str(), PARAM_SCRIPT_CREATED_DATE));
	query.execute((unsigned int)m_objPlacementId, PARAM_SCRIPT_FILE_PATH, fileName.c_str(), PARAM_SCRIPT_CREATED_DATE);

	//This must be done when making multiple calls or else you get the dreaded 'commands out of sync' error.
	checkMultipleResultSets(query, "SaveScriptTask::ExecuteTask");

	trans.commit();

	END_TRY_SQL_LOG_ONLY(m_conn);
}

} // namespace KEP
