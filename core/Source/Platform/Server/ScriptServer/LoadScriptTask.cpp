///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <fstream>
#include <string>
#include "LoadScriptTask.h"
#include "MySqlUtil.h"
#include "ScriptServerEngine.h"
#include "param_ids.h"
#include "JsonConfig.h"
#include "HttpClient\GetBrowserPage.h"
#include "LogHelper.h"

using namespace mysqlpp;

namespace KEP {

static LogInstance("Instance");
static Logger alert_logger(Logger::getInstance(L"Alert"));

LoadScriptTask::LoadScriptTask(
	//	log4cplus::Logger& logger,
	KGP_Connection* conn,
	fast_recursive_mutex& dbSync,
	ZoneIndex zoneId,
	NETID clientNetId,
	const std::string& playerName,
	bool loadSystemScripts,
	bool loadSystemScriptsSynch,
	DWORD kgpStartWaitTimeout) :
		ScriptServerTaskObject("LoadScriptTask"),
		//	m_logger(logger),
		m_conn(conn),
		m_dbSync(dbSync),
		m_zoneId(zoneId),
		m_clientNetId(clientNetId),
		m_playerName(playerName),
		m_loadSystemScripts(loadSystemScripts),
		m_loadSystemScriptsSynch(loadSystemScriptsSynch),
		m_kgpStartWaitTimeout(kgpStartWaitTimeout),
		m_stopping(false) {}

/*!
* \brief
* Write brief comment for ExecuteTask here.
*
* \throws <exception class>
* Description of criteria for throwing this exception.
*
* Write detailed description for ExecuteTask here.
*
* \remarks
* Write remarks for ExecuteTask here.
*
* \see
* Separate items with the '|' character.
*/
void LoadScriptTask::ExecuteTask() {
	//	_LogTrace("LoadScriptTask: " << GetName() << " has id of " << GetID() << ", zone " << m_zoneId.toLong());

	struct ScriptInfo {
		std::string fileName;
		ULONG placementId;
		bool isSystemScript;
	};
	std::vector<ScriptInfo> allScripts;

	// AB Test Helper - to be moved to ScriptZone class
	std::vector<std::string> configGroups; // Use vector to guarantee processing order
	configGroups.push_back("global");

	// Query WokWeb for experiment groups this zone is associated with
	/**
	GET http://dev-wok.kaneva.com/kgp/server/worldExperiment.aspx?action=getActiveExperimentGroups&assignAutomatically=Y&zoneInstanceId=743002&zoneType=3

	<Result>
	<ReturnCode>0</ReturnCode>
	<ReturnDescription>success</ReturnDescription>
	<ArrayOfWorldExperimentGroup xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<WorldExperimentGroup>
	<WorldGroupId>766dbc6e-15f4-11e5-aa91-002219919c2a</WorldGroupId>
	<ZoneInstanceId>743002</ZoneInstanceId>
	<ZoneType>3</ZoneType>
	<GroupId>1f05d08b-15f4-11e5-aa91-002219919c2a</GroupId>
	<AssignmentDate>2015-06-18T15:59:00</AssignmentDate>
	<ConversionDate xsi:nil="true" />
	</WorldExperimentGroup>
	</ArrayOfWorldExperimentGroup>
	</Result>
	**/
	const ULONG XML_SNIPPET_LEN = 80;
	std::stringstream ssUrl;
	ssUrl << m_server->m_worldExperimentUrl
		  << "&zoneInstanceId=" << (unsigned)m_zoneId.GetInstanceId()
		  << "&zoneType=" << m_zoneId.GetType();
	std::string resultText, httpStatusDesc;
	DWORD httpStatusCode = 0;
	size_t resultSize = 0;
	if (GetBrowserPage(ssUrl.str(), resultText, resultSize, httpStatusCode, httpStatusDesc) && (httpStatusCode == HTTP_STATUS_OK)) {
		TiXmlDocument doc;
		doc.Parse(resultText.c_str());
		if (doc.Error()) {
			std::string temp(resultText, std::min<ULONG>(resultText.size(), XML_SNIPPET_LEN));
			LogError("Error parsing XML from worldExperiment.aspx: " << doc.ErrorDesc() << " " << temp << "...");
		} else {
			auto root = doc.FirstChildElement("Result");
			auto node = root ? root->FirstChildElement("ReturnCode") : NULL;
			if (root && node && strcmp(node->GetText(), "0") == 0) {
				node = root->FirstChildElement("ArrayOfWorldExperimentGroup");
				if (node) {
					for (auto groupNode = node->FirstChildElement("WorldExperimentGroup"); groupNode != NULL; groupNode = groupNode->NextSiblingElement()) {
						auto groupIdNode = groupNode->FirstChildElement("GroupId");
						jsAssert(groupIdNode && groupIdNode->GetText());
						if (groupIdNode && groupIdNode->GetText()) {
							std::string groupId(groupIdNode->GetText());
							configGroups.push_back(groupId);
						}
					}
				}
			} else {
				LogWarn("Bad result from worldExperiment.aspx: ReturnCode is " << (node ? node->GetText() : "(null)") << ", expected: 0");
			}
		}
	} else {
		LogError("Error calling worldExperiment.aspx. HTTP status: " << httpStatusCode << " (" << httpStatusDesc << "). Zone instance ID: " << (unsigned)m_zoneId.GetInstanceId() << ", zone type: " << m_zoneId.GetType());
	}

	// Save config groups for future reference
	m_server->SetZoneConfigGroups(m_zoneId, configGroups);

	LoadZoneSpinDownDelayMinutesFromDB(m_zoneId);

	//
	// Load all the scripts for the given zoneId.
	// I probably need to change the name of the task object.
	//
	{
		std::lock_guard<fast_recursive_mutex> lock(m_dbSync);
		BEGIN_TRY_SQL_FOR("Load Server Script");

		Transaction trans(*m_conn);
		Query query = m_conn->query();
		if (m_loadSystemScripts) {
			query << "(SELECT script, obj_placement_id, 1 AS system FROM kgp_common.preload_scripts ORDER BY obj_placement_id) "
					 "UNION ALL (";
		}
		query << "SELECT value as script, do.obj_placement_id, 0 AS system "
				 " FROM dynamic_object_parameters dop "
				 " INNER JOIN dynamic_objects do "
				 "  ON do.zone_index = %0 "
				 "  AND do.zone_instance_id = %1 "
				 "  AND do.obj_placement_id = dop.obj_placement_id "
				 "  AND param_type_id = %2 "
				 " ORDER BY do.obj_placement_id";
		if (m_loadSystemScripts) {
			query << ")";
		}

		query.parse();

		ZoneIndex zi = m_zoneId.GetBaseZoneIndex();
		unsigned int zone_index = zi.getClearedInstanceId();
		unsigned int zone_instance_id = zi.GetInstanceId();

		//		_LogDebug("LoadScriptTask: " << query.str(zone_index, zone_instance_id, PARAM_SCRIPT_FILE_PATH));
		StoreQueryResult res = query.store(zone_index, zone_instance_id, PARAM_SCRIPT_FILE_PATH);

		//
		// Retrieve the highest placement id from the DB for this zone.
		// It will be used when generating dynamic objects via script.
		// Adding 100000 to the max in order to prevent edge case error
		// of arena creator placing an object while script is generating
		// object
		//
		if (m_server->GetZoneMaxPlacementId(m_zoneId) == 0) {
			ULONG maxPlacementId = 0;
			Query selectMaxId = m_conn->query();
			selectMaxId << "SELECT COALESCE(MAX(obj_placement_id), 0) + 100000 as max_placement_id "
						   "FROM dynamic_objects "
						   "WHERE zone_index = %0 "
						   "AND zone_instance_id = %1";
			selectMaxId.parse();

			//			_LogDebug("LoadScriptTask: " << selectMaxId.str(zone_index, zone_instance_id));
			StoreQueryResult selectMaxIdresult = selectMaxId.store(zone_index, zone_instance_id);

			if (selectMaxIdresult.num_rows() > 0) {
				maxPlacementId = selectMaxIdresult.at(0).at(0);
			}

			m_server->SetZoneMaxPlacementId(m_zoneId, maxPlacementId);
		}

		allScripts.reserve(res.num_rows());
		for (size_t i = 0; i < res.num_rows(); ++i) {
			ScriptInfo info;
			info.fileName = (const char*)res.at(i).at(0);
			info.isSystemScript = (int)res.at(i).at(2) != 0;
			if (info.isSystemScript) {
				// System scripts: reassign placement IDs
				info.placementId = m_server->GetIncrementedZoneMaxPlacementId(m_zoneId);
			} else {
				// User scripts: Use placement IDs from DB
				info.placementId = (ULONG)res.at(i).at(1);
			}
			allScripts.push_back(info);
		}

		END_TRY_SQL_LOG_ONLY(m_conn);
	}

	// System scripts will be loaded first because they are returned in the result set before any user scripts.
	jsAssert(m_server->m_globalJson != nullptr);
	auto scriptNameMapper = m_server->m_globalJson->getScriptNameMapper(configGroups);
	int i = 0;
	for (auto iter = allScripts.cbegin(); iter != allScripts.cend(); ++iter, ++i) {
		auto fileName = scriptNameMapper(iter->fileName);

		ULONG objPlacementId = iter->placementId;
		bool isSystemScript = iter->isSystemScript;
		//		_LogDebug("LoadScriptTask[" << m_zoneId.toLong() << "]: Loading " << (i + 1) << " of " << allScripts.size() << " : " << fileName << ", type: " << (isSystemScript ? "SYSTEM" : "USER"));
		//
		// Now load the file to get the scriptCode.  If if this fails,
		// I probably don't want to try loading and empty file, because it
		// means we have a low-level problem (the scripts table not matching the
		// script cache on disk).
		//
		std::string sourceCode;
		bool blocking = m_loadSystemScriptsSynch && isSystemScript;
		if (!m_server->LoadScriptFile(fileName.c_str(), sourceCode)) {
			LogWarn(m_zoneId.ToStr() << " Failed to loaded script file: " << fileName << ", pid " << objPlacementId << ", type: " << (isSystemScript ? "SYSTEM" : "USER"));
			continue;
		}

		// If a final departed event is received, abort this task. Any scripts loaded from previous iterations should have received shutdown event.
		if (m_stopping) {
			break;
		}

		if (!m_server->LoadScript(fileName.c_str(), sourceCode, NULL, 0, objPlacementId, m_zoneId, m_clientNetId, isSystemScript, blocking ? m_kgpStartWaitTimeout : 0)) {
			LogWarn(m_zoneId.ToStr() << " Failed to loaded script: " << fileName << ", pid " << objPlacementId << ", type: " << (isSystemScript ? "SYSTEM" : "USER"));
			continue;
		}

		// If stopping flag changed during LoadScript call. Send shutdown event to this script only. Previously loaded scripts should received shutdown event already.
		// It's possible to for this script to receive duplicated SendScriptShutdownEvent calls. However the function call itself will filter out duplicates.
		if (m_stopping) {
			ServerScriptPtr pSS = m_server->m_zoneMap.getScriptByID(objPlacementId, m_zoneId, false);
			if (pSS)
				m_server->SendScriptShutdownEvent(pSS, m_clientNetId, "last player departed");
			break;
		}

		LogInfo(m_zoneId.ToStr() << " Loaded script ok: " << fileName << ", pid " << objPlacementId << ", type: " << (isSystemScript ? "SYSTEM" : "USER"));
	}

	std::lock_guard<fast_recursive_mutex> lockEH(m_server->m_eventHandlerCS);
	if (!m_stopping) {
		//		_LogDebug("LoadScriptTask[" << m_zoneId.toLong() << "]: Calling SendArrivedEventToZone");
		m_server->SendArrivedEventToZone(m_zoneId, m_clientNetId, m_playerName);
	} else {
		LogWarn(m_zoneId.ToStr() << " Aborted by final PlayerDeparted event");
	}

	m_server->RemovePendingLoadScriptTask(this);
}

void LoadScriptTask::LoadZoneSpinDownDelayMinutesFromDB(const ZoneIndex& zoneIndex) {
	ZoneIndex zi = zoneIndex.GetBaseZoneIndex();
	unsigned zoneInstanceId = zi.GetInstanceId();
	unsigned zoneType = zi.GetType();

	std::lock_guard<fast_recursive_mutex> lock(m_dbSync);

	BEGIN_TRY_SQL();

	// Read current zone's spin down delay time
	Query querySpinDownDelay = m_conn->query();
	querySpinDownDelay << "SELECT spin_down_delay_minutes FROM script_zones WHERE zone_instance_id = %0 AND zone_type = %1";
	querySpinDownDelay.parse();

	//	_LogDebug("LoadScriptTask: " << querySpinDownDelay.str(zoneInstanceId, zoneType));
	StoreQueryResult rsSpinDownDelay = querySpinDownDelay.store(zoneInstanceId, zoneType);

	if (rsSpinDownDelay.num_rows() > 0) {
		int spinDownDelayMins = rsSpinDownDelay.at(0).at(0);
		m_server->SetZoneSpinDownDelayMinutes(zoneIndex, spinDownDelayMins, false);
	}

	END_TRY_SQL_LOG_ONLY(m_conn);
}

}; // namespace KEP
