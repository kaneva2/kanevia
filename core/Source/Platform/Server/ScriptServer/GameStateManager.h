///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Event/Events/ScriptClientEvent.h"
#include "InstanceId.h"
#include "Glids.h"

#include "common\include\CoreStatic\AnimSettings.h"

#include "Log4CPlus/logger.h"
#include <vector>
#include <map>
#include <queue>

enum class EffectCode;

namespace KEP {

class GameStateModifier {
public:
	GameStateModifier(ScriptClientEvent::EventType eventType, int subType, ULONG objId, LONG zoneId, LONG instanceId);
	GameStateModifier(const GameStateModifier& rhs) {
		operator=(rhs);
	}
	GameStateModifier(GameStateModifier&& rhs) {
		operator=(std::move(rhs));
	}
	virtual ~GameStateModifier();

	GameStateModifier& operator=(const GameStateModifier& rhs);
	GameStateModifier& operator=(GameStateModifier&& rhs);

	// Order by (IsObjectGenerate(), timestamp)
	bool operator>(const GameStateModifier& rhs) const {
		return m_eventType != ScriptClientEvent::ObjectGenerate && rhs.m_eventType == ScriptClientEvent::ObjectGenerate ||
			   rhs.m_eventType == ScriptClientEvent::ObjectGenerate && m_timestamp > rhs.m_timestamp ||
			   m_eventType != ScriptClientEvent::ObjectGenerate && m_timestamp > rhs.m_timestamp;
	}

	// Queries
	void get(ScriptClientEvent*& e) const {
		e = m_suppressEvent ? NULL : m_event;
	}
	void extract(ScriptClientEvent*& e) {
		get(e); // get and erase - NOTE can't wipe timestamp at this moment because it's used for ordering in priority queue.
		m_event = NULL;
	}
	TimeMs timestamp() const {
		return m_timestamp;
	}

	// Unit test helper
	bool validate(ULONG objId, LONG zoneId, LONG instanceId) const {
		return m_objId == objId && m_zoneId == zoneId && m_instanceId == instanceId;
	}

	// Handle event
	virtual bool push(ScriptClientEvent* e, int subType);

	// Bake modifier into event with finalized property values. If target parameter is valid, update the target modifier instead.
	virtual void bake(GameStateModifier* /*target*/ = NULL) {} // Default: do nothing

	// Make a full copy of self
	virtual GameStateModifier* clone() const {
		return new GameStateModifier(*this);
	}

private:
	ScriptClientEvent* m_event;
	TimeMs m_timestamp;

	ScriptClientEvent::EventType m_eventType;
	int m_subType; //Additional discriminator for handling multi-purpose APIs like objectControl or objectSetParticle

protected:
	bool m_suppressEvent;
	ULONG m_objId;
	LONG m_zoneId, m_instanceId;
};

typedef std::map<std::pair<ULONG, int>, GameStateModifier*> GameStateModifierMapBase;

class GameStateModifierMap : public GameStateModifierMapBase {
public:
	struct Comparator {
		bool operator()(const GameStateModifier* a, const GameStateModifier* b) {
			return (*a > *b); // Reverse GameStateModifier order
		}
	};
	typedef std::priority_queue<GameStateModifier*, std::vector<GameStateModifier*>, Comparator> OrderedQueue;

	GameStateModifierMap() :
			GameStateModifierMapBase() {}
	GameStateModifierMap(const GameStateModifierMap& rhs) :
			GameStateModifierMapBase(rhs) {
		cloneItems();
	}
	GameStateModifierMap(GameStateModifierMap&& rhs) :
			GameStateModifierMapBase(rhs) {
		rhs.clear();
	}
	GameStateModifierMap& operator=(const GameStateModifierMap& rhs) {
		GameStateModifierMapBase::operator=(rhs);
		cloneItems();
		return *this;
	}
	GameStateModifierMap& operator=(GameStateModifierMap&& rhs) {
		GameStateModifierMapBase::operator=(std::move(rhs));
		rhs.clear();
		return *this;
	}

	~GameStateModifierMap() {
		for (auto itr = begin(); itr != end(); itr++) {
			GameStateModifier* mod = itr->second;
			delete mod;
		}
	}

	// Retrieve existing entry by type
	GameStateModifier* getModifier(ULONG type, int subType);

	// Set or replace existing entry. Dispose old modifier if replacing.
	void addModifier(ULONG type, int subType, GameStateModifier* mod);
	void addModifier(ScriptClientEvent* e, int subType, ULONG objId, LONG zoneId, LONG instanceId);

	// Dump map entries into a priority queue for sorted access
	void getSorted(OrderedQueue& queue) {
		for (auto itr = begin(); itr != end(); ++itr) {
			queue.push(itr->second);
		}
	}

	// Bake modifiers in the container
	void bake();

private:
	// Replace each of the item pointers with its clone
	void cloneItems() {
		for (auto itr = begin(); itr != end(); ++itr) {
			itr->second = itr->second->clone();
		}
	}
};

class GameStateManager {
public:
	GameStateManager(const ZoneIndex& zoneIndex, log4cplus::Logger logger, const std::set<ULONG>& permObjPids);

	// Pseudo object ID for per-zone environment settings
	const static ULONG ID_ENVIRONMENT = (ULONG)-1;

	typedef std::map<ULONG, GameStateModifierMap> GameStateMap;

	bool AddGameStateObject(ZoneIndex zi, ScriptClientEvent* e, ULONG objId);
	bool AddGameStateObjectModifier(ZoneIndex zi, ScriptClientEvent* e, ULONG objId, int subType = 0);
	bool AddGameStateObjectStartLocation(ZoneIndex zi, ULONG objId, double x, double y, double z, double rx, double ry, double rz, bool overwriteExisting, bool syncPosRot);

	bool RemoveGameStateObject(ZoneIndex zi, ULONG objId);
	bool RemoveGameStateObjectModifier(ZoneIndex zi, ULONG objId, ULONG type, int subType = 0);

	void AppendPermanentObject(ZoneIndex zi, ULONG oid);

	bool IsEmpty() const {
		return m_isEmpty;
	}
	GameStateMap GetZoneGameState(ZoneIndex zi);
	bool ObjectExists(ZoneIndex zi, ULONG objId);
	bool ObjectExists(ULONG objId);
	bool ObjectModifierExists(ZoneIndex zi, ULONG objId, ULONG type, int subType = 0);

	// timeOffset=INT_MAX for current position
	bool EvaluateObjectPosRot(ULONG objId, double& x, double& y, double& z, double& rx, double& ry, double& rz, TimeMs& timeLeft, TimeMs timeOffset = 0);
	bool EvaluateObjectSpeed(ULONG objId, double& spx, double& spy, double& spz);
	bool EvaluateObjectEffect(ULONG objId, EffectCode type, GLID& glid, bool& reverse, eAnimLoopCtl& loopCtl, TimeMs& offset);

private:
	ZoneIndex m_zoneIndex;
	TimeMs m_averageLatency;

	GameStateMap m_objectStates;
	std::set<ULONG> m_permanentObjects;
	bool m_isEmpty; // Use a separate flag so that the map doesn't need to be locked for IsEmpty()

	bool isPermanentObj(ZoneIndex zi, ULONG id);

	log4cplus::Logger m_logger;

	fast_recursive_mutex m_gameStateSync;
	fast_recursive_mutex m_permObjectsSync;
};

} // namespace KEP