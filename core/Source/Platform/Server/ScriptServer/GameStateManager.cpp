///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GameStateManager.h"
#include "KEPConstants.h"
#include "LogHelper.h"
#include "Core/Math/Vector.h"

#include "common\include\CoreStatic\AnimSettings.h"

namespace KEP {

const Vector3d DefDirVect(1, 0, 0);
const Vector3d DefUpVect(0, 1, 0);

///////////////////////////////////////////////////////////
// GameStateModifier

class PositionModifier;
class RotationModifier;
class StaticPosRotModifier;
class StartLocationModifier;
class NewObjectModifier;

// Generic
GameStateModifier::GameStateModifier(ScriptClientEvent::EventType type, int subType, ULONG objId, LONG zoneId, LONG instanceId) :
		m_event(NULL), m_timestamp(0), m_eventType(type), m_subType(subType), m_suppressEvent(false), m_objId(objId), m_zoneId(zoneId), m_instanceId(instanceId) {}

GameStateModifier& GameStateModifier::operator=(const GameStateModifier& rhs) {
	m_event = rhs.m_event ? new ScriptClientEvent(rhs.m_event, rhs.m_event->GetScope()) : NULL;
	m_timestamp = rhs.m_timestamp;
	m_eventType = rhs.m_eventType;
	m_subType = rhs.m_subType;
	m_suppressEvent = rhs.m_suppressEvent;
	m_objId = rhs.m_objId;
	m_zoneId = rhs.m_zoneId;
	m_instanceId = rhs.m_instanceId;
	return *this;
}

GameStateModifier& GameStateModifier::operator=(GameStateModifier&& rhs) {
	std::swap(m_event, rhs.m_event);
	m_timestamp = rhs.m_timestamp;
	m_eventType = rhs.m_eventType;
	m_subType = rhs.m_subType;
	m_suppressEvent = rhs.m_suppressEvent;
	m_objId = rhs.m_objId;
	m_zoneId = rhs.m_zoneId;
	m_instanceId = rhs.m_instanceId;
	return *this;
}

GameStateModifier::~GameStateModifier() {
	// Q: can we not clone m_event but call incQueCount on it instead?
	// A: possibly not. The event buffer will be altered with new data padded to the end before the event is transmitted to the client.
	if (m_event) {
		m_event->Delete();
	}
}

bool GameStateModifier::push(ScriptClientEvent* e, int subType) {
	// update timestamp
	m_timestamp = fTime::TimeMs();

	if (m_event)
		m_event->Delete();

	if (!e || e->GetEventType() == m_eventType && subType == m_subType) {
		m_event = e;
		return true;
	}

	// Incompatible event received
	jsAssert(false);
	e->Delete();
	e = NULL;
	return false;
}

// InterpolatedValueModifier
template <typename T>
class InterpolatedValueModifier : public GameStateModifier {
	typedef InterpolatedValueModifier<T> _MyT;

public:
	InterpolatedValueModifier<T>(ScriptClientEvent::EventType type, ULONG objId, LONG zoneId, LONG instanceId) :
			GameStateModifier(type, 0, objId, zoneId, instanceId), m_duration(0), m_accDur(0), m_constVelDur(0), m_accDistFrac(0), m_constVelDistFrac(1) {}

public:
	void update(const T& val, TimeMs duration) {
		// Update current value
		TimeMs timeLeft;
		m_from = interpolated(timeLeft);

		// Update next value
		m_to = val;
		m_duration = duration;

		if (m_duration == 0) {
			m_maxSpeed = m_maxSpeed * 0;
			m_speed = m_maxSpeed;
		} else {
			if (m_constVelDur <= 0) {
				m_maxSpeed = (m_to - m_from) * (1000 / m_accDur);
			} else {
				m_maxSpeed = (m_to - m_from) * (m_constVelDistFrac * 1000 / m_constVelDur);
			}
		}
	}

	T interpolated(TimeMs& timeLeft, TimeMs timeOffset = 0) {
		TimeMs elapsed = (timeOffset > INT_MAX) ? INT_MAX : fTime::ElapsedMs(timestamp()) + timeOffset;
		if (elapsed >= m_duration) { // Arrived
			timeLeft = 0;
			m_maxSpeed = m_maxSpeed * 0;
			m_speed = m_maxSpeed;
			return m_to;
		} else { // Half-way
			jsAssert(m_duration != 0);
			timeLeft = m_duration - elapsed;
			double factor;
			if (m_accDur > elapsed) {
				factor = (m_accDistFrac * elapsed * elapsed) / (m_accDur * m_accDur);
				m_speed = m_maxSpeed * (factor / m_accDistFrac);
			} else if ((m_accDur + m_constVelDur) > elapsed) {
				factor = m_accDistFrac + (m_constVelDistFrac * (elapsed - m_accDur) / m_constVelDur);
				m_speed = m_maxSpeed;
			} else {
				TimeMs effectiveTimeElapsed = m_duration - elapsed;
				TimeMs timeNormalizingFactor = m_duration - m_constVelDur - m_accDur;
				double remDistFactor = 1 - m_accDistFrac - m_constVelDistFrac;
				factor = 1 - ((remDistFactor * effectiveTimeElapsed * effectiveTimeElapsed) / (timeNormalizingFactor * timeNormalizingFactor));
				m_speed = m_maxSpeed * ((1 - factor) / remDistFactor);
			}
			return m_from * factor + m_to * (1 - factor);
		}
	}

	// Delta per second
	T speed() {
		TimeMs timeLeft;
		T curr = interpolated(timeLeft);
		return m_speed;
	}

	void get(T& curr, TimeMs& timeLeft) const {
		curr = interpolated(timeLeft);
	}

	GameStateModifier* clone() const {
		return new _MyT(*this);
	}

protected:
	TimeMs m_duration;
	T m_from, m_to, m_speed, m_maxSpeed;
	TimeMs m_accDur, m_constVelDur;
	double m_accDistFrac, m_constVelDistFrac;
};

// Position
class PositionModifier : public InterpolatedValueModifier<Vector3d> {
public:
	PositionModifier(ULONG objId, LONG zoneId, LONG instanceId) :
			InterpolatedValueModifier<Vector3d>(ScriptClientEvent::ObjectMove, objId, zoneId, instanceId) {}

	bool push(ScriptClientEvent* e, int subType) {
		if (!InterpolatedValueModifier<Vector3d>::push(e, subType)) {
			return false;
		}

		if (e) {
			Vector3d pos(0, 0, 0);
			double duration;
			ScriptClientEvent::DecodeObjectMove(e, m_objId, m_zoneId, m_instanceId, pos[0], pos[1], pos[2], duration, m_accDur, m_constVelDur, m_accDistFrac, m_constVelDistFrac);
			update(pos, (TimeMs)duration);
		} else {
			update(Vector3d(0, 0, 0), 0);
		}
		return true;
	}

	void bake(GameStateModifier* target = NULL);
	GameStateModifier* clone() const {
		return new PositionModifier(*this);
	}
};

// Rotation
struct Orientation {
	Vector3d dir, up;
	Orientation(const Vector3d& d = DefDirVect, const Vector3d& u = DefUpVect) :
			dir(d), up(u) {}
	Orientation operator*(double scalar) const {
		Orientation res(*this);
		res.dir *= scalar;
		res.up *= scalar;
		return res;
	}
	Orientation operator+(const Orientation& rhs) const {
		Orientation res(*this);
		res.dir += rhs.dir;
		res.up += rhs.up;
		return res;
	}
	Orientation operator-(const Orientation& rhs) const {
		Orientation res(*this);
		res.dir -= rhs.dir;
		res.up -= rhs.up;
		return res;
	}
	void extract(Vector3d& d, Vector3d& u) const {
		d = dir;
		u = up;
	}
	Vector3d slide() const {
		return up.Cross(dir); // cross product
	}
};

class RotationModifier : public InterpolatedValueModifier<Orientation> {
public:
	RotationModifier(ULONG objId, LONG zoneId, LONG instanceId) :
			InterpolatedValueModifier<Orientation>(ScriptClientEvent::ObjectRotate, objId, zoneId, instanceId) {}

	bool push(ScriptClientEvent* e, int subType) {
		if (!InterpolatedValueModifier<Orientation>::push(e, subType)) {
			return false;
		}

		if (e) {
			Vector3d dir(0, 0, 0), slide(0, 0, 0);
			double duration;
			ScriptClientEvent::DecodeObjectRotate(e, m_objId, m_zoneId, m_instanceId, dir[0], dir[1], dir[2], slide[0], slide[1], slide[2], duration);
			update(Orientation(dir, DefUpVect), (TimeMs)duration);
		} else {
			update(Orientation(), 0);
		}
		return true;
	}

	void bake(GameStateModifier* target = NULL);
	GameStateModifier* clone() const {
		return new RotationModifier(*this);
	}
};

// Starting Position and Rotation for Permanent Objects (those in dynamic_objects table)
class StaticPosRotModifier : public GameStateModifier {
public:
	StaticPosRotModifier(ScriptClientEvent::EventType type, ULONG objId, LONG zoneId, LONG instanceId) :
			GameStateModifier(type, 0, objId, zoneId, instanceId), m_pos(0, 0, 0) {}

	void get(Vector3d& pos, Vector3d& dir, Vector3d& up) {
		pos = m_pos;
		dir = m_rot.dir;
		up = m_rot.up;
	}
	void setPos(const Vector3d& pos) {
		m_pos = pos;
	}
	void setRot(const Orientation& rot) {
		m_rot = rot;
	}
	GameStateModifier* clone() const {
		return new StaticPosRotModifier(*this);
	}

protected:
	Vector3d m_pos;
	Orientation m_rot;
};

// Object Start Location
class StartLocationModifier : public StaticPosRotModifier {
public:
	StartLocationModifier(ULONG objId, LONG zoneId, LONG instanceId) :
			StaticPosRotModifier(ScriptClientEvent::ObjectGetStartLocation, objId, zoneId, instanceId) {}

	GameStateModifier* clone() const {
		return new StartLocationModifier(*this);
	}
};

// Object Generate Information
class NewObjectModifier : public StaticPosRotModifier {
public:
	NewObjectModifier(ULONG objId, LONG zoneId, LONG instanceId) :
			StaticPosRotModifier(ScriptClientEvent::ObjectGenerate, objId, zoneId, instanceId), m_globalId(0), m_placementType(0), m_gameItemId(0) {}

	bool push(ScriptClientEvent* e, int subType) {
		if (!StaticPosRotModifier::push(e, subType)) {
			return false;
		}

		if (e) {
			ScriptClientEvent::DecodeObjectGenerate(e, m_objId, m_globalId, m_zoneId, m_instanceId, m_pos[0], m_pos[1], m_pos[2], m_rot.dir[0], m_rot.dir[1], m_rot.dir[2], m_placementType, m_gameItemId);
		} else {
			setPos(Vector3d(0, 0, 0));
			setRot(Orientation());
		}
		return true;
	}

	void bake(GameStateModifier* target = NULL);
	GameStateModifier* clone() const {
		return new NewObjectModifier(*this);
	}

private:
	ULONG m_globalId;
	int m_placementType, m_gameItemId;
};

// Effects: animation, sound, particle, etc.
class EffectModifier : public GameStateModifier {
public:
	EffectModifier(ScriptClientEvent::EventType type, int subType, ULONG objId, LONG zoneId, LONG instanceId) :
			GameStateModifier(type, subType, objId, zoneId, instanceId) {}

	void get(GLID& glid, bool& reverse, TimeMs& duration, bool& looping, eAnimLoopCtl& loopCtl, TimeMs& offset) const {
		glid = m_glid;
		reverse = m_reverse;
		duration = m_duration;
		looping = m_looping;
		loopCtl = m_loopCtl;

		int elapsed = fTime::ElapsedMs(timestamp());
		int dur = duration;
		if (!looping) {
			offset = (elapsed >= dur) ? 0 : dur - elapsed;
		} else {
			offset = elapsed % dur;
		}
	}

	GameStateModifier* clone() const {
		return new EffectModifier(*this);
	}

protected:
	GLID m_glid = 0; // Effect GLID
	bool m_reverse = false; // reverse play (e.g. for animation)
	eAnimLoopCtl m_loopCtl = eAnimLoopCtl::Default; // drf - added
	TimeMs m_duration = 0; // Effect duration
	bool m_looping = false;
};

class AnimEffectModifier : public EffectModifier {
public:
	AnimEffectModifier(ULONG objId, LONG zoneId, LONG instanceId) :
			EffectModifier(ScriptClientEvent::ObjectControl, ANIM_START, objId, zoneId, instanceId) {}

	bool push(ScriptClientEvent* pSCE, int subType) {
		if (!EffectModifier::push(pSCE, subType))
			return false;

		if (pSCE) {
			int _ctlType, argCount;
			int loopCtlInt = (int)eAnimLoopCtl::Default;
			int glidSpecial = 0;

			*pSCE->InBuffer() >> m_objId >> m_zoneId >> m_instanceId >> _ctlType >> argCount;
			assert(argCount == 3);
			*pSCE->InBuffer() >> glidSpecial >> loopCtlInt >> m_duration;

			m_glid = abs(glidSpecial);
			m_reverse = glidSpecial < 0;
			m_loopCtl = (eAnimLoopCtl)loopCtlInt;
			m_looping = true;
		} else {
			m_glid = GLID_INVALID;
			m_reverse = false;
			m_loopCtl = eAnimLoopCtl::Default;
			m_looping = false;
			m_duration = (TimeMs)0;
		}
		return true;
	}
};

void PositionModifier::bake(GameStateModifier* target /*= NULL*/) {
	ScriptClientEvent* e = NULL;
	GameStateModifier::get(e);

	TimeMs timeLeft = 0;
	TimeMs accDur = 0;
	TimeMs constVelDur = 0;
	double accDistFrac = 0;
	double constVelDistFrac = 0;
	Vector3d curr = interpolated(timeLeft);
	double distFracCov = ((curr - m_from).Length() / (m_to - m_from).Length());
	double distFracRem = 1 - distFracCov;

	// Update event based on movement transition state
	if (timeLeft != 0 && e) {
		try {
			e->ResetForOutput();
			TimeMs timeElapsed = m_duration - timeLeft;
			if (m_accDur > timeElapsed) {
				accDur = m_accDur - timeElapsed;
				accDistFrac = (m_accDistFrac - distFracCov) / distFracRem;
				constVelDistFrac = m_constVelDistFrac / distFracRem;
			} else if ((m_accDur + m_constVelDur) > timeElapsed) {
				accDur = 0;
				accDistFrac = 0;
				constVelDur = m_accDur + m_constVelDur - timeElapsed;
				constVelDistFrac = (m_constVelDistFrac + m_accDistFrac - distFracCov) / distFracRem;
			} else if (m_duration > timeElapsed) {
				accDur = 0;
				accDistFrac = 0;
				constVelDur = 0;
				constVelDistFrac = 0;
			} else {
				accDur = 0;
				constVelDur = timeLeft;
				accDistFrac = 0;
				constVelDistFrac = 1;
			}
			ScriptClientEvent::EncodeObjectMove(e, m_objId, m_zoneId, m_instanceId, m_to[0], m_to[1], m_to[2], timeLeft, accDur, constVelDur, accDistFrac, constVelDistFrac);
		} catch (...) {
		}
	}

	// Also update ObjectGenerate if linked
	if (target) {
		NewObjectModifier* modGen = dynamic_cast<NewObjectModifier*>(target);
		jsAssert(modGen != NULL);
		if (modGen) {
			modGen->setPos(curr);
			if (timeLeft == 0) {
				m_suppressEvent = true; // Suppress own event - leave it to ObjectGenerate
			}
		}
	}
}

void RotationModifier::bake(GameStateModifier* target /*= NULL*/) {
	ScriptClientEvent* e = NULL;
	GameStateModifier::get(e);

	TimeMs timeLeft;
	Orientation curr = interpolated(timeLeft);

	// Update event based on movement transition state
	if (timeLeft != 0 && e) {
		try {
			Vector3d slide = m_to.slide();

			e->ResetForOutput();
			ScriptClientEvent::EncodeObjectRotate(e, m_objId, m_zoneId, m_instanceId, m_to.dir[0], m_to.dir[1], m_to.dir[2], slide[0], slide[1], slide[2], timeLeft);
		} catch (...) {
		}
	}

	// Also update ObjectGenerate if linked
	if (target) {
		NewObjectModifier* modGen = dynamic_cast<NewObjectModifier*>(target);
		jsAssert(modGen != NULL);
		if (modGen) {
			modGen->setRot(curr);
			if (timeLeft == 0) {
				m_suppressEvent = true; // Suppress own event - leave it to ObjectGenerate
			}
		}
	}
}

void NewObjectModifier::bake(GameStateModifier* target /*= NULL*/) {
	jsAssert(target == NULL);
	target; // Does not support retargeting

	ScriptClientEvent* e = NULL;
	GameStateModifier::get(e);

	if (e) {
		try {
			e->ResetForOutput();
			ScriptClientEvent::EncodeObjectGenerate(e,
				m_objId, m_globalId, m_zoneId, m_instanceId,
				m_pos[0], m_pos[1], m_pos[2], m_rot.dir[0], m_rot.dir[1], m_rot.dir[2],
				m_placementType, m_gameItemId);
		} catch (...) {
		}
	}
}

///////////////////////////////////////////////////////////
// GameStateManager

GameStateManager::GameStateManager(const ZoneIndex& zoneIndex, Logger logger, const std::set<ULONG>& permObjPids) :
		m_logger(logger), m_zoneIndex(zoneIndex), m_permanentObjects(permObjPids), m_isEmpty(true), m_averageLatency(100) { // Hard-coded 100ms for now until we have real-time latency updates
}

/*!
 * \brief
 * Add an object to game state moniotoring.  This is used for object creation.
 *
 * \param zi
 * The ZoneIndex the object is generated in.
 *
 * \param e
 * Event that creates the object
 *
 * \param objId
 * Id of object being created.
 *
 * \returns
 * Success or failure
 *
 * This function is for adding the create event for a new gamestate object.
 *
 */
bool GameStateManager::AddGameStateObject(ZoneIndex zi, ScriptClientEvent* e, ULONG objId) {
	jsAssert(zi == m_zoneIndex);
	zi;
	LONG zoneId = zi.getClearedInstanceId();
	LONG instanceId = zi.GetInstanceId();

	if (e->GetEventType() != ScriptClientEvent::ObjectGenerate) {
		jsAssert(false);
		LogWarn("Expects a ScriptClientEvent::ObjectGenerate event only.");
		e->Delete();
		return false;
	}

	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);

	try {
		auto itrList = m_objectStates.find(objId);
		if (itrList != m_objectStates.end()) { //find object
			//Game state info of the same object id should not exist.
			jsAssert(false);
			LogWarn("Object " << objId << " already has gamestate events.  Ignoring generate.");
			e->Delete();
			return false;
		} else {
			//Can't find object's map, create
			GameStateModifierMap& os = m_objectStates[objId];
			os.addModifier(e, 0, objId, zoneId, instanceId); //add state to vector
			m_isEmpty = false;
		}
	} catch (...) {
	}

	return true;
}

/*!
 * \brief
 * Add an action that modifies an object that already exists.
 *
 * \param zi
 * The ZoneIndex the object is generated in.
 *
 * \param e
 * Event that modifying the object
 *
 * \param objId
 * Id of object being modified.
 *
 * \param subType
 * Additional discriminators for multi-purposes events like objectControl or objectSetParticle
 *
 * \returns
 * Success or failure
 *
 * This function is for a modifying event for a gamestate object.
 *
 */
bool GameStateManager::AddGameStateObjectModifier(ZoneIndex zi, ScriptClientEvent* e, ULONG objId, int subType) {
	jsAssert(zi == m_zoneIndex);
	zi;
	LONG zoneId = zi.getClearedInstanceId();
	LONG instanceId = zi.GetInstanceId();

	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);

	try {
		auto itrList = m_objectStates.find(objId);
		if (itrList == m_objectStates.end()) {
			//check permanent dynamic objects
			if (isPermanentObj(zi, objId)) {
				//create entry for permanent object
				m_objectStates.insert(make_pair(objId, GameStateModifierMap())); //add to zone
			} else {
				//object doesn't exist
				LogError("Unknown placement ID: " << objId);
				if (e)
					e->Delete();
				return false;
			}
		}

		itrList = m_objectStates.find(objId);
		jsAssert(itrList != m_objectStates.end());

		if (itrList != m_objectStates.end()) { //find object
			GameStateModifierMap& os = itrList->second;
			GameStateModifier* mod = os.getModifier(e->GetEventType(), subType);
			if (mod) {
				mod->push(e, subType);
			} else {
				os.addModifier(e, subType, objId, zoneId, instanceId);
			}
		}
	} catch (...) {
	}

	return true;
}

bool GameStateManager::AddGameStateObjectStartLocation(ZoneIndex zi, ULONG objId, double x, double y, double z, double rx, double ry, double rz, bool overwriteExisting, bool syncPosRot) {
	jsAssert(zi == m_zoneIndex);
	zi;
	LONG zoneId = zi.getClearedInstanceId();
	LONG instanceId = zi.GetInstanceId();

	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);

	try {
		auto itrList = m_objectStates.find(objId);
		if (itrList == m_objectStates.end()) {
			//check permanent dynamic objects
			if (isPermanentObj(zi, objId)) {
				//create entry for permanent object
				m_objectStates.insert(make_pair(objId, GameStateModifierMap())); //add to zone
			} else {
				//object doesn't exist
				LogError("Unknown placement ID: " << objId);
				return false;
			}
		}

		itrList = m_objectStates.find(objId);
		jsAssert(itrList != m_objectStates.end());

		if (itrList != m_objectStates.end()) { //find object
			GameStateModifierMap& os = itrList->second;
			GameStateModifier* mod = os.getModifier(ScriptClientEvent::ObjectGetStartLocation, 0);
			if (mod == NULL) {
				mod = new StartLocationModifier(objId, zoneId, instanceId);
				os.addModifier(ScriptClientEvent::ObjectGetStartLocation, 0, mod);
			} else if (!overwriteExisting) {
				// Already exists and no overwriting
				return true;
			}

			StartLocationModifier* modSPR = dynamic_cast<StartLocationModifier*>(mod);
			if (modSPR) {
				modSPR->setPos(Vector3d(x, y, z));
				modSPR->setRot(Orientation(Vector3d(rx, ry, rz)));
			} else {
				jsAssert(false);
				LogError("StartLocation modifier is not of StartPosRotModifer type, PID=" << objId);
			}

			// Synchronize with position and rotation modifiers (discard intermediate updates)
			if (syncPosRot) {
				PositionModifier* modPos = dynamic_cast<PositionModifier*>(os.getModifier(ScriptClientEvent::ObjectMove, 0));
				if (modPos) {
					modPos->update(Vector3d(x, y, z), 0);
				}
				RotationModifier* modRot = dynamic_cast<RotationModifier*>(os.getModifier(ScriptClientEvent::ObjectRotate, 0));
				if (modRot) {
					modRot->update(Orientation(Vector3d(rx, ry, rz)), 0);
				}
			}
		}
	} catch (...) {
	}

	return true;
}

/*!
 * \brief
 * Add an object placed by player to gamestate tracking.
 *
 * \param zi
 * The ZoneIndex the object resides in.
 *
 * \param oid
 * Object placement id.
 *
 * \returns
 * None
 *
 * This function is for adding a dynamic object the player places to gamestate tracking.
 *
 */
void GameStateManager::AppendPermanentObject(ZoneIndex zi, ULONG oid) {
	jsAssert(zi == m_zoneIndex);
	zi;

	std::lock_guard<fast_recursive_mutex> lock(m_permObjectsSync);
	m_permanentObjects.insert(oid);
}

/*!
 * \brief
 * Check to see if a placement id is an object from the database.
 *
 * \param zi
 * The ZoneIndex the object resides in.
 *
 * \param objId
 * Vector of object placement ids.
 *
 * \returns
 * Yes (database)/No (generated or non existant)
 *
 * This function is for checking if a placement id is an object from the database or generated.
 *
 */
bool GameStateManager::isPermanentObj(ZoneIndex zi, ULONG objId) {
	jsAssert(zi == m_zoneIndex);
	zi;

	std::lock_guard<fast_recursive_mutex> lock(m_permObjectsSync);
	return m_permanentObjects.find(objId) != m_permanentObjects.end();
}

/*!
 * \brief
 * Remove an object from gamestate tracking based on its zone, id, and type.
 *
 * \param zi
 * The ZoneIndex the object is generated in.
 *
 * \param objId
 * Id of object
 *
 * \param type
 * Event filter
 *
 * \param subType
 * Additional discriminators for multi-purposes events like objectControl or objectSetParticle
 *
 * \returns
 * Success or failure
 *
 * This function is for removing an object from gamestate tracking based on its zone, id, and type.
 *
 */
bool GameStateManager::RemoveGameStateObjectModifier(ZoneIndex zi, ULONG objId, ULONG type, int subType) {
	jsAssert(zi == m_zoneIndex);
	zi;

	bool ret = false;

	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);

	try {
		auto itrList = m_objectStates.find(objId);
		if (itrList != m_objectStates.end()) { //find object
			GameStateModifierMap& os = itrList->second;

			auto itr = os.find(std::make_pair(type, subType));
			if (itr != os.end()) {
				GameStateModifier* mod = itr->second;
				delete mod;
				os.erase(itr);
				ret = true;
			}
		}
	} catch (...) {
	}

	return ret;
}

/*!
 * \brief
 * Remove an object from gamestate tracking based on its zone and id.
 *
 * \param zi
 * The ZoneIndex the object is generated in.
 *
 * \param objId
 * Id of object
 *
 * \returns
 * Success or failure
 *
 * This function is for removing an object from gamestate tracking based on its zone and id.
 *
 */
bool GameStateManager::RemoveGameStateObject(ZoneIndex zi, ULONG objId) {
	jsAssert(zi == m_zoneIndex);
	zi;

	bool ret = false;

	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);

	try {
		m_objectStates.erase(objId);
		m_isEmpty = m_objectStates.empty();
	} catch (...) {
	}

	return ret;
}

/*!
 * \brief
 * Return a copy of the zone's gamestate.  A copy is returned because it is
 * expected that this will be passed to the event which will delete events.
 *
 * \param zi
 * The ZoneIndex of requested gamestate.
 *
 * \returns
 * GameStateManager::ObjectIdEventMap
 *
 * Return a copy of the zone's gamestate.  A copy is returned because it is
 * expected that this will be passed to the event which will delete events.
 */
GameStateManager::GameStateMap GameStateManager::GetZoneGameState(ZoneIndex zi) {
	jsAssert(zi == m_zoneIndex);
	zi;

	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);
	return m_objectStates;
}

/*!
 * \brief
 * Check of an object's game state exists.
 *
 * \param zi
 * The ZoneIndex of requested game state.
 *
 * \param objId
 * Object id of the zone object.
 *
 * \returns
 * true / false
 *
 * Check of an object's game state exists.
 */
bool GameStateManager::ObjectExists(ZoneIndex zi, ULONG objId) {
	jsAssert(zi == m_zoneIndex);
	zi;

	return ObjectExists(objId);
}

bool GameStateManager::ObjectExists(ULONG objId) {
	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);
	return m_objectStates.find(objId) != m_objectStates.end();
}

/*!
 * \brief
 * Check of an object's specific game state modifier exists.
 *
 * \param zi
 * The ZoneIndex of requested game state.
 *
 * \param objId
 * Object id of the zone object.
 *
 * \param type
 * Event filter
 *
 * \param subType
 * Additional discriminators for multi-purposes events like objectControl or objectSetParticle
 *
 * \returns
 * true / false
 *
 * Check of an object's specific game state modifier exists.
 */
bool GameStateManager::ObjectModifierExists(ZoneIndex zi, ULONG objId, ULONG type, int subType /*= 0*/) {
	jsAssert(zi == m_zoneIndex);
	zi;

	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);

	auto itr = m_objectStates.find(objId);
	if (itr != m_objectStates.end()) {
		GameStateModifierMap& os = itr->second;
		return os.getModifier(type, subType) != NULL;
	}

	return false;
}

bool GameStateManager::EvaluateObjectPosRot(ULONG objId, double& x, double& y, double& z, double& rx, double& ry, double& rz, TimeMs& timeLeft, TimeMs timeOffset /*=0*/) {
	Vector3d pos(0, 0, 0), dir(DefDirVect), up(DefUpVect);
	TimeMs timeLeftPos = 0, timeLeftRot = 0;

	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);

	auto itr = m_objectStates.find(objId);
	if (itr != m_objectStates.end()) {
		GameStateModifierMap& os = itr->second;

		// Generated objects are guaranteed to have both position and rotation modifiers
		auto modPos = dynamic_cast<PositionModifier*>(os.getModifier(ScriptClientEvent::ObjectMove, 0));
		auto modRot = dynamic_cast<RotationModifier*>(os.getModifier(ScriptClientEvent::ObjectRotate, 0));

		// Permanent objects, if manipulated by script, usually have start position and rotation recorded (not guaranteed)
		auto modStart = dynamic_cast<StartLocationModifier*>(os.getModifier(ScriptClientEvent::ObjectGetStartLocation, 0));

		if (modStart || modPos) {
			if (modStart) {
				// Start position and rotation
				modStart->get(pos, dir, up);

				// Override position
				if (modPos) {
					pos = modPos->interpolated(timeLeftPos, timeOffset);
				}

				// Override rotation
				if (modRot) {
					modRot->interpolated(timeLeftRot, timeOffset).extract(dir, up);
				}
			} else {
				// Position
				pos = modPos->interpolated(timeLeftPos, timeOffset);

				// Rotation
				if (modRot) {
					modRot->interpolated(timeLeftRot, timeOffset).extract(dir, up);
				}
			}

			x = pos[0];
			y = pos[1];
			z = pos[2];
			rx = dir[0];
			ry = dir[1];
			rz = dir[2];
			timeLeft = (std::max)(timeLeftPos, timeLeftRot);

			return true;
		}
	}

	return false;
}

bool GameStateManager::EvaluateObjectSpeed(ULONG objId, double& spx, double& spy, double& spz) {
	// Init as zero
	spx = spy = spz = 0.0;

	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);

	auto itr = m_objectStates.find(objId);
	if (itr != m_objectStates.end()) {
		GameStateModifierMap& os = itr->second;

		// Generated objects are guaranteed to have both position and rotation modifiers
		auto modPos = dynamic_cast<PositionModifier*>(os.getModifier(ScriptClientEvent::ObjectMove, 0));

		if (modPos) {
			Vector3d speed = modPos->speed();
			spx = speed[0];
			spy = speed[1];
			spz = speed[2];
			return true;
		}
	}

	return false;
}

bool GameStateManager::EvaluateObjectEffect(ULONG objId, EffectCode type, GLID& glid, bool& reverse, eAnimLoopCtl& loopCtl, TimeMs& offset) {
	EffectModifier* mod = NULL;

	std::lock_guard<fast_recursive_mutex> lock(m_gameStateSync);

	auto itr = m_objectStates.find(objId);
	if (itr != m_objectStates.end()) {
		GameStateModifierMap& os = itr->second;
		switch (type) {
			case EffectCode::EFX_ANIMATION:
				mod = dynamic_cast<EffectModifier*>(os.getModifier(ScriptClientEvent::ObjectControl, ANIM_START));
				break;
				// TODO: sound and particle
		}

		if (mod) {
			TimeMs _duration;
			bool _looping;
			mod->get(glid, reverse, _duration, _looping, loopCtl, offset);
			return true;
		}
	}

	return false;
}

///////////////////////////////////////////////////////////
// GameStateModifierMap

GameStateModifier* GameStateModifierMap::getModifier(ULONG type, int subType) {
	auto key = std::make_pair(type, subType);
	auto itr = find(key);
	if (itr != end()) {
		return itr->second;
	}
	return NULL;
}

void GameStateModifierMap::addModifier(ULONG type, int subType, GameStateModifier* mod) {
	jsVerifyReturnVoid(mod != NULL);

	auto key = std::make_pair(type, subType);
	auto itr = find(key);
	if (itr != end()) {
		GameStateModifier* curr = itr->second;
		delete curr;
		erase(itr);
	}

	insert(std::make_pair(key, mod));
}

void GameStateModifierMap::addModifier(ScriptClientEvent* e, int subType, ULONG objId, LONG zoneId, LONG instanceId) {
	jsVerifyReturnVoid(e != NULL);

	GameStateModifier* mod = NULL;
	switch (e->GetEventType()) {
		case ScriptClientEvent::ObjectGenerate:
			mod = new NewObjectModifier(objId, zoneId, instanceId);
			break;
		case ScriptClientEvent::ObjectMove:
			mod = new PositionModifier(objId, zoneId, instanceId);
			break;
		case ScriptClientEvent::ObjectRotate:
			mod = new RotationModifier(objId, zoneId, instanceId);
			break;
		case ScriptClientEvent::ObjectControl:
			if (subType == ANIM_START) {
				mod = new AnimEffectModifier(objId, zoneId, instanceId);
			}
			break;
	}

	// Catch-all: use generic modifier
	if (mod == NULL) {
		mod = new GameStateModifier(e->GetEventType(), subType, objId, zoneId, instanceId);
	}

	// Rewind event input buffer
	e->RewindInput();

	// Insert modifier into container
	addModifier(e->GetEventType(), subType, mod);

	// Push event to the modifier
	mod->push(e, subType);

	// Additional modifiers for ObjectGenerate
	if (e->GetEventType() == ScriptClientEvent::ObjectGenerate) {
		NewObjectModifier* modGen = dynamic_cast<NewObjectModifier*>(mod);
		jsAssert(modGen != NULL);
		if (modGen) {
			// Also create position and rotation modifier
			Vector3d pos(0, 0, 0), dir(0, 0, 0), up(0, 0, 0);
			modGen->get(pos, dir, up);

			PositionModifier* modPos = new PositionModifier(objId, zoneId, instanceId);
			modPos->update(pos, 0);
			addModifier(ScriptClientEvent::ObjectMove, 0, modPos);

			RotationModifier* modRot = new RotationModifier(objId, zoneId, instanceId);
			modRot->update(Orientation(dir, up), 0);
			addModifier(ScriptClientEvent::ObjectRotate, 0, modRot);
		}
	}
}

void GameStateModifierMap::bake() {
	auto modGen = getModifier(ScriptClientEvent::ObjectGenerate, 0);
	auto modMov = getModifier(ScriptClientEvent::ObjectMove, 0);
	auto modRot = getModifier(ScriptClientEvent::ObjectRotate, 0);

	if (modMov) {
		modMov->bake(modGen);
	}

	if (modRot) {
		modRot->bake(modGen);
	}

	// Bake ObjectGenerate after ObjectMove and ObjectRotate
	if (modGen) {
		modGen->bake();
	}
}

} // namespace KEP