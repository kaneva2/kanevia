///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/include/IKEPConfig.h"
#include "Common/include/CoreItems.h"
#include "Event/Base/IEventBlade.h"
#include "Event/Base/IDispatcher.h"
#include "Event/Events/ScriptServerEvent.h"
#include "InstanceId.h"
#include "ScriptServerTaskSystem.h"
#include "ScriptMap.h"
#include "ScriptPlayerInfo.h"
#include "GameStateManager.h"
#include "ServerScript.h"
#include "LuaSharedData.h"
#include "LuaVMConfig.h"
#include "ScriptAPITypes.h"
#include "ScriptEventArgs.h"
#include "Core/Math/Quaternion.h"

#include <boost/optional.hpp>
#include <memory>

namespace KEP {

class KGP_Connection;
class MessageQueueAgent;
class ScriptAPIRegistry;
class ScriptClientEvent;

// Specialize TScriptAPIRetType
struct ScriptAPIRetType {
	using SYNC = TScriptAPIRetType<ScriptAPIType::SYNC, ServerScript, IEvent, ScriptServerTaskObject>;
	using BLOC = TScriptAPIRetType<ScriptAPIType::BLOC, ServerScript, IEvent, ScriptServerTaskObject>;
	using ASYN = TScriptAPIRetType<ScriptAPIType::ASYN, ServerScript, IEvent, ScriptServerTaskObject>;
};

// Specialize TScriptAPIScript (ServerScript wrapper)
template <>
struct TScriptAPIScript<ServerScript, IEvent, ScriptServerTaskObject> {
	TScriptAPIScript<ServerScript, IEvent, ScriptServerTaskObject>(const ServerScriptPtr& ss) :
			m_ss(ss) {}
	int LuaYield(int unblockEventType, const std::string& reason, IEvent* e, ScriptServerTaskObject* task) {
		return m_ss->LuaYield(static_cast<ScriptServerEvent::EventType>(unblockEventType), reason, e, task);
	}

private:
	const ServerScriptPtr& m_ss;
};

class Scheduler;
class RunVMTask;
class ServerScript;
class TransmitGameStateTask;
class LoadScriptTask;
class JsonConfig;
class DictUserData;
struct AsyncUserArg;
struct ScriptRuntimeData;
struct ObjectLabelInfo;
struct GameItem;
typedef std::pair<std::string, std::string> ScriptDataPair;
typedef std::map<std::string, std::string> ScriptDataMap;
typedef std::map<int, std::map<std::string, std::string>> ScriptPlayListDataMap;
typedef struct {
} * HDICT; // Opaque handle type for ScriptServerEngine::Dictionary

//////////////////////////////////////////////////////////////
// ScriptServerEngine
class ScriptServerEngine : public IEventBlade {
public:
	ScriptServerEngine();
	~ScriptServerEngine();

	// IBlade overrides
	virtual ULONG Version() const override { return BladeVersion(); }
	virtual std::string PathBase() const override { return m_pathBase; }
	virtual void SetPathBase(const std::string& baseDir, bool readonly) override {
		m_pathBase = baseDir;
		m_baseDirReadOnly = readonly;
	}
	virtual bool IsBaseDirReadOnly() const override { return m_baseDirReadOnly; }
	virtual const char* Name() const override { return "ScriptServerEventBlade"; }
	virtual void FreeAll() override;

	// IEventBlade overrides
	virtual void SetDBConfig(const DBConfig* dbConfig) override { m_dbConfig = dbConfig; }
	virtual bool RegisterEvents(IDispatcher* dispatcher, IEncodingFactory* factory) override;
	virtual bool RegisterEventHandlers(IDispatcher* dispatcher, IEncodingFactory* factory) override;
	virtual bool ReRegisterEventHandlers(IDispatcher*, IEncodingFactory*) override { return true; }

	//
	// Technically this isn't a singleton, but if there is more than one
	// of them I'll need to change the Instance method to take a handle
	// and return the correct instance.  I don't think this will happen
	// though (more than one ScriptServerEngine in the same address space).
	//
	static ScriptServerEngine* Instance() { /*ASSERT(m_instance);*/
		return m_instance;
	}
	static ULONG BladeVersion() { return PackVersion(1, 0, 0, 0); }
	virtual Monitored::DataPtr monitor() const { return Monitored::DataPtr(new Monitored::Data()); }

	bool Initialize(const char* baseDir, const char* configFile, const DBConfig& dbConfig);
	bool LoadConfigValues(const IKEPConfig* cfg);
	void SaveConfigValues(IKEPConfig* cfg);

	// Script Server events.
	static EVENT_PROC_RC ScriptServerEventHandler(ULONG lparam, IDispatcher* disp, IEvent* e);
	static EVENT_PROC_RC UpdateScriptZoneSpinDownDelayHandler(ULONG lparam, IDispatcher* disp, IEvent* e);

	EVENT_PROC_RC ScriptServerEventHandler(IEvent* se);

	// AB-Test
	void SetZoneConfigGroups(const ZoneIndex& zi, const std::vector<std::string>& configGroups);

	//
	// API functions.  These are called from the static API functions
	// put into the LUA address space and must therefore be threadsafe.
	//
	template <ScriptClientEvent::EventType eventType, typename Arg0, typename... Args>
	void SendPlayerScriptClientEvent(lua_State* L, boost::optional<NETID> clientId, Arg0&& arg0, Args&&... args); // clientId empty will send to everyone

	//
	// Cache ScriptClientEvent in game state manager for future retrieval when new player zones in.
	// Written as a template function to handle various Arg0 type forwarded from SendPlayerScriptClientEvent(). Default version should not be called (the hence assertions).
	// This function is be specialized based on Arg0 type. Currently only one specialization is supported (when arg0 is a PID).
	template <typename Arg0>
	void CacheScriptClientEvent(lua_State* L, ScriptClientEvent* sce, const Arg0& arg0, int gameStateModifierType, int subType, bool removeGameStateModifier) {
		static_assert(!std::is_same<Arg0, ScriptPIDType>::value, "Missing specialization for ScriptPIDType");
		assert(false);
	}

	// -------------------------------------------
	// Player APIs
	void PlayerTellCallback(void* vm, ULONG clientId, const char* str, const char* objectTitle, bool isTell);
	bool PlayerTeleportCallback(void* vm, ULONG clientId, double x, double y, double z, double rx, double ry, double rz);
	bool PlayerGotoUrlCallback(void* vm, ULONG clientId, const char* url);
	ScriptAPIRetType::BLOC PlayerGetLocationCallback(void* vm, ULONG clientId);
	ScriptAPIRetType::BLOC PlayerCastRayCallback(void* vm, ULONG clientId);
	bool PlayerSendEventCallback(void* vm, ULONG clientId, const std::vector<std::string>& data);
	bool PlayerLoadMenuCallback(void* vm, ULONG clientId, const char* menuName);
	std::string PlayerGetNameCallback(void* vm, ULONG clientId);
	std::string PlayerGetTitleCallback(void* vm, ULONG clientId);
	int PlayerGetPermissionsCallback(void* vm, ULONG clientId);
	ScriptAPIRetType::BLOC PlayerUseItemCallback(void* vm, ULONG clientId, GLID glid);
	ScriptAPIRetType::BLOC PlayerEquipItemCallback(void* vm, ULONG clientId, GLID glid);
	ScriptAPIRetType::BLOC PlayerUnEquipItemCallback(void* vm, ULONG clientId, GLID glid);
	ScriptAPIRetType::BLOC PlayerSaveEquippedCallback(void* vm, ULONG clientId, const std::string& outfitName);
	ScriptAPIRetType::BLOC PlayerGetEquippedCallback(void* vm, ULONG clientId);
	ScriptAPIRetType::BLOC PlayerSetEquippedCallback(void* vm, ULONG clientId, const std::string& glids, const std::string& outfitName);
	ScriptAPIRetType::BLOC PlayerRemoveAllAccessories(void* vm, ULONG clientId);
	ScriptAPIRetType::BLOC PlayerCheckInventoryCallback(void* vm, ULONG clientId, int globalId);
	void PlayerRemoveItemCallback(void* vm, ULONG clientId, int globalId);
	ScriptAPIRetType::BLOC PlayerAddItemCallback(void* vm, ULONG clientId, GLID glid, int quantity);
	ScriptAPIRetType::BLOC PlayerJoinInstanceCallback(void* vm, int instanceId, ULONG clientId);
	void PlayerSetAnimationCallback(void* vm, ULONG clientId, GLID glid);
	void PlayerSetEquippedItemAnimationCallback(void* vm, ULONG clientId, GLID glidItem, GLID glidAnim);
	void PlayerDefineAnimationCallback(void* vm, ULONG clientId, GLID standId, GLID runId, GLID walkId, GLID jumpId);
	bool PlayerSetPhysics(void* vm, ULONG clientId, std::vector<int> arg_vect);
	char PlayerGetGender(void* vm, ULONG clientId);
	bool PlayerSetTitleCallback(void* vm, ULONG clientId, const char* title);
	bool PlayerSetNameCallback(void* vm, ULONG clientId, const char* name);
	bool PlayerMove(void* vm, ULONG clientId, double x, double y, double z, double rx, double ry, double rz, double speed, int anim, int timeout, int recalcInterval, int aiMode, bool noPlayerControl, bool noPhysics);
	bool PlayerScaleCallback(void* vm, ULONG clientId, double x, double y, double z);
	bool PlayerRenderLabelsOnTop(void* vm, ULONG clientId, bool onTop);
	bool PlayerClosePresetMenu(void* vm, ULONG clientId, const std::string& menuName);
	bool PlayerMovePresetMenu(void* vm, ULONG clientId, const std::string& menuName, double x, double y);
	bool PlayerOpenYesNoMenu(void* vm, ULONG clientId, const std::string& titleStr, const std::string& descStr, const std::string& yesStr, const std::string& noStr, const std::string& idStr);
	bool PlayerOpenKeyListenerMenu(void* vm, ULONG clientId, const std::string& keysStr, const std::string& idStr);
	bool PlayerOpenTextTimerMenu(void* vm, ULONG clientId, const std::string& textStr, int timeInSec, const std::string& idStr);
	bool PlayerOpenButtonMenu(void* vm, ULONG clientId, const std::string& titleStr, const std::string& descStr, const std::string& btnStr, const std::string& idStr);
	bool PlayerOpenStatusMenu(void* vm, ULONG clientId, const std::string& titleStr, const std::string& statusStr, const std::string& idStr);
	bool PlayerOpenListBoxMenu(void* vm, ULONG clientId, const std::string& titleStr, const std::string& listStr, const std::string& btnStr, const std::string& idStr);
	bool PlayerOpenShopMenu(void* vm, ULONG clientId, const std::string& titleStr, const std::string& idStr);
	bool PlayerAddItemToShopMenu(void* vm, ULONG clientId, int objId, const std::string& itemNameStr, int cost, const std::string& iconStr, const std::string& idStr);
	bool PlayerUpdateCoinHUD(void* vm, ULONG clientId, const std::string& typeStr, int value);
	bool PlayerUpdateHealthHUD(void* vm, ULONG clientId, const std::string& typeStr, const std::string& targetStr, int health, int healthChange);
	bool PlayerUpdateXPHUD(void* vm, ULONG clientId, int level, int levelXP, int xpToNextLevel);
	void PlayerAddHealthIndicator(void* vm, ULONG clientId, int type, const std::string& idStr, int healthPercentage);
	void PlayerRemoveHealthIndicator(void* vm, ULONG clientId, int type, const std::string& idStr);
	bool PlayerShowStatusInfo(void* vm, ULONG clientId, const std::string& messageStr, int timeMs, int r, int g, int b);
	void PlayerSetCustomTextureOnAccessoryCallback(void* vm, ULONG clientId, GLID glid, const std::string& urlStr);
	void PlayerSaveZoneSpawnPointCallback(void* vm, ULONG clientId, float x, float y, float z, float rx, float ry, float rz, int playerIdOverride);
	void PlayerDeleteZoneSpawnPointCallback(void* vm, ULONG clientId, int playerIdOverride);
	bool PlayerSetVisibilityFlagCallback(void* vm, ULONG clientId, bool bVisible);
	void PlayerSetParticleCallback(void* vm, ULONG clientId, GLID glid, const std::string& boneName, int particleSlotId,
		double offsetX, double offsetY, double offsetZ,
		double dirX, double dirY, double dirZ,
		double upX, double upY, double upZ);
	void PlayerRemoveParticleCallback(void* vm, ULONG clientId, const std::string& boneName, int particleSlotId);
	void PlayerLookAtCallback(void* vm,
		ULONG clientId, int targetType, ULONG targetId, const std::string& targetBone,
		double offsetX, double offsetY, double offsetZ,
		bool followTarget, bool cancelableByPlayer);
	void PlayerAddIndicatorCallback(void* vm, ULONG clientId, int type, const std::string& id, double offsetX, double offsetY, double offsetZ, int glid, double speed, double moveY);
	void PlayerClearIndicatorsCallback(void* vm, ULONG clientId, int type, const std::string& id);
	bool PlayerSpawnVehicle(void* vm,
		ULONG clientId,
		int placementId,
		const Vector3f& offset,
		const Quaternionf& orientation,
		const Vector3f& dimensions,
		float speed, float hp,
		float loadSusp, float unloadSusp,
		float massKG,
		float wheelFriction,
		Vector2f jumpVelocityMPS,
		GLID glidThrottle,
		GLID glidSkid);
	bool playerModifyVehicle(ULONG clientId, float maxSpeedKPH, float horsepower, Vector2f jumpVelocityMPS);
	bool PlayerLeaveVehicle(lua_State* vm, ULONG clientId, int placementId);

	ULONG PlayerGetLinkedHomeId(lua_State* vm, const std::string& playerName);
	bool PlayerHasLinkedHome(lua_State* vm, const std::string& playerName);
	bool PlayerInLinkedHome(lua_State* vm, const std::string& playerName);
	bool GameHasDynamicLink(lua_State* vm);

	bool GetZoneCommunityId(void* vm, unsigned int& communityId, unsigned int zoneInstanceId, unsigned int zoneType);

	bool GetParentZoneInfoCallback(void* vm, unsigned int& parentZoneInstanceId, unsigned int& parentZoneIndex,
		unsigned int& parentZoneType, unsigned int& parentCommunityId, unsigned int zoneInstanceId, unsigned int zoneType);

	bool GetParentZoneInfoFromDB(unsigned int zoneInstanceId, unsigned int zoneType, unsigned int& parentZoneInstanceId, unsigned int& parentZoneIndex, unsigned int& parentZoneType, unsigned int& parentCommunityId);

	bool GetChildZoneIds(void* vm, std::vector<std::vector<ULONG>>& zoneIds, unsigned int zoneInstanceId, unsigned int zoneType);

	bool GameItemIsPlaced(lua_State* vm, unsigned int gameItemId);

	// -------------------------------------------
	// Game item APIs
	bool GameItemGetByApp(void* vm, std::vector<std::vector<std::string>>& gameItems);
	bool GameItemGetByPlayer(void* vm, const std::string& playerNameStr, std::vector<std::pair<int, int>>& gameItems);
	bool GameItemLoadAll(void* vm, std::vector<GameItem>& gameItems, unsigned int zone_instance_id, unsigned int zone_type);
	bool GameItemUpdate(void* vm, const GameItem& gameItem, unsigned int zone_instance_id, unsigned int zone_type);
	bool GameItemDelete(void* vm, int giId, unsigned int zone_instance_id, unsigned int zone_type);
	bool GameItemGetNextId(void* vm, int& _nextID);
	ScriptAPIRetType::BLOC GameItemPlaceCallback(void* vm, ULONG clientId, GLID glid, int gameItemId, double x, double y, double z, double rx, double ry, double rz);
	ScriptAPIRetType::BLOC GameItemPlaceAsUserCallback(void* vm, ULONG kanevaUserId, GLID glid, int gameItemId, double x, double y, double z, double rx, double ry, double rz);
	bool GameItemSetPosRotCallback(void* vm, ULONG placementId, double x, double y, double z, double rx, double ry, double rz);
	bool GameItemDeletePlacementCallback(void* vm, ULONG placementId);
	bool GameGetFrameworkEnabledCallback(void* vm);
	bool GameSetFrameworkEnabledCallback(void* vm, bool enabled);

	// -------------------------------------------
	// Server data access
	ScriptAPIRetType::BLOC GameGetItemInfo(lua_State* vm, int globalId);
	TimeMs GetEffectDuration(lua_State* vm, int globalId);
	void SetEffectCompletionTimer(ServerScriptPtr& ss, int objectPID, EffectCode type, TimeMs duration, GLID effectGLID);
	void CancelEffectCompletionTimer(ServerScriptPtr& ss, int objectPID, EffectCode type);

	// -------------------------------------------
	// Script APIs
	bool ScriptRecordMetric(void* vm, std::string message);
	void ScriptErrorCallback(void* vm, int error, const char* str, bool setZombie = false, int stackLevel = 1);
	void ScriptErrorCallback(const ServerScriptPtr& ss, int error, const char* str, bool setZombie = false, int stackLevel = 1);
	bool ScriptLogMessageCallback(void* vm, const char* str);
	bool ScriptDoFileCallback(lua_State* vm, const char* filename, std::string& errMsg);
	void ScriptGetSourceInfo(void* vm, int stackLevel, std::string& source, int& lineNumber);
	ScriptAPIRetType::BLOC ScriptGetZonesInfoCallback(void* vm);
	int ScriptGetCurrentZoneCallback(void* vm);
	void ScriptGetPlayersInZoneCallback(void* vm, std::set<ULONG>& players);
	ScriptAPIRetType::BLOC ScriptGetInstanceInfoCallback(void* vm, int instanceId);
	ScriptAPIRetType::BLOC ScriptCreateInstanceCallback(void* vm, ULONG zoneId, ULONG clientId);
	ScriptAPIRetType::BLOC ScriptLockInstanceCallback(void* vm, int instanceId, bool lock);
	bool ScriptGetCurrentInstanceCallback(void* vm, long& instanceId, long& zoneId, long& zoneType);
	bool ScriptGetCurrentGameIdCallback(void* vm, long& gameId);
	ScriptAPIRetType::ASYN ScriptMakeWebcallAsync(void* vm, std::string url, bool usePost, const AsyncUserArg& userArg);
	ScriptAPIRetType::BLOC ScriptMakeWebcallCallback(void* vm, std::string url, bool usePost, const AsyncUserArg& userArg);
	bool ScriptCallStoredProcedureCallback(void* vm, std::vector<int> args, std::vector<std::vector<std::string>>& retValues);
	bool ScriptSetEnvironmentCallback(void* vm, std::vector<double> args);
	bool ScriptSetDayStateCallback(void* vm, std::vector<double> args);
	unsigned long ScriptServerEngine::ScriptGetTimerFrequencyCallback(void* vm);
	void ZoneGetStatsCallback(void* vm, unsigned int& numberOfScripts, unsigned long& totalMemoryUsed, std::vector<ScriptRuntimeData>& scriptList);

	// -------------------------------------------
	// Object APIs
	ULONG ObjectGenerateCallback(void* vm, GLID glid, double x, double y, double z, double rx, double ry, double rz, int placementType, bool bufferCall = true);
	bool ObjectDeleteCallback(void* vm, int objId);
	bool ObjectMoveCallback(void* vm, int objId, double time, double accDur, double constVelDur, double accDistFrac, double constVelDistFrac, double x, double y, double z, double dx, double dy, double dz, double sx, double sy, double sz, bool bufferCall = true);
	bool ObjectScaleCallback(void* vm, int objId, double x, double y, double z, bool bufferCall = true);
	bool ObjectRotateCallback(void* vm, int objId, double time, double x, double y, double z, bool bufferCall = true);
	bool ObjectGetStartLocationCallback(void* vm, int objId, double& x, double& y, double& z, double& rx, double& ry, double& rz);
	ULONG ObjectGetIdCallback(void* vm);
	void ObjectSendMessageCallback(void* vm, int objId, const char* msg, unsigned delay = 0);
	bool ObjectMapEventsCallback(void* vm, int objId);
	bool ObjectSetTextureCallback(void* vm, int objId, const char* textureURL, bool bufferCall = true);
	bool ObjectSetTexturePanningCallback(void* vm, int objId, int meshId, int uvSetId, double uIncr, double vIncr);
	void ObjectSetAnimationCallback(lua_State* vm, int objId, GLID glidSpecial, eAnimLoopCtl animLoopCtl, bool elapseCallback, bool bufferCall);
	void ObjectControlCallback(void* vm, int objId, unsigned long playerId, ULONG ctlId, const std::vector<int>* pCtlArgs, bool bufferCall = true);
	void ObjectSetScriptCallback(void* vm, int objectId, const char* directory, const char* filename, const char* arg);
	void ObjectSetParticleCallback(void* vm, ULONG clientId, int objId, GLID glid, const std::string& boneName, int particleSlotId,
		double offsetX, double offsetY, double offsetZ,
		double dirX, double dirY, double dirZ,
		double upX, double upY, double upZ,
		bool bufferCall = true);
	void ObjectRemoveParticleCallback(void* vm, ULONG clientId, int objId, const std::string& boneName, int particleSlotId);
	ScriptAPIRetType::BLOC ObjectGetNextPlayListItemCallback(void* vm, ULONG clientId);
	ScriptAPIRetType::BLOC ObjectSetNextPlayListItemCallback(void* vm, ULONG clientId, int playListItem);
	ScriptAPIRetType::BLOC ObjectEnumeratePlayListCallback(void* vm, ULONG clientId);
	ScriptAPIRetType::BLOC ObjectSetPlayListCallback(void* vm, ULONG clientId, int playListID, int playListItem, bool stopBroadcast);
	void ObjectAddLabelsCallback(void* vm, ULONG clientId, ULONG objId, const std::vector<ObjectLabelInfo>& labels, bool bufferCall = true);
	void ObjectClearLabelsCallback(void* vm, ULONG clientId, ULONG objId, bool bufferCall = true);
	void ObjectSetOutlineCallback(void* vm, ULONG objectId, bool show, bool setColor, double red, double green, double blue, bool bufferCall = true);
	void ObjectSetMediaCallback(void* vm, ULONG objectId, const MediaParams& mediaParams);
	void ObjectSetMediaVolumeAndRadiusCallback(void* vm, ULONG objectId, const MediaParams& mediaParams);
	void SetEffectTrackCallback(void* vm, int id, ScriptClientEvent::EventType eventType, const std::vector<std::vector<int>>& allEffects, const std::vector<std::string>& allStrings);
	bool ObjectGetDataCallback(void* vm, const GLID& glid, std::vector<std::string>& retValues);
	bool ObjectGetOwnerCallback(void* vm, ULONG pid, std::vector<std::string>& retValues);
	bool ObjectSetCollisionCallback(void* vm, int objectId, bool collisionValue, bool bufferCall = true);
	void ObjectSetMouseOverCallback(void* vm, int objType, ULONG objId, int mosMode, const std::string& tooltipStr, int cursorType, int mosCategory, bool bufferCall = true);
	bool ObjectSetDrawDistanceCallback(void* vm, int objId, double dist, bool bufferCall = true);
	void ObjectLookAtCallback(void* vm,
		ULONG objectId, int targetType, ULONG targetId, const std::string& targetBone,
		double offsetX, double offsetY, double offsetZ,
		bool followTarget, bool allowXZRotation,
		double forwardX, double forwardY, double forwardZ);

	// Sound functions
	void SoundAddModifierCallback(void* vm, int soundPlacementId, int input, int output, double gain, double offset);
	bool SoundAttachCallback(void* vm, int placementId, int attachType, ULONG target, bool bufferCall = true);
	bool SoundPlayOnClient(void* vm, int globalId, ULONG targetId);
	bool SoundStopOnClient(void* vm, int globalId, ULONG targetId);

	// Dictionary functions -- all dictionary functions are global and do not use VM parameter
	bool DictionaryGenerate(const char* name, HDICT& hDict, fast_recursive_mutex*& hDictMutex);
	int DictionaryGetValue(const DictUserData* udDict, const char* key, void* buffer, size_t bufSize);
	bool DictionarySetValue(const DictUserData* udDict, const char* key, lua_State* vm, int index);
	bool DictionarySetChildTableValue(const DictUserData* udDict, const char* key, const char* val);
	bool DictionaryRemoveValue(const DictUserData* udDict, const char* key);
	bool DictionaryClear(const DictUserData* udDict);
	bool DictionaryAddRef(const char* name);
	bool DictionaryRelease(const char* name);
	int DictionaryGetRefCount(const DictUserData* udDict);
	bool DictionaryGetAllKeys(const DictUserData* udDict, std::set<std::string>& allKeys);
	std::string DictionaryAutoGenerateName();
	std::string DictionaryGetDomainPrefix(void* vm);
	const char* DictionaryStripDomainPrefixFromName(const char* name);

	// NPC Functions
	int NPCSpawnCallback(
		void* vm,
		int siType,
		int placementId,
		const std::string& charConfigJson,
		int ccId,
		int dbIndex,
		const Vector3f& pos,
		double rotDeg,
		GLID glidAnimSpecial,
		const std::vector<GLID>& glidsArmed);

	// Game state container functions
	int ObjectGameStateGetValue(void* vm, ULONG pid, ILuaSharedDataProvider::Key key, void* buffer, size_t bufSize);
	int PlayerGameStateGetValue(void* vm, ULONG clientId, ILuaSharedDataProvider::Key key, void* buffer, size_t bufSize);

	// Inter-VM messaging
	void ScriptBroadcastMessage(lua_State* vm, const std::string& msg, bool currentZone, bool linkedZones); // If currentZone is true, broadcast to other scripts in the current zone. If linkedZones is true, broadcast to other linked zones
	void ScriptSetMessagePolicy(lua_State* vm, bool acceptsInZoneBroadcast, bool acceptsInterZoneBroadcast);

	//
	// Helper functions
	//
	bool SendToClient(IEvent* sce);
	bool SendDirectlyToClient(NETID clientId, IEvent* sce); // Send to one client
	bool SendDirectlyToZone(const ZoneIndex& zoneIndex, IEvent* sce); // Sends to all clients in zone.
	void SendErrorToClient(const ServerScriptPtr& ss, NETID clientNetId);
	void QueueSaveScriptTask(ULONG placementId, const std::string& fullPath);
	void SendPlayerTellToClient(ULONG clientId, const char* str, const char* objectTitle, bool isTell, int placementId);

	unsigned IncrementZonePopulation(const ZoneIndex& zoneIndex, bool& isNew);
	unsigned DecrementZonePopulation(const ZoneIndex& zoneIndex, bool removeIfEmpty);
	void SetZoneMaxPlacementId(const ZoneIndex& zoneIndex, ULONG maxPlacementId);
	ULONG GetZoneMaxPlacementId(const ZoneIndex& zoneIndex);
	ULONG GetIncrementedZoneMaxPlacementId(const ZoneIndex& zoneIndex);
	void SetZoneSpinDownDelayMinutes(const ZoneIndex& zoneIndex, int minutes, bool ignoreIfNotFound);
	unsigned GetZoneSpinDownDelayMinutes(const ZoneIndex& zoneIndex);
	void SetZoneFinalDepartureTime(const ZoneIndex& zoneIndex, TimeMs timestamp, bool ignoreIfNotFound);
	void GetAllExpiredZones(std::set<ZoneIndex>& zones);

	std::set<std::string> GetAllScriptFileNamesByType(ScriptServerEvent::UploadAssetTypes assetType);
	void GetScriptFileNamesFromFolder(const std::string& dir, const std::string& extension, std::set<std::string>& fileNames);
	bool CreateFileFromChunks(std::string dir, std::string filename, int chunkNumber, BYTE* buffer, unsigned int bytesToWrite, std::string extension, unsigned long& SeekEnd);
	bool SendAssetToClient(ScriptServerEvent::UploadAssetTypes assetType, int seekPosition, std::string fileName, NETID clientNetId);
	void GetZonePermanentObjects(ZoneIndex zi, std::set<ULONG>& permObjs);
	TaskSystem* GetTaskSystem() {
		return &m_taskSystem;
	};
	std::string GetActionInstanceScriptName(int placementId);
	void SendScriptShutdownEvent(const ServerScriptPtr& pScript, NETID clientNetId, const std::string& reason);

	enum PlayListResultTypes {
		NextPlayListItem,
		AllPlayListItems
	};

	void QueuePlayListResults(ScriptServerEvent* e, const ServerScriptPtr& ss, PlayListResultTypes type);

	void LogSettings();
	void LoadDevToolsFolders();

	// Returns true if the player has the minPermission or better (lower number)
	bool CheckPlayerPermissions(NETID clientNetId, int minPermission, std::string caller, bool failSilently = false);

	// Search path management
	bool GetAllSourceDirsAndExtension(ScriptServerEvent::UploadAssetTypes assetType, std::string& primaryDir, std::vector<std::string>& otherDirs, std::string& extension);
	bool GetSourceDirAndExtension(ScriptServerEvent::UploadAssetTypes assetType, const std::string& baseName, std::string& sourceDir, std::string& extension);
	bool GetScriptFullPathForRead(const std::string& relativePath, std::string& fullPathScript);
	bool GetScriptFullPathForWrite(const std::string& relativePath, std::string& fullPathScript);
	bool GetPublishingInfo(ScriptServerEvent::UploadAssetTypes assetType, const std::string& baseName,
		std::string& sourceDir, std::string& destDir, std::string& fullName,
		bool& isPatch, std::string& patchPrefix, bool& compileLua);
	ScriptServerEvent::UploadAssetTypes GetAssetTypeByRelPath(const std::string& relPath);

	// Delete a script or action item
	bool DeleteScript(ScriptServerEvent::UploadAssetTypes assetType, const std::string& baseName);

	//register patch URL
	bool RegisterAppPatchURL();

	//////////////////////////////////////////////////////////////////////////
	// BEGIN Player and Game Save Data
	//////////////////////////////////////////////////////////////////////////
	// Version 2 (current as of 3 June, 2014)
	bool ScriptSavePlayerDataCallback_v2(void* vm, const char* playerName, const char* attribute, const char* value, unsigned int zone_instance_id = 0, unsigned int zone_type = 0);
	bool ScriptLoadAllPlayerDataCallback_v2(void* vm, const char* playerName, ScriptDataMap& playerData, unsigned int zone_instance_id = 0, unsigned int zone_type = 0);
	std::string ScriptLoadPlayerDataCallback_v2(void* vm, const char* playerName, const char* attribute, unsigned int zone_instance_id = 0, unsigned int zone_type = 0);
	bool ScriptDeletePlayerDataCallback_v2(void* vm, const char* playerName, const char* attribute, unsigned int zone_instance_id = 0, unsigned int zone_type = 0);
	bool ScriptDeleteAllPlayerDataCallback_v2(void* vm, const char* playerName, unsigned int zone_instance_id = 0, unsigned int zone_type = 0);

	bool ScriptSaveGameDataCallback_v2(void* vm, const char* attribute, const char* value, unsigned int zone_instance_id = 0, unsigned int zone_type = 0);
	bool ScriptLoadAllGameDataCallback_v2(void* vm, ScriptDataMap& gameData);
	std::string ScriptLoadGameDataCallback_v2(void* vm, const char* attribute, unsigned int zone_instance_id = 0, unsigned int zone_type = 0);
	bool ScriptDeleteGameDataCallback_v2(void* vm, const char* attribute, unsigned int zone_instance_id = 0, unsigned int zone_type = 0);
	bool ScriptDeleteAllGameDataCallback_v2(void* vm);
	//////////////////////////////////////////////////////////////////////////
	// END Player and Game Save Data
	//////////////////////////////////////////////////////////////////////////

	bool GameLoadGlobalConfig(const std::vector<std::string>& _propertyList, ScriptDataMap& _outMap);
	bool GameInitFrameworkDB(void* vm);

	// Paths and URLs
	std::string m_devToolsSourceFolder;
	std::string m_devToolsAppSourceFolder;
	std::string m_devToolsServerFolder;
	std::string m_devToolsAppServerFolder;
	std::string m_devToolsPatchFolder;
	std::string m_devToolsAppPatchFolder;
	std::string m_devToolsAppPatchUrl;
	std::string m_devToolsLuaCompiler;
	std::string m_devToolsAddServerUrl;

protected:
	bool Upload3DAppAssetEventHandler(ScriptServerEvent* e, ZoneIndex zoneId, NETID clientNetId);
	bool DownloadScriptEventHandler(ScriptServerEvent* se, NETID clientNetId);
	bool DeleteAssetEventHandler(ScriptServerEvent* se, NETID clientNetId);
	bool AttachScriptEventHandler(ScriptServerEvent* se, NETID clientNetId, ULONG objPlacementId);
	bool DetachScriptEventHandler(ScriptServerEvent* se, NETID clientNetId, ULONG objPlacementId);
	bool DeleteScriptEventHandler(ScriptServerEvent* se);
	bool LoadTextFile(const std::string& fullPath, std::string& text);
	bool LoadScriptFile(const char* fileName, std::string& sourceCode);
	void TransmitGameStateHandler(std::shared_ptr<GameStateManager> gsm, ZoneIndex zi, NETID netId);
	bool GetServerLogEventHandler(ScriptServerEvent* se, NETID clientNetId);
	void AddPendingLoadScriptTask(LoadScriptTask* task) {
		m_pendingLoadScriptTasks.insert(task); // Caller must acquire m_eventHandlerCS first
	}
	void RemovePendingLoadScriptTask(LoadScriptTask* task) {
		m_pendingLoadScriptTasks.erase(task); // Caller must acquire m_eventHandlerCS first
	}
	void StopPendingLoadScriptTasks(const ZoneIndex& zi);

private:
	void ObjectRotateHelper(const ServerScriptPtr& ss, int objId, double time, double dx, double dy, double dz, double sx, double sy, double sz, bool bufferCall);

	friend class RunVMTask; // for now, at least
	friend class LoadScriptTask;

	struct ZoneInfo {
		unsigned m_population;
		unsigned m_maxPlacementId;
		unsigned m_spinDownDelayMinutes;
		TimeMs m_finalDepartureTime;

		ZoneInfo(unsigned pop = 0, unsigned maxPID = 0, unsigned spinDownDelayMins = 0) :
				m_population(pop), m_maxPlacementId(0), m_spinDownDelayMinutes(spinDownDelayMins), m_finalDepartureTime(0) {}
	};

	typedef std::map<ZoneIndex, ZoneInfo> ZoneInfoMap;
	typedef std::map<ZoneIndex, std::vector<std::string>> ZoneConfigGroups;
	typedef std::map<std::string, int> NamePlayerIdMap;
	typedef std::map<NETID, ScriptPlayerInfo> NETIDPlayerInfoMap;
	typedef std::map<NETID, ZoneIndex> NETIDZoneIndexMap;
	typedef std::map<GLID, std::vector<std::string>> ItemDataCache;
	typedef std::map<ULONG, std::vector<std::string>> ItemOwnerCache;
	typedef std::map<std::string, ILuaSharedDataProvider*> DictionaryMap;

	void initializeDBConnection(const char* gameDir, const DBConfig& cfg, const std::string& memcacheUrl);
	bool LoadScript(const char* filename, const std::string& sourceCode, const char* arg, ULONG callerPID, ULONG objPlacementId, ZoneIndex zoneId, NETID clientNetId, bool isSystemScript, UINT kgpStartWaitTimeout);
	void RemovePlayerFromZone(NETID clientNetId, ZoneIndex departedZone, ZoneIndex newZone, const ScriptPlayerInfo& playerInfo, bool disconnected);
	void AddPlayerToZone(NETID clientNetId, ZoneIndex zi, NETID prevNetID, ZoneIndex prevZi, unsigned playerArriveId, const ScriptPlayerInfo& playerInfo, unsigned parentZoneInstanceId);
	void loadRestAPIRegistrations();
	int getGameId();

	void HandlePlayerArrive(ScriptServerEvent* sse, NETID clientNetId, const ZoneIndex& zi, unsigned playerArriveId);
	void HandlePlayerDepart(ScriptServerEvent* sse, NETID clientNetId, const ZoneIndex& zi, unsigned playerArriveId);

	void ProcessPendingPlayerArrives();

	bool getScriptFullPathUnmanaged(const std::string& relativePath, std::string& fullPathScript);

	// Process previously recorded pending tasks from lua yield. Return true if script is set to BLOCKED.
	bool processLuaYield(const ServerScriptPtr& ss);

	// Handle engine-level timer event
	void handleTimerEvent();

	// Create or update wok.script_zones record
	void updateScriptZoneState(const ZoneIndex& zoneIndex, bool spinUp);

	// Get game state manager by zone index (caller should acquire m_eventHandlerCS)
	const std::shared_ptr<GameStateManager>& getGameStateManager(const ZoneIndex& zoneIndex) {
		static std::shared_ptr<GameStateManager> nullGSM;
		auto itr = m_gameStates.find(zoneIndex);
		if (itr != m_gameStates.end())
			return itr->second;
		return nullGSM;
	}

	// Moved from ScriptMap class
	void SendTimerEventToAllZones();
	void SendArrivedEventToZone(ZoneIndex zoneId, NETID clientNetId, const std::string& playerName);
	void SendDepartedEventToZone(ZoneIndex zoneId, NETID clientNetId, const ScriptPlayerInfo& playerInfo, bool disconnected);
	void SendShutdownEventToZone(ZoneIndex zoneId, const std::string& reason);
	void SendMenuEventToZone(IEvent* e, ZoneIndex zoneId, NETID clientNetId);
	void SendObjectMessageEventToScript(ScriptPID srcId, ZoneIndex dstZoneIndex, ScriptPID dstId, const char* msg, unsigned delay, bool isBroadcast, bool isInterZone);
	void SendObjectMessageEventToZone(ScriptPID srcId, ZoneIndex dstZoneIndex, const ScriptZonePtr& pZone, const char* msg, unsigned delay, bool isInterZone);
	void SendScriptInformationToClient(ZoneIndex zoneId, NETID clientNetId);

private:
	static ScriptServerEngine* m_instance;

	MessageQueueAgent* m_messageQueueAgent;
	bool m_isMyMessageQueueAgent;
	IDispatcher* m_myDispatcher;
	std::string m_scriptDir;
	TimeMs m_maxLoopTime;
	TimeMs m_processEventTime;
	std::string m_key;
	ULONG m_maxConnections;
	ItemDataCache m_itemDataCache;
	ItemOwnerCache m_itemOwnerCache;
	DictionaryMap m_dictionaryMap;
	fast_recursive_mutex m_dictionaryMapSync;
	int m_dictionaryAutoIndex;
	fast_recursive_mutex m_dictionaryAutoIndexSync;

	//
	// The ScriptMap contains all the scripts in the ScriptServerEngine.
	// m_taskSystem manages the background processing threads and a list
	// of all the outstanding tasks.
	//
	// When an event arrives, if the task is idle a RunVMTask object is
	// created and the task will be run.  Otherwise, the event is added
	// to the pending event queue for that VM.
	//
	// When an task is processed, it either finishes completely or yields.
	// If it yields, then the same task is added back onto the task queue.
	// If it finishes completely, then the pending event queue is checked
	// If there is a pending event, the function name and arguments are pushed
	// but the call to resume isn't made and a new task is pushed onto
	// the task list (this done by re-calling the event handler from the BG
	// thread, just as if the event had arrived via the dispatcher)
	// otherwise, the task is deleted (no more processing until another event
	// arrives).
	//
	// The one tricky bit is that lua_resume needs to know the number of arguments,
	// so that will have to be recorded.
	//
	ScriptMap m_zoneMap;
	ScriptServerTaskSystem m_taskSystem;
	ScriptServerTaskSystem m_loadScriptTaskSystem; // A separate task system to allow loadScriptTask to block on kgp_start
	fast_recursive_mutex m_eventHandlerCS;
	fast_recursive_mutex m_dbAccessCS;
	std::mutex m_zoneInfoMapMutex; // Protects m_zoneInfoMap - use non reentrant mutex
	ZoneInfoMap m_zoneInfoMap;
	ZoneConfigGroups m_zoneConfigGroups;
	NamePlayerIdMap m_playerIds;
	NETIDPlayerInfoMap m_players;
	NETIDZoneIndexMap m_playerZones;
	std::set<LoadScriptTask*> m_pendingLoadScriptTasks; // Synchronized through m_eventHandlerCS
	//
	// Background DB tasks are serialised through here, because they all use
	// the same connection.  It might be worthwhile to change this, pending
	// performance results.
	//
	KGP_Connection* m_conn;
	//
	// Some Lua-related configuration options.
	//
#define YIELD_COUNT_ALERT_THRESHOLD 500
#define YIELD_COUNT_ALERT_FILTER 500
#define DEFAULT_WORKER_THREADS_TAG "TaskSystemThreadCount"
#define DEFAULT_MAX_SCRIPTS_PER_ZONE 1000
#define DICTIONARY_AUTONAME_PREFIX "__AT."
#define DICTIONARY_DOMAIN_DELIMTER "!"
#define DEFAULT_ALLOWED_UPLOAD_ASSET_TYPES 0xFFFFFFFF // All types
#define DEFAULT_SCRIPT_ZONE_SPIN_DOWN_DELAY_MINUTES 10

	LuaVMConfig m_globalVMConfig;
	ScriptAPIRegistry* m_scriptAPIRegistry;
	int m_taskSystemThreadCount;
	static ULONG m_maxScriptsPerZone;
	DWORD m_allowedUploadAssetTypes; // Bit mask for all allowed asset types for uploading
	bool m_isolateDictionariesByZone; // If true: dictionary names will be prefixed with zone index (for WOK only)
	unsigned m_defaultSpinDownDelayMinutes;
	std::map<ZoneIndex, std::shared_ptr<GameStateManager>> m_gameStates;

	bool m_patchURLRegistered; // Filter to ensure we make up to one (successful) wokweb call per server session to register patch URL
	std::string m_pathBase; // PathBase is appPath For Engine Blades
	bool m_baseDirReadOnly;
	const DBConfig* m_dbConfig;
	std::string m_worldExperimentUrl;
	JsonConfig* m_globalJson;
	TimeMs m_lastZoneSpinDownExpiryCheckTimeMs;
	std::unique_ptr<Scheduler> m_pScheduler;

	struct PendingPlayerArrive {
		ScriptServerEvent* pPlayerArriveEvent;
		NETID clientNetId;
		ZoneIndex zoneIndex;
		unsigned int playerArriveId;
	};
	std::map<NETID, PendingPlayerArrive> m_pendingPlayerArrives; // A queue for player arrive events received while zone is being stopped (no mutex, used by main event thread only)
};

//
// Cache ScriptClientEvent in game state manager for future retrieval when new player zones in.
// Specialization for ScriptPIDType - currently only APIs with ScriptPIDType as arg0 will be cached in game state manager.
template <>
inline void ScriptServerEngine::CacheScriptClientEvent<ScriptPIDType>(lua_State* L, ScriptClientEvent* sce, const ScriptPIDType& pid, int gameStateModifierType, int subType, bool removeGameStateModifier) {
	ServerScriptPtr ss = m_zoneMap.getScriptByVM(L);
	assert(ss);
	if (ss) {
		if (removeGameStateModifier) {
			ss->GetGameStateManager()->RemoveGameStateObjectModifier(ss->GetZoneId(), pid.value, gameStateModifierType, subType);
		} else {
			ScriptClientEvent* e = new ScriptClientEvent(sce);
			e->SetRoutingOption(ScriptClientEvent::Routine_Client);
			ss->GetGameStateManager()->AddGameStateObjectModifier(ss->GetZoneId(), e, pid.value, subType);
		}
	}
}

template <ScriptClientEvent::EventType eventType, typename Arg0, typename... Args>
inline void ScriptServerEngine::SendPlayerScriptClientEvent(lua_State* L, boost::optional<NETID> clientId, Arg0&& arg0, Args&&... args) {
	ScriptClientEvent* e = new ScriptClientEvent(eventType, SERVER_NETID, clientId ? ScriptClientEvent::Scope_Player : ScriptClientEvent::Scope_Zone);
	StreamTo(*e->OutBuffer(), arg0, args...);

	if (clientId) {
		// Send to target player
		SendDirectlyToClient(clientId.get(), e);
	} else {
		// Cache event in game state manager if `GameStateModifierType' is set for this event.
		if (SCE_ClientHandler<eventType>::GameStateModifierType != ScriptClientEvent::InvalidEventType) {
			// ScriptClientEvents that can be cached in game state manager must have a matching specialization of
			// CacheScriptClientEvent() function with the type of arg0. Currently the function is only specialized
			// for ScriptPIDType (for a PID argument). Unspecialized version does assert(false) only.
			static_assert(SCE_ClientHandler<eventType>::GameStateModifierType == ScriptClientEvent::InvalidEventType || std::is_same<std::decay_t<Arg0>, ScriptPIDType>::value, "Only ScriptPIDType supported for CacheScriptClientEvent");
			CacheScriptClientEvent<std::decay_t<Arg0>>(L, e, arg0, SCE_ClientHandler<eventType>::GameStateModifierType, SCE_ClientHandler<eventType>::GameStateModifierSubType(arg0, args...), SCE_ClientHandler<eventType>::RemoveGameStateModifier);
		}

		// Broadcast to target zone
		SendDirectlyToZone(ServerScript::GetZoneIndexFromRegistry(L), e);
	}
}

}; // namespace KEP
