///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "TaskSystem.h"
#include "ScriptServerTaskObject.h"
#include "InstanceId.h"

namespace KEP {

class ScriptServerEngine;

class ScriptServerTaskSystem : public TaskSystem {
public:
	ScriptServerTaskSystem(ScriptServerEngine* server) :
			TaskSystem("ScriptServer"),
			m_server(server) {
		jsAssert(m_server != NULL);
	}

	void QueueTask(ScriptServerTaskObject* task) {
		task->SetServer(m_server);
		TaskSystem::QueueTask(task);
	}

private:
	using TaskSystem::QueueTask;

private:
	ScriptServerEngine* m_server;
};

}; // namespace KEP
