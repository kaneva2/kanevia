///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Scheduler.h"
#include "ServerScript.h"
#include "IDispatcher.h"
#include "ScriptServerTaskSystem.h"
#include "RunVMTask.h"
#include "LogHelper.h"

namespace KEP {

void Scheduler::startScript(const ServerScriptPtr& pScript, NETID clientNetId, const char* arg, ULONG callerPID) {
	assert(pScript->GetState() == ScriptState::STARTING);

	ScriptServerEvent* se = new ScriptServerEvent(ScriptServerEvent::Start, 0, 0, 0, clientNetId);
	*se->OutBuffer() << (arg ? arg : "") << callerPID;

	// Enable event queue and add event
	if (pScript->AddEvent(se, true)) {
		assert(pScript->GetState() == ScriptState::READY);
		// Notify scheduler to process the event and schedule a VM task if needed (normally)
		queueScript(pScript, "startScript");
	}
}

void Scheduler::scheduleScript(const ServerScriptPtr& pScript, size_t numArgs, bool unblocked, bool resumed) {
	ScriptState oldState = ScriptState::READY;
	if (pScript->SetState(ScriptState::SCHEDULED, oldState)) {
		assert(oldState == ScriptState::READY);

		// Create a new processing task.
		bool isNewTask = !unblocked && !resumed;
		RunVMTask* task = new RunVMTask(this, pScript, isNewTask, numArgs);
		if (task) {
			m_taskSystem.QueueTask(task);
			return;
		}
	}

	assert(false);
	LogErrorTo(m_logger, pScript->LogSubject() << "Failed to create new RunVMTask for " << pScript->GetCurrentGlueName());

	// Set script state to IDLE (or ZOMBIE??)
	pScript->SetState(ScriptState::IDLE, oldState);
}

void Scheduler::scriptCallRunning(const ServerScriptPtr& pScript) {
	ScriptState oldState = ScriptState::SCHEDULED;
	if (!pScript->SetState(ScriptState::RUNNING, oldState)) {
		// Not updated
		assert(false);
		return;
	}

	assert(oldState == ScriptState::SCHEDULED);
}

void Scheduler::scriptCallCompleted(const ServerScriptPtr& pScript) {
	// Signal task completion
	if (pScript->LuaCallTaskCompleted()) {
		// Get ready to process next event
		queueScript(pScript, "scriptCallCompleted");
	}
}

void Scheduler::scriptCallPreempted(const ServerScriptPtr& pScript) {
	ScriptState oldState = ScriptState::RUNNING;
	if (!pScript->SetState(ScriptState::YIELDED, oldState)) {
		// Not updated
		assert(false);
		return;
	}

	assert(oldState == ScriptState::RUNNING);
	queueScript(pScript, "scriptCallPreempted");
}

void Scheduler::scriptCallBlocked(const ServerScriptPtr& pScript, ScriptServerEvent::EventType unblockEventType, const std::string& reason) {
	ScriptState oldState = ScriptState::RUNNING;
	if (!pScript->SetState(ScriptState::BLOCKED, unblockEventType, reason, oldState)) {
		// Not updated
		assert(false);
		return;
	}

	assert(oldState == ScriptState::RUNNING);
	if (oldState != ScriptState::RUNNING) {
		LogError(pScript->LogSubject() << "Set state to BLOCKED, current state is " << static_cast<int>(oldState) << ", expected: RUNNING");
	}

	std::lock_guard<std::mutex> lock(m_blockedScriptsSync); // Serialize access to m_blockedScripts
	m_blockedScripts.push_back(pScript); // ScriptServerEngine checks this list to see if a script should timeout
}

void Scheduler::scriptCallUnblocked(const ServerScriptPtr& pScript) {
	std::lock_guard<std::mutex> lock(m_blockedScriptsSync);
	std::list<ServerScriptPtr>::iterator itr = m_blockedScripts.begin();
	while (itr != m_blockedScripts.end()) {
		// Remove the script pointer from the list, so it won't
		// be timed out.
		if (*itr == pScript) {
			m_blockedScripts.erase(itr);
			break;
		}
		itr++;
	}
}

void Scheduler::scriptCallErred(const ServerScriptPtr& pScript) {
	// Signal task completion
	if (pScript->LuaCallTaskCompleted()) {
		// Get ready to process next event
		queueScript(pScript, "scriptCallErred");
	}
}

void Scheduler::scriptCallAborted(const ServerScriptPtr& pScript) {
	// Something is wrong
	assert(false);

	// Signal task completion
	if (pScript->LuaCallTaskCompleted()) {
		// Get ready to process next event
		queueScript(pScript, "scriptCallAborted");
	}
}

void Scheduler::scriptUnloaded(const ServerScriptPtr& pScript) {
	// Check if there is a pending reload request (not sure if this is still useful however it's unlikely to be compatible with the new zone stopping flag)
	NETID clientNetId = 0;
	std::string fileName;
	if (pScript->FetchPendingReloadRequest(clientNetId, fileName)) {
		LogInfo(pScript->LogSubject() << "Queue an event to reload this script");
		ScriptServerEvent* sce = new ScriptServerEvent(ScriptServerEvent::AttachScript, pScript->GetPlacementId(), pScript->GetZoneId().toLong(), 0, clientNetId);
		(*sce->OutBuffer()) << fileName;
		m_pDispatcher->QueueEvent(sce);
	}
}

void Scheduler::sendEventToScript(const ServerScriptPtr& pScript, ScriptServerEvent* se) {
	if (pScript->AddEvent(se, false)) {
		ScriptState state = pScript->GetState();
		state;
		assert(state == ScriptState::READY || state == ScriptState::BLOCKED || state == ScriptState::ZOMBIE && se->GetEventType() == ScriptServerEvent::UnloadScript);

		// Notify scheduler to process the event and schedule a VM task if needed (normally)
		queueScript(pScript, "sendEventToScript");
	}
}

void Scheduler::checkScriptBlockingTimeout(unsigned timeoutMs, bool timeoutIsFatal) {
	//
	// m_blockedScripts are pushed_back and pop_front, so that they are
	// in time order.  The first script to block is at the front of the
	// list, so I can stop looking as soon as I find a script that hasn't
	// yet timed out.
	//
	std::lock_guard<std::mutex> lock(m_blockedScriptsSync);
	auto itr = m_blockedScripts.begin();
	if (itr != m_blockedScripts.end()) {
		TimeMs duration = 0;
		std::string reason;
		const ServerScriptPtr& pScript = *itr;
		if (pScript->GetBlockingInfo(duration, reason) && duration > timeoutMs) {
			// API function timed out
			LogWarn(pScript->LogSubject() << "Script " << pScript->GetFileName() << " has blocked for too long, reason: " << reason);

			if (timeoutIsFatal) {
				// For now, I'll just move the script into an error state.
				// If the script isn't associated with a VM, it shouldn't be in any of
				// the lists and should die when its ref. cnt goes to zero after the pop_front.

				// TODO
				assert(false);
				//ScriptErrorCallback(*itr, ServerScript::TIMEOUT_ERROR, "API function timed out.", true, 0);

				m_blockedScripts.pop_front();
			} else {
				// Graceful API timeout - resume VM and return nil
				LogInfo(pScript->LogSubject() << "Resume VM from blocked API: [" << reason << "]");
				ScriptServerEvent* sce = new ScriptServerEvent(ScriptServerEvent::GracefulAPITimeout, pScript->GetPlacementId(), pScript->GetZoneId().toLong(), pScript->GetZoneId().GetInstanceId());
				m_pDispatcher->QueueEvent(sce);
			}
		}
	}
}

void Scheduler::worker(Scheduler* me) {
	while (true) {
		ServerScriptPtr pScript = me->m_schedulerQueue.pop();
		if (!pScript) {
			// aborted
			break;
		}

		ScriptState state = pScript->GetState();
		state;
		assert(state == ScriptState::READY || state == ScriptState::BLOCKED || state == ScriptState::YIELDED || state == ScriptState::ZOMBIE);

		size_t numberOfArgs = 0;
		bool unblocked = false, resumed = false;
		if (pScript->CheckReadiness(numberOfArgs, unblocked, resumed)) {
			if (unblocked) {
				me->scriptCallUnblocked(pScript);
			}
			me->scheduleScript(pScript, numberOfArgs, unblocked, resumed);
		} else if (pScript->GetState() == ScriptState::STOPPED) {
			me->scriptUnloaded(pScript);
		}
	}
}

void SchedulerQueue::push(const ServerScriptPtr& pScript) {
	assert(pScript);

	ScriptState state = pScript->GetState();
	state;
	assert(
		state == ScriptState::READY || // Received event: IDLE -> READY or RUNNING -> READY
		state == ScriptState::BLOCKED || // Received unblock event
		state == ScriptState::YIELDED || // Reschedule after preempted
		state == ScriptState::ZOMBIE // Received UnloadScript in ZOMBIE state
	);

	pushOne(pScript);
}

} // namespace KEP
