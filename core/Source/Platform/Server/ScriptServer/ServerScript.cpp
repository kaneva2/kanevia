///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "ServerScript.h"
#include "Event/Base/IEvent.h"
#include "Event/LuaEventHandler/LuaScriptVM.h"
#include "Event/Events/ScriptServerEvent.h"
#include "GameStateManager.h"
#include "Script_API.h"
#include "ScriptMap.h"
#include "LuaHelper.h"
#include "Common/Include/KEPHelpers.h"
#include "CallEventHandlerWithArgs.h"
#include "ScriptPlayerInfo.h"
#include "ScriptEventArgValueHandlers.h"
#include "LuaAsyncFunc.h"
#include "LuaStackHelper.h"
#include "Core/Util/StaticLoop.h"

#include <jsBERDecoder.h>
#include <log4cplus/logger.h>

using namespace log4cplus;

namespace KEP {

extern Logger script_logger;
static LogInstance("ScriptServerEngine");

static void* ScriptMemoryAllocator(void* ud, void* ptr, size_t old_size, size_t new_size);
static void CountHook(lua_State* L, lua_Debug* ar);

ServerScript::EventHandlerInfoMap ServerScript::s_eventHandlerInfoMap;

ServerScript::ServerScript(const LuaVMConfig& luaVMConfig, const std::shared_ptr<GameStateManager>& pGSM, const std::shared_ptr<ScriptZone>& pScriptZone,
	ULONG placementId, bool isSystemScript, int assetType, const char* fileName) :
		m_luaVMConfig(luaVMConfig),
		m_luaMain(NULL),
		m_luaThread(NULL),
		m_pGameStateManager(pGSM),
		m_placementId(placementId),
		m_pZone(pScriptZone),
		m_errorCode(NONE),
		m_state(ScriptState::STARTING),
		m_recentYieldCount(0),
		m_handlerBitField(UNLOAD_BIT), // All scripts support unload requests
		m_currentMemory(0),
		m_unblockEvent(nullptr),
		m_blockedAt(0),
		m_unblockEventType(ScriptServerEvent::None),
		m_stopping(false),
		m_stoppingTime(0),
		m_callCompletionEvent(NULL),
		m_isSystemScript(isSystemScript),
		m_assetType(assetType),
		m_fileName(fileName ? fileName : "(null)"),
		m_currTask(NULL),
		m_acceptsInZoneBroadcast(true),
		m_acceptsInterZoneBroadcast(false) {
	assert(m_pGameStateManager);
	assert(m_pZone);
	memset(m_times, 0, sizeof m_times);

	// Common Logging labels
	StrBuild(m_logSubject, "[" << GetZoneId().toLong() << "." << m_placementId << "] ");
	SetCurrentGlueName(""); // this will update m_logSig

	// Event to be signaled when script enters STOPPED state
	m_stoppedEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (m_stoppedEvent == NULL)
		LogError(m_logSubject << "Error creating stopped event, err=" << GetLastError());

	// Event to be signaled by RunVMTask upon completion
	m_callCompletionEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (m_callCompletionEvent == NULL) {
		LogError(m_logSubject << "Error creating call completion event, err=" << GetLastError());
	} else {
		LogDebug(m_logSubject << "CreateEvent returned 0x" << std::hex << std::setw(8) << std::setfill('0') << (int)m_callCompletionEvent);
	}
}

ServerScript::~ServerScript() {
	// go through deleting them first
	std::lock_guard<std::mutex> lock(m_stateMutex);
	for (ScriptServerEvent* se : m_eventQueue) {
		se->Release();
	}
	m_eventQueue.clear();

	// Deref and delete m_unblockEvent if ref is 0
	if (m_unblockEvent != nullptr) {
		m_unblockEvent->Release();
		m_unblockEvent = nullptr;
	}

	if (m_luaMain)
		lua_close(m_luaMain);

	if (m_stoppedEvent)
		CloseHandle(m_stoppedEvent);

	if (m_callCompletionEvent)
		CloseHandle(m_callCompletionEvent);
}

bool ServerScript::AddEvent(ScriptServerEvent* se, bool enableQueue) {
	ScriptServerEvent::EventType eventType = se->GetEventType();

	const HandlerInfo* handlerInfo = GetEventHandlerInfo(eventType);
	if (handlerInfo == nullptr) {
		assert(false); // Bad event type
		return false;
	}

	// Lock state mutex for the entire function
	std::lock_guard<std::mutex> lock(m_stateMutex);

	if (enableQueue) {
		// Enable event queue
		assert(m_state == ScriptState::STARTING);
		assert(m_eventQueue.empty());
		m_state = ScriptState::IDLE; // Set state directly as we have acquired the mutex
	}

	if (handlerInfo->bit != 0 && !HasHandlerFor(static_cast<ULONG>(handlerInfo->bit))) {
		// Script does not have Lua handler for this event
		return false;
	}

	// Make sure script is expecting event at current state
	if (eventType != ScriptServerEvent::UnloadScript && (m_state == ScriptState::STARTING || m_state == ScriptState::ZOMBIE)) {
		// Still initializing or defunct
		return false;
	}

	// Check if unblock event
	if (handlerInfo->bit == NULL_BIT) {
		if (m_unblockEvent == nullptr && (eventType == m_unblockEventType || eventType == ScriptServerEvent::GracefulAPITimeout)) {
			// Record "unblock" event
			m_unblockEvent = se;
			// Increment reference count
			se->AddRef();
			// Return true to tell scheduler to process this unblock event
			return true;
		} else {
			// Discard event
			if (m_unblockEventType == ScriptServerEvent::None) {
				// Not expecting an unblock event
				LogError("PID[" << GetPlacementId() << "] Received unexpected unblock event (type: " << eventType << ")");
			} else {
				// Expecting a different unblock event
				LogError("PID[" << GetPlacementId() << "] Received incorrect unblock event (type: " << eventType << "), expecting " << m_unblockEventType);
			}
			assert(false);
			return false;
		}
	}

	// Queue "call function" event

	// Drop certain events if script is stopping
	if (IsStopping()) {
		// Do not process any player-initiated events while a script is stopping
		if (eventType == ScriptServerEvent::PlayerArrived ||
			eventType == ScriptServerEvent::PlayerDeparted ||
			eventType == ScriptServerEvent::Timer ||
			eventType == ScriptServerEvent::Trigger ||
			eventType == ScriptServerEvent::Collide ||
			eventType == ScriptServerEvent::EndCollide ||
			eventType == ScriptServerEvent::Touch ||
			eventType == ScriptServerEvent::ObjectClick_Deprecated ||
			eventType == ScriptServerEvent::ObjectLeftClick ||
			eventType == ScriptServerEvent::ObjectRightClick ||
			eventType == ScriptServerEvent::PlayerClick ||
			eventType == ScriptServerEvent::PresetMenuAction_Deprecated ||
			eventType == ScriptServerEvent::MenuButton ||
			eventType == ScriptServerEvent::MenuClose ||
			eventType == ScriptServerEvent::MenuKeyDown ||
			eventType == ScriptServerEvent::MenuKeyUp ||
			eventType == ScriptServerEvent::MenuListSelect ||
			eventType == ScriptServerEvent::MenuShopBuy) {
			return false;
		}
	}

	// Increment reference count
	se->AddRef();
	// Add to event queue
	m_eventQueue.push_back(se);

	if (m_state == ScriptState::IDLE) {
		ScriptState oldState;
		SetStateNoLock(ScriptState::READY, oldState);
		// Return true to tell scheduler to process event for this script if script is not running
		return true;
	}

	if (m_state == ScriptState::ZOMBIE) {
		assert(eventType == ScriptServerEvent::UnloadScript);
		return true;
	}

	return false;
}

bool ServerScript::CheckReadiness(size_t& numArgs, bool& unblocked, bool& resumed) {
	ScriptServerEvent* se = nullptr;
	ScriptState oldState;

	numArgs = 0;
	unblocked = false;

	{
		std::lock_guard<std::mutex> lock(m_stateMutex);

		switch (m_state) {
			case ScriptState::READY:
			case ScriptState::ZOMBIE:
				// Process next event in queue
				assert(!m_eventQueue.empty());
				se = m_eventQueue.front();
				m_eventQueue.pop_front();
				break;
			case ScriptState::BLOCKED:
				// Process unblock event
				assert(m_unblockEvent != nullptr);
				se = m_unblockEvent;
				SetStateNoLock(ScriptState::READY, oldState);
				unblocked = true;
				break;
			case ScriptState::YIELDED:
				// Make ready
				SetStateNoLock(ScriptState::READY, oldState);
				resumed = true;
				return true;
			default:
				assert(false);
				return false;
		}
	}

	assert(GetState() == ScriptState::READY || GetState() == ScriptState::ZOMBIE && se->GetEventType() == ScriptServerEvent::UnloadScript);
	try {
		if (ProcessEvent(se, numArgs)) {
			// Succeeded
			return true;
		}

		if (GetState() == ScriptState::STOPPED) {
			// Unloaded
			return false;
		}
	} catch (jsBERException* e) {
		LogError("PID[" << GetPlacementId() << "] Exception occurred while processing script event (type: " << se->GetEventType() << ") - " << e->text());
		delete e;
	} catch (...) {
		LogError("PID[" << GetPlacementId() << "] Exception occurred while processing script event (type: " << se->GetEventType() << ") - OTHER");
	}

	// Failed
	assert(false);

	// Change state to IDLE for future events
	SetStateNoLock(ScriptState::IDLE, oldState);
	return false;
}

bool ServerScript::ProcessEvent(ScriptServerEvent* se, size_t& numArgs) {
	assert(se != nullptr);
	numArgs = 0;

	ScriptServerEvent::EventType eventType = se->GetEventType();

	// Decode event if not already
	if (!se->isDecoded()) {
		// Extract standard parameter and cache it for future lookup
		ULONG objPlacementId;
		NETID clientNetId;
		int zoneId;
		int instanceId;
		se->ExtractInfo(eventType, objPlacementId, zoneId, instanceId, clientNetId);
	}

	const HandlerInfo* handlerInfo = GetEventHandlerInfo(se->GetEventType());
	assert(handlerInfo);
	int rc = handlerInfo->func(this, se);

	se->Release();
	se = nullptr;

	if (rc >= 0) {
		numArgs = rc;
		return true;
	} else if (rc == PCR_SKIPPED) {
		// No handler
		return false;
	} else {
		// Error
		assert(rc == PCR_ERROR);
		LogError("PID[" << GetPlacementId() << "] Error processing script event (type: " << eventType << ")");
		assert(false);
		return false;
	}
}

void ServerScript::SetError(int errorCode, const std::string& errorMsg, bool setZombie) {
	LOG4CPLUS_ERROR(script_logger, "[" << GetZoneId().GetInstanceId() << "] PID[" << GetPlacementId() << "] " << errorMsg << " [err=" << errorCode << "]");

	std::lock_guard<std::mutex> lock(m_stateMutex);
	if (setZombie) {
		assert(m_state == ScriptState::RUNNING);
		m_state = ScriptState::ZOMBIE;

		// Remove all events except UnloadScript
		for (auto itr = m_eventQueue.begin(); itr != m_eventQueue.end();) {
			ScriptServerEvent* se = *itr;
			if (se->GetEventType() != ScriptServerEvent::UnloadScript) {
				itr = m_eventQueue.erase(itr);
				se->Release();
			} else {
				++itr;
			}
		}
	}

	m_errorMsg = errorMsg;
	m_errorCode = errorCode;
}

void ServerScript::GetError(int& errorCode, std::string& msg) {
	std::lock_guard<std::mutex> lock(m_stateMutex);
	errorCode = m_errorCode;
	msg = m_errorMsg;
}

bool ServerScript::SetStateNoLock(ScriptState state, ScriptServerEvent::EventType unblockEventType, const std::string& reason, ScriptState& oldState) {
	if (state == m_state || m_state == ScriptState::STOPPED || m_state == ScriptState::ZOMBIE && state != ScriptState::STOPPED) {
		assert(false);
		return false;
	}

	oldState = m_state;
	m_state = state;

	// update the previous state with the time since the last state change
	m_times[static_cast<size_t>(oldState)] += m_timerStateChange.ElapsedMs();
	m_timerStateChange.Reset();

	switch (state) {
		case ScriptState::BLOCKED:
			m_blockedAt = fTime::TimeMs();
			m_blockingReason = reason;
			m_unblockEventType = unblockEventType;
			assert(m_unblockEvent == nullptr);
			m_unblockEvent = nullptr;
			break;
		case ScriptState::READY:
			m_blockedAt = 0;
			m_blockingReason.clear();
			m_unblockEventType = ScriptServerEvent::None;
			m_unblockEvent = nullptr;
			break;
		case ScriptState::STOPPED:
			// Signal whoever is wait for the script to stop
			SetEvent(m_stoppedEvent);
			break;
	}

	return true;
}

void ServerScript::Unload() {
	LogInfo(LogSubject() << "Stopped after " << GetElapsedStoppingTime() << "ms");

	ScriptZonePtr pZone = GetScriptZone();

	ServerScriptPtr pRemovedScript = pZone->removeScript(GetPlacementId());
	assert(pRemovedScript.get() == this);
	if (!pRemovedScript) {
		assert(false);
		LogWarn(GetZoneId().ToStr() << " PID[" << GetPlacementId() << "] Script not found");
	}

	// Set state to stopped
	ScriptState oldState;
	SetState(ScriptState::STOPPED, oldState);
}

void ServerScript::CachePlayerInfoInLuaRegistry(lua_State* L, NETID netID, const ScriptPlayerInfo& playerInfo) {
	// Save departing player information in the registry to allow player query API such
	// as playerGetName to work when called by Lua code from kgp_depart handler (during
	// which script server has disposed the departed player data.

	lua_newtable(L); // Create temp table
	lua_pushnumber(L, netID);
	lua_setfield(L, -2, KGPHANDLER_TEMPDATA_CLIENTID); // temp.CLIENTID = e->From()
	lua_pushstring(L, playerInfo.m_name.c_str());
	lua_setfield(L, -2, KGPHANDLER_TEMPDATA_PLAYERNAME); // temp.PLAYERNAME = playerName
	lua_pushnumber(L, playerInfo.m_playerId);
	lua_setfield(L, -2, KGPHANDLER_TEMPDATA_PLAYERID); // temp.PLAYERID = playerId
	lua_pushstring(L, playerInfo.m_title.c_str());
	lua_setfield(L, -2, KGPHANDLER_TEMPDATA_PLAYERTITLE); // temp.PLAYERTITLE = playerTitle

	char szGender[2];
	szGender[0] = playerInfo.m_gender;
	szGender[1] = 0;
	lua_pushstring(L, szGender);
	lua_setfield(L, -2, KGPHANDLER_TEMPDATA_PLAYERGENDER); // temp.PLAYERGENDER = playerGender

	lua_pushnumber(L, playerInfo.m_permissions);
	lua_setfield(L, -2, KGPHANDLER_TEMPDATA_PLAYERPERM); // temp.PLAYERPERM = playerPermissions
	lua_pushvalue(L, -2); // repush posrot table
	lua_setfield(L, -2, KGPHANDLER_TEMPDATA_POSITION); // temp.POSITION = position

	lua_setfield(L, LUA_REGISTRYINDEX, REGISTRY_KGPHANDLER_TEMPDATA); // Registry.kgpHandlerTempData = temp
}

bool ServerScript::GetBlockingInfo(TimeMs& duration, std::string& reason) {
	std::lock_guard<std::mutex> lock(m_stateMutex);
	if (m_state != ScriptState::BLOCKED) {
		return false;
	}
	duration = fTime::ElapsedMs(m_blockedAt);
	reason = m_blockingReason;
	return true;
}

int ServerScript::LuaYield(ScriptServerEvent::EventType unblockEventType, const std::string& reason, IEvent* e, ScriptServerTaskObject* task /*= NULL*/) {
	assert(e && !task || !e && task); // Only one

	// Guaranteed single-thread access (RunVMTask thread)
	m_yieldInfo.block = true;
	m_yieldInfo.unblockEventType = unblockEventType;
	m_yieldInfo.reason = reason;
	m_yieldInfo.eventToSend = e;
	m_yieldInfo.taskToQueue = task;

	lua_State* NL = LuaThreadState();
	lua_pushinteger(NL, YLD_BLOCKED); // Blocking API call: Leave yield type in the stack as return value
	return lua_yield(NL, 1);
}

bool ServerScript::RetrieveYieldInfo(ScriptServerEvent::EventType& unblockEventType, std::string& reason, IEvent*& eventToSend, ScriptServerTaskObject*& taskToQueue) {
	// Guaranteed single-thread access (mostly RunVMTask thread when script yielded, occasionally called by engine main thread during script initialization)
	bool block = m_yieldInfo.block;
	if (block) {
		unblockEventType = m_yieldInfo.unblockEventType;
		reason = m_yieldInfo.reason;
		eventToSend = m_yieldInfo.eventToSend;
		taskToQueue = m_yieldInfo.taskToQueue;
	}

	m_yieldInfo.clear();
	return block;
}

const char* ServerScript::GetStateName(ScriptState state) {
	switch (state) {
		case ScriptState::STARTING:
			return "STARTING";
		case ScriptState::IDLE:
			return "IDLE";
		case ScriptState::READY:
			return "READY";
		case ScriptState::SCHEDULED:
			return "SCHEDULED";
		case ScriptState::RUNNING:
			return "RUNNING";
		case ScriptState::BLOCKED:
			return "BLOCKED";
		case ScriptState::YIELDED:
			return "YIELDED";
		case ScriptState::ZOMBIE:
			return "ZOMBIE";
		case ScriptState::STOPPED:
			return "STOPPED";
	}

	return "UNKNOWN";
}

bool ServerScript::QueueReloadRequest(ULONG clientNetId, const std::string& fileName, TimeMs timeout) {
	// Guaranteed single-thread access (engine main thread)
	if (!m_stopping) {
		LogWarn(m_logSubject << "Script is still active. Call ShutdownScript first."); // for bug detection
		return false;
	}

	if (m_pendingReloadRequest.timestamp != 0) {
		LogWarn(m_logSubject << "Script already has a pending reload request."); // for bug detection
		return false;
	}

	m_pendingReloadRequest.timestamp = fTime::TimeMs();
	m_pendingReloadRequest.ttl = timeout;
	m_pendingReloadRequest.clientNetId = clientNetId;
	m_pendingReloadRequest.fileName = fileName;
	return true;
}

bool ServerScript::FetchPendingReloadRequest(ULONG& clientNetId, std::string& fileName) {
	// Guaranteed single-thread access (engine main thread)
	bool res = false;
	if (m_pendingReloadRequest.timestamp != 0 && fTime::ElapsedMs(m_pendingReloadRequest.timestamp) <= m_pendingReloadRequest.ttl) {
		res = true;
		clientNetId = m_pendingReloadRequest.clientNetId;
		fileName = m_pendingReloadRequest.fileName;
	}

	// Clear pending request
	m_pendingReloadRequest.clear();
	return res;
}

int ServerScript::LuaResume(int numberOfArgs, std::string& errMsg) {
	lua_State* NL = LuaThreadState();
	int rc = lua_resume(
		NL,
#if LUA_VERSION_NUM == 502
		NULL,
#endif
		numberOfArgs);

	if (rc != 0 && rc != LUA_YIELD) {
		ASSERT(lua_isstring(NL, -1));
		const char* szErrMsg = lua_tostring(NL, -1);
		errMsg = szErrMsg ? szErrMsg : "(NULL)";
	}

	return rc;
}

void ServerScript::LuaStackDump() {
	lua_State* NL = LuaThreadState();

	int top = lua_gettop(NL);
	std::ostringstream output;

	output << LogSubject() << std::endl
		   << "--- Lua Stack Dump ---";
	for (int i = 1; i <= top; i++) {
		/* repeat for each level */
		int t = lua_type(NL, i);
		output << "\n";
		switch (t) {
			case LUA_TNIL:
				output << "nil";
				break;

			case LUA_TBOOLEAN:
				output << lua_toboolean(NL, i) ? "true" : "false";
				break;

			case LUA_TNUMBER:
				lua_pushvalue(NL, i); // Push the value to the top of the stack
				output << lua_tostring(NL, -1); // then convert it to string (lua will do in-place conversion that's why we use a new stack entry)
				lua_pop(NL, 1); // Remove the temporary value
				break;

			case LUA_TSTRING:
				output << "\"" << lua_tostring(NL, i) << "\"";
				break;

			case LUA_TTABLE:
			case LUA_TLIGHTUSERDATA:
			case LUA_TUSERDATA:
			case LUA_TFUNCTION:
				output << lua_typename(NL, t) << ": 0x" << std::hex << std::setw(8) << std::setfill('0') << (DWORD)lua_topointer(NL, i);
				break;

			case LUA_TTHREAD:
				output << "thread";
				break;

			default: /* other values */
				output << "other: " << lua_typename(NL, t);
				break;
		}
	}

	LogTrace(output.str());
}

void ServerScript::LuaGetSourceInfo(int stackLevel, std::string& source, int& lineNumber) const {
	lua_State* NL = LuaThreadState();

	source.clear();
	lineNumber = -1;

	lua_Debug ar;
	int ret = lua_getstack(NL, stackLevel, &ar);

	if (ret == 1) {
		int ret2 = lua_getinfo(NL, "Snl", &ar);
		if (ret2 != 0) {
			lineNumber = ar.currentline;
			source = ar.short_src;

			// Strip path information
			size_t slashPos = source.rfind('\\');
			if (slashPos == std::string::npos) {
				slashPos = source.rfind('/');
			}
			if (slashPos != std::string::npos) {
				source = source.substr(slashPos + 1);
			}
		} else {
			LogError(m_logSubject << "lua_getinfo returned " << ret2);
		}
	} else {
		LogError(m_logSubject << "lua_getstack(" << stackLevel << ") returned " << ret);
	}
}

void ServerScript::EnableLuaMemoryTracking(lua_State* L) {
	// Give Lua a pointer to the SS that it will pass to the allocator, so the
	// allocator can start keeping track of memory use.  It should be safe to
	// give lua a pointer to the actual SS, since the SS manages the life cycle
	// of the lua.
	lua_setallocf(L, ScriptMemoryAllocator, this);
}

void ServerScript::LuaVMInit(const char* fileName, const ScriptAPIRegistry* scriptAPIs) {
	jsVerifyReturnVoid(scriptAPIs != NULL);
	jsVerifyReturnVoid(LuaGlobalState() == NULL && LuaThreadState() == NULL);

	// Create lua VM global state
	lua_State* L = lua_newstate(ScriptMemoryAllocator, NULL);
	if (L == NULL) {
		// Critical error
		ASSERT(FALSE);
		LogError(m_logSubject << "Error creating new Lua global state.");
		return;
	}

	m_luaMain = L;

	// Begin VM initialization
	lua_gc(L, LUA_GCSETPAUSE, m_luaVMConfig.getLuaGCPause());
	lua_gc(L, LUA_GCSETSTEPMUL, m_luaVMConfig.getLuaGCStep());

	// Define global variable __SCRIPT__ for current script file name
	lua_pushstring(L, fileName ? fileName : "N/A");
	lua_setglobal(L, "__SCRIPT__");

	// Define global variable __LUA_COMPAT_LEVEL__
	lua_pushnumber(L, CURRENT_SCRIPT_LUA_COMPAT_LEVEL);
	lua_setglobal(L, "__LUA_COMPAT_LEVEL__");

	// Define global variable __LUA_VERSION__
	lua_pushnumber(L, LUA_VERSION_NUM);
	lua_setglobal(L, "__LUA_VERSION__");

	// Store zone index in Lua registry for fast lookup without going through a global dictionary
	StoreZoneIndexInRegistry(L, GetZoneId());

	if (m_luaVMConfig.isLuaProtected()) {
		LUA_OPENLIBS(L, true);

		// We need to specifically remove some of the functions that we don't
		// want users to have from the base library, like dofile, load, collectgarbage, print etc
		// but allow functions like tonumber, ipairs, next, select.
		// This can be done by setting the value of the global names we want to disable to nil,
		// after opening the library.
		lua_pushnil(L);
		lua_setglobal(L, "dofile");
		// Re-enabling the loadfile function
		//lua_pushnil(L);
		//lua_setglobal(L, "loadfile");
		lua_pushnil(L);
		lua_setglobal(L, "print");

		// coroutine library is now disabled by the consumer script running on the worker thread
		// see LuaRunScript()
	} else {
		LUA_OPENLIBS(L, false);
	}

	//
	// Our API (from script to C)
	//
	scriptAPIs->RegisterAPIsOnLuaVM(L);

	// Register REST API IDs in a global table
	PopulateRestAPIDataForLua(L);

	if (!m_luaVMConfig.isStoreProcAccessEnabled()) {
		lua_getglobal(L, SCRIPT_API_NAMESPACE);
		lua_pushnil(L);
		lua_setfield(L, -2, "scriptCallStoredProcedure");
		lua_pushnil(L);
		lua_setfield(L, -2, "scriptcallstoredprocedure");
		lua_pushnil(L);
		lua_setfield(L, -2, "ScriptCallStoredProcedure");

		lua_pushnil(L);
		lua_setfield(L, -2, "scriptUnRegisterStoredProcedure");
		lua_pushnil(L);
		lua_setfield(L, -2, "scriptunregisterstoredprocedure");
		lua_pushnil(L);
		lua_setfield(L, -2, "ScriptUnRegisterStoredProcedure");

		lua_pushnil(L);
		lua_setfield(L, -2, "scriptRegisterStoredProcedure");
		lua_pushnil(L);
		lua_setfield(L, -2, "scriptregisterstoredprocedure");
		lua_pushnil(L);
		lua_setfield(L, -2, "ScriptRegisterStoredProcedure");

		lua_pop(L, 1);
	}

	// Create a coroutine for script code executions - required by lua-coco for full yielding support
	lua_State* NL = m_luaVMConfig.useCocoThread() ? lua_newcthread(L, 0) : lua_newthread(L);
	if (NL == NULL) {
		// Critical error
		ASSERT(FALSE);
		LogError(m_logSubject << "Error creating new Lua thread state.");
	}

	// We are going to keep this thread live as long as the main thread
	m_luaThread = NL;

	// Start tracking memory usage
	EnableLuaMemoryTracking(NL);
}

int ServerScript::LuaLoadScript(bool loadJson, const std::string& sourceCode, std::string& errMsg) {
	lua_State* L = LuaGlobalState(); // Load script into global state

	errMsg.clear();

	// process common libraries from GameFiles/ScriptServerScripts
	if (!m_luaVMConfig.getLuaCommonLibrary().empty()) {
		int res;
		std::string libCommon = PathAdd(m_luaVMConfig.getScriptRootDir(), m_luaVMConfig.getLuaCommonLibrary());
		res = luaL_loadfile(L, libCommon.c_str());
		if (res == 0) {
			res = lua_pcall(L, 0, LUA_MULTRET, 0);
			if (res != 0) {
				const char* szErrMsg = lua_tostring(L, -1);
				LogError(m_logSubject << "Error processing [" << libCommon << "], res = " << res << ", err = " << szErrMsg ? szErrMsg : "NULL");
			}
		} else {
			const char* szErrMsg = lua_tostring(L, -1);
			LogError(m_logSubject << "Error loading [" << libCommon << "], res = " << res << ", err = " << szErrMsg ? szErrMsg : "NULL");
		}
	}

	// load the script
	int rc;
	if (loadJson) {
		// Send JSON to VM
		lua_pushstring(L, sourceCode.c_str());
		lua_setglobal(L, "__JSON__");

		// Load action item through a proxy script
		const char* jsonLoader =
			"-- Action Item Parameter Loader v1.0 (c) Kaneva, LLC 2014\r\n" // Some dummy comments to prevent actual code being revealed in dev console.
			"local paramTable = cjson.decode(__JSON__)\r\n"
			"if paramTable==nil then\r\n"
			"    error(\"Error processing parameter file - JSON parsing failed\")\r\n"
			"end\r\n"
			// TODO: use a standard way to obtain script name by GLID (see also inventory.lua)
			"if type(paramTable.script)==\"number\" then\r\n"
			"	paramTable.script = string.format(\"%010d\", paramTable.script)\r\n"
			"end\r\n"
			"if type(paramTable.script)~=\"string\" then\r\n"
			"	error(\"Error processing parameter table - script not specified\")\r\n"
			"end\r\n"
			"if string.match(paramTable.script, \"[\\\\/:*?\\\"<>|]\") or string.len(paramTable.script)>80 then\r\n"
			"	error(\"Error processing parameter table - invalid script [\" .. tostring(paramTable.script) .. \"]\")\r\n"
			"end\r\n"
			"AIDOverride = paramTable\r\n"
			"kgp.scriptDoFile(\"ActionItems\\\\\" .. paramTable.script .. \".lua\")\r\n";

		rc = luaL_loadstring(L, jsonLoader);
	} else {
		// Load LUA script directly
		rc = luaL_loadstring(L, sourceCode.c_str());
	}

	if (rc != 0) {
		const char* szErrMsg = lua_tostring(L, -1);
		ASSERT(szErrMsg != NULL);
		errMsg = szErrMsg ? szErrMsg : "NULL";
	}
	return rc;
}

int ServerScript::LuaRunScript(std::string& errMsg) {
	errMsg.clear();

	//
	// The coroutine is running a infinite loop to prevent lua coco from disposing
	// Win32 Fiber context it uses for full yielding support. The scenario between
	// the main thread (global state) and the coroutine is shown below:
	//
	//          [MAIN THREAD]                                         [COROUTINE]
	// =========================================================================================
	// Init globals
	// Load script
	// Create coroutine                                         load thread main loop
	// ........................................................................................
	// lua_resume#1() suspended              =====>>            enter thread loop
	// lua_resume#1() returned               <<=====            coroutine.yield#1()
	// ........................................................................................
	// Push script body to main stack
	// xmove                                                    script body in coroutine stack
	// lua_resume#2() suspended              =====>>            coroutine.yield#1() returned
	//                                                          run script
	//                                                          script body completed
	//                                                          loop
	// lua_resume#2() returned (COMPETED)    <<=====            coroutine.yield#2() suspended
	// ........................................................................................
	// Push event handler and args to main stack
	// RunVMTask - call handler
	// xmove                                                    event handler and args in coroutine stack
	// lua_resume#3() suspended              =====>>            coroutine.yield#2() returned
	//                                                          run event handler
	// lua_resume#3() returned (PREMPTED)    <<=====            lua_yield(0) (prempted by count hook)
	// ........................................................................................
	// RunVMTask - continued
	// lua_resume#4() suspended              =====>>            resume event handler
	// lua_resume#4() returned (BLOCKED)     <<=====            lua_yield(1) (blocked by API)
	// ........................................................................................
	// push API return values to main stack
	// RunVMTask - resume API
	// xmove                                                    API return values in coroutine stack
	// lua_resume#5() suspended              =====>>            resume event handler
	//                                                          event handler completed
	//                                                          loop
	// lua_resume#5() returned (COMPLETED)   <<=====            coroutine.lua_yield#3()
	// ........................................................................................
	// RunVMTask - completed
	//

	// Script body function should be the only value in the global stack
	lua_State* L = LuaGlobalState();
	lua_State* NL = LuaThreadState();
	jsVerifyReturn(L != NULL && lua_type(L, -1) == LUA_TFUNCTION && NL != NULL, LUA_ERRERR);

	int rc = 0;

	if (m_luaVMConfig.useConsumerLoop()) {
		// Load consumer script on the worker thread
		const char* consumerLoop = // *** Worker Thread ***
			"local yield = coroutine.yield	\r\n" // Save coroutine.yield locally
			"coroutine = nil				\r\n" // Disable coroutine library: we don't want game developers creating coroutines and messing up the scheduling mechanisms in the ScriptServer.
			"while true do					\r\n" // === MAIN LOOP BEGINS ===
			"	local args = { yield(0) }	\r\n" // Call yield(YLD_COMPLETED) to tell engine that thread is IDLE.
			"	local f = args[1]			\r\n" // A new call task received from engine { f, arg1, arg2, ... }
			"	if f==nil then				\r\n"
			"		break					\r\n"
			"	end							\r\n"
			"	table.remove(args, 1)		\r\n"
			"	f(unpack(args))				\r\n" // Execute call task
			"end"; // === NEXT ITERATION ===

		// Use a consumer loop to keep worker thread alive - otherwise coco will dispose Fiber context once the script terminates
		rc = luaL_loadstring(NL, consumerLoop);
		if (rc != 0) {
			// Critical error
			ASSERT(FALSE);
			errMsg = "Error loading wrapper script on Lua thread.";
			return LUA_ERRERR;
		}

		// Run worker thread until it hits first yield.
		rc = LuaCall(0, false, errMsg); // Pass false because consumer loop is not yet started
		jsVerifyReturn(rc == 0, rc);
	} else {
		// Otherwise simply disable coroutine library
		lua_pushnil(L);
		lua_setglobal(L, "coroutine");
	}

	// Wake up the worker thread to process the script body
	// From stack top: [-1] main chunk, [-2] coroutine
	lua_xmove(L, NL, 1); // Move main chunk to the stack of the new thread

	// LuaCall is a wrapper function that calls lua_resume
	rc = LuaCall(0, true, errMsg);
	if (rc == LUA_YIELD) {
		// NOTE: Current mechanism does not handle the case when the script calls a asynchronous kgp API during the loading phase.
		// At this moment, I would catch it and set the VM to zombie state.
		ScriptServerEvent::EventType unblockEventType;
		std::string reason;
		IEvent* eventToSend = NULL;
		ScriptServerTaskObject* taskToQueue = NULL;
		if (RetrieveYieldInfo(unblockEventType, reason, eventToSend, taskToQueue)) {
			errMsg = "Calling a blocking API during script loading phase is not supported. Block reason: " + reason;
		} else {
			errMsg = "Yielding during script loading phase is not supported.";
		}

		return LUA_ERRERR;
	}

	jsVerifyReturn(rc == 0, rc);

	//
	// Go and find all the event handler functions defined
	// by the script and fill in the event handler function
	// bitfield that I can use to early-terminate event
	// handling for scripts that don't care about it.
	//
	// for each handler in the list of handlers
	//  lua_getglobal(L, "the handler's name");
	//  if(lua_isfunction(L, -1)) { update the defined handler bit field }
	//  lua_pop(L, 1) // remove "the handler's name" from the stack
	// end

	for (const auto& pr : s_eventHandlerInfoMap) {
		const HandlerInfo& handlerInfo = pr.second;
		if (handlerInfo.name == nullptr) {
			// Some handlers do not call Lua function: e.g. UnloadScript
			continue;
		}

		lua_getglobal(L, handlerInfo.name);
		// what if the name isn't found?
		// is anything pushed onto the stack?
		if (lua_isfunction(L, -1)) {
			m_handlerBitField |= handlerInfo.bit;
			lua_pop(L, 1);
		}
	}

	// Set count hook on the thread
	EnableCountHook();
	return 0;
}

// Run a C function in the global state to complete the pending API. The C function is expected to schedule a thread to resume the script in the coroutine.
int ServerScript::LuaResumeBlockingAPI(std::function<void(lua_State*)> func) {
	// Resuming API from the global state

	// A new thread is expected to be scheduled to resume the suspended callback coroutine that yielded on the API.
	// This is supposedly done in the function passed in.
#if 1
	if (GetState() != ScriptState::READY) {
		ASSERT(FALSE);
		LogError(m_logSubject << ": expecting script to be READY, actual: " << GetStateName(GetState()));
		return PCR_ERROR;
	}
#else
	ASSERT(GetState() == ServerScript::IDLE || GetState() == ServerScript::BLOCKED);
#endif

	lua_State* NL = LuaThreadState();
	if (NL == NULL) {
		ASSERT(FALSE);
		LogError(m_logSubject << "Lua global context does not exist.");
		return PCR_ERROR;
	}

	int n = lua_gettop(NL);
	func(NL);
	int numArgsOnStack = lua_gettop(NL) - n;
	assert(numArgsOnStack >= 0);

	return numArgsOnStack >= 0 ? numArgsOnStack : 0;
}

// Run a in-script event handler function that is already on the stack. Return true if a handler is found and scheduled to run.
int ServerScript::LuaPreCall(const char* glueName, std::function<void(lua_State*)> func) {
	jsVerifyReturn(glueName != NULL, -2);

	// Only READY script can call handler.
	if (GetState() != ScriptState::READY) {
		ASSERT(FALSE);
		LogError(m_logSubject << "\"" << glueName << "\": expecting script to be READY, actual: " << GetStateName(GetState()));
		return PCR_ERROR;
	}

	lua_State* NL = LuaThreadState();
	lua_getglobal(NL, glueName);
	if (lua_isfunction(NL, -1)) {
		// Handler on the stack

		// Record current event name
		SetCurrentGlueName(glueName);
		LogDebug(m_logSubject << glueName << " - precall");

		int n = lua_gettop(NL);

		// Now call back to push parameters
		func(NL);

		int numArgsOnStack = lua_gettop(NL) - n;
		assert(numArgsOnStack >= 0);

		return numArgsOnStack >= 0 ? numArgsOnStack : 0;
	} else {
		// Notify call completion (script has no handler)
		::SetEvent(m_callCompletionEvent);
	}

	// Not called
	return PCR_SKIPPED;
}

int ServerScript::LuaCall(int numberOfArgs, bool callFunction, std::string& errMsg) {
	lua_State* NL = LuaThreadState(); // thread
	if (m_luaVMConfig.useConsumerLoop() && callFunction) {
		// If consumer loop is used and we are calling a function, make the function itself a parameter
		// to allow consumer loop to pick up function and arguments together. See consumerLoop script.
		numberOfArgs++;
	}

	int rc = LuaResume(numberOfArgs, errMsg);

	if (!m_luaVMConfig.useConsumerLoop()) {
		// Single-threaded Lua VM - simply forward the return value from lua_resume()
		return rc;
	}

	// lua_resume returned from consumerLoop coroutine. ConsumerLoop coroutine runs a infinite loop and
	// does not terminate by itself. It will yield every time an event handler call is completed.
	switch (rc) {
		case 0: // terminated
			// The thread is supposed to do infinite loop. Something must be wrong if it terminates
			ASSERT(FALSE);
			errMsg = "Thread loop terminated unexpectedly";
			return LUA_ERRERR;

		case LUA_YIELD: // yielded
			// Causes:
			// 1) yielded explicitly by consumer loop after completion of a handler call (YLD_COMPLETE);
			// 2) yielded by a blocking API (YLD_BLOCKED);
			// 3) preempted by counthook (YLD_PREMPTED)

			// Determine if yield is called by the thread main loop which is actually the completion of the event handler
			if (lua_gettop(NL) == 1 && lua_isnumber(NL, -1) && lua_tonumber(NL, -1) == YLD_COMPLETED) {
				// Completion of event handler call
				// Tell caller that we are done
				return 0;
			} else {
				// Tell caller that VM has yielded
				return LUA_YIELD;
			}

		default: // erred
			return rc; // Forward the error to the caller
	}
}

bool ServerScript::LuaCallTaskCompleted() {
	SetCurrentGlueName("");

	// Unlink task from script
	SetCurrentTask(nullptr);

	// Cleanup temporary data from registry
	lua_State* NL = LuaThreadState();
	assert(NL != nullptr);
	if (NL != nullptr) {
		lua_pushnil(NL);
		lua_setfield(NL, LUA_REGISTRYINDEX, REGISTRY_KGPHANDLER_TEMPDATA); // LuaRegistry.kgpHandlerTempData = nil
	}

	// Signal event to wake up all waiters
	::SetEvent(m_callCompletionEvent);

	// Update script state based on event queue
	std::lock_guard<std::mutex> lock(m_stateMutex);
	if (m_state != ScriptState::ZOMBIE) {
		assert(m_state == ScriptState::RUNNING);
		ScriptState oldState;
		SetStateNoLock(m_eventQueue.empty() ? ScriptState::IDLE : ScriptState::READY, oldState);
	}
	return !m_eventQueue.empty(); // Notify scheduler if event queue is not empty
}

//Construct global lua table '__apireg'.
void ServerScript::PopulateRestAPIDataForLua(lua_State* L) {
	// local masterTable = {}
	lua_newtable(L);

	const LuaVMConfig::RestAPIDataMap& dataMap = m_luaVMConfig.getRestAPIDataMapRef();
	for (auto itGroups = dataMap.cbegin(); itGroups != dataMap.cend(); ++itGroups) {
		const std::string& group = itGroups->first;
		const std::map<std::string, std::string>& keyValuePairs = itGroups->second;

		// local table = {}
		lua_newtable(L);

		for (auto it = keyValuePairs.cbegin(); it != keyValuePairs.cend(); ++it) {
			const std::string& name = it->first;
			const std::string& value = it->second;

			// table[name] = value
			lua_pushstring(L, value.c_str());
			lua_setfield(L, -2, name.c_str());
		}

		// masterTable[group] = table
		lua_setfield(L, -2, group.c_str());
	}

	// __apireg = masterTable
	lua_setglobal(L, m_luaVMConfig.getRestAPIDataTableName().c_str());
}

bool ServerScript::WaitForCallCompletion(TimeMs timeout) {
	Timer timer;
	DWORD waitRc = ::WaitForSingleObject(m_callCompletionEvent, (DWORD)timeout);
	TimeMs elapsed = timer.ElapsedMs();

	bool res = false;

	switch (waitRc) {
		case WAIT_OBJECT_0:
			// Succeeded - do nothing
			LogInfo(m_logSubject << "kgp_start call COMPLETED, elapsed: " << FMT_TIME << elapsed << " ms");
			res = true;
			break;
		case WAIT_TIMEOUT:
			// kgp_start is taking too long to complete
			LogWarn(m_logSubject << "kgp_start call -TIMEOUT-, elapsed: " << FMT_TIME << elapsed << " ms");
			break;
		default:
			// Other errors
			LogError(m_logSubject << "WaitForSingleObject failed, rc=" << waitRc << ", err=" << GetLastError());
			break;
	}

	return res;
}

void ServerScript::DisableCountHook() {
	lua_sethook(LuaThreadState(), NULL, 0, 0);
}

void ServerScript::EnableCountHook() {
	lua_sethook(LuaThreadState(), CountHook, LUA_MASKCOUNT, m_luaVMConfig.getYieldAfter());
}

void ServerScript::SetCurrentGlueName(const std::string& glueName) {
	m_currGlueName = glueName;
	if (glueName.empty()) {
		StrBuild(m_logSig, " [" << GetFileName() << ":<IDLE>]");
	} else {
		StrBuild(m_logSig, " [" << GetFileName() << ":" << GetCurrentGlueName() << "]");
	}
}

static void* ScriptMemoryAllocator(void* ud, void* ptr, size_t old_size, size_t new_size) {
	bool allocate = true;

	if (ud) {
		// Should be safe to use a bald pointer in here and it makes
		// life much easier.
		ServerScript* ss = reinterpret_cast<ServerScript*>(ud);
		ASSERT(ss);

		if (ss) {
			allocate = ss->TrackLuaMemoryAllocation(old_size, new_size);
		}
	}

	if (new_size == 0) {
		free(ptr);
		return NULL;
	} else {
		if (allocate)
			return realloc(ptr, new_size);
		else
			return NULL;
	}
}

//
// A LUA count hook.
//
void CountHook(lua_State* L, lua_Debug* /*ar*/) {
	lua_yield(L, 0); // Preempted by the engine: as this can happen very often, use 0 return values to indicate YLD_PREMPTED
}

void ServerScript::StoreZoneIndexInRegistry(lua_State* L, const ZoneIndex& zoneIndex) {
	lua_pushinteger(L, zoneIndex.toLong());
	lua_rawseti(L, LUA_REGISTRYINDEX, REGISTRY_ZONEINDEX);
}

ZoneIndex ServerScript::GetZoneIndexFromRegistry(lua_State* L) {
	lua_rawgeti(L, LUA_REGISTRYINDEX, REGISTRY_ZONEINDEX);
	int zoneIndexInt = lua_tointeger(L, -1);
	lua_pop(L, 1);
	return ZoneIndex(zoneIndexInt);
}

ZoneIndex ServerScript::GetZoneId() const {
	return m_pZone->getZoneIndex();
}

///////////////////////////////////////////////////////////////
// Helpers for script event handlers
struct EventHandler_Base {
	static constexpr bool Specialized = true;
	static constexpr const char* HandlerName = nullptr;

	template <typename... LuaArgs>
	__forceinline static int LuaPreCallWithArgs(ServerScript* pScript, const char* handlerName, const LuaArgs&... args) {
		return pScript->LuaPreCall(handlerName, [&](lua_State* L) {
			ScriptArgManager sam(L);
			PushVarArgs(&sam, args...);
		});
	}

	template <typename... LuaArgs>
	__forceinline static int LuaResumeWithArgs(ServerScript* pScript, const LuaArgs&... args) {
		return pScript->LuaResumeBlockingAPI([&](lua_State* L) {
			ScriptArgManager sam(L);
			PushVarArgs(&sam, args...);
		});
	}
};

// Base class for all unblock handlers
struct EventHandler_UnblockBase : EventHandler_Base {
	static constexpr ServerScript::ScriptEventBit HandlerBit = ServerScript::NULL_BIT;
};

// Standard "call function" handler
template <typename TEH, typename... Args>
struct EventHandler_CallFunction : EventHandler_Base {
	static int Handle(ServerScript* pScript, const Args... args) {
		assert(TEH::HandlerName != nullptr);
		if (TEH::HandlerName == nullptr) {
			return -1;
		}
		return LuaPreCallWithArgs(pScript, TEH::HandlerName, args...);
	}
};

// Standard "unblock" handler
template <typename... Args>
struct EventHandler_Unblock : EventHandler_UnblockBase {
	static int Handle(ServerScript* pScript, Args... args) {
		return LuaResumeWithArgs(pScript, args...);
	}
};

// PresetMenuAction: clientNetId, menuName, data1, data2, identifier
template <typename TEH>
struct EventHandler_CallMenuActionFunc : EventHandler_CallFunction<TEH, ScriptNETIDType, std::string, std::string, std::string, std::string> {};

// Helper macros
#define SPECIAL_CALL_FUNCTION_HANDLER(eventType, handlerName, handlerBit, baseClass, ...)                            \
	template <>                                                                                                      \
	struct ServerScript::TEventHandler<eventType> : baseClass<ServerScript::TEventHandler<eventType>, __VA_ARGS__> { \
		static constexpr const char* HandlerName = handlerName;                                                      \
		static constexpr ServerScript::ScriptEventBit HandlerBit = handlerBit;                                       \
	}

#define STANDARD_CALL_FUNCTION_HANDLER(eventType, handlerName, handlerBit, ...) SPECIAL_CALL_FUNCTION_HANDLER(eventType, handlerName, handlerBit, EventHandler_CallFunction, __VA_ARGS__)

///////////////////////////////////////////////////////////////
// Script event handlers - call a Lua handler

STANDARD_CALL_FUNCTION_HANDLER(ScriptServerEvent::Timer, TIMER_HANDLER, ServerScript::TIMER_BIT, ScriptTimestamp); // Timestamp
STANDARD_CALL_FUNCTION_HANDLER(ScriptServerEvent::Start, START_HANDLER, ServerScript::START_BIT, ScriptNETIDType, std::string, ULONG); // clientNetId, arg, senderPID
STANDARD_CALL_FUNCTION_HANDLER(ScriptServerEvent::ShutdownScript, STOP_HANDLER, ServerScript::STOP_BIT, ScriptNETIDType, std::string); // clientNetId, reason
STANDARD_CALL_FUNCTION_HANDLER(ScriptServerEvent::MenuEvent, MENUEVENT_HANDLER, ServerScript::MENU_BIT, ScriptNETIDType, ScriptLegacyArray<std::string>); // clientNetID, params[]
STANDARD_CALL_FUNCTION_HANDLER(ScriptServerEvent::ObjectMessage, MESSAGE_HANDLER, ServerScript::MESSAGE_BIT, std::string, int, bool, bool); // msg, senderPID, isBroadcast, fromAnotherZone
STANDARD_CALL_FUNCTION_HANDLER(ScriptServerEvent::PlayerArrived, ARRIVE_HANDLER, ServerScript::ARRIVE_BIT, ScriptNETIDType); // clientNetId

// PlayerDeparted: clientNetId, playerName, posrot, playerInfoTable (in registry)
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerDeparted> : EventHandler_Base {
	static constexpr const char* HandlerName = DEPART_HANDLER;
	static constexpr ScriptEventBit HandlerBit = DEPART_BIT;

	static int Handle(ServerScript* pScript, const ScriptNETIDType& netID, const ScriptPlayerInfo& playerInfo, const ScriptPosRot<float>& posrot) {
		return pScript->LuaPreCall(HandlerName, [&](lua_State* L) {
			ScriptArgManager sam(L);
			PushVarArgs(&sam, netID, playerInfo.m_name, posrot);
			pScript->CachePlayerInfoInLuaRegistry(L, netID.value, playerInfo);
		});
	}
};

// UnloadScript:
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::UnloadScript> : EventHandler_Base {
	static constexpr ScriptEventBit HandlerBit = UNLOAD_BIT;

	static int Handle(ServerScript* pScript) {
		pScript->Unload();
		return PCR_SKIPPED; // Tell scheduler that no Lua call is needed
	}
};

// Touch: clientNetId, keyState, touchPos, objectList
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::Touch> : EventHandler_Base {
	static constexpr const char* HandlerName = TOUCH_HANDLER;
	static constexpr ScriptEventBit HandlerBit = TOUCH_BIT;

	static int Handle(ServerScript* pScript, const ScriptNETIDType& netID, const ScriptPos<float>& clickPos, const ScriptLegacyArray<std::tuple<int, int>>& objects) {
		std::vector<float> clickPosArr = { clickPos.x, clickPos.y, clickPos.z };
		std::vector<int> objectIDs;
		for (const auto& tup : objects.value) {
			objectIDs.push_back(std::get<1>(tup));
		}
		return LuaPreCallWithArgs(pScript, HandlerName, netID, clickPosArr, objectIDs);
	}
};

// ObjectLeftClick/ObjectRightClick:
template <typename TEH>
struct EventHandler_CallObjectClickFunc : EventHandler_Base {
	static int Handle(ServerScript* pScript, const ScriptNETIDType& netID, const KeyModifierTable& keyModifiers, const ScriptPos<float>& clickPos, const ScriptLegacyArray<std::tuple<int, int>>& objects) {
		std::vector<float> clickPosArr = { clickPos.x, clickPos.y, clickPos.z };
		std::vector<int> objectIDs;
		for (const auto& tup : objects.value) {
			objectIDs.push_back(std::get<1>(tup));
		}
		return LuaPreCallWithArgs(pScript, TEH::HandlerName, netID, keyModifiers, clickPosArr, objectIDs.size(), objectIDs);
	}
};

SPECIAL_CALL_FUNCTION_HANDLER(ScriptServerEvent::ObjectLeftClick, LCLICK_OBJECT_HANDLER, ServerScript::LC_OBJECT_BIT, EventHandler_CallObjectClickFunc); // clientNetId, keyState, touchPos, objectList
SPECIAL_CALL_FUNCTION_HANDLER(ScriptServerEvent::ObjectRightClick, RCLICK_OBJECT_HANDLER, ServerScript::RC_OBJECT_BIT, EventHandler_CallObjectClickFunc); // clientNetId, keyState, touchPos, objectList
STANDARD_CALL_FUNCTION_HANDLER(ScriptServerEvent::Collide, COLLIDE_HANDLER, ServerScript::COLLIDE_BIT, ScriptNETIDType, ScriptPIDType, ScriptPos<float>, std::true_type); // clientNetId, PID, hitPos, true (enter)
STANDARD_CALL_FUNCTION_HANDLER(ScriptServerEvent::EndCollide, COLLIDE_HANDLER, ServerScript::COLLIDE_BIT, ScriptNETIDType, ScriptPIDType, ScriptPos<float>, std::false_type); // clientNetId, PID, hitPos, false (exit)

// Trigger
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::Trigger> : EventHandler_Base {
	static constexpr const char* HandlerName = TRIGGER_HANDLER;
	static constexpr ScriptEventBit HandlerBit = TRIGGER_BIT;

	static int Handle(ServerScript* pScript, const ScriptNETIDType& netID, USHORT type, float radius, int intParam, const std::string& strParam, const ScriptPos<float>& playerPos, const ScriptPos<float>& triggerPos, int triggerId) {
		return LuaPreCallWithArgs(pScript, HandlerName, netID, static_cast<int>(type), playerPos, triggerPos, radius, intParam, triggerId);
	}
};

SPECIAL_CALL_FUNCTION_HANDLER(ScriptServerEvent::MenuButton, MENU_BUTTON_HANDLER, ServerScript::MENU_BUTTON_BIT, EventHandler_CallMenuActionFunc);
SPECIAL_CALL_FUNCTION_HANDLER(ScriptServerEvent::MenuClose, MENU_CLOSED_HANDLER, ServerScript::MENU_CLOSED_BIT, EventHandler_CallMenuActionFunc);
SPECIAL_CALL_FUNCTION_HANDLER(ScriptServerEvent::MenuKeyDown, MENU_KEY_DOWN_HANDLER, ServerScript::MENU_KEYDOWN_BIT, EventHandler_CallMenuActionFunc);
SPECIAL_CALL_FUNCTION_HANDLER(ScriptServerEvent::MenuKeyUp, MENU_KEY_UP_HANDLER, ServerScript::MENU_KEYUP_BIT, EventHandler_CallMenuActionFunc);
SPECIAL_CALL_FUNCTION_HANDLER(ScriptServerEvent::MenuListSelect, MENU_LIST_SELECTION_HANDLER, ServerScript::MENU_LIST_SELECTION_BIT, EventHandler_CallMenuActionFunc);
SPECIAL_CALL_FUNCTION_HANDLER(ScriptServerEvent::MenuShopBuy, MENU_SHOP_BUY_HANDLER, ServerScript::MENU_SHOP_BUY_BIT, EventHandler_CallMenuActionFunc);

///////////////////////////////////////////////////////////////
// Script event handlers - unblock script

// PlayerRayIntersects: objectID, objectType, distance (Changed LUA return value order for consistency with the event - was distance, objectID, objectType)
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerRayIntersects> : EventHandler_Unblock<int, int, float> {};

// PlayerLocation: x, y, z, rx, ry, rz, success
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerLocation> : EventHandler_Unblock<float, float, float, float, float, float, int> {};

// PlayerUseItem/PlayerEquipItem/PlayerUnEquipItem: success
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerUseItem> : EventHandler_Unblock<int> {};
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerEquipItem> : EventHandler_Unblock<int> {};
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerUnEquipItem> : EventHandler_Unblock<int> {};

// PlayerCheckInventory/PlayerRemoveItem/PlayerAddItem: inventoryCount
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerCheckInventory> : EventHandler_Unblock<int> {};
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerRemoveItem> : EventHandler_Unblock<int> {}; // Used to call DB update in SSE. Now moved to SE before the event is sent
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerAddItem> : EventHandler_Unblock<int> {}; // Used to call DB update in SSE. Now moved to SE before the event is sent

// PlayerSaveEquipped/PlayerGetEquipped/PlayerSetEquipped: GLIDs, exclusionGroups
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerSaveEquipped> : EventHandler_Unblock<std::string, std::string> {};
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerGetEquipped> : EventHandler_Unblock<std::string, std::string> {};
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerSetEquipped> : EventHandler_Unblock<std::string, std::string> {};

// PlayerRemoveAllAccessories: "success" / "fail"
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerRemoveAllAccessories> : EventHandler_Unblock<std::string> {};

// ObjectGetNextPlaylistItem: playlistAsset
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::ObjectGetNextPlaylistItem> : EventHandler_Unblock<PlaylistAsset*> {};

// ObjectSetNextPlaylistItem: success, playlistID, playlistItem
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::ObjectSetNextPlaylistItem> : EventHandler_Unblock<int, int, int> {};

// ObjectEnumeratePlaylist: playlistAsset[], playlistID
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::ObjectEnumeratePlaylist> : EventHandler_Unblock<std::vector<PlaylistAsset*>, ScriptPlaylistID> {};

// GameZonesInfo: scriptZoneInfo[] - NOTE: old code pushes nil when array is empty
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::GameZonesInfo> : EventHandler_Unblock<ScriptLegacyArray<ScriptZoneInfo>> {};

// GameInstanceInfo: instanceId, locked, player[]
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::GameInstanceInfo> : EventHandler_Unblock<ScriptZoneInstanceInfo> {};

// CreateInstance: instanceId - note code pushes nil when instanceId <= 0
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::CreateInstance> : EventHandler_Unblock<int> {};

// PlayerJoinInstance: success
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlayerJoinInstance> : EventHandler_Unblock<int> {};

// LockInstance: locked
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::LockInstance> : EventHandler_Unblock<int> {};

// ItemInfo: itemInfo
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::ItemInfo> : EventHandler_Unblock<ScriptItemInfo> {};

// GracefulAPITimeout: void
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::GracefulAPITimeout> : EventHandler_Unblock<> {};

// PlaceGameItem:
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::PlaceGameItem> : EventHandler_UnblockBase {
	static int Handle(ServerScript* pScript, ULONG objPlacementId) {
		if (objPlacementId > 0) {
			ZoneIndex zi = pScript->GetZoneId().GetBaseZoneIndex();
			pScript->GetGameStateManager()->AppendPermanentObject(zi, objPlacementId);
		}

		return pScript->LuaResumeBlockingAPI([&](lua_State* L) {
			lua_pushnumber(L, objPlacementId);
		});
	}
};

// ScriptMakeWebcall - unblock kgp.scriptMakeWebCall API
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::ScriptMakeWebcall> : EventHandler_UnblockBase {
	static int Handle(ServerScript* pScript, const AsyncUserArg&, DWORD httpStatusCode, const std::string& httpStatusDesc, const std::string& resultXML) {
		_LogInfo("<ScriptMakeWebcall> [" << httpStatusCode << " " << httpStatusDesc << "] Received " << resultXML.size() << " bytes async=NO");
		return LuaResumeWithArgs(pScript, httpStatusCode, httpStatusDesc, resultXML);
	}
};

// ScriptMakeWebcall - asynchronously `kgp_result' callback
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::ScriptMakeWebcallAsync> : EventHandler_Base {
	static constexpr const char* HandlerName = ASYNC_RESULT_HANDLER;
	static constexpr ServerScript::ScriptEventBit HandlerBit = ServerScript::RESULT_BIT;

	static int Handle(ServerScript* pScript, const AsyncUserArg& userArg, DWORD httpStatusCode, const std::string& httpStatusDesc, const std::string& resultXML) {
		_LogInfo("<ScriptMakeWebcallAsync> [" << httpStatusCode << " " << httpStatusDesc << "] Received " << resultXML.size() << " bytes async=YES");
		// Call kgp_result
		return pScript->LuaPreCall(HandlerName, [&](lua_State* L) {
			lua_newtable(L); // table = {} (arg1)
			lua_newtable(L); // table.http = {}
			lua_setField(L, "code", (int)httpStatusCode); // table.http.code = httpStatusCode
			lua_setField(L, "desc", httpStatusDesc); // table.http.desc = httpStatusDesc
			lua_setfield(L, -2, "http");
			switch (userArg.m_luaType) {
				case LUA_TNUMBER:
					lua_setField(L, "arg", userArg.m_userInt); // table.arg = userInt
					break;
				case LUA_TSTRING:
					lua_setField(L, "arg", userArg.m_userStr); // table.arg = userStr
					break;
			}
			lua_setField(L, "data", resultXML); // table.data = resultXML
		});
	}
};

// EffectCompletion - asynchronously `kgp_elapsed' callback - kgp_elasped(objectPID, type, duration, args...)
template <>
struct ServerScript::TEventHandler<ScriptServerEvent::EffectCompletion> : EventHandler_Base {
	static constexpr const char* HandlerName = ELAPSED_HANDLER;
	static constexpr ScriptEventBit HandlerBit = ELAPSED_BIT;

	static int Handle(ServerScript* pScript, ScriptServerEvent* pEvent, int objectPID, int effectType, TimeMs effectDuration, GLID effectGLID) {
		_LogDebug("<EffectCompletion> [" << objectPID << " " << effectType << "] duration = " << effectDuration << ", effectGLID = " << effectGLID);
		if (pScript->GetScriptZone()->clearEffectCompletionEvent(objectPID, static_cast<EffectCode>(effectType), pEvent)) {
			pEvent->Release(); // Not disposed yet. Still in the middle of the event handler - dispatcher is holding a reference
			return LuaPreCallWithArgs(pScript, HandlerName, objectPID, effectType, effectDuration, effectGLID);
		} else {
			// Pending callback has been canceled or replaced
			return PCR_SKIPPED; // Lua handler not called
		}
	}
};

bool ServerScript::InitEventHandlerMap() {
	// Scan all event types. If TEventHandler is specialized on a certain type, add it to event handler map
	ForEachInRange<ScriptServerEvent::EventType, ScriptServerEvent::None, ScriptServerEvent::EventTypes>([](auto eventTypeConst) {
		AddEventHandler<eventTypeConst.value>();
	});

	return true;
}

} // namespace KEP
