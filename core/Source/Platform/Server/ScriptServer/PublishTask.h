///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptServerTaskObject.h"
#include "Event/Events/ScriptClientEvent.h"
#include <string>
#include <jsLock.h>

namespace KEP {
class ScriptServerEngine;

class PublishTask : public ScriptServerTaskObject {
public:
	PublishTask(
		//		log4cplus::Logger& logger,
		int gameId,
		const std::string& sourceDir,
		const std::string& destDir,
		const std::string& fileName,
		bool addToPatch,
		const std::string& patchPrefix,
		bool compileLua,
		ScriptClientEvent* completionEvent);

	virtual void ExecuteTask() override;

private:
	//	log4cplus::Logger& m_logger;
	int m_gameId;
	std::string m_sourceDir;
	std::string m_destDir;
	std::string m_fileName;
	bool m_addToPatch;
	std::string m_patchPrefix;
	bool m_compileLua;
	ScriptClientEvent* m_completionEvent;

protected:
	static fast_recursive_mutex m_publishSync;
	friend class DeleteAssetTask;
};

}; // namespace KEP
