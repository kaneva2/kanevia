///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "TransmitGameStateTask.h"
#include "ScriptServerEngine.h"
#include "Event/Events/ScriptClientEvent.h"

namespace KEP {

void TransmitGameStateTask::ExecuteTask() {
	for (auto itrList = m_gameState.begin(); itrList != m_gameState.end(); itrList++) {
		GameStateModifierMap& os = itrList->second;
		os.bake();

		// Dump map values into priority queue
		GameStateModifierMap::OrderedQueue que;
		os.getSorted(que);

		// Send events by ascending order (IsObjectGenerate, Timestamp)
		while (!que.empty()) {
			GameStateModifier* mod = que.top();
			ScriptClientEvent* e = NULL;
			mod->extract(e);

			if (e) {
				if (e->GetRoutingOption() == ScriptClientEvent::Routine_Client) {
					m_server->SendDirectlyToClient(m_clientId, e);
				} else {
					if (e->GetScope() == ScriptClientEvent::Scope_Player)
						*e->OutBuffer() << m_clientId;
					m_server->SendToClient(e);
				}
			}

			que.pop();
		}
	}
}

} // namespace KEP