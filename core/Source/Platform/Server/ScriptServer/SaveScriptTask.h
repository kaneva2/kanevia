///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptServerTaskObject.h"

#include <string>

namespace KEP {

class KGP_Connection;
class ScriptServerEngine;
class fast_recursive_mutex;

class SaveScriptTask : public ScriptServerTaskObject {
public:
	SaveScriptTask(
		//		log4cplus::Logger& logger,
		KGP_Connection* conn,
		fast_recursive_mutex& dbSync,
		ULONG objPlacementId,
		const std::string& filename);

	virtual void ExecuteTask() override;

private:
	//	log4cplus::Logger& m_logger;
	KGP_Connection* m_conn;
	fast_recursive_mutex& m_dbSync;
	const std::string m_filename;
	const ULONG m_objPlacementId;
};

}; // namespace KEP
