///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ScriptServerTaskObject.h"
#include "Event/Events/ScriptClientEvent.h"
#include <string>

namespace KEP {
class ScriptServerEngine;

class DeleteAssetTask : public ScriptServerTaskObject {
public:
	DeleteAssetTask(
		//		log4cplus::Logger& logger,
		int gameId,
		const std::string& sourceDir,
		const std::string& destDir,
		const std::string& fileName,
		const std::string& patchPrefix,
		ScriptClientEvent* completionEvent);

	virtual void ExecuteTask() override;

private:
	//	log4cplus::Logger& m_logger;
	int m_gameId;
	std::string m_sourceDir;
	std::string m_destDir;
	std::string m_fileName;
	std::string m_patchPrefix;
	ScriptClientEvent* m_completionEvent;
};

}; // namespace KEP
