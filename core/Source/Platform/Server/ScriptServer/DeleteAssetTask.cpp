///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "DeleteAssetTask.h"
#include "server/ScriptServer/ScriptServerEngine.h"
#include "PublishTask.h"
#include "KEPFileNames.h"

#include "CVersionInfo.h"
#include "jsEnumFiles.h"

#include "common\include\CompressHelper.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

DeleteAssetTask::DeleteAssetTask(
	//	log4cplus::Logger& logger,
	int gameId,
	const std::string& sourceDir,
	const std::string& destDir,
	const std::string& fileName,
	const std::string& patchPrefix,
	ScriptClientEvent* completionEvent) :
		ScriptServerTaskObject("DeleteAssetTask"),
		//	m_logger(logger),
		m_gameId(gameId),
		m_sourceDir(sourceDir),
		m_destDir(destDir),
		m_fileName(fileName),
		m_patchPrefix(patchPrefix),
		m_completionEvent(completionEvent) {}

void DeleteAssetTask::ExecuteTask() {
	bool fatalError = false;
	bool missingFile = false;
	bool missingEntry = false;
	bool isDir = false;
	std::string errorString;

	std::string sourcePath = PathAdd(m_sourceDir, m_fileName);

	// Delete source file
	if (jsFileExists(sourcePath.c_str(), &isDir) && !isDir) {
		if (!::DeleteFileW(Utf8ToUtf16(sourcePath).c_str())) {
			fatalError = true;
			errorString = "Error deleting file: " + sourcePath;
			LogError("DeleteFile() FAILED - " << errorString);
		}
	} else {
		fatalError = true;
		missingFile = true;
		errorString = "Cannot delete, file not found on server: " + sourcePath;
		LogError("FileExists() FAILED - " << errorString);
	}

	if (!fatalError) {
		std::string patchVersionInfoPath = m_destDir + VERSIONINFO_DAT;
		std::string patchName = m_patchPrefix + "---" + m_fileName;
		std::string serverPatchName = patchName;
		serverPatchName = ::CompressTypeExtAdd(serverPatchName);

		PublishTask::m_publishSync.lock();

		CVersionInfo worldVIFile;
		if (worldVIFile.loadFile(patchVersionInfoPath) == false) {
			LogError("worldVIFile.loadFile() FAILED - '" << patchVersionInfoPath << "'");
			worldVIFile.clear();
		}
		worldVIFile.set_Version("2.0");
		worldVIFile.set_VIFType("WORLD");
		worldVIFile.set_GameId(std::to_string(static_cast<long long>(m_gameId)));
		worldVIFile.set_VIVersion("1.0");

		FileEntryMap::iterator found = worldVIFile.getFileEntries().find(patchName);
		if (found == worldVIFile.getFileEntries().end()) {
			fatalError = true;
			missingEntry = true;
			errorString = "File not found in patch: " + patchName;
			LogError(errorString);
		} else {
			// Loaded viFile and found the entry were working on
			std::string delPatchPath = PathAdd(m_destDir, serverPatchName);
			if (jsFileExists(delPatchPath.c_str(), &isDir) && !isDir) {
				if (!::DeleteFileW(Utf8ToUtf16(delPatchPath).c_str())) {
					fatalError = true;
					errorString = "Error deleting file: " + delPatchPath;
					LogError(errorString);
				}
			} else {
				fatalError = true;
				errorString = "Cannot delete, file not found on patch server: " + delPatchPath;
				LogError(errorString);
			}
			if (!fatalError) {
				if (worldVIFile.writeFile(patchVersionInfoPath) == false) {
					fatalError = true;
					errorString = "Fatal error creating new patch";
					LogError("worldVIFile.writeFile() FAILED - '" << patchVersionInfoPath << "'");
				}
			}
		}
		PublishTask::m_publishSync.unlock();
	}

	if (fatalError && missingEntry && missingFile) {
		// File is missing from both directory and versioninfo
		// Don't send this to client as an error since it's now possible for a user to delete a menu without matching script due to directory sharing
		fatalError = false;
		errorString.clear();
	}

	// Pad result to completion event
	(*m_completionEvent->OutBuffer()) << !fatalError;
	(*m_completionEvent->OutBuffer()) << errorString;

	m_server->SendToClient(m_completionEvent);
}

} // namespace KEP