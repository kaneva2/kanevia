PASSED = "PASSED"
FAILED = "FAILED"
TESTING = "TESTING"

ScriptEventNames = {
      "ScriptError",
      "PlayerTell",
      "PlayerTeleport",
		"PlayerLoadMenu",
		"PlayerSendEvent",
      "ObjectGenerate",
		"ObjectMove",
      "PlayerGetLocation",
      "ObjectDelete",
		"ObjectSetTexture",
		"PlayerUseItem",
		"PlayerCheckInventory",
		"PlayerRemoveItem",
		"GameZonesInfo",
		"GameInstanceInfo",
		"CreateInstance",
		"PlayerJoinInstance",
		"LockInstance"
    }

function printPassedApis(passList)
   numEntries = #passList
  
   f_data:write("----- PASSED APIs -----\n")
   for i = 1, numEntries do
      f_data:write(passList[i])
   end
   f_data:write("----------\n\n")
   
   return numEntries
   
end

function printFailedApis(failList)
   numEntries = #failList
   
   f_data:write("----- FAILED APIs -----\n")
   for i = 1, #failList do
      f_data:write(failList[i])
   end
   f_data:write("----------\n\n")
   
   return numEntries
      
end