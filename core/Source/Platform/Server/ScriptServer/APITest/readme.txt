API_Test.lua

	Drop this on an object and then touch that object to begin the API test.

API_Test_Parser.lua

	This needs to be in the serverfiles directory of the tested server.  It cleans up the output of the Server Record Playback (SRP) dumps.
	

API_Test_Parser_Helper.lua

	This needs to be in the serverfiles directory of the tested server.
	
ApiTestMenu.xml
ApiTestMenu.lua

	Necessary in menu assets to record scriptserver sendmenuevent API.	
	
woknettest_100506-1107.krec

	Most recent api test recording with all APIs verified working properly.	