-- NOTE:  Gender could affect tests.

TEST_TELL_NAME = "TEST ARBITER"
TESTING_NAME = " TESTING "
SUCCESS_NAME = " PASSED "
FAIL_NAME = " FAILED "

TEST_GAME_ID = 1
TEST_ATTRIBUTE_1 = "test_attribute_1"
TEST_VALUE_1 = "test_value_1"
TEST_ATTRIBUTE_2 = "test_attribute_2"
TEST_VALUE_2 = "test_value_2"
TEST_LOG_MESSAGE = "TEST MESSAGE"

TEST_ZONE_INDEX = "50000036"

-- Environment specific UGC GLIDs
TEST_SOUND_GLID_PREVIEW = 3315241
TEST_PARTICLE_GLID_PREVIEW = 3423761


TEST_CLOTHING_GLID = 1173 -- select something already in armed inventory.
TEST_DYN_OBJ_GLID = 1076
TEST_MENU_NAME = "ApiTestMenu.xml"
TEST_SOUND_GLID = TEST_SOUND_GLID_PREVIEW
TEST_ANIM_GLID = 5399
TEST_STAND_ANIM_GLID = 5399
TEST_WALK_ANIM_GLID = 5399
TEST_RUN_ANIM_GLID = 5399
TEST_JUMP_ANIM_GLID = 5399
TEST_PARTICLE_GLID = TEST_PARTICLE_GLID_PREVIEW
TEST_TRIGGER_PARAM = 2010

gUser = nil
gObjId = nil

gTimerTested = false

function kgp_start(user)

    gUser = user

    kgp.playertell(user, "API TEST BEGIN", TEST_TELL_NAME, 1)

    END_TEST("kgp_start", true)

end

function kgp_timer(tick)

   if not gTimerTested then
      END_TEST("kgp_timer", true)
      gTimerTested = true
   end

end

function kgp_arrive(user)

    END_TEST("kgp_arrive", true)

end

function kgp_depart(user)

    END_TEST("kgp_depart", true)

    kgp.playertell(gUser, "API TEST END", TEST_TELL_NAME, 1)

end

function kgp_message(msg, src)

    END_TEST("objectsendmessage", src == gObjId)

    END_TEST("kgp_message", true)

end

function kgp_menuevent(numParams, paramTable)

    END_TEST("kgp_menuevent", paramTable[1])

end

function kgp_trigger( user, eventType, playerPosition, triggerPosition, radius, intParam )

	-- Don't know how to fire this event though
	END_TEST("kgp_trigger", intParam )

end

function kgp_touch(user, clickloc, count, objects)

    gUser = user

    result = nil

    END_TEST("kgp_touch", true)

    ----- Player APIs -----

    BEGIN_TEST("playergetlocation")
        x, y, z, rx, ry, rz, result = kgp.playergetlocation(user)
    END_TEST("playergetlocation", result)

    BEGIN_TEST("playersendevent")
        result = kgp.playersendevent(user, 0)
    END_TEST("playersendevent", result)

    BEGIN_TEST("gamegetcurrentinstance")
        instanceId = kgp.gamegetcurrentinstance()
    END_TEST("gamegetcurrentinstance", result)

    BEGIN_TEST("playerteleport")
        result = kgp.playerteleport(user, 13.1, 10, 13.1, 1, 0, 0)
    END_TEST("playerteleport", result)

	BEGIN_TEST("playergetgender")
		gender = kgp.playerGetGender( user )
	END_TEST("playergetgender", gender=="M")
	
	BEGIN_TEST("playersetcamera")
		result = kgp.playerSetCamera( user, 1, true )
	END_TEST("playersetcamera", result)
	
	BEGIN_TEST("playermove")
		x, y, z, rx, ry, rz = kgp.objectgetstartlocation( kgp.objectgetid() )
		if x==nil then
			x, y, z, rx, ry, rz = 10, 10, 10, 1, 0, 0
		end
		result = kgp.playerMove( user, x, y, z, rx, ry, rz, 0.1, 0, 10000, 1000, 0, false, false )
	END_TEST("playermove", result)
	
   ----------


    ----- Player Save/Load Data APIs -----

    name = nil

    BEGIN_TEST("playergetname")
        name = kgp.playergetname(user) -- should be found since user touched to kick off test
    END_TEST("playergetname", name)

   ----------

    ----- Player APIs -----

    BEGIN_TEST("playeruseitem")
        result = kgp.playeruseitem(user, TEST_CLOTHING_GLID)
    END_TEST("playeruseitem", result)

    BEGIN_TEST("playercheckinventory")
        kgp.playercheckinventory(user, TEST_CLOTHING_GLID)
    END_TEST("playercheckinventory", true) -- if this returns to unblock it must have succeeded

    BEGIN_TEST("playerremoveitem")
        kgp.playerremoveitem(user, TEST_CLOTHING_GLID)
    END_TEST("playerremoveitem", true) -- if this returns to unblock it must have succeeded

    BEGIN_TEST("playerjoininstance")
        result = kgp.playerjoininstance(instanceId, user)
    END_TEST("playerjoininstance", result)

   ----------

    ----- Game APIs -----

    BEGIN_TEST("gamegettimerfrequency")
        result = kgp.gamegettimerfrequency()
    END_TEST("gamegettimerfrequency", result)

    BEGIN_TEST("gamegetzonesinfo")
        result = kgp.gamegetzonesinfo()
		for i,zoneInfo in ipairs(result) do
			zoneId = zoneInfo[1]
			break
		end
    END_TEST("gamegetzonesinfo", result)

    BEGIN_TEST("gamegetinstanceinfo")
        result = kgp.gamegetinstanceinfo(instanceId)
    END_TEST("gamegetinstanceinfo", result)

    BEGIN_TEST("gamecreateinstance")
        result = kgp.gamecreateinstance(zoneId, user)
    END_TEST("gamecreateinstance", result)

    BEGIN_TEST("gamelockinstance")
        result = kgp.gamelockinstance(instanceId, true)
        kgp.playertell(user, "Instance lock is "..tostring(result), TEST_TELL_NAME, 1)
        result = kgp.gamelockinstance(instanceId, false)
        kgp.playertell(user, "Instance lock is "..tostring(result), TEST_TELL_NAME, 1)
    END_TEST("gamelockinstance", true) -- if this returns to unblock it must have succeeded

   ----------

    BEGIN_TEST("scriptgettick")
        result = kgp.scriptgettick()
    END_TEST("scriptgettick", result)

    BEGIN_TEST("scriptmakewebcall")
        result = kgp.scriptmakewebcall("http://www.kaneva.com")
		resultSuccess = false
		if result > 0 then
			resultSuccess = true
		end
    END_TEST("scriptmakewebcall", resultSuccess)

    BEGIN_TEST("scriptdofile")
        result = kgp.scriptdofile("PlayerPhysicsIds.lua")
    END_TEST("scriptdofile", result)

    phys = {}
    phys[pPhysics.CURSPEED] = 10
    phys[pPhysics.MAXSPEED] = 10
    phys[pPhysics.JUMPPOWER] = 5
    BEGIN_TEST("playersetphysics")
        result = kgp.playersetphysics(0, phys) -- This must follow scriptdofile
    END_TEST("playersetphysics", result)

    ----- Object APIs -----

    BEGIN_TEST("objectgenerate")
        genObj = kgp.objectgenerate(TEST_DYN_OBJ_GLID, 20, 0, -20, 0, 0, 1)
    END_TEST("objectgenerate", genObj)

    BEGIN_TEST("objectmove")
        result = kgp.objectmove(genObj, 500, 0, 0, -20, 1, 0, 0)
    END_TEST("objectmove", result)

    BEGIN_TEST("objectmapevents")
        result = kgp.objectmapevents(genObj)
    END_TEST("objectmapevents", result)

    BEGIN_TEST("objectdelete")
        result = kgp.objectdelete(genObj)
    END_TEST("objectdelete", result)

    BEGIN_TEST("objectgetid")
        gObjId = kgp.objectgetid()
    END_TEST("objectgetid", gObjId)

    BEGIN_TEST("objectgetstartlocation")
        result = kgp.objectgetstartlocation(gObjId)
    END_TEST("objectgetstartlocation", result)

    BEGIN_TEST("objectsettexture")
        result = kgp.objectsettexture(gObjId, "filestore0/7980/10048/BountyFace.jpg")
    END_TEST("objectsettexture", result)

    BEGIN_TEST("zonegetstats")
        result = kgp.zonegetstats()
    END_TEST("zonegetstats", result)

    BEGIN_TEST("objectsendmessage")
        kgp.objectsendmessage(gObjId, "Hello World")
    --END_TEST to this call is in kgp_message

	BEGIN_TEST("objectsetfriction")
		kgp.objectsetfriction(gObjId, true)
	END_TEST("objectsetfriction", 1)

	-- The animation is for the fairy GLID 
	local testAnimationGLID = 3423709
	BEGIN_TEST("objectsetanimation")
	    kgp.objectsetanimation(gObjId, testAnimationGLID)
	END_TEST("objectsetanimation", 1)    

    ----------

    BEGIN_TEST("playerloadmenu")
        result = kgp.playerloadmenu(user, TEST_MENU_NAME)
    END_TEST("playerloadmenu", result)

	----- Sound APIs -----

    BEGIN_TEST("soundgenerate")
		genSound = kgp.soundGenerate(TEST_SOUND_GLID, 20, 0, -20, 0, 0, 1)
    END_TEST("soundgenerate", genSound)

	BEGIN_TEST("soundconfig")
		kgp.soundConfig(genSound, 1.3, 0.5, 10, 1, 1, 2000, 180, 180, 1)	-- no return value
	END_TEST("soundconfig", 1)

	BEGIN_TEST("soundstart")
		kgp.soundStart(genSound)	-- no return value
	END_TEST("soundstart", 1)

    BEGIN_TEST("soundmove")
        result = kgp.soundMove(genSound, 500, 0, 0, -20, 1, 0, 0)
    END_TEST("soundmove", result)

	BEGIN_TEST("soundstop")
		kgp.soundStop(genSound)	-- no return value
	END_TEST("soundstop", 1)

    BEGIN_TEST("sounddelete")
        result = kgp.soundDelete(genSound)
    END_TEST("sounddelete", result)

	----- Trigger APIs -----
	BEGIN_TEST("objectsettrigger")
		triggerId = kgp.objectSetTrigger(kgp.objectGetId(), 10, TEST_TRIGGER_PARAM)
	END_TEST("objectsettrigger", triggerId)

	BEGIN_TEST("objectremovetrigger")
		kgp.objectRemoveTrigger(kgp.objectGetId())	-- no return value
	END_TEST("objectremovetrigger", 1)
	
	----- Animation APIs -----
	BEGIN_TEST("playersetanimation")
		kgp.playerSetAnimation(user, TEST_ANIM_GLID)	-- no return value
	END_TEST("playersetanimation", 1)

	BEGIN_TEST("playerdefineanimation")
		kgp.playerDefineAnimation(user, TEST_STAND_ANIM_GLID, TEST_WALK_ANIM_GLID, TEST_RUN_ANIM_GLID, TEST_JUMP_ANIM_GLID )	-- no return value
	END_TEST("playersetanimation", 1)

	----- Particle APIs -----
	BEGIN_TEST("objectsetparticle")
		kgp.objectSetParticle(kgp.objectGetId(), TEST_PARTICLE_GLID)			-- no return value
	END_TEST("objectsetparticle", 1)

	BEGIN_TEST("objectremoveparticle")
		kgp.objectRemoveParticle(kgp.objectGetId())			-- no return value
	END_TEST("objectremoveparticle", 1)

	BEGIN_TEST("gameGetCurrentTime")
		result = kgp.gameGetCurrentTime()
	END_TEST("gameGetCurrentTime", result)
	
	BEGIN_TEST("gameGetCurrentTimeStamp")
		result = kgp.gameGetCurrentTimeStamp()
	END_TEST("gameGetCurrentTimeStamp", result)
	
	BEGIN_TEST("gameLogMessage")
		result = kgp.gameLogMessage(TEST_LOG_MESSAGE)
	END_TEST("gameLogMessage", result)

end


--------------------
-- Utilities
--------------------

function BEGIN_TEST( api_name )

    tick = kgp.scriptgettick()
    kgp.playertell(gUser, tostring(api_name)..TESTING_NAME, TEST_TELL_NAME, 1)

end

function END_TEST( api_name, result )

    result_text = FAIL_NAME

    if result then
        result_text = SUCCESS_NAME
    end

    tick = kgp.scriptgettick()
    kgp.playertell(gUser, tostring(api_name)..result_text, TEST_TELL_NAME, 1)

end


