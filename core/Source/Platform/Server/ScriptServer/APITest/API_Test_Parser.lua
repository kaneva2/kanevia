dofile("API_Test_Parser_Helper.lua")

gOverAllResult = PASSED

function main()

  source1 = nil
  source2 = nil
  
  target1 = nil
  target2 = nil

  print()

  if arg[1] ~= nil then
     source1 = arg[1]
  else
     print("Please enter a source file to compare against as the first argument")
     return
  end
  --print("SOURCE1: "..tostring(source1))
  
  if arg[2] ~= nil then 
      source2 = arg[2]
  else
     print("Please enter a source file to compare as the second argument")
     return
  end
  --print("SOURCE2: "..tostring(source2))
   
  --print()
  target1 = parseAndOutput(source1)
  target2 = parseAndOutput(source2)
  
  --print()
  --print(target1)
  --print(target2)
  
end

function parseAndOutput(source, outputMethod)

  NumTested = 0
  Passed = {}
  Failed = {}

  s, strPos, target = string.find(source, "(.*).txt")

  target = target..".out.txt" 

  f_xml = io.open(source, "r")
  
  if f_xml then
  
    f_data = io.open(target, "w")
  
    if f_data then
  
        begin_log = nil       
        line = f_xml:read("*line")
        while not begin_log and line ~= nil do
        
           if line ~= nil then         
              begin_log = string.match(line, "API TEST BEGIN", 1)
              if begin_log then   
                 f_data:write(begin_log.."\n")
                 print(begin_log)
              else
                 line = f_xml:read("*line")
              end
           end
                                
        end    
  
        line = f_xml:read("*line")
        testComplete = false;
        while line ~= nil and not testComplete do
       
          start = string.match(line, "API TEST END", 1)      
          if start ~= nil then
          
            testComplete = true
            f_data:write(start.."\n")
            print(start)
              
          else
             
              start = string.match(line, "TEST ARBITER", 1)          	
              if start then
            	   criteria = ".*>%s(.*)%s" 
            	   s, strPos, tell = string.find(line, criteria, 1)			
            
                 if tell then
                    api =  "API "..tostring(tell)
                    print(api)
                    api = api.."\n"
                    f_data:write(api)
                    criteria = "API%s.-%s(.-)\n"
                    result = string.match(api, criteria)    
         
                    if result then
                       if result == PASSED then
                          NumTested = NumTested + 1
                          table.insert(Passed,api)                                           
                       elseif result == FAILED then
                          NumTested = NumTested + 1
                          table.insert(Failed,api)
                          gOverAllResult = FAILED
                       elseif result == TESTING then
                          --Do nothing for now
                       else
                          --print("Unknown result! "..tostring(result))
                       end
                   else
                      print("Unexpected result")
                   end                                  
                end
                                
        	  else 
                  
                start = string.find(line, ".*ScriptClientEvent", 1)
                      
                if start then                    
                   criteria = ".*f%s=%s(.*)%so" 
                   s, strPos, result = string.find(line, criteria, 1)			
              
                   if tonumber(result) ~= 1 then -- using playertell events for logging so it gets noisy. 
                      eventStr = "EVENT ScriptClientEvent "..tostring(ScriptEventNames[result+1])
                      print(eventStr)     
                      eventStr = eventStr.."\n"                    
                      f_data:write(eventStr)               
                      if ScriptEventNames[result+1] == "ScriptError" then
                         gOverAllResult = FAILED
                      end                                
                   end                    
                end
                  
             end
        		
        		line = f_xml:read("*line")
        
          end
       
        end
        
        f_data:write("\n\n")
        
        numPassed = printPassedApis(Passed)
        numFailed = printFailedApis(Failed)
        
        performance = "Total: "..tostring(NumTested).." Passed: "..tostring(numPassed).." Failed: "..tostring(numFailed).."\n"
    	  print(performance)
    	  print()
    	  print("Overall Result: "..gOverAllResult)
    	  print()
    	  f_data:write(performance)
    	  f_data:write("Overall Result: "..gOverAllResult)
    		
        f_data:close()
     
     else
        print("Couldn't create target file: "..target)
     end
  
     f_xml:close()
  
  else
  	print("***Cannot find file: "..source)
  end
  
  return target

end

main()