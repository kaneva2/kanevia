///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Server Script API Flow
//
// Calls from server scripts follow this general flow through the platform...
// * Lua makes server API call
// * -calls-> Script_API::playerSetEquipped(params) - parses lua parameters
// * -calls-> ScriptServerEngine::PlayerSetEquippedCallback(params) - builds new ScriptClientEvent and yields lua call
// * Dispatcher::ProcessOneEvent(SCE) Loop
// * -calls-> ServerEngine::ScriptClientEventHandler(SCE)
// * -calls-> ServerEngine::PlayerSetEquipped() - acts on CPlayerObj and builds new ScriptServerEvent broadcasts SCE to clients if needed
// * -calls-> ScriptServerEngine::ScriptServerExecuteEvent(SSE)
// * -calls-> ScriptServerEngine::PlayerSetEquippedCommandHandler(SSE) - sets lua return value and resumes lua call
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "LuaHelper.h"

// These includes are needed here because of the convention of declaring script handler functions in the header,
// even though they are not used externally. We should review whether this convention is desirable.
#include "Core/Math/Vector.h"
#include "Glids.h"

// Lua namespace and metatables
#define SCRIPT_API_NAMESPACE "kgp"
#define DICTIONARY_METATABLE_NAME "kgp.dictionary" // Generic look-up dictionary
#define OBJECT_METATABLE_NAME "kgp.object" // Object game state container
#define PLAYER_METATABLE_NAME "kgp.player" // Player game state container

// Registry variables
// Numeric keys
#define REGISTRY_ZONEINDEX 1 // Let's pray no other LUA C library uses numeric indices in the registry
// String keys
#define REGISTRY_KGPHANDLER_TEMPDATA "_KHTD"
#define REGISTRY_DICTIONARY_USERDATA_CACHE "_DUDC"
#define REGISTRY_DICTIONARY_USERDATA_CACHE_READONLY "_DUDCR"
#define KGPHANDLER_TEMPDATA_CLIENTID "clntID"
#define KGPHANDLER_TEMPDATA_PLAYERID "plyrID"
#define KGPHANDLER_TEMPDATA_POSITION "plyrpstn"
#define KGPHANDLER_TEMPDATA_PLAYERNAME "plyrnm"
#define KGPHANDLER_TEMPDATA_PLAYERTITLE "plyrttl"
#define KGPHANDLER_TEMPDATA_PLAYERGENDER "plyrgndr"
#define KGPHANDLER_TEMPDATA_PLAYERPERM "plyrprm"

namespace KEP {

struct AsyncUserArg;

class ScriptAPIRegistry {
public:
	ScriptAPIRegistry();
	~ScriptAPIRegistry();

	void RegisterAPIsOnLuaVM(lua_State* L) const;

private:
	static const LuaCFunction s_scriptServerAPI[];
	void registerAPIs(lua_State* L, const char* libname, const luaL_Reg* l, void* upval) const;
	void registerAPIs(lua_State* L, const char* libname, const LuaCFunction* l) const;
};

} // namespace KEP

// the array of function names and function pointers
extern const luaL_Reg dictionaryMetamethods[];
extern const luaL_Reg objectGameStateMetamethods[];
extern const luaL_Reg playerGameStateMetamethods[];

/*!
 * \brief
 * Disables the script (as if an error had occured).
 *
 * \param code
 * A number to represent the error.
 *
 * \param message
 * A text message that represents the error.
 *
 * \returns
 * no return value
 *
 * Calling this function does the same thing as if there were
 * an error in the script itself; it disables further processing
 * of the script and reports an error back to any player who tries
 * to interact with the script.
 *
 * The error code and message can be anything.  The message will
 * be displayed to any player who attempts to interact (e.g. touch)
 * the object.  The code isn't used for anything yet.
 *
 * A script disabled using this function must be manualy re-started by
 * removing and replacing the object.
 */
extern int scriptError(lua_State* L);

/*!
 * \brief
 * Gets the current tick count
 *
 * \returns
 * The current tick count.
 *
 * Get the current tick count (ms).
 *
 */
extern int scriptGetTick(lua_State* L);

/*!
 * \brief
 * Performs a dofile on the requested script.
 *
 * \param scriptName
 * Name of the lua file.
 *
 * \returns
 * Success if the file is loaded, failure otherwise
 *
 * Perform a dofile on the requested script.
 *
 */
extern int scriptDoFile(lua_State* L);

/*!
 * \brief
 * Makes a web call
 *
 * \param url
 * URL of the web call to make
 *
 * \returns
 * returnCode, returnDescription, returnContent
 *
 * Returns the web call data for the specfied URL. This API can be called asynchronously with
 * kgp.scriptCallAsync API. Asynchronous version takes the exact same set of arguments. The
 * asynchronous call back will be delivered through kgp_result event.
 *
 */
extern int scriptMakeWebcall(lua_State* L);
extern int scriptMakeWebcallAsync(lua_State* L, const KEP::AsyncUserArg& userArgs);

/*!
 *
 * \brief
 * This registers a stored procedure name and argument type list.  This allows for
 * error checking and type knowledge when the store procedure is called.  This takes
 * a variable list of parameters.
 *
 * \param name
 * A string indicating the stored procedure name.
 *
 * \param multiple
 * A variable amount of parameters that indicate the parameter types for the stored procedure.
 *
 */
extern int scriptRegisterStoredProcedure(lua_State* L);

/*!
 *
 * \brief
 * This unregisters a stored procedure name and argument type list.  This makes it convenient
 * to change a parameter type list in the script during testing.
 *
 * \param name
 * A string indicating the name of the stored procedure to unregister.
 *
 */
extern int scriptUnRegisterStoredProcedure(lua_State* L);

/*!
 *
 * \brief
 * Calls an existing stored procedure with the associated parameters.
 *
 * \param name
 * A string indicating the stored procedure name.
 *
 * \param multiple
 * A variable amount of parameters that indicate the parameter types for the stored procedure.
 *
 * \returns
 * Returns a table of tables that contain the return data.  The indicies (keys) are numeric and the
 * the data (values) are all strings.  The scripter is expected to know the return format.
 *
 */
extern int scriptCallStoredProcedure(lua_State* L);

/*!
 * \brief
 * Set the environment attributes of the game
 *
 * \param Environment Settings
 * table - key value pairs of environement attributes to set.
 *
 * \returns
 * Success or failure
 *
 * Set the environment attributes of the game.
 *
 */
extern int scriptSetEnvironment(lua_State* L);

/*!
 * \brief
 * Set the day state attributes of the game
 *
 * \param Environment Settings
 * table - key value pairs of day state attributes to set.
 *
 * \returns
 * Success or failure
 *
 * Set the day state attributes of the game.
 *
 */
extern int scriptSetDayState(lua_State* L);

/*!
 * \brief
 * Returns data on the state of the zone's scripts.
 *
 * \returns
 * The number of scripts running within the current zone, the total memory being used by
 * those scripts and a table of script data tables.
 *
 * The third return value is a table of script data tables.  The outer table
 * uses the script name as the key and has script-specific data as the value.  The inner, script
 * data, table contains the following keys:
 *	CurrentState:	Either READY, IDLE or BLOCKED.  READY means that the script is running, or is waiting to run some code.  IDLE means
 *					it has nothing to do and BLOCKED means that it's waiting for a long-running API function to return (e.g. one that
 *					makes a web call).
 *	PlacementId:	placement id of the object to which the script is attached.
 *	TotalTime:		Time (in ms) since the script was started (either by attaching it to an object or re-starting the zone instance.
 *	TimeReady:		Time (in ms) spent in the READY state.
 *	TimeIdle:		Time (in ms) spent in the IDLE state.
 *	TimeBlocked:	Time (in ms) spent in the BLOCKED state.
 */
extern int zoneGetStats(lua_State* L);

/*!
 * \brief
 * Teleports a player to the given position and rotation within the same zone.
 *
 * \param player
 * The id of the player who is to be teleported.
 *
 * \param x
 * x coordinate of the location at which the player will appear.
 *
 * \param y
 * y coordinate of the location at which the player will appear
 *
 * \param z
 * z coordinate of the location at which the player will appear
 *
 * \param rx
 * x component of the direction vector
 *
 * \param ry
 * y component of the direction vector, which is ignored.
 *
 * \param rz
 * z component of the direction vector.
 *
 * \returns
 * success or nil
 *
 * Use this function to teleport a player somewhere.  In order to find valid values, you can type "/l"
 * (without quotes) into the chat box.  This will produce a line of text in the following format:
 * X, Y, Z on NNNN in zoneId:instance at YYYY/DD/MM HH:MI.
 *
 * The position (x, y, z) can be used as arguments into the playerTeleport
 * function and will teleport the player to the exact same spot.  The NNNN and time parts of the output
 * can be ignored.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerTeleport(lua_State* L);

/*!
 * \brief
 * Sends a player to the kaneva URL
 *
 * \param player
 * The id of the player who is to be teleported.
 *
 * \param url
 * The kaneva URL.  Can be for the current game, or any other game.
 *
 * \returns
 * success or nil
 *
 * Use this function to teleport a player somewhere.  The format of the
 * url is documented on docs.kaneva.com
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerGotoUrl(lua_State* L);

/*!
 * \brief
 * Retrieves the position and rotation of a player's avatar.
 *
 * \param playerId
 * The network id of the player.
 *
 * \returns
 * x, y, z of the player location and x, y, z of the player's direction vector
 *
 * This function sends a request to the game server for a player's
 * position and orientation.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerGetLocation(lua_State* L);

/*!
 * \brief
 * Sends a message to a player.
 *
 * \param player
 * The id of the player who is to receive a message.
 *
 * \param message
 * A text message that is sent to the given player.
 *
 * \param objectTitle
 * The name of the object that will appear in the player's chat menu
 *
 * \param isTell
 * If set to 0, the message will be sent to all player's within the current zone.
 * If set to 1, the message will only be sent to the player specified by the player parameter
 *
 * \returns
 * no return value
 *
 * The tell function sends a message to the player's chat box.  At the moment, the message
 * appears to come from the player himself.  We probably need to change the message on the
 * client, so that the text appears to come from something external to the player.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerTell(lua_State* L);

/*!
 * \brief
 * Sends an event that can be used within a menu to the player.
 *
 * \param player
 * The id of the player who is to be sent the event.
 *
 * \param data
 * Data to encode within the event.
 *
 * \returns
 * no return value
 *
 * The specified player will receive an event from the script with the
 * data encoded.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 * playerLoadMenu() for a way to load a menu that can listen for an event
 */
extern int playerSendEvent(lua_State* L);

/*!
 * \brief
 * Sends a load menu event to the player.
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param menuName
 * The name of the menu to load.
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * The specified player will load a menu with the the specified menuName.
 * This menu will need to be located on the player's local computer.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 * playerSendEvent() for a way to send an event for the menu to use
 */
extern int playerLoadMenu(lua_State* L);

/*!
 * \brief
 * Retrieves the name of a player's avatar.
 *
 * \param playerId
 * The network id of the player.
 *
 * \returns
 * name of the player
 *
 * This function sends a request to the game server for a player's
 * name.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerGetName(lua_State* L);

/*!
 * \brief
 * Deprecated
 */
extern int playerSaveData(lua_State* L);

/*!
 * \brief
 * Deprecated
 */
extern int playerLoadAllData(lua_State* L);

/*!
 * \brief
 * Deprecated
 */
extern int playerLoadData(lua_State* L);

/*!
 * \brief
 * Deprecated
 */
extern int playerDeleteData(lua_State* L);

/*!
 * \brief
 * Deprecated
 */
extern int playerDeleteAllData(lua_State* L);

/*!
 * \brief
 * Uses an item that is in the specified player's inventory.
 *
 * \param playerId
 * int - id of the player.
 *
 * \param globalId
 * int - the global id of the item that is to be used.
 *
 * \returns
 * 1 if successful, 0 if not
 *
 * playerUseItem uses an item (defined with an ID of globalId) from
 * within the inventory of a player (defined with an ID of playerId).
 *
 */
extern int playerUseItem(lua_State* L);

/*!
 * \brief
 * Equips an item.  Hopefully get rid of inventory checks
 *
 * \param playerId
 * int - id of the player.
 *
 * \param globalId
 * int - the global id of the item that is to be equipped.
 *
 * \returns
 * 1 if successful, 0 if not
 *
 * playerEquipItem equips an item (defined with an ID of globalId) on
 * a player (defined with an ID of playerId).
 *
 */
extern int playerEquipItem(lua_State* L);

/*!
 * \brief
 * UnEquips an item.
 *
 * \param playerId
 * int - id of the player.
 *
 * \param globalId
 * int - the global id of the item that is to be unequipped.
 *
 * \returns
 * 1 if successful, 0 if not
 *
 * playerUnEquipItem equips an item (defined with an ID of globalId) on
 * a player (defined with an ID of playerId).
 *
 */
extern int playerUnEquipItem(lua_State* L);

/*! DRF - Added
 * \brief
 * Saves the set of all glids a player is wearing as an 'outfit' by name.
 *
 * \param playerId
 * int - the network id of the player.
 *
 * \param outfitName
 * int - The name to save the outfit as.
 *
 * \returns
 * 1 if successful 0 if not
 */
extern int playerSaveEquipped(lua_State* L);

extern int playerGetEquipped(lua_State* L);
extern int playerSetEquipped(lua_State* L);

/*!
 * \brief
 * Uses an item that is in the specified player's inventory.
 *
 * \param playerId
 * int - id of the player.
 *
 * \param globalId
 * int - The global id of the item that is to be checked.
 *
 * \returns
 * the number of items with globalId in the player's inventory
 *
 * playerCheckInventory returns the number of items (defined with an ID of globalId)
 * that are in a player's inventory (defined with an ID of playerId)
 *
 */
extern int playerCheckInventory(lua_State* L);

extern int playerRemoveAllAccessories(lua_State* L);

/*!
 * \brief
 * Adds an item to the specified player's inventory.
 *
 * \param playerId
 * int - id of the player.
 *
 * \param globalId
 * int - the global id of the item that is to be added.
 *
 * \param quantity
 * int - amount of items to add to the specified player's inventory
 *
 * \returns
 * new inventory count, -1 if adding the item failed
 *
 * playerAddItem adds a specified quantity of items (defined with an ID of globalId) to
 * the inventory of a player (defined with an ID of playerId).
 *
 */
extern int playerAddItem(lua_State* L);

/*!
 * \brief
 * Spawn player to an app instance
 *
 * \param instanceId
 * instance to spawn the player
 *
 * \param playerId
 * int - id of the player.
 *
 * \returns
 * boolean - success or failure
 *
 * Spawn player to an app instance
 *
 */
extern int playerJoinInstance(lua_State* L);

/*!
 * \brief
 * Sets the animation of the specified player.
 *
 * \param player
 * The id of the player who is to have their animation set.
 *
 * \param animationId
 * The id of the animation to play on the player.
 *
 * \returns
 * no return value
 *
 * The specified player will begin playing the specified animation.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerSetAnimation(lua_State* L);

/*!
 * \brief
 * Sets the animation of an item currently equipped on the player's avatar.
 *
 * \param player
 * The id of the player who is to have their animation set.
 *
 * \param itemId
 * The id of the equipped item that will play the animation.
 *
 * \param animationId
 * The id of the animation to play on the player.  Set to 0 to stop the item animating.
 *
 * \returns
 * no return value
 *
 * Start the animation playing on any (or all) items given in the arguments.  The animation
 * won't play if the player doesn't have that item equipped, or the item is incapable of
 * playing the animation.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerSetEquippedItemAnimation(lua_State* L);

/*!
 * \brief
 * Redefine animation for locomotions of the specified player.
 *
 * \param player
 * The id of the player who is to have their animation set.
 *
 * \param standId
 * The id of new animation used for standing.
 *
 * \param runId
 * The id of new animation used for walking.
 *
 * \param walkId
 * The id of new animation used for running.
 *
 * \param jumpId
 * The id of new animation used for jumping.
 *
 * \returns
 * no return value
 *
 * The specified player will use new animations in standing, walking, and etc. Use nil value if
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 * The playersetanimation() function for setting player animation temporarily.
 */
extern int playerDefineAnimation(lua_State* L);

/*!
 * \brief
 * Set the physics attributes of a player or all players
 *
 * \param destination
 * int - A unique number to identify which game the player data is being sent or 0 for all players.
 *
 * \param physicsSettings
 * table - key value pairs of physics attributes to set.
 *
 * \returns
 * Success or failure
 *
 * Set the physics attributes of a player or all players.
 *
 */
extern int playerSetPhysics(lua_State* L);

/*!
 * \brief
 * Get gender information of the specified player.
 *
 * \param player
 * The network id of the player.
 *
 * \returns
 * Gender type: 'M' = male, 'F' = female, nil = failed / player not found
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerGetGender(lua_State* L);

/*!
 * \brief
 * Set the title of a player while they are on this server.
 *
 * \param player
 * The id of the player. 0 = all players in current zone.
 *
 * \param title
 * a string for the player title
 *
 * \returns
 * 1=succeeded, nil = failed / player not found
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerSetTitle(lua_State* L);

/*!
 * \brief
 * Retrieves the player's title.
 *
 * \param playerId
 * The network id of the player.
 *
 * \returns
 * title of the player
 *
 * This function sends a request to the game server for a player's
 * title.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerGetTitle(lua_State* L);

/*!
 * \brief
 * Set the name of a player while they are on this server.
 *
 * \param player
 * The id of the player. 0 = all players in current zone.
 *
 * \param name
 * a string for the player name
 *
 * \returns
 * 1=succeeded, nil = failed / player not found
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerSetName(lua_State* L);

/*!
 * \brief
 * Move player to specified destination, animated.
 *
 * \param player
 * The id of the player.
 *
 * \param x
 * x coordinate of the destination
 *
 * \param y
 * y coordinate of the destination (y is ignored with certain AI mode if physics is enabled)
 *
 * \param z
 * z coordinate of the destination
 *
 * \param rx
 * x component of the direction vector. If both rx and rz are set to 0, player may face any direction at the end, which is implementation specific.
 *
 * \param ry
 * y component of the direction vector, which is ignored.
 *
 * \param rz
 * z component of the direction vector. If both rx and rz are set to 0, player may face any direction at the end, which is implementation specific.
 *
 * \param speed
 * Speed of movement.
 *
 * \param animation
 * 0 = auto determine by speed, 1 = walk, 2 = run
 *
 * \param timeout
 * Maximum duration (in millisecs) to wait before completion or cancellation.
 *
 * \param recalcInterval
 * # of milliseconds between recalculations. 0 = Automatic (determined by AI)
 *
 * \param AIMode
 * 0 = Straight line to destination until arrive. Cancel if timeout or player resumes control. Destination Y coordinate is ignored.
 * Other values reserved.
 *
 * \param noPlayerControl
 * If true, player control is temporarily disabled until arrival at destination or timeout (destination unreachable).
 *
 * \param noPhysics
 * Use false if gravity and collision are respected. Otherwise, no gravity/collision and player will be moved along a straight path from current location to destination.
 *
 * \returns
 * 1=succeeded, nil = failed / player not found
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerMove(lua_State* L);

/*!
* \brief
* Set avatar visibility flag to true
*
* \param objectId
* The id of the player
*
* \returns
* No return value.
*
* Set avatar visibility flag to true.
*
* \see
* playerHide function
*/
extern int playerShow(lua_State* L);

/*!
* \brief
* Set avatar visibility flag to false
*
* \param objectId
* The id of the player
*
* \returns
* No return value.
*
* Set avatar visibility flag to false.
*
* \see
* playerShow function
*/
extern int playerHide(lua_State* L);

/*!
* \brief
* Add a particle effect to a player
*
* \param clientId
* The id of the player
*
* \param particleGLID
* The ID of the particle effect item.
*
* \param boneName
* The name of the bone where particle is to be attached. If empty string, particle will be attached to entity root.
*
* \param particleSlotId
* ID to identify a particular particle slot where the particle is to be attached. This will allow multiple
* particle emitters been attached to one bone. particleSlotId is an arbitrary ID specified by developer which
* will be used in subsequent playerRemoveParticle call.
*
* \param offsetX
* X component of the emitter location offset vector in the bone space (or entity space if bone not specified).
* Offset is optional, default = (0, 0, 0).
*
* \param offsetY
* Y component of the emitter location offset vector in the bone space (or entity space if bone not specified).
*
* \param offsetZ
* Z component of the emitter location offset vector in the bone space (or entity space if bone not specified).
*
* \param dirX
* X component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
* Direction vector work together with up vector to define particle orientation. Optional, default: dir=(0,0,1), up=(0,1,0)
*
* \param dirY
* Y component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
*
* \param dirZ
* Z component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
*
* \param upX
* X component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
*
* \param upY
* Y component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
*
* \param upZ
* Z component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
*
* \returns
* No return value.
*
* Add a particle effect to a player.
*
* \see
* playerRemoveParticle function
*/
extern int playerSetParticle(lua_State* L);

/*!
* \brief
* Remove a particle effect from a player
*
* \param clientId
* The id of the player
*
* \param boneName
* The name of the bone where particle is to be removed. If empty string, particle will be removed from entity root.
*
* \param particleSlotId
* ID to identify a particular particle slot where the particle is to be removed. This will allow multiple
* particle emitters been attached to one bone. particleSlotId is an arbitrary ID specified by developer at
* playerSetParticle function call.
*
* Remove a particle effect from a player
*
* \see
* playerSetParticle function
*/
extern int playerRemoveParticle(lua_State* L);

/*!
 * \brief
 * Scales a player's avatar.
 *
 * \param player
 * The id of the player who is to be scaled.
 *
 * \param x
 * x scale factor
 *
 * \param y
 * y scale factor
 *
 * \param z
 * z scale factor
 *
 * \returns
 * nothing
 *
 * Use this function to change the height, width of depth of a player's avatar.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerScale(lua_State* L);

/*!
 * \brief
 * Obtains the ID, type and distance of an object directly infront of the player.
 *
 * \param player
 * The network id of the player.
 *
 * \returns
 * distance, objectId, objectType
 *
 * This function casts a ray from the player's midsection in the direction the player's facing and returns
 * information about the first object that the ray intersects.
 *
 * \see
 * KanevaConstants.lua for the values of objectType.
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerGetObjectInfront(lua_State* L);

/*!
* \brief
* Rotate a player to look at a specified target entity.
*
* \param clientId
* The id of the player
*
* \param targetType
* Type of the target entity. 0=None, 1=World, 2=Zone, 3=Objects, 4=Player
*
* \param targetId
* ID of the target entity: e.g. object ID, player client ID, etc.
*
* \param targetBone
* The name of the bone on target entity where the player will turn to.
* Optional. Default = "" (target root).
*
* \param offsetX
* X component of the look-at target location offset vector in the bone space (or entity space if bone not specified).
* Offset is optional, default = (0, 0, 0).
*
* \param offsetY
* Y component of the look-at target location offset vector in the bone space (or entity space if bone not specified).
*
* \param offsetZ
* Z component of the look-at target location offset vector in the bone space (or entity space if bone not specified).
*
* \param followTarget
* Boolean. If true, the player will track the specified target and keep looking at it. Otherwise, player will only be rotated once to face the target.
* Optional. Default = false.
*
* \param cancelableByPlayer
* Boolean. If true, the look-at function will be canceled if player resumes client turning control by keyboard or mouse.
* Optional. Default = false. Only valid if followTarget is true.
*
* \returns
* No return value.
*
* Rotate a player to look at a specified target entity.
*
* \see
* objectLookAt function
*/
extern int playerLookAt(lua_State* L);

/*!
 * \brief
 * Play synchronized animations, sounds and particle effects on an object.  These are combined into an 'effect track.'
 *
 * \param clientId
 * The player this effect track will control.
 *
 * \param effectTable
 * A table of effects.  Each entry in the table is itself a table, which contains the ID of the effect,
 * the start offset in ms from the start of the track and a GLID for the effect.  Some effects have additional
 * parameters in their table.  The effect ID and table layout for each one is given below.
 *
 * Animations {0, Start Time, GLID}
 * Sound {1, Start Time, GLID}
 * Particle Effect Start {2, Start Time, Stop Time, Bone Name, Slot Number, GLID}
 * Sound Object {3, Start Time, sound object's id}
 *
 * Here's an example effect track:
 * effectTrack =
 *      {
 *          {EFFECT_CODES.ANIMATION, 0, 3323651}, -- a sit anim at the start
 *          {EFFECT_CODES.ANIMATION, 5000, 3443078}, -- dance anim plays after 5 seconds
 *          {EFFECT_CODES.SOUND, 5000, 3325723}, -- sound, 1 second after the start
 *          {EFFECT_CODES.SOUND_OBJ, 5000, soundId}, -- a sound previously created with kgp.soundgenerate() whos id is stored in soundId
 *          {EFFECT_CODES.PARTICLE, 5000, 7000, "Bip01 Head", 0, 3443812}, -- particle effect starts after 5 seconds and runs until 7 seconds (2 seconds total)
 *      }
 *
 * \returns
 * no return value
 *
 * This function allows the caller to coordinate the timing of animations, particle effects and sounds
 * that will play on an avatar.  Using this function, the timing can be far more precisely controlled
 * than if the sounds, animations and particle effects were set using other functions.  The effect track
 * will play once.
 *
 * These animations, sounds and particle effects should all play very close to the times specified in the
 * effect track, but that might not happen on a client that must download the effect first.
 *
 * \see
 * kgp.scriptdofile() to load KanevaConstants.lua
 * kgp.objectseteffecttrack() for a way to create effects on an object
 * kgp.objectgenerate() for a way to get an object Id.
 * kgp.soundgenerate() for a way to generate a sound object that can be manipulated by an effect track
 */
extern int playerSetEffectTrack(lua_State* L);

/*!
 * \brief
 * Determins whether labels are rendered on top of other objects
 *
 * \param player
 * The id of the player whose client will be affected
 *
 * \param onTop
 * bool for whether or not the labels should be on top
 *
 * \returns
 * nothing
 *
 * This function sets whether or not labels are rendered on top of other objects
 *
 */
extern int playerRenderLabelsOnTop(lua_State* L);

/*!
 * \brief
 * Retrieves the permissions the player has for this 3d app (gm = 0, owner = 1, moderator = 2, member = 3)
 *
 * \param playerId
 * The network id of the player.
 *
 * \returns
 * permissions of the player
 *
 * This function sends a request to the game server for the player's permissions
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerGetPermissions(lua_State* L);

/*!
 * \brief
 * Sends a load Yes No menu event to the player.
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param titleCaption
 * The caption for the title bar
 *
 * \param descriptionText
 * Text for the description
 *
 * \param yesText
 * Text for the yes button
 *
 * \param noText
 * Text for the no button
 *
 * \param identifier
 * A unique identifier for this menu
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * The specified player will load a yes no menu with an appearance specified by
 * the input parameters
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 * playerSendEvent() for a way to send an event for the menu to use
 */
extern int playerOpenYesNoMenu(lua_State* L);

/*!
 * \brief
 * Sends a load Key Listener menu event to the player.
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param keyCodes
 * A string of keyboard characters the script wishes to be notified of.
 *
 * \param identifier
 * A unique identifier for this menu
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * The specified player will load a KeyListener menu that listens for
 * the given keys
 *
 */
extern int playerOpenKeyListenerMenu(lua_State* L);

/*!
 * \brief
 * Sends a load Text Timer menu event to the player.
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param durationInMs
 * A number of seconds to display the menu
 *
 * \param identifier
 * A unique identifier for this menu
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * The specified player will load a test timer for the given number
 * of seconds
 *
 */
extern int playerOpenTextTimerMenu(lua_State* L);

/*!
 * \brief
 * Sends a load Button menu event to the player.
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param title
 * The caption for the title bar
 *
 * \param description
 * The description for the button menu
 *
 * \param buttonText
 * The text for the button
 *
 * \param identifier
 * A unique identifier for this menu
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * A button menu will be generated
 *
 */
extern int playerOpenButtonMenu(lua_State* L);

/*!
 * \brief
 * Sends a load Status menu event to the player.
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param title
 * The caption for the title bar
 *
 * \param status
 * The text for the status message
 *
 * \param identifier
 * A unique identifier for this menu
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * A button menu will be generated
 *
 */
extern int playerOpenStatusMenu(lua_State* L);

/*!
 * \brief
 * Sends a load ListBox menu event to the player.
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param title
 * The caption for the title bar
 *
 * \param listContents
 * A string with comma separated values for the listbox
 *
 * \param buttonText
 * The text for the button
 *
 * \param identifier
 * A unique identifier for this menu
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * A listbox menu will be generated
 *
 */
extern int playerOpenListBoxMenu(lua_State* L);

/*!
 * \brief
 * Closes a preset Menu
 *
 * \param player
 * The id of the player who is to close the menu.
 *
 * \param menuName
 * The name of the menu to close
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * The specified player will close the given menu
 *
 */
extern int playerClosePresetMenu(lua_State* L);

/*!
 * \brief
 * Moves a preset Menu
 *
 * \param player
 * The id of the player who is to close the menu.
 *
 * \param menuName
 * The name of the menu to move
  *
 * \param x
 * The the new x location
 *
 * \param y
 * The new y location
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * The specified player will close the given menu
 *
 */
extern int playerMovePresetMenu(lua_State* L);

/*!
 * \brief
 * Creates or updates the coin HUD
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param type
 * The type of update
 *
 * \param value
 * The new coin value
 *
 * The coin hud will be updated
 *
 */
extern int playerUpdateCoinHUD(lua_State* L);

/*!
 * \brief
 * Creates or updates the health HUD
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param type
 * The type of update SETPLAYER, CHANGEPLAYER, CHANGEOBJECT
 *
 * \param target
 * Either a player name or object id as a string
 *
 * \param health
 * The new health of the player or object
 *
 * \param healthChange
 * The change in health for the floater
 *
 * The health hud will be updated
 *
 */
extern int playerUpdateHealthHUD(lua_State* L);

/*!
 * \brief
 * Creates or updates the xp HUD
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param level
 * The current level of the player
 *
 * \param levelXP
 * The xp on the current level
 *
 * \param xpToNextLevel
 * The total xp to the next level
 *
 * The health hud will be updated
 *
 */
extern int playerUpdateXPHUD(lua_State* L);

/*!
 * \brief
 * Adds a health indicator over the player or object
 *
 * \param player
 * The id of the player who is to display the indicator
 *
 * \param type
 * 3=Objects, 4=Player
 *
 * \param id
 * The placementId or playerName
 *
 * \param health
 * The percentage of health
 *
 * A health indicator will be displayed
 *
 */
extern int playerAddHealthIndicator(lua_State* L);

/*!
 * \brief
 * remove a health indicator over the player or object
 *
 * \param player
 * The id of the player who is to display the indicator
 *
 * \param type
 * 3=Objects, 4=Player
 *
 * \param id
 * The placementId or playerName
 *
 * A health indicator will be displayed
 *
 */
extern int playerRemoveHealthIndicator(lua_State* L);

/*!
 * \brief
 * Sends a load Shop menu event to the player.
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param title
 * The caption for the title bar
 *
 * \param identifier
 * A unique identifier for this menu
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * A listbox menu will be generated
 *
 */
extern int playerOpenShopMenu(lua_State* L);

/*!
 * \brief
 * Adds item for sale to smart shop menu
 *
 * \param player
 * The id of the player who is add the item
 *
 * \param itemId
 * The id of the item
 *
 * \param itemName
 * The name of the item
 *
 * \param itemCost
 * The Cost of the item
 *
 * \param itemIcon
 * The icon of the item
 *
 * \param identifier
 * unique identifier
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * A item will be added to the menu
 *
 */
extern int playerAddItemToShop(lua_State* L);

/*!
 * \brief
 * Display a simple status for the player
 *
 * \param player
 * The id of the player
 *
 * \param message
 * The text of the message
 *
 * \param time
 * The amount of time to display the message
 *
 * \param r
 * The red color component
 *
 * \param g
 * The green color component
 *
 * \param b
 * The blue color component
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * The status message will be displayed for the player
 *
 */
extern int playerShowStatusInfo(lua_State* L);

/*!
 * \brief
 * Sets custom texture on an accessory
 *
 * \param player
 * The id of the player to set the texture on
 *
 * \param accessoryGlid
 * The glid of the accessory
 *
 * \param textureUrl
 * The url of the customTexture
 *
 * \returns
 * no return value
 *
 * This function sets a custom texture on an accessory
 *
 */
extern int playerSetCustomTextureOnAccessory(lua_State* L);

/*!
 * \brief
 * Save custom spawn point for specified player in current zone
 *
 * \param player
 * The id of the player
 *
 * \param x
 * X coordinate of spawn position
 *
 * \param y
 * Y coordinate of spawn position
 *
 * \param z
 * Z coordinate of spawn position
 *
 * \param rx
 * X component of the player orientation vector
 *
 * \param ry
 * Y component of the player orientation vector
 *
 * \param rz
 * Z component of the player orientation vector
 *
 * \returns
 * no return value
 *
 * \see
 * playerDeleteZoneSpawnPoint
 *
 * This function saves a custom spawn point for specified player in current zone.
 */
extern int playerSaveZoneSpawnPoint(lua_State* L);

/*!
 * \brief
 * Delete a previously saved  a custom spawn point for specified player in current zone.
 *
 * \param player
 * The id of the player
 *
 * \returns
 * no return value
 *
 * \see
 * playerSaveZoneSpawnPoint
 *
 * This function deletes a previously saved  a custom spawn point for specified player in current zone.
 */
extern int playerDeleteZoneSpawnPoint(lua_State* L);

/*!
 * \brief
 * Deprecated
 */
extern int gameSaveData(lua_State* L);

/*!
 * \brief
 * Deprecated
 */
extern int gameLoadAllData(lua_State* L);

/*!
 * \brief
 * Deprecated
 */
extern int gameLoadData(lua_State* L);

/*!
 * \brief
 * Deprecated
 */
extern int gameDeleteData(lua_State* L);

/*!
 * \brief
 * Deprecated
 */
extern int gameDeleteAllData(lua_State* L);

/*!
 * \brief
 * Get info about all the running zones of a game
 *
 * \returns
 * table with all the zones information in the form {{Zone ID, App Name, # of running instances}, ...}
 *
 * Get info (Zone ID, App Name, # of running instances) about all the running zones of a game
 */
extern int gameGetZonesInfo(lua_State* L);

/*!
 *\brief Get the zone id of the zone in which the script is running
 *\returns the zone id
 *\see getZonesInfo for information about this and other zones
 */
extern int gameGetCurrentZone(lua_State* L);

/*!
*\brief Get instance info for the parent zone of the world.
*\returns the zoneInstanceId, zoneIndex, and zoneType of the parent, or 0.
*/
extern int getParentZoneInfo(lua_State* L);

/*!
*\brief Get instance info for the child zones of the world.
*\returns the zoneInstanceId, zoneIndex, and zoneType of the each child, or nil if there are no children.
*/
extern int getChildZoneIds(lua_State* L);

/*!
 * \brief
 * Get info about an instance
 *
 * \param instanceId
 * The instance id of the instance to get information about
 *
 * \returns
 * table with all the instance information in the form {{Instance #, # of players}, ...}
 *
 * Get info (Instance Number, Number of Players) about a running instance
 */
extern int gameGetInstanceInfo(lua_State* L);

/*!
 * \brief
 * Create an instance and spawn the player into the instance
 *
 * \param zoneId
 * The id of the zone being instanced
 *
 * \param player
 * The id of the player
 *
 * \returns
 * instance Id of the newly created instance
 *
 * Create an instance and spawn the player into the instance
 */
extern int gameCreateInstance(lua_State* L);

/*!
 * \brief
 * Lock or Unlock and instance to allow/disallow players from joining this instance
 *
 * \param instanceId
 * The id of the instance to lock/unlock
 *
 * \param lock
 * boolean whether to set the lock or not
 *
 * \returns
 * new lock value
 *
 * Lock or Unlock and instance to allow/disallow players from joining this instance
 *
 */
extern int gameLockInstance(lua_State* L);

/*
 * int - Developer specified game ID.
 *
 * \returns
 * Frequency of the time event in milliseconds
 *
 * gameGetTimerFreq returns the frequency of the ScriptServerEvent::Timer event in milliseconds.
 *
 */
extern int gameGetTimerFrequency(lua_State* L);

/*!
 * \brief
 * Get the current time in milliseconds from January 1, 1601
 *
 * \returns
 * number representing the current epoch time in milliseconds
 */
extern int gameGetCurrentTime(lua_State* L);

/*!
* \brief
* Get the current local  time in milliseconds from January 1, 1601
* accounting for time zone and daylight savings
* \returns
* number representing the current local epoch time in milliseconds
*/
extern int gameGetCurrentLocalTime(lua_State* L);

/*!
 * \brief
 * Get the current server tick count (in accordance to the tick value from kgp_timer function)
 *
 * \returns
 * number representing the current tick count (
 */
extern int gameGetCurrentTick(lua_State* L);

/*!
 * \brief
 * Return the game ID associated with the 3D App server.
 *
 * \returns
 * the game ID
 */
extern int gameGetCurrentGameId(lua_State* L);

/*!
 * \brief
 * Get the current time stamp in readable format
 *
 * \returns
 * string representing the current time stamp in readable format
 */
extern int gameGetCurrentTimeStamp(lua_State* L);

/*!
* \brief
* Convert date to corresponding epoch time
*
* \returns
* int representing the epoch time in seconds
*/
extern int gameDateToEpochTime(lua_State* L);

/*!
 * \brief
 * Obtain decimal zone index, instance id, and string zone index in hex of current script.
 *
 * \returns
 * Obtain decimal zone index, instance id, and string zone index in hex of current script and result code (0 failure).
 *
 * Obtain zone index and instance id of current script.
 */
extern int gameGetCurrentInstance(lua_State* L);

/*!
 * \brief
 * Logs a message to the server log
 *
 * \returns
 * 1 for success
 */
extern int gameLogMessage(lua_State* L);

/*!
 * \brief
 * Retrieve a list of players currently in the zone.
 *
 * \returns
 * array of player IDs
 */
extern int gameGetPlayersInZone(lua_State* L);

extern int scriptSavePlayerData(lua_State* L);

extern int scriptLoadAllPlayerData(lua_State* L);

extern int scriptLoadPlayerData(lua_State* L);

extern int scriptDeleteAllPlayerData(lua_State* L);

extern int scriptDeletePlayerData(lua_State* L);

extern int scriptSaveGameData(lua_State* L);

extern int scriptLoadAllGameData(lua_State* L);

extern int scriptLoadGameData(lua_State* L);

extern int scriptDeleteAllGameData(lua_State* L);

extern int scriptDeleteGameData(lua_State* L);

extern int gameGetFrameworkEnabled(lua_State* L);

extern int gameSetFrameworkEnabled(lua_State* L);

/*!
 * \brief
 * Generates an object.
 *
 * \param globalId
 * The global id of the object that is to be created.
 *
 * \param x
 * x coordinate of the location at which the object will appear.
 *
 * \param y
 * y coordinate of the location at which the object will appear
 *
 * \param z
 * z coordinate of the location at which the object will appear
 *
 * \param rx
 * x component of the direction vector
 *
 * \param ry
 * y component of the direction vector, which is ignored.
 *
 * \param rz
 * z component of the direction vector.
 *
 * \returns
 * the generated object's id
 *
 * This function sends an event to the game server that indicates
 * an object should be created.  The rotation is defined by the
 * direction vector, not angles.
 *
 * \see
 * The objectDelete() function.
 */
extern int objectGenerate(lua_State* L);

/*!
 * \brief
 * Deletes an object.
 *
 * \param objectId
 * The id of the object that is to be deleted.
 *
 * \returns
 * no return value
 *
 * This function deletes an object.
 *
 * \see
 * The objectgenerate() function.
 */
extern int objectDelete(lua_State* L);

/*!
 * \brief kgp.objectMove(objectId, timeMs, x, y, z, dx=0, dy=0, dz=0, ux=0, uy=0, uz=0, bufferCall=true)
 *
 * Moves and rotates the selected object:
 *
 * \param objectId
 * The id of the object that was touched.
 *
 * \param time
 * The time, in ms, taken to move from the object's current position to its new position.
 *
 * \param x
 * x coordinate for the object to move to.
 *
 * \param y
 * y coordinate for the object to move to.
 *
 * \param z
 * z coordinate for the object to move to.
 *
 * \param dx
 * optional x component of the direction vector
 *
 * \param dy
 * optional y component of the direction vector
 *
 * \param dz
 * optional z component of the direction vector
 *
 * \param ux
 * optional x component of the up vector
 *
 * \param uy
 * optional y component of the up vector
 *
 * \param uz
 * optional z component of the up vector.
 *
 * \param bufferCall
 * not really sure what this does or if it works
 *
 * \returns
 * no return value
 *
 * This function sends an event to the game server the indicates
 * an object should be moved and rotated.
 *
 * \see
 * The objectgetid() function to get the id of the object that the script is attached to.
 */
extern int objectMove(lua_State* L);

/*!
 * \brief
 * Rotates the selected object.
 *
 * \param objectId
 * The id of the object that was touched.
 *
 * \param time
 * The time, in ms, taken to rotate from the object's current orientation to the new orientation.
 *
 * \param x
 * World space x axis rotation, in degrees.
 *
 * \param y
 * World space y axis rotation, in degrees.
 *
 * \param z
 * World space z axis rotation, in degrees.
 *
 * \returns
 * no return value
 *
 * This function rotates the object in the X, Y and then Z world space axes.
 *
 * \see
 * The objectgetid() function to get the id of the object that the script is attached to.
 */
extern int objectRotate(lua_State* L);

/*!
 * \brief
 * Scales the selected object
 *
 * \param objectId
 * The id of the object.
 *
 * \param scale
 * Uniform scale factor.
 *
 * \returns
 * no return value
 *
 * This function sends an event to the game server the indicates
 * an object should be scaled.
 *
 * \see
 * The objectgetid() function to get the id of the object that the script is attached to.
 */
extern int objectScale(lua_State* L);

extern int objectSetDrawDistance(lua_State* L);

/*!
 * \brief
 * Gets the object's id
 *
 * \returns
 * The id of the object the script is attached to
 * or nil for failure.
 *
 * This function gets the id of the object that the script is attached to.
 *
 */
extern int objectGetId(lua_State* L);

/*!
 * \brief
 * Retrieves the position and rotation of an object.
 *
 * \param objectId
 * The id of the object whose location is desired.
 *
 * \returns
 * x, y, z of position and x, y, z, components of orientation vector, or
 * nil on failure.
 *
 * The objectgetstartlocation function returns the position and orientation
 * of the object from the database.
 *
 * \see
 * The objectgetid() function to get the id of the object that the script is attached to.
 */
extern int objectGetStartLocation(lua_State* L);

/*!
 * \brief
 * Sends a string message to the given object.
 *
 * \param objectId
 * The id of the object to recieve the message.
 *
 * \param message
 * The message (treated as a string) to send to the object.
 *
 * \param delay
 * The delay (in milliseconds) before the message will be delivered.  This is an optional paramter.
 * Also see LuaVMConfig::m_minDelayMillis.
 *
 * \returns
 * no return value
 *
 * This function sends a string to another object.  The recipient
 * must be in the same zone as the sender.
 *
 * \see
 * The objectgetid() function to get the id of the object that the script is attached to.
 * The message() callback function
 */
extern int objectSendMessage(lua_State* L);

/*!
 * \brief
 * Associate an un-scripted object with a scripted object, so that the scripted
 * object recieves touch and collide events when the un-scripted object is touched
 * or hit.
 *
 * \param objectId
 * The un-scripted object you want to get mapped events from.
 *
 * \returns
 * The id of the new object.
 *
 * This function always succeeds, but if the target object has a script then that script
 * will recieve the mapped events, not your script.  The mapped events will not be forwarded
 * as soon as the target recieves the script, or at all if the script is already on the object
 * when objectmapevents() is called.  If the target object is likely to have a script associated
 * with it at any time, it's better to use inter-script messaging via kgp.objectmessage() and
 * kgp_message(), the message event handler.
 *
 * \see
 * The kgp_touch event handler
 * the kgp_collide event handler
 * kgp.objectgenerate() for a way to get an object Id.
 */
extern int objectMapEvents(lua_State* L);

/*!
 * \brief
 * Sets the texture of an object.
 *
 * \param objectId
 * The id of the object whose texture is to be set.
 *
 * \param kanevaTexturePath
 * The relative path to the texture (this can be found within Build Mode's Pattern Tool).
 *
 * \returns
 * No return value.
 *
 * Sets the texture of an object.
 *
 */
extern int objectSetTexture(lua_State* L);

/*!
 * \brief
 * Sets the animation on a dynamic object.
 *
 * \param object
 * The id of the object whose animation is to be set.
 *
 * \param animation
 * The animation GUID.  This must be compatible with the skeleton.
 *
 * \returns
 * No return value.
 *
 * Sets the current animation to play on a dynamic object that has
 * a compatible skeleton.  Right now (pre-UGC skeletons), all our
 * animations match our skeleton, but in the future this will probably
 * be expanded to fail gracefully if incompatible skeletons and animations
 * are used.
 */
extern int objectSetAnimation(lua_State* L);

/*!
 * \brief
 * Sets a script on an object from a user's saved scripts
 *
 * \param object
 * The id of the object whose script is to be set.
 *
 * \param scriptName
 * The name of the script to apply to the object.
 *
 * \param arg
 * Optional argument to be passed to kgp_start event for the script.
 *
 * \returns
 * No return value.
 *
 * This API will load a script from the user's saved scripts
 * onto the object specified by the objectId.
 */
extern int objectSetScript(lua_State* L);

/*!
 * \brief
 * Generates a particle effect on the specified object within the zone.
 *
 * \param objectId
 * The id of the object where particle effect is to be applied
 *
 * \param particleGLID
 * The ID of the particle effect item that is to be attached.
 *
 * \param boneName
 * The name of the bone where particle is to be attached. If empty string, particle will be attached to object root.
 * Optional, default is empty string.
 *
 * \param particleSlotId
 * ID to identify a particular particle slot where the particle is to be attached. This will allow multiple
 * particle emitters been attached to one bone. particleSlotId is an arbitrary ID specified by developer which
 * will be used in subsequent objectRemoveParticle call.
 * Optional, default is 0.
 *
 * \param offsetX
 * X component of the emitter location offset vector in the bone space (or object space if bone not specified).
 * Offset is optional, default = (0, 0, 0).
 *
 * \param offsetY
 * Y component of the emitter location offset vector in the bone space (or object space if bone not specified).
 *
 * \param offsetZ
 * Z component of the emitter location offset vector in the bone space (or object space if bone not specified).
 *
 * \param dirX
 * X component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 * Direction vector work together with up vector to define particle orientation. Optional, default: dir=(0,0,1), up=(0,1,0)
 *
 * \param dirY
 * Y component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 *
 * \param dirZ
 * Z component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 *
 * \param upX
 * X component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 *
 * \param upY
 * Y component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 *
 * \param upZ
 * Z component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 *
 * \param bufferCall
 * Boolean of whether the call should be buffered or not. Default True.
 *
 * \returns
 * No return value
 *
 * This function sends an event to the game server that indicates
 * a particle effect should be created.  The rotation is defined by the
 * direction vector, not angles.
 *
 * \see
 * The objectRemoveParticle() function.
 */
extern int objectSetParticle(lua_State* L);

/*!
 * \brief
 * Generates a particle effect on the specified object within the zone.
 *
 * \param clientID
 * The net ID of the client this call applies to.
 *
 * \param objectId
 * The id of the object where particle effect is to be applied
 *
 * \param particleGLID
 * The ID of the particle effect item that is to be attached.
 *
 * \param boneName
 * The name of the bone where particle is to be attached. If empty string, particle will be attached to object root.
 * Optional, default is empty string.
 *
 * \param particleSlotId
 * ID to identify a particular particle slot where the particle is to be attached. This will allow multiple
 * particle emitters been attached to one bone. particleSlotId is an arbitrary ID specified by developer which
 * will be used in subsequent objectRemoveParticle call.
 * Optional, default is 0.
 *
 * \param offsetX
 * X component of the emitter location offset vector in the bone space (or object space if bone not specified).
 * Offset is optional, default = (0, 0, 0).
 *
 * \param offsetY
 * Y component of the emitter location offset vector in the bone space (or object space if bone not specified).
 *
 * \param offsetZ
 * Z component of the emitter location offset vector in the bone space (or object space if bone not specified).
 *
 * \param dirX
 * X component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 * Direction vector work together with up vector to define particle orientation. Optional, default: dir=(0,0,1), up=(0,1,0)
 *
 * \param dirY
 * Y component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 *
 * \param dirZ
 * Z component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 *
 * \param upX
 * X component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 *
 * \param upY
 * Y component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 *
 * \param upZ
 * Z component of the direction vector specifying particle orientation (in bone space, or object space if bone not specified).
 *
 * \param bufferCall
 * Boolean of whether the call should be buffered or not. Default True.
 *
 * \returns
 * No return value
 *
 * This function sends an event to the game server that indicates
 * a particle effect should be created.  The rotation is defined by the
 * direction vector, not angles.
 *
 * \see
 * The objectRemoveParticle() function.
 */
extern int objectSetParticleForPlayer(lua_State* L);

/*!
 * \brief
 * Deletes a particle effect.
 *
 * \param objectId
 * The id of the object where particle effect is to be removed
 *
 * \param boneName
 * The name of the bone where particle is to be attached. If empty string, particle will be attached to object root.
 * Optional, default is empty string.
 *
 * \param particleSlotId
 * ID to identify a particular particle slot where the particle is to be removed. This will allow multiple
 * particle emitters been attached to one bone. particleSlotId is an arbitrary ID specified by developer at
 * objectSetParticle function call.
 * Optional, default = 0.
 *
 * \returns
 * no return value
 *
 * This function deletes a particle effect.
 *
 * \see
 * The objectSetParticle() function.
 */
extern int objectRemoveParticle(lua_State* L);

/*!
 * \brief
 * Deletes a particle effect.
 *
 * \param clientID
 * The net ID of the client this call applies to.
 *
 * \param objectId
 * The id of the object where particle effect is to be removed
 *
 * \param boneName
 * The name of the bone where particle is to be attached. If empty string, particle will be attached to object root.
 * Optional, default is empty string.
 *
 * \param particleSlotId
 * ID to identify a particular particle slot where the particle is to be removed. This will allow multiple
 * particle emitters been attached to one bone. particleSlotId is an arbitrary ID specified by developer at
 * objectSetParticle function call.
 * Optional, default = 0.
 *
 * \returns
 * no return value
 *
 * This function deletes a particle effect.
 *
 * \see
 * The objectSetParticle() function.
 */
extern int objectRemoveParticleForPlayer(lua_State* L);

/*!
 * \brief
 * Enable/disable surface friction
 *
 * \param objectId
 * The id of the object where friction flag is to be set.
 *
 * \param enable
 * true = enable friction. false = disable friction (default behavior).
 *
 * \returns
 * no return value
 *
 * This function enables/disables surface friction for an object.
 *
 * \see
 * The objectGetId() function.
 */
extern int objectSetFriction(lua_State* L);

/*!
 * \brief
 * Highlights the object.
 *
 * \param objectId
 * The id of the object who is to be highlighted.
 *
 * \returns
 * No return value.
 *
 * Highlights the object.
 *
 */
extern int objectHighlight(lua_State* L);

/*!
 * \brief
 * Set texture panning parameters on a placed object.
 *
 * \param objectId
 * The id of the object
 *
 * \param meshId
 * ID of the mesh where texture panning to be enabled.
 *
 * \param uvSetId
 * ID of the UV coordinates set where texture panning to be enabled
 *
 * \param uIncrement
 * Pre-frame increment value for U component of texture coordinates
 *
 * \param vIncrement
 * Pre-frame increment value for V component of texture coordinates
 *
 * \returns
 * No return value.
 *
 * Set texture panning parameters on a placed object. Note that texture panning is enabled on per mesh basis (one material per mesh).
 * Even if such mesh has more than one set of UV data, all sets will be transformed at the same rate.
 *
 */
extern int objectSetTexturePanning(lua_State* L);

/*!
* \brief
* Set object visibility flag to true
*
* \param objectId
* The id of the object
*
* \returns
* No return value.
*
* Set object visibility flag to true.
*
* \see
* objectHide function
*/
extern int objectShow(lua_State* L);

/*!
* \brief
* Set object visibility flag to false
*
* \param objectId
* The id of the object
*
* \param playerId
* optional playerId
*
* \returns
* No return value.
*
* Set object visibility flag to false.
*
* \see
* objectShow function
*/
extern int objectHide(lua_State* L);

/*!
 * \brief
 * Play synchronized animations, sounds and particle effects on an object.  These are combined into an 'effect track.'
 *
 * \param objectId
 * The object this effect track will control.
 *
 * \param effectTable
 * A table of effects.  Each entry in the table is itself a table, which contains the ID of the effect,
 * the start offset in ms from the start of the track and a GLID for the effect.  Some effects have additional
 * parameters in their table.  The effect ID and table layout for each one is given below.
 *
 * Animations {0, Start Time, GLID}
 * Sound {1, Start Time, GLID}
 * Particle Effect Start {2, Start Time, Stop Time, Bone Name, Slot Number, GLID}
 * Sound Object {3, Start Time, sound object's id}
 *
 * The advantage of using EFFECT_CODES.SOUND_OBJ is that having created a sound object using kgp.soundgenerate(),
 * it can be manipulated using kgp.soundconfig().  The Effect Track can be used to make sure the sound starts playing
 * at the correct time after it's been created.
 *
 * Here's an example effect track:
 * effectTrack =
 *      {
 *          {EFFECT_CODES.ANIMATION, 0, 3443726}, -- aerobatics anim starts immediately
 *          {EFFECT_CODES.SOUND, 1000, 3325723}, -- sound, 1 second after the start
 *          {EFFECT_CODES.SOUND_OBJ, 1500, soundId}, -- a sound previously created with kgp.soundgenerate() whos id is stored in soundId
 *          {EFFECT_CODES.PARTICLE, 0, 1000, "Bip01", 0, 3443809}, -- particle effect from the start for 1 second
 *          {EFFECT_CODES.ANIMATION, 4000, 3443715}  -- return to idle anim after 4 seconds, which is about when the first animation ends
 *      }
 *
 * \returns
 * no return value
 *
 * This function allows the caller to coordinate the timing of animations, particle effects and sounds
 * that will play on the object.  Using this function, the timing can be far more precisely controlled
 * than if the sounds, animations and particle effects were set on an object using other functions.  The
 * effect will play once and then end.  If the object is animated by the effect track, that animation will
 * continue to play after the track has finished, but other players that arrive in the zone will not see
 * it.  The last animation of an effect track should therefore return the object to its previous state,
 * which might have been set with kgp.objectsetanimation().  kgp.objectsetanimation(objectId, 0) can be
 * used to stop the object animating.
 *
 * These animations, sounds and particle effects should all play very close to the times specified in the
 * effect track, but that might not happen on a client that must download the effect first.
 *
 * \see
 * kgp.scriptdofile() to load KanevaConstants.lua
 * kgp.playerseteffecttrack() for a way to create effects on a player's avatar
 * kgp.objectgenerate() for a way to get an object Id.
 * kgp.soundgenerate() for a way to generate a sound object that can be manipulated by an effect track
 */
extern int objectSetEffectTrack(lua_State* L);

/*!
* \brief
* Rotate an object to look at a specified target entity.
*
* \param objectId
* The id of the object
*
* \param targetType
* Type of the target entity. 0=None, 1=World, 2=Zone, 3=Objects, 4=Player
*
* \param targetId
* ID of the target entity: e.g. object ID, player client ID, etc.
*
* \param targetBone
* The name of the bone on target entity where the player will turn to.
* Optional. Default = "" (target root).
*
* \param offsetX
* X component of the look-at target location offset vector in the bone space (or entity space if bone not specified).
* Offset is optional, default = (0, 0, 0).
*
* \param offsetY
* Y component of the look-at target location offset vector in the bone space (or entity space if bone not specified).
*
* \param offsetZ
* Z component of the look-at target location offset vector in the bone space (or entity space if bone not specified).
*
* \param followTarget
* Boolean. If true, the player will track the specified target and keep looking at it. Otherwise, player will only be rotated once to face the target.
* Optional. Default = false.
*
* \param allowXZRotation
* Boolean. Not supported at this time.
* Optional. Default = false.
*
* \param forwardX
* X component of the object forward direction vector in the object space.
* Optional. Default = (0, 0, 1).
*
* \param forwardY
* Y component of the object forward direction vector in the object space.
*
* \param forwardZ
* Z component of the object forward direction vector in the object space.
*
* \returns
* No return value.
*
* Rotate an object to look at a specified target entity.
*
* \see
* playerLookAt function
*/
extern int objectLookAt(lua_State* L);

extern int objectGetNextPlayListItem(lua_State* L);

extern int objectSetNextPlayListItem(lua_State* L);

extern int objectEnumeratePlayList(lua_State* L);

extern int objectSetPlayList(lua_State* L);

/*!
 * \brief
 * Adds lines of text above a dynamic object.
 *
 * \param objectId
 * The id of the object to which text will be added.
 *
 * \param label
 * A table describing the lines of text to be added.  Each row in the table
 * is a line of text that will appear below the previous row.
 *
 * \returns
 * no return value
 *
 * This function adds a line of text above a dynamic object.  You can add multiple lines of text
 * and each will appear below the previous line.
 *
 * The format of the table is:
 * {"label text", font_size, red, green, blue}
 *
 * Note that font_size, red, green and blue area all optional, but they must be included in the table
 * in that order.  As a result, it's not possible to specify the 'blue' value without first giving
 * the font_size, red and green values.
 *
 * If not provided, the font_size's default value is 0.01.  Red, green and blue values default to 1.0.
 * font_size is clamped to the range [0,1], although a zero font size isn't likely to be useful.
 * Red, green and blue values are clamped to the range [0,1].
 *
 * e.g.
 * {"label1"}
 * {"label2", 0.01}
 * {"label3", 0.01, 1.0, 1.0, 1.0}
 *
 * \see
 * The objectClearLabels() function.
 */
extern int objectAddLabels(lua_State* L);

/*!
 * \brief
 * Adds lines of text above a dynamic object.
 *
 * \param clientID
 * The net ID of the client this call applies to.
 *
 * \param objectId
 * The id of the object to which text will be added.
 *
 * \param label
 * A table describing the lines of text to be added.  Each row in the table
 * is a line of text that will appear below the previous row.
 *
 * \returns
 * no return value
 *
 * This function adds a line of text above a dynamic object.  You can add multiple lines of text
 * and each will appear below the previous line.
 *
 * The format of the table is:
 * {"label text", font_size, red, green, blue}
 *
 * Note that font_size, red, green and blue area all optional, but they must be included in the table
 * in that order.  As a result, it's not possible to specify the 'blue' value without first giving
 * the font_size, red and green values.
 *
 * If not provided, the font_size's default value is 0.01.  Red, green and blue values default to 1.0.
 * font_size is clamped to the range [0,1], although a zero font size isn't likely to be useful.
 * Red, green and blue values are clamped to the range [0,1].
 *
 * e.g.
 * {"label1"}
 * {"label2", 0.01}
 * {"label3", 0.01, 1.0, 1.0, 1.0}
 *
 * \see
 * The objectClearLabels() function.
 */
extern int objectAddLabelsForPlayer(lua_State* L);

/*!
 * \brief
 * Clears all the labels
 *
 * \param objectId
 * The id of the object from which all labels are to be removed.
 *
 * \returns
 * no return value
 *
 * This function deletes all the labels on a dynamic object.
 *
 * \see
 * The objectAddLabel() function.
 */
extern int objectClearLabels(lua_State* L);

/*!
 * \brief
 * Clears all the labels for a specific player
 *
 * \param clientID
 * The net ID of the client this call applies to.
 *
 * \param objectId
 * The id of the object from which all labels are to be removed.
 *
 * \returns
 * no return value
 *
 * This function deletes all the labels on a dynamic object.
 *
 * \see
 * The objectAddLabel() function.
 */
extern int objectClearLabelsForPlayer(lua_State* L);

/*!
 * \brief
 * Activate and optionally set the color of the selection outline effect.
 *
 * \param objectId
 * The id of the object to outline.
 *
 * \param show
 * True to show the outline, false to hide it.
 *
 * \param red
 * optional color [0,1]
 *
 * \param green
 * optional color [0,1]
 *
 * \param blue
 * optional color [0,1]
 *
 * \returns
 * no return value
 *
 * This shows or hides the same style of outline used when selecting an object.  The last
 * three parameters can be omitted to leave the color unchanged, or included (in which case,
 * all three must be included) to set the RGB components of the outline color.
 *
 * \see
 */
extern int objectSetOutline(lua_State* L);

/*!
 * \brief
 * Set the SWF on a DO
 *
 * \param objectId
 * The id of the object to modify.
 *
 * \param SWF
 * The flash URL
 *
 * \param SWF parameters
 * Additional parameters.  Use an empty string if there are no additional parameters.
 *
 * \param user
 * Optional parameter that specifies which user will play this SWF.  When used, only the specified user plays the SWF.
 *
 * \returns
 * no return value
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int objectSetFlash(lua_State* L);

/*!
 * \brief
 * Set the media on a DO
 *
 * \param objectId
 * The id of the object to modify.
 *
 * \param url (optional)
 * The media URL
 *
 * \param params (optional)
 * Additional parameters.  Use an empty string if there are no additional parameters.
 *
 * \param volPct (optional)
 * Media player volume in percent. (Default is -1=no change)
 *
 * \param radiusAudio (optional)
 * Media player trigger radius for audio. (Default is -1=no change)
 *
 * \param radiusVideo (optional)
 * Media player trigger radius for video. (Default is -1=no change)
 *
 * \returns
 * no return value
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int objectSetMedia(lua_State* L);

/*!
 * \brief
 * Set the media volume and radius on a DO
 *
 * \param objectId
 * The id of the object to modify.
 *
 * \param volPct (optional)
 * Media player volume in percent. (Default is -1=no change)
 *
 * \param radiusAudio (optional)
 * Media player trigger radius for audio. (Default is -1=no change)
 *
 * \param radiusVideo (optional)
 * Media player trigger radius for video. (Default is -1=no change)
 *
 * \returns
 * no return value
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int objectSetMediaVolumeAndRadius(lua_State* L);

/*!
 * \brief
 * Return meta data for an object
 *
 * \param objectId
 * The global id of the object.
 *
 * \returns
 * Table of object data
 *
 * This function checks the item data cache in memory for the item meta data
 * (name, description, etc.), if it is found it is returned to the script, otherwise
 * it is fetched from the database, added to cache and returned to script.
 *
 */
extern int objectGetData(lua_State* L);

/*!
* \brief
* Return ownership data for a placed object
*
* \param pid
* The placement id of the object.
*
* \returns
* Object ownership data
*
* This function checks the item owner cache in memory for the item owner data
* (player id, player name), if it is found it is returned to the script, otherwise
* it is fetched from the database, added to cache and returned to script.
*
*/
extern int objectGetOwner(lua_State* L);

/*!
 * \brief
 * Modifies an object to be collidable or not
 *
 * \param objectId
 * The id of the object
 *
 * \param value
 * The value to set the collision to
 *
 * \returns
 * no return value
 *
 * The objectSetCollision function sets collision filtering on a given object
 *
 * \see
 * The objectgetid() function to get the id of the object that the script is attached to.
 */
extern int objectSetCollision(lua_State* L);

/*!
 * \brief
 * Activate a mouse over effect for an object
 *
 * \param type
 * The type world_obj = 1, dynamic_obj = 2, entity = 4, sound = 8, equippable = 16
 *
 * \param id
 * the object id
 *
 * \param mosMode
 * bitmask mode of effect during mouse over of object (MOS::MODE)
 *
 * \param tooltip
 * an optional tooltip
 *
 * \param cursorType
 * an optional param to override CURSOR_MODE_OBJECT cursor type during mouse over of object (requires mode=cursorType)
 *
 * \param mosCategory
 * an optional param for bitmask mouse over setting categorization (MOS::CATEGORY)
 *
 * \returns
 * no return value
 *
 * This sets a mouse over effect on the specified object.
 *
 * \see
 */
extern int objectSetMouseOver(lua_State* L);

/*!
 * \brief
 * Add a gem indicator thing to an object
 *
 * \param player
 * The player id
 *
 * \param type
 * 3=Objects, 4=Player
 *
 * \param id
 * placement id as a string or playerName
 *
 * \param offsetX
 * x offset
 *
 * \param offsetY
 * y offfset
 *
 * \param offsetZ
 * z offfset
 *
 * \param identifierGlid
 * glid for the gem thing
 *
 * \param rotation speed
 * rotation speed for the gem thing
 *
  * \param movementY
 * The distance for the gem thing to bounce up and down
 *
 * \returns
 * no return value
 *
 * This adds a gem indicator to an object
 *
 * \see
 */
extern int playerAddIndicator(lua_State* L);

/*!
 * \brief
 * Clears gems from an object
 *
 * \param player
 * The player id
 *
 * \param placementId
 * placement id of the DO to clear indicators
 *
 * \returns
 * no return value
 *
 * This clears gem indicators from objects
 *
 * \see
 */
extern int playerClearIndicators(lua_State* L);

/*!
 * \brief
 * Start sound playback.
 *
 * \param objectId
 * Sound placement ID
 *
 * \param delay
 * Number of milliseconds to wait before playback starts (for scheduled playback, optional)
 *
 * \returns
 * No return value.
 *
 * Start sound playback.
 *
 */
extern int soundStart(lua_State* L);

/*!
 * \brief
 * Stop sound playback.
 *
 * \param objectId
 * Sound placement ID
 *
 * \param delay
 * Number of milliseconds to wait before playback stops (for scheduled playback, optional)
 *
 * \returns
 * No return value.
 *
 * Stop sound playback.
 *
 */
extern int soundStop(lua_State* L);

/*!
 * \brief
 * Configure sound options.
 *
 * \param objectId
 * Sound placement ID
 *
 * \param pitch
 * Pitch shift. 1.0=Identity.
 *
 * \param volume
 * Sound volume. 0=Silent. 1.0=Full (100%).
 *
 * \param range
 * Maximum distance of sound (in Kaneva Units).
 *
 * \param attenuation
 * Factor of sound attenuation (roll-off). 0=No attenuation.
 *
 * \param loop
 * Loop flag. 0=Play Once. 1=Loop.
 *
 * \param loopDelay
 * Milliseconds between each time sound is played (when looping).
 *
 * \param innerConeAngle
 * Sound's inner cone angle in degrees.  The default is 360 degrees (omni-directional sound).
 *
 * \param outerConeAngle
 * Sound's outer cone angle in degrees.  The default is 360 degrees (omni-directional sound).
 *
 * \param outerConeGain
 * Volume gain at outer part of cone.
 *
 * \returns
 * No return value.
 *
 * Configure sound options. If nil value is used for a parameter value, the particular parameter will be ignored.
 *
 */
extern int soundConfig(lua_State* L);

extern int soundAddModifier(lua_State* L);

extern int soundAttachToObject(lua_State* L);

extern int soundAttachToPlayer(lua_State* L);

extern int soundPlayOnClient(lua_State* L);

extern int soundStopOnClient(lua_State* L);

/*!
 * \brief
 * Generates an ambient sound.
 *
 * \param globalId
 * The global id of the sound that is to be created.
 *
 * \returns
 * the generated sound's id
 *
 * This function generates a looping sound at position 0,0,0. The generated sound can
 * be heard equally well anywhere in the zone and does not appear to the user to
 * originate from any particular point, because each listener considers the position
 * of the sound source to be relative and not absolute.  In other words, a sound at
 * world position 0,0,0 appears to be located at each player's current position and not
 * in the middle of the zone.
 *
 * If the sound is moved, remember that the movement will appear to be relative to
 * each listener.  For example, it's possible to make a sound that everyone hears in
 * only one ear, whatever the listeners actual position in the zone happens to be.
 *
 * \see
 * kgp.soundgenerate(), kgp.sounddelete(), kgp.soundconfig()
 */
extern int soundGenerateAmbient(lua_State* L);

/*!
 * \brief
 * Generates a sound placed within the zone.
 *
 * \param globalId
 * The global id of the sound that is to be created.
 *
 * \param x
 * x coordinate of the location at which the sound will be placed.
 *
 * \param y
 * y coordinate of the location at which the sound will be placed.
 *
 * \param z
 * z coordinate of the location at which the sound will be placed.
 *
 * \param rx
 * x component of the direction vector
 *
 * \param ry
 * y component of the direction vector, which is ignored.
 *
 * \param rz
 * z component of the direction vector.
 *
 * \returns
 * the generated sound's id
 *
 * This function sends an event to the game server that indicates
 * a sound should be created.  The rotation is defined by the
 * direction vector, not angles.
 *
 * \see
 * The sounddelete() function.
 */
extern int soundGenerate(lua_State* L);

/*!
 * \brief
 * Moves and rotates the specified sound
 *
 * \param soundId
 * The id of the sound that is to be moved.
 *
 * \param time
 * The time, in ms, taken to move from the sound's current position to its new position.
 *
 * \param x
 * x coordinate for the sound to move to.
 *
 * \param y
 * y coordinate for the sound to move to.
 *
 * \param z
 * z coordinate for the sound to move to.
 *
 * \param rx
 * x component of the direction vector
 *
 * \param ry
 * y component of the direction vector, which is ignored.
 *
 * \param rz
 * z component of the direction vector.
 *
 * \returns
 * no return value
 *
 * This function sends an event to the game server that indicates
 * a sound should be moved and rotated.
 *
 */
extern int soundMove(lua_State* L);

/*!
 * \brief
 * Deletes a sound.
 *
 * \param soundId
 * The id of the sound that is to be deleted.
 *
 * \returns
 * no return value
 *
 * This function deletes a sound.
 *
 * \see
 * The soundgenerate() function.
 */
extern int soundDelete(lua_State* L);

/*!
 * \brief
 * Arms the specified item to the specified player.
 *
 * \param player
 * The id of the player.
 *
 * \param itemId
 * The id of the item to arm on the player.
 *
 * \returns
 * Whether the item was armed or not.
 *
 * The specified player will arm the item.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerArmItem(lua_State* L);

/*!
 * \brief
 * Disarms the specified item from the specified player.
 *
 * \param player
 * The id of the player.
 *
 * \param itemId
 * The id of the item to disarm from the player.
 *
 * \returns
 * Whether the item was disarmed or not.
 *
 * The specified player will disarm the item.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerDisarmItem(lua_State* L);

/*!
 * \brief
 * Sets the interval of time between ticks for this script.
 *
 * \param timeInterval
 * The time in between ticks.
 *
 * \returns
 * No return value
 *
 * This sets the interval of time between ticks for this script.
 */
extern int timerSetInterval(lua_State* L);

/*!
 * \brief
 * Sets the texture of an zone object.
 *
 * \param zoneObjectId
 * The id of the zone object whose texture is to be set.
 *
 * \param textureId
 * The id of the texture that should be set on the object.
 *
 * \returns
 * No return value.
 *
 * Sets the texture of an zone object.
 *
 */
extern int zoneSetTexture(lua_State* L);

/*!
 * \brief
 * Plays the media specified by the mediaURL on the object.
 *
 * \param objectId
 * The id of the object to play the specified media.
 *
 * \param mediaURL
 * The URL of the media to play.
 *
 * \returns
 * no return value
 *
 * Plays the media specified by the mediaURL on the object
 */
extern int objectPlayMedia(lua_State* L);

/*!
 * \brief
 * Sends a load pay menu event to the player.
 *
 * \param player
 * The id of the player who is to load the menu.
 *
 * \param amount
 * The amount of credits that you want the player to spend.
 *
 * \param itemName
 * The name of the item that the player will be purchasing.
 *
 * \returns
 * no return value
 *
 * The specified player will load the Kaneva Pay menu.  This menu is provided
 * by Kaneva to allow the player to purchase items from your application.
 *
 * \see
 * The kgp_start(), kgp_arrive() or kgp_touch() functions for a way to get a player's id.
 */
extern int playerLoadPayMenu(lua_State* L);

/*!
 * \brief
 * Create a new dictionary.
 *
 * \param name : string or nil
 * The name of the dictionary. Name will be generated automatically if the parameter is nil.
 *
 * \param readonly : boolean
 * If true, the return value will be a read-only copy of the dictionary. Developers are advised not to set read-only flag if the name
 * parameter is nil because no one would have a way to write to such dictionary.
 *
 * \returns
 * dictionary: userdata `kgp.dictionary'
 *
 */
extern int dictGenerate(lua_State* L);

/*!
 * \brief
 * Release (de-ref) an existing dictionary.
 *
 * \param dictionary: userdata `kgp.dictionary'
 * dictionary to be released
 *
 * \returns
 * 1 for success, nil for failure.
 *
 * Decrements the reference count to the dictionary data by one. Dispose the dictionary if reference count turns zero.
 */
extern int dictRelease(lua_State* L);

/*!
 * \brief
 * This is a metamethod that reads a value from a dictionary by key.
 *
 * \param dictionary : userdata `kgp.dictionary'
 * dictionary
 *
 * \param key : string
 * key
 *
 * \returns
 * value (number, string, boolean or child dictionary, nil if not found)
 *
 * This is not a exposed function. It's a metamethod that handles read access to the dictionary.
 */
extern int dictGetValue(lua_State* L);

/*!
 * \brief
 * This is a metamethod that udpates a dictionary value by key.
 *
 * \param dictionary : userdata `kgp.dictionary'
 * dictionary
 *
 * \param key : string
 * key
 *
 * \param value : number, string, boolean, dictionary or nil
 * value
 *
 * \returns
 * none
 *
 * This is not a exposed function. It's a metamethod that handles write access to the dictionary.
 */
extern int dictSetValue(lua_State* L);

/*!
 * \brief
 * Remove all key-value pairs from the dictionary.
 *
 * \param dictionary: userdata `kgp.dictionary'
 * dictionary to be cleared
 *
 * \returns
 * 1 for success, nil for failure.
 */
extern int dictClearValues(lua_State* L);

/*!
 * \brief
 * Obtain dictionary metadata.
 *
 * \param dictionary: userdata `kgp.dictionary'
 * dictionary to be cleared
 *
 * \returns
 * dictionary name : string or nil
 * reference count : number or nil
 */
extern int dictGetInfo(lua_State* L);

/*!
 * \brief
 * Return all keys in a array table for a dictionary.
 *
 * \param dictionary: userdata `kgp.dictionary'
 * dictionary
 *
 * \returns
 * keys: table
 */
extern int dictGetAllKeys(lua_State* L);

/*!
 * \brief
 * Iterator function for a dictionary (same signature as LUA next() function)
 *
 * \param state: { dictionary, keyTable, index }
 * Iterator state
 *
 * \param lastKey: string
 * Last key traversed (ignored)
 *
 * \returns
 * key: string
 * value: variant
 */
extern int dictIterate(lua_State* L);

/*!
 * \brief
 * Script helper to retrieve an instance member by first delegate key to the dictionary then look up in the instance table itself.
 *
 * \param self: table
 * class instance
 *
 * \param key: string (NOTE: numeric keys not supported)
 * member name
 *
 * \returns
 * value
 */
extern int classGetProperty(lua_State* L);

/*!
 * \brief
 * Script helper to update value for an instance member by delegate all numeric/string/boolean and child dictionary values to the dictionary. Everything else (function etc) goes to the instance table itself.
 *
 * \param self: table
 * class instance
 *
 * \param key: string (NOTE: numeric keys not supported)
 * member name
 *
 * \param value: variant
 * new value
 *
 * \returns
 * value
 */
extern int classSetProperty(lua_State* L);

/*!
 * \brief
 * Force lua VM to execute a garbage collection.
 *
 * \returns
 * none
 */
extern int scriptGC(lua_State* L);

extern int gameItemGetOwned(lua_State* L);

extern int gameItemGetOwnedByPlayer(lua_State* L);

extern int gameItemLoadAllByGame(lua_State* L);

extern int gameItemDelete(lua_State* L);

extern int gameItemUpdate(lua_State* L);

extern int gameItemPlace(lua_State* L);

extern int gameItemPlaceAsUser(lua_State* L);

extern int gameItemDeletePlacement(lua_State* L);

/*!
 * \brief
 * Gets the next game item ID for this zone.
 *
 * \returns
 * number for success, nil for failure.
 *
 */
extern int gameItemGetNextId(lua_State* L);

// AS - Adding new interface to set absolute position and rotation of an item via lua server script
extern int gameItemSetPosRot(lua_State* L);
//~AS

/*!
 * \brief
 * Pull game global config items from the database.
 *
 * \param properties: table
 * A table with the string names of the properties to fetch.
 *
 * \returns
 * values: table
 * The key/value pairs for the requested items.
 * (nil for failure)
 */
extern int gameLoadGlobalConfig(lua_State* L);

/*!
 * \brief
 * Initialize the database tables used by the LUA Framework game systems.
 * Includes default script game items and script game custom data.
 *
 * \returns
 * boolean - success or failure
 */
extern int gameInitFrameworkDB(lua_State* L);

/*!
 * \brief
 * Return a data container that provides direct access to object game states
 *
 * \param objectId
 * The global id of the object.
 *
 * \returns
 * container of object states
 *
 * Return a data container that provides direct access to object game states.
 *
 */
extern int objectGetGameState(lua_State* L);

/*!
 * \brief
 * Return a data container that provides direct access to player game states
 *
 * \param playerId
 * The network id of the player.
 *
 * \returns
 * container of player states
 *
 * Return a data container that provides direct access to player game states
 *
 */
extern int playerGetGameState(lua_State* L);

/*!
 * \brief
 * This is a metamethod that reads a value from a object game state container by key.
 *
 * \param gs : userdata `kgp.object'
 * game state container
 *
 * \param key : string
 * key
 *
 * \returns
 * value (number, string, boolean or table, nil if not found)
 *
 * This is not a exposed function. It's a metamethod that handles read access to the container.
 */
extern int objectGameStateGetValue(lua_State* L);

/*!
 * \brief
 * This is a metamethod that reads a value from a player game state container by key.
 *
 * \param gs : userdata `kgp.player'
 * game state container
 *
 * \param key : string
 * key
 *
 * \returns
 * value (number, string, boolean or table, nil if not found)
 *
 * This is not a exposed function. It's a metamethod that handles read access to the container.
 */
extern int playerGameStateGetValue(lua_State* L);

/*!
* \brief
* This is a metamethod that reads a value from a player game state container by key.
*
* \param gs : userdata `kgp.player'
* game state container
*
* \param key : string
* key
*
* \returns
* value (number, string, boolean or table, nil if not found)
*
* This is not a exposed function. It's a metamethod that handles read access to the container.
*/
extern int scriptRecordMetric(lua_State* L);

/*!
 * \brief
 * Invoke an asynchronous API call
 *
 * \param userArg: int or string or null
 * A user arg to be passed back with the result.
 *
 * \param apiName: string
 * Name of the kgp API to be called (requested API must support asynchronous mode).
 *
 * \param API argument 1
 * \param API argument 2
 * ...
 *
 * \returns
 * No return value.
 */
extern int scriptCallAsync(lua_State* L);

// Vehicle Functions
//extern bool playerSpawnVehicle(lua_State* L, IScriptArgManager* pScriptArgManager);
extern bool playerSpawnVehicle(
	lua_State* L,
	SCRIPT_NAMED_PARAM(ULONG, clientId),
	SCRIPT_NAMED_PARAM(int, placementId),
	SCRIPT_NAMED_PARAM(KEP::Vector3f, meshOffset),
	SCRIPT_NAMED_PARAM(KEP::Vector4f, meshOrientation),
	SCRIPT_NAMED_PARAM(KEP::Vector3f, dimensions),
	SCRIPT_NAMED_PARAM(float, maxSpeedKPH),
	SCRIPT_NAMED_PARAM(float, horsepower),
	SCRIPT_NAMED_PARAM(float, suspHeightLoaded),
	SCRIPT_NAMED_PARAM(float, suspHeightUnloaded),
	SCRIPT_NAMED_PARAM(float, massKG),
	SCRIPT_NAMED_PARAM(float, wheelFriction),
	SCRIPT_NAMED_PARAM_OPT(GLID, throttleSoundGlid),
	SCRIPT_NAMED_PARAM_OPT(GLID, skidSoundGlid),
	SCRIPT_NAMED_PARAM_OPT(KEP::Vector2f, jumpVelocityMPS));
bool playerModifyVehicle(
	lua_State* L,
	SCRIPT_NAMED_PARAM(ULONG, clientId),
	SCRIPT_NAMED_PARAM(float, maxSpeedKPH),
	SCRIPT_NAMED_PARAM(float, horsepower),
	SCRIPT_NAMED_PARAM(KEP::Vector2f, jumpVelocityMPS));
extern bool playerLeaveVehicle(lua_State* L, ULONG clientId, int placementId);

// NPC Functions
extern int npcSpawn(
	lua_State* L,
	SCRIPT_NAMED_PARAM_OPT(int, PID),
	SCRIPT_NAMED_PARAM_OPT(std::string, charConfig),
	SCRIPT_NAMED_PARAM_OPT(int, charConfigId),
	SCRIPT_NAMED_PARAM_OPT(int, dbIndex),
	SCRIPT_NAMED_PARAM_OPT(KEP::Vector3f, pos),
	SCRIPT_NAMED_PARAM_OPT(double, rotDeg),
	SCRIPT_NAMED_PARAM_OPT(GLID, glidAnimSpecial),
	SCRIPT_NAMED_PARAM_OPT(std::vector<GLID>, glidsArmed));
