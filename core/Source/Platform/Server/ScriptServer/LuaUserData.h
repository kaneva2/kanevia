///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace KEP {

// Userdata structure for a dictionary
class DictUserData {
private:
	HDICT m_hDict; // ILuaSharedDataProvider *
	fast_recursive_mutex* m_hMutex;
	bool m_readonly;
	char m_name[1];

public:
	DictUserData(const char* dictName, bool readonly, HDICT hDict, fast_recursive_mutex* hDictMutex) :
			m_readonly(readonly), m_hDict(hDict), m_hMutex(hDictMutex) {
		if (dictName) {
			strcpy_s(m_name, strlen(dictName) + 1, dictName);
		}
	}

	const char* GetName() const {
		return m_name;
	}
	bool IsReadonly() const {
		return m_readonly;
	}
	bool IsValid() const {
		return m_hMutex != NULL && m_hDict != NULL && m_name[0] != '\0';
	}

protected:
	ILuaSharedDataProvider* GetDict() const {
		return (ILuaSharedDataProvider*)m_hDict;
	}
	fast_recursive_mutex* GetSync() const {
		return (fast_recursive_mutex*)m_hMutex;
	}

	friend class ScriptServerEngine;
};

// Userdata structure for a game state container
class GameStateUserData {
private:
	ULONG m_ID;

public:
	GameStateUserData(ULONG id) :
			m_ID(id) {}
	ULONG GetID() {
		return m_ID;
	}
};

} // namespace KEP
