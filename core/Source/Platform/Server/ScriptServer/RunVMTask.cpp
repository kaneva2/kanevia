///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "RunVMTask.h"
#include "ScriptServerEngine.h"
#include "ServerScript.h"
#include "Scheduler.h"
#include "js.h"
#include "Event/LuaEventHandler/LuaScriptVM.h"
#include "Event/Events/ScriptServerEvent.h"
#include "LuaHelper.h"

#include "LogHelper.h"
static LogInstance("Instance");

namespace KEP {

RunVMTask::RunVMTask(
	//	log4cplus::Logger& logger,
	Scheduler* pScheduler,
	const ServerScriptPtr& script,
	bool isNewTask,
	int numberOfArgs) :
		ScriptServerTaskObject("RunVMTask"),
		//	m_logger(logger),
		m_pScheduler(pScheduler),
		m_pScript(script),
		m_isNewTask(isNewTask),
		m_numberOfArgs(numberOfArgs) {}

void RunVMTask::ExecuteTask() {
	//Remove this so the script crashes.  Otherwise it will hang the server (game server too as event blade)
	//and we wont know immediately something is wrong.
	//try {
	//
	// The alternative approach is to dump the TaskSystem and create
	// single-purpose threads myself and have them just run this code
	// in a loop in doWork.  Other than the queue management, much of
	// the code would be duplicated, so I want to avoid doing that.
	//

	//
	// Run the script, honoring the number of arguments when calling
	// lua_resume.  If the script finishes, change its state to IDLE.
	// If the script yields, keep it in the READY state and re-queue
	// the task, making sure that the number of arguments is set correctly.
	//
	// m_numberOfArgs can be other than zero if this is the first time
	// lua_resume is to be called after pushing function name and arguments
	// onto the lua stack.
	//
	ASSERT(m_pScript);
	if (m_pScript == 0) {
		LogError("null script");
		return;
	}

	// Link current task with script for debugging purpose
	m_pScript->SetCurrentTask(this);

	// Verify script state
	ASSERT(m_pScript->GetState() == ScriptState::SCHEDULED);
	if (m_pScript->GetState() != ScriptState::SCHEDULED) {
		LogError(m_pScript->LogSubject() << "Script state not READY, it's = " << ServerScript::GetStateName(m_pScript->GetState()) << m_pScript->LogSig());
		if (m_pScript->GetState() == ScriptState::BLOCKED) {
			TimeMs duration = 0;
			std::string reason;
			if (m_pScript->GetBlockingInfo(duration, reason)) {
				LogWarn(m_pScript->LogSubject() << "Script has blocked " << FMT_TIME << duration << " ms for " << reason << m_pScript->LogSig());
			}
		}
		m_pScheduler->scriptCallAborted(m_pScript);
		return;
	}

	m_pScheduler->scriptCallRunning(m_pScript);

	if (m_isNewTask) {
		// Reset yield count if new task
		m_pScript->ResetYieldCount();
	}

	// Run lua code or do nothing if dummy
	std::string errMsg;
	int rc = m_pScript->LuaCall(m_numberOfArgs, m_isNewTask, errMsg); // If m_isNewTask==true, it's a first call to the event handler with handler function in stack

	switch (rc) {
		case 0: {
			// Signal task completion
			m_pScheduler->scriptCallCompleted(m_pScript);
		} break;

		case LUA_YIELD:
			if (!m_server->processLuaYield(m_pScript)) {
				// Script is PRE-EMPTED. Need to re-queue for further processing,
				// Pre-empted by count hook, increment yield count
				m_pScript->IncrementYieldCount();

				// Warn if too many yields
				DWORD yieldCount = m_pScript->GetYieldCount();
				if (yieldCount >= YIELD_COUNT_ALERT_THRESHOLD && (yieldCount - YIELD_COUNT_ALERT_THRESHOLD) % YIELD_COUNT_ALERT_FILTER == 0) {
					LogWarn(m_pScript->LogSubject() << "Yielded " << m_pScript->GetYieldCount() << " times" << m_pScript->LogSig());
				}

				// Unlink task from script
				m_pScript->SetCurrentTask(NULL);
				m_pScheduler->scriptCallPreempted(m_pScript);

			} else {
				// Script is BLOCKED
				if (m_logger.getLogLevel() == TRACE_LOG_LEVEL) {
					TimeMs duration;
					std::string reason;
					if (m_pScript->GetBlockingInfo(duration, reason)) {
						//						LogTrace(m_pScript->LogSubject() << "Task blocked for " << FMT_TIME << duration << " ms because of " << reason << m_pScript->LogSig());
					}
				}
			}
			break;

		default: {
			// Get the error string from the lua stack (frame idx -1).
			LogWarn(m_pScript->LogSubject() << "return code: " << rc << m_pScript->LogSig());
			LogWarn(m_pScript->LogSubject() << "lua error message: " << errMsg);

			// Send this error to available audience and also save it for future player arrival and other events until another error comes
			m_server->ScriptErrorCallback(m_pScript, ServerScript::RUNTIME_ERROR, errMsg.c_str(), true, 0); // source information at stack level 0

			m_pScheduler->scriptCallErred(m_pScript);
		} break;
	};
}

} // namespace KEP
