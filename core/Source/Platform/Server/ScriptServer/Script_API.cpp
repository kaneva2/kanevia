///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// DRF - Making a new server script API...
// 1. Copy a similar existing API in Script_API.cpp.  This functions is what
//    initially catches the lua call directly.  It should LuaParseArgs() and
//    call the associated SSE-><apiName>Callback() with the parsed arguments.
// 2. Copy it's <apiName>() declaration in Script_API.h
// 3. Add the <apiName> to the scriptServerAPI[] array below, otherwise the
//    api will never actually get called from lua. This is what registers
//    <apiName>() call with the server script lua vm.
// 4. Copy it's <apiName>Callback() in ScriptServerEngine.cpp.  This
//    function should create a new ScriptClientEvent(<apiName>, 0) and encode
//    the api parameters into this event.  It then makes a copy of the event
//    to give the GameStateManager which will assure player's clients arriving
//    later are also passed the event.  It finally calls SendToClient(sce) to
//    actually send the event to the player's client but not without first
//    making a stop in ServerEngine::ScriptClientEventHandler(). The '0' param
//    in creating the script client event is what tells it this is headed to
//    the server engine and not the actual player's client.
// 5. Copy it's <apiName>Callback() declaration in ScriptServerEngine.h
// 6. Enumerate the <apiName> script client event in ScriptClientEvent.h
// 7. Add a case to the ServerEngine::ScriptClientEventHandler() to catch the
//    script client event just created and call ServerEngine::<apiName>(sce).
//    This allows the ServerEngine to first handle the event providing server-
//    side additional parameters needed by the client before being re-encoded
//    and broadcast to the specific client or all clients.
// 8. Copy it's ServerEngine::<apiName>() in ServerEngine.cpp. This function
//    decodes the script client event you just encoded above.  It then unpacks
//    the parameters and creates a new script client event of the same name but
//    this time substituting the '0' with the actual client netId to send to.
//    This redirection is done just incase the client requires additional
//    parameters that need to come from the server state itself.  Finally
//    this newly formed event is 'broadcast' to the client/s via a call to
//    BroadcastEventFromZoneIndex().  This is the event that will get caught
//    on the client/s in the ClientEngine::SCE_Handler() switch statement.
// 9. Add a case to the the ClientEngine::SCE_Handler() to catch the script
//    client event now actually on the client and call SCE_<apiName>(sce).
// 10. Copy it's ClientEngine::SCE_<apiName>(sce) handler code.  This function
//    again decodes the event and calls whatever appropriate funcitons of the
//    client are required to handle it.
// 11. Copy it's SCE_<apiName>(sce) declaration in ClientEngine.h
// 12. Cross your fingers and hope you are done
//
//
// Alternatively, if you just need to forward a ScriptClientEvent to the client:
//
// 1. Create a new ScriptClientEvent::EventType enumeration value.
// 2. Create a specialization of SCE_ClientHandler for your EventType in SCE_ClientHandler.h
//    Define the Lua function name and declare your handler function in that specialization.
// 3. Create an implementation for your handler in ClientEngine_ScriptClientEventHandling.cpp
///////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"

#include "Script_API.h"

#include "common\KEPUtil\Helpers.h"
#include "common\KEPUtil\LocationRegistry.h"
#include "common\include\CoreItems.h"

#include "ScriptServerEngine.h"
#include "LuaAdapters.h"
#include "LuaAsyncFunc.h"
#include "LuaUserData.h"
#include "ObjectLabelInfo.h"
#include "ScriptRuntimeData.h"
#include "GameItem.h"
#include "LuaHelper.h"
#include "ScriptGluePrimitiveGeomArgs.h"
#include "Event/Events/SCE_ClientHandler.h"
#include "Event/Glue/SoundPlacementIds.h"
#include "Event/Glue/MovementCapsIds.h"
#include "Event/Glue/EnvironmentIds.h"
#include "Event/Glue/DayStateIds.h"
#include "Core/Math/Quaternion.h"
#include "Core/Math/Stream.h"

static LogInstance("ScriptServerEngine");
static void stackDump(lua_State* L, const std::string& strTitle);

namespace KEP {

template <bool bBroadcastToAll, ScriptClientEvent::EventType eventType, typename T>
struct PassThroughHandlerForFunction;

template <ScriptClientEvent::EventType eventType, typename ReturnType, typename... Args>
struct PassThroughHandlerForFunction<true, eventType, ReturnType (*)(Args...)> {
	static void Handler(lua_State* L, Args&&... args) {
		ScriptServerEngine::Instance()->SendPlayerScriptClientEvent<eventType>(L, boost::none, std::forward<Args>(args)...);
	}
};

template <ScriptClientEvent::EventType eventType, typename ReturnType, typename... Args>
struct PassThroughHandlerForFunction<false, eventType, ReturnType (*)(Args...)> {
	static void Handler(lua_State* L, NETID clientId, Args&&... args) {
		ScriptServerEngine::Instance()->SendPlayerScriptClientEvent<eventType>(L, clientId, std::forward<Args>(args)...);
	}
};

template <bool bBroadcastToAll, ScriptClientEvent::EventType eventType>
struct PassThroughHandler : public PassThroughHandlerForFunction<bBroadcastToAll, eventType, decltype(&SCE_ClientHandler<eventType>::Handle)> {};

// Adds a LuaCFunction to call SCE_ClientHandler for given event type to provided vector.
// std::list of strings is provided because LuaCFunction takes const char* and we sometimes need to construct strings dynamically.
template <ScriptClientEvent::EventType eventType>
struct AddPassThroughHandler {
	static void Call(std::vector<LuaCFunction>* pLuaCFunctions) {
		// Event broadcast to everyone in zone
		if (SCE_ClientHandler<eventType>::LuaFunctionNameForZone != nullptr) {
			LuaCFunction luaCFunc(SCE_ClientHandler<eventType>::LuaFunctionNameForZone, &PassThroughHandler<true, eventType>::Handler);
			pLuaCFunctions->push_back(luaCFunc);
		}

		// Event sent to single player
		if (SCE_ClientHandler<eventType>::LuaFunctionNameForPlayer != nullptr) {
			LuaCFunction luaCFunc(SCE_ClientHandler<eventType>::LuaFunctionNameForPlayer, &PassThroughHandler<false, eventType>::Handler);
			pLuaCFunctions->push_back(luaCFunc);
		}
	}
};

ScriptAPIRegistry::ScriptAPIRegistry() {
}

ScriptAPIRegistry::~ScriptAPIRegistry() {
}

void ScriptAPIRegistry::RegisterAPIsOnLuaVM(lua_State* L) const {
	std::vector<LuaCFunction> aLuaCFunctions;
	ForEachSCE_ClientHandler<AddPassThroughHandler>(&aLuaCFunctions);
	aLuaCFunctions.push_back(LuaCFunction()); // 'Null' terminated array required for registerAPIs
	registerAPIs(L, SCRIPT_API_NAMESPACE, aLuaCFunctions.data());

	registerAPIs(L, SCRIPT_API_NAMESPACE, s_scriptServerAPI);

	// Create dictionary metatable
	luaL_newmetatable(L, DICTIONARY_METATABLE_NAME);
	registerAPIs(L, NULL, dictionaryMetamethods, nullptr);
	lua_pop(L, 1);

	// Create object metatable
	luaL_newmetatable(L, OBJECT_METATABLE_NAME);
	registerAPIs(L, NULL, objectGameStateMetamethods, nullptr);
	lua_pop(L, 1);

	// Create player metatable
	luaL_newmetatable(L, PLAYER_METATABLE_NAME);
	registerAPIs(L, NULL, playerGameStateMetamethods, nullptr);
	lua_pop(L, 1);
}

void ScriptAPIRegistry::registerAPIs(lua_State* L, const char* libname, const luaL_Reg* l, void* upval) const {
	lua_pushlightuserdata(L, upval);
	LUA_REGISTER(L, libname, l, 1);
}

void ScriptAPIRegistry::registerAPIs(lua_State* L, const char* libname, const LuaCFunction* pLuaCFunctions) const {
	// Register one at a time so we can give each a unique upvalue.
	luaL_Reg luaRegData[2] = { 0 };
	const LuaCFunction* pNextFunction = pLuaCFunctions;
	for (; pNextFunction->m_pFunctionAddress; ++pNextFunction) {
		luaRegData[0].func = pNextFunction->m_pFunctionAddress;
		luaRegData[0].name = pNextFunction->m_pszFunctionName;
		lua_pushinteger(L, pNextFunction->m_iFunctionUpValue);
		LUA_REGISTER(L, libname, luaRegData, 1);
	}
}

} // namespace KEP

using namespace KEP;

#define BAD_ARGUMENTS_ERROR_CODE 0
#define BAD_ARGUMENTS_ERROR_STR "Bad Arguments"

#define SCRIPT_DATA_STORE_SIZE 256 // must match the size of the VARCHAR data column in wok.script_data

#define CLASS_PROPERTIES_TABLE "_properties"
#define CLASS_PROPERTIES_DICTIONARY "_dictionary"

//
// If it's not in this array, it won't be used by a script.
//
// We've decided to standardize on kgp.nounverb
// like the existing LUA libraries but with the noun first,
// which describes the object you'll be operating on.
//
const LuaCFunction KEP::ScriptAPIRegistry::s_scriptServerAPI[] = {
	{ "playerTell", playerTell },
	{ "playerTeleport", playerTeleport },
	{ "playerGetLocation", playerGetLocation },
	{ "playerSendEvent", playerSendEvent },
	{ "playerLoadMenu", playerLoadMenu },
	{ "playerGetName", playerGetName },
	{ "playerSaveData", playerSaveData },
	{ "playerLoadAllData", playerLoadAllData },
	{ "playerLoadData", playerLoadData },
	{ "playerDeleteData", playerDeleteData },
	{ "playerDeleteAllData", playerDeleteAllData },
	{ "playerUseItem", playerUseItem },
	{ "playerEquipItem", playerEquipItem },
	{ "playerUnEquipItem", playerUnEquipItem },
	{ "playerSaveEquipped", playerSaveEquipped },
	{ "playerGetEquipped", playerGetEquipped },
	{ "playerSetEquipped", playerSetEquipped },
	{ "playerCheckInventory", playerCheckInventory },
	{ "playerRemoveAllAccessories", playerRemoveAllAccessories },
	{ "playerAddItem", playerAddItem },
	{ "playerJoinInstance", playerJoinInstance },
	{ "playerSetAnimation", playerSetAnimation },
	{ "playerSetEquippedItemAnimation", playerSetEquippedItemAnimation },
	{ "playerDefineAnimation", playerDefineAnimation },
	{ "playerSetPhysics", playerSetPhysics },
	{ "playerGetGender", playerGetGender },
	{ "playerSetTitle", playerSetTitle },
	{ "playerGetTitle", playerGetTitle },
	{ "playerSetName", playerSetName },
	{ "playerMove", playerMove },
	{ "playerShow", playerShow },
	{ "playerHide", playerHide },
	{ "playerSetParticle", playerSetParticle },
	{ "playerRemoveParticle", playerRemoveParticle },
	{ "playerScale", playerScale },
	{ "playerGetObjectInfront", playerGetObjectInfront },
	{ "playerSetEffectTrack", playerSetEffectTrack },
	{ "playerSetCustomTextureOnAccessory", playerSetCustomTextureOnAccessory },
	{ "playerSaveZoneSpawnPoint", playerSaveZoneSpawnPoint },
	{ "playerDeleteZoneSpawnPoint", playerDeleteZoneSpawnPoint },
	{ "gameSaveData", gameSaveData },
	{ "gameLoadAllData", gameLoadAllData },
	{ "gameLoadData", gameLoadData },
	{ "gameDeleteData", gameDeleteData },
	{ "gameDeleteAllData", gameDeleteAllData },
	{ "gameGetTimerFrequency", gameGetTimerFrequency },
	{ "gameGetCurrentTime", gameGetCurrentTime },
	{ "gameGetCurrentLocalTime", gameGetCurrentLocalTime },
	{ "gameGetCurrentTick", gameGetCurrentTick },
	{ "gameGetCurrentTimeStamp", gameGetCurrentTimeStamp },
	{ "gameDateToEpochTime", gameDateToEpochTime },
	{ "gameLogMessage", gameLogMessage },
	{ "gameGetZonesInfo", gameGetZonesInfo },
	{ "gameGetCurrentZone", gameGetCurrentZone },
	{ "gameGetInstanceInfo", gameGetInstanceInfo },
	{ "gameCreateInstance", gameCreateInstance },
	{ "gameLockInstance", gameLockInstance },
	{ "gameGetCurrentInstance", gameGetCurrentInstance },
	{ "gameGetCurrentGameId", gameGetCurrentGameId },
	{ "gameGetPlayersInZone", gameGetPlayersInZone },
	{ "scriptSetEnvironment", scriptSetEnvironment },
	{ "scriptSetDayState", scriptSetDayState },
	{ "scriptError", scriptError },
	{ "scriptGetTick", scriptGetTick },
	{ "scriptDoFile", scriptDoFile },
	{ "scriptMakeWebcall", scriptMakeWebcall },
	{ "scriptRegisterStoredProcedure", scriptRegisterStoredProcedure },
	{ "scriptUnRegisterStoredProcedure", scriptUnRegisterStoredProcedure },
	{ "scriptCallStoredProcedure", scriptCallStoredProcedure },
	{ "objectGenerate", objectGenerate },
	{ "objectMove", objectMove },
	{ "objectRotate", objectRotate },
	{ "objectScale", objectScale },
	{ "objectSetDrawDistance", objectSetDrawDistance },
	{ "objectGetId", objectGetId },
	{ "objectGetStartLocation", objectGetStartLocation },
	{ "objectSendMessage", objectSendMessage },
	{ "objectDelete", objectDelete },
	{ "objectMapEvents", objectMapEvents },
	{ "objectSetTexture", objectSetTexture },
	{ "objectSetAnimation", objectSetAnimation },
	{ "objectSetScript", objectSetScript },
	{ "objectSetParticle", objectSetParticle },
	{ "objectSetParticleForPlayer", objectSetParticleForPlayer },
	{ "objectRemoveParticle", objectRemoveParticle },
	{ "objectRemoveParticleForPlayer", objectRemoveParticleForPlayer },
	{ "objectSetFriction", objectSetFriction },
	{ "objectSetTexturePanning", objectSetTexturePanning },
	{ "objectShow", objectShow },
	{ "objectHide", objectHide },
	{ "objectSetEffectTrack", objectSetEffectTrack },
	{ "objectAddLabels", objectAddLabels },
	{ "objectAddLabelsForPlayer", objectAddLabelsForPlayer },
	{ "objectClearLabels", objectClearLabels },
	{ "objectClearLabelsForPlayer", objectClearLabelsForPlayer },
	{ "objectSetOutline", objectSetOutline },
	{ "objectSetFlash", objectSetFlash },
	{ "objectSetMedia", objectSetMedia },
	{ "objectSetMediaVolumeAndRadius", objectSetMediaVolumeAndRadius },
	{ "objectGetData", objectGetData },
	{ "objectGetOwner", objectGetOwner },
	{ "objectSetCollision", objectSetCollision },
	{ "soundGenerate", soundGenerate },
	{ "soundGenerateAmbient", soundGenerateAmbient },
	{ "soundMove", soundMove },
	{ "soundDelete", soundDelete },
	{ "soundStart", soundStart },
	{ "soundStop", soundStop },
	{ "soundConfig", soundConfig },
	{ "soundAddModifier", soundAddModifier },
	{ "soundAttachToObject", soundAttachToObject },
	{ "soundAttachToPlayer", soundAttachToPlayer },
	{ "soundPlayOnClient", soundPlayOnClient },
	{ "soundStopOnClient", soundStopOnClient },
	{ "zoneGetStats", zoneGetStats },
	{ "playerGotoUrl", playerGotoUrl },
	{ "objectLookAt", objectLookAt },
	{ "playerLookAt", playerLookAt },
	{ "objectGetNextPlayListItem", objectGetNextPlayListItem },
	{ "objectSetNextPlayListItem", objectSetNextPlayListItem },
	{ "objectEnumeratePlayList", objectEnumeratePlayList },
	{ "objectSetPlayList", objectSetPlayList },
	{ "playerRenderLabelsOnTop", playerRenderLabelsOnTop },
	{ "playerGetPermissions", playerGetPermissions },
	{ "objectSetMouseOver", objectSetMouseOver },
	{ "playerClosePresetMenu", playerClosePresetMenu },
	{ "playerOpenYesNoMenu", playerOpenYesNoMenu },
	{ "playerOpenKeyListenerMenu", playerOpenKeyListenerMenu },
	{ "playerOpenTextTimerMenu", playerOpenTextTimerMenu },
	{ "playerOpenButtonMenu", playerOpenButtonMenu },
	{ "playerOpenStatusMenu", playerOpenStatusMenu },
	{ "playerOpenListBoxMenu", playerOpenListBoxMenu },
	{ "playerMovePresetMenu", playerMovePresetMenu },
	{ "dictGenerate", dictGenerate },
	{ "dictClearValues", dictClearValues },
	{ "dictGetInfo", dictGetInfo },
	{ "dictGetAllKeys", dictGetAllKeys },
	{ "dictIterate", dictIterate },
	{ "classGetProperty", classGetProperty },
	{ "classSetProperty", classSetProperty },
	{ "scriptGC", scriptGC },
	{ "playerAddIndicator", playerAddIndicator },
	{ "playerClearIndicators", playerClearIndicators },
	{ "playerUpdateCoinHUD", playerUpdateCoinHUD },
	{ "playerUpdateHealthHUD", playerUpdateHealthHUD },
	{ "playerUpdateXPHUD", playerUpdateXPHUD },
	{ "playerAddHealthIndicator", playerAddHealthIndicator },
	{ "playerRemoveHealthIndicator", playerRemoveHealthIndicator },
	{ "playerAddItemToShop", playerAddItemToShop },
	{ "playerOpenShopMenu", playerOpenShopMenu },
	{ "playerShowStatusInfo", playerShowStatusInfo },
	{ "playerSpawnVehicle", playerSpawnVehicle },
	{ "playerModifyVehicle", playerModifyVehicle },
	{ "playerLeaveVehicle", &ScriptServerEngine::PlayerLeaveVehicle },
	{ "gameItemGetOwned", gameItemGetOwned },
	{ "gameItemGetOwnedByPlayer", gameItemGetOwnedByPlayer },
	{ "gameItemLoadAllByGame", gameItemLoadAllByGame },
	{ "gameItemDelete", gameItemDelete },
	{ "gameItemUpdate", gameItemUpdate },
	{ "gameItemPlace", gameItemPlace },
	{ "gameItemPlaceAsUser", gameItemPlaceAsUser },
	{ "gameItemDeletePlacement", gameItemDeletePlacement },
	{ "gameItemGetNextId", gameItemGetNextId },
	{ "scriptSavePlayerData", scriptSavePlayerData },
	{ "scriptLoadAllPlayerData", scriptLoadAllPlayerData },
	{ "scriptLoadPlayerData", scriptLoadPlayerData },
	{ "scriptDeletePlayerData", scriptDeletePlayerData },
	{ "scriptDeleteAllPlayerData", scriptDeleteAllPlayerData },
	{ "scriptSaveGameData", scriptSaveGameData },
	{ "scriptLoadAllGameData", scriptLoadAllGameData },
	{ "scriptLoadGameData", scriptLoadGameData },
	{ "scriptDeleteGameData", scriptDeleteGameData },
	{ "scriptDeleteAllGameData", scriptDeleteAllGameData },
	{ "gameGetFrameworkEnabled", gameGetFrameworkEnabled },
	{ "gameSetFrameworkEnabled", gameSetFrameworkEnabled },
	{ "gameItemSetPosRot", gameItemSetPosRot },
	{ "gameLoadGlobalConfig", gameLoadGlobalConfig },
	{ "gameInitFrameworkDB", gameInitFrameworkDB },
	{ "objectGetGameState", objectGetGameState },
	{ "playerGetGameState", playerGetGameState },
	{ "scriptRecordMetric", scriptRecordMetric },
	{ "scriptCallAsync", scriptCallAsync },
	{ "npcSpawn", npcSpawn },
	{ "gameGetItemInfo", &ScriptServerEngine::GameGetItemInfo },
	{ "getEffectDuration", &ScriptServerEngine::GetEffectDuration },
	{ "getParentZoneInfo", getParentZoneInfo },
	{ "getChildZoneIds", getChildZoneIds },
	{ "gameItemIsPlaced", &ScriptServerEngine::GameItemIsPlaced },
	{ "scriptBroadcastMessage", &ScriptServerEngine::ScriptBroadcastMessage },
	{ "scriptSetMessagePolicy", &ScriptServerEngine::ScriptSetMessagePolicy },
	{ "playerHasLinkedHome", &ScriptServerEngine::PlayerHasLinkedHome },
	{ "playerInLinkedHome", &ScriptServerEngine::PlayerInLinkedHome },
	{ "gameHasDynamicLink", &ScriptServerEngine::GameHasDynamicLink },
	{}
};

// Declare asynchronous APIs (APIs that return immediately and expect the asynchronous result to come in later with kgp_result event)
std::map<std::string, LuaAsyncFunction> s_scriptServerAsyncAPIs{
	{ "scriptMakeWebcall", scriptMakeWebcallAsync },
};

const luaL_Reg dictionaryMetamethods[] = {
	{ "__index", dictGetValue },
	{ "__newindex", dictSetValue },
	{ "__gc", dictRelease },
	{ NULL, NULL }
};

const luaL_Reg objectGameStateMetamethods[] = {
	{ "__index", objectGameStateGetValue },
	{ NULL, NULL }
};

const luaL_Reg playerGameStateMetamethods[] = {
	{ "__index", playerGameStateGetValue },
	{ NULL, NULL }
};

//Declare dictionary helper functions
static DictUserData* dictionaryFromStack(lua_State* L, int index);
static void dictionaryGetUserDataCache(lua_State* L, bool readonly);
static bool dictionaryGetUserDataFromCache(lua_State* L, const char* name, bool readonly);
static bool dictionaryCreateUserData(lua_State* L, const char* name, bool readonly, HDICT hDict, fast_recursive_mutex* hDictMutex);
static void dictionaryGetValueByKey(lua_State* L, DictUserData* dict, const char* key);
static void dictionarySetPrimitiveValueByKey(lua_State* L, DictUserData* dict, const char* key, int valIndex);
static bool dictionarySetNestedChildDictionaryByKey(lua_State* L, DictUserData* dict, const char* key, int valIndex);
static bool dictionarySetNestedTableByKey(lua_State* L, DictUserData* dict, const char* key, int valIndex);
static int dictionaryImportTable(lua_State* L, int tableIndex);

//Effect track helper functions
static void effectTrackArgError(const std::string& func, lua_State* L, UINT effectIndex, UINT argIndex, int expectedType, int actualType);
static int effectTrackGetNextArgType(const std::vector<int>& effect);
static bool luaGetNextEffect(const std::string& func, lua_State* L, std::vector<std::vector<int>>& allEffects, std::vector<std::string>& allStrings);
static bool luaGetEffectTrack(const std::string& func, lua_State* L, std::vector<std::vector<int>>& allEffects, std::vector<std::string>& allStrings);

//Other helper functions
static bool luaGetCurrentPlayerData(lua_State* L, ULONG clientId, const std::string& key, int expectedType);
static bool luaExplodeTable(lua_State* L, int tableIndex, const char* const* keys, bool popTable, bool allowNilValues);

//This holds the name and argument type list for stored procedures.
//It is populated via scriptRegisterStoredProcedure.
//First entry in the int vector should always be a string type for sp name.
typedef std::map<std::string, std::vector<int>> SPMap;
SPMap scriptStoredProcedures;

#define pSSE (ScriptServerEngine::Instance())
#define ASSERT_SSE_BOOL   \
	{ /*ASSERT(pSSE);*/   \
		if (!pSSE)        \
			return false; \
	}
#define ASSERT_SSE_0    \
	{ /*ASSERT(pSSE);*/ \
		if (!pSSE)      \
			return 0;   \
	}
#define ASSERT_SSE_1        \
	{ /*ASSERT(pSSE);*/     \
		if (!pSSE) {        \
			lua_pushnil(L); \
			return 1;       \
		}                   \
	}
#define NULL_CHK_RETURN(testVal, retVal)   \
	{                                      \
		if ((testVal) == NULL) {           \
			LogError(#testVal " is NULL"); \
			return (retVal);               \
		}                                  \
	}
#define NULL_CHK_RETURN_VOID(testVal)      \
	{                                      \
		if ((testVal) == NULL) {           \
			LogError(#testVal " is NULL"); \
			return;                        \
		}                                  \
	}
template <>
inline ScriptServerEngine* GetGlueGlobal<ScriptServerEngine>() {
	return ScriptServerEngine::Instance();
}

//Popular argument types
static int args_NUM[] = { LUA_TNUMBER };
static int args_STR[] = { LUA_TSTRING };
static int args_NUMNUM[] = { LUA_TNUMBER, LUA_TNUMBER };
static int args_NUMSTR[] = { LUA_TNUMBER, LUA_TSTRING };
static int args_STRNUM[] = { LUA_TSTRING, LUA_TNUMBER };
static int args_STRSTR[] = { LUA_TSTRING, LUA_TSTRING };
static int args_NUMSTRSTR[] = { LUA_TNUMBER, LUA_TSTRING, LUA_TSTRING };
static int args_STRNUMSTR[] = { LUA_TSTRING, LUA_TNUMBER, LUA_TSTRING };
static int args_STRSTRSTR[] = { LUA_TSTRING, LUA_TSTRING, LUA_TSTRING };
static int args_STRNUMSTRSTR[] = { LUA_TSTRING, LUA_TNUMBER, LUA_TSTRING, LUA_TSTRING };
#define ARGS_N_SIZE(x) (x), ARRAYSIZE(x)
#define ARGS_NONE NULL, 0 // No arguments
#define ARGS_NUM ARGS_N_SIZE(args_NUM) // any number
#define ARGS_STR ARGS_N_SIZE(args_STR) // any string
#define ARGS_NETID ARGS_N_SIZE(args_NUM) // client net ID
#define ARGS_PID ARGS_N_SIZE(args_NUM) // object placement ID
#define ARGS_GLID ARGS_N_SIZE(args_NUM) // global ID
#define ARGS_GIID ARGS_N_SIZE(args_NUM) // game item ID
#define ARGS_NETID_GLID ARGS_N_SIZE(args_NUMNUM) // client net ID, global ID
#define ARGS_NAME ARGS_N_SIZE(args_STR) // player name
#define ARGS_NAME_ATTR ARGS_N_SIZE(args_STRSTR) // player name, attribute name
#define ARGS_NAME_ATTR_VAL ARGS_N_SIZE(args_STRSTRSTR) // player name, attribute name, value
#define ARGS_ATTR ARGS_N_SIZE(args_STR) // attribute name
#define ARGS_ATTR_VAL ARGS_N_SIZE(args_STRSTR) // attribute name, value
#define ARGS_FLT ARGS_N_SIZE(args_NUM) // filter
#define ARGS_FLT_ATTR ARGS_N_SIZE(args_NUMSTR) // filter, attribute name
#define ARGS_FLT_ATTR_VAL ARGS_N_SIZE(args_NUMSTRSTR) // filter, attribute name, value
#define ARGS_NAME_FLT ARGS_N_SIZE(args_STRNUM) // player name, filter
#define ARGS_NAME_FLT_ATTR ARGS_N_SIZE(args_STRNUMSTR) // player name, filter, attribute name
#define ARGS_NAME_FLT_ATTR_VAL ARGS_N_SIZE(args_STRNUMSTRSTR) // player name, filter, attribute name, value

static std::string luaTypeToString(int type) {
	std::string typeString("");
	switch (type) {
		case LUA_TNIL:
			typeString += "nil";
			break;
		case LUA_TBOOLEAN:
			typeString += "boolean";
			break;
		case LUA_TLIGHTUSERDATA:
			typeString += "lightuserdata";
			break;
		case LUA_TNUMBER:
			typeString += "number";
			break;
		case LUA_TSTRING:
			typeString += "string";
			break;
		case LUA_TTABLE:
			typeString += "table";
			break;
		case LUA_TFUNCTION:
			typeString += "function";
			break;
		case LUA_TUSERDATA:
			typeString += "userdata";
			break;
		case LUA_TTHREAD:
			typeString += "thread";
			break;
	}
	return typeString;
}

// Assumption: LUA_TTYPE constants is a value between 0 to 31
static std::string luaTypeMaskToString(int typeMask) {
	std::string typeListString("");
	if (typeMask != 0) {
		for (int type = 0; typeMask != 0; type++) {
			if (typeMask == 0) {
				break;
			}

			int chk = typeMask & 1;
			typeMask >>= 1;

			if (chk) {
				if (!typeListString.empty()) {
					typeListString.append(typeMask == 0 ? " or " : ", ");
				}
				typeListString.append(luaTypeToString(type));
			}
		}
	}

	return typeListString;
}

static void stackDump(lua_State* L, const std::string& strTitle) {
	LogInfo("-----------------------------------------");
	LogInfo("Stack Dump - " << strTitle);
	const int top = lua_gettop(L);
	for (int i = 1; i <= top; i++) {
		// repeat for each level
		const int t = lua_type(L, i);

		switch (t) {
			case LUA_TSTRING:
				// strings
				LogInfo("'" << lua_tostring(L, i) << "'");
				break;

			case LUA_TBOOLEAN:
				// booleans
				LogInfo(lua_toboolean(L, i) ? "true" : "false");
				break;

			case LUA_TNUMBER:
				// numbers
				LogInfo(lua_tonumber(L, i));
				break;

			default:
				// other values
				LogInfo(lua_typename(L, t));
				break;
		}
	}
	// end the listing
	LogInfo("-----------------------------------------");
}

// DRF - DEPRECATED - Use LuaParseArgs() Instead!!!
static bool badArgumentsCheck(lua_State* L, const int* expectedArgumentTypes, int numArguments, std::string* errorString, int numRequiredArguments = -1) {
	//All arguments are required
	if (numRequiredArguments == -1)
		numRequiredArguments = numArguments;

	ASSERT(numRequiredArguments <= numArguments);

	int n = lua_gettop(L);
	if (n > numArguments || n < numRequiredArguments) {
		std::ostringstream errorStr;
		if (numRequiredArguments == numArguments) {
			errorStr << numArguments << " arguments expected, got " << n;
		} else {
			errorStr << numRequiredArguments << " ~ " << numArguments << " arguments expected, got " << n;
		}
		*errorString = errorStr.str();
		return false;
	}
	int index;
	for (index = 1; index < numArguments + 1 && index <= n; index++) {
		if (expectedArgumentTypes[index - 1] != lua_type(L, index)) {
			std::ostringstream errorStr;
			errorStr << "Argument " << index << " must be " << luaTypeToString(expectedArgumentTypes[index - 1]) << ", got " << luaTypeToString(lua_type(L, index));
			*errorString = errorStr.str();
			return false;
		}
	}
	return true;
}

// DRF - DEPRECATED - Use LuaParseArgs() Instead!!!
// Advanced argument list check: allow multiple types per argument
static bool badArgumentsCheckAdvanced(lua_State* L, const int* expectedArgumentTypeMasks, int numArguments, std::string* errorString, int numRequiredArguments = -1) {
	//All arguments are required
	if (numRequiredArguments == -1)
		numRequiredArguments = numArguments;

	int n = lua_gettop(L);
	if (n > numArguments || n < numRequiredArguments) {
		std::ostringstream errorStr;
		if (numRequiredArguments == numArguments) {
			errorStr << numArguments << " arguments expected, got " << n;
		} else {
			errorStr << numRequiredArguments << " ~ " << numArguments << " arguments expected, got " << n;
		}
		*errorString = errorStr.str();
		return false;
	}

	int index;
	for (index = 1; index < numArguments + 1 && index <= n; index++) {
		int typeMask = expectedArgumentTypeMasks[index - 1];
		int argTypeMask = 1 << lua_type(L, index);
		if ((typeMask & argTypeMask) == 0) {
			std::ostringstream errorStr;
			errorStr << "Argument " << index << " must be " << luaTypeMaskToString(typeMask) << ", got " << luaTypeToString(lua_type(L, index));
			*errorString = errorStr.str();
			return false;
		}
	}
	return true;
}

// Get a numeric value from lua table by key. Return true if succeeded. False: key not found, not a numeric value etc.
static bool getLuaTableValue(lua_State* L, const char* key, double& val, int tableIndex = -1, bool rawGet = false) {
	if (rawGet) {
		lua_pushstring(L, key);
		lua_rawget(L, tableIndex < 0 ? tableIndex - 1 : tableIndex);
	} else
		lua_getfield(L, tableIndex, key);

	if (!lua_isnumber(L, -1))
		return false;

	val = (double)lua_tonumber(L, -1);
	lua_pop(L, 1); /* remove value */
	return true;
}

// Get a string value from lua table by key. Return true if succeeded. False: key not found, not a string value etc.
static bool getLuaTableValue(lua_State* L, const char* key, std::string& val, int tableIndex = -1, bool rawGet = false) {
	if (rawGet) {
		lua_pushstring(L, key);
		lua_rawget(L, tableIndex < 0 ? tableIndex - 1 : tableIndex);
	} else
		lua_getfield(L, tableIndex, key);

	if (!lua_isstring(L, -1)) {
		return false;
	}

	const char* result = (const char*)lua_tostring(L, -1);
	if (result == NULL)
		return false;

	val = result;
	lua_pop(L, 1); /* remove value */
	return true;
}

// Get a numeric value from lua table by index. Return true if succeeded. False: key not found, not a numeric value etc.
static bool getLuaTableValue(lua_State* L, int index, double& val, int tableIndex = -1, bool rawGet = false) {
	if (rawGet)
		lua_rawgeti(L, tableIndex, index);
	else {
		lua_pushnumber(L, index);
		lua_gettable(L, tableIndex < 0 ? tableIndex - 1 : tableIndex);
	}

	if (!lua_isnumber(L, -1))
		return false;

	val = (double)lua_tonumber(L, -1);
	lua_pop(L, 1); /* remove value */
	return true;
}

// Get a string value from lua table by index. Return true if succeeded. False: key not found, not a string value etc.
static bool getLuaTableValue(lua_State* L, int index, std::string& val, int tableIndex = -1, bool rawGet = false) {
	if (rawGet)
		lua_rawgeti(L, tableIndex, index);
	else {
		lua_pushnumber(L, index);
		lua_gettable(L, tableIndex < 0 ? tableIndex - 1 : tableIndex);
	}

	if (!lua_isstring(L, -1))
		return false;

	const char* result = (const char*)lua_tostring(L, -1);
	if (result == NULL) {
		return false;
	}

	val = result;
	lua_pop(L, 1); /* remove value */
	return true;
}

/** DRF
* Signal bad arguments error.
*/
#define LuaParseArgsError(errStr) _LuaParseArgsError(__FUNCTION__, L, errStr)
static void _LuaParseArgsError(const std::string& func, lua_State* L, const std::string& errStr) {
	std::string errMsg;
	StrBuild(errMsg, func << " - " << errStr);
	if (pSSE && L)
		pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, errMsg.c_str()); // also logs error
}

enum LUA_ARG_TYPE {
	LUA_ARG_TYPE_NONE = LUA_TNONE, // -1
	LUA_ARG_TYPE_NIL = LUA_TNIL, // 0
	LUA_ARG_TYPE_BOOLEAN = LUA_TBOOLEAN, // 1
	LUA_ARG_TYPE_LIGHTUSERDATA = LUA_TLIGHTUSERDATA, // 2
	LUA_ARG_TYPE_DOUBLE = LUA_TNUMBER, // 3
	LUA_ARG_TYPE_STRING = LUA_TSTRING, // 4
	LUA_ARG_TYPE_TABLE = LUA_TTABLE, // 5
	LUA_ARG_TYPE_FUNCTION = LUA_TFUNCTION, // 6
	LUA_ARG_TYPE_USERDATA = LUA_TUSERDATA, // 7
	LUA_ARG_TYPE_THREAD = LUA_TTHREAD, // 8
	LUA_ARG_TYPE_INTEGER = 9, // drf - added
	LUA_ARG_TYPE_ULONG = 10, // drf - added
	LUA_ARG_TYPES = 11
};

/** DRF - !!! Optimize For Speed - Occurs For Every Script API Call !!!
* Parse Lua arguments scanf-like. Returns number of arguments successfully parsed or 0 on error.
*  'n' = LUA_TNIL -> (void* = NULL)
*  'b' = LUA_TBOOLEAN -> (bool)
*  'l' = LUA_TLIGHTUSERDATA (void*)
*  'd' = LUA_TNUMBER -> (double)
*  's' = LUA_TSTRING -> (const char*)
*  't' = LUA_TTABLE -> (TODO)
*  'f' = LUA_TFUNCTION -> (TODO)
*  'o' = LUA_TUSERDATA -> (void*)
*  'x' = LUA_TTHREAD -> (TODO)
*  'i' = LUA_TNUMBER -> (int)
*  'u' = LUA_TNUMBER -> (ULONG)
*  '?' = Optional Arg
*  '(...)' = Multi-Type Arg
*/
#include "stdarg.h"
#define LuaParseArgs(fmt, ...) _LuaParseArgs(__FUNCTION__, L, fmt, __VA_ARGS__)
static int _LuaParseArgs(const std::string& func, lua_State* L, const char* fmt, ...) {
	static const char* argTypes = "nbldstfoxiu";
	va_list argp;
	va_start(argp, fmt);

	// Get Number Of Lua Args
	int args = lua_gettop(L);

	// Parse Lua Args
	std::string* errStr = NULL;
	const char* fmtPtr = fmt;
	char fmtChar = *fmtPtr;
	bool optional = false;
	for (int arg = 1; arg <= args; ++arg) {
		// Get Next Lua Arg Type
		LUA_ARG_TYPE luaArgType = (LUA_ARG_TYPE)lua_type(L, arg);
		if ((luaArgType < 0) || (luaArgType >= LUA_ARG_TYPES)) {
			errStr = new std::string;
			StrBuild(*errStr, "ARG TYPE UNKNOWN - arg=" << arg << " argType=" << luaArgType << " fmt='" << fmt << "'");
			goto _LuaParseArgsFail;
		}

		// Match Lua Arg Type Against Format String
		bool match = false;
		for (; ((fmtChar = *fmtPtr) != 0) && !match; ++fmtPtr) {
			switch (fmtChar) {
				case '?':
					optional = true;
					continue;
				case 'b':
					match = (luaArgType == LUA_ARG_TYPE_BOOLEAN) || (luaArgType == LUA_ARG_TYPE_DOUBLE);
					if (match)
						luaArgType = LUA_ARG_TYPE_BOOLEAN;
					break;
				case 'i':
					match = (luaArgType == LUA_ARG_TYPE_DOUBLE);
					if (match)
						luaArgType = LUA_ARG_TYPE_INTEGER;
					break;
				case 'u':
					match = (luaArgType == LUA_ARG_TYPE_DOUBLE);
					if (match)
						luaArgType = LUA_ARG_TYPE_ULONG;
					break;
				default:
					match = (argTypes[luaArgType] == fmtChar);
					break;
			}
			if (!match)
				break;
		}

		// Format String Mismatch ?
		if (!match) {
			errStr = new std::string;
			StrBuild(*errStr, "ARG TYPE MISMATCH - arg=" << arg << " argType=" << argTypes[luaArgType] << " fmt='" << fmt << "'");
			goto _LuaParseArgsFail;
		}

		// Extract Arg Type
		switch (luaArgType) {
			case LUA_ARG_TYPE_NIL:
				(*va_arg(argp, void**)) = NULL;
				break;
			case LUA_ARG_TYPE_BOOLEAN:
				(*va_arg(argp, bool*)) = (lua_toboolean(L, arg) != 0);
				break;
			case LUA_ARG_TYPE_LIGHTUSERDATA:
				(*va_arg(argp, void**)) = (void*)lua_touserdata(L, arg);
				break;
			case LUA_ARG_TYPE_DOUBLE:
				(*va_arg(argp, double*)) = (double)lua_tonumber(L, arg);
				break;
			case LUA_ARG_TYPE_INTEGER:
				(*va_arg(argp, int*)) = (int)lua_tointeger(L, arg);
				break;
			case LUA_ARG_TYPE_ULONG:
				(*va_arg(argp, ULONG*)) = (ULONG)lua_tonumber(L, arg);
				break;
			case LUA_ARG_TYPE_STRING:
				(*va_arg(argp, const char**)) = (const char*)lua_tolstring(L, arg, NULL);
				break;
			case LUA_ARG_TYPE_USERDATA:
				(*va_arg(argp, void**)) = (void*)lua_touserdata(L, arg);
				break;
			default:
				errStr = new std::string;
				StrBuild(*errStr, "ARG TYPE UNSUPPORTED - arg=" << arg << " argType=" << argTypes[luaArgType] << " fmt='" << fmt << "'");
				goto _LuaParseArgsFail;
		}
	}

	// Insufficient Args ?
	if (!optional && fmtChar && (fmtChar != '?')) {
		errStr = new std::string;
		StrBuild(*errStr, "ARGS INSUFFICIENT - args=" << args << " fmt='" << fmt << "'");
		goto _LuaParseArgsFail;
	}

	// Success
	va_end(argp);
	return args;

_LuaParseArgsFail:

	// Failure
	_LuaParseArgsError(func, L, *errStr);
	delete errStr;
	va_end(argp);
	return 0;
}

/** DRF - LUA API
* int kgp.playerTell(clientId, str, objectTitle, isTell)
*/
int playerTell(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	const char* str;
	const char* objectTitle;
	bool isTell;
	if (!LuaParseArgs("ussb", &clientId, &str, &objectTitle, &isTell))
		return 0;

	pSSE->PlayerTellCallback(L, clientId, str, objectTitle, isTell);

	return 0;
}

// Effect track helper functions

// Report argument error at specific effect and arg index
static void effectTrackArgError(const std::string& func, lua_State* L, UINT effectIndex, UINT argIndex, int expectedType, int actualType) {
	std::stringstream ssErrMsg;
	ssErrMsg << "Argument at effect # " << (effectIndex + 1);
	if (argIndex != (UINT)-1) { // -1: the entire row
		ssErrMsg << ", index # " << (argIndex + 1);
	}

	ssErrMsg << " must be " << luaTypeToString(expectedType) << ", got " << luaTypeToString(actualType);
	_LuaParseArgsError(func, L, ssErrMsg.str());
}

// Get next effect arg type
static int effectTrackGetNextArgType(const std::vector<int>& effect) {
	if (!effect.empty()) {
		UINT argIndex = effect.size();
		EffectCode effectType = static_cast<EffectCode>(effect[0]);
		if (effectType == EffectCode::EFX_PARTICLE && argIndex == EFX_PTC_BONE) {
			return LUA_TSTRING;
		}
	}

	return LUA_TNUMBER;
}

// Read a single effect from lua stack and push into vector
static bool luaGetNextEffect(const std::string& func, lua_State* L, std::vector<std::vector<int>>& allEffects, std::vector<std::string>& allStrings) {
	if (!lua_istable(L, -1)) {
		// fatal error
		effectTrackArgError(func, L, allEffects.size(), (UINT)-1, LUA_TTABLE, lua_type(L, -1));
		return false;
	}

	//
	// Under that we have the data to configure the effects,
	// which actually depends on the effect.  Just iterating
	// over the values in the table probably isn't such a
	// good idea, because it's hard to validate the data.
	//
	// The alternative, if I have time, would be to use
	// specific key values on the lua side and check them
	// in here.
	//

	// Current effect
	std::vector<int> effect;
	EffectCode effectCode = EffectCode::EFX_NULL;

	lua_pushnil(L);
	while (lua_next(L, -2) != 0) {
		// Validate input
		int actualArgType = lua_type(L, -1);
		int expectArgType = effectTrackGetNextArgType(effect);
		if (actualArgType != expectArgType) {
			// Bad argument type
			effectTrackArgError(func, L, allEffects.size(), effect.size(), expectArgType, actualArgType);
			return false;
		}

		if (actualArgType == LUA_TNUMBER) {
			int argIndex = effect.size();

			// Scale x / y / z / vector values by 1000 for PARTICLE, OBJECT_MOVE and OBJECT_ROTATE effects
			int value;
			if ((effectCode == EffectCode::EFX_OBJECT_MOVE || effectCode == EffectCode::EFX_OBJECT_ROTATE) && argIndex >= EFX_MOVROT_X && argIndex <= EFX_MOVROT_Z ||
				effectCode == EffectCode::EFX_PARTICLE && argIndex >= EFX_PTC_OFV_X && argIndex <= EFX_PTC_UPD_Z)
				value = (int)(lua_tonumber(L, -1) * 1000);
			else
				value = (int)lua_tonumber(L, -1);

			// Record effect code
			if (argIndex == 0)
				effectCode = static_cast<EffectCode>(value);

			effect.push_back(value);

		} else if (actualArgType == LUA_TSTRING) {
			std::string value = lua_tostring(L, -1);
			allStrings.push_back(value);
			effect.push_back(allStrings.size() - 1); // I can use this to find the corresponding string.
		} else {
			// Should not be here
			jsAssert(false);
			effectTrackArgError(func, L, allEffects.size(), effect.size(), expectArgType, actualArgType);
			return false;
		}

		lua_pop(L, 1);
	}

	// Must have at least two arguments per effect: effect code and time offset
	if (effect.size() < EFX_NUM_REQUIRED_ARGS) {
		std::stringstream ssErrMsg;
		ssErrMsg << "Too few arguments (at least two) for effect # " << (allEffects.size() + 1);
		_LuaParseArgsError(func, L, ssErrMsg.str());
		return false;
	}

	// Sanity check
	if (effect.size() > EFX_MAX_ARGS) {
		std::stringstream ssErrMsg;
		ssErrMsg << "Too many arguments for effect # " << (allEffects.size() + 1);
		_LuaParseArgsError(func, L, ssErrMsg.str());
		return false;
	}

	if (allEffects.size() >= EFX_MAX_EFFECTS) {
		std::stringstream ssErrMsg;
		ssErrMsg << "Too many effects (max: " << EFX_MAX_EFFECTS << ")";
		_LuaParseArgsError(func, L, ssErrMsg.str());
		return false;
	}

	allEffects.push_back(effect);
	return true;
}

// Read the effect track from lua stack
static bool luaGetEffectTrack(const std::string& func, lua_State* L, std::vector<std::vector<int>>& allEffects, std::vector<std::string>& allStrings) {
	jsAssert(lua_type(L, -1) == LUA_TTABLE);
	lua_pushnil(L);

	// The top level table is the table of effects.
	while (lua_next(L, -2) != 0) {
		if (!luaGetNextEffect(func, L, allEffects, allStrings)) {
			return false;
		}

		lua_pop(L, 1);
	}

	return true;
}

int playerSetEffectTrack(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	static const int args[] = {
		LUA_TNUMBER, // client net ID
		LUA_TTABLE, // effect table of tables
	};

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Get arguments.
	ULONG clientId = (ULONG)lua_tonumber(L, 1);

	// Now process the table of tables that stores information on each effect.
	std::vector<std::vector<int>> allEffects;
	std::vector<std::string> allStrings;
	if (luaGetEffectTrack(__FUNCTION__, L, allEffects, allStrings)) {
		pSSE->SetEffectTrackCallback(L, clientId, ScriptClientEvent::PlayerSetEffectTrack, allEffects, allStrings);
	}

	return 0;
}

int objectSetEffectTrack(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	static const int args[] = {
		LUA_TNUMBER, // client Net ID
		LUA_TTABLE, // effect table of tables
	};

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Get arguments.
	ULONG objectId = (ULONG)lua_tonumber(L, 1);

	// Now process the table of tables that stores information on each effect.
	std::vector<std::vector<int>> allEffects;
	std::vector<std::string> allStrings;
	if (luaGetEffectTrack(__FUNCTION__, L, allEffects, allStrings)) {
		pSSE->SetEffectTrackCallback(L, objectId, ScriptClientEvent::ObjectSetEffectTrack, allEffects, allStrings);
	}

	return 0;
}

int scriptError(lua_State* L) {
	ASSERT_SSE_0;

	int errorCode;
	const char* errorStrParam;
	if (!LuaParseArgs("is", &errorCode, &errorStrParam))
		return 0;

	pSSE->ScriptErrorCallback(L, errorCode, errorStrParam);

	return 0;
}

int scriptGetTick(lua_State* L) {
	ASSERT_SSE_1;

	TimeMs timeMs = fTime::TimeMs();
	lua_pushnumber(L, (lua_Number)timeMs);
	return 1;
}

int scriptDoFile(lua_State* L) {
	ASSERT_SSE_1;

	const char* scriptName;
	if (!LuaParseArgs("s", &scriptName))
		return 0;

	std::string errMsg;
	bool result = pSSE->ScriptDoFileCallback(L, scriptName, errMsg);
	if (result)
		lua_pushnumber(L, 1);
	else {
		lua_pushnil(L);
		lua_pushstring(L, errMsg.c_str());
	}

	return 1;
}

int scriptMakeWebcallHelper(lua_State* L, bool async, const AsyncUserArg& userArgs) {
	ASSERT_SSE_1;

	const char* url;
	bool usePost = false; // optional
	if (!LuaParseArgs("s?b", &url, &usePost))
		return 0;

	if (async) {
		return pSSE->ScriptMakeWebcallAsync(L, url, usePost, userArgs).getLuaRC();
	} else {
		return pSSE->ScriptMakeWebcallCallback(L, url, usePost, userArgs).getLuaRC();
	}
}

int scriptMakeWebcall(lua_State* L) {
	return scriptMakeWebcallHelper(L, false, AsyncUserArg());
}

int scriptMakeWebcallAsync(lua_State* L, const AsyncUserArg& userArgs) {
	return scriptMakeWebcallHelper(L, true, userArgs);
}

int scriptRegisterStoredProcedure(lua_State* L) {
	int n = lua_gettop(L);

	const char* spName;
	if (!LuaParseArgs("s", &spName))
		return 0;

	std::vector<int> args;
	args.push_back(LUA_TSTRING); //for SP name.  lets us exercise badArgumentsCheck when the call is made.
	for (int i = 0; i < (n - 1); i++) {
		int a = (int)lua_tonumber(L, i + 2);
		args.push_back(a);
	}

	scriptStoredProcedures.insert(std::pair<std::string, std::vector<int>>(spName, args));

	return 0;
}

int scriptUnRegisterStoredProcedure(lua_State* L) {
	const char* spName;
	if (!LuaParseArgs("s", &spName))
		return 0;

	SPMap::iterator spmIter = scriptStoredProcedures.find(spName);
	if (spmIter != scriptStoredProcedures.end())
		scriptStoredProcedures.erase(spmIter);

	return 0;
}

int scriptCallStoredProcedure(lua_State* L) {
	ASSERT_SSE_1;

	const char* spName;
	if (!LuaParseArgs("s", &spName))
		return 0;

	SPMap::iterator spmIter = scriptStoredProcedures.find(std::string(spName));
	if (spmIter == scriptStoredProcedures.end()) {
		LuaParseArgsError("Stored procedure not found. Please register the stored procedure using scriptRegisterStoredProcedure.");
		return 0;
	}

	std::string errorStr;
	std::vector<int> args = spmIter->second;
	if (!badArgumentsCheck(L, &args[0], args.size(), &errorStr)) {
		LuaParseArgsError("");
		return 0; // number of return values
	}

	// Any return values from the stored procedure call will be held in retValues
	std::vector<std::vector<std::string>> retValues;
	bool success = pSSE->ScriptCallStoredProcedureCallback(L, args, retValues);
	if (!success)
		return 1; // drf - should return 0 with error message ?

	lua_newtable(L);
	int topMain = lua_gettop(L);
	for (int r = 0; r < (int)retValues.size(); r++) { //"rows"
		lua_newtable(L);
		int topSub = lua_gettop(L);
		for (int c = 0; c < (int)retValues[r].size(); c++) { //"columns"
			lua_pushstring(L, retValues[r][c].c_str());
			lua_rawseti(L, topSub, c + 1);
		}
		lua_rawseti(L, topMain, r + 1);
	}

	return 1;
}

int playerTeleport(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	double loc[3];
	double rot[3];
	if (!LuaParseArgs("udddddd", &clientId, &loc[0], &loc[1], &loc[2], &rot[0], &rot[1], &rot[2]))
		return 0;

	rot[1] = 0.f; //This is currently necessary. Players will have a distorted view otherwise.

	//If this happens players will not be visible due to a 0 vector
	if (rot[0] == 0.f && rot[2] == 0.f) {
		LuaParseArgsError("{rotX, rotZ} must not be equal to zero");
		lua_pushnil(L);
		return 1; // number of return values
	}

	int success = pSSE->PlayerTeleportCallback(L, clientId, loc[0], loc[1], loc[2], rot[0], rot[1], rot[2]);
	if (success)
		lua_pushnumber(L, success);
	else
		lua_pushnil(L);

	return 1;
}

int playerScale(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	double scale[3];
	if (!LuaParseArgs("uddd", &clientId, &scale[0], &scale[1], &scale[2]))
		return 0;

	if ((scale[0] < 0.1 || scale[1] < 0.1 || scale[2] < 0.1) || (scale[0] > 50.0 || scale[1] > 50.0 || scale[2] > 50.0)) {
		LuaParseArgsError("Scale must be between 0.1 and 50.0 in all axes.");
		return 0;
	}

	int result = pSSE->PlayerScaleCallback(L, clientId, scale[0], scale[1], scale[2]);
	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int playerGetObjectInfront(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	return pSSE->PlayerCastRayCallback(L, clientId).getLuaRC();
}

int playerGotoUrl(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	const char* url;
	if (!LuaParseArgs("us", &clientId, &url))
		return 0;

	int success = pSSE->PlayerGotoUrlCallback(L, clientId, url);
	if (success)
		lua_pushnumber(L, success);
	else
		lua_pushnil(L);

	return 1;
}

int playerSendEvent(lua_State* L) {
	ASSERT_SSE_0;

	bool bad_arguments = false;
	bool result = false;
	std::ostringstream errorMsg("");
	int arg_count = lua_gettop(L);
	if (arg_count < 1 || !lua_isnumber(L, 1)) {
		bad_arguments = true;
		errorMsg << "ERROR:: " << BAD_ARGUMENTS_ERROR_STR << " for " __FUNCTION__ ": At least 2 arguments must be provided";
	} else if (!lua_isnumber(L, 1)) {
		bad_arguments = true;
		errorMsg << "ERROR:: " << BAD_ARGUMENTS_ERROR_STR << " for " __FUNCTION__ ": Argument 1 must be a number, got " << luaTypeToString(lua_type(L, 1));
	} else {
		ULONG clientId = (ULONG)lua_tonumber(L, 1);

		std::vector<std::string> arg_vect;
		for (int i = 2; i < (arg_count + 1); i++) {
			if (lua_istable(L, i)) {
				lua_pushnil(L);
				while (lua_next(L, i) != 0) {
					const char* data_entry = lua_tolstring(L, -1, NULL);

					// send error back if any of the arguments are nil
					if (data_entry != NULL) {
						std::string entry(data_entry);
						arg_vect.push_back(entry);
					} else {
						bad_arguments = true;
						errorMsg << "ERROR:: " << BAD_ARGUMENTS_ERROR_STR << " for " __FUNCTION__ ": a provided parameter was not a string or number";
						break;
					}
					lua_pop(L, 1);
				}
			} else {
				size_t len;
				const char* sptr = lua_tolstring(L, i, &len);
				if (sptr != NULL) {
					std::string entry(sptr, len + 1); //get null terminator
					arg_vect.push_back(entry);
				} else {
					bad_arguments = true;
					errorMsg << "ERROR:: " << BAD_ARGUMENTS_ERROR_STR << " for " __FUNCTION__ ": a provided parameter was not a string or number";
					break;
				}
			}
		}

		if (!bad_arguments) {
			result = pSSE->PlayerSendEventCallback(L, clientId, arg_vect);
		}
	}

	if (bad_arguments) {
		pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, errorMsg.str().c_str());
	}

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int playerLoadMenu(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	const char* menuName;
	if (!LuaParseArgs("us", &clientId, &menuName))
		return 0;

	bool ret = pSSE->PlayerLoadMenuCallback(L, clientId, menuName);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int doPlaceItem(lua_State* L, int placementType, const char* /*APIName*/) {
	ASSERT_SSE_0;

	int glid;
	double loc[3];
	double rot[3];
	bool bufferCall = true; // optional
	if (!LuaParseArgs("idddddd?b", &glid, &loc[0], &loc[1], &loc[2], &rot[0], &rot[1], &rot[2], &bufferCall))
		return 0;

	// The only valid value for ry is 0 right now,
	// because we can only rotate in the y axis.
	rot[1] = 0.f;

	if (rot[0] == 0.f && rot[2] == 0.f) {
		LuaParseArgsError("{rotX, rotZ} must not be equal to zero");
		return 0; // number of return values
	}

	ULONG id = pSSE->ObjectGenerateCallback(L, (GLID)glid, loc[0], loc[1], loc[2], rot[0], rot[1], rot[2], placementType, bufferCall);
	if (id)
		lua_pushnumber(L, id);
	else
		lua_pushnil(L);

	return 1;
}

int objectGenerate(lua_State* L) {
	return doPlaceItem(L, Placement_DynObj, __FUNCTION__);
}

int objectDelete(lua_State* L) {
	ASSERT_SSE_0;

	int objId;
	if (!LuaParseArgs("i", &objId))
		return 0;

	bool result = pSSE->ObjectDeleteCallback(L, objId);
	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int objectRotate(lua_State* L) {
	ASSERT_SSE_0;

	int objId;
	double time;
	double rot[3];
	if (!LuaParseArgs("idddd", &objId, &time, &rot[0], &rot[1], &rot[2]))
		return 0;

	bool result = pSSE->ObjectRotateCallback(L, objId, time, rot[0], rot[1], rot[2]);
	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int objectMove(lua_State* L) {
	ASSERT_SSE_0;

	/** DRF - LUA API
	* int kgp.objectMove(objectId, timeMs, x, y, z, dx=0, dy=0, dz=0, ux=0, uy=0, uz=0, bufferCall=true)
	*/
	int objId;
	double timeMs;
	double accDur;
	double constVelDur;
	double accDistFrac;
	double constVelDistFrac;
	double x;
	double y;
	double z;
	double dx = 0; // optional
	double dy = 0; // optional
	double dz = 0; // optional
	double ux = 0; // optional
	double uy = 0; // optional
	double uz = 0; // optional
	bool bufferCall = true; // optional
	if (!LuaParseArgs("idddddddd?ddd?ddd?b", &objId, &timeMs, &accDur, &constVelDur, &accDistFrac, &constVelDistFrac, &x, &y, &z, &dx, &dy, &dz, &ux, &uy, &uz, &bufferCall))
		return 0;

	// Execute Callback
	bool result = pSSE->ObjectMoveCallback(L, objId, timeMs, accDur, constVelDur, accDistFrac, constVelDistFrac, x, y, z, dx, dy, dz, ux, uy, uz, bufferCall);
	result ? lua_pushnumber(L, 1) : lua_pushnil(L);
	return 1;
}

int objectScale(lua_State* L) {
	ASSERT_SSE_0;

	int objId;
	double scale;
	if (!LuaParseArgs("id", &objId, &scale))
		return 0;

	// DO only support uniform scaling, so although the events support it, I only use
	// the 'x' value and only expose a single argument to the script.
	bool result = pSSE->ObjectScaleCallback(L, objId, scale, scale, scale);
	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int objectSetDrawDistance(lua_State* L) {
	ASSERT_SSE_0;

	int objId;
	double distance;
	if (!LuaParseArgs("id", &objId, &distance))
		return 0;

	bool result = pSSE->ObjectSetDrawDistanceCallback(L, objId, distance);
	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int objectGetId(lua_State* L) {
	ASSERT_SSE_1;

	// Return Object Id
	ULONG id = pSSE->ObjectGetIdCallback(L);
	if (id)
		lua_pushnumber(L, id);
	else
		lua_pushnil(L);

	return 1;
}

int objectGetStartLocation(lua_State* L) {
	ASSERT_SSE_1;

	int objId;
	if (!LuaParseArgs("i", &objId))
		return 0;

	// Result storage
	double loc[3];
	double rot[3];
	if (!pSSE->ObjectGetStartLocationCallback(L, objId, loc[0], loc[1], loc[2], rot[0], rot[1], rot[2])) {
		lua_pushnil(L);
		return 1; //number of return values
	}
	lua_pushnumber(L, loc[0]);
	lua_pushnumber(L, loc[1]);
	lua_pushnumber(L, loc[2]);
	lua_pushnumber(L, rot[0]);
	lua_pushnumber(L, rot[1]);
	lua_pushnumber(L, rot[2]);
	return 6;
}

int objectSetCollision(lua_State* L) {
	ASSERT_SSE_1;

	int objId;
	int enable; // collision flag: 1 - true, 0 - false
	if (!LuaParseArgs("ii", &objId, &enable))
		return 0;

	if (pSSE->ObjectSetCollisionCallback(L, objId, enable != 0))
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int playerGetLocation(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	// Workaround for calling this API in kgp_depart handler
	if (luaGetCurrentPlayerData(L, clientId, KGPHANDLER_TEMPDATA_POSITION, LUA_TTABLE)) {
		// kgp_depart has saved position table in the registry - convert it to loose return values
		static const char* const positionKeys[] = { "x", "y", "z", "rx", "ry", "rz", NULL };
		if (luaExplodeTable(L, -1, positionKeys, true, false)) {
			lua_pushnumber(L, 1); // why an extra return value?
			return 7;
		}

		// Position table is corrupted
		LogWarn("Current player position table from Lua registry is corrupted: " << clientId);

		lua_pop(L, 1); // pop it
		// Continue with standard method
	}

	try {
		std::pair<Vector3f, Vector3f> loc = LocationRegistry::singleton().getPlayerLocation(clientId);
		lua_pushnumber(L, loc.first.x);
		lua_pushnumber(L, loc.first.y);
		lua_pushnumber(L, loc.first.z);
		lua_pushnumber(L, loc.second.x);
		lua_pushnumber(L, loc.second.y);
		lua_pushnumber(L, loc.second.z);
		lua_pushnumber(L, 1);
		return 7;
	} catch (const KeyNotFoundException&) {
		// do nothing.  just let legacy logic run it's course
	}
	return pSSE->PlayerGetLocationCallback(L, clientId).getLuaRC();
}

int playerGetName(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	// Workaround for calling this API in kgp_depart handler
	if (luaGetCurrentPlayerData(L, clientId, KGPHANDLER_TEMPDATA_PLAYERNAME, LUA_TSTRING)) {
		// kgp_depart has saved the value in the registry - use it
		return 1;
	}

	std::string playerName = pSSE->PlayerGetNameCallback(L, clientId);
	if (!playerName.empty())
		lua_pushstring(L, playerName.c_str());
	else
		lua_pushnil(L);

	return 1;
}

int playerGetTitle(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	// Workaround for calling this API in kgp_depart handler
	if (luaGetCurrentPlayerData(L, clientId, KGPHANDLER_TEMPDATA_PLAYERTITLE, LUA_TSTRING)) {
		// kgp_depart has saved the value in the registry - use it
		return 1;
	}

	std::string playerTitle = pSSE->PlayerGetTitleCallback(L, clientId);
	if (!playerTitle.empty())
		lua_pushstring(L, playerTitle.c_str());
	else
		lua_pushnil(L);

	return 1;
}

int playerGetPermissions(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	// Workaround for calling this API in kgp_depart handler
	if (luaGetCurrentPlayerData(L, clientId, KGPHANDLER_TEMPDATA_PLAYERPERM, LUA_TNUMBER)) {
		// kgp_depart has saved the value in the registry - use it
		return 1;
	}

	int playerPermissions = pSSE->PlayerGetPermissionsCallback(L, clientId);
	lua_pushnumber(L, playerPermissions);

	return 1;
}

int playerSaveData(lua_State* L) {
	ASSERT_SSE_0;
	assert(false);
	pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "playerSaveData is deprecated");
	return 0;
}

int playerLoadAllData(lua_State* L) {
	ASSERT_SSE_0;
	assert(false);
	pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "playerLoadAllData is deprecated");
	return 0;
}

int playerLoadData(lua_State* L) {
	ASSERT_SSE_0;
	assert(false);
	pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "playerLoadData is deprecated");
	return 0;
}

int playerDeleteData(lua_State* L) {
	ASSERT_SSE_0;
	assert(false);
	pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "playerDeleteData is deprecated");
	return 0;
}

int playerDeleteAllData(lua_State* L) {
	ASSERT_SSE_0;
	assert(false);
	pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "playerDeleteAllData is deprecated");
	return 0;
}

int playerUseItem(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int globalId;
	if (!LuaParseArgs("ui", &clientId, &globalId))
		return 0;

	return pSSE->PlayerUseItemCallback(L, clientId, globalId).getLuaRC();
}

int playerEquipItem(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int globalId;
	if (!LuaParseArgs("ui", &clientId, &globalId))
		return 0;

	LogInfo("[" << clientId << "] glid=" << globalId);

	return pSSE->PlayerEquipItemCallback(L, clientId, globalId).getLuaRC();
}

int playerUnEquipItem(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int globalId;
	if (!LuaParseArgs("ui", &clientId, &globalId))
		return 0;

	LogInfo("[" << clientId << "] glid=" << globalId);

	return pSSE->PlayerUnEquipItemCallback(L, clientId, globalId).getLuaRC();
}

// DRF - Added
int playerSaveEquipped(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	const char* outfitName = NULL;
	if (!LuaParseArgs("us", &clientId, &outfitName))
		return 0;
	std::string outfitNameStr = outfitName ? outfitName : "";

	LogInfo("[" << clientId << "] outfitName='" << outfitNameStr << "'");

	return pSSE->PlayerSaveEquippedCallback(L, clientId, outfitNameStr).getLuaRC();
}

// DRF - Added
int playerGetEquipped(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	LogInfo("[" << clientId << "]");

	return pSSE->PlayerGetEquippedCallback(L, clientId).getLuaRC();
}

int playerSetEquipped(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	const char* glids = NULL; // optional
	const char* outfitName = NULL; // optional
	if (!LuaParseArgs("u?ss", &clientId, &glids, &outfitName))
		return 0;
	std::string glidsStr = glids ? glids : "";
	std::string outfitNameStr = outfitName ? outfitName : "";

	LogInfo("[" << clientId << "] glids='" << glidsStr << "' outfitName='" << outfitNameStr << "'");

	return pSSE->PlayerSetEquippedCallback(L, clientId, glidsStr, outfitNameStr).getLuaRC();
}

int playerRemoveAllAccessories(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	LogInfo("[" << clientId << "]");

	return pSSE->PlayerRemoveAllAccessories(L, clientId).getLuaRC();
}

int playerCheckInventory(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int globalId;
	if (!LuaParseArgs("ui", &clientId, &globalId))
		return 0;

	return pSSE->PlayerCheckInventoryCallback(L, clientId, globalId).getLuaRC();
}

int playerAddItem(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int globalId;
	int quantity;
	if (!LuaParseArgs("uii", &clientId, &globalId, &quantity))
		return 0;

	return pSSE->PlayerAddItemCallback(L, clientId, globalId, quantity).getLuaRC();
}

int playerJoinInstance(lua_State* L) {
	ASSERT_SSE_0;

	int instanceId;
	ULONG clientId;
	if (!LuaParseArgs("iu", &instanceId, &clientId))
		return 0;

	return pSSE->PlayerJoinInstanceCallback(L, instanceId, clientId).getLuaRC();
}

int playerSetEquippedItemAnimation(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	ULONG glidItem;
	ULONG glidAnim;
	if (!LuaParseArgs("uuu", &clientId, &glidItem, &glidAnim))
		return 0;

	pSSE->PlayerSetEquippedItemAnimationCallback(L, clientId, glidItem, glidAnim);
	return 0;
}

int playerSetAnimation(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	ULONG glidAnim;
	if (!LuaParseArgs("uu", &clientId, &glidAnim))
		return 0;

	pSSE->PlayerSetAnimationCallback(L, clientId, glidAnim);
	return 0;
}

int playerDefineAnimation(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	ULONG glidStand = GLID_INVALID; // optional
	ULONG glidRun = GLID_INVALID; // optional
	ULONG glidWalk = GLID_INVALID; // optional
	ULONG glidJump = GLID_INVALID; // optional
	if (!LuaParseArgs("u?uuuu", &clientId, &glidStand, &glidRun, &glidWalk, &glidJump))
		return 0;

	pSSE->PlayerDefineAnimationCallback(L, clientId, glidStand, glidRun, glidWalk, glidJump);
	return 0;
}

int playerSetPhysics(lua_State* L) {
	ASSERT_SSE_0;

	bool bad_args = false;
	bool result = false;
	std::vector<int> arg_vect;
	ULONG clientId = 0;

	// Check arguments.
	int n = lua_gettop(L);
	if (n != 2 ||
		!lua_isnumber(L, 1) ||
		!lua_istable(L, 2)) {
		bad_args = true;
	} else {
		clientId = (ULONG)lua_tonumber(L, 1);
		int key = 0;
		double value = 0;
		int intValue = 0;
		lua_pushnil(L);
		while (lua_next(L, 2) != 0) {
			if (!lua_isnumber(L, -2) || !lua_isnumber(L, -1)) {
				lua_pop(L, 1);
				continue;
			}

			key = (int)lua_tonumber(L, -2);
			value = lua_tonumber(L, -1);

			switch (key) { //make default case float conversion since there are so many.
				case MOVEMENTCAPSIDS_CURROTATIONVECT: //Dump these since we dont want to support them
				case MOVEMENTCAPSIDS_CURVELOCITYVECT: //and they probably dont work anyway.
				case MOVEMENTCAPSIDS_LASTPOSITION: {
					lua_pop(L, 1);
					continue;
				}
				case MOVEMENTCAPSIDS_AUTODECEL:
				case MOVEMENTCAPSIDS_STANDVERTICALTOSURFACE:
				case MOVEMENTCAPSIDS_DOGRAVITY:
				case MOVEMENTCAPSIDS_INVERTMOUSE:
				case MOVEMENTCAPSIDS_AUTODECELY:
				case MOVEMENTCAPSIDS_AUTODECELZ:
				case MOVEMENTCAPSIDS_JUMPRECOVERTIME:
				case MOVEMENTCAPSIDS_BISWALKING:
					intValue = (int)value;
					break;
				default:
					intValue = (int)(value * 1000);
					break;
			}

			arg_vect.push_back(key);
			arg_vect.push_back(intValue);

			lua_pop(L, 1);
		}
	}

	if (!arg_vect.empty() && ((arg_vect.size() % 2) == 0)) {
		result = pSSE->PlayerSetPhysics(L, clientId, arg_vect);
	} else
		bad_args = true;

	if (bad_args) {
		LuaParseArgsError("");
	}

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int playerGetGender(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	// Workaround for calling this API in kgp_depart handler
	if (luaGetCurrentPlayerData(L, clientId, KGPHANDLER_TEMPDATA_PLAYERGENDER, LUA_TSTRING)) {
		// kgp_depart has saved the value in the registry - use it
		return 1;
	}

	char gender = pSSE->PlayerGetGender(L, clientId);
	if (gender != 0) {
		char szGender[2];
		szGender[0] = gender;
		szGender[1] = 0;
		lua_pushstring(L, szGender);
	} else {
		lua_pushnil(L);
	}

	return 1;
}

int playerSetTitle(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	const char* title;
	if (!LuaParseArgs("us", &clientId, &title))
		return 0;

	bool ret = pSSE->PlayerSetTitleCallback(L, clientId, title);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int playerSetName(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	const char* name;
	if (!LuaParseArgs("us", &clientId, &name))
		return 0;

	bool ret = pSSE->PlayerSetNameCallback(L, clientId, name);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int playerMove(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	double loc[3];
	double rot[3];
	double speed;
	int anim;
	int timeout;
	int recalcInterval;
	int aiMode;
	bool noPlayerControl;
	bool noPhysics;
	if (!LuaParseArgs("udddddddiiiibb",
			&clientId,
			&loc[0], &loc[1], &loc[2],
			&rot[0], &rot[1], &rot[2],
			&speed, &anim, &timeout, &recalcInterval, &aiMode,
			&noPlayerControl, &noPhysics))
		return 0;

	bool ret = pSSE->PlayerMove(L,
		clientId,
		loc[0], loc[1], loc[2],
		rot[0], rot[1], rot[2],
		speed, anim, timeout, recalcInterval, aiMode,
		noPlayerControl, noPhysics);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int gameSaveData(lua_State* L) {
	ASSERT_SSE_0;
	assert(false);
	pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "gameSaveData is deprecated");
	return 0;
}

int gameLoadAllData(lua_State* L) {
	ASSERT_SSE_0;
	assert(false);
	pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "gameLoadAllData is deprecated");
	return 0;
}

int gameLoadData(lua_State* L) {
	ASSERT_SSE_0;
	assert(false);
	pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "gameLoadData is deprecated");
	return 0;
}

int gameDeleteData(lua_State* L) {
	ASSERT_SSE_0;
	assert(false);
	pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "gameDeleteData is deprecated");
	return 0;
}

int gameDeleteAllData(lua_State* L) {
	ASSERT_SSE_0;
	assert(false);
	pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "gameDeleteAllData is deprecated");
	return 0;
}

int gameGetTimerFrequency(lua_State* L) {
	ASSERT_SSE_0;

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_NONE, &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	unsigned long frequency = 0;
	frequency = pSSE->ScriptGetTimerFrequencyCallback(L);
	if (frequency)
		lua_pushnumber(L, frequency);
	else
		lua_pushnil(L);

	return 1;
}

int gameGetCurrentTime(lua_State* L) {
	double secondsEpoch = GetCurrentEpochTime();
	lua_pushnumber(L, secondsEpoch);
	return 1;
}

int gameGetCurrentLocalTime(lua_State* L) {
	double secondsEpoch = GetCurrentLocalEpochTime();
	lua_pushnumber(L, secondsEpoch);
	return 1;
}

int gameGetCurrentTick(lua_State* L) {
	TimeMs timeMs = fTime::TimeMs();
	lua_pushnumber(L, timeMs);
	return 1;
}

int gameGetCurrentTimeStamp(lua_State* L) {
	std::string date = GetCurrentTimeStamp();
	lua_pushstring(L, date.c_str());
	return 1;
}

int gameDateToEpochTime(lua_State* L) {
	ASSERT_SSE_0;

	double year, month, day, hour, minute, second, epochTime;
	if (!LuaParseArgs("dddddd", &year, &month, &day, &hour, &minute, &second))
		return 0;

	second++; //result returned is one second before passed in value

	epochTime = DateToEpochTime((WORD)year, (WORD)month, (WORD)day, (WORD)hour, (WORD)minute, (WORD)second, (WORD)0);
	lua_pushnumber(L, epochTime);
	return 1;
}

int gameLogMessage(lua_State* L) {
	ASSERT_SSE_0;

	const char* logMessage;
	if (!LuaParseArgs("s", &logMessage))
		return 0;

	bool result = pSSE->ScriptLogMessageCallback(L, logMessage);
	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int gameGetCurrentZone(lua_State* L) {
	ASSERT_SSE_0;

	int result = pSSE->ScriptGetCurrentZoneCallback(L);
	if (result)
		lua_pushnumber(L, result);
	else
		lua_pushnil(L);

	return 1;
}

int getParentZoneInfo(lua_State* L) {
	ASSERT_SSE_1;

	ULONG zoneInstanceId, zoneType;
	if (!LuaParseArgs("uu", &zoneInstanceId, &zoneType))
		return 0;

	unsigned int parentZoneInstanceId = 0;
	unsigned int parentZoneIndex = 0;
	unsigned int parentZoneType = 0;
	unsigned int parentCommunityId = 0;
	bool success = pSSE->GetParentZoneInfoCallback(L, parentZoneInstanceId, parentZoneIndex, parentZoneType, parentCommunityId, zoneInstanceId, zoneType);
	if (success) {
		lua_pushnumber(L, parentZoneInstanceId);
		lua_pushnumber(L, parentZoneIndex);
		lua_pushnumber(L, parentZoneType);
		lua_pushnumber(L, parentCommunityId);
	} else {
		lua_pushnil(L); // error
		return 1;
	}

	return 4;
}

int getChildZoneIds(lua_State* L) {
	ASSERT_SSE_0;

	ULONG zoneInstanceId, zoneType;
	if (!LuaParseArgs("uu", &zoneInstanceId, &zoneType))
		return 0;

	std::vector<std::vector<ULONG>> childZoneIds;
	bool res = pSSE->GetChildZoneIds(L, childZoneIds, zoneInstanceId, zoneType);
	if (res && !childZoneIds.empty()) {
		lua_newtable(L);
		int topMain = lua_gettop(L); // table of script data tables
		for (unsigned int i = 0; i < childZoneIds.size(); i++) {
			lua_newtable(L); // the new table I want at that index (i.e. a table of script data)
			int topSub = lua_gettop(L);

			lua_pushnumber(L, childZoneIds[i][0]);
			lua_setfield(L, topSub, "zoneInstanceId");

			lua_pushnumber(L, childZoneIds[i][1]);
			lua_setfield(L, topSub, "zoneIndex");

			lua_pushnumber(L, childZoneIds[i][2]);
			lua_setfield(L, topSub, "zoneType");

			// add the script table to the main table
			lua_rawseti(L, topMain, i);
		}
	} else {
		lua_pushnil(L);
	}
	return 1;
}

int gameGetZonesInfo(lua_State* L) {
	ASSERT_SSE_0;

	return pSSE->ScriptGetZonesInfoCallback(L).getLuaRC();
}

int gameGetInstanceInfo(lua_State* L) {
	ASSERT_SSE_0;

	int instanceId;
	if (!LuaParseArgs("i", &instanceId))
		return 0;

	return pSSE->ScriptGetInstanceInfoCallback(L, instanceId).getLuaRC();
}

int gameCreateInstance(lua_State* L) {
	ASSERT_SSE_0;

	ULONG zoneId;
	ULONG clientId;
	if (!LuaParseArgs("uu", &zoneId, &clientId))
		return 0;

	return pSSE->ScriptCreateInstanceCallback(L, zoneId, clientId).getLuaRC();
}

int gameLockInstance(lua_State* L) {
	ASSERT_SSE_0;

	int instanceId;
	bool lock;
	if (!LuaParseArgs("ib", &instanceId, &lock))
		return 0;

	return pSSE->ScriptLockInstanceCallback(L, instanceId, lock).getLuaRC();
}

int gameGetCurrentGameId(lua_State* L) {
	ASSERT_SSE_0;

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_NONE, &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	long gameId = -1;

	bool result = pSSE->ScriptGetCurrentGameIdCallback(L, gameId);

	lua_pushnumber(L, gameId);
	lua_pushnumber(L, result);

	return 2;
}

int gameGetCurrentInstance(lua_State* L) {
	ASSERT_SSE_0;

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_NONE, &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	long zoneInstance = -1;
	long zoneId = -1;
	long zoneType = -1;

	bool result = pSSE->ScriptGetCurrentInstanceCallback(L, zoneInstance, zoneId, zoneType);

	lua_pushnumber(L, zoneInstance);
	lua_pushnumber(L, zoneId);
	lua_pushnumber(L, zoneType);
	lua_pushnumber(L, result);

	return 4;
}

int scriptSetEnvironment(lua_State* L) {
	ASSERT_SSE_0;

	bool bad_args = false;
	bool result = false;
	std::vector<double> arg_vect;

	// Check arguments.
	int n = lua_gettop(L);
	if (n != 1 || !lua_istable(L, 1)) {
		bad_args = true;
	} else {
		double key = 0;
		double value = 0;
		lua_pushnil(L);
		while (lua_next(L, 1) != 0) { //Table of attributes and values
			if (!lua_isnumber(L, -2) ||
				!(lua_isnumber(L, -1) || lua_istable(L, -1))) {
				lua_pop(L, 1);
				continue;
			}

			key = lua_tonumber(L, -2);
			arg_vect.push_back(key);

			switch ((ULONG)key) {
				case ENVIRONMENTIDS_SUNPOSITION: //GetSet Positions
				case ENVIRONMENTIDS_AMBIENTLIGHTREDDAY: //GetSet RGBA
				case ENVIRONMENTIDS_SUNRED:
				case ENVIRONMENTIDS_FOGRED:
				case ENVIRONMENTIDS_BACKRED: {
					if (lua_istable(L, -1)) {
						double subkey = 0;
						double subvalue = 0;
						int count = 0;
						lua_pushnil(L);
						while (lua_next(L, -2) != 0) { //Table containing rgb or xyz data
							if (!lua_isnumber(L, -1)) {
								lua_pop(L, 1);
								continue;
							}
							if (!lua_isnumber(L, -2)) {
								lua_pop(L, 1);
								continue;
							}

							subkey = lua_tonumber(L, -2);
							subvalue = lua_tonumber(L, -1);
							arg_vect.push_back(subvalue);
							count++;
							lua_pop(L, 1);
						}

						if (count != 3) { //must be exactly 3 values (rgb or xyz)
							for (int i = 0; i < count; i++) //remove values
								arg_vect.pop_back();

							arg_vect.pop_back(); //remove the key
						}
					}
					break;
				}
				case ENVIRONMENTIDS_AMBIENTLIGHTON:
				case ENVIRONMENTIDS_SUNLIGHTON:
				case ENVIRONMENTIDS_FOGON:
				case ENVIRONMENTIDS_LOCKBACKCOLOR:
				case ENVIRONMENTIDS_FOGSTART:
				case ENVIRONMENTIDS_FOGRANGE:
				case ENVIRONMENTIDS_ENABLETIMESYSTEM:
				case ENVIRONMENTIDS_GRAVITY:
				case ENVIRONMENTIDS_SEALEVEL:
				case ENVIRONMENTIDS_SEAVISCUSDRAG: {
					value = lua_tonumber(L, -1);
					arg_vect.push_back(value);
					break;
				}
				default: {
					bad_args = true;
					break;
				}
			}
			lua_pop(L, 1);
		}
	}

	if (bad_args || arg_vect.empty()) {
		LuaParseArgsError("");
	} else
		result = pSSE->ScriptSetEnvironmentCallback(L, arg_vect);

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int scriptSetDayState(lua_State* L) {
	ASSERT_SSE_0;

	bool bad_args = false;
	bool result = false;
	std::vector<double> arg_vect;

	// Check arguments.
	int n = lua_gettop(L);
	if (n != 1 || !lua_istable(L, 1)) {
		bad_args = true;
	} else {
		double key = 0;
		double value = 0;
		lua_pushnil(L);
		while (lua_next(L, 1) != 0) { //Table of attributes and values
			if (!lua_isnumber(L, -2) ||
				!(lua_isnumber(L, -1) || lua_istable(L, -1))) {
				lua_pop(L, 1);
				continue;
			}

			key = lua_tonumber(L, -2);
			arg_vect.push_back(key);

			switch ((ULONG)key) {
				case DAYSTATEIDS_FOGCOLORRED: //GetSet RGBA
				case DAYSTATEIDS_SUNCOLORRED:
				case DAYSTATEIDS_AMBIENTCOLORRED: {
					if (lua_istable(L, -1)) {
						double subkey = 0;
						double subvalue = 0;
						int count = 0;
						lua_pushnil(L);
						while (lua_next(L, -2) != 0) { //Table containing rgb or xyz data
							if (!lua_isnumber(L, -1)) {
								lua_pop(L, 1);
								continue;
							}
							if (!lua_isnumber(L, -2)) {
								lua_pop(L, 1);
								continue;
							}

							subkey = lua_tonumber(L, -2);
							subvalue = lua_tonumber(L, -1);
							arg_vect.push_back(subvalue);
							count++;
							lua_pop(L, 1);
						}

						if (count != 3) { //must be exactly 3 values (rgb or xyz)
							for (int i = 0; i < count; i++) //remove values
								arg_vect.pop_back();

							arg_vect.pop_back(); //remove the key
						}
					}
					break;
				}
				case DAYSTATEIDS_DURATION:
				case DAYSTATEIDS_FOGSTARTRANGE:
				case DAYSTATEIDS_FOGENDRANGE:
				case DAYSTATEIDS_SUNROTATIONX:
				case DAYSTATEIDS_SUNROTATIONY:
				case DAYSTATEIDS_SUNDISTANCE: {
					value = lua_tonumber(L, -1);
					arg_vect.push_back(value);
					break;
				}
				default: {
					bad_args = true;
					break;
				}
			}
			lua_pop(L, 1);
		}
	}

	if (bad_args || arg_vect.empty()) {
		LuaParseArgsError("");
	} else
		result = pSSE->ScriptSetDayStateCallback(L, arg_vect);

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int objectSendMessage(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objId;
	const char* str;
	ULONG delay = 0; // optional
	if (!LuaParseArgs("us?u", &objId, &str, &delay))
		return 0;

	pSSE->ObjectSendMessageCallback(L, objId, str, delay);

	return 0;
}

int objectMapEvents(lua_State* L) {
	ASSERT_SSE_1;

	ULONG objId;
	if (!LuaParseArgs("u", &objId))
		return 0;

	bool result = pSSE->ObjectMapEventsCallback(L, objId);
	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int zoneGetStats(lua_State* L) {
	ASSERT_SSE_0;

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_NONE, &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	unsigned int numberOfScripts;
	unsigned long totalMemoryUsed;
	std::vector<ScriptRuntimeData> scripts;
	pSSE->ZoneGetStatsCallback(L, numberOfScripts, totalMemoryUsed, scripts);

	ASSERT(numberOfScripts == scripts.size());

	lua_pushnumber(L, numberOfScripts);
	lua_pushnumber(L, totalMemoryUsed);
	lua_newtable(L);
	int topMain = lua_gettop(L); // table of script data tables
	for (int i = 0; i < (int)scripts.size(); i++) {
		lua_newtable(L); // the new table I want at that index (i.e. a table of script data)
		int topSub = lua_gettop(L);

		lua_pushstring(L, scripts[i].m_name.c_str());
		lua_setfield(L, topSub, "Name");
		//
		// I want to add to this table the name of the script, its current state, its total life time and the time it's spent in various other states
		//
		lua_pushnumber(L, scripts[i].m_state);
		lua_setfield(L, topSub, "CurrentState");

		lua_pushnumber(L, scripts[i].m_lifeTime);
		lua_setfield(L, topSub, "TotalTime");

		lua_pushnumber(L, scripts[i].m_readyTime);
		lua_setfield(L, topSub, "TimeReady");

		lua_pushnumber(L, scripts[i].m_idleTime);
		lua_setfield(L, topSub, "TimeIdle");

		lua_pushnumber(L, scripts[i].m_blockedTime);
		lua_setfield(L, topSub, "TimeBlocked");

		// add the script table to the main table
		lua_rawseti(L, topMain, scripts[i].m_placementId); // use the placement id as the key (because it's unique), then add the attributes into the sub table
	}
	return 3;
}

int objectSetAnimation(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objId;
	int animGlidSpecial; // <0 = reverse animation
	int animLoopCtl = (int)eAnimLoopCtl::Default; // optional - 0=default, 1=loop off, 2=loop on
	bool elapseCallback = false;
	bool bufferCall = true; // optional
	if (!LuaParseArgs("ui?ibb", &objId, &animGlidSpecial, &animLoopCtl, &elapseCallback, &bufferCall))
		return 0;

	pSSE->ObjectSetAnimationCallback(L, objId, (GLID)animGlidSpecial, (eAnimLoopCtl)animLoopCtl, elapseCallback, bufferCall);

	return 0;
}

int objectSetScript(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objId;
	const char *filename, *arg = NULL;
	if (!LuaParseArgs("us?s", &objId, &filename, &arg))
		return 0;

	pSSE->ObjectSetScriptCallback(L, objId, SAVEDSCRIPTDIR, filename, arg);

	return 0;
}

int objectSetTexture(lua_State* L) {
	ASSERT_SSE_1;

	ULONG objId;
	const char* textureURL;
	bool bufferCall = true; // optional
	if (!LuaParseArgs("us?b", &objId, &textureURL, &bufferCall))
		return 0;

	bool result = pSSE->ObjectSetTextureCallback(L, objId, textureURL, bufferCall);
	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int soundStart(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objId;
	int delayMs = 0; // optional
	bool bufferCall = true; // optional
	if (!LuaParseArgs("u?ib", &objId, &delayMs, &bufferCall))
		return 0;

	std::vector<int> soundargs;
	soundargs.push_back(delayMs);
	pSSE->ObjectControlCallback(L, objId, 0, SOUND_START, &soundargs, bufferCall);

	return 0;
}

int soundStop(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objId;
	int delayMs = 0; // optional
	if (!LuaParseArgs("u?i", &objId, &delayMs))
		return 0;

	std::vector<int> soundargs;
	soundargs.push_back(delayMs);
	pSSE->ObjectControlCallback(L, objId, 0, SOUND_STOP, &soundargs);

	return 0;
}

int soundConfig(lua_State* L) {
	ASSERT_SSE_0;

	const int CONFIG_IDs[] = {
		-1,
		-1,
		SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_PITCH, // param2 = pitch
		SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_GAIN, // param3 = volume
		SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_MAX_DISTANCE, // param4 = range
		SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR, // param5 = attenuation
		SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_LOOP, // param6 = loop
		SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_LOOP_DELAY, // param7 = loopDelay
		SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_CONE_INNER_ANGLE, // param8 = innerConeAngle
		SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_CONE_OUTER_ANGLE, // param9 = outerConeAngle
		SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN, // param10= outerConeGain
	};

	int n = lua_gettop(L);
	static const int args[] = {
		LUA_TNUMBER, // placement ID
		LUA_TNUMBER,
		LUA_TNUMBER,
		LUA_TNUMBER,
		LUA_TNUMBER,
		LUA_TNUMBER,
		LUA_TNUMBER,
		LUA_TNUMBER,
		LUA_TNUMBER,
		LUA_TNUMBER,
	};

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	ULONG objectId = (ULONG)lua_tonumber(L, 1);
	std::vector<int> soundargs;
	for (int argId = 2; argId <= n; argId++) {
		int configID = CONFIG_IDs[argId]; // map param index into config ID
		if (configID != -1 && !lua_isnil(L, argId)) { // skip nil parameters
			double val = lua_tonumber(L, argId);
			int intVal = 0;

			switch (configID) {
				case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_PITCH:
				case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_GAIN:
				case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_MAX_DISTANCE:
				case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_ROLLOFF_FACTOR:
				case SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_CONE_OUTER_GAIN:
					intVal = (int)(val * 1000); // data helper for float type config values
					break;

				default:
					intVal = (int)val;
					break;
			}

			soundargs.push_back(configID);
			soundargs.push_back(intVal);
		}
	}

	pSSE->ObjectControlCallback(L, objectId, 0, SOUND_CONFIG, &soundargs);
	return 0;
}

// DRF - ED-2803 - Vehicle Sounds - Sound Modifiers
int soundAddModifier(lua_State* L) {
	ASSERT_SSE_0;

	int soundPlacementId;
	int input, output;
	double gain = 1.0; // optional
	double offset = 0.0; // optional
	if (!LuaParseArgs("iii?dd", &soundPlacementId, &input, &output, &gain, &offset))
		return 0;

	pSSE->SoundAddModifierCallback(L, soundPlacementId, input, output, gain, offset);
	return 0;
}

int soundGenerate(lua_State* L) {
	return doPlaceItem(L, Placement_Sound, __FUNCTION__);
}

int soundGenerateAmbient(lua_State* L) {
	ASSERT_SSE_0;

	int glid;
	if (!LuaParseArgs("i", &glid))
		return 0;

	// generate a new sound
	ULONG id = pSSE->ObjectGenerateCallback(L, (GLID)glid, 0, 0, 0, 1, 0, 0, Placement_Sound, true);
	if (id) {
		// Return the sound id so the user can use the other interfaces to modify it
		lua_pushnumber(L, id);

		// configure it so it covers the entire zone at the same volume everywhere
		// if the user wants to do something other than the default, send another control event?
		std::vector<int> soundargs;
		soundargs.push_back(SOUNDPLACEMENTIDS_IDS_SOUNDPLACEMENT_RELATIVE);
		soundargs.push_back(1);
		pSSE->ObjectControlCallback(L, id, 0, SOUND_CONFIG, &soundargs);
	} else {
		lua_pushnil(L);
	}

	return 1;
}

int soundMove(lua_State* L) {
	return objectMove(L);
}

int soundDelete(lua_State* L) {
	return objectDelete(L);
}

int soundPlayOnClient(lua_State* L) {
	ASSERT_SSE_0;

	int globalId;
	ULONG targetId;
	if (!LuaParseArgs("iu", &globalId, &targetId))
		return 0;

	bool success = pSSE->SoundPlayOnClient(L, globalId, targetId);
	lua_pushboolean(L, success);

	return 1;
}

int soundStopOnClient(lua_State* L) {
	ASSERT_SSE_0;

	int globalId;
	ULONG targetId;
	if (!LuaParseArgs("iu", &globalId, &targetId))
		return 0;

	bool success = pSSE->SoundStopOnClient(L, globalId, targetId);
	lua_pushboolean(L, success);

	return 1;
}

int soundAttachTo(lua_State* L, int type) {
	ASSERT_SSE_0;

	int soundPlacementId;
	ULONG targetId;
	bool bufferCall = true; // optional
	if (!LuaParseArgs("iu?b", &soundPlacementId, &targetId, &bufferCall))
		return 0;

	bool success = pSSE->SoundAttachCallback(L, soundPlacementId, type, targetId, bufferCall);
	lua_pushboolean(L, success);

	return 1;
}

int soundAttachToObject(lua_State* L) {
	return soundAttachTo(L, ObjTypeToInt(ObjectType::DYNAMIC));
}

int soundAttachToPlayer(lua_State* L) {
	return soundAttachTo(L, ObjTypeToInt(ObjectType::MOVEMENT));
}

int playerArmItem(lua_State* /*L*/) {
	return 0;
}

int playerDisarmItem(lua_State* /*L*/) {
	return 0;
}

int timerSetInterval(lua_State* /*L*/) {
	return 0;
}

int zoneSetTexture(lua_State* /*L*/) {
	return 0;
}

int objectSetParticle(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objectId;
	ULONG glidParticle;
	const char* boneName = ""; // optional
	int particleSlotId = 0; // optional
	double offset[3] = { 0, 0, 0 }; // optional
	double dir[3] = { 0, 0, 1 }; // optional
	double up[3] = { 0, 1, 0 }; // optional
	bool bufferCall = true; // optional
	if (!LuaParseArgs("uu?si?ddd?ddd?ddd?b",
			&objectId,
			&glidParticle,
			&boneName,
			&particleSlotId,
			&offset[0], &offset[1], &offset[2],
			&dir[0], &dir[1], &dir[2],
			&up[0], &up[1], &up[2],
			&bufferCall))
		return 0;

	pSSE->ObjectSetParticleCallback(L,
		0, // clientId not parameterized
		objectId,
		glidParticle,
		boneName,
		particleSlotId,
		offset[0], offset[1], offset[2],
		dir[0], dir[1], dir[2],
		up[0], up[1], up[2],
		bufferCall);

	return 0;
}

int objectSetParticleForPlayer(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	ULONG objectId;
	ULONG glidParticle;
	const char* boneName = ""; // optional
	int particleSlotId = 0; // optional
	double offset[3] = { 0, 0, 0 }; // optional
	double dir[3] = { 0, 0, 1 }; // optional
	double up[3] = { 0, 1, 0 }; // optional
	bool bufferCall = true; // optional
	if (!LuaParseArgs("uuu?si?ddd?ddd?ddd?b",
			&clientId,
			&objectId,
			&glidParticle,
			&boneName,
			&particleSlotId,
			&offset[0], &offset[1], &offset[2],
			&dir[0], &dir[1], &dir[2],
			&up[0], &up[1], &up[2],
			&bufferCall))
		return 0;

	pSSE->ObjectSetParticleCallback(L,
		clientId,
		objectId,
		glidParticle,
		boneName,
		particleSlotId,
		offset[0], offset[1], offset[2],
		dir[0], dir[1], dir[2],
		up[0], up[1], up[2],
		bufferCall);

	return 0;
}

int objectRemoveParticle(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objectId;
	const char* boneName = ""; // optional
	int particleSlotId = 0; // optional
	if (!LuaParseArgs("u?si", &objectId, &boneName, &particleSlotId))
		return 0;

	pSSE->ObjectRemoveParticleCallback(L, 0, objectId, boneName, particleSlotId);

	return 0;
}

int objectRemoveParticleForPlayer(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	ULONG objectId;
	const char* boneName = ""; // optional
	int particleSlotId = 0; // optional
	if (!LuaParseArgs("uu?si", &clientId, &objectId, &boneName, &particleSlotId))
		return 0;

	pSSE->ObjectRemoveParticleCallback(L, clientId, objectId, boneName, particleSlotId);

	return 0;
}

//////////////////////////////////////////////////////////////////////////
// This is the function that actually does the work for objectAddLabels
// and objectAddLabelsForPlayer.
//////////////////////////////////////////////////////////////////////////
static int objectAddLabelsHelper(ULONG _clientId, lua_State* L) {
	// Set up some parameters based on whether this is a zone-wide case or individual case.
	// Default assumption is zone-wide for initialization purposes.
	int argAdjust = 0;
	// If client ID is not 0, then this is specific to a client.
	if (_clientId != 0) {
		argAdjust = 1;
	}

	ULONG objectId = (ULONG)lua_tonumber(L, 1 + argAdjust);
	std::vector<ObjectLabelInfo> labels;
	lua_pushnil(L);
	while (lua_next(L, -2) != 0) {
		if (lua_istable(L, -1)) {
			ObjectLabelInfo label;
			//
			// Default values
			//
			label.m_size = 0.01f;
			label.m_red = 1.f;
			label.m_green = 1.f;
			label.m_blue = 1.f;
			lua_pushnil(L);
			//
			// Get the values from the table.
			//
			if (lua_next(L, -2) != 0) {
				label.m_label = lua_tostring(L, -1);
				lua_pop(L, 1);
				if (lua_next(L, -2) != 0) {
					label.m_size = (float)lua_tonumber(L, -1);
					if (label.m_size < 0)
						label.m_size = 0;
					else if (label.m_size > 1.f)
						label.m_size = 1.f;
					lua_pop(L, 1);
					if (lua_next(L, -2) != 0) {
						label.m_red = (float)lua_tonumber(L, -1);
						if (label.m_red < 0)
							label.m_red = 0;
						else if (label.m_red > 1.f)
							label.m_red = 1.f;
						lua_pop(L, 1);
						if (lua_next(L, -2) != 0) {
							label.m_green = (float)lua_tonumber(L, -1);
							if (label.m_green < 0)
								label.m_green = 0;
							else if (label.m_green > 1.f)
								label.m_green = 1.f;
							lua_pop(L, 1);
							if (lua_next(L, -2) != 0) {
								label.m_blue = (float)lua_tonumber(L, -1);
								if (label.m_blue < 0)
									label.m_blue = 0;
								else if (label.m_blue > 1.f)
									label.m_blue = 1.f;
								lua_pop(L, 1);
								//
								// This checks that we don't have too many args and gets the stack
								// ready for the next iteration of the main loop.
								//
								if (lua_next(L, -2) != 0) {
									// fatal error
									pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "too many arguments");
									return 0;
								}
							}
						}
					}
				}
			}
			labels.push_back(label);
		} else {
			// fatal error
			pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "expecting a table");
			return 0;
		}
		lua_pop(L, 1);
	}

	pSSE->ObjectAddLabelsCallback(L, _clientId, objectId, labels);
	return 0;
}

int objectAddLabels(lua_State* L) {
	ASSERT_SSE_0;

	static const int args[] = {
		LUA_TNUMBER, // objectId
		LUA_TTABLE, // label table
	};

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_N_SIZE(args), &errorStr, 1)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Call the helper function with a clientId of 0. This makes this a zone-wide label call.
	return objectAddLabelsHelper(0, L);
}

int objectAddLabelsForPlayer(lua_State* L) {
	ASSERT_SSE_0;

	static const int args[] = {
		LUA_TNUMBER, // client net ID
		LUA_TNUMBER, // objectId
		LUA_TTABLE, // label table
	};

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_N_SIZE(args), &errorStr, 2)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	ULONG clientId = (ULONG)lua_tonumber(L, 1);

	// Call the helper function with a clientId of 0. This make this a zone-wide particle call.
	return objectAddLabelsHelper(clientId, L);
}

// DEPRECATED
int objectSetFlash(lua_State* L) {
	return objectSetMedia(L);
}

// DRF - Added
int objectSetMedia(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objectId;
	std::string mUrlStr; // optional
	std::string mParamsStr; // optional
	double mVolPct = -1; // optional (-1=no change, 0=mute)
	double mRadiusAudio = -1; // optional (-1=no change, 0=global)
	double mRadiusVideo = -1; // optional (-1=no change, 0=global)
	const char* mUrl = ""; // optional
	const char* mParams = ""; // optional
	if (!LuaParseArgs("u?ssddd", &objectId, &mUrl, &mParams, &mVolPct, &mRadiusAudio, &mRadiusVideo))
		return 0;
	mUrlStr = mUrl ? mUrl : "";
	mParamsStr = mParams ? mParams : "";

	MediaParams mediaParams(mUrlStr, mParamsStr, mVolPct, mRadiusAudio, mRadiusVideo);
	pSSE->ObjectSetMediaCallback(L, objectId, mediaParams);

	return 0;
}

// DRF - Added
int objectSetMediaVolumeAndRadius(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objectId;
	double mVolPct = -1; // optional (-1=no change, 0=mute)
	double mRadiusAudio = -1; // optional (-1=no change, 0=global)
	double mRadiusVideo = -1; // optional (-1=no change, 0=global)
	if (!LuaParseArgs("u?ddd", &objectId, &mVolPct, &mRadiusAudio, &mRadiusVideo))
		return 0;

	MediaParams mediaParams("", "", mVolPct, mRadiusAudio, mRadiusVideo);
	pSSE->ObjectSetMediaVolumeAndRadiusCallback(L, objectId, mediaParams);

	return 0;
}

int objectSetOutline(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objectId;
	bool show;
	double color[3] = { 0, 0, 0 }; // optional
	bool bufferCall = true; // optional
	color[0] = -1;
	if (!LuaParseArgs("ub?ddd?b", &objectId, &show, &color[0], &color[1], &color[2], &bufferCall))
		return 0;
	bool setColor = (color[0] >= 0);
	if (!setColor)
		color[0] = 0;

	pSSE->ObjectSetOutlineCallback(L, objectId, show, setColor, color[0], color[1], color[2], bufferCall);

	return 0;
}

int objectClearLabels(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objectId;
	if (!LuaParseArgs("u", &objectId))
		return 0;

	pSSE->ObjectClearLabelsCallback(L, 0, objectId);

	return 0;
}

int objectClearLabelsForPlayer(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	ULONG objectId;
	if (!LuaParseArgs("uu", &clientId, &objectId))
		return 0;

	pSSE->ObjectClearLabelsCallback(L, clientId, objectId);

	return 0;
}

int objectSetFriction(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objectId;
	bool enable;
	bool bufferCall = true; // optional
	if (!LuaParseArgs("ub?b", &objectId, &enable, &bufferCall))
		return 0;

	pSSE->ObjectControlCallback(L, objectId, 0, enable ? FRICTION_ENABLE : FRICTION_DISABLE, NULL, bufferCall);

	return 0;
}

// DEPRECATED
int objectPlayMedia(lua_State* /*L*/) {
	return 0;
}

// DEPRECATED
int playerLoadPayMenu(lua_State* /*L*/) {
	return 0;
}

// DEPRECATED
int objectHighlight(lua_State* /*L*/) {
	return 0;
}

int objectSetTexturePanning(lua_State* L) {
	ASSERT_SSE_0;

	int objId; // should be ULONG?
	int meshId;
	int uvSetId;
	double uIncr;
	double vIncr;
	if (!LuaParseArgs("iiidd", &objId, &meshId, &uvSetId, &uIncr, &vIncr))
		return 0;

	pSSE->ObjectSetTexturePanningCallback(L, objId, meshId, uvSetId, uIncr, vIncr);

	return 0;
}

// Helper function for playerShow and playerHide
static int playerSetVisibilityFlag(lua_State* L, bool bVisible) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	pSSE->PlayerSetVisibilityFlagCallback(L, clientId, bVisible);

	return 0;
}

int playerShow(lua_State* L) {
	return playerSetVisibilityFlag(L, true);
}

int playerHide(lua_State* L) {
	return playerSetVisibilityFlag(L, false);
}

// Helper function for objectShow and objectHide
static int objectSetVisibilityFlag(lua_State* L, bool bVisible) {
	ASSERT_SSE_0;

	int objId; // should be ULONG?
	ULONG playerId = 0; // optional
	if (!LuaParseArgs("i?u", &objId, &playerId))
		return 0;

	pSSE->ObjectControlCallback(L, objId, playerId, bVisible ? OBJECT_SHOW : OBJECT_HIDE, NULL);

	return 0;
}

int objectShow(lua_State* L) {
	return objectSetVisibilityFlag(L, true);
}

int objectHide(lua_State* L) {
	return objectSetVisibilityFlag(L, false);
}

int playerRenderLabelsOnTop(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	bool onTop;
	if (!LuaParseArgs("ub", &clientId, &onTop))
		return 0;

	int result = pSSE->PlayerRenderLabelsOnTop(L, clientId, onTop);
	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int objectGetNextPlayListItem(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	return pSSE->ObjectGetNextPlayListItemCallback(L, clientId).getLuaRC();
}

int objectSetNextPlayListItem(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int playListItem;
	if (!LuaParseArgs("ui", &clientId, &playListItem))
		return 0;

	// play list item should be 1..n, or 0 if the user just wants to advance to the next index
	if (playListItem < 0) {
		LuaParseArgsError("playListItem < 1");
		return 0;
	}

	return pSSE->ObjectSetNextPlayListItemCallback(L, clientId, playListItem).getLuaRC();
}

int objectEnumeratePlayList(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	return pSSE->ObjectEnumeratePlayListCallback(L, clientId).getLuaRC();
}

int objectSetPlayList(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int playlistId;
	bool stopBroadcast;
	int playlistItem = 1; // optional
	if (!LuaParseArgs("uib?i", &clientId, &playlistId, &stopBroadcast, &playlistItem))
		return 0;

	return pSSE->ObjectSetPlayListCallback(L, clientId, playlistId, playlistItem, stopBroadcast).getLuaRC();
}

int playerSetParticle(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	ULONG glidParticle;
	std::string boneNameStr;
	int particleSlotId;
	double offset[3] = { 0, 0, 0 }; // optional
	double dir[3] = { 0, 0, 1 }; // optional
	double up[3] = { 0, 1, 0 }; // optional
	const char* boneName = "";
	if (!LuaParseArgs("uusi?ddd?ddd?ddd",
			&clientId, &glidParticle, &boneName, &particleSlotId,
			&offset[0], &offset[1], &offset[2],
			&dir[0], &dir[1], &dir[2],
			&up[0], &up[1], &up[2]))
		return 0;
	boneNameStr = boneName;

	pSSE->PlayerSetParticleCallback(L,
		clientId, glidParticle, boneNameStr, particleSlotId,
		offset[0], offset[1], offset[2],
		dir[0], dir[1], dir[2],
		up[0], up[1], up[2]);

	return 0;
}

int playerRemoveParticle(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string boneNameStr;
	int particleSlotId;
	const char* boneName;
	if (!LuaParseArgs("usi", &clientId, &boneName, &particleSlotId))
		return 0;
	boneNameStr = boneName;

	pSSE->PlayerRemoveParticleCallback(L, clientId, boneNameStr, particleSlotId);

	return 0;
}

int playerLookAt(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int targetType;
	ULONG targetId;
	std::string targetBoneStr;
	double offset[3] = { 0, 0, 0 }; // optional
	bool followTarget = false; // optional
	bool cancelableByPlayer = true; // optional
	const char* targetBone = "";
	if (!LuaParseArgs("uiu?s?ddd?bb",
			&clientId, &targetType, &targetId,
			&targetBone,
			&offset[0], &offset[1], &offset[2],
			&followTarget, &cancelableByPlayer))
		return 0;
	targetBoneStr = targetBone;

	pSSE->PlayerLookAtCallback(L,
		clientId, targetType, targetId, targetBoneStr,
		offset[0], offset[1], offset[2],
		followTarget, cancelableByPlayer);

	return 0;
}

int objectLookAt(lua_State* L) {
	ASSERT_SSE_0;

	ULONG objectId;
	int targetType;
	ULONG targetId;
	std::string targetBoneStr;
	double offset[3] = { 0, 0, 0 }; // optional
	bool followTarget = false; // optional
	bool allowXZRotation = false; // optional
	double forward[3] = { 0, 0, 1 }; // optional
	const char* targetBone = "";
	if (!LuaParseArgs("uiu?s?ddd?bb?ddd",
			&objectId, &targetType, &targetId,
			&targetBone,
			&offset[0], &offset[1], &offset[2],
			&followTarget, &allowXZRotation,
			&forward[0], &forward[1], &forward[2]))
		return 0;
	targetBoneStr = targetBone;

	pSSE->ObjectLookAtCallback(L,
		objectId, targetType, targetId,
		targetBoneStr,
		offset[0], offset[1], offset[2],
		followTarget, allowXZRotation,
		forward[0], forward[1], forward[2]);
	return 0;
}

int objectGetData(lua_State* L) {
	ASSERT_SSE_0;

	ULONG glid;
	if (!LuaParseArgs("u", &glid))
		return 0;

	std::vector<std::string> retValues;
	bool success = pSSE->ObjectGetDataCallback(L, (GLID)glid, retValues);
	if (success) {
		lua_newtable(L);
		int topMain = lua_gettop(L);
		for (int c = 0; c < (int)retValues.size(); c++) { //"columns"
			lua_pushstring(L, retValues[c].c_str());
			lua_rawseti(L, topMain, c);
		}
	} else {
		lua_pushnil(L);
	}

	return 1;
}

int objectGetOwner(lua_State* L) {
	ASSERT_SSE_0;

	ULONG pid;
	if (!LuaParseArgs("u", &pid))
		return 0;

	std::vector<std::string> retValues;
	bool success = pSSE->ObjectGetOwnerCallback(L, (ULONG)pid, retValues);
	if (success) {
		jsAssert(retValues.size() >= 2);
		lua_newtable(L);
		int topMain = lua_gettop(L);
		lua_pushnumber(L, atoi(retValues[0].c_str()));
		lua_setfield(L, topMain, "ownerId");
		lua_pushstring(L, retValues[1].c_str());
		lua_setfield(L, topMain, "ownerName");
	} else {
		lua_pushnil(L);
	}

	return 1;
}

int objectSetMouseOver(lua_State* L) {
	ASSERT_SSE_0;

	int objType;
	ULONG objId;
	int mosMode;
	std::string tooltipStr; // optional
	int cursorType = CURSOR_TYPE_DEFAULT; // optional
	int mosCategory = 0; //MOS::CATEGORY_NONE // optional
	const char* tooltip = "";
	if (!LuaParseArgs("iui?sii", &objType, &objId, &mosMode, &tooltip, &cursorType, &mosCategory))
		return 0;
	tooltipStr = tooltip;

	pSSE->ObjectSetMouseOverCallback(L, objType, objId, mosMode, tooltipStr, cursorType, mosCategory);

	return 0;
}

int playerAddIndicator(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int type;
	std::string idStr;
	double offset[3];
	int glid = GLID_GEM_INDICATOR; // optional
	double speed = 0; // optional
	double moveY = 0.2; // optional
	const char* id = "";
	if (!LuaParseArgs("uisddd?idd",
			&clientId, &type, &id,
			&offset[0], &offset[1], &offset[2],
			&glid, &speed, &moveY))
		return 0;
	idStr = id;

	pSSE->PlayerAddIndicatorCallback(L, clientId, type, idStr, offset[0], offset[1], offset[2], glid, speed, moveY);

	return 0;
}

int playerClearIndicators(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int type;
	std::string idStr;
	const char* id = "";
	if (!LuaParseArgs("uis", &clientId, &type, &id))
		return 0;
	idStr = id;

	pSSE->PlayerClearIndicatorsCallback(L, clientId, type, idStr);

	return 0;
}

int playerClosePresetMenu(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string menuNameStr;
	const char* menuName = "";
	if (!LuaParseArgs("us", &clientId, &menuName))
		return 0;
	menuNameStr = menuName;

	bool ret = pSSE->PlayerClosePresetMenu(L, clientId, menuNameStr);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerMovePresetMenu(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string menuNameStr;
	double x;
	double y;
	const char* menuName = "";
	if (!LuaParseArgs("usdd", &clientId, &menuName, &x, &y))
		return 0;
	menuNameStr = menuName;

	bool ret = pSSE->PlayerMovePresetMenu(L, clientId, menuNameStr, x, y);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerOpenYesNoMenu(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string titleStr;
	std::string descStr;
	std::string yesStr;
	std::string noStr;
	std::string idStr; // optional
	const char* title = "";
	const char* desc = "";
	const char* yes = "";
	const char* no = "";
	const char* id = "";
	if (!LuaParseArgs("ussss?s", &clientId, &title, &desc, &yes, &no, &id))
		return 0;
	titleStr = title;
	descStr = desc;
	yesStr = yes;
	noStr = no;
	idStr = id;

	bool ret = pSSE->PlayerOpenYesNoMenu(L, clientId, titleStr, descStr, yesStr, noStr, idStr);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerOpenKeyListenerMenu(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string keysStr;
	std::string idStr; // optional
	const char* keys = "";
	const char* id = "";
	if (!LuaParseArgs("us?s", &clientId, &keys, &id))
		return 0;
	keysStr = keys;
	idStr = id;

	bool ret = pSSE->PlayerOpenKeyListenerMenu(L, clientId, keysStr, idStr);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerOpenTextTimerMenu(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string textStr;
	int timeSec;
	std::string idStr; // optional
	const char* text = "";
	const char* id = "";
	if (!LuaParseArgs("usi?s", &clientId, &text, &timeSec, &id))
		return 0;
	textStr = text;
	idStr = id;

	bool ret = pSSE->PlayerOpenTextTimerMenu(L, clientId, textStr, timeSec, idStr);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerOpenButtonMenu(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string titleStr;
	std::string descStr;
	std::string btnStr;
	std::string idStr; // optional
	const char* title = "";
	const char* desc = "";
	const char* btn = "";
	const char* id = "";
	if (!LuaParseArgs("usss?s", &clientId, &title, &desc, &btn, &id))
		return 0;
	titleStr = title;
	descStr = desc;
	btnStr = btn;
	idStr = id;

	bool ret = pSSE->PlayerOpenButtonMenu(L, clientId, titleStr, descStr, btnStr, idStr);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerOpenStatusMenu(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string titleStr;
	std::string statusStr;
	std::string idStr; // optional
	const char* title = "";
	const char* status = "";
	const char* id = "";
	if (!LuaParseArgs("uss?s", &clientId, &title, &status, &id))
		return 0;
	titleStr = title;
	statusStr = status;
	idStr = id;

	bool ret = pSSE->PlayerOpenStatusMenu(L, clientId, titleStr, statusStr, idStr);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerOpenListBoxMenu(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string titleStr;
	std::string listStr;
	std::string btnStr;
	std::string idStr; // optional
	const char* title = "";
	const char* list = "";
	const char* btn = "";
	const char* id = "";
	if (!LuaParseArgs("usss?s", &clientId, &title, &list, &btn, &id))
		return 0;
	titleStr = title;
	listStr = list;
	btnStr = btn;
	idStr = id;

	bool ret = pSSE->PlayerOpenListBoxMenu(L, clientId, titleStr, listStr, btnStr, idStr);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerOpenShopMenu(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string titleStr;
	std::string idStr; // optional
	const char* title = "";
	const char* id = "";
	if (!LuaParseArgs("us?s", &clientId, &title, &id))
		return 0;
	titleStr = title;
	idStr = id;

	bool ret = pSSE->PlayerOpenShopMenu(L, clientId, titleStr, idStr);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerAddItemToShop(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int objId;
	std::string itemNameStr;
	int cost;
	std::string iconStr;
	std::string idStr; // optional
	const char* itemName = "";
	const char* icon = "";
	const char* id = "";
	if (!LuaParseArgs("uisis?s", &clientId, &objId, &itemName, &cost, &icon, &id))
		return 0;
	itemNameStr = itemName;
	iconStr = icon;
	idStr = id;

	bool ret = pSSE->PlayerAddItemToShopMenu(L, clientId, objId, itemNameStr, cost, iconStr, idStr);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerUpdateCoinHUD(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string typeStr;
	int value;
	const char* type = "";
	if (!LuaParseArgs("usi", &clientId, &type, &value))
		return 0;
	typeStr = type;

	bool ret = pSSE->PlayerUpdateCoinHUD(L, clientId, typeStr, value);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerUpdateHealthHUD(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string typeStr;
	std::string targetStr;
	int health;
	int healthChange;
	const char* type = "";
	const char* target = "";
	if (!LuaParseArgs("ussii", &clientId, &type, &target, &health, &healthChange))
		return 0;
	typeStr = type;
	targetStr = target;

	bool ret = pSSE->PlayerUpdateHealthHUD(L, clientId, typeStr, targetStr, health, healthChange);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerUpdateXPHUD(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int level;
	int levelXP;
	int xpToNextLevel;
	if (!LuaParseArgs("uiii", &clientId, &level, &levelXP, &xpToNextLevel))
		return 0;

	bool ret = pSSE->PlayerUpdateXPHUD(L, clientId, level, levelXP, xpToNextLevel);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int playerAddHealthIndicator(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int type;
	std::string idStr;
	int healthPercentage;
	const char* id = "";
	if (!LuaParseArgs("uisi", &clientId, &type, &id, &healthPercentage))
		return 0;
	idStr = id;

	pSSE->PlayerAddHealthIndicator(L, clientId, type, idStr, healthPercentage);

	return 0; // number of return values
}

int playerRemoveHealthIndicator(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	int type;
	std::string idStr;
	const char* id = "";
	if (!LuaParseArgs("uis", &clientId, &type, &id))
		return 0;
	idStr = id;

	pSSE->PlayerRemoveHealthIndicator(L, clientId, type, idStr);

	return 0; // number of return values
}

int playerShowStatusInfo(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	std::string messageStr;
	int timeMs = 1000; // optional
	int color[3] = { 255, 255, 255 }; // optional
	const char* message = "";
	if (!LuaParseArgs("us?iiii", &clientId, &message, &timeMs, &color[0], &color[1], &color[2]))
		return 0;
	messageStr = message;

	bool ret = pSSE->PlayerShowStatusInfo(L, clientId, messageStr, timeMs, color[0], color[1], color[2]);
	if (ret)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

bool playerSpawnVehicle(
	lua_State* L,
	SCRIPT_NAMED_PARAM(ULONG, clientId),
	SCRIPT_NAMED_PARAM(int, placementId),
	SCRIPT_NAMED_PARAM(Vector3f, meshOffset),
	SCRIPT_NAMED_PARAM(Vector4f, meshOrientation),
	SCRIPT_NAMED_PARAM(Vector3f, dimensions),
	SCRIPT_NAMED_PARAM(float, maxSpeedKPH),
	SCRIPT_NAMED_PARAM(float, horsepower),
	SCRIPT_NAMED_PARAM(float, suspHeightLoaded),
	SCRIPT_NAMED_PARAM(float, suspHeightUnloaded),
	SCRIPT_NAMED_PARAM(float, massKG),
	SCRIPT_NAMED_PARAM(float, wheelFriction),
	SCRIPT_NAMED_PARAM_OPT(GLID, throttleSoundGlid),
	SCRIPT_NAMED_PARAM_OPT(GLID, skidSoundGlid),
	SCRIPT_NAMED_PARAM_OPT(Vector2f, jumpVelocityMPS)) {
	ASSERT_SSE_BOOL;

	bool ret = pSSE->PlayerSpawnVehicle(L,
		clientId.Get(),
		placementId.Get(),
		meshOffset.Get(),
		Quaternionf(meshOrientation.Get()),
		dimensions.Get(),
		maxSpeedKPH.Get(), horsepower.Get(),
		suspHeightLoaded.Get(), suspHeightUnloaded.Get(),
		massKG.Get(), wheelFriction.Get(),
		jumpVelocityMPS.GetOptional(Vector2f(0, 0)),
		throttleSoundGlid.GetOptional(0),
		skidSoundGlid.GetOptional(0));
	return ret;
}

bool playerLeaveVehicle(lua_State* L, ULONG clientId, int placementId) {
	return pSSE->PlayerLeaveVehicle(L, clientId, placementId);
}

bool playerModifyVehicle(
	lua_State* L,
	SCRIPT_NAMED_PARAM(ULONG, clientId),
	SCRIPT_NAMED_PARAM(float, maxSpeedKPH),
	SCRIPT_NAMED_PARAM(float, horsepower),
	SCRIPT_NAMED_PARAM(Vector2f, jumpVelocityMPS)) {
	return pSSE->playerModifyVehicle(clientId.Get(), maxSpeedKPH.Get(), horsepower.Get(), jumpVelocityMPS.Get());
}

/** DRF - LUA API
* int kgp.playerSetCustomTextureOnAccessory(clientId, glidAccessory, urlStr)
*/
int playerSetCustomTextureOnAccessory(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	GLID glidAccessory;
	std::string urlStr;
	const char* url = "";
	if (!LuaParseArgs("uus", &clientId, &glidAccessory, &url))
		return 0;
	urlStr = url;

	pSSE->PlayerSetCustomTextureOnAccessoryCallback(L, clientId, glidAccessory, urlStr);

	return 0;
}

// Lua: function playerSaveZoneSpawnPoint(playerId, x, y, z, rx, ry, rz )
int playerSaveZoneSpawnPoint(lua_State* L) {
	ASSERT_SSE_0;

	// Get arguments.
	ULONG clientId;
	double x, y, z, rx, ry, rz;
	if (!LuaParseArgs("udddddd", &clientId, &x, &y, &z, &rx, &ry, &rz))
		return 0;

	int playerId = 0;
	if (luaGetCurrentPlayerData(L, clientId, KGPHANDLER_TEMPDATA_PLAYERID, LUA_TNUMBER)) {
		playerId = (int)lua_tonumber(L, -1);
		lua_pop(L, 1);
	}

	pSSE->PlayerSaveZoneSpawnPointCallback(L, clientId, (float)x, (float)y, (float)z, (float)rx, (float)ry, (float)rz, playerId);

	return 0;
}

// Lua: function playerDeleteZoneSpawnPoint(playerId)
int playerDeleteZoneSpawnPoint(lua_State* L) {
	ASSERT_SSE_0;

	// Get arguments.
	ULONG clientId;
	if (!LuaParseArgs("u", &clientId))
		return 0;

	int playerId = 0;
	if (luaGetCurrentPlayerData(L, clientId, KGPHANDLER_TEMPDATA_PLAYERID, LUA_TNUMBER)) {
		playerId = (int)lua_tonumber(L, -1);
		lua_pop(L, 1);
	}

	pSSE->PlayerDeleteZoneSpawnPointCallback(L, clientId, playerId);

	return 0;
}

int gameGetPlayersInZone(lua_State* L) {
	ASSERT_SSE_0;

	std::set<ULONG> playerIDs;
	pSSE->ScriptGetPlayersInZoneCallback(L, playerIDs);

	lua_newtable(L);
	int index = 1;
	for (auto it = playerIDs.cbegin(); it != playerIDs.cend(); ++it, ++index) {
		lua_pushnumber(L, *it);
		lua_rawseti(L, -2, index);
	}

	return 1;
}

int gameItemGetOwned(lua_State* L) {
	ASSERT_SSE_0;

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_NONE, &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	std::vector<std::vector<std::string>> gameItems;
	bool res = pSSE->GameItemGetByApp(L, gameItems);
	if (res && !gameItems.empty()) {
		lua_newtable(L);
		int topMain = lua_gettop(L); // table of script data tables
		for (unsigned int i = 0; i < gameItems.size(); i++) {
			lua_newtable(L); // the new table I want at that index (i.e. a table of script data)
			int topSub = lua_gettop(L);

			lua_pushnumber(L, atoi(gameItems[i][1].c_str()));
			lua_setfield(L, topSub, "UNID");

			lua_pushstring(L, gameItems[i][2].c_str());
			lua_setfield(L, topSub, "ownerName");

			// add the script table to the main table
			lua_rawseti(L, topMain, atoi(gameItems[i][0].c_str())); // use the placement id as the key (because it's unique), then add the attributes into the sub table
		}
	} else {
		lua_pushnil(L);
	}
	return 1;
}

int gameItemGetOwnedByPlayer(lua_State* L) {
	ASSERT_SSE_0;

	std::string playerNameStr;
	const char* playerName = "";
	if (!LuaParseArgs("s", &playerName))
		return 0;
	playerNameStr = playerName;

	// Return Table With All Player Game Items
	std::vector<std::pair<int, int>> gameItems;
	bool res = pSSE->GameItemGetByPlayer(L, playerNameStr, gameItems);
	if (res && !gameItems.empty()) {
		lua_newtable(L);
		for (unsigned int i = 0; i < gameItems.size(); i++) {
			lua_pushnumber(L, gameItems[i].second);
			lua_rawseti(L, -2, gameItems[i].first);
		}
	} else {
		lua_pushnil(L);
	}
	return 1;
}

int gameItemLoadAllByGame(lua_State* L) {
	ASSERT_SSE_0;

	ULONG zone_instance_id, zone_type;
	if (!LuaParseArgs("uu", &zone_instance_id, &zone_type))
		return 0;

	std::vector<GameItem> gameItems;
	bool res = pSSE->GameItemLoadAll(L, gameItems, zone_instance_id, zone_type);
	if (res && !gameItems.empty()) {
		lua_newtable(L);
		int topMain = lua_gettop(L);
		for (unsigned int i = 0; i < gameItems.size(); i++) {
			lua_newtable(L);
			int topSub = lua_gettop(L);
			for (unsigned int j = 0; j < gameItems[i].properties.size(); j++) {
				lua_pushstring(L, gameItems[i].properties[j].second.c_str());
				lua_setfield(L, topSub, gameItems[i].properties[j].first.c_str());
			}
			lua_pushboolean(L, gameItems[i].inventoryCompatible);
			lua_setfield(L, topSub, "inventoryCompatible");
			lua_pushnumber(L, gameItems[i].level);
			lua_setfield(L, topSub, "level");
			lua_pushnumber(L, gameItems[i].giGLID);
			lua_setfield(L, topSub, "GIGLID");
			lua_pushnumber(L, gameItems[i].glid);
			lua_setfield(L, topSub, "GLID");
			lua_pushnumber(L, gameItems[i].giId);
			lua_setfield(L, topSub, "GIID");
			lua_pushstring(L, gameItems[i].rarity.c_str());
			lua_setfield(L, topSub, "rarity");
			lua_pushstring(L, gameItems[i].itemType.c_str());
			lua_setfield(L, topSub, "itemType");
			lua_pushstring(L, gameItems[i].name.c_str());
			lua_setfield(L, topSub, "name");

			lua_rawseti(L, topMain, gameItems[i].giId);
		}
	} else {
		lua_pushnil(L);
	}
	return 1;
}

int gameItemDelete(lua_State* L) {
	ASSERT_SSE_1;

	int gameItemId;
	unsigned int zone_instance_id, zone_type;
	if (!LuaParseArgs("iuu", &gameItemId, &zone_instance_id, &zone_type))
		return 0;

	bool res = pSSE->GameItemDelete(L, gameItemId, zone_instance_id, zone_type);
	if (res) {
		lua_pushnumber(L, 1);
	} else {
		lua_pushnil(L);
	}

	return 1;
}

int gameItemUpdate(lua_State* L) {
	ASSERT_SSE_0;

	static const int args[] = {
		LUA_TNUMBER,
		LUA_TNUMBER,
		LUA_TNUMBER,
		LUA_TTABLE
	};

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}
	int giId = (int)lua_tonumber(L, 1);
	unsigned int zone_instance_id = (int)lua_tonumber(L, 2);
	unsigned int zone_type = (int)lua_tonumber(L, 3);

	if (!lua_istable(L, -1)) {
		pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "Bad arguments for gameItemUpdate third parameter must be table");
		return 0;
	}

	GameItem g;
	g.giId = giId;
	g.glid = 0;
	g.giGLID = 0;
	g.level = 1;
	g.name = "default";
	g.itemType = "generic";
	g.rarity = "Common";
	g.inventoryCompatible = false; // Determined by itemType, not included in updates/inserts

	lua_pushnil(L);
	while (lua_next(L, -2) != 0) {
		if (!lua_isstring(L, -1) || !lua_isstring(L, -2)) {
			lua_pop(L, 1);
			continue;
		}

		std::string param = lua_tostring(L, -2);
		std::string value = lua_tostring(L, -1);
		if (param.compare("GLID") == 0) {
			g.glid = atoi(value.c_str());
		} else if (param.compare("GIGLID") == 0) {
			g.giGLID = atoi(value.c_str());
		} else if (param.compare("level") == 0) {
			g.level = atoi(value.c_str());
		} else if (param.compare("GIID") == 0 || param.compare("inventoryCompatible") == 0 || param.compare("bundledGlids") == 0) {
			// already handled, but don't add to props
		} else if (param.compare("rarity") == 0) {
			g.rarity = value;
		} else if (param.compare("itemType") == 0) {
			g.itemType = value;
		} else if (param.compare("name") == 0) {
			g.name = value;
		} else {
			// non required property
			g.properties.push_back(make_pair(param, value));
		}
		lua_pop(L, 1);
	}

	bool res = pSSE->GameItemUpdate(L, g, zone_instance_id, zone_type);
	if (res)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);
	return 1;
}

int gameItemPlace(lua_State* L) {
	ASSERT_SSE_0;

	ULONG clientId;
	GLID glidDO;
	int gameItemId;
	double pos[3];
	double rot[3] = { 0, 0, 1 }; // optional
	if (!LuaParseArgs("uuiddd?ddd",
			&clientId,
			&glidDO,
			&gameItemId,
			&pos[0], &pos[1], &pos[2],
			&rot[0], &rot[1], &rot[2]))
		return 0;

	if (rot[0] == 0.f && rot[2] == 0.f) {
		LuaParseArgsError("{rx, rz} must not be equal to zero");
		return 0; // number of return values
	}

	return pSSE->GameItemPlaceCallback(L, clientId, glidDO, gameItemId, pos[0], pos[1], pos[2], rot[0], rot[1], rot[2]).getLuaRC();
}

int gameItemPlaceAsUser(lua_State* L) {
	ASSERT_SSE_0;

	ULONG kanevaUserId;
	GLID glidDO;
	int gameItemId;
	double pos[3];
	double rot[3] = { 0, 0, 1 }; // optional
	if (!LuaParseArgs("uuiddd?ddd",
			&kanevaUserId,
			&glidDO,
			&gameItemId,
			&pos[0], &pos[1], &pos[2],
			&rot[0], &rot[1], &rot[2]))
		return 0;

	if (rot[0] == 0.f && rot[2] == 0.f) {
		LuaParseArgsError("{rx, rz} must not be equal to zero");
		return 0; // number of return values
	}

	return pSSE->GameItemPlaceAsUserCallback(L, kanevaUserId, glidDO, gameItemId, pos[0], pos[1], pos[2], rot[0], rot[1], rot[2]).getLuaRC();
}

int gameItemSetPosRot(lua_State* L) {
	ASSERT_SSE_0;

	ULONG pid;
	double pos[3];
	double rot[3] = { 0, 0, 1 }; // optional
	if (!LuaParseArgs("uddd?ddd",
			&pid,
			&pos[0], &pos[1], &pos[2],
			&rot[0], &rot[1], &rot[2]))
		return 0;

	if (rot[0] == 0.f && rot[2] == 0.f) {
		LuaParseArgsError("{rx, rz} must not be equal to zero");
		return 0; // number of return values
	}

	if (!pSSE->GameItemSetPosRotCallback(L, pid, pos[0], pos[1], pos[2], rot[0], rot[1], rot[2])) {
		LogError("FAILED: pid = " << pid << ", pos = " << pos[0] << "," << pos[1] << "," << pos[2] << ", rot = " << rot[0] << "," << rot[1] << "," << rot[2]);
		return 0;
	}

	return 0; // number of return values
}

int gameItemDeletePlacement(lua_State* L) {
	ASSERT_SSE_0;

	ULONG pid;
	if (!LuaParseArgs("u", &pid))
		return 0;

	pSSE->GameItemDeletePlacementCallback(L, pid);

	return 0;
}

int gameItemGetNextId(lua_State* L) {
	ASSERT_SSE_1;

	/* push the object id */
	int nextId = 0;
	bool success = pSSE->GameItemGetNextId(L, nextId);
	if (success) {
		lua_pushnumber(L, nextId);
	} else {
		lua_pushnil(L); // error
	}

	return 1;
}

int scriptSavePlayerData(lua_State* L) {
	ASSERT_SSE_0;

	const char* playerName;
	const char* attribute;
	const char* value;
	ULONG zoneInstanceId = 0;
	ULONG zoneType = 0;
	if (!LuaParseArgs("sss?uu", &playerName, &attribute, &value, &zoneInstanceId, &zoneType))
		return 0;

	bool result = pSSE->ScriptSavePlayerDataCallback_v2(L, playerName, attribute, value, zoneInstanceId, zoneType);

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int scriptLoadAllPlayerData(lua_State* L) {
	ASSERT_SSE_0;

	const char* playerName;
	ULONG zoneInstanceId = 0;
	ULONG zoneType = 0;
	if (!LuaParseArgs("s?uu", &playerName, &zoneInstanceId, &zoneType))
		return 0;

	ScriptDataMap playerData;

	bool result = pSSE->ScriptLoadAllPlayerDataCallback_v2(L, playerName, playerData, zoneInstanceId, zoneType);

	//Create table form playerData and return
	if (!playerData.empty() && result) {
		lua_newtable(L);
		int top = lua_gettop(L);
		for (ScriptDataMap::iterator pD = playerData.begin(); pD != playerData.end(); pD++) {
			lua_pushstring(L, pD->second.c_str());
			lua_setfield(L, top, pD->first.c_str());
		}
	} else {
		lua_pushnil(L);
	}

	return 1;
}

int scriptLoadPlayerData(lua_State* L) {
	ASSERT_SSE_0;

	const char* playerName;
	const char* attribute;
	ULONG zoneInstanceId = 0;
	ULONG zoneType = 0;
	if (!LuaParseArgs("ss?uu", &playerName, &attribute, &zoneInstanceId, &zoneType))
		return 0;

	std::string value = pSSE->ScriptLoadPlayerDataCallback_v2(L, playerName, attribute, zoneInstanceId, zoneType);

	if (!value.empty())
		lua_pushstring(L, value.c_str());
	else
		lua_pushnil(L);

	return 1;
}

int scriptDeletePlayerData(lua_State* L) {
	ASSERT_SSE_0;

	const char* playerName;
	const char* attribute;
	ULONG zoneInstanceId = 0;
	ULONG zoneType = 0;
	if (!LuaParseArgs("ss?uu", &playerName, &attribute, &zoneInstanceId, &zoneType))
		return 0;

	bool result = pSSE->ScriptDeletePlayerDataCallback_v2(L, playerName, attribute, zoneInstanceId, zoneType);

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int scriptDeleteAllPlayerData(lua_State* L) {
	ASSERT_SSE_0;

	const char* playerName;
	ULONG zoneInstanceId = 0;
	ULONG zoneType = 0;
	if (!LuaParseArgs("s?uu", &playerName, &zoneInstanceId, &zoneType))
		return 0;

	bool result = pSSE->ScriptDeleteAllPlayerDataCallback_v2(L, playerName, zoneInstanceId, zoneType);

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int scriptSaveGameData(lua_State* L) {
	ASSERT_SSE_0;

	const char* attribute;
	const char* value;
	ULONG zoneInstanceId = 0;
	ULONG zoneType = 0;
	if (!LuaParseArgs("ss?uu", &attribute, &value, &zoneInstanceId, &zoneType))
		return 0;

	bool result = pSSE->ScriptSaveGameDataCallback_v2(L, attribute, value, zoneInstanceId, zoneType);

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int scriptLoadAllGameData(lua_State* L) {
	ASSERT_SSE_0;

	ScriptDataMap gameData;
	bool result = pSSE->ScriptLoadAllGameDataCallback_v2(L, gameData);

	//Create table form playerData and return
	if (!gameData.empty() && result) {
		lua_newtable(L);
		int top = lua_gettop(L);
		for (ScriptDataMap::iterator gD = gameData.begin(); gD != gameData.end(); gD++) {
			lua_pushstring(L, gD->second.c_str());
			lua_setfield(L, top, gD->first.c_str());
		}
	} else {
		lua_pushnil(L);
	}

	return 1;
}

int scriptLoadGameData(lua_State* L) {
	ASSERT_SSE_0;

	const char* attribute;
	ULONG zoneInstanceId = 0;
	ULONG zoneType = 0;
	if (!LuaParseArgs("s?uu", &attribute, &zoneInstanceId, &zoneType))
		return 0;

	std::string value = pSSE->ScriptLoadGameDataCallback_v2(L, attribute, zoneInstanceId, zoneType);

	if (!value.empty()) {
		lua_pushstring(L, value.c_str());
	} else {
		lua_pushnil(L);
	}

	return 1;
}

int scriptDeleteGameData(lua_State* L) {
	ASSERT_SSE_0;

	const char* attribute;
	ULONG zoneInstanceId = 0;
	ULONG zoneType = 0;
	if (!LuaParseArgs("s?uu", &attribute, &zoneInstanceId, &zoneType))
		return 0;

	bool result = pSSE->ScriptDeleteGameDataCallback_v2(L, attribute, zoneInstanceId, zoneType);

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int scriptDeleteAllGameData(lua_State* L) {
	ASSERT_SSE_0;

	bool result = pSSE->ScriptDeleteAllGameDataCallback_v2(L);

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int gameGetFrameworkEnabled(lua_State* L) {
	ASSERT_SSE_0;

	bool result = pSSE->GameGetFrameworkEnabledCallback(L);

	if (result)
		lua_pushnumber(L, 1);
	else
		lua_pushnumber(L, 0);

	return 1;
}

int gameSetFrameworkEnabled(lua_State* L) {
	ASSERT_SSE_0;
	bool enabled;
	if (!LuaParseArgs("b", &enabled))
		return 0;

	pSSE->GameSetFrameworkEnabledCallback(L, enabled);

	return 0;
}

int gameLoadGlobalConfig(lua_State* L) {
	ASSERT_SSE_0;

	// Function takes in a single table with a list of properties to fetch.
	static const int args[] = {
		LUA_TTABLE, // label table
	};

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Pull the table off the Lua state and verify it.
	std::vector<std::string> propertyList;
	lua_pushnil(L);
	while (lua_next(L, -2) != 0) {
		if (lua_type(L, -1) == LUA_TSTRING) {
			std::string prop = lua_tostring(L, -1);
			propertyList.push_back(prop);
		} else {
			// fatal error
			pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "expecting a list of strings");
			return 0;
		}
		lua_pop(L, 1);
	}

	// Now query the DB for the requested data.
	ScriptDataMap outMap;
	bool result = pSSE->GameLoadGlobalConfig(propertyList, outMap);

	// Create table from playerData and return
	if (!outMap.empty() && result) {
		lua_newtable(L);
		int top = lua_gettop(L);
		for (ScriptDataMap::iterator mapIter = outMap.begin(); mapIter != outMap.end(); mapIter++) {
			lua_pushstring(L, mapIter->second.c_str());
			lua_setfield(L, top, mapIter->first.c_str());
		}
	} else {
		lua_pushnil(L);
	}

	return 1;
}

int gameInitFrameworkDB(lua_State* L) {
	ASSERT_SSE_0;

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_NONE, &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	return pSSE->GameInitFrameworkDB(L);
}

///////////////////////////////////////////////////////////////////////////////
// Dictionary Functions
///////////////////////////////////////////////////////////////////////////////

// Helper function converts a Lua parameter into a dictionary user data
static DictUserData* dictionaryFromStack(lua_State* L, int index) {
	void* ud = luaL_checkudata(L, index, DICTIONARY_METATABLE_NAME);
	luaL_argcheck(L, ud != NULL, 1, "`dictionary' expected"); //TODO: check if lua to automatically return when error occurred
	return (DictUserData*)ud;
}

// Helper function obtains kgpDictionaryUserDataCache table from Lua registry and push it into stack. Create it if not already
static void dictionaryGetUserDataCache(lua_State* L, bool readonly) {
	const char* tableName = readonly ? REGISTRY_DICTIONARY_USERDATA_CACHE_READONLY : REGISTRY_DICTIONARY_USERDATA_CACHE;
	lua_getfield(L, LUA_REGISTRYINDEX, tableName);
	if (!lua_istable(L, -1)) {
		// Initialize kgpDictionaryUserDataCache in the registry with weak values
		lua_pop(L, 1);
		lua_newtable(L); // userDataCache = {}
		lua_pushvalue(L, -1); // dup ref to userDataCache for lua_setfield
		lua_setfield(L, LUA_REGISTRYINDEX, tableName); // registry["kgpDictionaryUserDataCache"] = userDataCache

		// Setup weak value metatable
		lua_newtable(L); // mt = {}
		lua_pushstring(L, "v");
		lua_setfield(L, -2, "__mode"); // mt.__mode = "v"

		// Assign metatable
		lua_setmetatable(L, -2); // setmetatable(kgpDictionaryUserDataCache, mt)

		// Leave userDataCache at the top of the stack
	}
}

// Helper function retrieves previous created fulluserdata value from cache (of current VM). The fulluserdata value (or nil) will be at the top of the Lua VM stack.
// Returns true if found. False otherwise.
static bool dictionaryGetUserDataFromCache(lua_State* L, const char* name, bool readonly) {
	//All arguments are required
	// look it up in the cache first to avoid creating duplicated userdata pointing to same dictionary
	dictionaryGetUserDataCache(L, readonly);
	lua_getfield(L, -1, name); // obtain dictionary userdata by dictionary name and leave it in stack
	lua_remove(L, -2); // remove cache table from stack

	return !lua_isnil(L, -1);
}

// Helper function creates a fulluserdata value in the stack and binds it to a dictionary
static bool dictionaryCreateUserData(lua_State* L, const char* name, bool readonly, HDICT hDict, fast_recursive_mutex* hDictMutex) {
	ASSERT_SSE_0

	jsAssert(hDict != NULL && hDictMutex != NULL);

	int udSize = sizeof(DictUserData) + strlen(name);
	DictUserData* dict = (DictUserData*)lua_newuserdata(L, udSize);
	if (dict == NULL) {
		//TODO: error reporting
		//Failed: nil at stack pos -1
		lua_pushnil(L);
		return false;
	}

	// call constructor by placement-new
	new (dict) DictUserData(name, readonly, hDict, hDictMutex);

	// Set metatable
	luaL_getmetatable(L, DICTIONARY_METATABLE_NAME);
	lua_setmetatable(L, -2);

	// Store in kgpDictionaryUserDataCache in current VM for future lookup
	{
		dictionaryGetUserDataCache(L, readonly); // save newly created userdata value in the cache for future lookup
		lua_pushvalue(L, -2); // value - userdata
		lua_setfield(L, -2, name); // record userdata in the cache with dictionary name as key
		lua_pop(L, 1); // pop the cache table
	}

	// Succeeded: userdata at stack pos -1
	return true;
}

// Helper function obtains dictionary value by key and pushes it to stack
static void dictionaryGetValueByKey(lua_State* L, DictUserData* dict, const char* key) {
	if (!pSSE)
		return;

	NULL_CHK_RETURN_VOID(dict);
	NULL_CHK_RETURN_VOID(key);

	// Allocate return buffer C in stack and cast buffer address to pointer
	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void** ppVT = (void**)buffer;
	ILuaAdapter* value = (ILuaAdapter*)buffer;

	// Wipe out first four bytes (vtable ptr) so we can do sanity check afterwards
	*ppVT = NULL;

	int type = pSSE->DictionaryGetValue(dict, key, buffer, sizeof(buffer));

	// Sanity check
	if (*ppVT == NULL) {
		// Something is wrong
		LogError("DictionaryGetValue did not fill buffer properly, dict=" << dict->GetName() << ", key=" << key);
		jsAssert(false);
		lua_pushnil(L);
		return;
	}

	switch (type) {
		case LUA_TUSERDATA: {
			auto sVal = dynamic_cast<LuaPrimValue<std::string>*>(value);
			if (sVal == NULL) {
				LogError("DictionaryGetValue returned type USERDATA but the buffer does not contain a valid string container, dict=" << dict->GetName() << ", key=" << key);
				jsAssert(false);
				lua_pushnil(L);
			} else if (dictionaryGetUserDataFromCache(L, sVal->val.c_str(), dict->IsReadonly())) {
				// Succeeded: userdata at stack pos -1
			} else {
				// Already orphaned and swept by GC
				lua_pop(L, 1); // pop the nil value

				// Call DictionaryGenerate to attach to this dictionary (and increment reference count)
				HDICT hDict = NULL;
				fast_recursive_mutex* hDictMutex = NULL;

				if (!pSSE->DictionaryGenerate(sVal->val.c_str(), hDict, hDictMutex)) {
					LogError("Error occurred while attaching to dictionary `" << sVal->val << "'");
				} else {
					// Create userdata in the stack
					(void)dictionaryCreateUserData(L, sVal->val.c_str(), dict->IsReadonly(), hDict, hDictMutex);
				}
			}
		} break;
		case LUA_TNUMBER:
		case LUA_TSTRING:
		case LUA_TBOOLEAN:
		case LUA_TTABLE:
		case LUA_TNIL:
			value->push(L);
			break;
		default:
			lua_pushnil(L);
			break;
	}

	//Call destructor explicitly
	value->~ILuaAdapter();
}

// Helper function obtains a primitive value from stack and store it in the dictionary by key
static void dictionarySetPrimitiveValueByKey(lua_State* L, DictUserData* dict, const char* key, int valIndex) {
	if (!pSSE)
		return;

	NULL_CHK_RETURN_VOID(dict);
	NULL_CHK_RETURN_VOID(key);

	pSSE->DictionarySetValue(dict, key, L, valIndex);
}

// Helper function obtains a child dictionary value from stack and store it in the master dictionary by key
static bool dictionarySetNestedChildDictionaryByKey(lua_State* L, DictUserData* dict, const char* key, int valIndex) {
	ASSERT_SSE_0;
	NULL_CHK_RETURN(dict, false);
	NULL_CHK_RETURN(key, false);

	DictUserData* childDict = dictionaryFromStack(L, valIndex);

	// If the userdata being copied to is the same as the userdata being copied from, we will run into infinite recursion. We don't want that.
	if (childDict == NULL || childDict == dict) {
		return false;
	}
	pSSE->DictionarySetChildTableValue(dict, key, childDict->GetName());
	return true;
}

// Helper function obtains a table value from stack, convert it to a child dictionary then nested it in the master dictionary by key
static bool dictionarySetNestedTableByKey(lua_State* L, DictUserData* dict, const char* key, int valIndex) {
	ASSERT_SSE_0;
	NULL_CHK_RETURN(dict, false);
	NULL_CHK_RETURN(key, false);

	// Make sure stack has room for at least three more slots (one userdata, one key, and one value slot)
	int iSuccess = lua_checkstack(L, 3);
	if (iSuccess == 0) {
		LogError("Lua table cannot be assigned to userdata as stack has run out of slots!!");
		return false;
	} else {
		// Generate name automatically
		std::string prefix = pSSE->DictionaryGetDomainPrefix(L);
		std::string autoName = prefix + pSSE->DictionaryAutoGenerateName();
		const char* childDictName = autoName.c_str();

		// Create dictionary and import table
		HDICT hChildDict = NULL;
		fast_recursive_mutex* hChildDictMutex = NULL;

		int iTopIndex = lua_gettop(L);

		bool bSuccess = pSSE->DictionaryGenerate(childDictName, hChildDict, hChildDictMutex);
		if (bSuccess) {
			bSuccess = dictionaryCreateUserData(L, childDictName, false, hChildDict, hChildDictMutex);
			if (bSuccess) {
				bSuccess = dictionaryImportTable(L, valIndex) >= 0;
				if (bSuccess) {
					bSuccess = pSSE->DictionarySetChildTableValue(dict, key, childDictName);
				}
			}
		}

		// Make sure we skim off any excess data
		int iNewTopIndex = lua_gettop(L);
		if (iNewTopIndex > iTopIndex) {
			lua_settop(L, iTopIndex);
		}

		return bSuccess;
	}
}

// Helper function imports a lua table into an existing dictionary (at stack top) then return number of values imported.
static int dictionaryImportTable(lua_State* L, int tableIndex) {
	int nonStringKeysFound = 0;
	int tableValuesFound = 0;
	int otherValuesFound = 0;
	int valuesImported = 0;

	DictUserData* dict = dictionaryFromStack(L, -1);
	if (dict == NULL) {
		LogError("Dictionary is not valid");
		return -1;
	}

	lua_pushnil(L); // first key
	while (lua_next(L, tableIndex) != 0) {
		// uses 'key' (at index -2) and 'value' (at index -1)
		int keyType = lua_type(L, -2);
		if (keyType != LUA_TSTRING && keyType != LUA_TNUMBER) {
			nonStringKeysFound++;
		} else {
			std::string strKey;
			// CJW, 05.12.2016 - If the key is a number, running lua_tostring on it will not only return a string value but also
			// convert the value on the stack to a string, messing up the next call to lua_next. Thus, we make a copy of the key
			// on the stack and run lua_tostring on that to preserve stack integrity.
			if (keyType == LUA_TNUMBER) {
				lua_pushvalue(L, -2); // Make a copy of the numeric key
				const char* szKey = lua_tostring(L, -1); // Convert key to string
				strKey = szKey ? szKey : "";
				lua_pop(L, 1); // Pop the copied key, as we have no more use for it.
			} else {
				const char* szKey = lua_tostring(L, -2);
				strKey = szKey ? szKey : "";
			}
			ASSERT(!strKey.empty());

			if (!strKey.empty()) {
				int valType = lua_type(L, -1);
				switch (valType) {
					case LUA_TNUMBER:
					case LUA_TSTRING:
					case LUA_TBOOLEAN:
						dictionarySetPrimitiveValueByKey(L, dict, strKey.c_str(), -1);
						valuesImported++;
						break;
					case LUA_TUSERDATA:
						dictionarySetNestedChildDictionaryByKey(L, dict, strKey.c_str(), -1);
						valuesImported++;
						break;
					case LUA_TTABLE:
						// tableValuesFound ++;
						{
							int iChildTableIndex = lua_gettop(L);
							dictionarySetNestedTableByKey(L, dict, strKey.c_str(), iChildTableIndex);
							valuesImported++;
						}
						break;
					default:
						otherValuesFound++;
						break;
				}
			}
		}

		/* removes 'value'; keeps 'key' for next iteration */
		lua_pop(L, 1);
	}

	if (nonStringKeysFound > 0) {
		LogWarn("Ignored " << nonStringKeysFound << " non-string keys (number, table, etc)");
	}

	if (tableValuesFound > 0) {
		if (m_logger.isEnabledFor(log4cplus::DEBUG_LOG_LEVEL)) {
			// Additional detail for debugging
			int lineNumber = -1;
			std::string source;
			ScriptServerEngine::Instance()->ScriptGetSourceInfo(L, 1, source, lineNumber);
			//			LogWarn("Ignored " << tableValuesFound << " nested tables, source: " << source << ", line: " << lineNumber);
		} else {
			//			LogWarn("Ignored " << tableValuesFound << " nested tables");
		}
	}

	if (otherValuesFound > 0) {
		LogWarn("Ignored " << otherValuesFound << " values of unsupported primitive types (functions e.g.)");
	}

	return valuesImported;
}

int dictGenerate(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	// Use bit mask and badArgumentsCheckAdvanced to check arguments with variant types
	static const int args[] = {
		(1 << LUA_TSTRING) | (1 << LUA_TNIL), // dictionary name
		1 << LUA_TBOOLEAN, // read-only flag
	};

	std::string errorStr;
	if (!badArgumentsCheckAdvanced(L, ARGS_N_SIZE(args), &errorStr, 1)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Get arguments.
	const char* dictName = lua_tostring(L, 1); // dictName can be NULL if the name parameter is not provided by the caller
	bool readonly = false;
	if (lua_gettop(L) > 1) {
		readonly = lua_toboolean(L, 2) != 0;
	}

	// Generate name automatically if not provided
	std::string autoName;
	if (dictName == NULL) {
		autoName = pSSE->DictionaryAutoGenerateName();
		dictName = autoName.c_str();
	}

	// Optional domain prefix for data isolation
	std::string nameWithPrefix;
	std::string prefix = pSSE->DictionaryGetDomainPrefix(L);
	if (!prefix.empty()) {
		nameWithPrefix = prefix + dictName;
		dictName = nameWithPrefix.c_str();
	}

	// Try get the fulluserdata value from cache first
	if (dictionaryGetUserDataFromCache(L, dictName, readonly)) {
		// Succeeded: userdata at stack pos -1
	} else {
		lua_pop(L, 1); // pop the nil value

		// Call engine to create a new dictionary or attach to an existing one
		HDICT hDict = NULL;
		fast_recursive_mutex* hDictMutex = NULL;
		if (!pSSE->DictionaryGenerate(dictName, hDict, hDictMutex)) {
			StrBuild(errorStr, "Error occurred while creating a new dictionary `" << dictName << "'");
			LuaParseArgsError(errorStr);
			return 0;
		}

		// Create userdata in the stack
		(void)dictionaryCreateUserData(L, dictName, readonly, hDict, hDictMutex);
	}

	return 1;
}

int dictRelease(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	// Use bit mask and badArgumentsCheckAdvanced to check arguments with variant types
	static const int args[] = { 1 << LUA_TUSERDATA }; // dictionary

	std::string errorStr;
	if (!badArgumentsCheckAdvanced(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Get arguments.
	DictUserData* dict = dictionaryFromStack(L, 1);
	if (dict == NULL) {
		StrBuild(errorStr, "Argument 1 must be dictionary, got " << luaTypeToString(lua_type(L, 1)));
		LuaParseArgsError(errorStr);
		return 0;
	}

	if (pSSE->DictionaryRelease(dict->GetName()))
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int dictGetValue(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	// Use bit mask and badArgumentsCheckAdvanced to check arguments with variant types
	static const int args[] = {
		1 << LUA_TUSERDATA, // dictionary
		(1 << LUA_TSTRING) | (1 << LUA_TNUMBER), // key
	};

	std::string errorStr;
	if (!badArgumentsCheckAdvanced(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Get arguments.
	DictUserData* dict = dictionaryFromStack(L, 1);
	if (dict == NULL) {
		StrBuild(errorStr, "Argument 1 must be dictionary, got " << luaTypeToString(lua_type(L, 1)));
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	const char* key = lua_tostring(L, 2);
	ASSERT(key != NULL);

	dictionaryGetValueByKey(L, dict, key);
	return 1; // number of return values
}

int dictSetValue(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	// Use bit mask and badArgumentsCheckAdvanced to check arguments with variant types
	static const int args[] = {
		1 << LUA_TUSERDATA, // dictionary
		(1 << LUA_TSTRING) | (1 << LUA_TNUMBER), // key
		(1 << LUA_TNUMBER) | (1 << LUA_TSTRING) |
			(1 << LUA_TBOOLEAN) | (1 << LUA_TUSERDATA) |
			(1 << LUA_TTABLE) | (1 << LUA_TNIL), // values
	};

	std::string errorStr;
	if (!badArgumentsCheckAdvanced(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Get arguments.
	DictUserData* dict = dictionaryFromStack(L, 1);
	if (dict == NULL) {
		StrBuild(errorStr, "Argument 1 must be dictionary, got " << luaTypeToString(lua_type(L, 1)));
		LuaParseArgsError(errorStr);
		return 0;
	}

	if (dict->IsReadonly()) {
		pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "ERROR:: object is read-only. Function: " __FUNCTION__);
		return 0; // number of return values
	}

	const char* key = lua_tostring(L, 2);
	ASSERT(key != NULL);

	switch (lua_type(L, 3)) {
		case LUA_TNUMBER:
		case LUA_TSTRING:
		case LUA_TBOOLEAN:
			dictionarySetPrimitiveValueByKey(L, dict, key, 3);
			break;
		case LUA_TUSERDATA:
			if (!dictionarySetNestedChildDictionaryByKey(L, dict, key, 3)) {
				StrBuild(errorStr, "Argument 3 must be boolean, number, string or dictionary that's different than the dictionary in argument 1, got " << luaTypeToString(lua_type(L, 3)));
				LuaParseArgsError(errorStr);
				return 0;
			}
			break;
		case LUA_TTABLE:
			if (!dictionarySetNestedTableByKey(L, dict, key, 3)) { // convert table into child dictionary
				StrBuild(errorStr, "Error occurred while building a child dictionary for the nested table value, key = `" << key << "'");
				LuaParseArgsError(errorStr);
				return 0;
			}
			break;
		default:
			//LUA_TNIL -> remove value
			pSSE->DictionaryRemoveValue(dict, key);
			break;
	}

	return 0; // number of return values
}

int dictClearValues(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	// Use bit mask and badArgumentsCheckAdvanced to check arguments with variant types
	static const int args[] = { 1 << LUA_TUSERDATA }; // dictionary

	std::string errorStr;
	if (!badArgumentsCheckAdvanced(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Get arguments.
	DictUserData* dict = dictionaryFromStack(L, 1);
	if (dict == NULL) {
		StrBuild(errorStr, "Argument 1 must be dictionary, got " << luaTypeToString(lua_type(L, 1)));
		LuaParseArgsError(errorStr);
		return 0;
	}

	if (dict->IsReadonly()) {
		pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "ERROR:: object is read-only. Function: " __FUNCTION__);
		return 0; // number of return values
	}

	if (pSSE->DictionaryClear(dict))
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1; // number of return values
}

int dictGetInfo(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	// Use bit mask and badArgumentsCheckAdvanced to check arguments with variant types
	// Since we can't tell light userdata type from a full userdata in the script, we have to tolerate a lightuserdata input, which will be ignored later.
	static const int args[] = { (1 << LUA_TUSERDATA) | (1 << LUA_TLIGHTUSERDATA) }; // dictionary

	std::string errorStr;
	if (!badArgumentsCheckAdvanced(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Get arguments.
	DictUserData* dict = lua_type(L, 1) == LUA_TUSERDATA ? dictionaryFromStack(L, 1) : NULL; // Fail silently if input is a light userdata value
	if (dict == NULL) {
		// Input is not a valid dictionary
		// Return nil quietly. Script can use this function to determine if an userdata value is a dictionary.
		return 0;
	}

	int refCount = pSSE->DictionaryGetRefCount(dict);
	if (refCount < 0) {
		return 0; // number of return values
	} else {
		const char* dictName = pSSE->DictionaryStripDomainPrefixFromName(dict->GetName());
		lua_pushstring(L, dictName);
		lua_pushnumber(L, refCount);
		return 2; // number of return values
	}
}

int dictGetAllKeys(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	// Use bit mask and badArgumentsCheckAdvanced to check arguments with variant types
	static const int args[] = { 1 << LUA_TUSERDATA }; // dictionary

	std::string errorStr;
	if (!badArgumentsCheckAdvanced(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Get arguments.
	DictUserData* dict = dictionaryFromStack(L, 1);
	if (dict == NULL) {
		StrBuild(errorStr, "Argument 1 must be dictionary, got " << luaTypeToString(lua_type(L, 1)));
		LuaParseArgsError(errorStr);
		return 0;
	}

	std::set<std::string> allKeys;
	pSSE->DictionaryGetAllKeys(dict, allKeys);

	lua_newtable(L);

	int index = 1;
	for (std::set<std::string>::const_iterator it = allKeys.begin(); it != allKeys.end(); ++it, ++index) {
		lua_pushstring(L, it->c_str());
		lua_rawseti(L, -2, index);
	}

	return 1;
}

int dictIterate(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	// Use bit mask and badArgumentsCheckAdvanced to check arguments with variant types
	static const int args[] = {
		1 << LUA_TTABLE, // iteration table (dictionary, allKeys, index)
		(1 << LUA_TSTRING) | (1 << LUA_TNIL), // previous key or nil
	};

	std::string errorStr;
	if (!badArgumentsCheckAdvanced(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Get arguments.
	// Unpack table (after unpacking: -3=dictionary, -2=allKeys, -1=index)
	lua_rawgeti(L, 1, 1);
	lua_rawgeti(L, 1, 2);
	lua_rawgeti(L, 1, 3);

	DictUserData* dict = dictionaryFromStack(L, -3);
	if (dict == NULL) {
		StrBuild(errorStr, "Argument 1 must be dictionary, got " << luaTypeToString(lua_type(L, 1)));
		LuaParseArgsError(errorStr);
		return 0;
	}

	// Read and pop index
	int index = 0;
	if (!lua_isnil(L, -1)) {
		index = (int)lua_tonumber(L, -1);
	}
	lua_pop(L, 1); // index popped, now -2=dictionary, -1=allKeys

	// Increment index and save it back in table
	index++;
	lua_pushnumber(L, index);
	lua_rawseti(L, 1, 3);

	// Get next key by the new index
	lua_rawgeti(L, -1, index);

	// Read from dictionary
	const char* key = NULL;
	if (!lua_isnil(L, -1)) {
		key = (const char*)lua_tostring(L, -1);
	}

	if (dict != NULL && key != NULL) {
		lua_pushstring(L, key);
		dictionaryGetValueByKey(L, dict, key);
	} else {
		lua_pushnil(L);
		lua_pushnil(L);
	}

	return 2;
}

int scriptGC(lua_State* L) {
	ASSERT_SSE_0;

	lua_gc(L, LUA_GCCOLLECT, 0);
	return 0;
}

int classGetProperty(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	// Use bit mask and badArgumentsCheckAdvanced to check arguments with variant types
	static const int args[] = {
		1 << LUA_TTABLE, // class
		1 << LUA_TSTRING, // property name
	};

	std::string errorStr;
	if (!badArgumentsCheckAdvanced(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	int n = lua_gettop(L);

	LogDebug("table[\"" << lua_tostring(L, 2) << "\"]");
	const char* key = lua_tostring(L, 2);

	// 1. Try the dictionary first
	// check if self._dictionary is dictionary
	lua_pushstring(L, CLASS_PROPERTIES_DICTIONARY);
	lua_rawget(L, 1);
	DictUserData* dict = dictionaryFromStack(L, -1);
	if (dict != NULL) {
		dictionaryGetValueByKey(L, dict, key);

		if (!lua_isnil(L, -1)) {
			LogDebug("table[\"" << key << "\"] returned " << luaTypeToString(lua_type(L, -1)) << " value from dictionary");
			return 1;
		}
	}

	// 2. Not in dictionary: do table rawget (do separate metatable lookup to avoid infinite recursion)
	lua_settop(L, n); // Restore stack
	lua_pushvalue(L, 2); // Duplicate key in the stack so that the original copy will not get popped after rawget
	lua_rawget(L, 1);
	if (!lua_isnil(L, -1)) { // rawget(self, key) == nil ?
		LogDebug("table[\"" << key << "\"] returned " << luaTypeToString(lua_type(L, -1)) << " value from self");
		return 1;
	}

	// 3. Not in self: try metatable
	lua_settop(L, n); // Restore stack
	lua_pushstring(L, "_class"); // Obtain self._class table
	lua_rawget(L, 1);
	if (!lua_istable(L, -1)) {
		LogWarn("table[\"" << key << "\"] invalid self._class, expected: table, got: " << luaTypeToString(lua_type(L, -1)));
		return 1;
	}

	lua_pushvalue(L, 2); // Push key
	lua_gettable(L, -2); // self._class[key]

	LogDebug("table[\"" << key << "\"] returned " << luaTypeToString(lua_type(L, -1)) << " value from class");
	return 1; // number of return values
}

int classSetProperty(lua_State* L) {
	ASSERT_SSE_0;

	// Check arguments.
	// Use bit mask and badArgumentsCheckAdvanced to check arguments with variant types
	static const int args[] = {
		1 << LUA_TTABLE, // class
		1 << LUA_TSTRING, // property name
		0x7FFFFFFF, // value - all types
	};

	std::string errorStr;
	if (!badArgumentsCheckAdvanced(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	int n = lua_gettop(L);

	LogDebug("table[\"" << lua_tostring(L, 2) << "\"], value type is " << luaTypeToString(lua_type(L, 3)));
	const char* key = lua_tostring(L, 2);

	// Check if _properties table exists
	lua_pushstring(L, CLASS_PROPERTIES_TABLE);
	lua_rawget(L, 1); // self._properties
	if (lua_istable(L, -1)) {
		// Verify key against _properties table
		lua_pushvalue(L, 2); // key
		lua_gettable(L, -2); // self._properties[key]
		if (!lua_isnil(L, -1)) {
			lua_settop(L, n); // Restore stack

			// Store number, string, boolean and child-dictionary in self._dictionary dictionary if available. Assign nil to dictionary as well.
			if (lua_type(L, 3) == LUA_TNUMBER || lua_type(L, 3) == LUA_TSTRING || lua_type(L, 3) == LUA_TBOOLEAN || lua_type(L, 3) == LUA_TUSERDATA || lua_type(L, 3) == LUA_TTABLE || lua_type(L, 3) == LUA_TNIL) {
				// check if self._dictionary is dictionary
				lua_pushstring(L, CLASS_PROPERTIES_DICTIONARY);
				lua_rawget(L, 1); // dictionary in stack
				if (lua_type(L, -1) == LUA_TUSERDATA) {
					DictUserData* dict = dictionaryFromStack(L, -1);
					if (dict != NULL) {
						// dictSetValue requires that all three parameters are at the bottom of the stack
						lua_pushvalue(L, 2); // push key, value
						lua_pushvalue(L, 3);
						lua_remove(L, 1); // remove original 3 parameters from stack
						lua_remove(L, 1);
						lua_remove(L, 1);

						LogDebug("table[\"" << key << "\"], stored value in dictionary. Value type is " << luaTypeToString(lua_type(L, 3)));
						return dictSetValue(L); // self._dictionary[key] = value
					}
				}

				LogError("table[\"" << key << "\"], `_properties' exists but `_dictionary' is invalid (" << luaTypeToString(lua_type(L, -1)) << ")");
				// Critical error
				StrBuild(errorStr, "Unable to set property `" << key << "': internal structure `_dictionary` is corrupted. Try restart your script.");
				LuaParseArgsError(errorStr);
				return 0; // number of return values
			} else {
				// Unsupported value type
				StrBuild(errorStr, "Invalid value for property `" << key << "', expected: number/string/boolean/table/dictionary, got: " << luaTypeToString(lua_type(L, 3)));
				LuaParseArgsError(errorStr);
				return 0; // number of return values
			}
		}
	} else if (!lua_isnil(L, -1)) {
		// _properties exists but is not a table.
		LogError("table[\"" << key << "\"], `_properties' exists but not a table (" << luaTypeToString(lua_type(L, -1)) << ")");
		// Critical error
		StrBuild(errorStr, "Unable to set property `" << key << "': internal structure `_properties' is corrupted");
		LuaParseArgsError(errorStr);
		return 0; // number of return values
	}

	// Otherwise, store in the table itself
	lua_settop(L, n); // Restore stack

	LogDebug("table[\"" << key << "\"], stored value in the table. Value type is " << luaTypeToString(lua_type(L, 3)));
	lua_rawset(L, 1); // self[key] = value
	return 0; // number of return values
}

// Read saved current player data from registry -- for kgp_depart workarounds when current player information is already removed from engine
static bool luaGetCurrentPlayerData(lua_State* L, ULONG clientId, const std::string& key, int expectedType) {
	// Return value
	bool res = false;

	// Save stack pointer
	int stackTop = lua_gettop(L);

	// Get temp data table from registry
	lua_getfield(L, LUA_REGISTRYINDEX, REGISTRY_KGPHANDLER_TEMPDATA);
	if (lua_istable(L, -1)) {
		// Get saved current client ID
		lua_getfield(L, -1, KGPHANDLER_TEMPDATA_CLIENTID); // kgpHandlerTempData.clientId
		if (lua_type(L, -1) == LUA_TNUMBER) {
			// Verify if client ID matches
			ULONG currentClientId = (ULONG)lua_tonumber(L, -1);
			lua_pop(L, 1);
			if (currentClientId == clientId) {
				// Retrieve data by key
				lua_getfield(L, -1, key.c_str()); // kgpHandlerTempData[key]
				if (lua_type(L, -1) == expectedType) {
					// Value exists - swap it with the temp data table so it will stay after stack restore
					lua_insert(L, stackTop + 1);
					res = true;
				}
			}
		}
	}

	// Restore stack and leave return value if found
	lua_settop(L, stackTop + (res ? 1 : 0));
	return res;
}

// Explode the table at the specified stack index into loose values. The table will be popped if popTable is true. Loose values will be pushed to the top of the stack.
// Second parameter `keys' is an array of strings terminated by a NULL pointer. It decides the order of the loose values. Nil values will be pushed on missing keys.
// If `keys' is NULL, this function will iterate the table by numeric indices beginning at 1 and stop if any index is missing.
//
// NOTE: This function use lua_rawgeti for numeric indices. Hence no metatable lookup in such case.
bool luaExplodeTable(lua_State* L, int tableIndex, const char* const* keys, bool popTable, bool allowNilValues) {
	// Validate parameters
	jsVerifyReturn(lua_istable(L, tableIndex) && tableIndex > LUA_REGISTRYINDEX, false);

	// Convert table index to absolute value
	int stackTop = lua_gettop(L);
	if (tableIndex < 0) {
		tableIndex += stackTop + 1;
	}

	jsVerifyReturn(tableIndex > 0, false);

	// Push a temp copy of the table at the top
	lua_pushvalue(L, tableIndex);

	if (keys) {
		// By string keys
		for (int i = 0; keys[i] != NULL; ++i) {
			lua_getfield(L, -1, keys[i]); // Value on top of the stack
			if (!allowNilValues && lua_isnil(L, -1)) {
				lua_settop(L, stackTop); // Restore stack and return false
				return false;
			}
			lua_insert(L, -2); // Swap value with table
		}
	} else {
		// By numeric indices
		for (int key = 1;; ++key) {
			lua_rawgeti(L, -1, key); // Value on top of the stack
			if (lua_isnil(L, -1)) {
				lua_pop(L, 1); // Pop the nil value and terminate loop
				break;
			}
			lua_insert(L, -2); // Swap value with table
		}
	}

	// Pop the temp table
	lua_pop(L, 1);

	// Remove the existing table if necessary
	if (popTable) {
		lua_remove(L, tableIndex);
	}

	return true;
}

inline int getGameStateHelper(lua_State* L, const char* metaTableName) {
	ULONG id;
	if (!LuaParseArgs("u", &id))
		return 0;

	int udSize = sizeof(GameStateUserData);
	DictUserData* dict = (DictUserData*)lua_newuserdata(L, udSize);
	if (dict == NULL) {
		//TODO: error reporting
		//Failed: nil at stack pos -1
		lua_pushnil(L);
		return 1;
	}

	// call constructor by placement-new
	new (dict) GameStateUserData(id);

	// Set metatable
	luaL_getmetatable(L, metaTableName);
	lua_setmetatable(L, -2);

	// Succeeded: userdata at stack pos -1
	return 1;
}

int objectGetGameState(lua_State* L) {
	ASSERT_SSE_0;
	return getGameStateHelper(L, OBJECT_METATABLE_NAME);
}

int playerGetGameState(lua_State* L) {
	ASSERT_SSE_0;
	return getGameStateHelper(L, PLAYER_METATABLE_NAME);
}

inline int gameStateGetValueHelper(lua_State* L, const char* func, int entityType) {
	if (lua_gettop(L) != 2) {
		_LuaParseArgsError(func, L, "Expecting 2 arguments");
		return 0;
	}

	if (lua_type(L, 2) != LUA_TSTRING) {
		_LuaParseArgsError(func, L, "Argument 2 must be a string");
		return 0;
	}

	jsAssert(entityType == ObjTypeToInt(ObjectType::DYNAMIC) || entityType == ObjTypeToInt(ObjectType::MOVEMENT));
	const char* metaTableName = entityType == ObjTypeToInt(ObjectType::DYNAMIC) ? OBJECT_METATABLE_NAME : PLAYER_METATABLE_NAME;

	// Get arguments.
	GameStateUserData* ud = (GameStateUserData*)luaL_checkudata(L, 1, metaTableName);
	luaL_argcheck(L, ud != NULL, 1, "`gameState' expected"); //TODO: check if lua to automatically return when error occurred
	if (ud == NULL) {
		std::string errorStr;
		StrBuild(errorStr, "Argument 1 must be an " << metaTableName << ", got " << luaTypeToString(lua_type(L, 1)));
		_LuaParseArgsError(func, L, errorStr);
		return 0;
	}

	const char* key = lua_tostring(L, 2);
	ASSERT(key != NULL);

	// Allocate return buffer C in stack and cast buffer address to pointer
	unsigned char buffer[MAX_LUA_VALUE_CONTAINER_SIZE];
	void** ppVT = (void**)buffer;
	ILuaAdapter* value = (ILuaAdapter*)buffer;

	// Wipe out first four bytes (vtable ptr) so we can do sanity check afterwards
	*ppVT = NULL;

	int valueType;
	if (entityType == ObjTypeToInt(ObjectType::DYNAMIC))
		valueType = pSSE->ObjectGameStateGetValue(L, ud->GetID(), key, buffer, sizeof(buffer));
	else
		valueType = pSSE->PlayerGameStateGetValue(L, ud->GetID(), key, buffer, sizeof(buffer));

	// Sanity check
	if (*ppVT == NULL) {
		// Something is wrong
		LogError("GameStateGetValue function did not fill buffer properly, class=" << metaTableName << ", ID=" << ud->GetID());
		jsAssert(false);
		lua_pushnil(L);
	} else {
		switch (valueType) {
			case LUA_TNUMBER:
			case LUA_TSTRING:
			case LUA_TBOOLEAN:
			case LUA_TTABLE:
			case LUA_TNIL:
				value->push(L);
				break;
			default:
				LogError("GameStateGetValue function returned unknown data type " << valueType << ", class=" << metaTableName << ", ID=" << ud->GetID());
				jsAssert(false);
				lua_pushnil(L);
				break;
		}

		//Call destructor explicitly
		value->~ILuaAdapter();
	}

	return 1;
}

int objectGameStateGetValue(lua_State* L) {
	return gameStateGetValueHelper(L, __FUNCTION__, ObjTypeToInt(ObjectType::DYNAMIC));
}

int playerGameStateGetValue(lua_State* L) {
	return gameStateGetValueHelper(L, __FUNCTION__, ObjTypeToInt(ObjectType::MOVEMENT));
}

int scriptRecordMetric(lua_State* L) {
	ASSERT_SSE_0;

	static const int args[] = {
		LUA_TSTRING,
		LUA_TTABLE
	};

	std::string errorStr;
	if (!badArgumentsCheck(L, ARGS_N_SIZE(args), &errorStr)) {
		LuaParseArgsError(errorStr);
		return 0; // incorrect number of return values
	}

	if (!lua_istable(L, -1)) {
		pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "Bad arguments for scriptRecordMetric; metrics data must be a table of key-value pairs.");
		return 0;
	}

	// Push metricName param onto CSV.
	std::stringstream ss;
	ss << lua_tostring(L, 1) << ",";

	// Add metric key-value pairs to CSV.
	std::string key;
	std::string value;

	lua_pushnil(L);
	while (lua_next(L, -2) != 0) {
		if (!lua_isstring(L, -1) || !lua_isstring(L, -2)) {
			pSSE->ScriptErrorCallback(L, BAD_ARGUMENTS_ERROR_CODE, "Bad arguments for scriptRecordMetric; keys and values of metrics data must be strings.");
			return 0;
		}

		key = lua_tostring(L, -2);
		value = lua_tostring(L, -1);

		// Clear out extra commas
		std::replace(key.begin(), key.end(), ',', ' ');
		std::replace(value.begin(), value.end(), ',', ' ');

		ss << key << "," << value << ",";

		lua_pop(L, 1);
	}

	// Remove trailing ",".
	std::string metricsCSV = ss.str();
	metricsCSV.pop_back();

	bool res = pSSE->ScriptRecordMetric(L, metricsCSV);
	if (res)
		lua_pushnumber(L, 1);
	else
		lua_pushnil(L);

	return 1;
}

int scriptCallAsync(lua_State* L) {
	int n = lua_gettop(L);
	if (n < 2) {
		LuaParseArgsError("Too few arguments, expecting at least 2");
		return 0;
	}

	if (lua_type(L, 1) != LUA_TNUMBER && lua_type(L, 1) != LUA_TSTRING && lua_type(L, 1) != LUA_TNIL) {
		LuaParseArgsError("Argument 1 must be either a number, a string or nil");
		return 0;
	}

	if (lua_type(L, 2) != LUA_TSTRING) {
		LuaParseArgsError("Argument 2 must be a string");
		return 0;
	}

	// Name of the API to invoke
	auto apiName = lua_tostring(L, 2);
	ASSERT(apiName != nullptr);
	std::string sAPIName(apiName ? apiName : "(null)");

	auto itr = s_scriptServerAsyncAPIs.find(apiName);
	if (itr == s_scriptServerAsyncAPIs.end()) {
		LuaParseArgsError("`" + sAPIName + "' is not a valid asynchronous API");
		return 0;
	}

	// User args
	AsyncUserArg userArg;
	userArg.m_luaType = lua_type(L, 1);
	switch (userArg.m_luaType) {
		case LUA_TNUMBER:
			userArg.m_userInt = lua_tonumber(L, 1);
			break;
		case LUA_TSTRING:
			ASSERT(lua_tostring(L, 1) != nullptr);
			userArg.m_userStr = lua_tostring(L, 1);
			break;
	}

	// Drop first two arguments from lua stack
	lua_remove(L, 1);
	lua_remove(L, 1);

	// Invoke
	return itr->second(L, userArg);
}

///////////////////////////////////////////////////////////////////////////////
// NPC Functions
///////////////////////////////////////////////////////////////////////////////

int npcSpawn(
	lua_State* L,
	SCRIPT_NAMED_PARAM_OPT(int, PID),
	SCRIPT_NAMED_PARAM_OPT(std::string, charConfig),
	SCRIPT_NAMED_PARAM_OPT(int, charConfigId),
	SCRIPT_NAMED_PARAM_OPT(int, dbIndex),
	SCRIPT_NAMED_PARAM_OPT(KEP::Vector3f, pos),
	SCRIPT_NAMED_PARAM_OPT(double, rotDeg),
	SCRIPT_NAMED_PARAM_OPT(GLID, glidAnimSpecial),
	SCRIPT_NAMED_PARAM_OPT(std::vector<GLID>, glidsArmed)) {
	ASSERT_SSE_0;

	if (!charConfigId.IsSet() && !charConfig.IsSet()) {
		LuaParseArgsError("Missing both charConfigId and charConfig parameters. Must provide one");
		return 0;
	}

	return pSSE->NPCSpawnCallback(
		L,
		5, // SERVER_ITEM_TYPE::SI_TYPE_PAPER_DOLL
		PID.GetOptional(0),
		charConfig.GetOptional(""),
		charConfigId.GetOptional(-1),
		dbIndex.GetOptional(0),
		pos.GetOptional(Vector3f()),
		rotDeg.GetOptional(0),
		glidAnimSpecial.GetOptional(GLID_INVALID),
		glidsArmed.GetOptional(std::vector<GLID>()));
}
